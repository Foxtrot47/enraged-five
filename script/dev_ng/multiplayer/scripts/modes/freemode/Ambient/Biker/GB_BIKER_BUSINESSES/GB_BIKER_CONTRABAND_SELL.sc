
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Written by:  Christopher Speirs & Steve Tiley																		//
// Date: 		02/02/2016					 																			//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    

// Useful prints to search for:
// SET_END_REASON

// Exchange: launch from Paleto Bay Cash
// Bag Drop: launch from Weed North

// BAGS:
// Removed locally in CLIENT_MAINTAIN_BAG_DROP_OFFS
// Server checks for delivery in SCRIPT_EVENT_BAG_DROP

// DEBUG: Use shift > D to display useful information onscreen

// Drop-offs: 		GET_SELL_DROP_COORDS
// Veh start pos: 	GET_LAND_SELL_VEHICLE_CREATION_POS
// Drop-off:		vGET_DROP_OFF_VEH_START

// Locate size drop off	: vLOCATE_DIMENSION
// Locate size visual	: vMARKER_DIMENSIONS

// Dop-off vehs made: CREATE_DROP_OFF_VEH

// Bugs with code: url:bugstar:3072039 - Sell - Benson - Unable to deliver motorcycle to truck due to ramp position

USING "globals.sch"
USING "net_mission.sch"
USING "net_common_functions.sch"
USING "net_realty_warehouse.sch"
USING "net_management.sch"
//USING "net_power_ups.sch"

USING "GB_BIKER_CONTRABAND_SELL_HEADER_PRIVATE.sch"


INT iDroppedOffCountClubRunCache = -1
INT iHelpType = -1
CONST_INT ciCLUB_RUN_HELP_TYPE_WAIT 	0
CONST_INT ciCLUB_RUN_HELP_TYPE_PRODUCT	1



FUNC BOOL SERVER_SET_SHIPMENT_TYPE()
	IF serverBD.eShipmentType = eBIKER_SHIPMENT_TYPE_INVALID
		INT iSize
		eBIKER_SHIPMENT_TYPE eType = BIKER_GET_SELL_SHIPMENT_TYPE(iSize)
		//eType = eSELL_SHIPMENT_TYPE_LAND_THREE_BRICKADE
		SET_SHIPMENT_TYPE(eType)
		SET_SHIPMENT_SIZE(iSize)
		RETURN TRUE
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				BIGMESSAGE														║

FUNC BOOL IS_LOCAL_BIGMESSAGE_BIT0_SET(INT iBitset)
	RETURN IS_BIT_SET(sLocaldata.iBigMessageBitSet, iBitset)
ENDFUNC

PROC SET_LOCAL_BIGMESSAGE_BIT0(INT iBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(iLocalBitset0, iBitset)
			PRINTLN("[BIKER_SELL] SET_LOCAL_BIGMESSAGE_BIT0 - ", iBitset)
		ENDIF
	ENDIF
	#ENDIF
	
	SET_BIT(sLocaldata.iBigMessageBitSet, iBitset)
	
ENDPROC

PROC CLEAR_LOCAL_BIGMESSAGE_BIT0(INT iBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF IS_BIT_SET(iLocalBitset0, iBitset)
			PRINTLN("[BIKER_SELL] CLEAR_LOCAL_BIGMESSAGE_BIT0 - ", iBitset)
		ENDIF
	ENDIF
	#ENDIF
	
	CLEAR_BIT(sLocaldata.iBigMessageBitSet, iBitset)
	
ENDPROC

#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Prints information about the big message that has just been requested to draw
/// PARAMS:
///    iBigMessage - the index of the message that was drawn
///    title - the title of the message
///    strapline - the subtitle of the message
PROC PRINT_BIG_MESSAGE_DEBUG_INFORMATION(INT iBigMessage, STRING title, STRING strapline)
	PRINTLN("[BIKER_SELL]  - ************ NEW SHARD HAS BEEN SETUP *************")
	PRINTLN("[BIKER_SELL]  - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Big Message:	", iBigMessage)
	PRINTLN("[BIKER_SELL]  - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Title: 			", GET_FILENAME_FOR_AUDIO_CONVERSATION(title))
	PRINTLN("[BIKER_SELL]  - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Strapline:		", GET_FILENAME_FOR_AUDIO_CONVERSATION(strapline))
	PRINTLN("[BIKER_SELL]  - ***************************************************")
ENDPROC
#ENDIF

PROC SERVER_SET_ACTIVE_DROP(INT iVeh, INT iCount)
	SET_BIT(serverBD.iActiveDropOffBitset[iVeh], iCount)
	PRINTLN("[BIKER_SELL] [SERVER_DROP] SERVER_SET_ACTIVE_DROP for iVeh = ", iVeh, " iCount = ", iCount)
ENDPROC

PROC SERVER_CLEAR_ACTIVE_DROP(INT iVeh, INT iCount)
	CLEAR_BIT(serverBD.iActiveDropOffBitset[iVeh], iCount)
	PRINTLN("[BIKER_SELL] [SERVER_DROP] SERVER_CLEAR_ACTIVE_DROP for iVeh = ", iVeh, " iCount = ", iCount)
ENDPROC

PROC SERVER_INCREASE_DROP_PENALTY(INT iPenalty = 1)
	serverBD.iDropPenalty += iPenalty
	PRINTLN("[BIKER_SELL] SERVER_INCREASE_DROP_PENALTY  serverBD.iDropPenalty = ", serverBD.iDropPenalty, " - iPenalty = ", iPenalty)
ENDPROC

PROC SERVER_INCREASE_VEHICLE_PENALTY(INT iVeh)
	serverBD.iVehPenalty[iVeh]++
	PRINTLN("[BIKER_SELL] SERVER_INCREASE_VEHICLE_PENALTY  serverBD.iVehPenalty = ", serverBD.iVehPenalty[iVeh])
ENDPROC

PROC SERVER_CLEAR_VEH_ACTIVE_DROPS(INT iVeh)
	IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
		INT i
		REPEAT GET_MAX_SELL_DROP_OFFS() i
			IF IS_BIT_SET(serverBD.iActiveDropOffBitset[iVeh], i)
				PRINTLN("[BIKER_SELL] SERVER_CLEAR_VEH_ACTIVE_DROPS - Vehicle ", iVeh, " has failed its drops, clearing drop ", i)
				CLEAR_BIT(serverBD.iActiveDropOffBitset[iVeh], i)
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

FUNC STRING GET_SHIPMENT_TYPE_STRING()

//	IF BIKER_SELL_EXCHANGE()
//	AND BIKER_SELL_HAS_TWIST_HAPPENED()
////		IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
////			RETURN	"SBCONTRA_VANS"
////		ELSE
//			RETURN	"SBCONTRA_VAN"
////		ENDIF
//	ENDIF
	
	SWITCH GET_SELL_VAR()
		//coke
		CASE eBIKER_SELL_STING_OP
			RETURN	"SBCONTRA_COKE"

		//meth
//		CASE eBIKER_SELL_TACO_VAN
//			RETURN	"SBCONTRA_METH"

		//money
//		CASE eBIKER_SELL_EXCHANGE
//			RETURN "SBCONTRA_MONEY"	

		//IDs
		CASE eBIKER_SELL_FRIENDS_IN_NEED
			RETURN "SBCONTRA_IDS"

		//weed
//		CASE eBIKER_SELL_MEDICAL_PURPOSES
//		CASE eBIKER_SELL_TRASHMASTER
		CASE eBIKER_SELL_PROVEN
			RETURN	"SBCONTRA_WEED"	

		//get based on factory launched from
		CASE eBIKER_SELL_DEFAULT
		CASE eBIKER_SELL_BORDER_PATROL
		CASE eBIKER_SELL_HELICOPTER_DROP
		CASE eBIKER_SELL_AIR_DROP_AT_SEA
		CASE eBIKER_SELL_TRASHMASTER
		CASE eBIKER_SELL_POSTMAN
		CASE eBIKER_SELL_BENSON
		CASE eBIKER_SELL_CLUB_RUN
		CASE eBIKER_SELL_RACE
		CASE eBIKER_SELL_BAG_DROP		
			SWITCH serverBD.factoryType
				CASE FACTORY_TYPE_CRACK			RETURN "SBCONTRA_COKE"
				CASE FACTORY_TYPE_FAKE_IDS		RETURN "SBCONTRA_IDS"
				CASE FACTORY_TYPE_FAKE_MONEY	RETURN "SBCONTRA_MONEY"	
				CASE FACTORY_TYPE_METH			RETURN "SBCONTRA_METH"
				CASE FACTORY_TYPE_WEED			RETURN "SBCONTRA_WEED"	
			ENDSWITCH
		BREAK
	ENDSWITCH

	//product
	RETURN "SBCONTRA_DEF"
ENDFUNC

FUNC STRING GET_SHIPMENT_TYPE_STRING_BLIPS()//BOOL bIgnoreMultiple = FALSE)

//	IF BIKER_SELL_EXCHANGE()
//	AND BIKER_SELL_HAS_TWIST_HAPPENED()
//		IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
//		AND NOT bIgnoreMultiple
//			RETURN "SBCONTRA_VANBS"
//		ELSE
//			RETURN "SBCONTRA_VANB"
//		ENDIF
//	ENDIF
	
	SWITCH GET_SELL_VAR()
		//coke
		CASE eBIKER_SELL_STING_OP
			RETURN "SBCONTRA_COKEB"

		//meth
//		CASE eBIKER_SELL_TACO_VAN
//			RETURN "SBCONTRA_METHB"

		//money
//		CASE eBIKER_SELL_EXCHANGE
//			RETURN "SBCONTRA_MONEYB"	

		//IDs
		CASE eBIKER_SELL_FRIENDS_IN_NEED
			RETURN "SBCONTRA_IDSB"

		//weed
//		CASE eBIKER_SELL_MEDICAL_PURPOSES
		CASE eBIKER_SELL_PROVEN
//		CASE eBIKER_SELL_TRASHMASTER
			RETURN "SBCONTRA_WEEDB"	
			
		CASE eBIKER_SELL_BENSON
			RETURN "SB_BIKE" // "Bike"

		//get based on factory launched from
		CASE eBIKER_SELL_DEFAULT
		CASE eBIKER_SELL_BORDER_PATROL
		CASE eBIKER_SELL_HELICOPTER_DROP
		CASE eBIKER_SELL_AIR_DROP_AT_SEA
//		CASE eBIKER_SELL_TRASHMASTER
		CASE eBIKER_SELL_POSTMAN
		
			SWITCH serverBD.factoryType
				CASE FACTORY_TYPE_CRACK			RETURN "SBCONTRA_COKEB"
				CASE FACTORY_TYPE_FAKE_IDS		RETURN "SBCONTRA_IDSB"
				CASE FACTORY_TYPE_FAKE_MONEY	RETURN "SBCONTRA_MONEYB"	
				CASE FACTORY_TYPE_METH			RETURN "SBCONTRA_METHB"
				CASE FACTORY_TYPE_WEED			RETURN "SBCONTRA_WEEDB"	
			ENDSWITCH
		BREAK
	ENDSWITCH

	//product
	RETURN "SBCONTRA_DEFB"
ENDFUNC

FUNC INT GET_NUM_SOLD_FOR_SHARD()

	IF BIKER_SELL_DEFAULT()
		RETURN serverBD.iVehicleCountDeliveredAllContraband
	ENDIF
	
	RETURN iDROPPED_OFF_COUNT()
ENDFUNC

FUNC INT GET_TOTAL_FOR_SHARD()

	IF BIKER_SELL_DEFAULT()
		RETURN GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE()
	ENDIF
	
	IF MODE_HAS_PICKUP_BAGS()
		RETURN (iDROP_TARGET() + serverBD.iDropPenalty)
	ENDIF
	
	RETURN iORIGINAL_DROP_TARGET()
ENDFUNC

FUNC BOOL HAVE_ALL_DELIVERIES_BEEN_MADE()

	IF MODE_HAS_PICKUP_BAGS()
		IF serverBD.iDropPenalty > 0
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN (GET_NUM_SOLD_FOR_SHARD() = GET_TOTAL_FOR_SHARD())
ENDFUNC

FUNC BOOL DOES_VARIATION_ONLY_HAVE_ONE_DROP()
	IF BIKER_SELL_DEFAULT()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING sVEH_TYPE()

	STRING sReturn
	
	SWITCH GET_SELL_VAR()
		CASE eBIKER_SELL_TRASHMASTER 
		
			sReturn = "DBR_OBJTRK"
		BREAK
		
		CASE eBIKER_SELL_POSTMAN
//		CASE eBIKER_SELL_TACO_VAN
		
			sReturn = "DBR_OBJVAN"
		BREAK
		
		CASE eBIKER_SELL_PROVEN
			IF serverBD.eShipmentType = eBIKER_SHIPMENT_TYPE_SMALL
				sReturn = "DBR_OBJVAN"
			ELIF serverBD.eShipmentType = eBIKER_SHIPMENT_TYPE_LARGE
				sReturn = "DBR_OBJTRK"
			ELSE serverBD.eShipmentType = eBIKER_SHIPMENT_TYPE_MEDIUM
				sReturn = "DBR_OBJVAN"
			ENDIF
			
//			sReturn = "DBR_OBJVAN"
		BREAK
		
//		CASE eBIKER_SELL_FRIENDS_IN_NEED
//		CASE eBIKER_SELL_PROVEN
//		
//		
//		BREAK
	ENDSWITCH
	
	RETURN sReturn
ENDFUNC

FUNC STRING GET_STRAPLINE_FOR_SHIPMENT_TYPE()
	SWITCH serverBD.factoryType
		CASE FACTORY_TYPE_FAKE_IDS			RETURN "SB_SELL_BM_05a"
		CASE FACTORY_TYPE_WEED				RETURN "SB_SELL_BM_05b"
		CASE FACTORY_TYPE_CRACK				RETURN "SB_SELL_BM_05c"
		CASE FACTORY_TYPE_METH				RETURN "SB_SELL_BM_05d"
		CASE FACTORY_TYPE_FAKE_MONEY		RETURN "SB_SELL_BM_05e"
	ENDSWITCH
	
	RETURN "SB_SELL_BM_05f"
ENDFUNC

PROC DO_BIG_MESSAGE( BIG_MESSAGE_ENUM thisMessage)

	STRING sMessageTitle, sStrapline
	BIG_MESSAGE_TYPE bmType
//	BOOL bPlayerMessage
	
	IF IS_SAFE_FOR_BIG_MESSAGE()
	OR ( thisMessage = eBIGM_GANG_FAIL )// AND GET_END_REASON() <> eENDREASON_NO_BOSS_LEFT ) // Show shard after death shard in these instances (except for VIP left)
	OR ( thisMessage = eBIGM_GANG_SUCCESS )
		IF NOT IS_LOCAL_BIGMESSAGE_BIT0_SET(ENUM_TO_INT(thisMessage))
			SWITCH thisMessage
			
				// Gang Messages
			
				CASE eBIGM_GANG_START
				
					bmType = BIG_MESSAGE_GB_START_OF_JOB
					
					sMessageTitle = "SB_SELL_BM_01"
					
					sStrapline = "SB_SELL_BMOPN"
					
					SETUP_NEW_BIG_MESSAGE(bmType, sMessageTitle, sStrapline)
					
					SET_LOCAL_BIGMESSAGE_BIT0(ENUM_TO_INT(thisMessage))
				BREAK
				
				CASE eBIGM_GANG_SUCCESS
					sMessageTitle = "EXEC_SOLD"
					bmType = BIG_MESSAGE_GB_END_OF_JOB_SUCCESS
					
					IF HAVE_ALL_DELIVERIES_BEEN_MADE()
					OR DOES_VARIATION_ONLY_HAVE_ONE_DROP()
						sStrapline = "SB_SELL_BM_03"
						SETUP_NEW_BIG_MESSAGE_WITH_STRING(bmType, GET_SHIPMENT_TYPE_STRING(), sStrapline, sMessageTitle)
					ELSE
						sStrapline = GET_STRAPLINE_FOR_SHIPMENT_TYPE()
						SETUP_NEW_BIG_MESSAGE_WITH_2_INTS(bmType, GET_NUM_SOLD_FOR_SHARD(), GET_TOTAL_FOR_SHARD(), sStrapline, DEFAULT, DEFAULT, DEFAULT, sMessageTitle)
					ENDIF
					
					SET_LOCAL_BIGMESSAGE_BIT0(ENUM_TO_INT(thisMessage))
				BREAK
				
				CASE eBIGM_GANG_FAIL
				
					sMessageTitle = "BIKER_UNSOLD"
					
					bmType = BIG_MESSAGE_GB_END_OF_JOB_FAIL
				
					sStrapline = "SB_SELL_BM_04"
						
					SETUP_NEW_BIG_MESSAGE_WITH_STRING(bmType, GET_SHIPMENT_TYPE_STRING(), sStrapline, sMessageTitle)
					
					SET_LOCAL_BIGMESSAGE_BIT0(ENUM_TO_INT(thisMessage))
				BREAK
				
			ENDSWITCH
			
			#IF IS_DEBUG_BUILD	
			PRINT_BIG_MESSAGE_DEBUG_INFORMATION(ENUM_TO_INT(thisMessage), sMessageTitle, sStrapline) 
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_RESTRICT_MAX_WANTED_LEVEL()
	SWITCH GET_SELL_DIFFICULTY()
		CASE eDIFFICULTY_EASY
			RETURN g_sMPTunables.iBIKER_SELL_EASY_WANTED_CAP < 5 
		CASE eDIFFICULTY_MEDIUM
			RETURN g_sMPTunables.iBIKER_SELL_MEDIUM_WANTED_CAP < 5 
	ENDSWITCH
	
	RETURN g_sMPTunables.iBIKER_SELL_HARD_WANTED_CAP < 5
ENDFUNC

FUNC INT GET_MAX_WANTED_LEVEL_FOR_SELL_CONTRABAND()

	IF BIKER_SELL_PROVEN()
		RETURN g_sMPTunables.iBIKER_SELL_EASY_WANTED_CAP
	ENDIF

	IF SHOULD_RESTRICT_MAX_WANTED_LEVEL()
		SWITCH GET_SELL_DIFFICULTY()
			CASE eDIFFICULTY_EASY
				RETURN g_sMPTunables.iBIKER_SELL_EASY_WANTED_CAP
			CASE eDIFFICULTY_MEDIUM
				RETURN g_sMPTunables.iBIKER_SELL_MEDIUM_WANTED_CAP
		ENDSWITCH
	ENDIF
	
	RETURN g_sMPTunables.iBIKER_SELL_HARD_WANTED_CAP
ENDFUNC

PROC MAINTAIN_RESTRICT_MAX_WANTED_LEVEL()
	IF BIKER_SELL_AIR_DROP_AT_SEA()
	OR BIKER_SELL_STING()
	OR BIKER_SELL_PROVEN()
		IF SHOULD_RESTRICT_MAX_WANTED_LEVEL()
			IF GET_MAX_WANTED_LEVEL() > GET_MAX_WANTED_LEVEL_FOR_SELL_CONTRABAND()
				SET_MAX_WANTED_LEVEL(GET_MAX_WANTED_LEVEL_FOR_SELL_CONTRABAND())
				PRINTLN("[BIKER_SELL] [MAINTAIN_RESTRICT_MAX_WANTED_LEVEL] SET_MAX_WANTED_LEVEL ")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DO_EASY_HELP_TEXT()
	RETURN ( GET_SELL_DIFFICULTY() = eDIFFICULTY_EASY )
//	RETURN (GET_BUYER() = 2) OR (GET_BUYER() = 3) OR (GET_CONTRABAND_UNITS_TO_SELL_FROM_BUYER() = 1) 
ENDFUNC

ENUM eCTRA_REMOVAL
	eREMOVE_INITIAL,
	eREMOVE_LOSS,
	eREMOVE_WIN,
	eREMOVE_EMPTY
ENDENUM
#IF IS_DEBUG_BUILD
FUNC STRING GET_eCTRA_REMOVAL_DEBUG_PRINT(eCTRA_REMOVAL eRemove)
	SWITCH eRemove
		CASE eREMOVE_INITIAL	RETURN "eREMOVE_INITIAL"
		CASE eREMOVE_LOSS		RETURN "eREMOVE_LOSS"
		CASE eREMOVE_WIN		RETURN "eREMOVE_WIN"
		CASE eREMOVE_EMPTY		RETURN "eREMOVE_EMPTY"
	ENDSWITCH
	RETURN ""
ENDFUNC
	#ENDIF


//PROC HANDLE_CONTRABAND_REMOVAL(eCTRA_REMOVAL eRemove)
//	// Dont do any of this if we've removed cash at the end
//	IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_CONTRABAND_REMOVED_AT_END)
//		IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_BIKER_GANG()
//			INT iContrabandToRemove
//			INT iEarnings = 0
////			BOOL bSpecialItemsOnly = FALSE
//			REMOVE_CONTRABAND_REASON_ENUM eReason
//			
//			// [NOTE FOR BOBBY] - we now only sell whatever Mark/Martin tell us to sell, not all of the contraband in the warehouse
//			
//			INT iTotalContraband = GET_CONTRABAND_UNITS_TO_SELL_FROM_BUYER() 
//			
//			FLOAT fNumVehicles = TO_FLOAT( GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() )
//			
//			FLOAT fDroppedOffAmount, fDropTarget, fTotalContraband
//			
//			PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, eRemove = ", GET_eCTRA_REMOVAL_DEBUG_PRINT(eRemove), " - iTotalContraband = ", iTotalContraband, " - serverBD.iStartingWarehouse = ", serverBD.iStartingWarehouse)
//			
//			SWITCH eRemove
//				CASE eREMOVE_INITIAL
//					IF iTotalContraband > 1
//						iContrabandToRemove = GET_INITIAL_CONTRABAND_QUANTITY_TO_REMOVE_FROM_SHIPMENT_TYPE()
//						iInitialContrabandRemoved = iContrabandToRemove
//						eReason = REMOVE_CONTRA_MISSION_STARTED
//						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, eREMOVE_INITIAL = ", iContrabandToRemove)
//					ELSE
//						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, eREMOVE_INITIAL - iTotalContraband = ", iTotalContraband, "; not removing any")
//					ENDIF
//				BREAK
//				CASE eREMOVE_LOSS	 
//				
//					IF GET_END_REASON() = eENDREASON_TRUCK_DESTROYED
//						
//						fTotalContraband 	= TO_FLOAT(iTotalContraband)
//						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, serverBD.iFailVehicle = ", serverBD.iFailVehicle)
//						IF serverBD.iFailVehicle != 0
//							IF IS_BIT_SET(serverBD.iFailVehicle, 0)
//								fDroppedOffAmount 	+= TO_FLOAT(serverBD.iVehDropCount[0])
//								PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, fDroppedOffAmount 	+= TO_FLOAT(serverBD.iVehDropCount[0]) = ", fDroppedOffAmount - serverBD.iVehDropCount[0], " + ", serverBD.iVehDropCount[0], " = ", fDroppedOffAmount)
//							ENDIF
//							IF IS_BIT_SET(serverBD.iFailVehicle, 1)
//								fDroppedOffAmount 	= TO_FLOAT(serverBD.iVehDropCount[1])
//								PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, fDroppedOffAmount 	+= TO_FLOAT(serverBD.iVehDropCount[1]) = ", fDroppedOffAmount - serverBD.iVehDropCount[1], " + ", serverBD.iVehDropCount[1], " = ", fDroppedOffAmount)
//							ENDIF
//							IF IS_BIT_SET(serverBD.iFailVehicle, 2)
//								fDroppedOffAmount 	= TO_FLOAT(serverBD.iVehDropCount[2])
//								PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, fDroppedOffAmount 	+= TO_FLOAT(serverBD.iVehDropCount[2]) = ", fDroppedOffAmount - serverBD.iVehDropCount[2], " + ", serverBD.iVehDropCount[2], " = ", fDroppedOffAmount)
//							ENDIF
//							IF IS_BIT_SET(serverBD.iFailVehicle, 3)
//								fDroppedOffAmount 	= TO_FLOAT(serverBD.iVehDropCount[3])
//								PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, fDroppedOffAmount 	+= TO_FLOAT(serverBD.iVehDropCount[3]) = ", fDroppedOffAmount - serverBD.iVehDropCount[3], " + ", serverBD.iVehDropCount[3], " = ", fDroppedOffAmount)
//							ENDIF
//						ELSE
//							fDroppedOffAmount 	= TO_FLOAT(serverBD.iDroppedOffCount)
//							PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, fDroppedOffAmount 	= TO_FLOAT(serverBD.iDroppedOffCount) = ", fDroppedOffAmount - serverBD.iDroppedOffCount, " + ", serverBD.iDroppedOffCount, " = ", fDroppedOffAmount)
//						ENDIF
//						fDropTarget			= TO_FLOAT(iDROP_TARGET())
//						
//						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, fTotalContraband = ", fTotalContraband, " fDroppedOffAmount = ", fDroppedOffAmount, " fDropTarget = ", fDropTarget)
//			
//						IF DOES_BIKER_SELL_MULTIPLE_DROP_OFFS()
//						OR BIKER_SELL_STING()
//						OR IS_THIS_AN_AIR_VARIATION()
//							iContrabandToRemove =  FLOOR( ( fTotalContraband / fNumVehicles ) * ( 1 - ( fDroppedOffAmount / fDropTarget ) ) )
//							PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL - iContrabandToRemove =  FLOOR( ( fTotalContraband / fNumVehicles ) * ( 1 - ( fDroppedOffAmount / fDropTarget ) ) ) = iContrabandToRemove = ", iContrabandToRemove)
//							PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL - ", iContrabandToRemove, " =  FLOOR( ( ", fTotalContraband, " / ", fNumVehicles, " ) * ( 1 - ( ", fDroppedOffAmount, " / ", fDropTarget, " ) ) ) = ", iContrabandToRemove)
//						ELSE
//							iContrabandToRemove =  FLOOR(fTotalContraband / fNumVehicles )
//							PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL - iContrabandToRemove =  FLOOR(fTotalContraband / fNumVehicles ) = iContrabandToRemove = ", iContrabandToRemove)
//							PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL - ", iContrabandToRemove, " =  FLOOR(", fTotalContraband, " / ", fNumVehicles, " ) = ",iContrabandToRemove)
//						ENDIF
//						
//						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, eENDREASON_TRUCK_DESTROYED, fNumVehicles = ", fNumVehicles, " iContrabandToRemove = ", iContrabandToRemove)
//					
//						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, FLOOR(TO_FLOAT(iContrabandToRemove) * g_sMPTunables.fexec_stock_remove_at_fail_sell) = ", FLOOR(TO_FLOAT(iContrabandToRemove) * g_sMPTunables.fexec_stock_remove_at_fail_sell))
//						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, FLOOR(TO_FLOAT(", iContrabandToRemove, ") * ", g_sMPTunables.fexec_stock_remove_at_fail_sell, ") = ", FLOOR(TO_FLOAT(iContrabandToRemove) * g_sMPTunables.fexec_stock_remove_at_fail_sell))
//						iContrabandToRemove =  FLOOR(TO_FLOAT(iContrabandToRemove) * g_sMPTunables.fexec_stock_remove_at_fail_sell)
//		
//						//Now remove the amount we've already removed, if there's any to remove
//						iContrabandToRemove -= iInitialContrabandRemoved
//						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, , iContrabandToRemove,  -= , iInitialContrabandRemoved,  = , iContrabandToRemove")
//						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, ", iContrabandToRemove + iInitialContrabandRemoved, " -= ", iInitialContrabandRemoved, " = ", iContrabandToRemove)
//						
//						eReason = REMOVE_CONTRA_MISSION_FAILED
//						
//						IF iContrabandToRemove < g_sMPTunables.iEXEC_CONTRABAND_MINIMUM_LOST
//							iContrabandToRemove = g_sMPTunables.iEXEC_CONTRABAND_MINIMUM_LOST
//						ENDIF
//						
////						iNumCratesForShard = iContrabandToRemove + iInitialContrabandRemoved
//						
////						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, iNumCratesForShard = ", iNumCratesForShard)
//						
//						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, eREMOVE_LOSS = ", iContrabandToRemove, " iEXEC_CONTRABAND_MINIMUM_LOS = ", g_sMPTunables.iEXEC_CONTRABAND_MINIMUM_LOST)
//					#IF IS_DEBUG_BUILD
//					ELSE
//						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, GET_END_REASON() != eENDREASON_TRUCK_DESTROYED")
//						#ENDIF
//					ENDIF
//				BREAK
//				CASE eREMOVE_WIN	 
//					iContrabandToRemove = (iTotalContraband - iInitialContrabandRemoved)
//					iEarnings = MPGlobalsAmbience.sMagnateGangBossData.iContrabandSellEarnings
//					eReason = REMOVE_CONTRA_MISSION_PASSED
//					PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, eREMOVE_WIN = (iTotalContraband - iInitialContrabandRemoved) = ", iTotalContraband," - ", iInitialContrabandRemoved, " =  ", iContrabandToRemove)
//				BREAK
//			ENDSWITCH
//			
//			//Cap it
//			IF iContrabandToRemove > GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(serverbd.iStartingWarehouse)
//				PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, iContrabandToRemove < 1 - iContrabandToRemove = ", GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(serverbd.iStartingWarehouse))
//				iContrabandToRemove = GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(serverbd.iStartingWarehouse)
//			ENDIF
//			
//			//If it will go nexitave then set it to the total, i.e. clear it all. 
//			IF iTotalContraband - iContrabandToRemove <= 0
//				iContrabandToRemove = iTotalContraband
//				PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, iContrabandToRemove = iTotalContraband = ", iContrabandToRemove)
//			ENDIF
//		
//			IF iContrabandToRemove > 0
//				PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, REMOVE_CONTRABAND_FROM_WAREHOUSE(", serverBD.iStartingWarehouse, ", ", iContrabandToRemove, ") - iContrabandToRemove = ", iContrabandToRemove)
//				PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, GET_DOES_BUYER_ONLY_WANT_SPECIAL_ITEMS() = ", GET_DOES_BUYER_ONLY_WANT_SPECIAL_ITEMS())
//			ELSE
//				PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, iContrabandToRemove <= 0")
//				ASSERTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, iContrabandToRemove <= 0")
//				MPGlobalsAmbience.sMagnateGangBossData.iContrabandSellEarnings = 0
//				SET_CLIENT_BIT0(eCLIENTBITSET0_CONTRABAND_ADDED_AT_END)
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

BOOL bIWon = FALSE

FUNC INT GET_PRODUCT_REMOVED_AT_START()
	IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_BIKER_GANG()
		RETURN playerBD[PARTICIPANT_ID_TO_INT()].iProductRemovedAtStart
	ELSE
		IF NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS()) != -1
			PARTICIPANT_INDEX bossPartId = NETWORK_GET_PARTICIPANT_INDEX(GB_GET_LOCAL_PLAYER_GANG_BOSS())
			IF NATIVE_TO_INT(bossPartId) != -1
				RETURN playerBD[NATIVE_TO_INT(bossPartId)].iProductRemovedAtStart
			ENDIF
		ENDIF
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC INT GET_PRODUCT_LEFT_IN_FACTORY()
	IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_BIKER_GANG()
		RETURN playerBD[PARTICIPANT_ID_TO_INT()].iProductLeftInFactory
	ELSE
		IF NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS()) != -1
			PARTICIPANT_INDEX bossPartId = NETWORK_GET_PARTICIPANT_INDEX(GB_GET_LOCAL_PLAYER_GANG_BOSS())
			IF NATIVE_TO_INT(bossPartId) != -1
				RETURN playerBD[NATIVE_TO_INT(bossPartId)].iProductLeftInFactory
			ENDIF
		ENDIF
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC MP_DAILY_OBJECTIVES GET_DAILY_OBJECTIVE_FOR_BIKER_DELIVERY()

	SWITCH serverBD.factoryType
		CASE FACTORY_TYPE_CRACK				RETURN MP_DAILY_AM_SELL_COCAINE
		CASE FACTORY_TYPE_WEED				RETURN MP_DAILY_AM_SELL_WEED
		CASE FACTORY_TYPE_FAKE_MONEY		RETURN MP_DAILY_AM_SELL_CASH
		CASE FACTORY_TYPE_METH				RETURN MP_DAILY_AM_SELL_METH
		CASE FACTORY_TYPE_FAKE_IDS			RETURN MP_DAILY_AM_SELL_DOCUMENTS
	ENDSWITCH
	
	RETURN NULL_OBJECTIVE

ENDFUNC

PROC HANDLE_REWARDS()
	
	IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALCULATED_REWARDS)
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF GET_END_REASON() != eENDREASON_NO_REASON_YET
			
				PLAYER_INDEX playerId = PLAYER_ID() // Set the winning player here before passing into wanted cleanup function. Using PLAYER_ID() as placeholder right now.
				
				IF GET_END_REASON() = eENDREASON_WIN_CONDITION_TRIGGERED
					bIWon = TRUE
					IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_BIKER_GANG()
						sGangBossManageRewardsData.iContrabandUnitsSold = GET_CONTRABAND_UNITS_TO_SELL_FROM_BUYER()

						IF BIKER_SELL_BORDER_PATROL()
							IF (serverBD.iBonus[0] + serverBD.iBonus[1] + serverBD.iBonus[2]) > 0
								sGangBossManageRewardsData.bDelayExtraCashReward = TRUE
								sGangBossManageRewardsData.iExtraCash = (serverBD.iBonus[0] + serverBD.iBonus[1] + serverBD.iBonus[2])
							ENDIF
						ENDIF

						IF GET_BIKER_SELL_MISSION_COMPLETE_TOTAL(GET_ACTIVE_CHARACTER_SLOT()) >= 3
						AND NOT DOES_PLAYER_OWN_FACTORY_UPGRADE(PLAYER_ID(), serverBD.factoryId, UPGRADE_ID_STAFF) 
							GB_SET_LOCAL_BIKER_CALL_BIT(eGB_LOCAL_BIKER_CALL_BITSET_NOTIFICATION_NO_STAFF)
							CPRINTLN(DEBUG_NET_MAGNATE, " GB_SET_LOCAL_BIKER_CALL_BIT(eGB_LOCAL_BIKER_CALL_BITSET_NOTIFICATION_NO_STAFF)") 
						ENDIF
					ENDIF
				ENDIF
						
				sGangBossManageRewardsData.eBikerVariation = GET_SELL_VAR()
				sGangBossManageRewardsData.iBuyerID = serverBD.iBuyerID
				sGangBossManageRewardsData.iNumDropsMade = GET_NUM_SOLD_FOR_SHARD()
				sGangBossManageRewardsData.iTotalDrops = GET_TOTAL_FOR_SHARD()
				IF GET_PRODUCT_REMOVED_AT_START() != -1
					sGangBossManageRewardsData.iAdditionalContrabandUnitsForRewards = GET_PRODUCT_REMOVED_AT_START()
					sGangBossManageRewardsData.iProductInFactoryAfterInitialRemoval = GET_PRODUCT_LEFT_IN_FACTORY()
				ENDIF
				IF BIKER_SELL_RACE()
					IF serverBD.sRaceServerVars.iWinnerPart[0] = NATIVE_TO_INT(PARTICIPANT_ID())
						sGangBossManageRewardsData.bDelayExtraCashReward = TRUE
						sGangBossManageRewardsData.iExtraCash = g_sMPTunables.iBiker_Sell_Race_Bonus_Cash_Reward_Per_Member * (GB_GET_NUM_GOONS_IN_LOCAL_GANG() + 1)
						PRINTLN("[BIKER_SELL] HANDLE_REWARDS - GANG_BOSS_MANAGE_REWARDS - I won the race, ", (GB_GET_NUM_GOONS_IN_LOCAL_GANG() + 1)," people in my gang, I should receive an extra $", sGangBossManageRewardsData.iExtraCash)
					ENDIF
				ENDIF
				GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_BIKER_CONTRABAND_SELL, bIWon, sGangBossManageRewardsData)
				
				IF bIWon
					// Do any extra processing here for if the locla player won, before passing into GANG_BOSS_MANAGE_REWARDS
					// sGangBossManageRewardsData etc.
					GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, playerId)
					//[GB_CONT_TICK_S] ~a~ ~s~sold a contraband shipment.
					SCRIPT_EVENT_DATA_TICKER_MESSAGE sData
					sData.TickerEvent = TICKER_EVENT_CONTRABAND_SELL_SOLD_BIKER
					sData.playerID = playerId
					sData.dataInt = ENUM_TO_INT(serverBD.factoryType)
					IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_BIKER_GANG()
						BROADCAST_TICKER_EVENT(sData, ALL_PLAYERS(FALSE))
					ENDIF
					IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_BIKER_GANG()
						INCREMENT_BIKER_SELL_MISSION_COMPLETED_COUNT(serverBD.factoryId, serverBD.eSellLocality)
						ADD_TO_BIKER_MISSION_EARNINGS(serverBD.factoryId, g_i_cashForEndEventShard)
					ENDIF
				ELSE
					GB_START_CONTRABAND_SELL_DELAY_TIMER()
					GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, INVALID_PLAYER_INDEX())
					
					IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_BIKER_GANG()
					AND sGangBossManageRewardsData.iNumDropsMade > 0
						FINISH_FACTORY_PRODUCTION_BOOST(serverBD.factoryId)
					ENDIF
				ENDIF
					
				EXECUTE_COMMON_END_TELEMTRY(bIWon, FALSE)
				
				SET_LOCAL_BIT0(eLOCALBITSET0_CALCULATED_REWARDS)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_CONTRABAND_REMOVED_AT_END)
			IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_BIKER_GANG()
				IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_CONTRABAND_ADDED_AT_END)
				
					REMOVE_CONTRABAND_REASON_ENUM eReason
				
					IF bIWon
					OR iAMOUNT_DROPPED_OFF_FOR_HUD() > 0 // url:bugstar:3102356 - Sell - Friends in Need - Transaction Error at end with 3 players, small shipment
						eReason = REMOVE_CONTRA_MISSION_PASSED
						PRINTLN("[CTRB_SELL] HANDLE_REWARDS GET_END_REASON() = REMOVE_CONTRA_MISSION_PASSED")
					ELIF GET_END_REASON() = eENDREASON_TRUCK_DESTROYED
						PRINTLN("[CTRB_SELL] HANDLE_REWARDS GET_END_REASON() = eENDREASON_TRUCK_DESTROYED")
						eReason = REMOVE_CONTRA_MISSION_FAILED
					//We failed and not because a truck was destroyed
					ELSE
						PRINTLN("[CTRB_SELL] HANDLE_REWARDS GET_END_REASON() != eENDREASON_TRUCK_DESTROYED")
						eReason = REMOVE_CONTRA_MISSION_FAILED
					ENDIF
					
					ILLICIT_GOODS_MISSION_DATA eData = GB_GET_PLAYER_ILLICIT_GOOD_MISSION_DATA()
					REMOVE_PRODUCT_FROM_FACTORY(eData.eMissionFactory, GET_PRODUCT_TOTAL_FOR_FACTORY(PLAYER_ID(), eData.eMissionFactory), g_i_cashForEndEventShard, TRUE, eReason, eResult)
					PRINTLN("[BIKER_SELL] HANDLE_REWARDS - REMOVE_PRODUCT_FROM_FACTORY: ", eData.eMissionFactory)
					
					IF eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
						PRINTLN("[BIKER_SELL] - HANDLE_REWARDS - Transaction success!")
						MPGlobalsAmbience.sMagnateGangBossData.iContrabandSellEarnings = 0
						SET_CLIENT_BIT0(eCLIENTBITSET0_CONTRABAND_ADDED_AT_END)
						IF eReason = REMOVE_CONTRA_MISSION_PASSED
							SET_MP_DAILY_OBJECTIVE_COMPLETE(GET_DAILY_OBJECTIVE_FOR_BIKER_DELIVERY())
						ENDIF
					ENDIF
					
					IF eResult = CONTRABAND_TRANSACTION_STATE_FAILED
						PRINTLN("[BIKER_SELL] - HANDLE_REWARDS - Transaction failed!")
						MPGlobalsAmbience.sMagnateGangBossData.iContrabandSellEarnings = 0
						SET_CLIENT_BIT0(eCLIENTBITSET0_CONTRABAND_ADDED_AT_END)
					ENDIF
				ENDIF					
			ELSE
				IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = INVALID_PLAYER_INDEX()
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						PRINTLN("[BIKER_SELL] HANDLE_REWARDS - Boss has quit the session, setting eSERVERBITSET1_CONTRABAND_REMOVED_AT_END as the boss is unable to")
						SET_SERVER_BIT1(eSERVERBITSET1_CONTRABAND_REMOVED_AT_END)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
				
			IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			AND NOT GB_SHOULD_HIDE_GANG_BOSS_EVENT(FMMC_TYPE_BIKER_CONTRABAND_SELL)
				IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_DONE_END_SHARD)
//					IF IS_SAFE_FOR_BIG_MESSAGE()
					
						SWITCH GET_END_REASON()
							CASE eENDREASON_NO_BOSS_LEFT
								PRINTLN("[BIKER_SELL] HANDLE_REWARDS - no boss left on script.")
								CLEAR_HELP()
//										DO_BIG_MESSAGE( eBIGM_GANG_FAIL)
								// Display end shard here.
							BREAK
							CASE eENDREASON_ALL_GOONS_LEFT
								PRINTLN("[BIKER_SELL] HANDLE_REWARDS - no goons left on script")
								CLEAR_HELP()
								DO_BIG_MESSAGE( eBIGM_GANG_FAIL)
								// Display end shard here.
							BREAK
							CASE eENDREASON_TIME_UP
								PRINTLN("[BIKER_SELL] HANDLE_REWARDS - mode time expired")
								CLEAR_HELP()
								DO_BIG_MESSAGE( eBIGM_GANG_FAIL)
								// Display end shard here
							BREAK
							CASE eENDREASON_TRUCK_DESTROYED
							CASE eENDREASON_VEHICLE_STUCK
								PRINTLN("[BIKER_SELL] HANDLE_REWARDS - eENDREASON_TRUCK_DESTROYED ")
								CLEAR_HELP()
								DO_BIG_MESSAGE( eBIGM_GANG_FAIL)
								// Display end shard here
							BREAK
							CASE eENDREASON_PED_KILLED
								PRINTLN("[BIKER_SELL] HANDLE_REWARDS - eENDREASON_PED_KILLED ")
								CLEAR_HELP()
								DO_BIG_MESSAGE( eBIGM_GANG_FAIL)
								// Display end shard here
							BREAK
							CASE eENDREASON_WIN_CONDITION_TRIGGERED
								PRINTLN("[BIKER_SELL] HANDLE_REWARDS - somebody won")
								CLEAR_HELP()
								DO_BIG_MESSAGE( eBIGM_GANG_SUCCESS)
								// Display end shard here
								// IF local player won set bIWon = TRUE here.
							BREAK
						ENDSWITCH
					
						SET_LOCAL_BIT0(eLOCALBITSET0_DONE_END_SHARD)
//					ENDIF
				ENDIF
			ELSE
				PRINTLN("[BIKER_SELL] HANDLE_REWARDS - GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE ")
			ENDIF
		ELSE
			PRINTLN("[BIKER_SELL] HANDLE_REWARDS - GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI ")
		ENDIF
		
	ENDIF
	
	IF (GB_MAINTAIN_BOSS_END_UI(sEndUI, FALSE)
	AND IS_SERVER_BIT1_SET(eSERVERBITSET1_CONTRABAND_REMOVED_AT_END))
	
	#IF IS_DEBUG_BUILD
	OR DEBUG_SKIPPED_MISSION()
	#ENDIF
	
		SET_CLIENT_BIT0(eCLIENTBITSET0_COMPLETED_REWARDS)
	ENDIF
	
ENDPROC

PROC SET_COOLDOWN_TIMER(INT iWarehouse)

	IF CHECK_WAREHOUSE_ID(iWarehouse)
		IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_SET_COOLDOWN)
			IF GET_END_REASON() = eENDREASON_WIN_CONDITION_TRIGGERED	
				IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_BIKER_GANG()
					IF DOES_LOCAL_PLAYER_OWN_WAREHOUSE(iWarehouse)
						INT iWarehouseIndex = GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse) 
						MPGlobalsAmbience.sMagnateGangBossData.iTimeOfLastSuccessfulSellMission[iWarehouseIndex] = GET_CLOUD_TIME_AS_INT()
						SET_MP_INT_CHARACTER_STAT(GET_STAT_FOR_SELL_COOLDOWN(iWarehouseIndex),MPGlobalsAmbience.sMagnateGangBossData.iTimeOfLastSuccessfulSellMission[iWarehouseIndex])
						PRINTLN("[BIKER_SELL] [GB_LAUNCH] SET_COOLDOWN_TIMER - Set to ",MPGlobalsAmbience.sMagnateGangBossData.iTimeOfLastSuccessfulSellMission[iWarehouseIndex]," iWarehouse = ",iWarehouse, " index = ",iWarehouseIndex)
						SET_LOCAL_BIT0(eLOCALBITSET0_SET_COOLDOWN)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC VECTOR vLOCATE_DIMENSION(BOOL bPackage = FALSE)

	IF BIKER_SELL_FRIENDS_IN_NEED()
	
		RETURN <<cfSELL_FIN_SIZE, cfSELL_FIN_SIZE, cfSELL_FIN_SIZE>>
	ENDIF

	IF IS_THIS_AN_AIR_VARIATION()
		IF BIKER_SELL_HELICOPTER_DROP()
			IF bPackage
				RETURN <<(cfSELL_CRATE_LOC_SIZE_Z/3), (cfSELL_CRATE_LOC_SIZE_Z/3), cfSELL_CRATE_LOC_SIZE_Z*1.5 >>
			ENDIF
			RETURN <<(cfSELL_CRATE_LOC_SIZE_Z/4), (cfSELL_CRATE_LOC_SIZE_Z/4), cfSELL_CRATE_LOC_SIZE_Z*1.5 >>
		ENDIF
		RETURN <<(cfSELL_CRATE_LOC_SIZE_Z/2), (cfSELL_CRATE_LOC_SIZE_Z/2), cfSELL_CRATE_LOC_SIZE_Z*1.5 >>
	ELIF DOES_BIKER_SELL_MULTIPLE_DROP_OFFS() // See url:bugstar:2812650 - Mission completed although we were not in the visible destination corona
	AND NOT BIKER_SELL_POSTMAN()
		IF BIKER_SELL_BORDER_PATROL()
//			IF bPackage
//				RETURN <<cfSELL_STING_MULTIPLE_SIZE*1.0, cfSELL_STING_MULTIPLE_SIZE*2.0, cfSELL_LOCATE_SIZE * 2>>
//			ELSE
				RETURN <<cfSELL_STING_MULTIPLE_SIZE*1.2, cfSELL_STING_MULTIPLE_SIZE*1.2, cfSELL_LOCATE_SIZE * 2>>
				//RETURN <<cfSELL_LOCATE_SIZE*1.2, cfSELL_LOCATE_SIZE*1.2, cfSELL_LOCATE_SIZE>>
//			ENDIF
		ELSE		
			IF bPackage
				RETURN <<cfSELL_STING_MULTIPLE_SIZE*1.5, cfSELL_STING_MULTIPLE_SIZE*1.5, cfSELL_LOCATE_SIZE>>
			ELSE
				RETURN <<cfSELL_STING_MULTIPLE_SIZE*1.2, cfSELL_STING_MULTIPLE_SIZE*1.2, cfSELL_LOCATE_SIZE>>
				//RETURN <<cfSELL_LOCATE_SIZE*1.2, cfSELL_LOCATE_SIZE*1.2, cfSELL_LOCATE_SIZE>>
			ENDIF
		ENDIF
		
// url:bugstar:3000184 - Sell - Sting op - Final drop off corona has a large radius allowing players to deliver without entering airport. 
// Spoke to Butch, said to make it smaller (flow_play) - Steve
//	ELIF BIKER_SELL_STING() // 
//		RETURN <<cfSELL_STING_MULTIPLE_SIZE, cfSELL_STING_MULTIPLE_SIZE, cfSELL_LOCATE_SIZE>>
	ENDIF
	
	IF BIKER_SELL_BAG_DROP()
		RETURN <<cfSELL_LOCATE_SIZE, cfSELL_LOCATE_SIZE, 5.0>>
	ENDIF
	
	RETURN <<cfSELL_LOCATE_SIZE, cfSELL_LOCATE_SIZE, cfSELL_LOCATE_SIZE>>
ENDFUNC

FUNC FLOAT fGET_ALLOWABLE_HEIGHT()
	
	SWITCH GET_SELL_DIFFICULTY()
		CASE eDIFFICULTY_EASY
			RETURN g_sMPTunables.fBIKER_AIR_DROP_AT_SEA_EASY_HEIGHT
		CASE eDIFFICULTY_MEDIUM
			RETURN g_sMPTunables.fBIKER_AIR_DROP_AT_SEA_MEDIUM_HEIGHT
		CASE eDIFFICULTY_HARD
			RETURN g_sMPTunables.fBIKER_AIR_DROP_AT_SEA_HARD_HEIGHT
	ENDSWITCH
	
	RETURN g_sMPTunables.fBIKER_AIR_DROP_AT_SEA_HARD_HEIGHT
ENDFUNC

#IF IS_DEBUG_BUILD

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				SELL LOGIC														║

FUNC STRING DBG_GET_MISSION_STAGE_NAME(eCLIENT_MISSION_STAGE sStage)
	SWITCH sStage
		CASE eSELL_STAGE_INIT 
			RETURN "eSELL_STAGE_INIT"
		BREAK
		CASE eSELL_STAGE_VEH 
			RETURN "eSELL_STAGE_VEH"
		BREAK
		CASE eSELL_STAGE_SELLING
			RETURN "eSELL_STAGE_SELLING"
		BREAK
		CASE eSELL_STAGE_CLEANUP
			RETURN "eSELL_STAGE_CLEANUP"
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

#ENDIF // #IF IS_DEBUG_BUILD

FUNC eCLIENT_MISSION_STAGE GET_LOCAL_CLIENT_MISSION_STAGE()
	RETURN playerBD[PARTICIPANT_ID_TO_INT()].eStage		
ENDFUNC

PROC SET_LOCAL_CLIENT_MISSION_STAGE(eCLIENT_MISSION_STAGE eStage)
	PRINTNL()
	PRINTLN("[BIKER_SELL] Current_stage ", DBG_GET_MISSION_STAGE_NAME(playerBD[PARTICIPANT_ID_TO_INT()].eStage))
	
	playerBD[PARTICIPANT_ID_TO_INT()].eStage = eStage
	
	PRINTNL()
	PRINTLN("[BIKER_SELL] SET_LOCAL_CLIENT_MISSION_STAGE >>> ", DBG_GET_MISSION_STAGE_NAME(eStage))
	PRINTNL()
ENDPROC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				CONTRABAND / VEHICLE											║

FUNC ENTITY_INDEX VEH_ENT(INT iVeh)
	ENTITY_INDEX eiVeh 
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh[iVeh])
		eiVeh = NET_TO_ENT(serverBD.niVeh[iVeh])
	ENDIF
	
	RETURN eiVeh
ENDFUNC

FUNC VEHICLE_INDEX VEH_ID(INT iVeh)
	VEHICLE_INDEX vehId
	
	IF iVeh <> -1
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh[iVeh])
			vehId = NET_TO_VEH(serverBD.niVeh[iVeh])
		ENDIF
	ENDIF
	
	RETURN vehId
ENDFUNC

FUNC BOOL IS_SELL_VEH_OK(INT iVeh)
	IF DOES_ENTITY_EXIST(VEH_ENT(iVeh))
	AND NOT IS_ENTITY_DEAD(VEH_ENT(iVeh))
		IF IS_VEHICLE_DRIVEABLE(VEH_ID(iVeh))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_VECTOR_INFRONT_OF_COORDS_WITH_HEADING(VECTOR vStart, FLOAT fHeading, FLOAT fDistance = 2.0)

    VECTOR vEnd 
    
    //end vector - pointing north 
    vEnd    = <<0.0, 1.0, 0.0>> 
    
    //point it in the same direction as the entity
    ROTATE_VECTOR_ABOUT_Z(vEnd, fHeading ) 

    //Make the normilised roted vector 300 times larger 
	
    vEnd.x *= fDistance 
    vEnd.y *= fDistance 
    vEnd.z *= fDistance 
    
    //add it on to the start vector to get the end vector coordinates 
    vEnd += vStart 
    
    RETURN vEnd 

ENDFUNC

PROC INIT_IDEAL_SPAWN_COORDS()
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF IS_THIS_A_LAND_VARIATION()
			serverBD.vSellVehSpawnCoords[i] =  GET_LAND_SELL_VEHICLE_CREATION_POS(i, 0, serverBD.iStartingWarehouse, serverBD.eShipmentType)
			serverBD.fSellVehSpawnHeading[i] = GET_LAND_SELL_VEHICLE_CREATION_HEADING(i, 0, serverBD.iStartingWarehouse, serverBD.eShipmentType)
		ELIF IS_THIS_AN_AIR_VARIATION()
			serverBD.vSellVehSpawnCoords[i] =  GET_AIR_SELL_VEHICLE_CREATION_POS(i)
			serverBD.fSellVehSpawnHeading[i] = GET_AIR_SELL_VEHICLE_CREATION_HEADING(i)
		ENDIF
		
		PRINTLN("[BIKER_SELL] CREATE_SELL_VEH preferred serverBD.vSellVehSpawnCoords[",i, "] = ", serverBD.vSellVehSpawnCoords[i]) 
		PRINTLN("[BIKER_SELL] CREATE_SELL_VEH preferred serverBD.fSellVehSpawnHeading[",i, "] = ", serverBD.fSellVehSpawnHeading[i])
		
		IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(serverBD.vSellVehSpawnCoords[i],4,1,1,1,FALSE,FALSE,FALSE)
			PRINTLN("[BIKER_SELL] CREATE_SELL_VEH VEH [", i, "] point not ok will try clear area")
			CLEAR_AREA(serverBD.vSellVehSpawnCoords[i], 4.0, TRUE, DEFAULT, DEFAULT, TRUE)
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL USE_CONVOY_SPAWNS()
	IF BIKER_SELL_DEFAULT()
	OR BIKER_SELL_PROVEN()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_IDEAL_SPAWN_POS_FOR_SELL_VEHICLE(INT iVeh, VECTOR &vIdeal, FLOAT &fIdeal)
	
	

	IF IS_THIS_A_LAND_VARIATION()
		//-- Before creating Land sell vehicles, try to figure out if any of the 3 groups of possible spawn locations for the warehouse are actually going to be valid
		//-- Do this by doing one iteration of HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS and checking the result
		//-- If any of the spawn locations looks like it'll be valid, go with that one. 
		//-- Otherwise move onto the next group of spawn coords and try those instead
		//-- If none of them look like they'll work, fallback to the first one and let the closest car node routines in HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS
		//-- figure out a location
	
		IF iVeh = 0																		
		OR (iVeh > 0 AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh - 1])) //-- 1 vehicle at a time
		
			IF NOT IS_VECTOR_ZERO(vIdeal)
				RETURN TRUE
			ENDIF
		
			VEHICLE_SPAWN_LOCATION_PARAMS Params
			Params.fMinDistFromCoords = 0.0 
			Params.bConsiderHighways= FALSE
			Params.fMaxDistance= 200.0
			Params.bAvoidSpawningInExclusionZones= TRUE
			Params.bConsiderOnlyActiveNodes= FALSE
			Params.bCheckEntityArea = TRUE
			Params.bUseExactCoordsIfPossible = TRUE
			VECTOR vSpawnHeading
			VECTOR vLocToTry = GET_LAND_SELL_VEHICLE_CREATION_POS(iVeh, serverBD.iFindIdealSpawnCoordProg, serverBD.iStartingWarehouse, serverBD.eShipmentType, USE_CONVOY_SPAWNS())
			FLOAT fHeading = GET_LAND_SELL_VEHICLE_CREATION_HEADING(iVeh, serverBD.iFindIdealSpawnCoordProg, serverBD.iStartingWarehouse, serverBD.eShipmentType, USE_CONVOY_SPAWNS())
			
			vSpawnHeading = GET_VECTOR_INFRONT_OF_COORDS_WITH_HEADING(vLocToTry, fHeading, 20.0)
			IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vLocToTry, 
															vSpawnHeading, 
															GET_SELL_VEH_MODEL(), 
															FALSE, 
															vLocToTry, 
															fHeading, 
															Params)
				vIdeal = vLocToTry
				fIdeal = fHeading
				
				serverBD.vSellVehSpawnCoords[iVeh] = vIdeal
				serverBD.fSellVehSpawnHeading[iVeh] = fIdeal
				
				PRINTLN("[BIKER_SELL] [CREATE_SELL_VEH] [GET_IDEAL_SPAWN_POS_FOR_SELL_VEHICLE] Got ideal coords for veh ", iVeh, " - using spawn group ", serverBD.iFindIdealSpawnCoordProg, " Ideal pos = ", vIdeal, " Heading = ", fIdeal, " Time - ",GET_CLOUD_TIME_AS_INT())  
				RETURN TRUE
			ELSE
				BOOL bResetSpawning
				
				//-- Reset the spawning data if there's been more than 1 attempt
				IF g_SpawnData.iSpawnVehicleServerRequestCount > 1
					IF g_SpawnData.iSpawnVehicleState > SPAWN_VEHICLE_SEND_REQUEST
						bResetSpawning = TRUE
						PRINTLN("[BIKER_SELL] [CREATE_SELL_VEH] [GET_IDEAL_SPAWN_POS_FOR_SELL_VEHICLE] .......... Resetting spawn as g_SpawnData.iSpawnVehicleServerRequestCount = ", g_SpawnData.iSpawnVehicleServerRequestCount)
					ENDIF
				ENDIF
				
				//-- Or if the IS_POINT_OK_FOR_NET_ENTITY_CREATION in the initial stage of HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS has failed, and it's using
				//-- the nearest car node instead.
				IF NOT IS_VECTOR_ZERO(g_SpawnData.vRequestedSpawnVehicleCoords)
					IF g_SpawnData.iSpawnVehicleState > SPAWN_VEHICLE_SEND_REQUEST
						IF NOT ARE_VECTORS_EQUAL(g_SpawnData.vRequestedSpawnVehicleCoords, vLocToTry)
							bResetSpawning = TRUE
							PRINTLN("[BIKER_SELL] [CREATE_SELL_VEH] [GET_IDEAL_SPAWN_POS_FOR_SELL_VEHICLE] .......... Resetting spawn as ARE_VECTORS_EQUAL")
						ENDIF
					ENDIF
				ENDIF
				
				IF bResetSpawning
					g_SpawnData.iSpawnVehicleState = SPAWN_VEHICLE_INIT
					Cleanup_Entity_Area_For_Spawning_Vehicle()
					CLEAR_CUSTOM_VEHICLE_NODES()
					
					serverBD.iFindIdealSpawnCoordProg++
					
					IF serverBD.iFindIdealSpawnCoordProg > 2
						//-- There's 3 groups of spawn coords per warehouse. If we've tried them all and they're all blocked, fall back to the first set and go with that
						vIdeal = GET_LAND_SELL_VEHICLE_CREATION_POS(iVeh, 0, serverBD.iStartingWarehouse, serverBD.eShipmentType)
						fIdeal = GET_LAND_SELL_VEHICLE_CREATION_HEADING(iVeh, 0, serverBD.iStartingWarehouse, serverBD.eShipmentType)
						
						serverBD.vSellVehSpawnCoords[iVeh] = vIdeal
						serverBD.fSellVehSpawnHeading[iVeh] = fIdeal
						
						PRINTLN("[BIKER_SELL] [CREATE_SELL_VEH] [GET_IDEAL_SPAWN_POS_FOR_SELL_VEHICLE] Got ideal coords for veh ", iVeh, " but failed to find a suitable spawn group, using 0", " Ideal pos = ", vIdeal, " Heading = ", fIdeal, " Time - ", GET_CLOUD_TIME_AS_INT())
						RETURN TRUE
					ELSE
						PRINTLN("[BIKER_SELL] [CREATE_SELL_VEH] [GET_IDEAL_SPAWN_POS_FOR_SELL_VEHICLE] .......... Failed to find ideal spawn pos for veh ", iVeh, ", will try spawn group ", serverBD.iFindIdealSpawnCoordProg) 
					ENDIF
				ENDIF
				
		
			ENDIF
		ENDIF
	ELIF IS_THIS_AN_AIR_VARIATION()
		vIdeal = GET_AIR_SELL_VEHICLE_CREATION_POS(iVeh)
		fIdeal = GET_AIR_SELL_VEHICLE_CREATION_HEADING(iVeh)
		RETURN TRUE
	ELIF IS_THIS_A_SEA_VARIATION()
		vIdeal = GET_SEA_SELL_VEHICLE_CREATION_POS(iVeh)
		fIdeal = GET_SEA_SELL_VEHICLE_CREATION_HEADING(iVeh)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
//
//FUNC BOOL CREATE_HELI_FOR_TITAN()
//	VECTOR vSpawnCoord
//	FLOAT fSpawnHeading
//	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBuzzardForTitan)
//		IF REQUEST_LOAD_MODEL(BUZZARD)
//			vSpawnCoord = GET_COORDS_FOR_BUZZARD_NEAR_TITAN()
//			fSpawnHeading = GET_HEADING_FOR_BUZZARD_NEAR_TITAN()
//			IF CREATE_NET_VEHICLE(serverBD.niBuzzardForTitan, BUZZARD, vSpawnCoord, fSpawnHeading)
//				NETWORK_FADE_IN_ENTITY(NET_TO_VEH(serverBD.niBuzzardForTitan), TRUE)
//				
//				PRINTLN("[BIKER_SELL] [CREATE_HELI_FOR_TITAN], Buzzard created; vPos: ", vSpawnCoord, "; fHeadingVeh: ", fSpawnHeading, " Time - ", GET_CLOUD_TIME_AS_INT())
//			
//				
//				//Add decorators
//				IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
//					INT iDecoratorValue
//					IF DECOR_EXIST_ON(NET_TO_VEH(serverBD.niBuzzardForTitan), "MPBitset")
//						iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(serverBD.niBuzzardForTitan), "MPBitset")
//					ENDIF
//					SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
//					SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
//					DECOR_SET_INT(NET_TO_VEH(serverBD.niBuzzardForTitan), "MPBitset", iDecoratorValue)
//				ENDIF
//				
//				SET_VEHICLE_AUTOMATICALLY_ATTACHES(NET_TO_VEH(serverBD.niBuzzardForTitan), FALSE)						
//				SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(NET_TO_VEH(serverBD.niBuzzardForTitan), TRUE)
//				SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niBuzzardForTitan), TRUE)
//				SET_VEHICLE_CAN_BE_USED_BY_FLEEING_PEDS(NET_TO_VEH(serverBD.niBuzzardForTitan), FALSE)
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC

FUNC BOOL CREATE_SELL_VEH(INT iVeh)
	
//	VECTOR vIdealPosVeh
//	FLOAT fIdealHeadingVeh
	

	
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh])

		IF GET_IDEAL_SPAWN_POS_FOR_SELL_VEHICLE(iVeh, serverBD.vIdealPosVeh, serverBD.fIdealHeadingVeh)
			PRINTLN("[BIKER_SELL] [CREATE_SELL_VEH] Got ideal spawn coords for veh ", iVeh, " Ideal pos = ", serverBD.vIdealPosVeh, " Heading = ", serverBD.fIdealHeadingVeh)
			IF NOT ARE_VECTORS_EQUAL(serverBD.vIdealPosVeh, << 0.0, 0.0, 0.0>>)
				IF iVeh = 0																		//-- Only find spawn coords if this is the first vehicle
				OR (iVeh > 0 AND IS_BIT_SET(serverBD.iSellVehicleSpawnBitset, iVeh - 1)) 		//-- or if all previous vehicles have spawn coords
					IF NOT IS_BIT_SET(serverBD.iSellVehicleSpawnBitset, iVeh)
						IF REQUEST_LOAD_MODEL(GET_SELL_VEH_MODEL())
							VEHICLE_SPAWN_LOCATION_PARAMS Params
							Params.fMinDistFromCoords = 0.0 
							Params.bConsiderHighways= FALSE
							Params.fMaxDistance= 200.0

							Params.bConsiderOnlyActiveNodes= FALSE
							IF NOT IS_THIS_AN_AIR_VARIATION()
							AND NOT IS_THIS_A_SEA_VARIATION()
								Params.bAvoidSpawningInExclusionZones= TRUE
							ELSE	
								Params.bAvoidSpawningInExclusionZones= FALSE
							ENDIF
							Params.bCheckEntityArea = TRUE
							Params.bUseExactCoordsIfPossible = TRUE
							VECTOR vSpawnHeading
							vSpawnHeading = GET_VECTOR_INFRONT_OF_COORDS_WITH_HEADING(serverBD.vIdealPosVeh, serverBD.fIdealHeadingVeh, 20.0)
							

					//		PRINTLN("[BIKER_SELL] [spawning] Called with vIdealPosVeh = ",vIdealPosVeh, " serverBD.vSellVehSpawnCoords[", iVeh, "] = ", serverBD.vSellVehSpawnCoords[iVeh])
							IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(serverBD.vIdealPosVeh, 
																			vSpawnHeading, 
																			GET_SELL_VEH_MODEL(), 
																			FALSE, 
																			serverBD.vSellVehSpawnCoords[iVeh], 
																			serverBD.fSellVehSpawnHeading[iVeh], 
																			Params)
																			

								
								SET_BIT(serverBD.iSellVehicleSpawnBitset, iVeh)
								PRINTLN("[BIKER_SELL] [CREATE_SELL_VEH], vehicle ", iVeh, " using coord ", serverBD.vSellVehSpawnCoords[iVeh], " and heading ", serverBD.fSellVehSpawnHeading[iVeh])
								PRINTLN("[BIKER_SELL] [CREATE_SELL_VEH], vehicle ", iVeh, " vIdealPosVeh = ", serverBD.vIdealPosVeh, " fIdealHeadingVeh = ", serverBD.fIdealHeadingVeh)
								
							ELSE
							//	PRINTLN("[BIKER_SELL] CREATE_SELL_VEH, waiting for HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS, vehicle ", iVeh)
//								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_THIS_A_SEA_VARIATION()
			GET_IDEAL_SPAWN_POS_FOR_SELL_VEHICLE(iVeh, serverBD.vSellVehSpawnCoords[iVeh], serverBD.fSellVehSpawnHeading[iVeh])
		ENDIF

		IF REQUEST_LOAD_MODEL(GET_SELL_VEH_MODEL())
			IF CAN_REGISTER_MISSION_VEHICLES(GB_GET_BOSS_MISSION_NUM_VEH_REQUIRED(FMMC_TYPE_BIKER_CONTRABAND_SELL))
				IF IS_BIT_SET(serverBD.iSellVehicleSpawnBitset, iVeh)
					IF CREATE_NET_VEHICLE(serverBD.niVeh[iVeh], GET_SELL_VEH_MODEL(), serverBD.vSellVehSpawnCoords[iVeh], serverBD.fSellVehSpawnHeading[iVeh])
						NETWORK_FADE_IN_ENTITY(NET_TO_VEH(serverBD.niVeh[iVeh]), TRUE)
						
						PRINTLN("[BIKER_SELL] [CREATE_SELL_VEH], vehicle ", iVeh, " created; vPos: ", serverBD.vSellVehSpawnCoords[iVeh], "; fHeadingVeh: ", serverBD.fSellVehSpawnHeading[iVeh], " Time - ", GET_CLOUD_TIME_AS_INT())
					
						CLEAR_AREA_OF_VEHICLES(serverBD.vSellVehSpawnCoords[iVeh], 20.00)
						
						//Add decorators
						IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
							INT iDecoratorValue
							IF DECOR_EXIST_ON(VEH_ID(iVeh), "MPBitset")
								iDecoratorValue = DECOR_GET_INT(VEH_ID(iVeh), "MPBitset")
							ENDIF
							SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
							SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
							DECOR_SET_INT(VEH_ID(iVeh), "MPBitset", iDecoratorValue)
						ENDIF
						
						IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
							PRINTLN("[BIKER_SELL] CREATE_DROP_OFF_VEH - DECOR_SET_INT(VEH_ID(iVeh), \"Not_Allow_As_Saved_Veh\",1)")
							DECOR_SET_INT(VEH_ID(iVeh), "Not_Allow_As_Saved_Veh",1)
						ENDIF
						// Delay setting this vehicle as containing contraband if the buyer is buying 20% or 50%
						IF NOT SHOULD_DELAY_BLIP_FOR_FACTORY()
							GB_SET_VEHICLE_AS_CONTRABAND(VEH_ID(iVeh),iVeh, GBCT_BIKER)
						ENDIF
						
						IF IS_THIS_A_SEA_VARIATION()
							IF CAN_ANCHOR_BOAT_HERE_IGNORE_PLAYERS(VEH_ID(iVeh))
								SET_BOAT_ANCHOR(VEH_ID(iVeh), TRUE)
							ENDIF
							SET_VEHICLE_COLOURS(VEH_ID(iVeh), 12, 12)
							SET_VEHICLE_EXTRA_COLOURS(VEH_ID(iVeh), 12, 0)
						ENDIF
						
						IF IS_THIS_AN_AIR_VARIATION()
							SET_ENTITY_MAX_HEALTH(VEH_ID(iVeh), g_sMPTunables.iexec_sell_plane_health)
							SET_ENTITY_HEALTH(VEH_ID(iVeh), g_sMPTunables.iexec_sell_plane_health)
							PRINTLN("[BIKER_SELL] [VEHICLE UPGRADE], CREATE_SELL_VEH - MAKING SURE PLANE IS RESISTANT ENOUGH")
							SET_VEHICLE_CAN_BREAK(VEH_ID(iVeh), FALSE)
							IF BIKER_SELL_AIR_DROP_AT_SEA()
								SET_VEHICLE_TYRES_CAN_BURST(VEH_ID(iVeh), FALSE)
							ENDIF
						ELSE 
							PRINTLN("[BIKER_SELL] - CREATE_SELL_VEH - Vehicle is a Brickade")
							SET_VEHICLE_PETROL_TANK_HEALTH(VEH_ID(iVeh), TO_FLOAT(g_sMPTunables.iexec_sell_brickade_health))
							SET_VEHICLE_BODY_HEALTH(VEH_ID(iVeh), TO_FLOAT(g_sMPTunables.iexec_sell_brickade_health))
							SET_VEHICLE_ENGINE_HEALTH(VEH_ID(iVeh), TO_FLOAT(g_sMPTunables.iexec_sell_brickade_health))
							SET_VEHICLE_COLOURS(VEH_ID(iVeh), 17, 12)
							SET_VEHICLE_EXTRA_COLOURS(VEH_ID(iVeh), 1, 156)	
							SET_VEHICLE_TYRES_CAN_BURST(VEH_ID(iVeh), FALSE)
						ENDIF
						
						SET_VEHICLE_AUTOMATICALLY_ATTACHES(VEH_ID(iVeh), FALSE)						
						SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(VEH_ID(iVeh), TRUE)
						SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(VEH_ID(iVeh), TRUE)
						SET_VEHICLE_CAN_BE_USED_BY_FLEEING_PEDS(VEH_ID(iVeh), FALSE)						
						SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(VEH_ID(iVeh), FALSE)
						IF !IS_THIS_AN_AIR_VARIATION()
							SET_VEHICLE_STRONG(VEH_ID(iVeh), TRUE)
						ENDIF
						SET_VEHICLE_USES_MP_PLAYER_DAMAGE_MULTIPLIER(VEH_ID(iVeh), FALSE)
						
						PRINTLN("[BIKER_SELL] [VEHICLE UPGRADE], CREATE_SELL_VEH - Increased armour NOT unlocked, setting damage scale to 0.3 (3 rocket hits)")
						SET_VEHICLE_DAMAGE_SCALE(VEH_ID(iVeh), g_sMPTunables.fBIKER_SELL_VEHICLE_DAMAGE_MODIFIER)
						
						SET_ENTITY_INVINCIBLE(VEH_ID(iVeh), TRUE)
						PRINTLN("MAINTAIN_VEHICLE_ACCESS, SET_ENTITY_INVINCIBLE, TRUE for i = ", iVeh)
						
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(VEH_ID(iVeh), TRUE)
						
						IF BIKER_SELL_POSTMAN()
						OR ( BIKER_SELL_PROVEN() AND serverBD.eShipmentType <> eBIKER_SHIPMENT_TYPE_SMALL )
							SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(VEH_ID(iVeh), ENUM_TO_INT(SC_DOOR_REAR_LEFT), VEHICLELOCK_LOCKED)
							SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(VEH_ID(iVeh), ENUM_TO_INT(SC_DOOR_REAR_RIGHT), VEHICLELOCK_LOCKED)
						ENDIF
						
						CALL_SET_ALLOW_RAMMING_SOOP_OR_RAMP(VEH_ID(iVeh))
						
						serverBD.vIdealPosVeh = << 0.0, 0.0, 0.0 >>
						serverBD.iFindIdealSpawnCoordProg = 0
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_15 tl15Name = "Veh "
						tl15Name += iVeh
						SET_VEHICLE_NAME_DEBUG(VEH_ID(iVeh), tl15Name)
						#ENDIF
						RETURN TRUE
						//SET_ENTITY_INVINCIBLE(VEH_ID(), TRUE)
					ENDIF
				ENDIF
			ENDIF

		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL bUSE_PLURAL()
	
	IF iDROP_TARGET() = 1
	OR BIKER_SELL_DEFAULT()
	OR BIKER_SELL_CLUB_RUN()
	OR BIKER_SELL_BAG_DROP()
	OR BIKER_SELL_RACE()
	OR BIKER_SELL_STING()
		RETURN FALSE
	ENDIF
	
//	IF BIKER_SELL_EXCHANGE()
//	AND BIKER_SELL_HAS_TWIST_HAPPENED()
//		RETURN FALSE
//	ENDIF
	
	RETURN ( iDROPPED_OFF_COUNT() < (iDROP_TARGET() - 1) )
ENDFUNC

FUNC STRING GET_OPENING_SELL_MISSION_CALL()
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
	
	IF BIKER_SELL_DEFAULT()
		RETURN "BPLES_CONV"
	ELIF BIKER_SELL_AIR_DROP_AT_SEA()
		RETURN "BPLES_AIRD"
	ELIF BIKER_SELL_POSTMAN()
		RETURN "BPLES_POST"
	ELIF BIKER_SELL_BENSON()
		RETURN "BPLES_BEN"
	ELIF BIKER_SELL_BORDER_PATROL()
		RETURN "BPLES_BORDER"
	ELIF BIKER_SELL_HELICOPTER_DROP()
		RETURN "BPLES_HELI"
	ELIF BIKER_SELL_TRASHMASTER()
		RETURN "BPLES_TRASH"
	ELIF BIKER_SELL_STING()
		RETURN "BPLES_STING"
	ELIF BIKER_SELL_FRIENDS_IN_NEED()
		RETURN "BPLES_INNEED"
	ELIF BIKER_SELL_PROVEN()
		RETURN "BPLES_PROVEN"
	ELIF BIKER_SELL_BAG_DROP()
		RETURN "BPLES_DROPS"
	ELIF BIKER_SELL_RACE()
		RETURN "BPLES_RACE"
	ELIF BIKER_SELL_CLUB_RUN()
		IF BIKER_SELL_HAS_TWIST_HAPPENED()
			IF IS_SERVER_BIT1_SET(eSERVERBITSET1_CLUB_RUN_GANG)
				RETURN "BPLES_CGANG"
			ELIF IS_SERVER_BIT1_SET(eSERVERBITSET1_CLUB_RUN_COPS)
				RETURN "BPLES_CCOPS"
			ENDIF
		ENDIF
		RETURN "BPLES_CLUB"
	ELSE
		SWITCH iRand
			CASE 0 RETURN "BPLES_SGEN1"
			CASE 1 RETURN "BPLES_SGEN2"
			CASE 2 RETURN "BPLES_SGEN3"
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

structPedsForConversation sSpeech
PROC DO_BOSS_INTRO_PHONECALL()
	IF IS_PLAYER_IN_OFFICE_PROPERTY(PLAYER_ID())
	OR NOT IS_PLAYER_PLAYING(PLAYER_ID())
	OR IS_PLAYER_IN_CUTSCENE(PLAYER_ID())
	OR IS_LOCAL_PLAYER_DOING_HELI_DOCK_CUTSCENE()
	OR IS_PLAYER_IN_FACTORY(PLAYER_ID())
		EXIT
	ENDIF
	
	IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_PHONE_CALL_SET_UP)
		ADD_PED_FOR_DIALOGUE(sSpeech, 2, NULL, "LJT")
		SET_LOCAL_BIT0(eLOCALBITSET0_PHONE_CALL_SET_UP)
	ELIF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_PHONE_TRIGGERED)
		IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE,DEFAULT, FALSE)
			INT phonecallModifiers
			IF LOCAL_PLAYER_IS_GOON_OF_ACTIVE_GANG()
				GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_BIKER_CH2, ENUM_TO_INT(g_Cellphone.PhoneOwner), KNOWN_CALLER)
			ENDIF
			SET_BIT(phonecallModifiers, MP_COMMS_MODIFIER_FORCE_PLAYER_TO_ANSWER)
			IF Request_MP_Comms_Message(sSpeech, CHAR_BIKER_CH2, "BPLESAU", GET_OPENING_SELL_MISSION_CALL(), phonecallModifiers) 
				SET_LOCAL_BIT0(eLOCALBITSET0_PHONE_TRIGGERED)
				PRINTLN("[intro phonecall] Request_MP_Comms_Message done")
			ELSE
				PRINTLN("[intro phonecall] waiting on Request_MP_Comms_Message to complete")
			ENDIF
		ENDIF
//	ELSE
//		//Reset the calls if needed.
//		IF NOT IS_LOCAL_BIT1_SET(eLOCALBITSET1_RESET_CALL_IF_NEEDED)
//			IF IS_SERVER_BIT1_SET(eSERVERBITSET1_CLUB_RUN_COPS)
//			OR IS_SERVER_BIT1_SET(eSERVERBITSET1_CLUB_RUN_GANG)
//				CLEAR_LOCAL_BIT0(eLOCALBITSET0_PHONE_TRIGGERED)
//				SET_LOCAL_BIT1(eLOCALBITSET1_RESET_CALL_IF_NEEDED)
//				PRINTLN("[intro phonecall] w[TWIST] aiting on Request_MP_Comms_Message to complete")
//			ENDIF
//		ENDIF	
	ENDIF
ENDPROC

FUNC BOOL CREATE_SELL_ENTITIES()
	IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEH_MADE)
		RETURN TRUE
	ENDIF
	INT i

	IF NOT MODE_HAS_PICKUP_BAGS()
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			IF CREATE_SELL_VEH(i)
				// Do anything else to the vehicle here
			ENDIF
		ENDREPEAT
		
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			IF NOT DOES_ENTITY_EXIST(VEH_ID(i))
				RETURN FALSE
			ENDIF
		ENDREPEAT
	ENDIF
	
	SET_SERVER_BIT0(eSERVERBITSET0_VEH_MADE)
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Deal with (Land) Sell vehicles which have become stuck not long after creation, likely because the spawning failed to find a sensible location 
PROC MAINTAIN_SELL_VEHICLES_STUCK_AFTER_CREATION()
	IF NOT IS_THIS_A_LAND_VARIATION()
		EXIT
	ENDIF
	
	INT i
	VEHICLE_SPAWN_LOCATION_PARAMS Params
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF IS_BIT_SET(serverBD.iVehStuckSinceCreatedBitset, i)
			IF IS_VEHICLE_DRIVEABLE(VEH_ID(i))
				IF serverBD.iSellVehToWarp = -1
				OR serverBD.iSellVehToWarp = i
					IF serverBD.iSellVehToWarp = -1
						serverBD.iSellVehToWarp = i
						serverBD.vWarpStuckLoc = GET_ENTITY_COORDS(VEH_ID(i))
						PRINTLN("[BIKER_SELL] [MAINTAIN_SELL_VEHICLES_STUCK_AFTER_CREATION] iSellVehToWarp = ", serverBD.iSellVehToWarp, " Current coords = ", serverBD.vWarpStuckLoc)
					ENDIF
					
					Params.fMinDistFromCoords = 10.0
					Params.bConsiderHighways= FALSE
					Params.fMaxDistance= 100.0
					Params.bConsiderOnlyActiveNodes= FALSE
					Params.bAvoidSpawningInExclusionZones= TRUE
					Params.bCheckEntityArea = TRUE //  TRUE
					Params.bUseExactCoordsIfPossible = FALSE
					VECTOR vNewCoord = <<0.0, 0.0, 0.0>>
					FLOAT fNewHeading = 0.0
					MODEL_NAMES model = GET_ENTITY_MODEL(VEH_ID(i))
					IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(serverBD.vWarpStuckLoc, 
																		<< 0.0, 0.0, 0.0 >>, 
																		model, 
																		FALSE, 
																		vNewCoord, 
																		fNewHeading, 
																		Params)
						IF TAKE_CONTROL_OF_ENTITY(VEH_ID(i))
							SET_ENTITY_COORDS(VEH_ID(i), vNewCoord)
							SET_ENTITY_HEADING(VEH_ID(i), fNewHeading)
							SET_VEHICLE_ON_GROUND_PROPERLY(VEH_ID(i))
							serverBD.iSellVehToWarp = -1
							CLEAR_BIT(serverBD.iVehStuckSinceCreatedBitset, i)
							PRINTLN("[BIKER_SELL] [MAINTAIN_SELL_VEHICLES_STUCK_AFTER_CREATION] Sell veh ", i, " warped to ", vNewCoord, " with heading ", fNewHeading)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[BIKER_SELL] [MAINTAIN_SELL_VEHICLES_STUCK_AFTER_CREATION] Want to warp veh ", i, " but not driveable!")
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC LOCK_SELL_VEH(INT iVeh, BOOL bLock, BOOL bForce = FALSE)
	IF DOES_ENTITY_EXIST(VEH_ENT(iVeh))
		IF NETWORK_HAS_CONTROL_OF_ENTITY(VEH_ENT(iVeh))
	//	OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.niVeh[iVeh]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
		OR ( bForce AND TAKE_CONTROL_OF_ENTITY(VEH_ENT(iVeh)) )
			IF bLock
				IF IS_SELL_VEH_OK(iVeh)
					IF GET_VEHICLE_DOOR_LOCK_STATUS(VEH_ID(iVeh)) <> VEHICLELOCK_LOCKED
						PRINTLN("[CTRB_SELL] 2806496 LOCK_SELL_VEH, VEHICLELOCK_LOCKED")
						SET_VEHICLE_DOORS_LOCKED(VEH_ID(iVeh), VEHICLELOCK_LOCKED)
						IF BIKER_SELL_BENSON()
							VEHICLE_INDEX viVehicle = VEH_ID(iVeh)
							SET_VEHICLE_AS_NO_LONGER_NEEDED(viVehicle)
						ENDIF
						IF bForce AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
							SET_BIT(serverBD.iDoorLocked, iVeh)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_SELL_VEH_OK(iVeh)
					IF GET_VEHICLE_DOOR_LOCK_STATUS(VEH_ID(iVeh)) <> VEHICLELOCK_UNLOCKED
						PRINTLN("[CTRB_SELL] 2806496 LOCK_SELL_VEH, VEHICLELOCK_UNLOCKED")
						SET_VEHICLE_DOORS_LOCKED(VEH_ID(iVeh), VEHICLELOCK_UNLOCKED)
						
						IF bForce AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
							CLEAR_BIT(serverBD.iDoorLocked, iVeh)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC FIX_SELL_VEH(INT iVeh)
	IF NETWORK_HAS_CONTROL_OF_ENTITY(VEH_ENT(iVeh))
		PRINTLN("[CTRB_SELL] FIX_SELL_VEH")
		SET_VEHICLE_ENGINE_HEALTH(VEH_ID(iVeh), 1000)
	ENDIF
ENDPROC

FUNC BOOL HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(INT iVeh)
	SWITCH iVeh
		CASE 0	RETURN IS_SERVER_BIT1_SET(eSERVERBITSET1_VEH_0_DELIVERED)
		CASE 1	RETURN IS_SERVER_BIT1_SET(eSERVERBITSET1_VEH_1_DELIVERED)
		CASE 2	RETURN IS_SERVER_BIT1_SET(eSERVERBITSET1_VEH_2_DELIVERED)
		CASE 3	RETURN IS_SERVER_BIT1_SET(eSERVERBITSET1_VEH_3_DELIVERED)
		CASE 4	RETURN IS_SERVER_BIT1_SET(eSERVERBITSET1_VEH_4_DELIVERED)
		CASE 5	RETURN IS_SERVER_BIT1_SET(eSERVERBITSET1_VEH_5_DELIVERED)
		CASE 6	RETURN IS_SERVER_BIT1_SET(eSERVERBITSET1_VEH_6_DELIVERED)
		CASE 7	RETURN IS_SERVER_BIT1_SET(eSERVERBITSET1_VEH_7_DELIVERED)
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// Local check
FUNC BOOL IS_PLAYER_IN_SELL_VEH(INT iVeh, BOOL bCheckFOrDelivered = TRUE)
	IF NOT IS_SELL_VEH_OK(iVeh)
		RETURN FALSE
	ENDIF
	
	IF bCheckFOrDelivered
		IF HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(iVeh)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[iVeh]))
		RETURN FALSE
	ENDIF
		
	RETURN TRUE
ENDFUNC

// local check
FUNC BOOL IS_PLAYER_IN_ANY_SELL_VEH(BOOL bCheckDelivered = TRUE)
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF IS_PLAYER_IN_SELL_VEH(i, bCheckDelivered)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

// Local/Remote Check
FUNC BOOL IS_THIS_PLAYER_IN_SELL_VEH(PLAYER_INDEX playerToCheck, INT iVeh, BOOL bCheckForDelivered = TRUE)
	IF NOT IS_SELL_VEH_OK(iVeh)
		RETURN FALSE
	ENDIF
	
	IF bCheckFOrDelivered
		IF HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(iVeh)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_PED_IN_VEHICLE(GET_PLAYER_PED(playerToCheck), NET_TO_VEH(serverBD.niVeh[iVeh]))
		RETURN FALSE
	ENDIF
		
	RETURN TRUE
ENDFUNC

// Local/Remote check
FUNC BOOL IS_THIS_PLAYER_IN_ANY_SELL_VEH(PLAYER_INDEX playerToCheck, BOOL bCheckDelivered = TRUE)
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF IS_THIS_PLAYER_IN_SELL_VEH(playerToCheck, i, bCheckDelivered)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_MY_SELL_VEH_AS_INT()
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF IS_PLAYER_IN_SELL_VEH(i)
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

FUNC BOOL IS_SELL_VEH_OCCUPIED_BY_TEAMMATE(INT iVeh, PED_INDEX pedPlayer)
	IF IS_SELL_VEH_OK(iVeh)
		IF pedPlayer = GET_PED_IN_VEHICLE_SEAT(VEH_ID(iVeh), VS_DRIVER)
		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DRIVER_OF_SELL_VEH(INT iVeh, BOOL bCheckFOrDelivered = TRUE)
	IF IS_PLAYER_IN_SELL_VEH(iVeh, bCheckFOrDelivered)
		IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
			IF PLAYER_PED_ID() = GET_PED_IN_VEHICLE_SEAT(VEH_ID(iVeh), VS_DRIVER)
		
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE	
ENDFUNC

FUNC BOOL IS_DRIVER_OF_ANY_SELL_VEH()
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF IS_DRIVER_OF_SELL_VEH(i)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC PLAYER_INDEX GET_DRIVER_OF_SELL_VEH(INT iVeh)

	INT iParticipant
	PARTICIPANT_INDEX participantID
	IF IS_SELL_VEH_OCCUPIED(iVeh)
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
				participantID = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
				PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(participantID)
				PED_INDEX pedPlayer
				IF IS_NET_PLAYER_OK(playerID, FALSE)
					pedPlayer = GET_PLAYER_PED(playerID)
					IF NOT IS_PED_INJURED(pedPlayer)
						IF IS_PED_IN_ANY_VEHICLE(pedPlayer)
							IF GET_VEHICLE_PED_IS_IN(pedPlayer) = NET_TO_VEH(serverBD.niVeh[iVeh])
								IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(pedPlayer), VS_DRIVER) = pedPlayer
									RETURN playerID
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

FUNC BOOL IS_PASSENGER(INT iVeh, BOOL bCrateDropCheck = FALSE)
	VEHICLE_SEAT seatToCheck
	INT i
	
	IF IS_PLAYER_IN_SELL_VEH(iVeh)
		IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
			IF bCrateDropCheck
				// If crate dropping, make sure we're in the front right seat
				IF PLAYER_PED_ID() = GET_PED_IN_VEHICLE_SEAT(VEH_ID(iVeh), VS_FRONT_RIGHT)
					RETURN TRUE
				ENDIF
			ELSE
				REPEAT COUNT_OF(VEHICLE_SEAT) i
					seatToCheck = INT_TO_ENUM(VEHICLE_SEAT, i)
					IF PLAYER_PED_ID() = GET_PED_IN_VEHICLE_SEAT(VEH_ID(iVeh), seatToCheck)
						RETURN TRUE
					ENDIF
				ENDREPEAT
			ENDIF
			
		ENDIF
	ENDIF
	
	RETURN FALSE	
ENDFUNC

FUNC BOOL IS_PASSENGER_OF_ANY_SELL_VEHICLE()
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF IS_PLAYER_IN_SELL_VEH(i)
			IF IS_PASSENGER(i)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_SELL_VEH_HAVE_PASSENGER(INT iVeh, BOOL bCrateDropCheck = FALSE)
	VEHICLE_SEAT seatToCheck
	INT i
	PED_INDEX ped
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh])
		IF NOT IS_ENTITY_DEAD(VEH_ID(iVeh))
			IF bCrateDropCheck
				ped = GET_PED_IN_VEHICLE_SEAT(VEH_ID(iVeh), VS_FRONT_RIGHT)
				IF DOES_ENTITY_EXIST(ped)
					RETURN TRUE
				ENDIF
			ELSE
				REPEAT COUNT_OF(VEHICLE_SEAT) i
					seatToCheck = INT_TO_ENUM(VEHICLE_SEAT, i)
				 	ped = GET_PED_IN_VEHICLE_SEAT(VEH_ID(iVeh), seatToCheck)
					IF DOES_ENTITY_EXIST(ped)
						RETURN TRUE
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL AM_I_IN_THIS_SELL_VEH(INT iVeh)
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh])
		IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[iVeh]))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL AM_I_IN_ANY_SELL_VEH()
	INT iVeh
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iVeh
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh])
			IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[iVeh]))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC
FUNC INT GET_SELL_VEH_ARRAY_POS_I_AM_IN()
	INT iVeh
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iVeh
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh])
			IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[iVeh]))
				RETURN iVeh
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC
//
//FUNC INT GET_SELL_VEH_I_AM_IN_int()
//	INT iVeh
//	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iVeh
//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh])
//			IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[iVeh]))
//				RETURN iVeh
//			ENDIF
//		ENDIF
//	ENDREPEAT
//	RETURN 0
//ENDFUNC

FUNC BOOL IS_ANY_SELL_VEH_OCCUPIED()
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF IS_SELL_VEH_OCCUPIED(i)
		AND IS_SELL_VEH_OK(i)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_SELL_VEHS_OCCUPIED()
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(i)
		AND IS_SELL_VEH_OK(i)
			IF NOT IS_SELL_VEH_OCCUPIED(i)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN TRUE
ENDFUNC

FUNC VECTOR VEH_POS(INT iVeh, BOOL bCloseBy = FALSE)

	VECTOR vReturn 
	
	IF IS_SELL_VEH_OK(iVeh)
		vReturn = GET_ENTITY_COORDS(VEH_ENT(iVeh))
	ENDIF
	
	IF bCloseBy
		vReturn = vReturn + <<5.0, 5.0, 0.0>>
	ENDIF
	
	RETURN vReturn
ENDFUNC

FUNC FLOAT fVEH_HEADING(INT iVeh)

	FLOAT fReturn
	
	IF IS_SELL_VEH_OK(iVeh)
		fReturn = GET_ENTITY_HEADING(VEH_ENT(iVeh))
	ENDIF
	
	RETURN fReturn
ENDFUNC

FUNC BOOL IS_ANY_SELL_VEH_NEAR_DROP_OFF()
	BOOL bToReturn = FALSE
	
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF GET_DISTANCE_BETWEEN_COORDS(VEH_POS(i), GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[i], serverBD.iRoute, i)) < 50
			bToReturn = TRUE
		ENDIF
	ENDREPEAT
	
	RETURN bToReturn
ENDFUNC

FUNC VEHICLE_INDEX GET_CLOSEST_SELL_VEH_TO_PED(INT iPed, INT &iVeh, BOOL bIgnoreDelivered = FALSE)
	INT i
	FLOAT fClosestveh = 9999.99
	FLOAT fTempDist
	VEHICLE_INDEX vehToReturn = INT_TO_NATIVE(VEHICLE_INDEX, -1)

	IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			IF DOES_ENTITY_EXIST(VEH_ID(i))
				IF NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(i)
				OR bIgnoreDelivered
					fTempDist = GET_DISTANCE_BETWEEN_ENTITIES(NET_TO_PED(serverBD.sPed[iPed].netIndex), VEH_ID(i))
					IF fTempDist < fClosestveh
						fClosestveh = fTempDist
						vehToReturn = VEH_ID(i)
						iVeh = i
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		vehToReturn = VEH_ID(0)
		iVeh = 0
	ENDIF
	
	RETURN vehToReturn	
ENDFUNC

FUNC BOOL bIS_POSTMAN_PAT_VEH(INT iVeh)
	
	IF BIKER_SELL_POSTMAN()
	
		RETURN IS_BIT_SET(serverBD.iPostVehParkedForDeliveryBitSet, iVeh)
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC INT GET_CLOSEST_SELL_VEH_TO_LOCAL_PLAYER(BOOL bIncludeOcupied = TRUE, BOOL bIgnoreDelivered = FALSE, BOOL bIncludePost = FALSE)
	INT i
	FLOAT fClosestveh = 9999.99
	FLOAT fTempDist
	INT iVehToReturn = -1

	IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
	AND NOT MODE_HAS_PICKUP_BAGS()
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			IF DOES_ENTITY_EXIST(VEH_ID(i))
			AND IS_SELL_VEH_OK(i)
				IF NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(i)
				OR bIgnoreDelivered
					fTempDist = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), VEH_ID(i))
					IF fTempDist < fClosestveh
						IF bIncludeOcupied
						OR NOT IS_SELL_VEH_OCCUPIED(i)
						OR IS_DRIVER_OF_SELL_VEH(i)
							IF NOT bIS_POSTMAN_PAT_VEH(i)
							OR bIncludePost
								fClosestveh = fTempDist
								iVehToReturn = i
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		iVehToReturn = 0
	ENDIF
	
	RETURN iVehToReturn	
ENDFUNC

// This is for drop-off vehicles not main Sell vehs
FUNC INT GET_CLOSEST_DROP_VEH_TO_ENTITY(ENTITY_INDEX entClosestTo)
	INT i
	FLOAT fClosestveh = 9999.99
	FLOAT fTempDist
	INT iVehToReturn = -1
	VEHICLE_INDEX vehId

	IF DOES_MODE_NEED_DROP_OFF_VEH()
		REPEAT GET_NUM_DROP_OFF_VEH_FOR_VARIATION() i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffVeh[i])
				
				vehId = NET_TO_VEH(serverBD.niDropOffVeh[i])
			
				IF DOES_ENTITY_EXIST(vehId)
				AND NOT IS_ENTITY_DEAD(vehId)
				
					IF DOES_ENTITY_EXIST(entClosestTo)
					AND NOT IS_ENTITY_DEAD(entClosestTo)
			
						fTempDist = GET_DISTANCE_BETWEEN_ENTITIES(entClosestTo, vehId)
						
						IF fTempDist < fClosestveh

							fClosestveh = fTempDist
							iVehToReturn = i
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		iVehToReturn = 0
	ENDIF
	
	RETURN iVehToReturn	
ENDFUNC

// Get the plane height above ground
FUNC FLOAT GET_HEIGHT_ABOVE_GROUND(VEHICLE_INDEX planeVehicle, FLOAT &fGroundHeight)

	FLOAT fHeightAboveGround
	VECTOR vPlayerCoords = GET_ENTITY_COORDS(planeVehicle)
	GET_GROUND_Z_FOR_3D_COORD(vPlayerCoords, fGroundHeight)
	IF fGroundHeight < 0
		fGroundHeight = 0
	ENDIF
	
//	IF IS_ENTITY_IN_AREA(planeVehicle, <<-181.68785, 3545.77539, 62.87735>>, <<2463.76489, 4640.36670, 31.42104>>, FALSE, FALSE)
//		IF fGroundHeight < 30
//			fGroundHeight = 30
//		ENDIF
//	ENDIF
	
	fHeightAboveGround = ( vPlayerCoords.z - fGroundHeight )	
	IF fHeightAboveGround < 0
		fHeightAboveGround *= -1
	ENDIF	
	
	RETURN fHeightAboveGround
ENDFUNC

FUNC BOOL bFLYING_TOO_HIGH()
//	RETURN ( serverBD.fDistAboveGround > fGET_ALLOWABLE_HEIGHT() )
	RETURN (sLocaldata.fMyDistAboveGround > fGET_ALLOWABLE_HEIGHT())
ENDFUNC

PROC DO_RADAR_ALTIMETER(BOOL bShow)
	IF BIKER_SELL_AIR_DROP_AT_SEA()
		IF bShow
			PRINTLN("DO_RADAR_ALTIMETER, sLocaldata.fMyDistGroundHeight = ", sLocaldata.fMyDistGroundHeight, " fGET_ALLOWABLE_HEIGHT() = ", fGET_ALLOWABLE_HEIGHT())
			SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(sLocaldata.fMyDistGroundHeight + fGET_ALLOWABLE_HEIGHT())	
		ELSE
			PRINTLN("DO_RADAR_ALTIMETER, 0 ")
			SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Deal with the player flying the plane too high during the "Air Low" variation
PROC MAINTAIN_PLANE_HEIGHT_CLIENT()
	IF BIKER_SELL_AIR_DROP_AT_SEA()
	
		FLOAT fDist
		FLOAT fGroundHeight
		BOOL bPlayTooHighSound = FALSE
		BOOL bDoAltimeter = FALSE
		INT i
	
		i = GET_SELL_VEH_I_AM_IN_INT()
		IF i != -1
			IF IS_SELL_VEH_OK(i)
				fDist = GET_HEIGHT_ABOVE_GROUND(VEH_ID(i), fGroundHeight)
					
				IF sLocaldata.fMyDistGroundHeight != fGroundHeight 
					sLocaldata.fMyDistGroundHeight = fGroundHeight
				ENDIF
				
				IF sLocaldata.fMyDistAboveGround != fDist
					sLocaldata.fMyDistAboveGround = fDist
				ENDIF
				
				IF sLocaldata.fMyDistAboveGround > fGET_ALLOWABLE_HEIGHT()
					//-- Too high
					
					IF bPlayTooHighSound = FALSE
						bPlayTooHighSound = TRUE
					ENDIF
					
					IF IS_DRIVER_OF_SELL_VEH(i)
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
							IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_GIVEN_WANTED_FOR_TOO_HIGH)
								CLEAR_LOCAL_BIT0(eLOCALBITSET0_GIVEN_WANTED_FOR_TOO_HIGH)
								
								IF HAS_NET_TIMER_STARTED(sLocaldata.timeTooHigh)
									RESET_NET_TIMER(sLocaldata.timeTooHigh)
									PRINTLN("[BIKER_SELL] - MAINTAIN_WANTED_RATINGS RESET timeTooHigh as wanted level is 0!")
								ENDIF
							ENDIF
							
							IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_PLANE_TOO_HIGH)
								PRINTLN("[BIKER_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] I'm too high - fMyDistAboveGround = ", sLocaldata.fMyDistAboveGround)
								SET_CLIENT_BIT0(eCLIENTBITSET0_PLANE_TOO_HIGH)
							ENDIF
						ENDIF
					ELSE
						IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_PLANE_TOO_HIGH)
							PRINTLN("[BIKER_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] Clear eCLIENTBITSET0_PLANE_TOO_HIGH as not driver ")
							CLEAR_CLIENT_BIT0(eCLIENTBITSET0_PLANE_TOO_HIGH)
						ENDIF
					ENDIF
					
				ELSE
					//-- Below max height
					
					IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_PLANE_TOO_HIGH)
						PRINTLN("[BIKER_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] Clear eCLIENTBITSET0_PLANE_TOO_HIGH as fMyDistAboveGround = ", sLocaldata.fMyDistAboveGround)
						CLEAR_CLIENT_BIT0(eCLIENTBITSET0_PLANE_TOO_HIGH)
					ENDIF
					
					IF bPlayTooHighSound = TRUE
						bPlayTooHighSound = FALSE
					ENDIF
					
					IF IS_DRIVER_OF_SELL_VEH(i)
						IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_FLY_LOW_HELP)
	 						IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
							AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONT_GOHP")
							AND IS_ENTITY_IN_AIR(VEH_ID(i))
								PRINT_HELP_NO_SOUND("SCONTRA_HLP13") //Fly as low as possible to avoid detection by the Cops.
								GB_SET_GANG_BOSS_HELP_BACKGROUND()
								SET_LOCAL_BIT0(eLOCALBITSET0_FLY_LOW_HELP)
								PRINTLN("[BIKER_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] Done fly-lo help")
							ENDIF
						ELSE
							IF NOT IS_LOCAL_BIT1_SET(eLOCALBITSET1_SHOWN_ALTIMETER_HELP)
								IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
								AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONT_GOHP")
								AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP13")
								AND IS_ENTITY_IN_AIR(VEH_ID(i))
									PRINT_HELP_NO_SOUND("BSELL_ALTI") 	//The altimeter on the radar shows how high your are flying the Dodo. 
																		// Remain below the upper horizon line to avoid detection.
									GB_SET_GANG_BOSS_HELP_BACKGROUND()
									SET_LOCAL_BIT1(eLOCALBITSET1_SHOWN_ALTIMETER_HELP)
									PRINTLN("[BIKER_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] Done altimeter help")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				bDoAltimeter = TRUE
			ELSE
				IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_PLANE_TOO_HIGH)
					PRINTLN("[BIKER_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] Clear eCLIENTBITSET0_PLANE_TOO_HIGH as not driver ")
					CLEAR_CLIENT_BIT0(eCLIENTBITSET0_PLANE_TOO_HIGH)
				ENDIF
			ENDIF
		ELSE
			IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_PLANE_TOO_HIGH)
				PRINTLN("[BIKER_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] Clear eCLIENTBITSET0_PLANE_TOO_HIGH as not driver ")
				CLEAR_CLIENT_BIT0(eCLIENTBITSET0_PLANE_TOO_HIGH)
			ENDIF
		ENDIF
		
		DO_RADAR_ALTIMETER(bDoAltimeter)
		
		IF bPlayTooHighSound
			IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_PLAY_ALTITUDE_WARNING_SOUND)
				//-- Play auido. Only Start playing if we haven't just stopped it playing as otherwise it sounds weird.
				IF NOT HAS_NET_TIMER_STARTED(sLocaldata.timeHeightWarningSTop)
				OR (HAS_NET_TIMER_STARTED(sLocaldata.timeHeightWarningSTop) AND HAS_NET_TIMER_EXPIRED(sLocaldata.timeHeightWarningSTop, 1000))
				//	PLAY_SOUND_FRONTEND(sLocaldata.iAltitudeWarningSound, "Altitude_Warning", "EXILE_1")
					PLAY_SOUND_FRONTEND(sLocaldata.iAltitudeWarningSound, "Altitude_Warning_Loop", "DLC_Exec_Fly_Low_Sounds")
					SET_LOCAL_BIT0(eLOCALBITSET0_PLAY_ALTITUDE_WARNING_SOUND)
					PRINTLN("[BIKER_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] Playing altitude warning audio")
				ENDIF
			ENDIF
		ELSE
			IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_PLAY_ALTITUDE_WARNING_SOUND)
				RESET_NET_TIMER(sLocaldata.timeHeightWarningSTop)
				START_NET_TIMER(sLocaldata.timeHeightWarningSTop)
				STOP_SOUND(sLocaldata.iAltitudeWarningSound)
				CLEAR_LOCAL_BIT0(eLOCALBITSET0_PLAY_ALTITUDE_WARNING_SOUND)
				PRINTLN("[BIKER_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] Stop warning sound as below height ")
			ENDIF
		ENDIF
		
		
		IF IS_SERVER_BIT1_SET(eSERVERBITSET1_SEND_WANTED_TEXT_MESSAGE)
			IF NOT IS_LOCAL_BIT1_SET(eLOCALBITSET1_LOST_WANTED_TEXT_MESSAGE)
				IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BIKER_CH2, "SBIKERP_NWBY", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
					PRINTLN("[BIKER_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] Sent too high text")
					SET_LOCAL_BIT1(eLOCALBITSET1_LOST_WANTED_TEXT_MESSAGE)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_PLANE_TOO_HIGH)
			IF NOT IS_LOCAL_BIT1_SET(eLOCALBITSET1_SENT_TOO_HIGH_TEXT)
				IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BIKER_CH2, "SBIKERP_HIGH", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
					PRINTLN("[BIKER_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] Sent too high text")
					SET_LOCAL_BIT1(eLOCALBITSET1_SENT_TOO_HIGH_TEXT)
				ENDIF
			ENDIF
			
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP11")
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP("SCONTRA_HLP11")
					RESET_NET_TIMER(sLocaldata.timeTooHighHelp)
				ENDIF
			ENDIF
				
			IF NOT HAS_NET_TIMER_STARTED(sLocaldata.timeTooHigh)
				START_NET_TIMER(sLocaldata.timeTooHigh)
				PRINTLN("[BIKER_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] Started timeTooHigh")
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(sLocaldata.timeTooHigh)
				RESET_NET_TIMER(sLocaldata.timeTooHigh)
				PRINTLN("[BIKER_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] Reset timeTooHigh as eCLIENTBITSET0_PLANE_TOO_HIGH is cleared")
			ENDIF
			
			//-- CLear the warning help, but only if it's been displayed for a minumum amount of time
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP11")
				IF HAS_NET_TIMER_EXPIRED(sLocaldata.timeTooHighHelp, 3000)
					CLEAR_HELP()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_BE_SET_AS_ILLICIT_GOODS_PLAYER(BOOL bVisibleToRivals = FALSE)

	IF bVisibleToRivals
		IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_VEHICLES_MARKED_AS_CONTRABAND)
			RETURN FALSE
		ENDIF
	ENDIF

	IF MODE_HAS_PICKUP_BAGS()
		RETURN DO_I_HAVE_A_BAG()
	ENDIF

	RETURN IS_DRIVER_OF_ANY_SELL_VEH()
ENDFUNC

PROC SET_ILLICIT_GOODS_PLAYER()
	IF SHOULD_BE_SET_AS_ILLICIT_GOODS_PLAYER()
	AND GET_END_REASON() = eENDREASON_NO_REASON_YET
		SET_PLAYER_RIVAL_ENTITY_TYPE(eRIVALENTITYTYPE_BIKER)
		
		IF SHOULD_BE_SET_AS_ILLICIT_GOODS_PLAYER(TRUE)
			SET_PLAYER_RIVAL_ENTITY_VISIBLE_TO_RIVALS(TRUE)
		ENDIF
	ELSE
		SET_PLAYER_RIVAL_ENTITY_TYPE(eRIVALENTITYTYPE_INVALID)
		SET_PLAYER_RIVAL_ENTITY_VISIBLE_TO_RIVALS(FALSE)
	ENDIF
ENDPROC

FUNC BOOL ENTITY_AT_DROP_OFF_COORDS( VECTOR vDrop, ENTITY_INDEX eiEntity, BOOL bPackage = FALSE)
	IF IS_ENTITY_AT_COORD(eiEntity, vDrop, vLOCATE_DIMENSION(bPackage))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC HANDLE_VEHICLE_STOPPING()
	INT iVeh

	IF NOT BIKER_SELL_HELICOPTER_DROP()
	AND NOT BIKER_SELL_AIR_DROP_AT_SEA()
	AND NOT DOES_BIKER_SELL_MULTIPLE_DROP_OFFS()
	AND NOT BIKER_SELL_STING()
		IF IS_DRIVER_OF_ANY_SELL_VEH()
			iVeh = GET_SELL_VEH_I_AM_IN_INT()
//			IF IS_BIT_SET(serverBD.iActiveDropOffBitset, serverBD.iClosestDropOff)
				IF HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(iVeh)
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(VEH_ID(GET_SELL_VEH_I_AM_IN_INT()))
				ENDIF
//			ENDIF
		ENDIF
	ENDIF
ENDPROC

SCRIPT_TIMER stBagDeathTimer
PROC MAINTAIN_BAG_DEATH_HELP()
	IF NOT IS_LOCAL_BIT1_SET(eLOCALBITSET1_BAG_DEATH_HELP)
		IF MODE_HAS_PICKUP_BAGS()
			IF NOT HAS_NET_TIMER_STARTED(stBagDeathTimer)
				START_NET_TIMER(stBagDeathTimer)
			ELIF HAS_NET_TIMER_EXPIRED(stBagDeathTimer, 30000)
			AND MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
			AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF DO_I_HAVE_A_BAG()
					//If you die while carrying a duffel bag the product remaining in the bag will be lost.  
					PRINT_HELP_NO_SOUND("BSB_HOLDERS")
				ELSE
					//Protect Members of the MC that have a duffel bag. If they are killed the product they are carrying will be lost.
					PRINT_HELP_NO_SOUND("BSB_HELPERS")
				ENDIF
				GB_SET_GANG_BOSS_HELP_BACKGROUND()
				SET_LOCAL_BIT1(eLOCALBITSET1_BAG_DEATH_HELP)
			ENDIF
		ELSE
			SET_LOCAL_BIT1(eLOCALBITSET1_BAG_DEATH_HELP)
		ENDIF 
	ENDIF 
ENDPROC

FUNC eCLIENT_BITSET_0 GET_AIR_CLEAR_LAND_BIT_TO_SET(INT iVeh)
	SWITCH iVeh
		CASE 0	RETURN eCLIENTBITSET0_PLANE_0_LANDED
		CASE 1	RETURN eCLIENTBITSET0_PLANE_1_LANDED
		CASE 2	RETURN eCLIENTBITSET0_PLANE_2_LANDED
	ENDSWITCH
	RETURN eCLIENTBITSET0_PLANE_0_LANDED
ENDFUNC

PROC EJECT_PLAYER_FROM_VEHICLE(INT iVeh, BOOL bIgnoreAirCheck = FALSE)
	PRINTLN("EJECT_PLAYER_FROM_VEHICLE Called")
	IF IS_PLAYER_IN_SELL_VEH(iVeh, FALSE)
	AND ( NOT IS_THIS_AN_AIR_VARIATION() OR bIgnoreAirCheck )
	AND NOT IS_THIS_A_SEA_VARIATION()
		PRINTLN("[BIKER_SELL] EJECT_PLAYER_FROM_VEHICLE exiting veh ", iVeh)
		TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), VEH_ID(iVeh))
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
	ENDIF
ENDPROC

PROC HANDLE_LANDING_PLANE_FLAG()

ENDPROC

FUNC PLAYER_INDEX PLAYER_REPAIRING()
	PLAYER_INDEX player = INVALID_PLAYER_INDEX()
	
	IF serverBD.iPlayerRepair <> -1
		player = INT_TO_PLAYERINDEX(serverBD.iPlayerRepair)
	ENDIF
	
	RETURN player
ENDFUNC

FUNC BOOL IS_TEAMMATE_REPAIRING()
	IF PLAYER_REPAIRING() <> INVALID_PLAYER_INDEX()
	AND PLAYER_REPAIRING() <> PLAYER_ID()
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_RUN_STUCK_CHECKS(INT iVeh)

	// If the vehicle has moved away from its spawn point
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(VEH_ID(iVeh), serverBD.vSellVehSpawnCoords[iVeh]) > 10
		RETURN TRUE
	ENDIF
	
	IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), VEH_ID(iVeh)) < 100
		RETURN TRUE
	ENDIF
	
	SWITCH iVeh
		CASE 0  
			IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_0_ENTERED_ONCE)	
				RETURN TRUE	
			ENDIF 
		BREAK
		CASE 1  
			IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_1_ENTERED_ONCE)	
				RETURN TRUE	
			ENDIF 
		BREAK
		CASE 2  
			IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_2_ENTERED_ONCE)	
				RETURN TRUE	
			ENDIF 
		BREAK
		CASE 3
			IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_3_ENTERED_ONCE)	
				RETURN TRUE	
			ENDIF 
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL bSHUT_VEH_DOOR(NETWORK_INDEX niDrop, SC_DOOR_LIST scDoor)
	IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niDrop)
	OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(niDrop) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
	OR (IS_NETWORK_ID_OWNED_BY_PARTICIPANT(niDrop) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
		IF TAKE_CONTROL_OF_NET_ID(niDrop)
		
			VEHICLE_INDEX vehId = NET_TO_VEH(niDrop)
		
			IF DOES_ENTITY_EXIST(vehId)
			AND NOT IS_ENTITY_DEAD(vehId)
		
				PRINTLN("[BIKER_SELL] bSHUT_VEH_DOOR - SET_VEHICLE_DOOR_LATCHED(NET_TO_VEH(serverBD.niDropOffVeh), TRUE, TRUE)")
				SET_VEHICLE_DOOR_LATCHED(vehId, scDoor, TRUE, TRUE)
				PRINTLN("[BIKER_SELL] bSHUT_VEH_DOOR - SET_VEHICLE_DOOR_AUTO_LOCK(NET_TO_VEH(serverBD.niDropOffVeh), TRUE)")
				SET_VEHICLE_DOOR_AUTO_LOCK(vehId, scDoor, TRUE)
				PRINTLN("[BIKER_SELL] bSHUT_VEH_DOOR - SET_VEHICLE_DOOR_OPEN(NET_TO_VEH(serverBD.niDropOffVeh), FALSE, TRUE)")
				SET_VEHICLE_DOOR_SHUT(vehId, scDoor, TRUE)
				SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_BOOT), VEHICLELOCK_LOCKED)
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_DROP_OFF_VEH()
	
	VECTOR vPosStart, vPosVeh
	FLOAT fHeadingVeh
	INT iLanes
	BOOL bCreated = TRUE
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//		vPosStart = GET_ENTITY_COORDS(PLAYER_PED_ID())
		vPosStart = GET_WAREHOUSE_COORDS(serverBD.iStartingWarehouse)
	ENDIF
	
	IF NOT DOES_MODE_NEED_DROP_OFF_VEH()
		SET_SERVER_BIT0(eSERVERBITSET0_DROP_OFF_VEH_MADE)
		RETURN TRUE
	ENDIF
	
	IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_DROP_OFF_VEH_MADE)
		IF DOES_MODE_NEED_DROP_OFF_VEH()
		OR GET_SAFE_VEHICLE_NODE(vPosStart, vPosVeh, fHeadingVeh, iLanes)
			INT i
			INT iRandVeh
			REPEAT GET_NUM_DROP_OFF_VEH_FOR_VARIATION() i
				IF BIKER_SELL_AIR_DROP_AT_SEA()
					iRandVeh = GET_RANDOM_INT_IN_RANGE(0,4)
				ENDIF
				IF REQUEST_LOAD_MODEL(GET_DROP_OFF_VEH_MODEL(i, iRandVeh))
				AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffVeh[i])
					IF DOES_MODE_NEED_DROP_OFF_VEH()
						vPosVeh = vGET_DROP_OFF_VEH_START(i)
						IF BIKER_SELL_BENSON()
							INT iCount = 0
							INT iLoop = 0
							fHeadingVeh = -1.0
							REPEAT 32 iLoop
								IF IS_BIT_SET(serverBD.dropOffLocationBitSet, iLoop)
								AND fHeadingVeh = -1.0
									IF iCount = i
										fHeadingVeh = fGET_DROP_OFF_VEH_HEAD(iLoop)
										PRINTLN("[BIKER_SELL] [BEN] CREATE_DROP_OFF_VEH - fGET_DROP_OFF_VEH_HEAD return fGET_DROP_OFF_VEH_HEAD(", iLoop, ") = ", fHeadingVeh)
									ELSE
										iCount++
										PRINTLN("[BIKER_SELL] [BEN] CREATE_DROP_OFF_VEH - fGET_DROP_OFF_VEH_HEAD - iCount = ", iCount)
									ENDIF
								ENDIF
							ENDREPEAT
						ELSE
							fHeadingVeh = fGET_DROP_OFF_VEH_HEAD(i)
						ENDIF
					ENDIF
					IF CAN_REGISTER_MISSION_VEHICLES((GB_GET_BOSS_MISSION_NUM_VEH_REQUIRED(FMMC_TYPE_BIKER_CONTRABAND_SELL) + 1))
						IF CREATE_NET_VEHICLE(serverBD.niDropOffVeh[i], GET_DROP_OFF_VEH_MODEL(i, iRandVeh), vPosVeh, fHeadingVeh)
						
							PRINTLN("[BIKER_SELL] CREATE_DROP_OFF_VEH, DONE ", vPosVeh, " fHeadingVeh = ", fHeadingVeh)
							
							CLEAR_AREA(vPosVeh, CLEAR_AREA_AROUND_NEW_VEH_AREA, TRUE, DEFAULT, DEFAULT, TRUE)
						
							IF IS_THIS_MODEL_A_BOAT(GET_DROP_OFF_VEH_MODEL(i, iRandVeh))
								SET_BOAT_ANCHOR(NET_TO_VEH(serverBD.niDropOffVeh[i]), TRUE)
							ENDIF
							
							SET_VEHICLE_TYRES_CAN_BURST(NET_TO_VEH(serverBD.niDropOffVeh[i]), FALSE)
							
							//Add decorators
							IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
								INT iDecoratorValue
								IF DECOR_EXIST_ON(NET_TO_ENT(serverBD.niDropOffVeh[i]), "MPBitset")
									iDecoratorValue = DECOR_GET_INT(NET_TO_ENT(serverBD.niDropOffVeh[i]), "MPBitset")
								ENDIF
								SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
								SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
								DECOR_SET_INT(NET_TO_ENT(serverBD.niDropOffVeh[i]), "MPBitset", iDecoratorValue)
							ENDIF
							IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
								PRINTLN("[BIKER_SELL] CREATE_DROP_OFF_VEH - DECOR_SET_INT(NET_TO_ENT(serverBD.niDropOffVeh[i]), \"Not_Allow_As_Saved_Veh\",1)")
								DECOR_SET_INT(NET_TO_ENT(serverBD.niDropOffVeh[i]), "Not_Allow_As_Saved_Veh",1)
							ENDIF
							SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(NET_TO_VEH(serverBD.niDropOffVeh[i]), FALSE)
							IF BIKER_SELL_BENSON()
								PRINTLN("[BIKER_SELL] CREATE_DROP_OFF_VEH - SET_VEHICLE_DOOR_LATCHED(NET_TO_VEH(serverBD.niDropOffVeh[", i, "]), SC_DOOR_BOOT, FALSE, FALSE)")
								SET_VEHICLE_DOOR_LATCHED(NET_TO_VEH(serverBD.niDropOffVeh[i]), SC_DOOR_BOOT, FALSE, FALSE)
								PRINTLN("[BIKER_SELL] CREATE_DROP_OFF_VEH - SET_VEHICLE_DOOR_AUTO_LOCK(NET_TO_VEH(serverBD.niDropOffVeh[", i, "]), SC_DOOR_BOOT, FALSE)")
								SET_VEHICLE_DOOR_AUTO_LOCK(NET_TO_VEH(serverBD.niDropOffVeh[i]), SC_DOOR_BOOT, FALSE)
								PRINTLN("[BIKER_SELL] CREATE_DROP_OFF_VEH - SET_VEHICLE_DOOR_OPEN(NET_TO_VEH(serverBD.niDropOffVeh[", i, "]), SC_DOOR_BOOT, FALSE, TRUE)")
								SET_VEHICLE_DOOR_OPEN(NET_TO_VEH(serverBD.niDropOffVeh[i]), SC_DOOR_BOOT, FALSE, TRUE)
								SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(NET_TO_VEH(serverBD.niDropOffVeh[i]), SC_DOOR_BOOT, FALSE)
								//SET_VEHICLE_EXTRA(NET_TO_VEH(serverBD.niDropOffVeh[i]), 0, FALSE)
								//SET_VEHICLE_EXTRA(NET_TO_VEH(serverBD.niDropOffVeh[i]), 1, FALSE)
								//SET_VEHICLE_EXTRA(NET_TO_VEH(serverBD.niDropOffVeh[i]), 2, TRUE)
							ENDIF
							SET_VEHICLE_AUTOMATICALLY_ATTACHES(NET_TO_ENT(serverBD.niDropOffVeh[i]), FALSE)
							SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(NET_TO_VEH(serverBD.niDropOffVeh[i]), FALSE)
							//IF NOT BIKER_SELL_EXCHANGE()
								SET_ENTITY_INVINCIBLE(NET_TO_ENT(serverBD.niDropOffVeh[i]), TRUE)
								SET_ENTITY_CAN_BE_DAMAGED(NET_TO_ENT(serverBD.niDropOffVeh[i]), FALSE)
							//ENDIF
							SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(NET_TO_VEH(serverBD.niDropOffVeh[i]), TRUE)
							SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niDropOffVeh[i]), TRUE)
							SET_VEHICLE_CAN_BE_USED_BY_FLEEING_PEDS(NET_TO_VEH(serverBD.niDropOffVeh[i]), FALSE)
							
							CALL_SET_ALLOW_RAMMING_SOOP_OR_RAMP(NET_TO_VEH(serverBD.niDropOffVeh[i]))
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			REPEAT GET_NUM_DROP_OFF_VEH_FOR_VARIATION() i
				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffVeh[i])
					bCreated = FALSE
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	IF bCreated
		SET_SERVER_BIT0(eSERVERBITSET0_DROP_OFF_VEH_MADE)
	ENDIF
	
	RETURN IS_SERVER_BIT0_SET(eSERVERBITSET0_DROP_OFF_VEH_MADE)
ENDFUNC

FUNC STRING GET_BONE_NAME()
	IF BIKER_SELL_HELICOPTER_DROP()
		RETURN "chassis"
	ENDIF

	RETURN "chassis_dummy"
ENDFUNC

FUNC BOOL CREATE_ATTACHED_VEH_PEDS()
				
	INT iVeh
	VECTOR vPos
	FLOAT fHead
	
	INT iCount
	
	PED_INDEX pedId
	
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE()  iVeh
	
		vPos = GET_LAND_SELL_VEHICLE_CREATION_POS(iVeh, 0, serverBD.iStartingWarehouse, serverBD.eShipmentType)
		
		PRINTLN("CREATE_ATTACHED_VEH_PEDS, iVeh = ", iVeh, " GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE()  = ", GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() )
		
		IF NOT DOES_ENTITY_EXIST(NET_TO_ENT(serverBD.niDropOffPed[iVeh]))
			IF CREATE_NET_PED(serverBD.niDropOffPed[iVeh], PEDTYPE_MISSION, GET_DROP_OFF_PED_MODEL(iVeh), vPos, fHead) 
				pedId = NET_TO_PED(serverBD.niDropOffPed[iVeh])
				PRINTLN("CREATE_ATTACHED_VEH_PEDS, vPos = ", vPos, " iVeh = ", iVeh)
				SET_ENTITY_INVINCIBLE(pedId, TRUE)
				SET_ENTITY_CAN_BE_DAMAGED(pedId, FALSE)
				SET_PED_KEEP_TASK(pedId, TRUE)
				STOP_PED_SPEAKING(pedId, TRUE)
				SET_PED_CONFIG_FLAG(pedId, PCF_DisableShockingEvents, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, TRUE)
				ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(serverBD.niDropOffPed[iVeh]), VEH_ENT(iVeh), GET_ENTITY_BONE_INDEX_BY_NAME(VEH_ID(iVeh), GET_BONE_NAME()), <<0.0,-1.4,0.55>>, <<0,0,-90>>)
				
//				IF BIKER_SELL_TACO_VAN()
//					SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_HEAD, 		2, 2)
//					SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_HAIR, 		0, 1)
//					SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_TORSO, 		2, 0)
//					SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_LEG, 		2, 0)
//					SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_FEET, 		2, 0)
//					SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_SPECIAL,	2, 0)
//				ENDIF	
				
				SET_ATTRIBUTES_FOR_FRIEND(pedID)
				SET_PED_TREATED_AS_FRIENDLY(pedId, TRUE)
			ENDIF	
		ELSE
			iCount++
		ENDIF

	ENDREPEAT	
	
	RETURN ( iCount = GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() )
ENDFUNC

FUNC BOOL CREATE_DROP_OFF_PED( INT iPed)

	VECTOR vPos
	FLOAT fHeading

	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffPed[iPed])
		IF REQUEST_LOAD_MODEL(GET_DROP_OFF_PED_MODEL( iPed))
		
			vPos = vGET_DROP_OFF_PED_START()
			fHeading = fGET_DROP_OFF_PED_HEAD()
	
			IF CAN_REGISTER_MISSION_PEDS(GB_GET_BOSS_MISSION_NUM_PEDS_REQUIRED(FMMC_TYPE_BIKER_CONTRABAND_SELL) + 1)
				IF SHOULD_SPAWN_DROP_OFF_PED_IN_VEHICLE()
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_DROP_OFF_VEH_PED_SHOULD_BE_IN(iPed))
						IF CREATE_NET_PED_IN_VEHICLE(serverBD.niDropOffPed[iPed], GET_DROP_OFF_VEH_PED_SHOULD_BE_IN(iPed), PEDTYPE_MISSION, GET_DROP_OFF_PED_MODEL( iPed), GET_DROP_OFF_PED_VEHICLE_SEAT(iPed))
							PRINTLN("[BIKER_SELL] CREATE_DROP_OFF_PED, Created ped ", iPed, " in vehicle")
							SET_ENTITY_INVINCIBLE(NET_TO_PED(serverBD.niDropOffPed[iPed]), TRUE)
							SET_ENTITY_CAN_BE_DAMAGED(NET_TO_PED(serverBD.niDropOffPed[iPed]), FALSE)
							SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niDropOffPed[iPed]), CA_LEAVE_VEHICLES, FALSE)
							SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.niDropOffPed[iPed]), PCF_DisablePanicInVehicle, TRUE)
							SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.niDropOffPed[iPed]), PCF_CanBeAgitated, FALSE)
							SET_PED_CAN_BE_DRAGGED_OUT(NET_TO_PED(serverBD.niDropOffPed[iPed]), FALSE)
							SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.niDropOffPed[iPed]), PCF_PlayersDontDragMeOutOfCar, TRUE)
							SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(serverBD.niDropOffPed[iPed]), FA_NEVER_FLEE, TRUE)
							SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.niDropOffPed[iPed]), rgFM_AiLike)
							SET_PED_KEEP_TASK(NET_TO_PED(serverBD.niDropOffPed[iPed]), TRUE)
							STOP_PED_SPEAKING(NET_TO_PED(serverBD.niDropOffPed[iPed]), TRUE)
							TASK_STAND_STILL(NET_TO_PED(serverBD.niDropOffPed[iPed]), -1)
							serverBD.iDropPedsMade++
						ENDIF
					ENDIF
				ELIF SHOULD_SPAWN_DROP_OFF_PED_ATTACHED_TO_VEHICLE()
					IF CREATE_ATTACHED_VEH_PEDS()
						
						serverBD.iDropPedsMade++
						PRINTLN("[BIKER_SELL] CREATE_DROP_OFF_PED, CREATE_ATTACHED_VEH_PEDS, DONE serverBD.iDropPedsMade = ", serverBD.iDropPedsMade)
					ENDIF
				ELSE		
					IF CREATE_NET_PED(serverBD.niDropOffPed[iPed], PEDTYPE_MISSION, GET_DROP_OFF_PED_MODEL( iPed), vPos, fHeading) 
						//IF BIKER_SELL_EXCHANGE()
						//	SET_ATTRIBUTES_FOR_GUARD(NET_TO_PED(serverBD.niDropOffPed[iPed]), DEFAULT, iPed)
						//ELSE
							SET_ENTITY_INVINCIBLE(NET_TO_PED(serverBD.niDropOffPed[iPed]), TRUE)
							SET_ENTITY_CAN_BE_DAMAGED(NET_TO_PED(serverBD.niDropOffPed[iPed]), FALSE)
						//ENDIF
						SET_PED_KEEP_TASK(NET_TO_PED(serverBD.niDropOffPed[iPed]), TRUE)
						STOP_PED_SPEAKING(NET_TO_PED(serverBD.niDropOffPed[iPed]), TRUE)
						
						serverBD.iDropPedsMade++
						PRINTLN("[BIKER_SELL] CREATE_DROP_OFF_PED, DONE ", vPos, " serverBD.iDropPedsMade = ", serverBD.iDropPedsMade)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_DROP_OFF_PEDS()
	INT i
	
	IF NOT DOES_MODE_NEED_DROP_OFF_PED()
		RETURN TRUE
	ENDIF
	
	REPEAT GET_NUM_DROP_OFF_PEDS_FOR_VARIATION() i
		IF CREATE_DROP_OFF_PED(i)
			PRINTLN("[BIKER_SELL] [DROPOFF_PED], CREATE_DROP_OFF_PEDS - Drop off ped ", i, " created.")
		ENDIF
	ENDREPEAT
	BOOL bReturn = TRUE
	REPEAT GET_NUM_DROP_OFF_PEDS_FOR_VARIATION() i
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffPed[i])
			PRINTLN("[BIKER_SELL] [DROPOFF_PED], CREATE_DROP_OFF_PEDS - NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID FALSE ( ", i, ")")
			bReturn = FALSE
		ENDIF
	ENDREPEAT 
	
	RETURN bReturn
ENDFUNC

//FUNC VECTOR DROP_OFF_VEH_POS(BOOL bCloseBy = FALSE)
//
//	VECTOR vReturn 
//	
//	IF IS_DROP_OFF_VEH_OK()
//		vReturn = GET_ENTITY_COORDS(DROP_OFF_VEH_ENT())
//	ENDIF
//	
//	IF bCloseBy
//		vReturn = vReturn + <<3.0, 3.0, 0.0>>
//	ENDIF
//	
//	RETURN vReturn
//ENDFUNC

//FUNC FLOAT DROP_OFF_VEH_HEADING()
//
//	FLOAT fReturn
//	
//	IF IS_DROP_OFF_VEH_OK()
//		fReturn = GET_ENTITY_HEADING(DROP_OFF_VEH_ENT())
//	ENDIF
//	
//	RETURN fReturn
//ENDFUNC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				AI PEDS															║

// Ped deletion

FUNC BOOL DELETE_PEDS()
	INT i
	REPEAT GET_NUM_PEDS_FOR_VARIATION() i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[i].netIndex)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[i].netIndex)
			OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[i].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
			OR (IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[i].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
				IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[i].netIndex)
					//IF NOT BIKER_SELL_EXCHANGE()
					IF NOT BIKER_SELL_DEFAULT()
						NETWORK_FADE_OUT_ENTITY(NET_TO_ENT(serverBD.sPed[i].netIndex), TRUE, TRUE)
						DELETE_NET_ID(serverBD.sPed[i].netIndex)
					ELSE
						CLEANUP_NET_ID(serverBD.sPed[i].netIndex) // For land defend, don't want to delete the peds as they might be right in front of the player
					ENDIF
					SET_PED_STATE(i, ePEDSTATE_CREATE)
					PRINTLN("[BIKER_SELL] [PED], DELETE_PEDS - Ped ", i, " deleted")
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	i = 0
	REPEAT GET_NUM_PEDS_FOR_VARIATION() i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[i].netIndex)
			PRINTLN("[BIKER_SELL] [PED], DELETE_PEDS - Waiting for ped ", i, " to be deleted")
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

// ------------------------------------------------------------------>|
// 		Ped Creation / Behaviour

FUNC BOOL DOES_AI_PED_EXIST(INT iPed)
	RETURN NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iPed].netIndex)
ENDFUNC

FUNC PED_INDEX GET_AI_PED_PED_INDEX(INT iPed)
	IF DOES_AI_PED_EXIST(iPed)
		RETURN NET_TO_PED(serverBD.sPed[iPed].netIndex)
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC ENTITY_INDEX GET_AI_PED_ENTITY_INDEX(INT iPed)
	IF DOES_AI_PED_EXIST(iPed)
		RETURN NET_TO_ENT(serverBD.sPed[iPed].netIndex)
	ENDIF
	RETURN NULL
ENDFUNC

/// PURPOSE:
///    Set the health of the specified ped to 0 if the server says so, and I have control
/// PARAMS:
///    iPed - 
PROC HANDLE_PED_DISTANCE_FROM_SELL_VEH_CLIENT(INT iPed)
	IF IS_BIT_SET(serverBD.iAiPedShouldDieBitset, iPed)
		IF DOES_AI_PED_EXIST(iPed)
			IF NOT IS_PED_INJURED(GET_AI_PED_PED_INDEX(iPed))
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
        		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
					IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
						PRINTLN("[BIKER_SELL] [PED] I'VE SET TO HEALTH OF PED ", iPed, " TO 0")
						SET_ENTITY_HEALTH(GET_AI_PED_ENTITY_INDEX(iPed), 0)
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

FUNC VECTOR vTACO_WINDOW_COORDS(INT iVeh)

	VECTOR vReturn
//	vReturn = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(VEH_POS(iVeh), fVEH_HEADING(iVeh), <<tfRepairY, tfMarkerY, 0.0>>)
	vReturn = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(VEH_POS(iVeh), fVEH_HEADING(iVeh), <<3.0, -2.0, 0.0>>)
	
	IF GET_GROUND_Z_FOR_3D_COORD(vReturn, vReturn.z)
		RETURN vReturn
	ENDIF
	
	RETURN vReturn
ENDFUNC

FUNC PED_INDEX GET_NEAREST_PED_TO_PLAYER(BOOL bIncludeDeadPeds = FALSE)
	INT i
	FLOAT fClosestPed = 9999.99
	FLOAT fTempDist
	PED_INDEX pedToReturn = INT_TO_NATIVE(PED_INDEX, -1)

	IF DOES_VARIATION_HAVE_AI_PEDS()
		REPEAT GET_NUM_PEDS_FOR_VARIATION() i
			IF NOT IS_PED_INJURED(GET_AI_PED_PED_INDEX(i))
			OR bIncludeDeadPeds
				fTempDist = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), GET_AI_PED_PED_INDEX(i))
				IF fTempDist < fClosestPed
					fClosestPed = fTempDist
					pedToReturn = GET_AI_PED_PED_INDEX(i)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN pedToReturn
ENDFUNC

FUNC INT iGET_NEAREST_BAG_TO_PED(INT iPed)

	INT iBags
	FLOAT fClosestPed = 9999.99
	FLOAT fTempDist
	INT iReturn
	PLAYER_INDEX piPlayer 
	IF DOES_AI_PED_EXIST(iPed)
		REPEAT GET_NUM_BAGS() iBags
			piPlayer = INT_TO_PLAYERINDEX(serverBD.iAssignedBag[iBags])
			
			IF serverBD.iAssignedBag[iBags] != -1
			AND NETWORK_IS_PLAYER_ACTIVE(piPlayer)
				fTempDist = GET_DISTANCE_BETWEEN_ENTITIES(GET_PLAYER_PED(piPlayer), GET_AI_PED_PED_INDEX(iPed))
				
				IF fTempDist < fClosestPed
					fClosestPed = fTempDist
					iReturn = iBags
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN iReturn
ENDFUNC

CONST_FLOAT ciPED_SEARCH_DIST 50.00

FUNC FLOAT fPED_SEARCH_DIST()

//	SWITCH GET_SELL_VAR()
//	
//		CASE eBIKER_SELL_TACO_VAN
//			RETURN ( ciPED_SEARCH_DIST * 4 )
//
//	ENDSWITCH

	RETURN ciPED_SEARCH_DIST
ENDFUNC

FUNC VECTOR vPED_DESTINATION(INT iVeh, INT iPed)//, VECTOR vVehCoords)

	FLOAT fPed
	fPed = TO_FLOAT(iPed)
	fPed = ( fPed / 10 )

	SWITCH GET_SELL_VAR()
	
//		CASE eBIKER_SELL_TACO_VAN
//			RETURN vTACO_WINDOW_COORDS(iVeh) + <<0.0, fPed, 0.0>>
			
		CASE eBIKER_SELL_FRIENDS_IN_NEED
			RETURN VEH_POS(iVeh) + <<1.0,1.0,1.0>> //<<tfRepairY, tfMarkerY, 0.0>>

	ENDSWITCH

	RETURN VEH_POS(iVeh)
ENDFUNC

FUNC FLOAT fPED_HEADING(FLOAT fVehHeading)

	SWITCH GET_SELL_VAR()
	
//		CASE eBIKER_SELL_TACO_VAN
//			RETURN ( fVehHeading + 90 )
			
		CASE eBIKER_SELL_FRIENDS_IN_NEED
			RETURN ( fVehHeading + 90 )
		
		BREAK
	ENDSWITCH

	RETURN fVehHeading
ENDFUNC

FUNC VECTOR vPED_DROP_DIMENSIONS()

	SWITCH GET_SELL_VAR()
	
//		CASE eBIKER_SELL_TACO_VAN
//			RETURN 
			
		CASE eBIKER_SELL_FRIENDS_IN_NEED
			RETURN <<1.0,1.0,1.0>>
		
		BREAK
	ENDSWITCH

	RETURN <<2.0, 2.0, 2.0>>
ENDFUNC

FUNC STRING sPOT_SCENARIO()
	RETURN "WORLD_HUMAN_SMOKING_POT"
ENDFUNC

FUNC BOOL bPED_PLAYING_SCENARIO(PED_INDEX pedId)
	IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_START_SCENARIO_IN_PLACE) = PERFORMING_TASK
	OR GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_START_SCENARIO_IN_PLACE) = WAITING_TO_START_TASK
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DO_POT_ANIMS(PED_INDEX pedId)
	IF DOES_ENTITY_EXIST(pedId)
	AND NOT IS_ENTITY_DEAD(pedId)	
		IF (GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_START_SCENARIO_IN_PLACE) != PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_START_SCENARIO_IN_PLACE) != WAITING_TO_START_TASK)
//			SET_ENTITY_COORDS(pedId, vREPAIR_COORDS())
//			SET_ENTITY_HEADING(pedId, ( fVEH_HEADING() -180.00 ))
			TASK_START_SCENARIO_IN_PLACE(pedId, sPOT_SCENARIO(), DEFAULT, FALSE)
//			TASK_START_SCENARIO_AT_POSITION(pedId, "WORLD_HUMAN_WELDING", vREPAIR_COORDS(), ( fVEH_HEADING() -180.00 ))
			PRINTLN("[BIKER_SELL] [PED] - Tasked for scenario DO_POT_ANIMS")
		ENDIF
	ENDIF
ENDPROC

PROC STOP_PED_SCENARIO(PED_INDEX pedId)
	IF DOES_ENTITY_EXIST(pedId)
	AND NOT IS_ENTITY_DEAD(pedId)
		IF (GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_START_SCENARIO_IN_PLACE) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_START_SCENARIO_IN_PLACE) = WAITING_TO_START_TASK)
			CLEAR_PED_TASKS_IMMEDIATELY(pedId)
			PRINTLN("[BIKER_SELL] [PED] - STOP_PED_SCENARIO")
		ENDIF
	ENDIF
ENDPROC

FUNC STRING sGET_PED_SCENARIO(BOOL bPedDelivered)

	STRING sReturn

	IF bPedDelivered
	
		sReturn = "WORLD_HUMAN_CHEERING"
	ELSE

		sLocaldata.iRandScenario = GET_RANDOM_INT_IN_RANGE(0, 10000)
		
		IF IS_INT_IN_RANGE(sLocaldata.iRandScenario, 8000, 10000)
			sReturn = "WORLD_HUMAN_SMOKING"
		ELIF IS_INT_IN_RANGE(sLocaldata.iRandScenario, 6000, 8000)
			sReturn = "WORLD_HUMAN_SMOKING_POT"
		ELIF IS_INT_IN_RANGE(sLocaldata.iRandScenario, 4000, 6000)
			sReturn = "WORLD_HUMAN_DRINKING"
		ELIF IS_INT_IN_RANGE(sLocaldata.iRandScenario, 2000, 4000)
			sReturn = "WORLD_HUMAN_STAND_MOBILE"
		ELSE
			sReturn = "WORLD_HUMAN_TOURIST_MAP"
		ENDIF
	ENDIF
	
	RETURN sReturn
ENDFUNC

PROC GIVE_PED_SCENARIO(PED_INDEX pedId, STRING sScenario, BOOL bForce)
	IF NOT bPED_PLAYING_SCENARIO(pedId)
	OR bForce
		TASK_START_SCENARIO_IN_PLACE(pedId, sScenario, DEFAULT, FALSE)
		PRINTLN("[BIKER_SELL] [PED] GIVE_PED_SCENARIO ", sScenario)
	ENDIF	
ENDPROC

PROC CLIENT_MAINTAIN_DROP_OFF_PED_BODIES()
//	IF BIKER_SELL_EXCHANGE()
//		INT iPed
//		PED_INDEX pedId
//		IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_PED_SHOT_AT)
//			REPEAT GET_NUM_DROP_OFF_PEDS_FOR_VARIATION() iPed
//				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffPed[iPed])
//					pedId = NET_TO_PED(serverBD.niDropOffPed[iPed])
//					IF DOES_ENTITY_EXIST(pedId)
//					AND NOT IS_PED_INJURED(pedId)
//						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedId, PLAYER_PED_ID(), TRUE)
//						OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(pedId)
//							SET_CLIENT_BIT0(eCLIENTBITSET0_SHOT_AT_PEDS)
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDREPEAT
//		ELSE
//			REPEAT GET_NUM_DROP_OFF_PEDS_FOR_VARIATION() iPed
//				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffPed[iPed])
//					pedId = NET_TO_PED(serverBD.niDropOffPed[iPed])
//					IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != PERFORMING_TASK
//					AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != WAITING_TO_START_TASK
//						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niDropOffPed[iPed])
//		        		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.niDropOffPed[iPed]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
//							IF TAKE_CONTROL_OF_NET_ID(serverBD.niDropOffPed[iPed])
//								SET_ATTRIBUTES_FOR_ATTACK( pedId, FALSE, TRUE, GET_PED_ACCURACY_FOR_VARIATION())
//		                    	TASK_COMBAT_HATED_TARGETS_IN_AREA(pedId, GET_ENTITY_COORDS(pedId), 299.9)
//								PRINTLN("[BIKER_SELL] [PED], CLIENT_MAINTAIN_DROP_OFF_PED_BODIES - Ped ", iPed, " tasked with TASK_COMBAT_HATED_TARGETS_IN_AREA.")
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDREPEAT
//		ENDIF
//	ENDIF
ENDPROC



FUNC PED_INDEX GET_RAND_PLAYER_TO_ATTACK()
	INT iBags
	PED_INDEX piReturn
	REPEAT GET_NUM_BAGS() iBags
		IF serverBD.iAssignedBag[iBags] != -1
			piReturn = GET_PLAYER_PED(INT_TO_PLAYERINDEX(serverBD.iAssignedBag[iBags]))
			IF DOES_ENTITY_EXIST(piReturn)
				RETURN piReturn
			ENDIF 	
		ENDIF
	ENDREPEAT
	RETURN piReturn
ENDFUNC

FUNC VEHICLE_INDEX GET_RAND_PLAYER_VEHILCE_TO_ATTACK()
	INT iBags
	PED_INDEX piPed
	REPEAT GET_NUM_BAGS() iBags
		IF serverBD.iAssignedBag[iBags] != -1
			piPed = GET_PLAYER_PED(INT_TO_PLAYERINDEX(serverBD.iAssignedBag[iBags]))
			IF IS_PED_IN_ANY_VEHICLE(piPed)
			AND DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(piPed))
				RETURN GET_VEHICLE_PED_IS_IN(piPed)	
			ENDIF 	
		ENDIF
	ENDREPEAT
	RETURN NULL
ENDFUNC
/// PURPOSE:
///    Clients process the bodies of the AI peds
PROC CLIENT_MAINTAIN_PED_BODIES()

	PED_INDEX pedId//, pedClosest
	VECTOR vFleeCoords//, vVehCoords
	INT iClosestVeh
	VEHICLE_INDEX pedVeh
	//ENTITY_INDEX attackEntity
//	FLOAT fVehHeading//, fDistBetweenEntities
	IF DOES_VARIATION_HAVE_AI_PEDS()
		
		INT iPed
		REPEAT GET_NUM_PEDS_FOR_VARIATION() iPed
		
			SWITCH GET_PED_STATE(iPed)
				CASE ePEDSTATE_INACTIVE
				BREAK
				
				// In this state whilst being created
				CASE ePEDSTATE_CREATE
					
				BREAK
				
				// Come in here to stand idly
				CASE ePEDSTATE_GUARD_CONTRABAND
					IF DOES_AI_PED_EXIST(iPed)
						IF BIKER_SELL_STING()
							IF BIKER_SELL_HAS_TWIST_HAPPENED()
								IF iPed = 0
									SET_PED_STATE(iPed, ePEDSTATE_FLEE_ON_FOOT)
								ELSE
									SET_PED_STATE(iPed, ePEDSTATE_ATTACK_HATED_TARGETS)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ePEDSTATE_FRIENDLY_STONED
					IF DOES_AI_PED_EXIST(iPed)
						pedID = GET_AI_PED_PED_INDEX(iPed)
						
						IF NOT IS_ENTITY_DEAD(pedID)
						
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
		            		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
							
								IF IS_PED_IN_ANY_VEHICLE(pedID)
								
									pedVeh = GET_VEHICLE_PED_IS_IN(pedID)
									
									IF DOES_ENTITY_EXIST(pedVeh)
									AND NOT IS_ENTITY_DEAD(pedVeh)
										IF IS_ENTITY_IN_WATER(pedVeh)
								
											IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
											
												SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, FALSE)
												TASK_LEAVE_ANY_VEHICLE(pedId)

												PRINTLN("[BIKER_SELL] [PED], CLIENT_MAINTAIN_PED_BODIES, ePEDSTATE_FRIENDLY_STONED, - Ped ", iPed, " tasked with TASK_LEAVE_ANY_VEHICLE.")

											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
						
				// Chase any vehicles/people with contraband
				CASE ePEDSTATE_CHASE_CONTRABAND
					IF DOES_AI_PED_EXIST(iPed)
						pedID = GET_AI_PED_PED_INDEX(iPed)
						
						//IF BIKER_SELL_EXCHANGE()
						IF BIKER_SELL_DEFAULT()
							HANDLE_PED_DISTANCE_FROM_SELL_VEH_CLIENT(iPed)
						ENDIF
						
						IF NOT IS_ENTITY_DEAD(pedID)
							IF GET_SCRIPT_TASK_STATUS(pedID, SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(pedID, SCRIPT_TASK_VEHICLE_MISSION) != WAITING_TO_START_TASK
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
			            		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
									IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
									AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_NET_VEHICLE_PED_SHOULD_BE_IN(iPed))	
										pedVeh = NET_TO_VEH(GET_NET_VEHICLE_PED_SHOULD_BE_IN(iPed))
										
										IF DOES_ENTITY_EXIST(pedVeh)
										AND DOES_ENTITY_EXIST(VEH_ID(serverBD.iRandVehToAttack))
											IF IS_ENTITY_ALIVE(pedVeh)
											AND IS_ENTITY_ALIVE(VEH_ID(serverBD.iRandVehToAttack))
												SET_ATTRIBUTES_FOR_CHASE( pedId, GET_PED_ACCURACY_FOR_VARIATION())
												TASK_VEHICLE_MISSION(pedId, pedVeh, VEH_ID(serverBD.iRandVehToAttack), MISSION_FOLLOW, 50.0, DRIVINGMODE_AVOIDCARS, -1, -1)
												PRINTLN("[BIKER_SELL] [PED], CLIENT_MAINTAIN_PED_BODIES - Ped ", iPed, " tasked with TASK_VEHICLE_MISSION.")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								//IF BIKER_SELL_EXCHANGE()
								IF BIKER_SELL_DEFAULT()
									IF NOT IS_PED_DOING_DRIVEBY(pedId)
										IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
			            				OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
											IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
											AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_NET_VEHICLE_PED_SHOULD_BE_IN(iPed))	
												pedVeh = NET_TO_VEH(GET_NET_VEHICLE_PED_SHOULD_BE_IN(iPed))
											
												IF DOES_ENTITY_EXIST(pedVeh)
												AND DOES_ENTITY_EXIST(VEH_ID(serverBD.iRandVehToAttack))
													IF IS_ENTITY_ALIVE(pedVeh)
													AND IS_ENTITY_ALIVE(VEH_ID(serverBD.iRandVehToAttack))
														TASK_DRIVE_BY(pedId, NULL, VEH_ID(serverBD.iRandVehToAttack), <<0.0,0.0,0.0>>, -1, 30, TRUE)
														PRINTLN("[BIKER_SELL] [PED], CLIENT_MAINTAIN_PED_BODIES - Ped ", iPed, " tasked with TASK_DRIVE_BY.")
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ePEDSTATE_CHASE_CONTRABAND_AND_ATTACK
					// Handle the ped task
					IF DOES_AI_PED_EXIST(iPed)
						pedID = GET_AI_PED_PED_INDEX(iPed) 
				
						IF NOT IS_ENTITY_DEAD(pedID)
							IF NOT IS_PED_IN_COMBAT(pedID)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedID, 299.99)
							ENDIF
						ENDIF
						HANDLE_PED_DISTANCE_FROM_SELL_VEH_CLIENT(iPed)
					ENDIF
				
				BREAK
				
				// Attack enemies nearby
				CASE ePEDSTATE_ATTACK_HATED_TARGETS
					IF DOES_AI_PED_EXIST(iPed)
						pedId = GET_AI_PED_PED_INDEX(iPed)
						
						//IF BIKER_SELL_EXCHANGE()
						IF BIKER_SELL_DEFAULT()
							HANDLE_PED_DISTANCE_FROM_SELL_VEH_CLIENT(iPed)
						ENDIF
						IF NOT IS_ENTITY_DEAD(pedId)
						//	IF BIKER_SELL_EXCHANGE()
							IF BIKER_SELL_DEFAULT()
								IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != WAITING_TO_START_TASK
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
				            		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
										IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
											SET_ATTRIBUTES_FOR_ATTACK( pedId, FALSE, TRUE, GET_PED_ACCURACY_FOR_VARIATION())
				                        	TASK_COMBAT_HATED_TARGETS_IN_AREA(pedId, GET_ENTITY_COORDS(pedId), 299.9)
											PRINTLN("[BIKER_SELL] [PED], CLIENT_MAINTAIN_PED_BODIES - Ped ", iPed, " tasked with TASK_COMBAT_HATED_TARGETS_IN_AREA.")
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != WAITING_TO_START_TASK
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
				            		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
										IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
											SET_ATTRIBUTES_FOR_ATTACK( pedId, TRUE, TRUE, GET_PED_ACCURACY_FOR_VARIATION())
				                        	TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedID, 299.9)
											PRINTLN("[BIKER_SELL] [PED], CLIENT_MAINTAIN_PED_BODIES - Ped ", iPed, " tasked with TASK_COMBAT_HATED_TARGETS_AROUND_PED.")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
	                    ENDIF
					ENDIF
				BREAK
				
				CASE ePEDSTATE_EXIT_VEH
					IF DOES_AI_PED_EXIST(iPed)
						pedId = GET_AI_PED_PED_INDEX(iPed)
						
						IF NOT IS_ENTITY_DEAD(pedId)
							IF (GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
			            		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
									IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, TRUE)
										TASK_LEAVE_ANY_VEHICLE(pedId)
										PRINTLN("[BIKER_SELL] [PED], CLIENT_MAINTAIN_PED_BODIES - Ped ", iPed, " tasked with TASK_LEAVE_ANY_VEHICLE.")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
						
				BREAK
				// Peds run away
				CASE ePEDSTATE_FLEE_ON_FOOT
					IF DOES_AI_PED_EXIST(iPed)
						pedId = GET_AI_PED_PED_INDEX(iPed)
						
						IF NOT IS_ENTITY_DEAD(pedId)
							IF (GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_SMART_FLEE_POINT) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_SMART_FLEE_POINT) != WAITING_TO_START_TASK)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
			            		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
									IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
										TASK_SMART_FLEE_COORD(pedId, GET_ENTITY_COORDS(pedId), 10000.0, 999999)
										SET_PED_FLEE_ATTRIBUTES(pedId, FA_UPDATE_TO_NEAREST_HATED_PED, TRUE)
										SET_PED_FLEE_ATTRIBUTES(pedId, FA_USE_VEHICLE, FALSE)
										PRINTLN("[BIKER_SELL] [PED], CLIENT_MAINTAIN_PED_BODIES - Ped ", iPed, " tasked with TASK_SMART_FLEE_COORD.")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ePEDSTATE_FLEE_IN_VEH
					IF DOES_AI_PED_EXIST(iPed)
						pedId = GET_AI_PED_PED_INDEX(iPed)
						
						IF NOT IS_ENTITY_DEAD(pedId)
							IF (GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_SMART_FLEE_POINT) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_SMART_FLEE_POINT) != WAITING_TO_START_TASK)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
			            		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
									IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
										TASK_SMART_FLEE_COORD(pedId, GET_ENTITY_COORDS(pedId), 10000.0, 999999)
										SET_PED_FLEE_ATTRIBUTES(pedId, FA_UPDATE_TO_NEAREST_HATED_PED, TRUE)
										SET_PED_FLEE_ATTRIBUTES(pedId, FA_USE_VEHICLE, FALSE)
										PRINTLN("[BIKER_SELL] [PED], CLIENT_MAINTAIN_PED_BODIES - Ped ", iPed, " tasked with TASK_SMART_FLEE_COORD.")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				// Flee drop-off
				CASE ePEDSTATE_FLEE_DROP_OFF
					IF DOES_AI_PED_EXIST(iPed)
						pedId = NET_TO_PED(serverBD.sPed[iPed].netIndex)
						
						IF NOT IS_ENTITY_DEAD(pedId)
							IF (GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_SMART_FLEE_POINT) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_SMART_FLEE_POINT) != WAITING_TO_START_TASK)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
			            		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
									IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
										GET_CLOSEST_SELL_VEH_TO_PED(iPed, iClosestVeh)
										IF iClosestVeh <> -1
											vFleeCoords = GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[iClosestVeh], serverBD.iRoute, iClosestVeh)
											IF NOT ARE_VECTORS_EQUAL(vFleeCoords, << 0.0, 0.0, 0.0 >>)
												SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, TRUE)
												SET_PED_COMBAT_ATTRIBUTES(pedId, CA_ALWAYS_FIGHT, FALSE)
												SET_PED_COMBAT_ATTRIBUTES(pedId, CA_ALWAYS_FLEE, TRUE)
												SET_PED_FLEE_ATTRIBUTES(pedId, FA_DISABLE_COWER,TRUE)
												SET_PED_FLEE_ATTRIBUTES(pedId,  FA_COWER_INSTEAD_OF_FLEE,FALSE)
												TASK_SMART_FLEE_COORD(pedId, vFleeCoords, 10000.0, 999999)
												PRINTLN("[BIKER_SELL] [PED], CLIENT_MAINTAIN_PED_BODIES - Ped ", iPed, " tasked with TASK_SMART_FLEE_COORD (ePEDSTATE_FLEE_DROP_OFF) from coord ", vFleeCoords)
											ELSE
												PRINTLN("[BIKER_SELL] [PED], CLIENT_MAINTAIN_PED_BODIES - Ped ", iPed, " can't flee because flee coords are zero! serverBD.iClosestDropOff = ", serverBD.iClosestDropOff[iClosestVeh])
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							HANDLE_PED_DISTANCE_FROM_SELL_VEH_CLIENT(iPed)
						ENDIF
					ENDIF
				BREAK
				
				// Peds drive to a specified location
				CASE ePEDSTATE_DRIVE_TO_POINT
					IF DOES_AI_PED_EXIST(iPed)
						
					ENDIF
				BREAK
				
				CASE ePEDSTATE_RELEASE_TO_AMBIENT_POPULATION
					IF DOES_AI_PED_EXIST(iPed)
						pedId = NET_TO_PED(serverBD.sPed[iPed].netIndex)
							
						IF NOT IS_ENTITY_DEAD(pedId)
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
		            		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
								IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
								
									SET_PED_CONFIG_FLAG(pedId, PCF_PhoneDisableCameraAnimations, TRUE)
									SET_PED_CONFIG_FLAG(pedId, PCF_DisableShockingEvents, TRUE)
									PRINTLN("[BIKER_SELL] [PED], CLIENT_MAINTAIN_PED_BODIES - Ped ", iPed, " ePEDSTATE_RELEASE_TO_AMBIENT_POPULATION")
								
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, FALSE)
									SET_PED_KEEP_TASK(pedId, TRUE)
									SET_PED_DIES_WHEN_INJURED(pedId, TRUE)
									CLEANUP_NET_ID(serverBD.sPed[iPed].netIndex)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				// Peds parachute when falling
				CASE ePEDSTATE_PARACHUTE
					IF DOES_AI_PED_EXIST(iPed)
					
					ENDIF
				BREAK
				
				// Move here when dead
				CASE ePEDSTATE_DEAD
				BREAK
			
				// Reset the peds here	
				CASE ePEDSTATE_RESET
				BREAK
			ENDSWITCH
		ENDREPEAT
	ENDIF
	
	// Make the peds leave their boats in border patrol of the boat becomes capsized
	IF DOES_MODE_NEED_DROP_OFF_PED()
	AND BIKER_SELL_BORDER_PATROL()
		INT i
		PED_INDEX ped
		VEHICLE_INDEX veh
		
		REPEAT GET_NUM_DROP_OFF_PEDS_FOR_VARIATION() i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffPed[i])
				ped = NET_TO_PED(serverBD.niDropOffPed[i])
			
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_DROP_OFF_VEH_PED_SHOULD_BE_IN(i))
					veh = NET_TO_VEH(GET_DROP_OFF_VEH_PED_SHOULD_BE_IN(i))
					
					IF IS_PED_IN_THIS_VEHICLE(ped, veh)
						IF GET_IS_BOAT_CAPSIZED(veh)
							IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_LEAVE_VEHICLE) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_LEAVE_VEHICLE) != WAITING_TO_START_TASK
								IF NETWORK_HAS_CONTROL_OF_ENTITY(ped)
								OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.niDropOffPed[i]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
									IF TAKE_CONTROL_OF_ENTITY(ped)
										SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niDropOffPed[i]), CA_LEAVE_VEHICLES, TRUE)
										TASK_LEAVE_VEHICLE(ped, veh)
										PRINTLN("[BIKER_SELL] - [PED], CLIENT_MAINTAIN_PED_BODIES - Ped ", i, " tasked with TASK_LEAVE_VEHICLE as their boat has capsized")
									ENDIF
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[PED], CLIENT_MAINTAIN_PED_BODIES - Boat ", NATIVE_TO_INT(veh), ", being driven by ped ", i," is not capsized")
						ENDIF
					ELSE
						PRINTLN("[PED], CLIENT_MAINTAIN_PED_BODIES - Ped ", i, " is not in a vehicle")
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

FUNC FLOAT GET_MAXIMUM_DISTANCE_FOR_ENTITY_FORCE_CLEANUP()
	RETURN 800.0
ENDFUNC

FUNC FLOAT GET_MINIMUM_DISTANCE_FOR_TIMED_ENTITY_CLEANUP()
	RETURN 600.0
ENDFUNC

FUNC PED_INDEX GET_CLOSEST_PLAYER_TO_ATTACK(INT iPed, INT &iVeh)
	INT iBags
	PED_INDEX piReturn
	PED_INDEX piTemp
	FLOAT fClosestveh = 9999.99
	FLOAT fTempDist
	REPEAT GET_NUM_BAGS() iBags
		IF serverBD.iAssignedBag[iBags] != -1
			piTemp = GET_PLAYER_PED(INT_TO_PLAYERINDEX(serverBD.iAssignedBag[iBags]))
			IF DOES_ENTITY_EXIST(piTemp)
				fTempDist = GET_DISTANCE_BETWEEN_ENTITIES(NET_TO_PED(serverBD.sPed[iPed].netIndex), piTemp)
				IF fTempDist < fClosestveh
					fClosestveh = fTempDist
					piReturn = piTemp
					iVeh = iBags
				ENDIF
			ENDIF 	
		ENDIF
	ENDREPEAT
	RETURN piReturn
ENDFUNC

PROC HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER(INT iPed)
	FLOAT fMaxCleanupDist = GET_MAXIMUM_DISTANCE_FOR_ENTITY_FORCE_CLEANUP()
	FLOAT fTimedCleanupDist = GET_MINIMUM_DISTANCE_FOR_TIMED_ENTITY_CLEANUP()
	INT iVeh
	
	ENTITY_INDEX veh
	IF BIKER_SELL_CLUB_RUN()
		veh = GET_CLOSEST_PLAYER_TO_ATTACK(iPed, iVeh)
	ELSE
		veh = GET_CLOSEST_SELL_VEH_TO_PED(iPed, iVeh)
	ENDIF
	IF DOES_ENTITY_EXIST(veh)
		//PRINTLN("[BIKER_SELL] [PED], HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER - Ped ", iPed, " DOES_ENTITY_EXIST(veh)")
		IF NOT IS_BIT_SET(serverBD.iAiPedShouldDieBitset, iPed)
			IF GET_DISTANCE_BETWEEN_ENTITIES(GET_AI_PED_ENTITY_INDEX(iPed), veh) > fMaxCleanupDist
				PRINTLN("[BIKER_SELL] [PED], HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER - Ped ", iPed, " is > fMaxCleanupDist from closest sell veh, killing ped immediately, fMaxCleanupDist = ", fMaxCleanupDist)
				//-- Dave W - I think you need to have control to set a ped's health. Set a bit instead and get whoever has control to set the health
				//-- (See HANDLE_PED_DISTANCE_FROM_SELL_VEH_CLIENT)
				
			//	SET_ENTITY_HEALTH(GET_AI_PED_ENTITY_INDEX(iPed), 0)
				SET_BIT(serverBD.iAiPedShouldDieBitset, iPed)
			ELSE
				IF GET_DISTANCE_BETWEEN_ENTITIES(GET_AI_PED_ENTITY_INDEX(iPed), veh) > fTimedCleanupDist
					IF NOT HAS_NET_TIMER_STARTED(serverBD.sPed[iPed].stOutOfRangeTimer)
						PRINTLN("[BIKER_SELL] [PED], HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER - Ped ", iPed, " is > fTimedCleanupDist from sell veh, starting ped out of range timer, fTimedCleanupDist = ", fTimedCleanupDist)
						START_NET_TIMER(serverBD.sPed[iPed].stOutOfRangeTimer)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(serverBD.sPed[iPed].stOutOfRangeTimer, GET_MODE_TUNEABLE_PED_OUT_OF_RANGE_TIME())
							PRINTLN("[BIKER_SELL] [PED], HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER - Ped ", iPed, " out of range timer expired, killing ped immediately")
						//	SET_ENTITY_HEALTH(GET_AI_PED_ENTITY_INDEX(iPed), 0)
							SET_BIT(serverBD.iAiPedShouldDieBitset, iPed)
						ENDIF
					ENDIF
				ELSE
					IF HAS_NET_TIMER_STARTED(serverBD.sPed[iPed].stOutOfRangeTimer)
						PRINTLN("[BIKER_SELL] [PED], HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER - Ped ", iPed, " is within fTimedCleanupDist from sell veh again, resetting ped out of range timer, fTimedCleanupDist = ", fTimedCleanupDist)
						RESET_NET_TIMER(serverBD.sPed[iPed].stOutOfRangeTimer)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//-- Flee dropoff
		VECTOR vDropOffCoord
		IF BIKER_SELL_CLUB_RUN()
		OR BIKER_SELL_DEFAULT()
			//PRINTLN("[BIKER_SELL] [PED], HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER - Ped ", iPed, " iAiPedShouldDieBitset(veh)")
			IF NOT IS_BIT_SET(serverBD.iAiPedShouldDieBitset, iPed) 
				//PRINTLN("[BIKER_SELL] [PED], HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER - Ped ", iPed, " iAiPedShouldFleeBitset(veh)")
				IF NOT IS_BIT_SET(serverBD.iAiPedShouldFleeBitset, iPed)
					//PRINTLN("[BIKER_SELL] [PED], HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER - serverBD.iClosestDropOff[", iVeh, "] = ", serverBD.iClosestDropOff[iVeh])
					IF serverBD.iClosestDropOff[iVeh] >= 0
						vDropOffCoord =  GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[iVeh], serverBD.iRoute, iVeh)
						//PRINTLN("[BIKER_SELL] [PED], HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER - vDropOffCoord = ", vDropOffCoord)
						FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(GET_AI_PED_ENTITY_INDEX(iPed), FALSE), vDropOffCoord)
						//PRINTLN("[BIKER_SELL] [PED], HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER - fDist = ", fDist)
						IF fDist < 100
							SET_BIT(serverBD.iAiPedShouldFleeBitset, iPed)
							PRINTLN("[BIKER_SELL] [PED], HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER - Ped ", iPed, " set to flee as they are this distance from drop off ", fDist, ", drop-off coord ", vDropOffCoord, " , closest drop-off = ", serverBD.iClosestDropOff[iVeh]) 
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Server processes the brains of the AI peds
PROC SERVER_MAINTAIN_PED_BRAINS()
	
	IF DOES_VARIATION_HAVE_AI_PEDS()
		INT iPed
		INT iBag
		REPEAT GET_NUM_PEDS_FOR_VARIATION() iPed
			SWITCH GET_PED_STATE(iPed)
				CASE ePEDSTATE_INACTIVE
				BREAK
				
				// In this state whilst being created
				CASE ePEDSTATE_CREATE
					IF DOES_AI_PED_EXIST(iPed)
						IF BIKER_SELL_PROVEN()
							SET_PED_STATE(iPed, ePEDSTATE_FRIENDLY_STONED)
						ELIF BIKER_SELL_CLUB_RUN()
							SET_PED_STATE(iPed, ePEDSTATE_CHASE_CONTRABAND_AND_ATTACK)
						ELSE
							SET_PED_STATE(iPed, ePEDSTATE_GUARD_CONTRABAND)
						ENDIF
					ENDIF
				BREAK
				
				CASE ePEDSTATE_DO_SCENARIOS
					IF bMODE_HAS_BUYER_PEDS()
						IF DOES_AI_PED_EXIST(iPed)
							IF IS_ENTITY_DEAD(GET_AI_PED_PED_INDEX(iPed))
								SET_PED_STATE(iPed, ePEDSTATE_DEAD)
							ELSE				
								IF IS_BIT_SET(serverBD.iPedDeliveredBitSet, iPed)
									SET_PED_STATE(iPed, ePEDSTATE_RELEASE_TO_AMBIENT_POPULATION)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ePEDSTATE_FRIENDLY_STONED
//					IF DOES_AI_PED_EXIST(iPed)
//						SET_PED_STATE(iPed, ePEDSTATE_FRIENDLY_STONED)
//					ENDIF
				BREAK
				
				// Come in here to stand idly and wait for spooking or other instruction
				CASE ePEDSTATE_GUARD_CONTRABAND
					IF DOES_AI_PED_EXIST(iPed)
						SWITCH GET_SELL_VAR()
						
							//CASE eBIKER_SELL_EXCHANGE
							CASE eBIKER_SELL_DEFAULT
							CASE eBIKER_SELL_FRIENDS_IN_NEED
								IF NOT IS_ENTITY_DEAD(GET_AI_PED_PED_INDEX(iPed))
									PRINTLN("[BIKER_SELL] [PED], SERVER_MAINTAIN_PED_BRAINS, eBIKER_SELL_THIRD_PARTY / eVAR_LAND_DEFEND - Ped ", iPed, " should moving to new state as they have been created")
									SET_PED_STATE(iPed, GET_PED_AI_STATE_FOR_VARIATION( iPed))
								ELSE
									SET_PED_STATE(iPed, ePEDSTATE_DEAD)
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				
				// Chase any vehicles/people with contraband
				CASE ePEDSTATE_CHASE_CONTRABAND
					IF DOES_AI_PED_EXIST(iPed)
						IF IS_ENTITY_DEAD(GET_AI_PED_ENTITY_INDEX(iPed))
							PRINTLN("[BIKER_SELL] [PED], SERVER_MAINTAIN_PED_BRAINS - Ped ", iPed, " has died, moving to dead ped state")
							SET_PED_STATE(iPed, ePEDSTATE_DEAD)
						ELSE
							//IF BIKER_SELL_EXCHANGE()
							IF BIKER_SELL_DEFAULT()
								HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER(iPed)
							ENDIF
							IF IS_BIT_SET(serverBD.iAiPedShouldFleeBitset, iPed)
								SET_PED_STATE(iPed, ePEDSTATE_FLEE_DROP_OFF)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ePEDSTATE_CHASE_CONTRABAND_AND_ATTACK
					IF DOES_AI_PED_EXIST(iPed)
						IF IS_ENTITY_DEAD(GET_AI_PED_ENTITY_INDEX(iPed))
							PRINTLN("[BIKER_SELL] [PED], SERVER_MAINTAIN_PED_BRAINS - Ped ", iPed, " has died, moving to dead ped state")
							SET_PED_STATE(iPed, ePEDSTATE_DEAD)
						ELSE
							IF BIKER_SELL_CLUB_RUN()
							OR BIKER_SELL_DEFAULT()
								HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER(iPed)
							ENDIF														
							IF IS_BIT_SET(serverBD.iAiPedShouldFleeBitset, iPed)
								SET_PED_STATE(iPed, ePEDSTATE_FLEE_DROP_OFF)
							ENDIF
							IF BIKER_SELL_CLUB_RUN()
							AND HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
								SET_PED_STATE(iPed, ePEDSTATE_RELEASE_TO_AMBIENT_POPULATION)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				// Attack enemies nearby
				CASE ePEDSTATE_ATTACK_HATED_TARGETS
					IF DOES_AI_PED_EXIST(iPed)
						IF IS_ENTITY_DEAD(GET_AI_PED_ENTITY_INDEX(iPed))
							PRINTLN("[BIKER_SELL] [PED], SERVER_MAINTAIN_PED_BRAINS - Ped ", iPed, " has died, moving to dead ped state")
							SET_PED_STATE(iPed, ePEDSTATE_DEAD)
						ELSE
							//IF BIKER_SELL_EXCHANGE()
							IF BIKER_SELL_DEFAULT()
								HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER(iPed)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				// Peds run away
				CASE ePEDSTATE_FLEE_ON_FOOT
				BREAK
				
				CASE ePEDSTATE_FLEE_IN_VEH
				BREAK
				
				CASE ePEDSTATE_EXIT_VEH
					
				BREAK
				
				// Avoid getting too close to drop-off
				CASE ePEDSTATE_FLEE_DROP_OFF
					IF DOES_AI_PED_EXIST(iPed)
						IF IS_ENTITY_DEAD(GET_AI_PED_ENTITY_INDEX(iPed))
							PRINTLN("[BIKER_SELL] [PED], SERVER_MAINTAIN_PED_BRAINS - Ped ", iPed, " has died, moving to dead ped state")
							SET_PED_STATE(iPed, ePEDSTATE_DEAD)
						ELSE
							IF GET_MODE_STATE() > eMODESTATE_RUN
								PRINTLN("[BIKER_SELL] [PED], SERVER_MAINTAIN_PED_BRAINS - Ped ", iPed, " moving to release to ambient population state")
								SET_PED_STATE(iPed, ePEDSTATE_RELEASE_TO_AMBIENT_POPULATION)
							ENDIF
							
							HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER(iPed)
						ENDIF
					ENDIF
				BREAK
				
				// Peds drive to a specified location
				CASE ePEDSTATE_DRIVE_TO_POINT
				BREAK
				
				// Peds parachute when falling
				CASE ePEDSTATE_PARACHUTE
				BREAK
				
				// Move here when dead
				CASE ePEDSTATE_DEAD
					IF BIKER_SELL_FRIENDS_IN_NEED()
//					OR BIKER_SELL_TACO_VAN()
					
						iBag = iGET_NEAREST_BAG_TO_PED(iPed)
						
						PRINTLN("[BIKER_SELL] [PED], SERVER_MAINTAIN_PED_BRAINS, SERVER_CHECK_FOR_LOST_PRODUCT, iBag ", iBag)
						
						IF iBag <> -1
							SERVER_INCREASE_DROP_PENALTY()
							serverBD.iBagPenalty[iBag]+=1
							SERVER_CLEAR_ACTIVE_DROP(iBag, serverBD.iClosestDropOff[iBag])
							CLEANUP_NET_ID(serverBD.sPed[iPed].netIndex)
							SET_PED_STATE(iPed, ePEDSTATE_RELEASE_TO_AMBIENT_POPULATION)
							PRINTLN("[BIKER_SELL] [PED], SERVER_MAINTAIN_PED_BRAINS, SERVER_CHECK_FOR_LOST_PRODUCT, CLEANUP_NET_ID ", iPed)
						ENDIF
					ENDIF	
				BREAK
			
				// Reset the peds here	
				CASE ePEDSTATE_RESET
				BREAK
			ENDSWITCH
		ENDREPEAT
	ENDIF
ENDPROC

/// PURPOSE:
///    Add all conditions for spooking/alerting AI peds here
PROC MAINTAIN_CLIENT_SPOOK_CHECKS()
	IF DOES_VARIATION_HAVE_AI_PEDS()
	AND NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_SEA_DEFEND_SPOOKED_PEDS)
		INT i
		FLOAT fDistance 
		FLOAT fDistance1 = 50
		FLOAT fDistance2 = 100
		PED_INDEX closestPed
		SWITCH GET_SELL_VAR()
			CASE eBIKER_SELL_STING_OP
				REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
						IF IS_SELL_VEH_OK(i)
							closestPed = GET_NEAREST_PED_TO_PLAYER()
							IF NATIVE_TO_INT(closestPed) != -1
								fDistance  = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), closestPed)
							ELSE
								fDistance  = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), VEH_ID(i))
							ENDIF
							IF fDistance < fDistance1
								IF IS_PED_SHOOTING(PLAYER_PED_ID())
									PRINTLN("[BIKER_SELL] [PLAYER], MAINTAIN_CLIENT_SPOOK_CHECKS - Local player is within 50m of sell veh and is shooting, spooking peds")
									SET_CLIENT_BIT0(eCLIENTBITSET0_SEA_DEFEND_SPOOKED_PEDS)
								ENDIF	
							ELIF fDistance < fDistance2
								IF IS_PED_SHOOTING(PLAYER_PED_ID())
								AND NOT IS_PED_CURRENT_WEAPON_SILENCED(PLAYER_PED_ID())
									PRINTLN("[BIKER_SELL] [PLAYER], MAINTAIN_CLIENT_SPOOK_CHECKS - Local player is within 100m of sell veh and is shooting a non-silenced weapon, spooking peds")
									SET_CLIENT_BIT0(eCLIENTBITSET0_SEA_DEFEND_SPOOKED_PEDS)
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

// Ped Creation
FUNC BOOL CREATE_AI_PED(INT iPed, BOOL bInitialSpawn = FALSE)
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_15 tl15Name
	#ENDIF
	STRING stTask 
	PED_INDEX pedId
	PRINTLN("[BIKER_SELL] [TWIST] CREATE_AI_PED")
	IF REQUEST_LOAD_MODEL(GET_PED_MODEL_FOR_VARIATION( iPed))	
		IF NOT DOES_AI_PED_EXIST(iPed)
			PRINTLN("[BIKER_SELL] [TWIST] DOES_AI_PED_EXIST not")
			IF SHOULD_AI_PED_SPAWN_IN_VEHICLE(iPed)
				PRINTLN("[BIKER_SELL] [TWIST] SHOULD_AI_PED_SPAWN_IN_VEHICLE")
				// Create peds in vehicle logic here
				NETWORK_INDEX vehToEnter
				vehToEnter = GET_NET_VEHICLE_PED_SHOULD_BE_IN( iPed)
				
				VEHICLE_SEAT thisSeat
				thisSeat = GET_VEHICLE_SEAT_PED_SHOULD_BE_IN(iPed)
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(vehToEnter)
					PRINTLN("[BIKER_SELL] [TWIST] NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID")
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(vehToEnter)
        			OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(vehToEnter) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
						PRINTLN("[BIKER_SELL] [TWIST] NETWORK_HAS_CONTROL_OF_NETWORK_ID")
						IF TAKE_CONTROL_OF_NET_ID(vehToEnter)
							PRINTLN("[BIKER_SELL] [TWIST] TAKE_CONTROL_OF_NET_ID")
							IF CREATE_NET_PED_IN_VEHICLE(serverBD.sPed[iPed].netIndex, vehToEnter, PEDTYPE_MISSION, GET_PED_MODEL_FOR_VARIATION( iPed), thisSeat)
								pedId = NET_TO_PED(serverBD.sPed[iPed].netIndex)
								PRINTLN("[BIKER_SELL] [PED] CREATE_AI_PED, Ped ", iPed, " created in vehicle ", NATIVE_TO_INT(vehToEnter), " in seat ", GET_VEHICLE_SEAT_NAME(thisSeat))
								CLEAR_BIT(serverBD.iAiPedShouldDieBitset, iPed)
								serverBD.sPed[iPed].vSpawnCoord = <<0.0, 0.0, 0.0>>
								serverBD.sPed[iPed].fSpawnHeading = 0.0
								RESET_NET_TIMER(serverBD.sPed[iPed].stOutOfRangeTimer)
								serverBD.iAIPedsMade++
								#IF IS_DEBUG_BUILD
								tl15Name = "PED " 
								tl15Name += iPed
								SET_PED_NAME_DEBUG(pedId, tl15Name)
								#ENDIF
								IF BIKER_SELL_STING()
									stTask = GET_DROP_OFF_PED_SECANARIO(iPed)
									IF NOT IS_STRING_NULL_OR_EMPTY(stTask)
										TASK_START_SCENARIO_IN_PLACE(pedId, stTask, DEFAULT, FALSE)
									ENDIF
									SET_ATTRIBUTES_FOR_COP(pedId)
								ELIF BIKER_SELL_CLUB_RUN()
									SET_ATTRIBUTES_FOR_CHASE(pedID)
									SET_PED_STATE(iPed, ePEDSTATE_CHASE_CONTRABAND_AND_ATTACK)
								ENDIF
								IF BIKER_SELL_PROVEN()
									SET_ATTRIBUTES_FOR_FRIEND(pedID)
									SET_PED_CONFIG_FLAG(pedId, PCF_PlayersDontDragMeOutOfCar, TRUE)
									SET_PED_CAN_PLAY_IN_CAR_IDLES(pedId, TRUE)
									SET_PED_CAN_BE_TARGETTED(pedId, FALSE)
									SET_AMBIENT_VOICE_NAME(pedId, "A_M_Y_Hippy_01_white_FULL_01")
									SET_PED_TREATED_AS_FRIENDLY(pedId, TRUE)
								ENDIF								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF bInitialSpawn
					VECTOR vPos
					vPos = GET_AI_PED_INITIAL_SPAWN_COORD(iPed)
					IF CREATE_NET_PED(serverBD.sPed[iPed].netIndex, PEDTYPE_MISSION, GET_PED_MODEL_FOR_VARIATION(iPed), vPos, GET_AI_PED_INITIAL_SPAWN_HEADING(iPed))
						pedId = NET_TO_PED(serverBD.sPed[iPed].netIndex)
						CLEAR_BIT(serverBD.iAiPedShouldDieBitset, iPed)
						serverBD.sPed[iPed].vSpawnCoord = <<0.0, 0.0, 0.0>>
						serverBD.sPed[iPed].fSpawnHeading = 0.0
						RESET_NET_TIMER(serverBD.sPed[iPed].stOutOfRangeTimer)
						IF BIKER_SELL_FRIENDS_IN_NEED()
//						OR BIKER_SELL_TACO_VAN()
						OR BIKER_SELL_PROVEN()
							SET_ATTRIBUTES_FOR_FRIEND(pedID)
							SET_PED_TREATED_AS_FRIENDLY(pedId, TRUE)
						ELSE
							SET_ATTRIBUTES_FOR_GUARD(pedID)
						ENDIF
						IF BIKER_SELL_STING()
							stTask = GET_DROP_OFF_PED_SECANARIO(iPed)
							IF NOT IS_STRING_NULL_OR_EMPTY(stTask)
								TASK_START_SCENARIO_IN_PLACE(pedId, stTask, DEFAULT, FALSE)
							ENDIF
							SET_ATTRIBUTES_FOR_COP(pedId)
							SET_PED_COMBAT_MOVEMENT(pedId, CM_DEFENSIVE)
							SET_PED_SPHERE_DEFENSIVE_AREA(pedId, vPos, 100.00)
						ENDIF
						IF BIKER_SELL_FRIENDS_IN_NEED()
//							SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(pedId, TRUE, RELGROUPHASH_SHARK)
							SET_PED_CAN_BE_TARGETTED(pedId, FALSE)
							GIVE_PED_SCENARIO(pedId, sGET_PED_SCENARIO(FALSE), FALSE)
						ENDIF
						serverBD.iAIPedsMade++
						#IF IS_DEBUG_BUILD
						tl15Name = "PED " 
						tl15Name += iPed
						SET_PED_NAME_DEBUG(pedId, tl15Name)
						PRINTLN("[BIKER_SELL] [PED] CREATE_AI_PED, Ped ", iPed, " created serverBD.iAIPedsMade = ", serverBD.iAIPedsMade, " at ", vPos, " model = ", GET_MODEL_NAME_FOR_DEBUG(GET_PED_MODEL_FOR_VARIATION(iPed)))
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_START_PEDS()
	// If no peds in this variation, return true
	IF NOT DOES_VARIATION_HAVE_AI_PEDS()
		RETURN TRUE
	ENDIF
	
	IF NOT DOES_VARIATION_SPAWN_PEDS_AT_START()
		RETURN TRUE
	ENDIF
	
	INT i
	BOOL bReturn = TRUE
	REPEAT GET_NUM_PEDS_FOR_VARIATION() i
		IF NOT CREATE_AI_PED(i, TRUE)
			PRINTLN("[BIKER_SELL] [PED], CREATE_START_PEDS - waiting for ped ", i, " to be created")
			bReturn = FALSE
		ENDIF
	ENDREPEAT
	
	RETURN bReturn
ENDFUNC

FUNC BOOL CREATE_START_VEHCILE(INT iVehicle)
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[iVehicle].netIndex)
		IF REQUEST_LOAD_MODEL(GET_VEHICLE_MODEL_FOR_VARIATION())
			VECTOR vSpawnCoords = GET_START_VEHICLE_COORDS(iVehicle)
			FLOAT fSpawnHeading = GET_START_VEHICLE_HEADING(iVehicle)
			IF CREATE_NET_VEHICLE(serverBD.sPedVeh[iVehicle].netIndex, GET_VEHICLE_MODEL_FOR_VARIATION(), vSpawnCoords, fSpawnHeading, TRUE, TRUE, TRUE, FALSE, TRUE)
				SET_AMBUSH_VEHICLE_SPAWN_ATTRIBUTES(iVehicle)
				CALL_SET_ALLOW_RAMMING_SOOP_OR_RAMP(NET_TO_VEH(serverBD.sPedVeh[iVehicle].netIndex))
				PRINTLN("[BIKER_SELL] [VEH], CREATE_START_VEHCILE - made vehicle ", iVehicle)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_START_VEHICLES()
	// If no peds in this variation, return true
	IF NOT DOES_VARIATION_HAVE_START_VEHICLE()
		RETURN TRUE
	ENDIF
		
	INT i
	BOOL bReturn = TRUE
	REPEAT GET_NUM_START_VEH_FOR_VARIATION() i
		IF NOT CREATE_START_VEHCILE(i)
			PRINTLN("[BIKER_SELL] [VEH], CREATE_START_VEHICLE - waiting for ped ", i, " to be created")
			bReturn = FALSE
		ENDIF
	ENDREPEAT
	IF bReturn
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_VEHICLE_MODEL_FOR_VARIATION())
	ENDIF
	RETURN bReturn
ENDFUNC

FUNC BOOL DOES_VARIATION_REQUIRE_CRATES_IN_VEHICLES()	
	SWITCH GET_SELL_VAR()
		CASE eBIKER_SELL_PROVEN
		CASE eBIKER_SELL_DEFAULT
			IF serverBD.eShipmentType = eBIKER_SHIPMENT_TYPE_SMALL
			OR serverBD.eShipmentType = eBIKER_SHIPMENT_TYPE_MEDIUM
			
				RETURN TRUE
			ENDIF		
		BREAK
		CASE eBIKER_SELL_BORDER_PATROL
		CASE eBIKER_SELL_HELICOPTER_DROP
		CASE eBIKER_SELL_POSTMAN
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR vATTACH_ROTATION()

	IF BIKER_SELL_HELICOPTER_DROP()
		RETURN <<0.0,0.0,0.0>>
	ENDIF

	RETURN <<0.0,0.0,90.0>>
ENDFUNC

FUNC VECTOR vATTACH_OFFSET()	
	SWITCH GET_SELL_VAR()
		CASE eBIKER_SELL_POSTMAN
		
			RETURN <<0.0,-1.0,-0.30>>
		BREAK
		
		CASE eBIKER_SELL_PROVEN
		CASE eBIKER_SELL_DEFAULT
			IF serverBD.eShipmentType = eBIKER_SHIPMENT_TYPE_SMALL
			
				RETURN <<0.0,-1.0,0.75>>
			ELIF serverBD.eShipmentType = eBIKER_SHIPMENT_TYPE_MEDIUM
			
				RETURN <<0.0,-1.0,0.00>>
			ENDIF
		BREAK
		
		CASE eBIKER_SELL_BORDER_PATROL
			RETURN <<0.0,-0.75,0.35>>
		BREAK
		
		CASE eBIKER_SELL_HELICOPTER_DROP
			RETURN <<0.0, -0.05, 0.0>>
		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC MODEL_NAMES mnCRATE_FOR_ATTACH()	
	SWITCH GET_SELL_VAR()
		CASE eBIKER_SELL_POSTMAN
		
			RETURN prop_box_wood01a
			
		CASE eBIKER_SELL_PROVEN
		CASE eBIKER_SELL_DEFAULT
			IF serverBD.eShipmentType = eBIKER_SHIPMENT_TYPE_SMALL
			
				RETURN prop_box_wood01a
			ELIF serverBD.eShipmentType = eBIKER_SHIPMENT_TYPE_MEDIUM
			
				RETURN Prop_box_wood04a
			ENDIF
		BREAK
		
		CASE eBIKER_SELL_HELICOPTER_DROP
//			RETURN prop_box_wood01a
			RETURN PROP_CARDBORDBOX_03A
//			RETURN Ex_Prop_Adv_Case_Sm_03
		BREAK
		
		CASE eBIKER_SELL_BORDER_PATROL
			RETURN PROP_CS_DUFFEL_01
		BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC BOOL ATTACH_CRATE_TO_VEHICLE(VEHICLE_INDEX vehID, INT iVeh)
	
	IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_VEH(serverBD.niVehicleCrate[iVeh]),vehID)
		STRING sBone = GET_BONE_NAME()
		ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(serverBD.niVehicleCrate[iVeh]), vehID, GET_ENTITY_BONE_INDEX_BY_NAME(vehID, sBone), vATTACH_OFFSET(), vATTACH_ROTATION(), TRUE, FALSE, TRUE)
		PRINTLN("[BIKER_SELL] - ATTACH_CRATE_TO_VEHICLE: attached package to vehicle ", NATIVE_TO_INT(vehID), " on bone: '", sBone, "'")
	ELSE
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_CRATE_FOR_VEHICLE(INT iVeh)
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVehicleCrate[iVeh])
		IF REQUEST_LOAD_MODEL(mnCRATE_FOR_ATTACH())
			IF CAN_REGISTER_MISSION_OBJECTS(iVeh + 1)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh])
				AND IS_SELL_VEH_OK(iVeh)
					VEHICLE_INDEX vehID = NET_TO_VEH(serverBD.niVeh[iVeh])
				
					IF CREATE_NET_OBJ(serverBD.niVehicleCrate[iVeh], mnCRATE_FOR_ATTACH(), (GET_ENTITY_COORDS(VEH_ID(iVeh)) + <<0.0,0.0,5.0>>))
						IF ATTACH_CRATE_TO_VEHICLE(vehID, iVeh)
							SET_ENTITY_LOD_DIST(NET_TO_OBJ(serverBD.niVehicleCrate[iVeh]), 100)
							SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niVehicleCrate[iVeh]),TRUE)
							SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.niVehicleCrate[iVeh]),TRUE)
							SET_MODEL_AS_NO_LONGER_NEEDED(mnCRATE_FOR_ATTACH())
							SET_ENTITY_PROOFS(NET_TO_OBJ(serverBD.niVehicleCrate[iVeh]),TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_VEHICLE_CRATES()
	IF DOES_VARIATION_REQUIRE_CRATES_IN_VEHICLES()
		INT i
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			IF NOT CREATE_CRATE_FOR_VEHICLE(i)
				PRINTLN("[BIKER_SELL] - CREATE_VEHICLE_CRATES - Waiting for veh crate ", i)
				RETURN FALSE
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN TRUE
ENDFUNC
FUNC BOOL CREATE_DROP_OFF_OBJECT(INT iProp)
	VECTOR vPos
	VECTOR vHeading
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sObj[iProp].netIndex)
		IF REQUEST_LOAD_MODEL(GET_SELL_PROP_MODEL())
			vPos = GET_SELL_PROP_VECTOR()
			vHeading = GET_SELL_PROP_ROTATION()
			IF CAN_REGISTER_MISSION_PEDS(GB_GET_BOSS_MISSION_NUM_OBJ_REQUIRED(FMMC_TYPE_BIKER_CONTRABAND_SELL) + 1)
				IF CREATE_NET_OBJ(serverBD.sObj[iProp].netIndex, GET_SELL_PROP_MODEL(), vPos)
					SET_ENTITY_COORDS_NO_OFFSET(NET_TO_ENT(serverBD.sObj[iProp].netIndex), vPos)
					SET_ENTITY_ROTATION(NET_TO_ENT(serverBD.sObj[iProp].netIndex), vHeading)
					PRINTLN("[BIKER_SELL] CREATE_DROP_OFF_PED, DONE ", vPos)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_DROP_OFF_OBJECTS()
	INT i
	
	IF NOT VAR_HAS_SELL_PROPS()
		RETURN TRUE
	ENDIF
	
	REPEAT GET_SELL_PROP_NUMBER() i
		IF CREATE_DROP_OFF_OBJECT(i)
			PRINTLN("[BIKER_SELL] [DROPOFF_OBJ], CREATE_DROP_OFF_OBJECTS - Drop off object ", i, " created.")
		ENDIF
	ENDREPEAT
	
	REPEAT GET_SELL_PROP_NUMBER() i
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sObj[i].netIndex)
			PRINTLN("[BIKER_SELL] [DROPOFF_OBJ], CREATE_DROP_OFF_OBJECTS - NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID FALSE ( ", i, ")")
			RETURN FALSE
		ENDIF
	ENDREPEAT 
	
	RETURN TRUE

ENDFUNC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				AI PED VEHICLES													║

FUNC BOOL DELETE_VEHICLES()
	INT i
	REPEAT GET_NUM_ATTACK_VEHICLES_FOR_VARIATION() i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[i].netIndex)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPedVeh[i].netIndex)
			OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPedVeh[i].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
			OR (IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPedVeh[i].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
				IF TAKE_CONTROL_OF_NET_ID(serverBD.sPedVeh[i].netIndex)
					//IF NOT BIKER_SELL_EXCHANGE()
					IF NOT BIKER_SELL_DEFAULT()
						NETWORK_FADE_OUT_ENTITY(NET_TO_ENT(serverBD.sPedVeh[i].netIndex), TRUE, TRUE)
					ENDIF
					CLEANUP_NET_ID(serverBD.sPedVeh[i].netIndex) // For land defend, don't want to delete the vehicles as they might be right in front of the player
					RESET_NET_TIMER(serverBD.stVehSpawnDelayTimer[i])
					SET_VEHICLE_STATE(i, eVEHICLESTATE_CREATE)
					PRINTLN("[BIKER_SELL] [PED_VEHICLE], DELETE_AMBUSH_VEHICLES - Vehicle ", i, " deleted")
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	i = 0
	REPEAT GET_NUM_ATTACK_VEHICLES_FOR_VARIATION() i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[i].netIndex)
			PRINTLN("[BIKER_SELL] [PED_VEHICLE], DELETE_AMBUSH_VEHICLES - Waiting for vehicle ", i, " to be deleted")
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Direction from the drop off that the Land Defend ai should come from 
FUNC FLOAT GET_CONRABAND_DROPOFF_AI_SPAWN_DIRECTION_AS_HEADING(INT iDrop)
	
	FLOAT fHeading
	SWITCH iDrop
		CASE ciD_LSIA				fHeading = 317.5908 	BREAK
		CASE ciD_Vespucci			fHeading = 65.8782 		BREAK
		CASE ciD_Rockford_Plaza		fHeading = 79.6437 		BREAK
		CASE ciD_Hawick				fHeading = 339.1620 	BREAK
		CASE ciD_Greenwich			fHeading = 300.5366 	BREAK
		CASE ciD_Rockford_Hills		fHeading = 26.3732 		BREAK
		CASE ciD_Pillbox_Hill		fHeading = 12.9312  	BREAK
		CASE ciD_Davis				fHeading = 306.5281 	BREAK
		CASE ciD_Terminal			fHeading = 70.6314 		BREAK
		CASE ciD_Chumash			fHeading = 199.4232 	BREAK
		CASE ciD_Banham_Canyon		fHeading = 232.5371 	BREAK
		CASE ciD_Pacific_Bluffs		fHeading = 217.9965 	BREAK
		CASE ciD_Murrieta_Heights	fHeading = 97.8555 		BREAK
		CASE ciD_Cypress_Flats		fHeading = 254.7614 	BREAK
		CASE ciD_Downtown_Vinewood	fHeading = 149.1127 	BREAK
		CASE ciD_Rehab				fHeading = 299.9396 	BREAK
		CASE ciGove					fHeading = 143.5423 	BREAK
		CASE ciLaFuente				fHeading = 153.6289		BREAK
		CASE ciVinewood				fHeading = 327.4365 	BREAK
		CASE ciElysian				fHeading = 0.0 			BREAK	
	ENDSWITCH
	
	PRINTLN("[BIKER_SELL], [CREATE_NON_CONTRABAND_VEHICLE] [GET_CONRABAND_DROPOFF_AI_SPAWN_DIRECTION_AS_HEADING] Called with iDrop = ", iDrop, " Returning ",fHeading) 
	RETURN fHeading
ENDFUNC

/// PURPOSE:
///    Converts the heading we want the AI to come from in Land Defend to a direction vector that can be passed to FIND_SPAWN_POINT_IN_DIRECTION
FUNC VECTOR GET_CONRABAND_DROPOFF_AI_SPAWN_DIRECTION_AS_VECTOR(INT iDrop)
	
	
	FLOAT fHeading = GET_CONRABAND_DROPOFF_AI_SPAWN_DIRECTION_AS_HEADING(iDrop)
	VECTOR vDir = CONVERT_ROTATION_TO_DIRECTION_VECTOR(<< 0.0, 0.0, fHeading >>)
	
	PRINTLN("[BIKER_SELL], [CREATE_NON_CONTRABAND_VEHICLE] [GET_CONRABAND_DROPOFF_AI_SPAWN_DIRECTION_AS_VECTOR] Called with iDrop = ", iDrop,  "Returning ",vDir) 
	RETURN vDir
ENDFUNC

/// PURPOSE:
///   Figure out which drop-off the delivered sell vehicle is closest to (for Land Defend)    
FUNC INT GET_DROP_OFF_FOR_LAND_DEFEND_AI_SPAWN(INT iSellVeh)
	PRINTLN("[BIKER_SELL], [CREATE_NON_CONTRABAND_VEHICLE] [GET_DROP_OFF_FOR_LAND_DEFEND_AI_SPAWN] Called with sell veh ", iSellVeh)
	INT i
	INT iClosest = -1
	FLOAT fClosest2 = 9999999999.0
	FLOAT fDist2
	VECTOR vCoords
	VECTOR vDropOffCoords
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iSellVeh])
		vCoords = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[iSellVeh]), FALSE)
		
		REPEAT GET_MAX_SELL_DROP_OFFS() i
			vDropOffCoords = GET_SELL_DROP_COORDS(GET_SELL_VAR(), i, serverBD.iRoute, iSellVeh)
			IF NOT IS_VECTOR_ZERO(vDropOffCoords)
				fDist2 = VDIST2(vCoords, vDropOffCoords)
				IF fDist2 < fClosest2
					fClosest2 = fDist2
					iClosest = i
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		PRINTLN("[BIKER_SELL], [CREATE_NON_CONTRABAND_VEHICLE] [GET_DROP_OFF_FOR_LAND_DEFEND_AI_SPAWN] Sell veh doesn't exist ")		
	ENDIF
	

	
	PRINTLN("[BIKER_SELL], [CREATE_NON_CONTRABAND_VEHICLE] [GET_DROP_OFF_FOR_LAND_DEFEND_AI_SPAWN] Returning ", iClosest, " Sell veh coords = ", vCoords)
	
	RETURN iClosest
		
ENDFUNC

/// PURPOSE:
///    How far away from the specifed drop off the Land Defend AI should ideally spawn
FUNC FLOAT GET_IDEAL_SPAWN_DISTANCE_FOR_LAND_DEFEND_AI(INT iDrop)
	FLOAT fDist = 225.0
	SWITCH iDrop
		CASE 16 	fDist = 150.0	BREAK
		CASE 19 	fDist = 150.0	BREAK
		
	ENDSWITCH
	
	RETURN fDist
ENDFUNC

FUNC BOOL CREATE_NON_CONTRABAND_VEHICLE(INT iVehicle, MODEL_NAMES model)
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_15 tl15Name
	#ENDIF
	PED_INDEX piAttackPed
	SWITCH GET_SELL_VAR()
		// ADD SPAWNING LOGIC FOR OTHER VARIATIONS AS NEEDED HERE
		CASE eBIKER_SELL_CLUB_RUN
			IF serverBD.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = -1
			OR serverBD.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = iVehicle
				piAttackPed = GET_RAND_PLAYER_TO_ATTACK()
				serverBd.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = iVehicle
				IF DOES_ENTITY_EXIST(piAttackPed)
					IF IS_VECTOR_ZERO(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords)	
						
						IF IS_VECTOR_ZERO(serverBD.sSpawnStruct.vSpawnDirection)
							serverBD.sSpawnStruct.vSpawnDirection = GET_ENTITY_FORWARD_VECTOR(piAttackPed)
							PRINTLN("[BIKER_SELL] - CREATE_NON_CONTRABAND_VEHICLE - serverBD.vSpawnDirection = GET_ENTITY_FORWARD_VECTOR = ", serverBD.sSpawnStruct.vSpawnDirection, " serverBD.iRandVehToAttack = ", serverBD.iRandVehToAttack)
						ELSE
							FLOAT fSpawnDistance
							
							IF (serverBD.sSpawnStruct.vSpawnDirection.z >= 0.0 AND serverBD.sSpawnStruct.vSpawnDirection.z < 90.0)
							OR (serverBD.sSpawnStruct.vSpawnDirection.z >= 270.0 AND serverBD.sSpawnStruct.vSpawnDirection.z < 360.0)
								fSpawnDistance = 220
							ELSE
								fSpawnDistance = 120
							ENDIF
							BOOL bSpawn
							FLOAT fSpanwDistFromContra
							VECTOR vOffsetFromContra
							
							bSpawn = TRUE 
							IF FIND_SPAWN_POINT_IN_DIRECTION(GET_ENTITY_COORDS(piAttackPed), serverBD.sSpawnStruct.vSpawnDirection, fSpawnDistance, serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords)
								fSpanwDistFromContra = GET_DISTANCE_BETWEEN_COORDS(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, GET_ENTITY_COORDS(piAttackPed, FALSE))
								vOffsetFromContra = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(piAttackPed, serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords)
								
								//-- Too far from the player?
								IF fSpanwDistFromContra > 290.0
									bSpawn = FALSE
									PRINTLN("[BIKER_SELL] - CREATE_NON_CONTRABAND_VEHICLE - rejecting  spawn coords too far from player ", serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, " dist = ", fSpanwDistFromContra)
								ENDIF
								
								//-- Location ok?
								IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 190)
									bSpawn = FALSE
									PRINTLN("[BIKER_SELL] - CREATE_NON_CONTRABAND_VEHICLE - serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords not okay for net entity creation, setting back to zero so we search again")
								ENDIF
								
								IF ABSF(vOffsetFromContra.x) > 100.0
									bSpawn = FALSE
									PRINTLN("[BIKER_SELL] - CREATE_NON_CONTRABAND_VEHICLE REJECTING COORDS ", serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords ," As X offset from contra vehicle = ", vOffsetFromContra.x)
								ENDIF
								
								IF NOT bSpawn
									serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords = << 0.0, 0.0, 0.0 >>
								ELSE
									serverBd.sSpawnStruct.fSpawnNonContrabandVehicleHeading = GET_HEADING_BETWEEN_VECTORS_2D(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, GET_ENTITY_COORDS(piAttackPed))
									PRINTLN("[BIKER_SELL] - CREATE_NON_CONTRABAND_VEHICLE - serverBd.vSpawnNonContrabandVehicleCoords is okay for net entity creation. Dist = ",fSpanwDistFromContra, " Tried to spawn at least this far ", fSpawnDistance, " serverBD.iRandVehToAttack = ", serverBD.iRandVehToAttack)
									PRINTLN("[BIKER_SELL] - CREATE_NON_CONTRABAND_VEHICLE - serverBd.fSpawnNonContrabandVehicleHeading = GET_HEADING_BETWEEN_VECTORS_2D = ", serverBd.sSpawnStruct.fSpawnNonContrabandVehicleHeading)
								
								ENDIF
								
								
							ELSE
								PRINTLN("[BIKER_SELL] - CREATE_NON_CONTRABAND_VEHICLE - FIND_SPAWN_POINT_IN_DIRECTION = FALSE. Adding rotation to coords with ADD_ROTATION_FOR_SPAWN_CHECK")
								ADD_ROTATION_FOR_SPAWN_CHECK(serverBD.sSpawnStruct.vSpawnDirection)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_VECTOR_ZERO(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords)	
						IF CREATE_NET_VEHICLE(serverBD.sPedVeh[iVehicle].netIndex, model, serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, serverBd.sSpawnStruct.fSpawnNonContrabandVehicleHeading, TRUE, TRUE, TRUE, FALSE, TRUE)
							SET_AMBUSH_VEHICLE_SPAWN_ATTRIBUTES(iVehicle)
							SET_MODEL_AS_NO_LONGER_NEEDED(model)
						//	SET_VEHICLE_STATE(iVehicle, GET_AFTER_CREATION_VEHICLE_STATE(iVehicle))
							serverBd.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = (-1)
							serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords = << 0.0, 0.0, 0.0 >>
							serverBd.sSpawnStruct.fSpawnNonContrabandVehicleHeading = 0.0
							ServerBd.sSpawnStruct.iAmbushGetNonContrabandCoordsAttempts = 0
							serverBd.sSpawnStruct.iGetSpawnNonContrabandVehicleCoordsStage = 0
							serverBD.fNumWavesSpawned += 1.0
							
							#IF IS_DEBUG_BUILD
							serverBD.iGangChaseVehCreated++
							PRINTLN("[BIKER_SELL] - CREATE_NON_CONTRABAND_VEHICLE - created non contraband vehicle ", iVehicle, " serverBD.iGangChaseVehCreated = ", serverBD.iGangChaseVehCreated, " serverBD.fNumWavesSpawned = ", serverBD.fNumWavesSpawned, " serverBD.iRandVehToAttack = ", serverBD.iRandVehToAttack)
							#ENDIF
							
							#IF IS_DEBUG_BUILD
							tl15Name = "VEH " 
							tl15Name += iVehicle
							SET_VEHICLE_NAME_DEBUG(NET_TO_VEH(serverBD.sPedVeh[iVehicle].netIndex), tl15Name)
							#ENDIF
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE eBIKER_SELL_DEFAULT
			IF serverBd.sSpawnStruct.iCurrentPedSearchingForSpawnCoords = (-1)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[serverBD.iRandVehToAttack])
					IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[serverBD.iRandVehToAttack])
						IF serverBD.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = -1
						OR serverBD.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = iVehicle
							
							serverBd.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = iVehicle
							
							IF IS_VECTOR_ZERO(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords)	
								
								IF IS_VECTOR_ZERO(serverBD.sSpawnStruct.vSpawnDirection)
									serverBD.sSpawnStruct.vSpawnDirection = GET_ENTITY_FORWARD_VECTOR(NET_TO_VEH(serverBD.niVeh[serverBD.iRandVehToAttack]))
									PRINTLN("[BIKER_SELL] - CREATE_NON_CONTRABAND_VEHICLE - serverBD.vSpawnDirection = GET_ENTITY_FORWARD_VECTOR = ", serverBD.sSpawnStruct.vSpawnDirection, " serverBD.iRandVehToAttack = ", serverBD.iRandVehToAttack)
								ELSE
									FLOAT fSpawnDistance
									
									IF (serverBD.sSpawnStruct.vSpawnDirection.z >= 0.0 AND serverBD.sSpawnStruct.vSpawnDirection.z < 90.0)
									OR (serverBD.sSpawnStruct.vSpawnDirection.z >= 270.0 AND serverBD.sSpawnStruct.vSpawnDirection.z < 360.0)
										fSpawnDistance = 220
									ELSE
										fSpawnDistance = 120
									ENDIF
									BOOL bSpawn
									FLOAT fSpanwDistFromContra
									VECTOR vOffsetFromContra
									
									bSpawn = TRUE 
									IF FIND_SPAWN_POINT_IN_DIRECTION(GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niVeh[serverBD.iRandVehToAttack])), serverBD.sSpawnStruct.vSpawnDirection, fSpawnDistance, serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords)
										fSpanwDistFromContra = GET_DISTANCE_BETWEEN_COORDS(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[serverBD.iRandVehToAttack]), FALSE))
										vOffsetFromContra = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(NET_TO_VEH(serverBD.niVeh[serverBD.iRandVehToAttack]), serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords)
										
										//-- Too far from the player?
										IF fSpanwDistFromContra > 290.0
											bSpawn = FALSE
											PRINTLN("[BIKER_SELL] - CREATE_NON_CONTRABAND_VEHICLE - rejecting  spawn coords too far from player ", serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, " dist = ", fSpanwDistFromContra)
										ENDIF
										
										//-- Location ok?
										IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 190)
											bSpawn = FALSE
											PRINTLN("[BIKER_SELL] - CREATE_NON_CONTRABAND_VEHICLE - serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords not okay for net entity creation, setting back to zero so we search again")
										ENDIF
										
										IF ABSF(vOffsetFromContra.x) > 100.0
											bSpawn = FALSE
											PRINTLN("[BIKER_SELL] - CREATE_NON_CONTRABAND_VEHICLE REJECTING COORDS ", serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords ," As X offset from contra vehicle = ", vOffsetFromContra.x)
										ENDIF
										
										IF NOT bSpawn
											serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords = << 0.0, 0.0, 0.0 >>
										ELSE
											serverBd.sSpawnStruct.fSpawnNonContrabandVehicleHeading = GET_HEADING_BETWEEN_VECTORS_2D(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niVeh[serverBD.iRandVehToAttack])))
											PRINTLN("[BIKER_SELL] - CREATE_NON_CONTRABAND_VEHICLE - serverBd.vSpawnNonContrabandVehicleCoords is okay for net entity creation. Dist = ",fSpanwDistFromContra, " Tried to spawn at least this far ", fSpawnDistance, " serverBD.iRandVehToAttack = ", serverBD.iRandVehToAttack)
											PRINTLN("[BIKER_SELL] - CREATE_NON_CONTRABAND_VEHICLE - serverBd.fSpawnNonContrabandVehicleHeading = GET_HEADING_BETWEEN_VECTORS_2D = ", serverBd.sSpawnStruct.fSpawnNonContrabandVehicleHeading)
										
										ENDIF
										
										
									ELSE
										PRINTLN("[BIKER_SELL] - CREATE_NON_CONTRABAND_VEHICLE - FIND_SPAWN_POINT_IN_DIRECTION = FALSE. Adding rotation to coords with ADD_ROTATION_FOR_SPAWN_CHECK")
										ADD_ROTATION_FOR_SPAWN_CHECK(serverBD.sSpawnStruct.vSpawnDirection)
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT IS_VECTOR_ZERO(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords)	
								IF CREATE_NET_VEHICLE(serverBD.sPedVeh[iVehicle].netIndex, model, serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, serverBd.sSpawnStruct.fSpawnNonContrabandVehicleHeading, TRUE, TRUE, TRUE, FALSE, TRUE)
									SET_AMBUSH_VEHICLE_SPAWN_ATTRIBUTES(iVehicle)
									SET_MODEL_AS_NO_LONGER_NEEDED(model)
								//	SET_VEHICLE_STATE(iVehicle, GET_AFTER_CREATION_VEHICLE_STATE(iVehicle))
									serverBd.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = (-1)
									serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords = << 0.0, 0.0, 0.0 >>
									serverBd.sSpawnStruct.fSpawnNonContrabandVehicleHeading = 0.0
									ServerBd.sSpawnStruct.iAmbushGetNonContrabandCoordsAttempts = 0
									serverBd.sSpawnStruct.iGetSpawnNonContrabandVehicleCoordsStage = 0
									serverBD.fNumWavesSpawned += 1.0
									
									#IF IS_DEBUG_BUILD
									serverBD.iGangChaseVehCreated++
									PRINTLN("[BIKER_SELL] - CREATE_NON_CONTRABAND_VEHICLE - created non contraband vehicle ", iVehicle, " serverBD.iGangChaseVehCreated = ", serverBD.iGangChaseVehCreated, " serverBD.fNumWavesSpawned = ", serverBD.fNumWavesSpawned, " serverBD.iRandVehToAttack = ", serverBD.iRandVehToAttack)
									#ENDIF
									
									#IF IS_DEBUG_BUILD
									tl15Name = "VEH " 
									tl15Name += iVehicle
									SET_VEHICLE_NAME_DEBUG(NET_TO_VEH(serverBD.sPedVeh[iVehicle].netIndex), tl15Name)
									#ENDIF
									RETURN TRUE
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC



PROC MAINTAIN_NON_CONTRABAND_VEHICLES()

	IF DOES_VARIATION_HAVE_AI_VEHICLES()
		//PRINTLN("[MAINTAIN_NON_CONTRABAND_VEHICLES] DOES_VARIATION_HAVE_AI_VEHICLES")
		INT iVehicle
		INT iPed
		
		REPEAT GET_NUM_ATTACK_VEHICLES_FOR_VARIATION() iVehicle
			SWITCH GET_VEHICLE_STATE(iVehicle)
			
				CASE eVEHICLESTATE_INACTIVE
					//PRINTLN("[MAINTAIN_NON_CONTRABAND_VEHICLES] eVEHICLESTATE_INACTIVE")
				BREAK
				
				CASE eVEHICLESTATE_CREATE
					//PRINTLN("[MAINTAIN_NON_CONTRABAND_VEHICLES] eVEHICLESTATE_CREATE")
					IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[iVehicle].netIndex)
						IF IS_SAFE_TO_CREATE_NON_CONTRABAND_VEHICLES()
							PRINTLN("[MAINTAIN_NON_CONTRABAND_VEHICLES] IS_SAFE_TO_CREATE_NON_CONTRABAND_VEHICLES")
							IF HAS_NET_TIMER_STARTED(serverBD.stPedVehRespawnDelay)
								PRINTLN("[MAINTAIN_NON_CONTRABAND_VEHICLES] HAS_NET_TIMER_STARTED")
								RESET_NET_TIMER(serverBD.stPedVehRespawnDelay)
							ENDIF
							IF REQUEST_LOAD_MODEL(GET_VEHICLE_MODEL_FOR_VARIATION())
								PRINTLN("[MAINTAIN_NON_CONTRABAND_VEHICLES] REQUEST_LOAD_MODEL")
								IF CREATE_NON_CONTRABAND_VEHICLE( iVehicle, GET_VEHICLE_MODEL_FOR_VARIATION())
									PRINTLN("[BIKER_SELL] [TWIST] [PED_VEHICLE], MAINTAIN_NON_CONTRABAND_VEHICLES - Vehicle ", iVehicle, " created, moving to populate")
									SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_POPULATE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE eVEHICLESTATE_POPULATE
					//PRINTLN("[MAINTAIN_NON_CONTRABAND_VEHICLES] eVEHICLESTATE_POPULATE")
					IF DOES_VARIATION_HAVE_AI_PEDS()
						REPEAT GET_NUM_PEDS_FOR_VARIATION() iPed
							IF CREATE_AI_PED( iPed)
								PRINTLN("[BIKER_SELL] [TWIST] [PED_VEHICLES], MAINTAIN_NON_CONTRABAND_VEHICLES, eVEHICLESTATE_POPULATE - Ped ", iPed, " created.")
							ENDIF
						ENDREPEAT
					ENDIF
					
					IF IS_NON_CONTRABAND_VEHICLE_OCCUPIED(iVehicle)
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_DRIVEABLE)
						IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_GIVE_CLUB_RUN_WANTED)
						AND BIKER_SELL_CLUB_RUN()
							SET_SERVER_BIT1(eSERVERBITSET1_GIVE_CLUB_RUN_WANTED)
						ENDIF
					ELSE
						PRINTLN("[BIKER_SELL] [PED_VEHICLES], MAINTAIN_NON_CONTRABAND_VEHICLES, VEH NOT OCCUPIED! Veh ", iVehicle)
					ENDIF
				BREAK
				
				CASE eVEHICLESTATE_DRIVEABLE
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[iVehicle].netIndex)
						IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.sPedVeh[iVehicle].netIndex))
						OR IS_ENTITY_DEAD(NET_TO_VEH(serverBD.sPedVeh[iVehicle].netIndex))
							PRINTLN("[BIKER_SELL] [PED_VEHICLES], MAINTAIN_NON_CONTRABAND_VEHICLES, eVEHICLESTATE_DRIVEABLE - Veh ", iVehicle, " no longer driveable.")
							SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_NOT_DRIVEABLE)
						ENDIF
					ENDIF
				BREAK
				
				CASE eVEHICLESTATE_NOT_DRIVEABLE

				BREAK
				
				CASE eVEHICLESTATE_RESET
					
				BREAK
				
			ENDSWITCH
		
		ENDREPEAT
	ENDIF
ENDPROC

/// PURPOSE:
///    For AI helicopters, need to clean them up as soon as they're not drivable, as otherwise they'll fall through the map and assert
/// RETURNS:
///    
FUNC BOOL SHOULD_CLEANUP_CONTRABAND_VEHICLES_IMMEDIATELY()
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_NON_CONTRABAND_PED_AND_VEHICLE_RESPAWNING()
	IF DOES_VARIATION_HAVE_AI_PEDS()
	AND DOES_VARIATION_HAVE_AI_VEHICLES()
		IF NOT HAS_VEHICLE_RESPAWNING_TIMER_STARTED()
			IF SHOULD_RESPAWN_VEHICLES()

				PRINTLN("[BIKER_SELL] [PED_VEHICLES], MAINTAIN_NON_CONTRABAND_VEHICLES, eVEHICLESTATE_NOT_DRIVEABLE - Vehicles should respawn, starting timer ")
				START_NET_TIMER(serverBD.stPedVehRespawnTimer)
				
			ENDIF
		ELSE
			IF HAS_VEHICLE_RESPAWNING_TIMER_EXPIRED()
				IF NOT HAS_NET_TIMER_STARTED(serverBD.stPedVehRespawnDelay)
					IF DELETE_VEHICLES()
					AND DELETE_PEDS()
						PRINTLN("[BIKER_SELL] [PED_VEHICLES], MAINTAIN_NON_CONTRABAND_VEHICLES RESPAWN TIMER EXPIRED AND ENTITIES DELETED")
						RESET_NET_TIMER(serverBD.stPedVehRespawnTimer)
						START_NET_TIMER(serverBD.stPedVehRespawnDelay)
						IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
							//IF NOT BIKER_SELL_EXCHANGE()
							IF NOT BIKER_SELL_DEFAULT()
								serverBD.iRandVehToAttack = GET_RANDOM_INT_IN_RANGE(0, GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE())
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				//-- If AI helicopters are wrecked then they need to be cleaned up pretty quickly, or
				//-- they'll fall through the map and assert.
				IF SHOULD_CLEANUP_CONTRABAND_VEHICLES_IMMEDIATELY()
					DELETE_VEHICLES()
					DELETE_PEDS()
					IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
						//IF NOT BIKER_SELL_EXCHANGE()
						IF NOT BIKER_SELL_DEFAULT()
							serverBD.iRandVehToAttack = GET_RANDOM_INT_IN_RANGE(0, GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE())
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_CLIENT_VEHICLE_SPAWN_CHECKS()
	IF DOES_VARIATION_HAVE_AI_VEHICLES()
		SWITCH GET_SELL_VAR()
		
//			CASE eBIKER_SELL_EXCHANGE
//				INT i
//				REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
//					IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_LAND_ATTACK_SPAWN_DISTANCE_REACHED)
//						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
//							IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[i])
//								IF NOT IS_EXCHANGE_VEHICLE_NEAR_VEHICLE_SWAP(i, 350)
//									PRINTLN("[BIKER_SELL] [PLAYER], MAINTAIN_CLIENT_VEHICLE_SPAWN_CHECKS - eBIKER_SELL_THIRD_PARTY - Vehicle is now > 200m from starting point, setting that server should spawn attackers")
//									SET_CLIENT_BIT0(eCLIENTBITSET0_LAND_ATTACK_SPAWN_DISTANCE_REACHED)
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDREPEAT
//			BREAK
			
			CASE eBIKER_SELL_DEFAULT
				IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_LAND_ATTACK_SPAWN_DISTANCE_REACHED)
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
						IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(VEH_ID(0), GET_LAND_SELL_VEHICLE_CREATION_POS(0, 0, serverBD.iStartingWarehouse, serverBD.eShipmentType)) > 200
								PRINTLN("[BIKER_SELL] [PLAYER], MAINTAIN_CLIENT_VEHICLE_SPAWN_CHECKS - eBIKER_SELL_DEFAULT - Vehicle is now > 200m from starting point, setting that server should spawn attackers")
								SET_CLIENT_BIT0(eCLIENTBITSET0_LAND_ATTACK_SPAWN_DISTANCE_REACHED)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC MAINTAIN_BLOCKING_ZONES()

	VECTOR vDropOff = GET_WAREHOUSE_COORDS(serverBD.iStartingWarehouse)
	IF NOT IS_VECTOR_ZERO(vDropOff)
		ADD_TRAFFIC_REDUCTION_ZONE(serverBD.iTrafficReductionIndex,vDropOff)
	ENDIF

ENDPROC


//╔═════════════════════════════════════════════════════════════════════════════╗
//║				CRATES															║

// serverBD.sContraband[serverBD.iNumCrates].iCrateBS

//PURPOSE: Loads the Crate assets
FUNC BOOL LOAD_CRATE_ASSETS()

	REQUEST_MODEL(modelCRATE())
	REQUEST_MODEL(modelCHUTE())
	REQUEST_ANIM_DICT(sANIM_DICT_CHUTE())

	IF HAS_MODEL_LOADED(modelCRATE())
	AND HAS_MODEL_LOADED(modelCHUTE())
	AND HAS_ANIM_DICT_LOADED(sANIM_DICT_CHUTE())
	    RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CREATE_CHUTE()
	
	IF IS_THIS_AN_AIR_VARIATION()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC NETWORK_INDEX PARCEL_ID()
	RETURN sLocaldata.niParcel
ENDFUNC

FUNC ENTITY_INDEX PARCEL_ENT()

	ENTITY_INDEX eiReturn
	IF NETWORK_DOES_NETWORK_ID_EXIST(PARCEL_ID())    
		eiReturn = NET_TO_ENT(sLocaldata.niParcel)
	ENDIF
	
	RETURN eiReturn
ENDFUNC

FUNC BOOL CREATE_PARCEL()
	IF IS_BIT_SET(sLocaldata.iParcelBitset, ciPARCEL_CREATE)
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(PARCEL_ID())     
			IF CAN_REGISTER_MISSION_OBJECTS(1)
				PED_INDEX pedId = PLAYER_PED_ID()
				
				IF DOES_ENTITY_EXIST(pedId)
				AND NOT IS_ENTITY_DEAD(pedId)
				
					IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), GET_HASH_KEY("CREATE_PROP"))
				
						VECTOR vParcel
						vParcel = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedId, <<0.5, 0.5, 0.0>>)
						PRINTLN("[BIKER_SELL] [PARCEL]  - CREATE_PARCEL ", vParcel)
						IF CREATE_NET_OBJ(sLocaldata.niParcel, modelCRATE(), vParcel, FALSE)
						
							ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(PARCEL_ID()), pedId, GET_PED_BONE_INDEX(pedId, BONETAG_PH_R_HAND), <<0, 0, 0>>, <<0, 0, 0>>, TRUE, TRUE)
							
							CLEAR_BIT(sLocaldata.iParcelBitset, ciPARCEL_CREATE)
							
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DETACH_PARCEL()
	ENTITY_INDEX eiParcel = PARCEL_ENT()
	PED_INDEX pedId = PLAYER_PED_ID()
	
	IF DOES_ENTITY_EXIST(pedId)
		IF DOES_ENTITY_EXIST(eiParcel)
			IF IS_ENTITY_DEAD(pedId)
			OR HAS_ANIM_EVENT_FIRED(pedId, GET_HASH_KEY("RELEASE_PROP"))
				DETACH_ENTITY(eiParcel)
				SET_ENTITY_AS_NO_LONGER_NEEDED(eiParcel)
				PRINTLN("[BIKER_SELL] [PARCEL] - DETACH_PARCEL  ")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_PARCELS()
	IF BIKER_SELL_POSTMAN()
	
		SWITCH sLocaldata.iParcelProgress
		
			CASE 0
				IF CREATE_PARCEL()
					sLocaldata.iParcelProgress++
					PRINTLN("[BIKER_SELL] [PARCEL] - MAINTAIN_PARCELS CREATE_PARCEL  ")
				ENDIF
			BREAK
			
			CASE 1
				IF DETACH_PARCEL()
				
					sLocaldata.iParcelProgress = 0
					PRINTLN("[BIKER_SELL] [PARCEL] - MAINTAIN_PARCELS DETACH_PARCEL  ")
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

//PURPOSE: Creates and drops a crate cache from the plane
FUNC BOOL CREATE_DROPPED_CRATE(VECTOR vCrate, FLOAT fCrate, INT iVeh)
	
	VECTOR vChuteCoords
	INT index = serverBD.iNumCrates[iVeh]
	
    //Create the Crate 
    IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[index].netId[iVeh])     
		IF CAN_REGISTER_MISSION_OBJECTS(1)
		
//			IF BIKER_SELL_POSTMAN()
//				 CREATE_NET_OBJ(serverBD.sContraband[index].netId[iVeh], modelCRATE(), vCrate)
//			ELSE
	       		 serverBD.sContraband[index].netId[iVeh] = OBJ_TO_NET(CREATE_PORTABLE_PICKUP(PICKUP_PORTABLE_CRATE_UNFIXED_INCAR, ( vCrate ), FALSE, modelCRATE()))
//			ENDIF
	        SET_ENTITY_HEADING(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]), fCrate)
	        SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.sContraband[index].netId[iVeh], TRUE)
	        
			IF NOT BIKER_SELL_POSTMAN()				
	     	   SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]), TRUE)
			ENDIF
	        SET_OBJECT_FORCE_VEHICLES_TO_AVOID(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]), TRUE)
	        
	        SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]), TRUE)
	        ACTIVATE_PHYSICS(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]))
	        SET_ENTITY_VELOCITY(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]), <<0, 0, -0.2>>)
	        
			IF NOT BIKER_SELL_POSTMAN()
	      		PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]), TRUE)
			ENDIF
	        
	        SET_ENTITY_LOD_DIST(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]), 1200)  
			
			IF HAS_NET_TIMER_STARTED(serverBD.sContraband[index].timeCleanup[iVeh])
				RESET_NET_TIMER(serverBD.sContraband[index].timeCleanup[iVeh])
			ENDIF
			
			IF BIKER_SELL_TRASHMASTER()
				SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]), FALSE)
				SET_PORTABLE_PICKUP_PERSIST(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]), TRUE)
			ENDIF
			
			IF BIKER_SELL_HELICOPTER_DROP()
				SET_ENTITY_COLLISION(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]), FALSE)
			ENDIF
			
			serverBD.eiTrash = NET_TO_ENT(serverBD.sContraband[index].netId[iVeh])
			
//			serverBD.eiProduct[NATIVE_TO_INT(serverBD.playerRequestedCrate)] = NET_TO_ENT(serverBD.sContraband[index].netId[iVeh])
						
			PRINTLN("[BIKER_SELL] [CRT] - CREATE_DROPPED_CRATE, CRATE_MADE vCrate = ", vCrate, " index = ", index, " iVeh = ", iVeh)
		ELSE
			PRINTLN("[BIKER_SELL] [CRT] - CAN_REGISTER_MISSION_PICKUPS Failing! vCrate = ", vCrate, " index = ", index, " iVeh = ", iVeh)
		ENDIF
    ENDIF
	
//	IF BIKER_SELL_POSTMAN()
//		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[index].netId[iVeh])
//			PED_INDEX pedId
//			pedId = GET_PLAYER_PED(serverBD.playerRequestedCrate)
//			IF DOES_ENTITY_EXIST(pedId)
//			AND NOT IS_ENTITY_DEAD(pedId)
//				ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]), pedId, GET_PED_BONE_INDEX(pedId,BONETAG_PH_R_HAND), <<0, 0, 0>>, <<0, 0, 0>>, TRUE, TRUE)
//				PRINTLN("[BIKER_SELL] [CRT]  - CREATE_DROPPED_CRATE, ATTACH_ENTITY_TO_ENTITY CRATE  ")
//			ENDIF
//		ENDIF
//	ENDIF
      
    //Create Chute
	IF SHOULD_CREATE_CHUTE()
		IF CAN_REGISTER_MISSION_OBJECTS(1)
		    IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[index].netId[iVeh])
			    IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[index].niChute[iVeh])
					vChuteCoords = GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]))
		            IF CREATE_NET_OBJ(serverBD.sContraband[index].niChute[iVeh], modelCHUTE(), ( vChuteCoords + <<0, 0, 1>>) )  
		                //SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.sContraband[index].niChute[iVeh], TRUE)
		          
		                PRINTLN("[BIKER_SELL] [CRT]  - CREATE_DROPPED_CRATE, CREATED CHUTE FOR CRATE vChuteCoords = ", vChuteCoords)
		          
				  		IF BIKER_SELL_HELICOPTER_DROP()
							SET_ENTITY_COLLISION(NET_TO_OBJ(serverBD.sContraband[index].niChute[iVeh]), FALSE)
						ENDIF
				  
		                ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(serverBD.sContraband[index].niChute[iVeh]), NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]), 0, <<0, 0, 0>>, <<0, 0, 0>>)
		                SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(NET_TO_OBJ(serverBD.sContraband[index].niChute[iVeh]), TRUE)
		                PLAY_ENTITY_ANIM(NET_TO_OBJ(serverBD.sContraband[index].niChute[iVeh]), "P_cargo_chute_S_deploy", "P_cargo_chute_S", INSTANT_BLEND_IN, FALSE, FALSE)
		                FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_OBJ(serverBD.sContraband[index].niChute[iVeh]))
		                
		                SET_ENTITY_LOD_DIST(NET_TO_OBJ(serverBD.sContraband[index].niChute[iVeh]), 1200)
		    
						START_NET_TIMER(serverBD.sContraband[index].CreatedTimer[iVeh])
						RESET_NET_TIMER(serverBD.sContraband[index].InAirCheckDelayTimer[iVeh])
		            ENDIF
			    ENDIF
		    ENDIF
	      
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[index].netId[iVeh])
			AND NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[index].niChute[iVeh])
			    RETURN TRUE
			ENDIF
		ELSE
			PRINTLN("[BIKER_SELL] [CRT] - CAN_REGISTER_MISSION_OBJECTS Failing! vCrate = ", vCrate, " index = ", index, " iVeh = ", iVeh)
		ENDIF
	ELSE
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[index].netId[iVeh])
		    RETURN TRUE
		ENDIF
	ENDIF
	  
	RETURN FALSE
ENDFUNC

PROC SERVER_SET_TRASH_VISIBLE() // url:bugstar:2972373 - Trashmaster - Can we please adjust the bags drop from the back; currently dropping out too soon / far away from back of truck
	IF BIKER_SELL_TRASHMASTER()	
//		PRINTLN("SERVER_SET_TRASH_VISIBLE, 1 ")
	    IF DOES_ENTITY_EXIST(serverBD.eiTrash)
		AND NOT IS_ENTITY_DEAD(serverBD.eiTrash)
//			PRINTLN("SERVER_SET_TRASH_VISIBLE, 2 ")
			IF IS_ENTITY_VISIBLE_TO_SCRIPT(serverBD.eiTrash)
				IF HAS_NET_TIMER_STARTED(serverBD.stTrashVisible)
					RESET_NET_TIMER(serverBD.stTrashVisible)
					PRINTLN("SERVER_SET_TRASH_VISIBLE, RESET_NET_TIMER ")
				ENDIF
//				PRINTLN("SERVER_SET_TRASH_VISIBLE, 3 ")
			ELSE
				IF HAS_NET_TIMER_STARTED(serverBD.stTrashVisible)
					IF HAS_NET_TIMER_EXPIRED(serverBD.stTrashVisible, 500)
						SET_ENTITY_VISIBLE(serverBD.eiTrash, TRUE) // Set invisible within CREATE_DROPPED_CRATE
						PRINTLN("SERVER_SET_TRASH_VISIBLE, SET_ENTITY_VISIBLE ")
					ENDIF
				ELSE
					START_NET_TIMER(serverBD.stTrashVisible)
					PRINTLN("SERVER_SET_TRASH_VISIBLE, START_NET_TIMER ")
				ENDIF
//				PRINTLN("SERVER_SET_TRASH_VISIBLE, 4 ")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL ROTATION_IS_TOO_FAR(INT i, INT iVeh)
	FLOAT fRot = GET_ENTITY_PITCH(NET_TO_OBJ(serverBD.sContraband[i].netId[iVeh]))
	
	IF fRot > 10
	OR fRot < -10
		PRINTLN("[BIKER_SELL] [CRT]  - ROTATION_IS_TOO_FAR - PITCH")
		RETURN TRUE
	ENDIF
	
	fRot = GET_ENTITY_ROLL(NET_TO_OBJ(serverBD.sContraband[i].netId[iVeh]))
	IF fRot > 10
	OR fRot < -10
		PRINTLN("[BIKER_SELL] [CRT]  - ROTATION_IS_TOO_FAR - ROLL")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Plays an anim when the package lands
FUNC BOOL SHOULD_LAND_ANIM_START(INT i, INT iVeh)
	IF HAS_NET_TIMER_EXPIRED(serverBD.sContraband[i].InAirCheckDelayTimer[iVeh], 3000)
		IF NOT IS_ENTITY_IN_AIR(NET_TO_OBJ(serverBD.sContraband[i].netId[iVeh]))
//			PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.sContraband[i].netId[iVeh]), FALSE)
			PRINTLN("[BIKER_SELL] [CRT]  - SHOULD_LAND_ANIM_START - TRUE - NOT IS_ENTITY_IN_AIR")
			RETURN TRUE
		ENDIF
	ENDIF
	IF IS_ENTITY_IN_WATER(NET_TO_OBJ(serverBD.sContraband[i].netId[iVeh]))
//		PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.sContraband[i].netId[iVeh]), FALSE)
		PRINTLN("[BIKER_SELL] [CRT]  - SHOULD_LAND_ANIM_START - TRUE - IS_ENTITY_IN_WATER")
		RETURN TRUE
	ENDIF
	IF ROTATION_IS_TOO_FAR(i, iVeh)
//		PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.sContraband[i].netId[iVeh]), FALSE)
		PRINTLN("[BIKER_SELL] [CRT]  - SHOULD_LAND_ANIM_START - TRUE - ROTATION_IS_TOO_FAR")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC CLEANUP_CRATES()
	INT i
	INT iVeh
	ENTITY_INDEX eToClean
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iVeh
		REPEAT ciMAX_CRATES i
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[i].niChute[iVeh])
			AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sContraband[i].niChute[iVeh])
//				DELETE_NET_ID(serverBD.sContraband[i].niChute[iVeh])
				eToClean = NET_TO_ENT(serverBD.sContraband[i].niChute[iVeh])
				SET_ENTITY_AS_NO_LONGER_NEEDED(eToClean)
			ENDIF
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[i].netId[iVeh])
			AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sContraband[i].netId[iVeh])
//				DELETE_NET_ID(serverBD.sContraband[i].netId[iVeh])
				eToClean = NET_TO_ENT(serverBD.sContraband[i].netId[iVeh])
				SET_ENTITY_AS_NO_LONGER_NEEDED(eToClean)
			ENDIF
		ENDREPEAT
	ENDREPEAT
ENDPROC

////PURPOSE: Plays an anim when the package lands - ALSO DOES DELETION AND TIMEOUT CHECKS
PROC CONTROL_CRATE_LANDING()
	 
	VECTOR vCratePos
	FLOAT vCrateGroundZ
//	ENTITY_INDEX eiCrate
	OBJECT_INDEX objCrate
	OBJECT_INDEX objChute
	
	INT i
	INT iVeh
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iVeh
		REPEAT ciMAX_CRATES i
	  
		 	//CRATE CHECKS
		  	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[i].netId[iVeh])
		 	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sContraband[i].netId[iVeh])
			
	//			eiCrate = NET_TO_ENT(serverBD.sContraband[i].netId[iVeh])
			
	//			PRINTLN("CONTROL_CRATE_LANDING a i = ", i)
				
				IF TAKE_CONTROL_OF_NET_ID(serverBD.sContraband[i].netId[iVeh])			
					APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(NET_TO_ENT(serverBD.sContraband[i].netId[iVeh]), APPLY_TYPE_FORCE, <<0,0,5>>, 0, FALSE, TRUE)
				ENDIF
							
		    	IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sContraband[i].netId[iVeh])
		        OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sContraband[i].netId[iVeh]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
				
					//CHUTE CHECKS
					IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[i].niChute[iVeh])
					AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sContraband[i].niChute[iVeh])  
					
						objCrate = NET_TO_OBJ(serverBD.sContraband[i].netId[iVeh])
						objChute = NET_TO_OBJ(serverBD.sContraband[i].niChute[iVeh])
					
	//					PRINTLN("CONTROL_CRATE_LANDING b i = ", i)
		                    
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sContraband[i].niChute[iVeh])
						OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sContraband[i].niChute[iVeh]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
						
	//						PRINTLN("CONTROL_CRATE_LANDING c i = ", i)
		                          
							IF TAKE_CONTROL_OF_NET_ID(serverBD.sContraband[i].netId[iVeh])
							AND TAKE_CONTROL_OF_NET_ID(serverBD.sContraband[i].niChute[iVeh])
							
	//							PRINTLN("CONTROL_CRATE_LANDING d i = ", i)
		                                
								//Delete Chute when Crumple is done
								IF NOT IS_BIT_SET(serverBD.sContraband[i].iCrateBS[iVeh], CRATE_BS_ANIM_DONE)
								AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iVeh], CRATE_BS_ANIM_DONE)
									IF IS_BIT_SET(serverBD.sContraband[i].iCrateBS[iVeh], CRATE_BS_ANIM_PLAYING)
									OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iVeh], CRATE_BS_ANIM_PLAYING)
										IF DOES_ENTITY_EXIST(objChute)
											IF NOT IS_ENTITY_PLAYING_ANIM(objChute, "P_cargo_chute_S", "P_cargo_chute_S_crumple")
												SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iVeh], CRATE_BS_ANIM_DONE)
												//SET_BIT(serverBD.sContraband[i].iCrateBS[iVeh], CRATE_BS_ANIM_DONE)
												DELETE_NET_ID(serverBD.sContraband[i].niChute[iVeh])
//												PREVENT_COLLECTION_OF_PORTABLE_PICKUP(objCrate, FALSE)
												PRINTLN("[BIKER_SELL] [CRT]  - CONTROL_CRATE_LANDING - PREVENT_COLLECTION_OF_PORTABLE_PICKUP - FALSE - B - ", i)
												PRINTLN("[BIKER_SELL] [CRT]  - CONTROL_CRATE_LANDING - LAND ANIM DONE - CHUTE DELETED - ", i) 
												
												CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iVeh], CRATE_BS_ANIM_STARTED )
												CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iVeh], CRATE_BS_ANIM_PLAYING )
												CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iVeh], CRATE_BS_ANIM_DONE	)
											ENDIF
										ENDIF
									ENDIF
								ELSE
									PRINTLN("CONTROL_CRATE_LANDING a i = ", i)
								ENDIF
								
								//Check Crumple is playing
								IF NOT IS_BIT_SET(serverBD.sContraband[i].iCrateBS[iVeh], CRATE_BS_ANIM_PLAYING)
								AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iVeh], CRATE_BS_ANIM_PLAYING)
									IF IS_BIT_SET(serverBD.sContraband[i].iCrateBS[iVeh], CRATE_BS_ANIM_STARTED)
									OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iVeh], CRATE_BS_ANIM_STARTED)
										IF DOES_ENTITY_EXIST(objChute)
											IF IS_ENTITY_PLAYING_ANIM(objChute, "P_cargo_chute_S", "P_cargo_chute_S_crumple")
												SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iVeh], CRATE_BS_ANIM_PLAYING)
												//SET_BIT(serverBD.sContraband[i].iCrateBS[iVeh], CRATE_BS_ANIM_STARTED)
												PRINTLN("[BIKER_SELL] [CRT]  - CONTROL_CRATE_LANDING - LAND ANIM P_cargo_chute_S_crumple -  PLAYING - ", i)
											ENDIF
										ENDIF
									ENDIF
								ELSE
									PRINTLN("CONTROL_CRATE_LANDING b i = ", i)
								ENDIF
										
		                        //Start Crumple
		                        IF NOT IS_BIT_SET(serverBD.sContraband[i].iCrateBS[iVeh], CRATE_BS_ANIM_STARTED)
	//	                        AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iVeh], CRATE_BS_ANIM_STARTED)
									IF SHOULD_LAND_ANIM_START(i, iVeh)
										PLAY_ENTITY_ANIM(objChute, "P_cargo_chute_S_crumple", "P_cargo_chute_S", INSTANT_BLEND_IN, FALSE, FALSE) //TRUE)
//										SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS, CRATE_BS_ANIM_STARTED)
										SET_BIT(serverBD.sContraband[i].iCrateBS[iVeh], CRATE_BS_ANIM_STARTED)
										PRINTLN("[BIKER_SELL] [CRT]  - CONTROL_CRATE_LANDING - LAND ANIM STARTED  P_cargo_chute_S_crumple  STARTED - ", i)
										PRINTLN("[BIKER_SELL] [CRT]  - CONTROL_CRATE_LANDING - NETID = ", NATIVE_TO_INT(serverBD.sContraband[i].niChute[iVeh]))
										PLAY_SOUND_FROM_ENTITY(sLocaldata.iChuteSoundId, "Parachute_Land", objChute, "DLC_Exec_Air_Drop_Sounds")
								
									

									ELSE
										SET_DAMPING(objCrate, PHYSICS_DAMPING_LINEAR_V2, 0.0245)
									ENDIF
								ELSE
									PRINTLN("CONTROL_CRATE_LANDING c i = ", i)
								ENDIF
								
		                    ENDIF
						ENDIF
					ENDIF
		            
					  
					//Check Crate is above ground
					IF DOES_ENTITY_EXIST(objCrate)
						vCratePos = GET_ENTITY_COORDS(objCrate)
						IF GET_GROUND_Z_FOR_3D_COORD(vCratePos, vCrateGroundZ) 
							IF vCratePos.z < vCrateGroundZ
							AND NOT IS_ENTITY_IN_WATER(objCrate)
								IF TAKE_CONTROL_OF_NET_ID(serverBD.sContraband[i].netId[iVeh])
									SET_ENTITY_COORDS(objCrate, <<vCratePos.x, vCratePos.y, vCrateGroundZ>>)
									PRINTLN("[BIKER_SELL] [CRT]  - CONTROL_CRATE_LANDING - NORMAL CRATE BELOW GROUND - WARP BACK UP ") 
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_THIS_A_LAND_VARIATION()
						IF NOT HAS_NET_TIMER_STARTED(serverBD.sContraband[i].timeCleanup[iVeh])
							START_NET_TIMER(serverBD.sContraband[i].timeCleanup[iVeh])
							PRINTLN("[BIKER_SELL] [CRT] Started crate cleanup timer Contra ", i, " Veh ", iVeh)
						ELSE
							IF HAS_NET_TIMER_EXPIRED(serverBD.sContraband[i].timeCleanup[iVeh], 30000)
								PRINTLN("[BIKER_SELL] [CRT] Cleanup timer expired Contra ", i, " Veh ", iVeh)
								CLEANUP_NET_ID(serverBD.sContraband[i].netId[iVeh])
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
ENDPROC

PROC CLEANUP_CRATE_DROP_ALARM()
	IF sLocaldata.iDropAlarm != -1
		PRINTLN("[BIKER_SELL]  [SELL AUDIO] - [DROP] CLEANUP_CRATE_DROP_ALARM - STOP_SOUND(", sLocaldata.iDropAlarm, ")")
		STOP_SOUND(sLocaldata.iDropAlarm)
		RELEASE_SOUND_ID(sLocaldata.iDropAlarm)
		sLocaldata.iDropAlarm = -1
		PRINTLN("[BIKER_SELL]  [SELL AUDIO] - [DROP] CLEANUP_CRATE_DROP_ALARM - sLocaldata.iDropAlarm = -1")
		DEBUG_PRINTCALLSTACK()
	ENDIF
ENDPROC

FUNC STRING GET_SOUNDSET_FOR_CRATE_DROP()

	SWITCH GET_SELL_VAR()
		CASE eBIKER_SELL_TRASHMASTER
			RETURN "DLC_Biker_Sell_Trash_Sounds"
			
		CASE eBIKER_SELL_POSTMAN
			RETURN "DLC_Biker_Sell_Postman_Sounds"
	
		CASE eBIKER_SELL_HELICOPTER_DROP
			RETURN "DLC_Biker_Air_Drop_Sounds"
			
		CASE eBIKER_SELL_FRIENDS_IN_NEED
		CASE eBIKER_SELL_BENSON
			RETURN "GTAO_Biker_Modes_Soundset"
			
	ENDSWITCH
	
	IF MODE_HAS_PICKUP_BAGS()
		RETURN "GTAO_Biker_Modes_Soundset"
	ENDIF

	RETURN "DLC_Biker_Sell_Sea_Sounds"
ENDFUNC

FUNC STRING GET_SOUNDNAME()

	SWITCH GET_SELL_VAR()			
		CASE eBIKER_SELL_FRIENDS_IN_NEED
		CASE eBIKER_SELL_BENSON
			RETURN "Deliver_Item"
	ENDSWITCH
	
	IF MODE_HAS_PICKUP_BAGS()
		RETURN "Deliver_Item"
	ENDIF

	RETURN "Drop_Package"
ENDFUNC

PROC PLAY_BIKER_DROP_SOUND()
	PRINTLN("[BIKER_SELL] [SELL AUDIO] - [DROP] PLAY_BIKER_DROP_SOUND, GET_SOUNDNAME = ", GET_SOUNDNAME(), " GET_SOUNDSET_FOR_CRATE_DROP() = ", GET_SOUNDSET_FOR_CRATE_DROP())
	PLAY_SOUND_FRONTEND(-1, GET_SOUNDNAME(), GET_SOUNDSET_FOR_CRATE_DROP(), FALSE)
ENDPROC

PROC PLAY_AIR_DROP_ALARM_SOUND()
	IF IS_THIS_AN_AIR_VARIATION()
		IF sLocaldata.iDropAlarm = -1
			sLocaldata.iDropAlarm = GET_SOUND_ID()
			PLAY_SOUND_FRONTEND(sLocaldata.iDropAlarm, "Drop_Zone_Alarm", GET_SOUNDSET_FOR_CRATE_DROP(), FALSE)
			PRINTLN("[BIKER_SELL]  [SELL AUDIO] - [DROP]  PLAY_AIR_DROP_ALARM_SOUND, PLAY_SOUND_FRONTEND(", sLocaldata.iDropAlarm, ",\"Drop_Zone_Alarm\",\DLC_Exec_Air_Drop_Sounds\",FALSE)")
			DEBUG_PRINTCALLSTACK()
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL bSHOULD_PLAY_MEXICAN_BEATS(INT iVeh)

	IF serverBD.eDoorState[iVeh] = eSELL_DOOR_STATE_OPEN 
	AND ( GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), VEH_ID(iVeh)) < 50.00 )
	AND IS_SELL_VEH_OK(iVeh)
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				THE DOORS														║

FUNC BOOL bSHOULD_OPEN_DOOR()
	
	SWITCH GET_SELL_VAR()
		CASE eBIKER_SELL_TRASHMASTER		
//		CASE eBIKER_SELL_PROVEN				
//		CASE eBIKER_SELL_TACO_VAN			
		CASE eBIKER_SELL_POSTMAN
//		CASE eBIKER_SELL_MEDICAL_PURPOSES
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL bSHOULD_OPEN_SECOND_DOOR()
	
	SWITCH GET_SELL_VAR()		
		CASE eBIKER_SELL_POSTMAN
//		CASE eBIKER_SELL_MEDICAL_PURPOSES
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC SC_DOOR_LIST DOOR_TO_OPEN()//(BOOL bSecondDoor = FALSE)
//	SWITCH GET_SELL_VAR()		
//		CASE eBIKER_SELL_POSTMAN
//		CASE eBIKER_SELL_MEDICAL_PURPOSES
//			IF bSecondDoor
//				RETURN SC_DOOR_REAR_RIGHT
//			ELSE
//				RETURN SC_DOOR_REAR_LEFT
//			ENDIF
//		BREAK
//	ENDSWITCH

	RETURN SC_DOOR_BOOT
ENDFUNC

PROC OPEN_VEH_DOORS(INT iVeh)
	SET_VEHICLE_DOOR_OPEN(VEH_ID(iVeh), DOOR_TO_OPEN())
	IF bSHOULD_OPEN_SECOND_DOOR()
		SET_VEHICLE_DOOR_OPEN(VEH_ID(iVeh), DOOR_TO_OPEN())
	ENDIF
ENDPROC

PROC CLOSE_VEH_DOORS(INT iVeh)
	SET_VEHICLE_DOOR_SHUT(VEH_ID(iVeh), DOOR_TO_OPEN(), FALSE)
	IF bSHOULD_OPEN_SECOND_DOOR()
		SET_VEHICLE_DOOR_SHUT(VEH_ID(iVeh), DOOR_TO_OPEN(), FALSE)
	ENDIF
ENDPROC

FUNC BOOL bOPEN_DOOR_RIGHT_AWAY()

//	IF BIKER_SELL_TACO_VAN()	
//	
//		RETURN TRUE
//	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL bVEH_DOOR_OPEN(INT iParticipant, INT iVeh)

	RETURN ( playerBD[iParticipant].eDoorState[iVeh] = eSELL_DOOR_STATE_OPEN )
ENDFUNC

PROC CLIENT_SET_DOORS_OPEN(BOOL bOpen, INT iVeh)
	IF bOpen		
		IF playerBD[PARTICIPANT_ID_TO_INT()].eDoorState[iVeh] <> eSELL_DOOR_STATE_OPEN
			PRINTLN("CLIENT_SET_DOORS_OPEN, [TACO] OPEN for iVeh = ", iVeh)
			playerBD[PARTICIPANT_ID_TO_INT()].eDoorState[iVeh] = eSELL_DOOR_STATE_OPEN
		ENDIF
	ELSE
		IF playerBD[PARTICIPANT_ID_TO_INT()].eDoorState[iVeh] <> eSELL_DOOR_STATE_CLOSE
			PRINTLN("CLIENT_SET_DOORS_OPEN, [TACO] CLOSE for iVeh = ", iVeh)
			playerBD[PARTICIPANT_ID_TO_INT()].eDoorState[iVeh] = eSELL_DOOR_STATE_CLOSE
		ENDIF
	ENDIF
ENDPROC

PROC SERVER_SET_OPEN_DOORS(INT iParticipant, INT iVeh, BOOL bCleanup = FALSE)	
	IF bCleanup
	OR playerBD[iParticipant].eDoorState[iVeh] = eSELL_DOOR_STATE_CLOSE
		IF serverBD.eDoorState[iVeh] <> eSELL_DOOR_STATE_CLOSE
			serverBD.eDoorState[iVeh] = eSELL_DOOR_STATE_CLOSE
			PRINTLN("SERVER_SET_OPEN_DOORS, [TACO] VEH CLOSED ", iVeh)
		ENDIF
	ELIF playerBD[iParticipant].eDoorState[iVeh] = eSELL_DOOR_STATE_OPEN
		IF serverBD.eDoorState[iVeh] <> eSELL_DOOR_STATE_OPEN
			serverBD.eDoorState[iVeh] = eSELL_DOOR_STATE_OPEN
			PRINTLN("SERVER_SET_OPEN_DOORS, [TACO] VEH OPEN ", iVeh)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_DOOR_OPENING(INT iVeh)
	IF bSHOULD_OPEN_DOOR()
		IF bVEH_DOOR_OPEN(PARTICIPANT_ID_TO_INT(), iVeh)
//		OR bOPEN_DOOR_RIGHT_AWAY()		
			IF NOT HAS_NET_TIMER_STARTED(sLocaldata.stDoor)
				START_NET_TIMER(sLocaldata.stDoor)
			ELSE
				OPEN_VEH_DOORS(iVeh)
//				PRINTLN("MAINTAIN_DOOR_OPENING, OPEN_VEH_DOORS ")
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(sLocaldata.stDoor)
			OR bOPEN_DOOR_RIGHT_AWAY()
				IF HAS_NET_TIMER_EXPIRED(sLocaldata.stDoor, 3000)
				OR bOPEN_DOOR_RIGHT_AWAY()
					RESET_NET_TIMER(sLocaldata.stDoor)
					CLOSE_VEH_DOORS(iVeh)
//					PRINTLN("MAINTAIN_DOOR_OPENING, CLOSE_VEH_DOORS ")
				ELSE
					OPEN_VEH_DOORS(iVeh)
//					PRINTLN("MAINTAIN_DOOR_OPENING, OPEN_VEH_DOORS 1")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//╚═════════════════════════════════════════════════════════════════════════════╝	

CONST_INT ciTHROW_DIRECTION_LEFT 		0
CONST_INT ciTHROW_DIRECTION_RIGHT 		1
CONST_INT ciTHROW_DIRECTION_FORWARD 	2
CONST_INT ciTHROW_DIRECTION_BACK 		3

#IF IS_DEBUG_BUILD
FUNC STRING GET_THROW_DIRECTION_STRING(INT iDirection)
	SWITCH iDirection
		CASE ciTHROW_DIRECTION_LEFT			RETURN "ciTHROW_DIRECTION_LEFT"
		CASE ciTHROW_DIRECTION_RIGHT		RETURN "ciTHROW_DIRECTION_RIGHT"
		CASE ciTHROW_DIRECTION_BACK			RETURN "ciTHROW_DIRECTION_BACK"
		CASE ciTHROW_DIRECTION_FORWARD		RETURN "ciTHROW_DIRECTION_FORWARD"
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC
#ENDIF

FUNC STRING GET_ANIM_DICT_FOR_THROW_DIRECTION(INT iDirection)

	IF iDirection = ciTHROW_DIRECTION_LEFT
		RETURN "veh@drivebystd_ds_grenades"
	ELIF iDirection = ciTHROW_DIRECTION_RIGHT
	OR iDirection = ciTHROW_DIRECTION_FORWARD
	OR iDirection = ciTHROW_DIRECTION_BACK
		RETURN "veh@drivebystd_ps_grenades"
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_THROW_ANIM(INT iDirection)
	SWITCH iDirection
		CASE ciTHROW_DIRECTION_LEFT
		CASE ciTHROW_DIRECTION_RIGHT
			RETURN "throw"
			
		CASE ciTHROW_DIRECTION_BACK
			RETURN "drop_grenade"
			
		CASE ciTHROW_DIRECTION_FORWARD
			RETURN "throw_0"
	ENDSWITCH

	RETURN ""
ENDFUNC

PROC PLAY_THROW_ID_ANIM(INT iDrop)
	
	INT iDirection
	STRING strDict, strAnim
	FLOAT fHeading

	// Calculate direction to throw

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffVeh[iDrop])
		IF IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.niDropOffVeh[iDrop]))
			fHeading = GET_HEADING_FROM_ENTITIES_LA(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niDropOffVeh[iDrop]))
			
			IF fHeading > 315 OR fHeading <= 45
				iDirection = ciTHROW_DIRECTION_FORWARD
			ELIF fHeading > 45 AND fHeading <= 135
				iDirection = ciTHROW_DIRECTION_RIGHT
			ELIF fHeading > 135 AND fHeading <= 225
				iDirection = ciTHROW_DIRECTION_BACK
			ELIF fHeading > 225 AND fHeading < 315
				iDirection = ciTHROW_DIRECTION_LEFT
			ENDIF
		ENDIF
	ENDIF

	// Do the throwing
	strDict = GET_ANIM_DICT_FOR_THROW_DIRECTION(iDirection)
	strAnim = GET_THROW_ANIM(iDirection)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(strDict)
	AND NOT IS_STRING_NULL_OR_EMPTY(strAnim)
		IF HAS_ANIM_DICT_LOADED(strDict)
			PRINTLN("[BIKER_SELL] - PLAY_THROW_ID_ANIM - Playing animation; throw direction:", GET_THROW_DIRECTION_STRING(iDirection))
			TASK_PLAY_ANIM(PLAYER_PED_ID(), strDict, strAnim)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_SAFE_FOR_CRATE_DROPPING(INT iVeh)

	BOOL bToReturn = FALSE

	#IF IS_DEBUG_BUILD 
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_GBAlwaysAllowDrop") 
		bToReturn = TRUE
	ENDIF
	#ENDIF
	
	IF NOT IS_THIS_AN_AIR_VARIATION()
		bToReturn = TRUE
	ENDIF

	IF IS_THIS_AN_AIR_VARIATION()
	AND IS_ENTITY_IN_AIR(VEH_ENT(iVeh))
		bToReturn = TRUE
	ENDIF
	
	RETURN bToReturn
ENDFUNC

FUNC STRING sDROP_BTN_TEXT(INT iVeh = -1)

	IF BIKER_SELL_POSTMAN()
	
		RETURN "SBIKER_POST1"
	ENDIF
	
//	IF BIKER_SELL_TACO_VAN()
//		RETURN "SBIKER_TDROP"
//	ENDIF
	
	IF BIKER_SELL_FRIENDS_IN_NEED()
		RETURN "SBIKER_FDROP"
	ENDIF

	IF BIKER_SELL_HELICOPTER_DROP()
	OR BIKER_SELL_AIR_DROP_AT_SEA()
		IF iVeh > -1
			IF (IS_DRIVER_OF_SELL_VEH(iVeh) AND NOT DOES_SELL_VEH_HAVE_PASSENGER(iVeh, TRUE))
			OR (IS_PASSENGER(iVeh, TRUE) AND IS_SELL_VEH_OCCUPIED(iVeh))
				RETURN "SBIKER_DROP"
			ELIF (IS_DRIVER_OF_SELL_VEH(iVeh) AND DOES_SELL_VEH_HAVE_PASSENGER(iVeh, TRUE))
				RETURN "SBIKER_DROP2"
			ENDIF
		ENDIF
	ENDIF

	RETURN "SBIKER_DROP"
ENDFUNC

FUNC BOOL bBOOT_IN_RANGE(ENTITY_INDEX eiVeh, VECTOR vDrop)

	STRING sBone = "platelight"
	INT iBone
	VECTOR vBone

	SWITCH GET_SELL_VAR()
		CASE eBIKER_SELL_TRASHMASTER
			iBone = GET_ENTITY_BONE_INDEX_BY_NAME(eiVeh, sBone)
			vBone = GET_ENTITY_BONE_POSTION(eiVeh, iBone)
			
			RETURN ( GET_DISTANCE_BETWEEN_COORDS(vBone, vDrop) < cfSELL_TRASH_SIZE )
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC DO_TRASH_HELP(BOOL bYes)
	SWITCH GET_SELL_VAR()
		CASE eBIKER_SELL_TRASHMASTER
			IF bYes
//				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SBIKERT_HREAR")
					// Navigate the rear of the Trashmaster into the ~y~drop-off~s~ area to drop the bag of weed.
//					PRINT_HELP("SBIKERT_HREAR")
					PRINT_HELP_WITH_STRING("SBIKERT_HREAR", GET_SHIPMENT_TYPE_STRING())
//				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("SBIKERT_HREAR", GET_SHIPMENT_TYPE_STRING())
					CLEAR_HELP()
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL bPOST_VEH_PARKED(INT iDropOffVeh)
	
	IF BIKER_SELL_POSTMAN()
	
		RETURN IS_BIT_SET(serverBD.iPostVehParkedForDeliveryBitSet, iDropOffVeh)
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL bCLOSEST_VEH_PARKED()

	INT iDropOffVeh
	
	IF BIKER_SELL_POSTMAN()
	
		iDropOffVeh = GET_CLOSEST_SELL_VEH_TO_LOCAL_PLAYER(TRUE, FALSE, TRUE)
		
		PRINTLN("bCLOSEST_VEH_PARKED, A iDropOffVeh = ", iDropOffVeh)
		
		IF iDropOffVeh <> -1
		
			IF IS_BIT_SET(serverBD.iPostVehParkedForDeliveryBitSet, iDropOffVeh)
				PRINTLN("bCLOSEST_VEH_PARKED, B iDropOffVeh = ", iDropOffVeh)
			ENDIF
	
			RETURN IS_BIT_SET(serverBD.iPostVehParkedForDeliveryBitSet, iDropOffVeh)
		ENDIF
	ENDIF
	
	PRINTLN("bCLOSEST_VEH_PARKED, FALSE iDropOffVeh = ", iDropOffVeh)

	RETURN FALSE
ENDFUNC

PROC SET_POSTMAN_VEH(INT iVeh)
	IF NOT IS_BIT_SET(serverBD.iPostVehParkedForDeliveryBitSet, iVeh)
		SET_BIT(serverBD.iPostVehParkedForDeliveryBitSet, iVeh)
		PRINTLN("SERVER_SET_POSTMAN, [POST] SET_POSTMAN_VEH, iVeh = ", iVeh)
	ENDIF
ENDPROC

PROC CLEAR_POSTMAN_VEH(INT iVeh)
	IF IS_BIT_SET(serverBD.iPostVehParkedForDeliveryBitSet, iVeh)
		CLEAR_BIT(serverBD.iPostVehParkedForDeliveryBitSet, iVeh)
		PRINTLN("SERVER_SET_POSTMAN, [POST] CLEAR_POSTMAN_VEH, iVeh = ", iVeh)
	ENDIF
ENDPROC

FUNC BOOL bIS_POSTMAN_PAT()
	IF BIKER_SELL_POSTMAN()

		INT iVeh = GET_SELL_VEH_I_AM_IN_INT()
		
		IF iVeh = -1
			iVeh = sLocaldata.iStoredPostVeh
		ENDIF
		
		IF iVeh <> -1
			RETURN IS_BIT_SET(serverBD.iPostManPat[iVeh], NETWORK_PLAYER_ID_TO_INT())
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL bIS_PLAYER_POSTMAN(INT iVeh, INT iPlayer)
	RETURN IS_BIT_SET(serverBD.iPostManPat[iVeh], iPlayer)
ENDFUNC

PROC SERVER_SET_POSTMAN(BOOL bSet, PLAYER_INDEX playerId, INT iVeh)

	INT iPlayer
	
	IF playerId <> INVALID_PLAYER_INDEX()
	
		iPlayer = NATIVE_TO_INT(playerId)

		IF bSet
			IF NOT IS_BIT_SET(serverBD.iPostManPat[iVeh], iPlayer)
				SET_BIT(serverBD.iPostManPat[iVeh], iPlayer)
				PRINTLN("SERVER_SET_POSTMAN, [POST] SET_BIT, iVeh = ", iVeh, " playerId = ", GET_PLAYER_NAME(playerId))
			ENDIF
		ELSE
			IF IS_BIT_SET(serverBD.iPostManPat[iVeh], iPlayer)
				CLEAR_BIT(serverBD.iPostManPat[iVeh], iPlayer)
				PRINTLN("SERVER_SET_POSTMAN, [POST] CLEAR_BIT, iVeh = ", iVeh, " playerId = ", GET_PLAYER_NAME(playerId))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL bSHOW_DROP_TEXT()

	IF BIKER_SELL_FRIENDS_IN_NEED()
		IF bVEH_DOOR_OPEN(PARTICIPANT_ID_TO_INT(), GET_SELL_VEH_I_AM_IN_INT())
		
			RETURN FALSE
		ELSE
		
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF BIKER_SELL_POSTMAN()
		
		RETURN FALSE
	ENDIF
	
	IF BIKER_SELL_BORDER_PATROL()
		
		RETURN FALSE
	ENDIF
	
	IF BIKER_SELL_TRASHMASTER()
		
		RETURN IS_DRIVER_OF_ANY_SELL_VEH()
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC FREEZE_VEHICLE_IN_PLACE(VEHICLE_INDEX vehID, INT iVeh, BOOL bTrue)
	SWITCH GET_SELL_VAR()
//		CASE eBIKER_SELL_TACO_VAN
//			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh[iVeh])
//				IF bTrue
//					IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehID)
//						FREEZE_ENTITY_POSITION(vehID, TRUE)
//						PRINTLN("[BIKER_SELL] [TACO] FREEZE_VEHICLE_IN_PLACE, TRUE ", iVeh)
//					ENDIF
//				ELSE
//					FREEZE_ENTITY_POSITION(vehID, FALSE)
//					PRINTLN("[BIKER_SELL] [TACO] FREEZE_VEHICLE_IN_PLACE, FALSE ", iVeh)
//				ENDIF
//			ENDIF
//		BREAK
		CASE eBIKER_SELL_POSTMAN
			IF iVeh <> -1
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh[iVeh])
					IF bTrue
						IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehID)
							FREEZE_ENTITY_POSITION(vehID, TRUE)
							PRINTLN("[BIKER_SELL] [TACO] FREEZE_VEHICLE_IN_PLACE, TRUE ", iVeh)
						ENDIF
					ELSE
						IF NOT bPOST_VEH_PARKED(iVeh)
							FREEZE_ENTITY_POSITION(vehID, FALSE)
							PRINTLN("[BIKER_SELL] [TACO] FREEZE_VEHICLE_IN_PLACE, FALSE ", iVeh)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC BLOCK_PED_SHUFFLE(BOOL bBlock)
	IF bBlock
		IF NOT IS_LOCAL_BIT1_SET(eLOCALBITSET1_SHUFFLE_BLOCKED)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, TRUE)
			PRINTLN("[BIKER_SELL] [POST] BLOCK_PED_SHUFFLE, TRUE ")
			SET_LOCAL_BIT1(eLOCALBITSET1_SHUFFLE_BLOCKED)
		ENDIF
	ELSE	
		IF IS_LOCAL_BIT1_SET(eLOCALBITSET1_SHUFFLE_BLOCKED)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, FALSE)
			PRINTLN("[BIKER_SELL] [POST] BLOCK_PED_SHUFFLE, FALSE ")
			CLEAR_LOCAL_BIT1(eLOCALBITSET1_SHUFFLE_BLOCKED)
		ENDIF
	ENDIF
ENDPROC

PROC DO_DROP_AREA_VEHICLE_TASKS(INT iVeh)
	SWITCH GET_SELL_VAR()
//		CASE eBIKER_SELL_TACO_VAN
//			IF IS_DRIVER_OF_SELL_VEH(iVeh)
//			
//				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_RIGHT)				
//					CLIENT_SET_DOORS_OPEN(TRUE, iVeh)
//				ENDIF
//				
//				IF serverBD.eDoorState[iVeh] = eSELL_DOOR_STATE_OPEN
//					IF NOT HAS_NET_TIMER_STARTED(sLocaldata.stTacoFreeze)
//						START_NET_TIMER(sLocaldata.stTacoFreeze)
//					ELSE
//						IF HAS_NET_TIMER_EXPIRED(sLocaldata.stTacoFreeze, 1500)
//							FREEZE_VEHICLE_IN_PLACE(vehID, iVeh, TRUE)
//						ENDIF
//					ENDIF
//				ELSE
//					IF HAS_NET_TIMER_STARTED(sLocaldata.stTacoFreeze)
//						RESET_NET_TIMER(sLocaldata.stTacoFreeze)
//					ENDIF
//				ENDIF
//			ENDIF
//		BREAK
		CASE eBIKER_SELL_FRIENDS_IN_NEED
			IF IS_DRIVER_OF_SELL_VEH(iVeh)
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_RIGHT)				
					CLIENT_SET_DOORS_OPEN(TRUE, iVeh)
				ENDIF
			ENDIF
		BREAK
		CASE eBIKER_SELL_POSTMAN

		BREAK
	ENDSWITCH
ENDPROC

PROC SHOW_DROP_HELP_PROMPT(INT iVeh = -1)
	SWITCH GET_SELL_VAR()
//		CASE eBIKER_SELL_TACO_VAN
//			PRINT_HELP(sDROP_BTN_TEXT(iVeh))
//		BREAK
		CASE eBIKER_SELL_FRIENDS_IN_NEED
		DEFAULT
			PRINT_HELP_WITH_STRING(sDROP_BTN_TEXT(iVeh), GET_SHIPMENT_TYPE_STRING())
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_SAFE_TO_PROMPT_DROP(INT iVeh)
	IF NOT BIKER_SELL_HELICOPTER_DROP()
		RETURN TRUE
	ENDIF

	FLOAT fHeight
	IF IS_SELL_VEH_OK(iVeh)
		fHeight = GET_ENTITY_HEIGHT_ABOVE_GROUND(VEH_ID(iVeh))
		IF fHeight >= g_sMPTunables.fBIKER_HELICOPTER_DROP_DROP_HEIGHT
			RETURN TRUE
		ELSE
			PRINTLN("[heli drop spam] - not high enough off ground - fHeight: ", fHeight)
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC STRING GET_POST_ANIM_DICT_NAME()

	STRING sReturn 
	
	sReturn = "anim@MP_FIREWORKS"
	
	RETURN sReturn
ENDFUNC

FUNC STRING GET_POST_ANIM_CLIP_NAME()

	STRING sReturn 
	
	sReturn = "PLACE_FIREWORK_BOX2"
	
	RETURN sReturn
ENDFUNC

FUNC BOOL bPOST_ANIMS_LOADED()

	STRING sAnimDictName = GET_POST_ANIM_DICT_NAME()

	REQUEST_ANIM_DICT(sAnimDictName)
	
	RETURN HAS_ANIM_DICT_LOADED(sAnimDictName)
ENDFUNC

PROC DO_POST_ANIM()

	STRING sAnimDictName = GET_POST_ANIM_DICT_NAME()
	STRING sAnimClipName = GET_POST_ANIM_CLIP_NAME()

	IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDictName, sAnimClipName)
		TASK_PLAY_ANIM(PLAYER_PED_ID(), sAnimDictName, sAnimClipName, DEFAULT, DEFAULT, DEFAULT, AF_HIDE_WEAPON)
		PRINTLN("[BIKER_SELL] [POST] DO_POST_ANIM ")
	ENDIF
ENDPROC

FUNC INT iGET_DROP_TARGET_FOR_BAG_REMOVAL(INT iBag)

	INT iDropTarget = 1
	
	IF BIKER_SELL_CLUB_RUN()
		iDropTarget = ciNUMBER_OF_CLUB_RUN_DROPS
	ELIF BIKER_SELL_FRIENDS_IN_NEED()
		iDropTarget = GET_SELL_VEH_TARGET_DROP_COUNT(iBag)
	ENDIF
	
	RETURN iDropTarget
ENDFUNC

FUNC BOOL bSHOULD_DO_BAG_DROP_FLASH_SLOW_AND_TICKER()

	SWITCH GET_SELL_VAR()
		
		CASE eBIKER_SELL_CLUB_RUN
		CASE eBIKER_SELL_FRIENDS_IN_NEED
		CASE eBIKER_SELL_BAG_DROP
		
			RETURN TRUE
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC STRING sBAG_DROP_TICKER()

	SWITCH GET_SELL_VAR()
		
		CASE eBIKER_SELL_CLUB_RUN
			IF serverBD.iDroppedOffCountClubRun = 0
				RETURN "SBIKER_CLB1"
			ELIF serverBD.iDroppedOffCountClubRun = 1
				RETURN "SBIKER_CLB2"
			ELIF serverBD.iDroppedOffCountClubRun = 2
				RETURN "SBIKER_CLB3"
			ENDIF
		BREAK
		
		CASE eBIKER_SELL_FRIENDS_IN_NEED
		
			RETURN "SBIKER_TFIN"
		BREAK
		
		CASE eBIKER_SELL_BAG_DROP
		
			RETURN "SBIKER_TBG"
		BREAK
		
	ENDSWITCH

	RETURN ""
ENDFUNC

PROC CLIENT_MAINTAIN_BAG_DROP_OFFS()

//	INT iBags
	INT iMyBag
	INT iClosest

	IF MODE_HAS_PICKUP_BAGS()
	AND DO_I_HAVE_A_BAG()
		
		iMyBag = iGET_PLAYER_BAG(PLAYER_ID())
		
		IF sLocaldata.iStoredBag = -1
		AND iMyBag <> -1
			sLocaldata.iStoredBag = iMyBag
			PRINTLN("[BIKER_SELL] [BAGS] CLIENT_MAINTAIN_BAG_DROP_OFFS, sLocaldata.iStoredBag = ", sLocaldata.iStoredBag)
		ENDIF
		
		IF iMyBag <> -1
			iClosest = serverBD.iClosestDropOff[iMyBag]
			
			VECTOR vDropCoords = GET_SELL_DROP_COORDS(GET_SELL_VAR(), iClosest, serverBD.iRoute, iMyBag, DEFAULT, FALSE)
			IF NOT IS_BIT_SET(sLocaldata.iBagDropBitSet, iClosest)
				IF ENTITY_AT_DROP_OFF_COORDS(vDropCoords, PLAYER_PED_ID(), TRUE)
					IF BIKER_SELL_RACE()
						IF NOT GB_IS_GLOBAL_CLIENT_BIT1_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_1_FINISHED_RACE)
							IF NOT GB_IS_GLOBAL_CLIENT_BIT1_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_1_KICKED_OFF_RACE)
								GB_SET_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_FINISHED_RACE)
							ENDIF
							BROADCAST_BAG_EVENT(iMyBag, iClosest, serverBD.iLaunchPosix)
						ENDIF
						SET_BIT(sLocaldata.iBagDropBitSet, iClosest)
					ELSE
						BROADCAST_BAG_EVENT(iMyBag, iClosest, serverBD.iLaunchPosix)
						SET_BIT(sLocaldata.iBagDropBitSet, iClosest)
					ENDIF
					PLAY_BIKER_DROP_SOUND()
					
					IF bSHOULD_DO_BAG_DROP_FLASH_SLOW_AND_TICKER()
					OR BIKER_SELL_RACE()

						FLASH_MINIMAP_DISPLAY()
						
						IF NOT BIKER_SELL_RACE()
							PRINT_TICKER(sBAG_DROP_TICKER())
						ENDIF

						IF NOT IS_BIT_SET(sLocaldata.iHaltBitSet, iMyBag)
							IF IS_NET_PLAYER_OK(PLAYER_ID())
							AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								IF DOES_ENTITY_EXIST(veh)
								AND NOT IS_ENTITY_DEAD(veh)
									IF GET_PED_IN_VEHICLE_SEAT(veh, VS_DRIVER) = PLAYER_PED_ID()
										SET_BIT(sLocaldata.iHaltBitSet, iMyBag)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(sLocaldata.iHaltBitSet, iMyBag)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(veh)
					AND NOT IS_ENTITY_DEAD(veh)
						IF GET_PED_IN_VEHICLE_SEAT(veh, VS_DRIVER) = PLAYER_PED_ID()
							IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(veh)
								CLEAR_BIT(sLocaldata.iHaltBitSet, iMyBag)
							ENDIF
						ELSE
							CLEAR_BIT(sLocaldata.iHaltBitSet, iMyBag)
						ENDIF
					ELSE
						CLEAR_BIT(sLocaldata.iHaltBitSet, iMyBag)
					ENDIF
				ELSE
					CLEAR_BIT(sLocaldata.iHaltBitSet, iMyBag)
				ENDIF
			ENDIF
			
		ENDIF
	
//		REPEAT GET_NUM_BAGS() iBags
//			IF BIKER_SELL_HAS_1ST_LOCATION_BIKE_BEEN_DROPPED_OFF(iBags)
//			IF NOT IS_NET_PLAYER_OK(PLAYER_ID())

				IF iMyBag = -1
				OR ( ( iMyBag <> -1 ) AND ( serverBD.iVehDropCount[iMyBag] >= iGET_DROP_TARGET_FOR_BAG_REMOVAL(iMyBag) ) )
//					IF serverBD.iAssignedBag[iBags] = -1
						#IF IS_DEBUG_BUILD
						IF iMyBag != -1
							PRINTLN("[BIKER_SELL] [BAGS] CLIENT_MAINTAIN_BAG_DROP_OFFS, CLEAR_CLIENT_BIT0(eCLIENTBITSET0_BAG_CARRIER) ", serverBD.iVehDropCount[iMyBag])
						ELSE
							PRINTLN("[BIKER_SELL] [BAGS] CLIENT_MAINTAIN_BAG_DROP_OFFS, CLEAR_CLIENT_BIT0(eCLIENTBITSET0_BAG_CARRIER) iMyBag = ", iMyBag)
						ENDIF
						#ENDIF
						
						IF IS_NET_PLAYER_OK(PLAYER_ID())
							CLEAR_CLIENT_BIT0(eCLIENTBITSET0_BAG_CARRIER)
							SET_CARRYING_CRITICAL_ENTITY(FALSE)
							RESET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID())
							IF IS_MP_HEIST_GEAR_EQUIPPED(PLAYER_PED_ID(), HEIST_GEAR_SPORTS_BAG)
								REMOVE_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_SPORTS_BAG)
							ENDIF
						ENDIF
//					ENDIF
				ENDIF
//			ENDIF
//		ENDREPEAT
	ENDIF
ENDPROC



PROC MAINTAIN_CRATE_DROP_CLIENT()
	IF bOK_TO_DO_CRATES()
//	OR BIKER_SELL_TACO_VAN()
	OR BIKER_SELL_BORDER_PATROL()
		IF NOT HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
			IF LOAD_CRATE_ASSETS()
			OR bMODE_HAS_BUYER_PEDS()
			
				VECTOR vDropPost
				BOOL bPostAnimsLoaded
				
				bPostAnimsLoaded = bPOST_ANIMS_LOADED()
			
				INT iVeh
				REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iVeh	
				
					// On foot Postal drop-offs here /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					IF bPostAnimsLoaded 
					AND bIS_POSTMAN_PAT()
					AND bIS_POSTMAN_PAT_VEH(iVeh)
						IF serverBD.iNumCrates[iVeh] < ciMAX_CRATES
							IF IS_SAFE_FOR_CRATE_DROPPING(iVeh)
							AND NOT IS_PHONE_ONSCREEN()
		//					AND NOT bWanted
								IF NOT IS_BIT_SET(sLocaldata.iDroppedBitSet[iVeh], serverBD.iClosestDropOff[iVeh])
									vDropPost = GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[iVeh], serverBD.iRoute, iVeh, DEFAULT, TRUE)
									PRINTLN("MAINTAIN_CRATE_DROP_CLIENT, vDropPost = ", vDropPost)
									IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDropPost, << LOCATE_SIZE_ON_FOOT_ONLY * 0.5, LOCATE_SIZE_ON_FOOT_ONLY * 0.5, LOCATE_SIZE_HEIGHT >>, TRUE)
									AND NOT IS_ANY_INTERACTION_ANIM_PLAYING()
									
										SHOW_DROP_HELP_PROMPT(iVeh)
										
										IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_RIGHT) 
											
											BROADCAST_CRATE_EVENT(iVeh, serverBD.iClosestDropOff[iVeh], serverBD.iLaunchPosix)
											DO_POST_ANIM()
											PRINTLN("[CRT]  - MAINTAIN_CRATE_DROP_CLIENT CASE, bIS_POSTMAN_PAT_VEH, COMPLETE = ", serverBD.iClosestDropOff[iVeh], " - iVeh = ", iVeh) 
											SET_BIT(sLocaldata.iDroppedBitSet[iVeh], serverBD.iClosestDropOff[iVeh])
											
											SET_BIT(sLocaldata.iParcelBitset, ciPARCEL_CREATE)
											
										ENDIF
										
										
									ENDIF	
//								ELSE
//									PRINTLN("MAINTAIN_CRATE_DROP_CLIENT, IS_BIT_SET = ", iVeh)
								ENDIF
//							ELSE
//								PRINTLN("MAINTAIN_CRATE_DROP_CLIENT, IS_SAFE_FOR_CRATE_DROPPING = ", iVeh)
							ENDIF
//						ELSE
//							PRINTLN("MAINTAIN_CRATE_DROP_CLIENT, iNumCrates = ", serverBD.iNumCrates[iVeh])
						ENDIF
//					ELSE
//						PRINTLN("MAINTAIN_CRATE_DROP_CLIENT, bIS_POSTMAN_PAT_VEH = ", iVeh)
					ENDIF
					///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
					IF (IS_DRIVER_OF_SELL_VEH(iVeh)
					OR IS_PASSENGER(iVeh, TRUE))
					AND serverBD.iNumCrates[iVeh] < ciMAX_CRATES
						BOOL bCanDrop, bInArea, bWanted
						//If we are in the area then sound the alarm
						BOOL bPassCoordCheck
						bPassCoordCheck = IS_ENTITY_AT_COORD(VEH_ENT(iVeh), 
											GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[iVeh], serverBD.iRoute, iVeh, DEFAULT, bIS_POSTMAN_PAT_VEH(iVeh)), vLOCATE_DIMENSION())
						
						MAINTAIN_DOOR_OPENING(iVeh)
						
						IF IS_SELL_VEH_OK(iVeh)
						AND bPassCoordCheck
						AND IS_SAFE_FOR_CRATE_DROPPING(iVeh)
						AND (NOT BIKER_SELL_BENSON()
						OR BIKER_SELL_BENSON() AND GET_ENTITY_SPEED(VEH_ID(iVeh)) < 1.0)
							IF NOT BIKER_SELL_TRASHMASTER()
							OR ( BIKER_SELL_TRASHMASTER() AND bBOOT_IN_RANGE(VEH_ID(iVeh), GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[iVeh], serverBD.iRoute, iVeh, DEFAULT, bIS_POSTMAN_PAT_VEH(iVeh))) )
								DO_TRASH_HELP(FALSE)
								PRINTLN("MAINTAIN_CRATE_DROP_CLIENT - Hit 7 - iVeh = ", iVeh)
								bInArea = TRUE
								PLAY_AIR_DROP_ALARM_SOUND()
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
							ELSE
								DO_TRASH_HELP(TRUE)
							ENDIF
						//Clean up the alarm
						ElSE
							DO_TRASH_HELP(FALSE)
							CLEANUP_CRATE_DROP_ALARM()
							IF IS_THIS_AN_AIR_VARIATION()
							AND IS_LOCAL_BIT1_SET(eLOCALBITSET1_BOMB_DOORS)
								CLOSE_BOMB_BAY_DOORS(VEH_ID(GET_SELL_VEH_I_AM_IN_INT()))
								CLEAR_LOCAL_BIT1(eLOCALBITSET1_BOMB_DOORS)
							ENDIF
							CLIENT_SET_DOORS_OPEN(FALSE, iVeh)
							FREEZE_VEHICLE_IN_PLACE(VEH_ID(GET_SELL_VEH_I_AM_IN_INT()), GET_SELL_VEH_I_AM_IN_INT(), FALSE)
						ENDIF
						
						bWanted = ((GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) != 0) 
									OR IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_WANTED))
						
						//If it's air restricted or STING
						IF(BIKER_SELL_STING())
						//If we've give the player the scripted wanted
						AND sLocaldata.iWantedStored != -1
						AND NOT Is_This_The_Current_Objective_Text("BYCB_CLY_LSC0")
							//PRINTLN("[BIKER_SELL] MAINTAIN_CRATE_DROP_CLIENT - bWanted = FALSE")
							bWanted = FALSE
						ENDIF
						
						IF BIKER_SELL_BORDER_PATROL()
							bWanted = FALSE
						ENDIF
						
						IF Is_This_The_Current_Objective_Text("BYCB_CLY_LSC0")
							bWanted = TRUE
						ENDIF
						PRINTLN("MAINTAIN_CRATE_DROP_CLIENT - bInArea = ", bInArea)
						PRINTLN("MAINTAIN_CRATE_DROP_CLIENT - bWanted = ", bWanted)
						PRINTLN("MAINTAIN_CRATE_DROP_CLIENT - IS_BIT_SET(sLocaldata.iDroppedBitSet[iVeh], serverBD.iClosestDropOff[", iVeh, "]) = ", IS_BIT_SET(sLocaldata.iDroppedBitSet[iVeh], serverBD.iClosestDropOff[iVeh]))
						
						//If we're in the area then print the help
						IF bInArea
						AND NOT bWanted
						AND NOT IS_BIT_SET(sLocaldata.iDroppedBitSet[iVeh], serverBD.iClosestDropOff[iVeh])
							IF IS_SAFE_TO_PROMPT_DROP(iVeh)
							
//								IF NOT BIKER_SELL_TACO_VAN()
								IF NOT ( IS_VEHICLE_STUCK_TIMER_UP(VEH_ID(iVeh), VEH_STUCK_ON_ROOF, 500) AND NOT IS_VEHICLE_STUCK_TIMER_UP(VEH_ID(iVeh), VEH_STUCK_ON_SIDE, 500) )
							
									IF BIKER_SELL_BORDER_PATROL()
										IF IS_DRIVER_OF_SELL_VEH(iVeh)
											bCanDrop = TRUE
										ENDIF
									ELSE
										IF (IS_DRIVER_OF_SELL_VEH(iVeh) AND NOT DOES_SELL_VEH_HAVE_PASSENGER(iVeh, TRUE))
										OR (IS_PASSENGER(iVeh, TRUE) AND IS_SELL_VEH_OCCUPIED(iVeh) AND NOT BIKER_SELL_TRASHMASTER())
										OR (IS_DRIVER_OF_SELL_VEH(iVeh) AND BIKER_SELL_TRASHMASTER())
											bCanDrop = TRUE
										ENDIF
									ENDIF
									
									// "Press ~INPUT_FRONTEND_RIGHT~ to drop Contraband."
									IF bSHOW_DROP_TEXT()
										SHOW_DROP_HELP_PROMPT(iVeh)
										PRINTLN("MAINTAIN_CRATE_DROP_CLIENT - ", sDROP_BTN_TEXT(iVeh))
									ENDIF

									DO_DROP_AREA_VEHICLE_TASKS(iVeh)
								ENDIF
							ELSE
								IF BIKER_SELL_HELICOPTER_DROP()
									IF (IS_DRIVER_OF_SELL_VEH(iVeh) AND NOT DOES_SELL_VEH_HAVE_PASSENGER(iVeh, TRUE))
										PRINT_HELP_WITH_STRING("BIKER_TOOLOW", GET_SHIPMENT_TYPE_STRING())
									ELIF (IS_DRIVER_OF_SELL_VEH(iVeh) AND DOES_SELL_VEH_HAVE_PASSENGER(iVeh, TRUE))
										PRINT_HELP_WITH_STRING("BIKER_TOOLOW2", GET_SHIPMENT_TYPE_STRING())
									ELIF (IS_PASSENGER(iVeh, TRUE) AND IS_SELL_VEH_OCCUPIED(iVeh))
										PRINT_HELP_WITH_STRING("BIKER_TOOLOW3", GET_SHIPMENT_TYPE_STRING())
									ENDIF
								ENDIF
							ENDIF
						ELIF IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED(sDROP_BTN_TEXT(iVeh), GET_SHIPMENT_TYPE_STRING())
						OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("SBIKER_LAND", GET_SHIPMENT_TYPE_STRING())
						OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("BIKER_TOOLOW", GET_SHIPMENT_TYPE_STRING())
							PRINTLN("MAINTAIN_CRATE_DROP_CLIENT - CLEAR ", sDROP_BTN_TEXT(iVeh))
							CLEAR_HELP(TRUE)
						ENDIF
						
						SWITCH sLocaldata.iCrateProgress
							CASE 0
									
								IF bCanDrop
								AND (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_RIGHT) OR BIKER_SELL_BORDER_PATROL())
								AND NOT IS_BIT_SET(sLocaldata.iDroppedBitSet[iVeh], serverBD.iClosestDropOff[iVeh])
								AND NOT bWanted
								AND NOT bMODE_HAS_BUYER_PEDS()
								
									IF BIKER_SELL_BORDER_PATROL()
										INT iMyVeh
										iMyVeh = GET_SELL_VEH_I_AM_IN_INT()
										IF iMyVeh != -1
											IF HAS_NET_TIMER_STARTED(playerBD[PARTICIPANT_ID_TO_INT()].BorderPatrolCheckpointTimer[iMyVeh])
												PRINTLN("[BIKER_SELL] - MAINTAIN_CRATE_DROP_CLIENT - Successful border patrol drop! Resetting timer for next drop!")
												REINIT_NET_TIMER(playerBD[PARTICIPANT_ID_TO_INT()].BorderPatrolCheckpointTimer[iMyVeh])
												IF IS_LOCAL_BIT1_SET(eLOCALBITSET1_SENT_FAIL)
													CLEAR_LOCAL_BIT1(eLOCALBITSET1_SENT_FAIL)
												ENDIF
											ENDIF
											PLAY_THROW_ID_ANIM(serverBD.iClosestDropOff[iVeh])
										ENDIF
									ENDIF
									
									CLIENT_SET_DOORS_OPEN(TRUE, iVeh)

									BROADCAST_CRATE_EVENT(iVeh, serverBD.iClosestDropOff[iVeh], serverBD.iLaunchPosix)
									PRINTLN("[CRT]  - MAINTAIN_CRATE_DROP_CLIENT CASE, COMPLETE = ", serverBD.iClosestDropOff[iVeh], " - iVeh = ", iVeh) 
									SET_BIT(sLocaldata.iDroppedBitSet[iVeh], serverBD.iClosestDropOff[iVeh])
									sLocaldata.iCrateProgress++
								ENDIF
							BREAK
							CASE 1
								sLocaldata.iCrateProgress = 0
							BREAK
						ENDSWITCH
					ELSE
						IF NOT IS_PASSENGER(iVeh)
							CLEANUP_CRATE_DROP_ALARM()
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED(sDROP_BTN_TEXT(), GET_SHIPMENT_TYPE_STRING())
			OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("SBIKER_LAND", GET_SHIPMENT_TYPE_STRING())
			OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("BIKER_TOOLOW", GET_SHIPMENT_TYPE_STRING())
				CLEAR_HELP(TRUE)
			ENDIF
			CLEANUP_CRATE_DROP_ALARM()
			
//			PRINTLN("MAINTAIN_CRATE_DROP_CLIENT, HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF, iDROPPED_OFF_COUNT() = ", iDROPPED_OFF_COUNT(), " iDROP_TARGET() = ", iDROP_TARGET())
		ENDIF
//	ELSE
//		PRINTLN("MAINTAIN_CRATE_DROP_CLIENT, bOK_TO_DO_CRATES, FALSE ")
	ENDIF
	
	IF BIKER_SELL_FRIENDS_IN_NEED()
		INT iBag
		iBag = iGET_PLAYER_BAG(PLAYER_ID())
		IF iBag <> -1
			IF NOT IS_BIT_SET(sLocaldata.iSoundBitsetDrop, serverBD.iClosestDropOff[iBag])
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[iBag], serverBD.iRoute, iBag, DEFAULT, FALSE), vLOCATE_DIMENSION())

					PLAY_BIKER_DROP_SOUND()
					SET_BIT(sLocaldata.iSoundBitsetDrop, serverBD.iClosestDropOff[iBag])
					PRINTLN("MAINTAIN_CRATE_DROP_CLIENT, PLAY_BIKER_DROP_SOUND() ")
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC STRING GET_AIR_RESTRICTED_DIALOUGE()
	SWITCH serverBD.iRoute
		CASE 0	RETURN "ARMY_ANN1"
		CASE 1	RETURN "ARMY_ANN2"
		CASE 2	RETURN "ARMY_ANN3"
		CASE 3	RETURN "ARMY_ANN4"
		CASE 4	RETURN "ARMY_ANN1"
	ENDSWITCH 
	RETURN "ARMY_ANN1"
ENDFUNC

PROC MAINTAIN_LAND_PLANE_CLIENT()
	IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINT_LAND_HELP)
		IF bOK_TO_DO_CRATES()
			INT i
			REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
				IF IS_DRIVER_OF_SELL_VEH(i)
				AND serverBD.iNumCrates[i] < ciMAX_CRATES
					IF IS_SELL_VEH_OK(i)
					AND IS_ENTITY_AT_COORD(VEH_ENT(i), GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[i], serverBD.iRoute, i, DEFAULT, bIS_POSTMAN_PAT_VEH(i)), vLOCATE_DIMENSION(TRUE))
						// "Press ~INPUT_FRONTEND_RIGHT~ to drop Contraband."
						PRINT_HELP("SCONTRA_CLAND")
						SET_LOCAL_BIT0(eLOCALBITSET0_PRINT_LAND_HELP)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_SET_VEHICLE_CREATE_OFFSET()
	IF BIKER_SELL_TRASHMASTER()
	
		RETURN <<0.0, -5.200, 1.0>> 
	ENDIF
	IF NOT IS_THIS_AN_AIR_VARIATION()
		RETURN <<0.0, -6.0, -0.32>> 
	ENDIF
	RETURN <<0.0, 0.0, -1.0>> 
ENDFUNC

PROC MAINTAIN_CRATE_DROP_SERVER()	

	INT i
	INT iBitToCheck
	eSERVER_BITSET_0 eServerBitToCheck
	
	SERVER_SET_TRASH_VISIBLE()
	
	VECTOR vCratePos
	
//	IF BIKER_SELL_POSTMAN()
//		ENTITY_INDEX eiParcel
//		IF IS_NET_PLAYER_OK(serverBD.playerRequestedCrate)
//			eiParcel = serverBD.eiProduct[NATIVE_TO_INT(serverBD.playerRequestedCrate)]
//			IF DOES_ENTITY_EXIST(eiParcel)
//				IF HAS_ANIM_EVENT_FIRED(GET_PLAYER_PED(serverBD.playerRequestedCrate), GET_HASH_KEY("RELEASE_PROP"))
//					DETACH_ENTITY(eiParcel)
//					PRINTLN("[BIKER_SELL] [POST] - DETACH_PARCEL  ")
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
		
	IF bOK_TO_DO_CRATES()
		IF LOAD_CRATE_ASSETS()
			REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
				
				iBitToCheck = ENUM_TO_INT(eSERVERBITSET0_CRATE_DROP1) + i
				eServerBitToCheck = INT_TO_ENUM(eSERVER_BITSET_0, iBitToCheck)
				
				IF IS_SERVER_BIT0_SET(eServerBitToCheck)
					IF serverBD.iNumCrates[i] < ciMAX_CRATES
											
						SWITCH serverBD.iCrateProgress[i]
							CASE 0
								PRINTLN("[BIKER_SELL] [CRT]  - MAINTAIN_CRATE_DROP_SERVER CASE 0 veh ", i) 
								serverBD.iCrateProgress[i]++
							BREAK
							CASE 1
							//	IF serverBD.iVehForCrateDrop <> -1
								IF IS_BIT_SET(serverBD.iVehForCrateDropBitSet, i)
								
									vCratePos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VEH_ENT(i), GET_SET_VEHICLE_CREATE_OFFSET())
									
//									IF bIS_POSTMAN_PAT_VEH(i)
//										vCratePos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GET_PLAYER_PED(serverBD.playerRequestedCrate), <<0.5, 0.5, 0.0>>)
//									ENDIF
								
									IF IS_SELL_VEH_OK(i)
									AND CREATE_DROPPED_CRATE(vCratePos, fVEH_HEADING(i), i )
										serverBD.iNumCrates[i]++
										PRINTLN("[BIKER_SELL] [CRT]  - MAINTAIN_CRATE_DROP_SERVER, serverBD.iNumCrates[",i ,"] = ", serverBD.iNumCrates[i]) 
										CLEAR_SERVER_BIT0(eServerBitToCheck)
										serverBD.iCrateProgress[i] = 0
										CLEAR_BIT(serverBD.iVehForCrateDropBitSet, i)
										
//										SERVER_SET_POSTMAN(FALSE, serverBD.playerRequestedCrate, i)
									ENDIF
								ELSE
									CLEAR_SERVER_BIT0(eServerBitToCheck)
									
									PRINTLN("[BIKER_SELL] [CRT] CLEARing server bit as bit ", i, " not set" )
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	CONTROL_CRATE_LANDING()
ENDPROC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				FX																║

// Joint smoke effect ///////////////////////////////////////////////////////////
FUNC BOOL LOAD_BLUNT_SMOKE_PARTICLE_FX()
	REQUEST_NAMED_PTFX_ASSET("scr_bike_contraband")

	IF HAS_NAMED_PTFX_ASSET_LOADED("scr_bike_contraband")
		//NET_PRINT_TIME() NET_PRINT("     ---------->    LOAD_BLUNT_SMOKE_PARTICLE_FX - DONE ") NET_NL()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_BLUNT_SMOKE()
	IF sLocaldata.ptfxSmoke <> NULL
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sLocaldata.ptfxSmoke)
			STOP_PARTICLE_FX_LOOPED(sLocaldata.ptfxSmoke)
			PRINTLN("     ---------->     ADD_BLUNT_SMOKE_FX - CLEANUP_BLUNT_SMOKE ")
		ELSE
			PRINTLN("     ---------->     DOES_PARTICLE_FX_LOOPED_EXIST - CLEANUP_BLUNT_SMOKE ")
		ENDIF
	ELSE
		PRINTLN("     ---------->     ptfxSmoke - CLEANUP_BLUNT_SMOKE ")
	ENDIF
ENDPROC

PROC ADD_BLUNT_SMOKE_FX()
	IF BIKER_SELL_PROVEN()
	
		PED_INDEX pedBlunt = GET_AI_PED_PED_INDEX(0)
		
		IF DOES_ENTITY_EXIST(pedBlunt)
		AND NOT IS_ENTITY_DEAD(pedBlunt)
		AND IS_PED_IN_ANY_VEHICLE(pedBlunt)
		AND NOT IS_ENTITY_IN_WATER(pedBlunt)
		
			IF LOAD_BLUNT_SMOKE_PARTICLE_FX()
				IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(sLocaldata.ptfxSmoke)		
					sLocaldata.ptfxSmoke = START_PARTICLE_FX_LOOPED_ON_PED_BONE("scr_bike_truck_weed_smoke_cabin", pedBlunt, <<0, 0, 0>>, <<0, 0, 0>>, BONETAG_PH_R_HAND, 1.0)
//					sLocaldata.ptfxSmoke = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_bike_truck_weed_smoke_cabin", VEH_ENT(0), <<0, 0, 0>>, <<0, 0, 0>>, GET_ENTITY_BONE_INDEX_BY_NAME(VEH_ID(0), "chassis_dummy"), 1.0)
					SET_PARTICLE_FX_LOOPED_EVOLUTION(sLocaldata.ptfxSmoke, "smoke_fade", 0.0)
					PRINTLN("     ---------->    ADD_BLUNT_SMOKE_FX, at coords ")//, vPos) 
				ENDIF
			ENDIF
		ELSE
			CLEANUP_BLUNT_SMOKE()
		ENDIF
	ENDIF
ENDPROC

////////////////////////////////////////////////////////////////////////////////

//Loads all particle effects required
FUNC BOOL LOAD_INTERACT_PARTICLE_FX()
	REQUEST_NAMED_PTFX_ASSET("scr_lowrider")

	IF HAS_NAMED_PTFX_ASSET_LOADED("scr_lowrider")
		//NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - LOAD_INTERACT_PARTICLE_FX - DONE ") NET_NL()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT fPARTICLE_SCALE()

	#IF IS_DEBUG_BUILD
	IF tfScale <> cfPARTICLE_SCALE
		
		RETURN tfScale
	ENDIF
	#ENDIF
	
	IF BIKER_SELL_HELICOPTER_DROP()
		RETURN cfPARTICLE_SCALE / 4
	ENDIF
	
	RETURN cfPARTICLE_SCALE
ENDFUNC

PROC ADD_PARTICLE_FX(INT iCrate, VECTOR vPos)
	IF LOAD_INTERACT_PARTICLE_FX()
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(sLocaldata.ptfxFlare[iCrate])		//scr_drug_traffic_flare_L
			BOOL bLocalOnly = TRUE
//			VEHICLE_INDEX vehID
			USE_PARTICLE_FX_ASSET("scr_lowrider")
			
//			IF BIKER_SELL_AIR_DROP_AT_SEA()
//				vehID = NET_TO_VEH(serverBD.niDropOffVeh[iCrate])
//				vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehID), GET_ENTITY_HEADING(vehID), <<0.0, 3.0, 1.0>>)
//				sLocaldata.ptfxFlare[iCrate] = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_lowrider_flare", vehID, vPos, <<0, 0, -0.2>>, GET_ENTITY_BONE_INDEX_BY_NAME(vehID, "chassis_dummy"), fPARTICLE_SCALE())
//			ELSE
				sLocaldata.ptfxFlare[iCrate] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_lowrider_flare" , vPos, <<0, 0, -0.2>>, fPARTICLE_SCALE(), DEFAULT, DEFAULT, DEFAULT, bLocalOnly)//, <<0, 0, 0>>)
//			ENDIF
			
//			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)
				SET_PARTICLE_FX_LOOPED_COLOUR(sLocaldata.ptfxFlare[iCrate], 0.8, 0.18, 0.19, bLocalOnly)
//			ELSE
//				SET_PARTICLE_FX_LOOPED_COLOUR(sLocaldata.ptfxFlare[iCrate], 1.0, 0.84, 0.0)
//			ENDIF
			PRINTLN("     ---------->    CRATE DROP - ADD_CRATE_FLARES ", iCrate, " at coords ",vPos) 
		ENDIF
	ENDIF			
ENDPROC

//PURPOSE: Removes a flare
PROC CLEANUP_CRATE_FLARE(INT iCrate)
	IF sLocaldata.ptfxFlare[iCrate] <> NULL
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sLocaldata.ptfxFlare[iCrate])
			STOP_PARTICLE_FX_LOOPED(sLocaldata.ptfxFlare[iCrate])
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CLEANUP_CRATE_FLARE ") NET_PRINT_INT(iCrate) NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Loops through and cleans up all flares
PROC CLEANUP_ALL_CRATE_FLARES()
	INT i
	REPEAT GET_MAX_SELL_DROP_OFFS() i
		CLEANUP_CRATE_FLARE(i)
	ENDREPEAT
ENDPROC

PROC CLEANUP_PLANE_FX()
	IF DOES_PARTICLE_FX_LOOPED_EXIST(sLocaldata.ptfxPlane)
		REMOVE_PARTICLE_FX(sLocaldata.ptfxPlane)
	ENDIF
//	REMOVE_PTFX_ASSET()
ENDPROC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				SERVER															║

FUNC INT STARTING_WAREHOUSE()
	RETURN serverBD.iStartingWarehouse
ENDFUNC

#IF IS_DEBUG_BUILD
CONST_INT debug_NUM_VARIATIONS_IN_PLAYLIST 3

FUNC eBIKER_SELL_VARIATION GET_NEXT_VARIATION_IN_PLAYLIST()
	SWITCH MPGlobalsAmbience.sMagnateGangBossData.iSellMissionPlaylistCount
		CASE 0	RETURN	eBIKER_SELL_HELICOPTER_DROP
	//	CASE 1	RETURN 	eVAR_LAND_BREAKDOWN
	//	CASE 2	RETURN 	eVAR_AIR_FAILURE
	ENDSWITCH
	
	RETURN eBIKER_SELL_HELICOPTER_DROP
ENDFUNC
#ENDIF

FUNC INT GET_TYPE_NUMBER(eBIKER_SELL_VARIATION eVar = eBIKER_SELL_MAX)
	IF eVar = eBIKER_SELL_MAX
		eVar = GET_SELL_VAR()
	ENDIF	
	IF IS_THIS_A_LAND_VARIATION()
		RETURN 0
	ELIF IS_THIS_AN_AIR_VARIATION()
		RETURN 1
	ELSE
		RETURN 2
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC BOOL IS_SELL_TYPE_DIFFERENT_FROM_PREVIOUS(eBIKER_SELL_VARIATION eVar = eBIKER_SELL_MAX)
	
	RETURN MPGlobalsAmbience.sMagnateGangBossData.iTypeOfLastSellMission != GET_TYPE_NUMBER(eVar)

ENDFUNC

PROC SET_PREVIOUS_SELL_TYPE(eBIKER_SELL_VARIATION eVar = eBIKER_SELL_MAX)

	MPGlobalsAmbience.sMagnateGangBossData.iTypeOfLastSellMission = GET_TYPE_NUMBER(eVar)
	PRINTLN("[BIKER_SELL] SET_PREVIOUS_SELL_TYPE = ",MPGlobalsAmbience.sMagnateGangBossData.iTypeOfLastSellMission)

ENDPROC

FUNC INT GET_HISTORY_LIST_SIZE()
	RETURN g_sMPTunables.iBIKER_SELL_HISTORY_LIST_SIZE
ENDFUNC

FUNC BOOL IS_VARIATION_IN_HISTORY_LIST(eBIKER_SELL_VARIATION eSellVar)
	INT i
	REPEAT GET_HISTORY_LIST_SIZE() i
		IF MPGlobalsAmbience.sMagnateGangBossData.eBikerSellMissionHistoryList[i] = eSellVar
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC ADD_VARIATION_TO_HISTORY_LIST(eBIKER_SELL_VARIATION eSellVar)
	// If there's no variation in slot 0, add it and exit
	IF MPGlobalsAmbience.sMagnateGangBossData.eBikerSellMissionHistoryList[0] = eBIKER_SELL_MAX
		MPGlobalsAmbience.sMagnateGangBossData.eBikerSellMissionHistoryList[0] = eSellVar
		EXIT
	ELSE
		// There's already a variation in slot 0 for the new variation, bump all other variations down the list, and add the variation to slot 0
		MPGlobalsAmbience.sMagnateGangBossData.eBikerSellMissionHistoryList[3] = MPGlobalsAmbience.sMagnateGangBossData.eBikerSellMissionHistoryList[2]
		MPGlobalsAmbience.sMagnateGangBossData.eBikerSellMissionHistoryList[2] = MPGlobalsAmbience.sMagnateGangBossData.eBikerSellMissionHistoryList[1]
		MPGlobalsAmbience.sMagnateGangBossData.eBikerSellMissionHistoryList[1] = MPGlobalsAmbience.sMagnateGangBossData.eBikerSellMissionHistoryList[0]
		MPGlobalsAmbience.sMagnateGangBossData.eBikerSellMissionHistoryList[0] = eSellVar
	ENDIF
ENDPROC


FUNC BOOL IS_GOON_IN_CLUBHOUSE()
	INT iPlayer
	INT iProperty = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
	PLAYER_INDEX PlayerIdTemp
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		//If they are active
		PlayerIdTemp = INT_TO_PLAYERINDEX(iPlayer)
		IF PlayerIdTemp != INVALID_PLAYER_INDEX()
		AND PlayerIdTemp != PLAYER_ID()
		AND NETWORK_IS_PLAYER_ACTIVE(PlayerIdTemp)
			IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PlayerIdTemp, PLAYER_ID())
			AND iProperty = GlobalplayerBD_FM[NATIVE_TO_INT(PlayerIdTemp)].propertyDetails.iCurrentlyInsideProperty
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VARIATION_BLOCKED(eBIKER_SELL_VARIATION eSellVar)

//	// Comment out the ones we want for the play
//	#IF IS_DEBUG_BUILD
//	IF GET_COMMANDLINE_PARAM_EXISTS("sc_BikerFlowPlayVariations")
//		SWITCH(eSellVar)
//			// REMOVE THESE AS VARIATIONS ARE IMPLEMENTED		
//			CASE eBIKER_SELL_DEFAULT
////			CASE eBIKER_SELL_AIR_DROP_AT_SEA
////			CASE eBIKER_SELL_HELICOPTER_DROP
////			CASE eBIKER_SELL_TRASHMASTER
//			CASE eBIKER_SELL_FRIENDS_IN_NEED
////			CASE eBIKER_SELL_POSTMAN
//			CASE eBIKER_SELL_TACO_VAN
//			CASE eBIKER_SELL_STING_OP
//			CASE eBIKER_SELL_PROVEN
////			CASE eBIKER_SELL_BENSON
//			CASE eBIKER_SELL_BORDER_PATROL
//			CASE eBIKER_SELL_EXCHANGE
////			CASE eBIKER_SELL_BAG_DROP
////			CASE eBIKER_SELL_RACE
////			CASE eBIKER_SELL_CLUB_RUN
//			CASE eBIKER_SELL_MAX
//
//				RETURN TRUE
//		ENDSWITCH
//	ENDIF
//	#ENDIF
	
	SWITCH eSellVar
		CASE eBIKER_SELL_DEFAULT						RETURN g_sMPTunables.bBIKER_DISABLE_SELL_CONVOY
		CASE eBIKER_SELL_TRASHMASTER					RETURN g_sMPTunables.bBIKER_DISABLE_SELL_TRASHMASTER
		CASE eBIKER_SELL_PROVEN							RETURN g_sMPTunables.bBIKER_DISABLE_SELL_PROVEN
		CASE eBIKER_SELL_FRIENDS_IN_NEED				RETURN g_sMPTunables.bBIKER_DISABLE_SELL_FRIENDS_IN_NEED
		CASE eBIKER_SELL_BORDER_PATROL					RETURN g_sMPTunables.bBIKER_DISABLE_SELL_BORDER_PATROL
		CASE eBIKER_SELL_HELICOPTER_DROP				RETURN g_sMPTunables.bBIKER_DISABLE_SELL_HELICOPTER_DROP
		CASE eBIKER_SELL_POSTMAN						RETURN g_sMPTunables.bBIKER_DISABLE_SELL_POSTMAN
		CASE eBIKER_SELL_AIR_DROP_AT_SEA				RETURN g_sMPTunables.bBIKER_DISABLE_SELL_AIR_DROP_AT_SEA
		CASE eBIKER_SELL_STING_OP						RETURN g_sMPTunables.bBIKER_DISABLE_SELL_STING_OP
		CASE eBIKER_SELL_BENSON							RETURN g_sMPTunables.bBIKER_DISABLE_SELL_BENSON
		CASE eBIKER_SELL_BAG_DROP						RETURN g_sMPTunables.bBIKER_DISABLE_SELL_BAG_DROP
		CASE eBIKER_SELL_RACE							RETURN g_sMPTunables.bBIKER_DISABLE_SELL_RACE
		CASE eBIKER_SELL_CLUB_RUN						RETURN g_sMPTunables.bBIKER_DISABLE_CLUB_RUN
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_OTHER_BOSS_ON_VARIATION(eBIKER_SELL_VARIATION eSellVar)
	INT i
	INT iVar
	PLAYER_INDEX playerID
	
	REPEAT NUM_NETWORK_PLAYERS i
		IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(i))
			playerID = INT_TO_PLAYERINDEX(i)
			
			IF GB_IS_PLAYER_BOSS_OF_A_GANG(playerID)
				IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID) = FMMC_TYPE_BIKER_CONTRABAND_SELL
					iVar = GB_GET_BIKER_SELL_VARIATION_PLAYER_IS_ON(playerID)
					
					IF ENUM_TO_INT(eSellVar) = iVar
						PRINTLN("[BIKER_SELL] IS_OTHER_BOSS_ON_VARIATION - Yes - Boss ", GET_PLAYER_NAME(playerID), " is already on ", BIKER_SELL_GET_VARIATION_DEBUG_PRINT(eSellVar))
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VARIATION_VALID_FOR_CONTRA_AMOUNT(eBIKER_SELL_VARIATION eSellVar)
	#IF IS_DEBUG_BUILD
	PRINTLN("[BIKER_SELL] [IS_VARIATION_VALID_FOR_CONTRA_AMOUNT] Called with eSellVar = ", BIKER_SELL_GET_VARIATION_DEBUG_PRINT(eSellVar), " g_iContraSellMissionCratesToSell = ", g_iContraSellMissionCratesToSell, " g_iContraSellMissionBuyerID = ", g_iContraSellMissionBuyerID) 
	#ENDIF
	

	SWITCH eSellVar
		CASE eBIKER_SELL_STING_OP
			RETURN g_iContraSellMissionCratesToSell >= 5
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC eBIKER_SELL_VARIATION GET_RANDOM_VARIATION_FOR_SPECIAL_ITEMS()	
	RETURN eBIKER_SELL_DEFAULT
ENDFUNC

FUNC BOOL BIKER_SELL_HAS_A_SET_ROUTE()
	IF DOES_BIKER_SELL_MULTIPLE_DROP_OFFS()
	//OR BIKER_SELL_STING()
	OR IS_THIS_AN_AIR_VARIATION()
	//OR BIKER_SELL_BENSON()
	//OR BIKER_SELL_EXCHANGE()
	OR BIKER_SELL_DEFAULT()

		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SELL_TYPE_COMPATIBLE_WITH_THIS_FACTORY_TYPE(eBIKER_SELL_VARIATION eSellVar)
	
	// Global variations
	IF eSellVar = eBIKER_SELL_DEFAULT
	OR eSellVar = eBIKER_SELL_BORDER_PATROL
	OR eSellVar = eBIKER_SELL_HELICOPTER_DROP
	OR eSellVar = eBIKER_SELL_AIR_DROP_AT_SEA
	OR eSellVar = eBIKER_SELL_TRASHMASTER
	OR eSellVar = eBIKER_SELL_POSTMAN
	OR eSellVar = eBIKER_SELL_BENSON
	OR eSellVar = eBIKER_SELL_BAG_DROP
	OR eSellVar = eBIKER_SELL_RACE
	OR eSellVar = eBIKER_SELL_CLUB_RUN
		RETURN TRUE
	ENDIF
	
	//coke
	IF eSellVar = eBIKER_SELL_STING_OP
	AND serverBD.factoryType = FACTORY_TYPE_CRACK
		RETURN TRUE
	ENDIF

	//meth
//	IF eSellVar = eBIKER_SELL_TACO_VAN
//	AND serverBD.factoryType = FACTORY_TYPE_METH
//		RETURN TRUE
//	ENDIF

	//money
//	IF eSellVar = eBIKER_SELL_EXCHANGE
//	AND serverBD.factoryType = FACTORY_TYPE_FAKE_MONEY
//		RETURN TRUE
//	ENDIF

	//IDs
	IF eSellVar = eBIKER_SELL_FRIENDS_IN_NEED
	AND serverBD.factoryType = FACTORY_TYPE_FAKE_IDS
		RETURN TRUE
	ENDIF
	
	IF eSellVar = eBIKER_SELL_TRASHMASTER
	AND serverBD.factoryType = FACTORY_TYPE_WEED
		RETURN TRUE
	ENDIF

	//weed
//	IF eSellVar = eBIKER_SELL_MEDICAL_PURPOSES
	IF eSellVar = eBIKER_SELL_PROVEN
	AND serverBD.factoryType = FACTORY_TYPE_WEED
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_THERE_ENOUGH_PLAYERS_FOR_VARIATION(eBIKER_SELL_VARIATION eSellVar)
	SWITCH eSellVar
		CASE eBIKER_SELL_BAG_DROP
			IF serverBD.iNumPlayersInFactory < g_sMPTunables.iBIKER_SELL_BAG_DROP_PLAYERS_REQUIRED
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE eBIKER_SELL_RACE
			IF serverBD.iNumPlayersInFactory < g_sMPTunables.iBIKER_SELL_RACE_PLAYERS_REQUIRED
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE eBIKER_SELL_CLUB_RUN
			IF serverBD.iNumPlayersInFactory < g_sMPTunables.iBIKER_SELL_CLUB_RUN_PLAYERS_REQUIRED
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE eBIKER_SELL_PROVEN
			IF GB_GET_NUM_GOONS_IN_LOCAL_GANG() + 1 < g_sMPTunables.iBIKER_SELL_PROVEN_MIN_PLAYERS
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL DOES_FACTORY_HAVE_ENOUGH_PRODUCT_FOR_VARIATION(eBIKER_SELL_VARIATION eSellVar)
	
	FLOAT fProductPerPlayer = TO_FLOAT((GET_PRODUCT_TOTAL_FOR_FACTORY(PLAYER_ID(), serverBD.factoryId) / serverBD.iNumPlayersInFactory))
	
	SWITCH eSellVar
		CASE eBIKER_SELL_BAG_DROP
			IF fProductPerPlayer < g_sMPTunables.iBIKER_SELL_BAG_DROP_UNITS_PER_PLAYER_REQUIRED
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE eBIKER_SELL_RACE
			IF fProductPerPlayer < g_sMPTunables.iBIKER_SELL_RACE_UNITS_PER_PLAYER_REQUIRED
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE eBIKER_SELL_CLUB_RUN
			IF fProductPerPlayer < g_sMPTunables.iBIKER_SELL_CLUB_RUN_UNITS_PER_PLAYER_REQUIRED
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SERVER_PICK_VARIATION()

	IF GET_SELL_VAR() = eBIKER_SELL_MAX
	
		#IF IS_DEBUG_BUILD
		
		// Debug menu
		serverBD.iDebugVariation2 = GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2()
		PRINTLN("[BIKER_SELL] SERVER_DEBUG_INIT [DEBUG_OVERRIDE], iDebugVariation2 =  ", serverBD.iDebugVariation2)
	
		// Debug override
		IF serverBD.iDebugVariation2 <> -1
		
			serverBD.eSellVar = INT_TO_ENUM(eBIKER_SELL_VARIATION, serverBD.iDebugVariation2)
			SERVER_SET_SHIPMENT_TYPE()
			IF BIKER_SELL_HAS_A_SET_ROUTE()
			OR BIKER_SELL_BENSON()
			OR BIKER_SELL_STING()
			OR BIKER_SELL_CLUB_RUN()
			OR BIKER_SELL_BAG_DROP()
				MPGlobalsAmbience.sMagnateGangBossData.iSellMissionVariationCounter[serverBD.iDebugVariation2]++
				IF MPGlobalsAmbience.sMagnateGangBossData.iSellMissionVariationCounter[serverBD.iDebugVariation2] >= 5
					MPGlobalsAmbience.sMagnateGangBossData.iSellMissionVariationCounter[serverBD.iDebugVariation2] = 0
				ENDIF
				serverBD.iRoute = MPGlobalsAmbience.sMagnateGangBossData.iSellMissionVariationCounter[serverBD.iDebugVariation2]
				
				IF BIKER_SELL_STING()
					IF serverBD.iRandEndRoute = -1
						serverBD.iRandEndRoute = GET_RANDOM_INT_IN_RANGE(0, 3)
						PRINTLN("[BIKER_SELL] SERVER_PICK_VARIATION - serverBD.iRandEndRoute = ", serverBD.iRandEndRoute)
					ENDIF
				ENDIF
				
				SET_SERVER_BIT0(eSERVERBITSET0_ROUTE_SET)
				PRINTLN("[BIKER_SELL] - SERVER_PICK_VARIATION - serverBD.iRoute being set to - serverBD.iRoute = ", serverBD.iRoute)

				IF BIKER_SELL_BORDER_PATROL()
					PRINTLN("[BIKER_SELL] - SERVER_PICK_VARIATION - Border patrol, overriding route to 0")
					serverBD.iRoute = 0
				ENDIF
			ELSE
				MPGlobalsAmbience.sMagnateGangBossData.iSellMissionVariationCounter[serverBD.iDebugVariation2]++
				IF MPGlobalsAmbience.sMagnateGangBossData.iSellMissionVariationCounter[serverBD.iDebugVariation2] >= 20
					MPGlobalsAmbience.sMagnateGangBossData.iSellMissionVariationCounter[serverBD.iDebugVariation2] = 0
				ENDIF
				serverBD.iFoundDropCount = 0
				INT iCount, iNumVeh
				iCount = MPGlobalsAmbience.sMagnateGangBossData.iSellMissionVariationCounter[serverBD.iDebugVariation2]
				serverBD.iFoundDropCount++
				
				INT iModeTarget
				
				IF MODE_HAS_PICKUP_BAGS()
					iModeTarget = GET_NUM_BAGS()
				ELSE
					iModeTarget = GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE()
				ENDIF
				
				REPEAT iModeTarget iNumVeh
					SERVER_SET_ACTIVE_DROP(iNumVeh, iCount)
					SET_BIT(serverBD.dropOffLocationBitSet, iCount)	
					PRINTLN("[BIKER_SELL] DBG SET_BIT(serverBD.iActiveDropOffBitset, ", iCount, ")")
				ENDREPEAT
			ENDIF
			
			
			PRINTLN("[BIKER_SELL] SERVER_DEBUG_INIT [DEBUG_OVERRIDE], iSellMissionVariationCounter[",serverBD.iDebugVariation2,"] is now ",MPGlobalsAmbience.sMagnateGangBossData.iSellMissionVariationCounter[serverBD.iDebugVariation2])
		ELSE
		
		#ENDIF
	
			eBIKER_SELL_VARIATION eSellVar
	
			eSellVar = INT_TO_ENUM(eBIKER_SELL_VARIATION, GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(eBIKER_SELL_MAX)))
			PRINTLN("[BIKER_SELL] SERVER_PICK_VARIATION - GeSellVar = INT_TO_ENUM(eBIKER_SELL_VARIATION, GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(eBIKER_SELL_MAX))) = ", BIKER_SELL_GET_VARIATION_DEBUG_PRINT(eSellVar))
			
			// THIS NEEDS SETTING UP AGAIN BUT FOR BIKER STUFF
			
			IF NOT IS_VARIATION_IN_HISTORY_LIST(eSellVar)
			AND NOT IS_VARIATION_BLOCKED(eSellVar)
			AND NOT IS_OTHER_BOSS_ON_VARIATION(eSellVar)
//			AND IS_VARIATION_VALID_FOR_CONTRA_AMOUNT(eSellVar)
//			AND IS_SELL_TYPE_DIFFERENT_FROM_PREVIOUS(eSellVar)
			AND IS_SELL_TYPE_COMPATIBLE_WITH_THIS_FACTORY_TYPE(eSellVar)
			AND ARE_THERE_ENOUGH_PLAYERS_FOR_VARIATION(eSellVar)
			AND DOES_FACTORY_HAVE_ENOUGH_PRODUCT_FOR_VARIATION(eSellVar)
			
				serverBD.eSellVar = eSellVar
				PRINTLN("[BIKER_SELL] SERVER_PICK_VARIATION - serverBD.eSellVar = eSellVar = ", BIKER_SELL_GET_VARIATION_DEBUG_PRINT(eSellVar))
				ADD_VARIATION_TO_HISTORY_LIST(eSellVar)
				SET_PREVIOUS_SELL_TYPE(eSellVar)
			#IF IS_DEBUG_BUILD
			ELSE
				IF IS_VARIATION_IN_HISTORY_LIST(eSellVar)
					PRINTLN("[BIKER_SELL] SERVER_PICK_VARIATION, ERROR IS_VARIATION_IN_HISTORY_LIST ")
				ENDIF
				IF IS_VARIATION_BLOCKED(eSellVar)
					PRINTLN("[BIKER_SELL] SERVER_PICK_VARIATION, ERROR IS_VARIATION_BLOCKED ", BIKER_SELL_GET_VARIATION_DEBUG_PRINT(eSellVar))
				ENDIF
				IF IS_OTHER_BOSS_ON_VARIATION(eSellVar)
					PRINTLN("[BIKER_SELL] SERVER_PICK_VARIATION, ERROR IS_OTHER_BOSS_ON_VARIATION ")
				ENDIF
				IF NOT IS_SELL_TYPE_COMPATIBLE_WITH_THIS_FACTORY_TYPE(eSellVar)
					PRINTLN("[BIKER_SELL] SERVER_PICK_VARIATION, ERROR NOT IS_SELL_TYPE_COMPATIBLE_WITH_THIS_FACTORY_TYPE ")
				ENDIF
			#ENDIF
			ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF // #IF IS_DEBUG_BUILD
				
		PRINTLN("[BIKER_SELL] SERVER_PICK_VARIATION = ", BIKER_SELL_GET_VARIATION_DEBUG_PRINT())
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ROUTE_BLOCKED(INT iPos)
	SWITCH GET_SELL_VAR()
		
		CASE eBIKER_SELL_BORDER_PATROL
			IF SHOULD_DROP_OFFS_BE_IN_COUNTRY()
				SWITCH iPos
					CASE 0		RETURN g_sMPTunables.bBIKER_DISABLE_SELL_BORDER_PATROL_0
					CASE 1		RETURN g_sMPTunables.bBIKER_DISABLE_SELL_BORDER_PATROL_1
					CASE 2		RETURN g_sMPTunables.bBIKER_DISABLE_SELL_BORDER_PATROL_2
					CASE 3		RETURN g_sMPTunables.bBIKER_DISABLE_SELL_BORDER_PATROL_3
					CASE 4		RETURN g_sMPTunables.bBIKER_DISABLE_SELL_BORDER_PATROL_4
				ENDSWITCH
			ELSE
				SWITCH iPos
					CASE 0		RETURN g_sMPTunables.bBIKER_DISABLE_SELL_BORDER_PATROL_5
					CASE 1		RETURN g_sMPTunables.bBIKER_DISABLE_SELL_BORDER_PATROL_6
					CASE 2		RETURN g_sMPTunables.bBIKER_DISABLE_SELL_BORDER_PATROL_7
					CASE 3		RETURN g_sMPTunables.bBIKER_DISABLE_SELL_BORDER_PATROL_8
					CASE 4		RETURN g_sMPTunables.bBIKER_DISABLE_SELL_BORDER_PATROL_9
				ENDSWITCH
			ENDIF
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// Select the drop-off route
FUNC BOOL SERVER_SET_ROUTE()
	
	INT iRoute
	
	IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_ROUTE_SET)
	
		IF BIKER_SELL_HAS_A_SET_ROUTE()
		OR BIKER_SELL_STING()
		OR BIKER_SELL_BAG_DROP()
		OR BIKER_SELL_RACE()
		OR BIKER_SELL_CLUB_RUN()
			INT iRouteMax = 5
			
//			IF BIKER_SELL_STING()
//				iRouteMax = 5
//			EL
//			IF BIKER_SELL_EXCHANGE()
			IF BIKER_SELL_RACE()
			OR BIKER_SELL_CLUB_RUN()
				iRouteMax = 10
			ELIF BIKER_SELL_BAG_DROP()
				iRouteMax = 20
			ENDIF
			PRINTLN("[BIKER_SELL] SERVER_SET_ROUTE - siRouteMax = ", iRouteMax)
		
			iRoute = GET_RANDOM_INT_IN_RANGE(0, iRouteMax)
			PRINTLN("[BIKER_SELL] SERVER_SET_ROUTE - iRoute = ", iRoute)
			
			IF IS_ROUTE_BLOCKED(iRoute)
				RETURN FALSE
			ENDIF
							
			serverBD.iRoute = iRoute
//			IF BIKER_SELL_EXCHANGE()
//			AND NOT SHOULD_DROP_OFFS_BE_IN_COUNTRY()
//				serverBD.iRoute += 5
//				PRINTLN("[BIKER_SELL] SERVER_SET_ROUTE - serverBD.iRoute += 5 = ", serverBD.iRoute)
//			ENDIF
			
			IF serverBD.iRandEndRoute = -1
				serverBD.iRandEndRoute = GET_RANDOM_INT_IN_RANGE(0, 3)
				PRINTLN("[BIKER_SELL] SERVER_SET_ROUTE - serverBD.iRandEndRoute = ", serverBD.iRandEndRoute)
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() <> -1
					serverBD.iRoute = GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION()	
					PRINTLN("[BIKER_SELL] SERVER_SET_ROUTE - Debug - serverBD.iRoute = ", serverBD.iRoute)
				ENDIF
			#ENDIF
						
			PRINTLN("[BIKER_SELL] SERVER_SET_ROUTE - serverBD.iRoute = ", serverBD.iRoute)
			
			SET_SERVER_BIT0(eSERVERBITSET0_ROUTE_SET)
			
		// Sea variations have 1 route per start point
		
		ELSE
			// Other variations dont require a route
			SET_SERVER_BIT0(eSERVERBITSET0_ROUTE_SET)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN IS_SERVER_BIT0_SET(eSERVERBITSET0_ROUTE_SET)
ENDFUNC

// Choose vehicle start pos.
FUNC BOOL SERVER_SET_START_POS()	

	INT iStartPos0, iStartPos1, iStartPos2
	
	IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_START_POS_SET)
		INT iBossId = GB_GET_GANG_ID_FROM_BOSS(PLAYER_ID())
		
		#IF IS_DEBUG_BUILD
			IF iBossId > 9
				NET_SCRIPT_ASSERT("SERVER_SET_START_POS iBossId > 9! Will use 0")
				PRINTLN("[BIKER_SELL] SERVER_SET_START_POS iBossId > 9! Will use 0")
				iBossId = 0
			ELIF iBossId < 0
				NET_SCRIPT_ASSERT("SERVER_SET_START_POS iBossId < 0! Will use 0")
				PRINTLN("[BIKER_SELL] SERVER_SET_START_POS iBossId < 0! Will use 0")
				iBossId = 0
			ENDIF
		#ENDIF
				
		PRINTLN("[BIKER_SELL] SERVER_SET_START_POS iBossId = ", iBossId)
		
		IF IS_THIS_AN_AIR_VARIATION()
			SWITCH GET_SHIPMENT_TYPE()
				CASE eBIKER_SHIPMENT_TYPE_SMALL
					iStartPos0 = iBossId * 3 //GET_RANDOM_INT_IN_RANGE(0, 20)
				BREAK
				
				CASE eBIKER_SHIPMENT_TYPE_MEDIUM
					iStartPos0 = iBossId * 3 //GET_RANDOM_INT_IN_RANGE(0, 20)
					iStartPos1 = iStartPos0 + 1 //GET_RANDOM_INT_IN_RANGE(0, 20)
					
					IF iStartPos0 = iStartPos1
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE eBIKER_SHIPMENT_TYPE_LARGE
					iStartPos0 = iBossId * 3 //GET_RANDOM_INT_IN_RANGE(0, 20)
					iStartPos1 = iStartPos0 + 1 //GET_RANDOM_INT_IN_RANGE(0, 20)
					iStartPos2 = iStartPos1 + 1 //GET_RANDOM_INT_IN_RANGE(0, 20)
					
					IF iStartPos0 = iStartPos1
					OR iStartPos0 = iStartPos2
					OR iStartPos1 = iStartPos2
						RETURN FALSE
					ENDIF
				BREAK
			ENDSWITCH
		ELSE
			iStartPos0 = GET_RANDOM_INT_IN_RANGE(0, 5)
						
			#IF IS_DEBUG_BUILD
				IF GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() <> -1
					iStartPos0 = GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION()	
					PRINTLN("[BIKER_SELL] SERVER_SET_START_POS - Debug - iStartPos0 = ", iStartPos0)
				ENDIF
			#ENDIF
		ENDIF

		serverBD.iStartingPos[0] = iStartPos0
		serverBD.iStartingPos[1] = iStartPos1
		serverBD.iStartingPos[2] = iStartPos2

		PRINTLN("[BIKER_SELL] SERVER_SET_START_POS, >>> ", serverBD.iStartingPos[0])
		PRINTLN("[BIKER_SELL] SERVER_SET_START_POS, >>> ", serverBD.iStartingPos[1])
		PRINTLN("[BIKER_SELL] SERVER_SET_START_POS, >>> ", serverBD.iStartingPos[2])
		
		SET_SERVER_BIT0(eSERVERBITSET0_START_POS_SET)
	ENDIF
	
	RETURN IS_SERVER_BIT0_SET(eSERVERBITSET0_START_POS_SET)
ENDFUNC

// Choose which drop-off will contain the wanted rating for the Sting variant.
PROC SERVER_SET_STING()
	
	IF serverBD.iSting = -1
	AND ( BIKER_SELL_STING() OR BIKER_SELL_PROVEN() )
		serverBD.iSting = 0
		PRINTLN("[BIKER_SELL] SERVER_SET_STING, >>> ", serverBD.iSting)
	ENDIF
ENDPROC

PROC SERVER_SET_PROVEN_WANTED()
	INT iRand
	IF serverBD.iWantedTimeElapsed = -1
	AND BIKER_SELL_PROVEN()
		iRand = GET_RANDOM_INT_IN_RANGE(30000, 120000)
		serverBD.iWantedTimeElapsed = iRand
		PRINTLN("[BIKER_SELL] SERVER_SET_PROVEN_WANTED, >>> ", serverBD.iWantedTimeElapsed)
	ENDIF
ENDPROC

FUNC BOOL bSHOULD_CHECK_MIN_DISTANCE()
	SWITCH GET_SELL_VAR() 
		CASE eBIKER_SELL_TRASHMASTER
		CASE eBIKER_SELL_BENSON
		CASE eBIKER_SELL_BAG_DROP
		CASE eBIKER_SELL_STING_OP
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

CONST_INT ciFURTHEST_DROP_MAX_DROPS 10

FUNC BOOL SHOULD_CHECK_FOR_FURTHEST_DROP()
	SWITCH GET_SELL_VAR()
		CASE eBIKER_SELL_PROVEN
		CASE eBIKER_SELL_DEFAULT
		CASE eBIKER_SELL_RACE
		CASE eBIKER_SELL_CLUB_RUN
			// IF TUNEABLE TRUE
				RETURN TRUE
			// ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_RANDOM_FURTHEST_DROP()

	INT i, iFurthestDrop[3]
	FLOAT fDistBetween, fStartDist[3]
	VECTOR vWarehouseCoords, vDropCoord
	
	fStartDist[0] = 0.0
	fStartDist[1] = 0.0
	fStartDist[2] = 0.0
	iFurthestDrop[0] = -1
	iFurthestDrop[1] = -1
	iFurthestDrop[2] = -1
	
	vWarehouseCoords = GET_FACTORY_COORDS(serverBD.factoryId)

	REPEAT ciFURTHEST_DROP_MAX_DROPS i
		vDropCoord = GET_SELL_DROP_COORDS(GET_SELL_VAR(), i, i, -1)
		fDistBetween = GET_DISTANCE_BETWEEN_COORDS(vWarehouseCoords, vDropCoord, FALSE)
		
		// If this is the closest distance so far
		IF (fDistBetween > fStartDist[0])
		
			// Second closest becomes third closest
			fStartDist[2] = fStartDist[1]
			iFurthestDrop[2] = iFurthestDrop[1]
			
			// Closest becomes second closest
			fStartDist[1] = fStartDist[0]
			iFurthestDrop[1] = iFurthestDrop[0]
			
			// New closest is the one
			fStartDist[0] = fDistBetween
			iFurthestDrop[0] = i
			
			PRINTLN("[BIKER_SELL] GET_RANDOM_FURTHEST_DROP - New Furthest drop: ", iFurthestDrop[0], "; distance: ", fDistBetween)
			
		// If this is the second closest distance
		ELIF (fDistBetween < fStartDist[0]) AND (fDistBetween > fStartDist[1])
		
			fStartDist[2] = fStartDist[1]
			iFurthestDrop[2] = iFurthestDrop[1]
			
			fStartDist[1] = fDistBetween
			iFurthestDrop[1] = i
			
			PRINTLN("[BIKER_SELL] GET_RANDOM_FURTHEST_DROP - New Second Furthest drop: ", iFurthestDrop[1], "; distance: ", fDistBetween)
		
		// If third closest
		ELIF (fDistBetween < fStartDist[1]) AND (fDistBetween > fStartDist[2])
		
			fStartDist[2] = fDistBetween
			iFurthestDrop[2] = i
			
			PRINTLN("[BIKER_SELL] GET_RANDOM_FURTHEST_DROP - New Third Furthest drop: ", iFurthestDrop[2], "; distance: ", fDistBetween)
			
		ELSE
			// We dont care about routes that arent closest
		ENDIF
	ENDREPEAT
	
	IF iFurthestDrop[0] != -1
	AND iFurthestDrop[1] != -1
	AND iFurthestDrop[2] != -1
		INT iRand = GET_RANDOM_INT_IN_RANGE(0,3)
		PRINTLN("[BIKER_SELL] GET_RANDOM_FURTHEST_DROP - Randomly selected ", iRand, "; returning drop ", iFurthestDrop[iRand])
		RETURN iFurthestDrop[iRand]
	ENDIF

	RETURN -1

ENDFUNC

FUNC FLOAT GET_MIN_DISTANCE()
	RETURN g_sMPTunables.fBIKER_SELL_MINIMUM_DISTANCE
ENDFUNC

// 2757409 ensure drop-off is far enough away from start pos
FUNC BOOL bPASSED_DIST_CHECK( INT iCount)
	FLOAT fCurrentDist
	VECTOR vWarehouseCoords
	
	IF bSHOULD_CHECK_MIN_DISTANCE()
		
		vWarehouseCoords = GET_FACTORY_COORDS(serverBD.factoryId)
		
		INT iVeh
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iVeh
			
			fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(GET_SELL_DROP_COORDS(GET_SELL_VAR(), iCount, serverBD.iRoute, iVeh), vWarehouseCoords)
			
			PRINTLN("[BIKER_SELL] bPASSED_DIST_CHECK, iCount = ", iCount, " fCurrentDist = ", fCurrentDist, " iVeh = ", iVeh)
			
			// As long as one of the drop offs is > 1000m away, use these/this route
			IF fCurrentDist >= GET_MIN_DISTANCE() //g_sMPTunables.iEXEC_SELL_MIN_SPAWN_DISTANCE_FROM_OFFICE
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GLOBAL_SELL_COORD_BLOCKED(INT iCoord)
	SWITCH iCoord
		CASE 0	RETURN g_sMPTunables.bexec_disable_sell_coord_global0
		CASE 1	RETURN g_sMPTunables.bexec_disable_sell_coord_global1
		CASE 2	RETURN g_sMPTunables.bexec_disable_sell_coord_global2
		CASE 3	RETURN g_sMPTunables.bexec_disable_sell_coord_global3
		CASE 4	RETURN g_sMPTunables.bexec_disable_sell_coord_global4
		CASE 5	RETURN g_sMPTunables.bexec_disable_sell_coord_global5
		CASE 6	RETURN g_sMPTunables.bexec_disable_sell_coord_global6
		CASE 7	RETURN g_sMPTunables.bexec_disable_sell_coord_global7
		CASE 8	RETURN g_sMPTunables.bexec_disable_sell_coord_global8
		CASE 9	RETURN g_sMPTunables.bexec_disable_sell_coord_global9
		CASE 10	RETURN g_sMPTunables.bexec_disable_sell_coord_global10
		CASE 11	RETURN g_sMPTunables.bexec_disable_sell_coord_global11
		CASE 12	RETURN g_sMPTunables.bexec_disable_sell_coord_global12
		CASE 13	RETURN g_sMPTunables.bexec_disable_sell_coord_global13
		CASE 14	RETURN g_sMPTunables.bexec_disable_sell_coord_global14
		CASE 15	RETURN g_sMPTunables.bexec_disable_sell_coord_global15
		CASE 16	RETURN g_sMPTunables.bexec_disable_sell_coord_global16
		CASE 17	RETURN g_sMPTunables.bexec_disable_sell_coord_global17
		CASE 18	RETURN g_sMPTunables.bexec_disable_sell_coord_global18
		CASE 19	RETURN g_sMPTunables.bexec_disable_sell_coord_global19
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC INT GET_MAX_DROP_OFFS_FOR_VARIATION()
	IF BIKER_SELL_HELICOPTER_DROP()
	OR BIKER_SELL_AIR_DROP_AT_SEA()
		RETURN 15
	ENDIF
	
	RETURN ciMAX_AIR_DROP_OFFS
ENDFUNC

FUNC BOOL SHOULD_VEHS_USE_DIFFERENT_DROPOFFS()
	IF BIKER_SELL_TRASHMASTER()
	OR BIKER_SELL_HELICOPTER_DROP()
	OR BIKER_SELL_POSTMAN()
//	OR BIKER_SELL_TACO_VAN()
	OR BIKER_SELL_AIR_DROP_AT_SEA()
	OR BIKER_SELL_BAG_DROP()
//	OR BIKER_SELL_FRIENDS_IN_NEED()
	OR BIKER_SELL_BENSON() 
		RETURN TRUE  
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SERVER_CHOOSE_DROP_OFFS()

	#IF IS_DEBUG_BUILD
	IF serverBD.iDebugVariation2 <> -1
		IF NOT BIKER_SELL_HAS_A_SET_ROUTE()
		AND NOT BIKER_SELL_BENSON()
		AND NOT BIKER_SELL_STING()
//		AND NOT BIKER_SELL_TACO_VAN()
		AND NOT BIKER_SELL_CLUB_RUN()
		AND NOT BIKER_SELL_BAG_DROP()
		AND NOT BIKER_SELL_RACE()
			PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS, DEBUG RETURN TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	#ENDIF

	INT iRand, iCount
	BOOL bSkipCountCheck
	
	// Should we ust the furthest drop from the factory?
	IF SHOULD_CHECK_FOR_FURTHEST_DROP()
		iCount = GET_RANDOM_FURTHEST_DROP()
		PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS [START] SHOULD_CHECK_FOR_FURTHEST_DROP ") 
		bSkipCountCheck = TRUE
	// For these modes we only want the first 5 as they are set coords, see GET_SELL_DROP_COORDS
	ELIF NOT BIKER_SELL_HAS_A_SET_ROUTE()
		// Or else pick random
		iRand = GET_RANDOM_INT_IN_RANGE(0, GET_MAX_SELL_DROP_OFFS())
		bSkipCountCheck = TRUE
		
		PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS [START] GET_MAX_SELL_DROP_OFFS = (", GET_MAX_SELL_DROP_OFFS(), ") ") 
		PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS [START] - NOT BIKER_SELL_HAS_A_SET_ROUTE() - iRand = GET_RANDOM_INT_IN_RANGE(0, GET_MAX_SELL_DROP_OFFS()) = ", iRand) 
		IF IS_GLOBAL_SELL_COORD_BLOCKED(iRand)
			PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS [START] , IS_GLOBAL_SELL_COORD_BLOCKED(", iRand, ") ") 
			RETURN FALSE
		ENDIF
		IF BIKER_SELL_CLUB_RUN()
		OR BIKER_SELL_BENSON()
		OR BIKER_SELL_BAG_DROP()
			iCount = iRand
			PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS [START]  - bag mission - iCount = iRand = ", iCount) 
		ELSE
			iCount = ( iRand + serverBD.iFoundDropCount )
			PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS [START]  - NOT BIKER_SELL_HAS_A_SET_ROUTE() - iCount = ( iRand + serverBD.iFoundDropCount ) = iCount = ( ", iRand, " + ", serverBD.iFoundDropCount, " ) = ", iCount) 
		ENDIF
	ELIF( BIKER_SELL_HELICOPTER_DROP()
	OR BIKER_SELL_AIR_DROP_AT_SEA() )
//	AND iDROP_TARGET() < GET_MAX_DROP_OFFS_FOR_VARIATION()
		iRand = GET_RANDOM_INT_IN_RANGE(0, GET_MAX_DROP_OFFS_FOR_VARIATION())
		IF BIKER_SELL_HELICOPTER_DROP()
		AND SHOULD_DROP_OFFS_BE_IN_COUNTRY()
		AND iRand = 3
		AND serverBD.bAltruistSelected = FALSE
			serverBD.bAltruistSelected = TRUE
			PRINTLN("[BIKER_SELL] - SERVER_CHOOSE_DROP_OFFSserverBD.bAltruistSelected = TRUE")
		ENDIF
		bSkipCountCheck = TRUE
		PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS [START] , NEW_AIR_MODE_LOGIC ") // url:bugstar:2831395 - Sell Missions - Air Drop variations - when selecting less than 10 can we make sure the blips are more spread out
		iCount = iRand 
	ELIF BIKER_SELL_POSTMAN()
		iRand = GET_RANDOM_INT_IN_RANGE(0, GET_MAX_SELL_DROP_OFFS())
		PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS - BIKER_SELL_TACO_VAN/BIKER_SELL_POSTMAN - iRand = ", iRand) 
		bSkipCountCheck = TRUE
		iCount = iRand 
	ELIF BIKER_SELL_BORDER_PATROL()
		iCount = serverBD.iFoundDropCount
		PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS - BIKER_SELL_BORDER_PATROL - iCount = ", iCount)
	ELIF BIKER_SELL_TRASHMASTER()
		iCount = serverBD.iFoundDropCount
		bSkipCountCheck = TRUE
		PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS [START]  - BIKER_SELL_TRASHMASTER, YES ") 
	ELIF BIKER_SELL_FRIENDS_IN_NEED()
		iCount = serverBD.iFoundDropCount
		bSkipCountCheck = TRUE
		PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS [START]  - BIKER_SELL_FRIENDS_IN_NEED, YES ") 
	ELSE
		iCount = ( iRand + serverBD.iFoundDropCount )
		PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS [START]  - iCount ") 
	ENDIF
	
	IF BIKER_SELL_BENSON()
		IF iCount >= 20
			PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS IF iCount >= 20") 
			iCount = 0
		ENDIF
	ELSE
		IF NOT BIKER_SELL_AIR_DROP_AT_SEA()
		AND NOT BIKER_SELL_HELICOPTER_DROP()
		AND NOT BIKER_SELL_CLUB_RUN()
		AND NOT BIKER_SELL_BAG_DROP()
			IF iCount >= iDROP_TARGET()
				PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS IF iCount >= iDROP_TARGET()") 
				iCount = 0
			ENDIF
		ENDIF
	ENDIF
	
	INT iTarget = iDROP_TARGET()
	IF BIKER_SELL_BORDER_PATROL()
		iTarget = iDROP_TARGET()/GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE()
	ELIF BIKER_SELL_CLUB_RUN()
		iTarget = ciNUMBER_OF_CLUB_RUN_DROPS 
	ENDIF
	
	INT iNumVeh
	IF iCount > -1
		IF bPASSED_DIST_CHECK(iCount)		
			PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS bPASSED_DIST_CHECK! ", serverBD.iFoundDropCount, " iTarget = ", iTarget)
			IF serverBD.iFoundDropCount <  iTarget
				PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS serverBD.iFoundDropCount < iDROP_TARGET()!")
				IF NOT IS_BIT_SET(serverBD.dropOffLocationBitSet, iCount)
					PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS IF NOT IS_BIT_SET(serverBD.iActiveDropOffBitset[0], iCount)!")
				
					serverBD.iFoundDropCount++
					
					IF bSkipCountCheck
					OR iCount < iMAX_DROP_OFFS()
						iNumVeh = 0
						
						BOOL bDiffrentDropOffs = SHOULD_VEHS_USE_DIFFERENT_DROPOFFS()
						INT iNumVehForMode = GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() 
						BOOL bClubRun = BIKER_SELL_CLUB_RUN()
						IF bClubRun
						OR BIKER_SELL_BAG_DROP()
						OR BIKER_SELL_RACE()
						OR BIKER_SELL_FRIENDS_IN_NEED()
							iNumVehForMode = GET_NUM_BAGS()
						ENDIF
	//					IF MODE_HAS_PICKUP_BAGS()

						PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS, iTarget = ", iTarget, " iNumVehForMode = ", iNumVehForMode)
						
						REPEAT iNumVehForMode iNumVeh
							IF bDiffrentDropOffs
								PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS, serverBD.iVehCounter[iNumVeh] = ", serverBD.iVehCounter[iNumVeh])
								IF serverBD.iVehCounter[iNumVeh] < ( iTarget / iNumVehForMode)
									SERVER_SET_ACTIVE_DROP(iNumVeh, iCount)
									
									PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS - SHOULD_VEHS_USE_DIFFERENT_DROPOFFS, TRUE, SET_BIT(serverBD.iActiveDropOffBitset, ", iCount, ")", " iNumVeh = ", iNumVeh)
									serverBD.iVehCounter[iNumVeh]++
									iNumVeh = iNumVehForMode
								ENDIF
							ELSE
								IF bClubRun
								AND serverBD.iFoundDropCount != 1
									SET_BIT(serverBD.iHiddenDropOff[iNumVeh], iCount)
									PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS - serverBD.iHiddenDropOff[", iNumVeh, "] = ", iCount)
								ENDIF
								SERVER_SET_ACTIVE_DROP(iNumVeh, iCount)
								PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS - SHOULD_VEHS_USE_DIFFERENT_DROPOFFS, FALSE, SET_BIT(serverBD.iActiveDropOffBitset[", iNumVeh, "], ", iCount, ")")
							ENDIF
						ENDREPEAT
							
						PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS SET_BIT(serverBD.dropOffLocationBitSet,", iCount, ")")
						SET_BIT(serverBD.dropOffLocationBitSet, iCount)	
					ENDIF
					PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS Picked - ", iCount)
					IF NOT BIKER_SELL_HAS_A_SET_ROUTE()
					AND NOT BIKER_SELL_BENSON()
					AND NOT BIKER_SELL_CLUB_RUN()
					AND NOT BIKER_SELL_BAG_DROP()
					//OR BIKER_SELL_EXCHANGE()
					OR BIKER_SELL_STING()
						PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS DONE!")
						RETURN TRUE
					ENDIF
				ELSE
					PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS serverBD.iFoundDropCount < IS_BIT_SET(serverBD.dropOffLocationBitSet, ", iCount, ")!")
				ENDIF
			ENDIF
		ELSE
		
			PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS bPASSED_DIST_CHECK, FALSE", serverBD.iFoundDropCount)
		ENDIF
	ELSE
		PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS - iCount = -1, FALSE")
	ENDIF
	
	PRINTLN("[BIKER_SELL] SERVER_CHOOSE_DROP_OFFS, serverBD.iFoundDropCount = ", serverBD.iFoundDropCount, " iTarget = ", iTarget)

	RETURN (serverBD.iFoundDropCount = iTarget)
ENDFUNC

PROC SERVER_SET_DROP_DONE_FOR_ALL_VEHICLES(INT iClosest)
	INT iLoop
	REPEAT ciMAX_SELL_BAGS iLoop
		SERVER_CLEAR_ACTIVE_DROP(iLoop, iClosest)
		PRINTLN("[BIKER_SELL] SERVER_SET_DROP_DONE_FOR_ALL_VEHICLES, SERVER_INCREMENT_DROP_COUNT - CLEAR_BIT(serverBD.iActiveDropOffBitset veh ", iLoop, " iClosest = ", iClosest)
	ENDREPEAT
ENDPROC

FUNC BOOL HAVE_ALL_CLUB_RUN_BAGS_GOT_TO_THIS_DROP(BOOL bDoOneBagCheck = FALSE)
	INT iDropCount, iLoopBags
	INT iOneBag = -1
	
	REPEAT GET_NUM_BAGS() iLoopBags
		IF serverBD.iAssignedBag[iLoopBags] != -1
			IF serverBD.iVehDropCount[iLoopBags] = 0
				PRINTLN("[BIKER_SELL] SERVER_INCREMENT_DROP_COUNT - HAVE_ALL_CLUB_RUN_BAGS_GOT_TO_THIS_DROP serverBD.iVehDropCount[", iLoopBags, "] = 0")
				RETURN FALSE
			ELSE
				IF iDropCount = 0
					iDropCount = serverBD.iVehDropCount[iLoopBags]
					PRINTLN("[BIKER_SELL] SERVER_INCREMENT_DROP_COUNT - HAVE_ALL_CLUB_RUN_BAGS_GOT_TO_THIS_DROP iDropCount = serverBD.iVehDropCount[", iLoopBags, "] = ", iDropCount)
					iOneBag = iLoopBags
				ELSE
					iOneBag = -1
				ENDIF
				IF serverBD.iVehDropCount[iLoopBags] != iDropCount
					PRINTLN("[BIKER_SELL] SERVER_INCREMENT_DROP_COUNT - HAVE_ALL_CLUB_RUN_BAGS_GOT_TO_THIS_DROP serverBD.iVehDropCount[", iLoopBags, "] != iDropCount - ", serverBD.iVehDropCount[iLoopBags], " != ", iDropCount)
					RETURN FALSE
				ENDIF
				IF serverBD.iVehDropCount[iLoopBags] <= serverBD.iDroppedOffCountClubRun
					PRINTLN("[BIKER_SELL] SERVER_INCREMENT_DROP_COUNT - HAVE_ALL_CLUB_RUN_BAGS_GOT_TO_THIS_DROP serverBD.iVehDropCount[", iLoopBags, "] <= serverBD.iDroppedOffCountClubRun - ", serverBD.iVehDropCount[iLoopBags], " <= ", serverBD.iDroppedOffCountClubRun)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	IF bDoOneBagCheck
	AND iOneBag != -1
		IF serverBD.iVehDropCount[iOneBag] = serverBD.iDroppedOffCountClubRun
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL BIKER_SELL_CLUB_RUN_CHECK_ALL_DROPS_DONE(BOOL bDoOneBagCheck = FALSE)
	IF HAVE_ALL_CLUB_RUN_BAGS_GOT_TO_THIS_DROP(bDoOneBagCheck)
		INT iLoop
		INT iLoopBags
		REPEAT GET_NUM_BAGS() iLoopBags
			REPEAT 32 iLoop
				IF IS_BIT_SET(serverBD.iHiddenDropOff[iLoopBags], iLoop)
					CLEAR_BIT(serverBD.iHiddenDropOff[iLoopBags], iLoop)
					PRINTLN("[BIKER_SELL] SERVER_INCREMENT_DROP_COUNT, CLEAR_BIT(serverBD.iHiddenDropOff[", iLoopBags, "], ", iLoop, ")")
					iLoop = 32
				ENDIF
			ENDREPEAT
			serverBD.iBagEventBitSet[iLoopBags] = 0
		ENDREPEAT
		serverBD.iDroppedOffCountClubRun++
		
		IF NOT BIKER_SELL_HAS_TWIST_HAPPENED()
			IF IS_SERVER_BIT1_SET(eSERVERBITSET1_CLUB_RUN_COPS)
			OR IS_SERVER_BIT1_SET(eSERVERBITSET1_CLUB_RUN_GANG)
				IF serverBD.iDroppedOffCountClubRun = (ciNUMBER_OF_CLUB_RUN_DROPS - 1)
					PRINTLN("[BIKER_SELL] [TWIST] SERVER_INCREMENT_DROP_COUNT - serverBD.iDroppedOffCountClubRun SET_SERVER_BIT0(eSERVERBITSET0_TWIST_HAPPENED)")
					SET_SERVER_BIT0(eSERVERBITSET0_TWIST_HAPPENED)
				ELSE
					INT iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
					IF iRand > 50
						SET_SERVER_BIT0(eSERVERBITSET0_TWIST_HAPPENED)
						PRINTLN("[BIKER_SELL] [TWIST] SERVER_INCREMENT_DROP_COUNT - SET_SERVER_BIT0(eSERVERBITSET0_TWIST_HAPPENED)")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		PRINTLN("[BIKER_SELL] SERVER_INCREMENT_DROP_COUNT - serverBD.iDroppedOffCountClubRun = ", serverBD.iDroppedOffCountClubRun)
		serverBD.iCurrentDropTeam = -1
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SERVER_INCREMENT_DROP_COUNT(INT iClosest, INT iVeh = 0)

	IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
	OR BIKER_SELL_CLUB_RUN()
	OR BIKER_SELL_FRIENDS_IN_NEED()
	OR BIKER_SELL_RACE()
	OR BIKER_SELL_BAG_DROP()
		IF iVeh >= 0
			serverBD.iVehDropCount[iVeh]++
			PRINTLN("[BIKER_SELL] SERVER_INCREMENT_DROP_COUNT - serverBD.iVehDropCount[", iVeh, "] = ", serverBD.iVehDropCount[iVeh])
		ENDIF
	ENDIF
	
	serverBD.iDroppedOffCount++
	PRINTLN("[BIKER_SELL] SERVER_INCREMENT_DROP_COUNT, COMPLETE iClosest = ", iClosest, " total now = ", serverBD.iDroppedOffCount, " iVeh = ", iVeh, " - serverBD.iDroppedOffCount")
	
	IF BIKER_SELL_CLUB_RUN()
	AND BIKER_SELL_CLUB_RUN_CHECK_ALL_DROPS_DONE()	
		SERVER_SET_DROP_DONE_FOR_ALL_VEHICLES(iClosest)	
	ELIF BIKER_SELL_FRIENDS_IN_NEED()
	OR BIKER_SELL_BENSON()
		SERVER_SET_DROP_DONE_FOR_ALL_VEHICLES(iClosest)
	ELSE
		PRINTLN("[BIKER_SELL] SERVER_INCREMENT_DROP_COUNT - CLEAR_BIT(serverBD.iActiveDropOffBitset = ", iClosest, " iVeh = ", iVeh)
		SERVER_CLEAR_ACTIVE_DROP(iVeh, iClosest)		
	ENDIF
	
ENDPROC

PROC SET_SELL_VEH_HAS_DELIVERED_CONTRABAND(INT iVeh)
	SWITCH iVeh
		CASE 0
			IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_VEH_0_DELIVERED)
				PRINTLN("[BIKER_SELL], SET_SELL_VEH_HAS_DELIVERED_CONTRABAND - veh ", iVeh, " has been delivered")
				SET_SERVER_BIT1(eSERVERBITSET1_VEH_0_DELIVERED)
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_VEH_1_DELIVERED)
				PRINTLN("[BIKER_SELL], SET_SELL_VEH_HAS_DELIVERED_CONTRABAND - veh ", iVeh, " has been delivered")
				SET_SERVER_BIT1(eSERVERBITSET1_VEH_1_DELIVERED)
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_VEH_2_DELIVERED)
				PRINTLN("[BIKER_SELL], SET_SELL_VEH_HAS_DELIVERED_CONTRABAND - veh ", iVeh, " has been delivered")
				SET_SERVER_BIT1(eSERVERBITSET1_VEH_2_DELIVERED)
			ENDIF
		BREAK
		CASE 3
			IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_VEH_3_DELIVERED)
				PRINTLN("[BIKER_SELL], SET_SELL_VEH_HAS_DELIVERED_CONTRABAND - veh ", iVeh, " has been delivered")
				SET_SERVER_BIT1(eSERVERBITSET1_VEH_3_DELIVERED)
			ENDIF
		BREAK
		CASE 4
			IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_VEH_4_DELIVERED)
				PRINTLN("[BIKER_SELL], SET_SELL_VEH_HAS_DELIVERED_CONTRABAND - veh ", iVeh, " has been delivered")
				SET_SERVER_BIT1(eSERVERBITSET1_VEH_4_DELIVERED)
			ENDIF
		BREAK
		CASE 5
			IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_VEH_5_DELIVERED)
				PRINTLN("[BIKER_SELL], SET_SELL_VEH_HAS_DELIVERED_CONTRABAND - veh ", iVeh, " has been delivered")
				SET_SERVER_BIT1(eSERVERBITSET1_VEH_5_DELIVERED)
			ENDIF
		BREAK
		CASE 6
			IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_VEH_6_DELIVERED)
				PRINTLN("[BIKER_SELL], SET_SELL_VEH_HAS_DELIVERED_CONTRABAND - veh ", iVeh, " has been delivered")
				SET_SERVER_BIT1(eSERVERBITSET1_VEH_6_DELIVERED)
			ENDIF
		BREAK
		CASE 7
			IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_VEH_7_DELIVERED)
				PRINTLN("[BIKER_SELL], SET_SELL_VEH_HAS_DELIVERED_CONTRABAND - veh ", iVeh, " has been delivered")
				SET_SERVER_BIT1(eSERVERBITSET1_VEH_7_DELIVERED)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_THIS_A_HIDDEN_DROP_OFF(INT i)
	INT iLoop
	REPEAT ciMAX_SELL_VEHS iLoop
		IF IS_BIT_SET(serverBD.iHiddenDropOff[iLoop], i)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE	
ENDFUNC
//VECTOR vDropLocate2, vDropLocate
//FLOAT fLocateWidth
FUNC BOOL IS_BIKE_IN_REAR_OF_TRUCK(VEHICLE_INDEX dropOffVeh, INT iDropOffVeh)
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffVeh[iDropOffVeh])
		VEHICLE_INDEX viVehicle = NET_TO_VEH(serverBD.niDropOffVeh[iDropOffVeh])
		IF DOES_ENTITY_EXIST(viVehicle)
		AND IS_VEHICLE_DRIVEABLE(viVehicle)
			//IF IS_ENTITY_IN_ANGLED_AREA(dropOffVeh, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viVehicle, vDropLocate), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viVehicle, vDropLocate2), fLocateWidth)
			IF IS_ENTITY_IN_ANGLED_AREA(dropOffVeh, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viVehicle, <<0.0, 0.3, 0.1>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viVehicle,<<0.000, -3.4, 1.4>>), 2.1)
				PRINTLN("[BIKER_SELL] IS_BIKE_IN_REAR_OF_TRUCK - iDropOffVeh = ", iDropOffVeh)//, "IS_ENTITY_AT_COORD(dropOffVeh, ", vDrop, ", ", vDropLocate2, ")")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE

ENDFUNC

FUNC BOOL AM_I_IN_REAR_OF_TRUCK(INT iDropOffVeh)
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffVeh[iDropOffVeh])
		VEHICLE_INDEX viVehicle = NET_TO_VEH(serverBD.niDropOffVeh[iDropOffVeh])
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viVehicle, <<0.0, 0.3, 0.1>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viVehicle,<<0.000, -3.4, 1.4>>), 2.1)

				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE

ENDFUNC

FUNC BOOL bDROP_CRATE_IN_FINAL_DROPOFF()//(INT iDropOffVeh)

//	SWITCH GET_SELL_VAR()
//		CASE eBIKER_SELL_TRASHMASTER	
//		
//			RETURN ( GET_SELL_VEH_DROP_OFF_COUNT(iDropOffVeh) < ( GET_SELL_VEH_TARGET_DROP_COUNT() - 1 ) )
//	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

//PROC SET_ALL_EXCHANGE_DROPS_DONE()
//	IF HAVE_ALL_1ST_LOCATION_BIKE_BEEN_DROPPED_OFF()
//		PRINTLN("[BIKER_SELL] BIKER_SELL_EXCHANGE - SET_ALL_EXCHANGE_DROPS_DONE")
//		//Set twist has happened. 
//		SET_SERVER_BIT0(eSERVERBITSET0_TWIST_HAPPENED)
//		INT iLoop
//		//Swap about the vehicles to make the new ones
//		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iLoop
//			NETWORK_INDEX niTemp = serverBD.niVeh[iLoop]
//			serverBD.niVeh[iLoop] = serverBD.niDropOffVeh[iLoop] 
//			serverBD.niDropOffVeh[iLoop] = niTemp
//			PRINTLN("[BIKER_SELL] BIKER_SELL_EXCHANGE - Swapped around serverBD.niVeh[", iLoop, "] & serverBD.niVeh[", iLoop, "]")
//		ENDREPEAT
//		
//		//reset the drop count
//		REPEAT ciMAX_SELL_BAGS iLoop
//			serverBD.iVehDropCount[iLoop] = 0
//		ENDREPEAT
//		serverBD.iDroppedOffCount = 0
//		CLEAR_SERVER_BIT1(eSERVERBITSET1_VEH_7_DELIVERED)
//		CLEAR_SERVER_BIT1(eSERVERBITSET1_VEH_6_DELIVERED)
//		CLEAR_SERVER_BIT1(eSERVERBITSET1_VEH_5_DELIVERED)
//		CLEAR_SERVER_BIT1(eSERVERBITSET1_VEH_4_DELIVERED)
//		CLEAR_SERVER_BIT1(eSERVERBITSET1_VEH_3_DELIVERED)
//		CLEAR_SERVER_BIT1(eSERVERBITSET1_VEH_2_DELIVERED)
//		CLEAR_SERVER_BIT1(eSERVERBITSET1_VEH_1_DELIVERED)
//		CLEAR_SERVER_BIT1(eSERVERBITSET1_VEH_0_DELIVERED)
//	ENDIF
//ENDPROC

PROC INVALIDATE_SELL_VEH(INT i)

	ENTITY_INDEX eiSellVeh
	eiSellVeh = VEH_ENT(i)
	
	IF DOES_ENTITY_EXIST(eiSellVeh)
	AND NOT IS_ENTITY_DEAD(eiSellVeh)
		PRINTLN("[BIKER_SELL] INVALIDATE_SELL_VEH, i = ", i)
		SET_ENTITY_AS_NO_LONGER_NEEDED(eiSellVeh)
	ENDIF
ENDPROC

//PROC SERVER_MAINTAIN_DROP_VEHS()
//	INT i
//	VEHICLE_INDEX vehId
//	INT iTime = 10000
//
//	IF DOES_MODE_NEED_DROP_OFF_VEH()
//		REPEAT GET_NUM_DROP_OFF_VEH_FOR_VARIATION() i
//			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffVeh[i])
//			
//				IF NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(i)
//				
//					vehId = NET_TO_VEH(serverBD.niDropOffVeh[i])
//				
//					IF DOES_ENTITY_EXIST(vehId)
//					AND NOT IS_ENTITY_DEAD(vehId)
//					
//						IF BIKER_SELL_BENSON()
//							IF NOT IS_BIT_SET(serverBD.iDropVehPenaltyBitSet, i)
//							
//								IF IS_VEHICLE_STUCK_TIMER_UP(vehId, VEH_STUCK_ON_ROOF, iTime)
//								OR IS_VEHICLE_STUCK_TIMER_UP(vehId, VEH_STUCK_ON_SIDE, iTime)
//								OR IS_ENTITY_IN_WATER(vehId)
//									SERVER_INCREASE_DROP_PENALTY()
//									SET_BIT(serverBD.iDropVehPenaltyBitSet, i)
//									PRINTLN("[BIKER_SELL] SERVER_MAINTAIN_DROP_VEHS, SERVER_INCREASE_DROP_PENALTY, IS_VEHICLE_STUCK_TIMER_UP i = ", i)
//								ENDIF
//								
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDREPEAT
//	ENDIF
//ENDPROC

PROC SERVER_MAINTAIN_DROP_OFFS( VEHICLE_INDEX dropOffVeh, INT iDropOffVeh, INT& iClosest)

	INT i
	INT iVehDropOffCount
	VECTOR vDrop
	INT iTempClosest = -1
	
	FLOAT fCurrentDist
	FLOAT fShortDist = 9999999.99

	ENTITY_INDEX eiEntity 
//	PED_INDEX pedID
	
	PLAYER_INDEX playerDriver
	
	#IF IS_DEBUG_BUILD
	BOOL bCrateExistsForDebug
	#ENDIF
	
//	INT iPed
	
	IF NOT HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()	
	
		// Checking for crates in drop-offs
		IF bOK_TO_DO_CRATES()
		AND NOT BIKER_SELL_POSTMAN()
		
			PRINTLN("SERVER_MAINTAIN_DROP_OFFS, CRATES ")
			
			IF bPOST_VEH_PARKED(iDropOffVeh)
				PRINTLN("SERVER_MAINTAIN_DROP_OFFS, bPOST_VEH_PARKED ")			
			ENDIF
		
			iVehDropOffCount = GET_SELL_VEH_DROP_OFF_COUNT(iDropOffVeh)
			IF iVehDropOffCount < ciMAX_CRATES
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[iVehDropOffCount].netId[iDropOffVeh]) 
					eiEntity = NET_TO_ENT(serverBD.sContraband[iVehDropOffCount].netId[iDropOffVeh])
					PRINTLN("SERVER_MAINTAIN_DROP_OFFS - eiEntity = NET_TO_ENT(serverBD.sContraband[", iVehDropOffCount, "].netId[", iDropOffVeh, "])")
					#IF IS_DEBUG_BUILD
					bCrateExistsForDebug = TRUE
					#ENDIF
				ENDIF
			ENDIF
		ELIF BIKER_SELL_POSTMAN()
		AND IS_NET_PLAYER_OK(serverBD.playerRequestedCrate, FALSE)
			eiEntity = GET_PLAYER_PED(serverBD.playerRequestedCrate)
			PRINTLN("[PARCEL] SERVER_MAINTAIN_DROP_OFFS, BIKER_SELL_POSTMAN ", GET_PLAYER_NAME(serverBD.playerRequestedCrate))
		ELSE
			PRINTLN("SERVER_MAINTAIN_DROP_OFFS, dropOffVeh ")
			// Checking for vehicles in drop-offs
			eiEntity = dropOffVeh
		ENDIF
		
		// Work out closest drop-off
		REPEAT GET_MAX_SELL_DROP_OFFS() i
		
			// Dont use closest drop off for border patrol, packages must be dropped off in sequence
			IF BIKER_SELL_BORDER_PATROL()
				// For border patrol, loop through drops and make the first active drop reached the closest drop off.
				IF iTempClosest = -1
					IF IS_BIT_SET(serverBD.iActiveDropOffBitset[iDropOffVeh], i)
						iTempClosest = i
						PRINTLN("[bp spam] - closest = ", i)
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(serverBD.iActiveDropOffBitset[iDropOffVeh], i)
				AND NOT IS_THIS_A_HIDDEN_DROP_OFF(i)
					vDrop = GET_SELL_DROP_COORDS(GET_SELL_VAR(), i, serverBD.iRoute, iDropOffVeh, DEFAULT, bIS_POSTMAN_PAT_VEH(iDropOffVeh))
					PRINTLN("[1546486] vDrop = ", vDrop)
					fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vDrop, GET_ENTITY_COORDS(dropOffVeh, FALSE))
					IF fCurrentDist < fShortDist
						fShortDist = fCurrentDist
						iClosest = i
						PRINTLN("[1546486] closest = ", i, " fCurrentDist = ", fCurrentDist, " fShortDist = ", fShortDist)
					ENDIF
				ENDIF
			ENDIF
			
		ENDREPEAT
		
		IF BIKER_SELL_BORDER_PATROL()
			IF iTempClosest != -1
				iClosest = iTempClosest
			ENDIF
		ENDIF
		
		// And count drop offs
		IF DOES_ENTITY_EXIST(eiEntity)
		AND NOT IS_ENTITY_DEAD(eiEntity)
			#IF IS_DEBUG_BUILD
				IF bCrateExistsForDebug 
					PRINTLN("[bCrateExistsForDebug] Crate exists checking coords....")
					VECTOR vCrateCoords = GET_ENTITY_COORDS(eiEntity, FALSE)
					VECTOR vDropOFfCoords = GET_SELL_DROP_COORDS(GET_SELL_VAR(), iClosest, serverBD.iRoute, iDropOffVeh, DEFAULT, bIS_POSTMAN_PAT_VEH(iDropOffVeh))
					FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(vCrateCoords, vDropOffCoords)
					BOOL bAtCoords = ENTITY_AT_DROP_OFF_COORDS(GET_SELL_DROP_COORDS(GET_SELL_VAR(), iClosest, serverBD.iRoute, iDropOffVeh, DEFAULT, bIS_POSTMAN_PAT_VEH(iDropOffVeh)), eiEntity, TRUE)
					PRINTLN("[bCrateExistsForDebug] bAtCoords = ", bAtCoords)
					PRINTLN("[bCrateExistsForDebug] vCrateCoords = ", vCrateCoords)
					PRINTLN("[bCrateExistsForDebug] vDropOFfCoords = ", vDropOFfCoords)
					PRINTLN("[bCrateExistsForDebug] Distance between = ", fDist)
					PRINTLN("[bCrateExistsForDebug] iClosest = ", iClosest)
					PRINTLN("[bCrateExistsForDebug] ")
				ENDIF
			#ENDIF
			
			VECTOR vDropCoords = GET_SELL_DROP_COORDS(GET_SELL_VAR(), iClosest, serverBD.iRoute, iDropOffVeh, DEFAULT, bIS_POSTMAN_PAT_VEH(iDropOffVeh))
			
			IF ( BIKER_SELL_BENSON() AND IS_BIKE_IN_REAR_OF_TRUCK(dropOffVeh, iDropOffVeh) )
			OR ( BIKER_SELL_STING() AND ENTITY_AT_DROP_OFF_COORDS(vDropCoords, eiEntity, TRUE) AND GET_PLAYER_WANTED_LEVEL(GET_DRIVER_OF_SELL_VEH(iDropOffVeh)) = 0 )
			OR ( NOT BIKER_SELL_BENSON() AND NOT BIKER_SELL_STING() AND ENTITY_AT_DROP_OFF_COORDS(vDropCoords, eiEntity, TRUE) )
			
				IF BIKER_SELL_BENSON() 
					IF NOT IS_BIT_SET(serverBD.iDropOffVehDoneBitSet, iDropOffVeh)
						SET_BIT(serverBD.iDropOffVehDoneBitSet, iDropOffVeh)
						PRINTLN("[BIKER_SELL] SERVER_MAINTAIN_DROP_OFFS, iDropOffVeh done ", iDropOffVeh)
					ENDIF
				ENDIF
			
				IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_DROP_OFF_REACHED)
					SET_SERVER_BIT1(eSERVERBITSET1_DROP_OFF_REACHED)
					PRINTLN("[BIKER_SELL] SERVER_MAINTAIN_DROP_OFFS, eSERVERBITSET1_DROP_OFF_REACHED  ")
				ENDIF
			
				IF BIKER_SELL_STING()
				AND NOT BIKER_SELL_HAS_TWIST_HAPPENED()
					PRINTLN("[BIKER_SELL] BIKER_SELL_STING - BIKER_SELL_SET_1ST_LOCATION_BIKE_BEEN_DROPPED_OFF - ", iDropOffVeh)
					SET_SERVER_BIT0(eSERVERBITSET0_TWIST_HAPPENED)
					SET_CLIENT_BIT0(eCLIENTBITSET0_SEA_DEFEND_SPOOKED_PEDS)
				ELIF BIKER_SELL_POSTMAN()
				AND NOT IS_BIT_SET(serverBD.iPostVehParkedForDeliveryBitSet, iDropOffVeh)
					playerDriver = GET_DRIVER_OF_SELL_VEH(iDropOffVeh)
					IF playerDriver <> INVALID_PLAYER_INDEX()
						SET_POSTMAN_VEH(iDropOffVeh)
						SERVER_SET_POSTMAN(TRUE, playerDriver, iDropOffVeh)
						PRINTLN("[BIKER_SELL] [POST] [PARCEL] SERVER_MAINTAIN_DROP_OFFS, iPostVehParkedForDeliveryBitSet = (", iDropOffVeh, ")", " playerDriver = ", GET_PLAYER_NAME(playerDriver))
					ENDIF
				ELSE
					SERVER_INCREMENT_DROP_COUNT(iClosest, iDropOffVeh)
					
					IF bOK_TO_DO_CRATES()
					OR BIKER_SELL_BENSON()
						IF NOT IS_THIS_AN_AIR_VARIATION()
							PRINTLN("[BIKER_SELL]  [SELL AUDIO] SERVER_MAINTAIN_DROP_OFFS, DLC_Exec_Land_Multiple_Sounds ")
							
							VECTOR vSoundPos
							vSoundPos = VEH_POS(iDropOffVeh)
							IF BIKER_SELL_POSTMAN()						
								IF DOES_ENTITY_EXIST(eiEntity)
								AND NOT IS_ENTITY_DEAD(eiEntity)
									vSoundPos = GET_ENTITY_COORDS(eiEntity)
									PRINTLN("[BIKER_SELL]  [SELL AUDIO] SERVER_MAINTAIN_DROP_OFFS, BIKER_SELL_POSTMAN, DLC_Exec_Land_Multiple_Sounds ")
								ENDIF
							ENDIF
							PLAY_SOUND_FROM_COORD(-1, GET_SOUNDNAME(), vSoundPos, GET_SOUNDSET_FOR_CRATE_DROP(), TRUE, 0)
						ENDIF
					ENDIF
					
					// Check for all deliveries made for vehicle
					IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
						PRINTLN("[BIKER_SELL] DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES(", iDropOffVeh, ")")
						IF GET_SELL_VEH_DROP_OFF_COUNT(iDropOffVeh) = GET_SELL_VEH_TARGET_DROP_COUNT(iDropOffVeh)
						OR BIKER_SELL_BENSON()
							
							SET_SELL_VEH_HAS_DELIVERED_CONTRABAND(iDropOffVeh)
							PRINTLN("[BIKER_SELL] SET_SELL_VEH_HAS_DELIVERED_CONTRABAND(", iDropOffVeh, ")")
						ELSE
							PRINTLN("[BIKER_SELL] SET_SELL_VEH_HAS_DELIVERED_CONTRABAND(", iDropOffVeh, ")GET_SELL_VEH_DROP_OFF_COUNT = ", GET_SELL_VEH_DROP_OFF_COUNT(iDropOffVeh), " GET_SELL_VEH_TARGET_DROP_COUNT() = ", GET_SELL_VEH_TARGET_DROP_COUNT() )
						ENDIF
					ELSE
						PRINTLN("[BIKER_SELL] ELSE(", iDropOffVeh, ")")
						IF HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
							PRINTLN("[BIKER_SELL] SET_SELL_VEH_HAS_DELIVERED_CONTRABAND(", iDropOffVeh, ")")
							SET_SELL_VEH_HAS_DELIVERED_CONTRABAND(iDropOffVeh)
						ENDIF
					ENDIF
					
					IF BIKER_SELL_POSTMAN()
						CLEAR_POSTMAN_VEH(iDropOffVeh)
						SERVER_SET_POSTMAN(FALSE, serverBD.playerRequestedCrate, iDropOffVeh)
						serverBD.playerRequestedCrate = INVALID_PLAYER_INDEX()
						PRINTLN("[BIKER_SELL] [PARCEL] SERVER_MAINTAIN_DROP_OFFS, RESET POSTMAN ")
					ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[2993511] - Entity not at drop")
				PRINTLN("[2993511] - Object? ", GET_STRING_FROM_BOOL(IS_ENTITY_AN_OBJECT(eiEntity)))
				PRINTLN("[2993511] - Vehicle? ", GET_STRING_FROM_BOOL(IS_ENTITY_A_VEHICLE(eiEntity)))
				VECTOR vEntCoord = GET_ENTITY_COORDS(eiEntity)
				PRINTLN("[2993511] - Coords: ", vEntCoord)
				PRINTLN("[2993511] - Drop Coords: ", vDropCoords)
			#ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			IF bCrateExistsForDebug 
				PRINTLN("[bCrateExistsForDebug] Crate exists but dead!")
			ENDIF
		#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLIENT_MANAGE_SMOKE_FROM_DROP_OFFS()
	IF HAS_ANY_SELL_VEHICLE_BEEN_ENTERED()
	
		INT iDropOffVeh, i 
		
		IF IS_PLAYER_IN_ANY_SELL_VEH(FALSE)
			iDropOffVeh = GET_SELL_VEH_I_AM_IN_INT()
		ELSE
			iDropOffVeh = GET_CLOSEST_SELL_VEH_TO_LOCAL_PLAYER(TRUE)
		ENDIF
			
		IF iDropOffVeh <> -1
		
			INT iClosest
			iClosest = serverBD.iClosestDropOff[iDropOffVeh]

			IF iClosest <> -1
				IF IS_THIS_AN_AIR_VARIATION()
		//		AND IS_ENTITY_ALIVE(dropOffVeh)
				AND IS_BIT_SET(serverBD.iActiveDropOffBitset[iDropOffVeh], iClosest)
					ADD_PARTICLE_FX(iClosest, GET_SELL_DROP_COORDS(GET_SELL_VAR(), iClosest, serverBD.iRoute, iDropOffVeh))
				ELSE
					BOOL bShouldCleanupPtfx = TRUE
					i = 0
					
					//-- Don't clean up the flare if there's multiple sell vehicles, and another vehicle still has to complete this drop off
					IF NOT IS_BIT_SET(serverBD.iActiveDropOffBitset[iDropOffVeh], iClosest)
						REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
							IF IS_BIT_SET(serverBD.iActiveDropOffBitset[i], iClosest)
								bShouldCleanupPtfx = FALSE
							ENDIF
						ENDREPEAT
					ELSE
						bShouldCleanupPtfx = FALSE
					ENDIF
					
					IF bShouldCleanupPtfx
						CLEANUP_CRATE_FLARE(iClosest)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SERVER_SET_START_WAREHOUSE()
	IF serverBD.iStartingWarehouse = -1
		
		serverBD.iStartingWarehouse = ENUM_TO_INT(sLocaldata.contraData.eMissionFactory)
		
		IF serverBD.iStartingWarehouse < 1
			PRINTLN("[BIKER_SELL] SERVER_SET_START_WAREHOUSE serverBD.iStartingWarehouse < 1")
			serverBD.iStartingWarehouse = 1
		ENDIF
		
		serverBD.iStartingContraband = GET_CONTRABAND_UNITS_TO_SELL_FROM_BUYER()
		
		IF serverBD.iStartingContraband <= 0
			PRINTLN("[BIKER_SELL] SERVER_SET_START_WAREHOUSE, iStartingContraband = 0, adding 1 contraband to warehouse so there's something to sell")
			serverBD.iStartingContraband = 1
			SET_SERVER_BIT1(eSERVERBITSET1_LAUNCHED_WITH_NO_CONTRABAND)
		ENDIF
		PRINTLN("[BIKER_SELL] SERVER_SET_START_WAREHOUSE, iStartingContraband =  ", serverBD.iStartingContraband)
		PRINTLN("[BIKER_SELL] SERVER_SET_START_WAREHOUSE, iStartingWarehouse =  ", serverBD.iStartingWarehouse)
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_CLOSEST_DROP_OFF_TO_PED(INT iBag, PED_INDEX piPed, INT& iClosest)

	INT i
	VECTOR vDrop
	FLOAT fCurrentDist
	FLOAT fShortDist = 9999999.99
	VECTOR vBag = GET_ENTITY_COORDS(piPed, FALSE)
	
	REPEAT GET_MAX_SELL_DROP_OFFS() i
		IF IS_BIT_SET(serverBD.iActiveDropOffBitset[iBag], i)
			IF NOT IS_THIS_A_HIDDEN_DROP_OFF(i)
		
				vDrop = GET_SELL_DROP_COORDS(GET_SELL_VAR(), i, serverBD.iRoute, iBag, DEFAULT, FALSE)
				PRINTLN("[1546486] iGET_CLOSEST_DROP_OFF_TO_BAG vDrop = ", vDrop, " iBag = ", iBag)
				fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vDrop, vBag)
				IF fCurrentDist < fShortDist
					fShortDist = fCurrentDist
					iClosest = i
					PRINTLN("[1546486] iGET_CLOSEST_DROP_OFF_TO_BAG closest = ", i, " fCurrentDist = ", fCurrentDist, " fShortDist = ", fShortDist, " iBag = ", iBag)
				ENDIF
			ELSE
				//PRINTLN("[1546486] ERROR, iGET_CLOSEST_DROP_OFF_TO_BAG IS_THIS_A_HIDDEN_DROP_OFF closest = ", i, " fCurrentDist = ", fCurrentDist, " fShortDist = ", fShortDist, " iBag = ", iBag)
			ENDIF
		ELSE
			//PRINTLN("[1546486] ERROR iGET_CLOSEST_DROP_OFF_TO_BAG iActiveDropOffBitset = ", i, " fCurrentDist = ", fCurrentDist, " fShortDist = ", fShortDist, " iBag = ", iBag)
		ENDIF
	ENDREPEAT
ENDPROC

FUNC PED_INDEX GET_NEAREST_PED_TO_BAG(PLAYER_INDEX piPlayerCarryingBag)

	INT i
	FLOAT fClosestPed = 9999.99
	FLOAT fTempDist
	PED_INDEX pedToReturn = INT_TO_NATIVE(PED_INDEX, -1)

	IF DOES_VARIATION_HAVE_AI_PEDS()
		REPEAT GET_NUM_PEDS_FOR_VARIATION() i
			IF NOT IS_PED_INJURED(GET_AI_PED_PED_INDEX(i))
				fTempDist = GET_DISTANCE_BETWEEN_ENTITIES(GET_PLAYER_PED(piPlayerCarryingBag), GET_AI_PED_PED_INDEX(i))
				IF fTempDist < fClosestPed
					fClosestPed = fTempDist
					pedToReturn = GET_AI_PED_PED_INDEX(i)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN pedToReturn
ENDFUNC

PROC SERVER_GIVE_PED_CHEER_SCENARIO(PED_INDEX pedAtDropOff, INT iCount)
	IF BIKER_SELL_FRIENDS_IN_NEED()
	//OR BIKER_SELL_EXCHANGE()
		IF DOES_ENTITY_EXIST(pedAtDropOff)
		AND NOT IS_ENTITY_DEAD(pedAtDropOff)
			PRINTLN("SERVER_GIVE_PED_CHEER_SCENARIO, iCount = ", iCount)
			IF NOT IS_BIT_SET(serverBD.iPedBitSet, iCount)
				DROP_AMBIENT_PROP(pedAtDropOff)
				GIVE_PED_SCENARIO(pedAtDropOff, sGET_PED_SCENARIO(TRUE), TRUE)
				PLAY_PED_AMBIENT_SPEECH(pedAtDropOff, "GENERIC_THANKS", SPEECH_PARAMS_STANDARD)
				PRINTLN("SERVER_GIVE_PED_CHEER_SCENARIO, STOP_PED_SCENARIO ")
				IF BIKER_SELL_FRIENDS_IN_NEED()
					SET_PED_AS_NO_LONGER_NEEDED(pedAtDropOff)
				ENDIF
				SET_BIT(serverBD.iPedBitSet, iCount)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SERVER_MAINTAIN_BAG_DROP_OFFS(INT& iClosest, INT iBags)

//	INT iBags
//	VECTOR vDropCoords
	PED_INDEX piPed
//	VECTOR vPedCoords
		
	//REPEAT GET_NUM_BAGS() iBags
		PLAYER_INDEX piPlayer = INT_TO_PLAYERINDEX(serverBD.iAssignedBag[iBags])
		IF serverBD.iAssignedBag[iBags] != -1
			IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
					
				piPed = GET_PLAYER_PED(piPlayer)
				
				PRINTLN("GET_CLOSEST_DROP_OFF_TO_PED   ", GET_PLAYER_NAME(piPlayer))
				GET_CLOSEST_DROP_OFF_TO_PED(iBags, piPed, iClosest)
				
//				vDropCoords = GET_SELL_DROP_COORDS(GET_SELL_VAR(), iClosest, serverBD.iRoute, iBags, DEFAULT, FALSE)
//				
//				vPedCoords = GET_ENTITY_COORDS(piPed)
//				
//				IF vPedCoords.z < -40.0
//					vPedCoords = NETWORK_GET_LAST_PLAYER_POS_RECEIVED_OVER_NETWORK(piPlayer)
//					PRINTLN("[BIKER_SELL] SERVER_MAINTAIN_BAG_DROP_OFFS, NETWORK_GET_LAST_PLAYER_POS_RECEIVED_OVER_NETWORK ", vPedCoords)
//				ENDIF 
//				
//				PRINTLN("[BIKER_SELL] SERVER_MAINTAIN_BAG_DROP_OFFS A - iBags = ", iBags, " - vDropCoords = ", vDropCoords, " iClosest = ", iClosest, " piPlayer = ", GET_PLAYER_NAME(piPlayer), " coord = ", vPedCoords)
				
//				IF ENTITY_AT_DROP_OFF_COORDS(vDropCoords, piPed, TRUE)
//				OR GET_DISTANCE_BETWEEN_COORDS(vDropCoords, vPedCoords) < 1.0
//					IF IS_BIT_SET(serverBD.iActiveDropOffBitset[iBags], iClosest)
//						IF NOT IS_THIS_A_HIDDEN_DROP_OFF(iClosest)
//							IF serverBD.iCurrentDropTeam = -1
//							AND BIKER_SELL_CLUB_RUN()
//								serverBD.iCurrentDropTeam = iClosest
//								PRINTLN("[BIKER_SELL] SERVER_MAINTAIN_BAG_DROP_OFFS serverBD.iCurrentDropTeam = ", serverBD.iCurrentDropTeam)
//							ENDIF  
//							PRINTLN("[BIKER_SELL] SERVER_MAINTAIN_BAG_DROP_OFFS B - iBags = ", iBags, " - vDropCoords = ", vDropCoords, " iClosest = ", iClosest)
//							SERVER_GIVE_PED_CHEER_SCENARIO(GET_NEAREST_PED_TO_BAG(piPlayer), serverBD.iDroppedOffCount)
//							BIKER_SELL_SET_1ST_LOCATION_BIKE_BEEN_DROPPED_OFF(iBags)
//							IF NOT BIKER_SELL_EXCHANGE()
//								SERVER_INCREMENT_DROP_COUNT(iClosest, iBags)
//							ELSE
//								SET_ALL_EXCHANGE_DROPS_DONE()
//							ENDIF
//							IF GET_SELL_VEH_DROP_OFF_COUNT(iBags) = GET_SELL_VEH_TARGET_DROP_COUNT()
//								PRINTLN("[BIKER_SELL] SET_SELL_VEH_HAS_DELIVERED_CONTRABAND(", iBags, ")")
//								SET_SELL_VEH_HAS_DELIVERED_CONTRABAND(iBags)
//							ENDIF
//						ELSE
//							PRINTLN("[BIKER_SELL] SERVER_MAINTAIN_BAG_DROP_OFFS D, IS_THIS_A_HIDDEN_DROP_OFF  ", iClosest)
//						ENDIF
//					ELSE
//						PRINTLN("[BIKER_SELL] SERVER_MAINTAIN_BAG_DROP_OFFS E, iActiveDropOffBitset, iBags = ", iBags, " iClosest = ", iClosest)
//					ENDIF
//				ELSE
//					PRINTLN("[BIKER_SELL] SERVER_MAINTAIN_BAG_DROP_OFFS F  ", vDropCoords)
//				ENDIF
			ELSE
				PRINTLN("SERVER_MAINTAIN_BAG_DROP_OFFS G  ")
			ENDIF
		ELSE
			PRINTLN("SERVER_MAINTAIN_BAG_DROP_OFFS E  ")
		ENDIF
	//ENDREPEAT
ENDPROC
FUNC INT GET_BIKER_SELL_FRIENDS_IN_NEED_DROP_PEN()
	SWITCH serverBD.eShipmentType
		CASE eBIKER_SHIPMENT_TYPE_SMALL
			RETURN 1
		CASE eBIKER_SHIPMENT_TYPE_MEDIUM
			RETURN 2
		CASE eBIKER_SHIPMENT_TYPE_LARGE
			RETURN 3
		ENDSWITCH
	RETURN 0
ENDFUNC

FUNC INT GET_BAG_DROP_PENALTY(INT iBag)
	PRINTLN("[BIKER_SELL] - SERVER_CHECK_FOR_LOST_PRODUCT GET_BAG_DROP_PENALTY - iBag = ", iBag)
	INT iPenalty = 1
	IF BIKER_SELL_FRIENDS_IN_NEED()
		iPenalty = ((GET_MAX_NUMBER_OF_DROPS_FOR_CONTRA_AMOUNT() * GET_BIKER_SELL_FRIENDS_IN_NEED_DROP_PEN() )/GET_NUM_BAGS()) - serverBD.iVehDropCount[iBag] - serverBD.iBagPenalty[iBag]
		PRINTLN("[BIKER_SELL] - SERVER_CHECK_FOR_LOST_PRODUCT GET_BAG_DROP_PENALTY - ((GET_MAX_NUMBER_OF_DROPS_FOR_CONTRA_AMOUNT() * GET_BIKER_SELL_FRIENDS_IN_NEED_DROP_PEN() )/GET_NUM_BAGS()) - serverBD.iVehDropCount[", iBag, "] - serverBD.iBagPenalty[", iBag, "]")
		PRINTLN("[BIKER_SELL] - SERVER_CHECK_FOR_LOST_PRODUCT GET_BAG_DROP_PENALTY - ((", GET_MAX_NUMBER_OF_DROPS_FOR_CONTRA_AMOUNT(), " * ", GET_BIKER_SELL_FRIENDS_IN_NEED_DROP_PEN(), " )/", GET_NUM_BAGS(), ") - ", serverBD.iVehDropCount[iBag], " - ", serverBD.iBagPenalty[iBag], " iPenalty = ", iPenalty)
	ELIF BIKER_SELL_CLUB_RUN()
		iPenalty = ciNUMBER_OF_CLUB_RUN_DROPS - serverBD.iVehDropCount[iBag]
		PRINTLN("[BIKER_SELL] - SERVER_CHECK_FOR_LOST_PRODUCT GET_BAG_DROP_PENALTY - BIKER_SELL_CLUB_RUN iPenalty = ciNUMBER_OF_CLUB_RUN_DROPS - serverBD.iVehDropCount[", iBag, "] = ", iPenalty)
	ENDIF
	PRINTLN("[BIKER_SELL] - SERVER_CHECK_FOR_LOST_PRODUCT GET_BAG_DROP_PENALTY - RETURN iPenalty")
	RETURN iPenalty
ENDFUNC

PROC SERVER_CHECK_FOR_LOST_PRODUCT(PLAYER_INDEX playerID, INT iVeh)
	SWITCH GET_SELL_VAR()
		CASE eBIKER_SELL_POSTMAN
			IF bIS_PLAYER_POSTMAN(iVeh, NATIVE_TO_INT(playerID))
				IF HAS_PLAYER_DIED(playerID, serverBD.iDeathFlag)
					SERVER_INCREASE_DROP_PENALTY()
					SERVER_SET_POSTMAN(FALSE, playerID, iVeh)
					SERVER_CLEAR_ACTIVE_DROP(iVeh, serverBD.iClosestDropOff[iVeh])
					CLEAR_POSTMAN_VEH(iVeh)
					SERVER_INCREASE_VEHICLE_PENALTY(iVeh)
					PRINTLN("[BIKER_SELL] - SERVER_CHECK_FOR_LOST_PRODUCT - iVeh = ", iVeh, " CLEAR_BIT(serverBD.iActiveDropOffBitset for ", serverBD.iClosestDropOff[iVeh])
				ELIF NOT IS_SELL_VEH_OK(iVeh)
					// If the van is destroyed whilst player is posting, clear the on foot postman duties (vehicle penalties handled elsewhere)
					SERVER_SET_POSTMAN(FALSE, playerID, iVeh)
					SERVER_CLEAR_ACTIVE_DROP(iVeh, serverBD.iClosestDropOff[iVeh])
					CLEAR_POSTMAN_VEH(iVeh)
				ENDIF
			ENDIF				
		BREAK
	ENDSWITCH
	IF MODE_HAS_PICKUP_BAGS()
		PARTICIPANT_INDEX partId
		INT iBag = -1
		IF IS_NET_PLAYER_OK(playerID, FALSE)
		AND playerID <> INVALID_PLAYER_INDEX()
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerID)
				partId = NETWORK_GET_PARTICIPANT_INDEX(playerID)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(partId)
					IF IS_CLIENT_BIT0_SET(partId, eCLIENTBITSET0_BAG_CARRIER)
						IF HAS_PLAYER_DIED(playerID, serverBD.iDeathFlag)
						OR IS_CLIENT_BIT0_SET(partId, eCLIENTBITSET0_COMMITTED_SUICIDE)
							iBag = iGET_PLAYER_BAG(playerID)
							IF iBag <> -1
							AND NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(iBag)
								SERVER_INCREASE_DROP_PENALTY(GET_BAG_DROP_PENALTY(iBag))
								SERVER_CLEAR_ACTIVE_DROP(iVeh, serverBD.iClosestDropOff[iVeh])
								serverBD.iAssignedBag[iBag] = -1
								PRINTLN("[BIKER_SELL] [BAGS] - SERVER_CHECK_FOR_LOST_PRODUCT - iVeh = ", iVeh, " CLEAR_BIT(serverBD.iActiveDropOffBitset for ", serverBD.iClosestDropOff[iVeh])
								IF BIKER_SELL_CLUB_RUN()
									BIKER_SELL_CLUB_RUN_CHECK_ALL_DROPS_DONE(TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SERVER_MAINTAIN_DROPS()
	
	INT iClosest
	INT i, iBags
	
	IF MODE_HAS_PICKUP_BAGS()
		IF NOT HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()	
			
			REPEAT GET_NUM_BAGS() iBags
				PLAYER_INDEX piPlayer = INT_TO_PLAYERINDEX(serverBD.iAssignedBag[iBags])
//				PRINTLN("3029992, SERVER_MAINTAIN_DROPS for piPlayer  ", GET_PLAYER_NAME(piPlayer), " iBags = ", iBags)
				IF serverBD.iAssignedBag[iBags] != -1
					IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
						IF NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(iBags)

							SERVER_CHECK_FOR_LOST_PRODUCT(piPlayer, iBags)						
						
							PRINTLN("3029992, SERVER_MAINTAIN_DROPS, SERVER_MAINTAIN_BAG_DROP_OFFS, for piPlayer  ", GET_PLAYER_NAME(piPlayer), " iBags = ", iBags)
							SERVER_MAINTAIN_BAG_DROP_OFFS(iClosest, iBags)
							IF serverBD.iClosestDropOff[iBags] <> iClosest
								serverBD.iClosestDropOff[iBags] = iClosest
								PRINTLN("3029992, SERVER_MAINTAIN_DROPS, serverBD.iClosestDropOff[",iBags,"] = ", serverBD.iClosestDropOff[iBags])
							ENDIF
							iClosest = 0
						ELSE
							PRINTLN("3029992, SERVER_MAINTAIN_DROPS HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND, iBags = ", iBags)
						ENDIF
					ELSE
						PRINTLN("3029992, SERVER_MAINTAIN_DROPS NETWORK_IS_PLAYER_ACTIVE, iBags = ", iBags)
					ENDIF
				ELSE
					//PRINTLN("3029992, SERVER_MAINTAIN_DROPS iAssignedBag, iBags = ", iBags)
				ENDIF
			ENDREPEAT
		ENDIF
	ELIF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			IF DOES_ENTITY_EXIST(VEH_ID(i))
			AND NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(i)
				SERVER_MAINTAIN_DROP_OFFS( VEH_ID(i), i, iClosest)
				IF serverBD.iClosestDropOff[i] <> iClosest
					serverBD.iClosestDropOff[i] = iClosest
					PRINTLN("SERVER_MAINTAIN_DROPS, serverBD.iClosestDropOff [",i,"] = ", serverBD.iClosestDropOff[i])
				ENDIF
				iClosest = 0
			ENDIF
		ENDREPEAT
	ELSE
		IF DOES_ENTITY_EXIST(VEH_ID(0))
			SERVER_MAINTAIN_DROP_OFFS( VEH_ID(0), 0, iClosest)
			
			//PRINTLN("SERVER_MAINTAIN_DROPS, iClosest = ", iClosest)
			IF serverBD.iClosestDropOff[0] <> iClosest
				serverBD.iClosestDropOff[0] = iClosest
				PRINTLN("SERVER_MAINTAIN_DROPS, serverBD.iClosestDropOff[0] = ", serverBD.iClosestDropOff[0])
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR vAIRPORT_COORDS()
	IF NOT IS_LAUNCH_FACTORY_IN_CITY()
		RETURN <<2074.1130, 4772.4551, 40.2459>>
	ENDIF

	RETURN <<-1156.0415, -2740.3728, 12.9525>>
ENDFUNC

FUNC BOOL IS_PLAYER_IN_AIRPORT()
	// Dont bother doing any LSIA blip / objective text if the factory is up north or if we're on air drop at sea
	IF BIKER_SELL_AIR_DROP_AT_SEA()
		RETURN TRUE
	ENDIF

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		IF IS_LAUNCH_FACTORY_IN_CITY()
			IF IS_COORD_IN_SPECIFIED_AREA(vPlayerCoords, AC_AIRPORT_AIRSIDE, 1000)
			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-1256.4573,-2150.7021,12.9248>>, <<100.00, 100.00, 100.00>>)
			
				RETURN TRUE
			ENDIF
		ELSE
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vAIRPORT_COORDS(), <<100.00, 100.00, 100.00>>)
			
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				BLIPS															║

FUNC INT GET_CONTRABAND_BLIP_COLOUR(INT i)

	UNUSED_PARAMETER(i)

//	IF IS_SELL_VEH_OCCUPIED(i)
//		RETURN GET_BLIP_COLOUR_FROM_HUD_COLOUR(GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
//	ENDIF

	RETURN BLIP_COLOUR_BLUE
ENDFUNC

PROC COLOUR_CONTRA_BLIP()
	IF DOES_BLIP_EXIST(sLocaldata.blipVeh)
		SET_BLIP_COLOUR(sLocaldata.blipVeh, GET_CONTRABAND_BLIP_COLOUR())
	ENDIF
ENDPROC

PROC REMOVE_CONTRABAND_BLIP(INT i)
	IF DOES_BLIP_EXIST(sLocaldata.blipVeh[i])
		REMOVE_BLIP(sLocaldata.blipVeh[i])
		PRINTLN("2738261, REMOVE_CONTRABAND_BLIP, blip ", i)
	ENDIF
ENDPROC

PROC REMOVE_CONTRABAND_BLIPS()
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		REMOVE_CONTRABAND_BLIP(i)
	ENDREPEAT
ENDPROC

FUNC BOOL PLAYER_WANTED()
	RETURN IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_WANTED)
ENDFUNC

FUNC BOOL SHOULD_HIDE_DROP_OFF_BLIPS()
	IF NOT HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
		IF Is_This_The_Current_Objective_Text("BYCB_CLY_LSC0")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL OK_TO_DISPLAY_CONTRABAND_VEH_BLIP(INT iVeh)
	IF AM_I_IN_THIS_SELL_VEH(iVeh)
		RETURN FALSE
	ENDIF
	
	IF HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
		RETURN FALSE
	ENDIF
	
	IF HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(iVeh)
		RETURN FALSE
	ENDIF
	
	IF NOT BIKER_SELL_STING()
		IF SHOULD_HIDE_DROP_OFF_BLIPS()
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_SELL_VEH_OCCUPIED(iVeh)
		IF NOT IS_SHIPMENT_BLIP_FLASH_ACTIVE()
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF bIS_POSTMAN_PAT()
//	OR bCLOSEST_VEH_PARKED()
	
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_PLAYER_BLIP_ALPHA_FOR_CONTRABAND_DRIVER(INT iVeh, INT iAlpha)
	PLAYER_INDEX playerDriver
	INT iPlayer
	
	playerDriver = GET_DRIVER_OF_SELL_VEH(iVeh)
	iPlayer = NATIVE_TO_INT(playerDriver)
	IF playerDriver != INVALID_PLAYER_INDEX()
	AND iPlayer != -1
	AND DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayer])
		PRINTLN("[MAINTAIN_CONTRABAND_BLIPS] Forcing player blip alpha for player ", GET_PLAYER_NAME(playerDriver), " Player int = ", iPlayer) 
		SET_BLIP_ALPHA(g_PlayerBlipsData.playerBlips[iPlayer], iAlpha)
	ENDIF
ENDPROC

FUNC BLIP_SPRITE GET_SELL_VEH_BLIP_SPRITE()
	
	IF IS_THIS_AN_AIR_VARIATION()
		IF BIKER_SELL_HELICOPTER_DROP()
			RETURN RADAR_TRACE_HELICOPTER
		ENDIF
		
		RETURN RADAR_TRACE_PLAYER_PLANE
		
	ELIF IS_THIS_A_SEA_VARIATION()
		RETURN RADAR_TRACE_PLAYER_BOAT
		
	ENDIF
	
	RETURN GET_ILLICIT_GOOD_BLIP_SPRITE()
ENDFUNC

PROC MAINTAIN_CONTRABAND_BLIPS()
	INT i
	
	IF MODE_HAS_PICKUP_BAGS()	
	
		REPEAT NUM_NETWORK_PLAYERS i
		
			PLAYER_INDEX piPlayer = INT_TO_PLAYERINDEX(i)
			
			IF piPlayer != PLAYER_ID()
				IF IS_NET_PLAYER_OK(piPlayer, FALSE)
					IF NETWORK_IS_PLAYER_A_PARTICIPANT(piPlayer)
					AND DOES_PLAYER_HAVE_A_BAG(piPlayer)
						IF IS_SHIPMENT_BLIP_FLASH_ACTIVE()								
							SET_PLAYER_BAG_CARRIER_BLIP(piPlayer, TRUE)
						ELSE
							SET_PLAYER_BAG_CARRIER_BLIP(piPlayer, FALSE)
						ENDIF
					ELSE
						SET_PLAYER_BAG_CARRIER_BLIP(piPlayer, FALSE)
					ENDIF		
				ELSE
					SET_PLAYER_BAG_CARRIER_BLIP(piPlayer, FALSE)
				ENDIF
			ENDIF
		ENDREPEAT
		
	ELSE
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			IF IS_SELL_VEH_OK(i)
				IF NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(i)
				
					IF NOT DOES_BLIP_EXIST(sLocaldata.blipVeh[i])
						sLocaldata.blipVeh[i] = ADD_BLIP_FOR_ENTITY(VEH_ENT(i))
						SET_BLIP_SPRITE(sLocaldata.blipVeh[i], GET_SELL_VEH_BLIP_SPRITE())
						
						SET_BLIP_COLOUR(sLocaldata.blipVeh[i], GET_CONTRABAND_BLIP_COLOUR(i))
						SET_BLIP_PRIORITY(sLocaldata.blipVeh[i], BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
						SET_BLIP_NAME_FROM_TEXT_FILE(sLocaldata.blipVeh[i], GET_SHIPMENT_TYPE_STRING_BLIPS())
		//					SET_BLIP_ROUTE(sLocaldata.blipVeh[i], TRUE)
						PRINTLN("2738261, DRAW_CONTRABAND_BLIP")
						IF GET_SELL_VEH_BLIP_SPRITE() = RADAR_TRACE_PLAYER_BOAT
						OR GET_SELL_VEH_BLIP_SPRITE() = RADAR_TRACE_PLAYER_PLANE
							SET_BLIP_ROTATION(sLocaldata.blipVeh[i], ROUND(GET_ENTITY_HEADING_FROM_EULERS(VEH_ENT(i))))
						ENDIF
					ELSE
						IF OK_TO_DISPLAY_CONTRABAND_VEH_BLIP(i)
							SET_BLIP_ALPHA(sLocaldata.blipVeh[i], 255)
												
							IF GET_BLIP_COLOUR(sLocaldata.blipVeh[i]) != GET_CONTRABAND_BLIP_COLOUR(i)
								SET_BLIP_COLOUR(sLocaldata.blipVeh[i], GET_CONTRABAND_BLIP_COLOUR(i))
							ENDIF
							
							IF GET_SELL_VEH_BLIP_SPRITE() = RADAR_TRACE_PLAYER_BOAT
							OR GET_SELL_VEH_BLIP_SPRITE() = RADAR_TRACE_PLAYER_PLANE
								SET_BLIP_ROTATION(sLocaldata.blipVeh[i], ROUND(GET_ENTITY_HEADING_FROM_EULERS(VEH_ENT(i))))
							ENDIF
							
							IF NOT sLocaldata.blipVehOkLastFrame[i]
								SET_PLAYER_BLIP_ALPHA_FOR_CONTRABAND_DRIVER(i, 0)
							ENDIF
						ELSE
							//-- Now that the contraband flashes on/off, need to force the contraband vehicle driver's blip on (actually set its alpha)
							//-- because otherwise there will be a brief period when there's no player blip or contraband blip due to the fact that
							//-- the remote player blip update is a staggered update (see bug 2827903)
							
							IF sLocaldata.blipVehOkLastFrame[i]
								IF NOT IS_SHIPMENT_BLIP_FLASH_ACTIVE()
									IF NOT AM_I_IN_THIS_SELL_VEH(i)
										IF IS_SELL_VEH_OCCUPIED(i)
											SET_PLAYER_BLIP_ALPHA_FOR_CONTRABAND_DRIVER(i, 255)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							SET_BLIP_ALPHA(sLocaldata.blipVeh[i], 1)
							
							sLocaldata.blipVehOkLastFrame[i] = FALSE
						ENDIF
					ENDIF
				
					sLocaldata.blipVehOkLastFrame[i] = TRUE
				ELSE
					REMOVE_CONTRABAND_BLIP(i)
				ENDIF
			ELSE
				REMOVE_CONTRABAND_BLIP(i)
			ENDIF
		ENDREPEAT
	ENDIF
	
	//-- Only do GPS to closest Contra vehicle (and if I'm not in a contra vehicle).
	IF NOT AM_I_IN_ANY_SELL_VEH()
	AND NOT IS_THIS_AN_AIR_VARIATION()
//	AND NOT bIS_POSTMAN_PAT()
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		INT iClosest = GET_CLOSEST_SELL_VEH_TO_LOCAL_PLAYER(FALSE)
		IF iClosest > -1
			IF NOT ARE_ALL_SELL_VEHS_OCCUPIED()
				IF sLocaldata.iCLosestCOntraVehForGPS != iClosest
					IF sLocaldata.iCLosestCOntraVehForGPS >= 0
						IF DOES_BLIP_EXIST(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS])
							IF DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS])
								PRINTLN("[MAINTAIN_CONTRABAND_BLIPS] REMOVED GPS FOR CONTRA VEH ", sLocaldata.iCLosestCOntraVehForGPS, " as no longer closest"  )
								SET_BLIP_ROUTE(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS], FALSE)
							ENDIF
						ENDIF
					ENDIF
					IF DOES_BLIP_EXIST(sLocaldata.blipVeh[iClosest])
						IF NOT DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipVeh[iClosest])
							PRINTLN("[MAINTAIN_CONTRABAND_BLIPS] ADDED GPS FOR CONTRA VEH ", iClosest)
							SET_BLIP_ROUTE(sLocaldata.blipVeh[iClosest], TRUE)
						ENDIF
					ENDIF
					
					sLocaldata.iCLosestCOntraVehForGPS = iClosest
					PRINTLN("[MAINTAIN_CONTRABAND_BLIPS] sLocaldata.iCLosestCOntraVehForGPS = ", sLocaldata.iCLosestCOntraVehForGPS) 
				ENDIF
			ELSE
				//-- Only draw a GPS to unoccupied contraband vehicles
				IF sLocaldata.iCLosestCOntraVehForGPS > -1
					IF DOES_BLIP_EXIST(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS])
						IF DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS])
							PRINTLN("[MAINTAIN_CONTRABAND_BLIPS] REMOVED GPS FOR CONTRA VEH ", sLocaldata.iCLosestCOntraVehForGPS, " as I'm not in a contra vehicle, but all are occupied")
							SET_BLIP_ROUTE(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS], FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF sLocaldata.iCLosestCOntraVehForGPS > -1
				IF DOES_BLIP_EXIST(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS])
					IF DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS])
						PRINTLN("[MAINTAIN_CONTRABAND_BLIPS] REMOVED GPS FOR CONTRA VEH ", sLocaldata.iCLosestCOntraVehForGPS, " as iClosest <= -1")
						SET_BLIP_ROUTE(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS], FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF sLocaldata.iCLosestCOntraVehForGPS > -1
			IF DOES_BLIP_EXIST(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS])
				IF DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS])
					PRINTLN("[MAINTAIN_CONTRABAND_BLIPS] REMOVED GPS FOR CONTRA VEH ", sLocaldata.iCLosestCOntraVehForGPS, " as I'm in a contra vehicle")
					SET_BLIP_ROUTE(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS], FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BLIP_SPRITE GET_BLIP_SPRITE_FOR_VEHICLE(VEHICLE_INDEX veh, BOOL bAttacker = FALSE)
	
	IF bAttacker
		RETURN RADAR_TRACE_AI
	ENDIF
	
	MODEL_NAMES model = GET_ENTITY_MODEL(veh)

	IF IS_THIS_MODEL_A_BIKE(model)
		RETURN RADAR_TRACE_GANG_BIKERS
	ENDIF
	
	IF IS_THIS_MODEL_A_HELI(model)
		IF IS_THIS_VEH_A_SELL_VEH(veh)
			RETURN RADAR_TRACE_HELICOPTER
		ELSE
			RETURN RADAR_TRACE_ENEMY_HELI_SPIN
		ENDIF
	ENDIF
	
	IF IS_THIS_MODEL_A_PLANE(model)
		RETURN RADAR_TRACE_PLAYER_PLANE
	ENDIF
	
	RETURN RADAR_TRACE_AI
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_BLIP_HAVE_FIXED_ROTATION(VEHICLE_INDEX veh)
	IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(veh))
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(veh))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PED_BE_BLIPPED(INT iPed)
	IF NOT IS_ENTITY_ALIVE(GET_AI_PED_PED_INDEX(iPed))
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

//FUNC VECTOR vGET_LOCAL_BAG_DROP_COORDS()
//
//	VECTOR vBag
//	INT iBag = iGET_PLAYER_BAG(PLAYER_ID())
//	
//	IF iBag > -1	
//		IF NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(iBag)
//			IF NOT IS_THIS_A_HIDDEN_DROP_OFF(serverBD.iClosestDropOff[iBag])
//			
//				vBag = GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[iBag], serverBD.iRoute, iBag)
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	RETURN vBag
//ENDFUNC

PROC MAINTAIN_PED_BLIPS()
	IF DOES_VARIATION_HAVE_AI_PEDS()
		IF HAVE_PEDS_BEEN_SPOOKED()
		OR NOT DOES_VARIATION_REQUIRE_PED_SPOOKING()
			BOOL bSting = BIKER_SELL_STING()
			INT i
			REPEAT GET_NUM_PEDS_FOR_VARIATION() i
				IF DOES_AI_PED_EXIST(i)
					IF SHOULD_PED_BE_BLIPPED_AS_ENEMY()
						IF SHOULD_PED_BE_BLIPPED(i)
						AND (NOT bSting OR i != 0)
							IF IS_PED_IN_ANY_VEHICLE(GET_AI_PED_PED_INDEX(i))
								VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_USING(GET_AI_PED_PED_INDEX(i))
								IF NOT IS_VEHICLE_A_NON_CONTRABAND_VEHICLE(veh)
									IF NOT DOES_BLIP_EXIST(sLocaldata.blipPed[i])
									OR (DOES_BLIP_EXIST(sLocaldata.blipPed[i]) AND (GET_BLIP_SPRITE(sLocaldata.blipPed[i]) != GET_BLIP_SPRITE_FOR_VEHICLE(veh)))
										IF NOT DOES_BLIP_EXIST(sLocaldata.blipPed[i])
											sLocaldata.blipPed[i] = ADD_BLIP_FOR_ENTITY(GET_AI_PED_PED_INDEX(i))
										ENDIF
										SET_BLIP_SPRITE(sLocaldata.blipPed[i], GET_BLIP_SPRITE_FOR_VEHICLE(veh))
										SET_BLIP_COLOUR_FROM_HUD_COLOUR(sLocaldata.blipPed[i], HUD_COLOUR_RED)
										PRINTLN("[BIKER_SELL] [MAINTAIN_PED_BLIPS] - Added Blip for ped ", i)
									ENDIF
								ELSE
									// Remove blips if the ped is in a contraband vehicle
									IF DOES_BLIP_EXIST(sLocaldata.blipPed[i])
										REMOVE_BLIP(sLocaldata.blipPed[i])
										PRINTLN("[BIKER_SELL] [MAINTAIN_PED_BLIPS] - Removed Blip for ped ", i, " - In a contraband vehicle")
									ENDIF
								ENDIF
							ELSE
								IF NOT DOES_BLIP_EXIST(sLocaldata.blipPed[i])
								OR (DOES_BLIP_EXIST(sLocaldata.blipPed[i]) AND (GET_BLIP_SPRITE(sLocaldata.blipPed[i]) != RADAR_TRACE_AI))

									IF NOT DOES_BLIP_EXIST(sLocaldata.blipPed[i])
										PRINTLN("[BIKER_SELL] [MAINTAIN_PED_BLIPS] - Added Blip for ped in contra vehicle, ped ", i)
										sLocaldata.blipPed[i] = ADD_BLIP_FOR_ENTITY(GET_AI_PED_PED_INDEX(i))
									ENDIF
									SET_BLIP_SPRITE(sLocaldata.blipPed[i], RADAR_TRACE_AI)
									SET_BLIP_COLOUR_FROM_HUD_COLOUR(sLocaldata.blipPed[i], HUD_COLOUR_RED)
									SET_BLIP_SCALE(sLocaldata.blipPed[i], BLIP_SIZE_NETWORK_PED)
								ENDIF
							ENDIF
						ELSE
							IF DOES_BLIP_EXIST(sLocaldata.blipPed[i])
								PRINTLN("[BIKER_SELL] [MAINTAIN_PED_BLIPS] - Removed Blip for ped ", i, " - Injured")
								REMOVE_BLIP(sLocaldata.blipPed[i])
							ENDIF
						ENDIF
					ELIF SHOULD_PED_BE_BLIPPED_AS_FRIEND()
						IF SHOULD_PED_BE_BLIPPED(i)
						AND NOT IS_BIT_SET(serverBD.iPedDeliveredBitSet, i)
//						AND ( IS_BIT_SET(serverBD.iPedTasked, i) OR NOT BIKER_SELL_TACO_VAN() )
							IF NOT DOES_BLIP_EXIST(sLocaldata.blipPed[i])
								IF NOT DOES_BLIP_EXIST(sLocaldata.blipPed[i])
									sLocaldata.blipPed[i] = ADD_BLIP_FOR_ENTITY(GET_AI_PED_PED_INDEX(i))
									PRINTLN("[BIKER_SELL] [MAINTAIN_PED_BLIPS] - SHOULD_PED_BE_BLIPPED_AS_FRIEND, Added Blip for ped in contra vehicle, ped ", i)
								ENDIF
								IF BIKER_SELL_FRIENDS_IN_NEED()
									SET_BLIP_SPRITE(sLocaldata.blipPed[i], RADAR_TRACE_VIP)
									SET_BLIP_SCALE(sLocaldata.blipPed[i], 1.0)
								ELSE
									SET_BLIP_SCALE(sLocaldata.blipPed[i], BLIP_SIZE_PED)
								ENDIF
								SET_BLIP_COLOUR_FROM_HUD_COLOUR(sLocaldata.blipPed[i], HUD_COLOUR_BLUE)
								SET_BLIP_NAME_FROM_TEXT_FILE(sLocaldata.blipPed[i], "SBIKER_PDBLIP")
							ENDIF
						ELSE
							IF DOES_BLIP_EXIST(sLocaldata.blipPed[i])
								PRINTLN("[BIKER_SELL] [MAINTAIN_PED_BLIPS] - SHOULD_PED_BE_BLIPPED_AS_FRIEND, Removed Blip for ped ", i, " - Injured")
								REMOVE_BLIP(sLocaldata.blipPed[i])
							ENDIF
						ENDIF			
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(sLocaldata.blipPed[i])
						PRINTLN("[BIKER_SELL] [MAINTAIN_PED_BLIPS] - Removed Blip for ped ", i, " - Ped ni doesn't exist")
						REMOVE_BLIP(sLocaldata.blipPed[i])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF	
ENDPROC

PROC REMOVE_PED_BLIPS()
	INT i
	REPEAT ciMAX_CONTRABAND_PEDS i
		IF DOES_BLIP_EXIST(sLocaldata.blipPed[i])
			REMOVE_BLIP(sLocaldata.blipPed[i])
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_NON_CONTRABAND_VEHICLE_BLIPS()
	IF DOES_VARIATION_HAVE_AI_VEHICLES()
		INT i
		REPEAT ciMAX_CONTRABAND_PED_VEHICLES i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[i].netIndex)
				VEHICLE_INDEX veh = NET_TO_VEH(serverBD.sPedVeh[i].netIndex)
				IF NOT IS_ENTITY_DEAD(veh)
					IF IS_NON_CONTRABAND_VEHICLE_OCCUPIED(i)
						IF NOT DOES_BLIP_EXIST(sLocaldata.blipPedVeh[i])
							sLocaldata.blipPedVeh[i] = ADD_BLIP_FOR_ENTITY(veh)
							SET_BLIP_SPRITE(sLocaldata.blipPedVeh[i], GET_BLIP_SPRITE_FOR_VEHICLE(veh, TRUE))
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(sLocaldata.blipPedVeh[i], HUD_COLOUR_RED)
							PRINTLN("[BIKER_SELL] [MAINTAIN_NON_CONTRABAND_VEHICLE_BLIPS] - Add blip for veh ", i)
							#IF IS_DEBUG_BUILD
							IF IS_LOCAL_BIT1_SET(eLOCALBITSET1_ADDED_TWIST_BLIPS)
								PRINTLN("[BIKER_SELL] SET_LOCAL_BIT1(eLOCALBITSET1_ADDED_TWIST_BLIPS)")
							ENDIF
							#ENDIF
							SET_LOCAL_BIT1(eLOCALBITSET1_ADDED_TWIST_BLIPS)
						ELSE
							IF GET_BLIP_SPRITE(sLocaldata.blipPedVeh[i]) != GET_BLIP_SPRITE_FOR_VEHICLE(veh, TRUE)
								SET_BLIP_SPRITE(sLocaldata.blipPedVeh[i], GET_BLIP_SPRITE_FOR_VEHICLE(veh, TRUE))
							ENDIF
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(sLocaldata.blipPedVeh[i])
							REMOVE_BLIP(sLocaldata.blipPedVeh[i])
							PRINTLN("[BIKER_SELL] [MAINTAIN_NON_CONTRABAND_VEHICLE_BLIPS] - Removed Blip for veh ", i, " - nobody in veh")
						ENDIF
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(sLocaldata.blipPedVeh[i])
						REMOVE_BLIP(sLocaldata.blipPedVeh[i])
						PRINTLN("[BIKER_SELL] [MAINTAIN_NON_CONTRABAND_VEHICLE_BLIPS] - Removed Blip for veh ", i, " - veh is dead")
					ENDIF
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(sLocaldata.blipPedVeh[i])
					REMOVE_BLIP(sLocaldata.blipPedVeh[i])
					PRINTLN("[BIKER_SELL] [MAINTAIN_NON_CONTRABAND_VEHICLE_BLIPS] - Removed Blip for veh ", i, " - veh ni doesn't exist")
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC REMOVE_VEHICLE_BLIPS()
	INT i
	REPEAT ciMAX_CONTRABAND_PED_VEHICLES i
		IF DOES_BLIP_EXIST(sLocaldata.blipPedVeh[i])
			REMOVE_BLIP(sLocaldata.blipPedVeh[i])
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_SELL_VEHICLE_THAT_HAS_NOT_COMPLETED_DROP(INT iDrop)

	INT iModeTarget, i
	IF MODE_HAS_PICKUP_BAGS()
		iModeTarget = GET_NUM_BAGS()
	ELSE
		iModeTarget = GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE()
	ENDIF
	
	REPEAT iModeTarget i
		IF IS_BIT_SET(serverBD.iActiveDropOffBitset[i], iDrop)
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

FUNC BOOL SHOULD_DROP_OFF_BE_BLIPPED_FOR_LOCAL_PLAYER(INT iDropOff, INT iVeh)

	INT i
	
	IF BIKER_SELL_FRIENDS_IN_NEED()
		RETURN FALSE
	ENDIF
	
	IF iVeh >= 0 
		IF BIKER_SELL_BORDER_PATROL()
			IF serverBD.iClosestDropOff[iVeh] <= (iDROP_TARGET() - 1)
				IF iDropOff = serverBD.iClosestDropOff[iVeh]
				OR iDropOff = serverBD.iClosestDropOff[iVeh] + 1
					RETURN IS_BIT_SET(serverBD.iActiveDropOffBitset[iVeh], iDropOff)
				ENDIF
			ENDIF
		ELSE
			RETURN IS_BIT_SET(serverBD.iActiveDropOffBitset[iVeh], iDropOff)
		ENDIF
	ELSE
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			IF IS_BIT_SET(serverBD.iActiveDropOffBitset[i], iDropOff)
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


FUNC FLOAT GET_AREA_BLIP_RADIUS()
	IF IS_THIS_AN_AIR_VARIATION()
		IF BIKER_SELL_HELICOPTER_DROP()
			RETURN (150.0/4)
		ENDIF
	
		RETURN (150.0/4)
	ENDIF

	RETURN 25.0
ENDFUNC

FUNC BOOL SHOULD_DO_KILL_ENEMIES_HELP()
	RETURN FALSE
ENDFUNC

PROC REMOVE_AIRPORT_BLIP()
	IF DOES_BLIP_EXIST(sLocaldata.blipLSIA)
		REMOVE_BLIP(sLocaldata.blipLSIA)
	ENDIF
ENDPROC

PROC TOGGLE_LSIA_AIRPORT_BLIP(BOOL bOn)
	IF bOn
	
//		INT iDropOffBlipR, iDropOffBlipG, iDropOffBlipB, iDropOffBlipA
//		GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iDropOffBlipR, iDropOffBlipG, iDropOffBlipB, iDropOffBlipA)
	
		IF DOES_BLIP_EXIST(sLocaldata.blipLSIA)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF NOT DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipLSIA)
					SET_BLIP_ROUTE(sLocaldata.blipLSIA, TRUE)
				ENDIF
			ELSE
				IF DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipLSIA)
					SET_BLIP_ROUTE(sLocaldata.blipLSIA, FALSE)
				ENDIF
			ENDIF
//			DRAW_MARKER(MARKER_CYLINDER, vAIRPORT_COORDS(), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<ciDELIVERY_DISTANCE, ciDELIVERY_DISTANCE, 0.5>>, 
//						iDropOffBlipR, iDropOffBlipG, iDropOffBlipB, iDropOffBlipA)
		ELSE
			sLocaldata.blipLSIA = ADD_BLIP_FOR_COORD(vAIRPORT_COORDS())
		ENDIF	
	ELSE
		REMOVE_AIRPORT_BLIP()
	ENDIF
ENDPROC

PROC MAINTAIN_AIRPORT_BLIP()
	IF IS_THIS_AN_AIR_VARIATION()
		IF IS_PLAYER_IN_AIRPORT()
		OR IS_PLAYER_IN_ANY_SELL_VEH()
		OR ARE_ALL_SELL_VEHS_OCCUPIED()
			TOGGLE_LSIA_AIRPORT_BLIP(FALSE)
		ELSE
			TOGGLE_LSIA_AIRPORT_BLIP(TRUE)
		ENDIF
	ENDIF
ENDPROC

INT iLocalSmallBlipBitset

PROC REMOVE_LAST_FOOT_BLIP()
	IF NOT bIS_POSTMAN_PAT()
//	AND NOT bCLOSEST_VEH_PARKED()
		IF DOES_BLIP_EXIST(sLocaldata.blipDropOnFoot[sLocaldata.iStoredFootBlip])
			PRINTLN("[BIKER_SELL] [BLIP] [POST] REMOVE_LAST_FOOT_BLIP, ", sLocaldata.iStoredFootBlip)
			REMOVE_BLIP(sLocaldata.blipDropOnFoot[sLocaldata.iStoredFootBlip])
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_DROP_BLIPS(INT iVeh = -1)

	INT i
	VECTOR vCoords
	
	BOOL bRemoveBlips = (SHOULD_HIDE_DROP_OFF_BLIPS() OR SHOULD_DO_KILL_ENEMIES_HELP())
	BOOL bRemoveFootBlips
	
	IF BIKER_SELL_HAS_TWIST_HAPPENED()
	AND NOT BIKER_SELL_CLUB_RUN()
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			IF DOES_BLIP_EXIST(sLocaldata.blipLSIA)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF NOT DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipLSIA)
						SET_BLIP_ROUTE(sLocaldata.blipLSIA, TRUE)
					ENDIF
				ELSE
					IF DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipLSIA)
						SET_BLIP_ROUTE(sLocaldata.blipLSIA, FALSE)
					ENDIF
				ENDIF
			ELSE
				IF BIKER_SELL_STING()
					sLocaldata.blipLSIA = ADD_BLIP_FOR_COORD(BIKER_GET_STING_BACKUP_COORDS())
					SET_BLIP_NAME_FROM_TEXT_FILE(sLocaldata.blipLSIA, "SB_SELL_BMRBB")
//				ELIF BIKER_SELL_EXCHANGE()
//					sLocaldata.blipLSIA = ADD_BLIP_FOR_COORD(BIKER_GET_EXCHANGE_BACKUP_COORDS())
//					SET_BLIP_NAME_FROM_TEXT_FILE(sLocaldata.blipLSIA, "SB_SELL_BMRBB")
				ELSE
					sLocaldata.blipLSIA = ADD_BLIP_FOR_COORD(BIKER_GET_EXCHANGE_BACKUP_COORDS())
				ENDIF	
			ENDIF	
		ELSE
			REMOVE_AIRPORT_BLIP()
		ENDIF
		bRemoveBlips = TRUE
	ENDIF
	
	BOOL BinAnySell = IS_PLAYER_IN_ANY_SELL_VEH()
	
	INT iVehicleIn = -1
	IF BinAnySell
		iVehicleIn = GET_SELL_VEH_I_AM_IN_INT()
	ELSE
		//-- If I'm not in a contra veh, but they're all occupied, draw a gps route to the closest drop-off to the closest contra veh (2814859)
		IF ARE_ALL_SELL_VEHS_OCCUPIED()
			iVehicleIn = GET_CLOSEST_SELL_VEH_TO_LOCAL_PLAYER(DEFAULT, TRUE)
		ENDIF
		
		IF MODE_HAS_PICKUP_BAGS()
			IF DO_I_HAVE_A_BAG()
				iVehicleIn = iGET_PLAYER_BAG(PLAYER_ID())
			ELSE
				iVehicleIn = sLocaldata.iStoredBag
			ENDIF
				
			IF iVehicleIn = -1
				iVehicleIn = 0
			ENDIF
		ENDIF
	ENDIF
	
	IF BIKER_SELL_BENSON()
		IF HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(iVehicleIn)
			bRemoveBlips = TRUE
			PRINTLN("[BIKER_SELL] [BLIP] MAINTAIN_DROP_BLIPS - HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND = TRUE")
		ELSE
			IF NOT BinAnySell
				IF NOT ARE_ALL_SELL_VEHS_OCCUPIED()
					bRemoveBlips = TRUE
					PRINTLN("[BIKER_SELL] [BLIP] MAINTAIN_DROP_BLIPS - bRemoveBlips = TRUE")
				ELSE
					PRINTLN("[BIKER_SELL] [BLIP] MAINTAIN_DROP_BLIPS - all sells occupied")
				ENDIF
			ELSE
				PRINTLN("[BIKER_SELL] [BLIP] MAINTAIN_DROP_BLIPS - in any sell = TRUE")
			ENDIF
		ENDIF
	ENDIF
	
	INT iClosest
	
	IF bIS_POSTMAN_PAT()
//	OR bCLOSEST_VEH_PARKED()

		IF iVehicleIn = -1
			iVehicleIn = GET_CLOSEST_SELL_VEH_TO_LOCAL_PLAYER(TRUE)
		ELSE
			iClosest = serverBD.iClosestDropOff[iVehicleIn]
		ENDIF

		IF iClosest <> -1
		
			IF SHOULD_DROP_OFF_BE_BLIPPED_FOR_LOCAL_PLAYER(iClosest, iVeh)
//			OR bCLOSEST_VEH_PARKED()
				IF NOT DOES_BLIP_EXIST(sLocaldata.blipDropOnFoot[iClosest])
					IF iVeh = -1
						iVeh = GET_SELL_VEHICLE_THAT_HAS_NOT_COMPLETED_DROP(iClosest)
					ENDIF

					vCoords = GET_SELL_DROP_COORDS(GET_SELL_VAR(), iClosest, serverBD.iRoute, iVeh, DEFAULT, TRUE)
					
					PRINTLN("[BIKER_SELL] [BLIP] [POST] MAINTAIN_DROP_BLIPS, [FOOT] ", iClosest, " vCoords = ", vCoords, " serverBD.iRoute = ", serverBD.iRoute, " iVeh = ", iVeh, 
							" ", BIKER_SELL_GET_VARIATION_DEBUG_PRINT(GET_SELL_VAR()))
							
					sLocaldata.blipDropOnFoot[iClosest] = ADD_BLIP_FOR_COORD(vCoords)
					
					SET_BLIP_NAME_FROM_TEXT_FILE(sLocaldata.blipDropOnFoot[iClosest], "SB_SELL_BMRBB")
					
					sLocaldata.iStoredFootBlip = iClosest
					PRINTLN("[BIKER_SELL] [BLIP] MAINTAIN_DROP_BLIPS, bIS_POSTMAN_PAT sLocaldata.iStoredFootBlip = ", sLocaldata.iStoredFootBlip)
				ENDIF
			ENDIF
		ENDIF
		
		bRemoveBlips = TRUE
	ELSE
		bRemoveFootBlips = TRUE
	ENDIF
	
//	INT iBag
	
	IF BinAnySell
//	OR BIKER_SELL_FRIENDS_IN_NEED()
		INT iNextDrop
		IF iVehicleIn <> -1
			iClosest = serverBD.iClosestDropOff[iVehicleIn]
			iNextDrop = serverBD.iClosestDropOff[iVehicleIn] + 1
//		ELIF iGET_PLAYER_BAG(PLAYER_PED_ID()) <> -1
//			iBag = iGET_PLAYER_BAG(PLAYER_PED_ID()) 
//			iClosest = serverBD.iClosestDropOff[iBag]
//			iNextDrop = serverBD.iClosestDropOff[iBag] + 1
		ENDIF
		IF iClosest <> -1
			IF DOES_BLIP_EXIST(sLocaldata.blipDrop[iClosest])
					
				// Ugly fix for url:bugstar:2814136
				vCoords = GET_SELL_DROP_COORDS(GET_SELL_VAR(), iClosest, serverBD.iRoute, iVehicleIn, DEFAULT, bIS_POSTMAN_PAT())
				IF NOT ARE_VECTORS_EQUAL(vCoords, <<-1143.7770, -2223.1814, 12.1958>>)
					IF NOT DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipDrop[iClosest])
						SET_BLIP_ROUTE(sLocaldata.blipDrop[iClosest], TRUE)
						PRINTLN("[BIKER_SELL] [GPS] MAINTAIN_DROP_BLIPS, A SET_BLIP_ROUTE ", iClosest)
					ENDIF	
				ELSE
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vCoords, FALSE) >= 100
						IF NOT DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipDrop[iClosest])
							SET_BLIP_ROUTE(sLocaldata.blipDrop[iClosest], TRUE)
							PRINTLN("[BIKER_SELL] [GPS] MAINTAIN_DROP_BLIPS, B SET_BLIP_ROUTE ", iClosest)
						ENDIF	
					ELSE
						IF DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipDrop[iClosest])
							SET_BLIP_ROUTE(sLocaldata.blipDrop[iClosest], FALSE)
							PRINTLN("[BIKER_SELL] [GPS] MAINTAIN_DROP_BLIPS, C SET_BLIP_ROUTE ", iClosest)
						ENDIF
					ENDIF
				ENDIF
				
				IF BIKER_SELL_BORDER_PATROL()
					IF IS_BIT_SET(iLocalSmallBlipBitset, iClosest)
						PRINTLN("[BLIP TESTING] - iClosest = ", iClosest)
						PRINTLN("[BLIP TESTING] - restoring blip scale as ", iClosest, " is now the closest")
						SET_BLIP_SCALE(sLocaldata.blipDrop[iClosest], 1.0)
						CLEAR_BIT(iLocalSmallBlipBitset, iClosest)
					ENDIF
				
					IF iNextDrop <= (iDROP_TARGET() - 1)
						IF DOES_BLIP_EXIST(sLocaldata.blipDrop[iNextDrop])
							IF NOT IS_BIT_SET(iLocalSmallBlipBitset, iNextDrop)
								PRINTLN("[BLIP TESTING] - iNextDrop = ", iNextDrop)
								PRINTLN("[BLIP TESTING] - setting blip scale and colour of blip = ", iNextDrop)
								SET_BLIP_SCALE(sLocaldata.blipDrop[iNextDrop], 0.5)
								SET_BLIP_COLOUR_FROM_HUD_COLOUR(sLocaldata.blipDrop[iNextDrop], HUD_COLOUR_YELLOW)
								SET_BIT(iLocalSmallBlipBitset, iNextDrop)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
	
	IF iVeh = -1
		IF MODE_HAS_PICKUP_BAGS()
			IF DO_I_HAVE_A_BAG()
				iVeh = iGET_PLAYER_BAG(PLAYER_ID())
			ELSE
				iVeh = sLocaldata.iStoredBag
			ENDIF
				
			IF iVeh = -1
				iVeh = 0
			ENDIF
		ENDIF
	ENDIF
	
	BOOL bDoClosestBlipGPS
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	FLOAT fDistMin = 999999.9
	FLOAT fDistTemp
	INT iClosestNoBag = -1
	
	REPEAT GET_MAX_SELL_DROP_OFFS() i
	
		IF MODE_HAS_PICKUP_BAGS()
			IF NOT DO_I_HAVE_A_BAG()
			
				iVeh = GET_SELL_VEHICLE_THAT_HAS_NOT_COMPLETED_DROP(i)
				bDoClosestBlipGPS = TRUE
			ENDIF
		ENDIF
	
		IF NOT SHOULD_DROP_OFF_BE_BLIPPED_FOR_LOCAL_PLAYER(i, iVeh)
		OR bRemoveBlips
		OR IS_THIS_A_HIDDEN_DROP_OFF(i)
			IF DOES_BLIP_EXIST(sLocaldata.blipDrop[i])
				
				#IF IS_DEBUG_BUILD
				IF iVeh != -1
					IF NOT SHOULD_DROP_OFF_BE_BLIPPED_FOR_LOCAL_PLAYER(i, iVeh)
						PRINTLN("[BIKER_SELL] [BLIP] - Removing blip because: NOT SHOULD_DROP_OFF_BE_BLIPPED_FOR_LOCAL_PLAYER(", i, ", ", iVeh, ")")
					ENDIF
					IF bRemoveBlips
						PRINTLN("[BIKER_SELL] [BLIP] - Removing blip because: bRemoveBlips")
					ENDIF
					IF NOT IS_BIT_SET(serverBD.iActiveDropOffBitset[iVeh], i)
						PRINTLN("[BIKER_SELL] [BLIP] - IS_BIT_SET(serverBD.iActiveDropOffBitset[", iVeh, "], ", i, ")")
					ENDIF
					IF IS_THIS_A_HIDDEN_DROP_OFF(i)
						PRINTLN("[BIKER_SELL] [BLIP] - Removing blip because: IS_THIS_A_HIDDEN_DROP_OFF(i)")
					ENDIF
				ENDIF
				#ENDIF
				
				PRINTLN("[BLIP] MAINTAIN_DROP_BLIPS, B REMOVE_DROP_BLIP ", i, " - iVeh = ", iVeh)
				REMOVE_BLIP(sLocaldata.blipDrop[i])
			ENDIF
			IF DOES_BLIP_EXIST(sLocaldata.blipDropArea[i])
				#IF IS_DEBUG_BUILD
				IF iVeh != -1
					IF NOT SHOULD_DROP_OFF_BE_BLIPPED_FOR_LOCAL_PLAYER(i, iVeh)
						PRINTLN("[BIKER_SELL] [BLIP] - Removing area blip because: NOT SHOULD_DROP_OFF_BE_BLIPPED_FOR_LOCAL_PLAYER(i, iVeh)")
					ENDIF
					IF bRemoveBlips
						PRINTLN("[BIKER_SELL] [BLIP] - Removing area blip because: bRemoveBlips")
					ENDIF
					
					IF IS_THIS_A_HIDDEN_DROP_OFF(i)
						PRINTLN("[BIKER_SELL] [BLIP] - Removing area blip because: IS_THIS_A_HIDDEN_DROP_OFF(i)")
					ENDIF
				ENDIF
				#ENDIF
				
				PRINTLN("[BIKER_SELL] [BLIP] MAINTAIN_DROP_BLIPS, B REMOVE_blipDropArea", i)
				REMOVE_BLIP(sLocaldata.blipDropArea[i])
			ENDIF
			IF bRemoveFootBlips
				IF DOES_BLIP_EXIST(sLocaldata.blipDropOnFoot[i])
					PRINTLN("[BIKER_SELL] [BLIP] [FOOT] MAINTAIN_DROP_BLIPS, B REMOVE_blipDropOnFoot", i)
					REMOVE_BLIP(sLocaldata.blipDropOnFoot[i])
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(sLocaldata.blipDropOnFoot[i])
				PRINTLN("[BIKER_SELL] [BLIP] [FOOT] REMOVE_FOOT_DROP_BLIPS C  ")
				REMOVE_BLIP(sLocaldata.blipDropOnFoot[i])
			ENDIF
			IF NOT DOES_BLIP_EXIST(sLocaldata.blipDrop[i])
				IF iVeh = -1
					iVeh = GET_SELL_VEHICLE_THAT_HAS_NOT_COMPLETED_DROP(i)
					PRINTLN("[BLIP] MAINTAIN_DROP_BLIPS iVeh  ", iVeh)
				ENDIF
				IF BIKER_SELL_BENSON()
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffVeh[iVeh])
					AND DOES_ENTITY_EXIST(NET_TO_VEH(serverBD.niDropOffVeh[iVeh]))
					AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.niDropOffVeh[iVeh]))
//					AND NOT DOES_BLIP_EXIST(GET_BLIP_FROM_ENTITY(NET_TO_VEH(serverBD.niDropOffVeh[iVeh])))
						sLocaldata.blipDrop[i] = ADD_BLIP_FOR_COORD(GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niDropOffVeh[iVeh])))  // ADD_BLIP_FOR_ENTITY(NET_TO_ENT(serverBD.niDropOffVeh[iVeh]))
						SET_BLIP_COLOUR(sLocaldata.blipDrop[i], BLIP_COLOUR_YELLOW)
						SET_BLIP_NAME_FROM_TEXT_FILE(sLocaldata.blipDrop[i], "SB_SELL_BMRBB")
						PRINTLN("[BIKER_SELL] [GPS] MAINTAIN_DROP_BLIPS, ADD_DROP_BLIP ", i)
						SET_BLIP_ROUTE(sLocaldata.blipDrop[i], TRUE)
					ENDIF
				ELSE
					vCoords = GET_SELL_DROP_COORDS(GET_SELL_VAR(), i, serverBD.iRoute, iVeh, DEFAULT, bIS_POSTMAN_PAT())
					PRINTLN("[BLIP] MAINTAIN_DROP_BLIPS, [ADD_DROP_BLIP] ", i, " vCoords = ", vCoords, " serverBD.iRoute = ", serverBD.iRoute, " iVeh = ", iVeh, 
							" ", BIKER_SELL_GET_VARIATION_DEBUG_PRINT(GET_SELL_VAR()))
					sLocaldata.blipDrop[i] = ADD_BLIP_FOR_COORD(vCoords)

					SET_BLIP_NAME_FROM_TEXT_FILE(sLocaldata.blipDrop[i], "SB_SELL_BMRBB")
					
					// GPS to closest for players not in the vehicle
					iVehicleIn = GET_CLOSEST_SELL_VEH_TO_LOCAL_PLAYER(TRUE, TRUE)
					
					IF bDoClosestBlipGPS
						fDistTemp = VDIST2(vPlayerPos, vCoords)
						IF fDistTemp < fDistMin
							fDistMin = fDistTemp
							iClosestNoBag = i
						ENDIF
					ENDIF
					
					IF MODE_HAS_PICKUP_BAGS()
					AND DO_I_HAVE_A_BAG()
						iVehicleIn = iGET_PLAYER_BAG(PLAYER_ID())
					ENDIF
					
					IF BIKER_SELL_CLUB_RUN()
					OR BIKER_SELL_RACE()
					OR BIKER_SELL_BAG_DROP()
						SET_BLIP_ROUTE(sLocaldata.blipDrop[i], TRUE)
					ELSE
						IF iVehicleIn <> -1
//							PRINTLN("[3021340] [GPS] iVehicleIn ", iVehicleIn)
							iClosest = serverBD.iClosestDropOff[iVehicleIn]
//							PRINTLN("[3021340] [GPS] iClosest ", iClosest)
							IF iClosest <> -1
								IF DOES_BLIP_EXIST(sLocaldata.blipDrop[iClosest])
									SET_BLIP_ROUTE(sLocaldata.blipDrop[iClosest], TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_THIS_AN_AIR_VARIATION()
				OR BIKER_SELL_STING()
				OR ( DOES_BIKER_SELL_MULTIPLE_DROP_OFFS() AND NOT BIKER_SELL_TRASHMASTER() AND NOT BIKER_SELL_POSTMAN() AND NOT BIKER_SELL_FRIENDS_IN_NEED())
					sLocaldata.blipDropArea[i] = ADD_BLIP_FOR_RADIUS(vCoords, GET_AREA_BLIP_RADIUS())
					SET_BLIP_COLOUR(sLocaldata.blipDropArea[i], BLIP_COLOUR_YELLOW)
					SET_BLIP_ALPHA(sLocaldata.blipDropArea[i], 100)
					SHOW_HEIGHT_ON_BLIP(sLocaldata.blipDropArea[i], FALSE)
					PRINTLN("[BIKER_SELL] [BLIP] MAINTAIN_DROP_BLIPS, blipDropArea ", i)
				ENDIF
			ENDIF
		ENDIF
		
		// GPS for helpers outside vehicles
		IF BIKER_SELL_POSTMAN()
			IF NOT IS_PLAYER_IN_ANY_SELL_VEH()
			
				iVehicleIn = GET_SELL_VEHICLE_THAT_HAS_NOT_COMPLETED_DROP(i)

				IF IS_NET_PLAYER_OK(PLAYER_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF iVehicleIn <> -1
		//					PRINTLN("[3021340] [GPS] A iVehicleIn ", iVehicleIn)
							iClosest = serverBD.iClosestDropOff[iVehicleIn]
		//					PRINTLN("[3021340] [GPS] A iClosest ", iClosest)
							IF iClosest <> -1
								IF DOES_BLIP_EXIST(sLocaldata.blipDrop[iClosest])
									IF NOT DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipDrop[iClosest])
										SET_BLIP_ROUTE(sLocaldata.blipDrop[iClosest], TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	// GPS
	IF iClosestNoBag != -1
		SET_BLIP_ROUTE(sLocaldata.blipDrop[iClosestNoBag], TRUE)
//		PRINTLN("[3021340] [GPS] iClosestNoBag ", iClosest)
	ENDIF
ENDPROC

PROC REMOVE_DROP_BLIPS(BOOL bRemoveFoot)
	INT i
	REPEAT GET_MAX_SELL_DROP_OFFS() i
		IF DOES_BLIP_EXIST(sLocaldata.blipDrop[i])
			PRINTLN("[BIKER_SELL] [BLIP] REMOVE_DROP_BLIP  ")
			REMOVE_BLIP(sLocaldata.blipDrop[i])
		ENDIF
		IF DOES_BLIP_EXIST(sLocaldata.blipDropArea[i])
			PRINTLN("[BIKER_SELL] [BLIP] REMOVE_DROP_BLIP  ")
			REMOVE_BLIP(sLocaldata.blipDropArea[i])
		ENDIF
		IF bRemoveFoot
			IF DOES_BLIP_EXIST(sLocaldata.blipDropOnFoot[i])
				PRINTLN("[BIKER_SELL] [BLIP] [FOOT] REMOVE_FOOT_DROP_BLIPS  ")
				REMOVE_BLIP(sLocaldata.blipDropOnFoot[i])
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC VECTOR vMARKER_DIMENSIONS()

	IF IS_THIS_AN_AIR_VARIATION()
		RETURN <<cfSELL_CRATE_LOC_SIZE_Z, cfSELL_CRATE_LOC_SIZE_Z, cfSELL_CRATE_LOC_SIZE_Z>>
	ENDIF
	
	IF IS_THIS_A_SEA_VARIATION()
		RETURN <<cfSELL_STING_MULTIPLE_SIZE*1.5, cfSELL_STING_MULTIPLE_SIZE*1.5, 4.5>>
	ENDIF
	
	IF BIKER_SELL_TRASHMASTER()
//	OR BIKER_SELL_TACO_VAN()
	OR BIKER_SELL_POSTMAN()
	
		RETURN <<cfSELL_TRASH_SIZE, cfSELL_TRASH_SIZE, cfSELL_TRASH_SIZE>>
	ENDIF
	
	IF BIKER_SELL_FRIENDS_IN_NEED()
	
		RETURN <<cfSELL_FIN_SIZE, cfSELL_FIN_SIZE, 0.5>>
	ENDIF

	RETURN <<ciDELIVERY_DISTANCE, ciDELIVERY_DISTANCE, 0.5>>
ENDFUNC

FUNC INT iMARKER_ALPHA( INT iDefault)
	#IF IS_DEBUG_BUILD
	IF tiAlphaMarker <> ciMARKER_ALPHA
		
		RETURN tiAlphaMarker
	ENDIF
	#ENDIF
	
	IF IS_THIS_AN_AIR_VARIATION()
	
		RETURN ciMARKER_ALPHA
	ENDIF
	
	IF BIKER_SELL_TRASHMASTER()
//	OR BIKER_SELL_TACO_VAN()
	OR BIKER_SELL_POSTMAN()
	OR BIKER_SELL_FRIENDS_IN_NEED()
	
		RETURN 75
	ENDIF
	
	RETURN iDefault
ENDFUNC

PROC DRAW_DROP_OFF_MARKER()
	INT iDropOffBlipR, iDropOffBlipG, iDropOffBlipB, iDropOffBlipA//, i
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iDropOffBlipR, iDropOffBlipG, iDropOffBlipB, iDropOffBlipA)
	
	VECTOR vMarker
	
	IF BIKER_SELL_FRIENDS_IN_NEED()
	OR BIKER_SELL_BAG_DROP()
	OR BIKER_SELL_RACE()
	OR BIKER_SELL_CLUB_RUN()	
	//OR BIKER_SELL_EXCHANGE()
		
		INT iBag = iGET_PLAYER_BAG(PLAYER_ID())
		IF iBag > -1	
			IF NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(iBag)
				IF NOT IS_THIS_A_HIDDEN_DROP_OFF(serverBD.iClosestDropOff[iBag])
				AND iHelpType != ciCLUB_RUN_HELP_TYPE_WAIT
					vMarker = GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[iBag], serverBD.iRoute, iBag)
				
					PRINTLN("DRAW_DROP_OFF_MARKER, iBag = ", iBag, " serverBD.iClosestDropOff[iBag] = ", serverBD.iClosestDropOff[iBag], " vMarker = ", vMarker)
					
					DRAW_MARKER(MARKER_CYLINDER, vMarker, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, vMARKER_DIMENSIONS(), iDropOffBlipR, iDropOffBlipG, iDropOffBlipB, iMARKER_ALPHA(iDropOffBlipA))
				ELSE
					PRINTLN("DRAW_DROP_OFF_MARKER, 2 ")
				ENDIF
			ELSE
				PRINTLN("DRAW_DROP_OFF_MARKER, 1 ")
			ENDIF
		ELSE
			PRINTLN("DRAW_DROP_OFF_MARKER, 3 ")
		ENDIF
	ELSE
		IF NOT BIKER_SELL_STING()
		AND NOT DOES_BIKER_SELL_MULTIPLE_DROP_OFFS()
		OR BIKER_SELL_BORDER_PATROL()
		OR (BIKER_SELL_STING() AND BIKER_SELL_HAS_TWIST_HAPPENED())
		OR BIKER_SELL_TRASHMASTER()
		OR BIKER_SELL_POSTMAN()
//		OR BIKER_SELL_TACO_VAN()
			
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_VEHICLE) <>  WAITING_TO_START_TASK
				AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_VEHICLE) <>  PERFORMING_TASK
					IF IS_PLAYER_IN_ANY_SELL_VEH()
						INT iMyVeh = GET_MY_SELL_VEH_AS_INT()
						IF iMyVeh >= 0
							IF BIKER_SELL_BENSON()
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffVeh[iMyVeh])
									VEHICLE_INDEX viVehicle = NET_TO_VEH(serverBD.niDropOffVeh[iMyVeh])
									IF DOES_ENTITY_EXIST(viVehicle)
									AND IS_VEHICLE_DRIVEABLE(viVehicle)
										DRAW_MARKER(MARKER_CYLINDER, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viVehicle, <<0.0, -1.55, 0.05>>), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 
												<<1.2, 1.2, 0.5>>, iDropOffBlipR, iDropOffBlipG, iDropOffBlipB, iMARKER_ALPHA( iDropOffBlipA))
									ENDIF
								ENDIF
							ELSE
								IF NOT BIKER_SELL_POSTMAN()
								OR NOT bPOST_VEH_PARKED(iMyVeh)
//									IF NOT BIKER_SELL_TACO_VAN()
									IF serverBD.eDoorState[iMyVeh] <> eSELL_DOOR_STATE_OPEN
										DRAW_MARKER(MARKER_CYLINDER, GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[iMyVeh], serverBD.iRoute, GET_SELL_VEH_I_AM_IN_INT(), DEFAULT, bIS_POSTMAN_PAT()), 
													<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, vMARKER_DIMENSIONS(), iDropOffBlipR, iDropOffBlipG, iDropOffBlipB, iMARKER_ALPHA( iDropOffBlipA))
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_ALL_BLIPS()
 	REMOVE_CONTRABAND_BLIPS()
	REMOVE_DROP_BLIPS(TRUE)
	REMOVE_PED_BLIPS()
	REMOVE_VEHICLE_BLIPS()
	TOGGLE_LSIA_AIRPORT_BLIP(FALSE)
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(i))
			PLAYER_INDEX piPlayer = INT_TO_PLAYERINDEX(i)
			SET_PLAYER_BAG_CARRIER_BLIP(piPlayer, FALSE)
		ENDIF
	ENDREPEAT
ENDPROC

CONST_INT ciWARNING_TIMER 	600000 // 10MIN
CONST_INT ciEND_TIMER_15	900000 // 15MIN
CONST_INT ciEND_TIMER_20	1800000 // 20MIN
//CONST_INT ciEND_TIMED_VAR	420000 // 7MIN

FUNC INT iTIME_LIMIT()//()

//	IF bTIMED()
//	
//		RETURN serverBD.iTimedTimeLimit
//	ENDIF

	RETURN GET_MODE_TUNEABLE_TIME_LIMIT() //ciEND_TIMER_15
ENDFUNC

FUNC INT GET_TIME_REMAINING()//()
	
	INT iTimeReturn
	
	iTimeReturn = ( iTIME_LIMIT() - ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), serverBD.modeTimer.Timer)) )

	IF iTimeReturn < 0
		iTimeReturn = 0
	ELIF iTimeReturn > iTIME_LIMIT()
		iTimeReturn = iTIME_LIMIT() 
	ENDIF
	
	RETURN iTimeReturn
ENDFUNC

FUNC INT GET_REPAIR_TIME_REMAINING()
	
	INT iTimeReturn
	
	iTimeReturn = ( ciREPAIR_TIME - ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), serverBD.stRepair.Timer))  )

	IF iTimeReturn < 0
		iTimeReturn = 0
	ELIF iTimeReturn > ciREPAIR_TIME
		iTimeReturn = ciREPAIR_TIME 
	ENDIF
	
	RETURN iTimeReturn
ENDFUNC


FUNC INT GET_COUNT_OF_DELIVERED_CONTRABAND()
	INT iReturn, iLoop
	REPEAT ciMAX_SELL_BAGS iLoop
		iReturn+=serverBD.iVehDropCount[iLoop]
	ENDREPEAT
	RETURN iReturn
ENDFUNC

FUNC INT iAMOUNT_TO_DROP_OFF_FOR_HUD()
	IF BIKER_SELL_DEFAULT()
	OR BIKER_SELL_PROVEN()
		RETURN GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE()	
	ENDIF

	INT i
	INT iAmount

	IF MODE_HAS_PICKUP_BAGS()
		IF BIKER_SELL_CLUB_RUN()
		OR BIKER_SELL_FRIENDS_IN_NEED()
			iAmount = iDROP_TARGET()
		ELSE
			iAmount = GET_NUM_BAGS() - serverBD.iDropPenalty
		ENDIF
	ELSE
		IF BIKER_SELL_POSTMAN()
			IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
				REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
					iAmount += serverBD.iVehDropCount[i]
					
					IF NOT IS_BIT_SET(serverBD.iFailVehicle, i)
						iAmount += (GET_SELL_VEH_TARGET_DROP_COUNT() - serverBD.iVehDropCount[i])
					ENDIF
					
					IF IS_SELL_VEH_OK(i)
						iAmount -= serverBD.iVehPenalty[i]
					ENDIF
				ENDREPEAT
			ELSE
				iAmount = iORIGINAL_DROP_TARGET()
			ENDIF
		ELSE
			IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
				REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
					iAmount += serverBD.iVehDropCount[i]
					
					IF NOT IS_BIT_SET(serverBD.iFailVehicle, i)
					AND NOT IS_BIT_SET(serverBD.iDropVehPenaltyBitSet, i)
						iAmount += (GET_SELL_VEH_TARGET_DROP_COUNT() - serverBD.iVehDropCount[i])
					ENDIF
				ENDREPEAT
			ELSE
				iAmount = iORIGINAL_DROP_TARGET()
			ENDIF
		ENDIF
	ENDIF

	RETURN iAmount
ENDFUNC

FUNC BOOL SHOULD_SHOW_CONTRABAND_UI()
	SWITCH GET_SELL_VAR()
		CASE eBIKER_SELL_AIR_DROP_AT_SEA
		CASE eBIKER_SELL_STING_OP
		CASE eBIKER_SELL_HELICOPTER_DROP
		CASE eBIKER_SELL_TRASHMASTER
//		CASE eBIKER_SELL_TACO_VAN
		CASE eBIKER_SELL_POSTMAN
		CASE eBIKER_SELL_FRIENDS_IN_NEED
		CASE eBIKER_SELL_BORDER_PATROL
		CASE eBIKER_SELL_BENSON
		CASE eBIKER_SELL_RACE
		CASE eBIKER_SELL_BAG_DROP
		CASE eBIKER_SELL_CLUB_RUN
//		CASE eBIKER_SELL_EXCHANGE
			RETURN TRUE
		CASE eBIKER_SELL_DEFAULT
			IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
				RETURN TRUE
			ENDIF
			RETURN FALSE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL bACTIVATE_GLOBAL_SIGNAL()

	SWITCH GET_SELL_VAR()
//		CASE eBIKER_SELL_TACO_VAN
//
//			RETURN IS_SERVER_BIT1_SET(eSERVERBITSET1_DROP_OFF_REACHED) 
			
//		CASE eBIKER_SELL_EXCHANGE
//		
//			IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_0_ENTERED_ONCE)	
//			OR IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_1_ENTERED_ONCE)	
//			OR IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_2_ENTERED_ONCE)	
//			OR IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_3_ENTERED_ONCE)
//			
//				RETURN TRUE
//			ELSE
//				RETURN FALSE
//			ENDIF
//		BREAK
	ENDSWITCH

	RETURN TRUE
ENDFUNC

PROC MAINTAIN_GLOBAL_SIGNAL_TIMER()
	IF SHOULD_DELAY_BLIP_FOR_FACTORY()
		IF HAS_NET_TIMER_STARTED(serverBD.stBlipDelayTimer)
			IF NOT HAS_NET_TIMER_EXPIRED(serverBD.stBlipDelayTimer, GET_BLIP_DELAY_TIME_FOR_FACTORY())
				INT iEventTime
				HUD_COLOURS eColour
		
				iEventTime = GET_BLIP_DELAY_TIME_FOR_FACTORY() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.stBlipDelayTimer)
				
				IF iEventTime > CONTRABAND_NEARLY_GLOBAL_TIME
					eColour = HUD_COLOUR_WHITE
				ELSE
					eColour = HUD_COLOUR_RED
				ENDIF
			
				IF iEventTime > 0
					DRAW_GENERIC_TIMER(iEventTime, "BYCB_GLBPNG", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_TOP, FALSE, eColour, HUDFLASHING_NONE, 0, FALSE, eColour)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DO_RADAR_JAM_HUD_AND_SOUNDS(INT iVeh)

	IF (IS_DRIVER_OF_SELL_VEH(iVeh)
	AND DOES_SELL_VEH_HAVE_PASSENGER(iVeh))
		RETURN TRUE
	ENDIF
	
	IF (IS_PASSENGER(iVeh)
	AND GET_DRIVER_OF_SELL_VEH(iVeh) != INVALID_PLAYER_INDEX())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC DRAW_HUD_BOTTOM_RIGHT()

	HUD_COLOURS hcColour = HUD_COLOUR_WHITE
	
	sLocalData.iTimeLeft = GET_TIME_REMAINING()	
	
	IF IS_OK_TO_DRAW_BOTTOM_RIGHT_HUD(TRUE)
	AND sLocalData.iTimeLeft < iTIME_LIMIT()

		SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
	
		// Repair meter
		IF HAS_NET_TIMER_STARTED(serverBD.stRepair)

			START_NET_TIMER(sLocaldata.stExtraRepairHudTime)

			DRAW_GENERIC_METER(( ciREPAIR_TIME - GET_REPAIR_TIME_REMAINING() ), ciREPAIR_TIME, "SCONTRA_REPAIR", HUD_COLOUR_RED, DEFAULT, HUDORDER_THIRDBOTTOM) 
			
		// url:bugstar:2768230 - Sell - Land Breakdown - The Repairing bar should stay on screen for a couple of extra seconds once it has reached full. 
		ELIF HAS_NET_TIMER_STARTED(sLocaldata.stExtraRepairHudTime) 
			IF HAS_NET_TIMER_EXPIRED(sLocaldata.stExtraRepairHudTime, ( ciREPAIR_TIME + 2000 ))
				RESET_NET_TIMER(sLocaldata.stExtraRepairHudTime)
			ELSE
				DRAW_GENERIC_METER(ciREPAIR_TIME, ciREPAIR_TIME, "SCONTRA_REPAIR", HUD_COLOUR_RED, DEFAULT, HUDORDER_THIRDBOTTOM) 
			ENDIF
		ENDIF

		IF sLocalData.iTimeLeft <= 30000
			hcColour = HUD_COLOUR_RED
		ENDIF
		
		// Dropped off meter
//		IF HAS_NET_TIMER_STARTED(serverBD.stDroppedOff)
//			DRAW_GENERIC_METER(GET_UNLOAD_TIME_REMAINING(), ciDROP_TIME, "SCONTRA_ST_DRP", HUD_COLOUR_WHITE, DEFAULT, HUDORDER_THIRDBOTTOM) 
//		ENDIF
		
		MAINTAIN_GLOBAL_SIGNAL_TIMER()
		
		// CONTRABAND SOLD 0/5 
		IF SHOULD_SHOW_CONTRABAND_UI()
			HUDORDER order
			
			IF AM_I_IN_ANY_SELL_VEH()
				IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
					INT iMyVeh = GET_SELL_VEH_I_AM_IN_INT()
					IF iMyVeh != -1
						IF NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(iMyVeh)
						AND IS_SELL_VEH_OK(iMyVeh)
							order = HUDORDER_THIRDBOTTOM
							
							IF BIKER_SELL_BORDER_PATROL()
								order = HUDORDER_FIFTHBOTTOM
							ENDIF
							
							INT iAmountToDrop = GET_SELL_VEH_TARGET_DROP_COUNT()
							INT iAmountHeld = iAmountToDrop - serverBD.iVehDropCount[iMyVeh]
							DRAW_GENERIC_BIG_NUMBER(iAmountHeld, "BSELL_PRODHELD", DEFAULT, HUD_COLOUR_WHITE, order)
						ENDIF
					ENDIF
				ELSE
					IF NOT HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
						order = HUDORDER_THIRDBOTTOM
						
						IF BIKER_SELL_BORDER_PATROL()
							order = HUDORDER_FIFTHBOTTOM
						ENDIF
						
						INT iAmountToDrop = GET_SELL_VEH_TARGET_DROP_COUNT()
						INT iAmountHeld = iAmountToDrop - serverBD.iDroppedOffCount
						DRAW_GENERIC_BIG_NUMBER(iAmountHeld, "BSELL_PRODHELD", DEFAULT, HUD_COLOUR_WHITE, order)
					ENDIF
				ENDIF
			ELIF MODE_HAS_PICKUP_BAGS()
				IF DO_I_HAVE_A_BAG()
					INT iBag = iGET_PLAYER_BAG(PLAYER_ID())
					IF iBag != -1
						IF NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(iBag)
							order = HUDORDER_THIRDBOTTOM							
							INT iAmountToDrop = GET_SELL_VEH_TARGET_DROP_COUNT()
							INT iAmountHeld = iAmountToDrop - serverBD.iVehDropCount[iBag]
							PRINTLN("iAmountToDrop = ", iAmountToDrop, " iAmountHeld = ", iAmountHeld, " GET_NUM_BAGS() = ", GET_NUM_BAGS())
							DRAW_GENERIC_BIG_NUMBER(iAmountHeld, "BSELL_PRODHELD", DEFAULT, HUD_COLOUR_WHITE, order)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			order = HUDORDER_SECONDBOTTOM
			
			IF BIKER_SELL_BORDER_PATROL()
				order = HUDORDER_FOURTHBOTTOM
			ENDIF
		
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(iAMOUNT_DROPPED_OFF_FOR_HUD(), iAMOUNT_TO_DROP_OFF_FOR_HUD(), "SCONTRA_HUD_SC", DEFAULT, HUD_COLOUR_WHITE, order)
		ENDIF
		
		IF BIKER_SELL_BORDER_PATROL()
			IF IS_DRIVER_OF_ANY_SELL_VEH()
			OR IS_PASSENGER_OF_ANY_SELL_VEHICLE()
				INT iMyVeh = GET_SELL_VEH_I_AM_IN_INT()
				IF iMyVeh != -1
					INT iVeh = GET_SELL_VEH_I_AM_IN_INT()
					INT iCheckpointTimeRemaining
					INT iDriver
					
					IF IS_DRIVER_OF_SELL_VEH(iVeh)
						iCheckpointTimeRemaining = GET_BORDER_PATROL_CHECKPOINT_TIME() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(playerBD[PARTICIPANT_ID_TO_INT()].BorderPatrolCheckpointTimer[iMyVeh])
					ELSE
						IF GET_DRIVER_OF_SELL_VEH(iVeh) != INVALID_PLAYER_INDEX()
							iDriver = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(GET_DRIVER_OF_SELL_VEH(iVeh)))
						ENDIF
					
						IF iDriver != -1
							iCheckpointTimeRemaining = GET_BORDER_PATROL_CHECKPOINT_TIME() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(playerBD[iDriver].BorderPatrolCheckpointTimer[iMyVeh])
						ENDIF
					ENDIF
				
					IF serverBD.iBonus[iVeh] > 0
						HUD_COLOURS eColour = HUD_COLOUR_WHITE
					
						IF iCheckpointTimeRemaining < 0
							iCheckpointTimeRemaining = 0
							eColour = HUD_COLOUR_RED
						ENDIF
												
						DRAW_GENERIC_TIMER(iCheckpointTimeRemaining, "BIKESELL_BPTR", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM, DEFAULT, eColour)
					ENDIF
					
					DRAW_GENERIC_SCORE(serverBD.iBonus[iVeh], "SCONTRA_HUD_DMG", DEFAULT, HUD_COLOUR_GOLD, HUDORDER_SECONDBOTTOM, FALSE, "HUD_CASH")
				ENDIF
			ENDIF
		ENDIF
	
		// Mission time
		DRAW_GENERIC_TIMER(sLocalData.iTimeLeft, "SCONTRA_TIMER", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, hcColour)
	ENDIF
ENDPROC

FUNC STRING sOPEN_TEXT()

	RETURN "BCONTRA_TXT_9"
ENDFUNC

PROC MANAGE_TEXT_MESSAGES()
//	IF NOT IS_BIT_SET(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_UNKNOWN)
//		IF IS_THIS_AN_AIR_VARIATION()
//			IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_BIKER_GANG()
//			OR IS_LOCAL_BIT0_SET(eLOCALBITSET0_PHONE_TRIGGERED)
//				IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
//				AND NOT IS_PHONE_ONSCREEN()
//					// "Hello, as per the PA's request the goods have been moved to the specified delivery vehicle."
//					IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_MP_RAY_LAVOY, "SCONTRA_TXT_21", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
//						SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_UNKNOWN)
//					ENDIF
//				ENDIF
//			ENDIF
//		ELSE
//			SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_UNKNOWN)
//		ENDIF
//	ELSE
		// Opening text message
		IF BIKER_SELL_PROVEN()			
			IF IS_BIT_SET(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_PROVEN_DRIVER)
				IF IS_DRIVER_OF_ANY_SELL_VEH()
				AND HAS_NET_TIMER_EXPIRED(sLocaldata.stInVeh, sLocaldata.iStonedDelay + 30000)
					IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BIKER_CH2, "BK_SELL_TXTM_2", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
						CLEAR_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_PROVEN_DRIVER)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF BIKER_SELL_STING()
			IF IS_SERVER_BIT1_SET(eSERVERBITSET1_SEND_WANTED_TEXT_MESSAGE)
				IF NOT IS_LOCAL_BIT1_SET(eLOCALBITSET1_LOST_WANTED_TEXT_MESSAGE)
					IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BIKER_CH2, "SBIKERP_NWBY", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
						PRINTLN("[BIKER_SELL] [MANAGE_TEXT_MESSAGES] SBIKERP_NWBY ")
						SET_LOCAL_BIT1(eLOCALBITSET1_LOST_WANTED_TEXT_MESSAGE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_PROVEN_WANTED)
			IF IS_LOCAL_BIT1_SET(eLOCALBITSET1_WANTED_PROVEN)
				// "Well they know you ain't a cop now! Lose them!"
				IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BIKER_CH2, "SBIKERP_HLP3", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
					SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_PROVEN_WANTED)
				ENDIF
			ELIF BIKER_SELL_PROVEN()
			AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			AND IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_0_ENTERED_ONCE)
				IF NOT IS_BIT_SET(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_PROVEN_MC)
					IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BIKER_CH2, "BK_SELL_TXTM_3", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
						SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_PROVEN_MC)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_TWIST_TEXT_SENT)
			SWITCH GET_SELL_VAR()
				CASE eBIKER_SELL_AIR_DROP_AT_SEA
					IF IS_DRIVER_OF_ANY_SELL_VEH()
						// "We don't want the cops on our tail. Flow low for this one! Keep an eye on your radar."
						IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BIKER_CH2, "BK_SELL_TXTM_1", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
							SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_TWIST_TEXT_SENT)
						ENDIF
					ENDIF
				BREAK
//				CASE eBIKER_SELL_EXCHANGE
//					IF IS_LOCAL_BIT1_SET(eLOCALBITSET1_ADDED_TWIST_BLIPS)
//						//Hah! They're not so dumb after all. Hold them off and get those wheels to the buyer.
//						IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BIKER_CH2, "BK_SELL_TXTM_0", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
//							SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_TWIST_TEXT_SENT)
//						ENDIF
//					ENDIF
//				BREAK
				CASE eBIKER_SELL_STING_OP
					IF HAVE_PEDS_BEEN_SPOOKED()
						// Shit. Undercover cops. I shoulda seen this coming... paid 'em off. Get the goods the hell outta there and I'll source another buyer.
						IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BIKER_CH2, "SBIKERS_TXTG1", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
							SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_TWIST_TEXT_SENT)
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
//	ENDIF
ENDPROC

FUNC STRING sDROP_TEXT( BOOL bDriver)

	STRING sDropText
	INT iBag
	
	IF iDroppedOffCountClubRunCache != -1
	AND iDroppedOffCountClubRunCache != serverBD.iDroppedOffCountClubRun
		iDroppedOffCountClubRunCache = -1
		PRINTLN("[BIKER_SELL] iDroppedOffCountClubRunCache != serverBD.iDroppedOffCountClubRun, iDroppedOffCountClubRunCache = -1")
	ENDIF
	
	IF bDriver
	OR BIKER_SELL_CLUB_RUN()
	OR (DO_I_HAVE_A_BAG() AND NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(iGET_PLAYER_BAG(PLAYER_ID())))
		iBag = iGET_PLAYER_BAG(PLAYER_ID())
		IF BIKER_SELL_CLUB_RUN()
			IF iDroppedOffCountClubRunCache = serverBD.iDroppedOffCountClubRun
			OR (iBag != -1
			AND serverBD.iVehDropCount[iBag] > serverBD.iDroppedOffCountClubRun
			AND DO_I_HAVE_A_BAG()
			AND ENTITY_AT_DROP_OFF_COORDS(GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iCurrentDropTeam, serverBD.iRoute, iBag), PLAYER_PED_ID()))
				//Wait for the rest of your MC to arrive.
				sDropText = "SBCONTRA_DROPW"	
				iHelpType = ciCLUB_RUN_HELP_TYPE_WAIT
				IF iDroppedOffCountClubRunCache = -1
					iDroppedOffCountClubRunCache = serverBD.iDroppedOffCountClubRun
					PRINTLN("[BIKER_SELL] iDroppedOffCountClubRunCache = serverBD.iDroppedOffCountClubRun, iDroppedOffCountClubRunCache = ", iDroppedOffCountClubRunCache)
				ENDIF
			ELIF DO_I_HAVE_A_BAG()
			AND iBag <> -1
				iHelpType = ciCLUB_RUN_HELP_TYPE_PRODUCT
				SWITCH serverBD.iDroppedOffCountClubRun
					CASE 0
						// "Deliver the ~a~ to the ~n~st ~y~drop-off."
						sDropText = "SDRP_1"
					BREAK
					CASE 1
						// "Deliver the ~a~ to the ~n~nd ~y~drop-off."
						sDropText = "SDRP_2"
					BREAK
					CASE 2
						// "Deliver the ~a~ to the ~n~nd ~y~drop-off."
						sDropText = "SDRP_3"
					BREAK
					DEFAULT
						//Wait for the rest of your MC to arrive.
						sDropText = "SBCONTRA_DROPW"	
					BREAK
				ENDSWITCH
			ELSE
				iHelpType = ciCLUB_RUN_HELP_TYPE_PRODUCT
				// "Help deliver the ~a~~s~ to the ~y~drop-offs."
				sDropText = "SBCONTRA_HLPD"
			ENDIF
		ELIF BIKER_SELL_RACE()
		OR BIKER_SELL_BAG_DROP()
			IF DO_I_HAVE_A_BAG()
				//Race the ~a~ to the drop-off.
				IF BIKER_SELL_RACE()
					sDropText = "SBCONTRA_R1"
				ELSE
					// "Deliver the ~a~ to the ~y~drop-off."
					sDropText = "SBCONTRA_DROP"
				ENDIF
			ELSE
				// "Help deliver the ~a~~s~ to the ~y~drop-offs."
				sDropText = "SBCONTRA_HLPD"
			ENDIF
		ELIF BIKER_SELL_AIR_DROP_AT_SEA()
			IF bUSE_PLURAL()
				// "Fly low and deliver Contraband to the ~y~drop-offs."
				sDropText = "SBCONTRA_DROPP"
			ELSE
				// "Fly low and deliver Contraband to the ~y~drop-off."
				sDropText = "SBCONTRA_DROP"
			ENDIF
		ELIF IS_THIS_AN_AIR_VARIATION()
			// "Deliver the Contraband to the ~y~restricted area."
			IF bUSE_PLURAL()
				// "Deliver the Contraband to the ~y~drop-offs."
				sDropText = "SBCONTRA_DROPP"
			ELSE
				// "Deliver the Contraband to the ~y~drop-off."
				sDropText = "SBCONTRA_DROP"
			ENDIF
		ELIF BIKER_SELL_POSTMAN()
			// "Go to a ~y~drop-off."
			sDropText = "SBIKER_POST2"
		ELIF BIKER_SELL_FRIENDS_IN_NEED()
			IF bUSE_PLURAL()
				// "Deliver the ~a~ to the ~b~buyer."
				sDropText = "SBCONTRA_DPFINP"
			ELSE
				// "Deliver the ~a~ to the ~b~buyers."
				sDropText = "SBCONTRA_DPFIN"
			ENDIF
		ELSE
			IF BIKER_SELL_HAS_TWIST_HAPPENED()		
			AND NOT BIKER_SELL_CLUB_RUN()
				IF bUSE_PLURAL()
				AND NOT BIKER_SELL_BENSON()
				AND NOT BIKER_SELL_STING()
					//Deliver the ~a~ to the back up ~y~drop-off.
					sDropText = "SBCONTRA_DROPPT"
				ELSE
					//"Deliver the ~a~ to the back up ~y~drop-off.
					sDropText = "SBCONTRA_DROPT"
				ENDIF
			ELSE
//				IF BIKER_SELL_EXCHANGE()
//					IF DO_I_HAVE_A_BAG()
//						//Deliver the ~a~ to the ~y~drop-off.
//						sDropText = "SBCONTRA_DROP"
//					ELSE
//						//Help deliver the ~a~~s~ to the ~y~drop-off.
//						sDropText = "SBCONTRA_HLPDT"
//					ENDIF
//				EL
				IF BIKER_SELL_BENSON()
					// "Deliver the ~a~ to the ~y~drop-off."
					sDropText = "SBCONTRA_DROP"
				ELIF bUSE_PLURAL()
					// "Deliver the ~a~ to the ~y~drop-offs."
					sDropText = "SBCONTRA_DROPP"
				ELSE
					// "Deliver the ~a~ to the final ~y~drop-off."
					IF DOES_BIKER_SELL_MULTIPLE_DROP_OFFS()
						sDropText = "SBCONTRA_DROPF"
					ELSE
						// "Deliver the ~a~ to the ~y~drop-off."
						sDropText = "SBCONTRA_DROP"
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF BIKER_SELL_HAS_TWIST_HAPPENED()
		AND NOT BIKER_SELL_CLUB_RUN()
			IF bUSE_PLURAL()
				sDropText = "SBCONTRA_HLPDPT"
			ELSE
				sDropText = "SBCONTRA_HLPDT"
			ENDIF
		ELIF BIKER_SELL_POSTMAN()
			IF bIS_POSTMAN_PAT()
				// "Deliver the parcel to the ~y~property."
				sDropText = "SBIKER_POST4"
			ELSE
				IF bUSE_PLURAL()
					// "Help deliver the ~a~~s~ at the ~y~drop-offs."
					sDropText = "SBIKER_POST3P"
				ELSE
					// "Help deliver the ~a~~s~ at the ~y~drop-off."
					sDropText = "SBIKER_POST3"
				ENDIF
			ENDIF
		ELIF BIKER_SELL_FRIENDS_IN_NEED()
			IF bUSE_PLURAL()
				// "Help deliver the ~a~ to the ~b~buyers."
				sDropText = "SB_HDPFINP"
			ELSE
				// "Help deliver the ~a~ to the ~b~buyer."
				sDropText = "SB_HDPFIN"
			ENDIF
		ELSE
			IF bUSE_PLURAL()
				// "Help deliver the ~a~~s~ to the ~y~drop-offs."
				sDropText = "SBCONTRA_HLPDP"
			ELSE
				// "Help deliver the ~a~~s~ to the ~y~drop-off."
				sDropText = "SBCONTRA_HLPD"
			ENDIF
		ENDIF
	ENDIF
	
	RETURN sDropText
ENDFUNC

FUNC STRING sVEH_TEXT()

	IF IS_THIS_AN_AIR_VARIATION()

		IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
			IF IS_PLAYER_IN_AIRPORT()
			OR HAS_ANY_SELL_VEHICLE_BEEN_ENTERED()
				IF IS_THIS_A_HELI_VARIATION()
					// "Enter a ~HUD_COLOUR_BLUE~helicopter."
					RETURN "SCONTRA_HELI_1b"
				ELSE
					// "Enter a ~HUD_COLOUR_BLUE~plane."
					RETURN "SCONTRA_VEH_1b"
				ENDIF
			ELSE	
				IF IS_LAUNCH_FACTORY_IN_CITY()
					IF IS_THIS_A_HELI_VARIATION()
						// "Go to ~y~LSIA ~s~and enter a ~b~helicopter."
						RETURN "SCONTRA_DRP_LHP"
					ELSE
						// "Go to ~y~LSIA ~s~and enter a ~b~plane."
						RETURN "SCONTRA_DROP_LP"
					ENDIF
				ELSE
					IF IS_THIS_A_HELI_VARIATION()
						// "Go to ~y~McKenzie Field ~s~and enter a ~b~helicopter."
						RETURN "SCONTRA_DRP_MHP"
					ELSE
						// "Go to ~y~McKenzie Field ~s~and enter a ~b~plane."
						RETURN "SCONTRA_DROP_MP"
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_PLAYER_IN_AIRPORT()
			OR HAS_ANY_SELL_VEHICLE_BEEN_ENTERED()
				IF IS_THIS_A_HELI_VARIATION()
					// "Enter the ~HUD_COLOUR_BLUE~helicopter."
					RETURN "SCONTRA_HELI_1"
				ELSE
					// "Enter the ~HUD_COLOUR_BLUE~plane."
					RETURN "SCONTRA_VEH_1"
				ENDIF
			ELSE	
				IF IS_LAUNCH_FACTORY_IN_CITY()
					IF IS_THIS_A_HELI_VARIATION()
						// "Go to ~y~LSIA ~s~and enter the ~b~helicopter."
						RETURN "SCONTRA_DRP_LHS"
					ELSE
						// "Go to ~y~LSIA ~s~and enter the ~b~plane."
						RETURN "SCONTRA_DROP_LS"
					ENDIF
				ELSE
					IF IS_THIS_A_HELI_VARIATION()
						// "Go to ~y~McKenzie Field ~s~and enter the ~b~helicopter."
						RETURN "SCONTRA_DRP_MHS"
					ELSE
						// "Go to ~y~McKenzie Field ~s~and enter the ~b~plane."
						RETURN "SCONTRA_DROP_MS"
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELIF IS_THIS_A_SEA_VARIATION()
		IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
			RETURN "SCONTRA_VEH_2b"
		ELSE
			RETURN "SCONTRA_VEH_2"
		ENDIF
	ENDIF
	
	SWITCH GET_SELL_VAR()
		CASE eBIKER_SELL_TRASHMASTER
			IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
				RETURN "SBIKERT_VEHb"
			ELSE
				RETURN "SBIKERT_VEH_VEH"
			ENDIF
		BREAK
		CASE eBIKER_SELL_POSTMAN 	
			IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
				RETURN "SBIKERM_VEHb"
			ENDIF
			RETURN "SBIKERM_VEH"
		BREAK
		CASE eBIKER_SELL_PROVEN
		CASE eBIKER_SELL_DEFAULT
			IF serverBD.eShipmentType = eBIKER_SHIPMENT_TYPE_SMALL
			OR serverBD.eShipmentType = eBIKER_SHIPMENT_TYPE_MEDIUM
				IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
					// "Enter the ~HUD_COLOUR_BLUE~van."
					RETURN "SBIKERM_VEHb"
				ELSE
					// "Enter a ~HUD_COLOUR_BLUE~van."
					RETURN "SBIKERM_VEH"
				ENDIF
			ELSE
				IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
					// "Enter a ~HUD_COLOUR_BLUE~truck."
					RETURN "SBIKERT_VEHb"
				ELSE
					// "Enter the ~HUD_COLOUR_BLUE~truck."
					RETURN "SBIKERT_VEH_VEH"
				ENDIF
			ENDIF
		BREAK
//		CASE eBIKER_SELL_TACO_VAN
//			IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
//				RETURN "SBIKERTc_VEHb"
//			ELSE
//				RETURN "SBIKERTc_VEH"
//			ENDIF
//		BREAK
	ENDSWITCH
//	
//	IF BIKER_SELL_EXCHANGE()
//	AND BIKER_SELL_HAS_TWIST_HAPPENED()
//		IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
//			RETURN "SCONTRA_VANb"
//		ENDIF
//		RETURN "SCONTRA_VAN"
//	ENDIF
	
	IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
		IF IS_MOTOR_BIKE_SELL_VEHICLE()
			RETURN "SBCONTRA_VEHb"
		ENDIF
		// "Enter a ~HUD_COLOUR_BLUE~truck."
		RETURN "SCONTRA_VEHb"
	ENDIF
	
	IF IS_MOTOR_BIKE_SELL_VEHICLE()
		RETURN "SBCONTRA_VEH"
	ENDIF
	// "Enter the ~HUD_COLOUR_BLUE~truck."
	RETURN "SCONTRA_VEH"
ENDFUNC

PROC HANDLE_SELL_BLIPS(BOOL bMissionOver = FALSE)
	INT iVeh

	IF NOT bMissionOver
		MAINTAIN_PED_BLIPS()
		MAINTAIN_NON_CONTRABAND_VEHICLE_BLIPS()
		MAINTAIN_CONTRABAND_BLIPS()
		MAINTAIN_AIRPORT_BLIP()
		
		// If local player is in a sell vehicle, show the drop off blips for that vehicle,
		// otherwise, wait for all vehs to be occupied and blip one of the drop offs
		IF IS_PLAYER_IN_ANY_SELL_VEH(FALSE)
			REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iVeh
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh])
					IF AM_I_IN_THIS_SELL_VEH(iVeh)
						//PRINTLN("[BIKER_SELL] HANDLE_SELL_BLIPS - in Loop")
						MAINTAIN_DROP_BLIPS(iVeh)
					ENDIF
				ENDIF
			ENDREPEAT
		ELSE
			IF ARE_ALL_SELL_VEHS_OCCUPIED()
				//PRINTLN("[BIKER_SELL] HANDLE_SELL_BLIPS - not in Loop")
				MAINTAIN_DROP_BLIPS()
			ELSE
				REMOVE_DROP_BLIPS(FALSE)
			ENDIF
			REMOVE_LAST_FOOT_BLIP()
		ENDIF

	ELSE
		REMOVE_ALL_BLIPS()
	ENDIF
ENDPROC

//Draws a marker above a vehicle so the arrow doesn't clip into it
PROC DRAW_MARKER_ABOVE_VEHICLE(VEHICLE_INDEX viTemp, HUD_COLOURS colour)
	VECTOR returnMin, returnMax
	FLOAT fOffset
	INT r, g, b, a
	
	GET_HUD_COLOUR(colour, r, g, b, a)
	
	fOffset = 0.5 
	
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(viTemp), returnMin, returnMax)

	FLOAT fCentreHeight = (returnMax.z - returnMin.z)/2
	FLOAT fZdiff = returnMax.z - fCentreHeight
	
	//-- Dave W - z-offset from centre needs to be at least the max height of the vehicle.
	IF fOffset <= (fZdiff + 0.1)
		fOffset = fZdiff + 0.4
	ENDIF
	
	DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(viTemp)+<<0,0,((returnMax.z - returnMin.z)/2) + fOffset>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, r, g, b, 100, TRUE, TRUE)
ENDPROC

PROC HANDLE_SELL_MARKERS()
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF DOES_ENTITY_EXIST(VEH_ID(i))
			IF IS_SELL_VEH_OCCUPIED(i)
			AND NOT AM_I_IN_THIS_SELL_VEH(i)
				DRAW_MARKER_ABOVE_VEHICLE(VEH_ID(i), GB_GET_PLAYER_GANG_HUD_COLOUR(serverBD.piLaunchBoss))
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL DOES_ORGANIZATION_HAVE_WANTED_RATING()
	INT iParticipant
	PLAYER_INDEX playerID
	PARTICIPANT_INDEX participantID
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
			participantID = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
			playerID = NETWORK_GET_PLAYER_INDEX(participantID)
			IF IS_NET_PLAYER_OK(playerID, FALSE)
				IF GET_PLAYER_WANTED_LEVEL(playerID) != 0
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_SELL_TEXT( BOOL bMissionOver = FALSE)
	BOOL bMissionEnding = FALSE

	IF HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
	OR bMissionOver 
		bMissionEnding = TRUE
	ENDIF
	
	// For bugs like url:bugstar:3073103
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF GET_IS_TASK_ACTIVE(PLAYER_PED_ID(), CODE_TASK_EXIT_VEHICLE) 
			IF NOT HAS_NET_TIMER_STARTED(sLocaldata.stExitVeh)
				START_NET_TIMER(sLocaldata.stExitVeh)
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(sLocaldata.stExitVeh)
				IF HAS_NET_TIMER_EXPIRED(sLocaldata.stExitVeh, 1500)
					RESET_NET_TIMER(sLocaldata.stExitVeh)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(sLocaldata.stExitVeh)
			RESET_NET_TIMER(sLocaldata.stExitVeh)
		ENDIF
	ENDIF
	
//	IF HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
//	OR bMissionOver 
	IF bMissionEnding
		REMOVE_ALL_BLIPS()
		Clear_Any_Objective_Text_From_This_Script() // Clear god text, mode is over.
	ELSE
		MANAGE_TEXT_MESSAGES()
		
		IF BIKER_SELL_FRIENDS_IN_NEED()
			GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_FIN)
		ENDIF
	
		// Contraband help trigger ("Deliver the contraband ~HUD_COLOUR_BLUE~~BLIP_CONTRABAND~~HUD_COLOUR_WHITE~ to the drop-off to receive payment.")
		IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_0_ENTERED_ONCE)
			IF GB_IS_LOCAL_HELP_BIT_SET(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER)
				GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER)
			ENDIF
			IF GB_IS_LOCAL_HELP_BIT_SET(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER_P)
				GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER_P)
			ENDIF
		ELSE
			IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_INITIAL_HELP_SENT)
				IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
					IF bUSE_PLURAL()
						GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER_P) 
					ELSE 
						GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER) 
					ENDIF
//					IF DO_EASY_HELP_TEXT()
//					AND GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
//						GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_EASY_SELL_MISSION)
//					ENDIF
					IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
						GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_EXPLAIN_CONTINUE_DESTROY)
					ENDIF
					SET_LOCAL_BIT0(eLOCALBITSET0_INITIAL_HELP_SENT)
				ENDIF
			ENDIF
		ENDIF
		
		//If the plane has landed then remove the blips and print the new god text
		IF SHOULD_DO_KILL_ENEMIES_HELP()
			IF NOT Is_This_The_Current_Objective_Text("SCONTRA_AIRCAE")
				Print_Objective_Text("SCONTRA_AIRCAE")
			ENDIF
		ELIF PLAYER_WANTED()			
			// "Lose the Cops."
			IF NOT Is_This_The_Current_Objective_Text("BYCB_CLY_LSC0")
				Print_Objective_Text("BYCB_CLY_LSC0")
			ENDIF
		ELIF DO_I_HAVE_A_BAG()
		OR BIKER_SELL_CLUB_RUN()
			STRING stDropText
			SWITCH GET_SELL_VAR()
				CASE eBIKER_SELL_CLUB_RUN
					stDropText = sDROP_TEXT(IS_DRIVER_OF_ANY_SELL_VEH())
					IF NOT Is_This_The_Current_Objective_Text(stDropText)
						IF iHelpType = ciCLUB_RUN_HELP_TYPE_WAIT
							Print_Objective_Text(stDropText)
						ELSE
							Print_Objective_Text_With_String(stDropText, GET_SHIPMENT_TYPE_STRING())
						ENDIF
					ENDIF
					DRAW_DROP_OFF_MARKER()
					PRINTLN("Print_Objective_Text, eBIKER_SELL_CLUB_RUN  ")
				BREAK
				
				DEFAULT
					IF NOT Is_This_The_Current_Objective_Text(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()))
						Print_Objective_Text_With_String(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()), GET_SHIPMENT_TYPE_STRING())
					ENDIF
					DRAW_DROP_OFF_MARKER()
					PRINTLN("Print_Objective_Text, sVEH_TEXT 111 ")
				BREAK
			ENDSWITCH
		ELIF IS_ANY_SELL_VEH_OCCUPIED()
			IF NOT g_bSellWarningHelpPrinted
			AND NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_BIKER_GANG()
			AND NOT GB_IS_LOCAL_HELP_BIT_SET(eGB_LOCAL_HELP_BITSET_NOTIFICATION_WARN_ABOUT_GRIEF) 
				g_bSellWarningHelpPrinted = TRUE
				GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_WARN_ABOUT_GRIEF) 
				PRINTLN("[BIKER_SELL] g_bSellWarningHelpPrinted = TRUE")
			ENDIF
		
			//Don't draw the markers if it's air drop
			IF NOT IS_THIS_AN_AIR_VARIATION()
				DRAW_DROP_OFF_MARKER()
			ENDIF
			
			IF IS_DRIVER_OF_ANY_SELL_VEH()
				IF BIKER_SELL_HELICOPTER_DROP()
					IF IS_ENTITY_IN_AIR(GET_SELL_VEH_I_AM_IN())
					AND NOT DOES_SELL_VEH_HAVE_PASSENGER(GET_SELL_VEH_I_AM_IN_INT())
					AND NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(GET_SELL_VEH_I_AM_IN_INT())
						// "Press ~INPUT_FRONTEND_RIGHT~ to drop Contraband when you are flying above a drop-off."
						IF NOT IS_LOCAL_BIT1_SET(eLOCALBITSET0_SHOW_HELI_DROP_HELP)
							IF IS_SAFE_FOR_HELP_TEXT()
								PRINT_HELP_WITH_STRING_NO_SOUND("SBIKER_HELID", GET_SHIPMENT_TYPE_STRING())
								GB_SET_GANG_BOSS_HELP_BACKGROUND()
								SET_LOCAL_BIT1(eLOCALBITSET0_SHOW_HELI_DROP_HELP)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				PRINTLN("[CRT]  - HANDLE_SELL_TEXT - IS_ANY_SELL_VEH_OCCUPIED") 
				
				PRINTLN("Print_Objective_Text, sVEH_TEXT 222 ")
				
				IF NOT Is_This_The_Current_Objective_Text(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()))
					Print_Objective_Text_With_String(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()), GET_SHIPMENT_TYPE_STRING())
				ENDIF
			ELSE
				IF BIKER_SELL_HELICOPTER_DROP()
					IF GET_SELL_VEH_I_AM_IN_INT() != -1
						IF IS_PASSENGER(GET_SELL_VEH_I_AM_IN_INT(), TRUE)
							IF IS_ENTITY_IN_AIR(GET_SELL_VEH_I_AM_IN())
							AND IS_SELL_VEH_OCCUPIED(GET_SELL_VEH_I_AM_IN_INT())
							AND NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(GET_SELL_VEH_I_AM_IN_INT())
								// "Press ~INPUT_FRONTEND_RIGHT~ to drop Contraband when you are flying above a drop-off."
								IF NOT IS_LOCAL_BIT1_SET(eLOCALBITSET0_SHOW_HELI_DROP_HELP)
									IF IS_SAFE_FOR_HELP_TEXT()
										PRINT_HELP_WITH_STRING_NO_SOUND("SBIKER_HELID", GET_SHIPMENT_TYPE_STRING())
										GB_SET_GANG_BOSS_HELP_BACKGROUND()
										SET_LOCAL_BIT1(eLOCALBITSET0_SHOW_HELI_DROP_HELP)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
				
					IF bIS_POSTMAN_PAT()
						// "// "Post the ~a~~s~ at the ~y~drop-off.""
						PRINTLN("Print_Objective_Text, sVEH_TEXT 222 ")
						IF NOT Is_This_The_Current_Objective_Text(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()))
							Print_Objective_Text(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()))//, GET_SHIPMENT_TYPE_STRING_BLIPS(), HUD_COLOUR_WHITE)
						ENDIF
					ELSE
						IF ARE_ALL_SELL_VEHS_OCCUPIED() AND NOT HAS_NET_TIMER_STARTED(sLocaldata.stExitVeh)
						OR IS_PASSENGER_OF_ANY_SELL_VEHICLE() AND NOT HAS_NET_TIMER_STARTED(sLocaldata.stExitVeh)
							// "Help <C>~a~</C> ~s~deliver the ~a~~s~ to the ~y~drop-off."
							PRINTLN("Print_Objective_Text, sVEH_TEXT 333 ")
							IF NOT Is_This_The_Current_Objective_Text(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()))					
								//Print_Objective_Text_With_String(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()), "SCONTRA_CTRA")
								Print_Objective_Text_With_Coloured_Text_Label(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()), GET_SHIPMENT_TYPE_STRING(), HUD_COLOUR_BLUE)// GET_PLAYER_HUD_COLOUR(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
							ENDIF
						ELSE
							PRINTLN("Print_Objective_Text, sVEH_TEXT aaa ")
							// "Enter the ~HUD_COLOUR_BLUE~truck."
							IF NOT Is_This_The_Current_Objective_Text(sVEH_TEXT())
								Print_Objective_Text(sVEH_TEXT())
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ELSE
			IF bIS_POSTMAN_PAT()
				// "// "Post the ~a~~s~ at the ~y~drop-off.""
				PRINTLN("Print_Objective_Text, sVEH_TEXT dddd ")
				IF NOT Is_This_The_Current_Objective_Text(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()))
					Print_Objective_Text(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()))//, GET_SHIPMENT_TYPE_STRING_BLIPS(), HUD_COLOUR_WHITE)
				ENDIF
			ELSE
				IF ( BIKER_SELL_POSTMAN() AND IS_LOCAL_BIT1_SET(eLOCALBITSET1_SHUFFLE_BLOCKED) )
//				OR bCLOSEST_VEH_PARKED()
					PRINTLN("Print_Objective_Text, sVEH_TEXT 444 ")
					IF NOT Is_This_The_Current_Objective_Text(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()))
						Print_Objective_Text_With_String(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()), GET_SHIPMENT_TYPE_STRING())
					ENDIF
				ELSE
					IF MODE_HAS_PICKUP_BAGS()
					AND NOT DO_I_HAVE_A_BAG()
						PRINTLN("Print_Objective_Text, sVEH_TEXT BIKER_SELL_FRIENDS_IN_NEED ")
						IF NOT Is_This_The_Current_Objective_Text(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()))
							Print_Objective_Text_With_String(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()), GET_SHIPMENT_TYPE_STRING())
						ENDIF
					ELSE						
						IF bCLOSEST_VEH_PARKED()
						
							PRINTLN("Print_Objective_Text, bCLOSEST_VEH_PARKED  ")
							IF NOT Is_This_The_Current_Objective_Text(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()))
								Print_Objective_Text_With_String(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()), GET_SHIPMENT_TYPE_STRING())
							ENDIF
						ELSE
							PRINTLN("Print_Objective_Text, sVEH_TEXT bbb ")
							// "Enter the ~HUD_COLOUR_BLUE~truck."
							IF NOT Is_This_The_Current_Objective_Text(sVEH_TEXT())
								Print_Objective_Text(sVEH_TEXT())
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║		STONED																	║

FUNC STRING sSTONED_EFFECT(BOOL bOutro = FALSE)

	IF bOutro
		RETURN "BikerFilterOut"
	ENDIF

	RETURN "BikerFilter"
ENDFUNC

PROC DO_STONED_EFFECT()
	ANIMPOSTFX_PLAY(sSTONED_EFFECT(), 0, TRUE)
	Make_Ped_Drunk_Constant(PLAYER_PED_ID())
	START_NET_TIMER(sLocaldata.stDrunk)
	PRINTLN("[BIKER_SELL] - DO_STONED_EFFECT ")
ENDPROC

FUNC INT iGET_STONED_OUTRO_TIME()
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
	
		RETURN 1000
	ENDIF
	
	RETURN 10000
ENDFUNC

PROC CLEANUP_STONED_EFFECT()
	IF BIKER_SELL_PROVEN()
		IF ANIMPOSTFX_IS_RUNNING(sSTONED_EFFECT())
			ANIMPOSTFX_STOP(sSTONED_EFFECT())
			ANIMPOSTFX_PLAY(sSTONED_EFFECT(TRUE), iGET_STONED_OUTRO_TIME(), FALSE)
		ENDIF
	ENDIF
ENDPROC

CONST_INT ciSTONED_RESET	0
CONST_INT ciSTONED_VEH		1
CONST_INT ciSTONED_CLEANUP	2

PROC MAINTAIN_STONED()
	IF BIKER_SELL_PROVEN()
		SWITCH sLocaldata.iStonedProgress
			CASE ciSTONED_RESET
				IF IS_PLAYER_IN_SELL_VEH(0)
					IF NOT HAS_NET_TIMER_STARTED(sLocaldata.stInVeh)
						START_NET_TIMER(sLocaldata.stInVeh)
						RESET_NET_TIMER(sLocaldata.stOnFoot)
						sLocaldata.iStonedDelay = GET_RANDOM_INT_IN_RANGE(10000, 15000)
						
						sLocaldata.iStonedProgress = ciSTONED_VEH
						PRINTLN("[BIKER_SELL] - ciSTONED_RESET >> ciSTONED_VEH ")
					ENDIF
				ENDIF
			BREAK
			CASE ciSTONED_VEH
				IF IS_PLAYER_IN_SELL_VEH(0)
					IF HAS_NET_TIMER_EXPIRED(sLocaldata.stInVeh, sLocaldata.iStonedDelay)
						DO_STONED_EFFECT()
						SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_PROVEN_DRIVER)
						sLocaldata.iStonedProgress = ciSTONED_CLEANUP
						PRINTLN("[BIKER_SELL] - ciSTONED_VEH >> ciSTONED_CLEANUP ")
					ENDIF
				ELSE
					RESET_NET_TIMER(sLocaldata.stInVeh)
					sLocaldata.iStonedProgress = ciSTONED_RESET
					PRINTLN("[BIKER_SELL] - ciSTONED_VEH >> ciSTONED_RESET ")
				ENDIF
			BREAK
			CASE ciSTONED_CLEANUP
				IF NOT IS_PLAYER_IN_SELL_VEH(0)
					IF NOT HAS_NET_TIMER_STARTED(sLocaldata.stOnFoot)
						START_NET_TIMER(sLocaldata.stOnFoot)
					ENDIF
				ELSE
					RESET_NET_TIMER(sLocaldata.stOnFoot)
				ENDIF
				IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
				OR ( HAS_NET_TIMER_STARTED(sLocaldata.stOnFoot) AND HAS_NET_TIMER_EXPIRED(sLocaldata.stOnFoot, 20000) )
					CLEANUP_STONED_EFFECT()
					Make_Ped_Sober(PLAYER_PED_ID())
					RESET_NET_TIMER(sLocaldata.stInVeh)
					RESET_NET_TIMER(sLocaldata.stDrunk)
					sLocaldata.iStonedProgress = ciSTONED_RESET
					PRINTLN("[BIKER_SELL] - ciSTONED_CLEANUP >> ciSTONED_RESET ")
				ENDIF			
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC MAINTAIN_POSTMAN()
	IF BIKER_SELL_POSTMAN()
	
		BLOCK_PED_SHUFFLE(IS_PASSENGER_OF_ANY_SELL_VEHICLE())
	
		INT iVeh
		iVeh = GET_SELL_VEH_I_AM_IN_INT()
		
		IF sLocaldata.iStoredPostVeh = -1
			sLocaldata.iStoredPostVeh = GET_SELL_VEH_I_AM_IN_INT()
			PRINTLN("3015638 sLocaldata.iStoredPostVeh = ", sLocaldata.iStoredPostVeh)
		ENDIF
		
		SWITCH sLocaldata.iPostProgress
		
			CASE 0
				IF iVeh <> -1				
					IF bIS_POSTMAN_PAT()
						EJECT_PLAYER_FROM_VEHICLE(iVeh)
						IF bIS_POSTMAN_PAT()
							sLocaldata.iStoredPostVeh = iVeh
							PRINTLN("[BIKER_SELL] - MAINTAIN_POSTMAN >> iPostProgress = 1 ", sLocaldata.iStoredPostVeh)
							sLocaldata.iPostProgress = 1
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF NOT bIS_POSTMAN_PAT()
					PRINTLN("[BIKER_SELL] - MAINTAIN_POSTMAN >> iPostProgress = 0 ", sLocaldata.iStoredPostVeh)
					sLocaldata.iPostProgress = 0
				ELSE
					IF iVeh <> -1
						FREEZE_VEHICLE_IN_PLACE(VEH_ID(iVeh), iVeh, TRUE)
					ENDIF
				ENDIF			
			BREAK
		 ENDSWITCH
	ENDIF
ENDPROC

//╚═════════════════════════════════════════════════════════════════════════════╝	

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				WANTED															║

CONST_INT ciTIME_ALLOWED_FLY_LOW 	10000

PROC SET_WANTED_TO(INT iThisMuch)
	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
		IF SHOULD_RESTRICT_MAX_WANTED_LEVEL()
			INT iMaxSellWanted = GET_MAX_WANTED_LEVEL_FOR_SELL_CONTRABAND()
			IF iThisMuch > iMaxSellWanted
				PRINTLN("[BIKER_SELL] [SET_WANTED_TO] Want to set wanted level to ", iThisMuch, " but max wanted restricted to ", iMaxSellWanted)
				iThisMuch = iMaxSellWanted
			ENDIF
		ENDIF
		PRINTLN("[BIKER_SELL] [SET_WANTED_TO] iThisMuch = ", iThisMuch)
		SET_DISABLE_NO_WANTED_LEVEL_GAIN_ZONES(TRUE)
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), iThisMuch)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())	
	ENDIF
ENDPROC

CONST_INT ciPROVEN_WANTED 1

FUNC INT GET_WANTED_FROM_DIFFICULTY()
	SWITCH serverBD.eDifficulty
		CASE eDIFFICULTY_EASY		RETURN g_sMPTunables.iBIKER_SELL_EASY_WANTED_CAP
		CASE eDIFFICULTY_MEDIUM		RETURN g_sMPTunables.iBIKER_SELL_MEDIUM_WANTED_CAP
		CASE eDIFFICULTY_HARD		RETURN g_sMPTunables.iBIKER_SELL_HARD_WANTED_CAP
	ENDSWITCH
	
	RETURN 0
ENDFUNC

PROC MAINTAIN_WANTED_RATINGS()

	INT iCurrentWanted
	iCurrentWanted = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
	
	// Set the flag for changing to lose wanted objective
	IF BIKER_SELL_STING()
	OR BIKER_SELL_AIR_DROP_AT_SEA()
	OR BIKER_SELL_PROVEN()
	OR BIKER_SELL_CLUB_RUN()
		IF BIKER_SELL_STING()
		AND BIKER_SELL_HAS_TWIST_HAPPENED()
			IF NOT IS_LOCAL_BIT1_SET(eLOCALBITSET1_GIVEN_WANTED)
				SET_WANTED_TO(GET_WANTED_FROM_DIFFICULTY())
				SET_LOCAL_BIT1(eLOCALBITSET1_GIVEN_WANTED)
				PRINTLN("[BIKER_SELL] WANT SET_LOCAL_BIT1(eLOCALBITSET1_GIVEN_WANTED)")
			ELIF iCurrentWanted > 0
			AND NOT IS_LOCAL_BIT1_SET(eLOCALBITSET1_LOST_WANTED)
				SET_LOCAL_BIT1(eLOCALBITSET1_LOST_WANTED)
				PRINTLN("[BIKER_SELL] WANT SET_LOCAL_BIT1(eLOCALBITSET1_LOST_WANTED)")
			ELIF iCurrentWanted = 0
			AND IS_LOCAL_BIT1_SET(eLOCALBITSET1_LOST_WANTED)
				IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_LOST_WANTED)
					SET_CLIENT_BIT0(eCLIENTBITSET0_LOST_WANTED)
					PRINTLN("[BIKER_SELL] WANT SET_CLIENT_BIT0(eCLIENTBITSET0_LOST_WANTED)")
				ENDIF
			ENDIF
		ENDIF
		
		IF iCurrentWanted = 0
			IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_WANTED)
				CLEAR_CLIENT_BIT0(eCLIENTBITSET0_WANTED)
			ENDIF
		ELSE
			IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_WANTED)
				SET_CLIENT_BIT0(eCLIENTBITSET0_WANTED)
			ENDIF
		ENDIF
	ENDIF
	
	IF BIKER_SELL_PROVEN()
		IF NOT IS_LOCAL_BIT1_SET(eLOCALBITSET1_WANTED_PROVEN)
			IF HAS_NET_TIMER_STARTED(sLocaldata.stDrunk)
				IF HAS_NET_TIMER_EXPIRED(sLocaldata.stDrunk, serverBD.iWantedTimeElapsed) // See SERVER_SET_PROVEN_WANTED
					IF iCurrentWanted < ciPROVEN_WANTED
						IF NOT IS_BIT_SET(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bNoCopsActive)
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[0], serverBD.iRoute, 0)) > 100.00
								SET_WANTED_TO(GET_WANTED_FROM_DIFFICULTY())
								SET_LOCAL_BIT1(eLOCALBITSET1_WANTED_PROVEN)
								PRINTLN("[BIKER_SELL] - MAINTAIN_WANTED_RATINGS, ciPROVEN_WANTED, GIVEN WANTED LEVEL = ", ciPROVEN_WANTED)
							ELSE
								SET_LOCAL_BIT1(eLOCALBITSET1_WANTED_PROVEN)
								PRINTLN("[BIKER_SELL] - MAINTAIN_WANTED_RATINGS, ciPROVEN_WANTED, ALREADY_CLOSE_TO_DROP_OFF, DON'T GIVE RATING ")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF BIKER_SELL_AIR_DROP_AT_SEA()
	AND HAS_NET_TIMER_STARTED(sLocaldata.timeTooHigh)
	AND HAS_NET_TIMER_EXPIRED(sLocaldata.timeTooHigh, ciTIME_ALLOWED_FLY_LOW)
		// Give wanted rating if flying too high for too long
		IF iCurrentWanted < GET_WANTED_FROM_DIFFICULTY()
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bNoCopsActive)
				IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_GIVEN_WANTED_FOR_TOO_HIGH)
					SET_WANTED_TO(GET_WANTED_FROM_DIFFICULTY())
					SET_LOCAL_BIT0(eLOCALBITSET0_GIVEN_WANTED_FOR_TOO_HIGH)
					PRINTLN("[BIKER_SELL] - MAINTAIN_WANTED_RATINGS GIVEN WANTED LEVEL = ", GET_WANTED_FROM_DIFFICULTY())
				ENDIF
			ENDIF
		ENDIF
	ELSE	
		// url:bugstar:2768502 - Sell Missions: Can you please suppress the Wanted level across all Sell Missions (expect for when we script it e.g. Air Restricted etc.)
		// unless you kill a cop...
		IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_KILLED_COP)
			IF iCurrentWanted <> 0
				// url:bugstar:2768502 - Sell Missions: Can you please suppress the Wanted level across all Sell Missions (expect for when we script it e.g. Air Restricted etc.)
				IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_WANTED)
					SET_WANTED_TO(0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC eLOCAL_BITSET_1 GET_CLEAR_INVINCIBILITY_LOCAL_BIT(INT iVeh)
	SWITCH iVeh
		CASE 0 RETURN eLOCALBITSET1_VEH_0_INVINCIBILITY_CLEARED
		CASE 1 RETURN eLOCALBITSET1_VEH_1_INVINCIBILITY_CLEARED
		CASE 2 RETURN eLOCALBITSET1_VEH_2_INVINCIBILITY_CLEARED
		CASE 3 RETURN eLOCALBITSET1_VEH_3_INVINCIBILITY_CLEARED
	ENDSWITCH
	
	RETURN eLOCALBITSET1_VEH_0_INVINCIBILITY_CLEARED
ENDFUNC

PROC CLIENT_SELL_VEH_LOOP()
	INT i

	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
			IF IS_SELL_VEH_OK(i)
				SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON(NET_TO_VEH(serverBD.niVeh[i]), FALSE)
				
				IF BIKER_SELL_BENSON()
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh[i])
						IF IS_BIT_SET(serverBD.iDropVehPenaltyBitSet, i)
							IF AM_I_IN_THIS_SELL_VEH(i)
								EJECT_PLAYER_FROM_VEHICLE(i)
							ENDIF
							LOCK_SELL_VEH(i, TRUE)
							INVALIDATE_SELL_VEH(i)
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
			IF BIKER_SELL_BORDER_PATROL()
				IF GET_IS_BOAT_CAPSIZED(VEH_ID(i))
					IF IS_BOAT_ANCHORED(VEH_ID(i))
						SET_BOAT_ANCHOR(VEH_ID(i), FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_VEHICLE_ACCESS()
	INT i
	ENTITY_INDEX eiDropVeh

	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		// Sell Vehicle
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(TRUE)
			AND (GB_GET_LOCAL_PLAYER_GANG_BOSS() = serverBD.piLaunchBoss
			OR PLAYER_ID() = serverBD.piLaunchBoss)
				IF IS_SELL_VEH_OK(i)
					IF GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(VEH_ID(i), PLAYER_ID())
						PRINTLN("2806496, MAINTAIN_VEHICLE_ACCESS, GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER, UNLOCK ")
						SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(VEH_ID(i), PLAYER_ID(), FALSE)
					ENDIF
				ENDIF
			ELSE
				IF IS_SELL_VEH_OK(i)
					IF NOT GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(VEH_ID(i), PLAYER_ID())
						PRINTLN("2806496, MAINTAIN_VEHICLE_ACCESS, GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER, LOCK 1 ")
						SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(VEH_ID(i), PLAYER_ID(), TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			// Check invincibility
			IF HAS_ANY_SELL_VEHICLE_BEEN_ENTERED()
				IF NOT IS_LOCAL_BIT1_SET(GET_CLEAR_INVINCIBILITY_LOCAL_BIT(i))
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh[i])
					OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.niVeh[i]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
						IF TAKE_CONTROL_OF_NET_ID(serverBD.niVeh[i])
							SET_ENTITY_INVINCIBLE(NET_TO_VEH(serverBD.niVeh[i]), FALSE)
							PRINTLN("MAINTAIN_VEHICLE_ACCESS, SET_ENTITY_INVINCIBLE, FALSE for i = ", i)
							SET_LOCAL_BIT1(GET_CLEAR_INVINCIBILITY_LOCAL_BIT(i))
//						ELSE
//							PRINTLN("2806496, ERROR, TAKE_CONTROL_OF_NET_ID, i = ", i)
						ENDIF
//					ELSE
//						PRINTLN("2806496, ERROR, NETWORK_HAS_CONTROL_OF_NETWORK_ID, i = ", i)
					ENDIF
//				ELSE
//					PRINTLN("2806496, ERROR, GET_CLEAR_INVINCIBILITY_LOCAL_BIT, i = ", i)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Drop off vehicle
	i = 0
	VEHICLE_INDEX vehDrop
	REPEAT GET_NUM_DROP_OFF_VEH_FOR_VARIATION() i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffVeh[i])
			vehDrop = NET_TO_VEH(serverBD.niDropOffVeh[i])
			eiDropVeh = NET_TO_ENT(serverBD.niDropOffVeh[i])
			IF IS_ENTITY_ALIVE(vehDrop)
				IF NOT GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(vehDrop, PLAYER_ID())
					PRINTLN("2806496, MAINTAIN_VEHICLE_ACCESS, GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER, LOCK 2 ")
					SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(vehDrop, PLAYER_ID(), TRUE)
				ENDIF
				
				IF BIKER_SELL_BENSON()
						
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niDropOffVeh[i])
					
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iDropVehStuck, i)
							IF IS_VEHICLE_STUCK_TIMER_UP(vehDrop, VEH_STUCK_ON_ROOF, 10000)
							OR IS_VEHICLE_STUCK_TIMER_UP(vehDrop, VEH_STUCK_ON_SIDE, 10000)
							OR IS_ENTITY_IN_WATER(eiDropVeh)	
								PRINTLN("[BENSON] MAINTAIN_VEHICLE_ACCESS, iDropVehStuck, i = ", i)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iDropVehStuck, i)
							ENDIF
						ENDIF
										
						IF NOT IS_BIT_SET(serverBD.iDropOffVehDoneBitSet, i)
							SET_VEHICLE_DOOR_LATCHED(vehDrop, SC_DOOR_BOOT, FALSE, FALSE)
							SET_VEHICLE_DOOR_AUTO_LOCK(vehDrop, SC_DOOR_BOOT, FALSE)
							SET_VEHICLE_DOOR_OPEN(vehDrop, SC_DOOR_BOOT, FALSE, TRUE)
//							SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehDrop, SC_DOOR_BOOT, FALSE)
//							PRINTLN("BIKER_SELL_BENSON, MAINTAIN_VEHICLE_ACCESS, BACK_DOOR OPEN ", i)
						ELSE
							IF NOT AM_I_IN_REAR_OF_TRUCK(i)
							AND NOT IS_ENTITY_ON_SCREEN(eiDropVeh)
								SET_VEHICLE_DOOR_LATCHED(vehDrop, SC_DOOR_BOOT, TRUE, TRUE)
								SET_VEHICLE_DOOR_AUTO_LOCK(vehDrop, SC_DOOR_BOOT, TRUE)
								SET_VEHICLE_DOOR_SHUT(vehDrop, SC_DOOR_BOOT, TRUE)
								SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehDrop, ENUM_TO_INT(SC_DOOR_BOOT), VEHICLELOCK_LOCKED)
//								PRINTLN("BIKER_SELL_BENSON, MAINTAIN_VEHICLE_ACCESS, BACK_DOOR SHUT ", i)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF DOES_VARIATION_HAVE_AI_VEHICLES()
		i=0
		REPEAT GET_NUM_ATTACK_VEHICLES_FOR_VARIATION() i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[i].netIndex)
				IF IS_ENTITY_ALIVE(NET_TO_ENT(serverBD.sPedVeh[i].netIndex))
					IF NOT GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.sPedVeh[i].netIndex), PLAYER_ID())
						PRINTLN("2806496, MAINTAIN_VEHICLE_ACCESS, GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER, LOCK 3 ")
						SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.sPedVeh[i].netIndex), PLAYER_ID(), TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	
	ENDIF
ENDPROC

PROC MAINTAIN_MUSIC_AND_SOUNDS()
	IF NOT IS_BIT_SET(sLocaldata.iSoundBitSet, ciSOUND_BIT_TRIGGERED_5S)
		// Arbitrary 30s so that it doesnt randomly trigger as soon as it launches
		IF HAS_NET_TIMER_STARTED(serverBD.modeTimer)
			IF HAS_NET_TIMER_EXPIRED(serverBD.modeTimer, 30000)
				IF sLocalData.iTimeLeft > 0
				AND sLocalData.iTimeLeft <= 5000
					PLAY_SOUND_FRONTEND(-1, "5s", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
					SET_BIT(sLocaldata.iSoundBitSet, ciSOUND_BIT_TRIGGERED_5S)
					PRINTLN("[BIKER_SELL] [SOUND], MAINTAIN_MUSIC_AND_SOUNDS - 5s countdown SFX triggered.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_ADDITIONAL_HELP()
//	IF NOT IS_BIT_SET(sLocaldata.iHelpBitSet, ciHELP_REMOVAL)
//		SET_BIT(sLocaldata.iHelpBitSet, ciHELP_REMOVAL)
//		GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_DO_COMPLETE_SHIPMENT_HELP)
//	ENDIF
	IF BIKER_SELL_PROVEN()
		IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_0_ENTERED_ONCE)	
			IF IS_DRIVER_OF_ANY_SELL_VEH()
				GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_PROVEN_STONED)
			ELSE
				GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_PROVEN_STONED_PASS)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC CLEANUP_CUSTOM_SPAWNS(BOOL bIgnoreBitSet = FALSE)
	IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_CUSTOM_SPAWN_POINTS_ACTIVATED)
	OR bIgnoreBitSet
		IF NOT IS_PLAYER_RESPAWNING(PLAYER_ID())
			CLEAR_LOCAL_BIT0(eLOCALBITSET0_CUSTOM_SPAWN_POINTS_ACTIVATED)
			CLEAR_LOCAL_BIT0(eLOCALBITSET0_CUSTOM_SPAWN_POINTS_ADDED)
			CLEAR_CUSTOM_SPAWN_POINTS()
			SET_PLAYER_RESPAWN_IN_VEHICLE(FALSE)
			USE_CUSTOM_SPAWN_POINTS(FALSE)
			PRINTLN("[BIKER_SELL], CLEANUP_CUSTOM_SPAWNS - called")
			PRINTLN("[BIKER_SELL], CLEANUP_CUSTOM_SPAWNS - bIgnoreBitSet = ", GET_STRING_FROM_BOOL(bIgnoreBitSet))
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_VEHICLE_CRATES()
	IF DOES_VARIATION_REQUIRE_CRATES_IN_VEHICLES()
		INT i
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVehicleCrate[i])
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVehicleCrate[i])
				OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.niVehicleCrate[i]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
					IF TAKE_CONTROL_OF_NET_ID(serverBD.niVehicleCrate[i])
						NETWORK_FADE_OUT_ENTITY(NET_TO_ENT(serverBD.niVehicleCrate[i]), TRUE, TRUE)
						CLEANUP_NET_ID(serverBD.niVehicleCrate[i])
						PRINTLN("[BIKER_SELL], CLEANUP_VEHICLE_CRATES, cleaned up crate ", i)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

CONST_INT ciSTUCK_TIME_LIMIT 20000
CONST_INT ciSTUCK_TIME_LIMIT_TRASH 40000
CONST_INT ciSTUCK_TIME_LIMIT_SHORT 5000
CONST_INT iSTUCK_AFTER_CREATION_MAX_TIME 20000

PROC VEHICLE_STUCK_CHECKS()
	
	INT iStuckTimeToCheck 		= ciSTUCK_TIME_LIMIT
	INT iJammedStuckTimeToCheck = ciSTUCK_TIME_LIMIT
	
	IF IS_THIS_A_LAND_VARIATION()	
	OR IS_THIS_AN_AIR_VARIATION()
	OR IS_THIS_A_SEA_VARIATION()
		INT i
		INT iClientBitToCheck
		eCLIENT_BITSET_0 clientBitToCheck
		BOOL bStuck[ciMAX_SELL_VEHS] 
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			iClientBitToCheck = ENUM_TO_INT(eCLIENTBITSET0_VEH0_STUCK) + i
			clientBitToCheck = INT_TO_ENUM(eCLIENT_BITSET_0, iClientBitToCheck)
			
			iStuckTimeToCheck = ciSTUCK_TIME_LIMIT
			
			IF IS_THIS_A_LAND_VARIATION()
				SWITCH i
					CASE 0
						IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_0_ENTERED_ONCE)	
							iStuckTimeToCheck = ciSTUCK_TIME_LIMIT_SHORT
						ENDIF
					BREAK
					
					CASE 1
						IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_1_ENTERED_ONCE)	
							iStuckTimeToCheck = ciSTUCK_TIME_LIMIT_SHORT
						ENDIF
					BREAK
					
					CASE 2
						IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_2_ENTERED_ONCE)	
							iStuckTimeToCheck = ciSTUCK_TIME_LIMIT_SHORT
						ENDIF
					BREAK
					CASE 3
						IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_3_ENTERED_ONCE)	
							iStuckTimeToCheck = ciSTUCK_TIME_LIMIT_SHORT
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			
			// url:bugstar:2975114 - Weed Sell - Trashmaster - mission ended abruptly for all players with no perceptible reason for failing the mission
			IF BIKER_SELL_TRASHMASTER()
//			OR BIKER_SELL_TACO_VAN()
				iJammedStuckTimeToCheck = ciSTUCK_TIME_LIMIT_TRASH
			ENDIF
			
			IF IS_ENTITY_ALIVE(VEH_ID(i))
				IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(),clientBitToCheck )
					IF NOT IS_BIT_SET(serverBD.iVehStuckSinceCreatedBitset, i)
						IF NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(i)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(VEH_ID(i))
								IF IS_VEHICLE_STUCK_TIMER_UP(VEH_ID(i), VEH_STUCK_ON_ROOF, iStuckTimeToCheck)
									PRINTLN("[BIKER_SELL] [VEHICLE_STUCK_CHECKS] Think Sell Veh, ", i, " is stuck! - VEH_STUCK_ON_ROOF ", iStuckTimeToCheck)
									bStuck[i] = TRUE
								ELIF IS_VEHICLE_STUCK_TIMER_UP(VEH_ID(i), VEH_STUCK_ON_SIDE, iStuckTimeToCheck)
									PRINTLN("[BIKER_SELL] [VEHICLE_STUCK_CHECKS] Think Sell Veh, ", i, " is stuck! - VEH_STUCK_ON_SIDE ", iStuckTimeToCheck)
									bStuck[i] = TRUE
								ELIF IS_VEHICLE_STUCK_TIMER_UP(VEH_ID(i), VEH_STUCK_HUNG_UP, iStuckTimeToCheck)
									PRINTLN("[BIKER_SELL] [VEHICLE_STUCK_CHECKS] Think Sell Veh, ", i, " is stuck! - VEH_STUCK_HUNG_UP ", iStuckTimeToCheck)
									bStuck[i] = TRUE
								ELIF IS_VEHICLE_STUCK_TIMER_UP(VEH_ID(i), VEH_STUCK_JAMMED, iJammedStuckTimeToCheck)
									PRINTLN("[BIKER_SELL] [VEHICLE_STUCK_CHECKS] Think Sell Veh, ", i, " is stuck! - VEH_STUCK_JAMMED ", iStuckTimeToCheck)
									bStuck[i] = TRUE
									
								ELIF iStuckTimeToCheck = ciSTUCK_TIME_LIMIT_SHORT
									IF NOT IS_MOTOR_BIKE_SELL_VEHICLE()//-- Additional check for the vehicle not being on all wheels shortly after spawning.
										IF NOT HAS_NET_TIMER_STARTED(serverBD.stVehicleStuckTimer[i])
											IF NOT IS_VEHICLE_ON_ALL_WHEELS(VEH_ID(i))
												START_NET_TIMER(serverBD.stVehicleStuckTimer[i])
												PRINTLN("[BIKER_SELL] [VEHICLE_STUCK_CHECKS] [CREATE_SELL_VEH] Think Sell Veh, ", i, " is not on all wheels - start timer")
											ENDIF
										ELSE
											IF NOT IS_VEHICLE_ON_ALL_WHEELS(VEH_ID(i))
												IF HAS_NET_TIMER_EXPIRED(serverBD.stVehicleStuckTimer[i], iStuckTimeToCheck)
													PRINTLN("[BIKER_SELL] [VEHICLE_STUCK_CHECKS] [CREATE_SELL_VEH] Think Sell Veh, ", i, " is stuck! - IS_VEHICLE_ON_ALL_WHEELS")
													bStuck[i] = TRUE
												ENDIF
											ELSE
												PRINTLN("[BIKER_SELL] [VEHICLE_STUCK_CHECKS] [CREATE_SELL_VEH] Think Sell Veh, ", i, " is on all wheels - reset timer")
												RESET_NET_TIMER(serverBD.stVehicleStuckTimer[i])
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF bStuck[i]
						IF IS_THIS_AN_AIR_VARIATION()
						OR IS_THIS_A_SEA_VARIATION()
							IF TAKE_CONTROL_OF_ENTITY(VEH_ID(i))
								PRINTLN("[BIKER_SELL] [VEHICLE_STUCK_CHECKS], NETWORK_EXPLODE_VEHICLE ", i)
								SET_ENTITY_CAN_BE_DAMAGED(VEH_ID(i), TRUE)
								SET_ENTITY_INVINCIBLE(VEH_ID(i), FALSE)
								NETWORK_EXPLODE_VEHICLE(VEH_ID(i))
							ENDIF
						ELSE
							SET_CLIENT_BIT0(clientBitToCheck)		
							
							SET_VEHICLE_ENGINE_ON(VEH_ID(i), FALSE, TRUE) 
							SET_VEHICLE_ENGINE_HEALTH(VEH_ID(i), 0)
							SET_VEHICLE_PETROL_TANK_HEALTH(VEH_ID(i), 0)
							PRINTLN("[BIKER_SELL] [VEHICLE_STUCK_CHECKS], SET_VEHICLE_ENGINE_OFF ", i)
						ENDIF
					ENDIF					
				ELSE
					IF IS_BIT_SET(serverBD.iVehStuckSinceCreatedBitset, i)
						CLEAR_CLIENT_BIT0(clientBitToCheck)
						PRINTLN("[BIKER_SELL] [VEHICLE_STUCK_CHECKS] Clearing stuck check for Veh ", i, " as server is warping veh")
					ENDIF
				ENDIF
				
				
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

FUNC BOOL ARE_ALL_ENEMIES_DEAD()
	INT iPed
	REPEAT GET_NUM_PEDS_FOR_VARIATION() iPed
		IF DOES_AI_PED_EXIST(iPed)
			IF NOT IS_ENTITY_DEAD(GET_AI_PED_PED_INDEX(iPed))
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC

PROC CLEAR_ANY_MISSION_HELP()

	IF IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED(sDROP_BTN_TEXT(), GET_SHIPMENT_TYPE_STRING())
	OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("SBIKER_LAND", GET_SHIPMENT_TYPE_STRING())
	OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("BIKER_TOOLOW", GET_SHIPMENT_TYPE_STRING())
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_RP_H1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_RP_H2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP11")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP10")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP9")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP12")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP8")
		CLEAR_HELP(TRUE)
	ENDIF	
ENDPROC

FUNC BOOL DOES_SELL_MODE_HAVE_SCORE()
	RETURN TRUE
ENDFUNC

//Set any audio flags on mode start
PROC SET_AUDIO_FLAGS_ON_MUSIC_START()
	IF NOT IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_SET_UP_DONE)
		IF DOES_SELL_MODE_HAVE_SCORE()
			PRINTLN("[BIKER_SELL] [SELL AUDIO] - [CTRB_SELL] SET_AUDIO_FLAGS_ON_MUSIC_START - SET_AUDIO_FLAG(\"DisableFlightMusic\", TRUE) ")
			PRINTLN("[BIKER_SELL] [SELL AUDIO] - [CTRB_SELL] SET_AUDIO_FLAGS_ON_MUSIC_START - SET_AUDIO_FLAG(\"WantedMusicDisabled\", TRUE) ")
			SET_AUDIO_FLAG("DisableFlightMusic", TRUE) 
			SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
		ENDIF
		SET_LOCAL_AUDIO_BIT0(eSELL_AUDIO_TRIGGER_SET_UP_DONE)
	ENDIF
ENDPROC

//Set any audio triggers that are needed. 
PROC SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGERS eTrigger)
	
	//If we've not already set the audio flag then trigger what we need
	IF NOT IS_LOCAL_AUDIO_BIT0_SET(eTrigger)
		PRINTLN("[BIKER_SELL] [SELL AUDIO] - [CTRB_SELL] SET_SELL_AUDIO_MUSIC_TRIGGERS - TRIGGER_MUSIC_EVENT(", GET_AUDIO_TRIGGER_DEBUG_STRING(eTrigger), ")")
		TRIGGER_MUSIC_EVENT(GET_AUDIO_TRIGGER_STRING(eTrigger))
		SET_LOCAL_AUDIO_BIT0(eTrigger)
	ENDIF
ENDPROC

FUNC eSELL_AUDIO_TRIGGERS GET_PASS_OR_FAIL_AUDIO_TRIGGER_FOR_VAR(BOOL bPassed)
	IF bPassed
		SWITCH GET_SELL_VAR()
			CASE eBIKER_SELL_TRASHMASTER
			CASE eBIKER_SELL_PROVEN
			CASE eBIKER_SELL_AIR_DROP_AT_SEA
			CASE eBIKER_SELL_BENSON
			CASE eBIKER_SELL_FRIENDS_IN_NEED
			CASE eBIKER_SELL_BORDER_PATROL
			CASE eBIKER_SELL_POSTMAN
			CASE eBIKER_SELL_HELICOPTER_DROP
			CASE eBIKER_SELL_STING_OP
			CASE eBIKER_SELL_DEFAULT
			CASE eBIKER_SELL_RACE
			CASE eBIKER_SELL_CLUB_RUN
			CASE eBIKER_SELL_BAG_DROP
				RETURN eSELL_AUDIO_TRIGGER_BIKER_MP_MUSIC_STOP
		ENDSWITCH
	ELSE
		SWITCH GET_SELL_VAR()
			CASE eBIKER_SELL_TRASHMASTER
			CASE eBIKER_SELL_PROVEN
			CASE eBIKER_SELL_AIR_DROP_AT_SEA
			CASE eBIKER_SELL_BENSON
			CASE eBIKER_SELL_FRIENDS_IN_NEED
			CASE eBIKER_SELL_BORDER_PATROL
			CASE eBIKER_SELL_POSTMAN
			CASE eBIKER_SELL_HELICOPTER_DROP
			CASE eBIKER_SELL_DEFAULT
			CASE eBIKER_SELL_STING_OP
			CASE eBIKER_SELL_RACE
			CASE eBIKER_SELL_CLUB_RUN
			CASE eBIKER_SELL_BAG_DROP
				RETURN eSELL_AUDIO_TRIGGER_BIKER_MP_MUSIC_FAIL
		ENDSWITCH
	ENDIF
	RETURN eSELL_AUDIO_TRIGGER_BIKER_MP_MUSIC_STOP
ENDFUNC


//Clean up any audio on mode end
PROC CLEAR_AUDIO_FLAGS_ON_MODE_END()
	IF NOT IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_CLEARED_ON_MODE_END)
		IF DOES_SELL_MODE_HAVE_SCORE()
			PRINTLN("[BIKER_SELL] [SELL AUDIO] - [CTRB_SELL] CLEAR_AUDIO_FLAGS_ON_MODE_END")
			IF IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_SET_UP_DONE)
				// Only do these triggers if we've not hit the 30s countdown
				IF eFinalCountdownStage = eFCOUNTDOWN_PREP
					IF GET_END_REASON() = eENDREASON_WIN_CONDITION_TRIGGERED
						SET_SELL_AUDIO_MUSIC_TRIGGERS(GET_PASS_OR_FAIL_AUDIO_TRIGGER_FOR_VAR(TRUE))
					ELSE
						SET_SELL_AUDIO_MUSIC_TRIGGERS(GET_PASS_OR_FAIL_AUDIO_TRIGGER_FOR_VAR(FALSE))
					ENDIF
				ENDIF
				PRINTLN("[BIKER_SELL] [SELL AUDIO] - [CTRB_SELL]CLEAR_AUDIO_FLAGS_ON_MODE_END - SET_AUDIO_FLAG(\"DisableFlightMusic\", FALSE) eBIKER_SELL_AIR_MAIL")
				PRINTLN("[BIKER_SELL] [SELL AUDIO] - [CTRB_SELL]CLEAR_AUDIO_FLAGS_ON_MODE_END - SET_AUDIO_FLAG(\"WantedMusicDisabled\", FALSE) eBIKER_SELL_AIR_MAIL")
				SET_AUDIO_FLAG("DisableFlightMusic", FALSE) 
				SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
			ENDIF
		ENDIF
		SET_LOCAL_AUDIO_BIT0(eSELL_AUDIO_TRIGGER_CLEARED_ON_MODE_END)
	ENDIF
ENDPROC

PROC TRIGGER_AUDIO_MUSIC_ON_WIN()
	IF DOES_SELL_MODE_HAVE_SCORE()
		PRINTLN("[BIKER_SELL] [SELL AUDIO] - [CTRB_SELL] TRIGGER_AUDIO_MUSIC_ON_WIN")
		SWITCH GET_SELL_VAR()
			CASE eBIKER_SELL_TRASHMASTER
			CASE eBIKER_SELL_PROVEN
			CASE eBIKER_SELL_AIR_DROP_AT_SEA
			CASE eBIKER_SELL_BENSON
			CASE eBIKER_SELL_FRIENDS_IN_NEED
			CASE eBIKER_SELL_BORDER_PATROL
			CASE eBIKER_SELL_POSTMAN
			CASE eBIKER_SELL_HELICOPTER_DROP
			CASE eBIKER_SELL_STING_OP
			CASE eBIKER_SELL_DEFAULT
			CASE eBIKER_SELL_RACE
			CASE eBIKER_SELL_CLUB_RUN
			CASE eBIKER_SELL_BAG_DROP
				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_BIKER_MP_MUSIC_STOP)
			BREAK
			DEFAULT
				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_BIKER_MP_MUSIC_STOP)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC BOOL ARE_ALL_AIR_ATTACK_HELIS_DEAD()
	INT iVeh
	IF serverBD.fNumWavesSpawned >= GET_MAX_AMBUSH_WAVES_FOR_VARIATION()
		REPEAT GET_NUM_ATTACK_VEHICLES_FOR_VARIATION() iVeh
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[iVeh].netIndex)
				RETURN FALSE
			ENDIF
		ENDREPEAT
	ELSE
		RETURN FALSE
	ENDIF
	RETURN TRUE	
ENDFUNC
FUNC BOOL ARE_ALL_ATTACK_PEDS_DEAD()
	INT iPed
	IF serverBD.fNumWavesSpawned >= GET_MAX_AMBUSH_WAVES_FOR_VARIATION()
		REPEAT GET_NUM_PEDS_FOR_VARIATION() iPed
			IF DOES_AI_PED_EXIST(iPed)
			AND NOT IS_PED_INJURED(GET_AI_PED_PED_INDEX(iPed))
				RETURN FALSE
			ENDIF
		ENDREPEAT
	ELSE
		RETURN FALSE
	ENDIF
	RETURN TRUE	
ENDFUNC

PROC MAINTAIN_COUNTDOWN_MUSIC(INT iTimeRemaining, INT iOvertime)
	SWITCH eFinalCountdownStage
		CASE eFCOUNTDOWN_PREP
			// IF within a second of hitting the end prep period
			IF iTimeRemaining <= (35000)
			AND iTimeRemaining >= (33000)
				IF PREPARE_MUSIC_EVENT("FM_PRE_COUNTDOWN_30S")
					PRINTLN("[BIKER_SELL]  - MAINTAIN_COUNTDOWN_MUSIC - eFCOUNTDOWN_PREP - FM_PRE_COUNTDOWN_30S has been triggered")
					SET_AUDIO_FLAG("DisableFlightMusic", TRUE)
					TRIGGER_MUSIC_EVENT("FM_PRE_COUNTDOWN_30S")
					eFinalCountdownStage = eFCOUNTDOWN_START
				ENDIF
			ENDIF
		BREAK

		CASE eFCOUNTDOWN_START
			IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_TriggeredCountdown)
				IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_SuppressWanted)
					IF iTimeRemaining <= 33000
					AND iTimeRemaining > 30000
						SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
						PRINTLN("[BIKER_SELL]  - MAINTAIN_COUNTDOWN_MUSIC - eFCOUNTDOWN_START - suppressing wanted music")
						SET_BIT(iFinalCountdownBitSet, iFCBS_SuppressWanted)
					ENDIF
				ENDIF
				IF iTimeRemaining <= 30000
					IF PREPARE_MUSIC_EVENT("FM_COUNTDOWN_30S")
						SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
						SET_USER_RADIO_CONTROL_ENABLED(FALSE)
						TRIGGER_MUSIC_EVENT("FM_COUNTDOWN_30S")
						PREPARE_MUSIC_EVENT("FM_COUNTDOWN_30S_KILL")
						PRINTLN("[BIKER_SELL]  - MAINTAIN_COUNTDOWN_MUSIC - eFCOUNTDOWN_START - APT_COUNTDOWN_30S has been triggered")
						SET_BIT(iFinalCountdownBitSet, iFCBS_TriggeredCountdown)
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_ReEnabledRadioControl)
					IF iTimeRemaining <= 27000
						SET_USER_RADIO_CONTROL_ENABLED(TRUE)
						SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
						PRINTLN("[BIKER_SELL]  - MAINTAIN_COUNTDOWN_MUSIC - eFCOUNTDOWN_START - Radio control re-enabled")
						SET_BIT(iFinalCountdownBitSet, iFCBS_ReEnabledRadioControl)
						eFinalCountdownStage = eFCOUNTDOWN_END_OR_KILL
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE eFCOUNTDOWN_END_OR_KILL
			IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_TriggeredCountdownKill)
				IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_Trigger5s)
					IF iTimeRemaining <= 5000
						PLAY_SOUND_FRONTEND(-1, "5s", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
						SET_BIT(iFinalCountdownBitSet, iFCBS_Trigger5s)
					ENDIF
				ENDIF

				IF iTimeRemaining <= 0
					IF PREPARE_MUSIC_EVENT("FM_COUNTDOWN_30S_FIRA")
						TRIGGER_MUSIC_EVENT("FM_COUNTDOWN_30S_FIRA")
						CANCEL_MUSIC_EVENT("FM_COUNTDOWN_30S_KILL")
						PRINTLN("[BIKER_SELL]  - MAINTAIN_COUNTDOWN_MUSIC - eFCOUNTDOWN_END_OR_KILL - Time ran out, FM_COUNTDOWN_30S_FIRA has been triggered")
						SET_BIT(iFinalCountdownBitSet, iFCBS_TriggeredCountdownKill)
					ENDIF
				ELSE
					IF GET_END_REASON() = eENDREASON_NO_BOSS_LEFT 
						IF PREPARE_MUSIC_EVENT("FM_COUNTDOWN_30S_FIRA")
							CANCEL_MUSIC_EVENT("FM_COUNTDOWN_30S")
							TRIGGER_MUSIC_EVENT("FM_COUNTDOWN_30S_KILL")
							TRIGGER_MUSIC_EVENT("FM_COUNTDOWN_30S_FIRA")
							PRINTLN("[BIKER_SELL]  - MAINTAIN_COUNTDOWN_MUSIC - eFCOUNTDOWN_END_OR_KILL - Mode ended before timeout, FM_COUNTDOWN_30S_KILL and FM_COUNTDOWN_30S_FIRA have been triggered")
							SET_BIT(iFinalCountdownBitSet, iFCBS_TriggeredCountdownKill)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF IS_BIT_SET(iFinalCountdownBitSet, iFCBS_SuppressWanted)
				IF iOvertime <= -2000
					SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
					SET_AUDIO_FLAG("DisableFlightMusic", FALSE)
					PRINTLN("[BIKER_SELL]  - MAINTAIN_COUNTDOWN_MUSIC - eFCOUNTDOWN_END_OR_KILL - unsuppressing wanted music")
					CLEAR_BIT(iFinalCountdownBitSet, iFCBS_SuppressWanted)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL ARE_ALL_BIKER_SELL_CHASE_PEDS_DEAD()
	INT iped
	REPEAT GET_NUM_PEDS_FOR_VARIATION() iPed
		IF GET_PED_STATE(iPed) != ePEDSTATE_DEAD
			RETURN FALSE
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC


//INT iVanAudioCache

//this is called every frame
PROC MAINTAIN_AUDIO_MUSIC_TRIGGERS()
	INT iTimeRemaining, iOvertime
	
	iTimeRemaining = ( iTIME_LIMIT() - ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), serverBD.modeTimer.Timer)))
	IF iTimeRemaining <= 0
		iOvertime = iTimeRemaining
		iTimeRemaining = 0
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(serverBD.modeTimer)
	AND iTimeRemaining < 35000
	AND iTimeRemaining > 0
		MAINTAIN_COUNTDOWN_MUSIC(iTimeRemaining, iOvertime)
	ELSE
		SWITCH GET_SELL_VAR()
			CASE eBIKER_SELL_TRASHMASTER
				IF IS_DRIVER_OF_ANY_SELL_VEH()
					SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_TRASHMASTER_START)
				ENDIF
			BREAK
			
			CASE eBIKER_SELL_DEFAULT
				IF IS_DRIVER_OF_ANY_SELL_VEH()
					SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_CONVOY_START)
				ENDIF
			BREAK
			
//			CASE eBIKER_SELL_EXCHANGE
//				IF NOT BIKER_SELL_HAS_TWIST_HAPPENED()
//					IF IS_DRIVER_OF_ANY_SELL_VEH()
//						SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_EXCHANGE_START)
//					ENDIF
//				ELSE
//					IF IS_DRIVER_OF_ANY_SELL_VEH()
//						SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_EXCHANGE_SWAP_VEHICLE)
//					ENDIF
//				ENDIF
//			BREAK
			
//			CASE eBIKER_SELL_TACO_VAN
//				IF IS_DRIVER_OF_ANY_SELL_VEH()
//					IF NOT IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_TACO_VAN_START)
//						SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_TACO_VAN_START)
//						iVanAudioCache = GET_MY_SELL_VEH_AS_INT()
//					//whenever player gets in a subsequent van to continue delivering
//					ELIF iVanAudioCache != GET_MY_SELL_VEH_AS_INT()
//						SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_TACO_VAN_DELIVERING)
//						IF IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_TACO_VAN_DELIVERED)
//							CLEAR_LOCAL_AUDIO_BIT0(eSELL_AUDIO_TRIGGER_TACO_VAN_DELIVERED)
//						ENDIF
//						iVanAudioCache = GET_MY_SELL_VEH_AS_INT()
//					//whenever player stops at a location and triggers the taco music (silent mood to avoid score clashing)
//					ELIF GET_SELL_VEH_I_AM_IN_INT() <> -1
//					AND	bVEH_DOOR_OPEN(PARTICIPANT_ID_TO_INT(), GET_SELL_VEH_I_AM_IN_INT())
//						SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_TACO_VAN_DELIVERED)
//					ENDIF
//				ENDIF
//						
//			BREAK
			
			CASE eBIKER_SELL_AIR_DROP_AT_SEA
				IF IS_DRIVER_OF_ANY_SELL_VEH()
					SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_AIRDROP_SEA_START)
				ENDIF
			BREAK
			CASE eBIKER_SELL_FRIENDS_IN_NEED
				IF NOT IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_FRIENDS_IN_NEED_START)
					IF NOT IS_PLAYER_IN_FACTORY(PLAYER_ID())
						SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_FRIENDS_IN_NEED_START)
					ENDIF
				ENDIF
			BREAK
			CASE eBIKER_SELL_BORDER_PATROL
				IF IS_DRIVER_OF_ANY_SELL_VEH()
					SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_BORDER_PATROL_START)
				ENDIF
			BREAK
			CASE eBIKER_SELL_BENSON
				IF IS_DRIVER_OF_ANY_SELL_VEH()
					SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_RUBBLE_START)
				ENDIF
			BREAK
			CASE eBIKER_SELL_HELICOPTER_DROP
				IF IS_DRIVER_OF_ANY_SELL_VEH()
					SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_BORDER_HELI_DROP_START)
				ENDIF
			BREAK
			CASE eBIKER_SELL_POSTMAN
				IF IS_DRIVER_OF_ANY_SELL_VEH()
					SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_POSTMAN_START)
				ENDIF
			BREAK
			
			CASE eBIKER_SELL_PROVEN
				IF IS_DRIVER_OF_ANY_SELL_VEH()
					IF NOT IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_PROVEN_START)
						SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_PROVEN_START)
					ELIF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						IF IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_PROVEN_WANTED)
							CLEAR_LOCAL_AUDIO_BIT0(eSELL_AUDIO_TRIGGER_PROVEN_WANTED)
							SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_PROVEN_DELIVERING)
							PRINTLN("[BIKER_SELL] - eBIKER_SELL_PROVEN - CLEAR_LOCAL_AUDIO_BIT0(eSELL_AUDIO_TRIGGER_PROVEN_WANTED)")
						ENDIF
					ELSE
						IF IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_PROVEN_DELIVERING)
							CLEAR_LOCAL_AUDIO_BIT0(eSELL_AUDIO_TRIGGER_PROVEN_DELIVERING)
							PRINTLN("[BIKER_SELL] - eBIKER_SELL_PROVEN - CLEAR_LOCAL_AUDIO_BIT0(eSELL_AUDIO_TRIGGER_PROVEN_DELIVERING)")
						ENDIF
						SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_PROVEN_WANTED)
					ENDIF
				ENDIF
			BREAK
			
			CASE eBIKER_SELL_STING_OP
				IF IS_DRIVER_OF_ANY_SELL_VEH()
					IF NOT IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_STING_OP_START)
						SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_STING_OP_START)
					ELIF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						IF IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_STING_OP_WANTED)
							CLEAR_LOCAL_AUDIO_BIT0(eSELL_AUDIO_TRIGGER_STING_OP_WANTED)
							PRINTLN("[BIKER_SELL] - eBIKER_SELL_PROVEN - CLEAR_LOCAL_AUDIO_BIT0(eSELL_AUDIO_TRIGGER_STING_OP_WANTED)")
							SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_STING_OP_DELIVERING)
						ENDIF
					ELSE
						IF IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_STING_OP_DELIVERING)
							CLEAR_LOCAL_AUDIO_BIT0(eSELL_AUDIO_TRIGGER_STING_OP_DELIVERING)
							PRINTLN("[BIKER_SELL] - eBIKER_SELL_PROVEN - CLEAR_LOCAL_AUDIO_BIT0(eSELL_AUDIO_TRIGGER_STING_OP_DELIVERING)")
						ENDIF
						SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_STING_OP_WANTED)
					ENDIF
				ENDIF
			BREAK
			
			CASE eBIKER_SELL_RACE
				IF NOT IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_RACE_START)
					IF NOT IS_PLAYER_IN_FACTORY(PLAYER_ID())
						SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_RACE_START)
					ENDIF
				ENDIF
			BREAK
			CASE eBIKER_SELL_CLUB_RUN
				IF NOT IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_CLUB_RUN_START)
					IF NOT IS_PLAYER_IN_FACTORY(PLAYER_ID())
						SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_CLUB_RUN_START)
					ENDIF
				ELIF BIKER_SELL_HAS_TWIST_HAPPENED()
					IF NOT IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_CLUB_RUN_WANTED)
						SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_CLUB_RUN_WANTED)
					ELIF NOT IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_CLUB_RUN_COPS_LOST)
					AND ARE_ALL_BIKER_SELL_CHASE_PEDS_DEAD()	
						SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_CLUB_RUN_COPS_LOST)
					ENDIF
				ENDIF
			BREAK
			CASE eBIKER_SELL_BAG_DROP
				IF NOT IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_DROP_START)
					IF NOT IS_PLAYER_IN_FACTORY(PLAYER_ID())
						SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_DROP_START)
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC MAINTAIN_BORDER_PATROL_CHECKPOINT_TIMER()
	IF BIKER_SELL_BORDER_PATROL()
		IF IS_PLAYER_IN_ANY_SELL_VEH()
			INT iMyVeh = GET_SELL_VEH_I_AM_IN_INT()
			IF iMyVeh != -1
				IF NOT HAS_NET_TIMER_STARTED(playerBD[PARTICIPANT_ID_TO_INT()].BorderPatrolCheckpointTimer[iMyVeh])
					IF IS_DRIVER_OF_SELL_VEH(iMyVeh)
						START_NET_TIMER(playerBD[PARTICIPANT_ID_TO_INT()].BorderPatrolCheckpointTimer[iMyVeh])
					ENDIF
				ELSE
					IF HAS_NET_TIMER_EXPIRED(playerBD[PARTICIPANT_ID_TO_INT()].BorderPatrolCheckpointTimer[iMyVeh], GET_BORDER_PATROL_CHECKPOINT_TIME())
						IF NOT IS_LOCAL_BIT1_SET(eLOCALBITSET1_SENT_FAIL)
							IF serverBD.iClosestDropOff[iMyVeh] < (iDROP_TARGET()-1)
								BROADCAST_PLAYER_FAILED_CHECKPOINT(iMyVeh, serverBD.iClosestDropOff[iMyVeh])
			//					REINIT_NET_TIMER(playerBD[PARTICIPANT_ID_TO_INT()].BorderPatrolCheckpointTimer)
							ENDIF
							SET_LOCAL_BIT1(eLOCALBITSET1_SENT_FAIL)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

CONST_INT ciMAX_SPEECH 16

FUNC BOOL bSPEECH_LEFT()
	RETURN ( sLocaldata.iSpeechCount < ciMAX_SPEECH )
ENDFUNC

FUNC STRING sSELL_SPEECH()
	SWITCH sLocaldata.iSpeechCount
		CASE 0
			RETURN "GENERIC_HOWS_IT_GOING"
		CASE 1
			RETURN "PHONE_CONV1_INTRO"
		CASE 2
			RETURN "PHONE_CONV1_CHAT1"
		CASE 3
			RETURN "PHONE_CONV1_CHAT2"
		CASE 4
			RETURN "PHONE_CONV1_CHAT3"
		CASE 5
			RETURN "PHONE_CONV2_CHAT2"
		CASE 6
			RETURN "PHONE_CONV2_CHAT3"
		CASE 7
			RETURN "PHONE_CONV3_CHAT1"
		CASE 8
			RETURN "PHONE_CONV3_CHAT2"
		CASE 9
			RETURN "PHONE_CONV3_CHAT3"
		CASE 10
			RETURN "PHONE_CONV4_INTRO"
		CASE 11
			RETURN "PHONE_CONV4_CHAT1"
		CASE 12
			RETURN "PHONE_CONV4_CHAT2"
		CASE 13
			RETURN "PHONE_CONV4_CHAT3"
		CASE 14
			RETURN "CHAT_STATE" 
		CASE 15
			RETURN "CHAT_STATE" 
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC MAINTAIN_PED_SPEECH()
	PED_INDEX pedId 
	SWITCH GET_SELL_VAR()
		CASE eBIKER_SELL_PROVEN
			IF bSPEECH_LEFT()
			AND IS_PLAYER_IN_SELL_VEH(0)
				pedId = GET_AI_PED_PED_INDEX(0)
				IF NOT HAS_NET_TIMER_STARTED(sLocaldata.stSpeech)
					START_NET_TIMER(sLocaldata.stSpeech)
					sLocaldata.iSpeechTime = GET_RANDOM_INT_IN_RANGE(20000, 40000)
					PRINTLN("[BIKER_SELL] [AUDIO] MAINTAIN_PED_SPEECH, START_NET_TIMER  ", sLocaldata.iSpeechTime)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(sLocaldata.stSpeech, sLocaldata.iSpeechTime)
					OR sLocaldata.iSpeechCount = 0
						PLAY_PED_AMBIENT_SPEECH(pedId, sSELL_SPEECH())
						RESET_NET_TIMER(sLocaldata.stSpeech)
						sLocaldata.iSpeechCount++
						PRINTLN("[BIKER_SELL] [AUDIO] MAINTAIN_PED_SPEECH, RESET_NET_TIMER, sLocaldata.iSpeechCount = ", sLocaldata.iSpeechCount)
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_CRATE_COLLISION_AFTER_DROP()
	IF BIKER_SELL_HELICOPTER_DROP()
		INT iVeh
		INT iIndex
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iVeh
			iIndex = (serverBD.iNumCrates[iVeh]-1)
		
			IF (serverBD.iNumCrates[iVeh]-1) > -1
				IF HAS_NET_TIMER_STARTED(serverBD.sContraband[iIndex].CreatedTimer[iVeh])
					IF HAS_NET_TIMER_EXPIRED(serverBD.sContraband[iIndex].CreatedTimer[iVeh], 1000)
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sContraband[iIndex].netId[iVeh])
							IF GET_ENTITY_COLLISION_DISABLED(NET_TO_OBJ(serverBD.sContraband[iIndex].netId[iVeh]))
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sContraband[iIndex].netId[iVeh])
								OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sContraband[iIndex].netId[iVeh]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
									IF TAKE_CONTROL_OF_NET_ID(serverBD.sContraband[iIndex].netId[iVeh])
										SET_ENTITY_COLLISION(NET_TO_OBJ(serverBD.sContraband[iIndex].netId[iVeh]), TRUE)
										PRINTLN("[BIKER_SELL] MAINTAIN_CRATE_COLLISION_AFTER_DROP - collisions restored for crate")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
//						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sContraband[iIndex].niChute[iVeh])
//							IF GET_ENTITY_COLLISION_DISABLED(NET_TO_OBJ(serverBD.sContraband[iIndex].niChute[iVeh]))
//								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sContraband[iIndex].niChute[iVeh])
//								OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sContraband[iIndex].niChute[iVeh]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
//									IF TAKE_CONTROL_OF_NET_ID(serverBD.sContraband[iIndex].niChute[iVeh])
//										SET_ENTITY_COLLISION(NET_TO_OBJ(serverBD.sContraband[iIndex].niChute[iVeh]), TRUE)
//										PRINTLN("[BIKER_SELL] MAINTAIN_CRATE_COLLISION_AFTER_DROP - collisions restored for chute")
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
					ELSE
						PRINTLN("[bsell spam] - timer for package ", iIndex, " has started, not yet expired")
					ENDIF
				ELSE
					PRINTLN("[bsell spam] - timer for package ", iIndex, " not started")
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

//PROC CLIENT_SETUP()
////	PED_INDEX pedId 
//	SWITCH GET_SELL_VAR()
//		CASE eBIKER_SELL_TACO_VAN
////			SET_RADIO_LOUD()
//		BREAK
////		CASE eBIKER_SELL_PROVEN
////
////		BREAK
//	ENDSWITCH
//ENDPROC

PROC KICK_EVERYONE_FROM_VEHICLE()
	INT i
	SWITCH GET_SELL_VAR()
		CASE eBIKER_SELL_TRASHMASTER
		CASE eBIKER_SELL_BENSON
		CASE eBIKER_SELL_DEFAULT
			REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
				EJECT_PLAYER_FROM_VEHICLE(i)
				LOCK_SELL_VEH(i, TRUE)
			ENDREPEAT
		BREAK
	ENDSWITCH
ENDPROC

PROC LOCK_VEHS_FOR_POSTMAN()
	IF BIKER_SELL_POSTMAN()
		INT i
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			IF IS_SELL_VEH_OK(i)
			AND NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(i)
				IF bIS_POSTMAN_PAT()
				OR bIS_POSTMAN_PAT_VEH(i)
					IF NOT GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(VEH_ID(i), PLAYER_ID())
						SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(VEH_ID(i), PLAYER_ID(), TRUE)
						PRINTLN("[BIKER_SELL] LOCK_VEHS_FOR_POSTMAN, TRUE ", i)
					ENDIF
				ELSE
					IF GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(VEH_ID(i), PLAYER_ID())
						SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(VEH_ID(i), PLAYER_ID(), FALSE)
						PRINTLN("[BIKER_SELL] LOCK_VEHS_FOR_POSTMAN, FALSE ", i)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC MAINTAIN_DUFFEL_BAG()
	// Check if this is a bag mode
	IF MODE_HAS_PICKUP_BAGS()
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_BAG_CARRIER)
				IF NOT IS_MP_HEIST_GEAR_EQUIPPED(PLAYER_PED_ID(), HEIST_GEAR_SPORTS_BAG)
					REQUEST_CLIP_SET("move_m@bag")
					IF HAS_CLIP_SET_LOADED("move_m@bag")
						SET_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_SPORTS_BAG)
						SET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID(), "move_m@bag")
						PRINTLN("[BIKER_SELL] - MAINTAIN_DUFFEL_BAG - Player is carrying a bag, equipping HEIST_GEAR_SPORTS_BAG")
					ENDIF
				ENDIF
			ELSE
				IF IS_MP_HEIST_GEAR_EQUIPPED(PLAYER_PED_ID(), HEIST_GEAR_SPORTS_BAG)
					REMOVE_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_SPORTS_BAG)
					REMOVE_CLIP_SET("move_m@bag")
					RESET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID())
					PRINTLN("[BIKER_SELL] - MAINTAIN_DUFFEL_BAG - Player is not carrying a bag, removing HEIST_GEAR_SPORTS_BAG")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_TEXT_MESSAGES()
	IF DOES_VARIATION_HAVE_EXTRA_TEXT_MESSAGES()
		// Send text when near altruist camp
		IF BIKER_SELL_HELICOPTER_DROP()
			IF NOT IS_LOCAL_BIT1_SET(eLOCALBITSET1_SENT_ALTRUIST_TEXT)
				IF AM_I_IN_ANY_SELL_VEH()
					IF serverBD.bAltruistSelected
					AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_SELL_DROP_COORDS(eBIKER_SELL_HELICOPTER_DROP, 3, serverBD.iRoute, GET_SELL_VEH_I_AM_IN_INT()), FALSE) < 100
						IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BIKER_CH2, "BSELL_ALTRUIST", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
							PRINTLN("[BIKER_SELL] MAINTAIN_TEXT_MESSAGES - Sent altruist text to local player, setting eLOCALBITSET1_SENT_ALTRUIST_TEXT")
							SET_LOCAL_BIT1(eLOCALBITSET1_SENT_ALTRUIST_TEXT)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC RENDER_SELL_DEBUG()
	
	FLOAT fDrawX, fDrawY
	INT r,g,b,a
	
	INT iVehicleIn 	
//	INT iVehicleClosest = GET_CLOSEST_SELL_VEH_TO_LOCAL_PLAYER()
	INT iClosestDropOff = -1
	INT iNumBags = GET_NUM_BAGS()
	
	iVehicleIn = GET_SELL_VEH_I_AM_IN_INT()
	
	IF MODE_HAS_PICKUP_BAGS()	
		iVehicleIn = iGET_PLAYER_BAG(PLAYER_ID())
	ENDIF
	
	IF iVehicleIn <> -1
		iClosestDropOff = serverBD.iClosestDropOff[iVehicleIn]
	ENDIF
	
	VECTOR vDrop = GET_SELL_DROP_COORDS(GET_SELL_VAR(), iClosestDropOff, serverBD.iRoute, iVehicleIn, DEFAULT, bIS_POSTMAN_PAT())
	
	FLOAT fTextScaleChk 	= 0.5
	fDrawX = 0.3
	fDrawY = 0.3
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_D, KEYBOARD_MODIFIER_SHIFT, "NO")
		IF sLocaldata.bSellDebug
			sLocaldata.bSellDebug = FALSE
		ELSE
			sLocaldata.bSellDebug = TRUE
		ENDIF
	ENDIF

	IF sLocaldata.bSellDebug
			
		// My Vehicle ~1~
		SET_TEXT_SCALE(0.0000, fTextScaleChk)
		GET_HUD_COLOUR(HUD_COLOUR_BLUE, r,g,b,a)
		SET_TEXT_COLOUR(r,g,b,a)		
		IF MODE_HAS_PICKUP_BAGS()
			DISPLAY_TEXT_WITH_NUMBER(fDrawX, fDrawY, "SLL_1B", iVehicleIn)
		ELSE
			DISPLAY_TEXT_WITH_NUMBER(fDrawX, fDrawY, "SLL_1", iVehicleIn)
		ENDIF
		
		// Closest Drop-off ~1~
		SET_TEXT_SCALE(0.0000, fTextScaleChk)
		GET_HUD_COLOUR(HUD_COLOUR_ORANGE, r,g,b,a)
		SET_TEXT_COLOUR(r,g,b,a)
		DISPLAY_TEXT_WITH_NUMBER(fDrawX, fDrawY + 0.05, "SLL_2", iClosestDropOff)

		// Num Bags ~1~	
		IF MODE_HAS_PICKUP_BAGS()
			SET_TEXT_SCALE(0.0000, fTextScaleChk)
			GET_HUD_COLOUR(HUD_COLOUR_GREEN, r,g,b,a)
			SET_TEXT_COLOUR(r,g,b,a)
			DISPLAY_TEXT_WITH_NUMBER(fDrawX, fDrawY + 0.10, "SLL_3", iNumBags)
		ENDIF
		
		// vDrop <<~1~, ~1~, ~1~>>
		IF NOT IS_VECTOR_ZERO(vDrop)
			SET_TEXT_SCALE(0.0000, fTextScaleChk)
			GET_HUD_COLOUR(HUD_COLOUR_YELLOW, r,g,b,a)
			SET_TEXT_COLOUR(r,g,b,a)
			DISPLAY_TEXT_WITH_VECTOR(fDrawX, fDrawY + 0.15, "SPN_V", vDrop, 2)
		ENDIF
		
		SET_TEXT_SCALE(0.0000, fTextScaleChk)
		GET_HUD_COLOUR(HUD_COLOUR_GREY, r,g,b,a)
		SET_TEXT_COLOUR(r,g,b,a)
		DISPLAY_TEXT(fDrawX, fDrawY + 0.20, GET_FACTORY_NAME_FROM_ID(serverBD.factoryId))
		
		SET_TEXT_SCALE(0.0000, fTextScaleChk)
		GET_HUD_COLOUR(HUD_COLOUR_BLUEDARK, r,g,b,a)
		SET_TEXT_COLOUR(r,g,b,a)
		DISPLAY_TEXT_WITH_NUMBER(fDrawX, fDrawY + 0.25, "SLL_6", GET_SELL_VEH_TARGET_DROP_COUNT())
		
		IF BIKER_SELL_POSTMAN()
			// "Postman Pat"
			IF bIS_POSTMAN_PAT()
				SET_TEXT_SCALE(0.0000, fTextScaleChk)
				GET_HUD_COLOUR(HUD_COLOUR_RED, r,g,b,a)
				SET_TEXT_COLOUR(r,g,b,a)
				DISPLAY_TEXT(fDrawX, fDrawY + 0.30, "SLL_4")
			ENDIF
		ELSE
			SET_TEXT_SCALE(0.0000, fTextScaleChk)
			GET_HUD_COLOUR(HUD_COLOUR_BLACK, r,g,b,a)
			SET_TEXT_COLOUR(r,g,b,a)
			IF iVehicleIn <> -1
				IF IS_BIT_SET(serverBD.iActiveDropOffBitset[iVehicleIn], iClosestDropOff)
					DISPLAY_TEXT_WITH_NUMBER(fDrawX, fDrawY + 0.30, "SLL_8", iClosestDropOff)
				ELSE
					DISPLAY_TEXT_WITH_NUMBER(fDrawX, fDrawY + 0.30, "SLL_9", iClosestDropOff)
				ENDIF
			ENDIF
		ENDIF	
		
		SET_TEXT_SCALE(0.0000, fTextScaleChk)
		GET_HUD_COLOUR(HUD_COLOUR_RED, r,g,b,a)
		SET_TEXT_COLOUR(r,g,b,a)
		DISPLAY_TEXT_WITH_NUMBER(fDrawX, fDrawY + 0.35, "SLL_7", serverBD.iDropPenalty)
	ENDIF
ENDPROC
#ENDIF

FUNC eLOCAL_BITSET_2 GET_SELL_VEH_DELIVERY_COMPLETE_BIT_TO_CHECK(INT iVeh)
	SWITCH iVeh
		CASE 0		RETURN eLOCALBITSET2_VEH_0_DELIVERED_ALL_CONTRA
		CASE 1		RETURN eLOCALBITSET2_VEH_1_DELIVERED_ALL_CONTRA
		CASE 2		RETURN eLOCALBITSET2_VEH_2_DELIVERED_ALL_CONTRA
		CASE 3		RETURN eLOCALBITSET2_VEH_3_DELIVERED_ALL_CONTRA
	ENDSWITCH
	
	RETURN eLOCALBITSET2_END
ENDFUNC

PROC DO_DELIVERY_COMPLETE_HELP(INT iVeh)
	eLOCAL_BITSET_2 eBitToCheck = GET_SELL_VEH_DELIVERY_COMPLETE_BIT_TO_CHECK(iVeh)
	IF eBitToCheck != eLOCALBITSET2_END
		IF NOT IS_LOCAL_BIT2_SET(eBitToCheck)
			PRINT_HELP("BSELL_DEL_COMP")
			SET_LOCAL_BIT2(eBitToCheck)
			PRINTLN("[BIKER_SELL] DO_DELIVERY_COMPLETE_HELP - Vehicle ", iVeh, " completed all drops, help shown: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("BSELL_DEL_COMP"))
		ENDIF
	ENDIF
ENDPROC

PROC CLIENT_PROCESS_MISSION_LOGIC()
	
	INT iVeh

	MAINTAIN_WANTED_RATINGS()
	MAINTAIN_STONED()
	MAINTAIN_POSTMAN()
	HANDLE_VEHICLE_STOPPING()
	HANDLE_LANDING_PLANE_FLAG()
	MAINTAIN_PLANE_HEIGHT_CLIENT()
	VEHICLE_STUCK_CHECKS()
	MAINTAIN_AUDIO_MUSIC_TRIGGERS()
	DO_BOSS_INTRO_PHONECALL()
	SET_ILLICIT_GOODS_PLAYER()
	CLIENT_MANAGE_SMOKE_FROM_DROP_OFFS()	
	ADD_BLUNT_SMOKE_FX()
	MAINTAIN_BORDER_PATROL_CHECKPOINT_TIMER()
	MAINTAIN_PED_SPEECH()
//	DO_MEXICAN_STREAM()
	LOCK_VEHS_FOR_POSTMAN()
	MAINTAIN_DUFFEL_BAG()
	MAINTAIN_TEXT_MESSAGES()
	MAINTAIN_PARCELS()
	
	#IF IS_DEBUG_BUILD
	RENDER_SELL_DEBUG()
	#ENDIF
	
	SWITCH GET_LOCAL_CLIENT_MISSION_STAGE()
		
		CASE eSELL_STAGE_INIT
			DO_BIG_MESSAGE( eBIGM_GANG_START)
//			CLIENT_SETUP()
			SET_LOCAL_CLIENT_MISSION_STAGE(eSELL_STAGE_VEH)
		BREAK
	
		CASE eSELL_STAGE_VEH
			IF IS_ANY_SELL_VEH_OCCUPIED()
			OR HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
				SET_LOCAL_CLIENT_MISSION_STAGE(eSELL_STAGE_SELLING)
			ENDIF
		BREAK
		CASE eSELL_STAGE_SELLING
			IF IS_ANY_SELL_VEH_OCCUPIED()
			OR HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()			
				IF HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
					IF NOT IS_THIS_AN_AIR_VARIATION()
					AND NOT IS_THIS_A_SEA_VARIATION()
						INT i
						REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
							EJECT_PLAYER_FROM_VEHICLE(i)
							LOCK_SELL_VEH(i, TRUE)
						ENDREPEAT
					ENDIF
					TRIGGER_AUDIO_MUSIC_ON_WIN()
					SET_LOCAL_CLIENT_MISSION_STAGE(eSELL_STAGE_CLEANUP)
				ELSE
					IF IS_PLAYER_IN_ANY_SELL_VEH(FALSE)
						iVeh = GET_SELL_VEH_I_AM_IN_INT()
						IF HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(iVeh)
							DO_DELIVERY_COMPLETE_HELP(iVeh)
							IF NOT IS_THIS_AN_AIR_VARIATION()
								EJECT_PLAYER_FROM_VEHICLE(iVeh)
								LOCK_SELL_VEH(iVeh, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				SET_LOCAL_CLIENT_MISSION_STAGE(eSELL_STAGE_VEH)
			ENDIF
		BREAK
		CASE eSELL_STAGE_CLEANUP
			GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_BLIP_WARN)
		BREAK
	ENDSWITCH
ENDPROC

//╚═════════════════════════════════════════════════════════════════════════════╝

#IF IS_DEBUG_BUILD

PROC MAINTAIN_J_SKIPS()
	
	IF NOT IS_CLIENT_DEBUG_BIT0_SET(PARTICIPANT_ID(), eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
		IF IS_DEBUG_KEY_JUST_RELEASED(KEY_S, KEYBOARD_MODIFIER_NONE, "S Pass")
			PRINTLN("[BIKER_SELL] KEY_S, KEYBOARD_MODIFIER_NONE just released")
			SCRIPT_EVENT_DATA_TICKER_MESSAGE debugTicker
			debugTicker.TickerEvent = TICKER_EVENT_PLAYER_CHEATED_SPASS
			BROADCAST_TICKER_EVENT(debugTicker, ALL_PLAYERS_ON_SCRIPT())
			SET_CLIENT_DEBUG_BIT0(eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
		ENDIF
	ENDIF
	
	IF NOT IS_CLIENT_DEBUG_BIT0_SET(PARTICIPANT_ID(), eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED)
		IF IS_DEBUG_KEY_JUST_RELEASED(KEY_F, KEYBOARD_MODIFIER_NONE, "F Fail")
			PRINTLN("[BIKER_SELL] KEY_F, KEYBOARD_MODIFIER_NONE just released")
			SCRIPT_EVENT_DATA_TICKER_MESSAGE debugTicker
			debugTicker.TickerEvent = TICKER_EVENT_PLAYER_CHEATED_FFAIL
			BROADCAST_TICKER_EVENT(debugTicker, ALL_PLAYERS_ON_SCRIPT())
			SET_CLIENT_DEBUG_BIT0(eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED)
		ENDIF
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF serverBD.iSPassPart > (-1)
			PRINTLN("[BIKER_SELL] [SERVER] serverBD.iSPassPart > (-1)")
			IF GET_MODE_STATE() < eMODESTATE_REWARDS
				SET_END_REASON(eENDREASON_WIN_CONDITION_TRIGGERED)
				SET_MODE_STATE(eMODESTATE_REWARDS)
			ENDIF
		ELIF serverBD.iFFailPart > (-1)
			PRINTLN("[BIKER_SELL] [SERVER] serverBD.iFFailPart > (-1)")
			IF GET_MODE_STATE() < eMODESTATE_REWARDS
				SET_END_REASON(eENDREASON_TIME_UP)
				SET_MODE_STATE(eMODESTATE_REWARDS)
			ENDIF
		ENDIF
	ENDIF
	
	// J skip to vehicle
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_NONE, "")
	
		IF MODE_HAS_PICKUP_BAGS()
		AND DO_I_HAVE_A_BAG()
		
//			INT iBags
			VECTOR vDropCoords
//			PED_INDEX piPedWithBag
			INT iClosest
				
//			REPEAT GET_NUM_BAGS() iBags
//				
//				PLAYER_INDEX piPlayer = INT_TO_PLAYERINDEX(serverBD.iAssignedBag[iBags])
//				IF serverBD.iAssignedBag[iBags] != -1
//				AND NETWORK_IS_PLAYER_ACTIVE(piPlayer)
//								
//					piPedWithBag = GET_PLAYER_PED(piPlayer)
//					
//					GET_CLOSEST_DROP_OFF_TO_PED(iBags, piPedWithBag, iClosest)
//					
//					vDropCoords = GET_SELL_DROP_COORDS(GET_SELL_VAR(), iClosest, serverBD.iRoute, iBags, DEFAULT, FALSE)
//				ENDIF
//			ENDREPEAT
			
			INT iBags = iGET_PLAYER_BAG(PLAYER_ID())
			
			GET_CLOSEST_DROP_OFF_TO_PED(iBags, PLAYER_PED_ID(), iClosest)
					
			vDropCoords = GET_SELL_DROP_COORDS(GET_SELL_VAR(), iClosest, serverBD.iRoute, iBags, DEFAULT, FALSE)
			
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDropCoords, <<5,5,5>>)
			
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vDropCoords)
			ELSE
				
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vDropCoords + <<4, 0, 0>>)
			ENDIF
		ELSE
			INT iVeh = GET_CLOSEST_SELL_VEH_TO_LOCAL_PLAYER(BIKER_SELL_BENSON())
			
			IF iVeh != -1
				IF IS_DRIVER_OF_SELL_VEH(iVeh)
					IF BIKER_SELL_BENSON()
						SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vGET_DROP_OFF_VEH_START(iVeh) + <<0,8,0>>)
						SET_ENTITY_HEADING(VEH_ENT(iVeh), fGET_DROP_OFF_VEH_HEAD(iVeh))
						PRINTLN("[BIKER_SELL] J_SKIP SET_PED_COORDS_KEEP_VEHICLE BIKER_SELL_BENSON")
					ELSE
						SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[iVeh], serverBD.iRoute, iVeh))
						PRINTLN("[BIKER_SELL] J_SKIP SET_PED_COORDS_KEEP_VEHICLE")
						SET_ENTITY_HEADING(VEH_ENT(iVeh), 355.6729)
					ENDIF
				ELSE
					IF bIS_POSTMAN_PAT()
						SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[iVeh], serverBD.iRoute, iVeh, FALSE, TRUE))
						PRINTLN("[BIKER_SELL] J_SKIP bIS_POSTMAN_PAT SET_ENTITY_COORDS")
					ELSE
						IF IS_THIS_AN_AIR_VARIATION()
						AND NOT IS_PLAYER_IN_AIRPORT()
							SET_ENTITY_COORDS(PLAYER_PED_ID(), vAIRPORT_COORDS())
							PRINTLN("[BIKER_SELL] J_SKIP SET_ENTITY_COORDS vAIRPORT_COORDS")
						ELSE
							IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), VEH_ENT(iVeh), <<7.0, 7.0, 7.0>>)
								IF IS_VEHICLE_SEAT_FREE(VEH_ID(iVeh), VS_DRIVER)
									PRINTLN("[BIKER_SELL] J_SKIP TASK_ENTER_VEHICLE")
									TASK_ENTER_VEHICLE(PLAYER_PED_ID(), VEH_ID(iVeh), -1, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED)
								ELSE
									IF IS_VEHICLE_SEAT_FREE(VEH_ID(iVeh), VS_FRONT_RIGHT)
										TASK_ENTER_VEHICLE(PLAYER_PED_ID(), VEH_ID(iVeh), -1, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED)
										PRINTLN("[BIKER_SELL] J_SKIP TASK_ENTER_VEHICLE VS_FRONT_RIGHT")
									ELSE
										SET_ENTITY_COORDS(PLAYER_PED_ID(), VEH_POS(iVeh, TRUE))
										PRINTLN("[BIKER_SELL] J_SKIP SET_ENTITY_COORDS VEH_POS 1 ")
									ENDIF
								ENDIF
							ELSE
								SET_ENTITY_COORDS(PLAYER_PED_ID(), VEH_POS(iVeh, TRUE))
								PRINTLN("[BIKER_SELL] J_SKIP SET_ENTITY_COORDS VEH_POS 2 ")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
#ENDIF


FUNC BOOL IS_PLAYER_WITHIN_RANGE_OF_DROP_OFF_FOR_WARP()
	
	// only done for sea variation so ok to pass 0 as only one vehicle
	VECTOR vDropOff = GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[0], serverBD.iRoute, 0)
	
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vDropOff, FALSE) <= 100
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PLAYER_WARP()

	IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_END_WARP_DONE)
		PRINTLN("contra sell warp spam - player should NOT warp - IS_LOCAL_BIT0_SET(eLOCALBITSET0_END_WARP_DONE)")
		RETURN FALSE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(sLocalData.stWarpAllowanceTimer)
		IF HAS_NET_TIMER_EXPIRED(sLocalData.stWarpAllowanceTimer, 2000)
			PRINTLN("contra sell warp spam - player should NOT warp - warp allowance timer expired")
			RETURN FALSE
		ENDIF
	ENDIF
	

	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		PRINTLN("contra sell warp spam - player should NOT warp - player is not okay")
		RETURN FALSE
	ENDIF

	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF DOES_ENTITY_EXIST(VEH_ID(i))
			IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), VEH_ID(i), TRUE)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("contra sell warp spam - player should NOT warp - player is in a vehicle")
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PLAYER_WARPING()
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_END_WARP_DONE)
		RETURN FALSE
	ENDIF

	IF sLocaldata.iWarpStage > 0 AND sLocaldata.iWarpStage < 5
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_CONTRABAND_DECORATOR()
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
			
			IF DOES_ENTITY_EXIST(VEH_ENT(i))
				IF NETWORK_HAS_CONTROL_OF_ENTITY(VEH_ENT(i))
					IF NOT IS_ENTITY_DEAD(VEH_ENT(i))
						GB_CLEAR_VEHICLE_AS_CONTRABAND(VEH_ID(i))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC
PROC SEND_CONTRABAND_VISIBLE_TICKER(BOOL bLocal = FALSE)
	SCRIPT_EVENT_DATA_TICKER_MESSAGE sData
	
	sData.TickerEvent = TICKER_EVENT_CONTRABAND_VISIBLE
	sData.playerID = PLAYER_ID()
	sData.playerID2 = GB_GET_LOCAL_PLAYER_GANG_BOSS()
	
	IF bLocal
		BROADCAST_TICKER_EVENT(sData, SPECIFIC_PLAYER(PLAYER_ID()))
	ELSE
		BROADCAST_TICKER_EVENT(sData, ALL_PLAYERS_ON_SCRIPT(TRUE))
	ENDIF
ENDPROC

PROC MAINTAIN_PLAYER_IN_SELL_VEH()
	IF BIKER_SELL_HELICOPTER_DROP()
		IF IS_DRIVER_OF_ANY_SELL_VEH()
		OR IS_PASSENGER_OF_ANY_SELL_VEHICLE()
			IF g_bBikerInSellHeli = FALSE
				PRINTLN("[BIKER_SELL] - MAINTAIN_PLAYER_IN_SELL_VEH - player is pilot of heli, setting g_bBikerInSellHeli = TRUE")
				g_bBikerInSellHeli = TRUE
				
				SET_DISABLE_OFFICE_LANDING_FOR_LOCAL_PLAYER(TRUE)
			ENDIF
		ELSE
			IF g_bBikerInSellHeli
				PRINTLN("[BIKER_SELL] - MAINTAIN_PLAYER_IN_SELL_VEH - player is no longer pilot of heli, setting g_bBikerInSellHeli = FALSE")
				g_bBikerInSellHeli = FALSE
				
				SET_DISABLE_OFFICE_LANDING_FOR_LOCAL_PLAYER(FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_CONTRABAND_BLIP_DELAY()
	IF SHOULD_DELAY_BLIP_FOR_FACTORY()
	AND GET_END_REASON() = eENDREASON_NO_REASON_YET
		IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_VEHICLES_MARKED_AS_CONTRABAND)
			IF HAS_NET_TIMER_STARTED(serverBD.stBlipDelayTimer)
			AND HAS_NET_TIMER_EXPIRED(serverBD.stBlipDelayTimer, GET_BLIP_DELAY_TIME_FOR_FACTORY())
				
				IF NOT MODE_HAS_PICKUP_BAGS()
					INT i
					REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
						GB_SET_VEHICLE_AS_CONTRABAND(VEH_ID(i), i, GBCT_BIKER)
					ENDREPEAT
				ENDIF
				
				SEND_CONTRABAND_VISIBLE_TICKER()
				
				SET_SERVER_BIT1(eSERVERBITSET1_VEHICLES_MARKED_AS_CONTRABAND)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_GLOBAL_REVEAL_SOUNDS()
	IF HAS_NET_TIMER_STARTED(serverBD.stBlipDelayTimer)
		IF NOT IS_LOCAL_BIT1_SET(eLOCALBITSET1_PLAYED_FIVE_SECOND_GLOBAL_TIMER)
			IF HAS_NET_TIMER_EXPIRED(serverBD.stBlipDelayTimer, (GET_BLIP_DELAY_TIME_FOR_FACTORY()-5000 ))
				PRINTLN("[BIKER_SELL] - contraband sell timer has five seconds left. BLEEP BLEEP BLEEP")
				PLAY_SOUND_FRONTEND(-1, "5s", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
				SET_LOCAL_BIT1(eLOCALBITSET1_PLAYED_FIVE_SECOND_GLOBAL_TIMER)
			ENDIF
		ENDIF
		
		IF NOT IS_LOCAL_BIT1_SET(eLOCALBITSET1_PLAYED_GLOBAL_BLIP)
			IF HAS_NET_TIMER_EXPIRED(serverBD.stBlipDelayTimer, GET_BLIP_DELAY_TIME_FOR_FACTORY())
				PRINTLN("[BIKER_SELL] - contraband sell timer is done. BEEEEEP")
				PLAY_SOUND_FRONTEND(-1, "Crates_Blipped", "GTAO_Biker_Modes_Soundset", FALSE)
				SET_LOCAL_BIT1(eLOCALBITSET1_PLAYED_GLOBAL_BLIP)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// [NOTE FOR BOBBY] - Need to hold off storing contraband vehicle coords for Martin until we say they should be blipped
//						Dont bother delaying if we dont need to

FUNC BOOL IS_SAFE_TO_MAINTAIN_ENTRY_TIMERS()
	IF SHOULD_DELAY_BLIP_FOR_FACTORY()
		RETURN IS_SERVER_BIT1_SET(eSERVERBITSET1_VEHICLES_MARKED_AS_CONTRABAND)
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_VEHICLE_ENTRY_TIMERS()
	BOOL bDelivered
	IF IS_SAFE_TO_MAINTAIN_ENTRY_TIMERS()
		bDelivered = HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(0)
		STORE_CONTRABAND_VEHICLE_COORDS(VEH_ID(0),vehicleCoordTimer[0],0, bDelivered)
		
		IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
			bDelivered = HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(1)
			STORE_CONTRABAND_VEHICLE_COORDS(VEH_ID(1),vehicleCoordTimer[1],1, bDelivered)

			bDelivered = HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(2)
			STORE_CONTRABAND_VEHICLE_COORDS(VEH_ID(2),vehicleCoordTimer[2],2, bDelivered)
			
			bDelivered = HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(3)
			STORE_CONTRABAND_VEHICLE_COORDS(VEH_ID(3),vehicleCoordTimer[3],3, bDelivered)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_EMERGENCY_SERVICES()
	IF GET_MODE_STATE() = eMODESTATE_RUN
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
	ELSE
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
	ENDIF
ENDPROC

PROC MAINTAIN_GHOST_ORG_HELP()
	IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_DONE_ORG_HELP)
		GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_BLIP_WARN) 
		IF HAS_ANY_SELL_VEHICLE_BEEN_ENTERED()
		AND IS_SAFE_TO_MAINTAIN_ENTRY_TIMERS()
	 		IF GB_TRIGGER_GHOST_ORG_HELP_TEXT_FOR_CONTRABAND_MISSIONS()
				PRINTLN("[BIKER_SELL] GB_TRIGGER_GHOST_ORG_HELP_TEXT_FOR_CONTRABAND_MISSIONS - DONE")
				SET_LOCAL_BIT0(eLOCALBITSET0_DONE_ORG_HELP)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HELP_TEXT_CLEANUP()
	IF GB_IS_LOCAL_HELP_BIT_SET(eGB_LOCAL_HELP_BITSET_NOTIFICATION_AIR_DROP)
		GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_AIR_DROP) 
	ENDIF
	IF GB_IS_LOCAL_HELP_BIT_SET(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER)
		GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER)
	ENDIF
	IF GB_IS_LOCAL_HELP_BIT_SET(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER_P)
		GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER_P)
	ENDIF
	IF GB_IS_LOCAL_HELP_BIT_SET(eGB_LOCAL_HELP_BITSET_NOTIFICATION_EXPLAIN_CONTINUE_DESTROY)
		GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_EXPLAIN_CONTINUE_DESTROY)
	ENDIF
	IF GB_IS_LOCAL_HELP_BIT_SET(eGB_LOCAL_HELP_BITSET_FIN)
		GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_FIN)
	ENDIF
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP8")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP14")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP6")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP6P")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP7")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP7P")
		CLEAR_HELP(TRUE)
	ENDIF
ENDPROC

//Ping the session if the player is more that 50 meters from the warehouse
PROC MAINTAIN_SET_CRITICAL_BAG()
	IF MODE_HAS_PICKUP_BAGS()
		IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_BAG_CARRIER)
			IF NOT IS_LOCAL_BIT1_SET(eLOCALBITSET1_SET_CARRYING_CRITICAL_PACKAGE)
			AND NOT IS_PLAYER_IN_FACTORY(PLAYER_ID())
				VECTOR vWarehoseCoords = GET_FACTORY_COORDS(serverBD.factoryId)
				IF VDIST2(vWarehoseCoords, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > 2500
					GB_SET_LOCAL_PLAYER_CARRYING_CRITICAL_PACKAGE(TRUE)
					SET_LOCAL_BIT1(eLOCALBITSET1_SET_CARRYING_CRITICAL_PACKAGE)
					IF NOT IS_LOCAL_BIT1_SET(eLOCALBITSET1_SET_CARRYING_CRITICAL_PACKAGE_TICKER)
						SEND_CONTRABAND_VISIBLE_TICKER(TRUE)
						SET_LOCAL_BIT1(eLOCALBITSET1_SET_CARRYING_CRITICAL_PACKAGE_TICKER)
					ENDIF
				ENDIF
			ENDIF
		ELIF IS_LOCAL_BIT1_SET(eLOCALBITSET1_PLAYED_FIVE_SECOND_GLOBAL_TIMER)
			GB_SET_LOCAL_PLAYER_CARRYING_CRITICAL_PACKAGE(FALSE)
			CLEAR_LOCAL_BIT1(eLOCALBITSET1_SET_CARRYING_CRITICAL_PACKAGE)
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_ENTITY_IN_SHOP()
	IF NOT MODE_HAS_PICKUP_BAGS()
		INT i
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i	
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
				VECTOR vVehPos = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[i]),FALSE)
				SHOP_NAME_ENUM closestShop = GET_CLOSEST_SHOP_OF_TYPE(vVehPos)

				VECTOR vShopCoords = GET_SHOP_COORDS(closestShop)

				INTERIOR_INSTANCE_INDEX interiorShop = GET_INTERIOR_AT_COORDS_WITH_TYPE(vShopCoords, GET_SHOP_INTERIOR_TYPE(closestShop))
				INTERIOR_INSTANCE_INDEX interiorVeh = GET_INTERIOR_FROM_ENTITY(NET_TO_VEH(serverBD.niVeh[i]))
				IF interiorShop != NULL
				AND interiorShop = interiorVeh
					IF NOT MPGlobalsAmbience.bKeepShopDoorsOpen
						MPGlobalsAmbience.bKeepShopDoorsOpen = TRUE
						CPRINTLN(DEBUG_NET_AMBIENT, "[BIKER_SELL] HANDLE_ENTITY_IN_SHOP - MPGlobalsAmbience.bKeepShopDoorsOpenForParcelVehicle = TRUE")
					ENDIF
					EXIT
				ENDIF
			ENDIF
		ENDREPEAT
	
		IF MPGlobalsAmbience.bKeepShopDoorsOpen
			MPGlobalsAmbience.bKeepShopDoorsOpen = FALSE
			CPRINTLN(DEBUG_NET_AMBIENT, "[BIKER_SELL] HANDLE_ENTITY_IN_SHOP - MPGlobalsAmbience.bKeepShopDoorsOpenForParcelVehicle = FALSE")
		ENDIF
	ENDIF
ENDPROC

PROC GIVE_BAG_TO_PLAYER()
	IF MODE_HAS_PICKUP_BAGS()
		IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_BAG_CARRIER)
			BOOL bHasBagAssigned = FALSE
			INT i
			REPEAT GET_NUM_BAGS() i
				IF serverBD.iAssignedBag[i] = NATIVE_TO_INT(PLAYER_ID())
					bHasBagAssigned = TRUE
					PRINTLN("[bag spam] - player has bag ", i, " assigned to them")
				ENDIF
			ENDREPEAT
			
			IF bHasBagAssigned
				PRINTLN("[bag spam] - mode has bags")
				IF IS_SCREEN_FADED_OUT()
				#IF IS_DEBUG_BUILD
				OR GET_COMMANDLINE_PARAM_EXISTS("sc_biker_sell_force_bag")
				#ENDIF
					PRINTLN("[BIKER_SELL] - GIVE_BAG_TO_PLAYER - Player is now carrying a bag")
					SET_CLIENT_BIT0(eCLIENTBITSET0_BAG_CARRIER)
					SET_CARRYING_CRITICAL_ENTITY(TRUE)
				ELSE
					PRINTLN("[bag spam] - screen not faded out")
				ENDIF
			ELSE
				PRINTLN("[bag spam] - player doesnt have a bag assigned to them")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_SELL_READY_GLOBAL(BOOL bSet)
	IF bSet
		IF NOT IS_BIT_SET(g_iSellBitSet, ciGB_BIKER_SELL_SERV_INIT_DONE)
			PRINTLN("[BIKER_SELL] SET_SELL_READY_GLOBAL SET_BIT ")
			SET_BIT(g_iSellBitSet, ciGB_BIKER_SELL_SERV_INIT_DONE)
		ENDIF
	ELSE
		IF IS_BIT_SET(g_iSellBitSet, ciGB_BIKER_SELL_SERV_INIT_DONE)
			PRINTLN("[BIKER_SELL] SET_SELL_READY_GLOBAL CLEAR_BIT ")
			CLEAR_BIT(g_iSellBitSet, ciGB_BIKER_SELL_SERV_INIT_DONE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL bSHOULD_OPEN_AIRPORT_GATES()

	IF IS_THIS_AN_AIR_VARIATION()
	AND SHOULD_DROP_OFFS_BE_IN_COUNTRY()
		RETURN TRUE
	ENDIF

	IF BIKER_SELL_BENSON()
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CHECK_FOR_SUICIDE()
	IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_COMMITTED_SUICIDE)
		IF g_MultiplayerSettings.g_bSuicide
			PRINTLN("[BIKER_SELL] - CHECK_FOR_SUICIDE - Player has used PI menu to commit suicide - setting eCLIENTBITSET0_COMMITTED_SUICIDE")
			SET_CLIENT_BIT0(eCLIENTBITSET0_COMMITTED_SUICIDE)
		ENDIF
	ENDIF
ENDPROC

PROC CLIENT_PROCESSING()
	
	#IF IS_DEBUG_BUILD
	MAINTAIN_J_SKIPS()
	#ENDIF
	// Blip the vehicle for off mission players
	MAINTAIN_VEHICLE_ENTRY_TIMERS()	
	CLIENT_MAINTAIN_PED_BODIES()
	CLIENT_MAINTAIN_DROP_OFF_PED_BODIES()
	MAINTAIN_GHOST_ORG_HELP()
	MAINTAIN_VEHICLE_ACCESS()
	MAINTAIN_EMERGENCY_SERVICES()
	CLIENT_SELL_VEH_LOOP()
	MAINTAIN_CRATE_COLLISION_AFTER_DROP()
	MAINTAIN_BAG_DEATH_HELP()
	MAINTAIN_GLOBAL_REVEAL_SOUNDS()
	HANDLE_ENTITY_IN_SHOP()
	CHECK_FOR_SUICIDE()
	
	IF GET_MAX_WANTED_LEVEL() = 0
		SET_LAW_PEDS_CAN_ATTACK_NON_WANTED_PLAYER_THIS_FRAME(PLAYER_ID())
	ENDIF
	
	SWITCH GET_MODE_STATE()
	
		CASE eMODESTATE_INIT
			// ********************************************************************
			// Server says when omode has intialised and moves us onto run state.
			// ********************************************************************
		BREAK
		
		CASE eMODESTATE_RUN
			
			// ***********************
			// Do common start calls
			// ***********************
			IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALLED_COMMON_SETUP)
				GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_BIKER_CONTRABAND_SELL,DEFAULT,ENUM_TO_INT(serverBD.eSellVar))
				GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
								
				SET_SELL_READY_GLOBAL(TRUE)
				
				IF bSHOULD_OPEN_AIRPORT_GATES()
					UNLOCK_AIRPORT_GATES()
				ENDIF
				// Unlock gates url:bugstar:2801049 - Sell Missions: Land Defend: Metal mesh gate remained closed at the the destination drop off point. 
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<210.93,-2022.58,17.65>>, 6.0, prop_fnclink_03gate1)
					PRINTLN("SET_STATE_OF_CLOSEST_DOOR_OF_TYPE, OPEN ")
					SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_fnclink_03gate1, <<210.93,-2022.58,17.65>>, TRUE, 1.0)
				ENDIF
				SET_LOCAL_BIT0(eLOCALBITSET0_CALLED_COMMON_SETUP)
				IF BIKER_SELL_RACE()
					GB_BIKER_RACE_SET_UP(sBikerRace, serverBD.sRaceServerVars, GET_SELL_DROP_COORDS(GET_SELL_VAR(), 0, serverBD.iRoute, 0), sLocaldata.blipDrop[0])
				ENDIF
			ELSE
				IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP)
					IF HAS_NET_TIMER_EXPIRED(serverBD.modeTimer, TRACK_GOON_DELAY)
						GB_SET_ITS_SAFE_FOR_SPEC_CAM()
						SET_LOCAL_BIT0(eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP)
					ENDIF
				ENDIF
			ENDIF
			
			// *******************************************
			// Main mode logic here, after setup is done
			// *******************************************
			
			// Warp out of warehouse at the start if we're inside
			// Process logic once we're outside
			IF IS_PLAYER_IN_FACTORY(PLAYER_ID())
			AND GET_FACTORY_PLAYER_IS_IN(PLAYER_ID()) = serverBD.factoryId
				PRINTLN("[bag spam] IS_PLAYER_IN_FACTORY ", GET_FACTORY_NAME_FROM_ID(serverBD.factoryId))
				GIVE_BAG_TO_PLAYER()
			ELSE
			
				IF IS_PLAYER_IN_FACTORY(PLAYER_ID())
					PRINTLN("[bag spam] serverBD.factoryId = ", GET_FACTORY_NAME_FROM_ID(serverBD.factoryId))
					PRINTLN("[bag spam] GET_FACTORY_PLAYER_IS_IN = ", GET_FACTORY_NAME_FROM_ID(GET_FACTORY_PLAYER_IS_IN(PLAYER_ID() ) ) )
//				ELSE
//					PRINTLN("[bag spam] IS_PLAYER_IN_FACTORY, FALSE ")
				ENDIF
//				PRINTLN("[bag spam] serverBD.factoryId = ", GET_FACTORY_NAME_FROM_ID(serverBD.factoryId))
//				PRINTLN("[bag spam] GET_FACTORY_PLAYER_IS_IN = ", GET_FACTORY_NAME_FROM_ID(GET_FACTORY_PLAYER_IS_IN(PLAYER_ID() ) ) )
			
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_biker_sell_force_bag")
//				OR serverBD.iDebugVariation2 < -1
					GIVE_BAG_TO_PLAYER()
				ENDIF
				#ENDIF
				
				/*
				IF g_SimpleInteriorData.bTriggerExit = TRUE
				OR NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_WARPED_OUT_OF_WAREHOUSE_AT_START)
				 	PRINTLN("[BIKER_SELL], Player was already outside of warehouse, setting eLOCALBITSET0_WARPED_OUT_OF_WAREHOUSE_AT_START")
					SET_LOCAL_BIT0(eLOCALBITSET0_WARPED_OUT_OF_WAREHOUSE_AT_START)
					g_SimpleInteriorData.bTriggerExit = FALSE
				ENDIF
				*/
			
				IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
				AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
				AND NOT GB_SHOULD_HIDE_GANG_BOSS_EVENT(FMMC_TYPE_BIKER_CONTRABAND_SELL)
					IF GET_END_REASON() = eENDREASON_NO_REASON_YET
						IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
						OR HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
							IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALLED_START_COMMON_TELEMETRY)
								GB_SET_COMMON_TELEMETRY_DATA_ON_START(ENUM_TO_INT(GET_SELL_VAR()), serverBd.factoryId)
								SET_LOCAL_BIT0(eLOCALBITSET0_CALLED_START_COMMON_TELEMETRY)
								PRINTLN("[BIKER_SELL] - [TELEMETRY] - [CTRB_SELL] called GB_SET_COMMON_TELEMETRY_DATA_ON_START()")
							ENDIF
														
							DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, 
														g_GBLeaderboardStruct.fmDpadStruct)
							CLIENT_PROCESS_MISSION_LOGIC()
							IF NOT g_SimpleInteriorData.bWalkingInOrOut //IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR() // url:bugstar:2825081
								HANDLE_SELL_TEXT()
								HANDLE_SELL_BLIPS()
								DRAW_HUD_BOTTOM_RIGHT()
							ENDIF
							HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
							HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
							HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
							
							MAINTAIN_SET_CRITICAL_BAG()
							MAINTAIN_CLIENT_SPOOK_CHECKS()
							MAINTAIN_CLIENT_VEHICLE_SPAWN_CHECKS()
							MAINTAIN_MUSIC_AND_SOUNDS()
							MAINTAIN_ADDITIONAL_HELP()
							MAINTAIN_RESTRICT_MAX_WANTED_LEVEL()
							MAINTAIN_PLAYER_IN_SELL_VEH()
							CLIENT_MAINTAIN_BAG_DROP_OFFS()
							
							IF BIKER_SELL_CLUB_RUN()
							AND BIKER_SELL_HAS_TWIST_HAPPENED()
								IF NOT IS_LOCAL_BIT2_SET(eLOCALBITSET2_GIVE_CLUB_RUN_WANTED)
									IF IS_SERVER_BIT1_SET(eSERVERBITSET1_CLUB_RUN_COPS)
										SET_LOCAL_BIT2(eLOCALBITSET2_GIVE_CLUB_RUN_WANTED)
										IF GET_MAX_WANTED_LEVEL() < GET_MAX_WANTED_LEVEL_FOR_SELL_CONTRABAND()
											SET_MAX_WANTED_LEVEL(GET_MAX_WANTED_LEVEL_FOR_SELL_CONTRABAND())
										ENDIF
										SET_WANTED_TO(GET_MAX_WANTED_LEVEL_FOR_SELL_CONTRABAND())
										SET_CLIENT_BIT0(eCLIENTBITSET0_KILLED_COP)
									ELSE
										SET_LOCAL_BIT2(eLOCALBITSET2_GIVE_CLUB_RUN_WANTED)
									ENDIF
								ENDIF
								IF NOT IS_LOCAL_BIT1_SET(eLOCALBITSET1_BAG_TWIST_PHONE_CALL)
									IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_PHONE_TRIGGERED)
										IF HAS_NET_TIMER_EXPIRED(sLocaldata.stCallDelayTimer, 10000)
											IF IS_SERVER_BIT1_SET(eSERVERBITSET1_CLUB_RUN_COPS)
											OR IS_SERVER_BIT1_SET(eSERVERBITSET1_CLUB_RUN_GANG)
												PRINTLN("[BIKER_SELL] [TWIST]  - HAS_NET_TIMER_EXPIRED(sLocaldata.stCallDelayTimer, 10000)")
												IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_CLUB_NOTHING)
													CLEAR_LOCAL_BIT0(eLOCALBITSET0_PHONE_TRIGGERED)
												ENDIF
											ENDIF
											SET_LOCAL_BIT1(eLOCALBITSET1_BAG_TWIST_PHONE_CALL)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF BIKER_SELL_RACE()
								GB_BIKER_RACE_MAINTAIN(sBikerRace, serverBD.sRaceServerVars, ciDELIVERY_DISTANCE)
								//If I don;t have a bag then kick me off the race. 
								IF NOT DO_I_HAVE_A_BAG()
								AND NOT GB_IS_GLOBAL_CLIENT_BIT1_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_1_KICKED_OFF_RACE)
									IF NOT GB_IS_GLOBAL_CLIENT_BIT1_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_1_FINISHED_RACE)
										GB_SET_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_KICKED_OFF_RACE)
									ENDIF
								ENDIF
								IF NOT BIK_IS_FORMATION_RESTRICTED()
									BIK_SET_FORMATION_RESTRICTED()
									CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS] - SET g_GB_WORK_VARS.iEventBitSet, ciGB_BOSS_SUPPRESS_RIDE_IN_FORMATION")
								ENDIF
							ENDIF
							
							IF BIKER_SELL_AIR_DROP_AT_SEA()
							OR BIKER_SELL_HELICOPTER_DROP()
							OR BIKER_SELL_STING()
							OR DOES_BIKER_SELL_MULTIPLE_DROP_OFFS()
								MAINTAIN_CRATE_DROP_CLIENT()
							ENDIF
							
							IF iFocusParticipant > (-1)
								IF iFocusParticipant = PARTICIPANT_ID_TO_INT()
									// I am playing, not spectating.
								ELSE
									// I am spectating not playing.
								ENDIF
							ENDIF
							
						ENDIF
						
					ENDIF
					
				ELSE
					GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_BLIP_WARN)
					Clear_Any_Objective_Text_From_This_Script() // Clear objective text if player is retricted for hide or passive.
				ENDIF
			ENDIF
		BREAK
		
		CASE eMODESTATE_REWARDS
		
			// url:bugstar:2813512 - Sell Missions - Takes quite a long time to clean up the sell mission after a fail - call was delayed for ages and couldn't get inside my warehouse
			IF GET_END_REASON() <> eENDREASON_WIN_CONDITION_TRIGGERED
				IF NOT IS_BIT_SET(g_GB_WORK_VARS.iEventBitSet, ciGB_BOSS_SELL_MISSION_FAILED)
					PRINTLN("[BIKER_SELL] CLIENT_PROCESSING, ciGB_BOSS_SELL_MISSION_FAILED")
					SET_BIT(g_GB_WORK_VARS.iEventBitSet, ciGB_BOSS_SELL_MISSION_FAILED)
				ENDIF
			ELSE
				KICK_EVERYONE_FROM_VEHICLE()
			ENDIF
			
			CLEANUP_CRATE_DROP_ALARM()
		
			// Clear any help at this stage becuase the mission has almost ended
			HELP_TEXT_CLEANUP()
			
			// to protect players from being killed whilst trying to warp
		
			SET_ILLICIT_GOODS_PLAYER()
			CLEANUP_CUSTOM_SPAWNS()
			CLEANUP_VEHICLE_CRATES()
			HANDLE_SELL_TEXT(TRUE)
			HANDLE_SELL_BLIPS(TRUE)
			HANDLE_REWARDS() // Handle end of mode rewards.
			SET_COOLDOWN_TIMER(serverBD.iStartingWarehouse) //Set the cooldown timer if successful
			CLEAR_AUDIO_FLAGS_ON_MODE_END()
			DO_RADAR_ALTIMETER(FALSE)	
			
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF IS_MP_HEIST_GEAR_EQUIPPED(PLAYER_PED_ID(), HEIST_GEAR_SPORTS_BAG)
					REMOVE_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_SPORTS_BAG)
				ENDIF
			ENDIF
			// Server will move us onto end state once all end of mode logic is complete.
			
			CLEANUP_CONTRABAND_DECORATOR()
		BREAK
		
		CASE eMODESTATE_END
		
			PRINTLN("[BIKER_SELL] CLIENT_PROCESSING, eMODESTATE_END  ")
			
		BREAK
		
	ENDSWITCH

ENDPROC

// For timed variant (removed)
PROC SERVER_GRAB_TIME_LIMIT()

	INT i
	VECTOR vDrop
	
	FLOAT fCurrentDist
	FLOAT fShortDist = 9999999.99
	
	IF serverBD.fDistDrop = -1.0
	
		// Work out closest drop-off
		REPEAT GET_MAX_SELL_DROP_OFFS() i
			
			IF IS_BIT_SET(serverBD.iActiveDropOffBitset, i)
			AND NOT IS_THIS_A_HIDDEN_DROP_OFF(i)
				vDrop = GET_SELL_DROP_COORDS(GET_SELL_VAR(), i, serverBD.iRoute)
				
				fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vDrop, VEH_POS())
				
				IF fCurrentDist < fShortDist
					fShortDist = fCurrentDist
				ENDIF
			ENDIF
		ENDREPEAT
		
		serverBD.fDistDrop = fShortDist
		PRINTLN("[BIKER_SELL] SERVER_GRAB_TIME_LIMIT, fDistDrop = ", serverBD.fDistDrop)
		
		INT iKms
		iKms = ROUND(serverBD.fDistDrop / 1000)
		
		PRINTLN("[BIKER_SELL] SERVER_GRAB_TIME_LIMIT, iKms = ", iKms)
		
		// 2 minutes plus a minute per km to the drop-off
		serverBD.iTimedTimeLimit = ( 120000 + ( 60000 * iKms ) )
		PRINTLN("[BIKER_SELL] SERVER_GRAB_TIME_LIMIT, = ", serverBD.iTimedTimeLimit)
	ENDIF
ENDPROC

PROC SERVER_VEHICLE_LOOP()
	INT i
	INT iDeliveredCount
	INT iPlayerPost
	PLAYER_INDEX playerPost
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i 
		IF HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(i)
			iDeliveredCount++
		ENDIF
		
		// Kick anyone out of the postvan that shouldn't be there
		IF BIKER_SELL_POSTMAN()
			IF bIS_POSTMAN_PAT_VEH(i)
			
				playerPost = GET_DRIVER_OF_SELL_VEH(i)
				
				IF playerPost <> INVALID_PLAYER_INDEX()
					IF IS_NET_PLAYER_OK(playerPost)
					
						iPlayerPost = NATIVE_TO_INT(playerPost)
					
						IF NOT bIS_PLAYER_POSTMAN(i, iPlayerPost)
						
							EJECT_PLAYER_FROM_VEHICLE(i, TRUE)		
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF serverBD.iVehicleCountDeliveredAllContraband <> iDeliveredCount
		serverBD.iVehicleCountDeliveredAllContraband = iDeliveredCount
	ENDIF
ENDPROC

PROC SERVER_MANAGE_BOSS_LEAVING()
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = INVALID_PLAYER_INDEX()
	AND GET_END_REASON() = eENDREASON_NO_REASON_YET
		PRINTLN("[BIKER_SELL] SERVER_PROCESSING, SERVER_MANAGE_BOSS_LEAVING, eENDREASON_NO_BOSS_LEFT  ")
		SET_END_REASON(eENDREASON_NO_BOSS_LEFT)
	ENDIF
ENDPROC

FUNC BOOL ALL_BAGS_HAVE_BEEN_DROPPED()
	INT i
	REPEAT GET_NUM_BAGS() i
		IF serverBD.iAssignedBag[i] != -1
			RETURN FALSE
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC

FUNC BOOL bSHOULD_TRIGGER_WIN()

	RETURN ( iDROPPED_OFF_COUNT() > 0 )
ENDFUNC

PROC START_BLIP_DELAY_TIMER(BOOL bIgnoreBagCheck = FALSE)
	IF MODE_HAS_PICKUP_BAGS()
	OR bIgnoreBagCheck
		IF SHOULD_DELAY_BLIP_FOR_FACTORY()
			IF NOT HAS_NET_TIMER_STARTED(serverBD.stBlipDelayTimer)
				PRINTLN("[BIKER_SELL] - START_BLIP_DELAY_TIMER - started")
				START_NET_TIMER(serverBD.stBlipDelayTimer)
			ENDIF
		ELSE
			SET_SERVER_BIT1(eSERVERBITSET1_VEHICLES_MARKED_AS_CONTRABAND)
		ENDIF
	ENDIF
ENDPROC

PROC SERVER_PROCESSING()

	MAINTAIN_CRATE_DROP_SERVER()
	SERVER_MAINTAIN_DROPS()
//	SERVER_MAINTAIN_DROP_VEHS()
	SERVER_MAINTAIN_PED_BRAINS()
	MAINTAIN_NON_CONTRABAND_VEHICLES()
	MAINTAIN_NON_CONTRABAND_PED_AND_VEHICLE_RESPAWNING()
	MAINTAIN_BLOCKING_ZONES()
	MAINTAIN_CONTRABAND_BLIP_DELAY()
	SERVER_VEHICLE_LOOP()
	SERVER_MANAGE_BOSS_LEAVING()
	BOOL bProgress = TRUE
	INT i
	SWITCH GET_MODE_STATE()
	
		CASE eMODESTATE_INIT
			
			// ********************************************
			// Initialise server data here.
			// Will remain in here until server moves on.
			// ********************************************
			
			IF CREATE_SELL_ENTITIES()
				IF NOT CREATE_DROP_OFF_VEH()
					PRINTLN("[BIKER_SELL] eMODESTATE_INIT IF NOT CREATE_DROP_OFF_VEH()")
					bProgress = FALSE
				ENDIF
				IF NOT CREATE_DROP_OFF_PEDS()
					PRINTLN("[BIKER_SELL] eMODESTATE_INIT IF NOT CREATE_DROP_OFF_PEDS()")
					bProgress = FALSE
				ENDIF
				IF NOT CREATE_START_VEHICLES()
					PRINTLN("[BIKER_SELL] eMODESTATE_INIT IF NOT CREATE_START_VEHICLES()")
					bProgress = FALSE
				ENDIF
				IF NOT CREATE_START_PEDS()
					PRINTLN("[BIKER_SELL] eMODESTATE_INIT IF NOT CREATE_START_PEDS()")
					bProgress = FALSE
				ENDIF
				IF NOT CREATE_DROP_OFF_OBJECTS()
					PRINTLN("[BIKER_SELL] eMODESTATE_INIT IF NOT CREATE_DROP_OFF_OBJECTS()")
					bProgress = FALSE
				ENDIF
				IF NOT CREATE_VEHICLE_CRATES()
					PRINTLN("[BIKER_SELL] eMODESTATE_INIT IF NOT CREATE_VEHICLE_CRATES()")
					bProgress = FALSE
				ENDIF
				
				IF bProgress 
								
					START_BLIP_DELAY_TIMER()
					
					serverBD.iLaunchPosix = GET_CLOUD_TIME_AS_INT()
					PRINTLN("[BIKER_SELL] SERVER_PROCESSING - serverBD.iLaunchPosix = ", serverBD.iLaunchPosix)
				
					// Move onto next stage.
					SET_MODE_STATE(eMODESTATE_RUN)
					
					IF DOES_VARIATION_SPAWN_PEDS_AT_START()
						REPEAT GET_NUM_PEDS_FOR_VARIATION() i
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_PED_MODEL_FOR_VARIATION(i))
						ENDREPEAT
					ENDIF
					IF DOES_MODE_NEED_DROP_OFF_PED()
						REPEAT GET_NUM_DROP_OFF_PEDS_FOR_VARIATION() i
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_DROP_OFF_PED_MODEL(i))
						ENDREPEAT
					ENDIF
					
					PRINTLN("[BIKER_SELL] SERVER_PROCESSING, eMODESTATE_INIT DONE  ")
				ENDIF
			ELSE
				PRINTLN("[BIKER_SELL] SERVER_PROCESSING, eMODESTATE_INIT waiting on CREATE_SELL_ENTITIES  ")
			ENDIF

		BREAK
		
		CASE eMODESTATE_RUN

//			PRINTLN("[BIKER_SELL] SERVER_PROCESSING, eMODESTATE_RUN  ")
			
			// Stay in run state until we have an end reason.
			IF GET_END_REASON() = eENDREASON_NO_REASON_YET
				
				// Widget for forcing time up at end of mode.
				#IF IS_DEBUG_BUILD
				IF serverBD.bFakeEndOfModeTimer
					PRINTLN("[BIKER_SELL] serverBD.bFakeEndOfModeTimer = TRUE.")
					SET_END_REASON(eENDREASON_TIME_UP)
				ENDIF
				#ENDIF
				
				// If the mode timer expires, set end reason to time up.
				IF NOT HAS_NET_TIMER_STARTED(serverBD.modeTimer)
					START_NET_TIMER(serverBD.modeTimer)
				ELSE
					
					IF HAS_NET_TIMER_EXPIRED(serverBD.modeTimer, iTIME_LIMIT())
						PRINTLN("[BIKER_SELL] SERVER_PROCESSING, eENDREASON_TIME_UP, HAS_NET_TIMER_EXPIRED  ", iTIME_LIMIT())
						
						SET_END_REASON(eENDREASON_TIME_UP)
						
					// Send warning text
					ELIF HAS_NET_TIMER_EXPIRED(serverBD.modeTimer, ciWARNING_TIMER)
						IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_TIME_TEXT)
							PRINTLN("[BIKER_SELL] SERVER_PROCESSING, eSERVERBITSET0_TIME_TEXT  ")
							SET_SERVER_BIT0(eSERVERBITSET0_TIME_TEXT)
						ENDIF
					ENDIF
				ENDIF
				
				// Veh destroyed
				REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i 
					IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEH_MADE)
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
						AND NOT IS_SELL_VEH_OK(i)
						
							IF BIKER_SELL_BENSON()
								IF NOT IS_BIT_SET(serverBD.iPenaltyBitSet, i)
									SERVER_INCREASE_DROP_PENALTY()
									SET_BIT(serverBD.iPenaltyBitSet, i)
									PRINTLN("[BIKER_SELL] [3015277] SERVER_PROCESSING, serverBD.iPenaltyBitSet = ", i)
								ENDIF							
							ENDIF
						
							IF BIKER_SELL_TRASHMASTER()
							OR BIKER_SELL_POSTMAN()
								IF NOT IS_BIT_SET(serverBD.iPenaltyBitSet, i)
									SERVER_INCREASE_DROP_PENALTY(GET_SELL_VEH_TARGET_DROP_COUNT(i) - serverBD.iVehDropCount[i] - serverBD.iVehPenalty[i])
									SET_BIT(serverBD.iPenaltyBitSet, i)
									PRINTLN("[BIKER_SELL] [3015277] SERVER_PROCESSING, serverBD.iPenaltyBitSet = ", i)
								ENDIF							
							ENDIF
							
							IF NOT IS_BIT_SET(serverBD.iFailVehicle, i)
								IF NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(i)
									SET_BIT(serverBD.iFailVehicle, i)
									SERVER_CLEAR_VEH_ACTIVE_DROPS(i)
									PRINTLN("[BIKER_SELL] SERVER_PROCESSING, serverBD.iFailVehicle = ", i, " - SET_BIT(serverBD.iFailVehicle, ", i, ")")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
				INT iCount, iWinCount 
				INT iModeTarget
				IF MODE_HAS_PICKUP_BAGS()
					iModeTarget = GET_NUM_BAGS()
				ELSE
					iModeTarget = GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE()
				ENDIF
				REPEAT iModeTarget  i 
					// Fail if all vehicles destroyed
					IF IS_BIT_SET(serverBD.iFailVehicle, i)
						iCount++
					ENDIF
					// Pass if all remaining vehicles have been delivered
					IF IS_BIT_SET(serverBD.iFailVehicle, i)
					OR HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(i)
						iWinCount++
					ENDIF
				ENDREPEAT
				
				IF iCount = iModeTarget
					IF NOT HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
						IF GET_END_REASON() != eENDREASON_TRUCK_DESTROYED
							SET_END_REASON(eENDREASON_TRUCK_DESTROYED)
							#IF IS_DEBUG_BUILD
							IF serverBD.iFailVehicle !=  0 
								REPEAT iModeTarget i 
									IF IS_BIT_SET(serverBD.iFailVehicle, i)
										PRINTLN("[BIKER_SELL] SERVER_PROCESSING, serverBD.iFailVehicle = ", i)
									ENDIF
								ENDREPEAT
							ENDIF
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF MODE_HAS_PICKUP_BAGS()
					IF ALL_BAGS_HAVE_BEEN_DROPPED()
						IF NOT HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
							IF GET_END_REASON() != eENDREASON_TRUCK_DESTROYED
								SET_END_REASON(eENDREASON_TRUCK_DESTROYED)
								PRINTLN("[BIKER_SELL] SERVER_PROCESSING, eENDREASON_TRUCK_DESTROYED, all bags gone  ", serverBD.iDropPenalty)
							ENDIF
						ENDIF				
					ENDIF
				ENDIF
				
				// Ped killed
				IF BIKER_SELL_PROVEN()
					IF GET_END_REASON() != eENDREASON_PED_KILLED
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[0].netIndex)
						AND IS_ENTITY_DEAD(NET_TO_PED(serverBD.sPed[0].netIndex))
							PRINTLN("[BIKER_SELL] SERVER_PROCESSING, eENDREASON_PED_KILLED  ")
							SET_END_REASON(eENDREASON_PED_KILLED)
						ENDIF
					ENDIF
				ENDIF
				
//				IF BIKER_SELL_EXCHANGE()
//					IF GET_END_REASON() != eENDREASON_PED_KILLED
//						IF IS_SERVER_BIT1_SET(eSERVERBITSET1_PED_SHOT_AT)
//							PRINTLN("[BIKER_SELL] SERVER_PROCESSING, eENDREASON_PED_KILLED  ")
//							SET_END_REASON(eENDREASON_PED_KILLED)
//						ENDIF
//					ENDIF
//				ENDIF

				IF IS_SERVER_BIT0_SET(eSERVERBITSET0_PLAYERS_FINISHED)
				OR (iWinCount = iModeTarget)
					IF GET_END_REASON() = eENDREASON_NO_REASON_YET
					
						IF bSHOULD_TRIGGER_WIN()
					
							PRINTLN("[BIKER_SELL] [3015277] SERVER_PROCESSING, eENDREASON_WIN_CONDITION_TRIGGERED, iWinCount = ", iWinCount, " GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE = ", 
									iModeTarget, " iDROPPED_OFF_COUNT = ", iDROPPED_OFF_COUNT())
									
							SET_END_REASON(eENDREASON_WIN_CONDITION_TRIGGERED)
						ELSE
							PRINTLN("[BIKER_SELL] [3015277] SERVER_PROCESSING, eENDREASON_PED_KILLED, iDROPPED_OFF_COUNT() = ", iDROPPED_OFF_COUNT())
							SET_END_REASON(eENDREASON_PED_KILLED)
						ENDIF
					ENDIF
				ENDIF

			ELSE
				PRINTLN("[BIKER_SELL] [SERVER] SERVER_PROCESSING,  GET_END_REASON_NAME = ", GET_END_REASON_NAME(GET_END_REASON()))
				// Move onto rewards once we have an end reason (therefore mode has ended). 
				SET_MODE_STATE(eMODESTATE_REWARDS)
			ENDIF
			
			MAINTAIN_SELL_VEHICLES_STUCK_AFTER_CREATION()
		BREAK
		
		CASE eMODESTATE_REWARDS
		
			//PRINTLN("[BIKER_SELL] SERVER_PROCESSING, eMODESTATE_REWARDS  ")
			
			// Once all clients have handled the end of the mode, allow the server to move to the end. 
			IF IS_SERVER_BIT0_SET(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
				PRINTLN("[BIKER_SELL] - [REWARDS] - all participants completed rewards.")
				SET_MODE_STATE(eMODESTATE_END)
			ENDIF
			
		BREAK
		
		CASE eMODESTATE_END
		
			PRINTLN("[BIKER_SELL] SERVER_PROCESSING, eMODESTATE_END  ")
			// Do anything required at very end of mode once rewards and gameplay are opver, then end.
			SET_SERVER_GAME_STATE(GAME_STATE_END)
		BREAK
		
	ENDSWITCH

ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC VEHICLE_CLEANUP()

	IF BIKER_SELL_HELICOPTER_DROP()
		IF g_bBikerInSellHeli = TRUE
			PRINTLN("[BIKER_SELL] - VEHICLE_CLEANUP - g_bBikerInSellHeli = FALSE")
			g_bBikerInSellHeli = FALSE
			SET_DISABLE_OFFICE_LANDING_FOR_LOCAL_PLAYER(FALSE)
		ENDIF
	ENDIF
	
	VEHICLE_INDEX vehId
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
		
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_SELL_VEH_MODEL())
			
			vehId = NET_TO_VEH(serverBD.niVeh[i])
		
			IF DOES_ENTITY_EXIST(VEH_ENT(i))
				IF NETWORK_HAS_CONTROL_OF_ENTITY(VEH_ENT(i))
				OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.niVeh[i]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
					IF TAKE_CONTROL_OF_ENTITY(VEH_ENT(i))
						IF NOT IS_ENTITY_DEAD(VEH_ENT(i))
							IF NOT IS_THIS_AN_AIR_VARIATION()
							AND NOT IS_THIS_A_SEA_VARIATION()
								BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(VEH_ID(i))
							ENDIF
							GB_CLEAR_VEHICLE_AS_CONTRABAND(VEH_ID(i))
						ENDIF
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehId)
						PRINTLN("[BIKER_SELL] SCRIPT_CLEANUP, SET_VEHICLE_AS_NO_LONGER_NEEDED ")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC DROP_VEHICLE_CLEANUP()

	
	VEHICLE_INDEX vehId
	INT i
	REPEAT GET_NUM_DROP_OFF_VEH_FOR_VARIATION() i
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_DROP_OFF_VEH_MODEL(i))
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffVeh[i])
		
			vehId = NET_TO_VEH(serverBD.niDropOffVeh[i])
		
			IF DOES_ENTITY_EXIST(vehId)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(vehId)
				OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.niDropOffVeh[i]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
					IF TAKE_CONTROL_OF_ENTITY(vehId)
						IF IS_VEHICLE_DRIVEABLE(vehId)
							SET_ENTITY_INVINCIBLE(vehId, FALSE)
							SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehId, FALSE)
							SET_ENTITY_CAN_BE_DAMAGED(vehId, TRUE)
						ENDIF
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehId)
						PRINTLN("[BIKER_SELL] SCRIPT_CLEANUP, DROP_VEHICLE_CLEANUP ")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC DROP_PED_CLEANUP()
	INT i
	
	REPEAT GET_NUM_DROP_OFF_PEDS_FOR_VARIATION() i

		SET_MODEL_AS_NO_LONGER_NEEDED(GET_DROP_OFF_PED_MODEL( i))
		ENTITY_INDEX ent
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffPed[i])	
			ent = NET_TO_ENT(serverBD.niDropOffPed[i])
			IF DOES_ENTITY_EXIST(ent)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(ent)
					IF IS_ENTITY_ALIVE(NET_TO_PED(serverBD.niDropOffPed[i]))
						SET_ENTITY_INVINCIBLE(NET_TO_PED(serverBD.niDropOffPed[i]), FALSE)
						SET_ENTITY_CAN_BE_DAMAGED(NET_TO_PED(serverBD.niDropOffPed[i]), TRUE)
					ENDIF
					PRINTLN("[BIKER_SELL] SCRIPT_CLEANUP, DROP_PED_CLEANUP - ", i)
					SET_ENTITY_AS_NO_LONGER_NEEDED(ent)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC AI_PED_CLEANUP()
	INT i
	
	REPEAT GET_NUM_PEDS_FOR_VARIATION() i
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_PED_MODEL_FOR_VARIATION( i))
		ENTITY_INDEX ent
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[i].netIndex)	
			ent = NET_TO_ENT(serverBD.sPed[i].netIndex)
			IF DOES_ENTITY_EXIST(ent)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(ent)
					IF NOT IS_NET_PED_INJURED(serverBD.sPed[i].netIndex)
						SET_ENTITY_INVINCIBLE(NET_TO_PED(serverBD.sPed[i].netIndex), FALSE)
						SET_ENTITY_CAN_BE_DAMAGED(NET_TO_PED(serverBD.sPed[i].netIndex), TRUE)
					ENDIF
					SET_ENTITY_AS_NO_LONGER_NEEDED(ent)
					PRINTLN("[BIKER_SELL] SCRIPT_CLEANUP, DROP_PED_CLEANUP - ", i)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC NON_CONTRABAND_VEHICLE_CLEANUP()
	INT i
	
	REPEAT GET_NUM_ATTACK_VEHICLES_FOR_VARIATION() i
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_VEHICLE_MODEL_FOR_VARIATION())
		ENTITY_INDEX ent
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[i].netIndex)	
			ent = NET_TO_ENT(serverBD.sPedVeh[i].netIndex)
			IF DOES_ENTITY_EXIST(ent)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(ent)
					IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sPedVeh[i].netIndex)
						SET_ENTITY_INVINCIBLE(ent, FALSE)
						SET_ENTITY_CAN_BE_DAMAGED(ent, TRUE)
					ENDIF
					SET_ENTITY_AS_NO_LONGER_NEEDED(ent)
					PRINTLN("[BIKER_SELL] SCRIPT_CLEANUP, DROP_PED_CLEANUP - ", i)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


PROC REMOVE_ALL_SELL_PA_TEXT_MESSAGES()
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER(sOPEN_TEXT())
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_6")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_7")
	//DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_2")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_1")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("BCONTRA_TXT_9")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_8")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_10")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("BK_SELL_TXTM_1")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_12")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_13")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_14")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_15")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_16")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_17")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_18")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_19")
	
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SBIKERP_NWBY")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SBIKERP_HIGH")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_21")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("BK_SELL_TXTM_2")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SBIKERP_HLP3")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("BK_SELL_TXTM_3")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("BK_SELL_TXTM_1")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("BK_SELL_TXTM_0")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SBIKERS_TXTG1")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("BSELL_ALTRUIST")
ENDPROC

BOOL bGangBossOnStart

FUNC INT GET_COUNT_OF_DESTROYED_CONTRABAND()
	IF GET_END_REASON() = eENDREASON_TRUCK_DESTROYED
		IF serverBD.iFailVehicle != 0
			INT iReturn = iDROP_TARGET()
			IF IS_BIT_SET(serverBD.iFailVehicle, 0)
				iReturn -= serverBD.iVehDropCount[0]
			ENDIF
			IF IS_BIT_SET(serverBD.iFailVehicle, 1)
				iReturn -= serverBD.iVehDropCount[1]
			ENDIF
			IF IS_BIT_SET(serverBD.iFailVehicle, 2)
				iReturn -= serverBD.iVehDropCount[2]
			ENDIF
			IF IS_BIT_SET(serverBD.iFailVehicle, 3)
				iReturn -= serverBD.iVehDropCount[3]
			ENDIF
			RETURN iReturn
		ENDIF
		RETURN (iDROP_TARGET() - serverBD.iDroppedOffCount)
	ENDIF
	RETURN 0
ENDFUNC

PROC SCRIPT_CLEANUP(BOOL bSetDoDissconnectHelp = FALSE)

	PRINTLN("[BIKER_SELL] SCRIPT_CLEANUP")
	SET_CARRYING_CRITICAL_ENTITY(FALSE)
	SET_SELL_READY_GLOBAL(FALSE)
	DETACH_PARCEL()
	CLEANUP_STONED_EFFECT()
	CLEANUP_BLUNT_SMOKE()
	Make_Ped_Sober(PLAYER_PED_ID())
	SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
	SET_PLAYER_RIVAL_ENTITY_TYPE(eRIVALENTITYTYPE_INVALID)
	BLOCK_PED_SHUFFLE(FALSE)
	REMOVE_ANIM_DICT(GET_POST_ANIM_DICT_NAME())
	GB_BIKER_RACE_CLEAN_UP()
	IF sLocaldata.bSetupScenarioBlocking
		REMOVE_SCENARIO_BLOCKING_AREA(sLocalData.ScenarioBlockingIndex)
	ENDIF
	IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_EXECUTED_END_TELEMTRY)
		IF  IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALLED_START_COMMON_TELEMETRY)
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(bIWon, ENUM_TO_INT(GET_END_REASON()), serverBD.factoryId, FALSE, serverBD.iDropPenalty, serverBD.iDroppedOffCount)
		ENDIF
	ENDIF
	IF IS_MP_HEIST_GEAR_EQUIPPED(PLAYER_PED_ID(), HEIST_GEAR_SPORTS_BAG)
		REMOVE_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_SPORTS_BAG)
	ENDIF
	
	IF bSetDoDissconnectHelp
	AND bGangBossOnStart
		SET_BIT(MPGlobalsAmbience.iFmGbHelpBitSet2, BI_FM_GANG_BOSS_HELP_2_DO_DISSCONNECT_HELP_BIKER)
		PRINTLN("[BIKER_SELL] [INIT_CLIENT] SET_BIT(MPGlobalsAmbience.iFmGbHelpBitSet2, BI_FM_GANG_BOSS_HELP_2_DO_DISSCONNECT_HELP_BIKER)")
	ENDIF
	IF IS_LOCAL_BIT1_SET(eLOCALBITSET1_GIVEN_WANTED)
		SET_DISABLE_NO_WANTED_LEVEL_GAIN_ZONES(FALSE)
	ENDIF
	
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)			
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)	
	
	IF MODE_HAS_PICKUP_BAGS()
		REMOVE_CLIP_SET("move_m@bag")
		RESET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID())
	ENDIF
	
	GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_AIR_DROP) 
	GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER)
	GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER_P)
	GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_EXPLAIN_CONTINUE_DESTROY)
	CLEAR_ANY_MISSION_HELP()
	REMOVE_ALL_BLIPS()
	CLEANUP_PLANE_FX()
	CLEANUP_ALL_CRATE_FLARES()
	CLEANUP_CRATES()
	CLEANUP_CRATE_DROP_ALARM()
	REMOVE_ALL_SELL_PA_TEXT_MESSAGES()
	CLEAR_AUDIO_FLAGS_ON_MODE_END()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF SHOULD_RESTRICT_MAX_WANTED_LEVEL()
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bNoCopsActive)
				SET_MAX_WANTED_LEVEL(5)
				PRINTLN("[BIKER_SELL] [SCRIPT_CLEANUP] SET_MAX_WANTED_LEVEL(5)")
			ENDIF
		ENDIF
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF(TRUE)
			GB_SET_LOCAL_BIKER_CALL_BIT(eGB_LOCAL_BIKER_CALL_BITSET_NOTIFICATION_SELL_PASS)
			PRINTLN("[BIKER_SELL] [GB_MAINTAIN_FAIL_CALLS] - eGB_LOCAL_BIKER_CALL_BITSET_NOTIFICATION_SELL_PASS")
		ELIF NOT HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF(TRUE)
		AND serverBD.iDroppedOffCount > 0
			GB_SET_LOCAL_BIKER_CALL_BIT(eGB_LOCAL_BIKER_CALL_BITSET_NOTIFICATION_SELL_OVER)
			PRINTLN("[BIKER_SELL] [GB_MAINTAIN_FAIL_CALLS] - eGB_LOCAL_BIKER_CALL_BITSET_NOTIFICATION_SELL_OVER")
		ELSE
			GB_SET_LOCAL_BIKER_CALL_BIT(eGB_LOCAL_BIKER_CALL_BITSET_NOTIFICATION_SELL_FAIL)
			PRINTLN("[BIKER_SELL] [GB_MAINTAIN_FAIL_CALLS] - eGB_LOCAL_BIKER_CALL_BITSET_NOTIFICATION_SELL_FAIL")
		ENDIF
	ENDIF
	
	//Kick players out the vehicle or damage it if they are in it
	IF GET_END_REASON()!= eENDREASON_NO_REASON_YET
		INT i
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
			AND NOT IS_ENTITY_DEAD(VEH_ID(i))
				IF IS_THIS_AN_AIR_VARIATION()
					PRINTLN("[BIKER_SELL] [SCRIPT_CLEANUPLOCK_SELL_VEH] bAIR_VARIATION, i = ", i)
					IF IS_ENTITY_IN_AIR(VEH_ENT(i))
						IF NETWORK_HAS_CONTROL_OF_ENTITY(VEH_ENT(i))
							PRINTLN("[BIKER_SELL] [SCRIPT_CLEANUPLOCK_SELL_VEH] bAIR_VARIATION, IS_ENTITY_IN_AIR ")
							IF GET_END_REASON() <> eENDREASON_WIN_CONDITION_TRIGGERED
								SET_VEHICLE_ENGINE_ON(VEH_ID(i), FALSE, TRUE)							
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[BIKER_SELL] [SCRIPT_CLEANUPLOCK_SELL_VEH] bAIR_VARIATION, IS_ENTITY_IN_AIR(false) ")
						EJECT_PLAYER_FROM_VEHICLE(i, TRUE)
						LOCK_SELL_VEH(i, TRUE)
					ENDIF
				ELIF GET_END_REASON() != eENDREASON_WIN_CONDITION_TRIGGERED	
				OR ( IS_THIS_A_LAND_VARIATION() AND GET_END_REASON() = eENDREASON_WIN_CONDITION_TRIGGERED )	
				OR ( IS_THIS_A_LAND_VARIATION() AND GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION() )
					PRINTLN("[BIKER_SELL] [SCRIPT_CLEANUPLOCK_SELL_VEH] backup ")
	
					EJECT_PLAYER_FROM_VEHICLE(i)
					LOCK_SELL_VEH(i, TRUE)
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		IF GET_SELL_VEH_I_AM_IN_INT() != -1
			EJECT_PLAYER_FROM_VEHICLE(GET_SELL_VEH_I_AM_IN_INT(), TRUE)
			IF ( IS_THIS_A_LAND_VARIATION() AND GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION() )
				LOCK_SELL_VEH(GET_SELL_VEH_I_AM_IN_INT(), TRUE)
				PRINTLN("[BIKER_SELL] [SCRIPT_CLEANUPLOCK_SELL_VEH] GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION ")
			ENDIF
		ENDIF
	ENDIF

	VEHICLE_CLEANUP()
	DROP_VEHICLE_CLEANUP()
	DROP_PED_CLEANUP()
	AI_PED_CLEANUP()
	NON_CONTRABAND_VEHICLE_CLEANUP()
	REMOVE_TRAFFIC_REDUCTION_ZONE(serverBD.iTrafficReductionIndex)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	IF MODE_HAS_PICKUP_BAGS()
		SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(GET_BAG_MODEL(), -1)
	ENDIF
	
	SET_SUPPRESS_GANG_CHASE_MODELS_FOR_VARIATION( FALSE)
	//
	
	IF BIKER_SELL_AIR_DROP_AT_SEA()
		IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_PLAY_ALTITUDE_WARNING_SOUND)
			STOP_SOUND(sLocaldata.iAltitudeWarningSound)
			CLEAR_LOCAL_BIT0(eLOCALBITSET0_PLAY_ALTITUDE_WARNING_SOUND)
			PRINTLN("[BIKER_SELL] [SCRIPT_CLEANUP] Stop warning sound")
		ENDIF
		
		RELEASE_SOUND_ID(sLocaldata.iAltitudeWarningSound)
	ENDIF
	
	GB_SET_BIKER_SELL_VARIATION_LOCAL_PLAYER_IS_ON(-1)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		
		PROCESS_CURRENT_BOSS_CONTRABAND_PLAYSTATS(	ENUM_TO_INT(GET_SELL_VAR()), 
													serverBD.iRoute, 
													serverBD.iShipmentSize, 
													serverBD.iStartingWarehouse, 
													serverBD.iStartingContraband, 
													serverBD.dropOffLocationBitSet,
													DEFAULT,
													GET_COUNT_OF_DESTROYED_CONTRABAND(),
													GET_COUNT_OF_DELIVERED_CONTRABAND())
		PRINTLN("[BIKER_SELL] - [TELEMETRY] - [CTRB_SELL] called PROCESS_CURRENT_BOSS_CONTRABAND_PLAYSTATS")
	ENDIF
	
	GB_COMMON_BOSS_MISSION_CLEANUP(DEFAULT, serverBD.factoryId)	
	
	Clear_Any_Objective_Text_From_This_Script()
	g_bLaunchedMissionFrmMCT = FALSE
	
	PRINTLN("[BIKER_SELL] * END ", BIKER_SELL_GET_VARIATION_DEBUG_PRINT(GET_SELL_VAR()))
	TERMINATE_THIS_THREAD()
ENDPROC

FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	
	GB_BIKER_RACE_PROCESS_PREGAME(serverBD.sRaceServerVars)
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission), missionScriptArgs)
	INT iLoop
	REPEAT ciMAX_CONTRABAND_PED_VEHICLES iLoop
		serverBD.iDropOffAiVehShouldUse[iLoop] = -1
	ENDREPEAT
	REPEAT ciMAX_SELL_BAGS iLoop
		serverBD.iAssignedBag[iLoop] = -1
	ENDREPEAT

	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(GB_GET_BOSS_MISSION_NUM_OBJ_REQUIRED(FMMC_TYPE_BIKER_CONTRABAND_SELL))
	RESERVE_NETWORK_MISSION_PEDS(GB_GET_BOSS_MISSION_NUM_PEDS_REQUIRED(FMMC_TYPE_BIKER_CONTRABAND_SELL))
	RESERVE_NETWORK_MISSION_VEHICLES(GB_GET_BOSS_MISSION_NUM_VEH_REQUIRED(FMMC_TYPE_BIKER_CONTRABAND_SELL))
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD,SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
//	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//		g_iContraSellMissionCratesToSell = 5
//		g_iContraSellMissionBuyerID = 2
//	ENDIF
	
	PRINTLN("[BIKER_SELL] PROCESS_PRE_GAME g_iContraSellMissionBuyerID = ", g_iContraSellMissionBuyerID, " g_iContraSellMissionCratesToSell = ", g_iContraSellMissionCratesToSell)
	
	sLocaldata.contraData = GB_GET_PLAYER_ILLICIT_GOOD_MISSION_DATA()
	
	GB_COMMON_BOSS_MISSION_PREGAME()
	
	#IF IS_DEBUG_BUILD
	PARTICIPANT_INDEX piHost = NETWORK_GET_HOST_OF_THIS_SCRIPT()
	PLAYER_INDEX playerHost = NETWORK_GET_PLAYER_INDEX(piHost)
	STRING sHost
	IF IS_NET_PLAYER_OK(playerHost)
		sHost = GET_PLAYER_NAME(playerHost) 
		PRINTLN("[BIKER_SELL], PROCESS_PRE_GAME, HOST_IS_CALLED ", sHost, " MODE = ", BIKER_SELL_GET_VARIATION_DEBUG_PRINT(GET_SELL_VAR()))
	ENDIF
	DEBUG_PRINT_PLAYERS()
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC


PROC RESTART_DAMAGE_TIMER()
	IF NOT HAS_NET_TIMER_STARTED(sLocaldata.localDamageTimer)
		START_NET_TIMER(sLocaldata.localDamageTimer)
		PRINTLN("[BIKER_SELL] [BIKER_SELL DAMAGE] [DMG]  - Starting damage timer.")
	ELSE
		REINIT_NET_TIMER(sLocaldata.localDamageTimer)
		START_NET_TIMER(sLocaldata.localDamageTimer)
		PRINTLN("[BIKER_SELL] [BIKER_SELL DAMAGE] [DMG]  - Resetting damage timer.")
	ENDIF
ENDPROC

CONST_INT ciVEH_DMG_MIN		1

// Cap url:bugstar:2794712
FUNC INT iDAMAGE_AMOUNT()

	INT iDamage
	
	iDamage = GET_RANDOM_INT_IN_RANGE(g_sMPTunables.iexec_sell_nodamage_min_value_reduction, ( g_sMPTunables.iexec_sell_nodamage_max_value_reduction  + 1 ) )
	
//	IF iDamage > g_sMPTunables.iexec_sell_nodamage_value_reduction
//		iDamage = g_sMPTunables.iexec_sell_nodamage_value_reduction
//	ENDIF

	RETURN iDamage
ENDFUNC

PROC SEND_CONTRABAND_DESTROYED_TICKER(PLAYER_INDEX destroyer)
	
	SCRIPT_EVENT_DATA_TICKER_MESSAGE sData
	
	sData.TickerEvent = TICKER_EVENT_BIKER_SELL_DESTROYED
	sData.playerID = destroyer
		
	INT iPlayer, iPlayerFlags
	
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(iPlayer))
			IF IS_NET_PLAYER_OK(destroyer, FALSE)
			
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(destroyer)
					SET_BIT(iPlayerFlags, iPlayer)
				ELSE
					IF NOT GB_IS_PLAYER_ON_CONTRABAND_MISSION(destroyer)
						SET_BIT(iPlayerFlags, iPlayer)
					ENDIF 
				ENDIF
			
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iPlayerFlags != 0
		BROADCAST_TICKER_EVENT(sData, iPlayerFlags)
	ENDIF
	
ENDPROC

PROC HANDLE_BONUS_REDUCTION(INT iVeh)

	// 10% of whatever each vehicle is carrying gets deducted
	INT iCashToDeductPerVeh = CEIL((GET_MODE_TUNEABLE_BORDER_PATROL_BONUS_CASH_TOTAL()/GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() * g_sMPTunables.fBiker_Sell_Border_Patrol_Bonus_Reduction))
	PRINTLN("[BIKER_SELL] - HANDLE_BONUS_REDUCTION - Start ***********************************************************")
	PRINTLN("[BIKER_SELL] - HANDLE_BONUS_REDUCTION - Affected vehicle: ", iVeh)
	PRINTLN("[BIKER_SELL] - HANDLE_BONUS_REDUCTION - Cash to deduct per vehicle: ", iCashToDeductPerVeh)
	
	PRINTLN("[BIKER_SELL] - HANDLE_BONUS_REDUCTION - Values before deduction: ")
	PRINTLN("[BIKER_SELL] - HANDLE_BONUS_REDUCTION - Total bonus: ", serverBD.iBonus[0] + serverBD.iBonus[1] + serverBD.iBonus[2])
	PRINTLN("[BIKER_SELL] - HANDLE_BONUS_REDUCTION - Veh 0 bonus: ", serverBD.iBonus[0])
	PRINTLN("[BIKER_SELL] - HANDLE_BONUS_REDUCTION - Veh 1 bonus: ", serverBD.iBonus[1])
	PRINTLN("[BIKER_SELL] - HANDLE_BONUS_REDUCTION - Veh 2 bonus: ", serverBD.iBonus[2])
	serverBD.iBonus[iVeh] -= iCashToDeductPerVeh
	PRINTLN("[BIKER_SELL] - HANDLE_BONUS_REDUCTION - Values after deduction: ")
	PRINTLN("[BIKER_SELL] - HANDLE_BONUS_REDUCTION - Total bonus: ", serverBD.iBonus[0] + serverBD.iBonus[1] + serverBD.iBonus[2])
	PRINTLN("[BIKER_SELL] - HANDLE_BONUS_REDUCTION - Veh 0 bonus: ", serverBD.iBonus[0])
	PRINTLN("[BIKER_SELL] - HANDLE_BONUS_REDUCTION - Veh 1 bonus: ", serverBD.iBonus[1])
	PRINTLN("[BIKER_SELL] - HANDLE_BONUS_REDUCTION - Veh 2 bonus: ", serverBD.iBonus[2])
	PRINTLN("[BIKER_SELL] - HANDLE_BONUS_REDUCTION - End *************************************************************")
ENDPROC

FUNC BOOL IS_PED_A_COP(PED_INDEX pedId)
	IF DOES_ENTITY_EXIST(pedId)
		IF GET_PED_TYPE(pedId) = PEDTYPE_COP
		OR GET_PED_TYPE(pedId) = PEDTYPE_ARMY
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC
FUNC BOOL IS_MODEL_A_COP_VEHICLE(MODEL_NAMES model)
	IF model = POLICE
	OR model = POLICE2
	OR model = POLICE3
	OR model = POLICE4
	OR model = POLICET
	OR model = POLMAV
	OR model = RIOT
	OR model = FBI
	OR model = FBI2
	OR model = SHERIFF
	OR model = SHERIFF2
	OR model = BARRACKS
	OR model = BARRACKS2
	OR model = BARRACKS3
	OR model = CRUSADER
	OR model = TANK
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL HAS_PLAYER_DAMAGED_A_VALID_COP_VEHICLE(ENTITY_INDEX vehID, PED_INDEX driverId, PED_INDEX passengerId)

	IF DOES_ENTITY_EXIST(driverId)
	AND IS_PED_A_PLAYER(driverId)
		RETURN FALSE
	ENDIF
	
	IF DOES_ENTITY_EXIST(passengerId)
	AND IS_PED_A_PLAYER(passengerId)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_A_COP(driverId)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_A_COP(passengerId)
		RETURN TRUE
	ENDIF
	
	IF IS_MODEL_A_COP_VEHICLE(GET_ENTITY_MODEL(vehID))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC eCLIENT_BITSET_0 GET_DESTROYED_EVENT_BIT_TO_SET(INT iVeh)
	
	SWITCH iVeh
		CASE 0	RETURN eCLIENTBITSET0_SERVER_SHOULD_SEND_DESTROYED_EVENT_VEH_0
		CASE 1	RETURN eCLIENTBITSET0_SERVER_SHOULD_SEND_DESTROYED_EVENT_VEH_1
		CASE 2	RETURN eCLIENTBITSET0_SERVER_SHOULD_SEND_DESTROYED_EVENT_VEH_2
		CASE 3	RETURN eCLIENTBITSET0_SERVER_SHOULD_SEND_DESTROYED_EVENT_VEH_3
		CASE 4	RETURN eCLIENTBITSET0_SERVER_SHOULD_SEND_DESTROYED_EVENT_VEH_4
		CASE 5	RETURN eCLIENTBITSET0_SERVER_SHOULD_SEND_DESTROYED_EVENT_VEH_5
		CASE 6	RETURN eCLIENTBITSET0_SERVER_SHOULD_SEND_DESTROYED_EVENT_VEH_6
		CASE 7	RETURN eCLIENTBITSET0_SERVER_SHOULD_SEND_DESTROYED_EVENT_VEH_7
	ENDSWITCH
	
	RETURN eCLIENTBITSET0_END
ENDFUNC

PROC PROCESS_DAMAGE(INT iCount)
	
	PED_INDEX VictimPedID	
	PED_INDEX DamagerPedID	
	PLAYER_INDEX playerDamager
	PLAYER_INDEX VictimPlayerID
	VEHICLE_INDEX VictimVehicleID
	PARTICIPANT_INDEX participantId
//	VEHICLE_INDEX DamagerVehicleID
	INT i
	INT iMAX_WANTED_RATING = GET_MAX_WANTED_LEVEL_FOR_SELL_CONTRABAND()

	STRUCT_ENTITY_DAMAGE_EVENT sEntityID
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEntityID, SIZE_OF(sEntityID))
		PRINTLN("2952950, PROCESS_DAMAGE 0 ")
		// IF the victim exists
		IF DOES_ENTITY_EXIST(sEntityID.VictimIndex)
			PRINTLN("2952950, PROCESS_DAMAGE 1 ")

			// IF the victim is a vehicle
			IF IS_ENTITY_A_VEHICLE(sEntityID.VictimIndex)
			
				PRINTLN("2952950, PROCESS_DAMAGE 2 ")
			
				VictimVehicleID = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
				
				REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
					IF VictimVehicleID = VEH_ID(i)
					
						PRINTLN("2952950, PROCESS_DAMAGE 3 ")
		
						IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
							PRINTLN("2952950, PROCESS_DAMAGE 4 ")
							IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
								PRINTLN("2952950, PROCESS_DAMAGE 5 ")
								DamagerPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
								IF IS_PED_A_PLAYER(DamagerPedID)
									playerDamager = NETWORK_GET_PLAYER_INDEX_FROM_PED(DamagerPedID)
									PRINTLN("2952950, PROCESS_DAMAGE 6 ")
									
									IF playerDamager = PLAYER_ID()
										IF GET_MAX_WANTED_LEVEL() < iMAX_WANTED_RATING
										OR GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 1
											PED_INDEX pedDriverId = GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex), VS_DRIVER)
											PED_INDEX pedPassId = GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex), VS_FRONT_RIGHT)
											IF HAS_PLAYER_DAMAGED_A_VALID_COP_VEHICLE(sEntityID.VictimIndex, pedDriverId, pedPassId)	
												SET_MAX_WANTED_LEVEL(iMAX_WANTED_RATING)
												IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 1
													SET_WANTED_TO(1)
												ENDIF
												SET_CLIENT_BIT0(eCLIENTBITSET0_KILLED_COP)
												PRINTLN("2952950, SET_MAX_WANTED_LEVEL(iMAX_WANTED_RATING) ")
											ENDIF
										ENDIF
									ENDIF
									
									IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), GET_DESTROYED_EVENT_BIT_TO_SET(i))
										PRINTLN("2952950, PROCESS_DAMAGE 7 ")
										IF NOT IS_SELL_VEH_OK(i)
										OR sEntityID.VictimDestroyed
											PRINTLN("2952950, PROCESS_DAMAGE 8 ")
											IF GET_END_REASON() = eENDREASON_NO_REASON_YET
												PRINTLN("2952950, PROCESS_DAMAGE 9 ")
												IF NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(i)
													
													playerBD[PARTICIPANT_ID_TO_INT()].damagerPlayer = playerDamager
													PRINTLN("2952950, PROCESS_DAMAGE 10 ", GET_PLAYER_NAME(playerDamager))
													SET_CLIENT_BIT0(GET_DESTROYED_EVENT_BIT_TO_SET(i))
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
			ELIF IS_ENTITY_A_PED(sEntityID.VictimIndex)
				IF sEntityID.VictimDestroyed
					VictimPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
					
					IF IS_PED_A_PLAYER(VictimPedID)
						VictimPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(VictimPedID) 
						
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(VictimPlayerID)
							IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
								PRINTLN("2952950, PROCESS_DAMAGE A ")
								IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
									PRINTLN("2952950, PROCESS_DAMAGE B ")
									DamagerPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
									IF IS_PED_A_PLAYER(DamagerPedID)
										PRINTLN("2952950, PROCESS_DAMAGE C ")
										playerDamager = NETWORK_GET_PLAYER_INDEX_FROM_PED(DamagerPedID)
										
										IF playerDamager = PLAYER_ID()
											IF GET_MAX_WANTED_LEVEL() < iMAX_WANTED_RATING
											OR GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 1
												IF GET_PED_TYPE(GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)) = PEDTYPE_COP
												OR GET_PED_TYPE(GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)) = PEDTYPE_ARMY
													SET_MAX_WANTED_LEVEL(iMAX_WANTED_RATING)
													IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 1
														SET_WANTED_TO(1)
													ENDIF
													SET_CLIENT_BIT0(eCLIENTBITSET0_KILLED_COP)
													PRINTLN("2952950, SET_MAX_WANTED_LEVEL(iMAX_WANTED_RATING) ")
												ENDIF
											ENDIF
										ENDIF
							
										IF VictimPlayerID != INVALID_PLAYER_INDEX()
											IF IS_NET_PLAYER_OK(VictimPlayerID, FALSE)
											
												PRINTLN("2952950, PROCESS_DAMAGE D ")
										
												participantId = NETWORK_GET_PARTICIPANT_INDEX(VictimPlayerID)
											
												IF IS_CLIENT_BIT0_SET(participantId, eCLIENTBITSET0_BAG_CARRIER)
													IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), GET_DESTROYED_EVENT_BIT_TO_SET(iGET_PLAYER_BAG(VictimPlayerID)))
														playerBD[PARTICIPANT_ID_TO_INT()].damagerPlayer = playerDamager
														PRINTLN("PROCESS_DAMAGE, eCLIENTBITSET0_BAG_CARRIER - damager: ", GET_PLAYER_NAME(playerDamager))
														PRINTLN("PROCESS_DAMAGE, eCLIENTBITSET0_BAG_CARRIER - victim: ", GET_PLAYER_NAME(VictimPlayerID))
														SET_CLIENT_BIT0(GET_DESTROYED_EVENT_BIT_TO_SET(iGET_PLAYER_BAG(VictimPlayerID)))
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_EVENTS()

    INT iCount, iBag, iClosest
    EVENT_NAMES ThisScriptEvent
	STRUCT_EVENT_COMMON_DETAILS Details
	SCRIPT_EVENT_DATA_EXEC_DROP sEvent
	STRUCT_SCRIPT_EVENT_SET_VEHICLE_AS_TARGETABLE_BY_HOMING_MISSILE eMissileEvent
	SCRIPT_EVENT_DATA_CONTRABAND_DESTROYED sDestroyedEvent
	SCRIPT_EVENT_DATA_BIKER_BORDER_PATROL_FAIL_CHECKPOINT sBorderPatrolEvent
	SCRIPT_EVENT_DATA_BAG_DROP sBagEvent

    REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount

        ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
       	
        SWITCH ThisScriptEvent
            
			CASE EVENT_NETWORK_SCRIPT_EVENT
				
				IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
					SWITCH Details.Type
				
						CASE SCRIPT_EVENT_BAG_DROP
							IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
								IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sBagEvent, SIZE_OF(sBagEvent))
									
									IF sBagEvent.iPosix = serverBD.iLaunchPosix
										iBag = sBagEvent.iBag
										iClosest = sBagEvent.iDrop
										
										IF iBag <> -1
											IF NOT IS_BIT_SET(serverBD.iBagEventBitSet[iBag], iClosest)
												IF IS_BIT_SET(serverBD.iActiveDropOffBitset[iBag], iClosest)
													IF NOT IS_THIS_A_HIDDEN_DROP_OFF(iClosest)
														SET_BIT(serverBD.iBagEventBitSet[iBag], iClosest)
														IF serverBD.iCurrentDropTeam = -1
														AND BIKER_SELL_CLUB_RUN()
															serverBD.iCurrentDropTeam = iClosest
															PRINTLN("[BIKER_SELL] [BAGS] SCRIPT_EVENT_BAG_DROP serverBD.iCurrentDropTeam = ", serverBD.iCurrentDropTeam)
														ENDIF  
														IF BIKER_SELL_STING()
														AND NOT BIKER_SELL_HAS_TWIST_HAPPENED()
															SET_SERVER_BIT0(eSERVERBITSET0_SEA_DEFEND_PEDS_SPOOKED)
															SET_SERVER_BIT0(eSERVERBITSET0_TWIST_HAPPENED)
														ENDIF
														SERVER_GIVE_PED_CHEER_SCENARIO(GET_NEAREST_PED_TO_BAG(sBagEvent.Details.FromPlayerIndex), serverBD.iDroppedOffCount)
														BIKER_SELL_SET_1ST_LOCATION_BIKE_BEEN_DROPPED_OFF(iBag)
														SERVER_INCREMENT_DROP_COUNT(iClosest, iBag)
														IF GET_SELL_VEH_DROP_OFF_COUNT(iBag) >= GET_SELL_VEH_TARGET_DROP_COUNT(iBag)
															PRINTLN("[BIKER_SELL] SCRIPT_EVENT_BAG_DROP SET_SELL_VEH_HAS_DELIVERED_CONTRABAND(", iBag, ")", " GET_SELL_VEH_DROP_OFF_COUNT(iBag) = ", 
																		GET_SELL_VEH_DROP_OFF_COUNT(iBag), " GET_SELL_VEH_TARGET_DROP_COUNT(iBag) = ", GET_SELL_VEH_TARGET_DROP_COUNT(iBag))
															SET_SELL_VEH_HAS_DELIVERED_CONTRABAND(iBag)
															serverBD.iAssignedBag[iBag] = -1
														ENDIF
														
														
														PRINTLN("[BIKER_SELL] [BAGS] SCRIPT_EVENT_BAG_DROP, iBag = ", iBag, " iDrop = ", iClosest, " player = ", GET_PLAYER_NAME(sBagEvent.Details.FromPlayerIndex))
													ELSE
														PRINTLN("[BIKER_SELL] [BAGS] SCRIPT_EVENT_BAG_DROP D, IS_THIS_A_HIDDEN_DROP_OFF  ", iClosest)
													ENDIF
												ENDIF
											ENDIF
										ELSE
											PRINTLN("[BIKER_SELL] [BAGS] iBag = -1 ")
										ENDIF
									ELSE
										PRINTLN("[BIKER_SELL] [BAGS] SCRIPT_EVENT_BAG_DROP - Posix doesn't match. sBagEvent.iPosix = ", sBagEvent.iPosix, " - serverBD.iLaunchPosix = ", serverBD.iLaunchPosix)
									ENDIF
								ENDIF
							ENDIF
						BREAK
					
						CASE SCRIPT_EVENT_CONTRABAND_SELL_START_RADAR_JAM
							
							IF NOT HAS_NET_TIMER_STARTED(sLocaldata.stRadarJamTimer)
								PRINTLN("[BIKER_SELL] [ST EXEC] [RJAM] [SERVER] PROCESS_EVENTS, Radar Jam Timer started")
								START_NET_TIMER(sLocaldata.stRadarJamTimer)
							ENDIF
						BREAK
						
						CASE SCRIPT_EVENT_CONTRABAND_SELL_STOP_RADAR_JAM
							IF HAS_NET_TIMER_STARTED(sLocaldata.stRadarJamTimer)
								PRINTLN("[BIKER_SELL] [ST EXEC] [RJAM] [SERVER] PROCESS_EVENTS, Radar Jam Timer reset")
								RESET_NET_TIMER(sLocaldata.stRadarJamTimer)
							ENDIF
						BREAK
						
						CASE SCRIPT_EVENT_EXEC_DROP
							IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEvent, SIZE_OF(sEvent))
								IF sEvent.iPosix = serverBD.iLaunchPosix
									IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
										IF BIKER_SELL_POSTMAN()
											PRINTLN("[BIKER_SELL] [POST] [PARCEL] [SERVER] PROCESS_EVENTS, serverBD.playerRequestedCrate = ", GET_PLAYER_NAME(sEvent.Details.FromPlayerIndex))
											serverBD.playerRequestedCrate = sEvent.Details.FromPlayerIndex
										ELSE
											PRINTLN("[BIKER_SELL] [CRT] PROCESS_EVENTS, SET_SERVER_BIT0(eSERVERBITSET0_CRATE_DROP) sEvent.iVeh = ", sEvent.iVeh, " player = ", GET_PLAYER_NAME(sEvent.Details.FromPlayerIndex)) 
											INT iBitToCheck
											eSERVER_BITSET_0 eServerBitToCheck
											iBitToCheck = ENUM_TO_INT(eSERVERBITSET0_CRATE_DROP1) + sEvent.iVeh
											eServerBitToCheck = INT_TO_ENUM(eSERVER_BITSET_0, iBitToCheck)
											SET_SERVER_BIT0(eServerBitToCheck)
											SET_BIT(serverBD.iVehForCrateDropBitSet, sEvent.iVeh)
											serverBD.playerRequestedCrate = sEvent.Details.FromPlayerIndex
											//serverBD.iVehForCrateDrop = sEvent.iVeh
										ENDIF
									ENDIF		
									
									IF IS_THIS_AN_AIR_VARIATION()
									OR IS_THIS_A_SEA_VARIATION()
										IF IS_NET_PLAYER_OK(sEvent.Details.FromPlayerIndex)
										AND IS_NET_PLAYER_OK(PLAYER_ID())
											IF sEvent.Details.FromPlayerIndex = PLAYER_ID()
											OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(GET_PLAYER_PED(sEvent.Details.FromPlayerIndex))) < 5.00
												PLAY_BIKER_DROP_SOUND()
											ENDIF
										ENDIF
									ENDIF
								ELSE
									PRINTLN("[BIKER_SELL] [POST] SCRIPT_EVENT_EXEC_DROP - Posix does not match. sEvent.iPosix = ", sEvent.iPosix, " - serverBD.iLaunchPosix = ", serverBD.iLaunchPosix)
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
				
				
				
				IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, eMissileEvent, SIZE_OF(eMissileEvent))
					SWITCH eMissileEvent.Details.Type
						CASE SCRIPT_EVENT_GB_SET_VEHICLE_AS_TARGETABLE_BY_HOMING_MISSILE
							IF IS_PLAYER_IN_ANY_SELL_VEH()
								INT iVeh 
								iVeh = GET_SELL_VEH_I_AM_IN_INT()
								IF iVeh <> -1
									IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh])
										IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh[iVeh])
											SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(VEH_ID(iVeh), eMissileEvent.bTargetable)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
				
				IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sDestroyedEvent, SIZE_OF(sDestroyedEvent))
					IF sDestroyedEvent.Details.Type = SCRIPT_EVENT_CONTRABAND_DESTROYED
						IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), sDestroyedEvent.contrabandOwner)
							IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//								IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_DESTROYED_TICKER_SENT)
									SEND_CONTRABAND_DESTROYED_TICKER(sDestroyedEvent.destroyer)
//									SET_SERVER_BIT1(eSERVERBITSET1_DESTROYED_TICKER_SENT)
//								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sBorderPatrolEvent, SIZE_OF(sBorderPatrolEvent))
					IF sBorderPatrolEvent.Details.Type = SCRIPT_EVENT_BIKER_FAIL_CHECKPOINT
						IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
							IF BIKER_SELL_BORDER_PATROL()
								HANDLE_BONUS_REDUCTION(sBorderPatrolEvent.iVeh)
//								IF (sBorderPatrolEvent.iFailedDropOff < iDROP_TARGET() -1)
//									SERVER_INCREMENT_DROP_COUNT(sBorderPatrolEvent.iFailedDropOff, sBorderPatrolEvent.iVeh)
//								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE EVENT_NETWORK_DAMAGE_ENTITY
				
				PROCESS_DAMAGE(iCount)	
			BREAK
		
        ENDSWITCH
            
    ENDREPEAT
ENDPROC

// [NOTE FOR BOBBY] - Start the timer for delaying blipping once players have entered vehicle
//						Look for calls to this if blip doesnt appear, likely the culprit

INT iParticipantCountCache
PROC SERVER_PROCESS_PARTICIPANT_LOOP()
	
	INT iParticipant
	INT iPlayer
	PLAYER_INDEX playerID
	PLAYER_INDEX playerDriver[ciMAX_SELL_VEHS]
	PARTICIPANT_INDEX participantID
	PED_INDEX pedID
	BOOL bAllCompletedRewards = TRUE
	BOOL bVehOccupied[ciMAX_SELL_VEHS]
	BOOL bPlayersFinished = TRUE
	BOOL bVehStuck = FALSE
	BOOL bLostWanted
	//INT iPlayerActiveBagCheck
	//BOOL bVehGrounded = FALSE
	INT iParticipantCount
	INT i
	// Refill data.
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
			
			participantID = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
			playerID = NETWORK_GET_PLAYER_INDEX(participantID)
			pedID = GET_PLAYER_PED(playerID)
			iPlayer = NATIVE_TO_INT(playerID)
			
			GB_BIKER_RACE_SERVER_MAINTAIN(serverBD.sRaceServerVars, iParticipant)

			SET_BIT(iParticipantActiveBitset, iParticipant)
			
			IF IS_NET_PLAYER_OK(playerID, FALSE)
				iParticipantCount++
				//SET_BIT(iPlayerActiveBagCheck, NATIVE_TO_INT(playerID))
				IF playerBD[iParticipant].eStage <> eSELL_STAGE_CLEANUP
					bPlayersFinished = FALSE
				ENDIF
				
				SET_BIT(iPlayerOkBitset, iPlayer)
				
				IF IS_ENTITY_DEAD(pedID)
					SET_BIT(iPlayerDeadBitset, iPlayer)
				ENDIF
				
				IF BIKER_SELL_BENSON()
				
					REPEAT GET_NUM_DROP_OFF_VEH_FOR_VARIATION() i
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffVeh[i])
							IF NOT IS_BIT_SET(serverBD.iDropVehPenaltyBitSet, i)
								IF IS_BIT_SET(playerBD[iParticipant].iDropVehStuck, i)
									SERVER_INCREASE_DROP_PENALTY()
									SET_BIT(serverBD.iDropVehPenaltyBitSet, i)
									PRINTLN("[BENSON] SERVER_PROCESS_PARTICIPANT_LOOP, iDropVehStuck, i = ", i)
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
					IF IS_SELL_VEH_OCCUPIED_BY_TEAMMATE(0, GET_PLAYER_PED(playerID))
						playerDriver[0] = playerID
						bVehOccupied[0] = TRUE
					ENDIF
					
					SERVER_SET_OPEN_DOORS(iParticipant, 0)
					SERVER_CHECK_FOR_LOST_PRODUCT(playerID, 0)
				ELSE
					SERVER_SET_OPEN_DOORS(iParticipant, 0, TRUE)
				ENDIF
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[1])
					IF IS_SELL_VEH_OCCUPIED_BY_TEAMMATE(1, GET_PLAYER_PED(playerID))
						playerDriver[1] = playerID
						bVehOccupied[1] = TRUE
					ENDIF
					
					SERVER_SET_OPEN_DOORS(iParticipant, 1)
					SERVER_CHECK_FOR_LOST_PRODUCT(playerID, 1)
				ELSE
					SERVER_SET_OPEN_DOORS(iParticipant, 1, TRUE)
				ENDIF
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[2])
					IF IS_SELL_VEH_OCCUPIED_BY_TEAMMATE(2, GET_PLAYER_PED(playerID))
						playerDriver[2] = playerID
						bVehOccupied[2] = TRUE
					ENDIF
					
					SERVER_SET_OPEN_DOORS(iParticipant, 2)
					SERVER_CHECK_FOR_LOST_PRODUCT(playerID, 2)
				ELSE
					SERVER_SET_OPEN_DOORS(iParticipant, 2, TRUE)
				ENDIF
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[3])
					IF IS_SELL_VEH_OCCUPIED_BY_TEAMMATE(3, GET_PLAYER_PED(playerID))
						playerDriver[3] = playerID
						bVehOccupied[3] = TRUE
					ENDIF
					
					SERVER_SET_OPEN_DOORS(iParticipant, 3)
					SERVER_CHECK_FOR_LOST_PRODUCT(playerID, 3)
				ELSE
					SERVER_SET_OPEN_DOORS(iParticipant, 3, TRUE)
				ENDIF
//				IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_VEH_GROUNDED)
//					bVehGrounded = TRUE
//				ENDIF
				
				IF NOT bVehStuck
					//IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_VEH_STUCK)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_VEH0_STUCK)
						IF NOT IS_BIT_SET(serverBD.iVehStuckSinceCreatedBitset, 0)
							IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_0_ENTERED_ONCE) 
							OR NOT IS_THIS_A_LAND_VARIATION()
								bVehStuck = TRUE
								PRINTLN("[BIKER_SELL] participant ", iParticipant, " has set eCLIENTBITSET0_VEH0_STUCK. PlayerId = ", iPlayer, " player ", GET_PLAYER_NAME(playerID))
							ELSE
								SET_BIT(serverBD.iVehStuckSinceCreatedBitset, 0)
								PRINTLN("[BIKER_SELL] participant ", iParticipant, " has set eCLIENTBITSET0_VEH0_STUCK but timeAllSellVehCreated not expired. PlayerId = ", iPlayer, " player ", GET_PLAYER_NAME(playerID))
							ENDIF
						ENDIF
					ELIF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_VEH1_STUCK)
						IF NOT IS_BIT_SET(serverBD.iVehStuckSinceCreatedBitset, 1)
							IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_1_ENTERED_ONCE) 
							OR NOT IS_THIS_A_LAND_VARIATION()
								bVehStuck = TRUE
								PRINTLN("[BIKER_SELL] participant ", iParticipant, " has set eCLIENTBITSET0_VEH1_STUCK. PlayerId = ", iPlayer, " player ", GET_PLAYER_NAME(playerID))
							ELSE
								SET_BIT(serverBD.iVehStuckSinceCreatedBitset, 1)
								PRINTLN("[BIKER_SELL] participant ", iParticipant, " has set eCLIENTBITSET0_VEH0_STUCK but timeAllSellVehCreated not expired. PlayerId = ", iPlayer, " player ", GET_PLAYER_NAME(playerID))
							ENDIF
						ENDIF
					ELIF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_VEH2_STUCK)
						IF NOT IS_BIT_SET(serverBD.iVehStuckSinceCreatedBitset, 2)
							IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_2_ENTERED_ONCE) 
							OR NOT IS_THIS_A_LAND_VARIATION()
								bVehStuck = TRUE
								PRINTLN("[BIKER_SELL] participant ", iParticipant, " has set eCLIENTBITSET0_VEH2_STUCK. PlayerId = ", iPlayer, " player ", GET_PLAYER_NAME(playerID))
							ELSE
								SET_BIT(serverBD.iVehStuckSinceCreatedBitset, 2)
								PRINTLN("[BIKER_SELL] participant ", iParticipant, " has set eCLIENTBITSET0_VEH0_STUCK but timeAllSellVehCreated not expired. PlayerId = ", iPlayer, " player ", GET_PLAYER_NAME(playerID))
							ENDIF
						ENDIF
					ELIF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_VEH3_STUCK)
						IF NOT IS_BIT_SET(serverBD.iVehStuckSinceCreatedBitset, 3)
							IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_3_ENTERED_ONCE) 
							OR NOT IS_THIS_A_LAND_VARIATION()
								bVehStuck = TRUE
								PRINTLN("[BIKER_SELL] participant ", iParticipant, " has set eCLIENTBITSET0_VEH3_STUCK. PlayerId = ", iPlayer, " player ", GET_PLAYER_NAME(playerID))
							ELSE
								SET_BIT(serverBD.iVehStuckSinceCreatedBitset, 3)
								PRINTLN("[BIKER_SELL] participant ", iParticipant, " has set eCLIENTBITSET0_VEH0_STUCK but timeAllSellVehCreated not expired. PlayerId = ", iPlayer, " player ", GET_PLAYER_NAME(playerID))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_COMPLETED_REWARDS)
					bAllCompletedRewards = FALSE
				ENDIF
				
				IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_LOST_WANTED)
					bLostWanted = TRUE
				ENDIF
				
				IF NOT IS_SERVER_BIT2_SET(eSERVERBITSET2_SENT_VEH_0_DESTROYED_EVENT)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_SERVER_SHOULD_SEND_DESTROYED_EVENT_VEH_0)
					AND playerBD[iParticipant].damagerPlayer != INVALID_PLAYER_INDEX()
						SEND_CONTRABAND_DESTROYED_EVENT(playerBD[iParticipant].damagerPlayer)
						PRINTLN("2952950 [BIKER_SELL] participant ", iParticipant, " Telling server to send destroyed event for vehicle/bag 0. PlayerId = ", GET_PLAYER_NAME(playerBD[iParticipant].damagerPlayer))
						SET_SERVER_BIT2(eSERVERBITSET2_SENT_VEH_0_DESTROYED_EVENT)
					ENDIF
				ENDIF
				
				IF NOT IS_SERVER_BIT2_SET(eSERVERBITSET2_SENT_VEH_1_DESTROYED_EVENT)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_SERVER_SHOULD_SEND_DESTROYED_EVENT_VEH_1)
					AND playerBD[iParticipant].damagerPlayer != INVALID_PLAYER_INDEX()
						SEND_CONTRABAND_DESTROYED_EVENT(playerBD[iParticipant].damagerPlayer)
						PRINTLN("2952950 [BIKER_SELL] participant ", iParticipant, " Telling server to send destroyed event for vehicle/bag 1. PlayerId = ", GET_PLAYER_NAME(playerBD[iParticipant].damagerPlayer))
						SET_SERVER_BIT2(eSERVERBITSET2_SENT_VEH_1_DESTROYED_EVENT)
					ENDIF
				ENDIF
				
				IF NOT IS_SERVER_BIT2_SET(eSERVERBITSET2_SENT_VEH_2_DESTROYED_EVENT)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_SERVER_SHOULD_SEND_DESTROYED_EVENT_VEH_2)
					AND playerBD[iParticipant].damagerPlayer != INVALID_PLAYER_INDEX()
						SEND_CONTRABAND_DESTROYED_EVENT(playerBD[iParticipant].damagerPlayer)
						PRINTLN("2952950 [BIKER_SELL] participant ", iParticipant, " Telling server to send destroyed event for vehicle/bag 2. PlayerId = ", GET_PLAYER_NAME(playerBD[iParticipant].damagerPlayer))
						SET_SERVER_BIT2(eSERVERBITSET2_SENT_VEH_2_DESTROYED_EVENT)
					ENDIF
				ENDIF
				
				IF NOT IS_SERVER_BIT2_SET(eSERVERBITSET2_SENT_VEH_3_DESTROYED_EVENT)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_SERVER_SHOULD_SEND_DESTROYED_EVENT_VEH_3)
					AND playerBD[iParticipant].damagerPlayer != INVALID_PLAYER_INDEX()
						SEND_CONTRABAND_DESTROYED_EVENT(playerBD[iParticipant].damagerPlayer)
						PRINTLN("2952950 [BIKER_SELL] participant ", iParticipant, " Telling server to send destroyed event for vehicle/bag 3. PlayerId = ", GET_PLAYER_NAME(playerBD[iParticipant].damagerPlayer))
						SET_SERVER_BIT2(eSERVERBITSET2_SENT_VEH_3_DESTROYED_EVENT)
					ENDIF
				ENDIF
				
				IF NOT IS_SERVER_BIT2_SET(eSERVERBITSET2_SENT_VEH_4_DESTROYED_EVENT)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_SERVER_SHOULD_SEND_DESTROYED_EVENT_VEH_4)
					AND playerBD[iParticipant].damagerPlayer != INVALID_PLAYER_INDEX()
						SEND_CONTRABAND_DESTROYED_EVENT(playerBD[iParticipant].damagerPlayer)
						PRINTLN("2952950 [BIKER_SELL] participant ", iParticipant, " Telling server to send destroyed event for vehicle/bag 4. PlayerId = ", GET_PLAYER_NAME(playerBD[iParticipant].damagerPlayer))
						SET_SERVER_BIT2(eSERVERBITSET2_SENT_VEH_4_DESTROYED_EVENT)
					ENDIF
				ENDIF
				
				IF NOT IS_SERVER_BIT2_SET(eSERVERBITSET2_SENT_VEH_5_DESTROYED_EVENT)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_SERVER_SHOULD_SEND_DESTROYED_EVENT_VEH_5)
					AND playerBD[iParticipant].damagerPlayer != INVALID_PLAYER_INDEX()
						SEND_CONTRABAND_DESTROYED_EVENT(playerBD[iParticipant].damagerPlayer)
						PRINTLN("2952950 [BIKER_SELL] participant ", iParticipant, " Telling server to send destroyed event for vehicle/bag 5. PlayerId = ", GET_PLAYER_NAME(playerBD[iParticipant].damagerPlayer))
						SET_SERVER_BIT2(eSERVERBITSET2_SENT_VEH_5_DESTROYED_EVENT)
					ENDIF
				ENDIF
				
				IF NOT IS_SERVER_BIT2_SET(eSERVERBITSET2_SENT_VEH_6_DESTROYED_EVENT)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_SERVER_SHOULD_SEND_DESTROYED_EVENT_VEH_6)
					AND playerBD[iParticipant].damagerPlayer != INVALID_PLAYER_INDEX()
						SEND_CONTRABAND_DESTROYED_EVENT(playerBD[iParticipant].damagerPlayer)
						PRINTLN("2952950 [BIKER_SELL] participant ", iParticipant, " Telling server to send destroyed event for vehicle/bag 6. PlayerId = ", GET_PLAYER_NAME(playerBD[iParticipant].damagerPlayer))
						SET_SERVER_BIT2(eSERVERBITSET2_SENT_VEH_6_DESTROYED_EVENT)
					ENDIF
				ENDIF
				
				IF NOT IS_SERVER_BIT2_SET(eSERVERBITSET2_SENT_VEH_7_DESTROYED_EVENT)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_SERVER_SHOULD_SEND_DESTROYED_EVENT_VEH_7)
					AND playerBD[iParticipant].damagerPlayer != INVALID_PLAYER_INDEX()
						SEND_CONTRABAND_DESTROYED_EVENT(playerBD[iParticipant].damagerPlayer)
						PRINTLN("2952950 [BIKER_SELL] participant ", iParticipant, " Telling server to send destroyed event for vehicle/bag 7. PlayerId = ", GET_PLAYER_NAME(playerBD[iParticipant].damagerPlayer))
						SET_SERVER_BIT2(eSERVERBITSET2_SENT_VEH_7_DESTROYED_EVENT)
					ENDIF
				ENDIF
				
				IF NOT HAS_AIR_CLEAR_AREA_PLANE_LANDED(0)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_PLANE_0_LANDED)
						PRINTLN("[BIKER_SELL] participant ", iParticipant, " has landed the plane. PlayerId = ", iPlayer)
						SET_SERVER_BIT0(eSERVERBITSET0_PLANE_0_LANDED)
						SET_SELL_VEH_HAS_DELIVERED_CONTRABAND(0)
						SERVER_INCREMENT_DROP_COUNT(ciDROP_OFF_UNRESTRICTED, 0)
					ENDIF
				ENDIF
				
				IF NOT HAS_AIR_CLEAR_AREA_PLANE_LANDED(1)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_PLANE_1_LANDED)
						PRINTLN("[BIKER_SELL] participant ", iParticipant, " has landed the plane. PlayerId = ", iPlayer)
						SET_SERVER_BIT0(eSERVERBITSET0_PLANE_1_LANDED)
						SET_SELL_VEH_HAS_DELIVERED_CONTRABAND(1)
						SERVER_INCREMENT_DROP_COUNT(ciDROP_OFF_UNRESTRICTED, 1)
					ENDIF
				ENDIF
				
				IF NOT HAS_AIR_CLEAR_AREA_PLANE_LANDED(2)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_PLANE_2_LANDED)
						PRINTLN("[BIKER_SELL] participant ", iParticipant, " has landed the plane. PlayerId = ", iPlayer)
						SET_SERVER_BIT0(eSERVERBITSET0_PLANE_2_LANDED)
						SET_SELL_VEH_HAS_DELIVERED_CONTRABAND(2)
						SERVER_INCREMENT_DROP_COUNT(ciDROP_OFF_UNRESTRICTED, 2)
					ENDIF
				ENDIF
				
				IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_SEA_ATTACK_START_VEH_SPAWN)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_SEA_ATTACK_SPAWN_DISTANCE_REACHED)
						PRINTLN("[BIKER_SELL] participant ", iParticipant, " reporting that sell veh is > 100m from start. PlayerId = ", iPlayer)
						SET_SERVER_BIT0(eSERVERBITSET0_SEA_ATTACK_START_VEH_SPAWN)
					ENDIF					
				ENDIF
				
				IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_CAN_LAND_GANG_CHASE_SPAWN)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_LAND_ATTACK_SPAWN_DISTANCE_REACHED)
						INT iContraVehToAttack
						iContraVehToAttack = GET_CONTRABAND_VEHICLE_TO_ATTACK()
						IF iContraVehToAttack > -1
							serverBD.iRandVehToAttack = iContraVehToAttack
							PRINTLN("[BIKER_SELL] participant ", iParticipant, " reporting that sell veh is > 100m from start. PlayerId = ", iPlayer, " set serverBD.iRandVehToAttack = ", serverBD.iRandVehToAttack)
							SET_SERVER_BIT0(eSERVERBITSET0_CAN_LAND_GANG_CHASE_SPAWN)
						ELSE
							PRINTLN("[BIKER_SELL] participant ", iParticipant, " reporting that sell veh is > 100m from start. PlayerId = ", iPlayer, " but can't find contraband vehicle to attack!")						
						ENDIF
					ENDIF					
				ENDIF
				
				IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_SEA_DEFEND_PLAYER_IN_RANGE)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_SEA_DEFEND_WITHIN_RANGE)
						PRINTLN("[BIKER_SELL] participant ", iParticipant, " is <= 100m from peds/veh. PlayerId = ", iPlayer)
						SET_SERVER_BIT0(eSERVERBITSET0_SEA_DEFEND_PLAYER_IN_RANGE)
					ENDIF
				ENDIF
				
				IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_CONTRABAND_REMOVED_AT_END)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_CONTRABAND_ADDED_AT_END)
						PRINTLN("[BIKER_SELL] boss participant ", iParticipant, " has transfered contraband to warehouse. PlayerId = ", iPlayer)
						SET_SERVER_BIT1(eSERVERBITSET1_CONTRABAND_REMOVED_AT_END)
					ENDIF
				ENDIF
				
				IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_LAND_TRACKIFY_SHOULD_BLIP_DROP_OFFS)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_LAND_TRACKIFY_SELL_VEH_NEAR_DROP_OFF)
						PRINTLN("[BIKER_SELL] participant ", iParticipant, " reporting that land trackify sell veh is near drop off. PlayerId = ", iPlayer)
						SET_SERVER_BIT1(eSERVERBITSET1_LAND_TRACKIFY_SHOULD_BLIP_DROP_OFFS)
					ENDIF
				ENDIF
				
				IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_SEA_DEFEND_PEDS_SPOOKED)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_SEA_DEFEND_SPOOKED_PEDS)
						PRINTLN("[BIKER_SELL] participant ", iParticipant, " has spooked peds. PlayerId = ", iPlayer)
						SET_SERVER_BIT0(eSERVERBITSET0_SEA_DEFEND_PEDS_SPOOKED)
						IF BIKER_SELL_STING()
							SET_SERVER_BIT0(eSERVERBITSET0_TWIST_HAPPENED)
						ENDIF
					ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD
				IF serverBD.iSPassPart = (-1)
				AND serverBD.iFFailPart = (-1)
					IF IS_CLIENT_DEBUG_BIT0_SET(participantID, eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
						serverBD.iSPassPart = iParticipant
						PRINTLN("[BIKER_SELL] participant ", iParticipant, " has S passed. Setting serverBD.iSPassPart = ", iParticipant)
					ELIF IS_CLIENT_DEBUG_BIT0_SET(participantID, eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED)
						serverBD.iFFailPart = iParticipant
						PRINTLN("[BIKER_SELL] participant ", iParticipant, " has F failed. Setting serverBD.iFFailPart = ", iParticipant)
					ENDIF
				ENDIF
				#ENDIF
				
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	
	IF GET_SERVER_GAME_STATE() = GAME_STATE_RUNNING
		INT iBags
		INT iPlayers
		IF iParticipantCountCache != iParticipantCount
		AND HAS_NET_TIMER_EXPIRED(serverBD.modeTimer, 15000)
			iParticipantCountCache = iParticipantCount
			PRINTLN("[BIKER_SELL] [BAGS] - SERVER_PROCESS_PARTICIPANT_LOOP - iParticipantCountCache = ", iParticipantCountCache)
			IF MODE_HAS_PICKUP_BAGS()
				REPEAT NUM_NETWORK_PLAYERS iPlayers
					//IF NOT IS_BIT_SET(iPlayerActiveBagCheck, iPlayers)
						PLAYER_INDEX piPlayer = INT_TO_PLAYERINDEX(iPlayers)
						IF piPlayer = INVALID_PLAYER_INDEX()
						OR NOT NETWORK_IS_PLAYER_ACTIVE(piPlayer)
						OR NOT NETWORK_IS_PLAYER_A_PARTICIPANT(piPlayer)
							REPEAT GET_NUM_BAGS() iBags
								IF serverBD.iAssignedBag[iBags] = iPlayers
									PRINTLN("[BIKER_SELL] [BAGS] - SERVER_PROCESS_PARTICIPANT_LOOP - SERVER_CHECK_FOR_LOST_PRODUCT - iBags = ", iBags, " = serverBD.iAssignedBag[iBag] = -1 for ", GET_PLAYER_NAME(piPlayer))
									SERVER_INCREASE_DROP_PENALTY(GET_BAG_DROP_PENALTY(iBags))
									serverBD.iAssignedBag[iBags] = -1
								ENDIF
							ENDREPEAT
						ENDIF
					//ENDIF
				ENDREPEAT
			ELSE BIKER_SELL_POSTMAN()
						
				REPEAT NUM_NETWORK_PLAYERS iPlayers

					PLAYER_INDEX piPlayer = INT_TO_PLAYERINDEX(iPlayers)
					
					REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iBags
						IF DOES_ENTITY_EXIST(VEH_ID(iBags))
						AND NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(iBags)
							IF bIS_PLAYER_POSTMAN(iBags, iPlayers)
								IF NOT NETWORK_IS_PLAYER_ACTIVE(piPlayer)
								OR NOT NETWORK_IS_PLAYER_A_PARTICIPANT(piPlayer)
									PRINTLN("[BIKER_SELL] [BAGS] - SERVER_PROCESS_PARTICIPANT_LOOP - SERVER_CHECK_FOR_LOST_PRODUCT, bIS_PLAYER_POSTMAN - iBags = ", iBags, " = serverBD.iAssignedBag[iBag] = -1")
									
									SERVER_INCREASE_DROP_PENALTY(GET_BAG_DROP_PENALTY(iBags))
									SERVER_INCREASE_VEHICLE_PENALTY(iBags)
									SERVER_SET_POSTMAN(FALSE, playerID, iBags)
									SERVER_CLEAR_ACTIVE_DROP(iBags, serverBD.iClosestDropOff[iBags])
									CLEAR_POSTMAN_VEH(iBags)
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT

				ENDREPEAT
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_SEND_WANTED_TEXT_MESSAGE)
		IF bLostWanted
			SET_SERVER_BIT1(eSERVERBITSET1_SEND_WANTED_TEXT_MESSAGE)
		ENDIF
	ENDIF
	
	// Store driver 1
	IF playerDriver[0] <> serverBD.playerDriver[0]
		serverBD.playerDriver[0] = playerDriver[0]
	ENDIF
	
	// Store driver 2
	IF playerDriver[1] <> serverBD.playerDriver[1]
		serverBD.playerDriver[1] = playerDriver[1]
	ENDIF
	
	// Store driver 3
	IF playerDriver[2] <> serverBD.playerDriver[2]
		serverBD.playerDriver[2] = playerDriver[2]
	ENDIF
	
	IF playerDriver[3] <> serverBD.playerDriver[3]
		serverBD.playerDriver[3] = playerDriver[3]
	ENDIF
	IF bVehOccupied[0] 
		IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_0_ENTERED_ONCE)
			START_BLIP_DELAY_TIMER(TRUE)
			SET_SERVER_BIT0(eSERVERBITSET0_VEHICLE_0_ENTERED_ONCE)
			PRINTLN("[BIKER_SELL] [BITS] SET_BIT, eSERVERBITSET0_VEHICLE_0_ENTERED_ONCE ")
		ENDIF
		IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_SELL_VEH_0_OCCUPIED)
			SET_SERVER_BIT0(eSERVERBITSET0_SELL_VEH_0_OCCUPIED)
			PRINTLN("[BIKER_SELL] [BITS] SET_BIT, eSERVERBITSET0_SELL_VEH_0_OCCUPIED ")
		ENDIF
	ELSE
		IF IS_SERVER_BIT0_SET(eSERVERBITSET0_SELL_VEH_0_OCCUPIED)
			CLEAR_SERVER_BIT0(eSERVERBITSET0_SELL_VEH_0_OCCUPIED)
			PRINTLN("[BIKER_SELL] [BITS] CLEAR_BIT, eSERVERBITSET0_SELL_VEH_0_OCCUPIED ")
		ENDIF
	ENDIF
	
	IF bVehOccupied[1] 
		IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_1_ENTERED_ONCE)
			START_BLIP_DELAY_TIMER(TRUE)
			SET_SERVER_BIT0(eSERVERBITSET0_VEHICLE_1_ENTERED_ONCE)
			PRINTLN("[BIKER_SELL] [BITS] SET_BIT, eSERVERBITSET0_VEHICLE_1_ENTERED_ONCE ")
		ENDIF
		IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_SELL_VEH_1_OCCUPIED)
			SET_SERVER_BIT0(eSERVERBITSET0_SELL_VEH_1_OCCUPIED)
			PRINTLN("[BIKER_SELL] [BITS] SET_BIT, eSERVERBITSET0_SELL_VEH_1_OCCUPIED ")
		ENDIF
	ELSE
		IF IS_SERVER_BIT0_SET(eSERVERBITSET0_SELL_VEH_1_OCCUPIED)
			CLEAR_SERVER_BIT0(eSERVERBITSET0_SELL_VEH_1_OCCUPIED)
			PRINTLN("[BIKER_SELL] [BITS] CLEAR_BIT, eSERVERBITSET0_SELL_VEH_1_OCCUPIED ")
		ENDIF
	ENDIF
	
	IF bVehOccupied[2] 
		IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_2_ENTERED_ONCE)
			START_BLIP_DELAY_TIMER(TRUE)
			SET_SERVER_BIT0(eSERVERBITSET0_VEHICLE_2_ENTERED_ONCE)
			PRINTLN("[BIKER_SELL] [BITS] SET_BIT, eSERVERBITSET0_VEHICLE_2_ENTERED_ONCE ")
		ENDIF
		IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_SELL_VEH_2_OCCUPIED)
			SET_SERVER_BIT0(eSERVERBITSET0_SELL_VEH_2_OCCUPIED)
			PRINTLN("[BIKER_SELL] [BITS] SET_BIT, eSERVERBITSET0_SELL_VEH_2_OCCUPIED ")
		ENDIF
	ELSE
		IF IS_SERVER_BIT0_SET(eSERVERBITSET0_SELL_VEH_2_OCCUPIED)
			CLEAR_SERVER_BIT0(eSERVERBITSET0_SELL_VEH_2_OCCUPIED)
			PRINTLN("[BIKER_SELL] [BITS] CLEAR_BIT, eSERVERBITSET0_SELL_VEH_2_OCCUPIED ")
		ENDIF
	ENDIF
	
	IF bVehOccupied[3] 
		IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_3_ENTERED_ONCE)
			START_BLIP_DELAY_TIMER(TRUE)
			SET_SERVER_BIT0(eSERVERBITSET0_VEHICLE_3_ENTERED_ONCE)
			PRINTLN("[BIKER_SELL] [BITS] SET_BIT, eSERVERBITSET0_VEHICLE_3_ENTERED_ONCE ")
		ENDIF
		IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_SELL_VEH_3_OCCUPIED)
			SET_SERVER_BIT0(eSERVERBITSET0_SELL_VEH_3_OCCUPIED)
			PRINTLN("[BIKER_SELL] [BITS] SET_BIT, eSERVERBITSET0_SELL_VEH_3_OCCUPIED ")
		ENDIF
	ELSE
		IF IS_SERVER_BIT0_SET(eSERVERBITSET0_SELL_VEH_3_OCCUPIED)
			CLEAR_SERVER_BIT0(eSERVERBITSET0_SELL_VEH_3_OCCUPIED)
			PRINTLN("[BIKER_SELL] [BITS] CLEAR_BIT, eSERVERBITSET0_SELL_VEH_3_OCCUPIED ")
		ENDIF
	ENDIF
//	IF bVehStuck
//		SET_SERVER_BIT0(eSERVERBITSET0_VEHICLE_STUCK)
//	ENDIF
//	IF BIKER_SELL_EXCHANGE()
//	AND bPedsShotAt
//		IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_PED_SHOT_AT)
//			PRINTLN("[BIKER_SELL] [BITS] SET_BIT, eSERVERBITSET1_PED_SHOT_AT ")
//			SET_SERVER_BIT1(eSERVERBITSET1_PED_SHOT_AT)
//		ENDIF
//	ENDIF
	
	IF bPlayersFinished
		IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_PLAYERS_FINISHED)
			PRINTLN("[BIKER_SELL] [BITS] SET_BIT, eSERVERBITSET0_PLAYERS_FINISHED ")
			SET_SERVER_BIT0(eSERVERBITSET0_PLAYERS_FINISHED)
		ENDIF
	ENDIF
	
	IF bAllCompletedRewards
		IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
			SET_SERVER_BIT0(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
		ENDIF
	ELSE
		IF IS_SERVER_BIT0_SET(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
			CLEAR_SERVER_BIT0(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD

TEXT_WIDGET_ID twIdServerGameState
TEXT_WIDGET_ID twIdClientGameState[GB_MAX_GANG_SIZE_INCLUDING_BOSS]
TEXT_WIDGET_ID twIdClientName[GB_MAX_GANG_SIZE_INCLUDING_BOSS]
TEXT_WIDGET_ID twIdModeState
BOOL bTerminateScriptNow
FLOAT xOffset
FLOAT yOffset
FLOAT zOffset
BOOL bShowOffsets = FALSE
BOOL bDrawSpawnAreaMarkers = FALSE

PROC UPDATE_SERVER_GAME_STATE_WIDGET()
	SET_CONTENTS_OF_TEXT_WIDGET(twIdServerGameState, GET_GAME_STATE_NAME(GET_SERVER_GAME_STATE()))
	SET_CONTENTS_OF_TEXT_WIDGET(twIdModeState, GET_MODE_STATE_NAME(GET_MODE_STATE()))
ENDPROC

PROC UPDATE_CLIENT_GAME_STATE_WIDGET(INT iParticipant)
	
	STRING strName
	
	SET_CONTENTS_OF_TEXT_WIDGET(twIdClientGameState[iParticipant], "NOT ACTIVE")
	SET_CONTENTS_OF_TEXT_WIDGET(twIdClientName[iParticipant], "NOT ACTIVE")
	
	IF IS_BIT_SET(iParticipantActiveBitset, iParticipant)
		IF IS_BIT_SET(iPlayerOkBitset, iParticipant)
			SET_CONTENTS_OF_TEXT_WIDGET(twIdClientGameState[iParticipant], GET_GAME_STATE_NAME(GET_CLIENT_GAME_STATE(iParticipant)))
			strName = GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, iParticipant))
			SET_CONTENTS_OF_TEXT_WIDGET(twIdClientName[iParticipant], strName)
		ENDIF
	ENDIF
	
ENDPROC

PROC CREATE_WIDGETS()
	
	INT iParticipant
	TEXT_LABEL_63 tl63Temp
//	fLocateWidth = 1.9
//	vDropLocate = <<-1.000, -1.0, 1.0>>
//	vDropLocate2 = <<1.000, 1.0, 1.5>>
	START_WIDGET_GROUP("GB_SELL ")
//		START_WIDGET_GROUP("TRUCK OFFSETS")
//			ADD_WIDGET_FLOAT_SLIDER("fLocateWidth", fLocateWidth, LOWEST_INT, HIGHEST_INT, 0.1)
//			ADD_WIDGET_FLOAT_SLIDER("vDropLocate x", vDropLocate.x, LOWEST_INT, HIGHEST_INT, 0.1)
//			ADD_WIDGET_FLOAT_SLIDER("vDropLocate y", vDropLocate.y, LOWEST_INT, HIGHEST_INT, 0.1)
//			ADD_WIDGET_FLOAT_SLIDER("vDropLocate z", vDropLocate.z, LOWEST_INT, HIGHEST_INT, 0.1)
//			ADD_WIDGET_FLOAT_SLIDER("vDropLocate2 x", vDropLocate2.x, LOWEST_INT, HIGHEST_INT, 0.1)
//			ADD_WIDGET_FLOAT_SLIDER("vDropLocate2 y", vDropLocate2.y, LOWEST_INT, HIGHEST_INT, 0.1)
//			ADD_WIDGET_FLOAT_SLIDER("vDropLocate2 z", vDropLocate2.z, LOWEST_INT, HIGHEST_INT, 0.1)
//		STOP_WIDGET_GROUP()

		ADD_WIDGET_BOOL("bSellDebug", sLocaldata.bSellDebug)
			
		ADD_WIDGET_INT_READ_ONLY("serverBD.iVehicleCountDeliveredAllContraband", serverBD.iVehicleCountDeliveredAllContraband)
		ADD_WIDGET_BOOL("Locally terminate Script Now", bTerminateScriptNow)
		ADD_WIDGET_BOOL("Fake End Of Mode Timer", serverBD.bFakeEndOfModeTimer)
		ADD_WIDGET_INT_READ_ONLY("serverBD.iStartingContraband", serverBD.iStartingContraband)
		ADD_WIDGET_INT_READ_ONLY("serverBD.iPlayerRepair", serverBD.iPlayerRepair)
		ADD_WIDGET_INT_READ_ONLY("iWantedStored", sLocaldata.iWantedStored)
		ADD_WIDGET_INT_READ_ONLY("iMissionWarehouse", serverBD.iStartingWarehouse)
		ADD_WIDGET_INT_READ_ONLY("iRoute", serverBD.iRoute)
		ADD_WIDGET_INT_READ_ONLY("serverBD.iSting", serverBD.iSting)
		ADD_WIDGET_INT_READ_ONLY("serverBD.iDroppedOffCount", serverBD.iDroppedOffCount)
		ADD_WIDGET_INT_READ_ONLY("serverBD.iClosestDropOff", serverBD.iClosestDropOff[0])
		ADD_WIDGET_INT_READ_ONLY("iNumCrates", serverBD.iNumCrates[0])
		ADD_WIDGET_INT_READ_ONLY("serverBD.iBreakdownCount", serverBD.iBreakdownCount)
		ADD_WIDGET_INT_READ_ONLY("sLocaldata.iRepairProgress", sLocaldata.iRepairProgress)
		ADD_WIDGET_FLOAT_READ_ONLY("serverBD.fStoredBreakdownDistance", serverBD.fStoredBreakdownDistance)
		ADD_WIDGET_FLOAT_READ_ONLY("serverBD.fDistDrop", serverBD.fDistDrop)
		ADD_WIDGET_INT_SLIDER("tiAlphaMarker", tiAlphaMarker, 0, 255, 1)
		ADD_WIDGET_FLOAT_SLIDER("tfScale", tfScale, -100, 255, 1)
		ADD_WIDGET_FLOAT_READ_ONLY("serverBD.fDistAboveGround", serverBD.fDistAboveGround)
		ADD_WIDGET_FLOAT_READ_ONLY("serverBD.fDistGroundHeight", serverBD.fDistGroundHeight)
//		ADD_WIDGET_FLOAT_SLIDER("Speed modifier", fNewSpeedModifier, 0, 10000, 25)
		
		START_WIDGET_GROUP("Tug Offset Debugging (draws marker)")
			ADD_WIDGET_BOOL("Show offet markers", bShowOffsets)
			ADD_WIDGET_FLOAT_SLIDER("Offset x", xOffset, LOWEST_INT, HIGHEST_INT, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Offset y", yOffset, LOWEST_INT, HIGHEST_INT, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Offset z", zOffset, LOWEST_INT, HIGHEST_INT, 0.01)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Sea Variation Spawn Markers")
			ADD_WIDGET_BOOL("Draw Markers at central spawn points", bDrawSpawnAreaMarkers)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("REPAIR")
			ADD_WIDGET_FLOAT_SLIDER("tfRepairY", tfRepairY, -100, 255, 1)
			ADD_WIDGET_FLOAT_SLIDER("tfMarkerY", tfMarkerY, -100, 255, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Game State")
			START_WIDGET_GROUP("Server")
				twIdServerGameState = ADD_TEXT_WIDGET("Game State")
				twIdModeState = ADD_TEXT_WIDGET("Mode State")
				UPDATE_SERVER_GAME_STATE_WIDGET()
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Client")
				REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() iParticipant
					tl63Temp = ""
					tl63Temp +="Part "
					tl63Temp += iParticipant
					tl63Temp += " Name"
					twIdClientName[iParticipant] = ADD_TEXT_WIDGET(tl63Temp)
					tl63Temp = ""
					tl63Temp += "Part "
					tl63Temp += iParticipant
					tl63Temp += " Game State"
					twIdClientGameState[iParticipant] = ADD_TEXT_WIDGET(tl63Temp)
					tl63Temp = ""
					tl63Temp +="Part "
					tl63Temp += iParticipant
					tl63Temp += " Player ID"
					ADD_WIDGET_INT_READ_ONLY(tl63Temp, iParticipant)
					tl63Temp = ""
					tl63Temp +="Part "
					tl63Temp += iParticipant
					tl63Temp += " Player stored Part ID"
					ADD_WIDGET_INT_READ_ONLY(tl63Temp, iParticipant)
					UPDATE_CLIENT_GAME_STATE_WIDGET(iParticipant)
				ENDREPEAT
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
	PRINTLN("[BIKER_SELL] - created widgets.")
	
ENDPROC		

PROC UPDATE_WIDGETS()
	
	INT iPartcount
	INT r,g,b,a
	
	UPDATE_SERVER_GAME_STATE_WIDGET()
	
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() iPartcount
		UPDATE_CLIENT_GAME_STATE_WIDGET(iPartcount)
	ENDREPEAT
	
	IF NOT IS_LOCAL_DEBUG_BIT0_SET(eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW)
		IF bTerminateScriptNow
			PRINTLN("[BIKER_SELL] widget bTerminateScriptNow = TRUE.")
			SET_LOCAL_DEBUG_BIT0(eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW)
		ENDIF
	ENDIF
	
	INT i
	IF bDrawSpawnAreaMarkers
		GET_HUD_COLOUR(HUD_COLOUR_YELLOW, r, g, b, a)
		DRAW_MARKER(MARKER_CYLINDER, GET_SPAWN_AREA_CENTRAL_COORDS(0), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<100, 100, 50>>, r, g, b, 255)
		DRAW_MARKER(MARKER_CYLINDER, GET_SPAWN_AREA_CENTRAL_COORDS(1), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<100, 100, 50>>, r, g, b, 255)
		DRAW_MARKER(MARKER_CYLINDER, GET_SPAWN_AREA_CENTRAL_COORDS(2), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<100, 100, 50>>, r, g, b, 255)
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF bShowOffsets
			i = 0
			VECTOR coords
			REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
					coords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(VEH_ID(i)), GET_ENTITY_HEADING(VEH_ID(i)), <<xOffset, yOffset, zOffset>>)
					GET_HUD_COLOUR(HUD_COLOUR_YELLOW, r, g, b, a)
					DRAW_MARKER(MARKER_CYLINDER, coords, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<0.5, 0.5, 0.2>>, r, g, b)
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
ENDPROC

#ENDIF

PROC UPDATE_FOCUS_PARTICIPANT()

	// Save out the current focus participant. Stays at -1 if not the local player or not spectating a script participant.
	iFocusParticipant = (-1)
	
	IF IS_BIT_SET(iPlayerOkBitset, NATIVE_TO_INT(PLAYER_ID()))
		IF IS_BIT_SET(iParticipantActiveBitset, PARTICIPANT_ID_TO_INT())
			IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
				iFocusParticipant = PARTICIPANT_ID_TO_INT()
			ELSE
				IF IS_A_SPECTATOR_CAM_RUNNING()
				AND NOT IS_SPECTATOR_HUD_HIDDEN()
					PED_INDEX specTargetPed = GET_SPECTATOR_SELECTED_PED()
					IF IS_PED_A_PLAYER(specTargetPed)
						PLAYER_INDEX specPlayerTemp = NETWORK_GET_PLAYER_INDEX_FROM_PED(specTargetPed)
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(specPlayerTemp)
							PARTICIPANT_INDEX specTargetParticipant = NETWORK_GET_PARTICIPANT_INDEX(specPlayerTemp)
							iFocusParticipant = NATIVE_TO_INT(specTargetParticipant)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC INIT_BUYER_ID()
	
	IF serverBD.iBuyerID = -1
		serverBD.iBuyerID = g_iContraSellMissionBuyerID
		PRINTLN("[BIKER_SELL], [INIT_SERVER] [INIT_BUYER_ID] serverBD.iBuyerID = ", serverBD.iBuyerID)
	ENDIF
ENDPROC

PROC INIT_NUMBER_OF_CRATES_TO_SELL()
	IF serverBD.iCratesToSell = -1
		serverBD.iCratesToSell = g_iContraSellMissionCratesToSell
		PRINTLN("[BIKER_SELL], [INIT_SERVER] [INIT_NUMBER_OF_CRATES_TO_SELL] serverBD.iCratesToSell = ", serverBD.iCratesToSell)
	ENDIF
ENDPROC

PROC HANDLE_BONUS_SETUP()
	IF BIKER_SELL_BORDER_PATROL()
		IF GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() = 1
			serverBD.iBonus[0] = GET_MODE_TUNEABLE_BORDER_PATROL_BONUS_CASH_TOTAL()
		ELIF GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() = 2
			serverBD.iBonus[0] = GET_MODE_TUNEABLE_BORDER_PATROL_BONUS_CASH_TOTAL() / 2
			serverBD.iBonus[1] = GET_MODE_TUNEABLE_BORDER_PATROL_BONUS_CASH_TOTAL() / 2
		ELIF GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() = 3
			serverBD.iBonus[0] = GET_MODE_TUNEABLE_BORDER_PATROL_BONUS_CASH_TOTAL() / 3
			serverBD.iBonus[1] = GET_MODE_TUNEABLE_BORDER_PATROL_BONUS_CASH_TOTAL() / 3
			serverBD.iBonus[2] = GET_MODE_TUNEABLE_BORDER_PATROL_BONUS_CASH_TOTAL() / 3
		ENDIF
	ENDIF
ENDPROC

PROC SERVER_SET_FACTORY()
	IF serverBD.factoryId = FACTORY_ID_INVALID
		INT iNumPlayersInFactory
		ILLICIT_GOODS_MISSION_DATA eData 
		eData = GB_GET_PLAYER_ILLICIT_GOOD_MISSION_DATA()
		
		FACTORY_TYPE category = GET_GOODS_CATEGORY_FROM_FACTORY_ID(eData.eMissionFactory)
		serverBD.factoryId = eData.eMissionFactory
		serverBD.factoryType = category
		serverBD.eSellLocality = eData.eSellLocality
	
		PRINTLN("[BIKER_SELL] [FACTORY INFORMATION] [START] - Factory ID: ", GET_FACTORY_NAME_FOR_DEBUG(serverBD.factoryId))
		PRINTLN("[BIKER_SELL] [FACTORY INFORMATION] [START] - Factory Type: ", GET_FACTORY_NAME_FROM_ID(serverBD.factoryId))
		PRINTLN("[BIKER_SELL] [FACTORY INFORMATION] [START] - Factory Product total: ", GET_PRODUCT_TOTAL_FOR_FACTORY(PLAYER_ID(), serverBD.factoryId))
		PRINTLN("[BIKER_SELL] [FACTORY INFORMATION] [START] - Factory Sell locality: ", GET_LOCALITY_NAME_FOR_DEBUG(serverBD.eSellLocality))
	
		#IF IS_DEBUG_BUILD
		IF GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2() > -1
			IF INT_TO_ENUM(eBIKER_SELL_VARIATION, GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2()) = eBIKER_SELL_CLUB_RUN
			OR INT_TO_ENUM(eBIKER_SELL_VARIATION, GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2()) = eBIKER_SELL_BAG_DROP
			OR INT_TO_ENUM(eBIKER_SELL_VARIATION, GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2()) = eBIKER_SELL_RACE
			OR INT_TO_ENUM(eBIKER_SELL_VARIATION, GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2()) = eBIKER_SELL_BENSON
			OR INT_TO_ENUM(eBIKER_SELL_VARIATION, GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2()) = eBIKER_SELL_FRIENDS_IN_NEED
				IF IS_PLAYER_IN_FACTORY(PLAYER_ID())
					IF GET_FACTORY_PLAYER_IS_IN(PLAYER_ID()) != serverBD.factoryId
						PRINTLN("[BIKER_SELL] SERVER_SET_FACTORY - Bag mode launched through M menu, updating factory information to match factory local player is inside")
						serverBD.factoryId = GET_FACTORY_PLAYER_IS_IN(PLAYER_ID())
						serverBD.factoryType = GET_GOODS_CATEGORY_FROM_FACTORY_ID(serverBD.factoryId)
						serverBD.eSellLocality = eData.eSellLocality
						PRINTLN("[BIKER_SELL] SERVER_SET_FACTORY - Factory ID: ", GET_FACTORY_NAME_FOR_DEBUG(serverBD.factoryId))
						PRINTLN("[BIKER_SELL] SERVER_SET_FACTORY - Factory Type: ", GET_FACTORY_TYPE_NAME(serverBD.factoryType))
						PRINTLN("[BIKER_SELL] SERVER_SET_FACTORY - Sell locality: ", GET_LOCALITY_NAME_FOR_DEBUG(serverBD.eSellLocality))
					ELSE
						PRINTLN("[BIKER_SELL] SERVER_SET_FACTORY - Bag mode launched through M menu, player is in the correct factory! Yaaay!")
					ENDIF
				ELSE
					PRINTLN("[BIKER_SELL] SERVER_SET_FACTORY - Player has launched bag mission and they arent in a factory, this just isn't going to work out, i'm sorry. It's over, you'll hear from my attorney.")
				ENDIF
			ENDIF
		ENDIF
		#ENDIF
		
		BOOL bMCTLaunch 

		// Grab the number of players in the factory
		INT i
		INT iBagCap = ciMAX_SELL_BAGS
		IF BIKER_SELL_FRIENDS_IN_NEED()
			iBagCap = g_sMPTunables.iBIKER_SELL_FRIENDS_IN_NEED_MAX_BAGS
		ENDIF
		REPEAT NUM_NETWORK_PLAYERS i
			IF iNumPlayersInFactory < iBagCap
			AND NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(i))
				PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(i)
				
				PRINTLN("[BIKER_SELL] [bag spam] SERVER_SET_FACTORY a ")
				
				IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerID, GB_GET_LOCAL_PLAYER_GANG_BOSS())
				
					bMCTLaunch = g_bLaunchedMissionFrmMCT AND IS_PLAYER_IN_ARCADE_PROPERTY(playerID) 
				
					PRINTLN("[BIKER_SELL] [bag spam] SERVER_SET_FACTORY b ", bMCTLaunch)
					IF IS_PLAYER_IN_FACTORY(playerID)
					OR bMCTLaunch
					
						PRINTLN("[BIKER_SELL] [bag spam] SERVER_SET_FACTORY c ")
						IF GET_FACTORY_PLAYER_IS_IN(playerID) = serverBD.factoryId
						OR bMCTLaunch
						
							serverBD.iAssignedBag[iNumPlayersInFactory] = i
							
							PRINTLN("[BIKER_SELL] [bag spam] SERVER_SET_FACTORY serverBD.iAssignedBag[iNumPlayersInFactory] = ", serverBD.iAssignedBag[iNumPlayersInFactory])
							iNumPlayersInFactory++
						ELSE
							PRINTLN("[BIKER_SELL] [bag spam] SERVER_SET_FACTORY serverBD.iDebugVariation2 = ", serverBD.iDebugVariation2)
						ENDIF
					ELSE
						PRINTLN("[BIKER_SELL] [bag spam] SERVER_SET_FACTORY serverBD.iDebugVariation2 = ", serverBD.iDebugVariation2)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		serverBD.iNumPlayersInFactory = iNumPlayersInFactory
		PRINTLN("[BIKER_SELL] SERVER_SET_FACTORY serverBD.iNumPlayersInFactory = ", serverBD.iNumPlayersInFactory)
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_IgnoreGBMissionLaunchChecks")
			IF serverBD.iNumPlayersInFactory < 1
				serverBD.iNumPlayersInFactory = 1
				serverBD.iAssignedBag[0] = NATIVE_TO_INT(PLAYER_ID())
				PRINTLN("[BIKER_SELL] SERVER_SET_FACTORY sc_IgnoreGBMissionLaunchChecks forcing serverBD.iNumPlayersInFactory = 1")
			ENDIF

			IF GET_COMMANDLINE_PARAM_EXISTS("sc_biker_sell_force_bag")
				serverBD.iNumPlayersInFactory = GET_COMMANDLINE_PARAM_INT("sc_biker_sell_force_bag")
				IF serverBD.iNumPlayersInFactory < 1
					serverBD.iNumPlayersInFactory = 1
				ELIF serverBD.iNumPlayersInFactory > 1
					serverBD.iAssignedBag[1] = 1
				ENDIF
				PRINTLN("[BIKER_SELL] SERVER_SET_FACTORY sc_biker_sell_force_bag forcing serverBD.iNumPlayersInFactory = ", serverBD.iNumPlayersInFactory)
			ENDIF
		ENDIF
		#ENDIF
		PRINTLN("[BIKER_SELL] SERVER_SET_FACTORY TO ", GET_FACTORY_NAME_FROM_ID(serverBD.factoryId), " category = ", GET_FACTORY_TYPE_NAME(category))
	ENDIF
ENDPROC


FUNC eDIFFICULTY_STATE GET_MISSION_DIFFICULTY_FROM_TUNABLE(INT iTunable)
	SWITCH iTunable
		CASE 0 RETURN eDIFFICULTY_EASY
		CASE 1 RETURN eDIFFICULTY_MEDIUM
		CASE 2 RETURN eDIFFICULTY_HARD
	ENDSWITCH
	RETURN eDIFFICULTY_EASY
ENDFUNC
PROC SERVER_SET_DIFFICULTY()
	IF serverBD.eDifficulty = eDIFFICULTY_NOT_SET
		SWITCH serverBD.factoryType
			CASE FACTORY_TYPE_CRACK		
				PRINTLN("SERVER_SET_DIFFICULTY TO eDIFFICULTY_HARD  ")
				serverBD.eDifficulty = GET_MISSION_DIFFICULTY_FROM_TUNABLE(g_sMPTunables.iBIKER_COCAINE_MISSION_DIFFICULTY)
			BREAK
			CASE FACTORY_TYPE_FAKE_IDS		
				PRINTLN("SERVER_SET_DIFFICULTY TO eDIFFICULTY_EASY  ")
				serverBD.eDifficulty = GET_MISSION_DIFFICULTY_FROM_TUNABLE(g_sMPTunables.iBIKER_DOCUMENTS_MISSION_DIFFICULTY)
			BREAK
			CASE FACTORY_TYPE_FAKE_MONEY	
				PRINTLN("SERVER_SET_DIFFICULTY TO eDIFFICULTY_MEDIUM  ")
				serverBD.eDifficulty = GET_MISSION_DIFFICULTY_FROM_TUNABLE(g_sMPTunables.iBIKER_COUNTERFEIT_CASH_MISSION_DIFFICULTY)
			BREAK
			CASE FACTORY_TYPE_METH			
				PRINTLN("SERVER_SET_DIFFICULTY TO eDIFFICULTY_MEDIUM  ")
				serverBD.eDifficulty = GET_MISSION_DIFFICULTY_FROM_TUNABLE(g_sMPTunables.iBIKER_METH_MISSION_DIFFICULTY)
			BREAK
			CASE FACTORY_TYPE_WEED			
				PRINTLN("SERVER_SET_DIFFICULTY TO eDIFFICULTY_EASY  ")
				serverBD.eDifficulty = GET_MISSION_DIFFICULTY_FROM_TUNABLE(g_sMPTunables.iBIKER_WEED_MISSION_DIFFICULTY)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC BOOL SERVER_SET_CHASE_VARIATION()
	IF BIKER_SELL_CLUB_RUN()
		IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_CLUB_RUN_COPS)
		AND NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_CLUB_RUN_GANG)
		AND NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_CLUB_NOTHING)
			PRINTLN("[BIKER_SELL] SERVER_SET_CHASE_VARIATION")
			INT iRand = GET_RANDOM_INT_IN_RANGE(0, 150)
			IF  iRand > 100
				//Nothing
				PRINTLN("[BIKER_SELL] [TWIST] SERVER_SET_CHASE_VARIATION - iRand = ", iRand, " - do nothing")
				SET_SERVER_BIT1(eSERVERBITSET1_CLUB_NOTHING)
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_biker_sell_force_bag")
					SET_SERVER_BIT1(eSERVERBITSET1_CLUB_RUN_COPS)
					PRINTLN("[BIKER_SELL] [TWIST] SERVER_SET_CHASE_VARIATION - iRand = ", iRand, " - SET_SERVER_BIT1(eSERVERBITSET1_CLUB_RUN_GANG)")
				ENDIF
				#ENDIF
			ELIF iRand > 50
				PRINTLN("[BIKER_SELL] [TWIST] SERVER_SET_CHASE_VARIATION - iRand = ", iRand, " - SET_SERVER_BIT1(eSERVERBITSET1_CLUB_RUN_COPS)")
				SET_SERVER_BIT1(eSERVERBITSET1_CLUB_RUN_COPS)
			ELSE
				SET_SERVER_BIT1(eSERVERBITSET1_CLUB_RUN_GANG)
				PRINTLN("[BIKER_SELL] [TWIST] SERVER_SET_CHASE_VARIATION - iRand = ", iRand, " - SET_SERVER_BIT1(eSERVERBITSET1_CLUB_RUN_GANG)")
			ENDIF
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC REQUEST_SELL_MODELS()

	INT i

	IF DOES_MODE_NEED_DROP_OFF_VEH()
	AND NOT BIKER_SELL_AIR_DROP_AT_SEA()
		REPEAT GET_NUM_DROP_OFF_VEH_FOR_VARIATION() i
			REQUEST_LOAD_MODEL(GET_DROP_OFF_VEH_MODEL(i))
		ENDREPEAT
	ENDIF
	
	IF DOES_MODE_NEED_DROP_OFF_PED()	
		REPEAT GET_NUM_DROP_OFF_PEDS_FOR_VARIATION() i
			REQUEST_LOAD_MODEL(GET_DROP_OFF_PED_MODEL(i))
		ENDREPEAT
	ENDIF
	
	IF DOES_VARIATION_HAVE_START_VEHICLE()
		REQUEST_LOAD_MODEL(GET_VEHICLE_MODEL_FOR_VARIATION())
	ENDIF
	
	IF DOES_VARIATION_HAVE_AI_PEDS()
	AND DOES_VARIATION_SPAWN_PEDS_AT_START()
		REPEAT GET_NUM_PEDS_FOR_VARIATION() i
			REQUEST_LOAD_MODEL(GET_PED_MODEL_FOR_VARIATION(i))	
		ENDREPEAT
	ENDIF
	
	IF VAR_HAS_SELL_PROPS()
		REQUEST_LOAD_MODEL(GET_SELL_PROP_MODEL())
	ENDIF
	
	IF DOES_VARIATION_REQUIRE_CRATES_IN_VEHICLES()
		REQUEST_LOAD_MODEL(mnCRATE_FOR_ATTACH())
	ENDIF
ENDPROC

FUNC BOOL INIT_SERVER()
	INIT_BUYER_ID()
	
	INIT_NUMBER_OF_CRATES_TO_SELL()
	SERVER_SET_FACTORY()
	SERVER_SET_DIFFICULTY()
	
	IF SERVER_PICK_VARIATION()
	AND SERVER_SET_START_WAREHOUSE()
	AND SERVER_SET_SHIPMENT_TYPE()
	AND SERVER_SET_START_POS()
	AND SERVER_SET_ROUTE()
	AND SERVER_SET_CHASE_VARIATION()
		SERVER_SET_STING()
		SERVER_SET_PROVEN_WANTED()
		REQUEST_SELL_MODELS()
		
		IF SERVER_CHOOSE_DROP_OFFS()
			INIT_GANG_CHASE_MODELS_FOR_VARIATION()
			
			IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
			AND NOT BIKER_SELL_CLUB_RUN()
				serverBD.iRandVehToAttack = GET_RANDOM_INT_IN_RANGE(0, GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE())
				PRINTLN("[BIKER_SELL], INIT_SERVER - Rand veh to attack is: ", serverBD.iRandVehToAttack)
			ENDIF
			
			PRINTLN("[BIKER_SELL] * INIT_SERVER DONE ")
			
			serverBD.iOriginalDropTarget = iDROP_TARGET()
			
			PRINTLN("[BIKER_SELL] * INIT_SERVER, serverBD.iOriginalDropTarget = ", serverBD.iOriginalDropTarget)
			
			HANDLE_BONUS_SETUP()
			
			IF BIKER_SELL_RACE()
				GB_BIKER_RACE_SET_IS_MULTI_RACE(serverBD.sRaceServerVars)
			ENDIF
			
			serverBD.piLaunchBoss = PLAYER_ID()
			
			serverBD.playerRequestedCrate = INVALID_PLAYER_INDEX()
			
			INIT_IDEAL_SPAWN_COORDS()
			
			// Get MatchIds for Telemetry
			PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchId1, serverBD.iMatchId2)
			
			RETURN TRUE
		ELSE
			PRINTLN("[BIKER_SELL] INIT_SERVER WAITING FOR SERVER_CHOOSE_DROP_OFFS")
		ENDIF
	ELSE
		PRINTLN("[BIKER_SELL] INIT_SERVER WAITING FOR SERVER_PICK_VARIATION ETC")
		
		
		
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL INIT_CLIENT()
	
	PRINTLN("[BIKER_SELL] INIT_CLIENT - Local player is not inside warehouse, proceeding with client initialisation")
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_FlowPlaySellMissionPlaylist")
		MPGlobalsAmbience.sMagnateGangBossData.iSellMissionPlaylistCount++
	ENDIF
	#ENDIF
	
	SET_SUPPRESS_GANG_CHASE_MODELS_FOR_VARIATION( TRUE)
		
	IF BIKER_SELL_AIR_DROP_AT_SEA()
		sLocaldata.iAltitudeWarningSound = GET_SOUND_ID()
		PRINTLN("[BIKER_SELL] [INIT_CLIENT] Got iAltitudeWarningSound sound id")
	ENDIF
	
	IF NOT GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
		IF serverBD.iCratesToSell != -1
			g_iContraSellMissionCratesToSell = serverBD.iCratesToSell
			g_iContraSellMissionBuyerID = serverBD.iBuyerID
			PRINTLN("[BIKER_SELL] [INIT_CLIENT] I'm not boss, set g_iContraSellMissionCratesToSell = ", g_iContraSellMissionCratesToSell, " g_iContraSellMissionBuyerID = ", g_iContraSellMissionBuyerID)
		ENDIF
	ENDIF
	
	IF BIKER_SELL_BORDER_PATROL()
		REQUEST_ANIM_DICT(GET_ANIM_DICT_FOR_THROW_DIRECTION(ciTHROW_DIRECTION_LEFT))
		REQUEST_ANIM_DICT(GET_ANIM_DICT_FOR_THROW_DIRECTION(ciTHROW_DIRECTION_RIGHT))
	ENDIF
	
	// Setup scenario blocking
	IF SHOULD_FACTORY_DROP_OFF_BLOCK_SCENARIO_PEDS(serverBD.factoryId)
		VECTOR vMin, vMax
		GET_FACTORY_DROP_OFF_SCENARIO_BLOCKING_AREA(serverBD.factoryId, vMin, vMax)
	
		IF NOT DOES_SCENARIO_BLOCKING_AREA_EXISTS(vMin, vMax)
			sLocalData.ScenarioBlockingIndex = ADD_SCENARIO_BLOCKING_AREA(vMin, vMax)
			sLocalData.bSetupScenarioBlocking = TRUE
		ENDIF
	ENDIF
	
	IF MODE_HAS_PICKUP_BAGS()
		SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(GET_BAG_MODEL(), 1)
	ENDIF
	
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)			
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)	
	
	playerBD[PARTICIPANT_ID_TO_INT()].damagerPlayer = INVALID_PLAYER_INDEX()
	
	GB_SET_BIKER_SELL_VARIATION_LOCAL_PLAYER_IS_ON(ENUM_TO_INT(GET_SELL_VAR()))
	
	g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchId1
	g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchId2
	
	RETURN TRUE
ENDFUNC

//----------------------
//	MAIN SCRIPT
//----------------------

 // FEATURE_BIKER

SCRIPT(MP_MISSION_DATA missionScriptArgs)


	PRINTLN("[BIKER_SELL] * START ")//, BIKER_SELL_GET_VARIATION_DEBUG_PRINT(GET_SELL_VAR()))
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			
			IF NOT PROCESS_PRE_GAME(missionScriptArgs)
				PRINTLN("[BIKER_SELL] TEST_BROKE1 ")
				EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
				SCRIPT_CLEANUP()
			ENDIF
			// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
			#IF IS_DEBUG_BUILD
				CREATE_WIDGETS()
			#ENDIF
		ELSE
			PRINTLN("[BIKER_SELL] TEST_BROKE2 ")
			PRINTLN("[BIKER_SELL] calling SCRIPT_CLEANUP() - IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE) = FALSE")
			SCRIPT_CLEANUP()
		ENDIF
	ELSE
		PRINTLN("[BIKER_SELL] TEST_BROKE3 ")
		PRINTLN("[BIKER_SELL] calling SCRIPT_CLEANUP() - NETWORK_IS_GAME_IN_PROGRESS() = FALSE")
		EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
		SCRIPT_CLEANUP()
		
	ENDIF
	
	bGangBossOnStart = GB_IS_LOCAL_PLAYER_BOSS_OF_A_BIKER_GANG()
	
	WHILE (TRUE)
	
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		IF IS_LOCAL_DEBUG_BIT0_SET(eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW)
			PRINTLN("[BIKER_SELL] TEST_BROKE4 ")
			PRINTLN("[BIKER_SELL]  eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW is set")
			EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		#ENDIF

		IF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION()
			PRINTLN("[BIKER_SELL] TEST_BROKE6 ")
			PRINTLN("[BIKER_SELL]  - GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION = TRUE")
			EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[BIKER_SELL] TEST_BROKE7 ")
			PRINTLN("[BIKER_SELL] SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
			EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
			SCRIPT_CLEANUP(TRUE)
		ENDIF
		
		UPDATE_FOCUS_PARTICIPANT()
		PROCESS_EVENTS()
		
		//GB_MAINTAIN_SPECTATE(sSpecVars)

		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
		#ENDIF
		
		SWITCH(GET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT()))
		
			CASE GAME_STATE_INIT
				IF GET_SERVER_GAME_STATE() = GAME_STATE_RUNNING

					IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_BIKER_GANG()
					AND NOT IS_INITIAL_PRODUCT_REMOVAL_DISABLED()
						IF DOES_FACTORY_HAVE_ENOUGH_CONTRABAND_TO_INITIALLY_REMOVE()
						
							IF playerBD[PARTICIPANT_ID_TO_INT()].iProductRemovedAtStart = -1
								playerBD[PARTICIPANT_ID_TO_INT()].iProductRemovedAtStart = GET_PRODUCT_TO_REMOVE_INITIALLY()
							ENDIF
							REMOVE_PRODUCT_FROM_FACTORY(serverBD.factoryId, GET_PRODUCT_TO_REMOVE_INITIALLY(), 0, TRUE, REMOVE_CONTRA_MISSION_STARTED, eResult)
							PRINTLN("[BIKER_SELL] HANDLE_REWARDS - REMOVE_PRODUCT_FROM_FACTORY: ", serverBD.factoryId)
							
							IF eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
							OR eResult = CONTRABAND_TRANSACTION_STATE_FAILED

								IF eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
									PRINTLN("[BIKER_SELL] HANDLE_REWARDS - REMOVE_PRODUCT_FROM_FACTORY: Transaction success! Removed product intially from factory")
									playerBD[PARTICIPANT_ID_TO_INT()].iProductLeftInFactory = GET_PRODUCT_TOTAL_FOR_FACTORY(PLAYER_ID(), serverBD.factoryId)
									PRINTLN("[BIKER_SELL] HANDLE_REWARDS - REMOVE_PRODUCT_FROM_FACTORY: [SUCCESS] playerBD[PARTICIPANT_ID_TO_INT()].iProductLeftInFactory = ", playerBD[PARTICIPANT_ID_TO_INT()].iProductLeftInFactory)
									PRINTLN("[BIKER_SELL] HANDLE_REWARDS - REMOVE_PRODUCT_FROM_FACTORY: [SUCCESS] playerBD[PARTICIPANT_ID_TO_INT()].iProductRemovedAtStart = ", playerBD[PARTICIPANT_ID_TO_INT()].iProductRemovedAtStart)
								ELIF eResult = CONTRABAND_TRANSACTION_STATE_FAILED
									PRINTLN("[BIKER_SELL] HANDLE_REWARDS - REMOVE_PRODUCT_FROM_FACTORY: Transaction fail! Failed to remove product intially from factory, this could be awkward")
									playerBD[PARTICIPANT_ID_TO_INT()].iProductRemovedAtStart = 0
									playerBD[PARTICIPANT_ID_TO_INT()].iProductLeftInFactory = GET_PRODUCT_TOTAL_FOR_FACTORY(PLAYER_ID(), serverBD.factoryId)
									PRINTLN("[BIKER_SELL] HANDLE_REWARDS - REMOVE_PRODUCT_FROM_FACTORY: [FAIL] playerBD[PARTICIPANT_ID_TO_INT()].iProductLeftInFactory = ", playerBD[PARTICIPANT_ID_TO_INT()].iProductLeftInFactory)
									PRINTLN("[BIKER_SELL] HANDLE_REWARDS - REMOVE_PRODUCT_FROM_FACTORY: [FAIL] playerBD[PARTICIPANT_ID_TO_INT()].iProductRemovedAtStart = ", playerBD[PARTICIPANT_ID_TO_INT()].iProductRemovedAtStart)
								ENDIF

								IF INIT_CLIENT()

									SET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_RUNNING)
									//Call the set up if it's not already been done
									SET_AUDIO_FLAGS_ON_MUSIC_START()
								ENDIF
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[BIKER_SELL] HANDLE_REWARDS - REMOVE_PRODUCT_FROM_FACTORY: Waiting for transaction result...")
							#ENDIF
							ENDIF
						ELSE
							playerBD[PARTICIPANT_ID_TO_INT()].iProductLeftInFactory = GET_PRODUCT_TOTAL_FOR_FACTORY(PLAYER_ID(), serverBD.factoryId)
							playerBD[PARTICIPANT_ID_TO_INT()].iProductRemovedAtStart = 0
							PRINTLN("[BIKER_SELL] HANDLE_REWARDS - REMOVE_PRODUCT_FROM_FACTORY: We dont have enough to remove, so the amount left is what is in the factory")
							PRINTLN("[BIKER_SELL] HANDLE_REWARDS - REMOVE_PRODUCT_FROM_FACTORY: [NOT ENOUGH] playerBD[PARTICIPANT_ID_TO_INT()].iProductLeftInFactory = ", playerBD[PARTICIPANT_ID_TO_INT()].iProductLeftInFactory)
							PRINTLN("[BIKER_SELL] HANDLE_REWARDS - REMOVE_PRODUCT_FROM_FACTORY: [NOT ENOUGH] playerBD[PARTICIPANT_ID_TO_INT()].iProductRemovedAtStart = ", playerBD[PARTICIPANT_ID_TO_INT()].iProductRemovedAtStart)
														
							IF INIT_CLIENT()

								SET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_RUNNING)
								//Call the set up if it's not already been done
								SET_AUDIO_FLAGS_ON_MUSIC_START()
							ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_BIKER_GANG()
							PRINTLN("[BIKER_SELL] - No initial contra taken - Local player is a goon")
						ENDIF
						
						IF IS_INITIAL_PRODUCT_REMOVAL_DISABLED()
							PRINTLN("[BIKER_SELL] - No initial contra taken - Initial contraband removal is disabled via tunable")
							playerBD[PARTICIPANT_ID_TO_INT()].iProductLeftInFactory = GET_PRODUCT_TOTAL_FOR_FACTORY(PLAYER_ID(), serverBD.factoryId)
							playerBD[PARTICIPANT_ID_TO_INT()].iProductRemovedAtStart = 0
							PRINTLN("[BIKER_SELL] HANDLE_REWARDS - REMOVE_PRODUCT_FROM_FACTORY: Product removal is disabled, so")
							PRINTLN("[BIKER_SELL] HANDLE_REWARDS - REMOVE_PRODUCT_FROM_FACTORY: [DISABLED] playerBD[PARTICIPANT_ID_TO_INT()].iProductLeftInFactory = ", playerBD[PARTICIPANT_ID_TO_INT()].iProductLeftInFactory)
							PRINTLN("[BIKER_SELL] HANDLE_REWARDS - REMOVE_PRODUCT_FROM_FACTORY: [DISABLED] playerBD[PARTICIPANT_ID_TO_INT()].iProductRemovedAtStart = ", playerBD[PARTICIPANT_ID_TO_INT()].iProductRemovedAtStart)
						ENDIF
						
						#ENDIF
					
						IF INIT_CLIENT()
							SET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_RUNNING)
							//Call the set up if it's not already been done
							SET_AUDIO_FLAGS_ON_MUSIC_START()
						ENDIF
					ENDIF
				ELIF GET_SERVER_GAME_STATE() = GAME_STATE_END
					SET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT(), GAME_STATE_END)
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_GAME_STATE() = GAME_STATE_RUNNING
				
					CLIENT_PROCESSING()
					
				ELIF GET_SERVER_GAME_STATE() = GAME_STATE_END
					SET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT(), GAME_STATE_END)
				ENDIF
			BREAK
			
			CASE GAME_STATE_END
				PRINTLN("[BIKER_SELL] TEST_BROKE8 ")
				SCRIPT_CLEANUP()
			BREAK
			
		ENDSWITCH
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			IF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION()
				PRINTLN("[BIKER_SELL] TEST_BROKE5 ")
				PRINTLN("[BIKER_SELL] - GB_SHOULD_KILL_ACTIVE_BOSS_MISSION = TRUE")
				SET_SERVER_GAME_STATE(GAME_STATE_END)
			ENDIF
		
			SWITCH(GET_SERVER_GAME_STATE())
			
				CASE GAME_STATE_INIT
					#IF IS_DEBUG_BUILD
					IF IS_DEBUG_KEY_JUST_RELEASED(KEY_F, KEYBOARD_MODIFIER_NONE, "F Fail")
						SCRIPT_CLEANUP()
					ENDIF
					#ENDIF
					IF INIT_SERVER()
						SET_SERVER_GAME_STATE(GAME_STATE_RUNNING)
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING
					SERVER_PROCESSING()
					SERVER_PROCESS_PARTICIPANT_LOOP()
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
		
		ENDIF
		
	ENDWHILE

	
	
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT


