
//////////////////////////////////////////////////////////////////////////////////////////
// Name:        GB_BIKER_RIPPING_IT_UP.sc												//
// Description: Bikers try to cause the most destruction in freemode					//
// Written by:  Martin McMillan															//
// Date: 18/07/2016																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"
USING "net_common_functions.sch"


//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_gang_boss.sch"
USING "am_common_ui.sch"
USING "DM_Leaderboard.sch"
USING "net_gang_boss_spectate.sch"
USING "clothes_shop_private.sch"

//----------------------
//	ENUM
//----------------------

ENUM eMODE_STATE
	eMODESTATE_INIT = 0,
	eMODESTATE_RUN,
	eMODESTATE_REWARDS,
	eMODESTATE_END
ENDENUM

ENUM eEND_REASON
	eENDREASON_NO_REASON_YET = 0,
	eENDREASON_TIME_UP,
	eENDREASON_NO_BOSS_LEFT,
	eENDREASON_ALL_GOONS_LEFT,
	eENDREASON_WIN_CONDITION_TRIGGERED
ENDENUM

ENUM eSERVER_BITSET_0
	eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS = 0,		//01
	eSERVERBITSET0_SERVER_INITIALISED,							//02
	eSERVERBITSET0_END // Keep this at the end.
ENDENUM

ENUM eCLIENT_BITSET_0
	eCLIENTBITSET0_COMPLETED_REWARDS = 0,
	eCLIENTBITSET0_CLIENT_INITIALISED,
	eCLIENTBITSET0_ON_BIKE,
	eCLIENTBITSET0_END // Keep this at the end.
ENDENUM

ENUM eLOCAL_BITSET_0
	eLOCALBITSET0_EXECUTED_END_TELEMTRY = 0,
	eLOCALBITSET0_CALCULATED_REWARDS,
	eLOCALBITSET0_CALLED_COMMON_SETUP,
	eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP,
	eLOCALBITSET0_CALLED_START_COMMON_TELEMETRY,
	eLOCALBITSET0_CHANGING_CLOTHES,
	eLOCALBITSET0_CALCULATED_CLOTHES_COST,
	eLOCALBITSET0_END // Keep this at the end.
ENDENUM

ENUM HELP_TEXT_ENUM
	eHELPTEXT_INTRO = 0,
	eHELPTEXT_INTRO2,
	
	eHT_END // keep this at the end
ENDENUM

ENUM BIG_MESSAGE_ENUM
	eBIGMESSAGE_START = 0,
	eBIGMESSAGE_WIN,
	eBIGMESSAGE_LOSE,
	eBIGMESSAGE_GOONS_LEFT,
	
	eBIGM_END // keep this at the end

ENDENUM

//----------------------
//	DEBUG
//----------------------

#IF IS_DEBUG_BUILD

ENUM eSERVER_DEBUG_BITSET_0
	eSERVERDEBUGBITSET0_TEMP = 0,
	eSERVERDEBUGBITSET0_END // Keep this at the end.
ENDENUM

ENUM eCLIENT_DEBUG_BITSET_0
	eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED = 0,
	eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED,
	eCLIENTDEBUGBITSET0_END // Keep this at the end.
ENDENUM

ENUM eLOCAL_DEBUG_BITSET_0
	eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW = 0,
	eLOCALDEBUGBITSET0_END // Keep this at the end.
ENDENUM

#ENDIF


//----------------------
//	CONSTANTS
//----------------------

CONST_INT GAME_STATE_INIT 								0
CONST_INT GAME_STATE_RUNNING							1
CONST_INT GAME_STATE_TERMINATE_DELAY					2
CONST_INT GAME_STATE_END								3

CONST_INT TRACK_GOON_DELAY								1000*10

//----------------------
//	STRUCT
//----------------------

STRUCT STRUCT_PARTICIPANT_ID_DATA
	PARTICIPANT_INDEX participantId
	INT iPlayerId
	 TEXT_LABEL_63 tl63Name
	BOOL bIsBoss
	BOOL bIsGoon
ENDSTRUCT

STRUCT STRUCT_PLAYER_ID_DATA
	PLAYER_INDEX playerId
	INT iParticipantId
	PED_INDEX playerPedId
ENDSTRUCT


//----------------------
//	LOCAL VARIABLES
//----------------------

STRUCT_PARTICIPANT_ID_DATA sParticipant[GB_MAX_GANG_SIZE_INCLUDING_BOSS]
STRUCT_PLAYER_ID_DATA sPlayer[NUM_NETWORK_PLAYERS]
GANG_BOSS_MANAGE_REWARDS_DATA sGangBossManageRewardsData
GB_MAINTAIN_SPECTATE_VARS sSpecVars
GB_STRUCT_BOSS_END_UI sEndUI

INT iLocalBitset0
INT iLocalHelpBitset0
INT iLocalBigMessageBitset0

INT iPlayerOkBitset
INT iParticipantActiveBitset
INT iPlayerDeadBitset

INT iFocusParticipant

INT iTimeRemaining

//VEHICLE_INDEX currentVehicle
//MODEL_NAMES eCurrentVehicleModel

//VECTOR vFocusParticipantCoords
INT iNumGoonsOnScript

#IF IS_DEBUG_BUILD
INT iLocalDebugBitset
#ENDIF

INT serverStaggeredPlayerLoop

GB_COUNTDOWN_MUSIC_STRUCT cmStruct

TEXT_LABEL_63 currentMusic

//----------------------
//	BROADCAST VARIABLES
//----------------------

// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData

	INT iServerGameState = GAME_STATE_INIT
	INT iServerBitSet
	SCRIPT_TIMER modeTimer
	eMODE_STATE eModeState
	eEND_REASON eEndReason
	
	PLAYER_INDEX piLaunchBoss
	
	#IF IS_DEBUG_BUILD
	INT iDebugBitset
	INT iSPassPart = -1
	INT iFFailPart = -1
	BOOL bFakeEndOfModeTimer
	#ENDIF
	
	INT					iMatchId1
	INT					iMatchId2
	
	INT	iPlayerBitSet
	
	AMBIENT_ACTIVITY_LEADERBOARD_STRUCT sSortedLeaderboard[GB_MAX_GANG_SIZE_INCLUDING_BOSS]
	
	INT iEndTime
	
ENDSTRUCT

ServerBroadcastData serverBD

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	
	INT iClientGameState = GAME_STATE_INIT
	INT iClientBitSet
	
	#IF IS_DEBUG_BUILD
	INT iDebugBitset
	#ENDIF
	
	INT iScore = 0
	
ENDSTRUCT

PlayerBroadcastData playerBD[GB_MAX_GANG_SIZE_INCLUDING_BOSS]


//----------------------
//	TUNEABLE FUNCTIONS
//----------------------

// Add tuneables here.
FUNC INT GET_MODE_TUNEABLE_TIME_LIMIT()
	#IF IS_DEBUG_BUILD
	IF g_iDebugBikerTimeOut != 0
		RETURN g_iDebugBikerTimeOut
	ENDIF
	#ENDIF
	RETURN g_sMPTunables.iBIKER_RIPPIN_TIME_LIMIT * 1000 // Default of 10 minutes. Change this to whatever is needed. Add tuneable once they have been added.
ENDFUNC


//----------------------
//	FUNCTIONS
//----------------------

#IF IS_DEBUG_BUILD
FUNC STRING GET_SERVER_BIT0_NAME(eSERVER_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS	RETURN "eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS"
		CASE eSERVERBITSET0_SERVER_INITIALISED					RETURN "eSERVERBITSET0_SERVER_INITIALISED"
		CASE eSERVERBITSET0_END									RETURN "eSERVERBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC

FUNC STRING GET_LOCAL_BIT0_NAME(eLOCAL_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eLOCALBITSET0_EXECUTED_END_TELEMTRY			RETURN "eLOCALBITSET0_EXECUTED_END_TELEMTRY"
		CASE eLOCALBITSET0_CALCULATED_REWARDS				RETURN "eLOCALBITSET0_CALCULATED_REWARDS"
		CASE eLOCALBITSET0_CALLED_COMMON_SETUP				RETURN "eLOCALBITSET0_CALLED_COMMON_SETUP"
		CASE eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP	RETURN "eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP"
		CASE eLOCALBITSET0_CALLED_START_COMMON_TELEMETRY	RETURN "eLOCALBITSET0_CALLED_START_COMMON_TELEMETRY"
		CASE eLOCALBITSET0_CHANGING_CLOTHES					RETURN "eLOCALBITSET0_CHANGING_CLOTHES"
		CASE eLOCALBITSET0_CALCULATED_CLOTHES_COST			RETURN "eLOCALBITSET0_CALCULATED_CLOTHES_COST"
		CASE eLOCALBITSET0_END								RETURN "eLOCALBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC

FUNC STRING GET_CLIENT_BIT0_NAME(eCLIENT_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eCLIENTBITSET0_COMPLETED_REWARDS	RETURN "eCLIENTBITSET0_COMPLETED_REWARDS"
		CASE eCLIENTBITSET0_CLIENT_INITIALISED	RETURN "eCLIENTBITSET0_CLIENT_INITIALISED"
		CASE eCLIENTBITSET0_ON_BIKE				RETURN "eCLIENTBITSET0_ON_BIKE"
		CASE eCLIENTBITSET0_END					RETURN "eCLIENTBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC
#ENDIF

FUNC BOOL IS_SERVER_BIT0_SET(eSERVER_BITSET_0 eBitset)
	RETURN IS_BIT_SET(serverBD.iServerBitSet, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_SERVER_BIT0(eSERVER_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_SERVER_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] GET_SERVER_BIT0_NAME - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		#IF IS_DEBUG_BUILD
		SCRIPT_ASSERT("[BIKER1] calling SET_SERVER_BIT0 on a non host.")
		PRINTLN("")
		#ENDIF
		EXIT
	ENDIF
	
	SET_BIT(serverBD.iServerBitSet, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_SERVER_BIT0(eSERVER_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF IS_BIT_SET(serverBD.iServerBitSet, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_SERVER_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] CLEAR_SERVER_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	CLEAR_BIT(serverBD.iServerBitSet, ENUM_TO_INT(eBitset))
	
ENDPROC

FUNC BOOL IS_CLIENT_BIT0_SET(PARTICIPANT_INDEX partId, eCLIENT_BITSET_0 eBitset)
	RETURN IS_BIT_SET(playerBD[NATIVE_TO_INT(partId)].iClientBitSet, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_CLIENT_BIT0(eCLIENT_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_CLIENT_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] SET_CLIENT_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_CLIENT_BIT0(eCLIENT_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_CLIENT_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] CLEAR_CLIENT_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, ENUM_TO_INT(eBitset))
	
ENDPROC

FUNC BOOL IS_LOCAL_BIT0_SET(eLOCAL_BITSET_0 eBitset)
	RETURN IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_LOCAL_BIT0(eLOCAL_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_LOCAL_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] SET_LOCAL_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	SET_BIT(iLocalBitset0, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_LOCAL_BIT0(eLOCAL_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_LOCAL_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] CLEAR_LOCAL_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	CLEAR_BIT(iLocalBitset0, ENUM_TO_INT(eBitset))
	
ENDPROC


#IF IS_DEBUG_BUILD

FUNC STRING GET_SERVER_DEBUG_BIT0_NAME(eSERVER_DEBUG_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eSERVERDEBUGBITSET0_TEMP	RETURN "eSERVERDEBUGBITSET0_TEMP"
		CASE eSERVERDEBUGBITSET0_END	RETURN "eSERVERDEBUGBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC

FUNC STRING GET_CLIENT_DEBUG_BIT0_NAME(eCLIENT_DEBUG_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED	RETURN "eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED"
		CASE eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED	RETURN "eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED"
		CASE eCLIENTDEBUGBITSET0_END				RETURN "eCLIENTDEBUGBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC

FUNC STRING GET_LOCAL_DEBUG_BIT0_NAME(eLOCAL_DEBUG_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW	RETURN "eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW"
		CASE eLOCALDEBUGBITSET0_END						RETURN "eLOCALBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC

FUNC BOOL IS_SERVER_DEBUG_BIT0_SET(eSERVER_DEBUG_BITSET_0 eBitset)
	RETURN IS_BIT_SET(serverBD.iDebugBitset, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_SERVER_DEBUG_BIT0(eSERVER_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)
	
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(serverBD.iDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_SERVER_DEBUG_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] GET_SERVER_DEBUG_BIT0_NAME - ", strTemp)
		ENDIF
	ENDIF
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SCRIPT_ASSERT("[BIKER1] calling SET_SERVER_DEBUG_BIT0 on a non host.")
		PRINTLN("[BIKER1] calling SET_SERVER_DEBUG_BIT0 on a non host.")
		EXIT
	ENDIF
	
	SET_BIT(serverBD.iDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_SERVER_DEBUG_BIT0(eCLIENT_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)

	IF NOT bBlockLogPrints
		IF IS_BIT_SET(serverBD.iDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_SERVER_DEBUG_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] CLEAR_SERVER_DEBUG_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	
	CLEAR_BIT(serverBD.iDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC



FUNC BOOL IS_CLIENT_DEBUG_BIT0_SET(PARTICIPANT_INDEX partId, eCLIENT_DEBUG_BITSET_0 eBitset)
	RETURN IS_BIT_SET(playerBD[NATIVE_TO_INT(partId)].iDebugBitset, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_CLIENT_DEBUG_BIT0(eCLIENT_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)

	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_CLIENT_DEBUG_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] SET_CLIENT_DEBUG_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	
	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_CLIENT_DEBUG_BIT0(eCLIENT_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)

	IF NOT bBlockLogPrints
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_CLIENT_DEBUG_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] CLEAR_CLIENT_DEBUG_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	
	CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

FUNC BOOL IS_LOCAL_DEBUG_BIT0_SET(eLOCAL_DEBUG_BITSET_0 eBitset)
	RETURN IS_BIT_SET(iLocalDebugBitset, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_LOCAL_DEBUG_BIT0(eLOCAL_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)

	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(iLocalDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_LOCAL_DEBUG_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] SET_LOCAL_DEBUG_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	
	SET_BIT(iLocalDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_LOCAL_DEBUG_BIT0(eLOCAL_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)

	IF NOT bBlockLogPrints
		IF IS_BIT_SET(iLocalDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_LOCAL_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] CLEAR_LOCAL_DEBUG_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	
	CLEAR_BIT(iLocalDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

#ENDIF

FUNC STRING GET_GAME_STATE_NAME(INT iState)

	SWITCH(iState)
		CASE GAME_STATE_INIT 				RETURN "GAME_STATE_INIT"
		CASE GAME_STATE_RUNNING 			RETURN "GAME_STATE_RUNNING"
		CASE GAME_STATE_TERMINATE_DELAY 	RETURN "GAME_STATE_TERMINATE_DELAY"
		CASE GAME_STATE_END 				RETURN "GAME_STATE_END"
	ENDSWITCH

	RETURN "***INVALID***"

ENDFUNC

//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_GAME_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iClientGameState
ENDFUNC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_GAME_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

//Helper function to set a clients game/mission state
PROC SET_CLIENT_GAME_STATE(INT iPlayer, INT iState)
	playerBD[iPlayer].iClientGameState = iState
	PRINTLN("[BIKER1] SET_CLIENT_GAME_STATE = ",GET_GAME_STATE_NAME(iState))
ENDPROC

//Helper function to set the servers game/mission state
PROC SET_SERVER_GAME_STATE(INT iState)
	serverBD.iServerGameState = iState
	PRINTLN("[BIKER1] SET_SERVER_GAME_STATE = ",GET_GAME_STATE_NAME(iState))
ENDPROC

#IF IS_DEBUG_BUILD 
FUNC STRING GET_MODe_STATE_NAME(eMODE_STATE eState)
	
	SWITCH eState
		CASE eMODESTATE_INIT	RETURN "eMODESTATE_INIT"
		CASE eMODESTATE_RUN		RETURN "eMODESTATE_RUN"
		CASE eMODESTATE_REWARDS	RETURN "eMODESTATE_REWARDS"
		CASE eMODESTATE_END		RETURN "eMODESTATE_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC
#ENDIF

FUNC eMODE_STATE GET_MODE_STATE()
	RETURN serverBD.eModeState
ENDFUNC

PROC SET_MODE_STATE(eMODE_STATE eState)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
	
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 errorMsg = "[BIKER1] - Client set mode state. Client = "
		errorMsg += PARTICIPANT_ID_TO_INT()
		SCRIPT_ASSERT(errorMsg)
		#ENDIF
		
		EXIT
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF eState != GET_MODE_STATE()
		STRING strStateName = GET_MODe_STATE_NAME(eState)
		PRINTLN("[BIKER1] - mode state going to ", strStateName)
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	serverBD.eModeState = eState	
	
ENDPROC

#IF IS_DEBUG_BUILD 
FUNC STRING GET_END_REASON_NAME(eEND_REASON eEndReason)
	
	SWITCH eEndReason
		CASE eENDREASON_NO_REASON_YET						RETURN "eENDREASON_NO_REASON_YET"
		CASE eENDREASON_TIME_UP								RETURN "TIME_UP"
		CASE eENDREASON_NO_BOSS_LEFT						RETURN "NO_BOSS_LEFT"
		CASE eENDREASON_ALL_GOONS_LEFT						RETURN "ALL_GOONS_LEFT"
		CASE eENDREASON_WIN_CONDITION_TRIGGERED				RETURN "eENDREASON_WIN_CONDITION_TRIGGERED"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC
#ENDIF

FUNC eEND_REASON GET_END_REASON()
	RETURN serverBD.eEndReason
ENDFUNC

PROC SET_END_REASON(eEND_REASON eEndReason)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
	
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 errorMsg = "[BIKER1] - Client set end reason. Client = "
		errorMsg += PARTICIPANT_ID_TO_INT()
		SCRIPT_ASSERT(errorMsg)
		#ENDIF
		
		EXIT
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF eEndReason != GET_END_REASON()
		STRING strReason = GET_END_REASON_NAME(eEndReason)
		PRINTLN("[BIKER1] - set end reason to ", strReason)
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	serverBD.eEndReason = eEndReason	
	
ENDPROC

// Local Help Text Bitset ------------------------------------------->|

FUNC BOOL IS_LOCAL_HELPTEXT_BIT0_SET(HELP_TEXT_ENUM eHelp)
	RETURN IS_BIT_SET(iLocalHelpBitset0, ENUM_TO_INT(eHelp))
ENDFUNC

PROC SET_LOCAL_HELPTEXT_BIT0(HELP_TEXT_ENUM eHelp #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(iLocalHelpBitset0, ENUM_TO_INT(eHelp))
			PRINTLN("[BIKER1] === RIPPIN_IT_UP ===  SET_LOCAL_HELPTEXT_BIT0 - ", ENUM_TO_INT(eHelp))
		ENDIF
	ENDIF
	#ENDIF
	
	SET_BIT(iLocalHelpBitset0, ENUM_TO_INT(eHelp))
	
ENDPROC

PROC CLEAR_LOCAL_HELPTEXT_BIT0(HELP_TEXT_ENUM eHelp #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF IS_BIT_SET(iLocalHelpBitset0, ENUM_TO_INT(eHelp))
			PRINTLN("[BIKER1] === RIPPIN_IT_UP ===  CLEAR_LOCAL_HELPTEXT_BIT0 - ", ENUM_TO_INT(eHelp))
		ENDIF
	ENDIF
	#ENDIF
	
	CLEAR_BIT(iLocalHelpBitset0, ENUM_TO_INT(eHelp))
	
ENDPROC

// ------------------------------------------------------------------|<

// Local Big Message Bitset ------------------------------------------->|

FUNC BOOL IS_LOCAL_BIGMESSAGE_BIT0_SET(BIG_MESSAGE_ENUM eBigM)
	RETURN IS_BIT_SET(iLocalBigMessageBitset0, ENUM_TO_INT(eBigM))
ENDFUNC

PROC SET_LOCAL_BIGMESSAGE_BIT0(BIG_MESSAGE_ENUM eBigM #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(iLocalBigMessageBitset0, ENUM_TO_INT(eBigM))
			PRINTLN("[BIKER1] === RIPPIN_IT_UP ===  SET_LOCAL_BIGMESSAGE_BIT0 - ", ENUM_TO_INT(eBigM))
		ENDIF
	ENDIF
	#ENDIF
	
	SET_BIT(iLocalBigMessageBitset0, ENUM_TO_INT(eBigM))
	
ENDPROC

PROC CLEAR_LOCAL_BIGMESSAGE_BIT0(BIG_MESSAGE_ENUM eBigM #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF IS_BIT_SET(iLocalBigMessageBitset0, ENUM_TO_INT(eBigM))
			PRINTLN("[BIKER1] === RIPPIN_IT_UP ===  CLEAR_LOCAL_BIGMESSAGE_BIT0 - ", ENUM_TO_INT(eBigM))
		ENDIF
	ENDIF
	#ENDIF
	
	CLEAR_BIT(iLocalBigMessageBitset0, ENUM_TO_INT(eBigM))
	
ENDPROC

// ------------------------------------------------------------------|<

#IF IS_DEBUG_BUILD
PROC MAINTAIN_J_SKIPS()
	
	IF GET_SERVER_GAME_STATE() = GAME_STATE_RUNNING
		IF NOT IS_CLIENT_DEBUG_BIT0_SET(PARTICIPANT_ID(), eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
			IF IS_DEBUG_KEY_JUST_RELEASED(KEY_S, KEYBOARD_MODIFIER_NONE, "S Pass")
				PRINTLN("[BIKER1] KEY_S, KEYBOARD_MODIFIER_NONE just released")
				SET_CLIENT_DEBUG_BIT0(eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
			ENDIF
		ENDIF
		
		IF NOT IS_CLIENT_DEBUG_BIT0_SET(PARTICIPANT_ID(), eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
			IF IS_DEBUG_KEY_JUST_RELEASED(KEY_F, KEYBOARD_MODIFIER_NONE, "F Fail")
				PRINTLN("[BIKER1] KEY_F, KEYBOARD_MODIFIER_NONE just released")
				SET_CLIENT_DEBUG_BIT0(eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED)
			ENDIF
		ENDIF
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF serverBD.iSPassPart > (-1)
				PRINTLN("[BIKER1] serverBD.iSPassPart > (-1)")
				IF GET_MODE_STATE() < eMODESTATE_REWARDS
					SET_END_REASON(eENDREASON_TIME_UP)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ELIF serverBD.iFFailPart > (-1)
				PRINTLN("[BIKER1] serverBD.iFFailPart > (-1)")
				IF GET_MODE_STATE() < eMODESTATE_REWARDS
					SET_END_REASON(eENDREASON_TIME_UP)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////// MAIN SCRIPT FUNCITONS				///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Tunables

/// PURPOSE:
///		1000 - Cash reward default.
FUNC INT GET_EOM_DEFAULT_CASH_REWARD()
	RETURN g_sMPTunables.iCRIMINAL_DAMAGE_GET_EOM_DEFAULT_CASH_REWARD
ENDFUNC

/// PURPOSE:
///		100 - RP reward default.
FUNC INT GET_EOM_DEFAULT_RP_REWARD()
	RETURN g_sMPTunables.iCRIMINAL_DAMAGE_GET_EOM_DEFAULT_RP_REWARD
ENDFUNC

/// PURPOSE:
/// 	3000 - Cash reward base.
FUNC INT GET_CASH_REWARD_BASE()
	RETURN g_sMPTunables.iBIKER_RIPPIN_BASE_CASH                                               
ENDFUNC

/// PURPOSE:
/// 	200 - RP reward base.
FUNC INT GET_RP_REWARD_BASE()
	RETURN g_sMPTunables.iBIKER_RIPPIN_BASE_RP                                                 
ENDFUNC

/// PURPOSE:
/// 	0.01 - Cash reward percentage.
FUNC FLOAT GET_CASH_REWARD_PERCENT()
	RETURN g_sMPTunables.fBIKER_RIPPIN_PERCENT_CASH                                            
ENDFUNC

/// PURPOSE:
/// 	0.001 - RP reward percentage.
FUNC FLOAT GET_RP_REWARD_PERCENT()
	RETURN g_sMPTunables.fBIKER_RIPPIN_PERCENT_RP                                              
ENDFUNC

/// PURPOSE:
/// 	1000 - Cash reward bonus (winner).
FUNC INT GET_CASH_REWARD_BONUS()
	RETURN g_sMPTunables.iBIKER_RIPPIN_BONUS_CASH                                              
ENDFUNC

/// PURPOSE:
/// 	5000 - RP reward bonus (winner).
FUNC INT GET_RP_REWARD_BONUS()
	RETURN g_sMPTunables.iBIKER_RIPPIN_BONUS_RP                                                
ENDFUNC

/// PURPOSE:
///    2,000,000 - Cap for score.
FUNC INT GET_REWARD_SCORE_CAP()
	RETURN g_sMPTunables.iBIKER_RIPPIN_SCORE_CAP                                               
ENDFUNC

/// PURPOSE:
/// 	10,000 - Player score threshold before qualifying for reward.
FUNC INT GET_REWARD_PLAYER_THRESHOLD()
	RETURN g_sMPTunables.iCRIMINAL_DAMAGE_GET_REWARD_PLAYER_THRESHOLD // TODO: Replace with tunable.
ENDFUNC

/// PURPOSE:
///    1.0 - Modifier for VEHICLE value.
FUNC FLOAT GET_VALUE_MODIFIER_FOR_VEHICLES()
	RETURN 1.0 //Replace with tunable
ENDFUNC

/// PURPOSE:
///    1.0 - Modifier for CLOTHING value.
FUNC FLOAT GET_VALUE_MODIFIER_FOR_CLOTHING()
	RETURN 0.1 //Replace with tunable
ENDFUNC


// Helper functions

FUNC PLAYER_INDEX GET_LAUNCH_BOSS_PLAYER_INDEX()
	RETURN serverBD.piLaunchBoss
ENDFUNC

FUNC BOOL IS_PLAYER_THE_LAUNCH_BOSS(PLAYER_INDEX playerID)
	RETURN (GET_LAUNCH_BOSS_PLAYER_INDEX() = playerID)
ENDFUNC

FUNC BOOL AM_I_THE_LAUNCH_BOSS()
	RETURN IS_PLAYER_THE_LAUNCH_BOSS(PLAYER_ID())
ENDFUNC

FUNC BOOL IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_INDEX playerID)
	RETURN GB_IS_PLAYER_MEMBER_OF_THIS_BIKER_GANG(playerID, GET_LAUNCH_BOSS_PLAYER_INDEX(), FALSE)
ENDFUNC

FUNC BOOL AM_I_GOON_IN_LAUNCH_GANG()
	RETURN IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
ENDFUNC

FUNC BOOL AM_I_IN_LAUNCH_GANG()
	IF AM_I_THE_LAUNCH_BOSS()
	OR AM_I_GOON_IN_LAUNCH_GANG()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SORT_PLAYER_SCORES(PLAYER_INDEX playerID, INT iPlayerScore)
	SORT_AMBIENT_ACTIVITY_LEADERBOARD(serverBD.sSortedLeaderboard, playerID, iPlayerScore, AAL_SORT_NON_UNIFORM_SCORING)
	
	//serverBD.iServerSyncID++
ENDPROC

PROC SET_PARTICIPANT_SCORE(INT iScore)

	IF playerBD[PARTICIPANT_ID_TO_INT()].iScore != iScore
		playerBD[PARTICIPANT_ID_TO_INT()].iScore = iScore
		g_sBIK_Telemetry_data.m_PropertyDamageValue = iScore
		PRINTLN("[BIKER1] === RIPPIN_IT_UP === SET_PLAYER_SCORE = ",iScore)
	ENDIF

ENDPROC

FUNC INT GET_PARTICIPANT_SCORE(INT iPart)

	IF iPart != -1
		RETURN playerBD[iPart].iScore
	ENDIF
	
	RETURN -1

ENDFUNC

PROC MAINTAIN_DPAD_LEADERBOARD()

	g_GBLeaderboardStruct.dpadVariables.eDpadVarType = DPAD_VAR_CASH

	INT i
	REPEAT GB_MAX_GANG_SIZE_INCLUDING_BOSS i
		g_GBLeaderboardStruct.challengeLbdStruct[i].playerID = serverBD.sSortedLeaderboard[i].playerID
		g_GBLeaderboardStruct.challengeLbdStruct[i].iScore = serverBD.sSortedLeaderboard[i].iScore
		g_GBLeaderboardStruct.challengeLbdStruct[i].iFinishTime = -1
	ENDREPEAT
	
	DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, 
						g_GBLeaderboardStruct.fmDpadStruct)

ENDPROC

// Help text

/// PURPOSE:
///    Check if its safe to display help text on screen
/// RETURNS:
///    
FUNC BOOL IS_SAFE_FOR_HELP_TEXT()
	IF NOT IS_CUSTOM_MENU_ON_SCREEN()
	AND NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_RADAR_HIDDEN()
	AND IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Display help text to the screen
/// PARAMS:
///    thisHelpText - 
PROC DO_HELP_TEXT(HELP_TEXT_ENUM thisHelpText)
	IF IS_SAFE_FOR_HELP_TEXT()
		IF NOT IS_LOCAL_HELPTEXT_BIT0_SET(thisHelpText)
		
			SWITCH thisHelpText
				CASE eHELPTEXT_INTRO
					PRINT_HELP_NO_SOUND("AMBC_HELP")
				BREAK
				
				CASE eHELPTEXT_INTRO2
					PRINT_HELP_NO_SOUND("AMBC_HELP1")
				BREAK
							
				DEFAULT
				
				BREAK
			ENDSWITCH
			
			GB_SET_GANG_BOSS_HELP_BACKGROUND()
			SET_LOCAL_HELPTEXT_BIT0(thisHelpText)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_SAFE_FOR_BIG_MESSAGE()	

	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_CUSTOM_MENU_ON_SCREEN()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Prints information about the big message that has just been requested to draw
/// PARAMS:
///    iBigMessage - the index of the message that was drawn
///    title - the title of the message
///    strapline - the subtitle of the message
PROC PRINT_BIG_MESSAGE_DEBUG_INFORMATION(INT iBigMessage, STRING title, STRING strapline)
	PRINTLN("[BIKER1] === RIPPIN_IT_UP === - ************ NEW SHARD HAS BEEN SETUP *************")
	PRINTLN("[BIKER1] === RIPPIN_IT_UP === - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Big Message:	", iBigMessage)
	PRINTLN("[BIKER1] === RIPPIN_IT_UP === - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Title: 			", GET_FILENAME_FOR_AUDIO_CONVERSATION(title))
	PRINTLN("[BIKER1] === RIPPIN_IT_UP === - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Strapline:		", GET_FILENAME_FOR_AUDIO_CONVERSATION(strapline))
	PRINTLN("[BIKER1] === RIPPIN_IT_UP === - ***************************************************")
ENDPROC
#ENDIF

/// PURPOSE:
///    Function that handles the drawing of the modes shards
/// PARAMS:
///    thisMessage - 
PROC DO_BIG_MESSAGE(BIG_MESSAGE_ENUM thisMessage)
	IF IS_SAFE_FOR_BIG_MESSAGE()
		IF NOT IS_LOCAL_BIGMESSAGE_BIT0_SET(thisMessage)
					
			SWITCH thisMessage
				CASE eBIGMESSAGE_START
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB,"RIU_SH_BIG","RIU_SH_OBJ")	
				BREAK
				
				CASE eBIGMESSAGE_WIN
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS,GET_PARTICIPANT_SCORE(PARTICIPANT_ID_TO_INT()),"RIU_SH_WIN","GB_WINNER")
				BREAK
				
				CASE eBIGMESSAGE_LOSE
					SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_OVER, serverBD.sSortedLeaderboard[0].playerID, serverBD.sSortedLeaderboard[0].iScore, "RIU_SH_LOSE", "GB_CHAL_OVER")
				BREAK
				
				CASE eBIGMESSAGE_GOONS_LEFT
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_GENERIC, "GB_CHAL_OVER", "RIU_SH_GLEFT")
				BREAK
					
				DEFAULT
					
				BREAK
			ENDSWITCH
			
			SET_LOCAL_BIGMESSAGE_BIT0(thisMessage)
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_END_REASON_AS_TELEMTRY_TYPE(eEND_REASON eEndReason, BOOL bIWon, BOOL bNobodyWon, BOOL bForcedEndOfMode)
	
	IF bForcedEndOfMode
		PRINTLN("[BIKER1] - GET_END_REASON_AS_TELEMTRY_TYPE - bExternallyForcedEndOfMode = TRUE, returning GB_TELEMETRY_END_FORCED")
		RETURN GB_TELEMETRY_END_FORCED
	ENDIF
	
	SWITCH eEndReason
		
		CASE eENDREASON_NO_BOSS_LEFT
			PRINTLN("[BIKER1] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_NO_BOSS_LEFT, returning GB_TELEMETRY_END_BOSS_LEFT")
			RETURN GB_TELEMETRY_END_BOSS_LEFT	
		
		CASE eENDREASON_ALL_GOONS_LEFT	
			PRINTLN("[BIKER1] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_ALL_GOONS_LEFT, returning GB_TELEMETRY_END_LOW_NUMBERS")
			RETURN GB_TELEMETRY_END_LOW_NUMBERS
		
		CASE eENDREASON_TIME_UP
		CASE eENDREASON_WIN_CONDITION_TRIGGERED
			IF bIWon	
				PRINTLN("[BIKER1] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_TIME_UP and bIWon = TRUE, returning GB_TELEMETRY_END_WON")
				RETURN GB_TELEMETRY_END_WON		
			ELIF bNobodyWon
				PRINTLN("[BIKER1] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_TIME_UP and bNobodyWon = TRUE, returning GB_TELEMETRY_END_TIME_OUT")
				RETURN GB_TELEMETRY_END_TIME_OUT	
			ELSE
				PRINTLN("[BIKER1] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_TIME_UP and bWon = FALSE and bNobodyWon = FALSE, returning GB_TELEMETRY_END_LOST")
				RETURN GB_TELEMETRY_END_LOST
			ENDIF
		BREAK
		
	ENDSWITCH
	
	PRINTLN("[BIKER1] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - all other telemetry type checks failed, must be leaving of own choice, returning GB_TELEMETRY_END_LEFT")
	RETURN GB_TELEMETRY_END_LEFT
	
ENDFUNC

PROC EXECUTE_COMMON_END_TELEMTRY(BOOL bIwon, BOOL bForcedEndOfMode)
	
	IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_EXECUTED_END_TELEMTRY)
	
		g_sBIK_Telemetry_data.m_TimeTakenForObjective = GET_MODE_TUNEABLE_TIME_LIMIT()
		
		BOOL bNobodyWon // Set based on mode specific logic.
		INT iEndType = GET_END_REASON_AS_TELEMTRY_TYPE(GET_END_REASON(), bIWon, bNobodyWon, bForcedEndOfMode)
		
		g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchId1
		g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchId2
		
		PRINTLN("[BIKER1] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - bForcedEndOfMode = ", bForcedEndOfMode)
		PRINTLN("[BIKER1] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - bIWon = ", bIWon)
		PRINTLN("[BIKER1] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - bNobodyWon = ", bNobodyWon)
		PRINTLN("[BIKER1] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - iEndType = ", iEndType)
		
		GB_SET_COMMON_TELEMETRY_DATA_ON_END(bIWon, iEndType)
		PRINTLN("[BIKER1] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - called GB_SET_COMMON_TELEMETRY_DATA_ON_END(", bIWon,", ", iEndType, ")")
		
		SET_LOCAL_BIT0(eLOCALBITSET0_EXECUTED_END_TELEMTRY)
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Calculate RP and GTA$ rewards for completing this activity.
/// PARAMS:
///    fPositionMod - Position modifier.
///    iCash - ByREF GTA$ reward.
///    iRP - ByREF RP reward.
PROC CALCULATE_EOM_REWARDS(INT& iCash, INT& iRP)
	INT iPartCash = 0
	
	iCash = 0
	iRP = 0

	//
	// Latest calculations are based on url:bugstar:2435074 - Update the reward calculation for Criminal Damage.
	//
	INT iScore = playerBD[PARTICIPANT_ID_TO_INT()].iScore
	
	IF iScore >= GET_REWARD_PLAYER_THRESHOLD()
	
		// New formula = (Base + (Score(max S_Cap) x Percent))) + Bonus(winner only)
		FLOAT fCashPercent = GET_CASH_REWARD_PERCENT()
		FLOAT fRPPercent = GET_RP_REWARD_PERCENT()
		
		INT iCashBase = GET_CASH_REWARD_BASE()
		INT iRPBase = GET_RP_REWARD_BASE()
		
		INT iCashBonus = 0
		INT iRPBonus = 0
		INT iPartRP = 0
		
		// Winner bonus
		IF serverBD.sSortedLeaderboard[0].playerID = PLAYER_ID()
			iCashBonus = GET_CASH_REWARD_BONUS()
			iRPBonus = GET_RP_REWARD_BONUS()
		ELSE
			
		ENDIF
		
		// Score cap
		IF iScore > GET_REWARD_SCORE_CAP()
			iScore = GET_REWARD_SCORE_CAP()
		ENDIF
		INT iParticipationTime = 10//FLOOR((TO_FLOAT(serverBD.iEndTime - iStartTime) / 60) / 1000)
//		IF iParticipationTime > g_sMPTunables.iParticipation_T_Cap
//			iParticipationTime = g_sMPTunables.iParticipation_T_Cap
//		ELIF iParticipationTime < 1
//			iParticipationTime = 1
//		ENDIF

		iPartCash = GET_EOM_DEFAULT_CASH_REWARD() * iParticipationTime
		iPartRP = GET_EOM_DEFAULT_RP_REWARD() * iParticipationTime
		
		CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_EOM_REWARDS - Participation Cash: ", iPartCash)
		
		iCash = ROUND_TO_NEAREST(ROUND((((iCashBase + FLOOR(iScore * fCashPercent)) + iCashBonus) * g_sMPTunables.fCriminal_Damage_Event_Multiplier_Cash)), 50)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_EOM_REWARDS - iCash: ((", iCashBase, " + (", iScore, " * ", fCashPercent, ")) + ", iCashBonus, ") = ", iCash, " - rounded to nearest 50")
		
		iRP = ROUND_TO_NEAREST(ROUND((((iRPBase + FLOOR(iScore * fRPPercent)) + iRPBonus) * g_sMPTunables.fCriminal_Damage_Event_Multiplier_RP) + iPartRP), 50)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_EOM_REWARDS - iRP: ((", iRPBase, " + (", iScore, " * ", fRPPercent, ")) + ", iRPBonus, " + ", iPartRP, ") = ", iRP, " - rounded to nearest 50")
		
	ELSE
		CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_EOM_REWARDS - Player score not high enough for participant reward! Value: ", iScore)
	ENDIF
	
	iCash += iPartCash
	CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_EOM_REWARDS - Cash after Boss Cut with Participation Added: ", iCash)
ENDPROC

PROC HANDLE_REWARDS()
	
	BOOL bIWon
	
	IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALCULATED_REWARDS)
		
		IF GET_END_REASON() != eENDREASON_NO_REASON_YET
		
			IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
				
				IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
				AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
				AND NOT GB_SHOULD_HIDE_GANG_BOSS_EVENT(FMMC_TYPE_BIKER_RIPPIN_IT_UP)
				
					IF IS_SAFE_FOR_BIG_MESSAGE()
						
						SWITCH GET_END_REASON()
							CASE eENDREASON_NO_BOSS_LEFT
								PRINTLN("[BIKER1] - no boss left on script.")
								CLEAR_HELP()
								// Display end shard here. 
							BREAK
							CASE eENDREASON_ALL_GOONS_LEFT
								PRINTLN("[BIKER1] - no goons left on script")
								CLEAR_HELP()
								DO_BIG_MESSAGE(eBIGMESSAGE_GOONS_LEFT)
							BREAK
							CASE eENDREASON_TIME_UP
								PRINTLN("[BIKER1] - mode time expired")
								CLEAR_HELP()
								// Display end shard here
								IF serverBD.sSortedLeaderboard[0].playerID = PLAYER_ID()
									DO_BIG_MESSAGE(eBIGMESSAGE_WIN)
									bIWon = TRUE
								ELSE
									DO_BIG_MESSAGE(eBIGMESSAGE_LOSE)
								ENDIF
								
							BREAK
							CASE eENDREASON_WIN_CONDITION_TRIGGERED
								PRINTLN("[BIKER1] - somebody won")
								CLEAR_HELP()
								
							BREAK
						ENDSWITCH
						
					ENDIF
					
				ENDIF
				
			ENDIF
			
			PLAYER_INDEX playerId = PLAYER_ID() // Set the winning player here before passing into wanted cleanup function. Using PLAYER_ID() as placeholder right now.
			GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, playerId)
			
			IF bIWon
				// Do any extra processing here for if the locla player won, before passing into GANG_BOSS_MANAGE_REWARDS
				// sGangBossManageRewardsData etc.
				
				
				
			ENDIF
			
			CALCULATE_EOM_REWARDS(sGangBossManageRewardsData.iExtraCash, sGangBossManageRewardsData.iExtraRp)
			
			GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_BIKER_RIPPIN_IT_UP, bIWon, sGangBossManageRewardsData)
			
			EXECUTE_COMMON_END_TELEMTRY(bIWon, FALSE)
			
			SET_LOCAL_BIT0(eLOCALBITSET0_CALCULATED_REWARDS)
			
		ENDIF
	
	ENDIF
	
	IF GB_MAINTAIN_BOSS_END_UI(sEndUI)
		SET_CLIENT_BIT0(eCLIENTBITSET0_COMPLETED_REWARDS)
	ENDIF
	
ENDPROC

// UI Maintains ----------------------------------------------------->|

/// PURPOSE:
///    Maintain the blips on entities and vehicles
PROC MAINTAIN_BLIPS()

ENDPROC

//Draws a marker above a vehicle so the arrow doesn't clip into it
PROC DRAW_MARKER_ABOVE_ENTITY(ENTITY_INDEX ent, INT r, INT g, INT b)
	VECTOR returnMin, returnMax
	FLOAT fOffset
	
	fOffset = 0.5 
	
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(ent), returnMin, returnMax)

	FLOAT fCentreHeight = (returnMax.z - returnMin.z)/2
	FLOAT fZdiff = returnMax.z - fCentreHeight
	
	//-- Dave W - z-offset from centre needs to be at least the max height of the vehicle.
	IF fOffset <= (fZdiff + 0.1)
		fOffset = fZdiff + 0.4
	ENDIF
	
	DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(ent)+<<0,0,((returnMax.z - returnMin.z)/2) + fOffset>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, r, g, b, 100, TRUE, TRUE)
ENDPROC

PROC MAINTAIN_MARKERS()
	
ENDPROC

/// PURPOSE:
///    Maintain the objective text for the mode
PROC MAINTAIN_OBJECTIVE_TEXT()
		
	IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(),eCLIENTBITSET0_ON_BIKE)
		Print_Objective_Text("RIU_OBJ_OBJ")
	ELSE
		Print_Objective_Text("RIU_OBJ_GETON")
	ENDIF

ENDPROC

/// PURPOSE:
///    Maintain the bottom right UI (event timers, kill/cash counts etc.)
PROC MAINTAIN_BOTTOM_RIGHT_UI()
	
	
	// Handle timers, counters etc
	IF HAS_NET_TIMER_STARTED(serverBD.modeTimer)
			
		iTimeRemaining = (GET_MODE_TUNEABLE_TIME_LIMIT() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.modeTimer))
		
		IF iTimeRemaining < GET_MODE_TUNEABLE_TIME_LIMIT()
		
			HUD_COLOURS timeColour
			IF iTimeRemaining > 30000
				timeColour = HUD_COLOUR_WHITE
			ELSE
				timeColour = HUD_COLOUR_RED
			ENDIF
			
			SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
			
			BOOL bForce
						
			IF iTimeRemaining < 0
				iTimeRemaining = 0
			ENDIF
			
			PRINTLN("[BIKER1] === RIPPIN_IT_UP === MAINTAIN_BOTTOM_RIGHT_UI - Attempting to display bottom right leaderboard")
			
			
			BOTTOM_RIGHT_UI_1STTIME_2NDTIME_3RDTIME_DAMAGE_TIMER(	serverBD.sSortedLeaderboard[0].playerID,
																 	serverBD.sSortedLeaderboard[1].playerID,
																	serverBD.sSortedLeaderboard[2].playerID,
																	serverBD.sSortedLeaderboard[0].iScore,
																	serverBD.sSortedLeaderboard[1].iScore,
																	serverBD.sSortedLeaderboard[2].iScore,
																	GET_PARTICIPANT_SCORE(PARTICIPANT_ID_TO_INT()),
																	iTimeRemaining,
																	bForce,
																	timeColour, 
																	"",
																	DEFAULT,DEFAULT,DEFAULT)
		
			
		ENDIF
	ENDIF	
ENDPROC

/// PURPOSE:
///    Maintain the UI of the boss work
PROC MAINTAIN_UI()

	IF AM_I_IN_LAUNCH_GANG()
		MAINTAIN_BLIPS()
		MAINTAIN_MARKERS()
		MAINTAIN_OBJECTIVE_TEXT()
		MAINTAIN_BOTTOM_RIGHT_UI()
	ELSE
		SWITCH GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())
			CASE GB_UI_LEVEL_NONE
				CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
			BREAK
			
			CASE GB_UI_LEVEL_BACKGROUND
			CASE GB_UI_LEVEL_MINIMAL
				MAINTAIN_BLIPS()
			BREAK
			
			CASE GB_UI_LEVEL_FULL
				MAINTAIN_BLIPS()
				MAINTAIN_MARKERS()
				MAINTAIN_OBJECTIVE_TEXT()
				MAINTAIN_BOTTOM_RIGHT_UI()
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC TRIGGER_MISSION_MUSIC(STRING sMusic)

	IF ARE_STRINGS_EQUAL(sMusic, "BKR_RIPPIN_IT_UP_START")
		SET_AUDIO_FLAG("DisableFlightMusic", TRUE)
	ENDIF

	IF NOT ARE_STRINGS_EQUAL(sMusic,currentMusic)
		TRIGGER_MUSIC_EVENT(sMusic)
		currentMusic = sMusic
		PRINTLN("[BIKER1] === RIPPIN_IT_UP === TRIGGER_MISSION_MUSIC - ",sMusic)
	ENDIF

ENDPROC

FUNC BOOL IS_PLAYER_ON_SUITABLE_BIKE(PLAYER_INDEX playerID)

	IF IS_NET_PLAYER_OK(playerID)
		PED_INDEX pedID = GET_PLAYER_PED(playerID)

		IF IS_PED_IN_ANY_VEHICLE(pedID)
			VEHICLE_INDEX vehID = GET_VEHICLE_PED_IS_IN(pedID)
			IF IS_VEHICLE_DRIVEABLE(vehID)
				MODEL_NAMES model = GET_ENTITY_MODEL(vehId)
				IF (IS_THIS_MODEL_A_BIKE(model) AND NOT IS_THIS_MODEL_A_BICYCLE(model))
				OR model = CHIMERA
				OR model = BLAZER4
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC MAINTAIN_PLAYER_ON_BIKE_CHECK()
	
	IF IS_PLAYER_ON_SUITABLE_BIKE(PLAYER_ID())
		SET_CLIENT_BIT0(eCLIENTBITSET0_ON_BIKE)		
	ELSE
		CLEAR_CLIENT_BIT0(eCLIENTBITSET0_ON_BIKE)
	ENDIF
	
ENDPROC

PROC CLIENT_PROCESSING()
		
	SWITCH GET_MODE_STATE()
	
		CASE eMODESTATE_INIT
			// ********************************************************************
			// Server says when omode has intialised and moves us onto run state.
			// ********************************************************************
		BREAK
		
		CASE eMODESTATE_RUN
			
			// ***********************
			// Do common start calls
			// ***********************
			
			IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALLED_COMMON_SETUP)
				GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_BIKER_RIPPIN_IT_UP)
				SET_LOCAL_BIT0(eLOCALBITSET0_CALLED_COMMON_SETUP)
			ELSE
				IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP)
					IF HAS_NET_TIMER_EXPIRED(serverBD.modeTimer, TRACK_GOON_DELAY)
						GB_SET_ITS_SAFE_FOR_SPEC_CAM()
						SET_LOCAL_BIT0(eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP)
					ENDIF
				ENDIF
			ENDIF
			
			// *******************************************
			// Main mode logic here, after setup is done
			// *******************************************
			
			IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			AND NOT GB_SHOULD_HIDE_GANG_BOSS_EVENT(FMMC_TYPE_BIKER_RIPPIN_IT_UP)
				
				IF GET_END_REASON() = eENDREASON_NO_REASON_YET
					
					IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
						
						IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALLED_START_COMMON_TELEMETRY)
							GB_SET_COMMON_TELEMETRY_DATA_ON_START()
							DO_BIG_MESSAGE(eBIGMESSAGE_START)
							TRIGGER_MISSION_MUSIC("BKR_RIPPIN_IT_UP_START")
							SET_LOCAL_BIT0(eLOCALBITSET0_CALLED_START_COMMON_TELEMETRY)
							SET_PLAYER_RESPAWN_IN_VEHICLE(TRUE, RATBIKE)
							PRINTLN("[BIKER1] - [TELEMETRY] - called GB_SET_COMMON_TELEMETRY_DATA_ON_START()")
						ENDIF
						
						// ********************************************
						// Run main mode logic here.
						// Remember to set the player as 
						// GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT() 
						// when appropriate.
						// ********************************************

						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							IF NOT IS_LOCAL_HELPTEXT_BIT0_SET(eHELPTEXT_INTRO)
								DO_HELP_TEXT(eHELPTEXT_INTRO)
							ELIF NOT IS_LOCAL_HELPTEXT_BIT0_SET(eHELPTEXT_INTRO2)
								DO_HELP_TEXT(eHELPTEXT_INTRO2)
							ENDIF
						ENDIF
						
						MAINTAIN_UI()
						MAINTAIN_PLAYER_ON_BIKE_CHECK()
						
						IF GET_PARTICIPANT_SCORE(PARTICIPANT_ID_TO_INT()) > 0
							GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
						ENDIF
						
						IF iFocusParticipant > (-1)
							IF iFocusParticipant = PARTICIPANT_ID_TO_INT()
								// I am playing, not spectating.
							ELSE
								// I am spectating not playing.
							ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
				
			ELSE
			
				Clear_Any_Objective_Text_From_This_Script() // Clear objective text if player is retricted for hide or passive.
				
			ENDIF
			
		BREAK
		
		CASE eMODESTATE_REWARDS
			
			TRIGGER_MISSION_MUSIC("BKR_RIPPIN_IT_UP_STOP")
			Clear_Any_Objective_Text_From_This_Script() // Clear god text, mode is over.
			HANDLE_REWARDS() // Handle end of mode rewards.
			
			// Server will move us onto end state once all end of mode logic is complete.
			
		BREAK
		
		CASE eMODESTATE_END
			
		BREAK
		
	ENDSWITCH
	
	MAINTAIN_DPAD_LEADERBOARD()
	GB_MAINTAIN_COUNTDOWN_MUSIC(iTimeRemaining,GET_END_REASON() != eENDREASON_NO_REASON_YET,cmStruct)

ENDPROC

/// PURPOSE:
///    Performs client initialisation - this will wait for the server to be initialised
/// RETURNS:
///    
FUNC BOOL INIT_CLIENT()
	
	IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_CLIENT_INITIALISED)
		IF IS_SERVER_BIT0_SET(eSERVERBITSET0_SERVER_INITIALISED)
			
			// Do any client initialisation here
			
			SET_CLIENT_BIT0(eCLIENTBITSET0_CLIENT_INITIALISED)
		ENDIF
	ENDIF


	RETURN IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_CLIENT_INITIALISED)
ENDFUNC

PROC SERVER_PROCESSING()
	
	SWITCH GET_MODE_STATE()
	
		CASE eMODESTATE_INIT
			
			// ********************************************
			// Initialise server data here.
			// Will remain in here until server moves on.
			// ********************************************
			
			// Move onto next stage.
			SET_MODE_STATE(eMODESTATE_RUN)
			
			// Get MatchIds for Telemetry
			PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchId1, serverBD.iMatchId2)
			
		BREAK
		
		CASE eMODESTATE_RUN
			
			// Stay in run state until we have an end reason.
			IF GET_END_REASON() = eENDREASON_NO_REASON_YET
				
				// Widget for forcing time up at end of mode.
				#IF IS_DEBUG_BUILD
				IF serverBd.bFakeEndOfModeTimer
					PRINTLN("[BIKER1] serverBd.bFakeEndOfModeTimer = TRUE.")
					SET_END_REASON(eENDREASON_TIME_UP)
				ENDIF
				#ENDIF
				
				// If the mode timer expires, set end reason to time up.
				IF NOT HAS_NET_TIMER_STARTED(serverBd.modeTimer)
					START_NET_TIMER(serverBd.modeTimer)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(serverBd.modeTimer, GET_MODE_TUNEABLE_TIME_LIMIT())
						SET_END_REASON(eENDREASON_TIME_UP)
					ENDIF
				ENDIF
			ELSE
				
				// Move onto rewards once we have an end reason (therefore mode has ended). 
				SET_MODE_STATE(eMODESTATE_REWARDS)
				serverBD.iEndTime = NATIVE_TO_INT(GET_NETWORK_TIME())
			ENDIF
			
		BREAK
		
		CASE eMODESTATE_REWARDS
			
			// Once all clients have handled the end of the mode, allow the server to move to the end. 
			IF IS_SERVER_BIT0_SET(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
				PRINTLN("[BIKER1] - [REWARDS] - all participants completed rewards.")
				SET_MODE_STATE(eMODESTATE_END)
			ENDIF
			
		BREAK
		
		CASE eMODESTATE_END
			// Do anything required at very end of mode once rewards and gameplay are opver, then end.
			SET_SERVER_GAME_STATE(GAME_STATE_END)
		BREAK
		
	ENDSWITCH
	
	MAINTAIN_PLAYER_LEAVING_AMBIENT_ACTIVITY_LEADERBOARD(serverBD.sSortedLeaderboard, serverStaggeredPlayerLoop, AAL_SORT_NON_UNIFORM_SCORING)

ENDPROC

/// PURPOSE:
///    Performs server initialisation
/// RETURNS:
///    
FUNC BOOL INIT_SERVER()
	
	IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_SERVER_INITIALISED)
		serverBD.piLaunchBoss = PLAYER_ID()
		
		// Initialise the leaderboard
		INITIALISE_AMBIENT_ACTIVITY_LEADERBOARD(serverBD.sSortedLeaderboard)
		
		SORT_PLAYER_SCORES(PLAYER_ID(), 0)
		
		SET_SERVER_BIT0(eSERVERBITSET0_SERVER_INITIALISED)
	ENDIF

	RETURN IS_SERVER_BIT0_SET(eSERVERBITSET0_SERVER_INITIALISED)
ENDFUNC

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------


PROC SCRIPT_CLEANUP()

	PRINTLN("[BIKER1] SCRIPT_CLEANUP")
	
	SET_PLAYER_RESPAWN_IN_VEHICLE(FALSE) 
	
	IF GET_END_REASON() = eENDREASON_NO_REASON_YET
		TRIGGER_MISSION_MUSIC("BKR_RIPPIN_IT_UP_FAIL")
	ENDIF
	
	PROCESS_CURRENT_BOSS_CHALLENGE_PLAYSTATS(g_sGb_Telemetry_data.sdata)
	PRINTLN("[BIKER1] - [TELEMETRY] - called PROCESS_CURRENT_BOSS_CHALLENGE_PLAYSTATS")
	
	GB_STOP_COUNTDOWN_MUSIC(cmStruct)
	
	SET_AUDIO_FLAG("DisableFlightMusic", FALSE)
	
	GB_COMMON_BOSS_MISSION_CLEANUP()	
	
	Clear_Any_Objective_Text_From_This_Script()
	
	GB_TIDYUP_SPECTATOR_CAM()
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)

	PRINTLN("[BIKER1] PROCESS_PRE_GAME")
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GB_MAX_GANG_SIZE_INCLUDING_BOSS, missionScriptArgs)
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(GB_GET_BOSS_MISSION_NUM_OBJ_REQUIRED(FMMC_TYPE_BIKER_RIPPIN_IT_UP))
	RESERVE_NETWORK_MISSION_PEDS(GB_GET_BOSS_MISSION_NUM_PEDS_REQUIRED(FMMC_TYPE_BIKER_RIPPIN_IT_UP))
	RESERVE_NETWORK_MISSION_VEHICLES(GB_GET_BOSS_MISSION_NUM_VEH_REQUIRED(FMMC_TYPE_BIKER_RIPPIN_IT_UP))
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD,SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	GB_COMMON_BOSS_MISSION_PREGAME()
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_VEHICLE_YACHT_VEHICLE(VEHICLE_INDEX victimVeh)

	IF DECOR_IS_REGISTERED_AS_TYPE("PYV_Owner",DECOR_TYPE_INT)
		IF DECOR_EXIST_ON(victimVeh,"PYV_Owner")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC CALCULATE_PRICE_OF_DAMAGE(INT iCount)

	STRUCT_ENTITY_DAMAGE_EVENT sei 
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(sei))

		IF DOES_ENTITY_EXIST(sei.DamagerIndex)
			IF IS_ENTITY_A_PED(sei.DamagerIndex) 
				IF GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex) =  PLAYER_PED_ID()
					
					IF DOES_ENTITY_EXIST(sei.VictimIndex)
						IF sei.VictimIndex != sei.DamagerIndex
							FLOAT fModelValue
						
							//Damaged a vehicle
							IF IS_ENTITY_A_VEHICLE(sei.VictimIndex)
								
								VEHICLE_INDEX victimVeh =  GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex)
								IF NOT IS_VEHICLE_A_PERSONAL_VEHICLE(victimVeh)
								AND NOT IS_VEHICLE_YACHT_VEHICLE(victimVeh)
								AND NOT IS_VEHICLE_A_PERSONAL_TRUCK(victimVeh, FALSE)
								AND NOT IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(victimVeh, FALSE)
								AND NOT IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(victimVeh, FALSE)
									
									CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - Entity is vehicle.")
									
									MODEL_NAMES victimModel = GET_ENTITY_MODEL(victimVeh)
									
									VEHICLE_VALUE_DATA sValueData
									IF GET_VEHICLE_VALUE(sValueData, victimModel, IS_VEHICLE_ALT_VERSION(victimVeh))
										fModelValue = TO_FLOAT(sValueData.iStandardPrice)
										CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - GET_VEHICLE_VALUE = ",fModelValue)
									ENDIF
									
									IF fModelValue <= 0
										fModelValue = TO_FLOAT(GET_VEHICLE_MODEL_VALUE(victimModel))
										CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - No vehicle value, using = GET_VEHICLE_MODEL_VALUE = ",fModelValue)
									ENDIF
									
									fModelValue *= GET_VALUE_MODIFIER_FOR_VEHICLES()
									CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - GET_VALUE_MODIFIER_FOR_VEHICLES = ",GET_VALUE_MODIFIER_FOR_VEHICLES()," fModelValue is now ",fModelValue)
													
									FLOAT fMaxDamageCap = TO_FLOAT(g_sMPTunables.icriminal_damage_vehicle_value_cap)
									IF fModelValue > fMaxDamageCap
										fModelValue = fMaxDamageCap
										CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - Applying cap of ",fMaxDamageCap," to vehicle model price.")
									ENDIF
																		
								ELSE
									//REPLACE WITH DO_HELP

//										PRINT_HELP_NO_SOUND("AMBC_PERS",DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
//										GB_SET_GANG_BOSS_HELP_BACKGROUND()

									CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - IS_VEHICLE_A_PERSONAL_VEHICLE - not counting.")
								ENDIF
																
							ELIF IS_ENTITY_A_PED(sei.VictimIndex)
								PED_INDEX pedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex)
								IF IS_PED_A_PLAYER(pedID)
									CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - Entity is player.")
	
									PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedID)
									IF NETWORK_IS_PLAYER_ACTIVE(playerID)
										INT iPlayer = NATIVE_TO_INT(playerID)
										IF iPlayer > -1 AND iPlayer < NUM_NETWORK_PLAYERS
										AND playerID != PLAYER_ID() //No score for suicide
											fModelValue = TO_FLOAT(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.iPlayerClothingCost)
											fModelValue *= GET_VALUE_MODIFIER_FOR_CLOTHING()
											CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - CLOTHING - Total fModelValue: ", fModelValue)
										ENDIF
									ENDIF							

								ENDIF
	
							//Dont care
							ELSE
								CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - Entity not veh or ped, don't care.")
								EXIT
							ENDIF
							
							IF fModelValue > 0
							
								INT iTotalCash
							
								IF IS_ENTITY_A_VEHICLE(sei.VictimIndex)
									iTotalCash = ROUND(fModelValue * g_sMPTunables.fCriminal_Damage_Damage_value_modifier * sei.Damage)
									
									IF iTotalCash < g_sMPTunables.iCriminal_damage_bullet_min_cash
										iTotalCash = g_sMPTunables.iCriminal_damage_bullet_min_cash
										CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - Applying min cap of ",g_sMPTunables.iCriminal_damage_bullet_min_cash)
									ENDIF
									
									IF iTotalCash > g_sMPTunables.iCriminal_damage_bullet_max_cash
										iTotalCash = g_sMPTunables.iCriminal_damage_bullet_max_cash
										CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - Applying max cap of ",g_sMPTunables.iCriminal_damage_bullet_max_cash)
									ENDIF
									
									CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - VEHICLE - iTotalCash = ",iTotalCash," = ",fModelValue," * ",g_sMPTunables.fCriminal_Damage_Damage_value_modifier," * ",sei.Damage)
							
								ELSE //Players
									IF GET_ENTITY_HEALTH(sei.VictimIndex) > 0 
								
										FLOAT fMaxHealth = TO_FLOAT(GET_ENTITY_MAX_HEALTH(sei.VictimIndex))
										FLOAT fDamage = FMIN(sei.Damage, fMaxHealth)
										FLOAT fMultiplier = (fDamage / fMaxHealth)
										FLOAT fTotalCash = fMultiplier * fModelValue
										iTotalCash = ROUND(g_sMPTunables.fCriminal_Damage_Player_Damage_value_modifier * fTotalCash)
										
										CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - Model Value = ",fModelValue)
										CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - Max Health = ",fMaxHealth)
										CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - Damage = ",fDamage)
										CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - Health Damage % = ",fMultiplier)
										CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - Damage Cash = ",fTotalCash," = ",iTotalCash)
										
										IF iTotalCash < g_sMPTunables.icriminal_damage_player_bullet_min_cash
											iTotalCash = g_sMPTunables.icriminal_damage_player_bullet_min_cash
											CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - Applying min cap of ",g_sMPTunables.icriminal_damage_player_bullet_min_cash)
										ENDIF
										
										IF iTotalCash > g_sMPTunables.icriminal_damage_player_bullet_max_cash
											iTotalCash = g_sMPTunables.icriminal_damage_player_bullet_max_cash
											CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - Applying max cap of ",g_sMPTunables.icriminal_damage_player_bullet_max_cash)
										ENDIF
										
										IF fDamage = fMaxHealth
											iTotalCash = 0
											CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - Not counting, damage is >= total health, should be counted in a destroyed event.",iTotalCash)
										ENDIF	
									ELSE
										iTotalCash = 0
										CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - Not counting since entity <Dead> = ",iTotalCash)
									ENDIF
								ENDIF
								
								PLAY_SOUND_FRONTEND(-1,"Criminal_Damage_Low_Value","GTAO_FM_Events_Soundset", FALSE)
								CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - Criminal_Damage_Low_Value")
								
								IF sei.VictimDestroyed
									FLOAT fDeadModifier
									IF IS_ENTITY_A_VEHICLE(sei.VictimIndex)
										fDeadModifier = g_sMPTunables.fCriminal_Damage_Destruction_value_modifier
									ELSE
										fDeadModifier = g_sMPTunables.fCriminal_Damage_Player_Death_value_modifier
									ENDIF
								
									iTotalCash = ROUND(fDeadModifier * fModelValue)
									
									//Play sound
									IF IS_ENTITY_A_PED(sei.VictimIndex)
										PLAY_SOUND_FRONTEND(-1,"Criminal_Damage_Kill_Player","GTAO_FM_Events_Soundset", FALSE)
										CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - Criminal_Damage_Kill_Player")
									ELSE
										START_AUDIO_SCENE("GTAO_FM_Events_Blade_Scene" )
										PLAY_SOUND_FRONTEND(-1,"Criminal_Damage_High_Value","GTAO_FM_Events_Soundset", FALSE)
										CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - Criminal_Damage_High_Value")
									ENDIF
									
									CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - Entity Destroyed = ",iTotalCash)
								ENDIF
								
								IF iTotalCash > 0
									INT iCurrentScore = GET_PARTICIPANT_SCORE(PARTICIPANT_ID_TO_INT())
									CPRINTLN(DEBUG_NET_AMBIENT, "=== RIPPIN_IT_UP === CALCULATE_PRICE_OF_DAMAGE - Total Cash = ",iCurrentScore," + ",iTotalCash)
									SET_PARTICIPANT_SCORE(iCurrentScore + iTotalCash)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_PLAYER_JOIN(INT iCount)
	
	STRUCT_PLAYER_SCRIPT_EVENTS data 
	
	GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, data, SIZE_OF(data))

	// Check player is valid
	IF data.PlayerIndex != INVALID_PLAYER_INDEX()

		// If we haven't yet had knowledge of this player then loop over threads ensuring they have joined this script
		IF NOT IS_BIT_SET(serverBD.iPlayerBitSet, NATIVE_TO_INT(data.PlayerIndex))
			INT iThread
			REPEAT data.NumThreads iThread

				IF data.Threads[iThread] = GET_ID_OF_THIS_THREAD()
					
					SORT_PLAYER_SCORES(data.PlayerIndex, 0)	
					
					SET_BIT(serverBD.iPlayerBitSet, NATIVE_TO_INT(data.PlayerIndex))
				ENDIF       
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_EVENTS()

    INT iCount
    EVENT_NAMES ThisScriptEvent
	STRUCT_EVENT_COMMON_DETAILS Details
   
    REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount

        ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
       	
        SWITCH ThisScriptEvent
		
			CASE EVENT_NETWORK_PLAYER_JOIN_SCRIPT
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					PROCESS_PLAYER_JOIN(iCount)
				ENDIF
			BREAK
            
			CASE EVENT_NETWORK_SCRIPT_EVENT
				
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
				
				//SWITCH Details.Type
					// Script events here.
				//ENDSWITCH
				
			BREAK
			
			CASE EVENT_NETWORK_DAMAGE_ENTITY
				
				IF GET_MODE_STATE() = eMODESTATE_RUN
					IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(),eCLIENTBITSET0_ON_BIKE)
						CALCULATE_PRICE_OF_DAMAGE(iCount)
					ENDIF
				ENDIF
				
			BREAK
			
        ENDSWITCH
            
    ENDREPEAT
    
ENDPROC

PROC MAINTAIN_PARTICIPANT_SCORES(PLAYER_INDEX player, INT iParticipant)

	SORT_PLAYER_SCORES(player,GET_PARTICIPANT_SCORE(iParticipant))

ENDPROC

PROC PROCESS_PARTICIPANT_LOOP()
	
	INT iParticipant
	INT iPlayer
	PLAYER_INDEX playerTemp
	PED_INDEX pedTemp
	TEXT_LABEL_63 tl63Temp
	//VEHICLE_INDEX vehTemp
	BOOL bNoBossOnScript = TRUE
	BOOL bAllCompletedRewards = TRUE
	
	// Reset data.
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		sPlayer[iPlayer].playerId = INVALID_PLAYER_INDEX()
		sPlayer[iPlayer].iParticipantId = (-1)
		sPlayer[iPlayer].playerPedId = pedTemp
		CLEAR_BIT(iPlayerOkBitset, iPlayer)
		CLEAR_BIT(iPlayerDeadBitset, iPlayer)
	ENDREPEAT
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		sParticipant[iParticipant].participantId = INVALID_PARTICIPANT_INDEX()
		sParticipant[iParticipant].iPlayerId = (-1)
		CLEAR_BIT(iParticipantActiveBitset, iParticipant)
		sParticipant[iParticipant].tl63Name = tl63Temp
		sParticipant[iParticipant].bIsBoss = FALSE
		sParticipant[iParticipant].bIsGoon = FALSE
	ENDREPEAT
	
	//currentVehicle = vehTemp
	//eCurrentVehicleModel = DUMMY_MODEL_FOR_SCRIPT
//	vMyBossCoords = << 0.0, 0.0, 0.0 >>
	//vFocusParticipantCoords = << 0.0, 0.0, 0.0 >>
	iNumGoonsOnScript = 0
//	fMyDistanceFromBoss = 999999.0
	
	// Refill data.
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
			
			sParticipant[iParticipant].participantId = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
			SET_BIT(iParticipantActiveBitset, iParticipant)
			
			playerTemp = NETWORK_GET_PLAYER_INDEX(sParticipant[iParticipant].participantId)
			iPlayer = NATIVE_TO_INT(playerTemp)
			
			IF IS_NET_PLAYER_OK(playerTemp, FALSE)
				
				sPlayer[iPlayer].playerId = playerTemp
				sParticipant[iParticipant].iPlayerId = iPlayer
				sPlayer[iPlayer].iParticipantId = iParticipant
				sPlayer[iPlayer].playerPedId = GET_PLAYER_PED(playerTemp)
				sParticipant[iParticipant].tl63Name = GET_PLAYER_NAME(playerTemp)
				
				SET_BIT(iPlayerOkBitset, iPlayer)
				
				IF IS_ENTITY_DEAD(sPlayer[iPlayer].playerPedId)
					SET_BIT(iPlayerDeadBitset, iPlayer)
				ENDIF
				
				// Track if boss or a goon.
				IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(sPlayer[iPlayer].playerId)
					sParticipant[iParticipant].bIsBoss = TRUE
				ELIF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(sPlayer[iPlayer].playerId, FALSE)
					sParticipant[iParticipant].bIsGoon = TRUE
				ENDIF
					
				// Track if in a target vehicle.
//				IF sPlayer[iPlayer].playerPedId = PLAYER_PED_ID()
				IF iFocusParticipant = iParticipant
					//vFocusParticipantCoords = GET_ENTITY_COORDS(sPlayer[iPlayer].playerPedId)
					IF IS_PED_IN_ANY_VEHICLE(sPlayer[iPlayer].playerPedId)
						//currentVehicle = GET_VEHICLE_PED_IS_IN(sPlayer[iPlayer].playerPedId)
						//eCurrentVehicleModel = GET_ENTITY_MODEL(currentVehicle)
					ENDIF
				ENDIF
				
				IF sParticipant[iParticipant].bIsBoss
					bNoBossOnScript = FALSE
//					vMyBossCoords = GET_ENTITY_COORDS(sPlayer[iPlayer].playerPedId, FALSE) // Track my boss coords.
					IF NOT IS_BIT_SET(iPlayerDeadBitset, iPlayer)
					AND NOT IS_BIT_SET(iPlayerDeadBitset, NATIVE_TO_INT(PLAYER_ID()))
//						fMyDistanceFromBoss = GET_DISTANCE_BETWEEN_COORDS(vFocusParticipantCoords, vMyBossCoords)
					ENDIF
				ELIF sParticipant[iParticipant].bIsGoon
					iNumGoonsOnScript++ // Track number of goons from my gang on this script.
				ENDIF
				
				IF NOT IS_CLIENT_BIT0_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET0_COMPLETED_REWARDS)
					bAllCompletedRewards = FALSE
				ENDIF
					
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					
					#IF IS_DEBUG_BUILD
					IF serverBD.iSPassPart = (-1)
					AND serverBD.iFFailPart = (-1)
						IF IS_CLIENT_DEBUG_BIT0_SET(sParticipant[iParticipant].participantId, eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
							serverBD.iSPassPart = iParticipant
							PRINTLN("[BIKER1] participant ", iParticipant, " has S passed. Setting serverBD.iSPassPart = ", iParticipant)
						ELIF IS_CLIENT_DEBUG_BIT0_SET(sParticipant[iParticipant].participantId, eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED)
							serverBD.iFFailPart = iParticipant
							PRINTLN("[BIKER1] participant ", iParticipant, " has F failed. Setting serverBD.iFFailPart = ", iParticipant)
						ENDIF
					ENDIF
					#ENDIF
					
					MAINTAIN_PARTICIPANT_SCORES(playerTemp,iParticipant)
				ENDIF
				
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
		IF bAllCompletedRewards
			IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
				SET_SERVER_BIT0(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
			ENDIF
		ELSE
			IF IS_SERVER_BIT0_SET(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
				CLEAR_SERVER_BIT0(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
			ENDIF
		ENDIF
		
		IF GET_END_REASON() = eENDREASON_NO_REASON_YET
		
			IF bNoBossOnScript
				SET_END_REASON(eENDREASON_NO_BOSS_LEFT)
			ELIF iNumGoonsOnScript = 0
				IF HAS_NET_TIMER_STARTED(serverBD.modeTimer)
					IF HAS_NET_TIMER_EXPIRED(serverBD.modeTimer, TRACK_GOON_DELAY) // Wait for little bit before ending script for lack of goons, to give them a chance to join the script before it terminates for them not being on it. 
					#IF IS_DEBUG_BUILD
					AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_IgnoreBossChallengeNoGoonCheck")
					#ENDIF
						SET_END_REASON(eENDREASON_ALL_GOONS_LEFT)
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD

TEXT_WIDGET_ID twIdServerGameState
TEXT_WIDGET_ID twIdClientGameState[GB_MAX_GANG_SIZE_INCLUDING_BOSS]
TEXT_WIDGET_ID twIdClientName[GB_MAX_GANG_SIZE_INCLUDING_BOSS]
TEXT_WIDGET_ID twIdModeState
BOOL bTerminateScriptNow

PROC UPDATE_SERVER_GAME_STATE_WIDGET()
	SET_CONTENTS_OF_TEXT_WIDGET(twIdServerGameState, GET_GAME_STATE_NAME(GET_SERVER_GAME_STATE()))
	SET_CONTENTS_OF_TEXT_WIDGET(twIdModeState, GET_MODE_STATE_NAME(GET_MODE_STATE()))
ENDPROC

PROC UPDATE_CLIENT_GAME_STATE_WIDGET(INT iParticipant)
	
	STRING strName
	
	SET_CONTENTS_OF_TEXT_WIDGET(twIdClientGameState[iParticipant], "NOT ACTIVE")
	SET_CONTENTS_OF_TEXT_WIDGET(twIdClientName[iParticipant], "NOT ACTIVE")
	
	IF IS_BIT_SET(iParticipantActiveBitset, iParticipant)
		IF IS_BIT_SET(iPlayerOkBitset, sParticipant[iParticipant].iPlayerId)
			SET_CONTENTS_OF_TEXT_WIDGET(twIdClientGameState[iParticipant], GET_GAME_STATE_NAME(GET_CLIENT_GAME_STATE(iParticipant)))
			strName = GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, sParticipant[iParticipant].iPlayerId))
			SET_CONTENTS_OF_TEXT_WIDGET(twIdClientName[iParticipant], strName)
		ENDIF
	ENDIF
	
ENDPROC

PROC CREATE_WIDGETS()
	
	INT iParticipant
	TEXT_LABEL_63 tl63Temp
	
	START_WIDGET_GROUP("Rippin it Up")
		ADD_WIDGET_BOOL("Locally terminate Script Now", bTerminateScriptNow)
		ADD_WIDGET_BOOL("Fake End Of Mode Timer", serverBd.bFakeEndOfModeTimer)
		START_WIDGET_GROUP("Game State")
			START_WIDGET_GROUP("Server")
				twIdServerGameState = ADD_TEXT_WIDGET("Game State")
				twIdModeState = ADD_TEXT_WIDGET("Mode State")
				UPDATE_SERVER_GAME_STATE_WIDGET()
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Client")
				REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() iParticipant
					tl63Temp = ""
					tl63Temp +="Part "
					tl63Temp += iParticipant
					tl63Temp += " Name"
					twIdClientName[iParticipant] = ADD_TEXT_WIDGET(tl63Temp)
					tl63Temp = ""
					tl63Temp += "Part "
					tl63Temp += iParticipant
					tl63Temp += " Game State"
					twIdClientGameState[iParticipant] = ADD_TEXT_WIDGET(tl63Temp)
					tl63Temp = ""
					tl63Temp +="Part "
					tl63Temp += iParticipant
					tl63Temp += " Player ID"
					ADD_WIDGET_INT_READ_ONLY(tl63Temp, sParticipant[iParticipant].iPlayerId)
					tl63Temp = ""
					tl63Temp +="Part "
					tl63Temp += iParticipant
					tl63Temp += " Player stored Part ID"
					ADD_WIDGET_INT_READ_ONLY(tl63Temp, sPlayer[sParticipant[iParticipant].iPlayerId].iParticipantId)
					UPDATE_CLIENT_GAME_STATE_WIDGET(iParticipant)
				ENDREPEAT
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
	PRINTLN("[BIKER1] - created widgets.")
	
ENDPROC		

PROC UPDATE_WIDGETS()
	
	INT iPartcount
	
	UPDATE_SERVER_GAME_STATE_WIDGET()
	
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() iPartcount
		UPDATE_CLIENT_GAME_STATE_WIDGET(iPartcount)
	ENDREPEAT
	
	IF NOT IS_LOCAL_DEBUG_BIT0_SET(eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW)
		IF bTerminateScriptNow
			PRINTLN("[BIKER1] widget bTerminateScriptNow = TRUE.")
			SET_LOCAL_DEBUG_BIT0(eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW)
		ENDIF
	ENDIF
	
ENDPROC

#ENDIF



PROC UPDATE_FOCUS_PARTICIPANT()

	// Save out the current focus participant. Stays at -1 if not the local player or not spectating a script participant.
	iFocusParticipant = (-1)
	
	IF IS_BIT_SET(iPlayerOkBitset, NATIVE_TO_INT(PLAYER_ID()))
		IF IS_BIT_SET(iParticipantActiveBitset, PARTICIPANT_ID_TO_INT())
			IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
				iFocusParticipant = PARTICIPANT_ID_TO_INT()
			ELSE
				IF IS_A_SPECTATOR_CAM_RUNNING()
				AND NOT IS_SPECTATOR_HUD_HIDDEN()
					PED_INDEX specTargetPed = GET_SPECTATOR_SELECTED_PED()
					IF IS_PED_A_PLAYER(specTargetPed)
						PLAYER_INDEX specPlayerTemp = NETWORK_GET_PLAYER_INDEX_FROM_PED(specTargetPed)
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(specPlayerTemp)
							PARTICIPANT_INDEX specTargetParticipant = NETWORK_GET_PARTICIPANT_INDEX(specPlayerTemp)
							iFocusParticipant = NATIVE_TO_INT(specTargetParticipant)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC


//----------------------
//	MAIN SCRIPT
//----------------------

SCRIPT(MP_MISSION_DATA missionScriptArgs)


	PRINTLN("[BIKER1] START")
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			
			IF NOT PROCESS_PRE_GAME(missionScriptArgs)
				EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
				SCRIPT_CLEANUP()
			ENDIF
			// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
			#IF IS_DEBUG_BUILD
				CREATE_WIDGETS()
			#ENDIF
		ELSE
			
			PRINTLN("[BIKER1] calling SCRIPT_CLEANUP() - IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE) = FALSE")
			SCRIPT_CLEANUP()
			
		ENDIF
	ELSE
		
		PRINTLN("[BIKER1] calling SCRIPT_CLEANUP() - NETWORK_IS_GAME_IN_PROGRESS() = FALSE")
		EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
		SCRIPT_CLEANUP()
		
	ENDIF
	
	WHILE (TRUE)
	
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		IF IS_LOCAL_DEBUG_BIT0_SET(eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW)
			PRINTLN("[BIKER1] eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW is set")
			EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		#ENDIF
		
		IF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION()
			PRINTLN("[BIKER1] - GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION = TRUE")
			EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[BIKER1] SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
			EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		
		UPDATE_FOCUS_PARTICIPANT()
		PROCESS_PARTICIPANT_LOOP()
		PROCESS_EVENTS()
		
		GB_MAINTAIN_SPECTATE(sSpecVars)
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
			MAINTAIN_J_SKIPS()
		#ENDIF
		
		SWITCH(GET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT()))
		
			CASE GAME_STATE_INIT
				IF GET_SERVER_GAME_STATE() = GAME_STATE_RUNNING
					IF INIT_CLIENT()
						SET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_RUNNING)
					ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_GAME_STATE() = GAME_STATE_RUNNING
				
					CLIENT_PROCESSING()
					
				ELIF GET_SERVER_GAME_STATE() = GAME_STATE_END
					SET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT(), GAME_STATE_END)
				ENDIF
			BREAK
			
			CASE GAME_STATE_END
				SCRIPT_CLEANUP()
			BREAK
			
		ENDSWITCH
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			IF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION()
				PRINTLN("[BIKER1] - GB_SHOULD_KILL_ACTIVE_BOSS_MISSION = TRUE")
				EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
				SET_SERVER_GAME_STATE(GAME_STATE_END)
			ENDIF
		
			SWITCH(GET_SERVER_GAME_STATE())
			
				CASE GAME_STATE_INIT
					IF INIT_SERVER()
						SET_SERVER_GAME_STATE(GAME_STATE_RUNNING)
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING
					SERVER_PROCESSING()
				BREAK
				
				CASE GAME_STATE_END
					SCRIPT_CLEANUP()
				BREAK
				
			ENDSWITCH
		
		ENDIF
		
	ENDWHILE

	
	
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT


