
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Written by:  Steve Tiley																								//
// Date: 		26/05/2016																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"
USING "net_common_functions.sch"


//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_gang_boss.sch"
USING "am_common_ui.sch"
USING "DM_Leaderboard.sch"
USING "net_gang_boss_spectate.sch"
USING "net_gang_boss_to_point.sch"

//----------------------
//	ENUM
//----------------------

ENUM eMODE_STATE
	eMODESTATE_INIT = 0,
	eMODESTATE_RUN,
	eMODESTATE_REWARDS,
	eMODESTATE_END
ENDENUM

ENUM eEND_REASON
	eENDREASON_NO_REASON_YET = 0,
	eENDREASON_TIME_UP,
	eENDREASON_NO_BOSS_LEFT,
	eENDREASON_ALL_GOONS_LEFT,
	eENDREASON_WIN_CONDITION_TRIGGERED,
	eENDREASON_BIKES_DESTROYED
ENDENUM

ENUM eSERVER_BITSET_0
	eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS = 0,		//01
	eSERVERBITSET0_SERVER_INITIALISED,							//02
	eSERVERBITSET0_END // Keep this at the end.
ENDENUM

ENUM eCLIENT_BITSET_0
	eCLIENTBITSET0_COMPLETED_REWARDS = 0,
	eCLIENTBITSET0_CLIENT_INITIALISED,
	eCLIENTBITSET0_END // Keep this at the end.
ENDENUM

ENUM eLOCAL_BITSET_0
	eLOCALBITSET0_EXECUTED_END_TELEMTRY = 0,
	eLOCALBITSET0_CALCULATED_REWARDS,
	eLOCALBITSET0_CALLED_COMMON_SETUP,
	eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP,
	eLOCALBITSET0_CALLED_START_COMMON_TELEMETRY,
	eLOCALBITSET0_END // Keep this at the end.
ENDENUM

ENUM HELP_TEXT_ENUM
	eHELPTEXT_INTRO = 0,
	
	
	eHT_END // keep this at the end
ENDENUM

ENUM BIG_MESSAGE_ENUM
	eBIGMESSAGE_START = 0,
	
	
	eBIGM_END // keep this at the end

ENDENUM

ENUM VARIATIONS_FOR_STEAL_BIKES
	VARIATIONS_SEWER 			= 0,
	VARIATIONS_POLICE_BIKES		= 1,
	VARIATIONS_FISHING_VILLAGE	= 2,
	VARIATIONS_MAX				= 3
ENDENUM

//----------------------
//	DEBUG
//----------------------

#IF IS_DEBUG_BUILD

ENUM eSERVER_DEBUG_BITSET_0
	eSERVERDEBUGBITSET0_TEMP = 0,
	eSERVERDEBUGBITSET0_END // Keep this at the end.
ENDENUM

ENUM eCLIENT_DEBUG_BITSET_0
	eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED = 0,
	eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED,
	eCLIENTDEBUGBITSET0_END // Keep this at the end.
ENDENUM

ENUM eLOCAL_DEBUG_BITSET_0
	eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW = 0,
	eLOCALDEBUGBITSET0_END // Keep this at the end.
ENDENUM

#ENDIF


//----------------------
//	CONSTANTS
//----------------------

CONST_INT GAME_STATE_INIT 								0
CONST_INT GAME_STATE_RUNNING							1
CONST_INT GAME_STATE_TERMINATE_DELAY					2
CONST_INT GAME_STATE_END								3
INT NUMBER_OF_GOONS_UNLOAD		=				9
CONST_INT TRACK_GOON_DELAY								1000*10
CONST_INT NUMBER_OF_VEHICLES_STEAL						4

INT soundidforalarm 
//----------------------
//	STRUCT
//----------------------

STRUCT STRUCT_PARTICIPANT_ID_DATA
	PARTICIPANT_INDEX participantId
	INT iPlayerId
	TEXT_LABEL_63 tl63Name
	BOOL bIsBoss
	BOOL bIsGoon

ENDSTRUCT

STRUCT STRUCT_PLAYER_ID_DATA
	PLAYER_INDEX playerId
	INT iParticipantId
	PED_INDEX playerPedId

ENDSTRUCT


//----------------------
//	LOCAL VARIABLES
//----------------------
GB_BIKER_RACE_DATA sBikerRace
STRUCT_PARTICIPANT_ID_DATA sParticipant[NUM_NETWORK_PLAYERS]
STRUCT_PLAYER_ID_DATA sPlayer[NUM_NETWORK_PLAYERS]
GANG_BOSS_MANAGE_REWARDS_DATA sGangBossManageRewardsData
GB_MAINTAIN_SPECTATE_VARS sSpecVars
GB_STRUCT_BOSS_END_UI sEndUI
GB_COUNTDOWN_MUSIC_STRUCT cmStruct
INT iLocalBitset0
INT iLocalHelpBitset0
INT iLocalBigMessageBitset0
BOOL additionalrival_objective_text = FALSE
INT iPlayerOkBitset
INT iParticipantActiveBitset
INT iPlayerDeadBitset
BOOL set_player_end_wanted_supression = FALSE
INT iFocusParticipant
//BOOL bmarkalljetskistobedeleted
INT iTimeRemaining
BLIP_INDEX blip_area
//BLIP_INDEX blip_area_area
BLIP_INDEX blip_bike[NUMBER_OF_VEHICLES_STEAL]
AI_BLIP_STRUCT 	blip_enemy_ai[15]

//BLIP_INDEX blip_bike_pre_1
BOOL removetunneltext
BLIP_INDEX blip_safehouse
BOOL tell_rivals_to_kill_players
BOOL bflashminimapsafehouserun
BOOL btriggerstolenbikemusic
BOOL TRIGGERENEMYAIMUSIC
BOOL bsetbikesasVULNERABLE[NUMBER_OF_VEHICLES_STEAL]
//INT VAN_ARRAY_NUMBER = 0
//VEHICLE_INDEX currentVehicle
//MODEL_NAMES eCurrentVehicleModel
SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
//VECTOR vFocusParticipantCoords
INT iNumGoonsOnScript

#IF IS_DEBUG_BUILD
INT iLocalDebugBitset
#ENDIF
BOOL makeplayerleavebike
BOOL blockeddoorsforenemyped
BOOL bSet
BOOL removecustomspawnpoint
BOOL bprinted_help_at_start = FALSE
BOOL bprinted_initial_help_at_start = FALSE
BOOL bsetallbikesasvunerable = FALSE
//----------------------
//	BROADCAST VARIABLES
//----------------------

// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData

	INT iServerGameState = GAME_STATE_INIT
	INT iServerBitSet
	SCRIPT_TIMER modeTimer
	eMODE_STATE eModeState
	eEND_REASON eEndReason
	
	PLAYER_INDEX piLaunchBoss
	NETWORK_INDEX 		serverbiketosteal[NUMBER_OF_VEHICLES_STEAL]
	NETWORK_INDEX 		servervehiclesprops[4]
	
	#IF IS_DEBUG_BUILD
	INT iDebugBitset
	INT iSPassPart = -1
	INT iFFailPart = -1
	BOOL bFakeEndOfModeTimer
	#ENDIF
	NETWORK_INDEX 		unloadgoons[15]
	INT					iMatchId1
	INT					iMatchId2
	INT numberofvehiclescreated
	BOOL bcreate_bVEHICLES
	BOOL bcreate_vehicles_and_goons
	BOOL bcreate_bDEAL_GOONS 
	BOOL bcreate_bDEAL_PICKUPS 
	BOOL bisthisbikedestroyed[NUMBER_OF_VEHICLES_STEAL]
	BOOL bisthisbikedelivered[NUMBER_OF_VEHICLES_STEAL]
	BOOL bisthisbikeoutoftunnel[NUMBER_OF_VEHICLES_STEAL]
	BOOL bisthisbikeidle[NUMBER_OF_VEHICLES_STEAL]
	BOOL bwasthisbikecreated[NUMBER_OF_VEHICLES_STEAL]
	GB_BIKER_RACE_SERVER_DATA sRaceServerVars
	VARIATIONS_FOR_STEAL_BIKES mission_variation
	INT inumberofcoverpoints
	OBJECT_INDEX obcoverpoints[8]
	BOOL bcreate_bDEAL_COVERPOINTS
	BOOL bcreate_bVEHICLES_PROP
	BOOL bmissionhasgoneglobal  = FALSE
ENDSTRUCT

ServerBroadcastData serverBD

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	
	INT iClientGameState = GAME_STATE_INIT
	INT iClientBitSet
	
	#IF IS_DEBUG_BUILD
	INT iDebugBitset
	#ENDIF
	BOOL this_player_on_any_bike
	BOOL this_player_in_deal_area
	BOOL this_player_in_deal_area_and_in_launch_gang
	BOOL this_player_is_at_safehouse
	INT  numbikesdestroyed
ENDSTRUCT

PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]


//----------------------
//	TUNEABLE FUNCTIONS
//----------------------

// Add tuneables here.
FUNC INT GET_MODE_TUNEABLE_TIME_LIMIT()
	RETURN 1000 * g_sMPTunables.iBIKER_NINE_TENTHS_TIME_LIMIT                                          // Default of 30 minutes. Change this to whatever is needed. Add tuneable once they have been added.
ENDFUNC
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////// MAIN SCRIPT FUNCITONS				///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Helper functions

FUNC PLAYER_INDEX GET_LAUNCH_BOSS_PLAYER_INDEX()
	RETURN serverBD.piLaunchBoss
ENDFUNC

FUNC BOOL IS_PLAYER_THE_LAUNCH_BOSS(PLAYER_INDEX playerID)
	RETURN (GET_LAUNCH_BOSS_PLAYER_INDEX() = playerID)
ENDFUNC

FUNC BOOL AM_I_THE_LAUNCH_BOSS()
	RETURN IS_PLAYER_THE_LAUNCH_BOSS(PLAYER_ID())
ENDFUNC

FUNC BOOL IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_INDEX playerID)
	RETURN GB_IS_PLAYER_MEMBER_OF_THIS_BIKER_GANG(playerID, GET_LAUNCH_BOSS_PLAYER_INDEX(), FALSE)
ENDFUNC

FUNC BOOL AM_I_GOON_IN_LAUNCH_GANG()
	RETURN IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
ENDFUNC

FUNC BOOL AM_I_IN_LAUNCH_GANG()
	IF AM_I_THE_LAUNCH_BOSS()
	OR AM_I_GOON_IN_LAUNCH_GANG()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

INT iPhoneCallBitset
CONST_INT biPhone_AddedChar		0
CONST_INT biPhone_DoneIntroCall	1

//----------------------
//	FUNCTIONS
//----------------------



structPedsForConversation sSpeech

PROC MAINTAIN_STEAL_BIKES_PHONECALLS()
	INT phonecallModifiers
	TEXT_LABEL_15 tl15Call
	
	//-- Add D* character
	IF NOT IS_BIT_SET(iPhoneCallBitset, biPhone_AddedChar)
		ADD_PED_FOR_DIALOGUE(sSpeech, 2, NULL, "MALCOLM")
		SET_BIT(iPhoneCallBitset, biPhone_AddedChar)
		PRINTLN("[BIKER_STEAL]     ---------->  [] [MAINTAIN_RESCUE_CONTACT_PHONECALLS] Set biPhone_AddedChar")
	ENDIF
	
	//-- Intro call
	IF NOT IS_BIT_SET(iPhoneCallBitset, biPhone_DoneIntroCall)
		IF IS_BIT_SET(iPhoneCallBitset, biPhone_AddedChar)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_START_OF_JOB)	
					//IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_BUDDY_ADDED_AT_LEAST_ONCE)
						IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE,DEFAULT, FALSE)
							SET_BIT(phonecallModifiers, MP_COMMS_MODIFIER_FORCE_PLAYER_TO_ANSWER)

							
							IF serverBD.mission_variation	= VARIATIONS_SEWER
								tl15Call = "BPMAL_NINE1"
							ELIF serverBD.mission_variation	= VARIATIONS_POLICE_BIKES 
								tl15Call = "BPMAL_NINE2"
							ELIF serverBD.mission_variation	= VARIATIONS_FISHING_VILLAGE 
								tl15Call = "BPMAL_NINE3"
							ENDIF
								
								
							PRINTLN("[BIKER_STEAL]     ---------->  [] [MAINTAIN_RESCUE_CONTACT_PHONECALLS] Doing intro phone call with label ", tl15Call)
							IF Request_MP_Comms_Message(sSpeech, GET_MY_CLUBHOUSE_PHONE_CONTACT(), "BPMALAU", tl15Call, phonecallModifiers) 
								SET_BIT(iPhoneCallBitset, biPhone_DoneIntroCall)
								PRINTLN("[BIKER_STEAL]     ---------->  [] [MAINTAIN_RESCUE_CONTACT_PHONECALLS] Set biPhone_DoneIntroCall")
							ENDIF
						ENDIF
					//ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC PROCESS_DAMAGE(INT iCount)
	


	VEHICLE_INDEX VictimVehicleID

	INT i

	STRUCT_ENTITY_DAMAGE_EVENT sEntityID
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEntityID, SIZE_OF(sEntityID))
		PRINTLN("2952950, PROCESS_DAMAGE 0 ")

		IF DOES_ENTITY_EXIST(sEntityID.VictimIndex)
			PRINTLN("2952950, PROCESS_DAMAGE 1 ")

			// IF the victim is a vehicle
	
			REPEAT serverBD.numberofvehiclescreated i
				IF IS_ENTITY_DEAD(sEntityID.VictimIndex)
					IF IS_ENTITY_A_VEHICLE(sEntityID.VictimIndex)
						VictimVehicleID = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
						IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
							IF  DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[i])
									IF VictimVehicleID = NET_TO_veh(serverBD.serverbiketosteal[i])
										IF PLAYER_PED_ID() = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
											playerBD[PARTICIPANT_ID_TO_INT()].numbikesdestroyed++
											PRINTLN("[biker_steal] PROCESS_DAMAGE i destroyed a bike ")
											PRINTLN("[biker_steal] playerBD[PARTICIPANT_ID_TO_INT()].numbikesdestroyed =  ", playerBD[PARTICIPANT_ID_TO_INT()].numbikesdestroyed)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GETVEHICLEPROPSPAWN_COORDS( INT i )
	IF  serverBD.mission_variation	= VARIATIONS_SEWER
		SWITCH i
			CASE 0  RETURN <<10.1750, -643.2500, 15.0880>>  BREAK
			CASE 1  RETURN <<37.0590, -670.0030, 15.4830>>   BREAK
		ENDSWITCH
	ELIF  serverBD.mission_variation	=VARIATIONS_POLICE_BIKES
		SWITCH i
			CASE 0  RETURN <<858.7310, -1357.9399, 25.0950>> BREAK
			CASE 1  RETURN <<858.4410, -1335.3430, 25.0920>> BREAK
			CASE 2  RETURN  <<860.1960, -1342.7520, 25.0300>> BREAK
			CASE 3  RETURN <<860.0750, -1351.4370, 25.0670>> BREAK
			
		ENDSWITCH
		
	//ELIF  serverBD.mission_variation	=VARIATIONS_FISHING_VILLAGE
	ELSE
		SWITCH i
			CASE 0  RETURN <<1360.5797, 4376.0098, 43.3690>> BREAK
			CASE 1	RETURN <<1370.5160, 4316.1836, 37.0272>> BREAK

			
		ENDSWITCH
	
	ENDIF
	SCRIPT_ASSERT("[BIKER_STEAL] Out of range param entered into GETVEHICLEPROPSPAWN_COORDS")
	RETURN <<1916.0400, 4711.1548, 40.1960>> 
ENDFUNC

FUNC FLOAT GETINITIALVEHPROPSPAWN_HEADING( INT i )
	IF  serverBD.mission_variation	= VARIATIONS_SEWER
		SWITCH i
			CASE 0  RETURN 153.0  BREAK
			CASE 1  RETURN 180.9   BREAK
		ENDSWITCH
	ELIF  serverBD.mission_variation	=VARIATIONS_POLICE_BIKES
		SWITCH i
			CASE 0  RETURN 260.0  BREAK
			CASE 1  RETURN 214.9   BREAK
			CASE 2  RETURN 90.0  BREAK
			CASE 3  RETURN 262.9   BREAK
		ENDSWITCH
	ELIF  serverBD.mission_variation	=VARIATIONS_FISHING_VILLAGE
		SWITCH i
			CASE 0 RETURN 156.1984 BREAK
			CASE 1 RETURN 14.3966		BREAK
		ENDSWITCH
	ENDIF
	
	SCRIPT_ASSERT("[BIKER_STEAL] Out of range param entered into GETCARGOBOBSPAWN_COORDS")
	RETURN  180.0
ENDFUNC


FUNC VECTOR GETVEHICLESPAWN_COORDS( INT i )

	IF  serverBD.mission_variation	= VARIATIONS_SEWER
		SWITCH i
			CASE 0  RETURN <<26.1220, -636.8010, 15.0880>>   BREAK
			CASE 1  RETURN <<28.0720, -638.7020, 15.0880>>   BREAK
			CASE 2  RETURN <<18.4310, -652.5150, 15.0880>>   BREAK
			CASE 3  RETURN <<12.1750, -644.2500, 15.0880>>   BREAK
			CASE 4  RETURN <<20.7740, -654.3670, 15.0880>>    BREAK
			CASE 5  RETURN <<6.80590, -636.0030, 15.4830>>    BREAK		
			
			
		ENDSWITCH
	ELIF  serverBD.mission_variation	=VARIATIONS_POLICE_BIKES
		SWITCH i
			CASE 0 RETURN <<851.7, -1358.9180, 26.2540>> 	BREAK
			CASE 1 RETURN <<870.7730, -1351.2080, 25.3140>> BREAK
			CASE 2 RETURN <<874.5550, -1348.9720, 25.3140>> BREAK		
			CASE 3 RETURN <<850.6880, -1329.3, 26.2880>> BREAK
			CASE 4 RETURN <<848.1960, -1340.7520, 25.0300>> BREAK

		
		ENDSWITCH
	ELIF  serverBD.mission_variation	=VARIATIONS_FISHING_VILLAGE
		SWITCH i
				CASE 0 RETURN <<1355.8721, 4383.6582, 43.3438>>		BREAK
				CASE 1 RETURN <<1336.0562, 4305.1318, 37.1164>> 	BREAK   
				CASE 2 RETURN <<1337.3313, 4362.3467, 43.3659>> 	BREAK   
				CASE 3 RETURN <<1368.5840, 4310.0483, 37.0895>> 	BREAK   
				CASE 4 RETURN <<1360.5797, 4376.0098, 43.3690>> 	BREAK   
				CASE 5 RETURN <<1370.5160, 4316.1836, 37.0272>> 	BREAK   

		ENDSWITCH
	
	ENDIF
	
	SCRIPT_ASSERT("[BIKER_STEAL] Out of range param entered into GETCARGOBOBSPAWN_COORDS")
	RETURN <<1916.0400, 4711.1548, 40.1960>> 
ENDFUNC
		

FUNC VECTOR GET_STEAL_BIKE_COORDS()
	IF  serverBD.mission_variation	= VARIATIONS_SEWER
		RETURN  <<-66.0629, -535.4803, 30.9105>>			// sewer entrance
	ELIF  serverBD.mission_variation	=VARIATIONS_POLICE_BIKES
		RETURN <<816.9740, -1321.2050, 25.0770>>
	
	ELIF  serverBD.mission_variation	=VARIATIONS_FISHING_VILLAGE
		RETURN <<1414.1576, 4398.5229, 42.9700>>
	
	ENDIF
	
	SCRIPT_ASSERT("[BIKER_STEAL] GET_STEAL_BIKE_COORDS INVALID zone" )
//RETURN <<128.6396, -610.9874, 16.7451>>			//go to inside sewer tunnels	
//RETURN <<1033.9708, -272.2930, 49.8483>>			// go to at sewer exit
//RETURN <<693.9766, -1539.3832, 8.7086>>		//	go to at getaway trucks
	RETURN <<-65.0629, -535.4803, 30.9105>>

ENDFUNC

FUNC VECTOR GET_SAFEHOUSE_COORDS()
	IF  serverBD.mission_variation	= VARIATIONS_SEWER
		RETURN  <<261.0435, 2579.0532, 44.0821>>
	ELIF serverBD.mission_variation	=VARIATIONS_FISHING_VILLAGE
		RETURN   <<-500.8526, 302.7271, 82.2071>>
	ELSE
		RETURN	 <<1357.4602, 3617.6865, 33.8887>>
	ENDIF

ENDFUNC

FUNC VECTOR GETINITIALCOVERPOINTSPAWN_COORDS( INT i )
	if  serverBD.mission_variation = VARIATIONS_SEWER
		SWITCH i
			CASE  0  RETURN <<9.4030, -637.8090, 15.0980>>	BREAK
			CASE  1  RETURN <<12.3220, -629.2180, 15.1080>> BREAK
			CASE  2  RETURN <<21.0740, -635.2870, 15.0980>> BREAK
			CASE  3  RETURN <<25.9480, -649.7600, 15.0980>> BREAK
			CASE  4  RETURN <<14.1060, -639.3230, 15.2130>> BREAK
			CASE  5  RETURN <<20.9490, -644.6770, 15.2130>> BREAK
		ENDSWITCH
		SCRIPT_ASSERT("[BIKER_STEAL] Out of range param entered into GETINITIALPACKAGESPAWN_COORDS")
		
		
	ENDIF
	
	
	if  serverBD.mission_variation = VARIATIONS_FISHING_VILLAGE
		SWITCH i
			CASE 	0 	RETURN <<1339.8147, 4313.0151, 36.9794>>	BREAK
			CASE 	1 	RETURN <<1367.8958, 4361.1074, 43.5241>>    BREAK
			CASE 	2 	RETURN <<1365.5142, 4361.3154, 43.5265>>    BREAK
			CASE 	3 	RETURN <<1340.8612, 4360.5083, 43.3678>>    BREAK
		ENDSWITCH
		SCRIPT_ASSERT("[BIKER_STEAL] Out of range param entered into GETINITIALPACKAGESPAWN_COORDS")
		
		
	ENDIF
	RETURN <<1916.0400, 4711.1548, 40.1960>> 
ENDFUNC

FUNC INT GET_NUMBER_OF_BIKES_DELIVERED()

	int counter
		
	INT i
	REPEAT serverBD.numberofvehiclescreated i
		IF serverBD.bisthisbikedelivered[i]
			counter++
		ENDIF
	ENDREPEAT
	
	RETURN counter
ENDFUNC

FUNC BOOL HAS_PLAYER_DESTROYED_A_MOTORCYCLE()
	INT i
	REPEAT NUMBER_OF_GOONS_UNLOAD i
		if NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.unloadgoons[i])
			IF NOT IS_ENTITY_DEAD(NET_TO_PED(serverBD.unloadgoons[i]))
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(NET_TO_PED(serverBD.unloadgoons[i]), PLAYER_PED_ID())
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF IS_PED_SHOOTING( PLAYER_PED_ID()) AND IS_PED_IN_COMBAT(NET_TO_PED(serverBD.unloadgoons[i]))
				RETURN TRUE
			ENDIF
			
		ENDIF	
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC MAKE_ENEMY_PEDS_KICK_OFF()
	INT i
	REPEAT NUMBER_OF_GOONS_UNLOAD i
		if NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.unloadgoons[i])
			IF NOT IS_ENTITY_DEAD(NET_TO_PED(serverBD.unloadgoons[i]))
				SET_PED_SEEING_RANGE(NET_TO_PED(serverBD.unloadgoons[i]), 50.0)
				SET_PED_ID_RANGE(NET_TO_PED(serverBD.unloadgoons[i]), 50.0)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(NET_TO_PED(serverBD.unloadgoons[i]) , 100.0) 
			ENDIF
			
		ENDIF	
	ENDREPEAT

ENDPROC


FUNC INT GET_NUMBER_OF_BIKES_DESTROYED()


	INT i
	int destroyedbikecounter
	REPEAT serverBD.numberofvehiclescreated i

		IF serverBD.bisthisbikedestroyed[i]
			destroyedbikecounter++
		ENDIF
	ENDREPEAT

	RETURN destroyedbikecounter
ENDFUNC


PROC DRAW_BOTTOM_RIGHT_HUD()
	
	IF NOT MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
		EXIT
	ENDIF
	
	IF GB_SHOULD_RUN_SPEC_CAM()
		EXIT
	ENDIF
	
	//IF GET_NUMBER_OF_BIKES_DELIVERED() < 1
	IF serverBD.bcreate_vehicles_and_goons  = FALSE
		EXIT
	ENDIF
	
	
	//INT numberoftotal = serverBD.numberofvehiclescreated - GET_NUMBER_OF_BIKES_DESTROYED()

	//DRAW_GENERIC_SCORE(GET_NUMBER_OF_BIKES_DELIVERED(), "GB_HUSTEALBIKE", DEFAULT, DEFAULT, HUDORDER_DONTCARE,  DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,numberoftotal )
	//DRAW_GENERIC_ELIMINATION


	
	INT i
	HUD_COLOURS eBoxColour[NUMBER_OF_VEHICLES_STEAL]
	//HUD_COLOURS eBoxinactive[NUMBER_OF_VEHICLES_STEAL]

	REPEAT serverBD.numberofvehiclescreated i
	

		IF serverBD.bisthisbikedestroyed[i]
			eBoxColour[i] = HUD_COLOUR_RED 
		//	eBoxinactive[i]  = HUD_COLOUR_BLACK 
		ELSE
		
			IF serverBD.bisthisbikedelivered[i]
				eBoxColour[i] = HUD_COLOUR_BLUE
			ELSE
				eBoxColour[i] = HUD_COLOUR_GREY
				
			ENDIF
		ENDIF
	ENDREPEAT
	//pram 39 where we draw crosses
	IF serverBD.bcreate_vehicles_and_goons  = TRUE
		IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
		OR AM_I_THE_LAUNCH_BOSS()	
		OR  additionalrival_objective_text 
			DRAW_GENERIC_ELIMINATION(serverBD.numberofvehiclescreated, "GB_HUSTEALBIKE2", serverBD.numberofvehiclescreated, 
							 TRUE, TRUE, TRUE, TRUE,
							 DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 
							 eBoxColour[0], eBoxColour[1], eBoxColour[2], eBoxColour[3], DEFAULT, DEFAULT ,DEFAULT,DEFAULT,
							 DEFAULT , DEFAULT , DEFAULT, DEFAULT, 
							 DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT, DEFAULT,
							  serverBD.bisthisbikedestroyed[0], serverBD.bisthisbikedestroyed[1], serverBD.bisthisbikedestroyed[2], serverBD.bisthisbikedestroyed[3])
							
		ENDIF
		
	ENDIF						
	/*DRAW_GENERIC_ELIMINATION(	GET_NUM_TARGETS(), "BCK_HUD", DEFAULT, 
								TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, eBoxColour[0], eBoxColour[1], eBoxColour[2], eBoxColour[3],
								eBoxColour[4], eBoxColour[5], eBoxColour[6], eBoxColour[7], DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 
								HAS_TARGET_BEEN_KILLED(0), HAS_TARGET_BEEN_KILLED(1), HAS_TARGET_BEEN_KILLED(2), HAS_TARGET_BEEN_KILLED(3),
								 HAS_TARGET_BEEN_KILLED(4), HAS_TARGET_BEEN_KILLED(5), HAS_TARGET_BEEN_KILLED(6), HAS_TARGET_BEEN_KILLED(7), DEFAULT, DEFAULT, eCrossColour[0], eCrossColour[1], eCrossColour[2], eCrossColour[3], eCrossColour[4], eCrossColour[5], eCrossColour[6], eCrossColour[7])
*/
ENDPROC



FUNC VECTOR GET_COVERPOINT_OBJECT_ROATATION( INT i )
	if  serverBD.mission_variation = VARIATIONS_SEWER
		SWITCH i
			CASE  0  RETURN <<0.0000, 0.0000, -35.6010>> BREAK
			CASE  1  RETURN <<0.0000, 0.0000, -59.4010>> BREAK
			CASE  2  RETURN <<0.0000, 0.0000, 161.3930>> BREAK
			CASE  3  RETURN <<0.0000, 0.0000, 120.6000>> BREAK
			CASE  4  RETURN <<0.0000, 0.0000, -111.8060>> BREAK
			CASE  5  RETURN <<0.0000, 0.0000, -159.4060>> BREAK
		ENDSWITCH
		SCRIPT_ASSERT("[BIKER_STEAL] Out of range param entered into GETINITIALPACKAGESPAWN_COORDS")
		
		
	ENDIF
	
	if  serverBD.mission_variation = VARIATIONS_FISHING_VILLAGE
		SWITCH i
			CASE 	0	RETURN <<0.0000, 0.0000, 82.1988>>	BREAK
			CASE 	1	RETURN <<0.0000, 0.0000, -5.8031>>   BREAK
			CASE 	2	RETURN <<0.0000, 0.0000, -5.8031>>   BREAK
			CASE 	3	RETURN <<0.0000, -0.0000, 129.5949>>   BREAK
		ENDSWITCH
		SCRIPT_ASSERT("[BIKER_STEAL] Out of range param entered into GETINITIALPACKAGESPAWN_COORDS")
		
		
	ENDIF
	RETURN <<1916.0400, 4711.1548, 40.1960>> 
ENDFUNC

FUNC BOOL CREATE_COVERPOINT_UNLOAD(OBJECT_INDEX &crate, VECTOR vPos, MODEL_NAMES model , VECTOR rotation )

	IF NOT IS_VECTOR_ZERO(vPos)

			//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
			crate = CREATE_OBJECT(model, vPos )
			SET_ENTITY_HEADING(crate, GET_HEADING_BETWEEN_TWO_VECTORS_2D(GETVEHICLESPAWN_COORDS(1), vPos ) )
			ADD_COVER_POINT(vPos, GET_HEADING_BETWEEN_TWO_VECTORS_2D(GETVEHICLESPAWN_COORDS(1), vPos ), COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_300TO0)
			SET_ENTITY_ROTATION(crate, rotation )
			//ADD_COVER_POINT((g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos),g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].fDirection,INT_TO_ENUM(COVERPOINT_USAGE, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].iCoverUsage),INT_TO_ENUM(COVERPOINT_HEIGHT, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].iCoverHeight),INT_TO_ENUM(COVERPOINT_ARC, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].iCoverArc))
			RETURN TRUE
		
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC MODEL_NAMES GET_COVERPOINT_MODEL_STEAL(int i )
	IF  serverBD.mission_variation	= VARIATIONS_SEWER
		SWITCH i
			CASE 0 RETURN prop_woodpile_01a BREAK 
			CASE 1 RETURN prop_woodpile_01a BREAK 
			CASE 2 RETURN prop_woodpile_01a BREAK 
			CASE 3 RETURN prop_woodpile_01a BREAK 
			CASE 4 RETURN prop_boxpile_02b BREAK 
			CASE 5 RETURN prop_boxpile_02b BREAK 
			CASE 6 RETURN  prop_boxpile_02b BREAK 
			CASE 7 RETURN  prop_boxpile_02b BREAK 
		ENDSWITCH
	ENDIF
	
	IF  serverBD.mission_variation	= VARIATIONS_FISHING_VILLAGE
		SWITCH i
			CASE 	0	RETURN prop_cementbags01		BREAK
			CASE 	1	RETURN prop_conc_sacks_02a      BREAK
			CASE 	2	RETURN prop_conc_sacks_02a      BREAK
			CASE 	3	RETURN prop_box_wood04a         BREAK
		ENDSWITCH		
	ENDIF
	RETURN prop_boxpile_02b
ENDFUNC

FUNC BOOL CREATE_COVERPOINTS_SERVER()
	IF serverBD.bcreate_bDEAL_COVERPOINTS
		RETURN TRUE
	ENDIF
	
	INT i
	
	
	IF  serverBD.mission_variation	= VARIATIONS_SEWER
		INTERIOR_INSTANCE_INDEX interiorint = GET_INTERIOR_AT_COORDS_WITH_TYPE(GETINITIALCOVERPOINTSPAWN_COORDS(0), "v_31_tun_swap:410") 
		
		IF IS_VALID_INTERIOR(interiorint)
			IF NOT IS_INTERIOR_READY(interiorint )
				PRINTLN(" [BIKER_STEAL] CREATE_COVERPOINTS_SERVER interior not ready  ")
				RETURN FALSE	
				
			ENDIF
		ELSE
		
			PRINTLN(" [BIKER_STEAL] CREATE_COVERPOINTS_SERVER interior not valid  ")
		ENDIF
	ENDIF
	
	PRINTLN(" [BIKER_STEAL] CREATE_COVERPOINTS_SERVER  ")
	REQUEST_MODEL( GET_COVERPOINT_MODEL_STEAL(0))
	REQUEST_MODEL( GET_COVERPOINT_MODEL_STEAL(1))
	REQUEST_MODEL( GET_COVERPOINT_MODEL_STEAL(2))
	REQUEST_MODEL( GET_COVERPOINT_MODEL_STEAL(3))
	REQUEST_MODEL( GET_COVERPOINT_MODEL_STEAL(4))

	IF HAS_MODEL_LOADED(GET_COVERPOINT_MODEL_STEAL(0))
	AND HAS_MODEL_LOADED(GET_COVERPOINT_MODEL_STEAL(1))
	AND HAS_MODEL_LOADED(GET_COVERPOINT_MODEL_STEAL(2))
	AND HAS_MODEL_LOADED(GET_COVERPOINT_MODEL_STEAL(3))
	AND HAS_MODEL_LOADED(GET_COVERPOINT_MODEL_STEAL(4))

	AND CAN_REGISTER_MISSION_OBJECTS(GB_GET_BOSS_MISSION_NUM_OBJ_REQUIRED(FMMC_TYPE_BIKER_STEAL_BIKES))
		IF  serverBD.mission_variation	= VARIATIONS_SEWER
			serverBD.inumberofcoverpoints = 6 
			 
		ELIF  serverBD.mission_variation	= VARIATIONS_FISHING_VILLAGE
			serverBD.inumberofcoverpoints = 4
		ENDIF 
		
		
		
		REPEAT  serverBD.inumberofcoverpoints i
			
			PRINTLN("[BIKER_STEAL] CREATE_COVERPOINTS_SERVER i = " ,i)


			IF CREATE_COVERPOINT_UNLOAD(serverBD.obcoverpoints[i], GETINITIALCOVERPOINTSPAWN_COORDS(i),  GET_COVERPOINT_MODEL_STEAL(i), GET_COVERPOINT_OBJECT_ROATATION(i))
				
					
			ENDIF
			
		ENDREPEAT

			SET_MODEL_AS_NO_LONGER_NEEDED( GET_COVERPOINT_MODEL_STEAL(0))
			SET_MODEL_AS_NO_LONGER_NEEDED( GET_COVERPOINT_MODEL_STEAL(1))
			SET_MODEL_AS_NO_LONGER_NEEDED( GET_COVERPOINT_MODEL_STEAL(2))
			SET_MODEL_AS_NO_LONGER_NEEDED( GET_COVERPOINT_MODEL_STEAL(3))
			SET_MODEL_AS_NO_LONGER_NEEDED( GET_COVERPOINT_MODEL_STEAL(4))

		RETURN TRUE
	ELSE
	REQUEST_MODEL( GET_COVERPOINT_MODEL_STEAL(0))
	REQUEST_MODEL( GET_COVERPOINT_MODEL_STEAL(1))
	REQUEST_MODEL( GET_COVERPOINT_MODEL_STEAL(2))
	REQUEST_MODEL( GET_COVERPOINT_MODEL_STEAL(3))
	REQUEST_MODEL( GET_COVERPOINT_MODEL_STEAL(4))

	ENDIF
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_THIS_BIKE_IDLE(int bikenumber)


	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[bikenumber])
	AND NOT IS_ENTITY_DEAD(NET_TO_veh(serverBD.serverbiketosteal[bikenumber]))
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF  SERVERBD.bisthisbikedelivered[bikenumber] = FALSE
				IF SERVERBD.bisthisbikedestroyed[bikenumber] = FALSE
					IF IS_VEHICLE_SEAT_FREE(NET_TO_veh(serverBD.serverbiketosteal[bikenumber]), VS_DRIVER )
						SERVERBD.bisthisbikeidle[bikenumber] =TRUE
						//PRINTLN("[BIKER_STEAL] SERVERBD.bisthisbikeidle 4444 IS TRUE BIKE number ", bikenumber)
						RETURN TRUE
					ELSE
						SERVERBD.bisthisbikeidle[bikenumber] =FALSE
					ENDIF
				ELSE
					SERVERBD.bisthisbikeidle[bikenumber] =FALSE
				ENDIF
			ELSE
				SERVERBD.bisthisbikeidle[bikenumber] =FALSE
			ENDIF
			
		ENDIF
	ENDIF	
	SERVERBD.bisthisbikeidle[bikenumber] =FALSE
	RETURN FALSE

ENDFUNC


FUNC BOOL IS_ANY_PLAYER_ON_THIS_STOLEN_BIKE(int bikenumber)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[bikenumber])
	AND NOT IS_ENTITY_DEAD(NET_TO_veh(serverBD.serverbiketosteal[bikenumber]))
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF IS_VEHICLE_SEAT_FREE(NET_TO_veh(serverBD.serverbiketosteal[bikenumber]), VS_DRIVER )
				RETURN FALSE
			ELSE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_BIKE_AT_THE_SAFEHOUSES()

	INT i
	REPEAT serverBD.numberofvehiclescreated i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[i])
		AND NOT IS_ENTITY_DEAD(NET_TO_veh(serverBD.serverbiketosteal[i]))
			IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
			OR AM_I_THE_LAUNCH_BOSS()
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(NET_TO_veh(serverBD.serverbiketosteal[i]), GET_SAFEHOUSE_COORDS(), FALSE) < (8.0)                                  
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						IF NOT serverBD.bisthisbikedelivered[i]
							serverBD.bisthisbikedelivered[i] = TRUE
						ENDIF
					ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	
	RETURN FALSE
ENDFUNC


FUNC BOOL ARE_ALL_BIKES_DESTROYED()

	INT i
	int destroyedbikecounter

	REPEAT serverBD.numberofvehiclescreated i
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[i])
		OR (NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[i]) AND IS_ENTITY_DEAD(NET_TO_veh(serverBD.serverbiketosteal[i])))
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				IF NOT serverBD.bisthisbikedestroyed[i]
					serverBD.bisthisbikedestroyed[i] = TRUE
					TickerEventData.TickerEvent = TICKER_EVENT_STEAL_BIKE_DESTROY1
			
					CLEANUP_NET_ID(serverBD.serverbiketosteal[i])
					BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
				ENDIF
			ENDIF
			destroyedbikecounter++
			
			IF IS_ANY_PLAYER_ON_THIS_STOLEN_BIKE(i)
				
			ENDIF
				
				
		ENDIF
		
	ENDREPEAT
	
	IF destroyedbikecounter >=serverBD.numberofvehiclescreated
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC



FUNC BOOL MAKE_PLAYERS_LOCK_BIKE(INT bikenumber)
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[bikenumber])
	AND NOT IS_ENTITY_DEAD(NET_TO_veh(serverBD.serverbiketosteal[bikenumber]))
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.serverbiketosteal[bikenumber])
			//TASK_EVERYONE_LEAVE_VEHICLE(NET_TO_veh(serverBD.unloadserverbiketosteal[VAN_ARRAY_NUMBER]))
			
			SET_VEHICLE_DOORS_LOCKED(NET_TO_veh(serverBD.serverbiketosteal[bikenumber]),VEHICLELOCK_CANNOT_ENTER) 
			SET_ENTITY_PROOFS(NET_TO_VEH(serverBD.serverbiketosteal[bikenumber]),TRUE,TRUE,TRUE,TRUE,TRUE)
			PRINTLN("[BIKER_STEAL] MAKE_PLAYERS_LOCK_AND_LEAVE_BIKE TRUE 1")

			PRINTLN("[BIKER_STEAL] MAKE_PLAYERS_LOCK_AND_LEAVE_BIKE TRUE")
			
			RETURN TRUE
		ELSE
			PRINTLN("[BIKER_STEAL] MAKE_PLAYERS_LOCK_BIKE FALSE. REQUEST CONTROL OF BIKE")
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.serverbiketosteal[bikenumber])
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL MAKE_PLAYER_LEAVE_BIKE(INT bikenumber)
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[bikenumber])
	AND NOT IS_ENTITY_DEAD(NET_TO_veh(serverBD.serverbiketosteal[bikenumber]))
	//	IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.serverbiketosteal[bikenumber])
			IF NOT IS_VEHICLE_SEAT_FREE(NET_TO_VEH(serverBD.serverbiketosteal[bikenumber]), VS_DRIVER)
				PED_INDEX ped_inside_van
				ped_inside_van = GET_PED_IN_VEHICLE_SEAT(NET_TO_veh(serverBD.serverbiketosteal[bikenumber]), VS_DRIVER )
				TASK_LEAVE_ANY_VEHICLE(ped_inside_van)
			//	SET_VEHICLE_DOORS_LOCKED(NET_TO_veh(serverBD.serverbiketosteal[bikenumber]),VEHICLELOCK_CANNOT_ENTER) 
				PRINTLN("[BIKER_STEAL] MAKE_PLAYERS_LEAVE_BIKE TRUE 1")
				
			ENDIF
			
			IF NOT IS_VEHICLE_SEAT_FREE(NET_TO_VEH(serverBD.serverbiketosteal[bikenumber]), VS_FRONT_RIGHT)
				PED_INDEX ped_inside_van
				ped_inside_van = GET_PED_IN_VEHICLE_SEAT(NET_TO_veh(serverBD.serverbiketosteal[bikenumber]), VS_FRONT_RIGHT )
				TASK_LEAVE_ANY_VEHICLE(ped_inside_van)
			//	SET_VEHICLE_DOORS_LOCKED(NET_TO_veh(serverBD.serverbiketosteal[bikenumber]),VEHICLELOCK_CANNOT_ENTER) 
				PRINTLN("[BIKER_STEAL] MAKE_PLAYERS_LEAVE_BIKE TRUE 1")
				
			ENDIF
			
			PRINTLN("[BIKER_STEAL] MAKE_PLAYER_LEAVE_BIKE TRUE")
			
			RETURN TRUE
		//ELSE
			//PRINTLN("[BIKER_STEAL] MAKE_PLAYER_LEAVE_BIKE FALSE. REQUEST CONTROL OF BIKE")
		//	NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.serverbiketosteal[bikenumber])
	//	ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC





FUNC BOOL HAS_ANY_PLAYER_REACHED_STEAL_AREA_SERVER()
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF  playerBD[i].this_player_in_deal_area  = TRUE
			//PRINTLN("[BIKER_STEAL] HAS_ANY_PLAYER_REACHED_UNLOAD_AREA_SERVER")
			
			RETURN TRUE
		ENDIF
		
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ANYONE_IN_LAUNCH_GANG_REACHED_STEAL_AREA_SERVER()
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF  playerBD[i].this_player_in_deal_area_and_in_launch_gang  = TRUE
			//PRINTLN("[BIKER_STEAL] HAS_ANY_PLAYER_REACHED_UNLOAD_AREA_SERVER")
			
			RETURN TRUE
		ENDIF
		
	ENDREPEAT
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_ANY_PLAYER_ON_A_STOLEN_BIKE()
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF  playerBD[i].this_player_on_any_bike  = TRUE
			//PRINTLN("[BIKER_STEAL] HAS_ANY_PLAYER_REACHED_UNLOAD_AREA_SERVER")
			RETURN TRUE
		ENDIF
		
	ENDREPEAT
	RETURN FALSE
ENDFUNC


FUNC INT GET_BIKE_PLAYER_IS_ON()
	INT i
	
	
	REPEAT serverBD.numberofvehiclescreated i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[i])
		AND NOT IS_ENTITY_DEAD(NET_TO_veh(serverBD.serverbiketosteal[i]))
			IF IS_PED_IN_THIS_VEHICLE(player_ped_id(), NET_TO_VEH(serverBD.serverbiketosteal[i]))
				RETURN i
			ENDIF
		ENDIF
	ENDREPEAT
	//SCRIPT_ASSERT("IGNORABLE ASSERT GET_BIKE_PLAYER_IS_ON returning invalid bike ")
	PRINTLN("[biker_steal]GET_BIKE_PLAYER_IS_ON assert value =  ", i)
	RETURN -1
ENDFUNC

FUNC BOOL SET_BIKES_AS_VULNERABLE(int i)

		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[i])
		AND NOT IS_ENTITY_DEAD(NET_TO_veh(serverBD.serverbiketosteal[i]))
			IF  NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.serverbiketosteal[i])
				SET_ENTITY_PROOFS(NET_TO_VEH(serverBD.serverbiketosteal[i]),FALSE,FALSE,FALSE,FALSE,FALSE)
				PRINTLN("[BIKER_STEAL] SET_BIKES_AS_VULNERABLE")
				RETURN TRUE
			ELSE
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.serverbiketosteal[i])
				RETURN FALSE
			ENDIF
			
		ENDIF
		
	
	RETURN FALSE
ENDFUNC
FUNC BOOL SET_ALL_BIKES_AS_VULNERABLE()
	INT i

	
	REPEAT serverBD.numberofvehiclescreated i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[i])
		AND NOT IS_ENTITY_DEAD(NET_TO_veh(serverBD.serverbiketosteal[i]))
			IF  NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.serverbiketosteal[i])
				SET_ENTITY_PROOFS(NET_TO_VEH(serverBD.serverbiketosteal[i]),FALSE,FALSE,FALSE,FALSE,FALSE)
				PRINTLN("[BIKER_STEAL] SET_BIKES_AS_VULNERABLE")
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE


ENDFUNC

//sets goon's ambient animation/activity
PROC SET_GOON_AMBIENT_ACTIVITY(INT thegoonarraynumber)


	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.unloadgoons[thegoonarraynumber])
	AND IS_ENTITY_ALIVE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]))
		IF serverBD.mission_variation	= VARIATIONS_SEWER
			SWITCH thegoonarraynumber

				CASE 0				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "CODE_HUMAN_MEDIC_KNEEL")
				BREAK
				
				CASE 1				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_HANG_OUT_STREET")
				BREAK
				
				
				CASE 2				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_SMOKING")
				BREAK
				
				CASE 3				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "CODE_HUMAN_MEDIC_KNEEL")
				BREAK
				
				CASE 4				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_SMOKING") //"WORLD_HUMAN_CLIPBOARD")
				BREAK
				
				CASE 5				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_HANG_OUT_STREET")
				BREAK
				
				CASE 6				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_STAND_IMPATIENT")
				BREAK
				
				CASE 7				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_SMOKING")
				BREAK
				
							
				CASE 8				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_PROSTITUTE_HIGH_CLASS")
				BREAK
				
				CASE 9			
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_SMOKING")
				BREAK
				
				CASE 10				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_HANG_OUT_STREET")
				BREAK
				CASE 11				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_CLIPBOARD")
				BREAK
				CASE 12				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_SMOKING")
				BREAK
				CASE 13				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_SMOKING")
				BREAK
				CASE 14				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_STAND_IMPATIENT")
				BREAK
			ENDSWITCH
		ENDIF
		IF serverBD.mission_variation	= VARIATIONS_POLICE_BIKES 
			SWITCH thegoonarraynumber

				CASE 0				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "CODE_HUMAN_MEDIC_TIME_OF_DEATH")
				BREAK
				
				CASE 1				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "CODE_HUMAN_MEDIC_KNEEL")
				BREAK
				
				
				CASE 2				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_COP_IDLES")
				BREAK
				
				CASE 3				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_WINDOW_SHOP_BROWSE")
				BREAK
				
				CASE 4				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "CODE_HUMAN_MEDIC_TIME_OF_DEATH") //"WORLD_HUMAN_CLIPBOARD")
				BREAK
				
				CASE 5				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_HANG_OUT_STREET")
				BREAK
				
				CASE 6				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_HANG_OUT_STREET")
				BREAK
				
				CASE 7				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_COP_IDLES")
				BREAK
				
							
				CASE 8				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_HANG_OUT_STREET")
				BREAK
				
				CASE 9			
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_HANG_OUT_STREET")
				BREAK
				
				CASE 10				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_COP_IDLES")
				BREAK
				CASE 11				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_CLIPBOARD")
				BREAK
				CASE 12				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_SMOKING")
				BREAK
				CASE 13				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_SMOKING")
				BREAK
				CASE 14				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_STAND_IMPATIENT")
				BREAK
			ENDSWITCH
		ENDIF
		
		IF serverBD.mission_variation	= VARIATIONS_FISHING_VILLAGE 
			SWITCH thegoonarraynumber

				CASE 0				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_STAND_IMPATIENT")
				BREAK
				
				CASE 1				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_STAND_MOBILE")
				BREAK
				
				
				CASE 2				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "CODE_HUMAN_MEDIC_KNEEL")
				BREAK
				
				CASE 3				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_HANG_OUT_STREET")
				BREAK
				
				CASE 4				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_HANG_OUT_STREET") //"WORLD_HUMAN_CLIPBOARD")
				BREAK
				
				CASE 5				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_DRINKING")
				BREAK
				
				CASE 6				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_STAND_IMPATIENT")
				BREAK
				
				CASE 7				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_SMOKING_POT")
				BREAK
				
							
				CASE 8				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_SMOKING")
				BREAK
				
				CASE 9			
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_WELDING")
				BREAK
				
				CASE 10				
					TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.unloadgoons[thegoonarraynumber]), "WORLD_HUMAN_SMOKING_POT")
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC




FUNC BOOL IS_THERE_ANY_FREE_BIKES()
	INT i
	REPEAT serverBD.numberofvehiclescreated i
		IF NOT SERVERBD.bisthisbikedelivered[i] 
		AND SERVERBD.bisthisbikeidle[i]
			RETURN TRUE
		ENDIF
	ENDREPEAT 
	RETURN FALSE
ENDFUNC
				


FUNC BOOL IS_THIS_PLAYER_NEAR_STEAL_AREA(PLAYER_INDEX thisPlayer)
	PED_INDEX playerPed = GET_PLAYER_PED(thisPlayer)
	IF  serverBD.mission_variation	= VARIATIONS_SEWER
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed,GET_STEAL_BIKE_COORDS(), TRUE) < (5.0  ) 
		OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed,GETVEHICLESPAWN_COORDS(0), TRUE) < (30.0  ) 
		OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed,<<-174.7, -922.6, 22.3>>, TRUE) < (20.0  )	
		OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed,<<-54.5, -550.6, 31.3>>, TRUE) < (7.0  )	
			IF removecustomspawnpoint = FALSE
				USE_CUSTOM_SPAWN_POINTS(TRUE)
				ADD_CUSTOM_SPAWN_POINT(GET_STEAL_BIKE_COORDS(), GET_HEADING_BETWEEN_TWO_VECTORS_2D(GET_STEAL_BIKE_COORDS(),GETVEHICLESPAWN_COORDS(0)))
			ENDIF
			PRINTLN("[BIKER_STEAL] IS_THIS_PLAYER_NEAR_STEAL_AREA TRUE ")
			playerBD[PARTICIPANT_ID_TO_INT()].this_player_in_deal_area = TRUE
			
			IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
			OR AM_I_THE_LAUNCH_BOSS()
				playerBD[PARTICIPANT_ID_TO_INT()].this_player_in_deal_area_and_in_launch_gang= TRUE
			ENDIF
			RETURN TRUE		
		ENDIF

	ELSE
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed,GET_STEAL_BIKE_COORDS(), FALSE) < (20.0  ) 
		OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed,GETVEHICLESPAWN_COORDS(0), FALSE) < (60.0  ) 
			IF removecustomspawnpoint = FALSE
				USE_CUSTOM_SPAWN_POINTS(TRUE)
				ADD_CUSTOM_SPAWN_POINT(GET_STEAL_BIKE_COORDS(), GET_HEADING_BETWEEN_TWO_VECTORS_2D(GET_STEAL_BIKE_COORDS(),GETVEHICLESPAWN_COORDS(0)))
				
			ENDIF
			PRINTLN("[BIKER_STEAL] IS_THIS_PLAYER_NEAR_STEAL_AREA TRUE 2 ")
			playerBD[PARTICIPANT_ID_TO_INT()].this_player_in_deal_area = TRUE
			IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
			OR AM_I_THE_LAUNCH_BOSS()
				playerBD[PARTICIPANT_ID_TO_INT()].this_player_in_deal_area_and_in_launch_gang= TRUE
			ENDIF
			RETURN TRUE		
		ENDIF
	
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_BIKE_OUT_OF_TUNNEL(INT bikenumber)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[bikenumber])
	AND NOT IS_ENTITY_DEAD(NET_TO_veh(serverBD.serverbiketosteal[bikenumber]))
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(NET_TO_veh(serverBD.serverbiketosteal[bikenumber]),GET_STEAL_BIKE_COORDS(), FALSE) < (20.0  ) 
		OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(NET_TO_veh(serverBD.serverbiketosteal[bikenumber]),<<1033, -274, 50.7>>, FALSE) < (20.0  )
		OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(NET_TO_veh(serverBD.serverbiketosteal[bikenumber]),<<-174.7, -922.6, 22.3>>, FALSE) < (20.0  )	
		OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(Player_ped_id(),GETVEHICLESPAWN_COORDS(0), FALSE) > (100.0  )
		//	PRINTLN("[BIKER_STEAL] IS_THIS_BIKE_OUT_OF_TUNNEL TRUE ")
			serverbd.bisthisbikeoutoftunnel[bikenumber] = TRUE
			
			RETURN TRUE		
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC



PROC ADD_BLIP_FOR_STEAL_BIKE_AREA()
	IF NOT DOES_BLIP_EXIST(blip_area)
		blip_area = ADD_BLIP_FOR_COORD(GET_STEAL_BIKE_COORDS())
		
		SET_BLIP_PRIORITY(blip_area, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
		SET_BLIP_FLASHES(blip_area, TRUE)
		SET_BLIP_FLASH_TIMER(blip_area, 10000)
		SET_BLIP_ROUTE(blip_area, TRUE) 
		MPGlobalsAmbience.bIsGPSDropOff = TRUE
		IF serverBD.mission_variation	= VARIATIONS_SEWER
			SET_BLIP_NAME_FROM_TEXT_FILE(blip_area,"GB_STETUNLIP")
		ELIF serverBD.mission_variation	= VARIATIONS_POLICE_BIKES 
			SET_BLIP_NAME_FROM_TEXT_FILE(blip_area,"GB_STEIMLIP")
		ELIF serverBD.mission_variation	= VARIATIONS_FISHING_VILLAGE 
			SET_BLIP_NAME_FROM_TEXT_FILE(blip_area,"GB_STEFHLIP")
		ENDIF
		//blip_area_area = ADD_BLIP_FOR_RADIUS(GET_STEAL_BIKE_COORDS(), 50.0)
		//SET_BLIP_ALPHA(blip_area_area, g_sMPTunables.iblip_area_alpha )
		//SET_BLIP_COLOUR_FROM_HUD_COLOUR(blip_area_area, HUD_COLOUR_YELLOW)
		MPGlobalsAmbience.vQuickGPS =  GET_STEAL_BIKE_COORDS()
		MPGlobalsAmbience.bIsGPSDropOff = TRUE
		PRINTLN("[BIKER_STEAL] ADD_BLIP_FOR_AIRFIELD added")
	ENDIF

ENDPROC
PROC REMOVE_BLIP_FOR_AREA()
	IF DOES_BLIP_EXIST(blip_area)
		REMOVE_BLIP(blip_area)
		PRINTLN("[BIKER_STEAL] REMOVE_BLIP_FOR_AREA ")
		
	ENDIF
	//IF DOES_BLIP_EXIST(blip_area_area)
	///	REMOVE_BLIP(blip_area_area)
	//	PRINTLN("[BIKER_STEAL] REMOVE_BLIP_FOR_AREA ")
		
	//ENDIF
ENDPROC

FUNC BOOL PRINT_THE_INITIAL_HELP_TEXT()

	IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
	OR AM_I_THE_LAUNCH_BOSS()
	
		IF HAS_NET_TIMER_EXPIRED(serverBd.modeTimer, 5000)  // 5 seconds into game
		
			PRINT_HELP("GB_STEAL_HELP")
			
			RETURN TRUE
			
		ENDIF
		
	ENDIF
	RETURN FALSE
ENDFUNC
FUNC BOOL PRINT_THE_STARTING_HELP_TEXT()

	IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
	OR AM_I_THE_LAUNCH_BOSS()
	
		IF HAS_NET_TIMER_EXPIRED(serverBd.modeTimer, 15000)  // 10 seconds into game
			IF (GB_GET_NUM_GOONS_IN_LOCAL_GANG()+1) = 2
				PRINT_HELP("HELP2PLYUNLOAD")
			ELIF (GB_GET_NUM_GOONS_IN_LOCAL_GANG()+1) = 3
				PRINT_HELP("HELP3PLYUNLOAD")
			ELIF (GB_GET_NUM_GOONS_IN_LOCAL_GANG()+1) = 4
				PRINT_HELP("HELP4PLYUNLOAD")
			ENDIF
			RETURN TRUE
			
		ENDIF
		
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC MODEL_NAMES GETINITIALVEHSPAWN_MODEL( INT i )
	IF serverBD.mission_variation	=VARIATIONS_POLICE_BIKES
		 RETURN Hexer	
	ELIF serverBD.mission_variation	= VARIATIONS_SEWER
	
	
		SWITCH i
			CASE 0  RETURN DAEMON	 	BREAK
			CASE 1  RETURN DAEMON 		BREAK  //	GBURRITO
			CASE 2  RETURN DAEMON	   BREAK
			CASE 3  RETURN DAEMON  BREAK
			CASE 4  RETURN INT_TO_ENUM(MODEL_NAMES, 2053223216)  BREAK
		ENDSWITCH
		
	
	ELIF serverBD.mission_variation	= VARIATIONS_FISHING_VILLAGE
		SWITCH i
			CASE 0  RETURN Enduro	 	BREAK
			CASE 1  RETURN Sanchez 		BREAK  //	GBURRITO
			CASE 2  RETURN Sanchez	   BREAK
			CASE 3  RETURN Enduro  BREAK
			CASE 4  RETURN Enduro  BREAK
		ENDSWITCH	
	ENDIF
	SCRIPT_ASSERT("[BIKER_STEAL] Out of range param entered into GETINITIALVEHSPAWN_MODEL")
	RETURN  INT_TO_ENUM(MODEL_NAMES, 2053223216)
ENDFUNC

				
FUNC FLOAT GETINITIALVEHSPAWN_HEADING( INT i )

	IF  serverBD.mission_variation	= VARIATIONS_SEWER
		SWITCH i
			CASE 0 	RETURN 295.6000    BREAK
			CASE 1 	RETURN 69.0000    BREAK
			CASE 2 	RETURN 10.1990    BREAK
			CASE 3 	RETURN 153.5970    BREAK
			CASE 4 	RETURN 29.1900   BREAK
		ENDSWITCH
	ELIF  serverBD.mission_variation	=VARIATIONS_POLICE_BIKES
		SWITCH i
			CASE 0 RETURN  134.1940	BREAK
			CASE 1 RETURN  64.9420 BREAK
			CASE 2 RETURN  214.1420 BREAK
			CASE 3 RETURN  214.1420 BREAK
			CASE 4 RETURN  90.3990 BREAK

		ENDSWITCH

	ELIF  serverBD.mission_variation	=VARIATIONS_FISHING_VILLAGE
		SWITCH i
			CASE 0 RETURN 145.7972
			CASE 1 RETURN 7.0000
			CASE 2 RETURN 116.3957
			CASE 3 RETURN 137.5963
			CASE 4 RETURN 156.1984
			CASE 5 RETURN 14.3966
			CASE 6 RETURN 270.4000
			CASE 7 RETURN 225.0000
		ENDSWITCH
	
	ENDIF
	SCRIPT_ASSERT("[BIKER_STEAL] Out of range param entered into GETINITIALVEHSPAWN_HEADING")
	RETURN  211.3950 
ENDFUNC

FUNC BOOL CHECK_TO_DELETE_BIKE_BLIP(int i)
	
		IF DOES_BLIP_EXIST(blip_bike[i])
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.serverbiketosteal[i])
				IF IS_PED_IN_THIS_VEHICLE(player_ped_id(), NET_TO_VEH(serverBD.serverbiketosteal[i]))
					//PRINTLN("[SALVAGE] CHECK_TO_DELETE_BIKE_BLIP() IS_PED_IN_THIS_VEHICLE TRUE ")
					//bmarkalljetskistobedeleted = TRUE
					RETURN TRUE
				ENDIF 
	
				IF serverBD.bisthisbikedestroyed[i] = TRUE
					RETURN TRUE
				ENDIF
				IF serverBD.bisthisbikedelivered[i]
					RETURN TRUE
				ENDIF
				
				//IF NOT IS_VEHICLE_SEAT_FREE(NET_TO_VEH(serverBD.serverbiketosteal[i]), VS_DRIVER)
					//PRINTLN("[SALVAGE] CHECK_TO_DELETE_BIKE_BLIP() TRUEIS_PED_IN_ANY_BOAT(PLAYER_PED_ID()) ")
				//	RETURN TRUE
				//ENDIF

				IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.serverbiketosteal[i]))
					//PRINTLN("[SALVAGE] CHECK_TO_DELETE_BIKE_BLIP() TRUE IS_VEHICLE_DRIVEABLE")
					RETURN TRUE
				ENDIF
			ELSE
				//PRINTLN("[SALVAGE] CHECK_TO_DELETE_JETSKI_BLIP jetski does not exist TRUE")
				RETURN TRUE
			
			ENDIF
		ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL ARE_GOONS_SHOOTING()
	INT i
	REPEAT NUMBER_OF_GOONS_UNLOAD i
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.unloadgoons[i])
		AND NOT IS_ENTITY_DEAD(NET_TO_PED(serverBD.unloadgoons[i]))
			
			IF IS_PED_SHOOTING(NET_TO_PED(serverBD.unloadgoons[i]))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC MANAGE_ENEMY_AI_BLIPS()
	INT i
	REPEAT NUMBER_OF_GOONS_UNLOAD i
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),GETVEHICLESPAWN_COORDS(0), FALSE) < (200.0  ) 
		AND NETWORK_DOES_NETWORK_ID_EXIST(serverBD.unloadgoons[i])
		AND NOT IS_ENTITY_DEAD(NET_TO_PED(serverBD.unloadgoons[i]))
			
			UPDATE_ENEMY_NET_PED_BLIP(serverBD.unloadgoons[i], blip_enemy_ai[i], 30.0, DEFAULT, DEFAULT, DEFAULT,"GB_STEENELIP",   BLIP_COLOUR_RED, DEFAULT, FALSE)
		//	IF DOES_BLIP_EXIST(blip_enemy_ai[i].BlipID)
				//SET_BLIP_NAME_FROM_TEXT_FILE(blip_enemy_ai[i].BlipID,"GB_STEENELIP")
			//ENDIF
		ENDIF
	ENDREPEAT
	

ENDPROC

FUNC BOOL CREATE_VEHICLES_SERVER()
	PLAYER_INDEX aPlayer
	IF serverBD.bcreate_bVEHICLES
		RETURN FALSE
	ENDIF
	
	INT i
	INT iParticipant
	serverBD.numberofvehiclescreated = GB_GET_NUM_GOONS_IN_LOCAL_GANG()+1
	
	IF (GB_GET_NUM_GOONS_IN_LOCAL_GANG()+1) >= g_sMPTunables.iBIKER_NINE_TENTHS_MAX_BIKES                 
		PRINTLN("[BIKER_STEAL] CREATE_VEHICLES_SERVER CApping number of bikes to 4 serverBD.numberofvehiclescreated = ", serverBD.numberofvehiclescreated)
		serverBD.numberofvehiclescreated = g_sMPTunables.iBIKER_NINE_TENTHS_MAX_BIKES                 
	ENDIF
	
	IF g_sMPTunables.iBIKER_NINE_TENTHS_MIN_BIKES >    (GB_GET_NUM_GOONS_IN_LOCAL_GANG()+1)
		serverBD.numberofvehiclescreated = g_sMPTunables.iBIKER_NINE_TENTHS_MIN_BIKES
	ENDIF
	

	
	PRINTLN("[BIKER_STEAL] CREATE_VEHICLES_SERVER serverBD.numberofvehiclescreated = ",serverBD.numberofvehiclescreated  )
	//INT numberofjetskistospawn = GB_GET_NUM_GOONS_IN_LOCAL_GANG()+1
	
	//PRINTLN("[BIKER_STEAL] CREATE_VEHICLES_SERVER ")
	REQUEST_MODEL(GETINITIALVEHSPAWN_MODEL(0))
	REQUEST_MODEL(GETINITIALVEHSPAWN_MODEL(1))
	REQUEST_MODEL(GETINITIALVEHSPAWN_MODEL(2))
	REQUEST_MODEL(GETINITIALVEHSPAWN_MODEL(3))
	REQUEST_MODEL(GETINITIALVEHSPAWN_MODEL(4))

	IF HAS_MODEL_LOADED	(GETINITIALVEHSPAWN_MODEL(0))
	AND HAS_MODEL_LOADED(GETINITIALVEHSPAWN_MODEL(1))
	AND HAS_MODEL_LOADED(GETINITIALVEHSPAWN_MODEL(2))
	AND HAS_MODEL_LOADED(GETINITIALVEHSPAWN_MODEL(3))
	AND HAS_MODEL_LOADED(GETINITIALVEHSPAWN_MODEL(4))
	AND CAN_REGISTER_MISSION_VEHICLES(GB_GET_BOSS_MISSION_NUM_VEH_REQUIRED(FMMC_TYPE_BIKER_STEAL_BIKES))

		//REPEAT serverBD.numberofvehiclescreated i
		CLEAR_AREA(GETVEHICLESPAWN_COORDS(0), 50.0, TRUE, DEFAULT,DEFAULT , TRUE)
			
		REPEAT serverBD.numberofvehiclescreated i
		
			
			CREATE_NET_VEHICLE(serverBD.serverbiketosteal[I], GETINITIALVEHSPAWN_MODEL(i), GETVEHICLESPAWN_COORDS(i), GETINITIALVEHSPAWN_HEADING(i), NETWORK_IS_HOST_OF_THIS_SCRIPT(), true )
			serverBD.bwasthisbikecreated[i] = TRUE
			//FREEZE_ENTITY_POSITION(NET_TO_VEH(serverBD.serverbiketosteal[0]), TRUE)
			PRINTLN("[BIKER_STEAL] CREATE_VEHICLES_SERVER created serverBD.serverbiketosteal")
			SET_VEHICLE_TYRES_CAN_BURST(NET_TO_VEH(serverBD.serverbiketosteal[i]), FALSE) 
			SET_ENTITY_PROOFS(NET_TO_VEH(serverBD.serverbiketosteal[i]),TRUE,TRUE,TRUE,TRUE,TRUE)
			IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
	            DECOR_SET_INT(NET_TO_VEH(serverBD.serverbiketosteal[i]), "Not_Allow_As_Saved_Veh", MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
				PRINTLN("     ---------->   [BIKER_STEAL] - DECORATOR 'Not_Allow_As_Saved_Veh' SET")
	  		ENDIF
			SET_VEHICLE_AUTOMATICALLY_ATTACHES(NET_TO_veh(serverBD.serverbiketosteal[i]), FALSE)
			REPEAT NUM_NETWORK_PLAYERS iParticipant
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
					aPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
					IF NOT IS_PLAYER_GOON_IN_LAUNCH_GANG(aPlayer)
					AND NOT  IS_PLAYER_THE_LAUNCH_BOSS(aPlayer)
						SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_veh(serverBD.serverbiketosteal[i]),aPlayer, TRUE)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT

		RETURN TRUE
	ELSE
		REQUEST_MODEL(GETINITIALVEHSPAWN_MODEL(0))
		REQUEST_MODEL(GETINITIALVEHSPAWN_MODEL(1))
		REQUEST_MODEL(GETINITIALVEHSPAWN_MODEL(2))
		REQUEST_MODEL(GETINITIALVEHSPAWN_MODEL(3))
		REQUEST_MODEL(GETINITIALVEHSPAWN_MODEL(4))

	ENDIF
	RETURN FALSE
ENDFUNC

FUNC MODEL_NAMES GET_VEHICLE_PROP_MODEL(INT vehiclenumber)

	IF serverBD.mission_variation	= VARIATIONS_SEWER
		  RETURN GBURRITO 
	ELIF serverBD.mission_variation	= VARIATIONS_POLICE_BIKES
		IF vehiclenumber = 0
		OR vehiclenumber = 1
			RETURN FLATBED 
		ELSE
		 	RETURN police3
		ENDIF
	ELSE
		RETURN GBURRITO 
	ENDIF
			
ENDFUNC


FUNC INT GET_NUMBER_OF_PROP_VEHICLES()

	IF serverBD.mission_variation	= VARIATIONS_SEWER
		  RETURN 2 
	ELIF serverBD.mission_variation	= VARIATIONS_POLICE_BIKES
		 RETURN 4 
	ELSE
		RETURN 2 
	ENDIF
			
ENDFUNC


FUNC BOOL CREATE_PROP_VEHICLES_SERVER()
	
	IF serverBD.bcreate_bVEHICLES_PROP
		RETURN FALSE
	ENDIF
	
	IF serverBD.mission_variation	!= VARIATIONS_SEWER
	AND  serverBD.mission_variation	!= VARIATIONS_POLICE_BIKES
		RETURN TRUE
	ENDIF
	INT i


	PRINTLN("[BIKER_STEAL] CREATE_PROP_VEHICLES_SERVER serverBD.numberofvehiclescreated = ",serverBD.numberofvehiclescreated  )
	//INT numberofjetskistospawn = GB_GET_NUM_GOONS_IN_LOCAL_GANG()+1
	
	//PRINTLN("[BIKER_STEAL] CREATE_VEHICLES_SERVER ")
	REQUEST_MODEL(GET_VEHICLE_PROP_MODEL(0))
	REQUEST_MODEL(GET_VEHICLE_PROP_MODEL(1))
	REQUEST_MODEL(GET_VEHICLE_PROP_MODEL(2))
	REQUEST_MODEL(GET_VEHICLE_PROP_MODEL(3))
	IF HAS_MODEL_LOADED	( GET_VEHICLE_PROP_MODEL(0))
	AND HAS_MODEL_LOADED	( GET_VEHICLE_PROP_MODEL(1))
	AND HAS_MODEL_LOADED	( GET_VEHICLE_PROP_MODEL(2))
	AND HAS_MODEL_LOADED	( GET_VEHICLE_PROP_MODEL(3))
	
	AND CAN_REGISTER_MISSION_VEHICLES(GB_GET_BOSS_MISSION_NUM_VEH_REQUIRED(FMMC_TYPE_BIKER_STEAL_BIKES))

		//REPEAT serverBD.numberofvehiclescreated i
		//CLEAR_AREA(GETVEHICLESPAWN_COORDS(0), 50.0,TRUE, DEFAULT,DEFAULT , TRUE)
			
		REPEAT GET_NUMBER_OF_PROP_VEHICLES() i
		
			
			CREATE_NET_VEHICLE(serverBD.servervehiclesprops[I], GET_VEHICLE_PROP_MODEL(i), GETVEHICLEPROPSPAWN_COORDS(i), GETINITIALVEHPROPSPAWN_HEADING(i), NETWORK_IS_HOST_OF_THIS_SCRIPT(), true )
			//FREEZE_ENTITY_POSITION(NET_TO_VEH(serverBD.servervehiclesprops[0]), TRUE)
			PRINTLN("[BIKER_STEAL] CREATE_PROP_VEHICLES_SERVER created serverBD.serverbiketosteal")
			SET_VEHICLE_TYRES_CAN_BURST(NET_TO_VEH(serverBD.servervehiclesprops[i]), FALSE) 
			SET_VEHICLE_DOORS_LOCKED(NET_TO_veh(serverBD.servervehiclesprops[i]),VEHICLELOCK_CANNOT_ENTER)
			SET_ENTITY_PROOFS(NET_TO_VEH(serverBD.servervehiclesprops[i]),TRUE,TRUE,TRUE,TRUE,TRUE)
			IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
	            DECOR_SET_INT(NET_TO_VEH(serverBD.servervehiclesprops[i]), "Not_Allow_As_Saved_Veh", MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
				PRINTLN("     ---------->   [BIKER_STEAL] - DECORATOR 'Not_Allow_As_Saved_Veh' SET")
	  		ENDIF
			
			IF GET_VEHICLE_PROP_MODEL(i) != flatbed
				SET_VEHICLE_DOOR_CONTROL(NET_TO_VEH(serverBD.servervehiclesprops[i]), SC_DOOR_REAR_LEFT, DT_DOOR_NO_RESET, 1.0)
	            SET_VEHICLE_DOOR_CONTROL(NET_TO_VEH(serverBD.servervehiclesprops[i]), SC_DOOR_REAR_RIGHT, DT_DOOR_NO_RESET, 1.0)
				IF NOT IS_VEHICLE_DOOR_FULLY_OPEN(NET_TO_VEH(serverBD.servervehiclesprops[i]),SC_DOOR_REAR_LEFT)
					SET_VEHICLE_DOOR_OPEN(NET_TO_VEH(serverBD.servervehiclesprops[i]), SC_DOOR_REAR_LEFT,TRUE, TRUE)
				ENDIF
				
				IF NOT IS_VEHICLE_DOOR_FULLY_OPEN(NET_TO_VEH(serverBD.servervehiclesprops[i]),SC_DOOR_REAR_RIGHT)
					SET_VEHICLE_DOOR_OPEN(NET_TO_VEH(serverBD.servervehiclesprops[i]), SC_DOOR_REAR_RIGHT,TRUE, TRUE)
				ENDIF
				SET_CAR_BOOT_OPEN(NET_TO_VEH(serverBD.servervehiclesprops[i]) )
			ENDIF
				
		ENDREPEAT

		RETURN TRUE
	ELSE
		REQUEST_MODEL(GET_VEHICLE_PROP_MODEL(0))
		REQUEST_MODEL(GET_VEHICLE_PROP_MODEL(1))
		REQUEST_MODEL(GET_VEHICLE_PROP_MODEL(2))
		REQUEST_MODEL(GET_VEHICLE_PROP_MODEL(3))

	ENDIF
	RETURN FALSE
ENDFUNC

FUNC WEAPON_TYPE GETINITIALWEAPONSPAWN_MODEL( INT i )
	SWITCH i
		CASE 0  RETURN WEAPONTYPE_SMG	 BREAK
		CASE 1  RETURN WEAPONTYPE_SMG   BREAK
		CASE 2  RETURN WEAPONTYPE_SMG   BREAK
		CASE 3  RETURN WEAPONTYPE_SMG BREAK
		CASE 4  RETURN WEAPONTYPE_SMG BREAK
		CASE 5  RETURN WEAPONTYPE_SMG	 BREAK
		CASE 6  RETURN WEAPONTYPE_SMG BREAK
		CASE 7  RETURN WEAPONTYPE_PISTOL BREAK
		CASE 8  RETURN WEAPONTYPE_PUMPSHOTGUN BREAK
	ENDSWITCH
//	SCRIPT_ASSERT("[BIKER_STEAL] Out of range param entered into GETINITIALWEAPONSPAWN_MODEL")
	RETURN  WEAPONTYPE_SMG 
ENDFUNC

	FUNC VECTOR GETINITIALGOONSPAWN_COORDS( INT i )

	IF  serverBD.mission_variation	= VARIATIONS_SEWER
		SWITCH i
			CASE 0			RETURN <<27.5620, -639.7480, 15.0880>>		BREAK
			CASE 1			RETURN <<12.0190, -631.7780, 15.0880>>     BREAK
			CASE 2			RETURN <<10.7390, -632.3940, 15.0880>>     BREAK
			CASE 3			RETURN <<24.6670, -636.2240, 15.0880>>     BREAK
			CASE 4			RETURN <<12.0780, -637.7680, 15.0880>>     BREAK
			CASE 5			RETURN <<22.0740, -647.5090, 15.0880>>     BREAK
			CASE 6			RETURN <<20.9770, -647.5030, 15.0880>>     BREAK
			CASE 7			RETURN <<35.3580, -665.8810, 15.4830>>     BREAK
			CASE 8			RETURN <<22.1710, -652.9360, 15.0880>>     BREAK
			CASE 9			RETURN <<22.7740, -653.7090, 15.0880>>     BREAK
			CASE 10			RETURN <<19.2580, -652.5010, 15.0880>>     BREAK
			CASE 11			RETURN <<11.8150, -639.7480, 15.0880>>     BREAK
			CASE 12			RETURN <<22.9130, -652.7610, 15.0880>>     BREAK
			CASE 13			RETURN <<26.3210, -639.5780, 15.0880>>     BREAK
			CASE 14			RETURN <<35.1230, -667.4480, 15.4830>>     BREAK

		ENDSWITCH
	ELIF  serverBD.mission_variation	=VARIATIONS_POLICE_BIKES
		SWITCH i
			CASE 0   RETURN <<868.5580, -1348.0880, 25.3140>>		BREAK
			CASE 1   RETURN <<869.2200, -1351.2990, 25.3140>>      BREAK
			CASE 2   RETURN <<863.5900, -1341.8630, 25.1170>>      BREAK
			CASE 3   RETURN <<863.1970, -1342.7090, 25.0600>>      BREAK
			CASE 4   RETURN <<860.8870, -1355.4530, 25.0870>>      BREAK
			CASE 5   RETURN <<856.0200, -1355.2960, 25.0840>>      BREAK
			CASE 6   RETURN <<857.3930, -1354.7480, 25.0810>>      BREAK
			CASE 7   RETURN <<857.4300, -1355.9220, 25.0860>>      BREAK
			CASE 8   RETURN <<855.5390, -1334.9360, 25.0840>>      BREAK
			CASE 9   RETURN <<855.3290, -1335.9570, 25.0610>>      BREAK
			CASE 10  RETURN <<866.1420, -1349.7791, 25.3140>>      BREAK
		ENDSWITCH
	

	ELIF  serverBD.mission_variation	=VARIATIONS_FISHING_VILLAGE
		SWITCH i
			CASE 0			RETURN <<1338.9387, 4361.0884, 43.3661>>		BREAK
			CASE 1			RETURN <<1338.7648, 4359.7832, 43.3670>>       BREAK
			CASE 2			RETURN <<1356.5535, 4382.4888, 43.3438>>       BREAK
			CASE 3			RETURN <<1339.8293, 4309.6479, 37.0807>>       BREAK
			CASE 4			RETURN <<1338.6133, 4310.2915, 37.0608>>       BREAK
			CASE 5			RETURN <<1367.8480, 4317.1802, 36.8920>>       BREAK
			CASE 6			RETURN <<1366.7070, 4317.1558, 36.8050>>       BREAK
			CASE 7			RETURN <<1366.2100, 4358.5991, 43.5000>>       BREAK
			CASE 8			RETURN <<1347.4220, 4338.1201, 37.0910>>       BREAK
			CASE 9			RETURN <<1357.7676, 4377.6250, 43.3425>>       BREAK
		ENDSWITCH
	
	ENDIF
	
	SCRIPT_ASSERT("[BIKER_STEAL] Out of range param entered into GETINITIALGOONSPAWN_COORDS")
	RETURN <<1916.0400, 4711.1548, 40.1960>> 
ENDFUNC


FUNC FLOAT GETINITIALGOONSPAWN_HEADING( INT i )

	IF  serverBD.mission_variation	= VARIATIONS_SEWER
		SWITCH i
			CASE 0   RETURN 328.5980	BREAK
			CASE 1   RETURN 105.9980   BREAK
			CASE 2   RETURN 288.1980   BREAK
			CASE 3   RETURN 238.1970  BREAK
			CASE 4   RETURN 237.9940  BREAK
			CASE 5   RETURN 95.5960   BREAK
			CASE 6   RETURN 260.1970   BREAK
			CASE 7   RETURN 165.1860   BREAK
			CASE 8   RETURN 239.7870   BREAK
			CASE 9   RETURN 16.7870   BREAK
			CASE 10  RETURN 81.3990   BREAK
			CASE 11  RETURN 145.3940  BREAK
			CASE 12  RETURN 166.1880  BREAK
			CASE 13  RETURN 296.3870   BREAK
			CASE 14  RETURN 334.3860  BREAK

		ENDSWITCH
		
		

		
	ELIF  serverBD.mission_variation	=VARIATIONS_POLICE_BIKES
		SWITCH i
			CASE 0   RETURN 356.7980	BREAK
			CASE 1   RETURN 286.5980   BREAK
			CASE 2   RETURN 122.7460   BREAK
			CASE 3   RETURN 101.9970   BREAK
			CASE 4   RETURN 200.1430   BREAK
			CASE 5   RETURN 269.3430   BREAK
			CASE 6   RETURN 101.3440   BREAK
			CASE 7   RETURN 61.1440   BREAK
			CASE 8   RETURN 176.7430   BREAK
			CASE 9   RETURN -3.8570   BREAK
			CASE 10  RETURN 257.7420   BREAK

		ENDSWITCH


	ELIF  serverBD.mission_variation	=VARIATIONS_FISHING_VILLAGE
		SWITCH i
			CASE 0		RETURN 171.3950			BREAK
			CASE 1		RETURN 335.7954	       BREAK
			CASE 2		RETURN 33.3970	       BREAK
			CASE 3		RETURN 51.1989	       BREAK
			CASE 4		RETURN 231.5992	       BREAK
			CASE 5		RETURN 92.5960	       BREAK
			CASE 6		RETURN 263.1950	       BREAK
			CASE 7		RETURN 356.7950	       BREAK
			CASE 8		RETURN 178.3940	       BREAK
			CASE 9		RETURN -7.8018	       BREAK
		ENDSWITCH
		

	ENDIF
	SCRIPT_ASSERT("[BIKER_STEAL] Out of range param entered into GETINITIALGOONSPAWN_COORDS")
	RETURN  211.3950 
ENDFUNC

		
FUNC MODEL_NAMES GETINITIALGOONSPAWN_MODEL( INT i )
	IF  serverBD.mission_variation	= VARIATIONS_SEWER
		SWITCH i
			CASE 0  	RETURN G_M_Y_Lost_01	BREAK
			CASE 1      RETURN G_F_Y_Lost_01        BREAK
			CASE 2      RETURN G_M_Y_Lost_01        BREAK
			CASE 3      RETURN G_M_Y_Lost_01        BREAK
			CASE 4      RETURN G_F_Y_Lost_01        BREAK
			CASE 5      RETURN G_F_Y_Lost_01        BREAK
			CASE 6      RETURN G_M_Y_Lost_01        BREAK
			CASE 7      RETURN G_M_Y_Lost_01        BREAK
			CASE 8      RETURN G_F_Y_Lost_01        BREAK
			CASE 9      RETURN G_M_Y_Lost_01         BREAK
			CASE 10     RETURN G_M_Y_Lost_01    BREAK
			CASE 11     RETURN G_M_Y_Lost_01    BREAK
			CASE 12     RETURN G_F_Y_Lost_01    BREAK
			CASE 13     RETURN G_F_Y_Lost_01    BREAK
			CASE 14     RETURN G_M_Y_Lost_01    BREAK
	
			
		ENDSWITCH
	ELIF  serverBD.mission_variation	=VARIATIONS_FISHING_VILLAGE
		RETURN A_M_M_Hillbilly_02
	
	ELSE

		SWITCH i
			CASE 0  	RETURN S_M_Y_Cop_01	BREAK
			CASE 1      RETURN S_M_M_GenTransport        BREAK
			CASE 2      RETURN S_M_Y_Cop_01        BREAK
			CASE 3      RETURN S_M_Y_Cop_01        BREAK
			CASE 4      RETURN S_M_Y_Cop_01        BREAK
			CASE 5      RETURN S_M_M_GenTransport        BREAK
			CASE 6      RETURN S_M_Y_Cop_01        BREAK
			CASE 7      RETURN S_M_Y_Cop_01        BREAK
			CASE 8      RETURN S_M_Y_Cop_01        BREAK
			CASE 9      RETURN S_M_M_GenTransport         BREAK
			CASE 10     RETURN S_M_Y_Cop_01    BREAK
			CASE 11     RETURN S_M_Y_Cop_01    BREAK
			CASE 12     RETURN S_M_Y_Cop_01    BREAK
			CASE 13     RETURN S_M_Y_Cop_01    BREAK
			CASE 14     RETURN S_M_Y_Cop_01    BREAK
	
			
		ENDSWITCH
	
	ENDIF
	SCRIPT_ASSERT("[BIKER_STEAL] Out of range param entered into GETINITIALGOONSPAWN_MODEL")
	RETURN  INT_TO_ENUM(MODEL_NAMES, 832784782)  
ENDFUNC

FUNC BOOL CREATE_DEAL_GOONS_SERVER()
	INT i
	IF serverBD.bcreate_bDEAL_GOONS
		RETURN TRUE
	ENDIF
	
	PED_INDEX tempped
	IF serverBD.mission_variation	= VARIATIONS_SEWER
		NUMBER_OF_GOONS_UNLOAD = 14
	ELIF serverBD.mission_variation	= VARIATIONS_POLICE_BIKES
		NUMBER_OF_GOONS_UNLOAD = 11
	
	ENDIF
	
	REQUEST_MODEL(GETINITIALGOONSPAWN_MODEL(0))
	REQUEST_MODEL(GETINITIALGOONSPAWN_MODEL(1))
	REQUEST_MODEL(GETINITIALGOONSPAWN_MODEL(2))

	IF  HAS_MODEL_LOADED(GETINITIALGOONSPAWN_MODEL(0))
	AND HAS_MODEL_LOADED(GETINITIALGOONSPAWN_MODEL(1))
	AND HAS_MODEL_LOADED(GETINITIALGOONSPAWN_MODEL(2))
	AND CAN_REGISTER_MISSION_PEDS(GB_GET_BOSS_MISSION_NUM_PEDS_REQUIRED(FMMC_TYPE_BIKER_STEAL_BIKES))
		REPEAT NUMBER_OF_GOONS_UNLOAD i
			//PRINTLN(" [BIKER_STEAL] CREATE_DEAL_GOONS_SERVER serverBD.vareablipcoordinates = ", GETINITIALGOONSPAWN_COORDS(i))
			CREATE_NET_PED(serverBD.unloadgoons[i],  PEDTYPE_CRIMINAL, GETINITIALGOONSPAWN_MODEL(i), GETINITIALGOONSPAWN_COORDS(i), GETINITIALGOONSPAWN_HEADING(i), NETWORK_IS_HOST_OF_THIS_SCRIPT(), true )
		//	FREEZE_ENTITY_POSITION(NET_TO_VEH(serverBD.unloadgoons[0]), TRUE)
			PRINTLN("[BIKER_STEAL] CREATE_DEAL_GOONS_SERVER created for ", i)
			GIVE_WEAPON_TO_PED(NET_TO_PED(serverBD.unloadgoons[i]),GETINITIALWEAPONSPAWN_MODEL(i), 100 )
			SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.unloadgoons[i]), rgFM_AiHatePlyrLikeCops)
			SET_PED_SEEING_RANGE(NET_TO_PED(serverBD.unloadgoons[i]), 30.0)
			SET_PED_ID_RANGE(NET_TO_PED(serverBD.unloadgoons[i]), 20.0)
			SET_PED_ACCURACY(NET_TO_PED(serverBD.unloadgoons[i]), 5)
			
			tempped = NET_TO_PED(serverBD.unloadgoons[i])
			GB_BIKER_SET_PED_ATTRIBUTES_FOR_DIFFICULTY(tempped)
		//	SET_PED_VARIATIONS
			SET_GOON_AMBIENT_ACTIVITY(I)
		ENDREPEAT


		SET_MODEL_AS_NO_LONGER_NEEDED(GETINITIALGOONSPAWN_MODEL(0))
		SET_MODEL_AS_NO_LONGER_NEEDED(GETINITIALGOONSPAWN_MODEL(1))
		SET_MODEL_AS_NO_LONGER_NEEDED(GETINITIALGOONSPAWN_MODEL(2))
			
		
		RETURN TRUE
	ELSE
	REQUEST_MODEL(GETINITIALGOONSPAWN_MODEL(0))
	REQUEST_MODEL(GETINITIALGOONSPAWN_MODEL(1))
	REQUEST_MODEL(GETINITIALGOONSPAWN_MODEL(2))


	ENDIF
	RETURN FALSE
ENDFUNC


PROC REMOVE_ALL_BLIPS_FOR_BIKE()

	INT i
	REPEAT serverBD.numberofvehiclescreated i
	
		IF DOES_BLIP_EXIST(blip_bike[i])
			REMOVE_BLIP(blip_bike[i])
			PRINTLN("[BIKER_STEAL] REMOVE_ALL_BLIPS_FOR_BIKE ")
		ENDIF
	ENDREPEAT

ENDPROC

PROC DELETE_A_BIKE_BLIP(INT i)
	IF DOES_BLIP_EXIST(blip_bike[i])
		REMOVE_BLIP(blip_bike[i])
		PRINTLN("[BIKER_STEAL] DELETE_BLIP done")
		//bhavejetskisbeendeleted = TRUE
	ENDIF
	

ENDPROC

PROC add_blip_for_all_free_bike(BOOL isenemy = FALSE)


	BOOL DELETE_BLIP_FOR_THIS_BIKE
/*
		IF blip_step = 0
			IF NOT DOES_BLIP_EXISTdd(blip_bike_pre_0)
				blip_bike_pre_0 =ADD_BLIP_FOR_COORD(<<-11.4, -802.4, 16.3>>)
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(blip_bike_pre_0, HUD_COLOUR_BLUE)
				SET_BLIP_PRIORITY(blip_bike_pre_0, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
				SET_BLIP_NAME_FROM_TEXT_FILE(blip_bike_pre_0,"GB_UNLOVANBLIP")
				blip_step++
			ENDIF
		ENDIF
	*/
	INT i
	REPEAT serverBD.numberofvehiclescreated i
		DELETE_BLIP_FOR_THIS_BIKE = FALSE
		IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) != GB_UI_LEVEL_NONE
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[i])
			AND NOT IS_ENTITY_DEAD(NET_TO_veh(serverBD.serverbiketosteal[i]))
			//OR 	IS_ANY_PLAYER_ON_A_STOLEN_BIKE()	
				//IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),GET_STEAL_BIKE_COORDS(), FALSE) < (50.0  )    
				IF (NOT IS_ANY_PLAYER_ON_THIS_STOLEN_BIKE(i) AND isenemy = FALSE)
				OR (isenemy = TRUE AND tell_rivals_to_kill_players)
					IF NOT serverBD.bisthisbikedelivered[i]
						IF NOT serverBD.bisthisbikedestroyed[i] 
						AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.serverbiketosteal[i]))
							IF NOT DOES_BLIP_EXIST(blip_bike[i])
								blip_bike[i] =ADD_BLIP_FOR_ENTITY(NET_TO_veh(serverBD.serverbiketosteal[i]))
								

								SET_BLIP_PRIORITY(blip_bike[i], BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
								
						
								SET_BLIP_SPRITE(blip_bike[i], RADAR_TRACE_GANG_BIKE)
								SET_BLIP_NAME_FROM_TEXT_FILE(blip_bike[i],"GB_STEBIKBLIP")
								PRINTLN("[BIKER_STEAL] add_blip_for_all_free_bike number, ", i)
								IF isenemy
									SET_BLIP_COLOUR_FROM_HUD_COLOUR(blip_bike[i], HUD_COLOUR_RED)
								ELSE
									SET_BLIP_COLOUR_FROM_HUD_COLOUR(blip_bike[i], HUD_COLOUR_BLUE)
								ENDIF
								
								//SET_BLIP_ROUTE(blip_bike, TRUE) 
							ENDIF
						ELSE
							DELETE_BLIP_FOR_THIS_BIKE = TRUE
						ENDIF
					ELSE
						DELETE_BLIP_FOR_THIS_BIKE = TRUE
					ENDIF
				ELSE
					DELETE_BLIP_FOR_THIS_BIKE = TRUE
				ENDIF
			ELSE
				DELETE_BLIP_FOR_THIS_BIKE = TRUE	
			ENDIF
		ELSE
			DELETE_BLIP_FOR_THIS_BIKE = TRUE	
		
		ENDIF
		
		IF DELETE_BLIP_FOR_THIS_BIKE
			IF DOES_BLIP_EXIST(blip_bike[i])
				REMOVE_BLIP(blip_bike[i])
				PRINTLN("[BIKER_STEAL] REMOVE_ALL_BLIPS_FOR_BIKE ")
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

FUNC INT GET_NUMBER_OF_DELIVERED_BIKES()

	INT acounter
	INT i
	REPEAT serverBD.numberofvehiclescreated i
		IF serverBD.bisthisbikedelivered[i]
			acounter++
		ENDIF
	ENDREPEAT
	RETURN acounter

ENDFUNC

FUNC INT GET_NUMBER_OF_DESTROYED_BIKES()
	INT i
	INT acounter
	REPEAT serverBD.numberofvehiclescreated i
		IF serverBD.bisthisbikedestroyed[i]
			acounter++
		ENDIF
	ENDREPEAT
	RETURN acounter

ENDFUNC


PROC add_blip_for_bikes_to_be_delivered_bike(BOOL isenemy = FALSE)
	BOOL DELETE_BLIP_FOR_THIS_BIKE

	INT i
	REPEAT serverBD.numberofvehiclescreated i
		DELETE_BLIP_FOR_THIS_BIKE = FALSE
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[i])
		AND NOT IS_ENTITY_DEAD(NET_TO_veh(serverBD.serverbiketosteal[i]))
	
				IF NOT serverBD.bisthisbikedelivered[i]
					IF NOT serverBD.bisthisbikedestroyed[i] 
						IF NOT DOES_BLIP_EXIST(blip_bike[i])
							blip_bike[i] =ADD_BLIP_FOR_ENTITY(NET_TO_veh(serverBD.serverbiketosteal[i]))
							

							SET_BLIP_PRIORITY(blip_bike[i], BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
							
					
							SET_BLIP_SPRITE(blip_bike[i], RADAR_TRACE_GANG_BIKE)
							PRINTLN("[BIKER_STEAL] add_blip_for_all_free_bike number, ", i)
							SET_BLIP_NAME_FROM_TEXT_FILE(blip_bike[i],"GB_STEBIKBLIP")
							IF isenemy
								SET_BLIP_COLOUR_FROM_HUD_COLOUR(blip_bike[i], HUD_COLOUR_RED)
							ELSE
								SET_BLIP_COLOUR_FROM_HUD_COLOUR(blip_bike[i], HUD_COLOUR_BLUE)
							ENDIF
							
							//SET_BLIP_ROUTE(blip_bike, TRUE) 
						ENDIF
					ELSE
						DELETE_BLIP_FOR_THIS_BIKE = TRUE
					ENDIF
				ELSE
					DELETE_BLIP_FOR_THIS_BIKE = TRUE
				ENDIF
		ELSE
			DELETE_BLIP_FOR_THIS_BIKE = TRUE	
		ENDIF
		
		IF DELETE_BLIP_FOR_THIS_BIKE
			IF DOES_BLIP_EXIST(blip_bike[i])
				REMOVE_BLIP(blip_bike[i])
				PRINTLN("[BIKER_STEAL] REMOVE_ALL_BLIPS_FOR_BIKE ")
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

PROC DRAW_SAFEHOUSE_MARKER()
	// DRAW safehouse marker
	VECTOR vPlayerPos =  GET_PLAYER_COORDS(PLAYER_ID())
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)	
	IF VDIST(vPlayerPos, GET_SAFEHOUSE_COORDS()) < 100.0
		IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
		OR AM_I_THE_LAUNCH_BOSS()
			DRAW_MARKER(MARKER_CYLINDER, GET_SAFEHOUSE_COORDS(), <<0, 0, 0>>, <<0, 0, 0>>, <<8.0, 8.0, 3.0 >>, iR, iG, iB, 150)
		ENDIF
	
	ENDIF
	
ENDPROC

PROC DRAW_TUNNEL_MARKER()
	// DRAW safehouse marker
	VECTOR vPlayerPos =  GET_PLAYER_COORDS(PLAYER_ID())
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)	
	IF VDIST(vPlayerPos, GET_STEAL_BIKE_COORDS()) < 100.0
		IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
		OR AM_I_THE_LAUNCH_BOSS()
			DRAW_MARKER(MARKER_CYLINDER, GET_STEAL_BIKE_COORDS(), <<0, 0, 0>>, <<0, 0, 0>>, <<2.5, 2.5, 2.5 >>, iR, iG, iB, 150)
		ENDIF
	
	ENDIF
	
ENDPROC

FUNC BOOL IS_BIKE_AT_SAFEHOUSE_UNLOAD(INT bikenumber )

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[bikenumber])
	AND NOT IS_ENTITY_DEAD(NET_TO_veh(serverBD.serverbiketosteal[bikenumber]))
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(NET_TO_veh(serverBD.serverbiketosteal[bikenumber]),GET_SAFEHOUSE_COORDS(), FALSE) < (6.0)                                  
			PRINTLN("[BIKER_STEAL] IS_VAN_AT_SAFEHOUSE_UNLOAD TRUE ")
			RETURN TRUE
		ENDIF
	ENDIF		
	RETURN FALSE
ENDFUNC

PROC ADD_BLIP_FOR_SAFEHOUSE_STEAL()
	IF NOT DOES_BLIP_EXIST(blip_safehouse)
		blip_safehouse = ADD_BLIP_FOR_COORD(GET_SAFEHOUSE_COORDS())
		SET_BLIP_PRIORITY(blip_safehouse, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
		SET_BLIP_NAME_FROM_TEXT_FILE(blip_safehouse,"BK_DEAL_DRBL")
		PRINTLN("[BIKER_STEAL] ADD_BLIP_FOR_SAFEHOUSE_STEAL added")
		SET_BLIP_ROUTE(blip_safehouse, TRUE) 
		MPGlobalsAmbience.vQuickGPS = GET_SAFEHOUSE_COORDS()
		
		MPGlobalsAmbience.bIsGPSDropOff = TRUE
	ENDIF
	
	IF IS_PLAYER_IN_CLUBHOUSE_PROPERTY(PLAYER_ID())
		IF DOES_BLIP_EXIST(blip_safehouse)
			IF MPGlobalsAmbience.bIsGPSDropOff = TRUE
				SET_BLIP_ROUTE(blip_safehouse, FALSE) 
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(blip_safehouse)
			IF MPGlobalsAmbience.bIsGPSDropOff = FALSE
				SET_BLIP_ROUTE(blip_safehouse, TRUE) 
				MPGlobalsAmbience.vQuickGPS = GET_SAFEHOUSE_COORDS()
				MPGlobalsAmbience.bIsGPSDropOff = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_BLIP_FOR_SAFEHOUSE_UNLOAD()
	IF DOES_BLIP_EXIST(blip_safehouse)
		
		REMOVE_BLIP(blip_safehouse)
		PRINTLN("[BIKER_STEAL] REMOVE_BLIP_FOR_SAFEHOUSE_UNLOAD ")
		
	    MPGlobalsAmbience.vQuickGPS =  <<0,0,0>> 
		MPGlobalsAmbience.bIsGPSDropOff = FALSE
	ENDIF
	//MPGlobalsAmbience.bIsGPSDropOff = FALSE
ENDPROC


FUNC BOOL IS_THIS_PLAYER_AT_SAFEHOUSE_UNLOAD(PLAYER_INDEX thisPlayer)
	PED_INDEX playerPed = GET_PLAYER_PED(thisPlayer)
	
	
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed,GET_SAFEHOUSE_COORDS(), FALSE) < (5.0)                                  
		playerBD[PARTICIPANT_ID_TO_INT()].this_player_is_at_safehouse = TRUE
		PRINTLN("[BIKER_STEAL] IS_THIS_PLAYER_AT_SAFEHOUSE_UNLOAD TRUE ")
		RETURN TRUE
	ENDIF
	

	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_ON_ANY_OF_THE_BIKES(PLAYER_INDEX aplayerindex)

	INT i
	REPEAT serverBD.numberofvehiclescreated i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[i])
		AND NOT IS_ENTITY_DEAD(NET_TO_veh(serverBD.serverbiketosteal[i]))
			IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(aplayerindex), NET_TO_veh(serverBD.serverbiketosteal[i]))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_NEAR_ANY_OF_THE_BIKES(player_index aplayer, float distance =500.0 )

	INT I
	REPEAT serverBD.numberofvehiclescreated i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[i])
		AND NOT IS_ENTITY_DEAD(NET_TO_veh(serverBD.serverbiketosteal[i]))
			IF GET_DISTANCE_BETWEEN_ENTITIES(NET_TO_veh(serverBD.serverbiketosteal[i]), GET_PLAYER_PED(aplayer) , FALSE) < distance 
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC DEAL_WITH_NON_LAUNCH_PEDS_GETTING_ON_MOTOTCYCLES(INT bikenumber )
		
		IF NOT IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
		AND NOT AM_I_THE_LAUNCH_BOSS()
			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[bikenumber])
			AND NOT IS_ENTITY_DEAD(NET_TO_veh(serverBD.serverbiketosteal[bikenumber]))
				IF  NOT GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_veh(serverBD.serverbiketosteal[bikenumber]), PLAYER_ID())
					SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_veh(serverBD.serverbiketosteal[bikenumber]), PLAYER_ID(), TRUE)
				ENDIF
			ENDIF
		ENDIF		

ENDPROC

PROC DEAL_WITH_BIKE_SAFEHOUSE_BLIPS()
	// GET BACK TO THE SAFEHOUSE/get in the van
	IF  serverBD.mission_variation	!= VARIATIONS_SEWER
		IF  playerBD[PARTICIPANT_ID_TO_INT()].this_player_in_deal_area = TRUE
		OR IS_ANY_PLAYER_ON_A_STOLEN_BIKE()
			REMOVE_BLIP_FOR_AREA()
		ENDIF
	ENDIF
	INT j
	IF IS_THIS_PLAYER_ON_ANY_OF_THE_BIKES(PLAYER_ID())
		IF NOT removecustomspawnpoint
			//PED_INDEX playerPed = GET_PLAYER_PED(PLAYER_ID())
			//IF IS_THIS_PLAYER_NEAR_STEAL_AREA(PLAYER_ID())
				USE_CUSTOM_SPAWN_POINTS(FALSE)
				removecustomspawnpoint = TRUE
				PRINTLN("[BIKER_STEAL] remove custom spawn point")
			//ENDIF
		ENDIF	
	ENDIF		

	IF NOT bflashminimapsafehouserun
		IF serverbd.bmissionhasgoneglobal = TRUE
			IF IS_ANY_PLAYER_ON_A_STOLEN_BIKE()	
				IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
				OR AM_I_THE_LAUNCH_BOSS()
					PRINT_HELP("GB_BKR_RVL") 
					FLASH_MINIMAP_DISPLAY()
					FLASH_MY_PLAYER_ARROW(TRUE, 5000)
					PRINTLN("[BIKER_SEAL] mission has gone global")
					bflashminimapsafehouserun = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	REPEAT serverBD.numberofvehiclescreated j
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[j])
		AND NOT IS_ENTITY_DEAD(NET_TO_veh(serverBD.serverbiketosteal[j]))
			
			
			
			IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
			OR AM_I_THE_LAUNCH_BOSS()
				//IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_veh(serverBD.serverbiketosteal[j]))
				IF IS_THIS_PLAYER_ON_ANY_OF_THE_BIKES(PLAYER_ID())
					
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_veh(serverBD.serverbiketosteal[j]))
						IF  serverBD.mission_variation	= VARIATIONS_SEWER
							IF IS_THIS_BIKE_OUT_OF_TUNNEL(j)
								REMOVE_BLIP_FOR_AREA()
								ADD_BLIP_FOR_SAFEHOUSE_STEAL()
								IF NOT Is_This_The_Current_Objective_Text(("GB_STEAL_OBJ3"))
									Clear_Any_Objective_Text_From_This_Script() // Clear objective text if player is retricted for hide or passive.
									Print_Objective_Text("GB_STEAL_OBJ3")   // get to safehouse
								ENDIF
							//	removetunneltext = TRUE
							ELSE
								IF removetunneltext = FALSE
									ADD_BLIP_FOR_STEAL_BIKE_AREA()
									SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BIKER_CH1, "GBSTEALBITXT1", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
									removetunneltext = TRUE
								ENDIF
								
								IF NOT Is_This_The_Current_Objective_Text(("GB_STEALEAVETUN"))
									Clear_Any_Objective_Text_From_This_Script() // Clear objective text if player is retricted for hide or passive.
									Print_Objective_Text("GB_STEALEAVETUN")   // leave tunnel	
								ENDIF
							ENDIF
						ELSE
							IF removetunneltext = FALSE
								IF serverBD.mission_variation	= VARIATIONS_POLICE_BIKES
									SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BIKER_CH1, "GBSTEALBITXT2", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
									removetunneltext = TRUE
								ENDIF
								
							ENDIF
							REMOVE_BLIP_FOR_AREA()
							ADD_BLIP_FOR_SAFEHOUSE_STEAL()
							IF NOT Is_This_The_Current_Objective_Text(("GB_STEAL_OBJ3"))
								Clear_Any_Objective_Text_From_This_Script() // Clear objective text if player is retricted for hide or passive.
								Print_Objective_Text("GB_STEAL_OBJ3")   // get to safehouse
							ENDIF
							
						ENDIF
					ENDIF
					
					REMOVE_ALL_BLIPS_FOR_BIKE()
					//IF bcreatejetskiblips = TRUE
					//AND  serverBD.bcreate_bVEHICLES = TRUE
					
					
					
					playerBD[PARTICIPANT_ID_TO_INT()].this_player_on_any_bike = TRUE
					
				ELSE
					IF HAS_ANYONE_IN_LAUNCH_GANG_REACHED_STEAL_AREA_SERVER()
						//INT i
					 	add_blip_for_all_free_bike()
						REMOVE_BLIP_FOR_AREA()
						
						/*IF serverBD.bcreate_bVEHICLES = TRUE
							REPEAT serverBD.numberofvehiclescreated i
								IF CHECK_TO_DELETE_BIKE_BLIP(i)
									DELETE_A_BIKE_BLIP(i)
								ENDIF 
							ENDREPEAT	   
						ENDIF
						*/
						playerBD[PARTICIPANT_ID_TO_INT()].this_player_on_any_bike = FALSE

						//IF IS_VEHICLE_SEAT_FREE(NET_TO_veh(serverBD.serverbiketosteal[j]), VS_DRIVER )
						IF IS_THERE_ANY_FREE_BIKES()
							IF NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
								IF NOT Is_This_The_Current_Objective_Text(("GB_STEAL_OBJ4"))
								AND NOT  Is_This_The_Current_Objective_Text(("GB_STEAL_OBJ4e"))
								AND NOT  Is_This_The_Current_Objective_Text(("GB_STEAL_OBJ4f"))
									REMOVE_BLIP_FOR_AREA()
									//Clear_Any_Objective_Text_From_This_Script() // Clear objective text if player is retricted for hide or passive.
									IF IS_THIS_PLAYER_AT_SAFEHOUSE_UNLOAD(player_id())
										Print_Objective_Text("GB_STEAL_OBJ4f")   // steal another bike
									ELSE
										IF NOT IS_PLAYER_IN_CLUBHOUSE_PROPERTY(PLAYER_ID())
											IF serverbd.numberofvehiclescreated > 1
												Print_Objective_Text("GB_STEAL_OBJ4e")   // get on a bike
											ELSE
												Print_Objective_Text("GB_STEAL_OBJ4")   // get on a bike
											ENDIF
										ENDIF
									ENDIF
									REMOVE_BLIP_FOR_SAFEHOUSE_UNLOAD()
								ENDIF
							ENDIF
						ELSE
							add_blip_for_bikes_to_be_delivered_bike()
							IF NOT Is_This_The_Current_Objective_Text(("GB_STEAL_OBJ4b"))
							AND NOT Is_This_The_Current_Objective_Text(("GB_STEAL_OBJ4bs"))	
								ADD_BLIP_FOR_SAFEHOUSE_STEAL()
								REMOVE_BLIP_FOR_AREA()
								//Clear_Any_Objective_Text_From_This_Script() // Clear objective text if player is retricted for hide or passive.
								INT numberofbikeslefttodeliver
								numberofbikeslefttodeliver =  serverBD.numberofvehiclescreated - (GET_NUMBER_OF_DELIVERED_BIKES() + GET_NUMBER_OF_DESTROYED_BIKES())
								IF NOT IS_PLAYER_IN_CLUBHOUSE_PROPERTY(PLAYER_ID())
									IF numberofbikeslefttodeliver > 1
										Print_Objective_Text("GB_STEAL_OBJ4bs")   // Protect the ~b~Van~s~ as it returns to the Clubhouse.
									ELSE
										Print_Objective_Text("GB_STEAL_OBJ4b")   // Protect the ~b~Van~s~ as it returns to the Clubhouse.
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ELSE
				// Non launch gang players logic
				//IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),NET_TO_veh(serverBD.serverbiketosteal[j]), FALSE) < 500.0                                  
				//IF IS_PLAYER_NEAR_ANY_OF_THE_BIKES(PLAYER_ID())
				//OR GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
				IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) > GB_UI_LEVEL_NONE //AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),NET_TO_veh(serverBD.serverbiketosteal[j]), FALSE) < 500.0 )
				OR IS_PLAYER_NEAR_ANY_OF_THE_BIKES(PLAYER_ID())  //AND GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())> GB_UI_LEVEL_NONE)
					IF tell_rivals_to_kill_players = FALSE
						IF serverbd.bmissionhasgoneglobal	= TRUE
							IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI	
								IF serverBD.numberofvehiclescreated  = 1
									SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_START_OF_JOB,"GB_STEAL_T", "GB_STEAL_ENEMY", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_LAUNCH_BOSS_PLAYER_INDEX()) , GB_GET_PLAYER_GANG_HUD_COLOUR(GET_LAUNCH_BOSS_PLAYER_INDEX()))
								ELSE
									SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_START_OF_JOB,"GB_STEAL_T", "GB_STEAL_ENEMYs", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_LAUNCH_BOSS_PLAYER_INDEX()) , GB_GET_PLAYER_GANG_HUD_COLOUR(GET_LAUNCH_BOSS_PLAYER_INDEX()))
								ENDIF
								GB_SET_GANG_BOSS_HELP_BACKGROUND()
								FLASH_MINIMAP_DISPLAY()
								FLASH_MY_PLAYER_ARROW(TRUE, 5000)
								add_blip_for_all_free_bike(TRUE)
								tell_rivals_to_kill_players = TRUE
								PRINTLN("[BIKER_STEAL]SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION( GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_LAUNCH_BOSS_PLAYER_INDEX()) , GB_GET_PLAYER_GANG_HUD_COLOUR(GET_LAUNCH_BOSS_PLAYER_INDEX()))  ")
								PRINT_HELP_WITH_PLAYER_NAME("GB_STEENEHELP", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_LAUNCH_BOSS_PLAYER_INDEX()), GB_GET_PLAYER_GANG_HUD_COLOUR(GET_LAUNCH_BOSS_PLAYER_INDEX()))
						
							ENDIF
						ENDIF	
					ENDIF
					
				ENDIF
				
				IF serverbd.bmissionhasgoneglobal	= TRUE
					IF additionalrival_objective_text = FALSE
						IF IS_PLAYER_NEAR_ANY_OF_THE_BIKES(PLAYER_ID())
							
							
							IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
								Print_Objective_Text("GB_STEAL_OBJ6")
								IF NOT  IS_TRANSITION_ACTIVE()
								AND  IS_SKYSWOOP_AT_GROUND() 
									TRIGGER_MUSIC_EVENT("BIKER_NINE_TENTHS_START")
								ENDIF
							
								additionalrival_objective_text = TRUE
								GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
								GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION() 
								Block_All_MissionsAtCoords_Missions(TRUE)
							ENDIF
						ENDIF
					ENDIF	
				ENDIF
				
				IF tell_rivals_to_kill_players = TRUE
					add_blip_for_all_free_bike(TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	IF IS_THIS_PLAYER_ON_ANY_OF_THE_BIKES(PLAYER_ID())
		DRAW_SAFEHOUSE_MARKER()
	ENDIF
ENDPROC



PROC REMOVE_ALL_BLIPS()
	REMOVE_ALL_BLIPS_FOR_BIKE()
	REMOVE_BLIP_FOR_AREA()
	PRINTLN("[BIKER_STEAL] REMOVE_ALL_BLIPS ")
	REMOVE_BLIP_FOR_SAFEHOUSE_UNLOAD()	
ENDPROC
#IF IS_DEBUG_BUILD
FUNC STRING GET_SERVER_BIT0_NAME(eSERVER_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS	RETURN "eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS"
		CASE eSERVERBITSET0_SERVER_INITIALISED					RETURN "eSERVERBITSET0_SERVER_INITIALISED"
		CASE eSERVERBITSET0_END									RETURN "eSERVERBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC

FUNC STRING GET_LOCAL_BIT0_NAME(eLOCAL_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eLOCALBITSET0_EXECUTED_END_TELEMTRY			RETURN "eLOCALBITSET0_EXECUTED_END_TELEMTRY"
		CASE eLOCALBITSET0_CALCULATED_REWARDS				RETURN "eLOCALBITSET0_CALCULATED_REWARDS"
		CASE eLOCALBITSET0_CALLED_COMMON_SETUP				RETURN "eLOCALBITSET0_CALLED_COMMON_SETUP"
		CASE eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP	RETURN "eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP"
		CASE eLOCALBITSET0_END								RETURN "eLOCALBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC

FUNC STRING GET_CLIENT_BIT0_NAME(eCLIENT_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eCLIENTBITSET0_COMPLETED_REWARDS	RETURN "eCLIENTBITSET0_COMPLETED_REWARDS"
		CASE eCLIENTBITSET0_CLIENT_INITIALISED	RETURN "eCLIENTBITSET0_CLIENT_INITIALISED"
		CASE eCLIENTBITSET0_END					RETURN "eCLIENTBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC
#ENDIF

FUNC BOOL IS_SERVER_BIT0_SET(eSERVER_BITSET_0 eBitset)
	RETURN IS_BIT_SET(serverBD.iServerBitSet, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_SERVER_BIT0(eSERVER_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_SERVER_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] GET_SERVER_BIT0_NAME - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		#IF IS_DEBUG_BUILD
		SCRIPT_ASSERT("[BIKER1] calling SET_SERVER_BIT0 on a non host.")
		PRINTLN("")
		#ENDIF
		EXIT
	ENDIF
	
	SET_BIT(serverBD.iServerBitSet, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_SERVER_BIT0(eSERVER_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF IS_BIT_SET(serverBD.iServerBitSet, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_SERVER_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] CLEAR_SERVER_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	CLEAR_BIT(serverBD.iServerBitSet, ENUM_TO_INT(eBitset))
	
ENDPROC

FUNC BOOL IS_CLIENT_BIT0_SET(PARTICIPANT_INDEX partId, eCLIENT_BITSET_0 eBitset)
	RETURN IS_BIT_SET(playerBD[NATIVE_TO_INT(partId)].iClientBitSet, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_CLIENT_BIT0(eCLIENT_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_CLIENT_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] SET_CLIENT_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_CLIENT_BIT0(eCLIENT_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_CLIENT_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] CLEAR_CLIENT_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, ENUM_TO_INT(eBitset))
	
ENDPROC

FUNC BOOL IS_LOCAL_BIT0_SET(eLOCAL_BITSET_0 eBitset)
	RETURN IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_LOCAL_BIT0(eLOCAL_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_LOCAL_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] SET_LOCAL_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	SET_BIT(iLocalBitset0, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_LOCAL_BIT0(eLOCAL_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_LOCAL_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] CLEAR_LOCAL_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	CLEAR_BIT(iLocalBitset0, ENUM_TO_INT(eBitset))
	
ENDPROC


#IF IS_DEBUG_BUILD

FUNC STRING GET_SERVER_DEBUG_BIT0_NAME(eSERVER_DEBUG_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eSERVERDEBUGBITSET0_TEMP	RETURN "eSERVERDEBUGBITSET0_TEMP"
		CASE eSERVERDEBUGBITSET0_END	RETURN "eSERVERDEBUGBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC

FUNC STRING GET_CLIENT_DEBUG_BIT0_NAME(eCLIENT_DEBUG_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED	RETURN "eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED"
		CASE eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED	RETURN "eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED"
		CASE eCLIENTDEBUGBITSET0_END				RETURN "eCLIENTDEBUGBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC

FUNC STRING GET_LOCAL_DEBUG_BIT0_NAME(eLOCAL_DEBUG_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW	RETURN "eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW"
		CASE eLOCALDEBUGBITSET0_END						RETURN "eLOCALBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC

FUNC BOOL IS_SERVER_DEBUG_BIT0_SET(eSERVER_DEBUG_BITSET_0 eBitset)
	RETURN IS_BIT_SET(serverBD.iDebugBitset, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_SERVER_DEBUG_BIT0(eSERVER_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)
	
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(serverBD.iDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_SERVER_DEBUG_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] GET_SERVER_DEBUG_BIT0_NAME - ", strTemp)
		ENDIF
	ENDIF
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SCRIPT_ASSERT("[BIKER1] calling SET_SERVER_DEBUG_BIT0 on a non host.")
		PRINTLN("[BIKER1] calling SET_SERVER_DEBUG_BIT0 on a non host.")
		EXIT
	ENDIF
	
	SET_BIT(serverBD.iDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_SERVER_DEBUG_BIT0(eCLIENT_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)

	IF NOT bBlockLogPrints
		IF IS_BIT_SET(serverBD.iDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_SERVER_DEBUG_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] CLEAR_SERVER_DEBUG_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	
	CLEAR_BIT(serverBD.iDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC



FUNC BOOL IS_CLIENT_DEBUG_BIT0_SET(PARTICIPANT_INDEX partId, eCLIENT_DEBUG_BITSET_0 eBitset)
	RETURN IS_BIT_SET(playerBD[NATIVE_TO_INT(partId)].iDebugBitset, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_CLIENT_DEBUG_BIT0(eCLIENT_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)

	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_CLIENT_DEBUG_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] SET_CLIENT_DEBUG_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	
	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_CLIENT_DEBUG_BIT0(eCLIENT_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)

	IF NOT bBlockLogPrints
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_CLIENT_DEBUG_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] CLEAR_CLIENT_DEBUG_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	
	CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

FUNC BOOL IS_LOCAL_DEBUG_BIT0_SET(eLOCAL_DEBUG_BITSET_0 eBitset)
	RETURN IS_BIT_SET(iLocalDebugBitset, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_LOCAL_DEBUG_BIT0(eLOCAL_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)

	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(iLocalDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_LOCAL_DEBUG_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] SET_LOCAL_DEBUG_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	
	SET_BIT(iLocalDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_LOCAL_DEBUG_BIT0(eLOCAL_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)

	IF NOT bBlockLogPrints
		IF IS_BIT_SET(iLocalDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_LOCAL_BIT0_NAME(eBitset)
			PRINTLN("[BIKER1] CLEAR_LOCAL_DEBUG_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	
	CLEAR_BIT(iLocalDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

#ENDIF

FUNC STRING GET_GAME_STATE_NAME(INT iState)

	SWITCH(iState)
		CASE GAME_STATE_INIT 				RETURN "GAME_STATE_INIT"
		CASE GAME_STATE_RUNNING 			RETURN "GAME_STATE_RUNNING"
		CASE GAME_STATE_TERMINATE_DELAY 	RETURN "GAME_STATE_TERMINATE_DELAY"
		CASE GAME_STATE_END 				RETURN "GAME_STATE_END"
	ENDSWITCH

	RETURN "***INVALID***"

ENDFUNC

//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_GAME_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iClientGameState
ENDFUNC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_GAME_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

//Helper function to set a clients game/mission state
PROC SET_CLIENT_GAME_STATE(INT iPlayer, INT iState)
	playerBD[iPlayer].iClientGameState = iState
	PRINTLN("[BIKER1] SET_CLIENT_GAME_STATE = ",GET_GAME_STATE_NAME(iState))
ENDPROC

//Helper function to set the servers game/mission state
PROC SET_SERVER_GAME_STATE(INT iState)
	serverBD.iServerGameState = iState
	PRINTLN("[BIKER1] SET_SERVER_GAME_STATE = ",GET_GAME_STATE_NAME(iState))
ENDPROC

#IF IS_DEBUG_BUILD 
FUNC STRING GET_MODe_STATE_NAME(eMODE_STATE eState)
	
	SWITCH eState
		CASE eMODESTATE_INIT	RETURN "eMODESTATE_INIT"
		CASE eMODESTATE_RUN		RETURN "eMODESTATE_RUN"
		CASE eMODESTATE_REWARDS	RETURN "eMODESTATE_REWARDS"
		CASE eMODESTATE_END		RETURN "eMODESTATE_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC
#ENDIF

FUNC eMODE_STATE GET_MODE_STATE()
	RETURN serverBD.eModeState
ENDFUNC

PROC SET_MODE_STATE(eMODE_STATE eState)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
	
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 errorMsg = "[BIKER1] - Client set mode state. Client = "
		errorMsg += PARTICIPANT_ID_TO_INT()
		SCRIPT_ASSERT(errorMsg)
		#ENDIF
		
		EXIT
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF eState != GET_MODE_STATE()
		STRING strStateName = GET_MODe_STATE_NAME(eState)
		PRINTLN("[BIKER1] - mode state going to ", strStateName)
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	serverBD.eModeState = eState	
	
ENDPROC

#IF IS_DEBUG_BUILD 
FUNC STRING GET_END_REASON_NAME(eEND_REASON eEndReason)
	
	SWITCH eEndReason
		CASE eENDREASON_NO_REASON_YET						RETURN "eENDREASON_NO_REASON_YET"
		CASE eENDREASON_TIME_UP								RETURN "TIME_UP"
		CASE eENDREASON_NO_BOSS_LEFT						RETURN "NO_BOSS_LEFT"
		CASE eENDREASON_ALL_GOONS_LEFT						RETURN "ALL_GOONS_LEFT"
		CASE eENDREASON_WIN_CONDITION_TRIGGERED				RETURN "eENDREASON_WIN_CONDITION_TRIGGERED"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC
#ENDIF

FUNC eEND_REASON GET_END_REASON()
	RETURN serverBD.eEndReason
ENDFUNC

PROC SET_END_REASON(eEND_REASON eEndReason)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
	
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 errorMsg = "[BIKER1] - Client set end reason. Client = "
		errorMsg += PARTICIPANT_ID_TO_INT()
		SCRIPT_ASSERT(errorMsg)
		#ENDIF
		
		EXIT
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF eEndReason != GET_END_REASON()
		STRING strReason = GET_END_REASON_NAME(eEndReason)
		PRINTLN("[BIKER1] - set end reason to ", strReason)
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	serverBD.eEndReason = eEndReason	
	
ENDPROC

// Local Help Text Bitset ------------------------------------------->|

FUNC BOOL IS_LOCAL_HELPTEXT_BIT0_SET(HELP_TEXT_ENUM eHelp)
	RETURN IS_BIT_SET(iLocalHelpBitset0, ENUM_TO_INT(eHelp))
ENDFUNC

PROC SET_LOCAL_HELPTEXT_BIT0(HELP_TEXT_ENUM eHelp #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(iLocalHelpBitset0, ENUM_TO_INT(eHelp))
			PRINTLN("[EXEC1] [TODO: ADD MODE NAME]  SET_LOCAL_HELPTEXT_BIT0 - ", ENUM_TO_INT(eHelp))
		ENDIF
	ENDIF
	#ENDIF
	
	SET_BIT(iLocalHelpBitset0, ENUM_TO_INT(eHelp))
	
ENDPROC

PROC CLEAR_LOCAL_HELPTEXT_BIT0(HELP_TEXT_ENUM eHelp #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF IS_BIT_SET(iLocalHelpBitset0, ENUM_TO_INT(eHelp))
			PRINTLN("[EXEC1] [TODO: ADD MODE NAME]  CLEAR_LOCAL_HELPTEXT_BIT0 - ", ENUM_TO_INT(eHelp))
		ENDIF
	ENDIF
	#ENDIF
	
	CLEAR_BIT(iLocalHelpBitset0, ENUM_TO_INT(eHelp))
	
ENDPROC

// ------------------------------------------------------------------|<

// Local Big Message Bitset ------------------------------------------->|

FUNC BOOL IS_LOCAL_BIGMESSAGE_BIT0_SET(BIG_MESSAGE_ENUM eBigM)
	RETURN IS_BIT_SET(iLocalBigMessageBitset0, ENUM_TO_INT(eBigM))
ENDFUNC

PROC SET_LOCAL_BIGMESSAGE_BIT0(BIG_MESSAGE_ENUM eBigM #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(iLocalBigMessageBitset0, ENUM_TO_INT(eBigM))
			PRINTLN("[EXEC1] [TODO: ADD MODE NAME]  SET_LOCAL_BIGMESSAGE_BIT0 - ", ENUM_TO_INT(eBigM))
		ENDIF
	ENDIF
	#ENDIF
	
	SET_BIT(iLocalBigMessageBitset0, ENUM_TO_INT(eBigM))
	
ENDPROC

PROC CLEAR_LOCAL_BIGMESSAGE_BIT0(BIG_MESSAGE_ENUM eBigM #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF IS_BIT_SET(iLocalBigMessageBitset0, ENUM_TO_INT(eBigM))
			PRINTLN("[EXEC1] [TODO: ADD MODE NAME]  CLEAR_LOCAL_BIGMESSAGE_BIT0 - ", ENUM_TO_INT(eBigM))
		ENDIF
	ENDIF
	#ENDIF
	
	CLEAR_BIT(iLocalBigMessageBitset0, ENUM_TO_INT(eBigM))
	
ENDPROC

// ------------------------------------------------------------------|<

#IF IS_DEBUG_BUILD
PROC MAINTAIN_J_SKIPS()
	
	IF GET_SERVER_GAME_STATE() = GAME_STATE_RUNNING
		IF NOT IS_CLIENT_DEBUG_BIT0_SET(PARTICIPANT_ID(), eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
			IF IS_DEBUG_KEY_JUST_RELEASED(KEY_S, KEYBOARD_MODIFIER_NONE, "S Pass")
				PRINTLN("[BIKER1] KEY_S, KEYBOARD_MODIFIER_NONE just released")
				SET_CLIENT_DEBUG_BIT0(eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
			ENDIF
		ENDIF
		
		IF NOT IS_CLIENT_DEBUG_BIT0_SET(PARTICIPANT_ID(), eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
			IF IS_DEBUG_KEY_JUST_RELEASED(KEY_F, KEYBOARD_MODIFIER_NONE, "F Fail")
				PRINTLN("[BIKER1] KEY_F, KEYBOARD_MODIFIER_NONE just released")
				SET_CLIENT_DEBUG_BIT0(eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED)
			ENDIF
		ENDIF
		IF IS_DEBUG_KEY_JUST_RELEASED(KEY_J, KEYBOARD_MODIFIER_NONE, "j Skip")
				
			IF playerBD[PARTICIPANT_ID_TO_INT()].this_player_on_any_bike = FALSE
				IF NET_WARP_TO_COORD(GET_STEAL_BIKE_COORDS(), 0, TRUE, FALSE)
					PRINTLN("[BIKER_STEAL] KEY_J, KEYBOARD_MODIFIER_NONE just released")
				ENDIF
			ELSE
				IF NET_WARP_TO_COORD(GET_SAFEHOUSE_COORDS(), 200, TRUE, FALSE)
					PRINTLN("[BIKER_STEAL] KEY_J, KEYBOARD_MODIFIER_NONE just released")
				ENDIF
			ENDIF
				
		ENDIF
		
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF serverBD.iSPassPart > (-1)
				PRINTLN("[BIKER1] serverBD.iSPassPart > (-1)")
				IF GET_MODE_STATE() < eMODESTATE_REWARDS
					SET_END_REASON(eENDREASON_TIME_UP)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ELIF serverBD.iFFailPart > (-1)
				PRINTLN("[BIKER1] serverBD.iFFailPart > (-1)")
				IF GET_MODE_STATE() < eMODESTATE_REWARDS
					SET_END_REASON(eENDREASON_TIME_UP)
					SET_MODE_STATE(eMODESTATE_REWARDS)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF




// Help text

/// PURPOSE:
///    Check if its safe to display help text on screen
/// RETURNS:
///    
FUNC BOOL IS_SAFE_FOR_HELP_TEXT()
	IF NOT IS_CUSTOM_MENU_ON_SCREEN()
	AND NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_RADAR_HIDDEN()
	AND IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Display help text to the screen
/// PARAMS:
///    thisHelpText - 
PROC DO_HELP_TEXT(HELP_TEXT_ENUM thisHelpText)
	IF IS_SAFE_FOR_HELP_TEXT()
		IF NOT IS_LOCAL_HELPTEXT_BIT0_SET(thisHelpText)
			SWITCH thisHelpText
				DEFAULT
				
				BREAK
			ENDSWITCH
			
			SET_LOCAL_HELPTEXT_BIT0(thisHelpText)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_SAFE_FOR_BIG_MESSAGE()	

	IF  NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_CUSTOM_MENU_ON_SCREEN()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Prints information about the big message that has just been requested to draw
/// PARAMS:
///    iBigMessage - the index of the message that was drawn
///    title - the title of the message
///    strapline - the subtitle of the message
PROC PRINT_BIG_MESSAGE_DEBUG_INFORMATION(INT iBigMessage, STRING title, STRING strapline)
	PRINTLN("[BIKER1] [TODO: ADD MODE NAME] - ************ NEW SHARD HAS BEEN SETUP *************")
	PRINTLN("[BIKER1] [TODO: ADD MODE NAME] - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Big Message:	", iBigMessage)
	PRINTLN("[BIKER1] [TODO: ADD MODE NAME] - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Title: 			", GET_FILENAME_FOR_AUDIO_CONVERSATION(title))
	PRINTLN("[BIKER1] [TODO: ADD MODE NAME] - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Strapline:		", GET_FILENAME_FOR_AUDIO_CONVERSATION(strapline))
	PRINTLN("[BIKER1] [TODO: ADD MODE NAME] - ***************************************************")
ENDPROC
#ENDIF

/// PURPOSE:
///    Function that handles the drawing of the modes shards
/// PARAMS:
///    thisMessage - 
PROC DO_BIG_MESSAGE(BIG_MESSAGE_ENUM thisMessage)
	IF IS_SAFE_FOR_BIG_MESSAGE()
		IF NOT IS_LOCAL_BIGMESSAGE_BIT0_SET(thisMessage)
					
			SWITCH thisMessage
				DEFAULT
					
				BREAK
			ENDSWITCH
			
			SET_LOCAL_BIGMESSAGE_BIT0(thisMessage)
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_END_REASON_AS_TELEMTRY_TYPE(eEND_REASON eEndReason, BOOL bIWon, BOOL bNobodyWon, BOOL bForcedEndOfMode)
	
	IF bForcedEndOfMode
		PRINTLN("[BIKER1] - GET_END_REASON_AS_TELEMTRY_TYPE - bExternallyForcedEndOfMode = TRUE, returning GB_TELEMETRY_END_FORCED")
		RETURN GB_TELEMETRY_END_FORCED
	ENDIF
	
	SWITCH eEndReason
		
		CASE eENDREASON_NO_BOSS_LEFT
			PRINTLN("[BIKER1] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_NO_BOSS_LEFT, returning GB_TELEMETRY_END_BOSS_LEFT")
			RETURN GB_TELEMETRY_END_BOSS_LEFT	
		
		CASE eENDREASON_ALL_GOONS_LEFT	
			PRINTLN("[BIKER1] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_ALL_GOONS_LEFT, returning GB_TELEMETRY_END_LOW_NUMBERS")
			RETURN GB_TELEMETRY_END_LOW_NUMBERS
		
		CASE eENDREASON_TIME_UP
		CASE eENDREASON_WIN_CONDITION_TRIGGERED
			IF bIWon	
				PRINTLN("[BIKER1] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_TIME_UP and bIWon = TRUE, returning GB_TELEMETRY_END_WON")
				RETURN GB_TELEMETRY_END_WON		
			ELIF bNobodyWon
				PRINTLN("[BIKER1] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_TIME_UP and bNobodyWon = TRUE, returning GB_TELEMETRY_END_TIME_OUT")
				RETURN GB_TELEMETRY_END_TIME_OUT	
			ELSE
				PRINTLN("[BIKER1] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_TIME_UP and bWon = FALSE and bNobodyWon = FALSE, returning GB_TELEMETRY_END_LOST")
				RETURN GB_TELEMETRY_END_LOST
			ENDIF
		BREAK
		
	ENDSWITCH
	
	PRINTLN("[BIKER1] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - all other telemetry type checks failed, must be leaving of own choice, returning GB_TELEMETRY_END_LEFT")
	RETURN GB_TELEMETRY_END_LEFT
	
ENDFUNC

PROC EXECUTE_COMMON_END_TELEMTRY(BOOL bIwon, BOOL bForcedEndOfMode)
	
	IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_EXECUTED_END_TELEMTRY)
		
		BOOL bNobodyWon // Set based on mode specific logic.
		INT iEndType = GET_END_REASON_AS_TELEMTRY_TYPE(GET_END_REASON(), bIWon, bNobodyWon, bForcedEndOfMode)
		
		PRINTLN("[BIKER1] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - bForcedEndOfMode = ", bForcedEndOfMode)
		PRINTLN("[BIKER1] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - bIWon = ", bIWon)
		PRINTLN("[BIKER1] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - bNobodyWon = ", bNobodyWon)
		PRINTLN("[BIKER1] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - iEndType = ", iEndType)
		
		g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchId1
		g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchId2
		IF  IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALLED_START_COMMON_TELEMETRY)
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(bIWon, iEndType)
		ENDIF
		PRINTLN("[BIKER1] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - called GB_SET_COMMON_TELEMETRY_DATA_ON_END(", bIWon,", ", iEndType, ")")
		
		SET_LOCAL_BIT0(eLOCALBITSET0_EXECUTED_END_TELEMTRY)
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    This should only handle end shard / UI since it can be suppressed by cutscenes etc.
PROC DISPLAY_END_SHARD()
	IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
		IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
		AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
		// AND NOT GB_SHOULD_HIDE_GANG_BOSS_EVENT(FMMC_TYPE_GB_CHAL_CARJACKING) - add your mode here.
		
			IF IS_SAFE_FOR_BIG_MESSAGE()
			
				TEXT_LABEL tl15End
				TEXT_LABEL tl15descrip
			
				SWITCH GET_END_REASON()
					CASE eENDREASON_NO_BOSS_LEFT
						PRINTLN("[BIKER_STEAL] - Shard - boss left")
						CLEAR_HELP()
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_CHAL_OVER", "LR_BOSSLEFT")
					BREAK
					CASE eENDREASON_ALL_GOONS_LEFT
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_CHAL_OVER", "LR_BOSSLEFT")
						PRINTLN("[BIKER_STEAL] - Shard - no goons left")
						CLEAR_HELP()
					BREAK
					CASE eENDREASON_TIME_UP
						PRINTLN("[BIKER_STEAL] - Shard - mode time expired")
						CLEAR_HELP()
						IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
						OR AM_I_THE_LAUNCH_BOSS()
						//OR rivalcantakevanback = TRUE
						OR additionalrival_objective_text = TRUE
							tl15End = "GB_UNLOADLOSET"
							PRINTLN("[BIKER_STEAL] - Shard - mode time expired show shard")
							CLEAR_HELP()
							SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "BK_OVER", tl15End,  GB_GET_ORGANIZATION_NAME_AS_A_STRING(), GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
						ENDIF
					BREAK
					
					CASE eENDREASON_BIKES_DESTROYED
						PRINTLN("[BIKER1] - Shard - Bikes destroyed")
						CLEAR_HELP()
						IF serverBD.numberofvehiclescreated  = 1
							tl15End = "GB_U_STEALLOSEV"
						ELSE
							tl15End = "GB_U_STEALOSEVs"
						ENDIF
						IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
						OR AM_I_THE_LAUNCH_BOSS()
							SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "BK_OVER", tl15End,  GB_GET_ORGANIZATION_NAME_AS_A_STRING(), GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
						ELSE
							IF playerBD[PARTICIPANT_ID_TO_INT()].numbikesdestroyed > 0	
								SETUP_NEW_BIG_MESSAGE_WITH_2_INTS(BIG_MESSAGE_GB_END_OF_JOB_OVER,playerBD[PARTICIPANT_ID_TO_INT()].numbikesdestroyed, serverBD.numberofvehiclescreated , "GB_STEAL_GENWU", DEFAULT, DEFAULT, DEFAULT , "BK_WINNER")
							ELSE
								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "BK_OVER", "GB_STEAL_WINe0")
							ENDIF
						ENDIF							
					BREAK
					CASE eENDREASON_WIN_CONDITION_TRIGGERED
						IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
						OR AM_I_THE_LAUNCH_BOSS()
							tl15End = "GB_STEAL_WIN"
							tl15descrip = "BK_WINNER"
							IF GET_NUMBER_OF_BIKES_DELIVERED() > 1
								tl15End = "GB_STEAL_WINs"
							ENDIF									
							IF GET_NUMBER_OF_BIKES_DESTROYED()> 1
								tl15descrip = "BK_OVER"
							ENDIF
							IF serverBD.numberofvehiclescreated > 1
								SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS,tl15descrip, tl15End, GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_LAUNCH_BOSS_PLAYER_INDEX()) , GB_GET_PLAYER_GANG_HUD_COLOUR(GET_LAUNCH_BOSS_PLAYER_INDEX()), "", GET_NUMBER_OF_BIKES_DELIVERED(), serverBD.numberofvehiclescreated )
							ELSE
								SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS,tl15descrip, tl15End, GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(GET_LAUNCH_BOSS_PLAYER_INDEX()) , GB_GET_PLAYER_GANG_HUD_COLOUR(GET_LAUNCH_BOSS_PLAYER_INDEX()), DEFAULT )
							ENDIF
						ELSE
							IF additionalrival_objective_text = TRUE
								tl15End = "GB_STEAL_WINe"
								IF serverBD.numberofvehiclescreated > 1
									tl15End = "GB_STEAL_WINes"
								ENDIF
								IF additionalrival_objective_text
									// SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "BK_OVER", tl15End,        GB_GET_ORGANIZATION_NAME_AS_A_STRING(),                                         GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()), DEFAULT, GET_NUMBER_OF_BIKES_DELIVERED())
									IF playerBD[PARTICIPANT_ID_TO_INT()].numbikesdestroyed > 0	
										SETUP_NEW_BIG_MESSAGE_WITH_2_INTS(BIG_MESSAGE_GB_END_OF_JOB_OVER,playerBD[PARTICIPANT_ID_TO_INT()].numbikesdestroyed, serverBD.numberofvehiclescreated , "GB_STEAL_GENWU", DEFAULT, DEFAULT, DEFAULT , "BK_WINNER")
											
									ELSE
											
										
										SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "BK_OVER", "GB_STEAL_WINe0")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ELSE
				PRINTLN("[BIKER1] [BIKER_STEAL] IS_SAFE_FOR_BIG_MESSAGE() = FALSE")
			ENDIF
		ELSE
			PRINTLN("[BIKER1] [BIKER_STEAL] Local player restricted from UI")
		ENDIF
	ELSE
		PRINTLN("[BIKER1] [BIKER_STEAL] MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI = FALSE")	
	ENDIF
ENDPROC


PROC HANDLE_REWARDS()
	
	BOOL bIWon
	//TEXT_LABEL musictoplaystaelbikes

	IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALCULATED_REWARDS)
		
		IF GET_END_REASON() != eENDREASON_NO_REASON_YET
		
			// Attempt to display the end messages
			DISPLAY_END_SHARD()						
			
			// Handle the cend of the mode and the rewards
			SWITCH GET_END_REASON()
				CASE eENDREASON_NO_BOSS_LEFT
					PRINTLN("[BIKER_STEAL] - no boss left on script.")
					IF NOT GB_HAS_COUNTDOWN_MUSIC_STARTED(cmStruct)
						TRIGGER_MUSIC_EVENT(  "BIKER_MP_MUSIC_FAIL")
					ENDIF					
				BREAK
				CASE eENDREASON_ALL_GOONS_LEFT
					PRINTLN("[BIKER_STEAL] - no goons left on script")
					IF NOT GB_HAS_COUNTDOWN_MUSIC_STARTED(cmStruct)
						TRIGGER_MUSIC_EVENT(  "BIKER_MP_MUSIC_FAIL")
					ENDIF
				BREAK
				CASE eENDREASON_TIME_UP
					PRINTLN("[BIKER_STEAL] - mode time expired")
					IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
					OR AM_I_THE_LAUNCH_BOSS()
					//OR rivalcantakevanback = TRUE
					OR additionalrival_objective_text = TRUE
						bIWon = FALSE
						PRINTLN("[BIKER_STEAL] - mode time expired")
						IF AM_I_IN_LAUNCH_GANG()
							GB_SET_LOCAL_BIKER_CALL_BIT(eGB_LOCAL_BIKER_CALL_BITSET_NOTIFICATION_FAIL_CLUB_MISSION)
						ENDIF
					ENDIF
					
					IF NOT GB_HAS_COUNTDOWN_MUSIC_STARTED(cmStruct)
						TRIGGER_MUSIC_EVENT( "BIKER_MP_MUSIC_FAIL")
					ENDIF
				BREAK
				
				CASE eENDREASON_BIKES_DESTROYED
					PRINTLN("[BIKER1] - Bikes destroyed")
					bIWon = FALSE
					IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
					OR AM_I_THE_LAUNCH_BOSS()
						bIWon = FALSE
						
						IF NOT GB_HAS_COUNTDOWN_MUSIC_STARTED(cmStruct)
							TRIGGER_MUSIC_EVENT(  "BIKER_MP_MUSIC_FAIL")							
						ENDIF
						
						IF AM_I_IN_LAUNCH_GANG()
							GB_SET_LOCAL_BIKER_CALL_BIT(eGB_LOCAL_BIKER_CALL_BITSET_NOTIFICATION_FAIL_CLUB_MISSION)
						ENDIF				
					ELSE
						IF NOT GB_HAS_COUNTDOWN_MUSIC_STARTED(cmStruct)
							TRIGGER_MUSIC_EVENT(  "BIKER_MP_MUSIC_STOP")						
						ENDIF
						IF  additionalrival_objective_text = TRUE
							bIWon = TRUE
						ENDIF
					ENDIF				
				BREAK
				
				CASE eENDREASON_WIN_CONDITION_TRIGGERED

					IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
					OR AM_I_THE_LAUNCH_BOSS()
					
						bIWon = TRUE
						
						IF NOT GB_HAS_COUNTDOWN_MUSIC_STARTED(cmStruct)
							TRIGGER_MUSIC_EVENT( "BIKER_MP_MUSIC_STOP")
						ENDIF
						
						IF AM_I_IN_LAUNCH_GANG()
							GB_SET_LOCAL_BIKER_CALL_BIT(eGB_LOCAL_BIKER_CALL_BITSET_NOTIFICATION_PASS_CLUB_MISSION)
						ENDIF
					ELSE
						IF additionalrival_objective_text = TRUE
							bIWon = FALSE
							IF additionalrival_objective_text
								
								IF AM_I_IN_LAUNCH_GANG()
									GB_SET_LOCAL_BIKER_CALL_BIT(eGB_LOCAL_BIKER_CALL_BITSET_NOTIFICATION_FAIL_CLUB_MISSION)
								ENDIF
								
								IF NOT GB_HAS_COUNTDOWN_MUSIC_STARTED(cmStruct)
									TRIGGER_MUSIC_EVENT( "BIKER_MP_MUSIC_FAIL")
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[BIKER1] [BIKER_STEAL] REWARDS Rival gets nothing tell_rivals_to_kill_players = FALSE ")
						ENDIF
					ENDIF
					
					PRINTLN("[BIKER_STEAL] - somebody won")
				BREAK
			ENDSWITCH
						
			CLEAR_HELP()
			REMOVE_ALL_BLIPS()
			IF NOT GB_HAS_COUNTDOWN_MUSIC_STARTED(cmStruct)
				TRIGGER_MUSIC_EVENT( "BIKER_MP_MUSIC_STOP")
			ENDIF
			PLAYER_INDEX playerId = PLAYER_ID() // Set the winning player here before passing into wanted cleanup function. Using PLAYER_ID() as placeholder right now.
			GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, playerId)
			
			IF bIWon
				INT iRPGiven

				INT icashGiven
				
			
				// Do any extra processing here for if the locla player won, before passing into GANG_BOSS_MANAGE_REWARDS
				// sGangBossManageRewardsData etc.
				NEXT_RP_ADDITION_SHOW_RANKBAR()
				IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
				OR AM_I_THE_LAUNCH_BOSS()
					iRPGiven = GET_NUMBER_OF_DELIVERED_BIKES() *  g_sMPTunables.iBIKER_NINE_TENTHS_RP_REWARD 
					//GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "", XPTYPE_SKILL,XPCATEGORY_COLLECT_CHECKPOINT, iRPGiven)
					icashGiven = GET_NUMBER_OF_DELIVERED_BIKES() * g_sMPTunables.iBIKER_NINE_TENTHS_CASH_REWARD
				ELSE
					iRPGiven = GET_NUMBER_OF_DESTROYED_BIKES() *  g_sMPTunables.iBIKER_NINE_TENTHS_RP_REWARD 
					//GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "", XPTYPE_SKILL,XPCATEGORY_COLLECT_CHECKPOINT, iRPGiven)
					icashGiven = GET_NUMBER_OF_DESTROYED_BIKES() *g_sMPTunables.iBIKER_NINE_TENTHS_CASH_REWARD
				ENDIF
				PRINTLN("[BIKER_STEAL] HANDLE_REWARDS icashGiven =   ", icashGiven)
				PRINTLN("[BIKER_STEAL] HANDLE_REWARDS iRPGiven =   ", iRPGiven)
				sGangBossManageRewardsData.iExtraCash	= icashGiven
				sGangBossManageRewardsData.iExtraRp		= iRPGiven
			ENDIF

			
			GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_BIKER_STEAL_BIKES, bIWon, sGangBossManageRewardsData)
			
			EXECUTE_COMMON_END_TELEMTRY(bIWon, FALSE)
			
			SET_LOCAL_BIT0(eLOCALBITSET0_CALCULATED_REWARDS)
			
		ENDIF
	
	ENDIF
	
	IF GB_MAINTAIN_BOSS_END_UI(sEndUI, FALSE)
		SET_CLIENT_BIT0(eCLIENTBITSET0_COMPLETED_REWARDS)
	ENDIF
	
ENDPROC

// UI Maintains ----------------------------------------------------->|

/// PURPOSE:
///    Maintain the blips on entities and vehicles
PROC MAINTAIN_BLIPS()

ENDPROC

//Draws a marker above a vehicle so the arrow doesn't clip into it
PROC DRAW_MARKER_ABOVE_ENTITY(ENTITY_INDEX ent, INT r, INT g, INT b)
	VECTOR returnMin, returnMax
	FLOAT fOffset
	
	fOffset = 0.5 
	
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(ent), returnMin, returnMax)

	FLOAT fCentreHeight = (returnMax.z - returnMin.z)/2
	FLOAT fZdiff = returnMax.z - fCentreHeight
	
	//-- Dave W - z-offset from centre needs to be at least the max height of the vehicle.
	IF fOffset <= (fZdiff + 0.1)
		fOffset = fZdiff + 0.4
	ENDIF
	
	DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(ent)+<<0,0,((returnMax.z - returnMin.z)/2) + fOffset>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, r, g, b, 100, TRUE, TRUE)
ENDPROC
PROC SUPRESS_WANTED_LEVEL()
	IF set_player_end_wanted_supression = FALSE
		IF  serverBD.mission_variation	=VARIATIONS_POLICE_BIKES
		
			IF  playerBD[PARTICIPANT_ID_TO_INT()].this_player_on_any_bike  = TRUE
			OR IS_PLAYER_NEAR_ANY_OF_THE_BIKES(PLAYER_ID(), 30.0)	
				set_player_end_wanted_supression = TRUE
				SET_MAX_WANTED_LEVEL(2)
				SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 2)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				PRINTLN("Player given wanted level")
				MAKE_ENEMY_PEDS_KICK_OFF()
			ENDIF
		ENDIF
	ENDIF	
	
	IF set_player_end_wanted_supression = FALSE
		IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
		OR AM_I_THE_LAUNCH_BOSS()
		//OR tell_rivals_to_kill_players //AND IS_PLAYER_NEAR_ANY_OF_THE_BIKES(player_id()))
			IF GET_MAX_WANTED_LEVEL() != 0
				SET_MAX_WANTED_LEVEL(0)
			ENDIF
		ELSE
			IF  additionalrival_objective_text
			AND IS_PLAYER_NEAR_ANY_OF_THE_BIKES(PLAYER_ID())
				IF GET_MAX_WANTED_LEVEL() != 0
					SET_MAX_WANTED_LEVEL(0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC
PROC MAINTAIN_MARKERS()
	
ENDPROC

/// PURPOSE:
///    Maintain the objective text for the mode
PROC MAINTAIN_OBJECTIVE_TEXT()
		

ENDPROC

/// PURPOSE:
///    Maintain the bottom right UI (event timers, kill/cash counts etc.)
PROC MAINTAIN_BOTTOM_RIGHT_UI()
	
	
	// Handle timers, counters etc
	IF HAS_NET_TIMER_STARTED(serverBD.modeTimer)
			
		iTimeRemaining = (GET_MODE_TUNEABLE_TIME_LIMIT() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.modeTimer))
		
		IF iTimeRemaining < GET_MODE_TUNEABLE_TIME_LIMIT()
		
			HUD_COLOURS timeColour
			IF iTimeRemaining > 30000
				timeColour = HUD_COLOUR_WHITE
			ELSE
				timeColour = HUD_COLOUR_RED
			ENDIF
			
			SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
		
			IF iTimeRemaining > 0
				DRAW_GENERIC_TIMER(iTimeRemaining, "SCONTRA_TIMER", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, timeColour)
			ELSE
				DRAW_GENERIC_TIMER(0, "SCONTRA_TIMER", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, timeColour)
			ENDIF
			
		ENDIF
	ENDIF	
ENDPROC

/// PURPOSE:
///    Maintain the UI of the boss work
PROC MAINTAIN_UI()

	IF AM_I_IN_LAUNCH_GANG()
	OR additionalrival_objective_text
		IF AM_I_IN_LAUNCH_GANG()
			MAINTAIN_EXIT_CLUBHOUSE_FOR_CLUBHOUSE_MISSION()
		ENDIF
		DRAW_BOTTOM_RIGHT_HUD()
		IF additionalrival_objective_text = TRUE
		OR AM_I_IN_LAUNCH_GANG()
			MAINTAIN_BOTTOM_RIGHT_UI()
		ENDIF

		IF HAS_LOCAL_PLAYER_EXITED_CLUBHOUSE_FOR_CLUBHOUSE_MISSION()
		OR NOT IS_PLAYER_IN_CLUBHOUSE_PROPERTY(PLAYER_ID())
			MAINTAIN_BLIPS()
			MAINTAIN_MARKERS()
			MAINTAIN_OBJECTIVE_TEXT()
					
			IF AM_I_IN_LAUNCH_GANG()
				MAINTAIN_STEAL_BIKES_PHONECALLS()
			ENDIF
		
			
			IF bprinted_initial_help_at_start = FALSE
				IF PRINT_THE_INITIAL_HELP_TEXT()
					bprinted_initial_help_at_start = TRUE
				ENDIF
			ENDIF
			
			IF bprinted_help_at_start = FALSE
				IF PRINT_THE_STARTING_HELP_TEXT()
					bprinted_help_at_start = TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		SWITCH GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())
			CASE GB_UI_LEVEL_NONE
				CLEAR_ANY_OBJECTIVE_TEXT_FROM_THIS_SCRIPT()
			BREAK
			
			CASE GB_UI_LEVEL_BACKGROUND
			CASE GB_UI_LEVEL_MINIMAL
				MAINTAIN_BLIPS()
			BREAK
			
			CASE GB_UI_LEVEL_FULL
				MAINTAIN_BLIPS()
				MAINTAIN_MARKERS()
				MAINTAIN_OBJECTIVE_TEXT()
				MAINTAIN_BOTTOM_RIGHT_UI()
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC CLIENT_PROCESSING()
	INT i
	
	SWITCH GET_MODE_STATE()
	
		CASE eMODESTATE_INIT
			// ********************************************************************
			// Server says when omode has intialised and moves us onto run state.
			// ********************************************************************
		BREAK
		
		CASE eMODESTATE_RUN
			
			// ***********************
			// Do common start calls
			// ***********************
			IF bsetallbikesasvunerable = FALSE
				IF serverbd.bmissionhasgoneglobal	= TRUE
					bsetallbikesasvunerable = TRUE
					SET_ALL_BIKES_AS_VULNERABLE()
				ENDIF
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(serverBD.modeTimer)
				GB_MAINTAIN_COUNTDOWN_MUSIC((GET_MODE_TUNEABLE_TIME_LIMIT() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.modeTimer)), GET_END_REASON() != eENDREASON_NO_REASON_YET, cmStruct)
			ENDIF

			
			IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALLED_COMMON_SETUP)
				// GB_COMMON_BOSS_MISSION_SETUP(// Add mode here.)
				SET_LOCAL_BIT0(eLOCALBITSET0_CALLED_COMMON_SETUP)
			ELSE
				IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP)
					IF HAS_NET_TIMER_EXPIRED(serverBD.modeTimer, TRACK_GOON_DELAY)
						GB_SET_ITS_SAFE_FOR_SPEC_CAM()
						SET_LOCAL_BIT0(eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP)
					ENDIF
				ENDIF
			ENDIF
			
			// *******************************************
			// Main mode logic here, after setup is done
			// *******************************************
			IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
			OR AM_I_THE_LAUNCH_BOSS()	
			OR additionalrival_objective_text
				DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, 
							g_GBLeaderboardStruct.fmDpadStruct)
					
			ENDIF
			IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			// AND NOT GB_SHOULD_HIDE_GANG_BOSS_EVENT(// Add your mode here)
				IF blockeddoorsforenemyped
					IF NOT IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
					AND NOT AM_I_THE_LAUNCH_BOSS()	
						REPEAT serverBD.numberofvehiclescreated i
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[i])
							AND NOT IS_ENTITY_DEAD(NET_TO_veh(serverBD.serverbiketosteal[i]))
								

								IF  GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_veh(serverBD.serverbiketosteal[i]), PLAYER_ID())
									SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_veh(serverBD.serverbiketosteal[i]), PLAYER_ID(), TRUE)
									blockeddoorsforenemyped = FALSE
									PRINTLN("[BIKER_STEAL] unlocking bike")
								ENDIF
								GB_MAINTAIN_BOSS_WORK_DISTANT_CHECKS(FMMC_TYPE_BIKER_STEAL_BIKES, GET_ENTITY_COORDS(NET_TO_VEH(serverBD.serverbiketosteal[i]), FALSE), bSet)
							ENDIF
							
						ENDREPEAT
					ELSE
					
					
					ENDIF
				ENDIF
				
				
				IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
				OR AM_I_THE_LAUNCH_BOSS()		
					REPEAT serverBD.numberofvehiclescreated i
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[i])
						AND NOT IS_ENTITY_DEAD(NET_TO_veh(serverBD.serverbiketosteal[i]))
							GB_MAINTAIN_BOSS_WORK_DISTANT_CHECKS(FMMC_TYPE_BIKER_STEAL_BIKES, GET_ENTITY_COORDS(NET_TO_VEH(serverBD.serverbiketosteal[i]), FALSE), bSet)
						ENDIF
					ENDREPEAT
				ENDIF
				
				
				IF GET_END_REASON() = eENDREASON_NO_REASON_YET
					IF  IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
					OR  AM_I_THE_LAUNCH_BOSS()	
					OR  additionalrival_objective_text = TRUE
						SUPRESS_WANTED_LEVEL()
					ENDIF
					IF  IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
					OR  AM_I_THE_LAUNCH_BOSS()	
						GB_BIKER_RACE_MAINTAIN(sBikerRace, serverBD.sRaceServerVars)
					ENDIF
					IF playerBD[PARTICIPANT_ID_TO_INT()].this_player_in_deal_area = FALSE
						
						IF IS_THIS_PLAYER_NEAR_STEAL_AREA(PLAYER_ID())
							playerBD[PARTICIPANT_ID_TO_INT()].this_player_in_deal_area = TRUE
							IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
							OR AM_I_THE_LAUNCH_BOSS()
								playerBD[PARTICIPANT_ID_TO_INT()].this_player_in_deal_area_and_in_launch_gang= TRUE
							ENDIF
							PRINTLN("[BIKER_STEAL] Player has reached area")
							
							IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
								IF NOT  IS_TRANSITION_ACTIVE()
								AND  IS_SKYSWOOP_AT_GROUND() 
									TRIGGER_MUSIC_EVENT("BIKER_NINE_TENTHS_TUNNEL")
								ENDIF
							ENDIF
						ELSE
							IF  serverBD.mission_variation	= VARIATIONS_SEWER
								IF DOES_BLIP_EXIST(blip_area)
									DRAW_TUNNEL_MARKER()
								ENDIF
							ENDIF
						ENDIF
					//ELSE
						
					ENDIF
				
					IF TRIGGERENEMYAIMUSIC = FALSE
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),GETVEHICLESPAWN_COORDS(0), FALSE) < 100.0  
						AND ARE_GOONS_SHOOTING()
							TRIGGERENEMYAIMUSIC = TRUE
							IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
								IF NOT  IS_TRANSITION_ACTIVE()
								AND  IS_SKYSWOOP_AT_GROUND() 
									TRIGGER_MUSIC_EVENT("BIKER_NINE_TENTHS_ENEMIES")
								ENDIF
							ENDIF
							PRINTLN("[BIKER_STEAL] Goons are pissed off")
						ENDIF
					
					ENDIF
					
					IF TRIGGERENEMYAIMUSIC = TRUE
					
						MANAGE_ENEMY_AI_BLIPS()
					ENDIF
					
					//fixed 3006199 Gunrunning - Local player constantly has "Exit the Clubhouse" god text even after exiting the clubhouse
					IF NOT IS_PLAYER_IN_CLUBHOUSE_PROPERTY(PLAYER_ID())
						IF  playerBD[PARTICIPANT_ID_TO_INT()].this_player_in_deal_area = FALSE
						AND NOT IS_ANY_PLAYER_ON_A_STOLEN_BIKE()	
						AND NOT HAS_ANY_PLAYER_REACHED_STEAL_AREA_SERVER()
							IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
							OR AM_I_THE_LAUNCH_BOSS()
								IF NOT GB_IS_GLOBAL_CLIENT_BIT1_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_1_FINISHED_RACE)
								AND NOT GB_IS_GLOBAL_CLIENT_BIT1_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_1_KICKED_OFF_RACE)
								AND NOT GB_BIKER_RACE_IS_RACE_OVER(serverBD.sRaceServerVars)
									IF NOT Is_This_The_Current_Objective_Text("GB_STEALSTRAPr")
									AND NOT Is_This_The_Current_Objective_Text("GB_STEALSTRAPsr")
									AND NOT Is_This_The_Current_Objective_Text("GB_STEALSTRA1sr")
									AND NOT Is_This_The_Current_Objective_Text("GB_STEALSTRA1r")
									AND NOT Is_This_The_Current_Objective_Text("GB_STEALSTRA2sr")
									AND NOT Is_This_The_Current_Objective_Text("GB_STEALSTRA2r")
										IF GB_GET_NUM_GOONS_IN_LOCAL_GANG()+1 > 1
											IF serverBD.mission_variation	= VARIATIONS_SEWER
												Print_Objective_Text("GB_STEALSTRAPsr") 
											ELIF serverBD.mission_variation	= VARIATIONS_POLICE_BIKES 
												Print_Objective_Text("GB_STEALSTRA1sr") 
											ELIF serverBD.mission_variation	= VARIATIONS_FISHING_VILLAGE 
												Print_Objective_Text("GB_STEALSTRA2sr") 
											ENDIF
										ELSE
											IF serverBD.mission_variation	= VARIATIONS_SEWER
												Print_Objective_Text("GB_STEALSTRAPr") 
											ELIF serverBD.mission_variation	= VARIATIONS_POLICE_BIKES 
												Print_Objective_Text("GB_STEALSTRA1r") 
											ELIF serverBD.mission_variation	= VARIATIONS_FISHING_VILLAGE 
												Print_Objective_Text("GB_STEALSTRA2r") 
											ENDIF
										ENDIF
														
										PRINTLN("[BIKER_STEAL] PRINTING GB_UNLOAD_HELP")
									ENDIF
								ELSE		
									IF NOT Is_This_The_Current_Objective_Text("GB_STEALSTRAP")
									AND NOT Is_This_The_Current_Objective_Text("GB_STEALSTRAPs")
									AND NOT Is_This_The_Current_Objective_Text("GB_STEALSTRA1s")
									AND NOT Is_This_The_Current_Objective_Text("GB_STEALSTRA1")
									AND NOT Is_This_The_Current_Objective_Text("GB_STEALSTRA2s")
									AND NOT Is_This_The_Current_Objective_Text("GB_STEALSTRA2")
										IF GB_GET_NUM_GOONS_IN_LOCAL_GANG()+1 > 1
											IF serverBD.mission_variation	= VARIATIONS_SEWER
												Print_Objective_Text("GB_STEALSTRAPs") 
											ELIF serverBD.mission_variation	= VARIATIONS_POLICE_BIKES 
												Print_Objective_Text("GB_STEALSTRA1s") 
											ELIF serverBD.mission_variation	= VARIATIONS_FISHING_VILLAGE 
												Print_Objective_Text("GB_STEALSTRA2s") 
											ENDIF
										ELSE
											IF serverBD.mission_variation	= VARIATIONS_SEWER
												Print_Objective_Text("GB_STEALSTRAP") 
											ELIF serverBD.mission_variation	= VARIATIONS_POLICE_BIKES 
												Print_Objective_Text("GB_STEALSTRA1") 
											ELIF serverBD.mission_variation	= VARIATIONS_FISHING_VILLAGE 
												Print_Objective_Text("GB_STEALSTRA2") 
											ENDIF
										ENDIF
														
										PRINTLN("[BIKER_STEAL] PRINTING GB_UNLOAD_HELP")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF  playerBD[PARTICIPANT_ID_TO_INT()].this_player_in_deal_area = TRUE
					OR IS_ANY_PLAYER_ON_A_STOLEN_BIKE()
						IF NOT btriggerstolenbikemusic
							IF IS_ANY_PLAYER_ON_A_STOLEN_BIKE()
							
								IF serverBD.mission_variation	= VARIATIONS_POLICE_BIKES
									IF REQUEST_SCRIPT_AUDIO_BANK("ALARM_BELL_02")
										soundidforalarm = GET_SOUND_ID() 
										PLAY_SOUND_FROM_COORD(soundidforalarm,"Bell_02",GETVEHICLEPROPSPAWN_COORDS( 0 ),"ALARMS_SOUNDSET", TRUE, 50)
										btriggerstolenbikemusic = TRUE
										IF NOT  IS_TRANSITION_ACTIVE()
										AND  IS_SKYSWOOP_AT_GROUND() 
										TRIGGER_MUSIC_EVENT("BIKER_NINE_TENTHS_BIKE")
										ENDIF
									ENDIF
								ELSE
									
									IF NOT  IS_TRANSITION_ACTIVE()
									AND  IS_SKYSWOOP_AT_GROUND() 
										btriggerstolenbikemusic = TRUE
										TRIGGER_MUSIC_EVENT("BIKER_NINE_TENTHS_BIKE")
									ENDIF
								ENDIF
							
								
							ENDIF
						ENDIF

					ENDIF
					
					DEAL_WITH_BIKE_SAFEHOUSE_BLIPS()
					
					IF  playerBD[PARTICIPANT_ID_TO_INT()].this_player_on_any_bike  = TRUE
						IF makeplayerleavebike = FALSE
							IF IS_THIS_PLAYER_AT_SAFEHOUSE_UNLOAD(player_id())
								IF GET_BIKE_PLAYER_IS_ON() != -1
									IF MAKE_PLAYERS_LOCK_BIKE(GET_BIKE_PLAYER_IS_ON())
										TASK_LEAVE_ANY_VEHICLE(player_PED_id())
										makeplayerleavebike = TRUE
										PRINTLN("[BIKER_STEAL] LEaving bike")
										PLAY_SOUND_FRONTEND(-1, "Deliver_Item", "GTAO_Biker_Modes_Soundset", FALSE)
										TickerEventData.TickerEvent = TICKER_EVENT_BIKER_STEAL_DELIVERED_BIKE
										TickerEventData.playerID = PLAYER_ID()
										BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF GET_BIKE_PLAYER_IS_ON() != -1
							IF NOT bsetbikesasVULNERABLE[GET_BIKE_PLAYER_IS_ON()]
								IF SET_BIKES_AS_VULNERABLE(GET_BIKE_PLAYER_IS_ON())
									bsetbikesasVULNERABLE[GET_BIKE_PLAYER_IS_ON()] = TRUE	
								ENDIF
							ENDIF
						ENDIF	
					ELSE
						makeplayerleavebike = FALSE
					ENDIF
					
					IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
						IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALLED_START_COMMON_TELEMETRY)
							GB_SET_COMMON_TELEMETRY_DATA_ON_START()
							SET_LOCAL_BIT0(eLOCALBITSET0_CALLED_START_COMMON_TELEMETRY)
							PRINTLN("[BIKER_STEAL] - [TELEMETRY] - called GB_SET_COMMON_TELEMETRY_DATA_ON_START()")
						ENDIF
						
						// ********************************************
						// Run main mode logic here.
						// Remember to set the player as 
						// GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT() 
						// when appropriate.
						// ********************************************
						
						MAINTAIN_UI()
						
						IF iFocusParticipant > (-1)
							IF iFocusParticipant = PARTICIPANT_ID_TO_INT()
								// I am playing, not spectating.
							ELSE
								// I am spectating not playing.
							ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
				
			ELSE
				
				Clear_Any_Objective_Text_From_This_Script() // Clear objective text if player is retricted for hide or passive.
				
				// fix for 3014204 Make passive mode goons get off bike
				IF NOT blockeddoorsforenemyped
					IF NOT IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
					AND NOT AM_I_THE_LAUNCH_BOSS()	
					
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[0])
						AND NOT IS_ENTITY_DEAD(NET_TO_veh(serverBD.serverbiketosteal[0]))
							IF NOT GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_veh(serverBD.serverbiketosteal[0]), PLAYER_ID())
								SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_veh(serverBD.serverbiketosteal[0]), PLAYER_ID(), TRUE)
								blockeddoorsforenemyped = TRUE
								PRINTLN("[BIKER_STEAL] locking bike")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE eMODESTATE_REWARDS
			
			Clear_Any_Objective_Text_From_This_Script() // Clear god text, mode is over.
			HANDLE_REWARDS() // Handle end of mode rewards.
			REMOVE_ALL_BLIPS_FOR_BIKE()
			REMOVE_BLIP_FOR_AREA()
			REMOVE_ALL_BLIPS()
			IF IS_THIS_PLAYER_ON_ANY_OF_THE_BIKES(PLAYER_ID())
				IF GET_BIKE_PLAYER_IS_ON() != -1
					IF MAKE_PLAYERS_LOCK_BIKE(GET_BIKE_PLAYER_IS_ON())
						TASK_LEAVE_ANY_VEHICLE(player_PED_id())
						makeplayerleavebike = TRUE
						PRINTLN("[BIKER_STEAL] LEaving bike2")		
					ENDIF
				ENDIF
			ENDIF
			
	
			
			// Server will move us onto end state once all end of mode logic is complete.
			
		BREAK
		
		CASE eMODESTATE_END
			
		BREAK
		
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Performs client initialisation - this will wait for the server to be initialised
/// RETURNS:
///    
FUNC BOOL INIT_CLIENT()
	
	IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_CLIENT_INITIALISED)
		IF IS_SERVER_BIT0_SET(eSERVERBITSET0_SERVER_INITIALISED)
			
			// Do any client initialisation here
			GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_BIKER_STEAL_BIKES)
			IF IS_PLAYER_GOON_IN_LAUNCH_GANG(PLAYER_ID())
			OR AM_I_THE_LAUNCH_BOSS()	
				IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI	
					IF GB_GET_NUM_GOONS_IN_LOCAL_GANG()+1 > 1
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "GB_STEAL_T", "GB_STEAL_Ds")		
					ELSE
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "GB_STEAL_T", "GB_STEAL_D")		
					ENDIF
					GB_SET_GANG_BOSS_HELP_BACKGROUND()

				ENDIF	
			
				IF  NOT Is_There_Any_Current_Objective_Text_From_This_Script()
				AND NOT IS_PLAYER_IN_CLUBHOUSE_PROPERTY(PLAYER_ID())
					IF GB_GET_NUM_GOONS_IN_LOCAL_GANG()+1 > 1
						IF serverBD.mission_variation	= VARIATIONS_SEWER
							Print_Objective_Text("GB_STEALSTRAPs") 
						ELIF serverBD.mission_variation	= VARIATIONS_POLICE_BIKES 
							Print_Objective_Text("GB_STEALSTRA1s") 
						ELIF serverBD.mission_variation	= VARIATIONS_FISHING_VILLAGE 
							Print_Objective_Text("GB_STEALSTRA2s") 
						ENDIF
					ELSE
						IF serverBD.mission_variation	= VARIATIONS_SEWER
							Print_Objective_Text("GB_STEALSTRAP") 
						ELIF serverBD.mission_variation	= VARIATIONS_POLICE_BIKES 
							Print_Objective_Text("GB_STEALSTRA1") 
						ELIF serverBD.mission_variation	= VARIATIONS_FISHING_VILLAGE 
							Print_Objective_Text("GB_STEALSTRA2") 
						ENDIF
					ENDIF
				ENDIF
				ADD_BLIP_FOR_STEAL_BIKE_AREA()
				IF DOES_BLIP_EXIST(blip_area)
					GB_BIKER_RACE_SET_UP(sBikerRace, serverBD.sRaceServerVars,  GET_STEAL_BIKE_COORDS(), blip_area)
				ENDIF
				
				//IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
				IF NOT  IS_TRANSITION_ACTIVE()
				AND  IS_SKYSWOOP_AT_GROUND() 
					TRIGGER_MUSIC_EVENT("BIKER_NINE_TENTHS_START")
				ENDIF
				//ENDIF
				
				GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
				GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION() 
			ENDIF
			
		
			
			SET_AUDIO_FLAG("DisableFlightMusic", TRUE) 
			SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
				
			MPGlobalsAmbience.bKeepShopDoorsOpen = TRUE
			g_sBIK_Telemetry_data.m_Location = ENUM_TO_INT(serverBD.mission_variation)
			g_sBIK_Telemetry_data.m_goonskilled = 0
			SET_CLIENT_BIT0(eCLIENTBITSET0_CLIENT_INITIALISED)
		ENDIF
	ENDIF


	RETURN IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_CLIENT_INITIALISED)
ENDFUNC

PROC SERVER_PROCESSING()
	INT i
	INT iParticipant

	SWITCH GET_MODE_STATE()
	
		CASE eMODESTATE_INIT
			
			// ********************************************
			// Initialise server data here.
			// Will remain in here until server moves on.
			// ********************************************
			
			// Move onto next stage.
			SET_MODE_STATE(eMODESTATE_RUN)
			PRINTLN("[BIKER_STEAL] SET_MODE_STATE(eMODESTATE_RUN)")
			// Get MatchIds for Telemetry
			PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchId1, serverBD.iMatchId2)
			
		BREAK
		
		CASE eMODESTATE_RUN
			
			// Stay in run state until we have an end reason.
			IF GET_END_REASON() = eENDREASON_NO_REASON_YET

				REPEAT NUM_NETWORK_PLAYERS iParticipant
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
						IF NOT HAS_ANYONE_IN_LAUNCH_GANG_REACHED_STEAL_AREA_SERVER()
							GB_BIKER_RACE_SERVER_MAINTAIN(serverBD.sRaceServerVars, iParticipant)
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF NOT serverBD.bcreate_vehicles_and_goons
					IF HAS_ANY_PLAYER_REACHED_STEAL_AREA_SERVER()
						//PRINTLN("[BIKER_STEAL] HAS_ANY_PLAYER_REACHED_STEAL_AREA_SERVER = TRUE)")
						IF serverBD.bcreate_bVEHICLES_PROP = TRUE
							IF serverBD.bcreate_bVEHICLES = FALSE
								IF CREATE_VEHICLES_SERVER() 
									serverBD.bcreate_bVEHICLES = TRUE
								ENDIF
							ENDIF
						ENDIF
						IF serverBD.bcreate_bDEAL_GOONS = FALSE
						and serverBD.bcreate_bVEHICLES = TRUE
							IF CREATE_DEAL_GOONS_SERVER()
								serverBD.bcreate_bDEAL_GOONS = TRUE
							ENDIF
						ENDIF
						
						IF serverBD.bcreate_bVEHICLES_PROP = FALSE
							IF CREATE_PROP_VEHICLES_SERVER()
								serverBD.bcreate_bVEHICLES_PROP = TRUE
							ENDIF
						ENDIF
						/*
						IF serverBD.bcreate_bDEAL_PICKUPS = FALSE
							IF CREATE_PICKUPS_SERVER()
								serverBD.bcreate_bDEAL_PICKUPS = TRUE
							ENDIF
						ENDIF
						*/
			
							
						IF serverBD.bcreate_bVEHICLES = TRUE
						AND serverBD.bcreate_bDEAL_GOONS = TRUE
						//AND serverBD.bcreate_bDEAL_COVERPOINTS = TRUE
						AND serverBD.bcreate_bVEHICLES_PROP = TRUE
							serverBD.bcreate_vehicles_and_goons = TRUE
						ENDIF
					ENDIF
				ELSE
					IF HAS_ANY_PLAYER_REACHED_STEAL_AREA_SERVER()
						IF serverBD.bcreate_bDEAL_COVERPOINTS = FALSE
							IF CREATE_COVERPOINTS_SERVER()
								serverBD.bcreate_bDEAL_COVERPOINTS = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				// Widget for forcing time up at end of mode.
				#IF IS_DEBUG_BUILD
				IF serverBd.bFakeEndOfModeTimer
					PRINTLN("[BIKER_STEAL] serverBd.bFakeEndOfModeTimer = TRUE.")
					SET_END_REASON(eENDREASON_TIME_UP)
				ENDIF
				#ENDIF
				
				// If the mode timer expires, set end reason to time up.
				IF NOT HAS_NET_TIMER_STARTED(serverBd.modeTimer)
					START_NET_TIMER(serverBd.modeTimer)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(serverBd.modeTimer, GET_MODE_TUNEABLE_TIME_LIMIT())
						PRINTLN("[BIKER_STEAL] IF HAS_NET_TIMER_EXPIRED(serverBd.modeTimer, GET_MODE_TUNEABLE_TIME_LIMIT()) eENDREASON_TIME_UP")
						SET_END_REASON(eENDREASON_TIME_UP)
					ENDIF
				ENDIF
				
				IF serverBD.bcreate_bVEHICLES = TRUE
					IF ARE_ALL_BIKES_DESTROYED()
						PRINTLN("[BIKER_STEAL] BIKE IS DESTROYED")
						SET_END_REASON(eENDREASON_BIKES_DESTROYED)
					ENDIF
					
					
	
					
				ENDIF
				

					//IF HAS_ANY_PLAYER_REACHED_UNLOAD_SAFEHOUSE_SERVER()
					//IF IS_ANY_BIKE_AT_THE_SAFEHOUSES()
				IF GET_NUMBER_OF_BIKES_DELIVERED() > 0
					IF (GET_NUMBER_OF_BIKES_DELIVERED() + GET_NUMBER_OF_BIKES_DESTROYED()) >= serverBD.numberofvehiclescreated
						SET_END_REASON(eENDREASON_WIN_CONDITION_TRIGGERED)
					ENDIF
				ENDIF
			
				
				
				REPEAT serverBD.numberofvehiclescreated i
					IF NOT serverBD.bisthisbikedelivered[i]
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverbiketosteal[i])
						AND NOT IS_ENTITY_DEAD(NET_TO_veh(serverBD.serverbiketosteal[i]))	
							IF  serverBD.mission_variation	= VARIATIONS_SEWER
								IF IS_THIS_BIKE_OUT_OF_TUNNEL(i)
								
								ENDIF
							ENDIF
							
							DEAL_WITH_NON_LAUNCH_PEDS_GETTING_ON_MOTOTCYCLES(i)
							
							IF IS_BIKE_AT_SAFEHOUSE_UNLOAD(i)
								serverBD.bisthisbikedelivered[i] = TRUE
							ENDIF	
							
							IF NOT bsetbikesasVULNERABLE[i]
								IF IS_ANY_PLAYER_ON_A_STOLEN_BIKE()	
								// SET ALL BIKES AS VUNERABLE
									IF serverbd.bmissionhasgoneglobal	= FALSE
										
											
										IF SET_BIKES_AS_VULNERABLE(i)
											bsetbikesasVULNERABLE[i] = TRUE	
										ENDIF
										serverbd.bmissionhasgoneglobal	= TRUE
										TickerEventData.TickerEvent = TICKER_EVENT_BIKER_STEAL_GONE_GLOBAL								
										BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_IN_MY_GANG( TRUE))
									ENDIF
									
									
								ENDIF
							ENDIF
						ENDIF
						IF IS_THIS_BIKE_IDLE(i)
						ENDIF
						
						
						
						
					ENDIF
				ENDREPEAT	
				
			ELSE
				
				// Move onto rewards once we have an end reason (therefore mode has ended). 
				SET_MODE_STATE(eMODESTATE_REWARDS)
				
			ENDIF
			
		BREAK
		
		CASE eMODESTATE_REWARDS
			
			// Once all clients have handled the end of the mode, allow the server to move to the end. 
			IF IS_SERVER_BIT0_SET(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
				PRINTLN("[BIKER_STEAL] - [REWARDS] - all participants completed rewards.")
				SET_MODE_STATE(eMODESTATE_END)
			ENDIF
			
		BREAK
		
		CASE eMODESTATE_END
			// Do anything required at very end of mode once rewards and gameplay are opver, then end.
			SET_SERVER_GAME_STATE(GAME_STATE_END)
		BREAK
		
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Performs server initialisation
/// RETURNS:
///    
FUNC BOOL INIT_SERVER()
	
	IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_SERVER_INITIALISED)
		serverBD.piLaunchBoss = PLAYER_ID()
		PRINTLN("[BIKER_STEAL] INIT_SERVER player is the launch boss")
		SET_SERVER_BIT0(eSERVERBITSET0_SERVER_INITIALISED)
	ENDIF

	// this will have to change

	

	serverBD.numberofvehiclescreated = GB_GET_NUM_GOONS_IN_LOCAL_GANG()+1
	
	IF (GB_GET_NUM_GOONS_IN_LOCAL_GANG()+1) >= g_sMPTunables.iBIKER_NINE_TENTHS_MAX_BIKES                 
		PRINTLN("[BIKER_STEAL] CREATE_VEHICLES_SERVER CApping number of bikes to 4 serverBD.numberofvehiclescreated = ", serverBD.numberofvehiclescreated)
		serverBD.numberofvehiclescreated = g_sMPTunables.iBIKER_NINE_TENTHS_MAX_BIKES                 
	ENDIF
	
	IF g_sMPTunables.iBIKER_NINE_TENTHS_MIN_BIKES >    (GB_GET_NUM_GOONS_IN_LOCAL_GANG()+1)
		serverBD.numberofvehiclescreated = g_sMPTunables.iBIKER_NINE_TENTHS_MIN_BIKES
	ENDIF
		
	IF GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() >-1
		PRINTLN("[BIKER_STEAL] GET_PLAYERS_CURRENT_AREA  = ",GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION())
		IF GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() = 0 
			serverBD.mission_variation	= VARIATIONS_SEWER
		ELIF  GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() =1
			serverBD.mission_variation	= VARIATIONS_POLICE_BIKES
		ELIF GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() =2
			serverBD.mission_variation	=  VARIATIONS_FISHING_VILLAGE
			
		ELSE
			serverBD.mission_variation	= VARIATIONS_SEWER
		ENDIF
	ELSE
		serverBD.mission_variation	= INT_TO_ENUM(VARIATIONS_FOR_STEAL_BIKES, GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(VARIATIONS_MAX)))
	ENDIF
	
	IF g_sMPTunables.bBIKER_DISABLE_NINE_TENTHS_0     
		IF serverBD.mission_variation	= VARIATIONS_SEWER
			IF NOT  g_sMPTunables.bBIKER_DISABLE_NINE_TENTHS_1
				serverBD.mission_variation	= VARIATIONS_POLICE_BIKES
				PRINTLN("[BIKER_STEAL] Tunable set.1")
			
			ELSE
				serverBD.mission_variation	= VARIATIONS_FISHING_VILLAGE  
				PRINTLN("[BIKER_STEAL] Tunable set.2")
			
			ENDIF
		ENDIF
	ENDIF
	IF g_sMPTunables.bBIKER_DISABLE_NINE_TENTHS_1   
		IF serverBD.mission_variation	= VARIATIONS_POLICE_BIKES
			IF NOT  g_sMPTunables.bBIKER_DISABLE_NINE_TENTHS_2
				serverBD.mission_variation	= VARIATIONS_FISHING_VILLAGE
				PRINTLN("[BIKER_STEAL] Tunable set.3")
			ELSE
				serverBD.mission_variation	= VARIATIONS_SEWER 
				PRINTLN("[BIKER_STEAL] Tunable set.4")
			ENDIF
		ENDIF
	ENDIF
	
	
	IF g_sMPTunables.bBIKER_DISABLE_NINE_TENTHS_2   
		IF serverBD.mission_variation	= VARIATIONS_FISHING_VILLAGE  
			IF NOT  g_sMPTunables.bBIKER_DISABLE_NINE_TENTHS_1
				serverBD.mission_variation	= VARIATIONS_SEWER
				PRINTLN("[BIKER_STEAL] Tunable set.5")
			ELSE
				serverBD.mission_variation	= VARIATIONS_POLICE_BIKES 
				PRINTLN("[BIKER_STEAL] Tunable set.6")
			ENDIF
		ENDIF
		
	ENDIF
	
	
	RETURN IS_SERVER_BIT0_SET(eSERVERBITSET0_SERVER_INITIALISED)
ENDFUNC

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------


PROC SCRIPT_CLEANUP()
	GB_BIKER_RACE_CLEAN_UP()
	GB_STOP_COUNTDOWN_MUSIC(cmStruct )
	IF GET_MAX_WANTED_LEVEL() != 5
		SET_MAX_WANTED_LEVEL(5)
	ENDIF
	PRINTLN("[BIKER1] SCRIPT_CLEANUP")
	 STOP_SOUND(soundidforalarm) 
	PROCESS_CURRENT_BOSS_CHALLENGE_PLAYSTATS(g_sGb_Telemetry_data.sdata)
	PRINTLN("[BIKER1] - [TELEMETRY] - called PROCESS_CURRENT_BOSS_CHALLENGE_PLAYSTATS")
	
	GB_COMMON_BOSS_MISSION_CLEANUP()	
	USE_CUSTOM_SPAWN_POINTS(FALSE)
	Clear_Any_Objective_Text_From_This_Script()
	
	GB_TIDYUP_SPECTATOR_CAM()
	SET_AUDIO_FLAG("DisableFlightMusic", FALSE) 
	SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
	MPGlobalsAmbience.bKeepShopDoorsOpen = FALSE
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("ALARM_BELL_02")
	CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GB_START_OF_JOB)
	TERMINATE_THIS_THREAD()
	
ENDPROC

FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)

	PRINTLN("[BIKER1] PROCESS_PRE_GAME")
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(NUM_NETWORK_PLAYERS, missionScriptArgs)
	GB_BIKER_RACE_PROCESS_PREGAME(serverBD.sRaceServerVars)
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(GB_GET_BOSS_MISSION_NUM_OBJ_REQUIRED(FMMC_TYPE_BIKER_STEAL_BIKES))
	RESERVE_NETWORK_MISSION_PEDS(GB_GET_BOSS_MISSION_NUM_PEDS_REQUIRED(FMMC_TYPE_BIKER_STEAL_BIKES))
	RESERVE_NETWORK_MISSION_VEHICLES(GB_GET_BOSS_MISSION_NUM_VEH_REQUIRED(FMMC_TYPE_BIKER_STEAL_BIKES))
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD,SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	GB_COMMON_BOSS_MISSION_PREGAME()
	
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_EVENTS()

    INT iCount
    EVENT_NAMES ThisScriptEvent
	STRUCT_EVENT_COMMON_DETAILS Details
   
    REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount

        ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
       	
        SWITCH ThisScriptEvent
            
			CASE EVENT_NETWORK_SCRIPT_EVENT
				
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
				
				//SWITCH Details.Type
					// Script events here.
				//ENDSWITCH
				
			BREAK
			
			CASE EVENT_NETWORK_DAMAGE_ENTITY
				
				PROCESS_DAMAGE(iCount)	
			BREAK
			
        ENDSWITCH
            
    ENDREPEAT
    
ENDPROC

PROC PROCESS_PARTICIPANT_LOOP()
	
	INT iParticipant
	INT iPlayer
	PLAYER_INDEX playerTemp
	PED_INDEX pedTemp
	TEXT_LABEL_63 tl63Temp
	//VEHICLE_INDEX vehTemp
	BOOL bNoBossOnScript = TRUE
	BOOL bAllCompletedRewards = TRUE
	
	// Reset data.
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		sPlayer[iPlayer].playerId = INVALID_PLAYER_INDEX()
		sPlayer[iPlayer].iParticipantId = (-1)
		sPlayer[iPlayer].playerPedId = pedTemp
		CLEAR_BIT(iPlayerOkBitset, iPlayer)
		CLEAR_BIT(iPlayerDeadBitset, iPlayer)
	ENDREPEAT
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		sParticipant[iParticipant].participantId = INVALID_PARTICIPANT_INDEX()
		sParticipant[iParticipant].iPlayerId = (-1)
		CLEAR_BIT(iParticipantActiveBitset, iParticipant)
		sParticipant[iParticipant].tl63Name = tl63Temp
		sParticipant[iParticipant].bIsBoss = FALSE
		sParticipant[iParticipant].bIsGoon = FALSE
	ENDREPEAT
	
	//currentVehicle = vehTemp
	//eCurrentVehicleModel = DUMMY_MODEL_FOR_SCRIPT
//	vMyBossCoords = << 0.0, 0.0, 0.0 >>
	//vFocusParticipantCoords = << 0.0, 0.0, 0.0 >>
	iNumGoonsOnScript = 0
//	fMyDistanceFromBoss = 999999.0
	
	// Refill data.
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
			
			sParticipant[iParticipant].participantId = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
			SET_BIT(iParticipantActiveBitset, iParticipant)
			
			playerTemp = NETWORK_GET_PLAYER_INDEX(sParticipant[iParticipant].participantId)
			iPlayer = NATIVE_TO_INT(playerTemp)
			
			IF IS_NET_PLAYER_OK(playerTemp, FALSE)
				
				sPlayer[iPlayer].playerId = playerTemp
				sParticipant[iParticipant].iPlayerId = iPlayer
				sPlayer[iPlayer].iParticipantId = iParticipant
				sPlayer[iPlayer].playerPedId = GET_PLAYER_PED(playerTemp)
				sParticipant[iParticipant].tl63Name = GET_PLAYER_NAME(playerTemp)
				
				SET_BIT(iPlayerOkBitset, iPlayer)
				
				IF IS_ENTITY_DEAD(sPlayer[iPlayer].playerPedId)
					SET_BIT(iPlayerDeadBitset, iPlayer)
				ENDIF
				
				// Track if boss or a goon.
				IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(sPlayer[iPlayer].playerId)
					sParticipant[iParticipant].bIsBoss = TRUE
				ELIF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(sPlayer[iPlayer].playerId, FALSE)
					sParticipant[iParticipant].bIsGoon = TRUE
				ENDIF
					
				// Track if in a target vehicle.
//				IF sPlayer[iPlayer].playerPedId = PLAYER_PED_ID()
				IF iFocusParticipant = iParticipant
					//vFocusParticipantCoords = GET_ENTITY_COORDS(sPlayer[iPlayer].playerPedId)
					IF IS_PED_IN_ANY_VEHICLE(sPlayer[iPlayer].playerPedId)
						//currentVehicle = GET_VEHICLE_PED_IS_IN(sPlayer[iPlayer].playerPedId)
						//eCurrentVehicleModel = GET_ENTITY_MODEL(currentVehicle)
					ENDIF
				ENDIF
				
				IF sParticipant[iParticipant].bIsBoss
					bNoBossOnScript = FALSE
//					vMyBossCoords = GET_ENTITY_COORDS(sPlayer[iPlayer].playerPedId, FALSE) // Track my boss coords.
					IF NOT IS_BIT_SET(iPlayerDeadBitset, iPlayer)
					AND NOT IS_BIT_SET(iPlayerDeadBitset, NATIVE_TO_INT(PLAYER_ID()))
//						fMyDistanceFromBoss = GET_DISTANCE_BETWEEN_COORDS(vFocusParticipantCoords, vMyBossCoords)
					ENDIF
				ELIF sParticipant[iParticipant].bIsGoon
					iNumGoonsOnScript++ // Track number of goons from my gang on this script.
				ENDIF
				
				IF NOT IS_CLIENT_BIT0_SET(sParticipant[iParticipant].participantId, eCLIENTBITSET0_COMPLETED_REWARDS)
					bAllCompletedRewards = FALSE
				ENDIF
					
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					
					#IF IS_DEBUG_BUILD
					IF serverBD.iSPassPart = (-1)
					AND serverBD.iFFailPart = (-1)
						IF IS_CLIENT_DEBUG_BIT0_SET(sParticipant[iParticipant].participantId, eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
							serverBD.iSPassPart = iParticipant
							PRINTLN("[BIKER1] participant ", iParticipant, " has S passed. Setting serverBD.iSPassPart = ", iParticipant)
						ELIF IS_CLIENT_DEBUG_BIT0_SET(sParticipant[iParticipant].participantId, eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED)
							serverBD.iFFailPart = iParticipant
							PRINTLN("[BIKER1] participant ", iParticipant, " has F failed. Setting serverBD.iFFailPart = ", iParticipant)
						ENDIF
					ENDIF
					#ENDIF
					
				ENDIF
				
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
		IF bAllCompletedRewards
			IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
				SET_SERVER_BIT0(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
			ENDIF
		ELSE
			IF IS_SERVER_BIT0_SET(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
				CLEAR_SERVER_BIT0(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
			ENDIF
		ENDIF
		
		IF GET_END_REASON() = eENDREASON_NO_REASON_YET
		
			IF bNoBossOnScript
				SET_END_REASON(eENDREASON_NO_BOSS_LEFT)
			ELIF iNumGoonsOnScript = 0
				/*IF HAS_NET_TIMER_STARTED(serverBD.modeTimer)
					IF HAS_NET_TIMER_EXPIRED(serverBD.modeTimer, TRACK_GOON_DELAY) // Wait for little bit before ending script for lack of goons, to give them a chance to join the script before it terminates for them not being on it. 
						SET_END_REASON(eENDREASON_ALL_GOONS_LEFT)
					ENDIF
				ENDIF*/
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD

TEXT_WIDGET_ID twIdServerGameState
TEXT_WIDGET_ID twIdClientGameState[NUM_NETWORK_PLAYERS]
TEXT_WIDGET_ID twIdClientName[NUM_NETWORK_PLAYERS]
TEXT_WIDGET_ID twIdModeState
BOOL bTerminateScriptNow

PROC UPDATE_SERVER_GAME_STATE_WIDGET()
	SET_CONTENTS_OF_TEXT_WIDGET(twIdServerGameState, GET_GAME_STATE_NAME(GET_SERVER_GAME_STATE()))
	SET_CONTENTS_OF_TEXT_WIDGET(twIdModeState, GET_MODE_STATE_NAME(GET_MODE_STATE()))
ENDPROC

PROC UPDATE_CLIENT_GAME_STATE_WIDGET(INT iParticipant)
	
	STRING strName
	
	SET_CONTENTS_OF_TEXT_WIDGET(twIdClientGameState[iParticipant], "NOT ACTIVE")
	SET_CONTENTS_OF_TEXT_WIDGET(twIdClientName[iParticipant], "NOT ACTIVE")
	
	IF IS_BIT_SET(iParticipantActiveBitset, iParticipant)
		IF IS_BIT_SET(iPlayerOkBitset, sParticipant[iParticipant].iPlayerId)
			SET_CONTENTS_OF_TEXT_WIDGET(twIdClientGameState[iParticipant], GET_GAME_STATE_NAME(GET_CLIENT_GAME_STATE(iParticipant)))
			strName = GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, sParticipant[iParticipant].iPlayerId))
			SET_CONTENTS_OF_TEXT_WIDGET(twIdClientName[iParticipant], strName)
		ENDIF
	ENDIF
	
ENDPROC

PROC CREATE_WIDGETS()
	
	INT iParticipant
	TEXT_LABEL_63 tl63Temp
	
	START_WIDGET_GROUP("GB Carjacking")
		ADD_WIDGET_BOOL("Locally terminate Script Now", bTerminateScriptNow)
		ADD_WIDGET_BOOL("Fake End Of Mode Timer", serverBd.bFakeEndOfModeTimer)
		START_WIDGET_GROUP("Game State")
			START_WIDGET_GROUP("Server")
				twIdServerGameState = ADD_TEXT_WIDGET("Game State")
				twIdModeState = ADD_TEXT_WIDGET("Mode State")
				UPDATE_SERVER_GAME_STATE_WIDGET()
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Client")
				REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() iParticipant
					tl63Temp = ""
					tl63Temp +="Part "
					tl63Temp += iParticipant
					tl63Temp += " Name"
					twIdClientName[iParticipant] = ADD_TEXT_WIDGET(tl63Temp)
					tl63Temp = ""
					tl63Temp += "Part "
					tl63Temp += iParticipant
					tl63Temp += " Game State"
					twIdClientGameState[iParticipant] = ADD_TEXT_WIDGET(tl63Temp)
					tl63Temp = ""
					tl63Temp +="Part "
					tl63Temp += iParticipant
					tl63Temp += " Player ID"
					ADD_WIDGET_INT_READ_ONLY(tl63Temp, sParticipant[iParticipant].iPlayerId)
					tl63Temp = ""
					tl63Temp +="Part "
					tl63Temp += iParticipant
					tl63Temp += " Player stored Part ID"
					ADD_WIDGET_INT_READ_ONLY(tl63Temp, sPlayer[sParticipant[iParticipant].iPlayerId].iParticipantId)
					IF iParticipant >0
					AND iParticipant <32
						UPDATE_CLIENT_GAME_STATE_WIDGET(iParticipant)
					ENDIF
				ENDREPEAT
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
	PRINTLN("[BIKER1] - created widgets.")
	
ENDPROC		

PROC UPDATE_WIDGETS()
	
	INT iPartcount
	
	UPDATE_SERVER_GAME_STATE_WIDGET()
	
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() iPartcount
		UPDATE_CLIENT_GAME_STATE_WIDGET(iPartcount)
	ENDREPEAT
	
	IF NOT IS_LOCAL_DEBUG_BIT0_SET(eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW)
		IF bTerminateScriptNow
			PRINTLN("[BIKER1] widget bTerminateScriptNow = TRUE.")
			SET_LOCAL_DEBUG_BIT0(eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW)
		ENDIF
	ENDIF
	
ENDPROC

#ENDIF


PROC UPDATE_FOCUS_PARTICIPANT()

	// Save out the current focus participant. Stays at -1 if not the local player or not spectating a script participant.
	iFocusParticipant = (-1)
	
	IF IS_BIT_SET(iPlayerOkBitset, NATIVE_TO_INT(PLAYER_ID()))
		IF IS_BIT_SET(iParticipantActiveBitset, PARTICIPANT_ID_TO_INT())
			IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
				iFocusParticipant = PARTICIPANT_ID_TO_INT()
			ELSE
				IF IS_A_SPECTATOR_CAM_RUNNING()
				AND NOT IS_SPECTATOR_HUD_HIDDEN()
					PED_INDEX specTargetPed = GET_SPECTATOR_SELECTED_PED()
					IF IS_PED_A_PLAYER(specTargetPed)
						PLAYER_INDEX specPlayerTemp = NETWORK_GET_PLAYER_INDEX_FROM_PED(specTargetPed)
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(specPlayerTemp)
							PARTICIPANT_INDEX specTargetParticipant = NETWORK_GET_PARTICIPANT_INDEX(specPlayerTemp)
							iFocusParticipant = NATIVE_TO_INT(specTargetParticipant)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC


//----------------------
//	MAIN SCRIPT
//----------------------

SCRIPT(MP_MISSION_DATA missionScriptArgs)


	PRINTLN("[BIKER1] START")
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			
			IF NOT PROCESS_PRE_GAME(missionScriptArgs)
				EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
				SCRIPT_CLEANUP()
			ENDIF
			// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
			#IF IS_DEBUG_BUILD
				CREATE_WIDGETS()
			#ENDIF
		ELSE
			
			PRINTLN("[BIKER1] calling SCRIPT_CLEANUP() - IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE) = FALSE")
			SCRIPT_CLEANUP()
			
		ENDIF
	ELSE
		
		PRINTLN("[BIKER1] calling SCRIPT_CLEANUP() - NETWORK_IS_GAME_IN_PROGRESS() = FALSE")
		EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
		SCRIPT_CLEANUP()
		
	ENDIF
	
	WHILE (TRUE)
	
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		IF IS_LOCAL_DEBUG_BIT0_SET(eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW)
			PRINTLN("[BIKER1] eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW is set")
			EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		#ENDIF
		
		IF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION()
			PRINTLN("[BIKER1] - GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION = TRUE")
			EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[BIKER1] SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
			EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		
		UPDATE_FOCUS_PARTICIPANT()
		PROCESS_PARTICIPANT_LOOP()
		PROCESS_EVENTS()
		
		GB_MAINTAIN_SPECTATE(sSpecVars)
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
			MAINTAIN_J_SKIPS()
		#ENDIF
		
		SWITCH(GET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT()))
		
			CASE GAME_STATE_INIT
				IF GET_SERVER_GAME_STATE() = GAME_STATE_RUNNING
					IF INIT_CLIENT()
						SET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_RUNNING)
					ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_GAME_STATE() = GAME_STATE_RUNNING
				
					CLIENT_PROCESSING()
					
				ELIF GET_SERVER_GAME_STATE() = GAME_STATE_END
					SET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT(), GAME_STATE_END)
				ENDIF
			BREAK
			
			CASE GAME_STATE_END
				SCRIPT_CLEANUP()
			BREAK
			
		ENDSWITCH
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			IF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION()
				PRINTLN("[BIKER1] - GB_SHOULD_KILL_ACTIVE_BOSS_MISSION = TRUE")
				EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
				SET_SERVER_GAME_STATE(GAME_STATE_END)
			ENDIF
		
			SWITCH(GET_SERVER_GAME_STATE())
			
				CASE GAME_STATE_INIT
					IF INIT_SERVER()
						SET_SERVER_GAME_STATE(GAME_STATE_RUNNING)
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING
					SERVER_PROCESSING()
				BREAK
				
				CASE GAME_STATE_END
					SCRIPT_CLEANUP()
				BREAK
				
			ENDSWITCH
		
		ENDIF
		
	ENDWHILE

	
	
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT


