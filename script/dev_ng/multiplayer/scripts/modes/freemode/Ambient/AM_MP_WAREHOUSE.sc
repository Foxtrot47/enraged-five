//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_WAREHOUSE.sch																			//
// Description: Script for managing the interior of a warehouse. Warehouse access, spawning etc. 			//
//				is managed by AM_MP_SMPL_INTERIOR_* script while this script is launched by simple interior	//
//				script to handle anything specific to warehouses - crates, any activities etc.				//
// Written by:  Tymon																						//
// Date:  		25/02/2015																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"

USING "net_wait_zero.sch"
USING "freemode_events_header.sch"

USING "net_MP_Radio.sch"

USING "net_simple_interior.sch"
#IF FEATURE_DLC_1_2022
USING "net_peds_script_management.sch"
#ENDIF

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ VARIABLES  ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

MP_RADIO_LOCAL_DATA_STRUCT MPRadioLocal
MP_PROP_LOCAL_CONTROL serverLocalActControl

STRUCT ServerBroadcastData
	INT iBS
	INT iCrateID[ciLARGE_WAREHOUSE_CRATE_CAPACITY]
	MP_RADIO_SERVER_DATA_STRUCT MPRadioServer
	MP_PROP_ACT_SERVER_CONTROL_STRUCT activityControl
ENDSTRUCT
ServerBroadcastData serverBD

CONST_INT BS_PLAYER_BD_AM_OWNER_AND_AM_WATCHING_INTRO_CUTSCENE 	0

CONST_INT SOURCE_CARGO_MENU_BS_REBUILD_MENU			0
CONST_INT SOURCE_CARGO_MENU_BS_RUN_TRANSACTION		1
CONST_INT SOURCE_CARGO_MENU_BS_DELAY_TIMER_ACTIVE	2

CONST_INT TOOLBOX_ENTER_ANIM_RUNNING 	0
CONST_INT TOOLBOX_IDLE_ANIM_RUNNING		1
CONST_INT TOOLBOX_EXIT_ANIM_RUNNING		2

STRUCT PlayerBroadcastData
	INT iBS
	MP_RADIO_CLIENT_DATA_STRUCT MPRadioClient
	INT iActivityRequested
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

ENUM WAREHOUSE_SCRIPT_STATE
	WAREHOUSE_STATE_LOADING,
	WAREHOUSE_STATE_IDLE,
	WAREHOUSE_STATE_PLAYING_INTRO,
	WAREHOUSE_STATE_LOADING_GARAGE_EXIT,
	WAREHOUSE_STATE_PLAYING_GARAGE_EXIT,
	WAREHOUSE_STATE_FINISHING_GARAGE_EXIT
ENDENUM

ENUM WAREHOUSE_CABINET_STATE
	WCABINET_STAGE_INIT,
	WCABINET_STAGE_WAITING_TO_TRIGGER,
	WCABINET_STAGE_ENTER_ANIM,
	WCABINET_STAGE_INTERACTING,
	WCABINET_STAGE_EXIT_ANIM,
	WCABINET_STAGE_CLEANUP
ENDENUM

ENUM WAREHOUSE_VEH_MENU
	WV_TRUCK_MENU = 0,
	WV_PLANE_MENU = 1,
	WV_BOAT_MENU = 2,
	WV_MAIN_MENU
ENDENUM

STRUCT WAREHOUSE_MENU_INPUT
	INT iLeftX, iLeftY, iRightX, iRightY, iMoveUp, iCursorValue
	BOOL bAccept
	BOOL bBack
	BOOL bPrevious
	BOOL bNext
ENDSTRUCT

CONST_INT VEH_UPGRADE_MAX_SUB_MENU_ITEM		3

STRUCT WAREHOUSE_UPGRADE_CABINET
	WAREHOUSE_CABINET_STATE eWCabinetState
	OBJECT_INDEX objectCabinet
	MP_PROP_OFFSET_STRUCT sCabinetPos
	INT iVehUpgradeContext = NEW_CONTEXT_INTENTION
	WAREHOUSE_MENU_INPUT sWarehouseMenuInput
	TIME_DATATYPE scrollDelay
	BOOL bMenuInitialised
	BOOL bReBuildMenu
	BOOL bIsMenuItemAvailable
	INT iMenuCurrentItem,iMenuTopItem,iSyncedSceneID,iToolBoxBS
	MP_PROP_OFFSET_STRUCT sToolBoxScene,sToolBoxTriggerLocate
	STRING sToolBoxDic
	WAREHOUSE_VEH_MENU eCurrentMenu
ENDSTRUCT

CONST_INT BS_WAREHOUSE_GOON_HELP_PROCESSED 	0
#IF FEATURE_DLC_1_2022
CONST_INT BS_FLASH_STAFF_BLIP 	1
CONST_INT BS_WAREHOUSE_STAFF_HELP_PROCESSED 	2

CONST_INT ciWAREHOUSE_PED_HELP_TEXT_AND_BLIP_TIME	10000

ENUM SOURCE_CARGO_MENU_STATE
	SOURCE_CARGO_MENU_STATE_INIT,
	SOURCE_CARGO_MENU_STATE_PROMPT,
	SOURCE_CARGO_MENU_STATE_MENU,
	SOURCE_CARGO_MENU_STATE_END
ENDENUM

STRUCT SOURCE_CARGO_MENU_DATA
	GENERIC_TRANSACTION_STATE eTransactionResult = TRANSACTION_STATE_DEFAULT
	
	INT iBS
	INT iContextIntention = NEW_CONTEXT_INTENTION
	INT iCurrentMenuDepth
	INT iCurrentMenuSelection
	
	SOURCE_CARGO_MENU_STATE eCurrentState = SOURCE_CARGO_MENU_STATE_INIT
	
	SCRIPT_TIMER stAnalogueMenuDelay
ENDSTRUCT
#ENDIF

STRUCT WAREHOUSE_DATA
	INT iBS
	WAREHOUSE_SCRIPT_STATE eState
	INT iID // ID of this warehouse
	SIMPLE_INTERIORS eSimpleInteriorID
	INT iScriptInstance
	INT iInvitingPlayer
	INT iWHContrabandTotal
	INT iWHMissionContrabandTotal
	INT iWHRemoveContrabandTotal
	INT iCrateCapacity
	INT iWHSaveSlot
	INT iContraType
	INT iContraTypeCount
	INT iSpecialItemCount
	BOOL bWaitingForCrateData
	PLAYER_INDEX pOwner
	BLIP_INDEX laptopBlip
	BLIP_INDEX workbenchBlip
	SIMPLE_INTERIOR_DETAILS interiorDetails
	WAREHOUSE_UPGRADE_CABINET sWUpgradeCabinet
	BOOL bCrateFadeTimerSet[ciLARGE_WAREHOUSE_CRATE_CAPACITY]
	BOOL bCrateFadeComplete[ciLARGE_WAREHOUSE_CRATE_CAPACITY]
	TIME_DATATYPE CrateFadeTimer[ciLARGE_WAREHOUSE_CRATE_CAPACITY]
	OBJECT_INDEX oCrateObjects[ciLARGE_WAREHOUSE_CRATE_CAPACITY]
	
	VECTOR vGarageExitMin
	VECTOR vGarageExitMax
	SIMPLE_INTERIOR_ENTRY_ANIM_DATA garageExitAnim
	
	SCRIPT_TIMER timerGoonHelpDelay
		// Ped data
	BOOL bPedScriptLaunched = FALSE
	SCRIPT_TIMER stPedLaunchingBailTimer
	
	#IF FEATURE_DLC_1_2022
	SOURCE_CARGO_MENU_DATA sSourceCargoMenuData
	SCRIPT_TIMER tSyncBDTimer
	SCRIPT_TIMER tHostRequestTimer
	BLIP_INDEX biWarehousePed
	#ENDIF
ENDSTRUCT
WAREHOUSE_DATA thisWarehouse

// Intro Cutscene
ENUM WAREHOUSE_INTRO_CUTSCENE_STAGE
	WAREHOUSE_CUTSCENE_LOADING,
	WAREHOUSE_CUTSCENE_SHELVES_SHOT,
	WAREHOUSE_CUTSCENE_LAPTOP_SHOT,
	WAREHOUSE_CUTSCENE_WORKBENCH_SHOT,
	WAREHOUSE_CUTSCENE_STAFF_SHOT
ENDENUM

STRUCT WAREHOUSE_INTRO_CUTSCENE_DATA
	BOOL bPlaying
	BOOL bOwnerVersion
	WAREHOUSE_INTRO_CUTSCENE_STAGE stage
	CAMERA_INDEX camera
	INT iBSSceneInitialised
	INT iBSSceneHelpShown
	SCRIPT_TIMER timerScene
	SCRIPT_TIMER timerHelpTextDelay
	BOOL bInputGaitSimulated
	BOOL bShownOnce
ENDSTRUCT
WAREHOUSE_INTRO_CUTSCENE_DATA warehouseIntroCutscene

#IF IS_DEBUG_BUILD
STRUCT WAREHOUSE_DEBUG_DATA
	BOOL bResetCutscene
	BOOL bPrintServerBDCrates
	BOOL bGiveSpecial
	BOOL bStatsTest
ENDSTRUCT
WAREHOUSE_DEBUG_DATA debugData
#ENDIF



//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PROCEDURES  ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_WAREHOUSE_STATE(WAREHOUSE_SCRIPT_STATE eState)
	RETURN thisWarehouse.eState = eState
ENDFUNC

PROC SET_WAREHOUSE_STATE(WAREHOUSE_SCRIPT_STATE eState)
	CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] AM_MP_WAREHOUSE - SET_WAREHOUSE_STATE - New state: ", ENUM_TO_INT(eState))
	thisWarehouse.eState = eState
ENDPROC

/// PURPOSE: Returns the crate object from the passed in crate slot
FUNC OBJECT_INDEX GET_CRATE_OBJECT_FROM_CRATE_SLOT(INT iCrateIndex)
	RETURN thisWarehouse.oCrateObjects[iCrateIndex]
ENDFUNC

/// PURPOSE: Returns TRUE if an object is in the passed in crate slot
FUNC BOOL DOES_CRATE_SLOT_HAVE_CRATE_OBJECT(INT iCrateIndex)
	RETURN DOES_ENTITY_EXIST(thisWarehouse.oCrateObjects[iCrateIndex])
ENDFUNC

PROC CLEANUP_SINGLE_WAREHOUSE_CRATE(INT iCrateIndex)
	OBJECT_INDEX oCrateObj = GET_CRATE_OBJECT_FROM_CRATE_SLOT(iCrateIndex)
	IF DOES_ENTITY_EXIST(oCrateObj)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(oCrateObj)
		DELETE_OBJECT(oCrateObj)
		thisWarehouse.oCrateObjects[iCrateIndex] = NULL
	ENDIF
ENDPROC

PROC CLEANUP_ALL_WAREHOUSE_CRATES()
	INT iCrateIndex
	FOR iCrateIndex = 0 TO (thisWarehouse.iCrateCapacity-1)
		OBJECT_INDEX oCrateObj = GET_CRATE_OBJECT_FROM_CRATE_SLOT(iCrateIndex)
		IF DOES_ENTITY_EXIST(oCrateObj)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(oCrateObj)
			DELETE_OBJECT(oCrateObj)
			thisWarehouse.oCrateObjects[iCrateIndex] = NULL
		ENDIF
	ENDFOR
ENDPROC

PROC CREATE_SINGLE_WAREHOUSE_CRATE(INT iCrateIndex)
	
	VECTOR vShelfCoords = GET_WAREHOUSE_SHELF_COORDS(GET_WAREHOUSE_SIZE(thisWarehouse.iID))
	VECTOR vCrateOffset = GET_WAREHOUSE_INTERIOR_CRATE_COORDS_OFFSET(thisWarehouse.iID, GET_WAREHOUSE_SIZE(thisWarehouse.iID), iCrateIndex)
	VECTOR vCrateCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vShelfCoords, 0, vCrateOffset)
	VECTOR vCrateRotation = GET_WAREHOUSE_INTERIOR_CRATE_ROTATION(thisWarehouse.iID, GET_WAREHOUSE_SIZE(thisWarehouse.iID), iCrateIndex)
	
	// Ensure model requested before attempting to create
	IF INT_TO_ENUM(MODEL_NAMES, serverBD.iCrateID[iCrateIndex]) != DUMMY_MODEL_FOR_SCRIPT
	AND IS_MODEL_VALID(INT_TO_ENUM(MODEL_NAMES, serverBD.iCrateID[iCrateIndex]))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, serverBD.iCrateID[iCrateIndex]))
		
		IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, serverBD.iCrateID[iCrateIndex]))
			OBJECT_INDEX oCrateObj = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, serverBD.iCrateID[iCrateIndex]), vCrateCoords, FALSE, FALSE)
			SET_ENTITY_ROTATION(oCrateObj, vCrateRotation)
			FREEZE_ENTITY_POSITION(oCrateObj, TRUE)
			thisWarehouse.oCrateObjects[iCrateIndex] = oCrateObj
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - CREATE_SINGLE_WAREHOUSE_CRATE - Creating crate in slot: ", iCrateIndex, " with model: ", GET_WAREHOUSE_CRATE_MODEL_INDEX(INT_TO_ENUM(MODEL_NAMES, serverBD.iCrateID[iCrateIndex])))
			
			g_iWarehouseCrateData[iCrateIndex] = GET_WAREHOUSE_CRATE_MODEL_INDEX(INT_TO_ENUM(MODEL_NAMES, serverBD.iCrateID[iCrateIndex]))
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Handles fading of crates. Crates fade in if bFadeIn is TRUE, fade out if not
PROC MAINTAIN_WAREHOUSE_CRATE_FADES(INT iCrateIndex, BOOL bFadeIn)

	IF NOT thisWarehouse.bCrateFadeTimerSet[iCrateIndex]
		thisWarehouse.CrateFadeTimer[iCrateIndex] = GET_TIME_OFFSET(GET_NETWORK_TIME(), ciWAREHOUSE_CRATE_FADE_TIME)
		thisWarehouse.bCrateFadeTimerSet[iCrateIndex] = TRUE
	ENDIF
	
	IF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), thisWarehouse.CrateFadeTimer[iCrateIndex])
		OBJECT_INDEX oCrateObj = GET_CRATE_OBJECT_FROM_CRATE_SLOT(iCrateIndex)
		IF DOES_ENTITY_EXIST(oCrateObj)
			INT iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), thisWarehouse.CrateFadeTimer[iCrateIndex]))
			INT iAlpha = ROUND((TO_FLOAT(iTimeDifference) / ciWAREHOUSE_CRATE_FADE_TIME) * 255)
			IF bFadeIn
				iAlpha = (255 - (ROUND((TO_FLOAT(iTimeDifference) / ciWAREHOUSE_CRATE_FADE_TIME) * 255)))
			ENDIF
			SET_ENTITY_ALPHA(oCrateObj, iAlpha, FALSE)
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - MAINTAIN_WAREHOUSE_CRATE_FADES - Fading crate: ", iCrateIndex, " with alpha = ", iAlpha, " iTimeDifference = ", iTimeDifference)
		ENDIF
	ELSE
		OBJECT_INDEX oCrateObj = GET_CRATE_OBJECT_FROM_CRATE_SLOT(iCrateIndex)
		IF DOES_ENTITY_EXIST(oCrateObj)
			IF bFadeIn
				// Clear model from memory once created
				SET_ENTITY_ALPHA(oCrateObj, 255, FALSE)
				IF IS_MODEL_VALID(GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(GET_WAREHOUSE_CRATES_STAT_INT(thisWarehouse.iWHSaveSlot, iCrateIndex)))
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(GET_WAREHOUSE_CRATES_STAT_INT(thisWarehouse.iWHSaveSlot, iCrateIndex)))
				ENDIF
				thisWarehouse.bCrateFadeComplete[iCrateIndex] = TRUE
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - MAINTAIN_WAREHOUSE_CRATE_FADES - Crate: ", iCrateIndex, " has fully faded in")
			ELSE
				// Delete crate after fade out
				SET_ENTITY_ALPHA(oCrateObj, 0, FALSE)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(oCrateObj)
					CLEANUP_SINGLE_WAREHOUSE_CRATE(iCrateIndex)
				ENDIF
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					serverBD.iCrateID[iCrateIndex] = ENUM_TO_INT(DUMMY_MODEL_FOR_SCRIPT)
				ENDIF
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - MAINTAIN_WAREHOUSE_CRATE_FADES - Crate: ", iCrateIndex, " has fully faded out & been deleted")
			ENDIF
		ENDIF
		
		thisWarehouse.bCrateFadeTimerSet[iCrateIndex] = FALSE
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_BLOCKING_CRATE_SPAWN_COORDS(PLAYER_INDEX playerID, INT iCrate)

	VECTOR vShelfCoords = GET_WAREHOUSE_SHELF_COORDS(GET_WAREHOUSE_SIZE(thisWarehouse.iID))
	VECTOR vCrateOffset = GET_WAREHOUSE_INTERIOR_CRATE_COORDS_OFFSET(thisWarehouse.iID, GET_WAREHOUSE_SIZE(thisWarehouse.iID), iCrate)
	VECTOR vCrateCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vShelfCoords, 0, vCrateOffset)
	VECTOR vPlayerCoords = GET_PLAYER_COORDS(playerID)
	
	IF ARE_VECTORS_ALMOST_EQUAL(vPlayerCoords, vCrateCoords, 1.5)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_PLAYER_BLOCKING_CRATE_SPAWN_COORDS(INT iCrate)

	VECTOR vShelfCoords = GET_WAREHOUSE_SHELF_COORDS(GET_WAREHOUSE_SIZE(thisWarehouse.iID))
	VECTOR vCrateOffset = GET_WAREHOUSE_INTERIOR_CRATE_COORDS_OFFSET(thisWarehouse.iID, GET_WAREHOUSE_SIZE(thisWarehouse.iID), iCrate)
	VECTOR vCrateCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vShelfCoords, 0, vCrateOffset)
	
	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
		    PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
		    IF IS_NET_PLAYER_OK(PlayerId)
				IF ARE_VECTORS_ALMOST_EQUAL(GET_PLAYER_COORDS(playerID), vCrateCoords, 1.5)
					RETURN TRUE
				ENDIF
			ENDIF
	    ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC POPULATE_WAREHOUSE_WITH_CRATES()
	INT iSaveSlot = GET_SAVE_SLOT_FOR_PLAYERS_WAREHOUSE(thisWarehouse.iID, thisWarehouse.pOwner)
	
	INT iCrateIndex
	FOR iCrateIndex = 0 TO (GET_PLAYER_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(thisWarehouse.pOwner, thisWarehouse.iID)+2)
		IF iCrateIndex < GET_WAREHOUSE_MAX_CAPACITY(thisWarehouse.iID)
			serverBD.iCrateID[iCrateIndex] = ENUM_TO_INT(GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(GET_WAREHOUSE_CRATES_STAT_INT(iSaveSlot, iCrateIndex)))
			IF iCrateIndex >= GET_PLAYER_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(thisWarehouse.pOwner, thisWarehouse.iID)
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - POPULATE_WAREHOUSE_WITH_CRATES - FAKE CRATE DATA - Filling Server BD Array - Index: ", iCrateIndex, " has Crate Model: ", GET_WAREHOUSE_CRATES_STAT_INT(iSaveSlot, iCrateIndex))
			ELSE
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - POPULATE_WAREHOUSE_WITH_CRATES - Filling Server BD Array - Index: ", iCrateIndex, " has Crate Model: ", GET_WAREHOUSE_CRATES_STAT_INT(iSaveSlot, iCrateIndex))
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC POPULATE_WAREHOUSE_CRATES_DATA_FROM_OWNER()
	INT iCrateIndex
	FOR iCrateIndex = 0 TO (GET_PLAYER_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(thisWarehouse.pOwner, thisWarehouse.iID)+2)
		IF iCrateIndex < GET_WAREHOUSE_MAX_CAPACITY(thisWarehouse.iID)
			serverBD.iCrateID[iCrateIndex] = ENUM_TO_INT(GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(g_iWarehouseCrateData[iCrateIndex]))
			IF iCrateIndex >= GET_PLAYER_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(thisWarehouse.pOwner, thisWarehouse.iID)
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - POPULATE_WAREHOUSE_WITH_CRATES_DATA_FROM_OWNER - FAKE CRATE DATA - Filling Server BD Array - Index: ", iCrateIndex, " has Crate Model: ", g_iWarehouseCrateData[iCrateIndex])
			ELSE
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - POPULATE_WAREHOUSE_WITH_CRATES_DATA_FROM_OWNER - Filling Server BD Array - Index: ", iCrateIndex, " has Crate Model: ", g_iWarehouseCrateData[iCrateIndex])
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC CLEANUP_GLOBAL_WAREHOUSE_CRATE_DATA()
	INT i
	FOR i = 0 TO (ciLARGE_WAREHOUSE_CRATE_CAPACITY-1)
		g_iWarehouseCrateData[i] = -1
	ENDFOR
ENDPROC

FUNC BOOL PERFORM_WAREHOUSE_CRATE_EVENT_CHECKS(INT iUpdatedContrabandTotal)
	BOOL bUpdateServerBD

	INT iCrateIndex
	FOR iCrateIndex = 0 TO (iUpdatedContrabandTotal+2)
		IF iCrateIndex < thisWarehouse.iCrateCapacity
			IF serverBD.iCrateID[iCrateIndex] != ENUM_TO_INT(GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(g_iWarehouseCrateData[iCrateIndex]))	
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - PERFORM_WAREHOUSE_CRATE_EVENT_CHECKS - Server BD doesn't equal same data as g_iWarehouseCrateData - update from events")
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - PERFORM_WAREHOUSE_CRATE_EVENT_CHECKS - New crate from event in slot: ", iCrateIndex)
						
				// Delete Existing Crate
				OBJECT_INDEX oCrateObj = GET_CRATE_OBJECT_FROM_CRATE_SLOT(iCrateIndex)
				IF DOES_ENTITY_EXIST(oCrateObj)
					CLEANUP_SINGLE_WAREHOUSE_CRATE(iCrateIndex)
				ENDIF
				
				// Update Server BD
				serverBD.iCrateID[iCrateIndex] = ENUM_TO_INT(GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(g_iWarehouseCrateData[iCrateIndex]))
				
				// Create
				IF NOT IS_ANY_PLAYER_BLOCKING_CRATE_SPAWN_COORDS(iCrateIndex)
					CREATE_SINGLE_WAREHOUSE_CRATE(iCrateIndex)
				ENDIF
				
				bUpdateServerBD = TRUE
			ENDIF
		ENDIF
	ENDFOR
	RETURN bUpdateServerBD
ENDFUNC

PROC PERFORM_WAREHOUSE_SPECIAL_CRATE_VALIDATION_CHECKS(INT iUpdatedContrabandTotal)
	INT iCrateIndex
	
	// Does Server BD Special Items Match Stats?
	IF CHECK_WAREHOUSE_ID(thisWarehouse.iID)
		IF GET_COUNT_OF_SPECIAL_ITEMS_IN_WAREHOUSE(thisWarehouse.iID) > 0
			FOR iCrateIndex = 0 TO (iUpdatedContrabandTotal-1)
				IF iCrateIndex < thisWarehouse.iCrateCapacity
					IF INT_TO_ENUM(MODEL_NAMES, serverBD.iCrateID[iCrateIndex]) != GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(GET_WAREHOUSE_CRATES_STAT_INT(thisWarehouse.iWHSaveSlot, iCrateIndex))
						IF IS_WAREHOUSE_CRATE_A_SPECIAL_ITEM(GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(GET_WAREHOUSE_CRATES_STAT_INT(thisWarehouse.iWHSaveSlot, iCrateIndex)))
							CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - PERFORM_WAREHOUSE_SPECIAL_CRATE_VALIDATION_CHECKS - Special Item has swapped slot and Server BD needs updating")
							CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - PERFORM_WAREHOUSE_SPECIAL_CRATE_VALIDATION_CHECKS - Special Item new slot: ", iCrateIndex, " model: ", GET_WAREHOUSE_CRATES_STAT_INT(thisWarehouse.iWHSaveSlot, iCrateIndex))
							CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - PERFORM_WAREHOUSE_SPECIAL_CRATE_VALIDATION_CHECKS - Swapped Item new slot: ", (iUpdatedContrabandTotal-1), " model: ", GET_WAREHOUSE_CRATES_STAT_INT(thisWarehouse.iWHSaveSlot, (iUpdatedContrabandTotal-1)))
							
							// Delete Existing Crate
							OBJECT_INDEX oCrateObj = GET_CRATE_OBJECT_FROM_CRATE_SLOT(iCrateIndex)
							IF DOES_ENTITY_EXIST(oCrateObj)
								CLEANUP_SINGLE_WAREHOUSE_CRATE(iCrateIndex)
							ENDIF
							
							// Update Server BD
							serverBD.iCrateID[iCrateIndex] = ENUM_TO_INT(GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(GET_WAREHOUSE_CRATES_STAT_INT(thisWarehouse.iWHSaveSlot, iCrateIndex)))
							serverBD.iCrateID[(iUpdatedContrabandTotal-1)] = ENUM_TO_INT(GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(GET_WAREHOUSE_CRATES_STAT_INT(thisWarehouse.iWHSaveSlot, (iUpdatedContrabandTotal-1))))
							
							// Create
							IF NOT DOES_CRATE_SLOT_HAVE_CRATE_OBJECT(iCrateIndex)
								IF NOT IS_ANY_PLAYER_BLOCKING_CRATE_SPAWN_COORDS(iCrateIndex)
									CREATE_SINGLE_WAREHOUSE_CRATE(iCrateIndex)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_MISSION_CRATES()
	
	INT i
	INT iCratesDelivered
	BOOL bEmptySlot
	
	REPEAT g_ciMaxContrabandDelivery i
		IF g_bWHCrateDelivered[i]
			INT iWarehouseContraband = thisWarehouse.iWHMissionContrabandTotal
			INT iCurrentSlot = (iCratesDelivered+iWarehouseContraband)
			
			IF thisWarehouse.pOwner = PLAYER_ID()
				IF GET_WAREHOUSE_CRATES_STAT_INT(thisWarehouse.iWHSaveSlot, iCurrentSlot) = 0
					bEmptySlot = TRUE
				ENDIF
			ELSE
				IF g_iWarehouseCrateData[iCurrentSlot] = 0
					bEmptySlot = TRUE
				ENDIF
			ENDIF
			
			IF serverBD.iCrateID[iCurrentSlot] = 0 AND bEmptySlot
				CONTRABAND_MISSION_DATA data = GB_GET_REMOTE_PLAYER_CONTRABAND_MISSION_DATA(thisWarehouse.pOwner)
				MODEL_NAMES eCrateModel = SET_WAREHOUSE_CRATE_MODEL(thisWarehouse.iID, iCurrentSlot, data.contrabandType)
				
				INT iRandCount = 0
				IF iCurrentSlot > 0
				AND NOT IS_WAREHOUSE_CRATE_ON_TOP_SHELF(GET_WAREHOUSE_SIZE(thisWarehouse.iID), iCurrentSlot)
					WHILE (eCrateModel = INT_TO_ENUM(MODEL_NAMES, serverBD.iCrateID[(iCurrentSlot-1)]) AND iRandCount < 10)
						eCrateModel = SET_WAREHOUSE_CRATE_MODEL(thisWarehouse.iID, iCurrentSlot, data.contrabandType)
						iRandCount++
					ENDWHILE
				ENDIF
				
				IF eCrateModel != DUMMY_MODEL_FOR_SCRIPT
					IF thisWarehouse.pOwner = PLAYER_ID()
						CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - MAINTAIN_MISSION_CRATES - SETTING STATS - Creating Fake Crate: ", GET_WAREHOUSE_CRATE_MODEL_INDEX(eCrateModel), " in Slot: ", iCurrentSlot)
						SET_WAREHOUSE_CRATES_STAT_INT(thisWarehouse.iWHSaveSlot, iCurrentSlot, GET_WAREHOUSE_CRATE_MODEL_INDEX(eCrateModel))
					ELSE
						CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - MAINTAIN_MISSION_CRATES - BROADCASTING TO WH OWNER - Set Fake Crate: ", GET_WAREHOUSE_CRATE_MODEL_INDEX(eCrateModel), " in Slot: ", iCurrentSlot)
						BROADCAST_DELIVERED_SINGLE_CRATE(thisWarehouse.pOwner, thisWarehouse.iWHSaveSlot, iCurrentSlot, GET_WAREHOUSE_CRATE_MODEL_INDEX(eCrateModel))
					ENDIF
				ENDIF
				
				IF serverBD.iCrateID[iCurrentSlot] = 0
					serverBD.iCrateID[iCurrentSlot] = ENUM_TO_INT(eCrateModel)
				ENDIF
				
				IF NOT DOES_CRATE_SLOT_HAVE_CRATE_OBJECT(iCurrentSlot)
					IF NOT IS_ANY_PLAYER_BLOCKING_CRATE_SPAWN_COORDS(iCurrentSlot)
						CREATE_SINGLE_WAREHOUSE_CRATE(iCurrentSlot)
					ENDIF
				ENDIF
			ENDIF
			iCratesDelivered++
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_MISSION_CRATE_FLAGS()
	IF  NOT g_bWHCrateDelivered[0]
	AND NOT g_bWHCrateDelivered[1]
	AND NOT g_bWHCrateDelivered[2]
		IF thisWarehouse.iWHMissionContrabandTotal != thisWarehouse.iWHContrabandTotal
			thisWarehouse.iWHMissionContrabandTotal = thisWarehouse.iWHContrabandTotal
		ENDIF
	ENDIF
ENDPROC

PROC INITALISE_WAREHOUSE_CRATES()
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - INITALISE_WAREHOUSE_CRATES")
	
	INT iCrateIndex
	thisWarehouse.iCrateCapacity = GET_WAREHOUSE_CRATE_CAPACITY(GET_WAREHOUSE_SIZE(thisWarehouse.iID))
	
	IF IS_WAREHOUSE_OWNER_ID_VALID(NATIVE_TO_INT(thisWarehouse.pOwner))
		thisWarehouse.iWHContrabandTotal = GET_PLAYER_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(thisWarehouse.pOwner, thisWarehouse.iID)
		thisWarehouse.iWHRemoveContrabandTotal = thisWarehouse.iWHContrabandTotal
		thisWarehouse.iWHMissionContrabandTotal = thisWarehouse.iWHContrabandTotal
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - INITALISE_WAREHOUSE_CRATES - Warehouse: ", thisWarehouse.iID, " Contraband Total: ", thisWarehouse.iWHContrabandTotal)
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF thisWarehouse.pOwner != PLAYER_ID()
				thisWarehouse.bWaitingForCrateData = TRUE
				
				// Populate Server BD with my WH owner data
				CLEANUP_GLOBAL_WAREHOUSE_CRATE_DATA()
				BROADCAST_WAREHOUSE_DATA_REQUEST(thisWarehouse.pOwner, thisWarehouse.iID)
				POPULATE_WAREHOUSE_CRATES_DATA_FROM_OWNER()
			ELSE
				// Populate Server BD with my data
				POPULATE_WAREHOUSE_WITH_CRATES()
			ENDIF
			MAINTAIN_MISSION_CRATES()  // Called once here to set crate serverBD
		ENDIF
		
		// Create
		FOR iCrateIndex = 0 TO (thisWarehouse.iWHContrabandTotal-1)
			IF iCrateIndex < thisWarehouse.iCrateCapacity
				IF NOT DOES_CRATE_SLOT_HAVE_CRATE_OBJECT(iCrateIndex)
					IF NOT IS_ANY_PLAYER_BLOCKING_CRATE_SPAWN_COORDS(iCrateIndex)
						IF IS_MODEL_VALID(INT_TO_ENUM(MODEL_NAMES, serverBD.iCrateID[iCrateIndex]))
							CREATE_SINGLE_WAREHOUSE_CRATE(iCrateIndex)
							thisWarehouse.bCrateFadeComplete[iCrateIndex] = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF	
ENDPROC

PROC MAINTAIN_WAREHOUSE_CRATES()
	
	INT iCrateIndex
	INT iUpdatedContrabandTotal = GET_PLAYER_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(thisWarehouse.pOwner, thisWarehouse.iID)
	
	IF thisWarehouse.bWaitingForCrateData
		PERFORM_WAREHOUSE_CRATE_EVENT_CHECKS(iUpdatedContrabandTotal)
		thisWarehouse.bWaitingForCrateData = FALSE
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF thisWarehouse.pOwner != PLAYER_ID()
			PERFORM_WAREHOUSE_CRATE_EVENT_CHECKS(iUpdatedContrabandTotal)
		ENDIF
		MAINTAIN_MISSION_CRATES()
		MAINTAIN_MISSION_CRATE_FLAGS()
	ENDIF
	
	// Adding Contraband Crates
	IF iUpdatedContrabandTotal > thisWarehouse.iWHContrabandTotal 
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - MAINTAIN_WAREHOUSE_CRATES - [CONTRABAND] Warehouse: ", thisWarehouse.iID, " Contraband Current Total: ", thisWarehouse.iWHContrabandTotal, " Contraband New Total: ", iUpdatedContrabandTotal)
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - MAINTAIN_WAREHOUSE_CRATES - [CONTRABAND] Adding: ", (iUpdatedContrabandTotal - thisWarehouse.iWHContrabandTotal), " Crate(s)")
		
		// Find special item array slot if not added to end of array (must be on bottom shelf)
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		AND thisWarehouse.pOwner = PLAYER_ID()
			IF NOT IS_WAREHOUSE_CRATE_A_SPECIAL_ITEM(GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(GET_WAREHOUSE_CRATES_STAT_INT(thisWarehouse.iWHSaveSlot, (iUpdatedContrabandTotal-1))))
				PERFORM_WAREHOUSE_SPECIAL_CRATE_VALIDATION_CHECKS(iUpdatedContrabandTotal)
			ENDIF
		ENDIF
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		AND thisWarehouse.pOwner != PLAYER_ID()
			thisWarehouse.bWaitingForCrateData = TRUE
			BROADCAST_WAREHOUSE_DATA_REQUEST(thisWarehouse.pOwner, thisWarehouse.iID)
		ELSE
			// Update Server BD
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				FOR iCrateIndex = thisWarehouse.iWHContrabandTotal TO (iUpdatedContrabandTotal-1)
					IF iCrateIndex < thisWarehouse.iCrateCapacity
						IF serverBD.iCrateID[iCrateIndex] = 0
							serverBD.iCrateID[iCrateIndex] = ENUM_TO_INT(GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(GET_WAREHOUSE_CRATES_STAT_INT(thisWarehouse.iWHSaveSlot, iCrateIndex)))
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
			
			// Create
			FOR iCrateIndex = thisWarehouse.iWHContrabandTotal TO (iUpdatedContrabandTotal-1)
				IF iCrateIndex < thisWarehouse.iCrateCapacity
					IF NOT DOES_CRATE_SLOT_HAVE_CRATE_OBJECT(iCrateIndex)
						IF NOT IS_ANY_PLAYER_BLOCKING_CRATE_SPAWN_COORDS(iCrateIndex)
							CREATE_SINGLE_WAREHOUSE_CRATE(iCrateIndex)
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
		thisWarehouse.iWHContrabandTotal = iUpdatedContrabandTotal
		
	// Clearing Contraband Crates
	ELIF iUpdatedContrabandTotal < thisWarehouse.iWHContrabandTotal
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - MAINTAIN_WAREHOUSE_CRATES - [CONTRABAND] Warehouse: ", thisWarehouse.iID, " Contraband Current Total: ", thisWarehouse.iWHContrabandTotal, " Contraband New Total: ", iUpdatedContrabandTotal)
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - MAINTAIN_WAREHOUSE_CRATES - [CONTRABAND] Removing: ", (thisWarehouse.iWHContrabandTotal - iUpdatedContrabandTotal), " Crate(s)")
		
		FOR iCrateIndex = iUpdatedContrabandTotal TO (thisWarehouse.iWHContrabandTotal-1)
			IF thisWarehouse.bCrateFadeComplete[iCrateIndex]
				thisWarehouse.bCrateFadeComplete[iCrateIndex] = FALSE
			ENDIF
		ENDFOR
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF thisWarehouse.pOwner = PLAYER_ID()
				PERFORM_WAREHOUSE_SPECIAL_CRATE_VALIDATION_CHECKS(iUpdatedContrabandTotal)
			ELSE
				thisWarehouse.bWaitingForCrateData = TRUE
				BROADCAST_WAREHOUSE_DATA_REQUEST(thisWarehouse.pOwner, thisWarehouse.iID)
			ENDIF
		ENDIF
		
		thisWarehouse.iWHRemoveContrabandTotal = thisWarehouse.iWHContrabandTotal
		thisWarehouse.iWHContrabandTotal = iUpdatedContrabandTotal
	ENDIF
	
	// Wait For Player To Move If Blocking Crate Spawn Coords
	FOR iCrateIndex = 0 TO (iUpdatedContrabandTotal-1)
		IF iCrateIndex < thisWarehouse.iCrateCapacity
			IF serverBD.iCrateID[iCrateIndex] != ENUM_TO_INT(DUMMY_MODEL_FOR_SCRIPT)
			OR serverBD.iCrateID[iCrateIndex] != ENUM_TO_INT(GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(g_iWarehouseCrateData[iCrateIndex]))
				IF NOT DOES_CRATE_SLOT_HAVE_CRATE_OBJECT(iCrateIndex)
					IF NOT IS_ANY_PLAYER_BLOCKING_CRATE_SPAWN_COORDS(iCrateIndex)
						IF IS_MODEL_VALID(INT_TO_ENUM(MODEL_NAMES, serverBD.iCrateID[iCrateIndex]))
							CREATE_SINGLE_WAREHOUSE_CRATE(iCrateIndex)
						ENDIF
					ENDIF
				ENDIF
				// Crate Fade In
				IF NOT IS_ANY_PLAYER_BLOCKING_CRATE_SPAWN_COORDS(iCrateIndex)
					IF NOT thisWarehouse.bCrateFadeComplete[iCrateIndex]
						MAINTAIN_WAREHOUSE_CRATE_FADES(iCrateIndex, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	// Crate Fade Out
	FOR iCrateIndex = 0 TO (thisWarehouse.iWHRemoveContrabandTotal-1)
		IF iCrateIndex < thisWarehouse.iCrateCapacity
			IF serverBD.iCrateID[iCrateIndex] != ENUM_TO_INT(DUMMY_MODEL_FOR_SCRIPT)
				IF iCrateIndex > (iUpdatedContrabandTotal-1)
				OR serverBD.iCrateID[iCrateIndex] != ENUM_TO_INT(GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(g_iWarehouseCrateData[iCrateIndex]))
					MAINTAIN_WAREHOUSE_CRATE_FADES(iCrateIndex, FALSE)
				ENDIF
			ELSE
				IF thisWarehouse.iWHRemoveContrabandTotal > iUpdatedContrabandTotal
					thisWarehouse.iWHRemoveContrabandTotal -= 1
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	// Mission Crates Fade
	FOR iCrateIndex = iUpdatedContrabandTotal TO (iUpdatedContrabandTotal+2)
		IF iCrateIndex < thisWarehouse.iCrateCapacity
			IF serverBD.iCrateID[iCrateIndex] != 0
				IF NOT DOES_CRATE_SLOT_HAVE_CRATE_OBJECT(iCrateIndex)
					IF NOT IS_ANY_PLAYER_BLOCKING_CRATE_SPAWN_COORDS(iCrateIndex)
						IF IS_MODEL_VALID(INT_TO_ENUM(MODEL_NAMES, serverBD.iCrateID[iCrateIndex]))
							CREATE_SINGLE_WAREHOUSE_CRATE(iCrateIndex)
						ENDIF
					ENDIF
				ENDIF
				IF NOT IS_ANY_PLAYER_BLOCKING_CRATE_SPAWN_COORDS(iCrateIndex)
					IF NOT thisWarehouse.bCrateFadeComplete[iCrateIndex]
						MAINTAIN_WAREHOUSE_CRATE_FADES(iCrateIndex, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════╡ VEHICLE UPGRADE MENU  ╞═══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC CLEANUP_VEH_UPGRADE_MENU(WAREHOUSE_UPGRADE_CABINET& sVehUpgrade,BOOL bForceCleanup = FALSE)
	
	RELEASE_CONTEXT_INTENTION(sVehUpgrade.iVehUpgradeContext)
	sVehUpgrade.iVehUpgradeContext = NEW_CONTEXT_INTENTION
	sVehUpgrade.iMenuCurrentItem = 0
	sVehUpgrade.iMenuTopItem = 0
	sVehUpgrade.eCurrentMenu = WV_MAIN_MENU
	sVehUpgrade.bMenuInitialised = FALSE
	sVehUpgrade.bReBuildMenu = FALSE	
	
	IF IS_BIT_SET(sVehUpgrade.iToolBoxBS,TOOLBOX_ENTER_ANIM_RUNNING)
		CLEAR_BIT(sVehUpgrade.iToolBoxBS,TOOLBOX_ENTER_ANIM_RUNNING)
	ENDIF
	
	IF IS_BIT_SET(sVehUpgrade.iToolBoxBS,TOOLBOX_IDLE_ANIM_RUNNING)
		CLEAR_BIT(sVehUpgrade.iToolBoxBS,TOOLBOX_IDLE_ANIM_RUNNING)
	ENDIF
	
	IF IS_BIT_SET(sVehUpgrade.iToolBoxBS,TOOLBOX_EXIT_ANIM_RUNNING)
		CLEAR_BIT(sVehUpgrade.iToolBoxBS,TOOLBOX_EXIT_ANIM_RUNNING)
	ENDIF
	
	
	IF bForceCleanup 
		IF DOES_ENTITY_EXIST(sVehUpgrade.objectCabinet)
			DELETE_OBJECT(sVehUpgrade.objectCabinet)
		ENDIF
		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("ShopUI_Title_Exec_VechUpgrade")
		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPShopSale")
	ENDIF 
	
	sVehUpgrade.eWCabinetState = WCABINET_STAGE_INIT
ENDPROC

/// PURPOSE:
///    sets up controls for office menus
/// PARAMS:
///    sWarehouseMenuInput - inputs
///    iMenuCurrentItem - current menu item 
PROC SETUP_WAREHOUSE_MENU_CONTROLS(WAREHOUSE_MENU_INPUT& sWarehouseMenuInput, INT& iMenuCurrentItem)

	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(sWarehouseMenuInput.iLeftX,sWarehouseMenuInput.iLeftY,sWarehouseMenuInput.iRightX,sWarehouseMenuInput.iRightY )
	
	// Set the input flag states
	sWarehouseMenuInput.bAccept		= (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
	sWarehouseMenuInput.bBack		= (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
	sWarehouseMenuInput.bPrevious	= ((sWarehouseMenuInput.iLeftY < -64) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)) 
	sWarehouseMenuInput.bNext		= ((sWarehouseMenuInput.iLeftY > 64) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN))	
	
	IF LOAD_MENU_ASSETS()
	AND GET_PAUSE_MENU_STATE() = PM_INACTIVE
	AND NOT IS_SYSTEM_UI_BEING_DISPLAYED()
	AND NOT IS_WARNING_MESSAGE_ACTIVE()
	AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
	AND NOT g_sShopSettings.bProcessStoreAlert
	
		// Mouse control support  
		IF IS_PC_VERSION()	
		AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
			IF IS_USING_CURSOR(FRONTEND_CONTROL)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)	
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_ACCEPT)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_UP)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
				IF ((g_iMenuCursorItem = MENU_CURSOR_NO_ITEM) OR (g_iMenuCursorItem = MENU_CURSOR_NO_CAMERA_MOVE) OR (g_iMenuCursorItem = MENU_CURSOR_DRAG_CAM))
					ENABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
					ENABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
				ELSE
					DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
					DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
				ENDIF	
				HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
				HANDLE_MENU_CURSOR(FALSE)
			ENDIF
			
			// Mouse select
			IF IS_MENU_CURSOR_ACCEPT_PRESSED()
				IF g_iMenuCursorItem = iMenuCurrentItem
					sWarehouseMenuInput.bAccept = TRUE
				ELSE
					iMenuCurrentItem = g_iMenuCursorItem
					SET_CURRENT_MENU_ITEM(iMenuCurrentItem)		
				ENDIF		
			ENDIF
		
			// Menu cursor back
			IF IS_MENU_CURSOR_CANCEL_PRESSED()
				sWarehouseMenuInput.bBack = TRUE
			ENDIF
			
			// Mouse scroll up
			IF IS_MENU_CURSOR_SCROLL_UP_PRESSED()
				sWarehouseMenuInput.bPrevious = TRUE
			ENDIF
			
			// Mouse scroll down
			IF IS_MENU_CURSOR_SCROLL_DOWN_PRESSED()
				sWarehouseMenuInput.bNext = TRUE
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///   Gets personal assistant service menu names
/// PARAMS:
///    eCurrentMenu - personal assistant servicet sub menu 
///    bTitle - is it menu title?
/// RETURNS:
///    string for menu itmes or titles
FUNC STRING GET_VEH_UPGRADE_MENU_ITEM_NAMES(WAREHOUSE_VEH_MENU eCurrentMenu,BOOL bTitle = FALSE)
	
	SWITCH eCurrentMenu		
		CASE WV_BOAT_MENU 			IF	!bTitle	RETURN	"WAR_VEH_MENUI1"  ELSE	RETURN	"WAR_VEH_MENUT1"	ENDIF	BREAK 
		CASE WV_TRUCK_MENU			IF	!bTitle	RETURN	"WAR_VEH_MENUI2"  ELSE	RETURN	"WAR_VEH_MENUT2"	ENDIF	BREAK 
		CASE WV_PLANE_MENU			IF	!bTitle	RETURN	"WAR_VEH_MENUI3"  ELSE	RETURN	"WAR_VEH_MENUT3"	ENDIF	BREAK
	ENDSWITCH
	RETURN ""
	
ENDFUNC
/// PURPOSE:
///    initialize vehicle upgrade cabinet 
PROC INIT_VEH_UPGRADE_CABINET(WAREHOUSE_UPGRADE_CABINET& sVehUpgrade)
	
	GET_WAREHOUSE_VEH_UPGRADE_CABINET_COORDS(GET_WAREHOUSE_SIZE(thisWarehouse.iID),sVehUpgrade.sCabinetPos)
	GET_WAREHOUSE_TOOLBOX_SCENE_DETAIL(GET_WAREHOUSE_SIZE(thisWarehouse.iID),sVehUpgrade.sToolBoxScene)
	GET_WAREHOUSE_TOOLBOX_TRIGGER_LOCATE(GET_WAREHOUSE_SIZE(thisWarehouse.iID),sVehUpgrade.sToolBoxTriggerLocate)
	sVehUpgrade.sToolBoxDic = "anim@amb@warehouse@toolbox@"
	
	IF NOT DOES_ENTITY_EXIST(sVehUpgrade.objectCabinet)
		REQUEST_MODEL(EX_PROP_EX_TOOLCHEST_01)
		IF HAS_MODEL_LOADED(EX_PROP_EX_TOOLCHEST_01)
			thisWarehouse.sWUpgradeCabinet.objectCabinet = CREATE_OBJECT_NO_OFFSET(EX_PROP_EX_TOOLCHEST_01,sVehUpgrade.sCabinetPos.vLoc,FALSE,FALSE)
			SET_ENTITY_ROTATION(sVehUpgrade.objectCabinet,sVehUpgrade.sCabinetPos.vRot)
			SET_MODEL_AS_NO_LONGER_NEEDED(EX_PROP_EX_TOOLCHEST_01)
			CDEBUG1LN(DEBUG_PROPERTY,"INIT_VEH_UPGRADE_CABINET - Cabinet object create")
		ELSE
			CDEBUG1LN(DEBUG_PROPERTY,"INIT_VEH_UPGRADE_CABINET - EX_PROP_EX_TOOLCHEST_01 not loaded")
		ENDIF
	ELSE
		IF IS_WAREHOUSE_OWNER_ID_VALID(NATIVE_TO_INT(thisWarehouse.pOwner))
		AND PLAYER_ID() = thisWarehouse.pOwner
			REQUEST_STREAMED_TEXTURE_DICT("ShopUI_Title_Exec_VechUpgrade")
			REQUEST_STREAMED_TEXTURE_DICT("MPShopSale")
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("ShopUI_Title_Exec_VechUpgrade")
			AND HAS_STREAMED_TEXTURE_DICT_LOADED("MPShopSale")
				CDEBUG1LN(DEBUG_PROPERTY,"INIT_VEH_UPGRADE_CABINET - Cabinet object exists")
				sVehUpgrade.eWCabinetState = WCABINET_STAGE_WAITING_TO_TRIGGER
			ENDIF
		ENDIF	
	ENDIF
	
ENDPROC

PROC BUILD_DELIVERY_VEH_UPGRADE_MAIN_MENU(WAREHOUSE_UPGRADE_CABINET& sVehUpgrade)

	CLEAR_MENU_DATA()
	SET_MENU_TITLE("WAR_VEH_TITLE")
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
	SET_MENU_HEADER_COLOUR(47,88,109,255,TRUE)
	SET_MENU_USES_HEADER_GRAPHIC(TRUE, "ShopUI_Title_Exec_VechUpgrade", "ShopUI_Title_Exec_VechUpgrade")
	
	INT iNumVeh
	REPEAT VEH_UPGRADE_MAX_SUB_MENU_ITEM iNumVeh
		ADD_MENU_ITEM_TEXT(iNumVeh, GET_VEH_UPGRADE_MENU_ITEM_NAMES(INT_TO_ENUM(WAREHOUSE_VEH_MENU,iNumVeh )),1)
		ADD_MENU_ITEM_TEXT_COMPONENT_STRING(GET_VEH_UPGRADE_MENU_ITEM_NAMES(INT_TO_ENUM(WAREHOUSE_VEH_MENU,iNumVeh )))
	ENDREPEAT	
	
	SET_CURRENT_MENU_ITEM(sVehUpgrade.iMenuCurrentItem)
	sVehUpgrade.bReBuildMenu = TRUE
	sVehUpgrade.eCurrentMenu = WV_MAIN_MENU
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
	
ENDPROC
PROC UPDATE_VEH_MAIN_MENU(WAREHOUSE_UPGRADE_CABINET& sVehUpgrade)

	BOOL bKeyPressSafe = TRUE
	IF GET_PAUSE_MENU_STATE() != PM_INACTIVE
	OR IS_SYSTEM_UI_BEING_DISPLAYED()
	OR IS_WARNING_MESSAGE_ACTIVE()
	OR NETWORK_TEXT_CHAT_IS_TYPING()
	OR g_sShopSettings.bProcessStoreAlert
		bKeyPressSafe = FALSE
	ENDIF
	
	// Block movememnt when it's not safe.
	IF NOT bKeyPressSafe
		// do nothing
		
	// Navigate menu
	ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP) AND ALLOW_ANALOGUE_MOVEMENT(sVehUpgrade.scrollDelay,sVehUpgrade.sWarehouseMenuInput.iMoveUp) 
	OR sVehUpgrade.sWarehouseMenuInput.bPrevious AND ALLOW_ANALOGUE_MOVEMENT(sVehUpgrade.scrollDelay,sVehUpgrade.sWarehouseMenuInput.iMoveUp)
		IF sVehUpgrade.sWarehouseMenuInput.iMoveUp > 0
			sVehUpgrade.iMenuCurrentItem--
			IF sVehUpgrade.iMenuCurrentItem < 0
				sVehUpgrade.iMenuCurrentItem = VEH_UPGRADE_MAX_SUB_MENU_ITEM - 1
			ENDIF
			SET_CURRENT_MENU_ITEM(sVehUpgrade.iMenuCurrentItem)	
			PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "DLC_EXEC_Warehouse_Upgrade_Bench_Sounds")
		ENDIF	
	ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN) AND ALLOW_ANALOGUE_MOVEMENT(sVehUpgrade.scrollDelay,sVehUpgrade.sWarehouseMenuInput.iMoveUp)
	OR sVehUpgrade.sWarehouseMenuInput.bNext AND ALLOW_ANALOGUE_MOVEMENT(sVehUpgrade.scrollDelay,sVehUpgrade.sWarehouseMenuInput.iMoveUp)
		IF sVehUpgrade.sWarehouseMenuInput.iMoveUp < 0
			sVehUpgrade.iMenuCurrentItem++
			IF sVehUpgrade.iMenuCurrentItem > VEH_UPGRADE_MAX_SUB_MENU_ITEM - 1
				sVehUpgrade.iMenuCurrentItem = 0
			ENDIF
			SET_CURRENT_MENU_ITEM(sVehUpgrade.iMenuCurrentItem)
			PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "DLC_EXEC_Warehouse_Upgrade_Bench_Sounds")
		ENDIF
	// Exit menu
	ELIF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL))
	OR sVehUpgrade.sWarehouseMenuInput.bBack 
		sVehUpgrade.eWCabinetState = WCABINET_STAGE_EXIT_ANIM
		PLAY_SOUND_FRONTEND(-1, "CANCEL", "DLC_EXEC_Warehouse_Upgrade_Bench_Sounds")
		sVehUpgrade.bMenuInitialised = FALSE
	// Select
	ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
	OR sVehUpgrade.sWarehouseMenuInput.bAccept
		PLAY_SOUND_FRONTEND(-1,"SELECT","DLC_EXEC_Warehouse_Upgrade_Bench_Sounds")
		IF sVehUpgrade.iMenuCurrentItem >= 0 
		AND sVehUpgrade.iMenuCurrentItem < VEH_UPGRADE_MAX_SUB_MENU_ITEM 
			sVehUpgrade.eCurrentMenu = INT_TO_ENUM(WAREHOUSE_VEH_MENU,sVehUpgrade.iMenuCurrentItem)
		ENDIF
		sVehUpgrade.bReBuildMenu = TRUE
		sVehUpgrade.iMenuTopItem = GET_TOP_MENU_ITEM()
		sVehUpgrade.iMenuCurrentItem = 0
		sVehUpgrade.bMenuInitialised = FALSE
	ENDIF
ENDPROC

/// PURPOSE:
///    Get veh upgrade costs 
FUNC INT GET_VEHICLE_UPGRADE_COST(WAREHOUSE_VEH_MENU eMenu, INT iItem)
	SWITCH eMenu
		CASE WV_BOAT_MENU
			SWITCH iItem
				CASE 0 RETURN g_sMPTunables.iEXEC_UPGRADES_BOAT_ARMOR_PRICE  BREAK // Boat armor
				CASE 1 RETURN g_sMPTunables.iEXEC_UPGRADES_BOAT_SPEED_PRICE  BREAK // Boat speed
			ENDSWITCH
		BREAK
		CASE WV_PLANE_MENU
			SWITCH iItem
				CASE 0 RETURN g_sMPTunables.iEXEC_UPGRADES_PLANE_ARMOR_PRICE  BREAK // Plane armor 
				CASE 1 RETURN g_sMPTunables.iEXEC_UPGRADES_PLANE_JAMMER_PRICE BREAK // Plane jammer 
			ENDSWITCH
		BREAK
		CASE WV_TRUCK_MENU
			SWITCH iItem
				CASE 0 RETURN g_sMPTunables.iEXEC_UPGRADES_TRUCK_ARMOR_PRICE  BREAK // Truck armor
				CASE 1 RETURN g_sMPTunables.iEXEC_UPGRADES_TRUCK_TIRES_PRICE  BREAK // Truck bpt
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN 0
ENDFUNC

FUNC STRING GET_VEH_UPGRADE_ITEM_TEXT(WAREHOUSE_VEH_MENU eMenu, INT iItem)
	SWITCH eMenu
		CASE WV_BOAT_MENU
			SWITCH iItem
				CASE 0 RETURN "WAR_BOAT_UP_1" BREAK // Boat armor
				CASE 1 RETURN "WAR_BOAT_UP_2" BREAK // Boat speed
			ENDSWITCH
		BREAK
		CASE WV_PLANE_MENU
			SWITCH iItem
				CASE 0 RETURN "WAR_BOAT_UP_1" BREAK // Plane armor
				CASE 1 RETURN "WAR_PLANE_UP_2" BREAK // Plane jammer
			ENDSWITCH
		BREAK
		CASE WV_TRUCK_MENU
			SWITCH iItem
				CASE 0 RETURN "WAR_BOAT_UP_1" BREAK // Truck armor
				CASE 1 RETURN "WAR_TRUCK_UP_2" BREAK // Truck bpt
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC INT GET_VEHICLE_UPGRADE_CATALOG_KEY(WAREHOUSE_VEH_MENU eMenu, INT iItem)
	SWITCH eMenu
		CASE WV_BOAT_MENU
			SWITCH iItem
				CASE 0 RETURN HASH("WP_VU_BOATARM_t0_v0") BREAK // Boat armor
				CASE 1 RETURN HASH("WP_VU_BOATSPEED_t0_v0") BREAK // Boat speed
			ENDSWITCH
		BREAK
		CASE WV_PLANE_MENU
			SWITCH iItem
				CASE 0 RETURN HASH("WP_VU_PLAARM_t0_v0") BREAK // Plane armor
				CASE 1 RETURN HASH("WP_VU_PLAJAM_t0_v0") BREAK // Plane jammer
			ENDSWITCH
		BREAK
		CASE WV_TRUCK_MENU
			SWITCH iItem
				CASE 0 RETURN HASH("WP_VU_TRKARM_t0_v0") BREAK // Truck armor
				CASE 1 RETURN HASH("WP_VU_TRKTYR_t0_v0") BREAK // Truck bpt
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Get model name for vehicle upgrade
///    used for purchase item hash 
FUNC MODEL_NAMES GET_VEHICLE_UPGRADE_MODEL_NAME(WAREHOUSE_VEH_MENU eMenu, INT iItem)
	SWITCH eMenu
		CASE WV_BOAT_MENU
			SWITCH iItem
				CASE 0 
				CASE 1 RETURN TUG BREAK 
			ENDSWITCH
		BREAK
		CASE WV_PLANE_MENU
			SWITCH iItem
				CASE 0 RETURN CUBAN800 BREAK 
				CASE 1 RETURN TITAN BREAK 
			ENDSWITCH
		BREAK
		CASE WV_TRUCK_MENU
			SWITCH iItem
				CASE 0 
				CASE 1 RETURN BRICKADE BREAK 
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

INT iUpgradeTransactionState = GENERIC_TRANSACTION_STATE_DEFAULT

FUNC BOOL PURCHASE_VEHICLE_UPGRADE(WAREHOUSE_UPGRADE_CABINET& sVehUpgrade)

	BOOL bSuccess = TRUE
	INT iItemKey,iItemCost

	IF USE_SERVER_TRANSACTIONS()
	
		bSuccess = FALSE
			
		iItemKey = GET_VEHICLE_UPGRADE_CATALOG_KEY(sVehUpgrade.eCurrentMenu, sVehUpgrade.iMenuCurrentItem)
		iItemCost = GET_VEHICLE_UPGRADE_COST(sVehUpgrade.eCurrentMenu, sVehUpgrade.iMenuCurrentItem)
		
		IF PROCESS_GENERIC_ITEM_TRANSACTION(CATEGORY_WEAPON, NET_SHOP_ACTION_SPEND, iItemKey, 1, iItemCost, iUpgradeTransactionState, DEFAULT, DEFAULT, ENUM_TO_INT(GET_VEHICLE_UPGRADE_MODEL_NAME(sVehUpgrade.eCurrentMenu,sVehUpgrade.iMenuCurrentItem)),GET_HASH_KEY(GET_VEH_UPGRADE_ITEM_TEXT(sVehUpgrade.eCurrentMenu,sVehUpgrade.iMenuCurrentItem)))
			IF (iUpgradeTransactionState = GENERIC_TRANSACTION_STATE_SUCCESS)
				PRINTLN("PURCHASE_VEHICLE_UPGRADE - Transaction success")
				bSuccess = TRUE
			ELSE
				PRINTLN("PURCHASE_VEHICLE_UPGRADE - Transaction fail")
			ENDIF
			iUpgradeTransactionState = GENERIC_TRANSACTION_STATE_DEFAULT
		ELSE
			PRINTLN("PURCHASE_VEHICLE_UPGRADE - Processing transaction")
		ENDIF
	ENDIF
	
	IF bSuccess
		
		PLAY_SOUND_FRONTEND(-1,"Purchase_Upgrade","DLC_EXEC_Warehouse_Upgrade_Bench_Sounds")
		
		INT iBitValue
		iBitValue = GET_DELIVERY_VEH_UPGRADE()
		IF sVehUpgrade.eCurrentMenu = WV_BOAT_MENU
			IF sVehUpgrade.iMenuCurrentItem = 0
				SET_BIT(iBitValue,0)
			ELSE
				SET_BIT(iBitValue,1)
			ENDIF
		ELIF sVehUpgrade.eCurrentMenu = WV_PLANE_MENU
			IF sVehUpgrade.iMenuCurrentItem = 0
				SET_BIT(iBitValue,2)
			ELSE
				SET_BIT(iBitValue,3)
			ENDIF
		ELIF sVehUpgrade.eCurrentMenu = WV_TRUCK_MENU
			IF sVehUpgrade.iMenuCurrentItem = 0
				SET_BIT(iBitValue,4)
			ELSE
				SET_BIT(iBitValue,5)
			ENDIF
		ENDIF
		
		iItemCost = GET_VEHICLE_UPGRADE_COST(sVehUpgrade.eCurrentMenu, sVehUpgrade.iMenuCurrentItem)
		
		SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_DELIVERY_VEH_UPGRADES, iBitValue)
		
		IF NOT USE_SERVER_TRANSACTIONS()
			BOOL fromBank = NETWORK_GET_VC_BANK_BALANCE()  >= iItemCost
			BOOL fromBankAndWallet = NOT fromBank AND NETWORK_GET_VC_BANK_BALANCE() > 0
			IF iItemCost > 0			
	    		NETWORK_BUY_ITEM(iItemCost,ENUM_TO_INT(GET_VEHICLE_UPGRADE_MODEL_NAME(sVehUpgrade.eCurrentMenu,sVehUpgrade.iMenuCurrentItem)) ,PURCHASE_ENDMISSIONCARUP,DEFAULT,fromBank,DEFAULT,DEFAULT,GET_HASH_KEY(GET_VEH_UPGRADE_ITEM_TEXT(sVehUpgrade.eCurrentMenu,sVehUpgrade.iMenuCurrentItem)),DEFAULT,fromBankAndWallet)
				PRINTLN("PURCHASE_VEHICLE_UPGRADE - item cost: ",iItemCost)
				PRINTLN("PURCHASE_VEHICLE_UPGRADE - Call NETWORK_BUY_ITEM")
			ENDIF
		ENDIF
		
		sVehUpgrade.bReBuildMenu = TRUE
		sVehUpgrade.iMenuTopItem = GET_TOP_MENU_ITEM()
		sVehUpgrade.bMenuInitialised = FALSE
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if current menu item is available
///    checks if item tunable is not disbaled or item is not purchased
///    returns true if items is purchased or tunable is disabled
FUNC BOOL IS_VEH_UPGRADE_MENU_OPTION_UNAVAILABLE(WAREHOUSE_VEH_MENU eCurrentMenu, INT iItem)
	INT iBitValue = GET_DELIVERY_VEH_UPGRADE()
	SWITCH eCurrentMenu
		CASE WV_BOAT_MENU
			SWITCH iItem
				CASE 0
					IF IS_BIT_SET(iBitValue,0)
					OR g_sMPTunables.bexec_upgrades_disable_boat_armor 
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1
					IF IS_BIT_SET(iBitValue,1)
					OR g_sMPTunables.bexec_upgrades_disable_boat_speed
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE WV_PLANE_MENU
			SWITCH iItem
				CASE 0
					IF IS_BIT_SET(iBitValue,2)
					OR g_sMPTunables.bexec_upgrades_disable_plane_armor 
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1
					IF IS_BIT_SET(iBitValue,3)
					OR g_sMPTunables.bexec_upgrades_disable_plane_jammer  
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE WV_TRUCK_MENU
			SWITCH iItem
				CASE 0
					IF IS_BIT_SET(iBitValue,4)
					OR g_sMPTunables.bexec_upgrades_disable_truck_armor
						RETURN TRUE
					ENDIF
				BREAK	
				CASE 1
					IF IS_BIT_SET(iBitValue,5)
					OR g_sMPTunables.bexec_upgrades_disable_truck_tires
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Update veh upgrade sub menus
PROC UPDATE_VEH_SUB_MENU(WAREHOUSE_UPGRADE_CABINET& sVehUpgrade)

	// Ignore keypress when it's not safe.
	BOOL bKeyPressSafe = TRUE
	IF IS_PAUSE_MENU_ACTIVE()
	OR IS_SYSTEM_UI_BEING_DISPLAYED()
	OR IS_WARNING_MESSAGE_ACTIVE()
	OR NETWORK_TEXT_CHAT_IS_TYPING()
	OR g_sShopSettings.bProcessStoreAlert
		bKeyPressSafe = FALSE
	ENDIF

	// Purchase
	IF (bKeyPressSafe AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT))
	OR (bKeyPressSafe AND sVehUpgrade.sWarehouseMenuInput.bAccept)
	OR iUpgradeTransactionState != GENERIC_TRANSACTION_STATE_DEFAULT
	
		PRINTLN("UPDATE_VEH_SUB_MENU")
		PRINTLN("...INPUT_FRONTEND_ACCEPT = ", IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT))
		PRINTLN("...bAccept = ", sVehUpgrade.sWarehouseMenuInput.bAccept)
		PRINTLN("...iUpgradeTransactionState != GENERIC_TRANSACTION_STATE_DEFAULT = ", iUpgradeTransactionState != GENERIC_TRANSACTION_STATE_DEFAULT)
		
		BOOL bCanAffordItem = NETWORK_CAN_SPEND_MONEY(GET_VEHICLE_UPGRADE_COST(sVehUpgrade.eCurrentMenu,sVehUpgrade.iMenuCurrentItem), FALSE, FALSE, TRUE)
		BOOL bTransactionInProgress = iUpgradeTransactionState != GENERIC_TRANSACTION_STATE_DEFAULT
		
		IF (bCanAffordItem = FALSE
		AND bTransactionInProgress = FALSE)
		OR IS_VEH_UPGRADE_MENU_OPTION_UNAVAILABLE(sVehUpgrade.eCurrentMenu,sVehUpgrade.iMenuCurrentItem)
			IF bCanAffordItem = FALSE
				STORE_LAST_VIEWED_SHOP_ITEM_FOR_COMMERCE_STORE(GET_HASH_KEY(GET_VEH_UPGRADE_ITEM_TEXT(sVehUpgrade.eCurrentMenu,sVehUpgrade.iMenuCurrentItem)), GET_VEHICLE_UPGRADE_COST(sVehUpgrade.eCurrentMenu,sVehUpgrade.iMenuCurrentItem)) 
				LAUNCH_STORE_CASH_ALERT(FALSE, STAT_SAVETYPE_END_SHOPPING, SPL_STORE)
			ENDIF
			PLAY_SOUND_FRONTEND(-1, "ERROR", "DLC_EXEC_Warehouse_Upgrade_Bench_Sounds")
		ELIF bCanAffordItem
		OR bTransactionInProgress
			IF NOT IS_VEH_UPGRADE_MENU_OPTION_UNAVAILABLE(sVehUpgrade.eCurrentMenu,sVehUpgrade.iMenuCurrentItem)
				PURCHASE_VEHICLE_UPGRADE(sVehUpgrade)
			ENDIF	
		ENDIF	
		sVehUpgrade.bReBuildMenu = TRUE
		sVehUpgrade.bMenuInitialised = FALSE
		
	// Block movememnt when it's not safe.
	ELIF NOT bKeyPressSafe
		// do nothing
		
	// Back to main menu
	ELIF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL))
	OR sVehUpgrade.sWarehouseMenuInput.bBack
		sVehUpgrade.iMenuCurrentItem = ENUM_TO_INT(sVehUpgrade.eCurrentMenu)
		sVehUpgrade.eCurrentMenu = WV_MAIN_MENU
		sVehUpgrade.bReBuildMenu = TRUE
		PLAY_SOUND_FRONTEND(-1, "BACK", "DLC_EXEC_Warehouse_Upgrade_Bench_Sounds")
		sVehUpgrade.bMenuInitialised = FALSE
		
		sVehUpgrade.iMenuTopItem = GET_TOP_MENU_ITEM()
	// Navigate menu
	ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP) AND ALLOW_ANALOGUE_MOVEMENT(sVehUpgrade.scrollDelay,sVehUpgrade.sWarehouseMenuInput.iMoveUp) 
	OR sVehUpgrade.sWarehouseMenuInput.bPrevious AND ALLOW_ANALOGUE_MOVEMENT(sVehUpgrade.scrollDelay,sVehUpgrade.sWarehouseMenuInput.iMoveUp)
		IF sVehUpgrade.sWarehouseMenuInput.iMoveUp > 0
			sVehUpgrade.iMenuCurrentItem --
			IF sVehUpgrade.iMenuCurrentItem  < 0
				sVehUpgrade.iMenuCurrentItem  = 1
			ENDIF
			SET_CURRENT_MENU_ITEM(sVehUpgrade.iMenuCurrentItem)	
			PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "DLC_EXEC_Warehouse_Upgrade_Bench_Sounds")
		ENDIF	
	ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN) AND ALLOW_ANALOGUE_MOVEMENT(sVehUpgrade.scrollDelay,sVehUpgrade.sWarehouseMenuInput.iMoveUp)
	OR sVehUpgrade.sWarehouseMenuInput.bNext AND ALLOW_ANALOGUE_MOVEMENT(sVehUpgrade.scrollDelay,sVehUpgrade.sWarehouseMenuInput.iMoveUp)
		IF sVehUpgrade.sWarehouseMenuInput.iMoveUp < 0
			sVehUpgrade.iMenuCurrentItem++
			IF sVehUpgrade.iMenuCurrentItem > 1
				sVehUpgrade.iMenuCurrentItem = 0
			ENDIF
			SET_CURRENT_MENU_ITEM(sVehUpgrade.iMenuCurrentItem)
			PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "DLC_EXEC_Warehouse_Upgrade_Bench_Sounds")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Update menu item descriptions
PROC HANDLE_VEH_MENU_DESCRIPTION(WAREHOUSE_UPGRADE_CABINET& sVehUpgrade)
 	INT iBitValue = GET_DELIVERY_VEH_UPGRADE()
	SWITCH sVehUpgrade.eCurrentMenu
		CASE WV_MAIN_MENU
			IF sVehUpgrade.iMenuCurrentItem  = 0
				SET_CURRENT_MENU_ITEM_DESCRIPTION("WAR_VEH_DES_M1")
			ELIF sVehUpgrade.iMenuCurrentItem  = 1
				SET_CURRENT_MENU_ITEM_DESCRIPTION("WAR_VEH_DES_M2")
			ELIF sVehUpgrade.iMenuCurrentItem  = 2
				SET_CURRENT_MENU_ITEM_DESCRIPTION("WAR_VEH_DES_M3")
			ENDIF
		BREAK
		CASE WV_BOAT_MENU
			IF sVehUpgrade.iMenuCurrentItem  = 0
				IF IS_BIT_SET(iBitValue,0) 
					SET_CURRENT_MENU_ITEM_DESCRIPTION("WAR_UPG_PURC")
				ELIF g_sMPTunables.bexec_upgrades_disable_boat_armor 
					SET_CURRENT_MENU_ITEM_DESCRIPTION("PM_INF_QMF5")
				ELSE
					SET_CURRENT_MENU_ITEM_DESCRIPTION("WAR_VEH_DES_I5")
				ENDIF
			ELSE
				IF IS_BIT_SET(iBitValue,1) 
					SET_CURRENT_MENU_ITEM_DESCRIPTION("WAR_UPG_PURC")
				ELIF g_sMPTunables.bexec_upgrades_disable_boat_speed
					SET_CURRENT_MENU_ITEM_DESCRIPTION("PM_INF_QMF5")
				ELSE
					SET_CURRENT_MENU_ITEM_DESCRIPTION("WAR_VEH_DES_I6")
				ENDIF
			ENDIF		
		BREAK
		CASE WV_PLANE_MENU
			IF sVehUpgrade.iMenuCurrentItem  = 0
				IF IS_BIT_SET(iBitValue,2) 
					SET_CURRENT_MENU_ITEM_DESCRIPTION("WAR_UPG_PURC")
				ELIF g_sMPTunables.bexec_upgrades_disable_plane_armor 
					SET_CURRENT_MENU_ITEM_DESCRIPTION("PM_INF_QMF5")
				ELSE	
					SET_CURRENT_MENU_ITEM_DESCRIPTION("WAR_VEH_DES_I3")
				ENDIF
			ELSE
				IF IS_BIT_SET(iBitValue,3) 
					SET_CURRENT_MENU_ITEM_DESCRIPTION("WAR_UPG_PURC")
				ELIF g_sMPTunables.bexec_upgrades_disable_plane_jammer
					SET_CURRENT_MENU_ITEM_DESCRIPTION("PM_INF_QMF5")
				ELSE
					SET_CURRENT_MENU_ITEM_DESCRIPTION("WAR_VEH_DES_I4")
				ENDIF
			ENDIF	
		BREAK
		CASE WV_TRUCK_MENU
			IF sVehUpgrade.iMenuCurrentItem  = 0
				IF IS_BIT_SET(iBitValue,4) 
					SET_CURRENT_MENU_ITEM_DESCRIPTION("WAR_UPG_PURC")
				ELIF g_sMPTunables.bexec_upgrades_disable_truck_armor
					SET_CURRENT_MENU_ITEM_DESCRIPTION("PM_INF_QMF5")
				ELSE 
					SET_CURRENT_MENU_ITEM_DESCRIPTION("WAR_VEH_DES_I1")
				ENDIF
			ELSE
				IF IS_BIT_SET(iBitValue,5) 
					SET_CURRENT_MENU_ITEM_DESCRIPTION("WAR_UPG_PURC")
				ELIF g_sMPTunables.bexec_upgrades_disable_truck_tires
					SET_CURRENT_MENU_ITEM_DESCRIPTION("PM_INF_QMF5")
				ELSE
					SET_CURRENT_MENU_ITEM_DESCRIPTION("WAR_VEH_DES_I2")
				ENDIF
			ENDIF	
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    update vehicle upgrade menu
PROC UPDATE_VEH_UPGRADE_MENU(WAREHOUSE_UPGRADE_CABINET& sVehUpgrade)

	SWITCH sVehUpgrade.eCurrentMenu
		CASE WV_MAIN_MENU
			UPDATE_VEH_MAIN_MENU(sVehUpgrade)
		BREAK
		DEFAULT 
			UPDATE_VEH_SUB_MENU(sVehUpgrade)
		BREAK
	ENDSWITCH
	
	IF sVehUpgrade.bReBuildMenu = FALSE
		HANDLE_VEH_MENU_DESCRIPTION(sVehUpgrade)
	ENDIF	
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
	
ENDPROC

// Check if any of the work bench upgrade items are discounted
FUNC BOOL IS_WORKBENCH_UPGRADE_DISCOUNT_ACTIVE(WAREHOUSE_UPGRADE_CABINET sVehUpgrade, INT iItem)
	INT iBitValue = g_sMPTunables.iWAREHOUSE_WORKBENCH_UPGRADE_DISCOUNT_BS

	SWITCH sVehUpgrade.eCurrentMenu
		CASE WV_BOAT_MENU
			SWITCH iItem
				CASE 0
					RETURN IS_BIT_SET(iBitValue,0) 
				CASE 1
					RETURN IS_BIT_SET(iBitValue,1)
			ENDSWITCH	
		BREAK
		CASE WV_PLANE_MENU
			SWITCH iItem
				CASE 0
					RETURN IS_BIT_SET(iBitValue,2) 
				CASE 1
					RETURN IS_BIT_SET(iBitValue,3) 
			ENDSWITCH	
		BREAK
		CASE WV_TRUCK_MENU
			SWITCH iItem
				CASE 0
					RETURN IS_BIT_SET(iBitValue,4) 
				CASE 1
					RETURN IS_BIT_SET(iBitValue,5) 
			ENDSWITCH	
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Build boat sub menu
PROC BUILD_BOAT_SUB_MENU(WAREHOUSE_UPGRADE_CABINET& sVehUpgrade)

	CLEAR_MENU_DATA()
	SET_MENU_TITLE(GET_VEH_UPGRADE_MENU_ITEM_NAMES(sVehUpgrade.eCurrentMenu,TRUE))
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
	SET_MENU_HEADER_COLOUR(47,88,109,255,TRUE)
	SET_MENU_USES_HEADER_GRAPHIC(TRUE, "ShopUI_Title_Exec_VechUpgrade", "ShopUI_Title_Exec_VechUpgrade")
	INT iBitValue = GET_DELIVERY_VEH_UPGRADE()
	
	IF IS_BIT_SET(iBitValue,0)
	OR g_sMPTunables.bexec_upgrades_disable_boat_armor  
		sVehUpgrade.bIsMenuItemAvailable = FALSE
	ELSE
		sVehUpgrade.bIsMenuItemAvailable = TRUE
	ENDIF
	IF IS_WORKBENCH_UPGRADE_DISCOUNT_ACTIVE(sVehUpgrade,0)
		ADD_MENU_ITEM_TEXT(0, "WAR_BOAT_UP_1", 1, sVehUpgrade.bIsMenuItemAvailable)
		ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
	ELSE
		ADD_MENU_ITEM_TEXT(0, "WAR_BOAT_UP_1",0,sVehUpgrade.bIsMenuItemAvailable)
	ENDIF
	IF IS_BIT_SET(iBitValue,0)		// Boat armor upgrade - already own this item
		ADD_MENU_ITEM_TEXT(0, "", 1,sVehUpgrade.bIsMenuItemAvailable)
		ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_CAR)	
	ELSE
		ADD_MENU_ITEM_TEXT(0, "ITEM_COST", 1,sVehUpgrade.bIsMenuItemAvailable)
		ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_VEHICLE_UPGRADE_COST(sVehUpgrade.eCurrentMenu,0))	
	ENDIF	
	
	IF IS_BIT_SET(iBitValue,1)
	OR g_sMPTunables.bexec_upgrades_disable_boat_speed
		sVehUpgrade.bIsMenuItemAvailable = FALSE
	ELSE
		sVehUpgrade.bIsMenuItemAvailable = TRUE
	ENDIF
	IF IS_WORKBENCH_UPGRADE_DISCOUNT_ACTIVE(sVehUpgrade,1)
		ADD_MENU_ITEM_TEXT(1, "WAR_BOAT_UP_2", 1, sVehUpgrade.bIsMenuItemAvailable)
		ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
	ELSE
		ADD_MENU_ITEM_TEXT(1, "WAR_BOAT_UP_2",0,sVehUpgrade.bIsMenuItemAvailable)
	ENDIF	
	IF IS_BIT_SET(iBitValue,1)	// Boat speed upgrade
		ADD_MENU_ITEM_TEXT(1, "", 1,sVehUpgrade.bIsMenuItemAvailable)
		ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_CAR)
	ELSE	
		ADD_MENU_ITEM_TEXT(1, "ITEM_COST", 1,sVehUpgrade.bIsMenuItemAvailable)
		ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_VEHICLE_UPGRADE_COST(sVehUpgrade.eCurrentMenu,1))	
	ENDIF
	
	SET_CURRENT_MENU_ITEM(sVehUpgrade.iMenuCurrentItem)
	sVehUpgrade.bReBuildMenu = TRUE
	sVehUpgrade.eCurrentMenu = WV_BOAT_MENU
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
	
ENDPROC

/// PURPOSE:
///    Build plane submenu
PROC BUILD_PLANE_SUB_MENU(WAREHOUSE_UPGRADE_CABINET& sVehUpgrade)

	CLEAR_MENU_DATA()
	SET_MENU_TITLE(GET_VEH_UPGRADE_MENU_ITEM_NAMES(sVehUpgrade.eCurrentMenu,TRUE))
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
	SET_MENU_HEADER_COLOUR(47,88,109,255,TRUE)
	SET_MENU_USES_HEADER_GRAPHIC(TRUE, "ShopUI_Title_Exec_VechUpgrade", "ShopUI_Title_Exec_VechUpgrade")
	INT iBitValue = GET_DELIVERY_VEH_UPGRADE()
	
	IF IS_BIT_SET(iBitValue,2)
	OR g_sMPTunables.bexec_upgrades_disable_plane_armor 
		sVehUpgrade.bIsMenuItemAvailable = FALSE
	ELSE
		sVehUpgrade.bIsMenuItemAvailable = TRUE
	ENDIF
	IF IS_WORKBENCH_UPGRADE_DISCOUNT_ACTIVE(sVehUpgrade,0)
		ADD_MENU_ITEM_TEXT(0, "WAR_BOAT_UP_1", 1, sVehUpgrade.bIsMenuItemAvailable)
		ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
	ELSE
		ADD_MENU_ITEM_TEXT(0, "WAR_BOAT_UP_1",0,sVehUpgrade.bIsMenuItemAvailable)
	ENDIF
	IF IS_BIT_SET(iBitValue,2)	// plane armor upgrade
		ADD_MENU_ITEM_TEXT(0, "", 1,sVehUpgrade.bIsMenuItemAvailable)
		ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_CAR)
	ELSE	
		ADD_MENU_ITEM_TEXT(0, "ITEM_COST", 1,sVehUpgrade.bIsMenuItemAvailable)
		ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_VEHICLE_UPGRADE_COST(sVehUpgrade.eCurrentMenu,0))	
	ENDIF
	
	IF IS_BIT_SET(iBitValue,3)
	OR g_sMPTunables.bexec_upgrades_disable_plane_jammer    
		sVehUpgrade.bIsMenuItemAvailable = FALSE
	ELSE
		sVehUpgrade.bIsMenuItemAvailable = TRUE
	ENDIF
	IF IS_WORKBENCH_UPGRADE_DISCOUNT_ACTIVE(sVehUpgrade,1)
		ADD_MENU_ITEM_TEXT(1, "WAR_PLANE_UP_2", 1, sVehUpgrade.bIsMenuItemAvailable)
		ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
	ELSE
		ADD_MENU_ITEM_TEXT(1, "WAR_PLANE_UP_2",0,sVehUpgrade.bIsMenuItemAvailable)
	ENDIF	
	IF IS_BIT_SET(iBitValue,3)	// Jammer upgrade
		ADD_MENU_ITEM_TEXT(1, "", 1,sVehUpgrade.bIsMenuItemAvailable)
		ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_CAR)
	ELSE	
		ADD_MENU_ITEM_TEXT(1, "ITEM_COST", 1,sVehUpgrade.bIsMenuItemAvailable)
		ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_VEHICLE_UPGRADE_COST(sVehUpgrade.eCurrentMenu,1))	
	ENDIF
	
	SET_CURRENT_MENU_ITEM(sVehUpgrade.iMenuCurrentItem)
	sVehUpgrade.bReBuildMenu = TRUE
	sVehUpgrade.eCurrentMenu = WV_PLANE_MENU
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
	
ENDPROC

/// PURPOSE:
///    Build truck submenu
PROC BUILD_TRUCK_SUB_MENU(WAREHOUSE_UPGRADE_CABINET& sVehUpgrade)

	CLEAR_MENU_DATA()
	SET_MENU_TITLE(GET_VEH_UPGRADE_MENU_ITEM_NAMES(sVehUpgrade.eCurrentMenu,TRUE))
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
	SET_MENU_HEADER_COLOUR(47,88,109,255,TRUE)
	SET_MENU_USES_HEADER_GRAPHIC(TRUE, "ShopUI_Title_Exec_VechUpgrade", "ShopUI_Title_Exec_VechUpgrade")
	INT iBitValue = GET_DELIVERY_VEH_UPGRADE()	
	 
	IF IS_BIT_SET(iBitValue,4)
	OR g_sMPTunables.bexec_upgrades_disable_truck_armor
		sVehUpgrade.bIsMenuItemAvailable = FALSE
	ELSE
		sVehUpgrade.bIsMenuItemAvailable = TRUE
	ENDIF
	IF IS_WORKBENCH_UPGRADE_DISCOUNT_ACTIVE(sVehUpgrade,0)
		ADD_MENU_ITEM_TEXT(0, "WAR_BOAT_UP_1", 1, sVehUpgrade.bIsMenuItemAvailable)
		ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
	ELSE
		ADD_MENU_ITEM_TEXT(0, "WAR_BOAT_UP_1",0,sVehUpgrade.bIsMenuItemAvailable)
	ENDIF
	IF IS_BIT_SET(iBitValue,4)	// truck armor upgrade
		ADD_MENU_ITEM_TEXT(0, "", 1,sVehUpgrade.bIsMenuItemAvailable)
		ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_CAR)
	ELSE	
		ADD_MENU_ITEM_TEXT(0, "ITEM_COST", 1,sVehUpgrade.bIsMenuItemAvailable)
		ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_VEHICLE_UPGRADE_COST(sVehUpgrade.eCurrentMenu,0))	
	ENDIF
	
	IF IS_BIT_SET(iBitValue,5)
	OR g_sMPTunables.bexec_upgrades_disable_truck_tires  
		sVehUpgrade.bIsMenuItemAvailable = FALSE
	ELSE
		sVehUpgrade.bIsMenuItemAvailable = TRUE
	ENDIF
	IF IS_WORKBENCH_UPGRADE_DISCOUNT_ACTIVE(sVehUpgrade,1)
		ADD_MENU_ITEM_TEXT(1, "WAR_TRUCK_UP_2", 1, sVehUpgrade.bIsMenuItemAvailable)
		ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
	ELSE
		ADD_MENU_ITEM_TEXT(1, "WAR_TRUCK_UP_2",0,sVehUpgrade.bIsMenuItemAvailable)
	ENDIF
	IF IS_BIT_SET(iBitValue,5)	// Bulletproof upgrade
		ADD_MENU_ITEM_TEXT(1, "", 1,sVehUpgrade.bIsMenuItemAvailable)
		ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_CAR)
	ELSE	
		ADD_MENU_ITEM_TEXT(1, "ITEM_COST", 1,sVehUpgrade.bIsMenuItemAvailable)
		ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_VEHICLE_UPGRADE_COST(sVehUpgrade.eCurrentMenu,1))	
	ENDIF
	
	SET_CURRENT_MENU_ITEM(sVehUpgrade.iMenuCurrentItem)
	sVehUpgrade.bReBuildMenu = TRUE
	sVehUpgrade.eCurrentMenu = WV_TRUCK_MENU
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
	
ENDPROC

PROC BUILD_VEH_UPGRADE_MENU(WAREHOUSE_UPGRADE_CABINET& sVehUpgrade)
	
	IF NOT sVehUpgrade.bMenuInitialised
		SWITCH sVehUpgrade.eCurrentMenu
			CASE WV_MAIN_MENU
				BUILD_DELIVERY_VEH_UPGRADE_MAIN_MENU(sVehUpgrade)
			BREAK
			CASE WV_BOAT_MENU
				BUILD_BOAT_SUB_MENU(sVehUpgrade)
			BREAK
			CASE WV_TRUCK_MENU
				BUILD_TRUCK_SUB_MENU(sVehUpgrade)
			BREAK
			
			CASE WV_PLANE_MENU
				BUILD_PLANE_SUB_MENU(sVehUpgrade)
			BREAK
				
		ENDSWITCH
		sVehUpgrade.bMenuInitialised = TRUE
	ENDIF
	
ENDPROC

FUNC BOOL RUN_TOOLBOX_IDLE_ANIM(WAREHOUSE_UPGRADE_CABINET& sVehUpgrade)
	
	IF NOT IS_BIT_SET(sVehUpgrade.iToolBoxBS,TOOLBOX_IDLE_ANIM_RUNNING)
		
		sVehUpgrade.iSyncedSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(sVehUpgrade.sToolBoxScene.vLoc, sVehUpgrade.sToolBoxScene.vRot,DEFAULT, TRUE)
		
		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(),sVehUpgrade.iSyncedSceneID, sVehUpgrade.sToolBoxDic, "idle", WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS , RBF_PLAYER_IMPACT, WALK_BLEND_IN)
		
		NETWORK_START_SYNCHRONISED_SCENE(sVehUpgrade.iSyncedSceneID)
		
		SET_BIT(sVehUpgrade.iToolBoxBS,TOOLBOX_IDLE_ANIM_RUNNING)
		RETURN TRUE
	ELSE	
		RETURN TRUE
	ENDIF
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_TOOLBOX_IDLE_ANIM(WAREHOUSE_UPGRADE_CABINET& sVehUpgrade)
	INT iLocalSceneID
	iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sVehUpgrade.iSyncedSceneID)

	IF iLocalSceneID != -1
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
			IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= 0.99
				CLEAR_BIT(sVehUpgrade.iToolBoxBS,TOOLBOX_IDLE_ANIM_RUNNING)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Build delivery vehicle upgrade menu
/// PARAMS:
///    sVehUpgrade - 
PROC PROCESS_VEH_UPGRADE_MENU(WAREHOUSE_UPGRADE_CABINET& sVehUpgrade)

	RUN_TOOLBOX_IDLE_ANIM(sVehUpgrade)
	PROCESS_TOOLBOX_IDLE_ANIM(sVehUpgrade)
	
	SETUP_WAREHOUSE_MENU_CONTROLS(sVehUpgrade.sWarehouseMenuInput,sVehUpgrade.iMenuCurrentItem)
	
	IF LOAD_MENU_ASSETS()
	
		IF sVehUpgrade.bReBuildMenu
			BUILD_VEH_UPGRADE_MENU(sVehUpgrade)
			sVehUpgrade.bReBuildMenu = FALSE
		ELSE
			SET_CURRENT_MENU_ITEM(sVehUpgrade.iMenuCurrentItem)
		ENDIF
		
		UPDATE_VEH_UPGRADE_MENU(sVehUpgrade)
		
		DRAW_MENU()
		
		IF sVehUpgrade.eWCabinetState != WCABINET_STAGE_INTERACTING
			CLEANUP_MENU_ASSETS()
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL RUN_TOOLBOX_ENTER_ANIM(WAREHOUSE_UPGRADE_CABINET& sVehUpgrade)
	
	REQUEST_ANIM_DICT(sVehUpgrade.sToolBoxDic)
	IF NOT HAS_ANIM_DICT_LOADED(sVehUpgrade.sToolBoxDic)
		RETURN FALSE
	ENDIF	
		
	IF NOT IS_BIT_SET(sVehUpgrade.iToolBoxBS,TOOLBOX_ENTER_ANIM_RUNNING)
		
		sVehUpgrade.iSyncedSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(sVehUpgrade.sToolBoxScene.vLoc, sVehUpgrade.sToolBoxScene.vRot, DEFAULT,TRUE)
		
		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sVehUpgrade.iSyncedSceneID, sVehUpgrade.sToolBoxDic, "enter", WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT, WALK_BLEND_IN)
		
		NETWORK_START_SYNCHRONISED_SCENE(sVehUpgrade.iSyncedSceneID)
		
		SET_BIT(sVehUpgrade.iToolBoxBS,TOOLBOX_ENTER_ANIM_RUNNING)
		RETURN TRUE
	ELSE	
		RETURN TRUE
	ENDIF
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_TOOLBOX_ENTER_ANIM(WAREHOUSE_UPGRADE_CABINET& sVehUpgrade)
	INT iLocalSceneID
	iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sVehUpgrade.iSyncedSceneID)

	IF iLocalSceneID != -1
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
			IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= 0.99
				sVehUpgrade.eWCabinetState = WCABINET_STAGE_INTERACTING
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL RUN_TOOLBOX_EXIT_ANIM(WAREHOUSE_UPGRADE_CABINET& sVehUpgrade)
	
	IF NOT IS_BIT_SET(sVehUpgrade.iToolBoxBS,TOOLBOX_EXIT_ANIM_RUNNING)
		
		sVehUpgrade.iSyncedSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(sVehUpgrade.sToolBoxScene.vLoc, sVehUpgrade.sToolBoxScene.vRot, DEFAULT,TRUE)
		
		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sVehUpgrade.iSyncedSceneID, sVehUpgrade.sToolBoxDic, "exit", WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT, WALK_BLEND_IN)
		
		NETWORK_START_SYNCHRONISED_SCENE(sVehUpgrade.iSyncedSceneID)
		
		SET_BIT(sVehUpgrade.iToolBoxBS,TOOLBOX_EXIT_ANIM_RUNNING)
		RETURN TRUE
	ELSE	
		RETURN TRUE
	ENDIF
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_TOOLBOX_EXIT_ANIM(WAREHOUSE_UPGRADE_CABINET& sVehUpgrade)
	INT iLocalSceneID
	iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sVehUpgrade.iSyncedSceneID)

	IF iLocalSceneID != -1
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
			IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= 0.99
				NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				sVehUpgrade.eWCabinetState = WCABINET_STAGE_CLEANUP
			ENDIF
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL IS_PLAYER_FACING_TOOLBOX(WAREHOUSE_UPGRADE_CABINET& sVehUpgrade)
	GET_WAREHOUSE_VEH_UPGRADE_CABINET_COORDS(GET_WAREHOUSE_SIZE(thisWarehouse.iID),sVehUpgrade.sCabinetPos)	
	VECTOR vToolBox = sVehUpgrade.sCabinetPos.vLoc
	VECTOR vToToolBox = NORMALISE_VECTOR((vToolBox - GET_ENTITY_COORDS(PLAYER_PED_ID())))
	FLOAT fDot = DOT_PRODUCT(GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID()), vToToolBox)
	
	RETURN fDot >= 0
ENDFUNC

/// PURPOSE:
///    Maintain personal assistant service's menu 
PROC MAINTAIN_VEH_UPGRADE_MENU(WAREHOUSE_UPGRADE_CABINET& sVehUpgrade)
	
	SWITCH sVehUpgrade.eWCabinetState
		
		CASE WCABINET_STAGE_INIT
			INIT_VEH_UPGRADE_CABINET(sVehUpgrade)
		BREAK 
		CASE WCABINET_STAGE_WAITING_TO_TRIGGER
			REQUEST_ANIM_DICT(sVehUpgrade.sToolBoxDic)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND GET_DISTANCE_BETWEEN_ENTITIES(sVehUpgrade.objectCabinet, PLAYER_PED_ID()) < 2.0
			AND IS_PLAYER_FACING_TOOLBOX(sVehUpgrade)
			AND IS_SAFE_TO_START_VEH_UPGRADE_MENUS()
			AND NOT warehouseIntroCutscene.bPlaying
				IF sVehUpgrade.iVehUpgradeContext = NEW_CONTEXT_INTENTION
					IF NOT IS_HELP_MESSAGE_ON_SCREEN()
						REGISTER_CONTEXT_INTENTION(sVehUpgrade.iVehUpgradeContext, CP_HIGH_PRIORITY, "WARE_VEH_UPG")
					ENDIF
				ENDIF
				IF NOT IS_INTERACTION_MENU_OPEN()
					IF HAS_CONTEXT_BUTTON_TRIGGERED(sVehUpgrade.iVehUpgradeContext)
						LOAD_MENU_ASSETS()
						BUILD_DELIVERY_VEH_UPGRADE_MAIN_MENU(sVehUpgrade)
						GET_WAREHOUSE_VEH_UPGRADE_CABINET_COORDS(GET_WAREHOUSE_SIZE(thisWarehouse.iID),sVehUpgrade.sCabinetPos,TRUE)
						TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(),GET_ANIM_INITIAL_OFFSET_POSITION(sVehUpgrade.sToolBoxDic, "enter",sVehUpgrade.sToolBoxScene.vLoc, sVehUpgrade.sToolBoxScene.vRot),PEDMOVEBLENDRATIO_WALK,5000,sVehUpgrade.sToolBoxScene.vRot.z,0.05)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
						RELEASE_CONTEXT_INTENTION(sVehUpgrade.iVehUpgradeContext)
						sVehUpgrade.eWCabinetState = WCABINET_STAGE_ENTER_ANIM
					ENDIF
				ENDIF	
			ELIF sVehUpgrade.iVehUpgradeContext != NEW_CONTEXT_INTENTION
				RELEASE_CONTEXT_INTENTION(sVehUpgrade.iVehUpgradeContext)
			ENDIF
		BREAK
		CASE WCABINET_STAGE_ENTER_ANIM
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),sVehUpgrade.sToolBoxTriggerLocate.vLoc,sVehUpgrade.sToolBoxTriggerLocate.vRot,1.0)
					IF RUN_TOOLBOX_ENTER_ANIM(sVehUpgrade)
						PROCESS_TOOLBOX_ENTER_ANIM(sVehUpgrade)
					ENDIF	
				ENDIF	
			ENDIF
		BREAK
		CASE WCABINET_STAGE_INTERACTING
			IF NOT warehouseIntroCutscene.bPlaying
				PROCESS_VEH_UPGRADE_MENU(sVehUpgrade)
			ELSE
				CLEANUP_MENU_ASSETS()
				sVehUpgrade.eWCabinetState = WCABINET_STAGE_CLEANUP
			ENDIF
		BREAK
		CASE WCABINET_STAGE_EXIT_ANIM
			IF RUN_TOOLBOX_EXIT_ANIM(sVehUpgrade)
				PROCESS_TOOLBOX_EXIT_ANIM(sVehUpgrade)
			ENDIF
		BREAK
		CASE WCABINET_STAGE_CLEANUP
			CLEANUP_VEH_UPGRADE_MENU(sVehUpgrade)
		BREAK
	ENDSWITCH
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════╡ WAREHOUSE GARAGE DOOR EXIT ANIM  ╞═════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC INITIALISE_WAREHOUSE_GARAGE_EXIT()
	thisWarehouse.garageExitAnim.dictionary = "anim@apt_trans@garage"
	thisWarehouse.garageExitAnim.clip = "gar_open_1_left"
	thisWarehouse.garageExitAnim.startingPhase = 0.0
	thisWarehouse.garageExitAnim.endingPhase = 0.45
	
	thisWarehouse.garageExitAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_EXEC_WH_GARAGE_DOOR_SOUNDS"
	thisWarehouse.garageExitAnim.strOnEnterFadeOutFinishSoundName = "Door_Open"
	
	SWITCH GET_WAREHOUSE_SIZE(thisWarehouse.iID)
		CASE eWarehouseSmall
			thisWarehouse.vGarageExitMin = <<1104.275, -3101.6143, -40.0000>>
			thisWarehouse.vGarageExitMax = <<1105.4827, -3097.0706, -36.9999>>
			
			thisWarehouse.garageExitAnim.syncScenePos = <<1105.095, -3099.446, -40.000>>
			thisWarehouse.garageExitAnim.syncSceneRot = <<0.0, 0.0, 26.64>>
		BREAK
		CASE eWarehouseMedium
			thisWarehouse.vGarageExitMin = <<1071.821, -3105.487, -40.0000>>
			thisWarehouse.vGarageExitMax = <<1073.979, -3099.819, -36.9999>>
			
			thisWarehouse.garageExitAnim.syncScenePos = <<1073.092, -3102.483, -40.000>>
			thisWarehouse.garageExitAnim.syncSceneRot = <<0.0, 0.0, 26.64>>
		BREAK
		CASE eWarehouseLarge
			thisWarehouse.vGarageExitMin = <<1026.509, -3104.374, -40.0000>>
			thisWarehouse.vGarageExitMax = <<1028.926, -3098.688, -36.9999>>
			
			thisWarehouse.garageExitAnim.syncScenePos = <<1027.822, -3101.432, -40.000>>
			thisWarehouse.garageExitAnim.syncSceneRot = <<0.0, 0.0, 26.64>>
		BREAK
	ENDSWITCH
	
	_SET_CAM_COORDS_SAME_AS_GARAGE_EXIT(thisWarehouse.garageExitAnim)
ENDPROC

FUNC BOOL IS_PLAYER_IN_WAREHOUSE_EXIT_LOCATE()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND IS_ENTITY_IN_AREA(PLAYER_PED_ID(), thisWarehouse.vGarageExitMin, thisWarehouse.vGarageExitMax)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC START_EXIT_VIA_GARAGE_DOOR()
	g_SimpleInteriorData.bShouldExitMenuBeVisible = FALSE
	START_SIMPLE_INTERIOR_ENTRY_ANIM(thisWarehouse.garageExitAnim)
	SET_WAREHOUSE_STATE(WAREHOUSE_STATE_LOADING_GARAGE_EXIT)
ENDPROC

PROC MAINTAIN_EXIT_VIA_GARAGE_DOOR()
	MAINTAIN_SIMPLE_INTERIOR_ENTRY_ANIM(thisWarehouse.garageExitAnim, thisWarehouse.eSimpleInteriorID)
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ INTRO CUTSCENE  ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL SHOULD_DO_WAREHOUSE_INTRO_CUTSCENE()
	
	IF IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_SUPPRESS_TUTORIAL)
		CLEAR_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_SUPPRESS_TUTORIAL)
		RETURN FALSE
	ENDIF

	IF IS_BROWSER_OPEN()
		RETURN FALSE
	ENDIF
	
	IF warehouseIntroCutscene.bShownOnce
		RETURN FALSE
	ENDIF

	IF NOT (thisWarehouse.pOwner = PLAYER_ID())
		// Trigger the cutscene if owner is watching it
		IF (
			NETWORK_IS_PLAYER_A_PARTICIPANT(thisWarehouse.pOwner) 
			AND NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(thisWarehouse.pOwner)) > -1
			AND IS_BIT_SET(playerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(thisWarehouse.pOwner))].iBS, BS_PLAYER_BD_AM_OWNER_AND_AM_WATCHING_INTRO_CUTSCENE)
		)
		OR IS_BIT_SET(GlobalPlayerBD[NATIVE_TO_INT(thisWarehouse.pOwner)].SimpleInteriorBD.iBS, BS_GLOBAL_PLAYER_BD_WAREHOUSE_SIMPLE_INTERIOR_BOSS_PLAYING_INTRO_CUTSCENE)
			/*
			IF NOT IS_BIG_MESSAGE_BEING_DRAWN()
				RETURN TRUE
			ENDIF
			*/
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - [INTRO_CUTSCENE] SHOULD_DO_WAREHOUSE_INTRO_CUTSCENE - Yes (we're a goon and boss is playing)!")
			#ENDIF
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ENDIF

	IF NOT HAS_LOCAL_PLAYER_VIEWED_INTERIOR_CUTSCENE(biFmCut_WarehouseCutscene)
	
		/*
		IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
		AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CONTRABAND_BUY
			// Wait till the end of buy mission to show the cutscene
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - [INTRO_CUTSCENE] SHOULD_DO_WAREHOUSE_INTRO_CUTSCENE - No, player is on buy mission")
			#ENDIF
			RETURN FALSE
		ENDIF
		
		IF IS_BIG_MESSAGE_BEING_DRAWN()
			// End of big mission might show big message, wait till it's gone
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - [INTRO_CUTSCENE] SHOULD_DO_WAREHOUSE_INTRO_CUTSCENE - No, IS_BIG_MESSAGE_BEING_DRAWN() = TRUE")
			#ENDIF
			RETURN FALSE
		ENDIF
		*/
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - [INTRO_CUTSCENE] SHOULD_DO_WAREHOUSE_INTRO_CUTSCENE - Yes!")
		#ENDIF
		
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC START_WAREHOUSE_INTRO_CUTSCENE()
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - [INTRO_CUTSCENE] START_WAREHOUSE_INTRO_CUTSCENE - Starting...")
	#ENDIF
	
	warehouseIntroCutscene.bPlaying = TRUE
	
	IF thisWarehouse.pOwner = PLAYER_ID()
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_PLAYER_BD_AM_OWNER_AND_AM_WATCHING_INTRO_CUTSCENE)
		warehouseIntroCutscene.bOwnerVersion = TRUE
	ENDIF
ENDPROC

FUNC STRING GET_HELP_TEXT_FOR_SCENE(WAREHOUSE_INTRO_CUTSCENE_STAGE stage)
	// Boss/owner version
	IF warehouseIntroCutscene.bOwnerVersion
		SWITCH stage
			CASE WAREHOUSE_CUTSCENE_SHELVES_SHOT
				RETURN "WHOUSE_TUT_1"
			BREAK
			CASE WAREHOUSE_CUTSCENE_LAPTOP_SHOT
				RETURN "WHOUSE_TUT_2"
			BREAK
			CASE WAREHOUSE_CUTSCENE_STAFF_SHOT
				RETURN "WHOUSE_TUT_3"
			BREAK
			CASE WAREHOUSE_CUTSCENE_WORKBENCH_SHOT
				RETURN "WHOUSE_TUT_4"
			BREAK
		ENDSWITCH
	// Goon version
	ELSE
		SWITCH stage
			CASE WAREHOUSE_CUTSCENE_SHELVES_SHOT
				RETURN "WHOUSE_TUT_1b"
			BREAK
			CASE WAREHOUSE_CUTSCENE_LAPTOP_SHOT
				RETURN "WHOUSE_TUT_2b"
			BREAK
			CASE WAREHOUSE_CUTSCENE_STAFF_SHOT
				RETURN "WHOUSE_TUT_3b"
			BREAK
			CASE WAREHOUSE_CUTSCENE_WORKBENCH_SHOT
				RETURN "WHOUSE_TUT_4b"
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

PROC GET_CAM_COORDS_FOR_SCENE(VECTOR &vPos, VECTOR &vRot, FLOAT &fFov, WAREHOUSE_INTRO_CUTSCENE_STAGE stage, BOOL bSceneStart)

	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - [INTRO_CUTSCENE] GET_CAM_COORDS_FOR_SCENE - Called for stage ", ENUM_TO_INT(stage), ", is it for start of the scene? ", bSceneStart)
	#ENDIF

	fFov = 50.0

	SWITCH GET_WAREHOUSE_SIZE(thisWarehouse.iID)
		CASE eWarehouseSmall
			SWITCH stage
				CASE WAREHOUSE_CUTSCENE_SHELVES_SHOT
					IF bSceneStart
						vPos = <<-7.1196, 3.2861, 2.8433>>
						vRot = <<-19.6293, -0.0000, 333.4520>>
					ELSE
						vPos = <<-6.3047, 3.0896, 2.7625>>
						vRot = <<-11.6070, -0.0000, 312.8062>>
					ENDIF
				BREAK
				CASE WAREHOUSE_CUTSCENE_LAPTOP_SHOT
					IF bSceneStart
						vPos = <<-3.4564, 1.7634, 1.2512>>
						vRot = <<-6.0992, -0.1015, 60.6463>>
						fFov = 38.6078
					ELSE
						vPos = <<-4.2775, 2.4607, 1.1418>>
						vRot = <<-5.1609, -0.1015, 67.4538>>
						fFov = 38.6078
					ENDIF
				BREAK
				CASE WAREHOUSE_CUTSCENE_STAFF_SHOT
					IF bSceneStart
						vPos = << 2.6156, 3.34399, 1.7716 >>
						vRot = <<-10.5400, -0.0000, -29.9661>>
						fFov = 44.5936
					ELSE
						vPos = << 3.20825, 4.37158, 1.5508 >>
						vRot = <<-10.5400, -0.0000, -29.9661>>
						fFov = 44.5936
					ENDIF
				BREAK
				CASE WAREHOUSE_CUTSCENE_WORKBENCH_SHOT
					IF bSceneStart
						vPos = <<3.6038, 5.6936, 2.0449>>
						vRot = <<-20.4804, 0.0625, 213.8164>>
						fFov = 28.395
					ELSE
						vPos = <<4.7391, 5.2048, 1.7035>>
						vRot = <<-21.5119, 0.0625, 198.2077>>
						fFov = 28.395
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE eWarehouseMedium
			SWITCH stage
				CASE WAREHOUSE_CUTSCENE_SHELVES_SHOT
					IF bSceneStart
						vPos = <<-7.6022, 7.3909, 2.1957>>
						vRot = <<-12.9481, -0.0000, 297.5837>>
					ELSE
						vPos = <<-7.4956, 7.4302, 2.7878>>
						vRot = <<-7.5138, -0.0000, 262.3093>>
					ENDIF
				BREAK
				CASE WAREHOUSE_CUTSCENE_LAPTOP_SHOT
					IF bSceneStart
						vPos = <<-5.0990, 3.7043, 1.1646>>
						vRot = <<-7.5678, 0.0046, 62.1785>>
						fFov = 39.8779
					ELSE
						vPos = <<-5.5675, 4.4756, 1.0903>>
						vRot = <<-5.4211, 0.0046, 76.1272>>
						fFov = 39.8779
					ENDIF
				BREAK
				CASE WAREHOUSE_CUTSCENE_STAFF_SHOT
					IF bSceneStart
						vPos = << 11.2979, 4.73657, 1.5821 >>
						vRot = <<-6.0242, -0.0000, -47.2027>>
						fFov = 44.8713
					ELSE
						vPos = << 11.8949, 5.29028, 1.4963 >>
						vRot = <<-6.0242, -0.0000, -47.2027>>
						fFov = 44.8713
					ENDIF
				BREAK
				CASE WAREHOUSE_CUTSCENE_WORKBENCH_SHOT
					IF bSceneStart
						vPos = <<-4.6832, 8.6699, 1.7686>>
						vRot = <<-18.8337, -0.0000, 26.1666>>
						fFov = 28.3081
					ELSE
						vPos = <<-5.5229, 8.9958, 1.5728>>
						vRot = <<-19.4507, -0.0000, 13.3843>>
						fFov = 28.3081
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE eWarehouseLarge
			SWITCH stage
				CASE WAREHOUSE_CUTSCENE_SHELVES_SHOT
					IF bSceneStart
						vPos = <<-12.6272, 1.9094, 4.7287>>
						vRot = <<-7.6941, -0.0000, 296.7407>>
						fFov = 40.9609
					ELSE
						vPos = <<-12.5826, 0.1160, 4.7625>>
						vRot = <<-7.6941, -0.0000, 254.3179>>
						fFov = 40.9609
					ENDIF
				BREAK
				CASE WAREHOUSE_CUTSCENE_LAPTOP_SHOT
					IF bSceneStart
						vPos = <<-13.6392, 2.7698, 1.5693>>
						vRot = <<-19.6345, 0.0000, 246.2228>>
					ELSE
						vPos = <<-13.7108, 1.7075, 1.5693>>
						vRot = <<-19.5723, -0.0000, 276.8700>>
					ENDIF
				BREAK
				CASE WAREHOUSE_CUTSCENE_STAFF_SHOT
					IF bSceneStart
						vPos = << 13.4382, 0.989746, 2.5511 >>
						vRot = <<-15.4300, -0.0000, -56.3520>>
						fFov = 46.8295
					ELSE
						vPos = << 14.4602, 1.66992, 2.2122 >>
						vRot = <<-17.0311, -0.0000, -56.3520>>
						fFov = 46.8295
					ENDIF
				BREAK
				CASE WAREHOUSE_CUTSCENE_WORKBENCH_SHOT
					IF bSceneStart
						vPos = <<-6.9604, 5.4856, 2.0835>>
						vRot = <<-19.2545, -0.0000, 121.8405>>
					ELSE
						vPos = <<-7.6966, 3.0127, 1.6129>>
						vRot = <<-20.9611, -0.0000, 89.4295>>
					ENDIF
				BREAK
			ENDSWITCH
		BREAK 
	ENDSWITCH
	
	vPos = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(vPos, thisWarehouse.interiorDetails)
	vRot.Z = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING(vRot.Z, thisWarehouse.interiorDetails)
ENDPROC

FUNC INT GET_WAREHOUSE_CUTSCENE_SCENE_DURATION(WAREHOUSE_INTRO_CUTSCENE_STAGE stage)
	SWITCH stage
		CASE WAREHOUSE_CUTSCENE_SHELVES_SHOT
			RETURN 8600
		BREAK
		CASE WAREHOUSE_CUTSCENE_LAPTOP_SHOT
			RETURN 7100
		BREAK
		CASE WAREHOUSE_CUTSCENE_STAFF_SHOT
			RETURN 6800
		BREAK
		CASE WAREHOUSE_CUTSCENE_WORKBENCH_SHOT
			RETURN 8800
		BREAK
	ENDSWITCH
	
	RETURN 7100
ENDFUNC

PROC RESET_WAREHOUSE_CUTSCENE_DATA()
	warehouseIntroCutscene.bPlaying = FALSE
	warehouseIntroCutscene.bShownOnce = TRUE
	warehouseIntroCutscene.stage = WAREHOUSE_CUTSCENE_LOADING
	warehouseIntroCutscene.iBSSceneInitialised = 0
	warehouseIntroCutscene.iBSSceneHelpShown = 0
	warehouseIntroCutscene.bInputGaitSimulated = FALSE
	RESET_NET_TIMER(warehouseIntroCutscene.timerScene)
	RESET_NET_TIMER(warehouseIntroCutscene.timerHelpTextDelay)
ENDPROC

PROC MAINTAIN_WAREHOUSE_INTRO_CUTSCENE()
	IF NOT warehouseIntroCutscene.bPlaying
		EXIT
	ENDIF
	
	IF warehouseIntroCutscene.stage = WAREHOUSE_CUTSCENE_LOADING
		SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, FALSE)
		START_MP_CUTSCENE(TRUE)
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		DO_SCREEN_FADE_IN(1500)
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] AM_MP_WAREHOUSE - [INTRO_CUTSCENE] MAINTAIN_WAREHOUSE_INTRO_CUTSCENE - START_MP_CUTSCENE(TRUE)")
		#ENDIF
		
		warehouseIntroCutscene.stage = WAREHOUSE_CUTSCENE_SHELVES_SHOT
		//warehouseIntroCutscene.stage = WAREHOUSE_CUTSCENE_WORKBENCH_SHOT
		
		SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_GLOBAL_PLAYER_BD_WAREHOUSE_SIMPLE_INTERIOR_WATCHING_TUTORIAL)
	ENDIF
	
	IF NOT IS_BIT_SET(warehouseIntroCutscene.iBSSceneHelpShown, ENUM_TO_INT(warehouseIntroCutscene.stage))
	
		IF IS_SCREEN_FADING_IN()
			REINIT_NET_TIMER(warehouseIntroCutscene.timerHelpTextDelay)
		ENDIF
	
		STRING strHelpText = GET_HELP_TEXT_FOR_SCENE(warehouseIntroCutscene.stage)
		IF NOT IS_STRING_NULL_OR_EMPTY(strHelpText)
		AND NOT IS_SCREEN_FADING_IN()
		AND (NOT HAS_NET_TIMER_STARTED(warehouseIntroCutscene.timerHelpTextDelay) OR HAS_NET_TIMER_EXPIRED(warehouseIntroCutscene.timerHelpTextDelay, 300))
			CLEAR_HELP(TRUE)
			PRINT_HELP(strHelpText, GET_WAREHOUSE_CUTSCENE_SCENE_DURATION(warehouseIntroCutscene.stage))
			SET_BIT(warehouseIntroCutscene.iBSSceneHelpShown, ENUM_TO_INT(warehouseIntroCutscene.stage))
			RESET_NET_TIMER(warehouseIntroCutscene.timerHelpTextDelay)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(warehouseIntroCutscene.iBSSceneInitialised, ENUM_TO_INT(warehouseIntroCutscene.stage))
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] AM_MP_WAREHOUSE - [INTRO_CUTSCENE] MAINTAIN_WAREHOUSE_INTRO_CUTSCENE - Current scene (", ENUM_TO_INT(warehouseIntroCutscene.stage) ,") not yet initialised, initialising.")
		#ENDIF
		
		REINIT_NET_TIMER(warehouseIntroCutscene.timerScene)
		
		IF NOT DOES_CAM_EXIST(warehouseIntroCutscene.camera)
			warehouseIntroCutscene.camera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] AM_MP_WAREHOUSE - [INTRO_CUTSCENE] MAINTAIN_WAREHOUSE_INTRO_CUTSCENE - Created cam")
			#ENDIF
		ENDIF
		
		VECTOR vCamPos, vCamRot
		FLOAT fFov
		
		GET_CAM_COORDS_FOR_SCENE(vCamPos, vCamRot, fFov, warehouseIntroCutscene.stage, TRUE)
		SET_CAM_PARAMS(warehouseIntroCutscene.camera, vCamPos, vCamRot, fFov)
		GET_CAM_COORDS_FOR_SCENE(vCamPos, vCamRot, fFov, warehouseIntroCutscene.stage, FALSE)
		SET_CAM_PARAMS(warehouseIntroCutscene.camera, vCamPos, vCamRot, fFov, GET_WAREHOUSE_CUTSCENE_SCENE_DURATION(warehouseIntroCutscene.stage), GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
		STOP_CAM_SHAKING(warehouseIntroCutscene.camera, TRUE)
		SHAKE_CAM(warehouseIntroCutscene.camera,"Hand_shake", 0.05)
	
		SET_BIT(warehouseIntroCutscene.iBSSceneInitialised, ENUM_TO_INT(warehouseIntroCutscene.stage))
	ENDIF
	
	// If we are on last scene and are doing the cutscene on entrance to the warehouse then
	// simulate input gait here at the versy last second of cutscene so we maintain that look
	// that player just walked in after the cutscene is done
	IF NOT warehouseIntroCutscene.bInputGaitSimulated AND NOT IS_SIMPLE_INTERIOR_CHILD_SCRIPT_READY()
	AND NOT (SHOULD_LOCAL_PLAYER_BE_AUTOWARPED_INSIDE_THIS_SIMPLE_INTERIOR(thisWarehouse.eSimpleInteriorID) AND IS_SIMPLE_INTERIOR_GLOBAL_BIT_SET(SI_BS_OverrideAutoWarpPosition, thisWarehouse.eSimpleInteriorID))
		IF (ENUM_TO_INT(warehouseIntroCutscene.stage) = COUNT_OF(WAREHOUSE_INTRO_CUTSCENE_STAGE) - 1)
		AND HAS_NET_TIMER_EXPIRED(warehouseIntroCutscene.timerScene, GET_WAREHOUSE_CUTSCENE_SCENE_DURATION(warehouseIntroCutscene.stage) - 400)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 850)
			ENDIF
			
			warehouseIntroCutscene.bInputGaitSimulated = TRUE
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(warehouseIntroCutscene.timerScene, GET_WAREHOUSE_CUTSCENE_SCENE_DURATION(warehouseIntroCutscene.stage))
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] AM_MP_WAREHOUSE - [INTRO_CUTSCENE] MAINTAIN_WAREHOUSE_INTRO_CUTSCENE - Timer for current scene (", ENUM_TO_INT(warehouseIntroCutscene.stage) ,") expired, next scene or quit.")
		#ENDIF
		
		// Move to next scene or finish cutscene if it's the last
		IF ENUM_TO_INT(warehouseIntroCutscene.stage) = COUNT_OF(WAREHOUSE_INTRO_CUTSCENE_STAGE) - 1
			// End of cutscene
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			STOP_CAM_SHAKING(warehouseIntroCutscene.camera, TRUE)
			DESTROY_CAM(warehouseIntroCutscene.camera)
			CLEANUP_MP_CUTSCENE(TRUE, FALSE)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			AND NOT warehouseIntroCutscene.bInputGaitSimulated
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, NSPC_PREVENT_VISIBILITY_CHANGES)
			
			// Reset all the warehouse cutscene data
			RESET_WAREHOUSE_CUTSCENE_DATA()
			
			IF thisWarehouse.pOwner = PLAYER_ID()
				SET_LOCAL_PLAYER_COMPLETED_WAREHOUSE_INTRO_CUTSCENE(TRUE)
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_PLAYER_BD_AM_OWNER_AND_AM_WATCHING_INTRO_CUTSCENE)
				CLEAR_BIT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_GLOBAL_PLAYER_BD_WAREHOUSE_SIMPLE_INTERIOR_BOSS_PLAYING_INTRO_CUTSCENE)
			ENDIF
			
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_GLOBAL_PLAYER_BD_WAREHOUSE_SIMPLE_INTERIOR_WATCHING_TUTORIAL)
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] AM_MP_WAREHOUSE - [INTRO_CUTSCENE] MAINTAIN_WAREHOUSE_INTRO_CUTSCENE - No more scenes to play, finishing.")
			#ENDIF
		ELSE
			warehouseIntroCutscene.stage = INT_TO_ENUM(WAREHOUSE_INTRO_CUTSCENE_STAGE, ENUM_TO_INT(warehouseIntroCutscene.stage) + 1)
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] AM_MP_WAREHOUSE - [INTRO_CUTSCENE] MAINTAIN_WAREHOUSE_INTRO_CUTSCENE - Next scene is: ", ENUM_TO_INT(warehouseIntroCutscene.stage))
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_GOON_WAREHOUSE_HELP()
	IF IS_BIT_SET(thisWarehouse.iBS, BS_WAREHOUSE_GOON_HELP_PROCESSED)
	OR IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_GOON_HELP_SHOWN)
		EXIT
	ENDIF
	
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
		IF NOT DOES_LOCAL_PLAYER_OWN_WAREHOUSE(thisWarehouse.iID)
			SET_BIT(thisWarehouse.iBS, BS_WAREHOUSE_GOON_HELP_PROCESSED)
			EXIT
		ENDIF
	ELSE
		SET_BIT(thisWarehouse.iBS, BS_WAREHOUSE_GOON_HELP_PROCESSED)
		EXIT
	ENDIF

	IF NOT IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_GOON_HELP_SHOWN)
		IF NOT NETWORK_IS_IN_MP_CUTSCENE()
		AND NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
		AND NOT IS_SCREEN_FADING_IN()
		AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
			IF HAS_NET_TIMER_EXPIRED(thisWarehouse.timerGoonHelpDelay, 1200)
				IF DOES_PLAYER_OWN_OFFICE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
					PRINT_HELP("WHOUSE_GOONHLP2C")
				ELSE
					PRINT_HELP("WHOUSE_GOON_HLP2")
				ENDIF
				SET_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_GOON_HELP_SHOWN)
			ENDIF
		ELSE
			RESET_NET_TIMER(thisWarehouse.timerGoonHelpDelay)
		ENDIF
	ENDIF
ENDPROC

#IF FEATURE_DLC_1_2022
PROC MAINTAIN_NEW_WAREHOUSE_STAFF_HELP()
	IF thisWarehouse.pOwner = PLAYER_ID()
		IF IS_BIT_SET(thisWarehouse.iBS, BS_WAREHOUSE_STAFF_HELP_PROCESSED)
			EXIT
		ENDIF
		
		IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DONE_NEW_WH_STAFF_FEATURE_HELP)
		OR NOT HAS_LOCAL_PLAYER_VIEWED_INTERIOR_CUTSCENE(biFmCut_WarehouseCutscene)
			PRINTLN("MAINTAIN_NEW_WAREHOUSE_STAFF_HELP - Skipping - Seen help? ", GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DONE_NEW_WH_STAFF_FEATURE_HELP))
			SET_BIT(thisWarehouse.iBS, BS_WAREHOUSE_STAFF_HELP_PROCESSED)
		ELSE
			IF NOT NETWORK_IS_IN_MP_CUTSCENE()
			AND NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
			AND NOT IS_SCREEN_FADING_IN()
			AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
				PRINT_HELP("WHOUSE_STAFF_HELP", ciWAREHOUSE_PED_HELP_TEXT_AND_BLIP_TIME)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DONE_NEW_WH_STAFF_FEATURE_HELP, TRUE)
				SET_BIT(thisWarehouse.iBS, BS_FLASH_STAFF_BLIP)
				SET_BIT(thisWarehouse.iBS, BS_WAREHOUSE_STAFF_HELP_PROCESSED)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC STRING GET_SOURCE_CARGO_MENU_STATE_STRING(SOURCE_CARGO_MENU_STATE eNewState)
	SWITCH eNewState
		CASE SOURCE_CARGO_MENU_STATE_INIT		RETURN "INIT"
		CASE SOURCE_CARGO_MENU_STATE_PROMPT		RETURN "PROMPT"
		CASE SOURCE_CARGO_MENU_STATE_MENU		RETURN "MENU"
		CASE SOURCE_CARGO_MENU_STATE_END		RETURN "END"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC SET_SOURCE_CARGO_MENU_STATE(SOURCE_CARGO_MENU_STATE eNewState)
	thisWarehouse.sSourceCargoMenuData.eCurrentState = eNewState	
	PRINTLN("AM_MP_WAREHOUSE - SET_SOURCE_CARGO_MENU_STATE: ", GET_SOURCE_CARGO_MENU_STATE_STRING(eNewState))
ENDPROC

PROC INITIALISE_SOURCE_CARGO_MENU_DATA(BOOL bResetMenuSelection = FALSE)
	IF bResetMenuSelection
		thisWarehouse.sSourceCargoMenuData.iCurrentMenuSelection = 0
		thisWarehouse.sSourceCargoMenuData.iCurrentMenuDepth = 0
	ENDIF
	
	thisWarehouse.sSourceCargoMenuData.iBS = 0
	
	RESET_NET_TIMER(thisWarehouse.sSourceCargoMenuData.stAnalogueMenuDelay)
	
	thisWarehouse.sSourceCargoMenuData.eTransactionResult = TRANSACTION_STATE_DEFAULT
	
	SET_BIT(thisWarehouse.sSourceCargoMenuData.iBS, SOURCE_CARGO_MENU_BS_REBUILD_MENU)
ENDPROC

PROC CLEANUP_SOURCE_CARGO_MENU()
	INITIALISE_SOURCE_CARGO_MENU_DATA(TRUE)
	
	CLEANUP_MENU_ASSETS()
	
	IF thisWarehouse.sSourceCargoMenuData.eCurrentState > SOURCE_CARGO_MENU_STATE_PROMPT
		DISPLAY_RADAR(TRUE)
		
		IF NOT IS_SKYSWOOP_MOVING()
		AND NOT IS_SKYSWOOP_IN_SKY()
		AND NOT IS_TRANSITION_ACTIVE()
		AND NOT IS_TRANSITION_RUNNING()
		AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		AND NOT IS_PLAYER_TELEPORT_ACTIVE()
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
		
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, FALSE)
	ENDIF
	
	IF thisWarehouse.sSourceCargoMenuData.iContextIntention != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(thisWarehouse.sSourceCargoMenuData.iContextIntention)
		thisWarehouse.sSourceCargoMenuData.iContextIntention = NEW_CONTEXT_INTENTION
	ENDIF
	SET_SOURCE_CARGO_MENU_STATE(SOURCE_CARGO_MENU_STATE_PROMPT)
ENDPROC
#ENDIF

// CLEANUP PROC
PROC SCRIPT_CLEANUP()
	PRINTLN("AM_MP_WAREHOUSE - SCRIPT_CLEANUP")
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - SCRIPT_CLEANUP")
	#ENDIF
	
	CLEANUP_ALL_WAREHOUSE_CRATES()
	CLEANUP_GLOBAL_WAREHOUSE_CRATE_DATA()
	CLEANUP_SIMPLE_INTERIOR_ENTRY_ANIM(thisWarehouse.garageExitAnim)
	SAFE_REMOVE_BLIP(thisWarehouse.laptopBlip)
	SAFE_REMOVE_BLIP(thisWarehouse.workbenchBlip)
	SAFE_REMOVE_BLIP(thisWarehouse.biWarehousePed)
	
	CLEANUP_VEH_UPGRADE_MENU(thisWarehouse.sWUpgradeCabinet,TRUE)
	
	CLEANUP_MP_RADIO(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPRadioClient, MPRadioLocal)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NETWORK_GET_THIS_SCRIPT_IS_NETWORK_SCRIPT()
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				CLEANUP_SERVER_RADIO(serverBD.MPRadioServer, InteriorPropStruct)
			ENDIF
		ENDIF
	ENDIF
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ DEBUG STUFF  ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD

PROC CREATE_DEBUG_WIDGETS()
	TEXT_LABEL_63 str = "AM_MP_WAREHOUSE"
	
	START_WIDGET_GROUP(str)
		ADD_WIDGET_BOOL("Reset cutscene", debugData.bResetCutscene)
		
		ADD_WIDGET_BOOL("Stats test", debugData.bStatsTest)
		ADD_WIDGET_BOOL("Give Special Items", debugData.bGiveSpecial)
		ADD_WIDGET_BOOL("Print Server BD Crates", debugData.bPrintServerBDCrates)
	STOP_WIDGET_GROUP()
ENDPROC

PROC UPDATE_DEBUG_WIDGETS()
	IF debugData.bResetCutscene
		SET_LOCAL_PLAYER_COMPLETED_WAREHOUSE_INTRO_CUTSCENE(FALSE)
		RESET_WAREHOUSE_CUTSCENE_DATA()
		warehouseIntroCutscene.bShownOnce = FALSE
		debugData.bResetCutscene = FALSE
	ENDIF
	
	IF debugData.bStatsTest
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[thisWarehouse.iWHSaveSlot].iContrabandUnitsTotal = 111
	
		INT i
		FOR i = 0 TO (ciLARGE_WAREHOUSE_CRATE_CAPACITY-1)
			SET_WAREHOUSE_CRATES_STAT_INT(thisWarehouse.iWHSaveSlot, i, 0)
			PRINTLN("AM_MP_WAREHOUSE - UPDATE_DEBUG_WIDGETS - AM_MP_WAREHOUSE - POPULATE_WAREHOUSE_WITH_CRATES - Filling Server BD Array - Index: ", i, " has Crate Model: ", GET_WAREHOUSE_CRATES_STAT_INT(thisWarehouse.iWHSaveSlot, i))
		ENDFOR
		debugData.bStatsTest = FALSE
	ENDIF
	
	IF debugData.bGiveSpecial
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[thisWarehouse.iWHSaveSlot].iContrabandUnitsTotal = 12
		
		SET_MP_INT_CHARACTER_STAT(MP_STAT_SPCONTOTALFORWHOUSE0, 5)
		iSpecialItemWarehouse[0] = thisWarehouse.iID
		
		SET_WAREHOUSE_CRATES_STAT_INT(thisWarehouse.iWHSaveSlot, 0, GET_WAREHOUSE_CRATE_MODEL_INDEX(EX_PROP_CRATE_OEGG))
		iContrabandSpecialItems[0] = ENUM_TO_INT(CONTRABAND_ITEM_ORNAMENTAL_EGG)
		
		SET_WAREHOUSE_CRATES_STAT_INT(thisWarehouse.iWHSaveSlot, 2, GET_WAREHOUSE_CRATE_MODEL_INDEX(EX_PROP_CRATE_MINIG))
		iContrabandSpecialItems[1] = ENUM_TO_INT(CONTRABAND_ITEM_GOLDEN_MINIGUN)
		
		SET_WAREHOUSE_CRATES_STAT_INT(thisWarehouse.iWHSaveSlot, 4, GET_WAREHOUSE_CRATE_MODEL_INDEX(EX_PROP_CRATE_XLDIAM))
		iContrabandSpecialItems[2] = ENUM_TO_INT(CONTRABAND_ITEM_EXTRA_LARGE_DIAMOND)
		
		SET_WAREHOUSE_CRATES_STAT_INT(thisWarehouse.iWHSaveSlot, 6, GET_WAREHOUSE_CRATE_MODEL_INDEX(EX_PROP_CRATE_SHIDE))
		iContrabandSpecialItems[3] = ENUM_TO_INT(CONTRABAND_ITEM_SASQUATCH_HIDE)
		
		SET_WAREHOUSE_CRATES_STAT_INT(thisWarehouse.iWHSaveSlot, 8, GET_WAREHOUSE_CRATE_MODEL_INDEX(EX_PROP_CRATE_FREEL))
		iContrabandSpecialItems[4] = ENUM_TO_INT(CONTRABAND_ITEM_FILM_REEL)
		
		SET_WAREHOUSE_CRATES_STAT_INT(thisWarehouse.iWHSaveSlot, 10, GET_WAREHOUSE_CRATE_MODEL_INDEX(EX_PROP_CRATE_WATCH))
		iContrabandSpecialItems[5] = ENUM_TO_INT(CONTRABAND_ITEM_RARE_POCKET_WATCH)
		
		debugData.bGiveSpecial = FALSE
	ENDIF
	
	IF debugData.bPrintServerBDCrates
		INT i
		FOR i = 0 TO (ciLARGE_WAREHOUSE_CRATE_CAPACITY-1)
			PRINTLN("AM_MP_WAREHOUSE - UPDATE_DEBUG_WIDGETS - ServerBD Crates[", i, "]: ", GET_WAREHOUSE_CRATE_MODEL_INDEX(INT_TO_ENUM(MODEL_NAMES, serverBD.iCrateID[i])))
		ENDFOR
		debugData.bPrintServerBDCrates = FALSE
	ENDIF
ENDPROC
#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ INITIALISATION  ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC INITIALISE()
	
	// Get simple interior details
	GET_SIMPLE_INTERIOR_DETAILS(thisWarehouse.eSimpleInteriorID, thisWarehouse.interiorDetails)
	
	INITALISE_WAREHOUSE_CRATES()
	
	INITIALISE_WAREHOUSE_GARAGE_EXIT()
	
	SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
	
	IF SHOULD_DO_WAREHOUSE_INTRO_CUTSCENE()
		SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(TRUE)
	ENDIF
	
	#IF FEATURE_DLC_1_2022
	INITIALISE_SOURCE_CARGO_MENU_DATA(TRUE)
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		CREATE_DEBUG_WIDGETS()
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - INITIALISE - Done.")
	#ENDIF
	
ENDPROC

PROC SCRIPT_INITIALISE(SIMPLE_INTERIOR_CHILD_SCRIPT_DATA& scriptData)
	
	thisWarehouse.eSimpleInteriorID = scriptData.eSimpleInteriorID
	thisWarehouse.iScriptInstance = scriptData.iScriptInstance
	thisWarehouse.iInvitingPlayer = scriptData.iInvitingPlayer
	thisWarehouse.iID = GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(scriptData.eSimpleInteriorID)
	thisWarehouse.pOwner = GET_WAREHOUSE_OWNER(thisWarehouse.iID)
	thisWarehouse.iWHSaveSlot = GET_SAVE_SLOT_FOR_PLAYERS_WAREHOUSE(thisWarehouse.iID, thisWarehouse.pOwner)
	
	REQUEST_CARGO_SOURCING(FALSE)
	InteriorPropStruct.iIndex = -1 // reseting property struct so the radio doesn't think the player is still in the last apartment they were in.
	
	MPRadioLocal.iWarehouseSize = GET_WAREHOUSE_SIZE_INT(thisWarehouse.iID)
	MPRadioLocal.piApartmentOwner = thisWarehouse.pOwner
	MPRadioLocal.iWarehouseSize = GET_WAREHOUSE_SIZE_INT(thisWarehouse.iID)
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][WAREHOUSE] AM_MP_WAREHOUSE - SCRIPT_INITIALISE - Launching instance ", thisWarehouse.iScriptInstance)
	#ENDIF
	
	IF thisWarehouse.pOwner = INVALID_PLAYER_INDEX()
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][WAREHOUSE] AM_MP_WAREHOUSE - SCRIPT_INITIALISE - Owner of this warehouse is invalid, exiting...")
		#ENDIF
		SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
		SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
		SCRIPT_CLEANUP()
	ENDIF
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, thisWarehouse.iScriptInstance)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("AM_MP_WAREHOUSE - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		PRINTLN("AM_MP_WAREHOUSE - INITIALISED")
	ELSE
		PRINTLN("AM_MP_WAREHOUSE - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	INITIALISE()
	
ENDPROC
CONST_INT NUM_SEAT_IN_WAREHOUSE  2
STRUCT OFFICE_SEAT_DATA 
	INT iSeatStagger
	INT bSeatInputLoopedNumber
	BOOL bSeatInputReceived
	INT bSeatHelpTextBitset[NUM_SEAT_IN_WAREHOUSE]
ENDSTRUCT
OFFICE_SEAT_DATA warehouseSeatData
VECTOR vWarehouseSeatPos[NUM_SEAT_IN_WAREHOUSE]
PROC INIT_WAREHOUSE_SEAT_DATA()
//	INT iSeatID
//	REPEAT NUM_SEAT_IN_WAREHOUSE iSeatID
//		GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurrentProperty, MP_PROP_ELEMENT_WAREHOUSE_SEAT_0 + iSeatID, mpOffsetOfficeSeats[iSeatID], PROPERTY_OFFICE_2_BASE)
//		CDEBUG1LN(DEBUG_SAFEHOUSE, "STAGE_INIT: mpOffsetOfficeSeats[", iSeatID, "] = ", mpOffsetOfficeSeats[iSeatID].vLoc)
//	ENDREPEAT
	vWarehouseSeatPos[0] = <<992.419, -3101.735, -39.489>>
	vWarehouseSeatPos[1] = <<992.419, -3100.978, -39.489>>
	
//	VECTOR vWarehouseSeatBox1A = <<992.878052,-3102.105469,-39.990833>>
//	VECTOR vWarehouseSeatBox1B= <<992.869873,-3101.349365,-38.490833>> 
//
//	VECTOR vWarehouseSeatBox2A = <<992.866150,-3100.604492,-39.990833>>
//	VECTOR vWarehouseSeatBox2B = <<992.869873,-3101.349365,-38.490833>> 
	
ENDPROC

PROC RUN_WAREHOUSE_SEAT_LOGIC(OFFICE_SEAT_DATA &warehouseSeatDataRef)
	
	FLOAT SeatWidth = 0.45
	
	
	
	CDEBUG2LN(DEBUG_SAFEHOUSE, "RUN_WAREHOUSE_SEAT_LOGIC: iSeatStagger = ", warehouseSeatDataRef.iSeatStagger)
	INT iSeat
	IF warehouseSeatDataRef.iSeatStagger = warehouseSeatDataRef.bSeatInputLoopedNumber
		CDEBUG2LN(DEBUG_SAFEHOUSE, "RUN_WAREHOUSE_SEAT_LOGIC: iSeatStagger = bSeatInputLoopedNumber = ", warehouseSeatDataRef.iSeatStagger)
		CDEBUG2LN(DEBUG_SAFEHOUSE, "RUN_WAREHOUSE_SEAT_LOGIC: bSeatInputReceived = FALSE")
		warehouseSeatDataRef.bSeatInputReceived = FALSE
	ENDIF

	IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_CONTEXT)
		warehouseSeatDataRef.bSeatInputReceived = TRUE
		warehouseSeatDataRef.bSeatInputLoopedNumber = warehouseSeatDataRef.iSeatStagger
		CDEBUG2LN(DEBUG_SAFEHOUSE, "RUN_WAREHOUSE_SEAT_LOGIC: warehouseSeatDataRef.iSeatStagger = ", warehouseSeatDataRef.iSeatStagger)
		CDEBUG2LN(DEBUG_SAFEHOUSE, "RUN_WAREHOUSE_SEAT_LOGIC: bSeatInputReceived = TRUE")
	ENDIF
	
	VECTOR CurrentSeatPos
	iSeat = warehouseSeatDataRef.iSeatStagger
		
		
	IF NOT IS_VECTOR_ZERO(vWarehouseSeatPos[iSeat])	
		
		CurrentSeatPos = vWarehouseSeatPos[iSeat]
		CurrentSeatPos.z += 0.5
		CurrentSeatPos.x += 0.5
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT PED_HAS_USE_SCENARIO_TASK(PLAYER_PED_ID())
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), CurrentSeatPos, <<SeatWidth, SeatWidth, SeatWidth>>)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "Entity is at coord for iSeat: ", iSeat)
				IF DOES_SCENARIO_EXIST_IN_AREA(CurrentSeatPos, 2.0, TRUE)
				
					IF NOT IS_SCENARIO_OCCUPIED(CurrentSeatPos, 2.0, TRUE)
						IF IS_BIT_SET(warehouseSeatDataRef.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_STAND)
							CLEAR_BIT(warehouseSeatDataRef.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_STAND)
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPTV_STAND")
								CLEAR_HELP()
							ENDIF
						ENDIF
					
						IF NOT IS_BIT_SET(warehouseSeatDataRef.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_SIT)
							SET_BIT(warehouseSeatDataRef.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_SIT)
							PRINT_HELP("MPTV_WALK")
							CDEBUG1LN(DEBUG_SAFEHOUSE, "Printing walk help for: ", iSeat)
						ENDIF
						
	//								IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_CONTEXT)
						IF warehouseSeatDataRef.bSeatInputReceived	
							SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseGoToPointForScenarioNavigation, TRUE)
							TASK_USE_NEAREST_SCENARIO_TO_COORD(PLAYER_PED_ID(), CurrentSeatPos, 2.0, 5000)
							CDEBUG1LN(DEBUG_SAFEHOUSE, " RUN_WAREHOUSE_SEAT_LOGIC: TASK_USE_NEAREST_SCENARIO_TO_COORD CurrentSeatPos = ", CurrentSeatPos, " ")
							warehouseSeatDataRef.bSeatInputReceived = FALSE
							warehouseSeatDataRef.bSeatInputLoopedNumber = -1
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPTV_WALK")
								CLEAR_HELP()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
	//					CDEBUG2LN(DEBUG_SAFEHOUSE, "Entity is NOT at coord for iSeat: ", iSeat)
				IF IS_BIT_SET(warehouseSeatDataRef.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_SIT)
					CLEAR_BIT(warehouseSeatDataRef.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_SIT)
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPTV_WALK")
						CLEAR_HELP()
					ENDIF
				ENDIF
				IF IS_BIT_SET(warehouseSeatDataRef.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_STAND)
					CLEAR_BIT(warehouseSeatDataRef.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_STAND)
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPTV_STAND")
						CLEAR_HELP()
					ENDIF
				ENDIF
			ENDIF
		ELSE
		
			IF IS_BIT_SET(warehouseSeatDataRef.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_SIT)
				CLEAR_BIT(warehouseSeatDataRef.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_SIT)
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPTV_WALK")
					CLEAR_HELP()
				ENDIF
			ENDIF
			
	//				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
		
			IF IS_SCENARIO_OCCUPIED(CurrentSeatPos, 2.0, TRUE)
				IF NOT IS_BIT_SET(warehouseSeatDataRef.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_STAND)
					SET_BIT(warehouseSeatDataRef.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_STAND)
					PRINT_HELP("MPTV_STAND")
				ENDIF

				IF warehouseSeatDataRef.bSeatInputReceived
					SET_PED_SHOULD_IGNORE_SCENARIO_EXIT_COLLISION_CHECKS(PLAYER_PED_ID(), TRUE)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					CDEBUG1LN(DEBUG_SAFEHOUSE, " RUN_WAREHOUSE_SEAT_LOGIC: CLEAR_PED_TASKS ")
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseGoToPointForScenarioNavigation, FALSE)
					warehouseSeatDataRef.bSeatInputReceived = FALSE
					warehouseSeatDataRef.bSeatInputLoopedNumber = -1
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPTV_STAND")
						CLEAR_HELP()
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF


	ENDIF
	
	warehouseSeatDataRef.iSeatStagger++
	IF (warehouseSeatDataRef.iSeatStagger >= NUM_SEAT_IN_WAREHOUSE)
		warehouseSeatDataRef.iSeatStagger = 0
	ENDIF

ENDPROC

#IF FEATURE_DLC_1_2022
///////////////////////////////////
///    SOURCE_CARGO MENU		///
///////////////////////////////////
CONST_INT SOURCE_CARGO_MENU_SELECTION_VEHICLES			0

FUNC INT GET_NUMBER_OF_SOURCE_CARGO_MENU_OPTIONS()	
	RETURN 1
ENDFUNC

FUNC STRING SOURCE_CARGO_MENU_OPTION(INT iIndex)
	SWITCH iIndex
		CASE 0		RETURN "SRC_CRG_1_1"
	ENDSWITCH
		
	RETURN ""
ENDFUNC

PROC MAINTAIN_SOURCE_CARGO_MENU_DESCRIPTION()
	IF NOT IS_WAREHOUSE_FULL(GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID()))
		IF GET_SAVE_SLOT_FOR_WAREHOUSE(GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID())) != 0
			SET_CURRENT_MENU_ITEM_DESCRIPTION("SRC_CRG_1_D")
		ELSE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("SRC_CRG_1_D_L")
		ENDIF
	ELSE
		SET_CURRENT_MENU_ITEM_DESCRIPTION("SRC_CRG_FULL")
	ENDIF
ENDPROC

//FUNC STRING GET_HEO_MOD_SHOP_MENU_HEADER_GRAPHIC()
//	RETURN "ShopUI_Title_Hao"
//ENDFUNC

PROC BUILD_SOURCE_CARGO_MENU()
	
	IF GET_SAVE_SLOT_FOR_WAREHOUSE(GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID())) != 0
		SETUP_MENU_WITH_TITLE("SRC_CRG_T")
	ELSE
		SETUP_MENU_WITH_TITLE("SRC_CRG_T_L")
	ENDIF
	
//	REQUEST_STREAMED_TEXTURE_DICT(GET_HEO_MOD_SHOP_MENU_HEADER_GRAPHIC())
//	
//	IF HAS_STREAMED_TEXTURE_DICT_LOADED(GET_HEO_MOD_SHOP_MENU_HEADER_GRAPHIC())
//		SET_MENU_USES_HEADER_GRAPHIC(TRUE, GET_HEO_MOD_SHOP_MENU_HEADER_GRAPHIC(), GET_HEO_MOD_SHOP_MENU_HEADER_GRAPHIC())
//	ENDIF
	
	BOOL bAvailable = TRUE			
	INT i
	
	IF NOT NETWORK_CAN_SPEND_MONEY(g_sMPTunables.iWAREHOUSE_CARGO_SOURCING_PRICE, FALSE, TRUE, FALSE)
	OR IS_WAREHOUSE_FULL(GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID()))
		bAvailable = FALSE
	ENDIF
	
	REPEAT GET_NUMBER_OF_SOURCE_CARGO_MENU_OPTIONS() i	
		ADD_MENU_ITEM_TEXT(i, SOURCE_CARGO_MENU_OPTION(i), 0, bAvailable)	
		ADD_MENU_ITEM_TEXT(i, "IMPOUND_COST", 1, bAvailable)
		ADD_MENU_ITEM_TEXT_COMPONENT_INT(g_sMPTunables.iWAREHOUSE_CARGO_SOURCING_PRICE)
	ENDREPEAT		

	SET_CURRENT_MENU_ITEM(thisWarehouse.sSourceCargoMenuData.iCurrentMenuSelection)
	
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
ENDPROC

PROC HANDLE_SOURCE_CARGO_MENU_PC_CONTROL(BOOL &bPrevious, BOOL &bNext, BOOL  &bAccept, BOOL &bBack)
	IF IS_PC_VERSION()
	AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
		IF IS_USING_CURSOR(FRONTEND_CONTROL)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_ACCEPT)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
			SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_CURSOR_ACCEPT)
			SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_UP)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
			
			IF ((g_iMenuCursorItem = MENU_CURSOR_NO_ITEM) OR (g_iMenuCursorItem = MENU_CURSOR_NO_CAMERA_MOVE) OR (g_iMenuCursorItem = MENU_CURSOR_DRAG_CAM))
				ENABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
				ENABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
			ELSE
				DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
			ENDIF
			
			HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
			
			HANDLE_MENU_CURSOR(FALSE)
		ENDIF
		
		IF IS_MENU_CURSOR_ACCEPT_PRESSED()
			IF g_iMenuCursorItem = thisWarehouse.sSourceCargoMenuData.iCurrentMenuSelection
				bAccept = TRUE
			ELSE
				thisWarehouse.sSourceCargoMenuData.iCurrentMenuSelection = g_iMenuCursorItem
				
				SET_CURRENT_MENU_ITEM(thisWarehouse.sSourceCargoMenuData.iCurrentMenuSelection)
			ENDIF
		ENDIF
		
		IF IS_MENU_CURSOR_CANCEL_PRESSED()
			bBack = TRUE
		ENDIF
		
		IF IS_MENU_CURSOR_SCROLL_UP_PRESSED()
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			bPrevious = TRUE
		ENDIF
		
		IF IS_MENU_CURSOR_SCROLL_DOWN_PRESSED()
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			bNext = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC CUSTOM_MENU_CONTROL_DISABLE(BOOL bDealWithHUDAndPhone = TRUE)
	IF bDealWithHUDAndPhone
		SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
		HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
	ENDIF
	
	IF (NOT IS_SYSTEM_UI_BEING_DISPLAYED())
	AND (NOT IS_PAUSE_MENU_ACTIVE())
		DISABLE_FRONTEND_THIS_FRAME()
		DISABLE_ALL_CONTROL_ACTIONS(PLAYER_CONTROL)
		ENABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_FRONTEND_ACCEPT)
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_FRONTEND_CANCEL)
	ENDIF
ENDPROC

PROC HANDLE_SOURCE_CARGO_MENU_INPUT()	
	BOOL bPrevious, bNext
	
	INT iLeftY
	INT iMenuInputDelay = 150
	
	IF IS_USING_CURSOR(FRONTEND_CONTROL)
		iMenuInputDelay = 110
	ENDIF
	
	CUSTOM_MENU_CONTROL_DISABLE()
	
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_MOVE_LR)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_MOVE_UD)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_X)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	
	BOOL bAccept = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
	BOOL bBack = (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
	
	iLeftY = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_MOVE_UD)
	
	IF NOT IS_BIT_SET(thisWarehouse.sSourceCargoMenuData.iBS, SOURCE_CARGO_MENU_BS_DELAY_TIMER_ACTIVE)
		IF IS_USING_CURSOR(FRONTEND_CONTROL)
			HANDLE_SOURCE_CARGO_MENU_PC_CONTROL(bPrevious, bNext, bAccept, bBack)
		ELSE
			bPrevious = ((iLeftY < 100) OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP))
			bNext = ((iLeftY > 150) OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN))
		ENDIF
		
		IF bAccept
			IF NETWORK_CAN_SPEND_MONEY(g_sMPTunables.iWAREHOUSE_CARGO_SOURCING_PRICE, FALSE, TRUE, FALSE)
			AND NOT IS_WAREHOUSE_FULL(GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID()))
				REQUEST_CARGO_SOURCING(TRUE)
				SET_PEDS_BIT(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_SOURCE_CARGO_POSITIVE)
				SET_SOURCE_CARGO_MENU_STATE(SOURCE_CARGO_MENU_STATE_END)
				SET_BIT(thisWarehouse.sSourceCargoMenuData.iBS, SOURCE_CARGO_MENU_BS_RUN_TRANSACTION)
			ENDIF
		ENDIF
		
		IF bPrevious OR bNext
			START_NET_TIMER(thisWarehouse.sSourceCargoMenuData.stAnalogueMenuDelay)
			
			SET_BIT(thisWarehouse.sSourceCargoMenuData.iBS, SOURCE_CARGO_MENU_BS_DELAY_TIMER_ACTIVE)
		ENDIF
	ELIF HAS_NET_TIMER_EXPIRED(thisWarehouse.sSourceCargoMenuData.stAnalogueMenuDelay, iMenuInputDelay)
		RESET_NET_TIMER(thisWarehouse.sSourceCargoMenuData.stAnalogueMenuDelay)
		
		CLEAR_BIT(thisWarehouse.sSourceCargoMenuData.iBS, SOURCE_CARGO_MENU_BS_DELAY_TIMER_ACTIVE)
	ENDIF

	//only one option, no need for sounds
//	IF bNext
//		thisWarehouse.sSourceCargoMenuData.iCurrentMenuSelection++
//		
//		IF thisWarehouse.sSourceCargoMenuData.iCurrentMenuSelection > (GET_NUMBER_OF_SOURCE_CARGO_MENU_OPTIONS() - 1)
//			thisWarehouse.sSourceCargoMenuData.iCurrentMenuSelection = 0
//		ENDIF
//		
//		PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
//	ENDIF
//	
//	IF bPrevious
//		thisWarehouse.sSourceCargoMenuData.iCurrentMenuSelection--
//		
//		IF thisWarehouse.sSourceCargoMenuData.iCurrentMenuSelection < 0
//			thisWarehouse.sSourceCargoMenuData.iCurrentMenuSelection = (GET_NUMBER_OF_SOURCE_CARGO_MENU_OPTIONS() - 1)
//		ENDIF
//		
//		PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
//	ENDIF
	
	IF bAccept
		BOOL bPlayErrorSound = FALSE
		
		IF NOT NETWORK_CAN_SPEND_MONEY(g_sMPTunables.iWAREHOUSE_CARGO_SOURCING_PRICE, FALSE, TRUE, FALSE)
		OR IS_WAREHOUSE_FULL(GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID()))
			bPlayErrorSound = TRUE
			SET_PEDS_BIT(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_SOURCE_CARGO_NEGATIVE)
		ENDIF
		
		IF bPlayErrorSound
			PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		ELSE
			PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		ENDIF
		
		
		
	ENDIF
	
	IF bBack
	OR SHOULD_LAUNCH_CRIMINAL_STARTER_PACK_BROWSER()
	OR IS_BROWSER_OPEN()	
		SET_SOURCE_CARGO_MENU_STATE(SOURCE_CARGO_MENU_STATE_END)
	ENDIF
ENDPROC

PROC DEAL_WITH_SOURCE_CARGO_MENU_HUD_ELEMENTS()
	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
ENDPROC

PROC RUN_SOURCE_CARGO_MENU()
	IF NOT IS_PAUSE_MENU_ACTIVE_EX()
		//REQUEST_STREAMED_TEXTURE_DICT(GET_HEO_MOD_SHOP_MENU_HEADER_GRAPHIC())
		HANDLE_SOURCE_CARGO_MENU_INPUT()
		
		IF LOAD_MENU_ASSETS("SNK_MNU", DEFAULT, TRUE)
			//IF HAS_STREAMED_TEXTURE_DICT_LOADED(GET_HEO_MOD_SHOP_MENU_HEADER_GRAPHIC())
				IF IS_BIT_SET(thisWarehouse.sSourceCargoMenuData.iBS, SOURCE_CARGO_MENU_BS_REBUILD_MENU)
					BUILD_SOURCE_CARGO_MENU()
					
					CLEAR_BIT(thisWarehouse.sSourceCargoMenuData.iBS, SOURCE_CARGO_MENU_BS_REBUILD_MENU)
				ELSE
					SET_CURRENT_MENU_ITEM(thisWarehouse.sSourceCargoMenuData.iCurrentMenuSelection)
				ENDIF
				
				MAINTAIN_SOURCE_CARGO_MENU_DESCRIPTION()
				
				DRAW_MENU()
				
				DEAL_WITH_SOURCE_CARGO_MENU_HUD_ELEMENTS()
			//ENDIF
		ENDIF
	ENDIF
ENDPROC 

FUNC VECTOR GET_WAREHOUSE_PED_POSITION()
	SWITCH g_iPedLayout
		//SMALL WAREHOUSE
		CASE 0		RETURN <<1099.822, -3097.927, -39.02092>>		BREAK
		CASE 1		RETURN <<1103.173, -3101.958, -39.02092>>		BREAK
		CASE 2		RETURN <<1093.597, -3098.030, -39.00166>>		BREAK
		//MEDIUM WAREHOUSE
		CASE 3		RETURN <<1071.233, -3097.829, -39.00166>>		BREAK
		CASE 4		RETURN <<1071.512, -3107.35,  -39.00166>>		BREAK
		CASE 5		RETURN <<1060.139, -3098.929, -38.00166>>		BREAK
		//LARGE WAREHOUSE
		CASE 6		RETURN <<1025.093, -3098.251, -38.00166>>		BREAK
		CASE 7		RETURN <<1001.805, -3092.803, -38.00166>>		BREAK
		CASE 8		RETURN <<997.8175, -3109.485, -38.00166>>		BREAK
	ENDSWITCH
	
	RETURN <<1093.25, -3102.813, -38.00166>>
ENDFUNC

FUNC BOOL CAN_INTERACT_WITH_WAREHOUSE_PED()
	SWITCH g_iPedLayout
		//SMALL WAREHOUSE
		CASE 0		RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1098.822, -3097.927, -38.00166>>, <<1100.822, -3097.927, -40.00166>>, 2.0) AND IS_ENTITY_FACING_COORD_WITHIN_RANGE(PLAYER_PED_ID(), <<1099.822, -3097.927, -38.00166>>, 80)			BREAK
		CASE 1		RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1102.173, -3101.958, -38.00166>>, <<1104.173, -3101.958, -40.00166>>, 2.0) AND IS_ENTITY_FACING_COORD_WITHIN_RANGE(PLAYER_PED_ID(), <<1103.173, -3101.958, -38.00166>>, 80)			BREAK
		CASE 2		RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1092.597, -3098.030, -38.00166>>, <<1094.597, -3098.030, -40.00166>>, 2.0) AND IS_ENTITY_FACING_COORD_WITHIN_RANGE(PLAYER_PED_ID(), <<1093.597, -3098.030, -38.00166>>, 80)			BREAK
		//MEDIUM WAREHOUSE
		CASE 3		RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1070.233, -3097.829, -38.00166>>, <<1072.233, -3097.829, -40.00166>>, 2.0) AND IS_ENTITY_FACING_COORD_WITHIN_RANGE(PLAYER_PED_ID(), <<1071.233, -3097.829, -38.00166>>, 80)			BREAK
		CASE 4		RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1070.512, -3107.350, -38.00166>>, <<1072.512, -3107.350, -40.00166>>, 2.0) AND IS_ENTITY_FACING_COORD_WITHIN_RANGE(PLAYER_PED_ID(), <<1071.512, -3107.350, -38.00166>>, 80)			BREAK
		CASE 5		RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1059.139, -3098.929, -38.00166>>, <<1061.139, -3098.929, -40.00166>>, 2.0) AND IS_ENTITY_FACING_COORD_WITHIN_RANGE(PLAYER_PED_ID(), <<1060.139, -3098.929, -38.00166>>, 80)			BREAK
		//LARGE WAREHOUSE
		CASE 6		RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1024.093, -3098.251, -38.00166>>, <<1026.093, -3098.251, -40.00166>>, 2.0) AND IS_ENTITY_FACING_COORD_WITHIN_RANGE(PLAYER_PED_ID(), <<1025.093, -3098.251, -38.00166>>, 80)			BREAK
		CASE 7		RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1000.805, -3092.803, -38.00166>>, <<1002.805, -3092.803, -40.00166>>, 2.0) AND IS_ENTITY_FACING_COORD_WITHIN_RANGE(PLAYER_PED_ID(), <<1001.805, -3092.803, -38.00166>>, 80)			BREAK
		CASE 8		RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<996.8175, -3109.485, -38.00166>>, <<998.8175, -3109.485, -40.00166>>, 2.0) AND IS_ENTITY_FACING_COORD_WITHIN_RANGE(PLAYER_PED_ID(), <<997.8175, -3109.485, -38.00166>>, 80)			BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_SOURCE_CARGO_MENU()	
	SWITCH thisWarehouse.sSourceCargoMenuData.eCurrentState
		CASE SOURCE_CARGO_MENU_STATE_INIT
			SET_SOURCE_CARGO_MENU_STATE(SOURCE_CARGO_MENU_STATE_PROMPT)
		BREAK
		
		CASE SOURCE_CARGO_MENU_STATE_PROMPT
			IF IS_NET_PLAYER_OK(PLAYER_ID())
			AND NOT g_bCelebrationScreenIsActive
			AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
			AND NOT IS_PHONE_ONSCREEN()
			AND NOT IS_BROWSER_OPEN()
			AND NOT IS_ANY_INTERACTION_ANIM_PLAYING()
			AND NOT IS_PAUSE_MENU_ACTIVE_EX()
			AND CAN_INTERACT_WITH_WAREHOUSE_PED()
			AND NOT IS_INTERACTION_MENU_OPEN()
			and IS_LOCAL_PLAYER_IN_WAREHOUSE_THEY_OWN()
			AND NOT g_sWarehouseCargoSourcingData.bRequestCargoSourcing
			AND NOT IS_WAREHOUSE_PED_SOURCING_CARGO(GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID()))
				IF thisWarehouse.sSourceCargoMenuData.iContextIntention = NEW_CONTEXT_INTENTION
					IF GET_SAVE_SLOT_FOR_WAREHOUSE(GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID())) != 0
						REGISTER_CONTEXT_INTENTION(thisWarehouse.sSourceCargoMenuData.iContextIntention, CP_HIGH_PRIORITY, "SRC_CRG_PROMPT")
					ELSE
						REGISTER_CONTEXT_INTENTION(thisWarehouse.sSourceCargoMenuData.iContextIntention, CP_HIGH_PRIORITY, "SRC_CRG_PROMPTL")
					ENDIF
				ELSE
					IF HAS_CONTEXT_BUTTON_TRIGGERED(thisWarehouse.sSourceCargoMenuData.iContextIntention)
						RELEASE_CONTEXT_INTENTION(thisWarehouse.sSourceCargoMenuData.iContextIntention)
						thisWarehouse.sSourceCargoMenuData.iContextIntention = NEW_CONTEXT_INTENTION
						
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
						
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, TRUE)
						
						IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						ENDIF
						
						DISPLAY_RADAR(FALSE)
						
						SET_SOURCE_CARGO_MENU_STATE(SOURCE_CARGO_MENU_STATE_MENU)
					ENDIF
				ENDIF
			ELSE
				IF thisWarehouse.sSourceCargoMenuData.iContextIntention != NEW_CONTEXT_INTENTION
					RELEASE_CONTEXT_INTENTION(thisWarehouse.sSourceCargoMenuData.iContextIntention)
					thisWarehouse.sSourceCargoMenuData.iContextIntention = NEW_CONTEXT_INTENTION
				ENDIF
			ENDIF
		BREAK
		
		CASE SOURCE_CARGO_MENU_STATE_MENU
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LEFT_ONLY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_RIGHT_ONLY)
			ENDIF
			
			RUN_SOURCE_CARGO_MENU()
			
			INVALIDATE_IDLE_CAM()
		BREAK
		
		CASE SOURCE_CARGO_MENU_STATE_END
			CLEANUP_SOURCE_CARGO_MENU()
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_CARGO_SOURCE_PED_BLIP()
	IF g_iPedLayout != -1
	AND thisWarehouse.iID != ciW_Invalid
		IF NOT DOES_BLIP_EXIST(thisWarehouse.biWarehousePed)
			IF GET_SAVE_SLOT_FOR_WAREHOUSE(thisWarehouse.iID) != -1
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSThree, GET_SAVE_SLOT_FOR_WAREHOUSE(thisWarehouse.iID))
				AND NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())	
					IF GET_SAVE_SLOT_FOR_WAREHOUSE(GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID())) != 0
						ADD_BASIC_BLIP(thisWarehouse.biWarehousePed, GET_WAREHOUSE_PED_POSITION(), RADAR_TRACE_VIP, "SRC_CRG_BLIP", 1.0)
					ELSE
						ADD_BASIC_BLIP(thisWarehouse.biWarehousePed, GET_WAREHOUSE_PED_POSITION(), RADAR_TRACE_VIP, "SRC_CRG_BLIP_L", 1.0)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSThree, GET_SAVE_SLOT_FOR_WAREHOUSE(thisWarehouse.iID))
			OR IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
				SAFE_REMOVE_BLIP(thisWarehouse.biWarehousePed)
			ENDIF
			
			
			IF IS_BIT_SET(thisWarehouse.iBS, BS_FLASH_STAFF_BLIP)
				CLEAR_BIT(thisWarehouse.iBS, BS_FLASH_STAFF_BLIP)
				IF DOES_BLIP_EXIST(thisWarehouse.biWarehousePed)
					SET_BLIP_FLASHES(thisWarehouse.biWarehousePed, TRUE)
					SET_BLIP_FLASH_TIMER(thisWarehouse.biWarehousePed, ciWAREHOUSE_PED_HELP_TEXT_AND_BLIP_TIME)
				ENDIF
			ENDIF
		ENDIF
	ELIF g_iPedLayout = -1
		IF DOES_BLIP_EXIST(thisWarehouse.biWarehousePed)
			SAFE_REMOVE_BLIP(thisWarehouse.biWarehousePed)
		ENDIF
	ENDIF
ENDPROC
#ENDIF

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                 MAIN LOGIC PROC                  /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

PROC RUN_MAIN_CLIENT_LOGIC()
	
	MAINTAIN_WAREHOUSE_CRATES()
	MAINTAIN_VEH_UPGRADE_MENU(thisWarehouse.sWUpgradeCabinet)
	RUN_WAREHOUSE_SEAT_LOGIC(warehouseSeatData)
	MAINTAIN_EXIT_VIA_GARAGE_DOOR()
	MAINTAIN_GOON_WAREHOUSE_HELP()
	
	#IF FEATURE_DLC_1_2022		
	IF IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
		MAINTAIN_PED_SCRIPT_LAUNCHING(PED_LOCATION_WAREHOUSE, thisWarehouse.bPedScriptLaunched, thisWarehouse.iScriptInstance, FALSE)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PED_SCRIPT_LAUNCHING")
		#ENDIF
		#ENDIF	
	ENDIF
	#ENDIF
	
	IF NATIVE_TO_INT(PLAYER_ID()) != -1
		CLIENT_MAINTAIN_MP_RADIO(serverBD.MPRadioServer, playerBD[NATIVE_TO_INT(PLAYER_ID())].MPRadioClient, MPRadioLocal, InteriorPropStruct, playerBD[NATIVE_TO_INT(PLAYER_ID())].iActivityRequested, serverBD.activityControl)	
	ENDIF
	
	IF IS_WAREHOUSE_STATE(WAREHOUSE_STATE_LOADING)
		INIT_WAREHOUSE_SEAT_DATA()
								
		BOOL bReady = TRUE
		
		IF NOT DOES_ENTITY_EXIST(thisWarehouse.sWUpgradeCabinet.objectCabinet) 
			// All conditions that must be met before we fade screen in should be here
			bReady = FALSE
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - Waiting for sWUpgradeCabinet.objectCabinet to be created...")
			#ENDIF
		ENDIF
		
		IF g_SimpleInteriorData.bWarehouseShouldWaitForDeliverySound
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - Waiting for delivery sound to start...")
			#ENDIF
			g_SimpleInteriorData.bWarehouseShouldWaitForDeliverySound = FALSE
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(TRUE)
			bReady = FALSE
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(g_SimpleInteriorData.timerDeliverySoundStart)
		
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - Waiting for delivery sound to finish...")
			#ENDIF
		
			// Wait till the sound plays 6s
			// For int script not to time out waiting for warehouse script we set warehouse script in control
			IF HAS_NET_TIMER_EXPIRED(g_SimpleInteriorData.timerDeliverySoundStart, 6000)
			AND bReady
				IF NOT SHOULD_DO_WAREHOUSE_INTRO_CUTSCENE() // If we still have to play the cutscene then remain in control
					SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(FALSE)
				ENDIF
			ELSE
				bReady = FALSE
				SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(TRUE)
			ENDIF
		ENDIF
		
		IF thisWarehouse.bWaitingForCrateData
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(TRUE)
			bReady = FALSE
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_WAREHOUSE - Waiting for crate data to be sent from WH owner...")
			#ENDIF
		ENDIF
		
		IF NOT HAVE_ALL_PEDS_BEEN_CREATED()
			PRINTLN("AM_MP_WAREHOUSE- LOAD_INTERIOR - Waiting for the peds to be created.")
			bReady = FALSE
			
			IF HAS_NET_TIMER_EXPIRED(thisWarehouse.stPedLaunchingBailTimer, g_iPedScriptLaunchingBailTimerMS)
				PRINTLN("AM_MP_WAREHOUSE - LOAD_INTERIOR - Ped script has taken longer than: ", g_iPedScriptLaunchingBailTimerMS, "ms so setting g_bInitPedsCreated to TRUE.")
				RESET_NET_TIMER(thisWarehouse.stPedLaunchingBailTimer)
				g_bInitPedsCreated = TRUE
			ENDIF
		ENDIF
			
		IF bReady
			IF SHOULD_DO_WAREHOUSE_INTRO_CUTSCENE()
				IF CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL() // Wait till it's fine to start the cutscene
					START_WAREHOUSE_INTRO_CUTSCENE()
					SET_WAREHOUSE_STATE(WAREHOUSE_STATE_PLAYING_INTRO)
				ENDIF
			ELSE
				SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE) // This is important, let internal script know that its child script is ready
				SET_WAREHOUSE_STATE(WAREHOUSE_STATE_IDLE)
			ENDIF
		ENDIF
		
	ELIF IS_WAREHOUSE_STATE(WAREHOUSE_STATE_IDLE)
	
		#IF FEATURE_DLC_1_2022
		MAINTAIN_NEW_WAREHOUSE_STAFF_HELP()
		MAINTAIN_CARGO_SOURCE_PED_BLIP()
		MAINTAIN_SOURCE_CARGO_MENU()
		MAINTAIN_LAUNCHING_CARGO_SOURCING()
		#ENDIF
		
		IF NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
			// Once in a while check if we should play intro cutscene - we might be a boss who hasnt seen it yet due to being on buy mission
			// or a goon whose boss is wathcing the cutscene right now
			IF (GET_FRAME_COUNT() % 10 = 0) OR IS_SCREEN_FADED_OUT()
				IF SHOULD_DO_WAREHOUSE_INTRO_CUTSCENE()
				
					IF SHOULD_LOCAL_PLAYER_BE_AUTOWARPED_INSIDE_THIS_SIMPLE_INTERIOR(thisWarehouse.eSimpleInteriorID)
						SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(TRUE)
						SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(FALSE)
					ENDIF
				
					IF NOT IS_SCREEN_FADED_OUT()
						IF NOT IS_SCREEN_FADING_OUT()
							DO_SCREEN_FADE_OUT(SIMPLE_INTERIOR_SCREEN_FADE_TIME)
						ENDIF
					ELSE
						START_WAREHOUSE_INTRO_CUTSCENE()
						SET_WAREHOUSE_STATE(WAREHOUSE_STATE_PLAYING_INTRO)
					ENDIF
				ENDIF
			ENDIF
			
			// Check if player is around garage door exit
			IF IS_PLAYER_IN_WAREHOUSE_EXIT_LOCATE()
				g_SimpleInteriorData.bShouldExitMenuBeVisible = TRUE
				
				// Start exit
				IF g_SimpleInteriorData.bExitMenuOptionAccepted
					START_EXIT_VIA_GARAGE_DOOR()
				ENDIF
			ELSE
				g_SimpleInteriorData.bShouldExitMenuBeVisible = FALSE
			ENDIF
			
			IF thisWarehouse.pOwner = PLAYER_ID()
				IF IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
					SAFE_REMOVE_BLIP(thisWarehouse.laptopBlip)
					SAFE_REMOVE_BLIP(thisWarehouse.workbenchBlip)
				ELIF NOT DOES_BLIP_EXIST(thisWarehouse.laptopBlip)
					EWarehouseSize size = GET_WAREHOUSE_SIZE(thisWarehouse.iID)
					VECTOR vLaptopCoord = <<1087.8722, -3101.3035, -39.1062>>
					VECTOR vWorkbenchCoord = <<1100.5688, -3102.0576, -39.0422>>
					FLOAT fScale = 1.0
					
					IF size = eWarehouseLarge
						fScale = 0.75
						vLaptopCoord = <<995.1203, -3099.9861, -39.0691>>
						vWorkbenchCoord = <<996.6937, -3099.1797, -39.0702>>
					ELIF size = eWarehouseMedium
						vLaptopCoord = <<1048.5197, -3100.6843, -39.0494>>
						vWorkbenchCoord = <<1050.4222, -3094.1421, -39.1122>>
					ENDIF
					
					ADD_BASIC_BLIP(thisWarehouse.laptopBlip, vLaptopCoord, RADAR_TRACE_LAPTOP, "BLIP_521", fScale)
					ADD_BASIC_BLIP(thisWarehouse.workbenchBlip, vWorkbenchCoord, RADAR_TRACE_GR_MOC_UPGRADE, "WHOUSE_WB_BLP", fScale)
				ENDIF
			ENDIF
			
			#IF FEATURE_GEN9_EXCLUSIVE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_WAREHOUSEVISITED, TRUE)
			#ENDIF
		ELSE
			// If player starts walking out cleanup the radio
			IF MPRadioLocal.eStage != MP_RADIO_CLIENT_STAGE_INIT
				CLEANUP_MP_RADIO(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPRadioClient, MPRadioLocal)
				IF IS_INTERACTION_MENU_DISABLED()
					ENABLE_INTERACTION_MENU()
				ENDIF
			ENDIF
		ENDIF
		
	ELIF IS_WAREHOUSE_STATE(WAREHOUSE_STATE_PLAYING_INTRO)
	
		IF SHOULD_LOCAL_PLAYER_BE_AUTOWARPED_INSIDE_THIS_SIMPLE_INTERIOR(thisWarehouse.eSimpleInteriorID)
			IF NOT IS_SIMPLE_INTERIOR_GLOBAL_BIT_SET(SI_BS_CanFinishAutoWarp, thisWarehouse.eSimpleInteriorID)
				// Wait till we're done autowarping becofre can do the cutscene
				EXIT
			ENDIF
		ENDIF
	
		MAINTAIN_WAREHOUSE_INTRO_CUTSCENE()
		
		IF NOT warehouseIntroCutscene.bPlaying
			IF NOT IS_SIMPLE_INTERIOR_CHILD_SCRIPT_READY()
				SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
				SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(FALSE)
				g_SimpleInteriorData.bDontSimulateInputGaitOnEntrance = TRUE
			ENDIF
			#IF FEATURE_DLC_1_2022
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DONE_NEW_WH_STAFF_FEATURE_HELP, TRUE)
			#ENDIF
			SET_WAREHOUSE_STATE(WAREHOUSE_STATE_IDLE)
		ENDIF
		
	ELIF IS_WAREHOUSE_STATE(WAREHOUSE_STATE_LOADING_GARAGE_EXIT)
	
		IF IS_SIMPLE_INTERIOR_ENTRY_ANIM_STAGE(SIMPLE_INTERIOR_ANIM_STAGE_ABORTED, thisWarehouse.garageExitAnim)
			STOP_SIMPLE_INTERIOR_ENTRY_ANIM(thisWarehouse.garageExitAnim, TRUE)
			EXIT
		ENDIF

		IF NOT IS_SIMPLE_INTERIOR_ENTRY_ANIM_STAGE(SIMPLE_INTERIOR_ANIM_STAGE_LOADING, thisWarehouse.garageExitAnim)
			// Block control and move to the next stage only if entry anim has started
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
				NETWORK_FADE_OUT_ENTITY(PLAYER_PED_ID(), TRUE, TRUE)
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_PREVENT_VISIBILITY_CHANGES)
			ENDIF
			SET_WAREHOUSE_STATE(WAREHOUSE_STATE_PLAYING_GARAGE_EXIT)
		ENDIF
	
	ELIF IS_WAREHOUSE_STATE(WAREHOUSE_STATE_PLAYING_GARAGE_EXIT)
		IF IS_SIMPLE_INTERIOR_ENTRY_ANIM_STAGE(SIMPLE_INTERIOR_ANIM_STAGE_FINISHED, thisWarehouse.garageExitAnim)
			DO_SCREEN_FADE_OUT(SIMPLE_INTERIOR_SCREEN_FADE_TIME)
			SET_WAREHOUSE_STATE(WAREHOUSE_STATE_FINISHING_GARAGE_EXIT)
		ENDIF
		
	ELIF IS_WAREHOUSE_STATE(WAREHOUSE_STATE_FINISHING_GARAGE_EXIT)
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_BIT_SET(thisWarehouse.garageExitAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_FADE_OUT_FINISH_SOUND_PLAYED)
				IF NOT IS_STRING_NULL_OR_EMPTY(thisWarehouse.garageExitAnim.strOnEnterFadeOutFinishSoundName)
				AND NOT IS_STRING_NULL_OR_EMPTY(thisWarehouse.garageExitAnim.strOnEnterFadeOutFinishSoundSet)
					PLAY_SOUND_FRONTEND(-1, thisWarehouse.garageExitAnim.strOnEnterFadeOutFinishSoundName, thisWarehouse.garageExitAnim.strOnEnterFadeOutFinishSoundSet)
				ENDIF
				SET_BIT(thisWarehouse.garageExitAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_FADE_OUT_FINISH_SOUND_PLAYED)
			ENDIF
			STOP_SIMPLE_INTERIOR_ENTRY_ANIM(thisWarehouse.garageExitAnim)
			TRIGGER_EXIT_FROM_SIMPLE_INTERIOR_NOW()
		ENDIF
	ENDIF
	
ENDPROC

PROC RUN_MAIN_SERVER_LOGIC()
	SERVER_MAINTAIN_MP_RADIO(serverBD.MPRadioServer, playerBD[MPRadioLocal.iServerPlayerStagger].MPRadioClient, MPRadioLocal , InteriorPropStruct )
	SERVER_MAINTAIN_PROPERTY_ACTIVITIES(playerBD[serverLocalActControl.iSlowPlayerLoop].iActivityRequested,serverBD.activityControl,serverLocalActControl)
ENDPROC

PROC MAINTAIN_OWNER_AS_HOST()
	IF thisWarehouse.pOwner = PLAYER_ID()
		IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF NOT HAS_NET_TIMER_STARTED(thisWarehouse.tSyncBDTimer)
				START_NET_TIMER(thisWarehouse.tSyncBDTimer)
			ELIF HAS_NET_TIMER_EXPIRED(thisWarehouse.tSyncBDTimer, 3000)
				IF NOT HAS_NET_TIMER_STARTED(thisWarehouse.tHostRequestTimer)
					PRINTLN("AM_MP_NIGHTCLUB - MAINTAIN_OWNER_AS_HOST - Requesting to be host.")
					
					NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT()
					
					START_NET_TIMER(thisWarehouse.tHostRequestTimer, TRUE)
				ELIF HAS_NET_TIMER_EXPIRED(thisWarehouse.tHostRequestTimer, 1000, TRUE)					
					RESET_NET_TIMER(thisWarehouse.tHostRequestTimer)
				ENDIF
			ENDIF
//		ELSE
//			IF SIMPLE_INTERIOR_IS_LOCAL_PLAYER_ENTERING_WITH_VEHICLE()
//			AND m_ServerBD.iEnteringVeh = -1
//				m_ServerBD.iEnteringVeh = 0
//				PRINTLN("AM_MP_NIGHTCLUB - MAINTAIN_OWNER_AS_HOST - m_ServerBD.iEnteringVeh: ", m_ServerBD.iEnteringVeh)
//			ENDIF
		ENDIF
	ENDIF
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT  (SIMPLE_INTERIOR_CHILD_SCRIPT_DATA scriptData) 
	
	IF NETWORK_IS_GAME_IN_PROGRESS() 
		SCRIPT_INITIALISE(scriptData)	
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
				
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("AM_MP_WAREHOUSE - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE(scriptData.iScriptInstance, thisWarehouse.eSimpleInteriorID)
			PRINTLN("AM_MP_WAREHOUSE - SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE = TRUE")
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		
		RUN_MAIN_CLIENT_LOGIC()
		
		#IF FEATURE_DLC_1_2022
		MAINTAIN_OWNER_AS_HOST()
		#ENDIF
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF
		
		#IF IS_DEBUG_BUILD
			UPDATE_DEBUG_WIDGETS()
		#ENDIF
		
	ENDWHILE
	

ENDSCRIPT
