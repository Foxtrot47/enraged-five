USING "globals.sch"
USING "rage_builtins.sch"

USING "fm_content_test_structs.sch"
USING "fm_content_gb_mission_structs.sch"
USING "fm_content_generic_structs.sch"

//////////////////////////
///    HELPERS
//////////////////////////
FUNC FMC_TEST_VARIATION GET_VARIATION()
	RETURN serverBD.sMissionData.eVariation
ENDFUNC

FUNC FMC_TEST_SUBVARIATION GET_SUBVARIATION()
	RETURN serverBD.sMissionData.eSubvariation
ENDFUNC

FUNC INT GET_FMC_TEST_CONTENT_FMMC_TYPE(FMC_TEST_CONTENT eContent)
	SWITCH eContent
		CASE eFMCTESTCONTENT_DRUG_VEHICLE
			RETURN FMMC_TYPE_DRUG_VEHICLE
		
		CASE eFMCTESTCONTENT_MOVIE_PROPS
			RETURN FMMC_TYPE_MOVIE_PROPS
		
		CASE eFMCTESTCONTENT_SUPER_BUSINESS_BATTLES
			RETURN FMMC_TYPE_SUPER_BUSINESS_BATTLES
		
		CASE eFMCTESTCONTENT_ISLAND_DJ_MISSION
			RETURN FMMC_TYPE_ISLAND_DJ_MISSION
		
		CASE eFMCTESTCONTENT_ISLAND_HEIST_PREP
			RETURN FMMC_TYPE_ISLAND_HEIST_PREP
		
		CASE eFMCTESTCONTENT_TUNER_ROBBERY
			RETURN FMMC_TYPE_TUNER_ROBBERY
		
		CASE eFMCTESTCONTENT_FIXER_VIP
			RETURN FMMC_TYPE_FIXER_VIP
		
		CASE eFMCTESTCONTENT_FIXER_SECURITY
			RETURN FMMC_TYPE_FIXER_SECURITY
		
		CASE eFMCTESTCONTENT_FIXER_PAYPHONE
			RETURN FMMC_TYPE_FIXER_PAYPHONE
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC FMC_TEST_CONTENT FIND_NEXT_FMC_TEST_CONTENT(FMC_TEST_CONTENT eContent = eFMCTESTCONTENT_INVALID, BOOL bReverse = FALSE)
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] FIND_NEXT_FMC_TEST_CONTENT - eContent = ", GET_FMC_TEST_CONTENT_NAME(eContent))
	
	IF eContent = eFMCTESTCONTENT_INVALID
		eContent = INT_TO_ENUM(FMC_TEST_CONTENT, COUNT_OF(FMC_TEST_CONTENT) - 1)
		
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] FIND_NEXT_FMC_TEST_CONTENT - eContent = eFMCTESTCONTENT_INVALID, returning last content (", GET_FMC_TEST_CONTENT_NAME(eContent), ", ).")
		
		RETURN eContent
	ENDIF
	
	INT iChange = 1
	
	IF bReverse
		iChange = -1
	ENDIF
	
	INT iContent = ENUM_TO_INT(eContent)
	iContent = IWRAP_INDEX(iContent + iChange, COUNT_OF(FMC_TEST_CONTENT))
	eContent = INT_TO_ENUM(FMC_TEST_CONTENT, iContent)
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] FIND_NEXT_FMC_TEST_CONTENT - Next content found, returning ", GET_FMC_TEST_CONTENT_NAME(eContent))
	
	RETURN eContent
ENDFUNC

FUNC INT FIND_NEXT_FMC_VARIATION(INT iFMMCType, INT iVariation = -1, BOOL bReverse = FALSE)
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] FIND_NEXT_FMC_VARIATION - iFMMCType = ", GANG_BOSS_GET_NAME_FOR_TYPE_FOR_DEBUG_PRINT(iFMMCType), ", iVariation = ", GET_FREEMODE_CONTENT_VARIATION_NAME(iFMMCType, iVariation), ", bReverse = ", GET_STRING_FROM_BOOL(bReverse))
	
	IF GET_FREEMODE_CONTENT_MAX_VARIATIONS(iFMMCType) < 1
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] FIND_NEXT_FMC_VARIATION - GET_FREEMODE_CONTENT_MAX_VARIATIONS(iFMMCType) < 1, returning -1.")
		
		RETURN -1
	ENDIF
	
	INT iChange = 1
	
	IF bReverse
		iChange = -1
		iVariation = CLAMP_INT(iVariation, 0, GET_FREEMODE_CONTENT_MAX_VARIATIONS(iFMMCType))
	ELSE
		iVariation = CLAMP_INT(iVariation, -1, GET_FREEMODE_CONTENT_MAX_VARIATIONS(iFMMCType) - 1)
	ENDIF
	
	iVariation = IWRAP_INDEX(iVariation + iChange, GET_FREEMODE_CONTENT_MAX_VARIATIONS(iFMMCType))
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] FIND_NEXT_FMC_VARIATION - Next FMC variation found, returning ", GET_FREEMODE_CONTENT_VARIATION_NAME(iFMMCType, iVariation))
	
	RETURN iVariation
ENDFUNC

FUNC INT FIND_NEXT_FMC_SUBVARIATION(INT iFMMCType, INT iVariation, INT iSubvariation = -1, BOOL bReverse = FALSE)
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] FIND_NEXT_FMC_SUBVARIATION - iFMMCType = ", GANG_BOSS_GET_NAME_FOR_TYPE_FOR_DEBUG_PRINT(iFMMCType), ", iVariation = ", GET_FREEMODE_CONTENT_VARIATION_NAME(iFMMCType, iVariation), ", iSubvariation = ", GET_FREEMODE_CONTENT_SUBVARIATION_NAME(iFMMCType, iSubvariation), ", bReverse = ", GET_STRING_FROM_BOOL(bReverse))
	
	IF GET_FREEMODE_CONTENT_MAX_SUBVARIATIONS(iFMMCType, iVariation) < 1
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] FIND_NEXT_FMC_SUBVARIATION - GET_FREEMODE_CONTENT_MAX_SUBVARIATIONS(iFMMCType, iSubvariation) < 1, returning -1.")
		
		RETURN -1
	ENDIF
	
	INT iStartSubvariation = GET_FREEMODE_CONTENT_START_SUBVARIATION(iFMMCType, iVariation)
	INT iNumSubvariations = GET_FREEMODE_CONTENT_MAX_SUBVARIATIONS(iFMMCType, iVariation)
	
	iSubvariation -= iStartSubvariation
	
	INT iChange = 1
	
	IF bReverse
		iChange = -1
		iSubvariation = CLAMP_INT(iSubvariation, 0, iNumSubvariations)
	ELSE
		iSubvariation = CLAMP_INT(iSubvariation, -1, iNumSubvariations - 1)
	ENDIF
	
	iSubvariation = IWRAP_INDEX(iSubvariation + iChange, iNumSubvariations)
	iSubvariation += iStartSubvariation
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] FIND_NEXT_FMC_SUBVARIATION - Next FMC subvariation found, returning ", GET_FREEMODE_CONTENT_SUBVARIATION_NAME(iFMMCType, iSubvariation))
	
	RETURN iSubvariation
ENDFUNC

FUNC BOOL SHOULD_PROCESS_QUICK_SET_DATA_FILENAME()
	IF NOT IS_PC_VERSION()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC STRING GET_QUICK_SET_DATA_FILENAME_HELP_LABEL()
	RETURN "FMCT_SBX_QSDF"
ENDFUNC

PROC CLEANUP_QUICK_SET_DATA_FILENAME()
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [QUICK_SET_DATA_FILENAME] CLEANUP_QUICK_SET_DATA_FILENAME")
	DEBUG_PRINTCALLSTACK()
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_QUICK_SET_DATA_FILENAME_HELP_LABEL())
		CLEAR_HELP()
	ENDIF
	
	sLocalVariables.sQuickSetDataFilename.eContent = eFMCTESTCONTENT_INVALID
	sLocalVariables.sQuickSetDataFilename.eContentCached = eFMCTESTCONTENT_INVALID
	sLocalVariables.sQuickSetDataFilename.iVariation = -1
	sLocalVariables.sQuickSetDataFilename.iVariationCached = -1
	sLocalVariables.sQuickSetDataFilename.iSubvariation = -1
	sLocalVariables.sQuickSetDataFilename.iSubvariationCached = -1
ENDPROC

PROC PROCESS_QUICK_SET_DATA_FILENAME_INPUT()
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		EXIT
	ENDIF
	
	FMC_TEST_INPUT eInput
	
	IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
		eInput = eFMCTESTINTPUT_CONTENT_PREVIOUS
	ELIF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
		eInput = eFMCTESTINTPUT_CONTENT_NEXT
	ELIF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
		eInput = eFMCTESTINTPUT_VARIATION_PREVIOUS
	ELIF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
		eInput = eFMCTESTINTPUT_VARIATION_NEXT
	ELIF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LS)
		eInput = eFMCTESTINTPUT_SUBVARIATION_PREVIOUS
	ELIF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_RS)
		eInput = eFMCTESTINTPUT_SUBVARIATION_NEXT
	ENDIF
	
	IF eInput != eFMCTESTINTPUT_NONE
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [QUICK_SET_DATA_FILENAME] PROCESS_QUICK_SET_DATA_FILENAME_INPUT - eInput = ", GET_FMC_TEST_INPUT_NAME(eInput))
		
		SWITCH eInput
			CASE eFMCTESTINTPUT_CONTENT_PREVIOUS
				sLocalVariables.sQuickSetDataFilename.eContent = FIND_NEXT_FMC_TEST_CONTENT(sLocalVariables.sQuickSetDataFilename.eContent, TRUE)
			BREAK
			
			CASE eFMCTESTINTPUT_CONTENT_NEXT
				sLocalVariables.sQuickSetDataFilename.eContent = FIND_NEXT_FMC_TEST_CONTENT(sLocalVariables.sQuickSetDataFilename.eContent)
			BREAK
			
			CASE eFMCTESTINTPUT_VARIATION_PREVIOUS
				sLocalVariables.sQuickSetDataFilename.iVariation = FIND_NEXT_FMC_VARIATION(GET_FMC_TEST_CONTENT_FMMC_TYPE(sLocalVariables.sQuickSetDataFilename.eContent), sLocalVariables.sQuickSetDataFilename.iVariation, TRUE)
			BREAK
			
			CASE eFMCTESTINTPUT_VARIATION_NEXT
				sLocalVariables.sQuickSetDataFilename.iVariation = FIND_NEXT_FMC_VARIATION(GET_FMC_TEST_CONTENT_FMMC_TYPE(sLocalVariables.sQuickSetDataFilename.eContent), sLocalVariables.sQuickSetDataFilename.iVariation)
			BREAK
			
			CASE eFMCTESTINTPUT_SUBVARIATION_PREVIOUS
				sLocalVariables.sQuickSetDataFilename.iSubvariation = FIND_NEXT_FMC_SUBVARIATION(GET_FMC_TEST_CONTENT_FMMC_TYPE(sLocalVariables.sQuickSetDataFilename.eContent), sLocalVariables.sQuickSetDataFilename.iVariation, sLocalVariables.sQuickSetDataFilename.iSubvariation, TRUE)
			BREAK
			
			CASE eFMCTESTINTPUT_SUBVARIATION_NEXT
				sLocalVariables.sQuickSetDataFilename.iSubvariation = FIND_NEXT_FMC_SUBVARIATION(GET_FMC_TEST_CONTENT_FMMC_TYPE(sLocalVariables.sQuickSetDataFilename.eContent), sLocalVariables.sQuickSetDataFilename.iVariation, sLocalVariables.sQuickSetDataFilename.iSubvariation)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC PROCESS_QUICK_SET_DATA_FILENAME_UPDATE(TEXT_LABEL_63& tlDataFilename)
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_QUICK_SET_DATA_FILENAME_HELP_LABEL())
			CLEAR_HELP()
		ENDIF
	ELSE
		PRINT_HELP_FOREVER(GET_QUICK_SET_DATA_FILENAME_HELP_LABEL())
	ENDIF
	
	BOOL bUpdated
	
	IF sLocalVariables.sQuickSetDataFilename.eContentCached != sLocalVariables.sQuickSetDataFilename.eContent
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [QUICK_SET_DATA_FILENAME] PROCESS_QUICK_SET_DATA_FILENAME_UPDATE - Content changed, ", sLocalVariables.sQuickSetDataFilename.eContentCached, " != ", sLocalVariables.sQuickSetDataFilename.eContent)
		
		sLocalVariables.sQuickSetDataFilename.eContentCached = sLocalVariables.sQuickSetDataFilename.eContent
		
		sLocalVariables.sQuickSetDataFilename.iVariation = FIND_NEXT_FMC_VARIATION(GET_FMC_TEST_CONTENT_FMMC_TYPE(sLocalVariables.sQuickSetDataFilename.eContent))
		sLocalVariables.sQuickSetDataFilename.iSubvariation = FIND_NEXT_FMC_SUBVARIATION(GET_FMC_TEST_CONTENT_FMMC_TYPE(sLocalVariables.sQuickSetDataFilename.eContent), sLocalVariables.sQuickSetDataFilename.iVariation)
		bUpdated = TRUE
	ENDIF
	
	IF sLocalVariables.sQuickSetDataFilename.iVariationCached != sLocalVariables.sQuickSetDataFilename.iVariation
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [QUICK_SET_DATA_FILENAME] PROCESS_QUICK_SET_DATA_FILENAME_UPDATE - Variation changed, ", sLocalVariables.sQuickSetDataFilename.iVariationCached, " != ", sLocalVariables.sQuickSetDataFilename.iVariation)
		
		sLocalVariables.sQuickSetDataFilename.iVariationCached = sLocalVariables.sQuickSetDataFilename.iVariation
		
		sLocalVariables.sQuickSetDataFilename.iSubvariation = FIND_NEXT_FMC_SUBVARIATION(GET_FMC_TEST_CONTENT_FMMC_TYPE(sLocalVariables.sQuickSetDataFilename.eContent), sLocalVariables.sQuickSetDataFilename.iVariation)
		bUpdated = TRUE
	ENDIF
	
	IF sLocalVariables.sQuickSetDataFilename.iSubvariationCached != sLocalVariables.sQuickSetDataFilename.iSubvariation
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [QUICK_SET_DATA_FILENAME] PROCESS_QUICK_SET_DATA_FILENAME_UPDATE - Subvariation changed, ", sLocalVariables.sQuickSetDataFilename.iSubvariationCached, " != ", sLocalVariables.sQuickSetDataFilename.iSubvariation)
		
		sLocalVariables.sQuickSetDataFilename.iSubvariationCached = sLocalVariables.sQuickSetDataFilename.iSubvariation
		
		bUpdated = TRUE
	ENDIF
	
	IF bUpdated
		tlDataFilename = FREEMODE_CONTENT_GET_MISSION_PLACEMENT_DATA_FILENAME(GET_FMC_TEST_CONTENT_FMMC_TYPE(sLocalVariables.sQuickSetDataFilename.eContent), sLocalVariables.sQuickSetDataFilename.iVariation, sLocalVariables.sQuickSetDataFilename.iSubvariation)
		sLocalVariables.sOnscreenKeyboard.iStage = OSK_STAGE_SET_UP
		
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [QUICK_SET_DATA_FILENAME] PROCESS_QUICK_SET_DATA_FILENAME_UPDATE - Updating onscreen keyboard, tlDataFilename = ", tlDataFilename)
	ENDIF
ENDPROC

PROC PROCESS_QUICK_SET_DATA_FILENAME(TEXT_LABEL_63& tl63DataFilename)
	IF NOT SHOULD_PROCESS_QUICK_SET_DATA_FILENAME()
		EXIT
	ENDIF
	
	PROCESS_QUICK_SET_DATA_FILENAME_INPUT()
	PROCESS_QUICK_SET_DATA_FILENAME_UPDATE(tl63DataFilename)
ENDPROC

PROC CLEANUP_ONSCREEN_KEYBOARD()
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] CLEANUP_ONSCREEN_KEYBOARD")
	DEBUG_PRINTCALLSTACK()
	
	#IF IS_DEBUG_BUILD
	IF sLocalVariables.sOnscreenKeyboard.iStage != OSK_STAGE_SET_UP
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] CLEANUP_ONSCREEN_KEYBOARD - Calling SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)")
		SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)
	ENDIF
	#ENDIF
	
	sLocalVariables.sOnscreenKeyboard.iStage = OSK_STAGE_SET_UP
ENDPROC

FUNC BOOL PROCESS_ONSCREEN_KEYBOARD(STRING strTitleLabel, TEXT_LABEL_63& tl63Input)
	OSK_STATUS eStatus
	INT iProfanityToken
	
	RUN_ON_SCREEN_KEYBOARD(	eStatus, iProfanityToken, sLocalVariables.sOnscreenKeyboard.iStage,
							DEFAULT,
							FALSE,
							tl63Input,
							DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT,
							TRUE,
							DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT,
							FALSE,
							DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT,
							strTitleLabel)
	
	SWITCH eStatus
		CASE OSK_SUCCESS
			STRING strResult
			strResult = GET_ONSCREEN_KEYBOARD_RESULT()
			
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_ONSCREEN_KEYBOARD - eStatus = OSK_SUCCESS. strResult = ", strResult)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(strResult)
				tl63Input = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(strResult, 0, IMIN(GET_LENGTH_OF_LITERAL_STRING(strResult), FMC_TEST_SANDBOX_TEXT_LABEL_63_LENGTH))
				
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_ONSCREEN_KEYBOARD - tl63Input = ", tl63Input)
			ENDIF
			
			CLEANUP_ONSCREEN_KEYBOARD()
			
			RETURN TRUE
		
		CASE OSK_CANCELLED
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_ONSCREEN_KEYBOARD - eStatus = OSK_CANCELLED. Setting tl63Input = \"\"")
			tl63Input = ""
			
			CLEANUP_ONSCREEN_KEYBOARD()
			
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//////////////////////////
///    OVERRIDES
//////////////////////////
FUNC STRING GET_MISSION_PLACEMENT_DATA_FILENAME()
	SWITCH GET_VARIATION()
		CASE FTV_SANDBOX
			RETURN TextLabelToString(serverBD.sMissionData.tl63DataFilename)
	ENDSWITCH
	
	RETURN FMC_TEST_GET_MISSION_PLACEMENT_DATA_FILENAME(GET_VARIATION(), GET_SUBVARIATION())
ENDFUNC

FUNC BOOL GET_MISSION_PLACEMENT_DATA()
	SWITCH GET_VARIATION()
		CASE FTV_SANDBOX
			IF IS_LOCAL_HOST
				TEXT_LABEL_63 tl63Input
				
				PROCESS_QUICK_SET_DATA_FILENAME(tl63Input)
				
				IF PROCESS_ONSCREEN_KEYBOARD("FMCT_SBX_ENTTTL", tl63Input)
					serverBD.sMissionData.tl63DataFilename = tl63Input
					
					CLEANUP_QUICK_SET_DATA_FILENAME()
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF LOAD_LOCAL_FREEMODE_CONTENT_DATA(GET_MISSION_PLACEMENT_DATA_FILENAME(), data, sLoad)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_MISSION_PLACEMENT_DATA - Successfully loaded data file: ", GET_MISSION_PLACEMENT_DATA_FILENAME())
				
				SET_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_LOADED_DATA_FILE)
			ENDIF
		BREAK
		
		CASE FTV_CARGOPLANE
			RETURN LOAD_LOCAL_FREEMODE_CONTENT_DATA(GET_MISSION_PLACEMENT_DATA_FILENAME(), data, sLoad)
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING GET_VARIATION_NAME_FOR_DEBUG()
	RETURN FMC_TEST_GET_VARIATION_NAME_FOR_DEBUG(GET_VARIATION())
ENDFUNC

FUNC STRING GET_SUBVARIATION_NAME_FOR_DEBUG()
	RETURN FMC_TEST_GET_SUBVARIATION_NAME_FOR_DEBUG(GET_SUBVARIATION())
ENDFUNC
#ENDIF

FUNC BOOL SETUP_VARIATION(INT iVariation, INT iSubvariation)
	serverBD.sMissionData.eVariation = INT_TO_ENUM(FMC_TEST_VARIATION, iVariation)
	serverBD.sMissionData.eSubvariation = INT_TO_ENUM(FMC_TEST_SUBVARIATION, iSubvariation)
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SETUP_VARIATION - Variation: ", GET_VARIATION_NAME_FOR_DEBUG())
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SETUP_VARIATION - Subvariation: ", GET_SUBVARIATION_NAME_FOR_DEBUG())
	
	RETURN TRUE
ENDFUNC

PROC MISSION_CLEANUP()
	CLEANUP_QUICK_SET_DATA_FILENAME()
	CLEANUP_ONSCREEN_KEYBOARD()
ENDPROC
