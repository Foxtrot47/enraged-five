USING "rage_builtins.sch"
USING "globals.sch"

USING "net_cloud_mission_loader_public.sch"
USING "fmmc_mp_setup_mission.sch"



// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Cloud_Mission_Loader.sc
//      CREATED         :   Keith
//      DESCRIPTION     :   A helper function for the mission triggerer to load cloud data without including the structs in a header.
//		NOTES			:	This will sit and wait for global variables to be set to tell it to load a apecific mission from the cloud.
//							This isn't a network script, but it runs during the network game.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************


// Structs needed for cloud-loaded mission function to work
// NOTE: The Mission Header Details struct is also copied into g_cloudLoadedMissionHeader for temporary access following successful downloading.
GET_UGC_CONTENT_STRUCT				sGetUGC_content
CLOUD_LOADED_MISSION_HEADER_DETAILS	theReturnedHeaderDetails

// KGM 19/11/14 [BUG 2137672]: Introduce a download safety timeout (initially for Heist Prep downloads but could be used elsewhere if needed)
CONST_INT	KGM_CLOUD_DOWNLOAD_SAFETY_TIMEOUT_msec		50000
INT			m_cloudDownloadSafetyTimeout = 0

// KGM 15/1/15 [BUG 2197556]: Kill this script when on a Heist mission to save memory in LastGen
BOOL		m_inUseThisFrame = FALSE


SCRIPT
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME() // Added by Steve T under Keith's direction to facilitate cross session invites.

	PRINTLN(".KGM [CloudLoader]: net_cloud_mission_loader.sc LAUNCHED")

	WHILE (TRUE)
		WAIT(0)
		
		// KGM 15/1/15 [BUG 2197556]: Used when figuring out if it's safe to kill this script to save memory in LastGen on a Heist
		m_inUseThisFrame = FALSE
		
		// KGM 21/4/15 [BUG 2291852]: Ensure the request to cancel a cloud loaded mission download gets cleared
		BOOL cancelMissionDataDownload = Is_Cloud_Loaded_Mission_Data_Load_To_Be_Cancelled()
		g_sCloudMissionLoaderMP.cmlCancelLoad = FALSE
		
		// Every frame, clear this - it'll be set to TRUE below if necessary
		g_prepMissionDownloadInProgress = FALSE
		
//		// Kill the script if the network game isn't in progress
//		IF NOT (NETWORK_IS_GAME_IN_PROGRESS())
//			PRINTSTRING("...KGM MP [CloudLoader]: net_cloud_mission_loader.sc TERMINATED") PRINTNL()
//			TERMINATE_THIS_THREAD()
//		ENDIF
		
		// Is mission data required to be loaded?
		IF (Is_Cloud_Loaded_Mission_Data_Required_To_Be_Loaded())
			// KGM 15/1/15 [BUG 2197556]: Set to in-use this frame to prevent this script being killed to save memory in LastGen when on a Heist
			m_inUseThisFrame = TRUE
		
			// ...yes, so setup (or continue) the load
			MP_MISSION_ID_DATA		theMissionIdData	= g_sCloudMissionLoaderMP.cmlMissionIdData
			TEXT_LABEL_31			theFilename			= theMissionIdData.idCloudFilename
			INT						theMissionType		= Convert_MissionID_To_FM_Mission_Type(g_sCloudMissionLoaderMP.cmlMissionIdData.idMission)
			INT						myPlayerGBD			= NATIVE_TO_INT(PLAYER_ID())
			
			// If specific offline content is required, modify the filename
			BOOL useOfflineID = g_sCloudMissionLoaderMP.cmlUseOfflineID
			IF (useOfflineID)
				theFilename = g_sCloudMissionLoaderMP.cmlOfflineID
			ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [CloudLoader]: Load Cloud Mission Data (File: ")
				PRINTSTRING(theFilename)
				PRINTSTRING(") (")
				PRINTSTRING(GET_MP_MISSION_NAME(g_sCloudMissionLoaderMP.cmlMissionIdData.idMission))
				PRINTSTRING(")")
			#ENDIF
			
			// Should the previous UGC Data be cleared?
			IF (g_sCloudMissionLoaderMP.cmlClearDataPreload)
				// Clear the data
				RESET_UGC_LOAD_VARS(sGetUGC_content)
				CLEAR_BIT(GlobalplayerBD_FM[myPlayerGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataLoaded)
				CLEAR_BIT(GlobalplayerBD_FM[myPlayerGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataCleared)
				CLEAR_BIT(GlobalplayerBD_FM[myPlayerGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_ReadyToVote)
				CLEAR_TRANSITION_SESSION_MISSION_LOADED_BEFORE_LAUNCH()
				CLEAR_TRANSITION_SESSIONS_MISSION_HAS_LOADED()
				#IF IS_DEBUG_BUILD
					PRINTSTRING(" - Resetting UGC Load Vars for fresh mission data download")
				#ENDIF
				
				g_sCloudMissionLoaderMP.cmlClearDataPreload = FALSE
			ENDIF
		
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		
				PRINTLN("[TS] .KGM [CloudLoader]: NETWORK_IS_GAME_IN_PROGRESS() = FALSE. Clear cloud loaded mission data so we don't continue loading outside of a Network game.")
				Clear_Cloud_Loaded_Mission_Data()
		
			// For specific offline content, the Filename will contain the offline COntentID and the optional forceOfflineID flag will get set
			ELIF NOT (FMMC_LOAD_MISSION_DATA(	sGetUGC_content, theFilename, GlobalplayerBD_FM[myPlayerGBD].currentMissionData.iBitSet, theMissionType, FALSE, FALSE, TRUE, DEFAULT, DEFAULT, useOfflineID, DEFAULT, DEFAULT, DEFAULT, TRUE))	
				// ...data not loaded
				#IF IS_DEBUG_BUILD
					PRINTSTRING(" - Still Loading")
					PRINTNL()
				#ENDIF
				
				g_sCloudMissionLoaderMP.cmlDataLoadFinished	= FALSE
				g_sCloudMissionLoaderMP.cmlReportingFrames	= -1
				
				// KGM 21/4/15 [BUG 2291852]: Check if the download should be cancelled
				IF (cancelMissionDataDownload)
					#IF IS_DEBUG_BUILD
						PRINTSTRING(".KGM [CloudLoader]: ...BUT DOWNLOAD CANCELLED. Cleaning up.")
						PRINTNL()
					#ENDIF
					
					RESET_UGC_LOAD_VARS(sGetUGC_content)
					UGC_CANCEL_QUERY()
					CLEAR_GETTING_CONTENT_BY_ID()
					
					g_sCloudMissionLoaderMP.cmlDataLoadFinished		= TRUE
					g_sCloudMissionLoaderMP.cmlDataLoadSuccessful	= FALSE

					// Maintain the number of frames that this reporting state will be retained before the variables are auto cleared
					// NOTE: This part is probably not needed here.
					IF (g_sCloudMissionLoaderMP.cmlReportingFrames < 0)
						g_sCloudMissionLoaderMP.cmlReportingFrames = 0
					ELSE
						g_sCloudMissionLoaderMP.cmlReportingFrames++
						
						IF (g_sCloudMissionLoaderMP.cmlReportingFrames >= CLOUD_DOWNLOAD_MAX_REPORTING_FRAMES)
							// ...time to stop reporting failure
							PRINTSTRING(".KGM [CloudLoader]: LAST SPAM after download cancelled - Clearing variables.")
							PRINTNL()
							Clear_Cloud_Loaded_Mission_Data()
							g_sCloudMissionLoaderMP.cmlClearDataPreload = TRUE
						ENDIF
					ENDIF
					// END NOTE: Probably not needed
				ENDIF
			ELSE
				// Check if the data loaded successfully
				IF (sGetUGC_content.bSuccess)
					// ...successful, so load finished
					g_sCloudMissionLoaderMP.cmlDataLoadFinished		= TRUE
					g_sCloudMissionLoaderMP.cmlDataLoadSuccessful	= TRUE
					
					// ...data loaded
					// NOTE: This may spam if the calling function no longer cares about the mission being downloaded
					#IF IS_DEBUG_BUILD
						PRINTSTRING(" - DATA LOADED SUCCESSFULLY")
					#ENDIF
					
					// Maintain the number of frames that this reporting state will be retained before the variables are auto cleared
					IF (g_sCloudMissionLoaderMP.cmlReportingFrames < 0)
						g_sCloudMissionLoaderMP.cmlReportingFrames = 0
					ELSE
						g_sCloudMissionLoaderMP.cmlReportingFrames++
						
						IF (g_sCloudMissionLoaderMP.cmlReportingFrames >= CLOUD_DOWNLOAD_MAX_REPORTING_FRAMES)
							// ...time to stop reporting success
							PRINTSTRING(" [LAST SPAM - CLEARING VARIABLES]")
							Clear_Cloud_Loaded_Mission_Data()
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
						PRINTNL()
					#ENDIF
				ELSE
					// ...not successful
					// Try Again?
					g_sCloudMissionLoaderMP.cmlNumLoadAttempts++
					
					IF (g_sCloudMissionLoaderMP.cmlNumLoadAttempts >= MAX_CLOUD_MISSION_DOWNLOAD_ATTEMPTS)
						// ...all automatic retry load attemps have been done without success - report failure
						g_sCloudMissionLoaderMP.cmlDataLoadFinished		= TRUE
						g_sCloudMissionLoaderMP.cmlDataLoadSuccessful	= FALSE
						
						#IF IS_DEBUG_BUILD
							PRINTSTRING(" - DATA FAILED TO LOAD AFTER ATTEMPT ")
							PRINTINT(g_sCloudMissionLoaderMP.cmlNumLoadAttempts)
							PRINTSTRING(" GIVING UP. REPORTING FAILURE")
							
							// Output an assert for informational purposes only. Don't really know if we can do anything.
							SCRIPT_ASSERT("NET_CLOUD_MISSION_LOADER failed to load mission after a number of attempts. See console log. Tell Keith.")
						#ENDIF
					
						// Maintain the number of frames that this reporting state will be retained before the variables are auto cleared
						IF (g_sCloudMissionLoaderMP.cmlReportingFrames < 0)
							g_sCloudMissionLoaderMP.cmlReportingFrames = 0
						ELSE
							g_sCloudMissionLoaderMP.cmlReportingFrames++
							
							IF (g_sCloudMissionLoaderMP.cmlReportingFrames >= CLOUD_DOWNLOAD_MAX_REPORTING_FRAMES)
								// ...time to stop reporting failure
								PRINTSTRING(" [LAST SPAM - CLEARING VARIABLES]")
								Clear_Cloud_Loaded_Mission_Data()
								g_sCloudMissionLoaderMP.cmlClearDataPreload = TRUE
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
							PRINTNL()
						#ENDIF
					ELSE
						// ...try again
						#IF IS_DEBUG_BUILD
							PRINTSTRING(" - DATA FAILED TO LOAD AFTER ATTEMPT ")
							PRINTINT(g_sCloudMissionLoaderMP.cmlNumLoadAttempts)
							PRINTSTRING(". CLEARING VARIABLES. TRYING AGAIN.")
							PRINTNL()
						#ENDIF
						
						g_sCloudMissionLoaderMP.cmlClearDataPreload = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// Is header data required to be loaded?
		IF (Is_Cloud_Loaded_Header_Data_Required_To_Be_Loaded())
			// KGM 15/1/15 [BUG 2197556]: Set to in-use this frame to prevent this script being killed to save memory in LastGen when on a Heist
			m_inUseThisFrame = TRUE
		
			// ...yes, so setup (or continue) the load
			TEXT_LABEL_31	theContentID	= g_sCloudMissionLoaderMP.cmlMissionIdData.idCloudFilename
			
			// If already finished then the data wasn't collected - it should have been collected on the previous frame
			IF (g_sCloudMissionLoaderMP.cmlDataLoadFinished)
				PRINTSTRING("...KGM MP [CloudLoader]: Load Cloud Header Data [HEADER DATA WASN'T COLLECTED - CLEARING VARIABLES]")
				PRINTNL()
				Clear_Cloud_Loaded_Mission_Data()
			ELSE
				// Should the previous UGC Data be cleared?
				IF (g_sCloudMissionLoaderMP.cmlClearDataPreload)
					// Clear the data
					RESET_UGC_LOAD_VARS(sGetUGC_content)
					#IF IS_DEBUG_BUILD
						PRINTSTRING("...KGM MP [CloudLoader]: Load Cloud Header Data - Resetting UGC Load Vars for fresh mission header download")
						PRINTNL()
					#ENDIF
					
					g_sCloudMissionLoaderMP.cmlClearDataPreload = FALSE
				ENDIF
				
				#IF IS_DEBUG_BUILD
					INT progressBefore = sGetUGC_content.iProgress
				#ENDIF
				INT iRootId = 0
				IF NOT (GET_UGC_HEADER_BY_CONTENT_ID(sGetUGC_content, theContentID, theReturnedHeaderDetails, iRootId))
					// ...data not loaded
					#IF IS_DEBUG_BUILD
						PRINTSTRING("...KGM MP [CloudLoader]: Load Cloud Header Data (File: ")
						PRINTSTRING(theContentID)
						PRINTSTRING(")")
						PRINTSTRING(" - Still Loading [pre-call sGetUGC_content.iProgress = ")
						PRINTINT(progressBefore)
						SWITCH (progressBefore)
							CASE ciUGC_PROGRESS_SET_UP
								PRINTSTRING(" - ciUGC_PROGRESS_SET_UP")
								BREAK
							CASE ciUGC_PROGRESS_START
								PRINTSTRING(" - ciUGC_PROGRESS_START")
								BREAK
							CASE ciUGC_PROGRESS_WAIT_FOR_SELECT
								PRINTSTRING(" - ciUGC_PROGRESS_WAIT_FOR_SELECT")
								BREAK
							DEFAULT
								PRINTSTRING(" - [OTHER - look it up]")
								PRINTINT(sGetUGC_content.iProgress)
								BREAK
						ENDSWITCH
						PRINTSTRING("]")
						IF (progressBefore != sGetUGC_content.iProgress)
							PRINTSTRING(" - PROGRESS CHANGED TO: ")
							PRINTINT(sGetUGC_content.iProgress)
						ENDIF
						PRINTNL()
					#ENDIF
					
					g_sCloudMissionLoaderMP.cmlDataLoadFinished	= FALSE
					g_sCloudMissionLoaderMP.cmlReportingFrames	= -1
				ELSE
					// Check if the data loaded successfully
					IF (sGetUGC_content.bSuccess)
						// ...successful, so load finished
						g_sCloudMissionLoaderMP.cmlDataLoadFinished		= TRUE
						g_sCloudMissionLoaderMP.cmlDataLoadSuccessful	= TRUE
						
						// ...data loaded
						// NOTE: This may spam if the calling function no longer cares about the mission being downloaded
						#IF IS_DEBUG_BUILD
							PRINTSTRING(" - HEADER LOADED SUCCESSFULLY")
							PRINTNL()
						#ENDIF
						
						// Return values
						g_sCloudMissionLoaderMP.cmlReturnType			= theReturnedHeaderDetails.iType
						g_sCloudMissionLoaderMP.cmlReturnSubtype		= theReturnedHeaderDetails.iSubType
						g_sCloudMissionLoaderMP.cmlReturnMissionName	= theReturnedHeaderDetails.tlMissionName
						g_sCloudMissionLoaderMP.cmlReturnDescription	= theReturnedHeaderDetails.tlMissionDec
						g_sTransitionSessionData.iGangOpsMissingHeaderDataRootId = iRootId
						#IF FEATURE_CASINO_HEIST
						g_sTransitionSessionData.iCasinoHeistMissingHeaderDataRootId = iRootId
						#ENDIF
						#IF FEATURE_HEIST_ISLAND
						g_sTransitionSessionData.iHeistIslandMissingHeaderDataRootId = iRootId
						#ENDIF
						g_sTransitionSessionData.iTunerRobberyMissingHeaderDataRootId = iRootId
						#IF IS_DEBUG_BUILD
							PRINTSTRING("      Specific Return Values:")
							PRINTSTRING(" [Type: ")			PRINTINT(g_sCloudMissionLoaderMP.cmlReturnType)				PRINTSTRING("]")
							PRINTSTRING(" [Subtype: ")		PRINTINT(g_sCloudMissionLoaderMP.cmlReturnSubtype)			PRINTSTRING("]")
							PRINTSTRING(" [Mission Name: ")	PRINTSTRING(g_sCloudMissionLoaderMP.cmlReturnMissionName)	PRINTSTRING("]")
							PRINTSTRING(" [Description: ")	PRINTINT(g_sCloudMissionLoaderMP.cmlReturnType)				PRINTSTRING("]")
							PRINTSTRING(" [RootId: ")	PRINTINT(iRootId)											PRINTSTRING("]")
							PRINTNL()
							PRINTLN("g_sTransitionSessionData.iGangOpsMissingHeaderDataRootId = ", g_sTransitionSessionData.iGangOpsMissingHeaderDataRootId)
							#IF FEATURE_CASINO_HEIST
							PRINTLN("g_sTransitionSessionData.iCasinoHeistMissingHeaderDataRootId = ", g_sTransitionSessionData.iCasinoHeistMissingHeaderDataRootId)
							#ENDIF
							#IF FEATURE_CASINO_HEIST
							PRINTLN("g_sTransitionSessionData.iHeistIslandMissingHeaderDataRootId = ", g_sTransitionSessionData.iHeistIslandMissingHeaderDataRootId)
							#ENDIF
							PRINTLN("g_sTransitionSessionData.iTunerRobberyMissingHeaderDataRootId = ", g_sTransitionSessionData.iTunerRobberyMissingHeaderDataRootId)
						#ENDIF
						
						// Copy the whole return struct into a temporary global struct
						g_cloudLoadedMissionHeader = theReturnedHeaderDetails
						
						#IF IS_DEBUG_BUILD
							PRINTSTRING("...KGM MP [CloudLoader]: Copy the Returned Header Values into the global copy struct:")	PRINTNL()
							PRINTSTRING("         ContentID   : ")		PRINTSTRING(g_cloudLoadedMissionHeader.tlName)				PRINTNL()
							PRINTSTRING("         Creator Name: ")		PRINTSTRING(g_cloudLoadedMissionHeader.tlOwner)				PRINTNL()
							PRINTSTRING("         Mission Name: ")		PRINTSTRING(g_cloudLoadedMissionHeader.tlMissionName)		PRINTNL()
							PRINTSTRING("         Desc Hash   : ")		PRINTINT(g_cloudLoadedMissionHeader.iMissionDecHash)		PRINTNL()
							PRINTSTRING("         Start Pos   : ")		PRINTVECTOR(g_cloudLoadedMissionHeader.vStartPos)			PRINTNL()
							PRINTSTRING("         Type        : ")		PRINTINT(g_cloudLoadedMissionHeader.iType)					PRINTNL()
							PRINTSTRING("         Subtype     : ")		PRINTINT(g_cloudLoadedMissionHeader.iSubType)				PRINTNL()
							PRINTSTRING("         Min Players : ")		PRINTINT(g_cloudLoadedMissionHeader.iMinPlayers)			PRINTNL()
							PRINTSTRING("         Rank        : ")		PRINTINT(g_cloudLoadedMissionHeader.iRank)					PRINTNL()
							PRINTSTRING("         Max Players : ")		PRINTINT(g_cloudLoadedMissionHeader.iMaxPlayers)			PRINTNL()
							PRINTSTRING("         Max Teams   : ")		PRINTINT(g_cloudLoadedMissionHeader.iMaxNumberOfTeams)		PRINTNL()
							PRINTSTRING("         Contact Char: ")		PRINTINT(g_cloudLoadedMissionHeader.iContactCharEnum)		PRINTNL()
						#ENDIF
					ELSE
						// ...not successful
						g_sCloudMissionLoaderMP.cmlDataLoadFinished		= TRUE
						g_sCloudMissionLoaderMP.cmlDataLoadSuccessful	= FALSE
							
						#IF IS_DEBUG_BUILD
							PRINTSTRING(" - HEADER FAILED TO LOAD")
							PRINTNL()
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// Is Heist Planning Header data required to be loaded?
		IF (Is_Cloud_Loaded_Heist_Planning_Header_Data_Required_To_Be_Loaded())
			// KGM 15/1/15 [BUG 2197556]: Set to in-use this frame to prevent this script being killed to save memory in LastGen when on a Heist
			m_inUseThisFrame = TRUE
		
			// ...yes, so setup (or continue) the load
			// If already finished then the data wasn't collected - it should have been collected on the previous frame
			IF (g_sCloudMissionLoaderMP.cmlDataLoadFinished)
				PRINTSTRING(".KGM [Heist][CloudLoader]: Load Cloud Heist Planning Header Data [DATA WASN'T COLLECTED - CLEARING VARIABLES]")
				PRINTNL()
				Clear_Cloud_Loaded_Mission_Data()
			ELSE
				// Should the previous UGC Data be cleared?
				IF (g_sCloudMissionLoaderMP.cmlClearDataPreload)
					// Clear the data
					RESET_UGC_LOAD_VARS(sGetUGC_content)
					#IF IS_DEBUG_BUILD
						PRINTSTRING(".KGM [Heist][CloudLoader]: Load Cloud Heist Planning Header Data - Resetting UGC Load Vars for fresh mission header download")
						PRINTNL()
					#ENDIF
					
					g_sCloudMissionLoaderMP.cmlClearDataPreload = FALSE
				ENDIF
				
				// KGM 19/11/14 [BUG 2137672]: Safety timeout needs access to progress value
				INT progressBefore = sGetUGC_content.iProgress
			
				IF NOT (DOWNLOAD_HEIST_PLANNING_MISSIONS(sGetUGC_content))
					// ...indicate a Prep Mission download is in progress
					g_prepMissionDownloadInProgress = TRUE
					
					// ...data not loaded
					#IF IS_DEBUG_BUILD
						PRINTSTRING(".KGM [Heist][CloudLoader]: Load Cloud Heist Planning Header Data - Still Loading [pre-call sGetUGC_content.iProgress = ")
						PRINTINT(progressBefore)
						SWITCH (progressBefore)
							CASE ciUGC_PROGRESS_SET_UP
								PRINTSTRING(" - ciUGC_PROGRESS_SET_UP")
								BREAK
							CASE ciUGC_PROGRESS_START
								PRINTSTRING(" - ciUGC_PROGRESS_START")
								BREAK
							CASE ciUGC_PROGRESS_WAIT_FOR_LIST
								PRINTSTRING(" - ciUGC_PROGRESS_WAIT_FOR_LIST")
								BREAK
							CASE ciUGC_PROGRESS_READ_FILE
								PRINTSTRING(" - ciUGC_PROGRESS_READ_FILE")
								BREAK
							DEFAULT
								PRINTSTRING(" - [OTHER - look it up]")
								PRINTINT(sGetUGC_content.iProgress)
								BREAK
						ENDSWITCH
						PRINTSTRING("]")
						PRINTNL()
					#ENDIF
					
					IF (progressBefore != sGetUGC_content.iProgress)
						#IF IS_DEBUG_BUILD
							PRINTSTRING(".KGM [Heist][CloudLoader]: - PROGRESS CHANGED TO: ")
							PRINTINT(sGetUGC_content.iProgress)
							PRINTNL()
						#ENDIF
						
						// KGM 19/11/14 [BUG 2137672]: Safety timeout - if progress has just become ciUGC_PROGRESS_READ_FILE then start safety timeout
						IF (sGetUGC_content.iProgress = ciUGC_PROGRESS_READ_FILE)
							m_cloudDownloadSafetyTimeout = GET_GAME_TIMER() + KGM_CLOUD_DOWNLOAD_SAFETY_TIMEOUT_msec
							
							#IF IS_DEBUG_BUILD
								PRINTSTRING(".KGM [Heist][CloudLoader]: Load Cloud Heist Planning Header Data - Start Read File Safety Timeout of ")
								PRINTINT(KGM_CLOUD_DOWNLOAD_SAFETY_TIMEOUT_msec)
								PRINTSTRING(" msec")
								PRINTNL()
							#ENDIF
						ENDIF
					ENDIF
					
					// KGM 19/11/14 [BUG 2137672]: Check for safety timeout
					BOOL reachedSafetyTimeout = FALSE
					
					IF (sGetUGC_content.iProgress = ciUGC_PROGRESS_READ_FILE)
						IF (GET_GAME_TIMER() > m_cloudDownloadSafetyTimeout)
							#IF IS_DEBUG_BUILD
								PRINTSTRING(".KGM [Heist][CloudLoader]: Load Cloud Heist Planning Header Data - Read File Safety Timeout has expired. Quitting download attempt.")
							#ENDIF
							
							g_sCloudMissionLoaderMP.cmlDataLoadFinished		= TRUE
							g_sCloudMissionLoaderMP.cmlDataLoadSuccessful	= FALSE
							reachedSafetyTimeout							= TRUE
						ENDIF
					ENDIF
					
					IF NOT (reachedSafetyTimeout)
						g_sCloudMissionLoaderMP.cmlDataLoadFinished	= FALSE
						g_sCloudMissionLoaderMP.cmlReportingFrames	= -1
					ENDIF
				ELSE
					// Check if the data loaded successfully
					IF (sGetUGC_content.bSuccess)
						// ...successful, so load finished
						g_sCloudMissionLoaderMP.cmlDataLoadFinished		= TRUE
						g_sCloudMissionLoaderMP.cmlDataLoadSuccessful	= TRUE
						
						// ...data loaded
						// NOTE: This may spam if the calling function no longer cares about the mission being downloaded
						#IF IS_DEBUG_BUILD
							PRINTSTRING(" - HEIST PLANNING HEADERS LOADED SUCCESSFULLY")
							PRINTNL()
						#ENDIF
					ELSE
						// ...not successful
						g_sCloudMissionLoaderMP.cmlDataLoadFinished		= TRUE
						g_sCloudMissionLoaderMP.cmlDataLoadSuccessful	= FALSE
							
						#IF IS_DEBUG_BUILD
							PRINTSTRING(" - HEIST PLANNING HEADERS FAILED TO LOAD")
							PRINTNL()
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			// FEATURE_HEIST_PLANNING

		// KGM 15/1/15 [BUG 2197556]: Can this script be killed to save memory in LastGen when on a Heist?
		IF NOT (m_inUseThisFrame)
			IF (Is_Player_Currently_On_MP_Heist(PLAYER_ID()))
			OR (Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID()))
				// ...not in use and on a Heist, so terminate if not in an apartment
				// NOTE: This may be used in an apartment to load the planning board details
				IF NOT (IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE))
					PRINTLN(".KGM [CloudLoader]: net_cloud_mission_loader.sc TERMINATING (On Heist, Not in Apartment, Loader not in use)")
					
					// Don't leave this F9 screen flag set if we're terminating, it's confusing
					#IF IS_DEBUG_BUILD
						g_DEBUG_F9_Prep_Download_Active = FALSE
					#ENDIF
			
					TERMINATE_THIS_THREAD()
				ENDIF
			ENDIF
		ENDIF
	
	ENDWHILE

ENDSCRIPT






