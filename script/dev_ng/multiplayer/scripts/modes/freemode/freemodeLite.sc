// ________________________________________
//
//	Game: GTAV - freemode.sc
//  Script: freemode.sc	
//  Author: Robert Wright 
//	Description: Freemode mode
// ___________________________________________   

USING "globals.sch"

#IF FEATURE_FREEMODE_CLASSIC


USING "net_reset_MP_Globals.sch"
USING "freemode_header.sch"
USING "net_bot_utils.sch"
USING "net_bots.sch"
USING "net_stat_generator.sch"
USING "net_main_client.sch"
USING "net_main_server.sch" 
USING "freemode_stunt_jumps.sch"
USING "net_vote_at_blip.sch"
USING "net_spectator_hud.sch"
USING "shop_private.sch"
USING "stripclub_public.sch"
USING "net_freemode_cut.sch"
USING "net_friend_request.sch"
USING "net_race_to_point.sch"
USING "net_script_tunables.sch"
USING "screen_gang_on_mission.sch"
USING "commands_brains.sch"
USING "net_betting_client.sch"
USING "net_betting_server.sch"
USING "net_crate_drop.sch"
USING "FM_car_generators.sch"
USING "FM_weapon_pickups.sch"
USING "Net_Entity_Icons.sch"
USING "net_additional_XP.sch"
USING "net_corona_position.sch"
USING "FM_Trigger_Tut.sch"
USING "net_heli_gun_launching.sch"
USING "fm_post_mission_cleanup.sch"
USING "leader_board_common.sch"
//USING "net_noTutorial_cut.sch"
USING "carmod_shop_private.sch"
USING "net_gang_angry.sch"
USING "commands_socialclub.sch"
USING "DM_Leaderboard.sch"
USING "net_spectator_cam.sch"
USING "net_interactions_synced.sch"
USING "net_cash_transactions.sch"
USING "net_vehicle_help_text.sch"

//for swapping objects
USING "NET_Swap_Objects.sch"

USING "fm_unlocks_header.sch"
USING "fm_hold_up_tut.sch"
#IF IS_DEBUG_BUILD
USING "net_debug.sch"
USING "profiler.sch"
USING "net_masks.sch"
#ENDIF

USING "net_doors.sch"

USING "net_bounty.sch"
USING "socialclub_leaderboard.sch"
USING "net_ammo_drop.sch"
USING "net_vehicle_drop.sch"
USING "net_boat_taxi.sch"
USING "net_heli_taxi.sch"
USING "net_realty_new.sch"
USING "net_joyrider.sch"
USING "net_contact_requests.sch"

USINg "net_backup_heli_launching.sch"
USING "net_bru_box.sch"
USING "net_hideout.sch"
USING "net_celebration_screen.sch"
USING "net_airstrike_launching.sch"
USING "net_impromptu_DM.sch"
USING "Net_gameDir.sch"
USING "net_crew_updates.sch"
USING "transition_crews.sch"
USING "ply_to_ply_calls.sch"
USING "net_power_ups.sch"
USING "emergency_call.sch"
USING "net_transition_sessions.sch"
USING "building_control_public.sch"
USING "net_kill_cam.sch"
USING "net_wait_zero.sch"
USING "net_snowball.sch"
USING "net_daily_objectives.sch"
USING "net_pv_rate_limiter.sch"
USING "net_cash_transactions.sch"
USING "net_text_chat_manager.sch"
USING "commands_camera.sch"
USING "net_pv_rate_limiter.sch"
USING "net_heartbeat.sch"
USING "net_passive_mode.sch"
USING "net_simple_cutscene_interior.sch"

USING "net_time_trials.sch"

USING "net_weapon_pickups.sch"
#IF IS_DEBUG_BUILD
#IF FEATURE_WANTED_SYSTEM
	USING "net_wanted_system.sch"
#ENDIF
#ENDIF


USING "net_bg_script_kick.sch"

USING "net_gang_boss_common.sch"
USING "net_gang_boss.sch"

USING "gb_vehicle_export_freemode_header.sch"

USING "net_grief_passive.sch"

USING "net_realty_warehouse.sch"
USING "net_simple_interior.sch"

USING "net_spawn_group.sch"
USING "net_interior_access_control_public.sch"
USING "net_clubhouse.sch"	

USING "net_realty_vehicle_garage.sch"
USING "net_management.sch"

USING "net_activity_creator_activities.sch"

USING "net_plate_dupes.sch"

USING "net_freemode_delivery_public.sch"

USING "net_vehicle_generator_debug.sch"
USING "net_realty_hangar.sch"
USING "net_wanted_vehicles.sch"
USING "net_veh_stealth_mode.sch"

USING "net_realty_business_hub.sch"
USING "net_realty_nightclub.sch"
USING "club_management_fm.sch"
USING "net_dancing.sch"
USING "net_dancing_player.sch"
USING "net_drone.sch"
USING "net_melee_rampage.sch"
using "net_rc_vehicle.sch"
USING "net_collectables_pickups.sch"

#IF IS_DEBUG_BUILD
BOOL bTestRougeWait
#ENDIF

STRUCT_MAGNATE_GANG_BOSS_LOCAL_DATA sMagnateGangBossLocalData

// Plane smoke variables
INT iAirFlareBS
PTFX_ID ptfxID
CONST_INT AIR_FLARE_BS_FIRED								0
CONST_INT AIR_COUNTERMEASURE_BS_MODEL_REQUESTED				1
CONST_INT AIR_COUNTERMEASURE_BS_TRIGGERED					2
CONST_INT AIR_COUNTERMEASURE_BS_CHAFF_PTFX_REQUESTED		3
CONST_INT AIR_COUNTERMEASURE_BS_FLARE_ONE_FIRED				4
CONST_INT AIR_COUNTERMEASURE_BS_FLARE_TWO_FIRED				5
CONST_INT AIR_COUNTERMEASURE_BS_FLARE_THREE_FIRED			6
CONST_INT AIR_COUNTERMEASURE_BS_FLARE_FOUR_FIRED			7
CONST_INT AIR_COUNTERMEASURE_BS_CHAFF_LOCKON_STOPPED		8
CONST_INT AIR_FLARE_BS_HELP_TEXT_SHOWN						9

VEHICLE_INDEX vehUsedForCountermeasure


// -----------------------------------------------------------------------

SCRIPT_TIMER iInitInteriorInstanceIndexTimer

INT StatTimerCounter
RANKUPBARSTRUCT AllRankupStuff
LBD_VARS lbdVars
DPAD_VARS freeDpadVars
FM_DPAD_LBD_STRUCT fmDpadVars

DPADDOWN_ACTIVATION CashDisplayChecker = DPADDOWN_SECOND

BOOL bInitAudioScene

BOOL bRequestedGoodBoyEarn
INT iGoodBoyEarnTransactionIndex
BOOL bProcessGoodBoyEarn

#IF IS_DEBUG_BUILD
BOOL bRunGoodBoyEarn
BOOL bCREATE_FM_WIDGETS
BOOL bCREATE_FM_WIDGETS_CACHED
#ENDIF

INT iBackupHeliPartCount
BOOL bDoHeliRelGroupUpdate
SCRIPT_TIMER timerUpdateGlobalHeliRelGroup
INt iFriendlyBitsetLocalCopy

INT HeadshotDeletionStages = 0

INT iRequestAudioFreemodeBitset
CONST_INT AUDIO_FREEMODE_LOADED_321_GO	0

STRUCT_BACKUP_HELI_LAUNCHING_DATA 	sBackUpHeliLaunchingData
STRUCT_AIRSTRIKE_LAUNCHING_DATA 	sAirstrikeLaunchingData
GB_BOSS_TEXT_MESSAGE_VARS sBossTextVars
FM_TRIGGER_INTRO_STRUCT fmTriggerIntro

LOOK_AT_FRIENDS_STRUCT lookAtFriend

#IF IS_DEBUG_BUILD
BOOL bPrintCurrentVehicleSetup
BOOL bPrintSavedVehicleStucts
MASK_ANIM_DATA maskAnimDataFM
DEBUG_ADD_VEH_GARAGE sDebugAddVeh
DEBUG_ADD_VEH_GARAGE sDebugAddBikeToClubhouse
BOOL bd_bForceApartmentCleaning
DEBUG_EXTERIOR_VEHICLE_SPAWN sDebugExtVehSpawn
BOOL db_bSetOwnsOffice
BOOL db_bRemovePlayerOffice
#ENDIF

STRUCT_POWER_UP_DATA 	PowerUpData

CONST_INT GOOD_BOY_REMINDER_FREQUENCY (60000*60)*2 // 2 hours.

CONST_INT NO_SAVES_REMINDER_FREQUENCY (60000*60) // 1 hours.

// For spotted in stolen vehicles. 
SCRIPT_TIMER stolenVehicleWantedTimer
INT iStolenVehicleNoWantedTime, iStolenVehicleWantedStage
SCRIPT_TIMER setVehWantedTimer
SCRIPT_TIMER globalUpdateTimer

BOOL bSetUpPlayer
BOOL unlockedBountyContact

BOOL bEnableMinigameCheck = TRUE

BOOL bHasClearedArea

BOOL bDisplayOfflineTicker
SCRIPT_TIMER bDisplayOfflineTimer

INT iPlayerKickCheckIndexInternal
INT iPlayerKickCheckIndexExternal

INT iRemoteVehicleStagger

CONTACT_REQUEST_LAUNCHING_STRUCT freemodeContactRequests

SCRIPT_TIMER scoreDataStartTimer
SCRIPT_TIMER AimPrefFeedMessageTimer

//For the swapping of map objects
INT iServerIPLbitSet[MAX_NUMBER_OF_MP_SWAP_OBJECT_BITSETS]

HOLD_UP_STRUCT HoldUpData


BG_SCRIPT_KICK_DETAILS BGScriptKickDetails

INT iGB_stagger

INT iFMWeaponPickups_ClientStagger

SIMPLE_INTERIOR_LOCAL_DATA SimpleInteriorLocalData


ENUM VEHICLE_EMP_POSTFX_STATE
	EMP_POSTFX_STATE_IDLE,
	EMP_POSTFX_STATE_RUNNING
ENDENUM
VEHICLE_EMP_POSTFX_STATE eEMPPostFXState

#IF IS_DEBUG_BUILD
STRUCT_TUG_BOAT_PED_PLACEMENT_WIDGETS_DATA sTugBoatPedPlacementWidgetData
#ENDIF

FREEMODE_DELIVERY_FREEMODE_DATA freemodeDeliveryData

#IF IS_DEBUG_BUILD
DEBUG_VEHICLE_GENERATOR_DATA g_dFreemodeVehGenData
CONTRABAND_TRANSACTION_STATE eTransactionState = CONTRABAND_TRANSACTION_STATE_DEFAULT
#ENDIF

SCENARIO_BLOCKING_INDEX scenOutsideApartment

INT iParkingParticipant  //used for parking 

INT iKickingClientNumberCheck

AMTT_VARS_STRUCT sTTVarsStruct
INT iAmbientEventLBDHelpCounter
SCRIPT_TIMER timerAmbientEventLDBHelpDelay


#IF FEATURE_STUNT_FM_EVENTS
AMTT_VARS_STRUCT sTTVarsStructStunt
#ENDIF

SCRIPT_TIMER IdleTrackingResetTimer
BOOL bMaintainBadSportHat


SCRIPT_TIMER PlayingAloneTimer


INT PlayerCheckCRCStages
SCRIPT_TIMER PlayerCheckCRCTimer
BOOL PlayerCheckCRCStagesDone[NUMBER_CRC_REPORT_CHECKS]
INT PlayerCheckCRCLastParticipantCount

BOOL bTurnedOnArmRadioEmitters

//For the rockstar feemessages
TIME_DATATYPE RemoteSavedVehicleTimer
TIME_DATATYPE SavedVehicleStaggerTimer[NUM_NETWORK_PLAYERS]

SCRIPT_TIMER st_QuickFixFeedTimer


BOOL bSomeoneWantsInvincbleShopkeeper
#IF IS_DEBUG_BUILD
BOOL bCheatLevelAmIaCheatLesWidget
INT iCheatLevelThreshold = g_sMPTunables.cheatThreshold
INT iNotCheatLevelThreshold = g_sMPTunables.NotcheatThreshold
FLOAT fCheatPlayerLevel 
INT iCheatTimeBeenACheaterWidget
BOOL iWasIaCheaterWidget
BOOL bCheatOverrideCheatLevels
BOOL bCheatOverrideCheatLevels_LastFrame
INT  bCheatOverrideCheatNumber

// for testing BROADCAST_LOCK_ANY_DUPE_AMBIENT_VEHICLES
INT iAmbDupe_ModelName
INT iAmbDupe_PlateTextHash
BOOL bAmbDupe_GetCurrentVehicleDetails
INT iAmbDupe_RandomID
BOOL bAmbDupe_send

STRUCT_STAT_DATE sBadSportDateBecameBadSportLesWidget
STRUCT_STAT_DATE sBadSportDateToBeForgivenWidget
BOOL bBadSportLevelAmIaBadSportLesWidget
INT iBadSportLevelThreshold = g_sMPTunables.BadSportThreshold
INT iNotBadSportLevelThreshold = g_sMPTunables.NotBadSportThreshold
FLOAT fBadSportForgiveLevel = g_sMPTunables.BadSportAmountToForgiveBy
FLOAT fBadSportPlayerLevel 
BOOL bBadSportPlayerReset
BOOL bBadSportCharReset
BOOL iWasIaBadSportWidget
BOOL bInitBadSportMuteWidget
INT iBadSportMuteCount
INT iBadSportTalkerCount
INT iBadSportMuteCount_Widget
INT iBadSportTalkerCount_Widget

INT iBadSportTimeBeenABadSportWidget

INT AnArrayFMDead[2]

BOOL bStartCoronaCloneScript
#ENDIF

BOOL binitialise_crewslots

#IF IS_DEBUG_BUILD
INT iMyRank
INT iMyXp, iMyCash, imycrewxp, iMyCrewSlot
INT iCashToGive, iXpToGive, icrewxptogive	

BOOL bUnlockallFMActivites

BOOL resetLocalCDOnContactRequests
BOOL resetTargetCDOnContactRequests

BOOL bFerrisWheelActive

#IF IS_DEBUG_BUILD
BOOL bGbStealVehicleClassTaskActive
BOOL bGbStealP2pTaskActive
BOOL bGbHitActive
BOOL bGbDeathmatchActive
BOOL bGbBossAttackActive
#ENDIF

BOOL bLaunchPlaneTakedown
BOOL bLaunchDistractCops
BOOL bLaunchDestroyVeh
BOOL bLaunchHotTarget
BOOL bLaunchUrbanWarfare
BOOL bLaunchTimeTrial
BOOL bLaunchCPCollection
BOOL bLaunchChallenges
BOOL bLaunchPennedIn
BOOL bLaunchMTA
BOOL bResetFMEventOptIn

VECTOR vDebugSphereLocation
FLOAT fDebugSphereRadius

BOOL bAmLauncherActive = TRUE
BOOL bDailyObjectivesActive = TRUE
#ENDIF

SCRIPT_TIMER st_ChatRestrictionsMessageTimer
SCRIPT_TIMER st_UGCRestrictionsMessageTimer

INT Global_stat_Broadcast_update_stagger_count

BOOL HasRunBadSportCheckAtStart
BOOL HasRunCheatCheckAtStart
BOOL bStandardTransitionInToFreemode = FALSE


SCRIPT_TIMER timeLaunchImport

FM_CAR_GENERATORS_STRUCT FM_CAR_GENERATORS_CAR_GENS
BOOL FM_Car_gens_have_been_created

SCRIPT_TIMER st_QueueReminder
BOOL b_QueueFirstTimeBool
BOOL bAudioLoaded

INT iNumberOfVotesToKickPlayer

SCRIPT_TIMER stBeforeCoronaVehicleDelay

INT iGangCallTypeStagger
INT iGangCallLastParticipant
PEDS_STORED_FOR_LOOTING pedsstoredforlooting

BLIP_INDEX carwashBlips[2]

PROPERTY_BLOCKS_STRUCT propertyBlocks

SCRIPT_TIMER stRankCacheChecked

#IF IS_DEBUG_BUILD

BOOL b_CallDisableRecordingSphere
VECTOR vDisableRecordingSphere_Coords
FLOAT fDisableRecordingSphere_Radius
FLOAT fDisableRecordingSphere_MaxDist
INT iDisableRecordingSphere_Duration

#ENDIF


BOOL bDefendMissionTimersActive

BOOL bBunkerDefendMissionTimersActive

BOOL bRuinerPersonalCheckDisable = FALSE

INT iOffRadarPrintTime = 10000

STRUCT ACTIVITY_BLIPS_STRUCT
	BLIP_INDEX cinemaBlips[3]
	BLIP_INDEX fairgroundBlip[2]

ENDSTRUCT


NO_WANTED_LEVEL_GAIN_ZONES_DATA sNoWantedLevelGainZonesData

CONST_INT ciADVERSARY_REMINDER_BIT_OP -268435456			// 1111 0000 ... 0000
CONST_INT ciADVERSARY_REMINDER_BIT_OP2 268435455			// 0000 1111 ... 1111

// New enum and variables for Adversary Reminder System for Jan2016 release.
ENUM ADVERSARY_REMINDER_STATE
	ADVERSARY_REMINDER_STATE_FIND,
	ADVERSARY_REMINDER_STATE_VALIDATE,
	ADVERSARY_REMINDER_STATE_SEND
ENDENUM

CONST_INT MAX_NUMBER_OF_INVITE_FLOW_TUNEABLES		12

INT iAdversaryReminderBit
SCRIPT_TIMER stAdversaryReminder

g_eContactMissionAdversaryTypes eAdversaryReminder = CM_ADVERSARY_TYPE_NULL
ADVERSARY_REMINDER_STATE eAdversaryReminderState = ADVERSARY_REMINDER_STATE_FIND

TWEAK_INT cfRAPPEL_LOOP_START_TIME				1500

SNOWBALL_DATA SnowballData

ACTIVITY_BLIPS_STRUCT activityBlips

// JamesA: Corona Position counter used by server to clean up bitsets (this perhaps could be integrated to a global system?)
INT iCoronaPosToCheck

VECTOR vStripperClub = <<130.1238, -1300.2657, 28.2811>>
BOOL bAddedStripperClubToActList
 
//BLIP_INDEX BountyBlip[BB_NUM_DIF_BOARDS]
//BLIP_INDEX BBTargetedPlayerBlip

//this is needed to handle when ped is left behind with warp but no longer player
//PED_INDEX bountyPed

INT iMaintainHelpTextCounter = 0
SCRIPT_TIMER timeStorePlayerFMSpawnPosition
SCRIPT_TIMER timeStoreDecreaseMentalStatePlaying


INT iFNMH_COUNTER_MAX = 72


LEADERBOARD_PLACEMENT_TOOLS LeaderboardPlacement
FM_LEADERBOARD_STRUCT FMLeaderboard[NUM_NETWORK_PLAYERS]
TEXT_LABEL_23 tl23LeaderboardPlayerNames[NUM_NETWORK_PLAYERS]

INT iActivityPhoneCallCounter	


CONST_INT UPDATE_PLAYER_TRUE_COORDS_TIME 2000
CONST_INT UPDATE_PLAYER_TRUE_COORDS_DIST 1
INT iUpdatePlayerTrueCoordsTimer = 0

GRIEF_PASSIVE_DATA_STRUCT sGriefPassiveData

structUnlockPhoneCall unlockFmPhoneCall
structPedsForConversation speechGbPhoneCall
INT iFlowCallType = -1
SCRIPT_TIMER stPassFailPhonecallDelay
structReminderPhonecall reminderFmPhonecall
structActivityReminderTimes reminderTimes
GB_MAINTAIN_NOTIFICATIONS_VARS sGbNotificationVars
CRIMINAL_ENTERPRISE_STARTER_PACK_MAINTAIN_NOTIFICATIONS_VARS sCriminalEnterpriseStarterPackNotificationVars
#IF IS_DEBUG_BUILD
	PV_LIMITER_DEBUG PVLimiterData
#ENDIF

ILLEGAL_DLC_DATA IllegalDLCData

RESTRICTED_INTERIOR_ACCESS_VARS sRIA

VEHICLE_DROP_PERSONAL_DATA VDPersonalData
VEHICLE_DROP_PERSONAL_DATA VDPersonalTruckData
BLIP_INDEX VDPersonalTruckBlip, VDPersonalAvengerBlip, VDPersonalHackerTruckBlip

#IF IS_DEBUG_BUILD
VEHICLE_DROP_PERSONAL_DEBUG_DATA VDPersonalTruckDebugData
#ENDIF

//MP_REALTY_CONTROL_STRUCT mpRealtyControler

GANG_ANGRY_DATA gangAngryData

VEHICLE_STATS_FOR_CLASS_TEMP vehicleStatLoading

INT iFacialIdles = 0
INT iWalkingStyleStage = 0

//Player to Player Text Message Literal
INT iTMLReplyCounter = 0
TEXT_LABEL_7 tlTMLReplyID[NUM_NETWORK_PLAYERS]	//TMLR0..15
INT iPlayerReplyNum = -1
//Keyboard Variables
STRUCT KEYBOARD_DATA 
    OSK_STATUS eStatus
    INT iProfanityToken
    INT iCurrentStatus
ENDSTRUCT
KEYBOARD_DATA KeyBoardData
TEXT_LABEL_63 tlSendMessage

// BOOL to track loading & unloading of new clipset when wearinga trenchcoat. Added for Luxe2. CMcM - bug#2335963 
BOOL bIsTrenchcoatLocoSet


// Widgets & Debug.
#IF IS_DEBUG_BUILD
	//For prints
	TEXT_LABEL_31 tl31MissionName = " FreeMode"
	BOOL bSetPlayerOnOwnTeam
	//BOOL bDoSwapSetUpOwn
	BOOL bSetUpUsjs 
	BOOL bGetXUID
	WIDGET_GROUP_ID missionTab
	BOOL bHostEndGameNow
	BOOL bHostDoNotEndGame
	BOOL bHostAllLeave
	//BOOL bSwapSetUp
	BOOL bLoadStreamingScript
	//sequence_index script variable
	SEQUENCE_INDEX SequenceIndex
	TEXT_WIDGET_ID twID
	SCRIPT_TIMER iGaragePrintTimer
	
	BOOL bWdLaunchMpIntroCut = FALSE
	BOOL bWdHasLaunchedMpIntroCut = FALSE
	BOOL bWdIntroScriptStarted = FALSE
	
	BOOL bWdDeleteCharacter0Headshot = FALSE
	BOOL bWdResetDeleteCharacter0Headshot = FALSE
	BOOL bWdGiverewardammotoplayer = FALSE
	BOOL bWdGiverewardtshirtcrosstheline = FALSE
	BOOL bWdTestPhoneToEar
	
	BOOL bWdDoNmhDebug // Non-mission help debug
	BOOL bWdSeenInStDebug
	BOOL bWdDoLectDebug
	BOOL bWdClearReplayJobHelpStat
	BOOL bWdClearTrackerHelpStat
	BOOL bWdSetBikeAtPos
	BOOL bWdSetCutsceneCOmplete
	BOOL bWdShowLockOnState
	INT iWdLockOnState
	BOOL bWdLaunchImportExportNow
	BOOL bWdShowUnlocked 
	BOOL bWdShowReminderTimer
	BOOL bWdResetVehicleUnlockText
	BOOL bWdDoMissionPassedText
	BOOL bWdDebugReminderCalls
	//BOOL bwdFullBody
	INT iWdBrucieReminder
	INT iWdBountyReminder
	INT iWdCrateDropReminder
	INT iWdParaReminder
	INT iWdSecVanReminder
	INT iWdGangAttackReminder
	INT iWdImpExpReminder
	INT iWdInsuranceReminder
	INT iWdLastSurvivalReminder
	INT iWdVagosBackupReminder
	INT iWdBikerBackupReminder
	INT iWdLesterVehReminder
	INT iWdMerryweatherReminder
	INT iWdLesterHelpReminder
	INT iWdPersVehDelReminder
	INT iWdPegasusVehReminder
	INT iWdLesterReqJobReminder
	INT iWdMartinReqJobReminder
	INT iWdRonReqJobReminder
	INT iWdGeraldReqJobReminder
	INT iWdSimeonReqJobReminder
	BOOL db_bOutputAllApartmentDetails = FALSE
	INT db_iOutputPropertiesAll = -1
	
	BOOL db_bOutputThisPropertyDetails
	INT db_bOutputPropertyID = 1
	INT db_bOutputYachtID = 22
	
	BOOL bWdPrintAllUnlocked
	BOOL bWdPrintCutsceneCompleteStatus
	BOOL bWdPrintAllContactUnlocked
	BOOL bWdRacesUnlocked
	BOOL bwdDeathMatchUnlocked
	BOOL bWdDoStripclubDebug
	BOOL bWdDoMechanicPvehDelText
	BOOL bWdIgnoreRedisplayCheck
	BOOL bWdPrintNmh
	BOOL bWdPrintObjective
	BOOL bWdForcePrintNMH
	BOOL bWdClearNMHStats
	BOOL bWdRunSetFmUnlocks
	BOOL bWdGetCarColours
	BOOL bWdNextColourComb
	INT iWdColourComb
	BOOL bWdGetCarPos
	BOOL bWdRunAbmientTutorialCheck
	BOOL bWdUnlockUpToCurrentRank
	BOOL bWdLaunchNpcInvite
	BOOL bWdLaunchSmMission
	BOOL bWdSendTextMessage
	BOOL bWdCheckPlayerMale
	BOOL bWdIsMale
	BOOL bWdIsFemale
	BOOL bWdGiveGlasses
	BOOL bWdLaunchBossDm
	SCRIPT_TIMER timeNmhDebug
	INT iWdTextMessChar
	BOOL bTestWarpToInterior
	BOOL bTestWarpTakeAwayControl
	
	BOOL bWdResetTriggerTut
	BOOL bWdShowTutSession
	BOOL bWdSetSplashActive
	BOOL bWdRemoveSplash
	//INT iSplashTest
	BOOL bWdShowPhaseTime
	BOOL bWdShowPlayerAnimTime
	FLOAT fWdAnimTime
	FLOAT fWdPhaseTime
	INT iWdLocalSession
	
	BOOL bWdShowCUrrentMissionType
	INT iWdCurrentMissionType
	//INT iWdBdRaceSession
	BOOL bWdHoldUpTutClearStat
	BOOL bWdLaunchCarModTut
	BOOL bWdLaunchLesterCut
	BOOL bWdLaunchTrevorCut
	BOOL bWdlaunchLowriderInt

	
	BOOL bWdSetCHoseNoTut
	BOOL bWdSetIntroDOne
	BOOL bWdSetTriggerTutDone
	BOOL bWdShowLeaderboardPos
	INT iWdLeaderboardPos = -99
	BOOL bDebugDrawSafeCircleSpawn
	FLOAT fDebugDrawSafeCircleRadius = 1.9
	BOOL bHoldUpSetTimerBit
	
	BOOL bWdEnableAllInteriors = FALSE
	BOOL bWdAllInteriorsEnabled = FALSE
	
	BOOL bWdSetInvalidModDecoratorOnVeh = FALSE
	
	BOOL bWdSetPassedLester
	BOOL bWdSetPassedLamar
	BOOL bWdSetPassedMartin
	BOOL bWdSetPassedSimeon
	BOOL bWdSetPassedTrevor
	BOOL bWdSetPassedRon
	BOOL bWdSetPassedGerald
	BOOL dDebugUploadLBData
	INT debugUploadLBRaceTime = 70000
	INT debugUploadLBBestLap = 35000
	
	BOOL bDebugGangCallWantOwnership[GANG_CALL_TYPE_TOTAL_NUMBER]
	BOOL bDebugGangCallBitset[GANG_CALL_TYPE_TOTAL_NUMBER]
	
	BOOL bDebugContactRequestOnCooldown[CONTACT_REQUEST_MAX]
	BOOL bDebugContactRequestEndCooldown[CONTACT_REQUEST_MAX]
	INT iDebugContactRequestCooldownStagger = 0
	
	BOOL db_bInsureCar
	BOOL db_bCleanupPV
	BOOL db_bCleanupPV_bdelete
	BOOL db_bCleanupPV_bIgnoreEmpty
	BOOL db_bCleanupPV_bReturnToGarage
	BOOL db_bCleanupPV_bReturnFromGarageAfterMission
	BOOL db_bCleanupPV_bDoFadeOut
	
	BOOL db_bDisplayPVDebug
	BOOL db_bOutputVehStates
	BOOL bDebugTestOrderedCars
	BOOL b_DB_AllowCoronaOutput
	INT d_iCachedVehicleSpawnQueue
	BOOL bDebugTestNPCBounty
	BOOL b_DeletePV
	
	BOOL b_ClearFreeHanagerVehSet
	//BOOL bEnableHeliTaxi
	BOOL bDebugClearDailyBountyData
	
	BOOL db_bCheckPutVehInGarage
	BOOL db_bTurnOnHydraulics
	
	INT iTestWarningBS
	BOOL db_bTestLBWarning
	
	BOOL db_outputCurrentCarStats
	VEHICLE_STATS_CURRENT_VEHICLE debugVehStats
	
	BOOL db_setMechanicBlockedFlag
	
	BOOL db_testFindMatchedGamers
	
	BOOL db_bPropertyProfileSave
	BOOL db_bPropertyNonProfileSave
	INT iTestPropertyVerificationID
	
	BOOL db_bMCDoorOverride
	BOOL db_bMCDoorClearOverride
	INT db_iMCDoorID
	INT db_iMCDoorState
	
	INT db_iPVOpenShut_Door
	BOOL db_bPVOpenShut_Open
	BOOL db_bPVOpenShut_Shut
	BOOL db_bPVOpenShut_Instant
	BOOL db_bPVOpenShut_SwingFree
	
	INT db_iPV_RadioStation
	BOOL db_bPV_RadioLoud
	BOOL db_bPV_RadioSet
	
	BOOL db_bPV_EngineRunning_set
	BOOL db_bPV_EngineRunning_unset
	
	INT db_iPV_LightSetting
	BOOL db_bPV_Lights_set

#ENDIF

// -----------------------------------	MAIN PROCS ----------------------------------------------------------------------FEATURE_GUNRUNNING
// -----------------------------------	DEBUGGERY ----------------------------------------------------------------------
FUNC BOOL GET_COMMANDLINE_PARAM_EXISTS_DBSAFE(STRING str)
	#IF IS_DEBUG_BUILD
		RETURN GET_COMMANDLINE_PARAM_EXISTS(str)
	#ENDIF
	UNUSED_PARAMETER(str)
	RETURN FALSE
ENDFUNC
#IF IS_DEBUG_BUILD


	BOOL bPrintMyCrewData
	PROC ADD_CLAN_ID_WIDGETS()
		START_WIDGET_GROUP("Print Crew Data")
			ADD_WIDGET_BOOL("Print My Crew Data", bPrintMyCrewData)
		STOP_WIDGET_GROUP()
	ENDPROC
	
	PROC MAINTAIN_CLAN_ID_WIDGETS()
		
		IF bPrintMyCrewData
			GAMER_HANDLE GamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
			NETWORK_CLAN_DESC retDesc
			NETWORK_CLAN_PLAYER_GET_DESC( retDesc, 0, GamerHandle)
			NET_PRINT("-------------------------")NET_NL()
			NET_PRINT("		  My Crew Data		")NET_NL()
			NET_PRINT("-------------------------")NET_NL()
			NET_PRINT("MAINTAIN_CLAN_ID_WIDGETS - crew ID: 					")NET_PRINT_INT(retDesc.Id)NET_NL()
			NET_PRINT("MAINTAIN_CLAN_ID_WIDGETS - crew Name:				")NET_PRINT(retDesc.ClanName)NET_NL()
			NET_PRINT("MAINTAIN_CLAN_ID_WIDGETS - crew Tag:					")NET_PRINT(retDesc.ClanTag)NET_NL()
			NET_PRINT("MAINTAIN_CLAN_ID_WIDGETS - crew Member Count:		")NET_PRINT_INT(retDesc.MemberCount)NET_NL()
			NET_PRINT("MAINTAIN_CLAN_ID_WIDGETS - crew Is System Crew:		")NET_PRINT_INT(retDesc.IsSystemClan)NET_NL()
			NET_PRINT("MAINTAIN_CLAN_ID_WIDGETS - crew Is Open Crew:		")NET_PRINT_INT(retDesc.IsOpenClan)NET_NL()
			NET_PRINT("MAINTAIN_CLAN_ID_WIDGETS - crew Rank Name:			")NET_PRINT(retDesc.RankName)NET_NL()
			NET_PRINT("MAINTAIN_CLAN_ID_WIDGETS - crew Rank Order:			")NET_PRINT_INT(retDesc.RankOrder)NET_NL()
			NET_PRINT("MAINTAIN_CLAN_ID_WIDGETS - crew Created Time Posix:	")NET_PRINT_INT(retDesc.CreatedTimePosix)NET_NL()
			bPrintMyCrewData = FALSE
		ENDIF
	
	ENDPROC
	
	
	PROC ADD_BROADCAST_DISABLE_RECORDING_FOR_SPHERE_WIDGET()
		START_WIDGET_GROUP("BROADCAST_DISABLE_RECORDING_FOR_SPHERE")
			ADD_WIDGET_BOOL("call ", b_CallDisableRecordingSphere)
			ADD_WIDGET_VECTOR_SLIDER("coords", vDisableRecordingSphere_Coords, -99999.9, 99999.9, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("radius", fDisableRecordingSphere_Radius, 0.0, 9999.9, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("max dist", fDisableRecordingSphere_MaxDist, 0.0, 9999.9, 0.01)
			ADD_WIDGET_INT_SLIDER("duration", iDisableRecordingSphere_Duration, 0, HIGHEST_INT, 1)
		STOP_WIDGET_GROUP()	
	ENDPROC
	PROC UPDATE_BROADCAST_DISABLE_RECORDING_FOR_SPHERE_WIDGET()
		IF (b_CallDisableRecordingSphere)
			BROADCAST_DISABLE_RECORDING_FOR_SPHERE(vDisableRecordingSphere_Coords, fDisableRecordingSphere_Radius, fDisableRecordingSphere_MaxDist, iDisableRecordingSphere_Duration)
			b_CallDisableRecordingSphere = FALSE
		ENDIF
	ENDPROC
	

PROC UPDATE_DEBUG	

	// Increase / decrease XP
	FLOAT fXpRank, fNextXpRank
	INT i
	
	IF GET_COMMANDLINE_PARAM_EXISTS("nocheats") = FALSE
	
		
		// Increase / decrease rank
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_COMMA, KEYBOARD_MODIFIER_SHIFT, "Decrease Rank")
			
			iMyRank = GET_PLAYER_FM_RANK(PLAYER_ID())
			
			GET_MIN_MAX_XP_FOR_FM_RANK(iMyRank-1, fXpRank, fNextXpRank)
			iXpToGive = iMyXp - ROUND(fXpRank + ((fNextXpRank - fXpRank) / 2))
			iXpToGive = iXpToGive*-1
			
			IF iMyRank <= 1
				SET_LOCAL_PLAYER_FM_XP_DO_NOT_USE(0, XPCATEGORY_KEYS_DEBUG )
				iXpToGive = 0
			ENDIF
			g_bPlayerDebugChangedRank = TRUE
			g_bPlayerDebuggedXP = TRUE
			NET_NL()NET_PRINT("iXpToGive = ")NET_PRINT_INT(iXpToGive)
		ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_PERIOD, KEYBOARD_MODIFIER_SHIFT, "Increase Rank") 
		
			iMyRank = GET_PLAYER_FM_RANK(PLAYER_ID())
			IF iMyRank > MAX_FM_RANK-1
				iXpToGive = 0
			ELIF iMyRank = MAX_FM_RANK-1
				SET_LOCAL_PLAYER_FM_XP_DO_NOT_USE(GET_XP_NEEDED_FOR_RANK(MAX_FM_RANK), XPCATEGORY_KEYS_DEBUG)
				iXpToGive = 0
			ELSE
				GET_MIN_MAX_XP_FOR_FM_RANK(iMyRank+1, fXpRank, fNextXpRank)
				iXpToGive = ROUND(fXpRank + ((fNextXpRank - fXpRank) / 2)) - iMyXp
			ENDIF
			g_bPlayerDebugChangedRank = TRUE
			g_bPlayerDebuggedXP = TRUE
			g_bDoUnlocksForDebugRankUp = TRUE
			NET_NL()NET_PRINT("iXpToGive = ")NET_PRINT_INT(iXpToGive)
		ENDIF

		// increase / decrease mental state
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_COMMA, KEYBOARD_MODIFIER_CTRL, "Decrease Mental State")	
			INCREASE_PLAYER_MENTAL_STATE(-5.0)
		ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_PERIOD, KEYBOARD_MODIFIER_CTRL, "Increase Mental State") 
			INCREASE_PLAYER_MENTAL_STATE(5.0)
		ENDIF
		
		
		IF iXpToGive > 0 		
			SET_LOCAL_PLAYER_FM_XP_DO_NOT_USE(GET_PLAYER_FM_XP(PLAYER_ID()) + iXpToGive, XPCATEGORY_KEYS_DEBUG)
			g_bPlayerDebuggedXP = TRUE
			iXpToGive = 0
		ELIF iXpToGive < 0 
			SET_LOCAL_PLAYER_FM_XP_DO_NOT_USE(GET_PLAYER_FM_XP(PLAYER_ID()) + iXpToGive, XPCATEGORY_KEYS_DEBUG)
			g_bPlayerDebuggedXP = TRUE
			iXpToGive = 0
		ENDIF
		
				
		IF iCrewXpToGive > 0 		
			INCREMENT_BY_MP_INT_PLAYER_STAT(GET_PLAYER_LOCAL_CREW_XP_FROM_PLAYER_ID(player_ID()), iCrewXpToGive)
			g_bPlayerDebuggedXP = TRUE
			iCrewXpToGive = 0
		ELIF iCrewXpToGive < 0 
			INCREMENT_BY_MP_INT_PLAYER_STAT(GET_PLAYER_LOCAL_CREW_XP_FROM_PLAYER_ID(player_ID()), iCrewXpToGive)
			g_bPlayerDebuggedXP = TRUE
			iCrewXpToGive = 0
		ENDIF
	
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_UP, KEYBOARD_MODIFIER_SHIFT, "Ruiner2000 Personal Check Toggle")
			bRuinerPersonalCheckDisable = !bRuinerPersonalCheckDisable
			
			IF bRuinerPersonalCheckDisable
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(PLAYER_PED_ID(), "Ruiner2000 Only Personal Vehicle Shock Check Deactivated", 0)
			ELSE
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(PLAYER_PED_ID(), "Ruiner2000 Only Personal Vehicle Shock Check Activated", 0)
			ENDIF
		ENDIF
		
		//Increase / Decrease Cash
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F6, KEYBOARD_MODIFIER_SHIFT, "Decrease Cash")
			GIVE_LOCAL_PLAYER_FM_CASH(-1000, 1, TRUE, 0) //Waiting on NETWORK_SPENT_FROM_DEBUG bug 1368783
		ENDIF
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F6)
			GIVE_LOCAL_PLAYER_FM_CASH(5000, 1, TRUE, 0)
			INT iTransactionID
			TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_DEBUG, 5000, iTransactionID)
			IF !USE_SERVER_TRANSACTIONS()
				NETWORK_EARN_FROM_DEBUG(5000)
			ENDIF
		ENDIF
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F6, KEYBOARD_MODIFIER_CTRL, "Lump Cash Increase")
			GIVE_LOCAL_PLAYER_FM_CASH(10000000, 1, TRUE, 0)
			INT iTransactionID
			TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_DEBUG, 10000000, iTransactionID)
			IF !USE_SERVER_TRANSACTIONS()
				NETWORK_EARN_FROM_DEBUG(10000000)
			ENDIF
		ENDIF
		
		IF iCashToGive != 0
			GIVE_LOCAL_PLAYER_FM_CASH(iCashToGive, 1, TRUE, 0)		
			IF iCashToGive > 0
				INT iTransactionID
				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_DEBUG, iCashToGive, iTransactionID)
				IF !USE_SERVER_TRANSACTIONS()
					NETWORK_EARN_FROM_DEBUG(iCashToGive)
				ENDIF
			ELSE	
				IF !USE_SERVER_TRANSACTIONS()
					NETWORK_SPENT_FROM_DEBUG(iCashToGive*-1, DEFAULT, TRUE)
				ENDIF
			ENDIF
			
			iCashToGive = 0
		ENDIF
		
	ENDIF
	
	
	
	iMyCash = GET_PLAYER_FM_CASH(PLAYER_ID())
	iMyXp = GET_PLAYER_FM_XP(PLAYER_ID())
	iMyCrewXp = GET_PLAYER_FM_CREW_XP(PLAYER_ID())
	 iMyCrewSlot =  GET_PLAYER_LOCAL_CREW_SLOT_FROM_PLAYER_ID(PLAYER_ID())
	IF bUnlockAllFMActivites
		UNLOCK_ALL_FM_ACTIVITIES()
		bUnlockAllFMActivites = FALSE
	ENDIF
	
	REPEAT MAX_OWNED_PROPERTIES i
		IF MPGlobalsAmbience.warpToCurrentProperty[i] = TRUE
			INT iPropertyStat = GET_OWNED_PROPERTY(i)
			IF iPropertyStat > 0
				GET_MP_PROPERTY_DETAILS(tempPropertyStruct,iPropertyStat) 
				MPGlobalsAmbience.bMenuWarp = TRUE
				
				MPGlobalsAmbience.bKeepVeh = TRUE
				
				MPGlobalsAmbience.vMenuWarpCoord = tempPropertyStruct.vHouseHeistExitLocation
				MPGlobalsAmbience.fMenuWarpHeading = 0
				DO_SCREEN_FADE_OUT(0)
			ENDIF
			PRINTLN("Debug warping to currently owned property #", iPropertyStat) 
			MPGlobalsAmbience.warpToCurrentProperty[i] = FALSE
		ENDIF
	ENDREPEAT
	
	IF bd_bForceApartmentCleaning
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.TODLastCleaned = GET_CURRENT_TIMEOFDAY()
		SUBTRACT_TIME_FROM_TIMEOFDAY(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.TODLastCleaned,0,0,0,8)
		bd_bForceApartmentCleaning = FALSE
	ENDIF
	
	IF MPGlobalsAmbience.bPrintOwnedProperties
		REPEAT MAX_OWNED_PROPERTIES i
			INT iPropertyStat = GET_OWNED_PROPERTY(i)
			PRINTLN("Currently owned property #", iPropertyStat) 
		ENDREPEAT
		MPGlobalsAmbience.bPrintOwnedProperties = FALSE
	ENDIF
	
	IF IS_PROPERTY_OFFICE(MPGlobalsAmbience.iWarpToPropertyNum)
		IF MPGlobalsAmbience.iWarpToPropertyNum != 0
		AND MPGlobalsAmbience.iWarpToPropertyNum <= MAX_MP_PROPERTIES
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE, MPGlobalsAmbience.iWarpToPropertyNum)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_OFFICE_0-2] =CEIL(TO_FLOAT(GET_MP_PROPERTY_VALUE(MPGlobalsAmbience.iWarpToPropertyNum))*g_sMPTunables.fPropertyMultiplier)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_VALUE,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_OFFICE_0-2])
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_0] = MPGlobalsAmbience.iWarpToPropertyNum
			REQUEST_SAVE(STAT_SAVETYPE_SCRIPT_MP_GLOBALS)
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_VAR,PROPERTY_VARIATION_4)
			PRINTLN("Debug given player Office property #", MPGlobalsAmbience.iWarpToPropertyNum) 
			MPGlobalsAmbience.iWarpToPropertyNum = 0
		ENDIF
	ELIF IS_PROPERTY_CLUBHOUSE(MPGlobalsAmbience.iWarpToPropertyNum)
		IF MPGlobalsAmbience.iWarpToPropertyNum != 0
		AND MPGlobalsAmbience.iWarpToPropertyNum <= MAX_MP_PROPERTIES
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_CLUBHOUSE, MPGlobalsAmbience.iWarpToPropertyNum)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_CLUBHOUSE-2] =CEIL(TO_FLOAT(GET_MP_PROPERTY_VALUE(MPGlobalsAmbience.iWarpToPropertyNum))*g_sMPTunables.fPropertyMultiplier)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_CLUBHOUSE_VALUE,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_CLUBHOUSE-2])
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_CLUBHOUSE] = MPGlobalsAmbience.iWarpToPropertyNum
			REQUEST_SAVE(STAT_SAVETYPE_SCRIPT_MP_GLOBALS)
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_CLUBHOUSE_VAR,PROPERTY_VARIATION_4)
			PRINTLN("Debug given player Clubhouse property #", MPGlobalsAmbience.iWarpToPropertyNum) 
			MPGlobalsAmbience.iWarpToPropertyNum = 0
		ENDIF
	
	ELIF IS_PROPERTY_OFFICE_GARAGE(MPGlobalsAmbience.iWarpToPropertyNum)
		INT iPropertySlot
		IF MPGlobalsAmbience.iWarpToPropertyNum != 0
		AND MPGlobalsAmbience.iWarpToPropertyNum <= MAX_MP_PROPERTIES
			MPGlobalsAmbience.iWarpToPropertyNum = GET_OFFICE_GARAGE_BASE_ID_FROM_PROPERTY(MPGlobalsAmbience.iWarpToPropertyNum)
			IF MPGlobalsAmbience.iWarpToPropertyNum = PROPERTY_OFFICE_1_GARAGE_LVL1
				iPropertySlot = PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR1, MPGlobalsAmbience.iWarpToPropertyNum)
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR1_VAR, 1)
				#IF IS_DEBUG_BUILD
					SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS(1,1,1,1)
					SET_PLAYER_OWNS_OFFICE_MOD_GARAGE(TRUE)
				#ENDIF
			ELIF MPGlobalsAmbience.iWarpToPropertyNum = PROPERTY_OFFICE_1_GARAGE_LVL2
				iPropertySlot = PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL2
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR2, MPGlobalsAmbience.iWarpToPropertyNum)
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR2_VAR, 1)
				#IF IS_DEBUG_BUILD
					SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS(1,1,1,1)
					SET_PLAYER_OWNS_OFFICE_MOD_GARAGE(TRUE)
				#ENDIF
			ELIF MPGlobalsAmbience.iWarpToPropertyNum = PROPERTY_OFFICE_1_GARAGE_LVL3
				iPropertySlot = PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL3
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR3, MPGlobalsAmbience.iWarpToPropertyNum)
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR3_VAR, 1)
				#IF IS_DEBUG_BUILD
					SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS(1,1,1,1)
					SET_PLAYER_OWNS_OFFICE_MOD_GARAGE(TRUE)
				#ENDIF
			ENDIF
			
			//g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[iPropertySlot-2] =CEIL(TO_FLOAT(GET_MP_PROPERTY_VALUE(MPGlobalsAmbience.iWarpToPropertyNum))*g_sMPTunables.fPropertyMultiplier)
			//SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_VALUE,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[iPropertySlot-2])
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[iPropertySlot] = MPGlobalsAmbience.iWarpToPropertyNum
			REQUEST_SAVE(STAT_SAVETYPE_SCRIPT_MP_GLOBALS)
			//SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_VAR,PROPERTY_VARIATION_4)
			PRINTLN("Debug given player Office garage property #", MPGlobalsAmbience.iWarpToPropertyNum) 
			MPGlobalsAmbience.iWarpToPropertyNum = 0
		ENDIF
	ELIF IS_PROPERTY_MEGAWARE_GARAGE(MPGlobalsAmbience.iWarpToPropertyNum)
		IF MPGlobalsAmbience.iWarpToPropertyNum != 0
		AND MPGlobalsAmbience.iWarpToPropertyNum <= MAX_MP_PROPERTIES
			IF MPGlobalsAmbience.iWarpToPropertyNum = PROPERTY_MEGAWARE_GARAGE_LVL1
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_MEGAWARE_GAR1, MPGlobalsAmbience.iWarpToPropertyNum)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL1-2] =CEIL(TO_FLOAT(GET_MP_PROPERTY_VALUE(MPGlobalsAmbience.iWarpToPropertyNum))*g_sMPTunables.fPropertyMultiplier)
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL1] = MPGlobalsAmbience.iWarpToPropertyNum
			ELIF MPGlobalsAmbience.iWarpToPropertyNum = PROPERTY_MEGAWARE_GARAGE_LVL2
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_MEGAWARE_GAR2, MPGlobalsAmbience.iWarpToPropertyNum)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL2-2] =CEIL(TO_FLOAT(GET_MP_PROPERTY_VALUE(MPGlobalsAmbience.iWarpToPropertyNum))*g_sMPTunables.fPropertyMultiplier)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_MEGAWARE_GAR2_VALUE,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL2-2])
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL2] = MPGlobalsAmbience.iWarpToPropertyNum
			ELIF MPGlobalsAmbience.iWarpToPropertyNum = PROPERTY_MEGAWARE_GARAGE_LVL3
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_MEGAWARE_GAR3, MPGlobalsAmbience.iWarpToPropertyNum)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL3-2] =CEIL(TO_FLOAT(GET_MP_PROPERTY_VALUE(MPGlobalsAmbience.iWarpToPropertyNum))*g_sMPTunables.fPropertyMultiplier)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_MEGAWARE_GAR3_VALUE,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL3-2])
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL3] = MPGlobalsAmbience.iWarpToPropertyNum
			ENDIF
			REQUEST_SAVE(STAT_SAVETYPE_SCRIPT_MP_GLOBALS)
			//SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_CLUBHOUSE_VAR,PROPERTY_VARIATION_4)
			PRINTLN("Debug given player property #", MPGlobalsAmbience.iWarpToPropertyNum) 
			MPGlobalsAmbience.iWarpToPropertyNum = 0
		ENDIF
	ELSE
		IF MPGlobalsAmbience.bReplacePropertySlot1
			IF MPGlobalsAmbience.iWarpToPropertyNum != 0
			AND MPGlobalsAmbience.iWarpToPropertyNum <= MAX_MP_PROPERTIES
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_1,MPGlobalsAmbience.iWarpToPropertyNum)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiProperty1Value =CEIL(TO_FLOAT(GET_MP_PROPERTY_VALUE(MPGlobalsAmbience.iWarpToPropertyNum))*g_sMPTunables.fPropertyMultiplier)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_1,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiProperty1Value)
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[1] = MPGlobalsAmbience.iWarpToPropertyNum
				REQUEST_SAVE(STAT_SAVETYPE_SCRIPT_MP_GLOBALS)
				PRINTLN("Debug given player property 1 #", MPGlobalsAmbience.iWarpToPropertyNum) 
				MPGlobalsAmbience.iWarpToPropertyNum = 0
			ENDIF
		ELIF MPGlobalsAmbience.bReplacePropertySlot2
			IF MPGlobalsAmbience.iWarpToPropertyNum != 0
			AND MPGlobalsAmbience.iWarpToPropertyNum <= MAX_MP_PROPERTIES
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_2,MPGlobalsAmbience.iWarpToPropertyNum)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[0] =CEIL(TO_FLOAT(GET_MP_PROPERTY_VALUE(MPGlobalsAmbience.iWarpToPropertyNum))*g_sMPTunables.fPropertyMultiplier)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_2,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[0])
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[2] = MPGlobalsAmbience.iWarpToPropertyNum
				REQUEST_SAVE(STAT_SAVETYPE_SCRIPT_MP_GLOBALS)
				PRINTLN("Debug given player property 2 #", MPGlobalsAmbience.iWarpToPropertyNum) 
				MPGlobalsAmbience.iWarpToPropertyNum = 0
			ENDIF
		ELIF MPGlobalsAmbience.bReplacePropertySlot3
			IF MPGlobalsAmbience.iWarpToPropertyNum != 0
			AND MPGlobalsAmbience.iWarpToPropertyNum <= MAX_MP_PROPERTIES
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_3,MPGlobalsAmbience.iWarpToPropertyNum)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[1] =CEIL(TO_FLOAT(GET_MP_PROPERTY_VALUE(MPGlobalsAmbience.iWarpToPropertyNum))*g_sMPTunables.fPropertyMultiplier)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_3,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[1])
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[3] = MPGlobalsAmbience.iWarpToPropertyNum
				REQUEST_SAVE(STAT_SAVETYPE_SCRIPT_MP_GLOBALS)
				PRINTLN("Debug given player property 3 #", MPGlobalsAmbience.iWarpToPropertyNum) 
				MPGlobalsAmbience.iWarpToPropertyNum = 0
			ENDIF
		ELSE
			IF MPGlobalsAmbience.iWarpToPropertyNum != 0
			AND MPGlobalsAmbience.iWarpToPropertyNum <= MAX_MP_PROPERTIES
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE,MPGlobalsAmbience.iWarpToPropertyNum)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_TIME,GET_CLOUD_TIME_AS_INT())
				SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_NEW,MPGlobalsAmbience.iWarpToPropertyNum)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_NEW_TIME,GET_CLOUD_TIME_AS_INT())
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iCurrentPropertyValue =CEIL(TO_FLOAT(GET_MP_PROPERTY_VALUE(MPGlobalsAmbience.iWarpToPropertyNum))*g_sMPTunables.fPropertyMultiplier)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_NEW_VALUE,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iCurrentPropertyValue)
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[0] = MPGlobalsAmbience.iWarpToPropertyNum
				REQUEST_SAVE(STAT_SAVETYPE_SCRIPT_MP_GLOBALS)
				PRINTLN("Debug given player property 4 #", MPGlobalsAmbience.iWarpToPropertyNum) 
				MPGlobalsAmbience.iWarpToPropertyNum = 0
			ENDIF
		ENDIF
	ENDIF
	
	//Increase / Decrease STATS KW 26/8/2014 for TOD) 1857730 SHIFT + H gives full stats
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_H, KEYBOARD_MODIFIER_SHIFT, "Max Player Stats")
			INCREMENT_PLAYER_PED_STAT(CHAR_MULTIPLAYER, PS_STAMINA , 100)
			INCREMENT_PLAYER_PED_STAT(CHAR_MULTIPLAYER, PS_STRENGTH , 100)
			INCREMENT_PLAYER_PED_STAT(CHAR_MULTIPLAYER, PS_LUNG_CAPACITY , 100)
			INCREMENT_PLAYER_PED_STAT(CHAR_MULTIPLAYER, PS_DRIVING_ABILITY , 100)
			INCREMENT_PLAYER_PED_STAT(CHAR_MULTIPLAYER, PS_SHOOTING_ABILITY , 100)
			INCREMENT_PLAYER_PED_STAT(CHAR_MULTIPLAYER, PS_FLYING_ABILITY, 100)
			INCREMENT_PLAYER_PED_STAT(CHAR_MULTIPLAYER, PS_STEALTH_ABILITY , 100)
			SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(PLAYER_ID(), 1.1)
			
		ENDIF
	
	IF sclbDebugMetrics.bReset
		RESET_SCLB_DEBUG_METRICS_DATA()
	ENDIF
	IF g_dbVehDupProt.bTriggerTest
		g_MpSavedVehicles[g_dbVehDupProt.iVehicleSlot[0]].iObtainedTime = g_dbVehDupProt.iVehicleTimestamp[0]
		g_MpSavedVehicles[g_dbVehDupProt.iVehicleSlot[1]].iObtainedTime = g_dbVehDupProt.iVehicleTimestamp[1]
		g_dbVehDupProt.bTriggerTest = FALSE
		SET_BIT(g_MpSavedVehicles[g_dbVehDupProt.iVehicleSlot[0]].iVehicleBS,MP_SAVED_VEHICLE_HAS_VALID_TIMESTAMP)
		SET_BIT(g_MpSavedVehicles[g_dbVehDupProt.iVehicleSlot[1]].iVehicleBS,MP_SAVED_VEHICLE_HAS_VALID_TIMESTAMP)
		PRINTLN("CDM: tiggered dup test for slots #",g_dbVehDupProt.iVehicleSlot[0], " and #",g_dbVehDupProt.iVehicleSlot[1])
		PRINTLN("CDM: times are  #",g_dbVehDupProt.iVehicleTimestamp[0], " and #",g_dbVehDupProt.iVehicleTimestamp[1])
	ENDIF
	
	VEHICLE_INDEX tempVeh
	
	IF db_bSetOwnsOffice
		SET_PLAYER_OWNS_OFFICE(TRUE)
		db_bSetOwnsOffice = FALSE
	ENDIF
	
	IF db_bRemovePlayerOffice
		SET_PLAYER_OWNS_OFFICE(FALSE)
		db_bRemovePlayerOffice = FALSE
	ENDIF

	MAINTAIN_CLOTHING_FOR_VEHICLE_PURCHASE_WIDGETS()
	
	TRIGGER_ANIMATED_XP_TEST()
	TEST_ELO_LEADERBOARDS()
	TEST_LIMITED_SHOP_ITEMS() //1445116
	UPDATE_WIDGETS_FOR_PV_LIMITER(PVLimiterData)
	
	UPDATE_BG_SCRIPT_KICK_DEBUG(BGScriptKickDetails)
	
	UPDATE_FW_WEAPON_PICKUPS_DEBUG()

	IF (db_bPVOpenShut_Open)
		INT iMPDecorator
		tempVeh = GET_PLAYERS_LAST_VEHICLE()
		IF DOES_ENTITY_EXIST(tempVeh)
		AND IS_VEHICLE_DRIVEABLE(tempVeh)
			IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
				IF DECOR_EXIST_ON(tempVeh,"MPBitset")
					iMPDecorator = DECOR_GET_INT(tempVeh,"MPBitset")
				ENDIF
				SET_BIT(iMPDecorator,MP_DECORATOR_BS_PIM_DOOR_CONTROL)
				DECOR_SET_INT(tempVeh,"MPBitset",iMPDecorator)
				PRINTLN("CDM: Setting flag on current to allow door control")
			ENDIF
		ENDIF
		db_bPVOpenShut_Open = FALSE
	ENDIF


	IF (db_bPV_RadioSet)
		db_bPV_RadioSet = FALSE
	ENDIF
	
	IF (db_bPV_EngineRunning_set)
		BROADCAST_PERSONAL_VEHICLE_SET_ENGINE_RUNNING(TRUE)
		db_bPV_EngineRunning_set = FALSE
	ENDIF
	
	IF (db_bPV_EngineRunning_unset)
		BROADCAST_PERSONAL_VEHICLE_SET_ENGINE_RUNNING(FALSE)
		db_bPV_EngineRunning_unset = FALSE
	ENDIF
	
	IF (db_bPV_Lights_set)
		BROADCAST_PERSONAL_VEHICLE_SET_LIGHTS(INT_TO_ENUM(VEHICLE_LIGHT_SETTING, db_iPV_LightSetting))
		db_bPV_Lights_set = FALSE
	ENDIF

	
ENDPROC
	
/// PURPOSE:
///    Temporary hacky way of dealing with launching mission from debug menu. When the proper mission launching system is in place this will be removed.
PROC HANDLE_LAUNCHING_MISSIONS()

	IF g_iDebugSelectedMission != 0
		IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(g_iDebugSelectedMission)
			REQUEST_SCRIPT_WITH_NAME_HASH(g_iDebugSelectedMission)
			IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(g_iDebugSelectedMission)
				START_NEW_SCRIPT_WITH_NAME_HASH(g_iDebugSelectedMission, DEFAULT_STACK_SIZE)
				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(g_iDebugSelectedMission)
				g_iDebugSelectedMission = 0
			ENDIF
		ELSE
			g_iDebugSelectedMission = 0
		ENDIF
	ENDIF

ENDPROC
	
	BOOL bDrawBigLeaderboard
	INT iDebugLBColumns = 3
	PROC DEBUG_DISPLAY_BIG_LEADERBOARD(LEADERBOARD_PLACEMENT_TOOLS& Placement, LBD_VARS &paramlbdVars)
	
		INT i, iSlotCounter
		
		REQUEST_STREAMED_TEXTURE_DICT("MPOverview")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPOverview")
		
			RENDER_LEADERBOARD(	paramlbdVars, Placement, SUB_PLAYLIST, FALSE, "PLAYLIST TEST", iDebugLBColumns, -1, TRUE)
								
			REQUEST_STREAMED_TEXTURE_DICT("MPOverview")
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPOverview")
				
				Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].x = DDS_X
				Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].y = DDS_Y
				Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].w = DDS_W
				Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].h = DDS_H
				
				TEXT_LABEL_63 names[5]
				
				REPEAT COUNT_OF(names) i
					names[i] = "testname"
				ENDREPEAT
				
				BOOL bTeam
				
				REPEAT 5 i
					POPULATE_COLUMN_LBD(paramlbdVars,
										Placement, 
										SUB_PLAYLIST, 
										LBD_VOTE_CONTINUE,
										names, 
										iDebugLBColumns, 
										0, // ratio
										15,											//1 DEBUG
										15,											//2
										15,// car									//3
										15,											//4
										15,											//5
										999,// winningx								//6
										0,
										0,
										iSlotCounter, 
										999,
										i,
										999, 
										INT_TO_PLAYERINDEX(i), 
										FALSE,
										bTeam,
										10,
										0)	
				
					iSlotCounter++			
				ENDREPEAT
			ENDIF
			
		ENDIF
		
	ENDPROC
	

	PROC DO_DEBUG_AMBIENT_LAUNCHING()
		IF MPGlobalsAmbience.bLaunchTutorialMission
			IF NOT NETWORK_IS_SCRIPT_ACTIVE("MpTutorial", -1, TRUE)
				IF NET_LOAD_AND_LAUNCH_SCRIPT("MpTutorial", MULTIPLAYER_MISSION_STACK_SIZE)
					MPGlobalsAmbience.bLaunchTutorialMission = FALSE
					PRINTLN("[MPTUT] - DO_DEBUG_AMBIENT_LAUNCHING - launched MpTutorial")
				ENDIF
			ELSE
				MPGlobalsAmbience.bLaunchTutorialMission = FALSE
			ENDIF
		ENDIF
		
		IF MPGlobalsAmbience.bLaunchDCTL
			IF NOT NETWORK_IS_SCRIPT_ACTIVE("dont_cross_the_line", -1, TRUE)
				IF NET_LOAD_AND_LAUNCH_SCRIPT("dont_cross_the_line", SPECIAL_ABILITY_STACK_SIZE)
					MPGlobalsAmbience.bLaunchDCTL = FALSE
					PRINTLN("[DCTL] - DO_DEBUG_AMBIENT_LAUNCHING - launched dont_cross_the_line")
				ENDIF
			ELSE
				MPGlobalsAmbience.bLaunchDCTL = FALSE
			ENDIF
		ENDIF
		
		IF MPGlobalsAmbience.bLaunchGridArcade
			IF NOT NETWORK_IS_SCRIPT_ACTIVE("grid_arcade_cabinet", -1, TRUE)
				IF NET_LOAD_AND_LAUNCH_SCRIPT("grid_arcade_cabinet", SPECIAL_ABILITY_STACK_SIZE)
					MPGlobalsAmbience.bLaunchGridArcade = FALSE
					PRINTLN("[DCTL] - DO_DEBUG_AMBIENT_LAUNCHING - launched grid_arcade_cabinet")
				ENDIF
			ELSE
				MPGlobalsAmbience.bLaunchGridArcade = FALSE
			ENDIF
		ENDIF

		IF MPGlobalsAmbience.bLaunchScrollingArcade
			IF NOT NETWORK_IS_SCRIPT_ACTIVE("scrolling_arcade_cabinet", -1, TRUE)
				IF NET_LOAD_AND_LAUNCH_SCRIPT("scrolling_arcade_cabinet", SPECIAL_ABILITY_STACK_SIZE)
					MPGlobalsAmbience.bLaunchScrollingArcade = FALSE
					PRINTLN("[DCTL] - DO_DEBUG_AMBIENT_LAUNCHING - launched scrolling_arcade_cabinet")
				ENDIF
			ELSE
				MPGlobalsAmbience.bLaunchScrollingArcade = FALSE
			ENDIF
		ENDIF
		
	ENDPROC
	
	#IF IS_DEBUG_BUILD

	BOOL bPerformedGBBecomeBossCheck
	// Debug support for becoming a boss immediately if we are the first in the session.
	PROC GB_FORCE_BOSS_IN_NEW_SESSION_CHECK()

		#IF IS_DEBUG_BUILD
		IF bPerformedGBBecomeBossCheck
		OR NOT IS_SKYSWOOP_AT_GROUND()
			EXIT
		ENDIF
		
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_GBMakeFirstPlayerInSessionABoss")
			IF NOT NETWORK_IS_ACTIVITY_SESSION()
			AND NETWORK_GET_NUM_PARTICIPANTS() <= 1
			AND NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				PRINTLN("[[MAGNATE_GANG_BOSS] DEBUG - Forcing local player as a boss due to : sc_GBMakeFirstPlayerInSessionABoss")	
				IF USE_SERVER_TRANSACTIONS()
					// We can't handle this right now (speak to Chris M)
					bPerformedGBBecomeBossCheck = TRUE
				ELSE
					NETWORK_EARN_FROM_DEBUG(g_sMPTunables.iBossBuyInCost)
					IF NETWORK_SPENT_BOSS_GOON(g_sMPTunables.iBossBuyInCost, FALSE, TRUE)
						GB_SET_LOCAL_PLAYER_AS_BOSS_OF_GANG(DEFAULT,TRUE,GT_VIP)
						bPerformedGBBecomeBossCheck = TRUE
					ENDIF
				ENDIF			
			ELSE
				bPerformedGBBecomeBossCheck = TRUE
			ENDIF
		ELSE
			bPerformedGBBecomeBossCheck = TRUE
		ENDIF
		#ENDIF

	ENDPROC
	#ENDIF

	
	PROC ADD_PV_DEBUG_LINE(STRING strName, STRING strValue, FLOAT fScreenX, FLOAT fScreenY, FLOAT &fYOffset)
		IF NOT IS_STRING_NULL_OR_EMPTY(strName)
		OR NOT IS_STRING_NULL_OR_EMPTY(strValue)
			SET_TEXT_FONT(FONT_STANDARD)
			SET_TEXT_SCALE(0.0000, 0.360)
			SET_TEXT_COLOUR(255, 165, 0, 255)
			SET_TEXT_WRAP(0.0, 5.0)
			BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("MPCT_PVNAME2")
				ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(strName)
				ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(strValue)
			FLOAT fWidth = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
			DRAW_RECT_FROM_CORNER(fScreenX, fScreenY+fYOffset, fWidth, 0.030, 0, 0, 0, 150)
			
			SET_TEXT_FONT(FONT_STANDARD)
			SET_TEXT_SCALE(0.0000, 0.360)
			SET_TEXT_COLOUR(255, 165, 0, 255)
			SET_TEXT_WRAP(0.0, 5.0)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("MPCT_PVNAME2")
				ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(strName)
				ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(strValue)
			END_TEXT_COMMAND_DISPLAY_TEXT(fScreenX, fScreenY+fYOffset)
			
			fYOffset += 0.030
		ENDIF
	ENDPROC
	
#IF IS_DEBUG_BUILD
		
	PROC DEBUG_START_RC_VEHICLE()
		IF g_bStartRCVehicleScript
			START_RC_VEHICLE(<<0,0,0>>, 0.0, <<0,0,0>>, RCBANDITO)
		ENDIF
	ENDPROC 
	
#ENDIF
	
	
	
	
	PROC UPDATE_WIDGETS()
		INT i
		VECTOR vLoc
		
		IF bGetXUID
			IF IS_XBOX360_VERSION()
			OR IS_DURANGO_VERSION()
				STRING stWidget = GET_CONTENTS_OF_TEXT_WIDGET(twID)
				STRING stResult = NETWORK_GET_XUID_FROM_GAMERTAG(stWidget)
				IF NOT IS_STRING_NULL_OR_EMPTY(stResult)
					PRINTLN()
					PRINTLN()
					PRINTSTRING("NETWORK_GET_XUID_FROM_GAMERTAG Result - ")PRINTLN()
					PRINTSTRING(stResult)
					PRINTLN()
					PRINTLN()
					PRINTLN()
					bGetXUID = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF fDebugSphereRadius > 0.0
			DRAW_MARKER_SPHERE(vDebugSphereLocation, fDebugSphereRadius, 255, 0, 0, 0.2)
		ENDIF

		UPDATE_GANG_ANGRY_WIDGETS(gangAngryData)
		
		Update_Headshot_Widgets()
		
		UPDATE_MP_AMBIENT_MANAGER_WIDGETS()
		
		UPDATE_CELEBRATION_SCREEN_WIDGETS()
		
		MAINTAIN_VEHICLE_GENERATOR_DEBUG_WIDGET(g_dFreemodeVehGenData)
		
		//Update Player Damage Modifier
		IF lw_bSetPlayerWeaponDamageModifier = TRUE
			BROADCAST_UPDATE_PLAYER_WEAPON_DAMAGE_MODIFIER(lw_fPlayerWeaponDamageModifier, ALL_PLAYERS())
			lw_bSetPlayerWeaponDamageModifier = FALSE
			NET_PRINT(" - LES WIDGETS - BROADCAST_UPDATE_PLAYER_WEAPON_DAMAGE_MODIFIER - lw_bSetPlayerWeaponDamageModifier = ") NET_PRINT_FLOAT(lw_fPlayerWeaponDamageModifier) NET_NL()
		ENDIF
		//Update Player Vehicle Damage Modifier
		IF lw_bSetPlayerVehicleDamageModifier = TRUE
			BROADCAST_UPDATE_PLAYER_VEHICLE_DAMAGE_MODIFIER(lw_fPlayerVehicleDamageModifier, ALL_PLAYERS())
			lw_bSetPlayerVehicleDamageModifier = FALSE
			NET_PRINT(" - LES WIDGETS - BROADCAST_UPDATE_PLAYER_VEHICLE_DAMAGE_MODIFIER - lw_bSetPlayerVehicleDamageModifier = ") NET_PRINT_FLOAT(lw_fPlayerVehicleDamageModifier) NET_NL()
		ENDIF
		//Update Ai Damage Modifier
		IF lw_bSetAiDamageModifier = TRUE
			BROADCAST_UPDATE_AI_DAMAGE_MODIFIER(lw_fAiDamageModifier, ALL_PLAYERS())
			lw_bSetAiDamageModifier = FALSE
			NET_PRINT(" - LES WIDGETS - BROADCAST_UPDATE_AI_DAMAGE_MODIFIER - lw_fAiDamageModifier = ") NET_PRINT_FLOAT(lw_fAiDamageModifier) NET_NL()
		ENDIF
		//Update Mission difficulty Modifier
		IF lw_bSetMissionDiffModifier = TRUE
			BROADCAST_UPDATE_MISSION_DIFF_MODIFIER(lw_fSetMissPedHealth,lw_fSetMissPedAccuracy,lw_iEnemyRespawns, ALL_PLAYERS())
			lw_bSetMissionDiffModifier = FALSE
			NET_PRINT(" - LES WIDGETS - BROADCAST_UPDATE_MISSION_DIFF_MODIFIER - lw_iSetMissPedHealth = ") NET_PRINT_FLOAT(lw_fSetMissPedHealth) NET_PRINT(" lw_iSetMissPedAccuracy = ") NET_PRINT_FLOAT(lw_fSetMissPedAccuracy)NET_PRINT(" lw_iEnemyRespawns = ") NET_PRINT_INT(lw_iEnemyRespawns) NET_NL()
		ENDIF
		IF g_bProcessBots
			//PROCESS_BOTS()
		ENDIF
		
		IF bDrawBigLeaderboard
			DEBUG_DISPLAY_BIG_LEADERBOARD(LeaderboardPlacement, lbdVars)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			
			IF bHoldUpSetTimerBit = TRUE
				IF NOT IS_BIT_SET(GlobalServerBD_HoldUp.HoldUpServerData.iHoldUpTimerDoneBitSet, 0)
					SET_BIT(GlobalServerBD_HoldUp.HoldUpServerData.iHoldUpTimerDoneBitSet, 0)
					PRINTLN(" HOLD UP - DEBUG WIDGET - bHoldUpSetTimerBit - iHoldUpTimerDoneBitSet SET")
				ELSE
					CLEAR_BIT(GlobalServerBD_HoldUp.HoldUpServerData.iHoldUpTimerDoneBitSet, 0)
					PRINTLN(" HOLD UP - DEBUG WIDGET - bHoldUpSetTimerBit - iHoldUpTimerDoneBitSet CLEARED")
				ENDIF
				bHoldUpSetTimerBit = FALSE
			ENDIF
			
			MAINTAIN_CLAN_ID_WIDGETS()
			
			UPDATE_BROADCAST_DISABLE_RECORDING_FOR_SPHERE_WIDGET()
			
			fCheatPlayerLevel = GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT)

			iWasIaCheaterWidget = GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_CHEATER)
			iCheatTimeBeenACheaterWidget = g_MPPLY_BECAME_CHEATER_NUM
			
			IF bCheatOverrideCheatLevels_LastFrame <> bCheatOverrideCheatLevels
				SC_GAMERDATA_BANK_CHEATER_OVERRIDE(bCheatOverrideCheatLevels)
			ENDIF
			bCheatOverrideCheatLevels_LastFrame = bCheatOverrideCheatLevels
			IF bCheatOverrideCheatLevels
				SC_GAMERDATA_BANK_CHEATER_RATING_OVERRIDE(bCheatOverrideCheatNumber)
			ENDIF
			

			IF HAVE_STATS_LOADED()
				bCheatLevelAmIaCheatLesWidget = NETWORK_PLAYER_IS_CHEATER()
			ENDIF
			IF iCheatLevelThreshold != g_sMPTunables.cheatThreshold
				NET_NL()NET_PRINT("TUNABLE: Changing value for CHEAT_THRESHOLD to ")NET_PRINT_INT(iCheatLevelThreshold)
				NETWORK_INSERT_TUNABLE_INT("MP_GLOBAL", "CHEAT_THRESHOLD", iCheatLevelThreshold)
				g_sMPTunables.cheatThreshold = iCheatLevelThreshold
			ENDIF
//			
			IF iNotCheatLevelThreshold != g_sMPTunables.NotcheatThreshold
				NET_NL()NET_PRINT("TUNABLE: Changing value for CHEAT_THRESHOLD_NOTCHEATER to ")NET_PRINT_INT(iNotCheatLevelThreshold)
				NETWORK_INSERT_TUNABLE_INT("MP_GLOBAL", "CHEAT_THRESHOLD_NOTCHEATER", iNotCheatLevelThreshold)
				g_sMPTunables.cheatThreshold = iNotCheatLevelThreshold
			ENDIF

		#ENDIF
		
		
		#IF IS_DEBUG_BUILD
		
			IF bWdGiverewardammotoplayer
				GIVE_CONTRABAND_SHIPMENT_AMMO_REWARD()
				bWdGiverewardammotoplayer = FALSE
			ENDIF
			
			IF bWdGiverewardtshirtcrosstheline 
				IF GET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_DCTL)
					SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_DCTL, FALSE)
					SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_DCTL_MSG, FALSE)
				ELSE
					SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_DCTL, TRUE)
					IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
						SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_DCTL_M"), TATTOO_MP_FM), TRUE)
					ELSE
						SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_DCTL_F"), TATTOO_MP_FM_F), TRUE)
					ENDIF
				ENDIF
				
				bWdGiverewardtshirtcrosstheline = FALSE
			ENDIF
			
			IF g_bTurnOnBadSportWidgets
				
				fBadSportPlayerLevel = GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT)

				IF g_iBadSportLevelIncreaseLesWidget != 0
				
					INT AmountBefore = FLOOR(GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT))
					
					INCREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, TO_FLOAT(g_iBadSportLevelIncreaseLesWidget))
					
					CHECK_BADSPORT_TICKER(AmountBefore)
					
					g_iBadSportLevelIncreaseLesWidget = 0
				ENDIF
				iWasIaBadSportWidget = GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_BAD_SPORT)
				iBadSportTimeBeenABadSportWidget = GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM)
				g_fBadSportLevelDisplayLesWidget = GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BadSport)
				sBadSportDateBecameBadSportLesWidget = (GET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_BADSPORT_DT))
				IF HAVE_STATS_LOADED()
					bBadSportLevelAmIaBadSportLesWidget = NETWORK_PLAYER_IS_BADSPORT()
				ENDIF
				
				IF iBadSportLevelThreshold != g_sMPTunables.BadSportThreshold
					NET_NL()NET_PRINT("TUNABLE: Changing value for BadSport_THRESHOLD to ")NET_PRINT_INT(iBadSportLevelThreshold)
					NETWORK_INSERT_TUNABLE_INT("MP_GLOBAL", "BadSport_THRESHOLD", iBadSportLevelThreshold)
					g_sMPTunables.BadSportThreshold = iBadSportLevelThreshold
				ENDIF
				
				IF iNotBadSportLevelThreshold != g_sMPTunables.NotBadSportThreshold
					NET_NL()NET_PRINT("TUNABLE: Changing value for BadSport_THRESHOLD_NOTBadSportER to ")NET_PRINT_INT(iNotBadSportLevelThreshold)
					NETWORK_INSERT_TUNABLE_INT("MP_GLOBAL", "BadSport_THRESHOLD_NOTBadSportER", iNotBadSportLevelThreshold)
					g_sMPTunables.BadSportThreshold = iNotBadSportLevelThreshold
				ENDIF

				IF fBadSportForgiveLevel != g_sMPTunables.BadSportAmountToForgiveBy
					NET_NL()NET_PRINT("TUNABLE: Changing value for AMOUNT_TO_FORGIVE_BY to ")NET_PRINT_FLOAT(fBadSportForgiveLevel)
					NETWORK_INSERT_TUNABLE_FLOAT("MP_GLOBAL", "AMOUNT_TO_FORGIVE_BY", fBadSportForgiveLevel)
					g_sMPTunables.BadSportAmountToForgiveBy = fBadSportForgiveLevel
				ENDIF
				
				
				IF bInitBadSportMuteWidget = FALSE
					NETWORK_GET_MUTE_COUNT_FOR_PLAYER(PLAYER_ID(), iBadSportMuteCount, iBadSportTalkerCount)
					bInitBadSportMuteWidget = TRUE
					
				ENDIF
				
				IF iBadSportMuteCount_Widget != 0
				OR iBadSportTalkerCount_Widget != 0
			
					IF iBadSportMuteCount_Widget != 0
						iBadSportMuteCount += iBadSportMuteCount_Widget
						iBadSportMuteCount_Widget = 0
					ENDIF
					IF iBadSportTalkerCount_Widget != 0
						iBadSportTalkerCount += iBadSportTalkerCount_Widget
						iBadSportTalkerCount_Widget = 0 
						
					ENDIF	
									
					DEBUG_NETWORK_SET_LOCAL_PLAYER_MUTE_COUNT(iBadSportMuteCount, iBadSportTalkerCount) 				
				
				ENDIF
			
			 
			
				IF bBadSportPlayerReset
					SET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BadSport, 0)
					REQUEST_SAVE()
					bBadSportPlayerReset = FALSE
				ENDIF
				IF bBadSportCharReset
					SET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BadSport, 0)
					REQUEST_SAVE()
					bBadSportCharReset = FALSE
				ENDIF
			ENDIF
		#ENDIF
		
		
		
				
		IF g_UpdatePlayersStartingHealth
			GIVE_PLAYER_ALL_UNLOCKED_HEALTH_FREEMODE()
			g_UpdatePlayersStartingHealth = FALSE
		ENDIF
		
		UPDATE_REWARD_WIDGETS()
		UPDATE_ALL_REWARD_UNLOCKS_WIDGETS()
		
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			PROCESS_HOLD_UP_DEBUG_WARPS(GlobalServerBD_HoldUp.HoldUpServerData)
		ENDIF
		

		IF bTestWarpToInterior
			IF NOT bTestWarpTakeAwayControl 
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
				bTestWarpTakeAwayControl = TRUE
			ELSE
				IF NET_WARP_TO_COORD(<< 16.1986, -1100.2493, 28.7970 >>, 245.2233,FALSE,FALSE)
					bTestWarpToInterior = FALSE
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		iCrateDropDay = GET_TIMEOFDAY_DAY(GlobalServerBD_BlockB.CrateDropServerLaunchData.TriggerTime)
	 	iCrateDropHour = GET_TIMEOFDAY_HOUR(GlobalServerBD_BlockB.CrateDropServerLaunchData.TriggerTime)
	 	iCrateDropMin = GET_TIMEOFDAY_MINUTE(GlobalServerBD_BlockB.CrateDropServerLaunchData.TriggerTime)
	 	bPlayerOnFmAmbient = IS_PLAYER_ON_ANY_MP_AMBIENT_SCRIPT(PLAYER_ID())
		
		bPlayerInTutorialSession = NETWORK_IS_IN_TUTORIAL_SESSION()
		
		IF bDebugDrawSafeCircleSpawn
			vLoc = GET_PLAYER_COORDS(PLAYER_ID())
			REPEAT NUM_NETWORK_PLAYERS i
				DRAW_DEBUG_SPHERE(GET_SAFE_WARP_COORD_IN_CIRCLE(i,vLoc,fDebugDrawSafeCircleRadius,FALSE),0.1,255,0,0,200)
			ENDREPEAT
		ENDIF
		
		
		g_DBi_OutputLeaderboardReadErrorsCounter++
		IF g_DBi_OutputLeaderboardReadErrorsCounter >= 10
			g_DBb_OutputLeaderboardReadErrors= TRUE
			g_DBi_OutputLeaderboardReadErrorsCounter = 0
		ELSE
			g_DBb_OutputLeaderboardReadErrors = FALSE
		ENDIF
		
		i = 0
		REPEAT GANG_CALL_TYPE_TOTAL_NUMBER i
			bDebugGangCallBitset[i] = IS_BIT_SET(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].iGangCallOwnerBitset, i)
		ENDREPEAT
		
		IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
			IF db_bInsureCar
				INSURE_MP_SAVED_VEHICLE_PRIVATE(CURRENT_SAVED_VEHICLE_SLOT())
				db_bInsureCar = FALSE
				PRINTLN("Applying insurance to currently player vehicle") 
			ENDIF
			
		ENDIF
		
		IF resetLocalCDOnContactRequests
			i= 0
			REPEAT CONTACT_REQUEST_MAX i
				RESET_NET_TIMER(g_TransitionSessionNonResetVars.contactRequests.stCDTimer[i])
			ENDREPEAT
			resetLocalCDOnContactRequests = FALSE
		ENDIF
		
		IF resetTargetCDOnContactRequests
			i= 0
			REPEAT CONTACT_REQUEST_MAX i
				RESET_NET_TIMER(g_AMC_playerBD[NETWORK_PLAYER_ID_TO_INT()].stPlayerCDTimer[i])
			ENDREPEAT
			resetTargetCDOnContactRequests = FALSE
		ENDIF
		
		IF db_bCleanupPV
			CLEANUP_MP_SAVED_VEHICLE(db_bCleanupPV_bdelete, db_bCleanupPV_bIgnoreEmpty, db_bCleanupPV_bReturnToGarage, db_bCleanupPV_bReturnFromGarageAfterMission, db_bCleanupPV_bDoFadeOut)
			db_bCleanupPV = FALSE
		ENDIF
		
		IF b_DeletePV
			IF NETWORK_DOES_NETWORK_ID_EXIST(PERSONAL_VEHICLE_NET_ID())
				DELETE_PERSONAL_VEHICLE_NET_ID()
			ENDIF
			b_DeletePV = FALSE
		ENDIF
		
		IF b_ClearFreeHanagerVehSet
			hangarGiftPlane.bAttemptedToAward = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HANGAR_OWNED_GIFTED_PLANE, FALSE)
			b_ClearFreeHanagerVehSet = FALSE
		ENDIF
		
		IF (bAmbDupe_GetCurrentVehicleDetails)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX tempVehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(tempVehicleID)
					iAmbDupe_PlateTextHash = GET_HASH_KEY(GET_VEHICLE_NUMBER_PLATE_TEXT(tempVehicleID))
					iAmbDupe_ModelName = ENUM_TO_INT(GET_ENTITY_MODEL(tempVehicleID))
				ENDIF
			ENDIF
			bAmbDupe_GetCurrentVehicleDetails = FALSE
		ENDIF		
			
		IF bAmbDupe_send
			AMBIENT_VEHICLE_DETAILS AmbVeh
			AmbVeh.ModelName = INT_TO_ENUM(MODEL_NAMES, iAmbDupe_ModelName) 
			AmbVeh.iHashPlateText = iAmbDupe_PlateTextHash
			AmbVeh.iRand = iAmbDupe_RandomID
			BROADCAST_LOCK_ANY_DUPE_AMBIENT_VEHICLES(AmbVeh)
			bAmbDupe_send = FALSE
		ENDIF
		

		IF db_bDisplayPVDebug
			IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed >= 0
				
				INT iVehSaveSlot = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed
				INT iDisplaySlot
				MPSV_GET_DISPLAY_SLOT_FROM_SAVE_SLOT(iVehSaveSlot, iDisplaySlot)
				
				INT iPropertySlot = GET_PROPERTY_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot)
				INT iPropertyID = GET_OWNED_PROPERTY(iPropertySlot)
				
				MODEL_NAMES vehicleModel = g_MpSavedVehicles[iVehSaveSlot].vehicleSetupMP.VehicleSetup.eModel
				
				TEXT_LABEL_63 tlValue
				FLOAT fScreenX, fScreenY, fYOffset
				fScreenX = 0.7
				fScreenY = 0.3
				
				tlValue = ""
				ADD_PV_DEBUG_LINE("~w~[Last Used PV]", tlValue, fScreenX, fScreenY, fYOffset)
			
				tlValue = "~g~"
				tlValue += GET_STRING_FROM_TEXT_FILE(mpProperties[iPropertyID].tl_PropertyName)
				ADD_PV_DEBUG_LINE("Property Name", tlValue, fScreenX, fScreenY, fYOffset)
				
				tlValue = "~g~"
				tlValue += iPropertyID
				ADD_PV_DEBUG_LINE("Property ID", tlValue, fScreenX, fScreenY, fYOffset)
				
				tlValue = "~g~"
				tlValue += iPropertySlot
				ADD_PV_DEBUG_LINE("Property Save Slot", tlValue, fScreenX, fScreenY, fYOffset)
				
				tlValue = ""
				IF (iDisplaySlot != -1)
					tlValue += "~g~"
				ELSE
					tlValue += "~r~"
				ENDIF
				tlValue += iDisplaySlot
				ADD_PV_DEBUG_LINE("Vehicle Display Slot", tlValue, fScreenX, fScreenY, fYOffset)
				
				tlValue = ""
				IF (iVehSaveSlot != -1)
					tlValue += "~g~"
				ELSE
					tlValue += "~r~"
				ENDIF
				tlValue += iVehSaveSlot
				ADD_PV_DEBUG_LINE("Vehicle Save Slot", tlValue, fScreenX, fScreenY, fYOffset)
				
				tlValue = "~g~"
				IF ARE_STRINGS_EQUAL(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(vehicleModel), "CARNOTFOUND")
					tlValue = "~r~"
				ENDIF
				tlValue += GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(vehicleModel)
				ADD_PV_DEBUG_LINE("Model Name", tlValue, fScreenX, fScreenY, fYOffset)
				
				tlValue = "~g~"
				tlValue += ENUM_TO_INT(vehicleModel)
				ADD_PV_DEBUG_LINE("Model Hash", tlValue, fScreenX, fScreenY, fYOffset)
				
				tlValue = "~g~"
				tlValue += g_MpSavedVehicles[iVehSaveSlot].vehicleSetupMP.VehicleSetup.tlPlateText
				ADD_PV_DEBUG_LINE("Plate", tlValue, fScreenX, fScreenY, fYOffset)
				
				tlValue = "~g~"
				tlValue += GET_HASH_KEY(g_MpSavedVehicles[iVehSaveSlot].vehicleSetupMP.VehicleSetup.tlPlateText)
				ADD_PV_DEBUG_LINE("Plate Hash", tlValue, fScreenX, fScreenY, fYOffset)
				
				tlValue = ""
				IF IS_BIT_SET(g_MpSavedVehicles[iVehSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_INSURED)
					tlValue += "~g~Yes"
				ELSE
					tlValue += "~r~No"
				ENDIF
				ADD_PV_DEBUG_LINE("Insured", tlValue, fScreenX, fScreenY, fYOffset)
				
				tlValue = ""
				IF IS_BIT_SET(g_MpSavedVehicles[iVehSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_BOUGHT_ONLINE)
					tlValue += "~g~Yes"
				ELSE
					tlValue += "~r~No"
				ENDIF
				ADD_PV_DEBUG_LINE("Bought Online", tlValue, fScreenX, fScreenY, fYOffset)
				
				tlValue = ""
				IF IS_BIT_SET(g_MpSavedVehicles[iVehSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_FREE_VEHICLE)
					tlValue += "~g~Yes"
				ELSE
					tlValue += "~r~No"
				ENDIF
				ADD_PV_DEBUG_LINE("Free", tlValue, fScreenX, fScreenY, fYOffset)
				
				tlValue = ""
				IF IS_BIT_SET(g_MpSavedVehicles[iVehSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_NON_SELLABLE)
					tlValue += "~g~Yes"
				ELSE
					tlValue += "~r~No"
				ENDIF
				ADD_PV_DEBUG_LINE("Sell blocked", tlValue, fScreenX, fScreenY, fYOffset)
				
				tlValue = ""
				IF IS_BIT_SET(g_MpSavedVehicles[iVehSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
					tlValue += "~g~Yes"
				ELSE
					tlValue += "~r~No"
				ENDIF
				ADD_PV_DEBUG_LINE("Out of garage", tlValue, fScreenX, fScreenY, fYOffset)
				
				tlValue = ""
				IF IS_BIT_SET(g_MpSavedVehicles[iVehSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED)
					tlValue += "~g~Yes"
				ELSE
					tlValue += "~r~No"
				ENDIF
				ADD_PV_DEBUG_LINE("Destroyed", tlValue, fScreenX, fScreenY, fYOffset)
				
				tlValue = ""
				IF IS_BIT_SET(g_MpSavedVehicles[iVehSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)
					tlValue += "~g~Yes"
				ELSE
					tlValue += "~r~No"
				ENDIF
				ADD_PV_DEBUG_LINE("Impounded", tlValue, fScreenX, fScreenY, fYOffset)
			ENDIF
		ENDIF
		
		IF db_bOutputVehStates
			PRINTLN("PERSONAL VEHICLE STATES")
			
			INT iVehSlot
			REPEAT 14 iVehSlot
				
				IF iVehSlot = CURRENT_SAVED_VEHICLE_SLOT()
					PRINTLN("VEHCILE ", iVehSlot, " ***LAST USED***")
				ELSE
					PRINTLN("VEHCILE ", iVehSlot)
				ENDIF
				
				PRINTLN("...MP_SAVED_VEHICLE_OUT_GARAGE = ", IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS, MP_SAVED_VEHICLE_OUT_GARAGE))
				PRINTLN("...MP_SAVED_VEHICLE_DESTROYED = ", IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS, MP_SAVED_VEHICLE_DESTROYED))
				PRINTLN("...MP_SAVED_VEHICLE_INSURED = ", IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS, MP_SAVED_VEHICLE_INSURED))
				PRINTLN("...MP_SAVED_VEHICLE_REMOTE_BOMB = ", IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS, MP_SAVED_VEHICLE_REMOTE_BOMB))
				PRINTLN("...MP_SAVED_VEHICLE_CLONED = ", IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS, MP_SAVED_VEHICLE_CLONED))
				PRINTLN("...MP_SAVED_VEHICLE_RECREATE_AFTER_SESSION = ", IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS, MP_SAVED_VEHICLE_RECREATE_AFTER_SESSION))
				PRINTLN("...MP_SAVED_VEHICLE_IMPOUNDED = ", IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS, MP_SAVED_VEHICLE_IMPOUNDED))
				PRINTLN("...MP_SAVED_VEHICLE_GIVEN_STATUS_MESSAGE = ", IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS, MP_SAVED_VEHICLE_GIVEN_STATUS_MESSAGE))
				PRINTLN("...MP_SAVED_VEHICLE_HAS_CREW_EMBLEM = ", IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS, MP_SAVED_VEHICLE_HAS_CREW_EMBLEM))
				PRINTLN("...MP_SAVED_VEHICLE_WAS_CREW_EMBLEM_PURCHASED = ", IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS, MP_SAVED_VEHICLE_WAS_CREW_EMBLEM_PURCHASED))
				PRINTLN("...MP_SAVED_VEHICLE_JUST_PURCHASED = ", IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS, MP_SAVED_VEHICLE_JUST_PURCHASED))
				PRINTLN("...MP_SAVED_VEHICLE_RECREATE_AT_MORS_MUTUAL = ", IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS, MP_SAVED_VEHICLE_RECREATE_AT_MORS_MUTUAL))
				PRINTLN("...MP_SAVED_VEHICLE_UPDATE_VEHICLE = ", IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS, MP_SAVED_VEHICLE_UPDATE_VEHICLE))
				PRINTLN("...MP_SAVED_VEHICLE_FREE_VEHICLE = ", IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS, MP_SAVED_VEHICLE_FREE_VEHICLE))
				PRINTLN("...MP_SAVED_VEHICLE_BOUGHT_ONLINE = ", IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS, MP_SAVED_VEHICLE_BOUGHT_ONLINE))
				PRINTLN("")
			ENDREPEAT
			
			db_bOutputVehStates = FALSE
		ENDIF
		
		IF bDebugTestOrderedCars
			INT iVehicleSlotArray[MAX_MP_SAVED_VEHICLES]
			MP_GET_SAVED_VEHICLES_OF_CLASS(iVehicleSlotArray,FMMC_VEHICLE_CLASS_SUPER)
			bDebugTestOrderedCars = FALSE
		ENDIF
		
		d_iCachedVehicleSpawnQueue = ENUM_TO_INT(g_eCachedVehicleSpawnQueue)
		
		if bDebugTestNPCBounty
			TRIGGER_NPC_BOUNTY(PLAYER_ID(),"TXT_BNTY_NPC3",CHAR_BLOCKED,TRUE)
			bDebugTestNPCBounty = FALSE
		ENDIF
		
		IF bDebugClearDailyBountyData
			CLEAR_DAILY_BOUNTY_DATA()
			bDebugClearDailyBountyData = FALSE
		ENDIF
		
		IF db_bCheckPutVehInGarage
			INT iReason
			IF MP_SAVE_VEHICLE_PUT_VEH_INTO_GARAGE(iReason,CURRENT_SAVED_VEHICLE_SLOT())
				PRINTLN("MP_SAVE_VEHICLE_PUT_VEH_INTO_GARAGE: reason for true = ",iReason)
				db_bCheckPutVehInGarage = FALSE
			ENDIF
		ENDIF
		VEHICLE_INDEX veh
		IF db_bTurnOnHydraulics
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					SET_CAN_USE_HYDRAULICS(veh,TRUE)
				ENDIF
			ENDIF
			db_bTurnOnHydraulics = FALSE
		ENDIF
		IF bPrintCurrentVehicleSetup
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				
				VEHICLE_SETUP_STRUCT_MP vehSetup
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					GET_VEHICLE_SETUP_MP(veh, vehSetup)
				ENDIF
			ENDIF
			bPrintCurrentVehicleSetup = FALSE
		ENDIF
		IF bPrintSavedVehicleStucts
		
			PRINTLN("[personal_vehicle] ------------- DEBUG PRINT ALL g_MpSavedVehicles DETAILS ---------")
		
			REPEAT MAX_MP_SAVED_VEHICLES i
				
				PRINTLN("[personal_vehicle] ------------- SLOT ", i, " --------------")
				
				NET_PRINT_VEHICLE_SETUP_STRUCT(g_MpSavedVehicles[i].vehicleSetupMP)
				
				PRINTLN("g_MpSavedVehicles[", i, "].iVehicleBS = ", g_MpSavedVehicles[i].iVehicleBS)
				PRINTLN("g_MpSavedVehicles[", i, "].iImpoundTime = ", g_MpSavedVehicles[i].iImpoundTime)
				PRINTLN("g_MpSavedVehicles[", i, "].tl31Destroyer = ", g_MpSavedVehicles[i].tl31Destroyer)
				PRINTLN("g_MpSavedVehicles[", i, "].tlRadioStationName = ", g_MpSavedVehicles[i].tlRadioStationName)
				PRINTLN("g_MpSavedVehicles[", i, "].iPrimaryColourGroup = ", g_MpSavedVehicles[i].iPrimaryColourGroup)
				PRINTLN("g_MpSavedVehicles[", i, "].iSecondaryColourGroup = ", g_MpSavedVehicles[i].iSecondaryColourGroup)
				PRINTLN("g_MpSavedVehicles[", i, "].iPricePaid = ", g_MpSavedVehicles[i].iPricePaid)
				PRINTLN("g_MpSavedVehicles[", i, "].iObtainedTime = ", g_MpSavedVehicles[i].iObtainedTime)
				
			ENDREPEAT
			
			bPrintSavedVehicleStucts = FALSE
		ENDIF
		
		
		
		
		IF bWdEnableAllInteriors
			IF NOT bWdAllInteriorsEnabled
				i=0
				REPEAT ENUM_TO_INT(INTERIOR_MAX_NUM) i
					PRINTLN("Enabling Interior ", i)
					SET_INTERIOR_DISABLED(INT_TO_ENUM(INTERIOR_NAME_ENUM, i), FALSE)
				ENDREPEAT
				bWdAllInteriorsEnabled = TRUE
			ENDIF
		ELSE
			IF bWdAllInteriorsEnabled
				i=0
				REPEAT ENUM_TO_INT(INTERIOR_MAX_NUM) i
					PRINTLN("Disabling Interior ", i)
					SET_INTERIOR_DISABLED(INT_TO_ENUM(INTERIOR_NAME_ENUM, i), TRUE)
				ENDREPEAT
				bWdAllInteriorsEnabled = FALSE
			ENDIF
		ENDIF
		
		IF db_bOutputAllApartmentDetails
			IF db_iOutputPropertiesAll = -1
				db_iOutputPropertiesAll = 1
			ENDIF
			PRINT_APARTMENT_INTERIOR_DETAILS(db_iOutputPropertiesAll)
			db_iOutputPropertiesAll++
			IF db_iOutputPropertiesAll >MAX_MP_PROPERTIES	
				db_bOutputAllApartmentDetails = FALSE
			ENDIF
		ENDIF
		
		IF db_bOutputThisPropertyDetails
			PRINT_FULL_PROPERTY_DETAILS(db_bOutputPropertyID,db_bOutputYachtID)
			db_bOutputThisPropertyDetails = FALSE
		ENDIF
		
		IF db_bTestLBWarning
			IF DO_SIGNED_OUT_WARNING(iTestWarningBS)
				iTestWarningBS = 0
				db_bTestLBWarning = FALSE
			ENDIF
		ENDIF
		VEHICLE_INDEX theVeh //= GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),TRUE)
		IF bWdSetInvalidModDecoratorOnVeh
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				theVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),TRUE)
				IF DOES_ENTITY_EXIST(theVeh)
				AND IS_VEHICLE_DRIVEABLE(theVeh )
				AND DECOR_IS_REGISTERED_AS_TYPE("Veh_Modded_By_Player", DECOR_TYPE_INT)
					PRINTLN("CDM: About to add invalid modded by decorator to vehicle")
					IF NETWORK_HAS_CONTROL_OF_ENTITY(theVeh )
						DECOR_SET_INT(theVeh , "Veh_Modded_By_Player",101)
						PRINTLN("CDM: Adding invalid modded by decorator to vehicle", 101)
					ENDIF
				ENDIF
			ENDIF
			bWdSetInvalidModDecoratorOnVeh = FALSE
		ENDIF
		
		IF db_outputCurrentCarStats
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				theVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),TRUE)
				GENERATE_VEHICLE_STATS_FOR_CURRENT_VEHICLE_INDEX(theVeh,debugVehStats)
				IF debugVehStats.bGotValues = TRUE
					PRINTLN("-----CURRENT VEHICLE STATS------")
					PRINTLN("Model Name: ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(theVeh)))
					PRINTLN("Top speed: ", debugVehStats.fValues[VEHICLE_STAT_TOP_SPEED_INDEX])
					PRINTLN("Braking: ", debugVehStats.fValues[VEHICLE_STAT_BRAKING_INDEX])
					PRINTLN("Acceleration: ", debugVehStats.fValues[VEHICLE_STAT_ACCELERATION_INDEX])
					PRINTLN("Traction: ", debugVehStats.fValues[VEHICLE_STAT_TRACTION_INDEX])
					debugVehStats.bGotValues = FALSE
					db_outputCurrentCarStats = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF db_setMechanicBlockedFlag
			SET_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iGeneralBS,MP_SAVE_GENERAL_BS_NOT_PAID_MECHANIC)
			db_setMechanicBlockedFlag = FALSE
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(g_TransitionSessionNonResetVars.contactRequests.stCDTimer[iDebugContactRequestCooldownStagger])
		AND NOT HAS_NET_TIMER_EXPIRED(	g_TransitionSessionNonResetVars.contactRequests.stCDTimer[iDebugContactRequestCooldownStagger],
										GET_REQUEST_LOCAL_CD_TIME(INT_TO_ENUM(CONTACT_REQUEST_TYPE, iDebugContactRequestCooldownStagger)),TRUE)
			bDebugContactRequestOnCooldown[iDebugContactRequestCooldownStagger] = TRUE
			IF bDebugContactRequestEndCooldown[iDebugContactRequestCooldownStagger]
				RESET_NET_TIMER(g_TransitionSessionNonResetVars.contactRequests.stCDTimer[iDebugContactRequestCooldownStagger])
			ENDIF
		ELSE
			bDebugContactRequestOnCooldown[iDebugContactRequestCooldownStagger] = FALSE
		ENDIF
		bDebugContactRequestEndCooldown[iDebugContactRequestCooldownStagger] = FALSE
		iDebugContactRequestCooldownStagger++
		IF iDebugContactRequestCooldownStagger >= ENUM_TO_INT(CONTACT_REQUEST_MAX)
			iDebugContactRequestCooldownStagger = 0
		ENDIF
		
		IF db_testFindMatchedGamers
			IF FIND_MATCHED_GAMERS()
				CLEAR_MATCHED_GAMERS_STRUCT()
				db_testFindMatchedGamers = FALSE
			ENDIF
		ENDIF
		
		IF db_bPropertyNonProfileSave
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE,iTestPropertyVerificationID)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_TIME,GET_CLOUD_TIME_AS_INT())
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iCurrentPropertyValue = GET_MP_PROPERTY_VALUE(iTestPropertyVerificationID)
			REQUEST_SAVE(STAT_SAVETYPE_SCRIPT_MP_GLOBALS)
			db_bPropertyNonProfileSave = FALSE								
		ENDIF
		
		IF db_bPropertyProfileSave
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_NEW,iTestPropertyVerificationID)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_NEW_TIME,GET_CLOUD_TIME_AS_INT())
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_NEW_VALUE,GET_MP_PROPERTY_VALUE(iTestPropertyVerificationID))
			REQUEST_SAVE(STAT_SAVETYPE_END_SHOPPING)
			db_bPropertyProfileSave = FALSE	
		ENDIF
		
		MP_DOOR_ONE_WAY_DETAILS Details
		IF db_bMCDoorOverride
			GET_MP_DOOR_ONE_WAY_DETAILS(db_iMCDoorID, Details)
			MISSION_CONTROL_NET_DOOR_OVERRIDE(Details.iDoorHash[0],INT_TO_ENUM(MISSION_CONTROL_NET_DOOR_OVERRIDE_ENUM,db_iMCDoorState))
			db_bMCDoorOverride = FALSE
		ENDIF
		
		IF db_bMCDoorClearOverride
			db_bMCDoorOverride = FALSE
			db_bMCDoorClearOverride = FALSE
			CLEAR_ALL_MISSION_CONTROL_NET_DOOR_OVERRIDE()
		ENDIF

		IF g_bWarpToGarage
			IF NOT IS_THREAD_ACTIVE(g_tiImpExpGarage)
				REQUEST_SCRIPT("VehicleSpawning")
				
				IF HAS_SCRIPT_LOADED("VehicleSpawning")
					g_tiImpExpGarage = START_NEW_SCRIPT("VehicleSpawning", DEFAULT_STACK_SIZE)
					g_bWarpToGarage = FALSE
				ENDIF
			ELSE
				g_bWarpToGarage = FALSE
			ENDIF
		ENDIF
		
		UPDATE_VEHICLE_DROP_PERSONAL_WIDGET(VDPersonalTruckData, VDPersonalTruckDebugData)
		
		DEBUG_START_RC_VEHICLE()


	ENDPROC

	//On all machines populate the sequence index with 
	//  tasks (this done on all machines so if the ped migrates 
	//  to another machine while performing the sequence it continues to work properly):
	BOOL bOpenTaskSequence = FALSE
	PROC FILL_SEQUENCE_TASK()
		OPEN_SEQUENCE_TASK(SequenceIndex)
			VECTOR playerCoord = GET_PLAYER_COORDS(PLAYER_ID())
	        TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -43.19, -57.01, 1.0>>, PEDMOVE_SPRINT, 999999, 10.0)
	        TASK_PED_SLIDE_TO_COORD(NULL, << -43.19, -57.01, 1.0>>, 0.0)
	        TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, playerCoord, PEDMOVE_SPRINT, 999999, 10.0)
	        TASK_PED_SLIDE_TO_COORD(NULL, playerCoord, 0.0)
		CLOSE_SEQUENCE_TASK(SequenceIndex)
		bOpenTaskSequence = FALSE
	ENDPROC

	// On the machine in control of the ped you want to do the 
	//   sequence, give the ped a task perform sequence and pass 
	//   in the sequence index from above.
	BOOL bStartTaskSequence = FALSE
	PROC DEBUG_TASK_SEQUENCES()
		// Start sequence if we have control of the focus ped.
		PED_INDEX pedIndex =  NETWORK_GET_FOCUS_PED_LOCAL_ID()
		IF pedIndex <> INT_TO_NATIVE(PED_INDEX, -1)
		AND pedIndex <> NULL
			IF NETWORK_HAS_CONTROL_OF_ENTITY(pedIndex)
				TASK_PERFORM_SEQUENCE(pedIndex, SequenceIndex)
			ENDIF
		ENDIF
		bStartTaskSequence = FALSE
	ENDPROC

	//Add widgets for setting relationship groups
	BOOL bSetToOwnTeam
	PROC ADD_RELATIONSHIP_GROUP_WIDGETS()
		START_WIDGET_GROUP("Player Teams")  				
			ADD_WIDGET_BOOL("On Own Team", bSetToOwnTeam)
		STOP_WIDGET_GROUP()		
	ENDPROC
	
	//Add widgets for temporary testing purposes
	BOOL bTestRappelTask
	BOOL bCreateTestPed
	BOOL bCreateTestVehicle
	BOOL bPlayAnimOnTestPed
	BOOL bTestPedFleeFromPlayer
	BOOL bTestPedFleeFromPoint
	BOOL bTestPedFollowNavMesh
	BOOL bTestPedGoToCoord
	BOOL bTestPedStandStill
	BOOL bTestPedCombatHatedPeds
	BOOL bTestPedGoToCoordAiming
	BOOL bTestPedPerformSequence
	BOOL bTestPedEnterVehicle
	BOOL bTestPedLeaveVehicle
	BOOL bTestPedDriveVehicleToCoord
	BOOL bTestPedDriveVehicleWander
	BOOL bTestPedDriveVehicleMission
	BOOL bClearTestPedTasks
	BOOL bClearTestPedTasksImmediately
	BOOL bDisplayScriptTaskStatus
	BOOL bDebugPeronsalVeh
	
	PROC ADD_TASK_DEBUG_WIDGETS()
	
		START_WIDGET_GROUP("Task Debug Widgets")
			
			ADD_WIDGET_BOOL("g_bForceUpdateActivityBlips", g_bForceUpdateActivityBlips)
			ADD_WIDGET_BOOL("Test Rappel task", bTestRappelTask)
			ADD_WIDGET_INT_SLIDER("Rappel sound start time", cfRAPPEL_LOOP_START_TIME, 0, 10000, 100)
			ADD_WIDGET_BOOL("Create Test Ped", bCreateTestPed)
			ADD_WIDGET_BOOL("Create Test Vehicle", bCreateTestVehicle)
			START_WIDGET_GROUP("Basic Tasks")
				ADD_WIDGET_BOOL("Play Anim On Test Ped", bPlayAnimOnTestPed)
				ADD_WIDGET_BOOL("Make Test Ped Flee From Player", bTestPedFleeFromPlayer)
				ADD_WIDGET_BOOL("Make Test Ped Flee From Point", bTestPedFleeFromPoint)
				ADD_WIDGET_BOOL("Make Test Ped Follow Nav Mesh", bTestPedFollowNavMesh)
				ADD_WIDGET_BOOL("Make Test Ped Go To Coord", bTestPedGoToCoord)
				ADD_WIDGET_BOOL("Make Test Ped Stand Still", bTestPedStandStill)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Combat Tasks")
				ADD_WIDGET_BOOL("Make Test Ped Combat Hated Peds", bTestPedCombatHatedPeds)
				ADD_WIDGET_BOOL("Make Test Ped Go To Coord Aiming", bTestPedGoToCoordAiming)
				ADD_WIDGET_BOOL("Make Test Ped Perform Sequence", bTestPedPerformSequence)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Vehicle Tasks")
				ADD_WIDGET_BOOL("Make Test Ped Enter Vehicle", bTestPedEnterVehicle)
				ADD_WIDGET_BOOL("Make Test Ped Leave Vehicle", bTestPedLeaveVehicle)
				ADD_WIDGET_BOOL("Make Test Drive Vehicle To Coord", bTestPedDriveVehicleToCoord)
				ADD_WIDGET_BOOL("Make Test Ped Drive Vehicle Wander", bTestPedDriveVehicleWander)
				ADD_WIDGET_BOOL("Make Test Ped Follow Vehicle Mission", bTestPedDriveVehicleMission)
			STOP_WIDGET_GROUP()
			ADD_WIDGET_BOOL("Clear test ped tasks", bClearTestPedTasks)
			ADD_WIDGET_BOOL("Clear test ped tasks immediately", bClearTestPedTasksImmediately)
			ADD_WIDGET_BOOL("Display Script Task Status", bDisplayScriptTaskStatus)
		STOP_WIDGET_GROUP()	
	ENDPROC


	//Process temporary test code
	NETWORK_INDEX TestPed
	NETWORK_INDEX TestVehicle
	ROPE_INDEX TestRopeID
	PROC PROCESS_TASK_DEBUG()

		IF bTestRappelTask
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<29.23, 50, 11>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 286)
				TestRopeID = ADD_ROPE(<<30, 50.75, 10.7>>, <<0,90,0>>, 20.0, PHYSICS_ROPE_DEFAULT)
				TASK_RAPPEL_DOWN_WALL(PLAYER_PED_ID(), <<29.23, 50, 12>>, <<30, 50.75, 10.7>>, 2.5, TestRopeID)
				bTestRappelTask = FALSE
			ENDIF
		ENDIF

		IF DOES_ROPE_EXIST(TestRopeID)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SCRIPTTASKSTATUS iStatus = GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_RAPPEL_DOWN_WALL)
				IF iStatus != WAITING_TO_START_TASK AND iStatus != PERFORMING_TASK
					DELETE_ROPE(TestRopeID)
				ENDIF
			ENDIF
		ENDIF

		IF bCreateTestPed
			IF REQUEST_LOAD_MODEL(A_M_Y_METHHEAD_01)
				IF CREATE_NET_PED(TestPed, PEDTYPE_CIVMALE, A_M_Y_METHHEAD_01, <<14,-12,1>>, 0, FALSE)
					bCreateTestPed = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF bCreateTestVehicle
			IF REQUEST_LOAD_MODEL(TAILGATER)
				IF CREATE_NET_VEHICLE(TestVehicle, TAILGATER, <<6,-22,1>>, 0, FALSE)
					bCreateTestVehicle = FALSE
				ENDIF
			ENDIF
		ENDIF

		IF NETWORK_DOES_NETWORK_ID_EXIST(TestPed)
			IF NOT IS_ENTITY_DEAD(NET_TO_PED(TestPed))
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_PED(TestPed))
					IF NOT HAS_PED_GOT_WEAPON(NET_TO_PED(TestPed), WEAPONTYPE_PISTOL)
						GIVE_WEAPON_TO_PED(NET_TO_PED(TestPed), WEAPONTYPE_PISTOL, -1, FALSE)
					ENDIF
					SET_CURRENT_PED_WEAPON(NET_TO_PED(TestPed), WEAPONTYPE_PISTOL, TRUE)
				ENDIF
			
				IF bTestPedFleeFromPlayer
					TASK_SMART_FLEE_PED(NET_TO_PED(TestPed), PLAYER_PED_ID(), 100.0, 5000)
					bTestPedFleeFromPlayer = FALSE
				ENDIF
				
				IF bTestPedFleeFromPoint
					TASK_SMART_FLEE_COORD(NET_TO_PED(TestPed), <<14,-12,1>>, 100.0, 5000)
					bTestPedFleeFromPoint = FALSE
				ENDIF
				
				IF bTestPedFollowNavMesh
					TASK_FOLLOW_NAV_MESH_TO_COORD(NET_TO_PED(TestPed), <<14,-12,1>>, PEDMOVE_WALK)
					bTestPedFollowNavMesh = FALSE
				ENDIF
				IF bTestPedGoToCoord
					TASK_GO_STRAIGHT_TO_COORD(NET_TO_PED(TestPed), <<14,-12,1>>, PEDMOVE_WALK)
					bTestPedGoToCoord = FALSE
				ENDIF
				IF bTestPedStandStill
					TASK_STAND_STILL(NET_TO_PED(TestPed), 5000)
					bTestPedStandStill = FALSE
				ENDIF
				
				IF bTestPedCombatHatedPeds
					SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(TestPed), RELGROUPHASH_HATES_PLAYER)
					SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), RELGROUPHASH_PLAYER)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NET_TO_PED(TestPed), 25.0)
					bTestPedCombatHatedPeds = FALSE
				ENDIF
				IF bTestPedGoToCoordAiming
					TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NET_TO_PED(TestPed), <<14,-12,1>>, <<14,-12,1>>, PEDMOVE_WALK, FALSE)
					bTestPedGoToCoordAiming = FALSE
				ENDIF
				IF bTestPedPerformSequence
					TASK_PERFORM_SEQUENCE(NET_TO_PED(TestPed), SequenceIndex)
					bTestPedPerformSequence = FALSE
				ENDIF
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(TestVehicle)
					IF NOT IS_ENTITY_DEAD(NET_TO_VEH(TestVehicle))
						IF bTestPedEnterVehicle
							TASK_ENTER_VEHICLE(NET_TO_PED(TestPed), NET_TO_VEH(TestVehicle))
							bTestPedEnterVehicle = FALSE
						ENDIF
						IF bTestPedLeaveVehicle
							TASK_LEAVE_ANY_VEHICLE(NET_TO_PED(TestPed))
							bTestPedLeaveVehicle = FALSE
						ENDIF
						IF bTestPedDriveVehicleToCoord
							TASK_VEHICLE_DRIVE_TO_COORD(NET_TO_PED(TestPed),
														NET_TO_VEH(TestVehicle),
														<<6,-22,1>>,
														12.0,
														DRIVINGSTYLE_NORMAL,
														GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(NET_TO_PED(TestPed))),
														DRIVINGMODE_AVOIDCARS,
														1,
														1)
							bTestPedDriveVehicleToCoord = FALSE
						ENDIF
						IF bTestPedDriveVehicleWander
							TASK_VEHICLE_DRIVE_WANDER(NET_TO_PED(TestPed),
													  NET_TO_VEH(TestVehicle),
													  12.0,
													  DRIVINGMODE_AVOIDCARS)
							bTestPedDriveVehicleWander = FALSE
						ENDIF
						IF bTestPedDriveVehicleMission
							TASK_VEHICLE_MISSION_COORS_TARGET(NET_TO_PED(TestPed),
															  NET_TO_VEH(TestVehicle),
															  <<6,-22,1>>,
															  MISSION_GOTO,
															  10.0,
															  DRIVINGMODE_AVOIDCARS,
															  -1,
															  -1)
							bTestPedDriveVehicleMission = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF bClearTestPedTasks
			IF NETWORK_DOES_NETWORK_ID_EXIST(TestPed)
				IF NOT IS_ENTITY_DEAD(NET_TO_PED(TestPed))
					CLEAR_PED_TASKS(NET_TO_PED(TestPed))
					SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(TestPed), RELGROUPHASH_PLAYER)
					SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), rgFM_Team[NATIVE_TO_INT(PLAYER_ID())])	//rgFM_DefaultPlayer)
					bClearTestPedTasks = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF bClearTestPedTasksImmediately
			IF NETWORK_DOES_NETWORK_ID_EXIST(TestPed)
				IF NOT IS_ENTITY_DEAD(NET_TO_PED(TestPed))
					CLEAR_PED_TASKS_IMMEDIATELY(NET_TO_PED(TestPed))
					SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(TestPed), RELGROUPHASH_PLAYER)
					SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), rgFM_Team[NATIVE_TO_INT(PLAYER_ID())])	//rgFM_DefaultPlayer)
					bClearTestPedTasksImmediately = FALSE
				ENDIF
			ENDIF
		ENDIF

		IF bDisplayScriptTaskStatus
			IF NETWORK_DOES_NETWORK_ID_EXIST(TestPed)
				IF NOT IS_ENTITY_DEAD(NET_TO_PED(TestPed))
					SCRIPTTASKSTATUS iStatus = GET_SCRIPT_TASK_STATUS(NET_TO_PED(TestPed), SCRIPT_TASK_PLAY_ANIM)
					IF iStatus = WAITING_TO_START_TASK
						NET_PRINT("TASK WAITING TO START") NET_NL()
					ELSE 
						IF iStatus = PERFORMING_TASK
							NET_PRINT("TASK RUNNING") NET_NL()
						ELSE
							NET_PRINT("TASK NOT RUNNING") NET_NL()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bDebugPeronsalVeh = TRUE
			IF g_doorInteractionsVehicle != GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				g_doorInteractionsVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			ENDIF
			bDebugPeronsalVeh = FALSE
		ENDIF
	ENDPROC
	
	PROC CREATE_ASSOCIATED_WANTED_LEVEL_WIDGETS()
		START_WIDGET_GROUP("Associated Wanted Level")
			ADD_WIDGET_BOOL("Do Debug Output", g_DoAssociatedWantedLevelPrints)
		STOP_WIDGET_GROUP()
	ENDPROC
	INT iGET_BUSINESSCallDelay = -1
	PROC CREATE_TEST_SCRIPT_WIDGETS()
		IF NOT DOES_WIDGET_GROUP_EXIST(missionTab)
			IF (GET_COMMANDLINE_PARAM_EXISTS("sc_CreateFreemodeWidgets"))
			OR (GET_COMMANDLINE_PARAM_EXISTS("sc_MechanicApartmentFeeTime"))
				bCREATE_FM_WIDGETS = TRUE
			ENDIF
			
			missionTab = START_WIDGET_GROUP(tl31MissionName)  
			ADD_WIDGET_BOOL("Create Freemode Widgets ", bCREATE_FM_WIDGETS)
			
			#IF SCRIPT_PROFILER_ACTIVE
			CREATE_SCRIPT_PROFILER_WIDGET()
			#ENDIF
			
			STOP_WIDGET_GROUP()
		ENDIF
	ENDPROC
	
	
	PROC CREATE_TEST_SCRIPT_WIDGETS_PROPER()
	
		INT iPlayer
		TEXT_LABEL_63 tl63
		INT i
		

		IF DOES_WIDGET_GROUP_EXIST(missionTab)

			fCheatPlayerLevel = GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT)
 	
			
			SET_CURRENT_WIDGET_GROUP(missionTab)

			
				#IF IS_DEBUG_BUILD

				
				ADD_WIDGET_BOOL("Start Corona Clone Outfit Testing Script", bStartCoronaCloneScript)
				
				ADD_WIDGET_BOOL("PI bKill", g_KillPiMenu.bKill)
				ADD_WIDGET_BOOL("Run Good Boy Earn", bRunGoodBoyEarn)
				
				ADD_WIDGET_BOOL(" LowFlow Debug", g_bFmLowFlowDebug)
				
				ADD_WIDGET_BOOL(" TURN_ON_HA", g_sMPTunables.bTURN_ON_HALLOWEEN_JOBS)
				ADD_WIDGET_BOOL(" TURN_ON_TH", g_sMPTunables.bTURN_ON_TH)
				ADD_WIDGET_BOOL(" RED_INVITE", g_sMPTunables.bRED_INVITE)
				ADD_WIDGET_BOOL(" COME_OUT_TO_PLAY", g_sMPTunables.bDisableRedInvite_HUNT_DARK)
				
				ADD_WIDGET_INT_SLIDER("iEventInviteMessageDebug", 	g_FM_EVENT_INVITE_VARS.iEventInviteMessageDebug , -1, 100, 1)		
				ADD_WIDGET_INT_SLIDER("GET_BUSINESS call delay", 	iGET_BUSINESSCallDelay , -1, 2880000, 1)		
				
				
				#ENDIF

				
				CREATE_ASSOCIATED_WANTED_LEVEL_WIDGETS()
				
				START_WIDGET_GROUP("Adversary Reminder Invite Test")
					ADD_WIDGET_BOOL("g_bSentAdversaryModeReminder", g_bSentAdversaryModeReminder)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Round And Match Widgets")
					ADD_WIDGET_BOOL("Force Round", g_bFakeRound)
					ADD_WIDGET_BOOL("Force Match", g_bFakeEndOfMatch)
				STOP_WIDGET_GROUP()
				
				ADD_WIDGET_BOOL("bTestRougeWait", bTestRougeWait)
				ADD_WIDGET_BOOL( "Set Player On Own Team", bSetPlayerOnOwnTeam)
				twID = ADD_TEXT_WIDGET("PLAYER NAME")
				ADD_WIDGET_BOOL( "GET XUIX", bGetXUID)
				ADD_WIDGET_BOOL( "bSetUpUsjs", bSetUpUsjs)
				ADD_WIDGET_BOOL("MPGlobalsAmbience.bLaunchBackUpHeli", MPGlobalsAmbience.bLaunchBackUpHeli)
				ADD_WIDGET_BOOL("bLeaderboardCamRenderingOrInterping", g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderboardCamRenderingOrInterping)
				ADD_WIDGET_BOOL("g_bLeaderboardCamExtraDebugPrints", g_bLeaderboardCamExtraDebugPrints)
				ADD_WIDGET_BOOL("bTestFlashingTags", bTestFlashingTags)
				ADD_WIDGET_BOOL("MPGlobals.bDoCreateSurvivalRewardVehicles", MPGlobals.bDoCreateSurvivalRewardVehicles)
				ADD_WIDGET_BOOL("bForceThroughCleanup", bForceThroughCleanup)
				ADD_WIDGET_BOOL("bDrawBigLeaderboard", bDrawBigLeaderboard)
				ADD_WIDGET_BOOL("bAmLauncherActive",bAmLauncherActive)
				ADD_WIDGET_BOOL("bFerrisWheelActive",bFerrisWheelActive)
				ADD_WIDGET_BOOL("bDailyObjectivesActive",bDailyObjectivesActive)
				ADD_WIDGET_BOOL("Launch Gang Boss Steal Veh Class", bGbStealVehicleClassTaskActive)
				ADD_WIDGET_BOOL("Launch Gang Boss Point 2 Point", bGbStealP2pTaskActive)

				ADD_WIDGET_BOOL("Launch Gang Boss DM", bGbDeathmatchActive)
				ADD_WIDGET_BOOL("Launch Gang Boss ", bGbBossAttackActive)

				START_WIDGET_GROUP("    ExecutiveCS ")
					ADD_WIDGET_BOOL("g_bFakeAirDrop", g_bFakeAirDrop)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("    Kills cap ")
					ADD_WIDGET_INT_READ_ONLY("g_i_RPKillsTotalThisJob", g_i_RPKillsTotalThisJob)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Archenemy")
					ADD_WIDGET_BOOL("g_b_DisplayedArchenemyInSessionFeed ", g_b_DisplayedArchenemyInSessionFeed)
					ADD_WIDGET_INT_SLIDER("Archenemy Player ", g_i_ArchenemyPlayer, -1, 100, 1)		
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("     LAST DAMAGER")
					ADD_WIDGET_INT_READ_ONLY("g_iLastDamagerPlayer", g_iLastDamagerPlayer)
					ADD_WIDGET_INT_READ_ONLY("g_iMyAmountOfKills", g_iMyAmountOfKills)
					ADD_WIDGET_INT_READ_ONLY("g_iMyDMScore", g_iMyDMScore)
					ADD_WIDGET_INT_READ_ONLY("g_iMyAmountOfDeaths", g_iMyAmountOfDeaths)
					ADD_WIDGET_INT_SLIDER("g_iMyAssists ", g_iMyAssists, 0, 100, 1)		
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Specator Camera") 
					ADD_WIDGET_BOOL("g_bUpdateSpectatorPosition", g_bUpdateSpectatorPosition)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP(" g_i_ActualLeaderboardPlayerSelected ")
					ADD_WIDGET_INT_SLIDER("g_i_ActualLeaderboardPlayerSelected", g_i_ActualLeaderboardPlayerSelected, -1, 9999, 1)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP(" Dpad LBD ")
					ADD_WIDGET_INT_READ_ONLY("g_i_NumPlayersForLbd", g_i_NumPlayersForLbd)
					ADD_WIDGET_BOOL("g_dPadSecondPage", g_dPadSecondPage)
					ADD_WIDGET_BOOL("g_DebugDrawDpadLbd", g_DebugDrawDpadLbd)
					ADD_WIDGET_INT_SLIDER("g_i_NumDpadLbdPlayers", g_i_NumDpadLbdPlayers, 0, 100, 1)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Executive Widgets")
					ADD_WIDGET_BOOL("Give player Office",db_bSetOwnsOffice)
					ADD_WIDGET_BOOL("Remove player Office",db_bRemovePlayerOffice)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Events")
					ADD_WIDGET_BOOL("g_bPrintScriptEventDetails", g_bPrintScriptEventDetails)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("     NJVS")
					ADD_WIDGET_BOOL("g_NJVS_Spam", g_NJVS_Spam)
					ADD_WIDGET_BOOL("g_SkipCelebAndLbd", g_SkipCelebAndLbd)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("          Mask Widgets")
					ADD_WIDGET_BOOL("MaskDebugOn", g_b_MaskDebug)
					ADD_WIDGET_INT_SLIDER("Anim To Play",  g_i_MaskAnimToPlay, -1, 14, 1)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("           Luke Widgets")
			
					ADD_WIDGET_BOOL("Hold Intro", g_DM_Intro_Hold)
					ADD_WIDGET_BOOL("g_b_FailCams", g_b_FailCams)
					ADD_WIDGET_BOOL("Restart Intro", g_DM_Reset)
					ADD_WIDGET_BOOL("Anim Name Onscreen", g_DM_Intro_Name_Anim)
					ADD_WIDGET_BOOL("g_b_CheckpointAnims", g_b_CheckpointAnims)
					
					ADD_WIDGET_BOOL("g_b_JobIntroOpeningCut", g_b_JobIntroOpeningCut)
					
					ADD_WIDGET_INT_SLIDER("Anim To Play",  g_i_AnimToPlay, -1, 100, 1)
					ADD_WIDGET_INT_SLIDER("Character",  g_i_AnimChar, -1, 100000, 1)
					ADD_WIDGET_INT_SLIDER("Duration",  g_i_Duration, -1, 100000, 1)
					ADD_WIDGET_FLOAT_SLIDER("camFOV",  g_f_FOV, 0.0, 100000, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("g_f_CargoX",  g_f_CargoX, 0.0, 100000, 0.01)
					
					START_WIDGET_GROUP("Deathmatch/Versus")
						START_WIDGET_GROUP("Old Cams")
							ADD_WIDGET_BOOL("Save Cam Choice", g_b_SaveOldCam)
							ADD_WIDGET_BOOL("Save Vs Choice", g_b_SaveVsCam)
							ADD_WIDGET_INT_SLIDER("Camera Choice ",  g_i_OldCamNumber, -1, (ciMAX_DM_CAMS -1), 1)
						STOP_WIDGET_GROUP()
					STOP_WIDGET_GROUP()
					

					START_WIDGET_GROUP("Cam1")
						ADD_WIDGET_BOOL("Save Cam1", g_b_SaveCam1)
						ADD_WIDGET_VECTOR_SLIDER("g_v_Cam1", g_v_Cam1, -99999, 99999, 0.001)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Cam1Point")
						ADD_WIDGET_VECTOR_SLIDER("g_v_CamRot1", g_v_CamRot1, -99999, 99999, 0.001)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Cam2")
						ADD_WIDGET_BOOL("Save Cam2", g_b_SaveCam2)
						ADD_WIDGET_VECTOR_SLIDER("g_v_Cam2", g_v_Cam2, -99999, 99999, 0.001)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Cam2Point")
						ADD_WIDGET_VECTOR_SLIDER("g_v_CamRot2", g_v_CamRot2, -99999, 99999, 0.001)
					STOP_WIDGET_GROUP()
					ADD_WIDGET_BOOL("Reset All cams", g_b_ResetAllcams)
					ADD_WIDGET_BOOL("Output", g_b_OutPut)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP(" DM_INTRO")
					ADD_WIDGET_BOOL("Hold", g_DM_Intro_Hold)
					ADD_WIDGET_BOOL("Restart", g_DM_Reset)
					ADD_WIDGET_BOOL("Print Anim Name", g_DM_Intro_Name_Anim)
					ADD_WIDGET_INT_SLIDER("Anim To Play",  g_i_AnimToPlay, -1, 100, 1)
					ADD_WIDGET_INT_SLIDER("g_i_AnimTime",  g_i_AnimTime, -1, 100000, 1)
					ADD_WIDGET_INT_SLIDER("g_i_AnimChar",  g_i_AnimChar, -1, 100000, 1)
					ADD_WIDGET_FLOAT_SLIDER("fPlayAnimFrom",  fPlayAnimFrom, 0.0, 1.0, 0.1)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("           Cloud Widgets")
					ADD_WIDGET_FLOAT_READ_ONLY("fCloudAlpha", g_TransitionSessionNonResetVars.sCloudData.fAlpha)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP( "Les Widgets")  
				
					START_WIDGET_GROUP("MISSION DIFFICULTY")	
						ADD_WIDGET_FLOAT_SLIDER("Mission health override Multi",  lw_fSetMissPedHealth, 0.1, 10, 0.1)
						ADD_WIDGET_FLOAT_SLIDER("Mission accuracy override Multi",  lw_fSetMissPedAccuracy, 0.1, 10, 0.1)
						ADD_WIDGET_INT_SLIDER("Mission enemy respawns",  lw_iEnemyRespawns, 1, 10, 1)
						ADD_WIDGET_BOOL("Update Mission Override", lw_bSetMissionDiffModifier)
					STOP_WIDGET_GROUP()

					START_WIDGET_GROUP("Damage Modifiers")
						ADD_WIDGET_FLOAT_SLIDER("Player Weapon Damage Modifier",  lw_fPlayerWeaponDamageModifier, 0.0, 100.0, 0.05)
						ADD_WIDGET_BOOL( "Set Player Weapon Damage Modifier", lw_bSetPlayerWeaponDamageModifier)
						ADD_WIDGET_FLOAT_SLIDER("Player Vehicle Damage Modifier",  lw_fPlayerVehicleDamageModifier, 0.0, 100.0, 0.05)
						ADD_WIDGET_BOOL( "Set Player Vehicle Damage Modifier", lw_bSetPlayerVehicleDamageModifier)
						ADD_WIDGET_INT_SLIDER("Player Starting Health Modifier", g_StartingHealthChange, -10000, 10000, 1)
						ADD_WIDGET_BOOL("Set Players Starting Health", g_UpdatePlayersStartingHealth)
						ADD_WIDGET_FLOAT_SLIDER("Ai Damage Modifier",  lw_fAiDamageModifier, 0.0, 100.0, 0.05)
						ADD_WIDGET_BOOL( "Ai Damage Modifier", lw_bSetAiDamageModifier)
						
						START_WIDGET_GROUP("Server")
							ADD_WIDGET_FLOAT_SLIDER("Server Weapon Damage Modifier",  GlobalServerBD.lw_fServerWeaponDamageModifier, 0.0, 100.0, 0.0)
							ADD_WIDGET_FLOAT_SLIDER("Server Vehicle Damage Modifier",  GlobalServerBD.lw_fServerVehicleDamageModifier, 0.0, 100.0, 0.0)
						STOP_WIDGET_GROUP()
						
						START_WIDGET_GROUP("TURN ON LOOTING")
							ADD_WIDGET_BOOL("TURN ON LOOTING", pedsstoredforlooting.b_looting_turn_on)		
						STOP_WIDGET_GROUP()
						
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Radar Zoom ")
						ADD_WIDGET_FLOAT_SLIDER("Radar Zoom ",  f_i_RadarZoom, -1.0, 5000.0, 1.0)
						ADD_WIDGET_FLOAT_SLIDER("Radar Zoom CAP ",  cfRADAR_ZOOM_CAP, -1.0, 5000.0, 1.0)
						ADD_WIDGET_FLOAT_SLIDER("cfRADAR_ZOOM_CAP_MIN ",  cfRADAR_ZOOM_CAP_MIN, -1.0, 5000.0, 1.0)
						ADD_WIDGET_BOOL("g_b_Do_RadarZoom", g_b_Do_RadarZoom)
						ADD_WIDGET_BOOL("g_b_CodeZoom", g_b_CodeZoom)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Cheater Rating")
						ADD_WIDGET_INT_SLIDER("Code Cheater Threshold", iCheatLevelThreshold, 0, 5000, 1)
						ADD_WIDGET_INT_SLIDER("Code Cheater Forgive Threshold", iNotCheatLevelThreshold, 0, 5000, 1)

						ADD_WIDGET_BOOL("SCRIPT AND CODE THINK I'M A CHEAT", bCheatLevelAmIaCheatLesWidget)
						ADD_WIDGET_FLOAT_READ_ONLY("Players Cheat Level", fCheatPlayerLevel)
						ADD_WIDGET_INT_READ_ONLY("Times Been a Cheater", iCheatTimeBeenACheaterWidget)
						ADD_WIDGET_BOOL("Was I a Cheater ", iWasIaCheaterWidget)
	//					
						ADD_WIDGET_BOOL("Override Code Cheat", bCheatOverrideCheatLevels)
						ADD_WIDGET_INT_SLIDER("Override Code Cheat Value", bCheatOverrideCheatNumber, 0, 200, 1)

	//					
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("BadSport Rating")
						ADD_WIDGET_BOOL("g_bTurnOnBadSportWidgets", g_bTurnOnBadSportWidgets)
						ADD_WIDGET_INT_SLIDER("Change Character BadSport Rating", g_iBadSportLevelIncreaseLesWidget, -10000, 10000, 1)
						ADD_WIDGET_INT_SLIDER("Code BadSporter Threshold", iBadSportLevelThreshold, 0, 5000, 1)
						ADD_WIDGET_INT_SLIDER("Code BadSporter Forgive Threshold", iNotBadSportLevelThreshold, 0, 5000, 1)
						ADD_WIDGET_FLOAT_SLIDER("Code BadSporter Forgive Level", fBadSportForgiveLevel, 0, 5000, 0.001)
						START_WIDGET_GROUP ("Became a Bad Sport" )
							ADD_WIDGET_DATE_READ_ONLY("Became ", sBadSportDateBecameBadSportLesWidget)
						STOP_WIDGET_GROUP()
						START_WIDGET_GROUP ("Bad Sport Forgiven Date" )
							ADD_WIDGET_DATE_READ_ONLY("Forgiven ", sBadSportDateToBeForgivenWidget)
						STOP_WIDGET_GROUP()
						ADD_WIDGET_BOOL("SCRIPT AND CODE THINK I'M A BadSport", bBadSportLevelAmIaBadSportLesWidget)
						ADD_WIDGET_BOOL("Trigger BadSport Phonecall and Broadcast", g_iBadSportPlayPhoneCall)
						ADD_WIDGET_FLOAT_READ_ONLY("Players BadSport Level", fBadSportPlayerLevel)
						ADD_WIDGET_BOOL("Reset Players BadSport Level", bBadSportPlayerReset)
						ADD_WIDGET_INT_READ_ONLY("Times Been a Bad sport", iBadSportTimeBeenABadSportWidget)
						ADD_WIDGET_BOOL("Was I a Bad Sport ", iWasIaBadSportWidget)
						
		
					STOP_WIDGET_GROUP()

					START_WIDGET_GROUP("AUTOMUTE Count")
						ADD_WIDGET_BOOL("g_bTurnOnBadSportWidgets", g_bTurnOnBadSportWidgets)
						ADD_WIDGET_INT_SLIDER("Change People Muted you", iBadSportMuteCount_Widget, -100, 100, 1)
						ADD_WIDGET_INT_SLIDER("Change Talkers Met", iBadSportTalkerCount_Widget, -100, 100, 1)
						
						ADD_WIDGET_INT_READ_ONLY("Current Mute Count" , iBadSportMuteCount)
						ADD_WIDGET_INT_READ_ONLY("Current Talkers Met Count" , iBadSportTalkerCount)
					
					STOP_WIDGET_GROUP()

				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP( "MP Intro Cut")
					ADD_WIDGET_BOOL("Launch MP Intro Cut", bWdLaunchMpIntroCut)
					ADD_WIDGET_BOOL("Has Launched MP Intro Cut", bWdHasLaunchedMpIntroCut)
					ADD_WIDGET_BOOL("INTRO SCRIPT LAUNCHED", bWdIntroScriptStarted)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Delete Character 0 Headshot")
					ADD_WIDGET_BOOL("Delete Character 0 Headshot", bWdDeleteCharacter0Headshot)
					ADD_WIDGET_BOOL("Reset Delete Character 0 Headshot", bWdResetDeleteCharacter0Headshot)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("OVER HEAD")					
					
					ADD_WIDGET_INT_READ_ONLY("Set Up OHD Bitset", g_sOverHead.iOverHeadSetUp)
					
					START_WIDGET_GROUP("OHD Inventory")	
						i = 0
						TEXT_LABEL_31 tl31
						FOR i = 0 to (NUM_NETWORK_PLAYERS - 1) 
							tl31 = "on["
							tl31 += i 
							tl31 += "]" 
							ADD_WIDGET_BOOL(tl31, GlobalplayerBD[i].bRunOverHeadinventory)
							tl31 = "bitset ["
							tl31 += i 
							tl31 += "]" 
							ADD_WIDGET_INT_READ_ONLY(tl31, GlobalplayerBD[i].iMyOverHeadInventory)
						ENDFOR
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("OHD Player ID")	
						FOR i = 0 to (NUM_NETWORK_PLAYERS - 1) 
							tl31 = "Player ["
							tl31 += i 
							tl31 += "]" 
							ADD_WIDGET_INT_READ_ONLY(tl31, g_sOverHead.iOverHeadPlayerNumber[i])
						ENDFOR
					STOP_WIDGET_GROUP()
					
				
			
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("GIVE REWARDS")	
					ADD_WIDGET_BOOL("Give player Contraband reward ammo", bWdGiverewardammotoplayer)
					ADD_WIDGET_BOOL("Give player reward  T SHIRT CROSS THE LINE", bWdGiverewardtshirtcrosstheline)
					
				STOP_WIDGET_GROUP()
			
				
				CREATE_MP_AMBIENT_MANAGER_WIDGETS()
				
				ADD_JOYRIDER_LAUNCHING_WIDGETS()
				
				CREATE_HELI_GUN_WIDGETS()
				
				SETUP_REWARD_WIDGETS()
				
				SETUP_KICKING_WIDGETS(iNumberOfVotesToKickPlayer)
				
				CREATE_HEAVILY_ACCESSED_STAT_WIDGET()
				
				CREATE_CELEBRATION_SCREEN_WIDGETS()
				
				CREATE_TUG_BOAT_PED_PLACEMENT_WIDGETS(sTugBoatPedPlacementWidgetData)
				
				// Gameplay debug, sever only.
				START_WIDGET_GROUP("Server Only Gameplay") 
					ADD_WIDGET_BOOL("All: Swap Teams", GlobalServerBD_FM.bSwapTeams)
					ADD_WIDGET_BOOL("All: End Match", bHostEndGameNow)
					ADD_WIDGET_BOOL("All: End Session", bHostAllLeave)
					ADD_WIDGET_BOOL("bHostDoNotEndGame", bHostDoNotEndGame)			
				STOP_WIDGET_GROUP()	
				
				// Data about the server
				START_WIDGET_GROUP("Server BD") 
						ADD_WIDGET_INT_SLIDER("S. Game state", GlobalServerBD.iServerGameState,-1, HIGHEST_INT,1)
				STOP_WIDGET_GROUP()	
				
				START_WIDGET_GROUP("Test functionality")
					ADD_WIDGET_BOOL("Test warp to interior", bTestWarpToInterior)
					ADD_WIDGET_BOOL("bEnableMinigameCheck", bEnableMinigameCheck)
					ADD_WIDGET_BOOL("d_bForceDebugSVMSeatsNoDriver", g_OfficeHeliDockData.d_bForceDebugSVMSeatsNoDriver)
					ADD_WIDGET_BOOL("d_bEveryoneUsesAnyPassenger", g_OfficeHeliDockData.d_bEveryoneUsesAnyPassenger)
				STOP_WIDGET_GROUP()
							
				// Data about the clients. * = You.
				START_WIDGET_GROUP("Client BD")  				
					REPEAT NUM_NETWORK_PLAYERS iPlayer
						tl63 = "Player "
						tl63 += iPlayer
						IF iPlayer = PARTICIPANT_ID_TO_INT()
							tl63 += "*"
						ENDIF
						START_WIDGET_GROUP(tl63)
							ADD_WIDGET_INT_SLIDER("Game state", GlobalplayerBD[iPlayer].iGameState,-1, HIGHEST_INT,1)
													
							START_WIDGET_GROUP("Weapon swapping")
								ADD_WIDGET_INT_READ_ONLY("State", GlobalplayerBD[iPlayer].weaponSwappingData.iState)
								ADD_WIDGET_INT_READ_ONLY("Part interacting with", GlobalplayerBD[iPlayer].weaponSwappingData.iPlayerInteractingWith)
								ADD_WIDGET_INT_READ_ONLY("Ammo", GlobalplayerBD[iPlayer].weaponSwappingData.iAmmo)
							STOP_WIDGET_GROUP()
						STOP_WIDGET_GROUP()
					ENDREPEAT
					
				STOP_WIDGET_GROUP()
								
				START_WIDGET_GROUP("Debug Task Sequence")
					ADD_WIDGET_BOOL("Open", bOpenTaskSequence)
					ADD_WIDGET_BOOL("Start", bStartTaskSequence)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Non mission help")
					ADD_WIDGET_BOOL("Do non-mission help debug", bWdDoNmhDebug)
					ADD_WIDGET_BOOL("Seem in stolen", bWdSeenInStDebug)
					ADD_WIDGET_BOOL("Do Lectro debug", bWdDoLectDebug)
					ADD_WIDGET_BOOL("Clear replay job stat", bWdClearReplayJobHelpStat)
					ADD_WIDGET_BOOL("Clear tracker help", bWdClearTrackerHelpStat)
					ADD_WIDGET_BOOL("Run SET_FM_UNLOCKS", bWdRunSetFmUnlocks)
					
					ADD_WIDGET_BOOL("unlock up to current rank", bWdUnlockUpToCurrentRank)
					ADD_WIDGET_BOOL("Strip Club Debug", bWdDoStripclubDebug)
					ADD_WIDGET_BOOL("Ignore redisplay check", bWdIgnoreRedisplayCheck)
					ADD_WIDGET_BOOL("Skip rank time", unlockFmPhoneCall.bSkipRankUnlockTime)
					ADD_WIDGET_BOOL("g_ShouldShiftingTutorialsBeSkipped", g_ShouldShiftingTutorialsBeSkipped)
					ADD_WIDGET_BOOL("g_bPlayerDebugChangedRank", g_bPlayerDebugChangedRank)
					ADD_WIDGET_BOOL("Print Help", bWdPrintNmh)
					ADD_WIDGET_BOOL("Print Objective", bWdPrintObjective)
					ADD_WIDGET_BOOL("Force Print",bWdForcePrintNMH)
					ADD_WIDGET_BOOL("Clear NMH stats", bWdClearNMHStats)
					ADD_WIDGET_BOOL("Set bike at pos", bWdSetBikeAtPos)
					ADD_WIDGET_BOOL("Show lock on state", bWdShowLockOnState)
					ADD_WIDGET_BOOL("g_cancelJobButtonOnScreen? ", g_cancelJobButtonOnScreen)
					ADD_WIDGET_INT_SLIDER("Lock on state", iWdLockOnState, 0 , 100, 1)
					START_WIDGET_GROUP("Text Message")
						START_NEW_WIDGET_COMBO()
							ADD_TO_WIDGET_COMBO("Lamar")
							ADD_TO_WIDGET_COMBO("Simeon")
							ADD_TO_WIDGET_COMBO("Merryweather")
							ADD_TO_WIDGET_COMBO("Dom")
							ADD_TO_WIDGET_COMBO("Lester")
						STOP_WIDGET_COMBO( "Contact", iWdTextMessChar )

						ADD_WIDGET_BOOL("Send Text", bWdSendTextMessage)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Player male Check")
						ADD_WIDGET_BOOL("Check male", bWdCheckPlayerMale)
						ADD_WIDGET_BOOL("Male", bWdIsMale)
						ADD_WIDGET_BOOL("Female", bWdIsFemale)
					STOP_WIDGET_GROUP()
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("FM Tutorials")
					ADD_WIDGET_BOOL("Run SET_FM_UNLOCKS", bWdRunSetFmUnlocks)
					ADD_WIDGET_BOOL("Run ambient tutorial check", bWdRunAbmientTutorialCheck)
					ADD_WIDGET_BOOL("lw_bUnlockAllShopItems set?", lw_bUnlockAllShopItems)
					ADD_WIDGET_BOOL("g_SkipFmTutorials", g_SkipFmTutorials)
					ADD_WIDGET_BOOL("Set intro done", bWdSetIntroDOne)
					ADD_WIDGET_BOOL("Set Race/Mission tut done", bWdSetTriggerTutDone)
					ADD_WIDGET_BOOL("Set chose no tut", bWdSetCHoseNoTut)
					ADD_WIDGET_BOOL("Tutorials complete?", g_bTutorialCompleteCheck)
					ADD_WIDGET_BOOL("Run Import/export", bWdLaunchImportExportNow)
					
					ADD_WIDGET_BOOL("Get Car pos", bWdGetCarPos)
					
					START_WIDGET_GROUP("Car colours")
						ADD_WIDGET_BOOL("Get car colours", bWdGetCarColours)
						ADD_WIDGET_INT_SLIDER("Colour comb", iWdColourComb, 0, 100, 1)
						ADD_WIDGET_BOOL("Set colour comb", bWdNextColourComb)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Lester No cops")
						ADD_WIDGET_INT_SLIDER("Duration ", MAX_TIME_COPS_TURN_BLIND_EYE, 1000, 10000000, 1000)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Unlocks")
						ADD_WIDGET_BOOL("Ignore recent comms unlock check",unlockFmPhoneCall.bIgnoreRecentCallCheck)
	#IF ADD_WIDGETS_FOR_REMINDER_TIMES
						ADD_WIDGET_INT_SLIDER("Min time between calls", MIN_TIME_BEWTEEN_UNLOCK_CALLS, 1000, 1000000, 1)
						ADD_WIDGET_INT_SLIDER("Min time vehicle texts", MIN_TIME_BETWEEN_VEHICLE_TEXTS, 1000, 1000000, 1)
	#ENDIF	//	ADD_WIDGETS_FOR_REMINDER_TIMES
						ADD_WIDGET_BOOL("Reset vehicle text stat", bWdResetVehicleUnlockText)
						ADD_WIDGET_BOOL("Do mission passed texts", bWdDoMissionPassedText)
						ADD_WIDGET_BOOL("Show unlocked", bWdShowUnlocked) 
						ADD_WIDGET_BOOL("Show call status", unlockFmPhoneCall.bShowCallStatus)
						ADD_WIDGET_BOOL("Allow crate drop unlock", unlockFmPhoneCall.bDebugCrateDropUnlock)
						ADD_WIDGET_BOOL("Allow No Cops Unlock", unlockFmPhoneCall.bDebugLesterNoCopsUnlock)
						ADD_WIDGET_BOOL("Races", bWdRacesUnlocked)
						ADD_WIDGET_BOOL("Deathmatch", bwdDeathMatchUnlocked)
						ADD_WIDGET_BOOL("Print all unlocks status", bWdPrintAllUnlocked)
						ADD_WIDGET_BOOL("Print all contact unlocks ", bWdPrintAllContactUnlocked)
						ADD_WIDGET_BOOL("Print cutscene complete", bWdPrintCutsceneCompleteStatus)
						ADD_WIDGET_BOOL("Show current mission type", bWdShowCUrrentMissionType)
						ADD_WIDGET_INT_SLIDER("current type ", iWdCurrentMissionType, -100, 1000, 1)
						ADD_WIDGET_BOOL("Test mechanic del text", bWdDoMechanicPvehDelText)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Reminder calls")
						START_WIDGET_GROUP("Saved time")
							ADD_WIDGET_BOOL("Show saved times", bWdShowReminderTimer)
							ADD_WIDGET_INT_SLIDER("Insurance", iWdInsuranceReminder, -1, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Personal Veh", iWdPersVehDelReminder, -1, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Import Export", iWdImpExpReminder, -1, 100000000, 1) 
							ADD_WIDGET_INT_SLIDER("Pegasus veh", iWdPegasusVehReminder, -1, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Bounty ", iWdBountyReminder, -1, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Base jump",iWdParaReminder , -1, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Crate Drop", iWdCrateDropReminder, -1, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Survival", iWdLastSurvivalReminder, -1, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Gang Attack", iWdGangAttackReminder, -1, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Brucie Pill", iWdBrucieReminder, -1, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Security van", iWdSecVanReminder, -1, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Biker backup", iWdBikerBackupReminder, -1, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Vagos Backup", iWdVagosBackupReminder, -1, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Lester veh", iWdLesterVehReminder, -1, 100000000, 1) 
							ADD_WIDGET_INT_SLIDER("Merryweather", iWdMerryweatherReminder, -1, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Lester help", iWdLesterHelpReminder, -1, 100000000, 1)
							
							ADD_WIDGET_INT_SLIDER("Lester Req job",iWdLesterReqJobReminder, -1, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Martin Req job",iWdMartinReqJobReminder, -1, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Ron Req job",iWdRonReqJobReminder, -1, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Gerald Req job",iWdGeraldReqJobReminder, -1, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Simeon Req job",iWdSimeonReqJobReminder, -1, 100000000, 1)
						STOP_WIDGET_GROUP()
						


						START_WIDGET_GROUP("Wait time") 
							ADD_WIDGET_BOOL("Debug reminder calls", bWdDebugReminderCalls) 
							ADD_WIDGET_INT_SLIDER("Insurance ", reminderTimes.iInsuranceReminder, 0, 100000000, 1) 
							ADD_WIDGET_INT_SLIDER("Personal veh del ", reminderTimes.iPersVehDelReminder, 0, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Import Export ", reminderTimes.iImpExpReminder, 0, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Pegasus veh", reminderTimes.iPegasusVehReminder, 0, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Bounty ", reminderTimes.iBountiesReminder, 0, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Base jump",reminderTimes.iParaReminder , 0, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Crate Drop", reminderTimes.iCrateDropReminder, 0, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Survival", reminderTimes.iSurvivalReminder, 0, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Gang attack", reminderTimes.iGangAttackReminder, 0, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Brucie Pill", reminderTimes.iBrucieBoxReminder, 0, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Security van", reminderTimes.iSecVanReminder, 0, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Lester Veh", reminderTimes.iLesterVehReminder, 0, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Merryweather", reminderTimes.iMerryReminder, 0, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Lester Help", reminderTimes.iLesterHelpRem, 0, 100000000, 1)
							
							ADD_WIDGET_INT_SLIDER("Lester Req job",reminderTimes.iLesterReqJobRem, 0, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Simeon Req job",reminderTimes.iSimeonReqJobRem, 0, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Ron Req job",reminderTimes.iRonReqJobRem	, 0, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Martin Req job",reminderTimes.iMartinReqJobRem, 0, 100000000, 1)
							ADD_WIDGET_INT_SLIDER("Gerald Req job",reminderTimes.iGeraldReqJobRem, 0, 100000000, 1)
							
						STOP_WIDGET_GROUP()


					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Trigger tut")
						ADD_WIDGET_BOOL("Set splash active",bWdSetSplashActive) 
						ADD_WIDGET_BOOL("Remove splash",bWdRemoveSplash)
						ADD_WIDGET_INT_SLIDER("Max wait", MAX_DM_TUT_TIMEOUT, 1000, 10000000, 1)
						ADD_WIDGET_INT_SLIDER("iMyTutorialSession", iWdLocalSession, -100, 100, 1)
						ADD_WIDGET_BOOL("Set cut complete", bWdSetCutsceneCOmplete)
						ADD_WIDGET_BOOL("Show tut session ID", bWdShowTutSession)
						ADD_WIDGET_BOOL("Reset Trigger tut", bWdResetTriggerTut)
						ADD_WIDGET_BOOL("Show phase time",bWdShowPhaseTime)
						ADD_WIDGET_FLOAT_SLIDER("Phase Time",fWdPhaseTime, -10.0, 10.0, 0.01)
						ADD_WIDGET_BOOL("Show player anim time", bWdShowPlayerAnimTime)
						ADD_WIDGET_FLOAT_SLIDER("Anim time",fWdAnimTime, -10.0, 10.0, 0.01)
						ADD_WIDGET_BOOL("Show leaderboard pos", bWdShowLeaderboardPos)
						ADD_WIDGET_INT_SLIDER("Pos", iWdLeaderboardPos, -1000, 1000, 1 )
						ADD_WIDGET_BOOL("Test phone to ear", bWdTestPhoneToEar)

					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Boss V Boss")
						ADD_WIDGET_BOOL("Send Boss DM Invite", bWdLaunchBossDm)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Mission passed text")
						ADD_WIDGET_INT_SLIDER("Time to wait",MIN_TIME_FOR_MISSION_PASSED_TEXT_MESSAGE, 0, 1000000, 1)
						ADD_WIDGET_BOOL("Lester", bWdSetPassedLester)
						ADD_WIDGET_BOOL("Lamar", bWdSetPassedLamar)
						ADD_WIDGET_BOOL("Martin",bWdSetPassedMartin)
						ADD_WIDGET_BOOL("Simeon", bWdSetPassedSimeon)
						ADD_WIDGET_BOOL("Trevor", bWdSetPassedTrevor)
						ADD_WIDGET_BOOL("Ron", bWdSetPassedRon)
						ADD_WIDGET_BOOL("Gerald", bWdSetPassedGerald)
					STOP_WIDGET_GROUP()
				
					
					START_WIDGET_GROUP("Shops")
						ADD_WIDGET_BOOL("Give glasses", bWdGiveGlasses)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Hold up tut")
						ADD_WIDGET_BOOL("Clear Stat and launch", bWdHoldUpTutClearStat)
						ADD_WIDGET_BOOL("Set Timer Done Bit", bHoldUpSetTimerBit)
						ADD_BIT_FIELD_WIDGET("iHoldUpTimerDoneBitSet", GlobalServerBD_HoldUp.HoldUpServerData.iHoldUpTimerDoneBitSet)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Car mod tut")
						ADD_WIDGET_BOOL("Launch car mod tut", bWdLaunchCarModTut)
					//	ADD_WIDGET_BOOL("Clear Stat", bWdClearCarModTutStat)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Lester Cutscene")
						ADD_WIDGET_BOOL("Launch Lester Cut", bWdLaunchLesterCut)
					//	ADD_WIDGET_BOOL("Clear Stat", bWdClearLesterCut)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Ron & Trevor Cutscene")
						ADD_WIDGET_BOOL("Launch Cut", bWdLaunchTrevorCut)
				//		ADD_WIDGET_BOOL("Clear Stat", bWdClearRonCut)
					STOP_WIDGET_GROUP()
					

					START_WIDGET_GROUP("Lowrider Int")
						ADD_WIDGET_BOOL("Launch Cut", bWdlaunchLowriderInt)
					STOP_WIDGET_GROUP()
					
					
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("NPC invites")
					ADD_WIDGET_BOOL("Launch npc invites", bWdLaunchNpcInvite)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Launch GD Miss")
					ADD_WIDGET_BOOL("Launch Miss", bWdLaunchSmMission)
				STOP_WIDGET_GROUP()
	                                                                                                         
				START_WIDGET_GROUP("Daily Objectives")
					START_WIDGET_GROUP("View Current Objectives - READ ONLY")
						REPEAT NUM_MP_DAILY_OBJECTIVES i
							ADD_WIDGET_INT_SLIDER("Objective: ",g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.Current_Daily_Objectives[i].iObjective,-1, ENUM_TO_INT(MP_DAILY_MAX), 1)
							ADD_WIDGET_BOOL("Completed: ",g_DEBUG_MP_Daily_Objective_Complete[i])
						ENDREPEAT
					STOP_WIDGET_GROUP()
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Time Trials")
					ADD_WIDGET_INT_SLIDER("Set Trial",MPGlobalsAmbience.iDEBUGTimeTrialVariation,-1,TOTAL_VARIATIONS-1,1)
					ADD_WIDGET_INT_SLIDER("Over Ride POSIX",g_iCurrentPosixOverrideForTimeTrial,-1,HIGHEST_INT-1,1)
					ADD_WIDGET_BOOL("Reset Trial",sTTVarsStruct.bEndScript)
					ADD_WIDGET_BOOL("Warp to Start",sTTVarsStruct.bWarpToStart)
					ADD_WIDGET_BOOL("Warp to End",sTTVarsStruct.bWarpToEnd)
					ADD_WIDGET_FLOAT_SLIDER("fPointX",sTTVarsStruct.fPointX,-4000,4500,1)
					ADD_WIDGET_FLOAT_SLIDER("fPointY",sTTVarsStruct.fPointY,-4000,8000,1)
					ADD_WIDGET_FLOAT_SLIDER("fPointZ",sTTVarsStruct.fPointZ,-4000,4000,1)
					ADD_WIDGET_FLOAT_SLIDER("fRotX",sTTVarsStruct.fRotX,0,360,1)
					ADD_WIDGET_FLOAT_SLIDER("fRotY",sTTVarsStruct.fRotY,0,360,1)
					ADD_WIDGET_FLOAT_SLIDER("fRotZ",sTTVarsStruct.fRotZ,0,360,1)
					
				STOP_WIDGET_GROUP()
				
				CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS DEBUG ===  CREATED WIDGETS ")
				
				START_WIDGET_GROUP("Freemode Event Opt In")
					ADD_WIDGET_BOOL("Reset", bResetFMEventOptIn)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Draw Debug Sphere")
					ADD_WIDGET_VECTOR_SLIDER("Location", vDebugSphereLocation, -8000.0, 8000.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Radius", fDebugSphereRadius, 0, 1000.0, 0.1)
				STOP_WIDGET_GROUP()
				
				ADD_CLAN_ID_WIDGETS()
				
				ADD_BROADCAST_DISABLE_RECORDING_FOR_SPHERE_WIDGET()
				
				ADD_RELATIONSHIP_GROUP_WIDGETS()			
				
				CREATE_ALL_REWARD_UNLOCKS_WIDGETS()
				
				CREATE_BOT_WIDGET(TRUE)
				
				ADD_TASK_DEBUG_WIDGETS()
				
				Create_Main_Generic_Client_Widgets()
				Create_Main_Generic_Server_Widgets()
				
				START_WIDGET_GROUP("Game Progress Unlocks")
					ADD_WIDGET_BOOL("Unlock All FM Activities", bUnlockAllFMActivites)
				STOP_WIDGET_GROUP()
	//			
				START_WIDGET_GROUP("TEST BROADCAST_LOCK_ANY_DUPE_AMBIENT_VEHICLES")
					ADD_WIDGET_INT_SLIDER("Model_Name (as int)", iAmbDupe_ModelName, LOWEST_INT, HIGHEST_INT, 1)
					ADD_WIDGET_INT_SLIDER("Plate Hash", iAmbDupe_PlateTextHash, LOWEST_INT, HIGHEST_INT, 1)
					ADD_WIDGET_BOOL("Grab current vehicle details", bAmbDupe_GetCurrentVehicleDetails)
					ADD_WIDGET_INT_SLIDER("RandomID", iAmbDupe_RandomID, 0, 65534, 1)
					ADD_WIDGET_BOOL("Send", bAmbDupe_send)
				STOP_WIDGET_GROUP()
				
				
				START_WIDGET_GROUP("PERSONAL VEHICLE")
					ADD_WIDGET_BOOL("Set vehicle as personal vehicle ", bDebugPeronsalVeh)
					
					START_WIDGET_GROUP("CLEANUP_MP_SAVED_VEHICLE")
						ADD_WIDGET_BOOL("bdelete",db_bCleanupPV_bdelete)
						ADD_WIDGET_BOOL("bIgnoreEmpty",db_bCleanupPV_bIgnoreEmpty)
						ADD_WIDGET_BOOL("bReturnToGarage",db_bCleanupPV_bReturnToGarage)
						ADD_WIDGET_BOOL("bReturnFromGarageAfterMission",db_bCleanupPV_bReturnFromGarageAfterMission)
						ADD_WIDGET_BOOL("bDoFadeOut",db_bCleanupPV_bDoFadeOut)
						ADD_WIDGET_BOOL("call",db_bCleanupPV)
					STOP_WIDGET_GROUP()
					
					ADD_WIDGET_BOOL("Clear hangar free veh", b_ClearFreeHanagerVehSet)
					
					
					ADD_WIDGET_BOOL("Delete PV", b_DeletePV)
					ADD_WIDGET_BOOL("Display PV debug",db_bDisplayPVDebug)
					ADD_WIDGET_BOOL("Output vehicle states",db_bOutputVehStates)
					ADD_WIDGET_BOOL("Test ordered cars",bDebugTestOrderedCars)
					ADD_WIDGET_BOOL("Test put veh in garage",db_bCheckPutVehInGarage)
					ADD_WIDGET_BOOL("Turn on hydraulics",db_bTurnOnHydraulics)
					ADD_WIDGET_BOOL("Output vehicle storage",g_db_bOutputDisplaySlotData)
					ADD_WIDGET_BOOL("Draw vehicle storage",g_db_bDrawDisplaySlotData)
					START_WIDGET_GROUP("Insurance")
						ADD_WIDGET_BOOL("Give Insurance", db_bInsureCar)
					STOP_WIDGET_GROUP()
					ADD_WIDGET_BOOL("Output current veh setup",bPrintCurrentVehicleSetup)
					ADD_WIDGET_BOOL("Output g_MpSavedVehicles ",bPrintSavedVehicleStucts)
					ADD_WIDGET_BOOL("Print fail create reason",b_bPrintFailReason_SavedVehicleAvailableForCreation)
					START_WIDGET_GROUP("Test Dup Protection")
						ADD_WIDGET_INT_SLIDER("Veh 1 TimeStamp",g_dbVehDupProt.iVehicleTimestamp[0],0,1000000,1)
						ADD_WIDGET_INT_SLIDER("Veh 1 Slot",g_dbVehDupProt.iVehicleSlot[0],0,MAX_MP_SAVED_VEHICLES-1,1)
						ADD_WIDGET_INT_SLIDER("Veh 2 TimeStamp",g_dbVehDupProt.iVehicleTimestamp[1],0,1000000,1)
						ADD_WIDGET_INT_SLIDER("Veh 2 Slot",g_dbVehDupProt.iVehicleSlot[1],0,MAX_MP_SAVED_VEHICLES-1,1)
						ADD_WIDGET_BOOL("Override Delete Vehicles",g_dbVehDupProt.bDeleteVehicles)
						ADD_WIDGET_BOOL("Enable Override",g_dbVehDupProt.bEnableOverride)
						ADD_WIDGET_BOOL("Trigger Test",g_dbVehDupProt.bTriggerTest)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Door open / shut")
						ADD_WIDGET_INT_SLIDER("iDoor",db_iPVOpenShut_Door,-1,5,1)
						ADD_WIDGET_BOOL("Open", db_bPVOpenShut_Open)
						ADD_WIDGET_BOOL("Shut", db_bPVOpenShut_Shut)
						ADD_WIDGET_BOOL("Instant", db_bPVOpenShut_Instant)
						ADD_WIDGET_BOOL("Swing Free", db_bPVOpenShut_SwingFree)
					STOP_WIDGET_GROUP()
					
					
					START_WIDGET_GROUP("Set radio station")
						ADD_WIDGET_INT_SLIDER("Radio staion index",db_iPV_RadioStation,-1,99,1)
						ADD_WIDGET_BOOL("Loud", db_bPV_RadioLoud)
						ADD_WIDGET_BOOL("set", db_bPV_RadioSet)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("engine running")
						ADD_WIDGET_BOOL("set engine runing",db_bPV_EngineRunning_set)	
						ADD_WIDGET_BOOL("unset engine running ", db_bPV_EngineRunning_unset)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("lights")
						ADD_WIDGET_INT_SLIDER("light setting ",db_iPV_LightSetting, 0, 4, 1)	
						ADD_WIDGET_BOOL("set pv light setting ", db_bPV_Lights_set)
					STOP_WIDGET_GROUP()
					
					
					
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_BypassPersonalVehicleTextDelay")
						g_bBypassPersonalVehicleTextDelay = TRUE
					ENDIF
					ADD_WIDGET_BOOL("Bypass PV text delay", g_bBypassPersonalVehicleTextDelay)
					
					START_WIDGET_GROUP("Fill Garage Debug")
						ADD_WIDGET_INT_SLIDER("Choose Garage Slot/For bicycle choose:10,11,12",sDebugAddVeh.db_iDebugGarageVehicleID,-1,MAX_MP_SAVED_VEHICLES,1)
						ADD_WIDGET_STRING("How to use:")
						ADD_WIDGET_STRING("Method A: 1.choose Fill all slots option or Fill all OFFICE GARAGE slots 2.Select any vehicle ")
						ADD_WIDGET_STRING("Method B: If you want to fill all player garage slots including Clubhouse and Office Garage with modded vehicles, ONLY press Fill all slots with supermod vehicles and WAIT till slider fills all slots :) ")
						ADD_WIDGET_STRING("NOTE: If Remove All vehicles option is not working for you, press it few time until all vehicles are deleted")
						ADD_WIDGET_BOOL("Add time-limited car",sDebugAddVeh.db_bAddVehicleToGarage[0])
						ADD_WIDGET_BOOL("Add Base unupgraded lowrider",sDebugAddVeh.db_bAddVehicleToGarage[1])
						ADD_WIDGET_BOOL("Add  Supermodded lowrider",sDebugAddVeh.db_bAddVehicleToGarage[2])
						ADD_WIDGET_BOOL("Add Base Benny’s non-lowrider",sDebugAddVeh.db_bAddVehicleToGarage[3])
						ADD_WIDGET_BOOL("Add Supermodded non-lowrider",sDebugAddVeh.db_bAddVehicleToGarage[4])
						ADD_WIDGET_BOOL("Add Generic Vehicles",sDebugAddVeh.db_bAddVehicleToGarage[6])
						ADD_WIDGET_BOOL("Fill all OFFICE GARAGE slots", sDebugAddVeh.db_bFillOfficeGarage)
						ADD_WIDGET_BOOL("Fill all slots",sDebugAddVeh.db_bFillAllSlots)
						ADD_WIDGET_BOOL("Fill all slots with supermod vehicles",sDebugAddVeh.db_bFillAllSlotsWithSupermod)
						ADD_WIDGET_BOOL("Remove vehicle in this slot",sDebugAddVeh.db_bRemoveVehicleFromGarage)
						ADD_WIDGET_BOOL("Remove ALL vehicles",sDebugAddVeh.db_bRemoveAllVehicles)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Fill Clubhouse Garage slots Debug")
						ADD_WIDGET_STRING("How to use: 1.Make sure you are in one of the clubhouses and you dont have any bikes stored in any garage slot 2. Choose clubhouse type and add bike or quadbike 3. Deliver vehicles to garage")
						ADD_WIDGET_BOOL("Traditional Clubhouse",sDebugAddBikeToClubhouse.db_bClubHouseType[0])
						ADD_WIDGET_BOOL("Urban Clubhouse",sDebugAddBikeToClubhouse.db_bClubHouseType[1])
						ADD_WIDGET_BOOL("Add Bike",sDebugAddBikeToClubhouse.db_bBikeType[0])
						ADD_WIDGET_BOOL("Add Quadbike",sDebugAddBikeToClubhouse.db_bBikeType[1])
						ADD_WIDGET_BOOL("Deliver vehicles to garage(s)",sDebugAddBikeToClubhouse.db_bDeliverVehicleToGarage)
						ADD_WIDGET_BOOL("Remove ALL vehicles",sDebugAddBikeToClubhouse.db_bRemoveAllVehicles)
						ADD_WIDGET_INT_SLIDER("Choose Garage Slot to edit",sDebugAddBikeToClubhouse.db_iDebugGarageVehicleID,0,16,1)
						ADD_WIDGET_VECTOR_SLIDER("Garage slot bike coords:",sDebugAddBikeToClubhouse.iVehPos,-9999.9, 9999.9,0.1)
						ADD_WIDGET_VECTOR_SLIDER("Garage slot bike rotation:",sDebugAddBikeToClubhouse.iVehRot,-360, 360,0.1)
						ADD_WIDGET_BOOL("Print Garage Slot Coords",sDebugAddBikeToClubhouse.bPrintDebugInfo)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Exterior clubhouse VehSpawn Debug")
						ADD_WIDGET_BOOL("Create Bikes",sDebugExtVehSpawn.bCreateBikes)
						ADD_WIDGET_BOOL("Create Quadbikes",sDebugExtVehSpawn.bCreateQuads)
						ADD_WIDGET_INT_SLIDER("iProperty",sDebugExtVehSpawn.iProperty,1,MAX_MP_PROPERTIES,1)
						ADD_WIDGET_INT_SLIDER("Choose Exterior spawn to edit",sDebugExtVehSpawn.iVehToEdit,0,7,1)
						ADD_WIDGET_VECTOR_SLIDER("Veh coords:",sDebugExtVehSpawn.iVehPos,-9999.9, 9999.9,0.1)
						ADD_WIDGET_VECTOR_SLIDER("Veh rotation:",sDebugExtVehSpawn.iVehRot,-360, 360,0.1)
						ADD_WIDGET_BOOL("Print details",sDebugExtVehSpawn.bPrintDebugInfo)
						ADD_WIDGET_BOOL("Delete vehicles",sDebugExtVehSpawn.bDeleteVehicle[0])
						ADD_WIDGET_BOOL("Are you sure?",sDebugExtVehSpawn.bDeleteVehicle[1])
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Vehicle spawn timer")
						ADD_WIDGET_BOOL("Clear spawn timer", g_SpawnData.bClearVehSpawnTimer)			
					STOP_WIDGET_GROUP()
					
					START_NEW_WIDGET_COMBO()
						ADD_TO_WIDGET_COMBO(GET_STRING_FROM_VEHICLE_SPAWN_QUEUE(eVeh_spawn_0_NONE))
						ADD_TO_WIDGET_COMBO(GET_STRING_FROM_VEHICLE_SPAWN_QUEUE(eVeh_spawn_1_PERSONAL))
						ADD_TO_WIDGET_COMBO(GET_STRING_FROM_VEHICLE_SPAWN_QUEUE(eVeh_spawn_2_PEGASUS))
						ADD_TO_WIDGET_COMBO(GET_STRING_FROM_VEHICLE_SPAWN_QUEUE(eVeh_spawn_3_TRUCK))
						ADD_TO_WIDGET_COMBO(GET_STRING_FROM_VEHICLE_SPAWN_QUEUE(eVeh_spawn_4_AIRCRAFT))
						ADD_TO_WIDGET_COMBO(GET_STRING_FROM_VEHICLE_SPAWN_QUEUE(eVeh_spawn_5_HACKERTRUCK))
					STOP_WIDGET_COMBO("Cached Vehicle Spawn Queue", d_iCachedVehicleSpawnQueue)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Bounties")
					ADD_WIDGET_BOOL("Test NPC Bounty (DEV_ONLY) ", bDebugTestNPCBounty)
					ADD_WIDGET_BOOL("Set 100% chance on steal car ",g_db_bsetAlwaysTriggerNPCBounty)
					ADD_WIDGET_BOOL("Clear daily bounty data",bDebugClearDailyBountyData)
					ADD_WIDGET_BOOL("Output bounty debug",g_db_OutputBountyDebug)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Cash/XP/Rank") 
					ADD_WIDGET_INT_SLIDER("My Rank",  iMyRank, -HIGHEST_INT, HIGHEST_INT, 1)
					ADD_WIDGET_INT_SLIDER("My Cash",  iMyCash, -HIGHEST_INT, HIGHEST_INT, 1)
					ADD_WIDGET_INT_SLIDER("My XP",  iMyXp, -HIGHEST_INT, HIGHEST_INT, 1)
					ADD_WIDGET_INT_SLIDER("Give Cash?",  iCashToGive, -HIGHEST_INT, HIGHEST_INT, 100)
					ADD_WIDGET_INT_SLIDER("Give XP?",  iXpToGive, -HIGHEST_INT, HIGHEST_INT, 100)	
					
					
					ADD_WIDGET_INT_SLIDER("My Crew XP",  iMyCrewXp, -HIGHEST_INT, HIGHEST_INT, 1)
					ADD_WIDGET_INT_SLIDER("Give Crew XP?",  iCrewXpToGive, -HIGHEST_INT, HIGHEST_INT, 100)
					ADD_WIDGET_INT_READ_ONLY("My Crew SLOT", iMyCrewSlot)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("NUMBER OF OTHERS LIKING PLAYERS MISSION") 
				
					ADD_WIDGET_INT_SLIDER("Number of Likes",  g_FMMC_HEADER_STRUCT.iMyHighestLikes, -HIGHEST_INT, HIGHEST_INT, 1)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Ambience")
					ADD_WIDGET_BOOL("PLAYER IN TUT SESSION", bPlayerInTutorialSession)
				
					ADD_WIDGET_BOOL("PLAYER ON Any FM AMBIENT", bPlayerOnFmAmbient)
					
					ADD_WIDGET_BOOL ("Print Mechanic Timer debug", bOutMechanicTimerDetailsThisFrame)
					ADD_WIDGET_INT_SLIDER("Override tuneable DJ",g_db_iOverrideClubIPLDJ,-1,3,1)
					
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_MechanicApartmentFeeTime")
						g_db_iMechanicApartmentFeeTime = GET_COMMANDLINE_PARAM_INT("sc_MechanicApartmentFeeTime")
						PRINTLN("g_db_iMechanicApartmentFeeTime = ", g_db_iMechanicApartmentFeeTime)
						g_db_bMechanicApartmentFeeTime = TRUE
					ENDIF
					ADD_WIDGET_INT_SLIDER("Time for Mechanic/Apartment Charges",g_db_iMechanicApartmentFeeTime,0,480000,1000)
					ADD_WIDGET_BOOL("Display for Mechanic/Apartment Charges",g_db_bMechanicApartmentFeeTime)
					
					ADD_WIDGET_BOOL ("bForceInvitedToApartment", bForceInvitedToApartment)
					
					ADD_WIDGET_BOOL("g_db_bTestUnlockAirportDoors",g_db_bTestUnlockAirportDoors)
					ADD_WIDGET_BOOL("g_db_bTestResetUnlockAirportDoors",g_db_bTestResetUnlockAirportDoors)
					START_WIDGET_GROUP("Launchers")
						START_WIDGET_GROUP("Hot Target")
							ADD_WIDGET_INT_SLIDER("Vehicle Type (0=Car, 1=Bike, 2=Heli)", MPGlobalsAmbience.iHotTargetType, -1, 2, 1)
							ADD_WIDGET_INT_SLIDER("Vehicle Model (Car 0-6, Bike 0-2, Heli 0-4)", MPGlobalsAmbience.iHotTargetVeh, -1, 6, 1)
							ADD_WIDGET_INT_SLIDER("Route Type (Car+Bike 0-1=Model 2=Shared, Heli 0=Model)", MPGlobalsAmbience.iHotTargetRoute, -2, 5, 1)
							ADD_WIDGET_INT_SLIDER("Route (Car+Bike 0-5, Heli 0)", MPGlobalsAmbience.iHotTargetRoute, -2, 5, 1)
							ADD_WIDGET_BOOL("Launch", bLaunchHotTarget)
						STOP_WIDGET_GROUP()

						START_WIDGET_GROUP("Urban Warfare")
							ADD_WIDGET_INT_SLIDER("Start Veh (0=Rhino, 1=Hydra, 2=Savage 3=Valkyrie 4 = Buzzard)", MPGlobalsAmbience.iUrbanWarfareVeh, -1, 4, 1)
							ADD_WIDGET_INT_SLIDER("Loc ", MPGlobalsAmbience.iUrbanWarfareLocation, -1, 6, 1)
						STOP_WIDGET_GROUP()
						ADD_WIDGET_BOOL("Launch bLaunchUrbanWarfare", bLaunchUrbanWarfare)
						ADD_WIDGET_BOOL("Launch bLaunchTimeTrial", bLaunchTimeTrial)
						ADD_WIDGET_BOOL("Launch bLaunchCPCollection", bLaunchCPCollection)
						ADD_WIDGET_BOOL("Launch bLaunchChallenges", bLaunchChallenges)
						ADD_WIDGET_BOOL("Launch Penned In", bLaunchPennedIn)
						ADD_WIDGET_BOOL("Launch Multi-Target Assassination", bLaunchMTA)
						ADD_WIDGET_BOOL("Launch Plane Takedown", bLaunchPlaneTakedown)
						ADD_WIDGET_BOOL("Launch Distract Cops", bLaunchDistractCops)
						ADD_WIDGET_BOOL("Launch Destroy Veh", bLaunchDestroyVeh)
						ADD_WIDGET_BOOL("Launch Race to Point", MPGlobalsAmbience.R2Pdata.bLaunchRaceToPoint)
						ADD_WIDGET_BOOL("Launch Ammo Drop", MPGlobalsAmbience.bLaunchAmmoDrop)
						ADD_WIDGET_BOOL("Launch Ballistics Drop", MPGlobalsAmbience.bLaunchBallisticsDrop)
						ADD_WIDGET_BOOL("Launch Boat Pickup", MPGlobalsAmbience.bLaunchBoatPickup)
						ADD_WIDGET_BOOL("Launch Heli Pickup", MPGlobalsAmbience.bLaunchHeliPickup)
						ADD_WIDGET_BOOL("Launch Brucie Box", MPGlobalsAmbience.bLaunchBruBox)
						ADD_WIDGET_BOOL("Launch JoyRider", bDebugLaunchJoyrider)
						ADD_WIDGET_BOOL("Launch Vehicle Drop Personal", MPGlobalsAmbience.bLaunchVehicleDropPersonal)
						ADD_WIDGET_BOOL("Launch Vehicle Drop Truck", MPGlobalsAmbience.bLaunchVehicleDropTruck)
						ADD_WIDGET_BOOL("Launch Vehicle Drop Armory Aircraft", MPGlobalsAmbience.bLaunchVehicleDropAvenger )
						ADD_WIDGET_BOOL("Launch Vehicle Drop Hacker Truck", MPGlobalsAmbience.bLaunchVehicleDropHackerTruck)
						ADD_WIDGET_BOOL("bFerrisWheelActive",bFerrisWheelActive)
						ADD_WIDGET_BOOL("Launch Gang Boss Steal Veh Class", bGbStealVehicleClassTaskActive)
						ADD_WIDGET_BOOL("Launch Gang Boss Put Out Hit", bGbHitActive)
						ADD_WIDGET_BOOL("Launch Gang Boss DM", bGbDeathmatchActive)
						//ADD_WIDGET_BOOL("Launch Gang Boss Challenges", bGbChallengesActive)
						ADD_WIDGET_BOOL("Launch Gang Boss Attack", bGbBossAttackActive)
						START_WIDGET_GROUP("Server Only")
							ADD_WIDGET_BOOL("Launch Crate Drop", bDebugLaunchCrateDrop)
							ADD_WIDGET_BOOL("Set Crate Drop Special", bDebugSetCrateDropSpecial)
							ADD_WIDGET_BOOL("Ignore Num Players Check", bDebugCrateDropIgnoreNumPlayers)
							ADD_WIDGET_BOOL("Reset Special Crate Drop Limit",GlobalServerBD_BlockB.CrateDropServerLaunchData.bSpecialCrateDone)
						STOP_WIDGET_GROUP()
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Power Ups")
						ADD_WIDGET_BOOL("Force Rage", bDebugForceRagePowerUp)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Crate Drop")
						ADD_WIDGET_INT_READ_ONLY("Next Crate Drop - Day", iCrateDropDay)
						ADD_WIDGET_INT_READ_ONLY("Next Crate Drop - Hour", iCrateDropHour)
						ADD_WIDGET_INT_READ_ONLY("Next Crate Drop - Minute", iCrateDropMin)
						ADD_WIDGET_INT_READ_ONLY("Client Crate Launch Stage", MPGlobalsAmbience.CrateDropLaunchData.iLaunchStageClient)
						ADD_WIDGET_INT_READ_ONLY("Server Crate Launch Stage", GlobalServerBD_BlockB.CrateDropServerLaunchData.iLaunchStage)
						
						START_WIDGET_GROUP("Server Only")
							ADD_WIDGET_BOOL("Launch Crate Drop", bDebugLaunchCrateDrop)
							ADD_WIDGET_BOOL("Set Crate Drop Special", bDebugSetCrateDropSpecial)
						STOP_WIDGET_GROUP()
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Gang Attack")
						ADD_WIDGET_BOOL("Do Kill Hostiles", bGangAttackDebugKill)
						ADD_WIDGET_BOOL("Do Steal Package", bGangAttackDebugPackage)
						ADD_WIDGET_BOOL("Trigger Steal Package Text", bDebugLaunchStealPackageTrigger)
						ADD_WIDGET_INT_READ_ONLY("Gang", MPGlobalsAmbience.HideoutData.iStealPackageForGang)
						ADD_WIDGET_VECTOR_SLIDER("Location", MPGlobalsAmbience.HideoutData.vStealPackageLocation, -99999, 99999, 0)
					STOP_WIDGET_GROUP()
									
					START_WIDGET_GROUP("Vehicle Drop")
						ADD_WIDGET_BOOL("Launch Vehicle Drop Personal", MPGlobalsAmbience.bLaunchVehicleDropPersonal)
						ADD_WIDGET_BOOL("Launch Vehicle Drop Truck", MPGlobalsAmbience.bLaunchVehicleDropTruck)
						ADD_WIDGET_BOOL("Launch Vehicle Drop Hacker Truck", MPGlobalsAmbience.bLaunchVehicleDropHackerTruck)
						ADD_WIDGET_INT_READ_ONLY("iVehicleDropPersonalStage", VDPersonalData.iVehicleDropPersonalStage)
						ADD_WIDGET_INT_READ_ONLY("Vehicle Drop Variation", MPGlobalsAmbience.iVehicleDropVariation)
						ADD_WIDGET_BOOL("Launch Vehicle Drop", MPGlobalsAmbience.bLaunchVehicleDrop)
						ADD_WIDGET_BOOL("Launch Vehicle Drop Drive", MPGlobalsAmbience.bLaunchVehicleDropDrive)
						ADD_WIDGET_INT_SLIDER("Vehicle Model",  MPGlobalsAmbience.iVehicleDropModel, 0, VEHICLE_DROP_MAX_VEHICLE_MODELS-1, 1)
					STOP_WIDGET_GROUP()


					START_WIDGET_GROUP("Contact Requests")
						ADD_WIDGET_BOOL("resetLocalCDOnContactRequests", resetLocalCDOnContactRequests)
						ADD_WIDGET_BOOL("resetTargetCDOnContactRequests", resetTargetCDOnContactRequests)
						ADD_WIDGET_BOOL("g_db_bOutputCallContactBlocks",g_db_bOutputCallContactBlocks)
						ADD_WIDGET_INT_SLIDER("Stripper 1 stay time",MPGlobalsAmbience.stripperRequest.iStayTime[0],0,300000,1000)
					STOP_WIDGET_GROUP()
					
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Hideout - Track Award")
					ADD_WIDGET_INT_READ_ONLY("Target Num", g_sDoneNumMissionTypeAwardData[HIDEOUT_DONE_NUM_MISSION_TYPE_AWARD].iTargetNumber)
					ADD_WIDGET_INT_READ_ONLY("Current Num", g_sDoneNumMissionTypeAwardData[HIDEOUT_DONE_NUM_MISSION_TYPE_AWARD].iCurrentNumber)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Pickups/Packages/Weapons")
					ADD_WIDGET_BOOL("Display All Pickups/Packages/Weapons", bDebugDisplayWeapons_pickups_packages)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Corona invites")
					ADD_WIDGET_BOOL("bDebugDrawSafeCircleSpawn", bDebugDrawSafeCircleSpawn)
					ADD_WIDGET_FLOAT_SLIDER("fDebugDrawSafeCircleRadius",fDebugDrawSafeCircleRadius,0,5,0.1)
					ADD_WIDGET_BOOL("b_DB_AllowCoronaOutput",b_DB_AllowCoronaOutput)
					ADD_WIDGET_BOOL("g_db_bAlwaysUsePresence",g_db_bAlwaysUsePresence)
					ADD_WIDGET_BOOL("db_testFindMatchedGamers",db_testFindMatchedGamers)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Debug upload SC data")
					ADD_WIDGET_BOOL("dDebugUploadLBData",dDebugUploadLBData)
					ADD_WIDGET_INT_SLIDER("debugUploadLBRaceTime",debugUploadLBRaceTime,30000,999999999,1)
					ADD_WIDGET_INT_SLIDER("debugUploadLBBestLap",debugUploadLBBestLap,5000,999999999,1)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Gang Call")
					START_WIDGET_GROUP("Debug Call")
						ADD_WIDGET_BOOL("Call Thieves", bDebugGangCallWantOwnership[GANG_CALL_TYPE_SPECIAL_THIEF])
						ADD_WIDGET_BOOL("Call Merryweather", bDebugGangCallWantOwnership[GANG_CALL_TYPE_SPECIAL_MERRYWEATHER])
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Client BD")
						ADD_WIDGET_BOOL("Client Claims Thief", bDebugGangCallBitset[GANG_CALL_TYPE_SPECIAL_THIEF])
						ADD_WIDGET_INT_READ_ONLY("Thief Target ID", GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].iGangCallTargetID)
						ADD_WIDGET_BOOL("Client Claims Merryweather", bDebugGangCallBitset[GANG_CALL_TYPE_SPECIAL_MERRYWEATHER])
						ADD_WIDGET_INT_READ_ONLY("Merryweather Target ID", GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].iGangCallTargetID)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Server BD")
						ADD_WIDGET_INT_READ_ONLY("Current Player Owner Thief", GlobalServerBD_FM.iGangCallCurrentOwnerPlayer[GANG_CALL_TYPE_SPECIAL_THIEF])
						ADD_WIDGET_INT_READ_ONLY("Current Player Target Thief", GlobalServerBD_FM.iGangCallCurrentTargetPlayer[GANG_CALL_TYPE_SPECIAL_THIEF])
						ADD_WIDGET_INT_READ_ONLY("Current Player Owner Merryweather", GlobalServerBD_FM.iGangCallCurrentOwnerPlayer[GANG_CALL_TYPE_SPECIAL_MERRYWEATHER])
						ADD_WIDGET_INT_READ_ONLY("Current Player Target Merryweather", GlobalServerBD_FM.iGangCallCurrentTargetPlayer[GANG_CALL_TYPE_SPECIAL_MERRYWEATHER])
					STOP_WIDGET_GROUP()
				STOP_WIDGET_GROUP()
				i= 0
				START_WIDGET_GROUP("Property Exterior Debug")
					ADD_WIDGET_BOOL("Turn on hide exterior",mpPropExtTest.bActive)
					ADD_WIDGET_BOOL("Turn on debug sphere",mpPropExtTest.bDrawDebugSphere)
					REPEAT 4 i
						tl63 = "Property "
						tl63 += i
						START_WIDGET_GROUP(tl63)
							ADD_WIDGET_VECTOR_SLIDER("Sphere centre",mpPropExtTest.vLoc[i] ,-10000,10000,0.1)
							ADD_WIDGET_FLOAT_SLIDER("Radius",mpPropExtTest.fRadius[i], 0,100,0.1)
						STOP_WIDGET_GROUP()
					ENDREPEAT
					ADD_WIDGET_BOOL("Force entrance failure",g_db_bTestPropertyEntranceFailure)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Interior Debug")
					ADD_WIDGET_BOOL("Enable All Interiors", bWdEnableAllInteriors)
					ADD_WIDGET_BOOL("g_db_bDisplayConcealedState", g_db_bDisplayConcealedState)
					ADD_WIDGET_BOOL("db_bOutputAllApartmentDetails",db_bOutputAllApartmentDetails)
					ADD_WIDGET_BOOL("Print server requests",g_db_bPrintServerPropertyRequestDetails)
					ADD_WIDGET_BOOL("Print all requests",g_db_bPrintPropertyRequestDetailsAll)
					ADD_WIDGET_BOOL("g_b_StartingInApartmentForQuickRestart",g_b_StartingInApartmentForQuickRestart)
					ADD_WIDGET_BOOL("g_b_AllPlayersReadyForApartmentQuickRestart",g_b_AllPlayersReadyForApartmentQuickRestart)
					
					ADD_WIDGET_BOOL("NEW output property data",db_bOutputThisPropertyDetails)
					ADD_WIDGET_BOOL("g_db_bTestFailedVehTranInGarage",g_db_bTestFailedVehTranInGarage)
					
					ADD_WIDGET_BOOL("bd_bForceApartmentCleaning",bd_bForceApartmentCleaning)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("BG Script Debug")
					ADD_WIDGET_BOOL("Set Invalid Mod Decorator on vehicle",bWdSetInvalidModDecoratorOnVeh)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Ability Stats Diminishing")
					ADD_WIDGET_INT_READ_ONLY("Last time played (in seconds)", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.ilasttimeplayed)
					//ADD_WIDGET_INT_READ_ONLY("Last time played (in days)",GET_NUMBER_OF_DAYS_LAST_PLAYED())
				STOP_WIDGET_GROUP()
				CREATE_GANG_ANGRY_WIDGETS(gangAngryData)

				
				CREATE_CORONA_POS_WIDGETS()
				
				WEAPONWIDGET WEAPONWIDGETfreemode
				CREATE_WEAPON_UNLOCK_WIDGETS(WEAPONWIDGETfreemode)
				
				CREATE_XP_WIDGETS()
				
				START_WIDGET_GROUP("Contact Request Cooldowns")
					REPEAT CONTACT_REQUEST_MAX i
						START_WIDGET_GROUP(EVENTS_GET_CONTACT_REQUEST_STRING_FROM_TYPE(INT_TO_ENUM(CONTACT_REQUEST_TYPE, i)))
							ADD_WIDGET_BOOL("On Cooldown", bDebugContactRequestOnCooldown[i])
							ADD_WIDGET_BOOL("End Cooldown?", bDebugContactRequestEndCooldown[i])
						STOP_WIDGET_GROUP()
					ENDREPEAT
					ADD_WIDGET_BOOL("db_setMechanicBlockedFlag",db_setMechanicBlockedFlag)
					STOP_WIDGET_GROUP()
				
				ADD_WIDGET_BOOL("db_bTestLBWarning",db_bTestLBWarning)

				START_WIDGET_GROUP("Broadcast App")
					ADD_WIDGET_BOOL("g_bBroadcastAppEnabled", g_bBroadcastAppEnabled)
					ADD_WIDGET_BOOL("g_bBroadcastAppOn", g_bBroadcastAppOn)
					ADD_WIDGET_BOOL("g_bBroadcastAppSuspended", g_bBroadcastAppSuspended)
				STOP_WIDGET_GROUP()
				
				ADD_WIDGET_BOOL("db_outputCurrentCarStats",db_outputCurrentCarStats)
				
				START_WIDGET_GROUP("CDM: LB - tests")
					ADD_WIDGET_BOOL("debugELOLeaderboard.bReadData",debugELOLeaderboard.bReadData)
					ADD_WIDGET_BOOL("debugELOLeaderboard.bWriteData",debugELOLeaderboard.bWriteData)
					ADD_WIDGET_INT_SLIDER("debugELOLeaderboard.iWriteNewEloValue",debugELOLeaderboard.iWriteNewEloValue,-1200,1200,1)
					ADD_WIDGET_BOOL("debugLimitedShopItems.bReadLeaderboardValues",debugLimitedShopItems.bReadLeaderboardValues)
					ADD_WIDGET_BOOL("debugLimitedShopItems.bTriggerPurchase",debugLimitedShopItems.bTriggerPurchase)
					ADD_WIDGET_INT_SLIDER("debugLimitedShopItems.iPurchaseItemID",debugLimitedShopItems.iPurchaseItemID,0,10,1)
					ADD_WIDGET_INT_SLIDER("debugLimitedShopItems.iPurchaseAmount",debugLimitedShopItems.iPurchaseAmount,0,100,1)
					
					ADD_WIDGET_BOOL("sclbDebugMetrics.bReset",sclbDebugMetrics.bReset)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("CDM: Test property verification")
					ADD_WIDGET_BOOL("Print owned property",MPGlobalsAmbience.bPrintOwnedProperties)
					ADD_WIDGET_BOOL("Non-Profile save",db_bPropertyNonProfileSave)
					ADD_WIDGET_BOOL("Profile save",db_bPropertyProfileSave)
					ADD_WIDGET_INT_SLIDER("iProperty",iTestPropertyVerificationID,0,MAX_MP_PROPERTIES,1)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("CDM: Test MISSION_CONTROL_NET_DOOR_OVERRIDE")
					ADD_WIDGET_BOOL("db_bMCDoorOverride",db_bMCDoorOverride)
					ADD_WIDGET_BOOL("db_bMCDoorClearOverride",db_bMCDoorClearOverride)
					ADD_WIDGET_INT_SLIDER("db_iMCDoorID",db_iMCDoorID,0,AIRPORT_FAR_RIGHT_SIDE_GATES ,1)
					ADD_WIDGET_INT_SLIDER("db_iMCDoorState",db_iMCDoorState,0,ENUM_TO_INT(MC_DOOR_OVERRIDE_FORCED_OPEN) ,1)
				STOP_WIDGET_GROUP()
				
				ADD_WIDGET_FOR_PV_LIMITER(PVLimiterData)

				START_WIDGET_GROUP("Car Stats")
					ADD_WIDGET_FLOAT_SLIDER("Speed reduction",fVehicleStatReduction[VEHICLE_STAT_TOP_SPEED_INDEX],0.0,200,0.1)
					ADD_WIDGET_FLOAT_SLIDER("Acceleration reduction",fVehicleStatReduction[VEHICLE_STAT_ACCELERATION_INDEX],0.0,200,0.1)
					ADD_WIDGET_FLOAT_SLIDER("Braking reduction",fVehicleStatReduction[VEHICLE_STAT_BRAKING_INDEX],0.0,200,0.1)
					ADD_WIDGET_FLOAT_SLIDER("Traction reduction",fVehicleStatReduction[VEHICLE_STAT_TRACTION_INDEX],0.0,200,0.1)
				STOP_WIDGET_GROUP()
				
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_RocketAccuracy")
					START_WIDGET_GROUP("Heat Seeking Rockets")
						ADD_WIDGET_FLOAT_SLIDER("BREAK_LOCK_ANGLE (Def 0.2)", g_fDebugBreakLockAngle, 0.0, 1.0, 0.05)
						ADD_WIDGET_FLOAT_SLIDER("BREAK_LOCK_ANGLE_CLOSE (Def 0.6)", g_fDebugBreakLockAngleClose, 0.0, 1.0, 0.05)
						ADD_WIDGET_FLOAT_SLIDER("BREAK_LOCK_CLOSE_DISTANCE (Def 20.0)", g_fDebugLockCloseDitance, 0.0, 1000.0, 1.0)
						ADD_WIDGET_FLOAT_SLIDER("TURN_RATE_MODIFIER (Def 1.0)", g_fDebugTurnModifier, 0.0, 10.0, 0.1)
					STOP_WIDGET_GROUP()
				ENDIF

					BG_SCRIPT_CREATE_WIDGETS(BGScriptKickDetails)
				
					ADD_WIDGETS_FOR_FM_WEAPON_PICKUPS()
				
				CREATE_GRIEF_PASSIVE_WIDGETS(iPlayer)
				
				START_WIDGET_GROUP("Global Invite Flow System")
					ADD_WIDGET_STRING("Use this to force send an invite flow invite. If the tunable value widget is left at -2, it will force an invite from the actual tunables.")
					ADD_WIDGET_STRING("A full list of valid tunables values can be found here: https://hub.take2games.com/display/RSGGTAV/Invite+Flow+System")
					ADD_WIDGET_INT_SLIDER("Force Tunable Value", g_iForcedInviteFlowWidget, -2, 108, 1)
					ADD_WIDGET_BOOL("Send Invite", g_bSendInviteFlowInviteWidget)
				STOP_WIDGET_GROUP()
				
				INIT_SIMPLE_INTERIOR_DEBUG_WIDGET(SimpleInteriorLocalData)
				
				START_WIDGET_GROUP("Office Prop Overrides")
					ADD_WIDGET_BOOL("Override Stats", g_bDebug_OfficeProp_Override)
					ADD_WIDGET_BOOL("Refresh Interior", g_bDebug_Refresh_Interior)
					
					ADD_WIDGET_INT_SLIDER("Warehouse Slot 1", g_iDebug_Warehouse_Override[0], 0, 22, 1)
					ADD_WIDGET_INT_SLIDER("Warehouse Slot 2", g_iDebug_Warehouse_Override[1], 0, 22, 1)
					ADD_WIDGET_INT_SLIDER("Warehouse Slot 3", g_iDebug_Warehouse_Override[2], 0, 22, 1)
					ADD_WIDGET_INT_SLIDER("Warehouse Slot 4", g_iDebug_Warehouse_Override[3], 0, 22, 1)
					ADD_WIDGET_INT_SLIDER("Warehouse Slot 5", g_iDebug_Warehouse_Override[4], 0, 22, 1)
					ADD_WIDGET_INT_SLIDER("IE Warehouse", g_iDebug_Warehouse_Override[5], 0, 10, 1)
					
					ADD_WIDGET_INT_SLIDER("Cash Level", g_iDebug_Cash_Override, 0, 25, 1)

					ADD_WIDGET_INT_SLIDER("SILVER", g_iDebug_Contraband_Override[0], 0, 3, 1)
					ADD_WIDGET_INT_SLIDER("PILLS", g_iDebug_Contraband_Override[1], 0, 3, 1)
					ADD_WIDGET_INT_SLIDER("MED", g_iDebug_Contraband_Override[2], 0, 3, 1)
					ADD_WIDGET_INT_SLIDER("JEWELWATCH", g_iDebug_Contraband_Override[3], 0, 3, 1)
					ADD_WIDGET_INT_SLIDER("IVORY", g_iDebug_Contraband_Override[4], 0, 3, 1)
					ADD_WIDGET_INT_SLIDER("GUNS", g_iDebug_Contraband_Override[5], 0, 3, 1)
					ADD_WIDGET_INT_SLIDER("GEMS", g_iDebug_Contraband_Override[6], 0, 3, 1)
					ADD_WIDGET_INT_SLIDER("FURCOATS", g_iDebug_Contraband_Override[7], 0, 3, 1)
					ADD_WIDGET_INT_SLIDER("ELECTRONIC", g_iDebug_Contraband_Override[8], 0, 3, 1)
					ADD_WIDGET_INT_SLIDER("DRUGSTATUE2", g_iDebug_Contraband_Override[9], 0, 3, 1)
					ADD_WIDGET_INT_SLIDER("DRUGBAGS3", g_iDebug_Contraband_Override[10], 0, 3, 1)
					ADD_WIDGET_INT_SLIDER("COUNTERFEIT3", g_iDebug_Contraband_Override[11], 0, 3, 1)
					ADD_WIDGET_INT_SLIDER("BOOZE_CIGS3", g_iDebug_Contraband_Override[12], 0, 3, 1)
					ADD_WIDGET_INT_SLIDER("ART3", g_iDebug_Contraband_Override[13], 0, 3, 1)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Clubhouse Prop Overrides")
					ADD_WIDGET_BOOL("Override Stats", g_bDebug_ClubhouseProp_Override)
					ADD_WIDGET_BOOL("Refresh Interior", g_bDebug_Refresh_Interior)
					
					ADD_WIDGET_INT_SLIDER("WEED", g_iDebug_CHContraband_Override[0], 0, 3, 1)
					ADD_WIDGET_INT_SLIDER("ID", g_iDebug_CHContraband_Override[1], 0, 3, 1)
					ADD_WIDGET_INT_SLIDER("COKE", g_iDebug_CHContraband_Override[2], 0, 3, 1)
					ADD_WIDGET_INT_SLIDER("METH", g_iDebug_CHContraband_Override[3], 0, 3, 1)
					ADD_WIDGET_INT_SLIDER("COUNTER", g_iDebug_CHContraband_Override[4], 0, 3, 1)
				STOP_WIDGET_GROUP()
				
				SETUP_CLOTHING_FOR_VEHICLE_PURCHASE_WIDGETS()
				#IF IS_DEBUG_BUILD
					CREATE_RESTRICTED_INTERIOR_WIDGETS(sRIA)
					SIMPLE_CUTSCENE_CREATE(g_SimpleInteriorData.db_simpleCutscene, "TEST CUTSCENE")
					SIMPLE_CUTSCENE_CREATE_DEBUG_WIDGETS(g_SimpleInteriorData.db_simpleCutscene)
				#ENDIF
				
				
				START_WIDGET_GROUP("Import-Export Player Garage")
					ADD_WIDGET_BOOL("Warp to Garage", g_bWarpToGarage)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("Collectables")
					ADD_WIDGET_BOOL("Restart Action Figure Collectables", g_bclearcollectablesactionfigures)
					ADD_WIDGET_INT_SLIDER("Action Figure Selector", g_iselectedactionfigurecollectable, 0, 99 , 1) 
					ADD_WIDGET_BOOL("Warp To Action Figure Collectable",g_bwarp_to_next_action_figure_collectable)
				STOP_WIDGET_GROUP()
				
		
				
				START_WIDGET_GROUP("Last Player To Damage You")
					
					ADD_WIDGET_INT_READ_ONLY("PLayer ID", g_iLastDamagerPlayer) 
					ADD_WIDGET_INT_READ_ONLY("Timestamp", g_iLastDamagerTimeStamp) 
					ADD_WIDGET_INT_READ_ONLY("Distance", g_iLastDamagerdistance) 
				STOP_WIDGET_GROUP() 
				
				START_WIDGET_GROUP("Air Bombs")
					ADD_WIDGET_BOOL("Force Passenger Bomb Control", g_bForcePassengerBombControl)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("RC Vehicle")
					ADD_WIDGET_BOOL("Start RC vehicle script", g_bStartRCVehicleScript)
					ADD_WIDGET_BOOL("Cleanup RC vehicle script", g_bCleanupRCVehicleScript)
				STOP_WIDGET_GROUP()

				
				CREATE_PLATE_DUPE_WIDGET()
				
				FREEMODE_DELIVERY_CREATE_DEBUG_WIDGETS(freemodeDeliveryData)
				CREATE_VEHICLE_DROP_PERSONAL_WIDGET(VDPersonalTruckData, VDPersonalTruckDebugData)

				#IF IS_DEBUG_BUILD
				DEBUG_VEHICLE_GENERATOR_WIDGET(g_dFreemodeVehGenData)
				#ENDIF
				
//				STOP_WIDGET_GROUP()
				CLEAR_CURRENT_WIDGET_GROUP(missionTab)
				
				bCREATE_FM_WIDGETS_CACHED = TRUE
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF FEATURE_WANTED_SYSTEM
		WANTED_SYSTEM_CREATE_WIDGETS(g_WantedSystemData)
		#ENDIF
		#ENDIF
		
	ENDPROC	
#ENDIF

// KGM 2/2/13:	Sends an event every frame to the local player and expects to receive it next frame
//				This is to test that events aren't being lost and that processing isn't being skipped for a frame
#IF IS_DEBUG_BUILD
INT m_debugHoldFrame = 0
PROC DEBUG_Send_Check_For_Missing_Events_Or_Skipped_Processing()
	
	IF (m_debugHoldFrame = 0)
		m_debugHoldFrame = GET_FRAME_COUNT() - 1
		
		NET_PRINT("DEBUG_Send_Check_For_Missing_Events_Or_Skipped_Processing(): One-off frame initialiser. Frame = ")
		NET_PRINT_INT(m_debugHoldFrame)
		NET_NL()
	ENDIF

	// Check for skipped processing in a frame
	INT thisFrame		= GET_FRAME_COUNT()
	INT expectedFrame	= m_debugHoldFrame + 1
	
	IF (thisFrame != expectedFrame)
		NET_PRINT("DEBUG_Send_Check_For_Missing_Events_Or_Skipped_Processing(): Freemode Frame Count was wrong. This Frame = ")
		NET_PRINT_INT(thisFrame)
		NET_PRINT("  Last Frame: ")
		NET_PRINT_INT(m_debugHoldFrame)
		NET_NL()
		SCRIPT_ASSERT("DEBUG_Send_Check_For_Missing_Events_Or_Skipped_Processing(): Freemode Frame Count was wrong. Tell Keith.")
		PRINT_HELP("MISSING_FRAME")
	ENDIF
	
	// Store this frame uto be checked next frame
	m_debugHoldFrame = thisFrame

	// Send an event to be expected on next frame
	INT nextFrame = thisFrame + 1

	// Store the Broadcast data
	m_structEventDebugFrameChecker Event
	
	Event.Details.Type				= SCRIPT_EVENT_DEBUG_FRAME_CHECKER
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	Event.expectedFrame				= nextFrame
	
	// Broadcast the event to local player only (so not across the network)
	// NOTE: Replicating hte 'SPECIFIC_PLAYER' function for generating player bits but without the debug output to prevent spamming
	INT playerBits = 0
	SET_BIT(playerBits , NATIVE_TO_INT(PLAYER_ID()))
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), playerBits)

ENDPROC
#ENDIF
#IF IS_DEBUG_BUILD
FUNC STRING PRINT_CLIENT_GAME_STATE(INT iStage)
	SWITCH iStage 
		CASE MAIN_GAME_STATE_INI 					 	RETURN "STATE_INI"
		CASE MAIN_GAME_STATE_WAIT						RETURN "STATE_WAIT"
		CASE MAIN_GAME_STATE_INITIAL_CAMERA_WORK 		RETURN "STATE_INITIAL_CAMERA_WORK"
		CASE MAIN_GAME_STATE_FIRST_SPAWN 				RETURN "STATE_FIRST_SPAWN"
		CASE MAIN_GAME_STATE_RUNNING					RETURN "STATE_RUNNING"
		CASE MAIN_GAME_STATE_END						RETURN "STATE_END"
		CASE MAIN_GAME_STATE_ALL_LEAVE				 	RETURN "STATE_ALL_LEAVE"
		CASE MAIN_GAME_STATE_SWAP_TEAM				 	RETURN "STATE_SWAP_TEAM"
		CASE MAIN_GAME_STATE_CHOOSE_ALTERNATIVE_TEAM	RETURN "STATE_CHOOSE_ALTERNATIVE_TEAM "
		CASE MAIN_GAME_STATE_RUN_LOBBY				 	RETURN "STATE_RUN_LOBBY"
		CASE MAIN_GAME_STATE_HOLD_FOR_TRANSITION		RETURN "STATE_HOLD_FOR_TRANSITION"
		CASE MAIN_GAME_STATE_RUN_TRANSITION			 	RETURN "STATE_RUN_TRANSITION"
	ENDSWITCH
	RETURN ""
ENDFUNC
#ENDIF



FUNC BOOL OK_TO_DO_NON_MISSION_HELP(BOOL bCheckOnAmbient = TRUE, 
									BOOL bCheckDoingWanted = TRUE, 
									BOOL bCheckReducedContent = TRUE, 
									BOOL bCHeckOnMission = TRUE, 
									BOOL bCheckForConv = TRUE,
									BOOL bCheckForPostMission = TRUE,
									BOOL bCheckPostHstInt = TRUE,
									BOOL bCheckSaveCLothesHelp = TRUE,
									BOOL bCheckForCritical = FALSE,
									BOOL bCheckForPostLR = TRUE,
									BOOL bCheckPostYcht = TRUE)


	UNUSED_PARAMETER(bCheckPostHstInt)
	UNUSED_PARAMETER(bCheckSaveCLothesHelp)
	UNUSED_PARAMETER(bCheckPostYcht)

	#IF IS_DEBUG_BUILD
		BOOL  bTimeDebeug
		IF NOT HAS_NET_TIMER_STARTED(timeNmhDebug)
			START_NET_TIMER(timeNmhDebug)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(timeNmhDebug, 3000)
				RESET_NET_TIMER(timeNmhDebug)
				bTimeDebeug = TRUE
			ENDIF
		ENDIF
	#ENDIF
	
	IF bCHeckOnMission
		IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
			#IF IS_DEBUG_BUILD
				IF bWdDoNmhDebug OR bTimeDebeug
					NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as IS_PLAYER_ON_ANY_FM_MISSION") NET_NL()
				ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
		
		//Count players in airlock transitions as on mission
		IF IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
			#IF IS_DEBUG_BUILD
				IF bWdDoNmhDebug OR bTimeDebeug
					NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING") NET_NL()
				ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_THIS_FM_EVENT(PLAYER_ID(), FMMC_TYPE_BUSINESS_BATTLES)
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as player is permanent to a business battle)") NET_NL()
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF

	IF bCheckReducedContent
		IF HAS_ENTERED_GAME_WITH_REDUCED_CONTENT_DUE_TO_CLOUD()
			#IF IS_DEBUG_BUILD
				IF bWdDoNmhDebug
				OR bTimeDebeug
					NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE because HAS_ENTERED_GAME_WITH_REDUCED_CONTENT_DUE_TO_CLOUD") NET_NL()
				ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF

	
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug
			OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as IS_HELP_MESSAGE_BEING_DISPLAYED") NET_NL()
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	
	IF IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(player_id())
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug
			OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as IS_PLAYER_ENTERING_OR_EXITING_PROPERTY") NET_NL()
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF

	
	IF bCheckDoingWanted
		IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_DOING_WANTED_HELP)
			#IF IS_DEBUG_BUILD
				IF bWdDoNmhDebug
				OR bTimeDebeug
					NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as BI_FM_NMH5_DOING_WANTED_HELP") NET_NL()
				ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF bCheckForConv
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			#IF IS_DEBUG_BUILD
				IF bWdDoNmhDebug OR bTimeDebeug
					NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as IS_ANY_CONVERSATION_ONGOING_OR_QUEUED") NET_NL()
				ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF bCheckForPostMission
		IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_POST_MISS_CASH)
			#IF IS_DEBUG_BUILD
				IF bWdDoNmhDebug OR bTimeDebeug
					NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as BI_FM_NMH7_POST_MISS_CASH") NET_NL()
				ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_SCREEN_FADED_IN()
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as IS_SCREEN_FADED_IN") NET_NL()
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF g_bAtmShowing
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as g_bAtmShowing") NET_NL()
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as IS_CUSTOM_MENU_ON_SCREEN") NET_NL()
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as IS_FM_MISSION_LAUNCH_IN_PROGRESS") NET_NL()
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF

	IF bCheckOnAmbient
		IF IS_PLAYER_ON_ANY_MP_AMBIENT_SCRIPT(PLAYER_ID())
			#IF IS_DEBUG_BUILD
				IF bWdDoNmhDebug OR bTimeDebeug
					NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as IS_PLAYER_ON_ANY_MP_AMBIENT_SCRIPT") NET_NL()
				ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_MISSION_SUMMARY_SCREEN_DISPLAYED()
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as IS_MISSION_SUMMARY_SCREEN_DISPLAYED") NET_NL()
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF unlockFmPhoneCall.bDoPhoneCall
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as unlockFmPhoneCall.bDoPhoneCall") NET_NL()
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as IS_PED_INJURED") NET_NL()
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as IS_PAUSE_MENU_ACTIVE") NET_NL()
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as IS_PLAYER_CONTROL_ON") NET_NL()
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF

	IF IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE") NET_NL()
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as MPAM_TYPE_CINEMA") NET_NL()
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()	
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as IS_SKYSWOOP_AT_GROUND") NET_NL()
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
		IF IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()	
			#IF IS_DEBUG_BUILD
				IF bWdDoNmhDebug OR bTimeDebeug
					NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as IS_ANY_BIG_MESSAGE_BEING_DISPLAYED") NET_NL()
				ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_Show_Lap_Dpad
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as g_Show_Lap_Dpad") NET_NL()
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	
	
	IF SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED") NET_NL()
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF IS_TRANSITION_SESSION_LAUNCHING()
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as IS_TRANSITION_SESSION_LAUNCHING") NET_NL()
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as IS_PLAYER_IN_CORONA") NET_NL()
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		//-- Internet active
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as g_bBrowserVisible") NET_NL()
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF Is_Invite_Being_Accepted()
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as Is_Invite_Being_Accepted") NET_NL()
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF Is_There_A_MissionsAtCoords_Focus_Mission()
	AND bCHeckOnMission
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as Is_There_A_MissionsAtCoords_Focus_Mission") NET_NL()
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	

	
	IF bCheckForCritical
		IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
			#IF IS_DEBUG_BUILD
				IF bWdDoNmhDebug OR bTimeDebeug
					NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as IS_PLAYER_CRITICAL_TO_ANY_EVENT") NET_NL()
				ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_RUNNING_A_POST_CELEBRATION_SCENE(PLAYER_ID())
		#IF IS_DEBUG_BUILD
			IF bWdDoNmhDebug OR bTimeDebeug
				NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as IS_PLAYER_RUNNING_A_POST_CELEBRATION_SCENE") NET_NL()
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	
	IF bCheckForPostLR
			IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet11, BI_FM_NMH11_DO_LR_POST_MISSION_HELP)
				IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet11, BI_FM_NMH11_DONE_LR_POST_MISSION_HELP2)
					#IF IS_DEBUG_BUILD
						IF bWdDoNmhDebug OR bTimeDebeug
							NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] FALSE as bCheckForPostLR") NET_NL()
						ENDIF
					#ENDIF
		
					RETURN FALSE
				ENDIF
			ENDIF
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
		IF bWdDoNmhDebug OR bTimeDebeug
			//NET_NL() NET_PRINT("[dsw] [OK_TO_DO_NON_MISSION_HELP] TRUE") NET_NL()
		ENDIF
	#ENDIF

	RETURN TRUE
ENDFUNC


#IF IS_DEBUG_BUILD
PROC DEBUG_DISABLE_PHONE()
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_disable_phone")
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	ENDIF
ENDPROC
#ENDIF



				
// -----------------------------------	STAGE FUNCTIONS
PROC SET_CLIENT_GAME_STATE(INT iStage)
	SWITCH iStage  
		CASE MAIN_GAME_STATE_INI 
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_INI  ", tl31MissionName) NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_INI ")
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_WAIT	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_WAIT  ", tl31MissionName) NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_WAIT ")
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_INITIAL_CAMERA_WORK 	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_INITIAL_CAMERA_WORK  ", tl31MissionName) NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_INITIAL_CAMERA_WORK ")
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_FIRST_SPAWN 			
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_FIRST_SPAWN  ", tl31MissionName) NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_FIRST_SPAWN ")
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_RUNNING
			g_bFinishedInitialSpawn = TRUE //used to check spawn position has been choosen before creating Personal Vehicle
			PRINTLN("Freemode: g_bFinishedInitialSpawn = TRUE")
			#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_RUNNING  ", tl31MissionName) NET_NL()
			#ENDIF	
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_RUNNING ")
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_END	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_END  ", tl31MissionName) NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_END ")
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_ALL_LEAVE	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_ALL_LEAVE  ", tl31MissionName) NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_ALL_LEAVE ")
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_SWAP_TEAM	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_SWAP_TEAM  ", tl31MissionName) NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_SWAP_TEAM ")
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_CHOOSE_ALTERNATIVE_TEAM 
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_CHOOSE_ALTERNATIVE_TEAM  ", tl31MissionName) NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_CHOOSE_ALTERNATIVE_TEAM ")
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_RUN_LOBBY					
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_RUN_LOBBY  ", tl31MissionName) NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_RUN_LOBBY ")
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_HOLD_FOR_TRANSITION					
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_HOLD_FOR_TRANSITION  ", tl31MissionName) NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_HOLD_FOR_TRANSITION ")
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_RUN_TRANSITION					
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_RUN_TRANSITION  ", tl31MissionName) NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_RUN_TRANSITION ")
			#ENDIF
		BREAK
		
		
		DEFAULT
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("SET_CLIENT_GAME_STATE Deathmath.sc invalid option")
			#ENDIF
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	PRINTLN("TRANSITION TIME - FREEMODE - SET_CLIENT_GAME_STATE              - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ") - ", PRINT_CLIENT_GAME_STATE(iStage) , ")")	
	#ENDIF
	#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("TRANSITION TIME - FREEMODE - SET_CLIENT_GAME_STATE              - FRAME COUNT =", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ") - ", iStage , ")")
	#ENDIF
			
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iGameState = iStage
ENDPROC

PROC SET_SERVER_GAME_STATE(INT iStage)

	#IF IS_DEBUG_BUILD
		NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SET_SERVER_GAME_STATE Called on client by mistake")
	#ENDIF

	SWITCH iStage
		CASE MAIN_GAME_STATE_INI 
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_INI  ", tl31MissionName) NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_INI (Freemode) ")
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_WAIT	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_WAIT  ", tl31MissionName) NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_WAIT (Freemode) ")
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_INITIAL_CAMERA_WORK 	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_INITIAL_CAMERA_WORK  ", tl31MissionName) NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_INITIAL_CAMERA_WORK (Freemode) ")
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_FIRST_SPAWN 			
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_FIRST_SPAWN  ", tl31MissionName) NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_FIRST_SPAWN (Freemode) ")
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_RUNNING	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_RUNNING  ", tl31MissionName) NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_RUNNING (Freemode) ")
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_END	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_END  ", tl31MissionName) NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_END (Freemode) ")
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_ALL_LEAVE	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_ALL_LEAVE  ", tl31MissionName) NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_ALL_LEAVE (Freemode) ")
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_SWAP_TEAM	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_SWAP_TEAM  ", tl31MissionName) NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_SWAP_TEAM (Freemode) ")
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_CHOOSE_ALTERNATIVE_TEAM 
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_CHOOSE_ALTERNATIVE_TEAM  ", tl31MissionName) NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_CHOOSE_ALTERNATIVE_TEAM (Freemode) ")
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_RUN_LOBBY					
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_RUN_LOBBY  ", tl31MissionName) NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_RUN_LOBBY (Freemode) ")
			#ENDIF
		BREAK
		DEFAULT
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("SET_SERVER_GAME_STATE Deathmath.sc invalid option")
			#ENDIF
		BREAK
	ENDSWITCH
			
	GlobalServerBD.iServerGameState = iStage
ENDPROC

FUNC BOOL SHOULD_CALL_SET_SKYSWOOP_DOWN_FOR_DO_FINAL_PRE_GAME_STATE_RUNNING_CHECKS()
	IF (SHOULD_TRANSITION_SESSIONS_RUN_FREEMODE_RELAUNCH()
	OR TRANSITION_SESSIONS_FREEMODE_DONT_PULL_DOWN_CAM())
	AND NETWORK_IS_ACTIVITY_SESSION()
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PMC_OWNING_SKYCAM()

	IF NATIVE_TO_INT(PLAYER_ID()) >= 0 
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].BbNeedsFmCleanup 
			CPRINTLN(DEBUG_TRANSITION, "[DO_FINAL_PRE_GAME_STATE_RUNNING_CHECKS] IS_PMC_OWNING_SKYCAM = TRUE ")
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Extracted out all these checks into a a more readable function.
FUNC BOOL DO_FINAL_PRE_GAME_STATE_RUNNING_CHECKS()
	IF DID_I_JOIN_MISSION_AS_SPECTATOR()	//to allow jip players to start immediately without waiting for skycam
		CPRINTLN(DEBUG_TRANSITION, "[DO_FINAL_PRE_GAME_STATE_RUNNING_CHECKS] True because DID_I_JOIN_MISSION_AS_SPECTATOR")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION()	//to allow jip players to start immediately without waiting for skycam
		CPRINTLN(DEBUG_TRANSITION, "[DO_FINAL_PRE_GAME_STATE_RUNNING_CHECKS] True because SHOULD_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION")
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_A_ROUNDS_MISSION()	//to allow jip players to start immediately without waiting for skycam
		CPRINTLN(DEBUG_TRANSITION, "[DO_FINAL_PRE_GAME_STATE_RUNNING_CHECKS] True because IS_THIS_A_ROUNDS_MISSION")
		RETURN TRUE
	ENDIF
	
	//Allow players on a playlist to progress immediately without waiting for skycam
	IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
		CPRINTLN(DEBUG_TRANSITION, "[DO_FINAL_PRE_GAME_STATE_RUNNING_CHECKS] True because IS_THIS_A_ROUNDS_MISSION")
		RETURN TRUE
	ENDIF
	
	IF NOT HAS_FM_TRIGGER_TUT_BEEN_DONE()
		//-- Need to do either the tutorial race or mission. Tutorial will bring down the cam - ok to move on
		CPRINTLN(DEBUG_TRANSITION, "[DO_FINAL_PRE_GAME_STATE_RUNNING_CHECKS] True because need to do Race / mission tutorial")
		RETURN TRUE
	ENDIF
	
	//Move on if we don't need to take the cam down so we are in the running state
	IF TRANSITION_SESSIONS_FREEMODE_DONT_PULL_DOWN_CAM()
		CPRINTLN(DEBUG_TRANSITION, "[DO_FINAL_PRE_GAME_STATE_RUNNING_CHECKS] True because TRANSITION_SESSIONS_FREEMODE_DONT_PULL_DOWN_CAM")
		RETURN TRUE
	ENDIF
	
	//Move on if we don't need to take the cam down so we are in the running state
	IF AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_SP()
		CPRINTLN(DEBUG_TRANSITION, "[DO_FINAL_PRE_GAME_STATE_RUNNING_CHECKS] True because AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_SP")
		RETURN TRUE
	ENDIF
	
	IF IS_SKYSWOOP_AT_GROUND()
		//-- Sky cam is already pulled down
		CPRINTLN(DEBUG_TRANSITION, "[DO_FINAL_PRE_GAME_STATE_RUNNING_CHECKS] True because sky swoop is at ground")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_PMC_OWNING_SKYCAM() 
		IF SHOULD_CALL_SET_SKYSWOOP_DOWN_FOR_DO_FINAL_PRE_GAME_STATE_RUNNING_CHECKS()
			IF SET_SKYSWOOP_DOWN(NULL, (NOT IS_PLAYER_IN_CORONA()))
				//-- Skycam has been pulled down.
				CPRINTLN(DEBUG_TRANSITION, "[DO_FINAL_PRE_GAME_STATE_RUNNING_CHECKS] True because SET_SKYSWOOP_DOWN finished")
				RETURN TRUE
			ENDIF		
		ENDIF
	ELSE
		CPRINTLN(DEBUG_TRANSITION, "[DO_FINAL_PRE_GAME_STATE_RUNNING_CHECKS] True because PMC owns the camera ")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC




PROC CLEAN_UP_OVER_HEAD_GLOBALS()
	INT i
	g_sOverHead.iOverHeadSetUp = 0
	FOR i = 0 TO (NUM_NETWORK_PLAYERS - 1)
		g_sOverHead.ClanTag[i]					= ""
		g_sOverHead.iWantedLevel[i] 			= 0
		g_sOverHead.iOverHeadPlayerNumber[i] 	= 0
		g_sOverHead.iPlayerRole[i]  			= 0
		g_sOverHead.bPlayerIsAlive[i]  			= FALSE
		
		CLEANUP_ABOVE_HEAD_DISPLAY_JUST_SCALEFORM(i)
	ENDFOR
	RESET_ALL_OVERHEAD_BITSETS()
ENDPROC


FUNC BOOL IS_TIME_EQUAL(INT CurrentHours, INT CurrentMinutes, INT TargetHours, INT TargetMinutes)

	IF CurrentHours=TargetHours
	AND CurrentMinutes= TargetMinutes
		RETURN TRUE
	ENDIF
RETURN FALSE

ENDFUNC

FUNC BOOL CHANGE_TOD_FOR_LAUNCH_SCRIPT(INT TargetHours, INT TargetMinutes, INT TargetSeconds)

	
	INT iCurrentHours = GET_CLOCK_HOURS()
	INT iCurrentMinutes = GET_CLOCK_MINUTES()
	INT iCurrentSeconds = GET_CLOCK_SECONDS()
	
	NET_NL()NET_PRINT("CHANGE_TOD_FOR_LAUNCH_SCRIPT: iCurrentHours = ")NET_PRINT_INT(iCurrentHours)
	NET_NL()NET_PRINT("CHANGE_TOD_FOR_LAUNCH_SCRIPT: iCurrentMinutes = ")NET_PRINT_INT(iCurrentMinutes)
	NET_NL()NET_PRINT("CHANGE_TOD_FOR_LAUNCH_SCRIPT: iCurrentSeconds = ")NET_PRINT_INT(iCurrentSeconds)
	NET_NL()NET_PRINT("CHANGE_TOD_FOR_LAUNCH_SCRIPT: TargetHours = ")NET_PRINT_INT(TargetHours)
	NET_NL()NET_PRINT("CHANGE_TOD_FOR_LAUNCH_SCRIPT: TargetMinutes = ")NET_PRINT_INT(TargetMinutes)
	NET_NL()NET_PRINT("CHANGE_TOD_FOR_LAUNCH_SCRIPT: TargetSeconds = ")NET_PRINT_INT(TargetSeconds)

	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF IS_TIME_EQUAL(iCurrentHours, iCurrentMinutes, TargetHours,TargetMinutes )
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	INT HoursInterval =  1
	INT MinutesInterval =  1
	INT SecondsInterval =  1

	INT HoursDifference = 24%TargetHours-iCurrentHours
	INT MinutesDifference = 60%TargetMinutes-iCurrentMinutes
	INT SecondsDifference = 60%TargetSeconds-iCurrentSeconds
	
	IF HoursDifference > 0
		HoursInterval = 1
	ELIF HoursDifference > 6
		HoursInterval = 2
	ELIF HoursDifference > 12
		HoursInterval = 4
	ENDIF
	
	IF MinutesDifference > 0
		MinutesInterval = 1
	ELIF MinutesDifference > 10
		MinutesInterval = 2
	ELIF MinutesDifference > 20
		MinutesInterval = 4
	ELIF MinutesDifference > 30
		MinutesInterval = 6
	ELIF MinutesDifference > 40
		MinutesInterval = 8
	ELIF MinutesDifference > 50
		MinutesInterval = 10
	ENDIF
	
	IF SecondsDifference > 0
		SecondsInterval = 1
	ELIF SecondsDifference > 10
		SecondsInterval = 2
	ELIF SecondsDifference > 20
		SecondsInterval = 4
	ELIF SecondsDifference > 30
		SecondsInterval = 6
	ELIF SecondsDifference > 40
		SecondsInterval = 8
	ELIF SecondsDifference > 50
		SecondsInterval = 10
	ENDIF
	
	

	
	IF iCurrentHours != TargetHours
		iCurrentHours += HoursInterval
	ENDIF
	IF iCurrentHours > 23
		iCurrentHours = 0
	ELIF iCurrentHours < 0
	 	iCurrentHours = 23
	ENDIF
	
	IF iCurrentMinutes != TargetMinutes
		iCurrentMinutes += MinutesInterval
	ENDIF
	IF iCurrentMinutes > 60
		iCurrentMinutes = 0
	ELIF iCurrentMinutes < 0
	 	iCurrentMinutes = 60
	ENDIF
	
	IF iCurrentSeconds != TargetSeconds
		iCurrentSeconds += SecondsInterval
	ENDIF
	IF iCurrentSeconds > 60
		iCurrentSeconds = 0
	ELIF iCurrentSeconds < 0
	 	iCurrentSeconds = 60
	ENDIF
	
	SET_CLOCK_TIME(iCurrentHours, iCurrentMinutes, iCurrentSeconds)
	NETWORK_OVERRIDE_CLOCK_TIME(iCurrentHours, iCurrentMinutes, iCurrentSeconds)
	
	
	RETURN FALSE

	
ENDFUNC



FUNC BOOL SET_TIME_OF_DAY_IF_ONLY_PLAYER_INTERP()

	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		PARTICIPANT_INDEX ParticipantID
		INT iNumPlayers = 0
		INT i
		FOR i=0 TO NUM_NETWORK_PLAYERS - 1
			ParticipantID = INT_TO_NATIVE(PARTICIPANT_INDEX, i)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(ParticipantID)
				iNumPlayers++
			ENDIF
		ENDFOR	
		
	
		IF iNumPlayers = 1
			#IF IS_DEBUG_BUILD
				NET_NL()
				NET_PRINT("[dsw] Setting Time of Day as I'm only player....")
				NET_NL()
			#ENDIF
			IF NOT HAS_FREEMODE_INTRO_BEEN_DONE()

				IF CHANGE_TOD_FOR_LAUNCH_SCRIPT(11, 0, 0)
					RETURN TRUE
				ELSE	
					RETURN FALSE
				ENDIF
			
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_TIME_OF_DAY_IF_ONLY_PLAYER()

	PARTICIPANT_INDEX ParticipantID
	INT iNumPlayers = 0
	INT i
	FOR i=0 TO NUM_NETWORK_PLAYERS - 1
		ParticipantID = INT_TO_NATIVE(PARTICIPANT_INDEX, i)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(ParticipantID)
			iNumPlayers++
		ENDIF
	ENDFOR	
	
	
	IF iNumPlayers = 1
		
		IF NOT HAS_FREEMODE_INTRO_BEEN_DONE()
				#IF IS_DEBUG_BUILD
				NET_NL()
				NET_PRINT("[dsw] Setting Time of Day as I'm only player....")
				NET_NL()
			#ENDIF
			IF NETWORK_IS_GAME_IN_PROGRESS()
				SET_CLOCK_TIME(11,0,0)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC



PROC WaitForFreemodeInitScriptToRun()
	
	WHILE NOT (g_FinishedFreemodeInit)
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(FREEMODE_INIT_HASH()) = 0)
			REQUEST_SCRIPT(FREEMODE_INIT_SCRIPT())	
			IF (HAS_SCRIPT_LOADED(FREEMODE_INIT_SCRIPT()))
			    START_NEW_SCRIPT(FREEMODE_INIT_SCRIPT(), MULTIPLAYER_MISSION_STACK_SIZE)
		    	SET_SCRIPT_AS_NO_LONGER_NEEDED(FREEMODE_INIT_SCRIPT())
			ENDIF
		ENDIF
		IF NOT (g_FinishedFreemodeInit)
			NET_PRINT("WaitForFreemodeInitScriptToRun - waiting...") NET_NL()
			WAIT(0)			
		ENDIF
	ENDWHILE
	
ENDPROC

PROC WaitForFreemodeClearGlobalsScriptToRun()
	
	WHILE NOT (g_bFinishedFreemodeClearGlobals)
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("freemode_clearGlobals"))= 0)
			REQUEST_SCRIPT("freemode_clearGlobals")	
			IF (HAS_SCRIPT_LOADED("freemode_clearGlobals"))
			    START_NEW_SCRIPT("freemode_clearGlobals", WAREHOUSE_STACK_SIZE)
		    	SET_SCRIPT_AS_NO_LONGER_NEEDED("freemode_clearGlobals")
			ENDIF
		ENDIF
		IF NOT (g_bFinishedFreemodeClearGlobals)
			NET_PRINT("WaitForFreemodeClearGlobalsScriptToRun - waiting...") NET_NL()
			WAIT(0)			
		ENDIF
	ENDWHILE
	
ENDPROC

// Updated in PROCESS_ENTITY_DAMAGED_EVENT (net_process_events.sch)
PROC GRAB_ARCHENEMY_PLAYER()

	STRING sArchenemy = GET_MP_USERID_CHARACTER_STAT(MP_STAT_ARCHENEMY)
	GAMER_HANDLE gHandle
	PLAYER_INDEX playerId
	  
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sArchenemy)
	AND NOT ARE_STRINGS_EQUAL(sArchenemy, "STAT_UNKNOWN")

		
		gHandle = GET_GAMER_HANDLE_USER(sArchenemy)
		playerId = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(gHandle)
	
		g_i_ArchenemyPlayer = NATIVE_TO_INT(playerId)
		PRINTLN("[ARCHENEMY] GRAB_ARCHENEMY_PLAYER: g_i_ArchenemyPlayer =  ", g_i_ArchenemyPlayer)
	ELSE
		PRINTLN("[ARCHENEMY] GRAB_ARCHENEMY_PLAYER: g_i_ArchenemyPlayer =  NULL ")
	ENDIF
ENDPROC

PROC CLEAR_AREA_OF_SP_VEHICLES()

	IF (g_bHasTransitionSpawnedPlayer)
		IF NOT (bHasClearedArea)	
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				//VECTOR vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
				//NET_PRINT("CLEAR_AREA_OF_SP_VEHICLES - CLEAR_AREA - called with ") NET_PRINT_VECTOR(vPlayerCoords) NET_NL()
				CLEAR_AREA(<<0.0, 0.0, 0.0>>, 100000.0, TRUE)
				NET_PRINT("CLEAR_AREA_OF_SP_VEHICLES - clear_area called") NET_NL()
			ELSE
				NET_PRINT("CLEAR_AREA_OF_SP_VEHICLES - not host. ") NET_NL()	
			ENDIF
			bHasClearedArea = TRUE
		ENDIF
	ENDIF
	
ENDPROC

PROC RESET_GlobalplayerBD_FM_3()
	CPRINTLN(DEBUG_NET_AMBIENT, "[FMEG] - RESET_GlobalplayerBD_FM_3")
	INT iCount
	GlobalPlayerBroadcastDataFM_3 GlobalplayerBD_FM_3_Temp
	FOR iCount = 0 TO (NUM_NETWORK_PLAYERS - 1)
		GlobalplayerBD_FM_3[iCount] = GlobalplayerBD_FM_3_Temp
		
		GlobalPlayerBD_FM_3[iCount].officeHeliDockBD.iBS = 0
		GlobalPlayerBD_FM_3[iCount].officeHeliDockBD.playerOwner = INVALID_PLAYER_INDEX()
		
		RESET_ARRAYED_BIT_SET(GlobalPlayerBD_FM_3[iCount].restrictedInterior.iRestrictedInteriorBitSet)
		GlobalplayerBD_FM_3[iCount].sMagnateGangBossData.fFormationTrigger = g_sMPTunables.fBIKER_RIDE_IN_FORMATION_AREA_FORMATION    
		GlobalplayerBD_FM_3[iCount].sMagnateGangBossData.fDirectionalTrigger = g_sMPTunables.fBIKER_RIDE_IN_FORMATION_AREA_DIRECTIONAL_TRIGGER 
		
		
	ENDFOR
ENDPROC


PROC RESET_GlobalServerBD_FM_events()
	CPRINTLN(DEBUG_NET_AMBIENT, "[FMEG] - RESET_GlobalServerBD_FM_events")
	GB_CLEANUP_ALL_GANG_IDS()
	AML_ServerBroadcastCurrentEventData sAActiveEventDataTemp
	GlobalServerBD_FM_events.sAActiveEventData = sAActiveEventDataTemp
	GlobalServerBD_FM_events.iServerBgScriptBitSet0 = 0
	GlobalServerBD_FM_events.iServerBgScriptBitSet1 = 0
	GlobalServerBD_FM_events.iUniqueSessionHash0 = 0
	GlobalServerBD_FM_events.iUniqueSessionHash1 = 0
ENDPROC

PROC RESET_GlobalplayerBD_FM_PropertyDetails()
	INT iCount
	PROPERTY_BROADCAST_DETAILS emptyStruct
	FOR iCount = 0 TO (NUM_NETWORK_PLAYERS - 1)
		GlobalplayerBD_FM[iCount].propertyDetails		 		= emptyStruct
	ENDFOR
	PRINTLN("...RESET_GlobalplayerBD_FM_PropertyDetails")
ENDPROC

PROC RESET_GANG_BOSS_GAVE_WANTED_STRUCT()
	CPRINTLN(DEBUG_NET_MAGNATE, "[GB_WANT] - RESET_GANG_BOSS_GAVE_WANTED_STRUCT()") 
	sGangBossGaveWantedVars.bGangBossWantedHelpPrinted = FALSE
	sGangBossGaveWantedVars.piPlayerWhoGaveWanted = INVALID_PLAYER_INDEX()
	sGangBossGaveWantedVars.iGaveWantedBitSet = 0
ENDPROC

PROC RESET_GANG_BLIP_NAMES()
	g_sOfficeBlipName = ""
	g_sClubhouseBlipName = ""
ENDPROC
PROC CLEAR_STRUCT_GB_BOSS_DROP_OVERALL()
	PRINTLN("CLEAR_STRUCT_GB_BOSS_DROP_OVERALL()")
	STRUCT_GB_BOSS_DROP_OVERALL g_sBossDropOverallTemp
	g_sBossDropOverall = g_sBossDropOverallTemp
ENDPROC 
PROC RESET_DATA_FOR_GLOBAL_INVITE_FLOW()
	g_iInviteFlowSearchAttempts = 0
	g_iInviteFlowMissionRootId = -3
ENDPROC
PROC SMRD_CLEAR_RIVAL_SMUGGLER_DATA_GLOBAL_INIT()
	SMRD_CLEAR_GLOBAL_RIVAL_MISSION_BITS()

	FREEMODE_DELIVERY_DEACTIVATE_DROPOFF_FOR_RIVAL_PACKAGE()
	SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(FALSE)

	MPGlobalsAmbience.sRivalBusinessDelivery.iBitset = 0
	MPGlobalsAmbience.sRivalBusinessDelivery.eResult = CONTRABAND_TRANSACTION_STATE_DEFAULT

	RESET_NET_TIMER(MPGlobalsAmbience.sRivalBusinessDelivery.ownerLeftTimer)
	SMRD_CLEAR_FREEMODE_DELIVERABLE_DATA()
	
	PRINTLN("[SMRD] SMRD_CLEAR_RIVAL_SMUGGLER_DATA_GLOBAL_INIT - Clearing struct data on entering freemode.") 
ENDPROC

PROC RESET_SCRIPT_EVENT_THROTTLING_VARS()
	PRINTLN("RESET_SCRIPT_EVENT_THROTTLING_VARS()")
	SCRIPT_EVENT_THROTTLING_VARS g_sScriptEventThrottlingTemp
	g_sScriptEventThrottling = g_sScriptEventThrottlingTemp
ENDPROC

PROC PROCESS_PRE_GAME()
	
	//INT i
	
	
	NET_NL()NET_PRINT("PROCESS PREGAME - FREEMODE")NET_NL()
	PRINTLN("[TS] *****************************************************************")
	IF IS_TRANSITION_SESSION_LAUNCHING()
		PRINTLN("[TS] * START - Freemode      - TRANSITION SESSION LAUNCHING - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")
		#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] * START - Freemode      - TRANSITION SESSION LAUNCHING - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")#ENDIF
	ELSE
		PRINTLN("[TS] * START - Freemode      - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")
		#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] * START - Freemode      - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")#ENDIF
	ENDIF
	PRINTLN("[TS] *****************************************************************")
	
	g_FinishedFreemodeInit = FALSE
	
	SET_IMPROMPTU_RIVAL_GLOBAL(INVALID_PLAYER_INDEX())
	RESET_SCRIPT_EVENT_THROTTLING_VARS()
	SET_PED_ABLE_TO_DROWN( PLAYER_PED_ID() )
	CLEAN_DEV_SPEC_GLOBALS()
	CLEAN_FM_EVENT_VARS_GLOBALS()
	CLEAN_FM_EVENT_HELP_VARS_GLOBALS()
	CLEAN_FM_EVENT_INVITE_VARS_GLOBALS()
	RESET_GANG_BOSS_GAVE_WANTED_STRUCT()
	CLEAR_STRUCT_GB_BOSS_DROP_OVERALL()

		
	PRINTLN("g_Private_IsMultiplayerCreatorRunning = ", g_Private_IsMultiplayerCreatorRunning)
	IF g_Private_IsMultiplayerCreatorRunning = TRUE
		g_Private_IsMultiplayerCreatorRunning = FALSE
		SCRIPT_ASSERT("g_Private_IsMultiplayerCreatorRunning = TRUE bug to Bobby CC Brenda please.")
	ENDIF
	grankbarchangedlevelactivatebigmessage = FALSE
	irankbarchangedlevelactivatebigmessagetimer = 0
	//Clear the flag that the tutorials are blocked
	IF DONE_UNLOCK_PHONECALL_FOR_ACTIVITY(FMMC_TYPE_RACE)
		CLEAR_TUTORIAL_INVITES_ARE_BLOCKED()
		SET_LOCAL_PLAYER_COMPLETED_INITIAL_AMBIENT_TUTORIALS(TRUE)
		PRINTLN("Freemode pre game CLEAR_TUTORIAL_INVITES_ARE_BLOCKED")
	ELSE
		PRINTLN("Freemode pre game SET_TUTORIAL_INVITES_ARE_BLOCKED")
		SET_TUTORIAL_INVITES_ARE_BLOCKED()
	ENDIF

	
	PRINTLN("[TS] MAINTAIN_SERVER_MAINTENANCE_TICKER_FEED - g_sServerMaintenanceVars.bDisplayNowMessage = FALSE")
	g_sServerMaintenanceVars.bDisplayNowMessage = FALSE
	
	IF g_sTransitionSessionData.bJustDoneMission
		SET_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_POST_MISS_CASH)
		PRINTLN("[dsw] Freemode pre game - bJustDoneMission is set")
		g_sTransitionSessionData.bJustDoneMission = FALSE
	ENDIF
	
	MPGlobals.g_bRefresh_BD_Stat_Data = TRUE
	
	RESET_CONTROL_MAP_ZOOM()
	
	//if we killed freemode before clear the flag that we did!
	IF DOES_TRANSITION_SESSIONS_NEED_TO_CLEANUP_FM_AFTER_INVITE()
		CLEAR_TRANSITION_SESSIONS_NEED_TO_CLEANUP_FM_AFTER_INVITE()
	ENDIF
	CLEAR_TRANSITION_SESSIONS_PLAYER_VEHICLE_CLEAN_UP_CHECK()
	//Set if this is a private session
	SET_IF_I_JOINED_A_PRIVATE_SESSION()
	
	IF SHOULD_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT()			
		VECTOR vCoords = GET_TRANSITION_SESSION_END_SESSION_POS()
		FLOAT fRadius = 50.0
		IF SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN(FALSE)
			fRadius = 0.1

		ELIF SHOULD_TRANSITION_SESSION_CONSIDER_INTERIORS_ON_RESPAWN()
			fRadius = 0.5
		ENDIF
		SETUP_SPECIFIC_SPAWN_LOCATION(vCoords, 0.0, fRadius, TRUE, 0.0, SHOULD_TRANSITION_SESSION_CONSIDER_INTERIORS_ON_RESPAWN())
		CLEAR_TRANSITION_SESSION_CONSIDER_INTERIORS_ON_RESPAWN()
	ENDIF
	// This makes sure the net script is active, waits untull it is.
	HANDLE_NET_SCRIPT_INITIALISATION(FALSE, ENUM_TO_INT(GAMEMODE_FM))	
	PRINTLN("TRANSITION TIME - FREEMODE - HANDLE_NET_SCRIPT_INITIALISATION ED- FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")		
	SET_MINIMAP_BLOCK_WAYPOINT(FALSE)
	//reserved for personal vehicle??
	RESERVE_NETWORK_MISSION_VEHICLES(1+2)	// need two vehs for the truck and its trailer


	INT i
	REPEAT MAX_BOUNTY_CLAIMS_IN_TEN_SECONDS i
		RESET_NET_TIMER(g_stCheCkForBountyReward[i])
		g_PlayerToCheckForBountyReward[i] = INVALID_PLAYER_INDEX()
		CLEAR_GAMER_HANDLE_STRUCT(g_ghBountyPlacer[i])
	ENDREPEAT
	
	//Reset the vehicle population budget
	IF IS_TUTORIAL_CUTSCENE_GOING_TO_RUN()
		PRINTLN("Don't set SET_VEHICLE_POPULATION_BUDGET(3)")
	ELSE
		SET_VEHICLE_POPULATION_BUDGET(3)
		PRINTLN("SET_VEHICLE_POPULATION_BUDGET(3)")
	ENDIF
	
	
	GlobalServerBD.iServerGameState = 0 // make sure this is set to zero (see 1973005)

	g_iOverheadNamesState = -1
	g_bFM_ON_TEAM_DEATHMATCH = FALSE
	
	//Clean up every thing before the initilisation
	RESET_GlobalplayerBD_FM_2()
	RESET_HAIR_BD()
	RESET_GlobalplayerBD_FM_PropertyDetails()
	INIT_FM_WEAPON_PICKUP_SPAWN_DATA()
	
	RESET_GlobalplayerBD_FM_3()
	RESET_GlobalServerBD_FM_events()
	
	g_amIKickedFromVehicle = FALSE
	RESET_NET_TIMER(g_KickedFromVehicleTimer)
	
	RESET_GANG_BLIP_NAMES()
	
	GB_INITIALISE_GANG_BOSS_LOCAL_DATA(sMagnateGangBossLocalData)
	GB_INITIALISE_GANG_BOSS_LOCAL_GLOBAL_DATA()
	GB_INITIALISE_GANG_BOSS_GLOBAL_CLIENT_DATA()
	GB_INITIALISE_GANG_BOSS_GLOBAL_SERVER_DATA()
	
	INITIALISE_SIMPLE_INTERIOR_GLOBAL_DATA()
	
	
	//Register Vote broadcast data struct.
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(GlobalplayerBD_FM_2,                           SIZE_OF(GlobalplayerBD_FM_2))
	PRINTLN("[TS] NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(GlobalplayerBD_FM_2,             ", SIZE_OF(GlobalplayerBD_FM_2), ")")
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(GlobalplayerBD_Interactions,                   SIZE_OF(GlobalplayerBD_Interactions))
	PRINTLN("[TS] NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(GlobalplayerBD_Interactions,     ", SIZE_OF(GlobalplayerBD_Interactions), ")")
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(GlobalplayerBD_FM,                             SIZE_OF(GlobalplayerBD_FM))
	PRINTLN("[TS] NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(GlobalplayerBD_FM,               ", SIZE_OF(GlobalplayerBD_FM), ")")
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(GlobalplayerBD_MissionName,                    SIZE_OF(GlobalplayerBD_MissionName))
	PRINTLN("[TS] NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(GlobalplayerBD_MissionName,      ", SIZE_OF(GlobalplayerBD_MissionName), ")")
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(GlobalplayerBD_Kicking,                        SIZE_OF(GlobalplayerBD_Kicking))
	PRINTLN("[TS] NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(GlobalplayerBD_Kicking,          ", SIZE_OF(GlobalplayerBD_Kicking), ")")
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(GlobalPlayerBD_SCTV,                           SIZE_OF(GlobalPlayerBD_SCTV))
	PRINTLN("[TS] NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(GlobalPlayerBD_SCTV,             ", SIZE_OF(GlobalPlayerBD_SCTV), ")")
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(g_AMC_playerBD,                                SIZE_OF(g_AMC_playerBD))
	PRINTLN("[TS] NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(g_AMC_playerBD,                  ", SIZE_OF(g_AMC_playerBD), ")")
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(GlobalplayerBD_FM_3,                           SIZE_OF(GlobalplayerBD_FM_3))
	PRINTLN("[TS] NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(GlobalplayerBD_FM_3,             ", SIZE_OF(GlobalplayerBD_FM_3), ")")
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(GlobalplayerBD, 				SIZE_OF(GlobalplayerBD))
	PRINTLN("[TS] NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(GlobalplayerBD, 				", SIZE_OF(GlobalplayerBD), ")")
	
	
	//Register Core broadcast data structs.
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD, 					SIZE_OF(GlobalServerBD))
	PRINTLN("[TS] NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD, 				 ", SIZE_OF(GlobalServerBD), ")")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_BlockB, 			SIZE_OF(GlobalServerBD_BlockB))	
	PRINTLN("[TS] NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_BlockB, 			 ", SIZE_OF(GlobalServerBD_BlockB), ")")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_MissionsShared, 	SIZE_OF(GlobalServerBD_MissionsShared))
	PRINTLN("[TS] NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_MissionsShared,  ", SIZE_OF(GlobalServerBD_MissionsShared), ")")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_MissionRequest, 	SIZE_OF(GlobalServerBD_MissionRequest))	
	PRINTLN("[TS] NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_MissionRequest,  	", SIZE_OF(GlobalServerBD_MissionRequest), ")")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_MissionList, 		SIZE_OF(GlobalServerBD_MissionList)) 
	PRINTLN("[TS] NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_MissionList,  	", SIZE_OF(GlobalServerBD_MissionList), ")")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_ExclusionAreas, 	SIZE_OF(GlobalServerBD_ExclusionAreas))
	PRINTLN("[TS] NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_ExclusionAreas,  	", SIZE_OF(GlobalServerBD_ExclusionAreas), ")")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_ActivitySelector, 	SIZE_OF(GlobalServerBD_ActivitySelector))
	PRINTLN("[TS] NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_ActivitySelector, ", SIZE_OF(GlobalServerBD_ActivitySelector), ")")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_Betting, 			SIZE_OF(GlobalServerBD_Betting))
	PRINTLN("[TS] NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_Betting, 		 ", SIZE_OF(GlobalServerBD_Betting), ")")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_HoldUp, 			SIZE_OF(GlobalServerBD_HoldUp)) 
	PRINTLN("[TS] NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_HoldUp, 		 	", SIZE_OF(GlobalServerBD_HoldUp), ")")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_SyncedInteractions,SIZE_OF(GlobalServerBD_SyncedInteractions))
	PRINTLN("[TS] NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_SyncedInteractions,", SIZE_OF(GlobalServerBD_SyncedInteractions), ")")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_FM, 				SIZE_OF(GlobalServerBD_FM))				//9 - 1524
	PRINTLN("[TS] NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_FM, 				", SIZE_OF(GlobalServerBD_FM), ")")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_Kicking, 			SIZE_OF(GlobalServerBD_Kicking))		//10 - 76
	PRINTLN("[TS] NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_Kicking, 				", SIZE_OF(GlobalServerBD_Kicking), ")")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_PropertyInstances, 	SIZE_OF(GlobalServerBD_PropertyInstances)) // number 13 
	PRINTLN("[TS] NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_PropertyInstances, 				", SIZE_OF(GlobalServerBD_PropertyInstances), ")")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_FM_events, SIZE_OF(GlobalServerBD_FM_events))
	PRINTLN("[TS] NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_FM_events, 				", SIZE_OF(GlobalServerBD_FM_events), ")")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_AsyncWarp, 			SIZE_OF(GlobalServerBD_AsyncWarp))
	PRINTLN("[TS] NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_AsyncWarp, 				", SIZE_OF(GlobalServerBD_AsyncWarp), ")")
	
	
	PRINTLN("BC: PROCESS_PRE_GAME (Freemode) Broadcast Variables Registered ")
	#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("BC: PROCESS_PRE_GAME (Freemode) Broadcast Variables Registered ")
	#ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		GlobalServerBD.iLaunchPosix = GET_CLOUD_TIME_AS_INT()
		PRINTLN("PROCESS PREGAME - FREEMODE, 3540699, GlobalServerBD.iLaunchPosix = ", GlobalServerBD.iLaunchPosix)
	ENDIF

	SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()	//NETWORK_SET_FRIENDLY_FIRE_OPTION(TRUE)
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	//Clear the time
	IF HAS_FM_TRIGGER_TUT_BEEN_DONE()
		SET_TIME_OF_DAY(TIME_OFF, TRUE)
	ELSE
		CPRINTLN(DEBUG_MP_TUTORIAL, "Freemode startup - not clearing clock override")
	ENDIF
	// temp - default hide jobs and events options to FALSE on commandline
	#IF IS_DEBUG_BUILD
		IF NOT g_bRespectedDefaultHideJobBlips
			IF GET_COMMANDLINE_PARAM_EXISTS("defaultHideJobBlips")
				INT iStatBitSet, iStat
				iStatBitSet = 0
				REPEAT ciPI_TYPE_M3_HIDE_JOB_MAX iStat
					CLEAR_BIT(iStatBitSet, iStat)
				ENDREPEAT
				SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_PIM_HIDE_JOBS, iStatBitSet)
				
				iStatBitSet = 0
				REPEAT g_sMPTunables.iciPI_TYPE_M3_HIDE_AMB_MAX iStat
					CLEAR_BIT(iStatBitSet, iStat)
				ENDREPEAT
				SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_PIM_HIDE_AMBIENT, iStatBitSet)			
			ENDIF
			g_bRespectedDefaultHideJobBlips = TRUE
		ENDIF
	#ENDIF
		
	//INIT_STATS() - moved to freemode_init.sc
		
	SET_MAX_WANTED_LEVEL(6)
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_CREATE_RANDOM_COPS(TRUE)
	SET_DITCH_POLICE_MODELS(FALSE)
	
	// switch on emergency services in freemode
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_VEHICLE_REQUEST, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_HELICOPTER, TRUE)
	
	SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), TRUE)
	
	IF DOES_SCENARIO_GROUP_EXIST("MP_POLICE")
      	IF IS_SCENARIO_GROUP_ENABLED("MP_POLICE")
        	SET_SCENARIO_GROUP_ENABLED("MP_POLICE", FALSE)
      	ENDIF
  	ENDIF

	
	#IF IS_DEBUG_BUILD
	IF (b_ScriptMetricsAreRecording)
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		AND NETWORK_IS_GAME_IN_PROGRESS()
			GlobalServerBD.g_bRecordScriptMetrics = TRUE
		ENDIF
	ENDIF
	#ENDIF
	

	
	EXPENSIVE_UPDATE_ALL_HEAVILY_ACCESSED_STATS()
	
	
	SET_AMBIENT_PEDS_DROP_MONEY(TRUE)
	
	//sets how long code waits before calls to other players time out.
	//NETWORK_SESSION_VOICE_SET_TIMEOUT(MP_VOICE_SESSION_TIMEOUT)
	
	SET_AI_WEAPON_DAMAGE_MODIFIER(FREEMODE_AI_DAMAGE)
	SET_AI_MELEE_WEAPON_DAMAGE_MODIFIER(FREEMODE_AI_DAMAGE)
	
	// stop scenario peds spawning in Floyd's appartment
	ADD_SCENARIO_BLOCKING_AREA(<<-1154.964722,-1520.982666, 9.132731>>, <<-1158.964722,-1524.982666,11.632731>>)
	
	// In FM, make sure each player has their own colour, and not the common 
	// team colour.
	USE_PLAYER_COLOUR_INSTEAD_OF_TEAM_COLOUR(TRUE)
	
	// Setup RelGroups
	CREATE_FM_DEFAULT_RELATIONSHIP_GROUPS()
	
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), rgFM_Team[NATIVE_TO_INT(PLAYER_ID())])	//rgFM_DefaultPlayer) //rgFM_Team[PARTICIPANT_ID_TO_INT()])	//Put all players on default team to start with rgFM_DefaultPlayer#
		//SET_CAN_ATTACK_FRIENDLY(PLAYER_PED_ID(), TRUE, TRUE) 
		PRINTLN("SET_CAN_ATTACK_FRIENDLY")
		//SET_PED_CAN_BE_TARGETTED_BY_TEAM(PLAYER_PED_ID(), -1, TRUE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT_STRING_INT("NATIVE_TO_INT(PLAYER_ID()) = ", NATIVE_TO_INT(PLAYER_ID())) NET_NL()
		IF NOT CREATE_NETWORK_BOTS_RELATIONSHIP_GROUPS()
			SCRIPT_ASSERT("NOT CREATE_NETWORK_BOTS_RELATIONSHIP_GROUPS")
		ENDIF
	#ENDIF

	PRINTLN("TRANSITION TIME - FREEMODE - WaitForFreemodeInitScriptToRun   ST- FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")		
	WaitForFreemodeInitScriptToRun()
	//Set that the launcher can run now
	g_bFinishedFreemodeInitFmmcLauncherCanMove = TRUE
	PRINTLN("TRANSITION TIME - FREEMODE - WaitForFreemodeInitScriptToRun   ED- FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")		

	g_b_ReapplyStickySaveFailedFeed = FALSE
	CLEAR_STORED_LAST_PLAYER_VEHICLE_FROM_INVITE()
	

	// Reset kill and death counters when player joins (CS)
	INITIALISE_KILL_AND_DEATH_COUNTERS() 

	//Reset all the previous overhead states. 
	CLEAN_UP_OVER_HEAD_GLOBALS()
	
	// Speirs added 07/07/11 to fix 153666
    // Set frontend player blip name
    BLIP_INDEX playerBlip = GET_MAIN_PLAYER_BLIP_ID()
    IF DOES_BLIP_EXIST(playerBlip)
    	SET_BLIP_NAME_FROM_TEXT_FILE(playerBlip, "BLIP_PLAYER")
    ENDIF
	
		
	SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_INI)
	INIT_STAT_TIMERS()
	
	//Holdups
	INITIALISE_HOLD_UP_TIMERS(GlobalServerBD_HoldUp.HoldUpServerData)
	
	// Is first spawn into session - used in giving player initial weapons (taken from CnC)
	g_MPCharData.bIsFirstSpawn = TRUE
	
	
	g_iMyRaceColor 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_PRIM_COLOUR)
	g_iMyRace2ndColor 	= GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_SECO_COLOUR)
	g_MyTerminateTarget = INVALID_PLAYER_INDEX()
	
	PRINTLN("g_iMyRaceColor       = GET_MP_INT_PLAYER_STAT(MPPLY_FM_PRIM_COLOUR) = ", g_iMyRaceColor)
	PRINTLN("g_iMyRace2ndColor    = GET_MP_INT_PLAYER_STAT(MPPLY_FM_SECO_COLOUR) = ", g_iMyRace2ndColor)
	PRINTLN("SET_VEHICLE_MODEL_IS_SUPPRESSED(JET, TRUE)")
	SET_VEHICLE_MODEL_IS_SUPPRESSED(JET, TRUE)
	//setup_weapon_pickups_Freemode(FM_weapon_pickups)
	TRACK_NUMBER_OF_AWARDS_UNLOCKED()
	
	INITIAL_SET_SC_PRESENCE_SET_ACTIVITY_RATING() //bug 1405771
	
	setup_hidden_package_pickups_Freemode(FM_hidden_package)

	
	RESETTING_RICH_PRESENCES_VALUES()
	
	//Cleanup kill cam (as if it's running now something has gone wrong and it will disable control)
	CLEANUP_KILL_CAM()
	
	IF GET_MP_BOOL_PLAYER_STAT(MPPLY_TOGGLE_OFF_BIG_RADAR) = FALSE
		SET_DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(TRUE)
		NET_NL()NET_PRINT("PROCESS_PRE_GAME: DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(TRUE)")
	ELSE
		SET_DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(FALSE)
		NET_NL()NET_PRINT("PROCESS_PRE_GAME: DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(FALSE)")
	ENDIF

	
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SETUP_FM_CAR_GENS(FM_CAR_GENERATORS_CAR_GENS)
	ENDIF
	
	//Clearing this flag for - 1429764 [PT] Player became stuck attempting to JIP a race in progress
	SET_EVENT_TRANSITION_END_LAUNCH_HAPPENED(FALSE)
	
	//Clear flag if we are coming in from being SCTV.
	IF IS_TRANSITION_SESSION_RELAUNCHING_FREEMODE_AS_SCTV()
		CLEAR_TRANSITION_SESSION_RELAUNCHING_FREEMODE_AS_SCTV()
	ENDIF
	
	//For 1427824, make sure nothing is left of the spectator mode from previous sessions
	CLEAR_SPECTATOR_CAM_FOR_FREEMODE_PRE_GAME()
	
	//To allow reporting to be refreshed in teh heavily accessed stats, a bit more frequently.  
	ALLOW_EXPENSIVE_UPDATE_ALL_HEAVILY_ACCESSED_STATS()
	EXPENSIVE_UPDATE_ALL_HEAVILY_ACCESSED_STATS()
	
	IF NOT IS_TRANSITION_SESSION_LAUNCHING()
	AND NOT IS_TRANSITION_SESSION_KILLING_SESSION()
		SET_PACKED_STAT_BOOL(PACKED_MP_weapons_and_pickups_have_been_created , FALSE)
	ENDIF
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		IF NOT DID_A_BAIL_HAPPEN()
			PRINTLN("Freemode Launch: Failed to receive initial network broadcast. Cleaning up and quitting MP.")
			#IF USE_FINAL_PRINTS 
				PRINTLN_FINAL("Freemode Launch: Failed to receive initial network broadcast. Cleaning up and quitting MP. ")
			#ENDIF
			SET_QUIT_MP_CORE_SCRIPT_CRASH_SET(TRUE) //Inform the transition scripts why we are quitting MP.
			IF NETWORK_CAN_BAIL()
				NETWORK_BAIL(ENUM_TO_INT(NETWORK_BAIL_QUIT_MP_CORE_SCRIPT_CRASH_SET_FREEMODE))
				PRINTLN("Freemode Launch: Failed to receive initial network broadcast. NETWORK_BAIL() - called ")
				#IF USE_FINAL_PRINTS 
					PRINTLN_FINAL("Freemode Launch: Failed to receive initial network broadcast. NETWORK_BAIL() - called ")
				#ENDIF
			ENDIF
		ELSE
			PRINTLN("Freemode Launch: Failed to receive initial network broadcast. But DID_A_BAIL_HAPPEN = TRUE so will clean up soon.")
		ENDIF
	ENDIF
	
	IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_USING_HAIR_SA_STAT)
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMyCurrentHair 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA) 
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHairTintPrime 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT)
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHairTintSecondary 	= GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT)
	ELSE
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMyCurrentHair 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO) 
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHairTintPrime 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT)
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHairTintSecondary 	= GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT)
	ENDIF
	
	PED_COMP_NAME_ENUM eMyCurrentHair = INT_TO_ENUM(PED_COMP_NAME_ENUM, GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMyCurrentHair)
	IF (eMyCurrentHair != DUMMY_PED_COMP)
		PED_COMP_NAME_ENUM eGRHairItem = DUMMY_PED_COMP
		IF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01)
			eGRHairItem = GET_MALE_HAIR(eMyCurrentHair)
		ELIF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01)
			eGRHairItem = GET_FEMALE_HAIR(eMyCurrentHair)
		ENDIF
		
		IF (eGRHairItem != DUMMY_PED_COMP)
		AND (eMyCurrentHair != eGRHairItem)
			PRINTLN("[Freemode Launch] gr_hair: replacing hair enum ", eMyCurrentHair, " with gunrunning hair enum ", eGRHairItem)
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMyCurrentHair = ENUM_TO_INT(eGRHairItem)
		ENDIF
	ENDIF
	
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
		PRINTLN("[CLUBHOUSE ", GET_PLAYER_NAME(PLAYER_ID()), "] PROCESS_PRE_GAME - reset timestamp for host")
		GlobalServerBD_FM.ClubhouseMissionData.timestamp = NULL
	ENDIF
	
	GB_CLEAR_PC_TEXT_CHAT_SETTINGS()
	
	// stuff that needs initialised straight after we have received the first broadcast data.
	IF NETWORK_IS_GAME_IN_PROGRESS()//Added to stop it asserting
		UPDATE_ACTIVE_PLAYERS_IN_MP_LAUNCH_SCRIPT()  // this must be called AFTER the wait_for_first_network_broadcast
		IF TRANSITION_SESSIONS_QUICK_JOB_JOINING_LAUNCHED()
		AND NETWORK_IS_ACTIVITY_SESSION()
			SET_LOCAL_PLAYER_JOINED_IN_PROGRESS_MATCH()
		ENDIF
	ENDIF
	
	
	SET_ACTIVE_PLAYERS()
	RESET_IMPROMPTU_GLOBALS()
	PRINTLN("g_iMpPauseMenuBS_LoadedMissions = 0")
	g_iMpPauseMenuBS_LoadedMissions = 0 
	g_bSyncedBroadcastFromFreemode = TRUE
	
	//Set Start Time
	gTimeSessionStarted = GET_NETWORK_TIME()
		
	#IF IS_DEBUG_BUILD
		IF (GET_COMMANDLINE_PARAM_EXISTS("sc_noWantedLevel"))
			SET_MAX_WANTED_LEVEL(0)
			PRINTLN("sc_noWantedLevel is set") 
		ENDIF
	#ENDIF
	
	BOOL bSetAward = FALSE
	IF IS_BIT_SET(g_iFmNmhBitSet8, BI_FM_NMH8_AWARD_OFF_PLANE)
		bSetAward = TRUE
	ENDIF
	g_iFmNmhBitSet8 = 0
	
	IF bSetAward
		SET_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_AWARD_OFF_PLANE)
		bSetAward = FALSE
	ENDIF
	
	IF g_intRoadTunnels[0] = NULL
	OR g_intTunnels[0] = NULL
		POPULATE_ROAD_TUNNEL_INTERIOR_INDEX_LIST()
		POPULATE_TUNNEL_INTERIOR_INDEX_LIST()
	ENDIF
	

	
	IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet3, BI_FM_NMH3_NO_CASH_DISPLAY)
		INT iPlayerXp = GET_PLAYER_XP(PLAYER_ID())
		INT iPlayerRank = GET_RANK_FROM_XP_VALUE(iPlayerXp, TRUE)
		PRINTLN("[dsw] Freemode pre-game - iPlayerRank - from xp = ", iPlayerRank)
		IF iPlayerRank >= 8
			SET_BIT(MPGlobalsAmbience.iFmNmhBitSet3, BI_FM_NMH3_NO_CASH_DISPLAY)
			ALLOW_DISPLAY_OF_MULTIPLAYER_CASH_TEXT(FALSE) // 1635472
			PRINTLN("[dsw] Freemode pre-game - Calling ALLOW_DISPLAY_OF_MULTIPLAYER_CASH_TEXT(FALSE)")
		ENDIF
	ENDIF	
	
	IF NOT IS_TRANSITION_SESSION_LAUNCHING()

		//-- Reset Race tutorial bitset
		CPRINTLN(DEBUG_MP_TUTORIAL,"[dsw] Resetting tutorial bitset...") 
		g_iMyRaceTutorialBitset = 0
		
		//-- Reset Hold up tutorial bitset
		g_iMyHoldUpTutorialBitset = 0
		
		DISABLE_PERSONAL_VEHICLE_CREATION_FOR_TRANSITION(FALSE)
		PRINTLN("Freemode Launch: NOT IS_TRANSITION_SESSION_LAUNCHING() called DISABLE_PERSONAL_VEHICLE_CREATION_FOR_TRANSITION(FALSE)")
	ELSE
		//-- Restore Race tutorial bitset
		CPRINTLN(DEBUG_MP_TUTORIAL, "[dsw] Restoring tutorial bitset...")
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset = g_iMyRaceTutorialBitset

		//-- Restore hold up tutorial bitset
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset = g_iMyHoldUpTutorialBitset
		PRINTLN("Freemode Launch:IS_TRANSITION_SESSION_LAUNCHING()")
	ENDIF
	
	GRAB_ARCHENEMY_PLAYER()	
	
	//Set the flag that we need to run the post mission clean up
	//Only if we are not launching a transition session
	IF NOT IS_TRANSITION_SESSION_LAUNCHING()
	AND NOT TRANSITION_SESSIONS_END_RESERVING_SLOT_ACEPTING_INVITE()
		//and these flags are true:
		IF (SHOULD_TRANSITION_SESSIONS_RESTART_AFTER_DO_TRANSITION_TO_NEW_GAME() AND NOT SHOULD_TRANSITION_SESSIONS_RESTART_PLAYLIST())
		OR SHOULD_SKIP_WAIT_FOR_TRANSITION_CAMERA()
		OR SHOULD_TRANSITION_SESSIONS_RESTART_AFTER_DO_TRANSITION_TO_NEW_GAME()
			#IF IS_DEBUG_BUILD
				IF (SHOULD_TRANSITION_SESSIONS_RESTART_AFTER_DO_TRANSITION_TO_NEW_GAME() AND NOT SHOULD_TRANSITION_SESSIONS_RESTART_PLAYLIST())
					PRINTLN("[TS] PROCESS PREGAME - FREEMODE - (SHOULD_TRANSITION_SESSIONS_RESTART_AFTER_DO_TRANSITION_TO_NEW_GAME() AND NOT SHOULD_TRANSITION_SESSIONS_RESTART_PLAYLIST())")
				ENDIF
				IF SHOULD_SKIP_WAIT_FOR_TRANSITION_CAMERA() 
					PRINTLN("[TS] PROCESS PREGAME - FREEMODE - SHOULD_SKIP_WAIT_FOR_TRANSITION_CAMERA")
				ENDIF
				IF SHOULD_TRANSITION_SESSIONS_RESTART_AFTER_DO_TRANSITION_TO_NEW_GAME() 
					PRINTLN("[TS] PROCESS PREGAME - FREEMODE - SHOULD_TRANSITION_SESSIONS_RESTART_AFTER_DO_TRANSITION_TO_NEW_GAME")
				ENDIF
			#ENDIF
			//And not SCTV
			IF NOT IS_PLAYER_SCTV(PLAYER_ID())
			AND NOT TRANSITION_SESSIONS_QUICK_JOB_JOINING_LAUNCHED()
				SET_CURRENT_FM_MISSION_NEEDS_CLEANUP(TRUE)
			ENDIF
		ENDIF
		//Force the post mission clean up
		IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bCleanUpReadyToMoveOn = TRUE
        AND g_TransitionSessionNonResetVars.sPostMissionCleanupData.eStage =  ePOSTMISSIONCLEANUPSTAGE_FREEMODE_DEAD
			PRINTLN("TRANSITION SESSIONS [WJK] - PROCESS PREGAME - FREEMODE - bCleanUpReadyToMoveOn and ePOSTMISSIONCLEANUPSTAGE_FREEMODE_DEAD calling SET_CURRENT_FM_MISSION_NEEDS_CLEANUP")
			SET_CURRENT_FM_MISSION_NEEDS_CLEANUP(TRUE)		
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.iMaintainStage = 1
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPostActivityCleanupComplete = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSkipTransitionCam = TRUE
		ENDIF
	ENDIF
	
	//-- Block photographers outside high-nmd apartment building - 1638063
	scenOutsideApartment = ADD_SCENARIO_BLOCKING_AREA(<<-784.5853, 286.8743, 80.0 >>, <<-753.2069, 306.6590, 90.0>>)
	
	
	//if we are restarting a PL then set that FLAG!
	IF SHOULD_TRANSITION_SESSIONS_RESTART_PLAYLIST()
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_CleanUp)
		PRINTLN("[TS] PROCESS PREGAME - FREEMODE - SHOULD_TRANSITION_SESSIONS_RESTART_PLAYLIST - SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_CleanUp)")
	ENDIF
	
	//set a player flag the the playlist is restarting. 
	IF IS_TRANSITION_SESSION_LAUNCHING()		
		SET_IM_RESTARTING_AFTER_LAUNCHING_TRANSITION_SESSION()
	ENDIF
	
	//Re initililise the players job points
	REINITIALISE_JOB_POINTS()
	
	
	//Make Sure Player is put into Passive Mode if flag is set
	IF g_PassiveModeStored.bEnabled = TRUE
		IF NOT NETWORK_IS_ACTIVITY_SESSION()
		AND NOT HAS_TRANSITION_SESSION_SELECTED_RESTART_RANDOM()
		AND NOT IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(PLAYER_ID())
			PRINTLN(" FREEMODE - PROCESS PREGAME -  g_PassiveModeStored.bEnabled = TRUE - PUT PLAYER INTO PASSIVE MODE")
			ENABLE_MP_PASSIVE_MODE(TRUE)
		ENDIF		
	ENDIF
	
	//Added as part of Bug 1657955
	IF IS_MP_DECORATOR_BIT_SET(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)
		CLEAR_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)
	ENDIF
	
	// initialise the broadcast mental state
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].fMentalState = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PLAYER_MENTAL_STATE)
	
	INIT_GRIEF_PASSIVE()
	
	REQUEST_SCRIPT_AUDIO_BANK("DLC_GTAO/SNACKS", FALSE)	//Adding for Bug 2164867
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
		GlobalServerBD_FM_events.sAActiveEventData.iEvent = FMMC_TYPE_INVALID
		CPRINTLN(DEBUG_NET_AMBIENT, "[FMEG] - GlobalServerBD_FM_events.sAActiveEventData.iEvent = FMMC_TYPE_INVALID")
	ENDIF
	//Set flag to run tunable check
	g_bRunTunableValidationChecks = TRUE
	
	// Cleanup Beast player ID
	BROADCAST_HUNT_THE_BEAST_UPDATE_GLOBAL(INVALID_PLAYER_INDEX(), ALL_PLAYERS())
	
	IF g_iInviteFlowLastInvitePosix = -1
		g_iInviteFlowLastInvitePosix = GET_CLOUD_TIME_AS_INT()
		PRINTLN("[FM] [INVITE_FLOW] [RE] PROCESS_PRE_GAME - Local just logged in at POSIX time: ", g_iInviteFlowLastInvitePosix)
	ELSE
		PRINTLN("[FM] [INVITE_FLOW] [RE] PROCESS_PRE_GAME - Local switched session. Last invite sent at POSIX time: ", g_iInviteFlowLastInvitePosix)
	ENDIF
	RESET_DATA_FOR_GLOBAL_INVITE_FLOW()
	
	SMRD_CLEAR_RIVAL_SMUGGLER_DATA_GLOBAL_INIT()
	
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
	AND NOT IS_TRANSITION_SESSION_LAUNCHING()
		RESET_iCurrentArrayPosProfessional()
	ENDIF
	CV2_SET_INITIAL_POSIX_TIME()
	

	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MP_CNC1, "[dsw] [FREEMODE_PRE_GAME] Debug build..")
		IF g_bFmEventHelpReset = FALSE
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_FmEventHelpReset")
				INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT7)
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM AME] - GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT7) = ", iStatInt, " - sc_FmEventHelpReset")
				CLEAR_BIT(iStatInt, biNmh7_FmEventsFirst1)
				CLEAR_BIT(iStatInt, biNmh7_FmEventsFirst2)
				CLEAR_BIT(iStatInt, biNmh7_FmEventsFirst3)
				CLEAR_BIT(iStatInt, biNmh7_FmEventsShare1)
				CLEAR_BIT(iStatInt, biNmh7_FmEventsShare2)
				CLEAR_BIT(iStatInt, biNmh7_FmEventsShare3)
				CLEAR_BIT(iStatInt, biNmh7_FmEventsShare4)
				CLEAR_BIT(iStatInt, biNmh7_FmEventsShare5)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT7, iStatInt)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_HIDECORONAHELPTEXT, 0)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_DSP_CTL_PLAYED_BIT_SET, 0)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NUM_FM_EVENTS_PLAYED, 0)
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM AME] - SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT7, ", iStatInt, ") - sc_FmEventHelpReset")
			ENDIF
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_LowFlowReset")
				CPRINTLN(DEBUG_NET_AMBIENT, "[LOWFLOW] - sc_LowFlowReset")
				CLEAR_FM_FLOW_PROGRESS_VARS()
				SET_FM_STRAND_PROGRESS(ciFLOW_STRAND_LOW_RIDER, 0)
				SET_FM_STRAND_CALL_PROGRESS(ciFLOW_STRAND_LOW_RIDER, 0)
				SET_UNLOCK_PHONECALL_FOR_ACTIVITY_THIS_SESSION(unlockFmPhoneCall, FMMC_TYPE_LOWRIDER_INTRO, FALSE)
				CLEAR_UNLOCK_PHONE_CALL_FOR_ACTIVITY(FMMC_TYPE_LOWRIDER_INTRO)
				SET_LOCAL_PLAYER_FINISHED_LOWRIDER_INTRO_CUTSCENE_BEEN_DONE(FALSE)
				MPGlobalsAmbience.bInitLowriderCutLaunch = FALSE
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HAS_WATCHED_BENNY_CUTSCE, FALSE) 	
				SET_MP_INT_CHARACTER_STAT(MP_STAT_LOWRIDER_FLOW_COMPLETE, 0)
				//Don't clear the super mod status if the unlock command line is there. 
				IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_UnlockSuperMod")
					IF IS_SHOP_AVAILABLE(CARMOD_SHOP_SUPERMOD)
						SET_CAR_MOD_SUPERMOD_GARAGES_FOR_FM(FALSE)
					ENDIF
					SET_UNLOCK_PHONECALL_FOR_ACTIVITY_THIS_SESSION(unlockFmPhoneCall, FMMC_TYPE_CAR_MOD_SUPERMOD, FALSE)
					CLEAR_UNLOCK_PHONE_CALL_FOR_ACTIVITY(FMMC_TYPE_CAR_MOD_SUPERMOD)
					INT iStatInt
					iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT8)
					CLEAR_BIT(iStatInt, biNmh8_LrPostMissHelp2)
					CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet11, BI_FM_NMH11_DONE_LR_POST_MISSION_HELP2)
					SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT8, iStatInt)
					CLEAR_BIT(iStatInt, biNmh8_LrPostMissHelp1)
					CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet11, BI_FM_NMH11_DONE_LR_POST_MISSION_HELP1)
					SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT8, iStatInt)
					CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet11, BI_FM_NMH11_DONE_LR_POST_MISSION_HELP2)
					CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet11, BI_FM_NMH11_DONE_LR_POST_MISSION_HELP1)
				ENDIF
			ENDIF
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_LowFlowExtraDebug")
				CPRINTLN(DEBUG_NET_AMBIENT, "[LOWFLOW] - sc_LowFlowExtraDebug - g_bFmLowFlowDebug = TRUE")
				g_bFmLowFlowDebug = TRUE
			ENDIF
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_LowFlowFlowOnF")
				CPRINTLN(DEBUG_NET_AMBIENT, "[LOWFLOW] - sc_LowFlowFlowOnF - g_bFmLowFlowOnF = TRUE")
				g_bFmLowFlowOnF = TRUE
			ENDIF
			g_bFmEventHelpReset = TRUE
		ENDIF
	#ENDIF
	//Set the flag to do the email
	IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet2, BI_FM_GANG_BOSS_HELP_2_EMAIL_SENT)
		CPRINTLN(DEBUG_NET_MAGNATE, "[RE] [MAINTAIN_FREEMODE_NON_MISSION_HELP] [GB_MAINTAIN_NOTIFICATIONS] eGB_LOCAL_HELP_BITSET_EMAIL_SENT - GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_EMAIL_SENT)")
		GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_EMAIL_SENT)
	ENDIF
	IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet2, BI_FM_DO_EMAIL_USE_LAPTOP)
		CPRINTLN(DEBUG_NET_MAGNATE, "[RE] [MAINTAIN_FREEMODE_NON_MISSION_HELP] [GB_MAINTAIN_NOTIFICATIONS] eGB_LOCAL_HELP_BITSET_LAPTOP_EMAIL_SENT - GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_LAPTOP_EMAIL_SENT)")
		GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_LAPTOP_EMAIL_SENT)
	ENDIF
	IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet2, BI_FM_GANG_BOSS_HELP_2_REMIND_BECOME_PRESIDENT)
		CPRINTLN(DEBUG_NET_MAGNATE, "[RE] [MAINTAIN_FREEMODE_NON_MISSION_HELP] [GB_MAINTAIN_NOTIFICATIONS] eGB_LOCAL_HELP_BITSET_REMIND_BECOME_PRESIDENT - GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_REMIND_BECOME_PRESIDENT)")
		GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_REMIND_BECOME_PRESIDENT)
	ENDIF
	
	IF IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet3, BI_FM_DO_DEADLINER_HELP_ON_RETURN_TO_MP)
		GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_DO_DEADLINER_TEXT)
	ENDIF
	
	IF IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet3, BI_FM_GANG_BOSS_HELP_3__NOTIFICATION_VEHICLE_UNLOCKED_GARAGE)
		CPRINTLN(DEBUG_NET_AMBIENT, " [PROCESS_PRE_GAME] - GB_SET_LOCAL_IMP_EXP_CALL_BIT(eGB_LOCAL_IMP_EXP_CALL_BITSET_NOTIFICATION_VEHICLE_UNLOCKED_GARAGE)")
		GB_SET_LOCAL_IMP_EXP_CALL_BIT(eGB_LOCAL_IMP_EXP_CALL_BITSET_NOTIFICATION_VEHICLE_UNLOCKED_GARAGE)
		CLEAR_BIT(MPGlobalsAmbience.iFmGbHelpBitSet3, BI_FM_GANG_BOSS_HELP_3__NOTIFICATION_VEHICLE_UNLOCKED_GARAGE)
	ENDIF
	
	
	// Gunrunning Trailer
	IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet3, BI_FM_GANG_BOSS_HELP_3_TRUCK_TRAILER_AVAILABLE_EMAIL_SENT)
		CPRINTLN(DEBUG_NET_MAGNATE, "[RE] [MAINTAIN_FREEMODE_NON_MISSION_HELP] [GB_MAINTAIN_NOTIFICATIONS] BI_FM_GANG_BOSS_HELP_3_TRUCK_TRAILER_AVAILABLE_EMAIL_SENT - GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_TRUCK_TRAILER_AVAILABLE_EMAIL_SENT)")
		GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_TRUCK_TRAILER_AVAILABLE_EMAIL_SENT)
	ENDIF
	
	IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet3, BI_FM_GANG_BOSS_HELP_3_TRUCK_TRAILER_PURCHASED_EMAIL_SENT)
		CPRINTLN(DEBUG_NET_MAGNATE, "[RE] [MAINTAIN_FREEMODE_NON_MISSION_HELP] [GB_MAINTAIN_NOTIFICATIONS] BI_FM_GANG_BOSS_HELP_3_TRUCK_TRAILER_PURCHASED_EMAIL_SENT - GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_TRUCK_TRAILER_PURCHASED_EMAIL_SENT)")
		GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_TRUCK_TRAILER_PURCHASED_EMAIL_SENT)
	ENDIF

	
	g_iPreScriptReadyToLaunch = 0
	//Clean the over ride player for the kill strip
	CLEANUP_KILL_STRIP_OVER_RIDE_PLAYER_ID()
	
	g_sTransitionSessionData.sStrandMissionData.iGangOpsMissionChoiceTrackingBitSet = 0
	
	HIDE_AIRFIELD_PROPS()

	// url:bugstar:5413046 (stat is opposite as defaults to true)
	IF NOT GET_MP_BOOL_PLAYER_STAT(MPPLY_AW_JOINSPEC)
		SET_LOCAL_PLAYER_STAYS_IN_ARENA_BOX_BETWEEN_ROUNDS(FALSE)
	ELSE
		SET_LOCAL_PLAYER_STAYS_IN_ARENA_BOX_BETWEEN_ROUNDS(TRUE)
	ENDIF
	
ENDPROC

PROC SET_LOCAL_PLAYERS_UNIQUE_HASH()
	IF NOT HAS_NET_TIMER_STARTED(g_sUniqueTimerData.stUniquieIDSyncTimer)
		START_NET_TIMER(g_sUniqueTimerData.stUniquieIDSyncTimer)
		g_iLastUniquePlayersHash = GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iUniquePlayersHash
		PRINTLN("[TS][UniquePlayersHash] g_iLastUniquePlayersHash = ", g_iLastUniquePlayersHash)
		INT iPositiveOrNexitave = GET_FRAME_COUNT()
		IF GET_RANDOM_BOOL()
			iPositiveOrNexitave *= -1
		ENDIF
		INT iRandomNumber = GET_RANDOM_INT_IN_RANGE(0, 65534) + GET_RANDOM_INT_IN_RANGE(0, 65534)
		INT iUniqueHash = GET_HASH_KEY(GET_PLAYER_NAME(PLAYER_ID())) + GET_HASH_KEY(GET_CLOUD_TIME_AS_STRING()) + iRandomNumber
		iUniqueHash *= iPositiveOrNexitave
		GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iUniquePlayersHash = iUniqueHash
		PRINTLN("[TS][UniquePlayersHash] GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iUniquePlayersHash = ", iUniqueHash)
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			GlobalServerBD_FM_events.iUniqueSessionHash1 = GlobalServerBD_FM_events.iUniqueSessionHash0
			PRINTLN("[TS][UniquePlayersHash] GlobalServerBD_FM_events.iUniqueSessionHash1 = ", GlobalServerBD_FM_events.iUniqueSessionHash1)
			GlobalServerBD_FM_events.iUniqueSessionHash0 = iUniqueHash
			PRINTLN("[TS][UniquePlayersHash] GlobalServerBD_FM_events.iUniqueSessionHash0 = ", GlobalServerBD_FM_events.iUniqueSessionHash0)
		ENDIF
		g_sUniqueTimerData.iTimerTime = GET_RANDOM_INT_IN_RANGE(24000, 31000)
		PRINTLN("[TS][UniquePlayersHash] g_sUniqueTimerData.iTimerTime = ", g_sUniqueTimerData.iTimerTime)
	ELIF HAS_NET_TIMER_EXPIRED(g_sUniqueTimerData.stUniquieIDSyncTimer, g_sUniqueTimerData.iTimerTime)
		RESET_NET_TIMER(g_sUniqueTimerData.stUniquieIDSyncTimer)
	ENDIF
ENDPROC

PROC SET_PREVIOUS_SAVED_RADIO_STATION()
	
	IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iRadioStationID[0] != -1
		IF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iPreviousApartmentRadioStation = -1
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iPreviousApartmentRadioStation = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iRadioStationID[0]
			
			CDEBUG1LN(DEBUG_RADIO, "SET_PREVIOUS_SAVED_RADIO_STATION: Radio station: ", GET_RADIO_STATION_NAME(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iPreviousApartmentRadioStation), " for player ", GET_PLAYER_NAME(PLAYER_ID()))
		ENDIF
	ELSE
//		CDEBUG1LN(DEBUG_RADIO, "SET_PREVIOUS_SAVED_RADIO_STATION: No previously saved radio station for player: ", GET_PLAYER_NAME(PLAYER_ID()))
	ENDIF
	
	
	IF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iApartmentRadioStationOn = -1
		IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bRadioOn[0]
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iApartmentRadioStationOn = 1
			CDEBUG1LN(DEBUG_RADIO, "SET_PREVIOUS_SAVED_RADIO_STATION: Radio was set to on for player ", GET_PLAYER_NAME(PLAYER_ID()))
		ELSE
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iApartmentRadioStationOn = 0
			CDEBUG1LN(DEBUG_RADIO, "SET_PREVIOUS_SAVED_RADIO_STATION: Radio was set to off for player ", GET_PLAYER_NAME(PLAYER_ID()))
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_CLEAN_UP_WHEN_NOT_ON_A_MISSION()
	//url:bugstar:2479319 - Kill List Competitive - Rhino 3: Enemy peds are invincible to the local player. 
	IF FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_KILL_LIST
		EXIT
	ENDIF
	IF g_bFreemodeCleanUpAfterMission = TRUE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), rgFM_Team[NATIVE_TO_INT(PLAYER_ID())])	//rgFM_DefaultPlayer) //rgFM_Team[PARTICIPANT_ID_TO_INT()])	//Put all players on default team to start with rgFM_DefaultPlayer#
			//SET_CAN_ATTACK_FRIENDLY(PLAYER_PED_ID(), TRUE, TRUE) 
			PRINTLN("SET_CAN_ATTACK_FRIENDLY Called")
			//SET_PED_CAN_BE_TARGETTED_BY_TEAM(PLAYER_PED_ID(), -1, TRUE)

			
			PRINTLN("MAINTAIN_CLEAN_UP_WHEN_NOT_ON_A_MISSION() Called")
			g_bFreemodeCleanUpAfterMission = FALSE
		ENDIF
	ENDIF
ENDPROC


PROC START_GLOBAL_SCENE_AUDIO(BOOL& Initialised)
	IF Initialised = FALSE
		IF NOT IS_TRANSITION_ACTIVE()
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			IF NOT IS_AUDIO_SCENE_ACTIVE("MP_GLOBAL_SCENE")
				START_AUDIO_SCENE("MP_GLOBAL_SCENE")
				Initialised = TRUE
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

PROC MAINTAIN_FREEMODE_DPAD_DOWN()
	MAINTAIN_DPADDOWN_CHECK()
	MAINTAIN_STREET_AND_VEHICLE_INFO_DISPLAY()
	IF GET_DPADDOWN_ACTIVATION_STATE() != DPADDOWN_NONE 
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(MPGlobalsHud.iOnMissionOverlayTimer, DPAD_DOWN_DISPLAY_TIME)
			SET_DPADDOWN_ACTIVATION_STATE(DPADDOWN_NONE)
			DPADDOWN_ONE_TIME_ACTIVE(FALSE)
		ENDIF
	ENDIF
	
ENDPROC


FUNC BOOL IS_ANY_VEHICLE_TYRE_BURST(VEHICLE_INDEX veh)
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(veh))
			IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(veh))
				IF IS_VEHICLE_TYRE_BURST(veh, SC_WHEEL_BIKE_FRONT)
				OR IS_VEHICLE_TYRE_BURST(veh, SC_WHEEL_BIKE_REAR)
					RETURN TRUE
				ENDIF
			ELSE
				IF IS_VEHICLE_TYRE_BURST(veh, SC_WHEEL_CAR_FRONT_LEFT)
				OR IS_VEHICLE_TYRE_BURST(veh, SC_WHEEL_CAR_FRONT_RIGHT)
				OR IS_VEHICLE_TYRE_BURST(veh, SC_WHEEL_CAR_REAR_RIGHT)
				OR IS_VEHICLE_TYRE_BURST(veh, SC_WHEEL_CAR_REAR_LEFT)
					RETURN TRUE
				ELSE	
					IF IS_BIG_VEHICLE(veh)
						IF IS_VEHICLE_TYRE_BURST(veh, SC_WHEEL_CAR_MID_LEFT)
						OR IS_VEHICLE_TYRE_BURST(veh, SC_WHEEL_CAR_MID_RIGHT)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


PROC CHECK_FOR_WRONG_HEADGEAR()
	IF IS_BIT_SET(g_iFmNmhBitSet8, BI_FM_NMH8_WRONG_HEADGEAR)
		IF NOT IS_BIT_SET(g_iFmNmhBitSet8, BI_FM_NMH8_HEADGEAR_HELP)
			IF OK_TO_DO_NON_MISSION_HELP(FALSE, DEFAULT, DEFAULT, FALSE) // Ignore on mission
				SET_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_HEADGEAR_HELP)
				CLEAR_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_WRONG_HEADGEAR)
				PRINT_HELP("FM_HLP_HEDG") // Your current headgear is incompatible with this action.
				
				#IF IS_DEBUG_BUILD
					NET_PRINT_TIME() NET_PRINT("[dsw] [MAINTAIN_FREEMODE_NON_MISSION_HELP] [CHECK_FOR_WRONG_HEADGEAR]: Printing help") NET_NL()
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_MY_GB_ASSAULT_IPL()
	VECTOR vPlayerCoords
	IF GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_GB_RESTORE_MY_ZANCUDO_IPL)
		vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		IF VDIST2(vPlayerCoords, <<-2115.0222, 3295.7969, 31.8103>>)  > 90000
			IF NOT IS_COORD_IN_SPECIFIED_AREA(vPlayerCoords, AC_MILITARY_BASE, 1000 )
				GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_GB_RESTORE_MY_ZANCUDO_IPL)
			ENDIF
		ENDIF
	ENDIF
ENDPROC





PROC MAINTAIN_CHEATER_BADSPORT_HAT(BOOL& Isremoved)
	
	IF NETWORK_PLAYER_IS_CHEATER()
	OR NETWORK_PLAYER_IS_BADSPORT()
	
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		
			IF Isremoved = FALSE
				VEHICLE_INDEX aVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF DOES_VEHICLE_HAVE_ROOF(aVeh)
//					SET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD, -1 )
					CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
					Isremoved = TRUE
				
				ENDIF
			ENDIF
		ELIF NOT HAS_FREEMODE_INTRO_BEEN_DONE()
			CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
			Isremoved = TRUE
		ELSE
			IF IS_PED_WEARING_HELMET(PLAYER_PED_ID()) = FALSE
				IF Isremoved 
					//Put dunce cap back on
					SET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD, 1 )
					IF IS_CHARACTER_MALE(GET_ACTIVE_CHARACTER_SLOT())
						set_ped_comp_item_current_mp(PLAYER_PED_ID(), comp_type_berd, BERD_FMM_0_0)
					ELSE
						set_ped_comp_item_current_mp(PLAYER_PED_ID(), comp_type_berd, BERD_FMF_0_0)
					ENDIF
					Isremoved = FALSE
				ENDIF
			ENDIF
		ENDIF
	
	ENDIF
	

ENDPROC



PROC MAINTAIN_DISABLE_RECORDING_FOR_SPHERE()
	IF (MPGlobalsAmbience.bDisableRecordingForSphereIsActive)
		IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobalsAmbience.DisableRecordingForSphere_StartTime)) > MPGlobalsAmbience.iDisableRecordingForSphere_Duration)
			PRINTLN("MAINTAIN_DISABLE_RECORDING_FOR_SPHERE - time expired")
			MPGlobalsAmbience.bDisableRecordingForSphereIsActive = FALSE
		ELSE
			#IF IS_DEBUG_BUILD
				INT iTimeDiff = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobalsAmbience.DisableRecordingForSphere_StartTime))
				PRINTLN("MAINTAIN_DISABLE_RECORDING_FOR_SPHERE - is active. coords ", MPGlobalsAmbience.vDisableRecordingForSphere_Coords, ", radius = ", MPGlobalsAmbience.fDisableRecordingForSphere_Radius, " max dist = ", MPGlobalsAmbience.fDisableRecordingForSphere_MaxDist, "  time remaining ", iTimeDiff)
			#ENDIF
			VECTOR vCurrentCamCoords = GET_FINAL_RENDERED_CAM_COORD()
			FLOAT fDist = VDIST(vCurrentCamCoords, MPGlobalsAmbience.vDisableRecordingForSphere_Coords)
			IF (fDist < MPGlobalsAmbience.fDisableRecordingForSphere_MaxDist)
				IF IS_SPHERE_VISIBLE(MPGlobalsAmbience.vDisableRecordingForSphere_Coords, MPGlobalsAmbience.fDisableRecordingForSphere_Radius)
					REPLAY_PREVENT_RECORDING_THIS_FRAME()  		
					PRINTLN("MAINTAIN_DISABLE_RECORDING_FOR_SPHERE - calling REPLAY_PREVENT_RECORDING_THIS_FRAME")
				ENDIF
			ELSE
				PRINTLN("MAINTAIN_DISABLE_RECORDING_FOR_SPHERE - out of range. vCurrentCamCoords  = ", vCurrentCamCoords, ", fDist = ", fDist)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_GRIEF_PASSIVE_SPAWN_HELP()
	INT iStatInt

	IF g_bShowGriefPassiveSpawnHelp
	AND IS_NET_PLAYER_OK(g_piFirstGriefPassivePlayer, FALSE)
	AND IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
	AND OK_TO_DO_NON_MISSION_HELP(FALSE)
		IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet11, BI_FM_NMH11_DONE_GRIEF_PASSIVE_SPAWN_HELP)
			iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT8)
			
			IF NOT IS_BIT_SET(iStatInt, biNmh8_DoneGriefPassiveSpawnHelp1)
			OR NOT IS_BIT_SET(iStatInt, biNmh8_DoneGriefPassiveSpawnHelp2)
			OR NOT IS_BIT_SET(iStatInt, biNmh8_DoneGriefPassiveSpawnHelp3)
				IF GB_IS_PLAYER_MEMBER_OF_A_GANG(g_piFirstGriefPassivePlayer)
					PRINT_HELP("GRIEF_SPWN_HELP3")
					CPRINTLN(DEBUG_NET_MAGNATE, "[RE] [MAINTAIN_GRIEF_PASSIVE_SPAWN_HELP] PRINT_HELP(GRIEF_SPWN_HELP3)")
				ELSE
					PRINT_HELP_WITH_PLAYER_NAME("GRIEF_SPWN_HELP", GET_PLAYER_NAME(g_piFirstGriefPassivePlayer), HUD_COLOUR_PURE_WHITE)
					CPRINTLN(DEBUG_NET_MAGNATE, "[RE] [MAINTAIN_GRIEF_PASSIVE_SPAWN_HELP] PRINT_HELP(GRIEF_SPWN_HELP)")
				ENDIF
				
				IF IS_BIT_SET(iStatInt, biNmh8_DoneGriefPassiveSpawnHelp2)
					SET_BIT(iStatInt, biNmh8_DoneGriefPassiveSpawnHelp3)
					CPRINTLN(DEBUG_NET_MAGNATE, "[RE] [MAINTAIN_GRIEF_PASSIVE_SPAWN_HELP] biNmh8_DoneGriefPassiveSpawnHelp3 SET.")
				ELIF IS_BIT_SET(iStatInt, biNmh8_DoneGriefPassiveSpawnHelp1)
					SET_BIT(iStatInt, biNmh8_DoneGriefPassiveSpawnHelp2)
					CPRINTLN(DEBUG_NET_MAGNATE, "[RE] [MAINTAIN_GRIEF_PASSIVE_SPAWN_HELP] biNmh8_DoneGriefPassiveSpawnHelp2 SET.")
				ELSE
					SET_BIT(iStatInt, biNmh8_DoneGriefPassiveSpawnHelp1)
					CPRINTLN(DEBUG_NET_MAGNATE, "[RE] [MAINTAIN_GRIEF_PASSIVE_SPAWN_HELP] biNmh8_DoneGriefPassiveSpawnHelp1 SET.")
				ENDIF
		
				SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT8, iStatInt) 
				SET_BIT(MPGlobalsAmbience.iFmNmhBitSet11, BI_FM_NMH11_DONE_GRIEF_PASSIVE_SPAWN_HELP)
			ELSE
				SET_BIT(MPGlobalsAmbience.iFmNmhBitSet11, BI_FM_NMH11_DONE_GRIEF_PASSIVE_SPAWN_HELP)
				CPRINTLN(DEBUG_NET_MAGNATE, "[RE] [MAINTAIN_GRIEF_PASSIVE_SPAWN_HELP] IS_BIT_SET(iStatInt, biNmh8_DoneGriefPassiveSpawnHelp) = TRUE. Already done BI_FM_NMH11_DONE_GRIEF_PASSIVE_SPAWN_HELP help message.")
			ENDIF
		ENDIF
		g_bShowGriefPassiveSpawnHelp = FALSE
		g_piFirstGriefPassivePlayer = INVALID_PLAYER_INDEX()
		CPRINTLN(DEBUG_NET_MAGNATE, "[RE] [MAINTAIN_GRIEF_PASSIVE_SPAWN_HELP] g_bShowGriefPassiveSpawnHelp = FALSE")
	ENDIF
ENDPROC

CONST_INT SLAMVAN_HELP			0
CONST_INT VIRGO3_HELP			1
CONST_INT FACTION_HELP			2
CONST_INT SABRE_HELP			3
CONST_INT TORNADO_HELP			4
CONST_INT MINIVAN_HELP			5
CONST_INT GROTTI_PROTO_HELP		6
CONST_INT DEWBAUCHEE_HELP		7
CONST_INT PFISTER_HELP			8

FUNC INT COMPARE_INT(INT iFirst, INT iSecond)
	IF iSecond >= iFirst
		RETURN iSecond
	ENDIF	
	RETURN iFirst
ENDFUNC

FUNC BOOL IS_SUPERMOD_VEHICLE_ENABLED_IN_TUNABLE(INT iSupermod)
	SWITCH iSupermod
		CASE GROTTI_PROTO_HELP		RETURN g_sMPTunables.bEnableExec1_GROTTI_PROTO	
		CASE DEWBAUCHEE_HELP		RETURN g_sMPTunables.bEnableExec1_DEWBAUCHEE	
		CASE PFISTER_HELP			RETURN g_sMPTunables.bEnableExec1_PFISTER		
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC INT MATCH_POSIX_TO_SUPERMOD_VEHICLE(INT iPosix)

	IF IS_SUPERMOD_VEHICLE_ENABLED_IN_TUNABLE(GROTTI_PROTO_HELP)
	AND iPosix = g_sMPTunables.iEnableExec1_GROTTI_PROTO_posix
		RETURN GROTTI_PROTO_HELP
	ENDIF
	IF IS_SUPERMOD_VEHICLE_ENABLED_IN_TUNABLE(DEWBAUCHEE_HELP)
	AND iPosix = g_sMPTunables.iEnableExec1_DEWBAUCHEE_posix
		RETURN DEWBAUCHEE_HELP
	ENDIF
	IF IS_SUPERMOD_VEHICLE_ENABLED_IN_TUNABLE(PFISTER_HELP)
	AND iPosix = g_sMPTunables.iEnableExec1_PFISTER_posix
		RETURN PFISTER_HELP
	ENDIF

	RETURN -1
ENDFUNC

FUNC INT GET_LATEST_SUPERMOD_VEHICLE()
	INT iMostRecentPosix = -1
	
	IF IS_SUPERMOD_VEHICLE_ENABLED_IN_TUNABLE(GROTTI_PROTO_HELP)
		iMostRecentPosix = COMPARE_INT(iMostRecentPosix, g_sMPTunables.iEnableExec1_GROTTI_PROTO_posix)
	ENDIF
	IF IS_SUPERMOD_VEHICLE_ENABLED_IN_TUNABLE(DEWBAUCHEE_HELP)
		iMostRecentPosix = COMPARE_INT(iMostRecentPosix, g_sMPTunables.iEnableExec1_DEWBAUCHEE_posix)
	ENDIF
	IF IS_SUPERMOD_VEHICLE_ENABLED_IN_TUNABLE(PFISTER_HELP)
		iMostRecentPosix = COMPARE_INT(iMostRecentPosix, g_sMPTunables.iEnableExec1_PFISTER_posix)
	ENDIF
	
	RETURN MATCH_POSIX_TO_SUPERMOD_VEHICLE(iMostRecentPosix)
ENDFUNC

FUNC STRING GET_SUPERMOD_VEHICLE_TXTLABEL(INT iSupermod)
	SWITCH iSupermod
		CASE SLAMVAN_HELP			RETURN "BENNY_NV_SLMVN"
		CASE VIRGO3_HELP			RETURN "BENNY_NV_VIRGO"
		CASE FACTION_HELP			RETURN "BENNY_NV_FACTI"
		CASE SABRE_HELP				RETURN "BENNY_NV_SABRE"
		CASE TORNADO_HELP			RETURN "BENNY_NV_TRNDO"
		CASE MINIVAN_HELP			RETURN "BENNY_NV_MNIVN"
		CASE GROTTI_PROTO_HELP		RETURN "EXEC1NV_GXP"
		CASE DEWBAUCHEE_HELP		RETURN "EXEC1NV_DS7"
		CASE PFISTER_HELP			RETURN "EXEC1NV_p811"
	ENDSWITCH
	RETURN "EXEC1NV_GXP"
ENDFUNC


PROC MAINTAIN_FREEMODE_NON_MISSION_HELP()
	IF g_SkipFmTutorials
		EXIT
	ENDIF

	MAINTAIN_CRIMINAL_ENTERPRISE_STARTER_PACK_NOTIFICATIONS(sCriminalEnterpriseStarterPackNotificationVars, unlockFmPhoneCall.bDoPhoneCall)
	
	BOOL bOkForFreemodeHelp = OK_TO_DO_NON_MISSION_HELP(FALSE)
	
	SWITCH iMaintainHelpTextCounter

		CASE 53  MAINTAIN_GRIEF_PASSIVE_SPAWN_HELP()       BREAK

		CASE 55 
			GB_MAINTAIN_IMPORTANT_NOTIFICATIONS()
			IF bOkForFreemodeHelp
				GB_MAINTAIN_NOTIFICATIONS(sGbNotificationVars)
			ENDIF
		BREAK

		
		CASE 58
			IF bOkForFreemodeHelp
			AND NOT NETWORK_IS_IN_MP_CUTSCENE()
				MAINTAIN_SMUGGLER_NOTIFICATIONS(sGbNotificationVars)
			ENDIF
		BREAK

		
		CASE 60
			IF bOkForFreemodeHelp
			AND NOT NETWORK_IS_IN_MP_CUTSCENE()
				UPDATE_NEW_INTERACTION_HELP()
			ENDIF
		BREAK
		
		
		CASE 65
		BREAK
		
		CASE 66
				MAINTAIN_SPECIAL_EVENT_SHIRT_AWARD()
		BREAK
		
		CASE 67
		BREAK
		
				
		CASE 68

		BREAK
		
		CASE 69

		BREAK
		
		CASE 70

		BREAK
		
		CASE 71
	
		BREAK
		//-- Remember to increase iFNMH_COUNTER_MAX if adding a new case
	ENDSWITCH	
	
	
	//-- Only non-intensive checks should go outside of the switch. Preferably nothing that reads a stat every frame for extended periods, for example
	CHECK_FOR_WRONG_HEADGEAR()
	
	iMaintainHelpTextCounter++
	IF iMaintainHelpTextCounter >= iFNMH_COUNTER_MAX
		iMaintainHelpTextCounter = 0
	ENDIF
	

	
	MAINTAIN_MY_GB_ASSAULT_IPL()
ENDPROC



PROC MAINTAIN_FM_ACTIVITY_UNLOCK_PHONECALLS()
	IF g_SkipFmTutorials
		EXIT
	ENDIF 
	
	IF iActivityPhoneCallCounter = 0
		IF NOT HAS_NET_TIMER_STARTED(reminderFmPhonecall.timeReminderPhonecall)
			START_NET_TIMER(reminderFmPhonecall.timeReminderPhonecall)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(reminderFmPhonecall.timeReminderPhonecall, 10000)
				SET_BIT(reminderFmPhonecall.iReminderBitset, biRem_ReminderTimerExpired)
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("MAINTAIN_FM_ACTIVITY_UNLOCK_PHONECALLS")
	#ENDIF
	#ENDIF	

	DO_FM_ACTIVITY_UNLOCK_PHONECALL(unlockFmPhoneCall, iActivityPhoneCallCounter)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("DO_FM_ACTIVITY_UNLOCK_PHONECALL")
	#ENDIF	
	#ENDIF
	
	DO_FM_ACTIVITY_REMINDER_PHONECALL(unlockFmPhoneCall, reminderFmPhonecall, iActivityPhoneCallCounter, reminderTimes)	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("DO_FM_ACTIVITY_REMINDER_PHONECALL")
	#ENDIF		
	#ENDIF	
	
	DO_FM_MISSION_PASSED_TEXT_MESSAGES(unlockFmPhoneCall, iActivityPhoneCallCounter, IS_BIT_SET(reminderFmPhonecall.iReminderBitset, biRem_ReminderTimerExpired))
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("DO_FM_MISSION_PASSED_TEXT_MESSAGES")
	#ENDIF		
	#ENDIF
	
	
	IF iActivityPhoneCallCounter > FMMC_TYPE_MAX
		iActivityPhoneCallCounter = 0
		IF IS_BIT_SET(reminderFmPhonecall.iReminderBitset, biRem_ReminderTimerExpired)
			CLEAR_BIT(reminderFmPhonecall.iReminderBitset, biRem_ReminderTimerExpired)
			RESET_NET_TIMER(reminderFmPhonecall.timeReminderPhonecall)
		ENDIF
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF	
	
ENDPROC



PROC SETUP_FM_USJS()

	IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_STUNT_JUMPS)
		ENABLE_STUNT_JUMP_SET(ciSINGLE_PLAYER_SJS_GROUP)
	
		PRINTLN("MPUSJ - ENABLE_STUNT_JUMP_SET(ciSINGLE_PLAYER_SJS_GROUP)")
	ELSE
		DISABLE_STUNT_JUMP_SET(ciSINGLE_PLAYER_SJS_GROUP)
		PRINTLN("MPUSJ - Stunt jumps not unlocked yet(ciFREEMODE_PLAYER_SJS_GROUP)")
	ENDIF
ENDPROC

SCRIPT_TIMER timeBroadcastApp
PROC UPDATE_BROADCAST_APP()
	IF (g_bBroadcastAppOn)
		
		IF NOT HAS_NET_TIMER_STARTED(timeBroadcastApp)
		OR HAS_NET_TIMER_EXPIRED(timeBroadcastApp, 1000)
		
			// if player is spectating then suspend broadcast app
			IF IS_A_SPECTATOR_CAM_ACTIVE()
				IF NOT (g_bBroadcastAppSuspended)
					NETWORK_OVERRIDE_SEND_RESTRICTIONS_ALL(FALSE)	
					g_bBroadcastAppSuspended = TRUE
				ENDIF
			ELSE
				IF (g_bBroadcastAppSuspended)
					NETWORK_OVERRIDE_SEND_RESTRICTIONS_ALL(TRUE)	
					g_bBroadcastAppSuspended = FALSE
				ENDIF
			ENDIF
	
		ENDIF
	
	ENDIF
ENDPROC

 
 FUNC INT GET_TIMEOFDAY_AS_MINUTES(TIMEOFDAY sTimeOfDay)



	INT iMinute = GET_TIMEOFDAY_MINUTE(sTimeOfDay)
	INT iHour	= GET_TIMEOFDAY_HOUR(sTimeOfDay)
	INT iDay	= GET_TIMEOFDAY_DAY(sTimeOfDay)
	INT iMonth  = ENUM_TO_INT(GET_TIMEOFDAY_MONTH(sTimeOfDay))
	INT iYear	= GET_TIMEOFDAY_YEAR(sTimeOfDay)
	INT i

	//NET_PRINT("GET_TIMEOFDAY_AS_MINUTES - called:") NET_PRINT_INT(iYear) NET_PRINT(",") NET_PRINT_INT(iMonth) NET_PRINT(",") NET_PRINT_INT(iDay) NET_PRINT(",") NET_PRINT_INT(iHour) NET_PRINT(",") NET_PRINT_INT(iMinute) NET_NL()
	
	
	// convert remaining months to days
	WHILE (iMonth > 0)
		iDay += GET_NUMBER_OF_DAYS_IN_MONTH(INT_TO_ENUM(MONTH_OF_YEAR, i), iYear)
		iMonth--
	ENDWHILE
	
	// covert days to hours
	WHILE (iDay > 0)
		iHour += 24
		iDay--
	ENDWHILE
	
	// convert hours to minutes
	WHILE (iHour > 0)
		iMinute += 60
		iHour--
	ENDWHILE
	
	//NET_PRINT("GET_TIMEOFDAY_AS_MINUTES - returning:") NET_PRINT_INT(iMinute) NET_NL()
		
	RETURN iMinute
	
ENDFUNC

FUNC INT GET_MINUTES_BETWEEN_TIMEOFDAYS(TIMEOFDAY sTimeOfDay1, TIMEOFDAY sTimeOfDay2)
	INT iMinute = GET_TIMEOFDAY_AS_MINUTES(sTimeOfDay1) - GET_TIMEOFDAY_AS_MINUTES(sTimeOfDay2)
	WHILE (iMinute < 0)
		iMinute += 525600	
	ENDWHILE
	//NET_PRINT("GET_MINUTES_BETWEEN_TIMEOFDAYS - returning:") NET_PRINT_INT(iMinute) NET_NL()
	RETURN(iMinute)
ENDFUNC

PROC DECREASE_MENTAL_STAT_FOR_PLAYING_TIME()
	IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		IF NETWORK_IS_SIGNED_ONLINE()
			// store last time player is playing the game - we need to calculate how long since the last time we stored to reduce the mental stat.
			TIMEOFDAY LastStoredTimeOfDay = CONVERT_STATDATE_TO_TIMEOFDAY(GET_MP_DATE_CHARACTER_STAT(MP_STAT_CHAR_LAST_PLAY_TIME))			
			TIMEOFDAY CurrentTimeOfDay = GET_CURRENT_TIMEOFDAY_POSIX()
								
				INT iElapsedMinutes = GET_MINUTES_BETWEEN_TIMEOFDAYS(CurrentTimeOfDay, LastStoredTimeOfDay)	
				FLOAT fElapsedGameDays = (TO_FLOAT(iElapsedMinutes) / 48.0) //+ fElapsedGameDayRemainder
			

				NET_PRINT("STORE_PLAYER_RE_ENTRY_STATS - elapsed game days since last call = ") NET_PRINT_FLOAT(fElapsedGameDays) NET_NL()
				INCREASE_PLAYER_MENTAL_STATE((fElapsedGameDays * g_sMPTunables.fMental_state_Decrease_Rate) * ( g_sMPTunables.fMental_state_Decrease_Amount * -1.0))
				NET_PRINT("MENTAL STATE: decreased by ") NET_PRINT_FLOAT((fElapsedGameDays * g_sMPTunables.fMental_state_Decrease_Rate) * g_sMPTunables.fMental_state_Decrease_Amount * -1.0) NET_PRINT("for days passed. ") NET_NL()

			
			SET_MP_DATE_CHARACTER_STAT(MP_STAT_CHAR_LAST_PLAY_TIME, GET_CURRENT_TIMEOFDAY_AS_STAT())		
		ENDIF
	ENDIF
ENDPROC
 
FUNC BOOL IS_OK_TO_STORE_RE_ENTRY_STATS(BOOL bIsFreemodeCleanup = FALSE)
	IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		IF NETWORK_IS_SIGNED_ONLINE()
			IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			OR (bIsFreemodeCleanup)
				IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				OR (bIsFreemodeCleanup)
					IF (g_b_On_Pilotschool)
					OR (NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())) //IS_PED_IN_FLYING_VEHICLE - Check entity is alive this frame
						IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
							IF NOT IS_PLAYER_IN_CORONA()
								IF (IS_RADAR_MAP_DISABLED() = FALSE) // dont store coords when on lesson list screen in pilot school. see 2002568
									RETURN(TRUE)				
								ELSE
									PRINTLN("IS_OK_TO_STORE_RE_ENTRY_STATS - g_b_DisableRadarMap = TRUE")
								ENDIF
							ELSE
								PRINTLN("IS_OK_TO_STORE_RE_ENTRY_STATS - IS_PLAYER_IN_CORONA = TRUE")
							ENDIF
						ELSE
							PRINTLN("IS_OK_TO_STORE_RE_ENTRY_STATS - IS_PLAYER_SPECTATING = TRUE")
						ENDIF
					ELSE
						PRINTLN("IS_OK_TO_STORE_RE_ENTRY_STATS - IS_PED_IN_FLYING_VEHICLE = TRUE")
					ENDIF
				ELSE
					PRINTLN("IS_OK_TO_STORE_RE_ENTRY_STATS - IS_PLAYER_SWITCH_IN_PROGRESS = TRUE")
				ENDIF
			ELSE
				PRINTLN("IS_OK_TO_STORE_RE_ENTRY_STATS - IS_PLAYER_CONTROL_ON = FALSE")	
			ENDIF
		ELSE
			PRINTLN("IS_OK_TO_STORE_RE_ENTRY_STATS - NETWORK_IS_SIGNED_ONLINE = FALSE")
		ENDIF
	ELSE
		PRINTLN("IS_OK_TO_STORE_RE_ENTRY_STATS - IS_NET_PLAYER_OK = FALSE")
	ENDIF

	RETURN(FALSE)
ENDFUNC
 
PROC STORE_PLAYER_RE_ENTRY_STATS()

	VECTOR vPosition
	FLOAT fHeading
				
	vPosition = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	
	// inside ANY displaced interior
	IF (g_TransitionSessionNonResetVars.bInsideDisplacedInterior)
	AND VMAG(g_TransitionSessionNonResetVars.vDisplacedPerceivedCoords) > 0.01
		vPosition = g_TransitionSessionNonResetVars.vDisplacedPerceivedCoords
		PRINTLN("STORE_PLAYER_RE_ENTRY_STATS - using displaced interior coords - ", g_TransitionSessionNonResetVars.vDisplacedPerceivedCoords)
	ENDIF		
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())	
		fHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
	ENDIF
	
	SET_PACKED_STAT_BOOL(PACKED_MP_WAS_IN_GARAGE, FALSE)

	
	// check if in own property
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE)
		
		// if player's spawn location is set to Last Location, then store this appartment as their last accessed. 
		IF (GET_MP_SPAWN_POINT_SETTING() = MP_SETTING_SPAWN_LAST_POSITION)
			SET_LAST_USED_PROPERTY(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		ENDIF
		
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_SPAWN_FLAG, TRUE )
		NET_PRINT("STORE_PLAYER_RE_ENTRY_STATS - in own property ") NET_NL()
	
		// is he in his garage?
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_A_GARAGE)
			NET_PRINT("STORE_PLAYER_RE_ENTRY_STATS - was in garage ") NET_NL()
			SET_PACKED_STAT_BOOL(PACKED_MP_WAS_IN_GARAGE, TRUE)		
		ENDIF
		
	
	// if in someone elses property
	ELIF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
		
		GET_MP_PROPERTY_DETAILS(tempPropertyStruct,GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		vPosition = tempPropertyStruct.vBlipLocation[0]
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_SPAWN_FLAG, FALSE )	
		NET_PRINT("STORE_PLAYER_RE_ENTRY_STATS - Player is in a property, setting vPosition to blip position at ") NET_PRINT_VECTOR(vPosition) NET_NL()
	
	ELSE
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_SPAWN_FLAG, FALSE )
	ENDIF
	
	
	
	
	// are we on pilot school?
	IF (g_b_On_Pilotschool)
		vPosition = << -1131.7418, -2688.5071, 12.9523 >>
		fHeading = fHeading
		NET_PRINT("STORE_PLAYER_RE_ENTRY_STATS - using flight school coords ") NET_NL()
	ENDIF

		
	//vPosition.z *= 10.0 // we need the exact z and the stat only save 1 decimal place
	NET_PRINT("STORE_PLAYER_RE_ENTRY_STATS - storing position ") NET_PRINT_VECTOR(vPosition) NET_NL()
	NET_PRINT("STORE_PLAYER_RE_ENTRY_STATS - storing heading ") NET_PRINT_FLOAT(fHeading) NET_NL()

	IF NOT IS_POINT_NEAR_SPECTATOR_HIDE_POSITION(vPosition)
		IF (vPosition.z >= -10.0)
			SET_MP_VECTOR_CHARACTER_STAT(MP_STAT_FM_SPAWN_POSITION, vPosition )
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_FM_SPAWN_HEADING, fHeading )
		ELSE
			NET_PRINT("STORE_PLAYER_RE_ENTRY_STATS - z below -10.0 not storing. ") NET_PRINT_VECTOR(vPosition) NET_NL()
		ENDIF
	ELSE
		NET_PRINT("STORE_PLAYER_RE_ENTRY_STATS - at spectator hide position, not storing. ") NET_PRINT_VECTOR(vPosition) NET_NL()
	ENDIF
					
	// is player in his personal vehicle?
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND IS_VEHICLE_MY_PERSONAL_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) 
		SET_PACKED_STAT_BOOL(PACKED_MP_WAS_IN_PERSONAL_VEHICLE, TRUE)
	ELSE
		SET_PACKED_STAT_BOOL(PACKED_MP_WAS_IN_PERSONAL_VEHICLE, FALSE)
	ENDIF	
	

			
ENDPROC

PROC MAINTAIN_STORE_PLAYER_RE_ENTRY_STATS(BOOL bForceNow=FALSE)
	IF NOT HAS_NET_TIMER_STARTED(timeStorePlayerFMSpawnPosition)
		IF IS_OK_TO_STORE_RE_ENTRY_STATS()
			STORE_PLAYER_RE_ENTRY_STATS()
			START_NET_TIMER(timeStorePlayerFMSpawnPosition)
		ENDIF
	ELSE
		IF HAS_NET_TIMER_EXPIRED(timeStorePlayerFMSpawnPosition, 180000) // 3 mins
		OR (bForceNow)
			
			#IF IS_DEBUG_BUILD
			IF (bForceNow)
				PRINTLN("MAINTAIN_STORE_PLAYER_RE_ENTRY_STATS - forced")
			ENDIF
			#ENDIF
		
			IF IS_OK_TO_STORE_RE_ENTRY_STATS()
				STORE_PLAYER_RE_ENTRY_STATS()
				REINIT_NET_TIMER(timeStorePlayerFMSpawnPosition)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_DECREASE_MENTAL_STAT_FOR_PLAYING_TIME()
	IF NOT HAS_NET_TIMER_STARTED(timeStoreDecreaseMentalStatePlaying)
		DECREASE_MENTAL_STAT_FOR_PLAYING_TIME()
		START_NET_TIMER(timeStoreDecreaseMentalStatePlaying)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(timeStoreDecreaseMentalStatePlaying, 180000) // 3 mins
			DECREASE_MENTAL_STAT_FOR_PLAYING_TIME()
			REINIT_NET_TIMER(timeStoreDecreaseMentalStatePlaying)
		ENDIF
	ENDIF
ENDPROC

PROC RETAIN_LAST_USED_VEHICLE_WHEN_ENTERING_PROPERTY()
	VEHICLE_INDEX lastVeh
	IF IS_BIT_SET(iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_TRUE)
		IF NOT IS_BIT_SET(iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_RETAINED)
			lastVeh = GET_PLAYERS_LAST_VEHICLE()
			IF DOES_ENTITY_EXIST(lastVeh)
				IF IS_VEHICLE_DRIVEABLE(lastVeh)
					IF IS_VEHICLE_EMPTY(lastVeh)
						IF IS_ENTITY_A_MISSION_ENTITY(lastVeh)
							PRINTLN("..CDM-MP: RETAIN_LAST_USED_VEHICLE_WHEN_ENTERING_PROPERTY: Retained last player (already a mission entity)")
							SET_BIT(iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_RETAINED)
							SAVE_MY_EOM_VEHICLE_DATA(lastVeh,TRUE, TRUE, 0)
						ELSE
							IF NOT IS_BIT_SET(iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_RESERVED)
								IF CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT( GET_NUM_RESERVED_MISSION_VEHICLES() + 1,FALSE,FALSE )
									RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES() + 1)
									PRINTLN("..CDM-MP: RETAIN_LAST_USED_VEHICLE_WHEN_ENTERING_PROPERTY: Reserving space for last vehicle")
									SET_BIT(iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_RESERVED)
								ENDIF
							ELSE
								IF NETWORK_HAS_CONTROL_OF_ENTITY(lastVeh)
									IF CAN_REGISTER_MISSION_VEHICLES(1)
										SET_ENTITY_AS_MISSION_ENTITY(lastVeh,FALSE,FALSE)
										SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(NETWORK_GET_NETWORK_ID_FROM_ENTITY(lastVeh),PLAYER_ID(),TRUE)
										g_vehIDMP_Prop_Retained = lastVeh
										PRINTLN("..CDM-MP: RETAIN_LAST_USED_VEHICLE_WHEN_ENTERING_PROPERTY: Retained last player vehicle(set as script entity)")
										SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(g_vehIDMP_Prop_Retained, TRUE)
										SAVE_MY_EOM_VEHICLE_DATA(lastVeh,TRUE, TRUE, 1)
										SET_BIT(iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_RETAINED)
									ENDIF
								ELSE
									NETWORK_REQUEST_CONTROL_OF_ENTITY(lastVeh)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						PRINTLN("..CDM-MP: RETAIN_LAST_USED_VEHICLE_WHEN_ENTERING_PROPERTY: Retained last player (NOT empty)")
						SET_BIT(iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_RETAINED)
					ENDIF
				ELSE
					PRINTLN("..CDM-MP: RETAIN_LAST_USED_VEHICLE_WHEN_ENTERING_PROPERTY: Retained last player (NOT driveable)")
					SET_BIT(iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_RETAINED)
				ENDIF
			ELSE
				PRINTLN("..CDM-MP: RETAIN_LAST_USED_VEHICLE_WHEN_ENTERING_PROPERTY: Retained last player (Doesn't exist)")
				SET_BIT(iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_RETAINED)
			ENDIF
		ENDIF
	ELSE
		BOOL bClear = TRUE
		IF IS_BIT_SET(iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_RETAINED)
			IF IS_BIT_SET(iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_RESERVED)
				IF DOES_ENTITY_EXIST(g_vehIDMP_Prop_Retained) 
					IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(g_vehIDMP_Prop_Retained)
						NETWORK_REQUEST_CONTROL_OF_ENTITY(g_vehIDMP_Prop_Retained)
						bClear = FALSE
						PRINTLN("..CDM-MP: RETAIN_LAST_USED_VEHICLE_WHEN_ENTERING_PROPERTY: VEH requesting control")
					ENDIF
				ENDIF
			ENDIF
			IF bClear
				SET_MY_SAVED_EOM_VEHICLE_AS_NO_LONGER_NEEDED() 
				RESET_CREATE_AND_ENTER_EOM_AMBIENT_VEHICLE_DATA()
				PRINTLN("..CDM-MP: RETAIN_LAST_USED_VEHICLE_WHEN_ENTERING_PROPERTY: VEH was retained clearing")
				IF IS_BIT_SET(iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_RESERVED)
					PRINTLN("..CDM-MP: RETAIN_LAST_USED_VEHICLE_WHEN_ENTERING_PROPERTY: VEH was reserved clearing")
					IF DOES_ENTITY_EXIST(g_vehIDMP_Prop_Retained)
						PRINTLN("..CDM-MP: RETAIN_LAST_USED_VEHICLE_WHEN_ENTERING_PROPERTY: cleaning up retained vehicle (no longer needed)")
						SET_VEHICLE_AS_NO_LONGER_NEEDED(g_vehIDMP_Prop_Retained)
					ELSE
						PRINTLN("..CDM-MP: RETAIN_LAST_USED_VEHICLE_WHEN_ENTERING_PROPERTY: retained vehicle does not exist?")
					ENDIF
					RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES() - 1)
					CLEAR_BIT(iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_RESERVED)
				ENDIF
				CLEAR_BIT(iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_RETAINED)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_HEADSHOT_DELETION(INT& HeadshotDeletion)
	
	INT I
	SWITCH HeadshotDeletion
	
		CASE 0 
			
			
						
			FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
				IF g_b_Private_DeleteHeadshot[I] 
					HeadshotDeletionStages = I+1
				ENDIF
			ENDFOR
			
		
		BREAK
		
		CASE 1
			IF Delete_Character_Headshot(0)
				NET_NL()NET_PRINT("MAINTAIN_HEADSHOT_DELETION Delete_Character_Headshot(0) = TRUE ")
				g_b_Private_DeleteHeadshot[0]  = FALSE
				HeadshotDeletionStages = 0
			ENDIF

		
		BREAK
		
		CASE 2
			IF Delete_Character_Headshot(1)
				NET_NL()NET_PRINT("MAINTAIN_HEADSHOT_DELETION Delete_Character_Headshot(1) = TRUE ")
				g_b_Private_DeleteHeadshot[1]  = FALSE
				HeadshotDeletionStages = 0
			ENDIF
		BREAK
		
		CASE 3
				HeadshotDeletionStages = 0
		BREAK
		
		CASE 4
				HeadshotDeletionStages = 0
		BREAK
		
		
		CASE 5
				HeadshotDeletionStages = 0
		BREAK
	ENDSWITCH

ENDPROC

PROC REQUEST_AUDIO_FREEMODE()

	IF NOT MPGlobalsHud.b_HasLoadedWastedAudioBank
		IF REQUEST_SCRIPT_AUDIO_BANK("MP_WASTED")
			PRINTLN("Loaded script audio bank MP_WASTED.")
			MPGlobalsHud.b_HasLoadedWastedAudioBank = TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iRequestAudioFreemodeBitset, AUDIO_FREEMODE_LOADED_321_GO)
		IF REQUEST_SCRIPT_AUDIO_BANK("HUD_321_GO")
			PRINTLN("Loaded script audio bank HUD_321_GO.")
			SET_BIT(iRequestAudioFreemodeBitset, AUDIO_FREEMODE_LOADED_321_GO)
		ENDIF
	ENDIF
	
ENDPROC

PROC CLEANUP_FREEMODE_SPECTATOR_CHAT()
	IF IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_FREEMODE_SPECTATOR_CHAT_SETUP)
		NETWORK_SET_OVERRIDE_SPECTATOR_MODE(FALSE)
		CLEAR_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_FREEMODE_SPECTATOR_CHAT_SETUP)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === CLEANUP_FREEMODE_SPECTATOR_CHAT CLEAR_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_FREEMODE_SPECTATOR_CHAT_SETUP)")
		#ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_FREEMODE_SPECTATOR_CHAT()
	IF IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_FREEMODE_SPECTATOR_CHAT_SETUP)
		IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
			CLEANUP_FREEMODE_SPECTATOR_CHAT()
		ENDIF
	ELSE
		IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
			NETWORK_SET_OVERRIDE_SPECTATOR_MODE(TRUE)
			SET_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_FREEMODE_SPECTATOR_CHAT_SETUP)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === MAINTAIN_FREEMODE_SPECTATOR_CHAT SET_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_FREEMODE_SPECTATOR_CHAT_SETUP)")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_FREEMODE_SPECTATOR_SET_CAM_MODES()
	//SET CINEMATIC CAM MODE
	IF IS_BIT_SET(MPSpecGlobals.iStoredCamBitSet, SCTV_SC_BIT_SET_CINEMATIC_CAM_MODE)
		//IF HAS_NET_TIMER_EXPIRED(MPSpecGlobals.SetCamModeDelay, 200)
		IF NETWORK_IS_IN_SPECTATOR_MODE()
			SET_SPEC_CAM_MODES(TRUE)
			//RESET_NET_TIMER(MPSpecGlobals.SetCamModeDelay)
			CLEAR_BIT(MPSpecGlobals.iStoredCamBitSet, SCTV_SC_BIT_SET_CINEMATIC_CAM_MODE)
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MAINTAIN_FREEMODE_SPECTATOR_SET_CAM_MODES - SCTV_SC_BIT_SET_CINEMATIC_CAM_MODE - CLEARED")
		ENDIF
	//SET STORED CAM MODES
	ELIF IS_BIT_SET(MPSpecGlobals.iStoredCamBitSet, SCTV_SC_BIT_SET_STORED_CAM_MODE)
		//IF HAS_NET_TIMER_EXPIRED(MPSpecGlobals.SetCamModeDelay, 200)
		IF NOT NETWORK_IS_IN_SPECTATOR_MODE()
			SET_SPEC_CAM_MODES(FALSE)
			//RESET_NET_TIMER(MPSpecGlobals.SetCamModeDelay)
			CLEAR_BIT(MPSpecGlobals.iStoredCamBitSet, SCTV_SC_BIT_SET_STORED_CAM_MODE)
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MAINTAIN_FREEMODE_SPECTATOR_SET_CAM_MODES - SCTV_SC_BIT_SET_STORED_CAM_MODE - CLEARED")
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC CLEAR_ANY_OF_MY_CONTACT_REQUESTS()

	// Clean any contact requests, then clear. 
	INT iContactRequest
	REPEAT ENUM_TO_INT(CONTACT_REQUEST_MAX) iContactRequest
		IF IS_BIT_SET(GlobalServerBD_FM.iActiveContactRequestsBS[iContactRequest],NATIVE_TO_INT(PLAYER_ID()))							

			CPRINTLN(DEBUG_MP_CONTACT_REQUESTS,	" CLEAR_ANY_OF_MY_CONTACT_REQUESTS - had contact request set for ", iContactRequest)

			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				CLEAR_BIT(GlobalServerBD_FM.iActiveContactRequestsBS[iContactRequest],NATIVE_TO_INT(PLAYER_ID()))				
			ELSE
				BROADCAST_TELL_SERVER_CONTACT_REQUEST_COMPLETE(	ALL_PLAYERS(), INT_TO_ENUM(CONTACT_REQUEST_TYPE, iContactRequest), PLAYER_ID())
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC
#ENDIF


PROC CLEANUP_PLANE_FX_SMOKE(PTFX_ID &smokeptfx)
	//PRINTLN("[AIR_COUNTERMEASURES] CLEANUP_PLANE_FX_SMOKE - STOP_PARTICLE_FX_LOOPED ID: ", NATIVE_TO_INT(smokeptfx))
	IF DOES_PARTICLE_FX_LOOPED_EXIST(smokeptfx)
		STOP_PARTICLE_FX_LOOPED(smokeptfx)
		PRINTLN("[AIR_COUNTERMEASURES] CLEANUP_PLANE_FX_SMOKE - STOP_PARTICLE_FX_LOOPED")	
	ENDIF	
	IF IS_BIT_SET(iAirFlareBS, AIR_FLARE_BS_FIRED)
		CLEAR_BIT(iAirFlareBS, AIR_FLARE_BS_FIRED)
		PRINTLN("[AIR_COUNTERMEASURES] CLEANUP_PLANE_FX_SMOKE - AIR_FLARE_BS_FIRED FALSE")	
	ENDIF
	IF IS_BIT_SET(iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
		CLEAR_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
		PRINTLN("[AIR_COUNTERMEASURES] CLEANUP_PLANE_FX_SMOKE - AIR_COUNTERMEASURE_BS_TRIGGERED FALSE")	
	ENDIF	
	vehUsedForCountermeasure = NULL
	SET_PLAYER_AIRCRAFT_SMOKE_ACTIVE(FALSE)
	RESET_NET_TIMER(g_sCountermeasurePressDelay)
ENDPROC

PROC SCRIPT_CLEANUP()	
	PRINTLN("SCRIPT_CLEANUP - BEFORE - GET_CURRENT_STACK_SIZE() = ", GET_CURRENT_STACK_SIZE())
	g_b_IsFreemodeRunningActive = FALSE
	
	g_iFlowPhonecallSetupBitset = 0
	PRINTLN("SCRIPT_CLEANUP - g_iFlowPhonecallSetupBitset = 0")

	LAUNCH_CRIMINAL_STARTER_PACK_BROWSER(FALSE)
	
	iAirFlareBS = 0	
	CLEANUP_PLANE_FX_SMOKE(ptfxID)
	
	PRINTLN("[TS] ***********************************")
	IF IS_TRANSITION_SESSION_LAUNCHING()
		PRINTLN("[TS] * END   - Freemode      - TRANSITION SESSION LAUNCHING - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")
		#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] * END   - Freemode      - TRANSITION SESSION LAUNCHING - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")#ENDIF
	ELIF NETWORK_IS_ACTIVITY_SESSION()
		PRINTLN("[TS] * END   - Freemode      - NETWORK_IS_ACTIVITY_SESSION  - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")
		#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] * END   - Freemode      - NETWORK_IS_ACTIVITY_SESSION  - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")#ENDIF
	ELSE
		PRINTLN("[TS] * END   - Freemode      - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")
		#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] * END   - Freemode      - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")#ENDIF
	ENDIF
	PRINTLN("[TS] ***********************************")
	
	// save where player was
	IF HAS_IMPORTANT_STATS_LOADED()
	AND IS_OK_TO_STORE_RE_ENTRY_STATS(TRUE)
		STORE_PLAYER_RE_ENTRY_STATS()
	ENDIF

	GB_CLEANUP_TARGETTING()
	
	GRIEF_PASSIVE_CLEANUP()
	
	CLEANUP_HELI_DOCK(g_ExteriorHeliDockData)
	
	CLEANUP_AIRFIELD_MODEL_HIDES()
	
	SET_PED_ABLE_TO_DROWN( PLAYER_PED_ID() )
	
	UNLOAD_ALL_COMMON_LEADERBOARD_ELEMENTS(LeaderboardPlacement)
	
	NUKE_ALL_AWARD_MOVIES(AllRankupStuff)
	
	PU_RAGE_CLEANUP(PowerUpData.RageData)

	//Stop hiding the hud
	CLEAR_TRANSITION_SESSION_HIDE_HUD_ELEMENTS_DURING_TRANSITION_BACK_TO_FM()
	CLEAR_TRANSITION_SESSIONS_FREEMODE_DONT_PULL_DOWN_CAM()
	//clear the JIP coord
	CLEAR_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION_INITIAL_SPAWN()
	//clear the player vehicle flag
	CLEAR_TRANSITION_SESSIONS_PLAYER_VEHICLE_CLEAN_UP_CHECK()
	// Cleanup Widgets
	#IF IS_DEBUG_BUILD
		// Keith 14/12/11: Terminate The Generic Client and Server Routines			
		Destroy_Main_Generic_Client_Widgets()
		Destroy_Main_Generic_Server_Widgets()

		g_MainMissionScriptID = INT_TO_NATIVE(THREADID, -1)
		g_bMainMissionScriptActive = FALSE
		
		g_bDebugFMNoPhoneCalls = FALSE
		
		CLEANUP_BOTS()
		
		
		
		NET_PRINT("...KGM MP: Cleaning Up FREEMODE") NET_NL()
		
		IF lw_bUnlockAllShopItems
			PRINTLN("Freemode cleanup - lw_bUnlockAllShopItems was set, clearing")
			lw_bUnlockAllShopItems = FALSE
		ENDIF
	#ENDIF
	//Release the audio
	IF bAudioLoaded
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_GTAO/SNACKS")
		CPRINTLN(DEBUG_NET_AMBIENT, "[FM AME] - RELEASE_NAMED_SCRIPT_AUDIO_BANK(\"DLC_GTAO/SNACKS\")")
	ENDIF
	RESET_NET_TIMER(st_ScriptTimers_BIG_MESSAGE_CashDisplay)
	REMOVE_MULTIPLAYER_WALLET_CASH()
	REMOVE_MULTIPLAYER_BANK_CASH()
	PRINTLN("[FAKECASH] Freemode cleanup remove calls. ")
	
	g_i_PlaneDpadIsActive = 0
	
	MPGlobalsHud.b_HasLoadedWastedAudioBank = FALSE
	
	CLEAR_SKIP_WAIT_FOR_TRANSITION_CAMERA()
	
	RESET_MY_KICKING_DATA()
	
	IF g_SkipFmTutorials
		g_SkipFmTutorials = FALSE
		#IF IS_DEBUG_BUILD NET_NL() NET_PRINT("[dsw] Freemode cleanup g_SkipFmTutorials was set, so clearing") NET_NL() #ENDIF
	ENDIF
	
	IF NOT HAS_FM_INTRO_MISSION_BEEN_DONE()
		IF HAS_FREEMODE_INTRO_BEEN_DONE()
//			SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_FM_INTRO_CUT_DONE)
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_INTRO_CUT_DONE, FALSE)
			PRINTNL() PRINTSTRING(" [dsw] FREEMODE -Clearing MP_STAT_FM_INTRO_CUT_DONE because intro not done ") PRINTNL()
		ENDIF
	ENDIF
	
	IF g_bPassedMyTutorialMission
		IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_TRIGTUT_DONE)
			SET_MP_BOOL_PLAYER_STAT(MPPLY_CAN_SPECTATE, TRUE) 
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_RACETUT_DONE, TRUE)
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_TRIGTUT_DONE, TRUE)
			SET_DEATHMATCH_TUTORIAL_AS_COMPLETE()
			PRINTNL() PRINTSTRING(" [dsw] FREEMODE Cleanup - g_bPassedMyTutorialMission is set but MP_STAT_FM_TRIGTUT_DONE not set. Will set") PRINTNL()
		ENDIF
	ENDIF
	
	REMOVE_SCENARIO_BLOCKING_AREA(scenOutsideApartment)
	
	// Clean up police scanner radio. Allow ambient reports again.
	SET_AUDIO_FLAG("OnlyAllowScriptTriggerPoliceScanner", FALSE)
	
	CLEANUP_FREEMODE_SPECTATOR_CHAT()
	
	CLEANUP_FM_BLIPS()
	CLEANUP_AMB_HOLD_UPS(HoldUpData)
	REMOVE_ALL_FM_CAR_GENS(FM_CAR_GENERATORS_CAR_GENS)
	REMOVE_ALL_CURRENT_FM_PACKAGE_PICKUPS(FM_hidden_package)
	
	KILL_CHASE_HINT_CAM(lookAtFriend.friendHintCamStruct)
	
	//Cleanup kill cam (as if it's running now something has gone wrong and it will disable control)
	CLEANUP_KILL_CAM()
	
	IF MPGlobalsAmbience.bRunningFmIntroCut
		//-- Fix 771699
		CLEANUP_MP_CUTSCENE()
		IF IS_SCREEN_FADED_OUT()
		OR IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
	ENDIF
	
	//Cleanup Time Trial
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(),MPAM_TYPE_TIME_TRIAL)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
		TIME_TRIALS_CLEANUP(sTTVarsStruct,TRUE)
	ENDIF
	
	IF IS_BIT_SET(fmTriggerIntro.iBitset, biFmT_ForceRenderCorona)
		// Dave W
		FORCE_RENDER_IN_GAME_UI(FALSE)
		CLEAR_BIT(fmTriggerIntro.iBitset, biFmT_ForceRenderCorona)
		#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Freemode cleanup - FORCE_RENDER_IN_GAME_UI cleared biFmT_ForceRenderCorona") #ENDIF
	ENDIF
	
	IF HAS_SCALEFORM_MOVIE_LOADED(fmTriggerIntro.scaleSplash) 
		REMOVE_TUTORIAL_SPLASH(fmTriggerIntro)
	ENDIF
	
	IF HAS_SCALEFORM_MOVIE_LOADED(g_sfMPCredits)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(g_sfMPCredits)
	ENDIF
	g_bMPCreditsOn = FALSE
	g_bMPCreditsLoaded = FALSE
	g_bMPCreditsIntroDraw = FALSE
	g_bDirectorMPCreditCleared = FALSE
	
	fmTriggerTutStruct.iFmTriggerTutorialBitset = 0
	MPGlobalsAmbience.iFmNmhBitSet = 0
	CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet, BI_FM_NMH_AFTER_INTO_CUT)
	CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet, BI_FM_NMH_WAYPOINT)
	CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet, BI_FM_NMH_MINIMAP)
	CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet, BI_FM_NMH_RACE_TO_POINT)
	CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet, BI_FM_NMH_PEND_ACTIV)
	CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet, BI_FM_NMH_STUNT_JUMP)
	
	PRINTLN("SET_VEHICLE_MODEL_IS_SUPPRESSED(JET, FALSE)")
	SET_VEHICLE_MODEL_IS_SUPPRESSED(JET, FALSE)
	
	//only clear if the corona is not being cleaned up
	IF NOT SHOULD_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT()
	AND NOT SHOULD_DO_TRANSIITON_TO_NEW_FREEMODE_SESSION_WITH_ALL_PLAYERS_AFTER_CORONA()
	AND NOT SHOULD_TRANSITION_SPAWN_PLAYERS_IN_SAME_APARTMENT()
	AND GET_TRANSITION_SESSIONS_KICK_REASON() != ciCORONA_KICK_CLOUD_CANT_LOAD_FROM_USER
		CLEAR_HELP()
	ENDIF

	CLEAR_PRINTS()
	
	REMOVE_MULTIPLAYER_HUD_CASH()
	IF MPGlobals.bStartedMPCutscene
		PRINTLN("Freemode SCRIPT_CLEANUP calling CLEANUP_MP_CUTSCENE")
		CLEANUP_MP_CUTSCENE()
	ENDIF
	
	CLEAR_PLAYER_BROADCAST_PLAYING_PROPERTY_TRANSITION_CUTSCENE()
	
	MPGlobals.g_bRefresh_BD_Stat_Data = TRUE
	
	REMOVE_ALL_AIR_DEFENCE_SPHERES()
	PRINTLN("[AIRDEF] REMOVE_ALL_AIR_DEFENCE_SPHERES called in Freemode Cleanup. ")

	
	//If a transition session is launching
	IF IS_TRANSITION_SESSION_LAUNCHING()
	
	//Or we're restarting after a mission
	//OR SHOULD_TRANSITION_SESSIONS_DO_TUT_MISSION_QUICKMATCH()
		//-- Save tutorial bitset
	//	PRINTLN("[dsw] Saving out tutorial bitset...")
		CPRINTLN(DEBUG_MP_TUTORIAL, "[dsw] Saving out tutorial bitset..")
		g_iMyRaceTutorialBitset = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset
		g_iMyHoldUpTutorialBitset = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset
		g_fmTriggerIntro = fmTriggerIntro
		IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(),TRUE)
			g_bIgnorePropertyHelpForTransition = TRUE
			PRINTLN("CDM MP: Setting g_bIgnorePropertyHelpForTransition = TRUE")
		ENDIF
		IF IS_LOCAL_PLAYER_IN_ACTIVITY_TUTORIAL_CORONA()
			PRINTLN("[dsw] Saving... In corona set")
		ELSE
			PRINTLN("[dsw] Saving... In corona NOT set")
		ENDIF
	ENDIF
	
	// Moved personal vehicle recreation checks into own section so it runs for both entering and leaving a job.
	IF IS_TRANSITION_SESSION_LAUNCHING()
	OR SHOULD_TRANSITION_SESSION_LAUNCH_MAIN_FM_SCRIPT()
		
		IF IS_TRANSITION_SESSION_LAUNCHING()
			SAVE_PERSONAL_VEHICLE_END_STATE(TRUE)
		ELSE
			SAVE_PERSONAL_VEHICLE_END_STATE(FALSE,IS_BIT_SET(g_TransitionSessionNonResetVars.MissionPVBitset,ci_PV_WAS_ON_MISSION))
		ENDIF
		CLEAR_BIT(g_TransitionSessionNonResetVars.MissionPVBitset,ci_PV_WAS_ON_MISSION)
		
		IF PERSONAL_VEHICLE_NET_ID(TRUE) != NULL
			PRINTLN("FREEMODE CLEANUP: CURRENT_SAVED_VEHICLE_SLOT() = ", CURRENT_SAVED_VEHICLE_SLOT())
			IF DOES_PLAYER_OWN_ANY_VEHICLE_STORAGE()
				IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
				AND CURRENT_SAVED_VEHICLE_SLOT() < MAX_MP_SAVED_VEHICLES
					IF NOT IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) 
						SET_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_RECREATE_AFTER_SESSION)
						PRINTLN("Call 1, Setting ,MP_SAVED_VEHICLE_RECREATE_AFTER_SESSION for slot # ", CURRENT_SAVED_VEHICLE_SLOT())
					ELSE
						PRINTLN("g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED for slot # ", CURRENT_SAVED_VEHICLE_SLOT())
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					IF NOT (CURRENT_SAVED_VEHICLE_SLOT() < MAX_MP_SAVED_VEHICLES)
						SCRIPT_ASSERT("CURRENT_SAVED_VEHICLE_SLOT() < MAX_MP_SAVED_VEHICLES")
					ENDIF
					#ENDIF
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(g_MpSavedVehicles[0].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) 
					SET_BIT(g_MpSavedVehicles[0].iVehicleBS,MP_SAVED_VEHICLE_RECREATE_AFTER_SESSION)
					PRINTLN("Call 2, Setting ,MP_SAVED_VEHICLE_RECREATE_AFTER_SESSION for slot # ", 0)
				ENDIF
			ENDIF
			//REQUEST_SAVE(STAT_SAVETYPE_SCRIPT_MP_GLOBALS) //this should be handled by the match end save.
		ELSE
			PRINTLN("Freemode Cleanup: Saved vehicle = NULL")
		ENDIF	
		DISABLE_PERSONAL_VEHICLE_CREATION_FOR_TRANSITION(TRUE)
		PRINTLN("Freemode Cleanup: called DISABLE_PERSONAL_VEHICLE_CREATION_FOR_TRANSITION(TRUE)")
		//save 
		
	ENDIF
	
	// Dave W, for Lester's "Cops turn blind eye" contact ability.
	IF IS_BIT_SET(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_Activated)
		CLEAR_BIT(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_Activated)
		CLEAR_BIT(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_Personal)
		globalplayerbd_FM[NATIVE_TO_INT(PLAYER_ID())].g_bNoCopsActive = FALSE
		IF IS_BIT_SET(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_ByAffiliation)
			CLEAR_BIT(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_ByAffiliation)
		ENDIF
		IF MPGlobalsAmbience.iMyMaxWantedLevel > 0
			SET_MAX_WANTED_LEVEL(MPGlobalsAmbience.iMyMaxWantedLevel)
		ENDIF
		MPGlobalsAmbience.iLesterDisableCopsProg = 0
		PRINTLN("[dsw] [MAINTAIN_LESTER_DISABLE_COPS] Freemode cleanup resetting")
	ENDIF
		
	IF NETWORK_DOES_NETWORK_ID_EXIST(PERSONAL_VEHICLE_NET_ID())
		IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
			IF IS_VEHICLE_EMPTY(PERSONAL_VEHICLE_ID())
				SET_VEHICLE_DOORS_LOCKED(PERSONAL_VEHICLE_ID(),VEHICLELOCK_LOCKED)
			ENDIF
		ENDIF
	ENDIF
	

	addedMPInsuranceContact = FALSE
	
	BOOL bSendBackToGarage = FALSE
	IF DOES_ENTITY_EXIST(PERSONAL_VEHICLE_ID())
		IF IS_MODEL_A_PERSONAL_TRAILER(GET_ENTITY_MODEL(PERSONAL_VEHICLE_ID()))
			bSendBackToGarage = TRUE	
			PRINTLN("[personal_vehicle] freemode cleanup - is a personal trailer")
		ELSE	
			PRINTLN("[personal_vehicle] freemode cleanup - is NOT a personal trailer")	
		ENDIF
	ENDIF
	
	CLEANUP_MP_SAVED_VEHICLE((IS_PERSONAL_VEHICLE_IN_IMPOUND_YARD() OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_CONCEAL_MY_PV)), DEFAULT, bSendBackToGarage)
	
	CLEANUP_MELEE_RAMPAGE()
	
	PRINTLN("SCRIPT_CLEANUP - Cleanup_Main_Generic_Client - BEFORE - GET_CURRENT_STACK_SIZE() = ", GET_CURRENT_STACK_SIZE())
	
	Cleanup_Main_Generic_Client() // call this just before the terminate
	
	PRINTLN("SCRIPT_CLEANUP - Cleanup_Main_Generic_Client - AFTER - GET_CURRENT_STACK_SIZE() = ", GET_CURRENT_STACK_SIZE())
	
	CLEANUP_PROPERTY_BLOCKS(propertyBlocks)
	
	UNHINT_SCRIPT_AUDIO_BANK()	//Adding for Bug 2164867
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		NETWORK_SET_PLAYER_IS_PASSIVE(FALSE)
	ENDIF
	
	SET_PAUSE_INTRO_DROP_AWAY(FALSE)
	
	//Disable relaxed riding style
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_AllowBikeAlternateAnimations, FALSE)
	
	//Remove all blocking areas
	REMOVE_SCENARIO_BLOCKING_AREAS()
	#IF IS_DEBUG_BUILD
	PRINTLN(" freemode calling REMOVE_SCENARIO_BLOCKING_AREAS()")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	// Release context intention failsafe as registering script ahs cleaned up. #2838218
	IF sMagnateGangBossLocalData.iContextButtonIntention != NEW_CONTEXT_INTENTION 
		RELEASE_CONTEXT_INTENTION(sMagnateGangBossLocalData.iContextButtonIntention) 
	ENDIF 
	
	#IF IS_DEBUG_BUILD
	#IF FEATURE_WANTED_SYSTEM
	WANTED_SYSTEM_SCRIPT_CLEANUP(g_wantedSystemData)
	#ENDIF
	#ENDIF	
	
	g_SimpleInteriorData.bForceTerminateInteriorScript = FALSE

	IF eEMPPostFXState = EMP_POSTFX_STATE_RUNNING
		eEMPPostFXState = EMP_POSTFX_STATE_IDLE
	ENDIF

	IF g_bRestrictedInteriorCleanupFreemode
		INT iLoop
		PRINTLN("Freemode: SCRIPT_CLEANUP: disabling all restricted Interiors")
		REPEAT COUNT_OF(MP_RESTRICTED_INTERIORS) iLoop
			MP_RESTRICTED_INTERIORS interior = INT_TO_ENUM(MP_RESTRICTED_INTERIORS, iLoop)
			IF _UNLOAD_RESTRICTED_INTERIOR_NOW(interior)
				_SET_DOOR_STATE_FOR_RESTRICTED_INTERIOR(interior, FALSE)
			ENDIF
		ENDREPEAT
	ENDIF
	
	RELEASE_CASH_TRANSACTION_SCRIPT()
	
	PRINTLN("SCRIPT_CLEANUP - AFTER - GET_CURRENT_STACK_SIZE() = ", GET_CURRENT_STACK_SIZE())
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	
ENDPROC

FUNC INT GET_NUM_FREEMODE_PLAYERS()
	INT iParticipant
	INT iPlayerCount
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))

			iPlayerCount++
		ENDIF
	ENDREPEAT
	
	RETURN iPlayerCount
ENDFUNC
//Check to see if rockstar content should refresh
PROC MAINTAIN_REFRESH_ROCKSTAR_CONTENT_CHECK(INT iParticipant)
	#IF IS_DEBUG_BUILD
		IF g_bRunRefreshContentCheck = FALSE
			EXIT
		ENDIF
	#ENDIF
	
	PARTICIPANT_INDEX ParticipantID = INT_TO_PARTICIPANTINDEX(iParticipant)
	PLAYER_INDEX PlayerID
	//If the participant is active
	IF NETWORK_IS_PARTICIPANT_ACTIVE(ParticipantID)
		//Get the player ID
		playerID = NETWORK_GET_PLAYER_INDEX(ParticipantID)
		//get the player gbd
		INT iPlayer = NATIVE_TO_INT(PlayerID)
		
		//Check that the time stamp is ok
		IF GlobalplayerBD_FM[iPlayer].sDownLoadedContent.iTimeStamp != 0
			//If the servers time is less then set it to the most recent
			IF GlobalServerBD_FM.iTimeStamp < GlobalplayerBD_FM[iPlayer].sDownLoadedContent.iTimeStamp
				GlobalServerBD_FM.iTimeStamp = GlobalplayerBD_FM[iPlayer].sDownLoadedContent.iTimeStamp
				PRINTLN("CLOUD REF - MAINTAIN_REFRESH_ROCKSTAR_CONTENT_CHECK GlobalServerBD_FM.iTimeStamp = ", GlobalServerBD_FM.iTimeStamp)
			ENDIF
			//Check that the hash is not 0
			IF GlobalplayerBD_FM[iPlayer].sDownLoadedContent.iHash != 0
				//If the server has is 0 then set it to this players
				IF GlobalServerBD_FM.iHash = 0
					GlobalServerBD_FM.iHash = GlobalplayerBD_FM[iPlayer].sDownLoadedContent.iHash
					PRINTLN("CLOUD REF - MAINTAIN_REFRESH_ROCKSTAR_CONTENT_CHECK GlobalServerBD_FM.iHash = ", GlobalServerBD_FM.iHash)
				//Server has is not zero
				ELSE
					//If they are not equal then tell every body about it. as we want a content refresh 
					IF  GlobalServerBD_FM.iHash != GlobalplayerBD_FM[iPlayer].sDownLoadedContent.iHash
						INT iCurrentCloudTime = GET_CLOUD_TIME_AS_INT()
						//If the last cloud ref is 0
						IF GlobalServerBD_FM.iLastCloudRef = 0
						//or the cloud ref is not 0 and the current time - the last time 
						OR (GlobalServerBD_FM.iLastCloudRef != 0
						AND (iCurrentCloudTime - GlobalServerBD_FM.iLastCloudRef > 60000))
							GlobalServerBD_FM.iLastCloudRef = iCurrentCloudTime
							PRINTLN("CLOUD REF - MAINTAIN_REFRESH_ROCKSTAR_CONTENT_CHECK")
							PRINTLN("CLOUD REF - iPlayer                                                      = ", iPlayer)
							PRINTLN("CLOUD REF - GlobalplayerBD_FM[iPlayer].sDownLoadedContent.iHash          = ", GlobalplayerBD_FM[iPlayer].sDownLoadedContent.iHash)
							PRINTLN("CLOUD REF - GlobalplayerBD_FM[iPlayer].sDownLoadedContent.iTimeStamp     = ", GlobalplayerBD_FM[iPlayer].sDownLoadedContent.iTimeStamp)
							PRINTLN("CLOUD REF - GlobalServerBD_FM.iHash                                      = ", GlobalServerBD_FM.iHash)
							PRINTLN("CLOUD REF - GlobalServerBD_FM.iTimeStamp                                 = ", GlobalServerBD_FM.iTimeStamp)
							PRINTLN("CLOUD REF - GlobalServerBD_FM.iLastCloudRef                              = ", GlobalServerBD_FM.iLastCloudRef)
							GlobalServerBD_FM.iHash = 0
							GlobalServerBD_FM.iTimeStamp = 0
							BROADCAST_GENERAL_EVENT(GENERAL_EVENT_REFRESH_FM_ROCKSTAR_CREATED, ALL_PLAYERS())
						ENDIF				
					ENDIF				
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SERVER_MAINTAIN_GANG_CALL(INT iParticipant)
	
	PARTICIPANT_INDEX ParticipantID = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
	PLAYER_INDEX PlayerID
	IF NETWORK_IS_PARTICIPANT_ACTIVE(ParticipantID)
		PlayerID = NETWORK_GET_PLAYER_INDEX(ParticipantID)
	ELSE
		PlayerID = INVALID_PLAYER_INDEX()	
	ENDIF

	INT iCurrentOwner
	PLAYER_INDEX playerCurrentOwner
	INT iPlayer
	INT i
	
	//REPEAT GANG_CALL_TYPE_TOTAL_NUMBER i
	
	i = iGangCallTypeStagger
		IF GlobalServerBD_FM.iGangCallCurrentOwnerPlayer[i] = -1							//If no-one else claims ownership of this gang
			IF PlayerID <> INVALID_PLAYER_INDEX()											//If is valid player
				iPlayer = NATIVE_TO_INT(PlayerID)
				IF IS_BIT_SET(GlobalplayerBD_FM[iPlayer].iGangCallOwnerBitset, i)			//If this player claims ownership of this gang
				AND NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayer].iGangCallSeenRunning, i)		//and has cleaned up the last instance
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode Server === GlobalServerBD_FM.iGangCallCurrentOwnerPlayer[", i, "] = ", iPlayer)
					#ENDIF
					GlobalServerBD_FM.iGangCallCurrentOwnerPlayer[i] = iPlayer				//Give ownership of this gang to this player
					
					SWITCH i
						CASE GANG_CALL_TYPE_SPECIAL_THIEF
//							#IF IS_DEBUG_BUILD
//							CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode Server === GlobalServerBD_FM.iGangCallCurrentTargetPlayer[", i, "] = ", iPlayer)
//							#ENDIF
							
							GlobalServerBD_FM.iGangCallCurrentTargetPlayer[i] = GlobalplayerBD_FM[iPlayer].iGangCallTargetID
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode Server === GlobalServerBD_FM.iGangCallCurrentTargetPlayer[", i, "] = ", GlobalServerBD_FM.iGangCallCurrentTargetPlayer[i])
							#ENDIF
							#IF IS_DEBUG_BUILD
								IF GlobalServerBD_FM.iGangCallCurrentTargetPlayer[i] = -1				//No target
									GlobalServerBD_FM.iGangCallCurrentTargetPlayer[i] = iPlayer			//Set target to myself
								ENDIF
							#ENDIF
						BREAK
						CASE GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
							
							
							GlobalServerBD_FM.iGangCallCurrentTargetPlayer[i] = GlobalplayerBD_FM[iPlayer].iGangCallTargetID
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode Server === GlobalServerBD_FM.iGangCallCurrentTargetPlayer[", i, "] = ", GlobalServerBD_FM.iGangCallCurrentTargetPlayer[i])
							#ENDIF
							
							#IF IS_DEBUG_BUILD
								IF GlobalServerBD_FM.iGangCallCurrentTargetPlayer[i] = -1				//No target
									GlobalServerBD_FM.iGangCallCurrentTargetPlayer[i] = iPlayer			//Set target to myself
								ENDIF
							#ENDIF
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		ELSE																				//If already marked to be running
			iCurrentOwner = GlobalServerBD_FM.iGangCallCurrentOwnerPlayer[i]				//Get current gang owner
			playerCurrentOwner = INT_TO_PLAYERINDEX(iCurrentOwner)
			IF NOT IS_NET_PLAYER_OK(playerCurrentOwner, FALSE)								//If current owner is invalid
			OR NOT IS_BIT_SET(GlobalplayerBD_FM[iCurrentOwner].iGangCallOwnerBitset, i)		//or if current owner no longer claims ownership
				IF i = GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
					BROADCAST_TELL_SERVER_CONTACT_REQUEST_COMPLETE(ALL_PLAYERS(),REQUEST_MERCENARIES,INT_TO_PLAYERINDEX(GlobalServerBD_FM.iGangCallCurrentTargetPlayer[i]))
				ELIF i = GANG_CALL_TYPE_SPECIAL_THIEF
					BROADCAST_TELL_SERVER_CONTACT_REQUEST_COMPLETE(ALL_PLAYERS(),REQUEST_THIEF1,INT_TO_PLAYERINDEX(GlobalServerBD_FM.iGangCallCurrentTargetPlayer[i]))
				ENDIF
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode Server === Clearing GlobalServerBD_FM.iGangCallCurrentOwnerPlayer[", i, "] = ", GlobalServerBD_FM.iGangCallCurrentOwnerPlayer[i])
				#ENDIF
				GlobalServerBD_FM.iGangCallCurrentOwnerPlayer[i] = -1				//Clear ownership of gang
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode Server === Clearing GlobalServerBD_FM.iGangCallCurrentTargetPlayer[", i, "] = ", GlobalServerBD_FM.iGangCallCurrentTargetPlayer[i])
				#ENDIF
				GlobalServerBD_FM.iGangCallCurrentTargetPlayer[i] = -1				//Clear target of gang
			ENDIF
		ENDIF
	//ENDREPEAT
	
	IF (g_iServerStaggeredParticipant <= iGangCallLastParticipant)
		iGangCallTypeStagger += 1
	ENDIF
	iGangCallLastParticipant = g_iServerStaggeredParticipant	
	
	IF (iGangCallTypeStagger >= GANG_CALL_TYPE_TOTAL_NUMBER)
		iGangCallTypeStagger = 0
	ENDIF

ENDPROC

FUNC BOOL IS_PLAYER_A_BETTER_CHOICE_FOR_MOST_WANTED(PLAYER_INDEX playerID)
	IF IS_PED_ALLOWED_TO_BE_VIEWED_BY_LOCAL_PLAYER(GET_PLAYER_PED(playerID), SPEC_MODE_NEWS)
		IF NOT GlobalServerBD_FM.bMostWantedCriminalSet
		OR NATIVE_TO_INT(GlobalServerBD_FM.playerMostWanted) = -1
		OR NOT IS_NET_PLAYER_OK(GlobalServerBD_FM.playerMostWanted, TRUE)
		OR GET_PLAYER_WANTED_LEVEL(playerID) > GET_PLAYER_WANTED_LEVEL(GlobalServerBD_FM.playerMostWanted)
			IF GET_PLAYER_WANTED_LEVEL(playerID) > 0
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_PLAYER_AS_MOST_WANTED(PLAYER_INDEX playerID)
	GlobalServerBD_FM.playerMostWanted = playerID
	GlobalServerBD_FM.bMostWantedCriminalLost = FALSE
	GlobalServerBD_FM.bMostWantedCriminalSet = TRUE
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, 	"=== MP_TV === GlobalServerBD_FM.playerMostWanted = ", 
								GET_PLAYER_NAME(GlobalServerBD_FM.playerMostWanted))
	#ENDIF
ENDPROC

PROC SERVER_MAINTAIN_AMBIENT_TV_CHANNELS(INT iParticipant)
	PLAYER_INDEX playerID
	PLAYER_INDEX playerNull
	
	BOOL bEmergencySearch = FALSE
	
	IF GlobalServerBD_FM.bMostWantedCriminalSet
		IF NATIVE_TO_INT(GlobalServerBD_FM.playerMostWanted) <> -1
		AND IS_NET_PLAYER_OK(GlobalServerBD_FM.playerMostWanted, TRUE)
			IF GET_PLAYER_WANTED_LEVEL(GlobalServerBD_FM.playerMostWanted) < 1			//If no longer most wanted as have lost police
				GlobalServerBD_FM.bMostWantedCriminalLost = TRUE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== MP_TV === GlobalServerBD_FM.bMostWantedCriminalLost = TRUE")
				#ENDIF
				bEmergencySearch = TRUE
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== MP_TV === GlobalServerBD_FM.bMostWantedCriminalSet = TRUE, but most wanted not valid")
			#ENDIF
			bEmergencySearch = TRUE
		ENDIF
	ENDIF
	
	IF bEmergencySearch
		GlobalServerBD_FM.playerMostWanted = playerNull
		GlobalServerBD_FM.bMostWantedCriminalSet = FALSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== MP_TV === GlobalServerBD_FM.playerMostWanted = playerNull")
		CPRINTLN(DEBUG_SPECTATOR, "=== MP_TV === GlobalServerBD_FM.bMostWantedCriminalSet = FALSE")
		#ENDIF
		
		INT i
		REPEAT NUM_NETWORK_PLAYERS i	//Check this frame to see if there is a suitable most wanted
			IF IS_PLAYER_A_BETTER_CHOICE_FOR_MOST_WANTED(INT_TO_PLAYERINDEX(i))
				SET_PLAYER_AS_MOST_WANTED(INT_TO_PLAYERINDEX(i))
			ENDIF
		ENDREPEAT
	ELSE
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
			playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
			
			IF NATIVE_TO_INT(playerID) <> -1
				IF IS_NET_PLAYER_OK(playerID, FALSE)
					IF IS_PLAYER_A_BETTER_CHOICE_FOR_MOST_WANTED(playerID)
						SET_PLAYER_AS_MOST_WANTED(playerID)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
ENDPROC






//╔═════════════════════════════════════════════════════════════════════════════╗
//║				IDLE KICK 	url:bugstar:3885904									║

CONST_INT ciSTICK_CACHE 4
INT iStickCache[ciSTICK_CACHE]
SCRIPT_TIMER stIdleKick

PROC RESET_STICK_VARS()
	INT i
	REPEAT ciSTICK_CACHE i
		iStickCache[i] = 0
	ENDREPEAT
ENDPROC

FUNC BOOL CHECK_STICK_MOVEMENT(INT iStick, INT &iCache)

	IF (iStick > 10
	OR iStick < -10)
	AND iCache = iStick
		RETURN TRUE
	ELSE
		iCache = iStick
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DO_THE_BAIL_CLEANUP()
	SET_FRONTEND_ACTIVE(FALSE)
	SET_SKYFREEZE_FROZEN()
	SET_HAS_KICKED_IDLING_TRANSITION(TRUE) //Tell the player they idled too long
	SET_QUIT_CURRENT_GAME(TRUE)
	RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL()
	SET_SCRIPT_BAILS_PROCESSING(TRUE)
	IF IS_TRANSITION_ACTIVE() = FALSE
		TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)				
	ENDIF
ENDPROC

PROC KICK_THE_BONE_IDLE()

	BOOL bIdle
	INT iPos[ciSTICK_CACHE]

	IF NOT g_b_IsInTransition
	AND g_Private_Gamemode_Current = GAMEMODE_FM
		IF NETWORK_IS_GAME_IN_PROGRESS()
		OR IS_TRANSITION_SESSION_LAUNCHING()
		OR IS_TRANSITION_SESSION_RESTARTING() 
		OR SHOULD_TRANSITION_SESSIONS_RUN_FREEMODE_RELAUNCH() 
		
			IF NETWORK_IS_ACTIVITY_SESSION()
				IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				
					// url:bugstar:4743319 - [PUBLIC][EXPLOIT] - 4 AFK players can make around 400k per hour in vespucci job
					IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
					OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
						bIdle = TRUE
						
						PRINTLN("KICK_THE_BONE_IDLE INPUT_VEH_EXIT PC ")
					ENDIF
				ELSE	
					
					GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iPos[0], iPos[1], iPos[2], iPos[3])
					
					INT i
					REPEAT ciSTICK_CACHE i
						IF CHECK_STICK_MOVEMENT(iPos[i], iStickCache[i])
							bIdle = TRUE
						ENDIF
					ENDREPEAT
					
					// url:bugstar:4743319 - [PUBLIC][EXPLOIT] - 4 AFK players can make around 400k per hour in vespucci job
					IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
					OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
						bIdle = TRUE
						
						PRINTLN("KICK_THE_BONE_IDLE INPUT_VEH_EXIT PS4 ")
					ENDIF
				ENDIF
				
				IF bIdle
					IF NOT HAS_NET_TIMER_STARTED(stIdleKick)
						PRINTLN("[CHEAT_HANDLER] KICK_THE_BONE_IDLE, START_NET_TIMER, iPos[0] = ", iPos[0], " iPos[1] = ", iPos[1], " iPos[2] = ", iPos[2], " iPos[3] = ", iPos[3])
						START_NET_TIMER(stIdleKick)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(stIdleKick, 60000*15)
							PRINTLN("[CHEAT_HANDLER] KICK_THE_BONE_IDLE, KICKED!, iPos[0] = ", iPos[0], " iPos[1] = ", iPos[1], " iPos[2] = ", iPos[2], " iPos[3] = ", iPos[3])
							DO_THE_BAIL_CLEANUP()
						ENDIF
					ENDIF
				ELSE
					IF HAS_NET_TIMER_STARTED(stIdleKick)
						PRINTLN("[CHEAT_HANDLER] KICK_THE_BONE_IDLE, RESET_NET_TIMER, iPos[0] = ", iPos[0], " iPos[1] = ", iPos[1], " iPos[2] = ", iPos[2], " iPos[3] = ", iPos[3])
						RESET_NET_TIMER(stIdleKick)
						
						RESET_STICK_VARS()
					ENDIF
				ENDIF
			ELSE 
				RESET_STICK_VARS()
			ENDIF
		ELSE 
			RESET_STICK_VARS()
		ENDIF	
	ENDIF
ENDPROC

 // #IF FEATURE_GUNRUNNING

PROC SHARED_STAGGERED_AND_NON_STAGGERED_FUNCTIONS(INT iParticipant)

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("SHARED_STAGGERED_AND_NON_STAGGERED_FUNCTIONS")
	#ENDIF
	#ENDIF
	INT i
	PARTICIPANT_INDEX ParticipantID = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
	PLAYER_INDEX PlayerID
	IF NETWORK_IS_PARTICIPANT_ACTIVE(ParticipantID)
		PlayerID = NETWORK_GET_PLAYER_INDEX(ParticipantID)
	ELSE
		PlayerID = INVALID_PLAYER_INDEX()	
	ENDIF
	
	/*#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === SHARED_STAGGERED_AND_NON_STAGGERED_FUNCTIONS iParticipant = ", iParticipant)
	#ENDIF*/

	
	IF NOT (PlayerID = INVALID_PLAYER_INDEX())
		IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)		
					
			IF IS_NET_PLAYER_OK(PlayerId)
				
				IF PlayerId != PLAYER_ID()
				
				
					COUNT_FORMATION_PLAYERS(PlayerID)
				
					IF GET_PLAYER_TEAM(PlayerId) != GET_PLAYER_TEAM(PLAYER_ID())
						NETWORK_SET_ANTAGONISTIC_TO_PLAYER(TRUE, PlayerId)
					ENDIF
					
					//Do your thing in here Ben - url:bugstar:2221085 - We need to make sure that players cannot record themselves with a prostitute in multiplayer and that they cannot record another player with a prostitute in MP. 
					//IF NOT recording is disabled
						//IF IS_BIT_SET(GlobalplayerBD_FM_2[NATIVE_TO_INT(PlayerId)].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_PLAYER_IS_DOING_A_HOOKER)
						IF GET_PED_RESET_FLAG(GET_PLAYER_PED(PlayerId), PRF_BlockRemotePlayerRecording)
						
							//Recording dist 10m more than the max dist the player can move the R*Editor freecam.
							FLOAT fRecordingDist = REPLAY_GET_MAX_DISTANCE_ALLOWED_FROM_PLAYER() + 10.0
							
							IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(GET_PLAYER_PED(PlayerId))) < (fRecordingDist*fRecordingDist)
								REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
							ENDIF
						ENDIF
					//ENDIF

					
					//-- For "other player blips" help text.
					IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet3, BI_FM_NMH3_CHECK_FOR_OTHERS)
						IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet3, BI_FM_NMH3_FOUND_OTHERS)
							IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerId)
								 IF IS_NET_PLAYER_BLIPPED(PlayerId)
							 	
							 		SET_BIT(MPGlobalsAmbience.iFmNmhBitSet3, BI_FM_NMH3_FOUND_OTHERS)
								ENDIF
							 ENDIF
						ENDIF
					ENDIF
					
					// update rotation of players blips that need manually rotated. 3210863
					UPDATE_MANUALLY_ROTATATED_BLIP_FOR_PLAYER(PlayerID)
					
					
					//-- For race to point reminder help 
					IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet3, BI_FM_NMH3_NEARBY_PLAYERS)
						IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet3, BI_FM_NMH3_FOUND_PLAYERS)
							IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(GET_PLAYER_PED(PlayerId))) < 2500
								IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerId)
									IF IS_ENTITY_VISIBLE(GET_PLAYER_PED(PlayerId))
										SET_BIT(MPGlobalsAmbience.iFmNmhBitSet3, BI_FM_NMH3_FOUND_PLAYERS)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//-- For hint cam look-at nearest friend help text
					IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_LOOK_FOR_FRIEND)
						IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_FOUND_FRIEND)
							IF NOT IS_BIT_SET(MPGlobalsAmbience.iTaxiBitSet, biG_PlayerUsingTaxi)
								PED_INDEX pedLookAt = GET_PLAYER_PED(PlayerId)
								IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedLookAt)) < 40000
									IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerId)
										IF NOT IS_NEAREST_FRIEND_PED_IN_SAME_VEHICLE_AS_ME(pedLookAt)
											IF IS_ENTITY_VISIBLE(pedLookAt)
												IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
													GAMER_HANDLE handlePlayer
													handlePlayer = GET_GAMER_HANDLE_PLAYER(PlayerId)
													IF NETWORK_IS_FRIEND(handlePlayer)
														SET_BIT(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_FOUND_FRIEND)
														PRINTLN("[dsw] Set BI_FM_NMH5_FOUND_FRIEND as found nearby friend = ", GET_PLAYER_NAME(PlayerId))
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
										
					ENDIF
					
					IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) <> FMMC_TYPE_GB_CONTRABAND_SELL
						IF GB_IS_PLAYER_CONTRABAND(PlayerId)
							IF !GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PlayerId, PLAYER_ID())
								GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER) 
								CPRINTLN(DEBUG_NET_MAGNATE, "[RE] [MAINTAIN_FREEMODE_NON_MISSION_HELP] [GB_MAINTAIN_NOTIFICATIONS] [EXEC1] [CTRB_SELL] [HLP] FREEMODE_HELP")
							ENDIF
						ENDIF
					ENDIF
					 //#IF FEATURE_EXECUTIVE
				ENDIF
				
				
				
				//Removed from staggered loop for now. Until it can go into the main overhead system. Distance check was causing this to flash. 
		

				HANDLE_FORCE_PLAYERS_FROM_CAR(PlayerId)
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("HANDLE_FORCE_PLAYERS_FROM_CAR")
				#ENDIF
				#ENDIF
				
				MAINTAIN_LAST_JOB_PLAYERS(PlayerId)
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_LAST_JOB_PLAYERS")
				#ENDIF
				#ENDIF
						
					
				IF IS_NET_PLAYER_OK(PLAYER_ID())	
				OR SHOW_KILLER_OVERHEAD(PlayerId)
				OR IS_A_SPECTATOR_CAM_RUNNING()
				OR IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IS_PENNED_IN_SPECTATOR)
					
					BOOL bIsDpaddownActive = (GET_DPADDOWN_ACTIVATION_STATE() != DPADDOWN_NONE)				
				
					g_sOverHead.bPlayerIsAlive[iParticipant] = TRUE
					
					IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PlayerId)
						IF NOT MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bBlockGlobalOverheadsForJobIntro
						#IF IS_DEBUG_BUILD
						AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_disablevehiclehealthoverhead")
						#ENDIF
							RUN_OVERALL_FREEMODE_LOGIC_CHECKS(PlayerId, iParticipant, bIsDpaddownActive)
						ENDIF
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("RUN_OVERALL_FREEMODE_LOGIC_CHECKS")
						#ENDIF
						#ENDIF
						
						//BOOL bInDeathmatch = (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_DEATHMATCH)
						
						IF NOT MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bBlockGlobalOverheadsForJobIntro
							PROCESS_PLAYER_NAME_DISPLAY_FM_DM_RC(PlayerId, iParticipant)//, bInDeathmatch)	
							
							#IF IS_DEBUG_BUILD
							#IF SCRIPT_PROFILER_ACTIVE
							ADD_SCRIPT_PROFILE_MARKER("PROCESS_PLAYER_NAME_DISPLAY_FM_DM_RC")
							#ENDIF
							#ENDIF
						ENDIf
					ENDIF
					
					
					SET_PED_RESET_FLAG(GET_PLAYER_PED(PlayerID), PRF_DontRaiseFistsWhenLockedOn, TRUE)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("PRF_DontRaiseFistsWhenLockedOn")
					#ENDIF
					#ENDIF
				
				ELSE
					HIDE_ALL_ABOVE_HEAD_DISPLAY_FOR_PLAYER_NOW(PLAYER_ID(), PARTICIPANT_ID_TO_INT(), TRUE)
				ENDIF
				
				//Update blip for property if its ownership has changed (only if player is ok)
				REPEAT MAX_OWNED_PROPERTIES i
					IF i != PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL2
					AND i != PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL3
						//PRINTLN("CDM: TEMP: propertyDetails.iOwnedProperty[",i,"] = ",GlobalplayerBD_FM[NATIVE_TO_INT(PlayerId)].propertyDetails.iOwnedProperty[i])
						//PRINTLN("CDM: TEMP: mpPropMaintain.iPropertyPlayerOwns[",i,"] = ",mpPropMaintain.iPropertyPlayerOwns[NATIVE_TO_INT(PlayerId)][i])
						IF GlobalplayerBD_FM[NATIVE_TO_INT(PlayerId)].propertyDetails.iOwnedProperty[i] > 0
						AND GlobalplayerBD_FM[NATIVE_TO_INT(PlayerId)].propertyDetails.iOwnedProperty[i] != mpPropMaintain.iPropertyPlayerOwns[NATIVE_TO_INT(PlayerId)][i]
							IF mpPropMaintain.iPropertyPlayerOwns[NATIVE_TO_INT(PlayerId)][i] > 0
								SET_MP_PROP_FLAG(mpPropMaintain.iPropertyUpdateBlipBS,GET_PROPERTY_BUILDING(mpPropMaintain.iPropertyPlayerOwns[NATIVE_TO_INT(PlayerId)][i]))
								PRINTLN("Updating blips for property building #",GET_PROPERTY_BUILDING(mpPropMaintain.iPropertyPlayerOwns[NATIVE_TO_INT(PlayerId)][i] ))
							ENDIF
							mpPropMaintain.iPropertyPlayerOwns[NATIVE_TO_INT(PlayerId)][i] = GlobalplayerBD_FM[NATIVE_TO_INT(PlayerId)].propertyDetails.iOwnedProperty[i]
							SET_MP_PROP_FLAG(mpPropMaintain.iPropertyUpdateBlipBS,GET_PROPERTY_BUILDING(mpPropMaintain.iPropertyPlayerOwns[NATIVE_TO_INT(PlayerId)][i]))
							PRINTLN("Also Updating blips for property building#",GET_PROPERTY_BUILDING(mpPropMaintain.iPropertyPlayerOwns[NATIVE_TO_INT(PlayerId)][i] ))
						ENDIF	
					ENDIF
				ENDREPEAT
			ELSE
				HIDE_ALL_ABOVE_HEAD_DISPLAY_FOR_PLAYER_NOW(PlayerId, iParticipant)
			ENDIF	
			
			
			
			
		ELSE
			//Update blip for property if its ownership has changed
			REPEAT MAX_OWNED_PROPERTIES i
				IF mpPropMaintain.iPropertyPlayerOwns[NATIVE_TO_INT(PlayerId)][i] > 0
					SET_MP_PROP_FLAG(mpPropMaintain.iPropertyUpdateBlipBS,GET_PROPERTY_BUILDING(mpPropMaintain.iPropertyPlayerOwns[NATIVE_TO_INT(PlayerId)][i]))
					PRINTLN("player left Updating blips for property building #",GET_PROPERTY_BUILDING(mpPropMaintain.iPropertyPlayerOwns[NATIVE_TO_INT(PlayerId)][i]))
					mpPropMaintain.iPropertyPlayerOwns[NATIVE_TO_INT(PlayerId)][i] = 0
				ENDIF
			ENDREPEAT
			CLEAR_BIT(lastJobPlayers.iStoredPlayersBS, native_to_int(playerID)) //no point checking as that would be more expensive than just setting.
			CLEANUP_ABOVE_HEAD_DISPLAY(iParticipant, PlayerId)

		ENDIF
		
		// update smoking pfx for player
		MANAGE_SMOKING_PFX_ON_REMOTE_PLAYER(PlayerId)  // - this is not working but going to try an get working for DLC (see 1629384)
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF
		
	
ENDPROC

/// PURPOSE:
///    Maintains functionality asked for in B* 1332244. Allows the player to chat to all friends while in freemode main, ignoring other restrictions (such as proximity checks).
/// PARAMS:
///    iParticipantCount - freemode particpant to maintain this frame.
PROC MAINTAIN_CHAT_TO_FRIENDS_RESTRICTIONS(INT iParticipantCount)

	IF MPGlobals.g_KillStripVoiceData.bVoiceTimerActive = FALSE	//IS_NET_PLAYER_OK(PLAYER_ID())
		#IF IS_DEBUG_BUILD IF IS_KEYBOARD_KEY_PRESSED(KEY_0)	PRINTLN("CHAT - MAINTAIN_CHAT_TO_FRIENDS_RESTRICTIONS - bVoiceTimerActive = FALSE ")	ENDIF #ENDIF
		IF NOT IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(PLAYER_ID())
		AND NOT FM_EVENT_IS_VOICE_CHAT_DISABLED_FOR_PLAYER_FOR_KILL_LIST(PLAYER_ID())
			#IF IS_DEBUG_BUILD IF IS_KEYBOARD_KEY_PRESSED(KEY_0)	PRINTLN("CHAT - MAINTAIN_CHAT_TO_FRIENDS_RESTRICTIONS - LOCAL PLAYER NOT ON JOB ")	ENDIF #ENDIF
			PARTICIPANT_INDEX partId = INT_TO_PARTICIPANTINDEX(iParticipantCount)
			PLAYER_INDEX playerId
			//GAMER_HANDLE eGHandle
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(partId)
				playerId = NETWORK_GET_PLAYER_INDEX(partId)
				#IF IS_DEBUG_BUILD IF IS_KEYBOARD_KEY_PRESSED(KEY_0)	PRINTLN("CHAT - MAINTAIN_CHAT_TO_FRIENDS_RESTRICTIONS - PLAYER ACTIVE - ", GET_PLAYER_NAME(playerId))	ENDIF #ENDIF
				//eGHandle = GET_GAMER_HANDLE_PLAYER(playerId)
				//IF IS_NET_PLAYER_OK(playerId)
				IF NOT IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(playerId)
					
					#IF IS_DEBUG_BUILD IF IS_KEYBOARD_KEY_PRESSED(KEY_0)	PRINTLN("CHAT - MAINTAIN_CHAT_TO_FRIENDS_RESTRICTIONS - PLAYER NOT ON JOB - ", GET_PLAYER_NAME(playerId))	ENDIF #ENDIF
					IF DOES_PLAYER_PASS_CHAT_OPTION(playerId)
						NETWORK_OVERRIDE_CHAT_RESTRICTIONS(playerId, TRUE)
					ELSE
						NETWORK_OVERRIDE_CHAT_RESTRICTIONS(playerId, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD IF IS_KEYBOARD_KEY_PRESSED(KEY_0)	PRINTLN("CHAT - MAINTAIN_CHAT_TO_FRIENDS_RESTRICTIONS - bVoiceTimerActive = TRUE ")	ENDIF #ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Maintain changes to friend status while game is ongoing
/// PARAMS:
///    iParticipantCount - freemode particpant to maintain this frame.
PROC MAINTAIN_FRIEND_STATUS(INT iParticipantCount)
	INT i
	BOOl bStatusUpdated
	PARTICIPANT_INDEX partId = INT_TO_PARTICIPANTINDEX(iParticipantCount)
	PLAYER_INDEX playerId
	GAMER_HANDLE eGHandle
	
	IF NETWORK_IS_PARTICIPANT_ACTIVE(partId)
		playerId = NETWORK_GET_PLAYER_INDEX(partId)
		eGHandle = GET_GAMER_HANDLE_PLAYER(playerId)
		IF NETWORK_IS_FRIEND(eGHandle)
			IF NOT IS_BIT_SET(friendStatusUpdate.iBS,iParticipantCount)
				bStatusUpdated = TRUE
				PRINTLN("MAINTAIN_FRIEND_STATUS - iParticipant is a friend") 
				SET_BIT(friendStatusUpdate.iBS,iParticipantCount)
			ENDIF
		ELSE
			IF IS_BIT_SET(friendStatusUpdate.iBS,iParticipantCount)
				bStatusUpdated = TRUE
				PRINTLN("MAINTAIN_FRIEND_STATUS - iParticipant is no longer a friend") 
				CLEAR_BIT(friendStatusUpdate.iBS,iParticipantCount)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(friendStatusUpdate.iBS,iParticipantCount)
			bStatusUpdated = TRUE
			PRINTLN("MAINTAIN_FRIEND_STATUS - Friend is no longer active") 
			CLEAR_BIT(friendStatusUpdate.iBS,iParticipantCount)
		ENDIF
	ENDIF
	
	//do whatever is needed when friend status is updated
	IF bStatusUpdated
		INT iProperty
		REPEAT MAX_OWNED_PROPERTIES i
			iProperty = GlobalplayerBD_FM[iParticipantCount].propertyDetails.iOwnedProperty[i]
			IF iProperty > 0
				SET_MP_PROP_FLAG(mpPropMaintain.iPropertyUpdateBlipBS,GET_PROPERTY_BUILDING(iProperty))
				PRINTLN("Updating property building blip ",GET_PROPERTY_BUILDING(iProperty) ,"player friend status updated") 
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

//PURPOSE: Controls the creation of a Text Message sent by other players
PROC MAINTAIN_TEXT_MESSAGE_LITERAL()
	
	//Process Receiving Message in Slot 0
	IF MPGlobalsAmbience.iTMLQueueSize > 0
		IF NOT IS_STRING_NULL_OR_EMPTY(MPGlobalsAmbience.TMLdata[0].tlMessage)
			PRINTLN("  ->  MAINTAIN_TEXT_MESSAGE_LITERAL - PROCESSING MESSAGE ", MPGlobalsAmbience.TMLdata[0].tlMessage) 
			
			//GAMER_HANDLE thisGamerHandle = GET_GAMER_HANDLE_PLAYER(MPGlobalsAmbience.TMLdata[0].SentFromPlayer)
			
			IF IS_ACCOUNT_OVER_17_FOR_CHAT(PLAYER_ID())
			AND NOT NETWORK_IS_PLAYER_MUTED_BY_ME(MPGlobalsAmbience.TMLdata[0].SentFromPlayer)
			AND NATIVE_TO_INT(MPGlobalsAmbience.TMLdata[0].SentFromPlayer) != -1
			AND NOT ARE_STRINGS_EQUAL(GET_PLAYER_NAME(MPGlobalsAmbience.TMLdata[0].SentFromPlayer), "**Invalid**")
			//OR (IS_GAMER_HANDLE_VALID(thisGamerHandle) AND NETWORK_IS_FRIEND(thisGamerHandle))
				TEXT_LABEL_7 tlThisLabel = "TMLR"
				tlThisLabel += NATIVE_TO_INT(MPGlobalsAmbience.TMLdata[0].SentFromPlayer)
				
				Delete_Text_Messages_With_This_Label(tlThisLabel)
				//IF Request_MP_Comms_Txtmsg_With_Components_From_Player(MPGlobalsAmbience.TMLdata[0].SentFromPlayer, "BRIEF_STRING2", " ", TRUE, NO_INT_SUBSTRING_COMPONENT_VALUE, MPGlobalsAmbience.TMLdata[0].tlMessage, "", FALSE, ALL_MP_COMMS_MODIFIERS_CLEAR)
				IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS(NO_CHARACTER, tlThisLabel, TXTMSG_UNLOCKED,
                                                                        "", -1, GET_PLAYER_NAME(MPGlobalsAmbience.TMLdata[0].SentFromPlayer), STRING_COMPONENT, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, REPLY_IS_REQUIRED, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 1, 
																		MPGlobalsAmbience.TMLdata[0].tlMessage)

					PRINTLN("  ->  MAINTAIN_TEXT_MESSAGE_LITERAL - SENT MESSAGE ", MPGlobalsAmbience.TMLdata[0].tlMessage, " FROM ", GET_PLAYER_NAME(MPGlobalsAmbience.TMLdata[0].SentFromPlayer)) 
					
					//SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS(
					
					//tlTMLReplyID[NATIVE_TO_INT(MPGlobalsAmbience.TMLdata[0].SentFromPlayer)] = "TMLR"
					//tlTMLReplyID[NATIVE_TO_INT(MPGlobalsAmbience.TMLdata[0].SentFromPlayer)] += NATIVE_TO_INT(MPGlobalsAmbience.TMLdata[0].SentFromPlayer)
					tlTMLReplyID[NATIVE_TO_INT(MPGlobalsAmbience.TMLdata[0].SentFromPlayer)] = tlThisLabel
					PRINTLN("  ->  MAINTAIN_TEXT_MESSAGE_LITERAL - tlTMLReplyID[", NATIVE_TO_INT(MPGlobalsAmbience.TMLdata[0].SentFromPlayer), "] = TMLR", NATIVE_TO_INT(MPGlobalsAmbience.TMLdata[0].SentFromPlayer)) 
					
					MPGlobalsAmbience.TMLdata[0].tlMessage = ""
					
					MPGlobalsAmbience.iTMLQueueSize--
					IF MPGlobalsAmbience.iTMLQueueSize < 0
						MPGlobalsAmbience.iTMLQueueSize = 0
					ENDIF
					
					INT i
					REPEAT MPGlobalsAmbience.iTMLQueueSize i
						MPGlobalsAmbience.TMLdata[i].SentFromPlayer = MPGlobalsAmbience.TMLdata[i+1].SentFromPlayer
						MPGlobalsAmbience.TMLdata[i].tlMessage = MPGlobalsAmbience.TMLdata[i+1].tlMessage
						PRINTLN("  ->  MAINTAIN_TEXT_MESSAGE_LITERAL - SLOT ", i, " = ", MPGlobalsAmbience.TMLdata[i].tlMessage, " FROM ", GET_PLAYER_NAME(MPGlobalsAmbience.TMLdata[i].SentFromPlayer)) 
					ENDREPEAT
				ENDIF
			ELSE
				PRINTLN("  ->  MAINTAIN_TEXT_MESSAGE_LITERAL - IS_ACCOUNT_OVER_17_FOR_CHAT = FALSE OR NETWORK_IS_PLAYER_MUTED_BY_ME = TRUE OR PLAYER IS INVALID  - DON'T DO MESSAGE AND RESET") 
				MPGlobalsAmbience.TMLdata[0].tlMessage = ""
					
				MPGlobalsAmbience.iTMLQueueSize--
				IF MPGlobalsAmbience.iTMLQueueSize < 0
					MPGlobalsAmbience.iTMLQueueSize = 0
				ENDIF
				
				INT i
				REPEAT MPGlobalsAmbience.iTMLQueueSize i
					MPGlobalsAmbience.TMLdata[i].SentFromPlayer = MPGlobalsAmbience.TMLdata[i+1].SentFromPlayer
					MPGlobalsAmbience.TMLdata[i].tlMessage = MPGlobalsAmbience.TMLdata[i+1].tlMessage
					PRINTLN("  ->  MAINTAIN_TEXT_MESSAGE_LITERAL - SLOT ", i, " = ", MPGlobalsAmbience.TMLdata[i].tlMessage, " FROM ", GET_PLAYER_NAME(MPGlobalsAmbience.TMLdata[i].SentFromPlayer)) 
				ENDREPEAT
			ENDIF
		ENDIF
	ENDIF
	
	//Process Replying to Text Messages
	IF IS_ACCOUNT_OVER_17_FOR_CHAT(PLAYER_ID())
		IF NOT IS_STRING_NULL_OR_EMPTY(tlTMLReplyID[iTMLReplyCounter])
			//Reply
			IF GET_TEXT_MESSAGE_REPLY_STATUS(tlTMLReplyID[iTMLReplyCounter]) = REPLIED_YES
				iPlayerReplyNum = iTMLReplyCounter
			ENDIF
		ENDIF
		//Reset Counter
		iTMLReplyCounter++
		IF iTMLReplyCounter >= NUM_NETWORK_PLAYERS
			iTMLReplyCounter = 0
		ENDIF
				
		//Deal with Reply Keyboard
	    IF iPlayerReplyNum >= 0
		AND iPlayerReplyNum < NUM_NETWORK_PLAYERS
			//Check Cooldown
			IF HAS_NET_TIMER_STARTED(MPGlobalsAmbience.iSendMessageToPlayerTimer[iPlayerReplyNum])
	            IF NOT HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.iSendMessageToPlayerTimer[iPlayerReplyNum], SEND_MESSAGE_COOLDOWN)
					IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE, FALSE)
		                PRINT_HELP_WITH_PLAYER_NAME("CELL_MP_1009H", GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, iPlayerReplyNum)), GET_PLAYER_HUD_COLOUR(INT_TO_NATIVE(PLAYER_INDEX, iPlayerReplyNum))) //There is a short wait before you can send another message to ~a~
		            ENDIF
		            SET_ALL_MATCHING_TEXT_MESSAGES_TO_REPLY_REQUIRED(tlTMLReplyID[iPlayerReplyNum])
		            PRINTLN("     ---------->     MAINTAIN_TEXT_MESSAGE_LITERAL - IN COOLDOWN - SEND TO ", GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, iPlayerReplyNum)))
		            iPlayerReplyNum = -1
					EXIT
				ENDIF
			ENDIF
			
			//Double Check Player is still Valid
			IF INT_TO_NATIVE(PLAYER_INDEX, iPlayerReplyNum) = INVALID_PLAYER_INDEX()
			OR NOT NETWORK_IS_PLAYER_ACTIVE(INT_TO_NATIVE(PLAYER_INDEX, iPlayerReplyNum))
			OR NOT IS_ACCOUNT_OVER_17_FOR_CHAT(PLAYER_ID())
            OR NOT IS_ACCOUNT_OVER_17_FOR_CHAT(INT_TO_NATIVE(PLAYER_INDEX, iPlayerReplyNum))
            //OR NETWORK_IS_PLAYER_MUTED_BY_ME(INT_TO_NATIVE(PLAYER_INDEX, iPlayerReplyNum))
            OR NETWORK_IS_PLAYER_BLOCKED_BY_ME(INT_TO_NATIVE(PLAYER_INDEX, iPlayerReplyNum))
            OR NETWORK_AM_I_BLOCKED_BY_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, iPlayerReplyNum))
            //OR NETWORK_AM_I_MUTED_BY_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, iPlayerReplyNum))
				IF IS_OK_TO_PRINT_FREEMODE_HELP()
	            AND INT_TO_NATIVE(PLAYER_INDEX, iPlayerReplyNum) != INVALID_PLAYER_INDEX()
                    IF NOT ARE_STRINGS_EQUAL(GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, iPlayerReplyNum)),"**Invalid**")
						PRINT_HELP_WITH_PLAYER_NAME("CELL_MP_1009M", GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, iPlayerReplyNum)), GET_PLAYER_HUD_COLOUR(INT_TO_NATIVE(PLAYER_INDEX, iPlayerReplyNum))) //It is currently not possible to send a message to ~a~.
                	ELSE
                    	PRINT_HELP("CELL_MP_1009N") //It is currently not possible to send a message.
					ENDIF
	            ENDIF
	            SET_ALL_MATCHING_TEXT_MESSAGES_TO_REPLY_REQUIRED(tlTMLReplyID[iPlayerReplyNum])
		        PRINTLN("     ---------->     MAINTAIN_TEXT_MESSAGE_LITERAL - NO LONGER A VALID PLAYER (UNDERAGE, BLOCKED, OR MUTED) - SEND TO ", GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, iPlayerReplyNum)))
				iPlayerReplyNum = -1
				EXIT
			ENDIF
			
			//Do Keyboard
	        IF RUN_ON_SCREEN_KEYBOARD(KeyBoardData.eStatus, KeyBoardData.iProfanityToken, KeyBoardData.iCurrentStatus, FALSE, FALSE, tlSendMessage, FALSE, 0, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE)
	            tlSendMessage = GET_ONSCREEN_KEYBOARD_RESULT()
	            NET_PRINT_TIME() NET_PRINT("     ---------->     MAINTAIN_TEXT_MESSAGE_LITERAL - KEYBOARD RESULT = ") NET_PRINT(tlSendMessage) NET_NL()
	            KeyBoardData.eStatus = OSK_PENDING
	            KeyBoardData.iCurrentstatus = OSK_STAGE_SET_UP                  
	            KeyBoardData.iProfanityToken = 0
	            
	           	GAMER_HANDLE thisHandle
				thisHandle = GET_GAMER_HANDLE_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, iPlayerReplyNum))
				
	             //Send the Message to the Player
	            REINIT_NET_TIMER(MPGlobalsAmbience.iSendMessageToPlayerTimer[iPlayerReplyNum])
	           	NETWORK_SEND_TEXT_MESSAGE(tlSendMessage, thisHandle)	//BROADCAST_TEXT_MESSAGE_USING_LITERAL(tlSendMessage, SPECIFIC_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, iPlayerReplyNum)))
	            NET_PRINT_TIME() NET_PRINT("     ---------->     MAINTAIN_TEXT_MESSAGE_LITERAL - MESSAGE SENT TO ") NET_PRINT(GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, iPlayerReplyNum))) NET_NL()
	            NET_PRINT_TIME() NET_PRINT("     ---------->     MAINTAIN_TEXT_MESSAGE_LITERAL - MESSAGE = ") NET_PRINT(tlSendMessage) NET_NL()
	            
				PRINT_TICKER_WITH_PLAYER_NAME("CP_TM_SENT", INT_TO_NATIVE(PLAYER_INDEX, iPlayerReplyNum))	//Your message has been sent to ~a~
				
	            SET_ALL_MATCHING_TEXT_MESSAGES_TO_REPLY_REQUIRED(tlTMLReplyID[iPlayerReplyNum])
	            iPlayerReplyNum = -1
	            PRINTLN("     ---------->     MAINTAIN_TEXT_MESSAGE_LITERAL - PiPlayerInteractionStage = PLAYER_INTERACTION_STAGE_SELECT - B")
	        ENDIF
	        
	        IF KeyBoardData.eStatus = OSK_CANCELLED
	            NET_PRINT_TIME() NET_PRINT("     ---------->     MAINTAIN_TEXT_MESSAGE_LITERAL - KEYBOARD CLOSED - oskStatus = ") NET_PRINT_INT(ENUM_TO_INT(KeyBoardData.eStatus)) NET_NL()
	            KeyBoardData.eStatus = OSK_PENDING
	            KeyBoardData.iCurrentstatus = OSK_STAGE_SET_UP                  
	            KeyBoardData.iProfanityToken = 0
	            SET_ALL_MATCHING_TEXT_MESSAGES_TO_REPLY_REQUIRED(tlTMLReplyID[iPlayerReplyNum])
	            iPlayerReplyNum = -1
				
	            PRINTLN("     ---------->     MAINTAIN_TEXT_MESSAGE_LITERAL - PiPlayerInteractionStage = PLAYER_INTERACTION_STAGE_SELECT - B")
	        ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Need to do this in global staggered loop as everyne needs to edit the relgroup but only one player runs the backup heli script.
PROC UPDATE_BACKUP_HELI_GLOBAL_REL_GROUP(INT &iParticipantCount, BOOL &bDoUpdate, SCRIPT_TIMER &timer, INT &iFriendlyBitsetCopy)
	
	//NET_PRINT_INT(iParticipantCount)NET_NL()
	
	IF NOT HAS_NET_TIMER_STARTED(timer) // No need to do this all the time. Doing every few second will be quick enough.
		
		START_NET_TIMER(timer)
	
	ELIF HAS_NET_TIMER_EXPIRED(timer, 1000)
		
		// Get ids.
		PARTICIPANT_INDEX partId = INT_TO_PARTICIPANTINDEX(iParticipantCount)
		PLAYER_INDEX playerId
		INT iCopy
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(partId)
			
			playerId = NETWORK_GET_PLAYER_INDEX(partId)
			
			// If player is passive add to friendly list, don't want to attack passive players.
			IF IS_PLAYER_PASSIVE(playerId)
				SET_BIT(iFriendlyBitsetCopy, NATIVE_TO_INT(playerId))
			ELSE
				CLEAR_BIT(iFriendlyBitsetCopy, NATIVE_TO_INT(playerId))
			ENDIF
			
			// If this player is running the backup heli script.
			IF NETWORK_IS_PLAYER_A_PARTICIPANT_ON_SCRIPT(playerId, "AM_backup_heli", NATIVE_TO_INT(playerId))
				
				// Set flag to say someone is running it.
				g_bSomeoneRunningBackupHeliScript = TRUE
				
				// If I am checking me.
				IF playerId != PLAYER_ID()
					
					// If the player running the backup heli's friendly bitset is different to mine, update mine.
					IF iFriendlyBitsetCopy != GlobalplayerBD[NATIVE_TO_INT(playerId)].iBackupHeliRelGroupBitset
						
						#IF IS_DEBUG_BUILD
						
							INT iDebug
							
							NET_PRINT("[WJK] - UPDATE_BACKUP_HELI_GLOBAL_REL_GROUP - GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBackupHeliRelGroupBitset != GlobalplayerBD[NATIVE_TO_INT(playerId)].iBackupHeliRelGroupBitset, setting equal.")NET_NL()
							
							REPEAT NUM_NETWORK_PLAYERS iDebug
								NET_PRINT("[WJK] - UPDATE_BACKUP_HELI_GLOBAL_REL_GROUP - bit ")NET_PRINT_INT(iDebug)
								IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerId)].iBackupHeliRelGroupBitset, iDebug)
									NET_PRINT(" is set.")
								ELSE
									NET_PRINT(" is not set.")
								ENDIF
								NET_NL()
							ENDREPEAT
							
						#ENDIF
						
						bDoUpdate = TRUE
						
						iFriendlyBitsetCopy = GlobalplayerBD[NATIVE_TO_INT(playerId)].iBackupHeliRelGroupBitset
					
					ENDIF
					
				ENDIF
				
			ENDIF
			
			// If I am checking me or a player on my gang.
			IF playerId = PLAYER_ID()
			OR GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), playerId)
			
				iCopy = iFriendlyBitsetCopy
				
				// Put me into friendly bitset, I don't want my backup heli attacking me or my gang. 
				SET_BIT(iFriendlyBitsetCopy, NATIVE_TO_INT(playerId))
				
				IF iCopy != iFriendlyBitsetCopy
					bDoUpdate = TRUE
				ENDIF
				
			ENDIF
			
		ENDIF
		
		// If we have checked all players.
		IF iParticipantCount >= (NUM_NETWORK_PLAYERS - 1)
			
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBackupHeliRelGroupBitset = iFriendlyBitsetCopy
			
			// If nobody is running the backup heli script, clear the friendly list.
			IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBackupHeliRelGroupBitset != 0)
				IF NOT g_bSomeoneRunningBackupHeliScript
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBackupHeliRelGroupBitset = 0
					iFriendlyBitsetCopy = 0
					bDoUpdate = TRUE
					NET_PRINT("[WJK] - UPDATE_BACKUP_HELI_GLOBAL_REL_GROUP - g_bSomeoneRunningBackupHeliScript = FALSE, setting local iBackupHeliRelGroupBitset = 0.")NET_NL()
				ENDIF
			ENDIF
			
			// Update my local copy of the backup heli rel group.
			IF bDoUpdate
				SET_AMBIENT_GANG_MERC_PLAYER_RELATIONSHIPS(rgFM_AiAmbientGangMerc[AmbientBackupHeliMerc], GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBackupHeliRelGroupBitset)
			ENDIF
			
			// Reset flag for start of next loop.
			g_bSomeoneRunningBackupHeliScript = FALSE
			
			iParticipantCount = 0
			bDoUpdate = FALSE
			
			RESET_NET_TIMER(timer)
			
		ELSE
			
			iParticipantCount++
			
		ENDIF
		
	ENDIF
	
ENDPROC



//PURPOSE: Checks if the player accepts an invite to an Apartment
PROC MAINTAIN_INVITED_TO_APARTMENT_ACCEPT_CHECKS(PLAYER_INDEX thisPlayer)
	INT i
	IF NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
		IF PLAYER_ID() != thisPlayer
			IF Does_Basic_Invite_From_Player_Exist(thisPlayer, FMMC_TYPE_APARTMENT)
				IF ARE_PLAYERS_IN_SAME_PROPERTY(PLAYER_ID(), thisPlayer, FALSE)
					Cancel_Basic_Invite_From_Player(thisPlayer, FMMC_TYPE_APARTMENT)
					PRINTLN("    ->     MAINTAIN_INVITED_TO_APARTMENT_ACCEPT_CHECKS - CANCEL INVITE - ALREADY IN PROPERTY OF ", GET_PLAYER_NAME(thisPlayer))
				ELIF IS_PLAYER_REMOTE_BROWSING_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD) 
					Cancel_Basic_Invite_From_Player(thisPlayer, FMMC_TYPE_APARTMENT)
					PRINTLN("    ->     MAINTAIN_INVITED_TO_APARTMENT_ACCEPT_CHECKS - CANCEL INVITE - IN MOD SHOP")
				ELIF Has_Player_Accepted_Basic_Invite_From_Player(thisPlayer, FMMC_TYPE_APARTMENT)
					MPGlobalsAmbience.InvitedToApartmentData.piApartmentOwner = thisPlayer
					MPGlobalsAmbience.InvitedToApartmentData.iProperty = GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobalsAmbience.InvitedToApartmentData.piApartmentOwner)].propertyDetails.iCurrentlyInsideProperty
					
					globalPropertyEntryData.iPropertyEntered 				= MPGlobalsAmbience.InvitedToApartmentData.iProperty
					globalPropertyEntryData.ownerID							= MPGlobalsAmbience.InvitedToApartmentData.piApartmentOwner
					globalPropertyEntryData.ownerHandle						= GET_GAMER_HANDLE_PLAYER(MPGlobalsAmbience.InvitedToApartmentData.piApartmentOwner)
					globalPropertyEntryData.iEntrance 						= 0
					globalPropertyEntryData.iVehSlot 						= -1
					globalPropertyEntryData.iOldVehicleSlot					= -1
					globalPropertyEntryData.iCostOfPropertyJustBought 		= 0
					globalPropertyEntryData.iTradeInValueOfPreviousProperty = 0
					globalPropertyEntryData.iVariation = GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobalsAmbience.InvitedToApartmentData.piApartmentOwner)].propertyDetails.iCurrentlyInsideVariation
					SET_BIT(globalPropertyEntryData.iBS, GLOBAL_PROPERTY_ENTRY_BS_PLAYER_INTERACTION_INVITE)
					SET_BIT(globalPropertyEntryData.iBS, GLOBAL_PROPERTY_ENTRY_BS_NOT_IN_A_VEHICLE)
					INT iPropertySlotID
					REPEAT MAX_OWNED_PROPERTIES i
						PRINTLN("    ->     MAINTAIN_INVITED_TO_APARTMENT_ACCEPT_CHECKS owners properties slot: ",I," =  ",GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobalsAmbience.InvitedToApartmentData.piApartmentOwner)].propertyDetails.iOwnedProperty[i])
						PRINTLN("invited property = ",MPGlobalsAmbience.InvitedToApartmentData.iProperty)
						IF GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobalsAmbience.InvitedToApartmentData.piApartmentOwner)].propertyDetails.iOwnedProperty[i] > 0
						AND (GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobalsAmbience.InvitedToApartmentData.piApartmentOwner)].propertyDetails.iOwnedProperty[i] = MPGlobalsAmbience.InvitedToApartmentData.iProperty
						OR GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobalsAmbience.InvitedToApartmentData.piApartmentOwner)].propertyDetails.iOwnedProperty[i] = GET_OFFICE_GARAGE_BASE_ID_FROM_PROPERTY(MPGlobalsAmbience.InvitedToApartmentData.iProperty))
							iPropertySlotID = i
						ENDIF
					ENDREPEAT

					SET_TRANSITION_SESSION_PROPERTY_DETAILS_DATA(globalPropertyEntryData.iPropertyEntered, 
															iPropertySlotID,
															globalPropertyEntryData.iEntrance,
															globalPropertyEntryData.ownerHandle
															 ,globalPropertyEntryData.iVariation )

					SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY)											
					globalPropertyEntryData.bInteriorWarpDone = FALSE
					PRINTLN("6 globalPropertyEntryData.bInteriorWarpDone = FALSE") 
					SET_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
					PRINTLN("    ->     MAINTAIN_INVITED_TO_APARTMENT_ACCEPT_CHECKS - ACCEPTED INVITE FROM ", GET_PLAYER_NAME(thisPlayer))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Controls setting the player to Warp into the invited Apartment
PROC MAINTAIN_INVITED_TO_APARTMENT_WARP()
	//PRINTLN("MAINTAIN_INVITED_TO_APARTMENT_WARP - called this frame")
	IF NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_InitDone)
		IF IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
			IF IS_NET_PLAYER_OK(MPGlobalsAmbience.InvitedToApartmentData.piApartmentOwner)
				IF NOT HAS_NET_TIMER_STARTED(MPGlobalsAmbience.InvitedToApartmentData.delayTimer)
				OR HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.InvitedToApartmentData.delayTimer,1500,TRUE)
					IF NOT IS_SCREEN_FADING_OUT()
					AND NOT IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_OUT(500)
					ENDIF
				ENDIF
				CLEAR_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_FreezePlayer)
				CLEAR_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_FrozePlayer)
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) > 0
					SET_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_CleanupApartScript)
					SET_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_FreezePlayer)
					PRINTLN("    ->     MAINTAIN_INVITED_TO_APARTMENT_WARP - biITA_CleanupApartScript SET")
				ENDIF
				SET_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_InitDone)
				PRINTLN("    ->     MAINTAIN_INVITED_TO_APARTMENT_WARP - biITA_InitDone SET")
			ELSE
				PRINTLN("MAINTAIN_INVITED_TO_APARTMENT_WARP - Owner not ok")
			ENDIF
		ENDIF
	ELSE
		IF NOT globalPropertyEntryData.bInteriorWarpDone
		AND NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_BailFromInvite)
			IF IS_SCREEN_FADED_OUT()
				IF IS_ENTITY_VISIBLE(PLAYER_PED_ID())
					//SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
					IF IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_FreezePlayer)
					//IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_FrozePlayer)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,NSPC_FREEZE_POSITION|NSPC_SET_INVISIBLE)
						SET_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_FrozePlayer)
						PRINTLN("MAINTAIN_INVITED_TO_APARTMENT_WARP: freezing player")
					ELSE
						NET_SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,NSPC_SET_INVISIBLE)
					ENDIF
					
					SET_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_HidePlayer)
					PRINTLN("MAINTAIN_INVITED_TO_APARTMENT_WARP: hiding player for warp")
				ENDIF
			ELSE
				IF NOT IS_SCREEN_FADING_OUT()
				AND NOT IS_SCREEN_FADED_OUT()
					IF NOT HAS_NET_TIMER_STARTED(MPGlobalsAmbience.InvitedToApartmentData.delayTimer)
					OR HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.InvitedToApartmentData.delayTimer,1500,TRUE)
						IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY)
						AND IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_HEIST_DROPOFF)
						AND IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
							PRINTLN("MAINTAIN_INVITED_TO_APARTMENT_WARP: not fading player has critical entity while entering")
						ELSE
							DO_SCREEN_FADE_OUT(500)
						ENDIF
					ELSE
						PRINTLN("MAINTAIN_INVITED_TO_APARTMENT_WARP: waiting for effect screen before fade.")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_BailFromInvite)
				IF IS_SCREEN_FADED_OUT()
					IF NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(500)
						CLEAR_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_BailFromInvite)
						PRINTLN("    ->     MAINTAIN_INVITED_TO_APARTMENT_WARP - biITA_BailFromInvite cleared")
					ENDIF
				ENDIF
				IF IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_HidePlayer)
					IF NOT IS_ENTITY_VISIBLE(PLAYER_PED_ID())
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
						PRINTLN("MAINTAIN_INVITED_TO_APARTMENT_WARP: un-hiding player for bail")
					ENDIF
					IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
					AND IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_FrozePlayer)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
						PRINTLN("MAINTAIN_INVITED_TO_APARTMENT_WARP: un-freezing player for bail")
					ENDIF
					CLEAR_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_FrozePlayer)
					CLEAR_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_HidePlayer)
					CLEAR_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_FreezePlayer)
					PRINTLN("MAINTAIN_INVITED_TO_APARTMENT_WARP: clearing biITA_HidePlayer")
				ENDIF
				IF NOT g_bPropInteriorScriptRunning	
					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY)
						CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY)
						PRINTLN("MAINTAIN_INVITED_TO_APARTMENT_WARP: clearing PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY")
					ENDIF
				ENDIF
				CLEAR_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet,biITA_DoWarningScreen)
			ENDIF
			
			IF IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_DisableApartScript)
				CLEAR_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_DisableApartScript)
				PRINTLN("    ->     MAINTAIN_INVITED_TO_APARTMENT_WARP - biITA_DisableApartScript CLEARED")
			ENDIF
			IF globalPropertyEntryData.bInteriorWarpDone
			AND g_bPropInteriorScriptRunning	
				SET_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_EnteringProperty)
				PRINTLN("    ->     MAINTAIN_INVITED_TO_APARTMENT_WARP - biITA_EnteringProperty set")
			ENDIF
			CLEAR_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
			CLEAR_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_InitDone)
			CLEAR_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_HidePlayer)
			CLEAR_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_FreezePlayer)
			CLEAR_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_FrozePlayer)
			IF HAS_NET_TIMER_STARTED(MPGlobalsAmbience.InvitedToApartmentData.delayTimer)
				//IF ANIMPOSTFX_IS_RUNNING("HeistLocate")
					ANIMPOSTFX_STOP("HeistLocate")
					PRINTLN("MAINTAIN_INVITED_TO_APARTMENT_WARP - stopped HeistLocate")
				//ENDIF
				RESET_NET_TIMER(MPGlobalsAmbience.InvitedToApartmentData.delayTimer)
			ENDIF
			REQUEST_PERSONAL_VEHICLE_TO_WARP_NEAR() // added by neil, see 3163195, thought should apply to appartments too.
			PRINTLN("    ->     MAINTAIN_INVITED_TO_APARTMENT_WARP - NET_WARP_TO_COORD - DONE")
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(MPGlobalsAmbience.InvitedToApartmentData.delayTimer)
		IF IS_SCREEN_FADED_OUT()
			//IF ANIMPOSTFX_IS_RUNNING("HeistLocate")
				ANIMPOSTFX_STOP("HeistLocate")
				PRINTLN("MAINTAIN_INVITED_TO_APARTMENT_WARP - stopped HeistLocate-2")
			//ENDIF
			RESET_NET_TIMER(MPGlobalsAmbience.InvitedToApartmentData.delayTimer)
		ELSE
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			PRINTLN("MAINTAIN_INVITED_TO_APARTMENT_WARP - hiding radar for heist pull in.")
		ENDIF
	ENDIF
	
	//DEBUG TEST
	#IF IS_DEBUG_BUILD
		IF bForceInvitedToApartment
			IF GET_OWNED_PROPERTY(0) > 0
				
				MPGlobalsAmbience.InvitedToApartmentData.piApartmentOwner = PLAYER_ID()
				MPGlobalsAmbience.InvitedToApartmentData.iProperty = GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobalsAmbience.InvitedToApartmentData.piApartmentOwner)].propertyDetails.iCurrentlyInsideProperty
				SET_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
				PRINTLN("    ->     MAINTAIN_INVITED_TO_APARTMENT_WARP - SET FAKE DETAILS")
			ENDIF
			bForceInvitedToApartment = FALSE
		ENDIF
	#ENDIF
ENDPROC

PROC UPDATE_STAGGERED_LOOP_FOR_FREEMODE()
	PLAYER_INDEX PlayerID
	SHARED_STAGGERED_AND_NON_STAGGERED_FUNCTIONS(g_iStaggeredParticipant)
	MAINTAIN_CHAT_TO_FRIENDS_RESTRICTIONS(g_iStaggeredParticipant) 			// Added for B* 1332244.
	MAINTAIN_FRIEND_STATUS(g_iStaggeredParticipant)
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(g_iStaggeredParticipant))
		PlayerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(g_iStaggeredParticipant))
		MAINTAIN_INVITED_TO_APARTMENT_ACCEPT_CHECKS(PlayerID)
		MANAGE_SPAWN_FLASHING_FOR_REMOTE_PLAYER(PlayerID)
		MAINTAIN_INVITED_TO_SIMPLE_INTERIOR_CHECKS(PlayerID)
	ENDIF
ENDPROC


// Repeat through all clients
PROC MAINTAIN_NEAR_PLAYERS_LOOP()

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("MAINTAIN_NEAR_PLAYERS_LOOP")
	#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_NEAR_PLAYERS_LOOP - start")
	#ENDIF
	#ENDIF	
	
	PLAYER_INDEX PlayerId
	PARTICIPANT_INDEX ParticipantID
	INT iCount	
	
		
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_PRE_LOOP_MARKER("loop")
	#ENDIF
	#ENDIF	

	REPEAT MAX_NUM_NEAR_PLAYERS iCount
	

		PlayerId = INT_TO_NATIVE(PLAYER_INDEX, NearPlayers[iCount])
				
		IF NOT (PlayerID = INVALID_PLAYER_INDEX())
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(PlayerID)
				ParticipantID = NETWORK_GET_PARTICIPANT_INDEX(PlayerId)			
				IF NOT (g_StaggeredParticipant = ParticipantID) // this player has already been processed in the staggered loop			
				AND IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)	
				
					SHARED_STAGGERED_AND_NON_STAGGERED_FUNCTIONS(NATIVE_TO_INT(ParticipantID))
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("SHARED_STAGGERED_AND_NON_STAGGERED_FUNCTIONS")
					#ENDIF
					#ENDIF
					
				ENDIF

				
			ENDIF
			
			
		ENDIF
		
		
	ENDREPEAT
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_POST_LOOP_MARKER()
	#ENDIF
	#ENDIF	

	
	NETWORK_SET_ANTAGONISTIC_TO_PLAYER(TRUE, INVALID_PLAYER_INDEX())
	
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_NEAR_PLAYERS_LOOP - end")
	#ENDIF
	#ENDIF		
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF
	
ENDPROC

PROC MAINTAIN_BROADCAST_IF_I_AM_SESSION_HOST()
	IF NETWORK_IS_HOST()
		SET_I_AM_SESSION_HOST_FLAG()
	ELSE
		CLEAR_I_AM_SESSION_HOST_FLAG()
	ENDIF
ENDPROC



PROC MAINTAIN_AM_PROSTITUTE_LAUNCHING()

	IF NOT MPGlobalsAmbience.bInitAMProstituteLaunch
		MP_MISSION_DATA amProstituteToLaunch		
		IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()

			//IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_PROSTITUTES)
				IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_PROSTITUTE",MPGlobalsAmbience.iAMProstituteLastInstance,TRUE)
					amProstituteToLaunch.mdID.idMission = eAM_PROSTITUTE
					amProstituteToLaunch.iInstanceId = -1
					NET_PRINT("Relaunching AM_PROSTITUTE NOT NETWORK_IS_IN_TUTORIAL_SESSION") NET_NL()
					IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(amProstituteToLaunch)
						NET_PRINT_STRING_INT("Relaunched AM_PROSTITUTE not in tutorial instance is: ", amProstituteToLaunch.iInstanceId) NET_NL()
						MPGlobalsAmbience.iAMProstituteLastInstance = amProstituteToLaunch.iInstanceId 
						MPGlobalsAmbience.bInitAMProstituteLaunch = TRUE
					ENDIF
				ENDIF
			//ENDIF
		ENDIF
	ENDIF
ENDPROC


CONST_INT ARMYBASE_WAIT_TIL_OFF_MISSION		0
CONST_INT ARMYBASE_INIT						1
CONST_INT ARMYBASE_RUNNING					2

INT iLocalArmyBaseStage			= ARMYBASE_INIT

PROC MAINTAIN_AM_ARMYBASE_LAUNCHING()
	SWITCH iLocalArmyBaseStage
		CASE ARMYBASE_WAIT_TIL_OFF_MISSION	
			IF NOT NETWORK_IS_ACTIVITY_SESSION()
			OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_BLOCK_ARMYBASE_LAUNCH)
				iLocalArmyBaseStage = ARMYBASE_INIT
				PRINTLN("NET_LOAD_AND_LAUNCH_SCRIPT_FM - ARMYBASE_WAIT_TIL_OFF_MISSION TO iLocalArmyBaseStage = ARMYBASE_INIT")
			ENDIF
		BREAK
		CASE ARMYBASE_INIT
			IF NETWORK_IS_ACTIVITY_SESSION()
			AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_BLOCK_ARMYBASE_LAUNCH)
				iLocalArmyBaseStage = ARMYBASE_WAIT_TIL_OFF_MISSION
				PRINTLN("NET_LOAD_AND_LAUNCH_SCRIPT_FM - iLocalArmyBaseStage = ARMYBASE_WAIT_TIL_OFF_MISSION")
				EXIT
			ENDIF
			
			IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
		
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), <<-2076.37354, 3112.29395, 33.13584>>) < (1000*1000)
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_ARMYBASE")) = 0
					AND ARE_NETWORK_PEDS_AVAILABLE_FOR_SCRIPT_LAUNCH(5, FALSE, FALSE)
					AND ARE_NETWORK_VEHICLES_AVAILABLE_FOR_SCRIPT_LAUNCH(3, FALSE, FALSE)
						IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_ARMYBASE")
						OR NETWORK_GET_NUM_SCRIPT_PARTICIPANTS("AM_ARMYBASE") < NUM_NETWORK_PLAYERS
							IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_ARMYBASE", DEFAULT, TRUE)
								MP_MISSION_DATA ArmyBaseToLaunch
								ArmyBaseToLaunch.mdID.idMission = eAM_ARMY_BASE
								ArmyBaseToLaunch.iInstanceId = DEFAULT_MISSION_INSTANCE //GET_PLAYER_TEAM(PLAYER_ID())
								IF NET_LOAD_AND_LAUNCH_SCRIPT_FM("AM_ARMYBASE", FRIEND_STACK_SIZE, ArmyBaseToLaunch, FALSE, FALSE, FALSE, TRUE)
									PRINTLN("NET_LOAD_AND_LAUNCH_SCRIPT_FM - iLocalArmyBaseStage = ARMYBASE_RUNNING")
									iLocalArmyBaseStage = ARMYBASE_RUNNING
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ARMYBASE_RUNNING
			
			// Wait until our script is no longer running
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_ARMYBASE")) = 0
				PRINTLN("NET_LOAD_AND_LAUNCH_SCRIPT_FM - iLocalArmyBaseStage = ARMYBASE_INIT")
				iLocalArmyBaseStage = ARMYBASE_INIT
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

CONST_INT PRISON_WAIT_TIL_OFF_MISSION			0
CONST_INT PRISON_INIT							1
CONST_INT PRISON_RUNNING						2
INT iLocalPrisonStage						= PRISON_INIT

PROC MAINTAIN_PRISON_LAUNCHING()
	SWITCH iLocalPrisonStage
		CASE PRISON_WAIT_TIL_OFF_MISSION
		BREAK
		CASE PRISON_INIT
		
			IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
		
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), <<1693.1636, 2559.6162, 44.5649>>) < (1000*1000)
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_PRISON")) = 0
						IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_PRISON")
						OR NETWORK_GET_NUM_SCRIPT_PARTICIPANTS("AM_PRISON") < NUM_NETWORK_PLAYERS
							IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_PRISON", DEFAULT, TRUE)
								MP_MISSION_DATA PrisonToLaunch
								PrisonToLaunch.mdID.idMission = eAM_PRISON
								PrisonToLaunch.iInstanceId = DEFAULT_MISSION_INSTANCE //GET_PLAYER_TEAM(PLAYER_ID())
								IF NET_LOAD_AND_LAUNCH_SCRIPT_FM("AM_PRISON", FRIEND_STACK_SIZE, PrisonToLaunch, FALSE, FALSE, FALSE, TRUE)
									PRINTLN("NET_LOAD_AND_LAUNCH_SCRIPT_FM - iLocalPrisonStage = PRISON_RUNNING")
									iLocalPrisonStage = PRISON_RUNNING
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE PRISON_RUNNING
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_PRISON")) = 0
				PRINTLN("NET_LOAD_AND_LAUNCH_SCRIPT_FM - iLocalPrisonStage = PRISON_INIT")
				iLocalPrisonStage = PRISON_INIT
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


CONST_INT FAIRGROUND_INIT			0
CONST_INT FAIRGROUND_RUNNING		1
INT iLocalFairgroundStage			= FAIRGROUND_INIT

PROC MAINTAIN_FAIRGROUND_LAUNCHING()
	SWITCH iLocalFairgroundStage
		CASE FAIRGROUND_INIT
		
			IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
			AND NOT IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(PLAYER_ID())
			AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
			AND NOT IS_PLAYER_ON_IMPROMPTU_DM()
			AND NOT IS_TRANSITION_ACTIVE()
			AND NOT IS_PLAYER_IN_CORONA()
			AND NOT NETWORK_IS_ACTIVITY_SESSION()
			AND IS_SKYSWOOP_AT_GROUND()
			AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
				
				//SCTV Check - Don't run until screen has faded in, so we know we are spectating this person fully.
				IF IS_PLAYER_SPECTATING(PLAYER_ID())
					IF IS_SCREEN_FADED_OUT()
						EXIT
					ENDIF
				ENDIF
				
				IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), <<-1645.5549, -1123.8719, 17.3436>>) < ciFAIRGROUND_START_DISTANCE
					//Exit early if all disabled
					IF g_sMPTunables.bDisableFerrisWheel = TRUE
						EXIT
					ENDIF
					
					MP_MISSION_DATA RideToLaunch
					RideToLaunch.iInstanceId = DEFAULT_MISSION_INSTANCE
					
					
					//Ferris Wheel
					IF g_sMPTunables.bDisableFerrisWheel = FALSE
						IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_FERRISWHEEL")) = 0
							IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_FERRISWHEEL")
							OR NETWORK_GET_NUM_SCRIPT_PARTICIPANTS("AM_FERRISWHEEL") < NUM_NETWORK_PLAYERS
								IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_FERRISWHEEL", DEFAULT, TRUE)
									RideToLaunch.mdID.idMission = eAM_FERRISWHEEL
									IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(RideToLaunch)
										PRINTLN(" FAIRGROUND - FERRIS WHEEL LAUNCHED")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//Move On now Rides are Running
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_FERRISWHEEL")) != 0
						PRINTLN(" FAIRGROUND - iLocalFairgroundStage = FAIRGROUND_RUNNING")
						iLocalFairgroundStage = FAIRGROUND_RUNNING
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE FAIRGROUND_RUNNING
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_FERRISWHEEL")) = 0
				PRINTLN(" FAIRGROUND - iLocalFairgroundStage = FAIRGROUND_INIT")
				iLocalFairgroundStage = FAIRGROUND_INIT
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

CONST_INT AMBIENCE_INIT			0
CONST_INT AMBIENCE_RUNNING		1
INT iLocalAmbienceStage			= AMBIENCE_INIT

PROC MAINTAIN_AMBIENCE_LAUNCHING()

	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisableFreemodeEvents")
		EXIT
	ENDIF
	#ENDIF

	SWITCH iLocalAmbienceStage
		CASE AMBIENCE_INIT
		
		#IF IS_DEBUG_BUILD
			IF bAmLauncherActive
		#ENDIF
		

			
				MP_MISSION_DATA AmLauncher
				AmLauncher.iInstanceId = DEFAULT_MISSION_INSTANCE

					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_LAUNCHER")) = 0
						IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_LAUNCHER")
						OR NETWORK_GET_NUM_SCRIPT_PARTICIPANTS("AM_LAUNCHER") < NUM_NETWORK_PLAYERS
							IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_LAUNCHER", DEFAULT, TRUE)
								AmLauncher.mdID.idMission = eAM_LAUNCHER
								IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(AmLauncher)
									PRINTLN("AMBIENT LAUNCHER LAUNCHED")
								ENDIF
							ENDIF
						ENDIF
					ENDIF

					//Move On now script is running
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_LAUNCHER")) != 0
						PRINTLN(" AM_LAUNCHER - iLocalAmbienceStage = LAUNCHER_RUNNING")
						iLocalAmbienceStage = AMBIENCE_RUNNING
					ENDIF
		
		#IF IS_DEBUG_BUILD
			ENDIF
		#ENDIF
		
		BREAK
		
		CASE AMBIENCE_RUNNING
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_LAUNCHER")) = 0
				PRINTLN(" FAIRGROUND - iLocalFairgroundStage = AMBIENCE_INIT")
				iLocalAmbienceStage = AMBIENCE_INIT
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC



CONST_INT STRIPCLUB_WAIT_TILL_OFF_MISSION 		0
CONST_INT STRIPCLUB_INIT 						1
CONST_INT STRIPCLUB_RUNNING 					2
CONST_INT STRIPCLUB_FINISHED					3
INT iLocalStripclubStage		= STRIPCLUB_INIT
BOOL bStripBlipAdded
BLIP_INDEX AmbientStripClubBlip
OUTSIDE_BOUNCER_INFO BouncerInfo

PROC MAINTAIN_STRIPCLUB_FM()
	
	IF NOT IS_FM_TYPE_UNLOCKED(FMMC_TYPE_STRIP_CLUB)
		IF DOES_BLIP_EXIST(AmbientStripClubBlip)
			REMOVE_BLIP(AmbientStripClubBlip)
		ENDIF
		IF bAddedStripperClubToActList
			REMOVE_ACTIVITY_FROM_DISPLAY_LIST(vStripperClub,GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_STRIP_CLUB))
		ENDIF
		
		IF (IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID()) OR IS_FM_MISSION_LAUNCH_IN_PROGRESS() OR g_bLauncherWillControlMiniMap)
		AND NOT IS_PLAYER_ON_IMPROMPTU_DM()
		AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
			//payer is on mission don't run
			iLocalStripclubStage = STRIPCLUB_WAIT_TILL_OFF_MISSION
		ELIF IS_PLAYER_USING_BALLISTIC_EQUIPMENT(PLAYER_ID()) 
			iLocalStripclubStage = STRIPCLUB_WAIT_TILL_OFF_MISSION
		ELIF iLocalStripclubStage = STRIPCLUB_WAIT_TILL_OFF_MISSION //stripclub needs to run even if the player hasn't unlocked it yet.
			iLocalStripclubStage = STRIPCLUB_INIT
		ENDIF
	ELSE
	
		IF DOES_BLIP_EXIST(AmbientStripClubBlip)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),vStripperClub) <= 100
					IF NOT bAddedStripperClubToActList
						ADD_NAMED_ACTIVITY_TO_DISPLAY_LIST(vStripperClub,GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_STRIP_CLUB),"")
						bAddedStripperClubToActList = TRUE
					ENDIF
				ELSE
					IF bAddedStripperClubToActList
						REMOVE_ACTIVITY_FROM_DISPLAY_LIST(vStripperClub,GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_STRIP_CLUB))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		//Add a blip
		IF bStripBlipAdded = FALSE
			IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
			AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
			AND NOT g_bLauncherWillControlMiniMap
			AND NOT NETWORK_IS_ACTIVITY_SESSION()
				IF NOT DOES_BLIP_EXIST(AmbientStripClubBlip)	
					//Don't add the blip if it is to be hidden
					IF NOT SHOULD_HIDE_MISC_BLIP(ciPI_TYPE_M3_HIDE_MISC_ALL)
					AND NOT SHOULD_HIDE_ALL_MISC_BLIPS_FOR_FM_EVENT()
					AND NOT SHOULD_HIDE_MISC_BLIP(ciPI_TYPE_M3_HIDE_MISC_STRIP_CLUB)
						AmbientStripClubBlip = ADD_BLIP_FOR_COORD(<<130.1238, -1300.2657, 28.2811>>) 
						SET_BLIP_PRIORITY(AmbientStripClubBlip, BLIPPRIORITY_LOW)
						SET_BLIP_SCALE(AmbientStripClubBlip, BLIP_SIZE_NETWORK_COORD)
						SET_BLIP_SPRITE(AmbientStripClubBlip, RADAR_TRACE_STRIP_CLUB)
						SET_BLIP_AS_SHORT_RANGE(AmbientStripClubBlip, TRUE)	
						SET_BLIP_HIGH_DETAIL(AmbientStripClubBlip, FALSE)
					ENDIF
					iLocalStripclubStage = STRIPCLUB_INIT
				ENDIF
				bStripBlipAdded = TRUE
			ENDIF
		ELSE
			IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
			OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
			OR g_bLauncherWillControlMiniMap
			OR NETWORK_IS_ACTIVITY_SESSION()
			OR IS_PLAYER_USING_BALLISTIC_EQUIPMENT(PLAYER_ID()) 
				IF DOES_BLIP_EXIST(AmbientStripClubBlip)
					REMOVE_BLIP(AmbientStripClubBlip)
					IF bAddedStripperClubToActList
						REMOVE_ACTIVITY_FROM_DISPLAY_LIST(vStripperClub,GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_STRIP_CLUB))
					ENDIF
				ENDIF
				bStripBlipAdded = FALSE
				iLocalStripclubStage = STRIPCLUB_WAIT_TILL_OFF_MISSION
			ENDIF
		ENDIF
		
		//Remove the blip if we've chosen to hide it
		IF SHOULD_HIDE_MISC_BLIP(ciPI_TYPE_M3_HIDE_MISC_ALL)
		OR SHOULD_HIDE_ALL_MISC_BLIPS_FOR_FM_EVENT()
		OR SHOULD_HIDE_MISC_BLIP(ciPI_TYPE_M3_HIDE_MISC_STRIP_CLUB)
			IF DOES_BLIP_EXIST(AmbientStripClubBlip)
				REMOVE_BLIP(AmbientStripClubBlip)
				IF bAddedStripperClubToActList
					REMOVE_ACTIVITY_FROM_DISPLAY_LIST(vStripperClub,GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_STRIP_CLUB))
				ENDIF
			ENDIF
			bStripBlipAdded = FALSE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_ON_IMPROMPTU_DM() AND iLocalStripclubStage = STRIPCLUB_WAIT_TILL_OFF_MISSION AND !IS_PLAYER_USING_BALLISTIC_EQUIPMENT(PLAYER_ID()) 
		//Okay to run stripclub is on impromptu deathmatch
		iLocalStripclubStage = STRIPCLUB_INIT
	ENDIF
			
	SWITCH iLocalStripclubStage
		CASE STRIPCLUB_WAIT_TILL_OFF_MISSION
			//Nothing
		BREAK
		CASE STRIPCLUB_INIT
			IF NOT NETWORK_IS_ACTIVITY_SESSION()
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<<114.64, -1290.34, 29.68>>) < (100*100)
					IF NOT NETWORK_IS_SCRIPT_ACTIVE("sclub_front_bouncer")
					OR NETWORK_GET_NUM_SCRIPT_PARTICIPANTS("sclub_front_bouncer") < MP_STRIPCLUB_MAX_PLAYERS
					AND ARE_NETWORK_PEDS_AVAILABLE_FOR_SCRIPT_LAUNCH(1, FALSE, FALSE)
						
						IF NOT NETWORK_IS_SCRIPT_ACTIVE("sclub_front_bouncer", -1, TRUE)
							//Run bouncer script
							REQUEST_SCRIPT("sclub_front_bouncer")
							IF HAS_SCRIPT_LOADED("sclub_front_bouncer")
								BouncerInfo.bIsMp = TRUE
								START_NEW_SCRIPT_WITH_ARGS("sclub_front_bouncer", BouncerInfo, SIZE_OF(OUTSIDE_BOUNCER_INFO), DEFAULT_STACK_SIZE)
								SET_SCRIPT_AS_NO_LONGER_NEEDED("sclub_front_bouncer")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
					
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<<114.64, -1290.34, 29.68>>) < (STRIPCLUB_MP_ACTIVATION_RANGE*STRIPCLUB_MP_ACTIVATION_RANGE)
				AND NOT IS_TRANSITION_ACTIVE() AND NOT NETWORK_IS_ACTIVITY_SPECTATOR() AND NOT IS_PLAYER_SCTV(PLAYER_ID())
				//AND ARE_PREGAME_TUTORIALS_COMPLETE() //AND CNC_FLOW_Get_flow_Flag_State(ciCNC_FLOW_STRIP_CLUB_ACTIVE)
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("stripclub_mp")) = 0
					AND ARE_NETWORK_PEDS_AVAILABLE_FOR_SCRIPT_LAUNCH(MAX_NUMBER_OF_CLUB_EMPLOYEES, FALSE, FALSE)
					AND ARE_NETWORK_OBJECTS_AVAILABLE_FOR_SCRIPT_LAUNCH(MAX_NUMBER_OF_STRIPCLUB_CASH_OBJECTS + 2, FALSE, FALSE)
						IF NOT NETWORK_IS_SCRIPT_ACTIVE("stripclub_mp")
						OR NETWORK_GET_NUM_SCRIPT_PARTICIPANTS("stripclub_mp") < MP_STRIPCLUB_MAX_PLAYERS
							IF NOT NETWORK_IS_SCRIPT_ACTIVE("stripclub_mp", -1, TRUE)
								MP_MISSION_DATA StripclubToLaunch
								StripclubToLaunch.mdID.idMission = eAM_STRIPCLUB
								StripclubToLaunch.iInstanceId = DEFAULT_MISSION_INSTANCE //GET_PLAYER_TEAM(PLAYER_ID())
								IF NET_LOAD_AND_LAUNCH_SCRIPT_FM("stripclub_mp", SHOP_STACK_SIZE, StripclubToLaunch, FALSE, FALSE, FALSE, TRUE)
									PRINTLN("NET_LOAD_AND_LAUNCH_SCRIPT_FM - iLocalStripclubStage = STRIPCLUB_RUNNING")
									iLocalStripclubStage = STRIPCLUB_RUNNING
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE STRIPCLUB_RUNNING
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("stripclub_mp")) > 0
				PRINTLN("NET_LOAD_AND_LAUNCH_SCRIPT_FM - iLocalStripclubStage = STRIPCLUB_FINISHED")
				iLocalStripclubStage = STRIPCLUB_FINISHED
			ENDIF
		BREAK
		CASE STRIPCLUB_FINISHED
		
			IF IS_ENTITY_IN_STRIPCLUB(PLAYER_PED_ID())
				IF DOES_BLIP_EXIST(AmbientStripClubBlip)
					CPRINTLN(DEBUG_SCLUB, "Removing strip club blip")
					REMOVE_BLIP(AmbientStripClubBlip)
				ENDIF
			ELSE
				IF NOT DOES_BLIP_EXIST(AmbientStripClubBlip)
					CPRINTLN(DEBUG_SCLUB, "Adding strip club blip")
					AmbientStripClubBlip = ADD_BLIP_FOR_COORD(<<130.1238, -1300.2657, 28.2811>>) 
					SET_BLIP_PRIORITY(AmbientStripClubBlip, BLIPPRIORITY_LOW)
					SET_BLIP_SCALE(AmbientStripClubBlip, BLIP_SIZE_NETWORK_COORD)
					SET_BLIP_SPRITE(AmbientStripClubBlip, RADAR_TRACE_STRIP_CLUB)
					SET_BLIP_AS_SHORT_RANGE(AmbientStripClubBlip, TRUE)
					SET_BLIP_HIGH_DETAIL(AmbientStripClubBlip, FALSE)
				ENDIF	
			ENDIF
			
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("stripclub_mp")) = 0

				PRINTLN("NET_LOAD_AND_LAUNCH_SCRIPT_FM - iLocalStripclubStage = STRIPCLUB_INIT")
				iLocalStripclubStage = STRIPCLUB_INIT
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////// Dealing with Player Global Broadcast Data  ////////////////////////////////////////////////////

PROC UPDATE_CHEAT_BITSET()

	INT iPlayerID = NATIVE_TO_INT(PLAYER_ID())
	
	IF NETWORK_PLAYER_IS_CHEATER()
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].scoreData.iCheatStateBitset, CHEATBITSET_CHEATER)
			NET_NL()NET_PRINT("UPDATE_CHEAT_BITSET: ")
			NET_NL()NET_PRINT("		- iPlayerID = ")NET_PRINT_INT(iPlayerID)
			NET_NL()NET_PRINT("		- Gamertag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))
			NET_NL()NET_PRINT("		- CHEATBITSET_CHEATER Set 4 ")

			SET_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iCheatStateBitset, CHEATBITSET_CHEATER)
		ENDIF
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iCheatStateBitset, CHEATBITSET_CHEATER)
	ENDIF
	
	IF NETWORK_PLAYER_IS_BADSPORT()
	
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].scoreData.iCheatStateBitset, CHEATBITSET_BADSPORT)

			NET_NL()NET_PRINT("UPDATE_CHEAT_BITSET: ")
			NET_NL()NET_PRINT("		- iPlayerID = ")NET_PRINT_INT(iPlayerID)
			NET_NL()NET_PRINT("		- Gamertag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))
			NET_NL()NET_PRINT("		- CHEATBITSET_BADSPORT Set 1 ")

			SET_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iCheatStateBitset, CHEATBITSET_BADSPORT)
		ENDIF
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iCheatStateBitset, CHEATBITSET_BADSPORT)
	ENDIF
	
	

ENDPROC


//PURPOSE: Update players global broadcast data with current scores, xp etc
PROC MAINTAIN_PLAYERS_GLOBAL_BROADCAST_DATA(INT& StaggerIndex)

	
	IF HAS_NET_TIMER_EXPIRED(scoreDataStartTimer, 20000)
	OR MPGlobals.g_bRefresh_BD_Stat_Data

	
		IF PLAYER_ID() <> INVALID_PLAYER_INDEX()
			// Keep all player score/cash/xp data up to date.
			IF NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
			
				INT iPlayerID = NATIVE_TO_INT(PLAYER_ID())
											
				SWITCH StaggerIndex
				
					CASE 0 
						GlobalplayerBD_FM[iPlayerID].scoreData.iCash = GET_PLAYER_CASH(PLAYER_ID())
						GlobalplayerBD_FM[iPlayerID].scoreData.iXp = GET_PLAYER_XP(PLAYER_ID())
						GlobalplayerBD_FM[iPlayerID].scoreData.iGlobalXP = GET_PLAYER_GLOBAL_XP(PLAYER_ID())
						GlobalplayerBD_FM[iPlayerID].scoreData.iRank = GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID()))
						GlobalplayerBD_FM[iPlayerID].scoreData.iTeam = GET_STAT_CHARACTER_TEAM()
						
						
						IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_USING_HAIR_SA_STAT)
							GlobalplayerBD[iPlayerID].iMyCurrentHair 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA)
							GlobalplayerBD[iPlayerID].iHairTintPrime 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT)
							GlobalplayerBD[iPlayerID].iHairTintSecondary 	= GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT)
						ELSE
							GlobalplayerBD[iPlayerID].iMyCurrentHair 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO) 
							GlobalplayerBD[iPlayerID].iHairTintPrime 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT)
							GlobalplayerBD[iPlayerID].iHairTintSecondary 	= GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT)
						ENDIF
						
						PED_COMP_NAME_ENUM eMyCurrentHair
						eMyCurrentHair = INT_TO_ENUM(PED_COMP_NAME_ENUM, GlobalplayerBD[iPlayerID].iMyCurrentHair)
						IF (eMyCurrentHair != DUMMY_PED_COMP)
							PED_COMP_NAME_ENUM eGRHairItem
							eGRHairItem = DUMMY_PED_COMP
							IF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01)
								eGRHairItem = GET_MALE_HAIR(eMyCurrentHair)
							ELIF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01)
								eGRHairItem = GET_FEMALE_HAIR(eMyCurrentHair)
							ENDIF
							
							IF (eGRHairItem != DUMMY_PED_COMP)
							AND (eMyCurrentHair != eGRHairItem)
								PRINTLN("[MAINTAIN_BROADCAST_DATA][0] gr_hair: replacing hair enum ", eMyCurrentHair, " with gunrunning hair enum ", eGRHairItem)
								GlobalplayerBD[iPlayerID].iMyCurrentHair = ENUM_TO_INT(eGRHairItem)
							ENDIF
						ENDIF

						StaggerIndex++
					BREAK
					
					CASE 1
						// Update every frame
						GlobalplayerBD_FM[iPlayerID].scoreData.fKDRatio = GET_MP_FLOAT_PLAYER_STAT(MPPLY_KILL_DEATH_RATIO)
						GlobalplayerBD_FM[iPlayerID].scoreData.iFired = GET_MP_INT_CHARACTER_STAT(MP_STAT_SHOTS)
						GlobalplayerBD_FM[iPlayerID].scoreData.iHit = GET_MP_INT_CHARACTER_STAT(MP_STAT_HITS_PEDS_VEHICLES)
						GlobalplayerBD_FM[iPlayerID].scoreData.iKills = GET_MP_INT_PLAYER_STAT(MPPLY_KILLS_PLAYERS)
						GlobalplayerBD_FM[iPlayerID].scoreData.iDeaths = GET_MP_INT_PLAYER_STAT(MPPLY_DEATHS_PLAYER) +  GET_MP_INT_PLAYER_STAT(MPPLY_DEATHS_PLAYER_SUICIDE)

						StaggerIndex++
					BREAK
					
					CASE 2
						// These should be initialised then only updated when incremented
						GlobalplayerBD_FM[iPlayerID].scoreData.iRacesWon = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_RACES_WON)
						GlobalplayerBD_FM[iPlayerID].scoreData.iRacesLost = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_RACES_LOST)
						GlobalplayerBD_FM[iPlayerID].scoreData.iRaceTop3 = GET_MP_INT_PLAYER_STAT(MPPLY_TIMES_FINISH_RACE_TOP_3)
						GlobalplayerBD_FM[iPlayerID].scoreData.iRaceFinishLast = GET_MP_INT_PLAYER_STAT(MPPLY_TIMES_FINISH_RACE_LAST)
						GlobalplayerBD_FM[iPlayerID].scoreData.iRaceBestLapHeld = GET_MP_INT_PLAYER_STAT(MPPLY_TIMES_RACE_BEST_LAP)
						StaggerIndex++
					BREAK
					
					CASE 3
						GlobalplayerBD_FM[iPlayerID].scoreData.iDMWon = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_DEATHMATCH_WON)
						GlobalplayerBD_FM[iPlayerID].scoreData.iDMLost = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_DEATHMATCH_LOST)
						
						GlobalplayerBD_FM[iPlayerID].scoreData.iTDMWon = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TDEATHMATCH_WON)
						GlobalplayerBD_FM[iPlayerID].scoreData.iTDMLost = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TDEATHMATCH_LOST)
						GlobalplayerBD_FM[iPlayerID].scoreData.iDMTop3 = GET_MP_INT_PLAYER_STAT(MPPLY_TIMES_FINISH_DM_TOP_3)
						GlobalplayerBD_FM[iPlayerID].scoreData.iDMFinishLast = GET_MP_INT_PLAYER_STAT(MPPLY_TIMES_FINISH_DM_LAST)
						StaggerIndex++
					
					BREAK
					
					CASE 4
					
						GlobalplayerBD_FM[iPlayerID].scoreData.iDartsWon = GET_MP_INT_PLAYER_STAT(MPPLY_DARTS_TOTAL_WINS)
						GlobalplayerBD_FM[iPlayerID].scoreData.iDartsMatches = GET_MP_INT_PLAYER_STAT(MPPLY_DARTS_TOTAL_MATCHES)
						
						GlobalplayerBD_FM[iPlayerID].scoreData.iArmWon = GET_MP_INT_PLAYER_STAT(MPPLY_ARMWRESTLING_TOTAL_WINS)
						GlobalplayerBD_FM[iPlayerID].scoreData.iArmMatches = GET_MP_INT_PLAYER_STAT(MPPLY_ARMWRESTLING_TOTAL_MATCH)
						
						GlobalplayerBD_FM[iPlayerID].scoreData.iTennisWon = GET_MP_INT_PLAYER_STAT(MPPLY_TENNIS_MATCHES_WON)
						GlobalplayerBD_FM[iPlayerID].scoreData.iTennisLost = GET_MP_INT_PLAYER_STAT(MPPLY_TENNIS_MATCHES_LOST)
						
						GlobalplayerBD_FM[iPlayerID].scoreData.iBasejumpWon = GET_MP_INT_PLAYER_STAT(MPPLY_BJ_WINS)
						GlobalplayerBD_FM[iPlayerID].scoreData.iBasejumpLost = GET_MP_INT_PLAYER_STAT(MPPLY_BJ_LOST)
						StaggerIndex++
					BREAK
				
					CASE 5
					
						GlobalplayerBD_FM[iPlayerID].scoreData.iGolfWon = GET_MP_INT_PLAYER_STAT(MPPLY_GOLF_WINS)
						GlobalplayerBD_FM[iPlayerID].scoreData.iGolfLost = GET_MP_INT_PLAYER_STAT(MPPLY_GOLF_LOSSES)
						
						GlobalplayerBD_FM[iPlayerID].scoreData.iShootingWon = GET_MP_INT_PLAYER_STAT(MPPLY_SHOOTINGRANGE_WINS)
						GlobalplayerBD_FM[iPlayerID].scoreData.iShootingLost = GET_MP_INT_PLAYER_STAT(MPPLY_SHOOTINGRANGE_LOSSES) 
						GlobalplayerBD_FM[iPlayerID].scoreData.iShootingSkill = GET_MP_INT_CHARACTER_STAT(MP_STAT_SHOOTING_ABILITY)
						
						GlobalplayerBD_FM[iPlayerID].scoreData.iSurvivalWon = GET_MP_INT_PLAYER_STAT(MPPLY_HORDEWINS)
						GlobalplayerBD_FM[iPlayerID].scoreData.iSurvivalTotal = GET_MP_INT_PLAYER_STAT(MPPLY_CRHORDE)
						
						GlobalplayerBD_FM[iPlayerID].scoreData.iMissionWon = GET_MP_INT_PLAYER_STAT(MPPLY_MCMWIN)
						GlobalplayerBD_FM[iPlayerID].scoreData.iMissionTotal = GET_MP_INT_PLAYER_STAT(MPPLY_CRMISSION)
					
						StaggerIndex++
					
					BREAK
					
					CASE 6
					
						// Get that unlock stats for current player for use within corona
						IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PLANE_ACCESS)
							SET_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iVehicleAccess, STAT_CARD_PLANE_UNLOCK)
						ELSE
							CLEAR_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iVehicleAccess, STAT_CARD_PLANE_UNLOCK)
						ENDIF
						
						IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HELI_ACCESS)
							SET_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iVehicleAccess, STAT_CARD_HELI_UNLOCK)
						ELSE
							CLEAR_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iVehicleAccess, STAT_CARD_HELI_UNLOCK)
						ENDIF
						
						IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_BOAT_ACCESS)
							SET_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iVehicleAccess, STAT_CARD_BOAT_UNLOCK)
						ELSE
							CLEAR_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iVehicleAccess, STAT_CARD_BOAT_UNLOCK)
						ENDIF
						
						IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_DOES_PLAYER_HAVE_VEH_ACCESS)
							SET_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iVehicleAccess, STAT_CARD_PERSONAL_CAR_UNLOCK)
						ELSE
							CLEAR_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iVehicleAccess, STAT_CARD_PERSONAL_CAR_UNLOCK)
						ENDIF
						
						// Update our BD data to include control type for use within corona
						GlobalplayerBD_FM[iPlayerID].sClientCoronaData.iControlType = GET_TRANSITION_SESSION_CONTROL_TYPE()
						
						StaggerIndex++
						
						
					BREAK
					
					CASE 7 
						GlobalplayerBD_FM[iPlayerID].scoreData.iMissionsCreated = GET_MP_INT_PLAYER_STAT(MPPLY_MISSIONS_CREATED)
						GlobalplayerBD_FM[iPlayerID].scoreData.fDropOutRate 	= GET_MP_FLOAT_PLAYER_STAT(MPPLY_DROPOUTRATE)
						GlobalplayerBD_FM[iPlayerID].scoreData.iCrewXp			= GET_PLAYER_FM_CREW_XP(PLAYER_ID())
						
						TEXT_LABEL_31 tlCrewRankTitle
						tlCrewRankTitle = GET_RANK_TEXTLABEL( GlobalplayerBD_FM[iPlayerID].scoreData.iCrewXp, GET_PLAYER_GLOBAL_TEAM(PLAYER_ID()), TRUE, MP_SETTING_TITLE_CREW)
						IF NOT ARE_STRINGS_EQUAL(tlCrewRankTitle, GET_FILENAME_FOR_AUDIO_CONVERSATION("HUD_NOCREWTIT"))
							GlobalplayerBD_FM[iPlayerID].scoreData.tlCrewRankTitle = tlCrewRankTitle
						ELSE
							GlobalplayerBD_FM[iPlayerID].scoreData.tlCrewRankTitle = ""
						ENDIF
						
						UPDATE_CHEAT_BITSET()
						UPDATE_LOCAL_PLAYER_AUTOMUTE()
						UPDATE_LOCAL_PLAYER_AUTOMUTE_BADSPORT()
						
						GlobalplayerBD_FM[iPlayerID].scoreData.bCanSpectate		= GET_MP_BOOL_PLAYER_STAT(MPPLY_CAN_SPECTATE)
						
						StaggerIndex++
						
						
					BREAK
					
					CASE 8
						GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iFormationtime0				= GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME0)
						GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iFormationtime1				= GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME1)
						GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iFormationtime2				= GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME2)
						GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iFormationtime3				= GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME3)
						SET_PLAYERS_FAVOURITE_PLAYLIST(GET_LOCAL_PLAYERS_FAVOURITE_JUKEBOX_PLAYLIST_ID())
//						GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iXXX_Four					 	= GET_MP_INT_CHARACTER_STAT(MP_STAT_XXX_Four)
						GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iMembersmarkedfordeath		= GET_MP_INT_CHARACTER_STAT(MP_STAT_MEMBERSMARKEDFORDEATH)
						
						GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iMckills		 				= GET_MP_INT_CHARACTER_STAT(MP_STAT_MCKILLS)
						GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iMcdeaths						= GET_MP_INT_CHARACTER_STAT(MP_STAT_MCDEATHS)
						GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iRivalpresidentkills			= GET_MP_INT_CHARACTER_STAT(MP_STAT_RIVALPRESIDENTKILLS)
						GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iRivalceoandvipkills	 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_RIVALCEOANDVIPKILLS)
						GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iMeleekills			 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_MELEEKILLS)
						
						GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iClubhousecontractscomplete	= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUBHOUSECONTRACTSCOMPLETE)
						GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iClubhousecontractearnings	= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUBHOUSECONTRACTEARNINGS)
						GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iClubworkcompleted			= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUBWORKCOMPLETED)
						GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iClubchallengescompleted		= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUBCHALLENGESCOMPLETED)
						GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iMemberchallengescompleted	= GET_MP_INT_CHARACTER_STAT(MP_STAT_MEMBERCHALLENGESCOMPLETED)
						
						g_bClubhouseMissionWallUpdate = TRUE
						StaggerIndex = 9
					BREAK
					
					CASE 9
						
						//GlobalplayerBD_FM[iPlayerID].scoreData.iSuicides = GET_MP_INT_CHARACTER_STAT(MP_STAT_SUICIDES)
						GlobalplayerBD_FM[iPlayerID].scoreData.iSexActs = GET_MP_INT_CHARACTER_STAT(MP_STAT_PROSTITUTES_FREQUENTED)
						GlobalplayerBD_FM[iPlayerID].scoreData.iLapDances = GET_MP_INT_CHARACTER_STAT(MP_STAT_LAP_DANCED_BOUGHT)
						GlobalplayerBD_FM[iPlayerID].scoreData.mnFavouriteVehicle = INT_TO_ENUM(MODEL_NAMES, GET_MP_INT_CHARACTER_STAT(MP_STAT_FAVORITE_VEH_TYPE_ALLTIME))
						GlobalplayerBD_FM[iPlayerID].scoreData.iFavouriteStation = GET_MP_INT_PLAYER_STAT(MPPLY_MOST_FAVORITE_STATION)
						GlobalplayerBD_FM[iPlayerID].scoreData.iEarnings = GET_LOCAL_PLAYER_VC_AMOUNT(TRUE)
						GlobalplayerBD_FM[iPlayerID].scoreData.fAccuracy = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_WEAPON_ACCURACY)
						StaggerIndex = 10
					BREAK
					
					CASE 10
						GlobalplayerBD_FM[iPlayerID].scoreData.mnFavouriteWeapon = GET_PLAYERS_MOST_USED_WEAPON()
						StaggerIndex = 999
					BREAK
				
					CASE 999
						REINIT_NET_TIMER(scoreDataStartTimer)
						StaggerIndex = 0 
					
					BREAK
				
				ENDSWITCH
			
				
				IF MPGlobals.g_bRefresh_BD_Stat_Data	
					GlobalplayerBD_FM[iPlayerID].scoreData.iCash = GET_PLAYER_CASH(PLAYER_ID())
					GlobalplayerBD_FM[iPlayerID].scoreData.iXp = GET_PLAYER_XP(PLAYER_ID())
					GlobalplayerBD_FM[iPlayerID].scoreData.iGlobalXP = GET_PLAYER_GLOBAL_XP(PLAYER_ID())
					GlobalplayerBD_FM[iPlayerID].scoreData.iRank = GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID()))
					GlobalplayerBD_FM[iPlayerID].scoreData.iTeam = GET_STAT_CHARACTER_TEAM()
					
					IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_USING_HAIR_SA_STAT)
						GlobalplayerBD[iPlayerID].iMyCurrentHair 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA) 
						GlobalplayerBD[iPlayerID].iHairTintPrime 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT)
						GlobalplayerBD[iPlayerID].iHairTintSecondary 	= GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT)
					ELSE							
						GlobalplayerBD[iPlayerID].iMyCurrentHair 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO) 
						GlobalplayerBD[iPlayerID].iHairTintPrime 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT)
						GlobalplayerBD[iPlayerID].iHairTintSecondary 	= GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT)
					ENDIF
					
					PED_COMP_NAME_ENUM eMyCurrentHair = INT_TO_ENUM(PED_COMP_NAME_ENUM, GlobalplayerBD[iPlayerID].iMyCurrentHair)
					IF (eMyCurrentHair != DUMMY_PED_COMP)
						PED_COMP_NAME_ENUM eGRHairItem = DUMMY_PED_COMP
						IF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01)
							eGRHairItem = GET_MALE_HAIR(eMyCurrentHair)
						ELIF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01)
							eGRHairItem = GET_FEMALE_HAIR(eMyCurrentHair)
						ENDIF
						
						IF (eGRHairItem != DUMMY_PED_COMP)
						AND (eMyCurrentHair != eGRHairItem)
							PRINTLN("[MAINTAIN_BROADCAST_DATA][refresh] gr_hair: replacing hair enum ", eMyCurrentHair, " with gunrunning hair enum ", eGRHairItem)
							GlobalplayerBD[iPlayerID].iMyCurrentHair = ENUM_TO_INT(eGRHairItem)
						ENDIF
					ENDIF
					
					PRINTLN("PLAYERS CURRENT HAIR SET IN FREEMODE GlobalplayerBD[iPlayerID].iMyCurrentHair =", GlobalplayerBD[iPlayerID].iMyCurrentHair)
					
					// Update every frame
					GlobalplayerBD_FM[iPlayerID].scoreData.fKDRatio = GET_MP_FLOAT_PLAYER_STAT(MPPLY_KILL_DEATH_RATIO)
					GlobalplayerBD_FM[iPlayerID].scoreData.iFired = GET_MP_INT_CHARACTER_STAT(MP_STAT_SHOTS)
					GlobalplayerBD_FM[iPlayerID].scoreData.iHit = GET_MP_INT_CHARACTER_STAT(MP_STAT_HITS_PEDS_VEHICLES)
					GlobalplayerBD_FM[iPlayerID].scoreData.iKills = GET_MP_INT_PLAYER_STAT(MPPLY_KILLS_PLAYERS)
					GlobalplayerBD_FM[iPlayerID].scoreData.iDeaths = GET_MP_INT_PLAYER_STAT(MPPLY_DEATHS_PLAYER) +  GET_MP_INT_PLAYER_STAT(MPPLY_DEATHS_PLAYER_SUICIDE)
					
					// These should be initialised then only updated when incremented
					GlobalplayerBD_FM[iPlayerID].scoreData.iRacesWon = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_RACES_WON)
					GlobalplayerBD_FM[iPlayerID].scoreData.iRacesLost = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_RACES_LOST)
					GlobalplayerBD_FM[iPlayerID].scoreData.iRaceTop3 = GET_MP_INT_PLAYER_STAT(MPPLY_TIMES_FINISH_RACE_TOP_3)
					GlobalplayerBD_FM[iPlayerID].scoreData.iRaceFinishLast = GET_MP_INT_PLAYER_STAT(MPPLY_TIMES_FINISH_RACE_LAST)
					GlobalplayerBD_FM[iPlayerID].scoreData.iRaceBestLapHeld = GET_MP_INT_PLAYER_STAT(MPPLY_TIMES_RACE_BEST_LAP)
					
					GlobalplayerBD_FM[iPlayerID].scoreData.iDMWon = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_DEATHMATCH_WON)
					GlobalplayerBD_FM[iPlayerID].scoreData.iDMLost = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_DEATHMATCH_LOST)
					
					GlobalplayerBD_FM[iPlayerID].scoreData.iTDMWon = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TDEATHMATCH_WON)
					GlobalplayerBD_FM[iPlayerID].scoreData.iTDMLost = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TDEATHMATCH_LOST)
					GlobalplayerBD_FM[iPlayerID].scoreData.iDMTop3 = GET_MP_INT_PLAYER_STAT(MPPLY_TIMES_FINISH_DM_TOP_3)
					GlobalplayerBD_FM[iPlayerID].scoreData.iDMFinishLast = GET_MP_INT_PLAYER_STAT(MPPLY_TIMES_FINISH_DM_LAST)
					
					GlobalplayerBD_FM[iPlayerID].scoreData.iDartsWon = GET_MP_INT_PLAYER_STAT(MPPLY_DARTS_TOTAL_WINS)
					GlobalplayerBD_FM[iPlayerID].scoreData.iDartsMatches = GET_MP_INT_PLAYER_STAT(MPPLY_DARTS_TOTAL_MATCHES)
					
					GlobalplayerBD_FM[iPlayerID].scoreData.iArmWon = GET_MP_INT_PLAYER_STAT(MPPLY_ARMWRESTLING_TOTAL_WINS)
					GlobalplayerBD_FM[iPlayerID].scoreData.iArmMatches = GET_MP_INT_PLAYER_STAT(MPPLY_ARMWRESTLING_TOTAL_MATCH)
					
					GlobalplayerBD_FM[iPlayerID].scoreData.iTennisWon = GET_MP_INT_PLAYER_STAT(MPPLY_TENNIS_MATCHES_WON)
					GlobalplayerBD_FM[iPlayerID].scoreData.iTennisLost = GET_MP_INT_PLAYER_STAT(MPPLY_TENNIS_MATCHES_LOST)
					
					GlobalplayerBD_FM[iPlayerID].scoreData.iBasejumpWon = GET_MP_INT_PLAYER_STAT(MPPLY_BJ_WINS)
					GlobalplayerBD_FM[iPlayerID].scoreData.iBasejumpLost = GET_MP_INT_PLAYER_STAT(MPPLY_BJ_LOST)
					
					GlobalplayerBD_FM[iPlayerID].scoreData.iGolfWon = GET_MP_INT_PLAYER_STAT(MPPLY_GOLF_WINS)
					GlobalplayerBD_FM[iPlayerID].scoreData.iGolfLost = GET_MP_INT_PLAYER_STAT(MPPLY_GOLF_LOSSES)
					
					GlobalplayerBD_FM[iPlayerID].scoreData.iShootingWon = GET_MP_INT_PLAYER_STAT(MPPLY_SHOOTINGRANGE_WINS)
					GlobalplayerBD_FM[iPlayerID].scoreData.iShootingLost = GET_MP_INT_PLAYER_STAT(MPPLY_SHOOTINGRANGE_LOSSES) 
					GlobalplayerBD_FM[iPlayerID].scoreData.iShootingSkill = GET_MP_INT_CHARACTER_STAT(MP_STAT_SHOOTING_ABILITY)
					
					GlobalplayerBD_FM[iPlayerID].scoreData.iSurvivalWon = GET_MP_INT_PLAYER_STAT(MPPLY_HORDEWINS)
					GlobalplayerBD_FM[iPlayerID].scoreData.iSurvivalTotal = GET_MP_INT_PLAYER_STAT(MPPLY_CRHORDE)
					
					GlobalplayerBD_FM[iPlayerID].scoreData.iMissionWon = GET_MP_INT_PLAYER_STAT(MPPLY_MCMWIN)
					GlobalplayerBD_FM[iPlayerID].scoreData.iMissionTotal = GET_MP_INT_PLAYER_STAT(MPPLY_CRMISSION)
					
					// Get that unlock stats for current player
					IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PLANE_ACCESS)
						SET_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iVehicleAccess, STAT_CARD_PLANE_UNLOCK)
					ELSE
						CLEAR_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iVehicleAccess, STAT_CARD_PLANE_UNLOCK)
					ENDIF
					
					IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HELI_ACCESS)
						SET_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iVehicleAccess, STAT_CARD_HELI_UNLOCK)
					ELSE
						CLEAR_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iVehicleAccess, STAT_CARD_HELI_UNLOCK)
					ENDIF
					
					IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_BOAT_ACCESS)
						SET_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iVehicleAccess, STAT_CARD_BOAT_UNLOCK)
					ELSE
						CLEAR_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iVehicleAccess, STAT_CARD_BOAT_UNLOCK)
					ENDIF
					
					IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_DOES_PLAYER_HAVE_VEH_ACCESS)
						SET_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iVehicleAccess, STAT_CARD_PERSONAL_CAR_UNLOCK)
					ELSE
						CLEAR_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iVehicleAccess, STAT_CARD_PERSONAL_CAR_UNLOCK)
					ENDIF
					
					// Update our BD data to include control type
					GlobalplayerBD_FM[iPlayerID].sClientCoronaData.iControlType = GET_TRANSITION_SESSION_CONTROL_TYPE()
					
					GlobalplayerBD_FM[iPlayerID].scoreData.iMissionsCreated = GET_MP_INT_PLAYER_STAT(MPPLY_MISSIONS_CREATED)
					GlobalplayerBD_FM[iPlayerID].scoreData.fDropOutRate 	= GET_MP_FLOAT_PLAYER_STAT(MPPLY_DROPOUTRATE)
					GlobalplayerBD_FM[iPlayerID].scoreData.iCrewXp			= GET_PLAYER_FM_CREW_XP(PLAYER_ID())
					
					TEXT_LABEL_31 tlCrewRankTitle
					tlCrewRankTitle = GET_RANK_TEXTLABEL(GlobalplayerBD_FM[iPlayerID].scoreData.iCrewXp, GET_PLAYER_GLOBAL_TEAM(PLAYER_ID()), TRUE, MP_SETTING_TITLE_CREW)
					IF NOT ARE_STRINGS_EQUAL(tlCrewRankTitle, GET_FILENAME_FOR_AUDIO_CONVERSATION("HUD_NOCREWTIT"))
						GlobalplayerBD_FM[iPlayerID].scoreData.tlCrewRankTitle = tlCrewRankTitle
					ELSE
						GlobalplayerBD_FM[iPlayerID].scoreData.tlCrewRankTitle = ""
					ENDIF
					
					// Update my player mood
					GlobalplayerBD_FM[iPlayerID].iPlayerMood = GET_MOOD_TYPE_FOR_MISSION_TYPE(g_FMMC_STRUCT.iMissionType)
					
					UPDATE_CHEAT_BITSET()
					UPDATE_LOCAL_PLAYER_AUTOMUTE()
					UPDATE_LOCAL_PLAYER_AUTOMUTE_BADSPORT()
					
					
					GlobalplayerBD[iPlayerID].IsOnlineTVRockstar = HAS_ENTERED_GAME_AS_PRIVATE_SCTV(PLAYER_ID())
					
					GlobalplayerBD_FM[iPlayerID].scoreData.bCanSpectate = GET_MP_BOOL_PLAYER_STAT(MPPLY_CAN_SPECTATE)
					
					GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iFormationtime0				= GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME0)
					GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iFormationtime1				= GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME1)
					GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iFormationtime2				= GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME2)
					GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iFormationtime3				= GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME3)
					SET_PLAYERS_FAVOURITE_PLAYLIST(GET_LOCAL_PLAYERS_FAVOURITE_JUKEBOX_PLAYLIST_ID())
//					GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iXXX_Four					 	= GET_MP_INT_CHARACTER_STAT(MP_STAT_XXX_Four)
					GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iMembersmarkedfordeath		= GET_MP_INT_CHARACTER_STAT(MP_STAT_MEMBERSMARKEDFORDEATH)
					
					GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iMckills		 				= GET_MP_INT_CHARACTER_STAT(MP_STAT_MCKILLS)
					GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iMcdeaths						= GET_MP_INT_CHARACTER_STAT(MP_STAT_MCDEATHS)
					GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iRivalpresidentkills			= GET_MP_INT_CHARACTER_STAT(MP_STAT_RIVALPRESIDENTKILLS)
					GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iRivalceoandvipkills	 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_RIVALCEOANDVIPKILLS)
					GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iMeleekills			 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_MELEEKILLS)
					
					GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iClubhousecontractscomplete	= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUBHOUSECONTRACTSCOMPLETE)
					GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iClubhousecontractearnings	= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUBHOUSECONTRACTEARNINGS)
					GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iClubworkcompleted			= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUBWORKCOMPLETED)
					GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iClubchallengescompleted		= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUBCHALLENGESCOMPLETED)
					GlobalPlayerBD_FM_3[iPlayerID].sClubhouseMissionWallStats.iMemberchallengescompleted	= GET_MP_INT_CHARACTER_STAT(MP_STAT_MEMBERCHALLENGESCOMPLETED)
					
					g_bClubhouseMissionWallUpdate = TRUE
					
					MPGlobals.g_bRefresh_BD_Stat_Data = FALSE
				
					
					
				ENDIF
				
			ENDIF
		ENDIF
	
	ENDIF
	
ENDPROC 


PROC REMOVE_CARWASH_BLIPS()
	IF DOES_BLIP_EXIST(carwashBlips[0])
		REMOVE_BLIP(carwashBlips[0])
		NET_PRINT("REMOVE_CARWASH_BLIPS: removing blip for strawberry") NET_NL()
	ENDIF
	IF DOES_BLIP_EXIST(carwashBlips[1])
		REMOVE_BLIP(carwashBlips[1])
		NET_PRINT("REMOVE_CARWASH_BLIPS: removing blip for ") NET_NL()
	ENDIF
ENDPROC
 
PROC MAINTAIN_CARWASH_BLIPS()
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
	OR HIDE_BLIP_BECAUSE_PLAYER_IS_STARTING_RACE()
	OR NOT HAS_PLAYER_COMPLETED_RACE_AND_DM_TUTORIAL(PLAYER_ID())
	OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
	OR SHOULD_HIDE_MISC_BLIP(ciPI_TYPE_M3_HIDE_MISC_ALL)
	OR SHOULD_HIDE_ALL_MISC_BLIPS_FOR_FM_EVENT()
	OR SHOULD_HIDE_MISC_BLIP(ciPI_TYPE_M3_HIDE_MISC_CAR_WASH)
		REMOVE_CARWASH_BLIPS()
	ELSE
		IF NOT DOES_BLIP_EXIST(carwashBlips[0])
			carwashBlips[0] = CREATE_BLIP_FOR_COORD(<<50.9, -1392.7, 28.9>>)
			SET_BLIP_SPRITE(carwashBlips[0], RADAR_TRACE_CAR_WASH)
			SET_BLIP_NAME_FROM_TEXT_FILE(carwashBlips[0], "MP_CARWASH")
			SET_BLIP_AS_SHORT_RANGE(carwashBlips[0],TRUE)
			SET_BLIP_HIGH_DETAIL(carwashBlips[0], FALSE)
			NET_PRINT("MAINTAIN_CARWASH_BLIPS: add blip for strawberry") NET_NL()
		ENDIF

		IF NOT DOES_BLIP_EXIST(carwashBlips[1])
			carwashBlips[1] = CREATE_BLIP_FOR_COORD(<<-699.7, -922.9, 18.5>>)
			SET_BLIP_SPRITE(carwashBlips[1], RADAR_TRACE_CAR_WASH)
			SET_BLIP_NAME_FROM_TEXT_FILE(carwashBlips[1], "MP_CARWASH")
			SET_BLIP_AS_SHORT_RANGE(carwashBlips[1],TRUE)
			SET_BLIP_HIGH_DETAIL(carwashBlips[1], FALSE)
			NET_PRINT("MAINTAIN_CARWASH_BLIPS: add blip for strawberry") NET_NL()
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_ACTIVITY_BLIPS()
	IF DOES_BLIP_EXIST(activityBlips.cinemaBlips[0])
		REMOVE_BLIP(activityBlips.cinemaBlips[0])
		NET_PRINT("REMOVE_ACTIVITY_BLIPS: removing blip for downtown cinema") NET_NL()
	ENDIF
	IF DOES_BLIP_EXIST(activityBlips.cinemaBlips[1])
		REMOVE_BLIP(activityBlips.cinemaBlips[1])
		NET_PRINT("REMOVE_ACTIVITY_BLIPS: removing blip for vinewood cinema 1") NET_NL()
	ENDIF
	IF DOES_BLIP_EXIST(activityBlips.cinemaBlips[2])
		REMOVE_BLIP(activityBlips.cinemaBlips[2])
		NET_PRINT("REMOVE_ACTIVITY_BLIPS: removing blip for morning wood") NET_NL()
	ENDIF
	IF DOES_BLIP_EXIST(activityBlips.fairgroundBlip[0])
		REMOVE_BLIP(activityBlips.fairgroundBlip[0])
		NET_PRINT("REMOVE_ACTIVITY_BLIPS: removing blip for Fairground - 0") NET_NL()
	ENDIF
	IF DOES_BLIP_EXIST(activityBlips.fairgroundBlip[1])
		REMOVE_BLIP(activityBlips.fairgroundBlip[1])
		NET_PRINT("REMOVE_ACTIVITY_BLIPS: removing blip for Fairground - 1") NET_NL()
	ENDIF
ENDPROC

PROC MAINTAIN_ACTIVITIES_BLIPS()
	
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
	OR HIDE_BLIP_BECAUSE_PLAYER_IS_STARTING_RACE()
	OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
	OR SHOULD_HIDE_ALL_MISC_BLIPS_FOR_FM_EVENT()
		REMOVE_ACTIVITY_BLIPS()
	ELSE
		//INT iHourOfDay = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())
		IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CINEMA)
		AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(),MPAM_TYPE_CINEMA)
		AND NOT SHOULD_HIDE_MISC_BLIP(ciPI_TYPE_M3_HIDE_MISC_ALL)
		AND NOT SHOULD_HIDE_ALL_MISC_BLIPS_FOR_FM_EVENT()
		AND NOT SHOULD_HIDE_MISC_BLIP(ciPI_TYPE_M3_HIDE_MISC_MOVIES)
			//downtown 
			IF NOT DOES_BLIP_EXIST(activityBlips.cinemaBlips[0])
				activityBlips.cinemaBlips[0] = CREATE_BLIP_FOR_COORD(<<394.368, -711.8287, 28.2877>>)
				SET_BLIP_SPRITE(activityBlips.cinemaBlips[0], RADAR_TRACE_CINEMA)
				SET_BLIP_NAME_FROM_TEXT_FILE(activityBlips.cinemaBlips[0],"MP_ACT_CIN")
				SET_BLIP_AS_SHORT_RANGE(activityBlips.cinemaBlips[0],TRUE)
				SET_BLIP_HIGH_DETAIL(activityBlips.cinemaBlips[0], FALSE)
				NET_PRINT("MAINTAIN_ACTIVITIES_BLIPS: adding blip for downtown cinema") NET_NL()
			ENDIF
			IF NOT DOES_BLIP_EXIST(activityBlips.cinemaBlips[1])
				activityBlips.cinemaBlips[1] = CREATE_BLIP_FOR_COORD(<<283.0143, 200.3528, 103.8434>>)
				SET_BLIP_SPRITE(activityBlips.cinemaBlips[1], RADAR_TRACE_CINEMA)
				SET_BLIP_NAME_FROM_TEXT_FILE(activityBlips.cinemaBlips[1],"MP_ACT_CIN")
				SET_BLIP_AS_SHORT_RANGE(activityBlips.cinemaBlips[1],TRUE)
				SET_BLIP_HIGH_DETAIL(activityBlips.cinemaBlips[1], FALSE)
				NET_PRINT("MAINTAIN_ACTIVITIES_BLIPS: adding blip for vinewood cinema2") NET_NL()
			ENDIF
			//morning wood
			IF NOT DOES_BLIP_EXIST(activityBlips.cinemaBlips[2])
				activityBlips.cinemaBlips[2] = CREATE_BLIP_FOR_COORD(<<-1422.673, -214.3225, 45.9932>>)
				SET_BLIP_SPRITE(activityBlips.cinemaBlips[2], RADAR_TRACE_CINEMA)
				SET_BLIP_NAME_FROM_TEXT_FILE(activityBlips.cinemaBlips[2],"MP_ACT_CIN")
				SET_BLIP_AS_SHORT_RANGE(activityBlips.cinemaBlips[2],TRUE)
				SET_BLIP_HIGH_DETAIL(activityBlips.cinemaBlips[2], FALSE)
				NET_PRINT("MAINTAIN_ACTIVITIES_BLIPS: adding blip for morning wood cinema") NET_NL()
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(activityBlips.cinemaBlips[0])
				REMOVE_BLIP(activityBlips.cinemaBlips[0])
				NET_PRINT("MAINTAIN_ACTIVITIES_BLIPS: removing blip for downtown cinema") NET_NL()
			ENDIF
			IF DOES_BLIP_EXIST(activityBlips.cinemaBlips[1])
				REMOVE_BLIP(activityBlips.cinemaBlips[1])
				NET_PRINT("MAINTAIN_ACTIVITIES_BLIPS: removing blip for vinewood cinema 1") NET_NL()
			ENDIF
			IF DOES_BLIP_EXIST(activityBlips.cinemaBlips[2])
				REMOVE_BLIP(activityBlips.cinemaBlips[2])
				NET_PRINT("MAINTAIN_ACTIVITIES_BLIPS: removing blip for morning wood") NET_NL()
			ENDIF
		ENDIF
		
		//Fairground Blips
		IF g_sMPTunables.bDisableFerrisWheel = FALSE
		AND NOT SHOULD_HIDE_MISC_BLIP(ciPI_TYPE_M3_HIDE_MISC_ALL)
		AND NOT SHOULD_HIDE_ALL_MISC_BLIPS_FOR_FM_EVENT()
		AND NOT SHOULD_HIDE_MISC_BLIP(ciPI_TYPE_M3_HIDE_MISC_FAIRGROUND)
			IF NOT DOES_BLIP_EXIST(activityBlips.fairgroundBlip[0])
				activityBlips.fairgroundBlip[0] = CREATE_BLIP_FOR_COORD(<<-1664.4, -1127.0, 12.0>>)
				SET_BLIP_SPRITE(activityBlips.fairgroundBlip[0], RADAR_TRACE_FAIRGROUND)
				SET_BLIP_NAME_FROM_TEXT_FILE(activityBlips.fairgroundBlip[0],"B_RIDE")
				SET_BLIP_AS_SHORT_RANGE(activityBlips.fairgroundBlip[0],TRUE)
				SET_BLIP_HIGH_DETAIL(activityBlips.fairgroundBlip[0], FALSE)
				NET_PRINT("MAINTAIN_ACTIVITIES_BLIPS: adding blip for Fairground - 0") NET_NL()
			ENDIF
		ENDIF
		IF NOT SHOULD_HIDE_MISC_BLIP(ciPI_TYPE_M3_HIDE_MISC_ALL)
		AND NOT SHOULD_HIDE_ALL_MISC_BLIPS_FOR_FM_EVENT()
		AND NOT SHOULD_HIDE_MISC_BLIP(ciPI_TYPE_M3_HIDE_MISC_FAIRGROUND)
			IF NOT DOES_BLIP_EXIST(activityBlips.fairgroundBlip[1])
				activityBlips.fairgroundBlip[1] = CREATE_BLIP_FOR_COORD(<<-1651.5, -1131.5, 18.3>>)
				SET_BLIP_SPRITE(activityBlips.fairgroundBlip[1], RADAR_TRACE_FAIRGROUND)
				SET_BLIP_NAME_FROM_TEXT_FILE(activityBlips.fairgroundBlip[1],"B_RIDE")
				SET_BLIP_AS_SHORT_RANGE(activityBlips.fairgroundBlip[1],TRUE)
				SET_BLIP_HIGH_DETAIL(activityBlips.fairgroundBlip[1], FALSE)
				NET_PRINT("MAINTAIN_ACTIVITIES_BLIPS: adding blip for Fairground - 1") NET_NL()
			ENDIF
		ENDIF
		
		IF SHOULD_HIDE_MISC_BLIP(ciPI_TYPE_M3_HIDE_MISC_ALL)
		OR SHOULD_HIDE_ALL_MISC_BLIPS_FOR_FM_EVENT()
		OR SHOULD_HIDE_MISC_BLIP(ciPI_TYPE_M3_HIDE_MISC_FAIRGROUND)
			IF DOES_BLIP_EXIST(activityBlips.fairgroundBlip[0])
				REMOVE_BLIP(activityBlips.fairgroundBlip[0])
				NET_PRINT("MAINTAIN_ACTIVITIES_BLIPS: removing blip for Fairground - 0") NET_NL()
			ENDIF
			IF DOES_BLIP_EXIST(activityBlips.fairgroundBlip[1])
				REMOVE_BLIP(activityBlips.fairgroundBlip[1])
				NET_PRINT("MAINTAIN_ACTIVITIES_BLIPS: removing blip for Fairground - 1") NET_NL()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_BOUNTY_UNLOCKED_FLAG()
	INT iStatInt//,iInitHelpBS
	IF IS_BOUNTY_UNLOCKED_FOR_LOCAL_PLAYER()
		IF NOT unlockedBountyContact
			IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet, BI_FM_NMH_BOUNTY_HELP) // From MAINTAIN_BOUNTY_UNLOCK_HELP_TEXT()
			#IF IS_DEBUG_BUILD OR g_ShouldShiftingTutorialsBeSkipped #ENDIF
				iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MISS_HELP_TEXT)
				IF NOT IS_BIT_SET(iStatInt, biNMH_BountyUnlockHelp)
					ADD_CONTACT_TO_PHONEBOOK (CHAR_LESTER, MULTIPLAYER_BOOK)
				ELSE
					ADD_CONTACT_TO_PHONEBOOK(CHAR_LESTER, MULTIPLAYER_BOOK, FALSE)
				ENDIF
				unlockedBountyContact = TRUE
			ENDIF
		ENDIF
		IF NOT GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bUnlockedBounty
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bUnlockedBounty = TRUE
			PRINTLN("MAINTAIN_BOUNTY_UNLOCKED_FLAG: GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bUnlockedBounty set to true ")
		ENDIF

	ELSE
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bUnlockedBounty = FALSE
		IF unlockedBountyContact
			REMOVE_CONTACT_FROM_INDIVIDUAL_PHONEBOOK(CHAR_LESTER, MULTIPLAYER_BOOK)
			unlockedBountyContact = FALSE
			iStatInt = GET_MP_INT_PLAYER_STAT(MPPLY_FM_NONMISS_HELP)
			IF IS_BIT_SET(iStatInt, FM_NONMISS_HELP_BOUNTY)
				CLEAR_BIT(iStatInt, FM_NONMISS_HELP_BOUNTY)
				SET_MP_INT_PLAYER_STAT(MPPLY_FM_NONMISS_HELP,iStatInt)
			ENDIF
		ENDIF
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bUnlockedBounty
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bUnlockedBounty = FALSE
			PRINTLN("MAINTAIN_BOUNTY_UNLOCKED_FLAG: GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bUnlockedBounty set to false ")
		ENDIF

	ENDIF
ENDPROC





//Returns TRUE if we can print the message on the screen
FUNC BOOL CAN_PRINT_ROCKSTAR_FEED_MSG()
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
	OR IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	OR IS_SCREEN_FADED_OUT()
	OR IS_CUSTOM_MENU_ON_SCREEN()
	OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
	OR IS_PAUSE_MENU_ACTIVE()
	OR IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
	OR NOT IS_SKYSWOOP_AT_GROUND()	
	OR SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
	OR IS_TRANSITION_SESSION_LAUNCHING()
	OR IS_PLAYER_IN_CORONA()
	OR NETWORK_IS_IN_MP_CUTSCENE()
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC


SCRIPT_TIMER gbCashMessageTimer
PROC MAINTAIN_GB_ENOUGH_CASH_MESSAGE()
	INT iStatInt
	IF NETWORK_IS_ACTIVITY_SESSION()
		RESET_NET_TIMER(gbCashMessageTimer)	
		EXIT
	ENDIF
	
	IF g_bGotBossMoneyHelp
		EXIT
	ENDIF
	
	IF NOT HAS_PLAYER_COMPLETED_RACE_AND_DM_TUTORIAL(PLAYER_ID()) 
	AND NOT g_SkipFmTutorials
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(gbCashMessageTimer)
		iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_GB_SYSTEM_HELP_TEXT)
		IF NOT IS_BIT_SET(iStatInt, biFmGb_Help_HAVE_MILLION_TEXT)
		//OR NOT IS_BIT_SET(iStatInt, biFmGb_Help_HAVE_MILLION_HELP_0)
		//OR NOT IS_BIT_SET(iStatInt, biFmGb_Help_HAVE_MILLION_HELP_1)
			START_NET_TIMER(gbCashMessageTimer)
			CPRINTLN(DEBUG_NET_MAGNATE, "[MAINTAIN_GB_ENOUGH_CASH_MESSAGE]  START_NET_TIMER")
		ELSE
			CPRINTLN(DEBUG_NET_MAGNATE, "[MAINTAIN_GB_ENOUGH_CASH_MESSAGE]  All help shown. g_bGotBossMoneyHelp = TRUE")
			g_bGotBossMoneyHelp = TRUE
		ENDIF
	ELIF HAS_NET_TIMER_EXPIRED(gbCashMessageTimer, 1080000)
		IF CAN_PRINT_ROCKSTAR_FEED_MSG()
			IF NOT FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
			AND NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
				iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_GB_SYSTEM_HELP_TEXT)
				IF !GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
				AND !DOES_PLAYER_HAVE_BOSS_UUID()
					IF NOT IS_BIT_SET(iStatInt, biFmGb_Help_HAVE_MILLION_TEXT)
						IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(	CHAR_MP_RAY_LAVOY,
																"GB_1M_TXT",
																TXTMSG_LOCKED, 
																TXTMSG_NOT_CRITICAL, 
																TXTMSG_AUTO_UNLOCK_AFTER_READ, 
																NO_REPLY_REQUIRED) 
							SET_BIT(iStatInt, biFmGb_Help_HAVE_MILLION_TEXT)
							SET_MP_INT_CHARACTER_STAT(MP_STAT_GB_SYSTEM_HELP_TEXT, iStatInt) 
							PRINT_HELP("GB_1M_HLP")
							CPRINTLN(DEBUG_NET_MAGNATE, "[MAINTAIN_GB_ENOUGH_CASH_MESSAGE]  biFmGb_Help_HAVE_MILLION_TEXT SET.")
							RESET_NET_TIMER(gbCashMessageTimer)
							g_bGotBossMoneyHelp = TRUE
						ENDIF
					ELSE
						PRINT_HELP("GB_1M_HLP")
						IF NOT IS_BIT_SET(iStatInt, biFmGb_Help_HAVE_MILLION_HELP_0)
							SET_BIT(iStatInt, biFmGb_Help_HAVE_MILLION_HELP_0)
							CPRINTLN(DEBUG_NET_MAGNATE, "[MAINTAIN_GB_ENOUGH_CASH_MESSAGE]  biFmGb_Help_HAVE_MILLION_HELP_0 SET.")
						ELSE
							SET_BIT(iStatInt, biFmGb_Help_HAVE_MILLION_HELP_1)
							CPRINTLN(DEBUG_NET_MAGNATE, "[MAINTAIN_GB_ENOUGH_CASH_MESSAGE]  biFmGb_Help_HAVE_MILLION_HELP_1 SET.")
						ENDIF
						SET_MP_INT_CHARACTER_STAT(MP_STAT_GB_SYSTEM_HELP_TEXT, iStatInt) 
						RESET_NET_TIMER(gbCashMessageTimer)
						g_bGotBossMoneyHelp = TRUE
					ENDIF
				ELSE
					SET_BIT(iStatInt, biFmGb_Help_HAVE_MILLION_TEXT)
					SET_BIT(iStatInt, biFmGb_Help_HAVE_MILLION_HELP_0)
					SET_BIT(iStatInt, biFmGb_Help_HAVE_MILLION_HELP_1)
					SET_MP_INT_CHARACTER_STAT(MP_STAT_GB_SYSTEM_HELP_TEXT, iStatInt) 
					g_bGotBossMoneyHelp = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_IMPORT_EXPORT_LAUNCHING()
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
//		IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet9, BI_FM_NMH9_DO_SAVE_CLOTH_HLP)
			IF NOT IS_PLAYER_IN_CORONA()
				IF NOT MPGlobalsAmbience.bInitFmImportExportLaunch
					MP_MISSION_DATA amImpExpToLaunch
					
					IF MPGlobalsAmbience.time_LaunchImportExport = NULL
						IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_IMP_EXP",MPGlobalsAmbience.iAmFmImportExportLastInstance,TRUE)
							IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
								IF HAS_FM_INTRO_MISSION_BEEN_DONE()
									MPGlobalsAmbience.time_LaunchImportExport = GET_NETWORK_TIME()
									#IF IS_DEBUG_BUILD NET_NL() NET_PRINT("[dsw] Initialised MPGlobalsAmbience.time_LaunchImportExport") NET_NL() #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_IMPORT_EXPORT)
							IF NOT HAS_NET_TIMER_STARTED(timeLaunchImport)
								START_NET_TIMER(timeLaunchImport)
								#IF IS_DEBUG_BUILD PRINTLN("[dsw] Started timeLaunchImport") #ENDIF
							ELSE
								IF HAS_NET_TIMER_EXPIRED(timeLaunchImport, 2000)
								OR IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_LAUNCH_IMPEXP_NOW) // Set when first unlocked
								#IF IS_DEBUG_BUILD OR bWdLaunchImportExportNow #ENDIF
									IF NETWORK_IS_GAME_IN_PROGRESS()
										IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_IMP_EXP",MPGlobalsAmbience.iAmFmImportExportLastInstance,TRUE)
											IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
												#IF IS_DEBUG_BUILD
												IF NOT g_bDemo
												#ENDIF
													amImpExpToLaunch.mdID.idMission = eAM_FM_IMP_EXP
													amImpExpToLaunch.iInstanceId = -1
													
													IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(amImpExpToLaunch)
														
														MPGlobalsAmbience.iAmFmImportExportLastInstance = amImpExpToLaunch.iInstanceId 
														MPGlobalsAmbience.bInitFmImportExportLaunch = TRUE
														#IF IS_DEBUG_BUILD 
															PRINTLN("[dsw] Launched Import / Export") 
															IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_LAUNCH_IMPEXP_NOW)
																PRINTLN("[dsw] Import/Export launch - BI_FM_LAUNCH_IMPEXP_NOW is set ")
															ENDIF
														#ENDIF
													ENDIF
												#IF IS_DEBUG_BUILD
												ENDIF
												#ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
						
				ENDIF
			ENDIF
//		ELSE
//			PRINTLN("[dsw] Import/Export launch - waiting for BI_FM_NMH9_DO_SAVE_CLOTH_HLP ")
//		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_TAXI_LAUNCHING()
	//IF NOT MPGlobalsAmbience.bInitMpTaxiLaunch
	//	MP_MISSION_DATA amTaxiLaunch
		
		//IF MPGlobalsAmbience.time_LaunchTaxi = NULL
		//	IF NOT NETWORK_IS_SCRIPT_ACTIVE("taxiLauncher", NATIVE_TO_INT(PLAYER_ID()), TRUE)
				//IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
				//	IF HAS_FM_INTRO_MISSION_BEEN_DONE()
		//				MPGlobalsAmbience.time_LaunchTaxi = GET_NETWORK_TIME()
		//				#IF IS_DEBUG_BUILD NET_NL() NET_PRINT(" -- TAXI - Initialised MPGlobalsAmbience.time_LaunchTaxi") NET_NL() #ENDIF
				//	ENDIF
				//ENDIF
		//	ENDIF
		//ELSE
			//IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobalsAmbience.time_LaunchTaxi) > 180000)
			
			//** SWAPPED BACK OVER DUE TO BUG 1587204 - UNSURE OF ORIGINAL REASON FOR CHANGING THESE TWO OVER IN DEV v553
			//IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_TAXI")) <= 0
			//url:bugstar:2197546 - Please kill the taxi script when on a heist
			IF NOT NETWORK_IS_ACTIVITY_SESSION()
				IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_TAXI", -1, TRUE)
					
					MP_MISSION_DATA amTaxiLaunch
					amTaxiLaunch.mdID.idMission = eAM_TAXI
					amTaxiLaunch.iInstanceId = DEFAULT_MISSION_INSTANCE
					
					IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
						IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(amTaxiLaunch)
							#IF IS_DEBUG_BUILD NET_NL() NET_PRINT(" -- TAXI - LAUNCHED SCRIPT AM_TAXI.sc - INSTANCE = ") NET_PRINT_INT(amTaxiLaunch.iInstanceId) NET_NL() #ENDIF
							//MPGlobalsAmbience.bInitMpTaxiLaunch = TRUE
						ENDIF
					ELSE
						IF IS_BIT_SET(MPGlobalsAmbience.iTaxiBitSet, biG_PlayerLeftTaxiInIntro)
						AND IS_LOCAL_PLAYER_IN_TUTORIAL_SESSION_FOR_INTRO()
							amTaxiLaunch.iInstanceId = NETWORK_GET_PLAYER_TUTORIAL_SESSION_INSTANCE(PLAYER_ID())
							IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(amTaxiLaunch)
								#IF IS_DEBUG_BUILD NET_NL() NET_PRINT(" -- TAXI - LAUNCHED SCRIPT AM_TAXI.sc - INSTANCE = ") NET_PRINT_INT(amTaxiLaunch.iInstanceId) NET_NL() #ENDIF
								//MPGlobalsAmbience.bInitMpTaxiLaunch = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			
			///ENDIF
		//ENDIF
		
	//ENDIF
ENDPROC


PROC MAINTAIN_FM_maintain_cloud_header_data_LAUNCHING()
	IF g_sRockstarUgcGetStruct.bHeistsKilledNewLoader
		IF g_sRockstarUgcGetStruct.bUseNewLoader = FALSE
			IF NOT NETWORK_IS_ACTIVITY_SESSION()
			AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
			AND NOT Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
			AND NOT Is_Player_Currently_On_MP_Heist(PLAYER_ID())
				PRINTLN("MAINTAIN_FM_maintain_cloud_header_data_LAUNCHING = FALSE")
				REQUEST_SCRIPT("FM_maintain_cloud_header_data")
				g_sRockstarUgcGetStruct.bSkipInitialRef = TRUE
				IF HAS_SCRIPT_LOADED("FM_maintain_cloud_header_data")
					START_NEW_SCRIPT("FM_maintain_cloud_header_data", DEFAULT_STACK_SIZE)
					PRINTLN("MAINTAIN_FM_maintain_cloud_header_data_LAUNCHING - START_NEW_SCRIPT - FM_maintain_cloud_header_data")
					g_sRockstarUgcGetStruct.bUseNewLoader = TRUE
					PRINTLN("MAINTAIN_FM_maintain_cloud_header_data_LAUNCHING - g_sRockstarUgcGetStruct.bHeistsKilledNewLoader = FALSE")
					g_sRockstarUgcGetStruct.bHeistsKilledNewLoader = FALSE
					CLEAR_CLOUD_REFRESH_SCRIPT_IS_TO_DIE()
					SET_UGC_CONTENT_LOADED_IN_SP(TRUE)
					SET_SCRIPT_AS_NO_LONGER_NEEDED("FM_maintain_cloud_header_data")
				ENDIF
			ENDIF
		ELSE
			g_sRockstarUgcGetStruct.bHeistsKilledNewLoader = FALSE
			PRINTLN("MAINTAIN_FM_maintain_cloud_header_data_LAUNCHING - g_sRockstarUgcGetStruct.bHeistsKilledNewLoader = FALSE 2")
		ENDIF
	ENDIF
ENDPROC
BOOL bnet_tunable_checkScriptLaunched
PROC MAINTAIN_FM_net_tunable_check_LAUNCHING()
	IF g_bRunTunableValidationChecks
	AND NOT bnet_tunable_checkScriptLaunched
		PRINTLN("MAINTAIN_TUNEABLES - MAINTAIN_FM_net_tunable_check_LAUNCHING")
		REQUEST_SCRIPT("net_tunable_check")
		IF HAS_SCRIPT_LOADED("net_tunable_check")
			START_NEW_SCRIPT("net_tunable_check", DEFAULT_STACK_SIZE)
			SET_SCRIPT_AS_NO_LONGER_NEEDED("net_tunable_check")
			PRINTLN("MAINTAIN_TUNEABLES - MAINTAIN_FM_net_tunable_check_LAUNCHING - START_NEW_SCRIPT - net_tunable_check")
			bnet_tunable_checkScriptLaunched = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_VEHICLE_SPAWN()
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM

		IF (CAN_PLAYER_USE_REQUEST_DURING_CURRENT_AMBIENT(REQUEST_LESTER_LOCATE_BOAT)	//If we can do something with vehicle spawn
			AND CAN_PLAYER_USE_REQUEST_DURING_CURRENT_JOB(REQUEST_LESTER_LOCATE_BOAT))
		OR (CAN_PLAYER_USE_REQUEST_DURING_CURRENT_AMBIENT(REQUEST_PEGASUS)
			AND CAN_PLAYER_USE_REQUEST_DURING_CURRENT_JOB(REQUEST_PEGASUS))
		
			IF HAS_PLAYER_COMPLETED_RACE_AND_DM_TUTORIAL(PLAYER_ID())
				IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
					IF NOT IS_LOCAL_PLAYER_IN_TUTORIAL_SESSION_FOR_INTRO()
						IF NOT IS_PLAYER_SCTV(PLAYER_ID())
							IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
								IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
									IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
										IF IS_ENTITY_VISIBLE(PLAYER_PED_ID()) AND NOT IS_PLAYER_IN_CORONA()
											IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
												IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_VEHICLE_SPAWN")) <= 0
												
													IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType <> FMMC_TYPE_MISSION
													OR g_bMissionReadyForAM_VEHICLE_SPAWN
													
														MP_MISSION_DATA amVehicleSpawn
														
														amVehicleSpawn.mdID.idMission = eAM_VEHICLE_SPAWN
														amVehicleSpawn.iInstanceId = DEFAULT_MISSION_INSTANCE
														IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(amVehicleSpawn)					//Run the vehicle spawn script
															#IF IS_DEBUG_BUILD
															CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === LAUNCHED SCRIPT AM_VEHICLE_SPAWN.sc - INSTANCE = ", 
																						amVehicleSpawn.iInstanceId)
															#ENDIF
														ENDIF
														
													ENDIF
													
												ENDIF
//											ELSE
//												#IF IS_DEBUG_BUILD
//													PRINTLN("MAINTAIN_VEHICLE_SPAWN - not launching because player switch is in progress.")
//												#ENDIF
											ENDIF
//										ELSE
//											#IF IS_DEBUG_BUILD
//												PRINTLN("MAINTAIN_VEHICLE_SPAWN - not launching because player is not visible.")
//											#ENDIF
										ENDIF
									ENDIF
								ENDIF
//							ELSE
//								#IF IS_DEBUG_BUILD
//									PRINTLN("MAINTAIN_VEHICLE_SPAWN - not launching because player joined as spectator.")
//								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("MAINTAIN_VEHICLE_SPAWN - not launching because player cant do anything with vehicle spawn.")
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN("MAINTAIN_VEHICLE_SPAWN - not launching because game mode is not freemode snd it's not SVM.")
		#ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_NOW_ACCEPTABLE_FOR_GANG_CALL()
	RETURN TRUE
ENDFUNC

PROC ATTEMPT_TO_LAUNCH_GANG_CALL(INT iInstance)
	MP_MISSION_DATA amGangCall
	PLAYER_INDEX playerID
	PED_INDEX pedID
	
	IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(GlobalServerBD_FM.iGangCallCurrentTargetPlayer[iInstance]))
		playerID = INT_TO_PLAYERINDEX(GlobalServerBD_FM.iGangCallCurrentTargetPlayer[iInstance])
		pedId = GET_PLAYER_PED(playerID)
		IF DOES_ENTITY_EXIST(pedID)
			amGangCall.mdID.idMission = eAM_GANG_CALL
			amGangCall.iInstanceId = iInstance
			amGangCall.mdUniqueID = GlobalServerBD_FM.iGangCallCurrentOwnerPlayer[iInstance]
			amGangCall.iBitSet = GlobalServerBD_FM.iGangCallCurrentTargetPlayer[iInstance]
			IF iInstance = GANG_CALL_TYPE_SPECIAL_THIEF
			OR iInstance = GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
				amGangCall.mdPrimaryCoords = <<0,0,0>>
			ELSE
				amGangCall.mdPrimaryCoords = GET_ENTITY_COORDS(pedID, FALSE)
			ENDIF
			IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(amGangCall)					//Run this gang script
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === LAUNCHED SCRIPT AM_GANG_CALL.sc - INSTANCE = ", amGangCall.iInstanceId)
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_GANG_CALL_LAUNCHING()
	INT i
	INT iGangCallLocalPlayerID = NETWORK_PLAYER_ID_TO_INT()
	
	REPEAT GANG_CALL_TYPE_TOTAL_NUMBER i
		IF GlobalServerBD_FM.iGangCallCurrentOwnerPlayer[i] <> -1							//Someone claims ownership of gang
			IF GlobalServerBD_FM.iGangCallCurrentOwnerPlayer[i] <> iGangCallLocalPlayerID				//If it's not me
				IF IS_BIT_SET(GlobalplayerBD_FM[iGangCallLocalPlayerID].iGangCallOwnerBitset, i)			//If trying to claim ownership locally
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === CLEAR_BIT(GlobalplayerBD_FM[", iGangCallLocalPlayerID, "].iGangCallOwnerBitset, ", i, ")")
					#ENDIF
					CLEAR_BIT(GlobalplayerBD_FM[iGangCallLocalPlayerID].iGangCallOwnerBitset, i)			//Clear local claim of ownership of gang
					GlobalplayerBD_FM[iGangCallLocalPlayerID].iGangCallTargetID = -1
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD																//Check if local wants to start this gang
			IF bDebugGangCallWantOwnership[i]
				bDebugGangCallWantOwnership[i] = FALSE										//Clear that attempt
			ENDIF
			#ENDIF
				
			IF GlobalServerBD_FM.iGangCallCurrentTargetPlayer[i] <> iGangCallLocalPlayerID					//If target is not me
				IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_GANG_CALL", i, TRUE)							//If not running this gang script locally
					IF NOT IS_BIT_SET(GlobalplayerBD_FM[iGangCallLocalPlayerID].iGangCallSeenRunning, i)	//If not seen this script run locally
						IF NETWORK_IS_SCRIPT_ACTIVE("AM_GANG_CALL", i)								//If someone else is running this script
							ATTEMPT_TO_LAUNCH_GANG_CALL(i)											//Attempt to launch the script
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(GlobalplayerBD_FM[iGangCallLocalPlayerID].iGangCallSeenRunning, i)
						SET_BIT(GlobalplayerBD_FM[iGangCallLocalPlayerID].iGangCallSeenRunning, i)		//Set seen this script run locally
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === 1SET_BIT(GlobalplayerBD_FM[", iGangCallLocalPlayerID, "].iGangCallSeenRunning, ", i, ")")
						#ENDIF
					ENDIF
				ENDIF
			ELSE																					//If target is me
				IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_GANG_CALL", i, TRUE)							//If not running this gang script locally
					IF NOT IS_BIT_SET(GlobalplayerBD_FM[iGangCallLocalPlayerID].iGangCallSeenRunning, i)	//If not seen this script run locally
						IF IS_NOW_ACCEPTABLE_FOR_GANG_CALL()										//If now is acceptable
							ATTEMPT_TO_LAUNCH_GANG_CALL(i)											//Attempt to launch the script	
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(GlobalplayerBD_FM[iGangCallLocalPlayerID].iGangCallSeenRunning, i)
						SET_BIT(GlobalplayerBD_FM[iGangCallLocalPlayerID].iGangCallSeenRunning, i)		//Set seen this script run locally
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === 2SET_BIT(GlobalplayerBD_FM[", iGangCallLocalPlayerID, "].iGangCallSeenRunning, ", i, ")")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_GANG_CALL", i)								//When no-one is running this gang script
			
				IF IS_BIT_SET(GlobalplayerBD_FM[iGangCallLocalPlayerID].iGangCallSeenRunning, i)
					CLEAR_BIT(GlobalplayerBD_FM[iGangCallLocalPlayerID].iGangCallSeenRunning, i)			//Clear seeing this script run locally
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === 1CLEAR_BIT(GlobalplayerBD_FM[", iGangCallLocalPlayerID, "].iGangCallSeenRunning, ", i, ")")
					#ENDIF
				ENDIF
			
				#IF IS_DEBUG_BUILD															//Check if local wants to start this gang
					IF bDebugGangCallWantOwnership[i]
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === SET_BIT(GlobalplayerBD_FM[", iGangCallLocalPlayerID, "].iGangCallOwnerBitset, ", i, ")")
						#ENDIF
						SET_BIT(GlobalplayerBD_FM[iGangCallLocalPlayerID].iGangCallOwnerBitset, i)	//Set local claim for ownership
						
						bDebugGangCallWantOwnership[i] = FALSE								//Clear that attempt
					ENDIF
				#ENDIF
			ELSE																			//If still running last gang
				#IF IS_DEBUG_BUILD															//Check if local wants to start this gang
				IF bDebugGangCallWantOwnership[i]
					bDebugGangCallWantOwnership[i] = FALSE									//Clear that attempt
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL IS_PLAYER_IN_PHONE_BLOCKING_ACTIVITY(PLAYER_INDEX playerID)
	IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_DEATHMATCH
	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_RACE
	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_SURVIVAL
	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_IMPROMPTU_DM
	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_BASE_JUMP
	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_ARMS_SMUGGLING
	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_MG_GOLF
	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_MG_TENNIS
	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_MG_SHOOTING_RANGE
	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_MG_DARTS
	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_MG_PILOT_SCHOOL
	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_MG_ARM_WRESTLING
	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_CINEMA
	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_CAR_MOD
	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_CUSTOM_CAR_GARAGE
	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_STRIP_CLUB
	OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(playerID,MPAM_TYPE_CINEMA)
	OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(playerID,MPAM_TYPE_STRIPCLUB)
	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_GB_BOSS_DEATHMATCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_MP_BLOCK_911()
	
	IF IS_PLAYER_IN_PHONE_BLOCKING_ACTIVITY(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
		OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
			IF IS_ENTITY_IN_AIR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
		RETURN TRUE
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_PHONE_ABILITIES()

	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_MISSION //Emergency calls on missions are handled by FM_Mission_Controller
		IF SHOULD_MP_BLOCK_911()
			IF NOT ARE_EMERGENCY_CALLS_SUPPRESSED()
				SUPPRESS_EMERGENCY_CALLS()
			ENDIF
		ELSE
			IF ARE_EMERGENCY_CALLS_SUPPRESSED()
				RELEASE_SUPPRESSED_EMERGENCY_CALLS()
			ENDIF
		ENDIF
	ENDIF

	//David G 24/01/2013 B*#972822
	INT iTimerDifference, iTimerDuration
	TEXT_LABEL_15 tlTimerLabel
	BOOL bGangOffRadar
	
	bGangOffRadar = IS_PLAYER_GANG_OFF_THE_RADAR()
	
	IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bOffTheRadar
	OR IS_PLAYER_IN_OFF_THE_RADAR_INFLUENCE()
	OR bGangOffRadar
	
		iTimerDifference = GET_TIME_DIFFERENCE(GET_NETWORK_TIME() , MPGlobals.g_timeOffTheRadar)
		
		// Prevent the timer going below 0
		IF iTimerDifference < 0
			MPGlobals.g_timeOffTheRadar = GET_NETWORK_TIME()
			PRINTLN("[GB][GB][AMEC] - MAINTAIN_PHONE_ABILITIES - OTR time difference is ",iTimerDifference," updating time to now")
		ENDIF
		
		IF bGangOffRadar
			iTimerDuration = g_sMPTunables.iGB_GHOST_ORG_DURATION
			tlTimerLabel = "GB_PIM_GHOST"
		ELSE
			iTimerDuration = OFF_THE_RADAR_TIME
			tlTimerLabel = "GC_OTR_TMR"
		ENDIF
		
		// If the player leaves their gang, cancel any gang abilities.
		IF iTimerDifference >= 5000 // Give a grace period for the network data sync.
			IF NOT bGangOffRadar
				IF IS_BIT_SET(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_Gang)
					IF IS_BIT_SET(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_GangJip)
						//PRINTLN("[GB][AMEC] - MAINTAIN_PHONE_ABILITIES - Player has JIPed to Ghost Org, allow grace period.")
						IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME() , MPGlobals.g_timeOffTheRadarJipGang) >= 5000)
							PRINTLN("[GB][AMEC] - MAINTAIN_PHONE_ABILITIES - JIP player is still not in gang after 5 seconds, cancel off the radar mode.")
							GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bOffTheRadar = FALSE
							
							CLEAR_BIT(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_Gang)
							CLEAR_BIT(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_GangJip)
							
							IF IS_BIT_SET(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_Personal)
								CLEAR_BIT(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_Personal)
							ENDIF
							
							IF IS_BIT_SET(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_DoneTicker)
								CLEAR_BIT(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_DoneTicker)
							ENDIF
							
							// Wait until the next frame to re-process, prevents sending random tickers in the mean time.
							EXIT
						ENDIF
					ELSE
						PRINTLN("[GB][AMEC] - MAINTAIN_PHONE_ABILITIES - Player has left gang during Ghost Org, cancel off the radar mode.")
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bOffTheRadar = FALSE
						CLEAR_BIT(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_Gang)
						IF IS_BIT_SET(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_Personal)
							CLEAR_BIT(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_Personal)
						ENDIF
						
						// Wait until the next frame to re-process, prevents sending random tickers in the mean time.
						EXIT
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_GangJip)
					CLEAR_BIT(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_GangJip)
				ENDIF
			ENDIF
		ENDIF
		
			
		IF iTimerDifference > iTimerDuration
		OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_BOSS_DEATHMATCH
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bOffTheRadar = FALSE
			
			IF IS_BIT_SET(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_Personal)
				CLEAR_BIT(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_Personal)
			ENDIF
			
			IF IS_BIT_SET(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_Gang)
				CLEAR_BIT(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_Gang)
			ENDIF
			
			IF IS_BIT_SET(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_GangJip)
				CLEAR_BIT(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_GangJip)
			ENDIF
			
			IF IS_BIT_SET(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_DoneTicker)
				CLEAR_BIT(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_DoneTicker)
			ENDIF
			
			iOffRadarPrintTime = 10000
			//RESET_PLAYER_OVERHEAD_STRING(PLAYER_ID())			//brenda's overhead string
		ELSE
			DRAW_GENERIC_TIMER((iTimerDuration-iTimerDifference), tlTimerLabel, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_TOP)

			IF iTimerDifference >= iOffRadarPrintTime
			
				// Don't do any feed messages until the JIP flag is processed.
				// Prevents flicking from one message to another in quick succession.
				IF NOT IS_BIT_SET(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_GangJip)
					
					IF bGangOffRadar
						IF NOT IS_BIT_SET(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_DoneTicker)
							IF GB_IS_LOCAL_PLAYER_BOSS_OF_THIS_GANG(PLAYER_ID())
								PRINTLN("[GB][AMEC] - MAINTAIN_PHONE_ABILITIES - Boss: Sending GHOST ORG ticker.")
								SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
								TickerEventData.TickerEvent = TICKER_EVENT_GHOST_ORGANISATION
								TickerEventData.dataString = GB_GET_ORGANIZATION_NAME_AS_A_STRING()
								BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS(TRUE))
								iOffRadarPrintTime += 10000
								SET_BIT(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_DoneTicker)
							ENDIF
						ENDIF
					ELSE
					
						PRINTLN("[GB][AMEC] - MAINTAIN_PHONE_ABILITIES - Sending OFF THE RADAR ticker.")
						SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
						INT iCost = GET_CONTACT_REQUEST_COST(REQUEST_OFF_THE_RADAR)
						TickerEventData.TickerEvent = TICKER_EVENT_OFF_THE_RADAR
						TickerEventData.dataInt = iCost
						TickerEventData.dataInt2 = ARE_OTHER_PLAYERS_OFF_RADAR()
						BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS(FALSE))
						iOffRadarPrintTime += 10000	
							
					ENDIF
				ENDIF
					
				
			ENDIF
		ENDIF
	ENDIF
	
	IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bRevealPlayers
		iTimerDifference = GET_TIME_DIFFERENCE(GET_NETWORK_TIME() , MPGlobals.g_timeRevealPlayers)
		IF iTimerDifference > REVEAL_PLAYERS_TIME
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bRevealPlayers = FALSE
		ELSE
			DRAW_GENERIC_TIMER((REVEAL_PLAYERS_TIME-iTimerDifference), "GC_RE_TMR",DEFAULT,DEFAULT,DEFAULT,DEFAULT,HUDORDER_TOP)
		ENDIF
	ENDIF
ENDPROC




PROC MAINTAIN_SPECTATOR_TARGET_INFORMATION()

	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	
	IF iPlayer > -1
		// is screen fading out
		IF IS_SCREEN_FADING_OUT()
			IF NOT IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_FADING_OUT)
				SET_BIT(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_FADING_OUT)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === SET_BIT(GlobalplayerBD[", iPlayer, "].iSpecInfoBitset, SPEC_INFO_BS_FADING_OUT)")
				#ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_FADING_OUT)
				CLEAR_BIT(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_FADING_OUT)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === CLEAR_BIT(GlobalplayerBD[", iPlayer, "].iSpecInfoBitset, SPEC_INFO_BS_FADING_OUT)")
				#ENDIF
			ENDIF
		ENDIF
		
		// is screen faded out
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_FADED_OUT)
				SET_BIT(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_FADED_OUT)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === SET_BIT(GlobalplayerBD[", iPlayer, "].iSpecInfoBitset, SPEC_INFO_BS_FADED_OUT)")
				#ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_FADED_OUT)
				CLEAR_BIT(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_FADED_OUT)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === CLEAR_BIT(GlobalplayerBD[", iPlayer, "].iSpecInfoBitset, SPEC_INFO_BS_FADED_OUT)")
				#ENDIF
			ENDIF
		ENDIF
		
		// is spectating
		IF IS_A_SPECTATOR_CAM_RUNNING()
			IF NOT IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SPECTATOR_CAM_IS_RUNNING)
				SET_BIT(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SPECTATOR_CAM_IS_RUNNING)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === SET_BIT(GlobalplayerBD[", iPlayer, "].iSpecInfoBitset, SPEC_INFO_BS_SPECTATOR_CAM_IS_RUNNING)")
				#ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SPECTATOR_CAM_IS_RUNNING)
				CLEAR_BIT(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SPECTATOR_CAM_IS_RUNNING)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === CLEAR_BIT(GlobalplayerBD[", iPlayer, "].iSpecInfoBitset, SPEC_INFO_BS_SPECTATOR_CAM_IS_RUNNING)")
				#ENDIF
			ENDIF
		ENDIF
		
		// is skyswoop goingup
		IF GET_SKYSWOOP_STAGE() = SKYSWOOP_GOINGUP
			IF NOT IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_GOINGUP)
				SET_BIT(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_GOINGUP)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === SET_BIT(GlobalplayerBD[", iPlayer, "].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_GOINGUP)")
				#ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_GOINGUP)
				CLEAR_BIT(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_GOINGUP)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === CLEAR_BIT(GlobalplayerBD[", iPlayer, "].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_GOINGUP)")
				#ENDIF
			ENDIF
		ENDIF
		
		// is skyswoop inskystatic
		IF GET_SKYSWOOP_STAGE() = SKYSWOOP_INSKYSTATIC
			IF NOT IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_INSKYSTATIC)
				SET_BIT(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_INSKYSTATIC)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === SET_BIT(GlobalplayerBD[", iPlayer, "].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_INSKYSTATIC)")
				#ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_INSKYSTATIC)
				CLEAR_BIT(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_INSKYSTATIC)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === CLEAR_BIT(GlobalplayerBD[", iPlayer, "].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_INSKYSTATIC)")
				#ENDIF
			ENDIF
		ENDIF
		
		// is skyswoop inskymoving
		IF GET_SKYSWOOP_STAGE() = SKYSWOOP_INSKYMOVING
			IF NOT IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_INSKYMOVING)
				SET_BIT(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_INSKYMOVING)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === SET_BIT(GlobalplayerBD[", iPlayer, "].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_INSKYMOVING)")
				#ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_INSKYMOVING)
				CLEAR_BIT(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_INSKYMOVING)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === CLEAR_BIT(GlobalplayerBD[", iPlayer, "].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_INSKYMOVING)")
				#ENDIF
			ENDIF
		ENDIF
		
		// is skyswoop goingdown
		IF GET_SKYSWOOP_STAGE() = SKYSWOOP_GOINGDOWN
			IF NOT IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_GOINGDOWN)
				SET_BIT(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_GOINGDOWN)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === SET_BIT(GlobalplayerBD[", iPlayer, "].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_GOINGDOWN)")
				#ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_GOINGDOWN)
				CLEAR_BIT(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_GOINGDOWN)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === CLEAR_BIT(GlobalplayerBD[", iPlayer, "].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_GOINGDOWN)")
				#ENDIF
			ENDIF
		ENDIF
		
		// was just in property
		IF IS_BIT_SET(GlobalplayerBD_FM[iPlayer].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPEC_WAS_JUST_IN_PROPERTY)
			IF NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
				IF NOT NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING()
					IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
						IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) < 1
							IF NOT IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_FADED_OUT)
							AND NOT IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_FADING_OUT)
								CLEAR_BIT(GlobalplayerBD_FM[iPlayer].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPEC_WAS_JUST_IN_PROPERTY)
								#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === CLEAR_BIT(GlobalplayerBD_FM[iPlayer].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPEC_WAS_JUST_IN_PROPERTY)")
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// update true coords
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			IF IS_ENTITY_DEAD(PLAYER_PED_ID())
			ENDIF
			IF iUpdatePlayerTrueCoordsTimer < GET_GAME_TIMER()
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GlobalplayerBD[iPlayer].vTruePlayerCoords) > UPDATE_PLAYER_TRUE_COORDS_DIST
					GlobalplayerBD[iPlayer].vTruePlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				ENDIF
				iUpdatePlayerTrueCoordsTimer = GET_GAME_TIMER() + UPDATE_PLAYER_TRUE_COORDS_TIME
			ENDIF
		ENDIF
		
		//Broadcast if the player thinks they are SCTV or a player (used to stop SCTV players appearing on other spectator lists as they join)
		CLEAR_BIT(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_IS_PLAYER)
		CLEAR_BIT(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_IS_SCTV)
		IF IS_PLAYER_SCTV(PLAYER_ID())
			SET_BIT(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_IS_SCTV)
		ELSE
			SET_BIT(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_IS_PLAYER)
		ENDIF
		

	ENDIF

ENDPROC

PROC MAINTAIN_SHOP_FLAGS()
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		INT iPlayerID = NATIVE_TO_INT(PLAYER_ID())
		
		IF IS_BIT_SET(MPGlobalsAmbience.iTaxiBitSet, biG_PlayerUsingTaxi)
			IF NOT GlobalplayerBD[iPlayerID].bUsingTaxi
				GlobalplayerBD[iPlayerID].bUsingTaxi = TRUE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === GlobalplayerBD[iPlayerID].bUsingTaxi = TRUE")
				#ENDIF
			ENDIF
		ELSE
			IF GlobalplayerBD[iPlayerID].bUsingTaxi
				GlobalplayerBD[iPlayerID].bUsingTaxi = FALSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === GlobalplayerBD[iPlayerID].bUsingTaxi = FALSE")
				#ENDIF
			ENDIF
		ENDIF
		
		IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
			IF NOT GlobalplayerBD[iPlayerID].bInShopMenu
				GlobalplayerBD[iPlayerID].bInShopMenu = TRUE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === GlobalplayerBD[iPlayerID].bInShopMenu = TRUE")
				#ENDIF
			ENDIF
		ELSE
			IF GlobalplayerBD[iPlayerID].bInShopMenu
				GlobalplayerBD[iPlayerID].bInShopMenu = FALSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === GlobalplayerBD[iPlayerID].bInShopMenu = FALSE")
				#ENDIF
			ENDIF
		ENDIF
		
		IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) <> GlobalplayerBD[iPlayerID].interiorCurrent
			GlobalplayerBD[iPlayerID].interiorCurrent = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
			#IF IS_DEBUG_BUILD
			VECTOR vDebugCoords = GET_PLAYER_COORDS(PLAYER_ID())
			CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === New Current Interior set at ", vDebugCoords)
			#ENDIF
		ENDIF
		
		IF IS_PLAYER_IN_ANY_SHOP()
			IF GlobalplayerBD[iPlayerID].iCurrentShop = -1
			OR NOT IS_PLAYER_IN_SHOP(INT_TO_ENUM(SHOP_NAME_ENUM, GlobalplayerBD[iPlayerID].iCurrentShop))
				INT i = 0
				BOOL bShopFound = FALSE
				
				GlobalplayerBD[iPlayerID].iCurrentShop = -1 //clear before search
				
				REPEAT NUMBER_OF_SHOPS i
					IF NOT bShopFound
						IF IS_PLAYER_IN_SHOP(INT_TO_ENUM(SHOP_NAME_ENUM, i))
							GlobalplayerBD[iPlayerID].iCurrentShop = i
							bShopFound = TRUE
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === In a shop, iCurrentShop = ", GlobalplayerBD[iPlayerID].iCurrentShop)
							#ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
				#IF IS_DEBUG_BUILD
				IF NOT bShopFound
					CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === In a shop, but shop not found, iCurrentShop = -1")
				ENDIF
				#ENDIF
				
			ENDIF
		ELSE
			IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iCurrentShop <> -1
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== Freemode === NOT in a shop, iCurrentShop = -1")
				#ENDIF
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iCurrentShop = -1
			ENDIF
		ENDIF
	ENDIF
ENDPROC


INT iScriptLaunchStagger

//PURPOSE: This launches a script with args as triggered by PROCESS_JOIN_AMBIENT_SCRIPT_EVENT
PROC MAINTAIN_SCRIPT_LAUNCH() 

	SWITCH iScriptLaunchStagger
		CASE 0
			INT i
			MP_MISSION_DATA emptyData	
			REPEAT MAX_NUM_PRE_GROUP_SCRIPTS i 
				IF IS_BIT_SET(g_iPreScriptReadyToLaunch, i)	
					// Load and launch the script, this deals with any instancing.   
					IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(MPGlobalsEvents.PreMissionScriptData[i])	
						
						// Make sure we clear up.
						MPGlobalsEvents.PreMissionScriptData[i] = emptyData

						NET_PRINT_STRING_INT("MAINTAIN_SCRIPT_LAUNCH launched queue: ", i) NET_NL()
						CLEAR_BIT(g_iPreScriptReadyToLaunch, i)
					ENDIF
				ENDIF
			ENDREPEAT 		
		BREAK
		CASE 1  MAINTAIN_FM_net_tunable_check_LAUNCHING()			BREAK
		CASE 2	MAINTAIN_FM_maintain_cloud_header_data_LAUNCHING()	BREAK
		CASE 3	MAINTAIN_AM_DOORS_LAUNCHING()						BREAK
		CASE 4	MAINTAIN_IMPORT_EXPORT_LAUNCHING()					BREAK
		CASE 10	
			IF NOT GET_COMMANDLINE_PARAM_EXISTS_DBSAFE("sc_UseNewAmbienceLauncher")
			MAINTAIN_TAXI_LAUNCHING() 		
			ENDIF	
		BREAK	
		CASE 11	MAINTAIN_VEHICLE_SPAWN()							BREAK
		CASE 12	MAINTAIN_GANG_CALL_LAUNCHING()						BREAK
		CASE 13	MAINTAIN_AM_PROSTITUTE_LAUNCHING()					BREAK
		CASE 15 MAINTAIN_AM_ARMYBASE_LAUNCHING()					BREAK
		CASE 16 MAINTAIN_FREEMODE_SPECTATOR_CHAT()					BREAK
		CASE 17 MAINTAIN_PRISON_LAUNCHING()							BREAK
		CASE 18 MAINTAIN_FAIRGROUND_LAUNCHING()						BREAK
		CASE 19 MAINTAIN_AMBIENCE_LAUNCHING()						BREAK
	ENDSWITCH
	
	// NOTE IF ADDING TO THE ABOVE SWITCH, MAKE SURE YOU UPDATE THE MAX VALUE IN THE BRACKETS BELOW
	iScriptLaunchStagger++
	IF (iScriptLaunchStagger > 22)
		iScriptLaunchStagger = 0
	ENDIF

ENDPROC

FUNC BOOL HAS_ALL_SPECTATOR_HUD_LEADERBOARD_ELEMENTS_LOADED()
	REQUEST_STREAMED_TEXTURE_DICT("MPOverview")
	IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPOverview")
		IF HAVE_ALL_COMMON_LEADERBOARD_ELEMENTS_LOADED(LeaderboardPlacement)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UNLOAD_ALL_SPECTATOR_HUD_LEADERBOARD_ELEMENTS()
	UNLOAD_ALL_COMMON_LEADERBOARD_ELEMENTS(LeaderboardPlacement)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPOverview")
ENDPROC

/// PURPOSE:
///   (SERVER ONLY: Maintains the leaderboard data)
PROC SERVER_MAINTAIN_FM_LEADERBOARD_FOR_PLAYER(FM_LEADERBOARD_STRUCT& leaderBoard[], INT iPlayer)
														
	//#IF IS_DEBUG_BUILD	NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SERVER_MAINTAIN_FM_LEADERBOARD_FOR_PLAYER called ") #ENDIF
	
	INT i
	FM_LEADERBOARD_STRUCT EmptyLeaderBoardStruct
	FM_LEADERBOARD_STRUCT StoredLeaderBoardStruct
	FM_LEADERBOARD_STRUCT TempLeaderBoardStruct

	// remove this player from leader board before we re-add him.
	BOOL bPlayerRemoved = FALSE
	REPEAT NUM_NETWORK_PLAYERS i 
		IF leaderBoard[i].bInitialise 		= FALSE
			leaderBoard[i].playerID 		= INVALID_PLAYER_INDEX()
			leaderBoard[i].iParticipant 	= -1
			leaderBoard[i].bInitialise 		= TRUE
		ENDIF
	
		IF NOT bPlayerRemoved
			IF leaderBoard[i].playerID = INT_TO_PLAYERINDEX(iPlayer)
				leaderBoard[i] = EmptyLeaderBoardStruct
				bPlayerRemoved = TRUE
			ENDIF
		ELSE
			TempLeaderBoardStruct = leaderBoard[i]
			leaderBoard[i-1] = 	TempLeaderBoardStruct
			leaderBoard[i] = EmptyLeaderBoardStruct
		ENDIF
	ENDREPEAT
	
	INT iCurrentRank = 1
	// add this player to the leader board
	BOOL bPlayerInserted = FALSE
	REPEAT NUM_NETWORK_PLAYERS i 
		IF NOT (bPlayerInserted)
			IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iPlayer), FALSE, TRUE)
				PLAYER_INDEX PlayerId = INT_TO_PLAYERINDEX(iPlayer)
				IF NOT IS_PLAYER_SCTV(PlayerId)
					BOOL bGoodToInsert = TRUE
					// if there is already a someone there?
					IF (leaderBoard[i].playerID <> playerID)
														
						// check that person is still active and compare their position
						IF NATIVE_TO_INT(leaderBoard[i].playerID) <> -1
						AND IS_NET_PLAYER_OK(leaderBoard[i].playerID, FALSE, TRUE)								
							
							INT iPlayerCash
							IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
								iPlayerCash = GET_PLAYER_FM_CASH(PlayerId)
							ENDIF
							
							// Who has most cash?
							IF (leaderBoard[i].iCash > iPlayerCash) 
								iCurrentRank ++
								bGoodToInsert = FALSE
							ENDIF
						ENDIF												
					ENDIF
				
					// are we still good to insert this person into the leader board?
					IF (bGoodToInsert)
						StoredLeaderBoardStruct = leaderBoard[i]

						leaderBoard[i].playerID = PlayerId
						leaderBoard[i].iParticipant = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(PlayerId))					
						
						IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
							leaderBoard[i].iCash = GET_PLAYER_FM_CASH(PlayerId)
						ENDIF
													
						leaderBoard[i].iKills = 0//playerBDPassed[iPlayer].iKills
						leaderBoard[i].iDeaths = 0//playerBDPassed[iPlayer].iDeaths
						leaderBoard[i].iCustomInt = 0//playerBDPassed[iPlayer].iCustomInt
						leaderBoard[i].iHeadshots = 0//playerBDPassed[iPlayer].iHeadshots
						leaderBoard[i].iHighestStreak = 0//playerBDPassed[iPlayer].iHighestKillStreak
						
						leaderBoard[i].iBadgeRank = GlobalplayerBD_FM[iPlayer].scoreData.iRank
//						leaderBoard[i].iBadgeTeam = StatDataPlayerBD[NATIVE_TO_INT(PlayerId)].WhatTeamAmI
						
						// NOT CONVINCED THIS RANK BIT IS WORKING CORRECTLY ***********************
						leaderBoard[i].iRank = iCurrentRank
					
						bPlayerInserted = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			// shift everyone else in the leaderboard down
			TempLeaderBoardStruct = leaderBoard[i]
			leaderBoard[i] = StoredLeaderBoardStruct
			StoredLeaderBoardStruct = TempLeaderBoardStruct
		ENDIF
	ENDREPEAT
ENDPROC

PROC MANAGE_FREEMODE_LEADERBOARD()
	BOOL bDraw[FMMC_MAX_TEAMS]
	IF IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)
		INT iTest = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType
		IF iTest <> FMMC_TYPE_DEATHMATCH
		AND iTest <> FMMC_TYPE_RACE
		AND iTest <> FMMC_TYPE_MISSION
		AND iTest <> FMMC_TYPE_SURVIVAL
		AND iTest <> FMMC_TYPE_BASE_JUMP
			IF HAS_ALL_SPECTATOR_HUD_LEADERBOARD_ELEMENTS_LOADED()
				INT i
				INT iSlotCounter = 0
							
				REPEAT NUM_NETWORK_PLAYERS i
					DEFINE_FREEMODE_NAMES_ARRAY(tl23LeaderboardPlayerNames, i)
					IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i), FALSE)
						PLAYER_INDEX PlayerId = INT_TO_PLAYERINDEX(i)
						IF NOT IS_PLAYER_SCTV(PlayerId)
							STORE_GAMER_HANDLES(lbdVars, PlayerId, i)
						ENDIF
					ENDIF
					SERVER_MAINTAIN_FM_LEADERBOARD_FOR_PLAYER(FMLeaderboard, i)
				ENDREPEAT
				
				IF HAVE_ALL_COMMON_LEADERBOARD_ELEMENTS_LOADED(LeaderboardPlacement)
				
					// Draw the leaderboard
					RENDER_LEADERBOARD(lbdVars, LeaderboardPlacement, SUB_FREEMODE, FALSE, "HUD_LBD_FM", GET_NUM_LBD_COLUMNS(SUB_FREEMODE)) 
					
					REPEAT NUM_NETWORK_PLAYERS i
						IF NATIVE_TO_INT(FMLeaderboard[i].playerID) <> -1
							IF IS_NET_PLAYER_OK(FMLeaderboard[i].playerID, FALSE)
								
								POPULATE_COLUMN_LBD(lbdVars,
													LeaderboardPlacement,
													SUB_FREEMODE,
													LBD_VOTE_NOT_USED,
													tl23LeaderboardPlayerNames,
													GET_NUM_LBD_COLUMNS(SUB_FREEMODE),
													0.0,
													FMLeaderboard[i].iCash,
													0, 0, 0, 0, 0,
													0,0,
													iSlotCounter,
													FMLeaderboard[i].iRank,
													FMLeaderboard[i].iParticipant,
													FMLeaderboard[i].iBadgeRank,
													FMLeaderboard[i].playerID,
													FALSE,
													bDraw[0],
													-1,
													-1)
													
								iSlotCounter++
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
//depot/gta5/script/dev_ng/singleplayer/scripts/debug/streaming.sc
#IF IS_DEBUG_BUILD
PROC MANAGE_LAUNCHING_STREAMING_SC()
	IF IS_DEBUG_KEY_PRESSED(KEY_NUMPAD6, KEYBOARD_MODIFIER_CTRL, "LaunchStreaming")
		bLoadStreamingScript = TRUE
	ENDIF
	IF bLoadStreamingScript
		IF NOT NETWORK_IS_SCRIPT_ACTIVE("streaming", -1, TRUE)
			IF NET_LOAD_AND_LAUNCH_SCRIPT("streaming", SCRIPT_XML_STACK_SIZE)
				bLoadStreamingScript = FALSE
				PRINTLN("=== Freemode === MANAGE_LAUNCHING_STREAMING_SC - LAUNCHED SCRIPT streaming.sc")
			ENDIF
		ELSE
			bLoadStreamingScript = FALSE
		ENDIF
	ENDIF
ENDPROC
#ENDIF


// -----------------------------------	MAINTAIN FREEMODE RUNNING! ----------------------------------------------------------------------

PROC MAINTAIN_GOOD_BOY_REMINDER()
	
	IF NOT IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
		IF NOT IS_TRANSITION_ACTIVE()
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(g_TransitionSessionNonResetVars.stGoodBoyReminder, GOOD_BOY_REMINDER_FREQUENCY) // Every 2 hours.
					PRINT_HELP("GOODBOYRMDR")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


PROC MAINTAIN_TELL_PLAYER_PLAYING_ALONE(SCRIPT_TIMER& AloneTimer)
	
	IF TELL_PLAYER_PLAYING_ALONE()
	
		IF IS_PLAYER_IN_CORONA()
			EXIT
		ENDIF
		
		IF NOT IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
			IF NOT IS_TRANSITION_ACTIVE()
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(AloneTimer, 12000) 
						NET_NL()NET_PRINT("MAINTAIN_TELL_PLAYER_PLAYING_ALONE: NETWORK_GET_TOTAL_NUM_PLAYERS = ")NET_PRINT_INT(NETWORK_GET_TOTAL_NUM_PLAYERS())
						IF NETWORK_GET_TOTAL_NUM_PLAYERS() = 1
							NET_NL()NET_PRINT("MAINTAIN_TELL_PLAYER_PLAYING_ALONE: print solo help. ")
							PRINT_HELP("HUD_ALONE") //You are currently in a session on your own. Find a new session by Pause Menu > Online > Join New Session.
						ENDIF
						SET_TELL_PLAYER_PLAYING_ALONE(FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC



PROC PRINT_SERVER_MAINTENANCE_TICKER_FEED(INT iTimeToExpire, STRING stMessage, INT iTimeLeft = 0, STRING stSub = NULL)
	IF NOT HAS_NET_TIMER_STARTED(g_sServerMaintenanceVars.sTimer)
		PRINTLN("[RSMAN] PRINT_SERVER_MAINTENANCE_TICKER_FEED - START_NET_TIMER - iTimeToExpire = ", iTimeToExpire)
		START_NET_TIMER(g_sServerMaintenanceVars.sTimer)
	ENDIF
	IF HAS_NET_TIMER_EXPIRED(g_sServerMaintenanceVars.sTimer, iTimeToExpire)
	OR g_sServerMaintenanceVars.bDisplayedThisBoot = FALSE
		//and we're able to print
		IF CAN_PRINT_ROCKSTAR_FEED_MSG()
			//then print to the ticker
			IF iTimeLeft = 0
				PRINT_TICKER(stMessage)	
				PRINTLN("[RSMAN] PRINT_SERVER_MAINTENANCE_TICKER_FEED - HAS_NET_TIMER_EXPIRED - iTimeToExpire = ", iTimeToExpire, " PRINT_TICKER(", stMessage, ") - stMessage = ", GET_FILENAME_FOR_AUDIO_CONVERSATION(stMessage))
			ELSE
				TEXT_LABEL_15 tlSubText
				IF IS_STRING_NULL_OR_EMPTY(stSub)
					UGC_DATE sDate
					CONVERT_POSIX_TIME(iTimeLeft, sDate)
					PRINT_TICKER_WITH_INT(stMessage, sDate.nHour)//The Rockstar game services will be down for scheduled maintenance tomorrow at ~a~.
					PRINTLN("[RSMAN] PRINT_SERVER_MAINTENANCE_TICKER_FEED - HAS_NET_TIMER_EXPIRED - iTimeToExpire = ", iTimeToExpire, " PRINT_TICKER_WITH_INT(", stMessage, ", ", sDate.nHour, ") - stMessage = ", GET_FILENAME_FOR_AUDIO_CONVERSATION(stMessage))
				ELSE
					tlSubText = stSub
					IF iTimeLeft <= 1
						iTimeLeft = 1
						tlSubText+=0 //[RSMAN_HSM0] - minute //[RSMAN_HSH0]- hour
					ELSE
						tlSubText+=1 //[RSMAN_HSM1] - minutes //[RSMAN_HSH1]- hours
					ENDIF
					PRINT_TICKER_WITH_STRING_AND_INT(stMessage, tlSubText, iTimeLeft)// time left in seconds/number of seconds in a minute
					PRINTLN("[RSMAN] PRINT_SERVER_MAINTENANCE_TICKER_FEED - HAS_NET_TIMER_EXPIRED - iTimeToExpire = ", iTimeToExpire, " PRINT_TICKER_WITH_CUSTOM_STRING_AND_INT(", stMessage, ", ", tlSubText, ", ", iTimeLeft, ") - stMessage = ", GET_FILENAME_FOR_AUDIO_CONVERSATION(stMessage), " - tlSubText = ", tlSubText)
				ENDIF
			ENDIF
			g_sServerMaintenanceVars.bDisplayedThisBoot = TRUE
			RESET_NET_TIMER(g_sServerMaintenanceVars.sTimer)
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RSMAN] PRINT_SERVER_MAINTENANCE_TICKER_FEED - HAS_NET_TIMER_EXPIRED - iTimeToExpire = ", iTimeToExpire, " CAN_PRINT_ROCKSTAR_FEED_MSG = FALSE")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PRINT_SERVER_MAINTENANCE_NOW_FEED()
	
	INT r, g, b, a
	INT numFlashes = 1
	
	SWITCH g_sServerMaintenanceVars.imessageStage
		CASE 0
			PRINTLN("[RSMAN] PRINT_SERVER_MAINTENANCE_NOW_FEED -DISPLAY_SAVE_FAIL_MESSAGE_FEED - SET UP")
			g_sServerMaintenanceVars.bFeedHidden = THEFEED_IS_PAUSED()
			THEFEED_FORCE_RENDER_ON()
			THEFEED_RESUME()
			THEFEED_FLUSH_QUEUE()
			RESET_NET_TIMER(g_sServerMaintenanceVars.sTimer)
			g_sServerMaintenanceVars.imessageStage++
		BREAK
		
		CASE 1
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(g_sServerMaintenanceVars.sTimer, 1000)
				PRINTLN("[RSMAN] PRINT_SERVER_MAINTENANCE_NOW_FEED -DISPLAY_SAVE_FAIL_MESSAGE_FEED - HAS_NET_TIMER_EXPIRED_ONE_FRAME(g_sServerMaintenanceVars.sTimer, 1000)")
			    GET_HUD_COLOUR(HUD_COLOUR_WHITE, r, g, b, a)
			    THEFEED_SET_RGBA_PARAMETER_FOR_NEXT_MESSAGE(r, g, b, a)
			    THEFEED_SET_FLASH_DURATION_PARAMETER_FOR_NEXT_MESSAGE(numFlashes)
				BEGIN_TEXT_COMMAND_THEFEED_POST("RSMAN_N")
				END_TEXT_COMMAND_THEFEED_POST_TICKER_FORCED(TRUE)					
				THEFEED_RESET_ALL_PARAMETERS()
				g_sServerMaintenanceVars.imessageStage++
			ENDIF
		BREAK
		
		CASE 2
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(g_sServerMaintenanceVars.sTimer, 10000)
			OR IS_WARNING_MESSAGE_ACTIVE()
				PRINTLN("[RSMAN] PRINT_SERVER_MAINTENANCE_NOW_FEED -DISPLAY_SAVE_FAIL_MESSAGE_FEED - HAS_NET_TIMER_EXPIRED_ONE_FRAME")
				g_sServerMaintenanceVars.imessageStage++
			ENDIF
		BREAK
		
		CASE 3
			PRINTLN("[RSMAN] PRINT_SERVER_MAINTENANCE_NOW_FEED -DISPLAY_SAVE_FAIL_MESSAGE_FEED - CLEAN UP")
			IF g_sServerMaintenanceVars.bFeedHidden = TRUE
				THEFEED_PAUSE()
				//THEFEED_FORCE_RENDER_OFF()
				PRINTLN("[RSMAN] PRINT_SERVER_MAINTENANCE_NOW_FEED -DISPLAY_SAVE_FAIL_MESSAGE_FEED - THEFEED_FORCE_RENDER_OFF")
			ENDIF
			g_sServerMaintenanceVars.bFeedHidden = FALSE
			g_sServerMaintenanceVars.imessageStage++
			PRINTLN("[RSMAN] PRINT_SERVER_MAINTENANCE_NOW_FEED - g_sServerMaintenanceVars.bDisplayNowMessage = FALSE")
			g_sServerMaintenanceVars.bDisplayNowMessage = FALSE
		BREAK
	ENDSWITCH
ENDPROC

//Controlled by the tunable:SERVER_MAINTENANCE_POSIX
PROC MAINTAIN_SERVER_MAINTENANCE_TICKER_FEED()
	//If have a time for the server maintenance
	IF g_sMPTunables.iServerMaintenancePosixTime != 0
		INT iCurrentPosix = GET_CLOUD_TIME_AS_INT()
		//If it's not ended or end time is 0
		IF iCurrentPosix < g_sMPTunables.iServerMaintenancePosixTimeEnd
		OR g_sMPTunables.iServerMaintenancePosixTimeEnd = 0
			//Continual
			IF iCurrentPosix >= g_sMPTunables.iServerMaintenancePosixTime
				IF g_sServerMaintenanceVars.imessageStage = 0
					PRINTLN("[RSMAN] MAINTAIN_SERVER_MAINTENANCE_TICKER_FEED - g_sServerMaintenanceVars.bDisplayNowMessage = TRUE")
					g_sServerMaintenanceVars.bDisplayNowMessage = TRUE
				ENDIF
			//Every 10 minutes in last hour
			ELIF g_sMPTunables.iServerMaintenancePosixTime - iCurrentPosix < 3600 //60*60
				PRINT_SERVER_MAINTENANCE_TICKER_FEED(600000, "RSMAN_H", (g_sMPTunables.iServerMaintenancePosixTime - iCurrentPosix)/60, "RSMAN_HSM") //60*10*1000 //The Rockstar game services will be down for scheduled maintenance in ~1~ ~a~.
			//Every hour minutes in last 24 hours
			ELIF g_sMPTunables.iServerMaintenancePosixTime - iCurrentPosix < 86400 //60*60*24
				PRINT_SERVER_MAINTENANCE_TICKER_FEED(3600000, "RSMAN_H", (g_sMPTunables.iServerMaintenancePosixTime - iCurrentPosix)/3600, "RSMAN_HSH") //60*60*1000 //The Rockstar game services will be down for scheduled maintenance in ~1~ ~a~.
			//every 5 hours in the last two days
			ELIF g_sMPTunables.iServerMaintenancePosixTime - iCurrentPosix < 172800 //60*60*24*2
				PRINT_SERVER_MAINTENANCE_TICKER_FEED(3600000, "RSMAN_TM", g_sMPTunables.iServerMaintenancePosixTime) //60*60*1000
			ELSE
				//Not due any time soon, forget about the prints
				PRINTLN("[RSMAN] MAINTAIN_SERVER_MAINTENANCE_TICKER_FEED - More than two days away")
			ENDIF
		ELSE
			g_sServerMaintenanceVars.bDisplayNowMessage = FALSE
			g_sServerMaintenanceVars.bDisplayedThisBoot = FALSE
		ENDIF
	ELSE
		g_sServerMaintenanceVars.bDisplayedThisBoot = FALSE
		g_sServerMaintenanceVars.bDisplayNowMessage = FALSE
	ENDIF
ENDPROC

//Controlled by the event PROC PROCESS_EVENT_NETWORK_CLAN_INVITE_REQUEST_RECEIVED(INT iCount)
PROC MAINTAIN_CLAN_INVITE_REQUEST_RECEIVED_TICKER_FEED()
	IF g_bCLAN_INVITE_REQUEST_RECEIVED
		IF CAN_PRINT_ROCKSTAR_FEED_MSG()
			PRINT_TICKER("FM_CREW_INV")	
			PRINTLN("[CIRR] - MAINTAIN_CLAN_INVITE_REQUEST_RECEIVED_TICKER_FEED - PRINT_TICKER")
			g_bCLAN_INVITE_REQUEST_RECEIVED = FALSE
		ENDIF
	ENDIF
ENDPROC



FUNC BOOL IS_IT_SAFE_TO_SEND_ADVERSARY_INVITE()
	IF CAN_PRINT_ROCKSTAR_FEED_MSG()
	AND g_FM_EVENT_VARS.iFmAmbientEventCountDownType = FMMC_TYPE_INVALID 
	AND NOT FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
	AND IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
	AND NOT IS_RADAR_HIDDEN()
	AND NOT IS_POST_MISSION_SCENE_ACTIVE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Determine where in the "invite playlist" the player is. This playlist should be the same globally, so all players are always within the same invite window.
/// PARAMS:
///    iSeed = iCurrentPosix - g_sMPTunables.iNPCFlowInviteStartPosix % 12 (generated from posix time during a 5 minute interval so that it will be the same globally for all players)
/// RETURNS:
///    Returns the appropriate tunable, depending on where in the playlist they are. 
///    Each tuneable is manually filled with either a specific adversary type, a random adversary flag, a random contact mission flag or a specific mission hash.
///    See GET_RANDOM_INVITE_FLOW_MISSION() or url:bugstar:2673192 for details on how it is used.
///    
///    NOTE: If additional tuneables are to be added here, please update the maximum count here: MAX_NUMBER_OF_INVITE_FLOW_TUNEABLES
FUNC INT GET_CURRENT_INVITE_FLOW_TUNEABLE(INT iSeed #IF IS_DEBUG_BUILD ,BOOL bHidePrints = FALSE #ENDIF)
	#IF IS_DEBUG_BUILD
	IF NOT bHidePrints
		CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - GET_RANDOM_INVITE_FLOW_TUNEABLE - iSeed = ", iSeed)
	ENDIF
	
	IF g_iForcedInviteFlowWidget != -2
		CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - GET_RANDOM_INVITE_FLOW_TUNEABLE - Using widget value - g_iForcedInviteFlowWidget = ", g_iForcedInviteFlowWidget)
		RETURN g_iForcedInviteFlowWidget
	ENDIF
	#ENDIF

	SWITCH iSeed
		CASE 0 
			RETURN g_sMPTunables.iNPCFlowInvite1
		CASE 1 
			RETURN g_sMPTunables.iNPCFlowInvite2
		CASE 2 
			RETURN g_sMPTunables.iNPCFlowInvite3
		CASE 3 
			RETURN g_sMPTunables.iNPCFlowInvite4
		CASE 4 
			RETURN g_sMPTunables.iNPCFlowInvite5
		CASE 5 
			RETURN g_sMPTunables.iNPCFlowInvite6
		CASE 6 
			RETURN g_sMPTunables.iNPCFlowInvite7
		CASE 7 
			RETURN g_sMPTunables.iNPCFlowInvite8
		CASE 8 
			RETURN g_sMPTunables.iNPCFlowInvite9
		CASE 9 
			RETURN g_sMPTunables.iNPCFlowInvite10
		CASE 10 
			RETURN g_sMPTunables.iNPCFlowInvite11
		CASE 11 
			RETURN g_sMPTunables.iNPCFlowInvite12
	ENDSWITCH
	
	CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - GET_CURRENT_INVITE_FLOW_TUNEABLE - iSeed is invalid. Returning -2.")
	RETURN -2
ENDFUNC

/// PURPOSE:
///    Returns the rootContentID for a random contact mission, using the player's level to check unlocked content.
FUNC INT GET_RANDOM_CONTACT_MISSION_FROM_RANDOM_CHARACTER(MP_MISSION_ID_DATA& mpMissionIdData)
	INT iCMArrayPos
	INT iCharacter = NETWORK_GET_RANDOM_INT_RANGED(0, 3)
	enumCharacterList eCharacter
	
	CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - GET_RANDOM_CONTACT_MISSION_FROM_RANDOM_CHARACTER - iCharacter = ", iCharacter)
	
	SWITCH iCharacter
		CASE 0			eCharacter = CHAR_MP_GERALD			BREAK
		CASE 1			eCharacter = CHAR_LESTER			BREAK
		CASE 2			eCharacter = CHAR_SIMEON			BREAK
		CASE 3			eCharacter = CHAR_RON				BREAK
	ENDSWITCH
	
	iCMArrayPos = Get_Random_Contact_Mission_For_Contact(eCharacter, GET_PLAYER_RANK(PLAYER_ID()))
	
	IF iCMArrayPos <> ILLEGAL_CONTACT_MISSION_SLOT
		mpMissionIdData = GATHER_AND_GET_MISSIONID_DATA_FOR_CONTACT_MISSION(iCMArrayPos)
		
		IF (ARE_THESE_FM_CLOUD_LOADED_ACTIVITY_DETAILS_AVAILABLE_LOCALLY(mpMissionIdData))
			RETURN GET_ROOTCONTENTIDHASH_FOR_FM_CLOUD_LOADED_ACTIVITY(mpMissionIdData)
		ELSE
			CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - ARE_THESE_FM_CLOUD_LOADED_ACTIVITY_DETAILS_AVAILABLE_LOCALLY = FALSE. RETURNING -2.")
			RETURN -2
		ENDIF
	ELSE
		CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - iCMArrayPos = ILLEGAL_CONTACT_MISSION_SLOT. RETURNING -2.")
		RETURN -2
	ENDIF
ENDFUNC

CONST_INT ciINVITE_FLOW_RANDOM 						-1
CONST_INT ciINVITE_FLOW_RANDOM_ADVERSARY 			99
CONST_INT ciINVITE_FLOW_RANDOM_CONTACT 				100
CONST_INT ciINVITE_FLOW_STUNT_SERIES				101
CONST_INT ciINVITE_FLOW_ADVERSARY_SERIES_0 			102
CONST_INT ciINVITE_FLOW_ADVERSARY_SERIES_1 			103
CONST_INT ciINVITE_FLOW_ADVERSARY_SERIES_2			104
CONST_INT ciINVITE_FLOW_FEATURED_ADVERSARY_SERIES	105
CONST_INT ciINVITE_FLOW_SPECIAL_RACES				106
CONST_INT ciINVITE_FLOW_BUNKER						107
CONST_INT ciINVITE_FLOW_TRANSFORM					108
CONST_INT ciINVITE_FLOW_TARGET_ASSAULT				109
CONST_INT ciINVITE_FLOW_SASS						110
CONST_INT ciINVITE_FLOW_RACE_SERIES					112
CONST_INT ciINVITE_FLOW_SURVIVAL_SERIES				113

#IF IS_DEBUG_BUILD
FUNC STRING GET_INVITE_FLOW_MISSION_TYPE_STRING(INT iMissionType)
	SWITCH iMissionType
		CASE ciINVITE_FLOW_RANDOM 						RETURN "ciINVITE_FLOW_RANDOM"
		CASE ciINVITE_FLOW_RANDOM_ADVERSARY 			RETURN "ciINVITE_FLOW_RANDOM_ADVERSARY"
		CASE ciINVITE_FLOW_RANDOM_CONTACT				RETURN "ciINVITE_FLOW_RANDOM_CONTACT"
		CASE ciINVITE_FLOW_STUNT_SERIES					RETURN "ciINVITE_FLOW_STUNT_SERIES"
		CASE ciINVITE_FLOW_ADVERSARY_SERIES_0			RETURN "ciINVITE_FLOW_ADVERSARY_SERIES_0"
		CASE ciINVITE_FLOW_ADVERSARY_SERIES_1			RETURN "ciINVITE_FLOW_ADVERSARY_SERIES_1"
		CASE ciINVITE_FLOW_ADVERSARY_SERIES_2			RETURN "ciINVITE_FLOW_ADVERSARY_SERIES_2"
		CASE ciINVITE_FLOW_FEATURED_ADVERSARY_SERIES	RETURN "ciINVITE_FLOW_FEATURED_ADVERSARY_SERIES"
		CASE ciINVITE_FLOW_SPECIAL_RACES				RETURN "ciINVITE_FLOW_SPECIAL_RACES"
		CASE ciINVITE_FLOW_BUNKER						RETURN "ciINVITE_FLOW_BUNKER"
		CASE ciINVITE_FLOW_TRANSFORM					RETURN "ciINVITE_FLOW_TRANSFORM"
		CASE ciINVITE_FLOW_TARGET_ASSAULT				RETURN "ciINVITE_FLOW_TARGET_ASSAULT"
		CASE ciINVITE_FLOW_SASS							RETURN "ciINVITE_FLOW_SASS"
		CASE ciINVITE_FLOW_RACE_SERIES					RETURN "ciINVITE_FLOW_RACE_SERIES"
		CASE ciINVITE_FLOW_SURVIVAL_SERIES				RETURN "ciINVITE_FLOW_SURVIVAL_SERIES"
	ENDSWITCH
	
	IF iMissionType >= 0 AND iMissionType < ENUM_TO_INT(CM_ADVERSARY_TYPE_MAX)
		RETURN Get_Contact_Mission_Adversary_Reminder_Name(INT_TO_ENUM(g_eContactMissionAdversaryTypes, iMissionType))
	ENDIF
	RETURN "SPECIFIC_MISSION"
ENDFUNC
#ENDIF
FUNC INT GET_ADVERSARY_SERIES_INVITE_FLOW_MISSION(INT iSeries)
	INT iSeriesArrayPos = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(iSeries)
	IF iSeriesArrayPos != -1
		INT iPlaylistPosition = CV2_PROFESSIONAL_GET_CURRENTLY_ACTIVE_V2_CORONA_POS(CV2_GET_ADVERSARY_SERIES_ARRAY_POS(iSeries))
		IF iPlaylistPosition != -1
			RETURN g_sMPTunables.iFmCoronaPlaylistProffesionalRootContentId[CV2_GET_ADVERSARY_SERIES_ARRAY_POS(iSeries)][iPlaylistPosition]
		ENDIF
		CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - GET_ADVERSARY_SERIES_INVITE_FLOW_MISSION - iSeries = ", iSeries, " - iPlaylistPosition = -1. Returning -2.")
		RETURN -2
	ENDIF
	CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - GET_ADVERSARY_SERIES_INVITE_FLOW_MISSION - iSeries = ", iSeries, " - iSeriesArrayPos = -1. Returning -2.")
	RETURN -2
ENDFUNC

FUNC INT GET_STUNT_SERIES_INVITE_FLOW_MISSION()
	INT iRootContentId = CV2_GET_CURRENTLY_ACTIVE_V2_CORONA_ROOT_CONTENT_ID_HASH(CV2_GET_CURRENTLY_ACTIVE_V2_CORONA_POS())
	IF iRootContentId != -1
		RETURN iRootContentId
	ENDIF
	CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - GET_STUNT_SERIES_INVITE_FLOW_MISSION - Root Content ID = -1. Returning -2.")
	RETURN -2
ENDFUNC

/// PURPOSE:
///    Reads the value of the parameter and returns an appropriate rootContentID for the selected mission type, or the specific mission if a hash is supplied.
/// PARAMS:
///    iMissionType - Determines what type of mission is selected. See below:
///   				-1 = Returns a random adversary or random contact mission.
///    				0 - CM_ADVERSARY_TYPE_MAX = Returns a specific type of adversary mode, depending on the value e.g. 1 = Siege Mentality.
///    				99 = Returns a random adversary mode.
///    				100 = Returns a random contact mission.
///  				101 = Returns the current stunt series race.
///  				102 = Returns the current adversary series mission.
///  				103 = Returns the current adversary series 1 mission.
///  				104 = Returns the current adversary series 2 mission.
///    				Specific Hash (e.g. 2004371279) = Returns this directly as a specific mission has been set in the tuneable.
///    mpMissionIdData - This is only relevant for contact missions. Updated to make sure invite is correct for Contact Missions if selected.
/// RETURNS:
///    Returns the rootContentID of a mission from the selected type, either passed in via parameter or randomised.
FUNC INT GET_RANDOM_INVITE_FLOW_MISSION(INT iMissionType, MP_MISSION_ID_DATA& mpMissionIdData)
	CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [ADVERSARY_PROMO] [RE] - GET_RANDOM_INVITE_FLOW_MISSION - iMissionType = ", iMissionType)

	// Check first if the mission type supplied matches to an adversary mode.
	SWITCH INT_TO_ENUM(g_eContactMissionAdversaryTypes, iMissionType)

		CASE CM_ADVERSARY_TYPE_COME_OUT_TO_PLAY						RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_NEW_VS_MISSION_ComeOutToPlay, FALSE)
		CASE CM_ADVERSARY_TYPE_SIEGE_MENTALITY						RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_NEW_VS_MISSION_SiegeMentality, FALSE)
		CASE CM_ADVERSARY_TYPE_HASTA_LA_VISTA						RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_NEW_VS_MISSION_HastaLaVista, FALSE)
		CASE CM_ADVERSARY_TYPE_HUNTING_PACK							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_NEW_VS_MISSION_HuntingPack, FALSE)
		CASE CM_ADVERSARY_TYPE_CROSS_THE_LINE						RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_NEW_VS_MISSION_CrossTheLine, FALSE)
		CASE CM_ADVERSARY_TYPE_SHEPHERD				 				RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SHEPHERD, FALSE)
		CASE CM_ADVERSARY_TYPE_RELAY				 				RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RELAY, FALSE)
		CASE CM_ADVERSARY_TYPE_SPEED_RACE			 				RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SPEEDRACE, FALSE)
		CASE CM_ADVERSARY_TYPE_EBC					 				RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_EBC, FALSE)	
		CASE CM_ADVERSARY_TYPE_HUNT_DARK				 			RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_HUNTDARK, FALSE)
		CASE CM_ADVERSARY_TYPE_RUNNING_BACK							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RUNNINGBACK, FALSE)
		CASE CM_ADVERSARY_TYPE_EXTRACTION			 				RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_EXTRACTION, FALSE)
		CASE CM_ADVERSARY_TYPE_BEAST_V_SLASHER  					RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_BVS1, FALSE)
		CASE CM_ADVERSARY_TYPE_DROP_ZONE							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DROP_ZONE, FALSE)
		CASE CM_ADVERSARY_TYPE_BE_MY_VALENTINE			 			RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_BE_MY_VALENTIN, FALSE)
		CASE CM_ADVERSARY_TYPE_DAVID_AND_GOLIATH					RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DAVID_AND_GOLIATH, FALSE)
		CASE CM_ADVERSARY_TYPE_SUMO									RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SUMO, FALSE)
		CASE CM_ADVERSARY_TYPE_RUGBY								RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RUGBY, FALSE)
		CASE CM_ADVERSARY_TYPE_IN_AND_OUT							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_IN_AND_OUT, FALSE)
		CASE CM_ADVERSARY_TYPE_POWER_PLAY							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_POWER_PLAY, FALSE)
		CASE CM_ADVERSARY_TYPE_TRADE_PLACES							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_GRASS_GREENER, FALSE)
		CASE CM_ADVERSARY_TYPE_ENTOURAGE							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_ENTOURAGE, FALSE)
		CASE CM_ADVERSARY_TYPE_DEADLINE								RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_DONT_CROSS_THE_LINE, FALSE)
		CASE CM_ADVERSARY_TYPE_LOSTVDAMNED							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_LOST_DAMNED, FALSE)
		CASE CM_ADVERSARY_TYPE_SLIPSTREAM							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_TOUR_DE_FORCE, FALSE)
		CASE CM_ADVERSARY_TYPE_KILL_QUOTA							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_GUNSMITH, FALSE)
		CASE CM_ADVERSARY_TYPE_JUGGERNAUT							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_JUGGERNAUT)
		CASE CM_ADVERSARY_TYPE_TURF_WAR								RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_TURF_WARS)
		CASE CM_ADVERSARY_TYPE_POINTLESS							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_POINTLESS)
		CASE CM_ADVERSARY_TYPE_VEHICLE_VENDETTA						RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_VEHICULAR_VENDETTA)
		CASE CM_ADVERSARY_TYPE_TINY_RACERS 							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_TINYRACERS)
		CASE CM_ADVERSARY_TYPE_LANDGRAB								RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_TURF_WARZONE)
		CASE CM_ADVERSARY_TYPE_RESURRECTION							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_RESURRECTION)
		CASE CM_ADVERSARY_TYPE_OVERTIME								RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_OVERTIME_TURNS)
		CASE CM_ADVERSARY_TYPE_DAWN_RAID							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DAWN_RAID, FALSE)
		CASE CM_ADVERSARY_TYPE_POWER_MAD							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_POWER_MAD)
		CASE CM_ADVERSARY_TYPE_OVERTIME_RUMBLE						RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_OVERTIME_RUMBLE)
		CASE CM_ADVERSARY_TYPE_VEHICLE_WARFARE						RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_VEHICULAR_WARFARE)
		CASE CM_ADVERSARY_TYPE_BOMBUSHKA_RUN						RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_BOMBUSHKA)
		CASE CM_ADVERSARY_TYPE_STOCKPILE							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_STOCKPILE)
		CASE CM_ADVERSARY_TYPE_CONDEMNED							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_CONDEMNED)
		CASE CM_ADVERSARY_TYPE_AIR_SHOOTOUT							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_AIRSHOOTOUT)
		CASE CM_ADVERSARY_TYPE_HARD_TARGET							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_HARD_TARGET)
		CASE CM_ADVERSARY_TYPE_SLASHERS								RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_SLASHERS)
		CASE CM_ADVERSARY_TYPE_HOSTILE_TAKEOVER						RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_HOSTILE_TAKEOVER)
		CASE CM_ADVERSARY_TYPE_AIR_QUOTA							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_AIRQUOTA)
		CASE CM_ADVERSARY_TYPE_VENETIAN_JOB							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_VENETIAN_JOB)
		CASE CM_ADVERSARY_TYPE_TRAP_DOOR							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_TRAP_DOOR)
		CASE CM_ADVERSARY_TYPE_SUMO_REMIX							RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_SUMO_RUN)
		CASE CM_ADVERSARY_TYPE_HUNTING_PACK_REMIX					RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_STUNTING_PACK)
		CASE CM_ADVERSARY_TYPE_TRADING_PLACES_REMIX					RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_TRADING_PLACES_REMIX)
		CASE CM_ADVERSARY_TYPE_RUNNING_BACK_REMIX					RETURN GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(-1, FALSE, ciNEWVS_RUNNING_BACK_REMIX)
	ENDSWITCH
	// If the mission type doesn't match an adversary mode, check to see if it is a random flag.
	SWITCH iMissionType
	
		// Random Mission
		CASE ciINVITE_FLOW_RANDOM 			IF (NETWORK_GET_RANDOM_INT_RANGED(0, 100) < 49)     RETURN GET_RANDOM_INVITE_FLOW_MISSION(100, mpMissionIdData)   ENDIF     RETURN GET_RANDOM_INVITE_FLOW_MISSION(99, mpMissionIdData)		
		CASE ciINVITE_FLOW_RANDOM_ADVERSARY 													RETURN GET_RANDOM_INVITE_FLOW_MISSION(NETWORK_GET_RANDOM_INT_RANGED(0, ENUM_TO_INT(CM_ADVERSARY_TYPE_MAX)-1), mpMissionIdData)
		CASE ciINVITE_FLOW_RANDOM_CONTACT														RETURN GET_RANDOM_CONTACT_MISSION_FROM_RANDOM_CHARACTER(mpMissionIdData)
		CASE ciINVITE_FLOW_STUNT_SERIES															RETURN GET_STUNT_SERIES_INVITE_FLOW_MISSION()
		CASE ciINVITE_FLOW_ADVERSARY_SERIES_0													RETURN GET_ADVERSARY_SERIES_INVITE_FLOW_MISSION(FMMC_TYPE_ADVERSARY_SERIES)
		CASE ciINVITE_FLOW_ADVERSARY_SERIES_1													RETURN GET_ADVERSARY_SERIES_INVITE_FLOW_MISSION(FMMC_TYPE_ADVERSARY_SERIES_1)
		CASE ciINVITE_FLOW_ADVERSARY_SERIES_2													RETURN GET_ADVERSARY_SERIES_INVITE_FLOW_MISSION(FMMC_TYPE_ADVERSARY_SERIES_2)
		CASE ciINVITE_FLOW_FEATURED_ADVERSARY_SERIES											RETURN GET_ADVERSARY_SERIES_INVITE_FLOW_MISSION(FMMC_TYPE_MISSION_NEW_VS)
		CASE ciINVITE_FLOW_SPECIAL_RACES														RETURN GET_ADVERSARY_SERIES_INVITE_FLOW_MISSION(FMMC_TYPE_SPECIAL_VEHICLE_RACE_SERIES)
		CASE ciINVITE_FLOW_BUNKER																RETURN GET_ADVERSARY_SERIES_INVITE_FLOW_MISSION(FMMC_TYPE_BUNKER_SERIES)
		CASE ciINVITE_FLOW_TRANSFORM															RETURN GET_ADVERSARY_SERIES_INVITE_FLOW_MISSION(FMMC_TYPE_TRANSFORM_SERIES)
		CASE ciINVITE_FLOW_TARGET_ASSAULT														RETURN GET_ADVERSARY_SERIES_INVITE_FLOW_MISSION(FMMC_TYPE_TARGET_ASSAULT_SERIES)
		CASE ciINVITE_FLOW_SASS																	RETURN GET_ADVERSARY_SERIES_INVITE_FLOW_MISSION(FMMC_TYPE_HOTRING_SERIES)	// Hotring repurposed for SASS
		CASE ciINVITE_FLOW_RACE_SERIES															RETURN GET_ADVERSARY_SERIES_INVITE_FLOW_MISSION(FMMC_TYPE_RACE_SERIES)
		CASE ciINVITE_FLOW_SURVIVAL_SERIES														RETURN GET_ADVERSARY_SERIES_INVITE_FLOW_MISSION(FMMC_TYPE_SURVIVAL_SERIES)	
	ENDSWITCH
	
	CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [ADVERSARY_PROMO] [RE] - GET_RANDOM_INVITE_FLOW_MISSION - iMissionType was not found in switch. Assuming it is a specific RootContentId and returning that. If -2, the tunable is off or series mission unavailable.")
	CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [ADVERSARY_PROMO] [RE] - GET_RANDOM_INVITE_FLOW_MISSION - RETURN iMissionType (", iMissionType, ")")
	RETURN iMissionType
ENDFUNC

/// PURPOSE:
///    Return the base text label to display for a adversary mode
FUNC TEXT_LABEL_15 Get_Adversay_Base_Help_Text(g_eContactMissionAdversaryTypes adversaryType, INT iSeries)
	TEXT_LABEL_15 tlHelpText = "ADVERSARY_NEW"
	
	IF iSeries != (-1)
		SWITCH iSeries
			CASE ciINVITE_FLOW_TARGET_ASSAULT			tlHelpText = "ADV_PR_TARGRACE"		BREAK
			CASE ciINVITE_FLOW_TRANSFORM				tlHelpText = "ADV_PR_TRANS"			BREAK
			CASE ciINVITE_FLOW_SPECIAL_RACES			tlHelpText = "ADV_PR_SPECRACE"		BREAK
			CASE ciINVITE_FLOW_SASS						tlHelpText = "ADV_PR_HOTRING"		BREAK
		ENDSWITCH
	ELSE
		IF adversaryType = CM_ADVERSARY_TYPE_DEADLINE
				IF g_sMPTunables.bTURN_ON_DEADLINER_JOB
					tlHelpText = "DEADLINER0"
				ENDIF
		ENDIF
	ENDIF
	
	RETURN tlHelpText
ENDFUNC

FUNC STRING GET_ADVERSARY_SERIES_PROMO_NAME(INT iPromoSeries)
	IF iPromoSeries = ciINVITE_FLOW_TARGET_ASSAULT	
		RETURN "RC_TARGASSAULT"
	ENDIF
	
	RETURN ""
ENDFUNC

// Exit if the local player is on a tutorial or in a solo session
FUNC BOOL SHOULD_EXIT_FROM_GLOBAL_INVITE_FLOW()
	IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
	OR IS_LOCAL_PLAYER_DOING_ANY_AMBIENT_TUTORIAL()
	OR IS_THIS_A_SOLO_SESSION()
		#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 30) = 0
			IF IS_THIS_A_SOLO_SESSION()
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - MAINTAIN_GLOBAL_INVITE_FLOW - Local player is in a solo session. Exiting.")
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - MAINTAIN_GLOBAL_INVITE_FLOW - Local player is on a tutorial. Exiting.")
			ENDIF
		ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

CONST_INT ARRAY_SLOTS_TO_PROCESS_AT_ONCE_FOR_GLOBAL_INVITE_FLOW		500
FUNC INT GET_START_LOOP_FOR_GLOBAL_INVITE_FLOW()
	RETURN g_iInviteFlowSearchAttempts * ARRAY_SLOTS_TO_PROCESS_AT_ONCE_FOR_GLOBAL_INVITE_FLOW
ENDFUNC

FUNC INT GET_END_LOOP_FOR_GLOBAL_INVITE_FLOW(INT iStartLoop)
	INT iEndLoop = (iStartLoop + ARRAY_SLOTS_TO_PROCESS_AT_ONCE_FOR_GLOBAL_INVITE_FLOW)
	IF iEndLoop >= FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED
		iEndLoop = (FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED-1)
	ENDIF
	RETURN iEndLoop
ENDFUNC

FUNC INT GET_TIME_UNTIL_INVITE_FOR_GLOBAL_INVITE_FLOW()
	IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
	AND DOES_PLAYER_OWN_OFFICE(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 30) = 0
			CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - MAINTAIN_GLOBAL_INVITE_FLOW - Local player is a CEO. Extending time between invites to ", g_sMPtunables.iNPCFlowInviteTimeUntilInviteInSecondsForCEO, " seconds from the usual ", g_sMPTunables.iNPCFlowInviteTimeUntilInviteInSeconds, " seconds.")
		ENDIF
		#ENDIF
		
		RETURN g_sMPtunables.iNPCFlowInviteTimeUntilInviteInSecondsForCEO
	ENDIF
	
	RETURN g_sMPTunables.iNPCFlowInviteTimeUntilInviteInSeconds
ENDFUNC

/// PURPOSE:
///    Set up a global "invite playlist" that will loop round. Every 5 minutes from when a player boots in they will receive an invite to
///    a mission manually set in a tuneable. Every 5 minutes globally the "playlist" will move on and read from the next tuneable. Every player
///    will be within the same "invite window" at the same time as it's calculated using posix from when the invite playlist was activated.
///    The idea is to invite players globally to the same missions every 5 minutes to bring them together for lobbied missions.
///    See url:bugstar:2673192 for further details.
PROC MAINTAIN_GLOBAL_INVITE_FLOW()
	INT iCurrentPosix = GET_CLOUD_TIME_AS_INT()
	INT iTuneableValue, iActiveTuneables
	MP_MISSION_ID_DATA mpMissionIdData
	
	enumCharacterList eInviteSenderCharacter =  CHAR_MARTIN
	
	// If this is taking place after the content is released/activated
	IF g_sMPTunables.bNPCFlowInvitePlaylistActive
	AND iCurrentPosix > g_sMPTunables.iNPCFlowInviteStartPosix
	
		IF SHOULD_EXIT_FROM_GLOBAL_INVITE_FLOW()
			RESET_DATA_FOR_GLOBAL_INVITE_FLOW()
			EXIT
		ENDIF
		
		////////////////////////////////////////////////////////////////////////
		// If we've selected a mission yet, send invite for mission to player //
		////////////////////////////////////////////////////////////////////////

		// -3 = Not selected yet. 
		// -2 = Invalid mission selected.
		IF g_iInviteFlowMissionRootId != -2
		AND g_iInviteFlowMissionRootId != -3
		
			// If it's a contact mission we're sending an invite for, process it here
			IF mpMissionIdData.idMission != eNULL_MISSION
				IF IS_IT_SAFE_TO_SEND_ADVERSARY_INVITE()
					Setup_Contact_Mission_From_CM_Data_And_Send_Invite(mpMissionIdData)
					g_iInviteFlowLastInvitePosix = iCurrentPosix
					RESET_DATA_FOR_GLOBAL_INVITE_FLOW()
					CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - MAINTAIN_GLOBAL_INVITE_FLOW - Sent contact mission invite. g_iInviteFlowLastInvitePosix =  ", g_iInviteFlowLastInvitePosix)
				ELSE
					CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - MAINTAIN_GLOBAL_INVITE_FLOW - Invite for contact mission not sent. IS_IT_SAFE_TO_SEND_ADVERSARY_INVITE = FALSE.")
				ENDIF
				
			// If it's an adversary mode or stunt race invite, process it here
			ELSE
				IF IS_IT_SAFE_TO_SEND_ADVERSARY_INVITE()
					INT iStartLoop = GET_START_LOOP_FOR_GLOBAL_INVITE_FLOW()
					INT iEndLoop = GET_END_LOOP_FOR_GLOBAL_INVITE_FLOW(iStartLoop)
					IF SET_MESSAGE_INVITE_FROM_ROOT_CONTENT_ID_HASH(g_iInviteFlowMissionRootId, eInviteSenderCharacter, TRUE, iStartLoop, iEndLoop)
						g_iInviteFlowLastInvitePosix = iCurrentPosix
						RESET_DATA_FOR_GLOBAL_INVITE_FLOW()
						CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - MAINTAIN_GLOBAL_INVITE_FLOW - Sent adversary/stunt invite. g_iInviteFlowLastInvitePosix = ", g_iInviteFlowLastInvitePosix, " - Start of loop: ", iStartLoop, " - End of loop: ", iEndLoop)
					ELSE
						g_iInviteFlowSearchAttempts++
						IF g_iInviteFlowSearchAttempts > 5
							#IF IS_DEBUG_BUILD
							IF SHOULD_USE_ROCKSTAR_CANDIDATE_MISSIONS()
								CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - MAINTAIN_GLOBAL_INVITE_FLOW - Mission invite failed while Rockstar Candidate missions are loaded. This is expected for released content while running the commandline.")
							ELSE
								CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - MAINTAIN_GLOBAL_INVITE_FLOW - Mission invite failed while not running candidate missions. This is expected if inviting to candidate content without the commandline.")
							ENDIF
							#ENDIF
							
							RESET_DATA_FOR_GLOBAL_INVITE_FLOW()
							CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - MAINTAIN_GLOBAL_INVITE_FLOW - Unable to find mission in array. Resetting data for next invite.")
						ENDIF
					ENDIF
				ELSE
					CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - MAINTAIN_GLOBAL_INVITE_FLOW - Invite not sent. IS_IT_SAFE_TO_SEND_ADVERSARY_INVITE = FALSE.")
				ENDIF
			ENDIF
			EXIT
		#IF IS_DEBUG_BUILD
		ELSE
			IF (GET_FRAME_COUNT() % 30) = 0
				IF g_iInviteFlowMissionRootId = -2
					CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - MAINTAIN_GLOBAL_INVITE_FLOW - iMission = ", g_iInviteFlowMissionRootId, " - Mission is invalid. Tuneable turned off.")
				ELSE
					CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - MAINTAIN_GLOBAL_INVITE_FLOW - iMission = ", g_iInviteFlowMissionRootId, " - No mission selected yet.")
				ENDIF
			ENDIF
			#ENDIF
		ENDIF
		
		/////////////////////////////////////////////////////////////////////////////////
		// If we've not selected a mission yet, process rest of function to select one //
		/////////////////////////////////////////////////////////////////////////////////
		
		// Get how long has passed since activation
		INT iTimePassedSinceActivate = (iCurrentPosix - g_sMPTunables.iNPCFlowInviteStartPosix)
		INT iTimeUntilInvite = GET_TIME_UNTIL_INVITE_FOR_GLOBAL_INVITE_FLOW()
	
		IF iCurrentPosix > (g_iInviteFlowLastInvitePosix + iTimeUntilInvite)
		#IF IS_DEBUG_BUILD
		OR g_bSendInviteFlowInviteWidget
		#ENDIF
		
			#IF IS_DEBUG_BUILD
			IF g_bSendInviteFlowInviteWidget
				g_bSendInviteFlowInviteWidget = FALSE
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - MAINTAIN_GLOBAL_INVITE_FLOW - Tunable used to send invite. Resetting g_bSendInviteFlowInviteWidget to false.")
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - MAINTAIN_GLOBAL_INVITE_FLOW - Current POSIX is ", iCurrentPosix, ". Last invite sent at POSIX ", g_iInviteFlowLastInvitePosix, ". At least ", iTimeUntilInvite, " seconds have passed since last invite.")
			ENDIF
			#ENDIF

			// Check to see how many tuneables are turned on, and use this to determine how long the playlist currently is
			// NOTE: This is to prevent a 5 minute delay in invites if a tuneable in the playlist is set to 0
			INT iIndex
			REPEAT MAX_NUMBER_OF_INVITE_FLOW_TUNEABLES iIndex
				IF GET_CURRENT_INVITE_FLOW_TUNEABLE(iIndex #IF IS_DEBUG_BUILD, TRUE #ENDIF) != -2
					iActiveTuneables++
				ENDIF
			ENDREPEAT
			CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - MAINTAIN_GLOBAL_INVITE_FLOW - Number of Active Tuneables = ", iActiveTuneables)
			
			// Seed the random number generator with the playlist position so that everyone is given the same result
			NETWORK_SEED_RANDOM_NUMBER_GENERATOR((iCurrentPosix / g_sMPTunables.iNPCFlowInviteIntervalInSeconds))
			
			// Figure out how far through the "playlist" of tuneables we are, and use that invite
			INT iSeed = (iTimePassedSinceActivate / g_sMPTunables.iNPCFlowInviteIntervalInSeconds) % iActiveTuneables
			iTuneableValue = GET_CURRENT_INVITE_FLOW_TUNEABLE(iSeed)
			CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - MAINTAIN_GLOBAL_INVITE_FLOW - Tuneable #", iSeed, ": ", GET_INVITE_FLOW_MISSION_TYPE_STRING(iTuneableValue))
			
			// Set the sender of the invite to Dom for Stunt Race invites
			IF iTuneableValue = ciINVITE_FLOW_STUNT_SERIES
				eInviteSenderCharacter = CHAR_DOM	
			ENDIF
			
			// Use the tuneable to select the appropriate mission
			g_iInviteFlowMissionRootId = GET_RANDOM_INVITE_FLOW_MISSION(iTuneableValue, mpMissionIdData)
			CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [INVITE_FLOW] [RE] - MAINTAIN_GLOBAL_INVITE_FLOW - Selected mission. Root Content ID: ", g_iInviteFlowMissionRootId)	
		ENDIF

	ENDIF
ENDPROC


/// PURPOSE:
///    Checks to see if the supplied adversary mode is set to be promoted through invites and help text
/// PARAMS:
///    adversaryType - The type of adversary mode that should be checked (g_eContactMissionAdversaryTypes) e.g. CM_ADVERSARY_TYPE_EXTRACTION
/// RETURNS:
///    Returns a boolean tunable value, depending on if the adversary mode is to be promoted (usually because it is a new mode)
FUNC BOOL IS_ADVERSARY_PROMO_PERIOD_TUNEABLE_SET(g_eContactMissionAdversaryTypes adversaryType)
	SWITCH adversaryType
		CASE CM_ADVERSARY_TYPE_EBC
		CASE CM_ADVERSARY_TYPE_RUNNING_BACK					RETURN g_sMPTunables.bTURN_ON_TH
		
		CASE CM_ADVERSARY_TYPE_EXTRACTION			 		IF g_sMPTunables.bTURN_ON_EXTRACTION OR g_sMPTunables.bTURN_ON_EXTRACTION_DROPZONE		RETURN TRUE 	ENDIF BREAK
		CASE CM_ADVERSARY_TYPE_DROP_ZONE					IF g_sMPTunables.bTURN_ON_DROPZONE OR g_sMPTunables.bTURN_ON_EXTRACTION_DROPZONE		RETURN TRUE 	ENDIF BREAK
		
		CASE CM_ADVERSARY_TYPE_BE_MY_VALENTINE			 	RETURN g_sMPTunables.bTURN_ON_BE_MY_VALENTINE
		CASE CM_ADVERSARY_TYPE_DAVID_AND_GOLIATH			RETURN g_sMPTunables.TURN_ON_DAVIDS_VS_GOLIATH
		
		CASE CM_ADVERSARY_TYPE_SUMO							RETURN g_sMPTunables.bTURN_ON_SUMO
		CASE CM_ADVERSARY_TYPE_RUGBY						RETURN g_sMPTunables.bTURN_ON_INCH_BY_INCH
		CASE CM_ADVERSARY_TYPE_IN_AND_OUT					RETURN g_sMPTunables.bTURN_ON_IN_AND_OUT
		
		CASE CM_ADVERSARY_TYPE_POWER_PLAY					RETURN g_sMPTunables.bTURN_ON_POWER_PLAY	
		CASE CM_ADVERSARY_TYPE_TRADE_PLACES					RETURN g_sMPTunables.bTURN_ON_TRADE_PLACES
		
		CASE CM_ADVERSARY_TYPE_ENTOURAGE					RETURN g_sMPTunables.bTURN_ON_ENTOURAGE				
		
		CASE CM_ADVERSARY_TYPE_DEADLINE	    				 RETURN (g_sMPTunables.bTURN_ON_DEADLINE OR g_sMPTunables.bTURN_ON_DEADLINER_JOB)
		CASE CM_ADVERSARY_TYPE_LOSTVDAMNED  				 RETURN g_sMPTunables.bTURN_ON_LOSTVDAMNED
		CASE CM_ADVERSARY_TYPE_SLIPSTREAM   				 RETURN g_sMPTunables.bTURN_ON_SLIPSTREAM
		CASE CM_ADVERSARY_TYPE_KILL_QUOTA   				 RETURN g_sMPTunables.bTURN_ON_KILL_QUOTA
		
		CASE CM_ADVERSARY_TYPE_VEHICLE_VENDETTA 			RETURN g_sMPTunables.bTURN_ON_VEHICLEVENDETTA
		CASE CM_ADVERSARY_TYPE_TURF_WAR						RETURN g_sMPTunables.bTURN_ON_TURFWARS
		CASE CM_ADVERSARY_TYPE_JUGGERNAUT					RETURN g_sMPTunables.bTURN_ON_JUGGERNAUT
		CASE CM_ADVERSARY_TYPE_POINTLESS					RETURN g_sMPTunables.bTURN_ON_POINTLESS
		
		CASE CM_ADVERSARY_TYPE_TINY_RACERS 					RETURN g_sMPTunables.bTURN_ON_TOPDOWN
		CASE CM_ADVERSARY_TYPE_LANDGRAB						RETURN g_sMPTunables.bTURN_ON_LANDGRAB
		CASE CM_ADVERSARY_TYPE_RESURRECTION					RETURN g_sMPTunables.bTURN_ON_RESURRECTION
		
		CASE CM_ADVERSARY_TYPE_OVERTIME						RETURN g_sMPTunables.bTURNON_GR_ADVMODE1
		CASE CM_ADVERSARY_TYPE_DAWN_RAID					RETURN g_sMPTunables.bTURNON_GR_ADVMODE2
		CASE CM_ADVERSARY_TYPE_POWER_MAD					RETURN g_sMPTunables.bTURNON_GR_ADVMODE3
		CASE CM_ADVERSARY_TYPE_OVERTIME_RUMBLE				RETURN g_sMPTunables.bTURNON_GR_ADVMODE4
		
		CASE CM_ADVERSARY_TYPE_VEHICLE_WARFARE				RETURN g_sMPTunables.bTURNON_SM_ADVMODE1
		CASE CM_ADVERSARY_TYPE_BOMBUSHKA_RUN				RETURN g_sMPTunables.bTURNON_SM_ADVMODE2		
		CASE CM_ADVERSARY_TYPE_STOCKPILE					RETURN g_sMPTunables.bTURNON_SM_ADVMODE3	
		CASE CM_ADVERSARY_TYPE_CONDEMNED					RETURN g_sMPTunables.bTURNON_SM_ADVMODE4	
		CASE CM_ADVERSARY_TYPE_AIR_SHOOTOUT					RETURN g_sMPTunables.bTURNON_SM_ADVMODE5	
		
		CASE CM_ADVERSARY_TYPE_HARD_TARGET					RETURN g_sMPTunables.bTURNON_H2_ADVMODE1
		CASE CM_ADVERSARY_TYPE_SLASHERS						RETURN g_sMPTunables.bTURNON_H2_ADVMODE2
		CASE CM_ADVERSARY_TYPE_HOSTILE_TAKEOVER				RETURN g_sMPTunables.bTURNON_H2_ADVMODE3
		CASE CM_ADVERSARY_TYPE_AIR_QUOTA					RETURN g_sMPTunables.bTURNON_H2_ADVMODE4
		
		CASE CM_ADVERSARY_TYPE_VENETIAN_JOB					RETURN g_sMPTunables.bTURNON_AC_ADVMODE1
		CASE CM_ADVERSARY_TYPE_TRAP_DOOR					RETURN g_sMPTunables.bTURNON_AC_ADVMODE2
		
		CASE CM_ADVERSARY_TYPE_SUMO_REMIX					RETURN g_sMPTunables.bTURNON_BH_SR
		CASE CM_ADVERSARY_TYPE_HUNTING_PACK_REMIX			RETURN g_sMPTunables.bTURNON_BH_HPR
		CASE CM_ADVERSARY_TYPE_TRADING_PLACES_REMIX			RETURN g_sMPTunables.bTURNON_BH_TPR
		CASE CM_ADVERSARY_TYPE_RUNNING_BACK_REMIX			RETURN g_sMPTunables.bTURNON_BH_RBR
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks to see if the supplied adversary mode is set to be promoted through invites and help text after the promotional period is over
/// PARAMS:
///    adversaryType - The type of adversary mode that should be checked (g_eContactMissionAdversaryTypes) e.g. CM_ADVERSARY_TYPE_EXTRACTION
/// RETURNS:
///    Returns a boolean tunable value, depending on if the adversary mode is to be promoted still (usually because it was a new mode but is passed the promotional period)
FUNC BOOL IS_ADVERSARY_AFTER_PROMO_TUNEABLE_SET(g_eContactMissionAdversaryTypes adversaryType)
	
	SWITCH adversaryType
		CASE CM_ADVERSARY_TYPE_EBC
		CASE CM_ADVERSARY_TYPE_RUNNING_BACK					RETURN g_sMPTunables.bTurnOnThanksgivingAfterPromoInvite
		
		CASE CM_ADVERSARY_TYPE_EXTRACTION			 		IF g_sMPTunables.bTurnOnExtractionAfterPromoInvite OR g_sMPTunables.bTurnOnExtractionDropzoneAfterPromoInvite	RETURN TRUE 	ENDIF 	BREAK
		CASE CM_ADVERSARY_TYPE_DROP_ZONE					IF g_sMPTunables.bTurnOnDropZoneAfterPromoInvite OR g_sMPTunables.bTurnOnExtractionDropzoneAfterPromoInvite		RETURN TRUE 	ENDIF 	BREAK
		
		CASE CM_ADVERSARY_TYPE_BE_MY_VALENTINE			 	RETURN g_sMPTunables.bTurnOnBeMyValentineAfterPromoInvite
		CASE CM_ADVERSARY_TYPE_DAVID_AND_GOLIATH			RETURN g_sMPTunables.bTurnOnDavidsVsGoliathAfterPromoInvite
		
		CASE CM_ADVERSARY_TYPE_SUMO							RETURN g_sMPTunables.bTURN_ON_SUMO_POST_PROMOTION
		CASE CM_ADVERSARY_TYPE_RUGBY						RETURN g_sMPTunables.bTURN_ON_INCH_BY_INCH_POST_PROMOTION
		CASE CM_ADVERSARY_TYPE_IN_AND_OUT					RETURN g_sMPTunables.bTURN_ON_IN_AND_OUT_POST_PROMOTION
		
		CASE CM_ADVERSARY_TYPE_POWER_PLAY					RETURN g_sMPTunables.bTURN_ON_POWER_PLAY_POST_PROMOTION
		CASE CM_ADVERSARY_TYPE_TRADE_PLACES					RETURN g_sMPTunables.bTURN_ON_TRADE_PLACES_POST_PROMOTION
		
		CASE CM_ADVERSARY_TYPE_ENTOURAGE					RETURN g_sMPTunables.bTURN_ON_ENTOURAGE_POST_PROMOTION				
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Loops through all Adversary Modes and checks to see if any of them are to be promoted during or after the promo period. Any that have to be promoted for
///    either reason is set in a corresponding bitset passed in by reference. We collect all active modes so that if there are multiple to be promoted we can select one randomly, rather
///    than always select whichever comes first.
/// PARAMS:
///    iPromoPeriodAdversaryBitset - Bitset used to tell if an adversary mode should be promoted during the promotional period
///    iPromoExpiredAdversaryBitset - Bitset used to tell if an adversary mode should be promoted still after the promotional period
PROC GET_ACTIVE_ADVERSARY_TUNEABLES(INT& iPromoPeriodAdversaryBitset[ADVERSARY_PROMO_ARRAY_SIZE], INT& iPromoExpiredAdversaryBitset[ADVERSARY_PROMO_ARRAY_SIZE])
	g_eContactMissionAdversaryTypes adversaryType
	
	INT i
	FOR i = (ENUM_TO_INT(eAdversaryReminder)+1) TO (ENUM_TO_INT(CM_ADVERSARY_TYPE_MAX)-1) STEP 1
		adversaryType = INT_TO_ENUM(g_eContactMissionAdversaryTypes, i)
		
		INT iBitSet = (i / 32)
		INT iBitVal = (i % 32)
		
		// Check if the adversary mode should be promoted as it's new
		IF IS_ADVERSARY_PROMO_PERIOD_TUNEABLE_SET(adversaryType)
			SET_BIT(iPromoPeriodAdversaryBitset[iBitSet], iBitVal)
			CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - GET_ACTIVE_ADVERSARY_TUNEABLES - ", Get_Contact_Mission_Adversary_Debug_Name(adversaryType) ," during promo period is active. Setting bit ", iBitVal, " in iPromoPeriodAdversaryBitset[", iBitSet,"].")
		ENDIF
		
		// Check if the adversary mode should still be promoted even after the promotional period
		IF IS_ADVERSARY_AFTER_PROMO_TUNEABLE_SET(adversaryType)
			SET_BIT(iPromoExpiredAdversaryBitset[iBitSet], iBitVal)
			CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - GET_ACTIVE_ADVERSARY_TUNEABLES - ", Get_Contact_Mission_Adversary_Debug_Name(adversaryType) ," after promo period is active. Setting bit ", iBitVal, " in iPromoExpiredAdversaryBitset[", iBitSet,"].")
		ENDIF
	ENDFOR
ENDPROC

/// PURPOSE:
///    Loops through the supplied bitset and gathers all active Adversary Modes into an array. From those active modes, a random one is returned.
///    This is to promote modes evenly if more than one is set to be promoted at a time.
/// PARAMS:
///    iAdversaryBitset - Bitset that is filled in GET_ACTIVE_ADVERSARY_TUNEABLES
/// RETURNS:
///    Returns a random adversary mode from those being promoted
FUNC g_eContactMissionAdversaryTypes GET_RANDOM_ADVERSARY_FROM_ACTIVE_TUNEABLES(INT& iAdversaryBitset[ADVERSARY_PROMO_ARRAY_SIZE])
	INT iActiveAdversary[CM_ADVERSARY_TYPE_MAX]
	
	CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - GET_RANDOM_ADVERSARY_FROM_ACTIVE_TUNEABLES - Selecting random adversary mode from those active...")
	
	
	INT iIndex, iCount
	FOR iIndex = (ENUM_TO_INT(eAdversaryReminder)+1) TO (ENUM_TO_INT(CM_ADVERSARY_TYPE_MAX)-1) STEP 1
		// Initialise the array to invalid
		IF iIndex < ENUM_TO_INT(CM_ADVERSARY_TYPE_MAX)
			iActiveAdversary[iIndex] = -2
		ENDIF
		
		// If the adversary mode was set as active during GET_ACTIVE_ADVERSARY_TUNEABLES, add it to the local array of active modes
		INT iBitSet = (iIndex / 32)
		INT iBitVal = (iIndex % 32)
		IF IS_BIT_SET(iAdversaryBitset[iBitSet], iBitVal)
			iActiveAdversary[iCount] = iIndex
			CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - GET_RANDOM_ADVERSARY_FROM_ACTIVE_TUNEABLES - ", Get_Contact_Mission_Adversary_Debug_Name(INT_TO_ENUM(g_eContactMissionAdversaryTypes, iIndex)) ," added to pool of possible of adversary mode invites to be sent.")
			
			iCount++
		ENDIF
	ENDFOR
	
	// Return a random adversary mode from the array of active modes
	RETURN INT_TO_ENUM(g_eContactMissionAdversaryTypes, iActiveAdversary[NETWORK_GET_RANDOM_INT_RANGED(0, iCount-1)])
ENDFUNC

FUNC BOOL ARE_ANY_SERIES_PROMO_TUNEABLES_SET(INT& iPromoSeriesValue)
	
	// Target Assault Series
	IF g_sMPTunables.bTURNON_TARGET_ASSAULT
		iPromoSeriesValue = ciINVITE_FLOW_TARGET_ASSAULT
		RETURN TRUE
	ENDIF
	
	// Special Races Series
	IF g_sMPTunables.bTURNON_AC_SPECIAL
		iPromoSeriesValue = ciINVITE_FLOW_SPECIAL_RACES
		RETURN TRUE
	ENDIF
	
	// Transform Races Series
	IF g_sMPTunables.bTURNON_AC_TRANSFORM
		iPromoSeriesValue = ciINVITE_FLOW_TRANSFORM
		RETURN TRUE
	ENDIF
	
	// SAS Races Series (Repurposed Hotring)
	IF g_sMPTunables.bTURNON_AW_RACES
		iPromoSeriesValue = ciINVITE_FLOW_SASS
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ADVERSARY_PROMO_BITSET_ARRAY_EMPTY(INT& iAdversaryBitset[ADVERSARY_PROMO_ARRAY_SIZE])
	INT iIndex
	REPEAT ADVERSARY_PROMO_ARRAY_SIZE iIndex
		IF iAdversaryBitset[iIndex] != 0
			RETURN FALSE
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC


SCRIPT_TIMER stTimerNewAdversaryInvites
BOOL bAdversaryHelpProcessed
/// PURPOSE:
///    Sends an invite to the player on booting in or changing session depending on what new Adversary Modes are currently being promoted.
///    See the follow functions above for more specific detail:
///    IS_ADVERSARY_PROMO_PERIOD_TUNEABLE_SET, IS_ADVERSARY_AFTER_PROMO_TUNEABLE_SET, GET_ACTIVE_ADVERSARY_TUNEABLES, 
///    GET_RANDOM_ADVERSARY_FROM_ACTIVE_TUNEABLES, GET_RANDOM_INVITE_FLOW_MISSION
///    See url:bugstar:2673074 for further detail
PROC MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP()

	// If we've already sent an invite this session
	IF bAdversaryHelpProcessed
		EXIT
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		EXIT
	ENDIF
	
	// Exit if the local player is on a tutorial
	IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
	OR IS_LOCAL_PLAYER_DOING_ANY_AMBIENT_TUTORIAL()
	OR IS_THIS_A_SOLO_SESSION()
		#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 10) = 0
			IF IS_THIS_A_SOLO_SESSION()
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE]  - MAINTAIN_GLOBAL_INVITE_FLOW - Local player is in a solo session. Exiting.")
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE]  - MAINTAIN_GLOBAL_INVITE_FLOW - Local player is on a tutorial. Exiting.")
			ENDIF
		ENDIF
		#ENDIF
		EXIT
	ENDIF
	
	INT iPromoPeriodAdversaryBitset[ADVERSARY_PROMO_ARRAY_SIZE]
	INT iPromoExpiredAdversaryBitset[ADVERSARY_PROMO_ARRAY_SIZE]
	INT iPromoSeriesValue = -1

	IF NOT g_sMPTunables.bForceFeaturedAdversaryInvite
		// Check to see if any Adversary Modes are currently being promoted either during or after the promo period
		GET_ACTIVE_ADVERSARY_TUNEABLES(iPromoPeriodAdversaryBitset, iPromoExpiredAdversaryBitset)
	ENDIF
	
	// If at least one adversary mode is being promoted during or after the promo the period
	IF ARE_ANY_SERIES_PROMO_TUNEABLES_SET(iPromoSeriesValue)
	OR NOT IS_ADVERSARY_PROMO_BITSET_ARRAY_EMPTY(iPromoPeriodAdversaryBitset)
	OR NOT IS_ADVERSARY_PROMO_BITSET_ARRAY_EMPTY(iPromoExpiredAdversaryBitset)
	OR g_sMPTunables.bForceFeaturedAdversaryInvite
	
		IF GET_PLAYER_RANK(PLAYER_ID()) >= g_sMPTunables.iAdModeInvitesRank
			IF HAS_NET_TIMER_EXPIRED(stTimerNewAdversaryInvites, 1000*60)
				IF IS_IT_SAFE_TO_SEND_ADVERSARY_INVITE()
					// This is for passing into GET_RANDOM_INVITE_FLOW_MISSION, which can be used for contact missions also.
					// In this instance we don't need to worry about it, so passing these in to please the compiler 
					MP_MISSION_ID_DATA unusedData

					// If there's at least one brand new Adversary Mode or Series, promote it
					IF iPromoSeriesValue != (-1)
					OR NOT IS_ADVERSARY_PROMO_BITSET_ARRAY_EMPTY(iPromoPeriodAdversaryBitset)
					OR g_sMPTunables.bForceFeaturedAdversaryInvite
						
						INT iMission
						g_eContactMissionAdversaryTypes adversaryType
						
						IF g_sMPTunables.bForceFeaturedAdversaryInvite
							iMission = GET_RANDOM_INVITE_FLOW_MISSION(g_sMPTunables.iForceFeaturedAdversaryInviteType, unusedData)
							CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - bForceFeaturedAdversaryInvite - Tunable value: ", g_sMPTunables.iForceFeaturedAdversaryInviteType, " - Type: ", GET_INVITE_FLOW_MISSION_TYPE_STRING(g_sMPTunables.iForceFeaturedAdversaryInviteType)," - Selected Mission ID: ", iMission)
						ELIF iPromoSeriesValue != (-1)
							iMission = GET_RANDOM_INVITE_FLOW_MISSION(iPromoSeriesValue, unusedData)
							CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - iPromoSeriesValue - ", GET_INVITE_FLOW_MISSION_TYPE_STRING(iPromoSeriesValue)," - Selected Mission ID: ", iMission)
						ELSE
							// Select a random Adversary Mode, in case there is more than one being actively promoted, then select a random variant of the Adversary Mode
							adversaryType = GET_RANDOM_ADVERSARY_FROM_ACTIVE_TUNEABLES(iPromoPeriodAdversaryBitset)
							CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - iPromoPeriodAdversaryBitset - ", Get_Contact_Mission_Adversary_Debug_Name(adversaryType), " selected from random active tuneables.")
						
							iMission = GET_RANDOM_INVITE_FLOW_MISSION(ENUM_TO_INT(adversaryType), unusedData)
							CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - iPromoPeriodAdversaryBitset - Selected Mission ID: ", iMission)
						ENDIF
						
						// Send an invite for the selected mission
						IF SET_MESSAGE_INVITE_FROM_ROOT_CONTENT_ID_HASH(iMission, DEFAULT, TRUE)
							// If the player has just booted in, send the help text, otherwise just the invite
							IF NOT g_bGotNewAdversaryHelp
								
								IF g_sMPTunables.bForceFeaturedAdversaryInvite
									PRINT_HELP("HVS_INVITE")
									CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - bForceFeaturedAdversaryInvite - Player just booted in. Sending generic help text.")
								ELSE
									TEXT_LABEL_15 tlHelpText = Get_Adversay_Base_Help_Text(adversaryType, iPromoSeriesValue)
			
									IF iPromoSeriesValue != (-1)
										PRINT_HELP(tlHelpText)
										CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - iPromoSeriesValue - Player just booted in. Sending help text for ", GET_INVITE_FLOW_MISSION_TYPE_STRING(iPromoSeriesValue))
									ELSE	
										STRING strAdversaryName = Get_Contact_Mission_Adversary_Reminder_Name(adversaryType)
										IF NOT IS_STRING_NULL_OR_EMPTY(strAdversaryName)
											PRINT_HELP_WITH_STRING(tlHelpText, strAdversaryName) // "A new Adversary Mode, ~a~, is now available"
										ELSE
											PRINT_HELP(tlHelpText)
										ENDIF
										CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - iPromoPeriodAdversaryBitset - Player just booted in. Sending help text for ", Get_Contact_Mission_Adversary_Debug_Name(adversaryType))
									ENDIF
								ENDIF
								
								g_bGotNewAdversaryHelp = TRUE
							ENDIF

							// Bump the timer for Keith's invite system to 5 minutes if it's less than that
							Delay_CM_Adversary_Invite_System()
							
							// Mark that we've sent an invite already this session
							bAdversaryHelpProcessed = TRUE
							
							#IF IS_DEBUG_BUILD
							IF g_sMPTunables.bForceFeaturedAdversaryInvite
								CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - bForceFeaturedAdversaryInvite - Invite sent for ", GET_INVITE_FLOW_MISSION_TYPE_STRING(g_sMPTunables.iForceFeaturedAdversaryInviteType))
							ELSE
								IF iPromoSeriesValue != (-1)
									CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - iPromoSeriesValue - Invite sent for ", GET_INVITE_FLOW_MISSION_TYPE_STRING(iPromoSeriesValue))
								ELSE	
									CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - iPromoPeriodAdversaryBitset - Invite sent for ", Get_Contact_Mission_Adversary_Debug_Name(adversaryType))
								ENDIF
							ENDIF
							#ENDIF
						ELSE
							bAdversaryHelpProcessed = TRUE
							
							// Here to make sure we're not getting issues from people running candidate missions while using this, as SET_MESSAGE_INVITE_FROM_ROOT_CONTENT_ID_HASH fails
							#IF IS_DEBUG_BUILD
							IF SHOULD_USE_ROCKSTAR_CANDIDATE_MISSIONS()
								CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - iPromoPeriodAdversaryBitset/iPromoSeriesValue - Invite failed while Rockstar Candidate missions are loaded. This is probably the issue.")
							ELSE
								CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - iPromoPeriodAdversaryBitset/iPromoSeriesValue - Invite failed.")
							ENDIF
							#ENDIF
						ENDIF
						
					// If it's a newish mode that has just left the promotional period
					// This means we only invite them when they boot in, rather than every time they change session like during the promotional period above
					ELIF NOT IS_ADVERSARY_PROMO_BITSET_ARRAY_EMPTY(iPromoExpiredAdversaryBitset)

						// If they've just booted in...
						IF NOT g_bGotNewAdversaryHelp
							// Select a random Adversary Mode, in case there is more than one that has just left the promotional period, then select a random variant of the Adversary Mode
							g_eContactMissionAdversaryTypes adversaryType = GET_RANDOM_ADVERSARY_FROM_ACTIVE_TUNEABLES(iPromoExpiredAdversaryBitset)
							CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - iPromoExpiredAdversaryBitset - ", Get_Contact_Mission_Adversary_Debug_Name(adversaryType), " selected from random active tuneables.")
							INT iMission = GET_RANDOM_INVITE_FLOW_MISSION(ENUM_TO_INT(adversaryType), unusedData)
							CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - iPromoExpiredAdversaryBitset - Selected Mission ID: ", iMission)
							
							// Send an invite for the selected mission
							IF SET_MESSAGE_INVITE_FROM_ROOT_CONTENT_ID_HASH(iMission, DEFAULT, TRUE)
							
								// These stats are used simultaneously rather than linear. 1 = First time shown, 2 = Second time shown, 3 = Third time shown.
								INT iStatInt1 = GET_MP_INT_CHARACTER_STAT(MP_STAT_ADVERSARY_PROMO_HELP1)
								INT iStatInt2 = GET_MP_INT_CHARACTER_STAT(MP_STAT_ADVERSARY_PROMO_HELP2)
								INT iStatInt3 = GET_MP_INT_CHARACTER_STAT(MP_STAT_ADVERSARY_PROMO_HELP3)
								
								// Check to see if they've already seen the help text 3 times, if so don't show the help text ever again
								IF NOT IS_BIT_SET(iStatInt1, ENUM_TO_INT(adversaryType))
								OR NOT IS_BIT_SET(iStatInt2, ENUM_TO_INT(adversaryType))
								OR NOT IS_BIT_SET(iStatInt3, ENUM_TO_INT(adversaryType))
								
									STRING strAdversaryName = Get_Contact_Mission_Adversary_Reminder_Name(adversaryType)
									PRINT_HELP_WITH_STRING("ADVERSARY_AVAIL", strAdversaryName) // The Adversary Mode, ~a~, is available.
			
									IF IS_BIT_SET(iStatInt2, ENUM_TO_INT(adversaryType))
										SET_BIT(iStatInt3, ENUM_TO_INT(adversaryType))
										SET_MP_INT_CHARACTER_STAT(MP_STAT_ADVERSARY_PROMO_HELP3, iStatInt3)
										
										CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - iPromoExpiredAdversaryBitset - iAdversaryModePromotionHelp3 set. Help text will not be shown anymore.")
									ELIF IS_BIT_SET(iStatInt1, ENUM_TO_INT(adversaryType))
										SET_BIT(iStatInt2, ENUM_TO_INT(adversaryType))
										SET_MP_INT_CHARACTER_STAT(MP_STAT_ADVERSARY_PROMO_HELP2, iStatInt2)
										
										CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - iPromoExpiredAdversaryBitset - iAdversaryModePromotionHelp2 set. One help text display remaining on boot.")
									ELSE
										SET_BIT(iStatInt1, ENUM_TO_INT(adversaryType))
										SET_MP_INT_CHARACTER_STAT(MP_STAT_ADVERSARY_PROMO_HELP1, iStatInt1)
										
										CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - iPromoExpiredAdversaryBitset - iAdversaryModePromotionHelp1 set. Two help text display remaining on boot.")
									ENDIF
									
									CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - iPromoExpiredAdversaryBitset - Player just booted in. Sending help text for ", Get_Contact_Mission_Adversary_Debug_Name(adversaryType))
								ELSE
									CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - iPromoExpiredAdversaryBitset - Help text for ", Get_Contact_Mission_Adversary_Debug_Name(adversaryType), " has already been shown three times. Not displaying.")
								ENDIF
								
								// Bump the timer for Keith's invite system to 5 minutes if it's less than that
								Delay_CM_Adversary_Invite_System()
								
								// Mark that we've sent an invite already this boot
								g_bGotNewAdversaryHelp = TRUE
								bAdversaryHelpProcessed = TRUE
								CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - iPromoExpiredAdversaryBitset - Invite sent for ", Get_Contact_Mission_Adversary_Debug_Name(adversaryType))
							ELSE
								// Here to make sure we're not getting issues from people running candidate missions while using this, as SET_MESSAGE_INVITE_FROM_ROOT_CONTENT_ID_HASH fails
								#IF IS_DEBUG_BUILD
								IF SHOULD_USE_ROCKSTAR_CANDIDATE_MISSIONS()
									CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - iPromoExpiredAdversaryBitset - Invite failed while Rockstar Candidate missions are loaded. This is probably the issue.")
								ELSE
									CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - iPromoExpiredAdversaryBitset - Invite failed.")
								ENDIF
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		g_bGotNewAdversaryHelp = TRUE
		bAdversaryHelpProcessed = TRUE
		
		IF IS_ADVERSARY_PROMO_BITSET_ARRAY_EMPTY(iPromoPeriodAdversaryBitset)
			CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - iPromoPeriodAdversaryBitset = 0")
		ENDIF
		IF IS_ADVERSARY_PROMO_BITSET_ARRAY_EMPTY(iPromoExpiredAdversaryBitset)
			CPRINTLN(DEBUG_NET_AMBIENT, "[FM] [ADVERSARY_PROMO] [RE] - MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP - iPromoExpiredAdversaryBitset = 0")
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Find the next reminder invite we need to process. Tunable will be non-zero
PROC ADVERSARY_MODE_FIND_INVITE()
	
	INT i
	INT iAdversaryOccurrenceValue
	g_eContactMissionAdversaryTypes adversaryType
	FOR i = (ENUM_TO_INT(eAdversaryReminder)+1) TO (ENUM_TO_INT(CM_ADVERSARY_TYPE_MAX)-1) STEP 1
		
		adversaryType = INT_TO_ENUM(g_eContactMissionAdversaryTypes, i)
		
		IF Is_Contact_Mission_Adversary_Type_Active(adversaryType)
			
			iAdversaryOccurrenceValue = Get_Contact_Mission_Adversary_Reminder_Tunable(adversaryType)
			PRINTLN("[Adversary][Reminder] ADVERSARY_MODE_FIND_INVITE - Mode: ", Get_Contact_Mission_Adversary_Debug_Name(adversaryType), ", tunable val = ", iAdversaryOccurrenceValue)
			
			IF (iAdversaryOccurrenceValue & ciADVERSARY_REMINDER_BIT_OP2) != 0
				
				PRINTLN("[Adversary][Reminder] ADVERSARY_MODE_FIND_INVITE - Mode: ", Get_Contact_Mission_Adversary_Debug_Name(adversaryType), ", requires reminder. eAdversaryReminderState = ADVERSARY_REMINDER_STATE_VALIDATE")

				// We have the possibility to send
				eAdversaryReminder = adversaryType
				eAdversaryReminderState = ADVERSARY_REMINDER_STATE_VALIDATE
				
				// Exit here, we have not processed all our adversary modes
				EXIT
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[Adversary][Reminder] ADVERSARY_MODE_FIND_INVITE - Mode: ", Get_Contact_Mission_Adversary_Debug_Name(adversaryType), " is not active")
		#ENDIF			
		ENDIF
	ENDFOR
	
	// No more adversary modes to check, set we have ran our check this boot.
	PRINTLN("[Adversary][Reminder] Completed loop of all Adversary Modes. Stop this boot: g_bSentAdversaryModeReminder = TRUE")
	g_bSentAdversaryModeReminder = TRUE
ENDPROC

/// PURPOSE:
///    This will validate the reminder invite we are attempting to send.
///    1. Do we need to reset our stat before sending?
///    2. Have we sent all the reminders indicated by tunable?
PROC ADVERSARY_MODE_VALIDATE_INVITE()
	
	INT iTunableValue = Get_Contact_Mission_Adversary_Reminder_Tunable(eAdversaryReminder)
	MP_INT_STATS adversaryStat = Get_Contact_Mission_Adversary_Reminder_Stat(eAdversaryReminder)
	
	// Bail if stat is invalid
	IF adversaryStat = INT_TO_ENUM(MP_INT_STATS, -1)
		PRINTLN("[Adversary][Reminder] ADVERSARY_MODE_VALIDATE_INVITE - Invalid stat for mode: ", Get_Contact_Mission_Adversary_Debug_Name(eAdversaryReminder))
		eAdversaryReminderState = ADVERSARY_REMINDER_STATE_FIND
		EXIT
	ENDIF
	
	// Is a bit reset required?
	IF Is_Adversary_Reminder_Tunable_Requesting_A_Reset(eAdversaryReminder)	
	
		PRINTLN("[Adversary][Reminder] ADVERSARY_MODE_VALIDATE_INVITE - Mode: ", Get_Contact_Mission_Adversary_Debug_Name(eAdversaryReminder), " tunable requires a RESET")
	
		INT iNewValue = iTunableValue & ciADVERSARY_REMINDER_BIT_OP			// AND bitwise operation with 00000..1111
		SET_MP_INT_CHARACTER_STAT(adversaryStat, iNewValue)
	ENDIF
	
	INT i
	INT iStatValue = GET_MP_INT_CHARACTER_STAT(adversaryStat)
	
	FOR i = 0 TO 27 STEP 1
		
		IF IS_BIT_SET(iTunableValue, i)
		AND NOT IS_BIT_SET(iStatValue, i)
			
			PRINTLN("[Adversary][Reminder] ADVERSARY_MODE_VALIDATE_INVITE - Mode: ", Get_Contact_Mission_Adversary_Debug_Name(eAdversaryReminder), " reminder for bit: ", i, " has not been processed.")
			
			iAdversaryReminderBit = i
			
			// We want to trigger invite
			START_NET_TIMER(stAdversaryReminder)
			//bump the timer to 5 minutes if it's less than that
			Delay_CM_Adversary_Invite_System()
			eAdversaryReminderState = ADVERSARY_REMINDER_STATE_SEND
			
			PRINTLN("[Adversary][Reminder] ADVERSARY_MODE_FIND_INVITE - Mode: ", Get_Contact_Mission_Adversary_Debug_Name(eAdversaryReminder), " should send reminder. eAdversaryReminderState = ADVERSARY_REMINDER_STATE_SEND")
			EXIT
		ENDIF
	ENDFOR
	
	PRINTLN("[Adversary][Reminder] ADVERSARY_MODE_VALIDATE_INVITE - We have processed all invites for mode ", Get_Contact_Mission_Adversary_Debug_Name(eAdversaryReminder), ", eAdversaryReminderState = ADVERSARY_REMINDER_STATE_FIND")
	eAdversaryReminderState = ADVERSARY_REMINDER_STATE_FIND
ENDPROC

/// PURPOSE:
///    Send the invite to the player when safe to do so. Updates local stat while also displaying help text
PROC ADVERSARY_MODE_SEND_INVITE()
	
	IF HAS_NET_TIMER_EXPIRED(stAdversaryReminder, 1000*60)
		IF IS_IT_SAFE_TO_SEND_ADVERSARY_INVITE()
			
			MP_INT_STATS adversaryStat = Get_Contact_Mission_Adversary_Reminder_Stat(eAdversaryReminder)
			
			// Bail if stat is invalid
			IF adversaryStat = INT_TO_ENUM(MP_INT_STATS, -1)
				eAdversaryReminderState = ADVERSARY_REMINDER_STATE_FIND
				EXIT
			ENDIF
			
			PRINTLN("[Adversary][Reminder] ADVERSARY_MODE_SEND_INVITE - Mode: ", Get_Contact_Mission_Adversary_Debug_Name(eAdversaryReminder), " will send an invite.")

			// Get the stat value, set the bit and save again.
			INT iStatValue = GET_MP_INT_CHARACTER_STAT(adversaryStat)
			SET_BIT(iStatValue, iAdversaryReminderBit)
			SET_MP_INT_CHARACTER_STAT(adversaryStat, iStatValue)
			
			// Display the help text
			STRING strAdversaryName = Get_Contact_Mission_Adversary_Reminder_Name(eAdversaryReminder)
			PRINT_HELP_WITH_STRING("ADV_REMINDER", strAdversaryName)
			
			// Send the invite for the Adversary Mode
			Setup_Adversary_Invite_And_Send_Invite(eAdversaryReminder)
			
			//bump the timer to 5 minutes if it's less than that
			Delay_CM_Adversary_Invite_System()
			
			PRINTLN("[Adversary][Reminder] ADVERSARY_MODE_SEND_INVITE - Mode: ", Get_Contact_Mission_Adversary_Debug_Name(eAdversaryReminder), " reminder has been sent. g_bSentAdversaryModeReminder = TRUE")
			PRINTLN("[Adversary][Reminder] ADVERSARY_MODE_SEND_INVITE - Mode: ", Get_Contact_Mission_Adversary_Debug_Name(eAdversaryReminder), ", eAdversaryReminderState = ADVERSARY_REMINDER_STATE_FIND")
			
			// Flag the invite is sent to exit this logic
			g_bSentAdversaryModeReminder = TRUE
			eAdversaryReminderState = ADVERSARY_REMINDER_STATE_FIND
			eAdversaryReminder = CM_ADVERSARY_TYPE_NULL			
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks at the beginning of a session if we need to send an Adversary reminder. Once per boot
PROC MAINTAIN_ADVERSARY_MODE_REMINDERS()

	// If we have already sent the reminder or in an Activity session, bail
	IF g_bSentAdversaryModeReminder
	OR (NOT g_bGotNewAdversaryHelp AND NOT bAdversaryHelpProcessed)
		EXIT
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		EXIT
	ENDIF	
	
	SWITCH eAdversaryReminderState
		CASE ADVERSARY_REMINDER_STATE_FIND
			ADVERSARY_MODE_FIND_INVITE()
		BREAK
		
		CASE ADVERSARY_REMINDER_STATE_VALIDATE
			ADVERSARY_MODE_VALIDATE_INVITE()
		BREAK
		
		CASE ADVERSARY_REMINDER_STATE_SEND
			ADVERSARY_MODE_SEND_INVITE()
		BREAK
	ENDSWITCH	
ENDPROC

//Controlled by the event PROC PROCESS_EVENT_NETWORK_CLAN_INVITE_REQUEST_RECEIVED(INT iCount)
PROC MAINTAIN_FM_EVENT_DO_INVITE_MESSAGE()
	//If the bit set has a value
	IF g_FM_EVENT_INVITE_VARS.iEventInviteMessageBitSet != 0
	AND g_sMPTunables.bEnable_FM_Events_Adversary_Invite_System			// Tunable must be active to reintroduce these invites
		//if it's ok to send a message
		IF CAN_PRINT_ROCKSTAR_FEED_MSG()
		AND g_FM_EVENT_VARS.iFmAmbientEventCountDownType = FMMC_TYPE_INVALID 
		AND NOT FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
		AND IS_NET_PLAYER_OK(PLAYER_ID())
		AND NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
			INT iRootContentID, iLoop				
			
		//ciFM_EVENT_INVITE_MESSAGE_CROSS_LINE_I		
		//If the message is for Cross the Line I
			IF IS_BIT_SET(g_FM_EVENT_INVITE_VARS.iEventInviteMessageBitSet, ciFM_EVENT_INVITE_MESSAGE_CROSS_LINE_I)
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM AME] - MAINTAIN_FM_EVENT_DO_INVITE_MESSAGE - ciFM_EVENT_INVITE_MESSAGE_CROSS_LINE_I")
				iRootContentID = g_sMPTunables.iCrossTheLine[0]
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM AME] - MAINTAIN_FM_EVENT_DO_INVITE_MESSAGE - ciFM_EVENT_INVITE_MESSAGE_CROSS_LINE_I - iRootContentID = g_sMPTunables.iCrossTheLine[0] = ", iRootContentID)
		//ciFM_EVENT_INVITE_MESSAGE_CROSS_LINE_II
		//If the message is for Cross the Line II
			ELIF IS_BIT_SET(g_FM_EVENT_INVITE_VARS.iEventInviteMessageBitSet, ciFM_EVENT_INVITE_MESSAGE_CROSS_LINE_II)
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM AME] - MAINTAIN_FM_EVENT_DO_INVITE_MESSAGE - ciFM_EVENT_INVITE_MESSAGE_CROSS_LINE_II")
				iRootContentID = g_sMPTunables.iCrossTheLine[1]
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM AME] - MAINTAIN_FM_EVENT_DO_INVITE_MESSAGE - ciFM_EVENT_INVITE_MESSAGE_CROSS_LINE_II - iRootContentID = g_sMPTunables.iCrossTheLine[1] = ", iRootContentID)
		//ciFM_EVENT_INVITE_MESSAGE_CROSS_LINE_III
		//If the message is for Cross the Line III
			ELIF IS_BIT_SET(g_FM_EVENT_INVITE_VARS.iEventInviteMessageBitSet, ciFM_EVENT_INVITE_MESSAGE_CROSS_LINE_III)
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM AME] - MAINTAIN_FM_EVENT_DO_INVITE_MESSAGE - ciFM_EVENT_INVITE_MESSAGE_CROSS_LINE_III")
				iRootContentID = g_sMPTunables.iCrossTheLine[2]
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM AME] - MAINTAIN_FM_EVENT_DO_INVITE_MESSAGE - ciFM_EVENT_INVITE_MESSAGE_CROSS_LINE_III - iRootContentID = g_sMPTunables.iCrossTheLine[2] = ", iRootContentID)
		//ciFM_EVENT_INVITE_MESSAGE_HUNTING_PACK_II
		//If the message is for Hunting Pack II
			ELIF IS_BIT_SET(g_FM_EVENT_INVITE_VARS.iEventInviteMessageBitSet, ciFM_EVENT_INVITE_MESSAGE_HUNTING_PACK_II)
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM AME] - MAINTAIN_FM_EVENT_DO_INVITE_MESSAGE - ciFM_EVENT_INVITE_MESSAGE_HUNTING_PACK_II")
				iRootContentID = g_sMPTunables.iHuntingPack[1]
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM AME] - MAINTAIN_FM_EVENT_DO_INVITE_MESSAGE - ciFM_EVENT_INVITE_MESSAGE_HUNTING_PACK_II - iRootContentID = g_sMPTunables.iHuntingPack[1] = ", iRootContentID)
		//ciFM_EVENT_INVITE_MESSAGE_HUNTING_PACK_VII
		//If the message is for Hunting Pack VII
			ELIF IS_BIT_SET(g_FM_EVENT_INVITE_VARS.iEventInviteMessageBitSet, ciFM_EVENT_INVITE_MESSAGE_HUNTING_PACK_VII)
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM AME] - MAINTAIN_FM_EVENT_DO_INVITE_MESSAGE - ciFM_EVENT_INVITE_MESSAGE_HUNTING_PACK_VII")
				iRootContentID = g_sMPTunables.iHuntingPack[6]
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM AME] - MAINTAIN_FM_EVENT_DO_INVITE_MESSAGE - ciFM_EVENT_INVITE_MESSAGE_HUNTING_PACK_VII - iRootContentID = g_sMPTunables.iHuntingPack[6] = ", iRootContentID)
		//ciFM_EVENT_INVITE_MESSAGE_HUNTING_PACK_V
		//If the message is for Hunting Pack III
			ELIF IS_BIT_SET(g_FM_EVENT_INVITE_VARS.iEventInviteMessageBitSet, ciFM_EVENT_INVITE_MESSAGE_HUNTING_PACK_V)
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM AME] - MAINTAIN_FM_EVENT_DO_INVITE_MESSAGE - ciFM_EVENT_INVITE_MESSAGE_HUNTING_PACK_V")
				iRootContentID = g_sMPTunables.iHuntingPack[4]
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM AME] - MAINTAIN_FM_EVENT_DO_INVITE_MESSAGE - ciFM_EVENT_INVITE_MESSAGE_HUNTING_PACK_V - iRootContentID = g_sMPTunables.iHuntingPack[4] = ", iRootContentID)
			ENDIF
			
			//We've got a hash to loop for, find it
			INT iArray = -1
			REPEAT FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED iLoop
				IF iArray = -1
				AND iRootContentID = g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iLoop].iRootContentIdHash
					CPRINTLN(DEBUG_NET_AMBIENT, "[FM AME] - MAINTAIN_FM_EVENT_DO_INVITE_MESSAGE - iRootContentID = g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[].iRootContentIdHash - iArray  = ", iLoop)
					iArray = iLoop
					iLoop = FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED
				ENDIF
			ENDREPEAT
			
			//If we got it then send the message
			IF iArray >=0
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM AME] - MAINTAIN_FM_EVENT_DO_INVITE_MESSAGE - Store_Invite_Details_For_Joblist_From_NPC - ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArray].tlMissionName, " -  ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArray].tlName)
				Store_Invite_Details_For_Joblist_From_NPC(FMMC_TYPE_MISSION, FMMC_TYPE_MISSION_NEW_VS, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArray].vStartPos, FMMC_ROCKSTAR_CREATOR_ID, CHAR_MARTIN,
		        											g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArray].tlMissionName, "", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArray].tlName)
				//clear the bit set to get out of here
				g_FM_EVENT_INVITE_VARS.iEventInviteMessageBitSet = 0
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//Deal with the on call help text
PROC MAINTAIN_EVENT_BOOT_HELP_TEXT()
	//If we're starting an on call
	IF FAILED_SC_PLAYLIST_RANK_CHECK()
		//and we're able to print
		IF CAN_PRINT_ROCKSTAR_FEED_MSG()
			IF NOT HAS_FM_TRIGGER_TUT_BEEN_DONE()
			AND HAS_FM_RACE_TUT_BEEN_DONE()
			AND NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				PRINTLN("[TS] MAINTAIN_EVENT_BOOT_HELP_TEXT - not HAS_FM_TRIGGER_TUT_BEEN_DONE and HAS_FM_RACE_TUT_BEEN_DONE")
				EXIT
			ENDIF
			//then print to the ticker
			PRINT_TICKER("FM_EVENTLL_H")		
			//print to the logs
			PRINTLN("[TS] FAILED_SC_PLAYLIST_RANK_CHECK - PRINT_TICKER(\"FM_EVENTLL_H\")")//You can't play the event until you've completed the tutorial. 
			//set the stat to not see this again
			CLEAR_FAILED_SC_PLAYLIST_RANK_CHECK()
		ENDIF	
	ENDIF
ENDPROC

//// Prevent Duke o' Death being used outwith Hot Target
//PROC MAINTAIN_DUKE_DEATH_UNUSABLE_IN_FREEMODE()
//	IF g_sMPTunables.bHot_Target_Prevent_Vehicle_In_FM
//		IF FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) != FMMC_TYPE_MOVING_TARGET
//		AND NOT NETWORK_IS_ACTIVITY_SESSION()
//		AND NOT	IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_MOVING_TARGET)
//			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//			AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//				VEHICLE_INDEX viTemp
//				viTemp = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
//				IF GET_ENTITY_MODEL(viTemp) = DUKES2
//					SET_VEHICLE_DOORS_LOCKED(viTemp, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
//					SET_VEHICLE_AUTOMATICALLY_ATTACHES(viTemp, FALSE)
//					SET_VEHICLE_ENGINE_HEALTH(viTemp, 0.0)
//					SET_VEHICLE_ENGINE_ON(viTemp, FALSE, TRUE)
//					TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), viTemp)
//					FREEZE_ENTITY_POSITION(viTemp, TRUE)
//					SET_VEHICLE_STAYS_FROZEN_WHEN_CLEANED_UP(viTemp, TRUE)
//					SET_VEHICLE_AS_NO_LONGER_NEEDED(viTemp)
//					PRINTLN("[FREEMODE] - MAINTAIN_DUKE_DEATH_UNUSABLE_IN_FREEMODE - Player is in duke of death, rendering vehicle useless.")
//					DEBUG_PRINTCALLSTACK()
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC


//PROC MAINTAIN_VEHICLE_TYRE_SMOKE_LOOP()
//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//			VEHICLE_INDEX vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//			IF DOES_ENTITY_EXIST(vehID)
//			AND IS_VEHICLE_DRIVEABLE(vehID)
//			AND GET_PED_IN_VEHICLE_SEAT(vehID, VS_DRIVER) = PLAYER_PED_ID()
//				IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
//					INT iDecoratorValue
//					IF DECOR_EXIST_ON(vehID, "MPBitset")
//						iDecoratorValue = DECOR_GET_INT(vehID,"MPBitset")
//					ENDIF
//					IF IS_BIT_SET(iDecoratorValue,MP_DECORATOR_BS_LOOP_TYRE_SMOKE)
//						IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_tdTyreSmokeLoopTimer)) > 600
//							INT iR, iG, iB
//							GET_VEHICLE_TYRE_SMOKE_COLOR(vehID, iR, iG, iB)
//							
//							// Red
//							IF (iR = 226 AND iG = 6 AND iB = 6)
//								//-> White
//								SET_VEHICLE_TYRE_SMOKE_COLOR(vehID, 254, 254, 254)
//							// White
//							ELIF (iR = 255 AND iG = 255 AND iB = 255)
//							OR (iR = 254 AND iG = 254 AND iB = 254)
//								//-> Blue
//								SET_VEHICLE_TYRE_SMOKE_COLOR(vehID, 0, 174, 239)
//							// Blue
//							ELIF (iR = 0 AND iG = 174 AND iB = 239)
//								//-> Red
//								SET_VEHICLE_TYRE_SMOKE_COLOR(vehID, 226, 6, 6)
//							ENDIF
//							
//							g_tdTyreSmokeLoopTimer = GET_NETWORK_TIME()
//						ENDIF
//					ENDIF
//				ENDIF
//			ELSE
//				g_tdTyreSmokeLoopTimer = GET_NETWORK_TIME()
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

PROC MAINTAIN_SHOP_DISCOUNT_FOR_GOOD_PEER_REVIEW_TICKER()
	
	IF NOT HAS_NET_TIMER_STARTED(g_TransitionSessionNonResetVars.stShopDiscountPeerReviewTickerTimer)
		START_NET_TIMER(g_TransitionSessionNonResetVars.stShopDiscountPeerReviewTickerTimer)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(g_TransitionSessionNonResetVars.stShopDiscountPeerReviewTickerTimer, 15000)
			IF g_TransitionSessionNonResetVars.bShownGoodPeerReviewShopDiscountTicker
				IF NOT SHOULD_GIVE_SHOP_DISCOUNT()
					g_TransitionSessionNonResetVars.bShownGoodPeerReviewShopDiscountTicker = FALSE
				ENDIF
			ELSE
				IF SHOULD_GIVE_SHOP_DISCOUNT()
					PRINT_TICKER_WITH_INT("95PEER_SHOPDIS", g_sMPTunables.iShopDiscountPercentValue)
					g_TransitionSessionNonResetVars.bShownGoodPeerReviewShopDiscountTicker = TRUE
				ENDIF
			ENDIF
			RESET_NET_TIMER(g_TransitionSessionNonResetVars.stShopDiscountPeerReviewTickerTimer)
		ENDIF
	ENDIF
	
ENDPROC

PROC RUN_ARCHENEMY_IN_GAME_FEED()
//	STRING sArchenemyPlayer
	PLAYER_INDEX aPlayer
	TEXT_LABEL_63 tlArchenemy = "<C>" 
	IF g_b_DisplayedArchenemyInSessionFeed = FALSE
		IF NOT IS_TRANSITION_ACTIVE()
			IF NOT NETWORK_IS_ACTIVITY_SESSION()
				IF g_i_ArchenemyPlayer <> -1
					IF HAS_PLAYER_BEEN_IN_SESSION_THIS_LONG(DEFAULT_DISPLAY_DELAY *2)		
						aPlayer = INT_TO_PLAYERINDEX(g_i_ArchenemyPlayer)
						IF IS_NET_PLAYER_OK(PLAYER_ID())
						AND IS_NET_PLAYER_OK(aPlayer)
							IF NOT IS_PLAYER_SCTV(aPlayer)
							AND NOT IS_PLAYER_SCTV(PLAYER_ID())
							
								IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(aPlayer, PLAYER_ID())
									IF aPlayer <> PLAYER_ID()
										
										tlArchenemy += GET_PLAYER_NAME(aPlayer)
										tlArchenemy += "</C>"
									
										PRINTLN("[ARCHENEMY] RUN_ARCHENEMY_IN_GAME_FEED: tlArchenemy = ", tlArchenemy)

										// "Your Archenemy ~a~ is currently in this game. "
										BEGIN_TEXT_COMMAND_THEFEED_POST("FM_ARCH_TICK")
											ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tlArchenemy)
										END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)	
										 
										g_b_DisplayedArchenemyInSessionFeed = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC



PROC RUN_GOOD_BEHAVIOR_CASH_REWARD()

		IF NOT IS_TRANSITION_ACTIVE()
			IF NOT NETWORK_IS_ACTIVITY_SESSION()
				IF NOT IS_PLAYER_SCTV(PLAYER_ID())
					IF HAS_PLAYER_BEEN_IN_SESSION_THIS_LONG(DEFAULT_DISPLAY_DELAY *2)		
						IF IS_NET_PLAYER_OK(PLAYER_ID())
							MAINTAIN_GOOD_BEHAVIOUR_CASH_REWARD()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
ENDPROC


PROC DISPLAY_AIM_PREFERENCE_REVERT_FEED(SCRIPT_TIMER& aTimer)

	//0 = hardlock, 1 = softlock, 2 = freeaim
	
	IF HAS_PREVIOUS_AIM_STATE_CHANGED()
		IF NOT IS_TRANSITION_ACTIVE()
			IF NOT NETWORK_IS_ACTIVITY_SESSION()
				IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					IF IS_SKYSWOOP_AT_GROUND()
						IF IS_PROPERTY_SKYCAM() = FALSE
							IF NOT IS_PLAYER_IN_CORONA()
								IF IS_NET_PLAYER_OK(PLAYER_ID())
									IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
										IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, 5000)
									
											IF GET_LOCAL_PLAYER_AIM_STATE() = 0
												PRINT_HELP("AIMFEED_TRA1") //Aim preference is set to Traditional GTA.
												SET_PREVIOUS_AIM_STATE_BEFORE_CHANGE(-1)
												
												#IF IS_DEBUG_BUILD
													NET_NL()NET_PRINT("[AIMPREF] DISPLAY_AIM_PREFERENCE_REVERT_FEED - AIMFEED_TRA = 0 ")
												#ENDIF
												#IF USE_FINAL_PRINTS
												PRINTLN_FINAL("[AIMPREF] DISPLAY_AIM_PREFERENCE_REVERT_FEED - AIMFEED_TRA = 0 ")
												#ENDIF
												
											ELIF GET_LOCAL_PLAYER_AIM_STATE() = 1
												PRINT_HELP("AIMFEED_ASS1") //Aim preference is set to Assissted Aim. 
												
												SET_PREVIOUS_AIM_STATE_BEFORE_CHANGE(-1)
												
												#IF IS_DEBUG_BUILD
													NET_NL()NET_PRINT("[AIMPREF] DISPLAY_AIM_PREFERENCE_REVERT_FEED - AIMFEED_ASS = 1 ")
												#ENDIF
												#IF USE_FINAL_PRINTS
												PRINTLN_FINAL("[AIMPREF] DISPLAY_AIM_PREFERENCE_REVERT_FEED - AIMFEED_ASS = 1 ")
												#ENDIF
	
											ELIF GET_LOCAL_PLAYER_AIM_STATE() = 2
												PRINT_HELP("AIMFEED_FAA1") //Aim preference is set to Free Aim - Assisted. 

												SET_PREVIOUS_AIM_STATE_BEFORE_CHANGE(-1)
	
												#IF IS_DEBUG_BUILD
													NET_NL()NET_PRINT("[AIMPREF] DISPLAY_AIM_PREFERENCE_REVERT_FEED - AIMFEED_FAA = 2 ")
												#ENDIF
												#IF USE_FINAL_PRINTS
												PRINTLN_FINAL("[AIMPREF] DISPLAY_AIM_PREFERENCE_REVERT_FEED - AIMFEED_FAA = 2 ")
												#ENDIF
	
											ELIF GET_LOCAL_PLAYER_AIM_STATE() = 3
												PRINT_HELP("AIMFEED_FRE1") //Aim preference is set to Free Aim. 
												
												SET_PREVIOUS_AIM_STATE_BEFORE_CHANGE(-1)
	
												#IF IS_DEBUG_BUILD
													NET_NL()NET_PRINT("[AIMPREF] DISPLAY_AIM_PREFERENCE_REVERT_FEED - AIMFEED_FRE = 3 ")
												#ENDIF
												#IF USE_FINAL_PRINTS
												PRINTLN_FINAL("[AIMPREF] DISPLAY_AIM_PREFERENCE_REVERT_FEED - AIMFEED_FRE = 3 ")
												#ENDIF
	
											ENDIF

											SET_PREVIOUS_AIM_STATE_HAS_CHANGED(FALSE)
											SET_AIMTYPE_ALTERED_BY_INVITE(FALSE)
										ENDIF
									ENDIF

								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	

ENDPROC

PROC MAINTAIN_GOODBOY_EARN()

	IF bProcessGoodBoyEarn
	#IF IS_DEBUG_BUILD
	OR bRunGoodBoyEarn
	#ENDIF
		
		PRINTLN("MAINTAIN_GOODBOY_EARN - being processed:")
		PRINTLN("MAINTAIN_GOODBOY_EARN - bProcessGoodBoyEarn - ", bProcessGoodBoyEarn)
		
		#IF IS_DEBUG_BUILD
		PRINTLN("MAINTAIN_GOODBOY_EARN - bRunGoodBoyEarn - ", bRunGoodBoyEarn)
		IF bRunGoodBoyEarn
			g_b_do_GoodBehaviourCashRewardAmount = g_sMPTunables.iGoodBehaviourCashawardValue
		ENDIF
		#ENDIF
		
		IF NOT IS_TRANSITION_ACTIVE()
			IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				IF NOT IS_SAVING_HAVING_TROUBLE()
					
					IF USE_SERVER_TRANSACTIONS()
					
						PRINTLN("MAINTAIN_GOODBOY_EARN - USE_SERVER_TRANSACTIONS = TRUE")	
						
						IF NOT bRequestedGoodBoyEarn
							PRINTLN("MAINTAIN_GOODBOY_EARN - calling NETWORK_REQUEST_CASH_TRANSACTION with g_b_do_GoodBehaviourCashRewardAmount = ", g_b_do_GoodBehaviourCashRewardAmount)
							IF NETWORK_REQUEST_CASH_TRANSACTION(iGoodBoyEarnTransactionIndex, NET_SHOP_TTYPE_SERVICE, NET_SHOP_ACTION_EARN, CATEGORY_SERVICE_WITH_THRESHOLD, SERVICE_EARN_NOT_BADSPORT, g_b_do_GoodBehaviourCashRewardAmount)
								bRequestedGoodBoyEarn = TRUE
							ELSE
								PRINTLN("MAINTAIN_GOODBOY_EARN - waiting on NETWORK_REQUEST_CASH_TRANSACTION, iGoodBoyEarnTransactionIndex = ", iGoodBoyEarnTransactionIndex)
							ENDIF
						ELSE
							IF IS_CASH_TRANSACTION_COMPLETE(iGoodBoyEarnTransactionIndex)
								SWITCH GET_CASH_TRANSACTION_STATUS(iGoodBoyEarnTransactionIndex)
									CASE CASH_TRANSACTION_STATUS_FAIL
										PRINTLN("MAINTAIN_GOODBOY_EARN - GET_CASH_TRANSACTION_STATUS = CASH_TRANSACTION_STATUS_FAIL, iGoodBoyEarnTransactionIndex = ", iGoodBoyEarnTransactionIndex)
									BREAK
									CASE CASH_TRANSACTION_STATUS_SUCCESS
										PRINTLN("MAINTAIN_GOODBOY_EARN - GET_CASH_TRANSACTION_STATUS = CASH_TRANSACTION_STATUS_SUCCESS, iGoodBoyEarnTransactionIndex = ", iGoodBoyEarnTransactionIndex)
										NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(iGoodBoyEarnTransactionIndex))
										NETWORK_EARN_FROM_NOT_BADSPORT(g_b_do_GoodBehaviourCashRewardAmount)
										IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS)
										OR NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_END_OF_JOB_OVER)
										OR NOT  IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_END_OF_JOB_FAIL)
										OR NOT  IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_GENERIC)
											PRINT_TICKER_WITH_INT("GOODBOYTICK", g_b_do_GoodBehaviourCashRewardAmount)
										ENDIF
										
										SET_GOOD_BEHAVIOUR_CASH_REWARD_TICKER_NEED_DISPLAY(FALSE, 0)
									BREAK
								ENDSWITCH
								DELETE_CASH_TRANSACTION(iGoodBoyEarnTransactionIndex)
								iGoodBoyEarnTransactionIndex = 0
								bRequestedGoodBoyEarn = FALSE
								SET_GOOD_BEHAVIOUR_CASH_REWARD_TICKER_NEED_DISPLAY(FALSE, 0)
								bProcessGoodBoyEarn = FALSE
								#IF IS_DEBUG_BUILD
								IF bRunGoodBoyEarn
									g_b_do_GoodBehaviourCashRewardAmount = 0
								ENDIF
								bRunGoodBoyEarn = FALSE
								#ENDIF
							ELSE
								PRINTLN("MAINTAIN_GOODBOY_EARN - waiting on IS_CASH_TRANSACTION_COMPLETE, iGoodBoyEarnTransactionIndex = ", iGoodBoyEarnTransactionIndex)
							ENDIF
						ENDIF
						
					ELSE
						
						PRINTLN("MAINTAIN_GOODBOY_EARN - USE_SERVER_TRANSACTIONS = FALSE")	
						
						NETWORK_EARN_FROM_NOT_BADSPORT(g_b_do_GoodBehaviourCashRewardAmount)
						PRINT_TICKER_WITH_INT("GOODBOYTICK", g_b_do_GoodBehaviourCashRewardAmount)
						SET_GOOD_BEHAVIOUR_CASH_REWARD_TICKER_NEED_DISPLAY(FALSE, 0)
						iGoodBoyEarnTransactionIndex = 0
						bRequestedGoodBoyEarn = FALSE
						bProcessGoodBoyEarn = FALSE
						#IF IS_DEBUG_BUILD
						IF bRunGoodBoyEarn
							g_b_do_GoodBehaviourCashRewardAmount = 0
						ENDIF
						bRunGoodBoyEarn = FALSE
						#ENDIF
						
					ENDIF
					
				ELSE
					PRINTLN("MAINTAIN_GOODBOY_EARN - IS_SAVING_HAVING_TROUBLE = TRUE")		
				ENDIF	
			ELSE
				PRINTLN("MAINTAIN_GOODBOY_EARN - IS_PLAYER_SWITCH_IN_PROGRESS = TRUE")
			ENDIF	
		ELSE
			PRINTLN("MAINTAIN_GOODBOY_EARN - IS_TRANSITION_ACTIVE = TRUE")
		ENDIF
	ENDIF
			
ENDPROC


PROC PROCESS_GB_MISSION_VEHICLES_SET_ON_FIRE()
	VEHICLE_INDEX VehicleID
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF DECOR_EXIST_ON(VehicleID, "GBMissionFire")
			IF NOT (g_iSmugglerUniqueMissionID = DECOR_GET_INT(VehicleID, "GBMissionFire"))
				IF (GET_VEHICLE_ENGINE_HEALTH(VehicleID) > (-1000.0))
					IF NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID)	
						SET_VEHICLE_ENGINE_HEALTH(VehicleID, -1000.0)
						SET_ENTITY_INVINCIBLE(VehicleID, FALSE)
						PRINTLN("PROCESS_GB_MISSION_VEHICLES_SET_ON_FIRE - setting vehicle on fire")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	// safety check to make sure g_iSmugglerUniqueMissionID can't somehow be left on a value when off a gb mission
	IF (g_iSmugglerUniqueMissionID > -1)
		IF NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
			g_iSmugglerUniqueMissionID = -1	
			PRINTLN("PROCESS_GB_MISSION_VEHICLES_SET_ON_FIRE - setting g_iSmugglerUniqueMissionID to -1 as not on mission")
			SCRIPT_ASSERT("PROCESS_GB_MISSION_VEHICLES_SET_ON_FIRE - g_iSmugglerUniqueMissionID was not -1 and player was not on gang boss mission, how!?")
		ENDIF
	ENDIF

ENDPROC



INT iUniqueFrame
CONST_INT MAX_NUMBER_OF_UNIQUE_FRAMES 44
PROC MAINTAIN_OVER_UNIQUE_FRAMES()

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("MAINTAIN_OVER_UNIQUE_FRAMES")
	#ENDIF
	#ENDIF	
	
	SWITCH iUniqueFrame
		CASE 0
			PROCESS_GIVE_WANTED_LEVEL_SEEN_IN_STOLEN_VEHICLE(stolenVehicleWantedTimer, iStolenVehicleWantedStage, iStolenVehicleNoWantedTime)
			#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("PROCESS_GIVE_WANTED_LEVEL_SEEN_IN_STOLEN_VEHICLE")
				#ENDIF
			#ENDIF		

			INIT_PV_LIMITER()

		BREAK
		CASE 1
			UPDATE_BROADCAST_APP()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("UPDATE_BROADCAST_APP")
			#ENDIF
			#ENDIF	
			APPLY_AUTO_MUTE()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("APPLY_AUTO_MUTE")
			#ENDIF
			#ENDIF			
		BREAK
		CASE 2
			MAINTAIN_BACKUP_HELI_LAUNCHING(sBackUpHeliLaunchingData)
			#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_BACKUP_HELI_LAUNCHING")
				#ENDIF
			#ENDIF	
			RUN_IDLE_KICK_MESSAGES(IdleTrackingResetTimer)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("RUN_IDLE_KICK_MESSAGES")
			#ENDIF
			#ENDIF		
			RUN_CONSTRAINED_KICK_MESSAGES()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("RUN_CONSTRAINED_KICK_MESSAGES")
			#ENDIF
			#ENDIF	
			
		BREAK
		CASE 3
			MAINTAIN_AIRSTRIKE_LAUNCHING(sAirstrikeLaunchingData)
			#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_AIRSTRIKE_LAUNCHING")
				#ENDIF
			#ENDIF	
			RUN_KICKING_CHECKS_CLIENT(iKickingClientNumberCheck)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("RUN_KICKING_CHECKS_CLIENT")
			#ENDIF
			#ENDIF				
		BREAK
		CASE 4
			MAINTAIN_SETTING_MY_STOLEN_VEHICLE_AS_WANTED(setVehWantedTimer)
			#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SETTING_MY_STOLEN_VEHICLE_AS_WANTED")
				#ENDIF
			#ENDIF		
			RUN_CRC_REPORT_ALL_PLAYER_CHEAT_WATCHER(PlayerCheckCRCStages, PlayerCheckCRCTimer, PlayerCheckCRCStagesDone, PlayerCheckCRCLastParticipantCount)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("RUN_CRC_REPORT_ALL_PLAYER_CHEAT_WATCHER")
			#ENDIF
			#ENDIF			
		BREAK	
		CASE 5
			
			MAINTAIN_SETTING_MY_STOLEN_VEHICLE_AS_WANTED(setVehWantedTimer)
			#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SETTING_MY_STOLEN_VEHICLE_AS_WANTED")
				#ENDIF
			#ENDIF			
		BREAK	
		
		// ones that only need to be processed if the player is off a mission
		CASE 6
			
			IF DOES_GOOD_BEHAVIOUR_CASH_REWARD_TICKER_NEED_DISPLAY()
				IF NOT IS_TRANSITION_ACTIVE()
					IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
						IF NOT IS_SAVING_HAVING_TROUBLE()
							bProcessGoodBoyEarn = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			MAINTAIN_CHEATER_BADSPORT_HAT(bMaintainBadSportHat)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CHEATER_BADSPORT_HAT")
			#ENDIF
			#ENDIF	
		BREAK
		CASE 7

			IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(), FALSE)
				MAINTAIN_GOOD_BOY_REMINDER()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_GOOD_BOY_REMINDER")
				#ENDIF
				#ENDIF	
				
				MAINTAIN_TELL_PLAYER_PLAYING_ALONE(PlayingAloneTimer)
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_TELL_PLAYER_PLAYING_ALONE")
				#ENDIF
				#ENDIF
			ENDIF

			RUN_BAD_SPORT_PHONECALL()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("RUN_BAD_SPORT_PHONECALL")
			#ENDIF
			#ENDIF				
		BREAK
		
		CASE 8
			MAINTAIN_GANG_ANGRY(gangAngryData)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_GANG_ANGRY")
			#ENDIF
			#ENDIF		
			RUN_RANK_CORRECTION(stRankCacheChecked)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("RUN_RANK_CORRECTION")
			#ENDIF
			#ENDIF			
		BREAK		

		CASE 9
			MAINTAIN_PLAYERS_GLOBAL_BROADCAST_DATA(Global_stat_Broadcast_update_stagger_count)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PLAYERS_GLOBAL_BROADCAST_DATA")
			#ENDIF
			#ENDIF			
		BREAK		
		
		CASE 10	
			MAINTAIN_SHOP_DISCOUNT_FOR_GOOD_PEER_REVIEW_TICKER()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SHOP_DISCOUNT_FOR_GOOD_PEER_REVIEW_TICKER")
			#ENDIF
			#ENDIF			
		BREAK		
		
		CASE 11
			MAINTAIN_CACHE_CLEARING()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CACHE_CLEARING")
			#ENDIF
			#ENDIF	
			MAINTAIN_CARWASH_BLIPS()
			#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER( "MAINTAIN_CARWASH_BLIPS")
				#ENDIF
			#ENDIF				
		BREAK		

		CASE 12
			MAINTAIN_VEH_ACCESS_STAT()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_VEH_ACCESS_STAT")
			#ENDIF
			#ENDIF			
		
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF NETWORK_CLAN_SERVICE_IS_VALID() 
					IF  g_bcheckstartingcrewxp = TRUE
						IF g_iFMoldcrewxp != -1
						
					
							MAINTAIN_CREWXP_RANKUP(g_iFMoldcrewxp)
							#IF IS_DEBUG_BUILD
							#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CREWXP_RANKUP")
							#ENDIF
							#ENDIF
						ELSE
						
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
			
		BREAK	
		
		CASE 13

			IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_COLLECTABLES)
				
				IF GET_PACKED_STAT_BOOL( PACKED_MP_weapons_and_pickups_have_been_created) = FALSE
			
					IF (FM_PICKUP_COUNTER < number_of_FM_hidden_package_pickups)
						CREATE_ALL_CURRENT_FM_PACKAGE_PICKUPS (FM_hidden_package, FM_PICKUP_COUNTER)//, TRUE)
						FM_PICKUP_COUNTER++
					ENDIF
					
					IF (FM_PICKUP_COUNTER = number_of_FM_hidden_package_pickups)
						FM_PICKUP_COUNTER = 0
						SET_PACKED_STAT_BOOL(PACKED_MP_weapons_and_pickups_have_been_created,TRUE)
					ENDIF
				ENDIF
				
				
				
			ENDIF
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("CREATE_ALL_CURRENT_FM_PACKAGE_PICKUPS")
			#ENDIF
			#ENDIF				
		BREAK
		
		CASE 14
			RUN_CHEATER_PHONECALL()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("RUN_CHEATER_PHONECALL")
			#ENDIF
			#ENDIF	
	
	
			IF GET_PACKED_STAT_BOOL( PACKED_MP_weapons_and_pickups_have_been_created)
				MAINTAIN_PACKAGE_PICKUP_BLIPS(FM_hidden_package, iHiddenPackagesProcessedLastFrame)
			ENDIF
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PACKAGE_PICKUP_BLIPS")
			#ENDIF
			#ENDIF				
		BREAK		
		
		CASE 15
			IF g_RUN_STAGGERED_CLOTHING_UNLOCKS_CHECK = TRUE
				RUN_FM_CHAR_INITIAL_CLOTHES_UNLOCK(FALSE, FM_RUN_STAGGERED_CLOTHES_UNLOCKS_CHECK_COUNTER)
				FM_RUN_STAGGERED_CLOTHES_UNLOCKS_CHECK_COUNTER++
				IF FM_RUN_STAGGERED_CLOTHES_UNLOCKS_CHECK_COUNTER > 15
					FM_RUN_STAGGERED_CLOTHES_UNLOCKS_CHECK_COUNTER = 0
					g_RUN_STAGGERED_CLOTHING_UNLOCKS_CHECK = FALSE
				ENDIF
			ENDIF	
			
			PROCESS_ASSOCIATED_WANTED_LEVEL()
			#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER( "PROCESS_ASSOCIATED_WANTED_LEVEL")
				#ENDIF
			#ENDIF
			
		BREAK
		
		CASE 16
			MAINTAIN_STORE_PLAYER_RE_ENTRY_STATS()
			#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_STORE_PLAYER_RE_ENTRY_STATS")
				#ENDIF
			#ENDIF	
	
			MAINTAIN_SHOP_FLAGS()	
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SHOP_FLAGS")
			#ENDIF
			#ENDIF	
			
			MAINTAIN_DECREASE_MENTAL_STAT_FOR_PLAYING_TIME()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DECREASE_MENTAL_STAT_FOR_PLAYING_TIME")
			#ENDIF
			#ENDIF	
		BREAK
		
		CASE 17
			MAINTAIN_ACTIVITIES_BLIPS()
			#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER( "MAINTAIN_ACTIVITIES_BLIPS")
				#ENDIF
			#ENDIF	
			
			RUN_ARCHENEMY_IN_GAME_FEED()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("RUN_ARCHENEMY_IN_GAME_FEED")
			#ENDIF
			#ENDIF	
			
			RUN_GOOD_BEHAVIOR_CASH_REWARD()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("RUN_GOOD_BEHAVIOR_CASH_REWARD")
			#ENDIF
			#ENDIF	
			
			
			DISPLAY_AIM_PREFERENCE_REVERT_FEED(AimPrefFeedMessageTimer)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("DISPLAY_AIM_PREFERENCE_REVERT_FEED")
			#ENDIF
			#ENDIF	
			
			REQUEST_AUDIO_FREEMODE()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("REQUEST_AUDIO_FREEMODE")
			#ENDIF
			#ENDIF			
			
		BREAK
		
		CASE 18
			IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				MAINTAIN_TRIGGER_NPC_BOUNTY()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_TRIGGER_NPC_BOUNTY()")
				#ENDIF
				#ENDIF					
			ENDIF
			
			MAINTAIN_BOUNTY_UNLOCKED_FLAG()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_BOUNTY_UNLOCKED_FLAG()")
			#ENDIF
			#ENDIF	
			
			MAINTAIN_STRIPPER_REQUESTS()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_STRIPPER_REQUESTS")
			#ENDIF
			#ENDIF
			
		BREAK
		
		CASE 19
			MAINTAIN_ACTIVE_VEHICLE_SLOT()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ACTIVE_VEHICLE_SLOT")
			#ENDIF
			#ENDIF	
			
			FAILSAFE_CLEAR_PURCHASE_FLAGS()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("FAILSAFE_CLEAR_PURCHASE_FLAGS")
			#ENDIF
			#ENDIF	
		BREAK
		
		CASE 20
			RUN_QUICK_FIX_FEED_ELEMENT(st_QuickFixFeedTimer)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("RUN_QUICK_FIX_FEED_ELEMENT")
			#ENDIF
			#ENDIF	
		BREAK
		CASE 22
			CHECK_30_CARS_ACHIEVEMENT()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("CHECK_30_CARS_ACHIEVEMENT")
			#ENDIF
			#ENDIF	
		BREAK
		CASE 24
			MAINTAIN_EVENT_BOOT_HELP_TEXT()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_EVENT_BOOT_HELP_TEXT")
			#ENDIF
			#ENDIF	
		BREAK	
		CASE 27
			MAINTAIN_SERVER_MAINTENANCE_TICKER_FEED()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_OSERVER_MAINTENANCE_TICKER_FEED")
			#ENDIF
			#ENDIF			
		BREAK
		CASE 28
			MANTAIN_TEXT_CHAT_ACTIVATION()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MANTAIN_TEXT_CHAT_ACTIVATION")
			#ENDIF
			#ENDIF			
		BREAK
		CASE 29
			MAINTAIN_CLAN_INVITE_REQUEST_RECEIVED_TICKER_FEED()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLAN_INVITE_REQUEST_RECEIVED_TICKER_FEED")
			#ENDIF
			#ENDIF			
		BREAK
		CASE 30
			MONITOR_PASSIVE_MODE_FOR_PLAYER_ON_EVENT()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MONITOR_PASSIVE_MODE_FOR_PLAYER_ON_EVENT")
			#ENDIF
			#ENDIF
		BREAK

		CASE 32
			MAINTAIN_FM_EVENT_DO_INVITE_MESSAGE()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FM_EVENT_DO_INVITE_MESSAGE")
			#ENDIF
			#ENDIF			
		BREAK
		
		CASE 33	
			MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP()
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ADVERSARY_MODE_INVITE_AND_HELP")
			#ENDIF
			#ENDIF
		
			// Maintain for the above invite reminders. We should eventually merge these into a single system
			MAINTAIN_ADVERSARY_MODE_REMINDERS()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ADVERSARY_MODE_REMINDERS")
			#ENDIF
			#ENDIF
		BREAK
		CASE 35
			MAINTAIN_GB_ENOUGH_CASH_MESSAGE()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_GB_ENOUGH_CASH_MESSAGE")
			#ENDIF
			#ENDIF
		BREAK
		CASE 36
			MAINTAIN_GLOBAL_INVITE_FLOW()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_GLOBAL_INVITE_FLOW")
			#ENDIF
			#ENDIF
		BREAK

		CASE 38
			MAINTAIN_FAVOURITE_VEHICLE()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FAVOURITE_VEHICLE")
			#ENDIF
			#ENDIF
		
			MAINTAIN_FAVOURITE_MOTORCYCLE()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FAVOURITE_MOTORCYCLE")
			#ENDIF
			#ENDIF
			
		BREAK
		CASE 39

		BREAK	
		CASE 42
			PROCESS_GB_MISSION_VEHICLES_SET_ON_FIRE()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_GB_MISSION_VEHICLES_SET_ON_FIRE")
			#ENDIF
			#ENDIF
		BREAK
		CASE 43	
			UPDATE_PLATE_DUPES()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("UPDATE_PLATE_DUPES")
			#ENDIF
			#ENDIF	
		BREAK
	ENDSWITCH
	
	iUniqueFrame++
	IF (iUniqueFrame >= MAX_NUMBER_OF_UNIQUE_FRAMES)
		iUniqueFrame = 0
	ENDIF
	
	//If we need to maintian the feed that we're going to kill the game
	IF g_sServerMaintenanceVars.bDisplayNowMessage
		PRINT_SERVER_MAINTENANCE_NOW_FEED()
	ENDIF
	
	// This needs process everyframe, if the bool in case 6 above is set to true. Due to needing to query Miguel's cash commands every frame so we know if the cash award has succeeded or failed.
	MAINTAIN_GOODBOY_EARN()
	
	// This needs to be processed on request so we can force all blips to be removed in one frame.
	IF g_bForceUpdateActivityBlips
		MAINTAIN_ACTIVITIES_BLIPS()
		MAINTAIN_CARWASH_BLIPS()
		
		INT iBlip
		REPEAT COUNT_OF(HoldUpData.HoldUpBlips) iBlip
			IF DOES_BLIP_EXIST(HoldUpData.HoldUpBlips[iBlip])
				CLEAR_BIT(MPGlobalsAmbience.iHoldUpActiveBitSet, iBlip)
				REMOVE_BLIP(HoldUpData.HoldUpBlips[iBlip])
				NET_PRINT("   --->   HOLD UP - BLIP REMOVED - BLIP = ") NET_PRINT_INT(iBlip) NET_NL()
			ENDIF
		ENDREPEAT
		
		g_bForceUpdateActivityBlips = FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF		

ENDPROC





#IF IS_DEBUG_BUILD
PROC MAINTAIN_BROADCASTING_NET_FATAL_ERROR()
	IF g_bBroadcastNetFatalError
		BROADCAST_SCRIPT_EVENT_FATAL_ERROR_IN_IMPORTANT_NET_SCRIPT()
		g_bBroadcastNetFatalError = FALSE
	ENDIF
ENDPROC
#ENDIF

PROC MAINTAIN_DELUXO_CONTROL_BLOCK()
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND (IS_PHONE_ONSCREEN()
	OR IS_BROWSER_OPEN())
		VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(veh) 
		AND GET_ENTITY_MODEL(veh) = DELUXO
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_TRANSFORM)
		ENDIF
	ENDIF	
ENDPROC

PROC MAINTAIN_BOUNTY_REWARDS()
	INT iReward
	IF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iBountyValue != 0
		IF NOT HAS_NET_TIMER_STARTED(g_stClearBountyRewardData)
			START_NET_TIMER(g_stClearBountyRewardData,TRUE)
		ELIF HAS_NET_TIMER_EXPIRED(g_stClearBountyRewardData,10000,TRUE)
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iBountyValue = 0
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].bountyClaimer = INVALID_PLAYER_INDEX()
			PRINTLN("MAINTAIN_BOUNTY_REWARDS: clearing local bounty broadcast data")
			RESET_NET_TIMER(g_stClearBountyRewardData)
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(g_stCheCkForBountyReward[g_iSlowBountyClaimLoop])
		IF g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop] != INVALID_PLAYER_INDEX()
		AND IS_NET_PLAYER_OK(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop],FALSE)
			IF HAS_NET_TIMER_EXPIRED(g_stCheCkForBountyReward[g_iSlowBountyClaimLoop],10000,TRUE)
				PRINTLN("MAINTAIN_BOUNTY_REWARDS: timer has expired but no broadcast data was set in slot # ",g_iSlowBountyClaimLoop)
				REMOVE_BOUNTY_DATA_DELAYED_REWARDS_ARRAY(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop],g_ghBountyPlacer[g_iSlowBountyClaimLoop])
			ELSE
				IF GlobalplayerBD_FM_3[NATIVE_TO_INT(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop])].bountyClaimer = PLAYER_ID()
				AND GlobalplayerBD_FM_3[NATIVE_TO_INT(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop])].iBountyValue != 0
					IF g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop] = PLAYER_ID()
						IF IS_BOUNTY_UNLOCKED_FOR_LOCAL_PLAYER()
							IF PLAYER_ID() = g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop]
								//Send_MP_Txtmsg_From_CharSheetID(CHAR_LESTER,"FM_TXT_BNTY1") //You have survived the bounty placed on you.
								iReward = GlobalplayerBD_FM_3[NATIVE_TO_INT(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop])].iBountyValue
								// Check the reward is within a hardcoded value
								PRINTLN("MAINTAIN_BOUNTY_REWARDS: bounty survived rewarding player in slot # ",g_iSlowBountyClaimLoop)
								IF iReward <= BOUNTY_MAX_VALUE
								AND iReward >= 0
									INT iScriptTransactionIndex
									//GIVE_LOCAL_PLAYER_CASH(iReward,TRUE)
									//INCREMENT_BY_MP_INT_CHARACTER_STAT( MP_STAT_BOUNTMONEY,iReward)
									GET_PACKED_STAT_BOOL(PACKED_MP_SPENT_BOUNTY)
									GAMER_HANDLE tempHandle = GET_GAMER_HANDLE_PLAYER(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop])
									IF USE_SERVER_TRANSACTIONS()
										TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_BOUNTY_COLLECTED, iReward, iScriptTransactionIndex, TRUE, TRUE)
										g_cashTransactionData[iScriptTransactionIndex].eExtraData.gamerHandle = g_ghBountyPlacer[g_iSlowBountyClaimLoop]
										g_cashTransactionData[iScriptTransactionIndex].eExtraData.gamerHandle2 = tempHandle
									ELSE
										NETWORK_EARN_FROM_BOUNTY(iReward,g_ghBountyPlacer[g_iSlowBountyClaimLoop],tempHandle,1)
									ENDIF
									SET_LAST_JOB_DATA(LAST_JOB_BOUNTY, iReward)
									IF GET_SPECIAL_BOUNTY_REWARD_VALUE(GlobalplayerBD_FM_3[NATIVE_TO_INT(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop])].iBountyValue,TRUE) != -1
										iReward = GET_SPECIAL_BOUNTY_REWARD_VALUE(GlobalplayerBD_FM_3[NATIVE_TO_INT(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop])].iBountyValue,TRUE)
									ELSE
										iReward = (GET_PLAYER_RANK(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop])*20)
										IF iReward > 2000
											iReward = 2000
										ELIF iReward < 500
											iReward = 500
										ENDIF
										iReward = ROUND(iReward*g_sMPTunables.fxp_tunable_Bounties_Target)  
									ENDIF
									SET_NEXT_XP_DESCRIPTION("XPT_BOUNTYSUR",iReward)
									GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "XPDT_BOUNTYSUR",XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_BOUNTY_HUNT_ESCAPED, iReward)
									SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_SURV_A_BOUNTY, TRUE)
									IF IS_NET_PLAYER_OK(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop],FALSE,FALSE)
										ADD_ITEM_TO_BOUNTY_TEXTS(CHAR_LESTER,"FM_TXT_BNTY1",GlobalplayerBD_FM_3[NATIVE_TO_INT(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop])].iBountyValue,GET_PLAYER_NAME(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop]),"",0,0)
									ELSE
										ADD_ITEM_TO_BOUNTY_TEXTS(CHAR_LESTER,"FM_TXT_BNTY1",GlobalplayerBD_FM_3[NATIVE_TO_INT(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop])].iBountyValue,"-","",0,0)
									ENDIF
								#IF IS_DEBUG_BUILD
								ELSE
									PRINTLN("BOUNTY_OVER: Timed out. Bounty iReward is out of range: iReward = ", iReward, " in slot # ",g_iSlowBountyClaimLoop)
								#ENDIF
								
								ENDIF
							ENDIF
						ENDIF
						REMOVE_BOUNTY_DATA_DELAYED_REWARDS_ARRAY(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop],g_ghBountyPlacer[g_iSlowBountyClaimLoop])
					ELSE
						IF g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop] != INVALID_PLAYER_INDEX()
						AND IS_NET_PLAYER_OK(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop],FALSE,FALSE)
							ADD_ITEM_TO_BOUNTY_TEXTS(CHAR_LESTER,"FM_TXT_BNTY6",GlobalplayerBD_FM_3[NATIVE_TO_INT(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop] )].iBountyValue,GET_PLAYER_NAME(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop]),"",0,0)
						ELSE
							ADD_ITEM_TO_BOUNTY_TEXTS(CHAR_LESTER,"FM_TXT_BNTY6",GlobalplayerBD_FM_3[NATIVE_TO_INT(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop] )].iBountyValue,"-","",0,0)
						ENDIF
						iReward = GlobalplayerBD_FM_3[NATIVE_TO_INT(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop] )].iBountyValue
						PRINTLN("MAINTAIN_BOUNTY_REWARDS: rewarding player in slot # ",g_iSlowBountyClaimLoop)
						// Check the reward is within a hardcoded value
						IF iReward <= BOUNTY_MAX_VALUE
						AND iReward >= 0
							INT iScriptTransactionIndex
							SET_PACKED_STAT_BOOL( PACKED_MP_SPENT_BOUNTY, TRUE)
							GAMER_HANDLE tempHandle = GET_GAMER_HANDLE_PLAYER(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop])
							IF USE_SERVER_TRANSACTIONS()
								PRINTLN("MAINTAIN_BOUNTY_REWARDS- Cash before boss cut: $", iReward)
								GB_HANDLE_GANG_BOSS_CUT(iReward)
								PRINTLN("MAINTAIN_BOUNTY_REWARDS - Cash after boss cut: $", iReward)
								IF iReward > 0
									TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_BOUNTY_COLLECTED, iReward, iScriptTransactionIndex, TRUE, TRUE)
								ENDIF
								g_cashTransactionData[iScriptTransactionIndex].eExtraData.gamerHandle = g_ghBountyPlacer[g_iSlowBountyClaimLoop]
								g_cashTransactionData[iScriptTransactionIndex].eExtraData.gamerHandle2 = tempHandle
							ELSE
								PRINTLN("MAINTAIN_BOUNTY_REWARDS- Cash before boss cut: $", iReward)
								GB_HANDLE_GANG_BOSS_CUT(iReward)
								PRINTLN("MAINTAIN_BOUNTY_REWARDS - Cash after boss cut: $", iReward)
								IF iReward > 0
									NETWORK_EARN_FROM_BOUNTY(iReward,g_ghBountyPlacer[g_iSlowBountyClaimLoop],tempHandle,1)
								ENDIF
							ENDIF
							
							SET_LAST_JOB_DATA(LAST_JOB_BOUNTY, iReward)
							REQUEST_SYSTEM_ACTIVITY_TYPE_DONE_BOUNTY(iReward)
							IF GET_SPECIAL_BOUNTY_REWARD_VALUE(GlobalplayerBD_FM_3[NATIVE_TO_INT(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop])].iBountyValue,TRUE) != -1
								iReward = GET_SPECIAL_BOUNTY_REWARD_VALUE(GlobalplayerBD_FM_3[NATIVE_TO_INT(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop])].iBountyValue,TRUE)
							ELSE
								iReward = GET_PLAYER_RANK(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop])*20
								IF iReward > 2000
									iReward = 2000
								ELIF iReward < 500
									iReward = 500
								ENDIF
								iReward = ROUND(iReward*g_sMPTunables.fxp_tunable_Bounties_Killer)  
							ENDIF
							SET_NEXT_XP_DESCRIPTION("XPT_BOUNTY",iReward)
							GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "XPDT_BOUNTY", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_BOUNTY_HUNT_EXECUTED, iReward)

							INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FMKILLBOUNTY, 1)
							SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_COLLECT_BOUNTY)
	//						IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
	//							BIK_ADD_POINTS_FOR_THIS(BIK_ADD_POINTS_CLAIM_BOUNTY)
	//						ENDIF
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("MAINTAIN_BOUNTY_REWARDS Bounty iReward is out of range: iReward = ", iReward, " in slot # ",g_iSlowBountyClaimLoop)
						#ENDIF
						
						ENDIF
						REMOVE_BOUNTY_DATA_DELAYED_REWARDS_ARRAY(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop],g_ghBountyPlacer[g_iSlowBountyClaimLoop])
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("MAINTAIN_BOUNTY_REWARDS: player to check is not valid exiting in slot # ",g_iSlowBountyClaimLoop)
			REMOVE_BOUNTY_DATA_DELAYED_REWARDS_ARRAY(g_PlayerToCheckForBountyReward[g_iSlowBountyClaimLoop],g_ghBountyPlacer[g_iSlowBountyClaimLoop])
		ENDIF
	ENDIF
	g_iSlowBountyClaimLoop++
	IF g_iSlowBountyClaimLoop >= MAX_BOUNTY_CLAIMS_IN_TEN_SECONDS
		g_iSlowBountyClaimLoop = 0
	ENDIF
ENDPROC



//Controlls the freemode main running state.
PROC MAINTAIN_FREEMODE_MAIN_GAME_STATE_RUNNING()

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("MAINTAIN_FREEMODE_MAIN_GAME_STATE_RUNNING")
	#ENDIF
	#ENDIF

	//flag to pause bounty this frame
	IF MPGlobalsAmbience.playerBounty.bPausePlayerBountyNextFrame
		MPGlobalsAmbience.playerBounty.bPausePlayerBountyThisFrame = TRUE
	ELSE
		MPGlobalsAmbience.playerBounty.bPausePlayerBountyThisFrame = FALSE
	ENDIF
	MPGlobalsAmbience.playerBounty.bPausePlayerBountyNextFrame = FALSE
	
	//flag to hide this player's bounty blip this frame
	IF MPGlobalsAmbience.playerBounty.bHidePlayerBountyBlipNextFrame
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bHideBountyBlip = TRUE
	ELSE
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bHideBountyBlip = FALSE
	ENDIF
	MPGlobalsAmbience.playerBounty.bHidePlayerBountyBlipNextFrame = FALSE
	
	
	//flag to disable contact menu
	IF g_TransitionSessionNonResetVars.contactRequests.bDisableContactRequestMenuNextFrame
		g_TransitionSessionNonResetVars.contactRequests.bDisableContactRequestMenuThisFrame = TRUE
	ELSE
		g_TransitionSessionNonResetVars.contactRequests.bDisableContactRequestMenuThisFrame = FALSE
	ENDIF
	g_TransitionSessionNonResetVars.contactRequests.bDisableContactRequestMenuNextFrame = FALSE
	
	//flag to disable player calls this frame
	IF voiceSession.bBusyNextFrame
		voiceSession.bBusyThisFrame = TRUE
	ELSE
		voiceSession.bBusyThisFrame = FALSE
	ENDIF
	voiceSession.bBusyNextFrame= FALSE
	
	IF IS_BIT_SET(g_iExteriorScriptProcessingWarpBS,0)
		SET_BIT(g_iExteriorScriptProcessingWarpBS,1)
	ELSE
		CLEAR_BIT(g_iExteriorScriptProcessingWarpBS,1)
	ENDIF
	CLEAR_BIT(g_iExteriorScriptProcessingWarpBS,0)
	
	IF g_bTriggerCharacterFacebookPostUpdates
		TRIGGER_CHARACTER_FACEBOOK_POST_UPDATES()
		g_bTriggerCharacterFacebookPostUpdates = FALSE
	ENDIF
	
	MAINTAIN_STRIPCLUB_FM()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_STRIPCLUB_FM")
	#ENDIF
	#ENDIF 	

	MAINTAIN_CLEAN_UP_WHEN_NOT_ON_A_MISSION()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLEAN_UP_WHEN_NOT_ON_A_MISSION")
	#ENDIF
	#ENDIF
	
	MPSV_RUN_DUPLICATE_CHECKS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MPSV_RUN_DUPLICATE_CHECKS")
	#ENDIF
	#ENDIF
	
	BOOL bPlayingLearningTheRopes = FALSE
	IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(Player_ID(), FMMC_TYPE_MISSION)
	AND NOT HAS_PLAYER_COMPLETED_RACE_AND_DM_TUTORIAL(Player_ID())
		bPlayingLearningTheRopes = TRUE
	ENDIF
	
	// Allow debug drawing of lbd /////
	#IF IS_DEBUG_BUILD
	IF g_DebugDrawDpadLbd
		PRINTLN("3534137, DEBUG_DRAW_DPAD_LBD!!! ")
		DEBUG_DRAW_DPAD_LBD(freeDpadVars.scaleformFreemodeMovie, SUB_FREEMODE, freeDpadVars, fmDpadVars)
	ELSE
	#ENDIF
	
	// Draw Freemode dpad leaderboard
	IF NOT NETWORK_IS_ACTIVITY_SESSION() // Not on a Job
	AND NOT MPGlobalsAmbience.bFMIntroCutDemandingDpadDownActive
	AND NOT bPlayingLearningTheRopes
		
		IF g_sDpadTypeToDraw.eDpadTypeToDraw = eDPAD_TYPE_CHALLENGE

			IF HAS_NET_TIMER_EXPIRED(timerAmbientEventLDBHelpDelay, 45000)
				HANDLE_CHALLENGE_LBD_PROMPT(iAmbientEventLBDHelpCounter)
			ENDIF
		ELSE

			RESET_NET_TIMER(timerAmbientEventLDBHelpDelay)
			iAmbientEventLBDHelpCounter = GET_PACKED_STAT_INT(PACKED_MP_AMBIENT_EVENT_LBD_HELP_COUNTER)

			
			DRAW_FREEMODE_DPAD_LBD(freeDpadVars.scaleformFreemodeMovie, SUB_FREEMODE, freeDpadVars, fmDpadVars)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("DRAW_FREEMODE_DPAD_LBD")
			#ENDIF
			#ENDIF
//			PRINTLN("[DM_LEADERBOARD] [FREEMODE] - MAINTAIN_FREEMODE_MAIN_GAME_STATE_RUNNING - DRAW_FREEMODE_DPAD_LBD")

		ENDIF
		
	ENDIF
	
	// Reset the dpad draw enum
	DISABLE_SCRIPT_DPAD_THIS_FRAME_RESET()
	
	// End debug draw lbd
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	///////////////////////////////////
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())  //Death Check needed in here. 
		
		/*IF bBuddyChecksSent = FALSE
			SEND_ALL_BUDDY_CHECKS()
			bBuddyChecksSent = TRUE
		ENDIF*/
		
		MAINTAIN_RANKUP_DISPLAY(AllRankupStuff)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_RANKUP_DISPLAY")
		#ENDIF
		#ENDIF
		
		
		IF g_bcheckstartingcrewxp = FALSE
		    GAMER_HANDLE player_handle 
     	    player_handle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	        IF NETWORK_CLAN_SERVICE_IS_VALID() 
	            IF NETWORK_CLAN_PLAYER_IS_ACTIVE(player_handle)   
					IF freemodeDataLB.bFinishedInitialLeaderboardCheck = TRUE
						//IF  HAS_PLAYER_COMPLETED_RACE_AND_DM_TUTORIAL2(PLAYER_ID())
							g_bcheckstartingcrewxp = TRUE
							g_iFMoldcrewxp = GET_PLAYER_FM_CREW_XP(Player_ID())
							MPGlobalsStats.bhas_crew_unlocks_been_initialised = FALSE
					     	RUN_INITIAL_FM_CREW_UNLOCK(FALSE)
							NET_PRINT("g_bcheckstartingcrewxp = TRUE initialised RUN_INITIAL_FM_CREW_UNLOCK ") NET_NL()
						//ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF


	ELSE
		RESET_RANKUP_PROCESS(AllRankupStuff) 
	ENDIF
	
	
	IF IS_TRANSITION_ACTIVE()
		 RESET_RANKUP_PROCESS(AllRankupStuff)
	ENDIF
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("RESET_RANKUP_PROCESS")
	#ENDIF
	#ENDIF	
	
	
	START_GLOBAL_SCENE_AUDIO(bInitAudioScene)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("START_GLOBAL_SCENE_AUDIO")
	#ENDIF
	#ENDIF
	
	
	
	IF g_bLauncherWillControlMiniMap = FALSE
		MAINTAIN_FREEMODE_DPAD_DOWN()
		RUN_RADAR_MAP()		
	ENDIF
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FREEMODE_DPAD_DOWN & RUN_RADAR_MAP")
	#ENDIF
	#ENDIF
	
	RUN_CASH_DISPLAYS(CashDisplayChecker)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("RUN_CASH_DISPLAYS")
	#ENDIF
	#ENDIF
	
	
	MAINTAIN_FORCE_OUT_OF_VEHICLE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FORCE_OUT_OF_VEHICLE")
	#ENDIF
	#ENDIF
	
	
	
//	MAINTAIN_FREEMODE_JOBLIST()
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE 
//		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FREEMODE_JOBLIST")
//	#ENDIF
//	#ENDIF

	
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
	OR GET_JOINING_GAMEMODE() = GAMEMODE_FM
		MAINTAIN_STAT_TRACKING(StatTimerCounter #IF IS_DEBUG_BUILD, missionTab #ENDIF)
	ENDIF
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_STAT_TRACKING")
	#ENDIF
	#ENDIF	

	
	
	
	// store crew slots (added by KW 28/1/13)
	IF binitialise_crewslots = FALSE
		IF NETWORK_CLAN_SERVICE_IS_VALID() 
			IF NETWORK_HAS_SOCIAL_CLUB_ACCOUNT()
				INITIALISE_PLAYERS_CREW_STAT_SLOTS()
				PRINTLN("TRANSITION TIME - FREEMODE - INITIALISE_PLAYERS_CREW_STAT_SLOTS - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")		
				 binitialise_crewslots = TRUE
			ENDIF
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("INITIALISE_PLAYERS_CREW_STAT_SLOTS")
	#ENDIF
	#ENDIF		
	
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
		IF CAN_FM_AMMO_TRACKER_RUN()
			TRACK_PLAYERS_AMMO_USE_IN_FREEMODE(FM_AMMO_TRACKING_COUNTER)
			FM_AMMO_TRACKING_COUNTER++
			IF FM_AMMO_TRACKING_COUNTER > 86
				FM_AMMO_TRACKING_COUNTER = 0
			ENDIF
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("TRACK_PLAYERS_AMMO_USE_IN_FREEMODE")
			#ENDIF
			#ENDIF
			//NET_PRINT("		TRACK_PLAYERS_AMMO_USE_IN_FREEMODE	")NET_NL()
			//NET_PRINT_INT(FM_AMMO_TRACKING_COUNTER)
		ENDIF
	ENDIF
	

	
	IF g_GIVE_PLAYER_ALL_UNLOCKED_AND_EQUIPED_WEAPONS_FM_CHECK = TRUE
		GIVE_PLAYER_ALL_UNLOCKED_AND_EQUIPED_WEAPONS_FM(WEAPONINHAND_FIRSTSPAWN_HOLSTERED,  g_MPCharData.bIsFirstSpawn, FM_RUN_STAGGERED_WEAPON_UNLOCKS_CHECK_COUNTER )
		FM_RUN_STAGGERED_WEAPON_UNLOCKS_CHECK_COUNTER++
		IF FM_RUN_STAGGERED_WEAPON_UNLOCKS_CHECK_COUNTER > 11
			FM_RUN_STAGGERED_WEAPON_UNLOCKS_CHECK_COUNTER = 0
			g_GIVE_PLAYER_ALL_UNLOCKED_AND_EQUIPED_WEAPONS_FM_CHECK = FALSE
		ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("GIVE_PLAYER_ALL_UNLOCKED_AND_EQUIPED_WEAPONS_FM")
		#ENDIF
		#ENDIF
	ENDIF	
	
	
	/////////
	///     
	///     Check if player is wearing valid clothing staggered check
	///    	BY KEVIN W 26 SEPT '14 
	///     Staggered loop to check if player is wearing invalid clothing
	///     
	///     
	///     
	IF NOT Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID())
	AND FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) != FMMC_TYPE_HUNT_THE_BEAST
	AND NOT IS_LOCAL_PLAYER_ON_A_NEW_AVERSAY() 
	AND NOT IS_PLAYER_IN_CORONA()
		IF g_bRUN_VALIDATE_PLAYERS_CLOTHING_CHECK = TRUE
		//	REMOVE_CHRISTMAS_CLOTHING_ITEM(PLAYER_ID(), FM_RUN_VALIDATE_PLAYERS_CLOTHING_CHECK_COUNTER)
			if  REMOVE_LOCKED_CLOTHING_ITEM(FM_RUN_VALIDATE_PLAYERS_CLOTHING_CHECK_COUNTER)
				PRINTLN("KW REMOVE_LOCKED_CLOTHING_ITEM counter:", FM_RUN_VALIDATE_PLAYERS_CLOTHING_CHECK_COUNTER, ", g_iPedComponentSlot:", g_iPedComponentSlot, ", GET_ACTIVE_CHARACTER_SLOT:", GET_ACTIVE_CHARACTER_SLOT())
				SAVE_PLAYERS_CLOTHES(PLAYER_PED_ID())
			ENDIF
			FM_RUN_VALIDATE_PLAYERS_CLOTHING_CHECK_COUNTER++
			IF FM_RUN_VALIDATE_PLAYERS_CLOTHING_CHECK_COUNTER > 3
				FM_RUN_VALIDATE_PLAYERS_CLOTHING_CHECK_COUNTER = 0
				g_bRUN_VALIDATE_PLAYERS_CLOTHING_CHECK = FALSE
				PRINTLN("KW REMOVE_LOCKED_CLOTHING_ITEM g_bRUN_VALIDATE_PLAYERS_CLOTHING_CHECK FM_RUN_VALIDATE_PLAYERS_CLOTHING_CHECK_COUNTER reset ")
				
			ENDIF
		ENDIF
	ENDIF
	
	IF g_bRUN_SPECIAL_REWARD_UNLOCKS = TRUE
		IF CAN_PRINT_ROCKSTAR_FEED_MSG()
		AND NOT IS_CUTSCENE_PLAYING()
		AND NOT g_bCelebrationScreenIsActive
			PRINTLN("2234067, UNLOCK_SPECIAL_EVENT_ITEMS ")
			UNLOCK_SPECIAL_EVENT_ITEMS(TRUE)
			g_bRUN_SPECIAL_REWARD_UNLOCKS = FALSE			 
		ENDIF
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("GIVE_PLAYER_ALL_UNLOCKED_AND_EQUIPED_WEAPONS_FM")
	#ENDIF
	#ENDIF	
	
	IF g_RUN_FM_INITIAL_CARMOD_UNLOCK_FM_CHECK = TRUE
		RUN_FM_INITIAL_CARMOD_UNLOCK(FALSE,FM_RUN_STAGGERED_RUN_FM_INITIAL_CARMOD_UNLOCK_CHECK_COUNTER )
		FM_RUN_STAGGERED_RUN_FM_INITIAL_CARMOD_UNLOCK_CHECK_COUNTER++
		IF FM_RUN_STAGGERED_RUN_FM_INITIAL_CARMOD_UNLOCK_CHECK_COUNTER > 10
			FM_RUN_STAGGERED_RUN_FM_INITIAL_CARMOD_UNLOCK_CHECK_COUNTER = 0
			g_RUN_FM_INITIAL_CARMOD_UNLOCK_FM_CHECK = FALSE
		ENDIF
	ENDIF	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("g_RUN_FM_INITIAL_CARMOD_UNLOCK_FM_CHECK")
	#ENDIF
	#ENDIF	
	
	
	IF MPGlobalsHud_TitleUpdate.display_unlock_tickers = TRUE
		IF NOT NETWORK_IS_IN_MP_CUTSCENE()
			IF MPGlobalsHud_TitleUpdate.unlock_ticker_main_timer > g_sMPTunables.iRUN_TICKER_INGAME_UNLOCKS_time_to_process
				RUN_TICKER_INGAME_UNLOCKS()
			ENDIF
			MPGlobalsHud_TitleUpdate.unlock_ticker_main_timer++
		ENDIF
	ENDIF
	
	
	IF pedsstoredforlooting.b_looting_turn_on
		IS_MP_PLAYER_LOOTING_DEAD_BODY(pedsstoredforlooting)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("IS_MP_PLAYER_LOOTING_DEAD_BODY")
		#ENDIF
		#ENDIF
		
		CHECK_AND_STORE_DEAD_BOD_FOR_LOOTING(pedsstoredforlooting)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("CHECK_AND_STORE_DEAD_BOD_FOR_LOOTING")
		#ENDIF
		#ENDIF
	ENDIF
		
	IF MPGlobalsHud.bDisplayedFirstTattooUnlock = FALSE
	AND MPGlobalsHud.bDisplayedFirstTattooUnlockQueue = TRUE
	AND GET_PACKED_STAT_BOOL(PACKED_MP_DISPLAY_FIRST_TATTOO_UNLOCK_HELP) = FALSE
		DISPLAY_FIRST_TATTOO_HELP_TEXT()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("DISPLAY_FIRST_TATTOO_HELP_TEXT")
		#ENDIF
		#ENDIF
	ENDIF
	
	IF MPGlobalsHud_TitleUpdate.CAN_DISPLAY_WEAPON_HELP = TRUE
	and not GET_PACKED_STAT_BOOL(PACKED_MP_1ST_DISPLAY_WEAPON_PURCHASE_HELP) 
	AND HAS_FM_TRIGGER_TUT_BEEN_DONE() 
		DISPLAY_FIRST_WEAPON_HELP_TEXT()
	ENDIF
	
	MAINTAIN_REQUEST_CORONA_POS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_REQUEST_CORONA_POS")
	#ENDIF
	#ENDIF	
	
	IF  NOT IS_BIT_SET(g_FMMC_STRUCT.iWepRestBS, REST_DISABLE_PICKUPS)
	OR NOT NETWORK_IS_ACTIVITY_SESSION()
		DROP_MP_INVENTORY_WEAPONS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("DROP_MP_INVENTORY_WEAPONS")
		#ENDIF
		#ENDIF
	ENDIF
	
	DRAW_UNLOCKS_SCALEFORM(AllRankupStuff, MPGlobalsHudRewards.RewardArray)


    #IF IS_DEBUG_BUILD
    #IF SCRIPT_PROFILER_ACTIVE 
    	ADD_SCRIPT_PROFILE_MARKER("DRAW_UNLOCKS_SCALEFORM")
    #ENDIF
    #ENDIF
	
	PROCESS_POWER_UPS(PowerUpData)
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER( "PROCESS_POWER_UPS")
		#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		MAINTAIN_BROADCASTING_NET_FATAL_ERROR()
	#ENDIF

	PROCESS_PLAYER_KEEPING_WANTED_LEVEL()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER( "PROCESS_PLAYER_KEEPING_WANTED_LEVEL")
		#ENDIF
	#ENDIF
	
	PROCESS_PLAYER_LOSING_WANTED_LEVEL()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER( "PROCESS_PLAYER_LOSING_WANTED_LEVEL")
		#ENDIF
	#ENDIF
	
	MAINTAIN_BOUNTY_FUNCTIONALITY()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_BOUNTY_FUNCTIONALITY")
		#ENDIF
	#ENDIF
	
	MAINTAIN_BOUNTY_REWARDS()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_BOUNTY_REWARDS")
		#ENDIF
	#ENDIF
	
	MAINTAIN_MP_VEHICLE_TEXT_MESSAGES()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MP_VEHICLE_TEXT_MESSAGES")
		#ENDIF
	#ENDIF
	
	MAINTAIN_AMMO_DROP_CLIENT()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_AMMO_DROP_CLIENT")
		#ENDIF
	#ENDIF
	
	MAINTAIN_BRU_BOX_CLIENT()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_BRU_BOX_CLIENT")
		#ENDIF
	#ENDIF
	
	MAINTAIN_VEHICLE_DROP_CLIENT(VDPersonalData, VDPersonalTruckData, VDPersonalTruckBlip, VDPersonalAvengerBlip, VDPersonalHackerTruckBlip)
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_VEHICLE_DROP_CLIENT")
		#ENDIF
	#ENDIF
	
	PROCESS_LAUNCH_PI_MENU()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_LAUNCH_PI_MENU")
		#ENDIF
	#ENDIF
	
	MAINTAIN_KILL_YOURSELF()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_KILL_YOURSELF")
		#ENDIF
	#ENDIF
	
	
	MAINTAIN_FREEMODE_NON_MISSION_HELP()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FREEMODE_NON_MISSION_HELP")
		#ENDIF
	#ENDIF
	
	IF OK_TO_DO_NON_MISSION_HELP(FALSE, FALSE, FALSE, TRUE, FALSE, TRUE, TRUE)
	OR (IS_CELLPHONE_CONVERSATION_PLAYING()
	AND (iFlowCallType != ciFLOW_CALL_NONE)	)
		
		GB_MAINTAIN_FAIL_CALLS(unlockFmPhoneCall, speechGbPhoneCall, iFlowCallType, stPassFailPhonecallDelay)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("GB_MAINTAIN_FAIL_CALLS")
		#ENDIF
		#ENDIF

	ENDIF	
	
	MAINTAIN_LESTER_DISABLE_COPS()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_LESTER_DISABLE_COPS")
		#ENDIF
	#ENDIF
	
	MAINTAIN_DISABLE_RECORDING_FOR_SPHERE()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DISABLE_RECORDING_FOR_SPHERE")
		#ENDIF
	#ENDIF	
	
	DISABLE_SCRIPT_HUD_THIS_FRAME_RESET()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("DISABLE_SCRIPT_HUD_THIS_FRAME_RESET()")
		#ENDIF
	#ENDIF	
	
	MAINTAIN_CONTACT_REQUESTS_LAUNCH(freemodeContactRequests)
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CONTACT_REQUESTS_LAUNCH()")
		#ENDIF
	#ENDIF
	
	MAINTAIN_SCRIPT_LAUNCH()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SCRIPT_LAUNCH")
	#ENDIF
	#ENDIF 
	
	MAINTAIN_FREEMODE_SPECTATOR_SET_CAM_MODES()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FREEMODE_SPECTATOR_SET_CAM_MODES")
	#ENDIF
	#ENDIF
	
	MAINTAIN_KILL_CAM()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_KILL_CAM")
	#ENDIF
	#ENDIF 

	Maintain_Buddies_In_Car_XP()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_Buddies_In_Car_XP")
	#ENDIF
	#ENDIF 

	
	//Deal with swapping objects
	MP_CONTROL_SWAPPED_MAP_OBJETS(g_iMP_SwapObjects_SwitchDone)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MP_CONTROL_SWAPPED_MAP_OBJETS")
	#ENDIF
	#ENDIF

	UPDATE_MP_SAVED_VEHICLE_SPAWNING()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_MP_SAVED_VEHICLE_SPAWNING")
	#ENDIF
	#ENDIF			
	
	MAINTAIN_FM_ACTIVITY_UNLOCK_PHONECALLS()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FM_ACTIVITY_UNLOCK_PHONECALLS")
		#ENDIF
	#ENDIF

	
	STORE_DETAILS_FOR_ALTER_WANTED_LEVEL_FOR_CHANGED_LOOK()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("STORE_DETAILS_FOR_ALTER_WANTED_LEVEL_FOR_CHANGED_LOOK")
		#ENDIF
	#ENDIF
	
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_RACE
	AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_DEATHMATCH
	AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_SURVIVAL
	AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_BASE_JUMP
		MAINTAIN_STARTING_RACE_TO_POINT()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_STARTING_RACE_TO_POINT")
		#ENDIF
		#ENDIF
	ENDIF
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		HANDLE_MAP_EXITING_PLAYERS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("HANDLE_MAP_EXITING_PLAYERS")
		#ENDIF
		#ENDIF
	ENDIF
	

	// ******************************************************************************
	//		MAINTAIN FUNCTIONS THAT ONLY NEED TO BE PROCESSED OFF MISSION
	// ******************************************************************************
	IF NOT IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID(), FALSE)
		RUN_START_OF_GAME_CHEAT_CHECK(HasRunCheatCheckAtStart, GAMEMODE_FM)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("RUN_START_OF_GAME_CHEAT_CHECK")
		#ENDIF
		#ENDIF	
		 
		RUN_BAD_SPORT_MESSAGE_CHECK()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("RUN_BAD_SPORT_MESSAGE_CHECK")
		#ENDIF
		#ENDIF	
		
		RUN_START_OF_GAME_BAD_SPORT_CHECK(HasRunBadSportCheckAtStart, GAMEMODE_FM)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("RUN_START_OF_GAME_BAD_SPORT_CHECK")
		#ENDIF
		#ENDIF		
		
		MAINTAIN_BOMB_VEHICLES()	
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_BOMB_VEHICLES")
		#ENDIF
		#ENDIF			
	
		IF GET_PACKED_STAT_BOOL(PACKED_MP_weapons_and_pickups_have_been_created )
			MAINTAIN_PACKAGE_PICKUP_BLIPS(FM_hidden_package, iHiddenPackagesProcessedLastFrame)
		ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PACKAGE_PICKUP_BLIPS")
		#ENDIF
		#ENDIF	
			
			
		
		MAINTAIN_HOLD_UPS(GlobalServerBD_HoldUp.HoldUpServerData, HoldUpData)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "MAINTAIN_HOLD_UPS")
		#ENDIF
		#ENDIF		
			
		MAINTAIN_STARTING_IMPROMPTU_DM()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_STARTING_IMPROMPTU_DM")
		#ENDIF
		#ENDIF	
		
		MAINTAIN_STARTING_BOSSVBOSS_DM()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_STARTING_BOSSVBOSS_DM")
		#ENDIF
		#ENDIF	
		
	ENDIF
	
	//this still needs to run on mission as it is used on heists to warp nearby players.
	MAINTAIN_INVITED_TO_APARTMENT_WARP()	
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER( "MAINTAIN_INVITED_TO_APARTMENT_WARP")
		#ENDIF
	#ENDIF	
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF	
	
ENDPROC

//Launch am door if a transition session is launching. 
FUNC BOOL LAUNCH_AM_DOORS_ON_TRANSITION_SESSION_LAUNCH()
	MP_MISSION_DATA amDoorsToLaunch
	amDoorsToLaunch.mdID.idMission = eAM_DOORS
	amDoorsToLaunch.iInstanceId = -1
	NET_PRINT("LAUNCH_AM_DOORS_ON_TRANSITION_SESSION_LAUNCH - Relaunching AM_DOORS NOT NETWORK_IS_IN_TUTORIAL_SESSION") NET_NL()
	IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(amDoorsToLaunch)
		NET_PRINT("LAUNCH_AM_DOORS_ON_TRANSITION_SESSION_LAUNCH - Relaunched AM_DOORS not in tutorial") NET_NL()
		NET_PRINT_STRING_INT("LAUNCH_AM_DOORS_ON_TRANSITION_SESSION_LAUNCH - Relaunched AM_DOORS not in tutorial instance is: ", MPGlobalsAmbience.iAMDoorsLastInstance) NET_NL()
		MPGlobalsAmbience.iAMDoorsLastInstance = amDoorsToLaunch.iInstanceId 
		MPGlobalsAmbience.bInitAMDoorsLaunch = TRUE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

// PURPOSE:	Requests and runs all non-network scripts that persiste throughout Freemode
//
// RETURN VALUE:			BOOL			TRUE when all scripts have been launched, otherwise FALSE
//
// NOTES:	KGM 21/12/12: Added by Keith to launch BootyCallHandler for Freemode, but if that becomes a network script then we can remove this.
FUNC BOOL Have_All_Persistent_Non_Network_Freemode_Scripts_Loaded()

	// Adding variables in expectation of more scripts being launched like this - should be able to copy the bootycallhandler functionality for other scripts
	BOOL allScriptsRunning	= TRUE
	BOOL thisScriptRunning	= TRUE

//	// BootyCallHandler	
//	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("bootycallhandler")) = 0)
//		// ...not running, so request, check, and launch
//		thisScriptRunning = FALSE
//		
//		REQUEST_SCRIPT("bootycallhandler")
//    	IF (HAS_SCRIPT_LOADED("bootycallhandler"))
//		    START_NEW_SCRIPT("bootycallhandler", DEFAULT_STACK_SIZE)
//		    SET_SCRIPT_AS_NO_LONGER_NEEDED("bootycallhandler")
//			
//			thisScriptRunning = TRUE
//		ELSE
//			#IF IS_DEBUG_BUILD
//        		NET_PRINT("...Freemode waiting for script to launch: bootycallhandler") NET_NL()
//			#ENDIF
//		ENDIF
//		
//		IF NOT (thisScriptRunning)
//			allScriptsRunning = FALSE
//		ENDIF
//	ENDIF

	// FMMC_Launcher
	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FMMC_Launcher")) = 0)
		// ...not running, so request, check, and launch
		thisScriptRunning = FALSE
		
		REQUEST_SCRIPT("FMMC_Launcher")
    	IF (HAS_SCRIPT_LOADED("FMMC_Launcher"))
		    START_NEW_SCRIPT("FMMC_Launcher", FMMC_LAUNCHER_STACK_SIZE)
		    SET_SCRIPT_AS_NO_LONGER_NEEDED("FMMC_Launcher")			
			thisScriptRunning = TRUE
		ELSE
			#IF IS_DEBUG_BUILD
        		NET_PRINT("...Freemode waiting for script to launch: FMMC_Launcher") NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("...Freemode waiting for script to launch: FMMC_Launcher")
			#ENDIF
		ENDIF
		
		IF NOT (thisScriptRunning)
			allScriptsRunning = FALSE
		ENDIF
	ENDIF
	
	IF IS_TRANSITION_SESSION_LAUNCHING()
	AND NOT MPGlobalsAmbience.bInitAMDoorsLaunch = TRUE
		IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_DOORS",MPGlobalsAmbience.iAMDoorsLastInstance,TRUE)
			// ...not running, so request, check, and launch
			thisScriptRunning = FALSE
			REQUEST_SCRIPT("AM_DOORS")
			IF LAUNCH_AM_DOORS_ON_TRANSITION_SESSION_LAUNCH()
				thisScriptRunning = TRUE
			ELSE
				#IF IS_DEBUG_BUILD
	        		NET_PRINT("...Freemode waiting for script to launch: AM_DOORS") NET_NL()
				#ENDIF			
				#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("...Freemode waiting for script to launch: AM_DOORS")
				#ENDIF
			ENDIF
			IF NOT (thisScriptRunning)
				allScriptsRunning = FALSE
			ENDIF
		ENDIF	
	ENDIF	
	
	// EmergencyCallLauncher
	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("emergencycalllauncher")) = 0)
		// ...not running, so request, check, and launch
		thisScriptRunning = FALSE
		
		REQUEST_SCRIPT("emergencycalllauncher")
    	IF (HAS_SCRIPT_LOADED("emergencycalllauncher"))
		    START_NEW_SCRIPT("emergencycalllauncher", DEFAULT_STACK_SIZE)
		    SET_SCRIPT_AS_NO_LONGER_NEEDED("emergencycalllauncher")
			
			thisScriptRunning = TRUE
		ELSE
			#IF IS_DEBUG_BUILD
        		NET_PRINT("...Freemode waiting for script to launch: emergencycalllauncher") NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("...Freemode waiting for script to launch: emergencycalllauncher")
			#ENDIF
		ENDIF
		
		IF NOT (thisScriptRunning)
			allScriptsRunning = FALSE
		ENDIF
	ENDIF

	// net_cloud_mission_loader - added by Keith 11/2/13
	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("net_cloud_mission_loader")) = 0)
		// ...not running, so request, check, and launch
		thisScriptRunning = FALSE
		
		REQUEST_SCRIPT("net_cloud_mission_loader")
    	IF (HAS_SCRIPT_LOADED("net_cloud_mission_loader"))
		    START_NEW_SCRIPT("net_cloud_mission_loader", FRIEND_STACK_SIZE)
		    SET_SCRIPT_AS_NO_LONGER_NEEDED("net_cloud_mission_loader")
			
			thisScriptRunning = TRUE
		ELSE
			#IF IS_DEBUG_BUILD
        		NET_PRINT("...Freemode waiting for script to launch: net_cloud_mission_loader") NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("...Freemode waiting for script to launch: net_cloud_mission_loader")
			#ENDIF
		ENDIF
		
		IF NOT (thisScriptRunning)
			allScriptsRunning = FALSE
		ENDIF
	ENDIF
	
	// fake_interiors - added by DaveyG 10/4/13
	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("fake_interiors")) = 0)
		// ...not running, so request, check, and launch
		thisScriptRunning = FALSE
		
		REQUEST_SCRIPT("fake_interiors")
    	IF (HAS_SCRIPT_LOADED("fake_interiors"))
		    START_NEW_SCRIPT("fake_interiors", DEFAULT_STACK_SIZE)
		    SET_SCRIPT_AS_NO_LONGER_NEEDED("fake_interiors")
			
			thisScriptRunning = TRUE
		ELSE
			#IF IS_DEBUG_BUILD
        		NET_PRINT("...Freemode waiting for script to launch: fake_interiors") NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("...Freemode waiting for script to launch: fake_interiors")
			#ENDIF
		ENDIF
		
		IF NOT (thisScriptRunning)
			allScriptsRunning = FALSE
		ENDIF
	ENDIF
	
	// All scripts loaded and launched?
	RETURN (allScriptsRunning)

ENDFUNC



//PROC MAINTAIN_NO_SAVES_REMINDER()
//	
//	IF IS_CLOUD_DOWN_CLOUD_LOADER() 
//	OR IS_PLAYER_KICKED_TO_GO_OFFLINE()
//		IF NOT IS_TRANSITION_ACTIVE()
//			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//				IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(g_TransitionSessionNonResetVars.stNoSavesReminder, NO_SAVES_REMINDER_FREQUENCY) // Every 2 hours.
//				OR bSavesOffLineFirstMessage = FALSE
//					PRINT_HELP("NOSAVESRMDR")
//					bSavesOffLineFirstMessage = TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	
//ENDPROC


PROC SET_ARM_WRESTLING_RADIO_EMITTER_STATE()
	IF NOT bTurnedOnArmRadioEmitters
		PRINTLN("SET_ARM_WRESTLING_EMITTER_STATE() turning ON for MP") 
		SET_STATIC_EMITTER_ENABLED("MP_ARM_WRESTLING_RADIO_01",TRUE)
		SET_STATIC_EMITTER_ENABLED("MP_ARM_WRESTLING_RADIO_02",TRUE)
		SET_STATIC_EMITTER_ENABLED("MP_ARM_WRESTLING_RADIO_03",TRUE)
		SET_STATIC_EMITTER_ENABLED("MP_ARM_WRESTLING_RADIO_04",TRUE)
		SET_STATIC_EMITTER_ENABLED("MP_ARM_WRESTLING_RADIO_05",TRUE)
		SET_STATIC_EMITTER_ENABLED("MP_ARM_WRESTLING_RADIO_06",TRUE)
		SET_STATIC_EMITTER_ENABLED("MP_ARM_WRESTLING_RADIO_07",TRUE)
		SET_STATIC_EMITTER_ENABLED("MP_ARM_WRESTLING_RADIO_08",TRUE)
		SET_STATIC_EMITTER_ENABLED("MP_ARM_WRESTLING_RADIO_09",TRUE)
		SET_STATIC_EMITTER_ENABLED("MP_ARM_WRESTLING_RADIO_10",TRUE)
		bTurnedOnArmRadioEmitters = TRUE
	ENDIF
ENDPROC

PROC SET_CINEMA_UNLOCK_FLAGS()
	//checks player has done the SP mission Michael 4 to set flag for action movie to be unlocked in MP
	IF GET_MISSION_COMPLETE_STATE(SP_MISSION_MICHAEL_4)
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCinemaUnlockBS,CINEMA_UNLOCK_SP_ACTION_MOVIE)
		PRINTLN("SET_CINEMA_UNLOCK_FLAGS: Local player has unlocked action movie setting unlock flag")
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCinemaUnlockBS,CINEMA_UNLOCK_SP_ACTION_MOVIE)
		PRINTLN("SET_CINEMA_UNLOCK_FLAGS: Local player has NOT unlocked action movie clearing unlock flag")
	ENDIF
ENDPROC

PROC DISABLE_BANK_SCENARIO_GROUPS()
	IF DOES_SCENARIO_GROUP_EXIST("Countryside_Banks")
		IF IS_SCENARIO_GROUP_ENABLED("Countryside_Banks")		
			SET_SCENARIO_GROUP_ENABLED("Countryside_Banks",FALSE)
			PRINTLN("DISABLE_BANK_SCENARIO_GROUPS: Disabled countryside_banks scenario group")
		ENDIF
	ENDIF
	IF DOES_SCENARIO_GROUP_EXIST("City_Banks")
		IF IS_SCENARIO_GROUP_ENABLED("City_Banks")		
			SET_SCENARIO_GROUP_ENABLED("City_Banks",FALSE)
			PRINTLN("DISABLE_BANK_SCENARIO_GROUPS: Disabled City_Banks scenario group")
		ENDIF
	ENDIF
	
ENDPROC

//if we need to restore the vehicle then set it
FUNC BOOL DEAL_WITH_TRANSITION_SESSIONS_RESTORE_VEHICLE_AND_WARP()

	//If we should restor a vehicle then restore it
	IF SHOULD_TRANSITION_SESSIONS_RESTORE_VEHICLE()
		PRINTLN("[WJK] - SAVE_CREATE_EOM_VEHICLE - SHOULD_TRANSITION_SESSIONS_RESTORE_VEHICLE = TRUE")
		IF CREATE_AND_ENTER_EOM_AMBIENT_VEHICLE(<<0.0,0.0,0.0>>) != eCREATEANDENTERAMBIENTVEHICLESTATUS_NO_RESULT
			CLEAR_TRANSITION_SESSIONS_RESTORE_VEHICLE()
			CLEAR_TRANSITION_SESSION_END_SESSION_POS()		
			RETURN FALSE
		ENDIF
	
	// If the shops are blocking the warp
	ELIF SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN(FALSE) AND NOT HAS_TRANSITION_SESSION_SHOP_STATE_INITIALISED()
		// This is where we would wait for the shop to set up and clear the flag...
		PRINTLN("DEAL_WITH_TRANSITION_SESSIONS_RESTORE_VEHICLE_AND_WARP - waiting for shop to restore...")
		PRINTLN("[WJK] - SAVE_CREATE_EOM_VEHICLE - SHOULD_TRANSITION_SESSIONS_RESTORE_VEHICLE = FALSE")
		CLEAR_TRANSITION_SESSION_END_SESSION_POS()
		
		IF NOT HAS_NET_TIMER_STARTED(g_sTransitionSessionData.sEndReserve.stShopRestoreTimer)
			START_NET_TIMER(g_sTransitionSessionData.sEndReserve.stShopRestoreTimer)
		ELIF HAS_NET_TIMER_EXPIRED(g_sTransitionSessionData.sEndReserve.stShopRestoreTimer, 10000)
			PRINTLN("DEAL_WITH_TRANSITION_SESSIONS_RESTORE_VEHICLE_AND_WARP - shop took longer than 10 seconds, clearing flags!")
			CLEAR_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
			SET_TRANSITION_SESSION_SHOP_STATE_INITIALISED()
		ENDIF
		
		RETURN FALSE

	//If the vehicle restore is over
	ELSE
		IF SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN(FALSE)
		AND g_SpawnData.NextSpawn.Location != SPAWN_LOCATION_INSIDE_PROPERTY
			//If it's not from the phone then don't clear
			IF g_sTransitionSessionData.sEndReserve.iRestoreShopStateScriptHash != HASH("appinternet")
				CLEAR_TRANSITION_SESSION_END_SESSION_POS()
				PRINTLN("CDM: CLEAR_TRANSITION_SESSION_END_SESSION_POS shop state.")
			ENDIF
		ENDIF
		
		//If the spawn has been done then pull the camera down
		IF IS_VECTOR_ZERO(GET_TRANSITION_SESSION_END_SESSION_POS())

			IF ENUM_TO_INT(g_JoinResponseEvent.nResponseCode) > 0
				WARNING_SCREEN_RETURN aWarningScreen = RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED_SCREEN_ONLY()
								
				IF aWarningScreen = WARNING_SCREEN_RETURN_NONE
					
					NET_NL()NET_PRINT("[TS] Freemode.sc -: Waiting on the player accepting joing failed warning screen: g_JoinResponseEvent.nResponseCode = ")NET_PRINT_INT(ENUM_TO_INT(g_JoinResponseEvent.nResponseCode))NET_PRINT(" (g_Private_BailWarningScreenEventParam = ")NET_PRINT_INT(g_Private_BailWarningScreenEventParam)NET_NL()
					RETURN FALSE
				ELSE
					NET_NL()NET_PRINT("[TS] Freemode.sc -:  ACCEPT has been pressed from RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED_SCREEN_ONLY resetting g_JoinResponseEvent.nResponseCode = RESPONSE_ACCEPT ")
					
					g_JoinResponseEvent.nResponseCode = RESPONSE_ACCEPT
				ENDIF
			ENDIF
			
			
		
		
			IF SET_SKYSWOOP_DOWN()
//				g_b_ReapplyStickySaveFailedFeed = FALSE //Commented out as it shows save successful after the xbox store when it still fails. 
				RETURN TRUE
			ENDIF
		//if we need to warp the player
		ELSE
			IF (g_sTransitionSessionData.bSpawnInApartment)
				IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_INSIDE_PROPERTY, FALSE, FALSE, FALSE, TRUE, TRUE, FALSE, FALSE, FALSE)
					CLEAR_TRANSITION_SESSION_END_SESSION_POS()
					g_sTransitionSessionData.bSpawnInApartment = FALSE
					RETURN FALSE
				ENDIF
			ELIF (g_sTransitionSessionData.bSpawnInGarage)
				IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_INSIDE_GARAGE, FALSE, FALSE, FALSE, TRUE, TRUE, FALSE, FALSE, FALSE)
					CLEAR_TRANSITION_SESSION_END_SESSION_POS()
					g_sTransitionSessionData.bSpawnInGarage = FALSE
					RETURN FALSE
				ENDIF				
			ELSE
				IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS, FALSE, FALSE, FALSE, TRUE, TRUE, FALSE, FALSE, FALSE)
					CLEAR_TRANSITION_SESSION_END_SESSION_POS()
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


#IF IS_DEBUG_BUILD
PROC RUN_NO_MORE_TUTORIALS_WARNING_FEED()

	IF g_b_HasSkippedTutorialDueToNoMoreTutorials = TRUE
			
		IF NOT IS_TRANSITION_ACTIVE()

			IF HAS_PLAYER_BEEN_IN_SESSION_THIS_LONG(10000)		
				IF GET_MP_BOOL_PLAYER_STAT(MPPLY_NO_MORE_TUTORIALS)
					IF IS_NET_PLAYER_OK(PLAYER_ID())
						
						PRINTLN("BC: RUN_NO_MORE_TUTORIALS_WARNING_FEED: - CALLED ")

						// "Your Archenemy ~a~ is currently in this game. "
						BEGIN_TEXT_COMMAND_THEFEED_POST("NOMORETUT")
							
						END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)	
						 
						g_b_HasSkippedTutorialDueToNoMoreTutorials = FALSE
						
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC
#ENDIF

// -------------------------------------------------------------------------------------------------------------------------------------------------------------------- \\
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------- \\
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------- \\
//																																										\\																																							\\
//																																										\\																																								\\
// 																			 Before Corona Vehicle																		\\
//																																										\\
//																				     Start																				\\
//																																										\\
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------- \\
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------- \\
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------- \\

PROC INFORM_PREVIOUS_OWNERS_VEHICLE_IS_MINE_NOW(VEHICLE_INDEX vehIndex)
	
	INT i
	PLAYER_INDEX playerId
	
	PRINTLN("[WJK] - before corona vehicle - MAINTAIN_SAVE_LAST_VEH_WHEN_ENTERING_CORONA - INFORM_PREVIOUS_OWNERS_VEHICLE_IS_MINE_NOW - being called.")
	
	REPEAT NUM_NETWORK_PLAYERS i
		
		PRINTLN("[WJK] - before corona vehicle - MAINTAIN_SAVE_LAST_VEH_WHEN_ENTERING_CORONA - INFORM_PREVIOUS_OWNERS_VEHICLE_IS_MINE_NOW - checking player ", i)
		
		playerId = INT_TO_NATIVE(PLAYER_INDEX, i)
		
		IF IS_NET_PLAYER_OK(playerId, FALSE)
			
			IF GET_PLAYERS_BEFORE_CORONA_VEH_DECORATOR_IS_SET(playerId,vehIndex)
				PRINTLN("[WJK] - before corona vehicle - MAINTAIN_SAVE_LAST_VEH_WHEN_ENTERING_CORONA - INFORM_PREVIOUS_OWNERS_VEHICLE_IS_MINE_NOW - decorator label exists on vehicle. Sending event to tell player ", i, " to cancel vehicle recreation upon corona exit, this car is mine now.")
				BROADCAST_SCRIPT_EVENT_CORONA_VEHICLE_WORLD_CHECK_IS_NO_LONGER_REQUIRED(playerId)
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	
ENDPROC

// Save last vehicle info here.
PROC  MAINTAIN_SAVE_LAST_VEH_WHEN_ENTERING_CORONA()
	
	BOOL bIsPersonalVehicle
	BOOL bIsModdedByAnotherPlayer
	BOOL bModdableVehicle
	BOOL bIAmDriver
	VEHICLE_INDEX vehIndex
	
	SWITCH g_TransitionSessionNonResetVars.iSaveBeforeCoronaVehicleStage
		
		CASE 0
			IF g_TransitionSessionNonResetVars.bSaveBeforeCoronaVehicle
				g_TransitionSessionNonResetVars.iSaveBeforeCoronaVehicleStage++
			ENDIF
		BREAK
		
		CASE 1
			
			vehIndex = GET_PLAYERS_LAST_VEHICLE()
			
			IF NOT g_TransitionSessionNonResetVars.bSavedLastVehBeforeJob
				
				g_TransitionSessionNonResetVars.sLastVehAfterJobData.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT
				
				IF DOES_ENTITY_EXIST(vehIndex)
					IF IS_VEHICLE_DRIVEABLE(vehIndex)
					
						IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT) 
		                	IF DECOR_EXIST_ON(vehIndex, "Player_Vehicle")
								bIsPersonalVehicle = TRUE
							ENDIF
						ENDIF
						
						IF DECOR_IS_REGISTERED_AS_TYPE("Player_Avenger", DECOR_TYPE_INT) 
		                	IF DECOR_EXIST_ON(vehIndex, "Player_Avenger")
								bIsPersonalVehicle = TRUE
							ENDIF
						ENDIF
						IF DECOR_IS_REGISTERED_AS_TYPE("Player_Truck", DECOR_TYPE_INT) 
		                	IF DECOR_EXIST_ON(vehIndex, "Player_Truck")
								bIsPersonalVehicle = TRUE
							ENDIF
						ENDIF
					
						IF DECOR_IS_REGISTERED_AS_TYPE("Veh_Modded_By_Player", DECOR_TYPE_INT) 
							IF DECOR_EXIST_ON(vehIndex, "Veh_Modded_By_Player") 
								IF DECOR_GET_INT(vehIndex, "Veh_Modded_By_Player") != NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
									PRINTLN("MAINTAIN_SAVE_LAST_VEH_WHEN_ENTERING_CORONA: bIsModdedByAnotherPlayer = TRUE - modded by another player")
									bIsModdedByAnotherPlayer = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_VEHICLE_BITSET_DECORATOR_SET(vehIndex, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
							PRINTLN("MAINTAIN_SAVE_LAST_VEH_WHEN_ENTERING_CORONA: bModdableVehicle = TRUE - vehicle is non-moddable.")
							bModdableVehicle = TRUE
						ENDIF
						
						IF GET_LAST_PED_IN_VEHICLE_SEAT(vehIndex) = PLAYER_PED_ID()
							bIAmDriver = TRUE
							INFORM_PREVIOUS_OWNERS_VEHICLE_IS_MINE_NOW(vehIndex)
						ENDIF
						
						IF NOT bIsPersonalVehicle
						AND NOT bIsModdedByAnotherPlayer
						AND NOT bModdableVehicle
							
							IF bIAmDriver
								
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehIndex), GET_PLAYER_COORDS(PLAYER_ID())) <= 100.0
									
//									IF NOT NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(vehIndex)
//										
//										SET_ENTITY_AS_MISSION_ENTITY(vehIndex, TRUE)
//										PRINTLN("[WJK] - before corona vehicle - MAINTAIN_SAVE_LAST_VEH_WHEN_ENTERING_CORONA - last vehicle not registered with this script, setting as mission entity.")
//										
//									ELSE
									
									IF NETWORK_HAS_CONTROL_OF_ENTITY(vehIndex)
										
										IF DECOR_IS_REGISTERED_AS_TYPE("CreatedByPegasus", DECOR_TYPE_BOOL)
											IF DECOR_EXIST_ON(vehIndex, "CreatedByPegasus")
												IF DECOR_GET_BOOL(vehIndex, "CreatedByPegasus")
													g_TransitionSessionNonResetVars.bIsPegasusVeh = TRUE
													PRINTLN("[WJK] - before corona vehicle - MAINTAIN_SAVE_LAST_VEH_WHEN_ENTERING_CORONA - vehicle has decorator CreatedByPegasus, setting bIsPegasusVeh = TRUE.")
												ENDIF
											ENDIF
										ENDIF
										
										SET_PLAYERS_BEFORE_CORONA_VEH_DECORATOR(PLAYER_ID(),vehIndex)
										
										GET_VEHICLE_SETUP_MP(vehIndex, g_TransitionSessionNonResetVars.sLastVehAfterJobData)
										g_TransitionSessionNonResetVars.vLastVehicleBeforeCoronaSavePosition = GET_ENTITY_COORDS(vehIndex)
										g_TransitionSessionNonResetVars.fLastVehicleBeforeCoronaSaveHeading = GET_ENTITY_HEADING(vehIndex)
										g_TransitionSessionNonResetVars.bSavedLastVehBeforeJob = TRUE
										g_TransitionSessionNonResetVars.viLastVehicleBeforeCorona = vehIndex
//										g_TransitionSessionNonResetVars.niCoronaLastVehicle = VEH_TO_NET(vehIndex)
//										SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(g_TransitionSessionNonResetVars.niCoronaLastVehicle, PLAYER_ID(), TRUE)
										g_TransitionSessionNonResetVars.iSaveBeforeCoronaVehicleStage++
										NET_PRINT("[WJK] - before corona vehicle - saved last vehicle data.")NET_NL()
										
//									ENDIF
									
										IF NOT HAS_NET_TIMER_STARTED(g_TransitionSessionNonResetVars.stSaveBeforeCoronaVehTimeout)
											START_NET_TIMER(g_TransitionSessionNonResetVars.stSaveBeforeCoronaVehTimeout)
										ELSE
											IF HAS_NET_TIMER_EXPIRED(g_TransitionSessionNonResetVars.stSaveBeforeCoronaVehTimeout, 5000)
												g_TransitionSessionNonResetVars.iSaveBeforeCoronaVehicleStage++
												PRINTLN("[WJK] - before corona vehicle - MAINTAIN_SAVE_LAST_VEH_WHEN_ENTERING_CORONA - last vehicle still not registered with this script?!")
											ENDIF
										ENDIF
									
									ELSE
										
										NETWORK_REQUEST_CONTROL_OF_ENTITY(vehIndex)
										NET_PRINT("[WJK] - before corona vehicle - MAINTAIN_SAVE_LAST_VEH_WHEN_ENTERING_CORONA - requesting control of last vehicle so we can add decorator that will sync to all players.")NET_NL()
										
									ENDIF
									
								ELSE
									g_TransitionSessionNonResetVars.iSaveBeforeCoronaVehicleStage++
									NET_PRINT("[WJK] - before corona vehicle - MAINTAIN_SAVE_LAST_VEH_WHEN_ENTERING_CORONA - last vehicle is > 100m away, not saving for recreation after job.")NET_NL()
								ENDIF
								
							ELSE
								g_TransitionSessionNonResetVars.iSaveBeforeCoronaVehicleStage++
								NET_PRINT("[WJK] - before corona vehicle - MAINTAIN_SAVE_LAST_VEH_WHEN_ENTERING_CORONA - I am not driver of my last vehicle, not saving.")NET_NL()
							ENDIF
							
						ELSE
							g_TransitionSessionNonResetVars.iSaveBeforeCoronaVehicleStage++
							IF bIsPersonalVehicle
								NET_PRINT("[WJK] - before corona vehicle - MAINTAIN_SAVE_LAST_VEH_WHEN_ENTERING_CORONA - last vehicle is personal vehicle, not saving.")NET_NL()
							ELIF bIsModdedByAnotherPlayer
								NET_PRINT("[WJK] - before corona vehicle - MAINTAIN_SAVE_LAST_VEH_WHEN_ENTERING_CORONA - last vehicle was modded by someone else, not saving.")NET_NL()
							ELIF bModdableVehicle
								NET_PRINT("[WJK] - before corona vehicle - MAINTAIN_SAVE_LAST_VEH_WHEN_ENTERING_CORONA - last vehicle is non-moddable, not saving.")NET_NL()
							ENDIF
						ENDIF
						
					ELSE
						g_TransitionSessionNonResetVars.iSaveBeforeCoronaVehicleStage++
						NET_PRINT("[WJK] - before corona vehicle - MAINTAIN_SAVE_LAST_VEH_WHEN_ENTERING_CORONA - last vehicle is not driveable.")NET_NL()
					ENDIF
					
				ELSE
					g_TransitionSessionNonResetVars.iSaveBeforeCoronaVehicleStage++
					NET_PRINT("[WJK] - before corona vehicle - MAINTAIN_SAVE_LAST_VEH_WHEN_ENTERING_CORONA - last vehicle does not exist.")NET_NL()
				ENDIF
				
			ENDIF
					
		BREAK
		
		CASE 2
			// Done. 
		BREAK
		
	ENDSWITCH
	
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING GET_CORONA_VEH_WORLD_CHECK_RESULT_NAME(eCORONA_VEH_WORLD_CHECK_RESULT eState)
	
	SWITCH eState
		CASE eCORONAVEHWORLDCHECKRESULT_NO_REQUEST_ACTIVE RETURN "NO_REQUEST_ACTIVE"
		CASE eCORONAVEHWORLDCHECKRESULT_PENDING			  RETURN "PENDING"
		CASE eCORONAVEHWORLDCHECKRESULT_NOBODY_KNOWS	  RETURN "NOBODY_KNOWS"
		CASE eCORONAVEHWORLDCHECKRESULT_SOMEBODY_KNOWS	  RETURN "SOMEBODY_KNOWS"
	ENDSWITCH
	
	RETURN "NOT_IN_SWITCH"
	
ENDFUNC
#ENDIF

FUNC eCORONA_VEH_WORLD_CHECK_RESULT GET_REQUEST_CORONA_VEH_WORLD_CHECK_RESULT()
	
	RETURN g_structCoronaVehicleWorldCheck.eCoronaVehWorldCheckResult
	
ENDFUNC

PROC SET_CORONA_VEH_WORLD_CHECK_RESULT(eCORONA_VEH_WORLD_CHECK_RESULT eState)
	
	#IF IS_DEBUG_BUILD
	IF GET_REQUEST_CORONA_VEH_WORLD_CHECK_RESULT() != eState
		PRINTLN("[WJK] - [CORONA_VEH_WORLD_CHECK] - SET_CORONA_VEH_WORLD_CHECK_RESULT called. Setting state to ", GET_CORONA_VEH_WORLD_CHECK_RESULT_NAME(eState))
	ENDIF
	#ENDIF
	
	g_structCoronaVehicleWorldCheck.eCoronaVehWorldCheckResult = eState
	
ENDPROC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Gets the name of a request corona vehicle world check state as a string.
/// PARAMS:
///    eState - state to get as a string.
/// RETURNS:
///    STRING - state enum as a string.
FUNC STRING GET_REQUEST_CORONA_VEH_WORLD_CHECK_STATE_NAME(eMAINTAIN_CORONA_VEHICLE_WORLD_CHECK_REQUEST_STATE eState)
	SWITCH eState
		CASE eMAINTAINCORONAVEHICLEWORLDCHECKREQUESTSTATE_NO_REQUEST			RETURN "NO_REQUEST"
		CASE eMAINTAINCORONAVEHICLEWORLDCHECKREQUESTSTATE_SEND_REQUEST			RETURN "SEND_REQUEST"
		CASE eMAINTAINCORONAVEHICLEWORLDCHECKREQUESTSTATE_WAITING_FOR_REPLIES	RETURN "WAITING_FOR_REPLIES"
		CASE eMAINTAINCORONAVEHICLEWORLDCHECKREQUESTSTATE_RESULT				RETURN "RESULT"
	ENDSWITCH
	RETURN "NOT_IN_SWITCH"
ENDFUNC
#ENDIF

/// PURPOSE:
///    Sets the state of the request corona veh world check.
/// PARAMS:
///    eState - state to set to.
PROC SET_REQUEST_CORONA_VEHICLE_WORLD_CHECK_STATE(eMAINTAIN_CORONA_VEHICLE_WORLD_CHECK_REQUEST_STATE eState)
	
	#IF IS_DEBUG_BUILD
	IF g_structCoronaVehicleWorldCheck.eRequestCoronaVehWorldCheckState != eState
		PRINTLN("[WJK] - [CORONA_VEH_WORLD_CHECK] - SET_REQUEST_CORONA_VEHICLE_WORLD_CHECK_STATE called. Setting state to ", GET_REQUEST_CORONA_VEH_WORLD_CHECK_STATE_NAME(eState))
	ENDIF
	#ENDIF
	
	g_structCoronaVehicleWorldCheck.eRequestCoronaVehWorldCheckState = eState
	
ENDPROC

/// PURPOSE:
///    Gets the current state of the request corona veh world check.
/// RETURNS:
///    State enum. 
FUNC eMAINTAIN_CORONA_VEHICLE_WORLD_CHECK_REQUEST_STATE GET_REQUEST_CORONA_VEH_WORLD_STATE_CHECK_STATE()
	
	RETURN g_structCoronaVehicleWorldCheck.eRequestCoronaVehWorldCheckState
	
ENDFUNC

/// PURPOSE:
///    Requests a world check to see if the decorator passed exists on any vehicle in the game. Will assert if called while system is mid-check (must be in state eMAINTAINCORONAVEHICLEWORLDCHECKREQUESTSTATE_NO_REQUEST).
PROC REQUEST_WORLD_CHECK_FOR_CORONA_VEHICLE()
		
	IF GET_REQUEST_CORONA_VEH_WORLD_STATE_CHECK_STATE() = eMAINTAINCORONAVEHICLEWORLDCHECKREQUESTSTATE_NO_REQUEST
		SET_REQUEST_CORONA_VEHICLE_WORLD_CHECK_STATE(eMAINTAINCORONAVEHICLEWORLDCHECKREQUESTSTATE_SEND_REQUEST)
	ENDIF
	
ENDPROC

FUNC BOOL HAVE_ALL_PLAYERS_RESPONDED_TO_CORONA_WORLD_VEHICLE_CHECK()
	
	INT i
	
	REPEAT NUM_NETWORK_PLAYERS i
		IF (INT_TO_NATIVE(PLAYER_INDEX, i) != INVALID_PLAYER_INDEX())
			IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, i))
				IF (g_structCoronaVehicleWorldCheck.iCoronaVehWorldCheckReplyValue[i] = CORONA_VEHICLE_WORLD_CHECK_RESPONSE_NONE)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL GET_DO_ANY_PLAYERS_KNOW_OF_CORONA_VEH()
	
	INT i
	
	REPEAT NUM_NETWORK_PLAYERS i
		IF (g_structCoronaVehicleWorldCheck.iCoronaVehWorldCheckReplyValue[i] = CORONA_VEHICLE_WORLD_CHECK_RESPONSE_KNOW_ABOUT_VEH)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DOES_MY_BEFORE_CORONA_VEHICLE_EXIST()
	
	VEHICLE_INDEX vehicleId
	
	vehicleId = GET_CLOSEST_VEHICLE(	g_TransitionSessionNonResetVars.vLastVehicleBeforeCoronaSavePosition,
										4.0,
										g_TransitionSessionNonResetVars.sLastVehAfterJobData.VehicleSetup.eModel,
										VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES 
										| VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES 
										| VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES 
										| VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS 
										| VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER                                                           
										| VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED                                   
										| VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_WITH_PEDS_ENTERING_OR_EXITING                                         
										| VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK      
										| VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_NON_DEFAULT_TASK	)
	
	PRINTLN("[WJK] - before corona vehicle - DOES_MY_BEFORE_CORONA_VEHICLE_EXIST - GET_CLOSEST_VEHICLE being called with: ", g_TransitionSessionNonResetVars.vLastVehicleBeforeCoronaSavePosition)
	PRINTLN("[WJK] - before corona vehicle - DOES_MY_BEFORE_CORONA_VEHICLE_EXIST - GET_CLOSEST_VEHICLE returned id: ", NATIVE_TO_INT(vehicleId))
	
	IF DOES_ENTITY_EXIST(vehicleId)
		PRINTLN("[WJK] - before corona vehicle - DOES_MY_BEFORE_CORONA_VEHICLE_EXIST - GET_CLOSEST_VEHICLE vehicle exists.")
		IF NOT IS_ENTITY_DEAD(vehicleId)
			PRINTLN("[WJK] - before corona vehicle - DOES_MY_BEFORE_CORONA_VEHICLE_EXIST - GET_CLOSEST_VEHICLE vehicle no dead.")
			RETURN TRUE
		ELSE
			PRINTLN("[WJK] - before corona vehicle - DOES_MY_BEFORE_CORONA_VEHICLE_EXIST - GET_CLOSEST_VEHICLE vehicle is dead.")
		ENDIF
	ELSE
		PRINTLN("[WJK] - before corona vehicle - DOES_MY_BEFORE_CORONA_VEHICLE_EXIST - GET_CLOSEST_VEHICLE vehicle does not exist.")
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(g_TransitionSessionNonResetVars.niCoronaLastVehicle)
		PRINTLN("[WJK] - before corona vehicle - DOES_MY_BEFORE_CORONA_VEHICLE_EXIST - niCoronaLastVehicle exists, returning TRUE.")
		RETURN TRUE
	ENDIF
	
	IF (GET_REQUEST_CORONA_VEH_WORLD_CHECK_RESULT() = eCORONAVEHWORLDCHECKRESULT_SOMEBODY_KNOWS)
		PRINTLN("[WJK] - before corona vehicle - DOES_MY_BEFORE_CORONA_VEHICLE_EXIST - GET_REQUEST_CORONA_VEH_WORLD_CHECK_RESULT = SOMEBODY_KNOWS, returning TRUE.")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC CLEAR_REMOVE_CORONA_VEHICLE_WORLD_CHECK_DATA(INT iPlayer)
	
	CLEAR_BIT(g_TransitionSessionNonResetVars.iRemoveBeforeCoronaDecorator, iPlayer)
	g_TransitionSessionNonResetVars.viFoundCoronaVehId[iPlayer] = NULL
	
ENDPROC

PROC MAINTAIN_REMOVING_CORONA_VEHICLE_WORLD_CHECK_DECORATORS()
	
	INT i
	
	REPEAT NUM_NETWORK_PLAYERS i
		
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.iRemoveBeforeCoronaDecorator, i)

			IF DOES_ENTITY_EXIST(g_TransitionSessionNonResetVars.viFoundCoronaVehId[i])
				IF NETWORK_HAS_CONTROL_OF_ENTITY(g_TransitionSessionNonResetVars.viFoundCoronaVehId[i])
					IF GET_PLAYERS_BEFORE_CORONA_VEH_DECORATOR_IS_SET(INT_TO_NATIVE(PLAYER_INDEX, i),g_TransitionSessionNonResetVars.viFoundCoronaVehId[i])
						CLEAR_PLAYERS_BEFORE_CORONA_VEH_DECORATOR(g_TransitionSessionNonResetVars.viFoundCoronaVehId[i])
					ELSE
						CLEAR_REMOVE_CORONA_VEHICLE_WORLD_CHECK_DATA(i)
					ENDIF
				ENDIF
			ELSE
				CLEAR_REMOVE_CORONA_VEHICLE_WORLD_CHECK_DATA(i)
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Maintains requests for a world check on the before corona vehicle. Waits for REQUEST_WORLD_CHECK_FOR_CORONA_VEHICLE() to be called somewhere, which sets the state to send request.
///    Send request send a script event to all players requesting a check for the decorator on the vehicle and then the state moves to waiting for replies.
///    Once reply events have been received from all active players, or the system times out, we display the result based on if anyone replied saying they know about that decorator.
PROC MAINTAIN_REQUEST_CORONA_VEH_WORLD_CHECK()
	
	SWITCH GET_REQUEST_CORONA_VEH_WORLD_STATE_CHECK_STATE()
	
		CASE eMAINTAINCORONAVEHICLEWORLDCHECKREQUESTSTATE_NO_REQUEST
			
			// Waiting for someone somewhere to call REQUEST_WORLD_CHECK_FOR_CORONA_VEHICLE(), which will change the state to eMAINTAINCORONAVEHICLEWORLDCHECKREQUESTSTATE_SEND_REQUEST.
			
		BREAK
		
		CASE eMAINTAINCORONAVEHICLEWORLDCHECKREQUESTSTATE_SEND_REQUEST
			
			// Send request here.
			BROADCAST_SCRIPT_EVENT_REQUEST_CORONA_VEHICLE_WORLD_CHECK()
			SET_CORONA_VEH_WORLD_CHECK_RESULT(eCORONAVEHWORLDCHECKRESULT_PENDING)
			SET_REQUEST_CORONA_VEHICLE_WORLD_CHECK_STATE(eMAINTAINCORONAVEHICLEWORLDCHECKREQUESTSTATE_WAITING_FOR_REPLIES)
			
		BREAK
		
		CASE eMAINTAINCORONAVEHICLEWORLDCHECKREQUESTSTATE_WAITING_FOR_REPLIES
			
			// Loop through participants waiting on results.
			IF HAVE_ALL_PLAYERS_RESPONDED_TO_CORONA_WORLD_VEHICLE_CHECK()
			// OR timeout
				IF GET_DO_ANY_PLAYERS_KNOW_OF_CORONA_VEH()
					SET_CORONA_VEH_WORLD_CHECK_RESULT(eCORONAVEHWORLDCHECKRESULT_SOMEBODY_KNOWS)
				ELSE
					SET_CORONA_VEH_WORLD_CHECK_RESULT(eCORONAVEHWORLDCHECKRESULT_NOBODY_KNOWS)
				ENDIF
				SET_REQUEST_CORONA_VEHICLE_WORLD_CHECK_STATE(eMAINTAINCORONAVEHICLEWORLDCHECKREQUESTSTATE_RESULT)
			ENDIF
			
		BREAK
		
		CASE eMAINTAINCORONAVEHICLEWORLDCHECKREQUESTSTATE_RESULT
			
			// Wait for reset here.
			
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC MAINTAIN_CREATE_BEFORE_CORONA_VEHICLE()
	
	VEHICLE_INDEX veh
	
	IF g_TransitionSessionNonResetVars.bCreateLastVehBeforeCoronaForSameSession
		IF g_TransitionSessionNonResetVars.bSavedLastVehBeforeJob
			SWITCH g_TransitionSessionNonResetVars.iCreateLastVehBeforeCoronaStage
				CASE 0
					RESET_NET_TIMER(stBeforeCoronaVehicleDelay)
					START_NET_TIMER(stBeforeCoronaVehicleDelay)
					PRINTLN("[WJK] - before corona vehicle - stBeforeCoronaVehicleDelay has been started")
					REQUEST_WORLD_CHECK_FOR_CORONA_VEHICLE()
					g_TransitionSessionNonResetVars.iCreateLastVehBeforeCoronaStage = 1
				BREAK
				CASE 1
					IF HAS_NET_TIMER_STARTED(stBeforeCoronaVehicleDelay)
						IF HAS_NET_TIMER_EXPIRED(stBeforeCoronaVehicleDelay, g_sMPTunables.iCoronaCreateVehicleDelay)
							IF NOT NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING()
							AND NOT NETWORK_IS_IN_TUTORIAL_SESSION()
							AND NETWORK_IS_GAME_IN_PROGRESS()
							AND ((GET_REQUEST_CORONA_VEH_WORLD_CHECK_RESULT() != eCORONAVEHWORLDCHECKRESULT_PENDING) OR HAS_NET_TIMER_EXPIRED(stBeforeCoronaVehicleDelay, 15000)) // If we haven't moved on from peding within 15 seconds, continue on.
								IF NOT DOES_MY_BEFORE_CORONA_VEHICLE_EXIST()
									PRINTLN("[WJK] - before corona vehicle - MAINTAIN_CREATE_BEFORE_CORONA_VEHICLE (GET_REQUEST_CORONA_VEH_WORLD_CHECK_RESULT() != eCORONAVEHWORLDCHECKRESULT_PENDING), DOES_MY_BEFORE_CORONA_VEHICLE_EXIST() = FALSE.")
									IF NOT g_TransitionSessionNonResetVars.bIsPegasusVeh
										PRINTLN("[WJK] - before corona vehicle - MAINTAIN_CREATE_BEFORE_CORONA_VEHICLE bIsPegasusVeh = FALSE.")
										REQUEST_LOAD_MODEL(g_TransitionSessionNonResetVars.sLastVehAfterJobData.VehicleSetup.eModel)
										IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(g_TransitionSessionNonResetVars.vLastVehicleBeforeCoronaSavePosition)
											g_TransitionSessionNonResetVars.iCreateLastVehBeforeCoronaStage = 3
											PRINTLN("[WJK] - before corona vehicle - MAINTAIN_CREATE_BEFORE_CORONA_VEHICLE original location ok for creation - ", g_TransitionSessionNonResetVars.vLastVehicleBeforeCoronaSavePosition)
										ELSE
											PRINTLN("[WJK] - before corona vehicle - MAINTAIN_CREATE_BEFORE_CORONA_VEHICLE original location not ok for creation - ", g_TransitionSessionNonResetVars.vLastVehicleBeforeCoronaSavePosition)
											g_TransitionSessionNonResetVars.iCreateLastVehBeforeCoronaStage = 2
											g_TransitionSessionNonResetVars.vLastVehicleBeforeCoronaSavePosition = <<0.0,0.0,0.0>>
											g_TransitionSessionNonResetVars.fLastVehicleBeforeCoronaSaveHeading = 0.0
										ENDIF
									ELSE
										PRINTLN("[WJK] - before corona vehicle - MAINTAIN_CREATE_BEFORE_CORONA_VEHICLE bIsPegasusVeh = TRUE.")
										IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iRestoreLastVehicleBitset, TRAN_SESS_NON_RESET_LAST_VEHICLE_NEED_PEGASUS_SPAWN)
											SET_BIT(g_TransitionSessionNonResetVars.iRestoreLastVehicleBitset, TRAN_SESS_NON_RESET_LAST_VEHICLE_NEED_PEGASUS_SPAWN)
											g_TransitionSessionNonResetVars.iCreateLastVehBeforeCoronaStage = 5
											PRINTLN("[WJK] - before corona vehicle - MAINTAIN_CREATE_BEFORE_CORONA_VEHICLE - bIsPegasusVeh = TRUE, setting bit TRAN_SESS_NON_RESET_LAST_VEHICLE_NEED_PEGASUS_SPAWN.")
											PRINTLN("[WJK] - before corona vehicle - MAINTAIN_CREATE_BEFORE_CORONA_VEHICLE - bIsPegasusVeh = FALSE, going to iCreateLastVehBeforeCoronaStage stage 5.")
										ENDIF
									ENDIF
								ELSE
									PRINTLN("[WJK] - before corona vehicle - viLastVehicleBeforeCorona still exists in this session, skipping creation and resetting data - there's nothing to create.")
									RESET_BEFORE_CORONA_VEHICLE_DATA()
								ENDIF
							ENDIF
						ENDIF
					ELSE
						SCRIPT_ASSERT("MAINTAIN_CREATE_BEFORE_CORONA_VEHICLE - > case 0, stBeforeCoronaVehicleDelay has not been started.")
					ENDIF
				BREAK
				CASE 2
					REQUEST_LOAD_MODEL(g_TransitionSessionNonResetVars.sLastVehAfterJobData.VehicleSetup.eModel)
					
					VEHICLE_SPAWN_LOCATION_PARAMS Params
					Params.fMinDistFromCoords = 0
					Params.fMaxDistance = 50

					IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(GET_PLAYER_PERCEIVED_COORDS(PLAYER_ID()), <<0.0, 0.0, 0.0>>, g_TransitionSessionNonResetVars.sLastVehAfterJobData.VehicleSetup.eModel, TRUE, g_TransitionSessionNonResetVars.vLastVehicleBeforeCoronaSavePosition, g_TransitionSessionNonResetVars.fLastVehicleBeforeCoronaSaveHeading, Params)
						g_TransitionSessionNonResetVars.iCreateLastVehBeforeCoronaStage++
						PRINTLN("[WJK] - before corona vehicle - MAINTAIN_CREATE_BEFORE_CORONA_VEHICLE got vehicle spawn coords - ", g_TransitionSessionNonResetVars.vLastVehicleBeforeCoronaSavePosition)
					ENDIF
				BREAK
				CASE 3
					IF REQUEST_LOAD_MODEL(g_TransitionSessionNonResetVars.sLastVehAfterJobData.VehicleSetup.eModel)
						IF CREATE_NET_VEHICLE(	g_TransitionSessionNonResetVars.niBeforeCoronaVeh, 
												g_TransitionSessionNonResetVars.sLastVehAfterJobData.VehicleSetup.eModel, 
												g_TransitionSessionNonResetVars.vLastVehicleBeforeCoronaSavePosition, 
												g_TransitionSessionNonResetVars.fLastVehicleBeforeCoronaSaveHeading, 
												FALSE)
							PRINTLN("[WJK] - before corona vehicle - MAINTAIN_CREATE_BEFORE_CORONA_VEHICLE created vehicle.")
							veh = NET_TO_VEH(g_TransitionSessionNonResetVars.niBeforeCoronaVeh)
							SET_VEHICLE_SETUP_MP(veh, g_TransitionSessionNonResetVars.sLastVehAfterJobData)
							SET_MODEL_AS_NO_LONGER_NEEDED(g_TransitionSessionNonResetVars.sLastVehAfterJobData.VehicleSetup.eModel)
							DECOR_SET_INT(veh, "Not_Allow_As_Saved_Veh", 1) // url:bugstar:3464068 - [PUBLIC] [EXPLOIT] Street vehicle dupe by timing their entry to LSC and customising the car including changing license plate.
							g_TransitionSessionNonResetVars.iCreateLastVehBeforeCoronaStage++
						ENDIF
					ENDIF
				BREAK
				CASE 4
					IF NOT HAS_NET_TIMER_STARTED(g_TransitionSessionNonResetVars.stMarkLastVehilceBeforeCoronaAsnoLongerNeededTimer)
						START_NET_TIMER(g_TransitionSessionNonResetVars.stMarkLastVehilceBeforeCoronaAsnoLongerNeededTimer)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(g_TransitionSessionNonResetVars.stMarkLastVehilceBeforeCoronaAsnoLongerNeededTimer, 10000)
							IF NETWORK_DOES_NETWORK_ID_EXIST(g_TransitionSessionNonResetVars.niBeforeCoronaVeh)
								veh = NET_TO_VEH(g_TransitionSessionNonResetVars.niBeforeCoronaVeh)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(veh)
								PRINTLN("[WJK] - before corona vehicle - MAINTAIN_CREATE_BEFORE_CORONA_VEHICLE - called SET_VEHICLE_AS_NO_LONGER_NEEDED on created car.")
							ENDIF
							RESET_BEFORE_CORONA_VEHICLE_DATA()
						ENDIf
					ENDIF
				BREAK
				CASE 5
					IF IS_BIT_SET(g_TransitionSessionNonResetVars.iRestoreLastVehicleBitset, TRAN_SESS_NON_RESET_LAST_VEHICLE_PEGASUS_SPAWN_SUCCESS)
						PRINTLN("[WJK] - before corona vehicle - MAINTAIN_CREATE_BEFORE_CORONA_VEHICLE - TRAN_SESS_NON_RESET_LAST_VEHICLE_PEGASUS_SPAWN_SUCCESS bit is set.")
						RESET_BEFORE_CORONA_VEHICLE_DATA()
					ENDIF
					IF IS_BIT_SET(g_TransitionSessionNonResetVars.iRestoreLastVehicleBitset, TRAN_SESS_NON_RESET_LAST_VEHICLE_PEGASUS_SPAWN_FAIL)
						PRINTLN("[WJK] - before corona vehicle - MAINTAIN_CREATE_BEFORE_CORONA_VEHICLE - TRAN_SESS_NON_RESET_LAST_VEHICLE_PEGASUS_SPAWN_FAIL bit is set.")
						RESET_BEFORE_CORONA_VEHICLE_DATA()
					ENDIF
				BREAK
			ENDSWITCH
		ELSE
			PRINTLN("[WJK] - before corona vehicle - bSavedLastVehBeforeJob = FALSE, skipping creation and resetting data - there's nothing to create.")
			RESET_BEFORE_CORONA_VEHICLE_DATA()
		ENDIF
	ENDIF
	
ENDPROC

// -------------------------------------------------------------------------------------------------------------------------------------------------------------------- \\
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------- \\
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------- \\
//																																										\\																																							\\
//																																										\\																																								\\
// 																			 Before Corona Vehicle																		\\
//																																										\\
//																				     End																				\\
//																																										\\
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------- \\
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------- \\
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------- \\


PROC MAINTAIN_EARN_JOBSHARE_CASH_TRANSACTIONS
	INT i
	
	REPEAT 5 i
		IF IS_BIT_SET(g_sEarnJobshareTransaction[i].iBitset, CI_EARN_JOBSHARE_REQUESTED)
			IF IS_CASH_TRANSACTION_COMPLETE(g_sEarnJobshareTransaction[i].index)
				PRINTLN("[CASH] MAINTAIN_EARN_JOBSHARE_CASH_TRANSACTIONS - Transaction ", g_sEarnJobshareTransaction[i].index ," complete!")				
				IF GET_CASH_TRANSACTION_STATUS(g_sEarnJobshareTransaction[i].index) = CASH_TRANSACTION_STATUS_SUCCESS
					PRINTLN("[CASH] MAINTAIN_EARN_JOBSHARE_CASH_TRANSACTIONS - Transaction ", g_sEarnJobshareTransaction[i].index ," successful!")
					ALTER_RECEIVE_LJ_CASH(g_sEarnJobshareTransaction[i].event.iCashToGive)						// If transaction succeeds
					NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(g_sEarnJobshareTransaction[i].index))					
					GIVE_LOCAL_PLAYER_CASH(g_sEarnJobshareTransaction[i].event.iCashToGive, g_sEarnJobshareTransaction[i].event.banimate)
					NETWORK_RECEIVE_PLAYER_JOBSHARE_CASH(g_sEarnJobshareTransaction[i].event.iCashToGive, g_sEarnJobshareTransaction[i].gamerHandle)			
					PRINT_TICKER_WITH_PLAYER_NAME_AND_INT("PIM_TIGC", g_sEarnJobshareTransaction[i].event.FromPlayerIndex, g_sEarnJobshareTransaction[i].event.iCashToGive)	//~a~ ~s~gave you ~g~$~1~
					
					//IF g_sEarnJobshareTransaction[i].bShouldRefund												// Refund excess cash to player
						//BROADCAST_GIVE_PLAYER_REFUND_FROM_LAST_JOB(g_sEarnJobshareTransaction[i].event.FromPlayerIndex, g_sEarnJobshareTransaction[i].iCashToRefund)
					//ENDIF
					
					DELETE_CASH_TRANSACTION(g_sEarnJobshareTransaction[i].index)
					PRINTLN("[CASH] MAINTAIN_EARN_JOBSHARE_CASH_TRANSACTIONS - Deleting transaction ", g_sEarnJobshareTransaction[i].index ,"!")
					CLEAR_BIT(g_sEarnJobshareTransaction[i].iBitset, CI_EARN_JOBSHARE_REQUESTED)	
				ELSE																							// If transaction fails
					PRINTLN("[CASH] MAINTAIN_EARN_JOBSHARE_CASH_TRANSACTIONS - Transaction ", g_sEarnJobshareTransaction[i].index ," failed!")
																	// Refund full amount if transaction fails
					//BROADCAST_GIVE_PLAYER_REFUND_FROM_LAST_JOB(g_sEarnJobshareTransaction[i].event.FromPlayerIndex, g_sEarnJobshareTransaction[i].event.iCashToGive)
					PRINTLN("[CASH] MAINTAIN_EARN_JOBSHARE_CASH_TRANSACTIONS - Refunding ", g_sEarnJobshareTransaction[i].event.iCashToGive ," to owner!")
									
//					NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(g_sEarnJobshareTransaction[i].index))	
					DELETE_CASH_TRANSACTION(g_sEarnJobshareTransaction[i].index)
					PRINTLN("[CASH] MAINTAIN_EARN_JOBSHARE_CASH_TRANSACTIONS - Deleting transaction ", g_sEarnJobshareTransaction[i].index ,"!")
					CLEAR_BIT(g_sEarnJobshareTransaction[i].iBitset, CI_EARN_JOBSHARE_REQUESTED)
				ENDIF
			ELSE
				PRINTLN("[CASH] MAINTAIN_EARN_JOBSHARE_CASH_TRANSACTIONS - Transaction ", g_sEarnJobshareTransaction[i].index ," is pending...")
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

PROC MAINTAIN_DROP_CASH_TRANSACTIONS
	IF IS_BIT_SET(g_iDropCashBit, biDROP_CASH_TRANSACTION_REQUESTED)
		IF IS_CASH_TRANSACTION_COMPLETE(g_sCashDropTransaction.iIndex)
			PRINTLN("[CASH] - MAINTAIN_DROP_CASH_TRANSACTIONS - IS_CASH_TRANSACTION_COMPLETE - TRUE")
			IF GET_CASH_TRANSACTION_STATUS(g_sCashDropTransaction.iIndex) = CASH_TRANSACTION_STATUS_SUCCESS
				PRINTLN("[CASH] - MAINTAIN_DROP_CASH_TRANSACTIONS - GET_CASH_TRANSACTION_STATUS - SUCCESS - transaction Succeeded, creating pickup and removing cash from player.")
				
				NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(g_sCashDropTransaction.iIndex))
				
				NETWORK_SPENT_CASH_DROP(g_sCashDropTransaction.iCashToDrop)
				GIVE_LOCAL_PLAYER_CASH(-g_sCashDropTransaction.iCashToDrop)
				SET_FM_PICKUP_ORIENTATION_FLAGS(g_sCashDropTransaction.iPlacementFlags)
				CREATE_AMBIENT_PICKUP(PICKUP_MONEY_VARIABLE, g_sCashDropTransaction.vDropCoords, g_sCashDropTransaction.iPlacementFlags, g_sCashDropTransaction.iCashToDrop)

				IF IS_BIT_SET(g_iDropCashDecor, biDROP_CASH_DECOR)
					IF DECOR_IS_REGISTERED_AS_TYPE("cashondeadbody", DECOR_TYPE_INT)
						IF NOT DECOR_EXIST_ON(GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID()), "cashondeadbody")
							DECOR_SET_INT(PLAYER_PED_ID(),"cashondeadbody",g_sCashDropTransaction.iCashToDrop)
							PRINTLN("Created decorator ",g_sCashDropTransaction.iCashToDrop)						
						ENDIF
					ENDIF
					CLEAR_BIT(g_iDropCashDecor, biDROP_CASH_DECOR)
				ENDIF
				
				DELETE_CASH_TRANSACTION(g_sCashDropTransaction.iIndex)
				CLEAR_BIT(g_iDropCashBit, biDROP_CASH_TRANSACTION_REQUESTED)
				g_sCashDropTransaction.iCashToDrop = 0
				g_sCashDropTransaction.iPlacementFlags = 0
				
			ELIF GET_CASH_TRANSACTION_STATUS(g_sCashDropTransaction.iIndex) = CASH_TRANSACTION_STATUS_FAIL
				PRINTLN("[CASH] - MAINTAIN_DROP_CASH_TRANSACTIONS - GET_CASH_TRANSACTION_STATUS - FAILED - transaction failed, not creating pickup.")
				DELETE_CASH_TRANSACTION(g_sCashDropTransaction.iIndex)
				CLEAR_BIT(g_iDropCashBit, biDROP_CASH_TRANSACTION_REQUESTED)
				g_sCashDropTransaction.iCashToDrop = 0
				g_sCashDropTransaction.iPlacementFlags = 0
			ELSE
				PRINTLN("[CASH] - MAINTAIN_DROP_CASH_TRANSACTIONS - Transaction is pending or NULL")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_WEARING_TRENCHCOAT()
	//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " IS_PLAYER_WEARING_TRENCHCOAT")
	IF !IS_PED_FEMALE(PLAYER_PED_ID())
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " IS_PLAYER_WEARING_TRENCHCOAT - !IS_PED_FEMALE(PLAYER_PED_ID())")
		INT iDrawable = GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB)
		INT iTexture = GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB)
		INT iJBIBHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(PED_COMP_JBIB), iDrawable, iTexture)
		IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iJBIBHash, DLC_RESTRICTION_TAG_LUXE2_DRAW_0, ENUM_TO_INT(SHOP_PED_COMPONENT))
		OR DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iJBIBHash, DLC_RESTRICTION_TAG_LUXE2_DRAW_1, ENUM_TO_INT(SHOP_PED_COMPONENT))
		      //Wearing a trenchcoat
			  //CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " IS_PLAYER_WEARING_TRENCHCOAT DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG")
			  RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DUFFEL_BAG_EQUIPPED()
	IF GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2) = 1
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_TRENCHCOAT_LOCO()
	STRING sMoveClipset = "move_m@tool_belt@multiplayer"
	IF DOES_ANIM_DICT_EXIST(sMoveClipset)
		IF IS_PLAYER_WEARING_TRENCHCOAT()
		AND !IS_DUFFEL_BAG_EQUIPPED()
			IF !bIsTrenchcoatLocoSet
				REQUEST_CLIP_SET(sMoveClipset)
				IF HAS_CLIP_SET_LOADED(sMoveClipset)
					CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " MAINTAIN_TRENCHCOAT_LOCO - bIsTrenchcoatLocoSet = TRUE")
					SET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID(), sMoveClipset)
					bIsTrenchcoatLocoSet = TRUE
				ENDIF
			ENDIF
		ELSE
			IF HAS_CLIP_SET_LOADED(sMoveClipset)
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " MAINTAIN_TRENCHCOAT_LOCO - bIsTrenchcoatLocoSet = FALSE")
				RESET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID())
				REMOVE_CLIP_SET(sMoveClipset)
				bIsTrenchcoatLocoSet = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL IS_GAMER_HANDLE_ABOVE_ME_IN_CREATE_SURVIVAL_REWARD_VEHICLES_GH_ARRAY(GAMER_HANDLE eHandle)
	
	INT i
	
	IF NETWORK_IS_HANDLE_VALID(eHandle, SIZE_OF(eHandle))
		REPEAT 4 i
			IF i < g_TransitionSessionNonResetVars.sPostMissionCleanupData.iCreateSurvivalRewardVehsMyGamerHandleArrayPos
				IF NETWORK_IS_HANDLE_VALID(	g_TransitionSessionNonResetVars.sPostMissionCleanupData.eCreateSurvivalRewardVehsGamerHandles[i], 
											SIZE_OF(g_TransitionSessionNonResetVars.sPostMissionCleanupData.eCreateSurvivalRewardVehsGamerHandles[i]))
					IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sPostMissionCleanupData.eCreateSurvivalRewardVehsGamerHandles[i], eHandle)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN FALSE
	
ENDFUNC



PROC SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(INTERIOR_INSTANCE_INDEX &Interior, VECTOR VecInCoors, STRING Name)
	IF NOT IS_VALID_INTERIOR(Interior)
		Interior = GET_INTERIOR_AT_COORDS_WITH_TYPE(VecInCoors, Name)
	ENDIF
ENDPROC

PROC InitialiseInteriorInstancesForDisabledJumping()	
	IF NOT(g_bHasInitialisedInteriorInstances)
		
		IF NOT HAS_NET_TIMER_STARTED(iInitInteriorInstanceIndexTimer)
			PRINTLN("Initialise_Global_Interior_Instances()- iInitInteriorInstanceIndexTimer started")
			START_NET_TIMER(iInitInteriorInstanceIndexTimer)
			EXIT
		ELIF NOT HAS_NET_TIMER_EXPIRED(iInitInteriorInstanceIndexTimer, 10000)
			EXIT
		ENDIF
		
		PRINTLN("Initialise_Global_Interior_Instances()- iInitInteriorInstanceIndexTimer Finished")
				
		#IF IS_DEBUG_BUILD
		INT i
		#ENDIF
			
		// mod shops
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_ModShop[0], GET_SHOP_COORDS(CARMOD_SHOP_01_AP), GET_SHOP_INTERIOR_TYPE(CARMOD_SHOP_01_AP))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_ModShop[1], GET_SHOP_COORDS(CARMOD_SHOP_05_ID2), GET_SHOP_INTERIOR_TYPE(CARMOD_SHOP_05_ID2))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_ModShop[2], GET_SHOP_COORDS(CARMOD_SHOP_06_BT1), GET_SHOP_INTERIOR_TYPE(CARMOD_SHOP_06_BT1))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_ModShop[3], GET_SHOP_COORDS(CARMOD_SHOP_07_CS1), GET_SHOP_INTERIOR_TYPE(CARMOD_SHOP_07_CS1))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_ModShop[4], GET_SHOP_COORDS(CARMOD_SHOP_08_CS6), GET_SHOP_INTERIOR_TYPE(CARMOD_SHOP_08_CS6))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_ModShop[5], GET_SHOP_COORDS(CARMOD_SHOP_SUPERMOD), GET_SHOP_INTERIOR_TYPE(CARMOD_SHOP_SUPERMOD))
		
		#IF IS_DEBUG_BUILD
			REPEAT COUNT_OF(g_Interior_ModShop) i
				IF NOT IS_VALID_INTERIOR(g_Interior_ModShop[i])
					PRINTLN("Initialise_Global_Interior_Instances()- could not get valid interior for g_Interior_ModShop ", i)
					SCRIPT_ASSERT("Initialise_Global_Interior_Instances() - could not get valid interior for g_Interior_ModShop")
				ELSE
					PRINTLN("Initialise_Global_Interior_Instances()- valid interior for g_Interior_ModShop ", i, " id = ", NATIVE_TO_INT(g_Interior_ModShop[i]))
				ENDIF
			ENDREPEAT
		#ENDIF
		
		// tattoo shops
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_TattooShop[0], GET_SHOP_COORDS(TATTOO_PARLOUR_01_HW), GET_SHOP_INTERIOR_TYPE(TATTOO_PARLOUR_01_HW))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_TattooShop[1], GET_SHOP_COORDS(TATTOO_PARLOUR_02_SS), GET_SHOP_INTERIOR_TYPE(TATTOO_PARLOUR_02_SS))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_TattooShop[2], GET_SHOP_COORDS(TATTOO_PARLOUR_03_PB), GET_SHOP_INTERIOR_TYPE(TATTOO_PARLOUR_03_PB))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_TattooShop[3], GET_SHOP_COORDS(TATTOO_PARLOUR_04_VC), GET_SHOP_INTERIOR_TYPE(TATTOO_PARLOUR_04_VC))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_TattooShop[4], GET_SHOP_COORDS(TATTOO_PARLOUR_05_ELS), GET_SHOP_INTERIOR_TYPE(TATTOO_PARLOUR_05_ELS))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_TattooShop[5], GET_SHOP_COORDS(TATTOO_PARLOUR_06_GOH), GET_SHOP_INTERIOR_TYPE(TATTOO_PARLOUR_06_GOH))
		
		#IF IS_DEBUG_BUILD
			REPEAT COUNT_OF(g_Interior_TattooShop) i
				IF NOT IS_VALID_INTERIOR(g_Interior_TattooShop[i])
					PRINTLN("Initialise_Global_Interior_Instances()- could not get valid interior for g_Interior_TattooShop ", i)
					SCRIPT_ASSERT("Initialise_Global_Interior_Instances() - could not get valid interior for g_Interior_TattooShop")
				ELSE
					PRINTLN("Initialise_Global_Interior_Instances()- valid interior for g_Interior_TattooShop ", i, " id = ", NATIVE_TO_INT(g_Interior_TattooShop[i]))
				ENDIF
			ENDREPEAT
		#ENDIF	
		
		// strip club
		g_Interior_StripClub = GET_INTERIOR_AT_COORDS(<<116.7, -1287.5, 29.7>>)
		IF NOT IS_VALID_INTERIOR(g_Interior_StripClub)
			PRINTLN("Initialise_Global_Interior_Instances()- could not get valid interior for g_Interior_StripClub ")
			SCRIPT_ASSERT("Initialise_Global_Interior_Instances() - could not get valid interior for g_Interior_StripClub")
		ELSE
			PRINTLN("Initialise_Global_Interior_Instances()- valid interior for g_Interior_StripClub id = ", NATIVE_TO_INT(g_Interior_StripClub))
		ENDIF
		
		// barber shops
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_Barbers[0], GET_SHOP_COORDS(HAIRDO_SHOP_01_BH), GET_SHOP_INTERIOR_TYPE(HAIRDO_SHOP_01_BH))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_Barbers[1], GET_SHOP_COORDS(HAIRDO_SHOP_02_SC), GET_SHOP_INTERIOR_TYPE(HAIRDO_SHOP_02_SC))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_Barbers[2], GET_SHOP_COORDS(HAIRDO_SHOP_03_V), GET_SHOP_INTERIOR_TYPE(HAIRDO_SHOP_03_V))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_Barbers[3], GET_SHOP_COORDS(HAIRDO_SHOP_04_SS), GET_SHOP_INTERIOR_TYPE(HAIRDO_SHOP_04_SS))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_Barbers[4], GET_SHOP_COORDS(HAIRDO_SHOP_05_MP), GET_SHOP_INTERIOR_TYPE(HAIRDO_SHOP_05_MP))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_Barbers[5], GET_SHOP_COORDS(HAIRDO_SHOP_06_HW), GET_SHOP_INTERIOR_TYPE(HAIRDO_SHOP_06_HW))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_Barbers[6], GET_SHOP_COORDS(HAIRDO_SHOP_07_PB), GET_SHOP_INTERIOR_TYPE(HAIRDO_SHOP_07_PB))
		
		#IF IS_DEBUG_BUILD
			REPEAT COUNT_OF(g_Interior_Barbers) i
				IF NOT IS_VALID_INTERIOR(g_Interior_Barbers[i])
					PRINTLN("Initialise_Global_Interior_Instances()- could not get valid interior for g_Interior_Barbers ", i)
					SCRIPT_ASSERT("Initialise_Global_Interior_Instances() - could not get valid interior for g_Interior_Barbers")
				ELSE
					PRINTLN("Initialise_Global_Interior_Instances()- valid interior for g_Interior_Barbers ", i, " id = ", NATIVE_TO_INT(g_Interior_Barbers[i]))
				ENDIF
			ENDREPEAT
		#ENDIF	
		
		// clothes shops
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_ClothesShop[0], GET_SHOP_COORDS(CLOTHES_SHOP_L_01_SC), GET_SHOP_INTERIOR_TYPE(CLOTHES_SHOP_L_01_SC))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_ClothesShop[1], GET_SHOP_COORDS(CLOTHES_SHOP_L_02_GS), GET_SHOP_INTERIOR_TYPE(CLOTHES_SHOP_L_02_GS))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_ClothesShop[2], GET_SHOP_COORDS(CLOTHES_SHOP_L_03_DT), GET_SHOP_INTERIOR_TYPE(CLOTHES_SHOP_L_03_DT))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_ClothesShop[3], GET_SHOP_COORDS(CLOTHES_SHOP_L_04_CS), GET_SHOP_INTERIOR_TYPE(CLOTHES_SHOP_L_04_CS))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_ClothesShop[4], GET_SHOP_COORDS(CLOTHES_SHOP_L_05_GSD), GET_SHOP_INTERIOR_TYPE(CLOTHES_SHOP_L_05_GSD))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_ClothesShop[5], GET_SHOP_COORDS(CLOTHES_SHOP_L_06_VC), GET_SHOP_INTERIOR_TYPE(CLOTHES_SHOP_L_06_VC))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_ClothesShop[6], GET_SHOP_COORDS(CLOTHES_SHOP_L_07_PB), GET_SHOP_INTERIOR_TYPE(CLOTHES_SHOP_L_07_PB))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_ClothesShop[7], GET_SHOP_COORDS(CLOTHES_SHOP_M_01_SM), GET_SHOP_INTERIOR_TYPE(CLOTHES_SHOP_M_01_SM))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_ClothesShop[8], GET_SHOP_COORDS(CLOTHES_SHOP_M_03_H), GET_SHOP_INTERIOR_TYPE(CLOTHES_SHOP_M_03_H))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_ClothesShop[9], GET_SHOP_COORDS(CLOTHES_SHOP_M_04_HW), GET_SHOP_INTERIOR_TYPE(CLOTHES_SHOP_M_04_HW))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_ClothesShop[10], GET_SHOP_COORDS(CLOTHES_SHOP_M_05_GOH), GET_SHOP_INTERIOR_TYPE(CLOTHES_SHOP_M_05_GOH))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_ClothesShop[11], GET_SHOP_COORDS(CLOTHES_SHOP_H_01_BH), GET_SHOP_INTERIOR_TYPE(CLOTHES_SHOP_H_01_BH))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_ClothesShop[12], GET_SHOP_COORDS(CLOTHES_SHOP_H_02_B), GET_SHOP_INTERIOR_TYPE(CLOTHES_SHOP_H_02_B))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_ClothesShop[13], GET_SHOP_COORDS(CLOTHES_SHOP_H_03_MW), GET_SHOP_INTERIOR_TYPE(CLOTHES_SHOP_H_03_MW))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_ClothesShop[14], GET_SHOP_COORDS(TATTOO_PARLOUR_06_GOH), GET_SHOP_INTERIOR_TYPE(TATTOO_PARLOUR_06_GOH))
		
		#IF IS_DEBUG_BUILD
			REPEAT COUNT_OF(g_Interior_ClothesShop) i
				IF NOT IS_VALID_INTERIOR(g_Interior_ClothesShop[i])
					PRINTLN("Initialise_Global_Interior_Instances()- could not get valid interior for g_Interior_ClothesShop ", i)
					SCRIPT_ASSERT("Initialise_Global_Interior_Instances() - could not get valid interior for g_Interior_ClothesShop")
				ELSE
					PRINTLN("Initialise_Global_Interior_Instances()- valid interior for g_Interior_ClothesShop ", i, " id = ", NATIVE_TO_INT(g_Interior_ClothesShop[i]))
				ENDIF
			ENDREPEAT
		#ENDIF					
		
		// ammunation
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_Ammunation[0], GET_SHOP_COORDS(GUN_SHOP_01_DT), GET_SHOP_INTERIOR_TYPE(GUN_SHOP_01_DT))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_Ammunation[1], GET_SHOP_COORDS(GUN_SHOP_02_SS), GET_SHOP_INTERIOR_TYPE(GUN_SHOP_02_SS))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_Ammunation[2], GET_SHOP_COORDS(GUN_SHOP_03_HW), GET_SHOP_INTERIOR_TYPE(GUN_SHOP_03_HW))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_Ammunation[3], GET_SHOP_COORDS(GUN_SHOP_04_ELS), GET_SHOP_INTERIOR_TYPE(GUN_SHOP_04_ELS))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_Ammunation[4], GET_SHOP_COORDS(GUN_SHOP_05_PB), GET_SHOP_INTERIOR_TYPE(GUN_SHOP_05_PB))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_Ammunation[5], GET_SHOP_COORDS(GUN_SHOP_06_LS), GET_SHOP_INTERIOR_TYPE(GUN_SHOP_06_LS))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_Ammunation[6], GET_SHOP_COORDS(GUN_SHOP_07_MW), GET_SHOP_INTERIOR_TYPE(GUN_SHOP_07_MW))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_Ammunation[7], GET_SHOP_COORDS(GUN_SHOP_08_CS), GET_SHOP_INTERIOR_TYPE(GUN_SHOP_08_CS))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_Ammunation[8], GET_SHOP_COORDS(GUN_SHOP_09_GOH), GET_SHOP_INTERIOR_TYPE(GUN_SHOP_09_GOH))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_Ammunation[9], GET_SHOP_COORDS(GUN_SHOP_10_VWH), GET_SHOP_INTERIOR_TYPE(GUN_SHOP_10_VWH))
		SAFE_GET_INTERIOR_AT_COORDS_WITH_TYPE(g_Interior_Ammunation[10], GET_SHOP_COORDS(GUN_SHOP_11_ID1), GET_SHOP_INTERIOR_TYPE(GUN_SHOP_11_ID1))
		
		#IF IS_DEBUG_BUILD
			REPEAT COUNT_OF(g_Interior_Ammunation) i
				IF NOT IS_VALID_INTERIOR(g_Interior_Ammunation[i])
					PRINTLN("Initialise_Global_Interior_Instances()- could not get valid interior for g_Interior_Ammunation ", i)
						SCRIPT_ASSERT("Initialise_Global_Interior_Instances() - could not get valid interior for g_Interior_Ammunation")
				ELSE
					PRINTLN("Initialise_Global_Interior_Instances()- valid interior for g_Interior_Ammunation ", i, " id = ", NATIVE_TO_INT(g_Interior_Ammunation[i]))
				ENDIF
			ENDREPEAT
		#ENDIF	
		
		g_bHasInitialisedInteriorInstances = TRUE
		NET_PRINT("InitialiseInteriorInstancesForDisabledJumping - finished intialising") NET_NL()
	ELSE
		//NET_PRINT("InitialiseInteriorInstancesForDisabledJumping - already initialised") NET_NL()
	ENDIF
ENDPROC

FUNC BOOL IsPlayerInAnyInteriorWithDisabledJumping()
	INT i
	INTERIOR_INSTANCE_INDEX InteriorID = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
	IF IS_VALID_INTERIOR(InteriorID)
		// mod shops
		REPEAT COUNT_OF(g_Interior_ModShop) i
			IF (InteriorID = g_Interior_ModShop[i])
				NET_PRINT("IsPlayerInAnyInteriorWithDisabledJumping - player inside mod shop") NET_NL()
				RETURN(TRUE)
			ENDIF
		ENDREPEAT
		// tattoo shops
		REPEAT COUNT_OF(g_Interior_TattooShop) i
			IF (InteriorID = g_Interior_TattooShop[i])
				NET_PRINT("IsPlayerInAnyInteriorWithDisabledJumping - player inside tattoo shop") NET_NL()
				RETURN(TRUE)
			ENDIF
		ENDREPEAT
		// strip club
		IF (InteriorID = g_Interior_StripClub)
			NET_PRINT("IsPlayerInAnyInteriorWithDisabledJumping - player inside strip club") NET_NL()
			RETURN(TRUE)
		ENDIF
		// barbers
		REPEAT COUNT_OF(g_Interior_Barbers) i
			IF (InteriorID = g_Interior_Barbers[i])
				NET_PRINT("IsPlayerInAnyInteriorWithDisabledJumping - player inside barbers") NET_NL()
				RETURN(TRUE)
			ENDIF
		ENDREPEAT
		// clothes shops
		REPEAT COUNT_OF(g_Interior_ClothesShop) i
			IF (InteriorID = g_Interior_ClothesShop[i])
				NET_PRINT("IsPlayerInAnyInteriorWithDisabledJumping - player inside clothes shop") NET_NL()
				RETURN(TRUE)
			ENDIF
		ENDREPEAT
		// ammunation
		REPEAT COUNT_OF(g_Interior_Ammunation) i
			IF (InteriorID = g_Interior_Ammunation[i])
				NET_PRINT("IsPlayerInAnyInteriorWithDisabledJumping - player inside ammunation") NET_NL()
				RETURN(TRUE)
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN(FALSE)
ENDFUNC

// to fix bug 1819453
PROC MaintainDisableJumpInInteriors()
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	
		IF IsPlayerInAnyInteriorWithDisabledJumping()
			IF !g_bEnableJumpingOffHalfTrackInProperty
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)	
			ENDIF
		ENDIF
	

			// mod shops			
//			IF IS_ENTITY_IN_AREA(PLAYER_PED_ID(), <<738.050, -1092.800, 28.625>>, <<724.250, -1063.00, 20.0>>) 
//			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-353.625, -127.925, 43.0>>, <<-321.35, -139.8, 38.0>>, 19.475) 
//			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1165.1, -2020.7, 17.5>>, <<-1140.425, -1996.475, 11.8>>, 19.475)
//			OR IS_ENTITY_IN_AREA(PLAYER_PED_ID(), <<1170.982, 2634.461, 36.08>>, <<1190.357, 2644.986, 41.538>>)
			
	ENDIF
ENDPROC

PROC MAINTAIN_DISABLE_CAR_ENTRY_DURING_WARP()
	IF IS_PLAYER_TELEPORT_ACTIVE()
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
		PRINTLN("MAINTAIN_DISABLE_CAR_ENTRY_DURING_WARP- disabling player enter vehicle while teleport is active")
	ENDIF
ENDPROC

SCRIPT_TIMER invisibleWithControlTimer
PROC PREVENT_INVISIBLE_PLAYER_WITH_CONTROL()
	IF NOT g_bRunInvisPlayerFailsafe
		RESET_NET_TIMER(invisibleWithControlTimer)
		EXIT
	ENDIF
	IF NOT HAS_NET_TIMER_STARTED(invisibleWithControlTimer)
		START_NET_TIMER(invisibleWithControlTimer,TRUE)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(invisibleWithControlTimer,2000,TRUE)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF NOT IS_ENTITY_VISIBLE(PLAYER_PED_ID())
					IF NOT NETWORK_IS_IN_MP_CUTSCENE()
					AND NOT IS_PLAYER_SPECTATING(PLAYER_ID())
					AND IS_SKYSWOOP_AT_GROUND()
					AND GET_SKYFREEZE_STAGE() = SKYFREEZE_NONE
					AND NOT IS_PLAYER_IN_CORONA()
					AND NOT IS_TRANSITION_ACTIVE()
					AND NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bGangOpsTransition
					AND NOT IS_LOCAL_PLAYER_USING_RCBANDITO()
						IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
							IF HAS_NET_TIMER_EXPIRED(invisibleWithControlTimer,5000,TRUE)
								SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
								PRINTLN("PREVENT_INVISIBLE_PLAYER_WITH_CONTROL- triggered forcing player visible")
								RESET_NET_TIMER(invisibleWithControlTimer)
							ENDIF
						ELSE
							//PRINTLN("PREVENT_INVISIBLE_PLAYER_WITH_CONTROL- reset 1")
							RESET_NET_TIMER(invisibleWithControlTimer)
						ENDIF
					ELSE
						//PRINTLN("PREVENT_INVISIBLE_PLAYER_WITH_CONTROL- reset 2")
						RESET_NET_TIMER(invisibleWithControlTimer)
					ENDIF
				ELSE
					//PRINTLN("PREVENT_INVISIBLE_PLAYER_WITH_CONTROL- reset 3")
					RESET_NET_TIMER(invisibleWithControlTimer)
				ENDIF
			ELSE
				//PRINTLN("PREVENT_INVISIBLE_PLAYER_WITH_CONTROL- reset 4")
				RESET_NET_TIMER(invisibleWithControlTimer)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_CLUBHOUSE_GUEST_BIKE_BAIL_CLEANUP()
	IF IS_PLAYER_REQUESTED_FREEMODE_TO_CLEANUP_CLUBHOUSE_GUEST_BIKE(PLAYER_ID())
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV))
				SET_ENTITY_COORDS(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV),g_vOfficeGargeSlotCoords)
				SET_ENTITY_HEADING(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV),g_fOfficeGarageSlotHeading)
				SET_ENTITY_VISIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV), TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV),0)
				SET_REQUEST_FREEMODE_TO_CLEANUP_CLUBHOUSE_GUEST_BIKE(FALSE)
			ELSE
				NETWORK_REQUEST_CONTROL_OF_ENTITY(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

CONST_INT VEHICLE_MINE_LONG_PRESS		300

BOOL bMineHeld = FALSE
BOOL bMineRequested = FALSE

MODEL_NAMES eMineModel = INT_TO_ENUM(MODEL_NAMES, HASH("W_EX_VEHICLEMINE"))

SCRIPT_TIMER sMinePressDelay

FUNC WEAPON_TYPE GET_VEHICLE_MINE_TYPE(VEHICLE_INDEX vehTemp)
	IF IS_VEHICLE_MODEL(vehTemp, RCBANDITO)
		SWITCH GET_VEHICLE_MOD(vehTemp, MOD_WING_R)
			CASE 0	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_KINETIC_RC
			CASE 1	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_EMP_RC
		ENDSWITCH
	ELIF SHOULD_USE_DPADR_FOR_MINE(vehTemp)
		IF IS_VEHICLE_MODEL(vehTemp, JB7002)
			SWITCH GET_VEHICLE_MOD(vehTemp, MOD_WING_R)
				CASE 0	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_SPIKE
				CASE 1	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_SLICK	
			ENDSWITCH
		ELIF NOT IS_VEHICLE_MODEL(vehTemp, RCBANDITO)
			SWITCH GET_VEHICLE_MOD(vehTemp, MOD_WING_R)
				CASE 0	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_KINETIC
				CASE 1	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_SPIKE
				CASE 2	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_EMP
				CASE 3	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_SLICK
				CASE 4	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_TAR
			ENDSWITCH
		ENDIF
	ENDIF
	
	RETURN WEAPONTYPE_DLC_VEHICLE_MINE
ENDFUNC

SCRIPT_CONTROL_HOLD_TIMER vehicleMineControl
FUNC BOOL IS_PLAYER_VEHICLE_MINE_CONTROL_PRESSED(VEHICLE_INDEX pedVehicle, CONTROL_TYPE control, CONTROL_ACTION action)
	IF IS_VEHICLE_DRIVEABLE(pedVehicle)
		IF IS_VEHICLE_MODEL(pedVehicle, DOMINATOR4)	
			vehicleMineControl.action = action
			vehicleMineControl.control = control
			RETURN IS_CONTROL_HELD(vehicleMineControl)
		ELSE
			RETURN IS_DISABLED_CONTROL_JUST_PRESSED(control, action)
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_VEHICLE_HAVE_SAW_BLADE(VEHICLE_INDEX vehTemp)
	IF DOES_ENTITY_EXIST(vehTemp)
		MODEL_NAMES eVehicleModel = GET_ENTITY_MODEL(vehTemp)
		
		IF eVehicleModel = ZR380
		OR eVehicleModel = ZR3802
		OR eVehicleModel = ZR3803
		OR eVehicleModel = DEATHBIKE
		OR eVehicleModel = DEATHBIKE2
		OR eVehicleModel = DEATHBIKE3
		OR eVehicleModel = IMPERATOR
		OR eVehicleModel = IMPERATOR2
		OR eVehicleModel = IMPERATOR3
		OR eVehicleModel = SLAMVAN4
		OR eVehicleModel = SLAMVAN5
		OR eVehicleModel = SLAMVAN6
			IF GET_VEHICLE_MOD(vehTemp, MOD_CHASSIS4) > -1
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_VEHICLE_MINES()
	BOOL bUnloadMineModel = FALSE
	
	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
	AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
		IF HAS_NET_TIMER_STARTED(sMinePressDelay)
		AND HAS_NET_TIMER_EXPIRED(sMinePressDelay, 750)
			RESET_NET_TIMER(sMinePressDelay)
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
			
			IF IS_VEHICLE_DRIVEABLE(vehTemp)
			AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
				IF CAN_VEHICLE_USE_MINES(vehTemp)
					
					IF NOT SHOULD_USE_DPADR_FOR_MINE(vehTemp)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
					ENDIF
					
					eMineModel = INT_TO_ENUM(MODEL_NAMES, HASH("W_EX_VEHICLEMINE"))

					IF IS_MODEL_VALID(eMineModel)
						IF NOT bMineRequested
							PRINTLN("[VEHICLE_MINES] MAINTAIN_VEHICLE_MINES - Requesting mine model")
							
							REQUEST_MODEL(eMineModel)
							
							bMineRequested = TRUE
						ELSE
							IF HAS_MODEL_LOADED(eMineModel)
								
								CONTROL_ACTION mineControl = INPUT_VEH_HORN
								IF SHOULD_USE_DPADR_FOR_MINE(vehTemp)
									mineControl = INPUT_CONTEXT
									
									IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
										mineControl = INPUT_FRONTEND_X
									ENDIF
								ENDIF
								
								vehicleMineControl.action = mineControl
								vehicleMineControl.control = PLAYER_CONTROL
								
								IF NOT bMineHeld
									IF IS_CONTROL_HELD(vehicleMineControl)
										bMineHeld = TRUE
									ENDIF
								ELSE
									IF NOT IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, mineControl)
									AND NOT IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, mineControl)
									AND NOT IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, mineControl)
										bMineHeld = FALSE
									ENDIF
								ENDIF
								
								IF IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, mineControl)
								AND NOT bMineHeld
								AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
								AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
								AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
								AND NOT IS_PLAYER_IN_A_MOD_SHOP(PLAYER_ID())
								AND NOT IS_PAUSE_MENU_ACTIVE()
								AND NOT HAS_NET_TIMER_STARTED(sMinePressDelay)
									IF !DOES_VEHICLE_HAVE_MINE_AMO(vehTemp)
										PRINTLN("[VEHICLE_MINES] MAINTAIN_VEHICLE_MINES - out of ammo")
										
										PLAY_SOUND_FRONTEND(-1, "rc_mines_empty", "DLC_AW_RCBandito_Mine_Sounds")
										
										START_NET_TIMER(sMinePressDelay)
										
										EXIT
									ENDIF

									VECTOR vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight
									DETERMINE_4_BOTTOM_VECTORS_FOR_VEHICLE_WEAPON(vehTemp, GET_ENTITY_MODEL(vehTemp), vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight)
									
									VECTOR vB1 = GET_INTERP_POINT_VECTOR_FOR_VEHICLE_WEAPON(vFrontBottomLeft, vFrontBottomRight, 0.0, 1.0, 0.5)
									VECTOR vB2 = GET_INTERP_POINT_VECTOR_FOR_VEHICLE_WEAPON(vBackBottomLeft, vBackBottomRight, 0.0, 1.0, 0.5)
									
									vB1.z += 0.2
									vB2.z += 0.2
									
									FLOAT fOffset = 0.7
									
									IF GET_ENTITY_MODEL(vehTemp) = SPEEDO4
										fOffset = 1.0
									ENDIF
									
									IF DOES_VEHICLE_HAVE_SAW_BLADE(vehTemp)
										fOffset = 1.0
									ENDIF
									
									VECTOR vB3 = GET_INTERP_POINT_VECTOR_FOR_VEHICLE_WEAPON(vB1, vB2, 0.0, 1.0, fOffset)
									
									vB1.z -= 0.2
									vB2.z -= 0.2
									
									VECTOR vB4 = GET_INTERP_POINT_VECTOR_FOR_VEHICLE_WEAPON(vB1, vB2, 0.0, 1.0, fOffset + 0.1)
									
									IF SHOULD_USE_DPADR_FOR_MINE(vehTemp)
									AND NOT IS_VEHICLE_MODEL(vehTemp, RCBANDITO)
									AND NETWORK_IS_ACTIVITY_SESSION()
										SET_VEHICLE_BOMB_AMMO(vehTemp, GET_VEHICLE_BOMB_AMMO(vehTemp) - 1)
									ENDIF
									
									SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vB3, vB4, 0, TRUE, GET_VEHICLE_MINE_TYPE(vehTemp), PLAYER_PED_ID(), DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
									
									IF IS_VEHICLE_MODEL(vehTemp, RCBANDITO)
										g_iRCVehicleMineNum ++
									ENDIF
									
									PRINTLN("[VEHICLE_MINES] MAINTAIN_VEHICLE_MINES - Firing a mine")
									
									START_NET_TIMER(sMinePressDelay)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[VEHICLE_MINES] MAINTAIN_VEHICLE_MINES - Model is invalid")
						
						eMineModel = INT_TO_ENUM(MODEL_NAMES, HASH("W_EX_VEHICLEMINE"))
						
						bUnloadMineModel = TRUE
					ENDIF
				ELSE
					bUnloadMineModel = TRUE
				ENDIF
			ELSE
				bUnloadMineModel = TRUE
			ENDIF
		ELSE
			bUnloadMineModel = TRUE
		ENDIF
	ELSE
		bUnloadMineModel = TRUE
	ENDIF
	
	IF bUnloadMineModel
	AND bMineRequested
		PRINTLN("[VEHICLE_MINES] MAINTAIN_VEHICLE_MINES - Unloading mine model")
		
		IF IS_MODEL_VALID(eMineModel)
			SET_MODEL_AS_NO_LONGER_NEEDED(eMineModel)
		ENDIF
		
		bMineRequested = FALSE
	ENDIF
ENDPROC

CONST_INT AIR_BOMBS_BS_CARPET_BOMB_TRIGGERED		0
CONST_INT AIR_BOMBS_BS_MODEL_REQUESTED				1
CONST_INT AIR_BOMBS_BS_CAMERA_CREATED				2
CONST_INT AIR_BOMBS_BS_DOORS_OPEN					3
CONST_INT AIR_BOMBS_BS_HELP_TEXT_SHOWN_1			4
CONST_INT AIR_BOMBS_BS_HELP_TEXT_SHOWN_2			5
CONST_INT AIR_BOMBS_BS_BOMB_FIRED					6
CONST_INT AIR_BOMBS_BS_CAMERA_CREATED_2				7
CONST_INT AIR_BOMBS_BS_DISABLE_VEHICLE_WEAPON		8

BOOL bCounterMeasureButtonReleased = FALSE
BOOL bBombDoorButtonReleased = FALSE
BOOL bFollowAircraftHeading = TRUE

CAMERA_INDEX cBombCamera[2]

ENTITY_INDEX entLastBomb

INT iAirBombsBS
INT iBombsFired = 0

MODEL_NAMES eBombModel = INT_TO_ENUM(MODEL_NAMES, HASH("w_smug_bomb_01"))
MODEL_NAMES eFlareModel = INT_TO_ENUM(MODEL_NAMES, HASH("w_pi_flaregun_shell"))

SCRIPT_TIMER sBombPressDelay
SCRIPT_TIMER sCarpetBombDelay
SCRIPT_TIMER sBombDoorPressDelay
SCRIPT_TIMER sFlareCooldownSoundTimer

STRING strPTFXAsset = "scr_ar_planes"
STRING strSmokeFX 	= "scr_ar_trail_smoke"

PROC FIRE_CHAFF(VEHICLE_INDEX vehPlane)
	IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehPlane)
	OR IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(vehPlane)
	OR g_bAllowCountermeasureAmmo
		IF GET_VEHICLE_COUNTERMEASURE_AMMO(vehPlane) <= 0
			PRINTLN("[AIR_COUNTERMEASURES] FIRE_CHAFF - out of ammo")
			
			PLAY_SOUND_FRONTEND(-1, "chaff_empty", "DLC_SM_Countermeasures_Sounds")
			
			START_NET_TIMER(g_sCountermeasurePressDelay)
			
			EXIT
		ENDIF
	ENDIF
	
	VECTOR vPlaneRot = GET_ENTITY_ROTATION(vehPlane)
	
	VECTOR vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight
	DETERMINE_4_BOTTOM_VECTORS_FOR_VEHICLE_WEAPON(vehPlane, GET_ENTITY_MODEL(vehPlane), vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight)
	
	FIRE_SPECIFIED_CHAFF(vehPlane, 0, vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight, vPlaneRot) // Mid Left
	FIRE_SPECIFIED_CHAFF(vehPlane, 1, vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight, vPlaneRot) // Mid Right
	FIRE_SPECIFIED_CHAFF(vehPlane, 2, vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight, vPlaneRot) // Far Left
	FIRE_SPECIFIED_CHAFF(vehPlane, 3, vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight, vPlaneRot) // Far Right
	
	PLAY_SOUND_FROM_ENTITY(-1, "chaff_released", vehPlane, "DLC_SM_Countermeasures_Sounds", TRUE)
	
	IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlane)
		SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(vehPlane, FALSE, TRUE)
	ELSE
		BROADCAST_SET_CHAFF_LOCK_ON(ALL_PLAYERS_ON_SCRIPT(), TRUE)
	ENDIF
	
	REINIT_NET_TIMER(g_sLockOnBlock)
	
	SET_BIT(iAirFlareBS, AIR_FLARE_BS_FIRED)
	
	IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehPlane)
	OR IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(vehPlane)
	OR g_bAllowCountermeasureAmmo
		IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlane)
			SET_VEHICLE_COUNTERMEASURE_AMMO(vehPlane, GET_VEHICLE_COUNTERMEASURE_AMMO(vehPlane) - 1)
		ELSE
			BROADCAST_DECREMENT_AIRCRAFT_AMMO(ALL_PLAYERS_ON_SCRIPT(), FALSE, TRUE)
		ENDIF
	ENDIF
	
	IF NOT MPGlobalsAmbience.bDisplayedChaffHelp
	AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		PRINT_HELP("CHAFF_FIRE")
		
		MPGlobalsAmbience.bDisplayedChaffHelp = TRUE
	ENDIF
	
	START_NET_TIMER(g_sCountermeasurePressDelay)
	
	PRINTLN("[AIR_COUNTERMEASURES] FIRE_CHAFF - Firing chaff")
ENDPROC

PROC MAINTAIN_USING_CHAFF(VEHICLE_INDEX vehPlane)
	IF HAS_NET_TIMER_STARTED(g_sCountermeasurePressDelay)
	AND HAS_NET_TIMER_EXPIRED(g_sCountermeasurePressDelay, 1500)
		IF IS_BIT_SET(iAirFlareBS, AIR_FLARE_BS_FIRED)
			CLEAR_BIT(iAirFlareBS, AIR_FLARE_BS_FIRED)
		ENDIF
		
		IF IS_BIT_SET(iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
			CLEAR_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
		ENDIF
		
		RESET_NET_TIMER(g_sCountermeasurePressDelay)
	ENDIF
	
	IF NOT IS_BIT_SET(iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
//		IF IS_MODEL_VALID(eFlareModel)
			IF NOT IS_BIT_SET(iAirFlareBS, AIR_COUNTERMEASURE_BS_CHAFF_PTFX_REQUESTED)
				PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_USING_CHAFF - Requesting chaff vfx")
				
				REQUEST_NAMED_PTFX_ASSET("scr_sm_counter")
				
				SET_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_CHAFF_PTFX_REQUESTED)
			ELSE
				IF HAS_NAMED_PTFX_ASSET_LOADED("scr_sm_counter")
					IF NOT IS_BIT_SET(iAirFlareBS, AIR_FLARE_BS_FIRED)
					AND NOT HAS_NET_TIMER_STARTED(g_sCountermeasurePressDelay)
						IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_FLY_COUNTER)
						AND NOT IS_PAUSE_MENU_ACTIVE_EX()
						AND NOT IS_INTERACTION_MENU_OPEN()
						AND NOT IS_PHONE_ONSCREEN()
						AND NOT IS_BROWSER_OPEN()
						AND bCounterMeasureButtonReleased
							IF NOT HAS_NET_TIMER_STARTED(g_sLockOnBlock)
								SET_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
								PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_USING_CHAFF - AIR_COUNTERMEASURE_BS_TRIGGERED TRUE")
							ELSE
								PLAY_SOUND_FRONTEND(-1, "chaff_cooldown", "DLC_SM_Countermeasures_Sounds")
								START_NET_TIMER(g_sCountermeasurePressDelay)
								
								IF NOT MPGlobalsAmbience.bDisplayedChaffDelayHelp
								AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									PRINT_HELP("CHAFF_DELAY")
									
									MPGlobalsAmbience.bDisplayedChaffDelayHelp = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
//		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iAirFlareBS, AIR_FLARE_BS_FIRED)
			FIRE_CHAFF(vehPlane)
		ENDIF
	ENDIF
ENDPROC

SCRIPT_TIMER flareTimer

PROC FIRE_AIR_FLARE(VEHICLE_INDEX vehPlane)
	
	IF GET_VEHICLE_MOD(vehPlane, MOD_BUMPER_F) != 1 
	AND !IS_VEHICLE_MODEL(vehPlane, OPPRESSOR2)
		PRINTLN("[AIR_COUNTERMEASURES] FIRE_AIR_FLARE - no flare available")
		EXIT
	ENDIF
	
	IF GET_VEHICLE_MOD(vehPlane, MOD_GRILL) != 1 
	AND IS_VEHICLE_MODEL(vehPlane, OPPRESSOR2)
		PRINTLN("[AIR_COUNTERMEASURES] FIRE_AIR_FLARE - no flare available")
		EXIT
	ENDIF
	
	BOOL bOnlyShootTwo = FALSE
	IF IS_VEHICLE_MODEL(vehPlane, OPPRESSOR2)
		bOnlyShootTwo = TRUE
	ENDIF
	
	IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehPlane)
	OR IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(vehPlane)
	OR g_bAllowCountermeasureAmmo
		IF GET_VEHICLE_COUNTERMEASURE_AMMO(vehPlane) <= 0
			PRINTLN("[AIR_COUNTERMEASURES] FIRE_AIR_FLARE - out of ammo")
			
			PLAY_SOUND_FRONTEND(-1, "flares_empty", "DLC_SM_Countermeasures_Sounds")
			
			SET_BIT(iAirFlareBS, AIR_FLARE_BS_FIRED)
			START_NET_TIMER(g_sCountermeasurePressDelay)
			RESET_NET_TIMER(flareTimer)
			CLEAR_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_ONE_FIRED)
			CLEAR_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_TWO_FIRED)
			CLEAR_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_THREE_FIRED)
			CLEAR_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_FOUR_FIRED)
			PRINTLN("[AIR_COUNTERMEASURES] FIRE_AIR_FLARE - Firing flares")
			
			EXIT
		ENDIF
	ENDIF
	
	VECTOR vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight
	DETERMINE_4_BOTTOM_VECTORS_FOR_VEHICLE_WEAPON(vehPlane, GET_ENTITY_MODEL(vehPlane), vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight)
	//VECTOR vUp
	VECTOR vRotation
	vRotation = GET_ENTITY_ROTATION(vehPlane, EULER_XYZ)
	vRotation.z = 0.0
	//GET_ENTITY_QUATERNION(vehPlane,  x,y,z,w)
	VECTOR vB1
	VECTOR vB2 
	vB1  = <<GET_FLARE_X_POS(vehPlane, 1),GET_FLARE_Y_POS(vehPlane),GET_FLARE_Z_POS(vehPlane)>>
	vB2 = <<-6,-4,-0.2>>
	ROTATE_VECTOR_FMMC(vB1, vRotation)
	ROTATE_VECTOR_FMMC(vB2, vRotation)
	vB1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehPlane), GET_ENTITY_HEADING(vehPlane), vB1)
	vB2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehPlane), GET_ENTITY_HEADING(vehPlane), vB2)
	
	IF NOT IS_BIT_SET(iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_THREE_FIRED)
	AND HAS_NET_TIMER_EXPIRED(flareTimer, 250)
	AND IS_BIT_SET(iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_ONE_FIRED)
		IF NOT bOnlyShootTwo
			SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vB1, vB2, 0, TRUE, WEAPONTYPE_DLC_FLAREGUN, PLAYER_PED_ID(), DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
		ENDIF
		SET_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_THREE_FIRED)
	ENDIF	
	
	vB1  = <<GET_FLARE_X_POS(vehPlane, 2),GET_FLARE_Y_POS(vehPlane),GET_FLARE_Z_POS(vehPlane)>>
	
	IF IS_VEHICLE_MODEL(vehPlane, OPPRESSOR2)
		vB2 = <<-1,-4,-0.2>>
	ELSE
		vB2 = <<-3,-4,-0.2>>
	ENDIF
	
	ROTATE_VECTOR_FMMC(vB1, vRotation)
	ROTATE_VECTOR_FMMC(vB2, vRotation)
	
	vB1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehPlane), GET_ENTITY_HEADING(vehPlane), vB1)
	vB2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehPlane), GET_ENTITY_HEADING(vehPlane), vB2)
	
	
	
	IF NOT IS_BIT_SET(iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_ONE_FIRED)
		SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vB1, vB2 , 0, TRUE, WEAPONTYPE_DLC_FLAREGUN, PLAYER_PED_ID(), DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
		START_NET_TIMER(flareTimer)
		SET_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_ONE_FIRED)
		PLAY_SOUND_FROM_ENTITY(-1, "flares_released", vehPlane, "DLC_SM_Countermeasures_Sounds", TRUE)
	ENDIF
	
	vB1  = <<GET_FLARE_X_POS(vehPlane, 3),GET_FLARE_Y_POS(vehPlane),GET_FLARE_Z_POS(vehPlane)>>
	
	IF IS_VEHICLE_MODEL(vehPlane, OPPRESSOR2)
		vB2 = <<1,-4,-0.2>>
	ELSE
		vB2 = <<3,-4,-0.2>>
	ENDIF
	
	ROTATE_VECTOR_FMMC(vB1, vRotation)
	ROTATE_VECTOR_FMMC(vB2, vRotation)
	
	vB1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehPlane), GET_ENTITY_HEADING(vehPlane), vB1)
	vB2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehPlane), GET_ENTITY_HEADING(vehPlane), vB2)
	
	IF NOT IS_BIT_SET(iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_TWO_FIRED)
		SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vB1, vB2, 0, TRUE, WEAPONTYPE_DLC_FLAREGUN, PLAYER_PED_ID(), DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
		SET_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_TWO_FIRED)
	ENDIF
	
	vB1  = <<GET_FLARE_X_POS(vehPlane, 4),GET_FLARE_Y_POS(vehPlane),GET_FLARE_Z_POS(vehPlane)>>
	vB2 = <<6,-4,-0.2>>
	
	ROTATE_VECTOR_FMMC(vB1, vRotation)
	ROTATE_VECTOR_FMMC(vB2, vRotation)
	
	vB1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehPlane), GET_ENTITY_HEADING(vehPlane), vB1)
	vB2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehPlane), GET_ENTITY_HEADING(vehPlane), vB2)
	
	IF NOT IS_BIT_SET(iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_FOUR_FIRED)
	AND IS_BIT_SET(iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_TWO_FIRED)
	AND HAS_NET_TIMER_EXPIRED(flareTimer, 250)
		IF NOT bOnlyShootTwo
			SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vB1, vB2, 0, TRUE, WEAPONTYPE_DLC_FLAREGUN, PLAYER_PED_ID(), DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
		ENDIF	
		SET_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_FOUR_FIRED)
	ENDIF
	
	IF IS_BIT_SET(iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_FOUR_FIRED)
		IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehPlane)
		OR IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(vehPlane)
		OR g_bAllowCountermeasureAmmo
			IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlane)
				SET_VEHICLE_COUNTERMEASURE_AMMO(vehPlane, GET_VEHICLE_COUNTERMEASURE_AMMO(vehPlane) - 1)
			ELSE
				BROADCAST_DECREMENT_AIRCRAFT_AMMO(ALL_PLAYERS_ON_SCRIPT(), FALSE, TRUE)
			ENDIF
		ENDIF
		
		SET_BIT(iAirFlareBS, AIR_FLARE_BS_FIRED)
		START_NET_TIMER(g_sCountermeasurePressDelay)
		RESET_NET_TIMER(flareTimer)
		CLEAR_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_ONE_FIRED)
		CLEAR_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_TWO_FIRED)
		CLEAR_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_THREE_FIRED)
		CLEAR_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_FOUR_FIRED)
		PRINTLN("[AIR_COUNTERMEASURES] FIRE_AIR_FLARE - Firing flares")
	ENDIF

ENDPROC

PROC MAINTAIN_USING_AIR_FLARES(VEHICLE_INDEX vehPlane)
	INT iFlareCooldown = 2500
	
	IF g_bOverrideFlareCooldown
		iFlareCooldown = g_iFlareCooldown
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(g_sCountermeasurePressDelay)
	AND HAS_NET_TIMER_EXPIRED(g_sCountermeasurePressDelay, iFlareCooldown)
		IF IS_BIT_SET(iAirFlareBS, AIR_FLARE_BS_FIRED)
			CLEAR_BIT(iAirFlareBS, AIR_FLARE_BS_FIRED)
		ENDIF
		IF IS_BIT_SET(iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
			CLEAR_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
		ENDIF	
		RESET_NET_TIMER(g_sCountermeasurePressDelay)
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(sFlareCooldownSoundTimer)
	AND HAS_NET_TIMER_EXPIRED(sFlareCooldownSoundTimer, 500)
		RESET_NET_TIMER(sFlareCooldownSoundTimer)
	ENDIF
	
	IF NOT IS_BIT_SET(iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
		IF IS_MODEL_VALID(eFlareModel)
			IF NOT IS_BIT_SET(iAirFlareBS, AIR_COUNTERMEASURE_BS_MODEL_REQUESTED)
				PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_USING_AIR_FLARES - Requesting flare model")
				
				REQUEST_MODEL(eFlareModel)
				
				SET_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_MODEL_REQUESTED)
			ELSE
				IF HAS_MODEL_LOADED(eFlareModel)
					IF NOT IS_BIT_SET(iAirFlareBS, AIR_FLARE_BS_FIRED)
						IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_FLY_COUNTER)
						AND NOT IS_PAUSE_MENU_ACTIVE_EX()
						AND NOT IS_INTERACTION_MENU_OPEN()
						AND NOT IS_PHONE_ONSCREEN()
						AND NOT IS_BROWSER_OPEN()
						AND bCounterMeasureButtonReleased
							RESET_NET_TIMER(flareTimer)
							SET_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
							PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_USING_AIR_FLARES - AIR_COUNTERMEASURE_BS_TRIGGERED TRUE")		
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iAirFlareBS, AIR_FLARE_BS_FIRED)
			FIRE_AIR_FLARE(vehPlane)
		ELSE
			IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_FLY_COUNTER)
			AND NOT IS_PAUSE_MENU_ACTIVE_EX()
			AND NOT IS_INTERACTION_MENU_OPEN()
			AND NOT IS_PHONE_ONSCREEN()
			AND NOT IS_BROWSER_OPEN()
			AND bCounterMeasureButtonReleased
				IF NOT HAS_NET_TIMER_STARTED(sFlareCooldownSoundTimer)
					START_NET_TIMER(sFlareCooldownSoundTimer)
					
					PLAY_SOUND_FRONTEND(-1, "flares_empty", "DLC_SM_Countermeasures_Sounds")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_SMOKE_OFFSET_VECTOR(VEHICLE_INDEX vehPlane)
	VECTOR vSmokeOffset = <<0.0, 0.0, 0.0>>
	
	SWITCH GET_ENTITY_MODEL(vehPlane)
		CASE CUBAN800	vSmokeOffset = <<0.0, -3.0, -0.3>>	BREAK
		CASE MOGUL		vSmokeOffset = <<0.0, -5.0, 0.7>>	BREAK
		CASE ROGUE		vSmokeOffset = <<0.0, -7.0, 0.6>>	BREAK
		CASE STARLING	vSmokeOffset = <<0.0, -3.0, 0.6>>	BREAK
		CASE SEABREEZE	vSmokeOffset = <<0.0, -3.0, 0.2>>	BREAK
		CASE TULA		vSmokeOffset = <<0.0, -5.0, 0.7>>	BREAK
		CASE BOMBUSHKA	vSmokeOffset = <<0.0, -21.0, 4.5>>	BREAK
		CASE HUNTER		vSmokeOffset = <<0.0, -6.0, 0.0>>	BREAK
		CASE NOKOTA		vSmokeOffset = <<0.0, -4.0, 0.0>>	BREAK
		CASE PYRO		vSmokeOffset = <<0.0, -3.0, 0.3>>	BREAK
		CASE MOLOTOK	vSmokeOffset = <<0.0, -5.0, 0.3>>	BREAK
		CASE HAVOK		vSmokeOffset = <<0.0, -4.0, 0.3>>	BREAK
		CASE ALPHAZ1	vSmokeOffset = <<0.0, -2.5, -0.2>>	BREAK
		CASE MICROLIGHT	vSmokeOffset = <<0.0, -2.0, 0.5>>	BREAK
		CASE HOWARD		vSmokeOffset = <<0.0, -3.5, 0.5>>	BREAK
		CASE AVENGER	vSmokeOffset = <<0.0, -10.0, 1.0>>	BREAK
		CASE AKULA		vSmokeOffset = <<0.0, -6.0, 0.0>>	BREAK
		CASE THRUSTER	vSmokeOffset = <<0.0, -0.5, 0.0>>	BREAK
		CASE OPPRESSOR2 vSmokeOffset = <<0.0, -1.2, -0.1>>	BREAK
		CASE VOLATOL	vSmokeOffset = <<0.0, -20.0, 1.0>>	BREAK
	ENDSWITCH
	
	RETURN vSmokeOffset
ENDFUNC

FUNC FLOAT GET_SMOKE_SCALE(VEHICLE_INDEX vehPlane)
	FLOAT vScale = 1.0
	
	SWITCH GET_ENTITY_MODEL(vehPlane)
		CASE ALPHAZ1	vScale = 0.82	BREAK
		CASE BOMBUSHKA  vScale = 0.7	BREAK
		CASE AVENGER  	vScale = 0.7	BREAK
		CASE THRUSTER	vScale = 0.7	BREAK
		CASE VOLATOL	vScale = 0.7	BREAK
		DEFAULT 		vScale = 1.0	BREAK
	ENDSWITCH
	
	RETURN vScale
ENDFUNC

PROC FIRE_SMOKE_FX(VEHICLE_INDEX vehPlane, PTFX_ID &smokeptfx)
	
	IF NOT DOES_PARTICLE_FX_LOOPED_EXIST( smokeptfx )	
		IF NOT VEHICLE_SUITABLE_FOR_AIR_FLARES(vehPlane)
			CLEANUP_PLANE_FX_SMOKE(smokeptfx)
			PRINTLN("[AIR_COUNTERMEASURES] FIRE_SMOKE_FX - VEHICLE_SUITABLE_FOR_AIR_FLARES - FALSE")
			EXIT
		ENDIF
		
		INT iR, iG, iB
		FLOAT fR, fG, fB
		GET_VEHICLE_TYRE_SMOKE_COLOR(vehPlane, iR,iG, iB)
		
		SWITCH GET_TYRE_SMOKE_COLOUR_ENUM_FROM_RGB(iR,iG, iB)
			CASE TYRE_SMOKE_WHITE
				fR = 255
				fG = 255
				fB = 255
			BREAK
			CASE TYRE_SMOKE_ORANGE
				fR = 255
				fG = 165
				fB = 0
			BREAK	
			CASE TYRE_SMOKE_YELLOW
				fR = 255
				fG = 255
				fB = 0
			BREAK	
			CASE TYRE_SMOKE_BLUE
				fR = 0
				fG = 0
				fB = 255
			BREAK
			CASE TYRE_SMOKE_RED
				fR = 255
				fG = 0
				fB = 0
			BREAK		
			CASE TYRE_SMOKE_BLACK
		 		fR = 0
				fG = 0
				fB = 0
			BREAK
			CASE TYRE_SMOKE_BUSINESS_PURPLE
				fR = 128
				fG = 0
				fB = 128	
			BREAK	
			CASE TYRE_SMOKE_BUSINESS_GREEN
				fR = 0
				fG = 255
				fB = 0
			BREAK
			CASE TYRE_SMOKE_HIPSTER_PINK
				fR = 255
				fG = 0
				fB = 255
			BREAK
			CASE TYRE_SMOKE_HIPSTER_BROWN
				fR = 0
				fG = 255
				fB = 255
			BREAK
		ENDSWITCH
		
		USE_PARTICLE_FX_ASSET(strPTFXAsset)
		
		smokeptfx = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY_BONE(strSmokeFX, vehPlane, GET_SMOKE_OFFSET_VECTOR(vehPlane), <<0.0,0.0,0.0>>, DEFAULT, GET_SMOKE_SCALE(vehPlane), DEFAULT, DEFAULT, DEFAULT, fR/255, fG/255, fB/255, TRUE)
		
		PRINTLN("[AIR_COUNTERMEASURES] FIRE_SMOKE_FX - Starting START_PARTICLE_FX_LOOPED_ON_ENTITY ID: ", NATIVE_TO_INT(smokeptfx))
		
		PRINTLN("[AIR_COUNTERMEASURES] FIRE_SMOKE_FX - iR: ",TO_FLOAT(iR) , " iG: ", TO_FLOAT(iG), " iB: ", TO_FLOAT(iB) )
		
		SET_BIT(iAirFlareBS, AIR_FLARE_BS_FIRED)
		
		SET_PLAYER_AIRCRAFT_SMOKE_ACTIVE(TRUE)
		
		START_NET_TIMER(g_sCountermeasurePressDelay)
		
	ENDIF
ENDPROC


PROC MAINTAIN_USING_SMOKE(VEHICLE_INDEX vehPlane)

	REQUEST_NAMED_PTFX_ASSET(strPTFXAsset)

	IF NOT IS_BIT_SET(iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
		
		IF HAS_NAMED_PTFX_ASSET_LOADED(strPTFXAsset)
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST( ptfxID )		
				IF NOT IS_BIT_SET(iAirFlareBS, AIR_FLARE_BS_FIRED)
					IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_FLY_COUNTER)
					AND NOT IS_PAUSE_MENU_ACTIVE_EX()
					AND NOT IS_INTERACTION_MENU_OPEN()
					AND NOT IS_PHONE_ONSCREEN()
					AND NOT IS_BROWSER_OPEN()
					AND bCounterMeasureButtonReleased
						SET_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
						PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_USING_SMOKE - AIR_COUNTERMEASURE_BS_TRIGGERED TRUE")
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
		IF NOT IS_BIT_SET(iAirFlareBS, AIR_FLARE_BS_FIRED)
			FIRE_SMOKE_FX(vehPlane, ptfxID)
		ELSE
			IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxID )		
			//AND HAS_NET_TIMER_EXPIRED(g_sCountermeasurePressDelay, 1500)
				IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_FLY_COUNTER)
				AND NOT IS_PAUSE_MENU_ACTIVE_EX()
				AND NOT IS_INTERACTION_MENU_OPEN()
				AND NOT IS_PHONE_ONSCREEN()
				AND NOT IS_BROWSER_OPEN()
				AND bCounterMeasureButtonReleased
					CLEANUP_PLANE_FX_SMOKE(ptfxID)
					PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_USING_SMOKE - CLEANUP_PLANE_FX_SMOKE")
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC STRING GET_COUNTERMEASURE_HELP_TEXT(VEHICLE_INDEX vVehicle)
	STRING sHelpText = ""
	
	IF CAN_VEHICLE_HAVE_AIR_COUNTERMEASURES(vVehicle)
		IF VEHICLE_SUITABLE_FOR_AIR_FLARES(vVehicle)
			IF GET_VEHICLE_MOD(vVehicle, MOD_BUMPER_F) =  1
				RETURN "FLARE_HELP"
			ENDIF
			IF IS_VEHICLE_MODEL(vVehicle, OPPRESSOR2)
				IF GET_VEHICLE_MOD(vVehicle, MOD_GRILL) =  1
					RETURN "FLARE_HELP"
				ENDIF
			ENDIF
		ENDIF
		
		IF VEHICLE_SUITABLE_FOR_CHAFF(vVehicle)
			IF GET_VEHICLE_MOD(vVehicle, MOD_BUMPER_F) =  0
				RETURN "CHAFF_HELP"
			ENDIF	
			IF IS_VEHICLE_MODEL(vVehicle, OPPRESSOR2)
				IF GET_VEHICLE_MOD(vVehicle, MOD_GRILL) =  0
					RETURN "CHAFF_HELP"
				ENDIF
			ENDIF
		ENDIF
		
		IF VEHICLE_SUITABLE_FOR_SMOKE(vVehicle)
			IF IS_TOGGLE_MOD_ON(vVehicle, MOD_TOGGLE_TYRE_SMOKE)
				RETURN "SMOKE_HELP"
			ENDIF
		ENDIF
	ENDIF
	
	RETURN sHelpText
ENDFUNC

PROC MAINTAIN_AIR_COUNTERMEASURES()
	BOOL bUnloadBombModel = FALSE
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		CLEANUP_PLANE_FX_SMOKE(ptfxID)
	ENDIF
	
	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
	AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
		IF NOT SHOULD_VEHICLE_FIRE_SMOKE()
			INT iTimer 
			IF SHOULD_VEHICLE_FIRE_FLARE()
				iTimer = 2500
	
				IF g_bOverrideFlareCooldown
					iTimer = g_iFlareCooldown
				ENDIF
			ELSE
				iTimer = 1500
			ENDIF
			IF HAS_NET_TIMER_STARTED(g_sCountermeasurePressDelay)
			AND HAS_NET_TIMER_EXPIRED(g_sCountermeasurePressDelay, iTimer)
				RESET_NET_TIMER(g_sCountermeasurePressDelay)
				IF IS_BIT_SET(iAirFlareBS, AIR_FLARE_BS_FIRED)
					CLEAR_BIT(iAirFlareBS, AIR_FLARE_BS_FIRED)
				ENDIF
				IF IS_BIT_SET(iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
					CLEAR_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
			vehUsedForCountermeasure  = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
			MAINTAIN_VEHICLE_COUNTERMEASURE_HELP_TEXT(vehTemp)
			
			IF SHOULD_VEHICLE_FIRE_CHAFF(vehTemp)
				IF HAS_NET_TIMER_STARTED(g_sLockOnBlock)
				AND HAS_NET_TIMER_EXPIRED(g_sLockOnBlock, g_sMPTunables.iSMUGGLER_CHAFF_DURATION)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(vehTemp)
						SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(vehTemp, TRUE, TRUE)
					ELSE
						IF NOT IS_BIT_SET(iAirFlareBS, AIR_COUNTERMEASURE_BS_CHAFF_LOCKON_STOPPED)
							SET_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_CHAFF_LOCKON_STOPPED)
							
							BROADCAST_SET_CHAFF_LOCK_ON(ALL_PLAYERS_ON_SCRIPT(), FALSE)
						ENDIF
					ENDIF
					
					INT iChaffCooldown = g_sMPTunables.iSMUGGLER_CHAFF_DURATION + g_sMPTunables.iSMUGGLER_CHAFF_COOLDOWN
					
					IF g_bOverrideChaffCooldown
						iChaffCooldown = g_sMPTunables.iSMUGGLER_CHAFF_DURATION + g_iChaffCooldown
					ENDIF
					
					IF HAS_NET_TIMER_STARTED(g_sLockOnBlock)
					AND HAS_NET_TIMER_EXPIRED(g_sLockOnBlock, iChaffCooldown)
						RESET_NET_TIMER(g_sLockOnBlock)
						
						CLEAR_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_CHAFF_LOCKON_STOPPED)
					ENDIF
					
					PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_AIR_COUNTERMEASURES - Resetting vehicle lock-on")
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehTemp)
			AND CAN_LOCAL_PLAYER_USE_COUNTERMEASURE(vehTemp)
			AND CAN_VEHICLE_HAVE_AIR_COUNTERMEASURES(vehTemp)
				//PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_AIR_COUNTERMEASURES - 1")
				IF SHOULD_VEHICLE_FIRE_FLARE()
				OR SHOULD_VEHICLE_FIRE_CHAFF(vehTemp)
					//PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_AIR_COUNTERMEASURES - 2")
					IF CAN_VEHICLE_USE_AIR_COUNTERMEASURES(vehTemp)
						//PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_AIR_COUNTERMEASURES - 3")
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
						
						IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehTemp)
						OR IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(vehTemp)
							IF NOT IS_BIT_SET(iAirFlareBS, AIR_FLARE_BS_HELP_TEXT_SHOWN)
							AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								PRINT_HELP(GET_COUNTERMEASURE_HELP_TEXT(vehTemp))
								
								SET_BIT(iAirFlareBS, AIR_FLARE_BS_HELP_TEXT_SHOWN)
							ENDIF
						ENDIF
						
						IF SHOULD_VEHICLE_FIRE_FLARE()
							MAINTAIN_USING_AIR_FLARES(vehTemp)
						ELIF SHOULD_VEHICLE_FIRE_CHAFF(vehTemp)
							//PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_AIR_COUNTERMEASURES - 4")
							MAINTAIN_USING_CHAFF(vehTemp)
						ENDIF
					ELSE
						IF HAS_NET_TIMER_STARTED(g_sCountermeasurePressDelay)
							RESET_NET_TIMER(g_sCountermeasurePressDelay)
						ENDIF
						
						bUnloadBombModel = TRUE
					ENDIF
					
				ELIF SHOULD_VEHICLE_FIRE_SMOKE()
					IF CAN_VEHICLE_USE_AIR_SMOKES(vehTemp)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
						
						IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehTemp)
						OR IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(vehTemp)
							IF NOT IS_BIT_SET(iAirFlareBS, AIR_FLARE_BS_HELP_TEXT_SHOWN)
							AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								PRINT_HELP(GET_COUNTERMEASURE_HELP_TEXT(vehTemp))
								
								SET_BIT(iAirFlareBS, AIR_FLARE_BS_HELP_TEXT_SHOWN)
							ENDIF
						ENDIF
						
						MAINTAIN_USING_SMOKE(vehTemp)
					ELSE
						IF HAS_NET_TIMER_STARTED(g_sCountermeasurePressDelay)
							RESET_NET_TIMER(g_sCountermeasurePressDelay)
						ENDIF
						
						CLEANUP_PLANE_FX_SMOKE(ptfxID)
						
						bUnloadBombModel = TRUE
					ENDIF
				ENDIF
			ELSE
				CLEANUP_PLANE_FX_SMOKE(ptfxID)
				bUnloadBombModel = TRUE
			ENDIF
		ELSE
		  	SET_PLAYER_AIRCRAFT_SMOKE_ACTIVE(FALSE)
			iAirFlareBS = 0
			CLEANUP_PLANE_FX_SMOKE(ptfxID)
			bUnloadBombModel = TRUE
		ENDIF
	ELSE
		iAirFlareBS = 0
		CLEANUP_PLANE_FX_SMOKE(ptfxID)
		bUnloadBombModel = TRUE
	ENDIF
	
	IF !IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
	OR NOT IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxID)
			CLEANUP_PLANE_FX_SMOKE(ptfxID)
			PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_AIR_COUNTERMEASURES - player left the session cleanup")
		ENDIF		
	ENDIF
	
	IF !IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	OR (DOES_ENTITY_EXIST(vehUsedForCountermeasure) AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehUsedForCountermeasure))
	OR !DOES_ENTITY_EXIST(vehUsedForCountermeasure)
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxID)
			CLEANUP_PLANE_FX_SMOKE(ptfxID)
			PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_AIR_COUNTERMEASURES - player not in vehicle")
		ENDIF	
	ENDIF
	
	IF bUnloadBombModel
	AND IS_BIT_SET(iAirFlareBS, AIR_COUNTERMEASURE_BS_MODEL_REQUESTED)
		PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_AIR_COUNTERMEASURES - Unloading bomb model")
		CLEANUP_PLANE_FX_SMOKE(ptfxID)
		IF IS_MODEL_VALID(eFlareModel)
			SET_MODEL_AS_NO_LONGER_NEEDED(eFlareModel)
		ENDIF
		CLEAR_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_MODEL_REQUESTED)
	ENDIF
	
	IF bUnloadBombModel
	AND IS_BIT_SET(iAirFlareBS, AIR_COUNTERMEASURE_BS_CHAFF_PTFX_REQUESTED)
		PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_AIR_COUNTERMEASURES - Unloading chaff vfx")
		
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_sm_counter")
			REMOVE_NAMED_PTFX_ASSET("scr_sm_counter")
		ENDIF
		
		CLEAR_BIT(iAirFlareBS, AIR_COUNTERMEASURE_BS_CHAFF_PTFX_REQUESTED)
	ENDIF
	
	IF IS_CONTROL_RELEASED(PLAYER_CONTROL, INPUT_VEH_FLY_COUNTER)
	AND NOT IS_PAUSE_MENU_ACTIVE_EX()
	AND NOT IS_INTERACTION_MENU_OPEN()
	AND NOT IS_PHONE_ONSCREEN()
	AND NOT IS_BROWSER_OPEN()
		bCounterMeasureButtonReleased = TRUE
	ENDIF
ENDPROC

FUNC FLOAT GET_BOMB_OFFSET(VEHICLE_INDEX vehPlane)
	FLOAT fBombOffset = 0.0
	
	SWITCH GET_ENTITY_MODEL(vehPlane)
		CASE CUBAN800
			fBombOffset = 0.5
		BREAK
		CASE MOGUL
			fBombOffset = 0.45
		BREAK
		CASE ROGUE
			fBombOffset = 0.46
		BREAK
		CASE STARLING
			fBombOffset = 0.55
		BREAK
		CASE SEABREEZE
			fBombOffset = 0.5
		BREAK
		CASE TULA
			fBombOffset = 0.6
		BREAK
		CASE BOMBUSHKA
			fBombOffset = 0.43
		BREAK
		CASE HUNTER
			fBombOffset = 0.5
		BREAK
		CASE AVENGER
			fBombOffset = 0.36
		BREAK
		CASE AKULA
			fBombOffset = 0.4
		BREAK
		CASE VOLATOL
			fBombOffset = 0.54
		BREAK
		CASE STRIKEFORCE
			fBombOffset = 0.7
		BREAK
	ENDSWITCH
	
	RETURN fBombOffset
ENDFUNC

PROC FIRE_AIR_BOMB(VEHICLE_INDEX vehPlane, BOOL bLastBomb)
	
	IF GET_VEHICLE_MOD(vehPlane, MOD_WING_R) < 0
		PRINTLN("[AIR_BOMBS] FIRE_AIR_BOMB - no bombs available")
		EXIT
	ENDIF
	
	IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehPlane)
	OR IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(vehPlane)
		IF GET_VEHICLE_BOMB_AMMO(vehPlane) <= 0
			PRINTLN("[AIR_BOMBS] FIRE_AIR_BOMB - out of ammo")
			
			IF bLastBomb
				PLAY_SOUND_FRONTEND(-1, "bombs_empty", "DLC_SM_Bomb_Bay_Bombs_Sounds")
			ENDIF
			
			START_NET_TIMER(sBombPressDelay)
			
			EXIT
		ENDIF
	ENDIF
	
	VECTOR vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight
	DETERMINE_4_BOTTOM_VECTORS_FOR_VEHICLE_WEAPON(vehPlane, GET_ENTITY_MODEL(vehPlane), vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight)
	
	VECTOR vB1 = GET_INTERP_POINT_VECTOR_FOR_VEHICLE_WEAPON(vFrontBottomLeft, vFrontBottomRight, 0.0, 1.0, 0.5)
	VECTOR vB2 = GET_INTERP_POINT_VECTOR_FOR_VEHICLE_WEAPON(vBackBottomLeft, vBackBottomRight, 0.0, 1.0, 0.5)
	
	vB1.z += 0.4
	vB2.z += 0.4
	
	VECTOR vB3 = GET_INTERP_POINT_VECTOR_FOR_VEHICLE_WEAPON(vB1, vB2, 0.0, 1.0, GET_BOMB_OFFSET(vehPlane))
	
	vB1.z -= 0.2
	vB2.z -= 0.2
	
	VECTOR vB4 = GET_INTERP_POINT_VECTOR_FOR_VEHICLE_WEAPON(vB1, vB2, 0.0, 1.0, GET_BOMB_OFFSET(vehPlane) - 0.0001)
	
	WEAPON_TYPE bombType
	SWITCH GET_VEHICLE_MOD(vehPlane, MOD_WING_R)
		CASE 0
			IF IS_VEHICLE_MODEL(vehPlane, VOLATOL)
				IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_SPYPLANE)
					bombType = WEAPONTYPE_DLC_VEHICLE_BOMB_STANDARD_WIDE
				ELSE
					bombType = WEAPONTYPE_DLC_VEHICLE_BOMB
				ENDIF
			ELSE
				bombType = WEAPONTYPE_DLC_VEHICLE_BOMB
			ENDIF
		BREAK
		CASE 1
			bombType = WEAPONTYPE_DLC_VEHICLE_BOMB_INCENDIARY
		BREAK
		CASE 2
			bombType = WEAPONTYPE_DLC_VEHICLE_BOMB_GAS
		BREAK
		CASE 3
			bombType = WEAPONTYPE_DLC_VEHICLE_BOMB_CLUSTER
		BREAK
	ENDSWITCH
	
	SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vB3, vB4, 0, DEFAULT, bombType, PLAYER_PED_ID(), DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
	
	IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehPlane)
	OR IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(vehPlane)
		IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlane)
			SET_VEHICLE_BOMB_AMMO(vehPlane, GET_VEHICLE_BOMB_AMMO(vehPlane) - 1)
		ELSE
			BROADCAST_DECREMENT_AIRCRAFT_AMMO(ALL_PLAYERS_ON_SCRIPT(), TRUE, FALSE)
		ENDIF
	ENDIF
	
	IF bLastBomb
	AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
		SET_BIT(iAirBombsBS, AIR_BOMBS_BS_BOMB_FIRED)
	ENDIF
	
	PLAY_SOUND_FRONTEND(-1, "bomb_deployed", "DLC_SM_Bomb_Bay_Bombs_Sounds")
	PRINTLN("[AIR_BOMBS] MAINTAIN_USING_AIR_BOMBS - Firing a bomb")
	
	START_NET_TIMER(sBombPressDelay)
ENDPROC

FUNC FLOAT GET_BOMB_Y_POS(VEHICLE_INDEX vehPlane)
	FLOAT fYPos = 0.0
	
	SWITCH GET_ENTITY_MODEL(vehPlane)
		CASE CUBAN800
			fYPos = 1.3
		BREAK
		CASE MOGUL
			fYPos = 1.25
		BREAK
		CASE ROGUE
			fYPos = -0.5
		BREAK
		CASE STARLING
			fYPos = 3.0
		BREAK
		CASE SEABREEZE
			fYPos = 1.0
		BREAK
		CASE TULA
			fYPos = 0.5
		BREAK
		CASE BOMBUSHKA
			fYPos = -2.0
		BREAK
		CASE HUNTER
			fYPos = 4.0
		BREAK
		CASE AVENGER
			fYPos = 0.9
		BREAK
		CASE AKULA
			fYPos = 6.5
		BREAK
		CASE VOLATOL
			fYPos = -9.0
		BREAK
		CASE STRIKEFORCE
			fYPos = -1.5
		BREAK
	ENDSWITCH
	
	RETURN fYPos
ENDFUNC

FUNC FLOAT GET_BOMB_CAM_Z(VEHICLE_INDEX vehPlane)
	FLOAT fZPos = 0.0
	
	SWITCH GET_ENTITY_MODEL(vehPlane)
		CASE CUBAN800
		CASE MOGUL
		CASE ROGUE
		CASE STARLING
		CASE SEABREEZE
		CASE TULA
		CASE BOMBUSHKA
		CASE HUNTER
		CASE AKULA
		CASE STRIKEFORCE
			fZPos = -1.5
		BREAK
		
		CASE AVENGER
			fZPos = -3.0
		BREAK
		
		CASE VOLATOL
			fZPos = -2.0
		BREAK
	ENDSWITCH
	
	RETURN fZPos
ENDFUNC

PROC CLEANUP_BOMB_CAMERA()
	IF IS_BIT_SET(iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			CLEAR_PED_TASKS(PLAYER_PED_ID())
		ENDIF
		
		IF DOES_CAM_EXIST(cBombCamera[0])
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DESTROY_CAM(cBombCamera[0])
		ENDIF
		
		IF DOES_CAM_EXIST(cBombCamera[1])
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DESTROY_CAM(cBombCamera[1])
			CLEAR_BIT(iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED_2)
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("DLC_SM_Bomb_Bay_View_Scene")
			STOP_AUDIO_SCENE("DLC_SM_Bomb_Bay_View_Scene")
		ENDIF
		
		bFollowAircraftHeading = TRUE
		
		CLEAR_BIT(iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED)
	ENDIF
ENDPROC

FUNC INT GET_CARPET_BOMB_COUNT(VEHICLE_INDEX vehPlane)
	IF IS_VEHICLE_MODEL(vehPlane, VOLATOL)
	AND IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_SPYPLANE)
		RETURN 10
	ENDIF
	
	RETURN 5
ENDFUNC

VECTOR vGoToPos

PROC MAINTAIN_USING_AIR_BOMBS(VEHICLE_INDEX vehPlane)
	VECTOR vBombPos
	VECTOR vPlaneCoords = GET_ENTITY_COORDS(vehPlane)
	FLOAT fHeading = GET_ENTITY_HEADING(vehPlane)
	
	IF IS_BIT_SET(iAirBombsBS, AIR_BOMBS_BS_BOMB_FIRED)
	AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
		WEAPON_TYPE bombType
		SWITCH GET_VEHICLE_MOD(vehPlane, MOD_WING_R)
			CASE 0
				IF IS_VEHICLE_MODEL(vehPlane, VOLATOL)
					IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_SPYPLANE)
						bombType = WEAPONTYPE_DLC_VEHICLE_BOMB_STANDARD_WIDE
					ELSE
						bombType = WEAPONTYPE_DLC_VEHICLE_BOMB
					ENDIF
				ELSE
					bombType = WEAPONTYPE_DLC_VEHICLE_BOMB
				ENDIF
			BREAK
			CASE 1
				bombType = WEAPONTYPE_DLC_VEHICLE_BOMB_INCENDIARY
			BREAK
			CASE 2
				bombType = WEAPONTYPE_DLC_VEHICLE_BOMB_GAS
			BREAK
			CASE 3
				bombType = WEAPONTYPE_DLC_VEHICLE_BOMB_CLUSTER
			BREAK
		ENDSWITCH
		
		IF GET_PROJECTILE_OF_PROJECTILE_TYPE_WITHIN_DISTANCE(PLAYER_PED_ID(), bombType, 25.0, vBombPos, entLastBomb)
			IF DOES_ENTITY_EXIST(entLastBomb)
			AND DOES_CAM_EXIST(cBombCamera[0])
				IF NOT IS_BIT_SET(iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED_2)
					IF DOES_CAM_EXIST(cBombCamera[1])
						DESTROY_CAM(cBombCamera[1])
					ENDIF
					
					cBombCamera[1] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
					SET_CAM_FOV(cBombCamera[1], 65.0)
					ATTACH_CAM_TO_ENTITY(cBombCamera[1], vehPlane, <<0.0, GET_BOMB_Y_POS(vehPlane), GET_BOMB_CAM_Z(vehPlane)>>)
					SET_CAM_ROT(cBombCamera[1], <<-105.0, 0.0, NORMALIZE_ANGLE(fHeading)>>)
					SET_CAM_ACTIVE_WITH_INTERP(cBombCamera[1], cBombCamera[0], 3000)
					SET_BIT(iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED_2)
					bFollowAircraftHeading = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		CLEAR_BIT(iAirBombsBS, AIR_BOMBS_BS_BOMB_FIRED)
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehPlane)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(vehPlane)
		IF GET_ARE_BOMB_BAY_DOORS_OPEN(vehPlane)
			SET_BIT(iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
		IF NOT IS_BIT_SET(iAirBombsBS, AIR_BOMBS_BS_HELP_TEXT_SHOWN_2)
		AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			PRINT_HELP("BOMB_CAM_HELP")
			
			SET_BIT(iAirBombsBS, AIR_BOMBS_BS_HELP_TEXT_SHOWN_2)
		ENDIF
		
		IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_FLY_BOMB_BAY)
		AND NOT IS_PAUSE_MENU_ACTIVE_EX()
		AND NOT IS_INTERACTION_MENU_OPEN()
		AND NOT IS_PHONE_ONSCREEN()
		AND NOT IS_BROWSER_OPEN()
		AND bBombDoorButtonReleased
			IF NOT HAS_NET_TIMER_STARTED(sBombDoorPressDelay)
				START_NET_TIMER(sBombDoorPressDelay)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(sBombDoorPressDelay, 500)
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BOMB_CAM_HELP")
						CLEAR_HELP()
					ENDIF
					
					IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlane)
						OPEN_BOMB_BAY_DOORS(vehPlane)
					ELSE
						BROADCAST_OPEN_BOMB_BAY_DOORS(ALL_PLAYERS_ON_SCRIPT(), TRUE)
					ENDIF
					
					REINIT_NET_TIMER(sBombPressDelay)
					RESET_NET_TIMER(sCarpetBombDelay)
					RESET_NET_TIMER(sBombDoorPressDelay)
					
					SET_BIT(iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
					
					bBombDoorButtonReleased = FALSE
					bCounterMeasureButtonReleased = FALSE
				ENDIF
			ENDIF
		ELSE
			RESET_NET_TIMER(sBombDoorPressDelay)
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iAirBombsBS, AIR_BOMBS_BS_HELP_TEXT_SHOWN_1)
		AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			PRINT_HELP("BOMB_HELP")
			
			SET_BIT(iAirBombsBS, AIR_BOMBS_BS_HELP_TEXT_SHOWN_1)
		ENDIF
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
		SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
		
		IF IS_CINEMATIC_CAM_RENDERING()
			SET_CINEMATIC_MODE_ACTIVE(FALSE)
		ENDIF
		
		IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_FLY_BOMB_BAY)
		AND NOT IS_PAUSE_MENU_ACTIVE_EX()
		AND NOT IS_INTERACTION_MENU_OPEN()
		AND NOT IS_PHONE_ONSCREEN()
		AND NOT IS_BROWSER_OPEN()
		AND bBombDoorButtonReleased
			IF NOT HAS_NET_TIMER_STARTED(sBombDoorPressDelay)
				START_NET_TIMER(sBombDoorPressDelay)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(sBombDoorPressDelay, 500)
					IF IS_BIT_SET(iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlane)
							CLOSE_BOMB_BAY_DOORS(vehPlane)
						ELSE
							BROADCAST_OPEN_BOMB_BAY_DOORS(ALL_PLAYERS_ON_SCRIPT(), FALSE)
						ENDIF
						
						IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlane)
							IF NOT GB_IS_PLAYER_ON_SMUGGLER_MISSION(PLAYER_ID())
							AND IS_ENTITY_A_MISSION_ENTITY(vehPlane)
							AND IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehPlane))
								SET_PLANE_TURBULENCE_MULTIPLIER(vehPlane, 1.0)
							ENDIF
						ENDIF
						
						SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
						
						CLEAR_BIT(iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
					ENDIF
					
					CLEANUP_BOMB_CAMERA()
					
					REINIT_NET_TIMER(sBombPressDelay)
					RESET_NET_TIMER(sCarpetBombDelay)
					RESET_NET_TIMER(sBombDoorPressDelay)
					
					bBombDoorButtonReleased = FALSE
					bCounterMeasureButtonReleased = FALSE
				ENDIF
			ENDIF
		ELSE
			RESET_NET_TIMER(sBombDoorPressDelay)
		ENDIF
		
		IF IS_BIT_SET(iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED)
			IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
			AND NOT IS_VEHICLE_MODEL(vehPlane, HUNTER)
			AND NOT IS_VEHICLE_MODEL(vehPlane, AKULA)
				IF GET_DISTANCE_BETWEEN_COORDS(vPlaneCoords, vGoToPos, FALSE) < 25.0
					VECTOR vPlaneForward = GET_ENTITY_FORWARD_VECTOR(vehPlane)
					
					vPlaneForward = NORMALISE_VECTOR(vPlaneForward)
					
					vPlaneForward.X *= 1000.0
					vPlaneForward.Y *= 1000.0
					
					vGoToPos = vPlaneCoords + vPlaneForward
					
					IF (IS_VEHICLE_MODEL(vehPlane, TULA)
					AND GET_VEHICLE_FLIGHT_NOZZLE_POSITION(vehPlane) = 0)
					OR (IS_VEHICLE_MODEL(vehPlane, AVENGER)
					AND GET_VEHICLE_FLIGHT_NOZZLE_POSITION(vehPlane) = 0)
						TASK_PLANE_MISSION(PLAYER_PED_ID(), vehPlane, NULL, NULL, <<vGoToPos.X, vGoToPos.Y, vPlaneCoords.Z>>, MISSION_GOTO, 50.0, 0.1, -1, 30, 20, FALSE)
					ELIF NOT IS_VEHICLE_MODEL(vehPlane, TULA)
					AND NOT IS_VEHICLE_MODEL(vehPlane, AVENGER)
						TASK_PLANE_MISSION(PLAYER_PED_ID(), vehPlane, NULL, NULL, <<vGoToPos.X, vGoToPos.Y, vPlaneCoords.Z>>, MISSION_GOTO, 50.0, 0.1, -1, 30, 20)
					ENDIF
				ENDIF
			ENDIF
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
			
			DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)
			
			IF NOT DOES_ENTITY_EXIST(entLastBomb)
				IF DOES_CAM_EXIST(cBombCamera[1])
					IF IS_BIT_SET(iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED_2)
						IF DOES_CAM_EXIST(cBombCamera[0])
							DESTROY_CAM(cBombCamera[0])
						ENDIF
						
						cBombCamera[0] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
						SET_CAM_FOV(cBombCamera[0], 65.0)
						ATTACH_CAM_TO_ENTITY(cBombCamera[0], vehPlane, <<0.0, GET_BOMB_Y_POS(vehPlane), GET_BOMB_CAM_Z(vehPlane)>>)
						SET_CAM_ROT(cBombCamera[0], <<-90.0, 0.0, NORMALIZE_ANGLE(fHeading)>>)
						SET_CAM_ACTIVE_WITH_INTERP(cBombCamera[0], cBombCamera[1], 2000)
						CLEAR_BIT(iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED_2)
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_CAM_EXIST(cBombCamera[1])
			AND NOT DOES_ENTITY_EXIST(entLastBomb)
			AND NOT IS_CAM_INTERPOLATING(cBombCamera[1])
			AND IS_BIT_SET(iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED_2)
				DESTROY_CAM(cBombCamera[1])
				CLEAR_BIT(iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED_2)
			ENDIF
			
			IF bFollowAircraftHeading
				SET_CAM_ROT(cBombCamera[0], <<-90.0, 0.0, NORMALIZE_ANGLE(fHeading)>>)
			ENDIF
			
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
			AND NOT IS_PAUSE_MENU_ACTIVE_EX()
			AND NOT IS_INTERACTION_MENU_OPEN()
			AND NOT IS_PHONE_ONSCREEN()
			AND NOT IS_BROWSER_OPEN()
				CLEANUP_BOMB_CAMERA()
			ENDIF
		ELSE
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
			AND NOT IS_PAUSE_MENU_ACTIVE_EX()
			AND NOT IS_INTERACTION_MENU_OPEN()
			AND NOT IS_PHONE_ONSCREEN()
			AND NOT IS_BROWSER_OPEN()
				IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
				AND NOT IS_VEHICLE_MODEL(vehPlane, HUNTER)
				AND NOT IS_VEHICLE_MODEL(vehPlane, AKULA)
					VECTOR vPlaneForward = GET_ENTITY_FORWARD_VECTOR(vehPlane)
					
					vPlaneForward = NORMALISE_VECTOR(vPlaneForward)
					
					vPlaneForward.X *= 1000.0
					vPlaneForward.Y *= 1000.0
					
					vGoToPos = vPlaneCoords + vPlaneForward
					
					IF (IS_VEHICLE_MODEL(vehPlane, TULA)
					AND GET_VEHICLE_FLIGHT_NOZZLE_POSITION(vehPlane) = 0)
					OR (IS_VEHICLE_MODEL(vehPlane, AVENGER)
					AND GET_VEHICLE_FLIGHT_NOZZLE_POSITION(vehPlane) = 0)
						TASK_PLANE_MISSION(PLAYER_PED_ID(), vehPlane, NULL, NULL, <<vGoToPos.X, vGoToPos.Y, vPlaneCoords.Z>>, MISSION_GOTO, 50.0, 0.1, -1, 30, 20, FALSE)
					ELIF NOT IS_VEHICLE_MODEL(vehPlane, TULA)
					AND NOT IS_VEHICLE_MODEL(vehPlane, AVENGER)
						TASK_PLANE_MISSION(PLAYER_PED_ID(), vehPlane, NULL, NULL, <<vGoToPos.X, vGoToPos.Y, vPlaneCoords.Z>>, MISSION_GOTO, 50.0, 0.1, -1, 30, 20)
					ENDIF
				ENDIF
				
				START_AUDIO_SCENE("DLC_SM_Bomb_Bay_View_Scene")
				
				cBombCamera[0] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0.0, 0.0, 0.0>>, <<-90.0, 0.0, NORMALIZE_ANGLE(fHeading)>>, 65, TRUE)
				ATTACH_CAM_TO_ENTITY(cBombCamera[0], vehPlane, <<0.0, GET_BOMB_Y_POS(vehPlane), GET_BOMB_CAM_Z(vehPlane)>>)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SET_BIT(iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED)
			ENDIF
		ENDIF
		
		SWITCH GET_VEHICLE_MOD(vehPlane, MOD_WING_R)
			CASE 0
				eBombModel = INT_TO_ENUM(MODEL_NAMES, HASH("w_smug_bomb_01"))
			BREAK
			CASE 1 
				eBombModel = INT_TO_ENUM(MODEL_NAMES, HASH("w_smug_bomb_04"))
			BREAK
			CASE 2
				eBombModel = INT_TO_ENUM(MODEL_NAMES, HASH("w_smug_bomb_03"))
			BREAK
			CASE 3
				eBombModel = INT_TO_ENUM(MODEL_NAMES, HASH("w_smug_bomb_02"))
			BREAK
		ENDSWITCH
		
		IF IS_MODEL_VALID(eBombModel)
			IF NOT IS_BIT_SET(iAirBombsBS, AIR_BOMBS_BS_MODEL_REQUESTED)
				PRINTLN("[AIR_BOMBS] MAINTAIN_USING_AIR_BOMBS - Requesting bomb model")
				
				REQUEST_MODEL(eBombModel)
				
				SET_BIT(iAirBombsBS, AIR_BOMBS_BS_MODEL_REQUESTED)
			ELSE
				IF HAS_MODEL_LOADED(eBombModel)
					IF IS_BIT_SET(iAirBombsBS, AIR_BOMBS_BS_CARPET_BOMB_TRIGGERED)
						IF iBombsFired < GET_CARPET_BOMB_COUNT(vehPlane)
							IF NOT HAS_NET_TIMER_STARTED(sCarpetBombDelay)
								IF iBombsFired = (GET_CARPET_BOMB_COUNT(vehPlane) - 1)
									FIRE_AIR_BOMB(vehPlane, TRUE)
								ELSE
									FIRE_AIR_BOMB(vehPlane, FALSE)
								ENDIF
								
								iBombsFired++
								
								START_NET_TIMER(sCarpetBombDelay)
							ELSE
								IF HAS_NET_TIMER_EXPIRED(sCarpetBombDelay, 200)
									RESET_NET_TIMER(sCarpetBombDelay)
								ENDIF
							ENDIF
						ELSE
							CLEAR_BIT(iAirBombsBS, AIR_BOMBS_BS_CARPET_BOMB_TRIGGERED)
							iBombsFired = 0
							
							RESET_NET_TIMER(sCarpetBombDelay)
						ENDIF
					ELSE
						IF IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
						AND NOT IS_PAUSE_MENU_ACTIVE_EX()
						AND NOT IS_INTERACTION_MENU_OPEN()
						AND NOT IS_PHONE_ONSCREEN()
						AND NOT IS_BROWSER_OPEN()
						AND NOT HAS_NET_TIMER_STARTED(sBombPressDelay)
							FIRE_AIR_BOMB(vehPlane, TRUE)
							
							RESET_NET_TIMER(sCarpetBombDelay)
						ENDIF
						
						IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
						AND NOT IS_PAUSE_MENU_ACTIVE_EX()
						AND NOT IS_INTERACTION_MENU_OPEN()
						AND NOT IS_PHONE_ONSCREEN()
						AND NOT IS_BROWSER_OPEN()
						AND NOT HAS_NET_TIMER_STARTED(sBombPressDelay)
							IF NOT HAS_NET_TIMER_STARTED(sCarpetBombDelay)
								START_NET_TIMER(sCarpetBombDelay)
							ELSE
								IF HAS_NET_TIMER_EXPIRED(sCarpetBombDelay, 500)
									SET_BIT(iAirBombsBS, AIR_BOMBS_BS_CARPET_BOMB_TRIGGERED)
								ENDIF
							ENDIF
						ELSE
							RESET_NET_TIMER(sCarpetBombDelay)
						ENDIF
					ENDIF
				ELSE
//					PRINTLN("[AIR_BOMBS] MAINTAIN_USING_AIR_BOMBS - eBombModel = ", ENUM_TO_INT(eBombModel, ", ")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_CONTROL_RELEASED(PLAYER_CONTROL, INPUT_VEH_FLY_BOMB_BAY)
	AND NOT IS_PAUSE_MENU_ACTIVE_EX()
	AND NOT IS_INTERACTION_MENU_OPEN()
	AND NOT IS_PHONE_ONSCREEN()
	AND NOT IS_BROWSER_OPEN()
		bBombDoorButtonReleased = TRUE
	ENDIF
ENDPROC

WEAPON_TYPE eCachedBombWeapon = WEAPONTYPE_UNARMED

PROC MAINTAIN_AIR_BOMBS()
	BOOL bUnloadBombModel = FALSE
	
	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
	AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
		IF HAS_NET_TIMER_STARTED(sBombPressDelay)
		AND HAS_NET_TIMER_EXPIRED(sBombPressDelay, 750)
			RESET_NET_TIMER(sBombPressDelay)
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
			
			MAINTAIN_VEHICLE_AIR_BOMBS_HELP_TEXT(vehTemp)
			
			IF IS_VEHICLE_DRIVEABLE(vehTemp)
			AND CAN_LOCAL_PLAYER_USE_AIR_BOMBS(vehTemp)
			AND CAN_VEHICLE_HAVE_AIR_BOMBS(vehTemp)
				IF IS_BIT_SET(iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK2)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
					
					IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
					AND DOES_VEHICLE_HAVE_WEAPONS(vehTemp)
						IF NOT IS_BIT_SET(iAirBombsBS, AIR_BOMBS_BS_DISABLE_VEHICLE_WEAPON)
							GET_CURRENT_PED_VEHICLE_WEAPON(PLAYER_PED_ID(), eCachedBombWeapon)
							
							PRINTLN("[AIR_BOMBS] - MAINTAIN_AIR_BOMBS - Disabling vehicle weapon")
							
							DISABLE_VEHICLE_WEAPON(TRUE, eCachedBombWeapon, vehTemp, PLAYER_PED_ID())
							
							SET_BIT(iAirBombsBS, AIR_BOMBS_BS_DISABLE_VEHICLE_WEAPON)
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(iAirBombsBS, AIR_BOMBS_BS_DISABLE_VEHICLE_WEAPON)
						DISABLE_VEHICLE_WEAPON(FALSE, eCachedBombWeapon, vehTemp, PLAYER_PED_ID())
						
						PRINTLN("[AIR_BOMBS] - MAINTAIN_AIR_BOMBS - Enabling vehicle weapon")
						
						CLEAR_BIT(iAirBombsBS, AIR_BOMBS_BS_DISABLE_VEHICLE_WEAPON)
					ENDIF
				ENDIF
				
				IF CAN_VEHICLE_USE_AIR_BOMBS(vehTemp)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(vehTemp)
						IF NOT GB_IS_PLAYER_ON_SMUGGLER_MISSION(PLAYER_ID())
						AND IS_ENTITY_A_MISSION_ENTITY(vehTemp)
						AND IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehTemp))
							IF IS_BIT_SET(iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
								SET_PLANE_TURBULENCE_MULTIPLIER(vehTemp, 0.0)
							ENDIF
						ENDIF
					ENDIF
					
					MAINTAIN_USING_AIR_BOMBS(vehTemp)
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BOMB_CAM_HELP")
						CLEAR_HELP()
					ENDIF
					
					RESET_NET_TIMER(sBombPressDelay)
					RESET_NET_TIMER(sCarpetBombDelay)
					
					CLEAR_BIT(iAirBombsBS, AIR_BOMBS_BS_CARPET_BOMB_TRIGGERED)
					bUnloadBombModel = TRUE
				ENDIF
				
				IF GET_ENTITY_HEIGHT_ABOVE_GROUND(vehTemp) < 1.5
				OR NOT IS_ENTITY_IN_AIR(vehTemp)
//				OR out of ammo
					IF NOT IS_VEHICLE_MODEL(vehTemp, VOLATOL)
					OR IS_VEHICLE_ON_ALL_WHEELS(vehTemp)
						IF IS_BIT_SET(iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(vehTemp)
								CLOSE_BOMB_BAY_DOORS(vehTemp)
								
								IF NOT GB_IS_PLAYER_ON_SMUGGLER_MISSION(PLAYER_ID())
								AND IS_ENTITY_A_MISSION_ENTITY(vehTemp)
								AND IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehTemp))
									SET_PLANE_TURBULENCE_MULTIPLIER(vehTemp, 1.0)
								ENDIF
							ELSE
								BROADCAST_OPEN_BOMB_BAY_DOORS(ALL_PLAYERS_ON_SCRIPT(), FALSE)
							ENDIF
							
							SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
							
							CLEAR_BIT(iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
						ENDIF
						
						IF IS_BIT_SET(iAirBombsBS, AIR_BOMBS_BS_DISABLE_VEHICLE_WEAPON)
							DISABLE_VEHICLE_WEAPON(FALSE, eCachedBombWeapon, vehTemp, PLAYER_PED_ID())
							
							PRINTLN("[AIR_BOMBS] - MAINTAIN_AIR_BOMBS - Enabling vehicle weapon")
							
							CLEAR_BIT(iAirBombsBS, AIR_BOMBS_BS_DISABLE_VEHICLE_WEAPON)
						ENDIF
					ENDIF
					
					CLEANUP_BOMB_CAMERA()
				ENDIF
			ELSE
				IF IS_BIT_SET(iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(vehTemp)
						CLOSE_BOMB_BAY_DOORS(vehTemp)
						
						IF NOT GB_IS_PLAYER_ON_SMUGGLER_MISSION(PLAYER_ID())
						AND IS_ENTITY_A_MISSION_ENTITY(vehTemp)
						AND IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehTemp))
							SET_PLANE_TURBULENCE_MULTIPLIER(vehTemp, 1.0)
						ENDIF
					ELSE
						BROADCAST_OPEN_BOMB_BAY_DOORS(ALL_PLAYERS_ON_SCRIPT(), FALSE)
					ENDIF
					
					SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
					
					CLEAR_BIT(iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
				ENDIF
				
				CLEAR_BIT(iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
				
				IF IS_BIT_SET(iAirBombsBS, AIR_BOMBS_BS_DISABLE_VEHICLE_WEAPON)
					DISABLE_VEHICLE_WEAPON(FALSE, eCachedBombWeapon, vehTemp, PLAYER_PED_ID())
					
					PRINTLN("[AIR_BOMBS] - MAINTAIN_AIR_BOMBS - Enabling vehicle weapon")
					
					CLEAR_BIT(iAirBombsBS, AIR_BOMBS_BS_DISABLE_VEHICLE_WEAPON)
				ENDIF
				
				CLEANUP_BOMB_CAMERA()
				
				bUnloadBombModel = TRUE
			ENDIF
		ELSE
			CLEANUP_BOMB_CAMERA()
			
			iAirBombsBS = 0
			bUnloadBombModel = TRUE
		ENDIF
	ELSE
		CLEANUP_BOMB_CAMERA()
		
		iAirBombsBS = 0
		bUnloadBombModel = TRUE
	ENDIF
	
	IF bUnloadBombModel
	AND IS_BIT_SET(iAirBombsBS, AIR_BOMBS_BS_MODEL_REQUESTED)
		PRINTLN("[AIR_BOMBS] MAINTAIN_AIR_BOMBS - Unloading bomb model")
		
		IF IS_MODEL_VALID(eBombModel)
			SET_MODEL_AS_NO_LONGER_NEEDED(eBombModel)
		ENDIF
		
		CLEAR_BIT(iAirBombsBS, AIR_BOMBS_BS_MODEL_REQUESTED)
	ENDIF
	
	IF bUnloadBombModel
		CLEANUP_BOMB_CAMERA()
	ENDIF
ENDPROC

CONST_INT OFF_RADAR_BS_HIDDEN			0
CONST_INT OFF_RADAR_BS_HELP_TEXT_SHOWN	1
CONST_INT OFF_RADAR_BS_FLASHING_ARROW	2

FLOAT fMicrolightSpeed = 0.0

INT iOffRadarBS

SCRIPT_TIMER sUltralightAccelDelay

PROC MAINTAIN_ULTRALIGHT_OFF_RADAR()
	BOOL bShowMyBlip = FALSE
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF IS_VEHICLE_MODEL(vehTemp, MICROLIGHT)
			AND GET_VEHICLE_MOD(vehTemp, MOD_EXHAUST) = 1
			AND NOT g_b_On_Race
				IF NOT IS_BIT_SET(iOffRadarBS, OFF_RADAR_BS_HELP_TEXT_SHOWN)
				AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
				AND IS_ENTITY_IN_AIR(vehTemp)
					PRINT_HELP("UL_RADAR_HELP")
					
					SET_BIT(iOffRadarBS, OFF_RADAR_BS_HELP_TEXT_SHOWN)
				ENDIF
				
				FLOAT fAccel = (GET_ENTITY_SPEED(vehTemp) - fMicrolightSpeed)
				
				fMicrolightSpeed = GET_ENTITY_SPEED(vehTemp)
				
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
				AND (fAccel >= g_sMPTunables.fULTRALIGHT_ACCEL_CAP
				OR fMicrolightSpeed > g_sMPTunables.fULTRALIGHT_SPEED_CAP)
					REINIT_NET_TIMER(sUltralightAccelDelay)
				ENDIF
				
				IF HAS_NET_TIMER_STARTED(sUltralightAccelDelay)
				AND HAS_NET_TIMER_EXPIRED(sUltralightAccelDelay, g_sMPTunables.iULTRALIGHT_ACCEL_TIMER)
					RESET_NET_TIMER(sUltralightAccelDelay)
				ENDIF
				
				IF IS_ENTITY_IN_AIR(vehTemp)
				AND NOT HAS_NET_TIMER_STARTED(sUltralightAccelDelay)
					bShowMyBlip = FALSE
				ELSE
					bShowMyBlip = TRUE
				ENDIF
			ELSE
				CLEAR_BIT(iOffRadarBS, OFF_RADAR_BS_HELP_TEXT_SHOWN)
				RESET_NET_TIMER(sUltralightAccelDelay)
				bShowMyBlip = TRUE
			ENDIF
		ELSE
			CLEAR_BIT(iOffRadarBS, OFF_RADAR_BS_HELP_TEXT_SHOWN)
			RESET_NET_TIMER(sUltralightAccelDelay)
			bShowMyBlip = TRUE
		ENDIF
	ELSE
		CLEAR_BIT(iOffRadarBS, OFF_RADAR_BS_HELP_TEXT_SHOWN)
		RESET_NET_TIMER(sUltralightAccelDelay)
		bShowMyBlip = TRUE
	ENDIF
	
	IF bShowMyBlip
		IF IS_BIT_SET(iOffRadarBS, OFF_RADAR_BS_HIDDEN)
			HIDE_MY_PLAYER_BLIP(FALSE, TRUE)
			
			IF IS_MY_PLAYER_ARROW_FLASHING()
			AND IS_BIT_SET(iOffRadarBS, OFF_RADAR_BS_FLASHING_ARROW)
				FLASH_MY_PLAYER_ARROW(FALSE, -1, TRUE)
			ENDIF
			
			CLEAR_BIT(iOffRadarBS, OFF_RADAR_BS_FLASHING_ARROW)
			CLEAR_BIT(iOffRadarBS, OFF_RADAR_BS_HIDDEN)
			
			PRINTLN("MAINTAIN_ULTRALIGHT_OFF_RADAR - Showing blip")
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iOffRadarBS, OFF_RADAR_BS_HIDDEN)
			HIDE_MY_PLAYER_BLIP(TRUE, TRUE)
			
			IF NOT IS_MY_PLAYER_ARROW_FLASHING()
			AND NOT IS_BIT_SET(iOffRadarBS, OFF_RADAR_BS_FLASHING_ARROW)
				SET_BIT(iOffRadarBS, OFF_RADAR_BS_FLASHING_ARROW)
				FLASH_MY_PLAYER_ARROW(TRUE, -1, TRUE)
			ENDIF
			
			SET_BIT(iOffRadarBS, OFF_RADAR_BS_HIDDEN)
			
			PRINTLN("MAINTAIN_ULTRALIGHT_OFF_RADAR - Hiding blip")
		ENDIF
	ENDIF
ENDPROC


CONST_INT RAPPLINGBITSET_PLAYER_RAPPELLING			1
CONST_INT RAPPLINGBITSET_PILOT_ALLOW_RAPPEL			2
CONST_INT RAPPLINGBITSET_PILOT_HOVER_FOR_RAPPEL		3
CONST_INT RAPPLINGBITSET_PLAY_LANDING_SOUND			4 // We need to play the landing sound when we hit the ground.

CONST_FLOAT RAPPELLING_MAX_HEIGHT					40.0
CONST_FLOAT RAPPELLING_MIN_HEIGHT					15.0
CONST_FLOAT RAPPELLING_DONE_MAX_HEIGHT				2.0
CONST_FLOAT RAPPELLING_CANCEL_DISTANCE				5.0

CONST_INT RAPPELLING_MAX_PASSENGERS					6

CONST_INT RAPPEL_BUTTON_TIME 						275
CONST_INT RAPPEL_STABILIZE_TIME 					2000

SCRIPT_TIMER RappelButtonTimer
SCRIPT_TIMER RappelStabilizeTimer
SCRIPT_TIMER m_tRappelStopwatch 	//How long have we been repelling for?
INT m_iRappelSoundId =-1
PLAYER_INDEX piRappellingPassengers[RAPPELLING_MAX_PASSENGERS]
INT iNumRappellingPassengers = 0
VECTOR vRappelPosition

PROC STABILIZE_HELI_RAPPEL(VEHICLE_INDEX vehHeli)
	IF HAS_NET_TIMER_EXPIRED(RappelStabilizeTimer, RAPPEL_STABILIZE_TIME)
		EXIT
	ENDIF

	VECTOR vCurrentHeliRot = GET_ENTITY_ROTATION(vehHeli)
	
	FLOAT fStablizeFrac = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(RappelStabilizeTimer)) / RAPPEL_STABILIZE_TIME
	fStablizeFrac = CLAMP(fStablizeFrac, 0, 1)
	
	PRINTLN("STABILIZE_HELI_RAPPEL - fStablizeFrac = ", fStablizeFrac, ", at time = ", TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(RappelStabilizeTimer)))
	
	SET_ENTITY_ROTATION(vehHeli, <<LERP_FLOAT(vCurrentHeliRot.X, 0, fStablizeFrac), LERP_FLOAT(vCurrentHeliRot.Y, 0, fStablizeFrac), vCurrentHeliRot.Z>>)
ENDPROC

PROC PILOT_CLEANUP_HELI_RAPPEL()

	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PILOT_ALLOW_RAPPEL)
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PILOT_ALLOW_RAPPEL)
	ENDIF
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PILOT_HOVER_FOR_RAPPEL)
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PILOT_HOVER_FOR_RAPPEL)
		IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
			CLEAR_PED_TASKS(PLAYER_PED_ID())
		ENDIF
	ENDIF
	IF iNumRappellingPassengers > 0
		iNumRappellingPassengers = 0
	ENDIF
ENDPROC

PROC PILOT_ALLOW_HELI_RAPPEL()

	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
	AND IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
		VEHICLE_INDEX vehHeli = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		IF NOT IS_VEHICLE_FUCKED_MP(vehHeli)
		AND GET_PED_IN_VEHICLE_SEAT(vehHeli, VS_DRIVER) = PLAYER_PED_ID()
			IF NETWORK_HAS_CONTROL_OF_ENTITY(vehHeli)
			
				//SET IF PASSANGERS ARE ALLOWED TO RAPPEL
				IF DOES_VEHICLE_ALLOW_RAPPEL(vehHeli)
					IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PILOT_ALLOW_RAPPEL)
						PRINTLN("PILOT_ALLOW_HELI_RAPPEL - pilot - rappel is available for this vehicle")
						SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PILOT_ALLOW_RAPPEL)
					ENDIF
				ELIF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PILOT_ALLOW_RAPPEL)
					PRINTLN("PILOT_ALLOW_HELI_RAPPEL - pilot - rappel is not available for this vehicle")
					CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PILOT_ALLOW_RAPPEL)
				ENDIF
				
				//HOVER WHILE A PASSENGER IS RAPPELLING
				
				// Check for rappelling passengers
				INT i, j
				BOOL bPassengerRappling = FALSE
				REPEAT GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(vehHeli) i
					VEHICLE_SEAT eSeat = INT_TO_ENUM(VEHICLE_SEAT, i)
					IF eSeat != VS_FRONT_RIGHT
					AND DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(vehHeli, eSeat))
						PLAYER_INDEX piPassenger = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(vehHeli, eSeat))
						IF piPassenger != INVALID_PLAYER_INDEX() 
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(piPassenger)].iRapplingBitset, RAPPLINGBITSET_PLAYER_RAPPELLING)
								bPassengerRappling = TRUE
								
								// Find if this rappelling passenger is a new passenger
								BOOL bNewPassenger = TRUE
								REPEAT iNumRappellingPassengers j
//									PRINTLN("PILOT_ALLOW_HELI_RAPPEL - pilot - checking passenger index ", NATIVE_TO_INT(piPassenger), " against stored index ", NATIVE_TO_INT(piRappellingPassengers[j]), " rappelling at index ", j, ".")
									IF piRappellingPassengers[j] = piPassenger
										bNewPassenger = FALSE
										PRINTLN("PILOT_ALLOW_HELI_RAPPEL - pilot - found passenger index ", NATIVE_TO_INT(piPassenger), " rappelling at index ", j, ".")
										BREAKLOOP
									ENDIF
								ENDREPEAT
								
								
								// Add to front of piRappellingPassengers array
								IF bNewPassenger
									PRINTLN("PILOT_ALLOW_HELI_RAPPEL - pilot - new passenger index ", NATIVE_TO_INT(piPassenger), " rappelling at index ", iNumRappellingPassengers, ".")
									iNumRappellingPassengers++
									REPEAT iNumRappellingPassengers j
										IF iNumRappellingPassengers - j > 1
											IF iNumRappellingPassengers - j - 1 < RAPPELLING_MAX_PASSENGERS
												piRappellingPassengers[iNumRappellingPassengers - j - 1] = piRappellingPassengers[iNumRappellingPassengers - j - 2]
											ELSE
												PRINTLN("PILOT_ALLOW_HELI_RAPPEL - Can't add new passenger, increase RAPPELLING_MAX_PASSENGERS")
											ENDIF
										ENDIF
									ENDREPEAT
									piRappellingPassengers[0] = piPassenger
									BREAKLOOP
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
				// Find rappeling passengers outside the heli
				IF !bPassengerRappling
				AND iNumRappellingPassengers > 0
					INT iNewNumPassengers = iNumRappellingPassengers
					REPEAT iNumRappellingPassengers j
						IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(piRappellingPassengers[iNumRappellingPassengers - j - 1])].iRapplingBitset, RAPPLINGBITSET_PLAYER_RAPPELLING)
							bPassengerRappling = TRUE
							BREAKLOOP
						ELSE
							PRINTLN("PILOT_ALLOW_HELI_RAPPEL - pilot - new passenger stopped rappelling")
							iNewNumPassengers--
						ENDIF
					ENDREPEAT
					iNumRappellingPassengers = iNewNumPassengers
				ENDIF
				
		
				
				IF bPassengerRappling
				AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PILOT_HOVER_FOR_RAPPEL)
					// Start hover
				
					SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PILOT_HOVER_FOR_RAPPEL)
				
					REINIT_NET_TIMER(RappelStabilizeTimer)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					vRappelPosition = GET_ENTITY_COORDS(vehHeli)
					TASK_HELI_MISSION(PLAYER_PED_ID(),vehHeli,null,null,vRappelPosition,MISSION_GOTO,0,-1,-1,-1,-1)
					SET_AIRCRAFT_PILOT_SKILL_NOISE_SCALAR(vehHeli, 0.0)
					
					
					PRINTLN("PILOT_ALLOW_HELI_RAPPEL - PASSENGER RAPPLING - START RAPPELL HOVER")
				ENDIF
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PILOT_HOVER_FOR_RAPPEL)
					// Maintain hover
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehHeli, DEFAULT_VEH_STOPPING_DISTANCE*2, DEFAULT, DEFAULT, TRUE)
					
					//causes syncing issues
					//STABILIZE_HELI_RAPPEL(vehHeli)
					
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehHeli), vRappelPosition) > RAPPELLING_CANCEL_DISTANCE
						bPassengerRappling = FALSE
					ENDIF
					
					IF NOT bPassengerRappling
						SET_AIRCRAFT_PILOT_SKILL_NOISE_SCALAR(vehHeli, 1.0)
						CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PILOT_HOVER_FOR_RAPPEL)
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						PRINTLN("PILOT_ALLOW_HELI_RAPPEL - ENDING RAPPELL HOVER")
					ELSE
						//display help for this
						DISPLAY_HELP_TEXT_THIS_FRAME("RAPP_HELP_PLT", FALSE)
						PRINTLN("PILOT_ALLOW_HELI_RAPPEL - WAITING ON RAPPELL PASSENGER EXIT")
					ENDIF
				ELSE
					//PRINTLN("PILOT_ALLOW_HELI_RAPPEL - NOT HOVERING")
				ENDIF
			ELSE
				NETWORK_REQUEST_CONTROL_OF_ENTITY(vehHeli)
				//PRINTLN("PILOT_ALLOW_HELI_RAPPEL - Requesting heli for pilot")
			ENDIF
		ELSE
			//PRINTLN("PILOT_ALLOW_HELI_RAPPEL - Heli is fucked or not a pilot")
			PILOT_CLEANUP_HELI_RAPPEL()
		ENDIF
	ELSE 
//		#IF IS_DEBUG_BUILD
//		IF (GET_FRAME_COUNT() % 30) = 0
//		PRINTLN("PILOT_ALLOW_HELI_RAPPEL - Not in heli")
//		ENDIF
//		#ENDIF
		PILOT_CLEANUP_HELI_RAPPEL()
	ENDIF
ENDPROC

PLAYER_INDEX piRappellingPilot

PROC PASSENGER_HELI_RAPPEL()
	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
	AND IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
	
		VEHICLE_INDEX vehHeli = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		IF IS_VEHICLE_FUCKED_MP(vehHeli)
		OR GET_PED_IN_VEHICLE_SEAT(vehHeli, VS_FRONT_RIGHT) = PLAYER_PED_ID()
		OR GET_PED_IN_VEHICLE_SEAT(vehHeli, VS_DRIVER) = PLAYER_PED_ID()
			EXIT
		ENDIF
		
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PLAYER_RAPPELLING)

			BOOL bIsRappelPressed = FALSE
			BOOL bIsRappelSupported = FALSE
			
			//check heli in correct state
			IF GET_ENTITY_SPEED(vehHeli) < 10.0
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_RAPPEL_FROM_HELI) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_RAPPEL_FROM_HELI) != WAITING_TO_START_TASK
			AND SAFE_TO_DO_INPUT_CHECK()
			AND GET_ENTITY_HEIGHT_ABOVE_GROUND(vehHeli) < RAPPELLING_MAX_HEIGHT
			AND GET_ENTITY_HEIGHT_ABOVE_GROUND(vehHeli) > RAPPELLING_MIN_HEIGHT
			AND IS_ENTITY_UPRIGHT(vehHeli, 15.0)
			AND NOT IS_ENTITY_UPSIDEDOWN(vehHeli)
				bIsRappelSupported = TRUE
			ENDIF
			
			//check heli supports rappling
			IF bIsRappelSupported
				IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(vehHeli, VS_DRIVER))
					piRappellingPilot = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(vehHeli, VS_DRIVER))
					IF piRappellingPilot = INVALID_PLAYER_INDEX() 
						bIsRappelSupported = FALSE
						PRINTLN("MAINTAIN_HELI_RAPPEL - pilot is not a player, disabling rappel")
					ELIF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(piRappellingPilot)].iRapplingBitset, RAPPLINGBITSET_PILOT_ALLOW_RAPPEL)
						bIsRappelSupported = FALSE
						PRINTLN("MAINTAIN_HELI_RAPPEL - rappel is not available for this vehicle")
					ENDIF
				ELSE
					bIsRappelSupported = FALSE
					PRINTLN("MAINTAIN_HELI_RAPPEL - no pilot, disabling rappel")
				ENDIF
			ENDIF
			
			IF bIsRappelSupported
				IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				AND IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_JUMP)//INPUT_ENTER)
					bIsRappelPressed = TRUE
				ELIF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)//INPUT_ENTER)
				OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
					bIsRappelPressed = TRUE
				ELSE
					//display help for this
					IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
						DISPLAY_HELP_TEXT_THIS_FRAME("RAPP_HELP_PAS1", FALSE)
					ELSE
						DISPLAY_HELP_TEXT_THIS_FRAME("RAPP_HELP_PAS0", FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			IF bIsRappelPressed
				
				IF HAS_NET_TIMER_EXPIRED(RappelButtonTimer, RAPPEL_BUTTON_TIME)
				
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					TASK_RAPPEL_FROM_HELI(PLAYER_PED_ID())
					
					RESET_NET_TIMER(m_tRappelStopwatch)
								
					PRINTLN("MAINTAIN_HELI_RAPPEL - TASK_RAPPEL_FROM_HELI - A")
					SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PLAYER_RAPPELLING)
					SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PLAY_LANDING_SOUND)
				ELSE
					PRINTLN("MAINTAIN_HELI_RAPPEL - WAITING ON RAPPELL HOLD BUTTON TIME")
				ENDIF
			ELSE
				IF HAS_NET_TIMER_STARTED(RappelButtonTimer)
					RESET_NET_TIMER(RappelButtonTimer)
					PRINTLN("MAINTAIN_HELI_RAPPEL - RESET RappelButtonTimer - B")
				ENDIF
			ENDIF
		ELSE 
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_RAPPEL_FROM_HELI) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_RAPPEL_FROM_HELI) != WAITING_TO_START_TASK
				CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PLAYER_RAPPELLING)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PLAYER_RAPPELLING)
			
			//If we have already landed, we should stop rappel sounds...
			BOOL bStopRappelLoop = NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PLAY_LANDING_SOUND)
			
				// Stop rappel if pilot stops hovering (if destroyed or collision with another aircraft)
			IF piRappellingPilot = INVALID_PLAYER_INDEX()
			OR NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(piRappellingPilot)].iRapplingBitset, RAPPLINGBITSET_PILOT_HOVER_FOR_RAPPEL)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					PRINTLN("MAINTAIN_HELI_RAPPEL - Cancelling rappel after collision/destruction or pilot absence")
					CLEAR_PED_TASKS(PLAYER_PED_ID())
				ENDIF
			ENDIF
			
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_RAPPEL_FROM_HELI) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_RAPPEL_FROM_HELI) != WAITING_TO_START_TASK
				bStopRappelLoop = TRUE
				CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PLAYER_RAPPELLING)
			ENDIF		
			
			//Start sound loop if player has begun decent...
			IF m_iRappelSoundId = -1
			AND NOT bStopRappelLoop
			AND HAS_NET_TIMER_EXPIRED(m_tRappelStopwatch, cfRAPPEL_LOOP_START_TIME)
				m_iRappelSoundId = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND( m_iRappelSoundId, "Rappel_Loop", "GTAO_Rappel_Sounds" ) 
				PRINTLN("MAINTAIN_HELI_RAPPEL - Starting Rappel_Loop (sound id == ",m_iRappelSoundId,")")
			ELIF bStopRappelLoop
			AND m_iRappelSoundId <> -1
				PRINTLN("MAINTAIN_HELI_RAPPEL - Stopping Rappel_Loop (sound id == ",m_iRappelSoundId,")")
				STOP_SOUND( m_iRappelSoundId )
				m_iRappelSoundId = -1
				PRINTLN("MAINTAIN_HELI_RAPPEL - Playing Rappel_Stop")
				PLAY_SOUND_FRONTEND( -1, "Rappel_Stop", "GTAO_Rappel_Sounds" ) 
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PLAY_LANDING_SOUND)
		IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
			IF GET_ENTITY_HEIGHT_ABOVE_GROUND(GET_PLAYER_PED(PLAYER_ID())) <= 1.0 	//Player resting height above ground
				CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PLAY_LANDING_SOUND)
				
				IF NOT IS_ENTITY_UNDERWATER(GET_PLAYER_PED(PLAYER_ID()))
				AND NOT IS_ENTITY_SUBMERGED_IN_WATER(GET_PLAYER_PED(PLAYER_ID()))
					PRINTLN("MAINTAIN_HELI_RAPPEL - Playing Rappel_Land")
					PLAY_SOUND_FRONTEND( -1, "Rappel_Land", "GTAO_Rappel_Sounds")	
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("MAINTAIN_HELI_RAPPEL - Not playing Rappel_Land because player landed in water")
				#ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("MAINTAIN_HELI_RAPPEL - Not playing Rappel_Land because player is not ok")
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRapplingBitset, RAPPLINGBITSET_PLAY_LANDING_SOUND)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_HELI_RAPPEL()
	PILOT_ALLOW_HELI_RAPPEL()
	PASSENGER_HELI_RAPPEL()
ENDPROC

BOOL bHasRunTurretIAAScript
PROC MANAGE_MISSION_TURRET_ACTIVITY_SCRIPTS()
//		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MANAGE_MISSION_TURRET_ACTIVITY_SCRIPTS")
		IF NOT bHasRunTurretIAAScript
			IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
			AND NETWORK_IS_GAME_IN_PROGRESS()
			AND NETWORK_IS_SESSION_ACTIVE()
//				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "g_bIAAGunCamerasEnabled = ", g_bIAAGunCamerasEnabled)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2061.5842, 2993.7402, -68.7022>>) < 20
//					IF g_bIAAGunCamerasEnabled
		//			AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = GET_INTERIOR_AT_COORDS(<<2061.5842, 2993.7402, -68.7022>>) // If in the morgue 
		//			AND MC_playerBD[iLocalPart].iGameState
						CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MANAGE_MISSION_TURRET_ACTIVITY_SCRIPTS: Requesting script: missionIAATurret")
						REQUEST_SCRIPT("missionIAATurret")
						
						IF HAS_SCRIPT_LOADED("missionIAATurret")
							CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MANAGE_MISSION_TURRET_ACTIVITY_SCRIPTS: HAS_SCRIPT_LOADED = TRUE for script: missionIAATurret")
							START_NEW_SCRIPT("missionIAATurret", ACTIVITY_CREATOR_INT_STACK_SIZE)
							
							SET_SCRIPT_AS_NO_LONGER_NEEDED("missionIAATurret")
							bHasRunTurretIAAScript = TRUE
						ELSE
							CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MANAGE_MISSION_TURRET_ACTIVITY_SCRIPTS: NOT LOADED script: net_apartment_activity, ")
						ENDIF
//					ELSE
//					ENDIF
				ENDIF
			ELSE
//				IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MANAGE_MISSION_TURRET_ACTIVITY_SCRIPTS: IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE) = TRUE")
//				ELSE
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MANAGE_MISSION_TURRET_ACTIVITY_SCRIPTS: IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE) = FALSE")
//				ENDIF
//				
//				IF NETWORK_IS_GAME_IN_PROGRESS()
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MANAGE_MISSION_TURRET_ACTIVITY_SCRIPTS: NETWORK_IS_GAME_IN_PROGRESS = TRUE")
//				ELSE
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MANAGE_MISSION_TURRET_ACTIVITY_SCRIPTS: NETWORK_IS_GAME_IN_PROGRESS = FALSE")
//				ENDIF
//				
//				IF NETWORK_IS_SESSION_ACTIVE()
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MANAGE_MISSION_TURRET_ACTIVITY_SCRIPTS: NETWORK_IS_SESSION_ACTIVE = TRUE")
//				ELSE
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MANAGE_MISSION_TURRET_ACTIVITY_SCRIPTS: NETWORK_IS_SESSION_ACTIVE = FALSE")
//				ENDIF
			ENDIF
		ELSE
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("missionIAATurret")) = 0 
			AND bHasRunTurretIAAScript
			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2061.5842, 2993.7402, -68.7022>>) > 20
				bHasRunTurretIAAScript = FALSE
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MANAGE_MISSION_TURRET_ACTIVITY_SCRIPTS: Reseting bHasRunTurretIAAScript = FALSE")
			ENDIF
			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MANAGE_MISSION_TURRET_ACTIVITY_SCRIPTS: NOT LOADED script: missionIAATurret running ")
			
		ENDIF	
	
ENDPROC


#IF IS_DEBUG_BUILD
PROC MAINTAIN_DEBUG_CLEAR_BADSPORT_INPUT()
	IF IS_DEBUG_KEY_JUST_RELEASED(KEY_O, KEYBOARD_MODIFIER_CTRL_SHIFT, "Clear_Badsport_shortcut")
		SET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BadSport, 0)
		REQUEST_SAVE()
		PRINT_TICKER("BADSPORT_CLEAR")
		PRINTLN("MAINTAIN_DEBUG_CLEAR_BADSPORT_INPUT - overall badsport has been cleared - SET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BadSport, 0)")
	ENDIF
ENDPROC
#ENDIF

PROC MAINTAIN_PASSIVE_MODE_KILL_TIMER()
	IF HAS_NET_TIMER_STARTED(g_stPassiveModeKillTimer)
		IF HAS_NET_TIMER_EXPIRED(g_stPassiveModeKillTimer, PASSIVE_MODE_COOLDOWN_TIME_AFTER_KILL)
			RESET_NET_TIMER(g_stPassiveModeKillTimer)
			PRINTLN("[PASSIVE_MODE] - MAINTAIN_PASSIVE_MODE_KILL_TIMER - g_stPassiveModeKillTimer Timer has expired, resetting.")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_GAME_STATE_RUNNING()

	//TEMP TEMP TEMP location!!
	//Neil put this where ever
	SET_MULTIPLAYER_HUD_CASH(GET_PLAYER_FM_CASH(PLAYER_ID()),TRUE )
		
		
	Maintain_Main_Generic_Client()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_Main_Generic_Client")
	#ENDIF
	#ENDIF	
	
	IF MpGlobals.Initialise_OHD_Displays = FALSE //Just a one off when you enter the game, but needs to be here to give everything time to setup. 
		INITIALISE_OVERHEADS()
		MpGlobals.Initialise_OHD_Displays = TRUE
	ENDIF
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("INITIALISE_OVERHEADS")
	#ENDIF
	#ENDIF
	
	//Load the audio bank that's for the events, it's always needed. 
	IF NOT bAudioLoaded
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_GTAO/SNACKS")
			CPRINTLN(DEBUG_NET_AMBIENT, "[FM AME] - REQUEST_SCRIPT_AUDIO_BANK(\"DLC_GTAO/SNACKS\")")
			bAudioLoaded = TRUE
		ENDIF
	ENDIF
	
	UPDATE_STAGGERED_LOOP_FOR_FREEMODE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("UPDATE_STAGGERED_LOOP_FOR_FREEMODE")
	#ENDIF
	#ENDIF

	MAINTAIN_NEAR_PLAYERS_LOOP()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_NEAR_PLAYERS_LOOP")
	#ENDIF
	#ENDIF	
	
	MAINTAIN_BROADCAST_IF_I_AM_SESSION_HOST()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_BROADCAST_IF_I_AM_SESSION_HOST")
	#ENDIF
	#ENDIF	
	
	MAINTAIN_RESTRICTED_INTERIOR_ACCESS_FOR_LOCAL_PLAYER(sRIA)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_RESTRICTED_INTERIOR_ACCESS_FOR_LOCAL_PLAYER")
	#ENDIF
	#ENDIF	
	

	#IF IS_DEBUG_BUILD
	DEBUG_DISABLE_PHONE()
	#ENDIF
	
	MAINTAIN_OVER_UNIQUE_FRAMES()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_OVER_UNIQUE_FRAMES")
	#ENDIF
	#ENDIF
	
	
	MAINTAIN_PHONE_ABILITIES() //Turns out this does have to run every frame, sorry!
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PHONE_ABILITIES")
	#ENDIF
	#ENDIF
	
	MAINTAIN_SPECTATOR_TARGET_INFORMATION()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SPECTATOR_TARGET_INFORMATION")
	#ENDIF
	#ENDIF 
	
	IF NOT IS_MINIGAME_IN_PROGRESS()
	OR NOT (bEnableMinigameCheck)				
		MAINTAIN_FREEMODE_MAIN_GAME_STATE_RUNNING()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FREEMODE_MAIN_GAME_STATE_RUNNING")
		#ENDIF
		#ENDIF 	
	ENDIF
	
	UPDATE_BACKUP_HELI_GLOBAL_REL_GROUP(iBackupHeliPartCount, bDoHeliRelGroupUpdate, timerUpdateGlobalHeliRelGroup, iFriendlyBitsetLocalCopy) // Needs to be ran in ther global staggered loop. everyone must update a copy of their rel group for (but only player is actually running the script).
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER( "UPDATE_BACKUP_HELI_GLOBAL_REL_GROUP")
		#ENDIF
	#ENDIF	

	BOOL bCheckLaunchPropertyInt = TRUE
	IF GET_CLIENT_SCRIPT_GAME_STATE(PLAYER_ID()) =MAIN_GAME_STATE_HOLD_FOR_TRANSITION
		IF SHOULD_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT()
		AND g_SpawnData.NextSpawn.Location = SPAWN_LOCATION_INSIDE_PROPERTY
			// allow interior script to launch early in this case.
		ELSE
			bCheckLaunchPropertyInt = FALSE
		ENDIF
	ENDIF
	MAINTAIN_MP_PROPERTIES(bCheckLaunchPropertyInt)
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MP_PROPERTIES()")
		#ENDIF
	#ENDIF	
	
	MAINTAIN_DISABLE_CAR_ENTRY_DURING_WARP()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DISABLE_CAR_ENTRY_DURING_WARP()")
		#ENDIF
	#ENDIF	
	
	MAINTAIN_INCOMING_PLAYER_CALLS()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PARTY_TARGETTING_FLAG()")
		#ENDIF
	#ENDIF	

	MAINTAIN_FREEMODE_DATA_LEADERBOARD_READ_WRITE()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FREEMODE_DATA_LEADERBOARD_READ_WRITE")
		#ENDIF
	#ENDIF

	MAINTAIN_PASSIVE_MODE_EFFECTS()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PASSIVE_MODE_EFFECTS")
		#ENDIF
	#ENDIF			
	
	IF HAS_IMPORTANT_STATS_LOADED()
		MAINTAIN_CHUTE_ON()
		#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CHUTE_ON")
			#ENDIF
		#ENDIF
	ENDIF
	
	MAINTAIN_TEXT_MESSAGE_LITERAL()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_TEXT_MESSAGE_LITERAL")
		#ENDIF
	#ENDIF	
	
	//Deal with personal vehicle
	MAINTAIN_MP_SAVED_VEHICLE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MP_SAVED_VEHICLE()")
	#ENDIF
	#ENDIF

	
	#IF IS_DEBUG_BUILD
	UPDATE_TUG_BOAT_PED_PLACEMENT_WIDGETS(sTugBoatPedPlacementWidgetData)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_TUG_BOAT_PED_PLACEMENT_WIDGETS()")
	#ENDIF
	#ENDIF	
	#ENDIF

	//Deal with remote personal vehicle
	MAINTAIN_REMOTE_MP_SAVED_VEHICLE(RemoteSavedVehicleTimer)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_REMOTE_MP_SAVED_VEHICLE()")
	#ENDIF
	#ENDIF	
	
	//maintain player entering the runner2000 electrocution.
	MAINTAIN_RUINER2000(bRuinerPersonalCheckDisable)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("	MAINTAIN_RUINER2000")
	#ENDIF
	#ENDIF
	
	MAINTAIN_HELI_RAPPEL()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("	MAINTAIN_HELI_RAPPEL()")
	#ENDIF
	#ENDIF
	
	MAINTAIN_VEHICLE_MINES()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_VEHICLE_MINES()")
	#ENDIF
	#ENDIF
	
	MAINTAIN_AIR_COUNTERMEASURES()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_AIR_COUNTERMEASURES()")
	#ENDIF
	#ENDIF
	
	MAINTAIN_AIR_BOMBS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_AIR_BOMBS()")
	#ENDIF
	#ENDIF
	
	MAINTAIN_ULTRALIGHT_OFF_RADAR()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ULTRALIGHT_OFF_RADAR()")
	#ENDIF
	#ENDIF
	
	MAINTAIN_ZANCUDO_AIRFIELD_MODEL_HIDES()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ZANCUDO_AIRFIELD_MODEL_HIDES")
	#ENDIF
	#ENDIF
	
	MAINTAIN_SIMPLE_INTERIOR_VEHICLE_ENTRY()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SIMPLE_INTERIOR_VEHICLE_ENTRY()")
	#ENDIF
	#ENDIF
	
	MAINTAIN_PLAYER_ACTION_COOLDOWN()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PLAYER_ACTION_COOLDOWN()")
	#ENDIF
	#ENDIF
	
	MAINTAIN_HEADSHOT_DELETION(HeadshotDeletionStages)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_HEADSHOT_DELETION")
	#ENDIF
	#ENDIF
	
	MAINTAIN_LOOK_AT_NEAREST_FRIEND(lookAtFriend)
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER( "MAINTAIN_LOOK_AT_NEAREST_FRIEND")
		#ENDIF
	#ENDIF	
	
	//Host of the session checks are in here. Don't move to host of this script section. 
	RUN_HOST_KICKING_CHECKS_OVERALL(iPlayerKickCheckIndexInternal, iPlayerKickCheckIndexExternal, iNumberOfVotesToKickPlayer)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("RUN_HOST_KICKING_CHECKS_OVERALL")
	#ENDIF
	#ENDIF 
	
	RETAIN_LAST_USED_VEHICLE_WHEN_ENTERING_PROPERTY()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("RETAIN_LAST_USED_VEHICLE_WHEN_ENTERING_PROPERTY()")
	#ENDIF
	#ENDIF
	
	
	// Handle end of mode cleanup.
	POST_MISSION_CLEANUP(g_TransitionSessionNonResetVars.sPostMissionCleanupData)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("POST_MISSION_CLEANUP")
	#ENDIF
	#ENDIF
	
	
	DISPLAY_LOCAL_TICKER_OFFLINE_PLAY(bDisplayOfflineTicker, bDisplayOfflineTimer)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("DISPLAY_LOCAL_TICKER_OFFLINE_PLAY")
	#ENDIF
	#ENDIF
	
	RUN_FRIEND_IN_GAME_FEED(g_b_RunFriendsInSessionFeed)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("RUN_FRIEND_IN_GAME_FEED")
	#ENDIF
	#ENDIF
	
	RUN_FRIEND_IN_ONLINE_WORLD_FEED(g_b_RunFriendsInOnlineFeed)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("RUN_FRIEND_IN_ONLINE_WORLD_FEED")
	#ENDIF
	#ENDIF
	
	RUN_QUEUE_REMINDER_MESSAGE(st_QueueReminder, b_QueueFirstTimeBool)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("RUN_QUEUE_REMINDER_MESSAGE")
	#ENDIF
	#ENDIF
	
	
	RUN_CHAT_RESTRICTION_MESSAGE(st_ChatRestrictionsMessageTimer)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("RUN_CHAT_RESTRICTION_MESSAGE")
	#ENDIF
	#ENDIF
	
	RUN_UGC_RESTRICTION_MESSAGE(st_UGCRestrictionsMessageTimer)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("RUN_UGC_RESTRICTION_MESSAGE")
	#ENDIF
	#ENDIF
	
	RUN_CASH_GIFT_MESSAGE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("RUN_CASH_GIFT_MESSAGE")
	#ENDIF
	#ENDIF
	
	RUN_RP_GIFT_MESSAGE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("RUN_RP_GIFT_MESSAGE")
	#ENDIF
	#ENDIF
	
	RUN_SET_RP_GIFT_ADMIN_MESSAGE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("RUN_SET_RP_GIFT_ADMIN_MESSAGE")
	#ENDIF
	#ENDIF	
	
	#IF IS_DEBUG_BUILD
	RUN_NO_MORE_TUTORIALS_WARNING_FEED()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("RUN_NO_MORE_TUTORIALS_WARNING_FEED")
	#ENDIF
	#ENDIF
	#ENDIF
	
	GENERATE_VEHICLE_STATS_FOR_ALL_VEHICLE_CLASSES(vehicleStatLoading)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("GENERATE_VEHICLE_STATS_FOR_ALL_VEHICLE_CLASSES")
	#ENDIF
	#ENDIF
	
	MAINTAIN_CAN_PICKUP_ITEM_CHECKS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CAN_PICKUP_ITEM_CHECKS")
	#ENDIF
	#ENDIF
	
	// facial idles
	IF g_FMMC_STRUCT.iMissionType != FMMC_TYPE_RACE
	AND g_FMMC_STRUCT.iMissionType != FMMC_TYPE_DEATHMATCH
	AND NOT IS_PLAYER_ON_IMPROMPTU_DM()		
	AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
		MAINTAIN_FACIALS_IDLES(iFacialIdles)				
	ELSE
		iFacialIdles = 0 // so when the mission finishes it returns to default.					
	ENDIF
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FACIALS_IDLES")
	#ENDIF
	#ENDIF	
	
	IF !bIsTrenchcoatLocoSet	
		MAINTAIN_PLAYER_WALK_STYLE(iWalkingStyleStage)
	ENDIF
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PLAYER_WALK_STYLE")
	#ENDIF
	#ENDIF
	
	MAINTAIN_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PLAYER_LOCALLY_VISIBLE_FOR_FADE")
	#ENDIF
	#ENDIF
	
	MAINTAIN_CLIENT_RESET_SERVER_PROPERTY_INSTANCE_REQUEST_DATA()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLIENT_RESET_SERVER_PROPERTY_INSTANCE_REQUEST_DATA")
	#ENDIF
	#ENDIF

	MAINTAIN_PROPERTY_NEAR_PLYS_WARNING()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PROPERTY_NEAR_PLYS_WARNING")
	#ENDIF
	#ENDIF
	
	MAINTAIN_SNOWBALLS(SnowballData)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SNOWBALLS")
	#ENDIF
	#ENDIF
	
	MAINTAIN_CAN_COLLECT_ARMOUR()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CAN_COLLECT_ARMOUR")
	#ENDIF
	#ENDIF
	
	MAINTAIN_PLATFORM_UPGRADE_LB_CHECKS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PLATFORM_UPGRADE_LB_CHECKS")
	#ENDIF
	#ENDIF
	
	MAINTAIN_SNOWBALLS(SnowballData)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SNOWBALLS")
	#ENDIF
	#ENDIF
	
	MAINTAIN_DROP_CASH_TRANSACTIONS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DROP_CASH_TRANSACTIONS")
	#ENDIF
	#ENDIF
	
	MAINTAIN_TRENCHCOAT_LOCO()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_TRENCHCOAT_LOCO")
	#ENDIF
	#ENDIF
	
	PREVENT_INVISIBLE_PLAYER_WITH_CONTROL()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("PREVENT_INVISIBLE_PLAYER_WITH_CONTROL")
	#ENDIF
	#ENDIF
	
	MAINTAIN_VEHICLE_EXPORT_FREEMODE_LOGIC()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_VEHICLE_EXPORT_FREEMODE_LOGIC")
	#ENDIF
	#ENDIF
	
	MAINTAIN_PASSIVE_MODE_KILL_TIMER()
	
	
	// do a force update of re-entry position
	IF (g_b_ForceGetLastCoordsForReEntryUpdate)
		MAINTAIN_STORE_PLAYER_RE_ENTRY_STATS(TRUE)
		g_b_ForceGetLastCoordsForReEntryUpdate = FALSE
	ENDIF
	
	
	RUN_BG_SCRIPT_KICK_CHECK(BGScriptKickDetails)

	CLIENT_UPDATE_FM_WEAPON_PICKUPS(iFMWeaponPickups_ClientStagger)

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("BEFORE_MAINTAIN_SIMPLE_INTERIOR")
	#ENDIF
	#ENDIF
	MAINTAIN_SIMPLE_INTERIOR(SimpleInteriorLocalData)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("AFTER_MAINTAIN_SIMPLE_INTERIOR")
	#ENDIF
	#ENDIF


	GB_BOSS_MAINTAIN_TEXT_MESSAGES(sBossTextVars)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("GB_BOSS_MAINTAIN_TEXT_MESSAGES")
	#ENDIF
	#ENDIF
	
	MAINTAIN_NO_WANTED_LEVEL_GAIN_ZONES(sNoWantedLevelGainZonesData)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_NO_WANTED_LEVEL_GAIN_ZONES")
	#ENDIF
	#ENDIF
		
	#IF IS_DEBUG_BUILD
	MAINTAIN_RESTRICTED_INTERIOR_WIDGETS(sRIA)
	SIMPLE_CUTSCENE_MAINTAIN_DEBUG_WIDGETS(g_SimpleInteriorData.db_simpleCutscene)
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_RESTRICTED_INTERIOR_WIDGETS")
	#ENDIF
	#ENDIF

	
	STOP_JACKING_IF_OTHER_PLAYER_IS_WARPING()	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("STOP_JACKING_IF_OTHER_PLAYER_IS_WARPING")
	#ENDIF
	#ENDIF
	
	FREEMODE_DELIVERY_MAINTAIN(freemodeDeliveryData)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("FREEMODE_DELIVERY_MAINTAIN")
	#ENDIF
	#ENDIF


	MAINTAIN_CLUBHOUSE_GUEST_BIKE_BAIL_CLEANUP()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLUBHOUSE_GUEST_BIKE_BAIL_CLEANUP")
	#ENDIF
	#ENDIF
	

	
	MAINTAIN_CASINO_COLLECTABLES(collectables_missiondata_main)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CASINO_COLLECTABLES")
	#ENDIF
	#ENDIF
	

	
		MAINTAIN_RC_BANDITO_REQUEST_PIM()

	
	
	// ******************************************************************************
	//		ONLY NEED TO BE PROCESSED WHILE ON MISSION
	// ******************************************************************************
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(), TRUE)
		MAINTAIN_CLIENT_BETTING()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLIENT_BETTING")
		#ENDIF
		#ENDIF
		
		MaintainDisableJumpInInteriors() // see bug 1819453
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MaintainDisableJumpInModShop")
		#ENDIF
		#ENDIF
		

	// ******************************************************************************
	//		ONLY NEED TO BE PROCESSED OFF MISSION
	// ******************************************************************************
	ELSE
		MAINTAIN_SAVE_LAST_VEH_WHEN_ENTERING_CORONA()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SAVE_LAST_VEH_WHEN_ENTERING_CORONA")
		#ENDIF
		#ENDIF
		
		MAINTAIN_REQUEST_CORONA_VEH_WORLD_CHECK()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_REQUEST_CORONA_VEH_WORLD_CHECK")
		#ENDIF
		#ENDIF
		
		MAINTAIN_CREATE_BEFORE_CORONA_VEHICLE()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CREATE_BEFORE_CORONA_VEHICLE")
		#ENDIF
		#ENDIF	
			
		MAINTAIN_REMOVING_CORONA_VEHICLE_WORLD_CHECK_DECORATORS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_REMOVING_CORONA_VEHICLE_WORLD_CHECK_DECORATORS")
		#ENDIF
		#ENDIF
		
		MAINTAIN_EARN_JOBSHARE_CASH_TRANSACTIONS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_EARN_JOBSHARE_CASH_TRANSACTIONS")
		#ENDIF
		#ENDIF
		
		// JamesA: [18/8/15] New prototype of Online Wanted System
		#IF IS_DEBUG_BUILD
		#IF FEATURE_WANTED_SYSTEM
		WANTED_SYSTEM_MAINTAIN_MAIN_LOOP(g_wantedSystemData)
		#ENDIF
		#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("WANTED_SYSTEM_MAINTAIN_MAIN_LOOP")
		#ENDIF
		#ENDIF
		
		PROCESS_ILLEGAL_DLC_VEHICLE_CHECK(IllegalDLCData)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_ILLEGAL_DLC_VEHICLE_CHECK")
		#ENDIF
		#ENDIF 	
		
		// SteveT: New Grief Passive system
		// Allows the player to becomes passive to a specific player if 
		// killed by that player 3 times in 5 minutes without retailiating
		MAINTAIN_GRIEF_PASSIVE(sGriefPassiveData)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_GRIEF_PASSIVE")
		#ENDIF
		#ENDIF
			

	ENDIF
	
	#IF IS_DEBUG_BUILD
		
		
			RENDER_DUPE_DETECTOR_DEBUG_PRINTS()
			
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("RENDER_DUPE_DETECTOR_DEBUG_PRINTS")
			#ENDIF
		
	#ENDIF
	
	// KGM 2/2/13: Check if the game is losing events or skipping script processing for a frame.
	//				Added for BUG 1072677 where a local event seemed to go missing.
		#IF IS_DEBUG_BUILD
		IF (GET_COMMANDLINE_PARAM_EXISTS_DBSAFE("sc_eventframecheck"))
			DEBUG_Send_Check_For_Missing_Events_Or_Skipped_Processing()
		ENDIF
		#ENDIF

	// Look for the server say the game has ended.
	IF GET_SERVER_SCRIPT_GAME_STATE() = MAIN_GAME_STATE_END
		//REMOVE_ALL_NET_DOORS(DOORS_GAMEMODE_FREEMODE)
		UNLOAD_ALL_SPECTATOR_HUD_LEADERBOARD_ELEMENTS()
		SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_END)			
	ENDIF	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Look for the server say the game has ended")
	#ENDIF
	#ENDIF 
	
	MAINTAIN_HANGAR_FUNCTIONALITY()
	
	IF DOES_LOCAL_PLAYER_OWN_A_HANGER()
		PROCESS_HANGAR_EVENTS()
		MAINTAIN_HANGAR_MISSION_PRODUCT_DATA_BROADCASTING_OWNER()
		MAINTAIN_HANGAR_MISSION_PRODUCT_DATA_BROADCASTING_REMOTE()
	ENDIF
	
	#IF IS_DEBUG_BUILD
	MAINTAIN_HANGAR_CONTRABAND_DEBUG_M_MENU(eTransactionState)
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	MAINTAIN_DEBUG_CLEAR_BADSPORT_INPUT()
	#ENDIF

ENDPROC


PROC ForceSpectatorInvisible()
	IF NETWORK_IS_PLAYER_ACTIVE(PLAYER_ID())
	AND (IS_PLAYER_SPECTATING(PLAYER_ID()) AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE))
		IF NOT IS_PLAYER_RESPAWNING(PLAYER_ID())
		AND IS_SCREEN_FADED_OUT()
			SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
		ENDIF
		//NET_PRINT("ForceAllSpectatorsInvisible - SET_PLAYER_INVISIBLE_LOCALLY called on local player ")  NET_NL()
	ENDIF
ENDPROC

PROC MAIN_GAME_STATE_RUNNING_SHOULD_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT()
	PRINTLN("[TS] FREEMODE - SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING) - SHOULD_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT() = TRUE")
	CLEAR_SKIP_WAIT_FOR_TRANSITION_CAMERA()
	IF SHOULD_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT()
	AND NOT TRANSITION_SESSIONS_CLEANUP_AFTER_JIP_BACK_OUT()
		SET_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE()
		SET_CURRENT_FM_MISSION_NEEDS_CLEANUP(FALSE)
	ENDIF
	SET_NEWS_FEED_SPLASH_SCREEN_ACTIVE(FALSE)	
	SET_TRANSITION_SESSION_RETURN_PLAYER_CONTROL_AFTER_CAM_DOWN()
	TOGGLE_RENDERPHASES(TRUE) 
	TOGGLE_RENDERPHASES(TRUE)
	TRIGGER_SCREENBLUR_FADE_OUT(0)
	CLEAR_TRANSITION_SESSION_HIDE_HUD_ELEMENTS_DURING_TRANSITION_BACK_TO_FM()
	CLEAR_TRANSITION_SESSIONS_CLEANUP_AFTER_JIP_BACK_OUT()
	// Want the player to be invincible for 4 seconds on respawn form a job.
	NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(4000)
	SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING)
	THEFEED_AUTO_POST_GAMETIPS_OFF()
	THEFEED_FLUSH_QUEUE()
	g_b_ReapplyStickySaveFailedFeed = FALSE
	PRINTLN("[TS] THEFEED_AUTO_POST_GAMETIPS_OFF() called")
	BUSYSPINNER_OFF()
ENDPROC

PROC FORCE_CONTACTS_ON_PHONE_ON_RESET()
	IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_INITIAL_SETUP)
		IF addedMPInsuranceContact
			ADD_CONTACT_TO_PHONEBOOK(CHAR_MP_MORS_MUTUAL, MULTIPLAYER_BOOK,FALSE) 
			PRINTLN("FORCE_CONTACTS_ON_PHONE_ON_RESET: Forcing mors to contact list after freemode restart")
		ENDIF
	ENDIF
ENDPROC




PROC INITIALISE_CASH_TRANSACTION_INDICES()
	g_sEarnJobshareTransaction[0].index = 0
	g_sEarnJobshareTransaction[1].index = 1
	g_sEarnJobshareTransaction[2].index = 2
	g_sEarnJobshareTransaction[3].index = 3
	g_sEarnJobshareTransaction[4].index = 4
ENDPROC

#IF FEATURE_FREEMODE_PED_ANIM_CREATOR_PROTOTYPE
PROC MANAGE_ACTIVITY_PROTOTYPE_SCRIPTS()
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_runPrototypeActivityPeds")
		IF HAS_SCRIPT_LOADED("activity_Creator_Prototype_Launcher")
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD1, KEYBOARD_MODIFIER_CTRL_SHIFT, "Launch ActivityUI Script") 
			AND NOT NETWORK_IS_SCRIPT_ACTIVE("activity_Creator_Prototype_Launcher")
			AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)	
				
				
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MANAGE_ACTIVITY_SCRIPTS: Requesting script: activity_Creator_Prototype_Launcher")
				REQUEST_SCRIPT("activity_Creator_Prototype_Launcher")
				
				IF HAS_SCRIPT_LOADED("activity_Creator_Prototype_Launcher")
					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MANAGE_ACTIVITY_SCRIPTS: HAS_SCRIPT_LOADED = TRUE for script: activity_Creator_Prototype_Launcher")

					START_NEW_SCRIPT("activity_Creator_Prototype_Launcher", ACTIVITY_CREATOR_INT_STACK_SIZE)
					SET_SCRIPT_AS_NO_LONGER_NEEDED("activity_Creator_Prototype_Launcher")
				ENDIF
				
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MANAGE_ACTIVITY_PROTOTYPE_SCRIPTS: Requesting script: activity_Creator_Prototype_Launcher")
			REQUEST_SCRIPT("activity_Creator_Prototype_Launcher")
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF


PROC MAINTAIN_SERVER_CURRENT_EVENT_GLOBALS()
	INT iActiveEvent = FMMC_TYPE_INVALID
	INT iPlayerCount
	VECTOR vAction
	BOOL bEventStarted
	BOOL bKillListStarted = TRUE
	BOOL bBeenInPlayerLoop = FALSE
	INT iPart
	INT iEvent
	INT iCheckPointBitSet
	PLAYER_INDEX piPlayer
	//Loop all players in the session
	REPEAT NUM_NETWORK_PLAYERS iPart
		//If thet are active on this script and they are a participant
		piPlayer = INT_TO_PLAYERINDEX(iPart)
		IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
			//If they are on an event
			IF GlobalplayerBD_FM_3[iPart].iFmAmbientEventActiveInSession != FMMC_TYPE_INVALID
				iActiveEvent = GlobalplayerBD_FM_3[iPart].iFmAmbientEventActiveInSession
				IF iCheckPointBitSet = 0
				AND GlobalplayerBD_FM_3[iPart].iCheckPointBitSet != 0 
					iCheckPointBitSet = GlobalplayerBD_FM_3[iPart].iCheckPointBitSet
				ENDIF
				//if it's got a pos the update it
				IF NOT IS_VECTOR_ZERO(GlobalplayerBD_FM_3[iPart].vAction)
					vAction += GlobalplayerBD_FM_3[iPart].vAction
					iPlayerCount++
				ENDIF
				//Get the player
				//get the event type
				iEvent = FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(piPlayer)
				//If the event has started the set that it has
				IF iEvent = GlobalplayerBD_FM_3[iPart].iFmAmbientEventActiveInSession
					bBeenInPlayerLoop = TRUE
					//If it's Kill List then they need to not be waiting for it to started fo it to have started
					IF iEvent = FMMC_TYPE_KILL_LIST
						IF FM_EVENT_IS_PLAYER_WAITING_FOR_FM_EVENT_TO_START(piPlayer)
							bKillListStarted = FALSE
						ENDIF
					ELSE
						bEventStarted = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Update the current active event type
	#IF IS_DEBUG_BUILD
	IF iActiveEvent != GlobalServerBD_FM_events.sAActiveEventData.iEvent
		CPRINTLN(DEBUG_NET_AMBIENT, "[FMEG] - Updating event - GlobalServerBD_FM_events.sAActiveEventData.iEvent = ", GET_FREEMODE_ACTIVITY_TYPE_PRINT(iActiveEvent))
	ENDIF
	//Update the current check point bit set
	IF GlobalServerBD_FM_events.sAActiveEventData.iCheckPointBitSet != iCheckPointBitSet 
		CPRINTLN(DEBUG_NET_AMBIENT, "[FMEG] - Updating event - GGlobalServerBD_FM_events.sAActiveEventData.iCheckPointBitSet = ", iCheckPointBitSet)
	ENDIF
	#ENDIF
	
	//Set the current active event
	GlobalServerBD_FM_events.sAActiveEventData.iEvent = iActiveEvent
	
	//Update the current check point bit set
	GlobalServerBD_FM_events.sAActiveEventData.iCheckPointBitSet = iCheckPointBitSet
	
	
	
	//If the event is invalid then clean the gloabl if we need to
	IF GlobalServerBD_FM_events.sAActiveEventData.iEvent = FMMC_TYPE_INVALID
		#IF IS_DEBUG_BUILD
		IF NOT IS_VECTOR_ZERO(GlobalServerBD_FM_events.sAActiveEventData.vAction)
			CPRINTLN(DEBUG_NET_AMBIENT, "[FMEG] - Clearing area vector - GlobalServerBD_FM_events.sAActiveEventData.vAction = ", GlobalServerBD_FM_events.sAActiveEventData.vAction)
		ENDIF
		#ENDIF
		FM_EVENT_SET_ACTIVE_FM_EVENT_STARTED(FALSE)
		GlobalServerBD_FM_events.sAActiveEventData.vAction = <<0.0, 0.0, 0.0>>
		GlobalServerBD_FM_events.sAActiveEventData.iEventVar = FMMC_TYPE_INVALID
	//If we've got an active event then update things
	ELSE
		//Calculate the average
		VECTOR vAverage = <<vAction.x/iPlayerCount, vAction.y/iPlayerCount, vAction.z/iPlayerCount>>
		//Prit if it's moved more than 30
		#IF IS_DEBUG_BUILD
		IF VDIST(GlobalServerBD_FM_events.sAActiveEventData.vAction, vAverage) > 30.0
		OR (IS_VECTOR_ZERO(vAverage) AND NOT IS_VECTOR_ZERO(GlobalServerBD_FM_events.sAActiveEventData.vAction))
			CPRINTLN(DEBUG_NET_AMBIENT, "[FMEG] - Setting area vector - GlobalServerBD_FM_events.sAActiveEventData.vAction = ", vAverage)
		ENDIF
		#ENDIF
		//Set the area
		GlobalServerBD_FM_events.sAActiveEventData.vAction = vAverage
		
		//If the varation has been set
		IF g_AML_serverBD.sHistory[0].iVaration != -1
		//And the type at the top of the history is the type of event we're on
		AND g_AML_serverBD.sHistory[0].event = GET_AML_SCRIPT_TYPE_FROM_FMMC_TYPE(GlobalServerBD_FM_events.sAActiveEventData.iEvent)
			//If we're changing the value then print about it
			#IF IS_DEBUG_BUILD
			IF GlobalServerBD_FM_events.sAActiveEventData.iEventVar != g_AML_serverBD.sHistory[0].iVaration
				CPRINTLN(DEBUG_NET_AMBIENT, "[FMEG] - Setting GlobalServerBD_FM_events.sAActiveEventData.iEventVar = ", g_AML_serverBD.sHistory[0].iVaration)
			ENDIF
			#ENDIF
			//Update the value
			GlobalServerBD_FM_events.sAActiveEventData.iEventVar = g_AML_serverBD.sHistory[0].iVaration
		ENDIF
		
		//Check to see if Kill list has started
		IF GlobalServerBD_FM_events.sAActiveEventData.iEvent = FMMC_TYPE_KILL_LIST
		AND bKillListStarted
		AND bBeenInPlayerLoop
			#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(GlobalServerBD_FM_events.sAActiveEventData.iEventBitSet, ciFM_EVENT_SERVER_EVENT_ACTIVE)
				CPRINTLN(DEBUG_NET_AMBIENT, "[FMEG] - FMMC_TYPE_KILL_LIST - bKillListStarted = TRUE")
			ENDIF
			#ENDIF
			bEventStarted = TRUE
		ENDIF
		FM_EVENT_SET_ACTIVE_FM_EVENT_STARTED(bEventStarted)
		//Set that the event has started
	ENDIF
	
ENDPROC

PROC MAINTAIN_CURRENT_EVENT_GLOBALS()
	
	//If we've not started the timer then do so
	IF NOT HAS_NET_TIMER_STARTED(globalUpdateTimer)
		START_NET_TIMER(globalUpdateTimer)
	//If it's expired then reset
	ELIF HAS_NET_TIMER_EXPIRED(globalUpdateTimer, 1000)
		RESET_NET_TIMER(globalUpdateTimer)
		
		//Local player stuff
		INT iPart = NATIVE_TO_INT(PLAYER_ID())
		//Maintain our current mission type:
		IF GlobalplayerBD_FM_3[iPart].iFmAmbientEventActiveInSession != g_FM_EVENT_VARS.iFmAmbientEventActiveInSession
			GlobalplayerBD_FM_3[iPart].iFmAmbientEventActiveInSession = g_FM_EVENT_VARS.iFmAmbientEventActiveInSession
			CPRINTLN(DEBUG_NET_AMBIENT, "[FMEG] - GlobalplayerBD_FM_3[", iPart, "].iFmAmbientEventActiveInSession = iFmAmbientEventActiveInSession = ", GET_FREEMODE_ACTIVITY_TYPE_PRINT(g_FM_EVENT_VARS.iFmAmbientEventActiveInSession), " iFmAmbientEventActiveInSession = ", g_FM_EVENT_VARS.iFmAmbientEventActiveInSession)
		ENDIF
		
		//If we're not on a mission then clean up the vector
		IF g_FM_EVENT_VARS.iFmAmbientEventActiveInSession = FMMC_TYPE_INVALID
			GlobalplayerBD_FM_3[iPart].vAction = <<0.0, 0.0, 0.0>>
		//If we are on a mission then update the action, but only from time to time. 
		ELIF VDIST(GlobalplayerBD_FM_3[iPart].vAction, g_FM_EVENT_VARS.vAction) > 15.0
			GlobalplayerBD_FM_3[iPart].vAction = g_FM_EVENT_VARS.vAction
			#IF IS_DEBUG_BUILD
			IF GET_GAME_TIMER() % 2500 < 50
				CPRINTLN(DEBUG_NET_AMBIENT, "[FMEG] - GlobalplayerBD_FM_3[", iPart, "].vAction = g_FM_EVENT_VARS.vAction = ", g_FM_EVENT_VARS.vAction)
			ENDIF
			#ENDIF
		ENDIF

		//Server only stuff to get the average area etc
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			MAINTAIN_SERVER_CURRENT_EVENT_GLOBALS()
		ENDIF
	ENDIF
ENDPROC

///Purpose: Every iexec_contraband_type_refresh_time minutes randomly assign contraband types for each warehouse
PROC RUN_TIMER_FOR_CONTRABAND_TYPES()
	IF g_iContrabandRefreshTime = 0
		g_iContrabandRefreshTime = GET_CLOUD_TIME_AS_INT()
	ELIF (GET_CLOUD_TIME_AS_INT() - g_iContrabandRefreshTime) >= g_sMPTunables.iexec_contraband_type_refresh_time
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE_CITEM] RANDOMIZE_WAREHOUSE_CONTRABAND_TYPES called from freemode")
		RANDOMIZE_WAREHOUSE_CONTRABAND_TYPES()
	ENDIF
ENDPROC

//Adding these functions here as we can't use them in net_realty_biker_warehouse.sch
PROC LAUNCH_FACTORY_DEFEND_MISSION(FACTORY_ID eFactory)
	INT iSaveSlot = GET_SAVE_SLOT_FOR_FACTORY(eFactory)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[FACTORY][FA_DEFEND] LAUNCH_DEFEND_MISSION called from freemode. Factory: ", eFactory)
	
	IF iSaveSlot = BUNKER_SAVE_SLOT
	
		GB_BOSS_REQUEST_MISSION_LAUNCH_FROM_SERVER(FMMC_TYPE_GUNRUNNING_DEFEND, ENUM_TO_INT(eFactory))
	
	ELSE
	
		GB_BOSS_REQUEST_ILLICIT_GOODS_MISSION_LAUNCH_FROM_SERVER(FMMC_TYPE_BIKER_CONTRABAND_DEFEND, eFactory)
	
	ENDIF
	
	UPDATE_FACTORY_PRODUCTION_ACTIVE_STATE(iSaveSlot, FALSE)
ENDPROC

FUNC BOOL IS_SERVER_ABLE_TO_LAUNCH_FACTORY_DEFEND_MISSION(FACTORY_ID eFactory)
	
		IF IS_FACTORY_OF_TYPE_WEAPONS(eFactory)
			RETURN GB_IS_BOSS_MISSION_AVAILABLE(PLAYER_ID(), FMMC_TYPE_GUNRUNNING_DEFEND)
		ENDIF
	
	RETURN GB_IS_BOSS_MISSION_AVAILABLE(PLAYER_ID(), FMMC_TYPE_BIKER_CONTRABAND_DEFEND)
ENDFUNC

FUNC BOOL IS_PLAYER_IN_PROPERTY_TRANSITION_STATE()
	//Standard simple interior transition
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	//Walking into an apartment or clubhouse
	IF IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	//Hub -> Club or Club -> Hub transition
	IF (IS_BIT_SET(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_SWITCHING_BETWEEN_TWO_SIMPLE_INTERIORS)
	OR (g_SimpleInteriorData.eSwitchToSecondaryInterior != SIMPLE_INTERIOR_INVALID AND NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())))
		RETURN TRUE
	ENDIF
	
	//Sitting in the entrance menu
	IF g_SimpleInteriorData.eInLocateInteriorID != SIMPLE_INTERIOR_INVALID
		RETURN TRUE
	ENDIF
	
	//Sitting on the vehicle entrance menu
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_PROCESS_VEHICLE_MENU)
	OR IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo, BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_VEH_STORAGE_WARN_FULL)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CHECK_FACTORY_DEFEND_TIMER_FOR_SLOT(INT iSaveSlot)
	FACTORY_ID 		eFactoryID 		= GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[iSaveSlot].eFactoryID
	FACTORY_TYPE 	eFactoryType 	= GET_GOODS_CATEGORY_FROM_FACTORY_ID(eFactoryID)
	INT 			iElapsedTime 	= (GET_CLOUD_TIME_AS_INT() - g_iFactoryDefendMissionTimers[iSaveSlot])
	
	IF eFactoryID != FACTORY_ID_INVALID
	AND g_iFactoryDefendMissionTimers[iSaveSlot] != 0
	AND NOT IS_FACTORY_DEFEND_MISSION_BLOCKED(eFactoryType)
	AND NOT IS_PLAYER_IN_PROPERTY_TRANSITION_STATE()
	AND NOT IS_FACTORY_TOO_CLOSE_TO_LAUNCH_DEFEND(eFactoryID)
	AND GET_FACTORY_PRODUCT_TOTAL_AS_PERCENTAGE(eFactoryID, PLAYER_ID()) >= GET_FACTORY_DEFEND_MISSION_THRESHOLD(eFactoryType)
	AND iElapsedTime >= GET_FACTORY_DEFEND_MISSION_TIME(eFactoryID, eFactoryType)
	
		//Conditions met for launching a mission. Now check if the server can launch a mission for us
		IF NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID(), TRUE)
		AND GB_IS_LOCAL_PLAYER_SAFE_TO_LAUNCH_GANG_BOSS_MISSION() 
		AND IS_SERVER_ABLE_TO_LAUNCH_FACTORY_DEFEND_MISSION(eFactoryID)
			LAUNCH_FACTORY_DEFEND_MISSION(eFactoryID)
			START_FACTORY_DEFEND_MISSION_TIMER(iSaveSlot)
			SAVE_FACTORY_DEFEND_TIMER_STATS()
			EXIT
		ELSE
			CDEBUG3LN(DEBUG_SAFEHOUSE, "[FACTORY][FA_DEFEND]RUN_FACTORY_DEFEND_MISSION_TIMERS called from freemode. Timer: ", iSaveSlot, " expired. Can't launch defend mission")
		ENDIF
		
	ELIF GET_FACTORY_PRODUCT_TOTAL_AS_PERCENTAGE(eFactoryID, PLAYER_ID()) < GET_FACTORY_DEFEND_MISSION_THRESHOLD(eFactoryType)
	OR IS_FACTORY_TOO_CLOSE_TO_LAUNCH_DEFEND(eFactoryID)
	
		g_iFactoryDefendMissionTimers[iSaveSlot] = 0
		
	ELIF g_iFactoryDefendMissionTimers[iSaveSlot] = 0
	AND NOT IS_FACTORY_PRODUCTION_HALTED(PLAYER_ID(), iSaveSlot)
	
		INITIALISE_DEFEND_MISSION_TIMER(iSaveSlot)
		CDEBUG1LN(DEBUG_AMBIENT, "[FACTORY][FA_DEFEND]RUN_FACTORY_DEFEND_MISSION_TIMERS Timer ", iSaveSlot, "  at 0 starting")
		
	ENDIF
ENDPROC

PROC RUN_BUNKER_DEFEND_MISSION_TIMERS()
	
	
	IF NOT bBunkerDefendMissionTimersActive
		IF IS_PLAYER_ABLE_TO_LAUNCH_FACTROY_DEFEND_AS_BOSS(PLAYER_ID(), TRUE)
			INITIALISE_DEFEND_MISSION_TIMER(BUNKER_SAVE_SLOT)
			bBunkerDefendMissionTimersActive = TRUE			
			CDEBUG1LN(DEBUG_AMBIENT, "[FACTORY][FA_DEFEND]RUN_BUNKER_DEFEND_MISSION_TIMERS: Timer initialised")
		ENDIF
		
	ELIF IS_PLAYER_ABLE_TO_LAUNCH_FACTROY_DEFEND_AS_BOSS(PLAYER_ID(), TRUE)
		CHECK_FACTORY_DEFEND_TIMER_FOR_SLOT(BUNKER_SAVE_SLOT)
		UPDATE_FACTORY_DEFEND_TIMER_STATS()
	ELSE
		#IF IS_DEBUG_BUILD
			IF NOT IS_PLAYER_ABLE_TO_LAUNCH_FACTROY_DEFEND_AS_BOSS(PLAYER_ID(), TRUE)
				CDEBUG1LN(DEBUG_AMBIENT, "[FACTORY][FA_DEFEND]RUN_FACTORY_DEFEND_MISSION_TIMERS IS_PLAYER_ABLE_TO_LAUNCH_FACTROY_DEFEND_AS_BOSS = FALSE. Timer stats saved")
			ELIF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty != -1
				CDEBUG1LN(DEBUG_AMBIENT, "[FACTORY][FA_DEFEND]RUN_FACTORY_DEFEND_MISSION_TIMERS in a property. Timer stats saved")
			ELSE
				CDEBUG1LN(DEBUG_AMBIENT, "[FACTORY][FA_DEFEND]RUN_FACTORY_DEFEND_MISSION_TIMERS No longer a boss. Timer stats saved")
			ENDIF
		#ENDIF
		SAVE_BUNKER_DEFEND_TIMER_STATS()
		bBunkerDefendMissionTimersActive = FALSE
	ENDIF
ENDPROC

PROC MAINTAIN_ARMORY_TRUCK_EXIT_TO_CAB_IN_FREEMODE()
	IF g_SimpleInteriorData.bTriggerArmoryTruckExitToCabFreemode
		
		IF (DOES_ENTITY_EXIST(PLAYER_PED_ID()) AND IS_ENTITY_ALIVE(PLAYER_PED_ID()))
			IF (DOES_ENTITY_EXIST(MPGlobalsAmbience.vehTruckVehicle[0]) AND IS_ENTITY_ALIVE(MPGlobalsAmbience.vehTruckVehicle[0]))
				
				IF IS_VEHICLE_ATTACHED_TO_TRAILER(MPGlobalsAmbience.vehTruckVehicle[0])
					
					IF IS_VEHICLE_SEAT_FREE(MPGlobalsAmbience.vehTruckVehicle[0], VS_ANY_PASSENGER)
						TASK_ENTER_VEHICLE(PLAYER_PED_ID(), MPGlobalsAmbience.vehTruckVehicle[0], DEFAULT, VS_ANY_PASSENGER, DEFAULT, ECF_WARP_PED)
						CDEBUG1LN(DEBUG_PROPERTY, "[FREEMODE] - MAINTAIN_ARMORY_TRUCK_EXIT_TO_CAB_IN_FREEMODE - Tasking player ped to enter armory truck cab, any passenger seat.")

					ELSE
						CDEBUG1LN(DEBUG_PROPERTY, "[FREEMODE] - MAINTAIN_ARMORY_TRUCK_EXIT_TO_CAB_IN_FREEMODE - No seats available in cab, aborting exit to cab.")
						g_SimpleInteriorData.bTriggerArmoryTruckExitToCabFreemode = FALSE
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_PROPERTY, "[FREEMODE] - MAINTAIN_ARMORY_TRUCK_EXIT_TO_CAB_IN_FREEMODE - Armoury truck cab is not attached to its trailer.")
					g_SimpleInteriorData.bTriggerArmoryTruckExitToCabFreemode = FALSE
				ENDIF
			
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), MPGlobalsAmbience.vehTruckVehicle[0])
					CDEBUG1LN(DEBUG_PROPERTY, "[FREEMODE] - MAINTAIN_ARMORY_TRUCK_EXIT_TO_CAB_IN_FREEMODE - g_SimpleInteriorData.bTriggerExitFromArmoryTruckToCab = FALSE")
					g_SimpleInteriorData.bTriggerArmoryTruckExitToCabFreemode = FALSE
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_PROPERTY, "[FREEMODE] - MAINTAIN_ARMORY_TRUCK_EXIT_TO_CAB_IN_FREEMODE - Armoury Truck does not exist or is not alive. Cannot warp player into armoury truck cab.")
				g_SimpleInteriorData.bTriggerArmoryTruckExitToCabFreemode = FALSE
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_PROPERTY, "[FREEMODE] - MAINTAIN_ARMORY_TRUCK_EXIT_TO_CAB_IN_FREEMODE - Player ped does not exist or is not alive. Cannot warp player into armoury truck cab.")
			g_SimpleInteriorData.bTriggerArmoryTruckExitToCabFreemode = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC RUN_FACTORY_DEFEND_MISSION_TIMERS()
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_BikerFlowPlayVariations")
	OR GET_COMMANDLINE_PARAM_EXISTS("sc_BlckFactoryDefend")
		EXIT
	ENDIF
	#ENDIF
	
	IF g_bBlockFactoryDefendMissionTimers
		EXIT
	ENDIF
	
	RUN_BUNKER_DEFEND_MISSION_TIMERS()
	
	INT i
	
	IF NOT bDefendMissionTimersActive
		IF IS_PLAYER_ABLE_TO_LAUNCH_FACTROY_DEFEND_AS_BOSS(PLAYER_ID())
			REPEAT MAX_OWNED_BIKER_FACTORIES i
				INITIALISE_DEFEND_MISSION_TIMER(i)
			ENDREPEAT
			
			CDEBUG1LN(DEBUG_AMBIENT, "[FACTORY][FA_DEFEND]RUN_FACTORY_DEFEND_MISSION_TIMERS: Timers initialised")
			bDefendMissionTimersActive = TRUE
		ENDIF
		
	ELIF IS_PLAYER_ABLE_TO_LAUNCH_FACTROY_DEFEND_AS_BOSS(PLAYER_ID())
		REPEAT MAX_OWNED_BIKER_FACTORIES i
			CHECK_FACTORY_DEFEND_TIMER_FOR_SLOT(i)
		ENDREPEAT
		
		UPDATE_FACTORY_DEFEND_TIMER_STATS()
		
	ELIF bDefendMissionTimersActive
		#IF IS_DEBUG_BUILD
			IF NOT IS_PLAYER_ABLE_TO_LAUNCH_FACTROY_DEFEND_AS_BOSS(PLAYER_ID())
				CDEBUG1LN(DEBUG_AMBIENT, "[FACTORY][FA_DEFEND]RUN_FACTORY_DEFEND_MISSION_TIMERS IS_PLAYER_ABLE_TO_LAUNCH_FACTROY_DEFEND_AS_BOSS = FALSE. Timer stats saved")
			ELIF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty != -1
				CDEBUG1LN(DEBUG_AMBIENT, "[FACTORY][FA_DEFEND]RUN_FACTORY_DEFEND_MISSION_TIMERS in a property. Timer stats saved")
			ELSE
				CDEBUG1LN(DEBUG_AMBIENT, "[FACTORY][FA_DEFEND]RUN_FACTORY_DEFEND_MISSION_TIMERS No longer a boss. Timer stats saved")
			ENDIF
		#ENDIF
		
		SAVE_FACTORY_DEFEND_TIMER_STATS()
		bDefendMissionTimersActive = FALSE
	ENDIF
		
ENDPROC


FUNC TATTOO_NAME_ENUM GET_CHILIAD_WAKE_UP_TATTOO()
	IF IS_PLAYER_MALE(PLAYER_ID())
		RETURN GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_Christmas2018_Tat_000_M"), TATTOO_MP_FM)
	ELSE
		RETURN GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_Christmas2018_Tat_000_F"), TATTOO_MP_FM_F)
	ENDIF
	RETURN INVALID_TATTOO
ENDFUNC



PROC MAINTAIN_WEAPONIZED_VEHICLE_PASSIVE_MODE()
	IF IS_MP_PASSIVE_MODE_ENABLED()
		IF IS_PLAYER_USING_A_VEHICLE_WITH_PASSIVE_MODE_RESTRICTIONS(PLAYER_ID())
			PRINTLN("[PASSIVE_MODE] MAINTAIN_WEAPONIZED_VEHICLE_PASSIVE_MODE - Player was in passive mode and inside a vehicle with passive mode restrictions. Disabling passive mode.")
			DISABLE_MP_PASSIVE_MODE()
		ENDIF
	ENDIF
ENDPROC

INT iPegasusForceCleanStagger = 0
INT iPegasusForceCleanPass = 0

PROC MONITOR_CURRENT_PEGASUS_FORCED_CLEANUP()
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF IS_BIT_SET(GlobalServerBD.iPegasusCleanupBS, i)
			IF NOT IS_BIT_SET(GlobalplayerBD[i].iPegasusBS, PEGASUS_BS_I_HAVE_ACTIVE_VEH)
				PRINTLN("[FORCE_PEGASUS] MONITOR_CURRENT_PEGASUS_FORCED_CLEANUP - Player is finished cleaning up Pegasus - ", i)
				
				CLEAR_BIT(GlobalServerBD.iPegasusCleanupBS, i)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC ATTEMPT_PEGASUS_FORCED_CLEANUP()
	INT i
	
	SWITCH iPegasusForceCleanPass
		CASE 0 // Pegasus not delivered yet, player has no MOC
			REPEAT NUM_NETWORK_PLAYERS i
				PLAYER_INDEX playerID
				playerID = INT_TO_PLAYERINDEX(i)
				
				IF IS_NET_PLAYER_OK(playerID)
					IF IS_BIT_SET(GlobalplayerBD[i].iPegasusBS, PEGASUS_BS_I_HAVE_ACTIVE_VEH)
					AND NOT IS_BIT_SET(GlobalplayerBD_FM[i].pegasusConceal.iBS, CONCEAL_PEGASUS_VEHICLE_BS_CONCEAL_CURRENT)
					AND IS_BIT_SET(GlobalplayerBD[i].iPegasusBS, PEGASUS_BS_VEHICLE_IS_BEING_DELIVERED)
					AND GlobalServerBD.iPegasusCleanupBS = 0
						PRINTLN("[FORCE_PEGASUS] ATTEMPT_PEGASUS_FORCED_CLEANUP - Forcing player to cleanup Pegasus - ", i)
						SET_BIT(GlobalServerBD.iPegasusCleanupBS, i)
						iPegasusForceCleanPass = 0
						EXIT
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
		
		CASE 1 // Pegasus is empty, player has no MOC
			REPEAT NUM_NETWORK_PLAYERS i
				PLAYER_INDEX playerID
				playerID = INT_TO_PLAYERINDEX(i)
				
				IF IS_NET_PLAYER_OK(playerID)
					IF IS_BIT_SET(GlobalplayerBD[i].iPegasusBS, PEGASUS_BS_I_HAVE_ACTIVE_VEH)
					AND NOT IS_BIT_SET(GlobalplayerBD_FM[i].pegasusConceal.iBS, CONCEAL_PEGASUS_VEHICLE_BS_CONCEAL_CURRENT)
					AND IS_BIT_SET(GlobalplayerBD[i].iPegasusBS, PEGASUS_BS_VEHICLE_IS_EMPTY)
					AND GlobalServerBD.iPegasusCleanupBS = 0
						PRINTLN("[FORCE_PEGASUS] ATTEMPT_PEGASUS_FORCED_CLEANUP - Forcing player to cleanup Pegasus - ", i)
						SET_BIT(GlobalServerBD.iPegasusCleanupBS, i)
						iPegasusForceCleanPass = 0
						EXIT
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
		
		CASE 2 // Pegasus not delivered yet or pegasus is empty
			REPEAT NUM_NETWORK_PLAYERS i
				PLAYER_INDEX playerID
				playerID = INT_TO_PLAYERINDEX(i)
				
				IF IS_NET_PLAYER_OK(playerID)
					IF IS_BIT_SET(GlobalplayerBD[i].iPegasusBS, PEGASUS_BS_I_HAVE_ACTIVE_VEH)
					AND NOT IS_BIT_SET(GlobalplayerBD_FM[i].pegasusConceal.iBS, CONCEAL_PEGASUS_VEHICLE_BS_CONCEAL_CURRENT)
					AND (IS_BIT_SET(GlobalplayerBD[i].iPegasusBS, PEGASUS_BS_VEHICLE_IS_BEING_DELIVERED)
					OR IS_BIT_SET(GlobalplayerBD[i].iPegasusBS, PEGASUS_BS_VEHICLE_IS_EMPTY))
					AND GlobalServerBD.iPegasusCleanupBS = 0
						PRINTLN("[FORCE_PEGASUS] ATTEMPT_PEGASUS_FORCED_CLEANUP - Forcing player to cleanup Pegasus - ", i)
						SET_BIT(GlobalServerBD.iPegasusCleanupBS, i)
						iPegasusForceCleanPass = 0
						EXIT
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
		
		CASE 3 // Next available pegasus that is not concealed
			REPEAT NUM_NETWORK_PLAYERS i
				PLAYER_INDEX playerID
				playerID = INT_TO_PLAYERINDEX(i)
				
				IF IS_NET_PLAYER_OK(playerID)
					IF IS_BIT_SET(GlobalplayerBD[i].iPegasusBS, PEGASUS_BS_I_HAVE_ACTIVE_VEH)
					AND NOT IS_BIT_SET(GlobalplayerBD_FM[i].pegasusConceal.iBS, CONCEAL_PEGASUS_VEHICLE_BS_CONCEAL_CURRENT)
					AND GlobalServerBD.iPegasusCleanupBS = 0
						PRINTLN("[FORCE_PEGASUS] ATTEMPT_PEGASUS_FORCED_CLEANUP - Forcing player to cleanup Pegasus - ", i)
						SET_BIT(GlobalServerBD.iPegasusCleanupBS, i)
						iPegasusForceCleanPass = 0
						EXIT
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
	ENDSWITCH
	
	iPegasusForceCleanPass++
	
	IF NOT g_bAggressivePegasusCleanup
		IF iPegasusForceCleanPass = 3
			iPegasusForceCleanPass = 0
		ENDIF
	ENDIF
	
	IF iPegasusForceCleanPass >= 4
		iPegasusForceCleanPass = 0
	ENDIF
ENDPROC

PROC MAINTAIN_PEGASUS_FORCED_CLEANUP()
	IF GlobalServerBD.iPegasusCleanupBS != 0
		MONITOR_CURRENT_PEGASUS_FORCED_CLEANUP()
	ENDIF
	
	IF iPegasusForceCleanStagger = 0
	AND GlobalServerBD.iPegasusCleanupBS = 0
		ATTEMPT_PEGASUS_FORCED_CLEANUP()
	ENDIF
	
	iPegasusForceCleanStagger++
	
	IF (iPegasusForceCleanStagger >= 60)
		iPegasusForceCleanStagger = 0
	ENDIF
ENDPROC


PROC MAINTAIN_EXCLUSION_ZONES()
	
	// By default enable exclusion zones prior to disabling, incase players leave a gang or remove their hangar.
	IF NOT (DOES_ENTITY_EXIST(PLAYER_PED_ID()) AND (IS_ENTITY_ALIVE(PLAYER_PED_ID()) = FALSE))
		GlobalExclusionArea[1].ExclusionArea.bIsActive = TRUE
		GlobalExclusionArea[2].ExclusionArea.bIsActive = TRUE
		GlobalExclusionArea[3].ExclusionArea.bIsActive = TRUE
		GlobalExclusionArea[4].ExclusionArea.bIsActive = TRUE
	ELSE
		PRINTLN("FREEMODE - MAINTAIN_EXCLUSION_ZONES - player is respawning, dont reset exclusion areas.")
	ENDIF

	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
		PLAYER_INDEX playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		
		IF playerBoss != INVALID_PLAYER_INDEX()
			IF DOES_PLAYER_OWN_A_HANGER(playerBoss)
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID()) AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
					IF IS_PLAYER_IN_FORT_ZANCUDO(PLAYER_PED_ID()) AND IS_PLAYERS_HANGAR_IN_FORT_ZANCUDO(playerBoss)
						// Disable exclusion zones in and around Fort Zancudo.
						GlobalExclusionArea[1].ExclusionArea.bIsActive = FALSE
						GlobalExclusionArea[2].ExclusionArea.bIsActive = FALSE
						
						PRINTLN("FREEMODE - MAINTAIN_EXCLUSION_ZONES - Disabling exclusion zones in and around Fort Zancudo.")
					ENDIF
					
					IF IS_PLAYER_IN_LSIA(PLAYER_PED_ID()) AND IS_PLAYERS_HANGAR_IN_LSIA(playerBoss)
						// Disable exclusion zones in and around LSIA.
						GlobalExclusionArea[3].ExclusionArea.bIsActive = FALSE
						GlobalExclusionArea[4].ExclusionArea.bIsActive = FALSE
						
						PRINTLN("FREEMODE - MAINTAIN_EXCLUSION_ZONES - Disabling exclusion zones in and around LSIA.")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF DOES_PLAYER_OWN_A_HANGER(PLAYER_ID())
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID()) AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
				IF IS_PLAYER_IN_FORT_ZANCUDO(PLAYER_PED_ID()) AND IS_PLAYERS_HANGAR_IN_FORT_ZANCUDO(PLAYER_ID())
					// Disable exclusion zones in and around Fort Zancudo.
					GlobalExclusionArea[1].ExclusionArea.bIsActive = FALSE
					GlobalExclusionArea[2].ExclusionArea.bIsActive = FALSE
						
					PRINTLN("FREEMODE - MAINTAIN_EXCLUSION_ZONES - Disabling exclusion zones in and around Fort Zancudo.")
				ENDIF
				
				IF IS_PLAYER_IN_LSIA(PLAYER_PED_ID()) AND IS_PLAYERS_HANGAR_IN_LSIA(PLAYER_ID())
					// Disable exclusion zones in and around LSIA.
					GlobalExclusionArea[3].ExclusionArea.bIsActive = FALSE
					GlobalExclusionArea[4].ExclusionArea.bIsActive = FALSE
					
					PRINTLN("FREEMODE - MAINTAIN_EXCLUSION_ZONES - Disabling exclusion zones in and around LSIA.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

BOOL bKilledSPCheats = FALSE

PROC MAINTAIN_KILLING_SP_CHEATS()
	IF NOT bKilledSPCheats
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, TRUE)
		AND NOT g_b_TransitionActive
		AND g_Private_Gamemode_Current = GAMEMODE_FM
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cheat_controller")) > 0
				TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("cheat_controller")
			ENDIF
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_MP_ABILITY_UNLOCKED(PLAYERABILITIES_FM_RUNFASTER4)
					SET_RUN_SPRINT_MULTIPLIER_FOR_PLAYER(PLAYER_ID(), 1.15)
				ELIF IS_MP_ABILITY_UNLOCKED(PLAYERABILITIES_FM_RUNFASTER3)
					SET_RUN_SPRINT_MULTIPLIER_FOR_PLAYER(PLAYER_ID(), 1.1)
				ELIF IS_MP_ABILITY_UNLOCKED(PLAYERABILITIES_FM_RUNFASTER2)
					SET_RUN_SPRINT_MULTIPLIER_FOR_PLAYER(PLAYER_ID(), 1.075)
				ELIF IS_MP_ABILITY_UNLOCKED(PLAYERABILITIES_FM_RUNFASTER1)
					SET_RUN_SPRINT_MULTIPLIER_FOR_PLAYER(PLAYER_ID(), 1.05)
				ELSE
					SET_RUN_SPRINT_MULTIPLIER_FOR_PLAYER(PLAYER_ID(), 1.0)
				ENDIF
				
				IF IS_MP_ABILITY_UNLOCKED(PLAYERABILITIES_FM_SWIMSPEED4)
					SET_SWIM_MULTIPLIER_FOR_PLAYER(PLAYER_ID(), 1.1)
				ELIF IS_MP_ABILITY_UNLOCKED(PLAYERABILITIES_FM_SWIMSPEED3)
					SET_SWIM_MULTIPLIER_FOR_PLAYER(PLAYER_ID(), 1.075)
				ELIF IS_MP_ABILITY_UNLOCKED(PLAYERABILITIES_FM_SWIMSPEED2)
					SET_SWIM_MULTIPLIER_FOR_PLAYER(PLAYER_ID(), 1.05)
				ELIF IS_MP_ABILITY_UNLOCKED(PLAYERABILITIES_FM_SWIMSPEED1)
					SET_SWIM_MULTIPLIER_FOR_PLAYER(PLAYER_ID(), 1.025)
				ELSE
					SET_SWIM_MULTIPLIER_FOR_PLAYER(PLAYER_ID(), 1.0)
				ENDIF
			ENDIF
			
			PRINTLN("MAINTAIN_KILLING_SP_CHEATS - Killed any remaining SP cheats")
			
			bKilledSPCheats = TRUE
		ENDIF
	ENDIF
ENDPROC


// -----------------------------------	MAIN LOOP ----------------------------------------------------------------------
SCRIPT
	PRINTLN("TRANSITION TIME - FREEMODE - INT                                - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")				
	#IF IS_DEBUG_BUILD
		NET_PRINT("\n\n >>>> freemode: Script Launched..... \n\n  ")
	#ENDIF
	//Request the launch script
	REQUEST_SCRIPT("FMMC_Launcher")
	REQUEST_SCRIPT(FREEMODE_INIT_SCRIPT())
	MAINTAIN_FM_net_tunable_check_LAUNCHING()
	//Remove any Sp scripts that might have been loaded.
	SET_ALL_SP_SCRIPT_AS_NO_LONGER_NEEDED_WHEN_ENTERING_FM()
	
	SC_LEADERBOARD_CACHE_CLEAR_ALL()
	freemodeDataLB.bFilledCrewSlots = FALSE
	RESET_PROPERTY_MAINTAIN_GLOBALS()
	LAUNCH_CRIMINAL_STARTER_PACK_BROWSER(FALSE)
	LAUNCH_CRIMINAL_STARTER_PACK_MENU(FALSE)
	
	g_bFinishedFreemodeClearGlobals = FALSE
	
	PRINTLN("TRANSITION TIME - FREEMODE - WaitForFreemodeClearGlobalsScriptToRun   ST- FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")		
	WaitForFreemodeClearGlobalsScriptToRun()
	PRINTLN("TRANSITION TIME - FREEMODE - WaitForFreemodeClearGlobalsScriptToRun   ED- FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")		
		
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
	PRINTLN("TRANSITION TIME - FREEMODE - SET_THIS_SCRIPT_IS_NETWORK_SCRIPT  - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	

	g_bLauncherWillControlMiniMap = FALSE
	Initialise_Main_Generic_Server()
	PRINTLN("TRANSITION TIME - FREEMODE - Initialise_Main_Generic_Server     - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) <= 0
		g_ownerOfPropertyIAmIn = INVALID_PLAYER_INDEX()
		PRINTLN("FREEMODE: Init g_ownerOfPropertyIAmIn to invalid on launch")
	ENDIF
	
	INIT_GUEST_PARKING_DATA()

	#IF IS_DEBUG_BUILD
		KEEP_ANY_SCRIPT_METRIC_RECORDING()
	#ENDIF
	//Clear that the launcher is ready
	g_bFinishedFreemodeInitFmmcLauncherCanMove = FALSE
	//launch the launcher
	IF IS_TRANSITION_SESSION_LAUNCHING()
		//Start the launcher script
		IF HAS_SCRIPT_LOADED("FMMC_Launcher")
		    START_NEW_SCRIPT("FMMC_Launcher", FMMC_LAUNCHER_STACK_SIZE)
		    SET_SCRIPT_AS_NO_LONGER_NEEDED("FMMC_Launcher")
			PRINTLN("TRANSITION TIME - FREEMODE - START_NEW_SCRIPT - FMMC_Launcher  Launched")
		ENDIF
	ENDIF
	
	PRINTLN("TRANSITION TIME - FREEMODE - START_NEW_SCRIPT - FMMC_Launcher   - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	
	
	// Carry out all the initial game starting duties. 
	PROCESS_PRE_GAME()	
	PRINTLN("TRANSITION TIME - FREEMODE - PROCESS_PRE_GAME                   - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	
	
	IF (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		GlobalServerBD.g_allowDynamicAllocation = FALSE	// 'OFF' by default
	ENDIF
	
	// Keith 28/6/13: Ensure Freemode is allowed to delete blips setup by other scripts
	// NOTE: This used to be called within MissionsAtCoords initialisation, but initialisation of that is now handled by freemode_init.sc so freemode.sc needs to call this explicitly
	SET_THIS_SCRIPT_CAN_REMOVE_BLIPS_CREATED_BY_ANY_SCRIPT(TRUE)

	
	mpPropExtTest.vBuildingCentre[0] = <<-791.29407, 338.07104, 200.41348>>
	mpPropExtTest.vLoc[0] = <<-807.7896, 346.5414, 230.2951>>
	mpPropExtTest.fRadius[0] = 0.25
	mpPropExtTest.vBuildingCentre[1] = <<-794.769714,327.526917,111.454330>> 
	mpPropExtTest.vLoc[1] = <<-807.7896, 346.5414, 230.2951>>
	mpPropExtTest.fRadius[1] = 0.25
	mpPropExtTest.vBuildingCentre[2] = <<-1461.842896,-541.008850,57.506992>> 
	mpPropExtTest.vLoc[2] = <<-1465.2595, -542.628, 122.700>>
	mpPropExtTest.fRadius[2] = 0.25
	mpPropExtTest.vBuildingCentre[3] = <<-463.865082,-692.728455,74.686890>>
	mpPropExtTest.vLoc[3] = <<-463.865,-692.729,93.987>>
	mpPropExtTest.fRadius[3] = 0.2
	mpPropExtTest.bActive = FALSE
	mpPropExtTest.bDrawDebugSphere = FALSE
	

	// set up proximity chat
	IF HAS_TRANSITION_SESSION_SELECTED_RESTART_RANDOM()
	OR IS_TRANSITION_SESSION_IN_CORONA_AFTER_RANDOM_RESTART()
	OR IS_TRANSITION_SESSION_LAUNCHING()
		PRINTLN("[CORONA]                  - CLIENT_SET_UP_EVERYONE_CHAT() - coming from VOTE SCREEN, FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	
		CLIENT_SET_UP_EVERYONE_CHAT()
	ELSE
		PRINTLN("[CORONA]                  - RESET_CHAT_PROXIMITY() - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	
		RESET_CHAT_PROXIMITY()
	ENDIF
	
	
	// initialising stat timers
	INIT_STAT_TIMERS()
	PRINTLN("TRANSITION TIME - FREEMODE - INIT_STAT_TIMERS                   - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	
	
	SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN, -1, TRUE)
	PRINTLN("TRANSITION TIME - FREEMODE - SET_GAME_STATE_ON_DEATH            - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	
		
	//Rank up Bar
	INITIALISE_RANKUP(AllRankupStuff)
	PRINTLN("TRANSITION TIME - FREEMODE - INITIALISE_RANKUP                  - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	

	REFRESH_CRC_REPORT_PLAYERS(PlayerCheckCRCStagesDone)

	//Set the unlocks bool
	SET_FM_UNLOCKS_BIT_SET(FALSE)
	PRINTLN("TRANSITION TIME - FREEMODE - SET_FM_UNLOCKS_BIT_SET             - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	
	
	//Turn off/On StuntJumps
	SETUP_FM_USJS()
	PRINTLN("TRANSITION TIME - FREEMODE - SETUP_FM_USJS                      - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	
	
	//Check if player is still in crews
	HAS_PLAYER_LEFT_CREW_EVENT()
	PRINTLN("TRANSITION TIME - FREEMODE - HAS_PLAYER_LEFT_CREW               - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	
	
	//Initialise the In Car with Buddies XP
	Initialise_Additional_XP()
	PRINTLN("TRANSITION TIME - FREEMODE - Initialise_Additional_XP           - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	
		
	RESET_SHOP_CONTROLLER_SCRIPT()
	PRINTLN("TRANSITION TIME - FREEMODE - RESET_SHOP_CONTROLLER_SCRIPT       - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	
	
	// Turn off ambient cop dispatch (turned back on in script cleanup)
	SET_AUDIO_FLAG("OnlyAllowScriptTriggerPoliceScanner", TRUE)
	PRINTLN("TRANSITION TIME - FREEMODE - SET_AUDIO_FLAG                     - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	
	
	CLEANUP_MP_SAVED_VEHICLE()
	SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_IMPOUND_HELP) // Fix for 1623686 - If you come back into session and your vehicle is impounded, can you make sure and tell the player 
	PRINTLN("TRANSITION TIME - FREEMODE - CLEANUP_MP_SAVED_VEHICLE           - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	


	INITIALISE_MP_PROPERTY_BLIPS()
	PRINTLN("TRANSITION TIME - FREEMODE - INITIALISE_MP_PROPERTY_BLIPS       - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	
	SET_GPS_FLASHES(FALSE)
	
	
	//Check if player is allowed to Pickup Inventory Items
	CAN_PICKUP_ITEM_CHECK(PROP_CHOC_PQ)
	CAN_PICKUP_ITEM_CHECK(PROP_CHOC_EGO)
	CAN_PICKUP_ITEM_CHECK(PROP_CHOC_METO)
	CAN_PICKUP_ITEM_CHECK(PROP_ECOLA_CAN)
	CAN_PICKUP_ITEM_CHECK(PROP_AMB_BEER_BOTTLE)
	CAN_PICKUP_ITEM_CHECK(PROP_CS_CIGGY_01)

	
	//these need to be turned off here as leaving them in the building controller leads to interior spawn and building controller init fighting
	//i.e. interior script turns it on and building controller turns it off.

	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_HIGH_APT_1
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_1,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_HIGH_APT_2
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_2,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_HIGH_APT_3
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_3,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_HIGH_APT_4
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_4,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_HIGH_APT_5
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_5,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_HIGH_APT_6
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_6,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_HIGH_APT_7
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_7,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_HIGH_APT_8
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_8,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_HIGH_APT_9
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_9,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_HIGH_APT_10
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_10,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_HIGH_APT_11
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_11,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_HIGH_APT_12
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_12,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_HIGH_APT_13
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_13,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_HIGH_APT_14
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_14,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_HIGH_APT_15
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_15,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_HIGH_APT_16
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_16,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_HIGH_APT_17
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_17,	TRUE)
	ENDIF

	SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_18,	TRUE)
	SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_19,	TRUE)
	SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_20,	TRUE)
	IF GET_PROPERTY_SIZE_TYPE(iGlobalCurrentlyInsidePropertyID) != PROP_SIZE_TYPE_MED_APT
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APART_MIDSPAZ,	TRUE)  //UNCOMMENT ON MERGE
	ENDIF
	
	IF NOT ( GET_PROPERTY_SIZE_TYPE(iGlobalCurrentlyInsidePropertyID) = PROP_SIZE_TYPE_LARGE_APT
	OR GET_PROPERTY_SIZE_TYPE(iGlobalCurrentlyInsidePropertyID) = PROP_SIZE_TYPE_10_GAR)
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_GARAGES,	TRUE)
	ENDIF
	IF NOT ( GET_PROPERTY_SIZE_TYPE(iGlobalCurrentlyInsidePropertyID) = PROP_SIZE_TYPE_MED_APT
	OR GET_PROPERTY_SIZE_TYPE(iGlobalCurrentlyInsidePropertyID) = PROP_SIZE_TYPE_6_GAR )
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_GARAGEM,			TRUE)
	ENDIF
	IF NOT ( GET_PROPERTY_SIZE_TYPE(iGlobalCurrentlyInsidePropertyID) = PROP_SIZE_TYPE_SMALL_APT
	OR GET_PROPERTY_SIZE_TYPE(iGlobalCurrentlyInsidePropertyID) = PROP_SIZE_TYPE_2_GAR)
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_GARAGEL,			TRUE)
	ENDIF
	
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_STILT_APT_1_BASE_B
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_STILT_1_BASE_B,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_STILT_APT_2_B
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_STILT_2_B,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_STILT_APT_3_B
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_STILT_3_B,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_STILT_APT_4_B
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_STILT_4_B,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_STILT_APT_5_BASE_A
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_STILT_5_BASE_A,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_STILT_APT_7_A
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_STILT_7_A,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_STILT_APT_8_A
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_STILT_8_A,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_STILT_APT_10_A
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_STILT_10_A,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_STILT_APT_12_A
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_STILT_12_A,	TRUE)
	ENDIF
	IF iGlobalCurrentlyInsidePropertyID != PROPERTY_STILT_APT_13_A
		SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_STILT_13_A,	TRUE)
	ENDIF


	#IF IS_DEBUG_BUILD
		g_MainMissionScriptID = GET_ID_OF_THIS_THREAD()
		SET_CURRENT_GAMEMODE(GAMEMODE_FM)
		NET_PRINT("\n\n >>>> freemode: Started..... \n\n  ")
	#ENDIF
		
	// Setup widgets						 
	#IF IS_DEBUG_BUILD
		g_bMainMissionScriptActive = TRUE
		CREATE_TEST_SCRIPT_WIDGETS()
	#ENDIF
	PRINTLN("TRANSITION TIME - FREEMODE - CREATE_TEST_SCRIPT_WIDGETS         - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	
	

	#IF IS_DEBUG_BUILD
		REQUEST_ADDITIONAL_TEXT("MPAWD", MP_STATS_TEXT_SLOT)
	#ENDIF
	MPGlobalsAmbience.playerBounty.playerWhoAddedBountyForYou = INVALID_PLAYER_INDEX()
	PRINTLN("GET_FRAME_COUNT MPGlobalsAmbience = ", GET_FRAME_COUNT())

	HIDE_HUD_AND_RADAR_THIS_FRAME()
	
	//Kill the script that processes transition session events
	IF SHOULD_PROCESS_TRANSITION_SESSIONS_NET_EVENTS()
		PRINTLN("[TS] Freemode.sc calling - CLEAR_TRANSITION_SESSIONS_PROCESS_NET_EVENTS")
		CLEAR_TRANSITION_SESSIONS_PROCESS_NET_EVENTS()
	ENDIF
	
	REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("AM_MP_PROPERTY_EXT")
	REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("AM_MP_GARAGE_CONTROL")
	REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("AM_MP_CARWASH_LAUNCH")
	REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("ACT_CINEMA")

	//Set up for a JIP
	//If a transition session is not launching
	IF NOT IS_TRANSITION_SESSION_LAUNCHING()
	//and we're not accepting a tournament invite OR it's an activity session
	AND (NOT IS_SC_COMMUNITY_PLAYLIST_LAUNCHING(ciEVENT_TOURNAMENT_PLAYLIST) OR NETWORK_IS_ACTIVITY_SESSION())
		SETUP_BEING_INVITED_TO_GAME_IN_PROGRESS(TRUE)	
		SET_TRANSITION_SESSIONS_JIP_SET_UP_FOR_SPAWN()
	ENDIF
	
	//sets flag for cinema unlock
	SET_CINEMA_UNLOCK_FLAGS()
	
	// turns off the cable car ambient zone as there are no cable cars in mp
	SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_COUNTRYSIDE_CHILEAD_CABLE_CAR_LINE", FALSE, TRUE)
	
	// turn off the casino alarm zones
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		PRINTLN("[JS][ALARM] Clearing Casino Alarm Zones - FreemodeLite")
		SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_H3_Casino_Alarm_Zone_01_Exterior", FALSE, TRUE)
		SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_H3_Casino_Alarm_Zone_02_Interior", FALSE, TRUE)
	ENDIF
	
	// turns off the ambient rollercoaster model
	BROADCAST_SET_ROLLERCOASTER_HIDDEN(SPECIFIC_PLAYER(PLAYER_ID()))

	// sets the radio emitters near arm wrestling as on
	SET_ARM_WRESTLING_RADIO_EMITTER_STATE()
	
	//turns off scenarios in banks/police stations that are locked off in mp.
	DISABLE_BANK_SCENARIO_GROUPS()
	PRINTLN("TRANSITION TIME - FREEMODE - WHILE TRUE                         - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")

	//add strippers that have already been unlocked to phone.
	HANDLE_UNLOCKED_STRIPPERS()
	
	//Re-init property values based on tunables
	REINIT_PROPERTY_VALUES()
	
	g_bFinishedInitialSpawn = FALSE //used to check that it is ok to spawn a car after having choosen a spawn location
	
	VERIFY_MP_PROPERTY_STAT() //Emergency patch fix for players losing saved property.
	
	VERIFY_MP_SAVED_VEHICLE_STATS() // Fix for players not being able to sell old high end vehicles.

	SETUP_PROPERTY_BLOCKS(propertyBlocks)
	
	INITIALISE_CASH_TRANSACTION_INDICES()	// Declare the indices for each transaction in the array of jobshare cash transactions
	
	FORCE_CONTACTS_ON_PHONE_ON_RESET() //added for 2146943
	
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_CLUBHOUSE_MOD_SHOP_AD_REMOTE_USAGE)
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_CLUBHOUSE_MOD_SHOP_AD_REMOTE_USAGE)
	ENDIF


	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bPassengerBombControl = GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_B_CONTROL)
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bPassengerCounterMeasureControl = GET_MP_BOOL_CHARACTER_STAT(MP_STAT_COUNTERMEASURE_CONTROL)
	

	
	#IF USE_FINAL_PRINTS
	PRINTLN_FINAL("PLAYSTATION_PLUS_PROMO (freemode) IS_ORBIS_VERSION() = ", IS_ORBIS_VERSION(), ", NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = ", NETWORK_HAVE_PLATFORM_SUBSCRIPTION(), ", NETWORK_IS_USING_ONLINE_PROMOTION() = ", NETWORK_IS_USING_ONLINE_PROMOTION(), ", NETWORK_SHOULD_SHOW_PROMOTION_ALERT_SCREEN() = ", NETWORK_SHOULD_SHOW_PROMOTION_ALERT_SCREEN())
	#ENDIF
	PRINTLN("PLAYSTATION_PLUS_PROMO (freemode) IS_ORBIS_VERSION() = ", IS_ORBIS_VERSION(), ", NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = ", NETWORK_HAVE_PLATFORM_SUBSCRIPTION(), ", NETWORK_IS_USING_ONLINE_PROMOTION() = ", NETWORK_IS_USING_ONLINE_PROMOTION(), ", NETWORK_SHOULD_SHOW_PROMOTION_ALERT_SCREEN() = ", NETWORK_SHOULD_SHOW_PROMOTION_ALERT_SCREEN())
	
	IF (IS_ORBIS_VERSION() = TRUE
	AND NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = TRUE
	AND NETWORK_IS_USING_ONLINE_PROMOTION() = TRUE)
	#IF IS_DEBUG_BUILD
	OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForcePSPlusPromoAlert")
	#ENDIF
		INCREMENT_MP_INT_PLAYER_STAT(MPPLY_WITHOUTPSPLUS)
	ENDIF

	CLEAR_ALL_PRIORITY_HUD_ELEMENTS()
	
	INIT_ASSASSINS_ABILITY()
	
	SEND_STARTER_PACK_METRIC()
	
	//reset the sync timer
	RESET_NET_TIMER(g_sUniqueTimerData.stUniquieIDSyncTimer)
	MAINTAIN_FM_net_tunable_check_LAUNCHING()
	
	INT iResetIdleTimer
	REPEAT NUM_NETWORK_PLAYERS iResetIdleTimer
		g_PlayerBlipsData.iBlipTimerIdle[iResetIdleTimer] = GET_NETWORK_TIME()
	ENDREPEAT

	PRINTLN("Freemode - just before while - GET_CURRENT_STACK_SIZE() = ", GET_CURRENT_STACK_SIZE())
	
	// Main loop.
	WHILE TRUE

		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("very start of frame")
		#ENDIF
		#ENDIF
		
		g_b_IsFreemodeRunningActive = TRUE
		IF NOT g_bDisableJumpOffHalfTrackFunctionality
			g_bEnableJumpingOffHalfTrackInProperty = FALSE
		ENDIF
		
		MAINTAIN_FREEMODE_BAIL_CONDITIONS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FREEMODE_BAIL_CONDITIONS")
		#ENDIF
		#ENDIF
		
		MAINTAIN_EXCLUSION_ZONES()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_EXCLUSION_ZONES")
		#ENDIF
		#ENDIF
		
		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("freemode SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - GET_CURRENT_STACK_SIZE() = ", GET_CURRENT_STACK_SIZE())
			SCRIPT_CLEANUP()
		ENDIF	
		
		MAINTAIN_KILLING_SP_CHEATS()
		
		IF NOT g_sMPTunables.bBLOCK_NS_TRANS
			REQUEST_CASH_TRANSACTION_SCRIPT()
		ELSE
			RELEASE_CASH_TRANSACTION_SCRIPT()
		ENDIF
		
		SET_LOCAL_PLAYERS_UNIQUE_HASH()

		KICK_THE_BONE_IDLE()
		
		SET_PREVIOUS_SAVED_RADIO_STATION()

		MAINTAIN_WEAPONIZED_VEHICLE_PASSIVE_MODE()
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			MAINTAIN_PEGASUS_FORCED_CLEANUP()
		ENDIF
		
		MAINTAIN_ASSASINS_ABILITY()
		
		PROCESS_SPECIAL_SPECTATOR_MODES_FOR_INSTANCED_CONTENT(TRUE)
		
		#IF IS_DEBUG_BUILD
		IF (bTestRougeWait)
			WAIT(0)
			bTestRougeWait= FALSE
		ENDIF
		#ENDIF

		RUN_TIMER_FOR_CONTRABAND_TYPES()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("RUN_TIMER_FOR_CONTRABAND_TYPES")
		#ENDIF
		#ENDIF 

				
		IF HAS_SCRIPT_TIMEDOUT_TRANSITION()
			PRINTLN("BC: - Freemode.sc SET_SCRIPT_TIMEDOUT_TRANSITION = TRUE so SCRIPT_CLEANUP ")
			PRINTLN("HAS_SCRIPT_TIMEDOUT_TRANSITION - GET_CURRENT_STACK_SIZE() = ", GET_CURRENT_STACK_SIZE())
			SCRIPT_CLEANUP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("HAS_SCRIPT_TIMEDOUT_TRANSITION")
		#ENDIF
		#ENDIF
			
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
		#ENDIF
		#ENDIF 	

		Maintain_Main_Generic_Client_Start_Of_Frame_Every_Frame()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("Maintain_Main_Generic_Client_Start_Of_Frame_Every_Frame")
		#ENDIF
		#ENDIF 	
		
		
		// Deal with the debug.
		#IF IS_DEBUG_BUILD		
			
			#IF SCRIPT_PROFILER_ACTIVE 
			OPEN_SCRIPT_PROFILE_MARKER_GROUP("Debug Stuff")
			#ENDIF

			PROCESS_TASK_DEBUG()
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("PROCESS_TASK_DEBUG")
			#ENDIF
			
			DO_DEBUG_AMBIENT_LAUNCHING()
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("DO_DEBUG_AMBIENT_LAUNCHING")
			#ENDIF	
		
			IF bCREATE_FM_WIDGETS <> bCREATE_FM_WIDGETS_CACHED
			AND NOT bCREATE_FM_WIDGETS_CACHED						// ONLY ALLOW US TO CREATE WIDGETS ONCE
				CREATE_TEST_SCRIPT_WIDGETS_PROPER()
			ENDIF
			
			IF bCREATE_FM_WIDGETS
				UPDATE_WIDGETS()
				Maintain_Main_Generic_Debug()
			ENDIF
			GB_FORCE_BOSS_IN_NEW_SESSION_CHECK()
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("UPDATE_WIDGETS")
			#ENDIF			
			
			MAINTAIN_CORONA_POS_WIDGETS()
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CORONA_POS_WIDGETS")
			#ENDIF
			
			IF bOpenTaskSequence
				FILL_SEQUENCE_TASK()
				bOpenTaskSequence = FALSE
			ENDIF
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("FILL_SEQUENCE_TASK")
			#ENDIF
			
			IF bStartTaskSequence
				DEBUG_TASK_SEQUENCES()
				bStartTaskSequence = FALSE
			ENDIF
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("DEBUG_TASK_SEQUENCES")
			#ENDIF
			
			
			IF NOT HAS_NET_TIMER_STARTED(iGaragePrintTimer)
				START_NET_TIMER(iGaragePrintTimer)
			ELIF HAS_NET_TIMER_EXPIRED(iGaragePrintTimer,2000)
				REINIT_NET_TIMER(iGaragePrintTimer)
				g_DB_AllowGarageOutput = TRUE
			ELSE
				g_DB_AllowGarageOutput = FALSE
			ENDIF	
			
			IF b_DB_AllowCoronaOutput
				//PRINTLN("Local player's GlobalplayerBD_FM[NATIVE_TO_INT(thePlayer)].loadedMissionData.vStartLocation :",GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].loadedMissionData.vStartLocation)
			ENDIF
			
			MAINTAIN_MASK_ANIM_DEBUG(maskAnimDataFM)
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MASK_ANIM_DEBUG")
			#ENDIF

			HANDLE_LAUNCHING_MISSIONS()
			
			IF g_db_bOutputDisplaySlotData
				MPSV_OUTPUT_GENERAL_SAVED_VEHICLE_DATA()
				g_db_bOutputDisplaySlotData = FALSE
			ENDIF
			
			IF g_db_bDrawDisplaySlotData
				MPSV_DISPLAY_GENERAL_SAVED_VEHICLE_DATA()
			ENDIF
			

			IF g_db_bTestUnlockAirportDoors
				UNLOCK_AIRPORT_GATES()
				g_db_bTestUnlockAirportDoors = FALSE
			ENDIF
			
			IF g_db_bTestResetUnlockAirportDoors
				RESET_UNLOCK_AIRPORT_GATES()
				g_db_bTestResetUnlockAirportDoors = FALSE	
			ENDIF
			
			IF bStartCoronaCloneScript
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("debug_clone_outfit_testing")) = 0
					REQUEST_SCRIPT_WITH_NAME_HASH(HASH("debug_clone_outfit_testing"))
					IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("debug_clone_outfit_testing"))
						START_NEW_SCRIPT_WITH_NAME_HASH(HASH("debug_clone_outfit_testing"), SPECIAL_ABILITY_STACK_SIZE)
						SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("debug_clone_outfit_testing"))
						bStartCoronaCloneScript = FALSE
					ENDIF
				ENDIF
			ENDIF
			
			#IF SCRIPT_PROFILER_ACTIVE 
			CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
			#ENDIF
			
		#ENDIF		
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("debug stuff")
		#ENDIF
		#ENDIF 	
		
// -----------------------------------	CLIENT
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			OPEN_SCRIPT_PROFILE_MARKER_GROUP("client processing")
		#ENDIF
		#ENDIF 
		
		
		InitialiseInteriorInstancesForDisabledJumping()
		
		//maintain the current event globals
		MAINTAIN_CURRENT_EVENT_GLOBALS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CURRENT_EVENT_GLOBALS")
		#ENDIF
		#ENDIF 	
	
	
		IF NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
			ForceSpectatorInvisible() 
		ENDIF
		//#IF IS_DEBUG_BUILD
		MAINTAIN_PROP_JIP_MISSING_DATA() //fix for broadcast data not synced when jipping into property. 2045912
		//#ENDIF

		CLEAR_REMOTE_SAVED_VEHICLE(iRemoteVehicleStagger, SavedVehicleStaggerTimer[iRemoteVehicleStagger]) // added to avoid car duping - see 1727927
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("CLEAR_REMOTE_SAVED_VEHICLE")
		#ENDIF
		#ENDIF 	
		
		UPDATE_NEARBY_MODDED_VEHICLES_TO_INVALIDATE_DECORATOR()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("UPDATE_NEARBY_MODDED_VEHICLES_TO_INVALIDATE_DECORATOR")
		#ENDIF
		#ENDIF
	
		SWITCH GET_CLIENT_SCRIPT_GAME_STATE(PLAYER_ID())

			// Wait until the server gives the all go before moving on.
			CASE MAIN_GAME_STATE_INI
				// KGM 21/12/12: New check added by Keith to launch all non-network scripts that should persist throughout Freemode (added for BootyCallHandler - but this may need to change)
				PRINTLN("GET_FRAME_COUNT MAIN_GAME_STATE_INI= ", GET_FRAME_COUNT())
				IF (Have_All_Persistent_Non_Network_Freemode_Scripts_Loaded())

//					//Check to see if all independance PTFX for smoke has loaded
					PRINTLN("GET_FRAME_COUNT Have_All_Persistent_Non_Network_Freemode_Scripts_Loaded = ", GET_FRAME_COUNT())
					IF GET_SERVER_SCRIPT_GAME_STATE() > MAIN_GAME_STATE_INI	
						PRINTLN("GET_FRAME_COUNT GET_SERVER_SCRIPT_GAME_STATE = ", GET_FRAME_COUNT())
				
						#IF IS_DEBUG_BUILD
						Initialise_MP_Damage_Modifiers()
						#ENDIF
						
						SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUN_TRANSITION)
					ELSE
						#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("MAIN_GAME_STATE_INI (freemode) waiting... GET_SERVER_SCRIPT_GAME_STATE() = ",GET_SERVER_SCRIPT_GAME_STATE() )
						#ENDIF
						PRINTLN("MAIN_GAME_STATE_INI (freemode) waiting... GET_SERVER_SCRIPT_GAME_STATE() = ",GET_SERVER_SCRIPT_GAME_STATE() )
					ENDIF

				ELSE
					#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("MAIN_GAME_STATE_INI (freemode) waiting... Have_All_Persistent_Non_Network_Freemode_Scripts_Loaded() = FALSE ")
					#ENDIF
					PRINTLN("MAIN_GAME_STATE_INI (freemode) waiting... Have_All_Persistent_Non_Network_Freemode_Scripts_Loaded() = FALSE " )
				ENDIF
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("MAIN_GAME_STATE_INI")
				#ENDIF
				#ENDIF 	
			BREAK
			
			CASE MAIN_GAME_STATE_RUN_TRANSITION
				TRIGGER_TRANSITION_MENU_ACTIVE(FALSE)  //Needs to be here so when DM_Deathmatch stops running, this can loop round and not instantly bring up the hud again.													
				SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_HOLD_FOR_TRANSITION)	
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("MAIN_GAME_STATE_RUN_TRANSITION")
				#ENDIF
				#ENDIF 	
			BREAK
			
			CASE MAIN_GAME_STATE_HOLD_FOR_TRANSITION
				
			
				//!!!BOBBY!!! if any of these checks are removed/added, please find and change 
				//DOES_MAINTRANSITION_NEED_TO_PUT_FREEMODE_INTO_RUNNING_STATE() Cheers! Brenda xxx
			
				//If we are launching a transition session
				IF IS_TRANSITION_SESSION_LAUNCHING()
				OR TRANSITION_SESSIONS_WAS_JOINING_WHEN_GOT_START_LAUNCH_EVENT()
					PRINTLN("[TS] FREEMODE - SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING) - IS_TRANSITION_SESSION_LAUNCHING() = TRUE")
					IF TRANSITION_SESSIONS_WAS_JOINING_WHEN_GOT_START_LAUNCH_EVENT()
						CLEAR_TRANSITION_SESSIONS_WAS_JOINING_WHEN_GOT_START_LAUNCH_EVENT()
					ENDIF
					SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING)	
					//Stop hiding the hud
					CLEAR_TRANSITION_SESSION_HIDE_HUD_ELEMENTS_DURING_TRANSITION_BACK_TO_FM()
					
				//If the main transition is being held because we are spectating then move on
				ELIF TRANSITION_SESSION_CAM_PULL_DOWN_WHEN_SPECTATING_READY() 
					PRINTLN("[TS] FREEMODE - SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING) - TRANSITION_SESSION_CAM_PULL_DOWN_WHEN_SPECTATING_READY() = TRUE")
					SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING)	
					//Stop hiding the hud
					CLEAR_TRANSITION_SESSION_HIDE_HUD_ELEMENTS_DURING_TRANSITION_BACK_TO_FM()
				
				//If we use a JIP
				ELIF HAS_TRANSITION_SESSION_USED_JIP()
					PRINTLN("[TS] FREEMODE - SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING) - HAS_TRANSITION_SESSION_USED_JIP() = TRUE")
					SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING)	
					//Stop hiding the hud
					CLEAR_TRANSITION_SESSION_HIDE_HUD_ELEMENTS_DURING_TRANSITION_BACK_TO_FM()
				
				//If we've exited a corona then we need to clean up stuff
				ELIF SHOULD_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT()
				OR TRANSITION_SESSIONS_CLEANUP_AFTER_JIP_BACK_OUT()
					IF NOT IM_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA()
					AND NOT SHOULD_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION()					
						PRINTLN("[TS] FREEMODE - SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING) - SHOULD_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT() = TRUE - FEATURE_HEIST_PLANNING")
						CLEAR_SKIP_WAIT_FOR_TRANSITION_CAMERA()
						IF SHOULD_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT()
						AND NOT TRANSITION_SESSIONS_CLEANUP_AFTER_JIP_BACK_OUT()
							SET_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE()
							SET_TRANSITION_SESSION_RETURN_PLAYER_CONTROL_AFTER_CAM_DOWN()
							TOGGLE_RENDERPHASES(TRUE) 
							TOGGLE_RENDERPHASES(TRUE)
							TRIGGER_SCREENBLUR_FADE_OUT(0)
							CLEAR_TRANSITION_SESSION_HIDE_HUD_ELEMENTS_DURING_TRANSITION_BACK_TO_FM()
							BUSYSPINNER_OFF()
							SET_CURRENT_FM_MISSION_NEEDS_CLEANUP(FALSE)
						ENDIF
						
						SET_NEWS_FEED_SPLASH_SCREEN_ACTIVE(FALSE)	
						CLEAR_TRANSITION_SESSIONS_CLEANUP_AFTER_JIP_BACK_OUT()
						// Want the player to be invincible for 4 seconds on respawn form a job.
						NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(4000)
						SET_FREEMODE_MOVED_ITSELF_INTO_RUNNING_STATE(TRUE)
						SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING)
						THEFEED_AUTO_POST_GAMETIPS_OFF()
						THEFEED_FLUSH_QUEUE()
						g_b_ReapplyStickySaveFailedFeed = FALSE
			  			PRINTLN("[TS] THEFEED_AUTO_POST_GAMETIPS_OFF() called")
					#IF IS_DEBUG_BUILD
					ELSE
			  			PRINTLN("[TS] SHOULD_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT = TRUE - IM_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA() - SHOULD_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION")						
						#ENDIF
					ENDIF
			
				
				//If the player quit out of the store
				ELIF SHOULD_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT()
					//if the car has been set up, or we don't need to
					IF DEAL_WITH_TRANSITION_SESSIONS_RESTORE_VEHICLE_AND_WARP()

						PRINTLN("[TS] FREEMODE - SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING) - SHOULD_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT() = TRUE")
						CLEAR_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT()
						CLEAR_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT_IS_CAM_UP()
						CLEAR_SPECIFIC_SPAWN_LOCATION() 
						SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING)	
						CLEAR_TRANSITION_SESSIONS_RESTORE_VEHICLE()
						CLEAR_TRANSITION_SESSION_END_SESSION_POS()
						NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(4000)
						IF NOT IS_PLAYER_SCTV(PLAYER_ID())							
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
						ENDIF
						BUSYSPINNER_OFF()
	  					PRINTLN("[TS] BUSYSPINNER_OFF() called")
						THEFEED_AUTO_POST_GAMETIPS_OFF()
						THEFEED_FLUSH_QUEUE()
						 g_b_ReapplyStickySaveFailedFeed = FALSE
	  					PRINTLN("[TS] THEFEED_AUTO_POST_GAMETIPS_OFF() called")
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PLAYER_PED_ID(), TRUE)
						ENDIF
						//Stop hiding the hud
						CLEAR_TRANSITION_SESSION_HIDE_HUD_ELEMENTS_DURING_TRANSITION_BACK_TO_FM()
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[TS] FREEMODE - CREATE_AND_ENTER_EOM_AMBIENT_VEHICLE = FALSE")						
					#ENDIF
					ENDIF
				
				//If transitioning back after a restart
				ELIF SHOULD_TRANSITION_SESSIONS_RESTART_AFTER_DO_TRANSITION_TO_NEW_GAME()		
					PRINTLN("[TS] FREEMODE - SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING) - SHOULD_TRANSITION_SESSIONS_RESTART_AFTER_DO_TRANSITION_TO_NEW_GAME() = TRUE")
					SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING)	
					IF NOT SHOULD_TRANSITION_SESSIONS_RESTART_PLAYLIST()
					AND NOT IS_PLAYER_SCTV(PLAYER_ID())						
	//					CLEAR_SKIP_WAIT_FOR_TRANSITION_CAMERA() //Moving into the transition and freemode cleanup
						//SET_CURRENT_FM_MISSION_NEEDS_CLEANUP(TRUE)			
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
					ENDIF
					THEFEED_AUTO_POST_GAMETIPS_OFF()
					THEFEED_FLUSH_QUEUE()
					 g_b_ReapplyStickySaveFailedFeed = FALSE
		  			PRINTLN("[TS] THEFEED_AUTO_POST_GAMETIPS_OFF() called")					
					//Stop hiding the hud
					CLEAR_TRANSITION_SESSION_HIDE_HUD_ELEMENTS_DURING_TRANSITION_BACK_TO_FM()
					CLEAR_TRANSITION_SESSIONS_RESTART_AFTER_DO_TRANSITION_TO_NEW_GAME()	
					
				//If the sky cam needs to clean up
				ELIF SHOULD_SKIP_WAIT_FOR_TRANSITION_CAMERA()
				OR SHOULD_TRANSITION_SESSIONS_RESTART_AFTER_DO_TRANSITION_TO_NEW_GAME()			
					IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
						PRINTLN("[TS] FREEMODE - SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING) - SHOULD_DO_TRANSITION_TO_GAME_RUN() = TRUE")
						//clear the flag as needed
						IF SHOULD_TRANSITION_SESSIONS_DO_TUT_MISSION_QUICKMATCH()
							CLEAR_TRANSITION_SESSIONS_DO_TUT_MISSION_QUICKMATCH()						
						ENDIF
						IF TRANSITION_SESSIONS_END_RESERVING_SLOT_ACEPTING_INVITE()
							CLEAR_TRANSITION_SESSIONS_END_RESERVING_SLOT_ACEPTING_INVITE()
							SET_CORONA_TO_CLEANUP(FALSE)
						ENDIF
						SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING)	
	//					CLEAR_SKIP_WAIT_FOR_TRANSITION_CAMERA() //Moving into the transition and freemode cleanup
						//SET_CURRENT_FM_MISSION_NEEDS_CLEANUP(TRUE)
						IF TRANSITION_SESSIONS_SHOULD_NOT_RESET_CONTROL()
							CLEAR_TRANSITION_SESSIONS_SHOULD_NOT_RESET_CONTROL()
						ELIF NOT IS_PLAYER_SCTV(PLAYER_ID())
							IF IS_SKYSWOOP_AT_GROUND() 
							AND NOT NETWORK_IS_ACTIVITY_SESSION()
								NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)		
							ENDIF
						ENDIF	
						THEFEED_AUTO_POST_GAMETIPS_OFF()
						THEFEED_FLUSH_QUEUE()
						 g_b_ReapplyStickySaveFailedFeed = FALSE
			  			PRINTLN("[TS] THEFEED_AUTO_POST_GAMETIPS_OFF() called")
						//Stop hiding the hud
						CLEAR_TRANSITION_SESSION_HIDE_HUD_ELEMENTS_DURING_TRANSITION_BACK_TO_FM()
						CLEAR_TRANSITION_SESSIONS_RESTART_AFTER_DO_TRANSITION_TO_NEW_GAME()	
						//If we're skipping the camera being pulled down and the cam is in the sky then set than we need to pull it down
						IF SHOULD_SKIP_WAIT_FOR_TRANSITION_CAMERA()
						AND IS_SKYSWOOP_MOVING()
							//If the post mission clean up is going to be doing the cam down
							IF NOT HAS_TRANSITION_SESSION_SELECTED_RESTART_RANDOM()
							//And we're not continuing as the post mission clean up will being that down
							AND GET_TRANSITION_SESSION_VOTE_STATUS() != ciFMMC_EOM_VOTE_STATUS_CONTINUE
								SET_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE()
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[TS] FREEMODE - SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING) - SET_TRANSITION_SESSION_SELECTED_RESTART_RANDOM - not calling SET_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE")						
								#ENDIF
							ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[TS] FREEMODE - SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING) - SHOULD_DO_TRANSITION_TO_GAME_RUN() = TRUE - IS_NEW_LOAD_SCENE_ACTIVE")						
						#ENDIF
					ENDIF
				//if player is swithcing to sctv
				ELIF SHOULD_TRANSITION_SESSION_ENTERING_FM_AFTER_SCRIPTS_KILLED_FOR_SCTV_SWITCH()				
					PRINTLN("[TS] FREEMODE - SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING) - SHOULD_TRANSITION_SESSION_ENTERING_FM_AFTER_SCRIPTS_KILLED_FOR_SCTV_SWITCH() = TRUE")
					CLEAR_TRANSITION_SESSION_ENTERING_FM_AFTER_SCRIPTS_KILLED_FOR_SCTV_SWITCH()
					SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING)	
					//Stop hiding the hud
					CLEAR_TRANSITION_SESSION_HIDE_HUD_ELEMENTS_DURING_TRANSITION_BACK_TO_FM()
					
				//It's a normal set up, set a flag to say so!
				ELSE
					bStandardTransitionInToFreemode = TRUE
					bStandardTransitionInToFreemode = bStandardTransitionInToFreemode
				ENDIF
				
				PROCESS_GAME_STATE_RUNNING()
				
				CLEAR_AREA_OF_SP_VEHICLES()
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("MAIN_GAME_STATE_HOLD_FOR_TRANSITION")
				#ENDIF
				#ENDIF 	
				
			BREAK
			
			CASE MAIN_GAME_STATE_INITIAL_CAMERA_WORK

				//HIDE_HUD_AND_RADAR_THIS_FRAME()
				IF bSetUpPlayer = FALSE

					bSetUpPlayer = TRUE
					
					BOOL bSetFreemodeTeam
					bSetFreemodeTeam = TRUE
						
					
					// In freemode, it's everyone against everyone
					IF NOT IS_PLAYER_SCTV(PLAYER_ID())
						IF bSetFreemodeTeam
							SET_PLAYER_TEAM(PLAYER_ID(), -1)
							PRINTSTRING(" FREEMODE - set players team to -1") PRINTNL()
						ENDIF
					ENDIF

					PRINTSTRING(" FREEMODE - bSetUpPlayer - DONE") PRINTNL()

				ELSE

					IF IS_PLAYER_SCTV(PLAYER_ID()) = FALSE
						IF HAS_EVENT_APP_LAUNCH_LIVE_AREA_RECIEVED() = FALSE 
						AND HAS_EVENT_APP_LAUNCH_STRAIGHT_INTO_FREEMODE_RECIEVED() = FALSE 
						AND HAS_EVENT_APP_LAUNCH_DIRECT_TO_CONTENT() = FALSE
												
								
							IF NOT IS_SKYSWOOP_AT_GROUND()
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
								ENDIF
							ENDIF
							
							//-- Player has watched the intro cut
							#IF IS_DEBUG_BUILD NET_NL() PRINTSTRING(" [dsw] FREEMODE - MAIN_GAME_STATE_RUNNING - FREEMODE INTRO ALREADY DONE") NET_NL() #ENDIF
							
							// Set all carmod blips to short range on FM entry B* 2415488
							SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_01_AP, FALSE)
							SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_05_ID2, FALSE)
							SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_06_BT1, FALSE)
							SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_07_CS1, FALSE)
							SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_08_CS6, FALSE)
							SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_SUPERMOD, FALSE)
							
						 	IF DO_FINAL_PRE_GAME_STATE_RUNNING_CHECKS()
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									IF NOT GET_ENTITY_COLLISION_DISABLED(PLAYER_PED_ID()) //added check for 2208850
										FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
										PRINTLN("FREEMODE- UN FREEZE entity position 111") 
									ELSE
										PRINTLN("FREEMODE- leaving player frozen- no collision") 
									ENDIF
									SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PLAYER_PED_ID(), TRUE)
								ENDIF

								
								IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
									INIT_LOCAL_PLAYER_CREW_DATA()
								ENDIF
								
								#IF IS_DEBUG_BUILD 
									IF g_SkipFmTutorials
										NET_NL() NET_PRINT("[dsw] Freemode launching and g_SkipFmTutorials is set!") NET_NL() 
									ENDIF
									IF NEED_TO_LAUNCH_MISSION_TUTORIAL()
										NET_NL() NET_PRINT("[dsw] Freemode launching and NEED_TO_LAUNCH_MISSION_TUTORIAL is set!") NET_NL() 
									ENDIF
								#ENDIF
								
								SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING)
							ENDIF

						ELSE
							SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING)
							PRINTSTRING(" FREEMODE - MAIN_GAME_STATE_RUNNING - PLAYER HAS_EVENT_APP_LAUNCH_RECIEVED = TRUE ")
						ENDIF
					ELSE
						SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING)
						PRINTSTRING(" FREEMODE - MAIN_GAME_STATE_RUNNING - PLAYER IS A SPECTATOR ")
					ENDIF
				ENDIF
			


				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("MAIN_GAME_STATE_INITIAL_CAMERA_WORK")
				#ENDIF
				#ENDIF 	
				
			BREAK
			
			// Main gameplay state.
			CASE MAIN_GAME_STATE_RUNNING	
			
				PROCESS_GAME_STATE_RUNNING()
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("PROCESS_GAME_STATE_RUNNING")
				#ENDIF
				#ENDIF 	
							
			BREAK	

			CASE MAIN_GAME_STATE_END				
				IF NETWORK_IS_IN_SESSION()
				AND NETWORK_CAN_SESSION_END()
					NETWORK_SESSION_END()	
				ENDIF
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("MAIN_GAME_STATE_END")
				#ENDIF
				#ENDIF 	
			BREAK
			
			DEFAULT
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("Freemode - invalid game state!!")
				#ENDIF
			BREAK
		ENDSWITCH
		

		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
		#ENDIF
		#ENDIF 
				
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("End of client processing")
		#ENDIF
		#ENDIF	

					
// -----------------------------------	SERVER	
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			OPEN_SCRIPT_PROFILE_MARKER_GROUP("Server processing")
		#ENDIF
		#ENDIF 

		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()

			
			SWITCH GET_SERVER_SCRIPT_GAME_STATE()
				
				// For now just move straight into playing.
				CASE MAIN_GAME_STATE_INI					
					//IF HAVE_ALL_PLAYERS_COMPLETED_FREEMODE_LOBBY()	// I removed this because we no longer have a loby and there for don't need to wait for other players. - Cheers, Bobby
						//IF SET_INITIAL_DOOR_STATES()
							SET_SERVER_GAME_STATE(MAIN_GAME_STATE_RUNNING)				
						//ENDIF
					//ENDIF
				BREAK
				
				// Look for game end conditions.
				CASE MAIN_GAME_STATE_RUNNING		
														
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAIN_GAME_STATE_RUNNING - start of frame")
					#ENDIF
					#ENDIF
				
					iParkingParticipant++
					IF iParkingParticipant >= NETWORK_GET_MAX_NUM_PARTICIPANTS()
						iParkingParticipant = 0	
					ENDIF
					SERVER_MAINTAIN_IMPOUND_PARKING_FOR_PLAYER(iParkingParticipant)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("SERVER_MAINTAIN_IMPOUND_PARKING_FOR_PLAYER")
					#ENDIF
					#ENDIF
					
					SERVER_MAINTAIN_SHOP_KEEPER_USAGE(g_iServerStaggeredParticipant)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_MAINTAIN_SHOP_KEEPER_USAGE")
					#ENDIF
					#ENDIF 

					SERVER_MAINTAIN_RESTRICTED_INTERIOR_ACCESS_FOR_PLAYERS(sRIA)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_MAINTAIN_RESTRICTED_INTERIOR_ACCESS_FOR_PLAYERS")
					#ENDIF
					#ENDIF 
					
					SERVER_MAINTAIN_INVINCIBLE_SHOP_KEEPER(g_iServerStaggeredParticipant, bSomeoneWantsInvincbleShopkeeper)										
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_MAINTAIN_INVINCIBLE_SHOP_KEEPER")
					#ENDIF
					#ENDIF 
					
					Maintain_Main_Generic_Server()	
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("Maintain_Main_Generic_Server")
					#ENDIF
					#ENDIF 
					
					MAINTAIN_MISC_PLAYER_STATE()					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MISC_PLAYER_STATE")
					#ENDIF
					#ENDIF 
				
					// JamesA: Maintain players leaving for corona positioning
					MAINTAIN_SERVER_CORONA_POS(iCoronaPosToCheck)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SERVER_CORONA_POS")
					#ENDIF
					#ENDIF 						
	
					// JamesA: Maintain our server side of betting
					MAINTAIN_SERVER_BETTING()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SERVER_BETTING")
					#ENDIF
					#ENDIF 
	
					SERVER_MAINTAIN_GANG_CALL(g_iServerStaggeredParticipant)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_MAINTAIN_GANG_CALL")
					#ENDIF
					#ENDIF
					
					SERVER_MAINTAIN_AMBIENT_TV_CHANNELS(g_iServerStaggeredParticipant)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_MAINTAIN_AMBIENT_TV_CHANNELS")
					#ENDIF
					#ENDIF
					
					MAINTAIN_CONTACT_REQUESTS_SERVER()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CONTACT_REQUESTS_SERVER")
					#ENDIF
					#ENDIF
					
					MAINTAIN_SERVER_INSTANCE_REQUESTS()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SERVER_INSTANCE_REQUESTS")
					#ENDIF
					#ENDIF

					SERVER_UPDATE_SYNCED_INTERACTION_REQUESTS()	
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_UPDATE_SYNCED_INTERACTION_REQUESTS")
					#ENDIF
					#ENDIF
					
					SERVER_UPDATE_GROUP_WARP_REQUESTS()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_UPDATE_GROUP_WARP_REQUESTS")
					#ENDIF
					#ENDIF
					
					MAINTAIN_REFRESH_ROCKSTAR_CONTENT_CHECK(g_iServerStaggeredParticipant)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_REFRESH_ROCKSTAR_CONTENT_CHECK")
					#ENDIF
					#ENDIF
					
					// --------------------------------------------------------------------------------//
					// Maintain the swapping of mmap objects
					// --------------------------------------------------------------------------------//
					MAIN32_MAINTAIN_IPLS(g_iServerStaggeredParticipant, iServerIPLbitSet)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("MAIN32_MAINTAIN_IPLS")
					#ENDIF
					#ENDIF
					
					IF FM_Car_gens_have_been_created = FALSE
						CREATE_ALL_FM_CAR_GENS(FM_CAR_GENERATORS_CAR_GENS,FM_Car_gens_have_been_created)//, BOOL bOnFoot)
					ENDIF
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("CREATE_ALL_FM_CAR_GENS")
					#ENDIF
					#ENDIF						
					

		
					SERVER_GB_UPDATE_GANG_IDS(iGB_stagger)	
		
				
					
					#IF IS_DEBUG_BUILD
						IF bHostEndGameNow		
							GlobalServerBD.iServerGameState = MAIN_GAME_STATE_END
						ENDIF
					#ENDIF	
				BREAK 
				
				CASE MAIN_GAME_STATE_END			
									
					// We'd want to upload scores here, show results, do whatever.
					
				BREAK	
				
				DEFAULT
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("Freemode.sc - SERVER invalid game state!!")
					#ENDIF
				BREAK
			ENDSWITCH
			

			
		ENDIF
		
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
			ADD_SCRIPT_PROFILE_MARKER("server processing")
		#ENDIF
		#ENDIF 	
		
	
		#IF IS_DEBUG_BUILD
			KEYPRESS_DEBUG()
			UPDATE_DEBUG()
		#ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("KEYPRESS_DEBUG and UPDATE_DEBUG")
		#ENDIF
		#ENDIF
			
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF
		#ENDIF		
		
	

		#IF IS_DEBUG_BUILD
			IF g_b_Debug_MakeFMArrayOverrun
				g_b_Debug_MakeFMArrayOverrun = FALSE
				INT FMDeadIndex = 4
				IF AnArrayFMDead[FMDeadIndex] = 0
					AnArrayFMDead[FMDeadIndex] = 5
				ENDIF
			ENDIF
			
		#ENDIF
		
		g_b_IsFreemodeRunningActive = FALSE
		IF NATIVE_TO_INT(PLAYER_ID()) != NATIVE_TO_INT(PARTICIPANT_ID())
			PRINTLN("[TS] * BC: [spawning] PLAYER_ID() != PARTICIPANT_ID() - PLAYER_ID() = ", NATIVE_TO_INT(PLAYER_ID()), " - PARTICIPANT_ID() = ", NATIVE_TO_INT(PARTICIPANT_ID()))
			ASSERTLN("[TS] * BC: [spawning] PLAYER_ID() != PARTICIPANT_ID() - PLAYER_ID() = ", NATIVE_TO_INT(PLAYER_ID()), " - PARTICIPANT_ID() = ", NATIVE_TO_INT(PARTICIPANT_ID()))
		ENDIF
	ENDWHILE
	
// End of Mission
ENDSCRIPT

#ENDIF // FEATURE_FREEMODE_CLASSIC


