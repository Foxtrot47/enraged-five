//////////////////////////////////////////////////////////////////////////////////////////////
// Name:        FM_INTRO_CUT_DEV.sc															//
// Description: Intro to Freemode for new players, script for development.				 	//
// Written by:  David Gentles																//
// Date: 08/10/2012																			//
//////////////////////////////////////////////////////////////////////////////////////////////


USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_fire.sch"
USING "building_control_public.sch"
USING "cutscene_public.sch"
USING "commands_streaming.sch"

// Network Headers
USING "net_events.sch"
USING "MP_SkyCam.sch"
USING "net_spawn.sch"


USING "net_mission.sch"
USING "net_scoring_common.sch"

USING "net_ambience.sch"
USING "net_rank_ui.sch"
USING "net_hud_activating.sch"
USING "selector_public.sch"
USING "help_at_location.sch"
USING "net_objective_text.sch"
USING "net_cutscene.sch"

USING "carmod_shop_private.sch"

USING "freemode_header.sch"
USING "net_mission_locate_message.sch"

USING "net_fm_intro_cut.sch"
USING "NET_TAXI.sch"

#IF IS_DEBUG_BUILD
	USING "net_debug_log.sch"
#ENDIF

CONST_INT NUM_MISSION_PLAYERS	2

VECTOR vPop

/// PURPOSE: The client broadcast data. Everyone can read this data, only the local player only the server can update it.
STRUCT PlayerBroadcastData
	INT iGameState
	INT iMissionState
	INT iFmIntroState
	INT iPlayerBitSet
	INT iIntroCutBitset
	#IF IS_DEBUG_BUILD
		BOOL bPressS = FALSE
	#ENDIF
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_MISSION_PLAYERS]

FM_CUT_STRUCT FmIntroCutscene

#IF IS_DEBUG_BUILD

	FLOAT fWdCurrentCutsceneTimer
	FLOAT fWdCurrentSRLAdjust
	INT iWdMusicPlaytime
	INT iWdMocapPlaytime
	INT iWdNumberOfTimings = 0
	FLOAT fWdCurrentShotPhase
	
/// PURPOSE:
///    Create widgets for intro cutscene development
	PROC CREATE_WIDGETS()
		START_WIDGET_GROUP("FM Intro Cut Dev")
			ADD_WIDGET_FLOAT_READ_ONLY("Current Cutscene Timer", fWdCurrentCutsceneTimer)
			ADD_WIDGET_FLOAT_READ_ONLY("Current SRL Adjust", fWdCurrentSRLAdjust)
			ADD_WIDGET_INT_READ_ONLY("GET_MUSIC_PLAYTIME()", iWdMusicPlaytime)
			ADD_WIDGET_INT_READ_ONLY("GET_CUTSCENE_TIME()", iWdMocapPlaytime)
			ADD_WIDGET_FLOAT_READ_ONLY("Current Shot Phase", fWdCurrentShotPhase)
		STOP_WIDGET_GROUP()
	ENDPROC
	
/// PURPOSE:
///    Update widgets for intro cutscene development
	PROC UPDATE_WIDGETS()
		IF iWdScriptProgress = 0
		ENDIF
		fWdCurrentCutsceneTimer = FmIntroCutscene.fLastSRLTimePassed
		fWdCurrentSRLAdjust = FmIntroCutscene.fLastSRLTimeAdjust
		
		iWdMusicPlaytime = GET_MUSIC_PLAYTIME()
		iWdMocapPlaytime = GET_CUTSCENE_TIME()
		
		fWdCurrentShotPhase	= GET_CURRENT_CUTSCENE_SHOT_PHASE(FmIntroCutscene)
		
		IF IS_BUTTON_JUST_RELEASED(PAD1, CROSS)
			SAVE_STRING_TO_DEBUG_FILE("Timing ")
			SAVE_INT_TO_DEBUG_FILE(iWdNumberOfTimings)
			SAVE_STRING_TO_DEBUG_FILE(" : ")
			SAVE_INT_TO_DEBUG_FILE(iWdMusicPlaytime)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			iWdNumberOfTimings++
		ENDIF
		
	ENDPROC
	
#ENDIF

/// PURPOSE:
///    Cleans up and terminates the intro cutscene development script
PROC SCRIPT_CLEANUP()

	CLEANUP_FREEMODE_INTRO_CUTSCENE(FmIntroCutscene)	
	
	SET_LOCAL_PLAYER_IN_TAXI_FOR_INTRO(FALSE)
	
	REMOVE_SCENARIO_BLOCKING_AREA(FmIntroCutscene.scenBlockCut)
	
	//-- Outside airport 
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA( <<-1042.918213,-2723.140137,17.341135>>, <<-1087.763916,-2667.605225,25.795933>>, 25.562500)
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA( <<-1005.742432,-2734.264648,14.919123>>, <<-1082.958374,-2675.188965,26.016474>>, 39.375000)
	
	CLEAR_BIT(MPGlobalsAmbience.iTaxiBitSet, biG_PlayerLeftTaxiInIntro)
	
	DISABLE_CELLPHONE_DIALOGUE_LINE_SKIP (FALSE)
	
	ENABLE_TAXI_SKIP()
	
	RESET_NET_TIMER(MPglobals.tdTimefade)
	MPglobals.g_iRealHours = 0
	MPglobals.g_iOverrideHours = 0
	MPglobals.g_iTutTimeDiff = 0
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) // 961426
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableCameraAnimations, TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTalkingAnimations, TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTextingAnimations, TRUE)
	ENDIF
	
	CLEAR_HELP()
	
	CLEAR_BIT(MPGlobalsAmbience.iTaxiBitSet, biG_SendTaxiToPlayer)
	HIDE_ALL_SHOP_BLIPS(FALSE)	
	HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_CARMOD, FALSE)
	HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_CLOTHES, FALSE)
	HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_HAIRDO, FALSE)
	HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_TATTOO, FALSE)
	HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_GUN, FALSE)
	
	SET_COUNTRYSIDE_AMMU_AVAILABLE(TRUE)
	SET_CITY_AMMU_AVAILABLE(TRUE)
	
	IF DOES_ENTITY_EXIST(FmIntroCutscene.vehLamar)
		DELETE_VEHICLE(FmIntroCutscene.vehLamar)
	ENDIF
	
	IF DOES_ENTITY_EXIST(FmIntroCutscene.pedIGLamar)
		DELETE_PED(FmIntroCutscene.pedIGLamar)
	ENDIF
	IF DOES_ENTITY_EXIST(FmIntroCutscene.pedCSLamar)
		DELETE_PED(FmIntroCutscene.pedCSLamar)
	ENDIF
	
	Clear_Any_Objective_Text_From_This_Script()
	
	SET_LOCAL_PLAYER_IS_RUNNING_INTRO_MISSION(FALSE)
	SET_LOCAL_PLAYER_RUNNING_FM_INTRO_CUT(FALSE)
		
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(),vPop, TRUE, FALSE, FALSE, FALSE)
	ENDIF
	
	TERMINATE_THIS_THREAD()
	
ENDPROC


SCRIPT
	
	#IF IS_DEBUG_BUILD
		PRINT_INTRO_STRING("FM_INTRO_CUT_DEV LAUNCHED")
		
		CREATE_WIDGETS()
		
	#ENDIF
	
	TRIGGER_MUSIC_EVENT("GLOBAL_KILL_MUSIC")
		
	// Main loop.
	WHILE TRUE

		WAIT(0) 
		
		IF g_bMPCreditsOn
			IF NOT g_bMPCreditsLoaded
				g_sfMPCredits = REQUEST_SCALEFORM_MOVIE("OPENING_CREDITS")
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== LAST CREDIT === REQUESTED")
					#ENDIF
				g_bMPCreditsLoaded = TRUE
			ELSE
				IF HAS_SCALEFORM_MOVIE_LOADED(g_sfMPCredits)
					IF NOT g_bMPCreditsIntroDraw
						DRAW_SCALEFORM_MOVIE_FULLSCREEN(g_sfMPCredits, 255, 255, 255, 255)
	
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== LAST CREDIT === g_sfMPCredits NOT LOADED")
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF RUN_FREEMODE_INTRO_CUTSCENE(FmIntroCutscene, playerBD[0].iIntroCutBitset)
			#IF IS_DEBUG_BUILD
			PRINT_INTRO_STRING("RUN_FREEMODE_INTRO_CUTSCENE = TRUE") 
			#ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		
		UPDATE_WIDGETS()
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_S, KEYBOARD_MODIFIER_NONE, "skip to end of dev intro")
		
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(g_sfMPCredits)
			g_bMPCreditsOn = FALSE
			g_bMPCreditsLoaded = FALSE
			g_bMPCreditsIntroDraw = FALSE
			g_bDirectorMPCreditCleared = FALSE
		
			vPop = GET_FINAL_RENDERED_CAM_COORD()
			RENDER_SCRIPT_CAMS(FALSE, TRUE, DEFAULT_INTERP_TO_FROM_GAME, TRUE, TRUE)
			DESTROY_ALL_CAMS()
			ENABLE_ALL_MP_HUD()
			DISABLE_SCRIPT_HUD(HUDPART_AWARDS, FALSE)
			DISPLAY_RADAR(TRUE)
			DISPLAY_HUD(TRUE)
			ENABLE_SELECTOR()
			CLEAR_OVERRIDE_WEATHER()
			TRIGGER_MUSIC_EVENT("GLOBAL_KILL_MUSIC")
			IF DOES_CAM_EXIST(FmIntroCutscene.cutsceneCam)
				SET_CAM_ACTIVE(FmIntroCutscene.cutsceneCam, FALSE)
			ENDIF
			SCRIPT_CLEANUP()
		ENDIF
		#ENDIF
		
	ENDWHILE
ENDSCRIPT
