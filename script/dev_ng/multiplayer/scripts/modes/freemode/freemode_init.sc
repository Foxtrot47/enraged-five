USING "globals.sch"
USING "mp_globals.sch"
USING "MP_Globals_FM.sch"
USING "net_stat_generator.sch"
USING "net_script_tunables.sch"
USING "net_main_client.sch"
USING "net_spawning_init.sch"
USING "net_interactions_synced.sch"
USING "net_pv_rate_limiter.sch"
#IF IS_DEBUG_BUILD
USING "net_debug.sch"
#ENDIF
USING "net_private_yacht.sch"

//Get the appropriate character stat
FUNC MP_INT_STATS GET_FLIGHT_SCHOOL_CHARACTER_MEDAL_STAT( INT iLesson )
	SWITCH iLesson
		CASE 0	RETURN MP_STAT_PILOT_SCHOOL_MEDAL_0
		CASE 1	RETURN MP_STAT_PILOT_SCHOOL_MEDAL_1
		CASE 2	RETURN MP_STAT_PILOT_SCHOOL_MEDAL_2		
		CASE 3	RETURN MP_STAT_PILOT_SCHOOL_MEDAL_3
		CASE 4	RETURN MP_STAT_PILOT_SCHOOL_MEDAL_4
		CASE 5	RETURN MP_STAT_PILOT_SCHOOL_MEDAL_5
		CASE 6	RETURN MP_STAT_PILOT_SCHOOL_MEDAL_6
		CASE 7	RETURN MP_STAT_PILOT_SCHOOL_MEDAL_7
		CASE 8	RETURN MP_STAT_PILOT_SCHOOL_MEDAL_8
		CASE 9	RETURN MP_STAT_PILOT_SCHOOL_MEDAL_9
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		//Backup stat should never return value from here
		PRINTLN("ERROR - FLIGHT SCHOOL LESSON STAT IS OUTWITH RANGE")
	#ENDIF
	RETURN MP_STAT_PILOT_SCHOOL_MEDAL_0
ENDFUNC

//Get the appropriate player stat
FUNC MPPLY_INT_STATS GET_FLIGHT_SCHOOL_PLAYER_MEDAL_STAT( INT iLesson )
	SWITCH iLesson
		CASE 0	RETURN MPPLY_PILOT_SCHOOL_MEDAL_0
		CASE 1	RETURN MPPLY_PILOT_SCHOOL_MEDAL_1
		CASE 2	RETURN MPPLY_PILOT_SCHOOL_MEDAL_2
		CASE 3	RETURN MPPLY_PILOT_SCHOOL_MEDAL_3
		CASE 4	RETURN MPPLY_PILOT_SCHOOL_MEDAL_4
		CASE 5	RETURN MPPLY_PILOT_SCHOOL_MEDAL_5
		CASE 6	RETURN MPPLY_PILOT_SCHOOL_MEDAL_6
		CASE 7	RETURN MPPLY_PILOT_SCHOOL_MEDAL_7
		CASE 8	RETURN MPPLY_PILOT_SCHOOL_MEDAL_8
		CASE 9	RETURN MPPLY_PILOT_SCHOOL_MEDAL_9
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		//Backup stat should never return value from here
		PRINTLN("ERROR - FLIGHT SCHOOL LESSON STAT IS OUTWITH RANGE")
	#ENDIF
	RETURN MPPLY_PILOT_SCHOOL_MEDAL_0
ENDFUNC

//Initialise the flight school player stats for b*1993597
PROC INIT_FLIGHT_SCHOOL_PLAYER_STATS()

	//Number of lessons
	CONST_INT iNumLessons 10
	
	INT icount
	INT pcount
	
	INT playerstat[iNumLessons]
	
	//=================================================
	//				GET INITIAL PLAYER STAT
	//=================================================
	FOR pcount = 0 to iNumLessons - 1
		playerstat[pcount] = GET_MP_INT_PLAYER_STAT(GET_FLIGHT_SCHOOL_PLAYER_MEDAL_STAT(pcount))
		#IF IS_DEBUG_BUILD
			PRINTLN("FLIGHT SCHOOL PRE UPDATE PLAYER STAT VAL : ", playerstat[pcount], " FOR LESSON: ", pcount)
		#ENDIF
	ENDFOR
	
	//==================================================
	//				  UPDATE STAT
	//==================================================
	
	//For each player character
	FOR icount = 0 TO MAX_NUM_CHARACTER_SLOTS - 1
		BOOL bisSlotActive = IS_STAT_CHARACTER_ACTIVE(icount) AND HAS_SLOT_BEEN_IGNORED_CODE(icount+1) = FALSE
		
		IF bisSlotActive = TRUE
			FOR pcount = 0 to iNumLessons - 1
			
				//Get current medal character stat
				INT iStatVal = GET_MP_INT_CHARACTER_STAT(GET_FLIGHT_SCHOOL_CHARACTER_MEDAL_STAT(pcount), icount)
			
				IF iStatVal > playerstat[pcount]
					#IF IS_DEBUG_BUILD
						PRINTLN("FLIGHT SCHOOL - SETTING PLAYER STAT NUM : ", pcount, " WITH VALUE:", iStatVal, " FOR PLAYER :", icount)
					#ENDIF
					
					//Update player stat to character stat val
					SET_MP_INT_PLAYER_STAT(GET_FLIGHT_SCHOOL_PLAYER_MEDAL_STAT(pcount), iStatVal)
				ENDIF
			ENDFOR
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
		//Player stats after update
		FOR pcount = 0 to iNumLessons - 1
			IF icount != 0
				playerstat[pcount] = GET_MP_INT_PLAYER_STAT(GET_FLIGHT_SCHOOL_PLAYER_MEDAL_STAT(pcount))
			ENDIF
			PRINTLN("FLIGHT SCHOOL AFTER UPDATE PLAYER STAT VAL : ", playerstat[pcount], " FOR LESSON: ", pcount)
		ENDFOR
	#ENDIF

ENDPROC


SCRIPT 
	
	NET_PRINT("********** freemode_init.sch started **********") NET_NL()

	INIT_STATS()
			
	INIT_MP_SPAWN_POINTS()
	
	INIT_SYNCED_INTERACTIONS()
	
	FLATTEN_GLOBALS_START_FREEMODE()
	
	// Fade the clouds back in 2193949
	SET_CLOUD_STATE(ENUMCLOUDSTATE_FADING_IN)
	
	Initialise_Main_Generic_Client()
	
//	Refresh_MP_Script_Tunables(TUNE_CONTEXT_FM)
	
	// To fix the played status when this rolls out, we do a check, if the player already has a 
	// time on mission that means they have played, only increment number of times played though
	// if they haven't been marked as played previously
	
	IF GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTELAPSEDTIME_0) > 0		
	AND NOT (GET_MP_INT_CHARACTER_STAT(MP_STAT_CRPILOTSCHOOL) > 0)
		PRINTLN("[MMM] KGM Auto-correcting pilotschool stats for the update.")
		PRINTLN("[MMM] Stat value for MP_STAT_CRPILOTSCHOOL : ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CRPILOTSCHOOL))
		INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CRPILOTSCHOOL)
		PRINTLN("[MMM] After  : ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CRPILOTSCHOOL)) 
	ENDIF
	
		INIT_FLIGHT_SCHOOL_PLAYER_STATS()
	
	g_FinishedFreemodeInit = TRUE
	
	//RUN_PV_STATS_SYNC_UPDATE() // removed due to 3893200, needs to be done after tunables have been initialised.
	
		INIT_PRIVATE_YACHT_CLIENT()
	
	INT iLastUsedPV = CURRENT_SAVED_VEHICLE_SLOT()
	IF (iLastUsedPV >= 0)
	AND NOT IS_THIS_MODEL_A_SPECIAL_VEHICLE(g_MpSavedVehicles[iLastUsedPV].vehicleSetupMP.VehicleSetup.eModel)
	AND NOT IS_THIS_MODEL_ALLOWED_IN_HANGAR(g_MpSavedVehicles[iLastUsedPV].vehicleSetupMP.VehicleSetup.eModel)
		MPGlobalsAmbience.iCachedPVSlotForPIMenu = iLastUsedPV
		PRINTLN("[personal_vehicle] freemode init - setting MPGlobalsAmbience.iCachedPVSlotForPIMenu to ", MPGlobalsAmbience.iCachedPVSlotForPIMenu)
	ELSE
		PRINTLN("[personal_vehicle] freemode init - iLastUsedPV was special vehicle, not setting MPGlobalsAmbience.iCachedPVSlotForPIMenu ", MPGlobalsAmbience.iCachedPVSlotForPIMenu)	
	ENDIF

	DELETE_ALL_NON_NETWORKED_VEHILCES()
	
	// remove car gens in the casino car park
	PRINTLN("removing car gens around casino")
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<948.45172, -39.27819, 76.75584>>, <<863.88330, 22.64865, 81.86767>>, FALSE)

	NET_PRINT("********** freemode_init.sch finished **********") NET_NL()

ENDSCRIPT
