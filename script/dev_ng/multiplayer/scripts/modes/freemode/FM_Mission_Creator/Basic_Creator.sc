//////////////////////////////////////////////////////////////
// Name: Basic_Creator.sc									//
// Description: Skeleton for a new creator					//
// Written by:  Jack Thallon								//
// Date: 30/01/2020											//
//////////////////////////////////////////////////////////////

#IF IS_FINAL_BUILD
script
endscript
#ENDIF

#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "globals.sch"
USING "script_player.sch"
USING "commands_pad.sch"
USING "commands_camera.sch" 
USING "FMMC_header.sch"
USING "FMMC_keyboard.sch"
USING "FMMC_MP_In_Game_Menu.sch"
USING "FM_Tutorials.sch"
USING "PM_MissionCreator_Public.sch"
USING "rc_helper_functions.sch"
#IF IS_DEBUG_BUILD
USING "profiler.sch"
#ENDIF

FMMC_LOCAL_STRUCT 						sFMMCdata
FMMC_CAM_DATA 							sCamData
PED_CREATION_STRUCT 					sPedStruct
TEAM_SPAWN_CREATION_STRUCT				sTeamSpawnStruct[FMMC_MAX_TEAMS]
VEHICLE_CREATION_STRUCT					sVehStruct
WEAPON_CREATION_STRUCT					sWepStruct
OBJECT_CREATION_STRUCT					sObjStruct
PROP_CREATION_STRUCT					sPropStruct
DYNOPROP_CREATION_STRUCT				sDynoPropStruct
INVISIBLE_OBJECT_STRUCT					sInvisibleObjects
LOCATION_CREATION_STRUCT				sLocStruct
TRAIN_CREATION_STRUCT					sTrainStruct
HUD_COLOURS_STRUCT 						sHCS
CREATION_VARS_STRUCT 					sCurrentVarsStruct
structFMMC_MENU_ITEMS 					sFMMCmenu
START_END_BLIPS							sStartEndBlips
FMMC_COMMON_MENUS_VARS 					sFMMCendStage
FMMC_UP_DOWN_STRUCT						menuScrollController
TRAIN_CREATION_STRUCT 					sTrainCreationStruct

GET_UGC_CONTENT_STRUCT sGetUGC_content

ENUM RESET_STATE 
	RESET_STATE_FADE,
	RESET_STATE_CLEAR,
	RESET_STATE_WEAPONS,
	RESET_STATE_UNLOAD_WEAPONS,
	RESET_STATE_VEHICLES,
	RESET_STATE_UNLOAD_VEHICLES,
	RESET_STATE_PROPS,
	RESET_STATE_DYNOPROPS,
	RESET_STATE_UNLOAD_PROPS,
	RESET_STATE_ATTACH,
	RESET_STATE_SPAWNS,
	RESET_STATE_TEAM_SPAWNS,
	RESET_STATE_OTHER,
	RESET_STATE_FINISH
ENDENUM
RESET_STATE iResetState

ENUM eTESTING_MC_MISSION_MENU_STATE
	eTESTINGMCMISSIONMENUSTATE_DISPLAY_HELP = 0,
	eTESTINGMCMISSIONMENUSTATE_SETUP_MENU,
	eTESTINGMCMISSIONMENUSTATE_SHOW_MENU
ENDENUM
STRUCT STRUCT_TEST_MENU_DATA
	eTESTING_MC_MISSION_MENU_STATE eTestMcMissionMenuState
	BOOL bDisplayedHelp
ENDSTRUCT
STRUCT_TEST_MENU_DATA structTestMcMissionMenuData

INT iLocalBitSet
INT iDoorSetupStage, iDoorSlowLoop
INT iLoadingOverrideTimer
INT iWarpState, iWarpStateTimeoutStart
INT iVehicleModelLoadTimers[FMMC_MAX_VEHICLES]
INT iPropModelTimers[FMMC_MAX_NUM_PROPS]

BOOL bInitialIntroCamSetup = TRUE
BOOL bInitialIntroCamWarp = FALSE
BOOL bIsMaleSPTC, bIsCreatorModelSetSPTC
BOOL bLastUseMouseKeyboard = FALSE
BOOL bOnGroundBeforeTest
BOOL bContentReadyForUGC

//Photo and Lobby Cameras
PREVIEW_PHOTO_STRUCT PPS
INT iIntroCamPlacementStage
SCALEFORM_INDEX SF_Movie_Gallery_Shutter_Index
INT iCamPanState = PAN_CAM_SETUP
JOB_INTRO_CUT_DATA jobIntroData
VECTOR vInitialPreviewPos

BOOL bShowMenu = TRUE
BOOL bShowMenuHighlight = FALSE
BOOL bDelayAFrame = TRUE

//Test Mode
BOOL bMpModeCleanedUp, bMpNeedsCleanedUp, bTestModeControllerScriptStarted, bTextSetUp
mission_display_struct on_mission_gang_box

// Camera Switching Stuff
VECTOR vSwitchVec = <<0,0,0>>
FLOAT fSwitchHeading = 0
BOOL bSwitchingCam = FALSE

//Trigger
INT iTriggerCreationStage = CREATION_STAGE_WAIT

//Blips
BLIP_INDEX bCameraTriggerBlip
BLIP_INDEX bCameraPanBlip
BLIP_INDEX bPhotoBlip

BOOL ButtonPressed
BOOL bCreatorLimitedCloudDown = FALSE
BOOL bSignedOut = FALSE

//Help
INT iHelpBitSet
INT iHelpBitSetOld
CONST_INT biPickupEntityButton 						0
CONST_INT biDeleteEntityButton 						4
CONST_INT biWarpToCameraButton 						6
CONST_INT biWarpToCoronaButton 						7

//Menus
INT iTopItem

PROC PROCESS_PRE_GAME()

	SET_BIT(sFMMCdata.iBitSet, bCameraActive)
	SET_CREATOR_AUDIO(TRUE, FALSE)
	SETUP_FMMC_SCRIPT(sFMMCMenu, sHCS, sCurrentVarsStruct)
	INITIALISE_MODEL_ARRAYS(sPedStruct,	sWepStruct, sObjStruct, sFMMCmenu)
	
	sFMMCmenu.iScriptCreationType	= SCRIPT_CREATION_TYPE_MISSION
	INT i
	FOR i = 0 TO PROP_LIBRARY_MAX_ITEMS-1
		sFMMCmenu.iNumberOfPropsInLibrary[i] = -1
	ENDFOR
	CLEAR_CACHED_AVAILABLE_VEHICLE_COUNTS()
	g_FMMC_STRUCT.tl63MissionName = ""
	g_FMMC_STRUCT.tl63MissionDecription[0] = ""
	g_FMMC_STRUCT.iMinNumParticipants = 1
	g_FMMC_STRUCT.iMaxNumberOfTeams = 1
	
	g_iStartingWave = 1
	
ENDPROC

PROC SCRIPT_CLEANUP(BOOL bSetWorldActive)

	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Main_Menu")) = 0
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(SF_Movie_Gallery_Shutter_Index)	
	
	IF DOES_CAM_EXIST(sCamData.cam)
		DESTROY_CAM(sCamData.cam)
	ENDIF
	
	IF DOES_BLIP_EXIST(sStartEndBlips.biStart)
		REMOVE_BLIP(sStartEndBlips.biStart)
	ENDIF
	
	IF sStartEndBlips.ciStartType != NULL
		DELETE_CHECKPOINT(sStartEndBlips.ciStartType)
		sStartEndBlips.ciStartType = NULL
	ENDIF
	
	IF DOES_BLIP_EXIST(bCameraPanBlip)
		REMOVE_BLIP(bCameraPanBlip)
	ENDIF
	IF DOES_BLIP_EXIST(bPhotoBlip)
		REMOVE_BLIP(bPhotoBlip)
	ENDIF
	
	CLEAR_CACHED_AVAILABLE_VEHICLE_COUNTS()
	
	g_bFMMC_TutorialSelectedFromMpSkyMenu = FALSE
	COMMON_FMMC_SCRIPT_CLEAN_UP(sPedStruct,	sVehStruct,	sWepStruct,	sObjStruct,sPropStruct, sDynoPropStruct, sInvisibleObjects, sCurrentVarsStruct, sFMMCmenu, TRUE, bSetWorldActive)
	iDoorSetupStage = 0
	
	CLEANUP_MENU_ASSETS()
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		IF SCRIPT_IS_CLOUD_AVAILABLE()
			PRINTLN("SCRIPT_IS_CLOUD_AVAILABLE()")
		    SET_FAKE_MULTIPLAYER_MODE(FALSE)
		ELSE
			PRINTLN("NOT SCRIPT_IS_CLOUD_AVAILABLE()")
			IF NOT IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED()
				NETWORK_SESSION_LEAVE_SINGLE_PLAYER() 
			ENDIF
			g_Private_IsMultiplayerCreatorRunning = FALSE
			g_Private_MultiplayerCreatorNeedsToEnd = TRUE
		ENDIF
	ENDIF
	
	SET_CREATOR_AUDIO(FALSE, FALSE)
	
	CLEANUP_ALL_INTERIORS(g_ArenaInterior)
	
	g_FMMC_STRUCT.iMissionType = 0
	
	RESET_SCRIPT_GFX_ALIGN()
	
	TERMINATE_THIS_THREAD()
ENDPROC

FUNC BOOL DEAL_WITH_SKIPPING_EMPTY_OPTIONS(INT iDirection)
	
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > sFMMCmenu.iCurrentMenuLength-1
		SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
	ENDIF
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < 0
		SET_CREATOR_MENU_SELECTION(sFMMCmenu, sFMMCmenu.iCurrentMenuLength - 1)
	ENDIF

	IF SHOULD_CURRENT_SELECTION_BE_SKIPPED(sFMMCmenu)
		SKIP_EMPTY_OPTION(sFMMCmenu, iDirection)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC SET_ENTITY_CREATION_STATUS(INT iNewState)
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[CREATOR] - SET_ENTITY_CREATION_STATUS - Setting entity creation state from ", sCurrentVarsStruct.iEntityCreationStatus, " to ", iNewState)
	sCurrentVarsStruct.iEntityCreationStatus = iNewState
ENDPROC

PROC TRACK_TIME_IN_CREATOR_MODE()

	IF NETWORK_CLAN_SERVICE_IS_VALID()  
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TIME_MISSION_CREATO, TIMERA())
		SETTIMERA(0)
	ENDIF
	
ENDPROC

PROC DEAL_WITH_AMBIENT_AND_HUD()
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
		sCurrentVarsStruct.creationStats.iCreatingTimeMS += GET_GAME_TIMER() - sCurrentVarsStruct.creationStats.iCurrentTimeMS
		
		FMMC_MAINTAIN_TIME_AND_WEATHER(g_FMMC_STRUCT.iTimeOfDay)
		FM_MAINTAIN_MISSION_DENSITYS_THIS_FRAME()	
		IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)	
			DISABLE_ADDITIONAL_CONTROLS()
		ENDIF
		BOOL bHideRadar
		FMMC_HIDE_HUD_ELEMENTS(sFMMCdata, sFMMCMenu, bHideRadar)
		MAINTAIN_CAMERA_ROTATION_LIMITS(sCamData, sFMMCMenu.sActiveMenu, sCurrentVarsStruct, bInitialIntroCamSetup, bInitialIntroCamWarp)
		IF bHideRadar
			SET_BIGMAP_ACTIVE(FALSE, FALSE)
		ELSE
			SET_BIGMAP_ACTIVE(TRUE, FALSE)
		ENDIF
	ENDIF
	sCurrentVarsStruct.creationStats.iCurrentTimeMS = GET_GAME_TIMER()
	
	DEAL_WITH_PHOTO_PREVIEW(PPS, sFMMCendStage.sTakePhotoVars, sCurrentVarsStruct.iMenuState, sFMMCMenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE AND iIntroCamPlacementStage = 2 AND NOT IS_BIT_SET(sFMMCmenu.iMiscBitSet, bsHidePreviewPhoto),sFMMCMenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE)
	DEAL_WITH_THE_PLACING_INTRO_CAM(SF_Movie_Gallery_Shutter_Index, sFMMCMenu.sActiveMenu, iIntroCamPlacementStage, bInitialIntroCamSetup, bShowMenu, sCurrentVarsStruct.iMenuState, sCurrentVarsStruct.iEntityCreationStatus)
	
	CREATOR_DOORS_SETUP(iDoorSlowLoop,iDoorSetupStage, FMMC_TYPE_MISSION)
	
	IF SCRIPT_IS_CLOUD_AVAILABLE()
		TRACK_TIME_IN_CREATOR_MODE()
	ENDIF
ENDPROC

PROC MAINTAIN_PLACED_MARKERS()
	MAINTAIN_PLACED_TRIGGER_LOCATION(g_FMMC_STRUCT.vStartPos, sStartEndBlips.biStart, sStartEndBlips.ciStartType, g_FMMC_STRUCT.vCameraPanPos, bCameraPanBlip, bPhotoBlip)
ENDPROC

PROC SET_ALL_MENU_ITEMS_ACTIVE()
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_CREATOR)
	IF g_FMMC_STRUCT.bMissionIsPublished = FALSE
	AND SCRIPT_IS_CLOUD_AVAILABLE()
		SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
	ENDIF
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_RADIO)
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_OPTIONS)
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_EXIT)
ENDPROC

PROC HANDLE_ENTITY_CREATION()
	SWITCH sFMMCmenu.iEntityCreation
		CASE -1
			sCurrentVarsStruct.iHoverEntityType  = -1
			IF sFMMCMenu.sActiveMenu != eFmmc_TOP_MENU
			AND sFMMCMenu.sActiveMenu != eFmmc_MAIN_OPTIONS_BASE
				UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT)
			ELSE
				UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT, FALSE)
			ENDIF
		BREAK
		
		CASE CREATION_TYPE_TRIGGER
			IF DO_TRIGGER_CREATION(sFMMCmenu, sCurrentVarsStruct, sFMMCdata, sStartEndBlips.biStart, sHCS.hcStartCoronaColour,sStartEndBlips.ciStartType, sFMMCendStage, iTriggerCreationStage, 3)
				SET_ALL_MENU_ITEMS_ACTIVE()
			ENDIF
		BREAK
		
		CASE CREATION_TYPE_PAN_CAM
			BOOL bResetCamMenu
			MAINTAIN_PLACING_INTRO_CAMERAS(sFMMCMenu, bCameraTriggerBlip, bResetCamMenu, bInitialIntroCamSetup)
			UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT, FALSE)
			IF bResetCamMenu
				REFRESH_MENU(sFMMCMenu)
				sCurrentVarsStruct.bResetUpHelp = TRUE
			ENDIF
		BREAK
		
		CASE CREATION_TYPE_VEHICLES
			DO_VEHICLE_CREATION(sVehStruct, sPedStruct, sObjStruct, sTrainCreationStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage, FMMC_MAX_VEHICLES, iLocalBitSet)
		BREAK
		
		CASE CREATION_TYPE_WEAPONS
			DO_WEAPON_CREATION(sWepStruct, sObjStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata,sFMMCendStage, FMMC_MAX_WEAPONS, g_FMMC_STRUCT.iVehicleDeathmatch = 0, iLocalBitSet)
		BREAK
		
		CASE CREATION_TYPE_PROPS
			DO_PROP_CREATION(sPropStruct, sVehStruct, sObjStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata,sFMMCendStage, sCamData, sPedStruct, sTrainCreationStruct, GET_FMMC_MAX_NUM_PROPS(), iLocalBitSet)
		BREAK
		
		CASE CREATION_TYPE_DYNOPROPS
			DO_DYNOPROP_CREATION(sDynoPropStruct, sPropStruct, sVehStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata,sFMMCendStage, sCamData, sPedStruct, sTrainCreationStruct, FMMC_MAX_NUM_DYNOPROPS, iLocalBitSet)
		BREAK
		
		CASE CREATION_TYPE_GOTO_LOC
			DO_LOCATION_CREATION(g_CreatorsSelDetails, sLocStruct, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sFMMCendStage, FMMC_MAX_GO_TO_LOCATIONS)
		BREAK
		
		CASE CREATION_TYPE_WORLD_PROPS
			DO_WORLD_PROP_SELECTION(sFMMCmenu, sCurrentVarsStruct)
		BREAK
		
		CASE CREATION_TYPE_DOOR
			DO_DOOR_SELECTION(sCurrentVarsStruct, sFMMCmenu, sFMMCdata)
		BREAK
		
		CASE CREATION_TYPE_PEDS
			DO_PED_CREATION(sPedStruct, sVehStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage, sTrainCreationStruct)
		BREAK
		
		CASE CREATION_TYPE_FMMC_ZONE
			DO_ZONE_CREATION(sCurrentVarsStruct, sFMMCmenu, sFMMCendStage)
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfZones, GET_MAX_PLACED_ZONES(), "FMMCCMENU_18")
		BREAK
		
		CASE CREATION_TYPE_DUMMY_BLIPS
			DO_DUMMY_BLIP_CREATION(g_CreatorsSelDetails, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sFMMCendStage)
		BREAK
		
	ENDSWITCH
ENDPROC

PROC CONTROL_CAMERA_AND_CORONA()
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)		

		CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciDRAW_CORONA)

		DEAL_WITH_HITTING_MAX_ENTITES(sCurrentVarsStruct, sFMMCmenu)
		
		IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
			MAINTAIN_FMMC_CAMERA(sFMMCdata, sCamData, sCurrentVarsStruct.iMenuState, DEFAULT, FALSE)
		ELSE
			IF IS_PLAYER_IN_A_MAP_ESCAPE(PLAYER_PED_ID())
				SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct,FALSE,FALSE,TRUE,FALSE)
			ENDIF
		ENDIF
		
		IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()	
		AND sFMMCMenu.sActiveMenu != eFmmc_PAN_CAM_BASE
		AND sFMMCMenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
			
			VECTOR vDummyRot = <<0,0,0>>
			IF DOES_CAM_EXIST(sCamData.cam)
				vDummyRot = GET_CAM_ROT(sCamData.cam)
			ENDIF
			
			MAINTAIN_SCREEN_CENTER_COORD(vDummyRot, sInvisibleObjects, sCurrentVarsStruct, sFMMCmenu, TRUE, 	sHCS.hcCurrentCoronaColour, sFMMCmenu.fCreationHeightIncrease, sFMMCmenu.fCreationHeightIncrease != 0.0, FALSE, IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround), FALSE, g_FMMC_STRUCT.iVehicleDeathmatch = 1)
			
		ENDIF
	ENDIF
ENDPROC


FUNC MENU_ICON_TYPE DOES_THIS_DESCRIPTION_NEED_A_WARNING(STRING description)
	IF ARE_STRINGS_EQUAL(description, "FMMCNO_CLOUD")
	OR ARE_STRINGS_EQUAL(description, GET_DISCONNECT_HELP_MESSAGE())
	OR ARE_STRINGS_EQUAL(description, "FMMCNO_SCLUB")
		RETURN MENU_ICON_ALERT
	ENDIF
	
	RETURN MENU_ICON_DUMMY
ENDFUNC

FUNC STRING GET_MENU_ITEM_DESCRIPTION()
	RETURN ""
ENDFUNC

PROC DRAW_MENU_SELECTION()
	
	// Clear all the help bits before checking
	clear_BIT(iHelpBitSet, biWarpToCoronaButton) 
	clear_BIT(iHelpBitSet, biWarpToCameraButton) 	
	CLEAR_BIT(iHelpBitSet, biPickupEntityButton)
	CLEAR_BIT(iHelpBitSet, biDeleteEntityButton)	
	sCurrentVarsStruct.bSelectedAnEntity = FALSE
		
	IF IS_BIT_SET(iLocalBitSet, biEntityInCorona)
	AND sCurrentVarsStruct.iHoverEntityType != CREATION_TYPE_CHECKPOINT
	AND sCurrentVarsStruct.iHoverEntityType != CREATION_TYPE_PROPS
		sCurrentVarsStruct.bSelectedAnEntity = TRUE
	ENDIF
	IF sFMMCMenu.iSelectedEntity = -1
		IF IS_BIT_SET(iLocalBitSet, biEntityInCorona)
		AND (sCurrentVarsStruct.iHoverEntityType = sFMMCmenu.iEntityCreation)
			IF IS_BIT_SET(iLocalBitSet, biCanEditEntityInCorona)
				SET_BIT(iHelpBitSet, biPickupEntityButton)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iLocalBitSet, biEntityInCorona)
			SET_BIT(iHelpBitSet, biDeleteEntityButton)
		ENDIF
	ENDIF
	
	IF sFMMCMenu.iSelectedEntity = -1		
		IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
			SET_BIT(iHelpBitSet, biWarpToCameraButton) 
		ELSE
			SET_BIT(iHelpBitSet, biWarpToCoronaButton)
		ENDIF
	ENDIF
	
	BOOL bRefresh
	IF iHelpBitSetOld != iHelpBitSet
		iHelpBitSetOld = iHelpBitSet
		PRINTLN("bRefresh = TRUE")
		bRefresh = TRUE
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF NOT IS_GAMEPLAY_CAM_RENDERING()
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_CREATOR_CAMERA_ORBIT_TOGGLE_BUTTON())
			OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, GET_CREATOR_CAMERA_ORBIT_TOGGLE_BUTTON())
				PRINTLN("bRefresh = TRUE - FORCED BY HOLDING LSHIFT DOWN")
				bRefresh = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// Set the current item and draw menu each frame
	IF sFMMCmenu.iCurrentMenuLength <= 10
		iTopItem = 0
	ENDIF
	SET_TOP_MENU_ITEM(iTopItem)
	SET_CURRENT_MENU_ITEM(GET_CREATOR_MENU_SELECTION(sFMMCmenu))
		
	IF sFMMCMenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
	OR sFMMCMenu.sActiveMenu = eFmmc_PAN_CAM_BASE
		sCurrentVarsStruct.bDisplaySelected = FALSE
	ENDIF
	
	PRINTLN("sFailReason = ", sCurrentVarsStruct.sFailReason)
	IF sCurrentVarsStruct.bDisplayFailReason
		IF sCurrentVarsStruct.iFailDescriptionTimer > GET_GAME_TIMER() - 5000
			SET_CURRENT_MENU_ITEM_DESCRIPTION(sCurrentVarsStruct.sFailReason)
		ELSE
			sCurrentVarsStruct.bDisplayFailReason = FALSE
		ENDIF
	ENDIF
	
	IF sCurrentVarsStruct.bDisplayFailReason = FALSE
		IF sCurrentVarsStruct.bDisplaySelected
		AND sFMMCmenu.iEntityCreation != CREATION_TYPE_NONE
			PROCESS_SHOWING_HIGHLIGHTED_ENTITY(sCurrentVarsStruct)
		ELSE
			SET_CURRENT_MENU_ITEM_DESCRIPTION(GET_MENU_ITEM_DESCRIPTION(), 0, DOES_THIS_DESCRIPTION_NEED_A_WARNING(GET_MENU_ITEM_DESCRIPTION())) // Pass in "" to clear
			IF ARE_STRINGS_EQUAL(GET_MENU_ITEM_DESCRIPTION(), "DMC_H_14AT")
			OR ARE_STRINGS_EQUAL(GET_MENU_ITEM_DESCRIPTION(), "DMC_H_14T1")
				ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(((g_FMMC_STRUCT.iNumParticipants + 2) * 2) - g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints)
				IF ((g_FMMC_STRUCT.iNumParticipants + 2) * 2) - g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints > 1
					ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING("FMMC_SPP")
				ELSE
					ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING("FMMC_SPS")
				ENDIF
			ELIF sFMMCmenu.sActiveMenu = efmmc_PROP_TEMPLATE_GRAB_PROPS
			AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
				ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING("FM_NXT_DM")
			ENDIF
		ENDIF
	ENDIF	
		
	INT iLines = 10
	
	SET_MAX_MENU_ROWS_TO_DISPLAY(iLines)
	
	DRAW_MENU()
	
	iTopItem = GET_TOP_MENU_ITEM()
	
	//If the help text need to be redrawn then do so.
	IF bRefresh
		sCurrentVarsStruct.bResetUpHelp = TRUE
	ENDIF
ENDPROC

PROC HANDLE_SWITCH_TO_GROUND_CAMERA()
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
		IF sFMMCmenu.sActiveMenu != eFmmc_PAN_CAM_BASE
		AND sFMMCmenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
		AND NOT (HAS_CURRENT_PROP_BEEN_FREE_ROTATED(sFMMCmenu) AND IS_PC_VERSION() AND IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL))
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_CREATOR_SWITCH_CAMERA_BUTTON())		
				IF sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE
				AND sCurrentVarsStruct.bCanSwapCams
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ELSE
					PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
					PRINTSTRING("CANT SWITCH ERROR SOUND EFFECT!!!")PRINTNL()
					sCurrentVarsStruct.bDisplayFailReason = TRUE
					sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
					IF sCurrentVarsStruct.bCanSwapCams
						sCurrentVarsStruct.sFailReason = "FMMC_ER_026"
					ELSE
						sCurrentVarsStruct.sFailReason = "FMMC_ER_CAMSWP"
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

PROC DEAL_WITH_CAMERA_SWITCH_SELECTION(BOOL bIncrease, INT iMaximumValue)
	
	IF NOT HAS_NET_TIMER_STARTED(sFMMCmenu.tdCycleSwitchCooldownTimer)
		START_NET_TIMER(sFMMCmenu.tdCycleSwitchCooldownTimer)
	ELIF HAS_NET_TIMER_STARTED(sFMMCmenu.tdCycleSwitchCooldownTimer)
		REINIT_NET_TIMER(sFMMCmenu.tdCycleSwitchCooldownTimer)
	ENDIF
	
	IF iMaximumValue > 0
		IF bSwitchingCam = FALSE
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			
			IF bIncrease
				sFMMCmenu.iSwitchCam ++
				IF sFMMCmenu.iSwitchCam < 0
					sFMMCmenu.iSwitchCam = 1
				ENDIF
			ELSE
				sFMMCmenu.iSwitchCam --
				IF sFMMCmenu.iSwitchCam < 0
					sFMMCmenu.iSwitchCam = iMaximumValue - 1
				ENDIF
			ENDIF
			IF sFMMCmenu.iSwitchCam > iMaximumValue - 1
				sFMMCmenu.iSwitchCam = 0
			ENDIF
			
			PLAY_EDIT_MENU_ITEM_SOUND()
			
			IF sFMMCMenu.sActiveMenu = eFmmc_DM_SPAWN_POINT
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[sFMMCmenu.iSwitchCam].vPos + <<0.0, 0.0, 8.1>>
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[sFMMCmenu.iSwitchCam].fHead
			ELIF sFMMCMenu.sActiveMenu = eFmmc_PICKUP_BASE
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sFMMCmenu.iSwitchCam].vPos + <<0.0, 0.0, 8.1>>
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sFMMCmenu.iSwitchCam].vRot.z
				IF IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ELIF sFMMCMenu.sActiveMenu = eFmmc_PROP_BASE
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedProp[sFMMCmenu.iSwitchCam].vPos
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedProp[sFMMCmenu.iSwitchCam].fHead
				IF IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ELIF sFMMCMenu.sActiveMenu = eFmmc_DYNOPROP_BASE
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sFMMCmenu.iSwitchCam].vPos + <<0.0, 0.0, 8.1>>
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sFMMCmenu.iSwitchCam].fHead
				IF IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ELIF sFMMCMenu.sActiveMenu = eFmmc_VEHICLES_BASE
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sFMMCmenu.iSwitchCam].vPos
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sFMMCmenu.iSwitchCam].fHead
				IF IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ELIF sFMMCMenu.sActiveMenu = eFmmc_DM_TEAM_SPAWN_POINT
				INT iTempTeam, iTempSpawn
				IF sFMMCmenu.iSwitchCam < g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0]
				AND sFMMCmenu.iSwitchCam >= 0
				AND g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0] > 0
					iTempTeam = 0
					iTempSpawn = sFMMCmenu.iSwitchCam
				ELIF sFMMCmenu.iSwitchCam < g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1] + g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0]
				AND sFMMCmenu.iSwitchCam >= g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0]
				AND g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1] > 0
					iTempTeam = 1
					iTempSpawn = sFMMCmenu.iSwitchCam - g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0]
				ELIF sFMMCmenu.iSwitchCam < g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[2] + g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1] + g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0]
				AND sFMMCmenu.iSwitchCam >= g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1] + g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0]
				AND g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[2] > 0
					iTempTeam = 2
					iTempSpawn = sFMMCmenu.iSwitchCam - (g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1] + g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0])
				ELSE
					iTempTeam = 3
					iTempSpawn = sFMMCmenu.iSwitchCam - (g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[2] + g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1] + g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0])
				ENDIF
				
				PRINTLN("iSwitchCam = ", sFMMCmenu.iSwitchCam, ", iTempTeam = ", iTempTeam, ", iTempSpawn = ", iTempSpawn)
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTempTeam][iTempSpawn].vPos + <<0.0, 0.0, 8.1>>
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTempTeam][iTempSpawn].fHead
			ENDIF
			
			bSwitchingCam = TRUE 
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_THIS_OPTION_SELECTABLE()
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DISPLAY_LIVERY_NAME()
	IF IS_ENTITY_ALIVE(sVehStruct.viCoronaVeh)	
		IF GET_VEHICLE_LIVERY_COUNT(sVehStruct.viCoronaVeh) > -1
		OR (IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, MULE) AND NOT IS_BIT_SET(sFMMCmenu.iVehBitSet, ciFMMC_VEHICLE_EXTRA1))
		OR GET_NUM_VEHICLE_MODS(sVehStruct.viCoronaVeh, MOD_LIVERY) > 0
			IF IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, MULE)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, SANCHEZ)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, POLMAV)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, SHAMAL)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, RUMPO)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, STUNT)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, GBURRITO2)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, JET)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, WINDSOR)
			OR (IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, AMBULANCE) AND IS_ROCKSTAR_DEV())
			OR (IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, POLICE3) AND IS_ROCKSTAR_DEV())
			OR (GET_NUM_VEHICLE_MODS(sVehStruct.viCoronaVeh, MOD_LIVERY) > 0 AND IS_ROCKSTAR_DEV())
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC EDIT_VEHICLE_BASE_MENU(INT iChange)
	MODEL_NAMES vehModel = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu.iVehicleLibrary, sFMMCMenu.iVehicleType)
	//Category
	IF IS_MENU_ITEM_SELECTED("FMMC_MCAT")
		EDIT_CREATOR_VEHICLE_LIBRARY_MENU_ITEM(sFMMCMenu, sFMMCmenu.iVehicleLibrary, sFMMCMenu.iVehicleType, iChange)
		RESET_CUR_VEH_MODS(sFMMCMenu)
	//Type
	ELIF IS_MENU_ITEM_SELECTED("FMMC_MTYPE")
		EDIT_CREATOR_VEHICLE_TYPE_MENU_ITEM(sFMMCMenu, sFMMCmenu.iVehicleLibrary, sFMMCMenu.iVehicleType, iChange)
		CHANGE_VEHICLE_COLOUR(sFMMCMenu, sVehStruct, sFMMCMenu.iCurrentEntitySelection[CREATION_TYPE_VEHICLE_COLOUR])
		RESET_CUR_VEH_MODS(sFMMCMenu)
		
	//Colour
	ELIF IS_MENU_ITEM_SELECTED("FMMC_VEH_COL")
		IF NOT ARE_STRINGS_EQUAL(sFMMCMenu.sSubTypeName[CREATION_TYPE_VEHICLE_COLOUR], "FMMC_COL_DEF")	
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCMenu.iCurrentEntitySelection[CREATION_TYPE_VEHICLE_COLOUR], iChange, ciNUMBER_OF_VEHICLE_COLOURS_PUBLIC, -1)
			CHANGE_VEHICLE_COLOUR(sFMMCMenu, sVehStruct, sFMMCMenu.iCurrentEntitySelection[CREATION_TYPE_VEHICLE_COLOUR])
		ENDIF
		
	//Livery
	ELIF IS_MENU_ITEM_SELECTED("FMMC_VEHL")
		IF IS_LIVERY_AVAILABLE(vehModel)
			IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
				CHANGE_VEHICLE_LIVERY(sFMMCMenu, vehModel, sVehStruct.viCoronaVeh,iChange)
			ENDIF
		ENDIF
	//Vehicle Health
	ELIF IS_MENU_ITEM_SELECTED("FMMC_VEH_HP")
		IF(iChange > 0 AND sFMMCmenu.iVehicleHealth >= 100)
		OR 	(iChange < 0 AND sFMMCmenu.iVehicleHealth > 100)
			iChange *= 25
		ELSE
			iChange *= 5
		ENDIF
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iVehicleHealth, iChange, 251, 25)
		
	//Bulletproof Tyres
	ELIF IS_MENU_ITEM_SELECTED("FMMC_VEH_EBPT")
	AND CAN_CURRENT_VEHICLE_HAVE_BULLETPROOF_TYRES(sFMMCMenu)
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iVehBitsetSeven,  ciFMMC_VEHICLE7_BULLETPROOF_TYRES)
		
	//Cycle Items
	ELIF IS_MENU_ITEM_SELECTED("FMMCCMENU_CYC")
		DEAL_WITH_CAMERA_SWITCH_SELECTION(iChange > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles)
		
	ENDIF
ENDPROC

PROC HANDLE_EVERY_FRAME_MENU_FUNCTIONALITY()
//	SWITCH sFMMCMenu.sActiveMenu
//		
//	ENDSWITCH
ENDPROC

PROC EDIT_MENU_OPTIONS(INT iChange)
	INT iType
	SWITCH sFMMCmenu.sActiveMenu
		CASE eFmmc_TOP_MENU
			
		BREAK
		
		CASE eFmmc_MAIN_OPTIONS_BASE
			HANDLE_BASIC_CREATOR_BASE_OPTIONS_MENU(sFMMCmenu, iChange)
		BREAK
		
		CASE eFmmc_PICKUP_BASE
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
				iType = CREATION_TYPE_WEAPON_LIBRARY
				sFMMCmenu.iCurrentEntitySelection[iType] = sFMMCmenu.iWepLibrary
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
				iType = CREATION_TYPE_WEAPONS
			ELIF NOT IS_ROCKSTAR_DEV() //once ammo & restrictions work, make this available to public
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChange > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 2
				iType = CREATION_TYPE_WEAPON_AMMO
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 3
				OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 4
				OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 5
				OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 6
				iType = CREATION_TYPE_WEAPON_REST
			ELSE
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChange > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons)
			ENDIF
		BREAK
		
		CASE eFmmc_VEHICLES_BASE
			EDIT_VEHICLE_BASE_MENU(iChange)
		BREAK
		
		CASE eFmmc_PROP_BASE
			MODEL_NAMES mnTemp 
			mnTemp = GET_CREATOR_PROP_MODEL(sFMMCMenu.iPropLibrary, sFMMCMenu.iPropType)
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_LIBRARY
				CLEAR_LONG_BITSET(sCurrentVarsStruct.iTooManyEntities)
				CHANGE_PROP_LIBRARY(sFMMCmenu, iChange)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_CREATE
				CHANGE_PROP_TYPE(sFMMCmenu, iChange)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_SET_INVISIBLE
				EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropBitSet, ciFMMC_PROP_SetInvisible)
			ENDIF
			IF NOT IS_STUNT_LIBRARY(sFMMCMenu.iPropLibrary)
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ROTATION_TYPE
					IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					AND IS_PROP_SUITABLE_FOR_ROTATION(mnTemp,sFMMCMenu.bPropSnapped)
						CHANGE_FREE_ROTATION_AXIS(sFMMCmenu, iChange, sPropStruct.viCoronaObj)
						PRINTLN("[LH] CHANGE_FREE_ROTATION_AXIS")
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_STACKING
					IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
						EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_STACK)
						REFRESH_MENU(sFMMCmenu)
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_SNAPPING
				AND (IS_ROCKSTAR_DEV() AND sFMMCmenu.iPropLibrary = PROP_LIBRARY_TEMPLATES)
					EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_ACTIVE)
					ENSURE_A_SNAP_MODE_IS_ACTIVE(sFMMCmenu)
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForSnappingOptions
				AND (sFMMCMenu.iPropLibrary = PROP_LIBRARY_TEMPLATES)
					IF IS_ROCKSTAR_DEV()
						EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_ACTIVE)
						ENSURE_A_SNAP_MODE_IS_ACTIVE(sFMMCmenu)
					ENDIF				
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForSnappingEntity
				AND (sFMMCMenu.iPropLibrary = PROP_LIBRARY_TEMPLATES)
					IF IS_ROCKSTAR_DEV()
						EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropSnapParentEntity, iChange, GET_NUMBER_OF_PROPS_IN_SAVED_TEMPLATE(sFMMCmenu.iProptype), -1)
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForPressurePad
					EDIT_PRESSUREPAD_TRAP_SELECTION_MENU_ITEM(sFMMCendStage, sFMMCMenu, iChange)
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForPressurePadDelay
				AND sFMMCMenu.iObjectPressurePadIndex >= 0
					EDIT_TRAP_PRESSUREPAD_DELAY_MENU_ITEM(sFMMCMenu, sFMMCendStage, iChange)
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iPropDisplayIndexes_MenuPos
					IF IS_ROCKSTAR_DEV()
						EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iCreatorInfoBitset, ciCIB_DRAW_PROP_NUMBERS)
					ENDIF
					
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = GET_CORRECT_CYCLE_PROPS_OPTION(sFMMCmenu)
					DEAL_WITH_CAMERA_SWITCH_SELECTION(iChange > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfProps)
				ENDIF
			ELSE
				MODEL_NAMES mnProp 
				mnProp = GET_CREATOR_PROP_MODEL(sFMMCMenu.iPropLibrary, sFMMCMenu.iPropType)
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_COLOUR AND NOT g_sMPTunables.bDisable_Prop_Colour
					IF sFMMCMenu.iPropLibrary = PROP_LIBRARY_STUNT_NEON_TUBE
						CHANGE_PROP_TYPE(sFMMCmenu, iChange*PROP_LIBRARY_STUNT_NEON_TUBE_TYPES)
						REFRESH_MENU(sFMMCmenu)
					ELSE
						INT i
						
						REPEAT MAX_COLOUR_PALETTE i
							EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropColour, iChange, MAX_COLOUR_PALETTE)
							
							IF IS_PROP_COLOUR_VALID(mnProp, sFMMCmenu.iPropColour)
								BREAKLOOP
							ENDIF
						ENDREPEAT
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ROTATION_TYPE
					IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					AND IS_PROP_SUITABLE_FOR_ROTATION(mnTemp,sFMMCMenu.bPropSnapped)
						CHANGE_FREE_ROTATION_AXIS(sFMMCmenu, iChange, sPropStruct.viCoronaObj)
						PRINTLN("[LH] CHANGE_FREE_ROTATION_AXIS")
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_STACKING
					IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
						EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_STACK)
						REFRESH_MENU(sFMMCmenu)
					ENDIF			
				ELIF IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary)
				OR IS_SNAPPABLE_STUNT_PROP(mnProp)
				OR (IS_ROCKSTAR_DEV() AND sFMMCmenu.iPropLibrary = PROP_LIBRARY_TEMPLATES)
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_SNAPPING
						EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_ACTIVE)
						ENSURE_A_SNAP_MODE_IS_ACTIVE(sFMMCmenu)
					ENDIF
				ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_STUNT_SPECIAL
					IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_FLARE_01"))
					OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("ind_prop_firework_01"))
					OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("ind_prop_firework_03"))
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_PLAYERS AND NOT g_sMPTunables.bDisable_Fireworks_players_to_trigger_for
							EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iPropBitSet,  ciFMMC_PROP_TriggerForAllRacers)
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_LAPS AND NOT g_sMPTunables.bDisable_Fireworks_Laps_to_Trigger_on
							EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iPropBitSet,  ciFMMC_PROP_TriggerOnEachLap)
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_RADIUS
							EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, sFMMCmenu.fPTFXTriggerRadius, TO_FLOAT(iChange*1),100.0)
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_SUB_TYPE
							EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropEffectSubType, iChange, ciSTUNT_FIREWORK_VFX_MAX, -1)
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_COLOUR
							EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropColouring, iChange, ciHUD_COLOUR_MAX -1)
						ENDIF
						
					ELIF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_tannoy"))
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iAlarmTypePos
							EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iAlarmSound, iChange, 12)
							DEAL_WITH_CHANGING_STUNT_ALARM_PREVIEW(sFMMCmenu, TRUE)
						ENDIF	
					ENDIF
				ENDIF
				IF IS_SPEED_BOOST_PROP(mnProp)
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCmenu.iSpeedBoostPropMenuPosition
						EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSpeedBoostAmount, iChange, 6, 1, TRUE)
					ENDIF		
				ENDIF
				IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown"))
				OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown_T1"))
				OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown_T2")) 
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCmenu.iSpeedBoostPropMenuPosition
						EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSpeedBoostAmount, iChange, 4, 1, TRUE)
					ENDIF		
				ENDIF			
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = GET_CORRECT_CYCLE_PROPS_OPTION(sFMMCmenu)
					DEAL_WITH_CAMERA_SWITCH_SELECTION(iChange > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfProps)
				ENDIF
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForPressurePad
					EDIT_PRESSUREPAD_TRAP_SELECTION_MENU_ITEM(sFMMCendStage, sFMMCMenu, iChange)
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForPressurePadDelay
				AND sFMMCMenu.iObjectPressurePadIndex >= 0
					EDIT_TRAP_PRESSUREPAD_DELAY_MENU_ITEM(sFMMCMenu, sFMMCendStage, iChange)
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iPropTurretTower_MenuPos
					EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iPropBitSet2,  ciFMMC_PROP2_UseAsTurret)
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iPropTurretTower_Heading
					EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, sFMMCMenu.fTurretHeading, TO_FLOAT(iChange)*5, 360.0, 0.0)
				ENDIF
			ENDIF
		BREAK
		
		CASE eFmmc_DYNOPROP_BASE
			MODEL_NAMES mnProp 
			mnProp = GET_CREATOR_PROP_MODEL(sFMMCMenu.iPropLibrary, sFMMCMenu.iPropType)
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DYNOPROP_MENU_LIBRARY
				CHANGE_PROP_LIBRARY(sFMMCmenu, iChange)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DYNOPROP_MENU_CREATE
				CHANGE_PROP_TYPE(sFMMCmenu, iChange)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DYNOPROP_MENU_ROTATION_TYPE
				IF (NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround) OR IS_ROCKSTAR_DEV())
				AND IS_PROP_SUITABLE_FOR_ROTATION(mnProp,sFMMCMenu.bPropSnapped)
					CHANGE_FREE_ROTATION_AXIS(sFMMCmenu, iChange, sDynoPropStruct.viCoronaObj)
					PRINTLN("[LH] CHANGE_FREE_ROTATION_AXIS (Dynoprop)")
				ENDIF
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DYNOPROP_MENU_COLOUR AND DOES_PROP_HAVE_EXTRA_COLOURS(mnProp)
				INT i
						
				REPEAT MAX_COLOUR_PALETTE i
					EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropColour, iChange, MAX_COLOUR_PALETTE)
					
					IF IS_PROP_COLOUR_VALID(mnProp, sFMMCmenu.iPropColour)
						BREAKLOOP
					ENDIF
				ENDREPEAT
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = (sFMMCMenu.iCurrentMenuLength - 1)
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChange > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps)
			ENDIF
		BREAK
		
		CASE eFmmc_DELETE_ENTITIES
			
		BREAK
		
		CASE eFmmc_TEAM_TEST
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
				
			ENDIF
		BREAK
		
		CASE eFmmc_ENEMY_BASE
			EDIT_BASIC_CREATOR_PED_BASE_MENU(sFMMCmenu, sFMMCendStage, iChange, sFMMCmenu.iPedModelTailIndex[sFMMCmenu.iPedLibrary])
		BREAK
		
		CASE eFmmc_LOCATION_BASE
		BREAK
		
		CASE eFmmc_DOORS_BASE
			EDIT_DOORS_BASE_LEGACY_MENU(sFMMCmenu, sFMMCendStage, iChange)
		BREAK
		
		CASE efMMC_WORLD_PROPS
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChange > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps)
			ENDIF
		BREAK
		
		CASE eFmmc_ZONES_BASE
			EDIT_ZONE_BASE_MENU(sFMMCmenu, sCurrentVarsStruct, sFMMCendStage, iChange)
		BREAK
		
		CASE eFMMC_ZONE_TYPE_SPECIFIC_OPTIONS
			EDIT_ZONE_TYPE_SPECIFIC_OPTIONS_MENU(sFMMCMenu, sFMMCendStage, iChange)
		BREAK
		
		CASE eFMMC_DUMMY_BLIPS
			EDIT_DUMMY_BLIP_BASE_MENU(sFMMCmenu, sFMMCendStage, iChange)
		BREAK
	ENDSWITCH
	
	IF iType != -1
	
		sFMMCmenu.iCurrentEntitySelection[iType]+= iChange
		INT iMaximum
		IF iType = CREATION_TYPE_WEAPONS
			IF g_FMMC_STRUCT.iVehicleDeathmatch = 0
				IF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_PISTOLS
					iMaximum = FMMC_MAX_NUM_PISTOLS
					IF NOT IS_ROCKSTAR_DEV()
						iMaximum = FMMC_MAX_NUM_PISTOLS
					ENDIF
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_SHOTGUNS
					iMaximum = FMMC_MAX_NUM_SHOTGUNS
					IF NOT IS_ROCKSTAR_DEV()
						iMaximum = FMMC_MAX_NUM_SHOTGUNS
					ENDIF
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_MGS
					iMaximum = FMMC_MAX_NUM_MGS
					IF NOT IS_ROCKSTAR_DEV()
						iMaximum = FMMC_MAX_NUM_MGS
					ENDIF
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_RIFLES
					iMaximum = FMMC_MAX_NUM_RIFLES
					IF NOT IS_ROCKSTAR_DEV()
						iMaximum = FMMC_MAX_NUM_RIFLES
					ENDIF
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_SNIPERS
					iMaximum = FMMC_MAX_NUM_SNIPERS
					IF NOT IS_ROCKSTAR_DEV()
						iMaximum = FMMC_MAX_NUM_SNIPERS
					ENDIF
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_HEAVY
					iMaximum = FMMC_MAX_NUM_HEAVY
					IF NOT IS_ROCKSTAR_DEV()
						iMaximum = FMMC_MAX_NUM_HEAVY-1
					ENDIF
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_THROWN
					iMaximum = FMMC_MAX_NUM_THROWN
					IF NOT IS_ROCKSTAR_DEV()
						iMaximum = FMMC_MAX_NUM_THROWN
					ENDIF
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_MELEE
					iMaximum = FMMC_MAX_NUM_MELEE
					IF NOT IS_ROCKSTAR_DEV()
						iMaximum = FMMC_MAX_NUM_MELEE
					ENDIF
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_SPECIAL
					iMaximum = FMMC_MAX_NUM_SPECIAL - 2
				ENDIF
			ELSE
				iMaximum = VEH_MAX_ARMOURY_TYPES
			ENDIF
		ELIF iType = CREATION_TYPE_WEAPON_AMMO
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sWepStruct.iNumberOfClips, iChange, FMMC_MAX_CLIPS)
			sFMMCmenu.iWepClips = sWepStruct.iNumberOfClips
		ELIF iType = CREATION_TYPE_WEAPON_REST
			EDIT_CREATOR_BOOL_MENU_ITEM(sFMMCendStage, sWepStruct.bWepRest[GET_CREATOR_MENU_SELECTION(sFMMCmenu)-3])
			sFMMCmenu.bWepRest[GET_CREATOR_MENU_SELECTION(sFMMCmenu)-3] = sWepStruct.bWepRest[GET_CREATOR_MENU_SELECTION(sFMMCmenu)-3]
		ELIF iType = CREATION_TYPE_WEAPON_LIBRARY
			IF g_FMMC_STRUCT.iVehicleDeathmatch = 0
				iMaximum = FMMC_WEAPON_LIBRARY_MAX
			ELSE
				iMaximum = FMMC_VEH_WEAPON_LIBRARY_MAX
			ENDIF
		ENDIF
		
		INT iWrapMax = iMaximum - 1
		
		IF sFMMCmenu.iCurrentEntitySelection[iType] < 0 
			sFMMCmenu.iCurrentEntitySelection[iType] = iWrapMax
		ELIF sFMMCmenu.iCurrentEntitySelection[iType] > iWrapMax
			sFMMCmenu.iCurrentEntitySelection[iType] = 0
		ENDIF
		
		IF iType = CREATION_TYPE_WEAPONS
			CHANGE_WEAPON_TYPE(sFMMCmenu, sWepStruct, sFMMCmenu.iCurrentEntitySelection[iType], TRUE, iChange > 0, iMaximum)
		ELIF iType = CREATION_TYPE_WEAPON_LIBRARY
			CHANGE_WEAPON_LIBRARY(sFMMCmenu, sWepStruct, sFMMCmenu.iCurrentEntitySelection[iType], TRUE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_THIS_OPTION_A_MENU_ACTION()	
	
	IF sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Action_Null
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_OPTION_A_MENU_GOTO()	
	
	IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Null_item
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
PROC REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(INT iCurrentState)

	MODEL_NAMES oldModel
	
	IF iCurrentState != CREATION_TYPE_VEHICLES
		IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
			oldModel = GET_ENTITY_MODEL(sVehStruct.viCoronaVeh)
			DELETE_VEHICLE(sVehStruct.viCoronaVeh)
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
	ENDIF
	IF iCurrentState !=CREATION_TYPE_WEAPONS
		IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
			oldModel = GET_ENTITY_MODEL(sWepStruct.viCoronaWep)
			DELETE_OBJECT(sWepStruct.viCoronaWep)					
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
	ENDIF
	IF iCurrentState != CREATION_TYPE_PROPS
		IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
			oldModel = GET_ENTITY_MODEL(sPropStruct.viCoronaObj)
			DELETE_OBJECT(sPropStruct.viCoronaObj)			
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
		DELETE_CURRENT_PROP_TEMPLATE_OBJECTS(sPropStruct)
	ENDIF
	IF iCurrentState != CREATION_TYPE_DYNOPROPS
		IF DOES_ENTITY_EXIST(sDynoPropStruct.viCoronaObj)
			oldModel = GET_ENTITY_MODEL(sDynoPropStruct.viCoronaObj)
			DELETE_OBJECT(sDynoPropStruct.viCoronaObj)
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
			
			IF GET_MODEL_NAME_OF_CHILD_PROP_FOR_DYNO(oldModel) != DUMMY_MODEL_FOR_SCRIPT
				IF DOES_ENTITY_EXIST(sDynoPropStruct.oiChildTemp)
					DELETE_OBJECT(sDynoPropStruct.oiChildTemp)
				ENDIF
				SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
			ENDIF
		ENDIF
	ENDIF
	IF iCurrentState != CREATION_TYPE_PEDS
		IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
			oldModel = GET_ENTITY_MODEL(sPedStruct.piTempPed)
			DELETE_PED(sPedStruct.piTempPed)
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF	
	ENDIF
ENDPROC

PROC CLEAN_LAST_GO_TO_LOCATION()
	PRINTLN("CLEAN_LAST_GO_TO_LOCATION")
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] < FMMC_MAX_GANG_HIDE_LOCATIONS
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].vLoc[0]			= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].fRadius[0]			= 0.0
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].vLoc1				= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].vLoc2				= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].fWidth				=  1.0
		INT iTeam
		FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
			g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].iRule[iTeam] 		= FMMC_OBJECTIVE_LOGIC_NONE
			g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].iPriority[iTeam]	= FMMC_PRIORITY_IGNORE
			g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].fHeading[iTeam]	= 0.0
		ENDFOR
	ENDIF
ENDPROC

PROC CANCEL_ENTITY_CREATION(INT iTypePassed)

	IF sFMMCMenu.iSelectedEntity = -1
		INT i
		
		IF sCurrentVarsStruct.mnThingToPlaceForBudget != DUMMY_MODEL_FOR_SCRIPT
			REMOVE_MODEL_FROM_CREATOR_BUDGET(sCurrentVarsStruct.mnThingToPlaceForBudget)
			sCurrentVarsStruct.mnThingToPlaceForBudget = DUMMY_MODEL_FOR_SCRIPT
		ENDIF
		
		sWepStruct.iSwitchingINT 	= CREATION_STAGE_WAIT
		sVehStruct.iSwitchingINT	= CREATION_STAGE_WAIT
		sObjStruct.iSwitchingINT	= CREATION_STAGE_WAIT
		SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)

		iTriggerCreationStage		= CREATION_STAGE_WAIT
		
		IF iTypePassed = CREATION_TYPE_VEHICLES
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1)
				CREATE_FMMC_BLIP(sVehStruct.biVehicleBlip[i], g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos, HUD_COLOUR_BLUEDARK, "FMMC_B_10", 1)
			ENDFOR
			UNLOAD_ALL_VEHICLE_MODELS()
		ELIF iTypePassed	=  CREATION_TYPE_WEAPONS
			FLOAT fSize
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons - 1)
				IF IS_SPECIAL_WEAPON_BLIP(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt)
					fSize = 1
				ELSE
					fSize = 1
				ENDIF
				CREATE_FMMC_BLIP(sWepStruct.biWeaponBlip[i], GET_ENTITY_COORDS(sWepStruct.oiWeapon[i]), HUD_COLOUR_GREEN, "", fSize)
				SET_BLIP_SPRITE(sWepStruct.biWeaponBlip[i] , GET_CORRECT_BLIP_SPRITE_FMMC(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt,sWepStruct.iSubType))
			ENDFOR
			UNLOAD_ALL_FMMC_WEAPON_ASSETS(TRUE, sWepStruct)
			UNLOAD_ALL_FMMC_WEAPON_ASSETS(FALSE, sWepStruct)
		ELIF iTypePassed	=  CREATION_TYPE_PROPS
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfProps - 1)
				IF DOES_ENTITY_EXIST(sPropStruct.oiObject[i])
					CREATE_FMMC_BLIP(sPropStruct.biObject[i], GET_ENTITY_COORDS(sPropStruct.oiObject[i]), HUD_COLOUR_PURPLE, "FMMC_B_5", 1)
				ENDIF
			ENDFOR
			UNLOAD_ALL_PROP_MODELS(sFMMCmenu)
		ELIF iTypePassed	=  CREATION_TYPE_DYNOPROPS
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps - 1)
				CREATE_FMMC_BLIP(sDynoPropStruct.biObject[i], GET_ENTITY_COORDS(sDynoPropStruct.oiObject[i]), HUD_COLOUR_NET_PLAYER2, "FMMC_B_13", 1)
			ENDFOR
			UNLOAD_ALL_PROP_MODELS(sFMMCmenu)
		ELIF iTypePassed	=  CREATION_TYPE_TRIGGER
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
				CREATE_FMMC_BLIP(sStartEndBlips.biStart, g_FMMC_STRUCT.vStartPos, HUD_COLOUR_WHITE, "FMMC_B_4", 1)	
				BLIP_SPRITE blip = GET_TRIGGER_RADAR_BLIP() 
				IF blip != RADAR_TRACE_INVALID
					SET_BLIP_SPRITE(sStartEndBlips.biStart, blip)
					SET_BLIP_NAME_FROM_TEXT_FILE(sStartEndBlips.biStart, "FMMC_B_4")	
				ENDIF
			ENDIF
		ELIF sFMMCmenu.iEntityCreation = CREATION_TYPE_GOTO_LOC
			CLEAN_LAST_GO_TO_LOCATION()
			sFMMCmenu.vLocAAVec1 = <<0,0,0>>
		ENDIF	
		
		IF sFMMCMenu.sActiveMenu != eFMMC_GENERIC_OVERRIDE_POSITION
		AND sFMMCMenu.sActiveMenu != eFmmc_GENERIC_OVERRIDE_ROTATION
		AND sFMMCMenu.sActiveMenu != eFmmc_PROP_ADVANCED_OPTIONS
		AND sFMMCMenu.sActiveMenu != eFmmc_SNAPPING_OPTIONS
		AND sFMMCMenu.sActiveMenu != eFMMC_OFFSET_POSITION
		AND sFMMCMenu.sActiveMenu != eFMMC_OFFSET_ROTATION
			PRINTLN("[LH][VANISH] 4")
			CLEAR_BIT(sFMMCmenu.iPropBitset, ciFMMC_PROP_Rotation_Override)
			CLEAR_BIT(sFMMCmenu.iPropBitset, ciFMMC_PROP_Position_Override)
			CLEAR_BIT(sFMMCmenu.iDynoPropBitset, ciFMMC_DYNOPROP_Position_Override)
		ENDIF
		
		IF sFMMCMenu.sActiveMenu != eFMMC_GENERIC_OVERRIDE_POSITION
		AND sFMMCMenu.sActiveMenu != eFmmc_GENERIC_OVERRIDE_ROTATION
		AND sFMMCMenu.sActiveMenu != eFmmc_PROP_ADVANCED_OPTIONS
		AND sFMMCMenu.sActiveMenu != eFmmc_SNAPPING_OPTIONS
		AND sFMMCMenu.sActiveMenu != eFMMC_OFFSET_POSITION
		AND sFMMCMenu.sActiveMenu != eFMMC_OFFSET_ROTATION
			PRINTLN("[JJT] sFMMCmenu.vOverridePosition changed 8")
			SET_OVERRIDE_POSITION_COORDS(sFMMCmenu, <<0,0,0>>)
			SET_OVERRIDE_ROTATION_VECTOR(sFMMCmenu, <<0,0,0>>)
			PRINTLN("[PROP ROTATION](0) Setting override rotation to zero ")
		ENDIF
		
		SET_LONG_BIT(sFMMCmenu.iBitActive, PAN_CAM_PLACE)
		IF DOES_BLIP_EXIST(bCameraTriggerBlip)
			REMOVE_BLIP(bCameraTriggerBlip)
		ENDIF
		
		SET_CREATION_TYPE(sFMMCMenu, CREATION_TYPE_NONE)
		
		sCurrentVarsStruct.fCheckPointSize = ciDEFAULT_CHECK_POINT_SIZE
		sHCS.hcCurrentCoronaColour = sHCS.hcDefaultCoronaColour	
		REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(sFMMCmenu.iEntityCreation)
	ELSE 
	
		PLAY_SOUND_FRONTEND(-1, "EDIT", GET_CREATOR_SPECIFIC_SOUND_SET())
			
		IF iTypePassed	=  CREATION_TYPE_VEHICLES		
			PRINTLN("IF iTypePassed	= CREATION_TYPE_VEHICLES")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_VEHICLE(sVehStruct, sFMMCMenu.iSelectedEntity)
			sVehStruct.iSwitchingINT 				= CREATION_STAGE_WAIT
		ELIF iTypePassed	=  CREATION_TYPE_WEAPONS
			PRINTLN("IF iTypePassed	= CREATION_TYPE_WEAPONS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_WEAPON(sFMMCmenu, sWepStruct, sFMMCMenu.iSelectedEntity, g_FMMC_STRUCT.iVehicleDeathmatch = 0, sCurrentVarsStruct)
			sWepStruct.iSwitchingINT 				= CREATION_STAGE_WAIT
		ELIF iTypePassed	=  CREATION_TYPE_PROPS
			PRINTLN("IF iTypePassed	= CREATION_TYPE_PROPS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_PROP(sPropStruct, sFMMCMenu.iSelectedEntity)
			SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
		ELIF iTypePassed	=  CREATION_TYPE_DYNOPROPS
			PRINTLN("IF iTypePassed	= CREATION_TYPE_DYNOPROPS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_DYNOPROP(sPropStruct, sDynoPropStruct, sFMMCMenu.iSelectedEntity)
			SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
		ELIF iTypePassed = CREATION_TYPE_PEDS
		
			PRINTLN("TypePassed	= CREATION_TYPE_PEDS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			IF DOES_ENTITY_EXIST(sPedStruct.piPed[sFMMCMenu.iSelectedEntity])	
				DELETE_PED(sPedStruct.piPed[sFMMCMenu.iSelectedEntity])
			ENDIF
			IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
				DELETE_PED(sPedStruct.piTempPed)
			ENDIF
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[sFMMCMenu.iSelectedEntity] = g_FMMC_STRUCT_ENTITIES.sEditedPed	
			
			REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sFMMCMenu.iSelectedEntity].mn)
			IF CREATE_PED_FMMC(sPedStruct.piPed[sFMMCMenu.iSelectedEntity], sFMMCMenu.iSelectedEntity, FALSE, FALSE)
				FMMC_SET_PED_VARIATION(sPedStruct.piPed[sFMMCMenu.iSelectedEntity], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sFMMCMenu.iSelectedEntity].mn, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sFMMCMenu.iSelectedEntity].iModelVariation)
				SET_ENTITY_COLLISION(sPedStruct.piPed[sFMMCMenu.iSelectedEntity] , TRUE)					
				CREATE_FMMC_BLIP(sPedStruct.biPedBlip[sFMMCMenu.iSelectedEntity], GET_ENTITY_COORDS(sPedStruct.piPed[sFMMCMenu.iSelectedEntity]), HUD_COLOUR_RED, "SC_BLP_ESP", 1)
				sPedStruct.iSwitchingINT = CREATION_STAGE_WAIT
			ENDIF
			IF g_FMMC_STRUCT_ENTITIES.iNumberOfPeds >= FMMC_MAX_PEDS
				IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
					DELETE_PED(sPedStruct.piTempPed)
				ENDIF
			ENDIF
			
			REFRESH_MENU(sFMMCmenu)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sEditedPed.mn)
		ENDIF	
		
	ENDIF

ENDPROC

FUNC enumFMMC_CIRCLE_MENU_ACTIONS RETURN_CIRCLE_MENU_SELECTIONS()
	IF LOAD_MENU_ASSETS()
		DRAW_MENU_SELECTION()
	ENDIF
	
	BOOL bSubmenuActive = NOT IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack) 
	
	HANDLE_EVERY_FRAME_MENU_FUNCTIONALITY()
	
	IF IS_SCREEN_FADED_IN()
		IF FFMC_IS_ACCEPT_JUST_PRESSED(sFMMCmenu) 
		OR (FMMC_IS_CIRCLE_MENU_A_CAMERA_MENU(sFMMCmenu.sActiveMenu) AND FMMC_IS_MAP_ACCEPT_JUST_PRESSED(sFMMCmenu))
			IF IS_THIS_OPTION_SELECTABLE()
				IF IS_THIS_OPTION_A_MENU_ACTION()
				OR IS_THIS_OPTION_A_MENU_GOTO() 
					IF (sFMMCmenu.sActiveMenu != eFmmc_PAN_CAM_BASE OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1)
					AND sFMMCmenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
						PLAY_SOUND_FRONTEND(-1, "SELECT", GET_SOUND_SET_FROM_CREATOR_TYPE())
						PRINTSTRING("CONFIRM SOUND EFFECT!!!")PRINTNL()
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Null_item
					PRINTSTRING("Set up menu")PRINTNL()
					//If it is then reset up the menu
					GO_TO_MENU(sFMMCmenu, sFMMCdata)
					IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
						IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
							sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
						ENDIF
					ENDIF
					IF sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
						IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
							sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
						ENDIF
					ENDIF
					CLEAR_BIT(sFMMCdata.iBitSet, bPerformingPlacementShapeTest)	
					
					RETURN eFmmc_Action_Null
				ENDIF
				
				IF sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Action_Null
						
					IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
					AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
						IF bInitialIntroCamSetup
							RETURN eFmmc_Action_Null
						ELIF GET_DISTANCE_BETWEEN_COORDS(g_FMMC_STRUCT.vStartPos, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 100
							PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
							PRINTSTRING("ERROR SOUND EFFECT")PRINTNL()
							sCurrentVarsStruct.bDisplayFailReason = TRUE
							sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
							sCurrentVarsStruct.sFailReason = "FMMC_ER_023"
							RETURN eFmmc_Action_Null
						ENDIF
					ENDIF
					
					RETURN  sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)]
				ENDIF
			ELSE
				PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
				PRINTSTRING("ERROR SOUND EFFECT")PRINTNL()
			ENDIF
			
		ENDIF
		
		IF FMMC_IS_CANCEL_JUST_PRESSED(sFMMCmenu)
		OR SHOULD_FORCE_MENU_BACKOUT(sFMMCMenu)
		
			sCurrentVarsStruct.bDisplayFailReason = FALSE					
			IF IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
				IF IS_BIT_SET(sFMMCdata.iBitSet, bResetMenu)
					CLEAR_BIT(sFMMCdata.iBitSet, bResetMenu)
				ENDIF
			ELSE
				sCurrentVarsStruct.bResetUpHelp = TRUE
				INT iSaveEntityType = sFMMCmenu.iEntityCreation
				
				IF IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack)
						
					CANCEL_ENTITY_CREATION(sFMMCmenu.iEntityCreation)
				ENDIF
				
				IF bSubmenuActive
					SET_CREATION_TYPE(sFMMCMenu, iSaveEntityType)
				ENDIF
				
				IF (sFMMCMenu.iSelectedEntity != -1 AND NOT bSubmenuActive)
					sFMMCMenu.iSelectedEntity = -1
					
				ELIF sFMMCmenu.sMenuBack != eFmmc_Null_item
					PLAY_SOUND_FRONTEND(-1, "BACK", GET_SOUND_SET_FROM_CREATOR_TYPE())
					GO_BACK_TO_MENU(sFMMCmenu)
					sFMMCmenu.sSubTypeName[CREATION_TYPE_CYCLE] = ""
				ENDIF
				IF sFMMCMenu.sActiveMenu = eFmmc_SNAPPING_OPTIONS
				OR sFMMCMenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS
					CHECK_IF_SNAPPING_SHOULD_BE_TURNED_OFF(sFMMCmenu)
				ENDIF
			ENDIF
			REFRESH_MENU(SFMMCMenu)
		ENDIF
		
		
		IF MENU_CONTROL_DOWN_CREATOR(menuScrollController)
			SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)+1)
			
			WHILE DEAL_WITH_SKIPPING_EMPTY_OPTIONS(1)
				PRINTLN("[TMS] Skipping options (down)...")
			ENDWHILE
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > sFMMCmenu.iCurrentMenuLength-1
				SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
			ENDIF
			IF sFMMCmenu.iCurrentMenuLength > 1
				PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", GET_SOUND_SET_FROM_CREATOR_TYPE())
				PRINTSTRING("SCROLL SOUND EFFECT!!!")PRINTNL()
			ENDIF
				
			IF NOT sFMMCendStage.bHasValidROS
				sFMMCendStage.bHasValidROS = NETWORK_HAS_VALID_ROS_CREDENTIALS()
			ENDIF
			
			sCurrentVarsStruct.bResetUpHelp  = TRUE
			
			REFRESH_MENU(sFMMCmenu)
		ENDIF
		
		IF MENU_CONTROL_UP_CREATOR(menuScrollController)
			SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)
			WHILE DEAL_WITH_SKIPPING_EMPTY_OPTIONS(-1)
				PRINTLN("[TMS] Skipping options (up)...")
			ENDWHILE
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < 0
				SET_CREATOR_MENU_SELECTION(sFMMCmenu, sFMMCmenu.iCurrentMenuLength - 1)
			ENDIF
			IF sFMMCmenu.iCurrentMenuLength > 1
				PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", GET_SOUND_SET_FROM_CREATOR_TYPE())
				PRINTSTRING("SCROLL SOUND EFFECT!!!")PRINTNL()
			ENDIF
			
			sCurrentVarsStruct.bResetUpHelp  = TRUE
			
			REFRESH_MENU(sFMMCmenu)
		ENDIF
		
		INT iChange = 0
		IF MENU_CONTROL_RIGHT_CREATOR(menuScrollController)
			iChange = 1
		ENDIF
		
		IF MENU_CONTROL_LEFT_CREATOR(menuScrollController)
			iChange = -1
		ENDIF
		
		IF iChange != 0
			EDIT_MENU_OPTIONS(iChange)
			REFRESH_MENU(sFMMCmenu)
		ENDIF
		
		HANDLE_SWITCH_TO_GROUND_CAMERA()
		
	ENDIF
	
	RETURN eFmmc_Action_Null
ENDFUNC

PROC HANDLE_MENU_STATE()
	SWITCH sCurrentVarsStruct.iMenuState
		CASE MENU_STATE_DEFAULT
			SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
			IF sFMMCMenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
				SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
				sCurrentVarsStruct.bFirstShapeTestCheckDone = FALSE
				sCurrentVarsStruct.sCoronaShapeTest.bDoStartShapeTest = FALSE
				sCurrentVarsStruct.sCoronaShapeTest.stiShapeTest  = NULL	
			ENDIF
			
			IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				IF SHOULD_MENU_BE_DRAWN(sFMMCData, sFMMCMenu, sCurrentVarsStruct, menuScrollController, 1 + g_FMMC_STRUCT.iVehicleDeathmatch, sFMMCMenu.sActiveMenu = eFmmc_TOP_MENU, bShowMenu)
					bShowMenuHighlight = TRUE
					IF MAINTAIN_FMMC_RADIO(sFMMCmenu, menuScrollController)
						sCurrentVarsStruct.bResetUpHelp = TRUE
						REFRESH_MENU(sFMMCMenu)
					ENDIF
					FMMC_DO_MENU_ACTIONS(RETURN_CIRCLE_MENU_SELECTIONS(), sCurrentVarsStruct, sFMMCmenu, sFMMCData, sFMMCendStage, sHCS)
					HANDLE_ENTITY_CREATION()
					SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME(2)
				ELSE
					sCurrentVarsStruct.bDiscVisible = FALSE
					
					sPedStruct.iSwitchingINT = CREATION_STAGE_WAIT
					sWepStruct.iSwitchingINT = CREATION_STAGE_WAIT
					sVehStruct.iSwitchingINT = CREATION_STAGE_WAIT
					SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
					
					IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
						DELETE_VEHICLE(sVehStruct.viCoronaVeh)
					ENDIF
					IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
						DELETE_OBJECT(sWepStruct.viCoronaWep)
					ENDIF
					IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
						DELETE_OBJECT(sPropStruct.viCoronaObj)
					ENDIF
					DELETE_CURRENT_PROP_TEMPLATE_OBJECTS(sPropStruct)
				ENDIF
			ENDIF
			
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND sCurrentVarsStruct.iMenuState != MENU_STATE_SWITCH_CAM
				BOOL bReloadMenu
				IF NOT IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
					DEAL_WITH_SKY_CAM_SWITCH(sFMMCdata,bSwitchingCam,vSwitchVec,sCamData, bReloadMenu)
				ELSE
					DEAL_WITH_PLAYER_WARP_TO_ITEM(sFMMCdata,bSwitchingCam,vSwitchVec,fSwitchHeading, bReloadMenu)
				ENDIF
				IF bReloadMenu
					REFRESH_MENU(sFMMCMenu)
				ENDIF
			ENDIF
			
			CHECK_FOR_MENU_SET_UP(sCurrentVarsStruct, sPedStruct, sVehStruct, sWepStruct, sObjStruct, sFMMCmenu)
			IF sCurrentVarsStruct.bResetUpHelp
			AND (sCurrentVarsStruct.bFirstShapeTestCheckDone OR sFMMCmenu.iEntityCreation = -1 OR sFMMCmenu.iEntityCreation = CREATION_TYPE_PAN_CAM)
				sCurrentVarsStruct.bResetUpHelp  = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_PLACE_CAM
			IF PPS.iStage != WAIT_PREVIEW_PHOTO
			AND PPS.iStage != CLEANUP_PREVIEW_PHOTO
			AND sFMMCendStage.sTakePhotoVars.iTakePhotoStage != ciFMMC_TAKE_PHOTO_STAGE_OPEN_SHUT
				PPS.iStage = CLEANUP_PREVIEW_PHOTO
				PPS.iPreviewPhotoDelayCleanup = 0
			ENDIF
			
			IF sFMMCMenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE							
				REMOVE_DECALS_FOR_PHOTOS(sFMMCendStage.sTakePhotoVars.iTakePhotoStage, sStartEndBlips, sPedStruct, sTeamSpawnStruct, sWepStruct, sObjStruct, sHCS.hcStartCoronaColour)
			ENDIF
			IF TAKE_PHOTO_FOR_FMMC(sFMMCendStage.sTakePhotoVars, SF_Movie_Gallery_Shutter_Index, sFMMCMenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE)
				IF sFMMCMenu.sActiveMenu = eFmmc_PAN_CAM_BASE
					g_FMMC_STRUCT.vCameraPanPos = GET_CAM_COORD(GET_RENDERING_CAM())
					g_FMMC_STRUCT.vCameraPanRot = GET_CAM_ROT(GET_RENDERING_CAM())
					VECTOR vRot
					vRot = GET_CAM_ROT(GET_RENDERING_CAM())
					g_FMMC_STRUCT.fCameraPanHead = vRot.z
					PRINTLN("PAN CAM POSITIONED Rot - ", g_FMMC_STRUCT.vCameraPanRot, " Vector - ", g_FMMC_STRUCT.vCameraPanPos)
				ENDIF
				sFMMCendStage.bMajorEditOnLoadedMission = TRUE
				REFRESH_MENU(sFMMCMenu)
				sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
			ENDIF
		BREAK
		
		CASE MENU_STATE_PAN_CAM
			IF DEAL_WITH_CREATOR_CAMERA_PAN_PREVIEW(iCamPanState, jobIntroData, vInitialPreviewPos)	
				IF !SCRIPT_IS_CLOUD_AVAILABLE()
					IF NOT IS_SCREEN_FADED_IN()
						DO_SCREEN_FADE_IN(500)
					ENDIF	
				ENDIF
				sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
			ENDIF
		BREAK
		
		CASE MENU_STATE_TITLE
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, TRUE)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCMenu)
					bDelayAFrame = TRUE
				ELSE							
					bShowMenuHighlight = FALSE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_DESCRIPTION
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, FALSE)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCMenu)
					bDelayAFrame = TRUE
				ELSE							
					bShowMenuHighlight = FALSE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_TAGS
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, FALSE, TRUE)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCMenu)
					bDelayAFrame = TRUE
				ELSE							
					bShowMenuHighlight = FALSE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_SWITCH_CAM
			IF NOT IS_SCREEN_FADED_OUT()
				PRINTSTRING("screen not faded out yet, calling fade out")PRINTNL()
				DO_SCREEN_FADE_OUT(500)								
				IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
					SET_ENTITY_ALPHA(sWepStruct.viCoronaWep, 0, FALSE)
				ENDIF
				IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
					SET_ENTITY_ALPHA(sPropStruct.viCoronaObj, 0, FALSE)
				ENDIF
				IF DOES_ENTITY_EXIST(sDynoPropStruct.viCoronaObj)
					SET_ENTITY_ALPHA(sDynoPropStruct.viCoronaObj, 0, FALSE)
				ENDIF
				IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
					SET_ENTITY_ALPHA(sVehStruct.viCoronaVeh, 0, FALSE)
				ENDIF
			ELSE
				PRINTSTRING("Screen is faded out do swap cams")PRINTNL()
				IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct,TRUE,FALSE,TRUE,FALSE)
				ELSE
					SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct,FALSE,FALSE,TRUE,FALSE)
				ENDIF
				NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 20)
				sCurrentVarsStruct.iMenuState = MENU_STATE_LOADING_AREA
				iLoadingOverrideTimer = GET_GAME_TIMER()
			ENDIF
		BREAK
		
		CASE MENU_STATE_LOADING_AREA
			IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
			OR IS_NEW_LOAD_SCENE_LOADED()	
			OR iLoadingOverrideTimer + 8000 < GET_GAME_TIMER()
			
				PRINTSTRING("LOADED, FADE IN")PRINTNL()
				IF NOT bSwitchingCam
					DO_SCREEN_FADE_IN(500)
				ENDIF
				NEW_LOAD_SCENE_STOP()
				IF IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				ELSE
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				ENDIF
				
				IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
					RESET_ENTITY_ALPHA(sWepStruct.viCoronaWep)
				ENDIF
				IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
					RESET_ENTITY_ALPHA(sPropStruct.viCoronaObj)
				ENDIF
				IF DOES_ENTITY_EXIST(sDynoPropStruct.viCoronaObj)
					RESET_ENTITY_ALPHA(sDynoPropStruct.viCoronaObj)
				ENDIF
				IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
					RESET_ENTITY_ALPHA(sVehStruct.viCoronaVeh)
				ENDIF								
				
				sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT

			ELSE							
				PRINTSTRING("WAITING FOR SCEEN TO LOAD")PRINTNL()
			ENDIF
		BREAK
		
		CASE MENU_STATE_TEST_MISSION
			//Empty
		BREAK
		
		CASE MENU_STATE_CUSTOM_TEMPLATE_NAME
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_CUSTOM_TEMPLATE_NAME(sFMMCendStage, g_FMMC_STRUCT.iNumberOfPropTemplates-1)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_WARP_TO_INTERIOR
			HANDLE_PLAYER_INTERIOR_WARP(iWarpState, iWarpStateTimeoutStart, sFMMCmenu, sFMMCdata, sCamData, sCurrentVarsStruct)
		BREAK
		
	ENDSWITCH
ENDPROC

PROC MENU_INITIALISATION()
	sFMMCmenu.sSubTypeName[CREATION_TYPE_WEAPONS] 					= GET_CREATOR_NAME_FOR_PICKUP_TYPE(GET_PICKUP_TYPE_FROM_WEAPON_TYPE(sWepStruct.wtGunType[0]))
	sFMMCmenu.sSubTypeName[CREATION_TYPE_PROPS]             		= GET_CREATOR_NAME_FOR_PROP_MODEL(GET_CREATOR_PROP_MODEL(sFMMCmenu.iPropLibrary, sFMMCmenu.iPropType))
	sFMMCmenu.sSubTypeName[CREATION_TYPE_DYNOPROPS]         		= GET_CREATOR_NAME_FOR_PROP_MODEL(GET_CREATOR_PROP_MODEL(sFMMCmenu.iPropLibrary, sFMMCmenu.iPropType))
	
	//CHECK_FOR_END_CONDITIONS_MET()
	
	SET_ALL_MENU_ITEMS_ACTIVE()
	
	REFRESH_MENU(sFMMCMenu)
		
ENDPROC

PROC SET_STARTING_MENUS_ACTIVE()
//	SET_BIT(sFMMCmenu.iBitActive, START_MENU_ITEM_DM)
//	SET_BIT(sFMMCmenu.iBitActive, MAP_MENU_ITEM_DM)
	
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_CREATOR)
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_RADIO)
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_OPTIONS)
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_EXIT)
ENDPROC

PROC HANDLE_MOUSE_ENTITY_SELECTION_CANCEL_AND_SWITCH(eFMMC_MENU_ENUM itemMenu, INT iNewItemIndex, INT iMaxItems)
	// cancel out of stuff
	CANCEL_ENTITY_CREATION(sFMMCmenu.iEntityCreation) 		
	IF sFMMCMenu.iSelectedEntity != -1	
		sFMMCMenu.iSelectedEntity = -1
	ENDIF

	// force the menu to be the one that we want
	GO_TO_MENU(sFMMCMenu, sFMMCdata, itemMenu)
	REFRESH_MENU(sFMMCMenu)

	// instead of looping through until the switch cam increases we force it
	sFMMCmenu.iSwitchCam = iNewItemIndex - 1
	DEAL_WITH_CAMERA_SWITCH_SELECTION(TRUE, iMaxItems)
	SET_CURSOR_POSITION(0.5, 0.5)
	PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", GET_SOUND_SET_FROM_CREATOR_TYPE())			
ENDPROC

PROC HANDLE_MOUSE_ENTITY_SELECTION(BOOL bTesting = FALSE)

	BOOL bMouseOverCorona = FALSE
	
	IF NOT FMMC_DO_HANDLE_MOUSE_SELECTION_CHECK(sFMMCmenu, sCurrentVarsStruct, bMouseOverCorona, bTesting)
		EXIT
	ENDIF
	
	FP_FMMC_MOUSE_ENTITY_SELECT_HANDLER funcPtr = &HANDLE_MOUSE_ENTITY_SELECTION_CANCEL_AND_SWITCH
	
	// check objects
	IF FMMC_HANDLE_MOUSE_DYNOPROP_SELECTION(sFMMCmenu, sDynoPropStruct, sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_PROP_SELECTION(sFMMCmenu, sPropStruct, sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	 
	IF FMMC_HANDLE_MOUSE_SPAWN_POINT_SELECTION(sFMMCmenu, sPedStruct, sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_TEAM_0_SPAWN_POINT_SELECTION(sFMMCmenu, sTeamSpawnStruct[0], sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_TEAM_1_SPAWN_POINT_SELECTION(sFMMCmenu, sTeamSpawnStruct[1], sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_TEAM_2_SPAWN_POINT_SELECTION(sFMMCmenu, sTeamSpawnStruct[2], sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_TEAM_3_SPAWN_POINT_SELECTION(sFMMCmenu, sTeamSpawnStruct[3], sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_VEHICLE_SELECTION(sFMMCmenu, sVehStruct, sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_WEAPON_SELECTION(sFMMCmenu, sWepStruct, sCurrentVarsStruct, sInvisibleObjects, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF (bMouseOverCorona = FALSE) AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, GET_CREATOR_CAMERA_ORBIT_TOGGLE_BUTTON())
		FMMC_SET_CURSOR_MAP_ACCEPT_BIT(sFMMCmenu, FALSE)
	ENDIF
ENDPROC

PROC DO_END_MENU()
	
	IF NOT IS_SCREEN_FADING_OUT()
		IF NOT IS_SCREEN_FADED_OUT()
			IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				DO_SCREEN_FADE_OUT(500)	
				SET_CREATOR_AUDIO(FALSE, TRUE)
			ENDIF
		ELSE

			DRAW_END_MENU()
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				sCurrentVarsStruct.bSwitching = FALSE
				sCurrentVarsStruct.stiScreenCentreLOS = NULL
				DO_SCREEN_FADE_IN(500)
				SET_CREATOR_AUDIO(TRUE, TRUE)
			ENDIF
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				SET_ENTITY_CREATION_STATUS(STAGE_DEAL_WITH_FINISHING)
			ENDIF
				
		ENDIF
	ENDIF
	
ENDPROC

PROC SET_RESET_STATE(RESET_STATE eNewState)
	PRINTLN("SET_RESET_STATE - Changing iResetState from ", iResetState, " to ", eNewState)
	iResetState = eNewState
ENDPROC

FUNC BOOL RESET_THE_MISSION_UP_SAFELY()

	INT i

	SWITCH iResetState
		CASE RESET_STATE_FADE
			PRINTLN("RESET_STATE_FADE")
			IF IS_FAKE_MULTIPLAYER_MODE_SET() = TRUE 
				
				SET_OVERRIDE_WEATHER("CLEAR")
				
			    IF SCRIPT_IS_CLOUD_AVAILABLE()
					PRINTLN("SCRIPT_IS_CLOUD_AVAILABLE()")
				    SET_FAKE_MULTIPLAYER_MODE(FALSE)
				ELSE
					PRINTLN("NOT SCRIPT_IS_CLOUD_AVAILABLE()")
					//NETWORK_SESSION_LEAVE_SINGLE_PLAYER() 
					g_Private_IsMultiplayerCreatorRunning = FALSE
					g_Private_MultiplayerCreatorNeedsToEnd = TRUE
				ENDIF
			ENDIF
			IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE 
				SET_RESET_STATE(RESET_STATE_CLEAR)
				RESET_INVISIBLE_OBJECT_POSITIONS(sInvisibleObjects)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())				
					SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT)
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)	
				ENDIF
				
				IF bOnGroundBeforeTest = FALSE
					CLEAR_BIT(sFMMCdata.iBitSet, bCameraActive)
					PRINTLN("CLEAR_BIT(sFMMCdata.iBitSet, bCameraActive) GO BACK TO CAMERA")
				ELSE
					SET_BIT(sFMMCdata.iBitSet, bCameraActive)	
					PRINTLN("SET_BIT(sFMMCdata.iBitSet, bCameraActive) GO BACK TO GROUND")
				ENDIF
				SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct, FALSE, FALSE, FALSE)
			ENDIF
			
		BREAK
		
		CASE RESET_STATE_CLEAR
			PRINTLN("RESET_STATE_CLEAR")
			IF (NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			AND NOT NETWORK_IS_GAME_IN_PROGRESS())
			OR NOT SCRIPT_IS_CLOUD_AVAILABLE()
				FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1)
					IF DOES_BLIP_EXIST(sVehStruct.biVehicleBlip[i])
						REMOVE_BLIP(sVehStruct.biVehicleBlip[i])
					ENDIF
					IF DOES_ENTITY_EXIST(sVehStruct.veVehcile[i])
						SET_ENTITY_AS_MISSION_ENTITY(sVehStruct.veVehcile[i], TRUE, TRUE)
						DELETE_VEHICLE(sVehStruct.veVehcile[i])
					ENDIF
				ENDFOR				
				FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfProps - 1)
					IF DOES_BLIP_EXIST(sPropStruct.biObject[i])
						REMOVE_BLIP(sPropStruct.biObject[i])
					ENDIF
					if DOES_ENTITY_EXIST(sPropStruct.oiObject[i])
						DELETE_OBJECT(sPropStruct.oiObject[i])
					ENDIF
				ENDFOR						
				FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps - 1)
					IF DOES_BLIP_EXIST(sDynoPropStruct.biObject[i])
						REMOVE_BLIP(sDynoPropStruct.biObject[i])
					ENDIF
					if DOES_ENTITY_EXIST(sDynoPropStruct.oiObject[i])
						DELETE_OBJECT(sDynoPropStruct.oiObject[i])
					ENDIF
				ENDFOR	
				FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons - 1)
					IF DOES_PICKUP_EXIST(sWepStruct.Pickups[i])
						REMOVE_PICKUP(sWepStruct.Pickups[i])
					ENDIF
					IF DOES_BLIP_EXIST(sWepStruct.biWeaponBlip[i])
						REMOVE_BLIP(sWepStruct.biWeaponBlip[i])
					ENDIF
					if DOES_ENTITY_EXIST(sWepStruct.oiWeapon[i])
						DELETE_OBJECT(sWepStruct.oiWeapon[i])
					ENDIF
					IF sWepStruct.iDecalNum[i] != -1
						IF IS_DECAL_ALIVE(sWepStruct.diDecal[sWepStruct.iDecalNum[i]])
							REMOVE_DECAL(sWepStruct.diDecal[sWepStruct.iDecalNum[i]])
						ENDIF
					ENDIF
				ENDFOR
				IF DOES_BLIP_EXIST(sStartEndBlips.biStart)
					REMOVE_BLIP(sStartEndBlips.biStart)
				ENDIF							
				IF sStartEndBlips.ciStartType != NULL
					DELETE_CHECKPOINT(sStartEndBlips.ciStartType)
					sStartEndBlips.ciStartType = NULL
				ENDIF
				SET_RESET_STATE(RESET_STATE_WEAPONS)
			ENDIF
		BREAK
		
		CASE RESET_STATE_WEAPONS
			PRINTLN("RESET_STATE_WEAPONS")
			IF CREATE_ALL_CURRENT_PICKUPS(sWepStruct, g_FMMC_STRUCT.iVehicleDeathmatch = 0, sCurrentVarsStruct)
				SET_RESET_STATE(RESET_STATE_PROPS)
			ENDIF
		BREAK
		
		CASE RESET_STATE_PROPS
			IF CREATE_ALL_CREATOR_PROPS(sPropStruct, iPropModelTimers)
				SET_RESET_STATE(RESET_STATE_DYNOPROPS)
			ENDIF
		BREAK
		
		CASE RESET_STATE_DYNOPROPS
			IF CREATE_ALL_CREATOR_DYNOPROPS(sDynoPropStruct)
				SET_RESET_STATE(RESET_STATE_VEHICLES)
			ENDIF
		BREAK
		
		CASE RESET_STATE_VEHICLES
			PRINTLN("RESET_STATE_VEHICLES")
			IF CREATE_ALL_CREATOR_VEHICLES(sVehStruct, iVehicleModelLoadTimers)
				SET_RESET_STATE(RESET_STATE_ATTACH)
			ENDIF			
		BREAK
		
		CASE RESET_STATE_ATTACH
			PRINTLN("RESET_STATE_ATTACH")
			FOR i = 0 TO FMMC_MAX_VEHICLES-1
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParent >= 0
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParentType = CREATION_TYPE_VEHICLES
						FMMC_ATTACH_VEHICLE_TO_VEHICLE(sVehStruct.veVehcile[i], sVehStruct.veVehcile[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParent])
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParentType = CREATION_TYPE_OBJECTS
						FMMC_ATTACH_VEHICLE_TO_OBJECT(sVehStruct.veVehcile[i], sObjStruct.oiObject[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParent])
					ENDIF
				ENDIF
			ENDFOR
			SET_RESET_STATE(RESET_STATE_OTHER)
		BREAK
		
		CASE RESET_STATE_OTHER
			PRINTLN("RESET_STATE_OTHER")
			
			REQUEST_STREAMED_TEXTURE_DICT("MPMissMarkers256")
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPMissMarkers256")
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
					SET_ALL_MENU_ITEMS_ACTIVE()
					
					CREATE_TRIGGER_BLIP_AND_CORONA_WITH_DECAL(sStartEndBlips.biStart, sHCS.hcStartCoronaColour, sStartEndBlips.ciStartType)
					
				ELSE
					SET_STARTING_MENUS_ACTIVE()
				ENDIF
				
				SET_RESET_STATE(RESET_STATE_FINISH)
			ENDIF
			
		BREAK
		CASE RESET_STATE_FINISH
			PRINTLN("RESET_STATE_FINISH")
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC WARP_CAMERA_TO_START_LOCATION(FMMC_CAM_DATA &sCamDataPassed, FLOAT fHeight = 40.0)
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
		IF DOES_CAM_EXIST(sCamDataPassed.cam)
			SET_CAM_COORD(sCamDataPassed.cam, g_FMMC_STRUCT.vStartPos+<<0.0,0.0, fHeight>>)
		ENDIF
	ELIF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[0].vPos)
		IF DOES_CAM_EXIST(sCamDataPassed.cam)
			SET_CAM_COORD(sCamDataPassed.cam, g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[0].vPos+<<0.0,0.0, fHeight>>)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL PREPARE_CONTENT_TO_BE_SAVED_OR_PUBLISHED()
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciSavedWithNewWeaponData)
	
	sCurrentVarsStruct.creationStats.bSuccessfulSave = FALSE
	
	PRINTLN("PRE PUBLISH/SAVE DONE. Time to go for it")
	
	RETURN TRUE
ENDFUNC

PROC SET_CORONA_ENTITIES_TO_DUMMY_MODELS()
	INT i
	FOR i = g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles TO FMMC_MAX_VEHICLES - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("SET TO DUMMY VEHICLE MODEL ", i)
		ENDIF
	ENDFOR
	FOR i = g_FMMC_STRUCT_ENTITIES.iNumberOfProps TO GET_FMMC_MAX_NUM_PROPS() - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("SET TO DUMMY PROP MODEL ", i)
		ENDIF
	ENDFOR
	FOR i = g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps TO FMMC_MAX_NUM_DYNOPROPS - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("SET TO DUMMY DYNOPROP MODEL ", i)
		ENDIF
	ENDFOR
ENDPROC

PROC RESET_TESTING_CONTENT_MENU_DATA(STRUCT_TEST_MENU_DATA &structMenuData)

	STRUCT_TEST_MENU_DATA sTemp
	structMenuData = sTemp
	
	NET_PRINT("[CREATOR] - RESET_TESTING_CONTENT_MENU_DATA() has been called.")NET_NL()
	
ENDPROC

PROC DEAL_WITH_TEST_BEING_ACTIVE(STRUCT_TEST_MENU_DATA &structMenuData)
	
	// Load menu.
	IF LOAD_MENU_ASSETS()
		
		// Process menu state.
		SWITCH structMenuData.eTestMcMissionMenuState
			
			// Wait for player to press up to activate menu. While waiting display help informing player they can do this.
			CASE eTESTINGMCMISSIONMENUSTATE_DISPLAY_HELP				
				
				IF NOT IS_PHONE_ONSCREEN()
					IF NOT structMenuData.bDisplayedHelp
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							NET_PRINT("[CREATOR] -called PRINT_HELP_FOREVER(MC_TEST3)")NET_NL()
							PRINT_HELP_FOREVER("MC_TEST")
							structMenuData.bDisplayedHelp = TRUE
						ENDIF
					ENDIF
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
						DISABLE_CELLPHONE(TRUE)
						structMenuData.eTestMcMissionMenuState = eTESTINGMCMISSIONMENUSTATE_SETUP_MENU
						NET_PRINT("[CREATOR] - pressed INPUT_FRONTEND_UP, going to eTESTINGMCMISSIONMENUSTATE_SETUP_MENU")NET_NL()
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_TEST")
						CLEAR_HELP()
						structMenuData.bDisplayedHelp = FALSE
					ENDIF
				ENDIF
				
			BREAK
			
			// Setup the menu.
			CASE eTESTINGMCMISSIONMENUSTATE_SETUP_MENU
				
				CLEAR_MENU_DATA()
				SET_MENU_TITLE("FMMCC_TTITLE")
				SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
				SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
				ADD_MENU_ITEM_TEXT(0, "MC_TEST2")
				
				structMenuData.eTestMcMissionMenuState = eTESTINGMCMISSIONMENUSTATE_SHOW_MENU
				
				NET_PRINT("[CREATOR] - menu setup, going to eTESTINGMCMISSIONMENUSTATE_SHOW_MENU")NET_NL()
				
			BREAK
			
			// While menu is showing, wait for inputs from player to either exit the menu or exit testing the mission.
			CASE eTESTINGMCMISSIONMENUSTATE_SHOW_MENU
				
				SET_CURRENT_MENU_ITEM_DESCRIPTION("FMMCC_EXTEST")
				// Draw the menu.
				DRAW_MENU()
				
				IF NOT IS_PAUSE_MENU_ACTIVE() //BC: 1789172 where the exit test menu buttons are listening with the pause menu active. 
					// Check for input.
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
						DISABLE_CELLPHONE(FALSE)
						RESET_TESTING_CONTENT_MENU_DATA(structMenuData)
						NET_PRINT("[CREATOR] - pressed INPUT_FRONTEND_UP, going to eTESTINGMCMISSIONMENUSTATE_DISPLAY_HELP")NET_NL()
					ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						RESET_TESTING_CONTENT_MENU_DATA(structMenuData)
						REFRESH_MENU(sFMMCmenu)
						g_FMMC_STRUCT.g_b_QuitTest = TRUE
						NET_PRINT("[CREATOR] - pressed INPUT_FRONTEND_ACCEPT, setting g_FMMC_STRUCT.g_b_QuitTest = TRUE.")NET_NL()
					ENDIF
				ENDIF
				
			BREAK
			
		ENDSWITCH
		
	ELSE
		NET_PRINT("[CREATOR] - waiting for LOAD_MENU_ASSETS() to return TRUE.")NET_NL()
	ENDIF
	
ENDPROC

PROC MAINTAIN_TEST_MODE_STATE()

	EXIT
	
	IF IS_BIT_SET(sFMMCdata.iBitSet, biSetUpTestMission)
		bTextSetUp = FALSE
		IF NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(500)
			SET_TEAM_SPAWN_VEHICLE_DATA_FROM_CREATOR_SELECTION(sVehStruct)
			COMMON_FMMC_SCRIPT_CLEAN_UP(sPedStruct,	sVehStruct,	sWepStruct,	sObjStruct,sPropStruct, sDynoPropStruct, sInvisibleObjects, sCurrentVarsStruct, sFMMCmenu, FALSE)			
		//	DELETE_BLIPS_AND_CHECKPOINTS()
			REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(sFMMCmenu.iEntityCreation)
			SET_CORONA_ENTITIES_TO_DUMMY_MODELS()
		ENDIF
		IF IS_SCREEN_FADED_OUT()
			IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE
				SET_FAKE_MULTIPLAYER_MODE(TRUE)
			ENDIF
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("sControllerName")) = 0
				REQUEST_SCRIPT("sControllerName")				
				IF HAS_SCRIPT_LOADED("sControllerName")
				AND IS_FAKE_MULTIPLAYER_MODE_SET()	
				AND NETWORK_IS_GAME_IN_PROGRESS()
					GlobalplayerBD[0].iGameState = MAIN_GAME_STATE_RUNNING
					START_NEW_SCRIPT("sControllerName", MULTIPLAYER_FREEMODE_STACK_SIZE)
					PRINTLN("----------------------------------------------------------------------------")
					PRINTLN("--                                                                        --")
					PRINTLN("--                                                                        --")
					PRINTLN("--                                                                        --")
					PRINTLN("--                 LAUNCHING sControllerName				     	       --")
					PRINTLN("--                                                                        --")
					PRINTLN("--                                                                        --")
					PRINTLN("--                                                                        --")
					PRINTLN("----------------------------------------------------------------------------")
					SET_SCRIPT_AS_NO_LONGER_NEEDED("sControllerName")
					
					SET_BIT(sFMMCdata.iBitSet, biSetUpTestMission)
					iDoorSetupStage = 0
					REMOVE_SCENARIO_BLOCKING_AREAS()
					SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(FALSE)
					SET_MOBILE_PHONE_RADIO_STATE(FALSE)
					IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
						SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct, TRUE, TRUE)
					ENDIF
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
						SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), FALSE)
						SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
					ENDIF
					CLEAR_BIT(sFMMCdata.iBitSet, biSetUpTestMission)
					SET_CREATOR_AUDIO(FALSE, FALSE)
					g_FMMC_STRUCT.g_b_QuitTest = FALSE
					bMpNeedsCleanedUp = TRUE
					bTextSetUp = FALSE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF bTextSetUp = FALSE
				Initialise_MP_Objective_Text()
				Initialise_MP_Communications()
				NET_PRINT("init objective text and comms called") NET_NL()
				bTextSetUp = TRUE
			ENDIF
			SET_PED_UNABLE_TO_DROWN(PLAYER_PED_ID())
		ENDIF
		IF bTestModeControllerScriptStarted = FALSE
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("sControllerName")) >= 1
				PRINTLN("----------------------------------------------------------------------------")
				PRINTLN("--                                                                        --")
				PRINTLN("--       GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH          --")
				PRINTLN("--                                                                        --")
				PRINTLN("----------------------------------------------------------------------------")
				bTestModeControllerScriptStarted = TRUE
				IF DOES_BLIP_EXIST(sStartEndBlips.biStart)
					REMOVE_BLIP(sStartEndBlips.biStart)
				ENDIF
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				ENDIF
				DISABLE_CELLPHONE(FALSE)
			ENDIF
		ELSE
			IF bMpNeedsCleanedUp = TRUE
				IF bMpModeCleanedUp = FALSE
					// Do menu here.
					DEAL_WITH_TEST_BEING_ACTIVE(structTestMcMissionMenuData)
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("sControllerName")) = 0
						RESET_TESTING_CONTENT_MENU_DATA(structTestMcMissionMenuData)
						SET_FAKE_MULTIPLAYER_MODE(FALSE)
						IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE
							IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
							AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
								SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							ENDIF
							CLEANUP_LEADERBOARD_CAM()
							REFRESH_MENU(sFMMCmenu)
							bMpModeCleanedUp = TRUE
							// Turn off the leaderboard camera.
							CLEANUP_LEADERBOARD_CAM()
							IF DOES_CAM_EXIST(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
								IF IS_CAM_ACTIVE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
									IF IS_CAM_RENDERING(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
										RENDER_SCRIPT_CAMS(FALSE, FALSE, 1000, FALSE, TRUE)
										SET_CAM_ACTIVE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam, FALSE)
									ENDIF
								ENDIF
								DESTROY_CAM(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam, TRUE)
							ENDIF
							
							PRINTLN("----------------------------------------------------------------------------")
							PRINTLN("--                                                                        --")
							PRINTLN("--                       MODE CLEANED UP = TRUE                           --")
							PRINTLN("--                                                                        --")
							PRINTLN("----------------------------------------------------------------------------")
						ENDIF
					ENDIF
				ELSE
					IF NETWORK_IS_GAME_IN_PROGRESS() = FALSE
						SETUP_FMMC_SCRIPT(sFMMCMenu, sHCS, sCurrentVarsStruct, TRUE)
						CLEAR_BIT(sFMMCdata.iBitSet, biTestMissionActive)
						CLEAR_BIT(sFMMCdata.iBitSet, bTestHelpTextDone)
						bMpNeedsCleanedUp = FALSE
						bTestModeControllerScriptStarted = FALSE
						bTextSetUp = FALSE
						bMpModeCleanedUp = FALSE
						SET_CREATOR_AUDIO(TRUE, FALSE)
						SET_ENTITY_CREATION_STATUS(STAGE_DEAL_WITH_RECREATING_MISSION)
						sCurrentVarsStruct.creationStats.iTimesTestedLoc++
						sCurrentVarsStruct.creationStats.bMadeAChange = FALSE	
						ANIMPOSTFX_STOP_ALL()
					ELSE
						PRINTLN("Mission creator - MAINTAIN_TEST_MISSION_STATE - NETWORK_IS_GAME_IN_PROGRESS() = TRUE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC BUTTON_HELP()

	REMOVE_MENU_HELP_KEYS()
	
	INT iBitSet
	
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
		
		//Left & Right
		IF IS_THIS_OPTION_SELECTABLE()
		AND NOT IS_THIS_OPTION_A_MENU_GOTO()
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_CYCLE_OPTIONS)
		ENDIF
		
		//A
		IF IS_BIT_SET(iHelpBitSet, biPickupEntityButton)
		AND sFMMCMenu.iSelectedEntity = -1
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_EDIT)
		ELSE
			IF IS_THIS_OPTION_SELECTABLE()
				IF IS_THIS_OPTION_A_MENU_GOTO()
				OR IS_THIS_OPTION_A_MENU_ACTION()
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
				ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
					IF sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE
					AND sCurrentVarsStruct.bAreaIsGoodForPlacement
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
					ENDIF
				ELIF NOT IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciDRAW_CORONA)
				OR sFMMCMenu.iSelectedEntity != -1
					IF sFMMCmenu.sActiveMenu != eFmmc_RADIO_MENU
					AND sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE
					AND sCurrentVarsStruct.bAreaIsGoodForPlacement
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		//B
		IF sFMMCmenu.sMenuBack != eFmmc_Null_item
		OR sFMMCMenu.iBoundsEditPointIndex > -1
			IF sFMMCMenu.iSelectedEntity = -1
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
			ELSE
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_CANCEL)
			ENDIF
		ENDIF
		//X
		IF IS_BIT_SET(iHelpBitSet, biDeleteEntityButton)
		AND CAN_DELETE_FROM_CURRENT_MENU(sFMMCMenu, sCurrentVarsStruct)
		AND sFMMCMenu.iSelectedEntity = -1
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
		ENDIF
		
		IF GET_HIGHLIGHTED_WORLD_PROP_INDEX(sCurrentVarsStruct.vCoronaHitEntity, sCurrentVarsStruct.vCoronaPos) != -1
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
		ENDIF
				
		IF sCurrentVarsStruct.bBlockDelete
			CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
		ENDIF
		
		//Y
		
		//Bumpers
		IF sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE
		OR sFMMCMenu.iSelectedEntity != -1
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_ROTATE)
		ENDIF
		//Triggers
		IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_ZOOM)
		ENDIF
		
		//Misc
		IF sFMMCmenu.iCurrentMenuLength > 1
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_SCROLL)
		ENDIF
		IF (sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE) AND (sFMMCMenu.sActiveMenu != eFmmc_RADIO_MENU)
		AND sFmmcMenu.sActiveMenu != eFmmc_PAN_CAM_BASE
		AND sFmmcMenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
			IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				IF IS_BIT_SET(iHelpBitSet, biWarpToCameraButton) 
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_SWITCH_CAM)
				ENDIF
			ELSE
				IF sFMMCMenu.iSelectedEntity = -1
					IF IS_BIT_SET(iHelpBitSet, biWarpToCoronaButton) 
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_SWITCH_CAM)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
		OR sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
			CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_ZOOM)
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_CAMERA_HEIGHT)
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_CAMERA_FOV)
			IF DOES_CAM_EXIST(sCamData.cam)
			AND GET_CAM_FOV(sCamData.cam) != 40
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_RESET_FOV)		
			ENDIF
		ENDIF
		
		IF NOT FMMC_IS_CIRCLE_MENU_A_CAMERA_MENU(sFMMCmenu.sActiveMenu) 
			IF NOT IS_GAMEPLAY_CAM_RENDERING()
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_ROTATE_CAM_HOLD)		
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_MOVE_CAMERA)
			ENDIF
		ENDIF
		
		SET_UP_PROP_HELP_BUTTONS(sFMMCmenu, iBitSet, sCurrentVarsStruct)
			
		IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
			CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
		ENDIF
		
		CREATE_FMMC_INSTRUCTIONAL_BUTTONS(sFMMCmenu, iBitSet)
	ELSE
		IF sFMMCmenu.sMenuBack != eFmmc_Null_item
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1 
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
			ENDIF
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_SCROLL)
			CREATE_FMMC_INSTRUCTIONAL_BUTTONS(sFMMCmenu, iBitSet)
		ENDIF
	ENDIF
ENDPROC

SCRIPT

	IF IS_PAUSE_MENU_REQUESTING_TO_EDIT_A_MISSION()
		PRINTLN("[JA@PAUSEMENU] Set up which file to load into creator", GET_PAUSE_MENU_MISSION_FILE_TO_LOAD())

		sFMMCendStage.iLoadDeleteStage  = ciLOAD_DELETE_STAGE_LOAD
		sFMMCendStage.iMenuReturn		= GET_PAUSE_MENU_MISSION_FILE_TO_LOAD()
	ENDIF
	CLEAN_UP_PAUSE_MENU_MISSION_CREATOR_DATA()
	
	PROCESS_PRE_GAME()	
	
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP))
		SCRIPT_CLEANUP(TRUE)
	ENDIF	
	
	// Used for the model memory budget. Must be initiated at the start.
	INIT_CREATOR_BUDGET()
	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
//	#IF IS_DEBUG_BUILD
//		CREATE_WIDGETS()
//	#ENDIF	
	
	// Main loop.
	WHILE TRUE	
	
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 tl63
		tl63 = "Current active menu: "
		tl63 += ENUM_TO_INT(sFMMCMenu.sActiveMenu)
		DRAW_DEBUG_TEXT_2D(tl63, <<0.8,0.8,0.0>>)
		#ENDIF
		
		PROCESS_SINGLEPLAYER_MODEL_ON_CREATOR(bIsCreatorModelSetSPTC, bIsMaleSPTC)
		FMMC_FAKE_LEFT_AND_RIGHT_INPUTS_FOR_MENUS(sFMMCmenu)
		
		sCurrentVarsStruct.bDiscVisible = FALSE
		sCurrentVarsStruct.bCanCreateADecalThisFrame = TRUE
		
		WAIT(0)
		
		BUTTON_HELP()
		
		IF NOT bTestModeControllerScriptStarted
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			DISPLAY_ALL_CURRENT_ROCKSTAR_CREATED(sFMMCendStage.sRocStarCreatedVars, sGetUGC_content)
			DEAL_WITH_AMBIENT_AND_HUD()
			
			//Keyboard and mouse
			FMMC_HANDLE_KEYBOARD_AND_MOUSE(sFMMCmenu, NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive) OR (bShowMenu), bShowMenuHighlight AND bShowMenu)
			// Switch help from mouse and keyboard to joypad if we change controls
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) != bLastUseMouseKeyboard
				bLastUseMouseKeyboard = IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			ENDIF
			
			IF sFMMCMenu.bRefreshMenuASAP
				REFRESH_MENU(sFMMCMenu)
				sFMMCMenu.bRefreshMenuASAP = FALSE
				PRINTLN("[TMS][ARENACREATOR][ARENA] Resetting menu due to bRefreshMenuASAP")
			ENDIF
			
			IF IS_PAUSE_MENU_REQUESTING_TRANSITION()
				IF sCurrentVarsStruct.iEntityCreationStatus != STAGE_DEAL_WITH_FINISHING
					SET_ENTITY_CREATION_STATUS(STAGE_DEAL_WITH_FINISHING)
				ENDIF
			ENDIF
			
			IF IS_TRANSITION_TELLING_CREATOR_TO_CLEANUP()
				IF sCurrentVarsStruct.iEntityCreationStatus != STAGE_DEAL_WITH_FINISHING
					SET_ENTITY_CREATION_STATUS(STAGE_DEAL_WITH_FINISHING)
				ENDIF
			ENDIF
			
			SWITCH sCurrentVarsStruct.iEntityCreationStatus
				CASE STAGE_CHECK_TO_LOAD_CREATION
					IF g_bFMMC_LoadFromMpSkyMenu
						IF LOAD_A_MISSION_INTO_THE_CREATOR(sGetUGC_content, sFMMCendStage, g_sFMMC_LoadedMission)
							sFMMCmenu.iForcedWeapon = GET_CREATOR_WEAPON_INDEX_FROM_WEAPON_TYPE(WEAPONTYPE_PISTOL)
							IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
								sCamData.camPos = g_FMMC_STRUCT.vStartPos + <<0,-0.5, 20>>
							ENDIF
							
							SET_ENTITY_CREATION_STATUS(STAGE_DEAL_WITH_LOADING)
						ENDIF
					ELSE
						SET_ENTITY_CREATION_STATUS(STAGE_DEAL_WITH_LOADING)
					ENDIF
				BREAK
				
				CASE STAGE_DEAL_WITH_LOADING
					IF SET_SKYSWOOP_UP(TRUE)
						IF RESET_THE_MISSION_UP_SAFELY()
							SET_RESET_STATE(RESET_STATE_FADE)
							IF sFMMCmenu.iRadioState = FMMC_RADIO_MAX_OPTIONS_STATE
								SET_MOBILE_PHONE_RADIO_STATE(TRUE)
							ENDIF			
							CLEAR_HELP()
							CLEAR_BIT(sFMMCdata.iBitSet, biTestMissionActive)
							CLEAR_BIT(sFMMCdata.iBitSet, biSetUpTestMission)	
							sCurrentVarsStruct.creationStats.bEditingACreation = true
							IF g_FMMC_STRUCT.bMissionIsPublished
								sCurrentVarsStruct.creationStats.bMadeAChange = FALSE
								SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
							ENDIF
							//Clean up
							REFRESH_MENU(sFMMCMenu)
							SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_LOAD)
							sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
							ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
							ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
							iHelpBitSetOld = -1

							WARP_CAMERA_TO_START_LOCATION(sCamData, 110.0)	
							
							IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_VOTING_DISABLED) 
								CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciVOTING_ENABLED)
							ELSE
								SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciVOTING_ENABLED)
							ENDIF
							
							DO_SCREEN_FADE_IN(500)
							
							IF IS_LOADING_ICON_ACTIVE()
								SET_LOADING_ICON_INACTIVE()
							ENDIF
						ELSE
							PRINTLN("RESET_THE_MISSION_UP_SAFELY = FALSE")
						ENDIF
					ENDIF
				BREAK
				
				CASE STAGE_ENTITY_PLACEMENT_LOAD
					IF HAS_ADDITIONAL_TEXT_LOADED(ODDJOB_TEXT_SLOT)
						IF LOAD_MENU_ASSETS()
							MENU_INITIALISATION()
							SET_ENTITY_CREATION_STATUS(STAGE_CG_TO_NG_WARNING)
						ENDIF
					ENDIF
				BREAK
				
				CASE STAGE_CG_TO_NG_WARNING
					IF g_FMMC_STRUCT.bIsUGCjobNG
					OR IS_STRING_EMPTY(g_FMMC_STRUCT.tl63MissionName)			
					OR DRAW_CG_TO_NG_WARNING()
						SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
					ENDIF
				BREAK
				
				CASE STAGE_ENTITY_PLACEMENT_SETUP
					IF g_TurnOnCreatorHud
						CONTROL_CAMERA_AND_CORONA()	
						HANDLE_MOUSE_ENTITY_SELECTION(IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive))
					ENDIF
					
					IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					OR IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
					ENDIF
					
					IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive) 
					AND NOT IS_BIT_SET(sFMMCdata.iBitSet, biSetUpTestMission)
						UPDATE_SWAP_CAM_STATUS(sCurrentVarsStruct, IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround))
						RESET_MAINTAIN_PLACED_ENTITY_VARIABLES(iLocalBitSet, sCurrentVarsStruct)
						MAINTAIN_PLACED_MARKERS()
						MAINTAIN_PLACED_ENTITIES(sFMMCmenu, sHCS, sCurrentVarsStruct, sPedStruct, sVehStruct, sWepStruct, sObjStruct, sLocStruct, sTrainStruct, sPropStruct, sDynoPropStruct, sInvisibleObjects, sFMMCdata, g_CreatorsSelDetails, sFMMCendStage, sCamData, iLocalBitSet)
						MAINTAIN_PLACED_ZONES(sCurrentVarsStruct, iLocalBitSet, sFMMCmenu)
						MAINTAIN_PLACED_DUMMY_BLIPS(sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sHCS, iLocalBitSet, sFMMCendStage)
						MAINTAIN_WORLD_PROPS(sFMMCmenu, sCurrentVarsStruct, iLocalBitSet)
					ENDIF
					
					DEAL_WITH_SETTING_HEIGHT_ON_ENTITY(sFMMCmenu, sCamData, sCurrentVarsStruct, IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround), CAN_HEIGHT_BE_SET_FOR_ENTITY(sFMMCmenu, sFMMCmenu.iEntityCreation, FALSE))
					REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(sFMMCmenu.iEntityCreation)
					
					HANDLE_MENU_STATE()
					
					BOOL bHideMarker
					IF sFMMCMenu.sActiveMenu = eFmmc_PAN_CAM_BASE
					OR sFMMCMenu.sActiveMenu = eFmmc_COVER_POINT_BASE
					OR sFMMCMenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
						bHideMarker = TRUE
					ELSE
						bHideMarker = FALSE
					ENDIF
					
					IF sFMMCMenu.sActiveMenu = eFmmc_RADIO_MENU
						sCurrentVarsStruct.bDiscVisible = FALSE
					ENDIF
					MAINTAIN_PLACEMENT_DISC(sCurrentVarsStruct, sFMMCmenu, iLocalBitSet, sFMMCdata.iBitSet, bHideMarker, sFMMCmenu.iEntityCreation = -1)
					
					BLIP_INDEX biFurthestBlip
					IF sFMMCmenu.bZoomedOutRadar
						biFurthestBlip = GET_FURTHEST_BLIP_TO_PLAYER(sPedStruct.biPedBlip, sVehStruct.biVehicleBlip, sWepStruct.biWeaponBlip, sPropStruct.biObject, sDynoPropStruct.biObject, sStartEndBlips.biStart, bCameraPanBlip, sCurrentVarsStruct.biLocateBlip)
					ENDIF
					IF IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
						CONTROL_PLAYER_BLIP(TRUE, sCurrentVarsStruct, sFMMCmenu.iEntityCreation, TRUE, biFurthestBlip)
					ELSE
						IF sFMMCMenu.sActiveMenu != eFmmc_PAN_CAM_BASE
						AND sFMMCMenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
						AND sFMMCMenu.sActiveMenu != eFmmc_MAIN_OPTIONS_BASE
						AND sFMMCMenu.sActiveMenu != eFmmc_TOP_MENU
						AND sFMMCMenu.sActiveMenu != eFmmc_RADIO_MENU
							CONTROL_PLAYER_BLIP(FALSE, sCurrentVarsStruct, sFMMCmenu.iEntityCreation, sFMMCmenu.bZoomedOutRadar, biFurthestBlip)
						ELSE
							CONTROL_PLAYER_BLIP(TRUE, sCurrentVarsStruct, sFMMCmenu.iEntityCreation, sFMMCmenu.bZoomedOutRadar, biFurthestBlip)
						ENDIF												
					ENDIF
				BREAK
				
				CASE STAGE_DELETE_ALL
				CASE STAGE_DELETE_PEDS
				CASE STAGE_DELETE_VEHICLES
				CASE STAGE_DELETE_WEAPONS
				CASE STAGE_DELETE_PROPS
				CASE STAGE_DELETE_DYNOPROPS
				CASE STAGE_DELETE_TEAM_START
					DO_CONFIRMATION_MENU(sPedStruct, sTeamSpawnStruct, sVehStruct, sWepStruct, sObjStruct, sPropStruct, sDynoPropStruct, sLocStruct, sCurrentVarsStruct, sFMMCmenu, sInvisibleObjects, sFMMCEndStage, ButtonPressed, iLocalBitSet)
				BREAK
				
				CASE STAGE_ENTITY_PLACEMENT_END_MENU
					DO_END_MENU()
				BREAK	
				
				CASE STAGE_CLOUD_FAILURE				
					FMMC_DRAW_CLOUD_FAIL_WARNING(bCreatorLimitedCloudDown, bSignedOut, FALSE)
					IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						IF !SCRIPT_IS_CLOUD_AVAILABLE()
							sCurrentVarsStruct.iMenuState = MENU_STATE_PAN_CAM
						ELSE
							IF NOT IS_SCREEN_FADED_IN()
								DO_SCREEN_FADE_IN(500)
							ENDIF
						ENDIF
						SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
						REFRESH_MENU(sFMMCMenu)
					ENDIF
				BREAK
				
				CASE STAGE_DEAL_WITH_RECREATING_MISSION
					IF NOT IS_SCREEN_FADING_OUT()
						IF RESET_THE_MISSION_UP_SAFELY()
							SET_RESET_STATE(RESET_STATE_FADE)	
							SET_STORE_ENABLED(TRUE)
							SET_CREATOR_AUDIO(TRUE, FALSE)						
							IF sFMMCmenu.iRadioState = FMMC_RADIO_MAX_OPTIONS_STATE
								SET_MOBILE_PHONE_RADIO_STATE(TRUE)
							ENDIF	
							
							CLEAR_HELP()
							CLEAR_BIT(sFMMCdata.iBitSet, biTestMissionActive)
							CLEAR_BIT(sFMMCdata.iBitSet, biSetUpTestMission)	
							
							ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
							ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
							
							SET_ACTIVE_MENU(sFMMCMenu, eFmmc_TOP_MENU)
							
							KILL_WORLD_FOR_THE_CREATORS()
							
							iHelpBitSetOld = -1
							SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
							sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
							
							sFMMCdata.vPlayerPos = <<0,0,10>>
									
							//CHECK_FOR_END_CONDITIONS_MET()
							//Clean up
							REFRESH_MENU(sFMMCMenu)
													
							if IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
								FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
								DO_SCREEN_FADE_IN(500)
							ELSE
								FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
							ENDIF	
						ENDIF
					ENDIF
				BREAK
				
				CASE STAGE_DEAL_WITH_SAVING
					IF bContentReadyForUGC = FALSE
						IF PREPARE_CONTENT_TO_BE_SAVED_OR_PUBLISHED()
							bContentReadyForUGC = TRUE
						ENDIF
					ELSE
						IF SAVE_THIS_CREATION(sGetUGC_content, sFMMCendStage, sCurrentVarsStruct.creationStats)
							sFMMCendStage.iEndStage = 0
							sFMMCendStage.dataStruct.iLoadStage = 0
							sFMMCendStage.iPublishConformationStage = 0
							sFMMCendStage.iPublishStage = 0
							sFMMCendStage.iButtonBitSet = 0
							#IF IS_DEBUG_BUILD
							PRINT_FMMC_DEBUGDATA(FALSE)	
							#ENDIF
							SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
							sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							DO_SCREEN_FADE_IN(200)
							sCurrentVarsStruct.bSwitching = FALSE
							sCurrentVarsStruct.stiScreenCentreLOS = NULL
							bContentReadyForUGC = FALSE
							IF NOT IS_STRING_NULL_OR_EMPTY(sFMMCendStage.tl23ReturnedContexID)
								sFMMCendStage.tl23LoadedContexID = sFMMCendStage.tl23ReturnedContexID						
							ENDIF
						ENDIF
						IF sFMMCendStage.oskStatus = OSK_CANCELLED
							sFMMCendStage.iEndStage = 0
							sFMMCendStage.dataStruct.iLoadStage = 0
							sFMMCendStage.iPublishConformationStage = 0
							sFMMCendStage.iPublishStage = 0
							sFMMCendStage.iButtonBitSet = 0
							sFMMCendStage.oskStatus = OSK_PENDING
							sFMMCendStage.iKeyBoardStatus = 0
							SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
							sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							sCurrentVarsStruct.bSwitching = FALSE
							sCurrentVarsStruct.stiScreenCentreLOS = NULL
							bContentReadyForUGC = FALSE
							DO_SCREEN_FADE_IN(200)
							#IF IS_DEBUG_BUILD
							PRINTLN("oskStatus = OSK_CANCELLED")
							#ENDIF					
						ENDIF
					ENDIF
				BREAK
				
				CASE STAGE_ENTITY_PLACEMENT_PUBLISH
					IF bContentReadyForUGC = FALSE
						IF PREPARE_CONTENT_TO_BE_SAVED_OR_PUBLISHED()
							bContentReadyForUGC = TRUE
						ENDIF
					ELSE
						IF PUBLISH_THIS_CREATION(sGetUGC_content, sFMMCendStage, sCurrentVarsStruct.creationStats)
							sFMMCendStage.iEndStage = 0
							sFMMCendStage.dataStruct.iLoadStage = 0
							sFMMCendStage.iPublishConformationStage = 0
							sFMMCendStage.iPublishStage = 0
							sFMMCendStage.iButtonBitSet = 0
							#IF IS_DEBUG_BUILD
							PRINT_FMMC_DEBUGDATA(FALSE)	
							#ENDIF
							SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)	
							sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
									
							IF sCurrentVarsStruct.creationStats.bSuccessfulSave = TRUE
								PRINTLN("sFMMCendStage.bSaveCancelled = FALSE, setting sFMMCendStage.bMajorEditOnLoadedMission = FALSE")
								sFMMCendStage.bMajorEditOnLoadedMission = FALSE
								IF NOT IS_STRING_NULL_OR_EMPTY(sFMMCendStage.tl23ReturnedContexID)
									sFMMCendStage.tl23LoadedContexID = sFMMCendStage.tl23ReturnedContexID						
								ENDIF
							ENDIF
							
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							sCurrentVarsStruct.bSwitching = FALSE
							sCurrentVarsStruct.stiScreenCentreLOS = NULL
							bContentReadyForUGC = FALSE
							//CHECK_FOR_END_CONDITIONS_MET()
							CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
							
							REFRESH_MENU(sFMMCmenu)
							DO_SCREEN_FADE_IN(200)
						ENDIF
						IF sFMMCendStage.oskStatus = OSK_CANCELLED
							sFMMCendStage.oskStatus = OSK_PENDING
							sFMMCendStage.iKeyBoardStatus = 0
							SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
							sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							sCurrentVarsStruct.bSwitching = FALSE
							sCurrentVarsStruct.stiScreenCentreLOS = NULL
							bContentReadyForUGC = FALSE
							DO_SCREEN_FADE_IN(200)
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDEFAULT_XP)
								g_FMMC_STRUCT.iXPReward = 0
								PRINTLN("ciDEFAULT_XP RESET TO = ", g_FMMC_STRUCT.iXPReward)
							ENDIF
							#IF IS_DEBUG_BUILD
							PRINTLN("oskStatus = OSK_CANCELLED")
							#ENDIF					
						ENDIF
					ENDIF
				BREAK
				
				CASE STAGE_DEAL_WITH_FINISHING
					IF IS_TRANSITION_TELLING_CREATOR_TO_CLEANUP()
						TRIGGER_TRANSITION_MENU_ACTIVE(TRUE) 					
						PRINTLN("[JA@PAUSEMENU] Clean up creator due to IS_TRANSITION_TELLING_CREATOR_TO_CLEANUP")
						DO_SCREEN_FADE_IN(200)
						SCRIPT_CLEANUP(TRUE)
					ELIF IS_PAUSE_MENU_REQUESTING_TRANSITION()
						TRIGGER_TRANSITION_MENU_ACTIVE(TRUE) 
						PRINTLN("[JA@PAUSEMENU] Clean up creator due to pause menu requesting transition")
						DO_SCREEN_FADE_IN(200)
						SCRIPT_CLEANUP(TRUE)
					ELIF DEAL_WITH_FINISHING_CREATOR(sFMMCendStage)
						DO_SCREEN_FADE_IN(200)
						PRINTLN("[END CREATOR] DONE")
					 	SCRIPT_CLEANUP(FALSE)
					ENDIF
				BREAK
				
			ENDSWITCH
		ELSE
			IF NETWORK_IS_GAME_IN_PROGRESS()
				MAINTAIN_MP_AMBIENT_MANAGER()
				IF bTextSetUp = TRUE
					Maintain_MP_Objective_Text()
					Maintain_MP_Communications()
				ENDIF
				HANDLE_FORCE_PLAYERS_FROM_CAR(PLAYER_ID())
				MAINTAIN_FORCE_OUT_OF_VEHICLE()
				RENDER_INVENTORY_HUD(on_mission_gang_box)
				MAINTAIN_BIG_MESSAGE() 
				PROCESS_LEADERBOARD_CAM()
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(sFMMCdata.iBitSet, bMenuJustChangedPlacementBlocker)
			PRINTLN("[LM] - Creator Bit - Clearing bMenuJustChangedPlacementBlocker")
			CLEAR_BIT(sFMMCdata.iBitSet, bMenuJustChangedPlacementBlocker)
		ENDIF
		
		IF g_Private_Gamemode_Current = GAMEMODE_FM
		AND NETWORK_IS_GAME_IN_PROGRESS()
			PRINTLN("[LH] A Network Game is in Progress! Cleaning up Creator.")
			SCRIPT_CLEANUP(TRUE)
		ENDIF
		
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("main")) != 0
			PRINTLN("[LH] The player has escaped to singleplayer! Cleaning up Creator.")
			SCRIPT_CLEANUP(TRUE)
		ENDIF
		
		IF g_bKillFMMC_SignedOut
			SCRIPT_CLEANUP(TRUE)
			PRINTLN("g_bKillFMMC_SignedOut = TRUE")
		ENDIF
		
		IF sCurrentVarsStruct.iEntityCreationStatus != STAGE_DEAL_WITH_RECREATING_MISSION
			MAINTAIN_TEST_MODE_STATE()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		
		MAINTAIN_ENTITY_DETAIL_EXPORT()
		MAINTAIN_ENTITY_DETAIL_EXPORT_ALTERNATE(sVehStruct)
		MAINTAIN_ENTITY_DETAIL_EXPORT_PLACEHOLDER(sVehStruct)
		
		//Force clean up
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE
				sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_END_MENU
			ENDIF
		ENDIF
		
		#ENDIF
		
	ENDWHILE
ENDSCRIPT

#ENDIF
