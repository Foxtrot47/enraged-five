//////////////////////////////////////////////////////////////////////
// Name:        FM_maintain_cloud_header_data						//
// Description: Controls the loading of all the cloud loader data	//
// Written by:  Robert Wright										//
// Date: 11/07/2013													//
//////////////////////////////////////////////////////////////////////
//Globals
USING "globals.sch"
//Headers
USING "FMMC_Cloud_loader.sch"
USING "net_cloud_mission_loader_public.sch"
USING "freemode_header.sch"

//The load vars
LOAD_ALL_UGC_VARS sUGC_load_vars

//If this returns true then we are allowed to do a cloud refresh
FUNC BOOL IS_CLOUD_REFRESH_ALLOWED_TO_HAPPEN()
	
	//If it's a forced NO
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_REFRESH_AS_ON_END_OF_JOB_VOTE_SCREEN)
		RETURN FALSE
	ENDIF
	//If I'm on the end of job vote screen then yes
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_On_END_OF_JOB_VOTE_SCREEN)
		RETURN TRUE
	ENDIF
	
	//And I'm IDLE
	IF GET_CORONA_STATUS() = CORONA_STATUS_IDLE
	//And a transition session is not launching
	AND NOT IS_TRANSITION_SESSION_LAUNCHING()
	//And not has the mause menu got a hold of the UGC
	AND NOT IS_PAUSE_MENU_IS_USING_UGC()
	//And not is the game restarting
	AND NOT TRANSITION_SESSIONS_CLEANUP_AFTER_MISSION()
	//And we are not restarting freemode
	AND NOT SHOULD_TRANSITION_SESSIONS_RUN_FREEMODE_RELAUNCH()
	//And not is the main freemode script about to relaunch
	AND NOT SHOULD_TRANSITION_SESSION_LAUNCH_MAIN_FM_SCRIPT()
	//And not if we are in the process of cleaning up a mission
	AND NOT DOES_CURRENT_FM_MISSION_NEED_CLEANED_UP()
	// And not if the missions are still in the process of being added/refreshed from a previous refresh
	AND NOT Is_Cloud_Loaded_Mission_Data_Being_Refreshed()
	// And Not if a mission download request in in progress (generally Gang Attack, or Mission Triggerer)
	AND NOT Is_Cloud_Loaded_Mission_Data_Download_In_Progress()
	//in the tutoriasl
	AND HAS_IMPORTANT_STATS_LOADED() AND ARE_ALL_INSTANCED_TUTORIALS_COMPLETE()
	//And a transition session is not being set up
	AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState != FMMC_LAUNCHER_STATE_LOAD_MISSION_FOR_TRANSITION_SESSION
	//And I'm not initilising a mission
	AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState != FMMC_LAUNCHER_STATE_INITILISE_MISSION
	//And not on a playlist
	AND NOT IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
	//and it;s not already getting
	AND NOT IS_SCRIPT_UCG_REQUEST_IN_PROGRESS()
	//And not in propertys
	AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

INT iUGC_reloadStage
BOOL bWaitForSafeReset
BOOL bWasLiteRefresh
BOOL bWasFullRefresh
//BOOL bResetVars
CONST_INT ciCHECK_FOR_RELOAD	0
CONST_INT ciLOAD_ALL_UGC_DATA	1

//set the player hash and time staps to ZERO!
PROC SET_HASH_AND_TIME_STAMP_TO_ZERO()
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	
	IF iPlayer < 0
		EXIT
	ENDIF
	
	IF GlobalplayerBD_FM[iPlayer].sDownLoadedContent.iHash != 0
		PRINTLN("CLOUD REF - SET_HASH_AND_TIME_STAMP_TO_ZERO - Settting GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sDownLoadedContent.iHash      = 0")
		PRINTLN("CLOUD REF - SET_HASH_AND_TIME_STAMP_TO_ZERO - Settting GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sDownLoadedContent.iTimeStamp = 0")
		GlobalplayerBD_FM[iPlayer].sDownLoadedContent.iHash = 0
		GlobalplayerBD_FM[iPlayer].sDownLoadedContent.iTimeStamp = 0
	ENDIF
ENDPROC

//is the FMMC launcher happy for a refresh
FUNC BOOL IS_LAUNCHER_IN_STATE_FOR_CLOUD_REFRESH()
	//game not if progress? fine!
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN TRUE
	ENDIF
	
	//game not if progress? fine!
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	IF iPlayer < 0
		RETURN TRUE
	ENDIF
	
	//Player not on a tutorial
	IF NOT IS_LOCAL_PLAYER_IN_ACTIVITY_TUTORIAL_CORONA()
	//And I'm not loading a transition session
	AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState != FMMC_LAUNCHER_STATE_LOAD_MISSION_FOR_TRANSITION_SESSION
	//And I'm not initilising a mission
	AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState != FMMC_LAUNCHER_STATE_INITILISE_MISSION
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SCRIPT_CLEAN_UP()
	PRINTLN("CLOUD REF - SCRIPT_CLEAN_Up")
	g_sRockstarUgcGetStruct.bUseNewLoader = FALSE
	g_sRockstarUgcGetStruct.bSkipInitialRef = TRUE
	CLEAR_CLOUD_REFRESH_SCRIPT_IS_TO_DIE()
	CLEAR_SHOULD_RUN_ROCKSTAR_CONTENT_REFRESH()
	CLEAR_CLOUD_REFRESH_IS_USING_UGC()
	CLEAR_FM_UGC_HAS_FINISHED()
	SET_SHOULD_RUN_FULL_CLOUD_REFRESH()
	SET_UGC_CONTENT_LOADED_IN_SP(FALSE)		
	TERMINATE_THIS_THREAD()
ENDPROC


PROC SET_VARS_AFTER_CLOUD_REFRESH()
	CLEAR_CLOUD_REFRESH_IS_USING_UGC()
	CLEAR_SHOULD_RUN_ROCKSTAR_CONTENT_REFRESH()
	g_sRockstarUgcGetStruct.bLoaderTellLauncherFinished = TRUE
	sUGC_load_vars.iLoadStage = 0
	iUGC_reloadStage = ciCHECK_FOR_RELOAD
	RESET_UGC_LOAD_VARS(sUGC_load_vars.sGetUGC, FALSE)
	PRINTLN("CLOUD REF - CHECK_IF_CONTENT_SHOULD_REFRESH - iUGC_reloadStage = ciCHECK_FOR_RELOAD")
ENDPROC

//main loop		
SCRIPT	 	

	PRINTLN("CLOUD REF - Starting FM_maintain_cloud_header_data.sc")
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	PRINTLN("CLOUD REF - FM_maintain_cloud_header_data.sc - SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)")
	
	//Make him safe!
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME() 
	PRINTLN("CLOUD REF - FM_maintain_cloud_header_data.sc - NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME")
	
	//clear that it is set to die
	IF IS_CLOUD_REFRESH_SCRIPT_IS_TO_DIE()
		CLEAR_CLOUD_REFRESH_SCRIPT_IS_TO_DIE()
	ENDIF
	
	//If we've not got the initial content then we need to clear the flag that we need to skip the initial download
	IF NOT GET_FM_UGC_INITIAL_HAS_FINISHED()
		PRINTLN("CLOUD REF - FM_maintain_cloud_header_data.sc - GET_FM_UGC_INITIAL_HAS_FINISHED = FALSE - calling g_sRockstarUgcGetStruct.bSkipInitialRef = FALSE")
		g_sRockstarUgcGetStruct.bSkipInitialRef = FALSE
	ENDIF

	//Wait till in MP
	//url:bugstar:2548386 - Please remove redundant QueryContent calls
	WHILE NOT g_b_MPMapIsLoaded
		IF IS_CLOUD_REFRESH_SCRIPT_IS_TO_DIE()
			SCRIPT_CLEAN_UP()
		ENDIF
		WAIT(0) 		
	ENDWHILE
	
	//If we should not skip the initial load
	IF NOT g_sRockstarUgcGetStruct.bSkipInitialRef
		//Start load
		SET_CLOUD_REFRESH_IS_USING_UGC()
		//Do the initial load but only once
		WHILE NOT LOAD_ALL_UGC_CONTENT_FOR_GTA_V_ONLINE(sUGC_load_vars, TRUE, TRUE)
			IF IS_CLOUD_REFRESH_SCRIPT_IS_TO_DIE()
				SCRIPT_CLEAN_UP()
			ENDIF
			WAIT(0) 		
		ENDWHILE
	ENDIF
	
	g_sRockstarUgcGetStruct.bSkipInitialRef = FALSE
	
	//Done with load
	CLEAR_CLOUD_REFRESH_IS_USING_UGC()
	
	PRINTLN("CLOUD REF - FM_maintain_cloud_header_data.sc - LOAD_ALL_UGC_CONTENT_FOR_GTA_V_ONLINE full load done")
	
	//Clear that we should do a full refresh
	IF SHOULD_RUN_FULL_CLOUD_REFRESH()
		CLEAR_SHOULD_RUN_FULL_CLOUD_REFRESH()
	ENDIF
	PRINTLN("CLOUD REF - FM_maintain_cloud_header_data.sc - CLEAR_SHOULD_RUN_FULL_CLOUD_REFRESH")
	
	//Clear that the cloud refresh is using UGC
	IF IS_CLOUD_REFRESH_IS_USING_UGC()
		CLEAR_CLOUD_REFRESH_IS_USING_UGC()
	ENDIF
	PRINTLN("CLOUD REF - FM_maintain_cloud_header_data.sc - CLEAR_CLOUD_REFRESH_IS_USING_UGC")
	
	
	//If we're not set to finished then set the flag. 
	IF NOT GET_FM_UGC_HAS_FINISHED()
		PRINTLN("CLOUD REF - FM_maintain_cloud_header_data.sc - SET_FM_UGC_HAS_FINISHED")
		SET_FM_UGC_HAS_FINISHED()
	ENDIF
	
	//the the main loop, wait for the load
	WHILE TRUE		
	
		IF IS_CLOUD_REFRESH_SCRIPT_IS_TO_DIE()
			SCRIPT_CLEAN_UP()
		ENDIF
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND NETWORK_IS_ACTIVITY_SESSION()
		AND NOT SHOULD_RUN_FULL_CLOUD_REFRESH()
			IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
			AND (g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_PLANNING OR g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_HEIST)
				IF Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
				OR Is_Player_Currently_On_MP_Heist(PLAYER_ID())
					PRINTLN("MAINTAIN_FM_maintain_cloud_header_data_LAUNCHING - g_sRockstarUgcGetStruct.bHeistsKilledNewLoader = TRUE")
					g_sRockstarUgcGetStruct.bHeistsKilledNewLoader = TRUE
					SCRIPT_CLEAN_UP()
				ENDIF
			ENDIF
		ENDIF
		
		//if we should do a full refresh
		IF SHOULD_RUN_FULL_CLOUD_REFRESH()
			bWasFullRefresh = TRUE
			//If we were in offline mode then reset to be ready to reuse
			IF bWasLiteRefresh
				PRINTLN("CLOUD REF - bWasLiteRefresh = TRUE")
				bWasLiteRefresh = FALSE
				IF sUGC_load_vars.iLoadStage != 0
					PRINTLN("CLOUD REF - bWasLiteRefresh = TRUE - sUGC_load_vars.iLoadStage != 0")
					RESET_UGC_LOAD_VARS(sUGC_load_vars.sGetUGC, FALSE)								
				ENDIF
			ENDIF
			//if we've started and it was force canceled then restart
			IF sUGC_load_vars.sGetUGC.iProgress != ciUGC_PROGRESS_SET_UP
			AND sUGC_load_vars.sGetUGC.iProgress != ciUGC_PROGRESS_START
				IF UGC_WAS_QUERY_FORCE_CANCELLED()
					PRINTLN("CLOUD REF - UGC_WAS_QUERY_FORCE_CANCELLED - SHOULD_RUN_FULL_CLOUD_REFRESH")
					RESET_UGC_LOAD_VARS(sUGC_load_vars.sGetUGC, FALSE)								
				ENDIF
			ENDIF
			IF NOT IS_CLOUD_REFRESH_IS_USING_UGC()
				SET_CLOUD_REFRESH_IS_USING_UGC()
			ENDIF	
			//Do the full reload
			IF LOAD_ALL_UGC_CONTENT_FOR_GTA_V_ONLINE(sUGC_load_vars, TRUE, TRUE)	
				CLEAR_SHOULD_RUN_FULL_CLOUD_REFRESH()
				CLEAR_CLOUD_REFRESH_IS_USING_UGC()
				SET_VARS_AFTER_CLOUD_REFRESH()
			ENDIF
			
			//Wait
			WAIT(0) 		
			
		//Other wise we shouldn't do a full refresh
		ELSE
			bWasLiteRefresh = TRUE
			IF bWasFullRefresh
				PRINTLN("CLOUD REF - bWasFullRefresh = TRUE")
				bWasFullRefresh = FALSE
				IF sUGC_load_vars.iLoadStage != 0
					RESET_UGC_LOAD_VARS(sUGC_load_vars.sGetUGC, FALSE)								
					PRINTLN("CLOUD REF - bWasFullRefresh = TRUE - sUGC_load_vars.iLoadStage != 0")
				ENDIF
			ENDIF
			
			WAIT(0) 
					
			SWITCH iUGC_reloadStage
				CASE ciCHECK_FOR_RELOAD
					//If we should refresh
					IF SHOULD_RUN_ROCKSTAR_CONTENT_REFRESH()
						IF IS_CLOUD_REFRESH_ALLOWED_TO_HAPPEN()					
							PRINTLN("CLOUD REF - CHECK_IF_CONTENT_SHOULD_REFRESH")
							g_FMMC_ROCKSTAR_CREATED.iTimeOfLoad		= 0
							g_FMMC_ROCKSTAR_CREATED.iHashOfLoaded	= 0				
							iUGC_reloadStage = ciLOAD_ALL_UGC_DATA
							CLEAR_FM_UGC_HAS_FINISHED()
							SET_CLOUD_REFRESH_IS_USING_UGC()
							g_sRockstarUgcGetStruct.bLoaderTellLauncherToClean = TRUE				
							PRINTLN("CLOUD REF - UGC_WAS_QUERY_FORCE_CANCELLED - IS_PAUSE_MENU_IS_USING_UGC = FALSE - ciCHECK_FOR_RELOAD")
							RESET_UGC_LOAD_VARS(sUGC_load_vars.sGetUGC)
							SET_UGC_LOADER_STATE(sUGC_load_vars.iLoadStage, ciGAME_STATE_CLOUD_LOADER_REGISTER_GLOBALS)
							sUGC_load_vars.iLoopStart = 0
							sUGC_load_vars.iLoopMax   = 0
							#IF IS_DEBUG_BUILD 
							g_TransitionSessionNonResetVars.MenuVars.iReLoadXmlFrameCount = 0
							#ENDIF
							bWaitForSafeReset = FALSE								
						//We are not is a state to refresh, then block it!
						ELSE
							SET_HASH_AND_TIME_STAMP_TO_ZERO()
						ENDIF
					ENDIF
				BREAK
			
				CASE ciLOAD_ALL_UGC_DATA
					//Set that the cloud refresh is happening
					sUGC_load_vars.sGetUGC.bIsCloudRefresh = TRUE
					//if there isn't basis to wait for a safe reset
					IF bWaitForSafeReset = FALSE
					AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_REFRESH_AS_ON_END_OF_JOB_VOTE_SCREEN)					 
						IF LOAD_ALL_UGC_CONTENT_FOR_GTA_V_ONLINE(sUGC_load_vars, FALSE, FALSE, TRUE) 
							SET_VARS_AFTER_CLOUD_REFRESH()
							#IF IS_DEBUG_BUILD
								g_TransitionSessionNonResetVars.MenuVars.bReLoadXML = TRUE
							#ENDIF
						ENDIF
					//We need to wait for a safe reset
					ELSE
						//If we are on the end of job vote screen then we need to clear that the pause menu is using the UGC
						IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_On_END_OF_JOB_VOTE_SCREEN)
							IF IS_PAUSE_MENU_IS_USING_UGC()
								CLEAR_PAUSE_MENU_IS_USING_UGC()
							ENDIF
						ENDIF
						//If the pause menu is not using it then reset
						IF NOT IS_PAUSE_MENU_IS_USING_UGC()
							bWaitForSafeReset = FALSE								
							PRINTLN("CLOUD REF - UGC_WAS_QUERY_FORCE_CANCELLED - IS_PAUSE_MENU_IS_USING_UGC = FALSE - ciLOAD_ALL_UGC_DATA")
							RESET_UGC_LOAD_VARS(sUGC_load_vars.sGetUGC)
							SET_UGC_LOADER_STATE(sUGC_load_vars.iLoadStage, ciGAME_STATE_CLOUD_LOADER_REGISTER_GLOBALS)
							sUGC_load_vars.iLoopStart = 0
							sUGC_load_vars.iLoopMax   = 0
							#IF IS_DEBUG_BUILD 
							g_TransitionSessionNonResetVars.MenuVars.iReLoadXmlFrameCount = 0
							#ENDIF
						//Spam what's happening
						#IF IS_DEBUG_BUILD
						ELSE
							#ENDIF
							PRINTLN("CLOUD REF - IS_PAUSE_MENU_IS_USING_UGC = TRUE")
						ENDIF
					ENDIF
					//If we're not on stage 0 and we are not waiting for a safe reset then set that we need to wait
					IF sUGC_load_vars.sGetUGC.iProgress != ciUGC_PROGRESS_SET_UP
					AND sUGC_load_vars.sGetUGC.iProgress != ciUGC_PROGRESS_START
					AND bWaitForSafeReset = FALSE
						IF UGC_WAS_QUERY_FORCE_CANCELLED()
							PRINTLN("CLOUD REF - UGC_WAS_QUERY_FORCE_CANCELLED")
							bWaitForSafeReset = TRUE								
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF	
	ENDWHILE

ENDSCRIPT
