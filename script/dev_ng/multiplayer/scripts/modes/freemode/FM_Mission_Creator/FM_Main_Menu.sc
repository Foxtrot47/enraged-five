//////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        FM_Main_Menu																//
// Description: Make your own amazing mission													//
// Written by:  ROBERT WRIGHT																	//
// Date: 19/10/2011																				//
//////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

//Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_interiors.sch"
USING "commands_path.sch"
USING "dialogue_public.sch"
USING "commands_pad.sch"
USING "commands_hud.sch"
USING "screen_tabs.sch"
USING "net_events.sch"
USING "net_hud_displays.sch"
USING "selector_public.sch"
USING "hud_drawing.sch"
USING "Screen_placements.sch"
USING "MP_SkyCam.sch"
USING "FMMC_header.sch"
USING "Transition_Common.sch"
USING "net_common_functions.sch"

//
//#IF IS_DEBUG_BUILD	
//USING "hud_creator_tool.sch"
//#ENDIF
//
//STRUCT_DATA_FILE FMMCdataStruct
ENUM MAIN_SERVER_MISSION_STAGES
	GAME_STATE_INI,
	GAME_STATE_DO_MENU,
	GAME_STATE_CAMERA_BACK_UP,
	GAME_STATE_WAIT,
	GAME_STATE_CREATE,
	GAME_STATE_SAVE_OUT_DATA,	
	GAME_STATE_LEAVE,		
	GAME_STATE_FAILED,		
	GAME_STATE_END		
ENDENUM

////////////////////////////// 				TAB CONST_INTS        
CONST_INT FMMC_TAB_CREATION_MISSION 				FMMC_TYPE_MISSION               
CONST_INT FMMC_TAB_CREATION_DEATHMATCH				FMMC_TYPE_DEATHMATCH               
CONST_INT FMMC_TAB_CREATION_RACE 					FMMC_TYPE_RACE    

CONST_INT FMMC_TAB_GAME_SET_UP						0
CONST_INT FMMC_TAB_RULES							1
CONST_INT FMMC_TAB_OPTIONS							2
         
CONST_INT FMMC_TAB_MAXIMUM_NUMBER 					3   
////////////////////////////// 				Max number of catogories
CONST_INT FMMC_MAX_NUM_CATEGORIES					17

//STRUCT FOR CONTAINING CURRENT SELECTED ITEM
STRUCT FMMC_MAIN_MENU_STRUCT
	SCALEFORM_INDEX 	AnotherButtonMovie
	INT 				iLobbyProgress							// An iterator for the lobby progress switch
	INT 				iCategorySelected 				= 0		// Category selected (e.g. Map, duration etc.)
	INT 				iColoumSelected 				= 0		// Category selected (e.g. Map, duration etc.)			
	INT 				iLobbyBitset							//Bit set
	INT 				iLastTypeCreated				= -1	//To check to see if the global struct should be cleaned
	INT  				iDelayInt						= 0 	//Int to control the delay of the analouge sticks
	INT 				iCurrentMenuLength						//An int the stores the current menu 
	// Lobby categories and their default values
	INT 				iMenuOption[FMMC_TAB_MAXIMUM_NUMBER][FMMC_MAX_NUM_CATEGORIES]
	INT 				iMenuRules[FMMC_TAB_MAXIMUM_NUMBER][FMMC_MAX_NUM_CATEGORIES]
ENDSTRUCT
FMMC_MAIN_MENU_STRUCT 		FMMCmainMenuStruct
//STRUCT_HEADER_DATA_FILE		FMMCheaderDataStruct
//BOOL 						bLoadedHeaderData

////////////////////////////// 				BIT SET INTS      
CONST_INT FMMC_MENU_BITSET_INITIALISED_DM			0
CONST_INT FMMC_MENU_BITSET_CLEAR_SCALEFORM_BUTTONS	1
CONST_INT FMMC_MENU_BITSET_CLEAR_SCALEFORM			2
CONST_INT FMMC_MENU_BITSET_SET_UP_WIDGETS			3
////////////////////////////// 				MAX LENGTH OF MENUS
CONST_INT ciNumberOfOptions_Mission 				11
CONST_INT ciNumberOfRules_Mission 					8
CONST_INT ciNumberOfOptions_DeathMatch 				15
CONST_INT ciNumberOfRules_DeathMatch 				8
//Const ints fot the placement array postions
CONST_INT ciOptionBG 								0 
CONST_INT ciRulesBG 								8 
//////////////////////////////     			COLOUM
CONST_INT FMMC_OPTIONS_COLOUM						0
CONST_INT FMMC_RULES_COLOUM							1
CONST_INT FMMC_MAX_NUMBER_COLOUMS					2
//////////////////////////////// 			CATEGORIES     
CONST_INT CAT_MIN									0
CONST_INT CAT_MAX									15

////////////////////////////// 		MISSION OPTIONS


////////////////////////////// 		DEATHMATCH OPTIONS
CONST_INT OPTION_NUM_PLAYERS 						OPTION_LOCATION			//0
CONST_INT OPTION_WEAPON_RESPAWN						OPTION_TRAFFIC			//5
CONST_INT OPTION_VEHICLES							OPTION_TIME_OFDAY		//13
//CONST_INT OPTION_VEHICLES_RESPAWN					OPTION_WEATHER			//14

//DEATHMATCH OPTIONS MAX_VALUES
CONST_INT MAX_OPTION_NUM_PLAYERS					14//NUM_NETWORK_PLAYERS
CONST_INT MAX_OPTION_TEAM_DEATHMATCH				1
CONST_INT MAX_OPTION_DURATION						DM_DURATION_60
CONST_INT MAX_OPTION_TARGET							DM_TARGET_KILLS_MAX	
CONST_INT MAX_OPTION_WEAPONS						DM_WEAPONS_SNIPERS
CONST_INT MAX_OPTION_WEAPON_RESPAWN					DM_SPAWN_TIME_LARGE	
CONST_INT MAX_OPTION_AUTO_AIM						DM_AUTO_AIM_DONT_ALLOW
CONST_INT MAX_OPTION_POLICE							DM_POLICE_OFF
CONST_INT MAX_OPTION_PEDS							DM_PEDS_HIGH	
CONST_INT MAX_OPTION_BLIPS							DM_BLIPS_ALWAYS	
CONST_INT MAX_OPTION_TAGS							TAGS_WHEN_TARGETED	
CONST_INT MAX_OPTION_RADIO							DM_RADIO_6	
CONST_INT MAX_OPTION_VOICE							VOICE_PROX	
CONST_INT MAX_OPTION_SPAWN_TIME						DM_SPAWN_TIME_LARGE	
CONST_INT MAX_OPTION_VEHICLES						14	
//CONST_INT MAX_OPTION_VEHICLES_RESPAWN				DM_SPAWN_TIME_30	




////////////////////////////// 		RACE OPTIONS
CONST_INT ciNumberOfOptions_Race 					9
//RACE OPTIONS MAX_VALUES


////////////////////////////// 		RULES
CONST_INT RULE_NUMBER_OF_KILLS						0
CONST_INT RULE_NUMBER_OF_DEATHS						1
CONST_INT RULE_COLLECT_OBJECT						2
CONST_INT RULE_COLLECT_VEHICLE						3
CONST_INT RULE_COLLECT_ENEMY						4
CONST_INT RULE_FIRST_TO_LOCATION					5
CONST_INT RULE_LAST_MAN_STANDING					6
CONST_INT RULE_TARGET_PEDS							7
CONST_INT ciNumberOfRules_Race 						8
//RULES MAX_VALUES
CONST_INT RULE_MAX_NUMBER_OF_KILLS					14
CONST_INT RULE_MAX_NUMBER_OF_DEATHS					14
CONST_INT RULE_MAX_COLLECT_OBJECT					9
CONST_INT RULE_MAX_COLLECT_VEHICLE					9
CONST_INT RULE_MAX_COLLECT_ENEMY					9
CONST_INT RULE_MAX_FIRST_TO_LOCATION				2
CONST_INT RULE_MAX_LAST_MAN_STANDING				2
CONST_INT RULE_MAX_TARGET_PEDS						3
////////////////////////////// 		RULES BASE VALUE
CONST_INT RULE_BASE									18

////////////////////////////// 		TEXT PLACEMENT
CONST_INT FMMC_MENU_OPTIONS_TITLE_COLOUM_0			0
CONST_INT FMMC_MENU_OPTIONS_TITLE_COLOUM_1			1
CONST_INT FMMC_MENU_OPTIONS_TITLE_COLOUM_2			2
CONST_INT FMMC_MENU_OPTIONS_NAMES_COLOUM_0			3
CONST_INT FMMC_MENU_OPTIONS_NAMES_COLOUM_1			4
CONST_INT FMMC_MENU_OPTIONS_NAMES_COLOUM_2			5
CONST_INT FMMC_MENU_SELECTED_OPTIONS_COLOUM_0		6
CONST_INT FMMC_MENU_SELECTED_OPTIONS_COLOUM_1		7
CONST_INT FMMC_MENU_SELECTED_OPTIONS_COLOUM_2		8


CONST_INT biNotEnoughSpaceOnGrid 		1

///////////////////////////////////////////////////////////
////////////////// VARIABLES FOR MISSION //////////////////
///////////////////////////////////////////////////////////
//
////WIDGET VARIABLES
//#IF IS_DEBUG_BUILD 
//	INT iServerMissionStageWidget
//	FLOAT fXMovePos = 0.298
//	FLOAT fYMovePos = 0.233
//	WIDGET_GROUP_ID wgGroup
//	BOOL bDoSave, bDoLOAD, bInitiliseFMMCstruct,bDoClear
//#ENDIF
PROC SCRIPT_CLEANUP()
	enable_selector()
	//SET_VEHICLE_DENSITY_MULTIPLIER(NETWORK_VEHICLE_DENSITY)
	//SET_PED_DENSITY_MULTIPLIER(NETWORK_PED_DENSITY)
	//SET_GRAVITY_LEVEL(GRAV_EARTH)	
	g_FMMC_STRUCT.bKillFMMC = FALSE
	g_bOkToExitMissionCreator = TRUE
	g_bKillFMMCmenu = FALSE
	TERMINATE_THIS_THREAD()
ENDPROC

FUNC BOOL ARE_ANY_CREATOR_SCRIPTS_RUNNING()
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Mission_Creator")) > 0
	OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Deathmatch_Creator")) > 0
	OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Race_Creator")) > 0
	OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Capture_Creator")) > 0
	OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_LTS_Creator")) > 0
	OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Survival_Creator")) > 0
	OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Basic_Creator")) > 0
	OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Freemode_Creator")) > 0
	
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//Launch the race editor script
//return true once it's been launched and it has then ended.
FUNC BOOL FM_LAUNCH_MISSION_CREATOR_SCRIPT(INT &iBitSetPassed)
	IF NOT IS_BIT_SET(iBitSetPassed, biScriptLaunched) 
		REQUEST_SCRIPT("FM_Mission_Creator")
		PRINTLN("FM_Main_Menu - REQUEST_SCRIPT - FM_Mission_Creator")				
	    IF HAS_SCRIPT_LOADED("FM_Mission_Creator") 
			PRINTLN("FM_Main_Menu - HAS_SCRIPT_LOADED - FM_Mission_Creator")				
			IF !ARE_ANY_CREATOR_SCRIPTS_RUNNING()
				PRINTLN("FM_Main_Menu - ARE_ANY_CREATOR_SCRIPTS_RUNNING - FM_Mission_Creator")				
				START_NEW_SCRIPT("FM_Mission_Creator", MISSION_STACK_SIZE)// DEFAULT_STACK_SIZE)
		   		SET_SCRIPT_AS_NO_LONGER_NEEDED("FM_Mission_Creator")
				SET_BIT(iBitSetPassed, biScriptLaunched)
			ELSE
				PRINTLN("FM_Main_Menu - MISSION_CREATOR SCRIPT RUNNING")				
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			IF g_bRunMissionCreatorInDebugMode
				SCRIPT_CLEANUP()
			ENDIF
		#ENDIF
		//IF GET_NUMBER_OF_THREADS_RUNNING_THIS_SCRIPT("FM_Mission_Creator") = 0
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Mission_Creator")) = 0
			CLEAR_BIT(iBitSetPassed, biScriptLaunched)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
//Launch the LTS Creator script
//return true once it's been launched and it has then ended.
FUNC BOOL FM_LAUNCH_LTS_CREATOR_SCRIPT(INT &iBitSetPassed)
	IF NOT IS_BIT_SET(iBitSetPassed, biScriptLaunched) 
		PRINTLN("FM_Main_Menu - REQUEST_SCRIPT - FM_LTS_Creator")				
		REQUEST_SCRIPT("FM_LTS_Creator")
	    IF HAS_SCRIPT_LOADED("FM_LTS_Creator") 
			PRINTLN("FM_Main_Menu - HAS_SCRIPT_LOADED - FM_LTS_Creator")				
			IF !ARE_ANY_CREATOR_SCRIPTS_RUNNING()
				PRINTLN("FM_Main_Menu - ARE_ANY_CREATOR_SCRIPTS_RUNNING - FM_LTS_Creator")				
				START_NEW_SCRIPT("FM_LTS_Creator", MISSION_STACK_SIZE)// DEFAULT_STACK_SIZE)
		   		SET_SCRIPT_AS_NO_LONGER_NEEDED("FM_LTS_Creator")
				SET_BIT(iBitSetPassed, biScriptLaunched)
			ELSE
				PRINTLN("FM_Main_Menu - MISSION_CREATOR SCRIPT RUNNING - FM_LTS_Creator")	
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			IF g_bRunMissionCreatorInDebugMode
				SCRIPT_CLEANUP()
			ENDIF
		#ENDIF
		//IF GET_NUMBER_OF_THREADS_RUNNING_THIS_SCRIPT("FM_LTS_Creator") = 0
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_LTS_Creator")) = 0
			CLEAR_BIT(iBitSetPassed, biScriptLaunched)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Launch the Capture Creator script
//return true once it's been launched and it has then ended.
FUNC BOOL FM_LAUNCH_CAPTURE_CREATOR_SCRIPT(INT &iBitSetPassed)
	IF NOT IS_BIT_SET(iBitSetPassed, biScriptLaunched) 
		PRINTLN("FM_Main_Menu - REQUEST_SCRIPT - FM_Capture_Creator")				
		REQUEST_SCRIPT("FM_Capture_Creator")
	    IF HAS_SCRIPT_LOADED("FM_Capture_Creator") 
			PRINTLN("FM_Main_Menu - HAS_SCRIPT_LOADED - FM_Capture_Creator")				
			IF !ARE_ANY_CREATOR_SCRIPTS_RUNNING()
				PRINTLN("FM_Main_Menu - ARE_ANY_CREATOR_SCRIPTS_RUNNING - FM_Capture_Creator")				
				START_NEW_SCRIPT("FM_Capture_Creator", MISSION_STACK_SIZE)// DEFAULT_STACK_SIZE)
		   		SET_SCRIPT_AS_NO_LONGER_NEEDED("FM_Capture_Creator")
				SET_BIT(iBitSetPassed, biScriptLaunched)
			ELSE
				PRINTLN("FM_Main_Menu - MISSION_CREATOR SCRIPT RUNNING - FM_Capture_Creator")	
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			IF g_bRunMissionCreatorInDebugMode
				SCRIPT_CLEANUP()
			ENDIF
		#ENDIF
		//IF GET_NUMBER_OF_THREADS_RUNNING_THIS_SCRIPT("FM_Mission_Creator") = 0
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Capture_Creator")) = 0
			CLEAR_BIT(iBitSetPassed, biScriptLaunched)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


//Launch the race editor script
//return true once it's been launched and it has then ended.
FUNC BOOL FM_LAUNCH_DEATHMATCH_CREATOR_SCRIPT(INT &iBitSetPassed)
	IF NOT IS_BIT_SET(iBitSetPassed, biScriptLaunched)
		REQUEST_SCRIPT("FM_Deathmatch_Creator")
	    IF HAS_SCRIPT_LOADED("FM_Deathmatch_Creator")
			IF !ARE_ANY_CREATOR_SCRIPTS_RUNNING()
				START_NEW_SCRIPT("FM_Deathmatch_Creator", MISSION_STACK_SIZE)// DEFAULT_STACK_SIZE)
		   		SET_SCRIPT_AS_NO_LONGER_NEEDED("FM_Deathmatch_Creator")
				SET_BIT(iBitSetPassed, biScriptLaunched)
			ELSE
				PRINTLN("DEATHMATCH_CREATOR SCRIPT RUNNING")
			ENDIF
		ENDIF
	ELSE
		//IF GET_NUMBER_OF_THREADS_RUNNING_THIS_SCRIPT("FM_Deathmatch_Creator") = 0
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Deathmatch_Creator")) = 0
			CLEAR_BIT(iBitSetPassed, biScriptLaunched)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
//Launch the race editor script
//return true once it's been launched and it has then ended.
FUNC BOOL FM_LAUNCH_RACE_CREATOR_SCRIPT(INT &iBitSetPassed)
	IF NOT IS_BIT_SET(iBitSetPassed, biScriptLaunched)
		REQUEST_SCRIPT("FM_Race_Creator")
	    IF HAS_SCRIPT_LOADED("FM_Race_Creator")
			IF !ARE_ANY_CREATOR_SCRIPTS_RUNNING()
				START_NEW_SCRIPT("FM_Race_Creator", MISSION_STACK_SIZE)// DEFAULT_STACK_SIZE)
		   		SET_SCRIPT_AS_NO_LONGER_NEEDED("FM_Race_Creator")
				SET_BIT(iBitSetPassed, biScriptLaunched)
			ELSE
				PRINTLN("RACE_CREATOR SCRIPT RUNNING")
			ENDIF
		ENDIF
	ELSE
		//IF GET_NUMBER_OF_THREADS_RUNNING_THIS_SCRIPT("FM_Race_Creator") = 0
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Race_Creator")) = 0
			CLEAR_BIT(iBitSetPassed, biScriptLaunched)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL FM_LAUNCH_SURVIVAL_CREATOR_SCRIPT(INT &iBitSetPassed)
	IF NOT IS_BIT_SET(iBitSetPassed, biScriptLaunched)
		REQUEST_SCRIPT("FM_Survival_Creator")
	    IF HAS_SCRIPT_LOADED("FM_Survival_Creator")
			IF !ARE_ANY_CREATOR_SCRIPTS_RUNNING()
				START_NEW_SCRIPT("FM_Survival_Creator", MISSION_STACK_SIZE)
		   		SET_SCRIPT_AS_NO_LONGER_NEEDED("FM_Survival_Creator")
				SET_BIT(iBitSetPassed, biScriptLaunched)
			ELSE
				PRINTLN("FM_Survival_Creator SCRIPT RUNNING")
			ENDIF
		ENDIF
	ELSE
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Survival_Creator")) = 0
			CLEAR_BIT(iBitSetPassed, biScriptLaunched)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL FM_LAUNCH_BASIC_CREATOR_SCRIPT(INT &iBitSetPassed)
	IF NOT IS_BIT_SET(iBitSetPassed, biScriptLaunched)
		REQUEST_SCRIPT("Basic_Creator")
	    IF HAS_SCRIPT_LOADED("Basic_Creator")
			IF !ARE_ANY_CREATOR_SCRIPTS_RUNNING()
				START_NEW_SCRIPT("Basic_Creator", MISSION_STACK_SIZE)
		   		SET_SCRIPT_AS_NO_LONGER_NEEDED("Basic_Creator")
				SET_BIT(iBitSetPassed, biScriptLaunched)
			ELSE
				PRINTLN("Basic_Creator SCRIPT RUNNING")
			ENDIF
		ENDIF
	ELSE
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Basic_Creator")) = 0
			CLEAR_BIT(iBitSetPassed, biScriptLaunched)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL FM_LAUNCH_FREEMODE_CREATOR_SCRIPT(INT &iBitSetPassed)
	IF NOT IS_BIT_SET(iBitSetPassed, biScriptLaunched)
		REQUEST_SCRIPT("Freemode_Creator")
	    IF HAS_SCRIPT_LOADED("Freemode_Creator")
			IF !ARE_ANY_CREATOR_SCRIPTS_RUNNING()
				START_NEW_SCRIPT("Freemode_Creator", MISSION_STACK_SIZE)
		   		SET_SCRIPT_AS_NO_LONGER_NEEDED("Freemode_Creator")
				SET_BIT(iBitSetPassed, biScriptLaunched)
			ELSE
				PRINTLN("Freemode_Creator SCRIPT RUNNING")
			ENDIF
		ENDIF
	ELSE
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Freemode_Creator")) = 0
			CLEAR_BIT(iBitSetPassed, biScriptLaunched)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////                  MAIN PROCS                  //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//#IF IS_DEBUG_BUILD
//PROC CREATE_WIDGETS()
//	wgGroup= wgGroup
//
//	wgGroup= START_WIDGET_GROUP( " FM_Main_Menu")  
//		ADD_WIDGET_BOOL("g_bShow_FMMC_rulesMenu",g_bShow_FMMC_rulesMenu)
//		START_WIDGET_GROUP( "Menu options") 
//			ADD_WIDGET_INT_READ_ONLY("OPTION_NUM_PLAYERS", FMMCmainMenuStruct.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_NUM_PLAYERS])
//			ADD_WIDGET_INT_READ_ONLY("OPTION_DURATION", FMMCmainMenuStruct.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_DURATION])				
//			ADD_WIDGET_INT_READ_ONLY("OPTION_TARGET", FMMCmainMenuStruct.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_TARGET])				
//			ADD_WIDGET_INT_READ_ONLY("OPTION_WEAPONS", FMMCmainMenuStruct.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_WEAPONS])				
//			ADD_WIDGET_INT_READ_ONLY("OPTION_WEAPON_RESPAWN", FMMCmainMenuStruct.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_WEAPON_RESPAWN])		
//			ADD_WIDGET_INT_READ_ONLY("OPTION_AUTO_AIM", FMMCmainMenuStruct.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_AUTO_AIM])				
//			ADD_WIDGET_INT_READ_ONLY("OPTION_POLICE", FMMCmainMenuStruct.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_POLICE])				
//			ADD_WIDGET_INT_READ_ONLY("OPTION_PEDS", FMMCmainMenuStruct.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_PEDS])					
//			ADD_WIDGET_INT_READ_ONLY("OPTION_BLIPS", FMMCmainMenuStruct.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_BLIPS])					
//			ADD_WIDGET_INT_READ_ONLY("OPTION_TAGS", FMMCmainMenuStruct.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_TAGS])					
//			ADD_WIDGET_INT_READ_ONLY("OPTION_RADIO", FMMCmainMenuStruct.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_RADIO])					
//			ADD_WIDGET_INT_READ_ONLY("OPTION_VOICE", FMMCmainMenuStruct.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_VOICE])				
//			ADD_WIDGET_INT_READ_ONLY("OPTION_SPAWN_TIME", FMMCmainMenuStruct.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_SPAWN_TIME])			
//			ADD_WIDGET_INT_READ_ONLY("OPTION_VEHICLES", FMMCmainMenuStruct.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_VEHICLES])				
//			//ADD_WIDGET_INT_READ_ONLY("OPTION_VEHICLES_RESPAWN", FMMCmainMenuStruct.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_VEHICLES_RESPAWN])		
//		STOP_WIDGET_GROUP()
//	
//		ADD_WIDGET_INT_SLIDER("iMissionType", g_FMMC_STRUCT.iMissionType, 0, 3, 1)
//		INT i 
//		TEXT_LABEL_63 tl63
//		ADD_WIDGET_BOOL("bDoSave",bDoSave)
//		ADD_WIDGET_BOOL("bDoLOAD",bDoLOAD)
//		ADD_WIDGET_BOOL("bInitiliseFMMCstruct",bInitiliseFMMCstruct)
//		ADD_WIDGET_BOOL("bDoClear",bDoClear)
//		ADD_WIDGET_FLOAT_SLIDER("fX",fXMovePos,-1.0, 1.0, 0.001)
//		ADD_WIDGET_FLOAT_SLIDER("fY",fYMovePos,-1.0, 1.0, 0.001)
//		START_WIDGET_GROUP( "g_FMMC_HEADER_STRUCT") 
//			ADD_WIDGET_INT_SLIDER("iNumberOfMissions", g_FMMC_HEADER_STRUCT.iNumberOfMissions, -1, 100, 1)
//			FOR i = 0 TO (MAX_NUMBER_FMMC_SAVES - 1)
//				tl63 = "startPos["
//				tl63 += i
//				tl63 += "]"
//				ADD_WIDGET_VECTOR_SLIDER(tl63, g_FMMC_HEADER_STRUCT.startPos[i], -9999.0, 9999.0,0.100 )
//				tl63 = "startMin["
//				tl63 += i
//				tl63 += "]"
//				ADD_WIDGET_INT_SLIDER(tl63, g_FMMC_HEADER_STRUCT.startMin[i], -1, 100, 1)
//				tl63 = "startMax["
//				tl63 += i
//				tl63 += "]"
//				ADD_WIDGET_INT_SLIDER(tl63, g_FMMC_HEADER_STRUCT.startMax[i], -1, 100, 1)
//			ENDFOR
//		STOP_WIDGET_GROUP()
//		ADD_WIDGET_INT_SLIDER("iServerMissionStageWidget", iServerMissionStageWidget, 0, 99, 1)
//		START_WIDGET_GROUP( "Server BD") 
//			START_WIDGET_GROUP( "vCheckPoint") 
//				FOR i = 0 TO (GET_FMMC_MAX_NUM_CHECKPOINTS() - 1)
//					tl63 = "vCheckPoint"
//					tl63 += i
//					ADD_WIDGET_VECTOR_SLIDER(tl63, g_FMMC_STRUCT.sPlacedCheckpoint[i].vCheckPoint, -9999.0, 9999.0,0.100 )
//				ENDFOR
//			STOP_WIDGET_GROUP()
//			START_WIDGET_GROUP( "sPlacedObject") 
//				FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS - 1)
//					tl63 = "g_FMMC_STRUCT_ENTITIES.sPlacedObject"
//					tl63 += i
//					ADD_WIDGET_VECTOR_SLIDER(tl63, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vPos, -9999.0, 9999.0,0.100 )
//				ENDFOR
//			STOP_WIDGET_GROUP()
//			ADD_WIDGET_INT_SLIDER("iNumberOfCheckPoints", g_FMMC_STRUCT.iNumberOfCheckPoints, -1, 100, 1)
//			ADD_WIDGET_INT_SLIDER("iNumberOfObjects", g_FMMC_STRUCT_ENTITIES.iNumberOfObjects, -1, 100, 1)
//			ADD_WIDGET_INT_SLIDER("iNumParticipants", g_FMMC_STRUCT.iNumParticipants, -1, 100, 1)
//			ADD_WIDGET_INT_SLIDER("iNumLaps", g_FMMC_STRUCT.iNumLaps, -1, 100, 1)
//		STOP_WIDGET_GROUP()
//		START_WIDGET_GROUP( "iMenuOption")  
//			INT i2
//			FOR  i = 0 TO (FMMC_TAB_MAXIMUM_NUMBER - 1)
//				FOR  i2 = 0 TO (FMMC_MAX_NUM_CATEGORIES - 1)
//					tl63 = "iMenuOption - "
//					tl63 += i
//					tl63 += " - "
//					tl63 += i2
//					ADD_WIDGET_INT_SLIDER(tl63, FMMCmainMenuStruct.iMenuOption[i][i2], -100, 10000, 1 )
//				ENDFOR
//			ENDFOR
//		STOP_WIDGET_GROUP()
//		START_WIDGET_GROUP( "iMenuRules")  
//			FOR  i = 0 TO (FMMC_TAB_MAXIMUM_NUMBER - 1)
//				FOR  i2 = 0 TO (FMMC_MAX_NUM_CATEGORIES - 1)
//					tl63 = "iMenuRules - "
//					tl63 += i
//					tl63 += " - "
//					tl63 += i2
//					ADD_WIDGET_INT_SLIDER(tl63, FMMCmainMenuStruct.iMenuRules[i][i2], -100, 10000, 1 )
//				ENDFOR
//			ENDFOR
//		STOP_WIDGET_GROUP()
//	STOP_WIDGET_GROUP()
//ENDPROC		
//
//PROC UPDATE_WIDGETS(MAIN_SERVER_MISSION_STAGES MissionStagePassed)
//	//Setup 
//	iServerMissionStageWidget =	ENUM_TO_INT(MissionStagePassed)
//ENDPROC
//
//#ENDIF
////Cleanup Mission Data
//PROC SCRIPT_CLEANUP()
//	enable_selector()
//	SET_VEHICLE_DENSITY_MULTIPLIER(1.0)
//	SET_PED_DENSITY_MULTIPLIER(1.0)
//	SET_GRAVITY_LEVEL(GRAV_EARTH)	
//	g_FMMC_STRUCT.bKillFMMC = FALSE
//	g_bOkToExitMissionCreator = TRUE
//	g_bKillFMMCmenu = FALSE
//	TERMINATE_THIS_THREAD()
//ENDPROC
////
//
//
//
//
//
//
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////              DRAW                   		 ////////////////////////////////////////////
//FUNC RECT GET_NAME_SLOT_RECT_PLACEMENT_DM(RECT BaseRect, INT iSlot)
//	
//	RECT TempRect = BaseRect 
//
//	FLOAT BaseY = 0.244 //+ 0.0375
//	FLOAT YStep = 0.0375
//
//	TempRect.y = BaseY + (iSlot*YStep)
//	
//	RETURN TempRect
//ENDFUNC
//
//FUNC TEXT_PLACEMENT GET_NAME_SLOT_TEXT_PLACEMENT_DM(TEXT_PLACEMENT BaseText, INT iSlot)
//	
//	TEXT_PLACEMENT TempText = BaseText 
//
//	FLOAT BaseY = 0.233
//	FLOAT YStep = 0.0375
//
//	TempText.y = BaseY + (iSlot*YStep)
//	
//	RETURN TempText
//ENDFUNC
//
//FUNC TEXT_PLACEMENT GET_NAME_SLOT_TEXT_PLACEMENT_DM_CATEGORIES(TEXT_PLACEMENT BaseText, INT iSlot)
//	
//	TEXT_PLACEMENT TempText = BaseText 
//
//	FLOAT BaseY = 0.196//0.2335
//	FLOAT YStep = 0.0375
//
//	TempText.y = BaseY + (iSlot*YStep)
//	
//	RETURN TempText
//ENDFUNC
//
//FUNC SPRITE_PLACEMENT GET_VOICE_ICON_PLACEMENT_DM(SPRITE_PLACEMENT BaseSprite, INT iSlot)
//
//	SPRITE_PLACEMENT TempSprite = BaseSprite 
//
//	FLOAT BaseY = 0.244
//	FLOAT YStep = 0.0375
//
//	TempSprite.y = BaseY + (iSlot*YStep)
//	
//	RETURN TempSprite
//ENDFUNC
//
//FUNC SPRITE_PLACEMENT GET_ARROW_PLACEMENT_DM(SPRITE_PLACEMENT BaseSprite, INT iSlot)
//
//	SPRITE_PLACEMENT TempSprite = BaseSprite 
//
//	FLOAT BaseY = 0.244//+0.0375
//	FLOAT YStep = 0.0375
//
//	TempSprite.y = BaseY + (iSlot*YStep)
//	
//	RETURN TempSprite
//ENDFUNC
//
//FUNC SPRITE_PLACEMENT GET_CROWN_PLACEMENT_DM(SPRITE_PLACEMENT BaseSprite, INT iSlot)
//
//	SPRITE_PLACEMENT TempSprite = BaseSprite 
//
//	FLOAT BaseY = 0.244
//	FLOAT YStep = 0.0375
//
//	TempSprite.y = BaseY + (iSlot*YStep)
//	
//	RETURN TempSprite
//ENDFUNC
//
//FUNC SPRITE_PLACEMENT GET_TICK_PLACEMENT_DM(SPRITE_PLACEMENT BaseSprite, INT iSlot)
//
//	SPRITE_PLACEMENT TempSprite = BaseSprite 
//
//	FLOAT BaseY = 0.244
//	FLOAT YStep = 0.0375
//
//	TempSprite.y = BaseY + (iSlot*YStep)
//	
//	RETURN TempSprite
//ENDFUNC
//
//FUNC SPRITE_PLACEMENT CHANGE_SPRITE_COLOUR(SPRITE_PLACEMENT BaseSprite, INT iColour, INT iAlpha)
//	
//	SPRITE_PLACEMENT TempSprite = BaseSprite
//	TempSprite.r = iColour
//	TempSprite.g = iColour
//	TempSprite.b = iColour
//	TempSprite.a = iAlpha
//	
//	RETURN TempSprite
//ENDFUNC
//
//FUNC SPRITE_PLACEMENT GET_TAB_LOCK_SPRITE_POS(SPRITE_PLACEMENT BaseSprite, FLOAT fXPos)
//	
//	SPRITE_PLACEMENT TempSprite = BaseSprite
//	TempSprite.x = fXPos
//
//	RETURN TempSprite
//ENDFUNC
//
//FUNC SPRITE_PLACEMENT GET_SCROLL_ARROW_SPRITE_POS(SPRITE_PLACEMENT BaseSprite, FLOAT fXPos, FLOAT fYPos, FLOAT fRot)
//	
//	SPRITE_PLACEMENT TempSprite = BaseSprite
//	TempSprite.x = fXPos
//	TempSprite.y = fYPos
//	TempSprite.fRotation = fRot
//
//	RETURN TempSprite
//ENDFUNC
//
//FUNC SPRITE_PLACEMENT GET_SELECTION_ARROW_SPRITE_POS(SPRITE_PLACEMENT BaseSprite, FLOAT fXPos, FLOAT fWPos)
//	
//	SPRITE_PLACEMENT TempSprite = BaseSprite
//	TempSprite.x = fXPos
//	TempSprite.w = fWPos
//
//	RETURN TempSprite
//ENDFUNC
//
//PROC INITIALISE_DM_LOBBY_VARIABLES(LARGE_PLACEMENT_TOOLS& Placement)
//
//	SET_STANDARD_INGAME_TEXT_DETAILS(Placement.aStyle)
//	
//	//////////////////////////////              SPRITES  
//	
//	////////////////	MINIMAP
//	Placement.SpritePlacement[0].x = 0.698
//	Placement.SpritePlacement[0].y = 0.358
//	Placement.SpritePlacement[0].w = 0.197
//	Placement.SpritePlacement[0].h = 0.263
//	Placement.SpritePlacement[0].r = 255
//	Placement.SpritePlacement[0].g = 255
//	Placement.SpritePlacement[0].b = 255
//	Placement.SpritePlacement[0].a = 255
//		
//	////////////////	SCALEFORM BOTTOM HELP BAR
//	Placement.SpritePlacement[1].x = 0.390
//	Placement.SpritePlacement[1].y = 0.467
//	Placement.SpritePlacement[1].w = 1.0
//	Placement.SpritePlacement[1].h = 1.0
//	Placement.SpritePlacement[1].r = 255
//	Placement.SpritePlacement[1].g = 255
//	Placement.SpritePlacement[1].b = 255
//	Placement.SpritePlacement[1].a = 200
//	
//	////////////////	SELECTION ARROW LEFT
//	Placement.SpritePlacement[2].x = 0.514
//	Placement.SpritePlacement[2].y = 0.245
//	Placement.SpritePlacement[2].w = -0.010
//	Placement.SpritePlacement[2].h = 0.026
//	Placement.SpritePlacement[2].r = 255
//	Placement.SpritePlacement[2].g = 255
//	Placement.SpritePlacement[2].b = 255
//	Placement.SpritePlacement[2].a = 204
//	
//	////////////////	CROWN
//	Placement.SpritePlacement[3].x = 0.300
//	Placement.SpritePlacement[3].y = 0.244
//	Placement.SpritePlacement[3].w = 0.020
//	Placement.SpritePlacement[3].h = 0.035
//	Placement.SpritePlacement[3].r = 255
//	Placement.SpritePlacement[3].g = 255
//	Placement.SpritePlacement[3].b = 255
//	Placement.SpritePlacement[3].a = 255
//	
//	////////////////	BOOT
//	Placement.SpritePlacement[4].x = 0.280
//	Placement.SpritePlacement[4].y = 0.244
//	Placement.SpritePlacement[4].w = 0.020
//	Placement.SpritePlacement[4].h = 0.035
//	Placement.SpritePlacement[4].r = 255
//	Placement.SpritePlacement[4].g = 255
//	Placement.SpritePlacement[4].b = 255
//	Placement.SpritePlacement[4].a = 255
//	
//	////////////////	SPEAKER
//	Placement.SpritePlacement[5].x = 0.321
//	Placement.SpritePlacement[5].y = 0.244
//	Placement.SpritePlacement[5].w = 0.020
//	Placement.SpritePlacement[5].h = 0.035
//	Placement.SpritePlacement[5].r = 255
//	Placement.SpritePlacement[5].g = 255
//	Placement.SpritePlacement[5].b = 255
//	Placement.SpritePlacement[5].a = 255
//	
//	////////////////	TICK
//	Placement.SpritePlacement[6].x = 0.364
//	Placement.SpritePlacement[6].y = 0.244
//	Placement.SpritePlacement[6].w = 0.020
//	Placement.SpritePlacement[6].h = 0.035
//	Placement.SpritePlacement[6].r = 255
//	Placement.SpritePlacement[6].g = 255
//	Placement.SpritePlacement[6].b = 255
//	Placement.SpritePlacement[6].a = 255
//	
//	////////////////	SCROLL ARROWS /\ COL1
//	Placement.SpritePlacement[7].x = 0.304
//	Placement.SpritePlacement[7].y = 0.841
//	Placement.SpritePlacement[7].w = 0.017
//	Placement.SpritePlacement[7].h = 0.015
//	Placement.SpritePlacement[7].r = 255
//	Placement.SpritePlacement[7].g = 255
//	Placement.SpritePlacement[7].b = 255
//	Placement.SpritePlacement[7].a = 255
//	Placement.SpritePlacement[7].fRotation = 270
//	
//	////////////////	LOADING ICON
//	Placement.SpritePlacement[8].x = 0.387
//	Placement.SpritePlacement[8].y = 0.903
//	Placement.SpritePlacement[8].w = 0.450
//	Placement.SpritePlacement[8].h = 0.041
//	Placement.SpritePlacement[8].r = 255
//	Placement.SpritePlacement[8].g = 255
//	Placement.SpritePlacement[8].b = 255
//	Placement.SpritePlacement[8].a = 200
//	
//	////////////////	LOCK TABS
//	Placement.SpritePlacement[9].x = 0.487
//	Placement.SpritePlacement[9].y = 0.187
//	Placement.SpritePlacement[9].w = 0.0110
//	Placement.SpritePlacement[9].h = 0.0190
//	Placement.SpritePlacement[9].r = 255
//	Placement.SpritePlacement[9].g = 255
//	Placement.SpritePlacement[9].b = 255
//	Placement.SpritePlacement[9].a = 200
//	
//	//////////////////////////////    	          TEXT  
//	
//
//
//	////////////////	OPTION COLOUM title		   
//	Placement.TextPlacement[FMMC_MENU_OPTIONS_TITLE_COLOUM_0].x	 	= 0.300	
//	Placement.TextPlacement[FMMC_MENU_OPTIONS_TITLE_COLOUM_0].y	 	= 0.233	
//	////////////////	RULE COLOUM title	   
//	Placement.TextPlacement[FMMC_MENU_OPTIONS_TITLE_COLOUM_1].x	 	= 0.500
//	Placement.TextPlacement[FMMC_MENU_OPTIONS_TITLE_COLOUM_1].y	 	= 0.233		
//	////////////////	OPTIONS names
//	Placement.TextPlacement[FMMC_MENU_OPTIONS_NAMES_COLOUM_0].x	 	= 0.219
//	Placement.TextPlacement[FMMC_MENU_OPTIONS_NAMES_COLOUM_0].y	 	= 0.196
//	////////////////	RULES names
//	Placement.TextPlacement[FMMC_MENU_OPTIONS_NAMES_COLOUM_1].x	 	= 0.419
//	Placement.TextPlacement[FMMC_MENU_OPTIONS_NAMES_COLOUM_1].y	 	= 0.196	
//	////////////////	OPTIONS selection
//	Placement.TextPlacement[FMMC_MENU_SELECTED_OPTIONS_COLOUM_0].x 	= 0.320		
//	Placement.TextPlacement[FMMC_MENU_SELECTED_OPTIONS_COLOUM_0].y 	= 0.2705	
//	////////////////	RULES	selection	
//	Placement.TextPlacement[FMMC_MENU_SELECTED_OPTIONS_COLOUM_1].x 	= 0.520 
//	Placement.TextPlacement[FMMC_MENU_SELECTED_OPTIONS_COLOUM_1].y 	= 0.2705								  	
//		
//	//REctangles
//	////////////////	OPTIONS BACKGROUNDS
//	Placement.RectPlacement[ciOptionBG].x = 0.298
//	Placement.RectPlacement[ciOptionBG].y = 0.281
//	Placement.RectPlacement[ciOptionBG].w = 0.197
//	Placement.RectPlacement[ciOptionBG].h = 0.035
//	Placement.RectPlacement[ciOptionBG].r = 0
//	Placement.RectPlacement[ciOptionBG].g = 0
//	Placement.RectPlacement[ciOptionBG].b = 0
//	Placement.RectPlacement[ciOptionBG].a = 90	
//	
//	//REctangles
//	////////////////	Rules BACKGROUNDS
//	Placement.RectPlacement[ciRulesBG].x = 0.498
//	Placement.RectPlacement[ciRulesBG].y = 0.281
//	Placement.RectPlacement[ciRulesBG].w = 0.197
//	Placement.RectPlacement[ciRulesBG].h = 0.035
//	Placement.RectPlacement[ciRulesBG].r = 0
//	Placement.RectPlacement[ciRulesBG].g = 0
//	Placement.RectPlacement[ciRulesBG].b = 0
//	Placement.RectPlacement[ciRulesBG].a = 90	
//	
//	
//	////////////////	SCOREBOX TITLE
//	Placement.RectPlacement[1].x = 0.698
//	Placement.RectPlacement[1].y = 0.694
//	Placement.RectPlacement[1].w = 0.197
//	Placement.RectPlacement[1].h = 0.035
//	Placement.RectPlacement[1].r = 0
//	Placement.RectPlacement[1].g = 0
//	Placement.RectPlacement[1].b = 0
//	Placement.RectPlacement[1].a = 190
//
//	////////////////	SCOREBOX MAIN
//	Placement.RectPlacement[2].x = 0.698
//	Placement.RectPlacement[2].y = 0.767
//	Placement.RectPlacement[2].w = 0.197
//	Placement.RectPlacement[2].h = 0.112
//	Placement.RectPlacement[2].r = 0
//	Placement.RectPlacement[2].g = 0
//	Placement.RectPlacement[2].b = 0
//	Placement.RectPlacement[2].a = 90
//	
//	////////////////	HOW TO PLAY TITLE
//	Placement.RectPlacement[3].x = 0.698
//	Placement.RectPlacement[3].y = 0.506
//	Placement.RectPlacement[3].w = 0.197
//	Placement.RectPlacement[3].h = 0.035
//	Placement.RectPlacement[3].r = 0
//	Placement.RectPlacement[3].g = 0
//	Placement.RectPlacement[3].b = 0
//	Placement.RectPlacement[3].a = 190
//	
//	////////////////	HOW TO PLAY MAIN
//	Placement.RectPlacement[4].x = 0.698
//	Placement.RectPlacement[4].y = 0.600
//	Placement.RectPlacement[4].w = 0.197
//	Placement.RectPlacement[4].h = 0.153
//	Placement.RectPlacement[4].r = 0
//	Placement.RectPlacement[4].g = 0
//	Placement.RectPlacement[4].b = 0
//	Placement.RectPlacement[4].a = 90
//	
//	////////////////	PLAYER BACKGROUNDS
//	Placement.RectPlacement[5].x = 0.298
//	Placement.RectPlacement[5].y = 0.244
//	Placement.RectPlacement[5].w = 0.197
//	Placement.RectPlacement[5].h = 0.035
//	Placement.RectPlacement[5].r = 0
//	Placement.RectPlacement[5].g = 0
//	Placement.RectPlacement[5].b = 0
//	Placement.RectPlacement[5].a = 204
//	
////	Placement.RectPlacement[6]
//	
//	////////////////	MINIMAP
//	Placement.RectPlacement[7].x = 0.698
//	Placement.RectPlacement[7].y = 0.358
//	Placement.RectPlacement[7].w = 0.197
//	Placement.RectPlacement[7].h = 0.263
//	Placement.RectPlacement[7].r = 0
//	Placement.RectPlacement[7].g = 0
//	Placement.RectPlacement[7].b = 0
//	Placement.RectPlacement[7].a = 204
//ENDPROC
//
//// Draw the lobby tabs along the top
//PROC RENDER_DM_LOBBY_TABS(LARGE_PLACEMENT_TOOLS& Placement, TAB_PLACEMENT_TOOLS& TabPlacement, FMMC_MAIN_MENU_STRUCT &FMMCpassed, STRING textLabel)
//	
//	TEXT_LABEL_23 iLabel
//	INT SlotCount = 0
//	INT i
//	
//	// Initialise data
//	IF NOT IS_BIT_SET(FMMCpassed.iLobbyBitset, FMMC_MENU_BITSET_INITIALISED_DM)
//
//		UNLOAD_ALL_HUD_TEXTURES()
//		
//		Placement.ScreenPlace.iSelection = 0
//		TabPlacement.iNumberOfTabs = FMMC_TAB_MAXIMUM_NUMBER
//		//from_player_i
//		SET_STANDARD_HUD_TEXT_DETAILS(Placement)
//		
//		// Init tabs
//		FOR I = 0 TO FMMC_TAB_MAXIMUM_NUMBER-1
//			IF SlotCount < 5
//				iLabel = textLabel
//				iLabel += SlotCount 
//				
//				TabPlacement.TabString[i] = iLabel
//				SET_ALL_TABS_TO_TEAM_COLOUR(0, TabPlacement)
//			//	SET_TAB_TO_PLAYER_COLOUR(i, TabPlacement)
//				SlotCount++
//			ENDIF
//		ENDFOR	
//		
//		INIT_TABS(TabPlacement)	
//		UNLOCK_ALL_TABS(TabPlacement)
//		SET_BIT(FMMCpassed.iLobbyBitset, FMMC_MENU_BITSET_INITIALISED_DM)
//	ENDIF
//
//	RENDER_TABS(TabPlacement, FALSE)
//
//ENDPROC
//
//// Draw the help bar along the bottom
//PROC RENDER_DM_SCALEFORM_BAR(LARGE_PLACEMENT_TOOLS& Placement)
//	DRAW_SCALEFORM_MOVIE(	Placement.ButtonMovie, Placement.SpritePlacement[1].x,Placement.SpritePlacement[1].y,
//							Placement.SpritePlacement[1].w,Placement.SpritePlacement[1].h,Placement.SpritePlacement[1].r,
//							Placement.SpritePlacement[1].g,	Placement.SpritePlacement[1].b,Placement.SpritePlacement[1].a)
//// Switch tabs 'LB RB'
//	BEGIN_SCALEFORM_MOVIE_METHOD(Placement.ButtonMovie, "SET_DATA_SLOT")
//	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
//	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_RB)))
//	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_LB)))
//	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FMMC_MC3")
//	END_SCALEFORM_MOVIE_METHOD()
//// Dpad adjust left, right
//	BEGIN_SCALEFORM_MOVIE_METHOD(Placement.ButtonMovie, "SET_DATA_SLOT")
//	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
//	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(GET_CONTROL_INSTRUCTIONAL_BUTTON_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_ALL)))
//	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FMMC_MC0")
//	END_SCALEFORM_MOVIE_METHOD()
////Start creation
//	BEGIN_SCALEFORM_MOVIE_METHOD(Placement.ButtonMovie, "SET_DATA_SLOT")
//	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
//	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)))
//	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FMMC_MC1")
//	END_SCALEFORM_MOVIE_METHOD()
//// 'Circle' back
//	BEGIN_SCALEFORM_MOVIE_METHOD(Placement.ButtonMovie, "SET_DATA_SLOT")
//	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)))
//	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FMMC_MC2")
//	END_SCALEFORM_MOVIE_METHOD()
//	
//	CALL_SCALEFORM_MOVIE_METHOD(Placement.ButtonMovie, "DRAW_INSTRUCTIONAL_BUTTONS")
//ENDPROC
//
//
//PROC RENDER_DM_OPTION(LARGE_PLACEMENT_TOOLS& Placement, TEXT_STYLE& Style, FMMC_MAIN_MENU_STRUCT &FMMCpassed, INT iMainOption, INT iSubOption, STRING textLabel) //BOOL bIsRuleOption = FALSE)
//	TEXT_LABEL_23 iLabel
//	iLabel = textLabel
//	iLabel += iSubOption
//
//	Placement.TextPlacement[FMMC_MENU_SELECTED_OPTIONS_COLOUM_0] = GET_NAME_SLOT_TEXT_PLACEMENT_DM_CATEGORIES(Placement.TextPlacement[FMMC_MENU_SELECTED_OPTIONS_COLOUM_0], iMainOption + 1)
//	
//	IF FMMCpassed.iCategorySelected = iMainOption 
//		SET_TEXT_BLACK(Style)
//		Style.drop = DROPSTYLE_NONE
//		DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[FMMC_MENU_SELECTED_OPTIONS_COLOUM_0], Style, iLabel, FALSE, FALSE)
//		SET_TEXT_WHITE(Style)
//	ELSE
//		SET_TEXT_WHITE(Style)
//		DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[FMMC_MENU_SELECTED_OPTIONS_COLOUM_0], Style, iLabel, FALSE, FALSE)
//	ENDIF	
//ENDPROC
//
//// Draw selectable options next to categories
//PROC RENDER_ALL_MISSION_SUB_OPTIONS(LARGE_PLACEMENT_TOOLS& Placement, TAB_PLACEMENT_TOOLS& TabPlacement, TEXT_STYLE& Style, FMMC_MAIN_MENU_STRUCT &FMMCpassed)
//	SWITCH TabPlacement.iSelectedTab
//		CASE FMMC_TAB_GAME_SET_UP
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_MISSION_NO_PLAYERS, 			FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_MISSION_NO_PLAYERS], 			"FMMC_T0_M4_O")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_MISSION_NO_TEAMS, 			FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_MISSION_NO_TEAMS], 			"FMMC_T0_M4_O")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_MISSION_PLAYER_RESPAWN, 		FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_MISSION_PLAYER_RESPAWN], 		"FMMC_T0_M2_O")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_MISSION_NUMBER_OF_LIVES,		FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_MISSION_NUMBER_OF_LIVES],		"FMMC_T0_M9_R")		
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_MISSION_WEAPONS, 				FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_MISSION_WEAPONS], 			"FMMC_T0_M5_O") 
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_MISSION_WEAPON_RESPAWN, 		FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_MISSION_WEAPON_RESPAWN], 		"FMMC_T0_M2_O")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_MISSION_VEHICLES, 			FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_MISSION_VEHICLES], 			"FMMC_T0_M6_O")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_MISSION_ENEMIES, 				FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_MISSION_ENEMIES], 			"FMMC_T0_M7_O")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_MISSION_ENEMY_RESPAWN, 		FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_MISSION_ENEMY_RESPAWN],		"FMMC_T0_M2_O")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_MISSION_OBJECTS, 				FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_MISSION_OBJECTS],				"FMMC_T0_M8_O")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_MISSION_SPAWN_AREA, 			FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_MISSION_SPAWN_AREA],			"FMMC_T0_M9_A")
//		BREAK
//		CASE FMMC_TAB_RULES	
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_NUMBER_OF_KILLS,				FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][0], 	"FMMC_T0_M1_R")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_NUMBER_OF_DEATHS,				FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][1], 	"FMMC_T0_M2_R")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_COLLECT_OBJECT,					FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][2],	"FMMC_T0_M3_R")					
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_COLLECT_VEHICLE,				FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][3], 	"FMMC_T0_M4_R")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_COLLECT_ENEMY,					FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][4], 	"FMMC_T0_M5_R") 
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_FIRST_TO_LOCATION,				FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][5],	"FMMC_T0_M6_R")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_LAST_MAN_STANDING,				FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][6], 	"FMMC_T0_M7_R")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_TARGET_PEDS,					FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][7],	"FMMC_T0_M8_R")
//		BREAK
//		CASE FMMC_TAB_OPTIONS
//		BREAK	
//	ENDSWITCH
//ENDPROC
//// Draw selectable options next to categories
//PROC RENDER_ALL_DM_SUB_OPTIONS(LARGE_PLACEMENT_TOOLS& Placement, TAB_PLACEMENT_TOOLS& TabPlacement, TEXT_STYLE& Style, FMMC_MAIN_MENU_STRUCT &FMMCpassed)
//	SWITCH TabPlacement.iSelectedTab
//		CASE FMMC_TAB_GAME_SET_UP															///	FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][FMMCpassed.iCategorySelected]
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,	OPTION_NUM_PLAYERS,					FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_NUM_PLAYERS], 		"FMMC_T0_M1_O")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,	OPTION_TEAM_DEATHMATCH,				FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_TEAM_DEATHMATCH],		"RL_AUTO_AIM_")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,	OPTION_DURATION,					FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_DURATION], 			"LOB_DURATION_")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,	OPTION_TARGET,						FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_TARGET],				"LOB_TARG_KIL_")				
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,	OPTION_SPAWN_TIME,					FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_SPAWN_TIME],			"FMMC_T0_M2_O")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,	OPTION_WEAPONS,						FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_WEAPONS],				"LOB_WEAPONS_")				
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,	OPTION_WEAPON_RESPAWN,				FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_WEAPON_RESPAWN], 		"LOB_SWNWP_") 
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,	OPTION_AUTO_AIM,					FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_AUTO_AIM], 			"LOB_AIM_")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,	OPTION_POLICE,						FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_POLICE],	 			"LOB_POLICE_")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,	OPTION_PEDS,						FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_PEDS],	 			"LOB_PEDS_")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,	OPTION_BLIPS,						FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_BLIPS], 				"LOB_BLIPS_")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,	OPTION_TAGS,						FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_TAGS], 				"LOB_TAGS_")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,	OPTION_RADIO,						FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_RADIO],				"LOB_RADIO_")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,	OPTION_VOICE,						FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_VOICE],				"LOB_VOICE_")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,	OPTION_VEHICLES,					FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_VEHICLES],			"FMMC_T0_M6_O")
//			//RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,	OPTION_VEHICLES_RESPAWN,			FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_VEHICLES_RESPAWN],	"FMMC_T0_M2_O")
//		BREAK
//		CASE FMMC_TAB_RULES	
////			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_NUMBER_OF_KILLS,				FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][0], 	"FMMC_T0_M1_R")
////			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_NUMBER_OF_DEATHS,				FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][1], 	"FMMC_T0_M2_R")
////			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_COLLECT_OBJECT,					FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][2],	"FMMC_T0_M3_R")					
////			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_COLLECT_VEHICLE,				FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][3], 	"FMMC_T0_M4_R")
////			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_COLLECT_ENEMY,					FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][4], 	"FMMC_T0_M5_R") 
////			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_FIRST_TO_LOCATION,				FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][5],	"FMMC_T0_M6_R")
////			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_LAST_MAN_STANDING,				FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][6], 	"FMMC_T0_M7_R")
////			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_TARGET_PEDS,					FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][7],	"FMMC_T0_M8_R")
//		BREAK
//		CASE FMMC_TAB_OPTIONS
//		BREAK
//	ENDSWITCH
//ENDPROC
//
//// Draw selectable options next to categories
//PROC RENDER_ALL_RACE_SUB_OPTIONS(LARGE_PLACEMENT_TOOLS& Placement, TAB_PLACEMENT_TOOLS& TabPlacement, TEXT_STYLE& Style, FMMC_MAIN_MENU_STRUCT &FMMCpassed)
//	SWITCH TabPlacement.iSelectedTab
//		CASE FMMC_TAB_GAME_SET_UP
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_RACE_NO_PLAYERS, 				FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_RACE_NO_PLAYERS], 	"FMMC_T0_M1_O")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_RACE_TYPE,					FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_RACE_TYPE],			"FMMC_T2_MRT0")	
//			IF FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_RACE_VEHICLE_CLASS] = -1
//				RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_RACE_VEHICLE_CLASS, 		 	0, 	"FMMC_VEH_CL_A")
//			ELSE
//				RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_RACE_VEHICLE_CLASS, 		 	FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_RACE_VEHICLE_CLASS], 	GET_RACE_BASE_STRING(FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_RACE_TYPE]))
//			ENDIF
//			IF FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_RACE_DEFAULT_VEHICLE] = -1
//				RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_RACE_DEFAULT_VEHICLE, 		0,"FMMC_VEH_CL_A")				
//			ELSE
//				RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_RACE_DEFAULT_VEHICLE, 		FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_RACE_DEFAULT_VEHICLE],GET_VEHICLE_BASE_STRING_FROM_RACE_TYPE(FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_RACE_TYPE], FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_RACE_VEHICLE_CLASS]))				
//			ENDIF
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_RACE_PLAYER_RESPAWN, 			FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_RACE_PLAYER_RESPAWN], "FMMC_T0_M2_O")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_RACE_NO_LAPS,					FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_RACE_NO_LAPS],		"FMMC_T0_M4_O")	
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_RACE_TRAFFIC, 		 		FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_RACE_TRAFFIC], 		"FMMC_T0_M5_O")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_RACE_WEAPONS,		 			FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_RACE_WEAPONS], 		"FMMC_T0_M5_O")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,OPTION_RACE_WEAPON_RESPAWN, 			FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][OPTION_RACE_WEAPON_RESPAWN], "FMMC_T0_M2_O")
//		BREAK
//		CASE FMMC_TAB_RULES		
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_NUMBER_OF_KILLS,				FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][0], 	"FMMC_T0_M1_R")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_NUMBER_OF_DEATHS,				FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][1], 	"FMMC_T0_M2_R")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_COLLECT_OBJECT,					FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][2],	"FMMC_T0_M3_R")					
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_COLLECT_VEHICLE,				FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][3], 	"FMMC_T0_M4_R")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_COLLECT_ENEMY,					FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][4], 	"FMMC_T0_M5_R") 
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_FIRST_TO_LOCATION,				FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][5],	"FMMC_T0_M6_R")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_LAST_MAN_STANDING,				FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][6], 	"FMMC_T0_M7_R")
//			RENDER_DM_OPTION(Placement,	Style, 	FMMCpassed,RULE_TARGET_PEDS,					FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][7],	"FMMC_T0_M8_R")
//		BREAK
//		CASE FMMC_TAB_OPTIONS
//		BREAK
//	ENDSWITCH
//ENDPROC
//
//
//
//PROC RENDER_ALL_OPTIONS_SUB_OPTIONS(LARGE_PLACEMENT_TOOLS& Placement, TAB_PLACEMENT_TOOLS& TabPlacement, TEXT_STYLE& Style)
//	TabPlacement = TabPlacement
//	Style = Style
//	// Draw Options tites
//	SET_TEXT_WHITE(Placement.aStyle.TS_STANDARDMEDIUMHUD)
//	DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[FMMC_MENU_OPTIONS_TITLE_COLOUM_0], Placement.aStyle.TS_STANDARDMEDIUMHUD, "FMMC_OC_0", FALSE, FALSE)
//ENDPROC
//// Draw kick boot
//PROC RENDER_DM_BOOT_ICON(LARGE_PLACEMENT_TOOLS& Placement)
//	DRAW_2D_SPRITE("MPLobby", "MP_CharCard_Stats_Icons7", Placement.SpritePlacement[4], FALSE)
//ENDPROC
//
//// Scrolling arrows /\\/
//PROC RENDER_DM_SCROLLING_ARROWS(LARGE_PLACEMENT_TOOLS& Placement, FMMC_MAIN_MENU_STRUCT &FMMCpassed)
//	REQUEST_STREAMED_TEXTURE_DICT("MPGangSelect")
//	SPRITE_PLACEMENT sTempPlacement
//
//	IF FMMCpassed.iColoumSelected = FMMC_OPTIONS_COLOUM
//		sTempPlacement = GET_SCROLL_ARROW_SPRITE_POS(Placement.SpritePlacement[7], 0.300, 0.841, 270)
//		DRAW_2D_SPRITE("MPGangSelect", "MP_ArrowLarge", sTempPlacement, FALSE)
//
//		sTempPlacement = GET_SCROLL_ARROW_SPRITE_POS(Placement.SpritePlacement[7], 0.3005, 0.858, 90)
//		DRAW_2D_SPRITE("MPGangSelect", "MP_ArrowLarge", sTempPlacement, FALSE)
//	ELIF FMMCpassed.iColoumSelected = FMMC_RULES_COLOUM
//		sTempPlacement = GET_SCROLL_ARROW_SPRITE_POS(Placement.SpritePlacement[7], 0.500, 0.841, 270)
//		DRAW_2D_SPRITE("MPGangSelect", "MP_ArrowLarge", sTempPlacement, FALSE)
//
//		sTempPlacement = GET_SCROLL_ARROW_SPRITE_POS(Placement.SpritePlacement[7], 0.5005, 0.858, 90)
//		DRAW_2D_SPRITE("MPGangSelect", "MP_ArrowLarge", sTempPlacement, FALSE)
//	ENDIF
//ENDPROC
//
////Darw the arrows next to the selected catogory < name >
//PROC DRAW_COLOUM_ARROWS(LARGE_PLACEMENT_TOOLS& Placement, FMMC_MAIN_MENU_STRUCT &FMMCpassed, FLOAT fLeftX, FLOAT fRightX)
//	SPRITE_PLACEMENT sTempPlacement
//	// LEFT
//	Placement.SpritePlacement[2] = GET_ARROW_PLACEMENT_DM(Placement.SpritePlacement[2], FMMCpassed.iCategorySelected)
//	sTempPlacement = GET_SELECTION_ARROW_SPRITE_POS(Placement.SpritePlacement[2], fLeftX, -0.010)
//	DRAW_2D_SPRITE("MPLobby", "MP_ArrowSmall", sTempPlacement, FALSE)
//	// RIGHT
//	sTempPlacement = GET_SELECTION_ARROW_SPRITE_POS(Placement.SpritePlacement[2], fRightX, 0.010)
//	DRAW_2D_SPRITE("MPLobby", "MP_ArrowSmall", sTempPlacement, FALSE)
//ENDPROC
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////              Draw Coloums              		 ////////////////////////////////////////
//PROC RENDER_DM_CATEGORY_NAMES(LARGE_PLACEMENT_TOOLS& Placement, FMMC_MAIN_MENU_STRUCT &FMMCpassed, STRING OptionsLabel, INT iNumberOfOptions)
//
//	TEXT_LABEL_23 iLabel
//	INT SlotCount = 1
//	INT i
//	////////////////	Options RECTS
//	FOR i = 0 TO CAT_MAX
//		Placement.RectPlacement[ciOptionBG] = GET_NAME_SLOT_RECT_PLACEMENT_DM(Placement.RectPlacement[ciOptionBG], i)
//		SET_RECT_TO_NORMAL_COLOUR(Placement.RectPlacement[ciOptionBG])
//		// Highlight category blocks white when hovered
//		IF i = FMMCpassed.iCategorySelected AND FMMCpassed.iColoumSelected = FMMC_OPTIONS_COLOUM
//			// Highlight white when selected
//			DRAW_RECTANGLE(Placement.RectPlacement[ciOptionBG], HIGHLIGHT_OPTION_SELECTED_WHITE)	
//		ELSE
//			// Normal unselected category
//			DRAW_RECTANGLE(Placement.RectPlacement[ciOptionBG])
//		ENDIF
//	ENDFOR
//
//	
//	////////////////	CATEGORY NAMES
//	FOR i = 0 TO iNumberOfOptions-1
//		IF g_FMMC_STRUCT.iMissionType != FMMC_TYPE_DEATHMATCH
//			IF SlotCount < 17	
//				iLabel = OptionsLabel
//				iLabel += SlotCount 
//				Placement.TextPlacement[FMMC_MENU_OPTIONS_NAMES_COLOUM_0] = GET_NAME_SLOT_TEXT_PLACEMENT_DM_CATEGORIES(Placement.TextPlacement[FMMC_MENU_OPTIONS_NAMES_COLOUM_0], SlotCount)
//				IF i = FMMCpassed.iCategorySelected AND FMMCpassed.iColoumSelected = FMMC_OPTIONS_COLOUM
//					// Draw text black (selected)
//					SET_TEXT_BLACK(Placement.aStyle.TS_STANDARDSMALLHUD)
//					DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[FMMC_MENU_OPTIONS_NAMES_COLOUM_0], Placement.aStyle.TS_STANDARDSMALLHUD, iLabel, FALSE, FALSE)
//					SET_TEXT_WHITE(Placement.aStyle.TS_STANDARDSMALLHUD)
//				ELSE
//					// Draw remaining categories white (not selected)
//					DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[FMMC_MENU_OPTIONS_NAMES_COLOUM_0], Placement.aStyle.TS_STANDARDSMALLHUD, iLabel, FALSE, FALSE)
//				ENDIF
//				SlotCount++			
//			ENDIF
//		ELSE
//			IF SlotCount < 18	
//				IF SlotCount = OPTION_NUM_PLAYERS + 1
//					iLabel = "FMMC_T0_M1"
//				ELIF SlotCount = OPTION_WEAPON_RESPAWN +1
//					iLabel = "FMMC_T0_M5"
//				ELIF SlotCount = OPTION_VEHICLES + 1
//					iLabel = "FMMC_T0_M6"
////				ELIF SlotCount = OPTION_VEHICLES_RESPAWN + 1
////					iLabel = "FMMC_T0_VR"
//				ELIF SlotCount = OPTION_TEAM_DEATHMATCH + 1
//					iLabel = "LOB_CAT_17"
//				ELSE
//					iLabel = OptionsLabel
//					iLabel += SlotCount 
//				ENDIF
//				Placement.TextPlacement[FMMC_MENU_OPTIONS_NAMES_COLOUM_0] = GET_NAME_SLOT_TEXT_PLACEMENT_DM_CATEGORIES(Placement.TextPlacement[FMMC_MENU_OPTIONS_NAMES_COLOUM_0], SlotCount)
//				IF i = FMMCpassed.iCategorySelected AND FMMCpassed.iColoumSelected = FMMC_OPTIONS_COLOUM
//					// Draw text black (selected)
//					SET_TEXT_BLACK(Placement.aStyle.TS_STANDARDSMALLHUD)
//					DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[FMMC_MENU_OPTIONS_NAMES_COLOUM_0], Placement.aStyle.TS_STANDARDSMALLHUD, iLabel, FALSE, FALSE)
//					SET_TEXT_WHITE(Placement.aStyle.TS_STANDARDSMALLHUD)
//				ELSE
//					// Draw remaining categories white (not selected)
//					DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[FMMC_MENU_OPTIONS_NAMES_COLOUM_0], Placement.aStyle.TS_STANDARDSMALLHUD, iLabel, FALSE, FALSE)
//				ENDIF
//				SlotCount++			
//			ENDIF
//		ENDIF
//	ENDFOR
//	
//	//DRAW_COLOUM_ARROWS(Placement, FMMCpassed, 0.314, 0.392)
//
//	
//ENDPROC
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////              COLUMN 3                   		 ////////////////////////////////////////
/////    
//// Draw minimap top right
//PROC RENDER_DM_MINIMAP(LARGE_PLACEMENT_TOOLS& Placement)
//	DRAW_RECTANGLE(Placement.RectPlacement[7])
//	DRAW_2D_SPRITE("MPLobby", "MP_Minimap1", Placement.SpritePlacement[0], FALSE)
//ENDPROC
//
//PROC RENDER_DM_HOW_TO_PLAY_BOX(LARGE_PLACEMENT_TOOLS& Placement)
//
//	// Title
//	SET_TEXT_STYLE(Placement.aStyle.TS_STANDARDSMALLHUD)	
//	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("LOB_TITLE_HOW")
//	END_TEXT_COMMAND_DISPLAY_TEXT(0.600, 0.496)
//	
//	DRAW_RECTANGLE(Placement.RectPlacement[3])
//	
//	// Description
//	SET_TEXT_STYLE(Placement.aStyle.TS_STANDARDSMALLHUD)	
////	IF IS_CASH_DEATHMATCH()
////		BEGIN_TEXT_COMMAND_DISPLAY_TEXT("LOB_HOW_TO2")
////	ELSE
////		BEGIN_TEXT_COMMAND_DISPLAY_TEXT("LOB_HOW_TO")
////	ENDIF
////	END_TEXT_COMMAND_DISPLAY_TEXT(0.600, 0.533)
//
//	DRAW_RECTANGLE(Placement.RectPlacement[4])	
//ENDPROC
//
//PROC RENDER_DM_SCOREBOX(LARGE_PLACEMENT_TOOLS& Placement)
//	// Title
//	SET_TEXT_STYLE(Placement.aStyle.TS_STANDARDSMALLHUD)	
//	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("LOB_TITLE_SCOR")
//	END_TEXT_COMMAND_DISPLAY_TEXT(0.600, 0.683)	
//	DRAW_RECTANGLE(Placement.RectPlacement[1])	
//	// Description
//	SET_TEXT_STYLE(Placement.aStyle.TS_STANDARDSMALLHUD)	
////	IF IS_CASH_DEATHMATCH()
////		BEGIN_TEXT_COMMAND_DISPLAY_TEXT("LOB_SCOR_NUM")
////		ADD_TEXT_COMPONENT_INTEGER(CASH_AWARD_KILL_NORMAL)
////		ADD_TEXT_COMPONENT_INTEGER(CASH_AWARD_BONUS_HEADSHOT)
////		ADD_TEXT_COMPONENT_INTEGER(CASH_PENALTY_SUICIDE)
////		END_TEXT_COMMAND_DISPLAY_TEXT(0.600, 0.721)
////		
////		// "Cash pickup"
////		SET_TEXT_STYLE(Placement.aStyle.TS_STANDARDSMALLHUD)
////		BEGIN_TEXT_COMMAND_DISPLAY_TEXT("LOB_SCOR_PKP")
////		ADD_TEXT_COMPONENT_INTEGER(CASH_AWARD_PICKUP)
////		END_TEXT_COMMAND_DISPLAY_TEXT(0.600, 0.775)
////	
////	ELSE
////		BEGIN_TEXT_COMMAND_DISPLAY_TEXT("LOB_SCORING2")
////		END_TEXT_COMMAND_DISPLAY_TEXT(0.600, 0.721)
////	ENDIF
//	
//	DRAW_RECTANGLE(Placement.RectPlacement[2])
//ENDPROC
//
//PROC CLEAN_UP_FMMC_MENU_VARIABLES(FMMC_MAIN_MENU_STRUCT &FMMCpassed)
//	FMMCpassed.iLobbyProgress		 	= 0
//	FMMCpassed.iCategorySelected 		= 0
//	FMMCpassed.iColoumSelected	 		= 0
//	FMMCpassed.iLobbyBitset		 		= 0
//	INT i1, i2
//	FOR i1 = 0 TO (FMMC_TAB_MAXIMUM_NUMBER - 1)
//		FOR i2 = 0 TO (FMMC_MAX_NUM_CATEGORIES - 1)
//			FMMCpassed.iMenuOption[i1][i2] = 0
//			FMMCpassed.iMenuRules[i1][i2] = 0
//		ENDFOR
//	ENDFOR
//ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////              PREPARATION                   		 ////////////////////////////////////
///    
			   
//
//FUNC BOOL IS_FMMC_MENU_SET_UP(LARGE_PLACEMENT_TOOLS& Placement, FMMC_MAIN_MENU_STRUCT &FMMCpassed)
//	IF Placement.bHudScreenInitialised = FALSE
//		CLEAN_UP_FMMC_MENU_VARIABLES(FMMCpassed)
//		INITIALISE_DM_LOBBY_VARIABLES(Placement)
//		SET_DEFAULT_MENU_VALUES(FMMCpassed)
//		Placement.bHudScreenInitialised = TRUE
//	ENDIF	
//	RETURN TRUE
//ENDFUNC 
//#IF IS_DEBUG_BUILD
//PROC SETUP_WIDGETS_FMMC_MENU(LARGE_PLACEMENT_TOOLS& Placement, FMMC_MAIN_MENU_STRUCT &FMMCpassed, WIDGET_GROUP_ID& ParentWidget)
//	IF NOT IS_BIT_SET(FMMCpassed.iLobbyBitset, FMMC_MENU_BITSET_SET_UP_WIDGETS)
//		CREATE_ONLY_LARGE_PLACEMENT_WIDGETS(Placement, ParentWidget)
//		SET_BIT(FMMCpassed.iLobbyBitset, FMMC_MENU_BITSET_SET_UP_WIDGETS)
//	ENDIF
//ENDPROC
//#ENDIF
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////           	  LOAD PROCS	        		 ////////////////////////////////////////
////Scaleform
//FUNC BOOL HAS_FMMC_MENU_SCALEFORM_LOADED(LARGE_PLACEMENT_TOOLS& Placement, FMMC_MAIN_MENU_STRUCT &FMMCpassed)
//	Placement.ButtonMovie = REQUEST_SCALEFORM_MOVIE("instructional_buttons")
//	FMMCpassed.AnotherButtonMovie = REQUEST_SCALEFORM_MOVIE("SAVING_FOOTER")
//	IF HAS_SCALEFORM_MOVIE_LOADED(FMMCpassed.AnotherButtonMovie)
//		IF HAS_SCALEFORM_MOVIE_LOADED(Placement.ButtonMovie)
//			RETURN TRUE
//		ENDIF
//	ENDIF
//	// Do once
//	IF NOT IS_BIT_SET(FMMCpassed.iLobbyBitset, FMMC_MENU_BITSET_CLEAR_SCALEFORM)
//		IF HAS_SCALEFORM_MOVIE_LOADED(Placement.ButtonMovie)
//			IF HAS_SCALEFORM_MOVIE_LOADED(FMMCpassed.AnotherButtonMovie)
//				CALL_SCALEFORM_MOVIE_METHOD(Placement.ButtonMovie, "CLEAR_ALL")
//				SET_BIT(FMMCpassed.iLobbyBitset, FMMC_MENU_BITSET_CLEAR_SCALEFORM)
//			ENDIF
//		ENDIF
//	ENDIF
//	RETURN FALSE
//ENDFUNC
////Textures
//FUNC BOOL HAVE_FMMC_MENU_TEXTURES_LOADED()
//	REQUEST_STREAMED_TEXTURE_DICT("MPGangSelect")
//	REQUEST_STREAMED_TEXTURE_DICT("MPHud")
//	REQUEST_STREAMED_TEXTURE_DICT("MPLobby")
//	REQUEST_STREAMED_TEXTURE_DICT("MPOverview")
//	IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPGangSelect")
//		IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPHud")
//			IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPOverview")
//				IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPLobby")
//					RETURN TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	RETURN FALSE
//ENDFUNC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////              Load and save procs        		 ////////////////////////////////////////
///    
PROC SET_DEFAULT_MENU_VALUES(FMMC_MAIN_MENU_STRUCT &FMMCpassed)
	UNUSED_PARAMETER( FMMCpassed )
	/*
	PRINTLN("SET_DEFAULT_DEATHMATCH_VALUES")
	FMMCpassed.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_NUM_PLAYERS]						= MAX_OPTION_NUM_PLAYERS
	FMMCpassed.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_TEAM_DEATHMATCH]					= 1	
	FMMCpassed.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_DURATION]							= 1
	FMMCpassed.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_TARGET]								= DM_TARGET_KILLS_OFF
	FMMCpassed.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_WEAPONS]							= 0
	FMMCpassed.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_WEAPON_RESPAWN]						= 2
	FMMCpassed.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_AUTO_AIM]							= 0
	FMMCpassed.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_POLICE]								= 1
	FMMCpassed.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_PEDS]								= 0
	FMMCpassed.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_BLIPS]								= 0
	FMMCpassed.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_TAGS]								= 2
	FMMCpassed.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_RADIO]								= 0
	FMMCpassed.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_VOICE]								= 0
	FMMCpassed.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_SPAWN_TIME]							= 0
	FMMCpassed.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_VEHICLES]							= 0
	//FMMCpassed.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_VEHICLES_RESPAWN]					= 5
	
	FMMCpassed.iMenuOption[FMMC_TYPE_RACE][OPTION_RACE_NO_PLAYERS]						= MAX_OPTION_NUM_PLAYERS
	FMMCpassed.iMenuOption[FMMC_TYPE_RACE][OPTION_RACE_TRAFFIC]								= 0
	FMMCpassed.iMenuOption[FMMC_TYPE_RACE][OPTION_RACE_DEFAULT_VEHICLE] 					= -1
	FMMCpassed.iMenuOption[FMMC_TYPE_RACE][OPTION_RACE_VEHICLE_CLASS] 						= -1
	FMMCpassed.iMenuOption[FMMC_TYPE_RACE][OPTION_RACE_NO_LAPS] 							= 2
	//FMMCpassed.iMenuOption[FMMC_TYPE_RACE][OPTION_RACE_c] 									= 2
	
	FMMCpassed.iMenuOption[FMMC_TYPE_MISSION][OPTION_MISSION_NO_PLAYERS]					= OPTION_MISSION_MAX_NO_PLAYERS - 1
	FMMCpassed.iMenuOption[FMMC_TYPE_MISSION][OPTION_MISSION_NO_TEAMS]						= 1
	FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_NUMBER_OF_LIVES]		= NUMBER_OF_LIVES_UNLIMITED
	*/
ENDPROC		
PROC CONVERT_FMMC_MENU_STRUCT_TO_GLOBALS(FMMC_MAIN_MENU_STRUCT &FMMCpassed)
		
	//Convert the mission varaiables
	IF g_FMMC_STRUCT.iMissionType =  FMMC_TYPE_MISSION
		//Mission options
		g_FMMC_STRUCT.iNumParticipants 		= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_NO_PLAYERS]
		g_FMMC_STRUCT.iMinNumParticipants	= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_NO_PLAYERS]
		//g_FMMC_STRUCT.iRespawnTime  		= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_PLAYER_RESPAWN]
		g_FMMC_STRUCT.iMaxNumberOfTeams 	= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_NO_TEAMS] + 1
//		g_FMMC_STRUCT.iNumberOfLives 		= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_NUMBER_OF_LIVES]
		//g_FMMC_STRUCT.iRoundTime  			= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_ROUND_TIME]
		//g_FMMC_STRUCT.iNumberOfRounds  		= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_NO_ROUNDS]		
//		g_FMMC_STRUCT_ENTITIES.iWeaponPallet  		= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_WEAPONS]
		//g_FMMC_STRUCT_ENTITIES.iWeaponRespawnTime  	= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_WEAPON_RESPAWN]
//		g_FMMC_STRUCT_ENTITIES.iVehiclePallet  		= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_VEHICLES]
		//g_FMMC_STRUCT_ENTITIES.iVehicleRespawnTime  	= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_VEHICLE_RESPAWN]
//		g_FMMC_STRUCT_ENTITIES.iPedPallet  			= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_ENEMIES]
		//g_FMMC_STRUCT_ENTITIES.iPedRespawnTime  		= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_ENEMY_RESPAWN]
		//g_FMMC_STRUCT_ENTITIES.iObjectPallet  		= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_OBJECTS]
		//g_FMMC_STRUCT.iAreaTriggered  		= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_SPAWN_AREA]
		//g_FMMC_STRUCT_ENTITIES.iObjectRespawnTime  	= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_OBJECT_RESPAWN]
		//Mission rules
		//g_FMMC_STRUCT.iNumberOfKills  		= FMMCpassed.iMenuRules[FMMC_TAB_CREATION_MISSION][RULE_NUMBER_OF_KILLS]	
		//g_FMMC_STRUCT.iNumberOfDeaths  		= FMMCpassed.iMenuRules[FMMC_TAB_CREATION_MISSION][RULE_NUMBER_OF_DEATHS]	
		//g_FMMC_STRUCT.iObjectRule  			= FMMCpassed.iMenuRules[FMMC_TAB_CREATION_MISSION][RULE_COLLECT_OBJECT]	
		//g_FMMC_STRUCT.iVehicleRule  		= FMMCpassed.iMenuRules[FMMC_TAB_CREATION_MISSION][RULE_COLLECT_VEHICLE]	
		//g_FMMC_STRUCT.iPedRule  			= FMMCpassed.iMenuRules[FMMC_TAB_CREATION_MISSION][RULE_COLLECT_ENEMY]	
		//g_FMMC_STRUCT.iFirstToLocationRule  = FMMCpassed.iMenuRules[FMMC_TAB_CREATION_MISSION][RULE_FIRST_TO_LOCATION]
		//g_FMMC_STRUCT.iLastManStandingRule  = FMMCpassed.iMenuRules[FMMC_TAB_CREATION_MISSION][RULE_LAST_MAN_STANDING]
		//g_FMMC_STRUCT.iTargetPedsRule  		= FMMCpassed.iMenuRules[FMMC_TAB_CREATION_MISSION][RULE_TARGET_PEDS]	
		
	//Convert the Deathmatch varaiables
	ELIF g_FMMC_STRUCT.iMissionType =  FMMC_TYPE_DEATHMATCH
		g_FMMC_STRUCT.iNumParticipants  	= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_NUM_PLAYERS]	
//		g_FMMC_STRUCT.iTeamDeathmatch 	 	= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_TEAM_DEATHMATCH]			
		g_FMMC_STRUCT.iRoundTime			= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_DURATION]	
		g_FMMC_STRUCT.iTargetScore 			= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_TARGET]		
		g_FMMC_STRUCT.iRespawnTime		  	= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_SPAWN_TIME]	
		g_FMMC_STRUCT_ENTITIES.iWeaponPallet  		= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_WEAPONS]		
		g_FMMC_STRUCT_ENTITIES.iWeaponRespawnTime 	= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_WEAPON_RESPAWN]
		g_FMMC_STRUCT.iAutoAim			  	= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_AUTO_AIM]	
		g_FMMC_STRUCT.iPolice  				= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_POLICE]		
		g_FMMC_STRUCT.iDMPedRule			= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_PEDS]		
		g_FMMC_STRUCT.iBlips	  			= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_BLIPS]		
		g_FMMC_STRUCT.iTags  				= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_TAGS]		
		g_FMMC_STRUCT.iRadio  				= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_RADIO]		
		g_FMMC_STRUCT.iVoice  				= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_VOICE]		
		g_FMMC_STRUCT_ENTITIES.iVehiclePallet  		= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_VEHICLES]	
		//g_FMMC_STRUCT_ENTITIES.iVehicleRespawnTime  	= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_VEHICLES_RESPAWN]
		
		
		//Deathmatch rules
//		g_FMMC_STRUCT.iNumberOfKills  		= FMMCpassed.iMenuRules[FMMC_TAB_CREATION_DEATHMATCH][RULE_NUMBER_OF_KILLS]	
//		g_FMMC_STRUCT.iNumberOfDeaths  		= FMMCpassed.iMenuRules[FMMC_TAB_CREATION_DEATHMATCH][RULE_NUMBER_OF_DEATHS]	
//		g_FMMC_STRUCT.iObjectRule  			= FMMCpassed.iMenuRules[FMMC_TAB_CREATION_DEATHMATCH][RULE_COLLECT_OBJECT]	
//		g_FMMC_STRUCT.iVehicleRule  		= FMMCpassed.iMenuRules[FMMC_TAB_CREATION_DEATHMATCH][RULE_COLLECT_VEHICLE]	
//		g_FMMC_STRUCT.iPedRule  			= FMMCpassed.iMenuRules[FMMC_TAB_CREATION_DEATHMATCH][RULE_COLLECT_ENEMY]	
//		g_FMMC_STRUCT.iFirstToLocationRule  = FMMCpassed.iMenuRules[FMMC_TAB_CREATION_DEATHMATCH][RULE_FIRST_TO_LOCATION]
//		g_FMMC_STRUCT.iLastManStandingRule  = FMMCpassed.iMenuRules[FMMC_TAB_CREATION_DEATHMATCH][RULE_LAST_MAN_STANDING]
//		g_FMMC_STRUCT.iTargetPedsRule  		= FMMCpassed.iMenuRules[FMMC_TAB_CREATION_DEATHMATCH][RULE_TARGET_PEDS]	
	
		PRINTSTRING("g_FMMC_STRUCT.iTeamDaethmatch 	= ")PRINTINT(g_FMMC_STRUCT.iTeamDeathmatch)PRINTNL()
		PRINTSTRING("g_FMMC_STRUCT.iNumParticipants 	= ")PRINTINT(g_FMMC_STRUCT.iNumParticipants)PRINTNL()
		PRINTSTRING("g_FMMC_STRUCT.iRespawnTime 		= ")PRINTINT(g_FMMC_STRUCT.iRespawnTime)PRINTNL()
		PRINTSTRING("g_FMMC_STRUCT.iRoundTime 			= ")PRINTINT(g_FMMC_STRUCT.iRoundTime)PRINTNL()
		//PRINTSTRING("g_FMMC_STRUCT.iNumberOfRounds 		= ")PRINTINT(g_FMMC_STRUCT.iNumberOfRounds)PRINTNL()
		PRINTSTRING("g_FMMC_STRUCT_ENTITIES.iWeaponPallet 		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.iWeaponPallet)PRINTNL()
		PRINTSTRING("g_FMMC_STRUCT_ENTITIES.iWeaponRespawnTime 	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.iWeaponRespawnTime)PRINTNL()
		PRINTSTRING("g_FMMC_STRUCT.iPolice 			= ")PRINTINT(g_FMMC_STRUCT.iPolice)PRINTNL()
		PRINTSTRING("g_FMMC_STRUCT_ENTITIES.iPedPallet 			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.iPedPallet)PRINTNL()
		PRINTSTRING("g_FMMC_STRUCT_ENTITIES.iPedRespawnTime 		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.iPedRespawnTime)PRINTNL()
		//PRINTSTRING("g_FMMC_STRUCT.iNumberOfKills 		= ")PRINTINT(g_FMMC_STRUCT.iNumberOfKills)PRINTNL()
		//PRINTSTRING("g_FMMC_STRUCT.iNumberOfDeaths 		= ")PRINTINT(g_FMMC_STRUCT.iNumberOfDeaths)PRINTNL()
		//PRINTSTRING("g_FMMC_STRUCT.iObjectRule 		= ")PRINTINT(g_FMMC_STRUCT.iObjectRule)PRINTNL()
		//PRINTSTRING("g_FMMC_STRUCT.iVehicleRule 		= ")PRINTINT(g_FMMC_STRUCT.iVehicleRule)PRINTNL()
		//PRINTSTRING("g_FMMC_STRUCT.iPedRule 			= ")PRINTINT(g_FMMC_STRUCT.iPedRule)PRINTNL()
		//PRINTSTRING("g_FMMC_STRUCT.iFirstToLocationRule	 = ")PRINTINT(g_FMMC_STRUCT.iFirstToLocationRule)PRINTNL()
		//PRINTSTRING("g_FMMC_STRUCT.iLastManStandingRule	 = ")PRINTINT(g_FMMC_STRUCT.iLastManStandingRule)PRINTNL()
		//PRINTSTRING("g_FMMC_STRUCT.iTargetPedsRule 		= ")PRINTINT(g_FMMC_STRUCT.iTargetPedsRule)PRINTNL()

	//Convert the Race varaiables
	ELIF g_FMMC_STRUCT.iMissionType			= FMMC_TYPE_RACE
		g_FMMC_STRUCT.iNumParticipants  	= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_NO_PLAYERS]	
//		g_FMMC_STRUCT.iRaceType		  		= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_TYPE]
		g_FMMC_STRUCT.iNumLaps  			= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_NO_LAPS]
	//	g_FMMC_STRUCT.iPedDensity			= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_AI_DENISTY]
	//	g_FMMC_STRUCT.iRaceClass  			= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_VEHICLE_CLASS]	
	//	g_FMMC_STRUCT.iVehicleModel			= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_DEFAULT_VEHICLE]	
	//	g_FMMC_STRUCT_ENTITIES.iWeaponRespawnTime  	= FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_WEAPON_RESPAWN]
		PRINTLN("g_FMMC_STRUCT.iVehicleModel	 = ", g_FMMC_STRUCT.iVehicleModel)
		//Race rules
		//g_FMMC_STRUCT.iNumberOfKills  		= FMMCpassed.iMenuRules[FMMC_TAB_CREATION_RACE][RULE_NUMBER_OF_KILLS]	
		//g_FMMC_STRUCT.iNumberOfDeaths  		= FMMCpassed.iMenuRules[FMMC_TAB_CREATION_RACE][RULE_NUMBER_OF_DEATHS]	
		////g_FMMC_STRUCT.iObjectRule  			= FMMCpassed.iMenuRules[FMMC_TAB_CREATION_RACE][RULE_COLLECT_OBJECT]	
		//g_FMMC_STRUCT.iVehicleRule  		= FMMCpassed.iMenuRules[FMMC_TAB_CREATION_RACE][RULE_COLLECT_VEHICLE]	
		//g_FMMC_STRUCT.iDMPedRule  			= FMMCpassed.iMenuRules[FMMC_TAB_CREATION_RACE][RULE_COLLECT_ENEMY]	
		//g_FMMC_STRUCT.iFirstToLocationRule  = FMMCpassed.iMenuRules[FMMC_TAB_CREATION_RACE][RULE_FIRST_TO_LOCATION]
		///g_FMMC_STRUCT.iLastManStandingRule  = FMMCpassed.iMenuRules[FMMC_TAB_CREATION_RACE][RULE_LAST_MAN_STANDING]
		//g_FMMC_STRUCT.iTargetPedsRule  		= FMMCpassed.iMenuRules[FMMC_TAB_CREATION_RACE][RULE_TARGET_PEDS]	
	ENDIF
	
ENDPROC

//This loads all the varaiables in to the FMMC menu (like loading a fucking save.)
PROC CONVERT_FMMC_GLOBALS_TO_MENU_STRUCT(FMMC_MAIN_MENU_STRUCT &FMMCpassed)		
	
	//Convert the mission varaiables
	IF g_FMMC_STRUCT.iMissionType =  FMMC_TYPE_MISSION
		//Mission options
		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_NO_PLAYERS]			= g_FMMC_STRUCT.iNumParticipants 			
		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_NO_TEAMS]				= g_FMMC_STRUCT.iMaxNumberOfTeams - 1
		
		//FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_PLAYER_RESPAWN]		= g_FMMC_STRUCT.iRespawnTime  		
		//FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_NUMBER_OF_LIVES]		= g_FMMC_STRUCT.iNumberOfLives
		//FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_ROUND_TIME]			= g_FMMC_STRUCT.iRoundTime  			
		//FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_NO_ROUNDS]				= g_FMMC_STRUCT.iNumberOfRounds  		
//		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_WEAPONS]				= g_FMMC_STRUCT_ENTITIES.iWeaponPallet  		
		//FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_WEAPON_RESPAWN]		= g_FMMC_STRUCT_ENTITIES.iWeaponRespawnTime  	
//		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_VEHICLES]				= g_FMMC_STRUCT_ENTITIES.iVehiclePallet  		
		//FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_VEHICLE_RESPAWN]		= g_FMMC_STRUCT_ENTITIES.iVehicleRespawnTime  	
//		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_ENEMIES]				= g_FMMC_STRUCT_ENTITIES.iPedPallet  			
		//FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_ENEMY_RESPAWN]			= g_FMMC_STRUCT_ENTITIES.iPedRespawnTime  		
		//FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_OBJECTS]				= g_FMMC_STRUCT_ENTITIES.iObjectPallet  		
		//FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_SPAWN_AREA]			= g_FMMC_STRUCT.iAreaTriggered  		
		//FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_OBJECT_RESPAWN]		= g_FMMC_STRUCT_ENTITIES.iObjectRespawnTime  	
		//Mission rules
	//	FMMCpassed.iMenuRules[FMMC_TAB_CREATION_MISSION][RULE_NUMBER_OF_KILLS]					= g_FMMC_STRUCT.iNumberOfKills  		
	//	FMMCpassed.iMenuRules[FMMC_TAB_CREATION_MISSION][RULE_NUMBER_OF_DEATHS]					= g_FMMC_STRUCT.iNumberOfDeaths  		
	//	FMMCpassed.iMenuRules[FMMC_TAB_CREATION_MISSION][RULE_COLLECT_OBJECT]					= g_FMMC_STRUCT.iObjectRule  			
		//FMMCpassed.iMenuRules[FMMC_TAB_CREATION_MISSION][RULE_COLLECT_VEHICLE]					= g_FMMC_STRUCT.iVehicleRule  		
		FMMCpassed.iMenuRules[FMMC_TAB_CREATION_MISSION][RULE_COLLECT_ENEMY]					= g_FMMC_STRUCT.iDMPedRule  			
		//FMMCpassed.iMenuRules[FMMC_TAB_CREATION_MISSION][RULE_FIRST_TO_LOCATION]				= g_FMMC_STRUCT.iFirstToLocationRule  
	//	FMMCpassed.iMenuRules[FMMC_TAB_CREATION_MISSION][RULE_LAST_MAN_STANDING]				= g_FMMC_STRUCT.iLastManStandingRule  	
		
	//Convert the Deathmatch varaiables
	ELIF g_FMMC_STRUCT.iMissionType =  FMMC_TYPE_DEATHMATCH
		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_NUM_PLAYERS]				= g_FMMC_STRUCT.iNumParticipants 
		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_TEAM_DEATHMATCH]			= g_FMMC_STRUCT.iTeamDeathmatch 		
		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_DURATION]					= g_FMMC_STRUCT.iRespawnTime  		
		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_TARGET]						= g_FMMC_STRUCT.iTargetScore 				
		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_WEAPONS]					= g_FMMC_STRUCT_ENTITIES.iWeaponPallet  			
		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_WEAPON_RESPAWN]				= g_FMMC_STRUCT_ENTITIES.iWeaponRespawnTime 	
		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_AUTO_AIM]					= g_FMMC_STRUCT.iAutoAim			  		
		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_POLICE]						= g_FMMC_STRUCT.iPolice  					
		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_PEDS]						= g_FMMC_STRUCT.iDMPedRule			  		
		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_BLIPS]						= g_FMMC_STRUCT.iBlips	  				
		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_TAGS]						= g_FMMC_STRUCT.iTags  							
		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_RADIO]						= g_FMMC_STRUCT.iRadio  							
		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_VOICE]						= g_FMMC_STRUCT.iVoice  							
		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_SPAWN_TIME]					= g_FMMC_STRUCT.iRespawnTime		  				
		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_VEHICLES]					= g_FMMC_STRUCT_ENTITIES.iVehiclePallet  					
		//FMMCpassed.iMenuOption[FMMC_TAB_CREATION_DEATHMATCH][OPTION_VEHICLES_RESPAWN]			= g_FMMC_STRUCT_ENTITIES.iVehicleRespawnTime  	
		//Deathmatch rules
//		FMMCpassed.iMenuRules[FMMC_TAB_CREATION_DEATHMATCH][RULE_NUMBER_OF_KILLS]				= g_FMMC_STRUCT.iNumberOfKills  		
//		FMMCpassed.iMenuRules[FMMC_TAB_CREATION_DEATHMATCH][RULE_NUMBER_OF_DEATHS]				= g_FMMC_STRUCT.iNumberOfDeaths  		
//		FMMCpassed.iMenuRules[FMMC_TAB_CREATION_DEATHMATCH][RULE_COLLECT_OBJECT]				= g_FMMC_STRUCT.iObjectRule  			
//		FMMCpassed.iMenuRules[FMMC_TAB_CREATION_DEATHMATCH][RULE_COLLECT_VEHICLE]				= g_FMMC_STRUCT.iVehicleRule  		
//		FMMCpassed.iMenuRules[FMMC_TAB_CREATION_DEATHMATCH][RULE_COLLECT_ENEMY]					= g_FMMC_STRUCT.iPedRule  			
//		FMMCpassed.iMenuRules[FMMC_TAB_CREATION_DEATHMATCH][RULE_FIRST_TO_LOCATION]				= g_FMMC_STRUCT.iFirstToLocationRule  
//		FMMCpassed.iMenuRules[FMMC_TAB_CREATION_DEATHMATCH][RULE_LAST_MAN_STANDING]				= g_FMMC_STRUCT.iLastManStandingRule  
//		FMMCpassed.iMenuRules[FMMC_TAB_CREATION_DEATHMATCH][RULE_TARGET_PEDS]					= g_FMMC_STRUCT.iTargetPedsRule  		
	
	//Convert the Race varaiables
	ELIF g_FMMC_STRUCT.iMissionType =  FMMC_TYPE_RACE	
		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_NO_PLAYERS]					= g_FMMC_STRUCT.iNumParticipants  	
		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_NO_LAPS]						= g_FMMC_STRUCT.iNumLaps  			  	
//		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_WEAPON_RESPAWN]				= g_FMMC_STRUCT_ENTITIES.iWeaponRespawnTime 
		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_VEHICLE_CLASS]				= g_FMMC_STRUCT.iRaceClass 
		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_DEFAULT_VEHICLE]				= g_FMMC_STRUCT.iVehicleModel 	 	
		//Race rules
		//FMMCpassed.iMenuRules[FMMC_TAB_CREATION_RACE][RULE_NUMBER_OF_KILLS]						= g_FMMC_STRUCT.iNumberOfKills  		
		//FMMCpassed.iMenuRules[FMMC_TAB_CREATION_RACE][RULE_NUMBER_OF_DEATHS]					= g_FMMC_STRUCT.iNumberOfDeaths  		
		//FMMCpassed.iMenuRules[FMMC_TAB_CREATION_RACE][RULE_COLLECT_OBJECT]						= g_FMMC_STRUCT.iObjectRule  			
		//FMMCpassed.iMenuRules[FMMC_TAB_CREATION_RACE][RULE_COLLECT_VEHICLE]						= g_FMMC_STRUCT.iVehicleRule  		
		//FMMCpassed.iMenuRules[FMMC_TAB_CREATION_RACE][RULE_COLLECT_ENEMY]						= g_FMMC_STRUCT.iPedRule  			
		//FMMCpassed.iMenuRules[FMMC_TAB_CREATION_RACE][RULE_FIRST_TO_LOCATION]					= g_FMMC_STRUCT.iFirstToLocationRule  
		//FMMCpassed.iMenuRules[FMMC_TAB_CREATION_RACE][RULE_LAST_MAN_STANDING]					= g_FMMC_STRUCT.iLastManStandingRule  
		//FMMCpassed.iMenuRules[FMMC_TAB_CREATION_RACE][RULE_TARGET_PEDS]							= g_FMMC_STRUCT.iTargetPedsRule  		
	ENDIF
ENDPROC
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////         	    Cap value Procs        			 ////////////////////////////////////////
////wraps around the maximum values of items
//PROC CAP_ITEM(INT iMaxValue, INT &iToCap, BOOL bRoundUp = FALSE, INT iLowest = 0)
//	PRINTSTRING("CAP_COLOUM_ITEMS tab ")PRINTNL()
//	IF bRoundUp
//		IF iToCap >= iMaxValue
//			iToCap = iMaxValue -1
//			IF iToCap < iLowest
//				iToCap = iLowest
//			ENDIF
//		ELIF iToCap < iLowest
//			iToCap = iLowest
//		ENDIF
//	ELSE
//		IF iToCap >= iMaxValue
//			iToCap = iLowest
//		ELIF iToCap < iLowest
//			iToCap = iMaxValue-1
//			IF iToCap < iLowest
//				iToCap = iLowest
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC
//
//PROC CAP_NUMBER_OF_PLAYERS_RACE(FMMC_MAIN_MENU_STRUCT &FMMCpassed, BOOL bRoundUp = FALSE)
//	IF FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_TYPE] = FMMC_RACE_TYPE_STANDARD
//	OR FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_TYPE] = FMMC_RACE_TYPE_P2P		
//	OR FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_TYPE] = FMMC_RACE_TYPE_BOAT	
//	OR FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_TYPE] = FMMC_RACE_TYPE_BOAT_P2P
//		CAP_ITEM(OPTION_RACE_MAX_NO_PLAYERS, 			FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_NO_PLAYERS], bRoundUp)			
//	ELSE
//		CAP_ITEM(OPTION_RACE_MAX_NO_PLAYERS_AIR, 		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_NO_PLAYERS], bRoundUp)			
//	ENDIF
//ENDPROC
//
//
////Caps all the coloum items for both options and rules
//PROC CAP_COLOUM_ITEMS(FMMC_MAIN_MENU_STRUCT &FMMCpassed, INT iTab, INT iCategorySelected, INT &iToCap)
//	//PRINTSTRING("CAP_COLOUM_ITEMS tab ")PRINTINT(iTab)PRINTSTRING(" -    ")PRINTINT(iCategorySelected)PRINTSTRING(" -To this value-   ")PRINTINT(iToCap)PRINTNL()
//	//Cap mission items
//	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
//		//cap options
//		IF iTab = FMMC_TAB_GAME_SET_UP
//			SWITCH iCategorySelected
//				CASE OPTION_MISSION_NO_PLAYERS
//					IF FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_SPAWN_AREA] = 0
//						CAP_ITEM(OPTION_MISSION_MAX_NO_PLAYERS, 		iToCap)
//					ELSE
//						CAP_ITEM(0, 		iToCap)
//					ENDIF
//				BREAK
//				CASE OPTION_MISSION_NO_TEAMS 			
//					IF FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_SPAWN_AREA] = 0
//						CAP_ITEM(OPTION_MISSION_MAX_NO_TEAMS, 			iToCap)
//					ELSE
//						CAP_ITEM(0, 			iToCap)
//					ENDIF
//				BREAK
//				CASE OPTION_MISSION_PLAYER_RESPAWN 		CAP_ITEM(OPTION_MISSION_MAX_PLAYER_RESPAWN, 	iToCap)			BREAK
//				CASE OPTION_MISSION_NUMBER_OF_LIVES 	CAP_ITEM(OPTION_MISSION_MAX_NO_LIVES,			iToCap)			BREAK
//			//	CASE OPTION_MISSION_ROUND_TIME			CAP_ITEM(OPTION_MISSION_MAX_ROUND_TIME, 		iToCap)			BREAK
//			//	CASE OPTION_MISSION_NO_ROUNDS 			CAP_ITEM(OPTION_MISSION_MAX_NO_ROUNDS, 			iToCap)			BREAK
//				CASE OPTION_MISSION_WEAPONS				CAP_ITEM(OPTION_MISSION_MAX_WEAPONS, 			iToCap)			BREAK
//				CASE OPTION_MISSION_WEAPON_RESPAWN 		CAP_ITEM(OPTION_MISSION_MAX_WEAPON_RESPAWN, 	iToCap)			BREAK
//				CASE OPTION_MISSION_VEHICLES 			CAP_ITEM(OPTION_MISSION_MAX_VEHICLES, 			iToCap)			BREAK
//			//	CASE OPTION_MISSION_VEHICLE_RESPAWN		CAP_ITEM(OPTION_MISSION_MAX_VEHICLE_RESPAWN, 	iToCap)			BREAK
//				CASE OPTION_MISSION_ENEMIES 			CAP_ITEM(OPTION_MISSION_MAX_ENEMIES, 			iToCap)			BREAK
//				CASE OPTION_MISSION_ENEMY_RESPAWN 		CAP_ITEM(OPTION_MISSION_MAX_ENEMY_RESPAWN, 		iToCap)			BREAK
//				CASE OPTION_MISSION_OBJECTS 			CAP_ITEM(OPTION_MISSION_MAX_OBJECTS, 			iToCap)			BREAK
//				CASE OPTION_MISSION_SPAWN_AREA 			
//					CAP_ITEM(OPTION_MISSION_MAX_SPAWN_AREA, 		iToCap)		
//					CAP_ITEM(0, 		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_NO_PLAYERS])
//					CAP_ITEM(0, 		FMMCpassed.iMenuOption[FMMC_TAB_CREATION_MISSION][OPTION_MISSION_NO_TEAMS])
//				BREAK
//			ENDSWITCH
//		//cap rules
//		ELIF iTab = FMMC_TAB_RULES
//			SWITCH iCategorySelected
//				CASE RULE_NUMBER_OF_KILLS				CAP_ITEM(RULE_MAX_NUMBER_OF_KILLS,				iToCap)			BREAK
//				CASE RULE_NUMBER_OF_DEATHS	 			CAP_ITEM(RULE_MAX_NUMBER_OF_DEATHS,				iToCap)			BREAK
//				CASE RULE_COLLECT_OBJECT				CAP_ITEM(RULE_MAX_COLLECT_OBJECT,				iToCap)			BREAK
//				CASE RULE_COLLECT_VEHICLE				CAP_ITEM(RULE_MAX_COLLECT_VEHICLE,				iToCap)			BREAK
//				CASE RULE_COLLECT_ENEMY					CAP_ITEM(RULE_MAX_COLLECT_ENEMY,				iToCap)			BREAK
//				CASE RULE_FIRST_TO_LOCATION 			CAP_ITEM(RULE_MAX_FIRST_TO_LOCATION,			iToCap)			BREAK
//				CASE RULE_LAST_MAN_STANDING				CAP_ITEM(RULE_MAX_LAST_MAN_STANDING,			iToCap)			BREAK
//				CASE RULE_TARGET_PEDS					CAP_ITEM(RULE_MAX_TARGET_PEDS,				 	iToCap)			BREAK
//			ENDSWITCH			
//		ENDIF
//		
//		
//	//for deathmatch
//	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
//		//PRINTSTRING(" g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH")PRINTNL()
//		//cap options
//		IF iTab = FMMC_TAB_GAME_SET_UP
//			//PRINTSTRING(" FMMC_TAB_GAME_SET_UP")PRINTNL()
//			SWITCH iCategorySelected
//				CASE OPTION_NUM_PLAYERS					CAP_ITEM(MAX_OPTION_NUM_PLAYERS + 1,				iToCap)			BREAK
//				CASE OPTION_TEAM_DEATHMATCH				CAP_ITEM(MAX_OPTION_TEAM_DEATHMATCH + 1,			iToCap)			BREAK
//				CASE OPTION_DURATION					CAP_ITEM(MAX_OPTION_DURATION + 1,		 			iToCap)			BREAK
//				CASE OPTION_TARGET						CAP_ITEM(MAX_OPTION_TARGET + 1,						iToCap)			BREAK
//				CASE OPTION_WEAPONS						CAP_ITEM(MAX_OPTION_WEAPONS + 1,					iToCap)			BREAK
//				CASE OPTION_WEAPON_RESPAWN				CAP_ITEM(MAX_OPTION_WEAPON_RESPAWN + 1,				iToCap)			BREAK
//				CASE OPTION_AUTO_AIM					CAP_ITEM(MAX_OPTION_AUTO_AIM + 1,					iToCap)			BREAK
//				CASE OPTION_POLICE						CAP_ITEM(MAX_OPTION_POLICE + 1,			 			iToCap)			BREAK
//				CASE OPTION_PEDS						CAP_ITEM(MAX_OPTION_PEDS + 1,						iToCap)			BREAK
//				CASE OPTION_BLIPS						CAP_ITEM(MAX_OPTION_BLIPS + 1,						iToCap)			BREAK
//				CASE OPTION_TAGS						CAP_ITEM(MAX_OPTION_TAGS + 1,						iToCap)			BREAK
//				CASE OPTION_RADIO						CAP_ITEM(MAX_OPTION_RADIO + 1,			 			iToCap)			BREAK
//				CASE OPTION_VOICE						CAP_ITEM(MAX_OPTION_VOICE + 1,			 			iToCap)			BREAK
//				CASE OPTION_SPAWN_TIME					CAP_ITEM(MAX_OPTION_SPAWN_TIME + 1,		 			iToCap)			BREAK
//				CASE OPTION_VEHICLES					CAP_ITEM(MAX_OPTION_VEHICLES + 1,		 			iToCap)			BREAK
//				//CASE OPTION_VEHICLES_RESPAWN			CAP_ITEM(MAX_OPTION_VEHICLES_RESPAWN + 1,		 	iToCap)			BREAK
//				
//			ENDSWITCH
//		//cap rules
//		ELIF iTab = FMMC_TAB_RULES
//			SWITCH iCategorySelected
//				CASE RULE_NUMBER_OF_KILLS				CAP_ITEM(RULE_MAX_NUMBER_OF_KILLS,				iToCap)			BREAK
//				CASE RULE_NUMBER_OF_DEATHS	 			CAP_ITEM(RULE_MAX_NUMBER_OF_DEATHS,				iToCap)			BREAK
//				CASE RULE_COLLECT_OBJECT				CAP_ITEM(RULE_MAX_COLLECT_OBJECT,				iToCap)			BREAK
//				CASE RULE_COLLECT_VEHICLE				CAP_ITEM(RULE_MAX_COLLECT_VEHICLE,				iToCap)			BREAK
//				CASE RULE_COLLECT_ENEMY					CAP_ITEM(RULE_MAX_COLLECT_ENEMY,				iToCap)			BREAK
//				CASE RULE_FIRST_TO_LOCATION 			CAP_ITEM(RULE_MAX_FIRST_TO_LOCATION,			iToCap)			BREAK
//				CASE RULE_LAST_MAN_STANDING				CAP_ITEM(RULE_MAX_LAST_MAN_STANDING,			iToCap)			BREAK
//				CASE RULE_TARGET_PEDS					CAP_ITEM(RULE_MAX_TARGET_PEDS,				 	iToCap)			BREAK
//			ENDSWITCH			
//		ENDIF
//		
//		
//	//for races
//	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
//		//cap options
//		IF iTab = FMMC_TAB_GAME_SET_UP
//			SWITCH iCategorySelected
//				CASE OPTION_RACE_NO_PLAYERS 
//					CAP_NUMBER_OF_PLAYERS_RACE(FMMCpassed)
//				BREAK
//				CASE OPTION_RACE_PLAYER_RESPAWN 		CAP_ITEM(OPTION_RACE_MAX_PLAYER_RESPAWN,		iToCap)			BREAK
//				CASE OPTION_RACE_NO_LAPS				CAP_ITEM(OPTION_RACE_MAX_NO_LAPS,				iToCap)			BREAK
//				CASE OPTION_RACE_TRAFFIC 				CAP_ITEM(OPTION_RACE_MAX_TRAFFIC, 				iToCap)			BREAK
//				CASE OPTION_RACE_WEAPONS				CAP_ITEM(OPTION_RACE_MAX_WEAPONS,				iToCap)			BREAK
//				CASE OPTION_RACE_WEAPON_RESPAWN 		CAP_ITEM(OPTION_RACE_MAX_WEAPON_RESPAWN,		iToCap)			BREAK
//				CASE OPTION_RACE_TYPE			 		
//					CAP_ITEM(OPTION_RACE_MAX_TYPES,					iToCap)		
//					CAP_NUMBER_OF_PLAYERS_RACE(FMMCpassed, TRUE)
//					FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_VEHICLE_CLASS]	= 0 
//					FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_DEFAULT_VEHICLE]	= 0
//				BREAK
//				
//				CASE OPTION_RACE_VEHICLE_CLASS 			
//					CAP_ITEM(GET_SELECTION_MAX_VEHICLE_CLASS(FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_TYPE]) + 1,		iToCap, FALSE, -1)		
//					PRINTLN("GET_SELECTION_MAX_VEHICLE_CLASS = ", GET_SELECTION_MAX_VEHICLE_CLASS(FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_TYPE]))
//					FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_DEFAULT_VEHICLE] = -1
//					//CAP_ITEM(GET_SELECTION_MAX_VEHICLE_TYPE(FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_TYPE], FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_VEHICLE_CLASS]), FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_DEFAULT_VEHICLE], FALSE,  -1)
//					PRINTLN("GET_SELECTION_MAX_VEHICLE_TYPE = ", GET_SELECTION_MAX_VEHICLE_TYPE(FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_TYPE], FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_VEHICLE_CLASS]))
//				BREAK
//				CASE OPTION_RACE_DEFAULT_VEHICLE		
//					IF FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_VEHICLE_CLASS] = -1
//						FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_DEFAULT_VEHICLE] = -1
//					ELSE
//						CAP_ITEM(GET_SELECTION_MAX_VEHICLE_TYPE(FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_TYPE], FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_VEHICLE_CLASS]), iToCap, FALSE,  -1)
//					ENDIF
//					PRINTLN("GET_SELECTION_MAX_VEHICLE_TYPE = ", GET_SELECTION_MAX_VEHICLE_TYPE(FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_TYPE], FMMCpassed.iMenuOption[FMMC_TAB_CREATION_RACE][OPTION_RACE_VEHICLE_CLASS]))
//				BREAK		
//				
//			ENDSWITCH
//		//cap rules
//		ELIF iTab  = FMMC_TAB_RULES
//			SWITCH iCategorySelected
//				CASE RULE_NUMBER_OF_KILLS				CAP_ITEM(RULE_MAX_NUMBER_OF_KILLS,				iToCap)			BREAK
//				CASE RULE_NUMBER_OF_DEATHS	 			CAP_ITEM(RULE_MAX_NUMBER_OF_DEATHS,				iToCap)			BREAK
//				CASE RULE_COLLECT_OBJECT				CAP_ITEM(RULE_MAX_COLLECT_OBJECT,				iToCap)			BREAK
//				CASE RULE_COLLECT_VEHICLE				CAP_ITEM(RULE_MAX_COLLECT_VEHICLE,				iToCap)			BREAK
//				CASE RULE_COLLECT_ENEMY					CAP_ITEM(RULE_MAX_COLLECT_ENEMY,				iToCap)			BREAK
//				CASE RULE_FIRST_TO_LOCATION 			CAP_ITEM(RULE_MAX_FIRST_TO_LOCATION,			iToCap)			BREAK
//				CASE RULE_LAST_MAN_STANDING				CAP_ITEM(RULE_MAX_LAST_MAN_STANDING,			iToCap)			BREAK
//				CASE RULE_TARGET_PEDS					CAP_ITEM(RULE_MAX_TARGET_PEDS,				 	iToCap)			BREAK
//			ENDSWITCH			
//		ENDIF
//	ENDIF
//ENDPROC
////Caps the current row selection
//PROC CAP_ROW_SELECTION(FMMC_MAIN_MENU_STRUCT &FMMCpassed, INT iTab, BOOL bRoundUp = FALSE)
//	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
//		IF iTab = FMMC_TAB_GAME_SET_UP
//			CAP_ITEM(ciNumberOfOptions_Mission, FMMCpassed.iCategorySelected, bRoundUp)
//		ELIF iTab = FMMC_TAB_RULES
//			CAP_ITEM(ciNumberOfRules_Mission, FMMCpassed.iCategorySelected, bRoundUp)
//		ENDIF
//	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
//		IF iTab = FMMC_TAB_GAME_SET_UP
//			CAP_ITEM(ciNumberOfOptions_DeathMatch, FMMCpassed.iCategorySelected, bRoundUp)
//		ELIF iTab = FMMC_TAB_RULES
//			CAP_ITEM(ciNumberOfRules_DeathMatch, FMMCpassed.iCategorySelected, bRoundUp)
//		ENDIF
//	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
//		IF iTab= FMMC_TAB_GAME_SET_UP
//			CAP_ITEM(ciNumberOfOptions_Race, FMMCpassed.iCategorySelected, bRoundUp)
//		ELIF iTab = FMMC_TAB_RULES
//			CAP_ITEM(ciNumberOfRules_Race, FMMCpassed.iCategorySelected, bRoundUp)
//		ENDIF
//	ENDIF
//ENDPROC
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////          Deal with button presses procs     	 ////////////////////////////////////////
////Controlls the switching of tabs and coloums
//PROC CONTROL_TAB_COLOUM_SWITCHING(TAB_PLACEMENT_TOOLS& TabPlacement, FMMC_MAIN_MENU_STRUCT &FMMCpassed)
//	//IF g_FMMC_STRUCT.iMissionType !=  FMMC_TYPE_MISSION
//	//OR g_bShow_FMMC_rulesMenu = FALSE
//	 
//		//Control the tabs
//		//move left
//		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Lb)
//		//OR IS_BUTTON_JUST_PRESSED(PAD1, LEFTSHOULDER1)	
//			TabPlacement.iSelectedTab--
//			//Cap	
//			CAP_ITEM(FMMC_TAB_MAXIMUM_NUMBER, TabPlacement.iSelectedTab)
//			CAP_ROW_SELECTION(FMMCpassed, TabPlacement.iSelectedTab, TRUE)
//		ENDIF
//		//move right
//		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Rb)
//		//OR IS_BUTTON_JUST_PRESSED(PAD1, RIGHTSHOULDER1)	
//			TabPlacement.iSelectedTab++
//			//Cap	
//			CAP_ITEM(FMMC_TAB_MAXIMUM_NUMBER, TabPlacement.iSelectedTab)
//			CAP_ROW_SELECTION(FMMCpassed, TabPlacement.iSelectedTab, TRUE)
//		ENDIF
//	//ENDIF
//	
//ENDPROC
//
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////              LOGIC                   		 ////////////////////////////////////////
//// Handle user input during lobby screens
//FUNC BOOL HAS_DM_LOBBY_SELECTION_LOGIC_FINISHED(TAB_PLACEMENT_TOOLS& TabPlacement, FMMC_MAIN_MENU_STRUCT &FMMCpassed)
//	
//	// Block pause menu
//	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
//	
//	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
//	//OR IS_BUTTON_JUST_PRESSED(PAD1, CROSS)
//		RETURN TRUE
//	ENDIF
//	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
//	AND g_bShow_FMMC_rulesMenu = TRUE
//		RETURN TRUE
//	ENDIF
//	IF HAS_HUD_CONTROL_BEEN_PRESSED(INPUT_FRONTEND_CANCEL) 
//	//OR IS_BUTTON_JUST_PRESSED(PAD1, CIRCLE)
//		// Reset position
//		IF FMMCpassed.iCategorySelected != 0
//			FMMCpassed.iCategorySelected = 0
//		ELIF TabPlacement.iSelectedTab= FMMC_TAB_OPTIONS
//			TabPlacement.iSelectedTab= FMMC_TAB_RULES
//		ELIF TabPlacement.iSelectedTab= FMMC_TAB_RULES
//			TabPlacement.iSelectedTab= FMMC_TAB_GAME_SET_UP			
//		ELIF TabPlacement.iSelectedTab = FMMC_TAB_GAME_SET_UP
//			HUD_CHANGE_STATE(HUD_STATE_CREATOR_LOBBY_PROMPT)
//			PRINTLN("HUD_CHANGE_STATE(HUD_STATE_CREATOR_LOBBY_PROMPT)")
//			SCRIPT_CLEANUP()
//			//g_FMMC_STRUCT.bKillFMMC = TRUE
//		ENDIF
//	ENDIF
//	
//	//Deal with Tab switching
//	CONTROL_TAB_COLOUM_SWITCHING(TabPlacement, FMMCpassed)
//	
//	//Deal with moving left and right
//	IF DECREASE_CURRENT_ROW(FMMCpassed.iDelayInt)
//		FMMCpassed.iCategorySelected--
//		CAP_ROW_SELECTION(FMMCpassed, TabPlacement.iSelectedTab)
//	ENDIF	
//	IF INCREASE_CURRENT_ROW(FMMCpassed.iDelayInt)
//		FMMCpassed.iCategorySelected++
//		CAP_ROW_SELECTION(FMMCpassed, TabPlacement.iSelectedTab)
//	ENDIF
//	//Cap items
//	
//	//Deal with moving left and right to increase and decrease options
//	//GAME SETUP 
//	IF TabPlacement.iSelectedTab = FMMC_TAB_GAME_SET_UP
//		//Deal with decreases
//		IF DECREASE_CURRENT_SELECTED_ITEM(FMMCpassed.iDelayInt)
//			FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][FMMCpassed.iCategorySelected]--
//			CAP_COLOUM_ITEMS(FMMCpassed, FMMC_TAB_GAME_SET_UP, FMMCpassed.iCategorySelected, FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][FMMCpassed.iCategorySelected])
//		ENDIF
//		//Deal with increases
//		IF INCREASE_CURRENT_SELECTED_ITEM(FMMCpassed.iDelayInt)	
//			FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][FMMCpassed.iCategorySelected]++
//			CAP_COLOUM_ITEMS(FMMCpassed, FMMC_TAB_GAME_SET_UP, FMMCpassed.iCategorySelected, FMMCpassed.iMenuOption[g_FMMC_STRUCT.iMissionType][FMMCpassed.iCategorySelected])
//		ENDIF
//		//Cap/wrap around the value
//	
//	//RULES 
//	ELIF TabPlacement.iSelectedTab= FMMC_TAB_RULES
//		//Deal with decreases
//		IF DECREASE_CURRENT_SELECTED_ITEM(FMMCpassed.iDelayInt)
//			FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][FMMCpassed.iCategorySelected]--
//			CAP_COLOUM_ITEMS(FMMCpassed, TabPlacement.iSelectedTab, FMMCpassed.iCategorySelected, FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][FMMCpassed.iCategorySelected])
//		ENDIF
//		//Deal with increases
//		IF INCREASE_CURRENT_SELECTED_ITEM(FMMCpassed.iDelayInt)	
//			FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][FMMCpassed.iCategorySelected]++
//			CAP_COLOUM_ITEMS(FMMCpassed, TabPlacement.iSelectedTab, FMMCpassed.iCategorySelected, FMMCpassed.iMenuRules[g_FMMC_STRUCT.iMissionType][FMMCpassed.iCategorySelected])
//		ENDIF
//		//Cap/wrap around the value
//	
//	//OPTIONS 
//	ELIF TabPlacement.iSelectedTab= FMMC_TAB_OPTIONS
//	
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC
//PROC SET_UP_ADDITIONAL_INFO_HEADING_TEXT()
//	SET_TEXT_SCALE(0.3500, 0.3500)
//	SET_TEXT_COLOUR(255, 255, 255, 255)
//	SET_TEXT_CENTRE(FALSE)
//	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
//	SET_TEXT_EDGE(0, 0, 0, 0, 0)
//ENDPROC
//PROC SET_UP_ADDITIONAL_INFO_TEXT()
//	SET_TEXT_SCALE(0.3000, 0.3000)
//	SET_TEXT_COLOUR(255, 255, 255, 255)
//	SET_TEXT_CENTRE(FALSE)
//	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
//	SET_TEXT_EDGE(0, 0, 0, 0, 0)
//ENDPROC
//FUNC STRING GET_HEADING_STRING_FROM_SELECTION(INT iCurrentSelection)
//	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
//		SWITCH iCurrentSelection
//			CASE OPTION_MISSION_NO_PLAYERS 			RETURN ""
//			CASE OPTION_MISSION_NO_TEAMS	 		RETURN ""
//			CASE OPTION_MISSION_PLAYER_RESPAWN 		RETURN ""
//			CASE OPTION_MISSION_NUMBER_OF_LIVES		RETURN ""
//			CASE OPTION_MISSION_WEAPONS				RETURN ""
//			CASE OPTION_MISSION_WEAPON_RESPAWN 		RETURN ""
//			CASE OPTION_MISSION_VEHICLES 			RETURN "FMMC_AIN_VH"
//			CASE OPTION_MISSION_ENEMIES 			RETURN "FMMC_AIN_AI"
//			CASE OPTION_MISSION_ENEMY_RESPAWN 		RETURN ""
//			CASE OPTION_MISSION_OBJECTS 			RETURN "FMMC_AIN_OB"
//			CASE OPTION_MISSION_SPAWN_AREA			RETURN ""
//		ENDSWITCH
//	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
//		SWITCH iCurrentSelection
//			CASE OPTION_NUM_PLAYERS					RETURN ""
//			CASE OPTION_TEAM_DEATHMATCH				RETURN ""
//			CASE OPTION_DURATION					RETURN ""
//			CASE OPTION_TARGET						RETURN ""
//			CASE OPTION_SPAWN_TIME					RETURN ""
//			CASE OPTION_WEAPONS						RETURN ""
//			CASE OPTION_WEAPON_RESPAWN				RETURN ""
//			CASE OPTION_AUTO_AIM					RETURN ""
//			CASE OPTION_POLICE						RETURN ""
//			CASE OPTION_PEDS						RETURN ""
//			CASE OPTION_BLIPS						RETURN ""
//			CASE OPTION_TAGS						RETURN ""
//			CASE OPTION_RADIO						RETURN ""
//			CASE OPTION_VOICE						RETURN ""
//			CASE OPTION_VEHICLES					RETURN "FMMC_AIN_VH"
//		ENDSWITCH
//	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
//		SWITCH iCurrentSelection
//			CASE OPTION_RACE_NO_PLAYERS 			RETURN ""
//			CASE OPTION_RACE_TYPE					RETURN ""
//			CASE OPTION_RACE_VEHICLE_CLASS 			RETURN "FMMC_AIN_RC0"
//			CASE OPTION_RACE_DEFAULT_VEHICLE		RETURN "FMMC_AIN_RC1"
//			CASE OPTION_RACE_PLAYER_RESPAWN 		RETURN ""
//			CASE OPTION_RACE_NO_LAPS				RETURN ""
//			CASE OPTION_RACE_TRAFFIC 				RETURN ""
//			CASE OPTION_RACE_WEAPONS				RETURN ""
//			CASE OPTION_RACE_WEAPON_RESPAWN 		RETURN ""
//		ENDSWITCH
//	ENDIF
//	RETURN ""
//ENDFUNC
//FUNC STRING GET_BASE_LABEL_FROM_SELECTION(INT iCurrentSelection)
//	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
//		SWITCH iCurrentSelection
//			CASE OPTION_MISSION_NO_PLAYERS 			RETURN ""
//			CASE OPTION_MISSION_NO_TEAMS	 		RETURN ""
//			CASE OPTION_MISSION_PLAYER_RESPAWN 		RETURN ""
//			CASE OPTION_MISSION_NUMBER_OF_LIVES		RETURN ""
//			CASE OPTION_MISSION_WEAPONS				RETURN ""
//			CASE OPTION_MISSION_WEAPON_RESPAWN 		RETURN ""
//			CASE OPTION_MISSION_VEHICLES 			RETURN "FMMC_VH_"
//			CASE OPTION_MISSION_ENEMIES 			RETURN "FMMC_PD_"
//			CASE OPTION_MISSION_ENEMY_RESPAWN 		RETURN ""
//			CASE OPTION_MISSION_OBJECTS 			RETURN "FMMC_OM_"
//			CASE OPTION_MISSION_SPAWN_AREA			RETURN ""
//		ENDSWITCH
//	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
//		SWITCH iCurrentSelection
//			CASE OPTION_NUM_PLAYERS					RETURN ""
//			CASE OPTION_TEAM_DEATHMATCH				RETURN ""
//			CASE OPTION_DURATION					RETURN ""
//			CASE OPTION_TARGET						RETURN ""
//			CASE OPTION_SPAWN_TIME					RETURN ""
//			CASE OPTION_WEAPONS						RETURN ""
//			CASE OPTION_WEAPON_RESPAWN				RETURN ""
//			CASE OPTION_AUTO_AIM					RETURN ""
//			CASE OPTION_POLICE						RETURN ""
//			CASE OPTION_PEDS						RETURN ""
//			CASE OPTION_BLIPS						RETURN ""
//			CASE OPTION_TAGS						RETURN ""
//			CASE OPTION_RADIO						RETURN ""
//			CASE OPTION_VOICE						RETURN ""
//			CASE OPTION_VEHICLES					RETURN "FMMC_VH_"
//		ENDSWITCH
//	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
//		SWITCH iCurrentSelection
//			CASE OPTION_RACE_NO_PLAYERS 			RETURN ""
//			CASE OPTION_RACE_TYPE					RETURN ""
//			CASE OPTION_RACE_VEHICLE_CLASS 			RETURN ""
//			CASE OPTION_RACE_DEFAULT_VEHICLE		RETURN ""
//			CASE OPTION_RACE_PLAYER_RESPAWN 		RETURN ""
//			CASE OPTION_RACE_NO_LAPS				RETURN ""
//			CASE OPTION_RACE_TRAFFIC 				RETURN ""
//			CASE OPTION_RACE_WEAPONS				RETURN ""
//			CASE OPTION_RACE_WEAPON_RESPAWN 		RETURN ""
//		ENDSWITCH
//	ENDIF
//	RETURN ""
//ENDFUNC
//FUNC  INT GET_MAX_ADDITIONAL_INFO(INT iCurrentSelection)
//IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
//		SWITCH iCurrentSelection
//			CASE OPTION_MISSION_NO_PLAYERS 			RETURN 0
//			CASE OPTION_MISSION_NO_TEAMS	 		RETURN 0
//			CASE OPTION_MISSION_PLAYER_RESPAWN 		RETURN 0
//			CASE OPTION_MISSION_NUMBER_OF_LIVES		RETURN 0
//			CASE OPTION_MISSION_WEAPONS				RETURN 0
//			CASE OPTION_MISSION_WEAPON_RESPAWN 		RETURN 0
//			CASE OPTION_MISSION_VEHICLES 			RETURN 8
//			CASE OPTION_MISSION_ENEMIES 			RETURN 8
//			CASE OPTION_MISSION_ENEMY_RESPAWN 		RETURN 0
//			CASE OPTION_MISSION_OBJECTS 			RETURN 4
//			CASE OPTION_MISSION_SPAWN_AREA			RETURN 0
//		ENDSWITCH
//	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
//		SWITCH iCurrentSelection
//			CASE OPTION_NUM_PLAYERS					RETURN 0
//			CASE OPTION_TEAM_DEATHMATCH				RETURN 0
//			CASE OPTION_DURATION					RETURN 0
//			CASE OPTION_TARGET						RETURN 0
//			CASE OPTION_SPAWN_TIME					RETURN 0
//			CASE OPTION_WEAPONS						RETURN 0
//			CASE OPTION_WEAPON_RESPAWN				RETURN 0
//			CASE OPTION_AUTO_AIM					RETURN 0
//			CASE OPTION_POLICE						RETURN 0
//			CASE OPTION_PEDS						RETURN 0
//			CASE OPTION_BLIPS						RETURN 0
//			CASE OPTION_TAGS						RETURN 0
//			CASE OPTION_RADIO						RETURN 0
//			CASE OPTION_VOICE						RETURN 0
//			CASE OPTION_VEHICLES					RETURN 8
//		ENDSWITCH
//	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
//		SWITCH iCurrentSelection
//			CASE OPTION_RACE_NO_PLAYERS 			RETURN 0
//			CASE OPTION_RACE_TYPE					RETURN 0
//			CASE OPTION_RACE_VEHICLE_CLASS 			RETURN 0
//			CASE OPTION_RACE_DEFAULT_VEHICLE		RETURN 0
//			CASE OPTION_RACE_PLAYER_RESPAWN 		RETURN 0
//			CASE OPTION_RACE_NO_LAPS				RETURN 0
//			CASE OPTION_RACE_TRAFFIC 				RETURN 0
//			CASE OPTION_RACE_WEAPONS				RETURN 0
//			CASE OPTION_RACE_WEAPON_RESPAWN 		RETURN 0
//		ENDSWITCH
//	ENDIF
//	RETURN 0
//ENDFUNC
//PROC RENDER_ADDITIONAL_INFO(FMMC_MAIN_MENU_STRUCT &FMMCpassed)
//	//Draw the background
//	DRAW_RECT(0.5000, 0.5250, 0.1980, 0.6000, 0, 0, 0, 140)
//
//	INT iCurrentSelection = FMMCpassed.iCategorySelected
//	INT iCount
//	TEXT_LABEL_15 tl15Base = GET_BASE_LABEL_FROM_SELECTION(iCurrentSelection)
//	TEXT_LABEL_15 tl15
//	INT iMaxadditionalInfo = GET_MAX_ADDITIONAL_INFO(iCurrentSelection)
//	//Do the heading text
//	SET_UP_ADDITIONAL_INFO_HEADING_TEXT()
//	DISPLAY_TEXT(0.4050, 0.2300, GET_HEADING_STRING_FROM_SELECTION(iCurrentSelection))
//
//	//Draw the additional info
//	IF iMaxadditionalInfo > 0
//		FOR iCount = 0 TO (iMaxadditionalInfo - 1)
//			SET_UP_ADDITIONAL_INFO_TEXT()
//			tl15 = tl15Base
//			tl15 += iCount
//			DISPLAY_TEXT(0.4050, 0.2600 + (0.02 * iCount), tl15 )
//		ENDFOR
//	ENDIF
//ENDPROC
//
//// The function Brenda calls in MP options
//FUNC BOOL HAS_FMMC_MENU_FINISHED(LARGE_PLACEMENT_TOOLS& Placement, TAB_PLACEMENT_TOOLS& TabPlacement,  TEXT_STYLE& Style, FMMC_MAIN_MENU_STRUCT &FMMCpassed #IF IS_DEBUG_BUILD, WIDGET_GROUP_ID &ParentWidget #ENDIF)
////	IF g_FMMC_STRUCT.iMissionType =  FMMC_TYPE_MISSION
//		RETURN TRUE
////	ENDIF
//	SWITCH FMMCpassed.iLobbyProgress
//		//Set up
//		CASE 0
//			//Reset and load
//			IF IS_FMMC_MENU_SET_UP(Placement, FMMCpassed)
//				IF HAS_FMMC_MENU_SCALEFORM_LOADED(Placement, FMMCpassed)				
//					FMMCpassed.iLobbyProgress++	
//				ENDIF
//			ENDIF
//		BREAK
//		CASE 1
//			IF HAVE_FMMC_MENU_TEXTURES_LOADED()
//
//				HIDE_HUD_AND_RADAR_THIS_FRAME()
//				IF g_bShow_FMMC_rulesMenu = FALSE
//					DRAW_MAIN_HUD_BACKGROUND()
//					RENDER_DM_SCALEFORM_BAR(Placement)	
//				//ELSE
//					//TabPlacement.iSelectedTab = FMMC_TAB_RULES
//				ENDIF
//				IF g_FMMC_STRUCT.iMissionType =  FMMC_TYPE_MISSION
//					
//					SWITCH TabPlacement.iSelectedTab
//						CASE FMMC_TAB_GAME_SET_UP
//							DRAW_TEXT(Placement.MainTitle, Placement.aStyle.TS_TITLEHUD, "FMMC_MU_TL_0") 	// "Freemode Mission Creation"
//							RENDER_DM_LOBBY_TABS(Placement, TabPlacement, FMMCpassed, "FMMC_TAB_M")
//							RENDER_DM_CATEGORY_NAMES(Placement, FMMCpassed, "FMMC_T0_M", ciNumberOfOptions_Mission)
//							RENDER_ALL_MISSION_SUB_OPTIONS(Placement, TabPlacement, Style, FMMCpassed)
//							FMMCpassed.iCurrentMenuLength = ciNumberOfOptions_Mission
//							g_bShow_FMMC_rulesMenu = FALSE
//							RENDER_ADDITIONAL_INFO(FMMCpassed)
//							
//						BREAK
//						CASE FMMC_TAB_RULES
//							g_bShow_FMMC_rulesMenu = TRUE
//						BREAK
//						CASE FMMC_TAB_OPTIONS
//							g_bShow_FMMC_rulesMenu = FALSE
//						BREAK
//					ENDSWITCH
//					
//				ELIF g_FMMC_STRUCT.iMissionType =  FMMC_TYPE_DEATHMATCH
//					//PRINTLN("OPTION_VEHICLES_RESPAWN = ",  FMMCmainMenuStruct.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_VEHICLES_RESPAWN])
//				
//					DRAW_TEXT(Placement.MainTitle, Placement.aStyle.TS_TITLEHUD, "FMMC_MU_TL_1") 	// "Freemode Deathmatch Creation"
//					RENDER_DM_LOBBY_TABS(Placement, TabPlacement, FMMCpassed, "FMMC_TAB_D")
//					#IF IS_DEBUG_BUILD
//					SETUP_WIDGETS_FMMC_MENU(Placement, FMMCpassed,  ParentWidget)
//					#ENDIF
//					SWITCH TabPlacement.iSelectedTab
//						CASE FMMC_TAB_GAME_SET_UP
//							RENDER_DM_CATEGORY_NAMES(Placement, FMMCpassed, "LOB_CAT_", ciNumberOfOptions_DeathMatch)
//							RENDER_ALL_DM_SUB_OPTIONS(Placement, TabPlacement, Style, FMMCpassed)
//							FMMCpassed.iCurrentMenuLength = ciNumberOfOptions_DeathMatch
//						BREAK
//						CASE FMMC_TAB_RULES
//						//	RENDER_DM_CATEGORY_NAMES(Placement, FMMCpassed, "FMMC_T1_R", ciNumberOfRules_DeathMatch)
//							//RENDER_ALL_DM_SUB_OPTIONS(Placement, TabPlacement, Style, FMMCpassed)
//							FMMCpassed.iCurrentMenuLength = ciNumberOfRules_DeathMatch
//						BREAK
//						CASE FMMC_TAB_OPTIONS
//						BREAK
//					ENDSWITCH		
//					RENDER_DM_MINIMAP(Placement)
//					RENDER_DM_HOW_TO_PLAY_BOX(Placement)
//					RENDER_DM_SCOREBOX(Placement)
//	//PRINTLN("OPTION_VEHICLES_RESPAWN = ",  FMMCmainMenuStruct.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_VEHICLES_RESPAWN])
//					
//				ELIF g_FMMC_STRUCT.iMissionType =  FMMC_TYPE_RACE
//					DRAW_TEXT(Placement.MainTitle, Placement.aStyle.TS_TITLEHUD, "FMMC_MU_TL_2") 	// "Freemode Race Creation"
//					RENDER_DM_LOBBY_TABS(Placement, TabPlacement, FMMCpassed, "FMMC_TAB_R")
//					SWITCH TabPlacement.iSelectedTab
//						CASE FMMC_TAB_GAME_SET_UP
//							RENDER_DM_CATEGORY_NAMES(Placement, FMMCpassed, "FMMC_TR_M", ciNumberOfOptions_Race)
//							RENDER_ALL_RACE_SUB_OPTIONS(Placement, TabPlacement, Style, FMMCpassed)
//							FMMCpassed.iCurrentMenuLength = ciNumberOfOptions_Race
//						BREAK
//						CASE FMMC_TAB_RULES
//							RENDER_DM_CATEGORY_NAMES(Placement, FMMCpassed, "FMMC_T2_R",  ciNumberOfRules_Race)
//							RENDER_ALL_RACE_SUB_OPTIONS(Placement, TabPlacement, Style, FMMCpassed)
//							FMMCpassed.iCurrentMenuLength = ciNumberOfRules_Race
//						BREAK
//						CASE FMMC_TAB_OPTIONS							
//						BREAK
//					ENDSWITCH						
//				ENDIF
//				
//				IF FMMCpassed.iCurrentMenuLength > (CAT_MAX - 1)
//					RENDER_DM_SCROLLING_ARROWS(Placement, FMMCpassed)
//				ENDIF	
//				
//				IF HAS_DM_LOBBY_SELECTION_LOGIC_FINISHED(TabPlacement, FMMCpassed)
//					RETURN TRUE
//				ENDIF
//			ENDIF
//		BREAK
//		DEFAULT 
//			#IF IS_DEBUG_BUILD
//				SCRIPT_ASSERT("DM Lobby: Problem in FMMCpassed.iLobbyProgress") 
//			#ENDIF
//		BREAK
//	ENDSWITCH
//	
//	RETURN FALSE
//ENDFUNC

MAIN_SERVER_MISSION_STAGES 	MissionStage = GAME_STATE_INI

INT iLocalBitSet
INT iCurrentCreationType
INT iCurrentCreationSubType
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////                      Main Loop                      //////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//LARGE_PLACEMENT_TOOLS 	FMMCPlacement
//TAB_PLACEMENT_TOOLS 	FMMCTabPlacement

SCRIPT
	//RESET_FMMC_GLOBALS()	
//	IF HAS_FORCE_CLEANUP_OCCURRED() 
//		SCRIPT_CLEANUP()
 //   ENDIF
	
	#IF IS_DEBUG_BUILD
		//CREATE_WIDGETS()
	#ENDIF
//	SET_VEHICLE_DENSITY_MULTIPLIER(0.0)
	//SET_PED_DENSITY_MULTIPLIER(0.0)
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP))
		PRINTLN("FORCE_CLEANUP_FLAG_SP_TO_MP")
		SCRIPT_CLEANUP()
	ENDIF
	//Text and scale form
	//SETUP_SCALEFORM()
	//SET_UP_TAB_MENU_TEXT(FMMC_ENTITY_PLACEMENT_TAB)
	REQUEST_ADDITIONAL_TEXT("FMMC", ODDJOB_TEXT_SLOT)
	//REQUEST_STREAMED_TEXTURE_DICT("MPHUD", TRUE)
	//Placement1.ButtonMovie = REQUEST_SCALEFORM_MOVIE("instructional_buttons")
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//	SET_PAUSE_MENU_ACTIVE(FALSE)

	iCurrentCreationSubType = g_FMMC_STRUCT.iMissionSubType
	
	g_bOkToExitMissionCreator = FALSE 
	FMMCmainMenuStruct.iLastTypeCreated = g_FMMC_STRUCT.iMissionType
	PRINTLN("FMMC_MAIN_MENU Setting g_bTransitionTellCreatorToCleanup to FALSE")
	g_bTransitionTellCreatorToCleanup = FALSE
	g_bKillFMMCmenu =FALSE
//	DISABLE_STUNT_JUMP_SET(ciSINGLE_PLAYER_SJS_GROUP)
	PRINTLN("MPUSJ - DISABLE_STUNT_JUMP_SET(ciSINGLE_PLAYER_SJS_GROUP)")
//	DISABLE_STUNT_JUMP_SET(ciFREEMODE_PLAYER_SJS_GROUP)
//	PRINTLN("MPUSJ - DISABLE_STUNT_JUMP_SET(ciFREEMODE_PLAYER_SJS_GROUP)")
	iCurrentCreationType = g_FMMC_STRUCT.iMissionType
	
	// NOTE: CLEAR_FMMC_STRUCT() wipes our missionSubType. We don't want this to be the case if its a mission as otherwise
	// LTS / CTF etc won't be launched correctly. So re-instate the value. Be aware issues of switching between mission creators as
	// more are released.
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
		PRINTLN("Type is MISSION - re-initialise subtype so we launch correct creator script")
		g_FMMC_STRUCT.iMissionSubType = iCurrentCreationSubType 
	ENDIF
	
	g_bShow_FMMC_mainMenu = FALSE
	
	// Main loop.
	WHILE TRUE
	
		IF iCurrentCreationType != g_FMMC_STRUCT.iMissionType
			PRINTLN("iCurrentCreationType != g_FMMC_STRUCT.iMissionT")
			SET_DEFAULT_MENU_VALUES(FMMCmainMenuStruct)
			iCurrentCreationType = g_FMMC_STRUCT.iMissionType
			CLEAR_BIT(FMMCmainMenuStruct.iLobbyBitset, FMMC_MENU_BITSET_INITIALISED_DM)
		ENDIF
		
//		IF bLoadedHeaderData = FALSE
//			IF LOAD_HEADER_DATA(FMMCheaderDataStruct)
//				bLoadedHeaderData = TRUE
//			ENDIF
//		ENDIF
		
		
		
		IF GET_MISSION_CREATOR_EXIT_STATE() = MISSIONCREATORHUD_QUIT
			PRINTLN("FMMC_MAIN_MENU  GET_MISSION_CREATOR_EXIT_STATE() = MISSIONCREATORHUD_QUIT")			
			SCRIPT_CLEANUP()
		ENDIF
		
		IF IS_CREATOR_MENU_TRIGGERED()
			PRINTLN("FMMC_MAIN_MENU  IS_CREATOR_MENU_TRIGGERED() = TRUE Cleanup Script")			
			SCRIPT_CLEANUP()
		ENDIF
		
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		WAIT(0) 

	
		SWITCH MissionStage

			CASE GAME_STATE_INI
				REQUEST_ADDITIONAL_TEXT("FMMC", ODDJOB_TEXT_SLOT)
				IF HAS_THIS_ADDITIONAL_TEXT_LOADED("FMMC",ODDJOB_TEXT_SLOT)
					MissionStage = GAME_STATE_DO_MENU
					PRINTLN("FM_Main_Menu - MissionStage = GAME_STATE_DO_MENU")				
				ENDIF
			BREAK
			
			CASE GAME_STATE_DO_MENU
				//PRINTLN("GAME_STATE_DO_MENU")
				//Check to see if the STRUCT should be cleaned up.
				
				IF FMMCmainMenuStruct.iLastTypeCreated != g_FMMC_STRUCT.iMissionType
					PRINTLN("FMMCmainMenuStruct.iLastTypeCreated != g_FMMC_STRUCT.iMissionType")
					PRINTLN(" FMMCmainMenuStruct.iLastTypeCreated = ", FMMCmainMenuStruct.iLastTypeCreated)
					PRINTLN(" g_FMMC_STRUCT.iMissionType = ",g_FMMC_STRUCT.iMissionType)
					FMMCmainMenuStruct.iLastTypeCreated = g_FMMC_STRUCT.iMissionType
					SET_DEFAULT_MENU_VALUES(FMMCmainMenuStruct)
				ENDIF
//				SET_PAUSE_MENU_ACTIVE(TRUE)	
				DISABLE_FRONTEND_THIS_FRAME()
				CONVERT_FMMC_MENU_STRUCT_TO_GLOBALS(FMMCmainMenuStruct)
				g_bShow_FMMC_rulesMenu = FALSE
				g_bShow_FMMC_mainMenu  = FALSE
				MissionStage = GAME_STATE_CREATE			
				PRINTLN("FM_Main_Menu - MissionStage = GAME_STATE_CREATE")				
				IF NOT IS_SELECTOR_DISABLED()
					disable_selector()
				ENDIF	
			BREAK
			CASE GAME_STATE_CREATE
				IF IS_THIS_A_MISSION()
					IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_LTS
						IF FM_LAUNCH_LTS_CREATOR_SCRIPT(iLocalBitSet)
							MissionStage = GAME_STATE_CAMERA_BACK_UP
							PRINTLN("FM_Main_Menu - MissionStage = GAME_STATE_CAMERA_BACK_UP")				
						ENDIF
					ELIF IS_THIS_A_SURVIVAL()
						IF FM_LAUNCH_SURVIVAL_CREATOR_SCRIPT(iLocalBitSet)
							MissionStage = GAME_STATE_CAMERA_BACK_UP
							PRINTLN("FM_Main_Menu - MissionStage = GAME_STATE_CAMERA_BACK_UP")				
						ENDIF
					ELIF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_BASIC
						IF FM_LAUNCH_BASIC_CREATOR_SCRIPT(iLocalBitSet)
							MissionStage = GAME_STATE_CAMERA_BACK_UP
							PRINTLN("FM_Main_Menu - MissionStage = GAME_STATE_CAMERA_BACK_UP")	
						ENDIF
					ELIF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_FREEMODE
						IF FM_LAUNCH_FREEMODE_CREATOR_SCRIPT(iLocalBitSet)
							MissionStage = GAME_STATE_CAMERA_BACK_UP
							PRINTLN("FM_Main_Menu - MissionStage = GAME_STATE_CAMERA_BACK_UP")	
						ENDIF
					ELIF g_FMMC_STRUCT.iMissionSubType != FMMC_MISSION_TYPE_CTF
					AND g_FMMC_STRUCT.iMissionType != FMMC_TYPE_MISSION_CTF
						IF FM_LAUNCH_MISSION_CREATOR_SCRIPT(iLocalBitSet)
							MissionStage = GAME_STATE_CAMERA_BACK_UP
							PRINTLN("FM_Main_Menu - MissionStage = GAME_STATE_CAMERA_BACK_UP")				
						ENDIF
					ELSE
						IF FM_LAUNCH_CAPTURE_CREATOR_SCRIPT(iLocalBitSet)
							MissionStage = GAME_STATE_CAMERA_BACK_UP
							PRINTLN("FM_Main_Menu - MissionStage = GAME_STATE_CAMERA_BACK_UP")				
						ENDIF
					ENDIF
				ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH					
					IF FM_LAUNCH_DEATHMATCH_CREATOR_SCRIPT(iLocalBitSet)
						MissionStage = GAME_STATE_CAMERA_BACK_UP
						PRINTLN("FM_Main_Menu - MissionStage = GAME_STATE_CAMERA_BACK_UP")				
					ENDIF
				ELIF IS_THIS_A_RACE()
					IF FM_LAUNCH_RACE_CREATOR_SCRIPT(iLocalBitSet)
						MissionStage = GAME_STATE_CAMERA_BACK_UP
						PRINTLN("FM_Main_Menu - MissionStage = GAME_STATE_CAMERA_BACK_UP")				
					ENDIF
				ENDIF
				
				IF g_bShow_FMMC_mainMenu = TRUE
					MissionStage = GAME_STATE_WAIT
					PRINTLN("FM_Main_Menu - MissionStage = GAME_STATE_WAIT")				
					g_bShow_FMMC_mainMenu = FALSE
				ENDIF
			BREAK
			CASE GAME_STATE_WAIT
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
					MissionStage = GAME_STATE_DO_MENU
					PRINTLN("FM_Main_Menu - MissionStage = GAME_STATE_DO_MENU")				
				ENDIF
			BREAK
			CASE GAME_STATE_CAMERA_BACK_UP
				//IF SET_SKYSWOOP_UP()
//					SET_PAUSE_MENU_ACTIVE(FALSE)
					MissionStage = GAME_STATE_DO_MENU
					PRINTLN("FM_Main_Menu - MissionStage = GAME_STATE_DO_MENU")				
				//ENDIF 
			BREAK
			
			// Look for game end conditions.
			CASE GAME_STATE_SAVE_OUT_DATA
				CONVERT_FMMC_MENU_STRUCT_TO_GLOBALS(FMMCmainMenuStruct)
				//Save the data
				//Move on to the next section of the mission
//				IF SAVEOUT_DATA(FMMCdataStruct)
//					
//				ENDIF
			BREAK
			
			CASE GAME_STATE_END			
				//end this script please
				PRINTLN("FMMC_MAIN_MENU  MissionStage = GAME_STATE_END")			
				
				SCRIPT_CLEANUP()
			BREAK	
			
		ENDSWITCH
		
		IF g_bKillFMMCmenu
			g_bKillFMMCmenu =FALSE
			PRINTLN("FMMC_MAIN_MENU  g_bKillFMMCmenu")			
			SCRIPT_CLEANUP()		
		ENDIF
		IF IS_TRANSITION_TELLING_CREATOR_TO_CLEANUP()
			PRINTLN(" IS_TRANSITION_TELLING_CREATOR_TO_CLEANUP()")			
			g_bKillFMMCmenu =FALSE
			SCRIPT_CLEANUP()		
		ENDIF
		
		#IF IS_DEBUG_BUILD	
			//Maintain_Hud_Creator_Tool(wgGroup)
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				PRINTLN("FMMC_MAIN_MENU  IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)")			
				SCRIPT_CLEANUP()
			ENDIF
		#ENDIF
		
		IF g_FMMC_STRUCT.bKillFMMC
			PRINTLN("FMMC_MAIN_MENU g_FMMC_STRUCT.bKillFMMC")			
			SCRIPT_CLEANUP()
		ENDIF
		
		//If we've been signed out the wait for Brenda to dealwith cleanup
		IF g_bKillFMMC_SignedOut
			SCRIPT_CLEANUP()
			PRINTLN("g_bKillFMMC_SignedOut = TRUE")
		ENDIF
		
	//PRINTLN("OPTION_VEHICLES_RESPAWN = ", FMMCmainMenuStruct.iMenuOption[FMMC_TYPE_DEATHMATCH][OPTION_VEHICLES_RESPAWN])
	
	ENDWHILE
	
// End of Mission
ENDSCRIPT








