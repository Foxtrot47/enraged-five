//////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        FM_LTS_CREATOR																	//
// Description: Make your own LTS missions														//
// Written by:  Various																			//
// Date: 01/07/2014																				//
//////////////////////////////////////////////////////////////////////////////////////////////////
///    
USING "rage_builtins.sch"
USING "globals.sch"
USING "script_player.sch"
USING "commands_pad.sch"
USING "FMMC_header.sch"
USING "FMMC_keyboard.sch"
USING "PM_MissionCreator_Public.sch"
USING "FM_Post_Mission_Cleanup.sch"
USING "FMMC_Bot_Controller.sch"
USING "net_snowball.sch"

#IF IS_DEBUG_BUILD
USING "hud_creator_tool.sch"
USING "profiler.sch"
USING "FMMC_Launcher_Menu.sch"
#ENDIF

SNOWBALL_DATA SnowballData


// Bit Fields
INT iLocalBitSet
INT iHelpBitSetOld 								= - 1
INT iHelpBitSet
INT iNumPointsPerTeam
INT iVehicleModelLoadTimers[FMMC_MAX_VEHICLES]
INT iPropModelTimers[FMMC_MAX_NUM_PROPS]

// iHelpBitSet Values
CONST_INT biPickupEntityButton 						0
CONST_INT biPickupCoronaButton						3
CONST_INT biRotateCoronaButton						4
CONST_INT biDeleteEntityButton						5
CONST_INT biWarpToCameraButton 						7
CONST_INT biWarpToCoronaButton 						8
CONST_INT biRemovePedsButton						9

// Trigger Stuff
INT iTriggerCreationStage = CREATION_STAGE_WAIT

// Photo and Lobby Cameras
SCALEFORM_INDEX SF_Movie_Gallery_Shutter_Index
INT iIntroCamPlacementStage
VECTOR vInitialPreviewPos
INT iCamPanState = PAN_CAM_SETUP
PREVIEW_PHOTO_STRUCT PPS
BLIP_INDEX bCameraTriggerBlip
BLIP_INDEX bCameraPanBlip
BLIP_INDEX bPhotoBlip
BLIP_INDEX bTeamCamPanBlip[FMMC_MAX_TEAMS]
BLIP_INDEX bCameraOutroBlip
GET_UGC_CONTENT_STRUCT sGetUGC_content
JOB_INTRO_CUT_DATA jobIntroData
BOOL bInitialIntroCamSetup = TRUE
BOOL bInitialIntroCamWarp = FALSE

BLIMP_SIGN sBlimpSign
 
// Test
BOOL bTestModeControllerScriptStarted
mission_display_struct on_mission_gang_box
BOOL bTextSetUp
MODEL_NAMES			mnDefaultPeds[4]
TEST_PEDS 			tpBotnet[32]
REL_GROUP_HASH 		rgTeamRel[MAX_NUM_NETWORK_SCRIPTS_ALLOWED]
INT iTeams[4]

//INT iSelectCoolOffTimer
int iTopItem
BOOL bContentReadyForUGC = FALSE
BOOL bCreatorLimitedCloudDown = FALSE
BOOL bSignedOut = FALSE

BLIP_INDEX biBounds

BOOL bDelayAFrame = TRUE

CONST_INT MAX_OBJECTIVE_TEXT_LENGTH					63-12 // Reserve characters for colouring e.g. "~r~~s~~y~~s~"

ENUM RESET_STATE 
	RESET_STATE_FADE,
	RESET_STATE_CLEAR,
	RESET_STATE_WEAPONS,
	RESET_STATE_VEHICLES,
	RESET_STATE_PROPS,
	RESET_STATE_DYNOPROPS,
	RESET_STATE_OBJECTS,
	RESET_STATE_CRATES,
	RESET_STATE_PEDS,
	RESET_STATE_ATTACH,
	RESET_STATE_TEAM_SPAWNS,
	RESET_STATE_OTHER,
	RESET_STATE_FINISH
ENDENUM
RESET_STATE iResetState

STRUCT_REL_GROUP_HASH 					sRGH
FMMC_LOCAL_STRUCT 						sFMMCdata
FMMC_CAM_DATA 							sCamData
PED_CREATION_STRUCT 					sPedStruct
TEAM_SPAWN_CREATION_STRUCT				sTeamSpawnStruct[FMMC_MAX_TEAMS]
VEHICLE_CREATION_STRUCT					sVehStruct
WEAPON_CREATION_STRUCT					sWepStruct
OBJECT_CREATION_STRUCT					sCapObjStruct
PROP_CREATION_STRUCT					sPropStruct
DYNOPROP_CREATION_STRUCT				sDynoPropStruct
INVISIBLE_OBJECT_STRUCT					sInvisibleObjects
LOCATION_CREATION_STRUCT				sLocStruct

FMMC_UP_DOWN_STRUCT						menuScrollController

HUD_COLOURS_STRUCT 						sHCS
CREATION_VARS_STRUCT 					sCurrentVarsStruct
structFMMC_MENU_ITEMS 					sFMMCmenu
START_END_BLIPS							sStartEndBlips
FMMC_COMMON_MENUS_VARS 					sFMMCendStage
TRAIN_CREATION_STRUCT 					sTrainCreationStruct

BOOL ButtonPressed

INT iMaxSpawnsPerTeam[FMMC_MAX_TEAMS]

//variables for the mission creator tutorial //////////////
BOOL bshowmenu = TRUE
BOOL bShowMenuHighlight = FALSE
BOOL bMpNeedsCleanedUp
BOOL bMpModeCleanedUp

INT iTestFailBitSet

INT iTeamSpawnLocations = 1

INT iSwitchCam = 0, iSwitchTeam
VECTOR vSwitchVec = <<0,0,0>>
FLOAT fSwitchHeading = 0
BOOL bSwitchingCam = FALSE

INT iDoorSetupStage
INT iDoorSlowLoop
BOOL bLastUseMouseKeyboard = FALSE

BOOL bIsMaleSPTC
BOOL bIsCreatorModelSetSPTC

#IF IS_DEBUG_BUILD

WIDGET_GROUP_ID wgGroup
BOOL bLoadXML
INT iXmlToLoad, iXmlToSave

BOOL bGetUGC_byIDWithString, bLoadFromOtherUaer

BOOL bGetMyUGC, bSaveToCloud, b948049Repo, bGetChallenges, bGetSpPlaylist, bGetAChallenge, bUse_GET_GET_BY_CONTENT_ID, bCallDATAFILE_PUBLISH_UGC, bGetHeader1, bEndCreator, 	bLoadOldData, bGetRockstarCandidateUGC, bDelete1, bGetUGC_byID,/* bUSE_NEW_UGC_SYSTEM_Local, */bGetMyPlaylists,	bGetMyPlaylistsDetails
BOOL bDealWithNewSave, bPublishMissionNew, bVersionMissionNew, bOverWriteMissionNew, bCopyContentByID
BOOL bDoTriggerRestricted = TRUE
STRUCT_DATA_FILE dataStruct

INT iContentType, iPlayList, iCATEGORYtype, iUGCtype
INT iPublishedSelection1 = -1
BOOL bVisCheckFlags[4]
TEXT_WIDGET_ID twID
BOOL bSetSomeLimits

PROC CREATE_WIDGETS( )
	
	INT i, i2
	TEXT_LABEL_15 tl15
	TEXT_LABEL_31 tl31
	wgGroup= START_WIDGET_GROUP( " FMMC Mission Creator") 
		ADD_WIDGET_BOOL(" Add RockStar Blips", sFMMCendStage.sRocStarCreatedVars.bDisplayRockstarUGC)
		ADD_WIDGET_BOOL(" Allow Mouse Input", sFMMCmenu.bAllowMouseInput)
		ADD_WIDGET_BOOL(" Allow Mouse Entity Selection", sFMMCmenu.bAllowMouseSelection)
		
		ADD_WIDGET_BOOL("Draw release boxes", sFMMCMenu.bDrawReleaseBoxes)
		ADD_WIDGET_INT_READ_ONLY("g_iMenuCursorItem", g_iMenuCursorItem)
		
		START_WIDGET_GROUP("NewSaveFlow")
			ADD_WIDGET_BOOL("bDealWithNewSave", bDealWithNewSave)
			ADD_WIDGET_BOOL("bPublishMissionNew", bPublishMissionNew)
			ADD_WIDGET_BOOL("bVersionMissionNew", bVersionMissionNew)
			ADD_WIDGET_BOOL("bOverWriteMissionNew", bOverWriteMissionNew)
			ADD_WIDGET_BOOL("bCopyContentByID", bCopyContentByID)
			//twID2 = ADD_TEXT_WIDGET("")
			ADD_WIDGET_BOOL("Get it", bGetUGC_byIDWithString )
			ADD_WIDGET_BOOL("From another user", bLoadFromOtherUaer )
		STOP_WIDGET_GROUP()
		
		PRINTLN("CREATE_WIDGETS 4")
		START_WIDGET_GROUP("Vehicles")
			FOR i = 0 TO FMMC_MAX_VEHICLES-1
				FOR i2 = 0 TO FMMC_MAX_TEAMS-1
					tl31 = "Veh "
					tl31 += i
					tl31 += " rule "
					tl31 += i2
					ADD_WIDGET_INT_READ_ONLY(tl31, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRule[i2])
				ENDFOR
			ENDFOR
		STOP_WIDGET_GROUP()
		PRINTLN("CREATE_WIDGETS 7")
		START_WIDGET_GROUP("STATS AND SAVING")
			ADD_WIDGET_BOOL("Made a change", sCurrentVarsStruct.creationStats.bMadeAChange)
			ADD_WIDGET_INT_SLIDER("Times tested", sCurrentVarsStruct.creationStats.iTimesTestedLoc, 0, 50, 1)
			ADD_WIDGET_BOOL("Editing a Saved Creation", sCurrentVarsStruct.creationStats.bEditingACreation)
			ADD_WIDGET_BOOL("Previously Published", g_FMMC_STRUCT.bMissionIsPublished)
		STOP_WIDGET_GROUP()
		PRINTLN("CREATE_WIDGETS 11")
		START_WIDGET_GROUP( "   NEW UGC") 
			ADD_WIDGET_BOOL(".bGetSpPlaylist", bGetSpPlaylist)
			
			START_WIDGET_GROUP( "    GET_UGC_CONTENT_BY_TYPE") 
			
				ADD_WIDGET_BOOL(".bGetMyUGC", bGetMyUGC)
				ADD_WIDGET_INT_SLIDER("GET Type ", iContentType, 0, 30, 1)
				ADD_WIDGET_INT_SLIDER("CATEGORY Type", iCATEGORYtype, 0, 30, 1)
				ADD_WIDGET_INT_SLIDER("UGC Type", iUGCtype, 0, 30, 1)
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_BOOL(".bLoadOldData", bLoadOldData)
			ADD_WIDGET_BOOL(".bGetAChallenge", bGetAChallenge)
			ADD_WIDGET_BOOL(".bGetChallenges", bGetChallenges)
			ADD_WIDGET_BOOL("bEndCreator", bEndCreator )
			ADD_WIDGET_BOOL("bGetMyPlaylists", bGetMyPlaylists )
			ADD_WIDGET_BOOL("..SAVE_MISSION_TO_UGC_SERVER", bGetMyPlaylistsDetails )
			ADD_WIDGET_INT_SLIDER("iPlayList ", iPlayList, 0, 30, 1)
			
			START_WIDGET_GROUP( " UGC_GET_GET_BY_CONTENT_ID") 
				twID = ADD_TEXT_WIDGET("")
				ADD_WIDGET_BOOL("Get it", bGetUGC_byIDWithString )
				ADD_WIDGET_BOOL("From another user", bLoadFromOtherUaer )
				
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_INT_SLIDER("iPublishedSelection1 ", iPublishedSelection1, -1, 200, 1)
			
			ADD_WIDGET_BOOL("bSkipName", sFMMCendStage.bSkipName)
			ADD_WIDGET_BOOL("  bGetUGC_byID", bGetUGC_byID )
			
			ADD_WIDGET_BOOL(" b948049Repo", b948049Repo )
			ADD_WIDGET_BOOL(" bDelete1", bDelete1 )
			ADD_WIDGET_BOOL(" bUse_GET_GET_BY_CONTENT_ID", bUse_GET_GET_BY_CONTENT_ID )
			ADD_WIDGET_BOOL(" bGetRockstarCandidateUGC", bGetRockstarCandidateUGC )
			ADD_WIDGET_BOOL(" bGetMyUGC", bGetMyUGC )
//			ADD_WIDGET_BOOL(" g_bUSE_NEW_UGC_SYSTEM", g_bUSE_NEW_UGC_SYSTEM )
//			ADD_WIDGET_BOOL(" SAVE TO NEW UGC", g_bSAVE_ONLY_TO_NEW_UGC)
//			
			ADD_WIDGET_BOOL(" bGetHeader1", bGetHeader1 )
			
			ADD_WIDGET_BOOL("bSaveToCloud", bSaveToCloud )
			ADD_WIDGET_BOOL("Call DATAFILE_PUBLISH_UGC every frame", bCallDATAFILE_PUBLISH_UGC ) 
			ADD_WIDGET_INT_SLIDER("iMenuReturn ", sFMMCendStage.iMenuReturn, 0, 30, 1)
		STOP_WIDGET_GROUP()

		START_WIDGET_GROUP( "XML")
			ADD_WIDGET_BOOL("Save Xml",sCurrentVarsStruct.bSaveXml)
			ADD_WIDGET_BOOL("Load XML",bLoadXML)
			ADD_WIDGET_INT_SLIDER("Xml To Save", iXmlToSave, 0, 31, 1)
			ADD_WIDGET_INT_SLIDER("Xml To Load", iXmlToLoad, 0, 31, 1)
		STOP_WIDGET_GROUP()
		PRINTLN("CREATE_WIDGETS 16")
		//ADD_WIDGET_FLOAT_SLIDER("tfSpeedMultiplier", tfSpeedMultiplier, 0.0, 1000.0, 0.001)
		//ADD_WIDGET_FLOAT_SLIDER("tfAccelerationMultiplier", tfAccelerationMultiplier, 0.0, 1000.0, 0.001)
				
		ADD_WIDGET_STRING("Switch Statement States")
		ADD_WIDGET_INT_SLIDER("MENU STATE", sCurrentVarsStruct.iMenuState, -1, 99, 1)
		ADD_WIDGET_INT_SLIDER("sFMMCmenu.iEntityCreation,", sFMMCmenu.iEntityCreation, -1, 99, 1)
		ADD_WIDGET_INT_SLIDER("sCurrentVarsStruct.iEntityCreationStatus,", sCurrentVarsStruct.iEntityCreationStatus, -1, 99, 1)
		
		ADD_WIDGET_STRING("Entity Creation Switch Statements")
		ADD_WIDGET_INT_SLIDER("sPedStruct.iSwitchingINT", sPedStruct.iSwitchingINT, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER("sWepStruct.iSwitchingINT", sWepStruct.iSwitchingINT, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER("sVehStruct.iSwitchingINT", sVehStruct.iSwitchingINT, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER("sCapObjStruct.iSwitchingINT", sCapObjStruct.iSwitchingINT, 0, 99, 1)
		ADD_WIDGET_INT_READ_ONLY("sFMMCmenu.iVehicleLiveryOption", sFMMCmenu.iVehicleLiveryOption)
		
		SET_UP_FMMC_GLOBAL_WIDGETS()
		PRINTLN("CREATE_WIDGETS 17")
		START_WIDGET_GROUP( "Everything else")  
			ADD_WIDGET_FLOAT_SLIDER("sCurrentVarsStruct.fCheckPointSize", sCurrentVarsStruct.fCheckPointSize, 0, 30, 0.1)
			ADD_WIDGET_INT_SLIDER("sPedStruct.iCurrentRelGroupSelection[0]", sPedStruct.iCurrentRelGroupSelection[0], 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("sPedStruct.iCurrentRelGroupSelection[1]", sPedStruct.iCurrentRelGroupSelection[1], 0, 99, 1)	
			ADD_WIDGET_INT_SLIDER("sPedStruct.iCurrentRelGroupSelection[2]", sPedStruct.iCurrentRelGroupSelection[2], 0, 99, 1)	
			ADD_WIDGET_INT_SLIDER("sPedStruct.iCurrentRelGroupSelection[3]", sPedStruct.iCurrentRelGroupSelection[3], 0, 99, 1)			
			ADD_WIDGET_INT_READ_ONLY("Contact Character",g_FMMC_STRUCT.iContactCharEnum)	
			ADD_WIDGET_INT_SLIDER("sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_PEDS]", sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_PEDS], 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_WEAPONS]", sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_WEAPONS], 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_VEHICLES]", sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_VEHICLES], 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_OBJECTS]", sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_OBJECTS], 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("g_FMMC_STRUCT.iNumberOfCheckPoints", g_FMMC_STRUCT.iNumberOfCheckPoints, 0, 99, 1)
			ADD_WIDGET_FLOAT_SLIDER("g_FMMC_STRUCT.fGridLength", g_FMMC_STRUCT.fGridLength, 0, 9999, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("g_FMMC_STRUCT.fGridWidth", g_FMMC_STRUCT.fGridWidth, 0, 9999, 0.01)
		STOP_WIDGET_GROUP()
		PRINTLN("CREATE_WIDGETS 18")
		START_WIDGET_GROUP( "g_FMMC_STRUCT Check point coordinates") 
			START_WIDGET_GROUP( "vCheckPoint") 
				FOR i = 0 TO (GET_FMMC_MAX_NUM_CHECKPOINTS() - 1)
					tl15 = "vCheckPoint"
					tl15 += i
					ADD_WIDGET_VECTOR_SLIDER(tl15, g_FMMC_STRUCT.sPlacedCheckpoint[i].vCheckPoint, -9999.0, 9999.0,0.100 )
				ENDFOR
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		PRINTLN("CREATE_WIDGETS 19")
		#IF SCRIPT_PROFILER_ACTIVE
		CREATE_SCRIPT_PROFILER_WIDGET()
		#ENDIF
		PRINTLN("CREATE_WIDGETS 20")
		START_WIDGET_GROUP("Paul flag check")
			ADD_WIDGET_BOOL("Ped ignore vis check", bVisCheckFlags[0])
			ADD_WIDGET_BOOL("Veh ignore vis check", bVisCheckFlags[1])
			ADD_WIDGET_BOOL("Obj ignore vis check", bVisCheckFlags[2])
		STOP_WIDGET_GROUP()
		PRINTLN("CREATE_WIDGETS 21")
	STOP_WIDGET_GROUP()
ENDPROC	

PROC RESET_DEBUG_FROM_MISSIONTYPE_BITSET()

ENDPROC

PROC UPDATE_WIDGETS()
	// Manually set the Capture Type
		
	RESET_DEBUG_FROM_MISSIONTYPE_BITSET()
	
ENDPROC

#ENDIF

/// PURPOSE:
///    This handles opening and closing the save/publish options, as well as setting sFMMCmenu.iBitMenuItemAlert which will be used to show warning icons in the menu.

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////              Script Clean UP	                //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

PROC RESET_ALL_RULES_RESET_BLIPS()
	BLIP_INDEX				biDropOffBlip
	INT iLoop
	FOR iLoop = 0 TO (ciMAX_RULES -1)
		g_CreatorsSelDetails.biDropOffBlip[iLoop]				= biDropOffBlip
	ENDFOR
ENDPROC

PROC DELETE_BLIPS_AND_CHECKPOINTS()
	INT i, j
	FOR i = 0 TO (COUNT_OF(sWepStruct.biWeaponBlip)-1)
		IF DOES_BLIP_EXIST(sWepStruct.biWeaponBlip[i])
			REMOVE_BLIP(sWepStruct.biWeaponBlip[i])
		ENDIF
	ENDFOR
	IF DOES_BLIP_EXIST(sStartEndBlips.biStart)
       REMOVE_BLIP(sStartEndBlips.biStart)
	ENDIF
					
	IF sStartEndBlips.ciStartType != NULL
		DELETE_CHECKPOINT(sStartEndBlips.ciStartType)
		sStartEndBlips.ciStartType = NULL
	ENDIF
	
	INT iLoop
	FOR iLoop = 0 TO (ciMAX_RULES -1)
		If DOES_BLIP_EXIST(g_CreatorsSelDetails.biDropOffBlip[iLoop])
			REMOVE_BLIP(g_CreatorsSelDetails.biDropOffBlip[iLoop])
			PRINTLN("DELETE A DROP OFF BLIP!")
		ENDIF
	ENDFOR
	
	IF DOES_BLIP_EXIST(bPhotoBlip)
		REMOVE_BLIP(bPhotoBlip)
	ENDIF
	IF DOES_BLIP_EXIST(bCameraPanBlip)
		REMOVE_BLIP(bCameraPanBlip)
	ENDIF
	IF DOES_BLIP_EXIST(bCameraOutroBlip)
		REMOVE_BLIP(bCameraOutroBlip)
	ENDIF
	
	FOR i = 0 TO (ciMAX_CREATED_PEDS-1)
		IF DOES_BLIP_EXIST(sPedStruct.biPedBlip[i] )
		     REMOVE_BLIP(sPedStruct.biPedBlip[i] )
		ENDIF
	ENDFOR
	FOR i = 0 TO (ciMAX_CREATED_OBJECTS-1)
		IF DOES_BLIP_EXIST(sCapobjStruct.biObject[i] )
		     REMOVE_BLIP(sCapobjStruct.biObject[i] )
		ENDIF
	ENDFOR
	FOR i = 0 TO (ciMAX_CREATED_VEHICLES-1)
		IF DOES_BLIP_EXIST(svehStruct.biVehicleBlip[i] )
		     REMOVE_BLIP(svehStruct.biVehicleBlip[i] )
		ENDIF
	ENDFOR
	FOR i = 0 TO (ciMAX_CREATED_PROPS-1)
		IF DOES_BLIP_EXIST(spropStruct.biObject[i] )
		     REMOVE_BLIP(spropStruct.biObject[i] )
		ENDIF
	ENDFOR
	FOR i = 0 TO (FMMC_MAX_GANG_HIDE_LOCATIONS - 1)
		IF DOES_BLIP_EXIST(sLocStruct.biGoToBlips[i])
			REMOVE_BLIP(sLocStruct.biGoToBlips[i])
		ENDIF
	ENDFOR
	FOR i = 0 TO (FMMC_MAX_GANG_HIDE_LOCATIONS - 1)
		IF DOES_BLIP_EXIST(sLocStruct.biGoToBlipCaptureRadius[i])
			REMOVE_BLIP(sLocStruct.biGoToBlipCaptureRadius[i])
		ENDIF
	ENDFOR
		
	//Vehicles
	IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
		DELETE_VEHICLE(sVehStruct.viCoronaVeh)
	ENDIF
	
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		FOR j = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[i] - 1)
			IF DOES_BLIP_EXIST(sTeamSpawnStruct[i].biPedBlip[j])
				REMOVE_BLIP(sTeamSpawnStruct[i].biPedBlip[j])
			ENDIF
		ENDFOR
	ENDFOR

ENDPROC

FUNC BOOL SHOULD_DISPLAY_LIVERY_NAME()
	IF IS_ENTITY_ALIVE(sVehStruct.viCoronaVeh)	
		IF GET_VEHICLE_LIVERY_COUNT(sVehStruct.viCoronaVeh) > -1
		OR (IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, MULE) AND NOT IS_BIT_SET(sFMMCmenu.iVehBitSet, ciFMMC_VEHICLE_EXTRA1))
			IF IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, MULE)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, SANCHEZ)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, POLMAV)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, SHAMAL)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, RUMPO)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, STUNT)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, SWIFT)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, JET)
			//OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, WINDSOR)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Cleanup Mission Data
PROC SCRIPT_CLEANUP(BOOL bSetWorldActive)

	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Main_Menu")) = 0
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	IF DOES_CAM_EXIST(sCamData.cam)
		DESTROY_CAM(sCamData.cam)
	ENDIF
	IF DOES_BLIP_EXIST(bPhotoBlip)
		REMOVE_BLIP(bPhotoBlip)
	ENDIF
	IF DOES_BLIP_EXIST(bCameraPanBlip)
		REMOVE_BLIP(bCameraPanBlip)
	ENDIF
	IF DOES_BLIP_EXIST(bCameraTriggerBlip)
		REMOVE_BLIP(bCameraTriggerBlip)
	ENDIF
	IF DOES_BLIP_EXIST(bCameraOutroBlip)
		REMOVE_BLIP(bCameraOutroBlip)
	ENDIF
	
	IF DOES_BLIP_EXIST(bTeamCamPanBlip[0])
		REMOVE_BLIP(bTeamCamPanBlip[0])
	ENDIF
	IF DOES_BLIP_EXIST(bTeamCamPanBlip[1])
		REMOVE_BLIP(bTeamCamPanBlip[1])
	ENDIF
	IF DOES_BLIP_EXIST(bTeamCamPanBlip[2])
		REMOVE_BLIP(bTeamCamPanBlip[2])
	ENDIF
	IF DOES_BLIP_EXIST(bTeamCamPanBlip[3])
		REMOVE_BLIP(bTeamCamPanBlip[3])
	ENDIF
	
	
	DELETE_SWITCHING_FAKE_PED()

	
	IF IS_SELECTOR_DISABLED()
		enable_selector()
	ENDIF
	
	
	COMMON_FMMC_SCRIPT_CLEAN_UP(sPedStruct,	sVehStruct,	sWepStruct,	sCapObjStruct, sPropStruct, sDynoPropStruct, sInvisibleObjects, sCurrentVarsStruct, sFMMCmenu, TRUE, bSetWorldActive)
	SET_CREATOR_AUDIO(FALSE, FALSE)
	iDoorSetupStage = 0	
	
	DELETE_BLIPS_AND_CHECKPOINTS()
	CLEANUP_MENU_ASSETS()
	DO_BLIMP_SIGNS(sBlimpSign, TRUE)
	
	#IF IS_DEBUG_BUILD
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	#ENDIF
	INT i 
	FOR i = 0 TO (g_FMMC_STRUCT.iNumberOfSj -1)
		IF IS_BIT_SET(sCurrentVarsStruct.iStuntJumpBitSet, i)
			DELETE_STUNT_JUMP(sCurrentVarsStruct.sjsID[i])
			CLEAR_BIT(sCurrentVarsStruct.iStuntJumpBitSet, i)
		ENDIF
	ENDFOR
	DISABLE_STUNT_JUMP_SET(0)
	REMOVE_RELATIONSHIP_GROUPS(sRGH)
		
	TEXT_LABEL_23	str_hash 
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDoors - 1
		str_hash = "FMMC_DOORN"
		str_hash += ENUM_TO_INT(i)
		sCurrentVarsStruct.iMissionControlledDoors[i] = GET_HASH_KEY(str_hash)		
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(sCurrentVarsStruct.iMissionControlledDoors[i])
			DOOR_SYSTEM_SET_OPEN_RATIO(sCurrentVarsStruct.iMissionControlledDoors[i], 0,FALSE,FALSE)
			DOOR_SYSTEM_SET_DOOR_STATE(sCurrentVarsStruct.iMissionControlledDoors[i],DOORSTATE_FORCE_UNLOCKED_THIS_FRAME ,FALSE,TRUE)
			REMOVE_DOOR_FROM_SYSTEM(sCurrentVarsStruct.iMissionControlledDoors[i])
			PRINTLN("Remove the door from teh system")
		ENDIF
	ENDFOR
	CLEAR_CACHED_AVAILABLE_VEHICLE_COUNTS()
	
	//Reset mission type
	g_FMMC_STRUCT.iMissionType = 0
	g_FMMC_STRUCT.iMissionSubType = 0
	
	#IF IS_DEBUG_BUILD 
	IF IS_ROCKSTAR_DEV() 
		SET_ALLOW_TILDE_CHARACTER_FROM_ONSCREEN_KEYBOARD(FALSE)
	ENDIF
	#ENDIF
	
	PRINTLN("TERMINATE THE MISSION CREATOR. WORLD ACTIVE = ", bSetWorldActive)
	
	REQUEST_IPL("CS1_02_cf_offmission")
	
	TERMINATE_THIS_THREAD()
ENDPROC

PROC REMOVE_TEMP_CREATION_ENTITIES()
	IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
		DELETE_PED(sPedStruct.piTempPed)
	ENDIF	
	IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
		DELETE_VEHICLE(sVehStruct.viCoronaVeh)
	ENDIF
	IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
		DELETE_OBJECT(sWepStruct.viCoronaWep)
	ENDIF
	IF DOES_ENTITY_EXIST(sCapObjStruct.viCoronaObj)
		DELETE_OBJECT(sCapObjStruct.viCoronaObj)
	ENDIF
	IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
		DELETE_OBJECT(sPropStruct.viCoronaObj)
	ENDIF
	DELETE_CURRENT_PROP_TEMPLATE_OBJECTS(sPropStruct)
ENDPROC

PROC REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(INT iCurrentState)

	MODEL_NAMES oldModel
	
	IF iCurrentState != CREATION_TYPE_PEDS
		IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
			oldModel = GET_ENTITY_MODEL(sPedStruct.piTempPed)
			DELETE_PED(sPedStruct.piTempPed)
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF	
	ENDIF
	
	IF iCurrentState != CREATION_TYPE_VEHICLES
		IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
			oldModel = GET_ENTITY_MODEL(sVehStruct.viCoronaVeh)
			DELETE_VEHICLE(sVehStruct.viCoronaVeh)
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
		if DOES_ENTITY_EXIST(sVehStruct.oiCoronaCrate)			
			oldModel = GET_ENTITY_MODEL(sVehStruct.oiCoronaCrate)
			DELETE_OBJECT(sVehStruct.oiCoronaCrate)
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
	ENDIF
	IF iCurrentState !=CREATION_TYPE_WEAPONS
		IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
			oldModel = GET_ENTITY_MODEL(sWepStruct.viCoronaWep)
			DELETE_OBJECT(sWepStruct.viCoronaWep)					
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
	ENDIF
	IF iCurrentState != CREATION_TYPE_OBJECTS
		IF DOES_ENTITY_EXIST(sCapObjStruct.viCoronaObj)
			oldModel = GET_ENTITY_MODEL(sCapObjStruct.viCoronaObj)
			DELETE_OBJECT(sCapObjStruct.viCoronaObj)					
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
	ENDIF
	IF iCurrentState != CREATION_TYPE_PROPS
		IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
			oldModel = GET_ENTITY_MODEL(sPropStruct.viCoronaObj)
			DELETE_OBJECT(sPropStruct.viCoronaObj)			
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
		DELETE_CURRENT_PROP_TEMPLATE_OBJECTS(sPropStruct)
	ENDIF
	IF iCurrentState != CREATION_TYPE_DYNOPROPS
		IF DOES_ENTITY_EXIST(sDynoPropStruct.viCoronaObj)
			oldModel = GET_ENTITY_MODEL(sDynoPropStruct.viCoronaObj)
			DELETE_OBJECT(sDynoPropStruct.viCoronaObj)
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
			
			// [ChildDyno]
			IF GET_MODEL_NAME_OF_CHILD_PROP_FOR_DYNO(oldModel) != DUMMY_MODEL_FOR_SCRIPT
				IF DOES_ENTITY_EXIST(sDynoPropStruct.oiChildTemp)
					DELETE_OBJECT(sDynoPropStruct.oiChildTemp)
				ENDIF
				SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEAN_LAST_GO_TO_LOCATION()
	PRINTLN("CLEAN_LAST_GO_TO_LOCATION")
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] < FMMC_MAX_GANG_HIDE_LOCATIONS
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].vLoc[0]			= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].fRadius[0]			= 0.0
		INT iTeam
		FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
			g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].iRule[iTeam] 		= FMMC_OBJECTIVE_LOGIC_NONE
			g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].iPriority[iTeam]	= FMMC_PRIORITY_IGNORE
		ENDFOR
	ENDIF
ENDPROC

PROC TRACK_TIME_IN_CREATOR_MODE()

	IF NETWORK_CLAN_SERVICE_IS_VALID()  
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TIME_MISSION_CREATO, TIMERA())
		SETTIMERA(0)
	ENDIF
	
ENDPROC

FUNC BOOL DEAL_WITH_SKIPPING_EMPTY_OPTIONS(INT iDirection)
	
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > sFMMCmenu.iCurrentMenuLength-1
		SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
	ENDIF
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < 0
		SET_CREATOR_MENU_SELECTION(sFMMCmenu, sFMMCmenu.iCurrentMenuLength - 1)
	ENDIF

	IF SHOULD_CURRENT_SELECTION_BE_SKIPPED(sFMMCmenu)
		SKIP_EMPTY_OPTION(sFMMCmenu, iDirection)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////        	    Mantain Items       	        //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
PROC CANCEL_ENTITY_CREATION(INT iTypePassed)

	PRINTLN("CANCEL_ENTITY_CREATION iTypePassed = ", iTypePassed)
	
	SET_LONG_BIT(sFMMCMenu.iBitActive,PAN_CAM_PLACE)
	IF DOES_BLIP_EXIST(bCameraTriggerBlip)
		REMOVE_BLIP(bCameraTriggerBlip)
	ENDIF
	
	IF sFMMCMenu.iSelectedEntity = -1
	
		IF sCurrentVarsStruct.mnThingToPlaceForBudget != DUMMY_MODEL_FOR_SCRIPT
			REMOVE_MODEL_FROM_CREATOR_BUDGET(sCurrentVarsStruct.mnThingToPlaceForBudget)
			sCurrentVarsStruct.mnThingToPlaceForBudget = DUMMY_MODEL_FOR_SCRIPT
		ENDIF
		
		IF sFMMCmenu.iEntityCreation 	=  CREATION_TYPE_GOTO_LOC
			CLEAN_LAST_GO_TO_LOCATION()
			sLocStruct.iLineState = 0
			GOLF_TRAIL_SET_ENABLED(FALSE)
			GOLF_TRAIL_SET_FACING(FALSE)
			GOLF_TRAIL_SET_FIXED_CONTROL_POINT_ENABLE(FALSE)
		ENDIF
		
		INT i
		IF iTypePassed = CREATION_TYPE_VEHICLES
			UNLOAD_ALL_VEHICLE_MODELS()
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1)
				CREATE_FMMC_BLIP(sVehStruct.biVehicleBlip[i], g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos, HUD_COLOUR_BLUEDARK, "FMMC_B_10", 1)
			ENDFOR
		ELIF iTypePassed	=  CREATION_TYPE_WEAPONS
			FLOAT fSize
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons - 1)
				IF IS_SPECIAL_WEAPON_BLIP(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt)
					fSize = 1
				ELSE
					fSize = 1
				ENDIF
				CREATE_FMMC_BLIP(sWepStruct.biWeaponBlip[i], GET_ENTITY_COORDS(sWepStruct.oiWeapon[i]), HUD_COLOUR_GREEN, "", fSize)
				SET_BLIP_SPRITE(sWepStruct.biWeaponBlip[i] , GET_CORRECT_BLIP_SPRITE_FMMC(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt,sWepStruct.iSubType))
				IF NOT IS_PICKUP_TYPE_INVALID_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt)
					SET_WEAPON_BLIP_NAME_FROM_PICKUP_TYPE(sWepStruct.biWeaponBlip[i], g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt)
				ENDIF
			ENDFOR
			UNLOAD_ALL_FMMC_WEAPON_ASSETS(TRUE, sWepStruct)
		ELIF iTypePassed	=  CREATION_TYPE_PROPS
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfProps - 1)
				IF DOES_ENTITY_EXIST(sPropStruct.oiObject[i])
					CREATE_FMMC_BLIP(sPropStruct.biObject[i], GET_ENTITY_COORDS(sPropStruct.oiObject[i]), HUD_COLOUR_PURPLE, "FMMC_B_5", 1)
				ENDIF
			ENDFOR
			UNLOAD_ALL_PROP_MODELS(sFMMCmenu)
		ELIF iTypePassed	=  CREATION_TYPE_DYNOPROPS
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps - 1)
				CREATE_FMMC_BLIP(sDynoPropStruct.biObject[i], GET_ENTITY_COORDS(sDynoPropStruct.oiObject[i]), HUD_COLOUR_NET_PLAYER2, "FMMC_B_13", 1)
			ENDFOR
			UNLOAD_ALL_PROP_MODELS(sFMMCmenu)
		ELIF iTypePassed	=  CREATION_TYPE_TRIGGER
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
				CREATE_FMMC_BLIP(sStartEndBlips.biStart, g_FMMC_STRUCT.vStartPos, HUD_COLOUR_WHITE, "FMMC_B_4", 1)		
				BLIP_SPRITE blip = GET_TRIGGER_RADAR_BLIP() 
				IF blip != RADAR_TRACE_INVALID
					SET_BLIP_SPRITE(sStartEndBlips.biStart, blip)
					SET_BLIP_NAME_FROM_TEXT_FILE(sStartEndBlips.biStart, "FMMC_B_4")	
				ENDIF
			ENDIF
		ELIF iTypePassed	=  CREATION_TYPE_OBJECTS
			UNLOAD_ALL_OBJ_MODELS(sCapObjStruct)
		ELIF iTypePassed	=	CREATION_TYPE_DROP_OFF
			IF IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[sFMMCmenu.iSelectedTeam].vDropOff[0])
				CLEAR_DROP_OFF_DATA(sFMMCmenu.iSelectedTeam)
			ENDIF
		ELIF iTypePassed	=	CREATION_TYPE_RESPAWN_AREA
			FOR i = 0 TO FMMC_MAX_TEAMS-1
				CLEAR_RESPAWN_AREA_DATA(i)
			ENDFOR
		ENDIF
		sPedStruct.iSwitchingINT 						= CREATION_STAGE_WAIT
		sVehStruct.iSwitchingINT 						= CREATION_STAGE_WAIT
		sCapObjStruct.iSwitchingINT 					= CREATION_STAGE_WAIT
		sWepStruct.iSwitchingINT 						= CREATION_STAGE_WAIT
		SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
		sCurrentVarsStruct.iZoneCreationStage 										= CREATION_STAGE_WAIT
		iTriggerCreationStage							= CREATION_STAGE_WAIT
		IF sFMMCmenu.sActiveMenu != eFmmc_GO_TO_CONSEQUENCES
			PRINTLN("Setting sFMMCmenu.iEntityCreation =  -1 loc3")
			SET_CREATION_TYPE(sFMMCMenu, CREATION_TYPE_NONE)
		ENDIF
		sCurrentVarsStruct.fCheckPointSize 				= ciDEFAULT_CHECK_POINT_SIZE
		sHCS.hcCurrentCoronaColour 						= sHCS.hcDefaultCoronaColour
		
		IF sFMMCmenu.sActiveMenu != eFMMC_GENERIC_OVERRIDE_POSITION
		AND sFMMCmenu.sActiveMenu != eFmmc_GENERIC_OVERRIDE_ROTATION
		AND sFMMCmenu.sActiveMenu != eFmmc_PROP_ADVANCED_OPTIONS
		AND sFMMCmenu.sActiveMenu != eFmmc_SNAPPING_OPTIONS
		AND sFMMCmenu.sActiveMenu != eFMMC_OFFSET_POSITION
		AND sFMMCmenu.sActiveMenu != eFMMC_OFFSET_ROTATION
		AND sFMMCmenu.sActiveMenu != eFmmc_SOUND_TRIGGER_OPTIONS
		AND sFMMCmenu.sActiveMenu != eFmmc_PROP_SIGN_TRIGGER_OPTIONS
			PRINTLN("[LH][VANISH] 4")
			CLEAR_BIT(sFMMCmenu.iPropBitset, ciFMMC_PROP_Rotation_Override)
			CLEAR_BIT(sFMMCmenu.iPropBitset, ciFMMC_PROP_Position_Override)
			CLEAR_BIT(sFMMCmenu.iDynoPropBitset, ciFMMC_DYNOPROP_Position_Override)
		ENDIF
		
		IF sFMMCmenu.sActiveMenu != eFMMC_GENERIC_OVERRIDE_POSITION
		AND sFMMCmenu.sActiveMenu != eFmmc_GENERIC_OVERRIDE_ROTATION
		AND sFMMCmenu.sActiveMenu != eFmmc_PROP_ADVANCED_OPTIONS
		AND sFMMCmenu.sActiveMenu != eFmmc_SNAPPING_OPTIONS
		AND sFMMCmenu.sActiveMenu != eFMMC_OFFSET_POSITION
		AND sFMMCmenu.sActiveMenu != eFMMC_OFFSET_ROTATION
		AND sFMMCmenu.sActiveMenu != eFmmc_SOUND_TRIGGER_OPTIONS
		AND sFMMCmenu.sActiveMenu != eFmmc_PROP_SIGN_TRIGGER_OPTIONS
			PRINTLN("[JJT] sFMMCmenu.vOverridePosition changed 8")
			SET_OVERRIDE_POSITION_COORDS(sFMMCmenu, <<0,0,0>>)
			SET_OVERRIDE_ROTATION_VECTOR(sFMMCmenu, <<0,0,0>>)
		ENDIF
		
		
		REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(sFMMCmenu.iEntityCreation)
		
	ELSE
	
		PLAY_SOUND_FRONTEND(-1, "EDIT", GET_CREATOR_SPECIFIC_SOUND_SET())
		
		IF iTypePassed	= CREATION_TYPE_PEDS
			PRINTLN("TypePassed	= CREATION_TYPE_PEDS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			IF DOES_ENTITY_EXIST(sPedStruct.piPed[sFMMCMenu.iSelectedEntity])	
				DELETE_PED(sPedStruct.piPed[sFMMCMenu.iSelectedEntity])
			ENDIF
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[sFMMCMenu.iSelectedEntity] = g_FMMC_STRUCT_ENTITIES.sEditedPed	
			
			INT iTeam
			FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
				IF sPedStruct.bSavedMissionCriticalValues[iTeam]
					SET_BIT(g_FMMC_STRUCT.iMissionCriticalPedDoubleBS[iTeam][GET_LONG_BITSET_INDEX(sFMMCMenu.iSelectedEntity)], GET_LONG_BITSET_BIT(sFMMCMenu.iSelectedEntity))
				ELSE
					CLEAR_BIT(g_FMMC_STRUCT.iMissionCriticalPedDoubleBS[iTeam][GET_LONG_BITSET_INDEX(sFMMCMenu.iSelectedEntity)], GET_LONG_BITSET_BIT(sFMMCMenu.iSelectedEntity))
				ENDIF
			ENDFOR
			
			IF CREATE_PED_FMMC(sPedStruct.piPed[sFMMCMenu.iSelectedEntity], sFMMCMenu.iSelectedEntity)
				FMMC_SET_PED_VARIATION(sPedStruct.piPed[sFMMCMenu.iSelectedEntity], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sFMMCMenu.iSelectedEntity].mn, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sFMMCMenu.iSelectedEntity].iModelVariation)
				SET_ENTITY_COLLISION(sPedStruct.piPed[sFMMCMenu.iSelectedEntity] , TRUE)					
				CREATE_FMMC_BLIP(sPedStruct.biPedBlip[sFMMCMenu.iSelectedEntity], GET_ENTITY_COORDS(sPedStruct.piPed[sFMMCMenu.iSelectedEntity]), HUD_COLOUR_RED, "FMMC_B_9", 1)
				SET_BLIP_SPRITE(sPedStruct.biPedBlip[sFMMCMenu.iSelectedEntity], RADAR_TRACE_AI)
				sPedStruct.iSwitchingINT 				= CREATION_STAGE_WAIT
			ENDIF
			IF sFMMCmenu.sActiveMenu = eFmmc_MISSION_CRITICAL_MENU
				SET_UP_MISSION_CRITICAL_OPTIONS_MENU(sFMMCmenu)
			ENDIF
			SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sEditedPed.mn)
		ELIF iTypePassed	= CREATION_TYPE_TEAM_SPAWN_LOCATION
			PRINTLN("TypePassed	= CREATION_TYPE_TEAM_SPAWN_LOCATION")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam][sFMMCMenu.iSelectedEntity] = g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint
			
			PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[sFMMCMenu.iSelectedEntity].iTeam  = ", g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam)
				
			CREATE_FMMC_BLIP(sTeamSpawnStruct[g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam].biPedBlip[sFMMCMenu.iSelectedEntity], g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam][sFMMCMenu.iSelectedEntity].vPos, HUD_COLOUR_WHITE, getNameForTeamSpawnBlip(g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam), 1)
			SET_BLIP_COLOUR(sTeamSpawnStruct[g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam].biPedBlip[sFMMCMenu.iSelectedEntity] , getColourForSpawnBlip(g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam))
			
			sTeamSpawnStruct[g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam].iSwitchingINT = CREATION_STAGE_WAIT
		ELIF iTypePassed	=  CREATION_TYPE_VEHICLES		
			PRINTLN("IF iTypePassed	= CREATION_TYPE_VEHICLES")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			INT iTeam
			FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
				IF sVehStruct.bSavedMissionCriticalValues[iTeam]
					SET_BIT(g_FMMC_STRUCT.iMissionCriticalVehBS[iTeam], sFMMCMenu.iSelectedEntity)
				ELSE
					CLEAR_BIT(g_FMMC_STRUCT.iMissionCriticalVehBS[iTeam], sFMMCMenu.iSelectedEntity)
				ENDIF
			ENDFOR
			CANCEL_FROM_EDITING_VEHICLE(sVehStruct, sFMMCMenu.iSelectedEntity)
			IF sFMMCmenu.sActiveMenu = eFmmc_MISSION_CRITICAL_MENU
				SET_UP_MISSION_CRITICAL_OPTIONS_MENU(sFMMCmenu)
			ENDIF
			sVehStruct.iSwitchingINT 				= CREATION_STAGE_WAIT
		ELIF iTypePassed	=  CREATION_TYPE_WEAPONS
			PRINTLN("IF iTypePassed	= CREATION_TYPE_WEAPONS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_WEAPON(sFMMCmenu, sWepStruct, sFMMCMenu.iSelectedEntity, TRUE, sCurrentVarsStruct)
			sWepStruct.iSwitchingINT 				= CREATION_STAGE_WAIT
		ELIF iTypePassed	=  CREATION_TYPE_OBJECTS
			PRINTLN("IF iTypePassed	= CREATION_TYPE_OBJECTS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_OBJECT(sCapObjStruct, sFMMCMenu.iSelectedEntity, sCurrentVarsStruct)
			sCapObjStruct.iSwitchingINT 				= CREATION_STAGE_WAIT
		ELIF iTypePassed	=  CREATION_TYPE_PROPS
			PRINTLN("IF iTypePassed	= CREATION_TYPE_PROPS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_PROP(sPropStruct, sFMMCMenu.iSelectedEntity)
			SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
		ELIF iTypePassed	=  CREATION_TYPE_DYNOPROPS
			PRINTLN("IF iTypePassed	= CREATION_TYPE_DYNOPROPS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_DYNOPROP(sPropStruct, sDynoPropStruct, sFMMCMenu.iSelectedEntity)
			SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
		ELIF iTypePassed	=  CREATION_TYPE_GOTO_LOC
			PRINTLN("IF iTypePassed	= CREATION_TYPE_GOTO_LOC")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_GOTO(sLocStruct, sFMMCMenu.iSelectedEntity)
			IF DOES_BLIP_EXIST(sLocStruct.biGoToBlipCaptureRadius[sFMMCMenu.iSelectedEntity])
				REMOVE_BLIP(sLocStruct.biGoToBlipCaptureRadius[sFMMCMenu.iSelectedEntity])
			ENDIF
			SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
			
		ELIF iTypePassed	=  CREATION_TYPE_DROP_OFF
			PRINTLN("IF iTypePassed	= CREATION_TYPE_DROP_OFF")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_DROPOFF(sFMMCMenu.iSelectedEntity)
			SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
			
		ELIF iTypePassed	=  CREATION_TYPE_RESPAWN_AREA
			PRINTLN("IF iTypePassed	= CREATION_TYPE_DROP_OFF")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_RESPAWN_AREA(sFMMCMenu.iSelectedEntity)
			SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
		ENDIF
		SET_BIT(sCurrentVarsStruct.iUpdateRule, ciENTITY_REMOVED_CHECK_RULES)
	ENDIF	
	
ENDPROC

PROC CAP_ITEM(INT iMaxValue, INT &iToCap, BOOL bRoundUp = FALSE, INT iLowest = 0)
	IF bRoundUp
		IF iToCap >= iMaxValue
			iToCap = iMaxValue -1
			IF iToCap < iLowest
				iToCap = iLowest
			ENDIF
		ELIF iToCap < iLowest
			iToCap = iLowest
		ENDIF
	ELSE
		IF iToCap >= iMaxValue
			iToCap = iLowest
		ELIF iToCap < iLowest
			iToCap = iMaxValue-1
			IF iToCap < iLowest
				iToCap = iLowest
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///  Deals with increasing / decreasing the selection number. 
///  Number is used to set a vector vSwitchVec - This is used inside DEAL_WITH_SKY_CAM_SWITCH
///  Also sets BOOL bSwitchingCam = TRUE - This starts the switch camera action inside DEAL_WITH_SKY_CAM_SWITCH
PROC DEAL_WITH_CAMERA_SWITCH_SELECTION(BOOL bIncrease, INT iMaximumValue)
	VECTOR vTemp
	
	IF NOT IS_ROCKSTAR_DEV()
		IF SHOULD_CYCLE_ITEMS_BE_BLOCKED(sFMMCMenu)
			EXIT
		ENDIF
	ENDIF

	IF NOT HAS_NET_TIMER_STARTED(sFMMCmenu.tdCycleSwitchCooldownTimer)
		START_NET_TIMER(sFMMCmenu.tdCycleSwitchCooldownTimer)		
	ELIF HAS_NET_TIMER_STARTED(sFMMCmenu.tdCycleSwitchCooldownTimer)
		REINIT_NET_TIMER(sFMMCmenu.tdCycleSwitchCooldownTimer)
	ENDIF

	IF iMaximumValue > 0
		IF bSwitchingCam = FALSE
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			IF sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_SPAWN_POINT
				// Awful fix for cycling team spawn points. Awful.
				IF bIncrease
					PRINTLN("INCREASING eFmmc_DM_TEAM_SPAWN_POINT SELECTION")
					
					iSwitchCam ++
					IF iSwitchCam > g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iSwitchTeam]-1
						iSwitchCam = 0
						iSwitchTeam++
						IF iSwitchTeam > 3 OR iSwitchCam > g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iSwitchTeam]-1
							iSwitchTeam = 0
						ENDIF
					ENDIF
					
				ELSE
					PRINTLN("DECREASING eFmmc_DM_TEAM_SPAWN_POINT SELECTION")
					
					iSwitchCam --
					IF iSwitchCam < 0
						iSwitchCam = 0
						iSwitchTeam--
						IF iSwitchTeam < 0
							iSwitchTeam = 3
						ENDIF
						IF iSwitchCam > g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iSwitchTeam]-1
							iSwitchTeam--
							IF iSwitchTeam < 0
								iSwitchTeam = 3
							ENDIF
							PRINTLN("DECREASING eFmmc_DM_TEAM_SPAWN_POINT SELECTION iSwitchTeam 1 = ", iSwitchTeam)
						ENDIF
						IF iSwitchCam > g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iSwitchTeam]-1
							iSwitchTeam--
							IF iSwitchTeam < 0
								iSwitchTeam = 3
							ENDIF
							PRINTLN("DECREASING eFmmc_DM_TEAM_SPAWN_POINT SELECTION iSwitchTeam 2 = ", iSwitchTeam)
						ENDIF
						IF iSwitchCam > g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iSwitchTeam]-1
							iSwitchTeam--
							IF iSwitchTeam < 0
								iSwitchTeam = 3
							ENDIF
							PRINTLN("DECREASING eFmmc_DM_TEAM_SPAWN_POINT SELECTION iSwitchTeam 3 = ", iSwitchTeam)
						ENDIF
						IF iSwitchCam > g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iSwitchTeam]-1
							iSwitchTeam--
							IF iSwitchTeam < 0
								iSwitchTeam = 3
							ENDIF
							PRINTLN("DECREASING eFmmc_DM_TEAM_SPAWN_POINT SELECTION iSwitchTeam 4 = ", iSwitchTeam)
						ENDIF
						iSwitchCam = g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iSwitchTeam]-1
					ENDIF
					
				ENDIF	
			ELSE
				IF bIncrease
					PRINTLN("INCREASING SELECTION")
					iSwitchCam ++
					IF iSwitchCam > iMaximumValue - 1
						iSwitchCam = 0
					ENDIF
				ELSE
					PRINTLN("DECREASING SELECTION")
					iSwitchCam --
					IF iSwitchCam < 0
					OR iSwitchCam > iMaximumValue - 1
						iSwitchCam = iMaximumValue - 1
					ENDIF
				ENDIF
				sFMMCmenu.iSwitchCam = iSwitchCam
			ENDIF
			BOOL bValidSwitch = TRUE
			IF sFMMCmenu.sActiveMenu = eFmmc_PICKUP_BASE
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iSwitchCam].vPos
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iSwitchCam].vRot.z
			ELIF sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE
				IF iSwitchCam < g_FMMC_STRUCT_ENTITIES.iNumberOfProps
				AND iSwitchCam >= 0
					vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iSwitchCam].vPos	
					fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iSwitchCam].fHead
				ELSE
					bValidSwitch = FALSE
				ENDIF
			ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_SPAWN_POINT
				IF iSwitchCam > FMMC_MAX_TEAMSPAWNPOINTS-1
					iSwitchCam = 0
				ENDIF
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iSwitchTeam][iSwitchCam].vPos
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iSwitchTeam][iSwitchCam].fHead
			ELIF sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
				IF iSwitchCam < g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps
				AND iSwitchCam >= 0
					vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iSwitchCam].vPos	
					fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iSwitchCam].fHead
				ELSE
					bValidSwitch = FALSE
				ENDIF
			ELIF sFMMCmenu.sActiveMenu = eFmmc_VEHICLES_BASE
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iSwitchCam].vPos
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iSwitchCam].fHead	
				IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ELIF sFMMCmenu.sActiveMenu = eFmmc_ZONES_BASE
				vTemp = GET_FMMC_ZONE_PLACED_CENTRE(iSwitchCam)
				IF NOT IS_VECTOR_ZERO(vTemp)
					vSwitchVec = vTemp
				ELSE
					EXIT
				ENDIF
			ENDIF
			
			IF bValidSwitch
				bSwitchingCam = TRUE 
				PLAY_EDIT_MENU_ITEM_SOUND()
			ENDIF
		ENDIF
	ENDIF
	 
ENDPROC

PROC CHECK_FOR_PLAY_AREA_VALIDATION()
	INT iTeam, iSpawn
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		FOR iSpawn = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTeam]
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[0].vPos)
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawn].vPos)
					IF NOT IS_THIS_SPAWN_IN_THE_BOUNDS(iTeam, iSpawn, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawn].vPos)
						IF IS_DECAL_ALIVE(sTeamSpawnStruct[iTeam].diDecal[iSpawn])
							REMOVE_DECAL(sTeamSpawnStruct[iTeam].diDecal[iSpawn])
						ENDIF
						MOVE_TEAM_SPAWN_ARRAY_DOWN_1(sTeamSpawnStruct[iTeam], iSpawn, CREATION_TYPE_TEAM_SPAWN_LOCATION, iTeam)
						CLEAR_INVISIBLE_OBJECT_POSITIONS(sInvisibleObjects)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR
ENDPROC

/// PURPOSE:
///    Common check if the option item should get the toggle arrows. Not yet includes sub option
/// PARAMS:
///    iItem - selected option item
/// RETURNS:
///    
FUNC BOOL THIS_OPTIONS_ITEM_TOGGLEABLE(INT iItem)

	IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
		IF iItem = OPTION_CAPTURE_TEAMVEHICLE
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC EDIT_MENU_OPTIONS(INT iChangeValue)
	
	INT iType
	INT iMaximum
	
	PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", GET_SOUND_SET_FROM_CREATOR_TYPE())
	PRINTLN("SCROLL SOUND EFFECT!!! 1")
	sCurrentVarsStruct.bResetUpHelp = TRUE
		
	IF sFMMCmenu.sActiveMenu = eFmmc_CAPTURE_TEAM_SCORE_OPTION
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.sFMMCEndConditions[GET_CREATOR_MENU_SELECTION(sFMMCmenu)].iTargetScore[0], iChangeValue, MAX_NUMBER_CTF_TARGET_SCORE_OPTIONS+1, CLAMP_INT(GET_NUMBER_OF_TEAM_CAPTURE_OBJECTS_IN_TEAM_BASE(GET_CREATOR_MENU_SELECTION(sFMMCmenu))+1, FMMC_TARGET_SCORE_1, MAX_NUMBER_CTF_TARGET_SCORE_OPTIONS-1), TRUE, TRUE)
	ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_WANTED_OPTION
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.sFMMCEndConditions[GET_CREATOR_MENU_SELECTION(sFMMCmenu)].iWantedChange[0], iChangeValue, 7, 1, TRUE, TRUE)	
	ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_NAME_OPTION
		EDIT_CREATOR_TEAM_NAME_MENU_ITEM(sFMMCmenu, iChangeValue)
	ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_START_WEAPON_OPTION
		CHANGE_TEAM_FORCED_WEAPON(sFMMCmenu, iChangeValue)
	ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_HEALTH
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iTeamHealth[GET_CREATOR_MENU_SELECTION(sFMMCmenu)], iChangeValue, 5, 0, TRUE, TRUE)
	ELIF sFMMCmenu.sActiveMenu = eFmmc_LTS_DEV_ONLY_OPTIONS
		IF IS_ROCKSTAR_DEV()
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_DEV_ONLY_RANK
				IF iChangeValue > 0
						IF g_FMMC_STRUCT.iRank = 9900
							iChangeValue *= 99
						ELSE
							IF g_FMMC_STRUCT.iRank < 100
							
							ELIF g_FMMC_STRUCT.iRank < 1000
								iChangeValue *= 10
							ELIF g_FMMC_STRUCT.iRank < 9999
								iChangeValue *= 100
							ELSE
								iChangeValue *= 90000
							ENDIF
						ENDIF
				ELSE
					IF g_FMMC_STRUCT.iRank = 9999
						iChangeValue *= 99
					ELSE
						IF g_FMMC_STRUCT.iRank <= 100
						
						ELIF g_FMMC_STRUCT.iRank <= 1000
							iChangeValue *= 10
						ELIF g_FMMC_STRUCT.iRank <= 9999
							iChangeValue *= 100
						ELSE
							iChangeValue *= 90000
						ENDIF
					ENDIF
				ENDIF
				g_FMMC_STRUCT.iRank			  		+= iChangeValue		
				CAP_FMMC_MENU_ITEM(OPTION_MISSION_MAX_RANK, g_FMMC_STRUCT.iRank, 1)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_DEV_ONLY_EXTRA_VEHICLE_COL_DAMAGE
				EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_EnableExtraCollisionDamageForVehicles)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_DEV_ONLY_DISPLAY_VEH_HEALTH_BAR_OVERHEAD
				EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_OVERHEAD_HEALTH)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_DEV_ONLY_DISPLAY_VEH_HEALTH_BAR_OVERHEAD_LOCAL_PLAYER
				EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_ShowVehicleHealthOverheadForLocalPlayer)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_DEV_ONLY_CLEAN_UP_LEAVERS_ON_MISSION
				EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_CLEAN_UP_LEAVERS_FOR_MISSION)
			ENDIF
		ENDIF

	ELIF sFMMCmenu.sActiveMenu = eFmmc_PLAY_AREA_BOUNDS
	
		sFMMCendStage.bMajorEditOnLoadedMission = TRUE
		
		INT i = 0
		BOOL bPlaced = TRUE
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos1.x = 0
		AND g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos1.y = 0
		AND g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos2.x = 0
		AND g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos2.y = 0
			bPlaced = FALSE
		ENDIF
		
		INT iSwitch = GET_CREATOR_MENU_SELECTION(sFMMCmenu)+1
		
		SWITCH iSwitch
			CASE ciLEGACY_BOUNDS_SET_TYPE
				IF g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].iType = 0
					FOR i = 0 TO FMMC_MAX_TEAMS-1
						g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].fRadius += (iChangeValue*5)
						IF g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].fRadius <= 10
							g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].fRadius = 10
						ELIF g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].fRadius >= 1000 AND NOT IS_ROCKSTAR_DEV()
							g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].fRadius = 1000
						ENDIF
						IF DOES_BLIP_EXIST(biBounds)
							REMOVE_BLIP(biBounds)
						ENDIF
						CHECK_FOR_PLAY_AREA_VALIDATION()
					ENDFOR
				ELSE 
					FOR i = 0 TO FMMC_MAX_TEAMS-1
						g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].fWidth += (iChangeValue*5)
						IF g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].fWidth <= 10
							g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].fWidth = 10
						ELIF g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].fWidth >= 1000
							g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].fWidth = 1000
						ENDIF
					ENDFOR
				ENDIF
			BREAK
			
			CASE ciLEGACY_BOUNDS_SET_AREA_MAXHEIGHT
				IF bPlaced
					IF g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos1.z > g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos2.z
						g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos1.z += (iChangeValue*2)
						IF g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos1.z - g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos2.z < 0
							IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos2)
								g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos2.z += (iChangeValue*2)
							ELSE
								g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].fMinHeight += (iChangeValue*2)
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos2)
							g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos2.z += (iChangeValue*2)
						ELSE
							g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].fMaxHeight += (iChangeValue*2)
						ENDIF
						
						IF g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos2.z -  g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos1.z < 0
							g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos1.z += (iChangeValue*2)
						ENDIF
					ENDIF
				ENDIF
			BREAK

			CASE ciLEGACY_BOUNDS_SET_AREA_MINHEIGHT
				IF bPlaced
					IF g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].iType = 1
						FOR i = 0 TO FMMC_MAX_TEAMS-1
							IF g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].vPos1.z < g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].vPos2.z
								g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].vPos1.z += (iChangeValue*2)
								
								IF g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].vPos2.z - g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].vPos1.z < 0
									IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].vPos2)
										g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].vPos2.z += (iChangeValue*2)
									ELSE
										g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].fMaxHeight += (iChangeValue*2)
									ENDIF
								ENDIF	
							ELSE
								IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].vPos2)
									g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].vPos2.z += (iChangeValue*2)
								ELSE
									g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].fMinHeight += (iChangeValue*2)
								ENDIF
								
								IF g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].vPos1.z - g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].vPos2.z < 0
									g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].vPos1.z += (iChangeValue*2)
								ENDIF
							ENDIF
						ENDFOR								
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	
	ELIF sFMMCmenu.sActiveMenu = eFmmc_AMBIENT_MENU
	
		SWITCH GET_CREATOR_MENU_SELECTION(sFMMCmenu)
			CASE OPTION_MISSION_AM_TIME
				CASCADE_SHADOWS_ENABLE_FREEZER(FALSE) // CMcM #1791254
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iTimeOfDay, iChangeValue, 4, 0, TRUE, TRUE)
			BREAK
			CASE OPTION_MISSION_AM_WEATHER
				EDIT_CREATOR_WEATHER_MENU_ITEM(g_FMMC_STRUCT.iWeather, iChangeValue)
			BREAK
			CASE OPTION_MISSION_AM_TRAFFIC
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iTraffic, iChangeValue, OPTION_CAPTURE_MAX_PED_DENSITY, 0, TRUE, TRUE)
			BREAK
			CASE OPTION_MISSION_AM_PEDS
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iPedDensity, iChangeValue, OPTION_CAPTURE_MAX_PED_DENSITY, 0, TRUE, TRUE)
			BREAK
			CASE OPTION_MISSION_AM_SERVICES				
				EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSet, ciEMERGENCY_SERVICES_DISABLED, TRUE)
			BREAK
			CASE OPTION_MISSION_AM_MUSIC
//				CHANGE_CREATOR_MUSIC(iChangeValue)
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iAudioScore, iChangeValue, MUSIC_TYPE_NEW_ELECTRONIC+1, MUSIC_TYPE_NEW_RANDOM, TRUE, TRUE)
				/*IF iChangeValue > 0
					IF g_FMMC_STRUCT.iAudioScore > MUSIC_TYPE_NEW_ELECTRONIC
						g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_DEFAULT
					ELIF g_FMMC_STRUCT.iAudioScore > MUSIC_TYPE_RANDOM_COUNTRYSIDE AND g_FMMC_STRUCT.iAudioScore < MUSIC_TYPE_NEW_RANDOM
						g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_NEW_RANDOM
					ENDIF
				ELSE
					IF g_FMMC_STRUCT.iAudioScore < MUSIC_TYPE_DEFAULT
						g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_NEW_ELECTRONIC
					ELIF g_FMMC_STRUCT.iAudioScore > MUSIC_TYPE_RANDOM_COUNTRYSIDE AND g_FMMC_STRUCT.iAudioScore < MUSIC_TYPE_NEW_RANDOM
						g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_RANDOM_COUNTRYSIDE
					ENDIF
				ENDIF*/
			BREAK
			CASE OPTION_MISSION_AM_RADIO								
				EDIT_LOBBY_RADIO_STATION_MENU_ITEM(iChangeValue)
			BREAK
			CASE OPTION_MISSION_AM_GANGS
				EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSet, ciGANGS_DISABLED, TRUE) 
			BREAK			
			CASE OPTION_MISSION_AM_WANTED_MAX	
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iPolice, iChangeValue, OPTION_CAPTURE_MAX_POLICE, 0, TRUE, TRUE)
			BREAK
			CASE OPTION_MISSION_AM_WANTED_CLEAR
				IF g_FMMC_STRUCT.iPolice != 1
					EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSet, ciWANTED_CLEARED_ON_DEATH, TRUE)
				ENDIF
			BREAK
		ENDSWITCH
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
		IF THIS_OPTIONS_ITEM_TOGGLEABLE(GET_CREATOR_MENU_SELECTION(sFMMCmenu))
			sFMMCendStage.bMajorEditOnLoadedMission = TRUE
			EDIT_MISSION_DETAILS_MENU(sFMMCmenu, sFMMCendStage, iChangeValue > 0, GET_CREATOR_MENU_SELECTION(sFMMCmenu), sFMMCmenu.iOptionsMenuBitSet)
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
		EDIT_PHOTO_CAM_BASE_MENU(sFMMCMenu, sFMMCendStage)
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_SPAWN_POINT
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
			INT i,j
			IF sFMMCMenu.iSelectedEntity != -1
				CANCEL_ENTITY_CREATION(CREATION_TYPE_TEAM_SPAWN_LOCATION)
				sFMMCMenu.iSelectedEntity 		= -1
			ENDIF
			FOR i = 0 TO FMMC_MAX_TEAMS - 1
				sTeamSpawnStruct[i].iSwitchingINT = CREATION_STAGE_WAIT
				FOR j = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[i] - 1)
					CREATE_FMMC_BLIP(sTeamSpawnStruct[i].biPedBlip[j], g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][j].vPos, HUD_COLOUR_WHITE, getNameForTeamSpawnBlip(i), 1)
					SET_BLIP_COLOUR(sTeamSpawnStruct[i].biPedBlip[j] , getColourForSpawnBlip(i))
				ENDFOR
			ENDFOR
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSelectedTeam, iChangeValue, g_FMMC_STRUCT.iMaxNumberOfTeams)
		ELSE
			DEAL_WITH_CAMERA_SWITCH_SELECTION(iChangeValue > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0]+g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1]+g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[2]+g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[3])
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_SELECT_START_END
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1	
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSelectedTeam, sFMMCmenu.iSelectedTeam, g_FMMC_STRUCT.iMaxNumberOfTeams)
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_ZONES_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
			IF IS_ROCKSTAR_DEV()
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.sMenuZone.iType, iChangeValue, ciFMMC_ZONE_TYPE__MAX)
			ELSE
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.sMenuZone.iType, iChangeValue, 3)
			ENDIF
		ENDIF
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE
		MODEL_NAMES mnTemp = GET_CREATOR_PROP_MODEL(sFMMCMenu.iPropLibrary, sFMMCMenu.iPropType)
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_LIBRARY
			CLEAR_LONG_BITSET(sCurrentVarsStruct.iTooManyEntities)
			CHANGE_PROP_LIBRARY(sFMMCmenu, iChangeValue)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_CREATE
			CHANGE_PROP_TYPE(sFMMCmenu, iChangeValue)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_SET_INVISIBLE
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropBitSet, ciFMMC_PROP_SetInvisible)
		ENDIF
		IF NOT IS_STUNT_LIBRARY(sFMMCMenu.iPropLibrary)
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ROTATION_TYPE
				IF (NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround) OR IS_ROCKSTAR_DEV())
				AND IS_PROP_SUITABLE_FOR_ROTATION(mnTemp,sFMMCMenu.bPropSnapped)
					CHANGE_FREE_ROTATION_AXIS(sFMMCmenu, iChangeValue, sPropStruct.viCoronaObj)
					PRINTLN("[LH] CHANGE_FREE_ROTATION_AXIS")
				ENDIF
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_STACKING
				IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_STACK)
					REFRESH_MENU(sFMMCmenu)
				ENDIF
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_SNAPPING
			AND (IS_ROCKSTAR_DEV() AND sFMMCmenu.iPropLibrary = PROP_LIBRARY_TEMPLATES)
				EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_ACTIVE)
				ENSURE_A_SNAP_MODE_IS_ACTIVE(sFMMCmenu)
			ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_TEMPLATES
			AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = GET_CORRECT_CYCLE_PROPS_OPTION(sFMMCmenu)
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChangeValue > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfProps)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = GET_CORRECT_CYCLE_PROPS_OPTION(sFMMCmenu)
			AND NOT ARE_STUNT_PROPS_AND_FEATURES_ALLOWED()
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChangeValue > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfProps)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = GET_CORRECT_CYCLE_PROPS_OPTION(sFMMCmenu)
			AND ARE_STUNT_PROPS_AND_FEATURES_ALLOWED()
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChangeValue > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfProps)
			ENDIF
		ELSE
			MODEL_NAMES mnProp = GET_CREATOR_PROP_MODEL(sFMMCMenu.iPropLibrary, sFMMCMenu.iPropType)
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_COLOUR AND NOT g_sMPTunables.bDisable_Prop_Colour
				IF sFMMCMenu.iPropLibrary = PROP_LIBRARY_STUNT_NEON_TUBE
					CHANGE_PROP_TYPE(sFMMCmenu, iChangeValue*PROP_LIBRARY_STUNT_NEON_TUBE_TYPES)
					REFRESH_MENU(sFMMCmenu)
				ELSE
					INT i
					
					REPEAT MAX_COLOUR_PALETTE i
						EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropColour, iChangeValue, MAX_COLOUR_PALETTE)
						
						IF IS_PROP_COLOUR_VALID(mnProp, sFMMCmenu.iPropColour)
							BREAKLOOP
						ENDIF
					ENDREPEAT
				ENDIF
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ROTATION_TYPE
				IF (NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround) OR IS_ROCKSTAR_DEV())
				AND IS_PROP_SUITABLE_FOR_ROTATION(mnTemp,sFMMCMenu.bPropSnapped)
					CHANGE_FREE_ROTATION_AXIS(sFMMCmenu, iChangeValue, sPropStruct.viCoronaObj)
					PRINTLN("[LH] CHANGE_FREE_ROTATION_AXIS")
				ENDIF
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_STACKING
				IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_STACK)
					REFRESH_MENU(sFMMCmenu)
				ENDIF
			ELIF IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary)
			OR IS_SNAPPABLE_STUNT_PROP(mnProp)
			OR (IS_ROCKSTAR_DEV() AND sFMMCmenu.iPropLibrary = PROP_LIBRARY_TEMPLATES)
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_SNAPPING
					EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_ACTIVE)
					ENSURE_A_SNAP_MODE_IS_ACTIVE(sFMMCmenu)
				ENDIF
			ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_STUNT_SPECIAL
				IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_FLARE_01"))
				OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("ind_prop_firework_01"))
				OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("ind_prop_firework_03"))
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_PLAYERS AND NOT g_sMPTunables.bDisable_Fireworks_players_to_trigger_for
						EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iPropBitSet,  ciFMMC_PROP_TriggerForAllRacers)
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_LAPS AND NOT g_sMPTunables.bDisable_Fireworks_Laps_to_Trigger_on
						EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iPropBitSet,  ciFMMC_PROP_TriggerOnEachLap)
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_RADIUS
						EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, sFMMCmenu.fPTFXTriggerRadius, TO_FLOAT(iChangeValue*1),100.0)
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_SUB_TYPE
						EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropEffectSubType, iChangeValue, ciSTUNT_FIREWORK_VFX_MAX, -1)
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_COLOUR
						EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropColouring, iChangeValue, ciHUD_COLOUR_MAX -1)
					ENDIF
					
				ELIF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_tannoy"))
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iAlarmTypePos
						EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iAlarmSound, iChangeValue, 12)
						DEAL_WITH_CHANGING_STUNT_ALARM_PREVIEW(sFMMCmenu, TRUE)
					ENDIF	
				ENDIF
			ENDIF
			IF IS_SPEED_BOOST_PROP(mnProp)
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCmenu.iSpeedBoostPropMenuPosition
					EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSpeedBoostAmount, iChangeValue, 6, 1, TRUE)
				ENDIF		
			ENDIF
			IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown"))
			OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown_T1"))
			OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown_T2")) 
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCmenu.iSpeedBoostPropMenuPosition
					EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSpeedBoostAmount, iChangeValue, 4, 1, TRUE)
				ENDIF		
			ENDIF			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = GET_CORRECT_CYCLE_PROPS_OPTION(sFMMCmenu)
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChangeValue > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfProps)
			ENDIF
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS
		IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 3 AND IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary))   //Lock Delete full menu
		OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 2 AND NOT IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary)) //Lock Delete smaller menu
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropBitSet, ciFMMC_PROP_Lock_Delete)
		ENDIF
		
		IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 5 AND IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary))   //No collision full menu
		OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 4 AND NOT IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary)) //No collision smaller menu
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropBitSet2, ciFMMC_PROP2_NoCollision)
		ENDIF
		
	ELIF sFMMCmenu.sActiveMenu = eFMMC_GENERIC_OVERRIDE_POSITION
		SWITCH sFMMCmenu.iEntityCreation
			CASE CREATION_TYPE_PROPS
				EDIT_GENERIC_OVERRIDE_POSITION_MENU(sFMMCmenu, sFMMCendStage, CURRENTLY_USING_FMMC_OVERRIDE_POSITION(sFMMCmenu), sFMMCMenu.iPropBitset2, ciFMMC_PROP2_Clamp_X_Override_Value, ciFMMC_PROP2_Clamp_Y_Override_Value, ciFMMC_PROP2_Clamp_Z_Override_Value)
			BREAK
			DEFAULT
				INT iDummyBS
				EDIT_GENERIC_OVERRIDE_POSITION_MENU(sFMMCmenu, sFMMCendStage, CURRENTLY_USING_FMMC_OVERRIDE_POSITION(sFMMCmenu), iDummyBS)
			BREAK
		ENDSWITCH
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_GENERIC_OVERRIDE_ROTATION
		EDIT_GENERIC_OVERRIDE_ROTATION_MENU(sFMMCmenu, sFMMCendStage, CURRENTLY_USING_FMMC_OVERRIDE_ROTATION(sFMMCmenu))
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_SNAPPING_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_PROXIMITY
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_PROXIMITY)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_TRIGGERED
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_TRIGGERED)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_CHAIN
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_CHAIN)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_OVERRIDE
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_OVERRIDES)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_ANGLE
			//Sets the angle to 0 first if it's currently -1
			IF sFMMCmenu.iCustomSnapRotationAngle = -1
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iCustomSnapRotationAngle, 1, 0, 0, FALSE)
			ENDIF
			
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iCustomSnapRotationAngle, iChangeValue * ciFMMC_SnapRotationAngleIncrement, 270, 0, FALSE)
			
			//Sets the angle to -1 if it's been set to 0
			IF sFMMCmenu.iCustomSnapRotationAngle = 0
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iCustomSnapRotationAngle, -1, -1, -1, FALSE)
			ENDIF
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFMMC_OFFSET_POSITION
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_USE_OFFSET
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_USE_POSITION_OFFSET)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_FREE_CAMERA
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_USE_FREE_CAMERA)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_LOCK_POSITION
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_LOCK_POSITION)
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFMMC_OFFSET_ROTATION
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_USE_OFFSET
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_USE_ROTATION_OFFSET)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_FREE_CAMERA
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_USE_FREE_CAMERA)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_LOCK_POSITION
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_LOCK_ROTATION)
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
		MODEL_NAMES mnProp = GET_CREATOR_PROP_MODEL(sFMMCMenu.iPropLibrary, sFMMCMenu.iPropType)
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_CREATE
			CHANGE_PROP_TYPE(sFMMCmenu, iChangeValue)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_LIBRARY
			CLEAR_LONG_BITSET(sCurrentVarsStruct.iTooManyEntities)
			CHANGE_PROP_LIBRARY(sFMMCmenu, iChangeValue)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 2 AND mnProp = Prop_Container_LD_PU	
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iCratesAmmo, iChangeValue, 3, 0)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DYNOPROP_MENU_COLOUR AND DOES_PROP_HAVE_EXTRA_COLOURS(mnProp)
			INT i
			
			REPEAT MAX_COLOUR_PALETTE i
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropColour, iChangeValue, MAX_COLOUR_PALETTE)
				
				IF IS_PROP_COLOUR_VALID(mnProp, sFMMCmenu.iPropColour)
					BREAKLOOP
				ENDIF
			ENDREPEAT
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = (sFMMCMenu.iCurrentMenuLength - 1)
			DEAL_WITH_CAMERA_SWITCH_SELECTION(iChangeValue > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps)
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_TEST
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iTestMyTeam, iChangeValue, g_FMMC_STRUCT.iMaxNumberOfTeams)
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iTestNumberOfTeams, iChangeValue, g_FMMC_STRUCT.iMaxNumberOfTeams+1, 1)
		IF g_FMMC_STRUCT.iTestNumberOfTeams <= g_FMMC_STRUCT.iTestMyTeam
			g_FMMC_STRUCT.iTestNumberOfTeams = g_FMMC_STRUCT.iTestMyTeam+1
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFMMC_VEHICLE_WEAPON_MODS
		EDIT_VEHICLE_WEAPON_MOD_MENU(sFMMCendStage, sFMMCmenu, iChangeValue)
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_VEHICLES_BASE
		MODEL_NAMES vehModel = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu.iVehicleLibrary, sFMMCMenu.iVehicleType)
		//Category
		IF IS_MENU_ITEM_SELECTED("FMMC_MCAT")
			EDIT_CREATOR_VEHICLE_LIBRARY_MENU_ITEM(sFMMCMenu, sFMMCmenu.iVehicleLibrary, sFMMCMenu.iVehicleType, iChangeValue)
			RESET_CUR_VEH_MODS(sFMMCMenu)
		//Type
		ELIF IS_MENU_ITEM_SELECTED("FMMC_MTYPE")
			EDIT_CREATOR_VEHICLE_TYPE_MENU_ITEM(sFMMCMenu, sFMMCmenu.iVehicleLibrary, sFMMCMenu.iVehicleType, iChangeValue)
			CHANGE_VEHICLE_COLOUR(sFMMCMenu, sVehStruct, sFMMCMenu.iCurrentEntitySelection[CREATION_TYPE_VEHICLE_COLOUR])
			RESET_CUR_VEH_MODS(sFMMCMenu)
			
		//Colour
		ELIF IS_MENU_ITEM_SELECTED("FMMC_VEH_COL")
			IF NOT ARE_STRINGS_EQUAL(sFMMCMenu.sSubTypeName[CREATION_TYPE_VEHICLE_COLOUR], "FMMC_COL_DEF")	
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCMenu.iCurrentEntitySelection[CREATION_TYPE_VEHICLE_COLOUR], iChangeValue, ciNUMBER_OF_VEHICLE_COLOURS_PUBLIC, -1)
				CHANGE_VEHICLE_COLOUR(sFMMCMenu, sVehStruct, sFMMCMenu.iCurrentEntitySelection[CREATION_TYPE_VEHICLE_COLOUR])
			ENDIF
			
		//Livery
		ELIF IS_MENU_ITEM_SELECTED("FMMC_VEHL")
			IF IS_LIVERY_AVAILABLE(vehModel)
				IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
					CHANGE_VEHICLE_LIVERY(sFMMCMenu, vehModel, sVehStruct.viCoronaVeh,iChangeValue)
				ENDIF
			ENDIF
			
		//Vehicle Health
		ELIF IS_MENU_ITEM_SELECTED("FMMC_VEH_HP")
			IF(iChangeValue > 0 AND sFMMCmenu.iVehicleHealth >= 100)
			OR 	(iChangeValue < 0 AND sFMMCmenu.iVehicleHealth > 100)
				iChangeValue *= 25
			ELSE
				iChangeValue *= 5
			ENDIF
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iVehicleHealth, iChangeValue, 251, 25)
			
		//Bulletproof Tyres
		ELIF IS_MENU_ITEM_SELECTED("FMMC_VEH_EBPT")
		AND CAN_CURRENT_VEHICLE_HAVE_BULLETPROOF_TYRES(sFMMCMenu)
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iVehBitsetSeven,  ciFMMC_VEHICLE7_BULLETPROOF_TYRES)
			
		//Cycle Items
		ELIF IS_MENU_ITEM_SELECTED("FMMCCMENU_CYC")
			DEAL_WITH_CAMERA_SWITCH_SELECTION(iChangeValue > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles)
			
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_PICKUP_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
			iType = CREATION_TYPE_WEAPON_LIBRARY
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
			iType = CREATION_TYPE_WEAPONS
		ELIF NOT IS_ROCKSTAR_DEV()
			DEAL_WITH_CAMERA_SWITCH_SELECTION(iChangeValue > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 2
			iType = CREATION_TYPE_WEAPON_AMMO
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 3
			OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 4
			OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 5
			OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 6
			iType = CREATION_TYPE_WEAPON_REST
		ELSE
			DEAL_WITH_CAMERA_SWITCH_SELECTION(iChangeValue > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons)
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_RESTRICTION_MENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < g_FMMC_STRUCT.iMaxNumberOfTeams
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iVehBitsetTwo,  GET_CREATOR_MENU_SELECTION(sFMMCmenu)+2)
		ENDIF
	ENDIF
	
	IF iType != -1
	
		sFMMCmenu.iCurrentEntitySelection[iType] += iChangeValue
		
		IF  iType = CREATION_TYPE_WEAPONS
			
			IF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_PISTOLS
				iMaximum = FMMC_MAX_NUM_PISTOLS
				IF NOT IS_ROCKSTAR_DEV()
					iMaximum = FMMC_MAX_NUM_PISTOLS
				ENDIF
			ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_SHOTGUNS
				iMaximum = FMMC_MAX_NUM_SHOTGUNS
				IF NOT IS_ROCKSTAR_DEV()
					iMaximum = FMMC_MAX_NUM_SHOTGUNS
				ENDIF
			ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_MGS
				iMaximum = FMMC_MAX_NUM_MGS
				IF NOT IS_ROCKSTAR_DEV()
					iMaximum = FMMC_MAX_NUM_MGS
				ENDIF
			ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_RIFLES
				iMaximum = FMMC_MAX_NUM_RIFLES
				IF NOT IS_ROCKSTAR_DEV()
					iMaximum = FMMC_MAX_NUM_RIFLES
				ENDIF
			ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_SNIPERS
				iMaximum = FMMC_MAX_NUM_SNIPERS
				IF NOT IS_ROCKSTAR_DEV()
					iMaximum = FMMC_MAX_NUM_SNIPERS
				ENDIF
			ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_HEAVY
				iMaximum = FMMC_MAX_NUM_HEAVY
				IF NOT IS_ROCKSTAR_DEV()
					iMaximum = FMMC_MAX_NUM_HEAVY-1
				ENDIF
			ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_THROWN
				iMaximum = FMMC_MAX_NUM_THROWN
				IF NOT IS_ROCKSTAR_DEV()
					iMaximum = FMMC_MAX_NUM_THROWN
				ENDIF
			ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_MELEE
				iMaximum = FMMC_MAX_NUM_MELEE
				IF NOT IS_ROCKSTAR_DEV()
					iMaximum = FMMC_MAX_NUM_MELEE
				ENDIF
			ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_SPECIAL
				iMaximum = FMMC_MAX_NUM_SPECIAL - 2
			ENDIF
			IF sFMMCmenu.iCurrentEntitySelection[iType] >= iMaximum
				sFMMCmenu.iCurrentEntitySelection[iType] = 0
			ELIF sFMMCmenu.iCurrentEntitySelection[iType] < 0
				sFMMCmenu.iCurrentEntitySelection[iType] = (iMaximum-1)
			ENDIF
		ELIF  iType = CREATION_TYPE_WEAPON_LIBRARY
			IF sFMMCmenu.iCurrentEntitySelection[iType] >= FMMC_WEAPON_LIBRARY_MAX
				sFMMCmenu.iCurrentEntitySelection[iType] = 0
			ELIF sFMMCmenu.iCurrentEntitySelection[iType] < 0
				sFMMCmenu.iCurrentEntitySelection[iType] = (FMMC_WEAPON_LIBRARY_MAX-1)
			ENDIF
		ELIF iType = CREATION_TYPE_WEAPON_AMMO
			IF iChangeValue > 0
				sWepStruct.iNumberOfClips++
				IF sWepStruct.iNumberOfClips > FMMC_MAX_CLIPS
					sWepStruct.iNumberOfClips = 0
				ENDIF
			ELSE
				sWepStruct.iNumberOfClips--
				IF sWepStruct.iNumberOfClips < 0
					sWepStruct.iNumberOfClips = FMMC_MAX_CLIPS
				ENDIF
			ENDIF
			sFMMCmenu.iWepClips = sWepStruct.iNumberOfClips
		ELIF iType = CREATION_TYPE_WEAPON_REST
			INT iArrayPos = GET_CREATOR_MENU_SELECTION(sFMMCmenu)-3
			IF sWepStruct.bWepRest[iArrayPos] = TRUE
				sWepStruct.bWepRest[iArrayPos] = FALSE
			ELSE
				sWepStruct.bWepRest[iArrayPos] = TRUE
			ENDIF
			sFMMCmenu.bWepRest[iArrayPos] = sWepStruct.bWepRest[iArrayPos]		
		ELIF iType = CREATION_TYPE_ENEMY_WEAPON
			IF sFMMCmenu.iCurrentEntitySelection[iType] >= MAX_NUM_IN_ACTOR_WEAPONS
				sFMMCmenu.iCurrentEntitySelection[iType] = 0
			ELIF sFMMCmenu.iCurrentEntitySelection[iType] < 0
				sFMMCmenu.iCurrentEntitySelection[iType] = (MAX_NUM_IN_ACTOR_WEAPONS-1)
			ENDIF
		ELSE	
			IF sFMMCmenu.iCurrentEntitySelection[iType] >= MAX_REL_GROUP_TYPES
				sFMMCmenu.iCurrentEntitySelection[iType] = 0
			ELIF sFMMCmenu.iCurrentEntitySelection[iType] < 0
				sFMMCmenu.iCurrentEntitySelection[iType] = (MAX_REL_GROUP_TYPES-1)
			ENDIF
		ENDIF
		
		IF iType = CREATION_TYPE_WEAPONS
			CHANGE_WEAPON_TYPE(sFMMCmenu, sWepStruct, sFMMCmenu.iCurrentEntitySelection[iType], TRUE, iChangeValue > 0, iMaximum)
		ELIF iType = CREATION_TYPE_WEAPON_LIBRARY
			CHANGE_WEAPON_LIBRARY(sFMMCmenu, sWepStruct, sFMMCmenu.iCurrentEntitySelection[iType], TRUE)
		ENDIF
		
	ENDIF	
ENDPROC

FUNC  BOOL IS_NEAR_START_END_LOCATION(CREATION_VARS_STRUCT &sCurrentVarsStructPassed, BOOL bIsStart = TRUE, FLOAT fRadius = 10.0)
	IF bIsStart
		IF  (sCurrentVarsStructPassed.vCoronaPos.x + fRadius) >  (g_FMMC_STRUCT.sFMMCEndConditions[0].vEndPos.x)
		AND (sCurrentVarsStructPassed.vCoronaPos.y + fRadius) >  (g_FMMC_STRUCT.sFMMCEndConditions[0].vEndPos.y)
		AND (sCurrentVarsStructPassed.vCoronaPos.z + fRadius) >  (g_FMMC_STRUCT.sFMMCEndConditions[0].vEndPos.z)
		AND (sCurrentVarsStructPassed.vCoronaPos.x - fRadius) <  (g_FMMC_STRUCT.sFMMCEndConditions[0].vEndPos.x)
		AND (sCurrentVarsStructPassed.vCoronaPos.y - fRadius) <  (g_FMMC_STRUCT.sFMMCEndConditions[0].vEndPos.y)
		AND (sCurrentVarsStructPassed.vCoronaPos.z - fRadius) <  (g_FMMC_STRUCT.sFMMCEndConditions[0].vEndPos.z)
			IF NOT IS_VECTOR_ZERO(sCurrentVarsStructPassed.vCoronaPos)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF  (sCurrentVarsStructPassed.vCoronaPos.x + fRadius) >  (g_FMMC_STRUCT.vStartPos.x)
		AND (sCurrentVarsStructPassed.vCoronaPos.y + fRadius) >  (g_FMMC_STRUCT.vStartPos.y)
		AND (sCurrentVarsStructPassed.vCoronaPos.z + fRadius) >  (g_FMMC_STRUCT.vStartPos.z)
		AND (sCurrentVarsStructPassed.vCoronaPos.x - fRadius) <  (g_FMMC_STRUCT.vStartPos.x)
		AND (sCurrentVarsStructPassed.vCoronaPos.y - fRadius) <  (g_FMMC_STRUCT.vStartPos.y)
		AND (sCurrentVarsStructPassed.vCoronaPos.z - fRadius) <  (g_FMMC_STRUCT.vStartPos.z)
			IF NOT IS_VECTOR_ZERO(sCurrentVarsStructPassed.vCoronaPos)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VECTOR_POINT_IN_CORONA(VECTOR Vpassed, FLOAT fRadius = 2.3)
	IF  (sCurrentVarsStruct.vCoronaPos.x + fRadius) >  (Vpassed.x)
	AND (sCurrentVarsStruct.vCoronaPos.y + fRadius) >  (Vpassed.y)
	AND (sCurrentVarsStruct.vCoronaPos.z + fRadius) >  (Vpassed.z)
	AND (sCurrentVarsStruct.vCoronaPos.x - fRadius) <  (Vpassed.x)
	AND (sCurrentVarsStruct.vCoronaPos.y - fRadius) <  (Vpassed.y)
	AND (sCurrentVarsStruct.vCoronaPos.z - fRadius) <  (Vpassed.z)
		IF NOT IS_VECTOR_ZERO(Vpassed)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_NUM_SPAWN_POINTS_NEEDED() 
	FLOAT fNumParticipants = TO_FLOAT(g_FMMC_STRUCT.iNumParticipants)
	FLOAT fNumberOfTeams = TO_FLOAT(g_FMMC_STRUCT.iMaxNumberOfTeams)
	FLOAT fNum = fNumParticipants/fNumberOfTeams
	RETURN CEIL(fNum)
ENDFUNC

PROC SET_FAIL_BIT_SET(INT &iFailBitSet)//, BOOL bIgnoreTest = FALSE)
	IF sCurrentVarsStruct.iEntityCreationStatus != STAGE_CHECK_TO_LOAD_CREATION
	AND sCurrentVarsStruct.iEntityCreationStatus != STAGE_DEAL_WITH_LOADING
		CLEAR_BIT_ALERT(sFMMCmenu)
		IF SCRIPT_IS_CLOUD_AVAILABLE()
			IF NOT CAN_I_PUBLISH_WITH_CURRENT_PRIVILEGES()
			SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_BLOCKED)
			PRINTLN("[ALERTBITS] ciMENU_ALERT_BLOCKED set")
		ENDIF
		ENDIF
		IF IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl63MissionName)
			SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TITLE)
		ENDIF
		IF NOT g_sMPTunables.bENABLE_AC_TEAM_NAMES
		AND NOT g_sMPTunables.bENABLE_AC_WANTED_LEVELS
			CLEAR_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_LTS_TEAM_DETAILS)
		ENDIF
		IF NOT SCRIPT_IS_CLOUD_AVAILABLE()
			IF bCreatorLimitedCloudDown = FALSE
				sCurrentVarsStruct.iEntityCreationStatus = STAGE_CLOUD_FAILURE
			ENDIF
			SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
			CLEAR_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_CAPTURE_NAME)
			CLEAR_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_CAPTURE_DESCRIPTION)
			CLEAR_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_CAPTURE_TAGS)
		ELSE
			IF bCreatorLimitedCloudDown = TRUE
				sCurrentVarsStruct.iEntityCreationStatus = STAGE_CLOUD_FAILURE
			ENDIF
			CLEAR_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
			SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_CAPTURE_NAME)
			SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_CAPTURE_DESCRIPTION)
			SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_CAPTURE_TAGS)
		ENDIF
		
		CLEAR_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_ENTITIES)
		//Check to see that the mission is testable?
		INT iTeam
		BOOL bSpawnAlert = FALSE
		FOR iTeam = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams - 1)			
			//Check to see if there is a start location per team
			IF g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTeam] < iMaxSpawnsPerTeam[iTeam]
				SET_BIT(iFailBitSet, ciFAIL_BIT_SET_NO_STARTLOC_BUT_NEEDED_TEAM_1 + iTeam)
				bSpawnAlert = TRUE
			ENDIF
			
			//Check to see if the time limit if off:
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[0] = FMMC_OBJECTIVE_TIME_UNLIMITED
				SET_BIT(iFailBitSet, ciFAIL_BIT_SET_NO_TIME_SET_TEAM_1 + iTeam)
			ENDIF
			
			//Check to see if it's untested. 
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.biTeamTestComplete, TEAM_1_TEST_COMPLETE+iTeam)
				SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_UNTESTED)
			ENDIF		
		ENDFOR
		
		IF bSpawnAlert
			SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_SPAWNS)
		ELSE
			CLEAR_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_SPAWNS)
		ENDIF
				
		IF IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl63MissionName)
			SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TITLE)
		ENDIF
		
		IF IS_MISSION_DESCIPTION_EMPTY()
			SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_DESC)
		ENDIF
		
		IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
			SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
		ENDIF
		
		IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vCameraPanPos)
			SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_INTROCAM)
		ENDIF
	
		IF IS_BIT_SET(g_FMMC_STRUCT.iPhoto, FMMC_PHOTO_TAKEN)
		AND g_FMMC_STRUCT.iPhotoPath != UGC_PATH_PHOTO_NG
		AND !bNextGenPhotoTaken 
			SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_LAST_GEN_PHOTO)
		ENDIF
		
		IF NETWORK_PLAYER_IS_CHEATER()
			SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_CHEATER)
		ENDIF
		
		//no photo
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPhoto, FMMC_PHOTO_TAKEN)
			SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_PHOTO)
		ENDIF
		//No camera
		IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vCameraPanPos)
			SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_INTROCAM)
		ENDIF
		
		IF IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos1) 
		OR (g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].iType = 1
			AND IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos2) )
			SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_PLAYBOUNDS)
		ENDIF
		
//		IF g_FMMC_STRUCT.bMissionIsPublished = TRUE 
//		AND (sFMMCendStage.bMajorEditOnLoadedMission = FALSE AND !sFMMCendStage.bSaveCancelled)
//			SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CHANGE)
//		ENDIF
		
		IF NOT IS_BIT_SET_ALERT(sFMMCmenu)
		AND iFailBitSet = 0
			CLEAR_BIT(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_NOT_PLAYABLE)
		ELSE
			SET_BIT(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_NOT_PLAYABLE)
		ENDIF
		
		IF iFailBitSet = 0
		AND NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_PLAYBOUNDS)
			SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)	
		ELSE
			CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)		
		ENDIF
		
		//BC: 21/03/2014 Don't allow the player to test if there's no cloud or signed out. Probably a better place for this, feel free to move. 
		IF NETWORK_IS_SIGNED_ONLINE() = FALSE
		OR NETWORK_IS_SIGNED_IN() = FALSE
		OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
			CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)		
		ENDIF
	ENDIF
ENDPROC

PROC UNLOCK_MENU_ITEMS_AND_ALLOW_MISSION_TEST()
	iTestFailBitSet = 0
    SET_FAIL_BIT_SET(iTestFailBitSet)//, TRUE)
	PRINTLN("SET_ALL_MISSION_MENU_ITEMS_ACTIVE iFailBitSet = ", iTestFailBitSet)
    SET_ALL_MISSION_MENU_ITEMS_ACTIVE(sFMMCmenu.iBitActive)//, iFailBitSet)
ENDPROC

PROC  DEAL_WITH_SELECTING_TRIGGER_LOCATION()
	IF DO_TRIGGER_CREATION(sFMMCmenu, sCurrentVarsStruct, sFMMCdata, sStartEndBlips.biStart, sHCS.hcStartCoronaColour,sStartEndBlips.ciStartType, sFMMCendStage,iTriggerCreationStage,  3 #IF IS_DEBUG_BUILD, bDoTriggerRestricted #ENDIF)	
		// If the start positions haven't been set yet, make them the same as the start trigger.
		// Note we FALSE g_FMMC_STRUCT.sFMMCEndConditions[iTeam].bOkToDrawStartPosCorona, we only want the team starts to be drawn once the user has placed themselves.
		INT iTeam
		REPEAT FMMC_MAX_TEAMS iTeam
//			IF IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vStartPos)
//			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].bOkToDrawStartPosCorona = FALSE
				g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vStartPos = g_FMMC_STRUCT.vStartPos
//			ENDIF
		ENDREPEAT		
		UNLOCK_MENU_ITEMS_AND_ALLOW_MISSION_TEST()
	ENDIF
ENDPROC

PROC  DEAL_WITH_SELECTING_START_END_LOCATIONS()
	#IF IS_DEBUG_BUILD
		IF bDrawMenu
			EXIT
		ENDIF
	#ENDIF
	
	INT iTeam


	IF sFMMCMenu.iSelectedEntity = -1
	AND sFMMCmenu.iEntityCreation = - 1
		IF sCurrentVarsStruct.iReSelectTimer < GET_GAME_TIMER() 		
			REPEAT FMMC_MAX_TEAMS iTeam
				IF IS_VECTOR_POINT_IN_CORONA(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vStartPos)
					IF NOT ARE_VECTORS_EQUAL(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vStartPos, g_FMMC_STRUCT.vStartPos)
						SET_BIT(iLocalBitSet, biEntityInCorona)
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_CREATOR_DELETE_BUTTON())
							g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fStartHeading = 0
							g_FMMC_STRUCT.sFMMCEndConditions[iTeam].bOkToDrawStartPosCorona = FALSE
							g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vStartPos = g_FMMC_STRUCT.vStartPos
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////          Control and draw the menu           //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////	
///    
PROC SET_STARTING_MENU_ITEMS_ACTIVE()

	SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_CAPTURE_NAME)
	SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_CAPTURE_DESCRIPTION)
	SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_CAPTURE_TAGS)
	SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_CAPTURE_PHOTO)
	SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_CAPTURE_NO_PLAYERS)
	SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_CAPTURE_TEAM_DETAILS)
	SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_CAPTURE_NO_TEAMS)
	SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_CAPTURE_TIME_LIMIT)
	SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_LTS_REMOVE_HELMETS)
	
ENDPROC

PROC MENU_INITILISATION()
	PRINTLN("MENU_INITILISATION")
	sFMMCmenu.sSubTypeName[CREATION_TYPE_WEAPONS] 				= GET_CREATOR_NAME_FOR_PICKUP_TYPE(GET_PICKUP_TYPE_FROM_WEAPON_TYPE(sWepStruct.wtGunType[0]))
	sFMMCmenu.sSubTypeName[CREATION_TYPE_OBJECTS] 				= GET_NAME_CAPTURE_OBJECT_MODEL(sCapObjStruct, 0, OBJECT_LIBRARY_CAPTURE)
	sFMMCmenu.sSubTypeName[CREATION_TYPE_VEHICLE_COLOUR] 		= GET_NAME_VEHICLE_COLOUR(ciNUMBER_OF_VEHICLE_COLOURS)
	sFMMCmenu.sSubTypeName[CREATION_TYPE_PROPS]	 				= GET_CREATOR_NAME_FOR_PROP_MODEL(GET_CREATOR_PROP_MODEL(sFMMCmenu.iPropLibrary, sFMMCmenu.iPropType))
	sFMMCmenu.sSubTypeName[CREATION_TYPE_DYNOPROPS]           	= GET_CREATOR_NAME_FOR_PROP_MODEL(GET_CREATOR_PROP_MODEL(sFMMCmenu.iPropLibrary, sFMMCmenu.iPropType))
	sFMMCmenu.sSubTypeName[CREATION_TYPE_ENEMY_WEAPON] 			= GET_ACTOR_WEAPON_NAME_FROM_SELECTION(0)
	sFMMCmenu.sSubTypeName[CREATION_TYPE_ENEMY_PATROL_RANGE] 	= ""
	sFMMCmenu.sSubTypeName[CREATION_TYPE_ENEMY_RULE] 			= GET_NAME_FOR_ENTITY_RULE_TYPE(ciDEFAULT_PED_RULE, 		CREATION_TYPE_PEDS)
	sFMMCmenu.sSubTypeName[CREATION_TYPE_VEHICLE_RULE] 			= GET_NAME_FOR_ENTITY_RULE_TYPE(ciDEFAULT_VEHICLE_RULE, 	CREATION_TYPE_VEHICLES)
	sFMMCmenu.sSubTypeName[CREATION_TYPE_OBJECT_RULE] 			= GET_NAME_FOR_ENTITY_RULE_TYPE(ciDEFAULT_OBJECT_RULE, 	CREATION_TYPE_OBJECTS)
	sFMMCmenu.sSubTypeName[CREATION_TYPE_PED_PATROL_AREA] 		= GET_NAME_ENEMY_PARTOL_DISPLAY(0)

	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_OPTIONS)
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_CREATOR)
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_RADIO)
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_EXIT)
		
	INT iTeam
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		sPedStruct.iSelectedPedRule[iTeam]		= ciDEFAULT_PED_RULE
		sVehStruct.iSelectedVehicleRule[iTeam]	= ciDEFAULT_VEHICLE_RULE
		sCapObjStruct.iSelectedObjectRule[iTeam]	= ciDEFAULT_OBJECT_RULE
	ENDFOR
	
	
	SET_FAIL_BIT_SET(iTestFailBitSet)
	
	REFRESH_MENU(sFMMCMenu)
ENDPROC


FUNC BOOL IS_THIS_OPTION_SELECTABLE()

	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < 0
		SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
		PRINTLN("sFMMCmenu.iCurrentSelection CAME BACK AS -1")
		RETURN FALSE	
	ENDIF
	
//	IF sFMMCendStage.bHasValidROS = FALSE
//		RETURN FALSE
//	EL
	IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
		IF NOT IS_LONG_BIT_SET(sFMMCMenu.iBitActive, GET_CREATOR_MENU_SELECTION(sFMMCmenu))
			PRINTLN("CMcM@CAPTURE_CREATOR - IS_THIS_OPTION_SELECTABLE sFMMCmenu.iCurrentSelection = ", GET_CREATOR_MENU_SELECTION(sFMMCmenu))
			RETURN FALSE
		ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_LTS_TMSTRT
		AND NOT ARE_PLAY_AREA_BOUNDS_SET_FOR_LTS()
			RETURN FALSE
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
		IF NOT IS_BIT_SET(sFMMCmenu.iTopMenuActive, GET_CREATOR_MENU_SELECTION(sFMMCmenu))
			RETURN FALSE
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
		IF NOT IS_BIT_SET(sFMMCmenu.iOptionsMenuBitSet, GET_CREATOR_MENU_SELECTION(sFMMCmenu))
			RETURN FALSE
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DELETE_ENTITIES
		SWITCH GET_CREATOR_MENU_SELECTION(sFMMCmenu)
			CASE 1
				IF g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0] = 0
				AND g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1] = 0
				AND g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[2] = 0
				AND g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[3] = 0
					RETURN FALSE
				ENDIF
			BREAK
			CASE 2
				IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons = 0
					RETURN FALSE
				ENDIF
			BREAK
			CASE 3
				IF g_FMMC_STRUCT_ENTITIES.iNumberOfProps = 0
					RETURN FALSE
				ENDIF
			BREAK
			CASE 4
				IF g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps = 0
					RETURN FALSE
				ENDIF
			BREAK
			CASE 5
				IF g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles = 0
					RETURN FALSE
				ENDIF
			BREAK
			CASE 6
				IF g_FMMC_STRUCT_ENTITIES.iNumberOfZones = 0
					RETURN FALSE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_THIS_OPTION_A_MENU_GOTO()	
	
	IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Null_item
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_OPTION_A_MENU_ACTION()	
	
	IF sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Action_Null
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_OPTION_TOGGLEABLE()	

	IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
		IF IS_THIS_OPTION_A_MENU_ACTION()
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
	OR sFMMCmenu.sActiveMenu = eFmmc_SELECT_START_END
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC
	
/// PURPOSE:
///     This is a horrible hard coded fix for B*2214706
/// RETURNS:
///    
FUNC BOOL IS_SHOULD_THIS_OPTION_HAVE_NO_CYCLE_OPTIONS()
	
	SWITCH (sFMMCmenu.sActiveMenu)
		CASE eFmmc_MAIN_OPTIONS_BASE
			SWITCH (GET_CREATOR_MENU_SELECTION(sFMMCmenu))
				CASE OPTION_LTS_NAME
				CASE OPTION_LTS_DESCRIPTION
				CASE OPTION_LTS_PHOTO
				CASE OPTION_LTS_TAGS
					
					RETURN TRUE
			ENDSWITCH	
		BREAK 
		CASE eFmmc_TEAM_TEST
			RETURN TRUE
	ENDSWITCH 
			
	RETURN FALSE
ENDFUNC

PROC RESET_UP_HELP()	

	INT iBitSet
	REMOVE_MENU_HELP_KEYS()
	
	IF IS_BIT_SET(iLocalBitSet, biCanEditEntityInCorona)
	AND sFMMCMenu.iSelectedEntity = -1
		SET_BIT(iBitSet, FMMC_HELP_BUTTON_EDIT)
	ENDIF
	IF IS_BIT_SET(iHelpBitSet, biPickupEntityButton)
	AND sFMMCMenu.iSelectedEntity = -1
	AND sFMMCmenu.sActiveMenu != eFmmc_ZONES_BASE // Can't Edit Zones
		SET_BIT(iBitSet, FMMC_HELP_BUTTON_EDIT)
	ELSE
		IF sFMMCmenu.sActiveMenu = eFmmc_CAPTURE_OBJECTIVE_TEXT
			IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECTIVE_TEXT_PRIMARY_TEXT 			AND IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[0].tl63Objective[0]))
			OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECTIVE_TEXT_SECONDARY_TEXT 		AND IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[0].tl63Objective1[0]))
			OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECTIVE_TEXT_ALT_SECONDARY_TEXT 	AND IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[0].tl63Objective2[0]))
			OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECTIVE_TEXT_BLIP_NAME 				AND IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[0].tl23ObjBlip[0]))
			OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECTIVE_TEXT_TICKER_NAME 			AND IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[0].tl23ObjSingular[0]))
			OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECTIVE_TEXT_PLURAL_TICKER_NAME 	AND IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[0].tl23ObjPlural[0]))
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
			ELSE
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_EDIT)
			ENDIF
		ELIF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
		OR sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
		OR sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
			IF IS_THIS_OPTION_SELECTABLE()
				IF IS_THIS_OPTION_A_MENU_GOTO()
				OR IS_THIS_OPTION_A_MENU_ACTION()
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
				ENDIF
			ENDIF
		ELIF sFMMCmenu.sActiveMenu = eFmmc_DELETE_ENTITIES
		OR sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_Action_Set_cap_ent_for_team
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
		ELIF NOT IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciDRAW_CORONA)
		OR sFMMCMenu.iSelectedEntity != -1
			IF sFMMCmenu.sActiveMenu != eFmmc_RADIO_MENU
			AND sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE
			AND NOT (sFMMCmenu.sActiveMenu = eFmmc_ZONES_BASE AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
			ENDIF
		ENDIF
		
		IF (sFMMCmenu.sActiveMenu = eFmmc_AMBIENT_MENU)
			CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
		ENDIF
		
		IF (sFMMCmenu.sActiveMenu = eFmmc_ZONES_BASE AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1)
			IF g_FMMC_STRUCT_ENTITIES.iNumberOfZones > 0
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_EDIT)
			ELSE	
				CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_EDIT)
			ENDIF
		ELIF sFMMCmenu.sActiveMenu = eFmmc_PLAY_AREA_BOUNDS 
			IF g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].iType = 0
				IF IS_ROCKSTAR_DEV()
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 2
						CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_EDIT)
					ENDIF
				ELSE
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
						CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_EDIT)
					ENDIF
				ENDIF
			ELSE
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 4
					CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_EDIT)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(iBitSet, FMMC_HELP_BUTTON_SELECT)
		AND sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) != OPTION_CAPTURE_NO_PLAYERS 		
			AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) != OPTION_CAPTURE_NO_TEAMS	 		
			AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) != OPTION_CAPTURE_TIME_LIMIT			
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
		SET_BIT(iBitSet, FMMC_HELP_BUTTON_ZOOM)	
	ENDIF
	
	IF CAN_HEIGHT_BE_SET_FOR_ENTITY(sFMMCmenu, sFMMCmenu.iEntityCreation, FALSE)
	AND NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
		SET_BIT(iBitSet, FMMC_HELP_BUTTON_HEIGHT)
	ENDIF
	
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive) AND NOT IS_BIT_SET(sFMMCdata.iBitSet, biSetUpTestMission)
		IF (sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE) AND (sFMMCmenu.sActiveMenu != eFmmc_RADIO_MENU)
			IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				IF IS_BIT_SET(iHelpBitSet, biWarpToCameraButton) 
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_SWITCH_CAM)
				ENDIF
			ELSE
				IF sFMMCMenu.iSelectedEntity = -1
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_SWITCH_CAM)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iHelpBitSet, biDeleteEntityButton)
	AND CAN_DELETE_FROM_CURRENT_MENU(sFMMCMenu, sCurrentVarsStruct)
	AND sFMMCMenu.iSelectedEntity = -1
		SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)		
	ENDIF
	
	IF IS_BIT_SET(iHelpBitSet, biRemovePedsButton)
	AND sFMMCMenu.iSelectedEntity = -1
		SET_BIT(iBitSet, FMMC_HELP_BUTTON_REMOVE_PEDS)		
	ENDIF
	
	IF IS_THIS_OPTION_SELECTABLE()
	AND IS_THIS_OPTION_TOGGLEABLE()
		SET_BIT(iBitSet, FMMC_HELP_BUTTON_ADJUST)
	ENDIF
	
	IF IS_THIS_OPTION_SELECTABLE()
		AND IS_THIS_OPTION_TOGGLEABLE()
		AND NOT IS_THIS_OPTION_A_MENU_GOTO()
		AND NOT IS_SHOULD_THIS_OPTION_HAVE_NO_CYCLE_OPTIONS()
		SET_BIT(iBitSet, FMMC_HELP_BUTTON_CYCLE_OPTIONS)
	ENDIF
					
	IF sFMMCmenu.sActiveMenu = eFmmc_RADIO_MENU
		SET_BIT(iBitSet, FMMC_HELP_BUTTON_CYCLE_OPTIONS)
		SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
	ENDIF
				
	IF sFMMCmenu.iCurrentMenuLength > 1
		SET_BIT(iBitSet, FMMC_HELP_BUTTON_SCROLL)
	ENDIF		
	
	IF sFMMCmenu.sMenuBack != eFmmc_Null_item
		IF sFMMCMenu.iSelectedEntity = -1
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
		ELSE
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_CANCEL)
		ENDIF
	ENDIF
	
	IF NOT IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciDRAW_CORONA)
	OR sFMMCMenu.iSelectedEntity != -1
		IF sFMMCmenu.sActiveMenu = eFmmc_ENEMY_BASE
		OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLES_BASE
		OR sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE
		OR sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS
		OR sFMMCmenu.sActiveMenu = eFmmc_SNAPPING_OPTIONS
		OR sFMMCmenu.sActiveMenu = eFMMC_OFFSET_POSITION
		OR sFMMCmenu.sActiveMenu = eFMMC_OFFSET_ROTATION
		OR sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
		OR sFMMCmenu.sActiveMenu = eFmmc_OBJECT_BASE
		OR sFMMCmenu.sActiveMenu = eFmmc_DM_SPAWN_POINT
		OR sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_SPAWN_POINT
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_ROTATE)
		ENDIF
	ENDIF
	
	
	IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE		// I've added this clause to update PAN_CAM_BASE and PHOTO_CAM_BASE handling to match the race/deathmtach creator.
	OR sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
		iBitSet = 0
		SET_BIT(iBitSet, FMMC_HELP_BUTTON_CAMERA_HEIGHT)
		SET_BIT(iBitSet, FMMC_HELP_BUTTON_SCROLL)
		SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
		
		IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
			IF NOT DOES_BLIP_EXIST(bCameraTriggerBlip)
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
				ENDIF	
			ENDIF
		ELSE
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
			ENDIF	
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_RESET_FOV)
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_CAMERA_FOV)			
		ENDIF

	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_CAMERAS
	OR   sFMMCmenu.sActiveMenu = eFmmc_OUT_CAM_BASE		// I'm leaving the DM_TEAM_CAMERAS and OUT_CAM_BASE settings as they are, as I don't know if they should be changed to match. (SamH)
		iBitSet = 0
		SET_BIT(iBitSet, FMMC_HELP_BUTTON_CAMERA_HEIGHT)
		SET_BIT(iBitSet, FMMC_HELP_BUTTON_SCROLL)
		SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
		IF sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_Action_Preview_Pan_Cam
			IF sFMMCmenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
			ENDIF
		ELSE
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
		ENDIF
		
	ENDIF

	IF sFMMCmenu.sActiveMenu = eFmmc_TEAM_TEST
		CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
		SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)			
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < OPTION_LTS_MIN_PLAYERS
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_AMBIENT
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_TEAM_DETAILS
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
		ELSE	
			CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
		ENDIF
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_TEST_BASE
		CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
		CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_SWITCH_CAM)
		SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
	ENDIF
	
	IF NOT FMMC_IS_CIRCLE_MENU_A_CAMERA_MENU(sFMMCmenu.sActiveMenu) 
		IF NOT IS_GAMEPLAY_CAM_RENDERING()
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_ROTATE_CAM_HOLD)		
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_MOVE_CAMERA)
		ENDIF
	ENDIF
	
	IF CAN_PROP_ROTATION_BE_RESET(sFMMCmenu, sCurrentVarsStruct)
		SET_BIT(iBitSet, FMMC_HELP_BUTTON_CLEAR_ROTATION)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
		CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
	ENDIF
	
	SET_UP_PROP_HELP_BUTTONS(sFMMCmenu, iBitSet, sCurrentVarsStruct)
	
	CREATE_FMMC_INSTRUCTIONAL_BUTTONS(sFMMCmenu, iBitSet)
		
ENDPROC

FUNC BOOL IS_OBJECTIVE_TEXT_OPTION_AVAILABLE_WITH_RULE(INT iRule, INT iRow)
	
	IF iRow = ciOBJ_TEXT_PRIM
	OR iRow = ciOBJ_TEXT_TITLE
		return TRUE
	ENDIF
	
	IF iRow = ciOBJ_TEXT_SING
	OR iRow = ciOBJ_TEXT_PLURAL
	OR iRow = ciOBJ_TEXT_BLIP
		SWITCH iRule
			CASE ciSELECTION_PLAYER_RULE_KILL_ANY	
			CASE ciSELECTION_PLAYER_RULE_KILL_TEAM0
			CASE ciSELECTION_PLAYER_RULE_KILL_TEAM1
			CASE ciSELECTION_PLAYER_RULE_KILL_TEAM2
			CASE ciSELECTION_PLAYER_RULE_KILL_TEAM3
			CASE ciSELECTION_PLAYER_RULE_GET_MASKS
				RETURN FALSE
			DEFAULT
				RETURN TRUE
		ENDSWITCH
	ENDIF
	
	IF iRow = ciOBJ_TEXT_SEC
	OR iRow = ciOBJ_TEXT_TERT
		SWITCH iRule
			CASE ciSELECTION_KILL_PED
			CASE ciSELECTION_DAMAGE_PED
			CASE ciSELECTION_GO_TO_PED		
			CASE ciSELECTION_LOCATION_GO_TO		
			CASE ciSELECTION_GO_TO_VEHICLE	
			CASE ciSELECTION_KILL_VEHICLE		
			CASE ciSELECTION_PROTECT_VEHICLE
			CASE ciSELECTION_KILL_OBJECT		
			CASE ciSELECTION_PROTECT_OBJECT		
			CASE ciSELECTION_GO_TO_OBJECT
			CASE ciSELECTION_PLAYER_RULE_KILL_ANY	
			CASE ciSELECTION_PLAYER_RULE_KILL_TEAM0
			CASE ciSELECTION_PLAYER_RULE_KILL_TEAM1
			CASE ciSELECTION_PLAYER_RULE_KILL_TEAM2
			CASE ciSELECTION_PLAYER_RULE_KILL_TEAM3
			CASE ciSELECTION_PLAYER_RULE_GET_MASKS
			CASE ciSELECTION_PED_GOTO_LOCATION
				RETURN FALSE
			DEFAULT
				RETURN TRUE
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC DISPLAY_TEXT_WITH_MISSION_STRING_FMMC(FLOAT fXpos, FLOAT fYpos, STRING pTextLabel, STRING pMissionNameString, BOOL bCenter = FALSE)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
		ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(pMissionNameString)
		SET_TEXT_CENTRE(bCenter)		
		SET_TEXT_DROP_SHADOW()
	END_TEXT_COMMAND_DISPLAY_TEXT(fXpos, fYpos)	
ENDPROC

PROC CLEANUP_MAP_SWAP(SELECTION_DETAILS_STRUCT &g_CreatorsSelDetailsPassed)
	IF g_CreatorsSelDetailsPassed.bMapSwapped = TRUE 
		IF GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_SECOND
			SET_DPADDOWN_ACTIVATION_STATE(DPADDOWN_FIRST)
			g_CreatorsSelDetailsPassed.bMapSwapped = FALSE
		ENDIF
	ENDIF
ENDPROC
PROC SETUP_OUTOF_BOUNDS_BLIP(SELECTION_DETAILS_STRUCT &g_CreatorsSelDetailsPassed)
	SET_BLIP_ALPHA(g_CreatorsSelDetailsPassed.biBlipBounds, 65)
	SET_BLIP_COLOUR(g_CreatorsSelDetailsPassed.biBlipBounds, BLIP_COLOUR_RED)
	SHOW_HEIGHT_ON_BLIP(g_CreatorsSelDetailsPassed.biBlipBounds, FALSE)
ENDPROC
PROC UNCOLOUR_OBJECTIVE_TEXT(TEXT_LABEL_63 &tl63Obj)
	
	INT i
	INT iLengthOfString = GET_LENGTH_OF_LITERAL_STRING(tl63Obj)
	BOOL bWaitingForEndTilda
	TEXT_LABEL_63 tl63ReturnLabel
	STRING sChar
	
	REPEAT iLengthOfString i
		sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tl63Obj, i, i+1)
		IF NOT ARE_STRINGS_EQUAL("~", sChar)
			IF NOT bWaitingForEndTilda
				tl63ReturnLabel += sChar
			ELSE
				// Do nothing, removing everything between tildas.
			ENDIF
		ELSE
			IF NOT bWaitingForEndTilda
				bWaitingForEndTilda = TRUE
			ELSE
				bWaitingForEndTilda = FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	tl63Obj = tl63ReturnLabel
	
ENDPROC

FUNC FLOAT GET_STRING_WIDTH_MISSION_CREATOR(STRING stPassed, BOOL bSpaces = TRUE)
	BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("STRING")
		ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(stPassed)
	RETURN END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(bSpaces)
ENDFUNC

PROC SETUP_TEXT_COLOUR_TEXT()
	SET_TEXT_SCALE(0.4000, 0.4000)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	SET_TEXT_CENTRE(TRUE)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
	SET_TEXT_EDGE(0, 0, 0, 0, 0)
 ENDPROC
 
 PROC GET_TEXT_COLOUR_FROM_OBJECTIVE(INT iRule, INT &iR, INT &iG, INT &iB, INT &iA, INT iColour)
 
 	IF iRule = ciSELECTION_PLAYER_RULE_KILL_ANY		
	OR iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM0		
	OR iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM1		
	OR iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM2		
	OR iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM3	
	OR iRule = ciSELECTION_PLAYER_RULE_GET_MASKS
		GET_HUD_COLOUR(HUD_COLOUR_ORANGE, iR,iG,iB,iA)
	ELSE
		SWITCH iColour
			CASE 0	
				GET_HUD_COLOUR(HUD_COLOUR_RED, iR,iG,iB,iA)
			BREAK
			CASE 1	
				GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR,iG,iB,iA)
			BREAK
			CASE 2	
				GET_HUD_COLOUR(HUD_COLOUR_BLUE, iR,iG,iB,iA)
			BREAK
			CASE 3			
				GET_HUD_COLOUR(HUD_COLOUR_GREEN, iR,iG,iB,iA)
			BREAK
			CASE 4			
				GET_HUD_COLOUR(HUD_COLOUR_BLUEDARK, iR,iG,iB,iA)
			BREAK
		ENDSWITCH
	ENDIF
 ENDPROC
 
FUNC STRING GET_COLOUR_STRING(BOOL bSecondaryColour)

	IF bSecondaryColour
		RETURN "~b~"
	ELSE
		RETURN "~g~"	
	ENDIF

	RETURN ""
ENDFUNC

PROC UP_DATE_GLOBAL_AFTER_RULE_CHANGE(INT iType, INT iOrder)
	PRINTLN("UP_DATE_GLOBAL_AFTER_RULE_CHANGE")
	INT iEntity
	SWITCH iType
		//Peds
		CASE ciRULE_TYPE_PED
			FOR iEntity = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - 1)
				IF FMMC_IS_LONG_BIT_SET(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[iOrder].iEntityBitSet, iEntity)
					g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntity].iRule[g_CreatorsSelDetails.iSelectedTeam]		= GET_RULE_FROM_RULE_SELECTION(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[iOrder].iRule)
					g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntity].iPriority[g_CreatorsSelDetails.iSelectedTeam] 	= iOrder
				ENDIF
			ENDFOR
		BREAK
		
		//Vehicles
		CASE ciRULE_TYPE_VEHICLE	
			FOR iEntity = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1)
				IF FMMC_IS_LONG_BIT_SET(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[iOrder].iEntityBitSet, iEntity)
					g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntity].iRule[g_CreatorsSelDetails.iSelectedTeam]		= GET_RULE_FROM_RULE_SELECTION(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[iOrder].iRule)
					g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntity].iPriority[g_CreatorsSelDetails.iSelectedTeam] 	= iOrder
				ENDIF
			ENDFOR
		BREAK		
		
		//Objects
		CASE ciRULE_TYPE_OBJECT		
			FOR iEntity = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1)
				IF FMMC_IS_LONG_BIT_SET(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[iOrder].iEntityBitSet, iEntity)
					g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntity].iRule[g_CreatorsSelDetails.iSelectedTeam]		= GET_RULE_FROM_RULE_SELECTION(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[iOrder].iRule)
					g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntity].iPriority[g_CreatorsSelDetails.iSelectedTeam]	= iOrder
				ENDIF
			ENDFOR
		BREAK		
		
		//GOTO
		CASE ciRULE_TYPE_GOTO		
			FOR iEntity = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] - 1)
				IF FMMC_IS_LONG_BIT_SET(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[iOrder].iEntityBitSet, iEntity)
					g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEntity].iRule[g_CreatorsSelDetails.iSelectedTeam] 		= GET_RULE_FROM_RULE_SELECTION(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[iOrder].iRule)
					g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEntity].iPriority[g_CreatorsSelDetails.iSelectedTeam]	= iOrder
				ENDIF
			ENDFOR
		BREAK	

		//KILL PLAYER
		CASE ciRULE_TYPE_PLAYER		
		CASE ciRULE_TYPE_GET_MASKS		
			FOR iEntity = 0 TO (g_FMMC_STRUCT.iNumberOfPlayerRules[g_CreatorsSelDetails.iSelectedTeam] - 1)
				IF FMMC_IS_LONG_BIT_SET(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[iOrder].iEntityBitSet, iEntity)
					g_FMMC_STRUCT.sPlayerRuleData[iEntity].iRule[g_CreatorsSelDetails.iSelectedTeam] 		= GET_RULE_FROM_RULE_SELECTION(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[iOrder].iRule)
					g_FMMC_STRUCT.sPlayerRuleData[iEntity].iPriority[g_CreatorsSelDetails.iSelectedTeam]		= iOrder
				ENDIF
			ENDFOR
		BREAK	
	ENDSWITCH
ENDPROC


FUNC INT GET_START_CHAR_FOR_WORD(TEXT_LABEL_63 tl63, INT iWord, INT iLength)
	INT i
	INT iWordLength
	INT iWordCount = -1
	
	BOOL bIgnoreWordCount
	LANGUAGE_TYPE currentLanguage = GET_CURRENT_LANGUAGE()
	
	IF currentLanguage = LANGUAGE_CHINESE
	OR currentLanguage = LANGUAGE_JAPANESE
	OR currentLanguage = LANGUAGE_CHINESE_SIMPLIFIED
		bIgnoreWordCount = TRUE
	ENDIF
	
	FOR i = 0 TO iLength
		IF bIgnoreWordCount
			IF i > 0
				IF iWord = i
					RETURN i-1
				ENDIF
			ENDIF
		ENDIF
		IF i = iLength // Reached end of phrase.
		OR (i < iLength AND ARE_STRINGS_EQUAL(" ", GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tl63, i, i+1))) // Reached a space in the phrase
			IF iWordLength > 0
				iWordCount++
			ENDIF
			IF iWord = iWordCount
			OR i = iLength
				RETURN i - iWordLength
			ELSE
				iWordLength = 0
			ENDIF
		ELSE
			iWordLength++
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

FUNC INT GET_END_CHAR_FOR_WORD(TEXT_LABEL_63 tl63, INT iWord, INT iLength)
	INT i
	INT iWordLength
	INT iWordCount = -1
	
	BOOL bIgnoreWordCount
	LANGUAGE_TYPE currentLanguage = GET_CURRENT_LANGUAGE()
	
	IF currentLanguage = LANGUAGE_CHINESE
	OR currentLanguage = LANGUAGE_JAPANESE
	OR currentLanguage = LANGUAGE_CHINESE_SIMPLIFIED
		bIgnoreWordCount = TRUE
	ENDIF
	
	FOR i = 0 TO iLength
		IF bIgnoreWordCount
			IF i > 0
				IF iWord = i
					RETURN i
				ENDIF
			ENDIF
		ENDIF
		IF i = iLength
		OR (i < iLength AND ARE_STRINGS_EQUAL(" ", GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tl63, i, i+1)))
			IF iWordLength > 0
				iWordCount++
				iWordLength = 0
			ENDIF
			IF iWord = iWordCount
			OR i = iLength-1
				RETURN i
			ELSE
				iWordLength = 0
			ENDIF
		ELSE
			iWordLength++
		ENDIF
	ENDFOR
	IF iWordLength > 0
		RETURN iLength
	ENDIF
	RETURN -1
ENDFUNC

FUNC TEXT_LABEL_63 BUILD_COLOURED_TEXT(TEXT_LABEL_63 tlOriginal, INT iStart, INT iEnd, INT iLength, INT iSecondaryStart = -1, INT iSecondaryEnd = -1)

	TEXT_LABEL_63 tl63Return = ""
	
	INT iColour1Start = iStart
	INT iColour1End = iEnd
	
	INT iColour2Start = iSecondaryStart
	INT iColour2End = iSecondaryEnd
	
	UNCOLOUR_OBJECTIVE_TEXT(tlOriginal)
	
	IF iSecondaryStart > -1
		IF iColour1Start >= iColour2Start
			iColour1Start = iSecondaryStart
			iColour1End = iSecondaryEnd
			iColour2Start = iStart
			iColour2End = iEnd
		ENDIF
	ENDIF
	
	//Build the string
	IF iColour1Start > 0 
		tl63Return = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME_WITH_BYTE_LIMIT(tlOriginal, 0, iColour1Start, 63)
	ENDIF
	
	tl63Return += GET_COLOUR_STRING(iColour1Start = iSecondaryStart)
	tl63Return += GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME_WITH_BYTE_LIMIT(tlOriginal, iColour1Start, iColour1End, 63)
	tl63Return += "~s~"
	
	IF iSecondaryStart = -1
		tl63Return += GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME_WITH_BYTE_LIMIT(tlOriginal, iColour1End, iLength, 63)
	ELSE
		IF iColour2Start>iColour1End
			tl63Return += GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME_WITH_BYTE_LIMIT(tlOriginal, iColour1End, iColour2Start, 63)
		ENDIF
		tl63Return += GET_COLOUR_STRING(iColour1Start != iSecondaryStart)
		tl63Return += GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME_WITH_BYTE_LIMIT(tlOriginal, iColour2Start, iColour2End, 63)
		tl63Return += "~s~"
		IF iColour2End < iLength-1
			tl63Return += GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME_WITH_BYTE_LIMIT(tlOriginal, iColour2End, iLength, 63)
		ENDIF
	ENDIF
	
	PRINTLN("tl63Return = ", tl63Return)
	RETURN tl63Return
	
ENDFUNC

PROC SET_UP_RIGHT_MENU_TEXT(BOOL bSelected = FALSE, BOOL bIsSelectable = TRUE )
	SET_TEXT_SCALE(0.0000, CUSTOM_MENU_TEXT_SCALE_Y)
	IF bIsSelectable
		IF bSelected
			SET_TEXT_COLOUR(0, 0 ,0 , 255)
		ELSE
			SET_TEXT_COLOUR(255, 255, 255, 255)
		ENDIF
	ELSE	
		IF bSelected
			SET_TEXT_COLOUR(100, 100, 100, 255)
		ELSE
			SET_TEXT_COLOUR(100, 100, 100, 255)
		ENDIF
	ENDIF	
	SET_TEXT_JUSTIFICATION(FONT_LEFT)
	SET_TEXT_WRAP(0.0, 1.0)
	SET_TEXT_CENTRE(FALSE)
    SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
    SET_TEXT_EDGE(0, 0, 0, 0, 0)
ENDPROC
FUNC INT GET_MAX_OPTIONS_FOR_RULE_TYPE(INT iRuleType)
	SWITCH iRuleType
		CASE ciRULE_TYPE_PED			RETURN FMMC_MAX_SUB_OPTIONS
		CASE ciRULE_TYPE_VEHICLE		RETURN FMMC_MAX_SUB_OPTIONS
		CASE ciRULE_TYPE_OBJECT			RETURN FMMC_MAX_SUB_OPTIONS
		CASE ciRULE_TYPE_GOTO			RETURN FMMC_MAX_SUB_OPTIONS
	ENDSWITCH
	RETURN FMMC_MAX_SUB_OPTIONS
ENDFUNC

FUNC FLOAT MAX_TEXT_WIDTH()
	RETURN (cfMAIN_WIDTH_RIGHT_MENU - (0.00245 *2))
ENDFUNC

FUNC  STRING GET_SUB_SUB_MENU_OPTION_NAME(INT iRuleType, INT iRow, INT iRule)
	
	IF iRule = ciSELECTION_PLAYER_RULE_KILL_ANY
	OR iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM0
	OR iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM1
	OR iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM2
	OR iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM3
		IF iRow = FMMC_SUB_OPTIONS_SELECTED_ENTITES
			RETURN "FMMC_SS_RL_11"
		ENDIF
	ENDIF
	
	IF iRule = ciSELECTION_PLAYER_RULE_GET_MASKS
		IF iRow = FMMC_SUB_OPTIONS_SELECTED_ENTITES
			RETURN "FMMC_MASK_RL_1"
		ENDIF
	ENDIF
	
	SWITCH iRuleType
		CASE 0
		DEFAULT
			SWITCH iRow	 
				CASE FMMC_SUB_OPTIONS_RULE_TYPE				RETURN GET_RULE_STRING_FROM_SELECTION(iRule)
				CASE FMMC_SUB_OPTIONS_TIME					RETURN "FMMC_SS_RL_0"
				CASE FMMC_SUB_OPTIONS_SCORE					RETURN "FMMC_SS_RL_1"
				CASE FMMC_SUB_OPTIONS_NUM_CARRYABLE			RETURN "FMMC_SS_RL_16"
				CASE FMMC_SUB_OPTIONS_TARGET_SCORE			RETURN "FMMC_SS_RL_13"
				CASE FMMC_SUB_OPTIONS_CAPTURE_OPTIONS		RETURN "FMMC_SS_RL_2"
				CASE FMMC_SUB_OPTIONS_RESPAWN_IN_VEH		RETURN "FMMC_SS_RL_19"
				CASE FMMC_SUB_OPTIONS_FAIL_OBJECTIVE		RETURN "FMMC_SS_RL_12"
				CASE FMMC_SUB_OPTIONS_MULTIPLIER			RETURN "FMMC_SS_RL_6"
				CASE FMMC_SUB_OPTIONS_GANG_B_MENU			RETURN "FMMC_SS_RL_24"
				CASE FMMC_SUB_OPTIONS_AUTOCOMPLETE			RETURN "FMMC_SS_RL_46"
				CASE FMMC_SUB_OPTIONS_HUD					RETURN "FMMC_SS_RL_47"
				CASE FMMC_SUB_OPTIONS_MUSIC_CUES			RETURN "FMMC_SS_RL_27"
				CASE FMMC_SUB_OPTIONS_WANTED_L_MENU			RETURN "FMMC_SS_RL_48"
				CASE FMMC_SUB_OPTIONS_DEAD_PHOTO			RETURN "FMMC_SS_RL_43"
				
				CASE  FMMC_SUB_OPTIONS_DROP_OFF_POINT
					IF iRule = ciSELECTION_HACK_OBJECT
						RETURN "FMMC_SS_RL_20"
					ELSE
						RETURN "FMMC_SS_RL_8"
					ENDIF
				BREAK
				CASE FMMC_SUB_OPTIONS_DROP_OFF_CORONA		RETURN "FMMC_DRPO_CM"
				CASE FMMC_SUB_OPTIONS_DROP_OFF_GPS			RETURN "FMMCCMENU_BPG"
				CASE FMMC_SUB_OPTIONS_LEGACY_OUT_OF_BOUNDS			RETURN "FMMC_SS_RL_14"				
				CASE FMMC_SUB_OPTIONS_TEXT					RETURN "FMMC_SS_RL_35"		
				CASE FMMC_SUB_OPTIONS_HACK_SCREEN			RETURN "FMMC_SS_RL_44"
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC INT CONVERT_FMMC_PLAYER_RULE_TYPE_TO_SELECTION(INT iRule)
	SWITCH iRule
		CASE FMMC_OBJECTIVE_LOGIC_KILL_PLAYERS		RETURN	 ciSELECTION_PLAYER_RULE_KILL_ANY  	
		CASE FMMC_OBJECTIVE_LOGIC_KILL_TEAM0  		RETURN	 ciSELECTION_PLAYER_RULE_KILL_TEAM0	
		CASE FMMC_OBJECTIVE_LOGIC_KILL_TEAM1  		RETURN	 ciSELECTION_PLAYER_RULE_KILL_TEAM1	
		CASE FMMC_OBJECTIVE_LOGIC_KILL_TEAM2  		RETURN	 ciSELECTION_PLAYER_RULE_KILL_TEAM2	
		CASE FMMC_OBJECTIVE_LOGIC_KILL_TEAM3  		RETURN	 ciSELECTION_PLAYER_RULE_KILL_TEAM3	
		CASE FMMC_OBJECTIVE_LOGIC_GET_MASKS  		RETURN	 ciSELECTION_PLAYER_RULE_GET_MASKS
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM0 		RETURN	 ciSELECTION_PLAYER_RULE_GO_TO_TEAM0	
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM1 		RETURN	 ciSELECTION_PLAYER_RULE_GO_TO_TEAM1	
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM2 		RETURN	 ciSELECTION_PLAYER_RULE_GO_TO_TEAM2
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM3 		RETURN	 ciSELECTION_PLAYER_RULE_GO_TO_TEAM3
		CASE FMMC_OBJECTIVE_LOGIC_LOOT_THRESHOLD	RETURN	 ciSELECTION_PLAYER_RULE_LOOT_THRESHOLD
		CASE FMMC_OBJECTIVE_LOGIC_POINTS_THRESHOLD	RETURN	 ciSELECTION_PLAYER_RULE_POINTS_THRESHOLD
		CASE FMMC_OBJECTIVE_LOGIC_HOLDING_RULE		RETURN	 ciSELECTION_PLAYER_RULE_HOLDING_RULE
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC  BOOL DOES_ENTITY_HAVE_THIS_RULE_WITH_THIS_PRIORITY(INT iTeam, INT iPriority)
	INT iCount	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfPeds > 0
		FOR iCount = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - 1)
			IF iPriority = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCount].iPriority[iTeam]
				PRINTLN("g_CreatorsSelDetails.sTeamOptions[", iTeam, "].sSelection[", iPriority, "].iRule = ", g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPriority].iRule)
				PRINTLN("GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[", iCount, "].iRule[", iTeam, "], CREATION_TYPE_PEDS) = ", GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCount].iRule[iTeam], CREATION_TYPE_PEDS))
				IF g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPriority].iRule = GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCount].iRule[iTeam], CREATION_TYPE_PEDS)
					PRINTLN("DOES_ENTITY_HAVE_THIS_RULE_WITH_THIS_PRIORITY PEDS TRUE")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles > 0
		FOR iCount = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1)
			IF iPriority = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCount].iPriority[iTeam]
				IF g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPriority].iRule = GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCount].iRule[iTeam], CREATION_TYPE_VEHICLES)
					PRINTLN("DOES_ENTITY_HAVE_THIS_RULE_WITH_THIS_PRIORITY VEHICLES TRUE")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfObjects > 0
		FOR iCount = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1)
			IF iPriority = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCount].iPriority[iTeam]
				IF g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPriority].iRule = GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCount].iRule[iTeam], CREATION_TYPE_OBJECTS)
					PRINTLN("DOES_ENTITY_HAVE_THIS_RULE_WITH_THIS_PRIORITY OBJECTS TRUE")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] > 0
		PRINTLN("g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] = ", g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0])
		FOR iCount = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] - 1)
			PRINTLN("iCount = ", iCount)
			PRINTLN("iPriority = ", iPriority)
			PRINTLN("g_FMMC_STRUCT_ENTITIES.sGotoLocationData[", iCount, "].iPriority[",iTeam,"] = ",  g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iCount].iPriority[iTeam])
			IF iPriority = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iCount].iPriority[iTeam]
				PRINTLN("g_CreatorsSelDetails.sTeamOptions[", iTeam, "].sSelection[", iPriority, "].iRule = ", g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPriority].iRule)
				PRINTLN("g_FMMC_STRUCT_ENTITIES.sGotoLocationData[", iCount, "].iRule[", iTeam, "] = ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iCount].iRule[iTeam])
				PRINTLN("GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[", iCount, "].iRule[", iTeam, "], CREATION_TYPE_GOTO_LOC) = ", GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iCount].iRule[iTeam], CREATION_TYPE_GOTO_LOC))
				IF g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPriority].iRule = GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iCount].iRule[iTeam], CREATION_TYPE_GOTO_LOC)
					PRINTLN("DOES_ENTITY_HAVE_THIS_RULE_WITH_THIS_PRIORITY GO TO LOCATION TRUE")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	PRINTLN("g_CreatorsSelDetails.sTeamOptions[",iTeam, "].sSelection[", iPriority, "].iRule = ", g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPriority].iRule)
	PRINTLN("iPriority = ", iPriority)
	IF g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam] > 0
		FOR iCount = 0 TO (g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam] - 1)
			PRINTLN("g_FMMC_STRUCT.sPlayerRuleData[", iCount, "].iPriority[", iTeam, "] = ", g_FMMC_STRUCT.sPlayerRuleData[iCount].iPriority[iTeam])
			IF iPriority = g_FMMC_STRUCT.sPlayerRuleData[iCount].iPriority[iTeam]
				PRINTLN("CONVERT_FMMC_PLAYER_RULE_TYPE_TO_SELECTION(g_FMMC_STRUCT.sPlayerRuleData[", iPriority, "].iRule[", iTeam, "]) = ", CONVERT_FMMC_PLAYER_RULE_TYPE_TO_SELECTION(g_FMMC_STRUCT.sPlayerRuleData[iCount].iRule[iTeam]))
				IF g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPriority].iRule = CONVERT_FMMC_PLAYER_RULE_TYPE_TO_SELECTION(g_FMMC_STRUCT.sPlayerRuleData[iCount].iRule[iTeam])
					PRINTLN("DOES_ENTITY_HAVE_THIS_RULE_WITH_THIS_PRIORITY PLAYER KILL RULE TRUE")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	PRINTLN("DOES_ENTITY_HAVE_THIS_RULE_WITH_THIS_PRIORITY FALSE")
	RETURN FALSE
ENDFUNC
PROC MOVE_PED_RULES_DOWN_ONE(INT iPriority, INT iTeam)
	INT iCount
	FOR iCount = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - 1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCount].iPriority[iTeam] > iPriority
		AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCount].iPriority[iTeam] != FMMC_PRIORITY_IGNORE
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCount].iPriority[iTeam]--
			PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedPed[", iCount, "].iPriority[", iTeam, "] = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCount].iPriority[iTeam])
		ENDIF
	ENDFOR
ENDPROC
PROC MOVE_VEHICLE_RULES_DOWN_ONE(INT iPriority, INT iTeam)
	INT iCount
	FOR iCount = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCount].iPriority[iTeam] > iPriority
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCount].iPriority[iTeam] != FMMC_PRIORITY_IGNORE
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCount].iPriority[iTeam]--
			PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[", iCount, "].iPriority[", iTeam, "] = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCount].iPriority[iTeam])
		ENDIF
	ENDFOR
ENDPROC
PROC MOVE_OBJECT_RULES_DOWN_ONE(INT iPriority, INT iTeam)
	INT iCount
	FOR iCount = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCount].iPriority[iTeam] > iPriority
		AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCount].iPriority[iTeam] != FMMC_PRIORITY_IGNORE
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCount].iPriority[iTeam]--
			PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedObject[", iCount, "].iPriority[", iTeam, "] = ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCount].iPriority[iTeam])
		ENDIF
	ENDFOR
ENDPROC
PROC MOVE_GOTO_RULES_DOWN_ONE(INT iPriority, INT iTeam)
	INT iCount
	FOR iCount = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] - 1)
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iCount].iPriority[iTeam] > iPriority
		AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iCount].iPriority[iTeam] != FMMC_PRIORITY_IGNORE
			g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iCount].iPriority[iTeam]--
			PRINTLN("g_FMMC_STRUCT_ENTITIES.sGotoLocationData[", iCount, "].iPriority[", iTeam, "] = ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iCount].iPriority[iTeam])
		ENDIF
	ENDFOR
ENDPROC

PROC MOVE_PLAYER_RULES_DOWN_ONE(INT iPriority, INT iTeam)
	INT iCount
	FOR iCount = 0 TO (g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam] - 1)
		IF g_FMMC_STRUCT.sPlayerRuleData[iCount].iPriority[iTeam] > iPriority
		AND g_FMMC_STRUCT.sPlayerRuleData[iCount].iPriority[iTeam] != FMMC_PRIORITY_IGNORE
			g_FMMC_STRUCT.sPlayerRuleData[iCount].iPriority[iTeam]--
			PRINTLN("g_FMMC_STRUCT.sPlayerRuleData[", iCount, "].iPriority[", iTeam, "] = ", g_FMMC_STRUCT.sPlayerRuleData[iCount].iPriority[iTeam])
		ENDIF
	ENDFOR
ENDPROC

PROC MOVE_OTHER_RULES_OF_THIS_TYPE_DOWN_ONE(SELECTION_DETAILS_STRUCT &g_CreatorsSelDetailsPassed, INT iRow, INT iTeam)
	SWITCH GET_RULE_TYPE_FROM_RULE(g_CreatorsSelDetailsPassed.sTeamOptions[iTeam].sSelection[iRow].iRule)
		CASE ciRULE_TYPE_PED
			MOVE_PED_RULES_DOWN_ONE(iRow, iTeam)
		BREAK
		CASE ciRULE_TYPE_VEHICLE
			MOVE_VEHICLE_RULES_DOWN_ONE(iRow, iTeam)
		BREAK
		CASE ciRULE_TYPE_OBJECT
			MOVE_OBJECT_RULES_DOWN_ONE(iRow, iTeam)
		BREAK
		CASE ciRULE_TYPE_GOTO
			MOVE_GOTO_RULES_DOWN_ONE(iRow, iTeam)
		BREAK
		DEFAULT
			PRINTLN("MOVE_OTHER_RULES_OF_THIS_TYPE_DOWN_ONE - default")
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    General way to set up a angled area bounds from teh rules menu
/// PARAMS:
///    vPos1 - 
///    vPos2 -
///    fWidth - 
///    fMinHeight - 
///    fMaxHeight - 
///    iAreaType - Bounds/Sperical or Leave/Enter. Whatever type is needed.
///    bDoingAreaStuff - The Bool that says if you are setting the angled area
///    iMenuOption - The menu item that is highlighted
///    iMaxTypes - The number you can scroll through in the area type.
PROC GET_ANGLED_AREA_FOR_RULES_MENU(VECTOR &vPos1, VECTOR &vPos2, FLOAT &fWidth, FLOAT &fMinHeight, FLOAT &fMaxHeight, INT &iAreaType, BOOL &bDoingAreaStuff, INT iMenuOption, INT iMaxTypes, FLOAT fWidthIncrease, INT &iBitset, INT iValue)

		VECTOR tempVec1
		VECTOR tempVec2
		
		IF NOT IS_VECTOR_ZERO(vPos1)
			tempVec1 = vPos1
			if NOT IS_VECTOR_ZERO(vPos2)
				tempVec2 = vPos2
			ELSE
				tempVec2 = sCurrentVarsStruct.vCoronaPos
				if tempVec1.z > tempVec2.z
					tempVec2.z += fMinHeight 
				ELSE	
					tempVec2.z += fMaxHeight		
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD
				DRAW_DEBUG_ANGLED_AREA(tempVec1, tempVec2, fWidth, 255,0,0,120)
			#ENDIF
		ENDIF
	
		IF fWidth < 2
			fWidth = 2.0
		ENDIF
		
		IF NOT IS_VECTOR_ZERO(vPos1)	
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
			OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
				IF iMenuOption = ciGANGAREA_WIDTH	
					fWidth -= fWidthIncrease
					IF fWidth < 2.0
						fWidth = 2.0
					ENDIF
				ELIF iMenuOption = ciGANGAREA_MAXHEIGHT
				
					if tempVec1.z > tempVec2.z
						vPos1.z -= 1.0
						
						IF tempVec1.z - tempVec2.z < 3
							IF NOT IS_VECTOR_ZERO(vPos2)
								vPos2.z -= 1.0
							ELSE
								fMinHeight -= 1.0
							ENDIF
						ENDIF
						
					ELSE
						IF NOT IS_VECTOR_ZERO(vPos2)
							vPos2.z -= 1.0
						ELSE
							fMaxHeight -= 1.0
						ENDIF
						
						IF tempVec2.z -  tempVec1.z < 3
							vPos1.z -= 1.0
						ENDIF
					ENDIF
					
				ELIF iMenuOption = ciGANGAREA_MINHEIGHT
				
					if tempVec1.z < tempVec2.z
						vPos1.z -= 1.0
					ELSE
						IF NOT IS_VECTOR_ZERO(vPos2)
							vPos2.z -= 1.0
						ELSE
							fMinHeight -= 1.0
						ENDIF
					ENDIF				
				ENDIF	
			ENDIF

			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
			OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
				IF iMenuOption = ciGANGAREA_WIDTH	
					fWidth += fWidthIncrease
					IF fWidth > 1500.0
						fWidth = 1500.0
					ENDIF
				ELIF iMenuOption = ciGANGAREA_MAXHEIGHT
					
					if tempVec1.z > tempVec2.z
						vPos1.z += 1.0
					ELSE
						IF NOT IS_VECTOR_ZERO(vPos2)
							vPos2.z += 1.0
						ELSE
							fMaxHeight += 1.0
						ENDIF
					ENDIF
				ELIF iMenuOption = ciGANGAREA_MINHEIGHT
					
					if tempVec1.z < tempVec2.z
						vPos1.z += 1.0
						
						IF tempVec2.z - tempVec1.z < 3
							IF NOT IS_VECTOR_ZERO(vPos2)
								vPos2.z += 1.0
							ELSE
								fMaxHeight += 1.0
							ENDIF
						ENDIF
						
					ELSE
						IF NOT IS_VECTOR_ZERO(vPos2)
							vPos2.z += 1.0
						ELSE
							fMinHeight += 1.0
						ENDIF
						
						IF tempVec1.z - tempVec2.z < 3
							vPos1.z += 1.0
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF iMenuOption = ciGANGAREA_TYPE		
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)			
				iAreaType ++ 
				CAP_FMMC_MENU_ITEM(iMaxTypes, iAreaType)
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)			
				iAreaType -- 
				CAP_FMMC_MENU_ITEM(iMaxTypes, iAreaType)
			ENDIF
		ENDIF
			
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR FMMC_IS_MAP_ACCEPT_JUST_PRESSED(sFMMCmenu)
			IF iMenuOption = ciGANGAREA_CLEAR
				vPos1 = <<0,0,0>>
				vPos2 = <<0,0,0>>
				fWidth = 5
				iAreaType = 0
				fMaxHeight = 2
				fMinHeight = -1
			ELSE
				IF iAreaType > 0
					IF IS_VECTOR_ZERO(vPos1)
						vPos1 	= <<sCurrentVarsStruct.vCoronaPos.x, sCurrentVarsStruct.vCoronaPos.y, sCurrentVarsStruct.vCoronaPos.z + 2>>
						PLAY_SOUND_FRONTEND(-1, "SELECT_LOCATION", GET_CREATOR_SPECIFIC_SOUND_SET())
						PRINTSTRING("SELECT_LOCATION SOUND EFFECT!!!")PRINTNL()	
					ELIF IS_VECTOR_ZERO(vPos2)	
						vPos1 = tempVec1
						vPos2 = tempVec2
						PLAY_SOUND_FRONTEND(-1, "SELECT_LOCATION", GET_CREATOR_SPECIFIC_SOUND_SET())
						PRINTSTRING("SELECT_LOCATION SOUND EFFECT!!!")PRINTNL()	
					ENDIF
					CLEAR_BIT(iBitset, iValue)
					sCurrentVarsStruct.fCheckPointSize = ciDEFAULT_CHECK_POINT_SIZE
					sHCS.hcCurrentCoronaColour = sHCS.hcDefaultCoronaColour
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			if IS_VECTOR_ZERO(vPos2)
				vPos1 = <<0,0,0>>
				fMaxHeight = 2
				fMinHeight = -1
				fWidth = 2	
				iAreaType = 0
			ENDIF
			sHCS.hcCurrentCoronaColour = sHCS.hcDefaultCoronaColour
			bDoingAreaStuff = FALSE
		ENDIF
ENDPROC

/// PURPOSE:
///    General way to set up a sphere area bounds from teh rules menu
/// PARAMS:
///    vPos1 - 
///    vPos2 -
///    fWidth - 
///    fMinHeight - 
///    fMaxHeight - 
///    iAreaType - Bounds/Sperical or Leave/Enter. Whatever type is needed.
///    bDoingAreaStuff - The Bool that says if you are setting the angled area
///    iMenuOption - The menu item that is highlighted
///    iMaxTypes - The number you can scroll through in the area type.
PROC GET_SPHERE_AREA_FOR_RULES_MENU(VECTOR vCoronaPos, VECTOR &vPos1, VECTOR &vPos2, FLOAT &fWidth, FLOAT &fMinHeight, FLOAT &fMaxHeight, INT &iAreaType, BOOL &bDoingAreaStuff, INT iMenuOption, INT iMaxTypes, FLOAT fWidthIncrease, INT &iBitset, INT iValue)

	VECTOR tempVec1		
	
	IF fWidth < 2
		fWidth = 2.0
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
		IF iMenuOption = ciGANGAREA_WIDTH	
			fWidth -= fWidthIncrease
			IF fWidth < 2.0
				fWidth = 2.0
			ENDIF
		ELIF iMenuOption = ciGANGAREA_MAXHEIGHT
			IF IS_VECTOR_ZERO(vPos1)
				fMaxHeight -= 1.0
			ENDIF
			IF fMaxHeight < 0
				fMaxHeight = 0
			ENDIF
		ENDIF	
	ENDIF

	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
		IF iMenuOption = ciGANGAREA_WIDTH	
			fWidth += fWidthIncrease
			IF fWidth > 1500.0
				fWidth = 1500.0
			ENDIF
		ELIF iMenuOption = ciGANGAREA_MAXHEIGHT
			IF IS_VECTOR_ZERO(vPos1)
				fMaxHeight += 1.0
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vPos1)
		tempVec1 = vPos1
	ELSE
		tempVec1 = vCoronaPos + <<0,0,fMaxHeight>>
		INT iR, iG, iB, iA
		GET_HUD_COLOUR(HUD_COLOUR_PURPLE, iR, iG, iB, iA)
		IF fMaxHeight > 0
			DRAW_MARKER(MARKER_SPHERE, tempVec1, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, <<fWidth, fWidth, fWidth>>, iR, iG, iB, 120)	
		ELSE
			DRAW_MARKER(MARKER_SPHERE, tempVec1, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, <<fWidth, fWidth, fWidth>>, iR, iG, iB, 120, FALSE, FALSE, EULER_YXZ, FALSE, NULL_STRING(), NULL_STRING(), TRUE)		
		ENDIF
	ENDIF
	
	
	IF iMenuOption = ciGANGAREA_TYPE		
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)			
			iAreaType ++ 
			CAP_FMMC_MENU_ITEM(iMaxTypes, iAreaType)
		ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)			
			iAreaType -- 
			CAP_FMMC_MENU_ITEM(iMaxTypes, iAreaType)
		ENDIF
	ENDIF
		
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR FMMC_IS_MAP_ACCEPT_JUST_PRESSED(sFMMCmenu)
		IF iMenuOption = ciGANGAREA_CLEAR
			vPos1 = <<0,0,0>>
			vPos2 = <<0,0,0>>
			fWidth = 2
			iAreaType = 0
			fMaxHeight = 0
			fMinHeight = -1
		ELSE
			IF iAreaType = 0
				IF IS_VECTOR_ZERO(vPos1)
					vPos1 = tempVec1
					vPos2 = <<0,0,0>>
					IF fMaxHeight > 0
						SET_BIT(iBitset, iValue)
					ELSE
						CLEAR_BIT(iBitset, iValue)
					ENDIF
					PLAY_SOUND_FRONTEND(-1, "SELECT_LOCATION", GET_CREATOR_SPECIFIC_SOUND_SET())
					PRINTSTRING("SELECT_LOCATION SOUND EFFECT!!!")PRINTNL()	
				ENDIF					
				sCurrentVarsStruct.fCheckPointSize = ciDEFAULT_CHECK_POINT_SIZE
				sHCS.hcCurrentCoronaColour = sHCS.hcDefaultCoronaColour
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		if IS_VECTOR_ZERO(vPos1)
			vPos1 = <<0,0,0>>
			vPos2 = <<0,0,0>>
			fMaxHeight = 0
			fMinHeight = -1
			fWidth = 5	
			iAreaType = 0
		ENDIF
		sHCS.hcCurrentCoronaColour = sHCS.hcDefaultCoronaColour
		bDoingAreaStuff = FALSE
	ENDIF
	
ENDPROC

PROC UPDATE_MENU_FOR_TEST_MISSION_STATE()
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
		UNLOCK_MENU_ITEMS_AND_ALLOW_MISSION_TEST()
	ENDIF
ENDPROC

FUNC STRING GET_FMMC_DOOR_STRING(INT i)
	SWITCH i
		CASE -1
			RETURN "FMMC_SS_RL_21A"
		BREAK
		CASE ciFMMC_DOOR_BANK_1
			RETURN "FMMC_SS_RL_21B"
		BREAK
		CASE ciFMMC_DOOR_BANK_VAULT
		CASE ciFMMC_DOOR_ORNATE_BANK_VAULT
			RETURN "FMMC_SS_RL_21C"
		BREAK
		CASE ciFMMC_DOOR_BANK_LOBBY
			RETURN "FMMC_SS_RL_21K"
		BREAK
		CASE ciFMMC_DOOR_CHEMICAL_GARAGE
			RETURN "FMMC_SS_RL_21D"
		BREAK
		CASE ciFMMC_DOOR_VINEWOOD_GARAGE
			RETURN "FMMC_SS_RL_21E"
		BREAK
		CASE ciFMMC_DOOR_DOCK_CONTROL
			RETURN "FMMC_SS_RL_21F"
		BREAK
		CASE ciFMMC_DOOR_PLAYBOY
			RETURN "FMMC_SS_RL_21G"
		BREAK
		CASE ciFMMC_DOOR_DOCK_CONTROL_2
			RETURN "FMMC_SS_RL_21H"
		BREAK
		CASE ciFMMC_DOOR_GOV_FACILITY_GATE
			RETURN "FMMC_SS_RL_21I"
		BREAK
		CASE ciFMMC_DOOR_AIRPORT_GATE_1
			RETURN "FMMC_SS_RL_21J"
		BREAK
		CASE ciFMMC_DOOR_FLECCA_RFHILLS1
		CASE ciFMMC_DOOR_FLECCA_PERSHING1
		CASE ciFMMC_DOOR_FLECCA_CHUMASH1
			RETURN "FMMC_SS_RL_21M"
		BREAK
		CASE ciFMMC_DOOR_FLECCA_RFHILLS2
		CASE ciFMMC_DOOR_FLECCA_PERSHING2
		CASE ciFMMC_DOOR_FLECCA_CHUMASH2
			RETURN "FMMC_SS_RL_21N"
		BREAK
		CASE ciFMMC_DOOR_FLECCA_RFHILLSEXT
		CASE ciFMMC_DOOR_FLECCA_PERSHINGEXT
		CASE ciFMMC_DOOR_FLECCA_CHUMASHEXT
			RETURN "FMMC_SS_RL_21L"
		BREAK
	ENDSWITCH
	
	RETURN "FMMC_SS_RL_21A"
ENDFUNC

FUNC STRING GET_MENU_ITEM_DESCRIPTION()
	
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) != -1
	AND NOT IS_STRING_NULL_OR_EMPTY(sFMMCmenu.sOptionDescription[GET_CREATOR_MENU_SELECTION(sFMMCmenu)])
		RETURN sFMMCmenu.sOptionDescription[GET_CREATOR_MENU_SELECTION(sFMMCmenu)]
	ENDIF
	
	MODEL_NAMES mnProp = GET_CREATOR_PROP_MODEL(sFMMCMenu.iPropLibrary, sFMMCMenu.iPropType)	
	IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_LTS_TRIGGER
			RETURN "LTS_H_SSL"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_LTS_PANCAM
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
				RETURN "FMMC_ER_006"
			ELSE
				RETURN "LTS_H_33"
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_LTS_TMSTRT
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
				RETURN "FMMC_ER_006"
			ELIF NOT ARE_PLAY_AREA_BOUNDS_SET_FOR_LTS()
				RETURN "FMMC_ER_PAB"
			ELSE
				RETURN "LTS_H_TMSTRT"
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_LTS_WEP
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
				RETURN "FMMC_ER_006"
			ELSE
				RETURN "MC_H_WEP"
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_LTS_VEH
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
				RETURN "FMMC_ER_006"
			ELSE
				RETURN "LTS_H_VEH"
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_LTS_PRP
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
				RETURN "FMMC_ER_006"
			ELSE
				RETURN "LTS_H_PRP"
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_LTS_ZONES
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
				RETURN "FMMC_ER_006"
			ELSE
				RETURN "CTF_H_ZONE"
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_LTS_PLAYAREA
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
				RETURN "FMMC_ER_006"
			ELSE
				RETURN "MC_H_PLYBD"
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_LTS_MAP
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
				RETURN "FMMC_ER_006"
			ELSE
				RETURN "MC_H_MAP"
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_LTS_RST
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
				RETURN "FMMC_ER_006"
			ELSE
				RETURN "LTS_H_RST"
			ENDIF
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_LTS_TEAM_DETAILS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_TEAM_NAME
			RETURN "CTF_H_TMNM"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_WANTED
			RETURN "CTF_H_TMWNTD"
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_LTS_DEV_ONLY_OPTIONS
	
	ELIF sFMMCmenu.sActiveMenu = eFmmc_ROAMING_SPECTATOR_OPTIONS
		RETURN GET_ROAMING_SPECTATOR_DESCRIPTIONS(sFMMCmenu)
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_NAME_OPTION
		RETURN "CTF_H_TMNAM"
	ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_WANTED_OPTION
		RETURN "CTF_H_TMWTD"
	ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_START_WEAPON_OPTION
		RETURN "CTF_H_TMFWP"
	ELIF sFMMCmenu.sActiveMenu = efmmc_PROP_TEMPLATE_BASE
		RETURN "MC_H_PTMP_BASE"
		
	ELIF sFMMCmenu.sActiveMenu = efmmc_PROP_TEMPLATE_GRAB_PROPS
		IF sFMMCmenu.iCurrentSelection = 0
			RETURN "MC_H_PTMP_NEW"
		ELIF sFMMCmenu.iCurrentSelection = 1
			RETURN "MC_H_PTMP_SAVE"
		ENDIF	
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_SPAWN_POINT
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
			IF sFMMCmenu.iSelectedTeam > -1
			AND sFMMCmenu.iSelectedTeam < FMMC_MAX_TEAMS
				iNumPointsPerTeam = GET_NUM_SPAWN_POINTS_NEEDED() 
				iNumPointsPerTeam -=g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[sFMMCmenu.iSelectedTeam]
				//More than enough spawn points
				IF iNumPointsPerTeam <= 0
					RETURN "LTS_H_14T"
				//need only one more
				ELIF iNumPointsPerTeam = 1
					RETURN "LTS_H_14A"
				//Need lots more
				ELSE
					RETURN "LTS_H_14AS"
				ENDIF
			ELSE
				RETURN "LTS_H_14AS"
			ENDIF
		ELSE
			IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				RETURN "LTS_H_14C"
			ELSE
				RETURN "LTS_H_14B"
			ENDIF
		ENDIF

	ELIF sFMMCmenu.sActiveMenu = eFmmc_ZONES_BASE
		RETURN GET_ZONE_BASE_MENU_DESCRIPTION(sFMMCmenu)
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
			IF DOES_BLIP_EXIST(bCameraTriggerBlip)
				RETURN "LTS_H_33B"
			ELSE
				RETURN "LTS_H_33"	
			ENDIF			
		ELSE
			RETURN "LTS_H_35"
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_CAMERAS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
			IF DOES_BLIP_EXIST(bCameraTriggerBlip)
				RETURN "LTS_H_36B"
			ELSE
				IF g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[sFMMCmenu.iSelectedCameraTeam] > 0
					RETURN "LTS_H_36A"	
				ELSE
					RETURN "LTS_H_36C"
				ENDIF
			ENDIF	
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
			RETURN "LTS_H_0"
		ELSE
			IF NOT DOES_BLIP_EXIST(bTeamCamPanBlip[sFMMCmenu.iSelectedCameraTeam])
				RETURN "LTS_H_36E"
			ELSE
				RETURN "LTS_H_36D"
			ENDIF
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_SELECT_START_END
		RETURN "LTS_H_SSL"
	ELIF sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_WEAPON_MODS
		RETURN "MC_H_VEH_VWM"
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_RESTRICTION_MENU
		RETURN "MC_H_VEH_RST0"
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_VEHICLES_BASE
		IF IS_BIT_SET(iLocalBitSet, biDLCLocked)
			RETURN "FMMC_ER_031"
		ENDIF
		//Category
		IF IS_MENU_ITEM_SELECTED("FMMC_MCAT")
			RETURN "CTF_H_VEH01"
			
		//Type
		ELIF IS_MENU_ITEM_SELECTED("FMMC_MTYPE")
			RETURN "mc_H_VEH0"
			
		//Colour
		ELIF IS_MENU_ITEM_SELECTED("FMMC_VEH_COL")
			RETURN "MC_H_VEH1"
			
		//Livery
		ELIF IS_MENU_ITEM_SELECTED("FMMC_VEHL")
			RETURN "MC_H_VEH1A"
			
		//Vehicle Mods
		ELIF IS_MENU_ITEM_SELECTED("FMMC_VEH_VWM")
			RETURN "MC_H_VEH_VWM"
			
		//Vehicle Health
		ELIF IS_MENU_ITEM_SELECTED("FMMC_VEH_HP")
			RETURN "MC_H_VEH4A"
			
		//Bulletproof Tyres
		ELIF IS_MENU_ITEM_SELECTED("FMMC_VEH_EBPT")
			RETURN "MC_H_VEH_EBPT"
			
		//Team Restrictions
		ELIF IS_MENU_ITEM_SELECTED("FMMC_VEH_RST")
			RETURN "MC_H_VEH_RST"
			
		//Cycle Items
		ELIF IS_MENU_ITEM_SELECTED("FMMCCMENU_CYC")
			RETURN "MC_H_VEH4"
			
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DELETE_ENTITIES
				
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0		
			RETURN "LTS_H_DE"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1	
			RETURN "LTS_H_DTSP"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 2	
			RETURN "LTS_H_DW"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 3	
			RETURN "LTS_H_DP"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 4	
			RETURN "LTS_H_DDP"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 5	
			RETURN "LTS_H_DV"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 6	
			RETURN "LTS_H_DZ"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 7	
			RETURN "LTS_H_DAPA"
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE
		IF IS_SWITCH_PROP_OPTION_SELECTED(sFMMCmenu)
			RETURN "DMC_H_16_3"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_LIBRARY
			RETURN "DMC_H_16_2a"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_CREATE
			RETURN "DMC_H_16_1"
		ENDIF
		IF IS_STUNT_LIBRARY(sFMMCMenu.iPropLibrary)
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_COLOUR
				RETURN "MC_H_PRP_CLR"
			ElIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ROTATION_TYPE
				RETURN GET_PROP_ROTATION_STRING(sFMMCmenu, mnProp)
			ElIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_STACKING
				RETURN "MC_H_PRP_STK"
			ElIF sFMMCMenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_Action_Set_Blimp_Message
				RETURN "MC_H_RSD_BPT"
			ENDIF
			IF IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary)
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_SNAPPING
					RETURN "MC_H_PRP_SNP"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ADVANCED_OPTIONS
					RETURN "MC_H_PRP_ADV"
				ENDIF
			ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_STUNT_SPECIAL
				IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_FLARE_01"))
				OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("ind_prop_firework_01"))
				OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("ind_prop_firework_03"))
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_PLAYERS
						RETURN "MC_H_PRP_PTF"
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_LAPS
						RETURN "MC_H_PRP_LTF"
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ADVANCED_OPTIONS
						RETURN "MC_H_PRP_ADV"
					ENDIF
				ELSE
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ADVANCED_OPTIONS
						RETURN "MC_H_PRP_ADV"
					ENDIF
				ENDIF	
			ELSE
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ADVANCED_OPTIONS
					RETURN "MC_H_PRP_ADV"
				ENDIF
			ENDIF
		ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_TEMPLATES
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ROTATION_TYPE
				RETURN GET_PROP_ROTATION_STRING(sFMMCmenu, mnProp)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_STACKING
				RETURN "MC_H_PRP_STK"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ADVANCED_OPTIONS
				RETURN "MC_H_PTMP_BASE"
			ElIF sFMMCMenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_Action_Set_Blimp_Message
				RETURN "MC_H_RSD_BPT"
			ENDIF
		ELSE
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ROTATION_TYPE
				RETURN GET_PROP_ROTATION_STRING(sFMMCmenu, mnProp)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_STACKING
				RETURN "MC_H_PRP_STK"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ADVANCED_OPTIONS
			AND ARE_STUNT_PROPS_AND_FEATURES_ALLOWED()
				RETURN "MC_H_PRP_ADV"
			ElIF sFMMCMenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_Action_Set_Blimp_Message
				RETURN "MC_H_RSD_BPT"
			ENDIF
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0	//Position offset
			RETURN "MC_H_PRP_PSO"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1	//Rotation offset
			RETURN "MC_H_PRP_RSO"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 2	//Snapping
			RETURN "MC_H_PRP_LKDL"
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_SNAPPING_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_PROXIMITY
			RETURN "MC_H_PRP_DPX"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_TRIGGERED
			RETURN "MC_H_PRP_DTR"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_CHAIN
			RETURN "MC_H_PRP_DCH"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_OVERRIDE
			RETURN "MC_H_PRP_ORS"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_ANGLE
			RETURN "MC_H_PRP_SNPANG"
		ENDIF
	ELIF (sFMMCmenu.sActiveMenu = eFMMC_GENERIC_OVERRIDE_POSITION)
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_USE_OVERRIDE
			RETURN "MC_H_ORPU"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_ALIGNMENT
			RETURN "MC_H_ORPL"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_X_VALUE
			RETURN "MC_H_ORPX"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_Y_VALUE
			RETURN "MC_H_ORPY"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_Z_VALUE
			RETURN "MC_H_ORPZ"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_RESET
			RETURN "MC_H_ORPR"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_WORLD_COORD
			RETURN "MC_H_WDPS"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_CLAMP_X
			RETURN "MC_H_OVR_CLMPX"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_CLAMP_Y
			RETURN "MC_H_OVR_CLMPY"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_CLAMP_Z
			RETURN "MC_H_OVR_CLMPZ"
		ENDIF
	ELIF (sFMMCmenu.sActiveMenu = eFmmc_GENERIC_OVERRIDE_ROTATION)
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_USE_OVERRIDE
			RETURN "MC_H_ORRU"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_X_VALUE
			RETURN "MC_H_ORRX"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_Y_VALUE
			RETURN "MC_H_ORRY"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_Z_VALUE
			RETURN "MC_H_ORRZ"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_RESET
			RETURN "MC_H_ORRR"
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFMMC_OFFSET_POSITION
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_USE_OFFSET
			RETURN "MC_H_PRP_UPO"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_FREE_CAMERA
			RETURN "MC_H_PRP_PTF"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_LOCK_POSITION
			RETURN "MC_H_PRP_UFC"
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFMMC_OFFSET_ROTATION
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_USE_OFFSET
			RETURN "MC_H_PRP_URO"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_FREE_CAMERA
			RETURN "MC_H_PRP_UFC"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_LOCK_POSITION
			RETURN "MC_H_PRP_LRO"
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_CREATE
			RETURN "DMC_H_16_1"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_LIBRARY
			RETURN "LTS_H_16_2b"
		ELIF IS_MENU_ITEM_SELECTED("FMMC_OVR_POS")
			RETURN "MC_OVR_POS_H"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 2 AND ARE_STRINGS_EQUAL(GET_CREATOR_NAME_FOR_PROP_MODEL(GET_CREATOR_PROP_MODEL(sFMMCmenu.iPropLibrary, sFMMCmenu.iPropType)),"FMMC_DPR_AMOCRT")
			RETURN "FMMC_AC_H_A"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCmenu.iColorDynoPropMenuPosition AND DOES_PROP_HAVE_EXTRA_COLOURS(GET_CREATOR_PROP_MODEL(sFMMCMenu.iPropLibrary, sFMMCMenu.iPropType))
			RETURN "MC_H_PRP_CLR"	
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCmenu.iAmmoDynoPropMenuPosition AND sFMMCmenu.iPropType = AMMO_CRATE_DYNOPROP_REF
			RETURN "MC_H_PRP_CLR"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCmenu.iOverridePositionDynoPropMenuPosition
			RETURN "MC_H_PRP_PSO"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCmenu.iOverrideRotationDynoPropMenuPosition
			RETURN "MC_H_PRP_RSO"
		ELIF IS_SWITCH_PROP_OPTION_SELECTED(sFMMCmenu)
			RETURN "DMC_H_16_3"
		ELSE
			RETURN "LTS_H_16_2b"
		ENDIF	
	ELIF sFMMCmenu.sActiveMenu = eFmmc_RADIO_MENU
		RETURN "LTS_H_20"	
	ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_NAME 
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
				IF bSignedOut
					RETURN GET_DISCONNECT_HELP_MESSAGE()
				ELSE
					RETURN "FMMCNO_CLOUD"
				ENDIF
			ELIF sFMMCendStage.bHasValidROS = FALSE
				RETURN "FMMCNO_SCLUB"
			ELSE
				RETURN "LTS_H_MD0"
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_DESCRIPTION
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
				IF bSignedOut
					RETURN GET_DISCONNECT_HELP_MESSAGE()
				ELSE
					RETURN "FMMCNO_CLOUD"
				ENDIF
			ELIF sFMMCendStage.bHasValidROS = FALSE
				RETURN "FMMCNO_SCLUB"
			ELSE
				RETURN "LTS_H_MD1"
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_PHOTO
			RETURN "LTS_H_40"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_TAGS
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
				IF bSignedOut
					RETURN GET_DISCONNECT_HELP_MESSAGE()
				ELSE
					RETURN "FMMCNO_CLOUD"
				ENDIF
			ELIF sFMMCendStage.bHasValidROS = FALSE
				RETURN "FMMCNO_SCLUB"
			ELSE
				RETURN "DMC_H_42"
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_MAX_PLAYERS
			RETURN "LTS_H_NOP"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_MIN_PLAYERS
			RETURN "LTS_H_MIP"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_NO_TEAMS
			RETURN "LTS_H_NOT"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_FORCE_CAMERA
			RETURN "FMMC_FRCAM_H"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_REMOVE_HELMETS
			RETURN "MC_H_RML_HEL"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_TIME_LIMIT
			RETURN "LTS_H_RNDT"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_STARTWEP
			RETURN "LTS_H_FWP"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_LOCKWEP
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMInitialWepLocked)
				RETURN "LTS_H1_LKW"
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMWeaponsLocked) 
				RETURN "LTS_H2_LKW"
			ELSE
				RETURN "LTS_H0_LKW"
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_NUMROUNDS
			RETURN "LTS_H_NR"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_AMBIENT
			RETURN "LTS_H_MD20"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_TEAM_DETAILS
			RETURN "LTS_H_TMDT"
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_LTS_DEV_ONLY_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_LOCKWEP
			RETURN "MC_H_EECDFV"			
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_PLAY_AREA_BOUNDS
		INT iCurrSelection = GET_CREATOR_MENU_SELECTION(sFMMCmenu)+1
		
		IF iCurrSelection = ciLEGACY_BOUNDS_SET_TYPE
			IF IS_ROCKSTAR_DEV()
				RETURN "LTS_BD_1" 
			ELSE
				RETURN "LTS_BD_2"
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].iType = 0
			IF IS_ROCKSTAR_DEV()
				IF iCurrSelection = ciLEGACY_BOUNDS_SET_AREA			
					RETURN "LTS_BD_2" 
				ELSE
					RETURN "LTS_BD_6"
				ENDIF
			ELSE
				RETURN "LTS_BD_6"
			ENDIF
		ELSE
			IF iCurrSelection = ciLEGACY_BOUNDS_SET_AREA
				RETURN "LTS_BD_2W"
			ELIF iCurrSelection = ciLEGACY_BOUNDS_SET_AREA_MAXHEIGHT
				RETURN "LTS_BD_3"
			ELIF iCurrSelection = ciLEGACY_BOUNDS_SET_AREA_MINHEIGHT
				RETURN "LTS_BD_4" 
			ELSE
				RETURN "LTS_BD_6"
			ENDIF
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_AMBIENT_MENU	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_TIME
			RETURN "LTS_H_MD15"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_WEATHER
			RETURN "LTS_H_WETH"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_SERVICES 
			RETURN "LTS_H_SRV"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_TRAFFIC
			RETURN "LTS_H_MD17"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_PEDS
			RETURN "LTS_H_MD18"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_MUSIC
			RETURN "LTS_H_MD16"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_RADIO
			RETURN "FMMC_HELP_RAD"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_GANGS
			RETURN "LTS_H_MD21"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_WANTED_MAX
			RETURN "LTS_H_POL"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_WANTED_CLEAR
			RETURN "LTS_H_WANTED"
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_RADAR_MENU	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_CAPTURE_RDR_ZOOM
			RETURN "FMMC_RDR_H0"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_CAPTURE_RDR_BLIP_PICKUP
			RETURN "FMMC_RDR_H1"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_CAPTURE_RDR_SPRINT
			RETURN "FMMC_RDR_H2"
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
		RETURN "LTS_H_40"
	ELIF sFMMCmenu.sActiveMenu = eFmmc_TEST_BASE
		RETURN "LTS_H_44P"
	ELIF sFMMCmenu.sActiveMenu = eFmmc_LTS_TEAM_DETAILS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_TEAMNAME
			RETURN "CTF_H_TMNM"
		ElIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_TEAMWANTED
			RETURN "CTF_H_TMWNTD"
		ElIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_LTS_TEAMFORCEDWEAPON
			RETURN "CTF_H_TMFWP"
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_OPTIONS
			RETURN "LTS_H_23"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_CREATOR
			//if we are allowed to place then say
			IF IS_BIT_SET(sFMMCmenu.iTopMenuActive, TOP_MENU_CREATOR)
				RETURN "LTS_H_24"		
			//not allwoed to place then say that. 
			ELSE
				RETURN "LTS_H_24A"
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_RADIO
			RETURN "LTS_H_20"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_EXIT
			RETURN "LTS_H_25"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_TEST
		
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
				IF bSignedOut
					RETURN GET_DISCONNECT_HELP_MESSAGE()
				ELSE
					RETURN "FMMCNO_CLOUD"
				ENDIF
			ELIF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
				RETURN "LTS_H_30"
			ELIF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_PLAYBOUNDS)
				RETURN "LTS_H_31E"
			ELIF NOT IS_BIT_SET(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)	
				//No start location
				IF IS_BIT_SET(iTestFailBitSet, ciFAIL_BIT_SET_NO_STARTLOC_BUT_NEEDED_TEAM_1)
				OR IS_BIT_SET(iTestFailBitSet, ciFAIL_BIT_SET_NO_STARTLOC_BUT_NEEDED_TEAM_2)
				OR IS_BIT_SET(iTestFailBitSet, ciFAIL_BIT_SET_NO_STARTLOC_BUT_NEEDED_TEAM_3)
				OR IS_BIT_SET(iTestFailBitSet, ciFAIL_BIT_SET_NO_STARTLOC_BUT_NEEDED_TEAM_4)
					RETURN "LTS_H_31B"				
				//no drop off
				ELIF IS_BIT_SET(iTestFailBitSet, ciFAIL_BIT_SET_NO_DOFF_BUT_NEEDED_TEAM_1)
				OR IS_BIT_SET(iTestFailBitSet, ciFAIL_BIT_SET_NO_DOFF_BUT_NEEDED_TEAM_2)
				OR IS_BIT_SET(iTestFailBitSet, ciFAIL_BIT_SET_NO_DOFF_BUT_NEEDED_TEAM_3)
				OR IS_BIT_SET(iTestFailBitSet, ciFAIL_BIT_SET_NO_DOFF_BUT_NEEDED_TEAM_4)
					RETURN "LTS_H_31C"	
				ENDIF
			ELSE
				IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_UNTESTED)
					RETURN "LTS_H_41"
				/*ELIF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_PHOTO)
					RETURN "LTS_H_40HA"
				ELIF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_INTROCAM)
					RETURN "LTS_H_40HB"
				ELIF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_POINT_MORE_THAN_OBJECTS)
					RETURN "LTS_H_40HC"*/
				ELSE
					RETURN "LTS_H_17"
				ENDIF
			ENDIF		
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_SAVE
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
				IF bSignedOut
					RETURN GET_DISCONNECT_HELP_MESSAGE()
				ELSE
					RETURN "FMMCNO_CLOUD"
				ENDIF
			ELSE
				IF g_FMMC_STRUCT.bMissionIsPublished
					RETURN "LTS_H_29C"
				ELSE
					IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TITLE)
						RETURN "LTS_H_38B"
					ELSE
						IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_NOT_PLAYABLE)
							RETURN "LTS_H_38"
						ELSE
							RETURN "LTS_H_28"
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_PUBLISH
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
				IF bSignedOut
					RETURN GET_DISCONNECT_HELP_MESSAGE()
				ELSE
					RETURN "FMMCNO_CLOUD"
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_BLOCKED)
					IF NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_CHEATER)
						IF NOT IS_BIT_SET(sFMMCmenu.iTopMenuActive, TOP_MENU_PUBLISH)
							//If there's no changes then say that there is none
							IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CHANGE)
								RETURN "LTS_H_38D"
							//If it's untested
							ELIF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_UNTESTED)
								IF g_FMMC_STRUCT.bMissionIsPublished = TRUE 
									RETURN "LTS_H_38T"
								ELSE
									RETURN "LTS_H_38TB"
								ENDIF
							//if it's because it's not valid then say that!. 
							ELSE
								RETURN "LTS_H_38C"
							ENDIF
						ELSE
							IF g_FMMC_STRUCT.bMissionIsPublished
								RETURN "LTS_H_29B"
							ELSE						
								RETURN "LTS_H_29"
							ENDIF
						ENDIF
					ELSE
						RETURN "LTS_H_38A"
					ENDIF
				ELSE
					STRING sRet
					IF g_FMMC_STRUCT.bMissionIsPublished
						sRet = "ALERT_LTS_UPD"
					ELSE
						sRet = "ALERT_LTS_PUB"
					ENDIF
					RETURN sRet
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN ""
ENDFUNC

FUNC MENU_ICON_TYPE DOES_THIS_DESCRIPTION_NEED_A_WARNING(STRING description)
	IF ARE_STRINGS_EQUAL(description, "CTF_H_40") AND (IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_PHOTO) OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_LAST_GEN_PHOTO))
	OR ARE_STRINGS_EQUAL(description, "CTF_H_PANF") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_INTROCAM)
	OR ARE_STRINGS_EQUAL(description, "CTF_H_SSL") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
	OR ARE_STRINGS_EQUAL(description, "CTF_H_MD0") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TITLE)
	OR ARE_STRINGS_EQUAL(description, "CTF_H_MD1") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_DESC)
	OR ARE_STRINGS_EQUAL(description, "CTF_H_33") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_INTROCAM)
	OR ARE_STRINGS_EQUAL(description, "CTF_H_33A") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_INTROCAM)
	OR ARE_STRINGS_EQUAL(description, "CTF_H_TMSTRT") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_SPAWNS)
	OR ARE_STRINGS_EQUAL(description, "ALERT_CTF_UPD") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_BLOCKED)
	OR ARE_STRINGS_EQUAL(description, "ALERT_CTF_PUB") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_BLOCKED)
	OR ARE_STRINGS_EQUAL(description, "ALERT_LTS_UPD") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_BLOCKED)
	OR ARE_STRINGS_EQUAL(description, "ALERT_LTS_PUB") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_BLOCKED)
	OR ARE_STRINGS_EQUAL(description, "FMMCNO_CLOUD")
	OR ARE_STRINGS_EQUAL(description, GET_DISCONNECT_HELP_MESSAGE())
	OR ARE_STRINGS_EQUAL(description, "FMMCNO_SCLUB")	
		RETURN MENU_ICON_ALERT
	ENDIF
	
	RETURN MENU_ICON_DUMMY
ENDFUNC

PROC DRAW_MENU_SELECTION(INT iSelected)//, BOOL bActive)
	
	IF g_bShow_FMMC_mainMenu = TRUE
		EXIT
	ENDIF
	
	IF g_bShow_FMMC_rulesMenu
		EXIT
	ENDIF
	
	CLEAR_BIT(iHelpBitSet, biPickupEntityButton)
	CLEAR_BIT(iHelpBitSet, biDeleteEntityButton)
	CLEAR_BIT(iHelpBitSet, biRemovePedsButton)
	
	IF (IS_BIT_SET(iLocalBitSet, biEntityInCorona)
	AND sFMMCMenu.iSelectedEntity = -1
	AND (sCurrentVarsStruct.iHoverEntityType  = sFMMCmenu.iEntityCreation))
	AND sCurrentVarsStruct.iHoverEntityType != CREATION_TYPE_CHECKPOINT
	AND sCurrentVarsStruct.iHoverEntityType != CREATION_TYPE_PROPS
		SET_BIT(iHelpBitSet, biPickupEntityButton)
	ENDIF
	
	IF IS_BIT_SET(iLocalBitSet, biGotoPointInCorona)
	AND sFMMCMenu.iSelectedEntity = -1
		SET_BIT(iHelpBitSet, biPickupCoronaButton)
	ELSE
		CLEAR_BIT(iHelpBitSet, biPickupCoronaButton)
	ENDIF
	
	sCurrentVarsStruct.bSelectedAnEntity = FALSE
	IF IS_BIT_SET(iLocalBitSet, biEntityInCorona)
	AND sCurrentVarsStruct.iHoverEntityType != CREATION_TYPE_CHECKPOINT
	AND sCurrentVarsStruct.iHoverEntityType != CREATION_TYPE_PROPS
	AND sCurrentVarsStruct.iHoverEntityType != CREATION_TYPE_WEAPONS
	AND sCurrentVarsStruct.iHoverEntityType != CREATION_TYPE_PEDS
	AND sCurrentVarsStruct.iHoverEntityType != CREATION_TYPE_DYNOPROPS
		sCurrentVarsStruct.bSelectedAnEntity = TRUE		
		IF (sFMMCMenu.iSelectedEntity = -1)
			SET_BIT(iHelpBitSet, biDeleteEntityButton)
		ENDIF
	ENDIF
	
	IF sPedStruct.iSwitchingINT = CREATION_STAGE_PLACE
	OR sCapObjStruct.iSwitchingINT = CREATION_STAGE_PLACE
	OR sCapObjStruct.iSwitchingINT = CREATION_STAGE_PLACE
	OR sVehStruct.iSwitchingINT = CREATION_STAGE_PLACE
	OR sWepStruct.iSwitchingINT = CREATION_STAGE_PLACE
	OR GET_CURRENT_GENERIC_CREATION_STAGE() = CREATION_STAGE_PLACE
		SET_BIT(iHelpBitSet, biRotateCoronaButton)
	ELSE
		CLEAR_BIT(iHelpBitSet, biRotateCoronaButton)
	ENDIF
	
	IF IS_BIT_SET(iLocalBitSet, biEntityInCorona)		
		IF (sFMMCMenu.iSelectedEntity = -1)
			VEHICLE_INDEX vehTemp
			IF IS_ENTITY_A_VEHICLE(sCurrentVarsStruct.vCoronaHitEntity)
				vehTemp = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sCurrentVarsStruct.vCoronaHitEntity)
				IF IS_VEHICLE_DRIVEABLE(vehTemp)
				AND NOT IS_VEHICLE_EMPTY(vehTemp)
					SET_BIT(iHelpBitSet, biRemovePedsButton)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	BOOL bRefresh
	IF iHelpBitSetOld != iHelpBitSet
		iHelpBitSetOld = iHelpBitSet
		bRefresh = TRUE
	ENDIF
	
	if sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE 
	or sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
		SET_TOP_MENU_ITEM(iTopItem)
	ENDIF
	
	// Set the current item and draw menu each frame
	SET_CURRENT_MENU_ITEM(iSelected)
	
	IF sCurrentVarsStruct.bDisplayFailReason
		IF sCurrentVarsStruct.iFailDescriptionTimer > GET_GAME_TIMER() - 5000
			SET_CURRENT_MENU_ITEM_DESCRIPTION(sCurrentVarsStruct.sFailReason)
		ELSE
			sCurrentVarsStruct.bDisplayFailReason = FALSE
		ENDIF
	ENDIF
	IF sCurrentVarsStruct.bDisplayFailReason = FALSE			
		IF IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Position_Override)
		AND IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Rotation_Override)
		AND (sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE OR sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRRPW", 0, MENU_ICON_ALERT)
		ELIF IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Position_Override)
		AND (sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE OR sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRPW", 0, MENU_ICON_ALERT)
		ELIF IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Rotation_Override)
		AND (sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE OR sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRRW", 0, MENU_ICON_ALERT)
			
		ELIF CURRENTLY_USING_FMMC_OVERRIDE_POSITION(sFMMCmenu)
		AND CURRENTLY_USING_FMMC_OVERRIDE_ROTATION(sFMMCmenu)
		AND sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRRDPW", 0, MENU_ICON_ALERT)
		ELIF CURRENTLY_USING_FMMC_OVERRIDE_POSITION(sFMMCmenu)
		AND sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRDPW", 0, MENU_ICON_ALERT)
		ELIF CURRENTLY_USING_FMMC_OVERRIDE_ROTATION(sFMMCmenu)
		AND sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRDRW", 0, MENU_ICON_ALERT)
			
		ELIF sCurrentVarsStruct.bDisplaySelected
		AND sFMMCmenu.iEntityCreation != CREATION_TYPE_NONE
			PROCESS_SHOWING_HIGHLIGHTED_ENTITY(sCurrentVarsStruct)
		ELSE
			STRING stHelp = GET_MENU_ITEM_DESCRIPTION()
			SET_CURRENT_MENU_ITEM_DESCRIPTION(stHelp, 0, DOES_THIS_DESCRIPTION_NEED_A_WARNING(stHelp))
			
			IF sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_SPAWN_POINT
			AND ARE_STRINGS_EQUAL(stHelp, "CTF_H_14AS")
				ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(iNumPointsPerTeam)
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = efmmc_PROP_TEMPLATE_GRAB_PROPS
			AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
				ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING("FM_NXT_LTS")
			ENDIF
			
		ENDIF
	ENDIF
	
	DRAW_MENU()		
	
	if sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
	or sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
		iTopItem = GET_TOP_MENU_ITEM()
	ENDIF
	
	IF bRefresh
		sCurrentVarsStruct.bResetUpHelp = TRUE
	ENDIF
	
ENDPROC

FUNC INT PED_TO_CHANGE()
	INT iPedToChange 
	IF sFMMCmenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_MISSION_CRITICAL_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_GO_TO_CONSEQUENCES
	OR sFMMCmenu.sActiveMenu = eFmmc_ACTOR_DIALOGUE_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_TEAM_REL_GROUPS
	OR sFMMCmenu.sActiveMenu = eFmmc_ENEMY_BASE
	OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLES_BASE
	OR sFMMCmenu.sActiveMenu = eFmmc_OBJECT_BASE
		iPedToChange = sFMMCMenu.iSelectedEntity
		IF sFMMCMenu.iSelectedEntity = -1
			IF sFMMCmenu.iEntityCreation = CREATION_TYPE_PEDS
				iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
			ELIF sFMMCmenu.iEntityCreation = CREATION_TYPE_VEHICLES
				iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
			ELIF sFMMCmenu.iEntityCreation = CREATION_TYPE_OBJECTS
				iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfObjects
			ELIF sFMMCmenu.iEntityCreation = CREATION_TYPE_PROPS
				iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfProps
			ENDIF
		ENDIF
	ENDIF
	RETURN iPedToChange
ENDFUNC

PROC HANDLE_MOUSE_ENTITY_SELECTION_CANCEL_AND_SWITCH(eFMMC_MENU_ENUM itemMenu, INT iNewItemIndex, INT iMaxItems)
	// cancel out of stuff
	CANCEL_ENTITY_CREATION(sFMMCmenu.iEntityCreation) 		
	IF sFMMCMenu.iSelectedEntity != -1	
		sFMMCMenu.iSelectedEntity = -1
	ENDIF
	
	// if we aren't in the  menu we jump into it
	GO_TO_MENU(sFMMCMenu, sFMMCdata, itemMenu)
	REFRESH_MENU(sFMMCMenu)

	// instead of looping through until the switch cam increases we force it
	iSwitchCam = iNewItemIndex - 1
	DEAL_WITH_CAMERA_SWITCH_SELECTION(TRUE, iMaxItems)
	SET_CURSOR_POSITION(0.5, 0.5)
	PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", GET_SOUND_SET_FROM_CREATOR_TYPE())			
ENDPROC

PROC HANDLE_MOUSE_ENTITY_SELECTION(BOOL bTesting = FALSE)

	BOOL bMouseOverCorona = FALSE
	
	IF NOT FMMC_DO_HANDLE_MOUSE_SELECTION_CHECK(sFMMCmenu, sCurrentVarsStruct, bMouseOverCorona, bTesting)
		EXIT
	ENDIF
	
	FP_FMMC_MOUSE_ENTITY_SELECT_HANDLER funcPtr = &HANDLE_MOUSE_ENTITY_SELECTION_CANCEL_AND_SWITCH
	
	// check objects
	IF FMMC_HANDLE_MOUSE_DYNOPROP_SELECTION(sFMMCmenu, sDynoPropStruct, sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_PROP_SELECTION(sFMMCmenu, sPropStruct, sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_SPAWN_POINT_SELECTION(sFMMCmenu, sPedStruct, sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_TEAM_0_SPAWN_POINT_SELECTION(sFMMCmenu, sTeamSpawnStruct[0], sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_TEAM_1_SPAWN_POINT_SELECTION(sFMMCmenu, sTeamSpawnStruct[1], sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_TEAM_2_SPAWN_POINT_SELECTION(sFMMCmenu, sTeamSpawnStruct[2], sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_TEAM_3_SPAWN_POINT_SELECTION(sFMMCmenu, sTeamSpawnStruct[3], sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_VEHICLE_SELECTION(sFMMCmenu, sVehStruct, sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_WEAPON_SELECTION(sFMMCmenu, sWepStruct, sCurrentVarsStruct, sInvisibleObjects, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_ZONE_SELECTION(sFMMCmenu, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF (bMouseOverCorona = FALSE) AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, GET_CREATOR_CAMERA_ORBIT_TOGGLE_BUTTON())
		FMMC_SET_CURSOR_MAP_ACCEPT_BIT(sFMMCmenu, FALSE)
	ENDIF
ENDPROC

FUNC enumFMMC_CIRCLE_MENU_ACTIONS RETURN_CIRCLE_MENU_SELECTIONS()
	
	IF g_bShow_FMMC_mainMenu = TRUE
	OR g_bShow_FMMC_rulesMenu = TRUE
		PRINTLN("DONT DO THE MENUS!")
		RETURN eFmmc_Action_Null
	ENDIF 
	
	IF NOT bshowmenu
		PRINTLN("NOT bshowmenu")	
		RETURN eFmmc_Action_Null
	ENDIF
			
	IF LOAD_MENU_ASSETS()
		DRAW_MENU_SELECTION(GET_CREATOR_MENU_SELECTION(sFMMCmenu))//, bActive)
	ENDIF
	
	IF g_CreatorsSelDetails.bColouringText
		RETURN eFmmc_Action_Null
	ENDIF
	
	IF FFMC_IS_ACCEPT_JUST_PRESSED(sFMMCmenu) OR (FMMC_IS_CIRCLE_MENU_A_CAMERA_MENU(sFMMCmenu.sActiveMenu) AND FMMC_IS_MAP_ACCEPT_JUST_PRESSED(sFMMCmenu))
		UPDATE_MENU_FOR_TEST_MISSION_STATE()
		IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
			IF IS_THIS_OPTION_SELECTABLE()	
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_CAPTURE_NAME
					sCurrentVarsStruct.iMenuState = MENU_STATE_TITLE		
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_CAPTURE_DESCRIPTION
					sCurrentVarsStruct.iMenuState = MENU_STATE_DESCRIPTION
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_CAPTURE_TAGS
					sCurrentVarsStruct.iMenuState = MENU_STATE_TAGS
				ENDIF
				IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Null_item				
					sCurrentVarsStruct.bResetUpHelp = TRUE
					PLAY_SOUND_FRONTEND(-1, "SELECT", GET_SOUND_SET_FROM_CREATOR_TYPE())
					GO_TO_MENU(sFMMCmenu, sFMMCdata)
					IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
					OR sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
					OR sFMMCmenu.sActiveMenu = eFmmc_OUT_CAM_BASE
					OR sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_CAMERAS
						IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
							sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
						ENDIF
					ENDIF
					RETURN eFmmc_Action_Null	
				ENDIF
			ENDIF

		ELSE
			IF IS_THIS_OPTION_SELECTABLE()				
				sCurrentVarsStruct.bResetUpHelp = TRUE
				PRINTSTRING("INPUT_FRONTEND_ACCEPT")PRINTNL()
				IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
					PLAY_SOUND_FRONTEND(-1, "SELECT", GET_SOUND_SET_FROM_CREATOR_TYPE())
					PRINTSTRING("CONFIRM SOUND EFFECT!!!")PRINTNL()
				ENDIF 
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) != -1
					IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Null_item
						IF sFMMCmenu.sActiveMenu != eFmmc_MAIN_MENU_BASE
							PLAY_SOUND_FRONTEND(-1, "SELECT", GET_SOUND_SET_FROM_CREATOR_TYPE())
							PRINTSTRING("CONFIRM SOUND EFFECT!!!")PRINTNL()
						ENDIF 
						//If it is then reset up the menu
						GO_TO_MENU(sFMMCmenu, sFMMCdata)
						IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
						OR sFMMCmenu.sActiveMenu = eFmmc_OUT_CAM_BASE
						OR sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_CAMERAS
							IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
								sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
							ENDIF
						ENDIF
						CLEAR_BIT(sFMMCdata.iBitSet, bPerformingPlacementShapeTest)
						RETURN eFmmc_Action_Null
					ENDIF		
				ENDIF		
				
				IF sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Action_Null
					IF (sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0)								
					OR (sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_CAMERAS AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) != 2)						
						IF bInitialIntroCamSetup
							RETURN eFmmc_Action_Null
						ELIF GET_DISTANCE_BETWEEN_COORDS(g_FMMC_STRUCT.vStartPos, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 100
							PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
							PRINTSTRING("ERROR SOUND EFFECT")PRINTNL()
							sCurrentVarsStruct.bDisplayFailReason = TRUE
							sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
							sCurrentVarsStruct.sFailReason = "FMMC_ER_023"
							RETURN eFmmc_Action_Null
						ENDIF
					ENDIF
					RETURN  sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)]
				ENDIF
			ELSE
				IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_PUBLISH
					OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_SAVE
						CHECK_FOR_BLOCKED_PRIVILEGES(sFMMCmenu)
					ENDIF
				ENDIF
				
				PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
				PRINTSTRING("ERROR SOUND EFFECT")PRINTNL()
			ENDIF
		ENDIF
	ENDIF
	
	IF FMMC_IS_CANCEL_JUST_PRESSED(sFMMCmenu)
	OR SHOULD_FORCE_MENU_BACKOUT(sFMMCMenu)
	
		UPDATE_MENU_FOR_TEST_MISSION_STATE()
		sCurrentVarsStruct.bDisplayFailReason = FALSE
		sFMMCmenu.iNewRule = 0				
		sCurrentVarsStruct.bResetUpHelp = TRUE
		PRINTSTRING("INPUT_FRONTEND_CANCEL")PRINTNL()
		#IF IS_DEBUG_BUILD
			INT iSaveEntityType = sFMMCmenu.iEntityCreation
			PRINTLN("iSaveEntityType = ", iSaveEntityType)
		#ENDIF
		CANCEL_ENTITY_CREATION(sFMMCmenu.iEntityCreation)
		PRINTLN("g_FMMC_STRUCT.iMissionType = ", g_FMMC_STRUCT.iMissionType)
		PRINTLN("sFMMCmenu.sActiveMenu = ", sFMMCmenu.sActiveMenu)
		IF sFMMCMenu.iSelectedEntity != -1	
			sFMMCMenu.iSelectedEntity 		= -1
		ELSE
			IF sFMMCmenu.sMenuBack != eFmmc_Null_item
				PLAY_SOUND_FRONTEND(-1, "BACK", GET_SOUND_SET_FROM_CREATOR_TYPE())
				GO_BACK_TO_MENU(sFMMCmenu)
			ENDIF
		ENDIF
		IF sFMMCmenu.sActiveMenu = eFmmc_SNAPPING_OPTIONS
		OR sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS
			CHECK_IF_SNAPPING_SHOULD_BE_TURNED_OFF(sFMMCmenu)
		ENDIF
		IF sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE
		OR sFMMCmenu.iEntityCreation != CREATION_TYPE_PROPS
			sFMMCmenu.bChainSnapBroken = FALSE
			sPropStruct.iRotTimerStart = -1
			sFMMCmenu.bForceRotate = FALSE
		ENDIF
		REFRESH_MENU(sFMMCMenu)
	ENDIF
	
	IF MENU_CONTROL_DOWN_CREATOR(menuScrollController)
		SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)+1)
		IF g_FMMC_STRUCT.bMissionIsPublished
			IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
			AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_SAVE
				SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)+1)
			ENDIF
		ENDIF
		WHILE DEAL_WITH_SKIPPING_EMPTY_OPTIONS(1)
			PRINTLN("[TMS] Skipping options (down)...")
		ENDWHILE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > sFMMCmenu.iCurrentMenuLength-1
			SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
		ENDIF
		sCurrentVarsStruct.bResetUpHelp = TRUE
		IF NOT sFMMCendStage.bHasValidROS
			sFMMCendStage.bHasValidROS = NETWORK_HAS_VALID_ROS_CREDENTIALS()
		ENDIF
		REFRESH_MENU(sFMMCMenu)
	ENDIF
	
	IF MENU_CONTROL_UP_CREATOR(menuScrollController)
		SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)
		IF g_FMMC_STRUCT.bMissionIsPublished
			IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
			AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_SAVE
				SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)
			ENDIF
		ENDIF
		WHILE DEAL_WITH_SKIPPING_EMPTY_OPTIONS(-1)
			PRINTLN("[TMS] Skipping options (up)...")
		ENDWHILE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < 0
			SET_CREATOR_MENU_SELECTION(sFMMCmenu, sFMMCmenu.iCurrentMenuLength - 1)
		ENDIF
		sCurrentVarsStruct.bResetUpHelp = TRUE
		IF NOT sFMMCendStage.bHasValidROS
			sFMMCendStage.bHasValidROS = NETWORK_HAS_VALID_ROS_CREDENTIALS()
		ENDIF
		REFRESH_MENU(sFMMCMenu)
	ENDIF
		
	IF NOT sFMMCendStage.bHasValidROS
		sFMMCendStage.bHasValidROS = NETWORK_HAS_VALID_ROS_CREDENTIALS()
		IF sFMMCendStage.bHasValidROS //clear any SC unavailable messages
			REFRESH_MENU(sFMMCMenu)
		ENDIF
	ENDIF
	
	INT iUpOrDown = 0		
	IF MENU_CONTROL_LEFT_CREATOR(menuScrollController)
		iUpOrDown = -1
	ELIF MENU_CONTROL_RIGHT_CREATOR(menuScrollController)
		iUpOrDown = 1
	ENDIF
	
	IF sFMMCmenu.sActiveMenu =  eFmmc_DEV_PROOFS_OPTIONS
		EDIT_DEV_PLAYER_PROOFS_OPTIONS_MENU(sFMMCMenu, sFMMCendStage, sCurrentVarsStruct, iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_ROAMING_SPECTATOR_OPTIONS
		EDIT_ROAMING_SPECTATOR_OPTIONS_MENU(sFMMCmenu, sFMMCendStage, sCurrentVarsStruct, iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_DEV_AMBIENT_OPTIONS
		EDIT_DEV_AMBIENT_OPTIONS(sFMMCmenu, sFMMCendStage, iUpOrDown)
	ENDIF
	
	IF iUpOrDown != 0
		EDIT_MENU_OPTIONS(iUpOrDown)
		REFRESH_MENU(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.iSelectedEntity = -1		
		IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
			SET_BIT(iHelpBitSet, biWarpToCameraButton) 
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
		IF sFMMCmenu.sActiveMenu != eFmmc_PAN_CAM_BASE
		AND sFMMCmenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
		AND sFMMCmenu.sActiveMenu != eFmmc_OUT_CAM_BASE
		AND sFMMCmenu.sActiveMenu != eFmmc_DM_TEAM_CAMERAS
		AND !g_CreatorsSelDetails.bColouringText
		AND NOT (HAS_CURRENT_PROP_BEEN_FREE_ROTATED(sFMMCmenu) AND IS_PC_VERSION() AND IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) )
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_CREATOR_SWITCH_CAMERA_BUTTON())	
				IF sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE
				AND sCurrentVarsStruct.bCanSwapCams
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ELSE
					PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
					PRINTSTRING("CANT SWITCH ERROR SOUND EFFECT!!!")PRINTNL()
					sCurrentVarsStruct.bDisplayFailReason = TRUE
					sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
					IF sCurrentVarsStruct.bCanSwapCams
						sCurrentVarsStruct.sFailReason = "FMMC_ER_026"
					ELSE
						sCurrentVarsStruct.sFailReason = "FMMC_ER_CAMSWP"
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN eFmmc_Action_Null
ENDFUNC

FUNC BOOL DO_END_MENU()	
	
	PRINTSTRING("DO_END_MENU - being called")PRINTNL()
	
	IF IS_HELP_MESSAGE_ON_SCREEN()
		CLEAR_HELP()
	ENDIF
	
	IF NOT IS_SCREEN_FADING_OUT()
		IF NOT IS_SCREEN_FADED_OUT()
			IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)		
				DO_SCREEN_FADE_OUT(500)
			ENDIF
		ELSE
			DRAW_END_MENU()
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				PRINTSTRING("CONFIRM SOUND EFFECT!!!")PRINTNL()
				sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				DO_SCREEN_FADE_IN(500)
				sCurrentVarsStruct.bSwitching = FALSE
				sCurrentVarsStruct.stiScreenCentreLOS = NULL
			ENDIF			
		
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				PRINTSTRING("CONFIRM SOUND EFFECT!!!")PRINTNL()
				sCurrentVarsStruct.iEntityCreationStatus = STAGE_SET_UP_RULES_PAGE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
	
ENDFUNC

//PROC DISABLE_WEAPON_WHEEL()
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_NEXT_WEAPON)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_PREV_WEAPON)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
//ENDPROC

ENUM eTESTING_MC_MISSION_MENU_STATE
	eTESTINGMCMISSIONMENUSTATE_DISPLAY_HELP = 0,
	eTESTINGMCMISSIONMENUSTATE_SETUP_MENU,
	eTESTINGMCMISSIONMENUSTATE_SHOW_MENU
ENDENUM

STRUCT STRUCT_TEST_MENU_DATA
	eTESTING_MC_MISSION_MENU_STATE eTestMcMissionMenuState
	BOOL bDisplayedHelp
ENDSTRUCT
STRUCT_TEST_MENU_DATA structTestMcMissionMenuData

PROC RESET_TESTING_MC_MISSION_MENU_DATA(STRUCT_TEST_MENU_DATA &structMenuData)

	STRUCT_TEST_MENU_DATA sTemp
	structMenuData = sTemp
	
	NET_PRINT("[WJK] - Mission Creator - RESET_TESTING_MC_MISSION_MENU_DATA() has been called.")NET_NL()
	
ENDPROC

PROC CLEANUP_TEST()
	
	INT i = 0
	REPEAT MAX_NUM_NETWORK_SCRIPTS_ALLOWED i				
		INT j	
		REPEAT MAX_NUM_NETWORK_SCRIPTS_ALLOWED j
			IF rgTeamRel[i] <> rgTeamRel[j]
				CLEAR_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgTeamRel[i], rgTeamRel[j]) 
			ENDIF
		ENDREPEAT 
	ENDREPEAT
	
	REPEAT MAX_NUM_NETWORK_SCRIPTS_ALLOWED i	
		CLEAR_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()), rgTeamRel[i])
		CLEAR_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgTeamRel[i], GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()))
	ENDREPEAT
		
	REPEAT MAX_NUM_NETWORK_SCRIPTS_ALLOWED i	
		REMOVE_RELATIONSHIP_GROUP(rgTeamRel[i])
	ENDREPEAT

	FOR i = 0 TO 31
		DELETE_PED(tpBotnet[i].pedID)
		tpBotnet[i].iTeam = 0
	ENDFOR
	
	FOR i = 0 TO 3
		iTeams[i] = 0
	ENDFOR

	mnDefaultPeds[0] = DUMMY_MODEL_FOR_SCRIPT
ENDPROC

PROC MAINTAIN_LTS_TEST_MODE()											
	IF PROCESS_PRE_BOTNET(mnDefaultPeds, rgTeamRel)
		PROCESS_TEST_BOTS(mnDefaultPeds, rgTeamRel, tpBotnet, g_FMMC_STRUCT.iTestMyTeam, TRUE, iTeams)
		BLIP_ENEMIES_IN_TEST(tpBotnet)
		MAINTAIN_SNOWBALLS(SnowballData)
		UPDATE_TEST_STATE(tpBotnet)
	ENDIF
ENDPROC

PROC DEAL_WITH_TEST_BEING_ACTIVE(STRUCT_TEST_MENU_DATA &structMenuData)
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_TEST")
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	ENDIF
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		EXIT
	ENDIF
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND is_player_zooming_with_sniper_rifle()
		EXIT
	ENDIF

	// Load menu.
	IF LOAD_MENU_ASSETS()
		
		
		// Process menu state.
		SWITCH structMenuData.eTestMcMissionMenuState
			
			// Wait for player to press up to activate menu. While waiting display help informing player they can do this.
			CASE eTESTINGMCMISSIONMENUSTATE_DISPLAY_HELP				
				
				IF NOT IS_PHONE_ONSCREEN()
					IF NOT structMenuData.bDisplayedHelp
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							NET_PRINT("[WJK] - Mission Creator -called PRINT_HELP_FOREVER(MC_TEST3)")NET_NL()
							PRINT_HELP_FOREVER("MC_TEST")
							structMenuData.bDisplayedHelp = TRUE
						ENDIF
					ENDIF
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
						IF NOT IS_PAUSE_MENU_ACTIVE()
							DISABLE_CELLPHONE(TRUE)
							structMenuData.eTestMcMissionMenuState = eTESTINGMCMISSIONMENUSTATE_SETUP_MENU
							NET_PRINT("[WJK] - Mission Creator - pressed INPUT_FRONTEND_UP, going to eTESTINGMCMISSIONMENUSTATE_SETUP_MENU")NET_NL()
						ELSE
							NET_PRINT("[DSW] - DPAD down pressed but pause menu active!")NET_NL()
						ENDIF
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_TEST")
						CLEAR_HELP()
						structMenuData.bDisplayedHelp = FALSE
					ENDIF
				ENDIF
				bShowMenuHighlight = FALSE
			BREAK
			
			// Setup the menu.
			CASE eTESTINGMCMISSIONMENUSTATE_SETUP_MENU
			
				CLEAR_MENU_DATA()
				SET_MENU_TITLE("FMMCC_TTITLE")
				SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
				SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
				ADD_MENU_ITEM_TEXT(0, "MC_TEST2")
				
				structMenuData.eTestMcMissionMenuState = eTESTINGMCMISSIONMENUSTATE_SHOW_MENU
				
				NET_PRINT("[WJK] - Mission Creator - menu setup, going to eTESTINGMCMISSIONMENUSTATE_SHOW_MENU")NET_NL()
				bShowMenuHighlight = FALSE
			BREAK
			
			// While menu is showing, wait for inputs from player to either exit the menu or exit testing the mission.
			CASE eTESTINGMCMISSIONMENUSTATE_SHOW_MENU
				
				SET_CURRENT_MENU_ITEM_DESCRIPTION("FMMCC_EXTEST")
				// Draw the menu.
				DRAW_MENU()

				// Check for input.
				IF (NOT IS_SCREEN_FADED_OUT() AND NOT IS_SCREEN_FADING_OUT())
					IF NOT IS_PAUSE_MENU_ACTIVE()
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
						OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
							DISABLE_CELLPHONE(FALSE)
							RESET_TESTING_MC_MISSION_MENU_DATA(structMenuData)
							NET_PRINT("[WJK] - Mission Creator - pressed INPUT_FRONTEND_UP, going to eTESTINGMCMISSIONMENUSTATE_DISPLAY_HELP")NET_NL()
						ELIF FFMC_IS_ACCEPT_JUST_PRESSED(sFMMCmenu) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
							RESET_TESTING_MC_MISSION_MENU_DATA(structMenuData)
							REFRESH_MENU(sFMMCMenu)
							g_FMMC_STRUCT.g_b_QuitTest = TRUE
							PRINTLN("QUIT TEST 2")
							NET_PRINT("[WJK] - Mission Creator - pressed INPUT_FRONTEND_ACCEPT, setting g_FMMC_STRUCT.g_b_QuitTest = TRUE.")NET_NL()
						ENDIF
						
					ENDIF
				ENDIF
			BREAK
			
		ENDSWITCH
		
	ELSE
	
		NET_PRINT("[WJK] - Mission Creator - waiting for LOAD_MENU_ASSETS() to return TRUE.")NET_NL()
	
	ENDIF
		
	MAINTAIN_LTS_TEST_MODE()
	
ENDPROC 

PROC PROCESS_PRE_GAME()

	SET_CREATOR_AUDIO(TRUE, FALSE)
	
	SET_BIT(sFMMCdata.iBitSet, bCameraActive)
	
	SETUP_FMMC_SCRIPT(sFMMCMenu, sHCS, sCurrentVarsStruct, TRUE, TRUE)
	INITIALISE_MODEL_ARRAYS(sPedStruct,	sWepStruct, sCapObjStruct, sFMMCmenu, TRUE)
	
	CLEANUP_MENU_ASSETS()
	
	g_FMMC_STRUCT.mnVehicleModel[0] = DUMMY_MODEL_FOR_SCRIPT	//Gauntlet
	g_FMMC_STRUCT.mnVehicleModel[1] = DUMMY_MODEL_FOR_SCRIPT	//Gauntlet
	g_FMMC_STRUCT.mnVehicleModel[2] = DUMMY_MODEL_FOR_SCRIPT	//Gauntlet
	g_FMMC_STRUCT.mnVehicleModel[3] = DUMMY_MODEL_FOR_SCRIPT	//Gauntlet
	g_FMMC_STRUCT.iRespawnVehicle[0] = MAX_NUM_IN_VEHICLE_PALLET
	g_FMMC_STRUCT.iRespawnVehicle[0] = MAX_NUM_IN_VEHICLE_PALLET
	g_FMMC_STRUCT.iRespawnVehicle[0] = MAX_NUM_IN_VEHICLE_PALLET
	g_FMMC_STRUCT.iRespawnVehicle[0] = MAX_NUM_IN_VEHICLE_PALLET
	
	g_FMMC_STRUCT.iMinNumParticipants = 2
	
	SET_OBJECTIVE_TEXT(GET_FILENAME_FOR_AUDIO_CONVERSATION("LTS_OT_CVEH")	, OBJECTIVE_TEXT_PRIMARY_TEXT)
	SET_OBJECTIVE_TEXT(GET_FILENAME_FOR_AUDIO_CONVERSATION("LTS_OT_CVEH")	, OBJECTIVE_TEXT_SECONDARY_TEXT)
	SET_OBJECTIVE_TEXT(GET_FILENAME_FOR_AUDIO_CONVERSATION("LTS_OT_CVEH")	, OBJECTIVE_TEXT_ALT_SECONDARY_TEXT)
	SET_OBJECTIVE_TEXT(GET_FILENAME_FOR_AUDIO_CONVERSATION("LTS_OT_UVEH")	, OBJECTIVE_TEXT_BLIP_NAME)
	SET_OBJECTIVE_TEXT(GET_FILENAME_FOR_AUDIO_CONVERSATION("LTS_OT_LVEH")	, OBJECTIVE_TEXT_TICKER_NAME)
	SET_OBJECTIVE_TEXT(GET_FILENAME_FOR_AUDIO_CONVERSATION("LTS_OT_PVEH")	, OBJECTIVE_TEXT_PLURAL_TICKER_NAME)
	
	iMaxSpawnsPerTeam[0]=FMMC_MAX_TEAMSPAWNPOINTS
	iMaxSpawnsPerTeam[1]=FMMC_MAX_TEAMSPAWNPOINTS
	iMaxSpawnsPerTeam[2]=FMMC_MAX_TEAMSPAWNPOINTS
	iMaxSpawnsPerTeam[3]=FMMC_MAX_TEAMSPAWNPOINTS
	
	g_CreatorsSelDetails.iMaxNumberOfTeams = g_FMMC_STRUCT.iMaxNumberOfTeams 
	//Always display the patrol ranges.
	SET_BIT(sFMMCdata.iBitSet, bDM_Display_Ped_Patrol_range)
	
	//Set up the relationship groups
	SET_UP_RELATIONSHIP_GROUPS(sRGH)
	
	DISABLE_VEHICLE_DISTANTLIGHTS(TRUE)
	
	//Deal with the mapf
	sFMMCmenu.iScriptCreationType = SCRIPT_CREATION_TYPE_LTS
	
	g_FMMC_STRUCT.sFMMCEndConditions[0].iPointsPerKill = 0
	g_FMMC_STRUCT.sFMMCEndConditions[1].iPointsPerKill = 0
	g_FMMC_STRUCT.sFMMCEndConditions[2].iPointsPerKill = 0
	g_FMMC_STRUCT.sFMMCEndConditions[3].iPointsPerKill = 0
	
	g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].fRadius = 10
	g_FMMC_STRUCT.sFMMCEndConditions[1].sBoundsStruct[0].fRadius = 10
	g_FMMC_STRUCT.sFMMCEndConditions[2].sBoundsStruct[0].fRadius = 10
	g_FMMC_STRUCT.sFMMCEndConditions[3].sBoundsStruct[0].fRadius = 10
	
	g_FMMC_STRUCT.sFMMCEndConditions[0].iWantedChange[0] = 1
	g_FMMC_STRUCT.sFMMCEndConditions[1].iWantedChange[0] = 1
	g_FMMC_STRUCT.sFMMCEndConditions[2].iWantedChange[0] = 1
	g_FMMC_STRUCT.sFMMCEndConditions[3].iWantedChange[0] = 1
	
	g_FMMC_STRUCT.iNumPlayersPerTeam[0] = 1
	g_FMMC_STRUCT.iNumParticipants = 7 
	
	sFMMCmenu.fDropOffRadius = CLAMP(sFMMCmenu.fDropOffRadius, 3.0, 100.0)
	sFMMCmenu.fDropOffHeight = CLAMP(sFMMCmenu.fDropOffHeight, 2.0, 100.0)
	
	sFMMCmenu.fRespawnRadius = CLAMP(sFMMCmenu.fRespawnRadius, 20.0, 100.0)
	sFMMCmenu.fRespawnHeight = CLAMP(sFMMCmenu.fRespawnHeight, 5.0, 100.0)
	
	sFMMCMenu.bRespawnSphere = TRUE
	sFMMCMenu.bDropOffSphere = TRUE
	
	g_FMMC_STRUCT.tl63MissionName = ""
	g_FMMC_STRUCT.tl63MissionDecription[0] = ""
	
	g_FMMC_STRUCT.sFMMCEndConditions[0].iObjectiveTimeLimitRule[0] = FMMC_OBJECTIVE_TIME_5_MINUTES
	g_FMMC_STRUCT.sFMMCEndConditions[1].iObjectiveTimeLimitRule[0] = FMMC_OBJECTIVE_TIME_5_MINUTES
	g_FMMC_STRUCT.sFMMCEndConditions[2].iObjectiveTimeLimitRule[0] = FMMC_OBJECTIVE_TIME_5_MINUTES
	g_FMMC_STRUCT.sFMMCEndConditions[3].iObjectiveTimeLimitRule[0] = FMMC_OBJECTIVE_TIME_5_MINUTES
	
	sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_ENEMY_RULE]	 = ciDEFAULT_PED_RULE
	sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_VEHICLE_RULE]		 = ciDEFAULT_VEHICLE_RULE
	sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_OBJECT_RULE] = ciDEFAULT_OBJECT_RULE
	
	SET_BIT(sCapObjStruct.iTeamBitSet, 0)
	SET_BIT(sCapObjStruct.iTeamBitSet, 1)
	SET_BIT(sCapObjStruct.iTeamBitSet, 2)
	SET_BIT(sCapObjStruct.iTeamBitSet, 3)
	
	SET_BIT(sVehStruct.iTeamBitSet, 0)
	SET_BIT(sVehStruct.iTeamBitSet, 1)
	SET_BIT(sVehStruct.iTeamBitSet, 2)
	SET_BIT(sVehStruct.iTeamBitSet, 3)
		
	sFMMCmenu.iCurrentEntTeamRules = sCapObjStruct.iTeamBitSet
	
	INT iTemp
	FOR iTemp = 0 TO 3
		sFMMCmenu.sAssociatedRuleSettings[iTemp].iRule = -1
		sFMMCmenu.sAssociatedRuleSettings[iTemp].iSpawn = 0
		sFMMCmenu.sAssociatedRuleSettings[iTemp].iActionStart = 0
		sFMMCmenu.sAssociatedRuleSettings[iTemp].iScoreRequired = 0
		sFMMCmenu.sAssociatedRuleSettings[iTemp].iAlwaysForceSpawnOnRule = -1
		sFMMCmenu.sAssociatedRuleSettings[iTemp].iRuleEnd = -1
		sFMMCmenu.iAggroRule[iTemp] = -1
		sFMMCMenu.iPedSkipToRuleWhenAggro[iTemp] = -1
	ENDFOR
	FOR iTemp = 0 TO PROP_LIBRARY_MAX_ITEMS-1
		sFMMCmenu.iNumberOfPropsInLibrary[iTemp] = -1
	ENDFOR
	CLEAR_CACHED_AVAILABLE_VEHICLE_COUNTS()
	
	g_FMMC_STRUCT.iTraffic = 2
	g_FMMC_STRUCT.iPedDensity = 2
		
	SET_LONG_BIT(sFMMCMenu.iBitActive,PAN_CAM_PLACE)
	
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciVOTING_ENABLED)
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciGANGS_DISABLED) 
	
	g_FMMC_STRUCT.iMusic = GET_RANDOM_INT_IN_RANGE(0, ciRC_MAX_TYPE_RACE_RADIO)
	STRING sTempRadio
	sTempRadio = GET_RADIO_STATION_NAME(g_FMMC_STRUCT.iMusic)
	g_FMMC_STRUCT.iMusic = GET_HASH_KEY(sTempRadio)
 
	SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(TRUE)
	
	#if IS_DEBUG_BUILD 
	IF IS_ROCKSTAR_DEV() 
		SET_ALLOW_TILDE_CHARACTER_FROM_ONSCREEN_KEYBOARD(TRUE)
	ENDIF
	#ENDIF
	
ENDPROC

PROC WARP_CAMERA_TO_START_LOCATION(FMMC_CAM_DATA &sCamDataPassed, FLOAT fHeight = 40.0)
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
		IF DOES_CAM_EXIST(sCamDataPassed.cam)
			SET_CAM_COORD(sCamDataPassed.cam, g_FMMC_STRUCT.vStartPos+<<0.0,0.0, fHeight>>)
		ENDIF
	ELIF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].vPos)
		IF DOES_CAM_EXIST(sCamDataPassed.cam)
			SET_CAM_COORD(sCamDataPassed.cam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].vPos+<<0.0,0.0, fHeight>>)
		ENDIF
	ENDIF
ENDPROC

PROC RESET_TEAM_TAB_OPTIONS()
	INT iLoop
	INT iTeam
	SELECTED_ITEMS_STRUCT 	sSelection
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS -1)
		g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows = 0
		FOR iLoop = 0 TO (ciMAX_RULES -1)
			g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iLoop]	= sSelection
		ENDFOR
	ENDFOR
ENDPROC
PROC RESET_TEAM_SUB_OPTIONS()
	INT iLoop
	INT iTeam
	INT iSubOptions 
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS -1)
		FOR iLoop = 0 TO (ciMAX_RULES -1)
			FOR iSubOptions = 0 TO (FMMC_MAX_SUB_OPTIONS -1)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iLoop][iSubOptions] = 0	
			ENDFOR
		ENDFOR
	ENDFOR
ENDPROC

PROC SET_FMMC_GLOBALS_TO_MENU_OPTIONS()//(SELECTION_DETAILS_STRUCT &g_CreatorsSelDetails)
	//Team 0
	INT iOrder
	INT iEntity
	INT iTeam
	
	FMMC_GET_LAUNCH_TIMES_FROM_BITSET(g_FMMC_STRUCT.iLaunchTimesBit, g_FMMC_STRUCT.iLaunchStartTime, g_FMMC_STRUCT.iLaunchEndTime)
	PRINTLN("MISSION START TIME - ", g_FMMC_STRUCT.iLaunchStartTime, " END TIME - ", g_FMMC_STRUCT.iLaunchEndTime)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_VOTING_DISABLED) 
		CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciVOTING_ENABLED)
	ELSE
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciVOTING_ENABLED)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_START_AT_ALT_LOC) 
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciSTART_AT_ALTERNATE_LOC)
	ELSE	
		CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciSTART_AT_ALTERNATE_LOC)
	ENDIF
			
	IF g_FMMC_STRUCT.iCashReward > 19
		g_FMMC_STRUCT.iCashReward = 0
	ENDIF
	
	PRINTLN("MISSION TYPE = ", g_FMMC_STRUCT.iMissionSubType, " TUTORIAL = ", g_FMMC_STRUCT.iMissionTutorial)
	
	FOR iTeam = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams -1)
	
		//Convert the number of rules to the numver of rows
		g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iNumberOfTeamRules
		
		FOR iOrder = 0 TO (g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows -1)
			FOR iEntity = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - 1)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntity].iPriority[iTeam] = iOrder
					FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iEntityBitSet, iEntity)
					g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iRule =  GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntity].iRule[iTeam], CREATION_TYPE_PEDS)
				ENDIF
			ENDFOR
			
			FOR iEntity = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntity].iPriority[iTeam] = iOrder				
					FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iEntityBitSet, iEntity)
					g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iRule =  GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntity].iRule[iTeam], CREATION_TYPE_VEHICLES)
				ENDIF
			ENDFOR
			
			FOR iEntity = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntity].iPriority[iTeam] = iOrder
					FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iEntityBitSet, iEntity)
					g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iRule =  GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntity].iRule[iTeam], CREATION_TYPE_OBJECTS)
				ENDIF 
			ENDFOR
			
			FOR iEntity = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] - 1)
				IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEntity].iPriority[iTeam]	= iOrder
					FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iEntityBitSet, iEntity)
					g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iRule =  GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEntity].iRule[iTeam], CREATION_TYPE_GOTO_LOC)
					PRINTLN("g_CreatorsSelDetails.sTeamOptions[", iTeam, "].sSelection[", iOrder, "].iRule = ", g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iRule)
				ENDIF 
			ENDFOR
			
			FOR iEntity = 0 TO (g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam] - 1)
				IF g_FMMC_STRUCT.sPlayerRuleData[iEntity].iPriority[iTeam]	= iOrder
					FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iEntityBitSet, iEntity)
					g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iRule =  GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT.sPlayerRuleData[iEntity].iRule[iTeam], -1)
					PRINTLN("g_CreatorsSelDetails.sTeamOptions[", iTeam, "].sSelection[", iOrder, "].iRule = ", g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iRule)
					
					g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_SELECTED_ENTITES]		= g_FMMC_STRUCT.sPlayerRuleData[iEntity].iPlayerRuleLimit[iTeam]
					PRINTLN("g_FMMC_STRUCT.sPlayerRuleData[",iOrder, "].iPlayerRuleLimit[", iTeam, "] = ", g_FMMC_STRUCT.sPlayerRuleData[iEntity].iPlayerRuleLimit[iTeam])
				ENDIF 
			ENDFOR
			
			//Set up the rules rules
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_TIME]  			= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_SCORE] 			= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveScore[iOrder] 		
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_TARGET_SCORE] 	= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[iOrder] 	
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_NUM_CARRYABLE] 	= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMaxDeliveryEntitiesCanCarry[iOrder] - 1		
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_HACK_SCREEN] 		= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHackScreen[iOrder]
						
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSmallDropOffBitSet, iOrder)
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iExactDropOffBitSet, iOrder)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DROP_OFF_CORONA] 		= 0
			ELIF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSmallDropOffBitSet, iOrder)
			AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iExactDropOffBitSet, iOrder)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DROP_OFF_CORONA] 		= 2
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DROP_OFF_CORONA] 		= 1
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRespawnInVehicleBitset, iOrder)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_RESPAWN_IN_VEH] 		= 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_RESPAWN_IN_VEH] 		= 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPhotoCanBeDead, iOrder)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DEAD_PHOTO] 		= 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DEAD_PHOTO] 		= 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamFailBitset, iOrder)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_FAIL_OBJECTIVE] 		= 1
				PRINTLN("g_CreatorsSelDetails.sSubRowOptions[", iTeam, "].iSelection[", iOrder, "][FMMC_SUB_OPTIONS_FAIL_OBJECTIVE] = 1")
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_FAIL_OBJECTIVE] 		= 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffGPSBitSet, iOrder)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DROP_OFF_GPS] 		= 1
				PRINTLN("g_CreatorsSelDetails.sSubRowOptions[", iTeam, "].iSelection[", iOrder, "][FMMC_SUB_OPTIONS_DROP_OFF_GPS] = 1")
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DROP_OFF_GPS] 		= 0
			ENDIF
			
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_MULTIPLIER] 		= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iOrder].iCaptureMultiplier[iTeam] 			 
		ENDFOR
	ENDFOR

	
ENDPROC

FUNC BOOL RESET_THE_MISSION_UP_SAFELY()

	INT i, i2

	SWITCH iResetState
		CASE RESET_STATE_FADE
			PRINTLN("RESET_STATE_FADE")
			IF IS_FAKE_MULTIPLAYER_MODE_SET() = TRUE 
			    SET_FAKE_MULTIPLAYER_MODE(FALSE)
				ALLOW_MISSION_CREATOR_WARP(NOT IS_PUBLIC_ARENA_CREATOR())
				//PRINTLN("FM_CAPTURE_CREATOR - ALLOW_MISSION_CREATOR_WARP - TRUE - A")
			ENDIF
			IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE 
				iResetState = RESET_STATE_CLEAR
			ENDIF
			RESET_INVISIBLE_OBJECT_POSITIONS(sInvisibleObjects)
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())				
				SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)	
			ENDIF
			IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct, FALSE)
			ENDIF
		BREAK
		
		CASE RESET_STATE_CLEAR
			PRINTLN("RESET_STATE_CLEAR")
			IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE 
				IF NOT NETWORK_IS_GAME_IN_PROGRESS()
					FOR i = 0 TO (FMMC_MAX_PEDS - 1)
						IF DOES_BLIP_EXIST(sPedStruct.biPedBlip[i])
							REMOVE_BLIP(sPedStruct.biPedBlip[i])
						ENDIF
						IF DOES_ENTITY_EXIST(sPedStruct.piPed[i])
							DELETE_PED(sPedStruct.piPed[i])
						ENDIF
					ENDFOR
					FOR i = 0 TO (FMMC_MAX_VEHICLES - 1)
						IF DOES_BLIP_EXIST(sVehStruct.biVehicleBlip[i])
							REMOVE_BLIP(sVehStruct.biVehicleBlip[i])
						ENDIF
						IF DOES_ENTITY_EXIST(sVehStruct.veVehcile[i])
							DELETE_VEHICLE(sVehStruct.veVehcile[i])
						ENDIF
					ENDFOR				
					FOR i = 0 TO (GET_FMMC_MAX_NUM_PROPS() - 1)
						IF DOES_BLIP_EXIST(sPropStruct.biObject[i])
							REMOVE_BLIP(sPropStruct.biObject[i])
						ENDIF
						if DOES_ENTITY_EXIST(sPropStruct.oiObject[i])
							DELETE_OBJECT(sPropStruct.oiObject[i])
						ENDIF
					ENDFOR						
					FOR i = 0 TO (FMMC_MAX_NUM_DYNOPROPS - 1)
						IF DOES_BLIP_EXIST(sDynoPropStruct.biObject[i])
							REMOVE_BLIP(sDynoPropStruct.biObject[i])
						ENDIF
						if DOES_ENTITY_EXIST(sDynoPropStruct.oiObject[i])
							DELETE_OBJECT(sDynoPropStruct.oiObject[i])
						ENDIF
					ENDFOR	
					FOR i = 0 TO (FMMC_MAX_WEAPONS - 1)
						IF DOES_PICKUP_EXIST(sWepStruct.Pickups[i])
							REMOVE_PICKUP(sWepStruct.Pickups[i])
						ENDIF
						IF DOES_BLIP_EXIST(sWepStruct.biWeaponBlip[i])
							REMOVE_BLIP(sWepStruct.biWeaponBlip[i])
						ENDIF
						if DOES_ENTITY_EXIST(sWepStruct.oiWeapon[i])
							DELETE_OBJECT(sWepStruct.oiWeapon[i])
						ENDIF
					ENDFOR
					FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS - 1)
						IF DOES_ENTITY_EXIST(sCapObjStruct.oiObject[i])
							DELETE_OBJECT(sCapObjStruct.oiObject[i])
						ENDIF
						IF DOES_BLIP_EXIST(sCapObjStruct.biObject[i])
							REMOVE_BLIP(sCapObjStruct.biObject[i])
						ENDIF
					ENDFOR
					FOR i = 0 TO (FMMC_MAX_GANG_HIDE_LOCATIONS - 1)
						IF DOES_BLIP_EXIST(sLocStruct.biGoToBlips[i])
							REMOVE_BLIP(sLocStruct.biGoToBlips[i])
						ENDIF
					ENDFOR
					IF DOES_BLIP_EXIST(sStartEndBlips.biStart)
						REMOVE_BLIP(sStartEndBlips.biStart)
					ENDIF							
					IF sStartEndBlips.ciStartType != NULL
						DELETE_CHECKPOINT(sStartEndBlips.ciStartType)
						sStartEndBlips.ciStartType = NULL
					ENDIF
					
					DELETE_BLIPS_AND_CHECKPOINTS()
					
					iResetState = RESET_STATE_WEAPONS
				ENDIF
			ENDIF
		BREAK
		
		CASE RESET_STATE_WEAPONS
			PRINTLN("RESET_STATE_WEAPONS")
			IF CREATE_ALL_CURRENT_PICKUPS(sWepStruct, TRUE, sCurrentVarsStruct)
				iResetState = RESET_STATE_PROPS
			ENDIF
		BREAK
		
		CASE RESET_STATE_PROPS
			PRINTLN("RESET_STATE_PROPS")
			IF CREATE_ALL_CREATOR_PROPS(sPropStruct, iPropModelTimers)
				iResetState = RESET_STATE_DYNOPROPS
			ENDIF
		BREAK
		
		CASE RESET_STATE_DYNOPROPS
			PRINTLN("RESET_STATE_DYNOPROPS")
			IF CREATE_ALL_CREATOR_DYNOPROPS(sDynoPropStruct)
				iResetState = RESET_STATE_VEHICLES
			ENDIF
		BREAK
		
		CASE RESET_STATE_VEHICLES
			PRINTLN("RESET_STATE_VEHICLES")
			IF CREATE_ALL_CREATOR_VEHICLES(sVehStruct, iVehicleModelLoadTimers)
				iResetState = RESET_STATE_CRATES
			ENDIF
		BREAK	
		
		CASE RESET_STATE_CRATES
			PRINTLN("RESET_STATE_CRATES")
			IF CREATE_ALL_CREATOR_VEHICLE_CRATES(sVehStruct)
				iResetState = RESET_STATE_PEDS
			ENDIF			
		BREAK	
		
		CASE RESET_STATE_OBJECTS
			PRINTLN("RESET_STATE_OBJECTS")
			IF CREATE_ALL_CREATOR_OBJECTS(sCapObjStruct, sCurrentVarsStruct)
				iResetState = RESET_STATE_PEDS
			ENDIF			
		BREAK
		
		CASE RESET_STATE_PEDS
			PRINTLN("RESET_STATE_PEDS")
			IF CREATE_ALL_CREATOR_PEDS(sPedStruct, sVehStruct, sFMMCmenu)
				iResetState = RESET_STATE_ATTACH
			ENDIF			
		BREAK
		
		CASE RESET_STATE_ATTACH
			PRINTLN("RESET_STATE_ATTACH")
			FOR i = 0 TO FMMC_MAX_VEHICLES-1
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParent >= 0
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParentType = CREATION_TYPE_VEHICLES
						FMMC_ATTACH_VEHICLE_TO_VEHICLE(sVehStruct.veVehcile[i], sVehStruct.veVehcile[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParent])
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParentType = CREATION_TYPE_OBJECTS
						FMMC_ATTACH_VEHICLE_TO_OBJECT(sVehStruct.veVehcile[i], sCapObjStruct.oiObject[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParent])
					ENDIF
				ENDIF
			ENDFOR
			iResetState = RESET_STATE_TEAM_SPAWNS
		BREAK
		
		CASE RESET_STATE_TEAM_SPAWNS
			PRINTLN("RESET_STATE_TEAM_SPAWNS")		
			FOR i = 0 TO (FMMC_MAX_TEAMS - 1)		
				FOR i2 = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[i] - 1)
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].vPos)		
						IF NOT DOES_BLIP_EXIST(sTeamSpawnStruct[i].biPedBlip[i2])
							CREATE_FMMC_BLIP(sTeamSpawnStruct[i].biPedBlip[i2], g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].vPos, HUD_COLOUR_WHITE, getNameForTeamSpawnBlip(i), 1)
							SET_BLIP_COLOUR(sTeamSpawnStruct[i].biPedBlip[i2], getColourForSpawnBlip(i))			
							CREATE_TEAM_SPAWN_POINT_DECAL(sTeamSpawnStruct[i], i2, FALSE, sCurrentVarsStruct.bCanCreateADecalThisFrame, i)
							
							PRINTLN("CREATED A TEAM SPAWN BLIP AND DECAL ", i)	
							
							RETURN FALSE
						ENDIF
					ENDIF
				ENDFOR
			ENDFOR
			
			iResetState = RESET_STATE_OTHER
			
		BREAK
		
		CASE RESET_STATE_OTHER
			PRINTLN("RESET_STATE_OTHER")	
			REQUEST_STREAMED_TEXTURE_DICT("MPOnMissMarkers")
			REQUEST_STREAMED_TEXTURE_DICT("MPMissMarkers256")
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPOnMissMarkers")
			AND HAS_STREAMED_TEXTURE_DICT_LOADED("MPMissMarkers256")
				HAS_PATCHED_START_LOGO_STATE_SWAPPED(TRUE, 3)				
				
				g_FMMC_STRUCT.iContactChar = CONVERT_CHARACTER_INT_TO_MENU_OPTION(g_FMMC_STRUCT.iContactCharEnum)
								
				BLIP_ALL_FMMC_LOCATIONS_SP(sLocStruct.biGoToBlips, sLocStruct.biGoToBlipCaptureRadius)
				if NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)					
					CREATE_TRIGGER_BLIP_AND_CORONA_WITH_DECAL(sStartEndBlips.biStart, sHCS.hcStartCoronaColour, sStartEndBlips.ciStartType)					
				ELSE
					SET_DM_STARTING_MENU_ITEMS_ACTIVE(sFMMCmenu)
				ENDIF
					
				iResetState = RESET_STATE_FINISH
			
			ENDIF
		BREAK
		CASE RESET_STATE_FINISH
			PRINTLN("RESET_STATE_FINISH")			
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC SET_CORONA_ENTITIES_TO_DUMMY_MODELS()
	INT i
	FOR i = g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles TO FMMC_MAX_VEHICLES - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("SET TO DUMMY VEHICLE MODEL ", i)
		ENDIF
	ENDFOR
	FOR i = g_FMMC_STRUCT_ENTITIES.iNumberOfPeds TO FMMC_MAX_PEDS - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn != DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("SET TO DUMMY PED MODEL ", i)
		ENDIF
	ENDFOR
	FOR i = g_FMMC_STRUCT_ENTITIES.iNumberOfObjects TO FMMC_MAX_NUM_OBJECTS - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn != DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("SET TO DUMMY OBJECT MODEL ", i)
		ENDIF
	ENDFOR
	FOR i = g_FMMC_STRUCT_ENTITIES.iNumberOfProps TO GET_FMMC_MAX_NUM_PROPS() - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("SET TO DUMMY PROP MODEL ", i)
		ENDIF
	ENDFOR
	FOR i = g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps TO FMMC_MAX_NUM_DYNOPROPS - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("SET TO DUMMY DYNOPROP MODEL ", i)
		ENDIF
	ENDFOR
ENDPROC

PROC MAINTAIN_TEST_MISSION_STATE()

	IF IS_BIT_SET(sFMMCdata.iBitSet, biSetUpTestMission)
		bTextSetUp = FALSE
		IF NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(500)
			COMMON_FMMC_SCRIPT_CLEAN_UP(sPedStruct,	sVehStruct,	sWepStruct,	sCapObjStruct,sPropStruct, sDynoPropStruct, sInvisibleObjects, sCurrentVarsStruct, sFMMCmenu, FALSE)			
			DELETE_BLIPS_AND_CHECKPOINTS()
			REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(sFMMCmenu.iEntityCreation)
			SET_CORONA_ENTITIES_TO_DUMMY_MODELS()
		ENDIF
		
		IF (NETWORK_IS_GAME_IN_PROGRESS() = FALSE
		AND NETWORK_IS_SIGNED_ONLINE() = FALSE) OR
		NOT SCRIPT_IS_CLOUD_AVAILABLE()
			NET_NL()NET_PRINT("MAINTAIN_TEST_MISSION_STATE - Player is signed out but heading to a test, cleanup ")
			CLEAR_BIT(sFMMCdata.iBitSet, biSetUpTestMission)
			bMpNeedsCleanedUp = TRUE
			bTestModeControllerScriptStarted = TRUE
			
		ENDIF
		
		IF IS_SCREEN_FADED_OUT()
			IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE
				SET_FAKE_MULTIPLAYER_MODE(TRUE)
				ALLOW_MISSION_CREATOR_WARP(FALSE)
				//PRINTLN("FM_CAPTURE_CREATOR - ALLOW_MISSION_CREATOR_WARP - FALSE - A")
			ENDIF
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Mission_Controller")) = 0
				REQUEST_SCRIPT("FM_Mission_Controller")				
				IF HAS_SCRIPT_LOADED("FM_Mission_Controller")
				AND IS_FAKE_MULTIPLAYER_MODE_SET()	
				AND NETWORK_IS_GAME_IN_PROGRESS() 
					g_FMMC_STRUCT.g_b_QuitTest = FALSE
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_LTS_MISSION
					
					NET_NL()NET_PRINT("fm_capture_creator.sch MAINTAIN_TEST_MISSION_STATE - calling USING_MISSION_CREATOR(TRUE)  ")	
					USING_MISSION_CREATOR(TRUE) //Added for bug 1788377 so the stats stop tracking while testing CTF. BC: 17/03/2014
					GlobalplayerBD[0].iGameState = MAIN_GAME_STATE_RUNNING
					START_NEW_SCRIPT("FM_Mission_Controller", MULTIPLAYER_FREEMODE_STACK_SIZE)
					PRINTLN("----------------------------------------------------------------------------")
					PRINTLN("--                                                                        --")
					PRINTLN("--                                                                        --")
					PRINTLN("--                                                                        --")
					PRINTLN("--                   LAUNCHING MISSION CONTROLLER                          --")
					PRINTLN("--                                                                        --")
					PRINTLN("--                                                                        --")
					PRINTLN("--                                                                        --")
					PRINTLN("----------------------------------------------------------------------------")
					SET_SCRIPT_AS_NO_LONGER_NEEDED("FM_Mission_Controller")
					SET_MENU_OPTIONS_TO_FMMC_GLOBALS()
					SET_BIT(sFMMCdata.iBitSet, biSetUpTestMission)
					iDoorSetupStage = 0
					REMOVE_SCENARIO_BLOCKING_AREAS()
					SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(FALSE)
					SET_MOBILE_PHONE_RADIO_STATE(FALSE)
					
					#IF IS_DEBUG_BUILD
					PRINT_FMMC_DEBUGDATA(FALSE,TRUE)     
					#ENDIF
					IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
						SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct, TRUE, TRUE)
					ENDIF
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
						SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), FALSE)
						SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)	
					ENDIF
					CLEAR_BIT(sFMMCdata.iBitSet, biSetUpTestMission)
					SET_CREATOR_AUDIO(FALSE, FALSE)
					bMpNeedsCleanedUp = TRUE
					bTextSetUp = FALSE
										
					// SETTING UP PLAYERS HAIR FOR CELEBRATION SCREEN
					IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_USING_HAIR_SA_STAT)
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMyCurrentHair 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA) 
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHairTintPrime 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT)
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHairTintSecondary 	= GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT)
					ELSE
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMyCurrentHair 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO) 
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHairTintPrime 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT)
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHairTintSecondary 	= GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT)
					ENDIF
					
					PED_COMP_NAME_ENUM eMyCurrentHair = INT_TO_ENUM(PED_COMP_NAME_ENUM, GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMyCurrentHair)
					IF (eMyCurrentHair != DUMMY_PED_COMP)
						PED_COMP_NAME_ENUM eGRHairItem = DUMMY_PED_COMP
						IF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01)
							eGRHairItem = GET_MALE_HAIR(eMyCurrentHair)
						ELIF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01)
							eGRHairItem = GET_FEMALE_HAIR(eMyCurrentHair)
						ENDIF
						
						IF (eGRHairItem != DUMMY_PED_COMP)
						AND (eMyCurrentHair != eGRHairItem)
							PRINTLN("[MAINTAIN_STATE][LTS] gr_hair: replacing hair enum ", eMyCurrentHair, " with gunrunning hair enum ", eGRHairItem)
							GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMyCurrentHair = ENUM_TO_INT(eGRHairItem)
						ENDIF
					ENDIF
				ELSE
				
					IF HAS_SCRIPT_LOADED("FM_Mission_Controller") = FALSE
						NET_NL()NET_PRINT("Trying to launch FM_Mission_Controller but HAS_SCRIPT_LOADED(FM_Mission_Controller) = FALSE ")
					ENDIF
					
					IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE
						NET_NL()NET_PRINT("Trying to launch FM_Mission_Controller but IS_FAKE_MULTIPLAYER_MODE_SET = FALSE ")
					ENDIF
					
					IF NETWORK_IS_GAME_IN_PROGRESS() = FALSE
						NET_NL()NET_PRINT("Trying to launch FM_Mission_Controller but NETWORK_IS_GAME_IN_PROGRESS = FALSE ")
					ENDIF
					
					
				
				ENDIF
			ELSE
				NET_NL()NET_PRINT("Trying to launch FM_Mission_Controller but number already running is ")NET_PRINT_INT(GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Mission_Controller")))
				
			ENDIF
		ENDIF
	ELSE
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF bTextSetUp = FALSE
				Initialise_MP_Objective_Text()
				Initialise_MP_Communications()
				NET_PRINT("init objective text and comms called") NET_NL()
				bTextSetUp = TRUE
			ENDIF
		ENDIF
		IF bTestModeControllerScriptStarted = FALSE
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Mission_Controller")) >= 1
				PRINTLN("----------------------------------------------------------------------------")
				PRINTLN("--                                                                        --")
				PRINTLN("--                                                                        --")
				PRINTLN("--                                                                        --")
				PRINTLN("--       GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH          --")
				PRINTLN("--                                                                        --")
				PRINTLN("--                                                                        --")
				PRINTLN("--                                                                        --")
				PRINTLN("----------------------------------------------------------------------------")
				bTestModeControllerScriptStarted = TRUE
				IF DOES_BLIP_EXIST(sStartEndBlips.biStart)
					REMOVE_BLIP(sStartEndBlips.biStart)
				ENDIF
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				ENDIF
//				DISABLE_CELLPHONE(FALSE) //Don't turn the cellphone back on when testing a CTF. Bug 1788513 BC: 17/03/2014
			ENDIF
		ELSE
			IF bMpNeedsCleanedUp = TRUE
				IF bMpModeCleanedUp = FALSE
					// Do menu here.
					DEAL_WITH_TEST_BEING_ACTIVE(structTestMcMissionMenuData)
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Mission_Controller")) = 0
						RESET_TESTING_MC_MISSION_MENU_DATA(structTestMcMissionMenuData)
						SET_FAKE_MULTIPLAYER_MODE(FALSE)
						ALLOW_MISSION_CREATOR_WARP(NOT IS_PUBLIC_ARENA_CREATOR())
						iDoorSetupStage = 0
						//PRINTLN("FM_CAPTURE_CREATOR - ALLOW_MISSION_CREATOR_WARP - TRUE - B")
						IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE
							IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
							AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
								SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							ENDIF
							CLEANUP_LEADERBOARD_CAM()
							
							iTestFailBitSet = 0
    						SET_FAIL_BIT_SET(iTestFailBitSet)

							REFRESH_MENU(sFMMCMenu)
							bMpModeCleanedUp = TRUE
							INIT_CREATOR_BUDGET() // 1787432
							// Turn off the leaderboard camera.
							CLEANUP_LEADERBOARD_CAM()
							IF DOES_CAM_EXIST(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
								IF IS_CAM_ACTIVE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
									IF IS_CAM_RENDERING(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
										RENDER_SCRIPT_CAMS(FALSE, FALSE, 1000, FALSE, TRUE)
										SET_CAM_ACTIVE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam, FALSE)
									ENDIF
								ENDIF
								DESTROY_CAM(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam, TRUE)
							ENDIF
							PRINTLN("----------------------------------------------------------------------------")
							PRINTLN("--                                                                        --")
							PRINTLN("--                                                                        --")
							PRINTLN("--                                                                        --")
							PRINTLN("--                       MODE CLEANED UP = TRUE                           --")
							PRINTLN("--                                                                        --")
							PRINTLN("--                                                                        --")
							PRINTLN("--                                                                        --")
							PRINTLN("----------------------------------------------------------------------------")
						ENDIF 
					ELSE
						IF NOT GET_PLAYER_INVINCIBLE(PLAYER_ID())
							SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
						ENDIF
					ENDIF
				ELSE
					IF NETWORK_IS_GAME_IN_PROGRESS() = FALSE
						SETUP_FMMC_SCRIPT(sFMMCMenu, sHCS, sCurrentVarsStruct, TRUE)
						CLEAR_BIT(sFMMCdata.iBitSet, biTestMissionActive)
						CLEAR_BIT(sFMMCdata.iBitSet, bTestHelpTextDone)
						CLEANUP_TEST()
						bMpNeedsCleanedUp = FALSE
						bTestModeControllerScriptStarted = FALSE
						bTextSetUp = FALSE
						bMpModeCleanedUp = FALSE
						SET_CREATOR_AUDIO(TRUE, FALSE)
						sCurrentVarsStruct.iEntityCreationStatus = STAGE_DEAL_WITH_RECREATING_MISSION
						sCurrentVarsStruct.creationStats.iTimesTestedLoc++
						sCurrentVarsStruct.creationStats.bMadeAChange = FALSE	
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_NO_ACTIVITY
						sFMMCendStage.bMajorEditOnLoadedMission = TRUE
					ELSE
						PRINTLN("Mission creator - MAINTAIN_TEST_MISSION_STATE - NETWORK_IS_GAME_IN_PROGRESS() = TRUE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


#IF IS_DEBUG_BUILD
PROC MAINTAIN_SETTING_RETAINED_BITSETS()
	
ENDPROC

SAVE_OUT_UGC_PLAYER_DATA_VARS sSaveOutVars
#ENDIF

PROC DO_LTS_DOOR_SELECTION()
	DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfDoors,  FMMC_MAX_NUM_DOORS,	"FMMC_DOORS_M")
ENDPROC

PROC MAINTAIN_PLACED_TEAM_CAM_BLIPS()
	INT i 
	TEXT_LABEL_15 blipName
	
	FOR i = 0 to FMMC_MAX_TEAMS - 1
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][0].vPos)
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[i] > 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].vStartPos = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][0].vPos
			g_FMMC_STRUCT.sFMMCEndConditions[i].fStartHeading = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][0].fHead
		ELIF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
			g_FMMC_STRUCT.sFMMCEndConditions[i].vStartPos = g_FMMC_STRUCT.vStartPos
		ELSE
			g_FMMC_STRUCT.sFMMCEndConditions[i].vStartPos = <<0,0,0>>
		ENDIF
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][0].vPos)
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[i].vCameraPos)
			IF DOES_BLIP_EXIST(bTeamCamPanBlip[i])
				IF GET_DISTANCE_BETWEEN_COORDS(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][0].vPos, g_FMMC_STRUCT.sFMMCEndConditions[i].vCameraPos) > 100
				OR NOT ARE_VECTORS_EQUAL(GET_BLIP_COORDS(bTeamCamPanBlip[i]), g_FMMC_STRUCT.sFMMCEndConditions[i].vCameraPos)
					REMOVE_BLIP(bTeamCamPanBlip[i])
				ENDIF
			ELSE
				IF GET_DISTANCE_BETWEEN_COORDS(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][0].vPos, g_FMMC_STRUCT.sFMMCEndConditions[i].vCameraPos) < 100
					CREATE_FMMC_BLIP(bTeamCamPanBlip[i], g_FMMC_STRUCT.sFMMCEndConditions[i].vCameraPos, HUD_COLOUR_WHITE, "FMMC_B_14", 1)
					SET_BLIP_SPRITE(bTeamCamPanBlip[i], RADAR_TRACE_CAMERA)
					SET_BLIP_COLOUR(bTeamCamPanBlip[i], getColourForSpawnBlip(i))
					blipName = "FMMC_B_TC"
					blipName += i
					SET_BLIP_NAME_FROM_TEXT_FILE(bTeamCamPanBlip[i], blipName)
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(bTeamCamPanBlip[i])
				REMOVE_BLIP(bTeamCamPanBlip[i])
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC CHECK_NEEDS_RETESTED()
	INT iTeam
	//Check to see if we need to clear the tested bit sets
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		IF sFMMCendStage.bMissionNeedsRetested[iTeam]
			IF IS_BIT_SET(g_FMMC_STRUCT.biTeamTestComplete, iTeam)
				CLEAR_BIT(g_FMMC_STRUCT.biTeamTestComplete, iTeam)
				PRINTLN("CHECK_NEEDS_RETESTED - Clearing team ", iTeam, " as tested")
			ENDIF
			sFMMCendStage.bMissionNeedsRetested[iTeam] = FALSE
		ENDIF
	ENDFOR
ENDPROC

PROC DISPLAY_PLAY_AREA_BOUNDS()
	IF g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].iType = 0
		FLOAT fRadius = g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].fRadius
		INT iAlpha = 50
		IF sFMMCmenu.sActiveMenu = eFmmc_PLAY_AREA_BOUNDS
			iAlpha = 75
		ENDIF
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos1)
			DRAW_MARKER(MARKER_SPHERE, g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos1, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, <<fRadius, fRadius, fRadius>>, 255, 255, 255, iAlpha)
		ELSE
			IF sFMMCmenu.sActiveMenu = eFmmc_PLAY_AREA_BOUNDS
				IF sFMMCmenu.sActiveMenu = eFmmc_PLAY_AREA_BOUNDS
					DRAW_MARKER(MARKER_SPHERE, sCurrentVarsStruct.vCoronaPos + <<0,0,1>>, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, <<fRadius, fRadius, fRadius>>, 255, 255, 255, iAlpha)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos)
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos2) 
				DRAW_DEBUG_ANGLED_AREA(g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos1, g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos2, g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].fWidth, 255, 255, 255, 50)
				//DRAW_ANGLED_AREA_FROM_FACES(g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos1, g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos2, g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].fWidth, 255, 255, 255, 128)
			ELSE
				DRAW_DEBUG_ANGLED_AREA(g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos1, sCurrentVarsStruct.vCoronaPos, g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].fWidth, 255, 255, 255, 50)
				//DRAW_ANGLED_AREA_FROM_FACES(g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos1, sCurrentVarsStruct.vCoronaPos, g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].fWidth, 255, 255, 255, 128)
			ENDIF
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC HANDLE_PLACEMENT_OF_BOUNDS()
	INT i
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FMMCC_TEST3")
		EXIT
	ENDIF
		
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR FMMC_IS_MAP_ACCEPT_JUST_PRESSED(sFMMCmenu)
		IF (g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].iType = 0 AND (IS_ROCKSTAR_DEV() AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 2) )
		OR (g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].iType = 0 AND ((NOT IS_ROCKSTAR_DEV()) AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1))
		OR (g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].iType = 1 AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 4)
			sCurrentVarsStruct.iMenuState = MENU_STATE_OBJECTIVE_TEXT
			sFMMCmenu.iObjectiveTextType =  8
			sFMMCendStage.bMajorEditOnLoadedMission = TRUE
		ELSE
			IF sCurrentVarsStruct.bCoronaOnWater
				sCurrentVarsStruct.sFailReason = "FMMC_ER_009"
				sCurrentVarsStruct.bDisplayFailReason = TRUE
				sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
			ELIF IS_PLACEMENT_IN_A_RESTRICTED_AREA(sCurrentVarsStruct.vCoronaPos)
				sCurrentVarsStruct.sFailReason = "FMMC_ER_024"
				sCurrentVarsStruct.bDisplayFailReason = TRUE
				sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
			ELSE
				IF IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos1) 
				OR g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].iType = 0
				
					FOR i = 0 TO FMMC_MAX_TEAMS-1
						g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].vPos1 = sCurrentVarsStruct.vCoronaPos
						g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].vPos = sCurrentVarsStruct.vCoronaPos
					ENDFOR
					
					FMMC_REMOVE_ALL_TEAM_START_POINTS(sTeamSpawnStruct)
					CLEAR_INVISIBLE_OBJECT_POSITIONS(sInvisibleObjects)
					
					IF DOES_BLIP_EXIST(biBounds)
						REMOVE_BLIP(biBounds)
					ENDIF
				ELSE
					IF g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos2.x = 0 AND g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos2.y = 0
						FOR i = 0 TO FMMC_MAX_TEAMS-1
							g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].vPos2 = sCurrentVarsStruct.vCoronaPos
						ENDFOR
					ELSE
						FOR i = 0 TO FMMC_MAX_TEAMS-1
							g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].vPos1 = sCurrentVarsStruct.vCoronaPos
							g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].vPos2 = <<0,0,0>>
						ENDFOR
						
						IF DOES_BLIP_EXIST(biBounds)
							REMOVE_BLIP(biBounds)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos1) 
	OR (g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].iType = 1
		AND IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos2) )
		FOR i = 0 TO FMMC_MAX_TEAMS-1
			CLEAR_BIT(g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayAreaBitset, 0)
		ENDFOR
	ELSE
		FOR i = 0 TO FMMC_MAX_TEAMS-1
			SET_BIT(g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayAreaBitset, 0)
		ENDFOR
	ENDIF
	
	REFRESH_MENU(sFMMCMenu)
ENDPROC

PROC MAINTAIN_PLAY_AREA_BLIPS()
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos1) 
		IF NOT DOES_BLIP_EXIST(biBounds)
			biBounds = ADD_BLIP_FOR_RADIUS(g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos1, g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].fRadius)
			SET_BLIP_ALPHA(biBounds, 65)
			SET_BLIP_COLOUR(biBounds, BLIP_COLOUR_RED)
			SHOW_HEIGHT_ON_BLIP(biBounds, FALSE)
		ELSE
			SET_BLIP_COORDS(biBounds, g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos1)
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_PLAY_AREA_BOUNDS
		IF NOT DOES_BLIP_EXIST(biBounds)
			biBounds = ADD_BLIP_FOR_RADIUS(sCurrentVarsStruct.vCoronaPos, g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].fRadius)
			SET_BLIP_ALPHA(biBounds, 65)
			SET_BLIP_COLOUR(biBounds, BLIP_COLOUR_RED)
			SHOW_HEIGHT_ON_BLIP(biBounds, FALSE)
		ELSE
			SET_BLIP_COORDS(biBounds, sCurrentVarsStruct.vCoronaPos)
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(biBounds)
			REMOVE_BLIP(biBounds)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PLAY_AREA_BOUNDS()
	IF sFMMCmenu.sActiveMenu = eFmmc_PLAY_AREA_BOUNDS
		HANDLE_PLACEMENT_OF_BOUNDS()
	ENDIF	
	DISPLAY_PLAY_AREA_BOUNDS()
	MAINTAIN_PLAY_AREA_BLIPS()
	IF NOT ARE_PLAY_AREA_BOUNDS_SET_FOR_LTS()
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0] > 0
		OR g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1] > 0
		OR g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[2] > 0
		OR g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[3] > 0
			FMMC_REMOVE_ALL_TEAM_START_POINTS(sTeamSpawnStruct)
		ENDIF
	ENDIF
ENDPROC

PROC SETUP_LAST_TEAM_STANDING_MISSION_DATA()
	INT i, iTeam
	
	g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_HIGHEST

	FOR i = 0 TO FMMC_MAX_TEAMS-1
		g_FMMC_STRUCT.sPlayerRuleData[0].iRule[i] = FMMC_OBJECTIVE_LOGIC_KILL_PLAYERS
		g_FMMC_STRUCT.sPlayerRuleData[0].iPriority[i] = 0
		g_FMMC_STRUCT.iNumberOfPlayerRules[i] = 1
		g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayerLives = ciLEGACY__NUMBER_OF_LIVES_1_LIFE
		
		SET_OBJECTIVE_TEXT(GET_FILENAME_FOR_AUDIO_CONVERSATION("LTS_GEN_BND"), 8)
				
		IF IS_ROCKSTAR_DEV()
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].iType = 0
			
			FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
				SET_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayAreaBitset, 0)
			ENDFOR
		ENDIF
	ENDFOR
	
	IF g_FMMC_STRUCT.iAudioScore < MUSIC_TYPE_NEW_RANDOM
		g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_NEW_RANDOM
	ENDIF
		
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciNO_POINTS_HUD) 
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciTEAM_VS_RADAR) 
ENDPROC

SCRIPT		
	// JA: Adjust params needed to load in requested mission
	IF IS_PAUSE_MENU_REQUESTING_TO_EDIT_A_MISSION()
		PRINTLN("[JA@PAUSEMENU] Set up which file to load into creator", GET_PAUSE_MENU_MISSION_FILE_TO_LOAD())
	
		sFMMCendStage.iLoadDeleteStage  = ciLOAD_DELETE_STAGE_LOAD
		sFMMCendStage.iMenuReturn		= GET_PAUSE_MENU_MISSION_FILE_TO_LOAD()
	ENDIF
	
	CLEAN_UP_PAUSE_MENU_MISSION_CREATOR_DATA()
	
	INT i
	FOR i = 0 TO 30
		SET_BIT(sFMMCmenu.iOptionsMenuBitSet, i)
	ENDFOR
	
	PROCESS_PRE_GAME()
	INIT_CREATOR_BUDGET()

	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	sFMMCmenu.bAllowMouseInput = TRUE
	sFMMCmenu.bAllowMouseSelection = TRUE
	
	// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()
	#ENDIF
	
	#IF IS_DEBUG_BUILD		
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_useNewUgcSaveSystem")
			g_buseNewUgcSaveSystem = TRUE
		ENDIF
		SET_UP_FMMC_SKIP_NAMES(sFMMCendStage.SkipMenu)
	#ENDIF
	
	sCurrentVarsStruct.creationStats.iStartTimeMS = GET_GAME_TIMER()
	sCurrentVarsStruct.creationStats.iCurrentTimeMS = GET_GAME_TIMER()
	
	REQUEST_IPL("CS1_02_cf_onmission1")
	REQUEST_IPL("CS1_02_cf_onmission2")
	REQUEST_IPL("CS1_02_cf_onmission3")
	REQUEST_IPL("CS1_02_cf_onmission4")
	
	g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_HIGHEST
	
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciNO_POINTS_HUD) 

	g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
	g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_LTS
	IF g_FMMC_STRUCT.iMaxNumberOfTeams < 2
		g_FMMC_STRUCT.iMaxNumberOfTeams = 2
	ENDIF
	IF g_FMMC_STRUCT.iNumParticipants < 1
		g_FMMC_STRUCT.iNumParticipants = 1
	ENDIF
	
	SET_GLOBALS_TO_DEFAULS_FOR_LTS()
	#IF IS_DEBUG_BUILD 
	INITIALIZE_ADDITIONAL_DEBUG_AND_WIDGETS()
	#ENDIF	
		
	WHILE TRUE		
		PROCESS_SINGLEPLAYER_MODEL_ON_CREATOR(bIsCreatorModelSetSPTC,bIsMaleSPTC)
		IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
			sCurrentVarsStruct.creationStats.iCreatingTimeMS += GET_GAME_TIMER() - sCurrentVarsStruct.creationStats.iCurrentTimeMS
		ENDIF
		sCurrentVarsStruct.creationStats.iCurrentTimeMS = GET_GAME_TIMER()
		
		RESET_UP_HELP()
		IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
			IF sFMMCmenu.iSelectedTeam != 0
				sFMMCmenu.iSelectedTeam = 0
			ENDIF
			IF sFMMCmenu.iSelectedTeam != 0
				sFMMCmenu.iSelectedTeam = 0
			ENDIF
		ENDIF
				
		FOR i = 1 TO FMMC_MAX_TEAMS-1
			IF NOT ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sFMMCEndConditions[i].tl63Objective[0], g_FMMC_STRUCT.sFMMCEndConditions[0].tl63Objective[0]) 
				g_FMMC_STRUCT.sFMMCEndConditions[i].tl63Objective[0] = g_FMMC_STRUCT.sFMMCEndConditions[0].tl63Objective[0]
			ENDIF
			IF NOT ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sFMMCEndConditions[i].tl63Objective1[0], g_FMMC_STRUCT.sFMMCEndConditions[0].tl63Objective1[0]) 
				g_FMMC_STRUCT.sFMMCEndConditions[i].tl63Objective1[0] = g_FMMC_STRUCT.sFMMCEndConditions[0].tl63Objective1[0]
			ENDIF
			IF NOT ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sFMMCEndConditions[i].tl63Objective2[0], g_FMMC_STRUCT.sFMMCEndConditions[0].tl63Objective2[0]) 
				g_FMMC_STRUCT.sFMMCEndConditions[i].tl63Objective2[0] = g_FMMC_STRUCT.sFMMCEndConditions[0].tl63Objective2[0]
			ENDIF
			IF NOT ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sFMMCEndConditions[i].tl23ObjBlip[0], g_FMMC_STRUCT.sFMMCEndConditions[0].tl23ObjBlip[0]) 
				g_FMMC_STRUCT.sFMMCEndConditions[i].tl23ObjBlip[0] = g_FMMC_STRUCT.sFMMCEndConditions[0].tl23ObjBlip[0]
			ENDIF
			IF NOT ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sFMMCEndConditions[i].tl23ObjSingular[0], g_FMMC_STRUCT.sFMMCEndConditions[0].tl23ObjSingular[0]) 
				g_FMMC_STRUCT.sFMMCEndConditions[i].tl23ObjSingular[0] = g_FMMC_STRUCT.sFMMCEndConditions[0].tl23ObjSingular[0]
			ENDIF
			IF NOT ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sFMMCEndConditions[i].tl23ObjPlural[0], g_FMMC_STRUCT.sFMMCEndConditions[0].tl23ObjPlural[0]) 
				g_FMMC_STRUCT.sFMMCEndConditions[i].tl23ObjPlural[0] = g_FMMC_STRUCT.sFMMCEndConditions[0].tl23ObjPlural[0]
			ENDIF
		ENDFOR
		
		#IF IS_DEBUG_BUILD
		
		
		
		IF b948049Repo
			DATAFILE_ARRAY Array
			DATAFILE_DICT dfdMainDict
			DATAFILE_DICT ArrayDict
			CLEANUP_DATA_FILE()
			DATAFILE_CREATE()
			dfdMainDict = DATAFILE_GET_FILE_DICT()
			Array = DATADICT_CREATE_ARRAY(dfdMainDict, "hello_world")
			INT iCount = 0
			ArrayDict = DATAARRAY_CREATE_DICT(Array, iCount)
			IF  ArrayDict = NULL
				PRINTLN("ArrayDict = NULL")
			ENDIF	
			CLEANUP_DATA_FILE()
			b948049Repo = FALSE
		ENDIF
		
		INT iTemp = FMMC_MAX_TEAMS-1
				
		IF bGetSpPlaylist
			IF GET_UGC_BY_CONTENT_ID(sGetUGC_content, "",UGC_TYPE_GTA5_MISSION_PLAYLIST, TRUE, TRUE)
				bGetSpPlaylist = FALSE
				PRINTLN("TRY_TO_LOAD_PLAYLIST ->GET_UGC_BY_CONTENT_ID")
			ENDIF
		ENDIF
		
		IF bDealWithNewSave
		
			STRING stWidgetContents 
			STRING OverWriteName = ""
			stWidgetContents = GET_CONTENTS_OF_TEXT_WIDGET(twID)
			IF bVersionMissionNew
			stWidgetContents = "KH5P0cE-1U-VaYpT6RqktQ"
			ENDIF
			IF bOverWriteMissionNew
			stWidgetContents = "gwECCtApW0ebJyQzEFOZ5A"
			ENDIF
			
			IF DEAL_WITH_SAVING_UGC_SERVER(sGetUGC_content, sFMMCendStage, sCurrentVarsStruct.creationStats, bPublishMissionNew, bVersionMissionNew, bOverWriteMissionNew, stWidgetContents, OverWriteName)
				sFMMCendStage.iEndStage = 0
				sFMMCendStage.iSaveStage = 0				
				sFMMCendStage.iPublishStage = 0				
				sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				sCurrentVarsStruct.bSwitching = FALSE
				sCurrentVarsStruct.stiScreenCentreLOS = NULL
				sFMMCendStage.iEndStage = 0
				sFMMCendStage.dataStruct.iLoadStage = 0
				sFMMCendStage.iPublishConformationStage = 0
				sFMMCendStage.iPublishStage = 0
				sFMMCendStage.iButtonBitSet = 0
				bDealWithNewSave = FALSE
			ENDIF
			
		ENDIF
		
		IF bCopyContentByID
			IF COPY_UGC_BY_CONTENT_ID(sGetUGC_content, "gwECCtApW0ebJyQzEFOZ5A")
				bCopyContentByID = FALSE
			ENDIF
		ENDIF
			
		IF bEndCreator 
		sCurrentVarsStruct.iEntityCreationStatus = STAGE_SET_UP_RULES_PAGE	
		bEndCreator = FALSE
		ENDIF
		MAINTAIN_SETTING_RETAINED_BITSETS()
		sGetUGC_content.iPublishedSelection = iPublishedSelection1
		IF bGetMyUGC
			IF GET_UGC_CONTENT_BY_TYPE(sGetUGC_content, iTemp, iContentType, 0, INT_TO_ENUM(UGC_CATEGORY, iCATEGORYtype), INT_TO_ENUM(UGC_TYPE, iUGCtype))
				#IF IS_DEBUG_BUILD
					IF bGetHeader1
						SET_UP_FMMC_SKIP_NAMES(sFMMCendStage.SkipMenu, FALSE, TRUE)
					ENDIF
				#ENDIF
				bGetMyUGC = FALSE
			ENDIF
		ENDIF
		
		IF bLoadOldData
			STRING stWidgetContents 
			stWidgetContents = GET_CONTENTS_OF_TEXT_WIDGET(twID)
			IF IS_STRING_NULL(stWidgetContents)
				bLoadOldData = FALSE
			ENDIF
			IF LOAD_DATA_OLD(dataStruct, PLAYER_ID(), 0, "RSN_BobbyW_2", stWidgetContents)
				bLoadOldData = FALSE
			ENDIF	
		ENDIF
		
		IF bGetMyPlaylists
			IF GET_HEADER_DATA_ROCKSTAR_CREATED(sGetUGC_content, 0, UGC_TYPE_GTA5_MISSION_PLAYLIST, MAX_NUMBER_FMMC_SAVES)
				bGetMyPlaylists = FALSE
			ENDIF
		ENDIF
		
		IF bGetMyPlaylistsDetails
			//
			//IF PUBLISH_UGC_BY_CONTENT_ID(sGetUGC_content , "512fa42e8fc4321dd4b8c509")
			//IF SAVE_MISSION_TO_UGC_SERVER(sGetUGC_content, sFMMCendStage)
			//IF GET_UGC_BY_CONTENT_ID(sGetUGC_content, "511e02068fc432101c8b2a82", UGC_TYPE_GTA5_MISSION_PLAYLIST)//g_sLoadedPlaylistDetails.tl31szContentID[iPlayList])
				bGetMyPlaylistsDetails = FALSE
			//ENDIF
		ENDIF
		
		IF bGetRockstarCandidateUGC
//			STRING stWidgetContents = GET_CONTENTS_OF_TEXT_WIDGET(twID)
//			IF IS_STRING_NULL(stWidgetContents)
//				stWidgetContents = "510907328fc43205fc3ca8cb"
//			ENDIF
			IF SAVE_OUT_UGC_PLAYER_DATA("512f9751235bdb12a46ee3ed", sSaveOutVars)
				bGetRockstarCandidateUGC = FALSE
			ENDIF
		ENDIF
		
		IF (bGetUGC_byID
		OR bGetUGC_byIDWithString)
		AND NOT bLoadFromOtherUaer
			STRING stWidgetContents 
			IF bGetUGC_byIDWithString
				stWidgetContents = GET_CONTENTS_OF_TEXT_WIDGET(twID)
				IF IS_STRING_NULL(stWidgetContents)
					stWidgetContents = "510907328fc43205fc3ca8cb"
				ENDIF
			ELSE
				stWidgetContents = "510907328fc43205fc3ca8cb"
			ENDIF
			IF GET_UGC_BY_CONTENT_ID(sGetUGC_content, stWidgetContents )			
				bGetUGC_byID = FALSE
				bGetUGC_byIDWithString = FALSE
			ENDIF
		ENDIF
		
		IF bGetUGC_byIDWithString
		AND bLoadFromOtherUaer
			STRING stWidgetContents 
			stWidgetContents = GET_CONTENTS_OF_TEXT_WIDGET(twID)
			IF IS_STRING_NULL(stWidgetContents)
				bGetUGC_byIDWithString = FALSE
			ENDIF
			IF GET_UGC_BY_CONTENT_ID(sGetUGC_content, stWidgetContents)			
				bGetUGC_byIDWithString = FALSE
				CORRECT_MAX_FOR_MENUS_FROM_UGC(g_FMMC_STRUCT.iMissionType, g_FMMC_STRUCT.iNumParticipants, g_FMMC_STRUCT.iRaceType)
				sCurrentVarsStruct.iEntityCreationStatus = STAGE_DEAL_WITH_LOADING
			ENDIF
		ENDIF
		
		IF bDelete1
			STRING stWidgetContents 
			stWidgetContents = GET_CONTENTS_OF_TEXT_WIDGET(twID)
			IF IS_STRING_NULL(stWidgetContents)
				bDelete1 = FALSE
			ENDIF
			IF DELETE_UGC_BY_CONTENT_ID(sGetUGC_content, stWidgetContents)
				bDelete1 = FALSE
			ENDIF	
		ENDIF
		
		IF bGetChallenges
			IF GET_UGC_CONTENT_BY_TYPE( sGetUGC_content, iTemp, ciUGC_GET_GET_BY_CATEGORY, 0, UGC_CATEGORY_ROCKSTAR_VERIFIED_CANDIDATE, UGC_TYPE_GTA5_CHALLENGE, FMMC_MAX_PLAY_LIST_LENGTH)
				bGetChallenges = FALSE
			ENDIF
		ENDIF
		IF bGetAChallenge
			STRING stWidgetContents 
			stWidgetContents = GET_CONTENTS_OF_TEXT_WIDGET(twID)
			IF IS_STRING_NULL(stWidgetContents)
				bGetAChallenge = FALSE
			ENDIF
			IF GET_UGC_BY_CONTENT_ID(sGetUGC_content, stWidgetContents, UGC_TYPE_GTA5_CHALLENGE)//g_sLoadedPlaylistDetails.tl31szContentID[iPlayList])
				bGetAChallenge = FALSE
			ENDIF
		ENDIF
		
		#ENDIF
	
		sCurrentVarsStruct.bDiscVisible = FALSE
		sCurrentVarsStruct.bCanCreateADecalThisFrame = TRUE
		
		FMMC_FAKE_LEFT_AND_RIGHT_INPUTS_FOR_MENUS(sFMMCmenu)
		
		WAIT(0) 
		
		//Prevent recording whilst in the creator
		REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		
		PROCESS_CREATOR_EVENTS(sCurrentVarsStruct, sFMMCmenu)
		
		IF IS_FAKE_MULTIPLAYER_MODE_SET()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		ENDIF
		
		DISPLAY_ALL_CURRENT_ROCKSTAR_CREATED(sFMMCendStage.sRocStarCreatedVars, sGetUGC_content)
		
		DO_BLIMP_SIGNS(sBlimpSign, sCurrentVarsStruct.iMenuState = STAGE_DEAL_WITH_FINISHING)
		
		PROCESS_ON_DEMAND_MENU_REFRESHING(sFMMCMenu)
		
		//If the cloud bails on us. 
//		IF SHOULD_CREATOR_BAIL_DUE_TO_CLOUD_BEING_DOWN(sFMMCendStage.sClodBailStruct)
//			SCRIPT_CLEANUP(TRUE)
//		ENDIF

		//Check to see if we need to clear the tested bit sets		
		CHECK_NEEDS_RETESTED()
		FMMC_HANDLE_KEYBOARD_AND_MOUSE(sFMMCmenu, NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive) OR (bShowMenu), bShowMenuHighlight AND bShowMenu)
		IF (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) != bLastUseMouseKeyboard)
			bLastUseMouseKeyboard = IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		ENDIF
	
		bShowMenuHighlight = FALSE		
		FMMC_UPDATE_MENU_EXCLUSIVITY_IN_TEST_MODE(sFMMCdata)
		
		//Check to see if the creator should act dead for Brenda.
		IF g_TurnOnCreatorHud
		
			//setup doors
			HANDLE_MOUSE_ENTITY_SELECTION(IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive))
			CREATOR_DOORS_SETUP(iDoorSlowLoop,iDoorSetupStage,FMMC_TYPE_MISSION)
			
			//If we are not running the test mission stuff
			IF NOT bTestModeControllerScriptStarted			
				
				 //Fix for the time of day changing and for the weather
				FMMC_MAINTAIN_TIME_AND_WEATHER(g_FMMC_STRUCT.iTimeOfDay)
				FM_MAINTAIN_MISSION_DENSITYS_THIS_FRAME()
				IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)	
					DISABLE_ADDITIONAL_CONTROLS()
				ENDIF
			
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				SCRIPT_PROFILER_START_OF_FRAME()
				#ENDIF
				#ENDIF	
				
				IF !g_CreatorsSelDetails.bColouringText
					MAINTAIN_CAMERA_ROTATION_LIMITS(sCamData, sFMMCmenu.sActiveMenu, sCurrentVarsStruct, bInitialIntroCamSetup, bInitialIntroCamWarp)
				ENDIF
				
				// Tracking time in creator mode
				IF SCRIPT_IS_CLOUD_AVAILABLE()
					TRACK_TIME_IN_CREATOR_MODE()
				ENDIF
				
				// Deal with the debug.
				#IF IS_DEBUG_BUILD		
					UPDATE_WIDGETS()
				#ENDIF
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("UPDATE_WIDGETS")
				#ENDIF
				#ENDIF
				IF g_Cellphone.PhoneDS != PDS_DISABLED				
				AND NOT IS_BIT_SET(sFMMCdata.iBitSet, biSetUpTestMission)
					DISABLE_CELLPHONE (TRUE)
				ENDIF
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("disable selector and cellphone")
				#ENDIF
				#ENDIF	
				
				// James A: Handle request from pause menu
				
				IF sCurrentVarsStruct.iEntityCreationStatus != STAGE_DEAL_WITH_FINISHING
				AND sCurrentVarsStruct.iEntityCreationStatus != STAGE_SET_UP_RULES_PAGE
					IF IS_PAUSE_MENU_REQUESTING_TRANSITION()
						sCurrentVarsStruct.iEntityCreationStatus = STAGE_SET_UP_RULES_PAGE
					ENDIF
					IF IS_TRANSITION_TELLING_CREATOR_TO_CLEANUP()
						sCurrentVarsStruct.iEntityCreationStatus = STAGE_SET_UP_RULES_PAGE
					ENDIF
				ENDIF
				
				BOOL bHideRadar = FALSE
				IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
				OR sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
				OR sFMMCmenu.sActiveMenu = eFmmc_RADIO_MENU
				OR sFMMCmenu.sActiveMenu = eFmmc_STARTING_TEAM_INVENTORY
				OR sFMMCmenu.sActiveMenu = eFmmc_SMS_OPTIONS
				OR sFMMCmenu.sActiveMenu = eFmmc_EARN_POINTS
				OR sFMMCmenu.sActiveMenu = eFmmc_CAPTURE_TEAM_DETAILS
				OR sFMMCmenu.sActiveMenu = eFmmc_TEAM_NAME_OPTION
				OR sFMMCmenu.sActiveMenu = eFmmc_TEAM_WANTED_OPTION
				OR sFMMCmenu.sActiveMenu = eFmmc_TEAM_HEALTH
				OR sFMMCmenu.sActiveMenu = eFmmc_AMBIENT_MENU
					bHideRadar = TRUE
					HIDE_HUD_AND_RADAR_THIS_FRAME()
				ELSE
					TURN_OFF_IDLE_TICKERS()
					THEFEED_HIDE_THIS_FRAME()
				ENDIF
									
				DEAL_WITH_PHOTO_PREVIEW(PPS, sFMMCendStage.sTakePhotoVars, sCurrentVarsStruct.iMenuState, sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE AND iIntroCamPlacementStage = 2 AND NOT IS_BIT_SET(sFMMCmenu.iMiscBitSet, bsHidePreviewPhoto),sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE)
				
				DEAL_WITH_THE_PLACING_INTRO_CAM(SF_Movie_Gallery_Shutter_Index, sFMMCmenu.sActiveMenu, iIntroCamPlacementStage, bInitialIntroCamSetup, bShowMenu, sCurrentVarsStruct.iMenuState, sCurrentVarsStruct.iEntityCreationStatus)				
				
				// -----------------------------------
				// Process server game logic		
				SWITCH sCurrentVarsStruct.iEntityCreationStatus
					CASE STAGE_CHECK_TO_LOAD_CREATION
						
						IF g_bFMMC_LoadFromMpSkyMenu = TRUE
				
							IF LOAD_A_MISSION_INTO_THE_CREATOR(sGetUGC_content, sFMMCendStage, g_sFMMC_LoadedMission)
								sCurrentVarsStruct.iEntityCreationStatus = STAGE_DEAL_WITH_LOADING
								IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
									sCamData.camPos = g_FMMC_STRUCT.vStartPos + <<0,-0.5, 20>>
								ENDIF
								PRINTLN("[NEW WEAPONS] just loaded g_FMMC_STRUCT_ENTITIES.iWeaponPallet = ", g_FMMC_STRUCT_ENTITIES.iWeaponPallet)
								sFMMCMenu.iForcedWeapon = GET_CREATOR_WEAPON_INDEX_FROM_WEAPON_TYPE(GET_DEATHMATCH_CREATOR_RESPAWN_WEAPON(g_FMMC_STRUCT_ENTITIES.iWeaponPallet))
								PRINTLN("[NEW WEAPONS] just loaded sFMMCMenu.iForcedWeapon = ", sFMMCMenu.iForcedWeapon)
								UPDATE_TEAM_WANTED_LEVELS()
								SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
							ENDIF
							
						ELSE
							sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_LOAD
						ENDIF
					BREAK
					
					CASE STAGE_ENTITY_PLACEMENT_LOAD
					
						IF HAS_ADDITIONAL_TEXT_LOADED(MENU_TEXT_SLOT)
							IF LOAD_MENU_ASSETS()
								SET_MISSION_STARTING_MENU_ITEMS_ACTIVE(sFMMCmenu.iBitActive)
								MENU_INITILISATION()
								
								IF g_FMMC_STRUCT.iXPReward = 0
									SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDEFAULT_XP)
									PRINTLN("THIS MISSION USES ciDEFAULT_XP because XP = 0")
								ELSE
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDEFAULT_XP)
										g_FMMC_STRUCT.iXPReward = 0
										PRINTLN("THIS MISSION USES ciDEFAULT_XP because nit is set")
									ELSE
										PRINTLN("THIS MISSION IS NOT USING ciDEFAULT_XP")
									ENDIF
								ENDIF
								
								PRINTSTRING("EveryThingLoaded")PRINTNL()
								PRINTLN("LOADED g_FMMC_STRUCT.iMissionType = ", g_FMMC_STRUCT.iMissionType)
																								
								sCurrentVarsStruct.iEntityCreationStatus = STAGE_CG_TO_NG_WARNING
							ENDIF
						ENDIF
					BREAK
					CASE STAGE_CG_TO_NG_WARNING
						IF g_FMMC_STRUCT.bIsUGCjobNG
						OR IS_STRING_EMPTY(g_FMMC_STRUCT.tl63MissionName)
						OR DRAW_CG_TO_NG_WARNING()
							sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
						ENDIF
					BREAK
					
					CASE STAGE_ENTITY_PLACEMENT_SETUP			
						//Procs that need to run all the time
						//If the Test mission and mini map is not avctive
						
						IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
							IF bHideRadar
								SET_BIGMAP_ACTIVE(FALSE, FALSE)
							ELSE
								SET_BIGMAP_ACTIVE(TRUE, FALSE)
							ENDIF
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
							ENDIF
							
							IF g_CreatorsSelDetails.bColouringText
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_LOOK_LR)
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_LOOK_UD)
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_MOVE_LR)
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_MOVE_UD)
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPTED_FLY_LR)
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPTED_FLY_UD)
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPTED_FLY_ZDOWN)
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPTED_FLY_ZUP)
							ENDIF
							
						ENDIF						
												
						SETUP_LAST_TEAM_STANDING_MISSION_DATA()
						
						DEAL_WITH_HITTING_MAX_ENTITES(sCurrentVarsStruct, sFMMCmenu)
												
						//Deal with the camera	
						IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
							MAINTAIN_FMMC_CAMERA(sFMMCdata, sCamData, sCurrentVarsStruct.iMenuState)
						ELSE
							IF IS_PLAYER_IN_A_MAP_ESCAPE(PLAYER_PED_ID())
								SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct,FALSE,FALSE,TRUE,FALSE)
							ENDIF
						ENDIF
						
						IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
						AND sFMMCmenu.sActiveMenu != eFmmc_PAN_CAM_BASE
						AND sFMMCmenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
						AND sFMMCmenu.sActiveMenu != eFmmc_DM_TEAM_CAMERAS
							VECTOR vDummyRot
							
							IF DOES_CAM_EXIST(sCamData.cam)
								vDummyRot = GET_CAM_ROT(sCamData.cam)
							ELSE
								vDummyRot = <<0,0,0>>
							ENDIF
							BOOL bForceOnGroundCoronaToGround
							bForceOnGroundCoronaToGround = TRUE
							IF sFMMCmenu.sActiveMenu = eFmmc_OBJECT_BASE
								IF DOES_ENTITY_EXIST(sCapObjStruct.viCoronaObj)
									IF GET_ENTITY_MODEL(sCapObjStruct.viCoronaObj) = Prop_LD_KeyPad_01
									OR GET_ENTITY_MODEL(sCapObjStruct.viCoronaObj) = Prop_LD_KeyPad_01b
										bForceOnGroundCoronaToGround = FALSE
									ENDIF
								ENDIF
							ELIF sFMMCmenu.sActiveMenu = eFmmc_COVER_POINT_BASE
								bForceOnGroundCoronaToGround = FALSE
							ENDIF
							MAINTAIN_SCREEN_CENTER_COORD(vDummyRot, sInvisibleObjects, sCurrentVarsStruct, sFMMCmenu, TRUE, sHCS.hcCurrentCoronaColour, sFMMCmenu.fCreationHeightIncrease , sFMMCmenu.fCreationHeightIncrease != 0.0, FALSE, IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround), FALSE, FALSE, sFMMCmenu.sActiveMenu = eFmmc_OBJECT_BASE, bForceOnGroundCoronaToGround)
						ENDIF
						
						IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive) AND NOT IS_BIT_SET(sFMMCdata.iBitSet, biSetUpTestMission)
							#IF IS_DEBUG_BUILD												
							// [LM][CopyAndPaste] - Needs to be above the MAINTAIN placed entity kind of functions.
							PROCESS_COPY_PASTE_FRONTEND_LOGIC(sCurrentVarsStruct)
							sFMMCMenu.iCopyFromEntity = -1
							sFMMCMenu.iCopyFromEntityTeam = -1
							#ENDIF
							
							UPDATE_SWAP_CAM_STATUS(sCurrentVarsStruct, IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround))
							RESET_MAINTAIN_PLACED_ENTITY_VARIABLES(iLocalBitSet, sCurrentVarsStruct)							
							MAINTAIN_PLACED_TRIGGER_LOCATION(g_FMMC_STRUCT.vStartPos, sStartEndBlips.biStart, sStartEndBlips.ciStartType, g_FMMC_STRUCT.vCameraPanPos, bCameraPanBlip, bPhotoBlip)
							MAINTAIN_PLACED_TEAM_CAM_BLIPS()		
							
							INT j
							FOR j = 0 TO FMMC_MAX_TEAMS - 1
								//iMaxSpawnsPerTeam[j] = FMMC_MAX_TEAMSPAWNPOINTS
								HANDLE_EACH_TEAM_MAXIMUM_SPAWNS(iMaxSpawnsPerTeam)
								MAINTAIN_PLACED_TEAM_SPAWNPOINTS(sTeamSpawnStruct[j], j, sInvisibleObjects, sHCS, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, iLocalBitSet, sFMMCendStage, sVehStruct)
							ENDFOR
							
							MAINTAIN_PLACED_ENTITIES(sFMMCmenu, sHCS, sCurrentVarsStruct, sPedStruct, sVehStruct, sWepStruct, sCapObjStruct, sLocStruct, sTrainCreationStruct, sPropStruct, sDynoPropStruct, sInvisibleObjects, sFMMCdata, g_CreatorsSelDetails, sFMMCendStage, sCamData, iLocalBitSet, g_CreatorsSelDetails.iSelectedTeam)
							MAINTAIN_PLACED_ZONES(sCurrentVarsStruct, iLocalBitSet, sFMMCmenu)
							MAINTAIN_PLAY_AREA_BOUNDS()
							
							IF NOT DOES_BLIP_EXIST(bCameraOutroBlip)								
								IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vCameraOutroPos)
									CREATE_FMMC_BLIP(bCameraOutroBlip, g_FMMC_STRUCT.vCameraOutroPos, HUD_COLOUR_WHITE, "FMMC_B_7", 1)
									SET_BLIP_SPRITE(bCameraOutroBlip, RADAR_TRACE_CAMERA)
									SET_BLIP_NAME_FROM_TEXT_FILE(bCameraOutroBlip, "FMMC_B_7")
								ENDIF
							ELSE
								IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vCameraOutroPos)
								OR NOT ARE_VECTORS_EQUAL(GET_BLIP_COORDS(bCameraOutroBlip), g_FMMC_STRUCT.vCameraOutroPos)
									REMOVE_BLIP(bCameraOutroBlip)
								ENDIF
							ENDIF
							
						ELSE
							IF DOES_BLIP_EXIST(biBounds)
								REMOVE_BLIP(biBounds)
							ENDIF
						ENDIF
						
						DEAL_WITH_SELECTING_START_END_LOCATIONS()
						DEAL_WITH_SETTING_HEIGHT_ON_ENTITY(sFMMCmenu, sCamData, sCurrentVarsStruct, IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround), CAN_HEIGHT_BE_SET_FOR_ENTITY(sFMMCmenu, sFMMCmenu.iEntityCreation, FALSE))
						
						REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(sFMMCmenu.iEntityCreation)
						
						IF MAINTAIN_FMMC_RADIO(sFMMCmenu, menuScrollController)
							REFRESH_MENU(sFMMCMenu)
						ENDIF
												
						SWITCH sCurrentVarsStruct.iMenuState
						
							CASE MENU_STATE_DEFAULT
#IF IS_DEBUG_BUILD
								IF sFMMCmenu.sActiveMenu != eFmmc_TOP_MENU
								AND HAS_CREATOR_SEARCH_BEEN_TRIGGERED()
									sCurrentVarsStruct.iMenuState = MENU_STATE_SEARCH
								ELSE // Else rest of function
#ENDIF							
								IF sFMMCmenu.bTypeSet
									sFMMCmenu.bTypeSet = TRUE
								ENDIF

								SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
								
								iTestFailBitSet = 0
								SET_FAIL_BIT_SET(iTestFailBitSet)
								
								IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TITLE)
									CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
								ELSE
									SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
								ENDIF
								
								IF iTestFailBitSet = 0	
								AND NOT IS_BIT_SET_ALERT(sFMMCmenu)
								AND (sFMMCendStage.bMajorEditOnLoadedMission = TRUE OR !IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_NOT_PLAYABLE))
									SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_PUBLISH)
								ELSE
									CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_PUBLISH)	
								ENDIF

								IF g_FMMC_STRUCT.bMissionIsPublished
									CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
								ENDIF
								
								#IF IS_DEBUG_BUILD
									IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisableCreatorForceTest")
										IF g_FMMC_STRUCT.bMissionIsPublished
											SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_PUBLISH)
										ELSE
											SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
										ENDIF
									ENDIF
								#ENDIF
								
								IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
									SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
									sCurrentVarsStruct.bFirstShapeTestCheckDone = FALSE
									sCurrentVarsStruct.sCoronaShapeTest.bDoStartShapeTest = FALSE
									sCurrentVarsStruct.sCoronaShapeTest.stiShapeTest  = NULL		
								ENDIF	
								
								IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
								
									SET_FAIL_BIT_SET(iTestFailBitSet)
									
									IF SHOULD_MENU_BE_DRAWN(sFMMCData, sFMMCMenu, sCurrentVarsStruct, menuScrollController, 1, sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU, bShowMenu)
									AND NOT FMMC_PROCESS_ENTITY_LIST_MENU(sFMMCMenu, sFMMCData, sCurrentVarsStruct)	
									
										bShowMenuHighlight = TRUE
										FMMC_DO_MENU_ACTIONS(	RETURN_CIRCLE_MENU_SELECTIONS(),
															sCurrentVarsStruct, sFMMCmenu, 
															sFMMCData, sFMMCendStage, sHCS)
															
										IF CHECK_FOR_DLC_LOCKED_VEHICLE(sFMMCmenu)
										OR CHECK_FOR_DLC_LOCKED_WEAPONS(sWepStruct, sFMMCmenu.sActiveMenu)
											IF NOT IS_BIT_SET(iLocalBitset, biDLCLocked)
												SET_BIT(iLocalBitset, biDLCLocked)
												PRINTLN("BIT SET!")
												sCurrentVarsStruct.bResetUpHelp = TRUE
											ENDIF
										ELSE
											IF IS_BIT_SET(iLocalBitset, biDLCLocked)
												PRINTLN("BIT CLEARED!")
												CLEAR_BIT(iLocalBitset, biDLCLocked)
												sCurrentVarsStruct.bResetUpHelp = TRUE
											ENDIF
										ENDIF
										
										IF IS_BIT_SET(iHelpBitSet, biRemovePedsButton)
											IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
												INT iVeh
												FOR iVeh = 0 TO FMMC_MAX_VEHICLES - 1
													IF DOES_ENTITY_EXIST(sCurrentVarsStruct.vCoronaHitEntity)
													AND DOES_ENTITY_EXIST(sVehStruct.veVehcile[iVeh])
														VEHICLE_INDEX vehTemp
														IF IS_ENTITY_A_VEHICLE(sCurrentVarsStruct.vCoronaHitEntity)
															vehTemp = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sCurrentVarsStruct.vCoronaHitEntity)
															IF vehTemp = sVehStruct.veVehcile[iVeh]
																REMOVE_ALL_PEDS_FROM_VEHICLE(sPedStruct, sVehStruct, iVeh)
																CLEAR_BIT(iHelpBitSet, biRemovePedsButton)
															ENDIF
														ENDIF
													ENDIF
												ENDFOR
											ENDIF
										ENDIF
																				
										SWITCH sFMMCmenu.iEntityCreation
										
											CASE - 1
												sCurrentVarsStruct.iHoverEntityType  = -1
												IF sFMMCmenu.sActiveMenu != eFmmc_TOP_MENU
												AND sFMMCmenu.sActiveMenu != eFmmc_MAIN_OPTIONS_BASE
												AND sFMMCmenu.sActiveMenu != eFmmc_AMBIENT_MENU
													UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT)
												ELSE
													UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT, FALSE)
												ENDIF
												
												IF g_FMMC_STRUCT_ENTITIES.iNumberOfZones	< 10
													IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedZones[g_FMMC_STRUCT_ENTITIES.iNumberOfZones].vPos[0])
														IF IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedZones[g_FMMC_STRUCT_ENTITIES.iNumberOfZones].vPos[1])
															g_FMMC_STRUCT_ENTITIES.sPlacedZones[g_FMMC_STRUCT_ENTITIES.iNumberOfZones].vPos[0] = <<0,0,0>>
														ENDIF
													ENDIF
												ENDIF
												
											BREAK
											
											CASE CREATION_TYPE_PAN_CAM 									
												BOOL bResetCamMenu
												bResetCamMenu = FALSE
												MAINTAIN_PLACING_INTRO_CAMERAS(sFMMCmenu, bCameraTriggerBlip, bResetCamMenu, bInitialIntroCamSetup)
												UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT, FALSE)
												IF bResetCamMenu
													REFRESH_MENU(sFMMCMenu)
												ENDIF
												BREAK
												
											CASE CREATION_TYPE_TRIGGER 									
												DEAL_WITH_SELECTING_TRIGGER_LOCATION()
												BREAK	
												
											CASE CREATION_TYPE_PEDS
												DO_PED_CREATION(sPedStruct, sVehStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage, sTrainCreationStruct, ciMAX_CREATED_PEDS)
												BREAK
												
											CASE CREATION_TYPE_VEHICLES
												DO_VEHICLE_CREATION(sVehStruct, sPedStruct, sCapObjStruct, sTrainCreationStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage, ciMAX_CREATED_VEHICLES, iLocalBitSet)
												BREAK
												
											CASE CREATION_TYPE_WEAPONS 
												DO_WEAPON_CREATION(sWepStruct, sCapObjStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage, ciMAX_CREATED_WEAPONS, TRUE, iLocalBitSet)
												BREAK
												
											CASE CREATION_TYPE_OBJECTS 
												DO_CAPTURE_OBJECT_CREATION(sCapObjStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage, IS_BIT_SET(iLocalBitSet, biCanEditEntityInCorona), ciMAX_CREATED_OBJECTS)												
												BREAK
												
											CASE CREATION_TYPE_PROPS 
												DO_PROP_CREATION(sPropStruct, sVehStruct, sCapObjStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage, sCamData, sPedStruct, sTrainCreationSTruct, ciMAX_CREATED_PROPS, iLocalBitSet)					
												BREAK
												
											CASE CREATION_TYPE_DYNOPROPS 
												DO_DYNOPROP_CREATION(sDynoPropStruct, sPropStruct, sVehStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata,sFMMCendStage, sCamData, sPedStruct, sTrainCreationStruct, FMMC_MAX_NUM_DYNOPROPS, iLocalBitSet)
												BREAK
												
											CASE CREATION_TYPE_DROP_OFF
												DO_DROPOFF_CREATION(g_CreatorsSelDetails,  sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sFMMCendStage, g_FMMC_STRUCT.iMaxNumberOfTeams)
												BREAK
												
											CASE CREATION_TYPE_RESPAWN_AREA
												DO_RESPAWN_AREA_CREATION(g_CreatorsSelDetails,  sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sFMMCendStage, g_FMMC_STRUCT.iMaxNumberOfTeams, sTeamSpawnStruct[sFMMCmenu.iSelectedTeam])
												BREAK
												
											CASE CREATION_TYPE_GOTO_LOC
												INT iValue
												iValue = FMMC_MAX_GO_TO_LOCATIONS
												DO_LOCATION_CREATION(g_CreatorsSelDetails, sLocStruct, sCurrentVarsStruct/*, sHCS*/, sFMMCmenu, sFMMCdata, sFMMCendStage, iValue)
												BREAK
												
											CASE CREATION_TYPE_FMMC_ZONE
												DO_ZONE_CREATION(sCurrentVarsStruct, sFMMCmenu, sFMMCendStage)
												DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfZones,  10,	"FMMCCMENU_18T", -1)
												REFRESH_MENU(sFMMCMenu)
											BREAK
												
											CASE CREATION_TYPE_TEAM_SPAWN_LOCATION 
												iTeamSpawnLocations = CEIL(TO_FLOAT(g_FMMC_STRUCT.iNumParticipants) / TO_FLOAT(g_FMMC_STRUCT.iMaxNumberOfTeams))
												HANDLE_EXTRA_SPAWN_POINTS(sTeamSpawnStruct, iMaxSpawnsPerTeam, iTeamSpawnLocations, sInvisibleObjects)
												DO_TEAM_SPAWN_POINT_CREATION(sTeamSpawnStruct[sFMMCmenu.iSelectedTeam], sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage,sVehStruct, iMaxSpawnsPerTeam, IS_BIT_SET(iLocalBitSet, biCanEditEntityInCorona))
											BREAK
											
										ENDSWITCH
										
										SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME(2)
										
									ELSE
									
										//Reset stuff. 
										sPedStruct.iSwitchingINT = CREATION_STAGE_WAIT
										sWepStruct.iSwitchingINT = CREATION_STAGE_WAIT
										sVehStruct.iSwitchingINT = CREATION_STAGE_WAIT	
										SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)		
										
										IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
											DELETE_VEHICLE(sVehStruct.viCoronaVeh)
										ENDIF
										IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
											DELETE_OBJECT(sWepStruct.viCoronaWep)
										ENDIF
										IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
											DELETE_OBJECT(sPropStruct.viCoronaObj)
										ENDIF
										DELETE_CURRENT_PROP_TEMPLATE_OBJECTS(sPropStruct)
										
									ENDIF
									
								ENDIF
								
								IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
									IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())	
										BOOL bReloadMenu
										bReloadMenu = FALSE
										IF NOT IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
											DEAL_WITH_SKY_CAM_SWITCH(sFMMCdata,bSwitchingCam,vSwitchVec,sCamData, bReloadMenu)
										ELSE
											DEAL_WITH_PLAYER_WARP_TO_ITEM(sFMMCdata,bSwitchingCam,vSwitchVec,fSwitchHeading, bReloadMenu)
										ENDIF
										IF bReloadMenu
											REFRESH_MENU(sFMMCMenu)
										ENDIF
									ENDIF
								ENDIF
								
								CHECK_FOR_MENU_SET_UP(sCurrentVarsStruct, sPedStruct, sVehStruct, sWepStruct, sCapObjStruct, sFMMCmenu, g_CreatorsSelDetails.iSelectedTeam)
								
								//ADD_NEW_ENTITY_RULE(sFMMCmenu.iEntityCreation)
#IF IS_DEBUG_BUILD
								ENDIF // Else search function
#ENDIF
							BREAK
						
							CASE MENU_STATE_PLACE_CAM
								IF PPS.iStage != WAIT_PREVIEW_PHOTO
								AND PPS.iStage != CLEANUP_PREVIEW_PHOTO
								AND sFMMCendStage.sTakePhotoVars.iTakePhotoStage != ciFMMC_TAKE_PHOTO_STAGE_OPEN_SHUT
									PPS.iStage = CLEANUP_PREVIEW_PHOTO
									PPS.iPreviewPhotoDelayCleanup = 0
								ENDIF
								IF sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE								
									REMOVE_DECALS_FOR_PHOTOS(sFMMCendStage.sTakePhotoVars.iTakePhotoStage, sStartEndBlips, sPedStruct, sTeamSpawnStruct, sWepStruct, sCapObjStruct, sHCS.hcStartCoronaColour)
								ENDIF								
								IF TAKE_PHOTO_FOR_FMMC(sFMMCendStage.sTakePhotoVars, SF_Movie_Gallery_Shutter_Index, sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE)
									IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
										g_FMMC_STRUCT.vCameraPanPos = GET_CAM_COORD(GET_RENDERING_CAM())
										g_FMMC_STRUCT.vCameraPanRot = GET_CAM_ROT(GET_RENDERING_CAM())	
										VECTOR vRot
										vRot = GET_CAM_ROT(GET_RENDERING_CAM())
										g_FMMC_STRUCT.fCameraPanHead = vRot.z
										PRINTLN("PAN CAM POSITIONED Rot - ", g_FMMC_STRUCT.vCameraPanRot, " Vector - ", g_FMMC_STRUCT.vCameraPanPos)
									ELIF sFMMCmenu.sActiveMenu = eFmmc_OUT_CAM_BASE
										g_FMMC_STRUCT.vCameraOutroPos = GET_CAM_COORD(GET_RENDERING_CAM())
										g_FMMC_STRUCT.vCameraOutroRot = GET_CAM_ROT(GET_RENDERING_CAM())	
										PRINTLN("OUTRO CAM POSITIONED Rot - ", g_FMMC_STRUCT.vCameraOutroRot, " Vector - ", g_FMMC_STRUCT.vCameraOutroPos)
									ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_CAMERAS
										g_FMMC_STRUCT.sFMMCEndConditions[sFMMCmenu.iSelectedCameraTeam].vCameraPos = GET_CAM_COORD(GET_RENDERING_CAM())
										g_FMMC_STRUCT.sFMMCEndConditions[sFMMCmenu.iSelectedCameraTeam].vCameraRot = GET_CAM_ROT(GET_RENDERING_CAM())
										PRINTLN("Team = ", sFMMCmenu.iSelectedCameraTeam," PAN CAM POSITIONED Rot - ", g_FMMC_STRUCT.sFMMCEndConditions[sFMMCmenu.iSelectedCameraTeam].vCameraRot, " Vector - ", g_FMMC_STRUCT.sFMMCEndConditions[sFMMCmenu.iSelectedCameraTeam].vCameraPos)
									ENDIF
									sFMMCendStage.bMajorEditOnLoadedMission = TRUE
									REFRESH_MENU(sFMMCMenu)
									sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
								ENDIF
							BREAK
						
							CASE MENU_STATE_PAN_CAM
								IF sFMMCmenu.sActiveMenu != eFmmc_OUT_CAM_BASE
									IF DEAL_WITH_CREATOR_CAMERA_PAN_PREVIEW(iCamPanState, jobIntroData, vInitialPreviewPos)	
										// Shonky fix for 2102449
										IF !SCRIPT_IS_CLOUD_AVAILABLE()
											IF NOT IS_SCREEN_FADED_IN()
												DO_SCREEN_FADE_IN(500)
											ENDIF	
										ENDIF
										sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
									ENDIF
								ELSE
									SET_CAM_COORD(sCamData.cam, g_FMMC_STRUCT.vCameraOutroPos)
									SET_CAM_ROT(sCamData.cam, g_FMMC_STRUCT.vCameraOutroRot)
									sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
								ENDIF
							BREAK
						
							CASE MENU_STATE_SET_SPEC_OBJ_TEXT
								IF bDelayAFrame = FALSE	
									IF sFMMCMenu.iSelectedTeam > -1
										IF DEAL_WITH_SETTING_SIMPLE_TL63(sFMMCendStage, g_FMMC_STRUCT.tl63_RoamingSpectatorObjectiveTextOverride[sFMMCMenu.iSelectedTeam])										
											sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
											REFRESH_MENU(sFMMCmenu)
											bDelayAFrame = TRUE
										ENDIF
									ENDIF
								ELSE
									bDelayAFrame = FALSE
								ENDIF
							BREAK
							
							CASE MENU_STATE_TITLE
								IF bDelayAFrame = FALSE
									IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, TRUE)	
										sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
										REFRESH_MENU(sFMMCMenu)
										bDelayAFrame = TRUE
									ELSE							
										bShowMenuHighlight = FALSE
									ENDIF
								ELSE
									bDelayAFrame = FALSE
								ENDIF
							BREAK
							
							CASE MENU_STATE_DESCRIPTION
								IF bDelayAFrame = FALSE
									IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, FALSE)	
										sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
										REFRESH_MENU(sFMMCMenu)
										bDelayAFrame = TRUE
									ELSE							
										bShowMenuHighlight = FALSE
									ENDIF
								ELSE
									bDelayAFrame = FALSE
								ENDIF
							BREAK
							
							CASE MENU_STATE_BLIMP_MESSAGE
								IF bDelayAFrame = FALSE
									IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, FALSE, FALSE, TRUE)
										REFRESH_MENU(sFMMCMenu)
										sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
										bDelayAFrame = TRUE
										CLEANUP_BLIMP(sBlimpSign)
									ELSE
										bShowMenuHighlight = FALSE
									ENDIF
								ELSE
									bDelayAFrame = FALSE
								ENDIF
							BREAK
#IF IS_DEBUG_BUILD
							CASE MENU_STATE_SEARCH
								IF bDelayAFrame = FALSE
									IF DEAL_WITH_MENU_SEARCH(sFMMCendStage, sFMMCmenu)	
										sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
										REFRESH_MENU(sFMMCmenu)
										bDelayAFrame = TRUE
									ENDIF
								ELSE
									bDelayAFrame = FALSE
								ENDIF
							BREAK
#ENDIF					
							
							CASE MENU_STATE_TAGS
								IF bDelayAFrame = FALSE
									IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, FALSE, TRUE)	
										sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
										REFRESH_MENU(sFMMCMenu)
										bDelayAFrame = TRUE
									ELSE							
										bShowMenuHighlight = FALSE
									ENDIF
								ELSE
									bDelayAFrame = FALSE
								ENDIF
							BREAK
							
							CASE MENU_STATE_SMS_MESSAGE
								IF bDelayAFrame = FALSE
									IF DEAL_WITH_SETTING_MISSION_SMS_MESSAGE(sFMMCendStage)	
										sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
										bDelayAFrame = TRUE
									ELSE							
										bShowMenuHighlight = FALSE
									ENDIF
								ELSE
									bDelayAFrame = FALSE
								ENDIF
							BREAK
														
							CASE MENU_STATE_TEST_MISSION
								// MAINTAIN_TEST_MISSION_STATE()
							BREAK
							
							CASE MENU_STATE_SWITCH_CAM
								IF NOT IS_SCREEN_FADED_OUT()
									PRINTSTRING("screen not faded out yet, calling fade out")PRINTNL()
									DO_SCREEN_FADE_OUT(500)
								ELSE
									PRINTSTRING("Screen is faded out do swap cams")PRINTNL()
									IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
										SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct,TRUE,FALSE,TRUE,FALSE)
									ELSE
										SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct,FALSE,FALSE,TRUE,FALSE)
									ENDIF
									NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 20)
									sCurrentVarsStruct.iMenuState = MENU_STATE_LOADING_AREA
								ENDIF							
							BREAK
							
							CASE MENU_STATE_LOADING_AREA
							
								IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
								OR IS_NEW_LOAD_SCENE_LOADED()		
									PRINTSTRING("LOADED, FADE IN")PRINTNL()
									DO_SCREEN_FADE_IN(500)
									NEW_LOAD_SCENE_STOP()
									RESET_INVISIBLE_OBJECT_POSITIONS(sInvisibleObjects)
									IF IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
										SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
										SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
										FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
									ENDIF
									
									REFRESH_MENU(sFMMCMenu)
									
									sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT		
								ELSE
									PRINTSTRING("WAITING FOR SCENE TO LOAD")PRINTNL()								
								ENDIF
								
							BREAK
							
							CASE MENU_STATE_CUSTOM_TEMPLATE_NAME
								IF bDelayAFrame = FALSE
									IF DEAL_WITH_SETTING_CUSTOM_TEMPLATE_NAME(sFMMCendStage, g_FMMC_STRUCT.iNumberOfPropTemplates-1)
										sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
										REFRESH_MENU(sFMMCmenu)
										bDelayAFrame = TRUE
									ENDIF
								ELSE
									bDelayAFrame = FALSE
								ENDIF
							BREAK
						ENDSWITCH
						
						BOOL bHideMarker
						IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
						OR sFMMCmenu.sActiveMenu = eFmmc_COVER_POINT_BASE
						OR sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_CAMERAS
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FMMCC_TEST3")
							bHideMarker = TRUE
						ELSE
							bHideMarker = FALSE
						ENDIF
						MAINTAIN_PLACEMENT_DISC(sCurrentVarsStruct, sFMMCmenu, iLocalBitSet, sFMMCdata.iBitSet, bHideMarker, sFMMCmenu.iEntityCreation = -1)
						
						IF g_bShow_FMMC_rulesMenu = TRUE
							IF g_CreatorsSelDetails.bMenuSetUp = FALSE
								//AUTO_POPULATE_RULES_MENU(g_CreatorsSelDetails)
								g_CreatorsSelDetails.bMenuSetUp = TRUE
							ENDIF
							IF DO_RULES_END_MENU()
					 			g_bShow_FMMC_rulesMenu = FALSE			
								g_CreatorsSelDetails.bMenuSetUp = FALSE
							ENDIF
						ENDIF	
						BLIP_INDEX biFurthestBlip
						IF sFMMCmenu.bZoomedOutRadar
							biFurthestBlip = GET_FURTHEST_BLIP_TO_PLAYER(sPedStruct.biPedBlip, sVehStruct.biVehicleBlip, sWepStruct.biWeaponBlip, sPropStruct.biObject, sDynoPropStruct.biObject, sStartEndBlips.biStart, bCameraPanBlip, sCurrentVarsStruct.biLocateBlip)
						ENDIF
						IF IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
							CONTROL_PLAYER_BLIP(TRUE, sCurrentVarsStruct, sFMMCmenu.iEntityCreation, TRUE, biFurthestBlip)
							IF DOES_BLIP_EXIST(biBounds)
								REMOVE_BLIP(biBounds)
							ENDIF
						ELSE
							IF sFMMCmenu.sActiveMenu != eFmmc_PAN_CAM_BASE
							AND sFMMCmenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
							AND sFMMCmenu.sActiveMenu != eFmmc_DM_TEAM_CAMERAS
							AND sFMMCmenu.sActiveMenu != eFmmc_MAIN_OPTIONS_BASE
							AND sFMMCmenu.sActiveMenu != eFmmc_TOP_MENU
							AND sFMMCmenu.sActiveMenu != eFmmc_AMBIENT_MENU
							AND sFMMCmenu.sActiveMenu != eFmmc_RADIO_MENU
								CONTROL_PLAYER_BLIP(FALSE, sCurrentVarsStruct, sFMMCmenu.iEntityCreation, sFMMCmenu.bZoomedOutRadar, biFurthestBlip)
							ELSE
								CONTROL_PLAYER_BLIP(TRUE, sCurrentVarsStruct, sFMMCmenu.iEntityCreation, sFMMCmenu.bZoomedOutRadar, biFurthestBlip)
							ENDIF
						ENDIF
						
					BREAK
					
					CASE STAGE_DELETE_ALL
					CASE STAGE_DELETE_PEDS
					CASE STAGE_DELETE_VEHICLES
					CASE STAGE_DELETE_WEAPONS
					CASE STAGE_DELETE_PROPS
					CASE STAGE_DELETE_DYNOPROPS
					CASE STAGE_DELETE_OBJECTS
					CASE STAGE_DELETE_STUNT_JUMPS
					CASE STAGE_DELETE_TEAM_START
					CASE STAGE_DELETE_ZONES
					CASE STAGE_DELETE_BOUNDS
					CASE STAGE_DELETE_CTF_VEHICLES
					CASE STAGE_DELETE_NON_CTF_VEHICLES
						//Deal with confirming selection
						DO_CONFIRMATION_MENU(sPedStruct, sTeamSpawnStruct, sVehStruct, sWepStruct, sCapObjStruct, sPropStruct, sDynoPropStruct, sLocStruct, sCurrentVarsStruct, sFMMCmenu, sInvisibleObjects, sFMMCEndStage, ButtonPressed, iLocalBitSet)
					BREAK
					
					//Deal with the End Menu
					CASE STAGE_ENTITY_PLACEMENT_END_MENU
											
						//Deal with the end menu
						PRINTLN("Setting iEntityCreation = -1  loc2")
						SET_CREATION_TYPE(sFMMCMenu, CREATION_TYPE_NONE)
						sCurrentVarsStruct.iMenuState = 0
						MAINTAIN_FMMC_CAMERA(sFMMCdata, sCamData, sCurrentVarsStruct.iMenuState)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
						ENDIF
						
						IF DO_END_MENU()
				 			sCurrentVarsStruct.iEntityCreationStatus = STAGE_SET_UP_RULES_PAGE
							#IF IS_DEBUG_BUILD
							PRINT_FMMC_DEBUGDATA(FALSE)	
							#ENDIF
						ENDIF
					BREAK	
					
					CASE STAGE_CLOUD_FAILURE	
						FMMC_DRAW_CLOUD_FAIL_WARNING(bCreatorLimitedCloudDown, bSignedOut)
						IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
							// Shonky fix for 2102449
							IF !SCRIPT_IS_CLOUD_AVAILABLE()
								sCurrentVarsStruct.iMenuState = MENU_STATE_PAN_CAM
							ELSE
								IF NOT IS_SCREEN_FADED_IN()
									DO_SCREEN_FADE_IN(500)
								ENDIF
							ENDIF
							sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
							REFRESH_MENU(sFMMCMenu)
						ENDIF
					BREAK
					
					//Deal With LOADING a previous mission
					CASE STAGE_DEAL_WITH_LOADING
						PRINTLN("STAGE_DEAL_WITH_LOADING")
						IF SET_SKYSWOOP_UP(TRUE)
							IF RESET_THE_MISSION_UP_SAFELY()
								iResetState = RESET_STATE_FADE
								IF sFMMCmenu.iRadioState = FMMC_RADIO_MAX_OPTIONS_STATE
									SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(TRUE)
									SET_MOBILE_PHONE_RADIO_STATE(TRUE)
								ENDIF			
								CLEAR_HELP()
								
								//Clean up
								REFRESH_MENU(sFMMCMenu)
									
								sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_LOAD
								sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT

								WARP_CAMERA_TO_START_LOCATION(sCamData, 110.0)
								SET_FMMC_GLOBALS_TO_MENU_OPTIONS()
								sCurrentVarsStruct.creationStats.bEditingACreation = true
								
							
								INT iTeam
								FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1									
									if iTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams	
										PRINTLN("iTeam = ", iTeam, " AND IS GREATER THAN THE MAX = ", g_FMMC_STRUCT.iMaxNumberOfTeams)
										g_FMMC_STRUCT.sFMMCEndConditions[iTeam].bOkToDrawStartPosCorona = FALSE
										g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vStartPos = g_FMMC_STRUCT.vStartPos
									ELSE
										IF ARE_VECTORS_EQUAL(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vStartPos, g_FMMC_STRUCT.vStartPos)
											PRINTLN("iTeam = ", iTeam, " AND IS SAME VECTOR AS TRIGGER")
											g_FMMC_STRUCT.sFMMCEndConditions[iTeam].bOkToDrawStartPosCorona = FALSE
										ELSE
											PRINTLN("iTeam = ", iTeam, " AND NOT SAME SO THE BOOL IS TRUE")
											g_FMMC_STRUCT.sFMMCEndConditions[iTeam].bOkToDrawStartPosCorona = TRUE
										ENDIF
									ENDIF
								ENDFOR	
								
//								sFMMCmenu.iSelectedTab = 1
//								iSelectedTabOld = 0
								
																
								CLEAR_BIT(sFMMCdata.iBitSet, biTestMissionActive)
								CLEAR_BIT(sFMMCdata.iBitSet, bTestHelpTextDone)
								SET_BIT(sFMMCdata.iBitSet, bCameraActive)
								bMpNeedsCleanedUp = FALSE
								bTestModeControllerScriptStarted = FALSE
								
								bTextSetUp = FALSE
								
								bMpModeCleanedUp = FALSE	
										
								IF IS_LOADING_ICON_ACTIVE()
									SET_LOADING_ICON_INACTIVE()
								ENDIF
								
								DO_SCREEN_FADE_IN(1000)
								
							ENDIF
						ENDIF	
					BREAK
					
					//Deal with the rules page
					CASE STAGE_SET_UP_RULES_PAGE
						PRINTLN("STAGE_SET_UP_RULES_PAGE")
						IF DO_RULES_END_MENU()
				 			sCurrentVarsStruct.iEntityCreationStatus = STAGE_DEAL_WITH_FINISHING					
							#IF IS_DEBUG_BUILD
							PRINT_FMMC_DEBUGDATA(FALSE)	
							#ENDIF
						ENDIF
					BREAK
					
					//Deal With ENDING THE the FMMC Script
					CASE STAGE_DEAL_WITH_SAVING	
						PRINTLN("STAGE_DEAL_WITH_SAVING")
						IF bContentReadyForUGC = FALSE
							SET_MENU_OPTIONS_TO_FMMC_GLOBALS()
							bContentReadyForUGC = TRUE
							PRINTLN("SET_MENU_OPTIONS_TO_FMMC_GLOBALS Done. Time to Save")
						ELSE
							//New save methord
							#IF IS_DEBUG_BUILD
							IF g_buseNewUgcSaveSystem
							
							ELSE
							#ENDIF
							//Old save methord	
								IF SAVE_THIS_CREATION(sGetUGC_content, sFMMCendStage, sCurrentVarsStruct.creationStats)
									#IF IS_DEBUG_BUILD
									PRINT_FMMC_DEBUGDATA(FALSE)	
									#ENDIF
									sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
									sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
									SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
									sCurrentVarsStruct.bSwitching = FALSE
									sCurrentVarsStruct.stiScreenCentreLOS = NULL
									sFMMCendStage.iEndStage = 0
									sFMMCendStage.dataStruct.iLoadStage = 0
									sFMMCendStage.iPublishConformationStage = 0
									sFMMCendStage.iPublishStage = 0
									sFMMCendStage.iButtonBitSet = 0
									DO_SCREEN_FADE_IN(200)
									IF NOT IS_STRING_NULL_OR_EMPTY(sFMMCendStage.tl23ReturnedContexID)
										sFMMCendStage.tl23LoadedContexID = sFMMCendStage.tl23ReturnedContexID						
										// Removed for: 2162073/2162036 CREATOR_WRITE_TO_LEADERBOARD(sCurrentVarsStruct.creationStats, sFMMCendStage.tl23ReturnedContexID)
									ENDIF
	//								sCurrentVarsStruct.creationStats.bEditingACreation = true
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDEFAULT_XP)
										g_FMMC_STRUCT.iXPReward = 0
										PRINTLN("ciDEFAULT_XP RESET TO = ", g_FMMC_STRUCT.iXPReward)
									ENDIF
									bContentReadyForUGC = FALSE
								ENDIF
							#IF IS_DEBUG_BUILD
							ENDIF
							#ENDIF
							IF sFMMCendStage.oskStatus = OSK_CANCELLED
								sFMMCendStage.oskStatus = OSK_PENDING
								sFMMCendStage.iKeyBoardStatus = 0
								sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
								sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
								SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
								sCurrentVarsStruct.bSwitching = FALSE
								sCurrentVarsStruct.stiScreenCentreLOS = NULL
								DO_SCREEN_FADE_IN(200)
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDEFAULT_XP)
									g_FMMC_STRUCT.iXPReward = 0
									PRINTLN("ciDEFAULT_XP RESET TO = ", g_FMMC_STRUCT.iXPReward)
								ENDIF
								bContentReadyForUGC = FALSE
								#IF IS_DEBUG_BUILD
								PRINTLN("oskStatus = OSK_CANCELLED")
								#ENDIF					
							ENDIF
						ENDIF
					BREAK	
					
					//Deal With ENDING THE the FMMC Script
					CASE STAGE_ENTITY_PLACEMENT_PUBLISH
						PRINTLN("STAGE_ENTITY_PLACEMENT_PUBLISH")
						IF bContentReadyForUGC = FALSE
							SET_MENU_OPTIONS_TO_FMMC_GLOBALS()
							CLEAR_BIT(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_NOT_PLAYABLE)
							bContentReadyForUGC = TRUE
							PRINTLN("SET_MENU_OPTIONS_TO_FMMC_GLOBALS Done. Time to Publish")
						ELSE
							IF PUBLISH_THIS_CREATION(sGetUGC_content, sFMMCendStage, sCurrentVarsStruct.creationStats)
								PRINTLN("WE HAVE FINISHED PUBLISHING. TIME TO GET OUT OF HERE.")
								sFMMCendStage.iEndStage = 0
								sFMMCendStage.dataStruct.iLoadStage = 0
								sFMMCendStage.iPublishConformationStage = 0
								sFMMCendStage.iPublishStage = 0
								sFMMCendStage.iButtonBitSet = 0
								sFMMCendStage.bMajorEditOnLoadedMission = FALSE
								#IF IS_DEBUG_BUILD
								PRINT_FMMC_DEBUGDATA(FALSE)	
								#ENDIF
					 			sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
								sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
								SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
								sCurrentVarsStruct.bSwitching = FALSE
								sCurrentVarsStruct.stiScreenCentreLOS = NULL
								IF sCurrentVarsStruct.creationStats.bSuccessfulSave = TRUE
									IF NOT IS_STRING_NULL_OR_EMPTY(sFMMCendStage.tl23ReturnedContexID)
										sFMMCendStage.tl23LoadedContexID = sFMMCendStage.tl23ReturnedContexID						
										// Removed for: 2162073/2162036 CREATOR_WRITE_TO_LEADERBOARD(sCurrentVarsStruct.creationStats, sFMMCendStage.tl23ReturnedContexID)
									ENDIF
								ENDIF
								SET_FAIL_BIT_SET(iTestFailBitSet)
//								sCurrentVarsStruct.creationStats.bEditingACreation = true
								bContentReadyForUGC = FALSE
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDEFAULT_XP)
									g_FMMC_STRUCT.iXPReward = 0
									PRINTLN("ciDEFAULT_XP RESET TO = ", g_FMMC_STRUCT.iXPReward)
								ENDIF
								CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
								REFRESH_MENU(sFMMCMenu)
								DO_SCREEN_FADE_IN(200)
							ENDIF
							IF sFMMCendStage.oskStatus = OSK_CANCELLED
								sFMMCendStage.oskStatus = OSK_PENDING
								sFMMCendStage.iKeyBoardStatus = 0
								sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
								sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
								SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
								sCurrentVarsStruct.bSwitching = FALSE
								sCurrentVarsStruct.stiScreenCentreLOS = NULL
								bContentReadyForUGC = FALSE
								DO_SCREEN_FADE_IN(200)
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDEFAULT_XP)
									g_FMMC_STRUCT.iXPReward = 0
									PRINTLN("ciDEFAULT_XP RESET TO = ", g_FMMC_STRUCT.iXPReward)
								ENDIF
								#IF IS_DEBUG_BUILD
								PRINTLN("oskStatus = OSK_CANCELLED")
								#ENDIF					
							ENDIF
						ENDIF
					BREAK
					
					//Deal With ENDING THE the FMMC Script
					CASE STAGE_DEAL_WITH_FINISHING 
						PRINTLN("STAGE_DEAL_WITH_FINISHING")
						IF IS_TRANSITION_TELLING_CREATOR_TO_CLEANUP()	
							DO_SCREEN_FADE_IN(200)
							PRINTLN("[JA@PAUSEMENU] Clean up mission creator due to pause menu requesting transition, or just a normal quit ")
							SCRIPT_CLEANUP(TRUE)
						ELIF DEAL_WITH_FINISHING_CREATOR(sFMMCendStage)	
							DO_SCREEN_FADE_IN(200)	
							PRINTLN("[END CREATOR] DONE")
							SCRIPT_CLEANUP(FALSE)
						ENDIF
						
					BREAK
					
					CASE STAGE_DEAL_WITH_RECREATING_MISSION
						PRINTLN("STAGE_DEAL_WITH_RECREATING_MISSION")
						if not IS_SCREEN_FADING_OUT()	
							IF RESET_THE_MISSION_UP_SAFELY()
							//	TAKE_CONTROL_OF_TRANSITION()
								iResetState = RESET_STATE_FADE
								IF sFMMCmenu.iRadioState = FMMC_RADIO_MAX_OPTIONS_STATE
									SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(TRUE)
									SET_MOBILE_PHONE_RADIO_STATE(TRUE)
								ENDIF			
								CLEAR_HELP()
								CLEAR_BIT(sFMMCdata.iBitSet, biTestMissionActive)
								CLEAR_BIT(sFMMCdata.iBitSet, biSetUpTestMission)	
								//Clean up

								REFRESH_MENU(sFMMCmenu)
									
								sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
								sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
								
								SET_FAIL_BIT_SET(iTestFailBitSet)
								
								//set the player at origin and freeze them, force the camera to reload.
								//this is how the DM does it.
								sFMMCdata.vPlayerPos = <<0,0,10>>
								IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
									FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
									DO_SCREEN_FADE_IN(500)
								ELSE
									FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
								ENDIF	
								
								//sCamData.bLoadingArea = TRUE
																
								//DO_SCREEN_FADE_IN(500)	
								SET_SKYBLUR_CLEAR()
								PRINTLN("DO SCREEN FADE IN")
								
								UNLOCK_MENU_ITEMS_AND_ALLOW_MISSION_TEST()
								ENABLE_ALL_MP_HUD()										
								CLEAR_BIT(sFMMCdata.iBitSet, bTestHelpTextDone)
								SET_BIT(sFMMCdata.iBitSet, bCameraActive)
								bMpNeedsCleanedUp = FALSE
								bTestModeControllerScriptStarted = FALSE
//								bcompletedTutorialTest = TRUE
								bTextSetUp = FALSE
								bMpModeCleanedUp = FALSE
								#IF IS_DEBUG_BUILD
								PRINT_FMMC_DEBUGDATA(FALSE)
								#ENDIF
							ENDIF
						ENDIF
					BREAK
					
				ENDSWITCH
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("process main logic")
				#ENDIF
				#ENDIF
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("PLACE_STUNTJUMPS")
				#ENDIF
				#ENDIF
				
				//Kill this script
				#IF IS_DEBUG_BUILD	
					IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD1, KEYBOARD_MODIFIER_SHIFT, "TURN ON MISSION CREATOR DEBUG")
						g_bRunMissionCreatorInDebugMode = TRUE
					ENDIF				
					
					Maintain_Hud_Creator_Tool(wgGroup)
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
						IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE
							IF NETWORK_IS_GAME_IN_PROGRESS() = FALSE
								sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_END_MENU
							ELSE
								PRINTLN("NETWORK_IS_GAME_IN_PROGRESS() ")
							ENDIF
						ELSE
							PRINTLN("IS_FAKE_MULTIPLAYER_MODE_SET()")
						ENDIF
					ENDIF
				#ENDIF
				
				IF g_CreatorsSelDetails.iMaxNumberOfTeams != g_FMMC_STRUCT.iMaxNumberOfTeams 
					g_CreatorsSelDetails.iMaxNumberOfTeams = g_FMMC_STRUCT.iMaxNumberOfTeams 
					PRINTLN("g_CreatorsSelDetails.iMaxNumberOfTeams has changed, it is now = ", g_FMMC_STRUCT.iMaxNumberOfTeams)
				ENDIF
				
				IF sFMMCmenu.sActiveMenu != eFmmc_MAIN_OPTIONS_BASE
					iTeamSpawnLocations = CEIL(TO_FLOAT(g_FMMC_STRUCT.iNumParticipants) / TO_FLOAT(g_FMMC_STRUCT.iMaxNumberOfTeams))
					HANDLE_EXTRA_SPAWN_POINTS(sTeamSpawnStruct, iMaxSpawnsPerTeam, iTeamSpawnLocations, sInvisibleObjects)	
				ENDIF
			ELSE
				IF NETWORK_IS_GAME_IN_PROGRESS()
					MAINTAIN_MP_AMBIENT_MANAGER()
					IF bTextSetUp = TRUE
						Maintain_MP_Objective_Text()
						Maintain_MP_Communications()
					ENDIF
					HANDLE_FORCE_PLAYERS_FROM_CAR(PLAYER_ID())
					MAINTAIN_FORCE_OUT_OF_VEHICLE()
					#IF IS_DEBUG_BUILD	
						MAINTAIN_J_SKIP_WARP()
					#ENDIF
					RENDER_INVENTORY_HUD(on_mission_gang_box)
					MAINTAIN_BIG_MESSAGE() 
					PROCESS_LEADERBOARD_CAM()
					
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(sFMMCdata.iBitSet, bMenuJustChangedPlacementBlocker)
			PRINTLN("[LM] - Creator Bit - Clearing bMenuJustChangedPlacementBlocker")
			CLEAR_BIT(sFMMCdata.iBitSet, bMenuJustChangedPlacementBlocker)
		ENDIF
		
		//If we've been signed out the wait for Brenda to dealwith cleanup
		IF g_bKillFMMC_SignedOut
			SCRIPT_CLEANUP(TRUE)
			PRINTLN("g_bKillFMMC_SignedOut = TRUE")
		ENDIF
		
		IF g_Private_Gamemode_Current = GAMEMODE_FM
		AND NETWORK_IS_GAME_IN_PROGRESS()
			PRINTLN("[LH] A Network Game is in Progress! Cleaning up Creator.")
			SCRIPT_CLEANUP(TRUE)
		ENDIF
		
		//Quit if the player is in singleplayer
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(hash("main")) != 0
			PRINTLN("[LH] The player has escaped to singleplayer! Cleaning up Creator.")
			SCRIPT_CLEANUP(TRUE)
		ENDIF
		
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(hash("freemode")) != 0
			PRINTLN("The player has escaped to freemode! Cleaning up Creator.")
			SCRIPT_CLEANUP(TRUE)
			NETWORK_BAIL_FROM_CREATOR()
		ENDIF
		
		IF sCurrentVarsStruct.iEntityCreationStatus != STAGE_DEAL_WITH_RECREATING_MISSION
			MAINTAIN_TEST_MISSION_STATE()
		ENDIF
		#IF IS_DEBUG_BUILD		
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF		
		IF g_bDisableCreatorForceTest
			sCurrentVarsStruct.creationStats.bMadeAChange = FALSE
			sCurrentVarsStruct.creationStats.iTimesTestedLoc = 1
		ENDIF
		IF bSetSomeLimits
			g_FMMC_STRUCT.sFMMCEndConditions[0].iTargetScore[0] = FMMC_TARGET_SCORE_1
			g_FMMC_STRUCT.sFMMCEndConditions[1].iTargetScore[0] = FMMC_TARGET_SCORE_1
			g_FMMC_STRUCT.sFMMCEndConditions[2].iTargetScore[0] = FMMC_TARGET_SCORE_1
			g_FMMC_STRUCT.sFMMCEndConditions[3].iTargetScore[0] = FMMC_TARGET_SCORE_1
			g_FMMC_STRUCT.sFMMCEndConditions[0].iObjectiveTimeLimitRule[0] = FMMC_OBJECTIVE_TIME_2_MINUTES
			g_FMMC_STRUCT.sFMMCEndConditions[1].iObjectiveTimeLimitRule[0] = FMMC_OBJECTIVE_TIME_2_MINUTES
			g_FMMC_STRUCT.sFMMCEndConditions[2].iObjectiveTimeLimitRule[0] = FMMC_OBJECTIVE_TIME_2_MINUTES
			g_FMMC_STRUCT.sFMMCEndConditions[3].iObjectiveTimeLimitRule[0] = FMMC_OBJECTIVE_TIME_2_MINUTES
			bSetSomeLimits = FALSE
		ENDIF
		#ENDIF
	ENDWHILE
ENDSCRIPT

