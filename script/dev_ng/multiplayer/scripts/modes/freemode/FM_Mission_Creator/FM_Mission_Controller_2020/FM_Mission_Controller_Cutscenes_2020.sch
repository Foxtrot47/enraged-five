// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Cutscenes ---------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: This header contains the logic that drives all cutscenes for the Mission controller.
// ##### Scripted Cutscenes. Cameras, players and placed entities are tasked in this controlled sequence.
// ##### Mocap Cutscenes. Motion Captured mid-mission and end-mission cutscenes are performed within this header.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_EntityObjectives_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cutscene Entity Utility/Helper functions  																								 -----------------------
// ##### Description: Functions that are or can be used by both Motion Capture and Scripted cutscenes fall under this banner.									 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC BLOCK_INPUTS_FOR_CUTSCENE_PROCEDURES()	
	
	PRINTLN("[Cutscenes] - CUTSCENE IS BEING PROCESSED. BLOCKING VARIOUS INPUTS")
	
	// Attacking		
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	
	// Vehicle
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SPECIAL)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	
	// Misc
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT_SECONDARY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_THROW_GRENADE)
	
ENDPROC

PROC CLEAR_CUTSCENE_PRIMARY_PLAYER(INT iTeam)

	IF MC_serverBD.piCutscenePlayerIDs[iTeam][0] != INVALID_PLAYER_INDEX()
	
		INT iOldPart = -1
	
		IF IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[iTeam][0], FALSE)
			PARTICIPANT_INDEX oldPart = NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[iTeam][0])
			iOldPart = NATIVE_TO_INT(oldPart)
		ENDIF
		
		IF iOldPart > -1
			CLEAR_BIT(MC_serverBD.iCutscenePlayerBitset[iTeam],iOldPart)
			
			INT iOtherTeam
			FOR iOtherTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
				IF DOES_TEAM_LIKE_TEAM(iOtherTeam, iTeam)
					CLEAR_BIT(MC_serverBD.iCutscenePlayerBitset[iOtherTeam], iOldPart)
				ENDIF
			ENDFOR
			
			PRINTLN("[Cutscenes] clearing old primary cutscene part bitset: ", iOldPart," for team: ", iTeam)
		ENDIF
		
	ENDIF

ENDPROC

PROC SET_CUTSCENE_PRIMARY_PLAYER(INT iPart, INT iteam)
	
	IF IS_BIT_SET(MC_serverBD.iServerBitset8, SBBOOL8_PREVENT_CUTSCENE_PRIMARY_PLAYER_FROM_BEING_ADDED)
		PRINTLN("[Cutscenes] SET_CUTSCENE_PRIMARY_PLAYER - iteam: ", iteam, " iPart: ", iPart, " - SBBOOL8_PREVENT_CUTSCENE_PRIMARY_PLAYER_FROM_BEING_ADDED is set, BAIL")
		EXIT
	ENDIF
	
	PARTICIPANT_INDEX partID = INT_TO_PARTICIPANTINDEX(iPart)
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(partID)
		PRINTLN("[Cutscenes] SET_CUTSCENE_PRIMARY_PLAYER - iteam: ", iteam, " iPart: ", iPart, " - Participant not active, BAIL")
		EXIT
	ENDIF
	
	PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(partID)								
	IF NOT IS_NET_PLAYER_OK(playerID)
		PRINTLN("[Cutscenes] SET_CUTSCENE_PRIMARY_PLAYER - iteam: ", iteam, " iPart: ", iPart, " partID: ", NATIVE_TO_INT(partID), " playerID: ", NATIVE_TO_INT(playerID)," - player NOT OK, BAIL")
		EXIT
	ENDIF

	CLEAR_CUTSCENE_PRIMARY_PLAYER(iTeam)

	MC_serverBD.piCutscenePlayerIDs[iteam][0] = playerID
	SET_BIT(MC_serverBD.iCutscenePlayerBitset[iteam], iPart)
	PRINTLN("[Cutscenes] SET_CUTSCENE_PRIMARY_PLAYER - iteam: ", iteam, " iPart: ", iPart, " partID: ", NATIVE_TO_INT(partID), " playerID: ", NATIVE_TO_INT(playerID)," - SET as PRIMARY PLAYER")
	
	INT iOtherTeam
	FOR iOtherTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		IF DOES_TEAM_LIKE_TEAM(iOtherTeam, iteam)
		
			MC_serverBD.piCutscenePlayerIDs[iOtherTeam][0] = playerID
			SET_BIT(MC_serverBD.iCutscenePlayerBitset[iOtherTeam], iPart)
			
			PRINTLN("[Cutscenes] SET_CUTSCENE_PRIMARY_PLAYER - iteam: ", iteam, " iPart: ", iPart, " partID: ", NATIVE_TO_INT(partID), " playerID: ", NATIVE_TO_INT(playerID)," - SET as PRIMARY PLAYER for iOtherTeam: ", iOtherTeam)
		ENDIF
	ENDFOR

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cutscene Utility/Helper functions  																										 -----------------------
// ##### Description: Functions that are or can be used by both Motion Capture and Scripted cutscenes fall under this banner.									 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL ARE_ALL_PLAYERS_SYNCED_FOR_INTRO_CUTSCENE_RULE()
	
	SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_READY_FOR_INTRO_CUTSCENE_RULE)
	   
	PLAYER_INDEX piPlayer
	PARTICIPANT_INDEX piPart
	INT iPart = 0	
	FOR iPart = 0 TO (MAX_NUM_MC_PLAYERS - 1)	
		PRINTLN("[Cutscene_Intro] ARE_ALL_PLAYERS_SYNCED_FOR_INTRO_CUTSCENE_RULE - Checking part: ", iPart)
		
		piPart = INT_TO_PARTICIPANTINDEX(iPart)
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
			RELOOP
		ENDIF
		
		piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
		
		// Spectators transition differently and they won't set the same data as clients for some intro cutscenes so it's dangerous to wait around for them in here.
		IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piPlayer)
		OR IS_PLAYER_SCTV(piPlayer)
			RELOOP
		ENDIF
	
		IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_READY_FOR_INTRO_CUTSCENE_RULE)
			PRINTLN("[Cutscene_Intro] ARE_ALL_PLAYERS_SYNCED_FOR_INTRO_CUTSCENE_RULE - part: ", iPart, " is not ready")
			RETURN FALSE
		ELSE
			PRINTLN("[Cutscene_Intro] ARE_ALL_PLAYERS_SYNCED_FOR_INTRO_CUTSCENE_RULE - part: ", iPart, " READY.")
		ENDIF
		
	ENDFOR
	
	PRINTLN("[Cutscene_Intro] ARE_ALL_PLAYERS_SYNCED_FOR_INTRO_CUTSCENE_RULE - Returning True.")
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL ARE_ALL_PLAYERS_PLAYING_INTRO_CUTSCENE_RULE()
	
	PLAYER_INDEX piPlayer
	PARTICIPANT_INDEX piPart
	INT iPart = 0	
	FOR iPart = 0 TO (MAX_NUM_MC_PLAYERS - 1)	
		PRINTLN("[Cutscene_Intro] ARE_ALL_PLAYERS_PLAYING_INTRO_CUTSCENE_RULE - Checking part: ", iPart)
		
		piPart = INT_TO_PARTICIPANTINDEX(iPart)
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
			RELOOP
		ENDIF
		
		piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
		
		// Spectators transition differently and they won't set the same data as clients for some intro cutscenes so it's dangerous to wait around for them in here.
		IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piPlayer)
		OR IS_PLAYER_SCTV(piPlayer)
			RELOOP
		ENDIF
	
		IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE)
			PRINTLN("[Cutscene_Intro] ARE_ALL_PLAYERS_PLAYING_INTRO_CUTSCENE_RULE - part: ", iPart, " is not ready")
			RETURN FALSE
		ELSE
			PRINTLN("[Cutscene_Intro] ARE_ALL_PLAYERS_PLAYING_INTRO_CUTSCENE_RULE - part: ", iPart, " READY.")
		ENDIF
		
	ENDFOR
	
	PRINTLN("[Cutscene_Intro] ARE_ALL_PLAYERS_PLAYING_INTRO_CUTSCENE_RULE - Returning True.")
	
	RETURN TRUE
	
ENDFUNC

PROC CHECK_FOR_END_CUTSCENE_VARIATION_CHANGE()

	//Check for a variation change and update the mocap for all teams
	IF MC_ServerBD.iEndMocapVariation != -1
		IF MC_ServerBD.iEndCutscene != MC_ServerBD.iEndMocapVariation
			PRINTLN("[Cutscenes] CHECK_FOR_END_CUTSCENE_VARIATION_CHANGE - Updating MC_ServerBD.iEndCutscene from ",MC_ServerBD.iEndCutscene," to equal MC_ServerBD.iEndMocapVariation = ",MC_ServerBD.iEndMocapVariation)
			MC_ServerBD.iEndCutscene = MC_ServerBD.iEndMocapVariation
		ENDIF
	ENDIF
		
ENDPROC

PROC CLEANUP_MOCAP_PLAYER_CLONES()
	
	INT iplayer
	FOR iplayer = 0 TO (FMMC_MAX_CUTSCENE_PLAYERS-1)
		IF DOES_ENTITY_EXIST(MocapPlayerPed[iplayer])
			DELETE_PED(MocapPlayerPed[iplayer])
		ENDIF
	ENDFOR
	
ENDPROC

PROC CLEANUP_CUTSCENE_SPECIAL_CASE_LOGIC(STRING mocapName)
	IF ARE_STRINGS_EQUAL(mocapName, "MPCAS5_EXT")
		PRINTLN("[Cutscenes][CLEANUP_CUTSCENE_SPECIAL_CASE_LOGIC][Cutscene_Mocap] - MPCAS5_EXT - casino_manager_workout")
				
		// Ensure interior index is valid
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[Cutscenes] INTERIOR_V_CASINO_APARTMENT iInteriorIndex = NULL Set up failed")
		ENDIF		
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "casino_manager_workout")
				DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "casino_manager_workout")
				PRINTLN("REMOVE_CASINO_ENTITY_SET - Deactivating: 'casino_manager_workout'")
			ELSE
				PRINTLN("REMOVE_CASINO_ENTITY_SET - BAIL: Entity set 'casino_manager_workout' is not active so can't deactivate")
			ENDIF
		ELSE
			PRINTLN("REMOVE_CASINO_ENTITY_SET - BAIL: Interior is invalid")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_SEE_MOCAP_INTERIOR(/*STRING strMocapName, VECTOR &vLocation*/)
	
	RETURN FALSE
ENDFUNC

//Some mocaps require an interior streamed for the spectator
//For cutscenes that aren't in apartments
FUNC BOOL LOAD_INTERIOR_FOR_SPECTATOR_MOCAP( STRING strMocapName )

	UNUSED_PARAMETER(strMocapName)
	
	VECTOR vInteriorLocation

	IF IS_PLAYER_SPECTATING(LocalPlayer)
		IF SHOULD_SEE_MOCAP_INTERIOR()
			PRINTLN("[Cutscenes] LOAD_INTERIOR_FOR_SPECTATOR_MOCAP HAVE VALID CUTSCENE TO STREAM INTERIOR")
			IF IS_VALID_INTERIOR(iCutInterior)
				IF IS_INTERIOR_READY(iCutInterior)
					PRINTLN("[Cutscenes] LOAD_INTERIOR_FOR_SPECTATOR_MOCAP INTERIOR IS READY")
					RETURN TRUE
				ELSE
					PRINTLN("[Cutscenes] LOAD_INTERIOR_FOR_SPECTATOR_MOCAP INTERIOR ISN'T READY")
				ENDIF
			ELSE
				IF NOT ARE_VECTORS_EQUAL(vInteriorLocation, <<0,0,0>>)
					iCutInterior = GET_INTERIOR_AT_COORDS(vInteriorLocation)
					IF IS_VALID_INTERIOR(iCutInterior)
						PRINTLN("[Cutscenes] LOAD_INTERIOR_FOR_SPECTATOR_MOCAP PINNING INTERIOR IN MEMORY")
						PIN_INTERIOR_IN_MEMORY(iCutInterior)
					ENDIF
				ELSE
					PRINTLN("[Cutscenes] LOAD_INTERIOR_FOR_SPECTATOR_MOCAP INVALID INTERIOR SUPPLIED")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_PLAYERS_FACE_DIRECTION_IN_COVER(INT &iCutsceneBitset3, INT iPlayerIndex)
	SWITCH iPlayerIndex
		CASE 0	RETURN IS_BIT_SET(iCutsceneBitset3, ci_CSBS3_ForceFaceLeftInCoverPlayer0)
		CASE 1	RETURN IS_BIT_SET(iCutsceneBitset3, ci_CSBS3_ForceFaceLeftInCoverPlayer1)
		CASE 2	RETURN IS_BIT_SET(iCutsceneBitset3, ci_CSBS3_ForceFaceLeftInCoverPlayer2)
		CASE 3	RETURN IS_BIT_SET(iCutsceneBitset3, ci_CSBS3_ForceFaceLeftInCoverPlayer3)
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_CUTSCENE_RESET_GAMEPLAY_CAMERA_PITCH_OR_HEADING(FMMC_CUTSCENE_TYPE eCutType, INT iCutsceneToUse)
	
	IF IS_BIT_SET(iSpecificCutsceneBitset2, ciSpecificCutsceneBitset2_SET_INTO_COVER)
		RETURN FALSE
	ENDIF
	
	IF PERFORMING_VEHICLE_DRIVEBY_SEQUENCE()
		RETURN FALSE
	ENDIF
		
	IF eCutType = FMMCCUT_SCRIPTED
		IF iCutsceneToUse >= -1
		AND iCutsceneToUse < _POST_CASINO_HEIST__MAX_SCRIPTED_CUTSCENES
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_EndCutsceneInCover)
			OR IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset3, ci_CSBS3_PutPlayerIntoCoverDuringCutscene)	
			OR IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_SyncSceneBreakoutEndsCutscene)
				RETURN FALSE
			ENDIF
		ENDIF
	ELIF eCutType = FMMCCUT_MOCAP
		IF iCutsceneToUse >= -1
		AND iCutsceneToUse < MAX_MOCAP_CUTSCENES
			IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_EndCutsceneInCover)
			OR IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset3, ci_CSBS3_PutPlayerIntoCoverDuringCutscene)	
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC

PROC SET_PLAYER_INTO_COVER_AFTER_CUTSCENE(BOOL bForceFaceLeft)

	IF IS_BIT_SET(iSpecificCutsceneBitset2, ciSpecificCutsceneBitset2_SET_INTO_COVER)	
		PRINTLN("[Cutscenes] PROCESS_SCRIPTED_CUTSCENE / PROCESS_MOCAP_CUTSCENE - SET_PLAYER_INTO_COVER_AFTER_CUTSCENE - ciSpecificCutsceneBitset2_SET_INTO_COVER already set...")
		EXIT
	ENDIF
	
	//FLOAT fHead
	
	IF NOT IS_PED_IN_COVER(localPlayerPed)
	AND NOT IS_PED_GOING_INTO_COVER(localPlayerPed)
		
		PRINTLN("[Cutscenes] PROCESS_SCRIPTED_CUTSCENE / PROCESS_MOCAP_CUTSCENE - SET_PLAYER_INTO_COVER_AFTER_CUTSCENE - Setting player to enter cover NOW!")
		
		CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
		
		VECTOR vCoverPos = GET_ENTITY_COORDS(LocalPlayerPed)
		
		COVERPOINT_INDEX cpIndex
		cpIndex = GET_CLOSEST_COVER_POINT_TO_LOCATION(GET_ENTITY_COORDS(LocalPlayerPed), vCoverPos)	
		SET_PED_TO_LOAD_COVER(LocalPlayerPed, TRUE)
		
		IF cpIndex != NULL
			TASK_WARP_PED_DIRECTLY_INTO_COVER(localPlayerPed, -1, FALSE, TRUE, bForceFaceLeft, cpIndex)
			PRINTLN("[Cutscenes] PROCESS_SCRIPTED_CUTSCENE / PROCESS_MOCAP_CUTSCENE - SET_PLAYER_INTO_COVER_AFTER_CUTSCENE - using TASK_WARP_PED_DIRECTLY_INTO_COVER, with cpIndex at vCoverPos: ", vCoverPos)
		ELSE
			IF IS_VECTOR_ZERO(vCoverPos)
				vCoverPos = GET_ENTITY_COORDS(LocalPlayerPed)
			ENDIF
			TASK_PUT_PED_DIRECTLY_INTO_COVER(LocalPlayerPed, vCoverPos, -1, TRUE, DEFAULT, TRUE, bForceFaceLeft)			
			PRINTLN("[Cutscenes] PROCESS_SCRIPTED_CUTSCENE / PROCESS_MOCAP_CUTSCENE - SET_PLAYER_INTO_COVER_AFTER_CUTSCENE - using TASK_PUT_PED_DIRECTLY_INTO_COVER, with vCoverPos: ", vCoverPos)		
		ENDIF
		
		FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, TRUE)
		
		// Might not be necessary. If it's not remove it when new version of TASK_PUT_PED_DIRECTLY_INTO_COVER is implemented. Currently the gameplay cam position is correct, because the player faces the cover instead of along it. 
		// New native rotates the player which also might rotate the gameplay camera at "0" will be parallel to the geometry we are using as cover headin which is why we may have to keep this functionality.
		/*fHead = 90.0
		IF bForceFaceLeft
			fHead = -90.0
			PRINTLN("[Cutscenes] PROCESS_SCRIPTED_CUTSCENE / PROCESS_MOCAP_CUTSCENE - SET_PLAYER_INTO_COVER_AFTER_CUTSCENE - Forcing the player to face left in cover.")
		ELSE
			PRINTLN("[Cutscenes] PROCESS_SCRIPTED_CUTSCENE / PROCESS_MOCAP_CUTSCENE - SET_PLAYER_INTO_COVER_AFTER_CUTSCENE - Forcing the player to face right in cover.")
		ENDIF		
		PRINTLN("[Cutscenes] - PROCESS_SCRIPTED_CUTSCENE / PROCESS_MOCAP_CUTSCENE - SET_PLAYER_INTO_COVER_AFTER_CUTSCENE - Calling SET_GAMEPLAY_CAM_RELATIVE_HEADING: ", fHead)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(fHead)*/		
		
		SET_BIT(iSpecificCutsceneBitset2, ciSpecificCutsceneBitset2_SET_INTO_COVER)
		SET_BIT(iLocalBoolCheck29, LBOOL29_KEEP_TASKS_AFTER_CUTSCENE)
	ENDIF
	
ENDPROC

//Turn off the vehicle Radio on the cutscene
PROC TURN_OFF_CUTSCENE_VEHICLE_RADIO()
	IF bLocalPlayerPedOK
		IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
			VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
			
			IF IS_VEHICLE_DRIVEABLE(vehPlayer)
				STRING sStationName = GET_PLAYER_RADIO_STATION_NAME()
				IF IS_STRING_NULL_OR_EMPTY(sStationName)
				OR NOT ARE_STRINGS_EQUAL(sStationName, "OFF")
					// Cache it to re-enable it after cutscene.
					sCurrentRadioStation = sStationName
					IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
						SET_RADIO_TO_STATION_NAME("OFF")
						SET_VEH_FORCED_RADIO_THIS_FRAME(vehPlayer)
					ENDIF
					PRINTLN("[LM][TURN_OFF_CUTSCENE_VEHICLE_RADIO] - Setting Radio Station to OFF and caching the previous station as :", sCurrentRadioStation)
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
ENDPROC

PROC REDUCE_VOICE_CHAT_FOR_SPECIFIC_CUTSCENES(FMMC_CUTSCENE_TYPE eCutType)

	IF eCutType = FMMCCUT_SCRIPTED	
		IF IS_BIT_SET(g_fmmc_struct.sCurrentSceneData.iCutsceneBitSet3, ci_CSBS3_BlockVoiceChat)
			NETWORK_SET_VOICE_ACTIVE(FALSE)
		ENDIF	
	ELIF eCutType = FMMCCUT_MOCAP
		IF IS_BIT_SET(g_fmmc_struct.sCurrentMocapSceneData.iCutsceneBitSet3, ci_CSBS3_BlockVoiceChat)
			NETWORK_SET_VOICE_ACTIVE(FALSE)
		ENDIF
	ENDIF
ENDPROC 

FUNC BOOL CAN_REMOVE_MASKS_FOR_HEIST_CUTSCENE(STRING cutscene, BOOL bEarly = TRUE, FMMC_CUTSCENE_TYPE eCutType = FMMCCUT_MOCAP, INT iCutsceneToUse = -1)
	
	IF eCutType = FMMCCUT_MOCAP
		IF iCutsceneToUse > -1
			IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_KeepHatsAndGlassesEtc)
				PRINTLN("[Cutscenes][Mask] CAN_REMOVE_MASKS_FOR_HEIST_CUTSCENE - cutscene: ", cutscene, ", bEarly: ", bEarly, " FMMCCUT_MOCAP - ci_CSBS2_KeepHatsAndGlassesEtc is set RETURN FALSE")
				RETURN FALSE
			ENDIF
		ENDIF
	ELIF eCutType = FMMCCUT_ENDMOCAP
		IF IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet2, ci_CSBS2_KeepHatsAndGlassesEtc)
			PRINTLN("[Cutscenes][Mask] CAN_REMOVE_MASKS_FOR_HEIST_CUTSCENE - cutscene: ", cutscene, ", bEarly: ", bEarly, " FMMCCUT_ENDMOCAP - ci_CSBS2_KeepHatsAndGlassesEtc is set RETURN FALSE")
			RETURN FALSE
		ENDIF
	ENDIF
	
	PRINTLN("[Cutscenes] CAN_REMOVE_MASKS_FOR_HEIST_CUTSCENE - cutscene: ", cutscene, ", bEarly: ", bEarly, " RETURN TRUE")
	RETURN TRUE

ENDFUNC

FUNC BOOL CAN_REMOVE_HEIST_GEAR_FOR_CUTSCENE(STRING cutscene, BOOL bEarly = TRUE, FMMC_CUTSCENE_TYPE eCutType = FMMCCUT_MOCAP, INT iCutsceneToUse = -1)
	IF eCutType = FMMCCUT_MOCAP
		IF iCutsceneToUse > -1
			IF IS_BIT_SET(g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_RemoveHeistGearForCutscene)
				PRINTLN("[Cutscenes] CAN_REMOVE_HEIST_GEAR_FOR_CUTSCENE - cutscene: ", cutscene, ", bEarly: ", bEarly, " FMMCCUT_MOCAP - ci_CSBS3_RemoveHeistGearForCutscene is set RETURN TRUE")
				RETURN TRUE
			ENDIF
		ENDIF
	ELIF eCutType = FMMCCUT_ENDMOCAP
		IF IS_BIT_SET(g_fmmc_struct.sEndMocapSceneData.iCutsceneBitSet3, ci_CSBS3_RemoveHeistGearForCutscene)
			PRINTLN("[Cutscenes] CAN_REMOVE_HEIST_GEAR_FOR_CUTSCENE - cutscene: ", cutscene, ", bEarly: ", bEarly, " FMMCCUT_ENDMOCAP - ci_CSBS3_RemoveHeistGearForCutscene is set RETURN TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	PRINTLN("[Cutscenes] CAN_REMOVE_HEIST_GEAR_FOR_CUTSCENE - cutscene: ", cutscene, ", bEarly: ", bEarly, " RETURN FALSE	")
	RETURN FALSE
ENDFUNC

PROC REMOVE_MASKS_AND_REBREATHERS_FOR_CUTSCENES(PED_INDEX mocap_ped, PLAYER_INDEX PlayerIndex, MP_OUTFIT_MASK_ENUM eMask = OUTFIT_MASK_NONE, BOOL bRemoveHeistGear = FALSE)
	
	IF SHOULD_PLAYER_PED_MODEL_BE_SWAPPED(GET_PRE_ASSIGNMENT_PLAYER_TEAM())
		PRINTLN("[Cutscenes][Mask] HIDE_MASKS_AND_REBREATHERS_FOR_CUTSCENES Exiting early as not playing as a ped that has outfits")
		EXIT
	ENDIF
	
	INT iPlayerIndex = NATIVE_TO_INT(PlayerIndex)
	
	IF IS_MP_HEIST_GEAR_EQUIPPED(mocap_ped, HEIST_GEAR_REBREATHER)
		REMOVE_MP_HEIST_GEAR(mocap_ped,HEIST_GEAR_REBREATHER,iPlayerIndex)
		PRINTLN("[Cutscenes][Mask] HIDE_MASKS_AND_REBREATHERS_FOR_CUTSCENES take off masks 	REMOVE_MP_HEIST_GEAR(mocap_ped,HEIST_GEAR_REBREATHER)")
	ENDIF
	
	IF IS_MP_HEIST_GEAR_EQUIPPED(mocap_ped,  HEIST_GEAR_BALACLAVA)
		REMOVE_MP_HEIST_GEAR(mocap_ped,HEIST_GEAR_BALACLAVA,iPlayerIndex)
		PRINTLN("[Cutscenes][Mask] HIDE_MASKS_AND_REBREATHERS_FOR_CUTSCENES take off masks 	REMOVE_MP_HEIST_GEAR(mocap_ped,HEIST_GEAR_BALACLAVA)")
	ENDIF
	
	IF IS_MP_HEIST_GEAR_EQUIPPED(mocap_ped,  HEIST_GEAR_GAS_MASK)
		REMOVE_MP_HEIST_GEAR(mocap_ped,HEIST_GEAR_GAS_MASK,iPlayerIndex)
		PRINTLN("[Cutscenes][Mask] HIDE_MASKS_AND_REBREATHERS_FOR_CUTSCENES take off masks 	REMOVE_MP_HEIST_GEAR(mocap_ped,HEIST_GEAR_GAS_MASK)")
	ENDIF
	
	IF eMask != OUTFIT_MASK_NONE
		IF IS_MP_HEIST_GEAR_EQUIPPED(mocap_ped,  HEIST_GEAR_CORONA_MASK,eMask)
			REMOVE_MP_HEIST_GEAR(mocap_ped,HEIST_GEAR_CORONA_MASK,iPlayerIndex)
			PRINTLN("[Cutscenes][Mask] HIDE_MASKS_AND_REBREATHERS_FOR_CUTSCENES take off masks 	REMOVE_MP_HEIST_GEAR(mocap_ped,HEIST_GEAR_CORONA_MASK)")
		ENDIF
	ELSE
		PRINTLN("[Cutscenes][Mask] HIDE_MASKS_AND_REBREATHERS_FOR_CUTSCENES no mask set")
	ENDIF
	
	IF IS_MP_HEIST_GEAR_EQUIPPED(mocap_ped,  HEIST_GEAR_NIGHTVISION)
		REMOVE_MP_HEIST_GEAR(mocap_ped,HEIST_GEAR_NIGHTVISION,iPlayerIndex)
		PRINTLN("[Cutscenes][Mask] HIDE_MASKS_AND_REBREATHERS_FOR_CUTSCENES take off masks 	REMOVE_MP_HEIST_GEAR(mocap_ped,HEIST_GEAR_NIGHTVISION)")
	ENDIF
	
	SET_BAG_FOR_HEIST(MC_playerBD[iLocalPart].iteam, TRUE)
	
	MP_OUTFITS_APPLY_DATA sApplyDataRemoveMask
	sApplyDataRemoveMask.pedID        	= mocap_ped
    sApplyDataRemoveMask.eMask        	= OUTFIT_MASK_NONE
    sApplyDataRemoveMask.eApplyStage  	= AOS_SET
	sApplyDataRemoveMask.iPlayerIndex  	= iPlayerIndex
	IF !bRemoveHeistGear
		sApplyDataRemoveMask.eGear		= sApplyOutfitData.eGear
	ENDIF
	
	PRINTLN("[Cutscenes][Mask] HIDE_MASKS_AND_REBREATHERS_FOR_CUTSCENES - calling SET_PED_MP_OUTFIT with OUTFIT_MASK_NONE")
	SET_PED_MP_OUTFIT(sApplyDataRemoveMask, TRUE, TRUE, FALSE)

ENDPROC

FUNC BOOL SHOULD_REMOVE_HELMETS_FOR_SCENE()
	
	IF IS_BIT_SET(g_fmmc_struct.sEndMocapSceneData.iCutsceneBitSet2, ci_CSBS2_RemoveHelmets)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_FADE_MOCAP_IN_AT_END(INT iCutsceneToUse)
	
	IF bIsAnySpectator
	AND NOT IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData) // Stop seeing under the world.
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_RUNNING_WALL_RAPPEL_NOT_ON_RAPPEL_RULE)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_UseExternalFadeIn)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC INT GET_MOCAP_CUTSCENE_FADE_IN_TIME()

	// Replaced a heist content specific string check. Need to make sure that this wrapper is used throughout.
	IF IS_BIT_SET(g_fmmc_struct.sCurrentMocapSceneData.iCutsceneBitSet3, ci_CSBS3_UseQuickMocapFade)
		RETURN MOCAP_START_FADE_IN_DURATION_QUICK
	ENDIF
	
	RETURN MOCAP_START_FADE_IN_DURATION_NORMAL
ENDFUNC

FUNC BOOL SHOULD_LOCAL_PLAYER_WATCH_TEAM_CUTSCENE( INT iteam, INT icutscene, FMMC_CUTSCENE_TYPE eCutType)
	
	INT iTeamCheck = MC_playerBD[iPartToUse].iteam
	
	IF IS_BIT_SET( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE )
		RETURN TRUE // Always watch a cutscene we've already started, even if something below changes
	ENDIF
	
	//Get the spectated peds team
	IF USING_HEIST_SPECTATE()
		PED_INDEX spectatorTarget = GET_SPECTATOR_CURRENT_FOCUS_PED()
		
		IF NOT DOES_ENTITY_EXIST( spectatorTarget )
		OR IS_PED_INJURED( spectatorTarget )
			PRINTLN("[LM][Cutscenes] - Checking if I should watch a cutscene as a Heist Spectator, and my spectator target isn't valid!" )
			RETURN FALSE
		ENDIF
	
		iTeamCheck = GET_PLAYER_TEAM( NETWORK_GET_PLAYER_INDEX_FROM_PED( spectatorTarget ) )
		PRINTLN("[LM][Cutscenes] USING HEIST SPECTATE CHECKING AGAINST SPECTATED PLAYERS TEAM : ", iTeamCheck )
	ENDIF

	IF iTeamCheck < 0 OR iTeamCheck >= FMMC_MAX_TEAMS
		PRINTLN("[Cutscenes] SHOULD_LOCAL_PLAYER_WATCH_TEAM_CUTSCENE - iTeamCheck is invalid! The value is ", iTeamCheck, " returning false.")
		RETURN FALSE
	ENDIF

	IF HAS_TEAM_FAILED( iTeamCheck )
		PRINTLN("[Cutscenes] SHOULD_LOCAL_PLAYER_WATCH_TEAM_CUTSCENE - Team ", iTeamCheck, " has failed, returning false.")
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[ iTeamCheck ] >= FMMC_MAX_RULES
		PRINTLN("[Cutscenes] SHOULD_LOCAL_PLAYER_WATCH_TEAM_CUTSCENE - Team ", iTeamCheck, " Rule is >= FMMC_MAX_RULES, returning false.")
		RETURN FALSE
	ENDIF

	IF iteam = iTeamCheck
		PRINTLN("[Cutscenes] SHOULD_LOCAL_PLAYER_WATCH_TEAM_CUTSCENE - Team ", iTeamCheck, " is = to team the cutscene is assigned to. Returning true.")
		RETURN TRUE
	ENDIF

	//MC_serverBD_4.iPlayerRule[iPlayerRule][iteam] = FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE 
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF eCutType = FMMCCUT_MOCAP
			SWITCH iTeamCheck
			
				CASE 0
					IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team0_In)
						PRINTLN("[Cutscenes] SHOULD_LOCAL_PLAYER_WATCH_TEAM_CUTSCENE - Team ", iTeamCheck, " Pull Team In - Mocap.")
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1
					IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team1_In)
						PRINTLN("[Cutscenes] SHOULD_LOCAL_PLAYER_WATCH_TEAM_CUTSCENE - Team ", iTeamCheck, " Pull Team In - Mocap.")
						RETURN TRUE
					ENDIF		
				BREAK
				CASE 2
					IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team2_In)
						PRINTLN("[Cutscenes] SHOULD_LOCAL_PLAYER_WATCH_TEAM_CUTSCENE - Team ", iTeamCheck, " Pull Team In - Mocap.")
						RETURN TRUE
					ENDIF		
				BREAK
				CASE 3
					IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team3_In)
						PRINTLN("[Cutscenes] SHOULD_LOCAL_PLAYER_WATCH_TEAM_CUTSCENE - Team ", iTeamCheck, " Pull Team In - Mocap.")
						RETURN TRUE
					ENDIF		
				BREAK
			ENDSWITCH		
		ELSE
			SWITCH MC_playerBD[iPartToUse].iteam
			
				CASE 0
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team0_In)
						PRINTLN("[Cutscenes] SHOULD_LOCAL_PLAYER_WATCH_TEAM_CUTSCENE - Team ", iTeamCheck, " Pull Team In - Scripted.")
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team1_In)
						PRINTLN("[Cutscenes] SHOULD_LOCAL_PLAYER_WATCH_TEAM_CUTSCENE - Team ", iTeamCheck, " Pull Team In - Scripted.")
						RETURN TRUE
					ENDIF		
				BREAK
				CASE 2
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team2_In)
						PRINTLN("[Cutscenes] SHOULD_LOCAL_PLAYER_WATCH_TEAM_CUTSCENE - Team ", iTeamCheck, " Pull Team In - Scripted.")
						RETURN TRUE
					ENDIF		
				BREAK
				CASE 3
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team3_In)
						PRINTLN("[Cutscenes] SHOULD_LOCAL_PLAYER_WATCH_TEAM_CUTSCENE - Team ", iTeamCheck, " Pull Team In - Scripted.")
						RETURN TRUE
					ENDIF		
				BREAK
			ENDSWITCH
		ENDIF
		
	ENDIF
	
	RETURN FALSE

ENDFUNC

///PURPOSE: This function will get the local player's index from the cutscene, otherwise return -1
FUNC INT GET_LOCAL_PLAYER_CUTSCENE_INDEX(INT iCutsceneTeam)

	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		RETURN 0
	ENDIF

	INT iCutscenePlayer
	FOR iCutscenePlayer = 0 TO FMMC_MAX_CUTSCENE_PLAYERS - 1
		PRINTLN("[Cutscenes_SPAM] GET_LOCAL_PLAYER_CUTSCENE_INDEX | MC_serverBD.piCutscenePlayerIDs[", iCutsceneTeam, "][", iCutscenePlayer, "] = ", NATIVE_TO_INT(MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iCutscenePlayer]))
		IF MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iCutscenePlayer] = LocalPlayer
			RETURN iCutscenePlayer
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

FUNC INT GET_PLAYER_FALLBACK_CUTSCENE_INDEX()

	INT iPlayerLoop
	PLAYER_INDEX tempPlayer
	INT iIndexToReturn
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		RETURN 0
	ENDIF

	iPlayerIndexForCutscene = Get_Bitfield_Of_Players_On_Or_Joining_Mission( Get_UniqueID_For_This_Players_Mission( LocalPlayer ) )

	PRINTLN("[PLAYER_LOOP] - GET_PLAYER_FALLBACK_CUTSCENE_INDEX")
	FOR iPlayerLoop = 0 TO ( NUM_NETWORK_PLAYERS - 1 )
		
		tempPlayer = INT_TO_PLAYERINDEX( iPlayerLoop )
		IF IS_NET_PLAYER_OK( tempPlayer, FALSE )
	       	IF tempPlayer != LocalPlayer
				IF IS_BIT_SET( iPlayerIndexForCutscene, iPlayerLoop )
					IF NOT IS_PLAYER_SCTV(tempPlayer)
					AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(tempPlayer)
					AND NOT IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR(tempPlayer)
						iIndexToReturn++
					ENDIF
				ENDIF
			ELSE // Break out if we find the local player
				iPlayerLoop = NUM_NETWORK_PLAYERS
			ENDIF
		ENDIF
	ENDFOR

	RETURN iIndexToReturn

ENDFUNC

//Check if the local player is in the cutscene
FUNC BOOL IS_LOCAL_PLAYER_PED_IN_CUTSCENE()

	IF IS_STRING_NULL_OR_EMPTY(tlPlayerSceneHandle)
		PRINTLN("[Cutscenes] IS_LOCAL_PLAYER_PED_IN_CUTSCENE() FALSE")
		RETURN FALSE
	ELSE
		PRINTLN("[Cutscenes] IS_LOCAL_PLAYER_PED_IN_CUTSCENE() TRUE")
		RETURN TRUE
	ENDIF
ENDFUNC

FUNC PLAYER_INDEX GET_CUTSCENE_PLAYER_SPECIAL_CASE()
	PLAYER_INDEX TempPlayer = INVALID_PLAYER_INDEX()
	
	// Add behaviour/options here if special behaviour is required to put certain players in certain slots in the cutscene
	
	RETURN TempPlayer
ENDFUNC

FUNC PLAYER_INDEX GET_HEIST_LEADER_PLAYER_FOR_TEAM(INT iteam, FMMC_CUTSCENE_TYPE cutType, INT iCutsceneToUse, INT iPlayer = 0)
	
	INT iBitsetToCheck
	SWITCH cutType
		CASE FMMCCUT_ENDMOCAP	iBitsetToCheck = g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet						BREAK
		CASE FMMCCUT_MOCAP	 	iBitsetToCheck = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet 		BREAK
		CASE FMMCCUT_SCRIPTED 	iBitsetToCheck = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet 	BREAK
	ENDSWITCH
	
	IF iPlayer = 0
	AND NOT IS_BIT_SET(iBitsetToCheck, ci_CSBS_Heist_Leader_Primary)
		RETURN INVALID_PLAYER_INDEX()
	ENDIF
	
	IF iPlayer = 1
	AND NOT IS_BIT_SET(iBitsetToCheck, ci_CSBS_Heist_Leader_Secondary)
		RETURN INVALID_PLAYER_INDEX()
	ENDIF
	
	IF iPlayer = 2
	AND NOT IS_BIT_SET(iBitsetToCheck, ci_CSBS2_Heist_Leader_Third)
		RETURN INVALID_PLAYER_INDEX()
	ENDIF
	
	IF iPlayer = 3
		RETURN INVALID_PLAYER_INDEX()
	ENDIF
	
	INT iPart
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPart
	
		IF GET_FMMC_LOBBY_LEADER_PARTICIPANT_INDEX() != iPart
			RELOOP
		ENDIF
	
		IF IS_BIT_SET(MC_serverBD.iCutscenePlayerBitset[iteam], iPart)
			PRINTLN("[Cutscenes] GET_HEIST_LEADER_PLAYER_FOR_TEAM - iTeam: ", iteam, " iPlayer: ", iPlayer, " - Heist leader already selected for another slot")
			RELOOP
		ENDIF
		
		PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)
		PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
		PRINTLN("[Cutscenes] GET_HEIST_LEADER_PLAYER_FOR_TEAM - iTeam: ", iteam, " iPlayer: ", iPlayer, " - Found heist leader")
		RETURN tempPlayer
	ENDREPEAT

	PRINTLN("[Cutscenes] GET_HEIST_LEADER_PLAYER_FOR_TEAM - iTeam: ", iteam, " iPlayer: ", iPlayer, " - No player selected")
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

FUNC INT GET_CUTSCENE_TEAM( BOOL bFindStreamingTeam = FALSE )

	INT iCutsceneIndexToUse[FMMC_MAX_TEAMS]
	
	IF NOT bFindStreamingTeam
	OR IS_BIT_SET( iLocalBoolCheck9, LBOOL9_FORCE_STREAM_CUTSCENE )
		IF iCachedCutsceneTeam != -2 //Uninitialised
			RETURN iCachedCutsceneTeam
		ENDIF
		COPY_INT_ARRAY_VALUES(MC_serverBD.iCutsceneID, iCutsceneIndexToUse)
	ELSE
		IF iCachedCutsceneStreamingTeam != -2 //Uninitialised
			RETURN iCachedCutsceneStreamingTeam
		ENDIF
		COPY_INT_ARRAY_VALUES(MC_serverBD.iCutsceneStreamingIndex, iCutsceneIndexToUse)
	ENDIF
	
	INT iteamloop
	INT iteam = -1
	INT iCutsceneToUse
	
	IF iCutsceneIndexToUse[MC_playerBD[iPartToUse].iteam] != -1
	OR IS_FAKE_MULTIPLAYER_MODE_SET()
		PRINTLN("[Cutscenes] GET_CUTSCENE_TEAM - (3) Assiging Team here.")
		iTeam = MC_playerBD[iPartToUse].iteam
	ELSE
		FOR iteamloop = 0 TO ( g_FMMC_STRUCT.iNumberOfTeams - 1 )
			
			FMMC_CUTSCENE_TYPE eCutType
			IF iCutsceneIndexToUse[ iteamloop ] >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
			AND iCutsceneIndexToUse[ iteamloop ] < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
				iCutsceneToUse = ( iCutsceneIndexToUse[ iteamloop ] - FMMC_GET_MAX_SCRIPTED_CUTSCENES() )
				eCutType = FMMCCUT_MOCAP
				IF iCutsceneToUse >= MAX_MOCAP_CUTSCENES
					RELOOP
				ENDIF
			ELSE
				iCutsceneToUse = iCutsceneIndexToUse[ iteamloop ]
				eCutType = FMMCCUT_SCRIPTED
				IF iCutsceneToUse >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
					RELOOP
				ENDIF
			ENDIF
			
			IF iCutsceneToUse > -1
			AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iCutsceneFinishedBitset, iCutsceneIndexToUse[iteamloop])
			
				IF SHOULD_LOCAL_PLAYER_WATCH_TEAM_CUTSCENE(iteamloop, iCutsceneToUse, eCutType)
					PRINTLN("[Cutscenes] GET_CUTSCENE_TEAM - (4) Assiging Team here.")
					iteam = iteamloop
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	PRINTLN("[Cutscenes] GET_CUTSCENE_TEAM - Returning iteam: ", iTeam)
	
	IF !bFindStreamingTeam
		iCachedCutsceneTeam = iTeam
	ENDIF
	
	IF bFindStreamingTeam
		iCachedCutsceneStreamingTeam = iTeam
	ENDIF


	RETURN iTeam

ENDFUNC

//Store the cutscene peds feet
FUNC BOOL STORE_FEMALE_PED_FEET(PED_INDEX pedID)

	INT iTempDraw
	INT iTempTex

	// If we are female, reset to no heels
	IF IS_PED_FEMALE(pedID)
		IF DOES_ENTITY_EXIST(pedID)
		AND NOT IS_ENTITY_DEAD(pedID)
			if iPedFeetDrawable = -1
			AND iPedFeetTexture = -1
				iTempDraw = GET_PED_DRAWABLE_VARIATION(pedID, PED_COMP_FEET) 
				iTempTex = GET_PED_TEXTURE_VARIATION(pedID, PED_COMP_FEET)
		
				iPedFeetDrawable = iTempDraw
				iPedFeetTexture = iTempTex
				
				#IF IS_DEBUG_BUILD
					PRINTLN("STORING FEMALE PED FEET!")
					PRINTLN("STORED DRAWABLE: ", iTempDraw, " STORED TEXTURE: ", iTempTex )
				#ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("PED ISNT FEMALE NOT STORING PED FEET!")
	#ENDIF
	
	RETURN FALSE
ENDFUNC

//Restore the female ped feet to the variation stored
PROC RESTORE_FEMALE_PED_FEET(PED_INDEX pedID)
	
	// If we are female, reset to the stored variation
	IF IS_PED_FEMALE(pedID)
		IF DOES_ENTITY_EXIST(pedID)
		AND NOT IS_ENTITY_DEAD(pedID)
			
			//Check valid component is stored
			if iPedFeetDrawable != -1
			AND iPedFeetTexture != -1
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_FEET, iPedFeetDrawable, iPedFeetTexture)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("RESTORED FEMALE PED FEET!")
					PRINTLN("RESTORED VARIATION DRAWABLE : ", iPedFeetDrawable, " TEXTURE : ", iPedFeetTexture)
				#ENDIF
				
				//Reset vars
				iPedFeetDrawable = -1
				iPedFeetTexture = -1
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CONCEAL_ALL_OTHER_PLAYERS(BOOL bHide, bool include_local_player = false, bool b_remotely_visible = false)
	
	INT iParticipant
	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX tempplayer

	PRINTLN("[Cutscene] - CONCEAL_ALL_OTHER_PLAYERS - bHide: ", bHide, "include_local_player: ", include_local_player, " b_remotely_visible: ", b_remotely_visible)

	PRINTLN("[Cutscene] - [PLAYER_LOOP] - CONCEAL_ALL_OTHER_PLAYERS")
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		
		tempPart = INT_TO_PARTICIPANTINDEX(iParticipant)
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			RELOOP
		ENDIF
		
		tempplayer = NETWORK_GET_PLAYER_INDEX(tempPart)
		
		IF NOT IS_NET_PLAYER_OK(tempplayer)
			RELOOP
		ENDIF
		
       	IF tempplayer = LocalPlayer
			RELOOP
		ENDIF

		PED_INDEX pedPlayer = GET_PLAYER_PED(tempPlayer)
		
		IF IS_PED_INJURED(pedPlayer)
			RELOOP
		ENDIF
		
		PRINTLN("[Cutscene] - CONCEAL_ALL_OTHER_PLAYERS - SET_ENTITY_VISIBLE_IN_CUTSCENE - bHide SET TO: ", bHide, " Participant: ", iParticipant)
		SET_ENTITY_VISIBLE_IN_CUTSCENE(pedPlayer, !bHide)
	
	ENDREPEAT
	
	IF include_local_player
		SET_ENTITY_VISIBLE_IN_CUTSCENE(LocalPlayerPed, !bHide, b_remotely_visible)
		PRINTLN("[Cutscene] - CONCEAL_ALL_OTHER_PLAYERS - SET_ENTITY_VISIBLE_IN_CUTSCENE - SET TO : ", !bHide, " b_remotely_visible:", b_remotely_visible)
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_USE_ALT_END_POS(USE_ALT_END_POS &eUseAltEndPos)
	IF eUseAltEndPos = USE_ALT_END_POS_DISABLED
		PRINTLN("[Cutscene] SHOULD_USE_ALT_END_POS - returning false as the eUseAltEndPos is disabled.")
		RETURN FALSE
	ENDIF
	
	IF eUseAltEndPos = USE_ALT_END_POS_IF_PLAYER_IS_ON_FOOT
		PRINTLN("[Cutscene] SHOULD_USE_ALT_END_POS - USE_ALT_END_POS_IF_PLAYER_IS_ON_FOOT - Checking the condition...")
		IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, FALSE)
			PRINTLN("[Cutscene] SHOULD_USE_ALT_END_POS - USE_ALT_END_POS_IF_PLAYER_IS_ON_FOOT - Using the alternative position!")
			RETURN TRUE
		ELSE
			PRINTLN("[Cutscene] SHOULD_USE_ALT_END_POS- USE_ALT_END_POS_IF_PLAYER_IS_ON_FOOT - Return false as the player is in a vehicle!")
			RETURN FALSE
		ENDIF		
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CUTSCENE_REMOVE_PLAYER_FROM_VEHICLE_AT_START(INT iCutsceneIndex, BOOL bIsScriptedCut)
	
	IF iCutsceneIndex < 0 
	OR (bIsScriptedCut AND iCutsceneIndex >= FMMC_GET_MAX_SCRIPTED_CUTSCENES())
	OR (!bIsScriptedCut AND iCutsceneIndex >= MAX_MOCAP_CUTSCENES)
		CPRINTLN(DEBUG_MISSION, "SHOULD_CUTSCENE_REMOVE_PLAYER_FROM_VEHICLE_AT_START - iCutsceneIndex: ", iCutsceneIndex, " is out of range")
		RETURN FALSE
	ENDIF
	
	BOOL bResult
	IF bIsScriptedCut
		bResult = IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneIndex].iCutsceneBitset, ci_CSBS_RemoveFromVehStart)
	ELSE
		bResult = IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneIndex].iCutsceneBitset, ci_CSBS_RemoveFromVehStart)
	ENDIF

	CPRINTLN(DEBUG_MISSION, "SHOULD_CUTSCENE_REMOVE_PLAYER_FROM_VEHICLE_AT_START - iCutsceneIndex: ", iCutsceneIndex, " bResult: ", BOOL_TO_STRING(bResult))
	RETURN bResult
ENDFUNC

PROC REMOVE_PLAYER_FROM_VEHICLE_AT_CUTSCENE_START(INT iCutscene, BOOL bIsScriptedCut)
	
	IF SHOULD_CUTSCENE_REMOVE_PLAYER_FROM_VEHICLE_AT_START(iCutscene, bIsScriptedCut)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)		
			VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			
			IF IS_PED_DRIVING_ANY_VEHICLE(LocalPlayerPed)
			AND (GET_ENTITY_MODEL(vehIndex) = DELUXO OR GET_ENTITY_MODEL(vehIndex) = OPPRESSOR2)
				SET_SPECIAL_FLIGHT_MODE_TARGET_RATIO(vehIndex, 0)				
				PRINTLN("[REMOVE_PLAYER_FROM_VEHICLE_AT_CUTSCENE_START] - Calling SET_SPECIAL_FLIGHT_MODE_TARGET_RATIO on Deluxo Vehicle")
			ENDIF
			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(vehIndex)
				IF IS_ENTITY_IN_AIR(vehIndex)
					PRINTLN("[REMOVE_PLAYER_FROM_VEHICLE_AT_CUTSCENE_START] - Entity is in the air, setting vehicle on ground.")
					SET_VEHICLE_ON_GROUND_PROPERLY(vehIndex, 1.0)
				ENDIF
			ENDIF			
			
			//Switching to this as the car was carrying on after being taken out of vehicle
			TASK_LEAVE_ANY_VEHICLE(LocalPlayerPed, 0, ECF_DONT_CLOSE_DOOR)
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: This function will try and put the ped into the seat
FUNC BOOL PLACE_PED_IN_VEHICLE_SEAT_IF_POSSIBLE(VEHICLE_INDEX& veh, PED_INDEX ped, VEHICLE_SEAT seat)
	
	IF NOT IS_VEHICLE_SEAT_FREE(veh, seat)
		BOOL bResult = IS_PED_SITTING_IN_VEHICLE_SEAT(ped, veh, seat)
		PRINTLN("PLACE_PED_IN_VEHICLE_SEAT_IF_POSSIBLE - seat ", seat, " is occupied, returning: ", BOOL_TO_STRING(bResult))
		RETURN bResult
	ENDIF
	
	IF seat != VS_DRIVER
		IF NETWORK_HAS_CONTROL_OF_ENTITY(ped)
			SET_PED_CONFIG_FLAG(ped, PCF_PreventAutoShuffleToDriversSeat, TRUE)
		ENDIF
	ENDIF

	CLEAR_PED_TASKS_IMMEDIATELY(ped)

	PRINTLN("PLACE_PED_IN_VEHICLE_SEAT_IF_POSSIBLE - seat ", ENUM_TO_INT(seat), " using TASK_ENTER_VEHICLE")
	TASK_ENTER_VEHICLE(ped, veh, -1, seat, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED | ECF_BLOCK_SEAT_SHUFFLING)	
	
	PRINTLN("PLACE_PED_IN_VEHICLE_SEAT_IF_POSSIBLE - tasking ped to take the seat ", seat)

	IF NETWORK_HAS_CONTROL_OF_ENTITY(ped)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(ped, TRUE, TRUE)
	ENDIF

	PRINTLN("PLACE_PED_IN_VEHICLE_SEAT_IF_POSSIBLE - ped put in seat")
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_THIS_CUTSCENE_WARP_INTO_AN_INTERIOR(INT iCutsceneToUse, VECTOR vPlayerPos, VECTOR vTargetCoord)
	
	IF vehSeatForcedFromSyncScene != VS_ANY_PASSENGER
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE - DOES_THIS_CUTSCENE_WARP_INTO_AN_INTERIOR - Doing some forced vehicle warp - long warp only")
		RETURN TRUE
	ENDIF
	
	// Script automated.
	IF NOT IS_VECTOR_ZERO(vPlayerPos)
	AND NOT IS_VECTOR_ZERO(vTargetCoord)
	
		INTERIOR_INSTANCE_INDEX interiorPlayer = GET_INTERIOR_AT_COORDS(vPlayerPos)
		INTERIOR_INSTANCE_INDEX interiorTarget = GET_INTERIOR_AT_COORDS(vTargetCoord)
		
		// if the interior instances do not match then we know we are either going in/out of an interior
		IF interiorPlayer != interiorTarget
			PRINTLN("PROCESS_SCRIPTED_CUTSCENE - DOES_THIS_CUTSCENE_WARP_INTO_AN_INTERIOR - Returning TRUE. player is moving between interior & outside or 2 different interiors")
			RETURN TRUE
		
		// they match so no interior warp is needed
		ELSE
			PRINTLN("PROCESS_SCRIPTED_CUTSCENE - DOES_THIS_CUTSCENE_WARP_INTO_AN_INTERIOR - Returning FALSE. player is not moving between interiors or interior/outside.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Old Manual setting which content used to do.
	RETURN IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Warp_Into_Interior)
ENDFUNC

FUNC BOOL HAS_POST_CUTSCENE_IDLE_ANIMATION(FMMC_CUTSCENE_TYPE eCutType, INT iCutsceneToUse)
	
	SWITCH eCutType
		
		CASE FMMCCUT_MOCAP
			SWITCH iScriptedCutsceneTeam
				CASE 0 RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Team0_IdleAnims)
				CASE 1 RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Team1_IdleAnims)
				CASE 2 RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Team2_IdleAnims)
				CASE 3 RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Team3_IdleAnims)
			ENDSWITCH
		BREAK
		
		CASE FMMCCUT_SCRIPTED
			SWITCH iScriptedCutsceneTeam
				CASE 0 RETURN IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Team0_IdleAnims) 
				CASE 1 RETURN IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Team1_IdleAnims) 
				CASE 2 RETURN IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Team2_IdleAnims) 
				CASE 3 RETURN IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Team3_IdleAnims) 
			ENDSWITCH	
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_PLAYER_DO_ROLLING_START_FOR_CUTSCENE(FMMC_CUTSCENE_TYPE eCutType, INT iCutscene)
	IF eCutType = FMMCCUT_MOCAP
		IF iCutscene >= -1
		AND iCutscene < MAX_MOCAP_CUTSCENES
			IF IS_BIT_SET(g_fmmc_struct.sMocapCutsceneData[iCutscene].iCutsceneBitset, ci_CSBS_Rolling_Start)
			AND NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_DidRollingStart)
				RETURN TRUE
			ENDIF
		ENDIF
	ELIF eCutType = FMMCCUT_SCRIPTED
		IF iCutscene >= -1
		AND iCutscene < _POST_CASINO_HEIST__MAX_SCRIPTED_CUTSCENES
			IF IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutscene].iCutsceneBitset, ci_CSBS_Rolling_Start)
			AND NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_DidRollingStart)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DO_POST_CUTSCENE_COMMON_PLAYER_EXIT(INT iCutsceneToUse, FMMC_CUTSCENE_TYPE eCutType, INT iPlayerIndex)
	
	IF NOT bLocalPlayerOK
		EXIT
	ENDIF
	
	PRINTLN("[Cutscenes] DO_POST_CUTSCENE_COMMON_PLAYER_EXIT - Started")
	
	FLOAT fVehicleRollingStartSpeed = 6.0
	BOOL bGoToCover
	INT iCutsceneBitset2
	SWITCH eCutType
		CASE FMMCCUT_MOCAP			
			iCutsceneBitset2 = g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2
			fVehicleRollingStartSpeed = g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].fCutsceneRollingStartSpeed
			bGoToCover = IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_EndCutsceneInCover)
		BREAK
		CASE FMMCCUT_SCRIPTED 			
			iCutsceneBitset2 = g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2
			fVehicleRollingStartSpeed = g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].fCutsceneRollingStartSpeed
			bGoToCover = IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_EndCutsceneInCover)
		BREAK
	ENDSWITCH
	
	IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		
		VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(localPlayerPed)
		IF IS_BIT_SET(iCutsceneBitset2, ci_CSBS2_Hover_Vehicle_On_End)	
			PRINTLN("[Cutscenes] DO_POST_CUTSCENE_COMMON_PLAYER_EXIT - ci_CSBS2_Hover_Vehicle_On_End")
			SET_HELI_HOVERING(vehIndex)
		ENDIF
	
	ENDIF
	
	IF SHOULD_PLAYER_DO_ROLLING_START_FOR_CUTSCENE(eCutType, iCutsceneToUse)
		PRINTLN("[Cutscenes] DO_POST_CUTSCENE_COMMON_PLAYER_EXIT - ci_CSBS_Rolling_Start")
		SET_PLAYER_ROLLING_START(DEFAULT, fVehicleRollingStartSpeed)
	ENDIF
	
	IF HAS_POST_CUTSCENE_IDLE_ANIMATION(eCutType, iCutsceneToUse) 
	
		PRINTLN("[Cutscenes] DO_POST_CUTSCENE_COMMON_PLAYER_EXIT - giving random re-entry idle anim.")
		START_RANDOM_RE_ENTRY_IDLE_ANIM()	
		
	ENDIF
	
	IF IS_BIT_SET(iCutsceneBitset2, ci_CSBS2_EquipWeaponAtEnd)
		
		PRINTLN("[Cutscenes] DO_POST_CUTSCENE_COMMON_PLAYER_EXIT - cutscene should equip weapon at end")
		IF wtPreCutsceneWeapon = WEAPONTYPE_UNARMED
		OR wtPreCutsceneWeapon = WEAPONTYPE_INVALID
			PUT_WEAPON_IN_HAND( WEAPONINHAND_LASTWEAPON_BOTH )
			PRINTLN("DO_POST_CUTSCENE_COMMON_PLAYER_EXIT - wtPreCutsceneWeapon is an invalid weapon. Putting last known weapon into hand.")
		ELSE
			SET_CURRENT_PED_WEAPON(LocalPlayerPed, wtPreCutsceneWeapon, TRUE)
			PRINTLN("DO_POST_CUTSCENE_COMMON_PLAYER_EXIT - Putting weapon back in player's hand: ", GET_WEAPON_NAME(wtPreCutsceneWeapon))
		ENDIF
		
		WEAPON_TYPE wtWeapon
		GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtWeapon)
		IF wtWeapon = WEAPONTYPE_UNARMED
		OR wtWeapon = WEAPONTYPE_INVALID
			PUT_WEAPON_IN_HAND( WEAPONINHAND_BEST_WEAPON )
			PRINTLN("[Cutscenes] DO_POST_CUTSCENE_COMMON_PLAYER_EXIT - last weapon was unequipped. Put best weapon in hand")
		ENDIF		
	ENDIF
	
	IF bGoToCover
		SET_PLAYER_INTO_COVER_AFTER_CUTSCENE(GET_PLAYERS_FACE_DIRECTION_IN_COVER(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet3, iPlayerIndex))
	ENDIF
	
ENDPROC

PROC HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS(INT iCutsceneTeam, INT iWarpVeh, INT iSeat)
	
	IF IS_PARTICIPANT_A_NON_HEIST_SPECTATOR(iLocalPart)
		PRINTLN("[Cutscenes] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - Player is a non-heist spectator")
		EXIT
	ENDIF
	
	INT iLocalCutsceneIdx = GET_LOCAL_PLAYER_CUTSCENE_INDEX(iCutsceneTeam)
	
	IF iLocalCutsceneIdx = -1
		PRINTLN("[Cutscenes] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - Player is not part of the cutscene, iLocalCutsceneIdx = ",iLocalCutsceneIdx)
		EXIT
	ENDIF
	
	IF iWarpVeh = -1
		PRINTLN("[Cutscenes] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - No warp vehicle set up for this cutscene")
		EXIT
	ENDIF
	
	IF iSeat != ciFMMC_Cutscene_SeatPreference_SameAsCurrentVeh
		PRINTLN("[Cutscenes] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - Cutscene player index ",iLocalCutsceneIdx," doesn't get the same seat, iSeat = ", iSeat)
		EXIT
	ENDIF
		
	IF NOT IS_PLAYER_IN_CREATOR_AIRCRAFT(LocalPlayer)
		PRINTLN("[Cutscenes] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - Not in the hold, IS_PLAYER_IN_CREATOR_AIRCRAFT returned FALSE")
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iWarpVeh].mn != AVENGER
		PRINTLN("[Cutscenes] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - Warp vehicle ",iWarpVeh," is not an avenger, mn = ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iWarpVeh].mn),"/",ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iWarpVeh].mn))
		EXIT
	ENDIF
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iWarpVeh])
		PRINTLN("[Cutscenes] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - Warp vehicle ",iWarpVeh," net ID doesn't exist")
		EXIT
	ENDIF
	
	VEHICLE_INDEX tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iWarpVeh])
	
	IF NOT DOES_ENTITY_EXIST(tempVeh)
		PRINTLN("[Cutscenes] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - Warp vehicle ",iWarpVeh," vehicle index doesn't exist")
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF MPGlobalsAmbience.vehCreatorAircraft = tempVeh
		PRINTLN("[Cutscenes] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - MPGlobalsAmbience.vehCreatorAircraft is already warp vehicle ",iWarpVeh)
	ENDIF
	IF NOT DOES_ENTITY_EXIST(MPGlobalsAmbience.vehCreatorAircraft)
		PRINTLN("[Cutscenes] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - MPGlobalsAmbience.vehCreatorAircraft doesn't exist yet")
	ENDIF
	#ENDIF
	
	PRINTLN("[Cutscenes] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - Set MPGlobalsAmbience.vehCreatorAircraft to equal creator vehicle ",iWarpVeh)
	MPGlobalsAmbience.vehCreatorAircraft = tempVeh
	
ENDPROC

PROC PROCESS_WARP_SELECTED_PED_AFTER_CUTSCENE(FMMC_ENTITY_CUTSCENE_WARP_DATA sCutsceneEntityWarpData, INT iEntity, INT iWarpIndex)
	NETWORK_INDEX pedNetId = MC_serverBD_1.sFMMC_SBD.niPed[iEntity]	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(pedNetId)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_PED_AFTER_CUTSCENE - ped: ", iEntity, " NETWORK_DOES_NETWORK_ID_EXIST = FALSE")
		EXIT
	ENDIF
		
	ENTITY_INDEX entity = NET_TO_ENT(pedNetId)
	IF NOT DOES_ENTITY_EXIST(entity)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_PED_AFTER_CUTSCENE - ped: ", iEntity, " DOES_ENTITY_EXIST = FALSE")
		EXIT
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(entity)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_PED_AFTER_CUTSCENE - ped: ", iEntity, " NETWORK_HAS_CONTROL_OF_ENTITY = FALSE")
		EXIT
	ENDIF
	
	PED_INDEX ped = GET_PED_INDEX_FROM_ENTITY_INDEX(entity)
	IF IS_PED_INJURED(ped)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_PED_AFTER_CUTSCENE - ped: ", iEntity, " is injured")
		EXIT
	ENDIF
	
	PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_PED_AFTER_CUTSCENE - ped: ", iEntity, " is being moved to position: ", sCutsceneEntityWarpData.vWarpPos[iWarpIndex])
	
	SET_ENTITY_COORDS(ped, sCutsceneEntityWarpData.vWarpPos[iWarpIndex])
	SET_ENTITY_HEADING(ped, sCutsceneEntityWarpData.fWarpHead[iWarpIndex])
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(ped, TRUE)
	
	SET_BIT(iWarpCutsceneEntityBS, iWarpIndex)
ENDPROC

PROC PROCESS_WARP_SELECTED_VEHICLE_AFTER_CUTSCENE(FMMC_ENTITY_CUTSCENE_WARP_DATA sCutsceneEntityWarpData, INT iEntity, INT iWarpIndex)
	NETWORK_INDEX vehNetId = MC_serverBD_1.sFMMC_SBD.niVehicle[iEntity]	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(vehNetId)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_VEHICLE_AFTER_CUTSCENE - veh: ", iEntity, "NETWORK_DOES_NETWORK_ID_EXIST = FALSE")
		EXIT
	ENDIF
	
	ENTITY_INDEX entity = NET_TO_ENT(vehNetId)
	IF NOT DOES_ENTITY_EXIST(entity)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_VEHICLE_AFTER_CUTSCENE - veh: ", iEntity, "DOES_ENTITY_EXIST = FALSE")
		EXIT
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(entity)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_VEHICLE_AFTER_CUTSCENE - veh: ", iEntity, "NETWORK_HAS_CONTROL_OF_ENTITY = FALSE")
		EXIT
	ENDIF
	
	VEHICLE_INDEX veh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entity)
	IF NOT IS_VEHICLE_DRIVEABLE(veh)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_VEHICLE_AFTER_CUTSCENE - veh: ", iEntity, " is undrivable")
		EXIT
	ENDIF
	
	PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_PED_AFTER_CUTSCENE - veh: ", iEntity, " is being moved to position: ", sCutsceneEntityWarpData.vWarpPos[iWarpIndex])
	
	SET_ENTITY_COORDS(veh, sCutsceneEntityWarpData.vWarpPos[iWarpIndex])
	SET_ENTITY_HEADING(veh, sCutsceneEntityWarpData.fWarpHead[iWarpIndex])
	SET_VEHICLE_ON_GROUND_PROPERLY(veh)	
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(veh, TRUE)
	
	SET_BIT(iWarpCutsceneEntityBS, iWarpIndex)
ENDPROC

PROC PROCESS_WARP_SELECTED_OBJECT_AFTER_CUTSCENE(FMMC_ENTITY_CUTSCENE_WARP_DATA sCutsceneEntityWarpData, INT iEntity, INT iWarpIndex)
	
	NETWORK_INDEX ObjNetId = GET_OBJECT_NET_ID(iEntity)
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(ObjNetId)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_OBJECT_AFTER_CUTSCENE - obj: ", iEntity, " NETWORK_DOES_NETWORK_ID_EXIST = FALSE")
		EXIT
	ENDIF
		
	ENTITY_INDEX entity = NET_TO_ENT(ObjNetId)
	IF NOT DOES_ENTITY_EXIST(entity)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_OBJECT_AFTER_CUTSCENE - obj: ", iEntity, " DOES_ENTITY_EXIST = FALSE")
		EXIT
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(entity)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_OBJECT_AFTER_CUTSCENE - obj: ", iEntity, " NETWORK_HAS_CONTROL_OF_ENTITY = FALSE")
		EXIT
	ENDIF
	
	OBJECT_INDEX obj = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(entity)
	IF IS_ENTITY_DEAD(obj)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_OBJECT_AFTER_CUTSCENE - obj: ", iEntity, " is dead")
		EXIT
	ENDIF
	
	PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_OBJECT_AFTER_CUTSCENE - obj: ", iEntity, " is being moved to position: ", sCutsceneEntityWarpData.vWarpPos[iWarpIndex])
	
	SET_ENTITY_COORDS(obj, sCutsceneEntityWarpData.vWarpPos[iWarpIndex])
	SET_ENTITY_HEADING(obj, sCutsceneEntityWarpData.fWarpHead[iWarpIndex])
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(obj, TRUE)
	
	SET_BIT(iWarpCutsceneEntityBS, iWarpIndex)
ENDPROC

PROC PROCESS_WARP_SELECTED_INTERACTABLE_AFTER_CUTSCENE(FMMC_ENTITY_CUTSCENE_WARP_DATA sCutsceneEntityWarpData, INT iEntity, INT iWarpIndex)
	
	NETWORK_INDEX InteractableNetId = GET_INTERACTABLE_NET_ID(iEntity)
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(InteractableNetId)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_INTERACTABLE_AFTER_CUTSCENE - interactable: ", iEntity, " NETWORK_DOES_NETWORK_ID_EXIST = FALSE")
		EXIT
	ENDIF
		
	ENTITY_INDEX entity = NET_TO_ENT(InteractableNetId)
	IF NOT DOES_ENTITY_EXIST(entity)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_INTERACTABLE_AFTER_CUTSCENE - interactable: ", iEntity, " DOES_ENTITY_EXIST = FALSE")
		EXIT
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(entity)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_INTERACTABLE_AFTER_CUTSCENE - interactable: ", iEntity, " NETWORK_HAS_CONTROL_OF_ENTITY = FALSE")
		EXIT
	ENDIF
	
	OBJECT_INDEX interactable = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(entity)
	IF IS_ENTITY_DEAD(interactable)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_INTERACTABLE_AFTER_CUTSCENE - interactable: ", iEntity, " is dead")
		EXIT
	ENDIF
	
	PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_INTERACTABLE_AFTER_CUTSCENE - interactable: ", iEntity, " is being moved to position: ", sCutsceneEntityWarpData.vWarpPos[iWarpIndex])
	
	SET_ENTITY_COORDS(interactable, sCutsceneEntityWarpData.vWarpPos[iWarpIndex])
	SET_ENTITY_HEADING(interactable, sCutsceneEntityWarpData.fWarpHead[iWarpIndex])
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(interactable, TRUE)	
	
	SET_BIT(iWarpCutsceneEntityBS, iWarpIndex)
ENDPROC

FUNC BOOL PROCESS_WARP_SELECTED_PERSONAL_VEHICLE_AFTER_CUTSCENE(FMMC_ENTITY_CUTSCENE_WARP_DATA sCutsceneEntityWarpData, INT iEntity, INT iWarpIndex)
	
	IF iEntity = -1
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_PERSONAL_VEHICLE_AFTER_CUTSCENE - No participant set in creator")
		RETURN TRUE
	ENDIF
	
	INT iPart = GET_ORDERED_PARTICIPANT_NUMBER(iEntity)	
		
	IF iPart = -1
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_PERSONAL_VEHICLE_AFTER_CUTSCENE - No participant found for iEntity: ", iEntity)
		RETURN TRUE
	ENDIF
	
	PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_PERSONAL_VEHICLE_AFTER_CUTSCENE - iEntity: ", iEntity)
	
	PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iPart)
	
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_PERSONAL_VEHICLE_AFTER_CUTSCENE - Participant is not active for iPart: ", iPart)
		RETURN TRUE
	ENDIF
	
	PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)	
		
	IF NOT NETWORK_IS_PLAYER_ACTIVE(piPlayer)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_PERSONAL_VEHICLE_AFTER_CUTSCENE - Player is not active for iPart: ", iPart)
		RETURN TRUE
	ENDIF
	
	VEHICLE_INDEX vehPV = MPGlobals.RemotePV[NATIVE_TO_INT(piPlayer)]
	
	IF NOT DOES_ENTITY_EXIST(vehPV)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_PERSONAL_VEHICLE_AFTER_CUTSCENE - DOES_ENTITY_EXIST = FALSE")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(vehPV)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_PERSONAL_VEHICLE_AFTER_CUTSCENE - personal veh is undrivable")
		RETURN TRUE
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(vehPV)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_PERSONAL_VEHICLE_AFTER_CUTSCENE - Do not have control of VEH.")		
		IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(vehPV), sCutsceneEntityWarpData.vWarpPos[iWarpIndex], 1.5)
			PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_PERSONAL_VEHICLE_AFTER_CUTSCENE - Vehicle not warped yet.")			
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF		
	ENDIF
	
	PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_PERSONAL_VEHICLE_AFTER_CUTSCENE - Warping Personal Vehicle")
	
	SET_ENTITY_COORDS(vehPV, sCutsceneEntityWarpData.vWarpPos[iWarpIndex])
	SET_ENTITY_HEADING(vehPV, sCutsceneEntityWarpData.fWarpHead[iWarpIndex])
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehPV, TRUE)	
	SET_VEHICLE_ON_GROUND_PROPERLY(vehPV)
	
	IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(vehPV), sCutsceneEntityWarpData.vWarpPos[iWarpIndex], 1.5)
		PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_PERSONAL_VEHICLE_AFTER_CUTSCENE - Vehicle not warped yet.")	
		RETURN FALSE
	ENDIF
		
	SET_BIT(iWarpCutsceneEntityBS, iWarpIndex)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL PROCESS_WARP_LAST_VEHICLE_AFTER_CUTSCENE(FMMC_ENTITY_CUTSCENE_WARP_DATA sCutsceneEntityWarpData, INT iEntity, INT iWarpIndex)
	IF iEntity = -1
		PRINTLN("[Cutscene] PROCESS_WARP_LAST_VEHICLE_AFTER_CUTSCENE - No participant set in creator")
		RETURN TRUE
	ENDIF
	
	INT iPart = GET_ORDERED_PARTICIPANT_NUMBER(iEntity)	
	
	IF iPart != iLocalPart 
		PRINTLN("[Cutscene] PROCESS_WARP_LAST_VEHICLE_AFTER_CUTSCENE - Not the local player!")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[Cutscene] PROCESS_WARP_LAST_VEHICLE_AFTER_CUTSCENE - Process Player ", iEntity)

	VEHICLE_INDEX viLastVeh = viLastVehicleIndexBeforeCutscene
	
	IF NOT DOES_ENTITY_EXIST(viLastVeh)
		PRINTLN("[Cutscene] PROCESS_WARP_LAST_VEHICLE_AFTER_CUTSCENE - DOES_ENTITY_EXIST = FALSE")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(viLastVeh)
		PRINTLN("[Cutscene] PROCESS_WARP_LAST_VEHICLE_AFTER_CUTSCENE - last veh is undrivable")
		RETURN TRUE
	ENDIF
	
	MODEL_NAMES mnTempModel = GET_ENTITY_MODEL(viLastVeh)
	
	//By design we just want to warp cars and bikes with this function
	IF NOT IS_THIS_MODEL_A_CAR(mnTempModel)
	AND NOT IS_THIS_MODEL_A_BIKE(mnTempModel)
		PRINTLN("[Cutscene] PROCESS_WARP_LAST_VEHICLE_AFTER_CUTSCENE - last veh is not a car or a bike!")
		RETURN TRUE
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(viLastVeh)
		PRINTLN("[Cutscene] PROCESS_WARP_LAST_VEHICLE_AFTER_CUTSCENE - Do not have control of VEH.")
		NETWORK_REQUEST_CONTROL_OF_ENTITY(viLastVeh)
		IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(viLastVeh), sCutsceneEntityWarpData.vWarpPos[iWarpIndex], 1.5)
			PRINTLN("[Cutscene] PROCESS_WARP_LAST_VEHICLE_AFTER_CUTSCENE - Vehicle not warped yet.")			
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF		
	ENDIF
	
	PRINTLN("[Cutscene] PROCESS_WARP_LAST_VEHICLE_AFTER_CUTSCENE - Warping Last Vehicle")
	
	SET_ENTITY_COORDS(viLastVeh, sCutsceneEntityWarpData.vWarpPos[iWarpIndex])
	SET_ENTITY_HEADING(viLastVeh, sCutsceneEntityWarpData.fWarpHead[iWarpIndex])
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(viLastVeh, TRUE)	
	SET_VEHICLE_ON_GROUND_PROPERLY(viLastVeh)
	
	IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(viLastVeh), sCutsceneEntityWarpData.vWarpPos[iWarpIndex], 1.5)
		PRINTLN("[Cutscene] PROCESS_WARP_LAST_VEHICLE_AFTER_CUTSCENE - Vehicle not warped yet.")	
		RETURN FALSE
	ENDIF
			
	IF NOT IS_BIT_SET(iWarpCutsceneEntityBS, iWarpIndex)
		SET_BIT(iWarpCutsceneEntityBS, iWarpIndex)
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetCutsceneEntityBSWarped, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE, iWarpIndex)
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL PROCESS_WARP_SELECTED_ENTITIES_AFTER_CUTSCENE(FMMC_ENTITY_CUTSCENE_WARP_DATA sCutsceneEntityWarpData)

	PRINTLN("[Cutscene] PROCESS_WARP_SELECTED_ENTITIES_AFTER_CUTSCENE - Calling warp functionality.")
	
	INT i = 0
	FOR i = 0 TO CUTSCENE_WARP_ENTITY_MAX-1
		
		IF IS_BIT_SET(iWarpCutsceneEntityBS, i)
			RELOOP
		ENDIF
	
		SWITCH sCutsceneEntityWarpData.iEntityType[i]
			CASE CUTSCENE_WARP_ENTITY_TYPE_PED
				PROCESS_WARP_SELECTED_PED_AFTER_CUTSCENE(sCutsceneEntityWarpData, sCutsceneEntityWarpData.iEntityIndex[i], i)
			BREAK
			CASE CUTSCENE_WARP_ENTITY_TYPE_VEH
				PROCESS_WARP_SELECTED_VEHICLE_AFTER_CUTSCENE(sCutsceneEntityWarpData, sCutsceneEntityWarpData.iEntityIndex[i], i)
			BREAK
			CASE CUTSCENE_WARP_ENTITY_TYPE_OBJECT
				PROCESS_WARP_SELECTED_OBJECT_AFTER_CUTSCENE(sCutsceneEntityWarpData, sCutsceneEntityWarpData.iEntityIndex[i], i)
			BREAK
			CASE CUTSCENE_WARP_ENTITY_TYPE_INTERACTABLE
				PROCESS_WARP_SELECTED_INTERACTABLE_AFTER_CUTSCENE(sCutsceneEntityWarpData, sCutsceneEntityWarpData.iEntityIndex[i], i)
			BREAK
			CASE CUTSCENE_WARP_ENTITY_TYPE_PERSONAL_VEHICLE
				IF NOT PROCESS_WARP_SELECTED_PERSONAL_VEHICLE_AFTER_CUTSCENE(sCutsceneEntityWarpData, sCutsceneEntityWarpData.iEntityIndex[i], i)
					RETURN FALSE
				ENDIF
			BREAK
			CASE CUTSCENE_WARP_ENTITY_TYPE_LAST_VEHICLE
				IF NOT PROCESS_WARP_LAST_VEHICLE_AFTER_CUTSCENE(sCutsceneEntityWarpData, sCutsceneEntityWarpData.iEntityIndex[i], i)
					RETURN FALSE
				ENDIF
			BREAK
		ENDSWITCH
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Get the vehicle to use for warping from the cutscene ped warp data
FUNC VEHICLE_INDEX GET_CUTSCENE_PED_WARP_VEHICLE(FMMC_CUTSCENE_PED_WARP_DATA &sWarpData)

	IF sWarpData.iVehID = -1
		PRINTLN("[Cutscenes] GET_CUTSCENE_PED_WARP_VEHICLE - iVehId is invalid")
		RETURN NULL
	ENDIF
	
	NETWORK_INDEX vehNetId = MC_serverBD_1.sFMMC_SBD.niVehicle[sWarpData.iVehID]
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(vehNetId)
	
		PRINTLN("[Cutscenes][Vehicle ", sWarpData.iVehID, "] GET_CUTSCENE_PED_WARP_VEHICLE - iVehID: ",  sWarpData.iVehID, " - Primary vehicle does not exist, falling back to secondary")
		
		IF sWarpData.iVehIDSecondary = -1
			PRINTLN("[Cutscenes][Vehicle ", sWarpData.iVehID, "] GET_CUTSCENE_PED_WARP_VEHICLE - no secondary id found, bail")
			RETURN NULL
		ENDIF
		
		vehNetId = MC_serverBD_1.sFMMC_SBD.niVehicle[sWarpData.iVehIDSecondary]
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(vehNetId)
			PRINTLN("[Cutscenes][Vehicle ", sWarpData.iVehID, "] GET_CUTSCENE_PED_WARP_VEHICLE - iVehIDSecondary: ", sWarpData.iVehIDSecondary, " - secondary vehicle does not exist, bail")
			RETURN NULL
		ENDIF
	ENDIF
	
	ENTITY_INDEX entity = NET_TO_ENT(vehNetId)
	IF NOT DOES_ENTITY_EXIST(entity)
		PRINTLN("[Cutscenes][Vehicle ", sWarpData.iVehID, "] GET_CUTSCENE_PED_WARP_VEHICLE - DOES_ENTITY_EXIST = FALSE")
		RETURN NULL
	ENDIF
	
	VEHICLE_INDEX veh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entity)
	IF NOT IS_VEHICLE_DRIVEABLE(veh)
		PRINTLN("[Cutscenes][Vehicle ", sWarpData.iVehID, "] GET_CUTSCENE_PED_WARP_VEHICLE - veh is undrivable")
		RETURN NULL
	ENDIF
	
	PRINTLN("[Cutscenes][Vehicle ", sWarpData.iVehID, "] GET_CUTSCENE_PED_WARP_VEHICLE - veh: ", NATIVE_TO_INT(veh)," - Vehicle found")
	RETURN veh
ENDFUNC

PROC GET_SCRIPTED_CUTSCENE_END_WARP_TARGET(INT iCutscene, INT iPlayerCutId, VECTOR &vWarpCoord, FLOAT &fWarpHeading, VEHICLE_INDEX &vehWarpTarget)

	// always grab this, we just dont necessarily use it's coords for the warp
	vehWarpTarget = GET_CUTSCENE_PED_WARP_VEHICLE(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sPlayerEndWarp[iPlayerCutId])

	// Only try and use the vehicle coords if there wasn't already valid coords
	IF IS_VECTOR_ZERO(vWarpCoord)
		IF DOES_ENTITY_EXIST(vehWarpTarget)
		AND NOT IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_RemoveFromVehEnd)
			
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			AND GET_VEHICLE_PED_IS_IN(LocalPlayerPed) = vehWarpTarget
				PRINTLN("[Cutscenes] GET_SCRIPTED_CUTSCENE_END_WARP_TARGET - Already inside this vehicle, do not use it as target for warp.")
				EXIT
			ENDIF
			
			vWarpCoord = GET_ENTITY_COORDS(vehWarpTarget, FALSE)
			fWarpHeading = GET_ENTITY_HEADING(vehWarpTarget)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Get the vehicle seat to use for warping from the veh seat preference
FUNC VEHICLE_SEAT GET_CUTSCENE_PED_WARP_VEHICLE_SEAT(PED_INDEX ped, VEHICLE_INDEX veh, INT iVehSeatPreference, VEHICLE_SEAT eCurrentSeat = VS_ANY_PASSENGER)
	
	IF vehSeatForcedFromSyncScene != VS_ANY_PASSENGER
		PRINTLN("[Cutscenes] GET_CUTSCENE_PED_WARP_VEHICLE_SEAT - Using forced Seat from sync scene: ", ENUM_TO_INT(vehSeatForcedFromSyncScene))
		RETURN vehSeatForcedFromSyncScene
	ENDIF
	
	IF iVehSeatPreference = ciFMMC_SeatPreference_None
		PRINTLN("[Cutscenes] GET_CUTSCENE_PED_WARP_VEHICLE_SEAT - iSeat = ciFMMC_SeatPreference_None, bail")
		RETURN VS_ANY_PASSENGER
	ENDIF
	
	VEHICLE_SEAT vehSeat = VS_ANY_PASSENGER
	IF iVehSeatPreference = ciFMMC_Cutscene_SeatPreference_SameAsCurrentVeh
		
		IF IS_PED_IN_ANY_VEHICLE(ped)
			VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(ped)
			INT iMaxSeats = (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(tempVeh) + 1) //Add 1 for the driver
			INT i
			REPEAT iMaxSeats i
				VEHICLE_SEAT vehSeatTemp = INT_TO_ENUM(VEHICLE_SEAT, i-1)
				IF GET_PED_IN_VEHICLE_SEAT(tempVeh, vehSeatTemp) = ped
					vehSeat = vehSeatTemp
				ENDIF
			ENDREPEAT
		ELSE
			vehSeat = eCurrentSeat
		ENDIF
	ELSE
		SWITCH iVehSeatPreference
			CASE ciFMMC_SeatPreference_Driver			vehSeat = VS_DRIVER				BREAK
			CASE ciFMMC_SeatPreference_Passenger		vehSeat = VS_ANY_PASSENGER		BREAK 
			CASE ciFMMC_SeatPreference_Rear_right		vehSeat = VS_BACK_RIGHT			BREAK 
			CASE ciFMMC_SeatPreference_Rear_left		vehSeat = VS_BACK_LEFT			BREAK 
			CASE ciFMMC_SeatPreference_Front_Seats		vehSeat = VS_FRONT_RIGHT		BREAK 
			CASE ciFMMC_SeatPreference_Extra_right_1	vehSeat = VS_EXTRA_RIGHT_1		BREAK 
			CASE ciFMMC_SeatPreference_Extra_left_1		vehSeat = VS_EXTRA_LEFT_1		BREAK 
			CASE ciFMMC_SeatPreference_Extra_right_2	vehSeat = VS_EXTRA_RIGHT_2		BREAK 
			CASE ciFMMC_SeatPreference_Extra_left_2		vehSeat = VS_EXTRA_LEFT_2		BREAK 
			CASE ciFMMC_SeatPreference_Extra_right_3	vehSeat = VS_EXTRA_RIGHT_3		BREAK 
			CASE ciFMMC_SeatPreference_Extra_left_3		vehSeat = VS_EXTRA_LEFT_3		BREAK 
		ENDSWITCH
	ENDIF

	// Figure out seat to use if non-specfic of seat is not available
	IF vehSeat = VS_ANY_PASSENGER
	OR NOT IS_VEHICLE_SEAT_FREE(veh, vehSeat)

		IF vehSeat = VS_ANY_PASSENGER
			PRINTLN("[Cutscenes] GET_CUTSCENE_PED_WARP_VEHICLE_SEAT - unable to find my vehicle seat in previous vehicle (?) - need to grab a free one")
		ELSE
			PRINTLN("[Cutscenes] GET_CUTSCENE_PED_WARP_VEHICLE_SEAT - vehicle seat ",ENUM_TO_INT(vehSeat)," in previous vehicle isn't free in new vehicle - need to grab a free one")
		ENDIF
		vehSeat = MC_GET_FIRST_FREE_VEHICLE_SEAT(veh)
		PRINTLN("[Cutscenes] GET_CUTSCENE_PED_WARP_VEHICLE_SEAT - GET_FIRST_FREE_VEHICLE_SEAT_RC returned ",ENUM_TO_INT(vehSeat))
	ENDIF
	
	RETURN vehSeat
ENDFUNC

PROC SET_UP_VEHICLE_PED_IS_IN_FOR_CUTSCENE(PED_INDEX pedIndex)
	
	IF NOT DOES_ENTITY_EXIST(pedIndex)
	OR IS_PED_INJURED(pedIndex)
		EXIT
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(pedIndex)
		EXIT
	ENDIF
	
	IF GET_SEAT_PED_IS_IN(pedIndex) != VS_DRIVER
		EXIT
	ENDIF
	
	VEHICLE_INDEX vehPedIsIn = GET_VEHICLE_PED_IS_IN(pedIndex)
	
	IF NOT IS_VEHICLE_DRIVEABLE(vehPedIsIn)
		EXIT
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(vehPedIsIn)
		EXIT
	ENDIF
		
	MODEL_NAMES model = GET_ENTITY_MODEL(vehPedIsIn)
		
	IF NOT IS_THIS_MODEL_A_HELI(model)
	AND NOT IS_THIS_MODEL_A_PLANE(model)	
		EXIT		
	ENDIF
		
	SET_VEHICLE_ENGINE_ON(vehPedIsIn, TRUE, FALSE)
	
	SET_HELI_BLADES_FULL_SPEED(vehPedIsIn)
	
	PRINTLN("[Cutscenes] - SET_UP_VEHICLE_PED_IS_IN_FOR_CUTSCENE - Calling SET_VEHICLE_ENGINE_ON and SET_HELI_BLADES_FULL_SPEED")
	
ENDPROC

PROC INIT_SCRIPTED_CUTSCENE_SYNC_SCENE_ENTITIES(INT iCutscene)

	NETWORK_INDEX niIndex
	PED_INDEX pedIndex
	INT i = 0
	FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES-1
		INT iType = g_fmmc_struct.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType
		INT iIndex = g_fmmc_struct.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex
		
		IF iType = -1
			BREAKLOOP
		ENDIF
		
		niIndex = GET_CUTSCNE_VISIBLE_NET_ID(iType, iIndex)
		
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niIndex)
			RELOOP
		ENDIF
		
		SWITCH iType 
			CASE CREATION_TYPE_PEDS				
				PRINTLN("[Cutscenes] - INIT_SCRIPTED_CUTSCENE_SYNC_SCENE_ENTITIES - Initialising Ped: ", iIndex, " for scripted cutscene.")
				pedIndex = NET_TO_PED(niIndex)
				SET_UP_VEHICLE_PED_IS_IN_FOR_CUTSCENE(pedIndex)
			BREAK
		ENDSWITCH
	ENDFOR	

ENDPROC

/// PURPOSE:
///  Warps a ped to a vehicle using the cutscene ped warp data  
/// PARAMS:
///    ped - 
///    sWarpData - 
///    eCurrentSeat - 
/// RETURNS:
///    TRUE if the ped was warped to a vehicle
FUNC BOOL CUTSCENE_WARP_PED_TO_VEHICLE_COMMON(PED_INDEX ped, FMMC_CUTSCENE_PED_WARP_DATA &sWarpData, VEHICLE_SEAT eCurrentSeat = VS_ANY_PASSENGER)

	IF bIsAnySpectator
		RETURN FALSE
	ENDIF

	VEHICLE_INDEX vehWarpTo = GET_CUTSCENE_PED_WARP_VEHICLE(sWarpData)
	
	// No vehicle - no further action
	IF vehWarpTo = NULL
		PRINTLN("[Cutscenes] CUTSCENE_WARP_PED_TO_VEHICLE_COMMON - vehWarpTo = NULL, bail")
		RETURN FALSE
	ENDIF
	
	VEHICLE_SEAT vehSeat = GET_CUTSCENE_PED_WARP_VEHICLE_SEAT(ped, vehWarpTo, sWarpData.iVehSeat, eCurrentSeat)	
	IF (vehSeat != VS_ANY_PASSENGER)
		IF NOT PLACE_PED_IN_VEHICLE_SEAT_IF_POSSIBLE(vehWarpTo, ped, vehSeat)
			// failed to place ped in vehicle
			PRINTLN("[Cutscenes] CUTSCENE_WARP_PED_TO_VEHICLE_COMMON - failed to place ped in vehicle seat: ", vehSeat, ", bail")
			RETURN FALSE
		ENDIF
	ENDIF
	
	PRINTLN("[Cutscenes] CUTSCENE_WARP_PED_TO_VEHICLE_COMMON - Placed ped in vehicle. vehSeat = ", ENUM_TO_INT(vehSeat))
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(vehWarpTo)
		// no controller, can't manipulate engine, etc
		PRINTLN("[Cutscenes] CUTSCENE_WARP_PED_TO_VEHICLE_COMMON - no net control, no further work required")
		RETURN TRUE
	ENDIF
	
	//
	// Engine on stuff
	//
	BOOL bEngineOn = IS_BIT_SET(sWarpData.iBitset1, ciCSPW_BS1_VEHICLE_START_ENGINE)
	BOOL bEngineOnInstantly = IS_BIT_SET(sWarpData.iBitset1, ciCSPW_BS1_VEHICLE_START_ENGINE_INSTANTLY)

	IF bEngineOn
		
		SET_VEHICLE_ENGINE_ON(vehWarpTo, TRUE, bEngineOnInstantly)
		IF bEngineOnInstantly
			MODEL_NAMES model = GET_ENTITY_MODEL(vehWarpTo)
			IF IS_THIS_MODEL_A_HELI(model)
			OR IS_THIS_MODEL_A_PLANE(model)	
				SET_HELI_BLADES_FULL_SPEED(vehWarpTo)
			ENDIF
		ENDIF
		
		PRINTLN("[Cutscenes] CUTSCENE_WARP_PED_TO_VEHICLE_COMMON - Engine on - bEngineOn: ", BOOL_TO_STRING(bEngineOn), " bEngineOnInstantly: ", BOOL_TO_STRING(bEngineOnInstantly))
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC CUTSCENE_WARP_PLACED_PEDS_TO_VEHICLES(FMMC_CUTSCENE_PLACED_PED_WARP_DATA& sPeds[])

	IF IS_BIT_SET(g_fmmc_struct.sCurrentMocapSceneData.iCutsceneBitSet3, ci_CSBS3_BlockWarpingPedsToVehicles)
		EXIT
	ENDIF

	INT i
	
	// Placed Peds
	FOR i = 0 TO COUNT_OF(sPeds)-1

		IF sPeds[i].iPedID <= -1
			PRINTLN("[Cutscenes] CUTSCENE_WARP_PLACED_PEDS_TO_VEHICLES - i: ", i, " - iPedId: ", sPeds[i].iPedID, " - invalid id")
			RELOOP
		ENDIF
	
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[sPeds[i].iPedID])
			PRINTLN("[Cutscenes] CUTSCENE_WARP_PLACED_PEDS_TO_VEHICLES - i: ", i, " - iPedId: ", sPeds[i].iPedID, " - invalid net id")
			RELOOP
		ENDIF
		
		PED_INDEX ped = NET_TO_PED( MC_serverBD_1.sFMMC_SBD.niPed[sPeds[i].iPedID])
		IF NETWORK_HAS_CONTROL_OF_ENTITY(ped)
			CUTSCENE_WARP_PED_TO_VEHICLE_COMMON(ped, sPeds[i].sTarget)
		ENDIF
	ENDFOR

ENDPROC

FUNC BOOL CAN_MOCAP_SHORT_WARP_AT_END(INT iCutsceneNum)

	IF iCutsceneNum = -1
		RETURN FALSE
	ENDIF

	IF bIsAnySpectator
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
		RETURN FALSE
	ENDIF
	
	IF GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_ENTER_VEHICLE ) = WAITING_TO_START_TASK
	OR GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_ENTER_VEHICLE ) = PERFORMING_TASK
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_MOCAP_SHORT_WARP_REMOVE_FROM_VEHICLE(INT iCutsceneToUse)
	UNUSED_PARAMETER(iCutsceneToUse)
	// Only Gang ops checked this data in this function previously before the 2020 split.
	//RETURN IS_BIT_SET(g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_RemoveFromVehEnd)
	RETURN FALSE
ENDFUNC

//Function to update on the player exit state
FUNC BOOL UPDATE_ON_PLAYER_EXIT_STATE(INT iCutsceneNum, INT iCutsceneTeam, VECTOR vEndPos, FLOAT fEndHeading, BOOL bForce = FALSE)

	IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)	//Manual Exit State from above has been done for player
	OR CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(tlPlayerSceneHandle)		//Exit State can be set for local player
	OR bForce = TRUE														//Where we want to force an update on the player e.g. when having no handle

		PRINTLN("[Cutscenes] PLAYER SCENE HANDLE tlPlayerSceneHandle =", tlPlayerSceneHandle, " / iCutsceneNum = ", iCutsceneNum, " / iCutsceneTeam = ", iCutsceneTeam)
		
		INT iLocalPlayerCutId = GET_LOCAL_PLAYER_CUTSCENE_INDEX(iCutsceneTeam)
		
		IF IS_LOCAL_PLAYER_PED_IN_CUTSCENE()
			FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
			SET_ENTITY_LOCALLY_VISIBLE(LocalPlayerPed)
		ENDIF
		
		IF iCutsceneNum >= 0 AND iCutsceneNum < MAX_MOCAP_CUTSCENES
	
			CUTSCENE_WARP_PLACED_PEDS_TO_VEHICLES(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneNum].sPedEndWarp)
						
			PRINTLN("[Cutscenes] UPDATE_ON_PLAYER_EXIT_STATE | iLocalPlayerCutId = ", iLocalPlayerCutId)
			IF iLocalPlayerCutId >= 0 AND iLocalPlayerCutId < FMMC_MAX_CUTSCENE_PLAYERS
				CUTSCENE_WARP_PED_TO_VEHICLE_COMMON(LocalPlayerPed, g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneNum].sPlayerEndWarp[iLocalPlayerCutId])
			ENDIF
				
		ENDIF	
		
		IF CAN_MOCAP_SHORT_WARP_AT_END(iCutsceneNum)
		
			IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneNum].iCutsceneBitset, ci_CSBS_WarpPlayersEnd)
			AND NOT IS_VECTOR_ZERO(vEndPos)
			AND fEndHeading != ciINVALID_CUTSCENE_WARP_HEADING
				IF NET_WARP_TO_COORD(vEndPos, fEndHeading, NOT SHOULD_MOCAP_SHORT_WARP_REMOVE_FROM_VEHICLE(iCutsceneNum), FALSE, FALSE, FALSE, TRUE, TRUE)
					DO_POST_CUTSCENE_COMMON_PLAYER_EXIT(iCutsceneNum, FMMCCUT_MOCAP, iLocalPlayerCutId)
					
					IF NOT IS_BIT_SET(iSpecificCutsceneBitset2, ciSpecificCutsceneBitset2_SET_INTO_COVER)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, TRUE)
					ENDIF
					
					PRINTLN("[Cutscenes] UPDATE_ON_PLAYER_EXIT_STATE | WARPED AT THE END OF THE CUTSCENE TO: Position: ", vEndPos, " Heading: ", fEndHeading )
					
					SET_BIT(iLocalBoolCheck30, LBOOL30_WARPED_FROM_EXIT_STATE)
				ENDIF
			ENDIF
		ENDIF

		//Restore the female ped feet to stored at start
		RESTORE_FEMALE_PED_FEET( LocalPlayerPed )
	
		//restore the player weapon if not invalid
		if weapMocap != WEAPONTYPE_INVALID
			SET_CURRENT_PED_WEAPON( LocalPlayerPed, weapMocap, TRUE )
			weapMocap = WEAPONTYPE_INVALID
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Handle clearing ambient peds, vehicles and moving script vehicles
PROC CLEAR_CUTSCENE_AREA(FLOAT fRadius, VECTOR vClearArea, VECTOR vNewPosCentre, BOOL bClearVehicles = TRUE, BOOL bClearPersonalVehicles = FALSE)
	//Check if clear area is set
	IF fRadius <= 0
		EXIT
	ENDIF
	
	INT iTemp
	VECTOR vCurrentPos
	
	CLEAR_AREA(vClearArea, fRadius, TRUE, FALSE, FALSE, FALSE)	//Clear ambient peds and vehicles
	CLEAR_AREA_LEAVE_VEHICLE_HEALTH(vClearArea, fRadius, FALSE, FALSE, TRUE, FALSE) // make sure we delete wrecked and abandoned vehicles.
	
	INT iNumExcludedEntities
	
	INT iCutsceneToUse
	INT iCutsceneEntity
	IF g_iFMMCScriptedCutscenePlaying> -1
		IF g_iFMMCScriptedCutscenePlaying >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
		AND g_iFMMCScriptedCutscenePlaying < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
			iCutsceneToUse = (g_iFMMCScriptedCutscenePlaying - FMMC_GET_MAX_SCRIPTED_CUTSCENES()) 
		ELSE
			iCutsceneToUse = g_iFMMCScriptedCutscenePlaying
		ENDIF
		
		
		REPEAT FMMC_MAX_CUTSCENE_ENTITIES iCutsceneEntity
			INT iType = g_fmmc_struct.sScriptedCutsceneData[ iCutsceneToUse ].sCutsceneEntities[ iCutsceneEntity ].iType
			INT iIndex = g_fmmc_struct.sScriptedCutsceneData[ iCutsceneToUse ].sCutsceneEntities[ iCutsceneEntity ].iIndex
			
			IF iType = -1
				BREAKLOOP
			ENDIF
			
			SWITCH( iType )
				CASE CREATION_TYPE_VEHICLES
					excludedEntities[iNumExcludedEntities].iType = iType
					excludedEntities[iNumExcludedEntities].iIndex = iIndex
					iNumExcludedEntities++
					
					IF iNumExcludedEntities >= ciMAX_EXCLUDED_CUTSCENE_ENTITIES
						BREAKLOOP
					ENDIF
				BREAK
				
				DEFAULT
				BREAK
			ENDSWITCH
			
		ENDREPEAT
	ENDIF

	//Relocate script vehicles
	IF NOT IS_VECTOR_ZERO( vNewPosCentre )
		//Relocate script vehicles
		IF bClearVehicles
			REPEAT FMMC_MAX_VEHICLES iTemp
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iTemp])
					VEHICLE_INDEX currentVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iTemp])
					
					BOOL bSkip = FALSE
					
					REPEAT iNumExcludedEntities iCutsceneEntity
						IF excludedEntities[ iCutsceneEntity ].iType = CREATION_TYPE_VEHICLES
						AND excludedEntities[ iCutsceneEntity ].iIndex = iTemp
							bSkip = TRUE
							BREAKLOOP // Break out of this loop
						ENDIF
					ENDREPEAT
					
					IF bSkip 
						RELOOP // skip to the next vehicle
					ENDIF
					
					IF DOES_ENTITY_EXIST(currentVeh)
					AND NETWORK_HAS_CONTROL_OF_ENTITY(currentVeh)
						vCurrentPos = GET_ENTITY_COORDS(currentVeh, FALSE)
						
						IF GET_DISTANCE_BETWEEN_COORDS(vCurrentPos,vClearArea) <= fRadius
						
							vCurrentPos.x = vCurrentPos.x - vClearArea.x
							vCurrentPos.y = vCurrentPos.y - vClearArea.y
							
							//Move vehicle to inside safe zone, relative to its previous position
							SET_ENTITY_COORDS(currentVeh,<<vCurrentPos.x + vNewPosCentre.x, vCurrentPos.y + vNewPosCentre.y,vNewPosCentre.z>>, FALSE)
						
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ELSE
		PRINTLN("[Cutscenes] Clear cutscene area new position is zero - not relocating any entities." )
	ENDIF
	
	IF bClearVehicles
	AND NOT bClearPersonalVehicles		
	AND NOT IS_VECTOR_ZERO(vNewPosCentre)
		PRINTLN("[Cutscenes][Personal_vehicles] - Clear cutscene area set up to move Personal Vehicles")
		
		INT iNumVehicles
		INT i
		iNumVehicles = GET_ALL_VEHICLES(g_PoolVehicles)		
		
		PRINTLN("[Cutscenes][Personal_vehicles] - iNumVehicles = ") NET_PRINT_INT(iNumVehicles) NET_NL()
		REPEAT iNumVehicles i
			IF DOES_ENTITY_EXIST(g_PoolVehicles[i])
				IF DECOR_EXIST_ON(g_PoolVehicles[i], "Player_Vehicle")
					IF NETWORK_HAS_CONTROL_OF_ENTITY(g_PoolVehicles[i])						
						vCurrentPos = GET_ENTITY_COORDS(g_PoolVehicles[i], FALSE)	
						IF GET_DISTANCE_BETWEEN_COORDS(vCurrentPos, vClearArea) <= fRadius
						
							vCurrentPos.x = vCurrentPos.x - vClearArea.x
							vCurrentPos.y = vCurrentPos.y - vClearArea.y
							
							//Move vehicle to inside safe zone, relative to its previous position
							SET_ENTITY_COORDS(g_PoolVehicles[i], <<vCurrentPos.x + vNewPosCentre.x, vCurrentPos.y + vNewPosCentre.y,vNewPosCentre.z>>, FALSE)
													
							PRINTLN("[Cutscenes][Personal_vehicles] - Moving a personal vehicle to the new location.")
						ENDIF
					ENDIF
				ELSE			
					PRINTLN("[Cutscenes][Personal_vehicles] - Not moving Personal Vehicle as it's not flagged with 'Player_Vehicle'.")
				ENDIF
			ENDIF
		ENDREPEAT
	ELIF bClearPersonalVehicles
		// cleanup my personal vehicle if inside area
		IF DOES_ENTITY_EXIST(MPGlobals.RemotePV[NATIVE_TO_INT(LocalPlayer)])
			IF NOT IS_ENTITY_DEAD(MPGlobals.RemotePV[NATIVE_TO_INT(LocalPlayer)])
				vCurrentPos = GET_ENTITY_COORDS(MPGlobals.RemotePV[NATIVE_TO_INT(LocalPlayer)])
				IF GET_DISTANCE_BETWEEN_COORDS(vCurrentPos,vClearArea) <= fRadius
					CLEANUP_MP_SAVED_VEHICLE(TRUE, FALSE, TRUE, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		// cleanup any old pv nearby
		DELETE_ANY_OLD_PV_NEARBY()
	ENDIF
	
ENDPROC

PROC HIDE_ENTITIES_IN_CUTSCENE_AREA(FLOAT fRadius,VECTOR vClearArea,VECTOR vNewPosCentre, BOOL bClearVehicles = TRUE, BOOL bClearPersonalVehicles = FALSE)
	
	VECTOR vCurrentPos	
	INT iNumExcludedEntities	
	INT iCutsceneToUse
	INT iCutsceneEntity
	
	IF g_iFMMCScriptedCutscenePlaying> -1
		IF g_iFMMCScriptedCutscenePlaying >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
		AND g_iFMMCScriptedCutscenePlaying < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
			iCutsceneToUse = (g_iFMMCScriptedCutscenePlaying - FMMC_GET_MAX_SCRIPTED_CUTSCENES()) 
		ELSE
			iCutsceneToUse = g_iFMMCScriptedCutscenePlaying
		ENDIF		
		
		REPEAT FMMC_MAX_CUTSCENE_ENTITIES iCutsceneEntity
			INT iType = g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].sCutsceneEntities[iCutsceneEntity].iType
			INT iIndex = g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].sCutsceneEntities[iCutsceneEntity].iIndex
			
			IF iType = -1
				BREAKLOOP
			ENDIF
			
			SWITCH( iType )
				CASE CREATION_TYPE_VEHICLES
					excludedEntities[iNumExcludedEntities].iType = iType
					excludedEntities[iNumExcludedEntities].iIndex = iIndex
					iNumExcludedEntities++
					
					IF iNumExcludedEntities >= ciMAX_EXCLUDED_CUTSCENE_ENTITIES
						BREAKLOOP
					ENDIF
				BREAK
				
				DEFAULT
				BREAK
			ENDSWITCH
			
		ENDREPEAT
	ENDIF

	//hide script vehicles
	IF NOT IS_VECTOR_ZERO(vNewPosCentre)
		//hide script vehicles
		
		IF bClearVehicles
			
			VEHICLE_INDEX currentVeh	
			
			// check any nearby vehicles
			
			INT iNumVehicles
			INT i
			
			iNumVehicles = GET_ALL_VEHICLES(g_PoolVehicles)
		
			FOR i = 0 TO iNumVehicles-1
				
				currentVeh = g_PoolVehicles[i]
				
				IF NOT DOES_ENTITY_EXIST(currentVeh)
					RELOOP
				ENDIF
				
				IF IS_ENTITY_A_MISSION_ENTITY(currentVeh)
					RELOOP
				ENDIF
				
				vCurrentPos = GET_ENTITY_COORDS(currentVeh, FALSE)
				
				IF GET_DISTANCE_BETWEEN_COORDS(vCurrentPos, vClearArea) > fRadius
					RELOOP						
				ENDIF	
				
				SET_ENTITY_LOCALLY_INVISIBLE(currentVeh)
				
			ENDFOR
			
			FOR i = 0 TO FMMC_MAX_VEHICLES-1
		
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
					RELOOP
				ENDIF
				
				currentVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
				
				BOOL bSkip = FALSE
				
				REPEAT iNumExcludedEntities iCutsceneEntity
					IF excludedEntities[iCutsceneEntity].iType = CREATION_TYPE_VEHICLES
					AND excludedEntities[iCutsceneEntity].iIndex = i
						bSkip = TRUE
						BREAKLOOP // Break out of this loop
					ENDIF
				ENDREPEAT
				
				IF bSkip 
					RELOOP // skip to the next vehicle
				ENDIF
				
				IF DOES_ENTITY_EXIST(currentVeh)
					vCurrentPos = GET_ENTITY_COORDS(currentVeh, FALSE)						
					IF GET_DISTANCE_BETWEEN_COORDS(vCurrentPos, vClearArea) <= fRadius						
						SET_ENTITY_LOCALLY_INVISIBLE(currentVeh)
					ENDIF
				ENDIF
				
			ENDFOR		
		ENDIF
	ELSE
		PRINTLN("[Cutscenes] Clear cutscene area new position is zero - not hiding any entities." )
	ENDIF

	IF bClearPersonalVehicles				
		PROCESS_HIDE_PERSONAL_VEHICLES_LOCALLY_FOR_PLAYER()
	ENDIF
	
ENDPROC

PROC KILL_ALARM_SOUNDS()
	INT iAlarms
	FOR iAlarms = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1)
	    IF isoundid[iAlarms] <> -1
			PRINTLN("[Cutscenes] [ALARM] - KILL_ALARM_SOUNDS STOP_SOUND(isoundid[", iAlarms, "]")
            STOP_SOUND(isoundid[iAlarms])
        ENDIF
	ENDFOR
ENDPROC

// Player respawning on missions that can have end cutscenes
// This stops the situation where we die as we start END_STAGE (where the team can't fail)
// but the end cutscene doesn't play and other bad things happen because we're dead
PROC RESPAWN_LOCAL_PLAYER_IF_DEAD_AND_GOING_INTO_END_CUTSCENE()

	IF IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
		PRINTLN("[Cutscenes] Skipping RESPAWN_LOCAL_PLAYER_IF_DEAD_AND_GOING_INTO_END_CUTSCENE due to being in test mode")
		EXIT
	ENDIF
	
	IF NOT g_bFailedMission
		IF MC_ServerBD.iEndCutscene > -1
		AND NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP )
		
			IF NOT bLocalPlayerOK
			OR IS_PLAYER_RESPAWNING( LocalPlayer )
				IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_PLAYER_RESURRECTED_FOR_END_CUTSCENE)
					FMMC_NETWORK_RESURRECT_LOCAL_PLAYER(GET_ENTITY_COORDS( LocalPlayerPed, FALSE ),
													GET_ENTITY_HEADING( LocalPlayerPed ),
													1000,
													FALSE,
													FALSE) // We don't want to unpause the renderphases here
					SET_BIT( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF ) // Say the renderphases are toggled, so the cutscene unpauses them
					SET_BIT(iLocalBoolCheck31, LBOOL31_PLAYER_RESURRECTED_FOR_END_CUTSCENE)
					PRINTLN("[Cutscenes] Force respawning the player for the end mocap" )
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL ARE_ALL_PLAYERS_SYNCED_FOR_END_OF_CUTSCENE()

	INT iPart
	PRINTLN("[PLAYER_LOOP] - ARE_ALL_PLAYERS_SYNCED_FOR_END_OF_CUTSCENE")
	FOR iPart = 0 TO NUM_NETWORK_PLAYERS-1
		PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(iPart)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
			IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE)			
				IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet4, PBBOOL4_SYNC_PLAYERS_FOR_END_OF_CUTSCENE)
					PRINTLN("[ARE_ALL_PLAYERS_SYNCED_FOR_END_OF_CUTSCENE] Waiting for ", iPart, " so returning FALSE") 
					RETURN FALSE
				ELSE
					PRINTLN("[ARE_ALL_PLAYERS_SYNCED_FOR_END_OF_CUTSCENE] iPart ", iPart, " Synced.") 
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("[ARE_ALL_PLAYERS_SYNCED_FOR_END_OF_CUTSCENE] All players synced at end of cutscene, returning TRUE")
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SPECIAL_CUTSCENE_VEHICLE_WARP( INT iCutsceneToUse, INT iBackupPlayerSpawnPos )
	BOOL bWarpVehicles
	
	INT index
	NETWORK_INDEX niVeh
	VEHICLE_INDEX veh
	
	INT iSpawnPosition
	REPEAT FMMC_MAX_TEAMS iSpawnPosition
		index = g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iVehicleIndexForPosWarp[ iSpawnPosition ]
	
		IF index < 0
		OR index >= FMMC_MAX_VEHICLES
			RELOOP
		ENDIF
		
		niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[ index ]
			
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST( niVeh )
			RELOOP
		ENDIF
		
		// we know at this point that we at least want to warp a vehicle
		// so don't do the normal cutscene start pos stuff
		bWarpVehicles = TRUE
		
		IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID( niVeh )
			RELOOP
		ENDIF
		
		veh = NET_TO_VEH( niVeh )
		
		IF IS_ENTITY_DEAD( veh )
			RELOOP
		ENDIF
		
		PRINTLN("[Cutscenes] Vehicle ",index, " has a special vehicle warp - it's going to player spawn pos ", iSpawnPosition, " which is at ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerStartPos[ iSpawnPosition ] )
		
		// always move the vehicle to the start position
		SET_ENTITY_COORDS( veh, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerStartPos[ iSpawnPosition ] )
		SET_ENTITY_HEADING( veh, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerStartHeading[ iSpawnPosition ] )		
	ENDREPEAT
	
	IF bWarpVehicles // only do a backup warp if we have the data set up
		IF NOT IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
			IF NOT SHOULD_USE_ALT_END_POS(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].eUseAltEndPos)
				PRINTLN("[Cutscenes] The local player isn't in a vehicle, so we're going to put them in a backup position here: ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerEndPos[ iBackupPlayerSpawnPos ] )		
				SET_ENTITY_COORDS( LocalPlayerPed, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerEndPos[ iBackupPlayerSpawnPos ] )
				SET_ENTITY_HEADING( LocalPlayerPed, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerEndHeading[ iBackupPlayerSpawnPos ] )
			ELSE
				PRINTLN("[Cutscenes] The local player isn't in a vehicle, so we're going to put them in a backup position here (using alternative pos): ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerAltEndPos[ iBackupPlayerSpawnPos ] )		
				SET_ENTITY_COORDS( LocalPlayerPed, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerAltEndPos[ iBackupPlayerSpawnPos ] )
				SET_ENTITY_HEADING( LocalPlayerPed, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerAltEndHeading[ iBackupPlayerSpawnPos ] )
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bWarpVehicles
ENDFUNC

FUNC BOOL SHOULD_CUTSCENE_WAIT_FOR_CELEB_SCREEN_TO_FINISH()
	
	IF DID_I_JOIN_MISSION_AS_SPECTATOR()
		PRINTLN("[PROCESS_SCRIPTED_CUTSCENE] - SHOULD_CUTSCENE_WAIT_FOR_CELEB_SCREEN_TO_FINISH - Returning true (JIP)")
		RETURN FALSE
	ENDIF
	
	IF g_bCelebrationScreenIsActive
		PRINTLN("[PROCESS_SCRIPTED_CUTSCENE] - SHOULD_CUTSCENE_WAIT_FOR_CELEB_SCREEN_TO_FINISH - Waiting for celeb screen to finish.")
		RETURN TRUE
	ENDIF
	
	IF bIsAnySpectator
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
	OR IS_BIT_SET(iLocalBoolCheck3,LBOOL3_LATE_SPECTATE)
	OR IS_BIT_SET(iLocalBoolCheck3,LBOOL3_WAIT_END_SPECTATE)
		IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
			PRINTLN("[PROCESS_SCRIPTED_CUTSCENE] - SHOULD_CUTSCENE_WAIT_FOR_CELEB_SCREEN_TO_FINISH - Wait for Spectator Cam to be active...")
			RETURN TRUE
		ELSE
			PRINTLN("[PROCESS_SCRIPTED_CUTSCENE] - SHOULD_CUTSCENE_WAIT_FOR_CELEB_SCREEN_TO_FINISH - Spectator Cam is active...")
		ENDIF		
	ENDIF

	RETURN FALSE
ENDFUNC

PROC PROCESS_DEAD_PLAYERS_WATCHING_CUTSCENE()
	
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
		PRINTLN("[PROCESS_SCRIPTED_CUTSCENE] - PROCESS_DEAD_PLAYERS_WATCHING_CUTSCENE - Player Fail or Out of Lives.")
		IF g_bCelebrationScreenIsActive
		OR (HAS_NET_TIMER_STARTED(tdovertimer) AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdovertimer) < 4000)
		OR (HAS_NET_TIMER_STARTED(tdwastedtimer) AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdwastedtimer) < 4000)
			PRINTLN("[PROCESS_SCRIPTED_CUTSCENE] - PROCESS_DEAD_PLAYERS_WATCHING_CUTSCENE - Waiting for screen to be frozen by summary before resurrecting")
			EXIT
		ENDIF
	ENDIF
	
	IF NOT bLocalPlayerPedOK
		PRINTLN("[PROCESS_SCRIPTED_CUTSCENE] - PROCESS_DEAD_PLAYERS_WATCHING_CUTSCENE - Resurrecting Local Player for Cutscene - Preventing netcode Asserts")
		VECTOR vSpawnPos = GET_ENTITY_COORDS(LocalPlayerPed)
		FLOAT fSpawnHead = GET_ENTITY_HEADING(LocalPlayerPed)
		FMMC_NETWORK_RESURRECT_LOCAL_PLAYER(vSpawnPos, fSpawnHead, 0, TRUE, FALSE)
		NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_SET_INVISIBLE | NSPC_FREEZE_POSITION | NSPC_NO_COLLISION)
	ELSE
		PRINTLN("[PROCESS_SCRIPTED_CUTSCENE] - PROCESS_DEAD_PLAYERS_WATCHING_CUTSCENE - Player is OKAY")
	ENDIF
ENDPROC

FUNC INT SERVER_CHOOSE_MOCAP_END_CUTSCENE(INT iCutsceneType)
	
	UNUSED_PARAMETER(iCutsceneType)
	
	// NOTE: Previous this function allowed the server to chose a selection of cutscenes based on the one selected in the creator. This was only really implemented in the original heists. Removed for 2020.	
	RETURN 1

ENDFUNC

//Pre load a scripted cutscene cam shot
PROC PRE_LOAD_CAM_SHOT_AREA(INT iCShot, VECTOR vloc, FLOAT fRadius)
	IF iCShot > iCamShotLoaded
	
		IF NOT IS_VECTOR_ZERO( vloc )
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			//Only clear the load area if we are going to be loading a new one.
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				PRINTLN("[Cutscenes] PROCESS_SCRIPTED_CUTSCENE - PRE_LOAD_CAM_SHOT_AREA - Calling to stop Load Scene.")
				NEW_LOAD_SCENE_STOP()
			ENDIF
			
			PRINTLN("[Cutscenes] PROCESS_SCRIPTED_CUTSCENE - PRE_LOAD_CAM_SHOT_AREA - Calling to start a new Load Scene.")
			
			//Load the designated area
			NEW_LOAD_SCENE_START_SPHERE(vloc, fRadius)
			CLEAR_BIT(iLocalBoolCheck7, LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED)
		ELSE
			PRINTLN("[Cutscenes] PROCESS_SCRIPTED_CUTSCENE - PRE_LOAD_CAM_SHOT_AREA - Calling to start a new Load Scene.")
			SET_BIT(iLocalBoolCheck7, LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED)
		ENDIF
		
		PRINTLN("[Cutscenes] PROCESS_SCRIPTED_CUTSCENE - PRE_LOAD_CAM_SHOT_AREA - iCamShotLoaded is now: ", iCShot)
		
		//Update pre loaded shot		
		iCamShotLoaded = iCShot
	ELSE
		if iCShot = iCamShotLoaded
			IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED)
				PRINTLN("[Cutscenes] PROCESS_SCRIPTED_CUTSCENE - PRE_LOAD_CAM_SHOT_AREA - checking if iCShot: ", iCShot, " is loaded")
				
				IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
				OR IS_NEW_LOAD_SCENE_LOADED()
					PRINTLN("[Cutscenes] PROCESS_SCRIPTED_CUTSCENE - PRE_LOAD_CAM_SHOT_AREA - Cam Shot ", iCamShotLoaded, " Load Scene is loaded")
					SET_BIT(iLocalBoolCheck7, LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED)
				ENDIF
			ENDIF
		ENDIF
	
	ENDIF

ENDPROC

//PURPOSE: Init and update the cutscene intro focus cam
PROC DO_FOCUS_INTRO_CAM( INT iCutsceneToUse )
	// Init the focus intro
	IF( NOT IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_FOCUS_INTRO ) )
		IF( NOT IS_BIT_SET( g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Hint_At_Start )
		OR bIsSCTV)
			PRINTLN( "[Cutscenes] DO_FOCUS_INTRO_CAM - NO HINT INTRO SETTING TO SKIP" )
			SET_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_FOCUS_INTRO )
			SET_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_FOCUS_INTRO )
		ELSE
			// Kill any other hint camera
			KILL_CHASE_HINT_CAM( sIntroHintCam )
			
			// Force the chase hint cam
			FORCE_CHASE_HINT_CAM( sIntroHintCam, TRUE )	
			
			// Set interp times
			SET_CUSTOM_CHASE_HINT_CAM_INTERP_IN_TIME( sIntroHintCam, g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iTransitionInTime )
			INT iTransitionTime = g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iTransitionInTime + g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iTransitionHoldTime
			SET_CUSTOM_CHASE_HINT_CAM_INTERP_OUT_TIME( sIntroHintCam, iTransitionTime * 2 )
			
			SET_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_FOCUS_INTRO )
		ENDIF
	ENDIF

	// Update the focus intro
	IF( IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_FOCUS_INTRO )
	AND NOT IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_FOCUS_INTRO ) )
		IF( IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT() )
			DO_SCREEN_FADE_IN( 500 )
		ENDIF
		
		PRINTLN( "[Cutscenes] DO_FOCUS_INTRO_CAM - RUNNING FOCUS INTRO" )
		
		// Update the hint cam
		CONTROL_COORD_CHASE_HINT_CAM_ANY_MEANS( sIntroHintCam, g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].vIntroCoOrds, 
			"", HINTTYPE_DEFAULT, FALSE, FALSE )
		
		// Check the timer has started
		IF( HAS_NET_TIMER_STARTED( tHintCam ) )
			INT iTransitionTime = g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iTransitionInTime + g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iTransitionHoldTime
			IF( GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( tHintCam ) > iTransitionTime )
				CPRINTLN( DEBUG_SIMON, "kill" )
				IF( IS_GAMEPLAY_HINT_ACTIVE() )
					STOP_GAMEPLAY_HINT( TRUE )
					ANIMPOSTFX_STOP( "FocusIn" )
					STOP_AUDIO_SCENE( "HINT_CAM_SCENE" )
					sIntroHintCam.bPostFXActive = FALSE
				ENDIF
				KILL_CHASE_HINT_CAM( sIntroHintCam )
				SET_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_FOCUS_INTRO )
				RESET_NET_TIMER( tHintCam )
			ENDIF
		ELSE	
			// Set the focus cam timer
			REINIT_NET_TIMER( tHintCam )
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_ATTACH_CAMERA_VEHICLE_POSITION( INT iCutscene, INT iShot )
	VECTOR 			vVehPos 	= <<0.0, 0.0, 0.0>>
	INT 			iVehIndex 	= g_FMMC_STRUCT.sScriptedCutsceneData[ iCutscene ].iVehicleToAttachTo[ iShot ]
	NETWORK_INDEX	niVeh		= MC_serverBD_1.sFMMC_SBD.niVehicle[ iVehIndex ]
	VEHICLE_INDEX 	veh			= NULL
	
	IF( NETWORK_DOES_NETWORK_ID_EXIST( niVeh ) )
		veh = NET_TO_VEH( niVeh )
		IF( DOES_ENTITY_EXIST( veh ) )
			vVehPos = GET_ENTITY_COORDS( veh, FALSE )
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF( IS_VECTOR_ZERO( vVehPos ) )
		ASSERTLN( "MANAGE_MID_MISSION_CUTSCENE - Failed to get position of camera attach vehicle for streaming!" )
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_ATTACH_CAM_VEHICLE_DOES_NOT_EXIST, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "Failed to get position of camera attach vehicle for streaming!")
		#ENDIF
	ENDIF
	#ENDIF
	
	RETURN vVehPos
ENDFUNC

PROC SETUP_CAM_PARAMS(INT icutscene)
	
	BOOL bDidCameraShotCut = TRUE

	IF iCamShot - 1 >= 0
	
		VECTOR vLastShotEnd = g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vCamEndPos[iCamShot-1]
		VECTOR vNextShotStart = g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vCamStartPos[iCamShot]
		
		VECTOR vLastShotEndRot = g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vCamEndRot[iCamShot-1]
		VECTOR vNextShotStartRot = g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vCamStartRot[iCamShot]
		
		FLOAT fLastShotEndFOV = g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fEndCamFOV[iCamShot-1]
		FLOAT fNextShotStartFOV = g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fStartCamFOV[iCamShot]
		
		IF ARE_VECTORS_EQUAL(vLastShotEnd, vNextShotStart)
		AND ARE_VECTORS_EQUAL(vLastShotEndRot, vNextShotStartRot)
		AND fLastShotEndFOV = fNextShotStartFOV
			bDidCameraShotCut = FALSE
			PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - SET bDidCameraShotCut FALSE")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iCutsceneShotBitSet[iCamShot - 1], ciCSS_BS_Attach_Enable)
			DETACH_CAM(cutscenecam)
			DESTROY_CAM(cutscenecam)
			cutscenecam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE) 
			PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - detaching camera")
		ENDIF

	ENDIF

	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vCamStartPos[iCamShot])
		SET_CAM_PARAMS(cutscenecam,g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vCamStartPos[iCamShot],
			g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vCamStartRot[iCamShot],
			g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fStartCamFOV[iCamShot])
		PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - setting new params" )
	ENDIF
	
	IF( IS_BIT_SET( g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Attach_Enable ) )
		PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - attaching camera to iVeh: ", g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iVehicleToAttachTo[iCamShot], " vPos: ", g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vOffsetPos[iCamShot])
	
		INT 			iVehIndex 	= g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iVehicleToAttachTo[iCamShot]
		NETWORK_INDEX	niVeh		= MC_serverBD_1.sFMMC_SBD.niVehicle[ iVehIndex ]
		VEHICLE_INDEX 	veh			= NULL
		
		IF( NETWORK_DOES_NETWORK_ID_EXIST( niVeh ) )
			veh = NET_TO_VEH( niVeh )			
			ATTACH_CAM_TO_ENTITY( cutscenecam, veh, g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vOffsetPos[iCamShot], TRUE )
			POINT_CAM_AT_ENTITY( cutscenecam, veh, g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vLookAtPos[iCamShot], TRUE )
		ELSE
			PRINTLN( "SETUP_CAM_PARAMS - icutscene: ", icutscene, " - Couldn't get cam attach entity" )
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iShakeType[iCamShot] != -1
	AND g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fShakeMag[iCamShot] != 0
	
		BOOL bApplyShake = TRUE
		
		// Do not apply this shots shake if there was not change in the camera shot and the shake is the same as the previous shot
		IF NOT bDidCameraShotCut
		AND iCamShot - 1 >= 0
			PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - shot: ", iCamShot, ", iShakeType: ", g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iShakeType[iCamShot])
			PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - shot: ", iCamShot-1, ", iShakeType: ", g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iShakeType[iCamShot-1])
			PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - shot: ", iCamShot, ", fShakeMag: ", g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fShakeMag[iCamShot])
			PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - shot: ", iCamShot-1, ", fShakeMag: ", g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fShakeMag[iCamShot-1])
			
			IF g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iShakeType[iCamShot] = g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iShakeType[iCamShot-1]
			//AND g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fShakeMag[iCamShot] = g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fShakeMag[iCamShot-1]
				bApplyShake = FALSE
				PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - Set bApplyShake FALSE" )
			ENDIF
		ENDIF
	
		IF bApplyShake
			SHAKE_CAM(cutscenecam,GET_CAM_SHAKE_TYPE_FROM_INT(g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iShakeType[iCamShot]),g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fShakeMag[iCamShot])
			PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - shaking cam" )
		ENDIF
	ELSE
		STOP_CAM_SHAKING(cutscenecam,TRUE)
		PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - stopping shaking cam" )
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vCamEndPos[iCamShot])
		PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - setting interp data" )
		SET_CAM_PARAMS(cutscenecam,g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vCamEndPos[iCamShot],
			g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vCamEndRot[iCamShot],
			g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fEndCamFOV[iCamShot],
			g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iInterpTime[iCamShot],
			g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].CamType[iCamShot],
			g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].CamType[iCamShot])	
	ENDIF
	
	IF g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fMaxFarClipDistance[iCamShot] != 0
		PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " iCamShot: ", iCamShot, " - fMaxFarClipDistance: ", g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fMaxFarClipDistance[iCamShot])
		FORCE_CAM_FAR_CLIP(cutscenecam, g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fMaxFarClipDistance[iCamShot])
	ELSE		
		FORCE_CAM_FAR_CLIP(cutscenecam, 10000)  // 10000 is default
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cutscene Data functions  ----------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions that return where the cutscene will take place, how many players are meant to be in it etc.									 ---------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC RESET_SERVER_CUTSCENE_DATA( INT iCutsceneTeam )
	#IF IS_DEBUG_BUILD
	PRINTLN("RESET_SERVER_CUTSCENE_DATA")
	DEBUG_PRINTCALLSTACK()
	#ENDIF

	INT iplayer

	FOR iplayer = 0 TO ( FMMC_MAX_CUTSCENE_PLAYERS - 1 )
		MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer] = INVALID_PLAYER_INDEX()
	ENDFOR
	MC_serverBD.iCutscenePlayerBitset[iCutsceneTeam] = 0
	MC_serverBD.iCutsceneStreamingIndex[iCutsceneTeam] = -1
	MC_serverBD.iCutsceneID[iCutsceneTeam] = -1
		
	CLEAR_BIT(MC_serverBD.iServerBitSet, SBBOOL_CUTSCENE_TEAM0_PLAYERS_SELECTED)
	CLEAR_BIT(MC_serverBD.iServerBitSet, SBBOOL_CUTSCENE_TEAM1_PLAYERS_SELECTED)
	CLEAR_BIT(MC_serverBD.iServerBitSet, SBBOOL_CUTSCENE_TEAM2_PLAYERS_SELECTED)
	CLEAR_BIT(MC_serverBD.iServerBitSet, SBBOOL_CUTSCENE_TEAM3_PLAYERS_SELECTED)
	
	CLEAR_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_0)
	CLEAR_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_1)
	CLEAR_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_2)
	CLEAR_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_3)

ENDPROC
///PURPOSE: Clears data stating which teams should watch iTeamToClear's cutscene
PROC CLEAR_CUTSCENE_TEAM_CONSIDERATION_BITSET( INT iTeamToClear )
	
	SWITCH iTeamToClear
		CASE 0
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM0_CUTSCENE_SEEN_BY_TEAM0 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM0_CUTSCENE_SEEN_BY_TEAM1 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM0_CUTSCENE_SEEN_BY_TEAM2 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM0_CUTSCENE_SEEN_BY_TEAM3 )
		BREAK
		
		CASE 1
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM1_CUTSCENE_SEEN_BY_TEAM0 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM1_CUTSCENE_SEEN_BY_TEAM1 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM1_CUTSCENE_SEEN_BY_TEAM2 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM1_CUTSCENE_SEEN_BY_TEAM3 )
		BREAK
		
		CASE 2
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM2_CUTSCENE_SEEN_BY_TEAM0 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM2_CUTSCENE_SEEN_BY_TEAM1 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM2_CUTSCENE_SEEN_BY_TEAM2 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM2_CUTSCENE_SEEN_BY_TEAM3 )
		BREAK
		
		CASE 3
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM3_CUTSCENE_SEEN_BY_TEAM0 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM3_CUTSCENE_SEEN_BY_TEAM1 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM3_CUTSCENE_SEEN_BY_TEAM2 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM3_CUTSCENE_SEEN_BY_TEAM3 )
		BREAK
	ENDSWITCH
	
	PRINTLN("[Cutscenes] MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene has been cleared for team ", iTeamToClear )
	PRINTLN("[Cutscenes] MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene is now ", MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene )
ENDPROC

FUNC BOOL SHOULD_SELECT_PLAYER_OF_TEAM(INT iteam, INT iCutsceneToUse, FMMC_CUTSCENE_TYPE cutType = FMMCCUT_MOCAP, INT iplayer = 0 )
	
	#IF IS_DEBUG_BUILD
	IF g_SHOULD_SELECT_PLAYER_OF_TEAMReturnTrue 
		RETURN TRUE
	ENDIF
	#ENDIF

	SWITCH cutType
	
		CASE FMMCCUT_MOCAP
		
			SWITCH iTeam
				CASE 0
					SWITCH iPlayer
						CASE 0 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet, ci_CSBS_Team0_Closest)
						CASE 1 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet, ci_CSBS_Team0_Closest_Secondary)
						CASE 2 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_Team0_Closest_Third)
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iPlayer
						CASE 0 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet, ci_CSBS_Team1_Closest)
						CASE 1 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet, ci_CSBS_Team1_Closest_Secondary)
						CASE 2 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_Team1_Closest_Third)
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPlayer
						CASE 0 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet, ci_CSBS_Team2_Closest)
						CASE 1 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet, ci_CSBS_Team2_Closest_Secondary)
						CASE 2 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_Team2_Closest_Third)
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iPlayer
						CASE 0 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet, ci_CSBS_Team3_Closest)
						CASE 1 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet, ci_CSBS_Team3_Closest_Secondary)
						CASE 2 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_Team3_Closest_Third)
					ENDSWITCH
				BREAK
			ENDSWITCH
		
		BREAK
	
		CASE FMMCCUT_ENDMOCAP
		
			SWITCH iTeam
				CASE 0
					SWITCH iPlayer
						CASE 0 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet, ci_CSBS_Team0_Closest)
						CASE 1 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet, ci_CSBS_Team0_Closest_Secondary)
						CASE 2 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet2, ci_CSBS2_Team0_Closest_Third)
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iPlayer
						CASE 0 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet, ci_CSBS_Team1_Closest)
						CASE 1 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet, ci_CSBS_Team1_Closest_Secondary)
						CASE 2 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet2, ci_CSBS2_Team1_Closest_Third)
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPlayer
						CASE 0 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet, ci_CSBS_Team2_Closest)
						CASE 1 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet, ci_CSBS_Team2_Closest_Secondary)
						CASE 2 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet2, ci_CSBS2_Team2_Closest_Third)
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iPlayer
						CASE 0 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet, ci_CSBS_Team3_Closest)
						CASE 1 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet, ci_CSBS_Team3_Closest_Secondary)
						CASE 2 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet2, ci_CSBS2_Team3_Closest_Third)
					ENDSWITCH
				BREAK
			ENDSWITCH
		
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MC_DOES_MID_MISSION_CUTSCENE_REQUIRE_CUSTOM_CONCATS_REBUILT_DURING_GAMEPLAY(INT iCutsceneToUse)
	
	IF iCutsceneToUse = -1
		RETURN FALSE
	ENDIF
	
	// Special Concat Settings	
	IF ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName, "FIX_TRIP2_MCS1")	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
	
ENDFUNC 

FUNC BOOL MC_DOES_MID_MISSION_CUTSCENE_USE_CUSTOM_CONCATS(INT iCutsceneToUse)
	
	IF iCutsceneToUse = -1
		RETURN FALSE
	ENDIF
	
	// Special Concat Settings
	IF ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName, "FIXF_FIN_MCS2")
	OR ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName, "FIX_TRIP2_MCS1")	
	OR ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName, "FIX_TRIP1_INT")
	OR ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName, "FIX_TRIP2_INT")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
	
ENDFUNC 

FUNC BOOL MC_IS_CONCAT_VALID_FOR_MID_MISSION_CUTSCENE(INT iCutsceneToUse, INT iScene)
	
	IF iCutsceneToUse = -1
		RETURN FALSE
	ENDIF	
		
	// Special Concat Settings
	IF ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName, "FIXF_FIN_MCS2")
		IF GET_NUMBER_OF_PLAYING_PLAYERS() > 1
			SWITCH iScene
				CASE 0
				CASE 1
				CASE 2
				CASE 3
				CASE 5
				CASE 6
				CASE 10
				CASE 11
				CASE 12
					RETURN TRUE 
				BREAK
			ENDSWITCH
		ELSE
			SWITCH iScene
				CASE 0
				CASE 1
				CASE 2
				CASE 3
				CASE 4
				CASE 6
				CASE 7
				CASE 8
				CASE 9
					RETURN TRUE
				BREAK				
			ENDSWITCH
		ENDIF
		
		RETURN FALSE
		
	ELIF ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName, "FIX_TRIP2_MCS1")
				
		IF MC_serverBD.piCutscenePlayerIDs[0][0] != INVALID_PLAYER_INDEX()
			
			#IF IS_DEBUG_BUILD
			INT iTeamTemp = GET_PLAYER_TEAM(MC_serverBD.piCutscenePlayerIDs[0][0])
			PRINTLN("MC_IS_CONCAT_VALID_FOR_MID_MISSION_CUTSCENE - FIX_TRIP2_MCS1 - GET_PLAYER_NAME(", GET_PLAYER_NAME(MC_serverBD.piCutscenePlayerIDs[0][0]), ") Player team: ", iTeamTemp)
			#ENDIF
			
			IF GET_PLAYER_TEAM(MC_serverBD.piCutscenePlayerIDs[0][0]) = 0
				RETURN iScene = 1
			ELSE
				RETURN iScene = 0
			ENDIF
		ELSE
			PRINTLN("MC_IS_CONCAT_VALID_FOR_MID_MISSION_CUTSCENE - FIX_TRIP2_MCS1 - Waiting for Primary Cutscene Player to be assigned.")
		ENDIF
		
	ELIF ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName, "FIX_TRIP1_INT")
		SWITCH iScene
			CASE 0
			CASE 1
			CASE 2
			CASE 3
			CASE 4
			CASE 7
				RETURN TRUE
			BREAK
			CASE 5			
				RETURN GET_PLAYER_TEAM(LocalPlayer) = 0
			BREAK
			CASE 6			
				RETURN GET_PLAYER_TEAM(LocalPlayer) = 1
			BREAK
		ENDSWITCH
		
		RETURN FALSE
		
	ELIF ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName, "FIX_TRIP2_INT")
		SWITCH iScene
			CASE 0			
			CASE 3
			CASE 4
				RETURN TRUE
			BREAK			
			CASE 1
				RETURN GET_PLAYER_TEAM(LocalPlayer) = 1
			BREAK			
			CASE 2
				RETURN GET_PLAYER_TEAM(LocalPlayer) = 0
			BREAK
		ENDSWITCH
		
		RETURN FALSE
		
	ENDIF
	
	// All concat are valid.
	IF g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iVariationBS = 0
		RETURN TRUE
	ENDIF
	
	// Creator Concat Settings
	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iVariationBS, iScene)
ENDFUNC

FUNC CUTSCENE_SECTION GET_CUTSCENE_SECTION_FLAGS(INT iCutsceneToUse, FMMC_CUTSCENE_TYPE eCutType)
	CUTSCENE_SECTION cutSectionFlags
	INT iBS = 0, iBit = 1
	FOR iBS = 0 TO 31
		IF (eCutType = FMMCCUT_ENDMOCAP AND MC_IS_CONCAT_VALID_FOR_END_CUTSCENE(MC_ServerBD.iEndCutscene, iBS, MC_ServerBD.iEndCutsceneConcatBS))
		OR (eCutType = FMMCCUT_MOCAP AND MC_IS_CONCAT_VALID_FOR_MID_MISSION_CUTSCENE(iCutsceneToUse, iBS))		
			cutSectionFlags = cutSectionFlags | INT_TO_ENUM(CUTSCENE_SECTION, iBit)
		ENDIF
		iBit *= 2
	ENDFOR
	
	RETURN cutSectionFlags
ENDFUNC

FUNC FLOAT GET_CUTSCENE_HEADING(INT iTeam, FMMC_CUTSCENE_TYPE cutType = FMMCCUT_ENDMOCAP, INT iCutsceneToUse = -1)

	FLOAT fReturn = 0.0
	
	SWITCH cutType
	
		CASE FMMCCUT_SCRIPTED
			
		BREAK
		
		CASE FMMCCUT_MOCAP			
			IF iCutsceneToUse != -1
				fReturn = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vCutsceneRotation.z
			ENDIF
		BREAK
		
		CASE FMMCCUT_ENDMOCAP
			VECTOR vReturn
			vReturn = GET_END_CUTSCENE_ROTATION()
			fReturn = vReturn.z
		BREAK
	ENDSWITCH
	
	PRINTLN("GET_CUTSCENE_HEADING - 		iTeam: ", iteam, " cutType: ", cutType, " - fReturn = ", fReturn)
	PRINTLN("GET_CUTSCENE_HEADING - 		iCutsceneToUse: ", iCutsceneToUse)
	
	RETURN fReturn
ENDFUNC

FUNC BOOL SHOULD_MOCAP_CUTSCENE_PLAY_AT_SPECIFIED_COORDS(FMMC_CUTSCENE_TYPE eCutType, INT iCutsceneToUse)
	
	IF eCutType = FMMCCUT_MOCAP
		IF iCutsceneToUse > -1 
		AND IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_ForceCutsceneToPlayAtPositionAndRotation)		
			RETURN TRUE
		ENDIF
	
	ELIF eCutType = FMMCCUT_ENDMOCAP
		IF NOT IS_MOCAP_CUTSCENE_A_PLACEHOLDER(eCutType #IF IS_DEBUG_BUILD , sMocapCutscene #ENDIF )
			IF (MC_ServerBD.iLastCompletedGoToLocation > -1 
			AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_ServerBD.iLastCompletedGoToLocation].iLocBS3, ciLoc_BS3_UseFirstAdditionalSpawnPosAsEndCutscenePos))
				RETURN TRUE
			ENDIF
			IF NOT IS_VECTOR_ZERO(MC_ServerBD.vEndCutsceneOverridePosition)
			OR NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sEndMocapSceneData.vCutscenePosition)
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDIF	
	
	RETURN FALSE
ENDFUNC

///purpose: obtains the coords for the teams mocap. 
FUNC VECTOR GET_CUTSCENE_COORDS(int iteam, FMMC_CUTSCENE_TYPE cutType = FMMCCUT_ENDMOCAP)

	VECTOR vReturn = <<0,0,0>>
	INT iCutsceneToUse = -1
	SWITCH cutType
	
		CASE FMMCCUT_SCRIPTED
		
			IF iteam < 0
			OR iTeam >= FMMC_MAX_TEAMS
				ASSERTLN("GET_CUTSCENE_COORDS - iTeam: ", iTeam, ", cutType: ", cutType," - iTeam is out of range, returning <<0,0,0>>")
				RETURN <<0,0,0>>
			ENDIF
			
			iCutsceneToUse = MC_serverBD.iCutsceneID[iteam]
			
			IF iCutsceneToUse < 0 
			OR iCutsceneToUse >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
				ASSERTLN("GET_CUTSCENE_COORDS - iTeam: ", iTeam, ", cutType: ", cutType," - iCutsceneToUse: ", iCutsceneToUse, " is out of range, returning <<0,0,0>>")
				RETURN <<0,0,0>>
			ENDIF
			
			vReturn = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerStartPos[0]
			
			BREAK
		
		CASE FMMCCUT_MOCAP
			
			IF iteam < 0
			OR iTeam >= FMMC_MAX_TEAMS
				ASSERTLN("GET_CUTSCENE_COORDS - iTeam: ", iTeam, ", cutType: ", cutType," - iTeam is out of range, returning <<0,0,0>>")
				RETURN <<0,0,0>>
			ENDIF
			
			//Pre-Streamed Mocap
			IF MC_serverBD.iCutsceneStreamingIndex[iteam] >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
			AND MC_serverBD.iCutsceneStreamingIndex[iteam] < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
			
				iCutsceneToUse = MC_serverBD.iCutsceneStreamingIndex[iteam] - FMMC_GET_MAX_SCRIPTED_CUTSCENES()
	
			// Mocap cutscene
			ELIF MC_serverBD.iCutsceneID[iteam] >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
			AND MC_serverBD.iCutsceneID[iteam] < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
				iCutsceneToUse = MC_serverBD.iCutsceneID[iteam] - FMMC_GET_MAX_SCRIPTED_CUTSCENES()
			ENDIF
			
			IF iCutsceneToUse < 0 
			OR iCutsceneToUse >= MAX_MOCAP_CUTSCENES
				ASSERTLN("GET_CUTSCENE_COORDS - iTeam: ", iTeam, ", cutType: ", cutType," - iCutsceneToUse: ", iCutsceneToUse, " is out of range, returning <<0,0,0>>")
				RETURN <<0,0,0>>
			ENDIF
			
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vCutscenePosition)
				vReturn = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vCutscenePosition
			ELSE
				vReturn = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vPlayerStartPos[0]
			ENDIF
		BREAK
		
		CASE FMMCCUT_ENDMOCAP
			vReturn = GET_END_CUTSCENE_COORDS()
		BREAK
	ENDSWITCH

	
	PRINTLN("GET_CUTSCENE_COORDS - 		iTeam: ", iteam, " cutType: ", cutType, " - vReturn = ", vReturn)
	PRINTLN("GET_CUTSCENE_COORDS - 		iCutsceneToUse: ", iCutsceneToUse)
	PRINTLN("GET_CUTSCENE_COORDS - 		MC_ServerBD.iEndCutscene: ", MC_ServerBD.iEndCutscene)
	
	IF IS_VECTOR_ZERO(vReturn)
		CERRORLN(DEBUG_CONTROLLER, "GET_CUTSCENE_COORDS - iTeam: ", iteam, " cutType: ", cutType, " - Returning a ZERO vector!")
	ENDIF
	
	RETURN vReturn
ENDFUNC

//Get the closest cutscne 
FUNC PLAYER_INDEX GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM(FMMC_CUTSCENE_TYPE cutType, INT iteam, INT iCutsceneToUse, INT iCutsceneplayer)

	INT icount
	FLOAT fdist
	FLOAT fSmallestDist
	
	fSmallestDist = GET_CUTSCENE_PLAYER_INCLUSION_RANGE(iCutsceneToUse, cutType)
	
	INT iReturnPlayer = -1
	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX tempPlayer  = INVALID_PLAYER_INDEX()
	PLAYER_INDEX closestPlayer = INVALID_PLAYER_INDEX()
	VECTOR vPlayerPos
	VECTOR vPos = GET_CUTSCENE_COORDS(iteam, cutType)
	
	PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - START")
	PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM 		vCutPos = ", vPos)
	PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM 		cutType = ", cutType)
	PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM 		inclusionRange(fSmallestDist) = ", fSmallestDist)
 
	PRINTLN("[PLAYER_LOOP] - GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM")
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() icount
		
		IF IS_BIT_SET(MC_playerBD[icount].iClientBitSet,PBBOOL_START_SPECTATOR)
		OR IS_BIT_SET(MC_playerBD[icount].iClientBitSet,PBBOOL_EARLY_END_SPECTATOR)
			PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - iPart[", icount, "] - RELOOP(CONTINUE)" )
			PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM		IS_BIT_SET(MC_playerBD[icount].iClientBitSet,PBBOOL_START_SPECTATOR) = ", IS_BIT_SET(MC_playerBD[icount].iClientBitSet,PBBOOL_START_SPECTATOR) )
			PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM 		IS_BIT_SET(MC_playerBD[icount].iClientBitSet,PBBOOL_EARLY_END_SPECTATOR) = ", IS_BIT_SET(MC_playerBD[icount].iClientBitSet,PBBOOL_EARLY_END_SPECTATOR) )
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD.iCutscenePlayerBitset[iteam],icount)
			PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - iPart[", icount, "] - RELOOP(CONTINUE)" )
			PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM		IS_BIT_SET(MC_serverBD.iCutscenePlayerBitset[iteam],icount) = ", IS_BIT_SET(MC_serverBD.iCutscenePlayerBitset[iteam],icount) )
			RELOOP
		ENDIF
		
		IF NOT SHOULD_SELECT_PLAYER_OF_TEAM(MC_playerBD[icount].iteam, iCutsceneToUse, cutType, iCutsceneplayer)
			PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - iPart[", icount, "] - RELOOP(CONTINUE)" )
			PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM		SHOULD_SELECT_PLAYER_OF_TEAM() = FALSE" )
			RELOOP
		ENDIF
		
		tempPart = INT_TO_PARTICIPANTINDEX(icount)
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - iPart[", icount, "] - RELOOP(CONTINUE)" )
			PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM		NETWORK_IS_PARTICIPANT_ACTIVE() = FALSE" )
			RELOOP
		ENDIF
		
		tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
		IF NOT IS_NET_PLAYER_OK(tempPlayer)
			PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - iPart[", icount, "] - RELOOP(CONTINUE)" )
			PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM		IS_NET_PLAYER_OK() = FALSE" )
			RELOOP
		ENDIF
		
		IF tempPlayer != LocalPlayer
			vPlayerPos = NETWORK_GET_LAST_PLAYER_POS_RECEIVED_OVER_NETWORK(tempPlayer)
			PRINTLN("[Cutscenes] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - Grabbing player's last network pos.")
		ENDIF
		
		IF IS_VECTOR_ZERO(vPlayerPos)
			vPlayerPos = GET_ENTITY_COORDS(GET_PLAYER_PED(tempPlayer))
			PRINTLN("[Cutscenes] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - vPlayerPos was ZERO, getting player ped pos")
		ENDIF
		
		fDist = GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vPos)
		//Check if the player is closer than stored

		#IF IS_DEBUG_BUILD
			PRINTLN("[Cutscenes] [PED CUT] part = ", icount, " coords = ", vPlayerPos)
		#endif 

		IF fDist >= fSmallestDist
			PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - iPart[", icount, "] - RELOOP(CONTINUE)" )
			PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM		fDist too great, fDist = ", fDist, " fSmallestDist = ", fSmallestDist )
			RELOOP
		ENDIF
		
		fSmallestDist = fDist
		closestPlayer = tempPlayer
		iReturnPlayer = icount
	ENDREPEAT
	
	//Set that we've grabbed this player!
	IF iReturnPlayer != -1
		PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - Found a player")
	ELSE
		PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - Failed to find a player")
	ENDIF
	PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM		fSmallestDist = ", fSmallestDist )
	PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM		closestPlayer = ", NATIVE_TO_INT( closestPlayer ) )
	PRINTLN("[Cutscenes] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM		iReturnPlayer = ", iReturnPlayer )
	RETURN closestPlayer
ENDFUNC

//Get the cutscene vehicle driver
//Can override the seat if required
FUNC PLAYER_INDEX GET_CUTSCENE_PLAYER_FROM_VEHICLE(FMMC_CUTSCENE_TYPE cutType, INT iteam, STRING sVehicle, INT iCutsceneToUse, VEHICLE_SEAT vsSeat = VS_DRIVER)

	PRINTLN("[Cutscenes] INSIDE GET_CUTSCENE_PLAYER_FROM_VEHICLE")

	IF IS_STRING_NULL_OR_EMPTY(sVehicle)
		PRINTLN("[Cutscenes] GET_CUTSCENE_PLAYER_FROM_VEHICLE - sVehicle = null or empty")
		RETURN INVALID_PLAYER_INDEX()
	ENDIF
	PRINTLN("[Cutscenes] GET_CUTSCENE_PLAYER_FROM_VEHICLE - HAVE VALID HANDLE FOR VEHICLE ", sVehicle)
	PRINTLN("[Cutscenes] GET_CUTSCENE_PLAYER_FROM_VEHICLE - SEAT: ", ENUM_TO_INT(vsSeat))

	INT icount
	FOR icount = 0 TO (FMMC_MAX_CUTSCENE_ENTITIES - 1)
	
		TEXT_LABEL_31 tlCutsceneHandle
		INT iCutsceneEntityIndex
	
		SWITCH cutType

			CASE FMMCCUT_ENDMOCAP
				IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[icount].iType = CREATION_TYPE_VEHICLES
					tlCutsceneHandle = g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[icount].tlCutsceneHandle
					iCutsceneEntityIndex = g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[icount].iIndex
				ENDIF
			BREAK
			
			CASE FMMCCUT_MOCAP
				IF g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[icount].iType = CREATION_TYPE_VEHICLES
					tlCutsceneHandle = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[icount].tlCutsceneHandle
					iCutsceneEntityIndex = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[icount].iIndex
				ENDIF
			BREAK
			
			CASE FMMCCUT_SCRIPTED
				IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sCutsceneEntities[icount].iType = CREATION_TYPE_VEHICLES
					iCutsceneEntityIndex = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sCutsceneEntities[icount].iIndex
				ENDIF
			BREAK
			
			DEFAULT
				tlCutsceneHandle = ""
				iCutsceneEntityIndex = -1
			BREAK
			
		ENDSWITCH
		
		// No valid entity index, skip to next
		IF iCutsceneEntityIndex = -1
		OR iCutsceneEntityIndex >= FMMC_MAX_VEHICLES
			RELOOP
		ENDIF
		
		// handle doesn't match, skip to next
		IF NOT ARE_STRINGS_EQUAL(tlCutsceneHandle, sVehicle) 
		AND NOT ARE_STRINGS_EQUAL(sVehicle, "ANY")
			RELOOP
		ENDIF
		
		// Invalid/no existent entity, skip to next
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iCutsceneEntityIndex])
			RELOOP
		ENDIF

		VEHICLE_INDEX veh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iCutsceneEntityIndex])
		IF NOT DOES_ENTITY_EXIST(veh)
			RELOOP
		ENDIF

		INT iPart
		PRINTLN("[PLAYER_LOOP] - GET_CUTSCENE_PLAYER_FROM_VEHICLE")
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPart

			IF IS_BIT_SET(MC_serverBD.iCutscenePlayerBitset[iteam], iPart)
				RELOOP
			ENDIF

			IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_START_SPECTATOR)
				RELOOP
			ENDIF
			
			IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_EARLY_END_SPECTATOR)
				RELOOP
			ENDIF
			
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)
			IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				RELOOP
			ENDIF
						
			PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
			IF NOT IS_NET_PLAYER_OK(tempPlayer)
				RELOOP
			ENDIF
			
			PED_INDEX tempPed = GET_PLAYER_PED(tempPlayer)
			IF NOT IS_PED_IN_VEHICLE(TempPed, veh)
				RELOOP
			ENDIF
			
			IF tempPed != GET_PED_IN_VEHICLE_SEAT(veh, vsSeat)
				RELOOP
			ENDIF
			
			PRINTLN("[Cutscenes] GET_CUTSCENE_PLAYER_FROM_VEHICLE - iTeam: ", iTeam, " - found player in iCutsceneEntityIndex: ", iCutsceneEntityIndex, " seat: ", vsSeat)
			RETURN tempPlayer

		ENDREPEAT
		
	ENDFOR
	
	PRINTLN("[Cutscenes] GET_CUTSCENE_PLAYER_FROM_VEHICLE - iTeam: ", iTeam, " - could not find player in vehicle seat")
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

FUNC STRING GET_MOCAP_CUTSCENE_NAME(INT icutsceneNum, INT iCutsceneType, INT iTeam, FMMC_CUTSCENE_TYPE cutType)
	
	INT iMocap
	INT iLoop
	INT iCutsceneIndexToUse[FMMC_MAX_TEAMS]
	STRING sCutsceneName

	SWITCH cutType
	
		CASE FMMCCUT_MOCAP
			
			iCutsceneStreamingTeam = iTeam
			
			IF IS_BIT_SET(iLocalBoolCheck9, LBOOL9_FORCE_STREAM_CUTSCENE)
				FOR iLoop = 0 TO FMMC_MAX_TEAMS - 1
					iCutsceneIndexToUse[iLoop] = MC_serverBD.iCutsceneID[ iLoop ]
				ENDFOR
			ELSE
				FOR iLoop = 0 TO FMMC_MAX_TEAMS - 1
					iCutsceneIndexToUse[iLoop] = MC_serverBD.iCutsceneStreamingIndex[iLoop]
				ENDFOR
			ENDIF
			
			PRINTLN("[LM][GET_MOCAP_CUTSCENE_NAME] - icutsceneNum: ", icutsceneNum, " icutsceneType: ", iCutsceneType, " iCutsceneStreamingTeam: ", iCutsceneStreamingTeam, " LBOOL2_CUTSCENE_STREAMED: ", IS_BIT_SET(iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED))
			
			IF iCutsceneStreamingTeam >= 0
			AND NOT g_bMissionEnding
				
				PRINTLN("[LM][GET_MOCAP_CUTSCENE_NAME] - iCutsceneIndexToUse: ",  iCutsceneIndexToUse[iCutsceneStreamingTeam])
				
				IF iCutsceneIndexToUse[iCutsceneStreamingTeam] >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
				AND iCutsceneIndexToUse[iCutsceneStreamingTeam] < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
					iMocap = (iCutsceneIndexToUse[iCutsceneStreamingTeam] - FMMC_GET_MAX_SCRIPTED_CUTSCENES())
					
					IF NOT IS_STRING_NULL_OR_EMPTY( g_FMMC_STRUCT.sMocapCutsceneData[iMocap].tlName)
					AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iCutsceneFinishedBitset, iCutsceneIndexToUse[iCutsceneStreamingTeam])
						sCutsceneName = Get_String_From_TextLabel(g_FMMC_STRUCT.sMocapCutsceneData[iMocap].tlName )						
						PRINTLN("[Cutscenes][GET_MOCAP_CUTSCENE_NAME] - Mid-mission cutscene found, use iMocap ", iMocap, " name is ", sCutsceneName )
						IF NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_MID_MISSION_MOCAP )
							SET_BIT( iLocalBoolCheck5, LBOOL5_MID_MISSION_MOCAP )							
							PRINTLN("[Cutscenes][GET_MOCAP_CUTSCENE_NAME] - Mid-mission cutscene found, use iMocap ", iMocap, " name is ", sCutsceneName )
						ENDIF
						RETURN sCutsceneName
					ELSE
						PRINTLN("[Cutscenes][GET_MOCAP_CUTSCENE_NAME] - Invalid name or this cutscene has been played already: ", sCutsceneName )				
					ENDIF
				ELSE
				//	PRINTLN("[Cutscenes][GET_MOCAP_CUTSCENE_NAME] - Not found a valid cutscene to stream - not a mocap" )			
				ENDIF
			ENDIF
			
			IF iCutsceneNum = -1
				RETURN ""
			ENDIF
			
			//PRINTLN("[Cutscenes] GET_MOCAP_CUTSCENE_NAME - Returning sCutsceneName = ",sCutsceneName)
			
		BREAK
		
		CASE FMMCCUT_ENDMOCAP
		
			SWITCH iCutsceneType
				// Casino Heist
				CASE ciMISSION_CUTSCENE_CASINO_HEIST_DRP1
					sCutsceneName = "HS3F_ALL_DRP1" 
				BREAK
				CASE ciMISSION_CUTSCENE_CASINO_HEIST_DRP2
					sCutsceneName = "HS3F_ALL_DRP2" 
				BREAK
				CASE ciMISSION_CUTSCENE_CASINO_HEIST_DRP3
					sCutsceneName = "HS3F_ALL_DRP3" 
				BREAK
				
				// Island Heist
				CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_MAIN_ENT
					sCutsceneName = "HS4F_MAIN_ENT"
				BREAK
				
				CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_NRP_ENT
					sCutsceneName = "HS4F_NRP_ENT"
				BREAK
			
				CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_DRP_OFF
					sCutsceneName = "HS4F_DRP_OFF"
				BREAK
				
				CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_TUN_ENT
					sCutsceneName = "HS4F_TUN_ENT"
				BREAK
				
				CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_MAIN_EXT
					sCutsceneName = "HS4F_MAIN_EXT"
				BREAK
				
				CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_TRO_ENT
					sCutsceneName = "HS4F_TRO_ENT"
				BREAK	
				
				CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_SSG_ENT_V1
					sCutsceneName = "HS4F_SSG_ENT"
				BREAK	
				
				CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_SSG_ENT_V2
					sCutsceneName = "HS4F_SSG_ENT"
				BREAK	
				
				CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_EXT
					sCutsceneName = "HS4F_EXT"
				BREAK	
				
				CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_SSG_EXT_V1
					sCutsceneName = "HS4F_SSG_EXT"
				BREAK	
				
				CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_SSG_EXT_V2
					sCutsceneName = "HS4F_SSG_EXT"
				BREAK
				
				// Tuner
				CASE ciMISSION_CUTSCENE_TUNER_TUNF_DRP_HAND
					sCutsceneName = "TUNF_DRP_HAND"
				BREAK
				
				CASE ciMISSION_CUTSCENE_TUNER_TUNF_DRP_BAG
					sCutsceneName = "TUNF_DRP_BAG"
				BREAK
				
				CASE ciMISSION_CUTSCENE_TUNER_TUNF_DRP_AVI
					sCutsceneName = "TUNF_DRP_AVI"
				BREAK
				
				//Fixer
				CASE ciMISSION_CUTSCENE_FIXER_FIX_GOLF_EXT
					sCutsceneName = "FIX_GOLF_EXT"
				BREAK
				
				CASE ciMISSION_CUTSCENE_FIXER_FIX_TRIP1_EXT
					sCutsceneName = "FIX_TRIP1_EXT"
				BREAK
				
				CASE ciMISSION_CUTSCENE_FIXER_FIX_STU_EXT
					sCutsceneName = "FIX_STU_EXT"
				BREAK
				
				CASE ciMISSION_CUTSCENE_FIXER_FIX_TRIP2_EXT
					sCutsceneName = "FIX_TRIP2_EXT"
				BREAK	
				
				CASE ciMISSION_CUTSCENE_FIXER_FIX_FIN_MCS3
					sCutsceneName = "FIXF_FIN_MCS3"
				BREAK
				
				CASE ciMISSION_CUTSCENE_FIXER_FIXF_TRIP3_EXT
					sCutsceneName = "FIX_TRIP3_EXT"
				BREAK
				
			ENDSWITCH
			
		BREAK
		
	ENDSWITCH
	
	RETURN sCutsceneName

ENDFUNC

FUNC PLAYER_INDEX GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM_WITH_PRIORITY_TO_TEAM(STRING strCutsceneName, INT iTeam, FMMC_CUTSCENE_TYPE cutType, INT icutscene, BOOL bUsedFallbackRange)
	
	VECTOR vCutPos = GET_CUTSCENE_COORDS(iTeam, cutType)
	FLOAT fSmallestDist
	
	IF bUsedFallbackRange
		fSmallestDist = 999999
	ELSE
		fSmallestDist = GET_CUTSCENE_PLAYER_INCLUSION_RANGE(iCutscene, cutType)
	ENDIF
	
	PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM_WITH_PRIORITY_TO_TEAM - START")
	PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM_WITH_PRIORITY_TO_TEAM 		vCutPos = ", vCutPos)
	PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM_WITH_PRIORITY_TO_TEAM 		bUsedFallbackRange = ", BOOL_TO_STRING(bUsedFallbackRange))
	PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM_WITH_PRIORITY_TO_TEAM 		cutType = ", cutType)
	PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM_WITH_PRIORITY_TO_TEAM 		strCutsceneName = ", strCutsceneName)
	PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM_WITH_PRIORITY_TO_TEAM 		inclusionRange(fSmallestDist) = ", fSmallestDist)
	
	BOOL bDonePriorityTeam
	INT iPart, iTeamLoop	
	FOR iTeamLoop = iTeam TO (MC_serverBD.iNumberOfTeams-1)
		
		// Do the Priority Team first, then the rest. Skip the Priority Team 
		IF iTeamLoop = iTeam
		AND bDonePriorityTeam
			RELOOP
		ENDIF
		
		iPart = -1
		DO_PARTICIPANT_LOOP_FLAGS eFlags = DPLF_CHECK_PLAYER_OK | DPLF_FILL_PLAYER_IDS | GET_PARTICIPANT_LOOP_FLAGS_TO_CHECK_SINGLE_TEAM(iTeamLoop)
		WHILE DO_PARTICIPANT_LOOP(iPart, eFlags)
			IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_START_SPECTATOR)
				PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM_WITH_PRIORITY_TO_TEAM - PBBOOL_START_SPECTATOR = TRUE")
				RELOOP
			ENDIF
			
			IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_EARLY_END_SPECTATOR)
				PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM_WITH_PRIORITY_TO_TEAM - PBBOOL_EARLY_END_SPECTATOR = TRUE")
				RELOOP
			ENDIF
			
			IF IS_BIT_SET(MC_serverBD.iCutscenePlayerBitset[iTeam], ipart)
				PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM_WITH_PRIORITY_TO_TEAM - IS_BIT_SET(MC_serverBD.iCutscenePlayerBitset[iteam], ipart) = TRUE")
				RELOOP
			ENDIF
			
			IF DOES_TEAM_LIKE_TEAM(iTeamLoop, iTeam)
			AND SHOULD_TEAM_BE_IN_CUTSCENE(cutType, icutscene, iTeamLoop, iTeam)
								
				IF IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR(piParticipantLoop_PlayerIndex)
				OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piParticipantLoop_PlayerIndex)
				OR IS_PLAYER_SCTV(piParticipantLoop_PlayerIndex)
					PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM_WITH_PRIORITY_TO_TEAM - Player is flagged as a JIP or SCTV Spectator.")
					RELOOP
				ENDIF
				
				FLOAT fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(piParticipantLoop_PedIndex, vCutPos)
				IF fDist > fSmallestDist
					PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM_WITH_PRIORITY_TO_TEAM - Out of range of fSmallestDist: ", fSmallestDist)
					RELOOP
				ENDIF

				PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM_WITH_PRIORITY_TO_TEAM - Found a player. ReturnPlayer: ", GET_PLAYER_NAME(piParticipantLoop_PlayerIndex))
				RETURN piParticipantLoop_PlayerIndex

			ENDIF
		ENDWHILE
		
		IF iTeamLoop = iTeam
		AND NOT bDonePriorityTeam
			iTeamLoop = 0
			bDonePriorityTeam = TRUE
		ENDIF
	ENDFOR
		
	PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM_WITH_PRIORITY_TO_TEAM - Failed to find a player")
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

FUNC PLAYER_INDEX GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM(STRING strCutsceneName, INT iteam, FMMC_CUTSCENE_TYPE cutType, INT icutscene, BOOL bUsedFallbackRange)
	
	VECTOR vCutPos = GET_CUTSCENE_COORDS(iteam, cutType)
	FLOAT fSmallestDist
	
	IF bUsedFallbackRange
		fSmallestDist = 999999
	ELSE
		fSmallestDist = GET_CUTSCENE_PLAYER_INCLUSION_RANGE(iCutscene, cutType)
	ENDIF
	
	PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - START")
	PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM 		vCutPos = ", vCutPos)
	PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM 		bUsedFallbackRange = ", BOOL_TO_STRING(bUsedFallbackRange))
	PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM 		cutType = ", cutType)
	PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM 		strCutsceneName = ", strCutsceneName)
	PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM 		inclusionRange(fSmallestDist) = ", fSmallestDist)
	
	INT iPart
	PRINTLN("[PLAYER_LOOP] - GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM")
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() ipart
		
		IF IS_BIT_SET(MC_playerBD[ipart].iClientBitSet, PBBOOL_START_SPECTATOR)
			PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - PBBOOL_START_SPECTATOR = TRUE")
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[ipart].iClientBitSet, PBBOOL_EARLY_END_SPECTATOR)
			PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - PBBOOL_EARLY_END_SPECTATOR = TRUE")
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD.iCutscenePlayerBitset[iteam], ipart)
			PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - IS_BIT_SET(MC_serverBD.iCutscenePlayerBitset[iteam], ipart) = TRUE")
			RELOOP
		ENDIF
			
		IF MC_playerBD[ipart].iteam = iteam
		OR (DOES_TEAM_LIKE_TEAM(MC_playerBD[ipart].iteam,iteam)
		AND SHOULD_TEAM_BE_IN_CUTSCENE(cutType, icutscene, MC_playerBD[ipart].iteam , iteam))
			
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(ipart)
			IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - Inactive participant")
				RELOOP
			ENDIF
			
			PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
			IF NOT IS_NET_PLAYER_OK(tempPlayer, NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_PlayEndCutsceneForAllPlayers))
				PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - net player not OK")
				RELOOP
			ENDIF
			
			IF IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR(tempPlayer)
			OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(tempPlayer)
			OR IS_PLAYER_SCTV(tempPlayer)
				PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - Player is flagged as a JIP or SCTV Spectator.")
				RELOOP
			ENDIF
			
			PED_INDEX tempPed = GET_PLAYER_PED(tempPlayer)
			
			FLOAT fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(tempPed, vCutPos)
			IF fDist > fSmallestDist
				PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - Out of range of fSmallestDist: ", fSmallestDist)
				RELOOP
			ENDIF

			PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - Found a player = ReturnPlayer")
			RETURN tempPlayer

		ENDIF

	ENDREPEAT
	
	PRINTLN("[Cutscenes] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - Failed to find a player")
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

FUNC BOOL IS_CUTSCENE_REQUESTED_FOR_TEAM( INT iteam )
	
	IF IS_INTRO_CUTSCENE_RULE_ACTIVE()
		RETURN TRUE
	ENDIF
	
	SWITCH iteam
	
		CASE 0
			IF IS_BIT_SET( MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_0 )
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 1
			IF IS_BIT_SET( MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_1 )
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 2
			IF IS_BIT_SET( MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_2 )
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 3
			IF IS_BIT_SET( MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_3 )
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE


ENDFUNC

///PURPOSE: Returns TRUE if iThisTeam should watch iCutsceneTeam's cutscene
///    
///PARAMETERS:
///    	* iThisTeam - The team that wants to watch the cutscene
///     * iCutsceneTeam - The team that owns the cutscene
///    
///RETURNS: TRUE if iThisTeam should watch iCutsceneTeam's cutscene, otherwise FALSE
FUNC BOOL SHOULD_THIS_TEAM_WATCH_OTHER_TEAM_CUTSCENE( INT iThisTeam, INT iCutsceneTeam )
	
	IF iThisTeam < 0 OR iThisTeam > 3
		CASSERTLN( DEBUG_CONTROLLER, "[Cutscenes] SET_THIS_TEAM_SHOULD_WATCH_OTHER_TEAM_CUTSCENE - iThisTeam is invalid - something has gone terribly wrong" )
	ENDIF
	
	IF iCutsceneTeam < 0 OR iCutsceneTeam > 3
		CASSERTLN( DEBUG_CONTROLLER, "[Cutscenes] SET_THIS_TEAM_SHOULD_WATCH_OTHER_TEAM_CUTSCENE - iCutsceneTeam is invalid - something has gone terribly wrong" )
	ENDIF
	
	BOOL bReturn = FALSE
	
	IF iThisTeam = iCutsceneTeam
		bReturn = TRUE
	ENDIF
	
	SWITCH iCutsceneTeam
		CASE 0
			IF IS_BIT_SET( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM0_CUTSCENE_SEEN_BY_TEAM0 + iThisTeam )
				bReturn = TRUE
			ENDIF
		BREAK
		CASE 1
			IF IS_BIT_SET( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM1_CUTSCENE_SEEN_BY_TEAM0 + iThisTeam )	
				bReturn = TRUE
			ENDIF
		BREAK
		CASE 2
			IF IS_BIT_SET( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM2_CUTSCENE_SEEN_BY_TEAM0 + iThisTeam )	
				bReturn = TRUE
			ENDIF
		BREAK
		CASE 3
			IF IS_BIT_SET( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM3_CUTSCENE_SEEN_BY_TEAM0 + iThisTeam )	
				bReturn = TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN bReturn
ENDFUNC

FUNC INT GET_NUMBER_OF_PEOPLE_SHOULD_BE_WATCHING_CUTSCENE( INT iTeam )
	INT iReturn = 0
	
	INT iTeamLoop = 0
	FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 1
		IF SHOULD_THIS_TEAM_WATCH_OTHER_TEAM_CUTSCENE(iTeamLoop, iTeam)
			iReturn += MC_ServerBD_2.iNumPartPlayingAndFinished[iTeamLoop] + MC_ServerBD_1.iNumCutsceneSpectatorPlayers[iTeamLoop]
		ENDIF
	ENDFOR
	
	RETURN iReturn
ENDFUNC

FUNC INT GET_NUMBER_OF_PEOPLE_FINISHED_CUTSCENE( INT iTeam )
	INT iReturn = 0
	
	INT iTeamLoop = 0
	FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 1
		IF SHOULD_THIS_TEAM_WATCH_OTHER_TEAM_CUTSCENE(iTeamLoop, iTeam)
			iReturn += MC_serverBD.iNumCutscenePlayersFinished[iTeamLoop]
		ENDIF
	ENDFOR
	
	RETURN iReturn
ENDFUNC

FUNC BOOL ARE_ANY_PLAYERS_ON_TEAM_REQUESTING_CUTSCENE(INT iTeam)
	
	INT iPart = -1
	DO_PARTICIPANT_LOOP_FLAGS eFlags = GET_PARTICIPANT_LOOP_FLAGS_TO_CHECK_SINGLE_TEAM(iTeam)
	WHILE DO_PARTICIPANT_LOOP(iPart, eFlags | DPLF_EARLY_END_SPECTATORS)
		IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_REQUEST_CUTSCENE_PLAYERS)
			PRINTLN("[Cutscenes] ARE_ANY_PLAYERS_ON_TEAM_REQUESTING_CUTSCENE - team ", iTeam , " iPlayer: ", iPart, " Requesting cutscene still...")
			RETURN TRUE
		ENDIF
	ENDWHILE
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_EVERYONE_FINISHED_WATCHING_THIS_TEAMS_CUTSCENE( INT iTeam )

	INT iNumberOfPeopleShouldWatchCutscene = GET_NUMBER_OF_PEOPLE_SHOULD_BE_WATCHING_CUTSCENE(iTeam)
	INT iNumberOfPeopleFinishedCutscene = GET_NUMBER_OF_PEOPLE_FINISHED_CUTSCENE(iTeam)
	
	PRINTLN("[Cutscenes] HAS_EVERYONE_FINISHED_WATCHING_THIS_TEAMS_CUTSCENE - iTeam: ", iTeam, " iNumberOfPeopleShouldWatchCutscene: ", iNumberOfPeopleShouldWatchCutscene, " iNumberOfPeopleFinishedCutscene: ", iNumberOfPeopleFinishedCutscene)
	
	IF iNumberOfPeopleShouldWatchCutscene > 0
		IF iNumberOfPeopleFinishedCutscene >= iNumberOfPeopleShouldWatchCutscene
			IF NOT ARE_ANY_PLAYERS_ON_TEAM_REQUESTING_CUTSCENE(iTeam)
				PRINTLN("[Cutscenes] HAS_EVERYONE_FINISHED_WATCHING_THIS_TEAMS_CUTSCENE returning TRUE for team ", iTeam)
				RETURN TRUE
			ELSE
				PRINTLN("[Cutscenes] HAS_EVERYONE_FINISHED_WATCHING_THIS_TEAMS_CUTSCENE team ", iTeam , " Players are still requesting cutscene!!!!")
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Check if the cutscene needs players
FUNC BOOL DOES_MOCAP_CUTSCENE_NEED_PLAYERS(STRING cutname, INT iCutsceneToUse)

	IF NOT IS_STRING_NULL_OR_EMPTY(cutname)
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Hide_Your_Player) AND IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Hide_Other_Players)
			PRINTLN("[Cutscene_Mocap] - DOES_MOCAP_CUTSCENE_NEED_PLAYERS - CUTSCENE All Players Set to hidden - Returning False")
			RETURN FALSE
		ENDIF
		
		PRINTLN("[Cutscene_Mocap] - DOES_MOCAP_CUTSCENE_NEED_PLAYERS - Returning TRUE")
		
		RETURN TRUE
	ENDIF

	//Shouldn't return this
	PRINTLN("[Cutscene_Mocap] - DOES_MOCAP_CUTSCENE_NEED_PLAYERS - CUTSCENE STRING WAS EMPTY")
	RETURN TRUE

ENDFUNC

//Get the end cutscene vehicle
FUNC STRING GET_CUTSCENE_VEHICLE_STRING(FMMC_CUTSCENE_TYPE cutType, INT iCutsceneToUse)

	UNUSED_PARAMETER(iCutsceneToUse)

	SWITCH cutType
	
		CASE FMMCCUT_ENDMOCAP
		
		BREAK
		
		CASE FMMCCUT_MOCAP		RETURN "ANY"
		CASE FMMCCUT_SCRIPTED	RETURN "ANY"
	
	ENDSWITCH
	
	RETURN ""
ENDFUNC

//Check if we should return the passenger for a cutscene for the handle
FUNC BOOL SHOULD_CUTSCENE_GET_PASSENGER(FMMC_CUTSCENE_TYPE cutType, INT iCutsceneToUse)
	
	UNUSED_PARAMETER(iCutsceneToUse)
	SWITCH cutType

		CASE FMMCCUT_MOCAP
			IF IS_BIT_SET(g_fmmc_struct.sCurrentMocapSceneData.iCutsceneBitSet3, ci_CSBS3_PassengersNotSuitable)
				RETURN FALSE
			ENDIF
		BREAK
	
	ENDSWITCH
	
	PRINTLN("[Cutscenes] SHOULD_CUTSCENE_GET_PASSENGER - RETURNING TRUE")
	RETURN TRUE
ENDFUNC

///PURPOSE: This function checks creator data and picks the correct player for each role in the cutscene
///    e.g. if the heist leader should be the primary it'll pick them first etc
FUNC PLAYER_INDEX GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM(INT iteam, INT iplayer, INT iCutsceneToUse, FMMC_CUTSCENE_TYPE cutType, STRING strCutsceneName, BOOL bTeamPrioritySlots)

	PRINTLN("[Cutscenes] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM iteam = ", iteam)
	PRINTLN("[Cutscenes] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM iplayer = ", iplayer)
	PRINTLN("[Cutscenes] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM iCutsceneToUse = ", iCutsceneToUse)
	PRINTLN("[Cutscenes] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM cutType = ", GET_FMMC_CUTSCENE_TYPE_NAME(cutType))
	PRINTLN("[Cutscenes] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM strCutsceneName = ", strCutsceneName)
	PRINTLN("[Cutscenes] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM sMocapCutscene = ", sMocapCutscene)

	PLAYER_INDEX tempPlayer = INVALID_PLAYER_INDEX()
		
	//
	// SPECIAL CASES
	//
	tempPlayer = GET_CUTSCENE_PLAYER_SPECIAL_CASE(/*iTeam, iPlayer, iCutsceneToUse, strCutsceneName, cutType*/)
	IF tempPlayer != INVALID_PLAYER_INDEX()
		PRINTLN("[Cutscenes] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - iTeam: ", iteam, " iPlayer: ", iPlayer, " Found player: ", NATIVE_TO_INT(tempPlayer), " GET_PLAYER_NAME: ", GET_PLAYER_NAME(tempPlayer), " using GET_CUTSCENE_PLAYER_SPECIAL_CASE")
		RETURN tempPlayer
	ENDIF

	//
	// TEAM PRIORITIZED SLOTS
	//
	IF bTeamPrioritySlots
		tempPlayer = GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM_WITH_PRIORITY_TO_TEAM(strCutsceneName, iteam, cutType, iCutsceneToUse, FALSE)
		IF tempPlayer != INVALID_PLAYER_INDEX()
			PRINTLN("[Cutscenes] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - iTeam: ", iteam, " iPlayer: ", iPlayer, " Found player: ", NATIVE_TO_INT(tempPlayer), " GET_PLAYER_NAME: ", GET_PLAYER_NAME(tempPlayer), " using GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM_WITH_PRIORITY_TO_TEAM")
			RETURN tempPlayer
		ENDIF
	ENDIF
		
	//
	// HEIST LEADER
	//
	tempPlayer = GET_HEIST_LEADER_PLAYER_FOR_TEAM(iteam, cutType, iCutsceneToUse, iplayer)
	IF tempPlayer != INVALID_PLAYER_INDEX()
		PRINTLN("[Cutscenes] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - iTeam: ", iteam, " iPlayer: ", iPlayer, " Found player: ", NATIVE_TO_INT(tempPlayer), " using GET_HEIST_LEADER_PLAYER_FOR_TEAM")
		RETURN tempPlayer
	ENDIF
	
	// 
	// OBJECTIVE PLAYER (PRIMARY ONLY)
	//
	IF iplayer = 0
	AND MC_serverBD.piCutscenePlayerIDs[iteam][0] != INVALID_PLAYER_INDEX()
	AND IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[iteam][0])
		tempPlayer = MC_serverBD.piCutscenePlayerIDs[iteam][0]
		PRINTLN("[Cutscenes] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - iTeam: ", iteam, " iPlayer: ", iPlayer, " Found player: ", NATIVE_TO_INT(tempPlayer), " GET_PLAYER_NAME: ", GET_PLAYER_NAME(tempPlayer), " using Objective Completed by this player")
		RETURN tempPlayer
	ENDIF
	
	//
	// VEHICLE 
	//
	VEHICLE_SEAT vehicleSeat
	SWITCH iplayer
		CASE 0	vehicleSeat = VS_DRIVER			BREAK
		CASE 1	vehicleSeat = VS_FRONT_RIGHT	BREAK
		CASE 2	vehicleSeat = VS_BACK_LEFT		BREAK
		CASE 3	vehicleSeat = VS_BACK_RIGHT		BREAK
	ENDSWITCH
	
	IF vehicleSeat = VS_DRIVER 
	OR SHOULD_CUTSCENE_GET_PASSENGER(cutType, iCutsceneToUse)
		TempPlayer = GET_CUTSCENE_PLAYER_FROM_VEHICLE(cutType, iteam, GET_CUTSCENE_VEHICLE_STRING(cutType, iCutsceneToUse), iCutsceneToUse, vehicleSeat)
		IF tempPlayer != INVALID_PLAYER_INDEX()
			PRINTLN("[Cutscenes] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - iTeam: ", iteam, " iPlayer: ", iPlayer, " Found player: ", NATIVE_TO_INT(tempPlayer), " GET_PLAYER_NAME: ", GET_PLAYER_NAME(tempPlayer), " using GET_CUTSCENE_PLAYER_FROM_VEHICLE")
			RETURN tempPlayer
		ENDIF
	ENDIF

	//
	// CLOSEST PLAYER
	//
	tempPlayer = GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM(cutType, iteam, iCutsceneToUse, iplayer)
	IF tempPlayer != INVALID_PLAYER_INDEX()
		PRINTLN("[Cutscenes] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - iTeam: ", iteam, " iPlayer: ", iPlayer, " Found player: ", NATIVE_TO_INT(tempPlayer), " GET_PLAYER_NAME: ", GET_PLAYER_NAME(tempPlayer), " using GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM")
		RETURN tempPlayer
	ENDIF
		
	//
	// ANY SUITIBLE PLAYER FOR TEAM
	//
	tempPlayer = GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM(strCutsceneName, iteam, cutType, iCutsceneToUse, FALSE)
	IF tempPlayer != INVALID_PLAYER_INDEX()
		PRINTLN("[Cutscenes] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - iTeam: ", iteam, " iPlayer: ", iPlayer, " Found player: ", NATIVE_TO_INT(tempPlayer), " GET_PLAYER_NAME: ", GET_PLAYER_NAME(tempPlayer), " using GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM")
		RETURN tempPlayer
	ENDIF
	
	IF iplayer = 0
		//
		// ANY SUITIBLE PLAYER FOR TEAM (with fallback range)
		//
		tempPlayer = GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM(strCutsceneName, iteam, cutType, iCutsceneToUse, TRUE)
		IF tempPlayer != INVALID_PLAYER_INDEX()
			PRINTLN("[Cutscenes] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - iTeam: ", iteam, " iPlayer: ", iPlayer, " Found player: ", NATIVE_TO_INT(tempPlayer), " GET_PLAYER_NAME: ", GET_PLAYER_NAME(tempPlayer), " using GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM(FALLBACK RANGE)")
			RETURN tempPlayer
		ENDIF
	ENDIF
	
	PRINTLN("[Cutscenes] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - team: ", iteam, " iPlayer: ", iPlayer, " failed to find player index")
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

PROC SELECT_CUTSCENE_PLAYERS_FOR_TEAM(INT iteam)

	INT iplayer
	INT iCutsceneToUse = -1
	FMMC_CUTSCENE_TYPE cutType
	
	TEXT_LABEL_63 sCutName
	BOOL bUsePlayers = TRUE
	PLAYER_INDEX tempPlayer
	BOOL bTeamPrioritySlots
	BOOL bClearObjectivePrimaryPlayer
	
	IF iteam < 0 AND iteam >= FMMC_MAX_TEAMS
		EXIT
	ENDIF
	
	// Players have already been selected
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_CUTSCENE_TEAM0_PLAYERS_SELECTED + iTeam)
		EXIT
	ENDIF
	
	// We are watching someone elses Cutscene and don't need to assign cutscene players to that array as they will not be used.
	INT iTeamLoop
	FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 1
		
		IF iTeamLoop =  iTeam
			RELOOP
		ENDIF
		
		PRINTLN("[Cutscenes] [PED CUT] - iTeam: ", iTeam, " Checking for iTeamLoop: ", iTeamLoop)
		
		IF SHOULD_THIS_TEAM_WATCH_OTHER_TEAM_CUTSCENE(iTeam, iTeamLoop)
			PRINTLN("[Cutscenes] [PED CUT] - iTeam: ", iTeam, " does not need to select players. Watching iTeamLoop : ", iTeamLoop, "'s cutscene.")
			EXIT			
		ENDIF
	ENDFOR	
	
	IF NOT IS_CUTSCENE_REQUESTED_FOR_TEAM(iteam)
		EXIT
	ENDIF
		
	PRINTLN("[Cutscenes] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - MC_serverBD.iCutsceneID[", iTeam, "]: ", MC_serverBD.iCutsceneID[iteam])
		
	// Scripted cut
	IF MC_serverBD.iCutsceneID[iteam] >= 0 
	AND MC_serverBD.iCutsceneID[iteam] < FMMC_GET_MAX_SCRIPTED_CUTSCENES()

		cutType = FMMCCUT_SCRIPTED
		iCutsceneToUse = MC_serverBD.iCutsceneID[iteam]
		bUsePlayers = TRUE
		bTeamPrioritySlots = IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_ForceTeamTriggeringCutsceneForSlotPriority)
		bClearObjectivePrimaryPlayer = IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_ClearExistingPrimaryCutscenePlayer)
		
	// Mocap
	ELIF MC_serverBD.iCutsceneID[iteam] >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
	AND MC_serverBD.iCutsceneID[iteam] < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
		
		cutType = FMMCCUT_MOCAP
		iCutsceneToUse = MC_serverBD.iCutsceneID[iteam] - FMMC_GET_MAX_SCRIPTED_CUTSCENES()
		sCutName = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName
		bTeamPrioritySlots = IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_ForceTeamTriggeringCutsceneForSlotPriority)
		bClearObjectivePrimaryPlayer = IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_ClearExistingPrimaryCutscenePlayer)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sCutName) 
			bUsePlayers = DOES_MOCAP_CUTSCENE_NEED_PLAYERS(sCutName, iCutsceneToUse)
			PRINTLN("[Cutscenes] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - iTeam: ", iTeam, " - DOES_MOCAP_CUTSCENE_NEED_PLAYERS : ", BOOL_TO_STRING(bUsePlayers))
		ELSE
			PRINTLN("[Cutscenes] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - iTeam: ", iTeam, " - sCutName is empty")
		ENDIF
		
	// End Mocap
	ELIF MC_serverBD.iEndCutsceneNum[iteam] != -1
		
		cutType = FMMCCUT_ENDMOCAP		
		bTeamPrioritySlots = IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet3, ci_CSBS3_ForceTeamTriggeringCutsceneForSlotPriority)
		bClearObjectivePrimaryPlayer = IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet3, ci_CSBS3_ClearExistingPrimaryCutscenePlayer)
		
		IF IS_STRING_NULL_OR_EMPTY(sCutName)
		AND NOT IS_STRING_NULL_OR_EMPTY(sMocapCutscene)
//			PRINTLN("[Cutscenes] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - bEndMocap = TRUE")
			PRINTLN("[Cutscenes] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - iTeam: ", iTeam, " - setting sCutName to sMocapCutscene, (", sMocapCutscene, ")")
			sCutName = sMocapCutscene
		ELSE
			PRINTLN("[Cutscenes] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - iTeam: ", iTeam, " - bEndMocap = TRUE, but either sCutName is filled or sMocapCutscene is empty")
		ENDIF
		
	ELSE
		// If this asserts in content where Team 1 is watching Team 0's cutscene then ignore it. Players are put into the team 0 array and grabbed from there.
		ASSERTLN("SELECT_CUTSCENE_PLAYERS_FOR_TEAM - SELECT_CUTSCENE_PLAYERS_FOR_TEAM Exit - Players could not be selected from Team: ", iTeam)
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_SELECT_CUTSCENE_PLAYERS_FOR_TEAM_EXITTED, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "SELECT_CUTSCENE_PLAYERS_FOR_TEAM Exit - Players could not be selected from Team # ", iTeam)
		#ENDIF
		EXIT
	ENDIF

	PRINTLN("[Cutscenes] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - iTeam: ", iteam, 
		" cutType: ", GET_FMMC_CUTSCENE_TYPE_NAME(cutType), 
		" iCutsceneID: ", MC_serverBD.iCutsceneID[iteam],
		" iEndCutsceneNum: ", MC_serverBD.iEndCutsceneNum[iteam])
	
	IF bClearObjectivePrimaryPlayer		
		PRINTLN("[Cutscenes] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - bClearObjectivePrimaryPlayer = TRUE, Clearing primary player")
		CLEAR_CUTSCENE_PRIMARY_PLAYER(iTeam)
		
		#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitset8, SBBOOL8_PREVENT_CUTSCENE_PRIMARY_PLAYER_FROM_BEING_ADDED)
			PRINTLN("[Cutscenes] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - bClearObjectivePrimaryPlayer = TRUE, Setting SBBOOL8_PREVENT_CUTSCENE_PRIMARY_PLAYER_FROM_BEING_ADDED")
		ENDIF
		#ENDIF
		SET_BIT(MC_serverBD.iServerBitset8, SBBOOL8_PREVENT_CUTSCENE_PRIMARY_PLAYER_FROM_BEING_ADDED)
	ENDIF
	
	IF bUsePlayers
		
		MC_serverBD.iCutscenePlayerBitset[iteam] = 0
		
		FOR iplayer = 0 TO (FMMC_MAX_CUTSCENE_PLAYERS - 1)
			tempplayer = GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM(iteam, iplayer, iCutSceneToUse, cutType, sCutName, bTeamPrioritySlots)
			IF tempplayer != INVALID_PLAYER_INDEX()
			
				MC_serverBD.piCutscenePlayerIDs[iteam][iplayer] = tempplayer
				SET_BIT(MC_serverBD.iCutscenePlayerBitset[iteam], NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(tempplayer)))
		
				PRINTLN("[Cutscenes] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - iTeam: ", iTeam, " - ASSIGNING PLAYER SLOT - piCutscenePlayerIDs[", iTeam, "][", iPlayer, "] = ", NATIVE_TO_INT(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer]))
			ELSE
				PRINTLN("[Cutscenes] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - iTeam: ", iTeam, " - Failed to fill slot - piCutscenePlayerIDs[", iTeam, "][", iPlayer, "] = ", NATIVE_TO_INT(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer]))
			ENDIF
		ENDFOR

	ELSE
		PRINTLN("[Cutscenes] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - iTeam: ", iTeam, " - Cutscene set not to use players")
		
		//Set initial player to invalid index
		MC_serverBD.piCutscenePlayerIDs[iteam][0] = INVALID_PLAYER_INDEX()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		
		FOR iplayer = 0 TO FMMC_MAX_CUTSCENE_PLAYERS - 1
			IF MC_serverBD.piCutscenePlayerIDs[iteam][iplayer] != INVALID_PLAYER_INDEX()
				PRINTLN("[Cutscenes] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - iTeam: ", iTeam, " - MC_serverBD.piCutscenePlayerIDs[", iteam, "][", iplayer, "] = ", NATIVE_TO_INT(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer]), " (", GET_PLAYER_NAME(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer]), ")")
			ELSE
				PRINTLN("[Cutscenes] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - iTeam: ", iTeam, " - MC_serverBD.piCutscenePlayerIDs[", iteam, "][", iplayer, "] = ", NATIVE_TO_INT(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer]))
			ENDIF
		ENDFOR
	
	#ENDIF
	
	SET_BIT(MC_serverBD.iServerBitSet, SBBOOL_CUTSCENE_TEAM0_PLAYERS_SELECTED + iTeam)

ENDPROC

PROC ASSIGN_MOCAP_CUTSCENE_TO_HAVE_EXTENDED_STREAMING_RANGES(FLOAT &fStreamOut, FLOAT &fStreamIn)

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_PlayEndCutsceneForAllPlayers) 	
		fStreamIn = 9999
		fStreamOut = 12000
	ENDIF

	PRINTLN("[LM][ASSIGN_MOCAP_CUTSCENE_TO_HAVE_EXTENDED_STREAMING_RANGES] fStreamOut: ", fStreamOut, " fStreamIn: ", fStreamIn)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cutscene Special Case - 1/Elevator  --------------------------------------------------------------------------------------------------------------------------
// ##### Description: Unique and special case logic that handles Lift/Elevator during a cutscene..									 							 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC MOVE_SCRIPTED_CUTSCENE_LIFT(FLOAT fChange, VECTOR vLift)
	
	SET_ENTITY_COORDS(objCutsceneLift_Body, <<vLift.x, vLift.y, vLift.z + fChange>>)
	
	IF DOES_ENTITY_EXIST(objCutsceneLift_DoorL)
		VECTOR vDoorL = GET_ENTITY_COORDS(objCutsceneLift_DoorL)
		SET_ENTITY_COORDS(objCutsceneLift_DoorL, <<vDoorL.x, vDoorL.y, vDoorL.z + fChange>>)
	ENDIF
	
	IF DOES_ENTITY_EXIST(objCutsceneLift_DoorR)
		VECTOR vDoorR = GET_ENTITY_COORDS(objCutsceneLift_DoorR)
		SET_ENTITY_COORDS(objCutsceneLift_DoorR, <<vDoorR.x, vDoorR.y, vDoorR.z + fChange>>)
	ENDIF
	
ENDPROC

PROC INIT_SCRIPTED_CUTSCENE_LIFT(INT iCutsceneToUse)
	
	eCSLift = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].eCSLiftData
	
	IF eCSLift = eCSLift_None
		EXIT
	ENDIF
	
	VECTOR vLift = GET_SCRIPTED_CUTSCENE_LIFT_COORDS(eCSLift)
	
	fCutsceneLift_OriginZ = vLift.z
	
	MODEL_NAMES mnBase, mnDoorL, mnDoorR, mnDoorLO, mnDoorRO, mnExtras1
	GET_SCRIPTED_CUTSCENE_LIFT_MODELS(eCSLift, mnBase, mnDoorL, mnDoorR, mnDoorLO, mnDoorRO, mnExtras1)
	
	PRINTLN("[Cutscenes][CSLift] INIT_SCRIPTED_CUTSCENE_LIFT - Creating lift ",GET_SCRIPTED_CUTSCENE_LIFT_DEBUG_NAME(eCSLift)," model hides at vLift ",vLift)
	IF IS_MODEL_VALID(mnBase)
		CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(vLift, 0.5, mnBase, TRUE)
	ENDIF
	IF IS_MODEL_VALID(mnDoorL)
		CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(vLift, 0.5, mnDoorL, TRUE)
	ENDIF
	IF IS_MODEL_VALID(mnDoorR)
		CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(vLift, 0.5, mnDoorR, TRUE)
	ENDIF
	IF IS_MODEL_VALID(mnDoorLO)
		CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(vLift, 0.5, mnDoorLO, TRUE)
	ENDIF
	IF IS_MODEL_VALID(mnDoorRO)
		CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(vLift, 0.5, mnDoorRO, TRUE)
	ENDIF
	IF IS_MODEL_VALID(mnExtras1)
		CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(vLift, 0.5, mnExtras1, TRUE)
	ENDIF	
	
	FLOAT fLiftBodyHeading
	GET_SCRIPTED_CUTSCENE_LIFT_HEADINGS(eCSLift, fLiftBodyHeading, fCutsceneLift_DoorLOpenHeading, fCutsceneLift_DoorROpenHeading, fCutsceneLift_DoorsClosedHeading)
	
	PRINTLN("[Cutscenes][CSLift] INIT_SCRIPTED_CUTSCENE_LIFT - Creating lift ",GET_SCRIPTED_CUTSCENE_LIFT_DEBUG_NAME(eCSLift)," objects at ",vLift)
	
	IF IS_MODEL_VALID(mnBase)
		objCutsceneLift_Body = CREATE_OBJECT(mnBase, vLift, FALSE, FALSE)
		SET_ENTITY_HEADING(objCutsceneLift_Body, fLiftBodyHeading)
		FREEZE_ENTITY_POSITION(objCutsceneLift_Body, TRUE)
		SET_ENTITY_NO_COLLISION_ENTITY(LocalPlayerPed, objCutsceneLift_Body, FALSE)
	ENDIF
	
	IF IS_MODEL_VALID(mnDoorL)
		objCutsceneLift_DoorL = CREATE_OBJECT(mnDoorL, vLift, FALSE, FALSE)
		SET_ENTITY_HEADING(objCutsceneLift_DoorL, fCutsceneLift_DoorLOpenHeading)
		FREEZE_ENTITY_POSITION(objCutsceneLift_DoorL, TRUE)
	ENDIF
	
	IF IS_MODEL_VALID(mnDoorR)
		objCutsceneLift_DoorR = CREATE_OBJECT(mnDoorR, vLift, FALSE, FALSE)
		SET_ENTITY_HEADING(objCutsceneLift_DoorR, fCutsceneLift_DoorROpenHeading)
		FREEZE_ENTITY_POSITION(objCutsceneLift_DoorR, TRUE)
	ENDIF
	
	IF IS_MODEL_VALID(mnDoorLO)
		objCutsceneLift_DoorLO = CREATE_OBJECT(mnDoorLO, vLift, FALSE, FALSE)
		SET_ENTITY_HEADING(objCutsceneLift_DoorLO, fCutsceneLift_DoorLOpenHeading)
		FREEZE_ENTITY_POSITION(objCutsceneLift_DoorLO, TRUE)
	ENDIF
	
	IF IS_MODEL_VALID(mnDoorRO)
		objCutsceneLift_DoorRO = CREATE_OBJECT(mnDoorRO, vLift, FALSE, FALSE)
		SET_ENTITY_HEADING(objCutsceneLift_DoorRO, fCutsceneLift_DoorROpenHeading)
		FREEZE_ENTITY_POSITION(objCutsceneLift_DoorRO, TRUE)
	ENDIF
	
	IF IS_MODEL_VALID(mnExtras1)
		objCutsceneLift_Body_Extras1 = CREATE_OBJECT(mnExtras1, vLift, FALSE, FALSE)
		SET_ENTITY_HEADING(objCutsceneLift_Body_Extras1, fLiftBodyHeading)
		FREEZE_ENTITY_POSITION(objCutsceneLift_Body_Extras1, TRUE)	
	ENDIF
	
	IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fLiftZOffset_Start != 0
		// The lift will start off the ground, so close the doors and raise it up
		PRINTLN("[Cutscenes][CSLift] INIT_SCRIPTED_CUTSCENE_LIFT - The lift starts ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fLiftZOffset_Start," off the floor, close doors & move up")
	
		IF DOES_ENTITY_EXIST(objCutsceneLift_DoorL)
			SET_ENTITY_HEADING(objCutsceneLift_DoorL, fCutsceneLift_DoorsClosedHeading)
		ENDIF
		
		IF DOES_ENTITY_EXIST(objCutsceneLift_DoorLO)
			SET_ENTITY_HEADING(objCutsceneLift_DoorLO, fCutsceneLift_DoorsClosedHeading)
		ENDIF
		
		IF DOES_ENTITY_EXIST(objCutsceneLift_DoorR)
			SET_ENTITY_HEADING(objCutsceneLift_DoorR, fCutsceneLift_DoorsClosedHeading)
		ENDIF
		
		IF DOES_ENTITY_EXIST(objCutsceneLift_DoorRO)
			SET_ENTITY_HEADING(objCutsceneLift_DoorRO, fCutsceneLift_DoorsClosedHeading)
		ENDIF
		
		MOVE_SCRIPTED_CUTSCENE_LIFT(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fLiftZOffset_Start, vLift)
	ELSE
		IF DOES_ENTITY_EXIST(objCutsceneLift_Body)
			#IF IS_DEBUG_BUILD
			VECTOR vOldLift = GET_ENTITY_COORDS(objCutsceneLift_Body)
			PRINTLN("[Cutscenes][CSLift] INIT_SCRIPTED_CUTSCENE_LIFT - Moving lift back to vLift ",vLift,", coords before this warp = ",vOldLift)
			#ENDIF
			SET_ENTITY_COORDS(objCutsceneLift_Body, vLift)
		ENDIF
	ENDIF
	
ENDPROC

PROC SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLiftMovementState eNewState)
	IF eNewState != eLiftState
		PRINTLN("[Cutscenes][CSLift] SET_SCRIPTED_CUTSCENE_LIFT_STATE - Moving from lift state ",GET_SCRIPTED_CUTSCENE_LIFT_STATE_STRING(eLiftState)," to ",GET_SCRIPTED_CUTSCENE_LIFT_STATE_STRING(eNewState))
		eLiftState = eNewState
	ENDIF
ENDPROC

PROC MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT(INT iCutsceneToUse, PED_INDEX &pedPlayers[])
	
	VECTOR vLiftRot = GET_ENTITY_ROTATION(objCutsceneLift_Body)
	VECTOR vOffset, vRotOffset, vPedCoords, vPedRot, vEndPos
	FLOAT fEndHeading
	FLOAT fHeadingOffset
	INT i
	
	#IF IS_DEBUG_BUILD
	VECTOR vLift = GET_ENTITY_COORDS(objCutsceneLift_Body)
	PRINTLN("[Cutscenes][CSLift] MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT - Called w lift at ",vLift,", vLiftRot ",vLiftRot)
	#ENDIF
	
	INTERIOR_DATA_STRUCT structInteriorData = GET_INTERIOR_DATA(GET_SCRIPTED_CUTSCENE_LIFT_INTERIOR(eCSLift))
	INTERIOR_INSTANCE_INDEX iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
	
	FOR i = 0 to (FMMC_MAX_CUTSCENE_PLAYERS - 1)
		IF DOES_ENTITY_EXIST(pedPlayers[i])
		AND NOT IS_PED_INJURED(pedPlayers[i])

			CLEAR_PED_TASKS_IMMEDIATELY(pedPlayers[i])
			
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerStartPos[i])
				vPedCoords = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerStartPos[i]
				vPedRot = <<0.0, 0.0, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerStartHeading[i]>>
				
			ELIF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fLiftZOffset_Start != 0 // If the lift is coming in to land:			
				vEndPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerEndPos[i]
				fEndHeading = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerEndHeading[i]
				PRINTLN("[Cutscenes][CSLift] MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT - Ped ",i," has vEndPos ",vEndPos,", fEndHeading ",fEndHeading)
				
				vPedCoords = vEndPos + <<0, 0, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fLiftZOffset_Start + 1>> // +1 in z as the end pos is on the ground
				PRINTLN("[Cutscenes][CSLift] MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT - Ped ",i," adjust end pos z by fLiftZOffset_Start ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fLiftZOffset_Start," to get vPedCoords ",vPedCoords," & call SET_ENTITY_COORDS + SET_ENTITY_HEADING")
				
				SET_ENTITY_COORDS(pedPlayers[i], vPedCoords)
				SET_ENTITY_HEADING(pedPlayers[i], fEndHeading)
				
				RETAIN_ENTITY_IN_INTERIOR(pedPlayers[i], iInteriorIndex)
			ELSE
				vPedCoords = GET_ENTITY_COORDS(pedPlayers[i])
				PRINTLN("[Cutscenes][CSLift] MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT - Ped ",i," not warping, grab vPedCoords as ",vPedCoords)
			ENDIF
			
			vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(objCutsceneLift_Body, vPedCoords)
			
			vOffset += GET_SCRIPTED_CUTSCENE_LIFT_COORDS_PED_ATTACHMENT_OFFSET(eCSLift)
			
			PRINTLN("[Cutscenes][CSLift] MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT - Ped ",i," offset from lift = ",vOffset)
			
			vPedRot = GET_ENTITY_ROTATION(pedPlayers[i])
			vRotOffset = vPedRot - vLiftRot
			PRINTLN("[Cutscenes][CSLift] MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT - Ped ",i," rotation ",vPedRot," -> vRotOffset ",vRotOffset)
			
			PRINTLN("[Cutscenes][CSLift] MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT - Ped ",i," call ATTACH_ENTITY_TO_ENTITY")
			
			ATTACH_ENTITY_TO_ENTITY(pedPlayers[i], objCutsceneLift_Body, -1, vOffset, vRotOffset)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedPlayers[i])
		ELSE
			IF i = 0
				PRINTLN("[Cutscenes][CSLift] MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT - Ped 0 no good? DOES_ENTITY_EXIST = ",DOES_ENTITY_EXIST(pedPlayers[i]))
			ENDIF
		ENDIF
	ENDFOR
		
	IF DOES_ENTITY_EXIST(objCutsceneLift_Body_Extras1)
		PRINTLN("[Cutscenes][CSLift] MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT - Attaching Extra Component 1 to Lift Body.")
		vOffset = GET_SCRIPTED_CUTSCENE_LIFT_COORDS_ATTACHMENT_OFFSET_FOR_EXTRA_COMPONENT_1(eCSLift)
		fHeadingOffset = GET_SCRIPTED_CUTSCENE_LIFT_HEADING_ATTACHMENT_OFFSET_FOR_EXTRA_COMPONENT_1(eCSLift)
		
		ATTACH_ENTITY_TO_ENTITY(objCutsceneLift_Body_Extras1, objCutsceneLift_Body, -1, vOffset, <<0.0, 0.0, fHeadingOffset>>)
	ENDIF
			
	SET_BIT(iLocalBoolCheck35, LBOOL35_ATTACHED_CLONES_FOR_LIFT_SCENE)
ENDPROC

PROC PROCESS_SCRIPTED_CUTSCENE_LIFT_AUDIO__MOVE_LOOP(INT iCutsceneToUse, BOOL bGoingUp)
	
	UNUSED_PARAMETER(iCutsceneToUse)
	
	SWITCH eCSLift		
		CASE eCSLift_ServerFarm
		CASE eCSLift_IAABase
			IF NOT bGoingUp
				IF iElevatorMovementSound = -1
					PRINTLN("[Cutscenes][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - iElevatorMovementSound = -1 The lift has started moving (Start Sound). elevator_descend_loop")
					iElevatorMovementSound = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iElevatorMovementSound, "elevator_descend_loop", "dlc_xm_IAA_Finale_sounds")
				ENDIF
			ENDIF
		BREAK
		
		CASE eCSLift_ConstructionSkyscraper_SouthA_Bottom	
		CASE eCSLift_ConstructionSkyscraper_SouthA_Mid		
		CASE eCSLift_ConstructionSkyscraper_SouthB_Bottom	
		CASE eCSLift_ConstructionSkyscraper_SouthB_Mid 		
		CASE eCSLift_ConstructionSkyscraper_NorthA_Bottom	
		CASE eCSLift_ConstructionSkyscraper_NorthA_Mid		
		CASE eCSLift_ConstructionSkyscraper_NorthA_Top		
		CASE eCSLift_ConstructionSkyscraper_NorthB_Bottom	
		CASE eCSLift_ConstructionSkyscraper_NorthB_Mid		
		CASE eCSLift_ConstructionSkyscraper_NorthB_Top	
			IF iElevatorMovementSound = -1
				PRINTLN("[Cutscenes][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - iElevatorMovementSound = -1 The lift has started moving (Start Sound). Motor_01")
				START_AUDIO_SCENE("ULP5_Deal_Breaker_Ride_Lift_Scene")
				iElevatorMovementSound = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(iElevatorMovementSound, "Motor_01", "FREIGHT_ELEVATOR_SOUNDS")
			ENDIF
		BREAK
	ENDSWITCH	
	
ENDPROC

PROC PROCESS_SCRIPTED_CUTSCENE_ATTACH_CLONES_EARLY(INT iCutsceneToUse, PED_INDEX &pedPlayers[])
	
	IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iLiftMovementOnShot > 0
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(objCutsceneLift_Body)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_Scripted_UsePlayerClones)
		EXIT
	ENDIF
	
	PRINTLN("[Cutscenes][CSLift] PROCESS_SCRIPTED_CUTSCENE_ATTACH_CLONES_EARLY - Attaching clones early, and we need to perform the transition between players and clones instantaenously")
	
	MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT(iCutsceneToUse, pedPlayers)		
	
	SET_BIT(iLocalBoolCheck35, LBOOL35_ATTACHED_CLONES_FOR_LIFT_SCENE)
	
ENDPROC

PROC PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING(INT iCutsceneToUse, INT iCurrentShot, INT iPlayerCutId, PED_INDEX &pedPlayers[])
	
	IF NOT DOES_ENTITY_EXIST(objCutsceneLift_Body)
		EXIT
	ENDIF
	
	VECTOR vLift = GET_ENTITY_COORDS(objCutsceneLift_Body)
	FLOAT fNewZ, fChangeZ
	
	FLOAT fStartZOffset = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fLiftZOffset_Start
	FLOAT fEndZOffset = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fLiftZOffset_End
	FLOAT fStartZ = fCutsceneLift_OriginZ + fStartZOffset
	FLOAT fEndZ = fCutsceneLift_OriginZ + fEndZOffset
	
	INT iMovementOnShot = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iLiftMovementOnShot
	FLOAT fSpeed = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fLiftSpeed
	
	FLOAT fDoorSpeed = 20
	FLOAT fDoorLHeading
	FLOAT fDoorRHeading
	FLOAT fDoorChange
	
	IF DOES_ENTITY_EXIST(objCutsceneLift_DoorL)
		fDoorLHeading = GET_ENTITY_HEADING(objCutsceneLift_DoorL)
	ENDIF
	
	IF DOES_ENTITY_EXIST(objCutsceneLift_DoorR)
		fDoorRHeading = GET_ENTITY_HEADING(objCutsceneLift_DoorR)
	ENDIF

	IF iSoundIDCamBackground = -1
		iSoundIDCamBackground = GET_SOUND_ID()
	ENDIF
	IF iSoundPan = -1
		iSoundPan = GET_SOUND_ID()
	ENDIF
	IF iSoundZoom = -1
		iSoundZoom = GET_SOUND_ID()
	ENDIF
	IF iCamSound = -1
		iCamSound = GET_SOUND_ID()
	ENDIF	
	SWITCH eLiftState
		CASE eLMS_WaitForStart
			IF iMovementOnShot >= 0
				IF iCurrentShot >= iMovementOnShot
					SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_AttachClones)
				ELSE
					PRINTLN("[Cutscenes][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - Waiting for shot ",iMovementOnShot," before moving, iCurrentShot = ",iCurrentShot)
				ENDIF
			ELSE
				PRINTLN("[Cutscenes][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - No movement shot set, finish")
				SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_Complete)
			ENDIF
		BREAK
		
		CASE eLMS_AttachClones
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_Scripted_UsePlayerClones)
				IF NOT IS_BIT_SET(iLocalBoolCheck35, LBOOL35_ATTACHED_CLONES_FOR_LIFT_SCENE)
					MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT(iCutsceneToUse, pedPlayers)
				ENDIF
				
				IF fStartZOffset != 0
					// Move the player ped now to remove the long load time at the end of this cutscene
					PRINTLN("[Cutscenes][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - Warping the player to ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerEndPos[iPlayerCutId]," & freeze them there, to get ready for cutscene end")
					
					NET_WARP_TO_COORD(
						g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerEndPos[iPlayerCutId],
						g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerEndHeading[iPlayerCutId],
						FALSE, FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE)
					
					FREEZE_ENTITY_POSITION(LocalPlayerPed, TRUE)
				ENDIF
			ENDIF
			
			IF iSoundIDCamBackground > -1
				PRINTLN("[Cutscenes][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - iSoundIDCamBackground > -1 and we are moving the Lift. Stopping CCTV sound.")
				STOP_SOUND(iSoundIDCamBackground)
			ENDIF					
			IF iSoundPan > -1
				PRINTLN("[Cutscenes][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - iSoundPan > -1 and we are moving the Lift. Stopping CCTV sound.")
				STOP_SOUND(iSoundPan)
			ENDIF
			IF iSoundZoom > -1
				PRINTLN("[Cutscenes][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - iSoundZoom > -1 and we are moving the Lift. Stopping CCTV sound.")
				STOP_SOUND(iSoundZoom)
			ENDIF
			
			IF fStartZOffset = 0
				SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_CloseDoors)
			ELSE
				IF fEndZOffset < fStartZOffset
					SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_MoveLiftDown)										
				ELSE
					SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_MoveLiftUp)
				ENDIF
			ENDIF
		BREAK
		
		CASE eLMS_CloseDoors
			IF fDoorLHeading <= fCutsceneLift_DoorsClosedHeading
			AND fDoorRHeading >= fCutsceneLift_DoorsClosedHeading
				IF fEndZOffset < fStartZOffset
					SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_MoveLiftDown)
				ELSE
					SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_MoveLiftUp)
				ENDIF
			ELSE
				fDoorChange = (fDoorSpeed * fLastFrameTime)
				
				fDoorLHeading -= fDoorChange
				fDoorLHeading = FMAX(fDoorLHeading, fCutsceneLift_DoorsClosedHeading)
				
				fDoorRHeading += fDoorChange
				fDoorRHeading = FMIN(fDoorRHeading, fCutsceneLift_DoorsClosedHeading)
				
				PRINTLN("[Cutscenes][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - Set new fDoorLHeading ",fDoorLHeading,", fDoorRHeading ",fDoorRHeading)
				IF DOES_ENTITY_EXIST(objCutsceneLift_DoorL)
					SET_ENTITY_HEADING(objCutsceneLift_DoorL, fDoorLHeading)
				ENDIF
				IF DOES_ENTITY_EXIST(objCutsceneLift_DoorLO)
					SET_ENTITY_HEADING(objCutsceneLift_DoorLO, fDoorLHeading)
				ENDIF
				IF DOES_ENTITY_EXIST(objCutsceneLift_DoorR)
					SET_ENTITY_HEADING(objCutsceneLift_DoorR, fDoorRHeading)
				ENDIF
				IF DOES_ENTITY_EXIST(objCutsceneLift_DoorRO)
					SET_ENTITY_HEADING(objCutsceneLift_DoorRO, fDoorRHeading)
				ENDIF
			ENDIF
		BREAK
		
		CASE eLMS_MoveLiftUp
			PROCESS_SCRIPTED_CUTSCENE_LIFT_AUDIO__MOVE_LOOP(iCutsceneToUse, TRUE)
			
			IF vLift.z >= fEndZ
				PRINTLN("[Cutscenes][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - Lift z ",vLift.z," reached target end z ",fEndZ)
				
				IF fEndZOffset = 0
					SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_OpenDoors)
				ELSE
					SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_Complete)
				ENDIF
			ELSE
				fChangeZ = (fSpeed * fLastFrameTime)
				fNewZ = CLAMP(vLift.z + fChangeZ, fStartZ, fEndZ) // Make sure we don't go over the top
				fChangeZ = fNewZ - vLift.z
				
				PRINTLN("[Cutscenes][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - Moving lift up by ",fChangeZ," to new z ",fNewZ)
				MOVE_SCRIPTED_CUTSCENE_LIFT(fChangeZ, vLift)
			ENDIF
		BREAK
		
		CASE eLMS_MoveLiftDown
			PROCESS_SCRIPTED_CUTSCENE_LIFT_AUDIO__MOVE_LOOP(iCutsceneToUse, FALSE)
			
			IF vLift.z <= fEndZ
				PRINTLN("[Cutscenes][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - Lift z ",vLift.z," reached target end z ",fEndZ)
				
				IF fEndZOffset = 0
					SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_OpenDoors)
				ELSE
					SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_Complete)
				ENDIF
			ELSE
				fChangeZ = -(fSpeed * fLastFrameTime)
				fNewZ = CLAMP(vLift.z + fChangeZ, fEndZ, fStartZ) // Make sure we don't go out the bottom
				fChangeZ = fNewZ - vLift.z
				
				PRINTLN("[Cutscenes][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - Moving lift down by ",fChangeZ," to new z ",fNewZ)
				MOVE_SCRIPTED_CUTSCENE_LIFT(fChangeZ, vLift)
			ENDIF
		BREAK
		
		CASE eLMS_OpenDoors
			STOP_SCRIPTED_CUTSCENE_LIFT_AUDIO__MOVE_LOOP()
			
			IF fDoorLHeading >= fCutsceneLift_DoorLOpenHeading
			AND fDoorRHeading <= fCutsceneLift_DoorROpenHeading
				SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_Complete)
			ELSE
				fDoorChange = (fDoorSpeed * fLastFrameTime)
				
				fDoorLHeading += fDoorChange
				fDoorLHeading = FMIN(fDoorLHeading, fCutsceneLift_DoorLOpenHeading)
				
				fDoorRHeading -= fDoorChange
				fDoorRHeading = FMAX(fDoorRHeading, fCutsceneLift_DoorROpenHeading)
				
				PRINTLN("[Cutscenes][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - Set new fDoorLHeading ",fDoorLHeading,", fDoorRHeading ",fDoorRHeading)			
				IF DOES_ENTITY_EXIST(objCutsceneLift_DoorL)
					SET_ENTITY_HEADING(objCutsceneLift_DoorL, fDoorLHeading)
				ENDIF
				IF DOES_ENTITY_EXIST(objCutsceneLift_DoorLO)
					SET_ENTITY_HEADING(objCutsceneLift_DoorLO, fDoorLHeading)
				ENDIF
				IF DOES_ENTITY_EXIST(objCutsceneLift_DoorR)
					SET_ENTITY_HEADING(objCutsceneLift_DoorR, fDoorRHeading)
				ENDIF
				IF DOES_ENTITY_EXIST(objCutsceneLift_DoorRO)
					SET_ENTITY_HEADING(objCutsceneLift_DoorRO, fDoorRHeading)
				ENDIF
			ENDIF
		BREAK
		
		CASE eLMS_Complete
			STOP_SCRIPTED_CUTSCENE_LIFT_AUDIO__MOVE_LOOP()
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL SHOULD_SPECTATOR_FADE_OUT_FOR_CUTSCENE(INT iCutsceneToUse)

	IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)		
	AND (DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer) 
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)	
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
	OR IS_BIT_SET(iLocalBoolCheck3, LBOOL3_LATE_SPECTATE)
	OR IS_BIT_SET(iLocalBoolCheck3, LBOOL3_WAIT_END_SPECTATE)
	OR IS_PED_INJURED(localPlayerPed))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cutscene Special Case - External Cutscenes  ---------------------------------------------------------------------------------------------------------------------
// ##### Description: External Cutscene logic 									 							 ---------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC HIDE_BUNKER_DOOR_COLLISION_IPLS_FOR_ACTIVITY_SESSION(BOOL bHide)
	INT iBunker
	OBJECT_INDEX objBunkerDoor
	TRANSFORM_STRUCT sBunkerTransform
	
	FOR iBunker = SIMPLE_INTERIOR_BUNKER_1 TO SIMPLE_INTERIOR_BUNKER_12 
		sBunkerTransform = BUNKER_CUTSCENE_GET_DELIVERY_SCENE_TRANSFORM(INT_TO_ENUM(SIMPLE_INTERIORS, iBunker))
		objBunkerDoor =	GET_CLOSEST_OBJECT_OF_TYPE(sBunkerTransform.Position, 1.0, GET_BUNKER_DOOR_IPL_MODEL(INT_TO_ENUM(SIMPLE_INTERIORS, iBunker)), FALSE, FALSE, FALSE)
		
		IF DOES_ENTITY_EXIST(objBunkerDoor)
			IF bHide
				IF IS_ENTITY_VISIBLE(objBunkerDoor)
					CDEBUG1LN(DEBUG_SMPL_INTERIOR, "HIDE_BUNKER_DOOR_COLLISION_IPLS_FOR_ACTIVITY_SESSION - *HIDE BUNKER DOOR IPL, ID: ", iBunker, " Name: ", GET_SIMPLE_INTERIOR_DEBUG_NAME(INT_TO_ENUM(SIMPLE_INTERIORS, iBunker)))
					SET_ENTITY_VISIBLE(objBunkerDoor, FALSE)
					SET_ENTITY_COLLISION(objBunkerDoor, FALSE)
				ENDIF
			ELSE
				IF NOT IS_ENTITY_VISIBLE(objBunkerDoor)
					SET_ENTITY_VISIBLE(objBunkerDoor, TRUE)
					SET_ENTITY_COLLISION(objBunkerDoor, FALSE)
					CDEBUG1LN(DEBUG_SMPL_INTERIOR, "HIDE_BUNKER_DOOR_COLLISION_IPLS_FOR_ACTIVITY_SESSION - *SHOW BUNKER DOOR IPL, ID: ", iBunker, " Name: ", GET_SIMPLE_INTERIOR_DEBUG_NAME(INT_TO_ENUM(SIMPLE_INTERIORS, iBunker)))
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL HAS_EXTERNAL_CUTSCENE_SEQUENCE_BUNKER_ENTRY_FINISHED(INT iCutscene)
	
	SIMPLE_INTERIORS simpInterior
	
	SWITCH g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].eExternalCutsceneSequence
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_1
			simpInterior = SIMPLE_INTERIOR_BUNKER_1
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_2
			simpInterior = SIMPLE_INTERIOR_BUNKER_2
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_3
			simpInterior = SIMPLE_INTERIOR_BUNKER_3
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_4
			simpInterior = SIMPLE_INTERIOR_BUNKER_4
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_5
			simpInterior = SIMPLE_INTERIOR_BUNKER_5
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_6
			simpInterior = SIMPLE_INTERIOR_BUNKER_6
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_7
			simpInterior = SIMPLE_INTERIOR_BUNKER_7
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_9
			simpInterior = SIMPLE_INTERIOR_BUNKER_9
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_10
			simpInterior = SIMPLE_INTERIOR_BUNKER_10
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_11
			simpInterior = SIMPLE_INTERIOR_BUNKER_11
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_12
			simpInterior = SIMPLE_INTERIOR_BUNKER_12
		BREAK
	ENDSWITCH
	
	IF simpInterior != simpInteriorCache
		simpInteriorCache = simpInterior
		PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Bunker] - HAS_EXTERNAL_CUTSCENE_SEQUENCE_BUNKER_ENTRY_FINISHED Caching simpInteriorCache to: ", ENUM_TO_INT(simpInteriorCache))
	ENDIF
	
	SWITCH eExternalScriptedCutsceneSequenceState
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_IDLE				
			
			NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_FREEZE_POSITION)
			
			_SETUP_BUNKER_DETAILS_VIA_DEFAULT_OFFSETS(simpInterior, entryAnimExtCut)
			
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
					FREEZE_ENTITY_POSITION(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), TRUE)
				ENDIF
				
				PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Bunker] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP Starting Load Cutscene for in Veh.")
				SET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED(BE_VEHICLE)
			ELSE
				PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Bunker] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP Starting Load Cutscene for On Foot.")
				SET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED(BE_FOOT)
			ENDIF
			SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_LOAD, eExternalScriptedCutsceneSequenceState)		
		BREAK
		
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_LOAD			
			
			//IF NOT BUNKER_CUTSCENE_CREATE_ENTRY_DOOR(simpInterior, entryAnimExtCut.objectArray[BC_PROP_DOOR], entryAnimExtCut.objectArray[BC_PROP_DOOR_FRAME])
			//	PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Bunker] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP Creating Doors.")
			//	RETURN FALSE
			//ELSE
			//	PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Bunker] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP Created Doors.")
			//ENDIF
			
			IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			OR NOT IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
				IF GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = BE_VEHICLE
					CDEBUG1LN(DEBUG_PROPERTY, "[INTERIOR_BUNKER] - HAVE_BUNKER_CUSTOM_CUTSCENE_ASSETS_LOADED - Entry is Vehicle but not in a vehicle , default to foot entry. (from Mission Controller)")
					SET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED(BE_FOOT)
				ENDIF
			ENDIF
			
			IF HAVE_BUNKER_CUSTOM_CUTSCENE_ASSETS_LOADED(simpInterior, entryAnimExtCut)
				PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Bunker] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP Loaded Cutscene.")
				SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP, eExternalScriptedCutsceneSequenceState)
			ELSE
				PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Bunker] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP Waiting to Load Cutscene.")
			ENDIF
		BREAK
		
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP
			IF SETUP_BUNKER_CUSTOM_CUTSCENE_SCENES(simpInterior, entryAnimExtCut)
				PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Bunker] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP Starting Cutscene.")
			
				TRANSFORM_STRUCT sSceneTransform
				sSceneTransform = BUNKER_CUTSCENE_GET_DELIVERY_SCENE_TRANSFORM(simpInterior)
				entryAnimExtCut.syncScenePos = sSceneTransform.Position
				entryAnimExtCut.syncSceneRot = sSceneTransform.Rotation

				SIMPLE_CUTSCENE_START(g_SimpleInteriorData.sCustomCutsceneExt, SCS_PREVENT_FROZEN_CHANGES_START|SCS_PREVENT_VISIBLITY_CHANGES_START)
				HIDE_BUNKER_DOOR_COLLISION_IPLS_FOR_ACTIVITY_SESSION(TRUE)
					
				IF NOT IS_PED_INJURED(LocalPlayerPed)
					PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Bunker] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP Setting ped as not visible in cutscene.")								
					SET_ENTITY_VISIBLE(LocalPlayerPed, FALSE)
				ENDIF
					
				IF IS_FAKE_MULTIPLAYER_MODE_SET()
					IF DOES_ENTITY_EXIST(oiBunkerEntryFrame)
						PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Bunker] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP Setting Door Entry Frame as not Visible in Cutscene.")						
						SET_ENTITY_VISIBLE(oiBunkerEntryFrame, FALSE)
					ENDIF
					IF DOES_ENTITY_EXIST(oibunkerEntryDoor)
						PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Bunker] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP Setting Door as not Visible in Cutscene.")						
						SET_ENTITY_VISIBLE(oibunkerEntryDoor, FALSE)
					ENDIF
				ENDIF
				
				g_SimpleInteriorData.sCustomCutsceneExt.sScenes[1].iFadeOutTime = 500
				
				SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN, eExternalScriptedCutsceneSequenceState)
			ELSE
				PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Bunker] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP Waiting for scenes to be set up.")
			ENDIF
		BREAK
		
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN
			SIMPLE_CUTSCENE_MAINTAIN(g_SimpleInteriorData.sCustomCutsceneExt)
			
			IF HAS_BUNKER_CUSTOM_CUTSCENE_FINISHED(simpInterior, entryAnimExtCut)
				IF NOT IS_SCREEN_FADING_OUT()
					PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Bunker] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP Doing screen fadeout manually.")
					DO_SCREEN_FADE_OUT(500)
				ENDIF
				
				IF IS_SCREEN_FADED_OUT()
					PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Bunker] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP Finished Cutscene - Moving on.")
					SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_CLEANUP, eExternalScriptedCutsceneSequenceState)
					HIDE_BUNKER_DOOR_COLLISION_IPLS_FOR_ACTIVITY_SESSION(FALSE)
				ELSE
					PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Bunker] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP Finished Cutscene - Waiting for Fadeout")
				ENDIF
			ELSE
				PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Bunker] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN Waiting for HAS_BUNKER_CUSTOM_CUTSCENE_FINISHED")
			ENDIF
		BREAK
		
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_CLEANUP
			PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Bunker] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_CLEANUP calling cleanup functions.")
			
			CLEANUP_BUNKER_CUSTOM_CUTSCENE(simpInterior, entryAnimExtCut)
						
			IF NOT IS_PED_INJURED(LocalPlayerPed)
				PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Bunker] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_CLEANUP calling SET_ENTITY_VISIBLE true for localplayerped.")
				SET_ENTITY_VISIBLE(LocalPlayerPed, TRUE)
			ENDIF
				
			IF IS_FAKE_MULTIPLAYER_MODE_SET()
				IF DOES_ENTITY_EXIST(oiBunkerEntryFrame)
					SET_ENTITY_VISIBLE(oiBunkerEntryFrame, TRUE)
				ENDIF				
				IF DOES_ENTITY_EXIST(oibunkerEntryDoor)
					SET_ENTITY_VISIBLE(oibunkerEntryDoor, TRUE)
				ENDIF
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)				
			AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
				FREEZE_ENTITY_POSITION(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), FALSE)			
			ENDIF
			
			SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_IDLE, eExternalScriptedCutsceneSequenceState)
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_CASINO_ENTRY_FINISHED()
	
	IF eExternalScriptedCutsceneSequenceState > EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_IDLE
		INT iTeam = iScriptedCutsceneTeam
		IF iScriptedCutsceneTeam = -1
			iTeam = MC_PlayerBD[iLocalPart].iTeam
		ENDIF
		PED_INDEX OriginalPlayerPed
		VEHICLE_INDEX viOriginalVehicle
		INT i = 0
		FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
			IF MC_serverBD.piCutscenePlayerIDs[iTeam][i] = INVALID_PLAYER_INDEX()
				RELOOP
			ENDIF
		
			OriginalPlayerPed = GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[iTeam][i])			
			
			IF IS_PED_INJURED(OriginalPlayerPed)
				RELOOP
			ENDIF
			
			IF NOT IS_PED_IN_ANY_VEHICLE(OriginalPlayerPed)
				IF NOT IS_BIT_SET(iExternalCutsceneBitset, ciREPOSITION_ORIGINAL_ENTITIES_PLAYER_0 + i)
				AND OriginalPlayerPed = LocalPlayerPed
					FLOAT fHead = 326.3484
					VECTOR vPos = <<914.3851, -25.5898, 77.7641>>
					VECTOR vNewPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHead, <<2.0 * i, 0.0, 0.0>>)
					SET_ENTITY_COORDS(OriginalPlayerPed, vNewPos)
					SET_ENTITY_HEADING(OriginalPlayerPed, fHead)
					
					SET_BIT(iExternalCutsceneBitset, ciREPOSITION_ORIGINAL_ENTITIES_PLAYER_0 + i)
					PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - Moving player: ", i)
				ENDIF
				
				IF OriginalPlayerPed != LocalPlayerPed
					PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - Setting player: ", i, " invisible.")			
					SET_ENTITY_LOCALLY_INVISIBLE(OriginalPlayerPed)
				ENDIF
				
				RELOOP
			ENDIF
			
			viOriginalVehicle = GET_VEHICLE_PED_IS_IN(OriginalPlayerPed)
			
			PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - Setting player: ", i, " player/vehicle invisible.")
			
			SET_ENTITY_LOCALLY_INVISIBLE(OriginalPlayerPed)
			SET_ENTITY_LOCALLY_INVISIBLE(viOriginalVehicle)
						
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(viOriginalVehicle)
			OR NOT IS_VEHICLE_DRIVEABLE(viOriginalVehicle)
				RELOOP
			ENDIF
			
			IF NOT IS_BIT_SET(iExternalCutsceneBitset, ciREPOSITION_ORIGINAL_ENTITIES_PLAYER_0 + i)
				FLOAT fHead = 326.3484
				VECTOR vPos = <<914.3851, -25.5898, 77.7641>>
				VECTOR vNewPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHead, <<2.0 * i, 0.0, 0.0>>)
				SET_ENTITY_COORDS(viOriginalVehicle, vNewPos)
				SET_ENTITY_HEADING(viOriginalVehicle, fHead)
				
				SET_BIT(iExternalCutsceneBitset, ciREPOSITION_ORIGINAL_ENTITIES_PLAYER_0 + i)
				PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - Moving player: ", i, " player/vehicle.")
			ENDIF
		ENDFOR
	ENDIF
	
	SWITCH eExternalScriptedCutsceneSequenceState
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_IDLE				
			
			CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iBS, BS_SIMPLE_CUTSCENE_GLOBAL_PLAYER_BD_PLAYING)
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iCutsceneHash = 0
			SIMPLE_CUTSCENE_CLEAR_ALL_DATA(g_SimpleInteriorData.sCustomCutsceneExt)
			
			NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_FREEZE_POSITION)
			
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
					FREEZE_ENTITY_POSITION(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), TRUE)
				ENDIF
				
				PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_IDLE Starting Load Cutscene for in Veh.")
				SET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED(BE_VEHICLE)
			ELSE
				PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_IDLE Starting Load Cutscene for On Foot.")
				SET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED(BE_FOOT)
			ENDIF
			
			SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_LOAD, eExternalScriptedCutsceneSequenceState)
		BREAK
		
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_LOAD			
			IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			OR NOT IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
				IF GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = BE_VEHICLE		
					SET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED(BE_FOOT)
				ENDIF
			ENDIF
			
			IF GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = BE_VEHICLE							
				IF EXT_CUTSCENE_CASINO_GARAGE_ENTRY_CREATE_ASSETS(entryAnimExtCut)
					PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_LOAD Loaded Cutscene.")
					
					SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP, eExternalScriptedCutsceneSequenceState)
				ELSE
					PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_LOAD Waiting to Load Cutscene.")
				ENDIF
				IF DOES_ENTITY_EXIST(entryAnimExtCut.sCutsceneVeh.vehicle)
				AND DOES_ENTITY_EXIST(entryAnimExtCut.vehOverride)
					SET_ENTITY_NO_COLLISION_ENTITY(entryAnimExtCut.sCutsceneVeh.vehicle, entryAnimExtCut.vehOverride, FALSE)
				ENDIF
			ELSE
				PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_LOAD Loaded Cutscene (on foot)")					
				SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP, eExternalScriptedCutsceneSequenceState)
			ENDIF
		BREAK
		
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP								
			IF GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = BE_VEHICLE
				PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP Starting Cutscene (Vehicle).")
				SIMPLE_CUTSCENE_START(g_SimpleInteriorData.sCustomCutsceneExt, SCS_PREVENT_FROZEN_CHANGES_START|SCS_PREVENT_VISIBLITY_CHANGES_START)
				SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN, eExternalScriptedCutsceneSequenceState)
			ELSE
				SIMPLE_INTERIOR_DETAILS details
				GET_CASINO_APT_ENTRANCE_SCENE_DETAILS(SIMPLE_INTERIOR_CASINO_APT, details, ciCASINO_APT_ENTRANCE_FOOT_1)
				
				REQUEST_ANIM_DICT(details.entryAnim.dictionary)
				IF HAS_ANIM_DICT_LOADED(details.entryAnim.dictionary)
					PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP Starting Cutscene (Foot).")				
					SIMPLE_CUTSCENE_ADD_SCENE(g_SimpleInteriorData.sCustomCutsceneExt, 2000, "Garage door", details.entryAnim.cameraPos, details.entryAnim.cameraRot, details.entryAnim.cameraFov, details.entryAnim.cameraPos, details.entryAnim.cameraRot, details.entryAnim.cameraFov, details.entryAnim.fCamShake, 0, 1000)		
					SIMPLE_CUTSCENE_START(g_SimpleInteriorData.sCustomCutsceneExt, SCS_PREVENT_FROZEN_CHANGES_START|SCS_PREVENT_VISIBLITY_CHANGES_START)				
					iScriptedCutsceneSimpleInteriorSyncScene = CREATE_SYNCHRONIZED_SCENE(details.entryAnim.syncScenePos, details.entryAnim.syncSceneRot)
					TASK_SYNCHRONIZED_SCENE(localPlayerPed, iScriptedCutsceneSimpleInteriorSyncScene, details.entryAnim.dictionary, details.entryAnim.clip, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
					SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN, eExternalScriptedCutsceneSequenceState)					
					MC_REINIT_NET_TIMER_WITH_TIMESTAMP(iScriptedCutsceneSimpleInteriorSyncSceneTimeStamp)
				ELSE
					PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP Requesting Cutscene (Foot).")				
				ENDIF
			ENDIF			
		BREAK
		
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN							
			
			IF NOT g_SimpleInteriorData.sCustomCutsceneExt.bPlaying
				PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN Waiting for cutscene to start.")
				RETURN FALSE
			ENDIF
			
			SIMPLE_CUTSCENE_MAINTAIN(g_SimpleInteriorData.sCustomCutsceneExt)
			
			IF GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = BE_VEHICLE
								
				SIMPLE_INTERIORS simpInteriorUnused				
				
				IF DOES_ENTITY_EXIST(entryAnimExtCut.sCutsceneVeh.vehicle)
					
					PREVENT_COLLISIIONS_WITH_NEARBY_VEHICLES(entryAnimExtCut.sCutsceneVeh.vehicle, TRUE)
	
					IF NOT IS_BIT_SET(iExternalCutsceneBitset, ciEXTERNAL_SET_VEH_ON_GROUND_PROPERLY)
						SET_VEHICLE_ON_GROUND_PROPERLY(entryAnimExtCut.sCutsceneVeh.vehicle)
						SET_BIT(iExternalCutsceneBitset, ciEXTERNAL_SET_VEH_ON_GROUND_PROPERLY)
						PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN calling SET_VEHICLE_ON_GROUND_PROPERLY")	
					ENDIF

					IF IS_VEHICLE_ON_ALL_WHEELS(entryAnimExtCut.sCutsceneVeh.vehicle)
						PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN Not on all wheels...")
						IF IS_SCREEN_FADED_OUT()
						AND NOT IS_BIT_SET(iExternalCutsceneBitset, ciEXTERNAL_CUTSCENE_BITSET_FADED_IN)
							PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN Doing screen fadein manually.")
							SET_BIT(iExternalCutsceneBitset, ciEXTERNAL_CUTSCENE_BITSET_FADED_IN)
							DO_SCREEN_FADE_IN(100)
							FREEZE_ENTITY_POSITION(entryAnimExtCut.sCutsceneVeh.vehicle, FALSE)
						ENDIF
					ELSE
						SET_VEHICLE_ON_GROUND_PROPERLY(entryAnimExtCut.sCutsceneVeh.vehicle)
					ENDIF
					
				ENDIF
							
				IF EXT_CUTSCENE_CASINO_GAR_MAINTAIN(simpInteriorUnused, entryAnimExtCut)
					IF NOT IS_SCREEN_FADING_OUT()
						PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN Doing screen fadeout manually.")
						DO_SCREEN_FADE_OUT(500)
					ENDIF
					
					IF IS_SCREEN_FADED_OUT()
						PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN Finished Cutscene - Moving on.")					
						SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_CLEANUP, eExternalScriptedCutsceneSequenceState)
						HIDE_BUNKER_DOOR_COLLISION_IPLS_FOR_ACTIVITY_SESSION(FALSE)
					ELSE
						PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN Finished Cutscene - Waiting for Fadeout")
					ENDIF
				ELSE
					PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN Waiting for EXT_CUTSCENE_CASINO_GAR_MAINTAIN")
				ENDIF
			ELSE
				IF MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(iScriptedCutsceneSimpleInteriorSyncSceneTimeStamp, 1800)
					IF NOT IS_SCREEN_FADING_OUT()
						PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN Doing screen fadeout manually.")
						DO_SCREEN_FADE_OUT(500)
					ENDIF
					IF IS_SCREEN_FADED_OUT()
						SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_CLEANUP, eExternalScriptedCutsceneSequenceState)
					ENDIF
				ELSE
					IF IS_SCREEN_FADED_OUT()
						PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN Doing screen fadein manually. (Foot)")
						DO_SCREEN_FADE_IN(100)
					ENDIF
				ENDIF		
			ENDIF
		BREAK
		
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_CLEANUP
			PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Casino] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_CLEANUP calling cleanup functions.")
			
			PREVENT_COLLISIIONS_WITH_NEARBY_VEHICLES(entryAnimExtCut.sCutsceneVeh.vehicle, FALSE)
	
			SIMPLE_CUTSCENE_CLEAR_ALL_DATA(g_SimpleInteriorData.sCustomCutsceneExt)
			CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iBS, BS_SIMPLE_CUTSCENE_GLOBAL_PLAYER_BD_PLAYING)
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iCutsceneHash = 0
			iExternalCutsceneBitset = 0
			CLEAR_FOCUS()
			
			SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_IDLE, eExternalScriptedCutsceneSequenceState)			
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_VEHICLE_ENTRY(INT iCutscene)
		
	PED_INDEX PedToUse = LocalPlayerPed	
	SIMPLE_INTERIORS eSimpleInteriorID			
	SWITCH g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].eExternalCutsceneSequence		
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_HAWICK
			eSimpleInteriorID = SIMPLE_INTERIOR_FIXER_HQ_HAWICK				
			PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - eSimpleInteriorID = SIMPLE_INTERIOR_FIXER_HQ_HAWICK.")
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_ROCKFORD
			eSimpleInteriorID = SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD
			PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - eSimpleInteriorID = SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD.")
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_SEOUL
			eSimpleInteriorID = SIMPLE_INTERIOR_FIXER_HQ_SEOUL
			PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - eSimpleInteriorID = SIMPLE_INTERIOR_FIXER_HQ_SEOUL.")
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_VESPUCCI		
			eSimpleInteriorID = SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI
			PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - eSimpleInteriorID = SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI.")
		BREAK
		
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_HAWICK_PRIMARY_PLAYER
			eSimpleInteriorID = SIMPLE_INTERIOR_FIXER_HQ_HAWICK				
			PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - eSimpleInteriorID = SIMPLE_INTERIOR_FIXER_HQ_HAWICK.")
			IF NETWORK_IS_PLAYER_ACTIVE(MC_serverBD.piCutscenePlayerIDs[0][0])
				PedToUse = GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[0][0])
			ENDIF
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_ROCKFORD_PRIMARY_PLAYER
			eSimpleInteriorID = SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD
			PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - eSimpleInteriorID = SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD.")
			IF NETWORK_IS_PLAYER_ACTIVE(MC_serverBD.piCutscenePlayerIDs[0][0])
				PedToUse = GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[0][0])
			ENDIF
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_SEOUL_PRIMARY_PLAYER
			eSimpleInteriorID = SIMPLE_INTERIOR_FIXER_HQ_SEOUL
			PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - eSimpleInteriorID = SIMPLE_INTERIOR_FIXER_HQ_SEOUL.")
			IF NETWORK_IS_PLAYER_ACTIVE(MC_serverBD.piCutscenePlayerIDs[0][0])
				PedToUse = GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[0][0])
			ENDIF
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_VESPUCCI_PRIMARY_PLAYER
			eSimpleInteriorID = SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI
			PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - eSimpleInteriorID = SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI.")
			IF NETWORK_IS_PLAYER_ACTIVE(MC_serverBD.piCutscenePlayerIDs[0][0])
				PedToUse = GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[0][0])
			ENDIF				
		BREAK
	ENDSWITCH
		
	IF eExternalScriptedCutsceneSequenceState > EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP
		INT iTeam = iScriptedCutsceneTeam
		IF iScriptedCutsceneTeam = -1
			iTeam = MC_PlayerBD[iLocalPart].iTeam
		ENDIF
		PED_INDEX OriginalPlayerPed
		VEHICLE_INDEX viOriginalVehicle
		INT i = 0
		FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
			IF MC_serverBD.piCutscenePlayerIDs[iTeam][i] = INVALID_PLAYER_INDEX()
				RELOOP
			ENDIF
		
			OriginalPlayerPed = GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[iTeam][i])			
			
			IF IS_PED_INJURED(OriginalPlayerPed)
				RELOOP
			ENDIF
			
			IF NOT IS_PED_IN_ANY_VEHICLE(OriginalPlayerPed)
				IF NOT IS_BIT_SET(iExternalCutsceneBitset, ciREPOSITION_ORIGINAL_ENTITIES_PLAYER_0 + i)
				AND OriginalPlayerPed = LocalPlayerPed
					FLOAT fHead = GET_ENTITY_HEADING(localPlayerPed)
					VECTOR vPos = GET_ENTITY_COORDS(localPlayerPed)
					VECTOR vNewPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHead, <<2.0 * i, 0.0, 0.0>>)
					SET_ENTITY_COORDS(OriginalPlayerPed, vNewPos)
					SET_ENTITY_HEADING(OriginalPlayerPed, fHead)
					
					SET_BIT(iExternalCutsceneBitset, ciREPOSITION_ORIGINAL_ENTITIES_PLAYER_0 + i)
					PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - Moving player: ", i)
				ENDIF
				
				IF OriginalPlayerPed != LocalPlayerPed
					PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - Setting player: ", i, " invisible.")			
					SET_ENTITY_LOCALLY_INVISIBLE(OriginalPlayerPed)
				ENDIF
				
				RELOOP
			ENDIF
			
			viOriginalVehicle = GET_VEHICLE_PED_IS_IN(OriginalPlayerPed)
			
			PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - Setting player: ", i, " player/vehicle invisible.")
			
			SET_ENTITY_LOCALLY_INVISIBLE(OriginalPlayerPed)
			SET_ENTITY_LOCALLY_INVISIBLE(viOriginalVehicle)
			
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(viOriginalVehicle)
			OR NOT IS_VEHICLE_DRIVEABLE(viOriginalVehicle)
				RELOOP
			ENDIF
			
			IF NOT IS_BIT_SET(iExternalCutsceneBitset, ciREPOSITION_ORIGINAL_ENTITIES_PLAYER_0 + i)
				FLOAT fHead = GET_ENTITY_HEADING(OriginalPlayerPed)
				VECTOR vPos = GET_ENTITY_COORDS(OriginalPlayerPed)
				VECTOR vNewPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHead, <<2.0 * i, 0.0, 0.0>>)
				SET_ENTITY_COORDS(viOriginalVehicle, vNewPos)
				SET_ENTITY_HEADING(viOriginalVehicle, fHead)
				
				SET_BIT(iExternalCutsceneBitset, ciREPOSITION_ORIGINAL_ENTITIES_PLAYER_0 + i)
				PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - Moving player: ", i, " player/vehicle.")
			ENDIF
		ENDFOR
	ENDIF
			
	SWITCH eExternalScriptedCutsceneSequenceState
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_IDLE				
			
			CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iBS, BS_SIMPLE_CUTSCENE_GLOBAL_PLAYER_BD_PLAYING)
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iCutsceneHash = 0
			SIMPLE_CUTSCENE_CLEAR_ALL_DATA(g_SimpleInteriorData.sCustomCutsceneExt)
			
			NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_FREEZE_POSITION)
			
			IF IS_PED_IN_ANY_VEHICLE(PedToUse)				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(PedToUse))
					FREEZE_ENTITY_POSITION(GET_VEHICLE_PED_IS_IN(PedToUse), TRUE)
				ENDIF
				
				PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_IDLE Starting Load Cutscene for in Veh.")
				SET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED(BE_VEHICLE)
			ELSE
				PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_IDLE Starting Load Cutscene for On Foot.")
				SET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED(BE_FOOT)
			ENDIF
			
			SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_LOAD, eExternalScriptedCutsceneSequenceState)
		BREAK
		
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_LOAD			
			IF NOT IS_PED_IN_ANY_VEHICLE(PedToUse)
			OR NOT IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PedToUse))
				IF GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = BE_VEHICLE		
					SET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED(BE_FOOT)
				ENDIF
			ENDIF
			
			IF GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = BE_VEHICLE
				entryAnimExtCut.vehOverride = GET_VEHICLE_PED_IS_IN(PedToUse)
				
				IF EXT_CUTSCENE_FIXER_HQ_CREATE_ASSETS(eSimpleInteriorID, entryAnimExtCut)
					PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_LOAD Loaded Cutscene.")
					
					SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP, eExternalScriptedCutsceneSequenceState)
				ELSE
					PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_LOAD Waiting to Load Cutscene.")
				ENDIF	
				IF DOES_ENTITY_EXIST(entryAnimExtCut.sCutsceneVeh.vehicle)
				AND DOES_ENTITY_EXIST(entryAnimExtCut.vehOverride)
					SET_ENTITY_NO_COLLISION_ENTITY(entryAnimExtCut.sCutsceneVeh.vehicle, entryAnimExtCut.vehOverride, FALSE)
				ENDIF
			ELSE
				PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_LOAD Loaded Cutscene (on foot)")					
				SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP, eExternalScriptedCutsceneSequenceState)
			ENDIF
		BREAK
		
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP								
			IF GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = BE_VEHICLE
				PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP Starting Cutscene (Vehicle).")
				SIMPLE_CUTSCENE_START(g_SimpleInteriorData.sCustomCutsceneExt, SCS_PREVENT_FROZEN_CHANGES_START | SCS_PREVENT_VISIBLITY_CHANGES_START)
				SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN, eExternalScriptedCutsceneSequenceState)
			ELSE
				SIMPLE_INTERIOR_DETAILS details				
					
				GET_FIXER_HQ_ENTRANCE_SCENE_DETAILS(eSimpleInteriorID, details, ciFIXER_HQ_ENTRANCE_GARAGE_ON_FOOT)
				
				REQUEST_ANIM_DICT(details.entryAnim.dictionary)
				IF HAS_ANIM_DICT_LOADED(details.entryAnim.dictionary)
					PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP Starting Cutscene (Foot).")				
					SIMPLE_CUTSCENE_ADD_SCENE(g_SimpleInteriorData.sCustomCutsceneExt, 2000, "Garage door", details.entryAnim.cameraPos, details.entryAnim.cameraRot, details.entryAnim.cameraFov, details.entryAnim.cameraPos, details.entryAnim.cameraRot, details.entryAnim.cameraFov, details.entryAnim.fCamShake, 0, 1000)		
					SIMPLE_CUTSCENE_START(g_SimpleInteriorData.sCustomCutsceneExt, SCS_PREVENT_FROZEN_CHANGES_START|SCS_PREVENT_VISIBLITY_CHANGES_START)				
					iScriptedCutsceneSimpleInteriorSyncScene = CREATE_SYNCHRONIZED_SCENE(details.entryAnim.syncScenePos, details.entryAnim.syncSceneRot)
					TASK_SYNCHRONIZED_SCENE(localPlayerPed, iScriptedCutsceneSimpleInteriorSyncScene, details.entryAnim.dictionary, details.entryAnim.clip, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
					SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN, eExternalScriptedCutsceneSequenceState)					
					MC_REINIT_NET_TIMER_WITH_TIMESTAMP(iScriptedCutsceneSimpleInteriorSyncSceneTimeStamp)
				ELSE
					PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP Requesting Cutscene (Foot).")				
				ENDIF
			ENDIF			
		BREAK
		
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN							
			
			IF NOT g_SimpleInteriorData.sCustomCutsceneExt.bPlaying
				PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN Waiting for cutscene to start.")
				RETURN FALSE
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet4, ci_CSBS4_UseRenderphasePauseInsteadOfFade)	
				INT iScene
				FOR iScene = 0 TO SIMPLE_CUTSCENE_MAX_SCENES-1
					g_SimpleInteriorData.sCustomCutsceneExt.sScenes[iScene].iFadeOutTime = 0
				ENDFOR
			ENDIF
			
			SIMPLE_CUTSCENE_MAINTAIN(g_SimpleInteriorData.sCustomCutsceneExt)			
			
			IF GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = BE_VEHICLE
				
				IF DOES_ENTITY_EXIST(entryAnimExtCut.sCutsceneVeh.vehicle)
					
					PREVENT_COLLISIIONS_WITH_NEARBY_VEHICLES(entryAnimExtCut.sCutsceneVeh.vehicle, TRUE)
				
					IF NOT IS_BIT_SET(iExternalCutsceneBitset, ciEXTERNAL_SET_VEH_ON_GROUND_PROPERLY)
						SET_VEHICLE_ON_GROUND_PROPERLY(entryAnimExtCut.sCutsceneVeh.vehicle)
						SET_BIT(iExternalCutsceneBitset, ciEXTERNAL_SET_VEH_ON_GROUND_PROPERLY)
						PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN calling SET_VEHICLE_ON_GROUND_PROPERLY")	
					ENDIF

					IF IS_VEHICLE_ON_ALL_WHEELS(entryAnimExtCut.sCutsceneVeh.vehicle)
						PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN Not on all wheels...")
						IF IS_SCREEN_FADED_OUT()
						AND NOT IS_BIT_SET(iExternalCutsceneBitset, ciEXTERNAL_CUTSCENE_BITSET_FADED_IN)
							PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN Doing screen fadein manually.")
							SET_BIT(iExternalCutsceneBitset, ciEXTERNAL_CUTSCENE_BITSET_FADED_IN)
							DO_SCREEN_FADE_IN(100)
							FREEZE_ENTITY_POSITION(entryAnimExtCut.sCutsceneVeh.vehicle, FALSE)
						ENDIF
					ELSE
						SET_VEHICLE_ON_GROUND_PROPERLY(entryAnimExtCut.sCutsceneVeh.vehicle)
					ENDIF
					
				ENDIF
							
				IF EXT_CUTSCENE_FIXER_HQ_MAINTAIN(eSimpleInteriorID, entryAnimExtCut)
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet4, ci_CSBS4_UseRenderphasePauseInsteadOfFade)	
						IF MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(iScriptedCutsceneSimpleInteriorSyncSceneTimeStamp, 1800)
							TOGGLE_PAUSED_RENDERPHASES(FALSE)
							PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN Finished Cutscene - Moving on.")					
							SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_CLEANUP, eExternalScriptedCutsceneSequenceState)
						ENDIF
					ELSE					
						IF NOT IS_SCREEN_FADING_OUT()
							PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN Doing screen fadeout manually.")
							DO_SCREEN_FADE_OUT(500)
						ENDIF
						
						IF IS_SCREEN_FADED_OUT()
							PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN Finished Cutscene - Moving on.")					
							SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_CLEANUP, eExternalScriptedCutsceneSequenceState)
						ELSE
							PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN Finished Cutscene - Waiting for Fadeout")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet4, ci_CSBS4_UseRenderphasePauseInsteadOfFade)						
					IF IS_SCREEN_FADED_OUT()
						PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN Doing screen fadein manually. (Foot)")
						DO_SCREEN_FADE_IN(100)
					ENDIF
					IF MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(iScriptedCutsceneSimpleInteriorSyncSceneTimeStamp, 1800)
						TOGGLE_PAUSED_RENDERPHASES(FALSE)
						PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN Finished Cutscene - Moving on.")					
						SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_CLEANUP, eExternalScriptedCutsceneSequenceState)
					ENDIF
				ELSE
					IF MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(iScriptedCutsceneSimpleInteriorSyncSceneTimeStamp, 1800)
						IF NOT IS_SCREEN_FADING_OUT()
							PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN Doing screen fadeout manually.")
							DO_SCREEN_FADE_OUT(500)
						ENDIF
						IF IS_SCREEN_FADED_OUT()
							SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_CLEANUP, eExternalScriptedCutsceneSequenceState)
						ENDIF
					ELSE
						IF IS_SCREEN_FADED_OUT()
							PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN Doing screen fadein manually. (Foot)")
							DO_SCREEN_FADE_IN(100)
						ENDIF
					ENDIF
				ENDIF				
			ENDIF
		BREAK
		
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_CLEANUP
			PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Fixer] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_CLEANUP calling cleanup functions.")
			
			PREVENT_COLLISIIONS_WITH_NEARBY_VEHICLES(entryAnimExtCut.sCutsceneVeh.vehicle, FALSE)
			
			entryAnimExtCut.vehOverride = NULL
			SIMPLE_CUTSCENE_CLEAR_ALL_DATA(g_SimpleInteriorData.sCustomCutsceneExt)
			CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iBS, BS_SIMPLE_CUTSCENE_GLOBAL_PLAYER_BD_PLAYING)
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iCutsceneHash = 0
			iExternalCutsceneBitset = 0
			CLEAR_FOCUS()
			
			SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_IDLE, eExternalScriptedCutsceneSequenceState)			
			RETURN TRUE
		BREAK
		
	ENDSWITCH	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_EXTERNAL_CUTSCENE_SEQUENCE_PLAY(INT iCutscene)
	RETURN g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].eExternalCutsceneSequence != EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_NONE
ENDFUNC

FUNC BOOL SHOULD_EXTERNAL_CUTSCENE_SEQUENCE_USE_FORCED_FADEOUT_AND_WARP(INT iCutscene)
	SWITCH g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].eExternalCutsceneSequence		
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_1
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_2
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_3
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_4
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_5
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_6
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_7
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_9
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_10
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_11
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_12
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_CASINO_ENTRY
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_EXTERNAL_CUTSCENE_SEQUENCE_BE_SKIPPED_BY_SPECTATOR(INT iCutscene)
	SWITCH g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].eExternalCutsceneSequence
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_1
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_2
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_3
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_4
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_5
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_6
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_7
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_9
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_10
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_11
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_12
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_HAWICK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_ROCKFORD
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_SEOUL
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_VESPUCCI		
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_HAWICK_PRIMARY_PLAYER
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_ROCKFORD_PRIMARY_PLAYER
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_SEOUL_PRIMARY_PLAYER
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_VESPUCCI_PRIMARY_PLAYER
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_EXTERNAL_CUTSCENE_SEQUENCE(INT iCutscene)
	
	IF NOT HAS_NET_TIMER_STARTED(tdSafetyTimerExternalCutscenes)
		START_NET_TIMER(tdSafetyTimerExternalCutscenes)
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdSafetyTimerExternalCutscenes, ciSAFETY_TIMER_EXTERNAL_CUTSCENES)
		PRINTLN("[ExternalCutscene][MC_SimpleCutscene] - PROCESS_EXTERNAL_CUTSCENE_SEQUENCE - Hit safety timer, returning finished.")
		RETURN TRUE
	ENDIF
	
	SWITCH g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].eExternalCutsceneSequence
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_1
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_2
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_3
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_4
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_5
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_6
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_7
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_9
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_10
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_11
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_12	
			RETURN HAS_EXTERNAL_CUTSCENE_SEQUENCE_BUNKER_ENTRY_FINISHED(iCutscene)
		BREAK
		
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_CASINO_ENTRY
			RETURN HAS_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_CASINO_ENTRY_FINISHED()
		BREAK
		
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_HAWICK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_ROCKFORD
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_SEOUL
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_VESPUCCI
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_HAWICK_PRIMARY_PLAYER
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_ROCKFORD_PRIMARY_PLAYER
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_SEOUL_PRIMARY_PLAYER
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_ENTRY_HQ_VESPUCCI_PRIMARY_PLAYER
			RETURN HAS_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_FIXER_PROPERTY_VEHICLE_ENTRY(iCutscene)
		BREAK
		
	ENDSWITCH	
	
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cutscene Special Case - Hacking  --------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Unique and special case logic that handles Hacking during a cutscene..									 							 ---------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL HAS_ANY_REMOTE_PLAYER_COMPLETED_HACKING_INTRO()
	INT i
	PRINTLN("[PLAYER_LOOP] - HAS_ANY_REMOTE_PLAYER_COMPLETED_HACKING_INTRO")
	REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() i
		IF( IS_BIT_SET( MC_playerBD[ i ].iClientBitSet2, PBBOOL2_HACKING_INTRO_COMPLETED ) )
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC DISABLE_CONTROL_FOR_HACKING_INTRO()
	INT i
	FOR i = ENUM_TO_INT( INPUT_SPRINT ) TO ENUM_TO_INT( INPUT_VEH_SHUFFLE )
		CONTROL_ACTION action = INT_TO_ENUM( CONTROL_ACTION, i )
		IF( action <> INPUT_PHONE )
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, action )
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL HAS_THIS_PLAYER_COMPLETED_HACKING_MINGAME()
	RETURN IS_BIT_SET( MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_HACKING_MINIGAME_COMPLETED )
ENDFUNC

FUNC BOOL IS_CUTSCENE_USING_HACKING_INTRO( INT iCutsceneToUse )
	RETURN IS_BIT_SET( g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneBitset, ci_CSBS_Hacking_App_intro )
ENDFUNC	
	
FUNC BOOL IS_HACKING_INTRO_COMPLETE()
	RETURN IS_BIT_SET( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO )
ENDFUNC

PROC DO_CUTSCENE_PHONE_INTRO( INT iCutsceneToUse )
	// hacking app launch
	IF( IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_FOCUS_INTRO )
	AND NOT IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO )
	AND NOT IS_BIT_SET( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO ) )
		// if the content creator flag isn't set to start the hacking intro we're done
		IF( NOT IS_CUTSCENE_USING_HACKING_INTRO( iCutsceneToUse ) )
			CPRINTLN( DEBUG_SIMON, "DO_CUTSCENE_PHONE_INTRO - Not in use" )				
			SET_BIT( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO_INIT )
			SET_BIT( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO )
		ELSE
			IF( NOT IS_BIT_SET( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO_INIT ) )
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				IF( NOT IS_PHONE_ONSCREEN() )
					RESET_NET_TIMER( tdPhoneCutSafetyTimer )
					START_NET_TIMER( tdPhoneCutSafetyTimer )
					IF( HAS_THIS_PLAYER_COMPLETED_HACKING_MINGAME() )
						g_bEnableHackingApp = TRUE
					ENDIF
					SET_BIT( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO_INIT )
					CPRINTLN( DEBUG_SIMON, "DO_CUTSCENE_PHONE_INTRO - Init done" )
				ENDIF
			ELSE
				// if the local player completed the hacking minigame
				IF( HAS_THIS_PLAYER_COMPLETED_HACKING_MINGAME() )
				AND NOT bIsSCTV
					CPRINTLN( DEBUG_SIMON, "DO_CUTSCENE_PHONE_INTRO - This player is hacker" )
					CLEAR_BIT( iLocalBoolCheck7, LBOOL7_SHOULD_CUTSCENE_DISABLE_PHONE_THIS_FRAME )
					DISPLAY_HELP_TEXT_THIS_FRAME( "HEIST_HELP_29", FALSE ) // "Bring up your phone and launch the VLSI Unlock app."
					PRINT_NOW( "HEIST_OBJ_1", 10, 1 ) // "Unlock the vault."
				
					// wait for the player to launch the unlock app
					IF( g_HackingAppHasBeenLaunched )
						CPRINTLN( DEBUG_SIMON, "DO_CUTSCENE_PHONE_INTRO - App Launched" )
						DISABLE_CELLPHONE_THIS_FRAME_ONLY()
						IF( NOT IS_PHONE_ONSCREEN() )
							CPRINTLN( DEBUG_SIMON, "DO_CUTSCENE_PHONE_INTRO - Done" )
							CLEAR_THIS_PRINT( "HEIST_OBJ_1" )
							SET_BIT( MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_HACKING_INTRO_COMPLETED )
							SET_BIT( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO )
							g_bEnableHackingApp = FALSE
							g_HackingAppHasBeenLaunched = FALSE
						ENDIF
					ENDIF
				ELSE
					
					// non hacker players wait for hacker to unlock door
					CPRINTLN( DEBUG_SIMON, "DO_CUTSCENE_PHONE_INTRO - This player is not hacker" )

					IF( HAS_ANY_REMOTE_PLAYER_COMPLETED_HACKING_INTRO() )
					OR IS_FAKE_MULTIPLAYER_MODE_SET()
						CLEAR_THIS_PRINT( "HEIST_OBJ_2" )
						SET_BIT( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO )
					ENDIF
				ENDIF
				
				// timed out so progress
				IF( GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( tdPhoneCutSafetyTimer ) > 40000 )
					DISABLE_CELLPHONE_THIS_FRAME_ONLY()
					IF( NOT IS_PHONE_ONSCREEN() )
						CPRINTLN( DEBUG_SIMON, "DO_CUTSCENE_PHONE_INTRO - Timed out" )
						CLEAR_THIS_PRINT( "HEIST_OBJ_1" )
						CLEAR_THIS_PRINT( "HEIST_OBJ_2" )
						g_bEnableHackingApp = FALSE
						g_HackingAppHasBeenLaunched = FALSE
						SET_BIT( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO )
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	// phone intro init
	IF IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_FOCUS_INTRO )
	AND IS_BIT_SET( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO )
	AND NOT IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO )
		
		IF NOT GET_USINGNIGHTVISION()
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset,ci_CSBS_Phone_Intro)
			OR bIsSCTV
				g_FMMC_STRUCT.bSecurityCamActive = FALSE
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_HidePhone_DontHangup)
					HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
				ELSE
					HIDE_ACTIVE_PHONE(TRUE)
				ENDIF
				
				IF NOT IS_INTRO_CUTSCENE_RULE_ACTIVE()
				AND NOT IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_ScriptedCutsceneUseInterpTimeAsBlackScreenScene)
				AND NOT SHOULD_EXTERNAL_CUTSCENE_SEQUENCE_PLAY(iCutsceneToUse)
				AND NOT SHOULD_SPECTATOR_FADE_OUT_FOR_CUTSCENE(iCutsceneToUse)										
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)
					IF IS_SCREEN_FADED_OUT()
					OR IS_SCREEN_FADING_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF
				ENDIF
				
				PRINTLN("[Cutscenes] DO_CUTSCENE_PHONE_INTRO - NO PHONE INTRO SETTING SO SKIPPING")
				SET_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO)
				SET_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO)
			ELSE
				CLEAR_BIT( iLocalBoolCheck7, LBOOL7_SHOULD_CUTSCENE_DISABLE_PHONE_THIS_FRAME )
			
				g_FMMC_STRUCT.bSecurityCamActive = TRUE
				IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_READY_FOR_PHONE_INTRO)
					IF( GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH( "cellphone_flashhand" ) ) = 0
					AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH( "appinternet" ) ) = 0 )
						IF g_Cellphone.PhoneDS >= PDS_RUNNINGAPP
							PRINTLN("[Cutscenes] DO_CUTSCENE_PHONE_INTRO - APP IS RUNNING, HANGING UP PHONE")
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_HidePhone_DontHangup)
								HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
							ELSE
								HIDE_ACTIVE_PHONE(TRUE)
							ENDIF
							SET_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_READY_FOR_PHONE_INTRO)
						ELSE
							PRINTLN("[Cutscenes] DO_CUTSCENE_PHONE_INTRO - APP ISN'T RUNNING SO READY TO START INTRO")
							LAUNCH_CELLPHONE_APPLICATION(AppCamera, FALSE, TRUE, FALSE)
							SET_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO)
							REINIT_NET_TIMER(tdPhoneCutSafetyTimer)
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_PHONE_ONSCREEN()
						PRINTLN("[Cutscenes] DO_CUTSCENE_PHONE_INTRO - PHONE NOW ISN'T ON SCREEN SO READY TO START INTRO")
						LAUNCH_CELLPHONE_APPLICATION(AppCamera, FALSE, TRUE, FALSE)
						SET_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO)
						REINIT_NET_TIMER(tdPhoneCutSafetyTimer)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	// phone intro update
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO)
	AND NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO)
	AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_FOCUS_INTRO)
	AND IS_BIT_SET( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO )
		CLEAR_BIT( iLocalBoolCheck7, LBOOL7_SHOULD_CUTSCENE_DISABLE_PHONE_THIS_FRAME )
		
		IF NOT IS_INTRO_CUTSCENE_RULE_ACTIVE()
		AND NOT IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_ScriptedCutsceneUseInterpTimeAsBlackScreenScene)
		AND NOT SHOULD_EXTERNAL_CUTSCENE_SEQUENCE_PLAY(iCutsceneToUse)
		AND NOT SHOULD_SPECTATOR_FADE_OUT_FOR_CUTSCENE(iCutsceneToUse)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)
			IF IS_SCREEN_FADED_OUT()
			OR IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
		
		IF bLocalPlayerPedOk
			IF IS_ENTITY_ALIVE(LocalPlayerPed)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(LocalPlayerPed)	
					IF NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(LocalPlayerPed)
						CLEAR_PED_TASKS(LocalPlayerPed)
						TASK_USE_MOBILE_PHONE(LocalPlayerPed,TRUE,Mode_ToText)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		VECTOR g_Test3dPhonePosVec
		GET_MOBILE_PHONE_POSITION(g_Test3dPhonePosVec)
		
	   	PRINTLN( "[Cutscenes] DO_CUTSCENE_PHONE_INTRO - phone position: ", g_Test3dPhonePosVec, " g_Phone_Active_but_Hidden: ", g_Phone_Active_but_Hidden )
		
		IF g_Test3dPhonePosVec.x = 1.5 AND g_Test3dPhonePosVec.z = -17.0
			PRINTLN("[Cutscenes] DO_CUTSCENE_PHONE_INTRO - PHONE HAS FILLED SCREEN, DONE")
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_HidePhone_DontHangup)
				HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
			ELSE
				HIDE_ACTIVE_PHONE(TRUE)
			ENDIF
			SET_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO)
			RESET_NET_TIMER(tdPhoneCutSafetyTimer)
		ELSE
			//Safety backup for if the phone refuses to open up:
			BOOL bPhone 	= IS_PHONE_ONSCREEN()
			INT  iTimerDiff = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdPhoneCutSafetyTimer)
			
			PRINTLN("[Cutscenes] DO_CUTSCENE_PHONE_INTRO - bPhone:", bPhone, ", iTimerDiff:", iTimerDiff)
			
			IF NOT IS_BIT_SET(iLocalBoolCheck7,LBOOL7_CUTSCENE_PHONE_SAFETY_TRIGGERED)
				IF NOT bPhone
					IF iTimerDiff > 2000
						//Try again:
						LAUNCH_CELLPHONE_APPLICATION(AppCamera, FALSE, TRUE, FALSE)
						SET_BIT(iLocalBoolCheck7,LBOOL7_CUTSCENE_PHONE_SAFETY_TRIGGERED)
						REINIT_NET_TIMER(tdPhoneCutSafetyTimer)
						PRINTLN("[Cutscenes] DO_CUTSCENE_PHONE_INTRO - Phone not appeared on screen, try again...")
					ENDIF
				ELSE
					IF iTimerDiff > 6000
						//Give up:
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_HidePhone_DontHangup)
							HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
						ELSE
							HIDE_ACTIVE_PHONE(TRUE)
						ENDIF
						SET_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO)
						RESET_NET_TIMER(tdPhoneCutSafetyTimer)
						PRINTLN("[Cutscenes] DO_CUTSCENE_PHONE_INTRO - Phone on screen but not opened camera app quickly enough, giving up")
					ENDIF
				ENDIF
			ELSE
				IF (bPhone AND iTimerDiff > 6000)
				OR ((NOT bPhone) AND iTimerDiff > 2000)
					//Give up:
					HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
					SET_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO)
					RESET_NET_TIMER(tdPhoneCutSafetyTimer)
					PRINTLN("[Cutscenes] DO_CUTSCENE_PHONE_INTRO - Phone taken too long, giving up")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC RESET_PER_SHOT_PHONE_INTRO( INT iShot = -1, BOOL bBitFlagsOnly = TRUE )
	CLEAR_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO )
	CLEAR_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO )
	CLEAR_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_READY_FOR_PHONE_INTRO )
	CLEAR_BIT( iLocalBoolCheck7, LBOOL7_CUTSCENE_PHONE_SAFETY_TRIGGERED )
	IF( NOT bBitFlagsOnly )
		iPhoneIntroDoneForShot = iShot
	ENDIF
ENDPROC

FUNC BOOL IS_PHOTO_INTRO_DONE_FOR_SHOT( INT iCutsceneToUse, INT iShot )
	RETURN iPhoneIntroDoneForShot = iCamShot 
	AND IS_BIT_SET( g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[ iShot ], ciCSS_BS_Use_Phone_Intro )
ENDFUNC

PROC DO_PER_SHOT_PHONE_INTRO( INT iCutsceneToUse, INT iShot )
	// make sure current shot doesn't end before phone fill screen finished

	IF( iPhoneIntroDoneForShot <> iShot )
		// if the phone intro hasn't been started
		IF( NOT IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO ) )
			// if we don't want a phone intro or the player is spectating then skip it
			IF( NOT IS_BIT_SET( g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[ iShot ], ciCSS_BS_Use_Phone_Intro )
			OR bIsSCTV)
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_HidePhone_DontHangup)
					HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
				ELSE
					HIDE_ACTIVE_PHONE(TRUE)
				ENDIF
				
				IF NOT (IS_INTRO_CUTSCENE_RULE_ACTIVE() AND IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iShot], ciCSS_BS_HideShotInIntro))
				AND NOT SHOULD_SPECTATOR_FADE_OUT_FOR_CUTSCENE(iCutsceneToUse)
				AND NOT IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_ScriptedCutsceneUseInterpTimeAsBlackScreenScene)
				AND NOT SHOULD_EXTERNAL_CUTSCENE_SEQUENCE_PLAY(iCutsceneToUse)				
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)
					IF( IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT() )
						DO_SCREEN_FADE_IN( 500 )
					ENDIF
				ENDIF
				
				PRINTLN( "[Cutscenes] DO_PER_SHOT_PHONE_INTRO - NO PHONE INTRO FOR SHOT: ", iShot )
				CPRINTLN( DEBUG_SIMON, "[Cutscenes] DO_PER_SHOT_PHONE_INTRO - NO PHONE INTRO FOR SHOT: ", iShot )
				RESET_PER_SHOT_PHONE_INTRO( iShot, FALSE )
			ELSE // we want a phone intro so init
				CLEAR_BIT( iLocalBoolCheck7, LBOOL7_SHOULD_CUTSCENE_DISABLE_PHONE_THIS_FRAME )
				
				// if the init isn't finished
				IF( NOT IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_READY_FOR_PHONE_INTRO ) )
					// put phone away safely
					IF( g_Cellphone.PhoneDS >= PDS_RUNNINGAPP )
						HANG_UP_AND_PUT_AWAY_PHONE( TRUE )
						SET_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_READY_FOR_PHONE_INTRO )
					ELSE
						LAUNCH_CELLPHONE_APPLICATION( AppCamera, FALSE, TRUE, FALSE )
						SET_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO )
						REINIT_NET_TIMER( tdPhoneCutSafetyTimer )
					ENDIF
				ELSE // init finished
					IF( NOT IS_PHONE_ONSCREEN() )
						LAUNCH_CELLPHONE_APPLICATION( AppCamera, FALSE, TRUE, FALSE )
						SET_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO )
						REINIT_NET_TIMER( tdPhoneCutSafetyTimer )
					ENDIF
				ENDIF
			ENDIF
		ELSE // do the phone intro
			CLEAR_BIT( iLocalBoolCheck7, LBOOL7_SHOULD_CUTSCENE_DISABLE_PHONE_THIS_FRAME )
		
			IF NOT (IS_INTRO_CUTSCENE_RULE_ACTIVE() AND IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iShot], ciCSS_BS_HideShotInIntro))
			AND NOT SHOULD_SPECTATOR_FADE_OUT_FOR_CUTSCENE(iCutsceneToUse)
			AND NOT IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_ScriptedCutsceneUseInterpTimeAsBlackScreenScene)
			AND NOT SHOULD_EXTERNAL_CUTSCENE_SEQUENCE_PLAY(iCutsceneToUse)				
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)
				IF( IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT() )
					DO_SCREEN_FADE_IN( 500 )
				ENDIF
			ENDIF
			
			IF bLocalPlayerPedOk
				IF NETWORK_HAS_CONTROL_OF_ENTITY(LocalPlayerPed)	
					IF( NOT IS_PED_RUNNING_MOBILE_PHONE_TASK( LocalPlayerPed ) )
						CLEAR_PED_TASKS( LocalPlayerPed )
						TASK_USE_MOBILE_PHONE( LocalPlayerPed, TRUE, Mode_ToText )
					ENDIF
				ENDIF
			ENDIF
			
			VECTOR g_Test3dPhonePosVec
			GET_MOBILE_PHONE_POSITION( g_Test3dPhonePosVec )
			
	       	PRINTLN( "[Cutscenes] DO_PER_SHOT_PHONE_INTRO - phone position: ", g_Test3dPhonePosVec, " g_Phone_Active_but_Hidden: ", g_Phone_Active_but_Hidden )
			CPRINTLN( DEBUG_SIMON, "[Cutscenes] DO_PER_SHOT_PHONE_INTRO - phone position: ", g_Test3dPhonePosVec, " g_Phone_Active_but_Hidden: ", g_Phone_Active_but_Hidden )
			
			IF( g_Test3dPhonePosVec.x = 1.5 AND g_Test3dPhonePosVec.z = -17.0 )
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_HidePhone_DontHangup)
					HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
				ELSE
					HIDE_ACTIVE_PHONE(TRUE)
				ENDIF
				SET_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO )
				RESET_NET_TIMER( tdPhoneCutSafetyTimer )
			ELSE
				//Safety backup for if the phone refuses to open up:
				BOOL bPhone = IS_PHONE_ONSCREEN()
				INT iTimerDiff = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( tdPhoneCutSafetyTimer )
				
				IF( NOT IS_BIT_SET( iLocalBoolCheck7, LBOOL7_CUTSCENE_PHONE_SAFETY_TRIGGERED ) )
					IF( NOT bPhone )
						IF( iTimerDiff > 2000 )
							//Try again
							LAUNCH_CELLPHONE_APPLICATION( AppCamera, FALSE, TRUE, FALSE )
							SET_BIT( iLocalBoolCheck7, LBOOL7_CUTSCENE_PHONE_SAFETY_TRIGGERED )
							REINIT_NET_TIMER( tdPhoneCutSafetyTimer )
							PRINTLN( "[Cutscenes] DO_PER_SHOT_PHONE_INTRO - Phone not appeared on screen, try again..." )
							CPRINTLN( DEBUG_SIMON, "[Cutscenes] DO_PER_SHOT_PHONE_INTRO - Phone not appeared on screen, try again..." )
						ENDIF
					ELSE
						IF( iTimerDiff > 6000 )
							//Give up
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_HidePhone_DontHangup)
								HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
							ELSE
								HIDE_ACTIVE_PHONE(TRUE)
							ENDIF
							SET_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO )
							RESET_NET_TIMER( tdPhoneCutSafetyTimer )
							PRINTLN( "[Cutscenes] DO_PER_SHOT_PHONE_INTRO - Phone not appeared on screen, try again..." )
							CPRINTLN( DEBUG_SIMON, "[Cutscenes] DO_PER_SHOT_PHONE_INTRO - Phone not appeared on screen, try again..." )
						ENDIF
					ENDIF
				ELSE
					IF( ( bPhone AND iTimerDiff > 6000 ) OR ( NOT bPhone AND iTimerDiff > 2000 ) )
						//Give up						
						HANG_UP_AND_PUT_AWAY_PHONE( TRUE )
						SET_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO )
						RESET_NET_TIMER( tdPhoneCutSafetyTimer )
						PRINTLN( "[Cutscenes] DO_PER_SHOT_PHONE_INTRO - Phone taken too long, giving up" )
						CPRINTLN( DEBUG_SIMON, "[Cutscenes] DO_PER_SHOT_PHONE_INTRO - Phone taken too long, giving up" )
					ENDIF
				ENDIF
			ENDIF
			
			IF( IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO ) )
				RESET_PER_SHOT_PHONE_INTRO( iShot, FALSE )
			ENDIF
		ENDIF
	ENDIF
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cutscene Special Case - CCTV  -----------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Unique and special case logic that handles CCTV controls during a cutscene.									 						 ---------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
PROC SET_CCTV_LOCATION(STRING sCamDetails , STRING sCamLocale )
	
	BEGIN_SCALEFORM_MOVIE_METHOD(SI_SecurityCam, "SET_DETAILS")
		IF IS_STRING_EMPTY(sCamDetails)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPH_CAMDETAILS1")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sCamDetails)
		ENDIF
		PRINTLN("[Cutscenes] SET_CCTV_LOCATION -SET_DETAILS : ")
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(SI_SecurityCam, "SET_LOCATION")
		IF IS_STRING_EMPTY(sCamLocale)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPH_CAMLOCALE11")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sCamLocale)
		ENDIF
		PRINTLN("[Cutscenes] SET_CCTV_LOCATION -SET_LOCATION : ")
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

FUNC BOOL DOES_THIS_CUTSCENE_USE_A_CCTV_FILTER( INT iCutsceneToUse )
	INT iShotLoop
	REPEAT MAX_CAMERA_SHOTS iShotLoop
		IF IS_BIT_SET( g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[ iShotLoop ], ciCSS_BS_SecurityCamFilter )
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC SETUP_MANUAL_CCTV_HELP()
	
	CLEAR_MENU_DATA()
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT)

	SET_MENU_ITEM_TOGGLEABLE(FALSE)
	
	LOAD_MENU_ASSETS()
	REMOVE_MENU_HELP_KEYS()
	IF iSpectatorTarget = -1
		ADD_MENU_HELP_KEY_INPUT( INPUT_SCRIPT_RIGHT_AXIS_X, "CCTV_PAN")	//pan
		ADD_MENU_HELP_KEY_INPUT( INPUT_SCRIPT_LEFT_AXIS_Y, "CCTV_ZOO")	//zoom
		ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_DPAD_LR, "CCTV_CH")	//change feed
	ENDIF
	
	PRINTLN("SETUP_MANUAL_CCTV_HELP() - adding menu" )

ENDPROC

PROC RENDER_CCTV_OVERLAY()
	
	INT iThisHours,iThisMinutes

	iThisHours = GET_CLOCK_HOURS()
	iThisMinutes = GET_CLOCK_MINUTES()
	
	PRINTLN("[Cutscenes] RENDER_CCTV_OVERLAY -iThisHours: ", iThisHours)
	PRINTLN("[Cutscenes] RENDER_CCTV_OVERLAY -iThisMinutes: ", iThisMinutes)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(SI_SecurityCam, "SET_TIME")		
		IF iThisHours >= 0 AND iThisHours <= 12
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iThisHours)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iThisHours - 12)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iThisMinutes)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(00)
		IF iThisHours >= 0 AND iThisHours < 12
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPH_TIMEAM")
			PRINTLN("[Cutscenes] SETTING MPH_TIMEAM ")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPH_TIMEPM")
			PRINTLN("[Cutscenes] SETTING MPH_TIMEPM ")
		ENDIF
		
	END_SCALEFORM_MOVIE_METHOD()
	
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)

	DRAW_SCALEFORM_MOVIE_FULLSCREEN(SI_SecurityCam, 255, 255, 255, 255)
	
ENDPROC

PROC UPDATE_CAM_PARAMS_FOR_CCTV(INT iThisCutscene,INT iThisCamShot)
	IF( iThisCamShot - 1 >= 0 )
		IF( IS_BIT_SET( g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].iCutsceneShotBitSet[iThisCamShot - 1], ciCSS_BS_Attach_Enable ) )
			DETACH_CAM( cutscenecam )
			DESTROY_CAM( cutscenecam )
			cutscenecam = CREATE_CAM( "DEFAULT_SCRIPTED_CAMERA", TRUE ) 
			PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - detaching cam" )
		ENDIF
	ENDIF
	
	VECTOR vCCTVUpdateCamRot
	FLOAT fCCTVUpdateFov
	
	vCCTVUpdateCamRot = g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].vCamStartRot[iThisCamShot]
	IF NOT IS_VECTOR_ZERO(vCCTVRot[iThisCamShot])
		vCCTVUpdateCamRot.z = vCCTVRot[iThisCamShot].z
	ENDIF
	
	fCCTVUpdateFov = fCCTVFOV[iThisCamShot]
	
	IF fCCTVUpdateFov > 0
		SET_CAM_PARAMS(cutscenecam,g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].vCamStartPos[iThisCamShot],vCCTVUpdateCamRot,fCCTVUpdateFov)
		
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - setting new params" )
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - iThisCamShot:", iThisCamShot)
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - vCCTVUpdateCamRot: ", vCCTVUpdateCamRot )
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - fCCTVUpdateFov: ",fCCTVUpdateFov )
	ELSE
		SET_CAM_PARAMS(cutscenecam,g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].vCamStartPos[iThisCamShot],vCCTVUpdateCamRot,g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fStartCamFOV[iThisCamShot])
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - iThisCamShot:", iThisCamShot)
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - setting new params" )
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - vCCTVUpdateCamRot: ", vCCTVUpdateCamRot )
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - fCCTVUpdateFov: ",g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fStartCamFOV[iThisCamShot] )
	ENDIF
	
	IF( IS_BIT_SET( g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Attach_Enable ) )
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - attaching cam" )
	
		INT 			iVehIndex 	= g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].iVehicleToAttachTo[iThisCamShot]
		NETWORK_INDEX	niVeh		= MC_serverBD_1.sFMMC_SBD.niVehicle[ iVehIndex ]
		VEHICLE_INDEX 	veh			= NULL
		
		IF( NETWORK_DOES_NETWORK_ID_EXIST( niVeh ) )
			veh = NET_TO_VEH( niVeh )
			
			ATTACH_CAM_TO_ENTITY( cutscenecam, veh, g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].vOffsetPos[iThisCamShot], TRUE )
			POINT_CAM_AT_ENTITY( cutscenecam, veh, g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].vLookAtPos[iThisCamShot], TRUE )
		ELSE
			ASSERTLN( "UPDATE_CAM_PARAMS_FOR_CCTV - Couldn't get cam attach entity" )			
			#IF IS_DEBUG_BUILD
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_ATTACH_CAM_ENTITY_DOES_NOT_EXIST, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "Cutscene Couldn't get cam attach entity!")
			#ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].iShakeType[iThisCamShot] != -1
	AND g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fShakeMag[iThisCamShot] != 0
		SHAKE_CAM(cutscenecam,GET_CAM_SHAKE_TYPE_FROM_INT(g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].iShakeType[iThisCamShot]),g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fShakeMag[iThisCamShot])
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - shaking cam" )
	ELSE
		STOP_CAM_SHAKING(cutscenecam,TRUE)
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - stop shaking cam" )
	ENDIF

ENDPROC

PROC UPDATE_CCTV_WITH_OVERRIDDEN_CONSTRAINTS(INT iCutscene, INT iShot, FLOAT fMovementX)
	FLOAT fMinBound = 360 - g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].fOverrideRotMin[iShot]
	FLOAT fMaxBound = 360 - g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].fOverrideRotMax[iShot]

	//If out of 360 to -360 bounds, cap it to back within those. We do calculations with the assumption that
	//	the rotation is without those bounds.
	IF vCCTVRot[iShot].z > 360
		vCCTVRot[iShot].z -= 360 * ABSI(FLOOR(vCCTVRot[iShot].z / 360))
	ELIF vCCTVRot[iShot].z < -360
		vCCTVRot[iShot].z += 360 * ABSI(FLOOR(vCCTVRot[iShot].z / 360))
	ENDIF
	
	FLOAT fActualRot = ABSF(360 - vCCTVRot[iShot].z)
	
	IF fActualRot > 360
		fActualRot -= 360
	ENDIF
	
	FLOAT fTargetRot = vCCTVRot[iShot].z + (fMovementX * -1)	
	IF ABSF(fMaxBound - fMinBound) = 360				// Allow 360 movement if bounds are 0 and 360.
	OR (fMovementX > 0 AND fTargetRot > fMaxBound) 		//Panning to the right
	OR (fMovementX < 0 AND fTargetRot < fMinBound) 		//Panning to the left		 	   
		vCCTVRot[iShot].z = fTargetRot
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bOverrideCCTVRotDebug
			
			TEXT_LABEL_63 tl63
			
			tl63 = "Cutscene "
			tl63 += iCutscene
			tl63 += ", Shot "
			tl63 += iShot
			tl63 += ". fMovementX: "
			tl63 += GET_STRING_FROM_FLOAT(fMovementX, 3)
			DRAW_DEBUG_TEXT_2D(tl63, << 0.4, 0.4, 0 >>)
			
			tl63 = "Start Rot: "
			tl63 += GET_STRING_FROM_VECTOR(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].vCamStartRot[iShot])
			DRAW_DEBUG_TEXT_2D(tl63, << 0.6, 0.4, 0 >>)
			
			tl63 = "Rot: "
			tl63 += GET_STRING_FROM_FLOAT(vCCTVRot[iShot].z, 3)
			tl63 += ", ActualRot: "
			tl63 += GET_STRING_FROM_FLOAT(fActualRot, 3)
			DRAW_DEBUG_TEXT_2D(tl63, << 0.4, 0.416, 0 >>)
			
			tl63 = "Min: "
			tl63 += GET_STRING_FROM_FLOAT(fMinBound)
			tl63 += ", Max: "
			tl63 += GET_STRING_FROM_FLOAT(fMaxBound)
			DRAW_DEBUG_TEXT_2D(tl63, << 0.4, 0.432, 0 >>)
			
			VECTOR vMinLineEndPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].vCamStartPos[iShot], g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].fOverrideRotMin[iShot], <<0,13.5,0>>)
			VECTOR vMaxLineEndPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].vCamStartPos[iShot], g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].fOverrideRotMax[iShot], <<0,13.5,0>>)
			
			DRAW_DEBUG_LINE(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].vCamStartPos[iShot], vMinLineEndPos)
			DRAW_DEBUG_LINE(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].vCamStartPos[iShot], vMaxLineEndPos)
		ENDIF
	#ENDIF
ENDPROC

PROC PLAY_CUTSCENE_SHOT_REACHED_TICKER(INT iCutsceneToUse)
	
	IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotCustomTicker[iCamShot] = -1
	OR IS_BIT_SET(iLocalBoolCheck35, LBOOL35_PLAYED_CUTSCENE_SHOT_REACHED_TICKER)
	OR bIsAnySpectator
		EXIT
	ENDIF	
									
	PRINTLN("[Cutscene_Scripted] - PLAY_CUTSCENE_SHOT_REACHED_TICKER - iCutsceneShotCustomTicker: ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotCustomTicker[iCamShot])
	
	THEFEED_SHOW()
	THEFEED_RESUME()
	THEFEED_FORCE_RENDER_ON()	
	
	TEXT_LABEL_63 tlCustom_Ticker
	tlCustom_Ticker = GET_CUSTOM_STRING_LIST_TEXT_LABEL(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotCustomTicker[iCamShot])
	IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_ThisShotCustomStringTickerInsertPlayerName)	
		PRINT_TICKER_WITH_PLAYER_NAME(tlCustom_Ticker, LocalPlayer)
	ELSE
		PRINT_TICKER(tlCustom_Ticker)
	ENDIF

	SET_BIT(iLocalBoolCheck35, LBOOL35_PLAYED_CUTSCENE_SHOT_REACHED_TICKER)

	BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_PlayTickerOnShot, iCutsceneToUse, iCamShot, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotCustomTicker[iCamShot])	
			
ENDPROC

PROC OVERRIDE_CUTSCENE_ENDING_INTERP_TIME_FROM_CURRENT_CAM_SHOT(INT iCutsceneToUse)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_ThisShotProgressesSceneAfterInterpTime)
	OR bIsAnySpectator
		EXIT
	ENDIF	
	
	IF iCutsceneInterpTimeOverride > -1
		EXIT
	ENDIF
	
	REINIT_NET_TIMER(tdScriptedCutsceneTimer)
	iCutsceneInterpTimeOverride = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpTime[iCamShot]	
	BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_OverrideCutsceneInterpTimeToFinish, iCutsceneToUse, iCamShot, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotCustomTicker[iCamShot], DEFAULT, DEFAULT, DEFAULT, DEFAULT, iCutsceneInterpTimeOverride)	
ENDPROC

PROC MANAGE_MANUAL_CCTV_CONTROL(INT iThisCutscene,INT iThisCamShot)

	fThisPadRightX = GET_DISABLED_CONTROL_NORMAL (FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
	fThisPadLeftY = GET_DISABLED_CONTROL_NORMAL (FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)

	fXMove = fThisPadRightX * fCamMoveSensitivity 
    fYMove = fThisPadLeftY * fCamMoveSensitivity 
	fYMove = fYMove * - 1
	
	BOOL bUsingOverriddenRotConstraints = FALSE
	
	IF iThisCutscene > -1
	AND iThisCamShot > -1
		bUsingOverriddenRotConstraints = IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].iCutsceneShotBitSet[iThisCamShot], ciCSS_BS_OverrideRotationBounds)
	ENDIF
	
	IF bUsingOverriddenRotConstraints
		UPDATE_CCTV_WITH_OVERRIDDEN_CONSTRAINTS(iThisCutscene, iThisCamShot, fXMove)
	ELSE
		IF iThisCamShot = 0 
			//- right
			IF fXMove > 0
				PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - trying to pan right")
				IF vCCTVRot[iThisCamShot].z > 5
					vCCTVRot[iThisCamShot].z = vCCTVRot[iThisCamShot].z  +  (fXMove * - 1)
				ELSE
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - bounds met for panning right")
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV Rot is: ", vCCTVRot[iThisCamShot].z)
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV max rot is: 5 ")
				ENDIF
			ENDIF
			
			//-left
			IF fXMove < 0 
				PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - trying to pan left")
				IF vCCTVRot[iThisCamShot].z < 96
					vCCTVRot[iThisCamShot].z = vCCTVRot[iThisCamShot].z  + (fXMove * - 1)
				ELSE
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - bounds met for panning left")
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV Rot is: ", vCCTVRot[iThisCamShot].z)
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV left bound is: 96")
				ENDIF
			ENDIF
		ENDIF
		
		IF iThisCamShot = 1 
			//- right
			IF fXMove > 0
				PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - trying to pan right")
				IF vCCTVRot[iThisCamShot].z > 107
					vCCTVRot[iThisCamShot].z = vCCTVRot[iThisCamShot].z  +  (fXMove * - 1)
				ELSE
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - bounds met for panning right")
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV Rot is: ", vCCTVRot[iThisCamShot].z)
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV max rot is: 107 ")
				ENDIF
			ENDIF
			
			//-left
			IF fXMove < 0 
				PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - trying to pan left")
				IF vCCTVRot[iThisCamShot].z < 180
					vCCTVRot[iThisCamShot].z = vCCTVRot[iThisCamShot].z  + (fXMove * - 1)
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - greater than 0 pan left freely")
				ELSE
					IF vCCTVRot[iThisCamShot].z < 208
						vCCTVRot[iThisCamShot].z = vCCTVRot[iThisCamShot].z  + (fXMove * - 1)
					ELSE
						PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - bounds met for panning left")
						PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV Rot is: ", vCCTVRot[iThisCamShot].z)
						PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV left bound is: -153")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF iThisCamShot = 2 
			//- right
			IF fXMove > 0
				PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - trying to pan right")
				IF vCCTVRot[iThisCamShot].z > 304
					vCCTVRot[iThisCamShot].z = vCCTVRot[iThisCamShot].z  +  (fXMove * - 1)
				ELSE
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - bounds met for panning right")
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV Rot is: ", vCCTVRot[iThisCamShot].z)
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV max rot is: 5 ")
				ENDIF
			ENDIF
			
			//-left
			IF fXMove < 0 
				PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - trying to pan left")
				IF vCCTVRot[iThisCamShot].z < 432
					vCCTVRot[iThisCamShot].z = vCCTVRot[iThisCamShot].z  + (fXMove * - 1)
				ELSE
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - bounds met for panning left")
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV Rot is: ", vCCTVRot[iThisCamShot].z)
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV left bound is: 49")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Zoom
	//In
	IF fYMove > 0
		PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - trying to zoom in")
		IF fCCTVFOV[iThisCamShot] > g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fStartCamFOV[iThisCamShot] - 20
			fCCTVFOV[iThisCamShot] = fCCTVFOV[iThisCamShot]  + (fYMove * - 1)
			PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - decreasing FOV by: ", (fYMove * - 1))
			PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - Zoom in - New FOV value: ", fCCTVFOV[iThisCamShot])
		ELSE
			PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - bounds met for zooming in")
			PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV zoom is: ", fCCTVFOV[iThisCamShot])
			PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV max zoom is: ", g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fStartCamFOV[iThisCamShot] - 20)
		ENDIF
	ENDIF
	
	//Out
	IF fYMove < 0
		PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - trying to zoom out")
		IF fCCTVFOV[iThisCamShot] < g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fStartCamFOV[iThisCamShot] + 20
			fCCTVFOV[iThisCamShot] = fCCTVFOV[iThisCamShot]  + (fYMove * - 1)
			PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - increasing FOV by: ", (fYMove * - 1))
			PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - Zoom out - New FOV value: ", fCCTVFOV[iThisCamShot])
		ELSE
			PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - bounds met for zooming out")
			PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV zoom is: ", fCCTVFOV[iThisCamShot])
			PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV min zoom is: ", g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fStartCamFOV[iThisCamShot] + 20)
		ENDIF
	ENDIF

ENDPROC

FUNC STRING GET_CAM_AUDIO_BG_LOOP_SOUND_SET(INT iThisCutscene, INT iThisCamShot)
	IF IS_USING_CUTSCENE_DRONE_SCALEFORM(g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].eDroneUIPreset[iThisCamShot])
		RETURN "DLC_MPSUM2_Drone_Sounds"
	ENDIF
	
	RETURN "MP_CCTV_SOUNDSET"
ENDFUNC

FUNC STRING GET_CAM_AUDIO_BG_LOOP_SOUND_NAME(INT iThisCutscene, INT iThisCamShot)
	IF IS_USING_CUTSCENE_DRONE_SCALEFORM(g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].eDroneUIPreset[iThisCamShot])
		RETURN "HUD_Loop"
	ENDIF
	
	RETURN "Background"
ENDFUNC

FUNC STRING GET_CAM_AUDIO_ZOOM_SOUND_SET(INT iThisCutscene, INT iThisCamShot)
	IF IS_USING_CUTSCENE_DRONE_SCALEFORM(g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].eDroneUIPreset[iThisCamShot])
		RETURN "DLC_MPSUM2_Drone_Sounds"
	ENDIF
	
	RETURN "MP_CCTV_SOUNDSET"
ENDFUNC

FUNC STRING GET_CAM_AUDIO_ZOOM_SOUND_NAME(INT iThisCutscene, INT iThisCamShot)
	IF IS_USING_CUTSCENE_DRONE_SCALEFORM(g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].eDroneUIPreset[iThisCamShot])
		RETURN "HUD_Zoom_Loop"
	ENDIF
	
	RETURN "Zoom"
ENDFUNC

FUNC STRING GET_CAM_AUDIO_PAN_SOUND_SET(INT iThisCutscene, INT iThisCamShot)
	UNUSED_PARAMETER(iThisCutscene)
	UNUSED_PARAMETER(iThisCamShot)
	RETURN "MP_CCTV_SOUNDSET"
ENDFUNC

FUNC STRING GET_CAM_AUDIO_PAN_SOUND_NAME(INT iThisCutscene, INT iThisCamShot)
	UNUSED_PARAMETER(iThisCutscene)
	UNUSED_PARAMETER(iThisCamShot)
	RETURN "Pan"
ENDFUNC

FUNC STRING GET_CAM_AUDIO_CHANGE_CAM_SOUND_SET(INT iThisCutscene, INT iThisCamShot)
	IF IS_USING_CUTSCENE_DRONE_SCALEFORM(g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].eDroneUIPreset[iThisCamShot])
		RETURN "DLC_MPSUM2_Drone_Sounds"
	ENDIF
	
	RETURN "MP_CCTV_SOUNDSET"
ENDFUNC

FUNC STRING GET_CAM_AUDIO_CHANGE_CAM_SOUND_NAME(INT iThisCutscene, INT iThisCamShot)
	IF IS_USING_CUTSCENE_DRONE_SCALEFORM(g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].eDroneUIPreset[iThisCamShot])
		RETURN "HUD_Change_Cam"
	ENDIF
	
	RETURN "Change_Cam"
ENDFUNC

PROC MANAGE_CAM_AUDIO(INT iThisCutscene, INT iThisCamShot, BOOL bManualControl)
		
	IF eLiftState > eLMS_WaitForStart
		PRINTLN("[MANAGE_CAM_AUDIO] - PLAYING LIFT CUTSCENE MOVEMENT. BLOCKING CCTV SOUNDS")
		EXIT
	ENDIF
	
	IF iSoundIDCamBackground = -1
		iSoundIDCamBackground = GET_SOUND_ID()
	ENDIF
	IF iSoundPan = -1
		iSoundPan = GET_SOUND_ID()
	ENDIF
	IF iSoundZoom = -1
		iSoundZoom = GET_SOUND_ID()
	ENDIF
	IF iCamSound = -1
		iCamSound = GET_SOUND_ID()
	ENDIF
	
	IF NOT IS_BIT_SET(iCamSoundBitSet,1)
		IF NOT IS_GAMEPLAY_CAM_RENDERING()
			PLAY_SOUND_FRONTEND(iSoundIDCamBackground, GET_CAM_AUDIO_BG_LOOP_SOUND_NAME(iThisCutscene, iThisCamShot), GET_CAM_AUDIO_BG_LOOP_SOUND_SET(iThisCutscene, iThisCamShot), FALSE)
			START_AUDIO_SCENE("DLC_HEIST_BANK_CCTV_SCENE") 
			SET_BIT(iCamSoundBitSet,1)
			PRINTLN("[MANAGE_CAM_AUDIO] - PLAYING BACKGROUND SOUND")
		ENDIF
	ENDIF
	
	IF bManualControl
		IF NOT IS_BIT_SET(iCamSoundBitSet,2)
			IF fXMove != 0
				IF HAS_SOUND_FINISHED(iSoundPan)
				OR iSoundPan = -1
					IF NOT IS_GAMEPLAY_CAM_RENDERING()
						IF NOT IS_VECTOR_ZERO(vCCTVRot[iThisCamShot])
							PLAY_SOUND_FRONTEND(iSoundPan, GET_CAM_AUDIO_PAN_SOUND_NAME(iThisCutscene, iThisCamShot), GET_CAM_AUDIO_PAN_SOUND_SET(iThisCutscene, iThisCamShot), FALSE)
							PRINTLN("[MANAGE_CAM_AUDIO] - PLAYING PAN")
							SET_BIT(iCamSoundBitSet,2)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF fXMove = 0
				STOP_SOUND(iSoundPan)
				PRINTLN("[MANAGE_CAM_AUDIO] - STOPPING PAN")
				CLEAR_BIT(iCamSoundBitSet,2)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iCamSoundBitSet,2)
			IF NOT ARE_VECTORS_EQUAL(g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].vCamStartRot[iThisCamShot],g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].vCamEndRot[iThisCamShot])
				IF HAS_SOUND_FINISHED(iSoundPan)
				OR iSoundPan = -1
					IF NOT IS_GAMEPLAY_CAM_RENDERING()
						PLAY_SOUND_FRONTEND(iSoundPan, GET_CAM_AUDIO_PAN_SOUND_NAME(iThisCutscene, iThisCamShot), GET_CAM_AUDIO_PAN_SOUND_SET(iThisCutscene, iThisCamShot), FALSE)
						PRINTLN("[MANAGE_CAM_AUDIO] - PLAYING PAN")
						SET_BIT(iCamSoundBitSet,2)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF ARE_VECTORS_EQUAL(g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].vCamStartRot[iThisCamShot],g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].vCamEndRot[iThisCamShot])
				STOP_SOUND(iSoundPan)
				PRINTLN("[MANAGE_CAM_AUDIO] - STOPPING PAN")
				CLEAR_BIT(iCamSoundBitSet,2)
			ENDIF
		ENDIF
	ENDIF
	
	
	IF bManualControl
		IF NOT IS_BIT_SET(iCamSoundBitSet,3)
			IF fYMove != 0
				IF HAS_SOUND_FINISHED(iSoundZoom)
				OR iSoundZoom = -1
					IF NOT IS_GAMEPLAY_CAM_RENDERING()
						PLAY_SOUND_FRONTEND(iSoundZoom, GET_CAM_AUDIO_ZOOM_SOUND_NAME(iThisCutscene, iThisCamShot), GET_CAM_AUDIO_ZOOM_SOUND_SET(iThisCutscene, iThisCamShot), FALSE)
						PRINTLN("[MANAGE_CAM_AUDIO] - PLAYING ZOOM")
						SET_BIT(iCamSoundBitSet,3)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF fYMove = 0
				STOP_SOUND(iSoundZoom)
				CLEAR_BIT(iCamSoundBitSet,3)
				PRINTLN("[MANAGE_CAM_AUDIO] - STOPPING ZOOM")
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iCamSoundBitSet,3)
			IF g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fStartCamFOV[iThisCamShot] != g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fEndCamFOV[iThisCamShot]
				IF HAS_SOUND_FINISHED(iSoundZoom)
				OR iSoundZoom = -1
					IF NOT IS_GAMEPLAY_CAM_RENDERING()
						PLAY_SOUND_FRONTEND(iSoundZoom, GET_CAM_AUDIO_ZOOM_SOUND_NAME(iThisCutscene, iThisCamShot), GET_CAM_AUDIO_ZOOM_SOUND_SET(iThisCutscene, iThisCamShot), FALSE)
						PRINTLN("[MANAGE_CAM_AUDIO] - PLAYING ZOOM")
						SET_BIT(iCamSoundBitSet,3)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fStartCamFOV[iThisCamShot] = g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fEndCamFOV[iThisCamShot]
				STOP_SOUND(iSoundZoom)
				CLEAR_BIT(iCamSoundBitSet,3)
				PRINTLN("[MANAGE_CAM_AUDIO] - STOPPING ZOOM")
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iCamSoundBitSet,4)
		IF iStoreLastCam != iThisCamShot
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].iCutsceneShotBitSet[iThisCamShot],ciCSS_BS_NewShot)
			OR IS_USING_CUTSCENE_DRONE_SCALEFORM(g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].eDroneUIPreset[iThisCamShot])
				IF HAS_SOUND_FINISHED(iCamSound)
				OR iCamSound = -1
					IF NOT IS_GAMEPLAY_CAM_RENDERING()
						PLAY_SOUND_FRONTEND(iCamSound, GET_CAM_AUDIO_CHANGE_CAM_SOUND_NAME(iThisCutscene, iThisCamShot), GET_CAM_AUDIO_CHANGE_CAM_SOUND_SET(iThisCutscene, iThisCamShot),  FALSE)
						SET_BIT(iCamSoundBitSet,4)
						iLocalCamSoundTimer = GET_GAME_TIMER()
						PRINTLN("[MANAGE_CAM_AUDIO] - PLAYING CHANGE CAM")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF CHECK_IF_TIME_PASSED_IN_HAS_EXPIRED(iLocalCamSoundTimer,100)
			STOP_SOUND(iCamSound)
			PRINTLN("[MANAGE_CAM_AUDIO] - STOPPING CHANGE CAM")
			CLEAR_BIT(iCamSoundBitSet,4)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].iCutsceneShotBitSet[iThisCamShot], ciCSS_BS_PlayCutsceneSFX_DroneSounds)
				PLAY_FAKE_FLIGHT_LOOP_SOUND(GET_CAM_COORD(CutsceneCam))	
			ENDIF
		ENDIF
	ENDIF

	iStoreLastCam = iThisCamShot
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cutscene Shared  ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: These functions are for checking and registering entities for a cutscene.									 							 ---------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
PROC SET_PROCESS_SCRIPTED_CUTSCENE_END_WARP_STATE(SCRIPTED_CUTSCENE_END_WARP_LOCAL_STATE eState)
	IF sScriptedCutsceneEndWarp.eState = eState
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINTLN("SET_PROCESS_SCRIPTED_CUTSCENE_END_WARP_STATE - Changing state: ", GET_SCRIPTED_CUTSCENE_END_WARP_STATE_NAME(sScriptedCutsceneEndWarp.eState), " -> ", GET_SCRIPTED_CUTSCENE_END_WARP_STATE_NAME(eState))
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	sScriptedCutsceneEndWarp.eState = eState
ENDPROC

FUNC BOOL PROCESS_SCRIPTED_CUTSCENE_END_WARP(INT iCutscene, INT iPlayerCutID, VECTOR vEndPos, FLOAT fEndPos, BOOL bQuickWarp)
	
	IF bIsAnySpectator
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
	OR IS_BIT_SET(iLocalBoolCheck3, LBOOL3_LATE_SPECTATE)
	OR IS_BIT_SET(iLocalBoolCheck3, LBOOL3_WAIT_END_SPECTATE)
		// Bail and finish for spectators
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - iCutscene: ", iCutscene, " iPlayerCutID: ", iPlayerCutID, " - IS_LOCAL_PLAYER_ANY_SPECTATOR = TRUE, Returning TRUE")
		RETURN TRUE
	ENDIF
		
	IF sScriptedCutsceneEndWarp.eState = SCUT_EWLS_INIT
	
		IF bIsLocalPlayerHost		
			IF MC_ServerBD_1.iScriptedCutsceneSyncScene > -1
				NETWORK_STOP_SYNCHRONISED_SCENE(MC_ServerBD_1.iScriptedCutsceneSyncScene)
				MC_ServerBD_1.iScriptedCutsceneSyncScene = -1
				PRINTLN("[LM][Cutscene_Scripted][ScriptedAnimSyncScene] - PROCESS_SCRIPTED_CUTSCENE - Cleaned up sync scene as we are warping (host)...")
			ENDIF			
		ENDIF	
		
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_STOP()
		ENDIF
		
		// set the end pos
		sScriptedCutsceneEndWarp.vWarpCoord = vEndPos
		sScriptedCutsceneEndWarp.fWarpHeading = fEndPos
		
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - 	(before)	vEndPos: ", vEndPos, " fEndPos: ", fEndPos)
		
		// check the end target (this may overwrite the end pos to be that of the vehicle if the existing end pos was invalid)
		GET_SCRIPTED_CUTSCENE_END_WARP_TARGET(iCutscene, iPlayerCutID, 
			sScriptedCutsceneEndWarp.vWarpCoord,
			sScriptedCutsceneEndWarp.fWarpHeading,
			sScriptedCutsceneEndWarp.vehTarget)
			
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - iCutscene: ", iCutscene, " iPlayerCutID: ", iPlayerCutID, " - Initialized end target")
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - 		vWarpCoord: ", sScriptedCutsceneEndWarp.vWarpCoord)
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - 		fWarpHeading: ", sScriptedCutsceneEndWarp.fWarpHeading)
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - 		vehTarget: ", NATIVE_TO_INT(sScriptedCutsceneEndWarp.vehTarget))

		// If we have a vehicle move it
		IF NOT IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_DoNotWarpToEndCoords)
		AND DOES_ENTITY_EXIST(sScriptedCutsceneEndWarp.vehTarget)
		AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(NETWORK_GET_NETWORK_ID_FROM_ENTITY(sScriptedCutsceneEndWarp.vehTarget))
			PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - iCutscene: ", iCutscene, " iPlayerCutID: ", iPlayerCutID, " - Target vehicle found, moving to end coords")
			SET_ENTITY_COORDS(sScriptedCutsceneEndWarp.vehTarget, sScriptedCutsceneEndWarp.vWarpCoord)
			SET_ENTITY_HEADING(sScriptedCutsceneEndWarp.vehTarget, sScriptedCutsceneEndWarp.fWarpHeading)
			SET_VEHICLE_ON_GROUND_PROPERLY(sScriptedCutsceneEndWarp.vehTarget)
		ENDIF
		
		// cache the current seat case the net warp removes them from the seat and we need this info later
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			sScriptedCutsceneEndWarp.eCurrentSeat = GET_SEAT_PED_IS_IN(LocalPlayerPed)
			PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - iCutscene: ", iCutscene, " iPlayerCutID: ", iPlayerCutID, " - player currently in a vehicle, setting seat: ", sScriptedCutsceneEndWarp.eCurrentSeat)
		ENDIF
		
		SET_PROCESS_SCRIPTED_CUTSCENE_END_WARP_STATE(SCUT_EWLS_WARP)
	ENDIF
	
	IF sScriptedCutsceneEndWarp.eState = SCUT_EWLS_WARP
	
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - SCUT_EWLS_WARP")
		
		// WARP
		IF NOT IS_VECTOR_ZERO(sScriptedCutsceneEndWarp.vWarpCoord)
		AND NOT IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_DoNotWarpToEndCoords)
			
			BOOL bInVehicle = IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			BOOL bKeepVehicle
			IF NOT IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_RemoveFromVehEnd)
				bKeepVehicle = TRUE
			ENDIF
			
			IF sScriptedCutsceneEndWarp.vehTarget != NULL	
			
				IF bInVehicle
				AND GET_VEHICLE_PED_IS_IN(LocalPlayerPed) = sScriptedCutsceneEndWarp.vehTarget
					bKeepVehicle = TRUE
				ELSE
					bKeepVehicle = FALSE
				ENDIF
				
				IF bInVehicle
				AND sScriptedCutsceneEndWarp.vehTarget = NULL
				AND NOT IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_RemoveFromVehEnd)
					bKeepVehicle = TRUE
				ENDIF
				
			ENDIF
					
			PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - iCutscene: ", iCutscene, " bKeepVehicle: ", bKeepVehicle, " InVehicle: ", bInVehicle)
						
			IF NOT NET_WARP_TO_COORD(
				sScriptedCutsceneEndWarp.vWarpCoord, 
				sScriptedCutsceneEndWarp.fWarpHeading, bKeepVehicle, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bQuickWarp)
				RETURN FALSE
			ENDIF
			
		ENDIF
		
		IF eCSLift != eCSLift_None
		AND IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitset2, ci_CSBS2_Scripted_UsePlayerClones)
		AND ENUM_TO_INT(eLiftState) > ENUM_TO_INT(eLMS_AttachClones)
		AND g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].fLiftZOffset_Start != 0
			PRINTLN("[Cutscenes][CSLift] PROCESS_SCRIPTED_CUTSCENE_END_WARP - Unfreeze the player now")
			FREEZE_ENTITY_POSITION(LocalPlayerPed, FALSE)
		ENDIF
		
		// POST WARP - Need to place in target vehicle
		IF CUTSCENE_WARP_PED_TO_VEHICLE_COMMON(LocalPlayerPed, g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sPlayerEndWarp[iPlayerCutID], sScriptedCutsceneEndWarp.eCurrentSeat)
		AND sScriptedCutsceneEndWarp.vehTarget != NULL
			IF SHOULD_PLAYER_DO_ROLLING_START_FOR_CUTSCENE(FMMCCUT_SCRIPTED, iCutscene)
				// B*4238761: only go to SCUT_EWLS_WAIT_FOR_VEHICLE if we're going to need a rolling start
				// otherwise this will cause a delay for situations where we don't care about passing the ped in vehicle check
				SET_PROCESS_SCRIPTED_CUTSCENE_END_WARP_STATE(SCUT_EWLS_WAIT_FOR_VEHICLE)
			ELIF IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_SyncAllPlayersAtEnd)
				// This avoids issues where players are not position synced and will sometimes appear on top of vehicles.
				SET_PROCESS_SCRIPTED_CUTSCENE_END_WARP_STATE(SCUT_EWLS_WAIT_FOR_VEHICLE)
			ELSE
				SET_PROCESS_SCRIPTED_CUTSCENE_END_WARP_STATE(SCUT_EWLS_END)
			ENDIF
		ELIF GET_INTERIOR_AT_COORDS(GET_ENTITY_COORDS(LocalPlayerPed)) != NULL
			SET_PROCESS_SCRIPTED_CUTSCENE_END_WARP_STATE(SCUT_EWLS_WAIT_FOR_INTERIOR)
		ELSE
			SET_PROCESS_SCRIPTED_CUTSCENE_END_WARP_STATE(SCUT_EWLS_END)
		ENDIF
	
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - SCUT_EWLS_WARP END")
	ENDIF
	
	IF sScriptedCutsceneEndWarp.eState = SCUT_EWLS_WAIT_FOR_VEHICLE
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			SET_PROCESS_SCRIPTED_CUTSCENE_END_WARP_STATE(SCUT_EWLS_END)
		ELSE
			PRINTLN("[Cutscenes][CSLift] PROCESS_SCRIPTED_CUTSCENE_END_WARP - Ped not in Vehicle. Waiting...")
		ENDIF		
		
		IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
		AND GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
		AND GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
			PRINTLN("[Cutscenes][CSLift] PROCESS_SCRIPTED_CUTSCENE_END_WARP - Ped not in Vehicle. We also don't have a task, escaping limbo, attempting again.")
			SET_PROCESS_SCRIPTED_CUTSCENE_END_WARP_STATE(SCUT_EWLS_WARP)
		ENDIF
	ENDIF
	
	IF sScriptedCutsceneEndWarp.eState = SCUT_EWLS_WAIT_FOR_INTERIOR
		
		// We'll want an option to ensure that Simple Interior scripts have properly loaded before we move on from this part of the script. SIMPLE_INTERIOR_CASINO being used in the param should be a variable.
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableCasino_InteriorScripts)
			IF NOT g_bInitCasinoPedsCreated
			AND IS_PLAYER_WITHIN_SIMPLE_INTERIOR_BOUNDS(SIMPLE_INTERIOR_CASINO)
				IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_REQUEST_CASINO_INTERIOR_SCRIPT)
						REQUEST_RELAUCHING_OF_SIMPLE_INTERIOR_INT_SCRIPT(SIMPLE_INTERIOR_CASINO)
						SET_BIT(iLocalBoolCheck31, LBOOL31_REQUEST_CASINO_INTERIOR_SCRIPT)
						PRINTLN("[Cutscenes]PROCESS_SCRIPTED_CUTSCENE_END_WARP - REQUEST_RELAUCHING_OF_SIMPLE_INTERIOR_INT_SCRIPT(SIMPLE_INTERIOR_CASINO), LBOOL31_REQUEST_CASINO_INTERIOR_SCRIPT SET")
					ENDIF
				
				PRINTLN("[Cutscenes]PROCESS_SCRIPTED_CUTSCENE_END_WARP - waiting for the casino peds to be created")
				RETURN FALSE
			ENDIF
		ENDIF
		
		SET_PROCESS_SCRIPTED_CUTSCENE_END_WARP_STATE(SCUT_EWLS_END)		
	ENDIF
	
	IF sScriptedCutsceneEndWarp.eState = SCUT_EWLS_END	
	
		DO_POST_CUTSCENE_COMMON_PLAYER_EXIT(iCutscene, FMMCCUT_SCRIPTED, iPlayerCutID)
		
		SCRIPTED_CUTSCENE_END_WARP_LOCAL sEmpty
		
		sScriptedCutsceneEndWarp = sEmpty
		
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - iCutscene: ", iCutscene, " iPlayerCutID: ", iPlayerCutID, " - COMPLETE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL SHOULD_WARP_PLAYER_TO_END_POSITION_DURING_CUTSCENE(INT iCutsceneToUse, FMMC_CUTSCENE_TYPE eCutType)

	SWITCH eCutType
		CASE FMMCCUT_MOCAP
			IF iCutsceneToUse >= -1
			AND iCutsceneToUse < MAX_MOCAP_CUTSCENES
			AND IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset3, ci_CSBS3_WarpPlayerToEndPositionDuringCutscene)				
				RETURN TRUE
			ENDIF
		BREAK
		CASE FMMCCUT_SCRIPTED
			IF iCutsceneToUse >= -1
			AND iCutsceneToUse < _POST_CASINO_HEIST__MAX_SCRIPTED_CUTSCENES
			AND IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset3, ci_CSBS3_WarpPlayerToEndPositionDuringCutscene)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_PUT_PLAYER_IN_COVER_DURING_CUTSCENE(INT iCutsceneToUse, FMMC_CUTSCENE_TYPE eCutType)

	SWITCH eCutType
		CASE FMMCCUT_MOCAP
			IF iCutsceneToUse >= -1
			AND iCutsceneToUse < MAX_MOCAP_CUTSCENES
			AND IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset3, ci_CSBS3_PutPlayerIntoCoverDuringCutscene)					
				RETURN TRUE
			ENDIF
		BREAK
		CASE FMMCCUT_SCRIPTED
			IF iCutsceneToUse >= -1
			AND iCutsceneToUse < _POST_CASINO_HEIST__MAX_SCRIPTED_CUTSCENES
			AND IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset3, ci_CSBS3_PutPlayerIntoCoverDuringCutscene)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_CUTSCENE_NEARLY_FINISHED(FMMC_CUTSCENE_TYPE eCutType)

	SWITCH eCutType
		CASE FMMCCUT_MOCAP			
			INT iNearlyFinishedTime
			iNearlyFinishedTime = GET_CUTSCENE_END_TIME() - 4000
			IF GET_CUTSCENE_TIME() > iNearlyFinishedTime
				RETURN TRUE
			ELSE
				PRINTLN("[Cutscenes] IS_CUTSCENE_NEARLY_FINISHED - FALSE. GET_CUTSCENE_TIME: ", GET_CUTSCENE_TIME(), " iNearlyFinishedTime: ", iNearlyFinishedTime)	
			ENDIF
		BREAK
		CASE FMMCCUT_SCRIPTED
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_CUTSCENE_FUNCTIONALITY_NEARLY_FINISHED(INT iCutsceneToUse, FMMC_CUTSCENE_TYPE eCutType, VECTOR vEndPos, FLOAT fEndPos, INT iPlayerIndex = -1)
	
	IF NOT IS_CUTSCENE_NEARLY_FINISHED(eCutType)
		PRINTLN("[Cutscenes] PROCESS_CUTSCENE_FUNCTIONALITY_NEARLY_FINISHED - ci_CSBS3_WarpPlayerToEndPositionDuringCutscene = TRUE")	
	ENDIF
	
	IF SHOULD_WARP_PLAYER_TO_END_POSITION_DURING_CUTSCENE(iCutsceneToUse, eCutType)
		PRINTLN("[Cutscenes] PROCESS_CUTSCENE_FUNCTIONALITY_NEARLY_FINISHED - SHOULD_WARP_PLAYER_TO_END_POSITION_DURING_CUTSCENE = TRUE")
		IF eCutType = FMMCCUT_SCRIPTED
			IF PROCESS_SCRIPTED_CUTSCENE_END_WARP(iCutsceneToUse, iPlayerIndex, vEndPos, fEndPos, TRUE)
				IF CAN_CUTSCENE_RESET_GAMEPLAY_CAMERA_PITCH_OR_HEADING(FMMCCUT_SCRIPTED, iCutsceneToUse)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()	
				ENDIF
			ELSE
				PRINTLN("[Cutscenes] PROCESS_CUTSCENE_FUNCTIONALITY_NEARLY_FINISHED - Waiting for PROCESS_SCRIPTED_CUTSCENE_END_WARP")	
				RETURN FALSE
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_MovedPlayerPedDuringCutscene)
				PRINTLN("[Cutscenes] PROCESS_CUTSCENE_FUNCTIONALITY_NEARLY_FINISHED - ci_CSBS3_WarpPlayerToEndPositionDuringCutscene = TRUE")	
				SET_ENTITY_COORDS(localPlayerPed, vEndPos)
				SET_ENTITY_HEADING(localPlayerPed, fEndPos)
				DO_POST_CUTSCENE_COMMON_PLAYER_EXIT(iCutsceneToUse, FMMCCUT_MOCAP, iPlayerIndex)
				SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_MovedPlayerPedDuringCutscene)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cutscene Entities  ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: These functions are for checking and registering entities for a cutscene.									 							 ---------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//Set data of a particular weapon, INT iPlayer is the position in the array you want the reciever to use. 
PROC SEND_WEAPON_DATA_TO_REMOTE_PLAYERS(WEAPON_TYPE wtWeapon)	
	
	IF IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_SentWeaponData)
		EXIT
	ENDIF
	
	BROADCAST_EVENT_FMMC_MY_WEAPON_DATA(wtWeapon)
	
	SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_SentWeaponData)
	
ENDPROC

FUNC WEAPON_TYPE GET_WEAPON_TYPE_FOR_MOCAP_SEND_LOCAL_PLAYER_DATA(STRING sMocapCutsceneName)
	
	IF ARE_STRINGS_EQUAL(sMocapCutsceneName, "FIX_TRIP3_MCS1")	
		RETURN WEAPONTYPE_HEAVYSNIPER
	ENDIF
	
	RETURN WEAPONTYPE_INVALID
ENDFUNC

FUNC BOOL SHOULD_MOCAP_SEND_LOCAL_PLAYER_DATA(STRING sMocapCutsceneName)
	
	IF ARE_STRINGS_EQUAL(sMocapCutsceneName, "FIX_TRIP3_MCS1")
		// Only Franklin has a weapon here.
		IF MC_PlayerBD[iLocalPart].iTeam = 0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_TEAM_CUTSCENE_PLAYERS_SELECTED(INT iteam)

	SWITCH iteam
		CASE 0
			IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_CUTSCENE_TEAM0_PLAYERS_SELECTED)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 1
			IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_CUTSCENE_TEAM1_PLAYERS_SELECTED)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 2
			IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_CUTSCENE_TEAM2_PLAYERS_SELECTED)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 3
			IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_CUTSCENE_TEAM3_PLAYERS_SELECTED)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC INT GET_MOCAP_NUMBER_OF_PLAYERS(STRING sCutscenestring)
		
	IF IS_STRING_EMPTY(Scutscenestring)	
		PRINTLN("[Cutscenes] - GET_MOCAP_NUMBER_OF_PLAYERS - STRING is null or empty.")
		RETURN 0
	ENDIF
	
	IF ARE_STRINGS_EQUAL("TUNF_UNI_SWAT", Scutscenestring)
		RETURN 0
	ENDIF
	
	IF ARE_STRINGS_EQUAL("tunf_drp_avi", Scutscenestring)
		RETURN 1
	ENDIF
	
	INT iPlayerCount
	TEXT_LABEL_15 tl15
	INT i = 0 
	FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
		tl15 = "MP_"
		tl15 += i+1
		CUTSCENE_HANDLE_SEARCH_RESULT eDoesCutHandleExit = DOES_CUTSCENE_HANDLE_EXIST(tl15)
		
		IF eDoesCutHandleExit = CHSR_HANDLE_EXISTS
			PRINTLN("[Cutscenes] - GET_MOCAP_NUMBER_OF_PLAYERS - Handle: ", tl15, " Exists")
			iPlayerCount++
		ENDIF
	ENDFOR
		
	PRINTLN("[Cutscenes] - GET_MOCAP_NUMBER_OF_PLAYERS - Returning Player Count ", iPlayerCount, " for mocap: ", sCutscenestring)
	
	RETURN iPlayerCount
		
ENDFUNC

FUNC BOOL DOES_MC_CUTSCENE_HAVE_A_MODIFIED_PLAYER_NUMBER(STRING sMocapCutscenePassed)
	
	IF ARE_STRINGS_EQUAL(sMocapCutscenePassed, "HS4F_TRO_ENT")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_MC_CUTSCENE_MODIFIED_PLAYER_NUMBER(STRING sMocapCutscenePassed, INT iPlayerNumber)
	
	PRINTLN("[Cutscene_Mocap][GET_CUTSCENE_PLAYER_PREFIX_INT] - Checking sMocapCutscenePassed: ", sMocapCutscenePassed)
	
	IF ARE_STRINGS_EQUAL(sMocapCutscenePassed, "HS4F_TRO_ENT")

		PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iPlayerNumber)
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
			PRINTLN("[Cutscene_Mocap][GET_CUTSCENE_PLAYER_PREFIX_INT] - iPlayerNumber: ", iPlayerNumber, " part is not active.")
			RETURN iPlayerNumber
		ENDIF
		
		PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
				
		PED_INDEX pedPlayer = GET_PLAYER_PED(piPlayer)
		IF IS_PED_INJURED(pedPlayer)
			PRINTLN("[Cutscene_Mocap][GET_CUTSCENE_PLAYER_PREFIX_INT] - iPlayerNumber: ", iPlayerNumber, " Ped is Injured.")
			RETURN iPlayerNumber
		ENDIF
	
		IF NOT IS_PED_IN_ANY_VEHICLE(pedPlayer)
			PRINTLN("[Cutscene_Mocap][GET_CUTSCENE_PLAYER_PREFIX_INT] - iPlayerNumber: ", iPlayerNumber, " Not in a vehicle.")
			RETURN iPlayerNumber
		ENDIF
		
		VEHICLE_INDEX vehPedIsIn = GET_VEHICLE_PED_IS_IN(pedPlayer)
		
		IF NOT IS_VEHICLE_DRIVEABLE(vehPedIsIn)				
			PRINTLN("[Cutscene_Mocap][GET_CUTSCENE_PLAYER_PREFIX_INT] - iPlayerNumber: ", iPlayerNumber, " Vehicle is not driveable")
			RETURN iPlayerNumber
		ENDIF
		
		VEHICLE_SEAT vs_Seat = GET_SEAT_PED_IS_IN(pedPlayer)
		
		PRINTLN("[Cutscene_Mocap][GET_CUTSCENE_PLAYER_PREFIX_INT] - iPlayerNumber: ", iPlayerNumber, " We are in vs_Seat: ", ENUM_TO_INT(vs_Seat))
		
		SWITCH vs_Seat
			CASE VS_DRIVER
				iPlayerNumber = 0
			BREAK						
			CASE VS_FRONT_RIGHT
				iPlayerNumber = 1
			BREAK	
			CASE VS_EXTRA_LEFT_3
				iPlayerNumber = 3
			BREAK						
			CASE VS_EXTRA_RIGHT_2
				iPlayerNumber = 4
			BREAK						
			CASE VS_EXTRA_RIGHT_3
				iPlayerNumber = 2
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN iPlayerNumber
ENDFUNC

FUNC INT GET_CUTSCENE_PLAYER_PREFIX_INT(INT iPlayerNum, BOOL bNoModification = FALSE)
	
	PRINTLN("[Cutscene_Mocap][GET_CUTSCENE_PLAYER_PREFIX_INT] - iPlayerNum: ", iPlayerNum)
	
	IF NOT bNoModification
	AND DOES_MC_CUTSCENE_HAVE_A_MODIFIED_PLAYER_NUMBER(sMocapCutscene)				
		iPlayerNum = GET_MC_CUTSCENE_MODIFIED_PLAYER_NUMBER(sMocapCutscene, iPlayerNum)
		PRINTLN("[Cutscene_Mocap][GET_CUTSCENE_PLAYER_PREFIX_INT] - Using GET_MC_CUTSCENE_MODIFIED_PLAYER_NUMBER... iPlayerNum changed to: ", iPlayerNum)
	ENDIF
	
	RETURN iPlayerNum
ENDFUNC

FUNC TEXT_LABEL_23 GET_CUTSCENE_PLAYER_PREFIX(INT iTempPlayerNum, BOOL bNoModification = FALSE)
	
	INT iplayerNum = GET_CUTSCENE_PLAYER_PREFIX_INT(iTempPlayerNum, bNoModification)
	
	//handles start at 1 not 0.
	iplayerNum++
	
	TEXT_LABEL_23 sreturn
	sreturn = "MP_"	
	sreturn += iplayerNum
	
	
	PRINTLN("[Cutscene_Mocap][GET_CUTSCENE_PLAYER_PREFIX] - returning ", sreturn)
	RETURN sreturn
ENDFUNC

PROC SET_PLAYER_COMPONENTS_FOR_CUTSCENE(INT iplayer)

	TEXT_LABEL_23 sPlayerString

	sPlayerString = GET_CUTSCENE_PLAYER_PREFIX(iplayer, TRUE)

	SET_CUTSCENE_ENTITY_STREAMING_FLAGS(sPlayerString, DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
	PRINTLN("[Cutscene_Mocap][SET_PLAYER_COMPONENTS_FOR_CUTSCENE] - Registering player for component variations for cutscene: ", iplayer, " handle: ", sPlayerString)

ENDPROC

PROC SETUP_CUTSCENE_PLAYERS_COMPONENTS()

	SET_PLAYER_COMPONENTS_FOR_CUTSCENE(0)
	SET_PLAYER_COMPONENTS_FOR_CUTSCENE(1)
	SET_PLAYER_COMPONENTS_FOR_CUTSCENE(2)
	SET_PLAYER_COMPONENTS_FOR_CUTSCENE(3)
	SET_PLAYER_COMPONENTS_FOR_CUTSCENE(4) // some cutscenes have MP_5 as a variation.
	
ENDPROC

/// PURPOSE: returns true if the player was registered for the cutscene by the server. 
FUNC BOOL IS_PLAYER_REGISTERED_FOR_CUTSCENE(INT iCutsceneTeam)
					
	INT iPlayer 
	FOR iPlayer = 0 to FMMC_MAX_CUTSCENE_PLAYERS - 1
	
		IF MC_ServerBD.piCutscenePlayerIDs[iCutsceneTeam][iPlayer] = LocalPlayer
			RETURN TRUE
		ENDIF
		
	ENDFOR 

	RETURN FALSE
ENDFUNC

FUNC BOOL REMOVE_CUTSCENE_PLAYER(INT iplayer)

	IF iPlayer < 0
	OR iPlayer >= FMMC_MAX_CUTSCENE_PLAYERS
		ASSERTLN("REMOVE_CUTSCENE_PLAYER_PROPS - iPlayer: ", iPlayer, " is out of range")
		RETURN FALSE
	ENDIF

	TEXT_LABEL_23 sPlayerString
	sPlayerString = GET_CUTSCENE_PLAYER_PREFIX(iplayer)
	
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		PASS_OVER_CUT_SCENE_PED(NULL, sPlayerString)
	ELSE
		IF NOT IS_STRING_NULL_OR_EMPTY(sPlayerString)
			PRINTLN("[Cutscenes] [PED CUT] * REGISTER_ENTITY_FOR_CUTSCENE(NULL, ", sPlayerString, ", CU_DONT_ANIMATE_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)")
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, sPlayerString, CU_DONT_ANIMATE_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)
			
			PRINTLN("Removing Player from cutscene sPlayerString: ", sPlayerString, " iplayer: ", iplayer)			
		ELSE
			PRINTLN("Player not in the Cutscene anyway. sPlayerString: ", sPlayerString, " iplayer: ", iplayer)
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL REMOVE_PLAYER_CUTSCENE_HANDLE(INT iHandle)
	TEXT_LABEL_23 sPlayerString
	sPlayerString = "MP_"
	sPlayerString += iHandle+1 // cutscene handles start at 1 not 0 for players.
	
	PRINTLN("[Cutscenes] [PED CUT] * REMOVE_PLAYER_CUTSCENE_HANDLE - iHandle: ", iHandle, " sPlayerString: ", sPlayerString)
	
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()		
		PASS_OVER_CUT_SCENE_PED(NULL, sPlayerString)
	ELSE
		PRINTLN("[Cutscenes] [PED CUT] * REMOVE_PLAYER_CUTSCENE_HANDLE * REGISTER_ENTITY_FOR_CUTSCENE(NULL, ", sPlayerString, ", CU_DONT_ANIMATE_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME) (REMOVE_CUTSCENE_HANDLE)")
		REGISTER_ENTITY_FOR_CUTSCENE(NULL, sPlayerString, CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL REMOVE_CUTSCENE_PLAYER_AND_PROPS(INT iPlayer, TEXT_LABEL_23 &tlPropHandles[])

	IF NOT REMOVE_CUTSCENE_PLAYER(iPlayer)
		RETURN FALSE
	ENDIF

	// Associated player props
	INT i
	FOR i = 0 TO COUNT_OF(tlPropHandles)-1
		
		TEXT_LABEL_23 propHandle = tlPropHandles[i]
		
		IF IS_STRING_NULL_OR_EMPTY(propHandle)
			PRINTLN("[Cutscenes] REMOVE_CUTSCENE_PLAYER - propHandle is empty/null, skipping")
			RELOOP
		ENDIF
		
		IF DOES_CUTSCENE_HANDLE_EXIST(propHandle) != CHSR_HANDLE_EXISTS
			PRINTLN("[Cutscenes] REMOVE_CUTSCENE_PLAYER - propHandle: ", propHandle," - does not exit, skipping")
			RELOOP
		ENDIF
	
		IF IS_A_STRAND_MISSION_BEING_INITIALISED()
			PRINTLN("[Cutscenes] REMOVE_CUTSCENE_PLAYER - propHandle: ", propHandle," - Prop removed (strand)")
			PASS_OVER_CUT_SCENE_OBJECT(NULL, propHandle)
		ELSE
			PRINTLN("[Cutscenes] REMOVE_CUTSCENE_PLAYER - propHandle: ", propHandle," - Prop removed")
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, propHandle, CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
		ENDIF
		
	ENDFOR
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_SCRIPTED_CUTSCENE_SKIP_ENTITY_REGISTRATION(INT iCutsceneToUse, SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)
		RETURN FALSE
	ENDIF
	
	IF eType = SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_STEAL_YOUGA_VAN
		//Registration is just to get reference to the van
		PRINTLN("[Cutscene_Scripted] SHOULD_SCRIPTED_CUTSCENE_SKIP_ENTITY_REGISTRATION - Skipping - SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_STEAL_YOUGA_VAN")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_CUTSCENE_HAVE_REGISTERED_ENTITIES(INT iCutscene, FMMC_CUTSCENE_TYPE cutType)

	INT i
	
	IF (cutType = FMMCCUT_MOCAP OR cutType = FMMCCUT_SCRIPTED)
	AND (iCutscene < 0 OR iCutScene >= PICK_INT(cutType = FMMCCUT_SCRIPTED, FMMC_GET_MAX_SCRIPTED_CUTSCENES(), MAX_MOCAP_CUTSCENES))
		ASSERTLN("[Cutscenes] DOES_CUTSCENE_HAVE_REGISTERED_ENTITIES - cutType: ", cutType, ", iCutscene: ", iCutscene, " - iCutscene is out of range for the given cutType")
		RETURN FALSE
	ENDIF

	SWITCH cutType
	
		CASE FMMCCUT_SCRIPTED 
			PRINTLN("[Cutscenes] DOES_CUTSCENE_HAVE_REGISTERED_ENTITIES - Checking Scripted Cutscene")
			
			FOR i = 0 TO (FMMC_MAX_CUTSCENE_ENTITIES-1)
				IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType !=-1
				AND g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex != -1
					PRINTLN("[Cutscenes] WE HAVE A VALID ENTITY IN THE LIST SCRIPTED CUTSCENE")
					RETURN TRUE
				ENDIF
			ENDFOR
			
			/* Only if we NEED it. Adding this here made Scripted Cutscenes hide any entities that were not registered to the cutscene. No Cutscenes need this YET but this was initially added here for consistency.
			FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps-1
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sWorldProps[i].iBitset, ciFMMC_WorldHiddenInCutscenes)
					CPRINTLN( DEBUG_CONTROLLER,"[Cutscenes] WE HAVE A VALID ENTITY VIA WORLD PROPS.")
					RETURN TRUE
				ENDIF
			ENDFOR*/
		BREAK
		
		CASE FMMCCUT_MOCAP
			PRINTLN("[Cutscenes] DOES_CUTSCENE_HAVE_REGISTERED_ENTITIES - Checking Mid Mission Mocap")
			
			FOR i = 0 TO (FMMC_MAX_CUTSCENE_ENTITIES-1)
				IF g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iType !=-1
				AND g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iIndex != -1
					CPRINTLN( DEBUG_CONTROLLER,"[Cutscenes] WE HAVE A VALID ENTITY IN THE LIST MID MOCAP")
					RETURN TRUE
				ENDIF	
			ENDFOR
		
			FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps-1
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sWorldProps[i].iBitset, ciFMMC_WorldHiddenInCutscenes)
					CPRINTLN( DEBUG_CONTROLLER,"[Cutscenes] WE HAVE A VALID ENTITY VIA WORLD PROPS.")
					RETURN TRUE
				ENDIF
			ENDFOR
		BREAK
		
		CASE FMMCCUT_ENDMOCAP
			PRINTLN("[Cutscenes] DOES_CUTSCENE_HAVE_REGISTERED_ENTITIES - Checking End Mocap")
			
			FOR i = 0 TO (FMMC_MAX_CUTSCENE_ENTITIES-1)				
				IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType !=-1
				AND g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex != -1
					CPRINTLN( DEBUG_CONTROLLER,"[Cutscenes] WE HAVE A VALID ENTITY IN THE LIST END MOCAP")
					RETURN TRUE
				ENDIF
			ENDFOR
			
			FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps-1
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sWorldProps[i].iBitset, ciFMMC_WorldHiddenInCutscenes)
					CPRINTLN( DEBUG_CONTROLLER,"[Cutscenes] WE HAVE A VALID ENTITY VIA WORLD PROPS.")
					RETURN TRUE
				ENDIF
			ENDFOR
		BREAK
	
	ENDSWITCH
	
	PRINTLN("[Cutscenes] DOES_CUTSCENE_HAVE_REGISTERED_ENTITIES - WE HAVE NO VALID ENTITIES IN THE LIST")
	RETURN FALSE
	
ENDFUNC

//Manually register specific entities for cutscenes that haven't been passed in from the creator
//e.g. world created object
PROC MANUALLY_REGISTER_SPECIFIC_ENTITIES(STRING sMocapName, FMMC_CUTSCENE_TYPE eCutType, INT iCutsceneToUse)

	PRINTLN("[Cutscene] - MANUALLY_REGISTER_SPECIFIC_ENTITIES iScriptedCutsceneTeam: ", iScriptedCutsceneTeam, " sMocapName: ", sMocapName)
		
	TEXT_LABEL_23 tl23_a
	TEXT_LABEL_23 tl23_b
	BOOL bStrandMission
	INT i = 0

	IF ARE_STRINGS_EQUAL(sMocapName, "HS3F_MUL_RP1")
		PRINTLN("[Cutscene] - MANUALLY_REGISTER_SPECIFIC_ENTITIES - HS3F_MUL_RP1 Running t his cutscene which requires specific entity registration (Ropes are not entities...)")
		INT iNumOfPlayers = GET_MOCAP_NUMBER_OF_PLAYERS(sMocapName)
		INT iCutsceneTeam
		IF iScriptedCutsceneTeam != -1
		AND eCutType != FMMCCUT_ENDMOCAP
			iCutsceneTeam = iScriptedCutsceneTeam
		ELSE
			iCutsceneTeam = MC_PlayerBD[iPartToUse].iTeam
		ENDIF
		
		bStrandMission = IS_A_STRAND_MISSION_BEING_INITIALISED()
		
		FOR i = 0 TO iNumOfPlayers-1
			IF MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][i] = INVALID_PLAYER_INDEX()
				tl23_a = "Player_Rope_"
				tl23_a += i+1
				tl23_b = "Player_Pulley_"
				tl23_b += i+1
				IF bStrandMission
					PASS_OVER_CUT_SCENE_OBJECT(NULL, tl23_a)
					PASS_OVER_CUT_SCENE_OBJECT(NULL, tl23_b)
				ELSE
					PRINTLN("[Cutscene] - MANUALLY_REGISTER_SPECIFIC_ENTITIES - HS3F_MUL_RP1 - Registering: ", tl23_a, " With NULL...")				
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, tl23_a, CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
					PRINTLN("[Cutscene] - MANUALLY_REGISTER_SPECIFIC_ENTITIES - HS3F_MUL_RP1 - Registering: ", tl23_b, " With NULL...")
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, tl23_b, CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapName, "HS3F_DIR_ENT")
		INT iNumOfPlayers = GET_MOCAP_NUMBER_OF_PLAYERS(sMocapName)
		INT iCutsceneTeam
		IF iScriptedCutsceneTeam != -1
		AND eCutType != FMMCCUT_ENDMOCAP
			iCutsceneTeam = iScriptedCutsceneTeam
		ELSE
			iCutsceneTeam = MC_PlayerBD[iPartToUse].iTeam
		ENDIF
		
		bStrandMission = IS_A_STRAND_MISSION_BEING_INITIALISED()

		FOR i = 0 TO iNumOfPlayers-1
			IF MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][i] = INVALID_PLAYER_INDEX()
				tl23_a = "Player_SMG_"
				tl23_a += i+1
				tl23_b = "Player_Mag_"
				tl23_b += i+1
				IF bStrandMission
					PASS_OVER_CUT_SCENE_OBJECT(NULL, tl23_a)
					PASS_OVER_CUT_SCENE_OBJECT(NULL, tl23_b)
				ELSE
					PRINTLN("[Cutscene] - MANUALLY_REGISTER_SPECIFIC_ENTITIES - HS3F_DIR_ENT - Registering: ", tl23_a, " With NULL...")				
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, tl23_a, CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
					PRINTLN("[Cutscene] - MANUALLY_REGISTER_SPECIFIC_ENTITIES - HS3F_DIR_ENT - Registering: ", tl23_b, " With NULL...")
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, tl23_b, CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapName, "TUNF_DRP_HAND")
	OR ARE_STRINGS_EQUAL(sMocapName, "TUNF_DRP_BAG")
		FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES - 1			
			// Hide the one not in use.
			IF ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].tlCutsceneHandle, "Client")
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Client_Female", CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				PRINTLN("[Cutscene] - MANUALLY_REGISTER_SPECIFIC_ENTITIES - ", sMocapName, " - Registering: Client_Female With NULL...")				
				BREAKLOOP
			ELIF ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].tlCutsceneHandle, "Client_Female")
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Client", CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				PRINTLN("[Cutscene] - MANUALLY_REGISTER_SPECIFIC_ENTITIES - ", sMocapName, " - Registering: Client With NULL...")		
				BREAKLOOP
			ENDIF
		ENDFOR
	ENDIF
	
	IF ARE_STRINGS_EQUAL("TUNF_IAA_MCS1", sMocapName)
		INT iCutsceneTeam
		IF iScriptedCutsceneTeam != -1
		AND eCutType != FMMCCUT_ENDMOCAP
			iCutsceneTeam = iScriptedCutsceneTeam
		ELSE
			iCutsceneTeam = MC_PlayerBD[iPartToUse].iTeam
		ENDIF
		
		PRINTLN("[Cutscene] - MANUALLY_REGISTER_SPECIFIC_ENTITIES - ", sMocapName)
		
		IF MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][3] = INVALID_PLAYER_INDEX()
			PRINTLN("[Cutscene] - MANUALLY_REGISTER_SPECIFIC_ENTITIES - ", sMocapName, " - Nulling MP_4_Pistol.")			
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "MP_4_Pistol", CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "MP_4_Pistol_Mag", CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
		ENDIF
		IF MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][2] = INVALID_PLAYER_INDEX()
			PRINTLN("[Cutscene] - MANUALLY_REGISTER_SPECIFIC_ENTITIES - ", sMocapName, " - Nulling MP_3_Pistol.")		
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "MP_3_Pistol", CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "MP_3_Pistol_Mag", CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
		ENDIF
	ENDIF

	IF eCutType = FMMCCUT_ENDMOCAP
		FOR i = 0 TO FMMC_MAX_CUTSCENE_HIDE_ENTITIES - 1					
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sEndMocapSceneData.tl31_HideCutsceneHandles[i])
				PRINTLN("[Cutscene] - MANUALLY_REGISTER_SPECIFIC_ENTITIES - ", sMocapName, " - (end) attempting to hide:",  g_FMMC_STRUCT.sEndMocapSceneData.tl31_HideCutsceneHandles[i])
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, g_FMMC_STRUCT.sEndMocapSceneData.tl31_HideCutsceneHandles[i], CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
			ENDIF
		ENDFOR
	ELIF eCutType = FMMCCUT_MOCAP	
		FOR i = 0 TO FMMC_MAX_CUTSCENE_HIDE_ENTITIES - 1
			IF iCutsceneToUse != -1
				IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tl31_HideCutsceneHandles[i])
					PRINTLN("[Cutscene] - MANUALLY_REGISTER_SPECIFIC_ENTITIES - ", sMocapName, " - (mocap) attempting to hide:",  g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tl31_HideCutsceneHandles[i])
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tl31_HideCutsceneHandles[i], CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				ENDIF
			ELSE
				PRINTLN("[Cutscene] - MANUALLY_REGISTER_SPECIFIC_ENTITIES - ", sMocapName, " - iCutsceneToUse = -1, will not work.")
			ENDIF
		ENDFOR
	ENDIF
	
	IF ARE_STRINGS_EQUAL("FIX_GOLF_INT", sMocapName)
	AND iScriptedCutsceneTeam != -1
		IF MC_serverBD.iNumStartingPlayers[iScriptedCutsceneTeam] = 1			
			PRINTLN("[Cutscene] - MANUALLY_REGISTER_SPECIFIC_ENTITIES - ", sMocapName, " - Hiding Player2_GolfCart, as Player 2 does not exist.")
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Player2_GolfCart", CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)			
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapName, "FIX_TRIP3_MCS1")
		
		OBJECT_INDEX oiWeapon
		PED_INDEX pedPlayer 									  	
		i = -1
		DO_PARTICIPANT_LOOP_FLAGS eFlags
		eFlags = DPLF_CHECK_PED_ALIVE | DPLF_CHECK_PLAYER_OK | DPLF_FILL_PLAYER_IDS | DPLF_IGNORE_TEAM_1 | DPLF_IGNORE_TEAM_2 | DPLF_IGNORE_TEAM_3
		WHILE DO_PARTICIPANT_LOOP(i, eFlags)
			PRINTLN("[Cutscene] - MANUALLY_REGISTER_SPECIFIC_ENTITIES - ", sMocapName, " - iPart: ", i, " is Franklin.")
			pedPlayer = piParticipantLoop_PedIndex
			BREAKLOOP
		ENDWHILE
		
		IF IS_ENTITY_ALIVE(pedPlayer)
			oiWeapon = CREATE_WEAPON_OBJECT_FROM_WEAPON_INFO(sPlayerWeaponDetailsForCutscene[0].sWeaponInfo, GET_ENTITY_COORDS(piParticipantLoop_PedIndex) - <<0,0,10>>)
			
			eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_MCS1_Sniper] = oiWeapon
			
			SET_ENTITY_VISIBLE(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_MCS1_Sniper], TRUE)
		ENDIF
		
		IF DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_MCS1_Sniper])
			PRINTLN("[Cutscene] - MANUALLY_REGISTER_SPECIFIC_ENTITIES - ", sMocapName, " - Registering Sniper.")
			REGISTER_ENTITY_FOR_CUTSCENE(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_MCS1_Sniper], "Franklin_sniper", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DEFAULT, CEO_IGNORE_MODEL_NAME)
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "sniper_scope", CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
		ELSE
			PRINTLN("[Cutscene] - MANUALLY_REGISTER_SPECIFIC_ENTITIES - ", sMocapName, " - Sniper was not created.")
		ENDIF
		
	ENDIF
		
	IF ARE_STRINGS_EQUAL(sMocapName, "FIXF_FIN_MCS3")
	
		INT iPlayers = MC_serverBD.iNumStartingPlayers[0]
		
		PRINTLN("[Cutscene] - MANUALLY_REGISTER_SPECIFIC_ENTITIES - ", sMocapName, " - iPlayers: ", iPlayers)
		
		IF iPlayers = 2
		OR iPlayers = 3
			PRINTLN("[Cutscene] - MANUALLY_REGISTER_SPECIFIC_ENTITIES - ", sMocapName, " - iPlayers: ", iPlayers, " REGISTER_ENTITY_FOR_CUTSCENE Producer_SportsCar with NULL")
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Producer_SportsCar", CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_PassOverWorldAlarms)		
		PRINTLN("[Cutscene] - MANUALLY_REGISTER_SPECIFIC_ENTITIES - ", sMocapName, " - ciOptionsBS28_PassOverWorldAlarms is set. Attempting to pass over alarms.")
		
		IF IS_BIT_SET(iWorldAlarmTypeBitset, ciWORLD_ALARM_TYPE_CUTSCENE_ISLAND_HEIST_ONE_SHOT)
			PASS_OVER_CUTSCENE_WORLD_ALARM_TYPE(vLocalPlayerPosition, ciWORLD_ALARM_TYPE_CUTSCENE_ISLAND_HEIST_ONE_SHOT)
		ENDIF		
		IF IS_BIT_SET(iWorldAlarmTypeBitset, ciWORLD_ALARM_TYPE_CUTSCENE_ISLAND_HEIST_GATE)
			PASS_OVER_CUTSCENE_WORLD_ALARM_TYPE((<<5008.1, -5754, 17.6>>), ciWORLD_ALARM_TYPE_CUTSCENE_ISLAND_HEIST_GATE)
		ENDIF
	ENDIF
		
ENDPROC

FUNC BOOL LOAD_ASSETS_FOR_MOCAP(STRING sMocapCutsceneName)
	
	IF ARE_STRINGS_EQUAL(sMocapCutsceneName, "FIX_TRIP3_MCS1")	
		REQUEST_WEAPON_ASSET(GET_WEAPON_TYPE_FOR_MOCAP_SEND_LOCAL_PLAYER_DATA(sMocapCutsceneName), DEFAULT, WEAPON_COMPONENT_FLASH | WEAPON_COMPONENT_SCOPE | WEAPON_COMPONENT_SUPP | WEAPON_COMPONENT_SCLIP2 | WEAPON_COMPONENT_GRIP)
		IF NOT HAS_WEAPON_ASSET_LOADED(GET_WEAPON_TYPE_FOR_MOCAP_SEND_LOCAL_PLAYER_DATA(sMocapCutsceneName))
			PRINTLN("[Cutscene] - LOAD_ASSETS_FOR_MOCAP - ", sMocapCutsceneName, " - REQUEST_WEAPON_ASSET")			
			RETURN FALSE
		ENDIF
	ENDIF
		
	RETURN TRUE
ENDFUNC

PROC RELEASE_ASSETS_FOR_MOCAP(STRING sMocapCutsceneName)	
	IF ARE_STRINGS_EQUAL(sMocapCutsceneName, "FIX_TRIP3_MCS1")	
		IF HAS_WEAPON_ASSET_LOADED(GET_WEAPON_TYPE_FOR_MOCAP_SEND_LOCAL_PLAYER_DATA(sMocapCutsceneName))
			PRINTLN("[Cutscene] - RELEASE_ASSETS_FOR_MOCAP - ", sMocapCutsceneName, " - REMOVE_WEAPON_ASSET")
			REMOVE_WEAPON_ASSET(GET_WEAPON_TYPE_FOR_MOCAP_SEND_LOCAL_PLAYER_DATA(sMocapCutsceneName))
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_CUTSCENE_AND_SCENE_HANDLE_USE_IGNORE_MODEL_NAME_FLAG(STRING sMocapCutsceneName, STRING sCutsceneHandle)
	
	IF ARE_STRINGS_EQUAL(sMocapCutsceneName, "TUNF_DRP_BAG")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "Client")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "Client_Female")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "Client_Backup_1")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "Client_Backup_2")
			RETURN TRUE
		ENDIF
		
	ELIF ARE_STRINGS_EQUAL(sMocapCutsceneName, "TUNF_DRP_HAND")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "Client")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "Client_Female")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "Client_Backup_1")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "Client_Backup_2")
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sCutsceneHandle, "MP_1")
	OR ARE_STRINGS_EQUAL(sCutsceneHandle, "MP_2")
	OR ARE_STRINGS_EQUAL(sCutsceneHandle, "MP_3")
	OR ARE_STRINGS_EQUAL(sCutsceneHandle, "MP_4")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_CUTSCENE_SET_PEDS_AS_FAKE_PLAYERS(STRING sCutsceneHandle)
							
	IF ARE_STRINGS_EQUAL("MP_1", sCutsceneHandle)
		SET_BIT(iPedFakePlayersBitset, ciPedFakePlayersBitset_MP_1)
	ENDIF
	
	IF ARE_STRINGS_EQUAL("MP_2", sCutsceneHandle)
		SET_BIT(iPedFakePlayersBitset, ciPedFakePlayersBitset_MP_2)
	ENDIF
	
	IF ARE_STRINGS_EQUAL("MP_3", sCutsceneHandle)
		SET_BIT(iPedFakePlayersBitset, ciPedFakePlayersBitset_MP_3)
	ENDIF
	
	IF ARE_STRINGS_EQUAL("MP_4", sCutsceneHandle)
		SET_BIT(iPedFakePlayersBitset, ciPedFakePlayersBitset_MP_4)
	ENDIF
	
ENDPROC

FUNC BOOL IS_CUTSCENE_HANDLE_AND_ENTITY_BANNED_FROM_REGISTRATION(STRING sCutsceneHandle, NETWORK_INDEX netid)
		
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "FIXF_FIN_MCS3")
	
		IF ARE_STRINGS_EQUAL("Producer_SportsCar", sCutsceneHandle)
			
			IF netID = NULL 
			OR NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netid)
				PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - IS_CUTSCENE_HANDLE_AND_ENTITY_BANNED_FROM_REGISTRATION - ", sMocapCutscene, " - sCutsceneHandle: ", sCutsceneHandle, " is banned, because the vehicle is null.")
				RETURN TRUE
			ENDIF
			
			INT iPlayers = MC_serverBD.iNumberOfPlayingPlayers[0]			
			IF iPlayers = 2
			OR iPlayers = 3
				PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - IS_CUTSCENE_HANDLE_AND_ENTITY_BANNED_FROM_REGISTRATION - Banning Sportcars cause of play numbers.")
				RETURN TRUE
			ENDIF
			
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

///PURPOSE: This function loops through all the entities tagged in the creator and registers them to the current cutscene
PROC REGISTER_CUTSCENE_ENTITIES(INT iCutscene, BOOL bshow, FMMC_CUTSCENE_TYPE eCutType)
	
	INT i
	NETWORK_INDEX netid = NULL
	ENTITY_INDEX EntID = NULL
	BOOL bStrand = IS_A_STRAND_MISSION_BEING_INITIALISED()

	IF iCutscene = -1
	AND eCutType != FMMCCUT_ENDMOCAP
		PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - iCutscene: ", iCutscene, " eCutType: ", GET_FMMC_CUTSCENE_TYPE_NAME(eCutType), " - invalid iCutscene/eCutType combination")
		EXIT
	ENDIF
	
	IF eCutType = FMMCCUT_SCRIPTED
		
		//Scripted Cutscene
		FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES-1
			
			IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType =-1
			OR g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex = -1
				RELOOP
			ENDIF
			
			IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType = CREATION_TYPE_PROPS
				EntID = GET_CUTSCNE_VISIBLE_PROP(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex)
				IF EntID != NULL
					SET_ENTITY_VISIBLE(EntID, TRUE)
				ENDIF
			ELSE
				netid = GET_CUTSCNE_VISIBLE_NET_ID(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType,g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex)
				PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - GET_CUTSCNE_VISIBLE_NET_ID has returned a thing: NULL = ",netid = NULL,", IS_CUTSCENE_PLAYING = ",IS_CUTSCENE_PLAYING())
				IF netid != NULL AND NOT IS_CUTSCENE_PLAYING()
					PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - calling SET_NETWORK_ID_VISIBLE_IN_CUTSCENE with ",bShow)
					SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(netid,bshow,FALSE)
				ENDIF
			ENDIF
			
		ENDFOR
	
	ELIF eCutType = FMMCCUT_MOCAP
	OR eCutType = FMMCCUT_ENDMOCAP

		#IF IS_DEBUG_BUILD
		IF IS_A_STRAND_MISSION_BEING_INITIALISED()
			PRINTLN("[Cutscene] - [MSRAND] - REGISTER_CUTSCENE_ENTITIES")
		ENDIF
		#ENDIF
				
		INT iCutsceneEntityType = -1
		INT iCutsceneEntityIndex = -1
		TEXT_LABEL_23 sCutsceneHandle = ""
		
		//Mocap Cutscene
		FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES - 1
						
			// end mocap
			IF eCutType = FMMCCUT_ENDMOCAP
			
				iCutsceneEntityType = g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType
				iCutsceneEntityIndex = g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex
				sCutsceneHandle = g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].tlCutsceneHandle

				#IF IS_DEBUG_BUILD
				IF IS_A_STRAND_MISSION_BEING_INITIALISED()
					PRINTLN("[Cutscene] - [MSRAND] - REGISTER_CUTSCENE_ENTITIES - iCutsceneEntityType  = ", iCutsceneEntityType)
					PRINTLN("[Cutscene] - [MSRAND] - REGISTER_CUTSCENE_ENTITIES - iCutsceneEntityIndex = ", iCutsceneEntityIndex)
					PRINTLN("[Cutscene] - [MSRAND] - REGISTER_CUTSCENE_ENTITIES - sCutsceneHandle      = ", sCutsceneHandle)
				ENDIF
				#ENDIF
			// mid mocap
			ELIF eCutType = FMMCCUT_MOCAP
			
				iCutsceneEntityType = g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iType
				iCutsceneEntityIndex = g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iIndex
				sCutsceneHandle = g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].tlCutsceneHandle
				
			ENDIF
			
			IF iCutsceneEntityType = -1
			OR iCutsceneEntityIndex = -1
				PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " iCutsceneEntityType: ", iCutsceneEntityType, " iCutsceneEntityIndex: ", iCutsceneEntityIndex, " invalid type/index, reloop")
				RELOOP
			ENDIF
			
			BOOL bReloop
			INT ii = 0
			IF eCutType = FMMCCUT_ENDMOCAP
				PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - END MOCAP")
					
				FOR ii = 0 TO FMMC_MAX_CUTSCENE_HIDE_ENTITIES - 1
					
					PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " iCutsceneEntityType: ", iCutsceneEntityType, " iCutsceneEntityIndex: ", iCutsceneEntityIndex, " g_FMMC_STRUCT.sEndMocapSceneData.tl31_HideCutsceneHandles[", ii, " ]: ", g_FMMC_STRUCT.sEndMocapSceneData.tl31_HideCutsceneHandles[ii], " sCutsceneHandle: ", sCutsceneHandle)
					
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sEndMocapSceneData.tl31_HideCutsceneHandles[ii])
					AND ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sEndMocapSceneData.tl31_HideCutsceneHandles[ii], sCutsceneHandle)
						PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " iCutscene: ", iCutscene, ", eCutType: ", GET_FMMC_CUTSCENE_TYPE_NAME(eCutType), ", cutscene entity: ", i, " - Skipping '", sCutsceneHandle, "'. They are set as HIDDEN.")
						#IF IS_DEBUG_BUILD
						ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_END_CUTSCENE_TRYING_TO_HIDE_REG_ENTS, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "End Cutscene trying to hide entities we are also registering! This is wrong!")
						#ENDIF
						bReloop = TRUE
						BREAKLOOP
					ENDIF
				ENDFOR
			ELIF eCutType = FMMCCUT_MOCAP
				FOR ii = 0 TO FMMC_MAX_CUTSCENE_HIDE_ENTITIES - 1
					IF iCutscene != -1
						IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].tl31_HideCutsceneHandles[ii])
						AND ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].tl31_HideCutsceneHandles[ii], sCutsceneHandle)
							PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " iCutscene: ", iCutscene, ", eCutType: ", GET_FMMC_CUTSCENE_TYPE_NAME(eCutType), ", cutscene entity: ", i, " - Skipping '", sCutsceneHandle, "' They are set as HIDDEN.")
							#IF IS_DEBUG_BUILD
							ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_MOCAP_CUTSCENE_TRYING_TO_HIDE_REG_ENTS, ENTITY_RUNTIME_ERROR_TYPE_WARNING_CUTSCENE, "Mocap trying to hide entities we are also registering! This is wrong!", iCutscene)
							#ENDIF
							bReloop = TRUE
							BREAKLOOP
						ENDIF
					ENDIF
				ENDFOR				
			ENDIF
			IF bReloop
				bReloop = FALSE
				RELOOP
			ENDIF
			
			IF IS_STRING_NULL_OR_EMPTY(sCutsceneHandle)
				PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " iCutscene: ", iCutscene, ", eCutType: ", GET_FMMC_CUTSCENE_TYPE_NAME(eCutType), ", cutscene entity: ", i, " - sCutsceneHandle = NULL/EMPTY, please set a valid handle in the creator")
				RELOOP
				
			ELIF (ARE_STRINGS_EQUAL("MP_1", sCutsceneHandle)
			OR ARE_STRINGS_EQUAL("MP_2", sCutsceneHandle)
			OR ARE_STRINGS_EQUAL("MP_3", sCutsceneHandle)
			OR ARE_STRINGS_EQUAL("MP_4", sCutsceneHandle))
			AND GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sCutsceneHandle) != NULL
				PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " iCutscene: ", iCutscene, ", eCutType: ", GET_FMMC_CUTSCENE_TYPE_NAME(eCutType), ", cutscene entity: ", i, " - sCutsceneHandle = ", sCutsceneHandle, " but player already registered.")
				RELOOP				
				
			ELIF ARE_STRINGS_EQUAL(sCutsceneHandle, "VISIBLE")
				
				PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " iCutscene: ", iCutscene, ", eCutType: ", GET_FMMC_CUTSCENE_TYPE_NAME(eCutType), ", cutscene entity: ", i, " - sCutsceneHandle: ", sCutsceneHandle, " - Entity is meant to be shown but not registered in Cutscene")
				
				IF NOT bStrand
					netid = GET_CUTSCNE_VISIBLE_NET_ID(iCutsceneEntityType, iCutsceneEntityIndex)
					IF netid = NULL
					OR NOT NETWORK_DOES_NETWORK_ID_EXIST(netid)
						PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - Entity does not exist.")
						RELOOP
					ENDIF
					
					// "Hack" version ensures collision stays loaded, so things that are not frozen do not fall under the floor where we are playing a cutscene far from player position.
					SET_NETWORK_ID_VISIBLE_IN_CUTSCENE_HACK( netid, bshow, bshow )
					PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - Calling: SET_NETWORK_ID_VISIBLE_IN_CUTSCENE")
					
				ELSE		
					IF iCutsceneEntityType = CREATION_TYPE_PEDS
						PASS_OVER_CUT_SCENE_PED(NET_TO_PED(netid), sCutsceneHandle)
					ELIF iCutsceneEntityType = CREATION_TYPE_VEHICLES
						PASS_OVER_CUT_SCENE_VEHICLE(NET_TO_VEH(netid), sCutsceneHandle)
					ELIF iCutsceneEntityType = CREATION_TYPE_OBJECTS
					OR iCutsceneEntityType = CREATION_TYPE_PROPS
						PASS_OVER_CUT_SCENE_OBJECT(NET_TO_OBJ(netid), sCutsceneHandle)
					ENDIF
				ENDIF							
			ELIF ARE_STRINGS_EQUAL(sCutsceneHandle, "FOCUSENTITY")
				IF iCutsceneEntityType = CREATION_TYPE_PROPS
					EntID = GET_CUTSCNE_VISIBLE_PROP( iCutsceneEntityIndex )
				ELSE
					netid = GET_CUTSCNE_VISIBLE_NET_ID(iCutsceneEntityType, iCutsceneEntityIndex)
					IF netid != NULL
					AND NETWORK_DOES_NETWORK_ID_EXIST(netid)
						EntID = NETWORK_GET_ENTITY_FROM_NETWORK_ID(netid)
					ENDIF
				ENDIF
				
				IF EntID != NULL
				AND DOES_ENTITY_EXIST(EntID)
					IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_MOCAP_FOCUS_ENTITY_SET)
						IF NOT IS_ENTITY_FOCUS(EntID)
							PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " iCutscene: ", iCutscene, ", eCutType: ", GET_FMMC_CUTSCENE_TYPE_NAME(eCutType), ", cutscene entity: ", i, " - sCutsceneHandle: ", sCutsceneHandle, " - Setting Focus")
							SET_FOCUS_ENTITY(EntID)
							SET_BIT(iLocalBoolCheck30, LBOOL30_MOCAP_FOCUS_ENTITY_SET)
						ENDIF
					ELSE
						ASSERTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " iCutscene: ", iCutscene, ", eCutType: ", GET_FMMC_CUTSCENE_TYPE_NAME(eCutType), ", cutscene entity: ", i, " - sCutsceneHandle: ", sCutsceneHandle, " - NOT Setting Focus - You have multiple set!")
						PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " iCutscene: ", iCutscene, ", eCutType: ", GET_FMMC_CUTSCENE_TYPE_NAME(eCutType), ", cutscene entity: ", i, " - sCutsceneHandle: ", sCutsceneHandle, " - NOT Setting Focus - You have multiple set!")
					ENDIF
				ENDIF
				RELOOP
			ELSE
				CUTSCENE_HANDLE_SEARCH_RESULT eDoesCutHandleExit = DOES_CUTSCENE_HANDLE_EXIST(sCutsceneHandle)
				IF eDoesCutHandleExit != CHSR_HANDLE_EXISTS
					PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " iCutscene: ", iCutscene, ", eCutType: ", GET_FMMC_CUTSCENE_TYPE_NAME(eCutType), ", cutscene entity: ", i, " - sCutsceneHandle: ", sCutsceneHandle, " does not exist for cutscene, please set a valid handle in the creator")
					RELOOP
				ENDIF
			ENDIF
			
			IF iCutsceneEntityType = CREATION_TYPE_PROPS
				IF bStrand
					EntID = GET_CUTSCNE_VISIBLE_PROP( iCutsceneEntityIndex )
					PASS_OVER_CUT_SCENE_OBJECT(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(EntID), sCutsceneHandle)
				ELSE
				
					PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " iCutscene: ", iCutscene, ", eCutType: ", GET_FMMC_CUTSCENE_TYPE_NAME(eCutType), ", cutscene entity: ", i, " - sCutsceneHandle: ", sCutsceneHandle, " - PROP")
					
					EntID = GET_CUTSCNE_VISIBLE_PROP( iCutsceneEntityIndex )
					IF EntID != NULL
					AND DOES_ENTITY_EXIST( EntID )
						SET_ENTITY_VISIBLE(EntID, TRUE)
					ENDIF
					
				ENDIF
			ELSE
				IF ARE_STRINGS_EQUAL(sCutsceneHandle, "VISIBLE")				
					RELOOP
				ENDIF
				
				netid = GET_CUTSCNE_VISIBLE_NET_ID( iCutsceneEntityType, iCutsceneEntityIndex )
				IF netid = NULL
					PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " iCutscene: ", iCutscene, ", eCutType: ", GET_FMMC_CUTSCENE_TYPE_NAME(eCutType), ", cutscene entity: ", i, " - sCutsceneHandle: ", sCutsceneHandle, " - GET_CUTSCNE_VISIBLE_NET_ID return NULL!")
				ENDIF
				
				IF IS_CUTSCENE_HANDLE_AND_ENTITY_BANNED_FROM_REGISTRATION(sCutsceneHandle, netid)
					PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - sCutsceneHandle: ", sCutsceneHandle, " - IS_CUTSCENE_HANDLE_AND_ENTITY_BANNED_FROM_REGISTRATION is returning true. Some special conditions mean that we do not want to register it!")
					RELOOP
				ENDIF								
				
				IF netid != NULL
				AND NETWORK_DOES_NETWORK_ID_EXIST(netid)
					
					//Register the cutscene entity for the cutscene if we have a handle
					IF NOT IS_STRING_NULL_OR_EMPTY( sCutsceneHandle )
						IF NOT IS_CUTSCENE_PLAYING()
							SET_NETWORK_ID_VISIBLE_IN_CUTSCENE( netid, bshow, bshow )
						ENDIF
						
						EntID = NET_TO_ENT( netid )
						
						MODEL_NAMES tempModel = GET_ENTITY_MODEL( EntID )
						
						PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - valid handle, attempting to register")
						
						IF NOT bStrand

							//Check vehicle is not dead and give it health
							IF DOES_ENTITY_EXIST( EntID )
							AND NETWORK_HAS_CONTROL_OF_ENTITY(EntID)

								IF IS_ENTITY_A_VEHICLE(EntID)
								
									PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - valid handle, IS A VEHICLE")
									
									VEHICLE_INDEX vehID
									vehID = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(EntID)	

									IF NOT IS_VEHICLE_DRIVEABLE(vehID)
									OR IS_ENTITY_DEAD(vehID)
										PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - valid handle, VEHICLE not drivable")
										SET_ENTITY_HEALTH(vehID, 1000)
									ENDIF
										
									IF IS_ENTITY_ON_FIRE(vehID)
										
										PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - valid handle, VEHICLE on FIRE")
										STOP_VEHICLE_FIRE(vehID)
									
									ENDIF
									
									IF GET_VEHICLE_ENGINE_HEALTH(vehID) < 200
										PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - valid handle, Engine health is low (<200)")
										SET_ENTITY_HEALTH(vehID, 1000)
									ENDIF
																		
								ELIF IS_ENTITY_DEAD(EntID)
								
									PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - valid handle, entity is DEAD")
									SET_ENTITY_HEALTH(EntID, 200)
									
								ENDIF
							
							ENDIF
							
							CUTSCENE_ENTITY_OPTION_FLAGS ceoFlags = CEO_NONE	
							IF SHOULD_CUTSCENE_AND_SCENE_HANDLE_USE_IGNORE_MODEL_NAME_FLAG(sMocapCutscene, sCutsceneHandle)
								PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - sMocapCutscene: ", sMocapCutscene, " SHOULD_CUTSCENE_AND_SCENE_HANDLE_USE_IGNORE_MODEL_NAME_FLAG = TRUE")
								ceoFlags = CEO_IGNORE_MODEL_NAME
							ENDIF					
							
							
							PROCESS_CUTSCENE_SET_PEDS_AS_FAKE_PLAYERS(sCutsceneHandle)							
							
							REGISTER_ENTITY_FOR_CUTSCENE(EntID, sCutsceneHandle, CU_ANIMATE_EXISTING_SCRIPT_ENTITY, tempModel, ceoFlags) // url:bugstar:7121510 needed to add CEO_IGNORE_MODEL_NAME which should not affect previous content.
							
						ELSE
						
							PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - valid handle - strand, doing PASS_OVER_CUT_SCENE_")

							IF iCutsceneEntityType = CREATION_TYPE_PEDS
								PASS_OVER_CUT_SCENE_PED(NET_TO_PED(netid), sCutsceneHandle)
							ELIF iCutsceneEntityType = CREATION_TYPE_VEHICLES
								PASS_OVER_CUT_SCENE_VEHICLE(NET_TO_VEH(netid), sCutsceneHandle)
							ELIF iCutsceneEntityType = CREATION_TYPE_OBJECTS
							OR iCutsceneEntityType = CREATION_TYPE_PROPS
								PASS_OVER_CUT_SCENE_OBJECT(NET_TO_OBJ(netid), sCutsceneHandle)
							ENDIF
							
						ENDIF
					ELSE
					
						//Show entities that haven't been registered with a handle
						IF NOT bStrand
							IF NOT IS_CUTSCENE_PLAYING()
								SET_NETWORK_ID_VISIBLE_IN_CUTSCENE( netid, bshow, bshow )
								PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - invalid handle - SET_NETWORK_ID_VISIBLE_IN_CUTSCENE")
							ENDIF
						ELSE
							PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - invalid handle - PASS_OVER_CUT_SCENE_ ")
							IF iCutsceneEntityType = CREATION_TYPE_PEDS
								PASS_OVER_CUT_SCENE_PED(NET_TO_PED(netid), sCutsceneHandle)
							ELIF iCutsceneEntityType = CREATION_TYPE_VEHICLES
								PASS_OVER_CUT_SCENE_VEHICLE(NET_TO_VEH(netid), sCutsceneHandle)
							ELIF iCutsceneEntityType = CREATION_TYPE_OBJECTS
							OR  iCutsceneEntityType = CREATION_TYPE_PROPS
								PASS_OVER_CUT_SCENE_OBJECT(NET_TO_OBJ(netid), sCutsceneHandle)
							ENDIF
						ENDIF
	
					ENDIF
				ELIF iCutsceneEntityType = CREATION_TYPE_VEHICLES
				
					IF IS_A_STRAND_MISSION_BEING_INITIALISED()
						PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - entity does not exist - Is strand, PASS_OVER_CUT_SCENE_VEHICLE = NULL")
						PASS_OVER_CUT_SCENE_VEHICLE(NULL, sCutsceneHandle) 
					ELSE	
						REGISTER_ENTITY_FOR_CUTSCENE(NULL, sCutsceneHandle, CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME )
						PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - entity does not exist - REGISTER_ENTITY_FOR_CUTSCENE = NULL")
						PRINTLN("[Cutscene] - REGISTER_CUTSCENE_ENTITIES - REGISTER_ENTITY_FOR_CUTSCENE(NULL, ", sCutsceneHandle, ", CU_DONT_ANIMATE_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)")
					ENDIF					
				ENDIF
			ENDIF
		
		ENDFOR
		
		// World Props - This system could be extended to work with the proper cutscene registration but for now it might be too volatile to allow the createor to select world props the same way we select mission entities. 
		// For now this can use the World Props pool and a flag. Also allows hidden entities for Scripted Cutscenes externally within PROCESS_WORLD_PROPS.
		PRINTLN("[Cutscene] - [WORLDPROPS] REGISTER_CUTSCENE_ENTITIES | iNumberOfWorldProps: ", g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps)
		TEXT_LABEL_23 tl23
		INT iWorldProp
		FOR iWorldProp = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps-1
			IF bStrand				
				PROCESS_WORLD_PROPS_CUTSCENE(iWorldProp, TRUE)
				IF IS_LONG_BIT_SET(sRunTimeWorldPropData.iWorldPropHiddenForCutsceneBitSet, iWorldProp)  // DELIBERATELY NOT FMMC_IS_LONG_BIT_SET, see PROCESS_WORLD_PROPS_CUTSCENE and World Prop system...
					PASS_OVER_CUT_SCENE_WORLD_PROPS(sRunTimeWorldPropData.oiWorldProps[iWorldProp], tl23, TRUE)
				ENDIF
			ELSE
				PROCESS_WORLD_PROPS_CUTSCENE(iWorldProp, TRUE)
			ENDIF
		ENDFOR
	ENDIF
	
ENDPROC

///PURPOSE: This function returns the string name of the next cutscene to stream
PROC REGISTER_PLAYER_FOR_CUTSCENE(INT iplayer, INT iCutsceneTeam, INT &iRegisteredHandlesBitset, BOOL bCloneLocalPlayer, INT iCutsceneId, FMMC_CUTSCENE_TYPE eCutType)

	TEXT_LABEL_23 sPlayerString = GET_CUTSCENE_PLAYER_PREFIX(iplayer)
	BOOL bStrandMission = IS_A_STRAND_MISSION_BEING_INITIALISED()
	
	IF MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer] = LocalPlayer
		tlPlayerSceneHandle = sPlayerString
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(sPlayerString)	
		PRINTLN("[Cutscene][PED CUT] * REGISTER_PLAYER_FOR_CUTSCENE - sPlayerString is NULL or EMPTY. Player will need to be removed from the cutscene as they don't have a handle to be assigned to..")
		EXIT
	ENDIF
	
	IF MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer] = INVALID_PLAYER_INDEX()
		PRINTLN("[Cutscene][PED CUT] * REGISTER_PLAYER_FOR_CUTSCENE - MC_serverBD.piCutscenePlayerIDs[", iCutsceneTeam, "][", iplayer, "] = INVALID_PLAYER_INDEX() - Will need to remove player handle '", sPlayerString ,"'  from cutscene..")
		EXIT
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer])
		PRINTLN("[Cutscene][PED CUT] * REGISTER_PLAYER_FOR_CUTSCENE - MC_serverBD.piCutscenePlayerIDs[", iCutsceneTeam, "][", iplayer, "] IS_NET_PLAYER_OK() = FALSE - Will need to remove player handle '", sPlayerString ,"'  from cutscene..")
		EXIT
	ENDIF
	
	// Use the Clone Ped
   	IF (MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer] != LocalPlayer OR bCloneLocalPlayer)
	OR bIsSCTV
	OR bIsAnySpectator
		
		IF IS_PED_INJURED(MocapPlayerPed[iplayer])
		OR MocapPlayerPed[iplayer] = NULL
			PRINTLN("[Cutscene][PED CUT] * REGISTER_PLAYER_FOR_CUTSCENE - CLONE DOES NOT EXIST OR IS INJRUED FOR iPlayer: ", iPlayer,  " sPlayerString: ", sPlayerString)
			EXIT		
		ENDIF
		
		IF bStrandMission
			PRINTLN("[Cutscene][PED CUT] * REGISTER_PLAYER_FOR_CUTSCENE - Passing over Clone Ped Player for cutscene: ", NATIVE_TO_INT(MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer]), " in as character: ", sPlayerString)
			PASS_OVER_CUT_SCENE_PED(MocapPlayerPed[iplayer], sPlayerString, iMocapPlayerPedMask[iplayer], MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer])
			MocapPlayerPed[iplayer] = NULL
		ELSE
			
			// Could change this to default functionality as contingencies already delete the mocap peds. Not enough time to test thoroughly so going on an option. note: CLEANUP_MOCAP_PLAYER_CLONES()
			CUTSCENE_USAGE csUsage = CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY
			
			IF eCutType = FMMCCUT_MOCAP
				IF iCutsceneId != -1
				AND IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneId].iCutsceneBitSet3, ci_CSBS3_CutsceneDoNotDeleteEntitites)
					PRINTLN("[Cutscene][PED CUT] * FMMCCUT_MOCAP - REGISTER_PLAYER_FOR_CUTSCENE - MocapPlayerPed[", iplayer, "], ", sPlayerString, ", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)")
					csUsage = CU_ANIMATE_EXISTING_SCRIPT_ENTITY
				ELSE
					PRINTLN("[Cutscene][PED CUT] * FMMCCUT_MOCAP - REGISTER_PLAYER_FOR_CUTSCENE - MocapPlayerPed[", iplayer, "], ", sPlayerString, ", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME), iCutsceneId: ", iCutsceneId)
				ENDIF
			ELIF eCutType = FMMCCUT_ENDMOCAP
				IF IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet3, ci_CSBS3_CutsceneDoNotDeleteEntitites)
					PRINTLN("[Cutscene][PED CUT] * FMMCCUT_ENDMOCAP - REGISTER_PLAYER_FOR_CUTSCENE - MocapPlayerPed[", iplayer, "], ", sPlayerString, ", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)")
					csUsage = CU_ANIMATE_EXISTING_SCRIPT_ENTITY
				ELSE
					PRINTLN("[Cutscene][PED CUT] * FMMCCUT_ENDMOCAP - REGISTER_PLAYER_FOR_CUTSCENE - MocapPlayerPed[", iplayer, "], ", sPlayerString, ", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME), iCutsceneId: ", iCutsceneId)
				ENDIF
			ENDIF
			
			REGISTER_ENTITY_FOR_CUTSCENE(MocapPlayerPed[iplayer], sPlayerString, csUsage, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
			PRINTLN("[Cutscene][PED CUT] * REGISTER_PLAYER_FOR_CUTSCENE - Registering Clone Ped Player for cutscene: ", NATIVE_TO_INT(MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer]), " in as character: ", sPlayerString)		
		ENDIF		
		
		SET_BIT(iRegisteredHandlesBitset, GET_CUTSCENE_PLAYER_PREFIX_INT(iPlayer))		
		//EXIT here as we've register a clone.
		EXIT
	ENDIF
	
	// Use the Local Index Instead
	IF bStrandMission
		PRINTLN("[Cutscene][PED CUT] * REGISTER_PLAYER_FOR_CUTSCENE - Passing over local player for cutscene: ",NATIVE_TO_INT(MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer])," in as character: ",sPlayerString)
		PASS_OVER_CUT_SCENE_PED(LocalPlayerPed, sPlayerString, GET_PLAYER_CHOSEN_MASK(LocalPlayer), LocalPlayer)		
	ELSE		
		IF NETWORK_IS_PLAYER_IN_MP_CUTSCENE(LocalPlayer)
			NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
			PRINTLN("[Cutscene][PED CUT] * REGISTER_PLAYER_FOR_CUTSCENE - REGISTER_PLAYER_FOR_CUTSCENE NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)")
			PRINTLN("[Cutscene][PED CUT] * REGISTER_PLAYER_FOR_CUTSCENE - player already in MP cutscene killing it")
		ENDIF
		
		SET_ENTITY_VISIBLE_IN_CUTSCENE(LocalPlayerPed, TRUE)
		SET_BIT(iLocalBoolCheck10, LBOOL10_LOCAL_PLAYER_IN_CUTSCENE)
		
		REGISTER_ENTITY_FOR_CUTSCENE(LocalPlayerPed, sPlayerString, CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
		PRINTLN("[Cutscene][PED CUT] * REGISTER_PLAYER_FOR_CUTSCENE - LocalPlayerPed, ", sPlayerString, ", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)")		
		PRINTLN("[Cutscene][PED CUT] * REGISTER_PLAYER_FOR_CUTSCENE - Registering local player for cutscene: ",NATIVE_TO_INT(MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer])," in as character: ",sPlayerString)
	ENDIF
	
	SET_BIT(iRegisteredHandlesBitset, GET_CUTSCENE_PLAYER_PREFIX_INT(iPlayer))
ENDPROC

PROC REGISTER_CUTSCENE_PLAYERS(INT iCutsceneTeam, INT iCutsceneId, FMMC_CUTSCENE_TYPE eCutType, BOOL bIncludePlayers = TRUE, BOOL bCloneLocalPlayer = FALSE)
	
	PRINTLN("[Cutscene][PED CUT] * REGISTER_CUTSCENE_PLAYERS - For sMocapCutscene: ", sMocapCutscene)
	
	INT iPlayer = 0	
	INT iNumberOfPlayers = GET_MOCAP_NUMBER_OF_PLAYERS(sMocapCutscene)
	INT iRegisteredHandlesBitset
	
	IF bIncludePlayers		
		FOR iPlayer = 0 TO iNumberOfPlayers-1
			PRINTLN("[Cutscene][PED CUT] * REGISTER_CUTSCENE_PLAYERS - Player: ", iPlayer)
			REGISTER_PLAYER_FOR_CUTSCENE(iPlayer, iCutsceneTeam, iRegisteredHandlesBitset, bCloneLocalPlayer, iCutsceneId, eCutType)
		ENDFOR
	ENDIF
	
	// REMOVE PLAYERS WHO ARE NULL OR WHO WERE NOT REGISTERED.	
	FOR iPlayer = 0 TO iNumberOfPlayers-1		
			
		INT iHandle = GET_CUTSCENE_PLAYER_PREFIX_INT(iPlayer)
		
		IF IS_BIT_SET(iRegisteredHandlesBitset, iHandle)
			RELOOP
		ENDIF
		
		SET_BIT(iRegisteredHandlesBitset, iHandle)
		
		IF IS_BIT_SET(iPedFakePlayersBitset, ciPedFakePlayersBitset_MP_1 + iHandle)
			PRINTLN("[Cutscenes] [PED CUT] REGISTER_CUTSCENE_PLAYERS - MP_", iHandle+1, " A replacement player has been registered! Relooping.")
			RELOOP
		ELSE
			PRINTLN("[Cutscenes] [PED CUT] REGISTER_CUTSCENE_PLAYERS - MP_", iHandle+1, " no entity registered.")
		ENDIF		 
		 
		IF (eCutType = FMMCCUT_MOCAP OR eCutType = FMMCCUT_ENDMOCAP)
		AND iCutsceneId >= 0
		AND iCutsceneId < MAX_MOCAP_CUTSCENES
			IF eCutType = FMMCCUT_MOCAP 				
				REMOVE_CUTSCENE_PLAYER_AND_PROPS(iPlayer, g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneId].sPlayerInfo[iPlayer].tlPropHandles)
			ELIF eCutType = FMMCCUT_ENDMOCAP
				REMOVE_CUTSCENE_PLAYER_AND_PROPS(iPlayer, g_FMMC_STRUCT.sEndMocapSceneData.sPlayerInfo[iPlayer].tlPropHandles)
			ENDIF
			PRINTLN("[Cutscenes] [PED CUT] REGISTER_CUTSCENE_PLAYERS - iPlayer: ", iPlayer, " Removing player & props")
		ELSE
			REMOVE_CUTSCENE_PLAYER(iPlayer)
			PRINTLN("[Cutscenes] [PED CUT] REGISTER_CUTSCENE_PLAYERS - iPlayer: ", iPlayer, " Removing player only")
		ENDIF
	ENDFOR
	
	// REMOVE EXTRA PLAYER SLOTS/HANDLES
	// if cutscene handles can be asynchronous to player index, then remove the handles that have not been registered.
	// For example, a cutscene with 6 player handles will have 2 unregistered.
	INT iPlayerHandle = 0
	IF DOES_MC_CUTSCENE_HAVE_A_MODIFIED_PLAYER_NUMBER(sMocapCutscene)		
		PRINTLN("[Cutscenes] [PED CUT] REGISTER_CUTSCENE_PLAYERS - Removing unused handles")
		
		FOR iPlayerHandle = 0 TO 4 //5 player handles max at the moment.
			IF NOT IS_BIT_SET(iRegisteredHandlesBitset, iPlayerHandle)
				PRINTLN("[Cutscenes] [PED CUT] REGISTER_CUTSCENE_PLAYERS - Not Registered. Removing iPlayerHandle: ", iPlayerHandle)
				REMOVE_PLAYER_CUTSCENE_HANDLE(iPlayerHandle)
			ELSE
				PRINTLN("[Cutscenes] [PED CUT] REGISTER_CUTSCENE_PLAYERS - Already Registered iPlayerHandle: ", iPlayerHandle)
			ENDIF
		ENDFOR
	ENDIF
	
ENDPROC

FUNC BOOL CLONE_MOCAP_PLAYERS(STRING sMocapName, INT iteam, BOOL bIncludePlayers = TRUE, FMMC_CUTSCENE_TYPE eCutType = FMMCCUT_MOCAP, INT iCutsceneToUse = -1, BOOL bCloneLocalPlayer = FALSE, BOOL bCanUseOriginalPlayerPeds = FALSE)

	IF NOT bIncludePlayers
		PRINTLN("[Cutscenes] CLONE_MOCAP_PLAYERS - NO NEED TO CLONE PEDS THEY ARENT IN CUTSCENE")
		RETURN TRUE
	ENDIF
		
	PLAYER_INDEX piPlayer	
	PED_INDEX tempPed
	MODEL_NAMES tempModel
	BOOL bReturn = TRUE
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 tl63PedNameDBG
	#ENDIF
	
	INT iplayer
	FOR iplayer = 0 TO (FMMC_MAX_CUTSCENE_PLAYERS-1)
		
		PRINTLN("[PED CUT] * CLONE_MOCAP_PLAYERS - Checking MC_serverBD.piCutscenePlayerIDs[", iteam, "][", iplayer, "] ")
		
		tempPed = NULL
		piPlayer = INVALID_PLAYER_INDEX()
		
		IF bCanUseOriginalPlayerPeds
			IF DOES_ENTITY_EXIST(MocapCachedIntroCutscenePlayerPedClone[iteam][iplayer])
				PRINTLN("[PED CUT] * CLONE_MOCAP_PLAYERS - USING MocapCachedIntroCutscenePlayerPedClone PED Team: ", iteam, " Player: ", iplayer)
				tempPed = MocapCachedIntroCutscenePlayerPedClone[iteam][iplayer]				
				piPlayer = MocapCachedIntroCutscenePlayer[iTeam][iplayer]
				tempModel = GET_ENTITY_MODEL(tempPed)
			ENDIF
		ELSE
			PRINTLN("[PED CUT] * CLONE_MOCAP_PLAYERS - USING MC_serverBD.piCutscenePlayerIDs PED Team: ", iteam, " Player: ", iplayer)
			IF MC_serverBD.piCutscenePlayerIDs[iteam][iplayer] != INVALID_PLAYER_INDEX()
				IF IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer])						
					IF (MC_serverBD.piCutscenePlayerIDs[iteam][iplayer] != LocalPlayer OR bCloneLocalPlayer)
					OR bIsSCTV
					OR bIsAnySpectator
						tempPed = GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer])
						piPlayer = MC_serverBD.piCutscenePlayerIDs[iteam][iplayer]
						tempModel = GET_ENTITY_MODEL(tempPed)
					ENDIF
				ELSE
					PRINTLN("[Cutscenes] CLONE_MOCAP_PLAYERS iteam: ", iteam, " iPlayer: ", iPlayer, " IS_NET_PLAYER_OKMC_serverBD.piCutscenePlayerIDs[iteam][iplayer]) = FALSE")
				ENDIF
			ELSE
				PRINTLN("[Cutscenes] CLONE_MOCAP_PLAYERS iteam: ", iteam, " iPlayer: ", iPlayer, " MC_serverBD.piCutscenePlayerIDs[iteam][iplayer] = INVALID_PLAYER_INDEX()")
			ENDIF
		ENDIF		
		
		IF DOES_ENTITY_EXIST(tempPed)
		AND NOT IS_PED_INJURED(tempPed)
			
			REQUEST_MODEL(tempModel)
			
			IF HAS_MODEL_LOADED(tempModel)
				IF NOT DOES_ENTITY_EXIST(MocapPlayerPed[iplayer])
					
					PRINTLN("[PED CUT] * CLONE_MOCAP_PLAYERS - piPlayer - GET_PLAYER_NAME() =  ", GET_PLAYER_NAME(piPlayer), " Model: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(tempModel))
					
					IF GET_ENTITY_MODEL(tempPed) = MP_F_FREEMODE_01
						MocapPlayerPed[iplayer] = CREATE_PED(PEDTYPE_CIVFEMALE, tempModel, GET_ENTITY_COORDS(tempPed), GET_ENTITY_HEADING(tempPed), FALSE, FALSE)
					ELSE
						MocapPlayerPed[iplayer] = CREATE_PED(PEDTYPE_CIVMALE, tempModel, GET_ENTITY_COORDS(tempPed), GET_ENTITY_HEADING(tempPed), FALSE, FALSE)
					ENDIF
					
					CLEAR_PED_BLOOD_DAMAGE(MocapPlayerPed[iplayer])
					CLONE_PED_TO_TARGET_ALT(tempPed, MocapPlayerPed[iplayer])
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MocapPlayerPed[iplayer], TRUE)
					SET_PED_RELATIONSHIP_GROUP_HASH(MocapPlayerPed[iplayer], rgFM_AiLike)
					SET_ENTITY_INVINCIBLE(MocapPlayerPed[iplayer],TRUE)
					SET_ENTITY_VISIBLE(MocapPlayerPed[iplayer],FALSE)
					SET_ENTITY_COLLISION(MocapPlayerPed[iplayer],FALSE)
					
					#IF IS_DEBUG_BUILD
					tl63PedNameDBG = "CLONE_"
					tl63PedNameDBG += iplayer
					tl63PedNameDBG += " - "
					tl63PedNameDBG += GET_PLAYER_NAME(piPlayer)
					SET_PED_NAME_DEBUG(MocapPlayerPed[iplayer], tl63PedNameDBG)
					#ENDIF

					iMocapPlayerPedMask[iplayer] = GET_PLAYER_CHOSEN_MASK(piPlayer)
					
					BOOL bRemoveHeistGear = CAN_REMOVE_HEIST_GEAR_FOR_CUTSCENE(sMocapName, DEFAULT, eCutType, iCutsceneToUse)
					
					IF CAN_REMOVE_MASKS_FOR_HEIST_CUTSCENE(sMocapName, DEFAULT, eCutType, iCutsceneToUse)
						//Now passing in the player index so that we dont have the issue with same player faces. CV
						REMOVE_MASKS_AND_REBREATHERS_FOR_CUTSCENES(MocapPlayerPed[iplayer], piPlayer, INT_TO_ENUM(mp_outfit_mask_enum,iMocapPlayerPedMask[iplayer]), bRemoveHeistGear)
					ENDIF
					
					IF bRemoveHeistGear
						REMOVE_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
					ENDIF
					
					BOOL bKeepHelmets							
					IF iCutsceneToUse > -1
					AND eCutType = FMMCCUT_MOCAP
						IF IS_BIT_SET(g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_KeepHatsAndGlassesEtc)
							bKeepHelmets = TRUE
						ENDIF
					ELIF eCutType = FMMCCUT_ENDMOCAP
						IF IS_BIT_SET(g_fmmc_struct.sEndMocapSceneData.iCutsceneBitSet2, ci_CSBS2_KeepHatsAndGlassesEtc)
							bKeepHelmets = TRUE
						ENDIF
					ENDIF

					BOOL bApplyHeadProp = TRUE
					IF SHOULD_REMOVE_HELMETS_FOR_SCENE()
					AND NOT bKeepHelmets
						IF IS_PED_WEARING_A_HELMET(MocapPlayerPed[iplayer])
							PRINTLN("[Cutscenes] CLONE_MOCAP_PLAYERS - REMOVE_PED_HELMET, bApplyHeadProp = FALSE ")
							REMOVE_PED_HELMET(MocapPlayerPed[iplayer], TRUE)
							SET_PED_PROP_INDEX( MocapPlayerPed[iplayer], ANCHOR_HEAD, 0, 0 ) 
							bApplyHeadProp = FALSE
						ENDIF
					ENDIF
					
					IF bApplyHeadProp
						INT iHatIndex = GET_PED_PROP_INDEX( tempPed, ANCHOR_HEAD )
						INT iHatTexture = GET_PED_PROP_TEXTURE_INDEX( tempPed, ANCHOR_HEAD )
						
						PRINTLN("[Cutscenes] CLONE_MOCAP_PLAYERS - iHatIndex: ", iHatIndex)
						PRINTLN("[Cutscenes] CLONE_MOCAP_PLAYERS - iHatTexture: ", iHatTexture)
						
						IF iHatIndex > -1 AND iHatTexture > -1
							PRINTLN("[Cutscenes] CLONE_MOCAP_PLAYERS - Calling SET_PED_PROP_INDEX on ANCHOR_HEAD")
							SET_PED_PROP_INDEX( MocapPlayerPed[iplayer], ANCHOR_HEAD, iHatIndex, iHatTexture ) 
						ENDIF
					ENDIF
															
					bReturn =  FALSE
				ELSE
					//Check the ped is ready for the cutscene
					IF NOT IS_PED_INJURED(MocapPlayerPed[iplayer])
					AND NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(MocapPlayerPed[iplayer])
						PRINTLN("[Cutscenes] CLONE_MOCAP_PLAYERS HAVE_ALL_STREAMING_REQUESTS_COMPLETED FALSE ")
						bReturn = FALSE
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[Cutscenes] CLONE_MOCAP_PLAYERS WAITING FOR MODEL LOAD: ", ENUM_TO_INT(tempModel))
				bReturn =  FALSE
			ENDIF
		ELSE
			PRINTLN("[Cutscenes] CLONE_MOCAP_PLAYERS iteam: ", iteam, " iPlayer: ", iPlayer, " Ped to clone is null")
		ENDIF
	ENDFOR
	
	RETURN bReturn

ENDFUNC

FUNC BOOL CLONE_SCRIPTED_PLAYERS(INT iteam, BOOL bIncludePlayers = TRUE, BOOL bCanUseOriginalPlayerPeds = FALSE)

	IF NOT bIncludePlayers
		PRINTLN("[Cutscenes] CLONE_SCRIPTED_PLAYERS - No need to clone peds")
		RETURN TRUE
	ENDIF
	
	PLAYER_INDEX piPlayer
	PED_INDEX tempPed, tempPed2
	VECTOR vPedCoords
	FLOAT fPedHeading
	MODEL_NAMES tempModel
	PED_TYPE pedType
	BOOL bReturn = TRUE
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 tl63PedNameDBG
	#ENDIF
	
	INT iplayer, iplayer2
		
	FOR iplayer = 0 TO (FMMC_MAX_CUTSCENE_PLAYERS - 1)
		
		IF MC_serverBD.piCutscenePlayerIDs[iteam][iplayer] = INVALID_PLAYER_INDEX()
		OR (NOT IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer]))
			RELOOP
		ENDIF
						
		IF bCanUseOriginalPlayerPeds
		AND DOES_ENTITY_EXIST(MocapCachedIntroCutscenePlayerPedClone[iteam][iplayer])
			PRINTLN("[PED CUT] * CLONE_SCRIPTED_PLAYERS - USING CACHED CLONE PED Team: ", iteam, " Player: ", iplayer)
			tempPed = MocapCachedIntroCutscenePlayerPedClone[iteam][iplayer]
			piPlayer = MocapCachedIntroCutscenePlayer[iTeam][iplayer]
			tempModel = GET_ENTITY_MODEL(tempPed)			
			PRINTLN("[Cutscenes] CLONE_SCRIPTED_PLAYERS - Cloning MocapCachedIntroCutscenePlayerPedClone[", iteam, "][", iplayer, "], player name = ", GET_PLAYER_NAME(piPlayer))
		ELSE
			tempPed = GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer])
			piPlayer = MC_serverBD.piCutscenePlayerIDs[iteam][iplayer]
			tempModel = GET_ENTITY_MODEL(tempPed)
			PRINTLN("[Cutscenes] CLONE_SCRIPTED_PLAYERS - Cloning MC_serverBD.piCutscenePlayerIDs[", iteam, "][", iplayer, "], player name = ", GET_PLAYER_NAME(piPlayer))
		ENDIF
					
		REQUEST_MODEL(tempModel)
		
		IF NOT HAS_MODEL_LOADED(tempModel)
			PRINTLN("[Cutscenes] CLONE_SCRIPTED_PLAYERS - Waiting for player ",iplayer,"'s model ",ENUM_TO_INT(tempModel))
			bReturn = FALSE
			RELOOP
		ENDIF
		
		IF DOES_ENTITY_EXIST(MocapPlayerPed[iplayer])
			//Check the ped is ready for the cutscene
			IF NOT IS_PED_INJURED(MocapPlayerPed[iplayer])
			AND NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(MocapPlayerPed[iplayer]) 
				PRINTLN("[Cutscenes] CLONE_SCRIPTED_PLAYERS - Waiting for streaming requests on player ",iplayer,"'s clone")
				bReturn = FALSE
			ENDIF
			
			RELOOP
		ENDIF
		
		vPedCoords = GET_ENTITY_COORDS(tempPed)
		fPedHeading = GET_ENTITY_HEADING(tempPed)
				
		PRINTLN("[Cutscenes] CLONE_SCRIPTED_PLAYERS - Player vPedCoords ",vPedCoords,", fPedHeading ",fPedHeading)
		
		IF IS_PLAYER_PED_FEMALE(piPlayer)
			pedType = PEDTYPE_CIVFEMALE
		ELSE
			pedType = PEDTYPE_CIVMALE
		ENDIF
						
		MocapPlayerPed[iplayer] = CREATE_PED(pedType, tempModel, vPedCoords, fPedHeading, FALSE, FALSE)		
		
		CLEAR_PED_BLOOD_DAMAGE(MocapPlayerPed[iplayer])		
		//CLONE_PED_TO_TARGET(tempPed, MocapPlayerPed[iplayer])
		CLONE_PED_TO_TARGET_ALT(tempPed, MocapPlayerPed[iplayer]) // Switch back to CLONE_PED_TO_TARGET if we have any issues... This was added to include tattoos
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MocapPlayerPed[iplayer], TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(MocapPlayerPed[iplayer], rgFM_AiLike)
		SET_ENTITY_INVINCIBLE(MocapPlayerPed[iplayer], TRUE)
		SET_ENTITY_VISIBLE(MocapPlayerPed[iplayer], FALSE)
		
		#IF IS_DEBUG_BUILD
		tl63PedNameDBG = "CLONE_"
		tl63PedNameDBG += iplayer
		tl63PedNameDBG += " - "
		tl63PedNameDBG += GET_PLAYER_NAME(piPlayer)
		SET_PED_NAME_DEBUG(MocapPlayerPed[iplayer], tl63PedNameDBG)
		#ENDIF
		
		INT iHatIndex = GET_PED_PROP_INDEX( tempPed, ANCHOR_HEAD )
		INT iHatTexture = GET_PED_PROP_TEXTURE_INDEX( tempPed, ANCHOR_HEAD )
		
		IF iHatIndex > -1 AND iHatTexture > -1
			SET_PED_PROP_INDEX( MocapPlayerPed[iplayer], ANCHOR_HEAD, iHatIndex, iHatTexture ) 
		ENDIF
		
		FOR iPlayer2 = 0 TO (FMMC_MAX_CUTSCENE_PLAYERS - 1)
			
			IF MC_serverBD.piCutscenePlayerIDs[iteam][iPlayer2] = INVALID_PLAYER_INDEX()
			OR (NOT IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[iteam][iPlayer2]))
				RELOOP
			ENDIF
			
			tempPed2 = GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[iteam][iPlayer2])
			
			PRINTLN("[Cutscenes] CLONE_SCRIPTED_PLAYERS - Setting ped ",iplayer," no collision with cutscene player ",iPlayer2,", name ",GET_PLAYER_NAME(MC_serverBD.piCutscenePlayerIDs[iteam][iPlayer2]))
			SET_ENTITY_NO_COLLISION_ENTITY(MocapPlayerPed[iplayer], tempPed2, FALSE)
		ENDFOR
		
		bReturn = FALSE // Need to wait for streaming requests to complete before moving on
		
	ENDFOR

	IF bReturn = FALSE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC SET_REGISTERED_ENTITIES_VISIBLE_FOR_CUTSCENE(FMMC_CUTSCENE_TYPE eCutType, INT iCutsceneToUse)
	
	IF eCutType != FMMCCUT_MOCAP
	AND eCutType != FMMCCUT_ENDMOCAP	
		EXIT
	ENDIF
	
	IF eCutType = FMMCCUT_MOCAP
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_ForceRegisteredEntitiesVisible)
		EXIT
	ENDIF
	
	IF eCutType = FMMCCUT_ENDMOCAP
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet2, ci_CSBS2_ForceRegisteredEntitiesVisible)
		EXIT
	ENDIF
	
	PRINTLN("[LM][Cutscenes] PROCESS_MOCAP_CUTSCENE - SET_REGISTERED_ENTITIES_VISIBLE_FOR_CUTSCENE - Checking for entities that should be visible.")
	
	TEXT_LABEL_31 tl31Handle 
	ENTITY_INDEX entID
	NETWORK_INDEX netID
	INT i
	
	FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES-1
			
		IF eCutType = FMMCCUT_MOCAP
			IF iCutsceneToUse <= -1
				EXIT
			ENDIF
			IF g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iType = -1
			OR g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iIndex = -1
				RELOOP
			ENDIF
			tl31Handle = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].tlCutsceneHandle
			
			IF IS_STRING_NULL_OR_EMPTY(tl31Handle)
				IF g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iType = CREATION_TYPE_PROPS
					EntID = GET_CUTSCNE_VISIBLE_PROP(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iIndex)
					IF EntID != NULL
						SET_ENTITY_VISIBLE(EntID, TRUE)
					ENDIF
				ELSE
					netID = GET_CUTSCNE_VISIBLE_NET_ID(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iType, g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iIndex)
					PRINTLN("[LM][Cutscenes] PROCESS_MOCAP_CUTSCENE - SET_REGISTERED_ENTITIES_VISIBLE_FOR_CUTSCENE - Checking for entities that should be visible.")
					IF netID != NULL
						PRINTLN("[LM][Cutscenes] PROCESS_MOCAP_CUTSCENE - SET_REGISTERED_ENTITIES_VISIBLE_FOR_CUTSCENE - Registered Cutscene Prop without Handle. Calling SET_ENTITY_VISIBLE with TRUE")
						SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(netID, TRUE, TRUE)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[LM][Cutscenes] PROCESS_MOCAP_CUTSCENE - SET_REGISTERED_ENTITIES_VISIBLE_FOR_CUTSCENE - Prop has a handle...")
			ENDIF
			
		ELIF eCutType = FMMCCUT_ENDMOCAP
			IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType =-1
			OR g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex = -1
				RELOOP
			ENDIF
			tl31Handle = g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].tlCutsceneHandle
			
			IF IS_STRING_NULL_OR_EMPTY(tl31Handle)
				IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType = CREATION_TYPE_PROPS
					EntID = GET_CUTSCNE_VISIBLE_PROP(g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex)
					IF EntID != NULL
						SET_ENTITY_VISIBLE(EntID, TRUE)
					ENDIF
				ELSE
					netID = GET_CUTSCNE_VISIBLE_NET_ID(g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType, g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex)					
					IF netID != NULL
						PRINTLN("[LM][Cutscenes] PROCESS_MOCAP_CUTSCENE - SET_REGISTERED_ENTITIES_VISIBLE_FOR_CUTSCENE - Registered Cutscene Entity without Handle. Calling SET_NETWORK_ID_VISIBLE_IN_CUTSCENE with TRUE, TRUE")						
						SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(netID, TRUE, TRUE)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[LM][Cutscenes] PROCESS_MOCAP_CUTSCENE - SET_REGISTERED_ENTITIES_VISIBLE_FOR_CUTSCENE - Entity has a handle...")
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL ARE_ALL_PLAYERS_SYNCED_FOR_ENDMOCAP_PASSOVER_ENTITY_REGISTRATION()
	   
	INT iPart = -1
	WHILE DO_PARTICIPANT_LOOP(iPart)
		IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet4, PBBOOL4_REGISTERED_ENTITIES_FOR_END_MOCAP_PASS_OVER)
			PRINTLN("[LM][ARE_ALL_PLAYERS_SYNCED_FOR_ENDMOCAP_PASSOVER_ENTITY_REGISTRATION] Waiting for ", iPart, " so returning FALSE") 
			RETURN FALSE
		ENDIF		
	ENDWHILE
	
	PRINTLN("[LM][ARE_ALL_PLAYERS_SYNCED_FOR_ENDMOCAP_PASSOVER_ENTITY_REGISTRATION] SYNCED, returning TRUE")
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: This fuction returns TRUE unless we've got a special dropoff type
///    eg apartment/garage and we want to wait for the apartment cutscenes to finish
///    before starting the mocaps
FUNC BOOL HAVE_MOCAP_START_CONDITIONS_BEEN_MET(INT iCutscene, FMMC_CUTSCENE_TYPE ecutType)
	
	IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE)
	AND NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF)
	AND NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_NO_MOCAP_AFTER_THIS_DROPOFF)
		RETURN FALSE
	ENDIF
	
	// Player is using the heli_cam turret system
	IF GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].iTurretSeat != -1
		RETURN FALSE
	ENDIF
	
	// Have cutscene entities spawned?
	IF NOT HAS_NET_TIMER_STARTED(tdCutsceneWaitForEntitiesTimer)
		START_NET_TIMER(tdCutsceneWaitForEntitiesTimer)
		PRINTLN("[LM][HAVE_MOCAP_START_CONDITIONS_BEEN_MET] - Starting safety Timer...")
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(tdCutsceneWaitForEntitiesTimer)
	AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdCutsceneWaitForEntitiesTimer, ciCUTSCENE_WAIT_FOR_ENTITIES_TIME)
		PRINTLN("[LM][HAVE_MOCAP_START_CONDITIONS_BEEN_MET] - Checking Entities.")
		INT i = 0
		FOR i = 0 TO (FMMC_MAX_CUTSCENE_ENTITIES-1)
			SWITCH ecutType
				CASE FMMCCUT_SCRIPTED 
					IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType != -1
					AND g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex != -1
						IF SHOULD_CUTSCENE_ENTITY_BE_SPAWNED(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType, g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex)
							PRINTLN("[LM][HAVE_MOCAP_START_CONDITIONS_BEEN_MET] - (Scripted) iType: ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType, " Index: ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex, " Return False.")
							RETURN FALSE
						ELSE
							PRINTLN("[LM][HAVE_MOCAP_START_CONDITIONS_BEEN_MET] - (Scripted) iType: ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType, " Index: ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex, " Is Spawned/Ready")
						ENDIF
					ENDIF
				BREAK
				CASE FMMCCUT_MOCAP
					IF g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iType != -1
					AND g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iIndex != -1
						IF SHOULD_CUTSCENE_ENTITY_BE_SPAWNED(g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iType, g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iIndex)
							PRINTLN("[LM][HAVE_MOCAP_START_CONDITIONS_BEEN_MET] - (MidMocap) iType: ", g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iType, " Index: ", g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iIndex, " Return False.")
							RETURN FALSE
						ELSE
							PRINTLN("[LM][HAVE_MOCAP_START_CONDITIONS_BEEN_MET] - (MidMocap) iType: ", g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iType, " Index: ", g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iIndex, " Is Spawned/Ready")
						ENDIF
					ENDIF
				BREAK
				CASE FMMCCUT_ENDMOCAP
					IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType != -1
					AND g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex != -1
						IF SHOULD_CUTSCENE_ENTITY_BE_SPAWNED(g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType, g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex)
							PRINTLN("[LM][HAVE_MOCAP_START_CONDITIONS_BEEN_MET] - (EndMocap) iType: ", g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType, " Index: ", g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex, " Return False.")
							RETURN FALSE
						ELSE
							PRINTLN("[LM][HAVE_MOCAP_START_CONDITIONS_BEEN_MET] - (EndMocap) iType: ", g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType, " Index: ", g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex, " Is Spawned/Ready")
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDFOR
	ELSE
		PRINTLN("[LM][HAVE_MOCAP_START_CONDITIONS_BEEN_MET] - Timeout, returning true...")
	ENDIF
	
	RETURN TRUE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Mocap Cutcene   -------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing of Motion Captured Cutscenes. 									 							 								 ---------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(eFMMC_MOCAP_RUNNING_PROGRESS eNewState)
	PRINTLN("[LM][SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS][Cutscene_Mocap] - Setting Mocap Running State from ", 
	GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS_NAME(eMocapRunningCutsceneProgress), " to ", GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS_NAME(eNewState))
	
	DEBUG_PRINTCALLSTACK()
	eMocapRunningCutsceneProgress = eNewState
ENDPROC

PROC SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(eFMMC_MOCAP_PROGRESS eNewState)
	PRINTLN("[LM][SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS][PROCESS_MOCAP_CUTSCENE] - Setting Mocap Manage State from ", 
	GET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS_NAME(eMocapManageCutsceneProgress), " to ", GET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS_NAME(eNewState))
	
	DEBUG_PRINTCALLSTACK()
	eMocapManageCutsceneProgress = eNewState
ENDPROC

PROC SET_MOCAP_STREAMING_STAGE(MOCAP_STREAMING_STAGE eNewState)
	PRINTLN("[LM][SET_MOCAP_STREAMING_STAGE] Moving From ", GET_MOCAP_STREAMING_STAGE_DEBUG_NAME(eMocapStreamingStage), " to >>>>>>>> ", GET_MOCAP_STREAMING_STAGE_DEBUG_NAME(eNewState))
	eMocapStreamingStage = eNewState
ENDPROC

PROC PROCESS_CUTSCENE_INIT_SPECIAL_CASE(STRING sMocapCutsceneName)
	
	IF ARE_STRINGS_EQUAL(sMocapCutsceneName, "FIX_TRIP3_MCS1")
	AND MC_playerBD[iLocalPart].iTeam = 0
		IF g_bTRIP3_MCS1_ForceSniper
			wtPreCutsceneWeapon = WEAPONTYPE_HEAVYSNIPER
		ENDIF
	ENDIF

	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		CONCEAL_ALL_OTHER_PLAYERS(TRUE)	
	ENDIF		
ENDPROC
		
PROC PROCESS_CUTSCENE_JUST_ENDING_SPECIAL_CASE(STRING sMocapCutsceneName)
	IF ARE_STRINGS_EQUAL(sMocapCutsceneName, "FIX_TRIP3_MCS1")
	AND MC_playerBD[iLocalPart].iTeam = 0
		IF g_bTRIP3_MCS1_ForceSniper
			PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_JUST_ENDING_SPECIAL_CASE - FIX_TRIP3_MCS1 - Forcing Sniper")
			iForceSniperScopeTime = g_iTRIP3_MCS1_Timer
			iForceSniperScopeTargetPed = g_iTRIP3_MCS1_PedTarget
			SET_BIT(iLocalBoolCheck11, LBOOL11_FORCE_SNIPER_SCOPE)
			PROCESS_FORCE_SNIPER_SCOPE_AIM() //Call it once to start off
		ENDIF
	ENDIF
ENDPROC
		
PROC PROCESS_CUTSCENE_FINAL_SPECIAL_CASE(STRING sMocapCutsceneName)
	IF ARE_STRINGS_EQUAL(sMocapCutsceneName, "FIX_BIL_MCS2")
		IF NOT IS_IPL_ACTIVE("sf_heli_crater")
			REQUEST_IPL("sf_heli_crater")
			PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_FINAL_SPECIAL_CASE - FIX_BIL_MCS2 - Adding heli crater")
		ENDIF
	ENDIF	
	
	IF ARE_STRINGS_EQUAL(sMocapCutsceneName, "FIX_TRIP2_INT")
	OR ARE_STRINGS_EQUAL(sMocapCutsceneName, "FIX_TRIP3_INT")
		PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_FINAL_SPECIAL_CASE - FIX_TRIP2_INT/FIX_TRIP3_INT - Playing character switch FX")
		ANIMPOSTFX_PLAY("SwitchSceneFranklin", 0, FALSE)
	ENDIF
ENDPROC

//Do pre mocap checks prior to cutscene launching
PROC DO_PRE_MOCAP_CHECKS(INT iCutsceneToUse)
	
	PRINTLN("[Cutscene_Mocap] - DOING PRE MOCAP CHECKS FOR ", sMocapCutscene)
		
	//Check for cutscene in hide mocap block list
	IF NOT DOES_MOCAP_CUTSCENE_NEED_PLAYERS(sMocapCutscene, iCutsceneToUse)
		//Block players from being cloned and passed into the cutscene
		SET_BIT(iLocalBoolCheck7, LBOOL7_HIDE_MOCAP_PLAYERS)
		PRINTLN("[Cutscene_Mocap] - HIDE MOCAP PLAYERS SET")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet4, ci_CSBS4_PlayCutsceneSFX_TurnOnInteriorLights_CutStart)
		IF eLocalArtificialLightsState != eARTIFICIAL_LIGHTS_STATE__ON
			TURN_ON_ARTIFICIAL_LIGHTS_LOCALLY(FALSE)
		ENDIF
	ELIF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet4, ci_CSBS4_PlayCutsceneSFX_TurnOffInteriorLights_CutStart)
		IF eLocalArtificialLightsState != eARTIFICIAL_LIGHTS_STATE__OFF
			TURN_OFF_ARTIFICIAL_LIGHTS_LOCALLY(FALSE)
		ENDIF
	ENDIF
	
ENDPROC

//Do post mocap checks to position objects etc...
PROC DO_POST_MOCAP_CHECKS()

	PRINTLN("[Cutscenes] DOING POST MOCAP CHECKS FOR ", sMocapCutscene)
	
ENDPROC

PROC MAINTAIN_REMOTE_PLAYER_MOCAP_ANIMATIONS()

	INT i,iplayer
	// Process all other players animations, playing on their clones
	FOR i= 0 TO (FMMC_MAX_CUTSCENE_PLAYERS-1)
		IF MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][i] != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][i])
				iplayer = NATIVE_TO_INT(MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][i])
				IF MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][i] != PlayerToUse
				OR bIsSCTV
					IF DOES_ENTITY_EXIST(MocapPlayerPed[i])
						UPDATE_INTERACTION_ANIM_FOR_PED(MocapPlayerPed[i], iplayer, gInteractionsPedsData[iplayer])
					ENDIF
				ELSE	
					IF DOES_ENTITY_EXIST(LocalPlayerPed)
						UPDATE_INTERACTION_ANIM_FOR_PED(LocalPlayerPed, iplayer, gInteractionsPedsData[iplayer])
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
	ENDFOR

ENDPROC

PROC MANAGE_PLAYER_GESTURES()

	IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_MOCAP_PLAYER_PLAYING_ANIM)
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LS)
		OR NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RS)
		OR NOT HAS_ANIM_EVENT_FIRED(LocalPlayerPed,GET_HASH_KEY("ALLOW_MP_GESTURE"))
			// Mark ourselves as not playing
			MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].bHoldLoop = FALSE 
			
			CLEAR_BIT(iLocalBoolCheck2, LBOOL2_MOCAP_PLAYER_PLAYING_ANIM)
			IF NOT IS_MP_PLAYER_ANIM_SETTING_CREW_DEFAULT()  
				//makes sure the player and clones can't to the bird anim on certain mocaps where there is not enough space to open their arms.
				BROADCAST_MOCAP_PLAYER_ANIMATION(ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER), GET_MP_PLAYER_ANIM_SETTING() , FALSE)
			ELSE
				BROADCAST_MOCAP_PLAYER_ANIMATION(ENUM_TO_INT(INTERACTION_ANIM_TYPE_CREW), MPGlobalsInteractions.iMyCrewInteractionAnim , FALSE)
		    ENDIF
		ENDIF
	ENDIF
	IF NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_MOCAP_PLAYER_PLAYING_ANIM)
	AND HAS_ANIM_EVENT_FIRED(LocalPlayerPed,GET_HASH_KEY("ALLOW_MP_GESTURE"))	
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LS)
		AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RS)
			
			SET_BIT(iLocalBoolCheck2, LBOOL2_MOCAP_PLAYER_PLAYING_ANIM)

			IF NOT IS_MP_PLAYER_ANIM_SETTING_CREW_DEFAULT() 
				//makes sure the player and clones can't to the bird anim on certain mocaps where there is not enough space to open their arms.
				MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].iInteractionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER) 
				   MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].iInteractionAnim = GET_MP_PLAYER_ANIM_SETTING()
			ELSE
				MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].iInteractionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_CREW)
				MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].iInteractionAnim = MPGlobalsInteractions.iMyCrewInteractionAnim
		    ENDIF
			
			MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].bPlayInteractionAnim = TRUE
	        MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].bHoldLoop = TRUE
			BROADCAST_MOCAP_PLAYER_ANIMATION(MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].iInteractionAnim, MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].iInteractionAnim , TRUE)
			
		ENDIF
	ENDIF
	
	MAINTAIN_REMOTE_PLAYER_MOCAP_ANIMATIONS()
		
ENDPROC

//Do checks immediately on running mocap
PROC DO_RUN_MOCAP_CHECKS()

	PRINTLN("[Cutscenes] DOING MOCAP RUN CHECKS FOR ", sMocapCutscene)
	
	IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_LOCAL_PLAYER_IN_CUTSCENE)
		SET_ENTITY_VISIBLE(LocalPlayerPed, TRUE)
	ENDIF
	
ENDPROC

//Do checks immediately after running mocap
PROC DO_POST_RUN_MOCAP_CHECKS()

	PRINTLN("[Cutscenes] Doing post-mocap checks for ", sMocapCutscene )

	//Unpin any existing interior
	IF IS_VALID_INTERIOR(iCutInterior)
		UNPIN_INTERIOR(iCutInterior)
	ENDIF
		
ENDPROC

//Cutscene handles manually being checked for exit states
PROC HANDLE_MANUAL_EXIT_STATES( INT iCutsceneNum, INT iCutsceneTeam, VECTOR vEndPos, FLOAT fEndHeading, FMMC_CUTSCENE_TYPE eCutType)//, WEAPON_TYPE weaponToArmOnExitState)
		
		UNUSED_PARAMETER(eCutType)

		//************************************CUTSCENE SPECIFIC EXIT STATES***************************
		// If doing manual exit state for a player make sure bit LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE
		// is set as CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY will only return once and won't enter
		// the default checks below otherwise.
		// In the event the cutscene has no player handle then the player logic is handled within
		// the camera exit state e.g. cutscene that don't include the player
		//********************************************************************************************
		
		IF ARE_STRINGS_EQUAL(sMocapCutscene, "subj_mcs0")
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_1")
				PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE subj_mcs0 - can set exit state - MP_1")
				IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneNum].iCutsceneBitSet2, ci_CSBS2_EquipOutfitForNextRule)
					SET_BIT(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
					PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE - subj_mcs0 - Setting LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE")
				ENDIF
				
				//-- Remove scuba tank & mask
				IF NOT IS_PED_FEMALE(LocalPlayerPed)
					SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_SPECIAL, 15, 0) 
				ELSE
					SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_SPECIAL, 3, 0) 
				ENDIF
				FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, TRUE, TRUE)
				
				SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_2")
				PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE subj_mcs0 - can set exit state - MP_2")
				IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneNum].iCutsceneBitSet2, ci_CSBS2_EquipOutfitForNextRule)
					SET_BIT(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
					PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE - subj_mcs0 - Setting LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE")
				ENDIF
				
				//-- Remove scuba tank & mask
				IF NOT IS_PED_FEMALE(LocalPlayerPed)
					SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_SPECIAL, 15, 0) 
				ELSE
					SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_SPECIAL, 3, 0) 
				ENDIF
				FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, TRUE, TRUE)
				
				SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
			ENDIF
		ENDIF
		
		
		IF ARE_STRINGS_EQUAL(sMocapCutscene, "subj_mcs1")
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_1")
				PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE subj_mcs1 - can set exit state - MP_1")
				// Remove the parachute
				SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_HAND, 0, 0)


				// Flippers
				scrShopPedComponent componentItem
				IF NOT IS_PED_FEMALE(LocalPlayerPed)
				      GET_SHOP_PED_COMPONENT(HASH("DLC_MP_X17_M_FEET_0_0"), componentItem)
				ELSE
				      GET_SHOP_PED_COMPONENT(HASH("DLC_MP_X17_F_FEET_0_0"), componentItem)
				ENDIF
				SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_FEET, componentItem.m_drawableIndex, componentItem.m_textureIndex)

				// Forced components all set up in the meta data
				PED_COMP_NAME_ENUM eFlipperFeet = GET_PED_COMP_ITEM_FROM_VARIATIONS(LocalPlayerPed, componentItem.m_drawableIndex, componentItem.m_textureIndex, COMP_TYPE_FEET)
				IF eFlipperFeet != DUMMY_PED_COMP
				      SET_PED_COMP_ITEM_CURRENT_MP(LocalPlayerPed, COMP_TYPE_FEET, eFlipperFeet, FALSE) 
				ENDIF
				CLEAR_PED_PROP(LocalPlayerPed, ANCHOR_HEAD)

				IF NOT IS_PED_FEMALE(LocalPlayerPed)
				   	SET_PLAYER_PREVIOUS_VARIATION_DATA(LocalPlayer, PED_COMP_SPECIAL, 15, 0, 0, 0)
				ELSE
				    SET_PLAYER_PREVIOUS_VARIATION_DATA(LocalPlayer, PED_COMP_SPECIAL, 3, 0, 0, 0)
				ENDIF

				SET_ENABLE_SCUBA(LocalPlayerPed, TRUE)
				
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableTakeOffScubaGear, TRUE)
				
				SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_2")
				PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE subj_mcs1 - can set exit state - MP_2")
				// Remove the parachute
				SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_HAND, 0, 0)


				// Flippers
				scrShopPedComponent componentItem
				IF NOT IS_PED_FEMALE(LocalPlayerPed)
				    GET_SHOP_PED_COMPONENT(HASH("DLC_MP_X17_M_FEET_0_0"), componentItem)
				ELSE
				    GET_SHOP_PED_COMPONENT(HASH("DLC_MP_X17_F_FEET_0_0"), componentItem)
				ENDIF
				SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_FEET, componentItem.m_drawableIndex, componentItem.m_textureIndex)

				// Forced components all set up in the meta data
				PED_COMP_NAME_ENUM eFlipperFeet = GET_PED_COMP_ITEM_FROM_VARIATIONS(LocalPlayerPed, componentItem.m_drawableIndex, componentItem.m_textureIndex, COMP_TYPE_FEET)
				IF eFlipperFeet != DUMMY_PED_COMP
				    SET_PED_COMP_ITEM_CURRENT_MP(LocalPlayerPed, COMP_TYPE_FEET, eFlipperFeet, FALSE) 
				ENDIF
				CLEAR_PED_PROP(LocalPlayerPed, ANCHOR_HEAD)

				IF NOT IS_PED_FEMALE(LocalPlayerPed)
				    SET_PLAYER_PREVIOUS_VARIATION_DATA(LocalPlayer, PED_COMP_SPECIAL, 15, 0, 0, 0)
				ELSE
				    SET_PLAYER_PREVIOUS_VARIATION_DATA(LocalPlayer, PED_COMP_SPECIAL, 3, 0, 0, 0)
				ENDIF

				SET_ENABLE_SCUBA(LocalPlayerPed, TRUE)
				
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableTakeOffScubaGear, TRUE)
				
				SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)

			ENDIF
			
			
		ENDIF
		
		//************************************DEFAULT EXIT STATE CHECK FOR PLAYERS****************************
		IF UPDATE_ON_PLAYER_EXIT_STATE(iCutsceneNum, iCutsceneTeam, vEndPos, fEndHeading)
			//Clear manual exit state bit
			IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
				CLEAR_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
			ENDIF
			
			//Set the bit that a player exit has been carried out
			IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_PLAYER_EXIT_STATE_CALLED)
				PRINTLN("[Cutscenes] SETTING LBOOL8_PLAYER_EXIT_STATE_CALLED")
				SET_BIT(iLocalBoolCheck8, LBOOL8_PLAYER_EXIT_STATE_CALLED)
			ENDIF
		ENDIF
	
ENDPROC

///PURPOSE: This function deals with all cutscene events for mocaps
PROC LISTEN_FOR_CUTSCENE_EVENTS( STRING sMocapName )
	
	// Note: Might not be able to work up a system to achieve this creator side, we probably want to keep this function around. Leaving a singular case here for easy reference.
	IF ARE_STRINGS_EQUAL(sMocapName, "HS3F_SUB_CEL")
		PRINTLN("[Cutscenes] HS3F_SUB_CEL Checking for Anim events.")
		
		ENTITY_INDEX entSub = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("brucie")
		IF IS_ENTITY_ALIVE(entSub)
			PRINTLN("[Cutscenes] HS3F_SUB_CEL Entity Handle 'Brucie' Exists.")
			IF HAS_ANIM_EVENT_FIRED(entSub, GET_HASH_KEY("brucie_shot"))
				PRINTLN("[Cutscenes] HS3F_SUB_CEL - Brucie_Shot for Brucie.")				
				PED_INDEX pedSub = GET_PED_INDEX_FROM_ENTITY_INDEX(entSub)
				IF NOT IS_PED_INJURED(pedSub)
					APPLY_BLOOD_TO_PED(10, pedSub)
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[Cutscenes] HS3F_SUB_CEL Entity Handle 'Brucie' did not give a valid entity.")
		ENDIF
		
	ELIF ARE_STRINGS_EQUAL(sMocapName, "HS4F_EXT")
	
		scrShopPedComponent componentItem
		ENTITY_INDEX entPlayer
		TEXT_LABEL tl23_player
		
		INT i = 0
		FOR i = 0 TO GET_MOCAP_NUMBER_OF_PLAYERS(sMocapName)-1
			tl23_player = "MP_"
			tl23_player += i+1
			
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - HS4F_EXT - tl23_player: ", tl23_player)
			
			entPlayer = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY(tl23_player)
			
			IF DOES_ENTITY_EXIST(entPlayer)	
			AND NOT IS_ENTITY_DEAD(entPlayer)			
				
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - HS4F_EXT - Player Exists")
				
				GET_SHOP_PED_COMPONENT(HASH("DLC_MP_X17_M_FEET_0_0"), componentItem)
			
				PED_INDEX pedPlayer = GET_PED_INDEX_FROM_ENTITY_INDEX(EntPlayer)
			
				IF GET_PED_DRAWABLE_VARIATION(pedPlayer, PED_COMP_FEET) = componentItem.m_drawableIndex					
					IF NOT IS_PED_FEMALE(pedPlayer)
						GET_SHOP_PED_COMPONENT(HASH("DLC_MP_X17_M_FEET_2_0"), componentItem)
					ELSE
						GET_SHOP_PED_COMPONENT(HASH("DLC_MP_X17_F_FEET_2_0"), componentItem)
					ENDIF			
					
					SET_PED_COMPONENT_VARIATION(pedPlayer, PED_COMP_FEET, componentItem.m_drawableIndex, componentItem.m_textureIndex)										
				ENDIF
				
				IF IS_PED_WEARING_SCUBA_TANK(pedPlayer)
					PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - HS4F_EXT - Clearing scuba tank.")
					IF NOT IS_PED_FEMALE(pedPlayer)
						SET_PED_COMPONENT_VARIATION(pedPlayer, PED_COMP_SPECIAL, 15, 0)
					ELSE
						SET_PED_COMPONENT_VARIATION(pedPlayer, PED_COMP_SPECIAL, 3, 0)
					ENDIF
				ENDIF
				
				IF GET_PED_PROP_INDEX(pedPlayer, ANCHOR_EYES) = 26
				OR GET_PED_PROP_INDEX(pedPlayer, ANCHOR_EYES) = 28
					PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - HS4F_EXT - Clearing scuba mask")
					SET_PED_PROP_INDEX(pedPlayer, ANCHOR_EYES, 0, 0)
				ENDIF
				
				IF GET_PED_DRAWABLE_VARIATION(pedPlayer, PED_COMP_BERD) = 122
					PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - HS4F_EXT - Clearing scuba balaclava")
					SET_PED_COMPONENT_VARIATION(pedPlayer, PED_COMP_BERD, 0, 0)
				ENDIF
			ENDIF
		ENDFOR

	ELIF ARE_STRINGS_EQUAL(sMocapName, "TUNF_IAA_MCS1")
	
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - TUNF_IAA_MCS1 GET_CUTSCENE_TIME(): ", GET_CUTSCENE_TIME(), " GET_CUTSCENE_END_TIME(): ", GET_CUTSCENE_END_TIME())
		
		IF NOT IS_BIT_SET(iCutsceneEventBS, ciCUTSCENE_EVENT_GENERIC_0)
			ENTITY_INDEX entSub = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("TUN_DEALER1")
			ENTITY_INDEX entPuncher = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_1")
			IF IS_ENTITY_ALIVE(entSub)
			AND IS_ENTITY_ALIVE(entPuncher)
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - TUNF_IAA_MCS1 Entity Handle 'TUN_DEALER1' Exists.")
				
				IF TO_FLOAT(GET_CUTSCENE_TIME()) / TO_FLOAT(GET_CUTSCENE_END_TIME()) >= 0.398
					PED_INDEX pedSub = GET_PED_INDEX_FROM_ENTITY_INDEX(entSub)
					PED_INDEX pedPuncher = GET_PED_INDEX_FROM_ENTITY_INDEX(entPuncher)
					IF NOT IS_PED_INJURED(pedSub)
					AND NOT IS_PED_INJURED(pedPuncher)
						PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - TUNF_IAA_MCS1 Entity Handle 'TUN_DEALER1' Applying Damage!")
						APPLY_BLOOD_TO_PED(11, pedSub)
						APPLY_BLOOD_TO_PED(12, pedPuncher)
						
						SET_BIT(iCutsceneEventBS, ciCUTSCENE_EVENT_GENERIC_0)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - TUNF_IAA_MCS1 Entity Handle 'TUN_DEALER1' did not give a valid entity.")
			ENDIF
		ENDIF
	
	ELIF ARE_STRINGS_EQUAL(sMocapName, "FIXF_FIN_MCS2")
		
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIXF_FIN_MCS2 Processing Events.")
		
		IF NOT DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIXF_FIN_MCS2_PhoneScreen])
			REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_DrP_01a")))
			IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_DrP_01a")))
				eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIXF_FIN_MCS2_PhoneScreen] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_DrP_01a")), <<10, 10, 73>>, FALSE, FALSE)
				SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_DrP_01a")))
				PRINTLN("[DREPHONE] - Created phone. sf_Prop_SF_Scrn_DrP_01a")
			ELSE
				PRINTLN("[DREPHONE] - Waiting for phone model. sf_Prop_SF_Scrn_DrP_01a")
			ENDIF
		ENDIF
		
		ENTITY_INDEX oiDrePhone = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("TUN_DOOR1_Phone", INT_TO_ENUM(MODEL_NAMES, HASH("Prop_NPC_Phone")))
		
		IF DOES_ENTITY_EXIST(oiDrePhone)
		AND DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIXF_FIN_MCS2_PhoneScreen])
		
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIXF_FIN_MCS2 - Phone exists.")
						
			IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIXF_FIN_MCS2_PhoneScreen], oiDrePhone)	
				SET_ENTITY_COORDS(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIXF_FIN_MCS2_PhoneScreen], GET_ENTITY_COORDS(oiDrePhone, FALSE))
				ATTACH_ENTITY_TO_ENTITY(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIXF_FIN_MCS2_PhoneScreen], oiDrePhone, 0, <<0.0, 0.0, 0.0>>, <<90.0, 0.0, 0.0>>)
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIXF_FIN_MCS2 - Attaching phone screen to phone cutscene entity")
			ENDIF
			
		ELSE
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIXF_FIN_MCS2 - Phone Entity Does not Exist.")
		ENDIF
		
		ENTITY_INDEX entPlayer = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_1")
		IF IS_ENTITY_ALIVE(entPlayer)
			IF HAS_ANIM_EVENT_FIRED(entPlayer, GET_HASH_KEY("start_track"))			
				IF NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIXF_FIN_MCS2_StartTrack1)
					SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIXF_FIN_MCS2_StartTrack1)
					PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIXF_FIN_MCS2 - Starting Dre Track.")
					SET_MUSIC_STATE(MUSIC_SILENT)
					SET_BIT(iLocalBoolCheck, LBOOL_MUSIC_CHANGED) 
					TRIGGER_MUSIC_EVENT("DFWD_CAR_TRACK_START")
					START_AUDIO_SCENE("DLC_Fixer_DL5_Music_Moment_Scene")
					PRINTLN("[Cutscenes][AUDIO] - LISTEN_FOR_CUTSCENE_EVENTS -  Starting DLC_Fixer_DL5_Music_Moment_Scene")
					SET_BIT(iLocalBoolCheck25, LBOOL25_MUSIC_STARTED_DRE_TRACK)
				ENDIF
			ENDIF
		ENDIF
		
		INT iPlayers = MC_serverBD.iNumStartingPlayers[0]
		
		PRINTLN("[Cutscene] - LISTEN_FOR_CUTSCENE_EVENTS - ", sMocapName, " - iPlayers: ", iPlayers)
		
		IF GET_CUTSCENE_TIME() >= 160000
			ENTITY_INDEX enthide = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Producer_SportsCar")
			IF IS_ENTITY_ALIVE(enthide)
				IF iPlayers = 2
				OR iPlayers = 3
					PRINTLN("[Cutscene] - LISTEN_FOR_CUTSCENE_EVENTS - ", sMocapName, " - iPlayers: ", iPlayers, " SET_ENTITY_LOCALLY_INVISIBLE Producer_SportsCar")				
					SET_ENTITY_LOCALLY_INVISIBLE(enthide)
				ENDIF
			ENDIF
			// this wasn't working but keeping here in case this comes back to script; url:bugstar:7445543 - Data Leak - Don't Fuck Wit Dre - FIXF_FIN_MCS2 - Golf club is left floating behind the car as the players and Dre drive away
			/*enthide = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Producer_golf", INT_TO_ENUM(MODEL_NAMES, HASH("SF_PROP_SF_GOLF_IRON_01a")))
			IF IS_ENTITY_ALIVE(enthide)
				PRINTLN("[Cutscene] - LISTEN_FOR_CUTSCENE_EVENTS - ", sMocapName, " - iPlayers: ", iPlayers, " SET_ENTITY_LOCALLY_INVISIBLE Producer_golf")				
				SET_ENTITY_LOCALLY_INVISIBLE(enthide)
			ELSE
				PRINTLN("[Cutscene] - LISTEN_FOR_CUTSCENE_EVENTS - ", sMocapName, " - iPlayers: ", iPlayers, " Producer_golf doesn't exist")	
			ENDIF*/
		ENDIF		
			
	ELIF ARE_STRINGS_EQUAL(sMocapName, "FIX_TRIP1_EXT")
		
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_EXT - Processing Events.")
		
		VECTOR vTruckLocation = <<139.6893, 157.9433, 105>>		
		FLOAT fTruckHeading = 182.5
		VEHICLE_INDEX vehGameplayMule
			
		vehGameplayMule = GET_CLOSEST_VEHICLE(vTruckLocation, 10.0, MULE5, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES)
				
		IF DOES_ENTITY_EXIST(vehGameplayMule)
		AND NETWORK_GET_ENTITY_IS_NETWORKED(vehGameplayMule)
			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(vehGameplayMule)
				IF NOT IS_VEHICLE_DRIVEABLE(vehGameplayMule)
					SET_VEHICLE_FIXED(vehGameplayMule)
					PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_EXT - Fixing Vehicle")				
				ENDIF
			ENDIF
			
			SET_ENTITY_VISIBLE_IN_CUTSCENE(vehGameplayMule, TRUE)
			
			IF IS_VEHICLE_DRIVEABLE(vehGameplayMule)
				IF NOT DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP1_EXT_Truck])
					PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_EXT - Creating Clone of Vehicle")
					
					IF CREATE_VEHICLE_CLONE(viTempForCutsceneEntities, vehGameplayMule, vTruckLocation+<<0.0, 0.0, 100.0>>, fTruckHeading, FALSE, FALSE)					
						eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP1_EXT_Truck] = viTempForCutsceneEntities	
						PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_EXT - Clone Created.")
						SET_ENTITY_LOAD_COLLISION_FLAG(viTempForCutsceneEntities, TRUE)
						FREEZE_ENTITY_POSITION(viTempForCutsceneEntities, TRUE)
						SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_RepositionExplodeTruck)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_EXT - Not Driveable waiting for owner to fix.")	
			ENDIF
			
			IF GET_ENTITY_MODEL(vehGameplayMule) = MULE5
			AND GET_CUTSCENE_TIME() > 10000
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(vehGameplayMule)
					DELETE_VEHICLE(vehGameplayMule)
					PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_EXT - DELETE_ENTITY on original mule.")
				ENDIF
			ENDIF
		
			EXIT
		
		ENDIF
		
		// NETWORKED VEH WOULDN'T WORK DUE TO TIMINGS AND OWNERSHIP ISSUES.
		IF IS_ENTITY_ALIVE(viTempForCutsceneEntities)
			
			IF NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_ExplodeTruck)
			AND IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_RepositionExplodeTruck)
				SET_ENTITY_COLLISION(viTempForCutsceneEntities, TRUE)
				SET_ENTITY_COORDS(viTempForCutsceneEntities, vTruckLocation)
				SET_ENTITY_HEADING(viTempForCutsceneEntities, fTruckHeading)
				FREEZE_ENTITY_POSITION(viTempForCutsceneEntities, FALSE)
				SET_ENTITY_DYNAMIC(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP1_EXT_Truck], TRUE)
				ACTIVATE_PHYSICS(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP1_EXT_Truck])
				CLEAR_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_RepositionExplodeTruck)
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_EXT - Repositioning Vehicle.")
			ENDIF
			
			IF NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_ExplodeTruck)
				ENTITY_INDEX eiFrank = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("FIX_FRANKLIN")
				ENTITY_INDEX eiLam = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("FIX_LAMAR")
				IF (IS_ENTITY_ALIVE(eiFrank) AND HAS_ANIM_EVENT_FIRED(eiFrank, GET_HASH_KEY("TruckExplosion")))
				OR (IS_ENTITY_ALIVE(eiLam) AND HAS_ANIM_EVENT_FIRED(eiLam, GET_HASH_KEY("TruckExplosion")))
					SET_ENTITY_COLLISION(viTempForCutsceneEntities, TRUE)
					FREEZE_ENTITY_POSITION(viTempForCutsceneEntities, FALSE)
					SET_ENTITY_DYNAMIC(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP1_EXT_Truck], TRUE)
					APPLY_FORCE_TO_ENTITY(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP1_EXT_Truck], APPLY_TYPE_IMPULSE, (<<-1.0, 0.0, 7.0>>), (<<0.0, 0.0, 0.0>>), 0, TRUE, TRUE, TRUE)
					EXPLODE_VEHICLE_IN_CUTSCENE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP1_EXT_Truck]), TRUE)				
					PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_EXT - TruckExplosion anim vent fired. ADD_EXPLOSION")
					SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_ExplodeTruck)
				ENDIF				
			ENDIF
		ENDIF		
		
	ELIF ARE_STRINGS_EQUAL(sMocapName, "FIX_TRIP3_EXT")		
		PROCESS_CUTSCENE_EVENTS_FOR_FIX_TRIP3_EXT()
		
	ELIF ARE_STRINGS_EQUAL(sMocapName, "FIX_TRIP3_MCS1")
		
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_MCS1 - Processing Events.")
		
		IF NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_TRIP3_MCS1_ExtendTaskRangeOnVagosPeds)
			NETWORK_SET_TASK_CUTSCENE_INSCOPE_MULTIPLER(3.0)
			SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_TRIP3_MCS1_ExtendTaskRangeOnVagosPeds)
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_MCS1 - Setting NETWORK_SET_TASK_CUTSCENE_INSCOPE_MULTIPLER(3.0).")		
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_MCS1_TabletScreen])
			REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_Tablet_01a")))
			IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_Tablet_01a")))
				eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_MCS1_TabletScreen] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_Tablet_01a")), <<10, 10, 73>>, FALSE, FALSE)
				SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_Tablet_01a")))
				PRINTLN("[DREPHONE] - Created phone. sf_Prop_SF_Scrn_Tablet_01a")
			ELSE
				PRINTLN("[DREPHONE] - Waiting for phone model. sf_Prop_SF_Scrn_Tablet_01a")
			ENDIF
		ENDIF
		
		ENTITY_INDEX oiTablet = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Marnie_Tablet", INT_TO_ENUM(MODEL_NAMES, HASH("hei_Prop_dlc_Tablet")))				
		
		IF DOES_ENTITY_EXIST(oiTablet)
		AND DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_MCS1_TabletScreen])
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_MCS1 - Tablet exists.")								
			IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_MCS1_TabletScreen], oiTablet)	
				SET_ENTITY_COORDS(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_MCS1_TabletScreen], GET_ENTITY_COORDS(oiTablet, FALSE))
				ATTACH_ENTITY_TO_ENTITY(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_MCS1_TabletScreen], oiTablet, 0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_MCS1 - Attaching Tablet screen to Tablet cutscene entity")
			ENDIF					
		ELSE
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_MCS1 - Tablet Entity Does not Exist.")
		ENDIF
	
	ELIF ARE_STRINGS_EQUAL(sMocapName, "FIX_TRIP1_INT")
	OR ARE_STRINGS_EQUAL(sMocapName, "FIX_TRIP2_INT")
	OR ARE_STRINGS_EQUAL(sMocapName, "FIX_TRIP3_INT")
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_INT / FIX_TRIP2_INT / FIX_TRIP3_INT - Processing Events.")
		
		ENTITY_INDEX entPlayer = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_1")
		IF IS_ENTITY_ALIVE(entPlayer)
			IF HAS_ANIM_EVENT_FIRED(entPlayer, GET_HASH_KEY("START_TRIP"))				
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_INT / FIX_TRIP2_INT / FIX_TRIP3_INT - (MP_1) - Setting FixerShortTrip fade in 10 sec.")				
				SET_TRANSITION_TIMECYCLE_MODIFIER("FixerShortTrip", 10.0)
			ENDIF
			
			IF HAS_ANIM_EVENT_FIRED(entPlayer, GET_HASH_KEY("MAX_TRIP"))				
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_INT / FIX_TRIP2_INT / FIX_TRIP3_INT - (MP_1) - Setting FixerShortTrip_Distort.")					
				SET_TIMECYCLE_MODIFIER("FixerShortTrip_Distort")
			ENDIF
			
			IF HAS_ANIM_EVENT_FIRED(entPlayer, GET_HASH_KEY("NO_DISTORT"))				
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_INT / FIX_TRIP2_INT / FIX_TRIP3_INT - (MP_1) - Clearing FixerShortTrip_Distort")
				CLEAR_TIMECYCLE_MODIFIER()
				SET_TIMECYCLE_MODIFIER("FixerShortTrip")
			ENDIF
			
			IF HAS_ANIM_EVENT_FIRED(entPlayer, GET_HASH_KEY("END_TRIP"))
			AND NOT ARE_STRINGS_EQUAL(sMocapName, "FIX_TRIP1_INT") // We want TRIP1_INT to fade this out during the leadout instead
				IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
				AND NOT GET_IS_TIMECYCLE_TRANSITIONING_OUT()
					PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_INT / FIX_TRIP2_INT / FIX_TRIP3_INT - (MP_1) - Setting FixerShortTrip fade out 10 sec.")									
					IF ARE_STRINGS_EQUAL(sMocapName, "FIX_TRIP3_INT")
						SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(20.0)
					ELSE
						SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(10.0)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		entPlayer = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("FIX_FRANKLIN")
		IF IS_ENTITY_ALIVE(entPlayer)
				IF HAS_ANIM_EVENT_FIRED(entPlayer, GET_HASH_KEY("START_TRIP"))				
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_INT / FIX_TRIP2_INT / FIX_TRIP3_INT - (FIX_FRANKLIN) - Setting FixerShortTrip fade in 10 sec.")				
				SET_TRANSITION_TIMECYCLE_MODIFIER("FixerShortTrip", 10.0)
			ENDIF
			
			IF HAS_ANIM_EVENT_FIRED(entPlayer, GET_HASH_KEY("MAX_TRIP"))				
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_INT / FIX_TRIP2_INT / FIX_TRIP3_INT - (FIX_FRANKLIN) - Setting FixerShortTrip_Distort.")					
				SET_TIMECYCLE_MODIFIER("FixerShortTrip_Distort")
			ENDIF
			
			IF HAS_ANIM_EVENT_FIRED(entPlayer, GET_HASH_KEY("NO_DISTORT"))
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_INT / FIX_TRIP2_INT / FIX_TRIP3_INT - (FIX_FRANKLIN) - Clearing FixerShortTrip_Distort")
				CLEAR_TIMECYCLE_MODIFIER()
				SET_TIMECYCLE_MODIFIER("FixerShortTrip")
			ENDIF
			
			IF HAS_ANIM_EVENT_FIRED(entPlayer, GET_HASH_KEY("END_TRIP"))
			AND NOT ARE_STRINGS_EQUAL(sMocapName, "FIX_TRIP1_INT") // We want TRIP1_INT to fade this out during the leadout instead
				IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
				AND NOT GET_IS_TIMECYCLE_TRANSITIONING_OUT()
					PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_INT / FIX_TRIP2_INT / FIX_TRIP3_INT - (FIX_FRANKLIN) - Setting FixerShortTrip fade out 10 sec.")									
					IF ARE_STRINGS_EQUAL(sMocapName, "FIX_TRIP3_INT")
						SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(20.0)
					ELSE
						SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(10.0)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ELIF ARE_STRINGS_EQUAL(sMocapName, "FIX_GOLF_INT")
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_GOLF_INT - Processing Events.")
		
		ENTITY_INDEX entPlayer = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_1")
		IF IS_ENTITY_ALIVE(entPlayer)
			IF NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_GOLF_INT_StartMusic)
				IF HAS_ANIM_EVENT_FIRED(entPlayer, GET_HASH_KEY("Start_Music"))
					SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_GOLF_INT_StartMusic)
					TRIGGER_MUSIC_EVENT(GET_MOOD_NAME_FOR_SONG(g_FMMC_STRUCT.iAudioScore, MUSIC_MED_INTENSITY))							
					PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_GOLF_INT - Starting Med Intensity.")
				ENDIF
			ENDIF
		ENDIF
	
	ELIF ARE_STRINGS_EQUAL(sMocapName, "FIX_TRIP1_MCS2")
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_MCS2 - Processing Events.")

		ENTITY_INDEX entPlayer = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("FIX_FRANKLIN")
		IF IS_ENTITY_ALIVE(entPlayer)
			//Tags are the wrong way around in cutscene data
			IF HAS_ANIM_EVENT_FIRED(entPlayer, GET_HASH_KEY("TC_OFF"))
				SET_TIMECYCLE_MODIFIER("Truck_Int_FIX_TRIP1_MCS2")
				SET_TIMECYCLE_MODIFIER_STRENGTH(1.0)
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_MCS2 - Timecycle On")
			ELIF HAS_ANIM_EVENT_FIRED(entPlayer, GET_HASH_KEY("TC_ON"))
				CLEAR_TIMECYCLE_MODIFIER()
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_MCS2 - Timecycle Off")
			ELIF HAS_ANIM_EVENT_FIRED(entPlayer, GET_HASH_KEY("TC .65 OFF"))
				SET_TIMECYCLE_MODIFIER("Truck_Int_FIX_TRIP1_MCS2")
				SET_TIMECYCLE_MODIFIER_STRENGTH(0.912)
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_MCS2 - Timecycle On to 0.65 strength")
			ENDIF
			
			IF NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_TRIP1_MCS2_StartMusic)
				IF HAS_ANIM_EVENT_FIRED(entPlayer, GET_HASH_KEY("CueMusic"))
					SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_TRIP1_MCS2_StartMusic)
					TRIGGER_MUSIC_EVENT("MP_SEED_CAPITAL_MULE_SONG_START")							
					PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP1_MCS2 - Starting MP_SEED_CAPITAL_MULE_SONG_START.")
				ENDIF
			ENDIF
		ENDIF		
	
	ELIF ARE_STRINGS_EQUAL(sMocapName, "FIXF_FIN_MCS3")
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIXF_FIN_MCS3 - Processing Events.")
		
		ENTITY_INDEX entChopper = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("HS4_PRODUCER_CHOPPER")		
		IF IS_ENTITY_ALIVE(entChopper)
			VEHICLE_INDEX vehChopper = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entChopper)
			
			IF NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIXF_FIN_MCS3_INIT_LANDING_GEAR)
				CONTROL_LANDING_GEAR(vehChopper, LGC_DEPLOY_INSTANT)
				SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIXF_FIN_MCS3_INIT_LANDING_GEAR)				
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIXF_FIN_MCS3 - Initializing Landing Gear.")
			ENDIF
			
			IF HAS_ANIM_EVENT_FIRED(entChopper, GET_HASH_KEY("gear_up_animate"))				
				CONTROL_LANDING_GEAR(vehChopper, LGC_RETRACT)				
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIXF_FIN_MCS3 - Retracting Landing Gear.")
			ENDIF
			
			IF HAS_ANIM_EVENT_FIRED(entChopper, GET_HASH_KEY("gear_up_instant"))
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIXF_FIN_MCS3 - Setting Landing Gear to be retracted instantly.")
				CONTROL_LANDING_GEAR(vehChopper, LGC_RETRACT_INSTANT)
			ENDIF
			
		ENDIF
	
	ELIF ARE_STRINGS_EQUAL(sMocapName, "FIX_STU_EXT")		
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_STU_EXT - Processing Events.")
		
		IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_MUSIC_STARTED_DRE_SHOOTOUT)
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_STU_EXT - Calling FITB_SHOOTOUT_TRACK_STOP")
			TRIGGER_MUSIC_EVENT("FITB_SHOOTOUT_TRACK_STOP")	
			CLEAR_BIT(iLocalBoolCheck25, LBOOL25_MUSIC_STARTED_DRE_SHOOTOUT)
		ENDIF		
		
		ENTITY_INDEX entFakePlayer
		TEXT_LABEL_31 tl31_handle
		INT i = 0
		FOR i = 0 TO (FMMC_MAX_CUTSCENE_PLAYERS-1)
									
			tl31_handle = "MP_"
			tl31_handle += (i+1)
			
			entFakePlayer = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY(tl31_handle)
			
			IF NOT DOES_ENTITY_EXIST(entFakePlayer)
				RELOOP
			ENDIF
			
			PED_INDEX pedIndex = GET_PED_INDEX_FROM_ENTITY_INDEX(entFakePlayer)
			
			IF IS_PED_INJURED(pedIndex)
				RELOOP
			ENDIF
			
			IF GET_ENTITY_MODEL(pedIndex) = MP_F_FREEMODE_01
			OR GET_ENTITY_MODEL(pedIndex) = MP_M_FREEMODE_01
				RELOOP
			ENDIF
			
			IF HAS_ANIM_EVENT_FIRED(entFakePlayer, GET_HASH_KEY("showped"))
			OR IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_TRIP3_EXT_ShowPed0+i)
				SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_TRIP3_EXT_ShowPed0+i)
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_STU_EXT - Showing Entity with handle: ", tl31_handle)
				SET_ENTITY_LOCALLY_VISIBLE(entFakePlayer)
			ELSE
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_STU_EXT - Hiding Entity with handle: ", tl31_handle)
				SET_ENTITY_LOCALLY_INVISIBLE(entFakePlayer)
			ENDIF
			
		ENDFOR
		
	ENDIF
		
	
ENDPROC

FUNC INT GET_END_MOCAP_WARP_RANGE()

	IF SHOULD_CUTSCENE_IGNORE_WARP_RANGE()
		RETURN 99999
		PRINTLN("[Cutscene][End] - WARP_TO_END_MOCAP - SHOULD_CUTSCENE_IGNORE_WARP_RANGE returned true! This cutscene will not use a fade out!")
	ENDIF
	
	IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_FIXER) 
		// Old Default. If we ever need an "off" allow creator to set to -1.
		IF g_fmmc_struct.sEndMocapSceneData.iCutsceneWarpRange = 0
			RETURN 100
		ENDIF
		
		RETURN g_fmmc_struct.sEndMocapSceneData.iCutsceneWarpRange
	ELSE
		RETURN 100
	ENDIF
	
ENDFUNC

///PURPOSE: This function warps the player to the vector returned by GET_CUTSCENE_COORDS if players are >100m away
FUNC BOOL WARP_TO_END_MOCAP()

	VECTOR vMocapWarpPos
	BOOL b3dCheck = TRUE
	
	RESPAWN_LOCAL_PLAYER_IF_DEAD_AND_GOING_INTO_END_CUTSCENE()
	
	// Wait for the player to respawn
	IF NOT bLocalPlayerOK
		RETURN FALSE
	ENDIF
	
	// Skip warping people to the apartment
	IF IS_BIT_SET( iLocalBoolCheck7, LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE )
		RETURN TRUE
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET( iLocalBoolCheck2, LBOOL2_CUTSCENE_WARP_DONE )
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_PLAYING_MOCAP_CUTSCENE)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene)
		RETURN TRUE
	ENDIF
	
	INT iMyTeam = MC_playerBD[iPartToUse].iteam
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
	OR IS_BIT_SET(g_fmmc_struct.sEndMocapSceneData.iCutsceneBitset, ci_CSBS_Pull_Team0_In + iMyTeam )
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_PlayEndCutsceneForAllPlayers)
		
		vMocapWarpPos = GET_CUTSCENE_COORDS(-1)
		
		INT iWarpRange = GET_END_MOCAP_WARP_RANGE()
		
		IF NOT IS_VECTOR_ZERO(vMocapWarpPos)
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, vMocapWarpPos, b3dCheck) > iWarpRange
				IF NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_CUTSCENE_WARP_DONE)
				AND NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP)	
					IF MC_serverBD.iEndCutsceneNum[ iMyTeam ] != -1
					AND NOT bIsSCTV
					AND HAS_TEAM_PASSED_MISSION( iMyTeam )
						SET_BIT(iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP)
						PRINTLN("[Cutscene][End] - WARP_TO_END_MOCAP - LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP SET ")
					ENDIF
					
					// url:bugstar:2197663
					IF bIsAnySpectator
						SET_BIT(iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP)
						SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_WARP_TO_END_MOCAP_IN_PROGRESS)
						PRINTLN("[Cutscene][End] - WARP_TO_END_MOCAP - iABI_WARP_TO_END_MOCAP_IN_PROGRESS - SET")
						PRINTLN("[Cutscene][End] - WARP_TO_END_MOCAP - Setting LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP for SPECTATOR ")
					ENDIF
				ENDIF
			ENDIF 
		ENDIF

		IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP)
			IF NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
				PRINTLN("[Cutscene][End] - WARP_TO_END_MOCAP Fading out screen.")
				DO_SCREEN_FADE_OUT( 1500 )
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()
			
				IF NOT busyspinner_is_on()
					BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("FMMC_PLYLOAD") 
					END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))					
				ENDIF
				
				IF (IS_ENTITY_DEAD(LocalPlayerPed)
				OR GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iRespawnState != RESPAWN_STATE_PLAYING)
				AND NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
					PRINTLN("[Cutscene][End] - WARP_TO_END_MOCAP - LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP SET BUT PLAYER DEAD FORCING STATE")
					SET_MANUAL_RESPAWN_STATE(MRS_NULL)
					RESET_GAME_STATE_ON_DEATH()
					FORCE_RESPAWN(TRUE)					
				ELSE
					IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_CANCEL_LOAD_SCENE_FOR_END_CUTSCENE_WARP)
						IF IS_NEW_LOAD_SCENE_ACTIVE()
							PRINTLN("[Cutscene][End] - WARP_TO_END_MOCAP Stopping load scene so that we can warp.")
							NEW_LOAD_SCENE_STOP()
						ENDIF
						SET_BIT(iLocalBoolCheck31, LBOOL31_CANCEL_LOAD_SCENE_FOR_END_CUTSCENE_WARP)
					ENDIF
					
					// Hide player so they don't pop into existence while other players are waiting.
					SET_ENTITY_VISIBLE(LocalPlayerPed, FALSE)
					
					IF NET_WARP_TO_COORD(vMocapWarpPos, 0.0, FALSE, FALSE)
						PRINTLN("[Cutscene][End] - WARP_TO_END_MOCAP Warped for end cutscene, setting LBOOL2_CUTSCENE_WARP_DONE.")
						IF busyspinner_is_on()
							BusySpinner_off()
						ENDIF
						CLEAR_BIT( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP )
						SET_BIT( iLocalBoolCheck2, LBOOL2_CUTSCENE_WARP_DONE )
					ELSE
						PRINTLN("[Cutscene][End] - WARP_TO_END_MOCAP Waiting for end cutscene warp")
					ENDIF
				ENDIF				
			ENDIF
			
		ELSE // End of warp checks
			IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_WARP_TO_END_MOCAP_IN_PROGRESS)
				CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_WARP_TO_END_MOCAP_IN_PROGRESS)
				PRINTLN("[Cutscene][End] - WARP_TO_END_MOCAP - iABI_WARP_TO_END_MOCAP_IN_PROGRESS - CLEARED - A")
			ENDIF
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_WARP_TO_END_MOCAP_IN_PROGRESS)
			CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_WARP_TO_END_MOCAP_IN_PROGRESS)
			PRINTLN("[Cutscene][End] - WARP_TO_END_MOCAP - iABI_WARP_TO_END_MOCAP_IN_PROGRESS - CLEARED - B")
		ENDIF
		RETURN TRUE
	ENDIF

	RETURN FALSE
	
ENDFUNC

// This function streams all mocap cutscenes, and returns TRUE when done, as well as
// SET_BIT( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED ), which is required to play a mocap
FUNC BOOL HANDLE_STREAMING_MOCAP( VECTOR vStreamPos, FLOAT fStreamInDistance, FLOAT fStreamOutDistance, FMMC_CUTSCENE_TYPE cutType)
	
	INT i
	INT iCutsceneToUse 
	PED_INDEX pedPlayer
	
	IF g_iFMMCScriptedCutscenePlaying = -1
	AND NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_FORCE_STREAM_CUTSCENE)
		INT iTeam = GET_CUTSCENE_TEAM(TRUE)
		IF iTeam > -1
			iCutsceneToUse = MC_serverBD.iCutsceneStreamingIndex[iTeam]
		ENDIF
		
		PRINTLN("HANDLE_STREAMING_MOCAP Using MC_serverBD.iCutsceneStreamingIndex: ", iCutsceneToUse, " g_iFMMCScriptedCutscenePlaying: ", g_iFMMCScriptedCutscenePlaying, " LBOOL9_FORCE_STREAM_CUTSCENE", IS_BIT_SET(iLocalBoolCheck9, LBOOL9_FORCE_STREAM_CUTSCENE))						
	ELSE
		INT iTeam = GET_CUTSCENE_TEAM(FALSE)
		IF iTeam > -1
			iCutsceneToUse = MC_serverBD.iCutsceneID[iTeam]
		ENDIF
		
		PRINTLN("HANDLE_STREAMING_MOCAP Using MC_serverBD.iCutsceneID: ", iCutsceneToUse)
	ENDIF
	
	IF iCutsceneToUse < FMMC_GET_MAX_SCRIPTED_CUTSCENES() 
	AND cutType != FMMCCUT_ENDMOCAP
		PRINTLN("HANDLE_STREAMING_MOCAP RETURN FALSE currently set to a scripted cutscene: ", iCutsceneToUse)
		RETURN FALSE
	ENDIF
	
	IF iCutsceneToUse >= FMMC_GET_MAX_SCRIPTED_CUTSCENES() 
	AND iCutsceneToUse < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP() 
		iCutsceneToUse = (iCutsceneToUse - FMMC_GET_MAX_SCRIPTED_CUTSCENES())
	ENDIF
	
	IF g_iFMMCScriptedCutscenePlaying >= 0
	AND cutType != FMMCCUT_ENDMOCAP
		IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName) 
			IF IS_STRING_NULL_OR_EMPTY(sMocapCutscene)
				sMocapCutscene = Get_String_From_TextLabel(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName)
				PRINTLN("HANDLE_STREAMING_MOCAP SETTING sMocapCutscene to '", sMocapCutscene, "' - was empty!")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET( iLocalBoolCheck9, LBOOL9_FORCE_STREAM_CUTSCENE )
	AND NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED )
	AND NOT IS_CUTSCENE_ACTIVE() // Don't print this out if we're already streaming
		PRINTLN("HANDLE_STREAMING_MOCAP Force streaming ", sMocapCutscene )
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_MOCAP_CUTSCENE_A_PLACEHOLDER(cutType, sMocapCutscene)
		SET_BIT( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED )
		PRINTLN("HANDLE_STREAMING_MOCAP MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_PLACE_HOLDER, setting LBOOL2_CUTSCENE_STREAMED" )
		RETURN TRUE
	ENDIF
	#ENDIF
		
	IF IS_STRING_NULL_OR_EMPTY( strPreviousMocapCutsceneName )
		PRINTLN("HANDLE_STREAMING_MOCAP strPrevousMocap is empty - setting. This will be the first time HANDLE_STREAMING_MOCAP is called." )
		strPreviousMocapCutsceneName = sMocapCutscene
	ENDIF
		
	INT iMyTeam = MC_playerBD[iPartToUse].iteam
		
	PRINTLN("HANDLE_STREAMING_MOCAP Calling function...   eMocapStreamingStage: ", GET_MOCAP_STREAMING_STAGE_DEBUG_NAME(eMocapStreamingStage))
	IF cutType = FMMCCUT_ENDMOCAP
		IF ( MC_serverBD.piCutscenePlayerIDs[iMyTeam][0] != INVALID_PLAYER_INDEX()
		AND LocalMainCutscenePlayer[iMyTeam] != INVALID_PLAYER_INDEX() )
		OR NOT ARE_STRINGS_EQUAL( strPreviousMocapCutsceneName, sMocapCutscene )
			IF LocalMainCutscenePlayer[iMyTeam] != MC_serverBD.piCutscenePlayerIDs[iMyTeam][0]
			OR NOT ARE_STRINGS_EQUAL( strPreviousMocapCutsceneName, sMocapCutscene )
				IF NOT IS_CUTSCENE_PLAYING()
				AND NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_PED_INVOLVED_IN_CUTSCENE )
				AND NOT IS_BIT_SET( MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_PLAYING_MOCAP_CUTSCENE )
				AND NOT g_bMissionEnding
				AND GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS() < MOCAPRUNNING_STAGE_1

					PRINTLN("HANDLE_STREAMING_MOCAP Mocap reset: ", sMocapCutscene )
					CLEAR_BIT( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED )
					
					strPreviousMocapCutsceneName = NULL
					sMocapCutscene = NULL
					PRINTLN("HANDLE_STREAMING_MOCAP SETTING sMocapCutscene to NULL - Mocap reset")
					
					SET_MOCAP_STREAMING_STAGE(MOCAP_STREAMING_STAGE_INIT)
					LocalMainCutscenePlayer[iMyTeam] = MC_serverBD.piCutscenePlayerIDs[iMyTeam][0]
					
					REMOVE_WEAPON_ASSET( WEAPONTYPE_DLC_SPECIALCARBINE )
					REMOVE_CUTSCENE()
					
					FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
						IF MC_serverBD.piCutscenePlayerIDs[iMyTeam][i] != INVALID_PLAYER_INDEX()
							pedPlayer = GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[iMyTeam][i])
							IF IS_PED_INJURED(pedPlayer)
								RELOOP
							ENDIF
							mnCutscenePlayerModels[i] = GET_ENTITY_MODEL(pedPlayer)
							SET_MODEL_AS_NO_LONGER_NEEDED(mnCutscenePlayerModels[i])
						ENDIF
					ENDFOR
					
					NEW_LOAD_SCENE_STOP()
					
					IF IS_SRL_LOADED()
						END_SRL()
					ENDIF
					
					if IPL_GROUP_SWAP_IS_ACTIVE()
						IPL_GROUP_SWAP_CANCEL()
					endif 
					
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	VECTOR vStreamDistance = <<fStreamInDistance, fStreamInDistance, fStreamInDistance>>
	
	VECTOR vSpecStreamDistance = vStreamDistance * << 2, 2, 2 >>
	
	SWITCH eMocapStreamingStage
	
		CASE MOCAP_STREAMING_STAGE_INIT
						
			IF NOT HAS_CUTSCENE_LOADED()
			AND NOT IS_PLAYER_IN_ANY_SHOP()
				
				IF ( ( IS_ENTITY_AT_COORD(LocalPlayerPed, vStreamPos, vStreamDistance)
					OR ( bIsAnySpectator AND IS_ENTITY_AT_COORD(LocalPlayerPed, vStreamPos, vSpecStreamDistance) ))
				
				OR IS_BIT_SET( iLocalBoolCheck2, LBOOL2_CUTSCENE_WARP_DONE )				// Warped to a cutscene location
				OR IS_BIT_SET( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP ) )	// End mocap request
				OR IS_BIT_SET( iLocalBoolCheck9, LBOOL9_FORCE_STREAM_CUTSCENE ) 			// Panic stream the cutscene
				
					PRINTLN("HANDLE_STREAMING_MOCAP Starting a stream for mocap ", sMocapCutscene )
					
					LocalMainCutscenePlayer[iMyTeam] = MC_serverBD.piCutscenePlayerIDs[iMyTeam][0]
											
					IF (cutType = FMMCCUT_MOCAP AND g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iVariationBS != 0 OR MC_DOES_MID_MISSION_CUTSCENE_USE_CUSTOM_CONCATS(iCutsceneToUse))
					OR (cutType = FMMCCUT_ENDMOCAP AND MC_DOES_END_CUTSCENE_USE_CONCATS(MC_ServerBD.iEndCutscene))
						CUTSCENE_SECTION cutSectionFlags
						cutSectionFlags = GET_CUTSCENE_SECTION_FLAGS(iCutsceneToUse, cutType)						
						PRINTLN("HANDLE_STREAMING_MOCAP - REQUEST_CUTSCENE_WITH_PLAYBACK_LIST (1) with creator cutscene section flags: ", ENUM_TO_INT(cutSectionFlags))						
						REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(sMocapCutscene, cutSectionFlags, GET_CUTSCENE_PLAYBACK_FLAGS())
					ELSE
						//request all other cutscens normally.
						REQUEST_CUTSCENE(sMocapCutscene, GET_CUTSCENE_PLAYBACK_FLAGS())
					ENDIF
					 
					FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
						IF MC_serverBD.piCutscenePlayerIDs[iMyTeam][i] != INVALID_PLAYER_INDEX()
							pedPlayer = GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[iMyTeam][i])							
							IF IS_PED_INJURED(pedPlayer)
								RELOOP
							ENDIF
							mnCutscenePlayerModels[i] = GET_ENTITY_MODEL(pedPlayer)
							REQUEST_MODEL(mnCutscenePlayerModels[i])
						ENDIF
					ENDFOR
					
					PRINTLN("HANDLE_STREAMING_MOCAP PRE-STREAMING MOCAP: ", sMocapCutscene)
					
					SET_MOCAP_STREAMING_STAGE(MOCAP_STREAMING_STAGE_REGISTER_ENTITIES)
				#IF IS_DEBUG_BUILD
				ELSE
					IF NOT IS_ENTITY_AT_COORD( LocalPlayerPed, vStreamPos, <<fStreamInDistance, fStreamInDistance, fStreamInDistance>> )
						PRINTLN("HANDLE_STREAMING_MOCAP: NOT IS_ENTITY_AT_COORD( -waiting")
					ENDIF
					IF NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP )
						PRINTLN("HANDLE_STREAMING_MOCAP: NOT LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP -waiting")
					ENDIF
					IF NOT IS_BIT_SET( iLocalBoolCheck9, LBOOL9_FORCE_STREAM_CUTSCENE )
						PRINTLN("HANDLE_STREAMING_MOCAP: NOT LBOOL9_FORCE_STREAM_CUTSCENE -waiting")
					ENDIF
				#ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				IF HAS_CUTSCENE_LOADED()
					PRINTLN("HANDLE_STREAMING_MOCAP: HAS_CUTSCENE_LOADED -waiting")
				ENDIF
				IF IS_PLAYER_IN_ANY_SHOP()
					PRINTLN("HANDLE_STREAMING_MOCAP: IS_PLAYER_IN_ANY_SHOP -waiting - g_bKickPlayerOutOfShop = TRUE")
					g_bKickPlayerOutOfShop = TRUE
				ENDIF
			#ENDIF
			ENDIF
			 
		BREAK

		CASE MOCAP_STREAMING_STAGE_REGISTER_ENTITIES
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()

				SETUP_CUTSCENE_PLAYERS_COMPONENTS()
				PRINTLN("HANDLE_STREAMING_MOCAP MOCAP Moving to stage 2: ", sMocapCutscene )
				
				PRINTLN("HANDLE_STREAMING_MOCAP iCutsceneToUse = ", iCutsceneToUse) 
				
				//Make sure ped component variations are streamed.
				REPEAT FMMC_MAX_CUTSCENE_ENTITIES i
					IF iCutsceneToUse != -1 // mid mission mocap 
					AND cutType != FMMCCUT_ENDMOCAP	
						IF g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iType = CREATION_TYPE_PEDS
							IF g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iIndex != -1
								STRING handle 
								handle = TEXT_LABEL_TO_STRING( g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].tlCutsceneHandle)
								SET_CUTSCENE_ENTITY_STREAMING_FLAGS(handle, DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
															
								PRINTLN("HANDLE_STREAMING_MOCAP ped ", i, " for cutscene ", iCutsceneToUse, " ped index is ", g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iIndex, " Calling SET_CUTSCENE_ENTITY_STREAMING_FLAGS with CES_DONT_STREAM_AND_APPLY_VARIATIONS, handle: ", handle)
							ENDIF
						ENDIF
						
						PRINTLN("HANDLE_STREAMING_MOCAP Entity ", i, " type : ", g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iType, ", index: ", g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iIndex)
						
					ELSE // end of mission mocap
						IF NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_MID_MISSION_MOCAP ) 
							IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType = CREATION_TYPE_PEDS
							AND g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex != -1						
								STRING handle 
								handle = TEXT_LABEL_TO_STRING(g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].tlCutsceneHandle)
								SET_CUTSCENE_ENTITY_STREAMING_FLAGS(handle, DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
								
								PRINTLN("HANDLE_STREAMING_MOCAP ped ", i, " for cutscene ", iCutsceneToUse, " ped index is ", g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex, " Calling SET_CUTSCENE_ENTITY_STREAMING_FLAGS with CES_DONT_STREAM_AND_APPLY_VARIATIONS, handle: ", handle)
							ENDIF
						ELSE
							PRINTLN("HANDLE_STREAMING_MOCAP iCutsceneTouse is -1 and we're not an end of mission cutscene. Bad times." )
						ENDIF
					ENDIF
				ENDREPEAT
				
				PRINTLN("HANDLE_STREAMING_MOCAP MOCAP Moving to stage 2: ",sMocapCutscene)
				SET_MOCAP_STREAMING_STAGE(MOCAP_STREAMING_STAGE_REQUEST_MODELS)
			ELSE
				PRINTLN("HANDLE_STREAMING_MOCAP waiting for CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY: ",sMocapCutscene)
			ENDIF
		BREAK
	
		CASE MOCAP_STREAMING_STAGE_REQUEST_MODELS
			IF HAS_CUTSCENE_LOADED()
			AND (mnCutscenePlayerModels[0] = DUMMY_MODEL_FOR_SCRIPT OR HAS_MODEL_LOADED(mnCutscenePlayerModels[0]))
			AND (mnCutscenePlayerModels[1] = DUMMY_MODEL_FOR_SCRIPT OR HAS_MODEL_LOADED(mnCutscenePlayerModels[1]))
			AND (mnCutscenePlayerModels[2] = DUMMY_MODEL_FOR_SCRIPT OR HAS_MODEL_LOADED(mnCutscenePlayerModels[2]))
			AND (mnCutscenePlayerModels[3] = DUMMY_MODEL_FOR_SCRIPT OR HAS_MODEL_LOADED(mnCutscenePlayerModels[3]))
			AND LOAD_INTERIOR_FOR_SPECTATOR_MOCAP(sMocapCutscene)
				PRINTLN("HANDLE_STREAMING_MOCAP - MOCAP IS STREAMED AND READY: ",sMocapCutscene)
				SET_MOCAP_STREAMING_STAGE(MOCAP_STREAMING_STAGE_ENDING)
			ELSE
				IF (cutType = FMMCCUT_MOCAP AND g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iVariationBS != 0 OR MC_DOES_MID_MISSION_CUTSCENE_USE_CUSTOM_CONCATS(iCutsceneToUse))
				OR (cutType = FMMCCUT_ENDMOCAP AND MC_DOES_END_CUTSCENE_USE_CONCATS(MC_ServerBD.iEndCutscene))
					CUTSCENE_SECTION cutSectionFlags
					cutSectionFlags = GET_CUTSCENE_SECTION_FLAGS(iCutsceneToUse, cutType)		
					PRINTLN("HANDLE_STREAMING_MOCAP - REQUEST_CUTSCENE_WITH_PLAYBACK_LIST (2) with creator cutscene section flags: ", ENUM_TO_INT(cutSectionFlags))						
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(sMocapCutscene, cutSectionFlags, GET_CUTSCENE_PLAYBACK_FLAGS())
				ELSE
					REQUEST_CUTSCENE(sMocapCutscene, GET_CUTSCENE_PLAYBACK_FLAGS())
				ENDIF
				
				FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
					IF mnCutscenePlayerModels[i] != DUMMY_MODEL_FOR_SCRIPT
						REQUEST_MODEL(mnCutscenePlayerModels[i])
												
						#IF IS_DEBUG_BUILD
						IF NOT HAS_MODEL_LOADED(mnCutscenePlayerModels[i])
							PRINTLN("HANDLE_STREAMING_MOCAP - HAS_MODEL_LOADED(", FMMC_GET_MODEL_NAME_FOR_DEBUG(mnCutscenePlayerModels[i]), ") = FALSE.")
						ENDIF
						#ENDIF
					ENDIF
				ENDFOR
								
				#IF IS_DEBUG_BUILD
				PRINTLN("HANDLE_STREAMING_MOCAP - CUTSCENE HASNT LOADED!")
				IF NOT HAS_CUTSCENE_LOADED()
					PRINTLN("HANDLE_STREAMING_MOCAP -  HAS_THIS_CUTSCENE_LOADED = FALSE. Mocap = ",sMocapCutscene)
				ENDIF			
				#ENDIF
				
			ENDIF
		BREAK
		
		CASE MOCAP_STREAMING_STAGE_ENDING
			IF ( NOT IS_ENTITY_AT_COORD( LocalPlayerPed, vStreamPos, <<fStreamOutDistance, fStreamOutDistance, fStreamOutDistance>> )
				OR IS_PLAYER_IN_ANY_SHOP() )
			AND NOT bIsAnySpectator
			AND NOT IS_CUTSCENE_PLAYING()
			AND NOT IS_BIT_SET( iLocalBoolCheck7, LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE )
			AND NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_PED_INVOLVED_IN_CUTSCENE )
			AND NOT IS_BIT_SET( MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_PLAYING_MOCAP_CUTSCENE )
			AND NOT g_bMissionEnding
			AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bPlayingPropertyTransitionCutscene)
			
				PRINTLN("HANDLE_STREAMING_MOCAP - unloading ", sMocapCutscene )
				
				IF NOT IS_ENTITY_AT_COORD( LocalPlayerPed, vStreamPos, <<fStreamOutDistance, fStreamOutDistance, fStreamOutDistance>> )
					PRINTLN("HANDLE_STREAMING_MOCAP - unloading cutscene because we're too far away." )
				ENDIF
				
				IF IS_PLAYER_IN_ANY_SHOP()
					PRINTLN("HANDLE_STREAMING_MOCAP - unloading cutscene because we're in a shop." )
				ENDIF

				IF IS_SRL_LOADED()
					END_SRL()
				ENDIF
				
				REMOVE_CUTSCENE()
				
				FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
					IF mnCutscenePlayerModels[i] != DUMMY_MODEL_FOR_SCRIPT
						SET_MODEL_AS_NO_LONGER_NEEDED(mnCutscenePlayerModels[i])
					ENDIF
				ENDFOR
				
				CLEAR_BIT( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED )
			
				sMocapCutscene = ""
				SET_MOCAP_STREAMING_STAGE(MOCAP_STREAMING_STAGE_INIT)
				PRINTLN("HANDLE_STREAMING_MOCAP - SETTING sMocapCutscene to \"\"(EMPTY) - unloading cutscene")
			ELSE
				IF NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED )	
					SET_BIT( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED )	
					PRINTLN("HANDLE_STREAMING_MOCAP - Mocap has finished streaming: ", sMocapCutscene )
				ENDIF
				
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bPlayingPropertyTransitionCutscene) 
					PRINTLN("HANDLE_STREAMING_MOCAP - GlobalPlayerBroadcastDataFM_BS_bPlayingPropertyTransitionCutscene can't cleanup" )
				ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC
	
PROC MANAGE_STREAMING_MOCAP()

	INT iCurrentCutsceneStreamingTeam = GET_CUTSCENE_TEAM( TRUE )
	
	INT iMyTeam = MC_playerBD[iPartToUse].iteam
	INT iCutsceneTeam = iCurrentCutsceneStreamingTeam

	// Check to see if we've skipped the rule before a cutscene
	// and are now on the mocap rule toensure we stream the mocap anyway
	IF iCutsceneTeam = -1
		iCutsceneTeam = GET_CUTSCENE_TEAM( FALSE )
	ENDIF

	FMMC_CUTSCENE_TYPE cutType
	IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_END_CUTSCENE_STARTED)
		cutType = FMMCCUT_ENDMOCAP
	ELSE
		cutType = FMMCCUT_MOCAP
	ENDIF
	
	sMocapCutscene = GET_MOCAP_CUTSCENE_NAME(MC_serverBD.iCutsceneID[iMyTeam], MC_ServerBD.iEndCutscene, iCutsceneTeam, cutType)
	PRINTLN("[Cutscenes] SETTING sMocapCutscene to '", sMocapCutscene, "' - calling GET_MOCAP_CUTSCENE_NAME for iTeam: ", iCutsceneTeam)

#IF IS_DEBUG_BUILD
	IF NOT IS_MOCAP_CUTSCENE_A_PLACEHOLDER(cutType, sMocapCutscene)
#ENDIF

		SET_STRAND_MISSION_CUTSCENE_NAME(sMocapCutscene)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sMocapCutscene)
			
			CLEAR_BIT(iLocalBoolCheck31, LBOOL31_NOT_STREAMING_CUTSCENE)
			
			INT iCutsceneIDToUse = MC_serverBD.iCutsceneID[iMyTeam] - FMMC_GET_MAX_SCRIPTED_CUTSCENES()
			IF iCutsceneIDToUse <= -1
				iCutsceneIDToUse = 0
			ENDIF
			
			FLOAT iStreamingInDistance = GET_CUTSCENE_PLAYER_INCLUSION_RANGE(iCutsceneIDToUse, cutType)
			FLOAT iStreamingOutDistance = iStreamingInDistance + 50
			
			HANDLE_STREAMING_MOCAP(GET_CUTSCENE_COORDS(iCutsceneTeam, cutType),
									iStreamingInDistance,
									iStreamingOutDistance, cutType)
			
		ELSE
			SET_BIT(iLocalBoolCheck31, LBOOL31_NOT_STREAMING_CUTSCENE)
		ENDIF
		
#IF IS_DEBUG_BUILD
	ENDIF	
#ENDIF
			
ENDPROC

PROC SET_CAMERA_FLY_DOWN_AFTER_CUTSCENE_STATE(CUTSCENE_FLY_CAM_STATE eCutsceneFlyCamStateNew)
	PRINTLN("[Cutscenes] - SET_CAMERA_FLY_DOWN_AFTER_CUTSCENE_STATE - Setting Cutscene Fly Cam State New: ", ENUM_TO_INT(eCutsceneFlyCamStateNew), " From Old: ", ENUM_TO_INT(eCutsceneFlyCamState))
	eCutsceneFlyCamState = eCutsceneFlyCamStateNew	
ENDPROC

PROC CACHE_POSITION_FOR_CUTSCENE_FLY_CAM(FMMC_CUTSCENE_TYPE eCutType, INT iCutsceneToUse, VECTOR vPosOverride, FLOAT fHeadingOverride = -9999.0)

	DEBUG_PRINTCALLSTACK()	
	
	IF eCutType = FMMCCUT_MOCAP
		IF iCutsceneToUse >= -1
		AND iCutsceneToUse < MAX_MOCAP_CUTSCENES
			IF NOT IS_BIT_SET(g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_UseFlyCamAfterCutscene_BeforeWarp)
				PRINTLN("[Cutscenes] - CACHE_POSITION_FOR_CUTSCENE_FLY_CAM - Exitting, ci_CSBS3_UseFlyCamAfterCutscene_BeforeWarp not set (Mocap).")
				EXIT
			ENDIF	
		ELSE
			PRINTLN("[Cutscenes] - CACHE_POSITION_FOR_CUTSCENE_FLY_CAM - Exitting, invalid index(Mocap).")
			EXIT
		ENDIF
	ELIF eCutType = FMMCCUT_SCRIPTED
		IF iCutsceneToUse >= -1
		AND iCutsceneToUse < _POST_CASINO_HEIST__MAX_SCRIPTED_CUTSCENES
			IF NOT IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_UseFlyCamAfterCutscene_BeforeWarp)
				PRINTLN("[Cutscenes] - CACHE_POSITION_FOR_CUTSCENE_FLY_CAM - Exitting, ci_CSBS3_UseFlyCamAfterCutscene_BeforeWarp not set (scripted).")
				EXIT
			ENDIF	
		ELSE
			PRINTLN("[Cutscenes] - CACHE_POSITION_FOR_CUTSCENE_FLY_CAM - Exitting, invalid index (scripted).")
			EXIT
		ENDIF
	ELSE
		PRINTLN("[Cutscenes] - CACHE_POSITION_FOR_CUTSCENE_FLY_CAM - Exitting, no valid type")	
		EXIT
	ENDIF
	
	IF IS_VECTOR_ZERO(vPosOverride)
		IF IS_PED_IN_ANY_VEHICLE(localPlayerPed, TRUE)
			vPlayerLastCutscenePosition = GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE), FALSE)
		ELSE
			vPlayerLastCutscenePosition = GET_ENTITY_COORDS(LocalPlayerPed)
		ENDIF
	ELSE
		PRINTLN("[Cutscenes] - CACHE_POSITION_FOR_CUTSCENE_FLY_CAM - Using Override: ", vPlayerLastCutscenePosition)
		vPlayerLastCutscenePosition = vPosOverride
	ENDIF
	IF fHeadingOverride = -9999
		IF IS_PED_IN_ANY_VEHICLE(localPlayerPed, TRUE)
			fPlayerLastCutsceneHeading = GET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE))
		ELSE
			fPlayerLastCutsceneHeading = GET_ENTITY_HEADING(LocalPlayerPed)
		ENDIF
	ELSE
		PRINTLN("[Cutscenes] - CACHE_POSITION_FOR_CUTSCENE_FLY_CAM - Using Override heading: ", fHeadingOverride)
		fPlayerLastCutsceneHeading = fHeadingOverride
	ENDIF
	
	PRINTLN("[Cutscenes] - CACHE_POSITION_FOR_CUTSCENE_FLY_CAM - vPlayerLastCutscenePosition: ", vPlayerLastCutscenePosition, " fPlayerLastCutsceneHeading: ", fPlayerLastCutsceneHeading)
	
ENDPROC

FUNC VECTOR GET_CAMERA_FLY_DOWN_AFTER_CUTSCENE_OFFSET_POSITION()
	
	// Will be tied to creator functionality. url:bugstar:7424150 - Data Leak 5 - Don't Fuck With Dre - Can please we have these camera changes made for the trip skip section of the mission? [Creator support]
	
	RETURN <<1.7994, 2.4098, 0.3674>>
	
ENDFUNC

FUNC VECTOR GET_CAMERA_FLY_DOWN_AFTER_CUTSCENE_POINT_AT_OFFSET_POSITION()
	
	// Will be tied to creator functionality. url:bugstar:7424150 - Data Leak 5 - Don't Fuck With Dre - Can please we have these camera changes made for the trip skip section of the mission? [Creator support]
	
	RETURN <<1.1786, 4.0436, 0.4533>>
	
ENDFUNC

FUNC FLOAT GET_CAMERA_FLY_DOWN_AFTER_CUTSCENE_FOV()
	
	// Will be tied to creator functionality. url:bugstar:7424150 - Data Leak 5 - Don't Fuck With Dre - Can please we have these camera changes made for the trip skip section of the mission? [Creator support]
	
	RETURN 50.0
	
ENDFUNC

PROC PROCESS_CAMERA_FLY_DOWN_ROLLING_START(FMMC_CUTSCENE_TYPE eCutType, INT iCutsceneToUse, VEHICLE_INDEX vehWarpTo)

	IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer) 
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
	OR IS_PLAYER_SCTV(localPlayer)
		EXIT
	ENDIF
	
	BOOL bDoRollingStart
	FLOAT fRollingStartSpeed
	fRollingStartSpeed = 5.0
	IF eCutType = FMMCCUT_MOCAP
		IF iCutsceneToUse >= -1
		AND iCutsceneToUse < MAX_MOCAP_CUTSCENES
			IF IS_BIT_SET(g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_UseFlyCamAfterCutscene_RollingStart)
			OR IS_BIT_SET(g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Rolling_Start)
				bDoRollingStart = TRUE
				IF g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].fCutsceneRollingStartSpeed > 0.0
					fRollingStartSpeed = g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].fCutsceneRollingStartSpeed
				ENDIF
			ENDIF
		ENDIF
	ELIF eCutType = FMMCCUT_SCRIPTED
		IF iCutsceneToUse >= -1
		AND iCutsceneToUse < _POST_CASINO_HEIST__MAX_SCRIPTED_CUTSCENES
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset3, ci_CSBS3_UseFlyCamAfterCutscene_RollingStart)
			OR IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Rolling_Start)
				bDoRollingStart = TRUE
				IF g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].fCutsceneRollingStartSpeed > 0.0
					fRollingStartSpeed = g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].fCutsceneRollingStartSpeed
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bDoRollingStart
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			VEHICLE_INDEX vehPlayer
			vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
						
			IF IS_VEHICLE_DRIVEABLE(vehPlayer)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
				SET_ENTITY_LOCALLY_VISIBLE(vehPlayer)
				VECTOR vForward = GET_ENTITY_FORWARD_VECTOR(vehPlayer)*(fRollingStartSpeed*5)
				#IF IS_DEBUG_BUILD
				VECTOR vTemp = GET_ENTITY_COORDS(vehPlayer)
				PRINTLN("[Cutscenes][validating veh] PROCESS_CAMERA_FLY_DOWN_ROLLING_START - VEHID : ", NETWORK_ENTITY_GET_OBJECT_ID(vehPlayer), " vehPos: ", vTemp, " fRollingStartSpeed: ", fRollingStartSpeed, " Attempting: SET_ENTITY_VELOCITY with: ", vForward)
				#ENDIF
				IF NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FlyCamVehicleTask)					
					IF IS_ENTITY_A_MISSION_ENTITY(vehPlayer)
						SET_VEHICLE_LIMIT_SPEED_WHEN_PLAYER_INACTIVE(vehPlayer, FALSE)
						SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE(vehPlayer, FALSE)
					ENDIF
					FREEZE_ENTITY_POSITION(vehPlayer, FALSE)
					SET_VEHICLE_ENGINE_ON(vehPlayer, TRUE, TRUE)
					SET_VEHICLE_HANDBRAKE(vehPlayer, FALSE)					
					SET_ENTITY_VELOCITY(vehPlayer, vForward)
					SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FlyCamVehicleTask)
					SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_DidRollingStart)
				ENDIF
				
				IF GET_PED_IN_VEHICLE_SEAT(vehPlayer, VS_DRIVER) = NULL
					SET_ENTITY_VELOCITY(vehPlayer, vForward)
				ENDIF
				
				IF GET_PED_IN_VEHICLE_SEAT(vehPlayer, VS_DRIVER) = localPlayerPed
				AND NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FlyCamVehicleRollingStart)
					SET_VEHICLE_FORWARD_SPEED(vehPlayer, fRollingStartSpeed*5)
					SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_DidRollingStart)
					SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FlyCamVehicleRollingStart)
					NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
				ENDIF
			ENDIF
		ELSE			
			IF IS_VEHICLE_DRIVEABLE(vehWarpTo)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(vehWarpTo)				
				SET_ENTITY_LOCALLY_VISIBLE(vehWarpTo)				
				
				VECTOR vForward = GET_ENTITY_FORWARD_VECTOR(vehWarpTo)*(fRollingStartSpeed*5)
				#IF IS_DEBUG_BUILD
				VECTOR vTemp = GET_ENTITY_COORDS(vehWarpTo)
				PRINTLN("[Cutscenes][validating veh] PROCESS_CAMERA_FLY_DOWN_ROLLING_START - VEHID : ", NETWORK_ENTITY_GET_OBJECT_ID(vehWarpTo), " vehPos: ", vTemp, " fRollingStartSpeed: ", fRollingStartSpeed, " Attempting: SET_ENTITY_VELOCITY with: ", vForward)
				#ENDIF
				IF NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FlyCamVehicleTask)									
					IF IS_ENTITY_A_MISSION_ENTITY(vehWarpTo)
						SET_VEHICLE_LIMIT_SPEED_WHEN_PLAYER_INACTIVE(vehWarpTo, FALSE)
						SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE(vehWarpTo, FALSE)
					ENDIF					
					FREEZE_ENTITY_POSITION(vehWarpTo, FALSE)
					SET_VEHICLE_ENGINE_ON(vehWarpTo, TRUE, TRUE)
					SET_VEHICLE_HANDBRAKE(vehWarpTo, FALSE)
					SET_ENTITY_VELOCITY(vehWarpTo, vForward)
					SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FlyCamVehicleTask)
					SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_DidRollingStart)							
					NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
				ENDIF
				
				IF GET_PED_IN_VEHICLE_SEAT(vehWarpTo, VS_DRIVER) = NULL
					SET_ENTITY_VELOCITY(vehWarpTo, vForward)
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

FUNC BOOL PROCESS_CAMERA_FLY_DOWN_AFTER_CUTSCENE(FMMC_CUTSCENE_TYPE eCutType, INT iCutsceneToUse, VECTOR vEndPos, FLOAT fEndHeading)
	
	IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer) 
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
	OR IS_PLAYER_SCTV(localPlayer)
		RETURN TRUE
	ENDIF
	
	VECTOR vCamPosOffset
	VECTOR vCamPointAtPosOffset	
	FLOAT fCamFov
	
	vCamPosOffset = GET_CAMERA_FLY_DOWN_AFTER_CUTSCENE_OFFSET_POSITION()
	vCamPointAtPosOffset = GET_CAMERA_FLY_DOWN_AFTER_CUTSCENE_POINT_AT_OFFSET_POSITION()
	fCamFov = GET_CAMERA_FLY_DOWN_AFTER_CUTSCENE_FOV()
	
	INT iCutsceneTeam
	IF iScriptedCutsceneTeam != -1
	AND eCutType != FMMCCUT_ENDMOCAP
		iCutsceneTeam = iScriptedCutsceneTeam
	ELSE
		iCutsceneTeam = MC_PlayerBD[iPartToUse].iTeam
	ENDIF
	
	INT iLocalPlayerCutId
	iLocalPlayerCutId = GET_LOCAL_PLAYER_CUTSCENE_INDEX(iCutsceneTeam)
	PRINTLN("[Cutscenes] iLocalPlayerCutId = ", iLocalPlayerCutId)
	VEHICLE_INDEX vehWarpTo
	IF iLocalPlayerCutId >= 0 AND iLocalPlayerCutId < FMMC_MAX_CUTSCENE_PLAYERS
		vehWarpTo = GET_CUTSCENE_PED_WARP_VEHICLE(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sPlayerEndWarp[iLocalPlayerCutId])	
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF DOES_ENTITY_EXIST(vehWarpTo)
		VECTOR vTemp = GET_ENTITY_COORDS(vehWarpTo)
		PRINTLN("[Cutscenes][validating veh] VEHID : ", NETWORK_ENTITY_GET_OBJECT_ID(vehWarpTo), " vehPos: ", vTemp)
	ENDIF
	#ENDIF
	
	PROCESS_CAMERA_FLY_DOWN_ROLLING_START(eCutType, iCutsceneToUse, vehWarpTo)
	
	SWITCH eCutsceneFlyCamState
		CASE eCutsceneFlyCamState_Init
			INVALIDATE_CINEMATIC_VEHICLE_IDLE_MODE()
			
			IF vehWarpTo != NULL
			AND IS_VECTOR_ZERO(vPlayerLastCutscenePosition)
				CACHE_POSITION_FOR_CUTSCENE_FLY_CAM(eCutsceneTypePlaying, iCutsceneIndexPlaying, GET_ENTITY_COORDS(vehWarpTo), GET_ENTITY_HEADING(vehWarpTo))
			ENDIF
			
			IF NOT IS_VECTOR_ZERO(vEndPos)
			AND IS_VECTOR_ZERO(vPlayerLastCutscenePosition)
				CACHE_POSITION_FOR_CUTSCENE_FLY_CAM(eCutType, iCutsceneToUse, vEndPos, fEndHeading)
			ENDIF
				
			IF IS_VECTOR_ZERO(vPlayerLastCutscenePosition)
				PRINTLN("[Cutscenes] - PROCESS_CAMERA_FLY_DOWN_AFTER_CUTSCENE - Init Caching Inside vPlayerLastCutscenePosition")
				CACHE_POSITION_FOR_CUTSCENE_FLY_CAM(eCutType, iCutsceneToUse, <<0.0, 0.0, 0.0>>, -9999)				
			ENDIF
			
			VECTOR vCamPos
			
			vCamPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPlayerLastCutscenePosition, fPlayerLastCutsceneHeading, vCamPosOffset)
				
			camCutsceneFlyCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamPos, <<0.0, 0.0, 0.0>>, fCamFov)				
			
			SET_CAM_ACTIVE(camCutsceneFlyCam, TRUE)
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE, 1500, DEFAULT, TRUE)
						
			SET_CAM_FOV(camCutsceneFlyCam, fCamFov)
						
			IF DOES_ENTITY_EXIST(vehWarpTo)				
				POINT_CAM_AT_ENTITY(camCutsceneFlyCam, vehWarpTo, vCamPointAtPosOffset)
				PRINTLN("[Cutscenes] - PROCESS_CAMERA_FLY_DOWN_AFTER_CUTSCENE - Pointing to vehWarpTo.")
			ELSE				
				POINT_CAM_AT_ENTITY(camCutsceneFlyCam, LocalPlayerPed, vCamPointAtPosOffset)
				PRINTLN("[Cutscenes] - PROCESS_CAMERA_FLY_DOWN_AFTER_CUTSCENE - Pointing to LocalPlayerPed.")
			ENDIF
			
			//STOP_CAM_POINTING(camCutsceneFlyCam)
				
			PRINTLN("[Cutscenes] - PROCESS_CAMERA_FLY_DOWN_AFTER_CUTSCENE - vCamPosOffset: ", vCamPosOffset, " vCamPointAtPosOffset: ", vCamPointAtPosOffset, " vPlayerLastCutscenePosition: ", vPlayerLastCutscenePosition, " fPlayerLastCutsceneHeading: ", fPlayerLastCutsceneHeading)
			
			SET_CAMERA_FLY_DOWN_AFTER_CUTSCENE_STATE(eCutsceneFlyCamState_Process)
		BREAK
		
		CASE eCutsceneFlyCamState_Process
			
			IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(iCutsceneFlyCamInterpTimestamp)
				MC_START_NET_TIMER_WITH_TIMESTAMP(iCutsceneFlyCamInterpTimestamp)				
				
				//STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(TRUE, 60.0)
				
				RENDER_SCRIPT_CAMS(FALSE, TRUE, 1800, DEFAULT, TRUE)
				
				PRINTLN("[Cutscenes] - PROCESS_CAMERA_FLY_DOWN_AFTER_CUTSCENE - Rendering Cam Now.")
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(1000)
			ENDIF
									
			IF NOT MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(iCutsceneFlyCamInterpTimestamp, 2500)
				RETURN FALSE
			ENDIF
			
			SET_CAMERA_FLY_DOWN_AFTER_CUTSCENE_STATE(eCutsceneFlyCamState_Cleanup)
		BREAK
		
		CASE eCutsceneFlyCamState_Cleanup
			vPlayerLastCutscenePosition = <<0.0, 0.0, 0.0>>
			fPlayerLastCutsceneHeading = 0.0
			MC_STOP_NET_TIMER_WITH_TIMESTAMP(iCutsceneFlyCamInterpTimestamp)
			DESTROY_CAM(camCutsceneFlyCam)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			SET_CAMERA_FLY_DOWN_AFTER_CUTSCENE_STATE(eCutsceneFlyCamState_Init)
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MOCAP_CUTSCENE_READY_TO_END( #IF IS_DEBUG_BUILD FMMC_CUTSCENE_TYPE eCutType, STRING sMocapName #ENDIF )	
	
	#IF IS_DEBUG_BUILD
	IF IS_MOCAP_CUTSCENE_A_PLACEHOLDER(eCutType, sMocapName)
		IF IS_MOCAP_PLACEHOLDER_CUTSCENE_FINISHED()
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	#ENDIF
	
	IF CAN_SET_EXIT_STATE_FOR_CAMERA(TRUE)
		PRINTLN("[Cutscene_Mocap] IS_MOCAP_CUTSCENE_READY_TO_END - Finish Reasons: CAN_SET_EXIT_STATE_FOR_CAMERA(TRUE) is true")
		RETURN TRUE
	ENDIF
	
	IF NOT bLocalPlayerOK
		PRINTLN("[Cutscene_Mocap] - Finish Reasons: bLocalPlayerOK is false")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(ilocalboolcheck, LBOOL_HIT_MOCAP_FAIL_SAFE)
		PRINTLN("[Cutscene_Mocap] - Finish Reasons: LBOOL_HIT_MOCAP_FAIL_SAFE is true")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_CUTSCENE_PLAYING()
		PRINTLN("[Cutscene_Mocap] - Finish Reasons: IS_CUTSCENE_PLAYING is false")
		RETURN TRUE
	ENDIF
	
	IF (IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration) AND GET_TOGGLE_PAUSED_RENDERPHASES_STATUS() = FALSE AND HAS_TEAM_FAILED(MC_playerBD[iPartToUse].iteam))
		PRINTLN("[Cutscene_Mocap] - Finish Reasons:  ciCELEBRATION_BIT_SET_TriggerEndCelebration: ", IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration),
							" GET_TOGGLE_PAUSED_RENDERPHASES_STATUS(): ", GET_TOGGLE_PAUSED_RENDERPHASES_STATUS(),
							" HAS_TEAM_FAILED: ", HAS_TEAM_FAILED(MC_playerBD[iPartToUse].iteam))
		RETURN TRUE	
	ENDIF
		
	RETURN FALSE
ENDFUNC

PROC MC_LEAVE_PLAYER_BEHIND_FOR_MP_CUTSCENE()
	
	PRINTLN("[cutscene] MC_LEAVE_PLAYER_BEHIND_FOR_MP_CUTSCENE Calling function.")	
	//NETWORK_LEAVE_PED_BEHIND_BEFORE_CUTSCENE(LocalPlayer, TRUE)
	//LocalPlayerPed = PLAYER_PED_ID()
	
ENDPROC

FUNC BOOL CAN_THIS_CUTSCENE_BE_SKIPPED(INT iCutsceneToUse, FMMC_CUTSCENE_TYPE eCutType)
	
	#IF IS_DEBUG_BUILD
		IF bDebugAlwaysAllowMocapSkipping
			RETURN TRUE
		ENDIF
	#ENDIF
	
	IF eCutType = FMMCCUT_MOCAP
		IF iCutsceneToUse >= -1
		AND iCutsceneToUse < MAX_MOCAP_CUTSCENES
									
			IF ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName, "FIX_TRIP1_INT")
			OR ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName, "FIX_TRIP2_INT")
			OR ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName, "FIX_TRIP3_INT")	
				IF HAS_PLAYER_COMPLETED_FIXER_SHORT_TRIP(LocalPlayer, GET_PLAYER_CURRENT_FIXER_SHORT_TRIP(LocalPlayer))
					PRINTLN("[Cutscenes] PROCESS_SKIPPING_CUTSCENE - CAN_PLAYER_SKIP_THIS_CUTSCENE - Return True!!")
					RETURN TRUE
				ELSE
					PRINTLN("[Cutscenes] PROCESS_SKIPPING_CUTSCENE - CAN_PLAYER_SKIP_THIS_CUTSCENE - Return False!!")
					RETURN FALSE
				ENDIF
			ENDIF
			
		ENDIF
	ELIF eCutType = FMMCCUT_ENDMOCAP
	
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_SKIPPING_CUTSCENE(INT iCutsceneToUse, FMMC_CUTSCENE_TYPE eCutType)
	
	BOOL bSkippable
	
	#IF IS_DEBUG_BUILD
		
		bSkippable = bDebugAlwaysAllowMocapSkipping
				
		#IF IS_DEBUG_BUILD
		IF HAS_ANY_PLAYER_DEBUG_SKIPPED_FOR_CUTSCENE()	
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_TellHostToSkipCutscene)
		ENDIF
		#ENDIF
		
	#ENDIF
	
	IF eCutType = FMMCCUT_SCRIPTED
		IF iCutsceneToUse >= -1
		AND iCutsceneToUse < _POST_CASINO_HEIST__MAX_SCRIPTED_CUTSCENES
			IF IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet4, ci_CSBS4_HostCanSkipCutscene)
				bSkippable = TRUE
			ENDIF
		ENDIF
	
	ELIF eCutType = FMMCCUT_MOCAP
		IF iCutsceneToUse >= -1
		AND iCutsceneToUse < MAX_MOCAP_CUTSCENES
			IF IS_BIT_SET(g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet4, ci_CSBS4_HostCanSkipCutscene)
				bSkippable = TRUE
			ENDIF
		ENDIF
	ELIF eCutType = FMMCCUT_ENDMOCAP
		IF IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitset4, ci_CSBS4_HostCanSkipCutscene)
			bSkippable = TRUE
		ENDIF
	ENDIF
	
	IF bSkippable
	
		IF (eCutType = FMMCCUT_SCRIPTED AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdScriptedCutsceneTimer) > 3000)
		OR (IS_CUTSCENE_PLAYING() AND GET_CUTSCENE_TIME() > 3000)
			
			IF (GET_FMMC_LOBBY_LEADER_PARTICIPANT_INDEX() = iLocalPart)
			AND CAN_THIS_CUTSCENE_BE_SKIPPED(iCutsceneToUse, eCutType)
				IF NOT IS_BIT_SET(iSpecificCutsceneBitset2, ciSpecificCutsceneBitset2_PlayedSkipText)
					SET_BIT(iSpecificCutsceneBitset2, ciSpecificCutsceneBitset2_PlayedSkipText)
					PRINT_HELP("MC_SKIP_CUT", 3000)
				ENDIF			
				
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					IF NOT IS_BIT_SET(iSpecificCutsceneBitset2, ciSpecificCutsceneBitset2_SkipTriggered)
						SET_BIT(iSpecificCutsceneBitset2, ciSpecificCutsceneBitset2_SkipTriggered)
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_TellHostToSkipCutscene)						
					ENDIF
				ENDIF
				
			ENDIF
			
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(MC_ServerBD.iServerBitSet8, SBBOOL8_SKIP_CUTSCENE_TRIGGERED)
		
		IF NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
			PRINTLN("[Cutscenes] PROCESS_SKIPPING_CUTSCENE - FADING!!")
			DO_SCREEN_FADE_OUT(1000)
		ENDIF
		
		IF eCutType = FMMCCUT_MOCAP
			IF IS_SCREEN_FADED_OUT()
				PRINTLN("[Cutscenes] PROCESS_SKIPPING_CUTSCENE - SKIP!!")
				SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE) // Interferes with certain cutscenes outros where we allow external systems to fade in the cutscene when ready.
				NETWORK_SET_MOCAP_CUTSCENE_CAN_BE_SKIPPED(TRUE)				
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

// [FIX_2020_CONTROLLER] - This function needs to be turned into the legacy version and a new one should be made for new content which excludes all of the zany weird special case "this_specific_cutscene" logic.
// It's worth noting that most cutscenes return true on MOCAPRUNNING_STAGE_3 and the other states are for specific cutscenes. We could also be able to convert MOCAPRUNNING_STAGE into enum state names that make sense..
///PURPOSE: This is the main mocap function that loads and runs mocap cutscenes
FUNC BOOL RUN_MOCAP_CUTSCENE(STRING sMocapName,
							FMMC_CUTSCENE_TYPE eCutType, 
							BOOL bShowCelebrationScreen, 
							VECTOR vStartPos, 
							FLOAT fStartHeading, 
							VECTOR vEndPos, 
							FLOAT fEndHeading, 
							BOOL bIncludePlayers = TRUE, 
							BOOL bClearAreaAndParticles = TRUE,
							BOOL bShouldFadeAtEnd = FALSE,
							INT iPlayerIndex = -1)

	INT iCutsceneTeam	
	BOOL bGivePlayerControl
	BOOL bCloneLocalPlayer
	STOP_LOCAL_PLAYER_FROM_GETTING_SHOT_THIS_FRAME()
	
	IF MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_PLACE_HOLDER
	AND eCutType = FMMCCUT_ENDMOCAP
		sMocapName = ""
	ENDIF
		
	IF iScriptedCutsceneTeam != -1
	AND eCutType != FMMCCUT_ENDMOCAP
		iCutsceneTeam = iScriptedCutsceneTeam
	ELSE
		iCutsceneTeam = MC_PlayerBD[iPartToUse].iTeam
	ENDIF
	
	IF bIsAnySpectator
	AND IS_CUTSCENE_PLAYING()
	AND GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS() < MOCAPRUNNING_STAGE_3
		SET_SPECTATOR_OVERRIDE_COORDS( GET_FINAL_RENDERED_CAM_COORD() )
		SHOW_HUD_COMPONENT_THIS_FRAME( NEW_HUD_SUBTITLE_TEXT )
	ENDIF
		
	IF IS_CUTSCENE_PLAYING()
	AND GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS() < MOCAPRUNNING_STAGE_3
		DISABLE_FRONTEND_THIS_FRAME()
	ENDIF
							
	INT iCutsceneToUse
	
	IF g_iFMMCScriptedCutscenePlaying >= FMMC_GET_MAX_SCRIPTED_CUTSCENES() 
	AND g_iFMMCScriptedCutscenePlaying < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP() 
		iCutsceneToUse = (g_iFMMCScriptedCutscenePlaying - FMMC_GET_MAX_SCRIPTED_CUTSCENES())
	ELSE 
		iCutsceneToUse = g_iFMMCScriptedCutscenePlaying
	ENDIF
	
	IF g_eIAAGunCamerasEnabled != IAA_FULL_ACTIVITY_KICK
		g_eIAAGunCamerasEnabled = IAA_FULL_ACTIVITY_KICK
	ENDIF
		
	IF eCutType = FMMCCUT_ENDMOCAP
		bCloneLocalPlayer = IS_BIT_SET(g_fmmc_struct.sEndMocapSceneData.iCutsceneBitSet3, ci_CSBS3_CloneLocalPlayerPed)
	ELSE	
		IF iCutsceneToUse != -1
			bCloneLocalPlayer = IS_BIT_SET(g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_CloneLocalPlayerPed)
		ENDIF
	ENDIF	
	
	BOOL bCheckSkipIfTooFar = TRUE

	PRINTLN("[Cutscenes][Cutscene_Mocap] sMocapName: ", sMocapName, " eCutType: ", ENUM_TO_INT(eCutType), " iCutsceneTeam: ", iCutsceneTeam, " iCutsceneToUse: ", iCutsceneToUse)
	
	#IF IS_DEBUG_BUILD
	IF IS_MOCAP_CUTSCENE_A_PLACEHOLDER(eCutType, sMocapName)
		SET_TEXT_SCALE(1.3, 1.3)
		SET_TEXT_COLOUR(255, 255, 255, 255)	
		SET_TEXT_DROPSHADOW(2, 0, 0, 0, 175)
		SET_TEXT_JUSTIFICATION(FONT_CENTRE)
		DRAW_RECT(0.5, 0.75, 0.7, 0.125, 0, 0, 0, 120)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.7, "STRING", "### PLACEHOLDER CUTSCENE ###")
	ENDIF
	#ENDIF
	
	SWITCH eMocapRunningCutsceneProgress
	
		CASE MOCAPRUNNING_STAGE_0
			
			IF NOT LOAD_ASSETS_FOR_MOCAP(sMocapName)
				PRINTLN("[Cutscenes][Cutscene_Mocap] - RUN_MOCAP_CUTSCENE - sMocapName: ", sMocapName, " WAITING FOR LOAD_ASSETS_FOR_MOCAP...")	
				RETURN FALSE
			ENDIF
			
			IF SHOULD_CUTSCENE_DELAY_CUTSCENE_START_BECAUSE_OF_ENTITY(iCutsceneToUse, eCutType)
				PRINTLN("[Cutscenes][Cutscene_Mocap] - RUN_MOCAP_CUTSCENE - sMocapName: ", sMocapName, " WAITING FOR SHOULD_CUTSCENE_DELAY_CUTSCENE_START_BECAUSE_OF_ENTITY...")	
				RETURN FALSE
			ENDIF
			
			IF IS_PLAYSTATION_PLATFORM()
				SET_CONTROL_LIGHT_EFFECT_COLOR(PLAYER_CONTROL, 255, 255, 255)
			ENDIF
			
			IF NOT bLocalPlayerOK
			OR (IS_PLAYER_IN_ANY_SHOP() AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED())		
				PRINTLN("[Cutscene_Mocap] - IS_PLAYER_IN_ANY_SHOP or dead skipping cutscene ")
				SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_2)
				RETURN FALSE
			ENDIF

			IF eCutType = FMMCCUT_ENDMOCAP
			AND (IS_BIT_SET( g_fmmc_struct.sEndMocapSceneData.iCutsceneBitset, ci_CSBS_Pull_Team0_In + MC_PlayerBD[iPartToUse].iTeam )
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_PlayEndCutsceneForAllPlayers))
				bCheckSkipIfTooFar = FALSE 
			ENDIF
			
			IF bCheckSkipIfTooFar
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( LocalPlayerPed, GET_CUTSCENE_COORDS(iCutsceneTeam, eCutType)) > 200
				AND NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_CUTSCENE_WARP_DONE )
				AND NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP )
					PRINTLN("[Cutscenes][Cutscene_Mocap] Skipping cutscene - too far away!")					
					SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_2)
					RETURN FALSE
				ENDIF
			ENDIF
								
			#IF IS_DEBUG_BUILD
			IF NOT IS_CUTSCENE_AUTHORIZED( sMocapName )
			AND NOT IS_MOCAP_CUTSCENE_A_PLACEHOLDER(eCutType, sMocapName)
				ASSERTLN("Cutscene ", sMocapName, " IS NOT AUTHORIZED - it might not play. Either you don't have latest, or you're playing a cutscene that is not marked as ready yet." )
				PRINTLN("Cutscene ", sMocapName, " IS NOT AUTHORIZED - it might not play. Either you don't have latest, or you're playing a cutscene that is not marked as ready yet." )
				
				IF eCutType = FMMCCUT_ENDMOCAP
					ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_CUTSCENE_NOT_AUTHORISED, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "Cutscene not authorised/marked as ready and might not play EndCutID #", MC_ServerBD.iEndCutscene)
				ELSE
					ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_CUTSCENE_NOT_AUTHORISED, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "Cutscene not authorised/marked as ready and might not play CutID #", iCutsceneToUse)
				ENDIF
			ENDIF
			#ENDIF
			
			IF NOT bIncludePlayers
			OR ARE_TEAM_CUTSCENE_PLAYERS_SELECTED( iCutsceneTeam )
				
				g_bAbortPropertyMenus = TRUE
				PRINTLN("[Cutscene_Mocap] - g_bAbortPropertyMenus = TRUE")
				SET_BIT( iLocalBoolCheck5, LBOOL5_PED_INVOLVED_IN_CUTSCENE )
				SET_BIT( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE )
				SET_BIT( iLocalBoolCheck9, LBOOL9_FORCE_STREAM_CUTSCENE )
				
				IF eCutType = FMMCCUT_MOCAP 
				AND MC_DOES_MID_MISSION_CUTSCENE_REQUIRE_CUSTOM_CONCATS_REBUILT_DURING_GAMEPLAY(iCutsceneToUse)
				AND NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_CUTSCENE_RE_REQUESTED_AFTER_GAMEPLAY)
					REMOVE_CUTSCENE()
					SET_BIT(iLocalBoolCheck31, LBOOL31_CUTSCENE_RE_REQUESTED_AFTER_GAMEPLAY)
					CLEAR_BIT(iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED)					
					PRINTLN("[Cutscene_Mocap] - RESTREAMING CUTSCENE. REQUIRES GAMEPLAY ASSOCIATED CONCATS")					
					SET_MOCAP_STREAMING_STAGE(MOCAP_STREAMING_STAGE_INIT)
				ENDIF
				
				IF HAS_CUTSCENE_LOADED()
				AND (IS_BIT_SET(iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED) OR HAS_NET_TIMER_EXPIRED(streamCSFailsafeTimer, 10000, TRUE))
				#IF IS_DEBUG_BUILD
				OR IS_MOCAP_CUTSCENE_A_PLACEHOLDER(eCutType, sMocapName)
				#ENDIF
				
					//wait untill FMMC_Launcher is ready #2 frames
					IF IS_A_STRAND_MISSION_BEING_INITIALISED()
						IF g_sTransitionSessionData.sStrandMissionData.iSwitchState < ciMAINTAIN_STRAND_MISSION_START_CUTSCENE
							PRINTLN("[Cutscenes][Cutscene_Mocap] waiting for g_sTransitionSessionData.sStrandMissionData.iSwitchState < ciMAINTAIN_STRAND_MISSION_START_CUTSCENE")
							RETURN FALSE
						ENDIF
					ENDIF
					
					IF HAVE_MOCAP_START_CONDITIONS_BEEN_MET(iCutsceneToUse, eCutType)
						BOOL bUseCloneOfOriginalPed
						IF iCutsceneToUse > -1
						AND IS_BIT_SET(g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset3, ci_CSBS3_UseCloneOfOriginalPed)							
							bUseCloneOfOriginalPed = TRUE
						ENDIF
						
						IF NOT bIncludePlayers
						OR CLONE_MOCAP_PLAYERS( sMocapName, iCutsceneTeam, bIncludePlayers, eCutType, iCutsceneToUse, bCloneLocalPlayer, bUseCloneOfOriginalPed)
						
							PRINTLN("[Cutscene_Mocap] - CURRENT NETWORK TIMER: ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
							PRINTLN("[Cutscene_Mocap] - GET_NET_TIMER_DIFF", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_ServerBD.tdCutsceneStartTime[iCutsceneTeam],FALSE,TRUE),"[Cutscenes] is host: ", bIsLocalPlayerHost)
							
							PRINTLN("[Cutscenes][Cutscene_Mocap] case 0" )
							
							IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_ServerBD.tdCutsceneStartTime[iCutsceneTeam],FALSE,TRUE) >= 1000
								
								SET_BIT(MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_PLAYING_MOCAP_CUTSCENE)
								
								IF NOT bIsAnySpectator
									IF IS_PLAYER_PERFORMING_INTERACT_WITH_CUTSCENE_LEAD_IN()
										PRINTLN("[Cutscene_Mocap][InteractWith] - PROCESS_MOCAP_CUTSCENE | Calling native to leave a ped behind for Interact-With cutscene leadin")
										MC_LEAVE_PLAYER_BEHIND_FOR_MP_CUTSCENE()
									ENDIF
								ENDIF
								
								clone_gameplay_camera()
								
								IF NOT IS_INTRO_CUTSCENE_RULE_ACTIVE()
									ANIMPOSTFX_STOP_ALL()
								ENDIF
								
								REDUCE_VOICE_CHAT_FOR_SPECIFIC_CUTSCENES(eCutType)
								
								CLEAR_BIT( iLocalBoolCheck7, LBOOL7_WAS_VEH_RADIO_ENABLED_AT_CS_START )
								
								sMyRadioStation = ""
								
								IF( NOT IS_ENTITY_DEAD( LocalPlayerPed ) )
									IF( IS_PED_SITTING_IN_ANY_VEHICLE( LocalPlayerPed ) )
										VEHICLE_INDEX veh
										veh = GET_VEHICLE_PED_IS_IN( LocalPlayerPed )
										IF( NOT IS_ENTITY_DEAD( veh ) )
											IF( IS_PLAYER_VEH_RADIO_ENABLE() ) 
												IF NETWORK_HAS_CONTROL_OF_ENTITY( veh )
													sMyRadioStation = GET_PLAYER_RADIO_STATION_NAME()
													SET_VEH_RADIO_STATION(veh, "OFF")
													SET_VEH_FORCED_RADIO_THIS_FRAME(veh)
													PRINTLN("[Cutscene_Mocap] - SET_VEH_RADIO_STATION(veh, OFF) model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(veh)), " sMyRadioStation = ", sMyRadioStation)
												ENDIF
												SET_BIT( iLocalBoolCheck7, LBOOL7_WAS_VEH_RADIO_ENABLED_AT_CS_START )
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								PRINTLN("[Cutscene_Mocap] - Mid-mission cutscene is set to:" , iCutsceneToUse )

								IF bIsSCTV
									SET_BIT( MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCTV_IN_MOCAP )
									PRINTLN("[Cutscene_Mocap] - GLOBAL_SPEC_BS_SCTV_IN_MOCAP SET")
								ENDIF
								IF IS_BIT_SET( MC_playerBD[ iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR ) 
									SET_SPECTATOR_RUNNING_CUTSCENE(TRUE)
									DISABLE_SPECTATOR_RADIO(TRUE)
									PRINTLN("[Cutscene_Mocap] - SET SPECTATOR FLAGS")
								ENDIF
								
								// Very few cutscenes actually utilize gameplay radio, we might want to add an option to prevent this logic from killing the radio.
								IF eCutType = FMMCCUT_ENDMOCAP
									PRINTLN("[Cutscene_Mocap] - End Mocap Cutscene - Killing Radios.")
									IF bLocalPlayerPedOK
										IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
											VEHICLE_INDEX veh
											veh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
											IF NOT IS_ENTITY_DEAD(veh)
												IF IS_PLAYER_VEH_RADIO_ENABLE()
													IF NETWORK_HAS_CONTROL_OF_ENTITY(veh)
														sMyRadioStation = GET_PLAYER_RADIO_STATION_NAME()
														SET_VEH_RADIO_STATION(veh, "OFF")
														SET_VEHICLE_RADIO_ENABLED(veh, FALSE)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									SET_AUDIO_FLAG("AllowRadioDuringSwitch", FALSE) 
									SET_MOBILE_PHONE_RADIO_STATE(FALSE)
									SET_AUDIO_FLAG("MobileRadioInGame", FALSE)
								ENDIF
								
								IF DOES_CUTSCENE_HAVE_REGISTERED_ENTITIES(iCutsceneToUse, eCutType)
									
									REGISTER_CUTSCENE_ENTITIES(iCutsceneToUse, TRUE, eCutType)
										
									PRINTLN("[Cutscene_Mocap] - MOCAP CUTSCENE ENTITIES SHOULD BE INVISIBLE")
									
								ENDIF
								
								MANUALLY_REGISTER_SPECIFIC_ENTITIES(sMocapName, eCutType, iCutsceneToUse)
								
								BOOL bRemoveHeistGear
								bRemoveHeistGear = CAN_REMOVE_HEIST_GEAR_FOR_CUTSCENE(sMocapName, DEFAULT, eCutType, iCutsceneToUse)
								IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_MOCAP_REMOVED_LOCAL_PLAYER_MASK)
									IF CAN_REMOVE_MASKS_FOR_HEIST_CUTSCENE(sMocapName, DEFAULT, eCutType, iCutsceneToUse)
										
										REMOVE_MASKS_AND_REBREATHERS_FOR_CUTSCENES(LocalPlayerPed, LocalPlayer, INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, GET_PLAYER_CHOSEN_MASK(LocalPlayer)), bRemoveHeistGear)
										SET_BIT(iLocalBoolCheck5, LBOOL5_MOCAP_REMOVED_LOCAL_PLAYER_MASK)
									ELIF bRemoveHeistGear
										REMOVE_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
									ENDIF
								ENDIF
																	
								IF (IS_A_STRAND_MISSION_BEING_INITIALISED() AND eCutType = FMMCCUT_ENDMOCAP)						
									
									IF NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_LATE_SPECTATE)
									AND NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_WAIT_END_SPECTATE)
						           		CLEAR_MISSION_CONTROLLER_JOINED_AS_SPECTATOR_FOR_CUTSCENE()
									ELSE
										SET_MISSION_CONTROLLER_JOINED_AS_SPECTATOR_FOR_CUTSCENE()
									ENDIF
									
									SET_MY_STRAND_MISSION_TEAM(GET_CUTSCENE_TEAM())
									
									REGISTER_CUTSCENE_PLAYERS(iCutsceneTeam, iCutsceneToUse, eCutType, bIncludePlayers, bCloneLocalPlayer)
									
									INT i 
									FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
										IF mnCutscenePlayerModels[i] != DUMMY_MODEL_FOR_SCRIPT											
											SET_MODEL_AS_NO_LONGER_NEEDED(mnCutscenePlayerModels[i])
										ENDIF
									ENDFOR
																		
									SET_STRAND_MISSION_CUTSCENE_NAME(sMocapName)
									
									SET_SCRIPT_CAN_START_CUTSCENE(GET_THREADID_OF_SCRIPT_WITH_THIS_NAME("FMMC_Launcher"))
									
									IF NOT IS_LOCAL_PLAYER_PED_IN_CUTSCENE()
									OR bCloneLocalPlayer
										IF bIsAnySpectator
											SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, FALSE)										
										ENDIF
										PRINTLN("[Cutscene_Mocap] - PLAYER NOT REGISTERED FOR CUTSCENE SETTING INVISIBLE")
									ENDIF									
									
									PRINTLN("[Cutscene_Mocap] - SET_SCRIPT_CAN_START_CUTSCENE - GET_THREADID_OF_SCRIPT_WITH_THIS_NAME(\"FMMC_Launcher\")")
								ELSE
									clear_prints()
									clear_help()
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									
									IF bIncludePlayers
										REGISTER_CUTSCENE_PLAYERS(iCutsceneTeam, iCutsceneToUse, eCutType, bIncludePlayers, bCloneLocalPlayer)
									ENDIF
															
									IF NOT bIsSCTV
									AND NOT bIsAnySpectator
										NET_SET_PLAYER_CONTROL(LocalPlayer,FALSE)
									ENDIF
							
									IF NOT IS_LOCAL_PLAYER_PED_IN_CUTSCENE()
									OR bCloneLocalPlayer
										IF bIsAnySpectator
											SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, FALSE)
										ENDIF
										PRINTLN("[Cutscene_Mocap] - PLAYER NOT REGISTERED FOR CUTSCENE SETTING INVISIBLE")
									ENDIF

									SET_STORE_ENABLED(FALSE)
									
									SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
									PRINTLN("[Cutscene_Mocap] - SET_SCRIPTS_SAFE_FOR_CUTSCENE = TRUE")
								ENDIF
								
								INT iCutsceneBitset									
								IF eCutType = FMMCCUT_ENDMOCAP
									iCutsceneBitset = g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitset
								ELIF iCutsceneToUse != -1
									iCutsceneBitset = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset
								ENDIF
							  
								IF NOT bIsAnySpectator
								    IF NOT IS_BIT_SET(iCutsceneBitset, ci_CSBS_Hide_Your_Player)
										IF NOT IS_LOCAL_PLAYER_PED_IN_CUTSCENE()
										OR bCloneLocalPlayer
									   		SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, FALSE)
									    	PRINTLN("[Cutscene_Mocap] - SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE( FALSE, FALSE ) - NOT IN CUTSCENE ")
										ELSE
											SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(TRUE, FALSE)
									    	PRINTLN("[Cutscene_Mocap] - SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE( TRUE, FALSE ) ")
										ENDIF
									ENDIF
								ENDIF
								
								g_sTransitionSessionData.bMissionCutsceneInProgress = TRUE
								PRINTLN("[Cutscene_Mocap] - Setting bMissionCutsceneInProgress to TRUE.")
								
								IF eCutType = FMMCCUT_ENDMOCAP
									SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REGISTERED_ENTITIES_FOR_END_MOCAP_PASS_OVER)
								ENDIF
								
								g_sTransitionSessionData.sStrandMissionData.bCutsceneEntityPassoverMustWaitForAllClients = TRUE
								
								IF iCutsceneToUse != -1		
								
									REMOVE_PLAYER_FROM_VEHICLE_AT_CUTSCENE_START(iCutsceneToUse, FALSE)
									
									IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_WarpPlayersStart)
									AND NOT IS_VECTOR_ZERO(vStartPos)
									AND fStartHeading != ciINVALID_CUTSCENE_WARP_HEADING
									AND NOT bIsSCTV
										
										IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(LocalPlayerPed), vStartPos) > 100.0
											PRINTLN("[Cutscene_Mocap] - WARPED AT THE BEGINNING OF THE CUTSCENE TO: Position = ", vStartPos," , Heading = ", fStartHeading)
											NET_WARP_TO_COORD( vStartPos,		//
															fStartHeading,	//
															!SHOULD_CUTSCENE_REMOVE_PLAYER_FROM_VEHICLE_AT_START( iCutsceneToUse, FALSE ),	// bKeepVehicle
															FALSE,																	// bLeavePedBehind
															FALSE,																	// bDontSetCameraBehindPlayer
															FALSE,																	// bKillLeftBehindPed
															TRUE,																	// bKeepPortableObjects
															TRUE )																	// bDoQuickWarp
										ENDIF
									ENDIF
								ENDIF
																
								PRINTLN("[Cutscene_Mocap] - iEndCutscene: ", MC_ServerBD.iEndCutscene, " sMocapName: ", sMocapName)
								
								IF eCutType = FMMCCUT_ENDMOCAP								
									IF NOT SHOULD_CUTSCENE_KEEP_AUDIO_SCORE()
										PRINTLN("[Cutscene_Mocap] - Stopping Audio Score for Cutscene.")
										MC_SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
									ENDIF
									
									IF g_FMMC_STRUCT.iEndCutsceneWeather != 0
										PRINTLN("[Cutscene_Mocap] - SET_WEATHER_FOR_FMMC_MISSION being called with: ", g_FMMC_STRUCT.iEndCutsceneWeather)
										SET_WEATHER_FOR_FMMC_MISSION(g_FMMC_STRUCT.iEndCutsceneWeather, TRUE)									
										SET_BIT(iLocalBoolCheck2, LBOOL2_WEATHER_SET)
										MC_serverBD.iWeather = g_FMMC_STRUCT.iEndCutsceneWeather
									ENDIF
								ENDIF
									
								IF (NOT IS_A_STRAND_MISSION_BEING_INITIALISED() AND eCutType = FMMCCUT_ENDMOCAP)
								AND NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_PLAYING_END_CUTSCENE_MUSIC)												
								AND g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneMusicMood != 0
									STRING sMusicEvent
									sMusicEvent = GET_MOOD_NAME_FOR_SONG(g_FMMC_STRUCT.iAudioScore, g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneMusicMood)
									
									IF NOT IS_STRING_NULL_OR_EMPTY(sMusicEvent)
										PRINTLN("[Cutscene_Mocap] - Playing End Mocap Music, Mood: ", sMusicEvent)
										
										IF NOT AUDIO_IS_SCRIPTED_MUSIC_PLAYING()
											SET_MISSION_MUSIC(g_FMMC_STRUCT.iAudioScore, FALSE)
										ENDIF
										
										TRIGGER_MUSIC_EVENT(sMusicEvent)
										
										SET_BIT(iLocalBoolCheck34, LBOOL34_PLAYING_END_CUTSCENE_MUSIC)
									ELSE
										PRINTLN("[Cutscene_Mocap] - Cannpt play End Mocap Music, Mood is null.")
									ENDIF
								ENDIF
								
								IF (IS_A_STRAND_MISSION_BEING_INITIALISED() AND eCutType = FMMCCUT_ENDMOCAP)
																											
									IF NOT IS_VECTOR_ZERO(GET_CUTSCENE_COORDS(iCutsceneTeam, FMMCCUT_ENDMOCAP))
										STORE_STRAND_MISSION_END_CUTSCENE_TYPE(MC_ServerBD.iEndCutscene)
										g_sTransitionSessionData.sStrandMissionData.vCutsceneOverrideCoord		= GET_CUTSCENE_COORDS(iCutsceneTeam, FMMCCUT_ENDMOCAP)
										g_sTransitionSessionData.sStrandMissionData.fCutsceneOverrideHeading	= GET_CUTSCENE_HEADING(iCutsceneTeam, FMMCCUT_ENDMOCAP)
										IF g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneMusicMood != 0
											g_sTransitionSessionData.sStrandMissionData.iCutsceneMusicType			= g_FMMC_STRUCT.iAudioScore
											g_sTransitionSessionData.sStrandMissionData.iCutsceneMusicMood			= g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneMusicMood
										ENDIF
										g_sTransitionSessionData.sStrandMissionData.iEndCutsceneConcatBS = MC_ServerBD.iEndCutsceneConcatBS
		
										PRINTLN("[Cutscene_Mocap] - START_CUTSCENE not called because it's a strand mission cut scene called - Overriding Position to: ", g_sTransitionSessionData.sStrandMissionData.vCutsceneOverrideCoord, " and heading: ", g_sTransitionSessionData.sStrandMissionData.fCutsceneOverrideHeading, " Music Score: ", g_sTransitionSessionData.sStrandMissionData.iCutsceneMusicType, " Music Mood: ", GET_MUSIC_STATE_NAME(g_sTransitionSessionData.sStrandMissionData.iCutsceneMusicMood))
									ELSE
										PRINTLN("[Cutscene_Mocap] - START_CUTSCENE not called because it's a strand mission cut scene called")
									ENDIF
									
								ELIF SHOULD_MOCAP_CUTSCENE_PLAY_AT_SPECIFIED_COORDS(eCutType, iCutsceneToUse)
									VECTOR vCoord
									vCoord = GET_CUTSCENE_COORDS(iCutsceneTeam, eCutType)
									PRINTLN("[Cutscene_Mocap] - start_cutscene_at_coords called with Coord: ", vCoord)				
									
									REQUEST_CUT_FILE(sMocapName)
									
									IF HAS_CUT_FILE_LOADED(sMocapName)
										START_CUTSCENE_AT_COORDS(vCoord)
										
										INT iLoopMax
										iLoopMax = GET_CUT_FILE_CONCAT_COUNT(sMocapName)
										IF iLoopMax > 0
											PRINTLN("[Cutscene_Mocap] - Cutscene has ", iLoopMax, " concats.")
											INT i
											FOR i = 0 TO iLoopMax-1
												IF (eCutType = FMMCCUT_ENDMOCAP AND MC_IS_CONCAT_VALID_FOR_END_CUTSCENE(MC_ServerBD.iEndCutscene, i, MC_ServerBD.iEndCutsceneConcatBS))
												OR (eCutType = FMMCCUT_MOCAP AND MC_IS_CONCAT_VALID_FOR_MID_MISSION_CUTSCENE(iCutsceneToUse, i))
													PRINTLN("[Cutscene_Mocap] - Setting Concat ", i, " to vCoord: ", vCoord)
													SET_CUTSCENE_ORIGIN(vCoord, GET_CUTSCENE_HEADING(iCutsceneTeam, eCutType, iCutsceneToUse), i)
												ELSE
													PRINTLN("[Cutscene_Mocap] - NOT Setting Concat ", i, " it is flagged as invalid (MC_IS_CONCAT_VALID_FOR_END_CUTSCENE).")
												ENDIF
											ENDFOR
											REMOVE_CUT_FILE(sMocapName)
										ELSE
											PRINTLN("[Cutscene_Mocap] - Cutscene has no concats")
										ENDIF
									ELSE
										PRINTLN("[Cutscene_Mocap] - Waiting for Cut File to load....")
										RETURN FALSE
									ENDIF									
								ELSE																		
									CLEAR_PRINTS()
									
									IF NOT IS_STRING_EMPTY(sMocapName)//url:bugstar:4628425
										PRINTLN("[Cutscenes][Cutscene_Mocap] START_CUTSCENE called for iCutsceneToUse: ", iCutsceneToUse)
										START_CUTSCENE()
									ELSE
										PRINTLN("[Cutscenes][Cutscene_Mocap] Not calling START_CUTSCENE, but that's okay if this is placeholder.")
									ENDIF
								ENDIF
								
								IF IS_LOCAL_PLAYER_USING_DRONE()
									SET_DRONE_FORCE_EXTERNAL_CLEANING_UP(TRUE)
								ENDIF
								
								IF eCutType = FMMCCUT_ENDMOCAP
									CLEANUP_FLARE_PROPS_FOR_CUTSCENE()
									
									//If we're on the end mo cap then set that we're over so STCTV doesn't clean up
									IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
										SET_MISSION_OVER_AND_ON_LB_FOR_SCTV()
									ENDIF
								ENDIF
																
								IF IS_A_STRAND_MISSION_BEING_INITIALISED()
									INITIALISE_CELEBRATION_SCREEN_DATA(sCelebrationData)
								ENDIF
								
								CLEAR_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bStartedCelebrationBeforeCutsceneEnded)
																
								IF eCutType = FMMCCUT_ENDMOCAP
								AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
									FREEZE_TOD_FOR_CUTSCENE()
								ENDIF
								
								IF NOT IS_ENTITY_VISIBLE(LocalPlayerPed)
									IF NOT IS_PLAYER_SPECTATING(LocalPlayer)
										SET_ENTITY_VISIBLE(LocalPlayerPed, TRUE)
									ENDIF 
								ENDIF
								
								SET_REGISTERED_ENTITIES_VISIBLE_FOR_CUTSCENE(eCutType, iCutsceneToUse)
								
								IF IS_CUTSCENE_PLAYING()
									PRINTLN("[Cutscene_Mocap] - IS_CUTSCENE_PLAYING - true moving to 1")			
								ENDIF
								
								REINIT_NET_TIMER(tdSafetyTimerCutsceneLengthLimit)						

								SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_1)
							ELSE
								PRINTLN("[Cutscene_Mocap] - waiting for cutscene timer")
							ENDIF
						ELSE
							PRINTLN("[Cutscene_Mocap] - waiting for CLONE_MOCAP_PLAYERS")
						ENDIF // End of IF HAVE_MOCAP_START_CONDITIONS_BEEN_MET()
					ELSE
						PRINTLN("[Cutscene_Mocap] - waiting for Conor's apartment cutscene to complete")
						PRINTLN("[Cutscene_Mocap] - CURRENT NETWORK TIMER: ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					IF NOT HAS_CUTSCENE_LOADED()
						IF NOT IS_STRING_NULL_OR_EMPTY(sMocapName)
							IF (eCutType = FMMCCUT_MOCAP AND g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iVariationBS != 0 OR MC_DOES_MID_MISSION_CUTSCENE_USE_CUSTOM_CONCATS(iCutsceneToUse))
							OR (eCutType = FMMCCUT_ENDMOCAP AND MC_DOES_END_CUTSCENE_USE_CONCATS(MC_ServerBD.iEndCutscene))
								CUTSCENE_SECTION cutSectionFlags
								cutSectionFlags = GET_CUTSCENE_SECTION_FLAGS(iCutsceneToUse, eCutType)					
								PRINTLN("[Cutscenes] HANDLE_STREAMING_MOCAP - REQUEST_CUTSCENE_WITH_PLAYBACK_LIST (3) with creator cutscene section flags: ", ENUM_TO_INT(cutSectionFlags))						
								REQUEST_CUTSCENE_WITH_PLAYBACK_LIST( sMocapName, cutSectionFlags, GET_CUTSCENE_PLAYBACK_FLAGS())
							ELSE
								REQUEST_CUTSCENE(sMocapName, GET_CUTSCENE_PLAYBACK_FLAGS())
							ENDIF
						ENDIF
						CPRINTLN( DEBUG_CONTROLLER,"[Cutscenes][Cutscene_Mocap] waiting for HAS_CUTSCENE_LOADED")
					ELIF NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED )
						CPRINTLN( DEBUG_CONTROLLER,"[Cutscenes][Cutscene_Mocap] waiting for LBOOL2_CUTSCENE_STREAMED ")
					ENDIF
					#ENDIF
					
					IF NOT HAS_NET_TIMER_STARTED( streamCSFailsafeTimer )
						PRINTLN("[Cutscenes][Cutscene_Mocap] Starting the cutscene failsafe timer." )
						START_NET_TIMER( streamCSFailsafeTimer )
					ENDIF
					
					PRINTLN("[Cutscene_Mocap] - waiting for HAS_CUTSCENE_LOADED or LBOOL2_CUTSCENE_STREAMED - CURRENT NETWORK TIMER: ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
				ENDIF
			ELSE
				CPRINTLN( DEBUG_CONTROLLER,"[Cutscenes] waiting for ARE_TEAM_CUTSCENE_PLAYERS_SELECTED")
				PRINTLN("[Cutscene_Mocap] - waiting for  ARE_TEAM_CUTSCENE_PLAYERS_SELECTED - CURRENT NETWORK TIMER: ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
			ENDIF
			
		BREAK
		
		CASE MOCAPRUNNING_STAGE_1
					
			PRINTLN("[Cutscenes][Cutscene_Mocap] case 1" )

			IF IS_A_STRAND_MISSION_BEING_INITIALISED() AND eCutType = FMMCCUT_ENDMOCAP
				IF ARE_ALL_PLAYERS_SYNCED_FOR_ENDMOCAP_PASSOVER_ENTITY_REGISTRATION()
					PRINTLN("[Cutscene_Mocap] - SET_MISSION_CONTROLLER_READY_TO_START_CUTSCENE called")
					SET_MISSION_CONTROLLER_READY_TO_START_CUTSCENE()
				ELSE					
					PRINTLN("[Cutscene_Mocap] - Waiting to call SET_MISSION_CONTROLLER_READY_TO_START_CUTSCENE some players are not ready.")
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET( MC_playerBD[ iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR )				
				IF NOT (eCutType = FMMCCUT_ENDMOCAP	AND IS_A_STRAND_MISSION_BEING_INITIALISED())					
		       		MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE, FALSE, TRUE, FALSE)
				ENDIF
			ELSE
				MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, TRUE, FALSE, TRUE, FALSE)
			ENDIF

			IF NOT bLocalPlayerOK
				PRINTLN("[Cutscene_Mocap] - NOT bLocalPlayerOK")
				SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_2)
				RETURN FALSE
			ENDIF
			
			IF eCutType = FMMCCUT_ENDMOCAP
			AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
				FREEZE_TOD_FOR_CUTSCENE()
			ENDIF
			
			BOOL bRemoveHeistGear
			bRemoveHeistGear = CAN_REMOVE_HEIST_GEAR_FOR_CUTSCENE(sMocapName, FALSE, eCutType, iCutsceneToUse)
			
			IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_MOCAP_REMOVED_LOCAL_PLAYER_MASK)
				IF CAN_REMOVE_MASKS_FOR_HEIST_CUTSCENE(sMocapName, FALSE, eCutType, iCutsceneToUse)
					REMOVE_MASKS_AND_REBREATHERS_FOR_CUTSCENES(LocalPlayerPed, LocalPlayer, INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, GET_PLAYER_CHOSEN_MASK(LocalPlayer)), bRemoveHeistGear)
					SET_BIT(iLocalBoolCheck5, LBOOL5_MOCAP_REMOVED_LOCAL_PLAYER_MASK)
				ELIF bRemoveHeistGear
					REMOVE_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
				ENDIF
			ENDIF

			IF IS_CUTSCENE_PLAYING()
			#IF IS_DEBUG_BUILD
			OR IS_MOCAP_CUTSCENE_A_PLACEHOLDER(eCutType, sMocapName)
			#ENDIF
				
				IF MC_playerBD[iPartToUse].iteam = iScriptedCutsceneTeam
				AND iCutsceneToUse != -1
					IF NOT IS_BIT_SET( MC_serverBD.iObjectiveMidPointBitset[ MC_playerBD[iPartToUse].iteam], MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] )
						IF g_FMMC_STRUCT.sMocapCutsceneData[ iCutsceneToUse ].iMidpointTimer != 0
							IF NOT HAS_NET_TIMER_STARTED(tdCutsceneMidpoint)
								PRINTLN("[Cutscenes][Cutscene_Mocap] - Case 1 - Initialising tdCutsceneMidpoint, cutscene midpoint timer" )
								REINIT_NET_TIMER(tdCutsceneMidpoint)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF ANIMPOSTFX_IS_RUNNING("MP_Celeb_Preload_Fade")
					ANIMPOSTFX_STOP("MP_Celeb_Preload_Fade")
					PRINTLN("[Cutscene_Mocap] - [SAC] - stopped MP_Celeb_Preload_Fade for mocap cutscene.")
				ENDIF
				
				IF IS_BIT_SET(iLocalBoolCheck30, LBOOL30_FORCED_HINT_CAM_TRIGGERED)
					CLEAR_FORCED_HINT_CAM_VARS()
				ENDIF
				
				//Avoid any 1 frame pops.
				LISTEN_FOR_CUTSCENE_EVENTS( sMocapName )
				
				PROCESS_SPECIFIC_CUTSCENE_FUNCTIONALITY(sMocapName)
				
				IF eCutType != FMMCCUT_ENDMOCAP					
					CLEAR_AREA_OF_OBJECTS(vStartPos, 100, CLEAROBJ_FLAG_FORCE )
					CLEAR_AREA_OF_PROJECTILES(vStartPos, 100, TRUE)
					PRINTLN("[Cutscene_Mocap] - DEFAULT - CLEARING AREA OF PROJECTILES AT <<", vStartPos.X, ", ", vStartPos.Y, ", ", vStartPos.Z, ">> with radius: 100")
				ELSE
					PRINTLN("[Cutscene_Mocap] - END OF MISSION CUTSCENE NOT DOING DEFAULT CLEAR ATTEMPTING SPECIFIC...")
				ENDIF
				
				//Clear specified projectile area from creator variables
				IF eCutType = FMMCCUT_ENDMOCAP
					IF g_FMMC_STRUCT.sEndMocapSceneData.fClearRadius > 0.0
						CLEAR_AREA_OF_PROJECTILES(g_FMMC_STRUCT.sEndMocapSceneData.vClearCoOrds, g_FMMC_STRUCT.sEndMocapSceneData.fClearRadius, TRUE)
						PRINTLN("[Cutscene_Mocap] - CLEARING AREA OF PROJECTILES AT <<", g_FMMC_STRUCT.sEndMocapSceneData.vClearCoOrds.X, ", ", g_FMMC_STRUCT.sEndMocapSceneData.vClearCoOrds.Y, ", ", g_FMMC_STRUCT.sEndMocapSceneData.vClearCoOrds.Z, ">> with radius: ", g_FMMC_STRUCT.sEndMocapSceneData.fClearRadius)
					ELSE
						PRINTLN("[Cutscene_Mocap] - NO CLEAR AREA SETUP IN CREATOR!")
					ENDIF
				ELSE
					IF iCutsceneToUse > -1
						IF g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].fClearRadius > 0.0
							CLEAR_AREA_OF_PROJECTILES(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vClearCoOrds, g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].fClearRadius, TRUE)
							PRINTLN("[Cutscene_Mocap] - CLEARING AREA OF PROJECTILES AT <<", g_FMMC_STRUCT.sEndMocapSceneData.vClearCoOrds.X, ", ", g_FMMC_STRUCT.sEndMocapSceneData.vClearCoOrds.Y, ", ", g_FMMC_STRUCT.sEndMocapSceneData.vClearCoOrds.Z, ">> with radius: ", g_FMMC_STRUCT.sEndMocapSceneData.fClearRadius)
						ELSE
							PRINTLN("[Cutscene_Mocap] - NO CLEAR AREA SETUP IN CREATOR!")
						ENDIF
					ENDIF
				ENDIF
				
				PROCESS_CUTSCENE_INIT_SPECIAL_CASE(sMocapName)
				
				IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()			
					IF bLocalPlayerOK	
						//Any entities created during a cutscene should be networked
						IF NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(LocalPlayer)
							NETWORK_SET_IN_MP_CUTSCENE(TRUE, TRUE) 
							PRINTLN("[TS] [MSRAND][Cutscene_Mocap] - NETWORK_SET_IN_MP_CUTSCENE(TRUE, TRUE)")
						ELSE
							PRINTLN("[TS] [MSRAND][Cutscene_Mocap] - Player already in MP cutscene")
						ENDIF
						SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
						CPRINTLN(DEBUG_MIKE, "[Cutscenes][Cutscene_Mocap] NETWORK_SET_IN_MP_CUTSCENE(TRUE, TRUE)")
					ELSE
						PRINTLN("[TS] [MSRAND][Cutscene_Mocap] - player NOT OK not settting MP_CUTSCENE")
					ENDIF
				ENDIF

				int i

				for i = 0 to (FMMC_MAX_CUTSCENE_PLAYERS - 1)
					if does_entity_exist(MocapPlayerPed[i])
						if not is_ped_injured(MocapPlayerPed[i])
							IF i < GET_MOCAP_NUMBER_OF_PLAYERS(sMocapName)
								PRINTLN("[MSRAND] [PED CUT][Cutscene_Mocap] * - SET_ENTITY_VISIBLE(MocapPlayerPed[", i, "], TRUE)")
								set_entity_visible(MocapPlayerPed[i], true)
							ENDIF
							
							IF DOES_ENTITY_EXIST(MocapPlayerWeaponObjects[i])
								SET_ENTITY_VISIBLE(MocapPlayerWeaponObjects[i], TRUE)
							ENDIF
						endif 
					endif 
				endfor
				
				IF DOES_ENTITY_EXIST(playersWeaponObject)
					SET_ENTITY_VISIBLE(playersWeaponObject, TRUE)
				ENDIF
					
				SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableVoiceDrivenMouthMovement, TRUE)

				PRINTLN("[Cutscene_Mocap] - MC_ServerBD.iEndCutscene = ", MC_ServerBD.iEndCutscene)
				
				destroy_all_cams()
				
				render_script_cams(false, false, DEFAULT, DEFAULT, TRUE)
				
				DO_RUN_MOCAP_CHECKS()				
				
				//Any entities created during a cutscene should be networked (should already have been called, this is a backup)
				IF NETWORK_IS_IN_MP_CUTSCENE()
					SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
					PRINTLN("[Cutscene_Mocap] - Calling SET_NETWORK_CUTSCENE_ENTITIES(TRUE)")
				ELSE
					PRINTLN("[Cutscene_Mocap] - Not calling SET_NETWORK_CUTSCENE_ENTITIES(TRUE) as NETWORK_IS_IN_MP_CUTSCENE() is false")
				ENDIF
				
				IF NOT IS_INTRO_CUTSCENE_RULE_ACTIVE()
					BOOL bClearTimecycle
					bClearTimecycle = TRUE
					IF eCutType = FMMCCUT_ENDMOCAP
					AND g_FMMC_STRUCT.sEndMocapSceneData.fEndCutsceneTimeCycleFade != 1.0
						bClearTimecycle = FALSE
						PRINTLN("[Cutscene_Mocap] - bClearTimecycle set to false")
					ENDIF
					IF bClearTimecycle
						CLEAR_TIMECYCLE_MODIFIER()
					ENDIF
					TOGGLE_PAUSED_RENDERPHASES(TRUE)
				ENDIF
				
				//Clear the area for most scenes.
				IF bClearAreaAndParticles
					IF eCutType = FMMCCUT_ENDMOCAP
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet4, ci_CSBS4_CutsceneClearAreaAtEndOnly)
							CLEAR_CUTSCENE_AREA(g_FMMC_STRUCT.sEndMocapSceneData.fClearRadius, g_FMMC_STRUCT.sEndMocapSceneData.vClearCoOrds, g_FMMC_STRUCT.sEndMocapSceneData.vReplacementArea, TRUE, IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet3, ci_CSBS3_ClearPersonalVehicles))
							PRINTLN("[Cutscene_Mocap] - End of mission cutscene CLEAR_AREA")
						ENDIF
					ELSE
						IF iCutsceneToUse != -1
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet4, ci_CSBS4_CutsceneClearAreaAtEndOnly)
								CLEAR_CUTSCENE_AREA(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].fClearRadius, g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vClearCoOrds, g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vReplacementArea, TRUE, IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_ClearPersonalVehicles))
								PRINTLN("[Cutscene_Mocap] - NON End of mission cutscene CLEAR_AREA")
							ENDIF
						ELSE
							CLEAR_AREA(vStartPos, 100.0, TRUE, TRUE, DEFAULT, FALSE)
							PRINTLN("[Cutscene_Mocap] - RUN_MOCAP_CUTSCENE() - Generical fall back CLEAR_AREA")
						ENDIF
					ENDIF					
					
					REMOVE_DECALS_IN_RANGE(vStartPos, 100)
				ENDIF
				
				PRINTLN("[Cutscene_Mocap] - IS_CUTSCENE_PLAYING - true moving to 2")
				
				SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_2)
			ELSE
			
				IF NOT HAS_CUTSCENE_LOADED() 
				AND NOT IS_STRING_NULL_OR_EMPTY( sMocapName )
					PRINTLN("[Cutscene_Mocap] - unloaded mocap after starting, re-requesting cutscene: ", sMocapName)
					
					IF (eCutType = FMMCCUT_MOCAP AND g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iVariationBS != 0 OR MC_DOES_MID_MISSION_CUTSCENE_USE_CUSTOM_CONCATS(iCutsceneToUse))
					OR (eCutType = FMMCCUT_ENDMOCAP AND MC_DOES_END_CUTSCENE_USE_CONCATS(MC_ServerBD.iEndCutscene))
						CUTSCENE_SECTION cutSectionFlags
						cutSectionFlags = GET_CUTSCENE_SECTION_FLAGS(iCutsceneToUse, eCutType)
						PRINTLN("[Cutscenes] HANDLE_STREAMING_MOCAP - REQUEST_CUTSCENE_WITH_PLAYBACK_LIST (3) with creator cutscene section flags: ", ENUM_TO_INT(cutSectionFlags))						
						REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(sMocapName, cutSectionFlags, GET_CUTSCENE_PLAYBACK_FLAGS())
					ELSE
						REQUEST_CUTSCENE(sMocapName, GET_CUTSCENE_PLAYBACK_FLAGS())
					ENDIF

				ELSE
					PRINTLN("[Cutscene_Mocap] - mocap is not playing ", sMocapName)
				ENDIF
				
				PRINTLN("[Cutscene_Mocap] - waiting for IS_CUTSCENE_PLAYING")
				IF NOT HAS_NET_TIMER_STARTED(mocapFailsafe)
					REINIT_NET_TIMER(mocapFailsafe)
				ELSE
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(mocapFailsafe) > 30000
						SET_BIT(ilocalboolcheck,LBOOL_HIT_MOCAP_FAIL_SAFE)
						PRINTLN("[Cutscene_Mocap] - waiting for IS_CUTSCENE_PLAYING - FAIL SAFE TIMER!!!!!!")
						SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_2)
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE MOCAPRUNNING_STAGE_2
		
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REACHED_CUT_PROCESS_SHOTS)
			
			SET_BIT(iLocalBoolCheck31, LBOOL31_PRIZED_VEHICLE_SHOULD_HIDE)
			
			IF MC_playerBD[iPartToUse].iteam = iScriptedCutsceneTeam
			AND iCutsceneToUse != -1
				IF NOT IS_BIT_SET( MC_serverBD.iObjectiveMidPointBitset[ MC_playerBD[iPartToUse].iteam  ], MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] )
					
					IF IS_STRING_NULL_OR_EMPTY(sMocapName)
						IF g_FMMC_STRUCT.sMocapCutsceneData[ iCutsceneToUse ].iMidpointTimer != 0
							IF NOT HAS_NET_TIMER_STARTED(tdCutsceneMidpoint)
								PRINTLN("[Cutscenes][Cutscene_Mocap] - Case 2 - Initialising tdCutsceneMidpoint, cutscene midpoint timer" )
								REINIT_NET_TIMER(tdCutsceneMidpoint)
							ELSE
								IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdCutsceneMidpoint) >= (g_FMMC_STRUCT.sMocapCutsceneData[ iCutsceneToUse ].iMidpointTimer * 1000)
									PRINTLN("[Cutscenes] RUN_MOCAP_CUTSCENE, Case 2 - Midpoint timer has passed value in creator (", g_FMMC_STRUCT.sMocapCutsceneData[ iCutsceneToUse ].iMidpointTimer, " seconds), broadcast midpoint!" )
									BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam],MC_playerBD[iPartToUse].iteam)
									REINIT_NET_TIMER(tdCutsceneMidpoint)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			PRINTLN("[Cutscene_Mocap] - Processing Cutscene.")
			
			IF eCutType = FMMCCUT_ENDMOCAP			
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet3, ci_CSBS3_DontHidePersonalVehicles)
					PROCESS_HIDE_PERSONAL_VEHICLES_LOCALLY_FOR_PLAYER()
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_DontHidePersonalVehicles)
					PROCESS_HIDE_PERSONAL_VEHICLES_LOCALLY_FOR_PLAYER()
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck4, LBOOL4_MOCAP_START_FADE_IN_DONE)
			
				// Don't need the fade
				IF IS_SCREEN_FADED_IN()
				OR IS_SCREEN_FADING_IN()
				
					PRINTLN("[Cutscene_Mocap] - Mocap fade start - No fade in to perform, sealed off logic")
					SET_BIT(iLocalBoolCheck4, LBOOL4_MOCAP_START_FADE_IN_DONE)

				// need the fade
				ELIF IS_CUTSCENE_PLAYING()
				AND GET_CUTSCENE_TIME() > 0 				
					
					IF bIsAnySpectator	
						PRINTLN("[Cutscene_Mocap] - Mocap fade start - Specator not in apartment - Called DO_SCREEN_FADE_IN")
						DO_SCREEN_FADE_IN(GET_MOCAP_CUTSCENE_FADE_IN_TIME())
						SET_BIT(iLocalBoolCheck4, LBOOL4_MOCAP_START_FADE_IN_DONE)
					ELIF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_CUTSCENE_WARP_DONE)
						PRINTLN("[Cutscene_Mocap] - Mocap fade start - warp complete - Calling DO_SCREEN_FADE_IN")
						DO_SCREEN_FADE_IN(GET_MOCAP_CUTSCENE_FADE_IN_TIME())
						SET_BIT(iLocalBoolCheck4, LBOOL4_MOCAP_START_FADE_IN_DONE)
					ELSE
						PRINTLN("[Cutscene_Mocap] - Cutscene is playing and we shouldn't fade in")
					ENDIF
				ELSE
					PRINTLN("[Cutscene_Mocap] - Mocap fade start - Waiting for cutscene to start before fading in.")
				ENDIF

			ELIF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_MOCAP_END_FADE_OUT_DONE)
				
				IF IS_SCREEN_FADED_OUT()
				OR IS_SCREEN_FADING_OUT()
				OR NOT bShouldFadeAtEnd
				
					PRINTLN("[Cutscene_Mocap] - Mocap fade end - No fade out to perform, sealed off logic, bShouldFadeAtEnd: ", BOOL_TO_STRING(bShouldFadeAtEnd))
					SET_BIT(iLocalBoolCheck5, LBOOL5_MOCAP_END_FADE_OUT_DONE)
				
				ELIF IS_CUTSCENE_PLAYING()
				AND GET_CUTSCENE_TIME() >= GET_CUTSCENE_END_TIME() - 1000

					PRINTLN("[Cutscene_Mocap] - Mocap fade end - bShouldFadeAtEnd = TRUE")
					
					IF IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN()
						PRINTLN("[Cutscene_Mocap] - Mocap fade end - bShouldFadeAtEnd = TRUE - Called DO_SCREEN_FADE_OUT")
						DO_SCREEN_FADE_OUT(MOCAP_END_FADE_DURATION)
					ENDIF
					
					SET_BIT(iLocalBoolCheck5, LBOOL5_MOCAP_END_FADE_OUT_DONE)
				ENDIF
				
			ENDIF

			//Clear area one frame after START_CUTSCENE to fix things like 2089738
			IF bCutsceneAreaClearedLate = FALSE
			AND IS_CUTSCENE_PLAYING()
			AND GET_CUTSCENE_TIME() > 1200			
				
				CLEAR_AREA_OF_PROJECTILES(g_FMMC_STRUCT.sEndMocapSceneData.vClearCoOrds, g_FMMC_STRUCT.sEndMocapSceneData.fClearRadius)
				REMOVE_DECALS_IN_RANGE(vStartPos, 100)
				
				bCutsceneAreaClearedLate = TRUE
			ENDIF
			
			IF (NOT IS_A_STRAND_MISSION_BEING_INITIALISED() AND eCutType = FMMCCUT_ENDMOCAP)
				KILL_ALARM_SOUNDS()
			ENDIF	
			
			IF (IS_A_STRAND_MISSION_BEING_INITIALISED() AND eCutType = FMMCCUT_ENDMOCAP)
				PRINTLN("[Cutscene_Mocap] - IS_A_STRAND_MISSION_BEING_INITIALISED skipping mocap going to case 4 ")
				SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_4)
				RETURN FALSE
			ENDIF
			
			IF eCutType = FMMCCUT_ENDMOCAP
				FREEZE_TOD_FOR_CUTSCENE()
			ENDIF
			
			LISTEN_FOR_CUTSCENE_EVENTS(sMocapName)
			
			PROCESS_SPECIFIC_CUTSCENE_FUNCTIONALITY(sMocapName)
			
			HANDLE_MANUAL_EXIT_STATES(iCutsceneToUse, iCutsceneTeam, vEndPos, fEndHeading, eCutType)
			
			PROCESS_SKIPPING_CUTSCENE(iCutsceneToUse, eCutType)
								
			IF IS_MOCAP_CUTSCENE_READY_TO_END( #IF IS_DEBUG_BUILD eCutType, sMocapName #ENDIF )
				
				PRINTLN("[Cutscene_Mocap] - finished cutscene, cleaning up" )

				CLEAR_BIT( iLocalBoolCheck9, LBOOL9_HAS_FIRST_OBJECTIVE_TEXT_BEEN_SHOWN )

				USE_FAKE_MP_CASH(FALSE)	
			
				IF IS_BIT_SET( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )
					TOGGLE_RENDERPHASES( TRUE )
					CLEAR_BIT( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )
					PRINTLN("[Cutscene_Mocap] - LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF set false- 2")
				ENDIF
				
				IF IS_CUTSCENE_PLAYING()
					STOP_CUTSCENE_IMMEDIATELY()
				ENDIF

				REMOVE_CUTSCENE()
				
				PRINTLN("[Cutscene_Mocap] - Clearing LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE ")
				CLEAR_BIT( iLocalBoolCheck5, LBOOL5_MID_MISSION_MOCAP )	
				CLEAR_BIT( iLocalBoolCheck7, LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE )
								
				CLEAR_BIT( iLocalBoolCheck9, LBOOL9_FORCE_STREAM_CUTSCENE)
				CLEAR_BIT(iLocalBoolCheck31, LBOOL31_CUTSCENE_RE_REQUESTED_AFTER_GAMEPLAY)
				CLEAR_BIT(iLocalBoolCheck9, LBOOL9_PRE_MOCAP_CINE_TRIGGERED)
				
				CLEAR_BIT(iLocalBoolCheck4, LBOOL4_MOCAP_START_FADE_IN_DONE)
				CLEAR_BIT(iLocalBoolCheck5, LBOOL5_MOCAP_END_FADE_OUT_DONE)
				
				IF eCutType != FMMCCUT_ENDMOCAP
				AND NOT bShowCelebrationScreen
					bGivePlayerControl = TRUE
				ENDIF
					
				CLEANUP_MP_CUTSCENE(IS_HELP_MESSAGE_BEING_DISPLAYED(), bGivePlayerControl)
				
				IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_WARP_TO_END_MOCAP_IN_PROGRESS)
					CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_WARP_TO_END_MOCAP_IN_PROGRESS)
					PRINTLN("[Cutscene_Mocap] - MOCAP - iABI_WARP_TO_END_MOCAP_IN_PROGRESS - CLEARED - C")
				ENDIF

				g_sTransitionSessionData.bMissionCutsceneInProgress = FALSE
				PRINTLN("[Cutscene_Mocap] - Setting bMissionCutsceneInProgress to FALSE.")

				IF( IS_BIT_SET( iLocalBoolCheck7, LBOOL7_WAS_VEH_RADIO_ENABLED_AT_CS_START ) )
					IF( NOT IS_ENTITY_DEAD( LocalPlayerPed ) )
						IF( IS_PED_SITTING_IN_ANY_VEHICLE( LocalPlayerPed ) )
							VEHICLE_INDEX veh
							veh = GET_VEHICLE_PED_IS_IN( LocalPlayerPed )
							IF( NOT IS_ENTITY_DEAD( veh ) )
								IF NETWORK_HAS_CONTROL_OF_ENTITY( veh )
									IF NOT IS_STRING_NULL_OR_EMPTY(sMyRadioStation)
										SET_VEH_RADIO_STATION(veh, sMyRadioStation)
										SET_VEHICLE_RADIO_ENABLED(veh, TRUE)
										PRINTLN("[Cutscene_Mocap] - SET_VEH_RADIO_STATION(veh, sMyRadioStation) model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(veh)), " sMyRadioStation = ", sMyRadioStation)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				CLEAR_BIT( iLocalBoolCheck7, LBOOL7_WAS_VEH_RADIO_ENABLED_AT_CS_START )
				CLEAR_BIT(iLocalBoolCheck5, LBOOL5_MOCAP_REMOVED_LOCAL_PLAYER_MASK)
								
				//Start the celebration screen early.
				IF ( bShowCelebrationScreen )
				OR NOT bIncludePlayers
					IF bShowCelebrationScreen
						//Start the celebration screen early.
						IF NOT IS_THIS_A_HEIST_MISSION()
							SET_SKYFREEZE_FROZEN()
							IF SHOULD_POSTFX_BE_WINNER_VERSION()
								PLAY_CELEB_WIN_POST_FX()
								PRINTLN("[Cutscene_Mocap] - ANIMPOSTFX_PLAY(MP_Celeb_Win, 0, TRUE) : bShowCelebrationScreen", bShowCelebrationScreen)
							ELSE
								PLAY_CELEB_LOSE_POST_FX()
								PRINTLN("[Cutscene_Mocap] - ANIMPOSTFX_PLAY(MP_Celeb_Lose, 0, TRUE): bShowCelebrationScreen", bShowCelebrationScreen)
							ENDIF
							
							PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS", FALSE)
						ENDIF
					ENDIF
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					IF eCutType != FMMCCUT_ENDMOCAP
					AND NOT bShowCelebrationScreen
						IF NOT bIsAnySpectator
							NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
						ENDIF
					ENDIF
					
					NETWORK_SET_VOICE_ACTIVE(true)
					NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
					
					PRINTLN("[Cutscene_Mocap] - NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)")
					
					IF DOES_ENTITY_EXIST(ClonePlayerForCutscene)
						DELETE_PED(ClonePlayerForCutscene)
					ENDIF
					
					PRINTLN("[Cutscene_Mocap] - SET_SCRIPTS_SAFE_FOR_CUTSCENE = FALSE")
				ENDIF
				
				CONCEAL_ALL_OTHER_PLAYERS(FALSE)
				
				PRINTLN("[Cutscene_Mocap] - CONCEAL_ALL_OTHER_PLAYERS(FALSE)")
				
				IF bIsSCTV					
					IF eCutType != FMMCCUT_ENDMOCAP
					AND NOT bShowCelebrationScreen
						SET_ENTITY_VISIBLE_IN_CUTSCENE(LocalPlayerPed, FALSE, FALSE)					
					ENDIF
					
					CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCTV_IN_MOCAP)
					PRINTLN("[Cutscene_Mocap] GLOBAL_SPEC_BS_SCTV_IN_MOCAP CLEARED")
				ENDIF
				
				IF bIsAnySpectator
					IF eCutType != FMMCCUT_ENDMOCAP
					AND NOT bShowCelebrationScreen
						SET_ENTITY_VISIBLE_IN_CUTSCENE(LocalPlayerPed, FALSE, FALSE)
					ENDIF
					
					DISABLE_SPECTATOR_RADIO(FALSE)
					PRINTLN("[Cutscene_Mocap] - CLEAR SPECTATOR FLAGS")
				ENDIF
				
				SET_SPECTATOR_RUNNING_CUTSCENE(FALSE)
					
				// url:bugstar:5832345 * If we're not already fading the screen out and our spectator target has not finish the cutscene then fadeout. 
				// This should hide all the popping and transition issues related to net syncing of player positions and clients being further/behind on processing the cutscene.
				IF bIsAnySpectator
				AND IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitset2, PBBOOL2_PLAYING_MOCAP_CUTSCENE)
				AND NOT IS_SCREEN_FADING_OUT()
				AND NOT IS_SCREEN_FADED_OUT()
					PRINTLN("[Cutscene_Mocap] - Player we are Spectating is still in a Mocap Cutscene. To avoid weird popping/streaming issues we need to fadeout.")
					START_INSTANCED_CONTENT_FADE_SCREEN_OUT_FOR_PERIOD_OF_TIME(0, 3000)
				ENDIF
				
				IF HAVE_SPECTATOR_COORDS_BEEN_OVERRIDDEN()
					CLEAR_SPECTATOR_OVERRIDE_COORDS()
				ENDIF
				
				DO_POST_RUN_MOCAP_CHECKS()
				
				CLEANUP_MOCAP_PLAYER_CLONES()
				
				CLEANUP_SPECIFIC_CUTSCENE_FUNCTIONALITY(sMocapName)
				
				CLEANUP_SPECIFIC_CUTSCENE_REFERENCES()
				
				IF NOT IS_BIT_SET( iLocalBoolCheck8, LBOOL8_PLAYER_EXIT_STATE_CALLED)
					UPDATE_ON_PLAYER_EXIT_STATE(iCutsceneToUse, iCutsceneTeam, vEndPos, fEndHeading, TRUE)
					PRINTLN("[Cutscene_Mocap] - NO PLAYER EXIT STATE WAS HIT IN HANDLE_MANUAL_EXIT_STATES DOING LOGIC IN CAMERA EXIT")
				ENDIF
				
				IF CAN_CUTSCENE_RESET_GAMEPLAY_CAMERA_PITCH_OR_HEADING(eCutType, iCutsceneToUse)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				ENDIF
				
				bCutsceneAreaClearedLate = FALSE
				
				CLEAR_BIT( MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_PLAYING_MOCAP_CUTSCENE )
				tlPlayerSceneHandle = ""
				RESET_NET_TIMER(streamCSFailsafeTimer)
				PRINTLN("[Cutscene_Mocap] - IS_CUTSCENE_PLAYING - FALSE / CAN_SET_EXIT_STATE_FOR_CAMERA = TRUE moving to 3" )
				
				SET_SRL_LONG_JUMP_MODE( FALSE )
					
				// Fly Cam Before Warp - Process
				IF eCutType = FMMCCUT_MOCAP
				AND iCutsceneToUse != -1
					IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_EndCutsceneInCover)
						SET_PLAYER_INTO_COVER_AFTER_CUTSCENE(GET_PLAYERS_FACE_DIRECTION_IN_COVER(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet3, iPlayerIndex))
					ENDIF	
					IF IS_BIT_SET(g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_UseFlyCamAfterCutscene_BeforeWarp)
						PROCESS_CAMERA_FLY_DOWN_AFTER_CUTSCENE(eCutType, iCutsceneToUse, vEndPos, fEndHeading)	
					ENDIF
				ENDIF
				
				PROCESS_CUTSCENE_JUST_ENDING_SPECIAL_CASE(sMocapName)

				SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_3)
			ELSE // Else we have not finished this cutscene yet
								
				IF IS_BIT_SET( iLocalBoolCheck8, LBOOL8_NO_MOCAP_AFTER_THIS_DROPOFF )
					CLEAR_BIT( iLocalBoolCheck8, LBOOL8_NO_MOCAP_AFTER_THIS_DROPOFF )
					
					PRINTLN("[Cutscene_Mocap] - Clearing LBOOL8_NO_MOCAP_AFTER_THIS_DROPOFF" )
				ENDIF
			
				IF IS_BIT_SET( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )
					TOGGLE_RENDERPHASES( TRUE )
					CLEAR_BIT( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )

					IF bIsAnySpectator
						PRINTLN("[Cutscene_Mocap] - Untoggle renderphases in this dropoff cutscene now that it's started. ")
						IF IS_SCREEN_FADED_OUT()
						OR IS_SCREEN_FADING_OUT()
							DO_SCREEN_FADE_IN(1500)
						ENDIF
					ENDIF
				ENDIF
				
				IF bLocalPlayerPedOk
					SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableVoiceDrivenMouthMovement , TRUE)
				ENDIF
				MANAGE_PLAYER_GESTURES()
				
			ENDIF
						
		BREAK
		
		CASE MOCAPRUNNING_STAGE_3
			
			// Fly Cam Before Warp - Process
			IF eCutType = FMMCCUT_MOCAP
			AND iCutsceneToUse != -1
			AND IS_BIT_SET(g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_UseFlyCamAfterCutscene_BeforeWarp)
				IF NOT PROCESS_CAMERA_FLY_DOWN_AFTER_CUTSCENE(eCutType, iCutsceneToUse, vEndPos, fEndHeading)
					PRINTLN("[Cutscene_Mocap] - Waiting for PROCESS_CAMERA_FLY_DOWN_AFTER_CUTSCENE to finish.")
					RETURN FALSE
				ENDIF
			ENDIF
		
			g_bAbortPropertyMenus = FALSE
			
			PRINTLN("[Cutscene_Mocap]- 3 g_bAbortPropertyMenus = FALSE ")
			
			IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
				IF bShowCelebrationScreen
					IF NOT IS_THIS_A_HEIST_MISSION()
						IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bStartedCelebrationBeforeCutsceneEnded)
							IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)								
								SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
								PRINTLN("[NETCELEBRATION] - [SAC] - ciCELEBRATION_BIT_SET_TriggerEndCelebration - call 2")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF HAVE_SPECTATOR_COORDS_BEEN_OVERRIDDEN()
				CLEAR_SPECTATOR_OVERRIDE_COORDS()
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck30, LBOOL30_MOCAP_FOCUS_ENTITY_SET)
				PRINTLN("[Cutscene_Mocap] - Clearing Focus Entity.")
				CLEAR_FOCUS()
				CLEAR_BIT(iLocalBoolCheck30, LBOOL30_MOCAP_FOCUS_ENTITY_SET)
			ENDIF
			
			IF IS_PLAYSTATION_PLATFORM()
				CLEAR_CONTROL_LIGHT_EFFECT(PLAYER_CONTROL)
			ENDIF
			
			PRINTLN( "[Cutscenes] [Cutscene_Mocap] - eMocapRunningCutsceneProgress: ", GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS_NAME(eMocapRunningCutsceneProgress), " - returned TRUE")
			RETURN TRUE
		
		BREAK
		
		//For the strand missions!
		CASE MOCAPRUNNING_STAGE_4
			PRINTLN("[Cutscenes] [Cutscene_Mocap] - STRAND MISSION - ENTERED CASE 4")

			g_bAbortPropertyMenus = FALSE
			PRINTLN("RUN_MOCAP_CUTSCENE- 4 g_bAbortPropertyMenus = FALSE ")
			IF HAVE_SPECTATOR_COORDS_BEEN_OVERRIDDEN()
				CLEAR_SPECTATOR_OVERRIDE_COORDS()
			ENDIF

			IF IS_PLAYSTATION_PLATFORM()
				CLEAR_CONTROL_LIGHT_EFFECT(PLAYER_CONTROL)
			ENDIF

			PRINTLN( "[Cutscenes] [Cutscene_Mocap] - eMocapRunningCutsceneProgress: ", GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS_NAME(eMocapRunningCutsceneProgress), " - returned TRUE")
			RETURN TRUE
		BREAK
		
	ENDSWITCH

	RETURN FALSE

ENDFUNC

PROC PROCESS_CUTSCENE_LIFT_FAKE_ARRIVAL_SFX(INT iCutsceneToUse)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_MocapElevatorArrivalSounds)
		EXIT
	ENDIF
		
	IF IS_VECTOR_ZERO(vPosNearestElevatorDoor)
		vPosNearestElevatorDoor = GET_NEAREST_DOOR_POSITION(GET_ENTITY_COORDS(PlayerPedToUse, FALSE))
		PRINTLN("[Cutscene] - iCutsceneToUse: ", iCutsceneToUse, " vPosNearestElevatorDoor:", vPosNearestElevatorDoor)
	ENDIF
		
	IF NOT IS_BIT_SET(bsSpecialCaseCutscene, BS_SCC_PLAYED_CUTSCENE_ELEVATOR_CLOSED)
		SET_BIT(bsSpecialCaseCutscene, BS_SCC_PLAYED_CUTSCENE_ELEVATOR_CLOSED)		
		PRINTLN("[Cutscene] - iCutsceneToUse: ", iCutsceneToUse, " Playing Fake_Arrival.")
		PLAY_SOUND_FROM_COORD(-1, "Fake_Arrival", vPosNearestElevatorDoor, "Union_Depository_Elevator_Sounds")
	ENDIF
	
ENDPROC


// [FIX_2020_CONTROLLER] - This function needs to be turned into the legacy version and a new one should be made for new content which excludes all of the zany weird special case "this_specific_cutscene" logic.
// I also believe a few areas of this function could do with being re-written as we won't have to worry about breaking old content. Such as Placing in vehicles and warping the peds. Also cloning the LOCALPLAYERPED for the cutscene instead of using the actual entity.
// Further to this allowing warping player in the middle of the cutscene so we can do what we like with the actual player entity while the cutscene is running, this will stop us having to use black screen fadeouts to hide us doing stuff after the cutscene has finished..
PROC PROCESS_MOCAP_CUTSCENE(INT iCutsceneToUse, INT iCutsceneTeam, FMMC_CUTSCENE_TYPE eCutType)

	STOP_LOCAL_PLAYER_FROM_GETTING_SHOT_THIS_FRAME()
	
	BLOCK_INPUTS_FOR_CUTSCENE_PROCEDURES()
	
	IF GET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS() = MOCAPPROG_INIT
		PRINTLN("[Cutscene_Mocap] MANAGE_MID_MISSION_CUTSCENE - bIsMocap = TRUE" )
	ENDIF
	
	FLOAT fWarpRadius = GET_CUTSCENE_PLAYER_WARP_RANGE(iCutsceneTouse, eCutType)
				
	INT iPlayerIndex = -1  
	IF ARE_TEAM_CUTSCENE_PLAYERS_SELECTED(iCutsceneTeam)
		iPlayerIndex = GET_LOCAL_PLAYER_CUTSCENE_INDEX(iCutsceneTeam)
		PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE - SET iPlayerIndex: ", iPlayerIndex, " from GET_LOCAL_PLAYER_CUTSCENE_INDEX")
	ELSE
		PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE - NOT SET iPlayerIndex: ", iPlayerIndex, " from GET_LOCAL_PLAYER_CUTSCENE_INDEX - due to ARE_TEAM_CUTSCENE_PLAYERS_SELECTED = FALSE")
	ENDIF
	
	// fallback
	IF iPlayerIndex = -1
		iPlayerIndex = GET_PLAYER_FALLBACK_CUTSCENE_INDEX()
		PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE - iPlayerIndex: ", iPlayerIndex, " from GET_PLAYER_FALLBACK_CUTSCENE_INDEX")
		
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_FALLBACK_CUTSCENE_PLAYER_ID, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "Cutscene using fallback player index!")
		#ENDIF
	ENDIF

	// clamp
	IF iPlayerIndex < 0 OR iPlayerIndex >= FMMC_MAX_CUTSCENE_PLAYERS
		ASSERTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE - iPlayerIndex: ", iPlayerIndex, " is out of range 0 - FMMC_MAX_CUTSCENE_PLAYERS(", FMMC_MAX_CUTSCENE_PLAYERS, "), Clamping")
		iPlayerIndex = CLAMP_INT(iPlayerIndex, 0, FMMC_MAX_CUTSCENE_PLAYERS-1)
	ENDIF
	
	VECTOR vStartPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vPlayerStartPos[iPlayerIndex]
	FLOAT fStartPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].fPlayerStartHeading[iPlayerIndex]
	IF IS_VECTOR_ZERO(vStartPos)
		vStartPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vPlayerStartPos[0]
		fStartPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].fPlayerStartHeading[0]
		PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE - iPlayerIndex: ", iPlayerIndex, " vStartPos: ", vStartPos, " fallback to idx 0 used!")
	ELSE
		PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE - iPlayerIndex: ", iPlayerIndex, " vStartPos: ", vStartPos, " used")
	ENDIF
	
	VECTOR vEndPos
	FLOAT  fEndPos
	
	//Should override end pos with an alternative location?
	IF SHOULD_USE_ALT_END_POS(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].eUseAltEndPos)
		//Alt End Pos
		vEndPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vPlayerAltEndPos[iPlayerIndex]
		fEndPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].fPlayerAltEndHeading[iPlayerIndex]	
			
		IF IS_VECTOR_ZERO(vEndPos)
			vEndPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vPlayerAltEndPos[0]
			fEndPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].fPlayerAltEndHeading[0]
			PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE - iPlayerIndex: ", iPlayerIndex, " vAltEndPos: ", vEndPos, " fallback to idx 0 used!")
		ELSE
			PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE - iPlayerIndex: ", iPlayerIndex, " vAltEndPos: ", vEndPos, " used")
		ENDIF	
	ELSE
		//End Pos
		vEndPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vPlayerEndPos[iPlayerIndex]
		fEndPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].fPlayerEndHeading[iPlayerIndex]	
			
		IF IS_VECTOR_ZERO(vEndPos)
			vEndPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vPlayerEndPos[0]
			fEndPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].fPlayerEndHeading[0]
			PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE - iPlayerIndex: ", iPlayerIndex, " vEndPos: ", vEndPos, " fallback to idx 0 used!")
		ELSE
			PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE - iPlayerIndex: ", iPlayerIndex, " vEndPos: ", vEndPos, " used")
		ENDIF
	ENDIF
	
	VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)	
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REACHED_CUT_PROCESS_SHOTS)
		IF NOT IS_VECTOR_ZERO(vEndPos)
		AND VDIST2(vEndPos, vPlayerPos) <= POW(200.0, 2.0)
			PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE - ADD_SCRIPTED_COVER_AREA - vEndPos is being used: ", vEndPos)
			ADD_SCRIPTED_COVER_AREA(vEndPos, 15.0)
		ENDIF
	ENDIF		
	
	IF eMocapManageCutsceneProgress > MOCAPPROG_INIT
		BOOL bBail		
		
		INT iLengthSafetyTimer = ciSAFETY_TIMER_MOPCAP_CUTSCENES_LENGTH		
		IF IS_CUTSCENE_PLAYING()
		AND GET_CUTSCENE_END_TIME() > 0
			iMocapCutsceneSafetyLengthTime = GET_CUTSCENE_END_TIME()
		ENDIF		
		iLengthSafetyTimer += iMocapCutsceneSafetyLengthTime
		
		IF HAS_NET_TIMER_STARTED(tdSafetyTimerCutsceneLengthLimit)
		AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdSafetyTimerCutsceneLengthLimit, iLengthSafetyTimer)
			PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE SAFETY TIMER HAS BEEN EXCEEDED iLengthSafetyTimer, ", iLengthSafetyTimer, ", SENDING TO CLEANUP.")						
			#IF IS_DEBUG_BUILD
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_MOCAP_CUTSCENE_FORCED_TO_CLEANUP_FROM_STUCK, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "Mocap Cutscene forced to cleanup stuck # ms. Cut Length is # ", iLengthSafetyTimer, GET_CUTSCENE_END_TIME())
			#ENDIF
			bBail = TRUE
		ENDIF				
		IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_CUTSCENE_BAIL_FROM_MISSION_FAILURE)			
			PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE - LBOOL25_CUTSCENE_BAIL_FROM_MISSION_FAILURE is set, bailing from the Mocap Cutscene!!")
			bBail = TRUE
		ENDIF
		IF bBail
			PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE - Sending to MOCAPPROG_CLEANUP!")
			SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_CLEANUP)
			
			IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_CUTSCENE_BAIL_FROM_MISSION_FAILURE)
				PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE - LBOOL25_CUTSCENE_BAIL_FROM_MISSION_FAILURE is set, Calling TOGGLE_PAUSED_RENDERPHASES")
				TOGGLE_PAUSED_RENDERPHASES(FALSE)
			ENDIF
			
			IF IS_CUTSCENE_PLAYING()
				STOP_CUTSCENE_IMMEDIATELY()
			ENDIF
			REMOVE_CUTSCENE()
		ENDIF
	ENDIF
	
	eCutsceneTypePlaying = eCutType
	iCutsceneIndexPlaying = iCutsceneToUse
	
	SWITCH eMocapManageCutsceneProgress
		
		CASE MOCAPPROG_INIT
		
			TURN_OFF_CUTSCENE_VEHICLE_RADIO()
			
			PRINTLN("[Cutscenes] Starting a mocap cutscene." )	
			
			PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE CURRENT NETWORK TIMER: ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
			PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE GET_NET_TIMER_DIFF", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_ServerBD.tdCutsceneStartTime[iScriptedCutsceneTeam],FALSE,TRUE),"[RCC MISSION] is host: ", bIsLocalPlayerHost)
			
			TOGGLE_PLAYER_DAMAGE_OVERLAY( FALSE )
			
			IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
				IF NOT bLocalPlayerOK
				OR IS_PLAYER_RESPAWNING(LocalPlayer)
					FMMC_NETWORK_RESURRECT_LOCAL_PLAYER(GET_ENTITY_COORDS( LocalPlayerPed, FALSE ),
													GET_ENTITY_HEADING( LocalPlayerPed ),
													10,
													FALSE, NOT IS_INTRO_CUTSCENE_RULE_ACTIVE())
				ENDIF
			ENDIF

			SET_PLAYER_INVINCIBLE( LocalPlayer, TRUE )
			
			SET_ENTITY_PROOFS( LocalPlayerPed, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, TRUE )
			
			//Store the last used vehicle for post warp
			viLastVehicleIndexBeforeCutscene = GET_PLAYERS_LAST_VEHICLE()
			
			g_bInMissionControllerCutscene = TRUE
			
			CLEAR_BIT(MC_playerBD[iLocaLPart].iClientBitSet4, PBBOOL4_SYNC_PLAYERS_FOR_END_OF_CUTSCENE)
			
			SET_IM_WATCHING_A_MOCAP()
			
			SET_BIT( iLocalBoolCheck5, LBOOL5_PED_INVOLVED_IN_CUTSCENE )
			
			SET_BIT( MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_STARTED_CUTSCENE )
						
			IF bIsAnySpectator
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_I_AM_SPECTATOR_WATCHING_CUTSCENE)
				PRINTLN("[Cutscene_Mocap] - PBBOOL2_I_AM_SPECTATOR_WATCHING_CUTSCENE = TRUE")
			ENDIF
			
			IF MC_playerBD[iPartToUse].iTeam = iScriptedCutsceneTeam
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_REQUEST_CUTSCENE_PLAYERS)
				BROADCAST_CUTSCENE_PLAYER_REQUEST(MC_playerBD[iPartToUse].iteam, FMMCCUT_MOCAP)
				PRINTLN("[Cutscene_Mocap] [LM][url:bugstar:5609784] - Setting PBBOOL_REQUEST_CUTSCENE_PLAYERS (1)")
			ENDIF
			
			// These things only happen if we DON'T skip the cutscene
			IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_skipped_to_mid_mission_cutscene_stage_5)
				IF Is_MP_Objective_Text_On_Display()
					Clear_Any_Objective_Text()
				ENDIF
				
				KILL_FACE_TO_FACE_CONVERSATION()
			ENDIF
			
			REINIT_NET_TIMER(tdSafetyTimerCutsceneLengthLimit)
			
			IF SHOULD_MOCAP_SEND_LOCAL_PLAYER_DATA(sMocapCutscene)
				SEND_WEAPON_DATA_TO_REMOTE_PLAYERS(GET_WEAPON_TYPE_FOR_MOCAP_SEND_LOCAL_PLAYER_DATA(sMocapCutscene))
			ENDIF
			
			SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_WAIT_PLAYER_SELECTION)
		BREAK
		
		CASE MOCAPPROG_WAIT_PLAYER_SELECTION			
			
			IF ARE_TEAM_CUTSCENE_PLAYERS_SELECTED(iCutsceneTeam)
		
				IF NOT IS_VECTOR_ZERO(vStartPos)
					PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE START POS VALID <<", vStartPos.X, ", ", vStartPos.Y, ", ",vStartPos.Z, ">>")
					
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( PlayerPedToUse, vStartPos ) > fWarpRadius
						IF IS_BIT_SET( g_FMMC_STRUCT.sMocapCutsceneData[ iCutsceneToUse ].iCutsceneBitset, ci_CSBS_SkipIfTooFar )
							SET_BIT(iLocalBoolCheck8, LBOOL8_skipped_to_mid_mission_cutscene_stage_5)
							PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE - Skipping because I'm too far")
							SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_CLEANUP)
						ELSE
							IF NOT bIsAnySpectator
								SET_BIT(iLocalBoolCheck2,LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP)
								PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE - NOT bIsAnySpectator")
								SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_FADEOUT)
							ELSE
								PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE bIsAnySpectator = TRUE")
								SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_INTRO)
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PlayerPedToUse, vStartPos)(", GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PlayerPedToUse, vStartPos), ") <= fWarpRadius( ", fWarpRadius, ")")
						SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_INTRO)
					ENDIF
					
				ELSE
					PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE Start pos NOT valid, not doing distance checks - running cutscene anyway")
					SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_INTRO)
				ENDIF
			
			ENDIF
		
		BREAK

		CASE MOCAPPROG_FADEOUT
		
			PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE Doing warp - Mocap stage: ", GET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS_NAME(GET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS()))
			PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE CASE 1 CURRENT NETWORK TIMER: ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
			PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE CASE 1 GET_NET_TIMER_DIFF", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_ServerBD.tdCutsceneStartTime[iScriptedCutsceneTeam],FALSE,TRUE),"[RCC MISSION] is host: ", bIsLocalPlayerHost)
			
			IF NOT IS_INTRO_CUTSCENE_RULE_ACTIVE()
				IF NOT IS_SCREEN_FADED_OUT()
				AND NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(2000)
				ENDIF
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()
			OR IS_INTRO_CUTSCENE_RULE_ACTIVE()
				REMOVE_PLAYER_FROM_VEHICLE_AT_CUTSCENE_START(iCutsceneToUse, FALSE)
				
				// 2161860, make the warping player invisible 
				IF IS_ENTITY_VISIBLE( LocalPlayerPed )
					SET_ENTITY_VISIBLE( LocalPlayerPed, FALSE )
				ENDIF

				IF NOT BUSYSPINNER_IS_ON()
				AND NOT IS_INTRO_CUTSCENE_RULE_ACTIVE()				
					BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("FMMC_PLYLOAD") 
					END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))					
				ENDIF

				IF NET_WARP_TO_COORD(vStartPos, fStartPos, !SHOULD_CUTSCENE_REMOVE_PLAYER_FROM_VEHICLE_AT_START( iCutsceneToUse, FALSE ), FALSE)
									
					IF IS_BIT_SET( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED )	
					OR IS_BIT_SET(iLocalBoolCheck31, LBOOL31_NOT_STREAMING_CUTSCENE)
						IF BUSYSPINNER_IS_ON()
							BUSYSPINNER_OFF()
						ENDIF
						PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE LBOOL2_CUTSCENE_STREAMED")						
						SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_INTRO)
					ELSE
						PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE waiting for streaming: ", sMocapCutscene)
					ENDIF
				ENDIF
			ENDIF	
			
		BREAK		
			
		CASE MOCAPPROG_INTRO
			
			IF IS_INTRO_CUTSCENE_RULE_ACTIVE()

				IF NOT IS_SKYSWOOP_AT_GROUND()
					PRINTLN("[Cutscene_Mocap] PROCESS_MOCAP_CUTSCENE - Dont progress til skyswoop is down - MOCAPPROG_INTRO")
					EXIT
				ENDIF
				
			ENDIF
			
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				NEW_LOAD_SCENE_STOP()
			ENDIF

			//Do specific mocap checks per cutscene
			DO_PRE_MOCAP_CHECKS(iCutsceneToUse)
			
			CLEAR_BIT(iLocalBoolCheck2,LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP)
			SET_BIT(iLocalBoolCheck2,LBOOL2_CUTSCENE_WARP_DONE)
						
			// This does nothing but waste a frame at the moment, reintroduce if needed with a check
			//SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_APARTMENT)
			SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_PROCESS_SHOTS)
			
		BREAK
		
		CASE MOCAPPROG_APARTMENT

			SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_PROCESS_SHOTS)

		BREAK
		
		CASE MOCAPPROG_PROCESS_SHOTS
			
			PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE CASE 3 CURRENT NETWORK TIMER: ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
			PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE CASE 3 GET_NET_TIMER_DIFF", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_ServerBD.tdCutsceneStartTime[iScriptedCutsceneTeam],FALSE,TRUE),"[Cutscenes] is host: ", bIsLocalPlayerHost)
			
			BOOL bWarpAtEnd, bFadeAtEnd
			IF NOT IS_VECTOR_ZERO(vEndPos)
			AND NOT bIsAnySpectator				
				IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_WarpPlayersEnd)
					bWarpAtEnd = TRUE
					PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE - bWarpAtEnd = TRUE ci_CSBS_WarpPlayersEnd")
				ENDIF 
				
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed,vEndPos) > fWarpRadius
					bWarpAtEnd = TRUE
					bFadeAtEnd = TRUE
					PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE - bWarpAtEnd = TRUE (Distance...)")
				ENDIF
						
				// There are certain conditions that require us to do a "WARPEND". Since there is no creator option we should use the conditions we know that will require it. Such as Rolling Start / Waiting for being in Vehicle.
				IF SHOULD_PLAYER_DO_ROLLING_START_FOR_CUTSCENE(FMMCCUT_MOCAP, iCutsceneToUse)
				AND NOT bIsAnySpectator
					bWarpAtEnd = TRUE
					bFadeAtEnd = TRUE
					PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE - bWarpAtEnd = TRUE (Rolling Start...)")
				ENDIF
								
				IF SHOULD_WARP_PLAYER_TO_END_POSITION_DURING_CUTSCENE(iCutsceneToUse, eCutType)				
					bWarpAtEnd = FALSE
					bFadeAtEnd = FALSE
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[ iCutsceneToUse ].iCutsceneBitset2, ci_CSBS2_EndCutsceneRappelTask)
			AND NOT bIsAnySpectator
				bFadeAtEnd = TRUE
				PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE - bFadeAtEnd = TRUE (End in Rappel...)")
			ENDIF
			
			IF DOES_ENTITY_EXIST(ClonePlayerForCutscene) 
			AND IS_ENTITY_VISIBLE(ClonePlayerForCutscene)
				NETWORK_FADE_OUT_ENTITY(ClonePlayerForCutscene, FALSE, TRUE)
			ENDIF
						
			IF RUN_MOCAP_CUTSCENE(sMocapCutscene, FMMCCUT_MOCAP, FALSE, 
				vStartPos, fStartPos, vEndPos, fEndPos,
				NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HIDE_MOCAP_PLAYERS), DEFAULT, bFadeAtEnd, iPlayerIndex)
			
				IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HIDE_MOCAP_PLAYERS)
					LOAD_RANDOM_RE_ENTRY_INTERACTION_ANIM() // neilf. so the re-entry anims will be loaded by the time gameplay resumes.
				ENDIF
				
				PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE RUN_MOCAP_CUTSCENE = TRUE.")
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_EquipOutfitForNextRule)
					SET_BIT(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
					PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE - Setting LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE")
				ENDIF
				
				PROCESS_CUTSCENE_LIFT_FAKE_ARRIVAL_SFX(iCutsceneToUse)
				
				IF bWarpAtEnd					
					PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE Warping the local ped after a cutscene to their end location")
					SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_WARPEND)
				ELSE
				
					IF NOT bIsAnySpectator
						PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE IS_LOCAL_PLAYER_ANY_SPECTATOR = false.")
						DO_POST_CUTSCENE_COMMON_PLAYER_EXIT(iCutsceneToUse, FMMCCUT_MOCAP, iPlayerIndex)
					ENDIF
				
					PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE Cleaning up. We're too close to the warp radius or MPH_PRI_FIN_MCS2 is being used.")
					SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_ENDING_PROCEDURES)
				ENDIF	
			ELSE

				//-- If we're changing outfits at the end of the cutscene, attenpt to preload the outfit (bit rough!)
				IF NOT IS_PLAYER_SPECTATING(LocalPlayer)
				AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
					IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_EquipOutfitForNextRule)
						IF NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_PRELOAD_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
							
							INT iRuleOutfit
							iRuleOutfit = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iOutfit[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]
							IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iOutfitVariations[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] > 0
								iRuleOutfit += NATIVE_TO_INT(LocalPlayer) % g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iOutfitVariations[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]
							ENDIF
						//	BOOL bOutfitSet = FALSE
							

							IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] + 1 < FMMC_MAX_RULES
								IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iOutfit[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] + 1] != -1
									iRuleOutfit =  g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iOutfit[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] + 1]
									
									IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iOutfitVariations[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] + 1] > 0
										iRuleOutfit += NATIVE_TO_INT(LocalPlayer) % g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iOutfitVariations[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] + 1]
									ENDIF
								ENDIF
							ENDIF
							
							
							sApplyOutfitData.pedID 		= LocalPlayerPed
							sApplyOutfitData.eOutfit 	= int_to_enum(MP_OUTFIT_ENUM,iRuleOutfit)
							
							IF iRuleOutfit > -1
								
								if PRELOAD_MP_OUTFIT(sApplyOutfitData,g_sOutfitsData)
									PRINTLN("[Cutscenes] [PROCESS_MOCAP_CUTSCENE] [DSW] Preloaded! Set LBOOL28_PRELOAD_OUTFIT_CHANGE_AT_END_OF_CUTSCENE")
									SET_BIT(iLocalBoolCheck28, LBOOL28_PRELOAD_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
								ELSE
									PRINTLN("[Cutscenes] [PROCESS_MOCAP_CUTSCENE] [DSW] Trying to preload outfit SET_PED_MP_OUTFIT... ePreloadStage: ", sApplyOutfitData.ePreloadStage, "	iRuleOutfit: ", iRuleOutfit)
								ENDIF
							ENDIF
						ENDIF	
					ENDIF 
				ENDIF
			ENDIF
			
			// Initialise the grace period timer after a mocap has finished
			REINIT_NET_TIMER(stGracePeriodAfterCutscene)
			
		BREAK
		
		CASE MOCAPPROG_WARPEND
			// Never fade out if the celebration screen is up
			IF IS_BIT_SET( iCelebrationBitSet, ciCELEBRATION_BIT_SET_bStartedCelebrationBeforeCutsceneEnded )
			OR IS_BIT_SET( iLocalBoolCheck8, LBOOL8_SHOULD_CELEBRATION_BE_SHOWN_PREMATURELY )	
			OR GET_TOGGLE_PAUSED_RENDERPHASES_STATUS() = FALSE // B*4151222
				PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE - MOCAPPROG_WARPEND - Skipping Fade out/in - Paused Renderphase or Celeb started.")
				SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_ENDING_PROCEDURES)
				EXIT
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_WarpPlayersEnd) 
			AND IS_BIT_SET(iLocalBoolCheck30, LBOOL30_WARPED_FROM_EXIT_STATE)
				PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE - MOCAPPROG_WARPEND - Skipping Fade out/in - Already Warped in exit state.")
				SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_ENDING_PROCEDURES)
				EXIT
			ENDIF
			
			IF NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
				PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE - MOCAPPROG_WARPEND - Doing Fadeout.")
				DO_SCREEN_FADE_OUT(MOCAP_END_FADE_DURATION)
			ELSE
				IF IS_SCREEN_FADED_OUT()
					IF bIsAnySpectator
						SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_ENDING_PROCEDURES)
					ELSE
						
						IF IS_PED_IN_ANY_VEHICLE(localPlayerPed, TRUE)		
							VEHICLE_INDEX vehIndex
							vehIndex = GET_VEHICLE_PED_IS_IN(localPlayerPed, TRUE)
							IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(vehIndex)
								PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE - MOCAPPROG_WARPEND - IN VEHICLE - No net control of entity")
								NETWORK_REQUEST_CONTROL_OF_ENTITY(vehIndex)
								EXIT
							ELSE
								PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE - MOCAPPROG_WARPEND - IN VEHICLE - We now have control!")
							ENDIF
						ENDIF
						
						// B*4251737 - added to hold up the net warp until the Enter Vehicle task has finished
						// See Paulius's comments in B*4212309
						SCRIPTTASKSTATUS taskStatus
						taskStatus = GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_ENTER_VEHICLE)
						IF taskStatus != WAITING_TO_START_TASK
						AND taskStatus != PERFORMING_TASK
						
							IF NET_WARP_TO_COORD(vEndPos, fEndPos, !SHOULD_CUTSCENE_REMOVE_PLAYER_FROM_VEHICLE_AT_START(iCutsceneToUse, FALSE), FALSE )								
								DO_POST_CUTSCENE_COMMON_PLAYER_EXIT(iCutsceneToUse, FMMCCUT_MOCAP, iPlayerIndex)
								SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_ENDING_PROCEDURES)
							ENDIF
							
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF	
			
		BREAK
		
		CASE MOCAPPROG_ENDING_PROCEDURES
			PROCESS_CUTSCENE_FINAL_SPECIAL_CASE(sMocapCutscene)
			
			IF NOT IS_BIT_SET(iLocalBoolCheck35, LBOOL35_CLEARED_AREA_AT_THE_END_OF_CUTSCENE)
				IF eCutType = FMMCCUT_ENDMOCAP
					IF IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet4, ci_CSBS4_CutsceneClearAreaAtEndOnly)
					OR IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet4, ci_CSBS4_CutsceneClearAreaAtStartAndEnd)	
						CLEAR_CUTSCENE_AREA(g_FMMC_STRUCT.sEndMocapSceneData.fClearRadius, g_FMMC_STRUCT.sEndMocapSceneData.vClearCoOrds, g_FMMC_STRUCT.sEndMocapSceneData.vReplacementArea, TRUE, IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet3, ci_CSBS3_ClearPersonalVehicles))
					ENDIF
				ELIF iCutsceneToUse != -1
					IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet4, ci_CSBS4_CutsceneClearAreaAtEndOnly)
					OR IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet4, ci_CSBS4_CutsceneClearAreaAtStartAndEnd)	
						CLEAR_CUTSCENE_AREA(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].fClearRadius, g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vClearCoOrds, g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vReplacementArea, TRUE, IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_ClearPersonalVehicles))
					ENDIF
				ENDIF				
				SET_BIT(iLocalBoolCheck35, LBOOL35_CLEARED_AREA_AT_THE_END_OF_CUTSCENE)
			ENDIF
			
			IF NOT bIsAnySpectator			
				IF NOT PROCESS_WARP_SELECTED_ENTITIES_AFTER_CUTSCENE(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntityWarpData)					
					PRINTLN("[Cutscenes] - PROCESS_MOCAP_CUTSCENE - Waiting for PROCESS_WARP_SELECTED_ENTITIES_AFTER_CUTSCENE to be finished.")
					EXIT
				ENDIF
			
				IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_EndCutsceneInCover)
					PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE - MOCAPPROG_ENDING_PROCEDURES - ci_CSBS2_EndCutsceneInCover")					
					SET_PLAYER_INTO_COVER_AFTER_CUTSCENE(GET_PLAYERS_FACE_DIRECTION_IN_COVER(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet3, iPlayerIndex))
				ENDIF
			ENDIF
			
			// Fly Cam After Warp
			IF eCutType = FMMCCUT_MOCAP
			AND iCutsceneToUse != -1
			AND IS_BIT_SET(g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_UseFlyCamAfterCutscene_AfterWarp)
				IF NOT PROCESS_CAMERA_FLY_DOWN_AFTER_CUTSCENE(eCutType, iCutsceneToUse, vEndPos, fEndPos)
					PRINTLN("[Cutscenes] - PROCESS_MOCAP_CUTSCENE - Waiting for PROCESS_CAMERA_FLY_DOWN_AFTER_CUTSCENE to finish.")
					EXIT
				ENDIF
			ENDIF			
					
			IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet4, ci_CSBS4_PlayCutsceneSFX_TurnOnInteriorLights_CutEnd)
				IF eLocalArtificialLightsState != eARTIFICIAL_LIGHTS_STATE__ON
					TURN_ON_ARTIFICIAL_LIGHTS_LOCALLY(FALSE)
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet4, ci_CSBS4_PlayCutsceneSFX_TurnOffInteriorLights_CutEnd)
				IF eLocalArtificialLightsState != eARTIFICIAL_LIGHTS_STATE__OFF
					TURN_OFF_ARTIFICIAL_LIGHTS_LOCALLY(FALSE)
				ENDIF
			ENDIF			
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_EndCutsceneRappelTask)
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_RUNNING_WALL_RAPPEL_NOT_ON_RAPPEL_RULE)
				PRINTLN("[WallRappel] MOCAPPROG_ENDING_PROCEDURES - Setting PBBOOL4_RUNNING_WALL_RAPPEL_NOT_ON_RAPPEL_RULE")
			ENDIF
			
			IF IS_BIT_SET(g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_SyncAllPlayersAtEnd)
				IF NOT IS_BIT_SET(MC_playerBD[iLocaLPart].iClientBitSet4, PBBOOL4_SYNC_PLAYERS_FOR_END_OF_CUTSCENE)
					PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE - MOCAPPROG_ENDING_PROCEDURES - setting PBBOOL4_SYNC_PLAYERS_FOR_END_OF_CUTSCENE")
					SET_BIT(MC_playerBD[iLocaLPart].iClientBitSet4, PBBOOL4_SYNC_PLAYERS_FOR_END_OF_CUTSCENE)
				ENDIF
				
				IF NOT ARE_ALL_PLAYERS_SYNCED_FOR_END_OF_CUTSCENE()
					PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE - MOCAPPROG_ENDING_PROCEDURES - iCutsceneToUse: ", iCutsceneToUse, ", exiting syncing players")
					EXIT
				ENDIF
			ENDIF
		
			// Intro Cutscenes have to be over a certain length otherwise they finish before getting to game state running, and cannot register it as started. 
			// It's too entangled in objective logic and staggered player loops to fix within the intro cutscene function.
			IF ( IS_INTRO_CUTSCENE_RULE_ACTIVE() #IF IS_DEBUG_BUILD OR bDebugForceCutsceneMinimumShotsProcessing #ENDIF )
			AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdSafetyTimerCutsceneLengthLimit, ciMINIMAL_CUTSCENE_LENGTH_FOR_OBJECTIVE_PROGRESSION)
				PRINTLN("[LM][Cutscene_Scripted] - MOCAPPROG_ENDING_PROCEDURES - CUTSCENE IS TOO SHORT.")
				#IF IS_DEBUG_BUILD
				ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_CUTSCENE_IS_TOO_SHORT_FOR_INTRO_SCENE, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "Cutscene Length is too short. Add Interp Time or delays in creator!")
				#ENDIF
				EXIT
			ENDIF
			
			SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_CLEANUP)
		BREAK
		
		CASE MOCAPPROG_CLEANUP
			
			RESET_NET_TIMER(tdEnterCoverEndingCutsceneTimeOut)
			
			IF NOT IS_CUTSCENE_PLAYING()
				
				RENDER_SCRIPT_CAMS(FALSE, FALSE, DEFAULT, DEFAULT, TRUE)
				
				IF DOES_CAM_EXIST(cutscenecam)
					PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE - Destroying Cam.")
					SET_CAM_ACTIVE(cutscenecam, FALSE)
					DESTROY_CAM(cutscenecam)
				ENDIF
								
				IF CAN_CUTSCENE_RESET_GAMEPLAY_CAMERA_PITCH_OR_HEADING(eCutType, iCutsceneToUse)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()									
				ENDIF
				
				INVALIDATE_CINEMATIC_VEHICLE_IDLE_MODE()
				
				INVALIDATE_IDLE_CAM()
				
				CLEANUP_CUTSCENE_SPECIAL_CASE_LOGIC(sMocapCutscene)
				
				RELEASE_ASSETS_FOR_MOCAP(sMocapCutscene)
								
				SET_PED_CONFIG_FLAG(LocalPlayerPed,PCF_PreventAutoShuffleToDriversSeat,FALSE)
				
				PRINTLN("[Cutscenes] - PROCESS_MOCAP_CUTSCENE - Set PCF_PreventAutoShuffleToDriversSeat, false")
				
				TOGGLE_PLAYER_DAMAGE_OVERLAY(TRUE)
				
				SET_PLAYER_INVINCIBLE( LocalPlayer, FALSE )
				
				RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
			
				PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE finished ")
				
				DO_POST_MOCAP_CHECKS()
				
				SET_BIT(MC_playerBD[iLocalPart].iCutsceneFinishedBitset, g_iFMMCScriptedCutscenePlaying)
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_FINISHED_CUTSCENE)	
				CLEAR_BIT( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE)
				CLEAR_BIT(iLocalBoolCheck31, LBOOL31_PRIZED_VEHICLE_SHOULD_HIDE)
				
				PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE Setting cutscene as finished: ", g_iFMMCScriptedCutscenePlaying)
				CLEAR_BIT(iLocalBoolCheck35, LBOOL35_CLEARED_AREA_AT_THE_END_OF_CUTSCENE)
				CLEAR_BIT(iLocalBoolCheck30, LBOOL30_WARPED_FROM_EXIT_STATE)
				CLEAR_BIT(iLocalBoolCheck8, LBOOL8_PLAYER_EXIT_STATE_CALLED)	
				CLEAR_BIT(iLocalBoolCheck5,LBOOL5_PED_INVOLVED_IN_CUTSCENE)
				CLEAR_BIT(iLocalBoolCheck5,LBOOL5_MID_MISSION_MOCAP)
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_REQUEST_CUTSCENE_PLAYERS)					
				CLEAR_BIT( iLocalBoolCheck7, LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE )
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REACHED_CUT_PROCESS_SHOTS)
				RESET_NET_TIMER(tdCutsceneWaitForEntitiesTimer)
				iWarpCutsceneEntityBS = 0
				g_bInMissionControllerCutscene = FALSE
				
				IF eCutType != FMMCCUT_ENDMOCAP
				AND SHOULD_FADE_MOCAP_IN_AT_END(iCutsceneToUse)
					IF IS_SCREEN_FADED_OUT()
						PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE SHOULD_FADE_MOCAP_IN_AT_END = true")
						DO_SCREEN_FADE_IN(MOCAP_END_FADE_DURATION)
					ENDIF
				ELSE
					PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE Not fading in at end of the mocap.")
				ENDIF

				sMocapCutscene = NULL
				PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE SETTING sMocapCutscene to NULL - cutscene finished 1")
				
				NETWORK_SET_VOICE_ACTIVE(true)
				
				REMOVE_CUTSCENE()
				
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					VEHICLE_INDEX vehPlayer
					vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
					IF IS_VEHICLE_DRIVEABLE(vehPlayer)
					AND NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
					AND IS_ENTITY_A_MISSION_ENTITY(vehPlayer)
						SET_VEHICLE_LIMIT_SPEED_WHEN_PLAYER_INACTIVE(vehPlayer, TRUE)
						SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE(vehPlayer, TRUE)
					ENDIF
				ENDIF				
				
				DISABLE_SPECTATOR_FILTER_OPTION( FALSE )
				
				INT i 
				FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
					IF mnCutscenePlayerModels[i] != DUMMY_MODEL_FOR_SCRIPT											
						SET_MODEL_AS_NO_LONGER_NEEDED(mnCutscenePlayerModels[i])
					ENDIF
				ENDFOR
				
				CLEAR_BIT( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED )
				
				SET_MOCAP_STREAMING_STAGE(MOCAP_STREAMING_STAGE_INIT)
				iScriptedCutsceneProgress = 0
				iMocapCutsceneProgress = 0
				
				SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_INIT)
				SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_0)
				
				RESET_NET_TIMER(mocapFailsafe)
				
				vPosNearestElevatorDoor = <<0.0, 0.0, 0.0>>
				bsSpecialCaseCutscene = 0
				iMocapCutsceneSafetyLengthTime = 0
				iScriptedCutsceneTeam = -1
				g_iFMMCScriptedCutscenePlaying = -1
				iCutsceneStreamingTeam = -1
				RESET_NET_TIMER(tdCutsceneMidpoint)				
				RESET_NET_TIMER(tdSafetyTimerCutsceneLengthLimit)
								
				CLEANUP_CUTSCENE_BINK_AND_RENDERTARGET()
								
				CLEAR_BIT(iLocalBoolCheck7, LBOOL7_HIDE_MOCAP_PLAYERS)
				CLEAR_BIT(iLocalBoolCheck2,LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP)
				CLEAR_BIT(iLocalBoolCheck2,LBOOL2_CUTSCENE_WARP_DONE)
				CLEAR_BIT(iLocalBoolCheck10, LBOOL10_LOCAL_PLAYER_IN_CUTSCENE)
				CLEAR_BIT(iLocalBoolCheck29, LBOOL29_KEEP_TASKS_AFTER_CUTSCENE)
				CLEAR_BIT(iLocalBoolCheck31, LBOOL31_CUTSCENE_RE_REQUESTED_AFTER_GAMEPLAY)
				MC_STOP_NET_TIMER_WITH_TIMESTAMP(iCutsceneWaitForEntitiesSafetyTimeStamp)
				
				IF bIsLocalPlayerHost
					#IF IS_DEBUG_BUILD
					IF IS_BIT_SET(MC_serverBD.iServerBitset8, SBBOOL8_PREVENT_CUTSCENE_PRIMARY_PLAYER_FROM_BEING_ADDED)
						PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE - Clearing SBBOOL8_PREVENT_CUTSCENE_PRIMARY_PLAYER_FROM_BEING_ADDED")
					ENDIF
					#ENDIF
					CLEAR_BIT(MC_serverBD.iServerBitset8, SBBOOL8_PREVENT_CUTSCENE_PRIMARY_PLAYER_FROM_BEING_ADDED)
				ENDIF
				
				// Force update godtext after a cutscene 
				REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_MOCAP_CUTSCENE - Cutscene over" #ENDIF )
				
				SET_DISABLE_RANK_UP_MESSAGE(FALSE)					
				
				iPedFakePlayersBitset = 0
				
				clear_bit(iLocalBoolCheck8, LBOOL8_skipped_to_mid_mission_cutscene_stage_5)					
				
				IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
					SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(FALSE)
					PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE setting SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY false - 1")
				ENDIF
				
				CLEAR_IM_WATCHING_A_MOCAP()
				CLEANUP_MP_CUTSCENE()
				
				NETWORK_SET_TASK_CUTSCENE_INSCOPE_MULTIPLER(1.0)
				
				sMocapCutscene = null
				PRINTLN("[Cutscenes] PROCESS_MOCAP_CUTSCENE SETTING sMocapCutscene to NULL - cutscene finished 2")
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Scripted Cutscenes   --------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing of Scripted Cutscenes. 									 							 								 	 ---------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_PLAYER_IN_SCRIPTED_CUTSCENE(INT iCutsceneToUse)
	IF DOES_CUTSCENE_HAVE_REGISTERED_ENTITIES(iCutsceneToUse, FMMCCUT_SCRIPTED)
	AND NOT SHOULD_SCRIPTED_CUTSCENE_SKIP_ENTITY_REGISTRATION(iCutsceneToUse, sCutsceneSyncSceneDataGameplay.eType)
		PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE DOES_CUTSCENE_HAVE_REGISTERED_ENTITIES TRUE ")
		REGISTER_CUTSCENE_ENTITIES(iCutsceneToUse, TRUE, FMMCCUT_SCRIPTED)
		IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Hide_Your_Player)						
			SET_ENTITY_VISIBLE_IN_CUTSCENE(LocalPlayerPed, FALSE)
			PRINTLN("[MJM] PROCESS_SCRIPTED_CUTSCENE IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Hide_Your_Player)")
		ELSE
			SET_ENTITY_VISIBLE_IN_CUTSCENE(LocalPlayerPed, TRUE, IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene))
			PRINTLN("[MJM] PROCESS_SCRIPTED_CUTSCENE NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Hide_Your_Player)")
		ENDIF
		START_MP_CUTSCENE(TRUE, TRUE)
		SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
	ELSE
		PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE PROCESS_SCRIPTED_CUTSCENE DOES_CUTSCENE_HAVE_REGISTERED_ENTITIES FALSE ")
		START_MP_CUTSCENE(FALSE)
	ENDIF
ENDPROC

PROC SET_PLAYER_OUT_OF_SCRIPTED_CUTSCENE_FOR_SYNC_SCENE()
	PRINTLN("[Cutscene_Scripted][ScriptedAnimSyncScene] PROCESS_SCRIPTED_CUTSCENE Calling NETWORK_SET_IN_MP_CUTSCENE and SET_NETWORK_CUTSCENE_ENTITIES with False")
	IF NETWORK_IS_PLAYER_IN_MP_CUTSCENE(LocalPlayer)
		SET_NETWORK_CUTSCENE_ENTITIES(FALSE)
		NETWORK_SET_IN_MP_CUTSCENE(FALSE)		
	ENDIF	
ENDPROC

PROC UNPAUSE_RENDERPHASE_FOR_INTRO_CUTSCENE()
	IF NOT IS_BIT_SET(iLocalBoolCheck4, LBOOL4_INTRO_CUT_RULE_RENDERPHASE_UNPAUSED)
		IF IS_INTRO_CUTSCENE_RULE_ACTIVE()
			ANIMPOSTFX_STOP_ALL()
			CLEANUP_ALL_CORONA_FX(FALSE, IS_BIT_SET(iLocalBoolCheck30, LBOOL30_MOCAP_FOCUS_ENTITY_SET))
			SET_SKYFREEZE_CLEAR(TRUE)
			SET_HIDOF_OVERRIDE(FALSE,FALSE,0,0,0,0)
			
			SET_MISSION_READY_FOR_ISLAND_WATER()
			
		ENDIF
		SET_BIT(iLocalBoolCheck4, LBOOL4_INTRO_CUT_RULE_RENDERPHASE_UNPAUSED)
		PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - UNPAUSE_RENDERPHASE_FOR_INTRO_CUTSCENE - Unpausing Renderphase")
	ENDIF
ENDPROC
					
PROC SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(eFMMC_SCRIPTED_CUTSCENE_PROGRESS eNewState)
	PRINTLN("[LM][SET_SCRIPTED_CUTSCENE_STATE_PROGRESS][PROCESS_SCRIPTED_CUTSCENE] - Setting Scripted Cutscene State from ", 
	GET_SCRIPTED_CUTSCENE_STATE_PROGRESS_NAME(eScriptedCutsceneProgress), " to ", GET_SCRIPTED_CUTSCENE_STATE_PROGRESS_NAME(eNewState))
	
	DEBUG_PRINTCALLSTACK()
	eScriptedCutsceneProgress = eNewState
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_SAFE_TO_INTERP_BACK_TO_GAMEPLAY()
	
	IF NOT IS_PED_INJURED(LocalPlayerPed)
	AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		VEHICLE_INDEX vehPedIsIn = GET_VEHICLE_PED_IS_IN(localPlayerPed)
		IF IS_VEHICLE_DRIVEABLE(vehPedIsIn)
			VEHICLE_SEAT vsSeat = GET_SEAT_PED_IS_IN(localPlayerPed)
			
			IF GET_ENTITY_MODEL(vehPedIsIn) = PATROLBOAT			
			AND vsSeat = VS_BACK_RIGHT
				RETURN FALSE // Prevents some janky camera interp.
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

//Turn on the vehicle Radio after the cutscenes
PROC TURN_ON_CUTSCENE_VEHICLE_RADIO()
	IF bLocalPlayerPedOK
		IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
			VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
			AND IS_VEHICLE_DRIVEABLE(vehPlayer)
				IF NOT IS_STRING_NULL_OR_EMPTY(sCurrentRadioStation)
				AND NOT ARE_STRINGS_EQUAL(sCurrentRadioStation, "OFF")
					SET_RADIO_TO_STATION_NAME(sCurrentRadioStation)
					SET_VEHICLE_RADIO_ENABLED(vehPlayer, TRUE)
					PRINTLN("[LM][TURN_ON_CUTSCENE_VEHICLE_RADIO] - Setting Radio Station to ", sCurrentRadioStation)
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
ENDPROC

FUNC STRING GET_PLAYER_SPECIFIC_ANIMATION_NAME_FROM_CREATOR_INT(INT iAnimation, INT iPlayer)
	SWITCH iAnimation
		CASE ci_CS_PS_ANIM_APARTMENT_DOOR_ENTRY
			SWITCH iPlayer
				CASE 0 RETURN "ext_player"	 	BREAK
				CASE 1 RETURN "ext_a"			BREAK
				CASE 2 RETURN "ext_b" 			BREAK
				CASE 3 RETURN "ext_c"			BREAK
			ENDSWITCH
		BREAK
		CASE ci_CS_PS_HINGE_L_EXIT1
			SWITCH iPlayer
				CASE 0 RETURN "player_exit" 	BREAK
				CASE 1 RETURN "player_exit"		BREAK
				CASE 2 RETURN "player_exit" 	BREAK
				CASE 3 RETURN "player_exit"		BREAK
			ENDSWITCH
		BREAK
		CASE ci_CS_PS_HINGE_L_EXIT2
			SWITCH iPlayer
				CASE 0 RETURN "ext_player"	 	BREAK
				CASE 1 RETURN "ext_player"		BREAK
				CASE 2 RETURN "ext_player" 		BREAK
				CASE 3 RETURN "ext_player"		BREAK
			ENDSWITCH
		BREAK
		CASE ci_CS_PS_HINGE_L_STEALTH_EXIT
			SWITCH iPlayer
				CASE 0 RETURN "player_exit"	 	BREAK
				CASE 1 RETURN "player_exit"		BREAK
				CASE 2 RETURN "player_exit" 	BREAK
				CASE 3 RETURN "player_exit"		BREAK
			ENDSWITCH
		BREAK
		CASE ci_CS_PS_HINGE_R_EXIT1
			SWITCH iPlayer
				CASE 0 RETURN "ext_player"	 	BREAK
				CASE 1 RETURN "ext_player"		BREAK
				CASE 2 RETURN "ext_player" 		BREAK
				CASE 3 RETURN "ext_player"		BREAK
			ENDSWITCH
		BREAK		
		CASE ci_CS_PS_HINGE_IDLE_GROUP_WAIT
			SWITCH iPlayer				
				CASE 0 RETURN "idle_a"	 	BREAK
				CASE 1 RETURN "idle_a"		BREAK
				CASE 2 RETURN "idle_b"		BREAK
				CASE 3 RETURN "idle_c"		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_PLAYER_SPECIFIC_ANIMATION_DICT_FROM_CREATOR_INT(INT iAnimation, INT iPlayer, BOOL bFemale)
	SWITCH iAnimation
		CASE ci_CS_PS_ANIM_APARTMENT_DOOR_ENTRY   	RETURN	"anim@BUILDING_TRANS@HINGE_L"		
		CASE ci_CS_PS_HINGE_L_EXIT1				 	RETURN	"anim@apt_trans@hinge_l_action"
		CASE ci_CS_PS_HINGE_L_EXIT2				 	RETURN	"anim@apt_trans@hinge_l"
		CASE ci_CS_PS_HINGE_L_STEALTH_EXIT		 	RETURN	"anim@apt_trans@hinge_l_stealth"
		CASE ci_CS_PS_HINGE_R_EXIT1				 	RETURN	"anim@apt_trans@hinge_r"
		CASE ci_CS_PS_HINGE_IDLE_GROUP_WAIT			
			SWITCH iPlayer
				CASE 0
					IF bFemale 
						RETURN "amb@world_human_hang_out_street@female_arms_crossed@idle_a"
					ELSE
						RETURN "amb@world_human_stand_guard@male@idle_a"
					ENDIF
				BREAK
				CASE 1
					IF bFemale
						RETURN "amb@world_human_stand_impatient@female@no_sign@idle_a"
					ELSE
						RETURN "amb@world_human_stand_impatient@male@no_sign@idle_a"
					ENDIF
				BREAK
				CASE 2
					IF bFemale
						RETURN "amb@world_human_stand_impatient@female@no_sign@idle_a"
					ELSE
						RETURN "amb@world_human_stand_impatient@male@no_sign@idle_a"
					ENDIF
				BREAK
				CASE 3
					IF bFemale
						RETURN "amb@world_human_stand_impatient@female@no_sign@idle_a"
					ELSE
						RETURN "amb@world_human_stand_impatient@male@no_sign@idle_a"
					ENDIF
				BREAK				
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC ASSIGN_PLAYER_SPECIFIC_ANIMATIONS_PED_POOL(PED_INDEX &pedCutscene[], PLAYER_INDEX &playerCutscene[], INT &iPlayer[], BOOL bUseClones, INT iTeam)
	INT i, iPlayerCount
		
	IF bUseClones
	
		PRINTLN("[PLAYER_LOOP] - ASSIGN_PLAYER_SPECIFIC_ANIMATIONS_PED_POOL - Using Clones.")
		
		FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1	
			PED_INDEX pedPlayer = MocapPlayerPed[i]			
			IF NOT IS_PED_INJURED(pedPlayer)
				pedCutscene[iPlayerCount] = pedPlayer				
				iPlayer[iPlayerCount] = i
				iPlayerCount++
			ENDIF
		ENDFOR
		
	ELSE
		
		PRINTLN("[PLAYER_LOOP] - ASSIGN_PLAYER_SPECIFIC_ANIMATIONS_PED_POOL - Not using clones")
				
		FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
			IF MC_serverBD.piCutscenePlayerIDs[iTeam][i] = INVALID_PLAYER_INDEX()
				RELOOP
			ENDIF
			
			playerCutscene[i] = MC_serverBD.piCutscenePlayerIDs[iTeam][i]
			pedCutscene[i] = GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[iTeam][i])			
			iPlayer[i] = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[iTeam][i]))
		ENDFOR
		
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_CUSTOM_SYNC_SCENE_PREVENT_SCRIPT_CAMS_RENDERING_AT_START(FMMC_CUTSCENE_SYNC_SCENE_DATA &sCutsceneSyncSceneData, INT iCutsceneToUse)
	IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)
	AND sCutsceneSyncSceneData.iShotToPlayOn = 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_CONVERSATION_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(FMMC_CUTSCENE_SYNC_SCENE_DATA sCutsceneSyncSceneData, INT iCutscene)
	
	IF IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_sCutsceneSyncSceneConversationProcessed)
		RETURN TRUE
	ENDIF
	
	STRING sSceneConversationBlock = GET_CONVERSATION_BLOCK_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneData.eType)
	STRING sSceneConversationRoot = GET_CONVERSATION_ROOT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneData.eType)
	
	IF IS_STRING_NULL_OR_EMPTY(sSceneConversationBlock)
	OR IS_STRING_NULL_OR_EMPTY(sSceneConversationRoot)
		PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_CONVERSATION_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE - No Conversation for this Sync Scene.")
		SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_sCutsceneSyncSceneConversationProcessed)
		RETURN TRUE
	ENDIF
				
	STRING sSceneConversationVoiceName_0 = GET_CONVERSATION_PED_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneData.eType, 0)
	INT sSceneConversationVoiceSpeakerID_0 = GET_CONVERSATION_VOICE_SPEAKER_ID_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneData.eType, 0)
	INT iPedIndex_0 = GET_CONVERSATION_PED_INDEX_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneData.eType, 0, iCutscene)	
	
	BOOL bNetworked = GET_CONVERSATION_SHOULD_PLAY_OVER_NETWORK_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneData.eType)
	INT iBS
	
	SET_BIT(iBS, ciScriptedAnimConversationBS_StartPaused)
	
	FMMC_SET_UP_SCRIPTED_ANIM_CONVERSATION(sSceneConversationBlock, sSceneConversationRoot, DEFAULT, iBS)
	
	IF iPedIndex_0 != -1
		FMMC_ADD_PED_TO_SCRIPTED_ANIM_CONVERSATION(0, iPedIndex_0, sSceneConversationRoot, sSceneConversationVoiceName_0, sSceneConversationVoiceSpeakerID_0, FALSE, FALSE, FALSE)
	ELSE
		PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_CONVERSATION_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE - Invalid ped setup or missing peds in cutscene registration.")
		SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_sCutsceneSyncSceneConversationProcessed)
		RETURN TRUE
	ENDIF
	
	PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_CONVERSATION_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE - Attempting to play Sync Scene Conversation with the following Details:")
	PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_CONVERSATION_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE - sSceneConversationBlock: ", sSceneConversationBlock, " sSceneConversationRoot: ", sSceneConversationRoot, " sSceneConversationVoiceName_0: ", sSceneConversationVoiceName_0, " sSceneConversationVoiceSpeakerID_0: ", sSceneConversationVoiceSpeakerID_0, " iPedIndex_0: ", iPedIndex_0, " bNetworked: ", bNetworked)	
	
	IF FMMC_PLAY_CURRENT_SCRIPTED_ANIM_CONVERSATION(bNetworked)
	OR IS_SCRIPTED_ANIM_CONVERSATION_PAUSED()
		PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_CONVERSATION_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE - Starting Sync Scene Conversation!")
		SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_sCutsceneSyncSceneConversationProcessed)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_WAIT_FOR_SYNC_SCENE_PARTICIPANTS(INT &iPlayers[])

	IF HAS_NET_TIMER_STARTED(tdScriptedCutsceneSyncSceneSafetyTimer)
	AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdScriptedCutsceneSyncSceneSafetyTimer, 6000)
		PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - SHOULD_WAIT_FOR_SYNC_SCENE_PARTICIPANTS - Safety Timer hit. Bailing.")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(tdScriptedCutsceneSyncSceneSafetyTimer)
		START_NET_TIMER(tdScriptedCutsceneSyncSceneSafetyTimer)
	ENDIF
	
	INT i
	FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
		IF iPlayers[i] = -1
			RELOOP
		ENDIF
		
		IF NOT IS_BIT_SET(MC_playerBD[iPlayers[i]].iClientBitSet2, PBBOOL2_STARTED_CUTSCENE)			
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - SHOULD_WAIT_FOR_SYNC_SCENE_PARTICIPANTS - Returning TRUE - Waiting for iPlayer: ", iPlayers[i])
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RESET_NET_TIMER(tdScriptedCutsceneSyncSceneSafetyTimer)
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_SYNC_SCENE_PARTICIPANTS_READY(INT &iPlayers[], PED_INDEX &pedPlayers[], SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType)

	IF HAS_NET_TIMER_STARTED(tdScriptedCutsceneSyncSceneSafetyTimer2)
	AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdScriptedCutsceneSyncSceneSafetyTimer2, 8000)
		PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - ARE_ALL_SYNC_SCENE_PARTICIPANTS_READY - Safety Timer hit. Bailing.")
		RETURN TRUE
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(tdScriptedCutsceneSyncSceneSafetyTimer2)
		START_NET_TIMER(tdScriptedCutsceneSyncSceneSafetyTimer2)
	ENDIF
	
	INT i
	FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
		IF iPlayers[i] = -1
			RELOOP
		ENDIF
		
		IF NOT IS_BIT_SET(MC_playerBD[iPlayers[i]].iClientBitSet4, PBBOOL4_READY_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE)			
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - ARE_ALL_SYNC_SCENE_PARTICIPANTS_READY - Returning FALSE - Waiting for iPlayer: ", iPlayers[i], " via PBBOOL4_READY_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE.")
			RETURN FALSE
		ENDIF
		
		IF NOT SHOULD_SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_USE_A_VEHICLE(eType)
			IF DOES_ENTITY_EXIST(pedPlayers[i])
			AND IS_ENTITY_ALIVE(pedPlayers[i])
				IF IS_PED_IN_ANY_VEHICLE(pedPlayers[i], TRUE)
					PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - ARE_ALL_SYNC_SCENE_PARTICIPANTS_READY - Returning FALSE - Waiting for iPlayer: ", iPlayers[i], " They are in a vehicle")
					RETURN FALSE
				ENDIF
				IF GET_SCRIPT_TASK_STATUS(pedPlayers[i], SCRIPT_TASK_LEAVE_VEHICLE) = WAITING_TO_START_TASK
				OR GET_SCRIPT_TASK_STATUS(pedPlayers[i], SCRIPT_TASK_LEAVE_VEHICLE) = PERFORMING_TASK
				OR GET_SCRIPT_TASK_STATUS(pedPlayers[i], SCRIPT_TASK_ENTER_VEHICLE) = WAITING_TO_START_TASK
				OR GET_SCRIPT_TASK_STATUS(pedPlayers[i], SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK
					PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - ARE_ALL_SYNC_SCENE_PARTICIPANTS_READY - Returning FALSE - Waiting for iPlayer: ", iPlayers[i], " They are entering/leaving a vehicle")
					RETURN FALSE
				ENDIF				
			ENDIF
		ENDIF
	ENDFOR
	
	IF IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_sCutsceneSyncSceneUseExrtaVehicleTaskTimer)
		IF NOT HAS_NET_TIMER_STARTED(tdScriptedCutsceneSyncSceneVehicleTaskTimer)
			START_NET_TIMER(tdScriptedCutsceneSyncSceneVehicleTaskTimer)
		ENDIF
		IF HAS_NET_TIMER_STARTED(tdScriptedCutsceneSyncSceneVehicleTaskTimer)
		AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdScriptedCutsceneSyncSceneVehicleTaskTimer, 2000)
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - ARE_ALL_SYNC_SCENE_PARTICIPANTS_READY - Waiting for tdScriptedCutsceneSyncSceneVehicleTaskTimer")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RESET_NET_TIMER(tdScriptedCutsceneSyncSceneSafetyTimer2)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_ALL_SYNC_SCENE_PARTICIPANTS_FINISHED(INT &iPlayers[])

	IF HAS_NET_TIMER_STARTED(tdScriptedCutsceneSyncSceneSafetyTimer3)
	AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdScriptedCutsceneSyncSceneSafetyTimer3, 5000)
		PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - ARE_ALL_SYNC_SCENE_PARTICIPANTS_FINISHED - Safety Timer hit. Bailing.")
		RETURN TRUE
	ENDIF

	IF NOT HAS_NET_TIMER_STARTED(tdScriptedCutsceneSyncSceneSafetyTimer3)
		START_NET_TIMER(tdScriptedCutsceneSyncSceneSafetyTimer3)
	ENDIF
	
	INT i
	FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
		IF iPlayers[i] = -1
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iPlayers[i]].iClientBitSet4, PBBOOL4_READY_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE)
		AND NOT IS_BIT_SET(MC_playerBD[iPlayers[i]].iClientBitSet4, PBBOOL4_FINISHED_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE)
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - ARE_ALL_SYNC_SCENE_PARTICIPANTS_FINISHED - Returning FALSE - Waiting for iPlayer: ", iPlayers[i])
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RESET_NET_TIMER(tdScriptedCutsceneSyncSceneSafetyTimer3)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE_BEEN_CREATED_YET()
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_CREATED_SCRIPTED_CUTSCENE_SYNC_SCENE)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC ADD_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE_EXTRA_VEHICLE_TASK_TIMER(PED_INDEX pedToCheck)
	IF IS_PED_IN_ANY_VEHICLE(pedToCheck, TRUE)
	OR GET_SCRIPT_TASK_STATUS(pedToCheck, SCRIPT_TASK_LEAVE_VEHICLE) = WAITING_TO_START_TASK
	OR GET_SCRIPT_TASK_STATUS(pedToCheck, SCRIPT_TASK_LEAVE_VEHICLE) = PERFORMING_TASK 		
	OR GET_SCRIPT_TASK_STATUS(pedToCheck, SCRIPT_TASK_ENTER_VEHICLE) = WAITING_TO_START_TASK
	OR GET_SCRIPT_TASK_STATUS(pedToCheck, SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK	
		IF NOT IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_sCutsceneSyncSceneUseExrtaVehicleTaskTimer)
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - ADD_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE_EXTRA_VEHICLE_TASK_TIMER - Vehicles tasks break on-foot sync scenes - Adding an extra timer to ensure cleaned up tasks.")
			SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_sCutsceneSyncSceneUseExrtaVehicleTaskTimer)
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_ANIM_SYNC_SCENE_PLAYER_INDEX_THIS_PED_IS_USING(STRING sHandle)
	IF ARE_STRINGS_EQUAL(sHandle, "Player0_Always")
	OR ARE_STRINGS_EQUAL(sHandle, "Player0_NotExist")
		RETURN 0
	ENDIF
	IF ARE_STRINGS_EQUAL(sHandle, "Player1_Always")
	OR ARE_STRINGS_EQUAL(sHandle, "Player1_NotExist")
		RETURN 1
	ENDIF
	IF ARE_STRINGS_EQUAL(sHandle, "Player2_Always")
	OR ARE_STRINGS_EQUAL(sHandle, "Player2_NotExist")
		RETURN 2
	ENDIF
	IF ARE_STRINGS_EQUAL(sHandle, "Player3_Always")
	OR ARE_STRINGS_EQUAL(sHandle, "Player3_NotExist")
		RETURN 3
	ENDIF
	IF ARE_STRINGS_EQUAL(sHandle, "Player_FirstFree")	
		INT i 
		FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
			IF iScriptedCutsceneTeam = -1
				BREAKLOOP
			ENDIF
			IF MC_serverBD.piCutscenePlayerIDs[iScriptedCutsceneTeam][i] = INVALID_PLAYER_INDEX()
				RETURN i
			ENDIF
		ENDFOR		
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC BOOL DOES_ANIM_SYNC_SCENE_PED_REPLACE_A_PLAYER(STRING sHandle)
	IF ARE_STRINGS_EQUAL(sHandle, "Player0_Always")
	OR ARE_STRINGS_EQUAL(sHandle, "Player1_Always")	
	OR ARE_STRINGS_EQUAL(sHandle, "Player2_Always")
	OR ARE_STRINGS_EQUAL(sHandle, "Player3_Always")	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE(FMMC_CUTSCENE_SYNC_SCENE_DATA sCutsceneSyncSceneData, INT iShot, INT iCutscene, PED_INDEX &pedCutscene[])
	UNUSED_PARAMETER(iShot)
	PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Calling to play an Anim Scene for this Scripted Cutscene.")
	
	SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType = sCutsceneSyncSceneData.eType
	VECTOR vCoords = sCutsceneSyncSceneData.vCoord
	VECTOR vRot = <<0.0, 0.0, sCutsceneSyncSceneData.fHeading+GET_SCRIPTED_CUTSCENE_SYNC_SCENE_HEADING_OFFSET(eType)>>	
	STRING animDict, animName, animFaceName, animDict_PrevUsed, animName_PrevUsed
	
	PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - eType: ", ENUM_TO_INT(eType), " vCoords: ", vCoords, " vRot: ", vRot)
		
	IK_CONTROL_FLAGS ikFlags
	ikFlags = AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK | AIK_DISABLE_HEAD_IK | AIK_DISABLE_TORSO_IK	| AIK_DISABLE_TORSO_REACT_IK
	
	SYNCED_SCENE_PLAYBACK_FLAGS sceneFlags
	sceneFlags = SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_PRESERVE_VELOCITY | SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_NET_DISREGARD_ATTACHMENT_CHECKS | SYNCED_SCENE_VEHICLE_ALLOW_PLAYER_ENTRY
	IF NOT SHOULD_SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_USE_A_VEHICLE(sCutsceneSyncSceneData.eType) 
		sceneFlags = sceneFlags | SYNCED_SCENE_SET_PED_OUT_OF_VEHICLE_AT_START
	ENDIF	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitset2, ci_CSBS2_ShowPlayerWeapon)
		sceneFlags = sceneFlags | SYNCED_SCENE_HIDE_WEAPON
	ENDIF
	
	RAGDOLL_BLOCKING_FLAGS rbfPed =	RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_FIRE | RBF_ELECTROCUTION | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_IMPACT_OBJECT | RBF_MELEE | RBF_RUBBER_BULLET | RBF_FALLING | RBF_WATER_JET | RBF_DROWNING |RBF_ALLOW_BLOCK_DEAD_PED | RBF_PLAYER_BUMP | RBF_PLAYER_RAGDOLL_BUMP | RBF_PED_RAGDOLL_BUMP | RBF_VEHICLE_GRAB
	
	INT iScriptedCutsceneSyncSceneID
	MC_ServerBD_1.iScriptedCutsceneSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, SHOULD_SCRIPTED_CUTSCENE_SYNC_SCENE_HOLD_LAST_FRAME(eType), FALSE, GET_PHASE_END_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(eType), GET_PHASE_START_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(eType))	
	iScriptedCutsceneSyncSceneID = MC_ServerBD_1.iScriptedCutsceneSyncScene
	PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Creation Scene ID: ", iScriptedCutsceneSyncSceneID, " host")
	
	PED_INDEX pedPlayer
	INT i, iPlayer
	FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
		
		iPlayer = GET_ADJUSTED_INDEX_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PLAYER(eType, i, SHOULD_SCRIPTED_CUTSCENE_SCYN_SCENE_TYPE_USE_ALTERNATE_VARIATION(eType, iCutscene), pedCutscene)		
		
		pedPlayer = pedCutscene[iPlayer]
		
		IF IS_PED_INJURED(pedPlayer)
		OR NOT DOES_ENTITY_EXIST(pedPlayer)
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Player: ", iPlayer, " is null.")
			RELOOP
		ENDIF
		
		animDict = GET_ANIM_DICT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(eType, SHOULD_SCRIPTED_CUTSCENE_SCYN_SCENE_TYPE_USE_ALTERNATE_VARIATION(eType, iCutscene, pedPlayer))
		animName = GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PLAYER(eType, PICK_INT(IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet4, ci_CSBS4_PlaySyncSceneCutsceneOnlyWithLocalPlayerPed), 0, iPlayer), SHOULD_SCRIPTED_CUTSCENE_SCYN_SCENE_TYPE_USE_ALTERNATE_VARIATION(eType, iCutscene, pedPlayer))
		animFaceName = GET_ANIM_NAME_FACE_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PLAYER(eType, PICK_INT(IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet4, ci_CSBS4_PlaySyncSceneCutsceneOnlyWithLocalPlayerPed), 0, iPlayer), SHOULD_SCRIPTED_CUTSCENE_SCYN_SCENE_TYPE_USE_ALTERNATE_VARIATION(eType, iCutscene, pedPlayer))
		
		IF NOT IS_STRING_NULL_OR_EMPTY(animName)
			
			animDict_PrevUsed = animDict
			animName_PrevUsed = animName
				
			SET_ENTITY_LOCALLY_VISIBLE(pedPlayer)
			SET_ENTITY_VISIBLE_IN_CUTSCENE(pedPlayer, TRUE, TRUE)
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (PLAYER) Setting Player Ped: ", iPlayer, " visible")
			
			IF MC_ServerBD_1.fSyncSceneAnimDurationForPhaseFadeoutTime = 0.0
				MC_ServerBD_1.fSyncSceneAnimDurationForPhaseFadeoutTime = GET_ANIM_DURATION(animDict, animName)*1000
			ENDIF
			
			NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedPlayer, iScriptedCutsceneSyncSceneID, animDict, animName, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, sceneFlags, rbfPed, DEFAULT, ikFlags)
			IF NOT SHOULD_SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_USE_A_VEHICLE(sCutsceneSyncSceneData.eType) 
				ADD_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE_EXTRA_VEHICLE_TASK_TIMER(pedPlayer)
			ENDIF
			
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (PLAYER) Adding iPlayer: ", iPlayer, " with animDict: ", animDict, " AnimName: ", animName, " model: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(pedPlayer)))
			
			IF NOT IS_STRING_NULL_OR_EMPTY(animFaceName)
				PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (PLAYER) Player Ped: ", iPlayer, " Should use animFaceName: ", animFaceName)
				PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedPlayer)				
				IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
					PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (PLAYER) Player Ped: ", iPlayer, " is player name: ", GET_PLAYER_NAME(piPlayer))
					PARTICIPANT_INDEX piPart = NETWORK_GET_PARTICIPANT_INDEX(piPlayer)
					BROADCAST_FMMC_PLAY_SYNC_SCENE_FACIAL_ANIM(eType, PICK_INT(IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet4, ci_CSBS4_PlaySyncSceneCutsceneOnlyWithLocalPlayerPed), 0, iPlayer), SHOULD_SCRIPTED_CUTSCENE_SCYN_SCENE_TYPE_USE_ALTERNATE_VARIATION(eType, iCutscene, pedPlayer), NATIVE_TO_INT(piPart))		
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (PLAYER) Not Adding iPlayer: ", iPlayer, " No anim name exists for them.")
			
			// This player will not be visible in the scene.
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet4, ci_CSBS4_PlaySyncSceneCutsceneOnlyWithLocalPlayerPed)	
				PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (PLAYER) iPlayer: ", iPlayer, " will be flagged as invisible.")
				SET_BIT(MC_ServerBD_1.iScriptedCutsceneSyncSceneInvisibilityBS, iPlayer)
				
				// Add them to the scene otherwise they won't use the camera.				
				IF NOT IS_STRING_NULL_OR_EMPTY(animName_PrevUsed)
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedPlayer, iScriptedCutsceneSyncSceneID, animDict_PrevUsed, animName_PrevUsed, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, sceneFlags, DEFAULT, DEFAULT, ikFlags)
					IF NOT SHOULD_SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_USE_A_VEHICLE(sCutsceneSyncSceneData.eType) 
						ADD_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE_EXTRA_VEHICLE_TASK_TIMER(pedPlayer)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDFOR
	
	NETWORK_INDEX netid
	PED_INDEX pedIndex
	ENTITY_INDEX entIndex
	
	INT iProp, iObj, iVeh, iPed
	FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES-1
		IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType = -1
		OR g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex = -1
			RELOOP
		ENDIF
		
		IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType = CREATION_TYPE_PROPS			
			entIndex = GET_CUTSCNE_VISIBLE_PROP(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex)
			IF entIndex != NULL
				animDict = GET_ANIM_DICT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(eType, FALSE)
				animName = GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PROP(eType, iProp)
				IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].tlCutsceneHandle)
					animName = TEXT_LABEL_TO_STRING(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].tlCutsceneHandle)
				ENDIF
				IF IS_STRING_NULL_OR_EMPTY(animName)
				OR IS_STRING_NULL_OR_EMPTY(animDict)
					PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (PROP) Trying to add but no anim.")
					RELOOP
				ENDIF
				
				NETWORK_ADD_MAP_ENTITY_TO_SYNCHRONISED_SCENE(MC_ServerBD_1.iScriptedCutsceneSyncScene, GET_ENTITY_MODEL(entIndex), GET_ENTITY_COORDS(entIndex, FALSE), animDict, animName, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, sceneFlags)
				PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (PROP) Adding iCutsceneEntity: ", i, " iProp: ", iProp, " with animDict: ", animDict, " AnimName: ", animName)
				iProp++
			ELSE
				PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (PROP) Trying to add prop: ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex, " but ent index is null")
			ENDIF
		ELSE
			netid = GET_CUTSCNE_VISIBLE_NET_ID(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType, g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex)
			IF netid != NULL 
				IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType = CREATION_TYPE_OBJECTS
					entIndex = NET_TO_ENT(netid)
					animDict = GET_ANIM_DICT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(eType, FALSE)
					animName = GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_OBJECT(eType, iObj)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].tlCutsceneHandle)
						animName = TEXT_LABEL_TO_STRING(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].tlCutsceneHandle)
					ENDIF
					IF IS_STRING_NULL_OR_EMPTY(animName)
					OR IS_STRING_NULL_OR_EMPTY(animDict)
						RELOOP
					ENDIF
					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(entIndex, iScriptedCutsceneSyncSceneID, animDict, animName, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, sceneFlags)
					PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (OBJECT) Adding iCutsceneEntity: ", i, " iObj: ", iObj, " with animDict: ", animDict, " AnimName: ", animName)
					iObj++
				ELIF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType = CREATION_TYPE_VEHICLES
					entIndex = NET_TO_ENT(netid)
					animDict = GET_ANIM_DICT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(eType, FALSE)
					animName = GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_VEHICLE(eType, iVeh)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].tlCutsceneHandle)
						animName = TEXT_LABEL_TO_STRING(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].tlCutsceneHandle)
					ENDIF
					IF IS_STRING_NULL_OR_EMPTY(animName)
					OR IS_STRING_NULL_OR_EMPTY(animDict)
						RELOOP
					ENDIF
					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(entIndex, iScriptedCutsceneSyncSceneID, animDict, animName, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, sceneFlags)
					PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (VEHICLE) Adding iCutsceneEntity: ", i, " iVeh: ", iVeh, " with animDict: ", animDict, " AnimName: ", animName)
					iVeh++
				ELIF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType = CREATION_TYPE_PEDS
					pedIndex = NET_TO_PED(netid)
					animDict = GET_ANIM_DICT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(eType, FALSE)
					animName = GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PED(eType, iPed)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].tlCutsceneHandle)
						animName = TEXT_LABEL_TO_STRING(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].tlCutsceneHandle)
					ENDIF								
					INT iPedPlayer = GET_ANIM_SYNC_SCENE_PLAYER_INDEX_THIS_PED_IS_USING(animName)
					IF iPedPlayer != -1
						IF DOES_ANIM_SYNC_SCENE_PED_REPLACE_A_PLAYER(animName)
							SET_BIT(MC_ServerBD_1.iScriptedCutsceneSyncSceneInvisibilityBS, iPedPlayer)
						ENDIF
						animName = GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PLAYER(eType, iPedPlayer, SHOULD_SCRIPTED_CUTSCENE_SCYN_SCENE_TYPE_USE_ALTERNATE_VARIATION(eType, iCutscene, pedIndex))
						animDict = GET_ANIM_DICT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(eType, SHOULD_SCRIPTED_CUTSCENE_SCYN_SCENE_TYPE_USE_ALTERNATE_VARIATION(eType, iCutscene, pedIndex))						
					ENDIF
					IF IS_STRING_NULL_OR_EMPTY(animName)
					OR IS_STRING_NULL_OR_EMPTY(animDict)
						RELOOP
					ENDIF
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedIndex, TRUE)
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedIndex, iScriptedCutsceneSyncSceneID, animDict, animName, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, sceneFlags, rbfPed, DEFAULT, ikFlags)
					PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (PED) Adding iCutsceneEntity: ", i, " iPed: ", iPed, " with animDict: ", animDict, " AnimName: ", animName)
					iPed++
				ENDIF
				IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType = CREATION_TYPE_DYNOPROPS
					entIndex = NET_TO_ENT(netid)
					animDict = GET_ANIM_DICT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(eType, FALSE)
					animName = GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PROP(eType, iProp)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].tlCutsceneHandle)
						animName = TEXT_LABEL_TO_STRING(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].tlCutsceneHandle)
					ENDIF
					IF IS_STRING_NULL_OR_EMPTY(animName)
					OR IS_STRING_NULL_OR_EMPTY(animDict)
						RELOOP
					ENDIF
					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(entIndex, iScriptedCutsceneSyncSceneID, animDict, animName, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, sceneFlags)
					PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (DYNO) Adding iCutsceneEntity: ", i, " iProp: ", iProp, " with animDict: ", animDict, " AnimName: ", animName)
					iProp++
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_UsePlacedCameraInCustomSyncScene)		
		animDict = GET_ANIM_DICT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(eType, FALSE)
		animName = GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_CAM(eType, IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet3, ci_CSBS3_UsePlacedCameraInCustomSyncScene_ForceLeftAnim), SHOULD_SCRIPTED_CUTSCENE_SCYN_SCENE_TYPE_USE_ALTERNATE_VARIATION(eType, iCutscene))
		IF NOT IS_STRING_NULL_OR_EMPTY(animName)
		AND NOT IS_STRING_NULL_OR_EMPTY(animDict)
			NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(iScriptedCutsceneSyncSceneID, animDict, animName)	
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (CAM) Using an Animated Camera. animDict: ", animDict, " animName: ", animName)	
		ELSE
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (CAM) NO VALID Animated Camera. animDict: ", animDict, " animName: ", animName)	
		ENDIF		
	ELSE
		PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (CAM) Not Using an Animated Camera.")
	ENDIF
	
	SET_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_CREATED_SCRIPTED_CUTSCENE_SYNC_SCENE)
	
	PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Sync Scene Creation Finished.")
ENDPROC

PROC PROCESS_ANIM_SYNC_SCENE_FADEOUT_AND_PROGRESS_FOR_SCRIPTED_CUTSCENE(INT iCutscene, BOOL &bProgress)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet3, ci_CSBS3_SyncSceneBreakoutEndsCutscene)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet3, ci_CSBS3_SyncSceneBreakoutFadesCutscene)
	AND NOT IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_EarlySetOutOfMpCutscene)
		SET_PLAYER_OUT_OF_SCRIPTED_CUTSCENE_FOR_SYNC_SCENE()
		SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_EarlySetOutOfMpCutscene)
	ENDIF
			
	IF NOT IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_sCutsceneSyncSceneClearedArea)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet4, ci_CSBS4_CutsceneClearAreaAtEndOnly)
			CLEAR_CUTSCENE_AREA(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].fClearRadius, g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].vClearCoOrds,g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].vReplacementArea, TRUE, IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet3, ci_CSBS3_ClearPersonalVehicles))
			SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_sCutsceneSyncSceneClearedArea)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet3, ci_CSBS3_SyncSceneBreakoutFadesCutscene)								
		IF NOT IS_SCREEN_FADING_OUT()
		AND NOT IS_SCREEN_FADED_OUT()
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Finished/Breakout - Fading screen out.")
			DO_SCREEN_FADE_OUT(250)
		ELIF IS_SCREEN_FADED_OUT()
			IF IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RenderingSyncSceneCam)
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet3, ci_CSBS3_SyncSceneBreakoutEndsCutscene)
				CLEAR_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RenderingSyncSceneCam)	
				PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Finished/Breakout - Turning On Script Cams.")
				SET_CAM_ACTIVE(cutscenecam, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)	
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RenderingSyncSceneCam)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet3, ci_CSBS3_SyncSceneBreakoutEndsCutscene)
			CLEAR_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RenderingSyncSceneCam)				
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Finished/Breakout - Turning On Script Cams.")
			SET_CAM_ACTIVE(cutscenecam, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)	
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet3, ci_CSBS3_SyncSceneBreakoutEndsCutscene)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet3, ci_CSBS3_SyncSceneBreakoutFadesCutscene)
		OR IS_SCREEN_FADED_OUT()
			bProgress = TRUE
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Finished/Breakout - Anim Scene is no longer running. Fading out and progressingCutscene")
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPTED_CUTSCENE_SYNC_SCENE_HIGH_DOF(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType)		
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_IAA_INTRO_SCENE
			SET_USE_HI_DOF_ON_SYNCED_SCENE_THIS_UPDATE()
		BREAK
	ENDSWITCH
ENDPROC

PROC TRIGGER_START_MUSIC_EVENT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType)
	
	IF IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_sCutsceneSyncSceneStartMusicEvent)
		EXIT
	ENDIF
	
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_IAA_INTRO_SCENE			
			TRIGGER_MUSIC_EVENT("ULP_INTRO_START_MUSIC")
		BREAK
	ENDSWITCH
	
	SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_sCutsceneSyncSceneStartMusicEvent)
	
ENDPROC

PROC TRIGGER_STOP_MUSIC_EVENT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType)
	
	IF IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_sCutsceneSyncSceneStopMusicEvent)
		EXIT
	ENDIF
	
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_IAA_INTRO_SCENE			
			TRIGGER_MUSIC_EVENT("ULP_INTRO_STOP_MUSIC")
		BREAK
	ENDSWITCH
	
	SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_sCutsceneSyncSceneStopMusicEvent)
	
ENDPROC

PROC FADE_IN_FOR_SYNC_SCENE_CUTSCENE()
	IF bLocalPlayerPedOK
	AND (NOT IS_PLAYER_RESPAWNING(LocalPlayer) OR IS_FAKE_MULTIPLAYER_MODE_SET())
	AND NOT g_bCelebrationScreenIsActive
	AND NOT (HAS_NET_TIMER_STARTED(tdovertimer) AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdovertimer) < 4000)
	AND NOT (HAS_NET_TIMER_STARTED(tdwastedtimer) AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdwastedtimer) < 4000)
		IF IS_SCREEN_FADING_OUT()
		OR IS_SCREEN_FADED_OUT()
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Screen was faded out. Fading back in now as we have started.")
			DO_SCREEN_FADE_IN(250)
		ENDIF
	ELSE
		PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Player is dead or not ok to fade back in.")
	ENDIF
ENDPROC

PROC PROCESS_SHOWING_AND_HIDING_CLONES_FOR_SCRIPTED_CUTSCENE_LIFT(INT iCutscene, PED_INDEX &pedCutscene[])

	eCSLift = g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].eCSLiftData	
	IF eCSLift = eCSLift_None	
	OR IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)	
	OR NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitset2, ci_CSBS2_Scripted_UsePlayerClones)
		EXIT
	ENDIF	
	
	INT i
	FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
		IF IS_PED_INJURED(pedCutscene[i])
		OR NOT DOES_ENTITY_EXIST(pedCutscene[i])
			RELOOP
		ENDIF
		IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_ATTACHED_CLONES_FOR_LIFT_SCENE)			
		AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REACHED_CUT_PROCESS_SHOTS)
			IF NETWORK_GET_ENTITY_IS_NETWORKED(pedCutscene[i])
				SET_ENTITY_LOCALLY_VISIBLE(pedCutscene[i])
			ELSE
				RESET_ENTITY_ALPHA(pedCutscene[i])
			ENDIF
			IF NETWORK_GET_ENTITY_IS_NETWORKED(MocapPlayerPed[i])
				SET_ENTITY_LOCALLY_INVISIBLE(MocapPlayerPed[i])
			ELSE
				SET_ENTITY_ALPHA(MocapPlayerPed[i], 255, FALSE)
			ENDIF
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_SHOWING_AND_HIDING_CLONES_FOR_SCRIPTED_CUTSCENE_LIFT - (Pre Starting) Player: ", i, " SET_ENTITY_LOCALLY_VISIBLE")
		ELSE
			IF NETWORK_GET_ENTITY_IS_NETWORKED(pedCutscene[i])
				SET_ENTITY_LOCALLY_INVISIBLE(pedCutscene[i])
			ELSE
				RESET_ENTITY_ALPHA(pedCutscene[i])
			ENDIF			
			IF NETWORK_GET_ENTITY_IS_NETWORKED(MocapPlayerPed[i])
				SET_ENTITY_LOCALLY_VISIBLE(MocapPlayerPed[i])
			ELSE
				SET_ENTITY_ALPHA(MocapPlayerPed[i], 255, FALSE)
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC PROCESS_POST_SHOWING_AND_HIDING_SYNC_SCENE_ENTITIES_FOR_SCRIPTED_CUTSCENE(INT iCutscene, PED_INDEX &pedCutscene[])
	IF IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RunningCutsceneSyncSceneLocally)
						
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet4, ci_CSBS4_PlaySyncSceneCutsceneOnlyWithLocalPlayerPed)	
			INT i
			FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
				IF IS_PED_INJURED(pedCutscene[i])
				OR NOT DOES_ENTITY_EXIST(pedCutscene[i])
					RELOOP
				ENDIF
				IF IS_BIT_SET(MC_ServerBD_1.iScriptedCutsceneSyncSceneInvisibilityBS, i)				
					SET_ENTITY_LOCALLY_INVISIBLE(pedCutscene[i])
					PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_POST_SHOWING_AND_HIDING_SYNC_SCENE_ENTITIES_FOR_SCRIPTED_CUTSCENE - (Post Starting) Player: ", i, " SET_ENTITY_LOCALLY_INVISIBLE")				
				ELSE
					SET_ENTITY_LOCALLY_VISIBLE(pedCutscene[i])
					PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_POST_SHOWING_AND_HIDING_SYNC_SCENE_ENTITIES_FOR_SCRIPTED_CUTSCENE - (Post Starting) Player: ", i, " SET_ENTITY_LOCALLY_VISIBLE")
				ENDIF
			ENDFOR	
		ELSE
			INT i
			FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
				IF IS_PED_INJURED(pedCutscene[i])
				OR NOT DOES_ENTITY_EXIST(pedCutscene[i])
					RELOOP
				ENDIF			
				IF pedCutscene[i] != localPlayerPed		
					SET_ENTITY_LOCALLY_INVISIBLE(pedCutscene[i])
					PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_POST_SHOWING_AND_HIDING_SYNC_SCENE_ENTITIES_FOR_SCRIPTED_CUTSCENE - (Post Starting) local only - Player: ", i, " SET_ENTITY_LOCALLY_INVISIBLE")	
				ENDIF
			ENDFOR		
		ENDIF
		
		HIDE_ENTITIES_IN_CUTSCENE_AREA(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].fClearRadius, g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].vClearCoOrds,g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].vReplacementArea, TRUE, NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet3, ci_CSBS3_DontHidePersonalVehicles)	)
	ENDIF
ENDPROC

PROC PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE(FMMC_CUTSCENE_SYNC_SCENE_DATA sCutsceneSyncSceneData, INT iShot, INT iCutscene, PED_INDEX &pedCutscene[], INT &iPlayers[], BOOL &bProgress)
	
	IF iShot != sCutsceneSyncSceneData.iShotToPlayOn
	AND NOT IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_PlayedCutsceneSyncScene)
	AND NOT IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RunningCutsceneSyncSceneLocally)
		PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Waiting for the correct Shot before starting sync scene. iCamShot: ", iCamShot, " iShotToPlayOn: ", sCutsceneSyncSceneDataGameplay.iShotToPlayOn)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)	
		EXIT
	ENDIF
	
	// Start the associated conversation and pause it. It will be resumed as soon as the anim scene is running.
	PROCESS_CONVERSATION_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneData, iCutscene)
	
	// Vehicle Tasks such as entering/exitting can break sync scenes that are performed on-foot.
	IF NOT SHOULD_SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_USE_A_VEHICLE(sCutsceneSyncSceneData.eType) 
		
		IF IS_PED_IN_ANY_VEHICLE(localPlayerPed, TRUE)
		AND GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_LEAVE_VEHICLE) != WAITING_TO_START_TASK
		AND GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_LEAVE_VEHICLE) != PERFORMING_TASK 			
			TASK_LEAVE_ANY_VEHICLE(localPlayerPed)
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Vehicles tasks break on-foot sync scenes - Calling TASK_LEAVE_ANY_VEHICLE and waiting.")
			REINIT_NET_TIMER(tdScriptedCutsceneTimer)
			EXIT
		ENDIF
		
		IF IS_LOCAL_PLAYER_PED_GETTING_IN_OR_OUT_OF_ANY_VEHICLE()
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Vehicles tasks break on-foot sync scenes - Waiting for player to leave their vehicle.")
			REINIT_NET_TIMER(tdScriptedCutsceneTimer)
			EXIT
		ENDIF
	ENDIF
	
	INT iSyncSceneID = MC_ServerBD_1.iScriptedCutsceneSyncScene	
	PROCESS_POST_SHOWING_AND_HIDING_SYNC_SCENE_ENTITIES_FOR_SCRIPTED_CUTSCENE(iCutscene, pedCutscene)
	
	IF iSyncSceneID != -1
	AND IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_sCutsceneSyncSceneConversationProcessed)
		
		PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Processing every frame checks.")
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_READY_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE)		
			
		IF MC_playerBD[iLocalPart].iSynchSceneID > -1
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - I am running a sync scene - cleaning up")
			STOP_SYNC_SCENE(MC_playerBD[iPartToUse].iSynchSceneID)
			MC_playerBD[iLocalPart].iSynchSceneID = -1
		ENDIF
		
		IF bIsLocalPlayerHost		
			IF ARE_ALL_SYNC_SCENE_PARTICIPANTS_READY(iPlayers, pedCutscene, sCutsceneSyncSceneData.eType)
				IF NOT IS_SYNC_SCENE_RUNNING(iSyncSceneID)
					IF NOT IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_PlayedCutsceneSyncScene)
					AND NOT IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RunningCutsceneSyncSceneLocally)						
						PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Starting the Sync scene!!!")
						NETWORK_START_SYNCHRONISED_SCENE(iSyncSceneID)
						SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_PlayedCutsceneSyncScene)												
					ELSE
						PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Already Played the Sync Scene - Perhaps we're waiting for other clients to finish.")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		BOOL bShouldScriptedCutsceneBreakout		
		bShouldScriptedCutsceneBreakout = SHOULD_SCRIPTED_CUTSCENE_SYNC_SCENE_BREAKOUT(sCutsceneSyncSceneData.eType, pedCutscene, iSyncSceneID)
				
		IF IS_SYNC_SCENE_RUNNING(iSyncSceneID)
		AND NOT bShouldScriptedCutsceneBreakout
						
			IF NOT IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RunningCutsceneSyncSceneLocally)				
				PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Setting ciScriptedCutsceneBitset_RunningCutsceneSyncSceneLocally as running.")
				SET_PLAYER_IN_SCRIPTED_CUTSCENE(iCutscene)		
				SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RunningCutsceneSyncSceneLocally)	
				SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_PlayedCutsceneSyncScene)	
				CLEAR_BIT(sCurrentScriptedAnimConversationData.iBS, ciScriptedAnimConversationBS_StartPaused)				
				INIT_SCRIPTED_CUTSCENE_SYNC_SCENE_ENTITIES(iCutscene)
				UNPAUSE_RENDERPHASE_FOR_INTRO_CUTSCENE()
				FADE_IN_FOR_SYNC_SCENE_CUTSCENE()
			ENDIF
			
			FLOAT fPhase = GET_SYNC_SCENE_PHASE(iSyncSceneID)
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Anim Scene is running, fPhase: ", fPhase)			
			
			// Playing local sounds, one-shot.
			STRING sSoundName = GET_ONE_SHOT_SOUND_NAME_FOR_START_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneData.eType)
			STRING sSoundSet = GET_ONE_SHOT_SOUND_SET_FOR_START_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneData.eType)						
			FLOAT fSoundPhase = GET_ONE_SHOT_SOUND_PHASE_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneData.eType)
			STRING sSceneName = GET_SOUND_SCENE_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneData.eType)
			
			TRIGGER_START_MUSIC_EVENT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneData.eType)			
			
			PROCESS_SCRIPTED_CUTSCENE_SYNC_SCENE_HIGH_DOF(sCutsceneSyncSceneData.eType)			
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sSoundName)
			AND NOT IS_STRING_NULL_OR_EMPTY(sSoundSet)
			AND NOT IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_PlayedOneShotSound_0)
				IF fSoundPhase = 0.0
				OR fPhase > fSoundPhase
					PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Playing one-shot sound now. fPhase: ", fPhase, " fSoundPhase: ", fSoundPhase)
					PLAY_SOUND_FRONTEND(-1, sSoundName, sSoundSet)
					SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_PlayedOneShotSound_0)
				ENDIF
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sSceneName)			
			AND NOT IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_sCutsceneSyncSceneAudioScenePlayed)
				PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Playing Audio Scene: ", sSceneName)
				START_AUDIO_SCENE(sSceneName)
				SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_sCutsceneSyncSceneAudioScenePlayed)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet3, ci_CSBS3_SyncSceneBreakoutFadesCutscene)
			AND MC_ServerBD_1.fSyncSceneAnimDurationForPhaseFadeoutTime > 0.0			
				IF fPhase * MC_ServerBD_1.fSyncSceneAnimDurationForPhaseFadeoutTime >= (MC_ServerBD_1.fSyncSceneAnimDurationForPhaseFadeoutTime - 400)
					IF NOT IS_SCREEN_FADING_OUT()
					AND NOT IS_SCREEN_FADED_OUT()
						PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Doing an early fadeout breakout.")
						DO_SCREEN_FADE_OUT(250)
					ENDIF				
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_UsePlacedCameraInCustomSyncScene)
			AND NOT IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_ForceLocalUseofSyncSceneCam)
				PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Using Sync Scene Camera")
				NETWORK_FORCE_LOCAL_USE_OF_SYNCED_SCENE_CAMERA(iSyncSceneID)
				SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_ForceLocalUseofSyncSceneCam)
			ENDIF
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_UsePlacedCameraInCustomSyncScene)
			AND NOT IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RenderingSyncSceneCam)
				SET_CAM_ACTIVE(cutscenecam, FALSE)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RenderingSyncSceneCam)
				PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Turning off Script Cams.")
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_UsePlacedCameraInCustomSyncScene)			
			AND NOT IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RenderingSyncSceneCam)
				SET_CAM_ACTIVE(cutscenecam, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RenderingSyncSceneCam)
				PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Turning On Script Cams.")
			ENDIF
		ELSE
			IF IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RunningCutsceneSyncSceneLocally)
				IF bShouldScriptedCutsceneBreakout
					PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Finished/Breakout - Anim Scene hit BREAKOUT EVENT.")
				ELSE
					PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Finished/Breakout - Anim Scene is ending now or is not running..")
				ENDIF
				TRIGGER_STOP_MUSIC_EVENT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneData.eType)
				PROCESS_ANIM_SYNC_SCENE_FADEOUT_AND_PROGRESS_FOR_SCRIPTED_CUTSCENE(iCutscene, bProgress)
			ELIF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_FINISHED_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE)
				PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Waiting for scene to start")
				REINIT_NET_TIMER(tdScriptedCutsceneTimer)
			ENDIF
		ENDIF
		
		EXIT
		
	ELIF IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RenderingSyncSceneCam)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet3, ci_CSBS3_SyncSceneBreakoutEndsCutscene)
		SET_CAM_ACTIVE(cutscenecam, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		CLEAR_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RenderingSyncSceneCam)
		PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Anim Scene is no longer running. Turning On Script Cams (2).")
	ELSE
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_READY_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE)
			TRIGGER_STOP_MUSIC_EVENT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneData.eType)
			PROCESS_ANIM_SYNC_SCENE_FADEOUT_AND_PROGRESS_FOR_SCRIPTED_CUTSCENE(iCutscene, bProgress)
			
		ELIF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_FINISHED_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE)
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Waiting for scene to start")
			REINIT_NET_TIMER(tdScriptedCutsceneTimer)
		ELSE
			bProgress = TRUE
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Finished/Breakout - Anim Scene is no longer running. Server Scene Data lost. bProgress = TRUE")
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_PLAY_PLAYER_SPECIFIC_ANIMATIONS_FOR_SCENE(FMMC_CUTSCENE_PLAYER_SPECIFIC_DATA &sPlayerSpecificData[], INT iShot, PED_INDEX &pedCutscene[], BOOL bUseClones)
	
	STRING animDict, animName
	INT i
	FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1	
		IF bUseClones
		AND IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[0][i])
			PED_INDEX OriginalPlayerPed = GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[0][i])
			SET_ENTITY_LOCALLY_INVISIBLE(OriginalPlayerPed)	
			PRINTLN("[LM][Cutscenes] - PROCESS_PLAY_PLAYER_SPECIFIC_ANIMATIONS_FOR_SCENE - iPedCutscene: ", i, " Using clones, so, Hiding original ped.")
		ENDIF
		
		// Validity Checks
		IF IS_PED_INJURED(pedCutscene[i])
		OR NOT DOES_ENTITY_EXIST(pedCutscene[i])
			PRINTLN("[LM][Cutscenes] - PROCESS_PLAY_PLAYER_SPECIFIC_ANIMATIONS_FOR_SCENE - iPedCutscene: ", i, " is null.")
			RELOOP
		ENDIF
		
		IF pedCutscene[i] != localPlayerPed
		AND NOT bUseClones
			PRINTLN("[LM][Cutscenes] - PROCESS_PLAY_PLAYER_SPECIFIC_ANIMATIONS_FOR_SCENE - iPedCutscene: ", i, " is not ME!")
			RELOOP
		ENDIF
		
		// Load up
		animDict = GET_PLAYER_SPECIFIC_ANIMATION_DICT_FROM_CREATOR_INT(sPlayerSpecificData[i].iAnimation, i, IS_PED_FEMALE(pedCutscene[i]))
		IF NOT IS_STRING_NULL_OR_EMPTY(animDict)
			REQUEST_ANIM_DICT(animDict)
			IF NOT HAS_ANIM_DICT_LOADED(animDict)
				PRINTLN("[LM][Cutscenes] - PROCESS_PLAY_PLAYER_SPECIFIC_ANIMATIONS_FOR_SCENE - iPedCutscene: ", i, " Waiting for AnimDict: ", animDict, " to load.")
				RELOOP
			ENDIF
		ELSE
			PRINTLN("[LM][Cutscenes] - PROCESS_PLAY_PLAYER_SPECIFIC_ANIMATIONS_FOR_SCENE - iPedCutscene: ", i, " AnimDict Has not been set up. Is null.")
			RELOOP
		ENDIF
				
		// Ready to play!
		IF sPlayerSpecificData[i].iPlayAnimOnShot = iShot
		OR sPlayerSpecificData[i].iPlayAnimOnShot = -1
			animName = GET_PLAYER_SPECIFIC_ANIMATION_NAME_FROM_CREATOR_INT(sPlayerSpecificData[i].iAnimation, i)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(animName)
				IF NOT IS_ENTITY_PLAYING_ANIM(pedCutscene[i], animDict, animName)
				AND GET_SCRIPT_TASK_STATUS(pedCutscene[i], SCRIPT_TASK_PLAY_ANIM) != WAITING_TO_START_TASK
				AND GET_SCRIPT_TASK_STATUS(pedCutscene[i], SCRIPT_TASK_PLAY_ANIM) != PERFORMING_TASK
					
					#IF IS_DEBUG_BUILD
					VECTOR vTempPed = GET_ENTITY_COORDS(pedCutscene[i])
					PRINTLN("[LM][Cutscenes] - PROCESS_PLAY_PLAYER_SPECIFIC_ANIMATIONS_FOR_SCENE - iPedCutscene: ", i, " Playing animation: ", animName, " from animdict: ", animDict, " Position: ", vTempPed)
					#ENDIF
					
					CLEAR_PED_TASKS_IMMEDIATELY(pedCutscene[i])
					IF bUseClones
						FREEZE_ENTITY_POSITION(pedCutscene[i], TRUE)
					ENDIF
					ANIMATION_FLAGS animFlags = AF_NOT_INTERRUPTABLE | AF_FORCE_START | AF_USE_KINEMATIC_PHYSICS | AF_ABORT_ON_WEAPON_DAMAGE | AF_IGNORE_GRAVITY | AF_OVERRIDE_PHYSICS | AF_ABORT_ON_PED_MOVEMENT
					TASK_PLAY_ANIM(pedCutscene[i], animDict, animName, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, animFlags)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedCutscene[i])										
					SET_BIT(iLocalBoolCheck32, LBOOL32_SCRIPTED_CUTSCENE_PLAYING_ANIM)
					SET_BIT(iLocalBoolCheck32, LBOOL32_SCRIPTED_CUTSCENE_PLAYING_ANIM_WARM_UP)
				ENDIF
			ENDIF
		ELIF sPlayerSpecificData[i].iPlayAnimOnShot > -1
			PRINTLN("[LM][Cutscenes] - PROCESS_PLAY_PLAYER_SPECIFIC_ANIMATIONS_FOR_SCENE - iPedCutscene: ", i, " is waiting for shot: ", sPlayerSpecificData[i].iPlayAnimOnShot, " we are currently on: ", iCamShot)
		ENDIF
	ENDFOR
	
	
ENDPROC

FUNC STRING GET_RANDOM_CLOTHING_ANIMATION_FOR_CUTSCENE(TEXT_LABEL_63 &tlDict)
	STRING sAnim = ""
	INT i = iLocalPart
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 6)
	SWITCH i
		CASE 0
			SWITCH iRand
				CASE 0 		sAnim = "try_trousers_negative_a" 	tlDict = "Clothingtrousers"	BREAK
				CASE 1 		sAnim = "try_trousers_neutral_a" 	tlDict = "Clothingtrousers" BREAK
				CASE 2 		sAnim = "try_trousers_positive_a" 	tlDict = "Clothingtrousers" BREAK
				CASE 3 		sAnim = "try_tie_negative_a" 		tlDict = "Clothingtie"		BREAK
				CASE 4 		sAnim = "try_tie_neutral_a" 		tlDict = "Clothingtie"		BREAK
				CASE 5 		sAnim = "try_tie_positive_a" 		tlDict = "Clothingtie"		BREAK
			ENDSWITCH
		BREAK
			
		CASE 1
			SWITCH iRand
				CASE 0 		sAnim = "try_trousers_negative_b" 	tlDict = "Clothingtrousers" BREAK
				CASE 1 		sAnim = "try_trousers_neutral_b" 	tlDict = "Clothingtrousers" BREAK
				CASE 2 		sAnim = "try_trousers_positive_b" 	tlDict = "Clothingtrousers" BREAK
				CASE 3 		sAnim = "try_tie_negative_b" 		tlDict = "Clothingtie"		BREAK
				CASE 4 		sAnim = "try_tie_neutral_b" 		tlDict = "Clothingtie"		BREAK
				CASE 5 		sAnim = "try_tie_positive_b" 		tlDict = "Clothingtie"		BREAK
			ENDSWITCH
		BREAK
		
		CASE 2
			SWITCH iRand
				CASE 0 		sAnim = "try_trousers_negative_c" 	tlDict = "Clothingtrousers" BREAK
				CASE 1 		sAnim = "try_trousers_neutral_c"  	tlDict = "Clothingtrousers"	BREAK
				CASE 2 		sAnim = "try_trousers_positive_c" 	tlDict = "Clothingtrousers"	BREAK
				CASE 3 		sAnim = "try_tie_negative_c" 		tlDict = "Clothingtie"		BREAK
				CASE 4 		sAnim = "try_tie_neutral_c" 		tlDict = "Clothingtie"		BREAK
				CASE 5 		sAnim = "try_tie_positive_c" 		tlDict = "Clothingtie"		BREAK
			ENDSWITCH
		BREAK
		
		CASE 3
			SWITCH iRand
				CASE 0 		sAnim = "try_trousers_negative_d"  	tlDict = "Clothingtrousers"	BREAK
				CASE 1 		sAnim = "try_trousers_neutral_d" 	tlDict = "Clothingtrousers"	BREAK
				CASE 2 		sAnim = "try_trousers_positive_d" 	tlDict = "Clothingtrousers"	BREAK
				CASE 3 		sAnim = "try_tie_negative_d" 		tlDict = "Clothingtie"		BREAK
				CASE 4 		sAnim = "try_tie_neutral_d"  		tlDict = "Clothingtie"		BREAK
				CASE 5 		sAnim = "try_tie_positive_d"  		tlDict = "Clothingtie"		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN sAnim	
ENDFUNC

FUNC BOOL REQUEST_RELEASE_SCRIPTED_CUTSCENE_CANNED_SCENE(INT iAnimation, BOOL bRequest)

	IF iAnimation = ci_CS_ANIM_NONE
	OR iAnimation = ci_CS_ANIM_MAX
		ASSERTLN("REQUEST_SCRIPTED_CUTSCENE_CANNED_SCENE - iAnimation: ", iAnimation, " - not a valid canned animation")
		RETURN FALSE
	ENDIF

	// REQUEST (Load)
	IF bRequest
	
		SWITCH iAnimation
			
			CASE ci_CS_ANIM_APARTMENT_DOOR_ENTRY
				
				REQUEST_ANIM_DICT("anim@BUILDING_TRANS@HINGE_L")
				IF HAS_ANIM_DICT_LOADED("anim@BUILDING_TRANS@HINGE_L")
					RETURN TRUE
				ENDIF
			
			BREAK
			
			// No specific assets to load, return true
			DEFAULT
				RETURN TRUE
			BREAK
	
		ENDSWITCH

	// RELEASE (Unload)
	ELSE
	
		SWITCH iAnimation
			
			CASE ci_CS_ANIM_APARTMENT_DOOR_ENTRY
				REMOVE_ANIM_DICT("anim@BUILDING_TRANS@HINGE_L")
			BREAK
			
		ENDSWITCH
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Canned scene for apartment door entry
/// PARAMS:
///    iCutsceneID - 
PROC PROCESS_SCRIPTED_CUTSCENE_CANNED_SCENE_APARTMENT_DOOR_ENTRY(INT iCutsceneID)

	// Not running
	IF iScriptedCutsceneCannedSyncedScene = -1
	AND NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_SCRIPTED_CUTSCENE_SYNCSCENE_STARTED)

		MODEL_NAMES doorModel = INT_TO_ENUM(MODEL_NAMES, ci_CS_MODEL_APARTMENT_DOOR)
		OBJECT_INDEX doorProp = NULL
		
		INT iCutsceneEntityID
		REPEAT FMMC_MAX_CUTSCENE_ENTITIES iCutsceneEntityID
			FMMC_CUTSCENE_ENTITY sEntityData = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneID].sCutsceneEntities[iCutsceneEntityID]
			
			// no valid prop info
			IF sEntityData.iIndex < 0
			OR sEntityData.iIndex >= GET_FMMC_MAX_NUM_PROPS()
				RELOOP
			ENDIF
			
			// not a prop
			IF sEntityData.iType != CREATION_TYPE_PROPS
				RELOOP
			ENDIF
			
			// prop doesnt exit
			OBJECT_INDEX prop = oiProps[sEntityData.iIndex]
			IF NOT DOES_ENTITY_EXIST(prop)
				RELOOP
			ENDIF
			
			// not the correct model
			MODEL_NAMES model = GET_ENTITY_MODEL(prop)
			IF model != doorModel
				RELOOP
			ENDIF
			
			// Door prop found, break loop
			doorProp = prop
			BREAKLOOP
			
		ENDREPEAT
		
		IF NOT DOES_ENTITY_EXIST(doorProp)
			ASSERTLN("PROCESS_SCRIPTED_CUTSCENE_CANNED_SCENE_APARTMENT_DOOR_ENTRY - unable to find door prop, make sure you have added a suitable prop as a cutscene entity")
			EXIT
		ENDIF
		
		VECTOR sceneCoord = GET_ENTITY_COORDS(doorProp)
		VECTOR sceneRotation = GET_ENTITY_ROTATION(doorProp)
		sceneRotation.z += 90
	
		iScriptedCutsceneCannedSyncedScene = CREATE_SYNCHRONIZED_SCENE(sceneCoord, sceneRotation)
		SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iScriptedCutsceneCannedSyncedScene, FALSE)
		
		STRING strAnimDict = "anim@BUILDING_TRANS@HINGE_L"
		
		// Player anims
		INT iPlayerPed
		REPEAT FMMC_MAX_CUTSCENE_PLAYERS iPlayerPed
		
			PED_INDEX ped = MocapPlayerPed[iPlayerPed]
			IF NOT DOES_ENTITY_EXIST(ped)
				RELOOP
			ENDIF
			
			IF IS_PED_INJURED(ped)
				RELOOP
			ENDIF
			
			STRING strAnim
			SWITCH iPlayerPed
				CASE 0 strAnim = "ext_player" BREAK
				CASE 1 strAnim = "ext_a" BREAK
				CASE 2 strAnim = "ext_b" BREAK
				CASE 3 strAnim = "ext_c" BREAK
			ENDSWITCH
			TASK_SYNCHRONIZED_SCENE(ped, iScriptedCutsceneCannedSyncedScene, strAnimDict, strAnim, 
				INSTANT_BLEND_IN, INSTANT_BLEND_OUT, 
				SYNCED_SCENE_DONT_INTERRUPT)
		
		ENDREPEAT

		// Door anim
		PLAY_SYNCHRONIZED_ENTITY_ANIM(doorProp, iScriptedCutsceneCannedSyncedScene, "ext_door" , strAnimDict, 
			INSTANT_BLEND_IN, INSTANT_BLEND_OUT, 
			ENUM_TO_INT(SYNCED_SCENE_DONT_INTERRUPT))
			
		REPEAT FMMC_MAX_CUTSCENE_PLAYERS iPlayerPed
			PED_INDEX ped = MocapPlayerPed[iPlayerPed]
			IF NOT DOES_ENTITY_EXIST(ped)
				RELOOP
			ENDIF
			
			IF IS_PED_INJURED(ped)
				RELOOP
			ENDIF
		
			FORCE_PED_AI_AND_ANIMATION_UPDATE(ped)
		ENDREPEAT
		
		FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(doorProp)

		SET_BIT(iLocalBoolCheck27, LBOOL27_SCRIPTED_CUTSCENE_SYNCSCENE_STARTED)
	ENDIF

ENDPROC


PROC PROCESS_SCRIPTED_CUTSCENE_CANNED_SCENE_VTOL(INT iCutsceneID)

	IF iCutsceneID < 0 OR iCutsceneID >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
		EXIT
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		EXIT
	ENDIF
	
	VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
	IF NOT IS_VEHICLE_DRIVEABLE(veh)
		EXIT
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(veh)
		EXIT
	ENDIF
	
	MODEL_NAMES eModel = GET_ENTITY_MODEL(veh)
	IF NOT IS_THIS_MODEL_A_HELI(eModel)
	AND eModel != AVENGER
	AND eModel != TULA
	AND eModel != HYDRA
		EXIT
	ENDIF

	IF GET_VEHICLE_FLIGHT_NOZZLE_POSITION(veh) != 1.0
		EXIT
	ENDIF
	
	INT iCutSceneLength
	INT i
	FOR i = 0 TO g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneID].iNumCamShots
		iCutSceneLength += g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneID].iInterpTime[i]
	ENDFOR

	IF NOT HAS_NET_TIMER_STARTED(stCannedTVOL)
		REINIT_NET_TIMER(stCannedTVOL)
	ENDIF
	
	FLOAT fAlpha = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stCannedTVOL)) / TO_FLOAT(iCutSceneLength)
	FLOAT fEndPosZ = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneID].vPlayerEndPos[0].Z
	FLOAT fStartPosZ = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneID].vPlayerStartPos[0].Z
	FLOAT fTargetPosZ = INTERP_FLOAT(fStartPosZ, fEndPosZ, fAlpha, INTERPTYPE_DECEL)
	
	VECTOR vCurrentPos = GET_ENTITY_COORDS(veh, FALSE)
	FLOAT fDistFromTargetZ = fTargetPosZ - vCurrentPos.Z
	FLOAT fZVelToUse = INTERP_FLOAT(0, 10, CLAMP(fDistFromTargetZ, 0, 1))
	
	SET_ENTITY_VELOCITY(veh, <<0,0,fZVelToUse>>)
	
ENDPROC

/// PURPOSE:
///    Process playing any cutscenes that are orchestrated entirely by script with minimal paramatisation from the creator side.
/// PARAMS:
///    iCutsceneID - 
///    iAnimation - 
PROC PROCESS_SCRIPTED_CUTSCENE_CANNED_SCENE(INT iCutsceneID, INT iAnimation)
	// Wait for assets to load
	IF NOT REQUEST_RELEASE_SCRIPTED_CUTSCENE_CANNED_SCENE(iAnimation, TRUE)
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE_CANNED_SCENE - iCutsceneID: ", iCutsceneID, " - iAnimation: ", iAnimation, " - Waiting for assets to laod")
		EXIT
	ENDIF

	SWITCH iAnimation
		CASE ci_CS_ANIM_APARTMENT_DOOR_ENTRY
			PROCESS_SCRIPTED_CUTSCENE_CANNED_SCENE_APARTMENT_DOOR_ENTRY(iCutsceneID)
		BREAK
		CASE ci_CS_CANNED_VTOL
			PROCESS_SCRIPTED_CUTSCENE_CANNED_SCENE_VTOL(iCutsceneID)
		BREAK
		DEFAULT
			ASSERTLN("PROCESS_SCRIPTED_CUTSCENE_CANNED_SCENE - iAnimation: ", iAnimation, " - not a valid canned animation")
		BREAK
	ENDSWITCH

ENDPROC

PROC PROCESS_CUTSCENE_LIFT_FAKE_CLOSE_AND_MOVE_SFX(INT iCutsceneToUse, INT iShot, PED_INDEX &pedCutscene[])

	IF iShot = -1
	OR iShot >= MAX_CAMERA_SHOTS
		EXIT
	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iShot], ciCSS_BS_PlayCutsceneSFX_ElevatorCloseAndMoving)
	AND NOT IS_BIT_SET(bsSpecialCaseCutscene, BS_SCC_PLAYED_CUTSCENE_ELEVATOR_SFX)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(bsSpecialCaseCutscene, BS_SCC_PLAYED_CUTSCENE_ELEVATOR_SFX)
		SET_BIT(bsSpecialCaseCutscene, BS_SCC_PLAYED_CUTSCENE_ELEVATOR_SFX)
		PRINTLN("[Cutscene] - iCutsceneToUse: ", iCutsceneToUse, " Starting Elevator Fake SFX")
	ENDIF
	
	IF IS_VECTOR_ZERO(vPosNearestElevatorDoor)
		vPosNearestElevatorDoor = GET_NEAREST_DOOR_POSITION(GET_ENTITY_COORDS(pedCutscene[0], FALSE))	
		PRINTLN("[Cutscene] - iCutsceneToUse: ", iCutsceneToUse, " vPosNearestElevatorDoor:", vPosNearestElevatorDoor)
	ENDIF
		
	IF NOT IS_BIT_SET(bsSpecialCaseCutscene, BS_SCC_PLAYED_CUTSCENE_ELEVATOR_CLOSED)
		SET_BIT(bsSpecialCaseCutscene, BS_SCC_PLAYED_CUTSCENE_ELEVATOR_CLOSED)
		START_NET_TIMER(tdElevatorMovingDelay)
		PRINTLN("[Cutscene] - iCutsceneToUse: ", iCutsceneToUse, " Playing Fake Close and starting delay for 'moving'.")
		PLAY_SOUND_FROM_COORD(-1, "Fake_Close", vPosNearestElevatorDoor, "Union_Depository_Elevator_Sounds")		
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdElevatorMovingDelay, ciCUTSCENE_ELEVATOR_MOVING_DELAY)
		IF NOT IS_BIT_SET(bsSpecialCaseCutscene, BS_SCC_PLAYED_CUTSCENE_ELEVATOR_MOVING)
			SET_BIT(bsSpecialCaseCutscene, BS_SCC_PLAYED_CUTSCENE_ELEVATOR_MOVING)			
			PRINTLN("[Cutscene] - iCutsceneToUse: ", iCutsceneToUse, " Playing Fake Moving Elevator SFX.")
			iElevatorMovingSFX = GET_SOUND_ID()
			PLAY_SOUND_FROM_COORD(iElevatorMovingSFX, "Elevator_Moving", vPosNearestElevatorDoor, "Union_Depository_Elevator_Sounds")
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPTED_CUTSCENE(INT iCutsceneToUse, INT iTeam)
		
	BOOL bQuickWarp
	INT i
	PED_INDEX pedPlayers[FMMC_MAX_CUTSCENE_PLAYERS]
	PLAYER_INDEX piPlayers[FMMC_MAX_CUTSCENE_PLAYERS]
	INT iPlayers[FMMC_MAX_CUTSCENE_PLAYERS]
	
	BLOCK_INPUTS_FOR_CUTSCENE_PROCEDURES()
	
	INT iPlayerIndex = -1  
	IF ARE_TEAM_CUTSCENE_PLAYERS_SELECTED(iTeam)
		iPlayerIndex = GET_LOCAL_PLAYER_CUTSCENE_INDEX(iTeam)
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE - ARE_TEAM_CUTSCENE_PLAYERS_SELECTED iPlayerIndex = ", iPlayerIndex)
	ENDIF
	
	// fallback
	IF iPlayerIndex = -1
		iPlayerIndex = GET_PLAYER_FALLBACK_CUTSCENE_INDEX()
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE - iPlayerIndex = -1, using fallback index: ", iPlayerIndex)
	ENDIF
	
	// clamp
	IF iPlayerIndex < 0 OR iPlayerIndex >= FMMC_MAX_CUTSCENE_PLAYERS
		ASSERTLN("PROCESS_SCRIPTED_CUTSCENE - iPlayerIndex: ", iPlayerIndex, " is out of range 0 - FMMC_MAX_CUTSCENE_PLAYERS(", FMMC_MAX_CUTSCENE_PLAYERS, "), Clamping")
		iPlayerIndex = CLAMP_INT(iPlayerIndex, 0, FMMC_MAX_CUTSCENE_PLAYERS-1)
	ENDIF
	
	VECTOR vStartPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerStartPos[iPlayerIndex]
	FLOAT fStartPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerStartHeading[iPlayerIndex]
	IF IS_VECTOR_ZERO(vStartPos)
		vStartPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerStartPos[0]
		fStartPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerStartHeading[0]
	ENDIF	

	VECTOR vEndPos
	FLOAT fEndPos
	
	//Should override end pos with an alternative location?
	IF SHOULD_USE_ALT_END_POS(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].eUseAltEndPos)
		//Alt End Pos
		vEndPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerAltEndPos[iPlayerIndex]
		fEndPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerAltEndHeading[iPlayerIndex]	
			
		IF IS_VECTOR_ZERO(vEndPos)
			vEndPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerAltEndPos[0]
			fEndPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerAltEndHeading[0]
			PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - iPlayerIndex: ", iPlayerIndex, " vAltEndPos: ", vEndPos, " fallback to idx 0 used!")
		ELSE
			PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - iPlayerIndex: ", iPlayerIndex, " vAltEndPos: ", vEndPos, " used")
		ENDIF	
	ELSE
		//End Pos
		vEndPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerEndPos[iPlayerIndex]
		fEndPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerEndHeading[iPlayerIndex]	
			
		IF IS_VECTOR_ZERO(vEndPos)
			vEndPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerEndPos[0]
			fEndPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerEndHeading[0]
			PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - iPlayerIndex: ", iPlayerIndex, " vEndPos: ", vEndPos, " fallback to idx 0 used!")
		ELSE
			PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - iPlayerIndex: ", iPlayerIndex, " vEndPos: ", vEndPos, " used")
		ENDIF
	ENDIF
	
	VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REACHED_CUT_PROCESS_SHOTS)
		IF NOT IS_VECTOR_ZERO(vEndPos)
		AND VDIST2(vEndPos, vPlayerPos) <= POW(200.0, 2.0)
			PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - ADD_SCRIPTED_COVER_AREA - vEndPos is being used: ", vEndPos)	
			ADD_SCRIPTED_COVER_AREA(vEndPos, 15.0)	
		ENDIF
	ENDIF
	
	BOOL bUseClones = IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_Scripted_UsePlayerClones)
	
	IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_SCRIPTED_CUTSCENE_LOCALLY_HIDE_PLAYERS)
		FOR i = 0 to (FMMC_MAX_CUTSCENE_PLAYERS - 1)
			IF IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[iteam][i])
				SET_ENTITY_LOCALLY_INVISIBLE(GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[iteam][i]))
			ENDIF
		ENDFOR
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Allow_manual_control)
		IF GET_SCRIPTED_CUTSCENE_STATE_PROGRESS() > SCRIPTEDCUTPROG_WARPINTRO
			PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE DISABLE_HUD_FOR_MANUAL_CCTV()")
			DISABLE_HUD(TRUE, IS_BIT_SET(iLocalBoolCheck35, LBOOL35_PLAYED_CUTSCENE_SHOT_REACHED_TICKER))
		ENDIF
	ENDIF

	IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_PLAYED_CUTSCENE_SHOT_REACHED_TICKER)
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
	ENDIF

	SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
	IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Allow_manual_control)
	OR IS_USING_CUTSCENE_DRONE_SCALEFORM(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].eDroneUIPreset[iCamShot])
	OR IS_BIT_SET(iLocalBoolCheck35, LBOOL35_PLAYED_CUTSCENE_SHOT_REACHED_TICKER)
		IF GET_SCRIPTED_CUTSCENE_STATE_PROGRESS() > SCRIPTEDCUTPROG_WARPINTRO
			SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_HELP_TEXT)
			SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FLOATING_HELP_TEXT_1)
			SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FLOATING_HELP_TEXT_2)
		ENDIF
	ENDIF
	
	BOOL bForceCamshotChange
	
	IF eScriptedCutsceneProgress > SCRIPTEDCUTPROG_INIT
		BOOL bBail
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdSafetyTimerCutsceneLengthLimit, ciSAFETY_TIMER_CUTSCENES_LENGTH)
			PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE SAFETY TIMER HAS BEEN EXCEEDED (ciSAFETY_TIMER_CUTSCENES_LENGTH). SENDING TO CLEANUP.")
			IF eScriptedCutsceneProgress <= SCRIPTEDCUTPROG_PROCESS_SHOTS
				SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_WARPEND_LONG)
			ENDIF			
			#IF IS_DEBUG_BUILD
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_SCRIPTED_CUTSCENE_FORCED_TO_CLEANUP_FROM_STUCK, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "Scripted Cutscene was forced to cleanup (was stuck # ms)", ciSAFETY_TIMER_CUTSCENES_LENGTH)
			#ENDIF
			bBail = TRUE
		ELIF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdSafetyTimerCutsceneLengthLimit, ciSAFETY_TIMER_CUTSCENES_LENGTH-7500)			
			// For special progress shots.
			INT iShots = 0
			FOR iShots = 0 TO g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iNumCamShots-1				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iShots], ciCSS_BS_ThisShotProgressesSceneAfterInterpTime)
					RELOOP
				ENDIF
				IF iCamShot = iShots
					RELOOP
				ENDIF
				iCamShot = iShots
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iShots], ciCSS_BS_PlayCutsceneSFX_DroneSounds)
					CLEANUP_FAKE_FLIGHT_LOOP_SOUND()
				ENDIF
				OVERRIDE_CUTSCENE_ENDING_INTERP_TIME_FROM_CURRENT_CAM_SHOT(iCutsceneToUse)
				PLAY_CUTSCENE_SHOT_REACHED_TICKER(iCutsceneToUse)
				bForceCamshotChange = TRUE
				BREAKLOOP
			ENDFOR			
		ENDIF				
		IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_CUTSCENE_BAIL_FROM_MISSION_FAILURE)
			PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - LBOOL25_CUTSCENE_BAIL_FROM_MISSION_FAILURE is set, bailing from the Scripted Cutscene!!")
			bBail = TRUE
		ENDIF
		IF bBail
			IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_CUTSCENE_BAIL_FROM_MISSION_FAILURE)
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - LBOOL25_CUTSCENE_BAIL_FROM_MISSION_FAILURE is set, Calling TOGGLE_PAUSED_RENDERPHASES")
				TOGGLE_PAUSED_RENDERPHASES(FALSE)
			ENDIF
			SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_CLEANUP)
		ENDIF
	ENDIF
		
	ASSIGN_PLAYER_SPECIFIC_ANIMATIONS_PED_POOL(pedPlayers, piPlayers, iPlayers, bUseClones, iTeam)
	
	PROCESS_SHOWING_AND_HIDING_CLONES_FOR_SCRIPTED_CUTSCENE_LIFT(iCutsceneToUse, pedPlayers)
		
	eCutsceneTypePlaying = FMMCCUT_SCRIPTED
	iCutsceneIndexPlaying = iCutsceneToUse
			
	SWITCH eScriptedCutsceneProgress
		
		CASE SCRIPTEDCUTPROG_INIT
							
			IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
				VEHICLE_INDEX viVeh
				viVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				
				IF IS_VEHICLE_DRIVEABLE(viVeh)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(viVeh)
						PRINTLN("[LM] PROCESS_SCRIPTED_CUTSCENE - Preventing vehicle from stopping due to loss of player control (1)")	
						SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE(viVeh, FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_PlayRandomClothesAnim)
				REQUEST_ANIM_DICT("clothingtrousers")
				IF NOT HAS_ANIM_DICT_LOADED("clothingtrousers")
					EXIT
				ENDIF
				REQUEST_ANIM_DICT("clothingtie")
				IF NOT HAS_ANIM_DICT_LOADED("clothingtie")
					EXIT
				ENDIF
			ENDIF
			
			PRELOAD_FAKE_DRONE_SCALEFORM(iCutsceneToUse)
			
			IF NOT IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_sCutsceneSyncSceneDataGameplayAssigned)		
				sCutsceneSyncSceneDataGameplay = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sCutsceneSyncSceneData					
				sCutsceneSyncSceneDataGameplay.eType = GET_MODIFIED_CUTSCENE_SYNC_SCENE_TYPE_BASED_ON_GAMEPLAY(sCutsceneSyncSceneDataGameplay.eType)			
				SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_sCutsceneSyncSceneDataGameplayAssigned)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)
				IF NOT PRELOAD_CUSTOM_SYNC_SCENE_DICTIONARIES(sCutsceneSyncSceneDataGameplay)
					EXIT
				ENDIF
			ENDIF
						
			IF SHOULD_CUTSCENE_DELAY_CUTSCENE_START_BECAUSE_OF_ENTITY(iCutsceneToUse, FMMCCUT_SCRIPTED)			
				PRINTLN("[Cutscenes][Cutscene_Mocap] - PROCESS_SCRIPTED_CUTSCENE - WAITING FOR SHOULD_CUTSCENE_DELAY_CUTSCENE_START_BECAUSE_OF_ENTITY...")	
				EXIT
			ENDIF
			
			g_bInMissionControllerCutscene = TRUE
			CLEAR_BIT(iLocalBoolCheck27, LBOOL27_SCRIPTED_CUTSCENE_SYNCSCENE_STARTED)
			CLEAR_BIT(iLocalBoolCheck30, LBOOL30_PROCESSED_LEAVE_VEHICLE_FOR_CUTSCENE)
			
			//Store the last used vehicle for post warp
			viLastVehicleIndexBeforeCutscene = GET_PLAYERS_LAST_VEHICLE()
	
			IF DOES_THIS_CUTSCENE_USE_A_CCTV_FILTER( iCutsceneToUse )
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE Case 0 of a scripted cutscene - requesting CCTV assets" )
				
				REQUEST_CCTV_ASSETS()
				
				IF NOT HAVE_CCTV_ASSETS_LOADED()
					EXIT
				ENDIF
				
				SET_CCTV_LOCATION( g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].sCamName[0], g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].sLocation[0] )
				
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - CCTV assets loaded " )
			ELSE
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Case 0 of a scripted cutscene - no CCTV assets in this scripted cutscene" )
			ENDIF
						
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - ci_CSBS2_UseCustomSyncScene is set setting NETWORK_ALLOW_REMOTE_SYNCED_SCENE_LOCAL_PLAYER_REQUESTS to true")
				NETWORK_ALLOW_REMOTE_SYNCED_SCENE_LOCAL_PLAYER_REQUESTS(TRUE)
			ENDIF
			
			//Disable phone instructions - so we can see the CCTV controls
			g_FMMC_STRUCT.bDisablePhoneInstructions = TRUE
			g_TransitionSessionNonResetVars.contactRequests.bDisableContactRequestMenuNextFrame = TRUE
			
			SET_BIT( MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_STARTED_CUTSCENE )
												
			IF bIsAnySpectator
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_I_AM_SPECTATOR_WATCHING_CUTSCENE)
				PRINTLN("[Cutscene_Mocap] - PBBOOL2_I_AM_SPECTATOR_WATCHING_CUTSCENE = TRUE")
			ENDIF
			
			CLEAR_BIT(bsSpecialCaseCutscene,BS_SCC_MANUAL_CCTV)
			CLEAR_BIT(bsSpecialCaseCutscene,BS_SCC_ADD_MANUAL_CCTV_HELP)
			CLEAR_BIT(bsSpecialCaseCutscene,BS_SCC_RESET_ADAPTATION)
			iCutsceneID_GotoShot = -1
				
			PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - SETTING_CCTV_LOCATION")
			
			//Pre load the first cam shot area, but only if we're <100m away, or always if we're on a heist
			VECTOR vCamStartPos
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[0], ciCSS_BS_Attach_Enable)
				vCamStartPos = GET_ATTACH_CAMERA_VEHICLE_POSITION(iCutsceneToUse, 0)
			ELSE
				vCamStartPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vCamStartPos[0]
			ENDIF		
			
			BOOL bPreloading
			IF (NOT IS_VECTOR_ZERO(vCamStartPos) AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, vCamStartPos) <= 100)
			AND NOT IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_ScriptedCutsceneUseInterpTimeAsBlackScreenScene)
			AND NOT SHOULD_EXTERNAL_CUTSCENE_SEQUENCE_PLAY(iCutsceneToUse)
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - calling PRE_LOAD_CAM_SHOT_AREA. (0)")
				bPreloading = TRUE
				PRE_LOAD_CAM_SHOT_AREA(0, vCamStartPos, 100.0)
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED)
			AND bPreloading
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - EXITTING - this is because - LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED")
				EXIT
			ENDIF
			
			SET_IM_WATCHING_A_MOCAP()
			SET_BIT( iLocalBoolCheck5, LBOOL5_PED_INVOLVED_IN_CUTSCENE )
			SET_BIT( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE)
		
			//Load animations:
			IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation != 0
				REQUEST_RELEASE_SCRIPTED_CUTSCENE_CANNED_SCENE(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation, TRUE)
			ENDIF
			
			IF Is_MP_Objective_Text_On_Display()
				Clear_Any_Objective_Text()
			ENDIF
			
			REINIT_NET_TIMER(tdStartWarpSafetyTimer)
			
			CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(LocalPlayerPed), 10.0)
			
			SET_FRONTEND_ACTIVE(FALSE)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Lock_Player_Vehicle)
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) 
						SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), VEHICLELOCK_LOCKED)
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - LOCKING VEHICLE FOR CUTSCENE")
					ENDIF
				ENDIF
			ENDIF
			
			IF MC_playerBD[iPartToUse].iTeam = iScriptedCutsceneTeam								
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_REQUEST_CUTSCENE_PLAYERS)
				BROADCAST_CUTSCENE_PLAYER_REQUEST(MC_playerBD[iPartToUse].iteam, FMMCCUT_SCRIPTED) // IF NOT bIsAnySpectator?
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - [url:bugstar:5609784] - Setting PBBOOL_REQUEST_CUTSCENE_PLAYERS (2)")
			ENDIF
			
			IF SHOULD_SPECTATOR_FADE_OUT_FOR_CUTSCENE(iCutsceneToUse)
				DO_SCREEN_FADE_OUT(250)
				SET_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_FADES_DISABLED)
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Spectator or Dead. We should just fade out for this cutscene.")			
			ENDIF
			
			PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - IS_PED_INJURED: ", !bLocalPlayerPedOK, " IS_PLAYER_RESPAWNING: ", IS_PLAYER_RESPAWNING(LocalPlayer), " IS_LOCAL_PLAYER_ANY_SPECTATOR: ", IS_PLAYER_RESPAWNING(LocalPlayer))
			
			REINIT_NET_TIMER(tdSafetyTimerCutsceneLengthLimit)			
			
			SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_WAIT_PLAYER_SELECTION)
		BREAK
		
		CASE SCRIPTEDCUTPROG_WAIT_PLAYER_SELECTION
			
			BOOL bPlayersSelected, bPlayersCloned, bLiftCutsceneAssetsLoaded, bForceBail
			bPlayersSelected = ARE_TEAM_CUTSCENE_PLAYERS_SELECTED(iTeam)
			bPlayersCloned = CLONE_SCRIPTED_PLAYERS(iTeam, bUseClones, IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset3, ci_CSBS3_UseCloneOfOriginalPed)) 
			bLiftCutsceneAssetsLoaded = LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS(iCutsceneToUse)
			
			IF NOT bPlayersSelected
			AND (IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER) OR (IS_PED_INJURED(LocalPlayerPed) AND NOT bIsAnySpectator))
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - SBBOOL_MISSION_OVER and NOT bPlayersSelected - Force Bail.")
				bForceBail = TRUE
			ENDIF
			
			IF bPlayersSelected
			AND bPlayersCloned
			AND bLiftCutsceneAssetsLoaded
				
				IF IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_ScriptedCutsceneUseInterpTimeAsBlackScreenScene)
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - ci_CSBS3_ScriptedCutsceneUseInterpTimeAsBlackScreenScene is set, going straight to fadeout.")
					SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_FADEOUT_AND_WARP)
					EXIT
				ENDIF
								
				IF SHOULD_EXTERNAL_CUTSCENE_SEQUENCE_USE_FORCED_FADEOUT_AND_WARP(iCutsceneToUse)
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - SHOULD_EXTERNAL_CUTSCENE_SEQUENCE_PLAY is set, going straight to fadeout.")
					SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_FADEOUT_AND_WARP)
					EXIT
				ENDIF
				
				INT iCutsceneWarpRange
				iCutsceneWarpRange = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneWarpRange
				IF iCutsceneWarpRange = 0
					iCutsceneWarpRange = 125 // default range
				ENDIF
				
				IF NOT IS_VECTOR_ZERO(vStartPos)
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - start pos valid: ", vStartPos)
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, vStartPos) > iCutsceneWarpRange
					AND NOT (IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR) OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
						IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_SkipIfTooFar)
							PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - skiping as it's too far and ci_CSBS_SkipIfTooFar")
							SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_CLEANUP)
						ELSE
							PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Out of Range, or forcing to do a fadeout/warp.")
							SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_FADEOUT_AND_WARP)
						ENDIF
					ELIF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_WarpPlayersStart)
						SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_WARP_PLAYERS)
						IF IS_NEW_LOAD_SCENE_ACTIVE()
							NEW_LOAD_SCENE_STOP()
						ENDIF
					ELSE
						IF NOT (IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR) OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
							bWarpPlayerAfterScriptCameaSetup = TRUE
						ENDIF
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Going Straight to Warp 1.")
						SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_WARPINTRO)
					ENDIF
				ELSE
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - start pos NOT valid: ")
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset,ci_CSBS_SkipIfTooFar)
					AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed,g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].vCamStartPos[ 0 ]) > iCutsceneWarpRange
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE skiping as it's too far and ci_CSBS_SkipIfTooFar")
						SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_CLEANUP)
					ELSE
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Going Straight to Warp 2.")
						IF NOT (IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR) OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
							bWarpPlayerAfterScriptCameaSetup = TRUE
						ENDIF
						SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_WARPINTRO)
					ENDIF
				ENDIF
			
			ELIF bForceBail
				SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_CLEANUP)
			ENDIF
			
			IF bPlayersSelected
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - ARE_TEAM_CUTSCENE_PLAYERS_SELECTED = true)")
			ELSE
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - ARE_TEAM_CUTSCENE_PLAYERS_SELECTED = false")
			ENDIF			
			IF bPlayersCloned
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - CLONE_SCRIPTED_PLAYERS = true)")
			ELSE
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - CLONE_SCRIPTED_PLAYERS = false")
			ENDIF			
			IF bLiftCutsceneAssetsLoaded
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS = true)")
			ELSE
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS = false")
			ENDIF
		BREAK
		
		CASE SCRIPTEDCUTPROG_WARP_PLAYERS
			PROCESS_DEAD_PLAYERS_WATCHING_CUTSCENE()
			
			IF DOES_THIS_CUTSCENE_USE_A_CCTV_FILTER( iCutsceneToUse )
				REQUEST_CCTV_ASSETS()
			ENDIF
			
			//Load animations:
			IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation != 0
				REQUEST_RELEASE_SCRIPTED_CUTSCENE_CANNED_SCENE(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation, TRUE)
			ENDIF
			
			IF DOES_THIS_CUTSCENE_WARP_INTO_AN_INTERIOR(iCutsceneToUse, GET_ENTITY_COORDS(LocalPlayerPed), vStartPos)
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - SCRIPTEDCUTPROG_FADEOUT_AND_WARP - Setting bQuickWarp, iCutsceneToUse: ", iCutsceneToUse)
				bQuickWarp = TRUE
			ENDIF
			
			IF SHOULD_CUTSCENE_WAIT_FOR_CELEB_SCREEN_TO_FINISH()
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE Waiting before doing fadeout. (SHOULD_CUTSCENE_WAIT_FOR_CELEB_SCREEN_TO_FINISH)")
				
			ELIF bIsAnySpectator
			OR IS_VECTOR_ZERO(vStartPos)
			OR NET_WARP_TO_COORD(vStartPos, fStartPos, TRUE, FALSE, DEFAULT, DEFAULT, DEFAULT, bQuickWarp)	 					
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE Warp Successful.")
				SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_WARPINTRO)
			ENDIF
		BREAK
		
		CASE SCRIPTEDCUTPROG_FADEOUT_AND_WARP
			PROCESS_DEAD_PLAYERS_WATCHING_CUTSCENE()
			
			IF DOES_THIS_CUTSCENE_USE_A_CCTV_FILTER( iCutsceneToUse )
				REQUEST_CCTV_ASSETS()
			ENDIF
			
			//Load animations:
			IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation != 0
				REQUEST_RELEASE_SCRIPTED_CUTSCENE_CANNED_SCENE(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation, TRUE)
			ENDIF
			
			IF SHOULD_CUTSCENE_WAIT_FOR_CELEB_SCREEN_TO_FINISH()
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE Waiting before doing fadeout. (SHOULD_CUTSCENE_WAIT_FOR_CELEB_SCREEN_TO_FINISH)")
				
			ELIF NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(500)
				
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()
				ENDIF
				
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Doing fadeout.")
			ELSE

				IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet4, ci_CSBS4_PlayCutsceneSFX_TurnOnInteriorLights_CutStart)
					IF eLocalArtificialLightsState != eARTIFICIAL_LIGHTS_STATE__ON
						TURN_ON_ARTIFICIAL_LIGHTS_LOCALLY(FALSE)
					ENDIF
				ELIF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet4, ci_CSBS4_PlayCutsceneSFX_TurnOffInteriorLights_CutStart)
					IF eLocalArtificialLightsState != eARTIFICIAL_LIGHTS_STATE__OFF
						TURN_OFF_ARTIFICIAL_LIGHTS_LOCALLY(FALSE)
					ENDIF
				ENDIF
			
				IF NOT IS_ENTITY_ALIVE(LocalPlayerPed)
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE SCRIPTEDCUTPROG_FADEOUT_AND_WARP - Player is dead, waiting for respawn.")
					
				ELIF IS_SCREEN_FADED_OUT()
					#IF IS_DEBUG_BUILD
						VECTOR vPlayerTempPos
						vPlayerTempPos = GET_ENTITY_COORDS(LocalPlayerPed)
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE Player Coords: ", vPlayerTempPos, " Target vStartPos: ", vStartPos, " ci_CSBS2_RemoveFromVehEnd: ", IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_RemoveFromVehEnd) , "In Vehicle: ", IS_PED_IN_ANY_VEHICLE( LocalPlayerPed ))
					#ENDIF
					
					CLEAR_AREA_OF_PROJECTILES( vStartPos, 20.0, TRUE ) //to kill flares from smoking out the cutscene
										
					IF NOT bWarpPlayerAfterScriptCameaSetup
					AND SHOULD_CUTSCENE_REMOVE_PLAYER_FROM_VEHICLE_AT_START(iCutsceneToUse, TRUE)
						IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_PROCESSED_LEAVE_VEHICLE_FOR_CUTSCENE)
							PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Called: REMOVE_PLAYER_FROM_VEHICLE_AT_CUTSCENE_START.")
							REMOVE_PLAYER_FROM_VEHICLE_AT_CUTSCENE_START(iCutsceneToUse, TRUE)	
							SET_BIT(iLocalBoolCheck30, LBOOL30_PROCESSED_LEAVE_VEHICLE_FOR_CUTSCENE)
						ENDIF			
						IF IS_BIT_SET(iLocalBoolCheck30, LBOOL30_PROCESSED_LEAVE_VEHICLE_FOR_CUTSCENE)
						AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Waiting to leave the vehicle.")
							EXIT
						ENDIF
					ENDIF
					
					IF IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
					AND NOT NETWORK_HAS_CONTROL_OF_ENTITY( GET_VEHICLE_PED_IS_IN( LocalPlayerPed ) )
						IF GET_DISTANCE_BETWEEN_COORDS( vStartPos, GET_ENTITY_COORDS( LocalPlayerPed ) ) < 20
						OR GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdScriptedCutsceneTimer) > 5000
							PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE We're close enough Or timed out. Going to Warp.")
							SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_WARPINTRO)
						ENDIF
					ELSE
						IF DOES_THIS_CUTSCENE_WARP_INTO_AN_INTERIOR(iCutsceneToUse, GET_ENTITY_COORDS(LocalPlayerPed), vStartPos)
							PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - SCRIPTEDCUTPROG_FADEOUT_AND_WARP - Setting bQuickWarp, iCutsceneToUse: ", iCutsceneToUse)
							bQuickWarp = TRUE
						ENDIF

						IF bIsAnySpectator
						OR IS_VECTOR_ZERO(vStartPos)
						OR NET_WARP_TO_COORD(vStartPos, fStartPos, TRUE, FALSE, DEFAULT, DEFAULT, DEFAULT, bQuickWarp)	 					
							PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE Warp Successful.")
							SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_WARPINTRO)
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		BREAK		
		
		//Intro shot
		CASE SCRIPTEDCUTPROG_WARPINTRO
			
			IF IS_INTRO_CUTSCENE_RULE_ACTIVE()
				IF NOT IS_SKYSWOOP_AT_GROUND()
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Dont progress til skyswoop is down - ScriptedCutsceneProgress: ", GET_SCRIPTED_CUTSCENE_STATE_PROGRESS_NAME(GET_SCRIPTED_CUTSCENE_STATE_PROGRESS()))
					EXIT
				ENDIF
			ENDIF
			
			PROCESS_DEAD_PLAYERS_WATCHING_CUTSCENE()
			
			IF SHOULD_CUTSCENE_WAIT_FOR_CELEB_SCREEN_TO_FINISH()
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE Waiting for players to be resurrected and finished with early end celeb.. (SHOULD_CUTSCENE_WAIT_FOR_CELEB_SCREEN_TO_FINISH)")
				EXIT
			ENDIF
			
			TOGGLE_PLAYER_DAMAGE_OVERLAY(FALSE)
			
			SET_PLAYER_INVINCIBLE(LocalPlayer, TRUE)
			
			DISABLE_INTERACTION_MENU()
			
			DO_FOCUS_INTRO_CAM( iCutsceneToUse )
			
			DO_CUTSCENE_PHONE_INTRO( iCutsceneToUse )
			
			IF NOT bIsAnySpectator
				IF( IS_CUTSCENE_USING_HACKING_INTRO( iCutsceneToUse ) AND NOT IS_HACKING_INTRO_COMPLETE() AND HAS_THIS_PLAYER_COMPLETED_HACKING_MINGAME() )
					DISABLE_CONTROL_FOR_HACKING_INTRO()
				ELSE
					NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_PREVENT_EVERYBODY_BACKOFF|NSPC_LEAVE_CAMERA_CONTROL_ON|NSPC_ALLOW_PAD_SHAKE|NSPC_REENABLE_CONTROL_ON_DEATH)
				ENDIF
			ENDIF
			
			IF DOES_THIS_CUTSCENE_USE_A_CCTV_FILTER( iCutsceneToUse )
				IF NOT IS_BIT_SET(bsSpecialCaseCutscene, BS_SCC_MANUAL_CCTV)
					vCCTVRot[iCamShot] = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vCamStartRot[iCamShot]
					iCoolDownTimerForManualCCTV = GET_GAME_TIMER()
					SET_BIT(bsSpecialCaseCutscene, BS_SCC_MANUAL_CCTV)
				ENDIF
			ENDIF
			
			INIT_SCRIPTED_CUTSCENE_LIFT(iCutsceneToUse)
						
			PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - INSIDE CUTSCENE INTRO SECTION ScriptedCutsceneProgress: ", GET_SCRIPTED_CUTSCENE_STATE_PROGRESS_NAME(GET_SCRIPTED_CUTSCENE_STATE_PROGRESS()))
			
			//After Intro Completion
			IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO)
			AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_FOCUS_INTRO) 
			AND IS_BIT_SET(iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO)
				//Load animations:
				IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation != 0
					REQUEST_RELEASE_SCRIPTED_CUTSCENE_CANNED_SCENE(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation, TRUE)
				ENDIF
				
				SET_BIT(iLocalBoolCheck4, LBOOL4_SCRIPTED_CUT_NEEDS_CLEANED_UP)
				
				iLocalDialoguePriority = -1
				
				//Kill any existing hint cams
				IF IS_GAMEPLAY_HINT_ACTIVE()
					CPRINTLN( DEBUG_SIMON, "next shot" )
					KILL_CHASE_HINT_CAM(sIntroHintCam)
				ENDIF

				IF NOT DOES_CAM_EXIST(cutscenecam)			
					cutscenecam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",FALSE) 
				ENDIF
				
				IF DOES_CAM_EXIST(cutscenecam)	
					SET_CAM_ACTIVE(cutscenecam, TRUE)
					SETUP_CAM_PARAMS(iCutsceneToUse)
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[0], ciCSS_BS_PlayCutsceneSFX_DroneSounds)
						PLAY_FAKE_FLIGHT_LOOP_SOUND(GET_CAM_COORD(CutsceneCam))	
					ENDIF
					IF NOT SHOULD_CUSTOM_SYNC_SCENE_PREVENT_SCRIPT_CAMS_RENDERING_AT_START(sCutsceneSyncSceneDataGameplay, iCutsceneToUse)
					AND NOT SHOULD_EXTERNAL_CUTSCENE_SEQUENCE_PLAY(iCutsceneToUse)
					AND NOT SHOULD_SPECTATOR_FADE_OUT_FOR_CUTSCENE(iCutsceneToUse)
						// Maybe check camshot?
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Rendering Script Cams.")
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					ELSE
						SET_CAM_ACTIVE(cutscenecam, FALSE)
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet4, ci_CSBS4_CutsceneClearAreaAtEndOnly)
						CLEAR_CUTSCENE_AREA(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fClearRadius, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vClearCoOrds,g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vReplacementArea, TRUE, IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_ClearPersonalVehicles))
					ENDIF
				ENDIF
						
				IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Hide_Other_Players)
					CONCEAL_ALL_OTHER_PLAYERS(TRUE)
				ENDIF
				
				IF ANIMPOSTFX_IS_RUNNING("MP_Celeb_Preload_Fade")
					ANIMPOSTFX_STOP("MP_Celeb_Preload_Fade")
					PRINTLN("[Cutscene_Scripted] - [SAC] - PROCESS_SCRIPTED_CUTSCENE  MP_Celeb_Preload_Fade for mid mission cutscene.")
				ENDIF
				
				REINIT_NET_TIMER(tdScriptedCutsceneTimer)
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_ShowPlayerWeapon)
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)
					SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,FALSE)
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_EquipWeaponAtEnd)
					WEAPON_TYPE tempWeapon
					GET_CURRENT_PED_WEAPON(LocalPlayerPed, tempWeapon)
					IF tempWeapon != WEAPONTYPE_UNARMED
					AND tempWeapon != WEAPONTYPE_INVALID
						GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtPreCutsceneWeapon)
						PRINTLN("[Cutscene_Scripted] - PROCESS_SCRIPTED_CUTSCENE - Storing weapon ",GET_WEAPON_NAME(wtPreCutsceneWeapon), " to re-equip.")
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_SecurityCamFilter)
					IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_SECURITY_CAM_FILTER_USED)
						SET_BIT(iLocalBoolCheck7, LBOOL7_SECURITY_CAM_FILTER_USED)
					ENDIF
					
					g_FMMC_STRUCT.bSecurityCamActive = TRUE
					SET_TIMECYCLE_MODIFIER("CAMERA_secuirity")
					REQUEST_CCTV_ASSETS()
					IF HAVE_CCTV_ASSETS_LOADED()
						IF HAS_SCALEFORM_MOVIE_LOADED(SI_SecurityCam)
							SET_CCTV_LOCATION(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].sCamName[iCamShot],g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].sLocation[iCamShot])
							PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE SETTING LOCATION DATA FOR THIS SHOT")
						ENDIF
					ENDIF
					
					g_FMMC_STRUCT.bSecurityCamActive = TRUE
					SET_TIMECYCLE_MODIFIER("CAMERA_secuirity")
				ELIF IS_RULE_INDEX_VALID(GET_TEAM_CURRENT_RULE(iTeam))
				AND (SHOULD_FILTERS_BE_PROCESSED(iTeam, GET_TEAM_CURRENT_RULE(iTeam))
					OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[GET_TEAM_CURRENT_RULE(iTeam)].iFilterFadeOutDuration != 0)
					g_FMMC_STRUCT.bSecurityCamActive = FALSE
					PRINTLN("[Cutscene_Scripted][FILTERS] PROCESS_SCRIPTED_CUTSCENE Using rule based filter")
				ELSE
					g_FMMC_STRUCT.bSecurityCamActive = FALSE
					IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_TURNED_ON_LIGHTS_OFF_TC_MODIFIER)
						CLEAR_TIMECYCLE_MODIFIER()
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)
					SET_PLAYER_IN_SCRIPTED_CUTSCENE(iCutsceneToUse)
				ENDIF
				
				HIDE_SPECTATOR_HUD(TRUE)
				HIDE_SPECTATOR_BUTTONS(TRUE)
				
				IF MC_playerBD[iPartToUse].iteam = iScriptedCutsceneTeam
					IF iCamShot >= g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iMidPoint
					AND NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[iPartToUse].iteam], MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
						BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam],MC_playerBD[iPartToUse].iteam)
					ENDIF
					IF MC_serverBD.iSpawnScene != iCutsceneToUse
					OR MC_serverBD.iSpawnShot != iCamShot
						IF NOT HAS_NET_TIMER_STARTED(tdCutsceneEventDelay)
						OR HAS_NET_TIMER_EXPIRED(tdCutsceneEventDelay, ciCUTSCENE_EVENT_DELAY)
							REINIT_NET_TIMER(tdCutsceneEventDelay)
							BROADCAST_CUTSCENE_SPAWN_PROGRESS(iCutsceneToUse,iCamShot)
						ENDIF
					ENDIF
				ENDIF
				
				RESET_PER_SHOT_PHONE_INTRO()
				
				// [FMMC2020] This is such filth. Investigate a way of moving this into the previous stage, was kept in the 2020 controller based on some awful hacks that were made in the legacy controller.
				IF bWarpPlayerAfterScriptCameaSetup
					
					IF NOT SPECIAL_CUTSCENE_VEHICLE_WARP( iCutsceneToUse, iPlayerIndex )
						REMOVE_PLAYER_FROM_VEHICLE_AT_CUTSCENE_START(iCutsceneToUse, TRUE)
						
						BOOL bShouldPlayerUseWarp 
						bShouldPlayerUseWarp = TRUE				
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_RemoveFromVehStart)
						AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
						AND NOT NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(localPlayerPed))
							bShouldPlayerUseWarp = FALSE
						ENDIF
												
						BOOL bKeepVehicle
						bKeepVehicle = (NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_RemoveFromVehStart))
						
						IF IS_FAKE_MULTIPLAYER_MODE_SET()
							IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_RemoveFromVehStart) 
							AND IS_PED_IN_ANY_VEHICLE(localPlayerPed) OR (IS_PED_IN_ANY_VEHICLE(localPlayerPed) AND NOT IS_PED_SITTING_IN_ANY_VEHICLE(localPlayerPed))
								bKeepVehicle = FALSE
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_WarpPlayersStart)
						AND bShouldPlayerUseWarp
							IF NOT (IS_PED_IN_ANY_VEHICLE(PlayerPedToUse) AND NOT NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(PlayerPedToUse)))
								IF NET_WARP_TO_COORD(vStartPos, fStartPos, bKeepVehicle, FALSE, FALSE, FALSE, TRUE, TRUE)
									PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE SET TO WARP with bKeepVehicle: ", bKeepVehicle)
									PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE, start valid warping, ScriptedCutsceneProgress: ", GET_SCRIPTED_CUTSCENE_STATE_PROGRESS_NAME(GET_SCRIPTED_CUTSCENE_STATE_PROGRESS()))
									SET_BIT(iLocalBoolCheck29, LBOOL29_PROCESSED_SCRIPTED_CUTSCENE_LATE_WARP)
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE ScriptedCutsceneProgress: ", GET_SCRIPTED_CUTSCENE_STATE_PROGRESS_NAME(GET_SCRIPTED_CUTSCENE_STATE_PROGRESS()))
						ENDIF
					ENDIF
					SET_BIT(iLocalBoolCheck30, LBOOL30_PROCESSED_LEAVE_VEHICLE_FOR_CUTSCENE)
					bWarpPlayerAfterScriptCameaSetup = FALSE
				ENDIF
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_WarpPlayersStart)
				OR IS_BIT_SET(iLocalBoolCheck29, LBOOL29_PROCESSED_SCRIPTED_CUTSCENE_LATE_WARP)
				OR NOT bWarpPlayerAfterScriptCameaSetup
					IF bUseClones						
						PED_INDEX OriginalPlayerPed
						VECTOR vPlayer
						
						//Release the peds from whatever scene they are currently in
						STOP_SYNC_SCENE(iScriptedCutsceneCannedSyncedScene)
						iScriptedCutsceneCannedSyncedScene = -1
						
						SET_BIT(iLocalBoolCheck27, LBOOL27_SCRIPTED_CUTSCENE_LOCALLY_HIDE_PLAYERS)
						
						FOR i = 0 to (FMMC_MAX_CUTSCENE_PLAYERS - 1)
							IF IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[iteam][i])
								
								// Hide the players and show the clones!								
								OriginalPlayerPed = GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[iteam][i])
								SET_ENTITY_LOCALLY_INVISIBLE(OriginalPlayerPed)
								
								IF DOES_ENTITY_EXIST(MocapPlayerPed[i])
								AND NOT IS_PED_INJURED(MocapPlayerPed[i])

									IF iCutsceneToUse != -1
									AND IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_WarpPlayersStart)
										SET_ENTITY_COORDS(MocapPlayerPed[i], g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerStartPos[i])
										SET_ENTITY_HEADING(MocapPlayerPed[i], g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerStartHeading[i])
										
										PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Setting clone ", i, " at player start pos coords ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerStartPos[i], " with heading: ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerStartHeading[i])
									ELSE
										vPlayer = GET_ENTITY_COORDS(OriginalPlayerPed)
										SET_ENTITY_COORDS(MocapPlayerPed[i], vPlayer)
										SET_ENTITY_HEADING(MocapPlayerPed[i], GET_ENTITY_HEADING(OriginalPlayerPed))
										
										PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Setting clone ", i, " at player coords ",vPlayer)									
									ENDIF
									
									SET_ENTITY_VISIBLE(MocapPlayerPed[i], TRUE)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(MocapPlayerPed[i])
									
									IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_PlayRandomClothesAnim)
										STRING sAnim
										TEXT_LABEL_63 tlDict
										tlDict = ""
										sAnim = GET_RANDOM_CLOTHING_ANIMATION_FOR_CUTSCENE(tlDict)
										TASK_PLAY_ANIM(MocapPlayerPed[i], sAnim, tlDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_FORCE_START)
										
										PRINTLN("[LM][RandomClothesAnim] - iPed: ", i, " sAnim: ", sAnim, " tlDict: ", tlDict)
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(bsSpecialCaseCutscene,BS_SCC_ADD_MANUAL_CCTV_HELP)
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Allow_manual_control)
						SETUP_MANUAL_CCTV_HELP()
						SET_BIT(bsSpecialCaseCutscene,BS_SCC_ADD_MANUAL_CCTV_HELP)
					ENDIF
				ENDIF
				
				IF NOT IS_VECTOR_ZERO(vStartPos)
					
					VEHICLE_INDEX vehStartWarp
					vehStartWarp = GET_CUTSCENE_PED_WARP_VEHICLE(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sPlayerStartWarp[iPlayerIndex])
					IF DOES_ENTITY_EXIST(vehStartWarp)
						
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Player Start Vehicle Warp - Valid vStartPos: ", vStartPos)
					
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Player Start Vehicle Warp - Vehicle exists")
					
						IF NETWORK_HAS_CONTROL_OF_ENTITY(vehStartWarp)
							
							PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Player Start Vehicle Warp - Setting vehicle coords: ", vStartPos, ", heading", fStartPos)
							
							SET_ENTITY_COORDS(vehStartWarp, vStartPos, FALSE)
							SET_ENTITY_HEADING(vehStartWarp, fStartPos)
							
							FLOAT fGround
							IF NOT GET_GROUND_Z_FOR_3D_COORD(vStartPos, fGround, TRUE)
							OR vStartPos.Z - fGround > 2.0
							
								MODEL_NAMES eModel
								eModel = GET_ENTITY_MODEL(vehStartWarp)
								IF IS_THIS_MODEL_A_HELI(eModel)
								OR IS_THIS_MODEL_A_PLANE(eModel)
									SET_HELI_HOVERING(vehStartWarp)
								ENDIF
							
							ENDIF
							
						ENDIF
						
					ELSE
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Player Start Vehicle Warp - no vehicle target")
					ENDIF
				ELSE
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Player Start Vehicle Warp - Invalid start coords")
				ENDIF
				CUTSCENE_WARP_PED_TO_VEHICLE_COMMON(LocalPlayerPed, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sPlayerStartWarp[iPlayerIndex])
				
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE Warped and Setup.")
			
				IF IS_LOCAL_PLAYER_USING_DRONE()
					SET_DRONE_FORCE_EXTERNAL_CLEANING_UP(TRUE)
				ENDIF
				
				PROCESS_SCRIPTED_CUTSCENE_ATTACH_CLONES_EARLY(iCutsceneToUse, pedPlayers)
				
				SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_PROCESS_SHOTS)
			ENDIF
			
		BREAK
		
		//Process Shots
		CASE SCRIPTEDCUTPROG_PROCESS_SHOTS			
			
			INT iNumCamShots
			iNumCamShots = g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iNumCamShots
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_DontHidePersonalVehicles)
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)
				PROCESS_HIDE_PERSONAL_VEHICLES_LOCALLY_FOR_PLAYER()
			ENDIF
			
			PROCESS_SKIPPING_CUTSCENE(iCutsceneToUse, FMMCCUT_SCRIPTED)
			
			BOOL bProgress
			BOOL bSkip
			
			IF IS_BIT_SET(MC_ServerBD.iServerBitSet8, SBBOOL8_SKIP_CUTSCENE_TRIGGERED)
			
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - HOST HAS SKIPPED CUTSCENE!!")
				bProgress = TRUE
				bSkip = TRUE
				
			ENDIF
			
			IF NOT bSkip
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Running SCRIPTEDCUTPROG_PROCESS_SHOTS")
				
				IF SHOULD_SPECTATOR_FADE_OUT_FOR_CUTSCENE(iCutsceneToUse)
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)	
						SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_READY_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE)
					ENDIF

					IF NOT IS_SCREEN_FADED_OUT()
					AND NOT IS_SCREEN_FADING_OUT()
						DO_SCREEN_FADE_OUT(SCRIPTED_CUTSCENE_END_FADE_DURATION)
					ENDIF
						
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - SHOULD_SPECTATOR_FADE_OUT_FOR_CUTSCENE = TRUE")			
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)
								
					IF NOT IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_sCutsceneSyncSceneDataGameplayAssignedPostWarp)
						vehSeatForcedFromSyncScene = GET_FORCED_VEHICLE_SEAT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PLAYER(sCutsceneSyncSceneDataGameplay.eType)
						SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_sCutsceneSyncSceneDataGameplayAssignedPostWarp)					
					ENDIF
					
					IF SHOULD_WAIT_FOR_SYNC_SCENE_PARTICIPANTS(iPlayers)
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - SCRIPTEDCUTPROG_PROCESS_SHOTS - Waiting for SHOULD_WAIT_FOR_SYNC_SCENE_PARTICIPANTS")
						EXIT					
					ENDIF
						
					IF NOT HAS_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE_BEEN_CREATED_YET()
						
						IF bIsLocalPlayerHost
							CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE(sCutsceneSyncSceneDataGameplay, iCamShot, iCutsceneToUse, pedPlayers)
						ENDIF
						
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - SCRIPTEDCUTPROG_PROCESS_SHOTS - Exitting. Waiting for Sync Scene to be created.")
						REINIT_NET_TIMER(tdScriptedCutsceneTimer)
						
						EXIT
					ENDIF
					
					IF iCamShot = sCutsceneSyncSceneDataGameplay.iShotToPlayOn
					OR IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_PlayedCutsceneSyncScene)
					OR IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RunningCutsceneSyncSceneLocally)
					
						IF fScriptedCutsceneSyncSceneDuration = 0.0
						AND IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_OverrideCutsceneDurationWithSceneSync)
							FLOAT fStart
							FLOAT fEnd
							FLOAT fDurationMultiplier
							
							fStart = GET_PHASE_START_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneDataGameplay.eType)
							fEnd = GET_PHASE_END_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneDataGameplay.eType)					
							fDurationMultiplier = (fEnd - fStart)
							
							fScriptedCutsceneSyncSceneDuration = GET_ANIM_DURATION(GET_ANIM_DICT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneDataGameplay.eType, FALSE),
										GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PLAYER(sCutsceneSyncSceneDataGameplay.eType, 0, SHOULD_SCRIPTED_CUTSCENE_SCYN_SCENE_TYPE_USE_ALTERNATE_VARIATION(sCutsceneSyncSceneDataGameplay.eType, iCutsceneToUse, pedPlayers[0])))
										
							fScriptedCutsceneSyncSceneDuration *= 1000
							fScriptedCutsceneSyncSceneDuration *= fDurationMultiplier
							
							PRINTLN("[Cutscene_Scripted][ScriptedAnimSyncScene] - PROCESS_SCRIPTED_CUTSCENE - (PLAYER) Assigning Anim Duration. fScriptedCutsceneSyncSceneDuration: ", fScriptedCutsceneSyncSceneDuration, " fDurationMultiplier is ", fDurationMultiplier)	
						ENDIF	
					
						PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE(sCutsceneSyncSceneDataGameplay, iCamShot, iCutsceneToUse, pedPlayers, iPlayers, bProgress)						
					ELSE
						PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_SCRIPTED_CUTSCENE - Waiting for the correct Shot before starting sync scene. iCamShot: ", iCamShot, " iShotToPlayOn: ", sCutsceneSyncSceneDataGameplay.iShotToPlayOn)
					ENDIF
					
				ENDIF
				
				PROCESS_CUTSCENE_LIFT_FAKE_CLOSE_AND_MOVE_SFX(iCutsceneToUse, iCamShot, pedPlayers)
				
				IF iCamShot < iNumCamShots
				
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_PlayCutsceneSFX_TurnOnInteriorLights)
						IF eLocalArtificialLightsState != eARTIFICIAL_LIGHTS_STATE__ON
							TURN_ON_ARTIFICIAL_LIGHTS_LOCALLY(FALSE)
						ENDIF
					ELIF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_PlayCutsceneSFX_TurnOffInteriorLights)
						IF eLocalArtificialLightsState != eARTIFICIAL_LIGHTS_STATE__OFF
							TURN_OFF_ARTIFICIAL_LIGHTS_LOCALLY(FALSE)
						ENDIF
					ENDIF
					
					IF IS_CAM_ACTIVE(cutscenecam)
						DRAW_CUTSCENE_DRONE_SCALEFORM(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].eDroneUIPreset[iCamShot], cutscenecam)
					ENDIF
					
				ENDIF
				
				IF NOT SHOULD_EXTERNAL_CUTSCENE_SEQUENCE_PLAY(iCutsceneToUse)
				AND NOT IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_ScriptedCutsceneUseInterpTimeAsBlackScreenScene)
					PROCESS_PLAY_PLAYER_SPECIFIC_ANIMATIONS_FOR_SCENE(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sPlayerSpecificData, iCamShot, pedPlayers, bUseClones)								
					IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_SCRIPTED_CUTSCENE_PLAYING_ANIM_WARM_UP)
						CLEAR_BIT(iLocalBoolCheck32, LBOOL32_SCRIPTED_CUTSCENE_PLAYING_ANIM_WARM_UP)
						EXIT
					ENDIF
				ENDIF
				
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( MC_ServerBD.tdCutsceneStartTime[iScriptedCutsceneTeam], FALSE, TRUE ) < 1000
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE CURRENT NETWORK TIMER: ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE GET_NET_TIMER_DIFF: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_ServerBD.tdCutsceneStartTime[iScriptedCutsceneTeam], FALSE, TRUE), "[Cutscene_Scripted] is host: ", bIsLocalPlayerHost)				
					EXIT
				ENDIF
				
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REACHED_CUT_PROCESS_SHOTS)
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Allow_manual_control)
					IF NOT IS_BIT_SET(bsSpecialCaseCutscene,BS_SCC_ADD_MANUAL_CCTV_HELP)
						IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Allow_manual_control)
							SETUP_MANUAL_CCTV_HELP()
							SET_BIT(bsSpecialCaseCutscene,BS_SCC_ADD_MANUAL_CCTV_HELP)
						ENDIF
					ENDIF
					
					IF LOAD_MENU_ASSETS()
						SETUP_MANUAL_CCTV_HELP()
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE TRYING TO DRAW SCALEFORM HELP - 1846")
						INT iScreenX, iScreenY
						GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
						DRAW_MENU_HELP_SCALEFORM(iScreenX)
					ELSE
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE TRYING TO LOAD_MENU_ASSETS()")
					ENDIF
				ELSE
					PRINTLN("iCamShot: ",iCamShot)
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE MANUAL CCTV IS TURNED OFF")
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Interior_to_Exterior)
					IF NOT IS_BIT_SET(bsSpecialCaseCutscene,BS_SCC_RESET_ADAPTATION)
						RESET_ADAPTATION(1)
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE RESET_ADAPTATION CALLED")
						SET_BIT(bsSpecialCaseCutscene,BS_SCC_RESET_ADAPTATION)
					ENDIF
				ENDIF

				IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_DISABLE_CASINO_PEDS_DURING_CUTSCENE)
					g_bTurnOnMissionPenthousePartyPeds = FALSE
					PRINTLN("[Cutscene_Scripted] - g_bTurnOnMissionPenthousePartyPeds = FALSE")
					CLEAR_BIT(iLocalBoolCheck28, LBOOL28_DISABLE_CASINO_PEDS_DURING_CUTSCENE)
				ENDIF
					
				//Play animations / dialogue:
				IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation != 0
					PROCESS_SCRIPTED_CUTSCENE_CANNED_SCENE(iCutsceneToUse, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation)
				ENDIF
				
				ENTITY_INDEX EntID
				NETWORK_INDEX netID
				
				REPEAT FMMC_MAX_CUTSCENE_ENTITIES i
					IF NOT IS_BIT_SET( iScriptedEntitiesRegsitered, i )		
						IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iType !=-1
						AND g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iIndex != -1
							
							IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iType = CREATION_TYPE_PROPS
								EntID = GET_CUTSCNE_VISIBLE_PROP(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iIndex)
								IF EntID != NULL
									SET_ENTITY_VISIBLE(EntID, TRUE)
									SET_BIT( iScriptedEntitiesRegsitered, i )
								ENDIF
							ELSE
								netid = GET_CUTSCNE_VISIBLE_NET_ID(	g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iType,
																	g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iIndex)
								PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE REGISTER_CUTSCENE_ENTITIES - GET_CUTSCNE_VISIBLE_NET_ID has returned a thing: NULL = ",netid = NULL,", IS_CUTSCENE_PLAYING = ",IS_CUTSCENE_PLAYING())
								IF netid != NULL
									SET_NETWORK_ID_VISIBLE_IN_CUTSCENE_HACK( netID, TRUE, FALSE )
									SET_BIT( iScriptedEntitiesRegsitered, i )
								ENDIF
							ENDIF
							
						ELSE
							SET_BIT( iScriptedEntitiesRegsitered, i )
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF SHOULD_EXTERNAL_CUTSCENE_SEQUENCE_PLAY(iCutsceneToUse)
					IF bIsAnySpectator
					AND SHOULD_EXTERNAL_CUTSCENE_SEQUENCE_BE_SKIPPED_BY_SPECTATOR(iCutsceneToUse)
						PRINTLN("[ExternalCutscene][Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Spectator cannot play this External Cutscene. bProgress = TRUE")
						bProgress = TRUE
					ELIF PROCESS_EXTERNAL_CUTSCENE_SEQUENCE(iCutsceneToUse)
						PRINTLN("[ExternalCutscene][Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - External Cutscene has finished. bProgress = TRUE")
						bProgress = TRUE
					ELSE
						PRINTLN("[ExternalCutscene][Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Waiting for External Cutscene to Finish.")
					ENDIF
				ELIF IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_ScriptedCutsceneUseInterpTimeAsBlackScreenScene)
				
					IF NOT HAS_NET_TIMER_STARTED(tdScriptedCutsceneBlackScreenScene)
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Starting Black Screen Scene Timer, with interp time: ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpTime[0])
						START_NET_TIMER(tdScriptedCutsceneBlackScreenScene)
					ENDIF
					
					IF NOT IS_SCREEN_FADED_OUT()
					AND NOT IS_SCREEN_FADING_OUT()
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Fading out screen now!")
						DO_SCREEN_FADE_OUT(500)
					ENDIF
					
					IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdScriptedCutsceneBlackScreenScene, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpTime[0])
						bProgress = TRUE
					ENDIF
				ELSE
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_PlayRandomClothesAnim)
						STRING sAnim
						TEXT_LABEL_63 tlDict 
						sAnim = GET_RANDOM_CLOTHING_ANIMATION_FOR_CUTSCENE(tlDict)
						IF NOT HAS_NET_TIMER_STARTED(tdRandomClothingAnimDelay)
							START_NET_TIMER(tdRandomClothingAnimDelay)
						ENDIF
						IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_CUTSCENE_PLAYED_CLOTHING_ANIM)								
							FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
							TASK_PLAY_ANIM(LocalPlayerPed, tlDict, sAnim, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_FORCE_START)
							PRINTLN("[LM][RandomClothesAnim] - sAnim: ", sAnim, " tlDict: ", tlDict)
							SET_BIT(iLocalBoolCheck30, LBOOL30_CUTSCENE_PLAYED_CLOTHING_ANIM)
						ENDIF
						IF NOT IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, tlDict, sAnim, ANIM_DEFAULT)
						AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdRandomClothingAnimDelay, 4000)
							RESET_NET_TIMER(tdRandomClothingAnimDelay)
							CLEAR_BIT(iLocalBoolCheck30, LBOOL30_CUTSCENE_PLAYED_CLOTHING_ANIM)
						ENDIF
					ENDIF
								
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Allow_manual_control)
						
						IF iCamShot < iNumCamShots
							
							VECTOR vStreamPos
							FLOAT fStreamRadius
							
							IF( IS_BIT_SET( g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[ iCamShot + 1 ], ciCSS_BS_Attach_Enable ) )
								vStreamPos = GET_ATTACH_CAMERA_VEHICLE_POSITION( iCutsceneToUse, iCamShot + 1 )
							ELSE
								vStreamPos = g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].vStreamStartPos[ iCamShot + 1 ]
							ENDIF
							
							fStreamRadius = g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].fStreamRadius[ iCamShot + 1 ]

							//Pre load the next cam shot area
							IF ( iCamShot + 1 ) < iNumCamShots
								PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - calling PRE_LOAD_CAM_SHOT_AREA. (1)")
								PRE_LOAD_CAM_SHOT_AREA( iCamShot + 1, vStreamPos, fStreamRadius )
							ELSE
								// This seems to just get bad coordinates like -180 under the floor and causing us to load bad places for spectators.
								IF NOT bIsAnySpectator
									PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - calling PRE_LOAD_CAM_SHOT_AREA. (2)")
									PRE_LOAD_CAM_SHOT_AREA( iCamShot + 1, GET_ENTITY_COORDS(localPlayerPed), 50.0 )
								ENDIF
							ENDIF
								
							DO_PER_SHOT_PHONE_INTRO( iCutsceneToUse, iCamShot )
							
							INT iInterpTime
							iInterpTime = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpTime[iCamShot]
							
							IF fScriptedCutsceneSyncSceneDuration > 0.0
							AND IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_OverrideCutsceneDurationWithSceneSync)
								iInterpTime = ROUND(fScriptedCutsceneSyncSceneDuration)
							ENDIF

							IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_EarlyFinish)
								iInterpTime -= g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iEndEarlyTime[iCamShot]
								IF iInterpTime < 0
									PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - iEndEarlyTime > iInterpTime - Setting to 0")
									iInterpTime = 0
								ENDIF
							ENDIF
													
							PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - TimeElapsed: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdScriptedCutsceneTimer), " iInterpTime: ", iInterpTime)
							
							IF ((GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdScriptedCutsceneTimer) >= iInterpTime) 
								AND (NOT IS_CAM_INTERPOLATING(cutscenecam) OR IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_EarlyFinish)))
							OR IS_PHOTO_INTRO_DONE_FOR_SHOT( iCutsceneToUse, iCamShot ) // phone intro done for this shot
								
								iCamShot++
																
								IF MC_playerBD[iPartToUse].iteam = iScriptedCutsceneTeam
									IF NOT IS_BIT_SET( MC_serverBD.iObjectiveMidPointBitset[ MC_playerBD[iPartToUse].iteam ], MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] )
										IF iCamShot >= g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iMidPoint
											BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam],MC_playerBD[iPartToUse].iteam)
										ENDIF
									ENDIF
									IF MC_serverBD.iSpawnScene != iCutsceneToUse
									OR MC_serverBD.iSpawnShot != iCamShot
										IF NOT HAS_NET_TIMER_STARTED(tdCutsceneEventDelay)
										OR HAS_NET_TIMER_EXPIRED(tdCutsceneEventDelay, ciCUTSCENE_EVENT_DELAY)
											REINIT_NET_TIMER(tdCutsceneEventDelay)
											BROADCAST_CUTSCENE_SPAWN_PROGRESS(iCutsceneToUse,iCamShot)
										ENDIF
									ENDIF
								ENDIF
								
								IF iCamShot < iNumCamShots
										
									IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_PlayCutsceneSFX_DroneSounds)
										CLEANUP_FAKE_FLIGHT_LOOP_SOUND()
									ENDIF
									
									SETUP_CAM_PARAMS(iCutsceneToUse)										
									IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_SecurityCamFilter) 
										g_FMMC_STRUCT.bSecurityCamActive = TRUE
										SET_TIMECYCLE_MODIFIER("CAMERA_secuirity")
										REQUEST_CCTV_ASSETS()
										IF HAVE_CCTV_ASSETS_LOADED()
											IF HAS_SCALEFORM_MOVIE_LOADED(SI_SecurityCam)
												SET_CCTV_LOCATION(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].sCamName[iCamShot],g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].sLocation[iCamShot])
												PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE SETTING LOCATION DATA FOR THIS SHOT")
											ENDIF
										ENDIF
									ELIF IS_RULE_INDEX_VALID(GET_TEAM_CURRENT_RULE(iTeam))
									AND (SHOULD_FILTERS_BE_PROCESSED(iTeam, GET_TEAM_CURRENT_RULE(iTeam))
										OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[GET_TEAM_CURRENT_RULE(iTeam)].iFilterFadeOutDuration != 0)
										g_FMMC_STRUCT.bSecurityCamActive = FALSE
										PRINTLN("[Cutscene_Scripted][FILTERS] PROCESS_SCRIPTED_CUTSCENE Using rule based filter for this shot")
									ELSE
										g_FMMC_STRUCT.bSecurityCamActive = FALSE
										IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_TURNED_ON_LIGHTS_OFF_TC_MODIFIER)
											CLEAR_TIMECYCLE_MODIFIER()
										ENDIF
									ENDIF
									REINIT_NET_TIMER(tdScriptedCutsceneTimer)
									PLAY_CUTSCENE_SHOT_REACHED_TICKER(iCutsceneToUse)
								ELIF ((iCamShot - 1) > -1 AND IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot - 1], ciCSS_BS_EarlyFinish))
								AND IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_InterpBackToGameplay)
									PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Early Interp To Gameplay (Last Shot)")
									
									IF CAN_CUTSCENE_RESET_GAMEPLAY_CAMERA_PITCH_OR_HEADING(FMMCCUT_SCRIPTED, iCutsceneToUse)
										SET_GAMEPLAY_CAM_RELATIVE_HEADING()
										SET_GAMEPLAY_CAM_RELATIVE_PITCH()
									ENDIF
									RENDER_SCRIPT_CAMS(FALSE, IS_LOCAL_PLAYER_SAFE_TO_INTERP_BACK_TO_GAMEPLAY(), g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpBackToGameplayTime, NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_DontLockInterpSourceFrame), TRUE)
									SET_BIT(iLocalBoolCheck4, LBOOL4_SCRIPTED_CUT_RENDER_CAMS_CALLED)
								ENDIF
								
							ENDIF
							
							IF iCutsceneID_GotoShot < iCamShot
								IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_KeepPriorTasksOnShot)
									IF iCutsceneID_GotoShot < iCamShot
										PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Keep Tasks & set iCutsceneID_GotoShot to iCamShot ", iCamShot, ", iCutsceneToUse ",iCutsceneToUse)
										iCutsceneID_GotoShot = iCamShot
									ENDIF
									
								ELIF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_LocalPlayerDrive)
								AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vDriveToPosition[iCamShot])
								AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
									
									VEHICLE_INDEX playerVeh
									playerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE)
									PED_INDEX driverPed
									driverPed = GET_PED_IN_VEHICLE_SEAT(playerVeh, VS_DRIVER, TRUE)
									
									IF driverPed = LocalPlayerPed
										
										FLOAT fCruiseSpeed, fVehArriveRange
										DRIVINGMODE dmFlags
										TASK_GO_TO_COORD_ANY_MEANS_FLAGS tgcamFlags
										BOOL bLongRange
										
										IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fDriveSpeed[iCamShot] > 0.0
											fCruiseSpeed = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fDriveSpeed[iCamShot]
										ELSE
											fCruiseSpeed = 5.0
										ENDIF
										
										fVehArriveRange = 4.0
										dmFlags = DF_SwerveAroundAllCars | DF_SteerAroundStationaryCars | DF_SteerAroundObjects | DF_SteerAroundPeds | DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions | DF_DriveIntoOncomingTraffic | DF_UseSwitchedOffNodes | DF_AdjustCruiseSpeedBasedOnRoadSpeed | DF_ForceJoinInRoadDirection
										tgcamFlags = TGCAM_IGNORE_VEHICLE_HEALTH | TGCAM_REMAIN_IN_VEHICLE_AT_DESTINATION
										
										IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_LocalPlayerDrive_PreferNavmesh)
											PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Player drive task add DF_PreferNavmeshRoute for iCamShot ",iCamShot," in iCutsceneToUse ",iCutsceneToUse)
											dmFlags = dmFlags | DF_PreferNavmeshRoute
										ENDIF
										
										IF VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vDriveToPosition[iCamShot]) >= 90000 // If we need to travel more than 300m
											bLongRange = TRUE
										ENDIF
										
										PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Calling TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED w fCruiseSpeed ",fCruiseSpeed," bLongRange ",bLongRange,", to vDriveToPosition ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vDriveToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse)
										TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED(LocalPlayerPed, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vDriveToPosition[iCamShot], PEDMOVEBLENDRATIO_RUN, playerVeh, bLongRange, dmFlags, -1, 0, 10, tgcamFlags, fCruiseSpeed, fVehArriveRange)

										iCutsceneID_GotoShot = iCamShot
									ELSE
										PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - I'm not in the driver seat; vDriveToPosition ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vDriveToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse)
										iCutsceneID_GotoShot = iCamShot
									ENDIF
									
								ELIF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_LocalPlayerWalk)
								OR IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_LocalPlayerRun)
								OR IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_LocalPlayerSprint)
									FLOAT fPedMoveBlendRatio
									fPedMoveBlendRatio = PEDMOVEBLENDRATIO_WALK
									IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_LocalPlayerRun)
										fPedMoveBlendRatio = PEDMOVEBLENDRATIO_RUN
									ENDIF
									IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_LocalPlayerSprint)
										fPedMoveBlendRatio = PEDMOVEBLENDRATIO_SPRINT
									ENDIF
									
									IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot])
										IF NOT bUseClones									
											IF bLocalPlayerPedOK
												IF NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed, TRUE)
													IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_SCRIPTED_CUTSCENE_PLAYING_ANIM)											
														IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fWalkToPositionRadius[iCamShot] < 1.0										
															PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Calling TASK_GO_TO_COORD_ANY_MEANS on player to vWalkToPosition ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse, " fPedMoveBlendRatio: ", fPedMoveBlendRatio)
															TASK_GO_TO_COORD_ANY_MEANS(LocalPlayerPed, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot], fPedMoveBlendRatio, NULL)
														ELSE
															PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Calling TASK_GO_STRAIGHT_TO_COORD on player to vWalkToPosition ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse, " fPedMoveBlendRatio: ", fPedMoveBlendRatio, "fRadius = ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fWalkToPositionRadius[iCamShot])
															TASK_GO_STRAIGHT_TO_COORD(LocalPlayerPed, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot], fPedMoveBlendRatio, DEFAULT, DEFAULT, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fWalkToPositionRadius[iCamShot])
														ENDIF
													ELSE
														PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Cannot call TASK_GO_TO_COORD_ANY_MEANS on player to vWalkToPosition ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse, " PLAYER IS PERFORMING SPECIFIC ANIM...")
													ENDIF
												ELSE
													IF GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_LEAVE_VEHICLE) != WAITING_TO_START_TASK
													AND GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_LEAVE_VEHICLE) != PERFORMING_TASK
														PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Calling TASK_LEAVE_ANY_VEHICLE on local player ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse)
														TASK_LEAVE_ANY_VEHICLE(LocalPlayerPed, 0, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
													ELSE
														PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Waiting for player to have left the vehicle ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse)
													ENDIF
												ENDIF
											ELSE
												PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Cannot call TASK_GO_TO_COORD_ANY_MEANS on player to vWalkToPosition ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse, " PLAYER IS INJURED...")
											ENDIF
										ELSE
											FOR i = 0 to (FMMC_MAX_CUTSCENE_PLAYERS - 1)
												IF DOES_ENTITY_EXIST(MocapPlayerPed[i])
												AND NOT IS_PED_INJURED(MocapPlayerPed[i])
												OR IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_LocalPlayerRun)
												OR IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_LocalPlayerSprint)
													IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_SCRIPTED_CUTSCENE_PLAYING_ANIM)
													OR MocapPlayerPed[i] != LocalPlayerPed
														IF NOT IS_PED_IN_ANY_VEHICLE(MocapPlayerPed[i], TRUE)
															IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fWalkToPositionRadius[iCamShot] < 1.0
																PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - ped: ", i, " Calling TASK_GO_TO_COORD_ANY_MEANS on clone ",i," to vWalkToPosition ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse, " fPedMoveBlendRatio: ", fPedMoveBlendRatio)
																TASK_GO_TO_COORD_ANY_MEANS(MocapPlayerPed[i], g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot], fPedMoveBlendRatio, NULL)
															ELSE
																PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - ped: ", i, " Calling TASK_GO_STRAIGHT_TO_COORD on clone ",i," to vWalkToPosition ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse, " fPedMoveBlendRatio: ", fPedMoveBlendRatio, "fRadius = ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fWalkToPositionRadius[iCamShot])
																TASK_GO_STRAIGHT_TO_COORD(MocapPlayerPed[i], g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot], fPedMoveBlendRatio, DEFAULT, DEFAULT, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fWalkToPositionRadius[iCamShot])
															ENDIF												
														ELSE
															IF GET_SCRIPT_TASK_STATUS(MocapPlayerPed[i], SCRIPT_TASK_LEAVE_VEHICLE) != WAITING_TO_START_TASK
															AND GET_SCRIPT_TASK_STATUS(MocapPlayerPed[i], SCRIPT_TASK_LEAVE_VEHICLE) != PERFORMING_TASK
																PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - ped: ", i, " Calling TASK_LEAVE_ANY_VEHICLE on clone player ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse)
																TASK_LEAVE_ANY_VEHICLE(MocapPlayerPed[i], 0, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
															ELSE
																PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - ped: ", i, " Waiting for clone player to have left the vehicle ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse)
															ENDIF
														ENDIF
													ELSE
														PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - ped: ", i, " Preventing call to TASK_GO_STRAIGHT_TO_COORD - We are performing a player specific animation.")
													ENDIF
													
												ENDIF
											ENDFOR
										ENDIF
										
										iCutsceneID_GotoShot = iCamShot
									ENDIF
									
								ELSE
									IF iCutsceneID_GotoShot != -1								
										PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Call CLEAR_PED_TASKS & set iCutsceneID_GotoShot back to -1, iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse)
										
										IF NOT bUseClones
											IF bLocalPlayerPedOK
											AND NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_SCRIPTED_CUTSCENE_PLAYING_ANIM)
												CLEAR_PED_TASKS(LocalPlayerPed)
											ENDIF
										ELSE
											FOR i = 0 to (FMMC_MAX_CUTSCENE_PLAYERS - 1)
												IF DOES_ENTITY_EXIST(MocapPlayerPed[i])
												AND NOT IS_PED_INJURED(MocapPlayerPed[i])
													IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_SCRIPTED_CUTSCENE_PLAYING_ANIM)
													OR MocapPlayerPed[i] != LocalPlayerPed
														CLEAR_PED_TASKS(MocapPlayerPed[i])
													ENDIF
												ENDIF
											ENDFOR
										ENDIF
										
										iCutsceneID_GotoShot = -1
									ENDIF
								ENDIF
							ENDIF

							PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING(iCutsceneToUse, iCamShot, iPlayerIndex, pedPlayers)
							
						ELSE
							IF IS_BIT_SET(iCamSoundBitSet,1)
								PLAY_SOUND_FRONTEND(iCamSound,"Change_Cam","MP_CCTV_SOUNDSET", FALSE)
								SET_BIT(iCamSoundBitSet,4)
								iLocalCamSoundTimer = GET_GAME_TIMER()
								PRINTLN("[MANAGE_CAM_AUDIO] PROCESS_SCRIPTED_CUTSCENE - PLAYING FINAL CHANGE CAM")
							ENDIF
							
							LOAD_RANDOM_RE_ENTRY_INTERACTION_ANIM()
							
							PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE: Final shot has finished, progressing cutscene.")
							bProgress = TRUE
						ENDIF
						
						IF iCamShot = ( iNumCamShots - 1 )
							IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdScriptedCutsceneTimer) > (g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpTime[iCamShot] - 450)
								IF MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam] = MC_serverBD.iMaxObjectives[mc_playerBD[iPartToUse].iteam]
									IF NOT IS_THIS_A_HEIST_MISSION()
										IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
										AND CHECK_TO_SEE_IF_THIS_IS_THE_LAST_STRAND_MISSION()
											IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
												SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
												PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - [NETCELEBRATION] - [SAC] - ciCELEBRATION_BIT_SET_TriggerEndCelebration - call 3.")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF (IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_FadeForTransitionAtEnd) OR IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_AlwaysFadeForEndOfCutscene))
							AND g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iFadeBeforeEndTime > 0
								IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdScriptedCutsceneTimer) > (g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpTime[iCamShot] - (g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iFadeBeforeEndTime * 1000))
									IF NOT IS_SCREEN_FADED_OUT()
										IF NOT IS_SCREEN_FADING_OUT()
											PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - iCutsceneToUse: ", iCutsceneToUse, " Set to end faded out - fading (early).")
											DO_SCREEN_FADE_OUT(SCRIPTED_CUTSCENE_END_FADE_DURATION)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE

						//populate rotation
						IF IS_VECTOR_ZERO(vCCTVRot[iCamShot])
							IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vCamStartRot[iCamShot].z > 0
								vCCTVRot[iCamShot] = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vCamStartRot[iCamShot]
							ELSE
								g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vCamStartRot[iCamShot].z = 360 - g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vCamStartRot[iCamShot].z
								PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE MANUAL_CCTV CONTROL - Capping rotation value.")
							ENDIF
						ENDIF
						
						//populate FOV
						IF fCCTVFOV[iCamShot] = 0
							fCCTVFOV[iCamShot] = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fStartCamFOV[iCamShot]
						ENDIF
						
						UPDATE_CAM_PARAMS_FOR_CCTV(iCutsceneToUse,iCamShot)
						
						//Manage manual control of shots
						IF iSpectatorTarget = -1
							MANAGE_MANUAL_CCTV_CONTROL(iCutsceneToUse,iCamShot)
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciDISABLE_CINEMATIC_TOGGLE_CUTSCENE)
							PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE Option ciDISABLE_CINEMATIC_TOGGLE_CUTSCENE is enabled, Disabling cinematic button")
							SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
						ENDIF
						
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE MANUAL_CCTV CONTROL - CUTSCENE WILL RUN FOR: ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpTime[0]) 
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE MANUAL_CCTV CONTROL - iCamShot :  ", iCamShot) 
						
						//Change Cameras
						IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
						OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
						OR bForceCamshotChange
							IF MANAGE_HACKING_TIMER(iCoolDownTimerForManualCCTV,500)
								
								IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
								AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_ThisShotProgressesSceneAfterInterpTime)								
								OR bIsAnySpectator)
									IF iCamShot < iNumCamShots -1
										iCamShot++
									ELSE
										iCamShot = 0
									ENDIF
									
									IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_PlayCutsceneSFX_DroneSounds)
										CLEANUP_FAKE_FLIGHT_LOOP_SOUND()
									ENDIF
									
									OVERRIDE_CUTSCENE_ENDING_INTERP_TIME_FROM_CURRENT_CAM_SHOT(iCutsceneToUse)
									PLAY_CUTSCENE_SHOT_REACHED_TICKER(iCutsceneToUse)
								ENDIF
								
								IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
								AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_ThisShotProgressesSceneAfterInterpTime)
								OR bIsAnySpectator)
									IF iCamShot > 0
										iCamShot --
									ELSE
										iCamShot = iNumCamShots -1
									ENDIF
									
									IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_PlayCutsceneSFX_DroneSounds)
										CLEANUP_FAKE_FLIGHT_LOOP_SOUND()
									ENDIF
									
									OVERRIDE_CUTSCENE_ENDING_INTERP_TIME_FROM_CURRENT_CAM_SHOT(iCutsceneToUse)
									PLAY_CUTSCENE_SHOT_REACHED_TICKER(iCutsceneToUse)
								ENDIF
								
								PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE CHANGING CCTV SHOT")
								PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE CHANGING CCTV SHOT - iCamShot",iCamShot)
								PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE CHANGING CCTV SHOT - iNumCamShots",iNumCamShots)
								
								iCoolDownTimerForManualCCTV = GET_GAME_TIMER()
							ENDIF
																			
							IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_SecurityCamFilter)
								g_FMMC_STRUCT.bSecurityCamActive = TRUE
								SET_TIMECYCLE_MODIFIER("CAMERA_secuirity")
								REQUEST_CCTV_ASSETS()
								IF HAVE_CCTV_ASSETS_LOADED()
									IF HAS_SCALEFORM_MOVIE_LOADED(SI_SecurityCam)
										SET_CCTV_LOCATION(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].sCamName[iCamShot],g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].sLocation[iCamShot])
										PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE SETTING LOCATION DATA FOR THIS SHOT")
									ENDIF
								ENDIF
							ELIF IS_RULE_INDEX_VALID(GET_TEAM_CURRENT_RULE(iTeam))
							AND (SHOULD_FILTERS_BE_PROCESSED(iTeam, GET_TEAM_CURRENT_RULE(iTeam))
								OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[GET_TEAM_CURRENT_RULE(iTeam)].iFilterFadeOutDuration != 0)
								g_FMMC_STRUCT.bSecurityCamActive = FALSE
								PRINTLN("[Cutscene_Scripted][FILTERS] PROCESS_SCRIPTED_CUTSCENE Using rule based filter. Changing camera")
							ELSE
								g_FMMC_STRUCT.bSecurityCamActive = FALSE
								IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_TURNED_ON_LIGHTS_OFF_TC_MODIFIER)
									CLEAR_TIMECYCLE_MODIFIER()
								ENDIF
							ENDIF
						ENDIF
						
						//Custom progression logic
						IF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdScriptedCutsceneTimer) > g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpTime[0] AND g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpTime[0] > -1)
						OR (iCutsceneInterpTimeOverride > -1 AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdScriptedCutsceneTimer) > iCutsceneInterpTimeOverride)
							IF IS_BIT_SET(iCamSoundBitSet,1)
								PLAY_SOUND_FRONTEND(iCamSound,"Change_Cam","MP_CCTV_SOUNDSET", FALSE)
								SET_BIT(iCamSoundBitSet,4)
								iLocalCamSoundTimer = GET_GAME_TIMER()
								PRINTLN("[MANAGE_CAM_AUDIO] PROCESS_SCRIPTED_CUTSCENE - PLAYING FINAL CHANGE CAM")
							ENDIF
						
							LOAD_RANDOM_RE_ENTRY_INTERACTION_ANIM() // neilf. so the re-entry anims will be loaded by the time gameplay resumes.
							
							IF IS_NEW_LOAD_SCENE_ACTIVE()
								NEW_LOAD_SCENE_STOP()
							ENDIF
							
							IF NOT bIsAnySpectator
								NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
							ENDIF
							
							CLEAR_FOCUS()
							
							PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE Timer exceeded progressing.")
							bProgress = TRUE
						ENDIF
						
						VECTOR vStreamPos
						FLOAT fStreamRadius
						
						IF( IS_BIT_SET( g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[ iCamShot + 1 ], ciCSS_BS_Attach_Enable ) )
							vStreamPos = GET_ATTACH_CAMERA_VEHICLE_POSITION( iCutsceneToUse, iCamShot + 1 )
						ELSE
							IF iCamShot < iNumCamShots -1
								vStreamPos = g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].vStreamStartPos[ iCamShot + 1 ]
							ELSE
								vStreamPos = g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].vStreamStartPos[0]
							ENDIF
						ENDIF						
						IF iCamShot < iNumCamShots -1
							fStreamRadius = g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].fStreamRadius[ iCamShot + 1 ]
						ELSE
							fStreamRadius = g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].fStreamRadius[0]
						ENDIF										
						//Pre load the next cam shot area
						IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdScriptedCutsceneTimer) > (g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpTime[0] - 2000)
							IF iCamShot < iNumCamShots -1
								PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - calling PRE_LOAD_CAM_SHOT_AREA. (3)")
								PRE_LOAD_CAM_SHOT_AREA( iCamShot + 1, vStreamPos, fStreamRadius )
							ELSE
								PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - calling PRE_LOAD_CAM_SHOT_AREA. (4)")						
								PRE_LOAD_CAM_SHOT_AREA(0, vStreamPos, fStreamRadius )
							ENDIF
						ELSE
							PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - calling PRE_LOAD_CAM_SHOT_AREA. (5)")
							PRE_LOAD_CAM_SHOT_AREA( iCamShot + 1, GET_ENTITY_COORDS( playerPedToUse ), 25.0 )
						ENDIF
						
						DO_PER_SHOT_PHONE_INTRO( iCutsceneToUse, iCamShot )
						
						IF MC_playerBD[iPartToUse].iteam = iScriptedCutsceneTeam
							IF NOT IS_BIT_SET( MC_serverBD.iObjectiveMidPointBitset[ MC_playerBD[iPartToUse].iteam  ], MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] )
								IF iCamShot >= g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iMidPoint
									BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam],MC_playerBD[iPartToUse].iteam)
								ENDIF
							ENDIF
							IF MC_serverBD.iSpawnScene != iCutsceneToUse
							OR MC_serverBD.iSpawnShot != iCamShot
								IF NOT HAS_NET_TIMER_STARTED(tdCutsceneEventDelay)
								OR HAS_NET_TIMER_EXPIRED(tdCutsceneEventDelay, ciCUTSCENE_EVENT_DELAY)
									REINIT_NET_TIMER(tdCutsceneEventDelay)
									BROADCAST_CUTSCENE_SPAWN_PROGRESS(iCutsceneToUse,iCamShot)
								ENDIF
							ENDIF
						ENDIF
						
						IF iCamShot = ( iNumCamShots - 1 )
							IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdScriptedCutsceneTimer) > (g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpTime[iCamShot] - 450)
								IF MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam] = MC_serverBD.iMaxObjectives[mc_playerBD[iPartToUse].iteam]
									IF NOT IS_THIS_A_HEIST_MISSION()
										IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
											IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)										
												SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
												PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - [NETCELEBRATION] - [SAC] - ciCELEBRATION_BIT_SET_TriggerEndCelebration - call 4.")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF (IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_FadeForTransitionAtEnd) OR IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_AlwaysFadeForEndOfCutscene))
							AND g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iFadeBeforeEndTime > 0
								IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdScriptedCutsceneTimer) > (g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpTime[iCamShot] - (g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iFadeBeforeEndTime * 1000))
									IF NOT IS_SCREEN_FADED_OUT()
										IF NOT IS_SCREEN_FADING_OUT()
											PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - iCutsceneToUse: ", iCutsceneToUse, " Set to end faded out - fading (early).")
											DO_SCREEN_FADE_OUT(SCRIPTED_CUTSCENE_END_FADE_DURATION)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF				
					ENDIF
				ENDIF
			ENDIF
			 			
			IF bProgress
								
				IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)		
					IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_FINISHED_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE)
						SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_FINISHED_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE)
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - SCRIPTEDCUTPROG_PROCESS_SHOTS - Setting PBBOOL4_FINISHED_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE")
					ENDIF
					IF NOT ARE_ALL_SYNC_SCENE_PARTICIPANTS_FINISHED(iPlayers)
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - SCRIPTEDCUTPROG_PROCESS_SHOTS - Waiting for ARE_ALL_SYNC_SCENE_PARTICIPANTS_FINISHED")
						EXIT
					ELSE
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - SCRIPTEDCUTPROG_PROCESS_SHOTS - ARE_ALL_SYNC_SCENE_PARTICIPANTS_FINISHED All players finished.")
					ENDIF
				ENDIF
				
				IF bIsLocalPlayerHost
				
					IF MC_ServerBD_1.iScriptedCutsceneSyncScene > -1
						NETWORK_STOP_SYNCHRONISED_SCENE(MC_ServerBD_1.iScriptedCutsceneSyncScene)
						MC_ServerBD_1.iScriptedCutsceneSyncScene = -1
						PRINTLN("[LM][Cutscene_Scripted][ScriptedAnimSyncScene] - PROCESS_SCRIPTED_CUTSCENE - Cleaned up sync scene as we need to ensure it's safe to warp (host)...")
					ENDIF
					
				ENDIF	
								
				IF MC_ServerBD_1.iScriptedCutsceneSyncScene > -1
					PRINTLN("[LM][Cutscene_Scripted][ScriptedAnimSyncScene] - PROCESS_SCRIPTED_CUTSCENE - Waiting for Sync Scene to be finished.")
					EXIT
				ENDIF
				
				IF NOT PROCESS_CUTSCENE_FUNCTIONALITY_NEARLY_FINISHED(iCutsceneToUse, FMMCCUT_SCRIPTED, vEndPos, fEndPos, iPlayerIndex)
					EXIT
				ENDIF
				
				IF iCutsceneToUse != -1
				AND IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_UseFlyCamAfterCutscene_BeforeWarp)
					PROCESS_CAMERA_FLY_DOWN_AFTER_CUTSCENE(FMMCCUT_SCRIPTED, iCutsceneToUse, vEndPos, fEndPos)
				ENDIF
				
				// Intro Cutscenes have to be over a certain length otherwise they finish before getting to game state running, and cannot register it as started. 
				// It's too entangled in objective logic and staggered player loops to fix within the intro cutscene function.
				IF ( IS_INTRO_CUTSCENE_RULE_ACTIVE() #IF IS_DEBUG_BUILD OR bDebugForceCutsceneMinimumShotsProcessing #ENDIF )
				AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdSafetyTimerCutsceneLengthLimit, ciMINIMAL_CUTSCENE_LENGTH_FOR_OBJECTIVE_PROGRESSION)
					PRINTLN("[LM][Cutscene_Scripted] - PROCESS_SCRIPTED_CUTSCENE - CUTSCENE IS TOO SHORT.")
					#IF IS_DEBUG_BUILD
					ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_CUTSCENE_IS_TOO_SHORT_FOR_INTRO_SCENE, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "Cutscene Length is too short. Add Interp Time or delays in creator!")
					#ENDIF
					EXIT
				ENDIF
				
				//Unload animations:
				IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation != 0
					REQUEST_RELEASE_SCRIPTED_CUTSCENE_CANNED_SCENE(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation, FALSE)
				ENDIF
				
				IF IS_BIT_SET(iCamSoundBitSet,4)
					IF CHECK_IF_TIME_PASSED_IN_HAS_EXPIRED(iLocalCamSoundTimer,100)
						STOP_SOUND(iCamSound)
						PRINTLN("[MANAGE_CAM_AUDIO] PROCESS_SCRIPTED_CUTSCENE STOPPING CAM CHANGE SOUND")
						CLEAR_BIT(iCamSoundBitSet,4)
					ENDIF
				ENDIF
				
				VEHICLE_INDEX vehDummy
				GET_SCRIPTED_CUTSCENE_END_WARP_TARGET(iCutsceneToUse, iPlayerIndex, vEndPos, fEndPos, vehDummy)
								
				STOP_FAKE_MOVING_ELEVATOR_SFX()					
								
				IF SHOULD_WARP_PLAYER_TO_END_POSITION_DURING_CUTSCENE(iCutsceneToUse, FMMCCUT_SCRIPTED)
					
					SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_ENDING_PROCEDURES)
					
				ELIF NOT IS_VECTOR_ZERO(vEndPos)
				AND g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iVehicleIndexForPosWarp[0] = -1	
				AND NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
				AND (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, vEndPos) > 100 OR DOES_THIS_CUTSCENE_WARP_INTO_AN_INTERIOR(iCutsceneToUse, GET_ENTITY_COORDS(LocalPlayerPed), vEndPos))
					
					SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_WARPEND_LONG)
					
				ELSE
				
					SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_WARPEND_SHORT)
				
				ENDIF
				
			ENDIF

		BREAK
		
		CASE SCRIPTEDCUTPROG_WARPEND_SHORT
			
			IF PROCESS_SCRIPTED_CUTSCENE_END_WARP(iCutsceneToUse, iPlayerIndex, vEndPos, fEndPos, TRUE)
				
				IF eCSLift != eCSLift_None
				AND IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_Scripted_UsePlayerClones)
				AND g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fLiftZOffset_End = 0
					// The player is going to be inside the lift here, we need to call this a frame before turning off the scripted cam to make sure the gameplay cam has worked out any collision with the lift body
					PRINTLN("[Cutscene_Scripted][CSLift] PROCESS_SCRIPTED_CUTSCENE - Call SET_GAMEPLAY_CAM_RELATIVE_HEADING early to sort lift cam collision")
					IF CAN_CUTSCENE_RESET_GAMEPLAY_CAMERA_PITCH_OR_HEADING(FMMCCUT_SCRIPTED, iCutsceneToUse)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					ENDIF
				ENDIF
				
				SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_ENDING_PROCEDURES)
			ENDIF
			
		BREAK
		
		CASE SCRIPTEDCUTPROG_WARPEND_LONG
		
			IF NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(SCRIPTED_CUTSCENE_END_FADE_DURATION)
			ELSE
				IF IS_SCREEN_FADED_OUT()
					
					IF PROCESS_SCRIPTED_CUTSCENE_END_WARP(iCutsceneToUse, iPlayerIndex, vEndPos, fEndPos, FALSE)
						SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_ENDING_PROCEDURES)						
					ENDIF
	
				ENDIF
			ENDIF						
		BREAK
		
		CASE SCRIPTEDCUTPROG_ENDING_PROCEDURES
		
			IF IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_FadeForTransitionAtEnd)
			OR IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_AlwaysFadeForEndOfCutscene)
				IF NOT IS_SCREEN_FADED_OUT()
					IF NOT IS_SCREEN_FADING_OUT()
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - iCutsceneToUse: ", iCutsceneToUse, " Set to end faded out - fading.")
						DO_SCREEN_FADE_OUT(SCRIPTED_CUTSCENE_END_FADE_DURATION)
					ENDIF
					EXIT
				ENDIF
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - iCutsceneToUse: ", iCutsceneToUse, " Set to end faded out - done.")
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck35, LBOOL35_CLEARED_AREA_AT_THE_END_OF_CUTSCENE)
				IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet4, ci_CSBS4_CutsceneClearAreaAtEndOnly)
				OR IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet4, ci_CSBS4_CutsceneClearAreaAtStartAndEnd)
					CLEAR_CUTSCENE_AREA(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fClearRadius, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vClearCoOrds, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vReplacementArea, TRUE, IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_ClearPersonalVehicles))
					SET_BIT(iLocalBoolCheck35, LBOOL35_CLEARED_AREA_AT_THE_END_OF_CUTSCENE)
				ENDIF
			ENDIF
			
			IF NOT PROCESS_WARP_SELECTED_ENTITIES_AFTER_CUTSCENE(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sCutsceneEntityWarpData)					
				PRINTLN("[LM][Cutscene_Scripted][ScriptedAnimSyncScene] - PROCESS_SCRIPTED_CUTSCENE - Waiting for PROCESS_WARP_SELECTED_ENTITIES_AFTER_CUTSCENE to be finished.")
				EXIT
			ENDIF
				
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet4, ci_CSBS4_PlayCutsceneSFX_TurnOnInteriorLights_CutEnd)
				IF eLocalArtificialLightsState != eARTIFICIAL_LIGHTS_STATE__ON
					TURN_ON_ARTIFICIAL_LIGHTS_LOCALLY(FALSE)
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet4, ci_CSBS4_PlayCutsceneSFX_TurnOffInteriorLights_CutEnd)
				IF eLocalArtificialLightsState != eARTIFICIAL_LIGHTS_STATE__OFF
					TURN_OFF_ARTIFICIAL_LIGHTS_LOCALLY(FALSE)
				ENDIF
			ENDIF
				
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_EndCutsceneInCover)
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - iCutsceneToUse: ", iCutsceneToUse, " Set to end cutscene in cover.")			
				SET_PLAYER_INTO_COVER_AFTER_CUTSCENE(GET_PLAYERS_FACE_DIRECTION_IN_COVER(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet3, iPlayerIndex))
			ENDIF		
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_EndCutsceneRappelTask)
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_RUNNING_WALL_RAPPEL_NOT_ON_RAPPEL_RULE)
				PRINTLN("[WallRappel] PROCESS_SCRIPTED_CUTSCENE - Setting PBBOOL4_RUNNING_WALL_RAPPEL_NOT_ON_RAPPEL_RULE")
			ENDIF
			
			IF iCutsceneToUse != -1
			AND IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_UseFlyCamAfterCutscene_AfterWarp)
				PROCESS_CAMERA_FLY_DOWN_AFTER_CUTSCENE(FMMCCUT_SCRIPTED, iCutsceneToUse, vEndPos, fEndPos)
			ENDIF
			
			IF IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_SyncAllPlayersAtEnd)
			OR IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)	
				IF NOT IS_BIT_SET(MC_playerBD[iLocaLPart].iClientBitSet4, PBBOOL4_SYNC_PLAYERS_FOR_END_OF_CUTSCENE)
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - setting PBBOOL4_SYNC_PLAYERS_FOR_END_OF_CUTSCENE")
					SET_BIT(MC_playerBD[iLocaLPart].iClientBitSet4, PBBOOL4_SYNC_PLAYERS_FOR_END_OF_CUTSCENE)
				ENDIF
				
				IF NOT ARE_ALL_PLAYERS_SYNCED_FOR_END_OF_CUTSCENE()
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - iCutsceneToUse: ", iCutsceneToUse, ", exiting syncing players")
					EXIT
				ENDIF
			ENDIF
			
			SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_CLEANUP)
		BREAK
		
		CASE SCRIPTEDCUTPROG_CLEANUP
			BOOL bReadytoCleanup
			bReadytoCleanup = TRUE
			
			IF bIsAnySpectator			
				IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_PLAYING_CUTSCENE)
				AND iPartToUse != iLocalPart
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - iPartToUse: ", iPartToUse, " still watching cutscene, waiting before cleanup (PBBOOL_PLAYING_CUTSCENE)")
					bReadytoCleanup = FALSE
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck4, LBOOL4_SCRIPTED_CUT_RENDER_CAMS_CALLED)
			AND IS_INTERPOLATING_FROM_SCRIPT_CAMS()
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Waiting for camera to finish interping to gameplay")
				bReadytoCleanup = FALSE
			ENDIF
			
			IF bReadyToCleanup
				g_bInMissionControllerCutscene = FALSE
				
				iScriptedEntitiesRegsitered = 0
				iWarpCutsceneEntityBS = 0
				
				vehSeatForcedFromSyncScene = VS_ANY_PASSENGER
				
				TOGGLE_PLAYER_DAMAGE_OVERLAY(TRUE)
				SET_PLAYER_INVINCIBLE(LocalPlayer, FALSE)
				
				g_FMMC_STRUCT.bDisablePhoneInstructions = FALSE
				
				IF NETWORK_IS_IN_MP_CUTSCENE()
					SET_NETWORK_CUTSCENE_ENTITIES( FALSE )
				ENDIF
				
				IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sPlayerStartWarp[iPlayerIndex].iVehID > -1
					HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS(
						iScriptedCutsceneTeam, 
						g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sPlayerStartWarp[iPlayerIndex].iVehID, 
						g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sPlayerStartWarp[iPlayerIndex].iVehSeat)
				ELIF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sPlayerEndWarp[iPlayerIndex].iVehID > -1
					HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS(
						iScriptedCutsceneTeam, 
						g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sPlayerEndWarp[iPlayerIndex].iVehID, 
						g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sPlayerEndWarp[iPlayerIndex].iVehSeat)
				ENDIF
				
				SET_BIT(MC_playerBD[iLocalPart].iCutsceneFinishedBitset, g_iFMMCScriptedCutscenePlaying)
								
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE)
				
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_SYNC_PLAYERS_FOR_END_OF_CUTSCENE)
				
				CLEAR_BIT(iLocalBoolCheck35, LBOOL35_CLEARED_AREA_AT_THE_END_OF_CUTSCENE)
				
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE Setting cutscene as finished: ", g_iFMMCScriptedCutscenePlaying)
				
				RESET_NET_TIMER(tdEnterCoverEndingCutsceneTimeOut)
				RESET_NET_TIMER(tdScriptedCutsceneBlackScreenScene)
				
				TURN_ON_CUTSCENE_VEHICLE_RADIO()
				
				CLEANUP_FAKE_FLIGHT_LOOP_SOUND()
				
				CLEANUP_FAKE_DRONE_SCALEFORM()
				
				IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_SECURITY_CAM_FILTER_USED)
					REFRESH_SPEC_HUD_FILTER()
					CLEAR_BIT(iLocalBoolCheck7, LBOOL7_SECURITY_CAM_FILTER_USED)
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Hide_Other_Players)
					CONCEAL_ALL_OTHER_PLAYERS(FALSE)
				ELIF bUseClones
					FOR i = 0 to (FMMC_MAX_CUTSCENE_PLAYERS - 1)
						IF IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[iteam][i])
							SET_ENTITY_LOCALLY_VISIBLE(GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[iteam][i]))
						ENDIF
					ENDFOR
					CLEAR_BIT(iLocalBoolCheck27, LBOOL27_SCRIPTED_CUTSCENE_LOCALLY_HIDE_PLAYERS)
					CLEANUP_MOCAP_PLAYER_CLONES()
				ENDIF
				
				RESET_NET_TIMER(tdScriptedCutsceneSyncSceneSafetyTimer)
				RESET_NET_TIMER(tdScriptedCutsceneSyncSceneSafetyTimer2)
				RESET_NET_TIMER(tdSafetyTimerExternalCutscenes)
				
				CLEAR_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_FADES_DISABLED)
				CLEAR_BIT(iLocalBoolCheck5,LBOOL5_PED_INVOLVED_IN_CUTSCENE)
				CLEAR_BIT(iLocalBoolCheck32, LBOOL32_SCRIPTED_CUTSCENE_PLAYING_ANIM)				
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_READY_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE)
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_FINISHED_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE)
						
				IF bIsLocalPlayerHost
					CLEAR_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_CREATED_SCRIPTED_CUTSCENE_SYNC_SCENE)	
				ENDIF
				
				//Phone Intro
				CLEAR_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO)
				CLEAR_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_READY_FOR_PHONE_INTRO)
				
				//Focus Intro
				CLEAR_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_FOCUS_INTRO)
				CLEAR_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_FOCUS_INTRO)
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_FINISHED_CUTSCENE)
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_REQUEST_CUTSCENE_PLAYERS)
				
				// This will hide the HUD after a cutscene until godtext has been shown again
				CLEAR_BIT(iLocalBoolCheck9, LBOOL9_HAS_FIRST_OBJECTIVE_TEXT_BEEN_SHOWN)
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REACHED_CUT_PROCESS_SHOTS)
				
				CLEAR_BIT(iLocalBoolCheck35, LBOOL35_ATTACHED_CLONES_FOR_LIFT_SCENE)
				
				IF bIsLocalPlayerHost
					#IF IS_DEBUG_BUILD
					IF IS_BIT_SET(MC_serverBD.iServerBitset8, SBBOOL8_PREVENT_CUTSCENE_PRIMARY_PLAYER_FROM_BEING_ADDED)
						PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Clearing SBBOOL8_PREVENT_CUTSCENE_PRIMARY_PLAYER_FROM_BEING_ADDED")
					ENDIF
					#ENDIF
					CLEAR_BIT(MC_serverBD.iServerBitset8, SBBOOL8_PREVENT_CUTSCENE_PRIMARY_PLAYER_FROM_BEING_ADDED)
					
					MC_ServerBD_1.fSyncSceneAnimDurationForPhaseFadeoutTime = 0.0
				ENDIF				
				
				//Stop any active load scenes
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Calling NEW_LOAD_SCENE_STOP")
					NEW_LOAD_SCENE_STOP()
				ENDIF
								
				IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)
					TRIGGER_STOP_MUSIC_EVENT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneDataGameplay.eType)
					UNLOAD_CUSTOM_SYNC_SCENE_DICTIONARIES(sCutsceneSyncSceneDataGameplay)
					UNLOAD_CUSTOM_SYNC_SCENE_AUDIO_SCENE(sCutsceneSyncSceneDataGameplay)
				ENDIF
				
				RESET_NET_TIMER(tdCutsceneWaitForEntitiesTimer)
				RESET_NET_TIMER(tdElevatorMovingDelay)
				vPosNearestElevatorDoor = <<0.0, 0.0, 0.0>>
				STOP_FAKE_MOVING_ELEVATOR_SFX()
				STOP_SCRIPTED_CUTSCENE_LIFT_AUDIO__MOVE_LOOP()
				
				SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_WaitForStart)		
				UNLOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS(GET_SCRIPTED_CUTSCENE_LIFT_COORDS(eCSLift))
				
				//Pre load cutscene shots
				iCamShotLoaded = -1
				CLEAR_BIT(iLocalBoolCheck7, LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED)
				iCutsceneInterpTimeOverride = -1
				iPhoneIntroDoneForShot = -1
				CLEAR_IM_WATCHING_A_MOCAP()
				
				IF NOT IS_BIT_SET(iLocalBoolCheck4, LBOOL4_SCRIPTED_CUT_RENDER_CAMS_CALLED)
				AND CAN_CUTSCENE_RESET_GAMEPLAY_CAMERA_PITCH_OR_HEADING(FMMCCUT_SCRIPTED, iCutsceneToUse)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				ENDIF
				
				CLEAR_BIT(iLocalBoolCheck35, LBOOL35_PLAYED_CUTSCENE_SHOT_REACHED_TICKER)
				iCutsceneInterpTimeOverride = -1
				bsSpecialCaseCutscene = 0
				iScriptedCutsceneSyncSceneBS = 0
				fScriptedCutsceneSyncSceneDuration = 0.0
				iScriptedCutsceneSimpleInteriorSyncScene = -1
				iExternalCutsceneBitset = 0
				iPedFakePlayersBitset = 0
				
				SIMPLE_CUTSCENE_CLEAR_ALL_DATA(g_SimpleInteriorData.sCustomCutsceneExt)
				CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iBS, BS_SIMPLE_CUTSCENE_GLOBAL_PLAYER_BD_PLAYING)
				GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iCutsceneHash = 0
								
				IF bIsLocalPlayerHost
					MC_ServerBD_1.iScriptedCutsceneSyncSceneInvisibilityBS = 0
					
					IF MC_ServerBD_1.iScriptedCutsceneSyncScene > -1
						NETWORK_STOP_SYNCHRONISED_SCENE(MC_ServerBD_1.iScriptedCutsceneSyncScene)
						MC_ServerBD_1.iScriptedCutsceneSyncScene = -1
						PRINTLN("[LM][Cutscene_Scripted][ScriptedAnimSyncScene] - PROCESS_SCRIPTED_CUTSCENE - Cleaned up sync scene during cutscene cleanup (host)...")
					ENDIF
				ENDIF	
				
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					VEHICLE_INDEX vehPlayer
					vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
					IF IS_VEHICLE_DRIVEABLE(vehPlayer)
					AND NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
					AND IS_ENTITY_A_MISSION_ENTITY(vehPlayer)
						SET_VEHICLE_LIMIT_SPEED_WHEN_PLAYER_INACTIVE(vehPlayer, TRUE)
						SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE(vehPlayer, TRUE)
					ENDIF
				ENDIF
				
				MC_STOP_NET_TIMER_WITH_TIMESTAMP(iCutsceneWaitForEntitiesSafetyTimeStamp)	
				
				CLEANUP_CUTSCENE_BINK_AND_RENDERTARGET()
				
				UNPAUSE_RENDERPHASE_FOR_INTRO_CUTSCENE()
				
				NETWORK_ALLOW_REMOTE_SYNCED_SCENE_LOCAL_PLAYER_REQUESTS(FALSE)
				
				CLEANUP_SCRIPTED_CUTSCENE(FALSE, 
							IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_InterpBackToGameplay),
							NOT IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_FadeForTransitionAtEnd),
							IS_BIT_SET(iLocalBoolCheck4, LBOOL4_SCRIPTED_CUT_RENDER_CAMS_CALLED),
							g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpBackToGameplayTime) 
							
				CLEAR_BIT(iLocalBoolCheck4, LBOOL4_SCRIPTED_CUT_RENDER_CAMS_CALLED)				
				CLEAR_BIT(iLocalBoolCheck7, LBOOL7_SHOULD_CUTSCENE_DISABLE_PHONE_THIS_FRAME)
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Cleared iLocalBoolCheck7 bit: LBOOL7_SHOULD_CUTSCENE_DISABLE_PHONE_THIS_FRAME")
				
				MP_FORCE_TERMINATE_INTERNET_CLEAR()
				
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - Calling MP_FORCE_TERMINATE_INTERNET_CLEAR()")

				RESET_NET_TIMER(tdScriptedCutsceneSyncSceneVehicleTaskTimer)
				
				DISABLE_SPECTATOR_FILTER_OPTION(FALSE)
				
				SET_DISABLE_RANK_UP_MESSAGE(FALSE)
							
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciDISABLE_CINEMATIC_TOGGLE_CUTSCENE)
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE Option ciDISABLE_CINEMATIC_TOGGLE_CUTSCENE is enabled, Re-enabling cinematic button")
					SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
				ENDIF
				
				INVALIDATE_CINEMATIC_VEHICLE_IDLE_MODE()
				INVALIDATE_IDLE_CAM()
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Lock_Player_Vehicle)
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) 
							SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), VEHICLELOCK_UNLOCKED)
							PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE UNLOCKING VEHICLE FOR CUTSCENE")
						ENDIF
					ENDIF
				ENDIF
						
				SCRIPTED_CUTSCENE_END_WARP_LOCAL sEmpty
				sScriptedCutsceneEndWarp = sEmpty
				
				RESET_NET_TIMER(tdSafetyTimerCutsceneLengthLimit)
				SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_INIT)
			ENDIF
		BREAK
		
	ENDSWITCH
		
	PROCESS_SHOWING_AND_HIDING_CLONES_FOR_SCRIPTED_CUTSCENE_LIFT(iCutsceneToUse, pedPlayers)

ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cutscene General Processing  ------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: The parent function of all processing for Cutscenes.									 						 						 ---------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SERVER_SELECT_END_MOCAP(INT iteam)
	
	INT iteam2
	
	IF MC_serverBD.iEndCutsceneNum[iteam] = -1
		IF MC_ServerBD.iEndCutscene != ciMISSION_CUTSCENE_DEFAULT
			IF MC_serverBD_4.iCurrentHighestPriority[iteam] >= MC_serverBD.iMaxObjectives[iteam]
			#IF IS_DEBUG_BUILD
			OR iPlayerPressedS != -1
			#ENDIF
				INT iEntityType = -1
				IF MC_serverBD.iNumLocHighestPriority[iteam] > 0
					iEntityType = ci_TARGET_LOCATION
				ENDIF
				IF MC_serverBD.iNumPedHighestPriority[iteam] > 0
					iEntityType = ci_TARGET_PED
				ENDIF
				IF MC_serverBD.iNumVehHighestPriority[iteam] > 0
					iEntityType = ci_TARGET_VEHICLE
				ENDIF
				IF MC_serverBD.iNumObjHighestPriority[iteam] > 0
					iEntityType = ci_TARGET_OBJECT
				ENDIF
				
				MC_serverBD.iEndCutsceneNum[iteam] = SERVER_CHOOSE_MOCAP_END_CUTSCENE(iEntityType)
				PRINTLN("[Cutscenes] SERVER_SELECT_END_MOCAP - MC_serverBD.iEndCutsceneNum[iteam] = ",MC_serverBD.iEndCutsceneNum[iteam]," for team ",iteam)
				
				FOR iteam2 = 0 TO (FMMC_MAX_TEAMS-1)
					IF DOES_TEAM_LIKE_TEAM(iteam2,iteam)
						MC_serverBD.iEndCutsceneNum[iteam2] = MC_serverBD.iEndCutsceneNum[iteam] 
						PRINTLN("[Cutscenes] SERVER_SELECT_END_MOCAP - 2; MC_serverBD.iEndCutsceneNum[iteam2] = ",MC_serverBD.iEndCutsceneNum[iteam2]," for team ",iteam2)
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

///PURPOSE: This function does the end mocap cutscene as well as celebration screen stuff.
PROC MANAGE_END_CUTSCENE()

	IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_SHOULD_PROCESS_END_CUTSCENE)
		EXIT 
	ENDIF
	
	IF IS_BIT_SET( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE )
	AND NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_OVER )	
	
		IF NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_SET_UP_STRAND_DETAILS_AT_END_OF_MISSION )
			PROCESS_INITIALISING_STRAND_MISSION()
			IF NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_PLAYED_INTRO_CUTSCENE_RULE)
				SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_0)
			ENDIF
		ELSE
			//If this is a strnd mission then set that we are ready to go
			IF IS_A_STRAND_MISSION_BEING_INITIALISED()
			AND NOT IS_MISSION_CONTROLLER_READY_FOR_CUT()               
				SET_MISSION_CONTROLLER_READY_FOR_CUT()
			ENDIF
			
			DISABLE_HUD()

			PRINTLN("[Cutscenes] TEST MB BEFORE LBOOL9_PRE_MOCAP_CINE_TRIGGERED CHECK ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(mocapPreCinematic))
			
			IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_PRE_MOCAP_CINE_TRIGGERED)
			OR (IS_BIT_SET(iLocalBoolCheck9, LBOOL9_PRE_MOCAP_CINE_TRIGGERED) AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(mocapPreCinematic) > 750)
			
				PRINTLN("[Cutscenes] TEST MB AFTER LBOOL9_PRE_MOCAP_CINE_TRIGGERED CHECK")
		     
				IF WARP_TO_END_MOCAP()		
				
					IF IS_STRING_EMPTY(sMocapCutscene)
						sMocapCutscene = ""
						PRINTLN("[Cutscenes] sMocapCutscene missing this may cause problems ")
					ENDIF
				
					IF NOT IS_BIT_SET( iLocalBoolCheck20, LBOOL20_TURNED_OFF_RADIO_FOR_END_CUTSCENE )
						IF IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
							IF NETWORK_HAS_CONTROL_OF_ENTITY( GET_VEHICLE_PED_IS_IN( LocalPlayerPed ) )
								PRINTLN("[Cutscenes] MANAGE_END_CUTSCENE - We're in a vehicle, so we should turn the radio off. ")
								
								SET_VEHICLE_RADIO_ENABLED( GET_VEHICLE_PED_IS_IN( LocalPlayerPed ), FALSE )
							ENDIF
						ENDIF
						
						SET_BIT( iLocalBoolCheck20, LBOOL20_TURNED_OFF_RADIO_FOR_END_CUTSCENE )
					ENDIF
					
					SET_BIT( iLocalBoolCheck31, LBOOL31_END_CUTSCENE_STARTED)
					
					// Disabled for now but worth looking at in the future. This is the end-mocap equivalent of the MC_LEAVE_PLAYER_BEHIND_FOR_MP_CUTSCENE seen for mid-mission mocaps but currently causes bigger issues than it fixes and will require an in-depth look
//					IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED)
//					AND NOT bIsAnySpectator
//						IF NOT IS_BIT_SET(sInteractWithVars.iInteractWith_Bitset, ciInteractWith_Bitset__SetToLeavePedBehindForLeadIn)
//							IF IS_PLAYER_PERFORMING_INTERACT_WITH_CUTSCENE_LEAD_IN()
//								PRINTLN("[Cutscene_Mocap][InteractWith] - MANAGE_END_CUTSCENE | Calling native to leave a ped behind for Interact-With cutscene leadin")
//								MC_LEAVE_PLAYER_BEHIND_FOR_MP_CUTSCENE()
//								SET_BIT(sInteractWithVars.iInteractWith_Bitset, ciInteractWith_Bitset__SetToLeavePedBehindForLeadIn)
//							ENDIF
//						ENDIF
//					ENDIF

					IF RUN_MOCAP_CUTSCENE(sMocapCutscene, FMMCCUT_ENDMOCAP, TRUE, <<0,0,0>>, ciINVALID_CUTSCENE_WARP_HEADING, <<0,0,0>>, ciINVALID_CUTSCENE_WARP_HEADING, DEFAULT, DEFAULT, DEFAULT)
						CPRINTLN( DEBUG_CONTROLLER,"[Cutscenes] MANAGE_END_CUTSCENE - end cutscene finished")
						
						CLEAR_BIT( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE )
				       
						SET_BIT( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_OVER )
				       
					   	SET_BIT(iLocalBoolCheck33, LBOOL33_SHOULD_SHOW_AND_POPULATE_RESULTS)
					   
						CLEAR_PRINTS()
						
						IF IS_NAMED_RENDERTARGET_REGISTERED("Prop_x17_Sec_Panel_01")
							RELEASE_NAMED_RENDERTARGET("Prop_x17_Sec_Panel_01")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE // Else there's not an end mocap on this mission
		IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_SHOULD_SHOW_AND_POPULATE_RESULTS)
			SET_BIT(iLocalBoolCheck33, LBOOL33_SHOULD_SHOW_AND_POPULATE_RESULTS)
			CPRINTLN( DEBUG_CONTROLLER,"[Cutscenes] MANAGE_END_CUTSCENE - No End Mocap Cutscene to run. Setting LBOOL33_SHOULD_SHOW_AND_POPULATE_RESULTS")
		ENDIF
	ENDIF
ENDPROC

//RIP Joe Debug

PROC PROCESS_ASSIGNING_LOCAL_CUTSCENE_AND_TEAM_PLAYING_CUTSCENE()
	iScriptedCutsceneTeam = GET_CUTSCENE_TEAM()
		
	IF iScriptedCutsceneTeam != -1
		PRINTLN("[LM][Cutscenes] - PROCESS_ASSIGNING_LOCAL_CUTSCENE_AND_TEAM_PLAYING_CUTSCENE - iScriptedCutsceneTeam : ",iScriptedCutsceneTeam)

		#IF IS_DEBUG_BUILD
		IF HAS_NET_TIMER_STARTED(MC_serverBD.tdCutsceneStartTime[iScriptedCutsceneTeam])
			PRINTLN("[LM][Cutscenes] - PROCESS_ASSIGNING_LOCAL_CUTSCENE_AND_TEAM_PLAYING_CUTSCENE - MC_serverBD.tdCutsceneStartTime on local player is (+1000) : ",(NATIVE_TO_INT(MC_serverBD.tdCutsceneStartTime[iScriptedCutsceneTeam].Timer)+1000) )
			PRINTLN("[LM][Cutscenes] - PROCESS_ASSIGNING_LOCAL_CUTSCENE_AND_TEAM_PLAYING_CUTSCENE - cutscene GET_NETWORK_TIME_ACCURATE : ",NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
		ENDIF
		#ENDIF

		g_iFMMCScriptedCutscenePlaying = MC_serverBD.iCutsceneID[iScriptedCutsceneTeam]
		
		IF g_iFMMCScriptedCutscenePlaying != -1
			
			TURN_OFF_CUTSCENE_VEHICLE_RADIO()
		
			PRINTLN("[LM][Cutscenes] PROCESS_ASSIGNING_LOCAL_CUTSCENE_AND_TEAM_PLAYING_CUTSCENE - g_iFMMCScriptedCutscenePlaying = ", g_iFMMCScriptedCutscenePlaying, " CURRENT NETWORK TIMER: ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
			PRINTLN("[LM][Cutscenes] PROCESS_ASSIGNING_LOCAL_CUTSCENE_AND_TEAM_PLAYING_CUTSCENE - g_iFMMCScriptedCutscenePlaying = ", g_iFMMCScriptedCutscenePlaying, " GET_NET_TIMER_DIFF", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_ServerBD.tdCutsceneStartTime[iScriptedCutsceneTeam],FALSE,TRUE),"[RCC MISSION] is host: ", bIsLocalPlayerHost)
			
			IF IS_BIT_SET( MC_playerBD[ iLocalPart ].iCutsceneFinishedBitset, g_iFMMCScriptedCutscenePlaying)
				STOP_LOCAL_PLAYER_FROM_GETTING_SHOT_THIS_FRAME()
				
				iScriptedCutsceneTeam = -1
				g_iFMMCScriptedCutscenePlaying = -1			
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_MID_MISSION_CUTSCENE()

	INT iCutsceneToUse
	FMMC_CUTSCENE_TYPE eCutType = FMMCCUT_SCRIPTED
	
	IF HAS_NET_TIMER_STARTED(stGracePeriodAfterCutscene)
	AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stGracePeriodAfterCutscene) < ciGRACE_PERIOD_AFTER_CUTSCENE
		STOP_LOCAL_PLAYER_FROM_GETTING_SHOT_THIS_FRAME()
	ENDIF
	
	IF g_iFMMCScriptedCutscenePlaying = -1
		PROCESS_ASSIGNING_LOCAL_CUTSCENE_AND_TEAM_PLAYING_CUTSCENE()		
	ELSE
		
		IF g_iFMMCScriptedCutscenePlaying >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
		AND g_iFMMCScriptedCutscenePlaying < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
			iCutsceneToUse = (g_iFMMCScriptedCutscenePlaying - FMMC_GET_MAX_SCRIPTED_CUTSCENES()) 
			IF NOT IS_STRING_NULL_OR_EMPTY( g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName ) 
				IF IS_STRING_NULL_OR_EMPTY(sMocapCutscene)
					sMocapCutscene = Get_String_From_TextLabel(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName)
					PRINTLN("[LM][Cutscenes] SETTING sMocapCutscene to '", sMocapCutscene, "' - was empty 2!")
				ENDIF
				eCutType = FMMCCUT_MOCAP 
			ELSE 
				PRINTLN("[LM][Cutscenes] CUTSCENE NAME IS EMPTY iCutsceneToUse : ", iCutsceneToUse)
	
			ENDIF 
		ELSE 
			iCutsceneToUse = g_iFMMCScriptedCutscenePlaying
		ENDIF
		
		IF (eCutType = FMMCCUT_MOCAP AND iCutsceneToUse >= MAX_MOCAP_CUTSCENES)
		OR (eCutType = FMMCCUT_SCRIPTED AND iCutsceneToUse >= FMMC_GET_MAX_SCRIPTED_CUTSCENES())
			PRINTLN("[LM][Cutscenes] CUTSCENE OUT OF BOUNDS EXITING BAD NEWS")
			EXIT
		ENDIF
	
		IF GET_SCRIPTED_CUTSCENE_STATE_PROGRESS() > SCRIPTEDCUTPROG_INIT
			INVALIDATE_CINEMATIC_VEHICLE_IDLE_MODE()
		ENDIF
		
		//Manage sounds
		IF eCutType = FMMCCUT_SCRIPTED
		AND iCamShot != -1
		AND GET_SCRIPTED_CUTSCENE_STATE_PROGRESS() < SCRIPTEDCUTPROG_CLEANUP
		AND iCutsceneToUse < FMMC_GET_MAX_SCRIPTED_CUTSCENES()
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_SecurityCamFilter) 
			OR IS_USING_CUTSCENE_DRONE_SCALEFORM(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].eDroneUIPreset[iCamShot])						
				MANAGE_CAM_AUDIO(iCutsceneToUse,iCamShot,IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Allow_manual_control))			
			ENDIF
		ENDIF
		
		IF SHOULD_LOCAL_PLAYER_WATCH_TEAM_CUTSCENE(iScriptedCutsceneTeam, iCutsceneToUse, eCutType)
			
			//Render CCTV
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_SecurityCamFilter) 
				REQUEST_CCTV_ASSETS()
				DISABLE_SPECTATOR_FILTER_OPTION(TRUE)
				IF HAVE_CCTV_ASSETS_LOADED()
					IF DOES_CAM_EXIST(cutscenecam)	
						IF IS_CAM_ACTIVE(cutscenecam)
							IF NOT IS_SKYSWOOP_IN_SKY()
								RENDER_CCTV_OVERLAY()								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET( MC_playerBD[ iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR ) 
				IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Allow_manual_control)
					g_FMMC_STRUCT.bDisablePhoneInstructions = TRUE
					IF bIsAnySpectator
						SETUP_MANUAL_CCTV_HELP()
					ENDIF
					PRINTLN("[LM][Cutscenes] MANAGE_MID_MISSION_CUTSCENE - SETTING g_FMMC_STRUCT.bDisablePhoneInstructions to TRUE")
					PRINTLN("[LM][Cutscenes] MANAGE_MID_MISSION_CUTSCENE - g_FMMC_STRUCT.bDisablePhoneInstructions =",g_FMMC_STRUCT.bDisablePhoneInstructions)
				ENDIF
			ENDIF

			SET_DISABLE_RANK_UP_MESSAGE(TRUE)
			
			DISABLE_FRONTEND_THIS_FRAME()
	
			STOP_IMPROMPTU_RACE_SETUP()
			
			CONTACT_REQUEST_MENU_DISABLE_THIS_FRAME()
			
			SET_BIT( iLocalBoolCheck7, LBOOL7_SHOULD_CUTSCENE_DISABLE_PHONE_THIS_FRAME )
			
			SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(LocalPlayer)

			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iCutsceneFinishedBitset, g_iFMMCScriptedCutscenePlaying)				
				
				PRINTLN("[LM][Cutscenes] MANAGE_MID_MISSION_CUTSCENE - Running Cutscene index: ", g_iFMMCScriptedCutscenePlaying)
				
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE)
				
				CONTACT_REQUEST_MENU_DISABLE_THIS_FRAME()
				
				IF (HAS_TEAM_FAILED(MC_playerBD[iLocalPart].iTeam) OR g_bFailedMission)
				AND IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
					PRINTLN("[LM][Cutscenes] MANAGE_MID_MISSION_CUTSCENE - Setting LBOOL25_CUTSCENE_BAIL_FROM_MISSION_FAILURE")
					SET_BIT(iLocalBoolCheck25, LBOOL25_CUTSCENE_BAIL_FROM_MISSION_FAILURE)
				ENDIF
				
				IF eCutType = FMMCCUT_MOCAP
					PROCESS_MOCAP_CUTSCENE(iCutsceneToUse, iScriptedCutsceneTeam, FMMCCUT_MOCAP)
				ELSE  
					PROCESS_SCRIPTED_CUTSCENE(iCutsceneToUse, iScriptedCutsceneTeam)
				ENDIF
			ELSE
				iScriptedCutsceneTeam = -1
				g_iFMMCScriptedCutscenePlaying = -1				
			ENDIF
			
			IF( IS_BIT_SET( iLocalBoolCheck7, LBOOL7_SHOULD_CUTSCENE_DISABLE_PHONE_THIS_FRAME ) )
				IF NOT MP_FORCE_TERMINATE_INTERNET_ACTIVE()
					MP_FORCE_TERMINATE_INTERNET()
					PRINTLN("[LM][Cutscenes] MANAGE_MID_MISSION_CUTSCENE - MP_FORCE_TERMINATE_INTERNET_ACTIVE() is FALSE - Calling MP_FORCE_TERMINATE_INTERNET()")
				ENDIF
				
				DISABLE_CELLPHONE_THIS_FRAME_ONLY(IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Hide_Phone_Instantly))
			ENDIF
			
		ELSE 
			//Set flag to register cutscene has started for player
			iScriptedCutsceneTeam = -1
			g_iFMMCScriptedCutscenePlaying = -1
			
			PRINTLN("[LM][Cutscenes] Local Player set up to not watch cutscene.")
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_ALL_CAM_BLUR()
	
	IF DOES_CAM_EXIST(g_sTransitionSessionData.ciPedCam)
		SET_CAM_MOTION_BLUR_STRENGTH(g_sTransitionSessionData.ciPedCam, 0.0)
	ENDIF
	
	IF DOES_CAM_EXIST(g_sTransitionSessionData.ciCam)
		SET_CAM_MOTION_BLUR_STRENGTH(g_sTransitionSessionData.ciCam, 0.0)
	ENDIF
	
	SET_GAMEPLAY_CAM_MAX_MOTION_BLUR_STRENGTH_THIS_UPDATE(0.0)	
	
ENDPROC

PROC DO_CAM_FREEZE_AND_FX_LOCAL()
	CLEANUP_ALL_CAM_BLUR()
		
	FLOAT fNearCam = GET_FINAL_RENDERED_CAM_NEAR_DOF()
	FLOAT fFarCam = GET_FINAL_RENDERED_CAM_FAR_DOF()
	
	// We no longer need to override out HIDOF for 2122508
	SET_HIDOF_OVERRIDE(TRUE, TRUE, 0, fNearCam, fFarCam, fFarCam + 25.0)
	
	SET_SKYFREEZE_FROZEN(TRUE)
	ANIMPOSTFX_PLAY("MP_job_load", 0, TRUE)	
	PLAY_SOUND_FRONTEND(-1, "SCREEN_FLASH", "CELEBRATION_SOUNDSET")
ENDPROC

PROC PROCESS_CUTSCENE_PROGRESSION_LOGIC(INT iIntroTeam = -1, INT iIntroRule = -1)
		
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
		
	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		
		IF iIntroTeam != -1		
		AND iIntroTeam != iTeam
			RELOOP
		ENDIF
		
		IF iIntroTeam = iTeam
		OR IS_TEAM_ACTIVE(iTeam)
			
			INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]			
			IF iIntroRule != -1
				iRule = iIntroRule
			ENDIF
			
			IF iRule < FMMC_MAX_RULES
				IF NOT IS_BIT_SET(MC_serverBD.iCutsceneStarted[iteam], iRule)
					IF MC_serverBD.iCutsceneID[iTeam] > -1
						IF MC_serverBD.iNumCutscenePlayers[iTeam] > 0
							SET_BIT(MC_serverBD.iCutsceneStarted[iteam], iRule)
							PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_PROGRESSION_LOGIC - MC_serverBD.iCutsceneStarted[", iTeam, "] set at priority: ", iRule) 
							PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_PROGRESSION_LOGIC - ~~** A cutscene has now started **~~")
						ELSE
							PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_PROGRESSION_LOGIC - Can't start cutscene as MC_serverBD.iNumCutscenePlayers[", iTeam, "] <= 0")
						ENDIF
					ELSE
						PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_PROGRESSION_LOGIC - Can't start cutscene as MC_serverBD.iCutsceneID[", iTeam, "] = -1")
					ENDIF
				ELSE					
					IF MC_serverBD.iCutsceneID[iteam] > -1
						IF HAS_EVERYONE_FINISHED_WATCHING_THIS_TEAMS_CUTSCENE(iTeam)
							PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_PROGRESSION_LOGIC - ~~** A cutscene has now finished **~~")
							PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_PROGRESSION_LOGIC - MC_serverBD.iCutsceneFinished[", iTeam, "] set at priority: ", iRule) 					
							PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_PROGRESSION_LOGIC - broadcasting an event to every team that tried to watch this cutscene:")
							
							INT iTeamLoop = 0
							FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 1
								IF SHOULD_THIS_TEAM_WATCH_OTHER_TEAM_CUTSCENE(iTeamLoop, iTeam)
									BROADCAST_TEAM_HAS_FINISHED_CUTSCENE_EVENT(iTeamLoop)
								ENDIF
							ENDFOR
							
							IF iRule < FMMC_MAX_RULES

								PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_PROGRESSION_LOGIC - Setting cutscene number ", MC_serverBD.iCutsceneID[iteam], " finished bit for team ", iteam, " for rule number ", iRule)
								
								SET_BIT(MC_serverBD.iCutsceneFinished[iteam], iRule)
								
								INT iCurrentPlayerRule = -1
								INT iPlayerRule = 0
								
								IF MC_serverBD.iNumPlayerRuleCreated > 0
								AND MC_serverBD.iNumPlayerRuleCreated <= FMMC_MAX_RULES
									REPEAT MC_serverBD.iNumPlayerRuleCreated iPlayerRule
										IF MC_serverBD_4.iPlayerRule[ iPlayerRule ][ iTeam ] = FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE
											IF MC_serverBD_4.iPlayerRulePriority[ iPlayerRule ][ iTeam ] = iRule
												iCurrentPlayerRule = iPlayerRule
											ENDIF
										ENDIF
									ENDREPEAT
									
									IF iCurrentPlayerRule != -1
										PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_PROGRESSION_LOGIC - Progressing the rule on for team ", iTeam)
										PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PLAYER, iCurrentPlayerRule, iteam, TRUE)
									ELSE
										PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_PROGRESSION_LOGIC - Not progressing player rule")
									ENDIF
								ELSE
									PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_PROGRESSION_LOGIC - No valid player rules found")
								ENDIF

								IF IS_INTRO_CUTSCENE_RULE_ACTIVE()
									SET_BIT(MC_ServerBD.iServerBitSet8, SBBOOL8_FINISHED_INTRO_CUTSCENE)
								ENDIF
								
								SET_TEAM_OBJECTIVE_OUTCOME( iteam, iRule, TRUE )
								RESET_SERVER_CUTSCENE_DATA( iteam )
								CLEAR_CUTSCENE_TEAM_CONSIDERATION_BITSET( iteam )						
								MC_serverBD.iCutsceneID[ iteam ] = -1
								
								CLEAR_BIT(MC_ServerBD.iServerBitSet8, SBBOOL8_SKIP_CUTSCENE_TRIGGERED)		
								
								PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_PROGRESSION_LOGIC - ~~** Progressing from cutscene rule **~~" )						
							ENDIF
						ELSE
							PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_PROGRESSION_LOGIC - Waiting for: HAS_EVERYONE_FINISHED_WATCHING_THIS_TEAMS_CUTSCENE")
						ENDIF
					ELSE
						PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_PROGRESSION_LOGIC - MC_serverBD.iCutsceneID[ iteam ] = " ,MC_serverBD.iCutsceneID[ iteam ], " for team ", iteam)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

FUNC BOOL PROCESS_INTRO_RULE_CUTSCENE()
	
	INT iTeamLoop
	
	IF NOT g_bMissionIPLsRequested
		PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - waiting before starting intro cutscene until mission controller requests IPLs (g_bMissionIPLsRequested is still FALSE)")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(tdSafetyTimer)
		PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - Safety Timer Started: ", NATIVE_TO_INT(GET_NETWORK_TIME()))
		REINIT_NET_TIMER(tdSafetyTimer)
	ENDIF
		
	IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSafetyTimer) < ciINTRO_SAFETY_TIMER_BAIL_TIME
	AND NOT HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO()
		PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - Waiting for HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO")
		RETURN FALSE
	ENDIF
	
	INT i
	INT iPlayerRulePriority = -1
	FOR iTeamLoop = 0 TO MC_serverBD.iNumberOfTeams - 1
		
		PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - iTeamLoop: ", iTeamLoop, " Checking this team to see if they start on a cutscene rule.")	
				
		FOR i = 0 TO (FMMC_MAX_RULES-1)
			IF GET_MC_TEAM_STARTING_RULE(iTeamLoop) = MC_serverBD_4.iPlayerRulePriority[i][iTeamLoop]
				iPlayerRulePriority = i
				PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - iTeamLoop: ", iTeamLoop, " iPlayerRulePriority: ", iPlayerRulePriority)				
				BREAKLOOP
			ENDIF
		ENDFOR
		
		IF GET_MC_TEAM_STARTING_RULE(iTeamLoop) = 0
			PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - iTeamLoop: ", iTeamLoop, " This team starts on a Cutscene Rule.")	
			
			IF bIsLocalPlayerHost				
				PROCESS_CUTSCENE_OBJECTIVE_LOGIC(iTeamLoop, iPlayerRulePriority)
				
				IF MC_serverBD.iCutsceneID[iTeamLoop] = -1
					PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - iTeamLoop: ", iTeamLoop, " still has -1 for MC_serverBD.iCutsceneID. Relooping.")	
					RELOOP
				ENDIF
				
				SELECT_CUTSCENE_PLAYERS_FOR_TEAM(iTeamLoop)
			ELSE
				PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - iTeamLoop: ", iTeamLoop, " Not the host.")	
			ENDIF			
			
			IF MC_serverBD.iCutsceneID[iTeamLoop] != -1
			AND iScriptedCutsceneTeam = -1
				PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - iTeamLoop: ", iTeamLoop, " Assigning Cutscene Team via PROCESS_ASSIGNING_LOCAL_CUTSCENE_AND_TEAM_PLAYING_CUTSCENE with MC_serverBD.iCutsceneID[iTeamLoop]: ", MC_serverBD.iCutsceneID[iTeamLoop])	
				PROCESS_ASSIGNING_LOCAL_CUTSCENE_AND_TEAM_PLAYING_CUTSCENE()
			ENDIF
			
			IF iPlayerRulePriority != -1
				PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - iTeamLoop: ", iTeamLoop, " Setting LBOOL28_HAS_INTRO_CUTSCENE_RULE, Using iPlayerRulePriority: ", iPlayerRulePriority)
				SET_BIT(iLocalBoolCheck28, LBOOL28_HAS_INTRO_CUTSCENE_RULE)
			ENDIF
			
		ENDIF
		
	ENDFOR
	
	INT iTeam = iScriptedCutsceneTeam
	
	IF iTeam != -1
	AND IS_BIT_SET(iLocalBoolCheck28, LBOOL28_HAS_INTRO_CUTSCENE_RULE)
					
		PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - Team: ", iTeam, " PROCESSING INTRO CUTSCENE RULE.")
				
		IF MC_serverBD.iCutsceneID[iTeam] = -1
			PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - Team: ", iTeam, " Waiting for a valid cutscene ID to be assigned... MC_serverBD.iCutsceneID[iteam] = -1")	
			RETURN FALSE
		ENDIF
		
		PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - Team: ", iTeam, " MC_serverBD.iServerBitSet: ", MC_serverBD.iServerBitSet)		
				
		INT iMocapIndex = MC_serverBD.iCutsceneID[iTeam] - FMMC_GET_MAX_SCRIPTED_CUTSCENES()
		
		PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - Team: ", iTeam, " iCutsceneID: ",  MC_serverBD.iCutsceneID[iTeam], " Mocap Index: ", iMocapIndex)
		
		FMMC_CUTSCENE_TYPE eCutType = FMMCCUT_SCRIPTED
		
		IF MC_serverBD.iCutsceneID[iTeam] >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
		AND MC_serverBD.iCutsceneID[iTeam] < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMocapCutsceneData[iMocapIndex].tlName ) 
				eCutType = FMMCCUT_MOCAP 
				SET_BIT(iLocalBoolCheck9, LBOOL9_FORCE_STREAM_CUTSCENE)
			ENDIF 
		ENDIF
		
		IF eCutType = FMMCCUT_MOCAP 
			MANAGE_STREAMING_MOCAP()
		ENDIF
		
		IF IS_BIT_SET( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED)		
		OR eCutType = FMMCCUT_SCRIPTED
			MANAGE_MID_MISSION_CUTSCENE()
		ELSE
			PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - Team: ", iTeam, " Waiting for cutscene to be streamed in.")
		ENDIF
				
		IF g_iFMMCScriptedCutscenePlaying = -1
			PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - Team: ", iTeam, " Waiting for cutscene to be initialised.")
			RETURN FALSE
		ENDIF
				
		SET_BIT(iLocalBoolCheck28, LBOOL28_PLAYED_INTRO_CUTSCENE_RULE)
				
		IF bIsLocalPlayerHost			
			INITIALISE_INT_ARRAY(tempiNumCutscenePlayers, 0)			
			
			FOR i = 0 TO NUM_NETWORK_PLAYERS - 1
				IF NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_PLAYING_CUTSCENE)
					RELOOP
				ENDIF
				tempiNumCutscenePlayers[MC_Playerbd[i].iTeam]++
			ENDFOR
			
			FOR i = 0 TO (FMMC_MAX_TEAMS-1)
				IF MC_serverBD.iNumCutscenePlayers[i] = tempiNumCutscenePlayers[i]				
					RELOOP
				ENDIF
				
				MC_serverBD.iNumCutscenePlayers[i] = tempiNumCutscenePlayers[i]
				
				PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - MC_serverBD.iNumCutscenePlayers[", i, "] updated to: ", MC_serverBD.iNumCutscenePlayers[i], " current rule:  ", GET_MC_TEAM_STARTING_RULE(i))
				
				IF MC_serverBD.iNumCutscenePlayers[i] <= MC_serverBD.iMaxNumCutscenePlayers[i]
					RELOOP
				ENDIF
				
			 	MC_serverBD.iMaxNumCutscenePlayers[i] = MC_serverBD.iNumCutscenePlayers[i]
		
				PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - MC_serverBD.iMaxNumCutscenePlayers[", i, "]  updated to: ", MC_serverBD.iMaxNumCutscenePlayers[i]) 				
			ENDFOR
			
			PROCESS_CUTSCENE_PROGRESSION_LOGIC(iTeam, GET_MC_TEAM_STARTING_RULE(iTeam))
		ENDIF
		
		IF NOT ARE_ALL_PLAYERS_PLAYING_INTRO_CUTSCENE_RULE()
			PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - Team: ", iTeam, " ARE_ALL_PLAYERS_PLAYING_INTRO_CUTSCENE_RULE - FALSE")			
			RETURN FALSE
		ENDIF
				
		IF NOT ARE_ALL_PLAYERS_SYNCED_FOR_INTRO_CUTSCENE_RULE()
			PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - Team: ", iTeam, " ARE_ALL_PLAYERS_SYNCED_FOR_INTRO_CUTSCENE_RULE - FALSE")			
			RETURN FALSE
		ENDIF
		
		IF eCutType = FMMCCUT_SCRIPTED
			
			IF GET_SCRIPTED_CUTSCENE_STATE_PROGRESS() = SCRIPTEDCUTPROG_WARPINTRO						
				IF IS_THIS_A_QUICK_RESTART_JOB()
					SET_SKYSWOOP_DOWN(DEFAULT, DEFAULT, TRUE, FALSE, DEFAULT, FALSE, TRUE)
					IF NOT IS_BIT_SET(iLocalBoolCheck4, LBOOL4_INTRO_CUT_RULE_RENDERPHASE_PAUSED)
						IF IS_SKYCAM_PAST_FIRST_CUT()
							DO_CAM_FREEZE_AND_FX_LOCAL()
							SET_BIT(iLocalBoolCheck4, LBOOL4_INTRO_CUT_RULE_RENDERPHASE_PAUSED)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
				
			IF GET_SCRIPTED_CUTSCENE_STATE_PROGRESS() < SCRIPTEDCUTPROG_PROCESS_SHOTS
				PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - Team: ", iTeam, " Waiting for scripted cutscene to start.")
				RETURN FALSE
			ENDIF
			
			IF g_iFMMCScriptedCutscenePlaying > -1
			AND g_iFMMCScriptedCutscenePlaying < FMMC_GET_MAX_SCRIPTED_CUTSCENES()
				IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[g_iFMMCScriptedCutscenePlaying].iCutsceneShotBitSet[iCamShot], ciCSS_BS_HideShotInIntro)
					PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - Team: ", iTeam, " Waiting for hidden scene to end.")
					RETURN FALSE
				ENDIF
			ENDIF
			
		ELIF eCutType = FMMCCUT_MOCAP
			
			IF eMocapManageCutsceneProgress = MOCAPPROG_INTRO						
				IF IS_THIS_A_QUICK_RESTART_JOB()
					SET_SKYSWOOP_DOWN(DEFAULT, DEFAULT, TRUE, FALSE, DEFAULT, FALSE, TRUE)
					IF NOT IS_BIT_SET(iLocalBoolCheck4, LBOOL4_INTRO_CUT_RULE_RENDERPHASE_PAUSED)
						IF IS_SKYCAM_PAST_FIRST_CUT()
							DO_CAM_FREEZE_AND_FX_LOCAL()
							SET_BIT(iLocalBoolCheck4, LBOOL4_INTRO_CUT_RULE_RENDERPHASE_PAUSED)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF eMocapManageCutsceneProgress < MOCAPPROG_PROCESS_SHOTS
			OR eMocapRunningCutsceneProgress < MOCAPRUNNING_STAGE_2
				PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - Team: ", iTeam, " Waiting for mocap cutscene to start.")
				RETURN FALSE
			ENDIF
			
			IF SHOULD_CUTSCENE_HOLD_RENDERPHASE_LONGER(sMocapCutscene)
				PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - Team: ", iTeam, " Waiting for mocap cutscene to play a little.")
				RETURN FALSE
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_HAS_INTRO_CUTSCENE_RULE)
		AND NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_PLAYED_INTRO_CUTSCENE_RULE)
			PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - Team: ", iTeam, " Returning FALSE. NOT YET SET.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF SET_SKYSWOOP_DOWN(DEFAULT, DEFAULT, TRUE, FALSE, DEFAULT, FALSE)
	AND NOT IS_PLAYER_SWITCH_TO_CHARACTER_IN_PROGRESS()
	AND IS_SCREEN_FADED_IN()
		
		IF g_iFMMCScriptedCutscenePlaying > -1
		AND g_iFMMCScriptedCutscenePlaying < FMMC_GET_MAX_SCRIPTED_CUTSCENES()
		AND IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[g_iFMMCScriptedCutscenePlaying].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)
			PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - Team: ", iTeam, " Scripted Sync Scene. Not unpausing renderphase yet - g_iFMMCScriptedCutscenePlaying: ", g_iFMMCScriptedCutscenePlaying)
		ELSE
			PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - Team: ", iTeam, " Unpausing Renderphase from PROCESS_INTRO_RULE_CUTSCENE - g_iFMMCScriptedCutscenePlaying: ", g_iFMMCScriptedCutscenePlaying)
			UNPAUSE_RENDERPHASE_FOR_INTRO_CUTSCENE()
		ENDIF
		
		IF DRAW_JOB_TEXT(jobIntroData, -1, SHOULD_LOCAL_PLAYER_SEE_TAKE_IN_CELEBRATION_SCREENS(), DEFAULT, DEFAULT, DEFAULT, DEFAULT)
			
			PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - Team: ", iTeam, " Moving straight into running state")
			
			DO_CORONA_POLICE_SETTINGS()
			
			MC_SET_ALL_MISSION_ENTITIES_VISIBLITY(TRUE)
			
			DO_SCREEN_FADE_IN(500)			
			
			IF SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
				CLEAR_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
			ENDIF
			
			RETURN TRUE
			
		ENDIF
		
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[Cutscene_Intro] PROCESS_INTRO_RULE_CUTSCENE - Team: ", iTeam, " waiting on skyswoop down before moving into running state. ")
	#ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_CUTSCENES_EVERY_FRAME()
	
	INT i
	IF bIsLocalPlayerHost
		CHECK_FOR_END_CUTSCENE_VARIATION_CHANGE()
		FOR i = 0 TO ( MC_serverBD.iNumberOfTeams - 1 )
			SERVER_SELECT_END_MOCAP( i )	
			SELECT_CUTSCENE_PLAYERS_FOR_TEAM( i )
		ENDFOR
	ENDIF

	MANAGE_MID_MISSION_CUTSCENE()
	
	PROCESS_CUTSCENE_PROGRESSION_LOGIC()
	
	MANAGE_STREAMING_MOCAP()
	
	MANAGE_END_CUTSCENE()
	
ENDPROC
