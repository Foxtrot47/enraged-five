// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Locations ---------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Locations processing. Go To, Capture etc.                                                                                              ---------------------------                                
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020                                                                                       
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_EntityObjectives_2020.sch"

// ##### ---------------------------------------------------------------------------
// ##### Section Name: Utility										----------------
// ##### Description: Utility functions for locations				----------------
// ##### ---------------------------------------------------------------------------
// ##### ---------------------------------------------------------------------------

FUNC BOOL HAS_ANY_ONE_HIT_LOCATE(INT iLoc, INT iRule)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ALL_TEAMS_SHARE_LOCATES)
		RETURN FALSE
	ENDIF
	
	INT iPart
	PRINTLN("[PLAYER_LOOP] - HAS_ANY_ONE_HIT_LOCATE")
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF NOT IS_BIT_SET(MC_playerBD_1[iLocalPart].iLocBitset, iLoc)
			IF IS_BIT_SET(MC_playerBD_1[iPart].iLocBitset, iLoc)
			AND iPart != iLocalPart
			AND iRule = MC_serverBD_4.iGotoLocationDataPriority[iLoc][MC_playerBD[iLocalPart].iteam]
				PRINTLN("[RCC MISSION] HAS_ANY_ONE_HIT_LOCATE - Returning TRUE for loc ", iLoc, " participant: ", iPart)
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PARTICIPANT_IN_SECONDARY_LOCATE_IF_THEY_NEED_TO_BE(INT iLoc, INT iPart, INT iTeamsToCheck)
	
	IF IS_BIT_SET(iTeamsToCheck, MC_playerBD[iPart].iTeam)
		RETURN IS_BIT_SET(MC_playerBD[iPart].iLocationInSecondaryLocation, iLoc)
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_EVERYONE_VALID_THAT_IS_NEEDED_FOR_LOCATE(INT iLoc, INT iTeamBS)
	
	INT iPart = -1
	DO_PARTICIPANT_LOOP_FLAGS eFlags = GET_PARTICIPANT_LOOP_FLAGS_FROM_VALID_TEAM_BITSET(iTeamBS)
	WHILE DO_PARTICIPANT_LOOP(iPart, eFlags | DPLF_CHECK_PED_ALIVE)
		
		IF NOT IS_PARTICIPANT_IN_SECONDARY_LOCATE_IF_THEY_NEED_TO_BE(iLoc, iPart, iTeamBS)
			PRINTLN("IS_EVERYONE_VALID_THAT_IS_NEEDED_FOR_LOCATE - Participant ", iPart, " isn't in the secondary locate they are needed in")
			RETURN FALSE
		ENDIF
		
		IF NOT CAN_PLAYER_PASS_THIS_LOCATION(GET_PLAYER_PED_INDEX_FROM_INT(iPart), iLoc, MC_playerBD[iPart].iTeam, FALSE, FALSE)
			
			IF IS_INPUT_TRIGGER_REQUIRED_FOR_LOCATION(iLoc)
			AND NOT IS_BIT_SET(MC_serverBD_4.iLocationInputTriggeredBitset, iLoc)
				SET_BIT(MC_serverBD_4.iLocationWaitingForInputBitset, iLoc)
				PRINTLN("IS_EVERYONE_VALID_THAT_IS_NEEDED_FOR_LOCATE - Setting MC_serverBD_4.iLocationWaitingForInputBitset, ", iLoc)
			ENDIF
			
			PRINTLN("IS_EVERYONE_VALID_THAT_IS_NEEDED_FOR_LOCATE - Participant ", iPart, " cannot pass this location")
			RETURN FALSE
		ELSE
			IF IS_BIT_SET(MC_serverBD_4.iLocationWaitingForInputBitset, iLoc)
				CLEAR_BIT(MC_serverBD_4.iLocationWaitingForInputBitset, iLoc)
			ENDIF
		ENDIF
		
	ENDWHILE

	RETURN TRUE
	
ENDFUNC

FUNC INT GET_LOCAL_TEAM_PERSONAL_LOCATE_TOTAL()
	INT i, iReturn
	PRINTLN("[PLAYER_LOOP] - GET_LOCAL_TEAM_PERSONAL_LOCATE_TOTAL")
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF MC_playerBD[i].iteam = MC_playerBD[iLocalPart].iteam
			iReturn += MC_playerBD[i].iPersonalLocatePoints
		ENDIF
	ENDFOR
	
	RETURN iReturn
ENDFUNC


FUNC FLOAT CALCULATE_MINIMUM_DISTANCE_BETWEEN_TEAM_MATES(INT iLoc, INT iTeam)

	FLOAT fDistance
	INT iTeamSizeThreshold

	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iTeamSizeThreshold = - 1
		iTeamSizeThreshold = 4
	ELSE
		iTeamSizeThreshold = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iTeamSizeThreshold
	ENDIF
	
	IF MC_serverBD.iNumberOfPlayingPlayers[iTeam] < iTeamSizeThreshold + 1 
		fDistance = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin
	ELSE
		fDistance = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin + ((MC_serverBD.iNumberOfPlayingPlayers[iTeam] - iTeamSizeThreshold) * g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededToleranceIncrease)
	ENDIF
	
	
	RETURN fDistance

ENDFUNC

PROC CALCULATE_DISTANCES_TO_LOCATE(INT iLoc)
	#IF IS_DEBUG_BUILD	IF bDebugCrossTheLineHUDPrints	PRINTLN("CALCULATE_DISTANCES_TO_LOCATE - - - - - - - iLoc = ", iLoc)	ENDIF	#ENDIF
	INT iParticipant
	
	FLOAT fDistanceToCrossline
	
	PARTICIPANT_INDEX participantIndex
	
	INT iTeamsToCheck, iTeamLoop
	
	FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_CrossLineDistUseMe_T0 + iTeamLoop)
			SET_BIT(iTeamsToCheck, iTeamLoop)
			
			#IF IS_DEBUG_BUILD	IF bDebugCrossTheLineHUDPrints	PRINTLN("CALCULATE_DISTANCES_TO_LOCATE - SET_BIT(iTeamsToCheck, iTeamLoop) ... iTeamLoop = ", iTeamLoop)	ENDIF	#ENDIF
		ENDIF
	ENDFOR
	
	FLOAT fDistanceToCrosslineLoop[6]
	
	PRINTLN("[PLAYER_LOOP] - CALCULATE_DISTANCES_TO_LOCATE")
	FOR iParticipant = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
		participantIndex = INT_TO_PARTICIPANTINDEX(iParticipant)

		IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
			PLAYER_INDEX playerIndex = NETWORK_GET_PLAYER_INDEX(participantIndex)
			
			IF GET_PLAYER_TEAM(playerIndex) != -1			
				IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerIndex)
				AND NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_ANY_SPECTATOR)
				AND NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_HEIST_SPECTATOR)
					IF IS_BIT_SET(iTeamsToCheck, MC_playerBD[iParticipant].iTeam)
						IF NOT (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_CrossLineLocalTeamOnly) 
						AND (MC_playerBD[iParticipant].iTeam) <> MC_playerBD[iPartToUse].iTeam)
							IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][MC_playerBD[iParticipant].iTeam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iParticipant].iTeam]
								#IF IS_DEBUG_BUILD	IF bDebugCrossTheLineHUDPrints	PRINTLN("CALCULATE_DISTANCES_TO_LOCATE - iParticipant = ", iParticipant)	ENDIF	#ENDIF
								#IF IS_DEBUG_BUILD	IF bDebugCrossTheLineHUDPrints	PRINTLN("CALCULATE_DISTANCES_TO_LOCATE - MC_playerBD[iParticipant].iTeam = ", MC_playerBD[iParticipant].iTeam)	ENDIF	#ENDIF
								
								PED_INDEX pedIndex = GET_PLAYER_PED(playerIndex)
								
								IF NOT IS_ENTITY_DEAD(pedIndex)
									IF MC_playerBD[iParticipant].iCurrentLoc = iLoc
										fDistanceToCrossline = -1	//DISPLAY TICK
									ELSE
										VECTOR vPedCoords = GET_ENTITY_COORDS(pedIndex)
										
										vPedCoords = <<vPedCoords.X, vPedCoords.Y, 0.0>>
										
										ANGLED_AREA_CORNER_STRUCT vAngledAreaCorners = GET_CORNERS_OF_ANGLED_AREA(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vLoc1, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vLoc2, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fWidth)
										
										vAngledAreaCorners.vCorner[0] = <<vAngledAreaCorners.vCorner[0].X, vAngledAreaCorners.vCorner[0].Y, 0.0>>
										vAngledAreaCorners.vCorner[1] = <<vAngledAreaCorners.vCorner[1].X, vAngledAreaCorners.vCorner[1].Y, 0.0>>
										vAngledAreaCorners.vCorner[2] = <<vAngledAreaCorners.vCorner[2].X, vAngledAreaCorners.vCorner[2].Y, 0.0>>
										vAngledAreaCorners.vCorner[3] = <<vAngledAreaCorners.vCorner[3].X, vAngledAreaCorners.vCorner[3].Y, 0.0>>
										
										#IF IS_DEBUG_BUILD	IF bDebugCrossTheLineHUDPrints
										PRINTLN("CALCULATE_DISTANCES_TO_LOCATE - vAngledAreaCorners.vCorner[0] = ", vAngledAreaCorners.vCorner[0])
										PRINTLN("CALCULATE_DISTANCES_TO_LOCATE - vAngledAreaCorners.vCorner[1] = ", vAngledAreaCorners.vCorner[1])
										PRINTLN("CALCULATE_DISTANCES_TO_LOCATE - vAngledAreaCorners.vCorner[2] = ", vAngledAreaCorners.vCorner[2])
										PRINTLN("CALCULATE_DISTANCES_TO_LOCATE - vAngledAreaCorners.vCorner[3] = ", vAngledAreaCorners.vCorner[3])
										ENDIF	#ENDIF
											
										
										
										fDistanceToCrosslineLoop[0] = MINIMUM_DISTANCE_POINT_TO_LINE(vAngledAreaCorners.vCorner[0], vAngledAreaCorners.vCorner[1], vPedCoords)
										fDistanceToCrosslineLoop[1] = MINIMUM_DISTANCE_POINT_TO_LINE(vAngledAreaCorners.vCorner[0], vAngledAreaCorners.vCorner[2], vPedCoords)
										fDistanceToCrosslineLoop[2] = MINIMUM_DISTANCE_POINT_TO_LINE(vAngledAreaCorners.vCorner[0], vAngledAreaCorners.vCorner[3], vPedCoords)
										fDistanceToCrosslineLoop[3] = MINIMUM_DISTANCE_POINT_TO_LINE(vAngledAreaCorners.vCorner[1], vAngledAreaCorners.vCorner[2], vPedCoords)
										fDistanceToCrosslineLoop[4] = MINIMUM_DISTANCE_POINT_TO_LINE(vAngledAreaCorners.vCorner[1], vAngledAreaCorners.vCorner[3], vPedCoords)
										fDistanceToCrosslineLoop[5] = MINIMUM_DISTANCE_POINT_TO_LINE(vAngledAreaCorners.vCorner[2], vAngledAreaCorners.vCorner[3], vPedCoords)
										
										fDistanceToCrossline = 99999
										
										INT iCornerLoop
										
										FOR iCornerLoop = 0 TO COUNT_OF(fDistanceToCrosslineLoop) - 1
											IF fDistanceToCrosslineLoop[iCornerLoop] < fDistanceToCrossline
												fDistanceToCrossline = fDistanceToCrosslineLoop[iCornerLoop]
											ENDIF
										ENDFOR
										
										fDistanceToCrossline = ABSF(fDistanceToCrossline)
									ENDIF
								ELSE
									fDistanceToCrossline = -2	//Dead
								ENDIF
								
								IF NOT SHOULD_USE_METRIC_MEASUREMENTS()
									IF fDistanceToCrossline >= 0
										//fDistanceToCrossline = fDistanceToCrossline * 3.2808399	//1 metre = 3.2808399 feet
										
										fDistanceToCrossline = CONVERT_METERS_TO_FEET(fDistanceToCrossline)
									ENDIF
								ENDIF
								
								IF playerIndex != LocalPlayer
									INT iTeamCheckOffset = MC_playerBD[iParticipant].iTeam * playerStateListDataCrossTheLine.iNumberOfPlayersToDisplayPerTeam
									
									IF NOT bIsOverLine[playerStateListDataCrossTheLine.iCurrentPlayerCheck[MC_playerBD[iParticipant].iTeam] + iTeamCheckOffset]
										IF fDistanceToCrossline = -1
											bIsOverLine[playerStateListDataCrossTheLine.iCurrentPlayerCheck[MC_playerBD[iParticipant].iTeam] + iTeamCheckOffset] = TRUE
											
											INT iTeamToCheck = MC_playerBD[iParticipant].iTeam
											INT iCTLSoundID = GET_SOUND_ID()
											FLOAT fIntensity = TO_FLOAT(MC_serverBD_1.iNumberOfTeamInArea[iLoc][iTeamToCheck]) / TO_FLOAT(MC_serverBD.iNumberOfPlayingPlayers[iTeamToCheck])
											
											IF MC_playerBD[iParticipant].iTeam = MC_playerBD[iPartToUse].iTeam
												PRINTLN("[CTLSFX] Playing Friendly with Intensity ", fIntensity)
												PLAY_SOUND_FRONTEND(iCTLSoundID,"Remote_Friendly_Enter_Line","GTAO_FM_Cross_The_Line_Soundset", FALSE)
											ELSE
												PRINTLN("[CTLSFX] Playing Enemy with Intensity ", fIntensity)
												PLAY_SOUND_FRONTEND(iCTLSoundID,"Remote_Enemy_Enter_Line","GTAO_FM_Cross_The_Line_Soundset", FALSE)
											ENDIF
											
											SET_VARIABLE_ON_SOUND(iCTLSoundID, "intensity", fIntensity)
										ENDIF
									ELSE
										IF fDistanceToCrossline >= 0
											bIsOverLine[playerStateListDataCrossTheLine.iCurrentPlayerCheck[MC_playerBD[iParticipant].iTeam] + iTeamCheckOffset] = FALSE
										ENDIF
									ENDIF
								ENDIF
								
								PLAYER_STATE_LIST_ADD_PLAYER(playerStateListDataCrossTheLine, iParticipant, ROUND(fDistanceToCrossline))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC INT RETURN_DISTANCE_FROM_LINE_FOR_LOCAL_PLAYER(PLAYER_STATE_LIST_DATA &playerStateListData)
	IF NOT IS_STRING_NULL_OR_EMPTY(playerStateListData.uiSlot[0].sName)
		IF playerStateListData.bSortLocalPlayerTeamTop
			RETURN playerStateListData.uiSlot[0].iValue
		ELSE
			INT i
			
			FOR i = 0 TO playerStateListData.iNumberOfPlayersToCheck - 1
				IF playerStateListData.piPlayerStateList[i] = LocalPlayer
					RETURN playerStateListData.iPlayerStateList[i]
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
	
	RETURN -3	//None
ENDFUNC

FUNC BOOL IS_PLAYER_IN_OUTFIT_FOR_LOCATION(INT iLocation)
	
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].eOutfitRequiredForLocation = OUTFIT_FOR_LOCATION_SPAWN_CHECK_NONE
		RETURN TRUE
	ENDIF
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].eOutfitRequiredForLocation
		CASE OUTFIT_FOR_LOCATION_SPAWN_CHECK_FIREMAN			RETURN IS_LOCAL_PLAYER_WEARING_A_FIREMAN_OUTFIT()
		CASE OUTFIT_FOR_LOCATION_SPAWN_CHECK_SWAT				RETURN IS_LOCAL_PLAYER_WEARING_A_NOOSE_OUTFIT()
		CASE OUTFIT_FOR_LOCATION_SPAWN_CHECK_HIGH_ROLLER		RETURN IS_LOCAL_PLAYER_WEARING_A_HIGH_ROLLER_OUTFIT()
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_PROCESS_LOCATION(INT iLocation)
	IF NOT IS_PLAYER_IN_OUTFIT_FOR_LOCATION(iLocation)
		PRINTLN("SHOULD_PROCESS_LOCATION - location: ", iLocation, " is being suppressed. The player is not in the correct outfit here.")
		RETURN FALSE
	ENDIF
		
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_START_LOCATION_DELAY_TIMER(INT iLocation)

	UNUSED_PARAMETER(iLocation)
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		PRINTLN("[LeaveArea] SHOULD_START_LOCATION_DELAY_TIMER - SBBOOL_FIRST_UPDATE_DONE isn't set")
		RETURN FALSE
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		PRINTLN("[LeaveArea] SHOULD_START_LOCATION_DELAY_TIMER - IS_CUTSCENE_PLAYING")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC START_PROCESSING_LOCATION(INT iLocation)

	IF NOT SHOULD_START_LOCATION_DELAY_TIMER(iLocation)
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(tdLocationProcDelayTimer[iLocation])
		START_NET_TIMER(tdLocationProcDelayTimer[iLocation])
		PRINTLN("[LeaveArea] PROCESS_LOCATIONS_EVERY_FRAME -  location: ", iLocation, " Starting processing delay timer.")
	ENDIF
ENDPROC

PROC SET_CAPTURE_TIME(FLOAT fCurrentCaptureTime, INT iLoc, INT iTeam)
	iCurrCapTime[iLoc][iTeam] = fCurrentCaptureTime
	PRINTLN("[Loc ",iLoc,"] iCurrCapTime[",iLoc,"][",iTeam,"] = ", iCurrCapTime[iLoc][iTeam])
	DEBUG_PRINTCALLSTACK()
ENDPROC

// ##### ---------------------------------------------------------------------------------------------------
// ##### Section Name: Visuals																----------------
// ##### Description: Functions for location visuals. Drawing, deleting, alpha etc.			----------------
// ##### ---------------------------------------------------------------------------------------------------
// ##### ---------------------------------------------------------------------------------------------------

/// PURPOSE: This function does all of the updates that are necessary for fading in/out coronas
///    
/// PARAMS:
///    toUpdate - This is the struct of the locate/corona you want to fade in/out
///    iDebugIndex - This is for debug output - the index of the corona/locate
///    iDebugTeam - This is for debug output - the index of the team who triggered this change
PROC UPDATE_LOCATE_FADE_STRUCT( locate_fade_out_data& toUpdate, INT iDebugIndex = -1, INT iDebugTeam = -1 )
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
	
		IF NOT g_bMissionEnding
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciDONT_REMOVE_LOCATES)
			OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iRule].iLocBS2, ciLoc_BS2_DontFadeWhenInside)
				IF toUpdate.eState = eLocateFadeOutState_FADING_OUT
					toUpdate.eState = eLocateFadeOutState_DISPLAYING
					toUpdate.iAlphaValue = 255
					EXIT
				ENDIF
			ENDIF
		ENDIF
		
		
		// Fade the locates in once they appear
		IF toUpdate.eState = eLocateFadeOutState_FADING_IN
			toUpdate.iAlphaValue = FLOOR( toUpdate.iAlphaValue +@ ( 255.0 * 3.5 ) )

			IF toUpdate.iAlphaValue >= 255
				toUpdate.iAlphaValue = 255
				toUpdate.eState = eLocateFadeOutState_DISPLAYING
				
				IF iDebugIndex > -1 AND iDebugTeam > -1
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Locate ", iDebugIndex, " for team ", iDebugTeam, " has fully reappeared." )
				ENDIF
			ENDIF
		ENDIF
		
		// Draw the locates even after we're done with them
		IF toUpdate.eState = eLocateFadeOutState_FADING_OUT
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CHECKPOINT_LOGIC)
				toUpdate.iAlphaValue = 0
			ELSE
				toUpdate.iAlphaValue = FLOOR( toUpdate.iAlphaValue -@ ( 255.0 * 3.5 ) )
			ENDIF
			
			IF toUpdate.iAlphaValue <= 0
				toUpdate.iAlphaValue = 0
				toUpdate.eState = eLocateFadeOutState_AFTER_FADE_OUT
				
				IF iDebugIndex > -1 AND iDebugTeam > -1
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Locate ", iDebugIndex, " for team ", iDebugTeam, " has fully disappeared" )
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// Tells the locate that we wish it to fade in now
PROC SET_LOCATE_TO_FADE_IN( locate_fade_out_data& toFadeOut,INT iDebugIndex = -1,INT iDebugTeam = -1)
	IF toFadeOut.eState = eLocateFadeOutState_BEFORE_FADE_IN		
		toFadeOut.eState = eLocateFadeOutState_FADING_IN // Set this locate to fade in
		toFadeOut.iAlphaValue = 0
		
		IF iDebugIndex > -1 AND iDebugTeam > -1
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Locate ", iDebugIndex, " for team ", iDebugTeam, " is now reappearing." )
		ENDIF
	ENDIF
ENDPROC

// Tells the locate that we wish it to fade out now
PROC SET_LOCATE_TO_FADE_OUT( locate_fade_out_data& toFadeOut, INT iDebugIndex = -1, INT iDebugTeam = -1)
	IF toFadeOut.eState = eLocateFadeOutState_DISPLAYING
		toFadeOut.eState = eLocateFadeOutState_FADING_OUT // Set this locate to fade out
		toFadeOut.iAlphaValue = 255
		
		IF iDebugIndex > -1 AND iDebugTeam > -1
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Locate ", iDebugIndex, " for team ", iDebugTeam, " is now disappearing." )
		ENDIF
	ENDIF

ENDPROC

// Instantly sets this locate to faded out after displaying
PROC SET_LOCATE_TO_NOT_DISPLAY( locate_fade_out_data& toNotDisplay, INT iDebugIndex = -1, INT iDebugTeam = -1)					
	IF toNotDisplay.eState != eLocateFadeOutState_AFTER_FADE_OUT
		toNotDisplay.eState = eLocateFadeOutState_AFTER_FADE_OUT
		toNotDisplay.iAlphaValue = 0
		
		IF iDebugIndex > -1 AND iDebugTeam > -1
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Locate ", iDebugIndex, " for team ", iDebugTeam, " set to not display." )
		ENDIF
	ENDIF
ENDPROC

// Instantly sets this locate to full opacity and to the displaying state
PROC SET_LOCATE_TO_DISPLAY( locate_fade_out_data& toDisplay, INT iDebugIndex = -1, INT iDebugTeam = -1)
	IF toDisplay.eState != eLocateFadeOutState_DISPLAYING
		toDisplay.eState = eLocateFadeOutState_DISPLAYING
		toDisplay.iAlphaValue = 255
		
		IF iDebugIndex > -1 AND iDebugTeam > -1
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Locate ", iDebugIndex, " for team ", iDebugTeam, " set to display." )
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: This function does all of the updates that are necessary for fading in/out coronas
///    
/// PARAMS:
///    toUpdate - This is the struct of the locate/corona you want to fade in/out
///    iDebugIndex - This is for debug output - the index of the corona/locate
///    iDebugTeam - This is for debug output - the index of the team who triggered this change
PROC UPDATE_LOCATE_FADE_STRUCT_FOR_LINE_MARKER( locate_fade_out_data& toUpdate,
								INT iDebugIndex = -1,
								INT iDebugTeam = -1, INT iDistance = -1 )
	toUpdate.eState = eLocateFadeOutState_DO_DISTANCE_FADING

	toUpdate.iAlphaValue = CEIL(255 - ((iDistance - 50.0) * -4.5))
	PRINTLN( "[RCC MISSION][AW LINE] toUpdate.iAlphaValue:," ,toUpdate.iAlphaValue)

	IF toUpdate.iAlphaValue >= 255
		toUpdate.iAlphaValue = 255	
		//toUpdate.eState = eLocateFadeOutState_DISPLAYING
		IF iDebugIndex > -1 AND iDebugTeam > -1
			PRINTLN( "[RCC MISSION][AW LINE] Locate ", iDebugIndex, " for team ", iDebugTeam, " has fully faded in." )
		ENDIF
	ENDIF

	IF toUpdate.iAlphaValue <= 30
		toUpdate.iAlphaValue = 30
		IF iDebugIndex > -1 AND iDebugTeam > -1
			PRINTLN( "[RCC MISSION][AW LINE]] Locate ", iDebugIndex, " for team ", iDebugTeam, " has disappeared" )
		ENDIF
	ENDIF
	
ENDPROC

PROC DRAW_RACE_STYLE_MARKER(VECTOR vLoc, INT iLoc, INT iAlphaOverride = -1)
	
	INT iteam = MC_playerBD[iPartToUse].iteam
	HUD_COLOURS TeamColour
	VECTOR vCreationOffset
	
	IF iTeam = -1 OR iTeam >= FMMC_MAX_TEAMS
		PRINTLN("[MMacK][DRAW_RACE_STYLE_MARKER] INVALID TEAM SELECTED") 
		EXIT
	ENDIF
	
	// Don't draw the marker if the objective is blocked
	IF IS_OBJECTIVE_BLOCKED()
		#IF IS_DEBUG_BUILD
		IF boutputdebugspam
			PRINTLN("[JS] url:bugstar:2503517 - IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, IS_OBJECTIVE_BLOCKED")
		ENDIF
		#ENDIF
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS, ciLoc_BS_HideMarker)
		#IF IS_DEBUG_BUILD
		IF boutputdebugspam
			PRINTLN("[JS] url:bugstar:2503517 - IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS, ciLoc_BS_HideMarker) = TRUE")
		ENDIF
		#ENDIF
		EXIT
	ENDIF
	
	IF NOT IS_PLAYER_IN_REQUIRED_LOCATION_VEHICLE(PlayerPedToUse, iteam, iLoc)
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_SpecificVehModelFullBlocker)
	AND MC_playerBD[iLocalPart].iPartIAmSpectating = -1

		#IF IS_DEBUG_BUILD
		IF boutputdebugspam
			PRINTLN("[JS] url:bugstar:2503517 - NOT IS_PLAYER_IN_REQUIRED_LOCATION_VEHICLE(PlayerPedToUse, iteam, iLoc) = TRUE")
		ENDIF
		#ENDIF
		EXIT
	ENDIF
	
	IF g_HeistApartmentDropoffPanStarted
	OR g_HeistGarageDropoffPanStarted
	OR NETWORK_IS_PLAYER_IN_MP_CUTSCENE(LocalPlayer)
		EXIT
	ENDIF
	
	INT iR, iG, iB, iA
	INT iR2, iG2, iB2, iA2
	CHECKPOINT_TYPE checkType
	VECTOR vPointAt
	
	GET_HUD_COLOUR(HUD_COLOUR_YELLOWLIGHT, iR, iG, iB, iA)
	iA = 150
	
	GET_HUD_COLOUR(HUD_COLOUR_NORTH_BLUE, iR2, iG2, iB2, iA2)
	iA2 = 150
	
	IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_CAPTURE_AREA
	AND IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
		IF g_iTheWinningTeam = -1
			//Show Yellow = Tied
			GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
		ELSE
			TeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(g_iTheWinningTeam, PlayerToUse)
			//Team 0 - Orange
			//Team 1 - Green
			//Team 2 - Pink
			//Team 3 - Purple
			PRINTLN( "[RCC MISSION][AW SMOKE] TEAM ",g_iTheWinningTeam, "is winning")
			GET_HUD_COLOUR(TeamColour, iR, iG, iB, iA)
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin > 0
	AND NOT ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(iTeam)
		GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
		iA = 150
	ENDIF
	
	INT iTime = GET_CLOCK_HOURS()
	
	IF iTime > 6
	AND iTime < 20
		//The race creator makes these markers more obvious during daytime, 1725777
		
		iA = ROUND(TO_FLOAT(iA) * 1.2)
		iA2 = ROUND(TO_FLOAT(iA2) * 1.4)
	ENDIF
	
	
	IF iAlphaOverride > -1
	AND iAlphaOverride < 256
		FLOAT fOverride = (TO_FLOAT(iAlphaOverride) / 255.0)
		
		iA = ROUND(TO_FLOAT(iA) * fOverride)
		iA2 = ROUND(TO_FLOAT(iA2) * fOverride)
	ENDIF
	
	IF NOT IS_BIT_SET(iCheckpointBS, iLoc) // The checkpoint doesn't exist yet, make it:
		
		VECTOR vUnitVec = <<0, 1, 0>>
		
		RotateVec( vUnitVec, <<0, 0, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fHeading[iteam]>> )
		
		vPointAt = vLoc + vUnitVec
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_RaceLocate_Chckpt1)
			IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iCoronaSize = LOCATION_CORONA_SIZE_ACTUAL
				checkType = CHECKPOINT_RACE_GROUND_CHEVRON_1_BASE  
			ELSE
				checkType = CHECKPOINT_RACE_GROUND_CHEVRON_1
			ENDIF
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_RaceLocate_Chckpt2)
			checkType = CHECKPOINT_RACE_GROUND_CHEVRON_2
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_RaceLocate_Chckpt3)
			checkType = CHECKPOINT_RACE_GROUND_CHEVRON_3
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_RaceLocate_Lap)
			checkType = CHECKPOINT_RACE_GROUND_LAP
		ELSE 
			checkType = CHECKPOINT_RACE_GROUND_FLAG
		ENDIF
		
		FLOAT fSize = FMMC_CHECKPOINT_SIZE*1*0.66
		
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iCoronaSize = LOCATION_CORONA_SIZE_ACTUAL
			fSize = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fRadius[0] * 2
		ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iCoronaSize = LOCATION_CORONA_SIZE_SMALL
			fSize *= 0.5
		ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iCoronaSize = LOCATION_CORONA_SIZE_RACE
			fSize = (8.5 *1.333)
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iCoronaSize = LOCATION_CORONA_SIZE_RACE
			IF checkType != CHECKPOINT_RACE_GROUND_FLAG
				checkType = CHECKPOINT_RACE_GROUND_CHEVRON_1_BASE
			ENDIF
			PRINTLN("[RCC MISSION] DRAW_RACE_STYLE_MARKER Size Race")
		ENDIF

		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iCoronaSize = LOCATION_CORONA_SIZE_RACE
			IF checkType = CHECKPOINT_RACE_GROUND_FLAG
				vCreationOffset = <<0,0,6>>
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_AngleCheckpoint)
					vCreationOffset = <<0,0,-2.5>> //-2 //<<0,0,2>> 
				ELSE
					vCreationOffset = <<0,0,-2>>
				ENDIF
			ENDIF
		ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iCoronaSize = LOCATION_CORONA_SIZE_ACTUAL
			vCreationOffset = <<0,0,0>>
		ELSE
			vCreationOffset =  <<0,0,4>>
		ENDIF

		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iCoronaSize = LOCATION_CORONA_SIZE_ACTUAL
			ciLocCheckpoint[iLoc] = CREATE_CHECKPOINT(checkType, vLoc + vCreationOffset, vPointAt + <<0,0,5>>, fSize, iR, iG, iB, iA)
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_AngleCheckpoint)
			//Create invisible 
			ciLocCheckpoint[iLoc] = CREATE_CHECKPOINT(checkType, vLoc + vCreationOffset, vPointAt + <<0,0,5>>, fSize, iR, iG, iB, 0)
			PRINTLN("[RCC MISSION] DRAW_RACE_STYLE_MARKER | ANGLED CHECKPOINT CREATED")
		ELSE
			ciLocCheckpoint[iLoc] = CREATE_CHECKPOINT(checkType, vLoc + vCreationOffset, vPointAt + <<0,0,5>>, fSize, iR, iG, iB, iA)
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iCoronaSize = LOCATION_CORONA_SIZE_RACE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_AngleCheckpoint)
				SET_CHECKPOINT_CYLINDER_HEIGHT(ciLocCheckpoint[iLoc],9.5,9.5,100)
				SET_CHECKPOINT_INSIDE_CYLINDER_HEIGHT_SCALE(ciLocCheckpoint[iLoc],0.75)
			ELSE
				SET_CHECKPOINT_CYLINDER_HEIGHT(ciLocCheckpoint[iLoc],9.5,9.5,100)
				SET_CHECKPOINT_INSIDE_CYLINDER_HEIGHT_SCALE(ciLocCheckpoint[iLoc],0.75)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_AngleCheckpoint)
			SET_BIT(bsCheckPointCreated,iLoc)
		ENDIF
		
		
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iCoronaSize = LOCATION_CORONA_SIZE_ACTUAL
			PRINTLN("[RCC MISSION] DRAW_RACE_STYLE_MARKER Overriding height")
			SET_CHECKPOINT_CYLINDER_HEIGHT(ciLocCheckpoint[iLoc],9,9,100)
			SET_CHECKPOINT_INSIDE_CYLINDER_HEIGHT_SCALE(ciLocCheckpoint[iLoc],0.2)
			SET_CHECKPOINT_INSIDE_CYLINDER_SCALE(ciLocCheckpoint[iLoc],0.5)
		ENDIF

		SET_CHECKPOINT_RGBA2(ciLocCheckpoint[iLoc], iR2, iG2, iB2, iA2)
		
		SET_BIT(iCheckpointBS, iLoc)
		
		PRINTLN("[RCC MISSION] DRAW_RACE_STYLE_MARKER - Creating checkpoint for iLoc ",iLoc," at ",vLoc)
		
	ELSE // Checkpoint already exists, update the alpha:
		SET_CHECKPOINT_RGBA(ciLocCheckpoint[iLoc], iR, iG, iB, iA)
		SET_CHECKPOINT_RGBA2(ciLocCheckpoint[iLoc], iR2, iG2, iB2, iA2)
	ENDIF
	
ENDPROC

PROC CALCULATE_CAPTURE_LOCATION_STATE_AND_COLOUR(INT iLoc)
	INT iA
	IF iLoc > -1
		IF MC_serverBD.iLocOwner[iLoc] != -1
			IF IS_GOTO_LOCATE_BEING_CONTESTED(iLoc)
				iCaptureLocateRed[iLoc] = 255
				iCaptureLocateGreen[iLoc] = 0
				iCaptureLocateBlue[iLoc] = 0
				eCaptureLocateState[iLoc] = eCapture_contested
			ELSE
				GET_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_serverBD.iLocOwner[iLoc], PlayerToUse), iCaptureLocateRed[iLoc], iCaptureLocateGreen[iLoc], iCaptureLocateBlue[iLoc], iA)
				eCaptureLocateState[iLoc] = eCapture_captured
			ENDIF
		ELSE
			IF IS_GOTO_LOCATE_BEING_CONTESTED(iLoc, FALSE, TRUE)
				iCaptureLocateRed[iLoc] = 255
				iCaptureLocateGreen[iLoc] = 0
				iCaptureLocateBlue[iLoc] = 0
				eCaptureLocateState[iLoc] = eCapture_contested
			ELSE
				iCaptureLocateRed[iLoc] = 255
				iCaptureLocateGreen[iLoc] = 255
				iCaptureLocateBlue[iLoc] = 0
				eCaptureLocateState[iLoc] = eCapture_neutral
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_THIS_A_LINE_STYLE_LOCATE_MAKER(INT iLoc)
	BOOL bLineStyle = FALSE
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_UseLineMarker)
		bLineStyle = TRUE
	ENDIF
	
	RETURN bLineStyle
ENDFUNC

FUNC BOOL IS_THIS_A_RACE_STYLE_LOCATE_MARKER(INT iLoc)
	
	BOOL bRaceStyle = FALSE
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_RaceLocate_Chckpt1)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_RaceLocate_Chckpt2)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_RaceLocate_Chckpt3)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_RaceLocate_Lap)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_RaceLocate_Finish)
		bRaceStyle = TRUE
	ENDIF
	
	RETURN bRaceStyle
	
ENDFUNC

/// PURPOSE: Draws a single DRAW_LOCATE_MARKER or DRAW_AIR_MARKER
///    
/// PARAMS:
///    iLoc - The index of the location we're going to draw
///    iOverrideAlpha - A value from 0-255 for overriding the alpha channel
PROC DRAW_ONE_LOCATION_MARKER(INT iLoc, INT iOverrideAlpha = -1, BOOL bEnemyMarker = FALSE, INT iTeamWhoOwnThis = -1)

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS, ciLoc_BS_HideMarker)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED)
		EXIT
	ENDIF
	
	IF IS_OBJECTIVE_BLOCKED()
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_UseLineMarker)
		EXIT
	ENDIF
	
	//Race MArker
	IF IS_THIS_A_RACE_STYLE_LOCATE_MARKER(iLoc)
		DRAW_RACE_STYLE_MARKER(GET_LOCATION_VECTOR(iLoc), iLoc, iOverrideAlpha)
		EXIT
	ENDIF

	//Air Marker
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iArial[0] = 1
		DRAW_AIR_MARKER(GET_LOCATION_VECTOR(iLoc), FALSE, iLoc)
		EXIT
	ENDIF
		
	VECTOR vLocPos = GET_LOCATION_VECTOR(iLoc) - <<0.0, 0.0, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fLocateDepth>>
		
	IF NOT IS_THIS_A_LINE_STYLE_LOCATE_MAKER(iLoc)
		bEnemyMarker = FALSE
	ENDIF

	DRAW_LOCATE_MARKER(vLocPos, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fRadius[0], FALSE,
				iLoc, iOverrideAlpha, bEnemyMarker, iTeamWhoOwnThis)
	
ENDPROC

PROC CHECK_FADED_OUT_CHECKPOINT( INT iLoc, INT iTeam, INT iRule, BOOL bLaps = FALSE)
	
	IF IS_BIT_SET(iCheckpointBS, iLoc)
		
		BOOL bDelete
		
		IF sFadeOutLocations[iLoc].eState = eLocateFadeOutState_AFTER_FADE_OUT
			bDelete = TRUE
			PRINTLN("[RCC MISSION] CHECK_FADED_OUT_CHECKPOINT - Delete faded out checkpoint for iLoc ",iLoc)
		ELIF sFadeOutLocations[iLoc].eState != eLocateFadeOutState_FADING_OUT
			IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] > iRule
				//If it's not an objective right now, clear it!
				bDelete = TRUE

				IF bLaps 
					sFadeOutLocations[iLoc].eState = eLocateFadeOutState_BEFORE_FADE_IN
					sFadeOutLocations[iLoc].iAlphaValue = -1
				ELSE
					sFadeOutLocations[iLoc].eState = eLocateFadeOutState_AFTER_FADE_OUT
					sFadeOutLocations[iLoc].iAlphaValue = 0
				ENDIF
				PRINTLN("[RCC MISSION] CHECK_FADED_OUT_CHECKPOINT - Getting rid of locate ",iLoc," as it isn't an objective any more!")
			ENDIF
		ENDIF
		
		IF bDelete
			DELETE_CHECKPOINT(ciLocCheckpoint[iLoc])
			CLEAR_BIT(iCheckpointBS, iLoc)
		ENDIF
	ENDIF
	
ENDPROC

// ##### ---------------------------------------------------------------------------
// ##### Section Name: Blipping										----------------
// ##### Description: Functions for blipping a location				----------------
// ##### ---------------------------------------------------------------------------
// ##### ---------------------------------------------------------------------------

PROC CREATE_BLIP_FOR_MP_AREA( INT iLoc, INT iPlayerTeam )
	
	CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION] Attempting to creating blip for MP area.")
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS, ciLoc_BS_HideCaptAreaOnMap)
	
		CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Hide area bit not set. Creating blip for MP area. Location = ", iLoc, " PlayerTeam = ", iPlayerTeam, ".")
	
		IF NOT DOES_BLIP_EXIST(LocBlip1[iLoc])
			LocBlip1[iLoc] = ADD_BLIP_FOR_RADIUS(GET_LOCATION_VECTOR(iLoc), g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fRadius[0])
		ENDIF
		
		IF MC_serverBD.iLocOwner[iLoc] = -1
			SET_BLIP_COLOUR( LocBlip1[iLoc], BLIP_COLOUR_YELLOW )
		ELIF MC_serverBD.iLocOwner[iLoc] = iPlayerTeam	
			WinningTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM( MC_serverBD.iLocOwner[iLoc], PlayerToUse)
			SET_BLIP_COLOUR( LocBlip1[iLoc],  ENUM_TO_INT(WinningTeamColour))
			PRINTLN("[RCC MISSION][AW SMOKE] SETTING BLIP COLOUR - 1")
			//SET_BLIP_COLOUR( LocBlip1[iLoc], BLIP_COLOUR_BLUE )
		ELSE
			SET_BLIP_COLOUR_FROM_HUD_COLOUR( LocBlip1[iLoc], GET_HUD_COLOUR_FOR_FMMC_TEAM( MC_serverBD.iLocOwner[iLoc], PlayerToUse) )
		ENDIF
		
		IF GET_CORRECT_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iAggroIndexBS_Entity_Loc) != BLIP_COLOUR_DEFAULT
			SET_BLIP_COLOUR(LocBlip1[iLoc], GET_BLIP_COLOUR_FROM_CREATOR(GET_CORRECT_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iAggroIndexBS_Entity_Loc)))
		ENDIF
				
		IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_CAPTURE_AREA
		AND IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
			IF g_iTheWinningTeam = -1
				//Show Yellow = Tied
				SET_BLIP_COLOUR(LocBlip1[iLoc], BLIP_COLOUR_YELLOW)
			ELSE
				WinningTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(g_iTheWinningTeam, PlayerToUse)
				// Winning team
				// 0 - Orange 
				// 1 - Green
				// 2 - Pink
				// 3 - Purple
				PRINTLN( "[RCC MISSION][AW SMOKE] TEAM ",g_iTheWinningTeam, "is winning")
				PRINTLN("[RCC MISSION][AW SMOKE] SETTING BLIP COLOUR - 3")
				SET_BLIP_COLOUR(LocBlip1[iLoc], ENUM_TO_INT(WinningTeamColour))
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_CONTESTED_COLOUR_FOR_LOCATES)
				IF IS_GOTO_LOCATE_BEING_CONTESTED(iLoc)
					SET_BLIP_COLOUR(LocBlip1[iLoc], BLIP_COLOUR_RED)
				ENDIF
			ENDIF
		ENDIF

		SET_BLIP_ALPHA(LocBlip1[iLoc], 150)
		SET_BLIP_PRIORITY(LocBlip1[iLoc], BLIPPRIORITY_LOW)
				
	ENDIF
ENDPROC

FUNC BOOL SHOULD_LOCATION_BE_BLIPPED(INT iLoc, BLIP_RUNTIME_VARS_STRUCT &sBlipRuntimeVars)

	IF SHOULD_ENTITY_BLIP_BE_FORCED_OFF(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_ENTITY_BLIP_BE_FORCED_ON(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct)
		RETURN TRUE
	ENDIF
		
	IF NOT SHOULD_ENTITY_BE_BLIPPED(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct, sBlipRuntimeVars, NULL, FALSE)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BLIP_COORD_IN_SAME_INTERIOR(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct, GET_LOCATION_VECTOR(iLoc))
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_REMOVE_LOCATION_BLIP(INT iLoc, BLIP_RUNTIME_VARS_STRUCT &sBlipRuntimeVars)

	FLOAT fRange = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipRange
	FLOAT fHeightRange = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipHeightDifference
	VECTOR vLocBlipPos = GET_LOCATION_VECTOR(iLoc)
	
	IF NOT SHOULD_LOCATION_BE_BLIPPED(iLoc, sBlipRuntimeVars)
		PRINTLN("SHOULD_REMOVE_LOCATION_BLIP - Blip: ", iLoc, " - SHOULD_LOCATION_BE_BLIPPED is returning FALSE")
		RETURN TRUE
	ENDIF
	
	IF NOT WAS_PLAYER_PREVIOUSLY_IN_RANGE_OF_BLIP(sBlipRuntimeVars)
		IF fRange > 0.0
		AND fHeightRange > 0.0
			PRINTLN("SHOULD_REMOVE_LOCATION_BLIP - Blip: ", iLoc, " - Both show range and show height are set")
			IF NOT IS_PLAYER_WITHIN_COORD_BLIP_RANGE(vLocBlipPos, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct)
			OR NOT IS_COORD_WITHIN_HEIGHT_DIFFERENCE_RANGE_FOR_BLIP(vLocBlipPos, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct)
				RETURN TRUE
			ENDIF
		ELIF fRange > 0.0
			PRINTLN("SHOULD_REMOVE_LOCATION_BLIP - Blip: ", iLoc, " - Just show range is set")
			IF NOT IS_PLAYER_WITHIN_COORD_BLIP_RANGE(vLocBlipPos, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct)
				RETURN TRUE
			ENDIF
		ELIF fHeightRange > 0.0
			PRINTLN("SHOULD_REMOVE_LOCATION_BLIP - Blip: ", iLoc, " - Just show height range is set")
			IF NOT IS_COORD_WITHIN_HEIGHT_DIFFERENCE_RANGE_FOR_BLIP(vLocBlipPos, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct)
				RETURN TRUE
			ENDIF
		ENDIF		
	ENDIF
		
	RETURN FALSE
	
ENDFUNC

PROC CREATE_LOCATION_BLIP(INT iLoc, FLOAT fDistToloc, BOOL bSetRoute, BOOL bEnemyBlip = FALSE)
	INT i
	
	IF SHOULD_REMOVE_LOCATION_BLIP(iLoc, sMissionLocationsLocalVars[iLoc].sBlipRuntimeVars)
		IF DOES_BLIP_EXIST(LocBlip[iLoc])
			CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][Blip] CREATE_LOCATION_BLIP - Location ", iLoc, " Removing blip. We are at fDistToLoc: ", fDistToLoc)
			REMOVE_BLIP(LocBlip[iLoc])
		ENDIF
		
		EXIT
	ENDIF
	
	IF IS_BLIP_SET_AS_HIDDEN(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct)
		EXIT
	ENDIF
	
	IF NOT DOES_BLIP_EXIST(LocBlip[iLoc])
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_UseLineMarker)	
			LocBlip[iLoc] = ADD_BLIP_FOR_AREA(GET_LOCATION_VECTOR(iLoc), 1, 1)
			PRINTLN("[RCC MISSION][Blip] CREATE_LOCATION_BLIP - Creating area blip for line marker location: ", iLoc)
			
			FOR i = 0 TO i_NUMBER_OF_GRADIENT_BLIPS -1
				IF NOT DOES_BLIP_EXIST(LineGradientBlips[i][iLoc])
					
					FLOAT multiplier
					IF i = 0
						multiplier =  g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fRadius[0] 
						multiplier *= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipScale
					ELSE
						multiplier =  g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fRadius[0] - (i * (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fRadius[0] / 8))
						multiplier *= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipScale
					ENDIF
					
					LineGradientBlips[i][iLoc] = ADD_BLIP_FOR_AREA(GET_LOCATION_VECTOR(iLoc), multiplier, 5)
					SET_BLIP_AS_SHORT_RANGE(LineGradientBlips[i][iLoc],TRUE)
					SET_BLIP_ROTATION(LineGradientBlips[i][iLoc], ROUND(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fDirection))
					
				ENDIF
			ENDFOR
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_OnlyShowBlipWhenOffRadar)
				LocBlip[iLoc] = ADD_BLIP_FOR_AREA( GET_LOCATION_VECTOR(iLoc) ,0.01, 0.01)
				PRINTLN("[RCC MISSION][Blip] CREATE_LOCATION_BLIP - Creating area blip for off radar location: ", iLoc)
			ELSE
				LocBlip[iLoc] = ADD_BLIP_FOR_COORD(GET_LOCATION_VECTOR(iLoc))
				PRINTLN("[RCC MISSION][Blip] CREATE_LOCATION_BLIP - Creating coord blip for location: ", iLoc)
			ENDIF
		ENDIF
		
		CREATE_BLIP_WITH_CREATOR_SETTINGS(
			FMMC_BLIP_LOCATION, 
			LocBlip[iLoc], 
			g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct, 
			sMissionLocationsLocalVars[iLoc].sBlipRuntimeVars, 
			NULL, 
			iLoc, 
			EMPTY_VEC(), 
			g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iAggroIndexBS_Entity_Loc
		)
		
		IF bSetRoute
						
			IF fDistToLoc > g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iGPSCutOff[0]
			AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iGPSCutOff[0] != -1
				IF NOT USING_HEIST_SPECTATE() AND NOT bIsAnySpectator
					CLEAR_GPS_FLAGS()
					SET_BLIP_ROUTE(LocBlip[iLoc], TRUE)
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS,ciLoc_BS_AvoidHighwayGPS)
						SET_GPS_FLAGS(GPS_FLAG_AVOID_HIGHWAY)
					ELSE
						SET_GPS_FLAGS(GPS_FLAG_NONE)
					ENDIF
				ELSE
					CLEAR_GPS_FLAGS()
					SET_BLIP_ROUTE(LocBlip[iLoc], FALSE)
				ENDIF
			ENDIF
		ENDIF

		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE3_IGNORE_GPS_BLOCKING)
				SET_IGNORE_NO_GPS_FLAG(TRUE)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_UseLineMarker)	
			SET_BLIP_SPRITE(LocBlip[iLoc], RADAR_TRACE_EDGE_POINTER)
			IF bEnemyBlip
			AND NOT ((IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_RunningBackMarkerAsRedT1)
			AND MC_PlayerBD[iPartToUse].iTeam = 0)
			OR MC_PlayerBD[iPartToUse].iTeam != 0)
			OR NOT ((IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_RunningBackMarkerAsRedT2)
			AND MC_PlayerBD[iPartToUse].iTeam = 1)
			OR MC_PlayerBD[iPartToUse].iTeam != 1)
				SET_BLIP_COLOUR( LocBlip[iLoc],  BLIP_COLOUR_BLUE )
			ELSE
				SET_BLIP_COLOUR( LocBlip[iLoc],  BLIP_COLOUR_RED )
				SET_BLIP_PRIORITY( LocBlip[iLoc],BLIP_PRIORITY_HIGHEST_SPECIAL_LOW)
			ENDIF
			
			iBlipTeam[iLoc] = MC_playerBD[iPartToUse].iteam
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_OnlyShowBlipWhenOffRadar)
				SET_BLIP_SPRITE(LocBlip[iLoc], RADAR_TRACE_EDGE_POINTER)
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION][Blip] CREATE_LOCATION_BLIP - CREATE_LOCATION_BLIP - iLoc blip exists ", iLoc)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_UseLineMarker)	
			IF iBlipTeam[iLoc] != MC_playerBD[iPartToUse].iteam
				FOR i = 0 TO  i_NUMBER_OF_GRADIENT_BLIPS -1
					IF DOES_BLIP_EXIST(LineGradientBlips[i][iLoc])
						REMOVE_BLIP(LineGradientBlips[i][iLoc])
					ENDIF
				ENDFOR
				REMOVE_BLIP(LocBlip[iLoc])
				PRINTLN("[RCC MISSION][Blip] CREATE_LOCATION_BLIP - removing blips for line since player team has changed from: ",iBlipTeam[iLoc],"MC_playerBD[iPartToUse].iteam", " for location: ",iLoc )
				iBlipTeam[iLoc] = MC_playerBD[iPartToUse].iteam
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CREATE_BLIP_FOR_ENEMY_MARKER_LINES(INT iLoc, Bool bMakeRed = FALSE, BOOL bIgnoreRule = FALSE)

	INT iPlayerTeam = MC_playerBD[iPartToUse].iteam
	INT i

	PRINTLN("[AW_MARKER - CREATE_BLIP_FOR_ENEMY_MARKER_LINES -  ")
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_DisplayOtherTeamsMarker)
		FOR i = 0 TO MC_ServerBD.iNumberOfTeams -1
			IF i != iPlayerTeam
				IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][i] = MC_serverBD_4.iCurrentHighestPriority[i]
				OR bIgnoreRule
				OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_IgnoreRuleForThisBlip)
					IF NOT DOES_BLIP_EXIST(LocBlip[iLoc])
						//Line Stuff
						PRINTLN("[AW_MARKER - CREATE_BLIP_FOR_ENEMY_MARKER_LINES - iPlayerTeam  ", iPlayerTeam)
						PRINTLN("[AW_MARKER - CREATE_BLIP_FOR_ENEMY_MARKER_LINES - i  ", i)
						PRINTLN("[AW_MARKER - CREATE_BLIP_FOR_ENEMY_MARKER_LINES - iLoc  ", iLoc)
						IF bMakeRed
							CREATE_LOCATION_BLIP(iLoc, 0, FALSE, TRUE)
							PRINTLN("[AW_MARKER - SETTING BLIP TO BE RED  ", iLoc)
						ELSE
							CREATE_LOCATION_BLIP(iLoc, 0, FALSE, TRUE)
							PRINTLN("[AW_MARKER - SETTING BLIP TO BE BLUE  ", iLoc)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
ENDPROC

PROC DRAW_LOCATE_RADIUS_BLIP(INT iRule, INT iLoc)
	
	INT iAlphaChange
	
	
	IF iRule < FMMC_MAX_RULES
	AND iRule > -1
	AND iLoc < FMMC_MAX_GO_TO_LOCATIONS
	AND iLoc > -1
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iPartToUse].iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_PLAYER_LOCATE_RADIUS_BLIP)
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin > 0
			INT iAlpha = 175
			
			IF iLoc != iCurrentLocateBlipped
				IF DOES_BLIP_EXIST(biLocateRadiusBlip)
					REMOVE_BLIP(biLocateRadiusBlip)
					PRINTLN("[JS][LOCATEBLIP] - DRAW_LOCATE_RADIUS_BLIP - Removing blip")
				ENDIF
			ENDIF
			
			iAlphaChange = 100 / MC_serverBD.iNumberOfPlayingPlayers[MC_PlayerBD[iPartToUse].iTeam]
			
			IF NOT DOES_BLIP_EXIST(biLocateRadiusBlip)
				biLocateRadiusBlip = ADD_BLIP_FOR_RADIUS(GET_ENTITY_COORDS(PlayerPedToUse,FALSE), CALCULATE_MINIMUM_DISTANCE_BETWEEN_TEAM_MATES(iLoc,MC_playerBD[iPartToUse].iteam))
				PRINTLN("[JS][LOCATEBLIP] - DRAW_LOCATE_RADIUS_BLIP - Adding blip, Radius = ", CALCULATE_MINIMUM_DISTANCE_BETWEEN_TEAM_MATES(iLoc,MC_playerBD[iPartToUse].iteam))
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(biLocateRadiusBlip, GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iPartToUse].iteam, PlayerToUse))
				iTempActivePlayers = MC_serverBD.iNumberOfPlayingPlayers[MC_PlayerBD[iPartToUse].iTeam]
				iCurrentLocateBlipped = iLoc
			ELSE
				IF IS_ENTITY_ALIVE(PlayerPedToUse)
					SET_BLIP_COORDS(biLocateRadiusBlip, GET_ENTITY_COORDS(PlayerPedToUse))
				ENDIF
				
				IF iTempActivePlayers != MC_serverBD.iNumberOfPlayingPlayers[MC_PlayerBD[iPartToUse].iTeam]
					REMOVE_BLIP(biLocateRadiusBlip)
				ENDIF
				
				IF NOT HAS_NET_TIMER_STARTED(tdRadiusBlipAlphaTimer)
					START_NET_TIMER(tdRadiusBlipAlphaTimer)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(tdRadiusBlipAlphaTimer, ciAlphaChangeTime)
						INT i
						PRINTLN("[PLAYER_LOOP] - DRAW_LOCATE_RADIUS_BLIP")
						FOR i = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() -1
							IF i != iPartToUse
								PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
								IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
									IF MC_playerBD[iPartToUse].iteam = MC_PlayerBD[i].iteam
										IF VDIST2(GET_ENTITY_COORDS(PlayerPedToUse), GET_ENTITY_COORDS(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(tempPart)),FALSE)) <= POW(CALCULATE_MINIMUM_DISTANCE_BETWEEN_TEAM_MATES(iLoc,MC_playerBD[iPartToUse].iteam), 2)
											iAlpha = CLAMP_INT((iAlpha - iAlphaChange), 75, 175)
											PRINTLN("[JS][LOCATEBLIP] - DRAW_LOCATE_RADIUS_BLIP - Part ", i," is in range, decreasing alpha to ", iAlpha)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						IF DOES_BLIP_EXIST(biLocateRadiusBlip)
							SET_BLIP_ALPHA(biLocateRadiusBlip, iAlpha)
						ENDIF
						REINIT_NET_TIMER(tdRadiusBlipAlphaTimer)
					ENDIF
				ENDIF
			ENDIF
		ELSE	
			IF DOES_BLIP_EXIST(biLocateRadiusBlip)
				REMOVE_BLIP(biLocateRadiusBlip)
				PRINTLN("[JS][LOCATEBLIP] - DRAW_LOCATE_RADIUS_BLIP - Removing blip fTeamMembersNeededWithin <= 0 (", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin,")")
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(biLocateRadiusBlip)
			REMOVE_BLIP(biLocateRadiusBlip)
			PRINTLN("[JS][LOCATEBLIP] - DRAW_LOCATE_RADIUS_BLIP - Invalid rule removing blip")
		ENDIF
	ENDIF
ENDPROC

// url:bugstar:3265534
FUNC BOOL IS_WRONG_BLIP_LOCATION_BEING_USED(BLIP_INDEX biBlip, INT iLoc)
	RETURN VDIST2(GET_BLIP_COORDS(biBlip), GET_LOCATION_VECTOR(iLoc)) > 1.0
ENDFUNC

PROC PROCESS_LOCATION_OUTFIT_BLIP_RANGE_RESET(INT iLoc)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_RequiresStartingOutfitForUnlimitedBlipRange)
		EXIT
	ENDIF
	
	IF MC_playerBD[iParttoUse].iStartingOutfit = -1 	
		EXIT
	ENDIF
	
	IF MC_playerBD[iParttoUse].iOutfit != MC_playerBD[iLocalPart].iStartingOutfit
	AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipRange != 10.0
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipRange = 10.0
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipScale -= 0.3
		PRINTLN("[LM][PROCESS_LOCATION_OUTFIT_BLIP_RANGE_RESET] - Setting fBlipRange to: ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipRange)
	ENDIF
ENDPROC

PROC CLEANUP_NON_PROCESSED_LOCATIONS(INT iLocation)
	IF DOES_BLIP_EXIST(LocBlip[iLocation])
		REMOVE_BLIP(LocBlip[iLocation])
		PRINTLN("CLEANUP_NON_PROCESSED_LOCATIONS - location: ", iLocation, " Removing Blip - Not being processed at this time.")
	ENDIF
	IF DOES_BLIP_EXIST(LocBlip1[iLocation])
		REMOVE_BLIP(LocBlip1[iLocation])
		PRINTLN("CLEANUP_NON_PROCESSED_LOCATIONS - location: ", iLocation, " Removing LocBlip1 (area) - Not being processed at this time.")
	ENDIF
ENDPROC

FUNC BOOL CAN_LOCATION_HAVE_BLIP_RECOLOURED_FOR_REQUIREMENT(INT iLoc, INT iTeam)
	
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocPreReqRequired != ciPREREQ_None
	OR g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].mnVehicleNeeded[iTeam] != DUMMY_MODEL_FOR_SCRIPT
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_NeedsCoronaVehicle)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_LOCATION_BLIP_BE_RECOLOURED_FOR_REQUIREMENT(INT iLoc, INT iTeam)
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocPreReqRequired != ciPREREQ_None
		IF NOT IS_PREREQUISITE_COMPLETED(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocPreReqRequired)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_PLAYER_IN_REQUIRED_LOCATION_VEHICLE(PlayerPedToUse, iTeam, iLoc)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_LOCATION_REQUIREMENT_BLIP(INT iLoc, BLIP_INDEX &biBlip)
	
	IF NOT DOES_BLIP_EXIST(biBlip)
		EXIT
	ENDIF
	
	INT iTeam = GET_LOCAL_PLAYER_TEAM(TRUE)
	
	IF NOT IS_TEAM_INDEX_VALID(iTeam)
		EXIT
	ENDIF
	
	IF NOT CAN_LOCATION_HAVE_BLIP_RECOLOURED_FOR_REQUIREMENT(iLoc, iTeam)
		EXIT
	ENDIF
		
	INT iBlipAlpha = GET_BLIP_ALPHA(biBlip)
	
	IF SHOULD_LOCATION_BLIP_BE_RECOLOURED_FOR_REQUIREMENT(iLoc, iTeam)
		IF iBlipAlpha != g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.iBlipAlpha / 2
			SET_BLIP_ALPHA(biBlip, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.iBlipAlpha / 2)
			SET_BLIP_COLOUR(biBlip, BLIP_COLOUR_WHITE)
			PRINTLN("PROCESS_LOCATION_REQUIREMENT_BLIP - Requirement not met - Updating blip visuals for location: ", iLoc)
		ENDIF
	ELSE
		IF iBlipAlpha != g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.iBlipAlpha
			SET_BLIP_ALPHA(biBlip, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.iBlipAlpha)
			SET_COLOUR_FOR_ENTITY_BLIP(biBlip, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct, FMMC_BLIP_LOCATION, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iAggroIndexBS_Entity_Loc)
			PRINTLN("PROCESS_LOCATION_REQUIREMENT_BLIP - Requirement met -  Updating blip visuals for location: ", iLoc)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DISABLE_LOCATION_ROUTE(INT iLoc, FLOAT fDistToLoc)
	
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iGPSCutOff[0] = -1
		RETURN TRUE
	ENDIF
	
	IF fDistToLoc <= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iGPSCutOff[0]
		RETURN TRUE
	ENDIF
	
	IF IS_BLIP_SET_AS_HIDDEN(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		
		IF DOES_ENTITY_EXIST(viPlayerVeh)
			MODEL_NAMES mnVeh = GET_ENTITY_MODEL(viPlayerVeh)
			IF IS_THIS_MODEL_A_BOAT(mnVeh)
			OR IS_THIS_MODEL_A_HELI(mnVeh)
			OR IS_THIS_MODEL_A_PLANE(mnVeh)	
				RETURN TRUE					
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_LOCATION_BLIP_ROUTE(BLIP_INDEX &biBlip, INT iLoc, INT iPlayerTeam, FLOAT fDistToLoc)
	IF SHOULD_DISABLE_LOCATION_ROUTE(iLoc, fDistToLoc)
		IF DOES_BLIP_HAVE_GPS_ROUTE(biBlip)
			SET_BLIP_ROUTE(biBlip, FALSE)
		ENDIF
	ELSE
		IF MC_serverBD.iNumLocHighestPriority[iPlayerTeam] = 1
			IF bIsAnySpectator
				SET_BLIP_ROUTE(biBlip, FALSE)
			ELSE
				IF NOT DOES_BLIP_HAVE_GPS_ROUTE(LocBlip[iLoc])
					SET_BLIP_ROUTE(biBlip, TRUE)
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS, ciLoc_BS_AvoidHighwayGPS)
						SET_GPS_FLAGS(GPS_FLAG_AVOID_HIGHWAY)
					ELSE
						SET_GPS_FLAGS(GPS_FLAG_NONE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_STAGGERED_LOCATION_BLIP__GO_TO_AND_PHOTO(BLIP_INDEX &biBlip, INT iLoc, INT iPlayerTeam, FLOAT fDistToLoc)
	
	IF NOT DOES_BLIP_EXIST(biBlip)
		IF NOT (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_RemoveBlipWhenInLocate)
		AND IS_LOCAL_PLAYER_IN_LOCATION(iLoc, iPlayerTeam))						
			IF (IS_PLAYER_IN_REQUIRED_LOCATION_VEHICLE(PlayerPedToUse, iPlayerTeam, iLoc) OR NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_SpecificVehModelFullBlocker))
			AND (MC_playerBD[iPartToUse].iCurrentLoc != iLoc OR MC_serverBD_4.iGotoLocationDataRule[iLoc][iPlayerTeam] = FMMC_OBJECTIVE_LOGIC_PHOTO)
				
				PRINTLN("PROCESS_STAGGERED_LOCATION_BLIP__GO_TO_AND_PHOTO - iPlayerTeam: ", iPlayerTeam," MC_playerBD[iPartToUse].iteam: ",MC_playerBD[iPartToUse].iteam, " fDistToLoc: ", fDistToLoc)
				CREATE_LOCATION_BLIP(iLoc, fDistToLoc, TRUE)
			ENDIF
		ENDIF
	ELSE
		
		PROCESS_LOCATION_BLIP_ROUTE(biBlip, iLoc, iPlayerTeam, fDistToLoc)
		
		IF (NOT IS_PLAYER_IN_REQUIRED_LOCATION_VEHICLE(PlayerPedToUse, iPlayerTeam, iLoc) AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_SpecificVehModelFullBlocker))
		AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset, PBBOOL_HEIST_SPECTATOR)
		OR (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_RemoveBlipWhenInLocate) AND IS_LOCAL_PLAYER_IN_LOCATION(iLoc,  iPlayerTeam))
		OR IS_WRONG_BLIP_LOCATION_BEING_USED(LocBlip[iLoc], iLoc) // url:bugstar:3265534
		OR SHOULD_REMOVE_LOCATION_BLIP(iLoc, sMissionLocationsLocalVars[iLoc].sBlipRuntimeVars)
			PRINTLN("PROCESS_STAGGERED_LOCATION_BLIP__GO_TO_AND_PHOTO - Removing Location Blip Call 2")
			REMOVE_BLIP(biBlip)
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_LOCATION_BLIP_COLOUR_FOR_TEAM(INT iLocTeam)
	SWITCH iLocTeam
		CASE 0 RETURN BLIP_COLOUR_ORANGE
		CASE 1 RETURN BLIP_COLOUR_GREEN
		CASE 2 RETURN BLIP_COLOUR_PINK
		CASE 3 RETURN BLIP_COLOUR_PURPLE
	ENDSWITCH
	
	RETURN BLIP_COLOUR_YELLOW
ENDFUNC

PROC PROCESS_STAGGERED_LOCATION_BLIP__CONTROL(BLIP_INDEX &biBlip, INT iLoc, INT iPlayerTeam, FLOAT fDistToLoc)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS,ciLoc_BS_HideBlipWhenInLocation)
	OR (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS,ciLoc_BS_HideBlipWhenInLocation) AND MC_playerBD[iPartToUse].iCurrentLoc != iLoc)
		CREATE_LOCATION_BLIP(iLoc, fDistToLoc, TRUE)
	ENDIF
	
	IF NOT DOES_BLIP_EXIST(biBlip)
		EXIT
	ENDIF
	
	IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_CAPTURE_AREA
	AND IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
		IF DOES_BLIP_EXIST(biBlip)
			IF g_iTheWinningTeam = -1
				SET_BLIP_COLOUR(biBlip, BLIP_COLOUR_YELLOW)
			ELSE
				
				SET_BLIP_COLOUR(biBlip, GET_LOCATION_BLIP_COLOUR_FOR_TEAM(g_iTheWinningTeam))
				
				WinningTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(g_iTheWinningTeam, PlayerToUse)

				PRINTLN("[RCC MISSION] PROCESS_STAGGERED_LOCATION_BLIP__CONTROL - Team ",g_iTheWinningTeam, "is winning biBlip")
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_playerBD[iPartToUse].iCurrentLoc != iLoc
		
		PROCESS_LOCATION_BLIP_ROUTE(biBlip, iLoc, iPlayerTeam, fDistToLoc)
		
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciEVERY_FRAME_GOTO_LOC_PROCESSING)
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].iRuleBitsetFive[MC_serverBD_4.iCurrentHighestPriority[iPlayerTeam]], ciBS_RULE5_RED_FILTER_IN_CAP_ZONE)
			AND NOT IS_OBJECTIVE_BLOCKED()
				IF SHOULD_RED_FILTER_SHOW(iPlayerTeam, MC_serverBD_4.iCurrentHighestPriority[iPlayerTeam], iLoc)
					IF NOT ANIMPOSTFX_IS_RUNNING("CrossLine")
						PRINTLN("[JS] PROCESS_STAGGERED_LOCATION_BLIP__CONTROL - Turning on capture filter early")
						IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_RED_FILTER_SOUND_PLAYED)
							PLAY_RED_FILTER_SOUND()
						ENDIF
						ANIMPOSTFX_PLAY("CrossLine", 0, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT DOES_BLIP_EXIST(LocBlip1[iLoc])
	OR ioldLocOwner[iLoc] != MC_serverBD.iLocOwner[iLoc]
		CREATE_BLIP_FOR_MP_AREA(iLoc, iPlayerTeam )
		
		ioldLocOwner[iLoc] = MC_serverBD.iLocOwner[iLoc]
	ELSE	
		IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_CAPTURE_AREA
		AND IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
			IF DOES_BLIP_EXIST(LocBlip1[iLoc])
				IF g_iTheWinningTeam = -1
					SET_BLIP_COLOUR(LocBlip1[iLoc], BLIP_COLOUR_YELLOW)
				ELSE
				
					INT iTeamOverrideBlip
					iTeamOverrideBlip = MC_serverBD.iLocOwner[iLoc]
					
					IF iTeamOverrideBlip > -1
						SET_BLIP_COLOUR(LocBlip1[iLoc], GET_LOCATION_BLIP_COLOUR_FOR_TEAM(iTeamOverrideBlip))
					ELSE
						SET_BLIP_COLOUR(LocBlip1[iLoc], GET_LOCATION_BLIP_COLOUR_FOR_TEAM(g_iTheWinningTeam))
					ENDIF
					
					WinningTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(g_iTheWinningTeam, PlayerToUse)

					PRINTLN("[RCC MISSION] PROCESS_STAGGERED_LOCATION_BLIP__CONTROL - Team ", g_iTheWinningTeam, "is winning LocBlip1[",iLoc,"]")
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

PROC PROCESS_STAGGERED_LOCATION_BLIP__LEAVE(INT iLoc, FLOAT fDistToLoc)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS, ciLoc_BS_HideBlipWhenInLocation)
	OR (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS, ciLoc_BS_HideBlipWhenInLocation) AND MC_playerBD[iPartToUse].iCurrentLoc != iLoc)
		CREATE_LOCATION_BLIP(iLoc, fDistToLoc, FALSE)
	ENDIF
	
ENDPROC

PROC PROCESS_STAGGERED_LOCATION_BLIP(INT iLoc)
	
	INT iPlayerTeam = MC_playerBD[iPartToUse].iTeam
	FLOAT fDistToLoc = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, GET_LOCATION_VECTOR(iLoc))
	
	IF MC_serverBD_4.iGotoLocationDataRule[iLoc][iPlayerTeam] = FMMC_OBJECTIVE_LOGIC_GO_TO
	OR MC_serverBD_4.iGotoLocationDataRule[iLoc][iPlayerTeam] = FMMC_OBJECTIVE_LOGIC_PHOTO
		
		PROCESS_STAGGERED_LOCATION_BLIP__GO_TO_AND_PHOTO(LocBlip[iLoc], iLoc, iPlayerTeam, fDistToLoc)
		
	ELIF MC_serverBD_4.iGotoLocationDataRule[iLoc][iPlayerTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
	
		PROCESS_STAGGERED_LOCATION_BLIP__CONTROL(LocBlip[iLoc], iLoc, iPlayerTeam, fDistToLoc)
		
	ELIF MC_serverBD_4.iGotoLocationDataRule[iLoc][iPlayerTeam] = FMMC_OBJECTIVE_LOGIC_LEAVE_LOCATION
		
		PROCESS_STAGGERED_LOCATION_BLIP__LEAVE(iLoc, fDistToLoc)
		
	ENDIF
			
	IF NOT DOES_BLIP_EXIST(LocBlip[iLoc])
		EXIT
	ENDIF

	PROCESS_LOCATION_REQUIREMENT_BLIP(iLoc, LocBlip[iLoc])
	
	PROCESS_ENTITY_BLIP(LocBlip[iLoc], g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct, GET_LOCATION_VECTOR(iLoc), g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iAggroIndexBS_Entity_Loc)
	
ENDPROC

// ##### --------------------------------------------------------------------------------------
// ##### Section Name: Objective Progression									---------------
// ##### Description: Functions for location objective progression				---------------
// ##### --------------------------------------------------------------------------------------
// ##### --------------------------------------------------------------------------------------

FUNC BOOL IS_LOCATION_AN_OBJECTIVE_FOR_ANY_TEAM(INT iLoc)

	INT iTeam
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iteam] < FMMC_MAX_RULES
			RETURN TRUE
		ENDIF
	ENDFOR

	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_LOCATION_CLEAN_UP_EARLY(INT iLoc)
	
	IF IS_BIT_SET(MC_serverBD_4.iLocationCleanedUpEarlyBitset, iLoc)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_CleanupEarly_Aggro)
	AND HAS_ANY_TEAM_TRIGGERED_AGGRO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iAggroIndexBS_Entity_Loc)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_LOCATION_IGNORED_FOR_TEAM(INT iLoc, INT iTeam)

	IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] >= FMMC_MAX_RULES
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCATION_CURRENT_OBJECTIVE_FOR_ANY_TEAM(INT iLoc)
	
	IF SHOULD_LOCATION_CLEAN_UP_EARLY(iLoc)
		RETURN FALSE
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_DisplayMarkerAsVisualOnly)
		//Needs processed as we are drawing it
		RETURN TRUE
	ENDIF

	INT iTeam
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF NOT IS_LOCATION_IGNORED_FOR_TEAM(iLoc, iTeam)
		AND MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]
			RETURN TRUE
		ENDIF
	ENDFOR

	RETURN FALSE
	
ENDFUNC

PROC PROCESS_LEAVE_AREA_COMPLETION_CONTINUITY(INT iLoc)

	IF iLoc = -1
		EXIT
	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ContentSpecificTracking)
		EXIT
	ENDIF
	
	INT iContentContinuityType = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iContentContinuityType
	IF iContentContinuityType > 0
		PROCESS_SPECIFIC_CONTENT_CONTINUITY_TYPE_TELEMETRY(iContentContinuityType)
		SET_BIT(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, iContentContinuityType)
		PRINTLN("[LEAVEAREA] - PROCESS_LEAVE_AREA_COMPLETION_CONTINUITY - Content Continuity Type Set: ", iContentContinuityType)
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_COUNT_LEAVE_LOCATION(INT iLoc, BOOL bInLocation = TRUE)

	IF MC_serverBD_4.iGotoLocationDataRule[iLoc][MC_playerBD[iPartToUse].iteam] != FMMC_OBJECTIVE_LOGIC_LEAVE_LOCATION
		RETURN FALSE
	ENDIF
	
	IF IS_OBJECTIVE_BLOCKED()
		RETURN TRUE
	ENDIF
	
	IF bInLocation
	
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_InverseRuleLogic)
			//Inverse rule logic set, being in this area counts as being out of all areas.
			IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_IN_INVERSE_LEAVE_AREA_LOCATION)
				SET_BIT(iLocalBoolCheck32, LBOOL32_IN_INVERSE_LEAVE_AREA_LOCATION)
				#IF FEATURE_HEIST_ISLAND
				PROCESS_LEAVE_AREA_COMPLETION_CONTINUITY(iLoc)
				#ENDIF
				PRINTLN("[LEAVEAREA] SHOULD_COUNT_LEAVE_LOCATION - Returning false and setting LBOOL32_IN_INVERSE_LEAVE_AREA_LOCATION")
			ENDIF
			iTempLeaveLoc = -1
			RETURN FALSE
		ELIF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_IN_INVERSE_LEAVE_AREA_LOCATION)
			iTempLeaveLoc = -1
			RETURN FALSE
		ENDIF
	
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[MC_playerBD[iPartToUse].iteam] != ciGOTO_LOCATION_INDIVIDUAL
		AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[MC_playerBD[iPartToUse].iteam] != ciGOTO_LOCATION_WHOLE_TEAM
			//i.e. it could be a locate that requires my existence even without me being on its rule
			RETURN TRUE
		ELSE
			IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][MC_playerBD[iPartToUse].iteam] < iLowestLeaveLocationPriority
				iLowestLeaveLocationPriority = MC_serverBD_4.iGotoLocationDataPriority[iLoc][MC_playerBD[iPartToUse].iteam]
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	PROCESS_LEAVE_AREA_COMPLETION_CONTINUITY(iLoc)
	#ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL AM_I_IN_SECONDARY_LOCATE_IF_I_NEED_TO_BE(INT iLoc)
	
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iLocationInSecondaryLocation, iLoc)
		PRINTLN("AM_I_IN_SECONDARY_LOCATE_IF_I_NEED_TO_BE - MC_playerBD[iLocalPart].iLocationInSecondaryLocation: ", MC_playerBD[iLocalPart].iLocationInSecondaryLocation)
		RETURN TRUE
	ENDIF
	
	BOOL bInLocate = FALSE
	FLOAT fSecondaryRadius = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fSecondaryRadius
	
	IF fSecondaryRadius > 0
		IF bLocalPlayerPedOK
			
			VECTOR vMyCoords = GET_ENTITY_COORDS(LocalPlayerPed)
			
			IF VDIST2(vMyCoords, GET_LOCATION_VECTOR(iloc)) <= (fSecondaryRadius * fSecondaryRadius)
				bInLocate = TRUE
			ENDIF
		ENDIF
	ELSE
		bInLocate = TRUE
	ENDIF
	
	RETURN bInLocate
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Flares ----------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing for the flares.  --------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_CORONA_NEW_RGB(INT iFlare, FLOAT fR, FLOAT fG, FLOAT fB )
	corona_flare_data[iFlare].fNextRed = fR
	corona_flare_data[iFlare].fNextGreen = fG
	corona_flare_data[iFlare].fNextBlue = fB
	PRINTLN("[RCC MISSION][AW SMOKE]SET_CORONA_NEW_RGB | fR: ",fR," fG:  ",fG," fB: " , fB )
ENDPROC

PROC SET_FLARE_NEW_RGB(INT iFlare, FLOAT fR, FLOAT fG, FLOAT fB, BOOL bAndCorona = TRUE  )
	flare_data[iFlare].fNextRed = fR
	flare_data[iFlare].fNextGreen = fG
	flare_data[iFlare].fNextBlue = fB
	PRINTLN("[RCC MISSION][AW SMOKE]SET_FLARE_NEW_RGB | fR: ",fR," fG:  ",fG," fB: " , fB )
	
	IF bAndCorona
		SET_CORONA_NEW_RGB(iFlare,fR,fG,fB)
	ENDIF
	
ENDPROC

PROC SET_CORONA_TO_TRANSITION(INT iFlare, FLARE_STATE fsNew )
	corona_flare_data[iFlare].fTransitionProgress = 0
	corona_flare_data[iFlare].iNewState = fsNew
	PRINTLN("[RCC MISSION][AW SMOKE]SET_CORONA_TO_TRANSITION | flare_data[iFlare].iNewState: ", corona_flare_data[iFlare].iNewState, "flare_data[iFlare].iFlareState (Current):", flare_data[iFlare].iFlareState  )
ENDPROC

PROC SET_FLARE_TO_TRANSITION(INT iFlare, FLARE_STATE fsNew,BOOL bAndCorona = TRUE )
	flare_data[iFlare].fTransitionProgress = 0
	flare_data[iFlare].iNewState = fsNew
	
	IF fsNew = FLARE_STATE_NEUTRAL
		flare_data[iFlare].iWinningTeam = -1
	ELIF  fsNew = FLARE_STATE_TEAM_0
		flare_data[iFlare].iWinningTeam = 0
	ELIF fsNew = FLARE_STATE_TEAM_1
		flare_data[iFlare].iWinningTeam = 1
	ELIF fsNew = FLARE_STATE_TEAM_2
		flare_data[iFlare].iWinningTeam = 2
	ELIF fsNew = FLARE_STATE_TEAM_3
		flare_data[iFlare].iWinningTeam = 3
	ENDIF
	
	PRINTLN("[RCC MISSION] [AW SMOKE]SET_FLARE_TO_TRANSITION | flare_data[iFlare].iNewState: ", flare_data[iFlare].iNewState, "flare_data[iFlare].iFlareState (Current):", flare_data[iFlare].iFlareState  )
	
	IF bAndCorona
		SET_CORONA_TO_TRANSITION(iFlare, fsNew)
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(INT iFlare, FLARE_STATE fsNewState)

	IF flare_data[iFlare].iNewState  != fsNewState
	AND flare_data[iFlare].iFlareState != fsNewState
		PRINTLN("[RCC MISSION] [AW SMOKE] SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE | flare_data[iFlare].iNewState: ", flare_data[iFlare].iNewState, "flare_data[iFlare].iFlareState (Current):", flare_data[iFlare].iFlareState  )
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF

ENDFUNC

FUNC BOOL HAS_FLARE_REACHED_NEW_STATE(INT iFlare)

	IF flare_data[iFlare].fTransitionProgress >= 1.0
		PRINTLN("AW SMOKE - HAS_FLARE_REACHED_NEW_STATE - TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC CLEAN_UP_FLARE_SFX_FOR_THIS_PROP(INT iProp)
	INT i
	FOR i = 0 TO iNumCreatedFlarePTFX-1
		IF FlareSoundIDPropNum[i] = iProp
			IF FlareSoundID[i] > -1
				PRINTLN("[RCC MISSION] - Cleaning up flare sound index: ", FlareSoundID[i])
				IF NOT HAS_SOUND_FINISHED(FlareSoundID[i])
					STOP_SOUND(FlareSoundID[i])
				ENDIF
				RELEASE_SOUND_ID(FlareSoundID[i])				
			ENDIF
			BREAKLOOP
		ENDIF
	ENDFOR
ENDPROC

PROC CLEANUP_FLARE_PROPS_FOR_CUTSCENE()
	INT iprop
	CLEANUP_MISSION_FLARE_PTFX()
	
	FOR iProp = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].mn = PROP_FLARE_01
			IF DOES_ENTITY_EXIST( oiProps[iprop] )	
				DELETE_OBJECT(oiProps[iprop] )
				PRINTLN("[RCC MISSION] cleaning up flare for end of mission cutscene: ",iprop)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC INITALISE_CORONA_DATA(INT iFlare)

	corona_flare_data[iFlare].iFlareState = FLARE_STATE_INIT
	corona_flare_data[iFlare].fNextRed = 1.0
	corona_flare_data[iFlare].FNextGreen = 1.0
	corona_flare_data[iFlare].fNextBlue = 0 
	corona_flare_data[iFlare].fRed = 1.0
	corona_flare_data[iFlare].fGreen = 1.0
	corona_flare_data[iFlare].fBlue = 0
	corona_flare_data[iFlare].iPTFXval = iFlare
	PRINTLN("[RCC MISSION][AW SMOKE]Setting up flare corona |   ",iFlare)
	
ENDPROC

PROC INITALISE_FLARE_DATA(INT iFlare, BOOL bAndCorona = TRUE)

	flare_data[iFlare].iFlareState = FLARE_STATE_INIT
	flare_data[iFlare].iNewState = FLARE_STATE_NEUTRAL
	flare_data[iFlare].fNextRed = 1.0
	flare_data[iFlare].FNextGreen = 1.0
	flare_data[iFlare].fNextBlue = 0 
	flare_data[iFlare].fRed = 1.0
	flare_data[iFlare].fGreen = 1.0
	flare_data[iFlare].fBlue = 0
	flare_data[iFlare].iPTFXval = iFlare
	flare_data[iFlare].iWinningTeam = -2
	PRINTLN("[RCC MISSION][AW SMOKE]Setting up flare |   ",iFlare)

	IF bAndCorona
		INITALISE_CORONA_DATA(iFlare)
	ENDIF
	
ENDPROC

PROC SET_LOCAL_CORONA_NEW_RGB(INT iFlare, FLOAT fR, FLOAT fG, FLOAT fB )
	corona_flare_data[iFlare].fNextRed = fR
	corona_flare_data[iFlare].fNextGreen = fG
	corona_flare_data[iFlare].fNextBlue = fB
	PRINTLN("[RCC MISSION][AW SMOKE]SET_CORONA_NEW_RGB | fR: ",fR," fG:  ",fG," fB: " , fB )
ENDPROC

PROC SET_LOCAL_FLARE_NEW_RGB(INT iFlare, FLOAT fR, FLOAT fG, FLOAT fB, BOOL bAndCorona = TRUE  )
	flare_data[iFlare].fNextRed = fR
	flare_data[iFlare].fNextGreen = fG
	flare_data[iFlare].fNextBlue = fB
	
	PRINTLN("[RCC MISSION][AW SMOKE]SET_FLARE_NEW_RGB | fR: ",fR," fG:  ",fG," fB: " , fB )
	
	IF bAndCorona
		SET_LOCAL_CORONA_NEW_RGB(iFlare,fR,fG,fB)
	ENDIF
ENDPROC

PROC PROCESS_EVERY_FRAME_FLARE_OBJECT()	
	
	INT i
	
	INT iPTFX
	
	IF iNumCreatedFlarePTFX >= 0
		IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
			FOR iPTFX = 0 TO iNumCreatedFlarePTFX -1
				IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxFlareIndex[iPTFX] )
					PRINTLN("[RCC MISSION][AW SMOKE] PROCESS_EVERY_FRAME_FLARE_OBJECT() - Clearing up smoke")
					STOP_PARTICLE_FX_LOOPED(ptfxFlareIndex[iPTFX])
					
					IF FlareSoundID[iPTFX] != -1
						PRINTLN("[RCC MISSION][AW SMOKE] PROCESS_EVERY_FRAME_FLARE_OBJECT() - Clearing up Sound ID: ", FlareSoundID[iPTFX])
						IF NOT HAS_SOUND_FINISHED(FlareSoundID[iPTFX])
							STOP_SOUND(FlareSoundID[iPTFX])
						ENDIF
						SCRIPT_RELEASE_SOUND_ID(FlareSoundID[iPTFX])
					ENDIF
				ENDIF
			ENDFOR
			
			FOR i = 0 TO FMMC_MAX_GO_TO_LOCATIONS -1
				IF DOES_BLIP_EXIST(LocBlip[i])
					REMOVE_BLIP(LocBlip[i])
				ENDIF
				IF DOES_BLIP_EXIST(LocBlip1[i])
					REMOVE_BLIP(LocBlip1[i])
				ENDIF
			ENDFOR
			
			
		ENDIF
	ENDIF
	
	FOR i = 0 TO iNumCreatedDynamicFlarePTFX -1
				
		IF flare_data[i].iPTFXval < ciTOTAL_PTFX
		AND flare_data[i].iPTFXval >= 0
			//if the current state isn't equal to the new state that has been set
			IF flare_data[i].iFlareState !=  flare_data[i].iNewState
				
				IF HAS_FLARE_REACHED_NEW_STATE(i)
					flare_data[i].iFlareState =  flare_data[i].iNewState
				ELSE
					IF flare_data[i].fRed != flare_data[i].fNextRed
						flare_data[i].fRed   = LERP_FLOAT(flare_data[i].fRed,flare_data[i].fNextRed,flare_data[i].fTransitionProgress)
					ENDIF
					IF flare_data[i].fGreen != flare_data[i].fNextGreen
						flare_data[i].fGreen = LERP_FLOAT(flare_data[i].fGreen,flare_data[i].fNextGreen,flare_data[i].fTransitionProgress)
					ENDIF
					IF flare_data[i].fBlue != flare_data[i].fNextBlue
						flare_data[i].fBlue  = LERP_FLOAT(flare_data[i].fBlue,flare_data[i].fNextBlue,flare_data[i].fTransitionProgress)
					ENDIF
					PRINTLN("AW SMOKE - PROCESS_EVERY_FRAME_FLARE_OBJECT - fTransitionProgress:", flare_data[i].fTransitionProgress)
					flare_data[i].fTransitionProgress = flare_data[i].fTransitionProgress + 0.01
				ENDIF
				
				IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxFlareIndex[flare_data[i].iPTFXval])
					PRINTLN("AW SMOKE -PROCESS_EVERY_FRAME_FLARE_OBJECT - fRed:", flare_data[i].fRed, " fGreen:", flare_data[i].fGreen," fBlue:", flare_data[i].fBlue)
					PRINTLN("AW SMOKE -PROCESS_EVERY_FRAME_FLARE_OBJECT - flare_data[",i,"].iPTFXval = ", flare_data[i].iPTFXval)
					SET_PARTICLE_FX_LOOPED_COLOUR(ptfxFlareIndex[flare_data[i].iPTFXval],flare_data[i].fRed, flare_data[i].fGreen, flare_data[i].fBlue ,TRUE)
				ELSE
					PRINTLN("AW SMOKE - PTFX DOES NOT EXIST")
				ENDIF
			ENDIF
		ELSE
			PRINTLN("AW SMOKE - iPTFXval:",flare_data[i]. iPTFXval)
		ENDIF
		
	ENDFOR

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Location Rule Warps
// ##### Description: Moving the Location to a creator placed position on a rule change.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC WARP_LOCATION_TO_WARP_LOCATION(INT iLocation, VECTOR vLocWarpPos, FLOAT fLocWarpHead, INT &iLocWarpedBS[], INT iEventLocWarpedBS)	
	// Should be checked last so that we can return true in this function on the first frame the checks come back positive.
	IF IS_WARP_LOCATION_VISIBLE_TO_ANY_PLAYERS(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].sWarpLocationSettings, GET_LOCATION_VECTOR(iLocation), vLocWarpPos)
		PRINTLN("[Warp_location][Locations][Location ", iLocation, "] - PROCESS_LOCATION_ON_RULE_WARP - IS_WARP_LOCATION_VISIBLE_TO_ANY_PLAYERS = TRUE")
		EXIT
	ENDIF
	IF NOT IS_VECTOR_ZERO(vLocWarpPos)
		PRINTLN("[Warp_location][Locations][Location ", iLocation, "] - WARP_LOCATION_TO_WARP_LOCATION - Warping this Location with COORD: ", vLocWarpPos, " and Heading: ", fLocWarpHead, " Setting iGoToLocationWarpedOnThisRule.")		
		PLAY_WARP_ENTITY_TO_WARP_LOCATION_PTFX(vGoToLocateVectors[iLocation], g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].sWarpLocationSettings.sEntityWarpPTFX, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_PlayWarpPTFX_Start))
		
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].vloc1)
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].vloc2)
			MC_PROCESS_OFFSET_FOR_AREA(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].vLoc1, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].vLoc2, 1, vLocWarpPos)			
		ELSE
			MC_PROCESS_OFFSET_FOR_AREA(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].vLoc[0], g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].vLoc1, 0, vLocWarpPos)
		ENDIF
		
		vGoToLocateVectors[iLocation] = GET_INIT_LOCATION_VECTOR(iLocation)
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].fHeading[0] = fLocWarpHead
		
		PLAY_WARP_ENTITY_TO_WARP_LOCATION_PTFX(vGoToLocateVectors[iLocation], g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].sWarpLocationSettings.sEntityWarpPTFX, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_PlayWarpPTFX_End))
		
		FMMC_SET_LONG_BIT(iLocWarpedBS, iLocation)
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, iEventLocWarpedBS, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, iLocation)
	ELSE
		ASSERTLN("[Warp_location] - WARP_LOCATION_TO_WARP_LOCATION - NULL VECTOR POSITION GIVEN. Check the Creator Data. ")
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_IGNORE, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_LOCATION, "Set up to warp on rule to a NULL/ZERO vector!", iLocation)
		#ENDIF
		FMMC_SET_LONG_BIT(iLocWarpedBS, iLocation)
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, iEventLocWarpedBS, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, iLocation)
	ENDIF
ENDPROC

FUNC BOOL CAN_LOCATION_PROCESS_WARP_LOCATION( #IF IS_DEBUG_BUILD INT iLocation #ENDIF )	
	IF NOT READY_TO_PROCESS_ENTITY_WARP()
		PRINTLN("[Warp_location][Locations][Location ", iLocation, "] - CAN_LOCATION_PROCESS_WARP_LOCATION - We should perform a warp, but we're not ready to process it yet.")
		RETURN FALSE
	ENDIF	
	RETURN TRUE
ENDFUNC

PROC PROCESS_LOCATION_ON_RULE_WARP(INT iLocation)
	
	IF NOT SHOULD_ENTITY_PERFORM_A_RULE_WARP_LOCATION_WARP(CREATION_TYPE_GOTO_LOC, iLocation)
		EXIT
	ENDIF
	
	IF NOT CAN_LOCATION_PROCESS_WARP_LOCATION( #IF IS_DEBUG_BUILD iLocation #ENDIF )
		EXIT
	ENDIF
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule =	MC_serverBD_4.iCurrentHighestPriority[iTeam] 
	
	FLOAT fLocWarpHead
	VECTOR vLocWarpPos
	
	GET_ENTITY_WARP_LOCATION_ON_THIS_RULE_START(iRule, iTeam, CREATION_TYPE_GOTO_LOC, iLocation, vLocWarpPos, fLocWarpHead)	
	
	WARP_LOCATION_TO_WARP_LOCATION(iLocation, vLocWarpPos, fLocWarpHead, iGoToLocationWarpedOnThisRule, g_ciInstancedcontentEventType_WarpedGoToLocateOnRuleStart)	
	
ENDPROC

PROC PROCESS_LOCATION_ON_DAMAGE_WARP(INT iLocation)
	
	IF NOT SHOULD_ENTITY_PERFORM_AN_ON_DAMAGE_LOCATION_WARP(CREATION_TYPE_GOTO_LOC, iLocation)
		EXIT
	ENDIF
	
	IF NOT CAN_LOCATION_PROCESS_WARP_LOCATION( #IF IS_DEBUG_BUILD iLocation #ENDIF )
		EXIT
	ENDIF
	
	FLOAT fLocWarpHead
	VECTOR vLocWarpPos
	
	GET_ENTITY_WARP_LOCATION_ON_DAMAGED(CREATION_TYPE_GOTO_LOC, iLocation, vLocWarpPos, fLocWarpHead)	
	
	WARP_LOCATION_TO_WARP_LOCATION(iLocation, vLocWarpPos, fLocWarpHead, iGoToLocationWarpedOnDamage, g_ciInstancedcontentEventType_WarpedGoToLocateOnDamageStart)	
	
ENDPROC

// ##### ---------------------------------------------------------------------------------------
// ##### Section Name: Capture													----------------
// ##### Description: Functions for capture location processing					----------------
// ##### ---------------------------------------------------------------------------------------
// ##### ---------------------------------------------------------------------------------------

//Tied to colour changing flares
PROC PROCESS_EVERY_FRAME_CAPTURE_LOGIC(INT iLoc )

	INT iTeam = MC_playerBD[iLocalPart].iteam
	
	HUD_COLOURS TeamColour
	
	FLOAT fNRed
	FLOAT fNGreen
	FLOAT fNBlue
	
	INT iRed,iGreen,iBlue,iAlpha
	
	INT iPTFX, iTeamsToCheck
	INT iNumberofCaptureTeams = 0
	INT iCaptureTeam = -1
	
	IF iNumCreatedFlarePTFX >= 0
		FOR iPTFX = 0 TO iNumCreatedFlarePTFX -1
			IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxFlareIndex[iPTFX] )
				PRINTLN( "[RCC MISSION][AW SMOKE] Particle FX Looped Exists  ")
				PRINTLN( "[RCC MISSION][AW SMOKE] flare_data[iPTFX].iPTFXval : ," ,flare_data[iPTFX].iPTFXval)
				
				IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
					IF flare_data[iPTFX].iConnectedLocate = -1
						IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()						
							FOR iTeamsToCheck = 0 TO MC_ServerBD.iNumberOfTeams -1
								IF iLoc < FMMC_MAX_GO_TO_LOCATIONS
								AND iLoc >= 0
									IF MC_serverBD_1.inumberOfTeamInArea[iLoc][iTeamsToCheck] != 0
									
										//If a team is in the area, increment the team count
										iNumberofCaptureTeams++
																				
										//Capture team only gets used if only one team is inside of the location e.g. iNumberofCaptureTeams = 1
										iCaptureTeam = iTeamsToCheck
										
										PRINTLN("[RCC MISSION] [AW SMOKE] Team [",iTeamsToCheck,"] percentage at: ", MC_ServerBD.iCaptureBarPercentage[iTeamsToCheck])
										
										IF MC_ServerBD.iCaptureBarPercentage[iTeamsToCheck] > iMaxCaptureAmount
											iMaxCaptureAmount = MC_ServerBD.iCaptureBarPercentage[iTeamsToCheck]
											g_iTheWinningTeam = iTeamsToCheck
											
											PRINTLN( "[RCC MISSION] [AW SMOKE] iMaxCaptureAmount:", iMaxCaptureAmount)
											PRINTLN( "[RCC MISSION] [AW SMOKE] g_iTheWinningTeam:", g_iTheWinningTeam)
										ENDIF
									ELSE										
										IF iTeamsToCheck = g_iTheWinningTeam
											g_iTheWinningTeam = -1
											iMaxCaptureAmount = 0
										ENDIF
									ENDIF 
								ENDIF
							ENDFOR
														
							//If the iNumberofCaptureTeams = 0, g_iTheWinningTeam = the default of iCaptureTeam which is -1 to reset
							//	the zone back to neutral colour (yellow) since no one is inside of it
							//
							//If iNumberofCaptureTeams = 1, g_iTheWinningTeam = the team that set iCaptureTeam = iTeamsToCheck above,
							//	and because they are the only one in control of the zone it is set to their colour, even if another team
							//	has a higher score
							IF iNumberofCaptureTeams <= 1
								g_iTheWinningTeam = iCaptureTeam
								iMaxCaptureAmount = 0
							ENDIF														
							
							//Reset 
							IF flare_data[iPTFX].iWinningTeam != g_iTheWinningTeam
								IF flare_data[iPTFX].fTransitionProgress > 0
								OR corona_flare_data[iPTFX].fTransitionProgress > 0
									PRINTLN( "[RCC MISSION][AW SMOKE] Reset Flare State - Transition not complete" )
									flare_data[iPTFX].iFlareState =  FLARE_STATE_INIT
									corona_flare_data[iPTFX].iFlareState =  FLARE_STATE_INIT
								ENDIF
							ENDIF
							
							PRINTLN( "[RCC MISSION][AW SMOKE] g_iTheWinningTeam: ", g_iTheWinningTeam) 
							
							IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
								IF iLoc > -1 AND iLoc < FMMC_MAX_GO_TO_LOCATIONS
									INT iTeamOverride = -1
									INT i = 0
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[MC_ServerBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE9_TEAM_COLOURED_LOCATES)
										// It doesn't matter if this sets a team twice, because we override that with the contested colour
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)
											FOR i = 0 TO FMMC_MAX_TEAMS-1
												IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
													iTeamOverride = i				
												ENDIF
											ENDFOR
										ENDIF
									ENDIF						
								
									IF g_iTheWinningTeam = -1
									AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAreaRetained, MC_ServerBD_4.iCurrentHighestPriority[iTeam]) OR MC_serverBD.iLocOwner[iLoc] = -1)
									AND iTeamOverride = -1
										//Show Yellow = Tied
										PRINTLN( "[RCC MISSION][AW SMOKE] YELLOW TIED -1 new")
										IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_NEUTRAL)
											SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_NEUTRAL)
										ENDIF
										
										SET_FLARE_NEW_RGB(iPTFX, 1.0, 1.0, 0)
										
										IF iWinningState != -1
											STRING sSoundSet 
											sSoundSet = "DLC_Apartments_Drop_Zone_Sounds"
											PLAY_SOUND_FRONTEND(-1,"Zone_Neutral",sSoundSet,FALSE)
											PRINTLN( "[RCC MISSION][AW SMOKE] PLAYING NEUTRAL SOUND")
											iWinningState = -1
										ENDIF
									ELSE 
										//A team is winning
										INT iTeamToUse = g_iTheWinningTeam
										
										IF g_iTheWinningTeam = -1
											IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAreaRetained, MC_ServerBD_4.iCurrentHighestPriority[iTeam])
												iTeamToUse = MC_serverBD.iLocOwner[iLoc]
											ENDIF
										ENDIF
										
										IF iTeamOverride > -1
											iTeamToUse = iTeamOverride
										ENDIF
										
										TeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamToUse, PlayerToUse)
										GET_HUD_COLOUR(TeamColour, iRed, iGreen, iBlue, iAlpha)
									
										fNRed = TO_FLOAT(iRed) / 255.0
										fNGreen = TO_FLOAT(iGreen) / 255.0
										fNBlue = TO_FLOAT(iBlue) / 255.0
										
										#IF IS_DEBUG_BUILD
											IF iTeamToUse  = 0
												PRINTLN( "[RCC MISSION][AW SMOKE] TEAM 0 WINNING (ORANGE) ")
											ELIF iTeamToUse  = 1
												PRINTLN( "[RCC MISSION][AW SMOKE] TEAM 1 WINNING (GREEN) ")
											ELIF iTeamToUse  = 2
												PRINTLN( "[RCC MISSION][AW SMOKE] TEAM 2 WINNING (PINK) ")
											ELIF iTeamToUse  = 3
												PRINTLN( "[RCC MISSION][AW SMOKE] TEAM 3 WINNING (PURPLE) ")
											ENDIF
											IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_0)
												PRINTLN( "[RCC MISSION][AW SMOKE] TRANSITIONING TO STATE - TEAM 0 ")
											ENDIF
											IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_1)
												PRINTLN( "[RCC MISSION][AW SMOKE] TRANSITIONING TO STATE - TEAM 1 ")
											ENDIF
											IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_2)
												PRINTLN( "[RCC MISSION][AW SMOKE] TRANSITIONING TO STATE - TEAM 2 ")
											ENDIF
											IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_3)
												PRINTLN( "[RCC MISSION][AW SMOKE] TRANSITIONING TO STATE - TEAM 3 ")
											ENDIF
											PRINTLN( "[RCC MISSION][AW SMOKE] iRed: ",iRed)
											PRINTLN( "[RCC MISSION][AW SMOKE] iGreen: ",iGreen)
											PRINTLN( "[RCC MISSION][AW SMOKE] iBlue: ",iBlue)
											
											PRINTLN( "[RCC MISSION][AW SMOKE] fNRed: ",fNRed)
											PRINTLN( "[RCC MISSION][AW SMOKE] fNGreen: ",fNGreen)
											PRINTLN( "[RCC MISSION][AW SMOKE] fNBlue: ",fNBlue)
										#ENDIF
										
										IF iTeamToUse = 0
											IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_0)
												SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TEAM_0)
											ENDIF
										ELIF iTeamToUse = 1
											IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_1)
												SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TEAM_1)
											ENDIF
										ELIF iTeamToUse = 2
											IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_2)
												SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TEAM_2)
											ENDIF
										ELIF iTeamToUse = 3
											IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_3)
												SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TEAM_3)
											ENDIF
										ENDIF
										
										SET_FLARE_NEW_RGB(iPTFX, fNRed, fNGreen, fNBlue)
										
									ENDIF					
									
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_CONTESTED_COLOUR_FOR_LOCATES)
										IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_EnableAdvancedCaptureStateLogic)
											IF g_iTheWinningTeam > -1
											OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)
												INT iHostileTeamsInsideLocate = 0
												
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)										
													iHostileTeamsInsideLocate = -1
													FOR i = 0 TO FMMC_MAX_TEAMS-1
														IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
															iHostileTeamsInsideLocate++
														ENDIF
													ENDFOR
												ELSE
													FOR i = 0 TO FMMC_MAX_TEAMS-1
														IF NOT DOES_TEAM_LIKE_TEAM(g_iTheWinningTeam, i)
														AND g_iTheWinningTeam != i
															IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
																iHostileTeamsInsideLocate++
															ENDIF
														ENDIF
													ENDFOR
												ENDIF
												
												PRINTLN("[RCC MISSION][AW SMOKE] - Contested Smoke check 1 - iHostileTeamsInsideLocate: ", iHostileTeamsInsideLocate)
												IF iHostileTeamsInsideLocate > 0
													iRed = 255
													iGreen = 0
													iBlue = 0
													
													fNRed = TO_FLOAT(iRed) / 255.0
													fNGreen = TO_FLOAT(iGreen) / 255.0
													fNBlue = TO_FLOAT(iBlue) / 255.0
													
													IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TRANSITION)
														SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TRANSITION)
													ENDIF
													
													SET_FLARE_NEW_RGB(iPTFX, fNRed, fNGreen, fNBlue)
												ENDIF
											ENDIF
										ELSE
											PRINTLN("[RCC MISSION] Processing capture locate Smoke Colours - 1 - Location: ", iLoc)
											fNRed = TO_FLOAT(iCaptureLocateRed[iLoc]) / 255.0
											fNGreen = TO_FLOAT(iCaptureLocateGreen[iLoc]) / 255.0
											fNBlue = TO_FLOAT(iCaptureLocateBlue[iLoc]) / 255.0

											IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TRANSITION)
												SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TRANSITION)
											ENDIF
											
											SET_FLARE_NEW_RGB(iPTFX, fNRed, fNGreen, fNBlue)

										ENDIF
									ENDIF						
								ENDIF
							ENDIF
							
							//Handle audio
							IF g_iTheWinningTeam != - 1
								STRING sSoundSet 
								sSoundSet = "DLC_Apartments_Drop_Zone_Sounds"
								IF iTeam = g_iTheWinningTeam
									IF iWinningState != 1
										PLAY_SOUND_FRONTEND(-1,"Zone_Team_Capture", sSoundSet,FALSE)
										PRINTLN( "[RCC MISSION][AW SMOKE] PLAYING WINNING SOUND")
										iWinningState = 1
									ENDIF
								ELSE
									IF iWinningState != 2
										PLAY_SOUND_FRONTEND(-1,"Zone_Enemy_Capture", sSoundSet,FALSE)
										PRINTLN( "[RCC MISSION][AW SMOKE] PLAYING LOSING SOUND")
										iWinningState = 2
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_CAPTURE_LOGIC - Flare: ", iPTFX, " ConnectedLoc: ", flare_data[iPTFX].iConnectedLocate, " MC_serverBD.iLocOwner: ", MC_serverBD.iLocOwner[flare_data[iPTFX].iConnectedLocate])
						IF MC_serverBD.iLocOwner[flare_data[iPTFX].iConnectedLocate] = -1
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_EnableAdvancedCaptureStateLogic)
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_CONTESTED_COLOUR_FOR_LOCATES)
									IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_NEUTRAL)
										SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_NEUTRAL)
									ENDIF
									SET_FLARE_NEW_RGB(iPTFX, 1.0, 1.0, 0)
								ELSE
									
									INT iHostileTeamsInsideLocate = 0
									INT i = 0
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)									
										iHostileTeamsInsideLocate = -1
										FOR i = 0 TO FMMC_MAX_TEAMS-1
											IF MC_serverBD_1.inumberOfTeamInArea[flare_data[iPTFX].iConnectedLocate][i] > 0
												iHostileTeamsInsideLocate++
											ENDIF
										ENDFOR
									ELSE
										FOR i = 0 TO FMMC_MAX_TEAMS-1
											IF NOT DOES_TEAM_LIKE_TEAM(MC_playerBD[iLocalPart].iteam, i)
											AND MC_playerBD[iLocalPart].iteam != i
												IF MC_serverBD_1.inumberOfTeamInArea[flare_data[iPTFX].iConnectedLocate][i] > 0
													iHostileTeamsInsideLocate++
												ENDIF
											ENDIF
										ENDFOR
									ENDIF
									PRINTLN("[RCC MISSION][AW SMOKE] - Contested Smoke check 2 - iHostileTeamsInsideLocate: ", iHostileTeamsInsideLocate)
									IF (iHostileTeamsInsideLocate > 0
									AND MC_serverBD_1.inumberOfTeamInArea[flare_data[iPTFX].iConnectedLocate][iTeam] > 0)
										IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX, FLARE_STATE_TRANSITION)
											SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TRANSITION)
										ENDIF
										SET_FLARE_NEW_RGB(iPTFX, 1.0, 0.0, 0.0)
									ELSE
										IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX, FLARE_STATE_NEUTRAL)
											SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_NEUTRAL)
										ENDIF

										SET_FLARE_NEW_RGB(iPTFX, 1.0, 1.0, 0.0)
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION] Processing capture locate Smoke Colours - 2 - Location: ", flare_data[iPTFX].iConnectedLocate)
								FLARE_STATE tempFlareState
								IF eCaptureLocateState[flare_data[iPTFX].iConnectedLocate] = eCapture_neutral
									tempFlareState = FLARE_STATE_NEUTRAL
								ELIF eCaptureLocateState[flare_data[iPTFX].iConnectedLocate] = eCapture_contested
									tempFlareState = FLARE_STATE_TRANSITION
								ENDIF
								
								IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX, tempFlareState)
									SET_FLARE_TO_TRANSITION(iPTFX, tempFlareState)
								ENDIF
								
								fNRed = TO_FLOAT(iCaptureLocateRed[flare_data[iPTFX].iConnectedLocate]) / 255.0
								fNGreen = TO_FLOAT(iCaptureLocateGreen[flare_data[iPTFX].iConnectedLocate]) / 255.0
								fNBlue = TO_FLOAT(iCaptureLocateBlue[flare_data[iPTFX].iConnectedLocate]) / 255.0

								SET_FLARE_NEW_RGB(iPTFX, fNRed, fNGreen, fNBlue)
							ENDIF
						ELSE
							INT iCurrentOwners = MC_serverBD.iLocOwner[flare_data[iPTFX].iConnectedLocate]
							
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_EnableAdvancedCaptureStateLogic)
							AND eCaptureLocateState[flare_data[iPTFX].iConnectedLocate] != eCapture_contested
								TeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iCurrentOwners, PlayerToUse)
								GET_HUD_COLOUR(TeamColour, iRed, iGreen, iBlue, iAlpha)
								PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_CAPTURE_LOGIC - MC_serverBD.iLocOwner[flare_data[iPTFX].iConnectedLocate]: ", MC_serverBD.iLocOwner[flare_data[iPTFX].iConnectedLocate])
								fNRed = TO_FLOAT(iRed) / 255.0
								fNGreen = TO_FLOAT(iGreen) / 255.0
								fNBlue = TO_FLOAT(iBlue) / 255.0
								
								IF iCurrentOwners = 0
									IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_0)
										SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TEAM_0)
									ENDIF
								ELIF iCurrentOwners = 1
									IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_1)
										SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TEAM_1)
									ENDIF
								ELIF iCurrentOwners = 2
									IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_2)
										SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TEAM_2)
									ENDIF
								ELIF iCurrentOwners = 3
									IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_3)
										SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TEAM_3)
									ENDIF
								ENDIF
								
								SET_FLARE_NEW_RGB(iPTFX, fNRed, fNGreen, fNBlue)
							ENDIF
							
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_EnableAdvancedCaptureStateLogic)
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_CONTESTED_COLOUR_FOR_LOCATES)
									IF iCurrentOwners > -1
									OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)
										INT iHostileTeamsInsideLocate = 0
										INT i = 0
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)										
											iHostileTeamsInsideLocate = -1
											FOR i = 0 TO FMMC_MAX_TEAMS-1
												IF MC_serverBD_1.inumberOfTeamInArea[flare_data[iPTFX].iConnectedLocate][i] > 0
													iHostileTeamsInsideLocate++
												ENDIF
											ENDFOR
										ELSE
											FOR i = 0 TO FMMC_MAX_TEAMS-1
												IF NOT DOES_TEAM_LIKE_TEAM(iCurrentOwners, i)
												AND iCurrentOwners != i
													IF MC_serverBD_1.inumberOfTeamInArea[flare_data[iPTFX].iConnectedLocate][i] > 0
														iHostileTeamsInsideLocate++
													ENDIF
												ENDIF
											ENDFOR
										ENDIF
										PRINTLN("[RCC MISSION][AW SMOKE] - Contested Smoke check 3 - iHostileTeamsInsideLocate: ", iHostileTeamsInsideLocate)
										IF iHostileTeamsInsideLocate > 0
											iRed = 255
											iGreen = 0
											iBlue = 0
											
											fNRed = TO_FLOAT(iRed) / 255.0
											fNGreen = TO_FLOAT(iGreen) / 255.0
											fNBlue = TO_FLOAT(iBlue) / 255.0
											
											IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TRANSITION)
												SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TRANSITION)
											ENDIF
											
											SET_FLARE_NEW_RGB(iPTFX, fNRed, fNGreen, fNBlue)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF eCaptureLocateState[flare_data[iPTFX].iConnectedLocate] = eCapture_contested
									PRINTLN("[RCC MISSION] Processing capture locate Smoke Colours - 3 (Contested) - Location: ", flare_data[iPTFX].iConnectedLocate)
									fNRed = TO_FLOAT(iCaptureLocateRed[flare_data[iPTFX].iConnectedLocate]) / 255.0
									fNGreen = TO_FLOAT(iCaptureLocateGreen[flare_data[iPTFX].iConnectedLocate]) / 255.0
									fNBlue = TO_FLOAT(iCaptureLocateBlue[flare_data[iPTFX].iConnectedLocate]) / 255.0

									IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TRANSITION)
										SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TRANSITION)
									ENDIF
									
									SET_FLARE_NEW_RGB(iPTFX, fNRed, fNGreen, fNBlue)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN( "[RCC MISSION][AW SMOKE] PTFX doesn't exist")
			ENDIF
		ENDFOR
	ENDIF
	
	IF bIsLocalPlayerHost
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciEVERY_FRAME_GOTO_LOC_PROCESSING)
		IF iLoc > -1
			INT iTempTeam
			FOR iTempTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
				IF MC_serverBD_1.inumberOfTeamInArea[iLoc][iTempTeam] != GET_NUMBER_OF_PARTICIPANTS_FROM_TEAM_IN_THIS_AREA(iLoc, iTempTeam)
					MC_serverBD_1.inumberOfTeamInArea[iLoc][iTempTeam] = GET_NUMBER_OF_PARTICIPANTS_FROM_TEAM_IN_THIS_AREA(iLoc, iTempTeam)
					PRINTLN("PROCESS_EVERY_FRAME_CAPTURE_LOGIC - MC_serverBD_1.inumberOfTeamInArea[iLoc][iTempTeam] = ", MC_serverBD_1.inumberOfTeamInArea[iLoc][iTempTeam])
				ENDIF
			ENDFOR
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_AREA_ABLE_TO_BE_CAPTURED(INT iTeam, INT iLoc, BOOL bIsHeld = FALSE)
	IF IS_THIS_A_VERSUS_MISSION()
		INT i
		
		//if we have competition in the capture zone, its a stalemate. 
		// This is now turned off as standard, needs to be turned on for each capture location if we want it using this system. 
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_UseAltCaptureLogic)
			FOR i = 0 TO FMMC_MAX_TEAMS-1
				IF NOT DOES_TEAM_LIKE_TEAM(iTeam, i)
				AND iTeam != i
					IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
						MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iHoldTimer[iLoc][iteam])
						RETURN FALSE
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
		IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
			IF NOT bIsHeld
				//check if we have already captured the area (this stops capture and held logic both running)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAreaRetained, MC_ServerBD_4.iCurrentHighestPriority[iTeam]) 
					IF MC_serverBD.iLocOwner[iLoc] = iTeam
						RETURN FALSE 
					ENDIF
				ENDIF

			ELSE
				
				//if we have already captured, we have to remain inside to retain
				IF MC_serverBD_1.inumberOfTeamInArea[iLoc][iTeam] = 0
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAreaRetained, MC_ServerBD_4.iCurrentHighestPriority[iTeam])					
					MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iHoldTimer[iLoc][iteam])
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN TRUE	
ENDFUNC

PROC SERVER_RESET_CAPTURE_LOCATION_DATA(INT iLoc, INT iTeamToUse, INT iCapturingTeam, INT iPriority, BOOL &bWasObjective)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciCHANGE_BLIP_COLOUR_TO_CONTROLLING_TEAM)
		MC_serverBD_1.inumberOfTeamInArea[iLoc][iTeamToUse] = 0
	ENDIF
	
	RESET_NET_TIMER(tdtimesincelastupdate[iLoc][iTeamToUse])	
	MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iAreaTimer[iLoc][iTeamToUse])
	CLEAR_BIT(MC_serverBD.iAreaTimerStartedBS[iTeamToUse], iLoc)
	MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iHoldTimer[iLoc][iTeamToUse])
	MC_serverBD_1.iNumTeamControlKills[iLoc][iTeamToUse] = 0
	MC_serverBD_1.iOldTeamControlkills[iLoc][iTeamToUse] = 0
	IF iPriority < FMMC_MAX_RULES
		IF MC_serverBD.iRuleReCapture[iTeamToUse][ipriority] <= 0
			IF iTeamToUse = iCapturingTeam
				IF IS_BIT_SET(MC_serverBD.iLocteamFailBitset[iLoc], iCapturingTeam)
					CLEAR_BIT(MC_serverBD.iLocteamFailBitset[iLoc], iCapturingTeam)
					bwasobjective = TRUE
				ENDIF
			ELSE
				IF DOES_TEAM_LIKE_TEAM(iTeamToUse, iCapturingTeam)
					IF IS_BIT_SET(MC_serverBD.iLocteamFailBitset[iLoc],iTeamToUse)
						CLEAR_BIT(MC_serverBD.iLocteamFailBitset[iLoc],iTeamToUse)
						bwasobjective = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC INT GET_TEAM_USING_GOTO_LOCATE_AS_OBJECTIVE(INT iLoc)
	
	INT iTeam
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] < FMMC_MAX_RULES
		AND MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] != -1
			PRINTLN("[RCC MISSION][LOCATES] GET_TEAM_USING_GOTO_LOCATE_AS_OBJECTIVE - iLoc : ", iLoc, " iTeam: ", iTeam)
			RETURN iTeam
		ENDIF
	ENDFOR
	
	RETURN -1
	
ENDFUNC

FUNC INT GET_TAKEOVER_TIME_FOR_CAPTURE_LOCATION(INT iLoc, INT iTeam)
	RETURN MC_serverBD.iGotoLocationDataTakeoverTime[iTeam][MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam]]
ENDFUNC

PROC PROCESS_SYNCED_CAPTURE_SCORING(INT iLoc)
	IF NOT g_bMissionEnding
	AND (NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
	OR ARE_WE_CURRENTLY_IN_SUDDEN_DEATH() AND (MC_playerBD[iLocalPart].iteam != -1 AND (GET_LOCAL_TEAM_PERSONAL_LOCATE_TOTAL() != MC_serverBD.iTeamScore[MC_playerBD[iLocalPart].iteam])))
		IF HAS_NET_TIMER_STARTED(MC_serverBD.tdCapturePointsTimer)
			IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][0] < FMMC_MAX_RULES
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdCapturePointsTimer) > GET_TAKEOVER_TIME_FOR_CAPTURE_LOCATION(iLoc, 0)

					IF bIsLocalPlayerHost
						IF MC_serverBD.iLocOwner[iLoc] != -1
							INT iTeam
							FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
								IF MC_serverBD.iLocOwner[iLoc] = iteam
								AND MC_serverBD_4.iGotoLocationDataPriority[iLoc][iteam] < FMMC_MAX_RULES
									iNumberLocsChecked++
									PRINTLN("PROCESS_SYNCED_CAPTURE_SCORING - Incrementing for captured location: ", iLoc)
									IF MC_serverBD.iTeamScore[iTeam] < g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore
									AND NOT IS_GOTO_LOCATE_BEING_CONTESTED(iLoc, FALSE)
										INCREMENT_SERVER_TEAM_SCORE(iteam, MC_serverBD_4.iGotoLocationDataPriority[iLoc][iteam], GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iGotoLocationDataPriority[iLoc][iteam]))
									ENDIF
								ENDIF
							ENDFOR
						ELSE
							iNumberLocsChecked++
							PRINTLN("PROCESS_SYNCED_CAPTURE_SCORING - Incrementing for location: ", iLoc)
						ENDIF
					ENDIF
					
				ELSE
					IF iCapturePointBroadcastBitSet != 0
						iCapturePointBroadcastBitSet = 0
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF bIsLocalPlayerHost
			IF iNumberLocsChecked >= (MC_serverBD.iNumLocCreated)
				REINIT_NET_TIMER(MC_serverBD.tdCapturePointsTimer)
				iNumberLocsChecked = 0
				iCapturePointBroadcastBitSet = 0
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ##### ---------------------------------------------------------------------------------------
// ##### Section Name: Every Frame												----------------
// ##### Description: Functions for every frame location processing				----------------
// ##### ---------------------------------------------------------------------------------------
// ##### ---------------------------------------------------------------------------------------

PROC PROCESS_LOCATION_HUD_AND_BLIPS(INT iLoc)
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iPriority = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	INT iControlHUDFlash
	TEXT_LABEL_63 tlCaptureBarText
	
	INT i
	
	IF HAS_NET_TIMER_STARTED(tdcontolHUDFlash)
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdcontolHUDFlash) > 500
			RESET_NET_TIMER(tdcontolHUDFlash)
		ELSE
			iControlHUDFlash = 500
		ENDIF
	ENDIF
	
	BOOL bBlipExists = DOES_BLIP_EXIST(LocBlip[iLoc])
	
	IF bBlipExists
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_OnlyShowBlipWhenOffRadar)
			IF VDIST2(vLocalPlayerPosition, GET_LOCATION_VECTOR(iLoc)) < POW(100, 2)
				SET_BLIP_ALPHA(LocBlip[iLoc], 0) 
			ELSE
				SET_BLIP_ALPHA(LocBlip[iLoc], 255) 
			ENDIF
		ENDIF

		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin > 0
			IF ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(iTeam)
				SET_BLIP_COLOUR(LocBlip[iLoc], BLIP_COLOUR_YELLOW)
			ELSE
				SET_BLIP_COLOUR(LocBlip[iLoc], BLIP_COLOUR_RED)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2, ciBS2_STOP_GO_HUD)
		IF MC_playerBD[iPartToUse].iCurrentLoc != -1
			IF HAS_NET_TIMER_STARTED(stStopHUD)
				IF HAS_NET_TIMER_EXPIRED(stStopHUD,200)
				AND (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH( "appinternet" ) ) = 0 )
					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
					DRAW_SCALEFORM_MOVIE_FULLSCREEN(cduiIntro.uiCountdown, 255, 255, 255, 100)
					
					INT iR, iG, iB, iA
					GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
								
					BEGIN_SCALEFORM_MOVIE_METHOD(cduiIntro.uiCountdown, "SET_MESSAGE")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("STOP_DASH")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iR)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iG)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iB)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)  
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
			ELSE
				START_NET_TIMER(stStopHUD)
			ENDIF
		ELSE
			IF iPriority > 0
				IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME+iTeam)
					START_NET_TIMER(stStopGoHUD)
					PRINTLN("[MMacK][StopGo] Started go timer")
				ELSE
					IF HAS_NET_TIMER_STARTED(stStopGoHUD)
						IF HAS_NET_TIMER_EXPIRED(stStopGoHUD,3000)
							PRINTLN("[MMacK][StopGo] Killed go timer")
							RESET_NET_TIMER(stStopGoHUD)
							RESET_NET_TIMER(stStopHUD)
						ELSE
							SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
							DRAW_SCALEFORM_MOVIE_FULLSCREEN(cduiIntro.uiCountdown, 255, 255, 255, 100)
							
							INT iR, iG, iB, iA
							GET_HUD_COLOUR(HUD_COLOUR_GREEN, iR, iG, iB, iA)
							
							BEGIN_SCALEFORM_MOVIE_METHOD(cduiIntro.uiCountdown, "SET_MESSAGE")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CNTDWN_GO")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iR)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iG)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iB)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
							END_SCALEFORM_MOVIE_METHOD()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	iControlHUDFlash = 0
	
	IF iPriority < FMMC_MAX_RULES
	AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_FINISHED)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iPriority], ciBS_RULE_HIDE_HUD)
			IF MC_playerBD[iPartToUse].iCurrentLoc = iLoc
			OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iPriority], ciBS_RULE4_SHOW_CAPTURE_BARS_AT_ALL_TIMES)
				IF MC_serverBD_4.iGotoLocationDataRule[iLoc][iTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
					FOR i = 0 TO FMMC_MAX_TEAMS -1
						IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][i] < FMMC_MAX_RULES
							IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iAreaTimer[iLoc][i])
								IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][i] = MC_serverBD_4.iCurrentHighestPriority[i]
									IF bPlayerToUseOK
										IF NOT IS_SPECTATOR_HUD_HIDDEN()
										AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
										AND NOT IS_OBJECTIVE_BLOCKED()
										AND NOT IS_STRING_NULL_OR_EMPTY(g_sMission_TeamName[i])
										
											g_b_ChangePlayerNameToTeamName = TRUE
											INT iMaxCapTime = GET_TAKEOVER_TIME_FOR_CAPTURE_LOCATION(iLoc, iTeam)
											INT iTimeNumber = iMaxCapTime
											
											IF iMaxCapTime = 0
												PRINTLN("[[AW SMOKE]]iMaxCapTime is zero - setting to default value 240000")
												//Setting iMaxCapTime to the default value if it's zero - to fix 2601090
												iTimeNumber = 240000
											ENDIF
											
											BOOL bShowCaptureBar = TRUE
											
											IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_NEVER_SHOW_BOTTOM_RIGHT_CAPTURE_BARS)
												bShowCaptureBar = FALSE
											ENDIF
											
											IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciONLY_SHOW_CAPTURE_BARS_IN_AREA)
												IF (NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iAreaTimer[iLoc][i])
												AND MC_serverBD.iLocOwner[iLoc] != iTeam)
												OR MC_serverBD.iLocOwner[iLoc] = iTeam
													bShowCaptureBar = FALSE
												ENDIF
											ENDIF
											
											IF bShowCaptureBar
												IF iTeam = i
												AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
													tlCaptureBarText = g_sMission_TeamName[iTeam]
													
													IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_ShowCustomHUDTextOnCaptureBar)
													AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63ObjectiveEntityHUDMessage[iPriority])
														tlCaptureBarText = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63ObjectiveEntityHUDMessage[iPriority]
													ENDIF
													DRAW_GENERIC_METER(ROUND(iCurrCapTime[iLoc][i]),iMaxCapTime,tlCaptureBarText,GET_HUD_COLOUR_FOR_FMMC_TEAM(i, PlayerToUse),-1,HUDORDER_DONTCARE,-1,-1,FALSE,TRUE,HUDFLASHING_FLASHWHITE,iControlHUDFlash, DEFAULT, DEFAULT, TRUE)
												ELSE
													IF iTeam = i
														DRAW_GENERIC_METER(ROUND(iCurrCapTime[iLoc][i]),iTimeNumber,g_sMission_TeamName[i],GET_HUD_COLOUR_FOR_FMMC_TEAM(i, PlayerToUse),-1,HUDORDER_SEVENTHBOTTOM,-1,-1,FALSE,TRUE,HUDFLASHING_FLASHWHITE,iControlHUDFlash, DEFAULT, DEFAULT, TRUE)
													ELSE
														DRAW_GENERIC_METER(ROUND(iCurrCapTime[iLoc][i]),iTimeNumber,g_sMission_TeamName[i],GET_HUD_COLOUR_FOR_FMMC_TEAM(i, PlayerToUse),-1,INT_TO_ENUM(HUDORDER, ENUM_TO_INT(HUDORDER_SIXTHBOTTOM) - i),-1,-1,FALSE,TRUE,HUDFLASHING_FLASHWHITE,iControlHUDFlash, DEFAULT, DEFAULT, TRUE)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									//Turn on the red filter if this is the active rule
									IF MC_playerBD[iPartToUse].iCurrentLoc = iLoc
										IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iPriority], ciBS_RULE4_SHOW_CAPTURE_BARS_AT_ALL_TIMES)
										AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_NEVER_SHOW_BOTTOM_RIGHT_CAPTURE_BARS)
											IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iPriority], ciBS_RULE5_RED_FILTER_IN_CAP_ZONE)
											AND NOT IS_OBJECTIVE_BLOCKED()
												IF SHOULD_RED_FILTER_SHOW(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], iLoc)
													IF NOT ANIMPOSTFX_IS_RUNNING("CrossLine")
														IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_RED_FILTER_SOUND_PLAYED)
															PLAY_RED_FILTER_SOUND()
														ENDIF
														ANIMPOSTFX_PLAY("CrossLine", 0, TRUE)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELSE
									//Turn off the red filter if it is turned on and we are no longer on the correct rule
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iPriority], ciBS_RULE5_RED_FILTER_IN_CAP_ZONE)
										IF ANIMPOSTFX_IS_RUNNING("CrossLine")
											ANIMPOSTFX_STOP("CrossLine")
											STRING sSoundSet 
											sSoundSet = "DLC_Apartments_Drop_Zone_Sounds"
											PLAY_SOUND_FRONTEND(-1,"Exit_Capture_Zone",sSoundSet,FALSE)
											ANIMPOSTFX_PLAY("CrossLineOut", 0, FALSE)
											CLEAR_BIT(iLocalBoolCheck27, LBOOL27_RED_FILTER_SOUND_PLAYED)
										ENDIF 
									ENDIF
								ENDIF
							ELSE
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iPriority], ciBS_RULE4_SHOW_CAPTURE_BARS_AT_ALL_TIMES)
								AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_NEVER_SHOW_BOTTOM_RIGHT_CAPTURE_BARS) 
									IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][i] = MC_serverBD_4.iCurrentHighestPriority[i]
										IF bPlayerToUseOK
											IF NOT IS_SPECTATOR_HUD_HIDDEN()
											AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
											AND NOT IS_OBJECTIVE_BLOCKED()
											AND NOT IS_STRING_NULL_OR_EMPTY(g_sMission_TeamName[i])
												g_b_ChangePlayerNameToTeamName = TRUE
												IF iTeam = i
													PRINTLN("[[AW]][DRAW ON BOTTOM] - 1 - iTeam: ",iTeam, " i: ", i )
													
													tlCaptureBarText = g_sMission_TeamName[iTeam]
													
													IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_ShowCustomHUDTextOnCaptureBar)
													AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63ObjectiveEntityHUDMessage[iPriority])
														tlCaptureBarText = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63ObjectiveEntityHUDMessage[iPriority]
													ENDIF
													
													//2593820 - Always have your teams bar on top in drop zone and opponent bars below that.
													DRAW_GENERIC_METER(0, GET_TAKEOVER_TIME_FOR_CAPTURE_LOCATION(iLoc, iTeam),tlCaptureBarText,GET_HUD_COLOUR_FOR_FMMC_TEAM(i, PlayerToUse),-1,HUDORDER_SEVENTHBOTTOM,-1,-1,TRUE,TRUE,HUDFLASHING_FLASHWHITE,iControlHUDFlash)
												ELSE
													PRINTLN("[[AW]][DRAW ON BOTTOM] - 2 - iTeam: ",iTeam, " i: ", i )
													DRAW_GENERIC_METER(0, GET_TAKEOVER_TIME_FOR_CAPTURE_LOCATION(iLoc, iTeam) ,g_sMission_TeamName[i],GET_HUD_COLOUR_FOR_FMMC_TEAM(i, PlayerToUse),-1,INT_TO_ENUM(HUDORDER, ENUM_TO_INT(HUDORDER_SIXTHBOTTOM) - i),-1,-1,TRUE,TRUE,HUDFLASHING_FLASHWHITE,iControlHUDFlash)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bBlipExists
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_AppearAsBlue)
			INT iR, iG, iB, iA
			GET_HUD_COLOUR(HUD_COLOUR_BLUE, iR, iG, iB, iA)
			APPLY_SECONDARY_COLOUR_TO_BLIP(LocBlip[iLoc], iR, iG, iB)
			PRINTLN("[url:bugstar:3342736] BLIP_CURRENT_CHECKPOINT, APPLY_SECONDARY_COLOUR_TO_BLIP, iLoc =  ", iLoc)
		ENDIF
	
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciCHANGE_BLIP_COLOUR_TO_CONTROLLING_TEAM)
			IF MC_serverBD.iLocOwner[iLoc] > -1
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_CONTESTED_COLOUR_FOR_LOCATES)
				AND IS_GOTO_LOCATE_BEING_CONTESTED(iLoc)
					SET_BLIP_COLOUR(LocBlip[iLoc], BLIP_COLOUR_RED)
					SET_BLIP_COLOUR(LocBlip1[iLoc], BLIP_COLOUR_RED)
				ELSE
					SET_BLIP_COLOUR(LocBlip[iLoc], GET_INT_FROM_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_serverBD.iLocOwner[iLoc],LocalPlayer)))
					SET_BLIP_COLOUR(LocBlip1[iLoc], GET_INT_FROM_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_serverBD.iLocOwner[iLoc],LocalPlayer)))
				ENDIF
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_CONTESTED_COLOUR_FOR_LOCATES)
				AND IS_GOTO_LOCATE_BEING_CONTESTED(iLoc, FALSE, TRUE)
					SET_BLIP_COLOUR(LocBlip[iLoc], BLIP_COLOUR_RED)
					SET_BLIP_COLOUR(LocBlip1[iLoc], BLIP_COLOUR_RED)
				ELSE
					SET_BLIP_COLOUR(LocBlip[iLoc], BLIP_COLOUR_YELLOW)
					SET_BLIP_COLOUR(LocBlip1[iLoc], BLIP_COLOUR_YELLOW)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][MC_playerBD[iPartToUse].iteam] > MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
	OR (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS,ciLoc_BS_HideBlipWhenInLocation) AND MC_playerBD[iPartToUse].iCurrentLoc = iLoc)
		IF bBlipExists
			PRINTLN("[LOCAL_PROCESS_LOCATION_OBJECTIVES] (LocBlip) Removing Blip 1")
			REMOVE_BLIP(LocBlip[iLoc])
		ENDIF
		
		IF DOES_BLIP_EXIST(LocBlip1[iLoc])			
			PRINTLN("[LOCAL_PROCESS_LOCATION_OBJECTIVES] (LocBlip) Removing Blip 3")
			REMOVE_BLIP(LocBlip1[iLoc])
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_LOCATION_CAPTURE__CAPTURE_TIME(INT iLoc, INT iTeam, INT iPriority)
	IF NOT IS_RULE_INDEX_VALID(iPriority)
	OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_FINISHED)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iPriority], ciBS_RULE_HIDE_HUD)
		EXIT
	ENDIF
	
	IF MC_playerBD[iPartToUse].iCurrentLoc != iLoc
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iPriority], ciBS_RULE4_SHOW_CAPTURE_BARS_AT_ALL_TIMES)
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iGotoLocationDataRule[iLoc][iTeam] != FMMC_OBJECTIVE_LOGIC_CAPTURE
		EXIT
	ENDIF
					
	INT iTeamLoop
	
	FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS -1
		
		IF NOT IS_RULE_INDEX_VALID(MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeamLoop])
			RELOOP
		ENDIF
		
		IF NOT IS_BIT_SET(MC_serverBD.iAreaTimerStartedBS[iTeamLoop], iLoc)
			SET_CAPTURE_TIME(0, iLoc, iTeamLoop)
			RELOOP
		ENDIF
		
		IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeamLoop] != MC_serverBD_4.iCurrentHighestPriority[iTeamLoop]
			//[JS]Pause timer for LBD
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciLBD_SHOW_TIME_COLUMN)
				IF fTempCaptureTime > 0
					MC_playerBD[iLocalPart].fCaptureTime += fTempCaptureTime
					fTempCaptureTime = 0
				ENDIF
			ENDIF
			RELOOP
		ENDIF
		
		IF bPlayerToUseOK
			IF NOT IS_SPECTATOR_HUD_HIDDEN()
			AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
			AND NOT IS_OBJECTIVE_BLOCKED()
			AND NOT IS_STRING_NULL_OR_EMPTY(g_sMission_TeamName[iTeamLoop])
			
				g_b_ChangePlayerNameToTeamName = TRUE
				INT iCurrentCapTarget = MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD.iAreaTimer[iLoc][iTeamLoop])
				INT iMaxCapTime = GET_TAKEOVER_TIME_FOR_CAPTURE_LOCATION(iLoc, iTeam)
				FLOAT iCurrentDifference = iCurrentCapTarget - iCurrCapTime[iLoc][iTeamLoop]
				
				IF IS_AREA_ABLE_TO_BE_CAPTURED(iteam, iLoc, (MC_serverBD.iLocOwner[iLoc] != -1))
					IF iCurrentDifference > 0
					OR iCurrCapTime[iLoc][iTeamLoop] = -1
						FLOAT fProportionDiff = iCurrentDifference / TO_FLOAT(iMaxCapTime)
						
						FLOAT fMaxBarFillRate = 0.5 * fLastFrameTime // This should be the fastest rate at which any bar will fill (a 2 second capture time)
						
						PRINTLN("[Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE__CAPTURE_TIME - Location ", iLoc, " Team ", iTeamLoop)
						PRINTLN("[Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE__CAPTURE_TIME - iCurrentDifference ", iCurrentDifference, " iMaxCapTime ", iMaxCapTime)
						PRINTLN("[Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE__CAPTURE_TIME - fProportionDiff ", fProportionDiff, " fMaxBarFillRate ", fMaxBarFillRate)
						PRINTLN("[Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE__CAPTURE_TIME - iCurrCapTime[iLoc][iTeamLoop]: ", iCurrCapTime[iLoc][iTeamLoop], " iCurrentCapTarget: ", iCurrentCapTarget)
						
						IF iCurrCapTime[iLoc][iTeamLoop] != -1
						AND fProportionDiff > fMaxBarFillRate
							FLOAT fChange = iCurrentDifference
							
							FLOAT fMaxFillForThisBar = (fMaxBarFillRate + 0.02) * iMaxCapTime
							
							PRINTLN("[Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE__CAPTURE_TIME - fLastFrameTime ", fLastFrameTime)
							PRINTLN("[Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE__CAPTURE_TIME - fChange ",fChange,", fMaxFillForThisBar ",fMaxFillForThisBar)
							
							fChange = FMIN(fChange, fMaxFillForThisBar)
							
							SET_CAPTURE_TIME(iCurrCapTime[iLoc][iTeamLoop] + fChange, iLoc, iTeamLoop)
							iPrevCapTime[iLoc][iTeamLoop] = iCurrCapTime[iLoc][iTeamLoop]
							PRINTLN("[Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE__CAPTURE_TIME - Newly interped iCurrCapTime[",iTeamLoop,"] = ",iCurrCapTime[iLoc][iTeamLoop]," thanks to fChange of ",fChange)
							
						ELSE
							PRINTLN("[Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE__CAPTURE_TIME - Just set iCurrCapTime[",iTeamLoop,"] equal to iCurrentCapTarget for loc ",iLoc," of ",iCurrentCapTarget)
							SET_CAPTURE_TIME(TO_FLOAT(iCurrentCapTarget), iLoc, iTeamLoop)
							iPrevCapTime[iLoc][iTeamLoop] = TO_FLOAT(iCurrentCapTarget)
						ENDIF
					ELSE
						PRINTLN("[Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE__CAPTURE_TIME - No time difference for loc ",iLoc,"?")
					ENDIF
				ELSE
					SET_CAPTURE_TIME(iPrevCapTime[iLoc][iTeamLoop], iLoc, iTeamLoop)
				ENDIF

				//Calculate the percentage of the capture bar for other modes
				FLOAT fMaxCapTime = TO_FLOAT(iMaxCapTime)
				FLOAT fCurrentCapTime = iCurrCapTime[iLoc][iTeamLoop]
				FLOAT fCaptureBarPercentage = (fCurrentCapTime/fMaxCapTime)*100
				MC_ServerBD.iCaptureBarPercentage[iTeamLoop] = ROUND(fCaptureBarPercentage)
			
			ENDIF
		ENDIF
		
		IF MC_playerBD[iPartToUse].iCurrentLoc = iLoc
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciLBD_SHOW_TIME_COLUMN)
				fTempCaptureTime += fLastFrameTime
				IF fTempCaptureTime > 1.0
					MC_playerBD[iLocalPart].fCaptureTime += fTempCaptureTime
					fTempCaptureTime = 0
				ENDIF
			ENDIF
		ENDIF
		
	ENDFOR

ENDPROC

PROC CLIENT_LOCATION_EVERY_FRAME_PROCESSING(INT iLoc)
	
	BOOL bInLoc 
	INT i

	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iPriority = MC_serverBD_4.iCurrentHighestPriority[iTeam]
					
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_DisplayMarkerAsVisualOnly)
		UPDATE_LOCATE_FADE_STRUCT( sFadeOutLocations[iLoc], iLoc, iTeam)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ALL_TEAMS_SHARE_LOCATES)
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
			MC_playerBD_1[iLocalPart].iLocBitset = 0
		ENDIF
	ENDIF
	
	PROCESS_LOCATION_OUTFIT_BLIP_RANGE_RESET(iLoc)	
	
	IF iSpectatorTarget = -1
		IF bPlayerToUseOK
			IF NOT IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_DisplayMarkerAsVisualOnly )
				IF  iPriority < FMMC_MAX_RULES
				AND iPriority != -1
					IF NOT IS_VECTOR_ZERO(GET_LOCATION_VECTOR(iLoc))
					
						IF AM_I_IN_SECONDARY_LOCATE_IF_I_NEED_TO_BE(iLoc)
							SET_BIT(MC_playerBD[iLocalPart].iLocationInSecondaryLocation, iLoc)
							PRINTLN("[Loc ",iLoc,"] CLIENT_LOCATION_EVERY_FRAME_PROCESSING iLocationInSecondaryLocation,", iLoc)
						ENDIF
						
						IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin > 0
							fTeamMembers_ReqDist = CALCULATE_MINIMUM_DISTANCE_BETWEEN_TEAM_MATES(iLoc,iTeam) 
						ENDIF
						
						BOOL bPedInLocation = IS_LOCAL_PLAYER_IN_LOCATION(iLoc,  iTeam)
						
						IF IS_LOCATION_AN_OBJECTIVE_FOR_ANY_TEAM(iLoc)
							IF SHOULD_COUNT_LEAVE_LOCATION(iLoc, bPedInLocation AND (MC_playerBD[iPartToUse].iObjectiveTypeCompleted = -1))
								PRINTLN("[RCC MISSION] PROCESS_LOCATION_EVERY_FRAME ", iLoc, " - counting player in leave location. bPedInLocation: ", BOOL_TO_STRING(bPedInLocation), ", iObjectiveTypeCompleted: ", MC_playerBD[iPartToUse].iObjectiveTypeCompleted)
								iTempLeaveLoc = iLoc
								IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] <= iPriority
									SET_BIT(iLocalBoolCheck6, LBOOL6_IN_MY_PRIORITY_LEAVE_LOC)
								ENDIF
							ENDIF
						ENDIF
						
						IF bPedInLocation
						OR HAS_ANY_ONE_HIT_LOCATE(iLoc, iPriority)
							bInLoc = TRUE
							iInAnyLocTemp = iLoc
						
							#IF IS_DEBUG_BUILD
							IF bGotoLocPrints
								PRINTLN("[RCC MISSION] PROCESS_LOCATION_EVERY_FRAME ",iLoc," - Player is in location")
							ENDIF
							#ENDIF
							
							IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] = iPriority
								IF sFadeOutLocations[iLoc].eState = eLocateFadeOutState_DISPLAYING
									IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fSecondaryRadius = 0
									OR (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iTeam] != ciGOTO_LOCATION_INDIVIDUAL
										AND AM_I_IN_SECONDARY_LOCATE_IF_I_NEED_TO_BE(iLoc))
										
										PRINTLN("[RCC MISSION] PROCESS_LOCATION_EVERY_FRAME ",iLoc," Fading out locate radius = ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fRadius[0] )
										
										SET_LOCATE_TO_FADE_OUT(sFadeOutLocations[iLoc], iLoc, iTeam)
										
									ENDIF
								ENDIF
							ELIF sFadeOutLocations[iLoc].iAlphaValue = 0
							AND sFadeOutLocations[iLoc].eState = eLocateFadeOutState_BEFORE_FADE_IN
								SET_LOCATE_TO_NOT_DISPLAY( sFadeOutLocations[iLoc], iLoc, iTeam )
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ALL_TEAMS_SHARE_LOCATES)
								SET_BIT(MC_playerBD_1[iLocalPart].iLocBitset, iLoc)
								PRINTLN("[RCC MISSION] Setting MC_playerBD_1[iLocalPart].iLocBitset, ", iLoc)
							ENDIF
							iLocateFromPreviousFrame = iLoc
							
						ELIF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] = iPriority
						AND sFadeOutLocations[iLoc].eState = eLocateFadeOutState_AFTER_FADE_OUT
						AND (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iTeam] != ciGOTO_LOCATION_INDIVIDUAL
							OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_REACTIVATE_LOCATES))
							#IF IS_DEBUG_BUILD
							IF bGotoLocPrints
								PRINTLN("[RCC MISSION] PROCESS_LOCATION_EVERY_FRAME ",iLoc," - Player is not in location, locate should fade back in")
							ENDIF
							#ENDIF
							
							// If this locate is still the current rule for an all-team-at-locate, fade it back in?
							RESET_LOCATE( iLoc, sFadeOutLocations[iLoc], iTeam )
							 
							SET_LOCATE_TO_FADE_IN( sFadeOutLocations[iLoc], iLoc, iTeam )
							
						ELSE
							#IF IS_DEBUG_BUILD
							IF bGotoLocPrints
								PRINTLN("[RCC MISSION] PROCESS_LOCATION_EVERY_FRAME ",iLoc," - Player is not in location, B")
							ENDIF
							#ENDIF
						ENDIF
						
						IF NOT bPedInLocation
						AND iLocateFromPreviousFrame = iLoc
							
							SET_CAPTURE_TIME(-1, iLoc, iTeam)
							
							// Broadcast that we just left a locate.
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetSixteen, ciDISABLE_RECPATURE_OF_LOCATES_WHEN_LEAVING_ONE)	
								PRINTLN("[RCC MISSION] [LM]PROCESS_LOCATION_EVERY_FRAME - IN")
								IF NOT HAS_NET_TIMER_STARTED(tdDisableLocateRecaptureTimer[iLocalPart])
									PRINTLN("[RCC MISSION] PROCESS_LOCATION_EVERY_FRAME - [Delay Recapture] - Left Locate")
									iLocateFromPreviousFrame = -1
									BROADCAST_FMMC_LEFT_LOCATE_DELAY_RECAPTURE(iLocalPart, iLoc)
								ENDIF
							ELSE	
								iLocateFromPreviousFrame = -1
							ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD
					ELIF bGotoLocPrints
						PRINTLN("[RCC MISSION] PROCESS_LOCATION_EVERY_FRAME ",iLoc," - Location vector is zero")
					#ENDIF
					ENDIF
				ENDIF
			ELSE
				UPDATE_LOCATE_FADE_STRUCT_FOR_LINE_MARKER( sFadeOutLocations[iLoc], iLoc, iTeam, RETURN_DISTANCE_FROM_LINE_FOR_LOCAL_PLAYER(playerStateListDataCrossTheLine))
			ENDIF
		ELSE
			iTempLeaveLoc = -1
			
			#IF IS_DEBUG_BUILD
			IF bGotoLocPrints
				PRINTLN("[RCC MISSION] PROCESS_LOCATION_EVERY_FRAME ",iLoc," - Player isn't okay")
			ENDIF
			#ENDIF
		ENDIF
		
		IF  iPriority < FMMC_MAX_RULES
		AND iPriority != -1
			IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] <= iPriority
			AND MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] < FMMC_MAX_RULES
				IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = -1
					
					IF bPlayerToUseOK

						//this is only to display the chevron at the right height - not very efficent but was what I was told to do.
						IF NOT IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_DisplayMarkerAsVisualOnly )
							IF MC_playerBD[iPartToUse].iCurrentLoc != iLoc								
							
								IF bInLoc
								
									MC_playerBD[iPartToUse].iCurrentLoc = iLoc
									
									PRINTLN("[RCC MISSION] PROCESS_LOCATION_EVERY_FRAME - 1 Setting iCurrentLoc to ", MC_playerBD[iLocalPart].iCurrentLoc)
									
									#IF IS_DEBUG_BUILD
									IF bGotoLocPrints
										PRINTLN("[RCC MISSION] PROCESS_LOCATION_EVERY_FRAME ",iLoc," - Setting iCurrentLoc as this locate!")
									ENDIF
									#ENDIF
					
								ENDIF
								
							#IF IS_DEBUG_BUILD
							ELIF bGotoLocPrints
								PRINTLN("[RCC MISSION] PROCESS_LOCATION_EVERY_FRAME ",iLoc," - iCurrentLoc is already set to this locate")
							#ENDIF
							ENDIF
						ENDIF
						
						IF (NOT bInLoc)
						OR (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iTeam] = ciGOTO_LOCATION_INDIVIDUAL)
						OR ((g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fSecondaryRadius > 0) AND NOT AM_I_IN_SECONDARY_LOCATE_IF_I_NEED_TO_BE( iLoc ))
														
							IF ENUM_TO_INT( sFadeOutLocations[iLoc].eState ) < ENUM_TO_INT( eLocateFadeOutState_AFTER_FADE_OUT )
								 
								IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] = iPriority // Only fade in if it's our current rule
									SET_LOCATE_TO_FADE_IN( sFadeOutLocations[iLoc], iLoc, iTeam )
								ENDIF

								INT iTeamWhoControlsLoc = -1
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iPriority], ciBS_RULE9_TEAM_COLOURED_LOCATES)
									iTeamWhoControlsLoc = MC_serverBD.iLocOwner[iLoc]
									
									// It doesn't matter if this sets a team twice, because we override that with the contested colour
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)
										FOR i = 0 TO FMMC_MAX_TEAMS-1
											IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0												
												iTeamWhoControlsLoc = i
											ENDIF
										ENDFOR
									ENDIF
								ENDIF
								
								DRAW_ONE_LOCATION_MARKER( iLoc, sFadeOutLocations[iLoc].iAlphaValue, DEFAULT, iTeamWhoControlsLoc )
							#IF IS_DEBUG_BUILD
							ELSE
								IF b4517649Debug
									PRINTLN("[4517649Debug] iLoc:", iLoc," Enum: ", ENUM_TO_INT(sFadeOutLocations[iLoc].eState))
								ENDIF
							#ENDIF
							ENDIF
						#IF IS_DEBUG_BUILD
						ELSE
							IF b4517649Debug
								PRINTLN("[4517649Debug] iLoc:", iLoc," NOT bInLoc: ", BOOL_TO_STRING((NOT bInLoc)))
								PRINTLN("[4517649Debug] iLoc:", iLoc," Check 2: ", BOOL_TO_STRING(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iTeam] = ciGOTO_LOCATION_INDIVIDUAL))
								PRINTLN("[4517649Debug] iLoc:", iLoc," Check 3: ", BOOL_TO_STRING(((g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fSecondaryRadius > 0) AND NOT AM_I_IN_SECONDARY_LOCATE_IF_I_NEED_TO_BE( iLoc ))))
							ENDIF
						#ENDIF
						ENDIF
												
						IF (MC_playerBD[iPartToUse].iCurrentLoc = iLoc)
							IF MC_serverBD_4.iGotoLocationDataRule[iLoc][iTeam] = FMMC_OBJECTIVE_LOGIC_PHOTO
								
								VECTOR vLoc = GET_LOCATION_VECTOR(iLoc)
								
								IF NOT DOES_PHOTO_TRACKED_POINT_EXIST(iPhotoTrackedPoint)
									//create new tracked point for current target coords
									CREATE_PHOTO_TRACKED_POINT_FOR_COORD(iPhotoTrackedPoint, vLoc, 0.2)
									vLocalPhotoCoords = vLoc
								ELSE
									IF NOT ARE_VECTORS_EQUAL(vLoc, vLocalPhotoCoords)
										vLocalPhotoCoords = vLoc
										SET_TRACKED_POINT_INFO(iPhotoTrackedPoint,vLoc,0.2)
									ENDIF
								ENDIF
								
								IF DOES_PHOTO_TRACKED_POINT_EXIST(iPhotoTrackedPoint)
								AND IS_PHOTO_TRACKED_POINT_VISIBLE(iPhotoTrackedPoint)
									IF IS_POINT_IN_CELLPHONE_CAMERA_VIEW(vLoc, 0.2)
										IF HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
											MC_playerBD[iPartToUse].iLocPhoto = iLoc
											PRINTLN("[RCC MISSION] PROCESS_LOCATION_EVERY_FRAME - Photo taken of location ", iLoc)
											
											IF SHOULD_PUT_AWAY_PHONE_AFTER_PHOTO(MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam], ci_TARGET_LOCATION)
												REINIT_NET_TIMER(tdPhotoTakenTimer)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						// Broadcast that we just left a locate.
						IF MC_playerBD[iPartToUse].iCurrentLoc > -1
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetSixteen, ciDISABLE_RECPATURE_OF_LOCATES_WHEN_LEAVING_ONE)						
								IF NOT HAS_NET_TIMER_STARTED(tdDisableLocateRecaptureTimer[iLocalPart])
									iLocateFromPreviousFrame = -1
									BROADCAST_FMMC_LEFT_LOCATE_DELAY_RECAPTURE(iLocalPart, MC_playerBD[iLocalPart].iCurrentLoc)
								ENDIF
							ENDIF
						ENDIF
						
						MC_playerBD[iPartToUse].iCurrentLoc = -1						
											
						#IF IS_DEBUG_BUILD
						IF bGotoLocPrints
							PRINTLN("[RCC MISSION] PROCESS_LOCATION_EVERY_FRAME ",iLoc," - Unable to set iCurrentLoc as player isn't okay")
						ENDIF
						#ENDIF
					ENDIF
					
				ELIF MC_playerBD[iPartToUse].iCurrentLoc != iLoc
				
					#IF IS_DEBUG_BUILD
					IF bGotoLocPrints
						PRINTLN("[RCC MISSION] PROCESS_LOCATION_EVERY_FRAME ",iLoc," - Unable to set iCurrentLoc as iObjectiveTypeCompleted = ",MC_playerBD[iPartToUse].iObjectiveTypeCompleted = -1)
					ENDIF
					#ENDIF
					
					// but we still want to draw aerial markers! So if we find one, still draw it. So long as it isn't the one we just flew through
					IF ENUM_TO_INT( sFadeOutLocations[iLoc].eState ) < ENUM_TO_INT( eLocateFadeOutState_AFTER_FADE_OUT )
					AND ENUM_TO_INT( sFadeOutLocations[iLoc].eState ) > ENUM_TO_INT( eLocateFadeOutState_BEFORE_FADE_IN )
						DRAW_ONE_LOCATION_MARKER( iLoc, sFadeOutLocations[iLoc].iAlphaValue )
					ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELIF bGotoLocPrints
				PRINTLN("[RCC MISSION] PROCESS_LOCATION_EVERY_FRAME ",iLoc," - Location is not a priority, priority for my team (",iTeam,") = ",MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam])
			#ENDIF
			ENDIF
		ENDIF
	ELIF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset, PBBOOL_HEIST_SPECTATOR)
		
		#IF IS_DEBUG_BUILD
		IF bGotoLocPrints
			PRINTLN("[RCC MISSION] PROCESS_LOCATION_EVERY_FRAME ",iLoc," - Player is a heist spectator")
		ENDIF
		#ENDIF
		
		IF bLocalPlayerPedOk
			IF MC_playerBD[iLocalPart].iObjectiveTypeCompleted = -1
			AND NOT IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_DisplayMarkerAsVisualOnly )
				IF NOT IS_VECTOR_ZERO(GET_LOCATION_VECTOR(iLoc))
					IF IS_LOCAL_PLAYER_IN_LOCATION(iLoc,  MC_playerBD[iLocalPart].iteam )
						
						IF IS_LOCATION_AN_OBJECTIVE_FOR_ANY_TEAM(iLoc)
							iInAnyLocTemp = iLoc
							IF SHOULD_COUNT_LEAVE_LOCATION(iLoc)
								iTempLeaveLoc = iLoc
							ENDIF
						ENDIF
						
						IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][MC_playerBD[iLocalPart].iteam] <= iPriority
						AND MC_serverBD_4.iGotoLocationDataPriority[iLoc][MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
							IF MC_playerBD[iLocalPart].iCurrentLoc != iLoc
								MC_playerBD[iLocalPart].iCurrentLoc = iLoc
								PRINTLN("[RCC MISSION] PROCESS_LOCATION_EVERY_FRAME - 2 Setting iCurrentLoc to ", MC_playerBD[iLocalPart].iCurrentLoc)
							ENDIF
						ENDIF
					ELSE
						IF IS_LOCATION_AN_OBJECTIVE_FOR_ANY_TEAM(iLoc)
							IF SHOULD_COUNT_LEAVE_LOCATION(iLoc, FALSE)
								iTempLeaveLoc = iLoc
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			// Broadcast that we just left a locate.
			IF MC_playerBD[iLocalPart].iCurrentLoc > -1
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetSixteen, ciDISABLE_RECPATURE_OF_LOCATES_WHEN_LEAVING_ONE)							
					IF NOT HAS_NET_TIMER_STARTED(tdDisableLocateRecaptureTimer[iLocalPart])
						iLocateFromPreviousFrame = -1
						BROADCAST_FMMC_LEFT_LOCATE_DELAY_RECAPTURE(iLocalPart, MC_playerBD[iLocalPart].iCurrentLoc)
					ENDIF
				ENDIF
			ENDIF
			
			MC_playerBD[iLocalPart].iCurrentLoc = -1
			iTempLeaveLoc = -1
			
		ENDIF
	ENDIF
	
	IF iSpectatorTarget != -1
		IF NOT g_bMissionEnding
			IF iPriority < FMMC_MAX_RULES
			AND iPriority != -1
				IF NOT IS_BLIP_SET_AS_HIDDEN(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct)
					IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] <= iPriority
						IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iArial[0] = 1
							DRAW_AIR_MARKER(GET_LOCATION_VECTOR(iLoc),FALSE,iLoc)
						ELSE
							IF bPlayerToUseOK
								IF IS_PLAYER_IN_LOCATION(PlayerPedToUse, iLoc,  iTeam )
									PRINTLN("[RCC MISSION] PROCESS_LOCATION_EVERY_FRAME I'm spectator iLoc = ",iLoc," Fading out locate radius = ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fRadius[0])
									SET_LOCATE_TO_FADE_OUT(sFadeOutLocations[iLoc], iLoc, iTeam)
								ELSE
									SET_LOCATE_TO_DISPLAY(sFadeOutLocations[iLoc], iLoc, iTeam)
								ENDIF
								INT iTeamWhoControlsLoc = -1
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iPriority], ciBS_RULE9_TEAM_COLOURED_LOCATES)
									iTeamWhoControlsLoc = MC_serverBD.iLocOwner[iLoc]
									
									// It doesn't matter if this sets a team twice, because we override that with the contested colour
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)
										FOR i = 0 TO FMMC_MAX_TEAMS-1
											IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0												
												iTeamWhoControlsLoc = i
											ENDIF
										ENDFOR
									ENDIF
								ENDIF
								DRAW_ONE_LOCATION_MARKER( iLoc, sFadeOutLocations[iLoc].iAlphaValue, DEFAULT, iTeamWhoControlsLoc )
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].sHideLocationMarkerStruct[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]].iHideBS[iLoc], ciBS_HIDE_LOCATION_MARKER_STRUCT)
			IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_DisplayMarkerAsVisualOnly )
				PRINTLN("[RCC MISSION][LOCATES] iLoc: ",iLoc, " != -1 " )
				
				IF GET_TEAM_USING_GOTO_LOCATE_AS_OBJECTIVE(iLoc) != iTeam
					PRINTLN("[RCC MISSION][LOCATES] iLoc: ",iLoc, " GET_TEAM_USING_GOTO_LOCATE_AS_OBJECTIVE(iLoc) = iTeam " ) 
					DRAW_ONE_LOCATION_MARKER( iLoc, sFadeOutLocations[iLoc].iAlphaValue,TRUE)
					CREATE_BLIP_FOR_ENEMY_MARKER_LINES(iLoc,TRUE)
				ELSE
					PRINTLN("[RCC MISSION][LOCATES] iLoc: ",iLoc, " GET_TEAM_USING_GOTO_LOCATE_AS_OBJECTIVE(iLoc) = iTeam " ) 
					DRAW_ONE_LOCATION_MARKER( iLoc, sFadeOutLocations[iLoc].iAlphaValue)
					CREATE_BLIP_FOR_ENEMY_MARKER_LINES(iLoc)
				ENDIF
			ELSE
				IF sFadeOutLocations[iLoc].eState = eLocateFadeOutState_FADING_OUT
					DRAW_ONE_LOCATION_MARKER( iLoc, sFadeOutLocations[iLoc].iAlphaValue )
				ELSE
					CHECK_FADED_OUT_CHECKPOINT(iLoc, iTeam, iPriority, FALSE) 
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	PROCESS_LOCATION_CAPTURE__CAPTURE_TIME(iLoc, iTeam, iPriority)	
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_CrossLineDistUseMe_T0)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_CrossLineDistUseMe_T1)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_CrossLineDistUseMe_T2)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_CrossLineDistUseMe_T3)
		IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED)
		AND IS_TEAM_ACTIVE(MC_playerBD[iPartToUse].iTeam)
		AND iPriority < FMMC_MAX_RULES
			SET_BIT(iLocalBoolCheck13, LBOOL13_DRAW_DISTANCE_TO_CROSS_LINE)
			CALCULATE_DISTANCES_TO_LOCATE(iLoc)
		ENDIF
	ENDIF
		
	IF  iPriority < FMMC_MAX_RULES
	AND iPriority != -1
	AND MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] = iPriority
		DRAW_LOCATE_RADIUS_BLIP(iPriority, iLoc)
		IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
			IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin > 0
				IF NOT ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(MC_PlayerBD[iPartToUse].iTeam)
					INT iPart
					INT iPlayersChecked
					PARTICIPANT_INDEX tempPart
					PRINTLN("[PLAYER_LOOP] - TDFMARKER")
					FOR iPart = 0 TO (NUM_NETWORK_PLAYERS - 1)
						IF iPart != iPartToUse
							IF MC_playerBD[iPart].iteam = MC_playerBD[iPartToUse].iteam
								tempPart = INT_TO_PARTICIPANTINDEX(iPart)
								IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
									IF (NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_ANY_SPECTATOR)
										OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_HEIST_SPECTATOR))
									AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
										IF IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
											IF NOT IS_BIT_SET( MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED )
												PRINTLN("[JS] [TDFMARKER] - drawing over part: ", iPart)
												GB_DRAW_CRITICAL_PLAYER_MARKER(NETWORK_GET_PLAYER_INDEX(tempPart), DEFAULT, TRUE)
												iPlayersChecked++
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							iPlayersChecked++
						ENDIF
						IF iPlayersChecked >= MC_serverBD.iNumberOfPlayingPlayers[MC_playerBD[iPartToUse].iteam]
							BREAKLOOP
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciSYNCED_CAPTURE_SCORING)
		PROCESS_SYNCED_CAPTURE_SCORING(iLoc)
	ENDIF
	
	IF IS_THIS_A_VERSUS_MISSION() //new way of calculating capture location state also in main.
		CALCULATE_CAPTURE_LOCATION_STATE_AND_COLOUR(iLoc)
	ENDIF
ENDPROC

PROC PROCESS_LOCATION_EARLY_CLEANUP(INT iLoc)
	
	IF NOT SHOULD_LOCATION_CLEAN_UP_EARLY(iLoc)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD_4.iLocationCleanedUpEarlyBitset, iLoc)
		EXIT
	ENDIF
	
	INT iTeamLoop			
	FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		MC_serverBD_4.iGotoLocationDataRule[iLoc][iTeamLoop] = FMMC_OBJECTIVE_LOGIC_NONE
	ENDFOR
	
	SET_BIT(MC_serverBD_4.iLocationCleanedUpEarlyBitset, iLoc)
	PRINTLN("PROCESS_LOCATION_EARLY_CLEANUP - Location ", iLoc, " has been cleaned up early")
ENDPROC

PROC SERVER_LOCATION_EVERY_FRAME_PROCESSING(INT iLoc)
	
	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF NOT IS_LOCATION_CURRENT_OBJECTIVE_FOR_ANY_TEAM(iLoc)
		PROCESS_LOCATION_EARLY_CLEANUP(iLoc)
		EXIT
	ENDIF

	INT iTeamLoop			
	
	FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF MC_serverBD_4.iGotoLocationDataRule[iLoc][iTeamLoop] = FMMC_OBJECTIVE_LOGIC_GO_TO
			IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeamLoop] < FMMC_MAX_RULES
				IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeamLoop] = MC_serverBD_4.iCurrentHighestPriority[iTeamLoop]
					IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].mnVehicleNeeded[iTeamLoop] != DUMMY_MODEL_FOR_SCRIPT
					AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_SpecificVehModelFullBlocker)
						iTempPriorityLocation[iTeamLoop] = iLoc
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF MC_serverBD_4.iGotoLocationDataRule[iLoc][iTeamLoop] = FMMC_OBJECTIVE_LOGIC_CAPTURE
			IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeamLoop] < FMMC_MAX_RULES
				IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeamLoop] = MC_serverBD_4.iCurrentHighestPriority[iTeamLoop]
					iTempCaptureLocation = iLoc
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

ENDPROC

FUNC BOOL SHOULD_PROCESS_LOCATIONS_THIS_FRAME()
	
	IF g_bMissionOver
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Location Every Frame Processing
// ##### Description: Client and Server Location Every Frame Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
PROC PROCESS_LOCATES_PRE_EVERY_FRAME()

	iTempLeaveLoc = -1
	CLEAR_BIT(iLocalBoolCheck32, LBOOL32_IN_INVERSE_LEAVE_AREA_LOCATION)
	
	MC_playerBD[iLocalPart].iLocationInSecondaryLocation = 0
	
ENDPROC

PROC PROCESS_LOCATES_POST_EVERY_FRAME()

	IF MC_playerBD[iLocalPart].iLeaveLoc != iTempLeaveLoc
		PRINTLN("[LEAVEAREA] PROCESS_LOCATES_POST_EVERY_FRAME - Updating iLeaveLoc from: ", MC_playerBD[iLocalPart].iLeaveLoc, " to: ", iTempLeaveLoc)
		MC_playerBD[iLocalPart].iLeaveLoc = iTempLeaveLoc
		
		REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_LOCATION, FALSE #IF IS_DEBUG_BUILD , "PROCESS_LOCATES_POST_EVERY_FRAME - iLeaveLoc changed" #ENDIF )
	ENDIF
	
	IF iOldLoc != MC_playerBD[iPartToUse].iCurrentLoc
		iOldLoc = MC_playerBD[iPartToUse].iCurrentLoc
		REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_LOCATION, FALSE #IF IS_DEBUG_BUILD , "PROCESS_LOCATES_POST_EVERY_FRAME - iCurrentLoc changed" #ENDIF )
	ENDIF
	
ENDPROC

PROC PROCESS_LOCATION_EVERY_FRAME_CLIENT(INT iLoc)
			
	IF NOT IS_LOCATION_CURRENT_OBJECTIVE_FOR_ANY_TEAM(iLoc)
		CLEANUP_NON_PROCESSED_LOCATIONS(iLoc)
		EXIT
	ENDIF		
				
	START_PROCESSING_LOCATION(iLoc)
	
	PROCESS_WARP_ON_RULE_DATA_FOR_ENTITY(CREATION_TYPE_GOTO_LOC, iLoc, iGoToLocationShouldWarpThisRuleStart, iGoToLocationWarpedOnThisRule)
		
	PROCESS_LOCATION_ON_RULE_WARP(iLoc)
	
	PROCESS_LOCATION_ON_DAMAGE_WARP(iLoc)
	
	IF SHOULD_PROCESS_LOCATION(iLoc)
		CLIENT_LOCATION_EVERY_FRAME_PROCESSING(iLoc)
	ELSE
		CLEANUP_NON_PROCESSED_LOCATIONS(iLoc)
	ENDIF
	
	PROCESS_LOCATION_HUD_AND_BLIPS(iLoc)
		
ENDPROC	

PROC PROCESS_LOCATION_EVERY_FRAME_SERVER(INT iLoc)
	
	SERVER_LOCATION_EVERY_FRAME_PROCESSING(iLoc)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Location Staggered Processing
// ##### Description: Client and Server Location Staggered Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
				
PROC PROCESS_LOCATION_STAGGERED_CLIENT(INT iLoc)
	
	IF NOT IS_LOCATION_CURRENT_OBJECTIVE_FOR_ANY_TEAM(iLoc)
		EXIT
	ENDIF
	
	INT iPlayerTeam = MC_playerBD[iPartToUse].iTeam
		
	IF NOT IS_OBJECTIVE_BLOCKED()
		IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iPlayerTeam] <= MC_serverBD_4.iCurrentHighestPriority[iPlayerTeam]
		AND MC_serverBD_4.iGotoLocationDataPriority[iLoc][iPlayerTeam] < FMMC_MAX_RULES
			IF bLocalPlayerPedOK
				IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
					IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocDisableVehSpecWeapRadius > 0
						VECTOR vPlayerPos = GET_ENTITY_COORDS(localPlayerPed)
					
						IF VDIST2(vPlayerPos, GET_LOCATION_VECTOR(iLoc)) > POW(TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocDisableVehSpecWeapRadius), 2)
							PRINTLN("[LM][LOCAL_PROCESS_LOCATION_OBJECTIVES] - Disabling Special Vehicle Attacks/Abilities We are within Loc Radius: ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocDisableVehSpecWeapRadius)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SPECIAL, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_JUMP, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CAR_JUMP, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			PROCESS_STAGGERED_LOCATION_BLIP(iLoc)
		ENDIF
	ENDIF
ENDPROC	

PROC PROCESS_LOCATION_PRE_STAGGERED_SERVER()
	
	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	
	INT iTeam
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		CLEAR_BIT(iLocalBoolCheck6, LBOOL6_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iTeam)
	ENDFOR
	
ENDPROC	
PROC PROCESS_LOCATION_POST_STAGGERED_SERVER()
	
	INT iTeam
	
	IF !bIsLocalPlayerHost
		
		//Prepare for potential host migration
		FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams -1
			IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iTeam)
				SET_BIT(iLocalBoolCheck6, LBOOL6_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iTeam)
			ELSE
				CLEAR_BIT(iLocalBoolCheck6, LBOOL6_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iTeam)
			ENDIF
		ENDFOR
	
		EXIT
		
	ENDIF
	
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		IF IS_BIT_SET(iLocalBoolCheck6,LBOOL6_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iTeam)
			PRINTLN("[LOCATIONS] PROCESS_LOCATION_POST_STAGGERED_SERVER - Setting bit MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_",iTeam,"_NEEDS_TO_WAIT_FOR_LEAVE_LOC")
			SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iTeam)
		ELSE
			PRINTLN("[LOCATIONS] PROCESS_LOCATION_POST_STAGGERED_SERVER - Clearing bit MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_",iTeam,"_NEEDS_TO_WAIT_FOR_LEAVE_LOC")
			CLEAR_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iTeam)
		ENDIF
	ENDFOR
	
ENDPROC	

PROC PROCESS_LOCATION_CAPTURE_SERVER_HOLD_TIME(INT iLoc, INT iTeam, BOOL &bWasObjective[], INT &iPriority[])
	
	IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAreaRetained, MC_ServerBD_4.iCurrentHighestPriority[iTeam])
	OR NOT IS_AREA_ABLE_TO_BE_CAPTURED(iTeam, iLoc, TRUE)
		EXIT
	ENDIF
	
	IF MC_serverBD.iLocOwner[iLoc] != iTeam
		IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iHoldTimer[iLoc][iTeam])
			MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iHoldTimer[iLoc][iTeam])
		ENDIF
		
		EXIT
	ENDIF
	
	IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iHoldTimer[iLoc][iTeam])
		MC_START_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iHoldTimer[iLoc][iTeam])
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciSYNCED_CAPTURE_SCORING)
			IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdCapturePointsTimer)
				START_NET_TIMER(MC_serverBD.tdCapturePointsTimer)
				SET_BIT(MC_serverBD.iServerBitset6, SBBOOL6_USING_SYNCED_SCORE_TIMER)
			ENDIF
		ENDIF
		
		EXIT
		
	ENDIF
	
	IF MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD.iHoldTimer[iLoc][iTeam]) <= GET_TAKEOVER_TIME_FOR_CAPTURE_LOCATION(iLoc, iTeam)
		
		EXIT
		
	ENDIF
	
	INT iTeamRepeat
		
	BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_HELD_AREA, 0, iTeam, -1, INVALID_PLAYER_INDEX(), ci_TARGET_LOCATION, iLoc)
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitset6, SBBOOL6_USING_SYNCED_SCORE_TIMER)
		INCREMENT_SERVER_TEAM_SCORE(iTeam, MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam], GET_FMMC_POINTS_FOR_TEAM(iTeam, MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam]))
		PRINTLN("Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE_SERVER_HOLD_TIME - Increment score call")
	ENDIF
	
	IF MC_serverBD.iRuleReCapture[iTeam][MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam]] <= 0
		FOR iTeamRepeat = 0 TO (MC_serverBD.iNumberOfTeams-1)
		
			iPriority[iTeamRepeat] = MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeamRepeat] 
			
			MC_serverBD_1.inumberOfTeamInArea[iLoc][iTeamRepeat] = 0
			RESET_NET_TIMER(tdtimesincelastupdate[iLoc][iTeamRepeat])
			MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iAreaTimer[iLoc][iTeamRepeat])
			CLEAR_BIT(MC_serverBD.iAreaTimerStartedBS[iTeamRepeat], iLoc)
			MC_serverBD_1.iNumTeamControlKills[iLoc][iTeamRepeat] = 0
			MC_serverBD_1.iOldTeamControlkills[iLoc][iTeamRepeat] = 0
			
			IF iTeamRepeat = iTeam
				IF IS_BIT_SET(MC_serverBD.iLocteamFailBitset[iLoc], iTeam)
					CLEAR_BIT(MC_serverBD.iLocteamFailBitset[iLoc], iTeam)
				ENDIF
			ELSE
				IF DOES_TEAM_LIKE_TEAM(iTeamRepeat, iTeam)
					IF IS_BIT_SET(MC_serverBD.iLocteamFailBitset[iLoc], iTeamRepeat)
						CLEAR_BIT(MC_serverBD.iLocteamFailBitset[iLoc], iTeamRepeat)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		FOR iTeamRepeat = 0 TO (MC_serverBD.iNumberOfTeams-1)
			IF iTeamRepeat != iTeam
				IF IS_BIT_SET(MC_serverBD.iLocteamFailBitset[iLoc], iTeamRepeat)
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamRepeat].iTeamFailBitset, ipriority[iTeamRepeat])														
						REQUEST_SET_TEAM_FAILED(iTeam, mFail_LOC_CAPTURED)
						PRINTLN("Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE_SERVER_HOLD_TIME - MISSION FAILED FOR TEAM: ", iTeam, " because loc ", iLoc, " was captured")
					ENDIF
					bWasObjective[iTeamRepeat] = TRUE
					CLEAR_BIT(MC_serverBD.iLocteamFailBitset[iLoc], iTeamRepeat)
				ENDIF
				IF DOES_TEAM_LIKE_TEAM(iTeamRepeat, iTeam)
					SET_TEAM_OBJECTIVE_OUTCOME(iTeamRepeat, ipriority[iTeamRepeat], TRUE)
					PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION, iLoc, iTeamRepeat, TRUE)
				ELSE
					SET_TEAM_OBJECTIVE_OUTCOME(iTeamRepeat, ipriority[iTeamRepeat], FALSE)
					PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION, iLoc, iTeamRepeat, FALSE)
				ENDIF
			ELSE
				SET_TEAM_OBJECTIVE_OUTCOME(iTeamRepeat, ipriority[iTeamRepeat], TRUE)
				PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION, iLoc, iTeamRepeat, TRUE)
			ENDIF
			MC_serverBD.iReasonForObjEnd[iTeamRepeat] = OBJ_END_REASON_LOC_CAPTURED

			MC_serverBD.iLocOwner[iLoc] = -1

			PRINTLN("Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE_SERVER_HOLD_TIME - Setting ignore capture loc for team: ", iTeamRepeat)
		ENDFOR
	ENDIF
	
	IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] < FMMC_MAX_RULES
		IF MC_serverBD.iRuleReCapture[iTeam][MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam]]!= UNLIMITED_CAPTURES_KILLS
			MC_serverBD.iRuleReCapture[iTeam][MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam]]--
		ENDIF
	ENDIF
	FOR iTeamRepeat = 0 TO (MC_serverBD.iNumberOfTeams-1)
		IF bWasObjective[iTeamRepeat]
			IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_LOCATION, iTeamRepeat, iPriority[iTeamRepeat])
				SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeamRepeat, iPriority[iTeamRepeat], MC_serverBD.iReasonForObjEnd[iTeamRepeat])
				BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeamRepeat], iTeamRepeat, iPriority[iTeamRepeat], DID_TEAM_PASS_OBJECTIVE(iTeamRepeat, iPriority[iTeamRepeat]))
			ENDIF
		ENDIF
	ENDFOR
	MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iHoldTimer[iLoc][iTeam])
	
ENDPROC

PROC PROCESS_LOCATION_CAPTURE_SERVER_COMPLETED(INT iLoc, INT iTeam, BOOL &bWasObjective[], INT &iPriority[])
	
	BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_CON_AREA,0,iTeam,-1,INVALID_PLAYER_INDEX(),ci_TARGET_LOCATION,iLoc)
	INCREMENT_SERVER_TEAM_SCORE(iTeam, MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam], GET_FMMC_POINTS_FOR_TEAM(iTeam,MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam]))
	PRINTLN("[Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE_SERVER - PROCESS_LOCATION_CAPTURE_SERVER_COMPLETED - Increment score call")
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciCOUNT_PERSONAL_LOCATE_SCORE)
		IF MC_serverBD_3.playerLocOwner[iLoc][iTeam] = INVALID_PLAYER_INDEX()
			PLAYER_INDEX tempPlayer = GET_PLAYER_IN_CAPTURE_AREA(iLoc, FALSE)
			IF tempPlayer != INVALID_PLAYER_INDEX()
				PRINTLN("[Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE_SERVER - PROCESS_LOCATION_CAPTURE_SERVER_COMPLETED - ciCOUNT_PERSONAL_LOCATE_SCORE - Grabbing a backup player for scores as one wasn't set.")
				MC_serverBD_3.playerLocOwner[iLoc][iTeam] = tempPlayer
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAreaRetained, MC_ServerBD_4.iCurrentHighestPriority[iTeam])
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciCHANGE_BLIP_COLOUR_TO_CONTROLLING_TEAM)
			PRINTLN("[Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE_SERVER - PROCESS_LOCATION_CAPTURE_SERVER_COMPLETED - Location ", iLoc, " is now held by team ", iTeam)
			MC_serverBD.iLocOwner[iLoc] = iTeam
		ELSE
			MC_serverBD.iLocOwner[iLoc] = -1
		ENDIF
	ENDIF
	
	INT iTeamRepeat
	
	FOR iTeamRepeat = 0 TO (MC_serverBD.iNumberOfTeams-1)
		iPriority[iTeamRepeat] = MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeamRepeat] 
		SERVER_RESET_CAPTURE_LOCATION_DATA(iLoc, iTeamRepeat, iTeam, iPriority[iTeamRepeat], bwasobjective[iTeamRepeat])
	ENDFOR
	
	FOR iTeamRepeat = 0 TO (MC_serverBD.iNumberOfTeams-1)
		IF ipriority[iTeamRepeat] < FMMC_MAX_RULES
			IF MC_serverBD.iRuleReCapture[iTeamRepeat][MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeamRepeat]]<= 0
				IF iTeamRepeat != iTeam
					IF IS_BIT_SET(MC_serverBD.iLocteamFailBitset[iLoc], iTeamRepeat)
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamRepeat].iTeamFailBitset, ipriority[iTeamRepeat])
							REQUEST_SET_TEAM_FAILED(iTeam, mFail_LOC_CAPTURED)
							PRINTLN("[Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE_SERVER - PROCESS_LOCATION_CAPTURE_SERVER_COMPLETED - MISSION FAILED FOR TEAM: ", iTeam, " because loc ", iLoc, " was captured")
						ENDIF
						bwasobjective[iTeamRepeat] = TRUE
						CLEAR_BIT(MC_serverBD.iLocteamFailBitset[iLoc], iTeamRepeat)
					ENDIF
					IF DOES_TEAM_LIKE_TEAM(iTeamRepeat, iTeam)
						SET_TEAM_OBJECTIVE_OUTCOME(iTeamRepeat,ipriority[iTeamRepeat],TRUE)
						PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION,iLoc,iTeamRepeat,TRUE)
					ELSE
						SET_TEAM_OBJECTIVE_OUTCOME(iTeamRepeat,ipriority[iTeamRepeat],FALSE)
						PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION,iLoc,iTeamRepeat,FALSE)
					ENDIF
				ELSE
					SET_TEAM_OBJECTIVE_OUTCOME(iTeamRepeat,ipriority[iTeamRepeat],TRUE)
					PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION,iLoc,iTeamRepeat,TRUE)
				ENDIF
				MC_serverBD.iReasonForObjEnd[iTeamRepeat] = OBJ_END_REASON_LOC_CAPTURED
				PRINTLN("[Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE_SERVER - PROCESS_LOCATION_CAPTURE_SERVER_COMPLETED - server processing JOB_COMP_CONTROL_AREA: ", iLoc, " for Team: ", iTeamRepeat)
			ENDIF
		ENDIF
	ENDFOR
	IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] < FMMC_MAX_RULES
		IF MC_serverBD.iRuleReCapture[iTeam][MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam]]!= UNLIMITED_CAPTURES_KILLS
			MC_serverBD.iRuleReCapture[iTeam][MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam]]--
		ENDIF
	ENDIF
	FOR iTeamRepeat = 0 TO (MC_serverBD.iNumberOfTeams-1)
		IF bwasobjective[iTeamRepeat]
			IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_LOCATION,iTeamRepeat,ipriority[iTeamRepeat])
				SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeamRepeat,ipriority[iTeamRepeat],MC_serverBD.iReasonForObjEnd[iTeamRepeat])
				BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeamRepeat],iTeamRepeat,ipriority[iTeamRepeat],DID_TEAM_PASS_OBJECTIVE(iTeamRepeat,ipriority[iTeamRepeat]))
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_LOCATION_CAPTURE_SERVER_TIME(INT iLoc, INT iTeam, BOOL &bWasObjective[], INT &iPriority[])
	
	IF MC_serverBD_1.inumberOfTeamInArea[iLoc][iTeam] <= 0 
	OR NOT IS_AREA_ABLE_TO_BE_CAPTURED(iTeam, iLoc)
		IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciCHANGE_BLIP_COLOUR_TO_CONTROLLING_TEAM)
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAreaRetained, MC_ServerBD_4.iCurrentHighestPriority[iTeam])
				IF MC_serverBD.iLocOwner[iLoc] > -1
				AND MC_serverBD.iLocOwner[iLoc] < FMMC_MAX_TEAMS
					IF MC_serverBD_1.inumberOfTeamInArea[iLoc][MC_serverBD.iLocOwner[iLoc]] <= 0
						IF MC_serverBD.iLocOwner[iLoc] != -1
							MC_serverBD.iLocOwner[iLoc] = -1
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdtimesincelastupdate[iLoc][iTeam]) > 333
			IF HAS_NET_TIMER_STARTED(tdtimesincelastupdate[iLoc][iTeam])
				MC_serverBD.iAreaTimer[iLoc][iTeam] += GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdtimesincelastupdate[iLoc][iTeam])
				PRINTLN("[Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE_SERVER - MC_serverBD.iAreaTimer[iLoc][iTeam]: ", MC_serverBD.iAreaTimer[iLoc][iTeam])
			ENDIF
			REINIT_NET_TIMER(tdtimesincelastupdate[iLoc][iTeam])
		ENDIF
		
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iAreaTimerStartedBS[iTeam], iLoc)
		MC_REINIT_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iAreaTimer[iLoc][iTeam])
		SET_BIT(MC_serverBD.iAreaTimerStartedBS[iTeam], iLoc)
		PRINTLN("[Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE_SERVER - Starting timer ", iLoc, " team ", iTeam)
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciCOUNT_PERSONAL_LOCATE_SCORE)
			IF MC_serverBD_3.playerLocOwner[iLoc][iTeam] != INVALID_PLAYER_INDEX()
				MC_serverBD_3.playerLocOwner[iLoc][iTeam] = INVALID_PLAYER_INDEX()
				PRINTLN("[Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE_SERVER Clearing MC_serverBD_3.playerLocOwner[",iLoc,"][",iTeam,"] to INVALID_PLAYER_INDEX()")
			ENDIF
		ENDIF
		
		EXIT
	
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciCOUNT_PERSONAL_LOCATE_SCORE)
		IF GET_PLAYER_IN_CAPTURE_AREA(iLoc) != INVALID_PLAYER_INDEX()
		AND (MC_serverBD_3.playerLocOwner[iLoc][iTeam] = INVALID_PLAYER_INDEX() OR IS_PED_INJURED(GET_PLAYER_PED(MC_serverBD_3.playerLocOwner[iLoc][iTeam]))) 
			MC_serverBD_3.playerLocOwner[iLoc][iTeam] = GET_PLAYER_IN_CAPTURE_AREA(iLoc)
			PRINTLN("[Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE_SERVER - Setting MC_serverBD_3.playerLocOwner[",iLoc,"][",iTeam,"] to: ", NATIVE_TO_INT(MC_serverBD_3.playerLocOwner[iLoc][iTeam]))
		ENDIF
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(tdtimesincelastupdate[iLoc][iTeam])
		REINIT_NET_TIMER(tdtimesincelastupdate[iLoc][iTeam])
	ELIF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdtimesincelastupdate[iLoc][iTeam]) > 333
		
		IF MC_serverBD_1.inumberOfTeamInArea[iLoc][iTeam] > 1
			
			FLOAT fControlMulti = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iCaptureMultiplier[iTeam]) / 100.0
			FLOAT fTimeMultiplier = (MC_serverBD_1.inumberOfTeamInArea[iLoc][iTeam]-1) * fControlMulti
			MC_serverBD.iAreaTimer[iLoc][iTeam] += ROUND(-1 * fTimeMultiplier * GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdtimesincelastupdate[iLoc][iTeam]))
			
		ENDIF
		
		REINIT_NET_TIMER(tdtimesincelastupdate[iLoc][iTeam])
		
		IF MC_serverBD_1.iNumTeamControlKills[iLoc][iTeam] > MC_serverBD_1.iOldTeamControlkills[iLoc][iTeam]
			MC_serverBD.iAreaTimer[iLoc][iTeam] += (-1 * (MC_serverBD_1.iNumTeamControlKills[iLoc][iTeam] - MC_serverBD_1.iOldTeamControlkills[iLoc][iTeam]) * 2 * 1000)
			PRINTLN("[Loc ",iLoc,"] PROCESS_LOCATION_CAPTURE_SERVER - Setting kill time adjust for Area ", iLoc, " team ", iTeam, " seconds gained: ", (MC_serverBD_1.iNumTeamControlKills[iLoc][iTeam] - MC_serverBD_1.iOldTeamControlkills[iLoc][iTeam]))
			MC_serverBD_1.iOldTeamControlkills[iLoc][iTeam] = MC_serverBD_1.iNumTeamControlKills[iLoc][iTeam]
		ENDIF
		
		IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] < FMMC_MAX_RULES
			IF MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD.iAreaTimer[iLoc][iTeam]) > GET_TAKEOVER_TIME_FOR_CAPTURE_LOCATION(iLoc, iTeam)
				PROCESS_LOCATION_CAPTURE_SERVER_COMPLETED(iLoc, iTeam, bWasObjective, iPriority)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_LOCATION_CAPTURE_SERVER(INT iLoc, INT iTeam, BOOL &bWasObjective[], INT &iPriority[])
	
	PROCESS_LOCATION_CAPTURE_SERVER_HOLD_TIME(iLoc, iTeam, bWasObjective, iPriority)
	
	PROCESS_LOCATION_CAPTURE_SERVER_TIME(iLoc, iTeam, bWasObjective, iPriority)
	
ENDPROC

PROC PROCESS_LOCATION_STAGGERED_SERVER(INT iLoc)
	
	IF NOT IS_LOCATION_CURRENT_OBJECTIVE_FOR_ANY_TEAM(iLoc)
		EXIT
	ENDIF
	
	INT iTeam
	INT iPriority[FMMC_MAX_TEAMS]
	BOOL bWasObjective[FMMC_MAX_TEAMS]

	IF NOT bIsLocalPlayerHost
		FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
			IF HAS_NET_TIMER_STARTED(tdtimesincelastupdate[iLoc][iteam])
				RESET_NET_TIMER(tdtimesincelastupdate[iLoc][iteam])
			ENDIF
		ENDFOR
		EXIT
	ENDIF
	
	IF (IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN))
	AND ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		EXIT
	ENDIF
	
	FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		
		IF (NOT HAS_TEAM_FAILED(iTeam))
		AND (NOT HAS_TEAM_FINISHED(iTeam))
			
			IF NOT IS_LOCATION_READY_FOR_PROCESSING(iLoc)
				EXIT
			ENDIF
			
			IF MC_serverBD_4.iGotoLocationDataRule[iLoc][iteam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
			
				PROCESS_LOCATION_CAPTURE_SERVER(iLoc, iTeam, bWasObjective, iPriority)
				
			ELIF MC_serverBD_4.iGotoLocationDataRule[iLoc][iteam] = FMMC_OBJECTIVE_LOGIC_LEAVE_LOCATION

				PRINTLN("[LeaveArea] PROCESS_LOCATION_STAGGERED_SERVER - LEAVE LOCATION - Hit MCb_serverBD_4.iGotoLocationDataPriority[iLoc][iteam] = MC_serverBD_4.iCurrentHighestPriority[iteam] for iteam = ", iteam, " and iLoc = ", iLoc)
				
				IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iteam] = MC_serverBD_4.iCurrentHighestPriority[iteam]
				AND IS_LOCATION_READY_FOR_PROCESSING(iLoc)
					PRINTLN("[LeaveArea] PROCESS_LOCATION_STAGGERED_SERVER - LEAVE LOCATION - location ", iLoc, " is ready for processing")
				
					IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_NEW_PARTICIPANT_LOOP_FINISHED)
						
						INT iPlayersInLoc = GET_PLAYERS_IN_LEAVE_LOCATION(iLoc, iTeam) // Number of players currently in the location
						INT iPlayersNeeded = GET_PLAYERS_REQUIRED_TO_LEAVE_LOCATION(iLoc, iTeam) // Number of players (or less) there needs to be to pass the rule

						PRINTLN("[LeaveArea] about to check if (iLocalBoolCheck6,LBOOL6_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iteam should be set for iteam = ", iteam)
						PRINTLN("[LeaveArea] about to check if (iLocalBoolCheck6,LBOOL6_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iteam should be set iPlayersInLoc = ", iPlayersInLoc)
						PRINTLN("[LeaveArea] about to check if (iLocalBoolCheck6,LBOOL6_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iteam should be set iPlayersNeeded = ", iPlayersNeeded)								
						PRINTLN("[LeaveArea] about to check if (iLocalBoolCheck6,LBOOL6_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iteam should be set g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iteam] = ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iteam])
						
						IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iteam] != ciGOTO_LOCATION_INDIVIDUAL
						AND iPlayersInLoc > iPlayersNeeded
							IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iteam)
								SET_BIT(iLocalBoolCheck6, LBOOL6_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iteam)
								PRINTLN("[LeaveArea] Setting (iLocalBoolCheck6,LBOOL6_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iteam for iteam = ", iteam)
							ENDIF
						ENDIF
						
						IF iPlayersInLoc != -1
						AND iPlayersNeeded != -1
							IF iPlayersInLoc <= iPlayersNeeded
								ipriority[iteam] = MC_serverBD_4.iGotoLocationDataPriority[iLoc][iteam] 
								IF ipriority[iteam] <= MC_serverBD_4.iCurrentHighestPriority[iteam]
									PRINTLN("[RCC MISSION][LeaveArea] - SERVER setting leave location complete for leave area w WholeTeamAtLocation ",g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iteam],", loc: ",iLoc," team:  ",iteam)
									
									MC_serverBD.iReasonForObjEnd[iteam] = OBJ_END_REASON_LVE_LOC
									
									BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_LVE_LOC,0,iteam,iLoc,INVALID_PLAYER_INDEX(),ci_TARGET_LOCATION,iLoc)

									INCREMENT_SERVER_TEAM_SCORE(iteam, ipriority[iteam], GET_FMMC_POINTS_FOR_TEAM(iteam,ipriority[iteam]))
									
									SET_TEAM_OBJECTIVE_OUTCOME(iteam,ipriority[iteam],TRUE)
									PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION,iLoc,iteam,TRUE)
									
									IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_LOCATION,iteam,ipriority[iteam])
										SET_LOCAL_OBJECTIVE_END_MESSAGE(iteam,ipriority[iteam],MC_serverBD.iReasonForObjEnd[iteam])
										BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iteam],iteam,ipriority[iteam],DID_TEAM_PASS_OBJECTIVE(iteam,ipriority[iteam]))
									ENDIF
									
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION][LeaveArea] - SERVER setting leave location complete for leave area but we havent left yet")
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Location Objective Processing
// ##### Description: Client and Server Location Objective Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
				
PROC PROCESS_LOCATION_OBJECTIVES_CLIENT(INT iLoc)
	
	IF IS_BLIP_SET_AS_HIDDEN(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct)
	AND NOT SHOULD_HINT_CAM_WORK_FOR_OBJECTIVE_ENTITY_WITHOUT_BLIP(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct)
		EXIT
	ENDIF
	
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	
	IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] > MC_serverBD_4.iCurrentHighestPriority[iTeam]
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	FLOAT fLocDist2 = GET_DIST2_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, GET_LOCATION_VECTOR(iLoc))
	IF fLocDist2 > fNearestTargetDist2Temp
		EXIT
	ENDIF
	
	IF NOT IS_PLAYER_IN_REQUIRED_LOCATION_VEHICLE(PlayerPedToUse, iTeam, iLoc)
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_SpecificVehModelFullBlocker)
		EXIT
	ENDIF

	iNearestTargetTemp = iLoc
	iNearestTargetTypeTemp = ci_TARGET_LOCATION
	fNearestTargetDist2Temp = fLocDist2
					
ENDPROC	

PROC PROCESS_LOCATION_OBJECTIVES_SERVER(INT iLoc, INT iTeam)
	
	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iGotoLocationDataRule[iLoc][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] > iCurrentHighPriority[iTeam]
		//Has an objective but the team has not reached it yet
		EXIT
	ENDIF

	PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_LOCATION_OBJECTIVES_SERVER - Loc: ", iLoc, " Team: ", iTeam, " Objective is valid - Assigning. Priority: ",
			MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam], " Rule: ", MC_serverBD_4.iGotoLocationDataRule[iLoc][iTeam])
				
	iCurrentHighPriority[iTeam] = MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam]
	iTempLocMissionLogic[iTeam] = MC_serverBD_4.iGotoLocationDataRule[iLoc][iTeam] 
	
	IF iOldHighPriority[iTeam] > iCurrentHighPriority[iTeam]
		iNumHighPriorityLoc[iTeam] = 0
		iOldHighPriority[iTeam] = iCurrentHighPriority[iTeam]
	ENDIF

	iNumHighPriorityLoc[iTeam]++
			
	RESET_TEMP_OBJECTIVE_VARS(iTeam, ciRULE_TYPE_GOTO)
			
ENDPROC
