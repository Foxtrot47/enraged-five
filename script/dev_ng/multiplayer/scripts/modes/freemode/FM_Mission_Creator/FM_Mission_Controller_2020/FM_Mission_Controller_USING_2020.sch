USING "globals.sch"
USING "net_spectator_cam.sch"
USING "net_save_create_eom_vehicle.sch"
USING "FMMC_next_job_screen.sch"
USING "net_celebration_screen.sch"
USING "heist_bags.sch"
USING "emergency_call.sch"
USING "net_private_yacht_private.sch"
USING "net_orbital_cannon.sch"
USING "net_collectables_pickups.sch"
USING "net_synced_ambient_pickups.sch"

//UI
USING "am_common_ui.sch"
USING "shared_hud_displays.sch"
USING "net_objective_text.sch"
USING "Thumbs.sch"

//Minigames
USING "net_beam_hack.sch"
USING "net_hotwire.sch"
USING "net_hacking_fingerprint_minigame.sch"
USING "net_hacking_order_unlock.sch"
USING "hacking_public.sch"
USING "net_safe_crack.sch"
USING "drill_minigame.sch"
USING "net_vaultDrill.sch"
USING "net_voltage_hack.sch"

USING "FMMC_Player_Inventory.sch"

//Island
USING "heist_island_loading.sch"
#IF FEATURE_HEIST_ISLAND
USING "net_island_backup_heli_launching.sch"
#ENDIF

#IF IS_DEBUG_BUILD
USING "net_vehicle_weapons.sch"
USING "FMMC_ContentOverviewDebug.sch"
#ENDIF


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Parameters
// ##### Description: Structs for caching frequently used parameters
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

STRUCT ELECTRONIC_PARAMS
	INT iIndex
	OBJECT_INDEX oiElectronic
	BLIP_INDEX biElectronicBlip
	FMMC_ELECTRONIC_ENTITY_TYPE eEntityType
	VECTOR vElectronicCoords
	
	//CCTV
	INT iCamIndex
	
ENDSTRUCT

ENUM INTERACT_WITH_CAMERA_STATE
	IW_CAM_NONE,
	IW_CAM_WAITING,
	IW_CAM_PROCESSING,
	IW_CAM_CLEAN_UP
ENDENUM

STRUCT INTERACT_WITH_CAMERA_VARS
	VECTOR vRot					//Initial rotation of the camera, gets offset by player heading
	VECTOR vOffsetCoords		//Position relative to the player
	FLOAT fPhaseEnd = 1.0		//When during the animation we want to clean up the camera
	FLOAT fFOV					//FOV for camera
	INT iDuration				//Animation duration, only needed for non sync scenes
	INT iLerpIn = 1000			//Lerp Time, set to 0 for a cut
	INT iLerpOut = 1000			//Lerp Time, set to 0 for a cut
	BOOL bIsAnimated = FALSE 	//Does this Interact-With use an animated camera
ENDSTRUCT

STRUCT INTERACT_WITH_ANIM_ENTITY_INFO
	TEXT_LABEL_63 sIW_Enter
	TEXT_LABEL_63 sIW_Looping
	TEXT_LABEL_63 sIW_Exit
	
	MODEL_NAMES mnIW_ModelName = DUMMY_MODEL_FOR_SCRIPT
	
	BOOL bPersistent // Set to TRUE for the Interact-With entity to have it stay animating after the Interact-With, or set to TRUE for spawned props to have them stick around after the Interact-With
	BOOL bHideDuringThisSubAnim
	BOOL bOptional
	BOOL bUsePlayerAnimPhase
	FLOAT fPropHideUntilPhase = -999.99 // Use this for props/the Interact-With entity to have them hidden until a particular phase
	FLOAT fPropHideAfterPhase = 999.99 // Use this for props/the Interact-With entity to have them hidden after a particular phase
ENDSTRUCT

CONST_INT ciINTERACT_WITH__MAX_SPAWNED_PROPS			5
CONST_INT ciINTERACT_WITH__MAX_PERSISTENT_PROPS			7
CONST_INT ciINTERACT_WITH__MAX_SPAWNED_PTFX				2
CONST_INT ciINTERACT_WITH__MAX_NEARBY_DYNOPROP_ANIMS	2
CONST_INT ciINTERACT_WITH__MAX_LINKED_OBJECTS			4

CONST_INT ciInteractWithAnimConfigBS_MoveToNextSubAnimAfterwards	0

CONST_FLOAT cfINTERACT_WITH__SPAWNED_PROP_UNDERGROUND_DISTANCE	100.0

STRUCT INTERACT_WITH_ANIM_ASSETS
	STRING sIW_AnimDict
	INTERACT_WITH_ANIM_ENTITY_INFO sPlayerAnims
	INTERACT_WITH_ANIM_ENTITY_INFO sPlayerFaceAnims
	
	INTERACT_WITH_ANIM_ENTITY_INFO sInteractWithEntityAnims

	INTERACT_WITH_ANIM_ENTITY_INFO sInteractWithLinkedVehicleAnims
	INTERACT_WITH_ANIM_ENTITY_INFO sInteractWithLinkedPedAnims
	INTERACT_WITH_ANIM_ENTITY_INFO sInteractWithLinkedDynopropAnims

	INTERACT_WITH_ANIM_ENTITY_INFO sNearbyDynopropAnims[ciINTERACT_WITH__MAX_NEARBY_DYNOPROP_ANIMS]
	INTERACT_WITH_ANIM_ENTITY_INFO sSpawnedPropAnims[ciINTERACT_WITH__MAX_SPAWNED_PROPS]
	INTERACT_WITH_ANIM_ENTITY_INFO sNearbyPedAnims
	INTERACT_WITH_ANIM_ENTITY_INFO sLinkedObjectAnims[ciINTERACT_WITH__MAX_LINKED_OBJECTS]
	
	INTERACT_WITH_ANIM_ENTITY_INFO sCameraAnims
	INTERACT_WITH_CAMERA_VARS sCameraVars
	
	STRING sInteractWithIncludedPTFXAsset
	
	INT iInteractWithAnimConfigBS
	
	// Cached
	INT iNumPropsToSpawn = 0
	INT iNumDynopropsToUse = 0
	INT iNumLinkedObjectsToUse = 0
ENDSTRUCT

CONST_INT ciINTERACT_WITH_PARAMS_BS__IsObjectiveMinigame								0
CONST_INT ciINTERACT_WITH_PARAMS_BS__IsInteractable										1
CONST_INT ciINTERACT_WITH_PARAMS_BS__IsBackgroundAnim									2
CONST_INT ciINTERACT_WITH_PARAMS_BS__OnlyReturnTrueAfterCleanup							3
CONST_INT ciINTERACT_WITH_PARAMS_BS__SpookNearbyPedsWhenUsed							4
CONST_INT ciINTERACT_WITH_PARAMS_BS__ChangeObjTextWhenWaitingForLobbyLeader				5
CONST_INT ciINTERACT_WITH_PARAMS_BS__EnableInteractWithEarlyEndForObjectiveMinigames	6
CONST_INT ciINTERACT_WITH_PARAMS_BS__LeadIntoCutscene									7
CONST_INT ciINTERACT_WITH_PARAMS_BS__LeadIntoCutsceneShouldWaitUntilLoop				8
CONST_INT ciINTERACT_WITH_PARAMS_BS__DontFinishAnimUntilConversationsEnd				9
CONST_INT ciINTERACT_WITH_PARAMS_BS__LeadOutOfCutscene									10
CONST_INT ciINTERACT_WITH_PARAMS_BS__DontGrabControlOfLinkedPed							11

CONST_INT ciINTERACT_WITH_PARAMS_RUNTIME_BS__InteractionSkipped		0
CONST_INT ciINTERACT_WITH_PARAMS_RUNTIME_BS__InteractionCancelled	1
CONST_INT ciINTERACT_WITH_PARAMS_RUNTIME_BS__StopLoopingASAP		2

FORWARD STRUCT INTERACT_WITH_PARAMS
TYPEDEF PROC PROCESS_INTERACT_WITH_ANIMATION_STATE_ON_STATE_CHANGE(INTERACT_WITH_PARAMS &sInteractWithParams)

STRUCT INTERACT_WITH_PARAMS

	// Base Parameters
	INT iInteractWith_AnimPreset = ciINTERACT_WITH_PRESET__NONE
	INT iInteractWith_AltAnimSet = ciIW_ALTANIMSET__DEFAULT
	OBJECT_INDEX oiInteractWithEntity
	INT iNetworkedObjectIndex
	
	INT iInteractWithParamsBS // For bits that will change the general functionality of the Interact-With
	INT iInteractWithParamsRuntimeBS // For passing in new stuff during the Interact-With such as "stop looping" or alternatively for receiving information back out (passed by reference) such as "interaction cancelled"
	
	INT iObjectiveObjectIndex = -1
	
	INT iInteractableIndex = -1
	INT iOngoingInteractionIndex = -1
	
	// Parameters when not using a Preset
	INTERACT_WITH_ANIM_ASSETS sCustomIWAnims
	ENTITY_INDEX eiIW_CustomAnim_AlignmentEntity
	
	// Extras
	INT iPaintingIndex = -1
	INT iClothesChangeOutfit = -1
	INT iKeypadAccessRequired = -1
	
	INT iAttachedVehicleIndex = -1
	INT iAttachedPedIndex = -1
	INT iAttachedDynopropIndex = -1
	INT iAttachedObjectIndex = -1
	
	INT iInteractWithEndConversationLine = -1 // When using ciINTERACT_WITH_PARAMS_BS__DontFinishAnimUntilConversationsEnd, this can be used to specify when we should consider the conversation to have ended
	
	PROCESS_INTERACT_WITH_ANIMATION_STATE_ON_STATE_CHANGE onStateChange
	
	INT iLinkedObjectIDs[ciINTERACT_WITH__MAX_LINKED_OBJECTS]
	INT iKeycardProp = INTERACT_WITH_KEYCARD_PROP__NONE
	INT iAngleGrinderProp = INTERACT_WITH_ANGLE_GRINDER_PROP__NONE
	
ENDSTRUCT

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entity State Structs
// ##### Description: Structs for caching frequently accessed native entity states
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT FMMC_STRUCT_TERTIARY_BOOL_NULL 	-1
CONST_INT FMMC_STRUCT_TERTIARY_BOOL_FALSE 	0
CONST_INT FMMC_STRUCT_TERTIARY_BOOL_TRUE 	1

STRUCT FMMC_PED_TARGET_DATA
	ENTITY_INDEX tempTarget
	ENTITY_INDEX temp_carrier
	PED_INDEX tempPedTarget
	VEHICLE_INDEX targetVeh
	BOOL bExists
	BOOL bTargetOK
	BOOL bPedTargetOK
	VECTOR vTempTargetCoords
	FLOAT fTargetSpeed
	FLOAT fTargetDist
ENDSTRUCT

STRUCT FMMC_PED_VEHICLE_DATA
	PED_INDEX driverPed
	BOOL bInVeh
	BOOL bVehOk
	BOOL bHeli
	BOOL bPlane
	BOOL bBoat
	BOOL bCombatVeh
	BOOL bDriverPed
	BOOL bDriver
	BOOL bDriverOk
	FLOAT fSpeed
ENDSTRUCT

//FMMC2020 - MOVE THIS BACK!
CONST_INT FMMC_PED_STATE_IN_COMBAT			0

STRUCT FMMC_PED_STATE
	BOOL bExists
	BOOL bHasControl
	BOOL bInjured
	BOOL bIsInAnyVehicle
	BOOL bEnteringAnyVehicle
	BOOL bIsPlacedPed	
	BOOL bIsOnPlacedTrain
	
	INT iBitset = 0
	INT iIndex = -1
	PED_INDEX pedIndex
	NETWORK_INDEX niIndex
	VEHICLE_INDEX vehIndexPedIsIn
	VECTOR ___vCoords //GET_FMMC_PED_COORDS
	INTERIOR_INSTANCE_INDEX ___iiiCurrentInterior = NULL //GET_FMMC_PED_INTERIOR
	MODEL_NAMES mn
	INT iGangChaseUnit = -1	
	
	FLOAT fDistFromLocalPlayerPed
	
	STEALTH_AND_AGGRO_SETTINGS sStealthAndAggroSystemPed
ENDSTRUCT

STRUCT FMMC_VEHICLE_STATE
	BOOL bExists
	BOOL bDrivable
	BOOL bHasControl
	BOOL bAlive
	INT ___iTB_LocalPlayerInVehicle = FMMC_STRUCT_TERTIARY_BOOL_NULL // IS_LOCAL_PLAYER_IN_FMMC_VEHICLE
	INT ___iTB_LocalPlayerInVehicleEntering = FMMC_STRUCT_TERTIARY_BOOL_NULL // IS_LOCAL_PLAYER_IN_FMMC_VEHICLE
	INT ___iTB_VehicleEmpty = FMMC_STRUCT_TERTIARY_BOOL_NULL // IS_FMMC_VEHICLE_EMPTY
	INT ___iTB_DriverSeatEmpty = FMMC_STRUCT_TERTIARY_BOOL_NULL // IS_FMMC_VEHICLE_DRIVERS_SEAT_FREE
	INT ___iTB_TrailerStuck = FMMC_STRUCT_TERTIARY_BOOL_NULL // IS_FMMC_VEHICLE_TRAILER_STUCK
	
	BOOL bIsOnPlacedTrain
	
//	INT iBitset = 0
	INT iIndex = -1
	FLOAT ___fBodyHealth // GET_FMMC_VEHICLE_CURRENT_BODY_HEALTH
	VEHICLE_INDEX vehIndex
	NETWORK_INDEX niIndex
	VECTOR ___vCoords // GET_FMMC_VEHICLE_COORDS
	INTERIOR_INSTANCE_INDEX ___iiiCurrentInterior = NULL //GET_FMMC_VEHICLE_INTERIOR
	FLOAT ___fHeading = -1.0 // GET_FMMC_VEHICLE_HEADING
	MODEL_NAMES mn
ENDSTRUCT

ENUM FMMC_INTERACTABLE_EVENT_TYPE
	FMMC_INTERACTABLE_EVENT_TYPE__PLAYER_STARTED_INTERACTING,
	FMMC_INTERACTABLE_EVENT_TYPE__PLAYER_STOPPED_INTERACTING,
	FMMC_INTERACTABLE_EVENT_TYPE__INTERACTION_COMPLETE,
	FMMC_INTERACTABLE_EVENT_TYPE__INTERACTION_NO_LONGER_COMPLETE,
	FMMC_INTERACTABLE_EVENT_TYPE__RESET_INTERACTABLE,
	FMMC_INTERACTABLE_EVENT_TYPE__INTERACTION_COMPLETED_ON_PREVIOUS_MISSION,
	
	// Interaction-Specific
	FMMC_INTERACTABLE_EVENT_TYPE__SYNCLOCK_ENGAGED,
	FMMC_INTERACTABLE_EVENT_TYPE__SYNCLOCK_DISENGAGED
	
ENDENUM

CONST_INT FMMC_INTERACTABLE_STATE_OCCUPIED				0

STRUCT FMMC_INTERACTABLE_STATE
	INT iBitset = 0
	INT iIndex = -1
	OBJECT_INDEX objIndex
	NETWORK_INDEX niIndex
	
	BOOL bInteractableExists
	BOOL bInteractableAlive
	BOOL bHaveControlOfInteractable
	
	FLOAT fDistanceToInteractable
	VECTOR vInteractionCoords
	FLOAT fInteractionHeading = 999.9
	
	INT iOngoingInteractionIndex = -1
	BOOL bIsBackgroundInteraction
	
	INT iInteractableHackingStructIndex = -1
	
	VECTOR ___vCoords // GET_FMMC_INTERACTABLE_COORDS
	MODEL_NAMES mn
ENDSTRUCT

CONST_INT FMMC_OBJECT_STATE_CARRIED_BY_SOMEONE				0
CONST_INT FMMC_OBJECT_STATE_CARRIED_BY_LOCAL_PLAYER			1
CONST_INT FMMC_OBJECT_STATE_CARRIED_BY_SOMEONE_ELSE			2
CONST_INT FMMC_OBJECT_STATE_ATTACHED_TO_SOMETHING			3

STRUCT FMMC_OBJECT_STATE
	INT iBitset = 0
	INT iIndex = -1
	OBJECT_INDEX objIndex
	NETWORK_INDEX niIndex
	
	BOOL bObjExists
	BOOL bObjAlive
	BOOL bObjBroken
	BOOL bHaveControlOfObj	
	
	VECTOR ___vCoords // GET_FMMC_OBJECT_COORDS
	INTERIOR_INSTANCE_INDEX ___iiiCurrentInterior = NULL //GET_FMMC_OBJECT_INTERIOR
	MODEL_NAMES mn
	
	INT iObjHackingStructIndex = -1
	
	ELECTRONIC_PARAMS sObjElectronicParams
ENDSTRUCT

STRUCT FMMC_DYNOPROP_STATE
	INT iIndex = -1
	OBJECT_INDEX objIndex
	NETWORK_INDEX niIndex
	
	BOOL bDynopropExists
	BOOL bDynopropAlive
	BOOL bHaveControlOfDynoprop
	
	VECTOR ___vCoords // GET_FMMC_DYNOPROP_COORDS
	MODEL_NAMES mn
	
	ELECTRONIC_PARAMS sDynopropElectronicParams
	//INT iBitset = 0		// Re-add if we require something that isn't suitable for bools
ENDSTRUCT

STRUCT FMMC_PLAYER_STATE
	INT iIndex = -1
	BOOL bParticipantActive // = NETWORK_IS_PARTICIPANT_ACTIVE
	BOOL bPlayerActive // = NETWORK_IS_PLAYER_ACTIVE
	BOOL bPlayerOK // = IS_NET_PLAYER_OK - BOOL bCheckPlayerAlive = TRUE
	BOOL bPedAlive // = NOT IS_PED_INJURED
	BOOL bSCTV // = IS_PLAYER_SCTV
	PARTICIPANT_INDEX piPartIndex // = INT_TO_PARTICIPANTINDEX
	PLAYER_INDEX piPlayerIndex // = NETWORK_GET_PLAYER_INDEX
	PED_INDEX piPedIndex // = GET_PLAYER_PED
	VECTOR ___vCoords // Call GET_FMMC_PLAYER_COORDS
	//INT iBitset = 0 - Re-add if we require something that isn't suitable for bools.
ENDSTRUCT

STRUCT FMMC_PICKUP_STATE
	INT iIndex = -1
	PICKUP_INDEX puiIndex
	OBJECT_INDEX ___oiPickupObject 		// GET_FMMC_PICKUP_OBJECT
	
	BOOL bPickupExists
	BOOL ___bPickupCollected 			// HAS_FMMC_PICKUP_BEEN_COLLECTED
	BOOL ___bPickupObjectExist 			// DOES_FMMC_PICKUP_OBJECT_EXIST (can give false positives, as it is based on how the object is processed by distance)
	BOOL ___bPickupObjectEntityExist 	// DOES_FMMC_PICKUP_OBJECT_ENTITY_EXIST (Checks if the object entity exists using does_entity_exist)
	
	PICKUP_TYPE ptType
ENDSTRUCT

STRUCT FMMC_TRAIN_STATE
	INT iIndex = -1
	BOOL bTrainExists
	BOOL bTrainAlive
	BOOL ___bTrainEmpty // IS_FMMC_TRAIN_EMPTY
	BOOL bHaveControlOfTrain
	VEHICLE_INDEX viTrainIndex
	NETWORK_INDEX niIndex
	VECTOR ___vCoords // GET_FMMC_TRAIN_COORDS
ENDSTRUCT

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Pausable Timers
// ##### Description: MC Pausable TImers Struct
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
STRUCT MC_PAUSABLE_TIMER
	INT iTimeStamp = 0
	INT iOffset = 0
ENDSTRUCT

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Telemetry
// ##### Description: Consts and Variables fro telemetry - can be mission specific
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//Island Heist - Entrances
CONST_INT ciIslandHeistTelemetry_CompoundEntrances_EXPLOSIVES_FRONT_GATE	0
CONST_INT ciIslandHeistTelemetry_CompoundEntrances_KEYCARD_SOUTH				1
CONST_INT ciIslandHeistTelemetry_CompoundEntrances_KEYCARD_NORTH				2
CONST_INT ciIslandHeistTelemetry_CompoundEntrances_RAPPEL_SOUTH				3
CONST_INT ciIslandHeistTelemetry_CompoundEntrances_RAPPEL_NORTH				4
CONST_INT ciIslandHeistTelemetry_CompoundEntrances_VEHICLE_FRONT_GATE		5
CONST_INT ciIslandHeistTelemetry_CompoundEntrances_SEWER_GRATE				6
																			
//Island Heist - Exits														
CONST_INT ciIslandHeistTelemetry_CompoundExits_FRONT_GATE					0
CONST_INT ciIslandHeistTelemetry_CompoundExits_KEYCARD_SOUTH					1
CONST_INT ciIslandHeistTelemetry_CompoundExits_KEYCARD_NORTH					2
CONST_INT ciIslandHeistTelemetry_CompoundExits_RAPPEL_SOUTH					3
CONST_INT ciIslandHeistTelemetry_CompoundExits_RAPPEL_NORTH					4

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Function Callbacks temp
// ##### Description: 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//FMMC2020 - MOVE THIS BACK!
CONST_INT ciMAX_PED_SPOOK_COORDS						5
CONST_INT ciSpookCoordID_GENERIC						0
CONST_INT ciSpookCoordID_THERMITE_PLACED				1
CONST_INT ciSpookCoordID_OBJECT_INTERACT_WITH			2
CONST_INT ciSpookCoordID_OBJECT_MULTI_SOLUTION_LOCK		3

STRUCT PED_SPOOK_AT_COORD_STRUCT
	SCRIPT_TIMER tdPedSpookAtCoordTimer
	VECTOR vPedSpookCoord
	FLOAT fPedSpookRadius
	ENTITY_INDEX entPedSpookTarget
	INT iPedSpookCoordID
	BOOL bPedNeedsLOS = TRUE
ENDSTRUCT
PED_SPOOK_AT_COORD_STRUCT sPedSpookAtCoord[ciMAX_PED_SPOOK_COORDS]

//FMMC2020 - AND THIS
ENUM eLocateFadeOutState
	eLocateFadeOutState_BEFORE_FADE_IN,
	eLocateFadeOutState_FADING_IN,
	eLocateFadeOutState_DISPLAYING,
	eLocateFadeOutState_FADING_OUT,
	eLocateFadeOutState_DO_DISTANCE_FADING,
	eLocateFadeOutState_AFTER_FADE_OUT
ENDENUM

STRUCT locate_fade_out_data
	INT iAlphaValue = -1
	eLocateFadeOutState eState = eLocateFadeOutState_BEFORE_FADE_IN
ENDSTRUCT

STRUCT CACHED_VEHICLE_STRUCT
	NETWORK_INDEX niCachedVehicle
	MODEL_NAMES mnModelName
	INT iVehColour1
	INT iVehColour2
	STRING strNumberPlateText
	INT iCustomModSettingsIndex
ENDSTRUCT

TYPEDEF FUNC BOOL AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH_CALLBACK(INT iPartOverride = -1)
AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH_CALLBACK amIWaitingForPlayersToEnterVeh

TYPEDEF FUNC BOOL SHOULD_CLEANUP_OBJECT_TRANSFORMATION_NOW_CALLBACK()
SHOULD_CLEANUP_OBJECT_TRANSFORMATION_NOW_CALLBACK shouldCleanupObjectTransformationNow

TYPEDEF PROC CLEANUP_OBJECT_TRANSFORMATIONS_CALLBACK()
CLEANUP_OBJECT_TRANSFORMATIONS_CALLBACK cleanupObjectTransformations

TYPEDEF PROC DETACH_ANY_PORTABLE_PICKUPS_CALLBACK()
DETACH_ANY_PORTABLE_PICKUPS_CALLBACK detachAnyPortablePickups

//FMMC2020 - John's Mass Callback Mess
TYPEDEF PROC GET_NEXT_SPAWNING_PED_AFTER_VEHICLE_SPAWN_CALLBACK(INT iVeh)
TYPEDEF PROC SET_PED_TO_SPAWN_NEXT_CALLBACK(INT iPed)
TYPEDEF FUNC INT GET_VEHICLE_BEING_DROPPED_OFF_CALLBACK(INT iTeam, INT iPartDroppingOff = -1)
TYPEDEF PROC DETACH_AND_RESET_ANY_PICKUPS_CALLBACK()
TYPEDEF FUNC INT DETACH_PORTABLE_PICKUP_FROM_SPECIFIC_PED_CALLBACK( ENTITY_INDEX ped, BOOL bStolen = FALSE)
TYPEDEF PROC PROCESS_HOLDING_PACKAGE_SCORING_CALLBACK()
TYPEDEF PROC SERVER_SELECT_END_MOCAP_CALLBACK(INT iteam)
TYPEDEF PROC PROCESS_OUT_OF_BOUNDS_TIMER_LIST_CALLBACK()
TYPEDEF PROC SHOW_PED_AGGRO_HELP_TEXT_CALLBACK()
TYPEDEF PROC PROCESS_REMOTE_PLAYER_THERMITE_OBJECTS_CALLBACK(INT iPart, PED_INDEX ped, INT iLocalSyncScene)
TYPEDEF PROC PROCESS_REMOTE_PLAYER_CASH_GRAB_OBJECTS_CALLBACK(PED_INDEX ped, INT iPart, INT iLocalSyncScene)
TYPEDEF FUNC BOOL SHOULD_VEHICLE_COUNT_AS_HELD_FOR_TEAM_CALLBACK(INT iveh,INT iteam)
TYPEDEF PROC DRAW_EXPLOSION_TIMER_HUD_CALLBACK()
TYPEDEF PROC SPOOK_PEDS_AT_COORD_IN_RADIUS_CALLBACK(VECTOR vCoord, FLOAT fRadius, INT iSpookID = ciSpookCoordID_GENERIC, ENTITY_INDEX entTarget = NULL, BOOL bPedNeedsLOS = TRUE)
TYPEDEF PROC PROCESS_EVERY_FRAME_PLAYER_ACTION_MODE_CALLBACK( BOOL bForceInstantUpdate )
TYPEDEF PROC FILL_TEAM_NAMES_CALLBACK( INT iTeam, PLAYER_INDEX thisPlayer )
TYPEDEF FUNC BOOL IS_PLAYER_IN_REQUIRED_LOCATION_VEHICLE_CALLBACK(PED_INDEX tempPed, INT iTeam, INT iLoc = -1)
TYPEDEF FUNC BOOL IS_TEAMMATE_READY_TO_DELIVER_SPECIFIC_VEHICLE_CALLBACK(INT iTeam)
TYPEDEF FUNC INT GET_NUMBER_OF_FREE_SEATS_IN_HIGH_PRIORITY_VEHICLES_FOR_TEAM_CALLBACK(INT iTeam)
TYPEDEF PROC SET_LOCATE_TO_DISPLAY_CALLBACK( locate_fade_out_data& toDisplay, INT iDebugIndex = -1, INT iDebugTeam = -1)
TYPEDEF PROC PROCESS_DISPLACEMENT_ARENA_INTERIOR_MC_CALLBACK()
TYPEDEF PROC SET_LOCATE_TO_FADE_OUT_CALLBACK( locate_fade_out_data& toFadeOut, INT iDebugIndex = -1, INT iDebugTeam = -1)
TYPEDEF PROC UPDATE_LOCATE_FADE_STRUCT_CALLBACK( locate_fade_out_data& toUpdate, INT iDebugIndex = -1, INT iDebugTeam = -1 )
TYPEDEF FUNC BOOL TASK_PERFORM_AMBIENT_ANIMATION_CALLBACK(FMMC_PED_STATE &sPedState, INT iAnimation, BOOL bForcingBreakout = FALSE, BOOL bisSpecialAnim = FALSE)
TYPEDEF FUNC BOOL PRINT_CUSTOM_OBJECTIVE_CALLBACK(TEXT_LABEL_63 tl63CustomGodText)
TYPEDEF FUNC BOOL PRINT_OBJECTIVE_WITH_CUSTOM_STRING_CALLBACK(STRING sMainText,STRING sInsertedText)
TYPEDEF FUNC BOOL CREATE_DRILL_MINIGAME_PROPS_CALLBACK(INT iObj)
TYPEDEF FUNC BOOL SHOULD_PED_RESPAWN_NOW_CALLBACK(FMMC_PED_STATE &sPedState)

GET_NEXT_SPAWNING_PED_AFTER_VEHICLE_SPAWN_CALLBACK	getNextSpawningPedAfterVehicleSpawn
SET_PED_TO_SPAWN_NEXT_CALLBACK	setPedToSpawnNext
GET_VEHICLE_BEING_DROPPED_OFF_CALLBACK	getVehicleBeingDroppedOff
DETACH_AND_RESET_ANY_PICKUPS_CALLBACK	detachAndResetAnyPickups
DETACH_PORTABLE_PICKUP_FROM_SPECIFIC_PED_CALLBACK	detachPortablePickupFromSpecificPed
PROCESS_REMOTE_PLAYER_THERMITE_OBJECTS_CALLBACK	processRemotePlayerThermiteObjects
PROCESS_REMOTE_PLAYER_CASH_GRAB_OBJECTS_CALLBACK	processRemotePlayerCashGrabObjects
SHOULD_VEHICLE_COUNT_AS_HELD_FOR_TEAM_CALLBACK	shouldVehicleCountAsHeldForTeam
DRAW_EXPLOSION_TIMER_HUD_CALLBACK	drawExplosionTimerHud
SPOOK_PEDS_AT_COORD_IN_RADIUS_CALLBACK	spookPedsAtCoordInRadius
PROCESS_EVERY_FRAME_PLAYER_ACTION_MODE_CALLBACK	processEveryFramePlayerActionMode
FILL_TEAM_NAMES_CALLBACK	fillTeamNames
IS_PLAYER_IN_REQUIRED_LOCATION_VEHICLE_CALLBACK	isPlayerInRequiredLocationVehicle
IS_TEAMMATE_READY_TO_DELIVER_SPECIFIC_VEHICLE_CALLBACK	isTeammateReadyToDeliverSpecificVehicle
GET_NUMBER_OF_FREE_SEATS_IN_HIGH_PRIORITY_VEHICLES_FOR_TEAM_CALLBACK	getNumberOfFreeSeatsInHighPriorityVehiclesForTeam
SET_LOCATE_TO_DISPLAY_CALLBACK	setLocateToDisplay
PROCESS_DISPLACEMENT_ARENA_INTERIOR_MC_CALLBACK	processDisplacementArenaInteriorMc
SET_LOCATE_TO_FADE_OUT_CALLBACK	setLocateToFadeOut
UPDATE_LOCATE_FADE_STRUCT_CALLBACK	updateLocateFadeStruct
TASK_PERFORM_AMBIENT_ANIMATION_CALLBACK	taskPerformAmbientAnimation
CREATE_DRILL_MINIGAME_PROPS_CALLBACK	createDrillMinigameProps
SHOULD_PED_RESPAWN_NOW_CALLBACK shouldPedRespawnNowCallback

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entity Self Destruct System
// ##### Description: Variables and CONSTS for the Entity Self Destruct Sequence.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT ciEntitySelfDestruct_Finished				0
CONST_INT ciEntitySelfDestruct_ProcessFizzle		1
CONST_INT ciEntitySelfDestruct_ProcessExplosion		2
CONST_INT ciEntitySelfDestruct_KillEntity			3

// Making it as a struct in case we want to add various stuff like PTFX/SFX or more complicated logic in the future.
STRUCT ENTITY_SELF_DESTRUCT_STRUCT
	SCRIPT_TIMER tdTimer
	INT iBS
	VECTOR vPos
ENDSTRUCT

ENTITY_SELF_DESTRUCT_STRUCT sPedSelfDestruct[FMMC_MAX_PEDS]
ENTITY_SELF_DESTRUCT_STRUCT sVehSelfDestruct[FMMC_MAX_VEHICLES]
ENTITY_SELF_DESTRUCT_STRUCT sObjSelfDestruct[FMMC_MAX_NUM_OBJECTS]
			
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Do Participant Loop
// ##### Description: Variables, Consts and Enums for DO_PARTICIPANT_LOOP()
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
				
INT iParticipantLoop_PreviousPartChecked = -1
INT iParticipantLoop_NumParticipantsChecked = 0
INT iParticipantLoop_NumParticipantsToCheck = 0
//PARTICIPANT_INDEX piParticipantLoop_ParticipantIndex  - Unreferenced, uncomment if needed.
PLAYER_INDEX piParticipantLoop_PlayerIndex
PED_INDEX piParticipantLoop_PedIndex

ENUM DO_PARTICIPANT_LOOP_FLAGS
	DPLF_NONE 					= 0,	//No special behaviour.
	DPLF_IGNORE_TEAM_0 			= 1,	//Players on Team 0 will NOT be included.
	DPLF_IGNORE_TEAM_1 			= 2,	//Players on Team 1 will NOT be included.
	DPLF_IGNORE_TEAM_2 			= 4,	//Players on Team 2 will NOT be included.
	DPLF_IGNORE_TEAM_3 			= 8,	//Players on Team 3 will NOT be included.
	DPLF_CHECK_PED_ALIVE		= 16,	//The player's ped must pass NOT IS_PED_INJURED() to be included.
	DPLF_EXCLUDE_LOCAL_PART		= 32,	//The player matching iPartToUse will NOT be included.
	DPLF_CHECK_PLAYER_OK		= 64,	//The player's ped must pass IS_NET_PLAYER_OK() to be included.
	DPLF_FILL_PLAYER_IDS		= 128,	//piParticipantLoop_ParticipantIndex, piParticipantLoop_PlayerIndex and piParticipantLoop_PedIndex will be filled for use within the WHILE loop
	DPLF_IGNORE_ACTIVE_CHECKS	= 256, 	//Doesn't check if players are active and does a full 32 player loop - should be used when called outside of game state running. Doesn't support any other flags currently.
	DPLF_EARLY_END_SPECTATORS	= 512 	//Includes early end spectators in the player count
ENDENUM

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Misc
// ##### Description: 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
INT iTimeStampForFirstRenderphasePause

ENUM eTeamOutcome
	TO_Win	= 0,
	TO_Lose	= 1,
	TO_Draw	= 2
ENDENUM

CONST_INT DIALOGUE_CANCEL_CALL_ANIM_TIME_DELAY 	3500
SCRIPT_TIMER tdDialogue_CancelCallAnimTimer

SCRIPT_TIMER tdCallVehicleCooldownTimer

SCRIPT_TIMER tdCatchFireDelay
SCRIPT_TIMER tdStopFireDelay

SCRIPT_TIMER tdHunterVisionTimer

SCRIPT_TIMER tdResurrectionShardTimer

SCRIPT_TIMER tdSpectateKillStripTimer

VECTOR vLastStoredWaypoint
BOOL bSentDrawMarkerRequest
VECTOR vNULLMARKER = <<9999,9999,9999>>

//FMMC2020 - MOVE fade out struct back here
CONST_INT OUT_OF_RANGE_TIME 750

locate_fade_out_data sFadeOutLocations[ FMMC_MAX_GO_TO_LOCATIONS ]
locate_fade_out_data locateDropOff

CONST_INT ciINVALID_CUTSCENE_WARP_HEADING		-666

BOOL bCutsceneAreaClearedLate
BOOL bLocalHandBrakeOnFlag

SCRIPT_TIMER stLbdStop
SCRIPT_TIMER streamCSFailsafeTimer
SCRIPT_TIMER tdRaceMissionTime

CONST_INT ciTIME_SUBMERSION_THRESHOLD	3000

SCRIPT_TIMER tdSubmersionTimer

INT iServerPropertyID

INT waittimezap = 700
INT iOpenAnimStarted = -1

INT iKilledByHealthDrainBS
INT iNotKilledByHealthDrainOverrideBS
SCRIPT_TIMER tdNotKilledByHealthDrainTimer
FLOAT fDrainHealPercentage = 0.0

MASK_ANIM_DATA 						maskAnimData
#IF IS_DEBUG_BUILD
INT iCachedPlayerOutfit	= -1
INT iDebug_OutfitValueOverride
INT iDebug_OutfitValueOverrideLast
CONST_INT ciDEBUG_OUTFIT_NONE					0
CONST_INT ciDEBUG_OUTFIT_BUGSTAR				1
CONST_INT ciDEBUG_OUTFIT_MAINTENANCE			2
CONST_INT ciDEBUG_OUTFIT_GRUPPE					3
CONST_INT ciDEBUG_OUTFIT_CELEB					4
CONST_INT ciDEBUG_OUTFIT_FIREFIGHTER			5
CONST_INT ciDEBUG_OUTFIT_NOOSE					6
CONST_INT ciDEBUG_OUTFIT_HIGHROLL				7
CONST_INT ciDEBUG_OUTFIT_ISLANDGUARD			8
CONST_INT ciDEBUG_OUTFIT_SMUGGLER				9
CONST_INT ciDEBUG_OUTFIT_ULP_MAINTENANCE		10
#ENDIF
//MP_OUTFITS_APPLY_DATA   			sMPOutfitApplyData

//BOOL bPreloadedMaskOutfitData 	= 	FALSE
BOOL bAnimationPlaying 			= 	FALSE

INT iteamstartPlayerJoinBitset
INT iPlayerIndexForCutscene
TEXT_LABEL_23 tlPlayerSceneHandle
//For forcing the host to not be a spectator
INT iManageSpechost = 0
INT iDesiredHost = -1

INT iDelayedDeliveryXPToAward
INT iXPExtraRewardGained
INT iTeamRowBit
INT iTeamNameRowBit 		

SCRIPT_TIMER timerSocialClubReadTime
INT iClearDeathVisualsStage

SCRIPT_TIMER stLeaderboardRounds
CONST_INT ciROUNDS_LBD_TIMEOUT 15000
INT iTimerProgressLbd
SCALEFORM_INDEX siLbdHelp
INT iCashGainedForCelebration = 0
INT iRPGainedForCelebration = 0

//The position in the leaderboard
INT iRoundsLBPosition = -1
INT iRoundsSuicides

BOOL bWasKnockOffSettingChangedForTrash = FALSE

BOOL bDelayedClientCheck = FALSE
BOOL bHasTripSkipJustFinished = FALSE

SCRIPT_TIMER tdTextDelay[FMMC_MAX_NUM_SMS]
INT iMyLocalSentTextMsgBitset

CONST_INT ciDT_Blocked_PedHasBeenTased			0
CONST_INT ciDT_Blocked_PedHasBeenTranquilized	1
CONST_INT ciDT_Blocked_PedHasBeenEMPed			2

SCRIPT_TIMER RoundCutsceneTimeOut

JOB_INTRO_CUT_DATA jobIntroData
SCRIPT_TIMER tdIntroTimer
SCRIPT_TIMER tdSafetyTimer

CONST_INT ciINTRO_SAFETY_TIMER_BAIL_TIME 50000

VECTOR vBuildingLocations
VECTOR vPlayerWarpLocation

INT iEntityRespawnType = ci_TARGET_NONE
INT iEntityRespawnID = 0
VECTOR vEntityRespawnLoc
INT iEntityRespawnInterior = -1
INT iEntityRespawnRandomSelected
INTERIOR_INSTANCE_INDEX SpawnInterior = NULL

INT iWaterCalmingQuad[FMMC_MAX_WATER_CALMING_QUADS]

LBD_VARS lbdVars
INT iRoundsLbdStage 

INT iRoundsBitSet
INT iRoundsTilePlayerBitSet
INT iRoundsTilePlayerBitSet2

INT iClientRefreshDpadValue

INT iKillSafetyTimer

INT iLightsOffSoundID = -1
INT iLightsBackOnSoundID = -1

INT iSoundIDCountdown = -1
INT iSoundIDVehBomb = GET_SOUND_ID()
INT iSoundIDVehBomb2 = GET_SOUND_ID()
INT iSoundIDVehBombCountdown = GET_SOUND_ID()
INT iSoundIDVehBeacon = GET_SOUND_ID()

INT iRadioEmitterPropCheck = -1

STRING sMyRadioStation

INT iPhoneIntroDoneForShot = -1

INT iPedCausedSpeechContextBlock_CAR_CRASH[FMMC_MAX_PEDS_BITSET]

INT iPedPlayerExittedAngledArea[FMMC_MAX_PEDS_BITSET]
INT iPedPlayerExittedAngledAreaTriggered[FMMC_MAX_PEDS_BITSET]

STRUCT STRUCT_RETASK_DIRTY_FLAG_DATA
	INT iDirtyFlagPedNeedsRetask[FMMC_MAX_PEDS_BITSET]	
	INT iPedSpookedBitsetCopy[FMMC_MAX_PEDS_BITSET]
	INT iPedAggroedBitsetCopy[FMMC_MAX_PEDS_BITSET]
	INT iPedGotoProgressCopy[FMMC_MAX_PEDS]
ENDSTRUCT

STRUCT_RETASK_DIRTY_FLAG_DATA sRetaskDirtyFlagData
INT iPedGivenSpookedGotoTaskBS[FMMC_MAX_PEDS_BITSET]
INT iPedTaskedGotoProgress[FMMC_MAX_PEDS]
INT iPedMovementClipsetOverrideBitset1[FMMC_MAX_PEDS_BITSET]
INT iPedMovementClipsetOverrideBitset2[FMMC_MAX_PEDS_BITSET]
INT iPedLocallyInitialisedBitset[FMMC_MAX_PEDS_BITSET]
INT iPedHasTrailerBitset[FMMC_MAX_PEDS_BITSET]
INT iPedTrailerDetachedBitset[FMMC_MAX_PEDS_BITSET]
INT iPedStartedInCoverBitset[FMMC_MAX_PEDS_BITSET]

SAVE_OUT_UGC_PLAYER_DATA_VARS sSaveOutVars //Ratings data cloud struct
//SCRIPT_TIMER timeThumbSafe

//BOOL bfriendcomparison
INT iSpectatorBit

INT iPlaylistProgress

CONST_INT PROCESSED_PRE_GAME_FAILSAFE_TIMEOUT 20000

CONST_INT ciTimerSpectatorDelay	1000
SCRIPT_TIMER timerSpectatorDelay

SCRIPT_TIMER tdresultstimer
SCRIPT_TIMER tdsafejointimer
SCRIPT_TIMER tdeomwaittimer
SCRIPT_TIMER tdSafeJoinVariations

INT ioldCarryLimit = -2
INT iOldLoc = -1
INT iOldVeh = -1

INT iClearedTasksForHover[FMMC_MAX_PEDS_BITSET]

WEAPON_TYPE wtWeaponToRemove
INT iDlcWeaponBitSet

SCRIPT_TIMER maxAccelerationUpdateTimers[FMMC_MAX_PEDS]
FLOAT maxAccelerationCurrentSpeed[FMMC_MAX_PEDS]

// Artificial lights.
ENUM eARTIFICIAL_LIGHTS_STATE
	eARTIFICIAL_LIGHTS_STATE__ON = 0,
	eARTIFICIAL_LIGHTS_STATE__WAITING_TO_TURN_OFF,
	eARTIFICIAL_LIGHTS_STATE__OFF,
	eARTIFICIAL_LIGHTS_STATE__DEFAULT
ENDENUM

STRUCT STRUCT_ARTIFICIAL_LIGHTS_DATA
	eARTIFICIAL_LIGHTS_STATE eBaseState
	SCRIPT_TIMER sDelayTimer
	INT iDelayDuration
ENDSTRUCT

ENUM eAUDIO_MIXES
	AUDIO_MIX_DEFAULT = 0,
	AUDIO_MIX_GUNFIGHT,
	AUDIO_MIX_CAR_CHASE,
	AUDIO_MIX_DRIVING,
	AUDIO_MIX_NONE,
	
	MAX_AUDIO_MIXES
ENDENUM

CONST_INT ciRUGBY_SOUND_COLLECTED_BALL		0
CONST_INT ciRUGBY_SOUND_DROPPED_BALL		1
INT iRugbyCarryingBallSoundID = -1

CONST_INT ciIN_AND_OUT_SOUND_PICKUP			0
CONST_INT ciIN_AND_OUT_SOUND_DROP			1
CONST_INT ciIN_AND_OUT_SOUND_DELIVERED		2

INT iVehCapTeam = -1

INT iLocalAudioMixPriority = -1
INT iLocalCurrentAudioMix = -1

INT iLocalSpectateTarget = -1	

//crate pickup info
INT iLocalCrateBrokenBitset
SAP_PICKUP_ID piCratePickup[FMMC_MAX_NUM_OBJECTS][ciMAX_CRATE_PICKUPS]

// Grabbed Cash
CONST_INT ciGRABBED_CASH_SOURCE__MISC								-1
CONST_INT ciGRABBED_CASH_SOURCE__CASH_GRAB_MINIGAME					0
CONST_INT ciGRABBED_CASH_SOURCE__EXTRA_TAKE_CASH_PICKUPS			1
CONST_INT ciGRABBED_CASH_SOURCE__INTERACTABLE_CONSEQUENCE			2

INT iExtraTakeFromPlacedCashPickups = 0

// Mission Equipment Pickups
INT iMissionEquipmentPickupsCollected[ciCUSTOM_MISSIONEQUIPMENT_MODEL__MAX]
INT iMissionEquipmentPickupsMax[ciCUSTOM_MISSIONEQUIPMENT_MODEL__MAX]
INT iMissionEquipment_DisplayXofXTickersForEquipmentTypeBS
INT iMissionEquipment_DisplayCollectionHUDForEquipmentTypeBS

SC_LEADERBOARD_CONTROL_STRUCT scLB_control

SCALEFORM_INDEX	scLB_scaleformID
DPAD_VARS dpadVars
DPAD_VARS celebdpadVars
SCALEFORM_INDEX scaleformDpadMovie
SCRIPT_TIMER stStartPosSetFailSafe
SCRIPT_TIMER tdovertimer
SCRIPT_TIMER tdForceEndTimer
SCRIPT_TIMER tdwastedtimer
SCRIPT_TIMER tHintCam

CONST_INT ciLOBBY_LEADER_CACHE_FAIL_SAFE_TIME	10000
INT LobbyLeaderCacheFailSafeTimeStamp

SCRIPT_TIMER timerManualRespawn
SCRIPT_TIMER tdForceVehicleRespawnTimer
SCRIPT_TIMER tdForceVehicleRespawnInWaterTimer
SCRIPT_TIMER tdForceUndriveableVehicleRespawnTimer
NETWORK_INDEX netRespawnVehicle

CONST_INT ciSTUCK_TIMEOUT 20000

INT iTeamVehCurrentHealth[MAX_NUM_MC_PLAYERS]
TEXT_LABEL_63 tl63_VehicleHealthString[MAX_NUM_MC_PLAYERS]

SCRIPT_TIMER tdRespawnTimer
SCRIPT_TIMER tdRespawnFailTimer


CONST_INT iHumLabDoorDelay 1000

INT iMissionResult = ciFMMC_END_OF_MISSION_STATUS_NOT_SET

STUNTJUMP_ID sjsID[FMMC_MAX_NUM_STUNTJUMPS]
INT iStuntJumpBitSet

INT iHighKillStreak
INT iStartSuicides

INT iPedCrashVehState_ResetStates_Completed[FMMC_MAX_PEDS_BITSET]
INT iPedCrashVehState_Action1_Triggered[FMMC_MAX_PEDS_BITSET]
INT iPedCrashVehState_Action1_Completed[FMMC_MAX_PEDS_BITSET]
INT iPedCrashVehState_Action2_Triggered[FMMC_MAX_PEDS_BITSET]
INT iPedCrashVehState_Action2_Completed[FMMC_MAX_PEDS_BITSET]
INT iPedCrashVehState_CrashCheckEnabled[FMMC_MAX_PEDS_BITSET]

INT iPedTagCreated[FMMC_MAX_PEDS_BITSET]
INT iPedTagInitalised[FMMC_MAX_PEDS_BITSET]
INT iPedTag[FMMC_MAX_PEDS]

INT iPedVehWeaponsBlockedBS[FMMC_MAX_PEDS_BITSET]

INT iPedTrackingDamageBS[FMMC_MAX_PEDS_BITSET]
INT iPedHomingLockonDisabledByRuleOptionBS[ciFMMC_PED_BITSET_SIZE]

INT iPedUseRelGroupChangedToFriendly[ciFMMC_PED_BITSET_SIZE]
INT iPedUseRelGroupChangedToNeutral[ciFMMC_PED_BITSET_SIZE]
INT iPedUseRelGroupChangedToHostile[ciFMMC_PED_BITSET_SIZE]

SCRIPT_TIMER 	pedDronePerceptionBufferTimer
INT 			iPedDroneSpotter 			= -1

INT iPedHitByStunGun[ciFMMC_PED_BITSET_SIZE]

INT iVehicleDoorUnlockBitset

INT	ibootbitset
HUD_COLOURS DeliverColour[FMMC_MAX_NUM_OBJECTS]
INT ipackageSlot
INT iPackageHUDBitset

CHECKPOINT_INDEX ciLocCheckpoint[FMMC_MAX_GO_TO_LOCATIONS]
INT bsCheckPointCreated = 0
INT iCheckpointBS

BOOL bonImpromptuRace
//INT iOldCarryCount

INT iVehPartIn[MAX_NUM_MC_PLAYERS]
INT iPartInVeh[FMMC_MAX_VEHICLES]

INT iCustomPlayerBlipSetBS

GROUP_INDEX giPlayerGroup 
VECTOR vWarpPos
FLOAT ftempStartHeading

VECTOR vCrashSoundPos

INT LocalRandomSpawnBitset[4]
INT iTeamPropSpawns

BOOL bFilterIsOn
INT iFilterBS = 0
INT iActiveFilter
INT iCachedFilter
FLOAT fFilterStrength = -1.0
FLOAT fFilterStrengthCached = -1.0
INT iCachedFilterScoreCount = -1

SCRIPT_TIMER tdFilterFadeTimer
SCRIPT_TIMER tdFilterAcceleratedFadeOutTimer
CONST_INT ciFilterStrengthTimeLength 15000

//Sniperball
INT bsHasObjExploded
INT iExplodeCountdownSound
INT iExplodeLocalTimer

INT iStoredNoWeaponZone
INT iSniperBallObj
OBJECT_INDEX oiSniperBallPackage

BOOL bTakedownDamageModifier = FALSE

INT isoundid[FMMC_MAX_NUM_PROPS]
SCRIPT_TIMER AlarmTimer[FMMC_MAX_NUM_PROPS]
PICKUP_INDEX pipickup[FMMC_MAX_WEAPONS]
OBJECT_INDEX oiProps[FMMC_MAX_NUM_PROPS]
OBJECT_INDEX oiPropsChildren[FMMC_MAX_NUM_PROP_CHILDREN]
INT iGetDeliverMax = -1


// Spawn Points Debug
#IF IS_DEBUG_BUILD
BOOL bCustomRespawnPointsDebug
BOOL bShowOnScreenCustomRespawnPointDebug

INT iDebugSpawnPointsActiveBS[FMMC_MAX_TEAM_SPAWN_POINT_BITSET]
INT iDebugPreviousSpawnPointsActiveBS[FMMC_MAX_TEAM_SPAWN_POINT_BITSET]

VECTOR vDebugSpawnLastSpawnLocation
FLOAT fDebugSpawnLastSpawnHeading

VECTOR vDebugSpawnLastDeathLocation
FLOAT  fDebugSpawnLastDeathHeading

CONST_FLOAT cfDEBUG_INFO_START_LEFT_X	0.025
CONST_FLOAT cfDEBUG_INFO_START_LEFT_Y	0.25
CONST_FLOAT cfDEBUG_INFO_START_RIGHT_X	0.55
CONST_FLOAT cfDEBUG_INFO_START_RIGHT_Y	0.25

CONST_INT ciMISSION_RUNNING_HELP_WINDOW_DELAY 		4000
SCRIPT_TIMER tdMissionRunning_HelpWindow

// Could convert this into a shared array at some point.
SCRIPT_TIMER tdTypeWriter_TextTyping_DebugHelp
SCRIPT_TIMER tdTypeWriter_TextFullyDrawn_DebugHelp
INT iTypeWriterCharCounter_DebugHelp

#ENDIF


INT TeamChangeTracker[MAX_NUM_MC_PLAYERS]
INT iTeamLivesHUD[FMMC_MAX_TEAMS]

INT iCrashSound = GET_SOUND_ID()
INT iCrashSoundProp = -1

INT iPropCleanedupBS[FMMC_MAX_PROP_BITSET]
INT iPropRespawnNowBS[FMMC_MAX_PROP_BITSET]
INT iPropCleanedupTriggeredBS[FMMC_MAX_PROP_BITSET]
INT iPropDetonateTriggeredBS[FMMC_MAX_PROP_BITSET]
INT iPropHasBeenPlacedIntoInteriorBS[FMMC_MAX_PROP_BITSET]

STRUCT RUNTIME_WORLD_PROP_DATA
	INT iWorldPropFrozenBitSet[FMMC_WORLD_PROP_BITSETS]
	INT iWorldPropInvincibleBitSet[FMMC_WORLD_PROP_BITSETS]
	INT iWorldPropHiddenForCutsceneBitSet[FMMC_WORLD_PROP_BITSETS]
	INT iEnteredWorldPropInterior
	INT iWorldPropCoverBlockBS[FMMC_WORLD_PROP_BITSETS]
	OBJECT_INDEX oiWorldProps[FMMC_MAX_WORLD_PROPS]
ENDSTRUCT
RUNTIME_WORLD_PROP_DATA sRuntimeWorldPropData

INT iLocalDriverPart[FMMC_MAX_VEHICLES]
INT iNumFreeVehicles
INT iNumFreeSeats
INT iAHighPriorityVehicle = -1//number of some high priority vehicle
INT iFirstHighPriorityVehicleThisRule[FMMC_MAX_TEAMS]
INT iOldClientStageObjText
INT iVehJustIn = -1

INT iOldVehDestroyBS
INT iOldHackTargetsRemaining
INT iCachedVehCapturing
INT iOldHackingVehInRange

INT iPedSpawnFailCount[FMMC_MAX_PEDS]
INT iPedSpawnFailDelay[FMMC_MAX_PEDS]
INT iPedVehStoppedTimer[FMMC_MAX_PEDS]
INT iPedScratchVehTimer[FMMC_MAX_PEDS]
INT iPedBlockedBitset[FMMC_MAX_PEDS_BITSET]

INT iVehSpawnFailCount[FMMC_MAX_VEHICLES]
INT iVehSpawnFailDelay[FMMC_MAX_VEHICLES]

INT iVehUsesCleanupDelayForDialogue
SCRIPT_TIMER tdVehDialogueDelayTimer // Shouldn't have multiple vehicles triggers this but if we do then array it [FMMC_MAX_VEHICLES]

VEHICLE_INDEX DeliveryVeh
NETWORK_INDEX OverwriteObjectiveNetID
SCRIPT_TIMER tdDhandlerTimer
SEQUENCE_INDEX temp_sequence

INT iLocalLeaderPart[FMMC_MAX_PEDS]
INT inumfreePeds
SCRIPT_TIMER tdparkTimer[FMMC_MAX_PEDS]
SCRIPT_TIMER tdPathFailCombat[FMMC_MAX_PEDS]
VEHICLE_SEAT targetseat[FMMC_MAX_PEDS]

SCRIPT_TIMER stMGTimer0
SCRIPT_TIMER stMGTimer1
INT iMGAttempts0
INT iMGAttempts1

TWEAK_FLOAT		f_Hacking_Render_Target_x		0.101
TWEAK_FLOAT		f_Hacking_Render_Target_y		0.178
TWEAK_FLOAT		f_Hacking_Render_Target_width	0.216
TWEAK_FLOAT		f_Hacking_Render_Target_height	0.347

SCALEFORM_INDEX sfLaptop

INT rt_laptop_monitor
INT rt_default

CONST_INT bs_LockSyncActivated 0
CONST_INT bs_ShouldPlayFail 1
CONST_INT bs_ShouldPlayPass 2

CONST_INT ciRALLY_HELP_TIMER 4500
CONST_INT ciRALLY_ARROW_TIMEOUT 2000

INT iLocalCarrierPart[FMMC_MAX_NUM_OBJECTS]
INT iLocalPackageHolding[FMMC_MAX_NUM_OBJECTS]
INT iCantCollectHelpDisp
INT inumfreeObjects
INT iPrevObjCapturing = -1
INT iAICarryBitset
INT iAIAttachedBitset
//Store for player female feet
INT iPedFeetDrawable = -1
INT iPedFeetTexture = - 1

INT iLastSpectatedTarget = -1
INT iLastPartToUse = -1


// the minimum time a mission must last for to get any xp/cash
CONST_INT ciMIN_REWARD_TIME 30000

CONST_INT ciFMMC_UNLIMITED_LIVES -1

CONST_INT 	ciNUM_PLAYERS_PER_AIRCRAFT_SPAWN		4
CONST_FLOAT cfDISTANCE_BETWEEN_PLAYERS				1.4

//ped logic variables
INT iPedArrivedBitset[FMMC_MAX_PEDS_BITSET]
INT iPedChangeTaskBitset[FMMC_MAX_PEDS_BITSET]
CONST_INT ciPED_ARRIVE_GOTO_RANGE 80
CONST_INT ciPED_LEAVE_GOTO_RANGE 140
CONST_INT ciPED_BOAT_HELI_ARRIVE_GOTO_RANGE 150
CONST_INT ciPED_BOAT_HELI_LEAVE_GOTO_RANGE 250
CONST_INT ciPED_PLANE_ARRIVE_GOTO_RANGE 250
CONST_INT ciPED_PLANE_LEAVE_GOTO_RANGE 299

INT iObjectiveMaxPickup
INT	iHUDPriority = -1

INT i_Pri_Sta_IG2_AudioStreamPedID = -1
	
//Drilling/other minigame variables moved here from FM_Mission_Controller_Minigame

BOOL bIsCaseInContainer //If it containers the case obj
INT iThisContainerNumBeingInvestigated = -1 //local object loop value stored
INT iBSContainerBeingInvestigated //Local bit set for player investiging the container
INT iInvestigateContainerState = 0 //Local - player can only investigate one container at a time.

//Bitset for Cleaning Up Vehicles based on rule & distance
INT bsCleanupVehicleRule = 0
INT bsCleanupVehicleDistance = 0

CONST_INT ciHICInit 0
CONST_INT ciHICIsPlayerPlace 2
CONST_INT ciHICHasAnimFinished 3
CONST_INT ciHICCleanUp 4

REL_GROUP_HASH 						relMyFmGroup

VECTOR vGoToPoint
VECTOR vGoToPointOffset
VECTOR vPlayAnimAdvancedPlayback

INT iSafetyTimer

FLOAT fObjRot

INT iAudioBankFails

//safe cracking data
SAFE_CRACK_STRUCT SafeCrackData

SCRIPT_TIMER tdRocketTimer
CONST_INT ciRocketTimer_Cooldown		2800

FLOAT fRealExplodeProgress
INT iExplodeProgressUpdate
INT iExplodeDriverPart
SCRIPT_TIMER tdExplodeSpeedUpdateTimer
BOOL bExplodeSpeedUpdateThisFrame
FLOAT fDisplayExplodeProgress
FLOAT fVehSpeedForAudioTrigger
INT iLastMinSpeed[FMMC_MAX_TEAMS]
INT iLastMeterMax[FMMC_MAX_TEAMS]
FLOAT fMinSpeedLerpProgress[FMMC_MAX_TEAMS]

SCRIPT_TIMER tdSpeedRaceCheckpointTimer

INT iSoundIDCamBackground = GET_SOUND_ID()
INT iSoundPan = GET_SOUND_ID()
INT iSoundZoom = GET_SOUND_ID()
INT iCamSound = GET_SOUND_ID()

INT iElevatorMovementSound = -1

//1 Background
//2 Pan
//3 Zoom
//4 Change_Cam
INT iCamSoundBitSet
INT iStoreLastCam
INT iLocalCamSoundTimer

SCALEFORM_INDEX SI_SecurityCam

CONST_INT MAX_NUM_SUPRESSED_MODELS 24
INT g_iNumSupressed = 0

INT iDropZoneMusicProgress = 0
INT iDropZoneMusicEvent = -1
//Drop Zone Music Events
CONST_INT ciDROPZONE_HELI		 	0
CONST_INT ciDROPZONE_JUMP 		 	1
CONST_INT ciDROPZONE_LAND 		 	2
CONST_INT ciDROPZONE_ACTION 	 	3
CONST_INT ciDROPZONE_ACTION_HIGH 	4

//radar Zone variables
FLOAT fFurthestTargetDistTemp
FLOAT fFurthestTargetDist
FLOAT fOldFurthestTargetDist

//last vehicle radio for manual respawning 2370609
//INT iManRespawnLastRadioStation = 255

//loop iterators

INT iWorldPropIterator
//INT iDynoPropIterator
INT iStaggeredWeaponIterator
INT iSpawnedWeapons[FMMC_MAX_WEAPON_BITSET]
INT iActiveWeapons[FMMC_MAX_WEAPON_BITSET]
INT iDroppedPickupBS[FMMC_MAX_WEAPON_BITSET]
INT iPedForceDropPickup[FMMC_MAX_PEDS_BITSET]

VECTOR vDroppedPickupLocation[FMMC_MAX_WEAPONS]
INT iPickupRespawns[FMMC_MAX_WEAPONS]

INT iDroppedDynoPropBS[FMMC_MAX_DYNOPROP_BITSET]
VECTOR vDroppedDynoPropLocation[FMMC_MAX_NUM_DYNOPROPS]

INT iDroppedPedBS[FMMC_MAX_PEDS_BITSET]

//Hacking phone minigame data
S_HACKING_DATA hackingMinigameData

INT iLocalVehicleExplosiveBitset

//carry counts
INT iMyPedCarryCount
INT iMyObjectCarryCount
INT iMyVehicleCarryCount
//INT iMyOldPedCarryCount
//INT iMyOldVehCarryCount
INT iMyOldObjCarryCount
INT iLowestLeaveLocationPriority = FMMC_PRIORITY_IGNORE

INT	iTempNumberOfPlayersLeftLeaveEntity[FMMC_MAX_TEAMS][FMMC_MAX_PEDS]


INT iInAnyLocTemp = -1
INT iTempLeaveLoc = -1

INT itempPartNear = -1

INT iPartEscorting
INT iTempPartEscorting

INT iTotalObjectsToDefend = -1

INT tempiNumberOfPart[FMMC_MAX_TEAMS]
INT tempiNumOfWanted[FMMC_MAX_TEAMS]
INT tempiNumOfFakeWanted[FMMC_MAX_TEAMS]
INT tempiNumOfHighestWanted[FMMC_MAX_TEAMS]
INT tempiNumOfHighestFakeWanted[FMMC_MAX_TEAMS]
INT tempiNumOfMasks[FMMC_MAX_TEAMS]
INT tempiNumCutscenePlayers[FMMC_MAX_TEAMS]
INT tempiNumCutscenePlayersFinished[ FMMC_MAX_TEAMS ]
INT tempiNumCutsceneSpactatorPlayers[ FMMC_MAX_TEAMS ]
INT tempiNumPartPlayingAndFinished[ FMMC_MAX_TEAMS ]
INT itempChaseTargets[FMMC_MAX_TEAMS]
INT iTempTotalMissionTime[FMMC_MAX_TEAMS]
INT	tempiNumberOfPlayers[FMMC_MAX_TEAMS]
INT	tempiNumberOfPlayersAndSpectators[FMMC_MAX_TEAMS]
INT itempTeamCriticalAmmo[FMMC_MAX_TEAMS]
FLOAT fTempLowestLootCapacity[FMMC_MAX_TEAMS]

INT iTempGranularCurrentPoints[FMMC_MAX_TEAMS]

INT tempiHighestPriority[FMMC_MAX_TEAMS]
INT iCurrentHighPriority[FMMC_MAX_TEAMS]
INT iOldHighPriority[FMMC_MAX_TEAMS]

SCRIPT_TIMER tdNightVisionDelayTimer

//Custom Text Shard on Team Scoring
INT iTeamScoreLast[FMMC_MAX_TEAMS]

INT iNumHighPriorityPlayerRule[FMMC_MAX_TEAMS]
INT iNumHighPriorityLoc[FMMC_MAX_TEAMS]
INT	iNumHighPriorityPed[FMMC_MAX_TEAMS]
INT iNumHighPriorityDeadPed[FMMC_MAX_TEAMS]
INT	iNumHighPriorityVeh[FMMC_MAX_TEAMS]
INT	iNumHighPriorityObj[FMMC_MAX_TEAMS]
INT	iNumHighPriorityPedHeld[FMMC_MAX_TEAMS]
INT iTempNumVehHighestPriorityHeld[FMMC_MAX_TEAMS]
INT	iNumHighPriorityObjHeld[FMMC_MAX_TEAMS]

INT iTempTeamVehNumPlayers[FMMC_MAX_TEAMS]
INT iVehHeldBS[FMMC_MAX_TEAMS]
INT iVehHeld_NotMe_BS[FMMC_MAX_TEAMS] // Excludes any vehicle I'm holding

INT iUniqueVehicleBS[FMMC_MAX_TEAMS]
INT iUniqueVehiclesHeld[FMMC_MAX_TEAMS]
INT iUniqueVehiclesInDropOff[FMMC_MAX_TEAMS]
INT iVehCloneFromLobbyBS
INT iVehicleLocalPartNeedsToCloneBS

INT iPlayersInSeparateVehicles[FMMC_MAX_TEAMS]
INT iPlayersInAnyLocate
INT iTempHackingFails

INT	iTempNumPriorityRespawnPed[FMMC_MAX_TEAMS] 
INT	iTempNumPriorityRespawnVeh[FMMC_MAX_TEAMS] 
INT	iTempNumPriorityRespawnObj[FMMC_MAX_TEAMS] 
INT iTempPlayerRuleMissionLogic[FMMC_MAX_TEAMS]
INT iTempLocMissionLogic[FMMC_MAX_TEAMS]
INT iTempPriorityLocation[FMMC_MAX_TEAMS]
INT iTempPedMissionLogic[FMMC_MAX_TEAMS]
INT iTempVehMissionLogic[FMMC_MAX_TEAMS]
INT iTempObjMissionLogic[FMMC_MAX_TEAMS]
INT iTempObjMissionSubLogic[FMMC_MAX_TEAMS]
INT iTempCurrentPlayerRule[FMMC_MAX_TEAMS]
INT iTempCaptureLocation

SCRIPT_TIMER tdtimesincelastupdate[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
INT tempNumberOfTeamInArea[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
INT tempNumberOfTeamInAnyArea[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
INT tempNumberOfTeamInLeaveArea[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
INT tempNumberOfTeamInAnyLeaveAreas[FMMC_MAX_TEAMS]
SCRIPT_TIMER tdcontolHUDFlash

CONST_INT ci_Ped_Inventory               1
CONST_INT ci_Veh_Inventory               2
CONST_INT ci_Obj_Inventory               3

CONST_INT ci_TIME_PER_POINT                     5

//Objective end variables
STRING sObjEndText = ""

INT ireasonObjEnd[FMMC_MAX_TEAMS]
INT iObjEndPriority[FMMC_MAX_TEAMS]
//INT iObjEndMostRecentTeam = -1

//INT iObjEndPriorityBackup = -1
//INT ireasonObjEndBackup = -1
//INT iObjEndTeamBackup = -1

//high score variables
SCRIPT_TIMER tdHackTimer

INT tempiTeamCivillianKills[FMMC_MAX_TEAMS]
INT tempiTeamEnemyKills[FMMC_MAX_TEAMS]

SCRIPT_TIMER tdWantedTimer

CONST_INT ciMAX_CUT_PLAYERS 4
//Struct for the players weapon details
STRUCT PLAYER_WEAPON_DETAILS_FOR_CUTSCENE
	BOOL bHasThisWeapon
	BOOL bSet
	WEAPON_INFO sWeaponInfo
	INT iParticipant
ENDSTRUCT
PLAYER_WEAPON_DETAILS_FOR_CUTSCENE sPlayerWeaponDetailsForCutscene[ciMAX_CUT_PLAYERS]


CONST_INT ciINITIAL_GOTO                        999
CONST_INT ciSECONDARY_GOTO                      1000

VECTOR vPedTargetCoords[FMMC_MAX_PEDS]
PED_INDEX piOldTargetPed[FMMC_MAX_PEDS]

SCRIPT_TIMER tdPlayerBlipsTimer
SCRIPT_TIMER tdeventsafetytimer
SCRIPT_TIMER tdVehDropoffNetIDRegTimer

SCRIPT_TIMER tddoorsafteytimer
SCRIPT_TIMER tdresultdelay
SCRIPT_TIMER tdDeliverTimer
SCRIPT_TIMER tdDeliverBackupTimer
SCRIPT_TIMER tdForceEveryoneOutBackupTimer

FMMC_EOM_DETAILS                sEndOfMission
PLAYER_CURRENT_STATUS_COUNTS    sPlayerCounts

LEADERBOARD_PLACEMENT_TOOLS LBPlacement 
//TEXT_LABEL_23 tParticipantNames[MAX_NUM_MC_PLAYERS]
INT iSavedRow[NUM_NETWORK_PLAYERS]
INT iSavedSpectatorRow[NUM_NETWORK_PLAYERS]
INT iLBPlayerSelection = -1
TIME_DATATYPE timeScrollDelay 

SCRIPT_TIMER mocapFailsafe

SCRIPT_TIMER mocapPreCinematic

CONST_INT BS_SCC_MANUAL_CCTV 								0
CONST_INT BS_SCC_ADD_MANUAL_CCTV_HELP 						1
CONST_INT BS_SCC_RESET_ADAPTATION 							2
CONST_INT BS_SCC_PLAYED_CUTSCENE_ELEVATOR_CLOSED			3
CONST_INT BS_SCC_PLAYED_CUTSCENE_ELEVATOR_MOVING		 	4
CONST_INT BS_SCC_PLAYED_CUTSCENE_ELEVATOR_SFX			 	5

INT iElevatorMovingSFX = -1
VECTOR vPosNearestElevatorDoor
SCRIPT_TIMER tdElevatorMovingDelay		
CONST_INT ciCUTSCENE_ELEVATOR_MOVING_DELAY	2000

INT bsSpecialCaseCutscene
INT iCutsceneID_GotoShot = -1
INT iCoolDownTimerForManualCCTV

VECTOR vCCTVRot[MAX_CAMERA_SHOTS]
FLOAT fCCTVFOV[MAX_CAMERA_SHOTS]

SCRIPT_TIMER stGracePeriodAfterCutscene // Used to stop people getting shot just after a cutscene
CONST_INT	ciGRACE_PERIOD_AFTER_CUTSCENE 4 * 1000

SCALEFORM_INDEX sfDroneCutscene
INT iDroneCutsceneSoundLoop = -1

INT iHunterThermalVisionSoundLoop = -1
INT iEnemyCloseSoundLoop[ciHALLOWEEN_ADVERSARY_MAX_PLAYERS_PER_TEAM]
ENTITY_INDEX eiCurrentClosestHuntedPlayer
SCRIPT_TIMER tdThermalVisionHelpText

INT iVehDeliveryStoppedCancelled

INT iCharacterAddedToPhoneBook

INT iRespawnPointIndexToKeep = -1
INT iBSRespawnActiveFromRadius[FMMC_MAX_TEAMS][3]
INT iBSRespawnDeactivated[FMMC_MAX_TEAMS][3]
INT iBSRespawnActiveFromRadiusRemoteToggle[FMMC_MAX_TEAMS][3]

SCRIPT_TIMER stBlockPackageSoundsTimer

INT iNonRepeatingAlarmPlayedBS
INT iCountdownBleepTime = 5

//Heli Rotor Health Cache
INT iHeliRotorHealthCacheBitset
FLOAT fHeliMainRotorHealthCache[FMMC_MAX_VEHICLES]
FLOAT fHeliTailRotorHealthCache[FMMC_MAX_VEHICLES]

//Staggered Loop Full Loop Check
INT iWeaponPickupStaggeredLoopIndex = -1

//For peds looking at other peds
INT iPedLookAtBS[FMMC_MAX_PEDS_BITSET]

INT iSeatPreferenceSlot = 0

INT iMidCountdownRuleNum
INT iMidCountdownLastScore

INT iArrowBitset
SCRIPT_TIMER stRallyArrowTimer
SCRIPT_TIMER stRallyHelpTimer

SCALEFORM_INDEX siRallyArrow

VEHICLE_INDEX viLastVehicleIndexBeforeCutscene

INT iLastTimePlayerFiredTheirWeaponInSec = -1
WEAPON_TYPE wtLastUsedWeapon = WEAPONTYPE_INVALID
INT iLastAmountOfAmmoInClip = -1

INT iLastTimePlayerGotShotInSec = -1

BOOL bHelpTextArrows = FALSE

INT iPrevWantedForHUD = 0

INT iCurrentRequestStaggerForStrandMission

INT iRelGroupStaggerVars[ 5 ]
INT iNumTimesCalledRelGroupFunc

//INT iTotalCashForEveryone = 0
INT iCashStacksGrabbed
INT iLowCashStacksGrabbed
CONST_INT ciOffRuleScore_Low 1
CONST_INT ciOffRuleScore_High 5

INT iCocaineParticleEffectBitset	//FMMC_MAX_NUM_OBJECTS

INT iDeliveryVehForcingOut = -1
INT iPartSending_DeliveryVehForcingOut = -1
INT iLocHit = -1
INT iOldLocate = -1
INT iLocalGOFoundryHostageSynchSceneID = -1
INT iGOFoundryHostagePerpResetFlagPed = -1

INT iPedLeftBSCache[ciFMMC_PED_BITSET_SIZE]
INT iVehLeftBSCache = 0
INT iObjLeftBSCache = 0

INT iPedCountCache = 0
INT iVehCountCache = 0
INT iObjCountCache = 0

FLOAT fTeamMembers_ReqDist = 0.0
INT iTeamMembers_WithinReqDist

SKYSWOOP iSyncRoundLastFrame

INT iTempTeamVehicle[FMMC_MAX_TEAMS]

INT iOldRule2[FMMC_MAX_TEAMS]
INT iNewRule2[FMMC_MAX_TEAMS]

FLOAT fLastFrameTime = 0.0333 // Time since last frame (in s) - initialise as if 30fps

INT iWasTeamWantedBS
INT iWasPlayerWantedBS

CONST_INT ciTCWBS_T0GOT		0
CONST_INT ciTCWBS_T1GOT		1
CONST_INT ciTCWBS_T2GOT		2
CONST_INT ciTCWBS_T3GOT		3
CONST_INT ciTCWBS_T0LOST	4
CONST_INT ciTCWBS_T1LOST	5
CONST_INT ciTCWBS_T2LOST	6
CONST_INT ciTCWBS_T3LOST	7

INT iTeamCarrierWantedBS

INT iContBitSet
INT iDropContBitSet

INT iShouldBlipVehBitSet
INT iVehicleCustomBlip
INT iEnemyVehBitSet
INT iPedWasAliveBitSet[FMMC_MAX_PEDS_BITSET]
INT iEscortingBitSet[FMMC_MAX_PEDS_BITSET]
INT iEscortSpeedUpdatedBitset[FMMC_MAX_PEDS_BITSET]
INT iPedShouldNotStartCombat[FMMC_MAX_PEDS_BITSET]
INT iGotoObjectBitSet[FMMC_MAX_PEDS_BITSET]
INT iBSTaskUsingCoordTarget[FMMC_MAX_PEDS_BITSET]
INT iBSTaskAchieveHeadingWait[FMMC_MAX_PEDS_BITSET]
INT iBSTaskUsingIgnoreCombatChecksForTask[FMMC_MAX_PEDS_BITSET]
INT iTempGangChasePedOverride = -1

INT iVehTargetingCheckedThisRuleBS
INT iVehSeatPreferenceCheckBS
//INT iVehSeatClearedPrefBS

SCRIPT_TIMER tdDisableControlTimer

INT iPedAnimBitset[FMMC_MAX_PEDS_BITSET]
INT iPedSpecAnimBitset[FMMC_MAX_PEDS_BITSET]
INT iPedSecondaryAnimCleanupBitset[FMMC_MAX_PEDS_BITSET]
INT iPedBloodApplied[FMMC_MAX_PEDS_BITSET]

INT iPedIdleAnimPropBitset[FMMC_MAX_PEDS_BITSET]

INT iPedInventoryCleanedUpBitset[FMMC_MAX_PEDS_BITSET]
SCRIPT_TIMER tdAssociatedGOTOWaitIdleTimer[FMMC_MAX_PEDS]

CONST_INT ciFMMC_MAX_CUSTOM_SCENARIOS_ACTIVE 2
INT iAssociatedGOTOCustomScenarioIndex[ciFMMC_MAX_CUSTOM_SCENARIOS_ACTIVE]

//Shared Custom Scenario Timer consts
CONST_INT ciFMMC_CUSTOM_SCENARIO_TIMER_SKIP 0

//Specific Custom Scenario Timer consts
CONST_INT ciFMMC_CUSTOM_SCENARIO_TIMER_SUPPRESSING_FIRE_INTERVAL 1

CONST_INT ciFMMC_CUSTOM_SCENARIO_TIMER_BLIND_EXPLOSIVE_INTERVAL 1

CONST_INT ciFMMC_MAX_CUSTOM_SCENARIO_TIMERS 2
SCRIPT_TIMER tdAssociatedGOTOCustomScenarioTimer[ciFMMC_MAX_CUSTOM_SCENARIOS_ACTIVE][ciFMMC_MAX_CUSTOM_SCENARIO_TIMERS]

//Specific Custom Scenario Int consts
CONST_INT ciFMMC_CUSTOM_SCENARIO_INT_VALUE_SUPPRESSING_FIRE_STATE_COVER 0
CONST_INT ciFMMC_CUSTOM_SCENARIO_INT_VALUE_SUPPRESSING_FIRE_STATE_SHOOT 1

CONST_INT ciFMMC_CUSTOM_SCENARIO_INT_VALUE_BLIND_EXPLOSIVE_STATE_COVER 0
CONST_INT ciFMMC_CUSTOM_SCENARIO_INT_VALUE_BLIND_EXPLOSIVE_STATE_WAIT 1
CONST_INT ciFMMC_CUSTOM_SCENARIO_INT_VALUE_BLIND_EXPLOSIVE_STATE_THROW 2

CONST_INT ciFMMC_CUSTOM_SCENARIO_INT_INDEX_SUPPRESSING_FIRE_STATE	0

CONST_INT ciFMMC_CUSTOM_SCENARIO_INT_INDEX_BLIND_EXPLOSIVE_STATE	0
CONST_INT ciFMMC_CUSTOM_SCENARIO_INT_INDEX_BLIND_EXPLOSIVE_COUNT	1

CONST_INT ciFMMC_MAX_CUSTOM_SCENARIO_INTS 2
INT iAssociatedGOTOCustomScenarioInt[ciFMMC_MAX_CUSTOM_SCENARIOS_ACTIVE][ciFMMC_MAX_CUSTOM_SCENARIO_INTS]

INT iFlaggedForHighPriorityProcessingBS[FMMC_MAX_PEDS_BITSET]							// They should be / still be a high priority Ped.
INT iFlaggedForHighPriorityProcessingNextFrameBS[FMMC_MAX_PEDS_BITSET]					// They should be a high priority ped for the next frame

INT iPedShouldProcessIdleAnim_CheckedThisFrame[FMMC_MAX_PEDS_BITSET]
INT iPedShouldProcessSpecialAnim_CheckedThisFrame[FMMC_MAX_PEDS_BITSET]
INT iPedShouldProcessIdleAnim_ResultThisFrame[FMMC_MAX_PEDS_BITSET]
INT iPedShouldProcessSpecialAnim_ResultThisFrame[FMMC_MAX_PEDS_BITSET]

NETWORK_INDEX niBriefcaseForPedTask
NETWORK_INDEX niPhoneforPedtask
NETWORK_INDEX niMinigameObjects[4]
NETWORK_INDEX niWeldTorch
NETWORK_INDEX niGolfClub

#IF IS_DEBUG_BUILD
BOOL bBagCapacityOnScreenDebug
TWEAK_FLOAT ciBAG_CAPACITY_WEIGHT__CASH			0.0
TWEAK_FLOAT ciBAG_CAPACITY_WEIGHT__COKE			0.0
TWEAK_FLOAT ciBAG_CAPACITY_WEIGHT__WEED			0.0
TWEAK_FLOAT ciBAG_CAPACITY_WEIGHT__GOLD			0.0
TWEAK_FLOAT ciBAG_CAPACITY_WEIGHT__PAINTING		0.0
#ENDIF
SCRIPT_TIMER tdBagCapacity_FullTimer
CONST_INT ciBagCapacity_FulTimerLength 5000
INT iBagCapacityMostGrabbedLootType = -1

//new way to apply outfits stuct within one call
MP_OUTFITS_APPLY_DATA sApplyOutfitData

INT iRuleOutfitLoading = -1
INT iLastOutfit = -1

INT iOldPriority[FMMC_MAX_TEAMS]
INT iOldMidpoint[FMMC_MAX_TEAMS]

CONST_INT 	BVTH_TIME_TO_STOP_FOR 5
CONST_FLOAT BVTH_STOPPING_DISTANCE 10.0

SCRIPT_TIMER tdCrateFailTimer[FMMC_MAX_NUM_OBJECTS]

//music variables
SCRIPT_TIMER tdMusicTimer
SCRIPT_TIMER tdMusicMoodTransitionDelayTimer
INT iPrevCustomMusicMoodForObjective

//Capture point variables

FLOAT iPrevCapTime[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
FLOAT iCurrCapTime[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
INT iStoredVehCaptureMS[FMMC_MAX_TEAMS][FMMC_MAX_VEHICLES] 

STRING sMocapCutscene
STRING strPreviousMocapCutsceneName
PED_INDEX MocapPlayerPed[FMMC_MAX_CUTSCENE_PLAYERS]
PED_INDEX MocapCachedIntroCutscenePlayerPedClone[FMMC_MAX_TEAMS][FMMC_MAX_CUTSCENE_PLAYERS]
PLAYER_INDEX MocapCachedIntroCutscenePlayer[FMMC_MAX_TEAMS][FMMC_MAX_CUTSCENE_PLAYERS]
INT iMocapPlayerPedMask[FMMC_MAX_CUTSCENE_PLAYERS]
OBJECT_INDEX MocapPlayerWeaponObjects[FMMC_MAX_CUTSCENE_PLAYERS]
OBJECT_INDEX playersWeaponObject
PLAYER_INDEX LocalMainCutscenePlayer[FMMC_MAX_TEAMS]
PED_INDEX ClonePlayerForCutscene
SCRIPT_TIMER tdCutsceneMidpoint

INT iCutsceneFlyCamInterpTimestamp
CUTSCENE_FLY_CAM_STATE eCutsceneFlyCamState
CAMERA_INDEX camCutsceneFlyCam
VECTOR vPlayerLastCutscenePosition
FLOAT fPlayerLastCutsceneHeading

MODEL_NAMES mnCutscenePlayerModels[FMMC_MAX_CUTSCENE_PLAYERS]

ENUM eLiftMovementState
	eLMS_WaitForStart,
	eLMS_AttachClones,
	eLMS_CloseDoors,
	eLMS_MoveLiftUp,
	eLMS_MoveLiftDown,
	eLMS_OpenDoors,
	eLMS_Complete
ENDENUM

OBJECT_INDEX objCutsceneLift_Body			// The main bulk of the lift
OBJECT_INDEX objCutsceneLift_Body_Extras1	// Extra component 1.
OBJECT_INDEX objCutsceneLift_DoorL 			// On the left when inside the lift, trying to leave through the doors
OBJECT_INDEX objCutsceneLift_DoorR 			// On the right when inside the lift, trying to leave through the doors
OBJECT_INDEX objCutsceneLift_DoorLO 		// Outer door, stays on the floor and does not move with the lift like the other doors
OBJECT_INDEX objCutsceneLift_DoorRO 		// Outer door, stays on the floor and does not move with the lift like the other doors
FLOAT fCutsceneLift_OriginZ
FLOAT fCutsceneLift_DoorLOpenHeading
FLOAT fCutsceneLift_DoorROpenHeading
FLOAT fCutsceneLift_DoorsClosedHeading
eLiftMovementState eLiftState
eCutsceneLift eCSLift

CONST_INT ciSpecificCutsceneEntityIndexes_MAX 5 //Keep up to date with the highest below

CONST_INT ciSpecificCutsceneEntityIndexes_HS4F_APP_SUB_Delta_Sub 0

CONST_INT ciSpecificCutsceneEntityIndexes_TUNF_UNI_SWAT_Riot_1 	0
CONST_INT ciSpecificCutsceneEntityIndexes_TUNF_UNI_SWAT_Riot_2 	1
CONST_INT ciSpecificCutsceneEntityIndexes_TUNF_UNI_SWAT_Riot_3 	2
CONST_INT ciSpecificCutsceneEntityIndexes_FIXF_FIN_MCS2_PhoneScreen 0
CONST_INT ciSpecificCutsceneEntityIndexes_FIX_TRIP1_EXT_Truck 	0
CONST_INT ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen0 0
CONST_INT ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen1 1
CONST_INT ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen2 2
CONST_INT ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen3 3 
CONST_INT ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen4 4
CONST_INT ciSpecificCutsceneEntityIndexes_FIX_TRIP3_MCS1_TabletScreen 0
CONST_INT ciSpecificCutsceneEntityIndexes_FIX_TRIP3_MCS1_Sniper 	1

ENTITY_INDEX eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_MAX]
VEHICLE_INDEX viTempForCutsceneEntities

INT iCutsceneRenderTargetID = -1
BINK_MOVIE_ID bmi_Cutscene
STRING sCutsceneRenderTarget

ENUM CUTSCENE_ANIM_EVENT_STAGE
	CUTSCENE_ANIM_EVENT_STAGE_0 = 0,
	CUTSCENE_ANIM_EVENT_STAGE_1,
	CUTSCENE_ANIM_EVENT_STAGE_2,
	CUTSCENE_ANIM_EVENT_STAGE_3,
	CUTSCENE_ANIM_EVENT_STAGE_4,
	CUTSCENE_ANIM_EVENT_STAGE_5,
	CUTSCENE_ANIM_EVENT_STAGE_6,
	CUTSCENE_ANIM_EVENT_STAGE_7,
	CUTSCENE_ANIM_EVENT_STAGE_8
ENDENUM
CUTSCENE_ANIM_EVENT_STAGE eCutsceneAnimEventStage

//Add new set of bits for each cutscene - use the same bitset
CONST_INT ciSpecificCutsceneBitset_HS4F_APP_SUB_Delta_Sub_Modded 						 	0
CONST_INT ciSpecificCutsceneBitset_TUNF_UNI_SWAT_SirensSet_1 							 	1
CONST_INT ciSpecificCutsceneBitset_TUNF_UNI_SWAT_SirensSet_2 								2
CONST_INT ciSpecificCutsceneBitset_TUNF_UNI_SWAT_SirensSet_3 							 	3
CONST_INT ciSpecificCutsceneBitset_HIDE_bh1_36_ballus08_l1_and_bh1_36_ballus09_l1 			4
CONST_INT ciSpecificCutsceneBitset_Hide_Spa_door											5
CONST_INT ciSpecificCutsceneBitset_Stop_Party_Music											6
CONST_INT ciSpecificCutsceneBitset_ExplodeTruck												7
CONST_INT ciSpecificCutsceneBitset_FIXF_FIN_MCS2_StartTrack1								8
CONST_INT ciSpecificCutsceneBitset_FIX_STU_EXT_HiddenDeskAndBlood							9
CONST_INT ciSpecificCutsceneBitset_FlyCamVehicleTask										10
CONST_INT ciSpecificCutsceneBitset_FIX_TRIP3_EXT_Play_Bink_1								11
CONST_INT ciSpecificCutsceneBitset_RepositionExplodeTruck									12
CONST_INT ciSpecificCutsceneBitset_FIX_TRIP3_EXT_DamageVan									13
CONST_INT ciSpecificCutsceneBitset_DidRollingStart											14
CONST_INT ciSpecificCutsceneBitset_FIX_GOLF_INT_StartMusic									15
CONST_INT ciSpecificCutsceneBitset_FIX_TRIP1_MCS2_StartMusic								16 // Fix these at some point: url:bugstar:7411006 - Rearrange bitset to reuse const values. iSpecificCutsceneBitset
CONST_INT ciSpecificCutsceneBitset_FIX_TRIP1_EXT_OverridePTFX								17
CONST_INT ciSpecificCutsceneBitset_MovedPlayerPedDuringCutscene								18
CONST_INT ciSpecificCutsceneBitset_ChangedCoverCameraHeading								19
CONST_INT ciSpecificCutsceneBitset_FIXF_FIN_MCS3_INIT_LANDING_GEAR							20
CONST_INT ciSpecificCutsceneBitset_FIX_STU_EXT_HiddenMouse									21
CONST_INT ciSpecificCutsceneBitset_SentWeaponData											22
CONST_INT ciSpecificCutsceneBitset_FlyCamVehicleRollingStart								23
CONST_INT ciSpecificCutsceneBitset_StopDreDriveMusicChecked									24
CONST_INT ciSpecificCutsceneBitset_FIX_TRIP3_EXT_Flash1										25
CONST_INT ciSpecificCutsceneBitset_FIX_TRIP3_EXT_Flash2										26
CONST_INT ciSpecificCutsceneBitset_FIX_TRIP3_EXT_Flash3										27
CONST_INT ciSpecificCutsceneBitset_FIX_TRIP3_EXT_ShowPed0									28
CONST_INT ciSpecificCutsceneBitset_FIX_TRIP3_EXT_ShowPed1									29
CONST_INT ciSpecificCutsceneBitset_FIX_TRIP3_EXT_ShowPed2									30
CONST_INT ciSpecificCutsceneBitset_FIX_TRIP3_EXT_ShowPed3									31

CONST_INT ciSpecificCutsceneBitset2_FIX_TRIP3_EXT_FixVan									0
CONST_INT ciSpecificCutsceneBitset_FIX_TRIP3_MCS1_ExtendTaskRangeOnVagosPeds				1
CONST_INT ciSpecificCutsceneBitset2_SET_INTO_COVER											2
CONST_INT ciSpecificCutsceneBitset2_PlayedSkipText											3
CONST_INT ciSpecificCutsceneBitset2_SkipTriggered											4

INT iSpecificCutsceneBitset																	
INT iSpecificCutsceneBitset2

CONST_FLOAT cfPreciseModelHideSize 		0.5

INT iCutscenePostFXTimeStamp

CONST_INT ciPedFakePlayersBitset_MP_1			0
CONST_INT ciPedFakePlayersBitset_MP_2			1
CONST_INT ciPedFakePlayersBitset_MP_3			2
CONST_INT ciPedFakePlayersBitset_MP_4			3

INT iPedFakePlayersBitset

INT iWarpCutsceneEntityBS

//for garge door sound fx during mocaps
int garage_door_sound_id = get_sound_id()

//for collect vehicle ticker
INT ivehcolBoolCheck
//for collect object ticker
INT iObjCollectHUDTriggeredBS
INT iobjCollectedBoolCheck
INT iPackageDelXPCount
INT iPackageColXPCount
INT iPedDeliveryRPCap

INT iLastRule = 0

//for goto ped bitset, stops them from leaving your group after collection.
INT iKeepFollowPedBitset[FMMC_MAX_PEDS_BITSET]

INT iObjDeliveredBitset
INT iPedDeliveredBitset[FMMC_MAX_PEDS_BITSET]

INT iObjHasBeenCarriedAtSomePointBS //Indicates that this object has been held by someone at some point (if cibsOBJ_Reset_If_Dropped_Out_Of_Bounds is set)

INT iBroadcastPriority = -1
INT iBroadcastRevalidate = -1
INT iBroadcastVehNear = -1
INT iBroadcastPedNear = -1

SCRIPT_TIMER stWaitForScoreUpdate

OBJECT_INDEX objDoorBlocker
CAMERA_INDEX ciWarpCam

INT iPedDisableReactionUntilTaskBitset[FMMC_MAX_PEDS_BITSET]
INT iPedPreventMigrationBitset[FMMC_MAX_PEDS_BITSET]
INT iPedPreventMigrationBitsetNext[FMMC_MAX_PEDS_BITSET]
#IF IS_DEBUG_BUILD
VECTOR debugPedThrowTarget[FMMC_MAX_PEDS]
#endif
ENTITY_INDEX projectileEntity[FMMC_MAX_PEDS]
SCRIPT_TIMER projectileTimer[FMMC_MAX_PEDS]

SEQUENCE_INDEX PedDropOffSequence

CONST_INT ciCOLLISION_STREAMING_MAX	15
INT iCollisionStreamingLimit
INT iPedCollisionBitset[FMMC_MAX_PEDS_BITSET]
INT iPedNavmeshCheckedBS[FMMC_MAX_PEDS_BITSET]
INT iPedNavmeshLoadedBS[FMMC_MAX_PEDS_BITSET]
INT iVehCollisionBitset

INT iPedFixationStateCleanedUpBS[FMMC_MAX_PEDS_BITSET]

INT iEarlyCelebrationTeamPoints[ FMMC_MAX_TEAMS ]

//Boss Type (CEO & MC President etc.)
INT iBossType = -1

CONST_INT ciBOSSTYPE_CEO			0
CONST_INT ciBOSSTYPE_MC_PRESIDENT	1

//Collect blip timer
SCRIPT_TIMER tdCollectBlipTimer
//Health drain
SCRIPT_TIMER tdPerSecondDrainTimer
SCRIPT_TIMER tdStopDrainTimer
SCRIPT_TIMER tdDamageDrainRateTimer
SCRIPT_TIMER tdIsShooting
INT iStopDrainTime

SCALEFORM_INDEX sfiPPGenericMovie
ENUM PPGenericState
	PPG_REQUEST_ASSETS = 0,
	PPG_INIT,
	PPG_RUNNING
ENDENUM
PPGenericState ePPGenericState

CONST_INT ciPPGICON_BLANK			0
CONST_INT ciPPGICON_SCORE			21
CONST_INT ciPPGICON_SCORE_RING		23

CONST_INT ciMax_MINIGAME_CRATES		 1							// The total number of minigame crates
CONST_INT ciMAX_MINIGAME_CRATE_DOORS ciMax_MINIGAME_CRATES * 3 	// We always want a multiple of 3 (two doors and one lock)

// entity killed bitsets

//Trip Skip Data
TRIP_SKIP_LOCAL_DATA TSLocalData
TRIP_SKIP_LOCAL_DATA FTSLocalData

INT iLocalQuickMaskBitSet
SCRIPT_TIMER stQuickEquipHelptextDelayTimer
CONST_INT ciQuickEquipHelptextDelayLength		2500

ENUM QUICK_EQUIP_MASK_TYPE
	QE_MASK_UNAVAILABLE,
	QE_MASK_EQUIP,
	QE_MASK_UNEQUIP,
	QE_MASK_AUTO_EQUIP,
	QE_MASK_REBREATHER
ENDENUM
QUICK_EQUIP_MASK_TYPE eMaskState

INT iStoreLastTeam0Score 
INT iStoreLastTeam1Score

INT iMaxCaptureAmount
INT iWinningState = -1

model_names garage_door_model

vehicle_index cutscene_vehicle[ciTOTAL_NUMBER_OF_MC_CUT_VEHCILES]

camera_index camera_a

//Storing and setting vehicle colour/livery when using manual respawn
STRUCT ManualRespawnLastVehicleDetails
	MODEL_NAMES mnModel				= DUMMY_MODEL_FOR_SCRIPT
	INT iVehicleLivery				= -1
	INT iVehicleColour1				= -1
	INT iVehicleColour2				= -1
	INT iVehicleExtraColour1		= -1
	INT iVehicleExtraColour2		= -1
ENDSTRUCT

ManualRespawnLastVehicleDetails sLastVehicleInformation

INT iLastOccupiedRespawnListVehicle = -1
VEHICLE_SEAT vsLastOccupiedVehicleSeat = VS_DRIVER
INT iVehicleSeatingEnforcementSlot

BOOL bStopDurationBlockSeatShuffle

INT iVehDamageTrackerBS = 0

// RENDERING HACKING SPRITE TO HACKING PLAYER'S PHONE
OBJECT_INDEX e_DefaultPlayerPhone
OBJECT_INDEX e_FakePlayerPhone

ENUM FAKE_PLAYER_PHONE_STATES
	FPPS_INIT,
	FPPS_UPDATE
ENDENUM
FAKE_PLAYER_PHONE_STATES e_FakePhoneState = FPPS_INIT

STRUCT MISSION_INTRO_COUNTDOWN_UI
	SCALEFORM_INDEX uiCountdown
	INT iBitFlags
	INT iFrameCount
	structTimer CountdownTimer
ENDSTRUCT

MISSION_INTRO_COUNTDOWN_UI cduiIntro

ENUM COUNTDOWN_UI_FLAGS_RACE
	CNTDWN_UI_SoundGo  		= BIT4,
	CNTDWN_UI2_Played_3 	= BIT0,
	CNTDWN_UI2_Played_2 	= BIT1,
	CNTDWN_UI2_Played_1 	= BIT2,
	CNTDWN_UI2_Played_Go	= BIT3
ENDENUM

INT iDroneCutsceneSound = -1

INT SyncCameraAndSkyswoopDownStages

SCALEFORM_INDEX uiGoStopDisplay

INT iZoomDelayGameTime

CONST_INT ciMAX_SCARED_HALLOWEEN_PEDS		8

PED_INDEX piHrtbScaryPedRef

CONST_INT ciHRTB_PULSE_INCREASE_RADIUS		25		//METERS
CONST_INT ciHRTB_PULSE_MINIMUM_RADIUS		5		//OVER 20 METERS WILL GO FROM CALMEST TO FASTEST
CONST_INT ciHRTB_SECOND_BEAT_BASE_TIME		300		//MILLISECONDS //100 MINIMUM
CONST_INT ciHRTB_RELOOP_PULSE_BASE_TIME		3000	//MILLISECONDS //1000 MINIMUM
CONST_INT ciHRTB_MAX_PLAYER_CHECK			8

SCRIPT_TIMER stHrtbVibrationTimer
SCRIPT_TIMER stFrndVibrationTimer
SCRIPT_TIMER stFrndVibrationCDTimer[ciMAX_SCARED_HALLOWEEN_PEDS]

INT iHrtbBitSet

CONST_INT ciHRTB_MODE_NOT_ACTIVE			0
CONST_INT ciHRTB_DONE_SECOND_HEARTBEAT		1
CONST_INT ciHRTB_HAS_HANDLE_TO_SCARY_PED	2
CONST_INT ciHRTB_LOCAL_PED_IS_SCARY_PED		3

INT iFrndVibCacheBitSet

FLOAT fHrtbDistanceFromScaryPed

INT iHrtbDistanceUpdateTimer = 10

INT iHunterBreathingSoundID
TEXT_LABEL_15 tlBreathingTrack


//Stop players camping variables
VECTOR vLastCachedPosition
CONST_INT ciIDLE_BLIP_CAMP_RADIUS	2 //meters

SCRIPT_TIMER tIdleTimer
INT iIdleBlipExitRetry

INT iPlayerThermalRefreshBitset

VECTOR vDZPlayerSpawnLoc
FLOAT fDZPlayerVehHeading
INT iDZVehicleIndex

SCRIPT_TIMER biBlipOnSpeedTimer[NUM_NETWORK_PLAYERS]
INT iBlippingPlayerBS

//Sets the time that the player can collect the pickup again after just dropping it
SCRIPT_TIMER stPickupTimeoutTimer
INT iPickupTimeoutTime = 3000

SCRIPT_TIMER stIconRemovalDelayTimer
INT iIconRemovalTime = 1500

#IF IS_DEBUG_BUILD
BOOL bBlockingObjectLastFrame = FALSE
#ENDIF

INT iNumObjsHeldLastFrame = 0

CONST_INT ciSTART_CUSTOM_LEVEL_SHARDS_WITH_STRAPLINES	4
CONST_INT ciEND_CUSTOM_LEVEL_SHARDS_WITH_STRAPLINES		12
CONST_INT ciSTART_CUSTOM_TLAD_SHARD_WITH_STRAPLINE		20
CONST_INT ciEND_CUSTOM_TLAD_SHARD_WITH_STRAPLINE		29
CONST_INT ciEND_CUSTOM_LEVEL_SHARD_TEXT					21
CONST_INT ci10_CUSTOM_LEVEL_SHARD_TEXT					26
CONST_INT ciVEHICLE_SWAP_SHARD_TEXT						27

INT iPartTeammateWasKilledBy = -2

//Vehicle Menu
SCRIPT_TIMER VehicleDelayTimer

//Vehicle Rockets Ammo
INT iVehicleRockets

//Vehicle Boost
INT iVehicleBoost

//Vehicle Repair
INT iVehicleRepair

//Vehicle Spikes
INT iVehicleSpikes

//Equip Last Weapon after Parachute
WEAPON_TYPE wtLastWeaponParachute
BOOL bParachuteInProgress

//For storing out the mission results
INT iLBPositionStore
INT iTeamLBPositionStore

FLOAT fBloomScale = 0

INT iTrailSound0 = -1 
INT iTrailSound1 = -1
INT iTrailSound2 = -1
INT iTrailSound3 = -1

//Rugby
WEAPON_TYPE wtRugbyRespawnWeapon = WEAPONTYPE_INVALID
PLAYER_INDEX piLastDeliverer = INVALID_PLAYER_INDEX()

SCRIPT_TIMER tdRugby_TryShardTimer
WEAPON_TYPE wtCachedLastWeaponHeld
BOOL bHasObjectJustBeenDropped

BOOL bAppliedVehicleDrag = FALSE

CONST_INT 	ciTIMESTAMP_FOR_TEAM_SWAP		2000

FLOAT fTempCaptureTime = 0

SCRIPT_TIMER tdTeamSwapPassiveModeTimer
SCRIPT_TIMER tdTeamSwapFailSafe

NETWORK_INDEX netVehicleToCleanUp
SCRIPT_TIMER vehicleCleanUpTimer
CONST_INT ciCLEAN_UP_VEHICLE_TIME		5000

INT iPartLastDamager = -1
INT iPartToShootPlayerLast = -1

CONST_INT ciObjectiveText_Primary 			0
CONST_INT ciObjectiveText_Secondary			1
CONST_INT ciObjectiveText_AltSecondary		2

//Lost vs Damned
INT iRuleHour
INT iRuleMinute
INT iTargetHour = -1
CONST_INT iSuddenDeathHour	 19 //DO NOT put this above 23. 0-23 only
PTFX_ID TLADptfxLensDirt

INT iNextTimeUpdateHour
INT iNextTimeUpdateMinute
INT iTimeUpdateFinishTime
INT iUpdateRateHour
INT iUpdateRateMinute

CONST_INT iDayAnimFXDura 1500

WEAPON_TYPE wtLastWeapon[MAX_NUM_MC_PLAYERS]

//Pointless
INT iPTLTeamMateScore[MAX_NUM_MC_PLAYERS]

TEXT_LABEL_23 sTLToSkip


CONST_INT DUNE_MAX_FLIPS 		4
SCRIPT_TIMER stFlipTimer[DUNE_MAX_FLIPS]
VEHICLE_INDEX viFlippedVeh[DUNE_MAX_FLIPS]

CONST_INT ciVeh_up_0		0
CONST_INT ciVeh_up_1		1
CONST_INT ciVeh_up_2		2
CONST_INT ciVeh_up_3		3
CONST_INT ciVeh_up_4		4
INT iLocalVehicleExplosionBitSet

INT iLocalObjectLightUpdated
INT iLocalObjectLightReadyForUpdate

SCRIPT_TIMER timerForAllowingRespawn

ENUM eManualRespawnState
	eManualRespawnState_OKAY_TO_SPAWN = 0,
	eManualRespawnState_FADING_OUT,
	eManualRespawnState_RESPAWNING,
	eManualRespawnState_FADING_IN,
	eManualRespawnState_WAITING_AFTER_RESPAWN
ENDENUM

eManualRespawnState manualRespawnState = eManualRespawnState_OKAY_TO_SPAWN
PLAYER_SPAWN_LOCATION eManualRespawnState_SpawnLocation

CONST_INT ciMAX_PLAYER_BARS			6

STRING sCurrentRadioStation

CONST_INT ci_FLASH_TIMER_INTERVAL 				500
SCRIPT_TIMER tdFlashPropTimer

CONST_INT iPickupQuantity_Bombs			3

SCRIPT_TIMER tdMissionStartWeaponsDisabled

INT iScoreUICached[FMMC_MAX_TEAMS]

TEXT_LABEL_23 sLabelObjHelpReprint
CONST_INT ci_OBJ_HELP_DEATH_CHECK_TIME 		4000
SCRIPT_TIMER tdObjHelpDeathCheckTimer

BOOL bTimecycleTransitionComplete = TRUE

// ===== SEA MINES ===== //
INT iSeaMine_HasExploded = 0

ENUM INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE
	INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__INIT,
	INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__IDLE,
	INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__PLANTING,
	INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__EXITING
ENDENUM

SCRIPT_TIMER tdJuggernautVFX
TWEAK_INT ciJUGGERNAUT_VFX_INTERVAL	4000
VECTOR vJuggernautVFXPos = <<0.1, 0.18, 0.0>>
VECTOR vJuggernautVFXRot = <<0.0, 0.0, 28.0>>

VECTOR vJuggernautVFXFire1Pos = <<0.1, 0.18, 0.0>>
VECTOR vJuggernautVFXFire1Rot = <<0.0, 47.0, 28.0>>
VECTOR vJuggernautVFXFire2Pos = <<0.1, 0.18, 0.0>>
VECTOR vJuggernautVFXFire2Rot = <<0.0, -47.0, 28.0>>

INT iSoundsSetForJuggernaut

#IF IS_DEBUG_BUILD
BOOL bEnableScriptAudioSpammyPrints = FALSE
#ENDIF

// ==================== Killstreak Powerups END ===============================

INT iBulletSoundID = -1

SPECIAL_VEHICLE_PICKUP eLastVehiclePickup

INT iMaxCashLocalPlayerCanCarry					

CONST_INT ci_WAIT_FOR_PED_RULE_SPAWN			2500
SCRIPT_TIMER tdWaitForPedRuleSpawn

INT iPartForTeamHealthBar[FMMC_MAX_TEAMS]

//SHAPETEST_INDEX siBombPlaceShapeTest
//BOOL bRequestedToPlaceBomb
//BOOL bCanPlaceBomb
//BOOL bPlaceCloserBomb

INT iCountThisVehAsDestroyedBS = 0

ENUM SPAWN_IN_PLACED_VEH_STAGE
	eRESTARTVEH_INIT = 0,
	eRESTARTVEH_ENTER_VEH,
	eRESTARTVEH_FINISHED
ENDENUM
SPAWN_IN_PLACED_VEH_STAGE eRestartVehStage

INT iVehToSpawnIn

SCRIPT_TIMER tdVehWarpBailTime
CONST_INT ciVEH_WARP_TIMEOUT 5000

SCRIPT_TIMER tdJipNoTeamTimer
CONST_INT ciJIP_NO_TEAM_TIMEOUT 15000

SCRIPT_TIMER tdLobbyHostBailTimer
CONST_INT ciLOBBY_HOST_BAIL_TIMER 15000

SCRIPT_TIMER tdInvalidServerBDBailTimer
CONST_INT ciJIP_INVALID_SERVER_BD_BAIL_TIMER 15000

SCRIPT_TIMER tdSVMIntroSafetyTimer

STRUCT UI_SLOT
	STRING sName
	INT iValue = -3
ENDSTRUCT

SCRIPT_TIMER tdManualRespawnTeamVehicle_FailTimedOut
CONST_INT ci_MAN_RESP_FAIL_TIMED_OUT		8000

WEAPON_TYPE wtCurrentRuinerWeaponCache
INT iRuinerRocketsAmmo			= -1
INT iRuinerRocketsHomingAmmo	= -1
INT iRuinerMGAmmo				= -1
INT iRuinerRocketsAmmoMax		= -1
INT iRuinerRocketsHomingAmmoMax	= -1
INT iRuinerMGAmmoMax			= -1

SCRIPT_TIMER tdDefenseSphereInvulnerabilityTimer
SCRIPT_TIMER tdDefenseSphereAlphaTimer

SCRIPT_TIMER tdMinigameDamageReductionTimer
CONST_INT ciMINIGAME_DAMAGE_REDUCTION_TIME 3000

INT iDoorHasLinkedEntityBS

INT iPickupCollected[2]
INT iPickupCollectedCleanupLocally[2]

INT iSoundIDLastAlive = -1


INT iTimeObjectTeamControlBarCached[FMMC_MAX_TEAMS]
INT iInventoryObjectIndex = -1

ENUM FMMC_INVENTORY_TYPES
	FMMC_INVENTORY_STARTING,
	FMMC_INVENTORY_MID_MISSION,
	FMMC_INVENTORY_MID_MISSION_2,
	FMMC_INVENTORY_CUSTOM_MID_MISSION
ENDENUM
INT iInventory_Starting[FMMC_MAX_TEAMS]
INT iInventory_CustomMidMission = -1

INT iLastUsedPlacedVeh = -1
INT iEnterVehiclePreReqsAppliedBS
INT iVehicleCargoBeepPlayedBS
INT iVehicleCargoDefusePlayedBS
INT iVehicleCargoExplodedBS

INT iCollectObjectPreReqsAppliedBS
INT iCollectObjectPreReqsNoResetAppliedBS

SCRIPT_TIMER tdAirstrikeExplodeTimer
VECTOR vAirstrikeOffset, vAirstrikeTargetCoords
BOOL bDisableExplosionBarKill, bUseRAGValues, bAirstrikeSpheres, bAirstrikeSphereMin, bAirstrikeSphereMax
FLOAT fRAGAirstrikeMin, fRAGAirstrikeMax
SCRIPT_TIMER tdAirstrikeRocketTimer, tdAirstrikeStartTimer, tdAirstrikeSafetyTimer
INT iAirstrikeExplosionCount
INT iAirstrikeRocketFiredBitSet
CONST_INT ci_iAirstrikeStartDelay 2000
VECTOR vDialogueAirstrikeCoords

SCRIPT_TIMER tdTransformDelayTimer
CONST_INT ciTRANSFORM_DELAY_TIME 2000

SCRIPT_TIMER tdWaitForScoreShardTimeOut

ENUM VEHICLE_AUDIO_STREAM_STATE
	VASS_WAIT,
	VASS_LOAD,
	VASS_START,
	VASS_PLAYING,
	VASS_END
ENDENUM
VEHICLE_AUDIO_STREAM_STATE eVehicleAudioStreamState
SCRIPT_TIMER tdVehicleAudioStreamTimer
CONST_INT ciVehicleAudioStreamDelay 4000

INT iMyParachuteMC = 0

FLOAT fLightIntesityScale = -1

CONST_INT ciGRANULAR_CAPTURE_TIME 1000
SCRIPT_TIMER tdGranularCaptureTimer

INT iGranularPointsThisFrame

INT iDisableVehicleWeaponBitset = 0
INT iDisableVehiclColdmgBitset = 0
INT iShouldProcessVehiclColdmgBitset = 0

INT iDisableVehiclePedNavigation = 0

#IF IS_DEBUG_BUILD
	SCRIPT_TIMER tdLoadAndCreateTimer
	BOOL bPrintedLoadTime = FALSE
#ENDIF

INT iBonusXP

INT iVehConsideredBS

SCRIPT_TIMER tdCutsceneEventDelay
CONST_INT ciCUTSCENE_EVENT_DELAY 500

CONST_INT ciMOC_COUNTDOWN_STOP_TIME		500
SCRIPT_TIMER tdMOCCountdownStopTimer

INT iCachedParachuteSmokeRValue
INT iCachedParachuteSmokeGValue
INT iCachedParachuteSmokeBValue

INT iCachedNextLightningStrike
INT iRemoveLightningStrikeBlipTime

FLOAT fCachedHealthRechargeMaxPercent

BLIP_INDEX biCachedVehicleBlip
BLIP_INDEX biCachedVehicleRangeCheckBlip

CONST_INT ciFLAMEEFFECT_SACTUS_WHEEL 			0
CONST_INT ciFLAMEEFFECT_SACTUS_HEADLIGHT 		1
CONST_INT ciFLAMEEFFECT_HUNTER_WEAPON 		2
CONST_INT ciFLAMEEFFECT_HUNTER 			3
CONST_INT ciFLAMEEFFECT_MAX				 		4
PTFX_ID ptfxFlameEffect[ciFLAMEEFFECT_MAX]

ENUM eHalloweenModeRiderStateForVFXEnum
	eHalloweenModeRiderStateForVFX_Invalid,
	eHalloweenModeRiderStateForVFX_Default,
	eHalloweenModeRiderStateForVFX_Weak,
	eHalloweenModeRiderStateForVFX_Dead
ENDENUM

eHalloweenModeRiderStateForVFXEnum eHalloweenModeRiderStateForVFX = eHalloweenModeRiderStateForVFX_Invalid
WEAPON_TYPE wtWeaponWithFireEffectApplied = WEAPONTYPE_INVALID

INT iLocalObjAtHolding

VEHICLE_SWAP_DATA sVehicleSwap

VECTOR vPlacedWorldMarkerForInteractable[FMMC_MAX_PLACED_MARKERS]
INT iPlacedWorldMarkerForInteractableOverriden[ciINTERACTABLE_LONG_BS_LENGTH]

SCRIPT_TIMER tdCircleHUDPulseTimer
CONST_INT ci_CIRCLE_HUD_PULSE_TIME 1000

CONST_INT ci_DISABLE_LOCATE_RECAPTURE_TIME		2500
SCRIPT_TIMER tdDisableLocateRecaptureTimer[NUM_NETWORK_PLAYERS]
INT iLocateFromPreviousFrame = -1

VEHICLE_INDEX vehOldTeamMultiRespawnVehicle
VEHICLE_INDEX vehOurMultiSwitchVehicle

INT iCachedPartOnTeamVehicleRespawn = -1
//INT iCachedNewVehicleDriver = -1
INT iCachedNewVehSeatPref = -3
CONST_INT ci_VEH_SEAT_SWAP_CONSENT_TIME		7000
SCRIPT_TIMER tdVehicleSeatSwapConsentTimer
CONST_INT ci_VEH_SEAT_SWAP_INVALID_TIME		5000

CONST_INT ci_VEH_SEAT_SWAP_SAFETY_TIME		3500
SCRIPT_TIMER tdVehicleSeatSwapSafetyTimer

CONST_FLOAT cfMIN_MOVING_VEH_SPEED 5.0

CONST_INT ci_VEH_SEAT_SWAP_COOLDOWN_TIME	4000
SCRIPT_TIMER tdVehicleSeatSwapCooldownTimer

SHAPETEST_INDEX stiTagShapeTest
INT iTagShapeHitSomething
ENTITY_INDEX TagTestEntity

SHAPETEST_INDEX stiHLContainerShapeTest
INT iHLContainerShapeHitSomething
ENTITY_INDEX HLContainerTestEntity

INT iManRespawnLastRadioStation = 255

SCRIPT_TIMER tdButtonHeldTimer

INT iVehLastInsideGrantedInvulnability = -1
INT iDetachedObjectSuccessfully


INT iNumberLocsChecked
INT iCapturePointBroadcastBitSet

SCRIPT_TIMER td_MissedShotTakeDamageTimer

BOOL bReadyToSwap = FALSE

INT iColourTeamCacheGotoLocateOld[FMMC_MAX_GO_TO_LOCATIONS]
INT iCaptureLocateRed[FMMC_MAX_GO_TO_LOCATIONS]
INT iCaptureLocateGreen[FMMC_MAX_GO_TO_LOCATIONS]
INT iCaptureLocateBlue[FMMC_MAX_GO_TO_LOCATIONS]

ENUM eCaptureLocateStateEnum
	eCapture_neutral,
	eCapture_captured,
	eCapture_contested
ENDENUM

eCaptureLocateStateEnum eCaptureLocateState[FMMC_MAX_GO_TO_LOCATIONS]

CONST_INT ciPED_AGGRO_RULE_SKIP_DELAY 3500

#IF IS_DEBUG_BUILD
WIDGET_GROUP_ID FMMC_WIDGET_GROUP
#ENDIF
INT iFlightModeEnabledForVeh
INT iHalloweenSoundID = -1

INT iSpawnRandomHeadingToSelect
INT iStaggeredVehLoopCounterPostCreationCalls

INT iPreviousVehHack = -1

CONST_INT ci_ChangeOutfitAppearance 2000
SCRIPT_TIMER tdChangeOutfitAppearance

BOOL bHasLaunchedFakeMissionOrbitalCannon = FALSE


INT iExplosionDestroyedObjectBitSet

SCRIPT_TIMER tdEndingTimer
CONST_INT ciEndingTimerLength 1000

SCALEFORM_INDEX sfiDownloadScreen

SCRIPT_TIMER timerResetWantedLevelInStealth

INT iVehStealthMode_MC_BS
CONST_INT	ci_VehStealthMode_HasDisabledLockOn			0
CONST_INT	ci_VehStealthMode_ActiveBeforeManualRespawn	1

CONST_INT ci_CleanUpPropEF_BS_ColourNotSet					0
INT iCleanUpPropEF_BS[FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME]

OBJECT_INDEX oiHangarWayfinding

CONST_INT ciFIB_OFFICE_NUM_PROPS	3
OBJECT_INDEX oiFIBOfficeProps[ciFIB_OFFICE_NUM_PROPS]
STRUCT FIB_OFFICE_PROP_STRUCT
	VECTOR vPosition
	VECTOR vRotation
ENDSTRUCT

SCRIPT_TIMER stHackPhoneTimer

BOOL bShowingAltitudeLimit = FALSE

#IF IS_DEBUG_BUILD
VECTOR vRagPlacementVector
VECTOR vRagPlacementVector_Prev
VECTOR vRagRotationVector
VECTOR vRagRotationVector_Prev
#ENDIF

FLOAT fInteractWithHitShakeScalar = 1.0

STRUCT MISSION_DEFUNCT_BASE_BODY_SCANNER_DATA
	VECTOR vAudioSource
	VECTOR vMin
	VECTOR vMax
	FLOAT fWidth	  	 	= 1.75
ENDSTRUCT
INT iTempHackingTargetsRemaining

MISSION_DEFUNCT_BASE_BODY_SCANNER_DATA sMissionBodyScannerData
SCRIPT_TIMER tdMissionBodyScannerTimer
INT iPlayersInsideSeatPrefOverrideVeh
INT iVehSeatPrefOverrideVeh = -1
INT iPlayersInsideSeatPref = -3

STRING sLastShardText

BOOL bAvengerHoldTransitionBlocked
INT iAvengerHoldTransitionBlockedCountTemp[FMMC_MAX_TEAMS]
SCRIPT_TIMER tdAvengerHoldUnlockTimer

INT iFTTimeElapsedLastFrame
INT iPedAliveChecked[ciFMMC_PED_BITSET_SIZE]
INT iCurrentPlacedPedsAlive
INT iModifiedTimeElapsed[FMMC_MAX_TEAMS]
INT iFTCurrentTimerProgress[FMMC_MAX_TEAMS]
SCRIPT_TIMER tdUpdateProgressBarChoppy

INT iScriptedEntitiesRegsitered
CONST_INT SCRIPTED_CUTSCENE_END_FADE_DURATION 	500
CONST_INT MOCAP_END_FADE_DURATION 				500

CONST_INT MOCAP_START_FADE_IN_DURATION_QUICK				500
CONST_INT MOCAP_START_FADE_IN_DURATION_NORMAL				1500

INT iFingerprintKeypadFailedOnce = 0
INT iFingerprintKeypadFailedTwice = 0
INT iFingerprintKeypadQuitOnce = 0

SCRIPT_TIMER tdTimeHackingTimer
INT iSiloAlarmLoopSound = -1

SCRIPT_TIMER tdEmptyAvengerTimer
CONST_INT ciForceAvengerCrash 5000

INT iPrevCurrentLoc = -1

INT iZoneRocketClearToFireBS = 0

INT iHackSuccessSoundPlayedBS = 0
INT iHackSuccessSoundProgress = 0
INT iLosingSignalForObj = -1

INT iMinigameObjBlipBlockerBS = 0
INT iMinigameObjPreReqBlockerBS = 0

VECTOR vBarragePlacementVector = <<0,0,0>>

SCRIPT_TIMER tdEveryFrameDropOffDelay[FMMC_MAX_NUM_OBJECTS]
CONST_INT ciEVERY_FRAME_DROP_OFF_DELAY_TIME 2000
SCRIPT_TIMER tdDropOff
CONST_INT iDropTimer 300

WEAPON_TYPE wtPreCutsceneWeapon

//VENETIAN JOB
FLOAT fRedTimerTime = 0.0

SCRIPT_TIMER tdPropColourChangeDelayTimer
SCRIPT_TIMER tdClearCoverTaskProp
INT iRadialPulseCounter = 0
FLOAT fRadialPulsTimePassed
FLOAT fRadialPulsTimeMax
BOOL bShowRadialPuse = FALSE
SCRIPT_TIMER tdFallingTimer
CONST_INT ci_COVER_TIME_FOR_SHARD	 1500
SCRIPT_TIMER tdCoverTimerForShard

BLIMP_SIGN sBlimpSign

INT iAlphaResetAttempts

SCRIPT_TIMER stMinSpeedSndCooldown

OBJECT_INDEX oiLargePowerups[FMMC_MAX_WEAPONS]

CONST_INT ciHUDHIDDEN_NOT_HIDDEN					-1
CONST_INT ciHUDHIDDEN_SPECTATORHUDHIDDEN			0
CONST_INT ciHUDHIDDEN_DIVE_SCORE					1
CONST_INT ciHUDHIDDEN_RENDERPHASE_PAUSED			2
CONST_INT ciHUDHIDDEN_RESPAWN_BACK_AT_START_LOGIC	3
CONST_INT ciHUDHIDDEN_MANUALLY_HIDDEN_HUD			4
CONST_INT ciHUDHIDDEN_INTERACT_WITH_DOWNLOAD		5
CONST_INT ciHUDHIDDEN_BEAMHACK						6
CONST_INT ciHUDHIDDEN_HOTWIRE						7
CONST_INT ciHUDHIDDEN_GENERAL_CHECKS_1				8
CONST_INT ciHUDHIDDEN_GENERAL_CHECKS_2				9
CONST_INT ciHUDHIDDEN_ELEVATOR						13
CONST_INT ciHUDHIDDEN_INTERACT_WITH					14

CONST_INT ciHuntingPackRemix_RUNNER					2

INT iHUDHiddenReason
INT iRadarHiddenFrame
INT iVisualAidCachedTeam[MAX_NUM_MC_PLAYERS]

SCRIPT_TIMER tdScoreAndLapSyncTimer

CONST_INT ci_MaxVehicleExtras 				5

SCRIPT_TIMER stWantedDeathCooldownTimer
CONST_INT ciWantedDeathCooldownTimer_Length		5000

INT iPlayerBlipRangeStaggeredIndex = 0

SCRIPT_TIMER stStopLocationContinueTimer
CONST_INT ciStopLocationContinue_Time		3000

SCRIPT_TIMER timerExtraVehDamageForceHitting	// Me hitting someone
CONST_INT ciTimeExtraVehDamageToBeHittingAgain 1000

MODEL_NAMES mnSuppressedVehicles[MAX_NUM_SUPRESSED_MODELS]

INT iPrevValidRule

INT iStuntRPEarned = 0

TARGET_FLOATING_SCORE sFloatingScore[TARGET_MAX_FS]

INT iLocalGhostRemotePlayerBitSet

INT iPlacedVehicleIAmIn = -1
CONST_INT ci_REGEN_VEHICLE_AFTER_THIS_TIME					1500
SCRIPT_TIMER tdVehicleRegenTimer
INT iRegenCapFromBounds
//INT iBombStaggeredIndex
//INT iTeamPotentialExplosionsThisMiniRound[FMMC_MAX_TEAMS]

//TRAPS
FMMC_ARENA_TRAPS_LOCAL_INFO sTrapInfo_Local

CONST_INT ci_STOLEN_VEH_SWAP_COOLDOWN_TIME					3000

CONST_INT ciExtraArenaTime 3000
SCRIPT_TIMER tdExtraArenaTimer

SCRIPT_TIMER tdLocalLocateDelayTimer

FLOAT fVehicleWeaponShootingTime = 2.5
FLOAT fVehicleWeaponTimeSinceLastfired = 2.0
BOOL bRegenVehicleWeapons = FALSE

INT iFakeAmmoRechargeSoundID = -1

MISSION_FACTORIES sMissionFactories
WARP_PORTAL_CLIENT_STRUCT sWarpPortal

TIME_DATATYPE timeNewsFilterNextUpdate
SCALEFORM_INDEX sfFilter
CONST_INT ci_MCFILTER_BIT_REFRESH_FILTER 0
CONST_INT ci_MCFILTER_BIT_REFRESH_NEWS_FILTER 1

FLOAT fNewsFilterScrollEntry
INT iFilterChangeTime
INT iFilterBitSet
INT iFilterStage = -1
SPEC_HUD_FILTER_LIST eCurrentFilter
INT screenRT
SPEC_HUD_FILTER_LIST eDesiredFilter
INT iSoundChangeFilter
SCRIPT_TIMER tdRandomClothingAnimDelay
SCRIPT_TIMER tdScriptedCutsceneBlackScreenScene

// Carnage Bar Local Vars - Start
SCRIPT_TIMER tdCarnageBar_LookingAtCarResetTimer
SCRIPT_TIMER tdCarnageBar_FillIncrementTimer
SCRIPT_TIMER tdCarnageBar_MoveOnRuleTimer
SCRIPT_TIMER tdCarnageBar_BigAirTimer
FLOAT fCarnageBar_BarFilledHeliHUD 											// Interp to fCarnageBar_BarFilledHeliTarget
FLOAT fCarnageBar_BarFilledCarHUD 											// Interp to fCarnageBar_BarFilledCarTarget
// Carnage Bar Local Vars - End

INT iObjectHiddenBS[FMMC_MAX_NUM_ZONES]
INT iVehHasBeenDamagedOrHarmedBS
INT iVehHasBeenDamagedOrHarmedByPlayerBS
INT iVehHasBeenTouchedByPlayerBS
INT iVehTrackTouchedByPlayerBS

CONST_FLOAT cfPlayerCrashedIntoEnemyVehMinSpeed			8.5
CONST_FLOAT cfPlayerCrashedIntoEnemyVehMaxSpeed			17.0
CONST_FLOAT cfPlayerCrashedIntoEnemyVehMinDamage		5.0
CONST_FLOAT cfPlayerCrashedIntoEnemyVehMaxDamage		10.0
INT iPlayerCrashedIntoEnemyVehIndex = -1
INT iPlayerCrashedIntoEnemyVehFrameCount = -1

INT iPedCrashReactionCooldownTimer
CONST_INT ciPedCrashReactionCooldownTimerLength		4000

INT iPlayerCollectedObjectID = -1
INT iPlayerCollectedObjectTeam = -1

SCRIPT_TIMER tdEndMissionStopMusicDelay

SCRIPT_TIMER tdVehicleWantedDelayTimer
SCRIPT_TIMER tdInitialIPLSetupTimer
SCRIPT_TIMER tdInitialIPLSetupSafetyTimeout
INT iVehicleHasBeenUnlockedBS
INT iVehicleHasBeenLockedBS

CONST_INT ciWARP_FROM_DAMAGE_TRACKER_MAX		8

INT iPedShouldWarpOnDamage[ciFMMC_PED_BITSET_SIZE]
INT iPedWarpedOnDamage[ciFMMC_PED_BITSET_SIZE]
INT iPedShouldWarpThisRuleStart[ciFMMC_PED_BITSET_SIZE]
INT iPedWarpedOnThisRule[ciFMMC_PED_BITSET_SIZE]

INT iVehicleShouldWarpOnDamage[FMMC_MAX_VEHICLE_BITSET] 	 		// Does not yet use a bitset. Wrappers require a Long Bitset.
INT iVehicleWarpedOnDamage[FMMC_MAX_VEHICLE_BITSET]					// Does not yet use a bitset. Wrappers require a Long Bitset.
INT iVehicleShouldWarpThisRuleStart[FMMC_MAX_VEHICLE_BITSET]  		// Does not yet use a bitset. Wrappers require a Long Bitset.
INT iVehicleWarpedOnThisRule[FMMC_MAX_VEHICLE_BITSET]				// Does not yet use a bitset. Wrappers require a Long Bitset.

INT iObjectShouldWarpOnDamage[FMMC_MAX_OBJECTS_BITSET]				// Does not yet use a bitset. Wrappers require a Long Bitset.
INT iObjectWarpedOnDamage[FMMC_MAX_OBJECTS_BITSET]					// Does not yet use a bitset. Wrappers require a Long Bitset.
INT iObjectShouldWarpThisRuleStart[FMMC_MAX_OBJECTS_BITSET]			// Does not yet use a bitset. Wrappers require a Long Bitset.
INT iObjectWarpedOnThisRule[FMMC_MAX_OBJECTS_BITSET]				// Does not yet use a bitset. Wrappers require a Long Bitset.

INT iGoToLocationShouldWarpOnDamage[FMMC_MAX_GOTO_BITSET]			// Does not yet use a bitset. Wrappers require a Long Bitset.
INT iGoToLocationWarpedOnDamage[FMMC_MAX_GOTO_BITSET]				// Does not yet use a bitset. Wrappers require a Long Bitset.
INT iGoToLocationShouldWarpThisRuleStart[FMMC_MAX_GOTO_BITSET]		// Does not yet use a bitset. Wrappers require a Long Bitset.
INT iGoToLocationWarpedOnThisRule[FMMC_MAX_GOTO_BITSET]				// Does not yet use a bitset. Wrappers require a Long Bitset.

INT iInteractableShouldWarpOnDamage[FMMC_MAX_INTERACTABLE_BITSET]
INT iInteractableWarpedOnDamage[FMMC_MAX_INTERACTABLE_BITSET]
INT iInteractableShouldWarpThisRuleStart[FMMC_MAX_INTERACTABLE_BITSET]
INT iInteractableWarpedOnThisRule[FMMC_MAX_INTERACTABLE_BITSET]

INT iOldMissionVehicleNeededToRepair = -1

INT iExplodeNearbyRocketsIterator = 0
CONST_INT ciNearbyRocketsListSize		15
ENTITY_INDEX eiNearbyRocketsList[ciNearbyRocketsListSize]
//ENTITY_INDEX eiPreviousNearbyRocketAddedToList
INT iExplodeNearbyRocketsListIndex = 0

INT iRenderPhasePauseDelay = 0
INT iRenderPhaseUnPauseDelay = 0
SCRIPT_TIMER tdRenderPhaseUnPauseDelay

INT iPedCompletedSecondaryGotoBS[FMMC_MAX_PEDS_BITSET]

FLOAT fPrizedVehicleHeading = 0.0
VEHICLE_INDEX vehPrizedVehicle
OBJECT_INDEX objPrizedPodium

COVERPOINT_INDEX cpPlacedCoverPoints[FMMC_MAX_NUM_COVER]

CONST_INT ci_MissionPed_MaxSyncSceneObjs			6

CONST_INT ci_MissionPedSyncSceneProp_Avi_Monitor	0
CONST_INT ci_MissionPedSyncSceneProp_Avi_Monitor2	1
CONST_INT ci_MissionPedSyncSceneProp_Avi_Mouse		2
CONST_INT ci_MissionPedSyncSceneProp_Avi_Desk		3
CONST_INT ci_MissionPedSyncSceneProp_Avi_Keyboard	4
CONST_INT ci_MissionPedSyncSceneProp_Avi_NAS		5
CONST_INT ci_MissionPedSyncSceneProp_Avi_MAX		6

OBJECT_INDEX oiAviSyncSceneObjs[ci_MissionPedSyncSceneProp_Avi_MAX]
CONST_FLOAT cfAviSequenceProcessDistance	25.0
CONST_FLOAT cfAviSequenceTriggerDistance	5.0

CONST_INT ciSASDisguiseGracePeriod			3000

CONST_INT ciScriptedFireDamageInterval		1500
CONST_INT ciScriptedFireDamageAmount		10

INT iPedSightModifierTOD = -1
INT iPedSightModifierWeather = -1

CONST_INT ciRULE_STARTED__PED_GOTO_OBJECTIVE_GRACE_PERIOD 3000
INT iRuleStartedTimeStamp

// Casino Brawl "Conversation Fight" Variables.
OBJECT_INDEX oiCasinoBrawlBottleObject
INT iCasinoBrawlBottleOutcome = -1
INT iCasinoBrawlCashierOutcome = -1

ENUM FMMC_CUTSCENE_TYPE
	FMMCCUT_MOCAP,
	FMMCCUT_ENDMOCAP,
	FMMCCUT_GENERIC_MOCAP,
	FMMCCUT_SCRIPTED
ENDENUM

FMMC_CUTSCENE_TYPE eCutsceneTypePlaying
INT iCutsceneIndexPlaying = -1

CONST_INT CUTSCENE_WAIT_FOR_ENTITIES_SAFETY_TIMER 10000
INT iCutsceneWaitForEntitiesSafetyTimeStamp

ENUM MOCAP_STREAMING_STAGE
	MOCAP_STREAMING_STAGE_INIT = 0,
	MOCAP_STREAMING_STAGE_REGISTER_ENTITIES,
	MOCAP_STREAMING_STAGE_REQUEST_MODELS,
	MOCAP_STREAMING_STAGE_ENDING
ENDENUM
MOCAP_STREAMING_STAGE eMocapStreamingStage

INT iPropHiddenForCutsceneBS[FMMC_MAX_PROP_BITSET]

INT iVehicleModShopOptionsBlockedBS

CONST_INT ciCUTSCENE_WAIT_FOR_ENTITIES_TIME			10000
SCRIPT_TIMER tdCutsceneWaitForEntitiesTimer
CONST_INT ciPROCESS_SCRIPT_INIT_LOAD_SCENE_TIMEOUT 	7500
SCRIPT_TIMER tdProcessScriptInitLoadSceneTimeout

INT iPedBlipStaggeredLoop = 0 
INT iPedAlwaysShowOneBlipCache = -1
INT iPedAlwaysShowOneBlipChosen = -1
FLOAT fPedAlwaysShowOneBlipDistCache = 9999999
VECTOR vPlayerPosCached
VECTOR vStartMicrophonePositionOverride


COMBAT_RANGE eCachedPedCombatRanges[FMMC_MAX_PEDS]

CONST_INT ciSTOLEN_GOODS_SLOW_SCALE_END 90
CONST_INT ciSTOLEN_GOODS_GOLD_MULTIPLIER 4

SCRIPT_TIMER tdLocationProcDelayTimer[FMMC_MAX_GO_TO_LOCATIONS]
INT iDoorBS_HiddenForCutscene

FMMC_MISSION_LOCAL_CONTINUITY_VARS sMissionLocalContinuityVars

SCRIPT_TIMER tdVehicleRadioSequenceDelayTimer[FMMC_MAX_VEHICLES]
INT iVehicleRadioSequenceBitset
SCRIPT_TIMER tdScriptedCutsceneSyncSceneSafetyTimer
SCRIPT_TIMER tdScriptedCutsceneSyncSceneSafetyTimer2
SCRIPT_TIMER tdScriptedCutsceneSyncSceneSafetyTimer3
SCRIPT_TIMER tdScriptedCutsceneSyncSceneVehicleTaskTimer

SCRIPT_TIMER stHintCamTemporarilyDisableTimer
CONST_INT ciHintCamTemporarilyDisableTimer_Length	750

INT iVehicleDoorSetupSetToCreatorSettings

FLOAT fCamMoveSensitivity = 0.5
FLOAT fThisPadRightX, fThisPadLeftY
FLOAT fXMove, fYMove

STRUCT sExcludedCutsceneEntities
	INT iType
	INT iIndex
ENDSTRUCT

CONST_INT ciMAX_EXCLUDED_CUTSCENE_ENTITIES 5

sExcludedCutsceneEntities excludedEntities[ ciMAX_EXCLUDED_CUTSCENE_ENTITIES ]

//Locations
HUD_COLOURS WinningTeamColour
CONST_INT ciAlphaChangeTime	500
INT iTempActivePlayers
STRUCT ANGLED_AREA_CORNER_STRUCT
	VECTOR vCorner[4]
ENDSTRUCT

STRUCT PLAYER_STATE_LIST_DATA
	UI_SLOT uiSlot[MAX_NUM_MC_PLAYERS]
	INT iCurrentPlayerCheck[FMMC_MAX_TEAMS]
	PLAYER_INDEX piPlayerStateList[MAX_NUM_MC_PLAYERS]
	INT iPlayerStateList[MAX_NUM_MC_PLAYERS]
	HUD_COLOURS hcTeam[FMMC_MAX_TEAMS]
	BOOL bSortLocalPlayerTeamTop = TRUE
	INT iNumberOfPlayersToDisplayPerTeam = 4
	INT iNumberOfTeamsToCheck = FMMC_MAX_TEAMS
	INT iNumberOfPlayersToCheck = MAX_NUM_MC_PLAYERS
ENDSTRUCT
PLAYER_STATE_LIST_DATA playerStateListDataCrossTheLine

BOOL bIsOverLine[8]
SCRIPT_TIMER stStopGoHUD
SCRIPT_TIMER stStopHUD

SCRIPT_TIMER RestartConvTimer
CONST_INT iRestartConvTime	2000

//Main
FLOAT fNewVehicleDensity = -1
FLOAT fNewPedDensity = -1 

//Players
VECTOR vLastPlayersPos

SCRIPT_TIMER tdBackGearRemoveAnimTimer
CONST_INT ciBackGearAnimTime 1750

ENUM DETONATE_BOMB_ORIGIN_ENUM
	DETONATE_BOMB_INVALID,
	DETONATE_BOMB_INTERACT_WITH,
	DETONATE_BOMB_VEHICLE_REMOTE
ENDENUM
DETONATE_BOMB_ORIGIN_ENUM eDetonateOrigin

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Player Abilities
// ##### Description: Data types and Variables for player ability functionality
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

REL_GROUP_HASH relPlayerAbilityPed

// HEAVY LOADOUT
ENUM HEAVY_LOADOUT_STATE
	HL_INACTIVE,
	HL_SET_UP_CONTAINER,
	HL_SPAWN_CONTAINER,
	HL_DROP_CONTAINER,
	HL_SPAWN_CONTENTS,
	HL_FINISHED
ENDENUM

CONST_INT ciHeavyLoadoutBS_WaitingContainerDeployment 0
CONST_INT ciHeavyLoadoutBS_ProcessingContainerDeployment 1
CONST_INT ciHeavyLoadoutBS_RequiresNetControlOfContainer 2

CONST_INT ciHeavyLoadout_MaxShapeTests 4
CONST_FLOAT cfHeavyLoadout_DropHeight 20.0		  // height above the spawn point the drop will be spawned
CONST_FLOAT cfHeavyLoadout_CharacterProximity 3.0 // proximity from the character where the drop will spawn

STRUCT HEAVY_LOADOUT_CONTAINER_PROGRESS
	INT iShapeTestCount
	VECTOR vShapeTestResults[ciHeavyLoadout_MaxShapeTests]
	VECTOR vCurrentShapeTestPoint
ENDSTRUCT

STRUCT HEAVY_LOADOUT_DATA
	HEAVY_LOADOUT_STATE eHLState	
	INT iBS_HeavyLoadoutBitset
	VECTOR vPlayerCoords
	VECTOR vPlayerForward
	HEAVY_LOADOUT_CONTAINER_PROGRESS sContainerProgress
#IF IS_DEBUG_BUILD
	BOOL bShowHeavyLoadoutDebug = FALSE
#ENDIF
ENDSTRUCT

// HELI BACKUP
ENUM  HELI_BACKUP_STATE
	HB_INACTIVE,
	HB_LAUNCHING,
	HB_ACTIVE
ENDENUM

STRUCT HELI_BACKUP_DATA
	HELI_BACKUP_STATE eHBState
ENDSTRUCT

// SUPPORT SNIPER
ENUM SUPPORT_SNIPER_STATE
	SS_INACTIVE,
	SS_DEPLOY,
	SS_READY,
	SS_AIM,
	SS_FIRE
ENDENUM

CONST_INT ciSupportSniperBS_MarkedInvalidTarget 	0
CONST_INT ciSupportSniperBS_KilledTarget 			1
CONST_INT ciSupportSniperBS_SniperHasTarget			2
CONST_INT ciSupportSniperBS_SniperAiming			3
CONST_INT ciSupportSniperBS_SniperEndedWithNoTarget	4
CONST_INT ciSupportSniperBS_SniperReadyTimerStarted 5
	
STRUCT SUPPORT_SNIPER_DATA
	SUPPORT_SNIPER_STATE eSSState
	SCRIPT_TIMER tDurationTimer
	SCRIPT_TIMER tAimTimer
	
	INT iBS_SupportSniperBitset = 0
		
#IF IS_DEBUG_BUILD
	BOOL bPlayerSpottedDebug = FALSE
	BOOL bHasValidTargetDebug = FALSE
	BOOL bShowSniperAbilityDebug = FALSE
#ENDIF
ENDSTRUCT

STRUCT VEHICLE_REPAIR_ABILITY_DATA
	VECTOR vVehicleRepairCoords
	FLOAT fVehicleRepairHeading
	
	INT iLastFrameSearchingForVehicleRepair
ENDSTRUCT

CONST_INT ciRIDE_ALONG_ABILITY_PEDS		2

STRUCT RIDE_ALONG_ABILITY_DATA
	INT iRideAlongPedIndexes[ciRIDE_ALONG_ABILITY_PEDS]
ENDSTRUCT

CONST_INT ciAMBUSH_ABILITY_VEHICLES		2
CONST_INT ciAMBUSH_ABILITY_PEDS			2
CONST_FLOAT cfAMBUSH_ABILITY_MINIMUM_DISTANCE		25.0
CONST_FLOAT cfAMBUSH_ABILITY_CLEANUP_DISTANCE		850.0

STRUCT AMBUSH_ABILITY_DATA
	INT iWarpPositionToUse = -1
	
	INT iAmbushVehicleIndexes[ciAMBUSH_ABILITY_VEHICLES]
	INT iAmbushPickupIndexes[ciAMBUSH_ABILITY_VEHICLES] // One pickup per vehicle
	INT iAmbushPedIndexes[ciAMBUSH_ABILITY_PEDS]
ENDSTRUCT

CONST_INT ciDISTRACTION_ABILITY_VEHICLES	2

STRUCT DISTRACTION_ABILITY_DATA
	INT iDistractionPedIndexes[ciDISTRACTION_ABILITY_VEHICLES]
ENDSTRUCT

//PLAYER ABILITIES COMMON
ENUM PLAYER_ABILITY_STATE
	PAS_ACTIVE,			// Ability is in use
	PAS_COOLDOWN,		// Ability is on cooldown
	PAS_READY,			// Ability can be used
	PAS_UNAVAILABLE		// Ability can not be used
ENDENUM

STRUCT LOCAL_PLAYER_ABILITY_STATE_STRUCT
	PLAYER_ABILITY_STATE eState = PAS_UNAVAILABLE
	SCRIPT_TIMER tDurationTimer
	INT iUseCount = 0
#IF IS_DEBUG_BUILD	
	BOOL bDebugActivate = FALSE
	BOOL bDebugAvailable = FALSE
#ENDIF
ENDSTRUCT

CONST_INT ciLocalPlayerAbilityMiscBS_RideAlongPedSpawned	0

STRUCT LOCAL_PLAYER_ABILITIES_STRUCT
	LOCAL_PLAYER_ABILITY_STATE_STRUCT sAbilities[PA_MAX]
	FLOAT fReconnaissanceDroneBlipRange = 500.0
	SUPPORT_SNIPER_DATA sSupportSniperData
	HELI_BACKUP_DATA sHeliBackupData
	HEAVY_LOADOUT_DATA sHeavyLoadoutData
	VEHICLE_REPAIR_ABILITY_DATA sVehicleRepairData
	RIDE_ALONG_ABILITY_DATA sRideAlongData
	AMBUSH_ABILITY_DATA sAmbushData
	DISTRACTION_ABILITY_DATA sDistractionData
	
	INT iLocalPlayerAbilityMiscBS
	
#IF IS_DEBUG_BUILD	
	BOOL bShowAbilityDebug = FALSE
	BOOL bDebugSkipCooldowns = FALSE
	BOOL bDebugIgnorePrereqs = FALSE
#ENDIF
ENDSTRUCT

STRUCT TIMER_INFO_STRUCT
	SCRIPT_TIMER tTimer
	INT iTimerDuration
	STRING sTimerTitle
ENDSTRUCT

LOCAL_PLAYER_ABILITIES_STRUCT sLocalPlayerAbilities
#IF IS_DEBUG_BUILD	
	CONST_FLOAT cfAbilityDebugRectSize 0.05
#ENDIF
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Doors
// ##### Description: Variables for door related functionality
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
OBJECT_INDEX oiCachedDoorObjects[FMMC_MAX_NUM_DOORS]
INT iCachedDoorHashes[FMMC_MAX_NUM_DOORS]
VECTOR vCachedDoorPositions[FMMC_MAX_NUM_DOORS]
INT iDoorHasBeenInitialisedBS

INT iDoorCurrentConfig[FMMC_MAX_NUM_DOORS]
INT iDoorConfigNeedsToBeAppliedBS
INT iDoorNeedsRefreshBS

INT iDoorExternallyUnlockedBS
INT iDoorExternallyLockedBS
INT iDoorExternallySetToUseAltConfigBS

INT iDoorModelSwappedBS

INT iDoorAnimDynopropAllPlayerStaggeredDoorIterator = 0
INT iDoorAnimDynopropInitialisedForAllPlayersBS
INT iDoorAnimDynopropSwappedInBS

INT iDirectionalDoorLocallyUnlockedBS
INT iPedsInDirectionalDoorsBS[FMMC_MAX_NUM_DOORS][ciFMMC_PED_BITSET_SIZE]
FLOAT fDirectionalDoorAutoDistance[FMMC_MAX_NUM_DOORS]
INT iDoorCoverAreaBlocked

SCRIPT_TIMER stDirectionalDoorEventTimer
CONST_INT ciDirectionalDoorEventCooldownDuration			350

STRUCT SCRIPTED_DOOR_SOUNDS
	STRING sDoorOpening
	STRING sDoorClosing
	STRING sDoorOpened
	STRING sDoorClosed
	
	STRING sDoorSoundset
ENDSTRUCT

CONST_FLOAT fDoorDynopropHeightOffset_Active	0.2
CONST_FLOAT fDoorDynopropHeightOffset_Inactive	5.0

CONST_FLOAT fDoorDynopropRealDoorHeightOffset_Active	0.0
CONST_FLOAT fDoorDynopropRealDoorHeightOffset_Inactive	0.0

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Burning Vehicles
// ##### Description: Variables used for burning vehicle processing.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
DECAL_ID di_RiotVanDecals[FMMC_MAX_VEHICLES]
INT iBurningVehicleScorchedBitset

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Burning Entities
// ##### Description: Variables used for burning entity processing.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
INT iBurningEntityPlayerDamageTimestamp
CONST_INT iBurningEntityPlayerDamageCooldownLength		500

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Init / Start Position / Spawning
// ##### Description: Variables used for initialising the player start position and spawning.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT BS_INIT_START_POSITION_USING_CONTINUITY							0
CONST_INT BS_INIT_START_POSITION_USING_CHECKPOINT							1
CONST_INT BS_INIT_START_POSITION_USING_TEAM_POINTS_SIMPLE					2
CONST_INT BS_INIT_START_POSITION_USING_CUSTOM_START_POINTS					3
CONST_INT BS_INIT_START_POSITION_USING_SINGLE_START_POS						4
CONST_INT BS_INIT_START_POSITION_USING_CUSTOM_START_POINT_CHECKPOINTS		5
CONST_INT BS_INIT_START_SPAWN_IN_PLACED_VEHICLE								6
CONST_INT BS_INIT_START_POSITION_ENTER_COVER								7
CONST_INT BS_INIT_START_SET_INTO_NEAREST_VEHICLE							8
CONST_INT BS_INIT_START_CHECK_INTERIOR										9
CONST_INT BS_INIT_START_SPAWN_PERSONAL_VEHICLE_NEAR_PLAYER					10
CONST_INT BS_INIT_START_POSITION_USES_VEH_POSITION							11
CONST_INT BS_INIT_START_LOAD_SCENE_STARTED									12
CONST_INT BS_INIT_START_SPAWN_IN_PERSONAL_VEHICLE							13
CONST_INT BS_INIT_START_RENABLE_PERSONAL_VEHICLES_IF_START_VEHICLE_IS_GONE	14
CONST_INT BS_INIT_START_SPAWN_PERSONAL_VEHICLE_AT_START						15
CONST_INT BS_INIT_START_SPAWN_IN_TEAM_VEHICLE								16
CONST_INT BS_INIT_START_SEPARATE_VEH_POS_IS_RELATIVE						17

CONST_INT ciENTER_VEHICLE_FAIL_SAFE_TIME		5000
CONST_INT ciPERSONAL_VEHICLE_FAIL_SAFE_TIME		6000

ENUM START_POSITION_WARPING_STATE
	START_POSITION_WARPING_STATE_INIT = 0,
	START_POSITION_WARPING_STATE_WARP,
	START_POSITION_WARPING_STATE_POST_WARP,
	START_POSITION_WARPING_STATE_VALIDATE,
	START_POSITION_WARPING_STATE_ADJUST_VEHICLE,
	START_POSITION_WARPING_STATE_FINISHED
ENDENUM

STRUCT SET_UP_START_POSITION
	START_POSITION_WARPING_STATE eStartPositionWarpingState
	INT iLocalBoolInitBS
	INT iVehicleIndexToSpawnInto	 = -1
	INT iVehicleSeatToSpawnInto 	 = -3
	INT iPlayerTeamSlot 		 	 = -1
	INT iPlayerTeam
	INT iStartRule
	VECTOR vStartPosition
	FLOAT fStartHeading
	VECTOR vPVStartPosition
	FLOAT fPVStartHeading
	SCRIPT_TIMER tdEnterVehicleFailSafeTimer
	SCRIPT_TIMER tdPersonalVehicleFailSafeTimer
	VEHICLE_INDEX vehStartingVehicle
	VECTOR vVehiclePositionOverride
	FLOAT fVehicleHeadingOverride
ENDSTRUCT

SET_UP_START_POSITION sSetUpStartPosition

CONST_FLOAT cf_ROAD_NODE_REPOSITION_EXCLUSION_LOW_MAX				0.0
CONST_FLOAT cf_ROAD_NODE_REPOSITION_EXCLUSION_MEDIUM_MAX			10.0
CONST_FLOAT cf_ROAD_NODE_REPOSITION_EXCLUSION_HIGH_MAX				50.0

CONST_INT ci_ROAD_NODE_REPOSITION_LOOP_EXCLUSION_LOW_MAX			25
CONST_INT ci_ROAD_NODE_REPOSITION_LOOP_EXCLUSION_MEDIUM_MAX			40
CONST_INT ci_ROAD_NODE_REPOSITION_LOOP_EXCLUSION_HIGH_MAX			60

CONST_INT ciSHOULD_USE_CUSTOM_START_POINT_CHECKPOINTS_RESULT		0
CONST_INT ciSHOULD_USE_CUSTOM_START_POINT_CHECKPOINTS_CHECKED		1
INT iCheckpointCacheBS

CONST_INT ciALT_VARS_SAFETY_TIMER			10000
SCRIPT_TIMER tdAltVars_SafetyTimer

CONST_INT ciALT_VARS_SAFETY_TIMER_CLIENT	20000
SCRIPT_TIMER tdAltVars_SafetyTimerClient

CONST_INT ciALT_VARS_SAFETY_TIMER_GANG		4000
SCRIPT_TIMER tdAltVars_SafetyTimerGang

CONST_INT ciPlayerPedModelSwapDelayLength 	3000
SCRIPT_TIMER tdPlayerPedModelSwapDelay

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Mission Yachts
// ##### Description: Variables used for mission yachts
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
YACHT_DATA sMissionYachtData[ciYACHT_MAX_YACHTS]
FMMC_YACHT_VARS sMissionYachtVars[ciYACHT_MAX_YACHTS]


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: End Conditions
// ##### Description: Variables and Consts used for deciding if the mission has ended
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CONST_INT ciPED_AGGRO_FAIL_DELAY 3000
CONST_INT ciCOP_SPOTTED_FAIL_DELAY 4000
CONST_INT ciALL_TEAMS_BITSET_VALUE 15

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Client Score Requests
// ##### Description: Data and Consts used in client based scoring requests
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT INCREMENT_SCORE_BITSET_TYPE_CURRENT_OBJECTIVE		0
CONST_INT INCREMENT_SCORE_BITSET_TYPE_FORCED_AMOUNT			1
CONST_INT INCREMENT_SCORE_BITSET_VICTIM_TYPE_PLAYER			2
CONST_INT INCREMENT_SCORE_BITSET_VICTIM_TYPE_PED			3
CONST_INT INCREMENT_SCORE_BITSET_VICTIM_TYPE_VEHICLE		4
CONST_INT INCREMENT_SCORE_BITSET_VICTIM_TYPE_OBJECT			5
CONST_INT INCREMENT_SCORE_BITSET_VICTIM_TYPE_OTHER			6
CONST_INT INCREMENT_SCORE_BITSET_GENERAL_KILL_POINTS		7

STRUCT INCREMENT_SCORE_DATA
	INT iScore = 0
	INT iVictimID = -1
	INT iTargetPart = -1
	INT iIdentifier = -1
	INT iBitset = 0
ENDSTRUCT

CONST_INT ciMAX_INCREMENT_SCORE_REQUESTS 	5
INCREMENT_SCORE_DATA sIncrementScoreData[ciMAX_INCREMENT_SCORE_REQUESTS]

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Server Score Requests
// ##### Description: Data and Consts used in server based scoring requests
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT ciMAX_SERVER_INCREMENT_SCORE_REQUESTS 			4
CONST_INT ciSERVER_INCREMENT_SCORE_BIT_WIDTH 				8
CONST_INT ciSERVER_INCREMENT_SCORE_MAX			 			255

CONST_INT ciSERVER_INCREMENT_SCORE_PARTICIPANT		 		0
CONST_INT ciSERVER_INCREMENT_SCORE_SCORE		 			1
CONST_INT ciSERVER_INCREMENT_SCORE_PRIORITY		 			2
CONST_INT ciSERVER_INCREMENT_SCORE_TEAM		 				3

CONST_INT ciSERVER_INCREMENT_SCORE_NEGATIVE		 			31

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Server Rule/Objective Requests
// ##### Description: Data and Consts used in Rule/Objective functionality requests
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//Set Team Failed Request
CONST_INT ciSET_TEAM_FAIL_BIT_WIDTH 						8
CONST_INT ciSET_TEAM_FAIL_REQUEST_INVALID 					255
CONST_INT ciSET_TEAM_FAIL_REQUEST_SHIFT 					2
	
CONST_INT ciSET_TEAM_FAIL_REQUEST_FAIL_REASON  				0
CONST_INT ciSET_TEAM_FAIL_REQUEST_FAIL_PART  				1
CONST_INT ciSET_TEAM_FAIL_REQUEST_FAIL_ENTITY  				2

CONST_INT ciSET_TEAM_FAIL_REQUEST_BS_ENABLED 				0
CONST_INT ciSET_TEAM_FAIL_REQUEST_BS_FORCE_UPDATE_REASON 	1

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Staggered Loop
// ##### Description: Variables used for staggered loop iterators and consts
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CONST_INT ciOBJECTIVE_ENTITIES_PER_FRAME 4
INT iObjectiveEntityIterator //Used for the Objective Entitity Loop

INT iStaggeredObjectIterator
INT iStaggeredVehicleIterator
INT iStaggeredTrainIterator

INT iStaggeredPedIterator
INT iPedsClientProcessedThisFrame[FMMC_MAX_PEDS_BITSET]
INT iPedsServerProcessedThisFrame[FMMC_MAX_PEDS_BITSET]

CONST_INT ciPROPS_PER_FRAME 5
INT iStaggeredPropIterator

CONST_INT ciINTERACTABLES_PER_FRAME 3
INT iStaggeredInteractableIterator

CONST_INT ciDOORS_PER_FRAME 1
INT iStaggeredDoorIterator

INT iStaggeredDynoPropIterator

CONST_INT ciZONES_PER_FRAME 1
INT iStaggeredZoneIterator

CONST_INT ciBOUNDS_PER_FRAME 1
INT iStaggeredBoundsIterator

INT iStaggeredLocationIterator
INT iStaggeredPlayerIterator
INT iStaggeredLocalPlayerIterator
INT iRuleChangedDuringStaggeredLocalPlayerIteratorTracker = -1

CONST_INT ciFMMC_MAX_LOCAL_PLAYER_STAGGERED_ITERATOR_ONE		0	// If these groups of processes take form into something able to be named/categorized then renamed "ONE".
CONST_INT ciFMMC_MAX_LOCAL_PLAYER_STAGGERED_ITERATOR_TWO		1	// If these groups of processes take form into something able to be named/categorized then renamed "TWO".
CONST_INT ciFMMC_MAX_LOCAL_PLAYER_STAGGERED_ITERATOR_THREE		2	// If these groups of processes take form into something able to be named/categorized then renamed "THREE".
CONST_INT ciFMMC_MAX_LOCAL_PLAYER_STAGGERED_ITERATOR_FOUR		3	// If these groups of processes take form into something able to be named/categorized then renamed "FOUR".
CONST_INT ciFMMC_MAX_LOCAL_PLAYER_STAGGERED_ITERATOR_FIVE		4	// If these groups of processes take form into something able to be named/categorized then renamed "FIVE".
CONST_INT ciFMMC_MAX_LOCAL_PLAYER_STAGGERED_ITERATOR			5			

INT iStaggeredMainStateIterator

CONST_INT ciFMMC_MAX_MAIN_STATE_STAGGERED_ITERATOR_ONE		0	// If these groups of processes take form into something able to be named/categorized then renamed "ONE".
CONST_INT ciFMMC_MAX_MAIN_STATE_STAGGERED_ITERATOR_TWO		1	// If these groups of processes take form into something able to be named/categorized then renamed "TWO".
CONST_INT ciFMMC_MAX_MAIN_STATE_STAGGERED_ITERATOR_THREE	2	// If these groups of processes take form into something able to be named/categorized then renamed "THREE"
CONST_INT ciFMMC_MAX_MAIN_STATE_STAGGERED_ITERATOR			3

INT iStaggeredTeamIterator // A general Team iterator that any system can use

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Proximity Spawning
// ##### Description: Vars for the proximity spawning system
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
INT iTempProximitySpawning_PedSpawnAllBS[FMMC_MAX_PEDS_BITSET]
INT iTempProximitySpawning_PedSpawnAnyBS[FMMC_MAX_PEDS_BITSET]
INT iTempProximitySpawning_PedSpawnOneBS[FMMC_MAX_PEDS_BITSET]
INT iTempProximitySpawning_PedCleanupBS[FMMC_MAX_PEDS_BITSET]

INT iTempProximitySpawning_DynoPropSpawnAllBS
INT iTempProximitySpawning_DynoPropSpawnAnyBS
INT iTempProximitySpawning_DynoPropSpawnOneBS
INT iTempProximitySpawning_DynoPropCleanupBS

INT iTempProximitySpawning_VehSpawnAllBS
INT iTempProximitySpawning_VehSpawnAnyBS
INT iTempProximitySpawning_VehSpawnOneBS
INT iTempProximitySpawning_VehCleanupBS

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: HUD
// ##### Description: Vars for HUD Processing - e.g. Shards
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT ciFMMC_MAX_HUD_PACKAGES 8

INT iCustomObjectiveTextOverride = -1

CONST_INT ciMAX_PLAY_SHARD_REQUESTS		4
ENUM PLAY_SHARD_TYPE
	PLAY_SHARD_TYPE_INVALID = 0,
	PLAY_SHARD_TYPE_CUSTOM_WASTED
ENDENUM
STRUCT PLAY_SHARD_DATA
	PLAY_SHARD_TYPE eShardType = PLAY_SHARD_TYPE_INVALID
	INT iVictimTeam = -1
	INT iVictim = -1
	INT iKiller = -1
ENDSTRUCT
PLAY_SHARD_DATA sPlayShardData[ciMAX_PLAY_SHARD_REQUESTS]

SCRIPT_TIMER stDelayShard
CONST_INT ciSYNC_RACE_COUNTDOWN_TIMER 3500

CONST_INT ciEntityHealthbarConfigBS_HideUnderPercentageLine		0
CONST_INT ciEntityHealthbarConfigBS_InvertPercentage			1

STRUCT ENTITY_HEALTHBAR_VARS

	ENTITY_INDEX eiEntity
	VECTOR vEntityCoords
	
	INT iEntityIndex
	INT iEntityType = ciENTITY_TYPE_NONE
	
	INT iOnlyDisplayOnRule = -1
	INT iOnlyDisplayOnRuleBS = -1
	INT iOnlyDisplayOnRule_Team = -1
	
	INT iEntityHealthbarConfigBS
	
	INT iDisplayInRange = -1
	PERCENTAGE_METER_LINE ePercentageMeterLine = PERCENTAGE_METER_LINE_NONE
	INT iPretendZeroIs = 0
	INT iBlipName = -1
	INT iCustomTitleCustomStringIndex = -1
	
	TEXT_LABEL_63 tlHealthbarTitle
	BOOL bIsLiteralString
	BOOL bIsPlayerName
	INT iPercentageToDisplay
	
	HUD_COLOURS iHealthbarColour = HUD_COLOUR_WHITE
	
	INT iFrameAdded = -1
	
	PED_INDEX piDriver
	//If you update this struct also update PROC RESET_ENTITY_HEALTHBAR_STRUCT(ENTITY_HEALTHBAR_VARS &sEntityHealthbarVars)
ENDSTRUCT

CONST_INT ciFMMC_MAX_ENTITY_HEALTHBARS 4
ENTITY_HEALTHBAR_VARS sHealthBars[ciFMMC_MAX_ENTITY_HEALTHBARS]
INT iCurrentHealthBars = 0

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Objective Text
// ##### Description: Vars and consts for the Objective Text system
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT OBJECTIVE_TEXT_DELAY				1250
CONST_INT OBJECTIVE_TEXT_DELAY_LONG			2000
CONST_INT OBJECTIVE_TEXT_DELAY_VERY_LONG	3000

CONST_INT ciOBJECTIVE_TEXT_DELAY_ALL		127 // = 2^ciOBJECTIVE_TEXT_DELAY_MAX - 1	(All bits set)
CONST_INT ciOBJECTIVE_TEXT_DELAY_VEHICLE	0
CONST_INT ciOBJECTIVE_TEXT_DELAY_PED		1
CONST_INT ciOBJECTIVE_TEXT_DELAY_OBJECT		2
CONST_INT ciOBJECTIVE_TEXT_DELAY_LOCATION	3
CONST_INT ciOBJECTIVE_TEXT_DELAY_PLAYER		4
CONST_INT ciOBJECTIVE_TEXT_DELAY_WANTED		5
CONST_INT ciOBJECTIVE_TEXT_DELAY_OVERRIDE	6
CONST_INT ciOBJECTIVE_TEXT_DELAY_MAX		7 // Update ciOBJECTIVE_TEXT_DELAY_ALL if changed

INT iObjectiveTextActiveDelayBitset = 0
INT iObjectiveTextRequestDelayBitset = 0
SCRIPT_TIMER tdObjectiveTextActiveDelayTimers[ciOBJECTIVE_TEXT_DELAY_MAX]

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Wanted
// ##### Description: Vars and consts for the wanted system
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INT iLocalWantedHighestPriority = -1
INT iLocalFakeWantedHighestPriority = -1
SCRIPT_TIMER tdWantedDelayTimer
SCRIPT_TIMER tdFakeWantedDelayTimer
INT iWantedVehBitset
INT iNewWantedThisFrame

CONST_INT ciFMMC_EASY_WANTED_TIME_DEFAULT 	60000
CONST_INT ciFMMC_EASY_WANTED_TIME_HARD 		120000
CONST_INT ciFMMC_EASY_WANTED_TIME_EASY 		45000
CONST_INT ciFMMC_EASY_WANTED_THRESHOLD 		2

CONST_INT ciFMMC_WANTED_REQUIREMENT_OFF		-2
CONST_INT ciFMMC_WANTED_REQUIREMENT_ANY		-1

INT iPlayerPedIgnoredWhenWanted
INT iPreviousWanted = -1
INT iFakeDisplayedWanted = -1
INT iPreviousRealDisplayedWanted = -1
INT iPreviousMaxDisplayedWanted = -1
INT iWantedBeforeDeath
SCRIPT_TIMER tdWantedDifficultyTimer

#IF IS_DEBUG_BUILD
INT iPreviousMaxWanted = -1
#ENDIF

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cached Vars
// ##### Description: Various variables that get cached either at the start of the mission or at the start of every frame
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INT iSpectatorTarget = -1
PLAYER_INDEX PlayerToUse
PLAYER_INDEX LocalPlayer
PED_INDEX PlayerPedToUse
PED_INDEX LocalPlayerPed
INT iPartToUse
INT iLocalPart
BOOL bPlayerToUseOK
BOOL bPedToUseOk
BOOL bLocalPlayerOK
BOOL bLocalPlayerPedOk
BOOL bIsLocalPlayerHost
BOOL bIsAnySpectator
BOOL bIsSCTV
VECTOR vLocalPlayerPosition

INTERIOR_INSTANCE_INDEX LocalPlayerCurrentInterior
INT iLocalPlayerCurrentInteriorHash = -1
INT iLocalPlayerCurrentRoomKey = -1
VECTOR vLocalPlayerCurrentInteriorPos

INT iCachedCutsceneTeam = -2 //GET_CUTSCENE_TEAM(FALSE)
INT iCachedCutsceneStreamingTeam = -2 //GET_CUTSCENE_TEAM(TRUE)

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Prerequisites
// ##### Description: Local variables for the Prerequisite system. 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CONST_INT ciMAX_RUNTIME_PREREQ_COUNTERS	5
STRUCT RUNTIME_PREREQ_COUNTER
	INT iDialogueTriggerIndex = -1
	INT iPreReqCount = 0
	INT iPreReqCount_Temp = 0
ENDSTRUCT
RUNTIME_PREREQ_COUNTER sActivePreReqCounterList[ciMAX_RUNTIME_PREREQ_COUNTERS]
INT iActiveRuntimePreReqCounters = 0

INT iPreReqCounterStaggeredBitIndex = 0

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Interactables
// ##### Description: Local variables for the Interactables system.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT ciINTERACTABLE_MAX_ONGOING_INTERACTIONS		8

CONST_INT ciONGOING_INTERACTION_BS__IS_BACKGROUND_INTERACTION							0
CONST_INT ciONGOING_INTERACTION_BS__CURRENT_LOOT_VEHICLE_INTERACTABLE_OPENING_TRUNK		1
CONST_INT ciONGOING_INTERACTION_BS__CASH_GRAB_COMPLETE									2

CONST_INT ciINTERACTION_SECONDARY_ANIM_STATE__LOADING			0
CONST_INT ciINTERACTION_SECONDARY_ANIM_STATE__PLAYING_INTRO		1
CONST_INT ciINTERACTION_SECONDARY_ANIM_STATE__PLAYING_LOOP		2
CONST_INT ciINTERACTION_SECONDARY_ANIM_STATE__PLAYING_EXIT		3

CONST_INT ciINTERACTION_MAX_PTFX	3
CONST_INT ciINTERACTION_PTFX__THERMAL_CHARGE_BURN		0
CONST_INT ciINTERACTION_PTFX__THERMAL_CHARGE_DRIP		1
CONST_INT ciINTERACTION_PTFX__THERMAL_CHARGE_SPARKS		2

STRUCT INTERACTION_VARS
	INT iInteractable = -1
	NETWORK_INDEX niInteractionSpawnedEntity
	PTFX_ID ptInteractionPTFX[ciINTERACTION_MAX_PTFX]
	SCRIPT_TIMER stInteractionTimer
	
	INT iOngoingInteractionBS
	
	INT iInteractable_LoopingSoundID = -1
	
	INT iInteractableSecondaryAnimState = ciINTERACTION_SECONDARY_ANIM_STATE__LOADING
ENDSTRUCT

INTERACTION_VARS sOngoingInteractionVars[ciINTERACTABLE_MAX_ONGOING_INTERACTIONS]

CONST_INT ciINTERACTABLE_EVENT_BS__RESET_SYNCLOCK_TIMER		0

CONST_INT ciSHARED_INTERACTABLE_BS__DISPLAYED_OBJECTIVE_TEXT_THIS_FRAME						0
CONST_INT ciSHARED_INTERACTABLE_BS__DISPLAYED_OBJECTIVE_TEXT_LAST_FRAME						1
CONST_INT ciSHARED_INTERACTABLE_BS__INPUT_BLOCKED_BY_INTERACT_WITH_POSITION					2
CONST_INT ciSHARED_INTERACTABLE_BS__BAIL_FOREGROUND_INTERACTION_DUE_TO_MULTI_INTERACT		3
CONST_INT ciSHARED_INTERACTABLE_BS__BAIL_FOREGROUND_INTERACTION_ASAP						4
CONST_INT ciSHARED_INTERACTABLE_BS__FREEZE_INTERACTION_COUNTERS_THIS_LOOP					5
INT iSharedInteractableBS

CONST_INT ciANY_INTERACTABLE	-1

INT iInteractableBailTime = -1
SCRIPT_TIMER stInteractableBailTimer

INT iClosestInteractable = -1 // The current closest Interactable, according to the results by the end of the last Interactable staggered loop
INT iClosestInteractable_Pending = -1 // The current closest Interactable (only to be used within the staggered loop)
FLOAT fCurrentPendingClosestInteractableDistance // The distance between the player and the interaction coords for iClosestInteractable_Pending

INT iInteractable_Temp_AvailableInteractionCount[ciInteractable_InteractionCounters]
INT iInteractable_AvailableInteractionCount[ciInteractable_InteractionCounters]
INT iInteractable_Temp_CompletedInteractionCount[ciInteractable_InteractionCounters]
INT iInteractable_CompletedInteractionCount[ciInteractable_InteractionCounters]

TEXT_LABEL_63 tlInteractableObjectiveText // A text label specified by either the interaction the player is currently doing (for more complicated, minigame-like Interactions) or simply the current closest Interactable if proximity objective text is set up

INT iInteractable_WasCompletedBS[ciINTERACTABLE_LONG_BS_LENGTH] // A long bitset to keep track of Interactables that have been completed at any point, even if they stopped being Completed later
INT iInteractable_StayCompleteForeverBS[ciINTERACTABLE_LONG_BS_LENGTH] // A long bitset to keep track of Interactables that should stay completed forever due to things that have happened during the mission such as temporary Interactables which are linked to a "stay complete forever" Interactable.
SCRIPT_TIMER stInteractableCompletedTimers[FMMC_MAX_NUM_INTERACTABLES] // Timers to keep track of how long an Interactable has been completed, for the cases where we want the Consequences to only last for so long
INT iInteractable_CompletionModelSwapRegisteredBS[ciINTERACTABLE_LONG_BS_LENGTH]

INT iInteractable_CachedAvailableBS[ciINTERACTABLE_LONG_BS_LENGTH] // This can be used to check if an Interactable only just became available or only just started being blocked by something (something other than the player's position/heading).

INT iInteractableHasBeenPlacedIntoInteriorBS[ciINTERACTABLE_LONG_BS_LENGTH]

CONST_INT ciInteractableUsageCooldownLength		2000
SCRIPT_TIMER stInteractableUsageCooldownTimer[FMMC_MAX_NUM_INTERACTABLES]

INT iInteractable_CachedLinkedInteractable[FMMC_MAX_NUM_INTERACTABLES]

INT iInteractable_OccupiedBS[ciINTERACTABLE_LONG_BS_LENGTH] // A local bitset assigned to over the course of the player loop as a quicker method of seeing if an Interactable is being used
INT iInteractable_TempOccupiedBS[ciINTERACTABLE_LONG_BS_LENGTH]

INT iInteractable_CompletedInteractionTypeBS
INT iInteractable_SentCompletionTimerExpiryEventBS[ciINTERACTABLE_LONG_BS_LENGTH]

STRING sInteractable_CachedSecondaryAnimDict = "" // Cached anim data to be used when cleaning up a secondary anim
INT iInteractable_CachedSecondaryAnimInteractable = -1
STRING sInteractable_CachedSecondaryExitAnimName = ""
STRING sInteractable_CachedSecondaryAnimFilter = ""

INT iInteractable_EMPChargesRequired = 0 // The number of Charge EMP consequences that need to be hit before the EMP is deployed.
INT iInteractable_EMPCauserIndex = -1

INT iInteractable_SyncLockEngagedBS[ciINTERACTABLE_LONG_BS_LENGTH]

SCRIPT_TIMER stSyncLockEngageTimer
CONST_INT ciSyncLockEngageTimeWindowDuration	3000 // The length of time allowed between players engaging their sync-locks in order to succeed
ENUM SYNCLOCK_INTERACTION_STATE
	SYNCLOCK_INTERACTION_STATE__OFF = 0,
	SYNCLOCK_INTERACTION_STATE__INIT = 1,
	SYNCLOCK_INTERACTION_STATE__IN_POSITION = 2,
	SYNCLOCK_INTERACTION_STATE__BACKING_OUT = 3,
	
	SYNCLOCK_INTERACTION_STATE__ENGAGE_INIT = 4,
	SYNCLOCK_INTERACTION_STATE__ENGAGE_ENTER = 5,
	SYNCLOCK_INTERACTION_STATE__ENGAGED_WAIT_FOR_RESULT = 6,
	
	SYNCLOCK_INTERACTION_STATE__SUCCESS = 7,
	SYNCLOCK_INTERACTION_STATE__FAIL = 8
ENDENUM

ENUM CUTPAINTING_INTERACTION_STATE
	CUTPAINTING_INTERACTION_STATE__OFF = 0,
	CUTPAINTING_INTERACTION_STATE__INIT = 1,
	
	CUTPAINTING_INTERACTION_STATE__IDLE = 2,
	CUTPAINTING_INTERACTION_STATE__CUTTING = 3,
	CUTPAINTING_INTERACTION_STATE__BACKING_OUT = 4,
	
	CUTPAINTING_INTERACTION_STATE__ROLLINGUP = 5
ENDENUM
CUTPAINTING_INTERACTION_STATE eCutPaintingInteractionState

CONST_INT ciTHERMAL_CHARGE_BURN_TIME					12000
CONST_INT ciTHERMAL_CHARGE_DIE_OFF_START_TIME			3000
CONST_INT ciTHERMAL_CHARGE_DIE_OFF_DRIP_TIME			4000
CONST_INT ciTHERMAL_CHARGE_DIE_OFF_SPARKS_TIME			2000
CONST_INT ciTHERMAL_CHARGE_WEAPON_REAPPEAR_DELAY		1500

CONST_FLOAT cfTHERMAL_CHARGE_COVER_EYES_DISTANCE		4.0
CONST_FLOAT cfTHERMAL_CHARGE_COVER_EYES_HEADING_RANGE	75.0

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Generic Render Targets
// ##### Description: Functions related to the shared render target system which can be used by all entity types.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CONST_INT ciSHARED_RT_STATE__OFF				-1
CONST_INT ciSHARED_RT_STATE__INIT_VARS			0
CONST_INT ciSHARED_RT_STATE__LOAD				1
CONST_INT ciSHARED_RT_STATE__REGISTER			2
CONST_INT ciSHARED_RT_STATE__LINK				3
CONST_INT ciSHARED_RT_STATE__PROCESS			4

CONST_INT ciSHARED_RT_EVENT_TYPE__NONE								-1
CONST_INT ciSHARED_RT_EVENT_TYPE__REGISTER_RENDERTARGET				0
CONST_INT ciSHARED_RT_EVENT_TYPE__PERFORM_SCALEFORM_METHOD			1
CONST_INT ciSHARED_RT_EVENT_TYPE__GENERIC_START						2
CONST_INT ciSHARED_RT_EVENT_TYPE__GENERIC_COMPLETE					3

CONST_INT ciSHARED_RT_EVENT_METHOD__NONE										-1
CONST_INT ciSHARED_RT_EVENT_METHOD__NAS_MONITOR_SHOW_DEVICE_DETECTED_SCREEN		0

CONST_INT ciSHARED_RT_MAX	10

CONST_INT ciSHARED_RT_BITSET__GENERIC_STARTED							0
CONST_INT ciSHARED_RT_BITSET__GENERIC_COMPLETED							1
CONST_INT ciSHARED_RT_BITSET__FEATURES_FILLABLE_BAR						2
CONST_INT ciSHARED_RT_BITSET__FEATURES_TIMER							3
CONST_INT ciSHARED_RT_BITSET__DONT_CARE_IF_ON_SCREEN					4

STRUCT SHARED_RT_VARS
	INT iEntityType = CREATION_TYPE_NONE
	INT iEntityIndex = -1
	
	MODEL_NAMES mnRenderTargetModel = DUMMY_MODEL_FOR_SCRIPT
	INT iSharedRT_ID = -1
	INT iSharedRT_State = ciSHARED_RT_STATE__OFF
	
	STRING sTextureDict
	STRING sTextureName
	STRING sRenderTargetName
	
	STRING sScaleformName
	SCALEFORM_INDEX siScaleformIndex
	
	FLOAT fCentreX = 0.5, fCentreY = 0.5
	FLOAT fWidth = 1.0, fHeight = 1.0
	
	INT iSharedRTBitset
	
	INT iSplitRenderTargetIndex = 0
	
	STRING sInitMethodName
	
	STRING sUpdateFillableBarMethodName
	STRING sProgressBarTitle
	
	STRING sUpdateTimerMethodName
	
	INT iMatchInteriorDestructionTimerIndex = -1
ENDSTRUCT
SHARED_RT_VARS sSharedRTs[ciSHARED_RT_MAX]
INT iCurrentSharedRTs = 0

CONST_INT ciSharedRT_MaxRenderTargetSplits	4

// Particular Render Target Vars
CONST_FLOAT cfSharedRT_FillBarSpeed	 	0.01
FLOAT fSharedRT_FillBarProgress = 0.0

INT iSharedRT_TimerTimeToDisplay[ciSharedRT_MaxRenderTargetSplits]

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Model Swaps
// ##### Description: 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
ENUM MODEL_SWAP_ENTITY_TYPE
	MODEL_SWAP_ENTITY_TYPE__INVALID,
	MODEL_SWAP_ENTITY_TYPE__PROP,
	MODEL_SWAP_ENTITY_TYPE__NETWORKED_OBJECT,
	MODEL_SWAP_ENTITY_TYPE__MODEL_SWAP_PROP
ENDENUM

CONST_INT ciMODEL_SWAP_BS__COLLISION_ONLY		0

STRUCT FMMC_MODEL_SWAP_ENTITY_DATA
	MODEL_SWAP_ENTITY_TYPE eModelSwapType = MODEL_SWAP_ENTITY_TYPE__INVALID
	NETWORK_INDEX niEntityNetID // The network ID of the networked entity we're swapping in or out, used for networked entities only
	INT iEntityIndex = -1 // The entity's index inside whichever array is relevant as dictated by MODEL_SWAP_ENTITY_TYPE, used for non-networked entities only
	MODEL_NAMES mnModelSwapPropModelName = DUMMY_MODEL_FOR_SCRIPT // The model name of the non-networked prop we'll swap in, only to be used for MODEL_SWAP_ENTITY_TYPE__MODEL_SWAP_PROP entities
ENDSTRUCT

ENUM MODEL_SWAP_STATE
	MODEL_SWAP_STATE__INVALID = 0,
	MODEL_SWAP_STATE__PREPARING = 1,
	MODEL_SWAP_STATE__READY = 2,
	MODEL_SWAP_STATE__SWAPPING_REVEAL = 3,
	MODEL_SWAP_STATE__SWAPPING_HIDE = 4,
	MODEL_SWAP_STATE__COMPLETE = 5
ENDENUM

STRUCT FMMC_MODEL_SWAP_CONFIG
	INT iModelSwapHash = -1
	INT iModelSwapBS
	FMMC_MODEL_SWAP_ENTITY_DATA sEntityToSwapOut
	FMMC_MODEL_SWAP_ENTITY_DATA sEntityToSwapIn
ENDSTRUCT

STRUCT FMMC_MODEL_SWAP_SLOT
	FMMC_MODEL_SWAP_CONFIG sModelSwapConfig // Data about what to swap out and what to swap in
	MODEL_SWAP_STATE eModelSwapState = MODEL_SWAP_STATE__INVALID
	BOOL bPerformModelSwapNow = FALSE
	BOOL bReverse = FALSE
	
	OBJECT_INDEX oiModelSwapProp_SwapOut = NULL // An object index slot we can use if we're creating a brand new non-networked prop to swap to
	OBJECT_INDEX oiModelSwapProp_SwapIn = NULL // An object index slot we can use if we're creating a brand new non-networked prop to swap to
ENDSTRUCT

CONST_INT ciMAX_MODEL_SWAPS		10
INT iCurrentActiveModelSwaps = 0
FMMC_MODEL_SWAP_SLOT sActiveModelSwaps[ciMAX_MODEL_SWAPS]

ENUM MODEL_SWAP_EVENT_TYPE
	MODEL_SWAP_EVENT_TYPE__ADD,
	MODEL_SWAP_EVENT_TYPE__PERFORM,
	MODEL_SWAP_EVENT_TYPE__REVERSE
ENDENUM

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Props
// ##### Description: 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CONST_INT ciMAX_EVERY_FRAME_PROPS 32 
INT iEveryFramePropIndices[ciMAX_EVERY_FRAME_PROPS]
INT iNumEveryFrameProps = 0

SCRIPT_TIMER stReadyToCreatePropBailTimer
CONST_INT ciReadyToCreatePropBailTimer_Length		25000

INT iPropStartedSlidingOutBS[FMMC_MAX_PROP_BITSET]
INT iPropSlidingCompleteBS[FMMC_MAX_PROP_BITSET]

CONST_INT ciMAX_SLIDING_PROP_SOUND_IDS	3
INT iSlidingPropSoundIDs[ciMAX_SLIDING_PROP_SOUND_IDS]
INT iSlidingPropSoundIDUserIndex[ciMAX_SLIDING_PROP_SOUND_IDS]

STRUCT SLIDING_PROP_SOUNDS
	STRING sSlidingOutLoop
	STRING sSlidingBackLoop
	STRING sSlideComplete
	STRING sSlideReturned
	
	STRING sSlideSoundset
ENDSTRUCT

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Blips
// ##### Description: Blip declarations
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

BLIP_INDEX biPedBlip[FMMC_MAX_PEDS]
BLIP_INDEX biVehBlip[FMMC_MAX_VEHICLES]
BLIP_INDEX biTrainBlip[FMMC_MAX_TRAINS]
BLIP_INDEX biObjBlip[FMMC_MAX_NUM_OBJECTS]
BLIP_INDEX biInteractableBlip[FMMC_MAX_NUM_INTERACTABLES]
BLIP_INDEX biPickup[FMMC_MAX_WEAPONS]
BLIP_INDEX LocBlip[FMMC_MAX_GO_TO_LOCATIONS]
BLIP_INDEX LocBlip1[FMMC_MAX_GO_TO_LOCATIONS]
BLIP_INDEX HomeBlip[FMMC_MAX_TEAMS]
BLIP_INDEX biZoneBlips[FMMC_MAX_NUM_ZONES]
BLIP_INDEX biDynoPropBlips[FMMC_MAX_NUM_DYNOPROPS]
BLIP_INDEX DummyBlip[FMMC_MAX_DUMMY_BLIPS]
BLIP_INDEX biCaptureRangeBlip[FMMC_MAX_PEDS]
BLIP_INDEX biVehCaptureRangeBlip[FMMC_MAX_VEHICLES]
BLIP_INDEX biObjCaptureRangeBlip[FMMC_MAX_NUM_OBJECTS]
BLIP_INDEX biTaggedEntity[FMMC_MAX_TAGGED_ENTITIES]
BLIP_INDEX biTrapCamBlip[FMMC_MAX_NUM_DYNOPROPS]
BLIP_INDEX biModshopBlip
BLIP_INDEX DeliveryBlip
BLIP_INDEX DestinationBlip
BLIP_INDEX blipSafeProp
BLIP_INDEX MaskShopBlip
BLIP_INDEX biCopDecoyBlip
BLIP_INDEX biDronePickup
BLIP_INDEX biRegenBlip		//Team Regen
BLIP_INDEX biLocateRadiusBlip
BLIP_INDEX biSpawnVehicle
BLIP_INDEX SuddenDeathAreaBlip
BLIP_INDEX SuddenDeathAreaRadiusBlip

// Ped blips
INT iPedBlipCachedNamePriority[FMMC_MAX_PEDS]
AI_BLIP_STRUCT biHostilePedBlip[FMMC_MAX_PEDS]
SCRIPT_TIMER tdPedBlipFlash

// Vehicle blips
CONST_INT BIT_FLASH_VEH_BLIP_RESYNC_REQUESTED		0
CONST_INT BIT_FLASH_VEH_BLIP_FLASHING_STOPPED		1
CONST_INT ci_VEHICLE_BLIP_RULE_ALWAYS_OFF -1
CONST_INT ci_VEHICLE_BLIP_RULE_ALWAYS_ON 0
CONST_INT ci_VEHICLE_IGNORE_BLIP_CREATION_BS_MAX 2
INT iVehBlipCachedNamePriority[FMMC_MAX_VEHICLES]
INT iVehBlipCreatedThisFrameBitset
INT iVehPhotoChecksBS
INT iVehBlipFlashingBitset
INT iVehBlipFlashingState
SCRIPT_TIMER tdVehicleBlipFlash[FMMC_MAX_VEHICLES]
INT iVehBlipFlashActiveBS
INT iVehBlipFlashExpiredBS
INT iVehOccupiedBS
INT iVehIgnoreBlipCreationBS[ci_VEHICLE_IGNORE_BLIP_CREATION_BS_MAX]

// Object blips
INT iObjBlipCachedNamePriority[FMMC_MAX_NUM_OBJECTS]
INT iObjectBlipAlpha[FMMC_MAX_NUM_OBJECTS]

// Pickup & Location blips
INT ioldLocOwner[FMMC_MAX_GO_TO_LOCATIONS]
INT iBlipTeam[FMMC_MAX_GO_TO_LOCATIONS]

// Dummy Blips
INT iDummyBlipCurrentOverride = -1
CONST_INT ciDUMMY_BLIP_RULE_ANY -2
CONST_INT ciDUMMY_BLIP_RULE_OFF -1
INT iDummyBlipBitset[FMMC_MAX_DUMMY_BLIPS]
CONST_INT ciDUMMY_BLIP_HIDE_FLYING_ALTITUDE	20

CONST_INT ciDBBS_Remove_On_Hide	0

// Home / Mod Shop / Delivery blips
INT iDeliveryBlip_veh = -1
INT iDelblipPriority

// Safe blips
INT iSafeBlippedProp = -1

// Drone blips
OBJECT_INDEX oiDronePickup
VECTOR 		 vDronePosCache

// Location blips
INT iCurrentLocateBlipped
SCRIPT_TIMER tdRadiusBlipAlphaTimer
SCRIPT_TIMER tdPassOutOfRangeTimer

// Spawn Vehicle blips
VEHICLE_INDEX vehSpawnVehicle
BOOL bManageSpawnVehicleBlip = FALSE

CONST_INT ciEntitySpawnSearchType_Peds			0
CONST_INT ciEntitySpawnSearchType_Vehicles		1
CONST_INT ciEntitySpawnSearchType_Objs			2
CONST_INT ciEntitySpawnSearchType_GangChase		3
CONST_INT ciEntitySpawnSearchType_VehicleRepair	4

CONST_INT ciBLIP_FLASH_TIME_DEFAULT 7000

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Spawn Group and Entity Spawning
// ##### Description: 
// ##### This data has been set up to support caching the result of a spawn group global data to entity comparison.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INT iSpawnGroupBS_CheckedSpawnPed[FMMC_MAX_PEDS_BITSET]
INT iSpawnGroupBS_CheckedSpawnVeh
INT iSpawnGroupBS_CheckedSpawnObj
INT iSpawnGroupBS_CheckedSpawnProp[FMMC_MAX_PROP_BITSET]
INT iSpawnGroupBS_CheckedSpawnDynoProp
INT iSpawnGroupBS_CheckedSpawnLoc
INT iSpawnGroupBS_CheckedSpawnWep[FMMC_MAX_WEAPON_BITSET]
INT iSpawnGroupBS_CheckedSpawnDummyBlip
INT iSpawnGroupBS_CheckedSpawnInteractable[FMMC_MAX_INTERACTABLE_BITSET]
INT iSpawnGroupBS_CheckedSpawnTeamSpawnPoint[FMMC_MAX_TEAMS][FMMC_MAX_TEAM_SPAWN_POINT_BITSET]
INT iSpawnGroupBS_CheckedSpawnDialogueTrigger[FMMC_MAX_TEAMS][FMMC_MAX_DIALOGUE_TRIGGER_BITSET]
INT iSpawnGroupBS_CheckedSpawnZone[FMMC_ZONES_LONG_BS_LENGTH]
INT iSpawnGroupBS_CheckedSpawnTrain
INT iSpawnGroupBS_CheckedSpawnQRCPoint

INT iSpawnGroupBS_CanSpawnPed[FMMC_MAX_PEDS_BITSET]
INT iSpawnGroupBS_CanSpawnVeh
INT iSpawnGroupBS_CanSpawnObj
INT iSpawnGroupBS_CanSpawnProp[FMMC_MAX_PROP_BITSET]
INT iSpawnGroupBS_CanSpawnDynoProp
INT iSpawnGroupBS_CanSpawnLoc
INT iSpawnGroupBS_CanSpawnWep[FMMC_MAX_WEAPON_BITSET]
INT iSpawnGroupBS_CanSpawnDummyBlip
INT iSpawnGroupBS_CanSpawnInteractable[FMMC_MAX_INTERACTABLE_BITSET]
INT iSpawnGroupBS_CanSpawnTeamSpawnPoint[FMMC_MAX_TEAMS][FMMC_MAX_TEAM_SPAWN_POINT_BITSET]
INT iSpawnGroupBS_CanSpawnDialogueTrigger[FMMC_MAX_TEAMS][FMMC_MAX_DIALOGUE_TRIGGER_BITSET]
INT iSpawnGroupBS_CanSpawnZone[FMMC_ZONES_LONG_BS_LENGTH]
INT iSpawnGroupBS_CanSpawnTrain
INT iSpawnGroupBS_CanSpawnQRCPoint

INT iSpawnPoolPedIndex = -1
INT iSpawnPoolVehIndex = -1
INT iSpawnPoolObjIndex = -1
INT iSpawnPoolInteractableIndex = -1
INT iSpawnPoolLocIndex = -1

INT iSpawnGroupMightSpawnLaterBS
INT iSpawnSubGroupMightSpawnLaterBS[ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS]

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entity Spawning
// ##### Description: Vars for managing the spawning of entities
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT ciFMMC_MAX_PER_FRAME_REQUESTS 		12
CONST_INT ciFMMC_MAX_PER_FRAME_REQUESTS_AIRLOCK 5
CONST_INT ciFMMC_MAX_PER_FRAME_CREATES 			6
CONST_INT ciFMMC_MAX_PER_FRAME_CREATES_AIRLOCK 	2

INT iEntitiesSpawnedThisFrame = 0
CONST_INT ciFMMC_MAX_ENTITIES_SPAWNED_PER_FRAME 1

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entity Cached Vars
// ##### Description: Vars for caching entity values
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
VECTOR vGoToLocateVectors[FMMC_MAX_GO_TO_LOCATIONS]
FLOAT fGoToLocateSize[FMMC_MAX_GO_TO_LOCATIONS]
FLOAT fGoToLocateHeight[FMMC_MAX_GO_TO_LOCATIONS]

INT iLocalPlayerInLocationBitset
INT iLocalPlayerNotInLocationBitset

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Gang chase / backup
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT SBBBOOL_RESERVATION_COMPLETE			0
CONST_INT SBBBOOL_CREATED						1
CONST_INT SBBBOOL_SPAWNING_FORWARDS				2
CONST_INT SBBBOOL_USES_COP_BLIPS				6
CONST_INT SBBBOOL_AIR_VEHICLE_IS_FROZEN			7
CONST_INT SBBBOOL_ON_GOTO_TASK					8
CONST_INT SBBBOOL_DESPAWN_ON_NEW_TYPE			9
CONST_INT SBBBOOL_SELECTED_GANG_TYPE			10
CONST_INT SBBBOOL_ATTACKING_PED_TARGET			11

CONST_INT ciMAX_GANG_CHASE_VEHICLES 			5
CONST_INT ciMAX_GANG_CHASE_PEDS 				20

CONST_INT ciGANG_CHASE_TARGET_NONE				-1

CONST_INT ciMIN_GANG_CHASE_VARIABLE_BACKUP 		1

CONST_INT ciGANG_CHASE_NEARBY_PED_ARRAY_SIZE	8
CONST_INT ciGANG_CHASE_NAVMESH_REQUIRED_PEDS	5

CONST_INT ciGANG_CHASE_PED_ACCURACY_EASY 		2
CONST_INT ciGANG_CHASE_PED_ACCURACY_NORMAL 		5
CONST_INT ciGANG_CHASE_PED_ACCURACY_HARD 		10

CONST_INT ciGANG_CHASE_PED_BASE_HEALTH 			200
CONST_INT ciGANG_CHASE_PED_MIN_HEALTH 			100 //Peds die at 100

CONST_INT ciGANG_CHASE_PED_AMMO					25000

CONST_INT ciGANG_CHASE_PLAYER_VAR_INTENSITY				-1
CONST_INT ciGANG_CHASE_PLAYER_VAR_PLUS_ONE_INTENSITY	-2

CONST_INT ciGANG_CHASE_MAX_TOTAL_SPAWNS			HIGHEST_INT
CONST_INT ciGANG_CHASE_DEFAULT_INTENSITY		3
CONST_INT ciGANG_CHASE_SPAWN_INTERVAL			1500

CONST_INT ciGANG_CHASE_TIMED_CLEANUP_TIME		10000

CONST_INT ciGANG_CHASE_FORWARD_SPAWN_ATTEMPTS	5

CONST_FLOAT cfGANG_CHASE_COMBAT_TASK_DISTANCE				250.0

CONST_FLOAT cfGANG_CHASE_VEHICLE_HEADING_TOLERANCE			90.0
CONST_FLOAT cfGANG_CHASE_VEHICLE_SPEED_FORWARDS				20.0
CONST_FLOAT cfGANG_CHASE_VEHICLE_SPEED_BACKWARDS			5.0
CONST_FLOAT cfGANG_CHASE_VEHICLE_ENGINE_DAMAGE_THRESHOLD	400.0

CONST_FLOAT cfGANG_CHASE_PED_ACCURACY_MOD					0.25
CONST_FLOAT cfGANG_CHASE_PED_HEALTH_MOD						0.5
CONST_FLOAT cfGANG_CHASE_PED_RANGE							299.0

CONST_FLOAT cfGANG_CHASE_NAVMESH_PED_RANGE					50.0
CONST_FLOAT cfGANG_CHASE_NAVMESH_RADIUS						125.0

CONST_FLOAT cfGANG_CHASE_ROTATION_AMOUNT_ON_FAIL			20.0

INT iSearchingForGangChase = -1 
VECTOR vGangChaseSearchPos

STRUCT GANG_CHASE_UNIT_CONFIG_DATA
	INT iTeam
	INT iRule
	INT iMaxPeds
	INT iGangType
	VECTOR vPosition
	INT iTargetPart
	PED_INDEX piTargetPed
	WEAPON_TYPE wtWeapon
	MODEL_NAMES mnVehicleModel
	MODEL_NAMES mnPedModel
	BOOL bSpawnForwards
	BOOL bTriedForwards
	BOOL bSpawnBehind
	VEHICLE_INDEX viVehicle
	FLOAT fPedArmour
	BOOL bUseAlternateComponentsForPedOutfit
	INT iOverrideVehicleColour1 = -1
	INT iOverrideVehicleColour2 = -1
	INT iVehicleModPresetToUse = -1
	INT iVehicleLivery 	= -1
ENDSTRUCT

CONST_INT ciGANG_CHASE_TARGET_TIMER 			3000

CONST_INT ciGANG_CHASE_UNIT_STATE_INIT 				0
CONST_INT ciGANG_CHASE_UNIT_STATE_FIND_TARGET 		1
CONST_INT ciGANG_CHASE_UNIT_STATE_WAIT_FOR_CREATION 2
CONST_INT ciGANG_CHASE_UNIT_STATE_ACTIVE			3

CONST_INT ciGANG_CHASE_UNIT_SPAWN_ATTEMPTS			10

STRUCT GANG_CHASE_DATA
	NETWORK_INDEX niVeh
	NETWORK_INDEX niPeds[ciMAX_GANG_CHASE_PEDS_PER_VEHICLE]
	MODEL_NAMES mnPedModelNames[ciMAX_GANG_CHASE_PEDS_PER_VEHICLE]
	INT iGangChaseState
	INT iTargetPart = ciGANG_CHASE_TARGET_NONE
	INT iSpawnTeam = -1
	INT iBitset
	INT iGangType = ciBACKUP_TYPE_NONE
	INT iFleeDist = 200
	INT iMaxPeds = 0
	SCRIPT_TIMER tdCleanupTimer //Cleanup immediately when outside 300m, or within 10s if outside 200m (if on a rule that uses the new forward spawning system)
ENDSTRUCT

INT iGangChaseUnitIterator
AI_BLIP_STRUCT biGangChasePedBlip[ciMAX_GANG_CHASE_PEDS]
VECTOR vGangChaseSpawnDirection
INT iGangChaseForwardSpawnAttempts
INT iGangChaseTotalSpawnAttempts
INT iGangChaseAirStandardSpawnAttempts
SCRIPT_TIMER tdGangChaseDelay[FMMC_MAX_TEAMS]
SCRIPT_TIMER tdGangChaseTargetTimer[ciMAX_GANG_CHASE_PEDS]
SCRIPT_TIMER tdGangChaseSpawnIntervalTimer[FMMC_MAX_TEAMS]
INT iOldGangChaseTimerPriority[FMMC_MAX_TEAMS]
INT iGangChasePedCollisionBitset
INT iGangChasePedNavmeshCheckedBS
INT iGangChasePedNavmeshLoadedBS
FLOAT fGangChasePedAccuracyMod = cfGANG_CHASE_PED_ACCURACY_MOD
FLOAT fGangChasePedHealthMod = cfGANG_CHASE_PED_HEALTH_MOD

#IF IS_DEBUG_BUILD
BOOL bDebugGangChaseActiveForMyTeam = FALSE
INT iDebugGangChaseIntensity = 0
INT iDebugGangChaseTotalSpawns = 0
INT iDebugGangChaseUnitDeadBS = 0
NETWORK_INDEX niDebugGangChaseTargetEntity[ciMAX_GANG_CHASE_VEHICLES]
#ENDIF

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Trackify
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT MAX_TRACKIFY_TARGETS 20
INT iTrackifyProgress
INT iTrackifyTargets[MAX_TRACKIFY_TARGETS]
INT iTrackifyTargetProcessed = 0
INT iNumberOfTrackifyTargets = 0
INT iAcquiredTargets
INT iTrackifyUpdateTimer
VECTOR vTrackifyVector[MAX_TRACKIFY_TARGETS]
VECTOR vTrackifyPlayerCoords
BOOL bTrackObjects 
BOOL bTrackifyDisabled
VEHICLE_INDEX vehTrackifyTarget[MAX_TRACKIFY_TARGETS]
OBJECT_INDEX objTrackifyTarget[MAX_TRACKIFY_TARGETS]
WEAPON_TYPE wtTrackifyCache = WEAPONTYPE_UNARMED

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Minigames
// ##### Description: Timers, variables, enums, structs and consts relating to various Minigames.
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
INT iObjectCachedHackingStructIndexes[FMMC_MAX_NUM_OBJECTS]
INT iInteractableCachedHackingStructIndexes[FMMC_MAX_NUM_INTERACTABLES]

// Beamhack
HACKING_CONTROLLER_STRUCT sBeamHack[ciMAX_HACKING_STRUCTS_BEAMHACK]

// Hotwire
HACKING_CONTROLLER_STRUCT sHotwire[ciMAX_HACKING_STRUCTS_DEFAULT]

// Voltage
HG_CONTROL_STRUCT sVoltageHack[ciMAX_HACKING_STRUCTS_VOLTAGE]
HACKING_GAME_STRUCT sVoltageGameplay
INT iNumberOfTimesExitedVoltage = 0
INT iNumberOfTimesFailedVoltage = 0

// Fingerprint
HG_CONTROL_STRUCT sFingerprintClone[ciMAX_HACKING_STRUCTS_FINGERPRINT]
FINGERPRINT_MINIGAME_GAMEPLAY_DATA sFingerprintCloneGameplay
INT iFingerprintClone_Exits = 0
INT iFingerprintClone_Fails = 0
INT iFingerprintClone_Wins = 0
INT iFingerprintClone_PatternDone = 0
INT iFingerprintClone_LivesLost = 0
INT iFingerprintClone_LocalFails[ciMAX_HACKING_STRUCTS_FINGERPRINT]
INT iFingerprintClone_LessPatternsDueToFailure[ciMAX_HACKING_STRUCTS_FINGERPRINT]

// Order Unlock
HG_CONTROL_STRUCT sOrderUnlock[ciMAX_HACKING_STRUCTS_DEFAULT]
ORDER_UNLOCK_GAMEPLAY_DATA	sOrderUnlockGameplay

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Minigames: Interact-With
// ##### Description: Timers, variables, enums, structs and consts relating to 'Interact-With' Minigames.
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

ENUM INTERACT_WITH_STATE
	IW_STATE_IDLE,
	IW_STATE_INIT,
	IW_STATE_WALKING,
	IW_STATE_TURNING,
	IW_STATE_PREPARING_ANIMATION,
	IW_STATE_STARTING_ANIMATION,
	IW_STATE_PERFORMING_ENTER_ANIMATION,
	IW_STATE_PERFORMING_LOOPING_ANIMATION,
	IW_STATE_PERFORMING_EXIT_ANIMATION,
	IW_STATE_COMPLETED,
	IW_STATE_CLEANUP_WHEN_READY,
	
	// SPECIAL CUSTOM STATES
	IW_STATE_CUSTOM__DOWNLOADING,
	IW_STATE_CUSTOM__PLANT_VAULT_DOOR_EXPLOSIVES,
	IW_STATE_CUSTOM__YACHT_CUTSCENE
ENDENUM // IF YOU ADD A NEW STATE, ADD IT TO GET_IW_STATE_NAME

ENUM INTERACT_WITH_ANIM_STAGE
	IW_ANIM_STAGE__ENTER,
	IW_ANIM_STAGE__LOOPING,
	IW_ANIM_STAGE__EXIT
ENDENUM

CONST_INT ciINTERACT_WITH_DOWNLOAD_PAUSE_AT_PERCENT		99
CONST_INT ciINTERACT_WITH_DOWNLOAD_PAUSE_TIME			1656
CONST_INT ciINTERACT_WITH_DOWNLOAD_STUTTER_AMOUNT		40
CONST_INT ciINTERACT_WITH_DOWNLOAD_START_STUTTERING_AT	80
CONST_INT ciINTERACT_WITH_DOWNLOAD_SPEED 				25

FLOAT fInteractWith_DownloadPercentage = 0
INT iInteractWith_DownloadSndID
INT iInteractWith_DownloadSndID_Remote

INT iInteractObject_AttachedVehicle[FMMC_MAX_NUM_NETWORKED_OBJ_POOL]

INT iObjPaintingForceLeftHandBS
INT iObjPaintingForceRightHandBS

INT iInteractablePaintingForceLeftHandBS
INT iInteractablePaintingForceRightHandBS

INT iPedInvolvedInInteractWithBS[FMMC_MAX_PEDS_BITSET]

INT iCleanVehicleDamageRecordBS[FMMC_MAX_VEHICLE_BITSET]

//FLOAT fPaintingCamBlendoutPhaseStart = 1.0
//FLOAT fPaintingCamBlendoutPhaseEnd = 1.0

CONST_INT ciInteractWith_Painting_Enter_TLE_CAM			0
CONST_INT ciInteractWith_Painting_Enter_TRE_CAM			1
CONST_INT ciInteractWith_Painting_Enter_BRE_CAM			2
CONST_INT ciInteractWith_Painting_Enter_BLE_CAM			3
CONST_INT ciInteractWith_Painting_Enter_RE_CAM			4

CONST_INT ciInteractWith_EMPUploadTime					3500

CONST_INT ciInteractWith_BailTime						30000
CONST_INT ciInteractWith_SkipWalkTime					6000
CONST_INT ciInteractWith_CutsceneLeadInBufferTime		8000
CONST_INT ciInteractWith_CutsceneLeadInBailTime			15000

CONST_INT ciInteractWith_PostAnimResetFrameDelay		7
CONST_INT ciInteractWith_MeterFillDuration 				6000

CONST_FLOAT cfInteractWith__NearbyPedDoWalkDistance		1.0

CONST_FLOAT cfInteractWith__InteractionAllowedMaxHeadingDifference						80.0
CONST_FLOAT cfInteractWith__TurningStateMaxHeadingDifference_SyncScene					10.0
CONST_FLOAT cfInteractWith__TurningStateMaxHeadingDifference_SyncScene_ExtraWide		35.0
CONST_FLOAT cfInteractWith__TurningStateMaxHeadingDifference_SyncScene_Huge				250.0
CONST_FLOAT cfInteractWith__TurningStateMaxHeadingDifference_NonSyncScene				20.0

ENUM HOLD_BUTTON_INTERACT_STATE
	HOLD_BUTTON_STATE_WAITING,
	HOLD_BUTTON_STATE_REACT_START,
	HOLD_BUTTON_STATE_REACT_LOOP,
	HOLD_BUTTON_STATE_RETURN_LOOP
ENDENUM

CONST_INT ciInteractWith_RenderTarget_PhoneHackA	0
CONST_INT ciInteractWith_MaxRenderTargets			1

CONST_INT ciInteractWith_Bitset__QuickBlend									0
CONST_INT ciInteractWith_Bitset__BrokeOut									1
CONST_INT ciInteractWith_Bitset__SlowBlendIn								2
CONST_INT ciInteractWith_Bitset__SlowBlendOut								3
CONST_INT ciInteractWith_Bitset__SkippedTransition							4
CONST_INT ciInteractWith_Bitset__HidPlayerWeaponForEndCutscene				5
CONST_INT ciInteractWith_Bitset__UsingHeistBag								6
CONST_INT ciInteractWith_Bitset__ReappliedHeistBag							7
CONST_INT ciInteractWith_Bitset__StartingQueuedSubAnim						8
CONST_INT ciInteractWith_Bitset__RecallInteractWith							9
CONST_INT ciInteractWith_Bitset__BailASAP									10
CONST_INT ciInteractWith_Bitset__HideHUD									11
CONST_INT ciInteractWith_Bitset__NeedToRefreezeInteractWithEntity			12
CONST_INT ciInteractWith_Bitset__InteractWithEntityIsInPostAnimState		13
CONST_INT ciInteractWith_Bitset__InteractWithDynopropIsInPostAnimState		14
CONST_INT ciInteractWith_Bitset__ClearedDecals								15
CONST_INT ciInteractWith_Bitset__StartedInputGait							16
CONST_INT ciInteractWith_Bitset__HideObjectiveTextForThisAnim				17
CONST_INT ciInteractWith_Bitset__SetToLeavePedBehindForLeadIn				18
CONST_INT ciInteractWith_Bitset__SentExternalRenderTargetEventA				19
CONST_INT ciInteractWith_Bitset__ReceivedSpecialCaseAnimEventA				20
CONST_INT ciInteractWith_Bitset__InteractWithLinkedObjectIsInPostAnimState0	21
CONST_INT ciInteractWith_Bitset__InteractWithLinkedObjectIsInPostAnimState1	22
CONST_INT ciInteractWith_Bitset__InteractWithLinkedObjectIsInPostAnimState2	23
CONST_INT ciInteractWith_Bitset__InteractWithLinkedObjectIsInPostAnimState3	24

STRUCT INTERACT_WITH_VARS
	
	INTERACT_WITH_STATE eInteractWith_CurrentState = IW_STATE_IDLE // The state of the entire system
	INTERACT_WITH_ANIM_STAGE eInteractWith_CurrentAnimStage = IW_ANIM_STAGE__ENTER // The anim stage we're currently at (Enter, Looping or Exit)
	
	INT iInteractWith_LastFrameProcessed = -1
	
	TEXT_LABEL_15 tlInteractWith_Object
	INT iInteractWith_HelptextGiverIndex = -1
	INT iInteractWith_LastFrameHelptextGiverIndex = -1
	INT iInteractWith_CachedObjectiveObjIndex = -1
	OBJECT_INDEX oiInteractWith_CachedInteractWithEntity
	
	INT iInteractWith_SubAnimPreset = ciIW_SUBANIM__STANDARD
	INT iInteractWith_LastPlayedSubAnimPreset = -1
	INT iInteractWith_NextSubAnimPreset = -1
	STRING sInteractWith_SubEnterAnim
	STRING sInteractWith_SubLoopingAnim
	STRING sInteractWith_SubExitAnim
	
	INT iInteractWith_Bitset
	
	INT iInteractWith_AudioLoopID = -1
	INT iInteractWith_AudioBS = 0
	
	WEAPON_TYPE wtInteractWith_StoredWeapon = WEAPONTYPE_PISTOL
	
	SCRIPT_TIMER tdInteractWith_AnimationTimer
	SCRIPT_TIMER stInteractWith_BailTimer
	INT iInteractWith_CutsceneLeadInBailTimeStamp
	INT iInteractWith_StartInteractionImmediately = -1
	
	ENTITY_INDEX eiInteractWith_CachedSceneAlignmentEntities[FMMC_MAX_NUM_NETWORKED_OBJ_POOL]
	ENTITY_INDEX eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity
	
	INT iInteractWith_ObjectiveTextToUse = ciObjectiveText_Primary
	INT iInteractWith_LastDisplayedObjText = ciObjectiveText_Primary
	
	// Camera
	CAMERA_INDEX camInteractWith_Camera
	INTERACT_WITH_CAMERA_STATE eInteractWith_CameraState
	
	// PTFX
	PTFX_ID piInteractWith_PTFX[ciINTERACT_WITH__MAX_SPAWNED_PTFX]
	
	// Temp Vars
	INT iInteractWith_CachedAnimPreset = ciINTERACT_WITH_PRESET__NONE
	INTERACT_WITH_ANIM_ASSETS sIWAnims
	OBJECT_INDEX oiInteractWith_NearbyDynopropsUsed[ciINTERACT_WITH__MAX_NEARBY_DYNOPROP_ANIMS]
	PED_INDEX piInteractWith_NearbyPedUsed
	
	STRING sInteractWith_FailsafeDictionary
	STRING sInteractWith_FailsafeAnimName
	STRING sInteractWith_AudioBankName
	
	// Sync Scene vars
	VECTOR vInteractWith_PlayerStartPos
	VECTOR vInteractWith_PlayerStartRot
	FLOAT fInteractWith_RequiredHeading
	
	VECTOR vInteractWith_CachedSyncScenePos
	VECTOR vInteractWith_CachedSyncSceneRot
	
	VECTOR vInteractWith_CachedInitialPlayerStartPos
	VECTOR vInteractWith_CachedInitialPlayerStartRot
	
	// Render Targets
	INT iInteractWith_RenderTargetIDs[ciInteractWith_MaxRenderTargets]
	INT iInteractWith_RenderTargetIDUsedThisFrameBS
	
	// Misc
	BOOL bInteractWith_Cancelled
	FLOAT fInteractWith_DistanceAllowed
	FLOAT fInteractWith_StartPhase = 0.15
	SCRIPT_TIMER stInteractWith_DownloadTimer
	SCRIPT_TIMER stInteractWith_DownloadStutterTimer
	INT iInteractWith_ObjectsPlacedLocalCounter
	INT iInteractWith_RemotePlayerPropStaggeredIndex
	TEXT_LABEL_63 tlInteractWith_PlayerCurrentAnimName
	VEHICLE_INDEX viInteractWith_LinkedVehicle
	PED_INDEX piInteractWith_LinkedPed
	OBJECT_INDEX oiInteractWith_LinkedDynoprop
	OBJECT_INDEX oiInteractWith_AttachedObject
	OBJECT_INDEX oiInteractWith_LinkedObjects[ciINTERACT_WITH__MAX_LINKED_OBJECTS]
	ENTITY_INDEX eiInteractWithEntityCachedAttachmentEntity
	
	// Debug
	BOOL bInteractWith_ExtraDebug = FALSE
	
	//Hold Interact
	HOLD_BUTTON_INTERACT_STATE eHoldInteractState
	SCRIPT_TIMER stHoldInteractReactTimer
	SCRIPT_TIMER stHoldInteractReactDuration
	INT iHoldInteractReactTimerLength
	INT iHoldInteractReactAnim
	BOOL bDoneLongHoldReact
	
ENDSTRUCT
INTERACT_WITH_VARS sInteractWithVars

//Remote
INT iInteractWith_SwapOutParticipantPropBagBS

//Yacht
STRUCT_NET_YACHT_SCENE sMoveScene
CAMERA_INDEX cYachtCam1, cYachtCam2
INT iYachtCutStage = -1
INT iYachtScene = -1

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Custom HUDs
// ##### Description: Bottom Right HUDs content can make in the creator
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

STRUCT CUSTOM_HUD_RUNTIME_STRUCT
	INT iScore = 0
	INT iScoreSecondary = 0
	INT iMaxScore = -1
	INT iMaxScoreSecondary = -1
	HUD_COLOURS eHUDColour = HUD_COLOUR_PURE_WHITE
ENDSTRUCT
CUSTOM_HUD_RUNTIME_STRUCT sCustomHUDStructs[FMMC_MAX_CUSTOM_HUDS]
SCRIPT_TIMER tdCustomHUDReset
CONST_INT ciCUSTOM_HUD_RESET_TIMER 3000

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Minigames: Beam Hack
// ##### Description: Timers, variables, enums, structs and const's relating to 'Interact-With' Minigames.
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//hacking data
S_HACKING_DATA sHackingData

// ===== Beam Hack Anim ===== //
ENUM BEAMHACKANIM_STATE
	BHA_STATE_NONE,
	BHA_STATE_TURNING,
	BHA_STATE_ENTER,
	BHA_STATE_LOOPING,
	BHA_STATE_EXIT,
	BHA_STATE_WALKING
ENDENUM

BEAMHACKANIM_STATE bhaBeamHackAnimState = BHA_STATE_NONE

// ===== Fingerprint Keypad Hack Anim ===== //
ENUM FINGERPRINTKEYPADHACKANIM_STATE
	FKHA_STATE_NONE,
	FKHA_STATE_TURNING,
	FKHA_STATE_ENTER,
	FKHA_STATE_LOOPING,
	FKHA_STATE_BACK_OUT,
	FKHA_STATE_LOOP_PRE_EXIT,
	FKHA_STATE_REATTEMPT,
	FKHA_STATE_QUICK_EXIT,
	FKHA_STATE_SUCCESS_EXIT,
	FKHA_STATE_EXIT,
	FKHA_STATE_WALKING
ENDENUM
FINGERPRINTKEYPADHACKANIM_STATE fkhaFingerprintKeypadHackAnimState = FKHA_STATE_NONE
VECTOR	vFingerprintKeypadHackAnimLastPos
VECTOR 	vFingerprintKeypadHackAnimLastRot
INT     iFingerprintKeypadForCurrentAnim = -1

#IF IS_DEBUG_BUILD
BOOL bEnableExtraLeaderboardPrints = FALSE
BOOL bDumpedF9Info
BOOL bDumpedPreGameF9Info
#ENDIF

INT iTeamsFailedForSDTimeExpired
INT iVehicleWepModelsToLoadBS

BOOL bReadyToCapture = FALSE
BOOL bShouldProcessCokeGrabHud = FALSE

SCRIPT_TIMER tdHackingComplete
CONST_INT ciHACKING_COMPLETE_TIME 5000
CONST_INT ciHACKING_COMPLETE_DELAY 500
INT iHackingPedHelpBitSet[FMMC_MAX_PEDS_BITSET]
INT iHackingPedInRangeBitSet[FMMC_MAX_PEDS_BITSET]
INT iHackingVehHelpBitSet
INT iHackingVehInRangeBitSet
INT iHackingLoopSoundID = -1

INT iHackProgress
INT iHackLimitTimer

WEAPON_TYPE eWeaponBeforeMinigame = WEAPONTYPE_UNARMED

//Hacking variables moved here from FM_Mission_Controller_Minigame
BOOL b_cibsLEGACY_FMMC_MINI_GAME_IGNORE_CONNECT
BOOL b_cibsLEGACY_FMMC_MINI_GAME_IGNORE_BRUTE
BOOL b_cibsLEGACY_FMMC_MINI_GAME_DIFF_EASY
BOOL b_cibsLEGACY_FMMC_MINI_GAME_DIFF_HARD
VECTOR vHackOffset = <<0.0,-0.55,0>>
VECTOR vHackAAOffset = <<0.0,-1.8,0>>
VECTOR vHackLaptopGotoOffset = <<-0.060,-0.7,0>> //<<-0.39,-0.7,0>>
VECTOR vHackKeypadGotoOffset = <<-0.02, -0.445, 0>> //<<-0.130, -0.290, -0.64>>
VECTOR vHackKeycardGotoOffset = <<0,-0.75,0>>//<<-0.39,-0.7,0>>
VECTOR vHackLaptopScenePlayback = << -0.100, -0.800, 0.020 >>
VECTOR vHackKeypadScenePlayback = <<-0.02, -0.445, 0>> //<< -0.100, -0.350, -0.600 >>
	VECTOR vHackKeycardScenePlayback = <<0,-0.75,0>>
FLOAT fThisTweakValue1 =  2.0
FLOAT fThisTweakValue2 =  1.5
FLOAT fUseGoToDistance = 0.2
INT iTimeOutTimer
INT iWallPaper
#IF IS_DEBUG_BUILD
	BOOL bSetUpHackWidgets
#ENDIF
STRING sKeyPadAnim = "anim@heists@keypad@"
STRING sBagAnim
STRING sLaptopHackDict

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Minigames: Drilling
// ##### Description: Timers, variables, enums, structs and const's relating to 'Interact-With' Minigames.
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT DRILL_STATE_INITIALISE			0
CONST_INT DRILL_STATE_WAITING_FOR_ASSETS	1
CONST_INT DRILL_STATE_STARTING_INTRO		2
CONST_INT DRILL_STATE_REMOVE_BAG			3
CONST_INT DRILL_STATE_WAITING_FOR_INTRO		4
CONST_INT DRILL_STATE_DRILLING				5
CONST_INT DRILL_STATE_WAITING_FOR_OUTRO		6
CONST_INT DRILL_STATE_DO_BAG_SWAP			7
CONST_INT DRILL_STATE_CLEANUP				8
CONST_INT DRILL_STATE_MESSED_UP				99
CONST_INT DRILL_STATE_BACK_OUT				100
CONST_INT DRILL_STATE_FAILSAFE				101
VECTOR vSceneNodeOffsetFromObj = <<-0.175, -0.24, -0.02>> 

S_DRILL_DATA sDrillData
S_VAULT_DRILL_DATA sVaultDrillData
S_CASH_GRAB_DATA sCashGrabData
//INT iSyncSceneThermite = -1

INT iCashGrabState
CONST_INT ciCASH_GRAB_STATE_IDLE				0
CONST_INT ciCASH_GRAB_STATE_INIT				1
CONST_INT ciCASH_GRAB_STATE_REQUEST_ASSETS		2
CONST_INT ciCASH_GRAB_STATE_PROCESS_ANIMATION	3
CONST_INT ciCASH_GRAB_STATE_INITIAL_CLEANUP		4
CONST_INT ciCASH_GRAB_STATE_CLEANUP				5


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Minigames: Thermite
// ##### Description: Timers, variables, enums, structs and const's relating to 'Interact-With' Minigames.
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INT iTimeStoppedCoverEyesAnim = 0
INT iTimeWeaponReturnedAfterThermite = 0
INT iThermiteBitset = 0

CONST_INT ciMAX_LOCAL_THERMITE_PTFX  4
INT iThermiteLocalBitset[ciMAX_LOCAL_THERMITE_PTFX]
INT iThermiteLocalSparkTimer[ciMAX_LOCAL_THERMITE_PTFX]
INT iThermiteLocalDripTimer[ciMAX_LOCAL_THERMITE_PTFX]
INT iThermiteRemoteBagFadeTimer[ciMAX_LOCAL_THERMITE_PTFX]

INT iThermiteSmokeEffectTimer = 0
INT iThermiteBagStage = 0
INT iThermiteBagReturnStage = 0
INT iThermiteBagFadeTimer = 0
INT iThermiteKeypadInRangeBS = 0
INT iThermiteKeypadWasLastTeamCharge[FMMC_MAX_TEAMS]
INT iThermiteKeypadWasLastPlayerCharge
INT iThermiteKeypadIsProcessing
	
BOOL bDrawCashGrabTakeRed
INT iDrawCashGrabTakeRedTime
INT iDrawCashGrabTakeRedDuration
  
CONST_INT THERMITE_STATE_INITIALISE				0
CONST_INT THERMITE_STATE_WAITING_FOR_ASSETS		1
CONST_INT THERMITE_STATE_START_ANIM				2
CONST_INT THERMITE_STATE_REMOVE_BAG				3
CONST_INT THERMITE_STATE_WAIT_FOR_ANIM			4
CONST_INT THERMITE_STATE_DO_BAG_SWAP			5
CONST_INT THERMITE_STATE_DO_DOOR_SWAP			6
CONST_INT THERMITE_STATE_WAIT_FOR_EXPLOSION		7
CONST_INT THERMITE_STATE_CLEANUP				8
CONST_INT THERMITE_STATE_FAIL_CLEANUP			9
CONST_INT THERMAL_CHARGE_DURATION				12000

CONST_INT THERMITE_BITSET_IS_COVER_EYES_ANIM_PLAYING		0
CONST_INT THERMITE_BITSET_HAS_DOOR_SOUND_TRIGGERED			1
CONST_INT THERMITE_BITSET_IS_THERMITE_BURNING				2
CONST_INT THERMITE_BITSET_WALK_TASK_GIVEN_EARLY				3
CONST_INT THERMITE_BITSET_TRIGGERED_ANIMS_EARLY				4
CONST_INT THERMITE_BITSET_SET_THERMITE_OBJECT_VISIBLE		5
CONST_INT THERMITE_BITSET_SET_THERMITE_OBJECT_TO_FLASH		6
CONST_INT THERMITE_BITSET_MOCAP_TRIGGERED_DURING_ANIM		7
CONST_INT THERMITE_BITSET_IS_COVER_EYES_INTRO_PLAYING		8
CONST_INT THERMITE_BITSET_FORCED_ANIM_UPDATE_LAST_FRAME		9

CONST_INT THERMITE_LOCAL_BITSET_CREATE_LOCAL_THERMITE_PTFX		0
CONST_INT THERMITE_LOCAL_BITSET_START_LOCAL_SPARK_DIE_OFF_EVO	1
CONST_INT THERMITE_LOCAL_BITSET_REMOTE_BAG_OBJECT_HIDDEN		2
CONST_INT THERMITE_LOCAL_BITSET_REMOTE_BAG_COMPONENT_REMOVED	3
CONST_INT THERMITE_LOCAL_BITSET_REMOTE_BAG_OBJECT_REVEALED		4
CONST_INT THERMITE_LOCAL_BITSET_REMOTE_BAG_COMPONENT_RESTORED	5
CONST_INT THERMITE_LOCAL_BITSET_IS_THERMITE_BURNING				6
CONST_INT THERMITE_LOCAL_BITSET_IS_ANY_THERMITE_PROCESSING		7

CONST_INT THERMITE_BAG_STAGE_ANIM_NOT_STARTED			0
CONST_INT THERMITE_BAG_STAGE_PLAYERS_BAG_VISIBLE		1
CONST_INT THERMITE_BAG_STAGE_OBJECT_BAG_INVISIBLE		2
CONST_INT THERMITE_BAG_STAGE_OBJECT_BAG_VISIBLE			3

CONST_INT THERMITE_BAG_RETURN_STAGE_ANIM_NOT_STARTED			0
CONST_INT THERMITE_BAG_RETURN_STAGE_WAITING_FOR_ANIM			1
CONST_INT THERMITE_BAG_RETURN_STAGE_WAITING_FOR_COMPONENT_BAG	2
CONST_INT THERMITE_BAG_RETURN_STAGE_SWAP_COMPLETE				3

CONST_INT THERMITE_DRIP_EFFECT_INDEX					0
CONST_INT THERMITE_SPARK_EFFECT_INDEX					1

VECTOR vThermitePlaybackPos = <<0.0, 0.0, 0.0>>
VECTOR vThermiteGotoPoint = <<0.0, 0.0, 0.0>>
VECTOR vThermiteSceneOffsetFromObj = <<-0.02, -0.05, -0.08>>
VECTOR vThermiteSparkEffectOffset = vThermiteSceneOffsetFromObj + <<0.0, 1.0, 0.0>> //2099588 - To fix rendering issues with the effect it has an offset applied that needs to be accounted for in the script.
VECTOR vThermiteDripEffectOffset = vThermiteSceneOffsetFromObj
VECTOR vThermitePosWhenPlaced = <<0.0, 0.0, 0.0>>
VECTOR vThermiteRotWhenPlaced = <<0.0, 0.0, 0.0>>
VECTOR vThermiteLocalDripOffset[ciMAX_LOCAL_THERMITE_PTFX]
VECTOR vThermiteLocalSparkOffset[ciMAX_LOCAL_THERMITE_PTFX]

OBJECT_INDEX oiLocalThermiteTarget[ciMAX_LOCAL_THERMITE_PTFX]

PTFX_ID ptfxThermite[2]
PTFX_ID ptfxThermiteLocalDrip[ciMAX_LOCAL_THERMITE_PTFX]
PTFX_ID ptfxThermiteLocalSpark[ciMAX_LOCAL_THERMITE_PTFX]

PTFX_ID DrillptfxIndex
PTFX_ID OverheatptfxIndex


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Minigames: Cash Grab
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT CASHGRAB_BITSET_REMOTE_BAG_COMPONENT_REMOVED 	1
CONST_INT CASHGRAB_BITSET_REMOTE_BAG_OBJECT_REVEALED	2	
CONST_INT CASHGRAB_BITSET_REMOTE_BAG_OBJECT_HIDDEN		3
CONST_INT CASHGRAB_BITSET_REMOTE_BAG_COMPONENT_RESTORED	4
CONST_INT CASHGRAB_BITSET_REMOTE_CASH_PILE_VISIBLE		5
CONST_INT CASHGRAB_BITSET_REMOTE_CASH_PILE_INVISIBLE	6

INT iCashGrabBitset[MAX_NUM_MC_PLAYERS]
INT iCashGrabTimer[MAX_NUM_MC_PLAYERS]

FLOAT	fCashGrabDropRadius				= 0.175
FLOAT	fCashGrabAngledAreaWidth		= 1.750

VECTOR 	vCashGrabCamAttachOffsetStart 	= <<0.6902, 0.0902, 0.895>>			//<<0.6902, 0.0902, 0.6794>>
VECTOR 	vCashGrabCamRotationStart		= <<-14.7232, 0.0033, -118.3009>>
VECTOR	vCashGrabAngledAreaOffset1		= << 0.0, 0.0, -0.450 >>
VECTOR	vCashGrabAngledAreaOffset2		= << 0.0, 1.300, 1.50 >>

VECTOR 	vCashGrabCamAttachOffsetEnd 	= <<0.3845, -0.4765, 0.725>>		//<<0.3845, -0.4765, 0.6170>>
VECTOR 	vCashGrabCamPointOffsetEnd 		= <<-0.5548, 0.750, 0.400>>			//<<-0.5548, 0.7236, 0.6954>>
VECTOR 	vCashGrabCamRotationEnd			= <<2.8980, -0.0000, 52.4134>>

CONST_INT ciLootGrabTotalItems_Gold 24
CONST_INT ciLootGrabTotalItems_Base 45

CONST_INT ciDROP_MONEY_FREQUENCY	600

STRUCT DROP_MONEY_STRUCT
	INT iNoteTimer = 0
	STRING sPTFXAsset
	STRING sPTFXName
	VECTOR vCashGrabDropOffset1// = << -0.180, -0.190, -0.150 >>
	VECTOR vCashGrabDropOffset2// = << -0.130, -0.190, 0.0000 >>
	VECTOR vCashGrabDropOffset3// = << -0.090, -0.170, 0.1600 >>
ENDSTRUCT
DROP_MONEY_STRUCT sDropMoney

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Underwater Tunnel Welding Minigame
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CONST_FLOAT cfUNDERWATERTUNNELWELD_BAR_CUT_TIME 		0.25 // The bars get cut far quicker than in the single-player version
CONST_FLOAT cfUNDERWATERTUNNELWELD_INTERACT_DISTANCE	8.0
CONST_INT NUM_COLUMNS 									6
CONST_INT NUM_NODES_PER_COLUMN							8
CONST_INT NUM_ROWS										NUM_NODES_PER_COLUMN - 1
CONST_INT NUM_NODES_PER_ROW								NUM_COLUMNS + 1
CONST_INT total_numer_of_grill_0_breaks		 			26


ENUM UNDERWATER_TUNNEL_WELD_STAGE
	UNDERWATER_TUNNEL_WELD_STAGE__WAITING_TO_START,
	UNDERWATER_TUNNEL_WELD_STAGE__INIT,
	UNDERWATER_TUNNEL_WELD_STAGE__INIT_STARTING_MOVE_NETWORK,
	UNDERWATER_TUNNEL_WELD_STAGE__STARTING,
	UNDERWATER_TUNNEL_WELD_STAGE__RUNNING,
	UNDERWATER_TUNNEL_WELD_STAGE__EARLY_EXIT,
	UNDERWATER_TUNNEL_WELD_STAGE__PASSED,	
	UNDERWATER_TUNNEL_WELD_STAGE__CLEANUP
ENDENUM
UNDERWATER_TUNNEL_WELD_STAGE erUnderwaterTunnel_WeldStage = UNDERWATER_TUNNEL_WELD_STAGE__WAITING_TO_START

STRUCT UNDERWATERTUNNELWELD_POINT
	FLOAT x, y
ENDSTRUCT

STRUCT UNDERWATERTUNNELWELD_DATA
	VECTOR v_root_pos
	VECTOR v_wall_rot
	UNDERWATERTUNNELWELD_POINT s_current_offset
	FLOAT f_sine_root_heading
	FLOAT f_cos_root_heading
	FLOAT f_sine_root_tilt
	FLOAT f_min_offset_x
	FLOAT f_max_offset_x
	FLOAT f_min_offset_y
	FLOAT f_max_offset_y
	FLOAT f_current_ptfx_rot
	
	OBJECT_INDEX oiGrillObject
	OBJECT_INDEX obj_cursor
	
	BOOL bAbort
	BOOL bUpdatePrompts = TRUE
	SCALEFORM_INDEX scaleformInstructionalButtonsIndex
	SCALEFORM_INSTRUCTIONAL_BUTTONS scaleformInstructionalButtons	
ENDSTRUCT

STRUCT UNDERWATERTUNNELWELD_GRID_POINT
	FLOAT fOffset_X
	FLOAT fOffset_Z
	FLOAT fWeldTime
	BOOL bFullyWelded
ENDSTRUCT

struct BOX_STRUCT
	float x
	float y
	float width
	float height 
	int r
	int g
	int b
	int a
endstruct 

STRUCT MISSION_OBJECT
	OBJECT_INDEX obj
	BLIP_INDEX blip
	MODEL_NAMES model
ENDSTRUCT

UNDERWATERTUNNELWELD_DATA sUnderwaterTunnelWeldData
UNDERWATERTUNNELWELD_GRID_POINT sUnderwaterTunnelWeldColumns[NUM_COLUMNS][NUM_NODES_PER_COLUMN]
UNDERWATERTUNNELWELD_GRID_POINT sUnderwaterTunnelWeldRows[NUM_ROWS][NUM_NODES_PER_ROW]

CONST_INT ciUnderwaterTunnelWeld_NumPTFX 40

INT iUnderwaterTunnelWeld_DesiredWeldRouteTop		= 0
INT iUnderwaterTunnelWeld_DesiredWeldRouteBottom	= 0
INT iUnderwaterTunnelWeld_DesiredWeldRouteLeft		= 0
INT iUnderwaterTunnelWeld_DesiredWeldRouteRight		= 0

FLOAT fUnderwaterTunnelWeld_MoveNetworkScaledX
FLOAT fUnderwaterTunnelWeld_MoveNetworkScaledY

//UnderwaterTunnelWeld camera positions
VECTOR vUnderwaterTunnelWeld_CamOffset_TopLeft  	= <<-1.1095, 4.5, 1.0>>
VECTOR vUnderwaterTunnelWeld_CamRot_TopLeft 		= <<-6.091456, -6.360575, 8.0>>

//top right z
VECTOR vUnderwaterTunnelWeld_CamOffset_TopRight 	= <<1.1095, 4.5, 0.75>> 
VECTOR vUnderwaterTunnelWeld_CamRot_TopRight		= <<-1.2780, -6.6450, -8.0>>

//bottom left
VECTOR vUnderwaterTunnelWeld_CamOffset_BottomLeft 	= <<-1.1095, 4.5, 0.0>>
VECTOR vUnderwaterTunnelWeld_CamRot_BottomLeft 		= <<-6.0978, -6.3602, 8.0>>

//bottom right 
VECTOR vUnderwaterTunnelWeld_CamOffset_BottomRight  = <<1.1095, 4.5, -0.25>>
VECTOR vUnderwaterTunnelWeld_CamRot_BottomRight 	= <<-1.2780, -6.6450, -8.0>>

PTFX_ID ptUnderwaterTunnelWeldPTFX_Weld
PTFX_ID ptUnderwaterTunnelWeldPTFX_Bubble
PTFX_ID ptUnderwaterTunnelWeldPTFX_Singe
PTFX_ID ptUnderwaterTunnelWeldPTFX_Break[ciUnderwaterTunnelWeld_NumPTFX]
PTFX_ID ptUnderwaterTunnelWeldPTFX_IdleBlueFlame

CAMERA_INDEX ciUnderwaterTunnelWeld_CutsceneCam
CAMERA_INDEX ciUnderwaterTunnelWeld_MainCam
OBJECT_INDEX objUnderwaterTunnelWeld_WeldTool
INT iUnderwaterTunnelWeldSound_BlowtorchLoop = -1
INT iUnderwaterTunnelWeldSound_BlowtorchCutLoop = -1
INT iUnderwaterTunnelWeld_ForceChangeTimer
INT iUnderwaterTunnelWeld_PlayerMoveProg
INT iUnderwaterTunnelWeldPTFX_CurrentBreakPTFX
INT iUnderwaterTunnelWeld_NumRouteNotHit

///    Test code for new UnderwaterTunnelWeld: based on balance controls
FLOAT fUnderwaterTunnelWeld_CurrentOffset
FLOAT fUnderwaterTunnelWeld_CurrentVel
FLOAT fUnderwaterTunnelWeld_CurrentAccel

BOOL bUnderwaterTunnelWeld_ActivateUnderwaterTunnelWeldCam = FALSE
BOOL bUnderwaterTunnelWeld_BarHasBeenCut = FALSE

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Enter Safe Combination Minigame
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

ENUM ENTER_SAFE_COMBINATION_STAGE
	ENTER_SAFE_COMBINATION_STAGE__WAITING_TO_START,
	ENTER_SAFE_COMBINATION_STAGE__INIT,
	ENTER_SAFE_COMBINATION_STAGE__STARTING,
	ENTER_SAFE_COMBINATION_STAGE__RUNNING,
	ENTER_SAFE_COMBINATION_STAGE__OPEN_SAFE,
	ENTER_SAFE_COMBINATION_STAGE__FAIL,
	ENTER_SAFE_COMBINATION_STAGE__EXIT,
	ENTER_SAFE_COMBINATION_STAGE__CLEANUP,
	ENTER_SAFE_COMBINATION_STAGE__CLEANED_UP
ENDENUM
ENTER_SAFE_COMBINATION_STAGE eEnterSafeCombinationStage = ENTER_SAFE_COMBINATION_STAGE__WAITING_TO_START

ENUM ENTER_SAFE_COMBINATION_DISPLAY_PANEL_STATE
	ENTER_SAFE_COMBINATION_DISPLAY_PANEL_STATE__OFF,
	ENTER_SAFE_COMBINATION_DISPLAY_PANEL_STATE__NORMAL,
	ENTER_SAFE_COMBINATION_DISPLAY_PANEL_STATE__ERROR,
	ENTER_SAFE_COMBINATION_DISPLAY_PANEL_STATE__OPEN	
ENDENUM

STRUCT ENTER_SAFE_COMBINATION_DISPLAY_PANEL
	FLOAT fCurrentSafeValue
	INT iCorrectSafeValue
ENDSTRUCT

CONST_INT ciENTER_SAFE_COMBINATION_DISPLAY_PANELS 			3
CONST_FLOAT cfENTER_SAFE_COMBINATION_INTERACT_DISTANCE		2.0

CONST_FLOAT cfENTER_SAFE_COMBINATION_ACCELERATION_SPEED		2.5
CONST_FLOAT cfENTER_SAFE_COMBINATION_ACCELERATION_MIN		1.0
CONST_FLOAT cfENTER_SAFE_COMBINATION_ACCELERATION_MAX		15.0

CONST_FLOAT cfENTER_SAFE_COMBINATION_VALUE_MIN	0.0
CONST_FLOAT cfENTER_SAFE_COMBINATION_VALUE_MAX	99.0

CONST_INT ciENTER_SAFE_COMBINATION_FAIL_SCREEN_LENGTH					2000
CONST_INT ciENTER_SAFE_COMBINATION_PROGRESS_SHARE_COOLDOWN_LENGTH		1500

STRUCT ENTER_SAFE_COMBINATION_DATA
	INT iCurrentlySelectedDisplayPanel = 0
	ENTER_SAFE_COMBINATION_DISPLAY_PANEL sEnterSafePanels[ciENTER_SAFE_COMBINATION_DISPLAY_PANELS] 
	
	INT iSafeRenderTargetID = -1
	SCALEFORM_INDEX siEnterSafeCombinationScaleformIndex
	
	BOOL bBackedOutSafeMinigameEarly
	BOOL bProgressHasChangedSinceLastShareEvent
	FLOAT fInputAcceleration = cfENTER_SAFE_COMBINATION_ACCELERATION_MIN
	
	SCRIPT_TIMER stEnterSafeCombinationFailScreenTimer
	SCRIPT_TIMER stEnterSafeCombinationProgressShareCooldownTimer
ENDSTRUCT

ENTER_SAFE_COMBINATION_DATA sEnterSafeCombinationData

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Beamhack (In vehicle) minigame
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

ENUM BEAMHACK_IN_VEHICLE_STAGE
	BEAMHACK_IN_VEHICLE_STAGE__WAITING_TO_START = 0,
	BEAMHACK_IN_VEHICLE_STAGE__INIT,
	BEAMHACK_IN_VEHICLE_STAGE__RUNNING,
	BEAMHACK_IN_VEHICLE_STAGE__EXIT,
	BEAMHACK_IN_VEHICLE_STAGE__SUCCESS,
	BEAMHACK_IN_VEHICLE_STAGE__FAIL
ENDENUM
BEAMHACK_IN_VEHICLE_STAGE eBeamhackInVehicleStage[ciMAX_HACKING_STRUCTS_BEAMHACK]

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Glass Cutting Minigame
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

ENUM GLASS_CUTTING_STAGE
	GLASS_CUTTING_STAGE__WAITING_TO_START,
	GLASS_CUTTING_STAGE__INIT,
	GLASS_CUTTING_STAGE__RUNNING,
	GLASS_CUTTING_STAGE__EXIT,
	GLASS_CUTTING_STAGE__SUCCESS,
	GLASS_CUTTING_STAGE__CLEANUP
ENDENUM
GLASS_CUTTING_STAGE eGlassCuttingStage = GLASS_CUTTING_STAGE__WAITING_TO_START

CONST_INT ciIW_SUBANIM__GLASSCUT_ENTER						0
CONST_INT ciIW_SUBANIM__GLASSCUT_ENTER_ALT					1
CONST_INT ciIW_SUBANIM__GLASSCUT_IDLE						2
CONST_INT ciIW_SUBANIM__GLASSCUT_CUTTING					3
CONST_INT ciIW_SUBANIM__GLASSCUT_OVERHEAT					4
CONST_INT ciIW_SUBANIM__GLASSCUT_EXIT						5
CONST_INT ciIW_SUBANIM__GLASSCUT_SUCCESS					6

CONST_INT ciGLASS_CUTTING_BITSET__STARTED 					0
CONST_INT ciGLASS_CUTTING_BITSET__SUCCESS 					1
CONST_INT ciGLASS_CUTTING_BITSET__STARTED_TIMECYCLE_MOD 	2

CONST_FLOAT cfGLASS_CUTTING_PROGRESS_BASE_SPEED							0.325
CONST_FLOAT cfGLASS_CUTTING_PROGRESS_SPEED_TRIGGER_MULTIPLIER			1.0
CONST_FLOAT cfGLASS_CUTTING_PROGRESS_SPEED_TIME_HELD_MULTIPLIER			0.01
CONST_FLOAT cfGLASS_CUTTING_PROGRESS_SPEED_TIME_HELD_MAX				20.0

CONST_FLOAT cfGLASS_CUTTING_HEAT_BUILDUP_BASE_SPEED							0.1
CONST_FLOAT cfGLASS_CUTTING_HEAT_BUILDUP_TRIGGER_MULTIPLIER					1.8
CONST_FLOAT cfGLASS_CUTTING_HEAT_BUILDUP_HEAT_MULTIPLIER					0.0
CONST_FLOAT cfGLASS_CUTTING_HEAT_BUILDUP_SPEED_MIN							0.01
CONST_FLOAT cfGLASS_CUTTING_HEAT_BUILDUP_SPEED_MAX							70.0
CONST_FLOAT cfGLASS_CUTTING_HEAT_BUILDUP_BASE_SPEED_TIME_HELD_MULTIPLIER	0.02
CONST_FLOAT cfGLASS_CUTTING_HEAT_BUILDUP_BASE_SPEED_TIME_HELD_MAX			30.0

CONST_FLOAT cfGLASS_CUTTING_HEAT_COOLDOWN_SPEED									4.5
CONST_FLOAT cfGLASS_CUTTING_HEAT_COOLDOWN_SPEED_MAX								95.0
CONST_FLOAT cfGLASS_CUTTING_HEAT_COOLDOWN_SPEED_CURRENT_HEAT_MULTIPLIER			0.9
CONST_FLOAT cfGLASS_CUTTING_HEAT_COOLDOWN_SPEED_OVERHEATED_MULTIPLIER			0.1

STRUCT GLASS_CUTTING_DATA
	INT iBitset = 0
	NETWORK_INDEX niPlacedGlassCutter
	INT iGlassCuttingLootPropIndex = -1
	FLOAT fGlassCuttingProgress = 0.0
	FLOAT fGlassCuttingHeat = 0.0
	
	SCRIPT_TIMER stDrillingTimer
	
	INT iGlassCutting_CuttingLoopSoundID = -1
	INT iGlassCutting_HeatLoopSoundID = -1
	
	PTFX_ID ptCutPTFX
	PTFX_ID ptOverheatPTFX
ENDSTRUCT

GLASS_CUTTING_DATA sGlassCuttingData

CONST_FLOAT cfGLASS_CUTTING_INTERACT_DISTANCE	2.0

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Interrogation
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
ENUM INTERROGATION_STAGE
	INTERROGATION_STAGE__WAITING_TO_START,
	INTERROGATION_STAGE__INIT,
	INTERROGATION_STAGE__WAITING_IN_POSITION,
	INTERROGATION_STAGE__RUNNING,
	INTERROGATION_STAGE__EXIT,
	INTERROGATION_STAGE__SUCCESS,
	INTERROGATION_STAGE__CLEANUP
ENDENUM
INTERROGATION_STAGE eInterrogationStage = INTERROGATION_STAGE__WAITING_TO_START

CONST_FLOAT cfInterrogationSequenceBlendOut		-0.05
CONST_INT ciInterrogationNoHitDialogueTimerLength	3500
CONST_INT ciInterrogationApologyDialogueAnimDelay	500

STRUCT INTERROGATION_MINIGAME_VARS
	INT iInterrogationSubAnimProgress = ciIW_SUBANIM__INTERROGATION_STAGE_1
	SEQUENCE_INDEX siIntimidationSequence_Golfer
	SEQUENCE_INDEX siIntimidationSequence_Franklin
	INT iInterrogationNoHitDialogueTimestamp
	INT iInterrogationApologyDialogueTimestamp
ENDSTRUCT
INTERROGATION_MINIGAME_VARS sInterrogationVars

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Tasered Entities
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

STRUCT FMMC_TASERED_ENTITY_VARS
	INT iHitByTaserBS // Set by the damage event when an object is hit by a taser & cleared in PROCESS_OBJECTS_EVERY_FRAME
	INT iReactedToTaserBS // Set & cleared by object processing functions on a per-system basis
	SCRIPT_TIMER stHitByTaserTimers[FMMC_MAX_NUM_DYNOPROPS]
	PTFX_ID ptLoopingTaserFX[FMMC_MAX_NUM_DYNOPROPS]
ENDSTRUCT

FMMC_TASERED_ENTITY_VARS sObjTaserVars
FMMC_TASERED_ENTITY_VARS sDynoPropTaserVars
INT iFuseBoxesCurrentlyTasered = 0


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: PTFX & flares
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

STRING sPTFXThermalAsset = "scr_ornate_heist"

INT	iTitanRotorParticlesBitset
PTFX_ID ptfxTitanRotorParticles[FMMC_MAX_VEHICLES][4]

ENUM FLARE_STATE
	FLARE_STATE_INIT=0,
	FLARE_STATE_NEUTRAL,
	FLARE_STATE_TEAM_0,
	FLARE_STATE_TEAM_1,
	FLARE_STATE_TEAM_2,
	FLARE_STATE_TEAM_3,
	FLARE_STATE_TRANSITION
ENDENUM

STRUCT prop_flare_data
	FLARE_STATE iFlareState
	FLARE_STATE iNewState
	FLOAT fNextRed
	FLOAT fNextGreen
	FLOAT fNextBlue
	FLOAT fRed
	FLOAT fGreen
	FLOAT fBlue
	INT   iPTFXval = - 1
	FLOAT fTransitionProgress
	INT   iWinningTeam
	INT		iConnectedLocate = -1
ENDSTRUCT

CONST_INT ciTOTAL_PTFX 			20 // This cannot be greater than 32

prop_flare_data flare_data [ciTOTAL_PTFX] // Previously FMMC_MAX_DYNAMIC_FLARES
prop_flare_data corona_flare_data [ciTOTAL_PTFX] // Previously FMMC_MAX_DYNAMIC_FLARES

INT iNumCreatedDynamicFlarePTFX

INT iNumCreatedFlarePTFX
INT iCreatedFlaresBitset[(FMMC_MAX_NUM_PROPS / 32) + 1]

INT iCreatedFireworkBitset[(FMMC_MAX_NUM_PROPS / 32) + 1]

OBJECT_INDEX objFlare
PTFX_ID players_underwater_flare
SCRIPT_TIMER stFlarePTFXDelayTimer
CONST_INT ciFlarePTFXDelayTimerLength	2000

PTFX_ID ptfxFlareIndex[ciTOTAL_PTFX]

INT FlareSoundID[ciTOTAL_PTFX]
INT FlareSoundIDPropNum[ciTOTAL_PTFX]

PTFX_ID trans_lights_ptfx

PTFX_ID ptfxHeliDamageSmoke1
PTFX_ID ptfxHeliDamageSmoke2

PTFX_ID ptfxFlickeringInvisiblePedEffects[FMMC_MAX_PEDS]

PTFX_ID ptfx_PropFadeoutEveryFrameIndex[FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME]
INT iPropFadeoutEveryFrameIndex[FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME]
INT iFlashToggle[FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME]
INT iFlashToggleTime[FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME]
SCRIPT_TIMER tdFlashToggleDissolvePTFX[FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME]

CONST_INT ci_FlashToggleDissolveTime			1000

PTFX_ID StockpileSmokeTrail
SCRIPT_TIMER SmokeTrailTimer
CONST_INT ci_SMOKE_TRAIL_TIMER	500

CONST_INT ci_PLACED_PTFX_Loaded								0
CONST_INT ci_PLACED_PTFX_Created							1
CONST_INT ci_PLACED_PTFX_SFX								2
CONST_INT ci_PLACED_PTFX_CleanedUp							3
STRUCT PLACED_PTFX_CLIENT_STRUCT 
	PTFX_ID ptfxID
	INT iPtfxBS
	SCRIPT_TIMER tdPtfxStarted
	INT iPTFXSoundID = -1
ENDSTRUCT
PLACED_PTFX_CLIENT_STRUCT sPlacedPtfxLocal[FMMC_MAX_PLACED_PTFX]


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Rules - Photo
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INT iPhotoTrackedPoint = -1

INT iVehPhotoTrackedPoint[FMMC_MAX_VEHICLES]
INT iVehPhotoTrackedPoint2[FMMC_MAX_VEHICLES]
VECTOR vVehPhotoCoords[FMMC_MAX_VEHICLES]

VECTOR vLocalPhotoCoords
CONST_INT ci_CHARM_RANGE  10
CONST_INT ci_CHARM_EXIT_RANGE  12
CONST_INT ci_PHOTO_RANGE_VEH 40
CONST_INT ci_PHOTO_RANGE_VEH_NUMPLATE 30
CONST_FLOAT cf_PHOTO_NUMPLATE_MAXANGLE 70.0

INT iObjPhotoIndex = -1
FLOAT fObjPhotoDistance2 = MAX_FLOAT
INT iObjPhotoTrackedPoint = -1
CONST_INT ciFMMC_PUT_AWAY_PHONE_TIMER 400
SCRIPT_TIMER tdPhotoTakenTimer

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Sudden Death
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT ciSUDDEN_DEATH_REASON_KILLS		0
CONST_INT ciSUDDEN_DEATH_REASON_POINTS		1
CONST_INT ciSUDDEN_DEATH_REASON_DEATHS		2
CONST_INT ciSUDDEN_DEATH_REASON_HSHOTS		3

INT iSuddenDeath_PlayerLives = -1

ENUM SUMO_SUDDEN_DEATH_STAGE_BD
	eSSDS_TIMER_GET_INSIDE_SPHERE,
	eSSDS_INSIDE_SPHERE,
	eSSDS_PUSHED_OUT_OF_SPHERE
ENDENUM

INT iSuddenDeathTargetInitStage
INT iSuddenDeathTargetRoundLast
BOOL bIncrementSuddenDeathTargetRound
INT iSuddenDeathTargetProp
OBJECT_INDEX objSuddenDeathTarget
INT iSuddenDeathTargetMoveStyle = 0
VECTOR vSuddenDeathTargetCentre = <<-0.1, 0.0, 6.0>>
VECTOR vSuddenDeathTargetBounds = <<0.0, 2.1, 2.3>>
VECTOR vSuddenDeathTargetOffset = <<0.0, 0.0, 0.0>>
VECTOR vSuddenDeathTargetInitialVelocity = <<0.0, 3.000, 3.125>>
VECTOR vSuddenDeathTargetVelocity = <<0.0, 3.000, 3.125>>
FLOAT fSuddenDeathTargetSinCosScale = 2.0
FLOAT fSuddenDeathTargetSinCosSpeed = 0.150

BOOL bShownScoreShardInSuddenDeath = FALSE

ENUM eSuddenDeathState
	eSuddenDeathState_WAIT_TO_START,
	eSuddenDeathState_START,
	eSuddenDeathState_WAIT_FOR_END,
	eSuddenDeathState_END
ENDENUM

SCRIPT_TIMER stSuddenDeathMusic
eSuddenDeathState sudden_death_music_state = eSuddenDeathState_WAIT_TO_START

// Sudden death sumo penned in variables 
VECTOR vSphereSpawnPoint
FLOAT fSphereStartRadius
FLOAT fSphereEndRadius
FLOAT fCurrentSphereRadius
FLOAT fCurrentSphereRadius_VisualOffset = 0.0
SCRIPT_TIMER stSumoSDBubbleUpdateTimer
SCRIPT_TIMER stSumoSDCountdownUpdateTimer
CONST_INT ciSUMO_SD_BUBBLE_UPDATE_INTERVAL			1000
CONST_FLOAT ciSUMO_SD_SIZE_OFFSET_TOPDOWN	-7.5

INT fSphereShrinkTime
INT fTimeToGetInsideSphere

INT iOutAreaSound = GET_SOUND_ID()
INT iDoorAlarmSoundID = -1

CONST_INT ciPBDBOOL_SSDS_IS_INSIDE_SPHERE			1
CONST_INT ciPBDBOOL_SSDS_COUNTDOWN_TIMER_FINISHED	2
CONST_INT ciPBDBOOL_SSDS_COUNTDOWN_SOUND_PLAYING	3
CONST_INT ciPBDBOOL_SSDS_HAS_COUNTDOWN_FINISHED		4

SCRIPT_TIMER stTimeToGetInSphere

//Kill all enemies penned in sudden death
SCRIPT_TIMER timeToBeInsideSphere
SCRIPT_TIMER timeToBeInsideSphereUpdateTimer
INT iTimeToBeInsideSphere
VECTOR vSphereSpawnPosition
SCRIPT_TIMER suddenDeathOutOfAreaTimer
CONST_INT ciOUT_OF_AREA_TIME		5000
CONST_INT ciKAE_PENNED_IN_INSIDE_SPHERE		0
CONST_INT ciKAE_ELIMINATION_TIMER_FINISHED	1


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cutscenes - Scripted
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

eFMMC_SCRIPTED_CUTSCENE_PROGRESS eScriptedCutsceneProgress
eFMMC_MOCAP_PROGRESS eMocapManageCutsceneProgress
eFMMC_MOCAP_RUNNING_PROGRESS eMocapRunningCutsceneProgress

INT iScriptedCutsceneProgress
INT iMocapCutsceneProgress
INT iCamShot
INT iScriptedCutsceneTeam = -1
INT iCutsceneStreamingTeam = -1
BOOL bWarpPlayerAfterScriptCameaSetup = FALSE
INT iCamShotLoaded = -1
INT iTODh,iTODm,iTODs
SCRIPT_TIMER tdPhoneCutSafetyTimer
SCRIPT_TIMER tdScriptedCutsceneTimer
SCRIPT_TIMER tdStartWarpSafetyTimer
CAMERA_INDEX cutscenecam
WEAPON_TYPE weapMocap = WEAPONTYPE_INVALID
INT iScriptedCutsceneCannedSyncedScene = -1
SCRIPT_TIMER stCannedTVOL

VEHICLE_SEAT vehSeatForcedFromSyncScene = VS_ANY_PASSENGER

INT iCoverAfterCutsceneTimeStamp

FLOAT fScriptedCutsceneSyncSceneDuration

INT iPartTriggeredLocateLast = -1

INT iCutsceneInterpTimeOverride = -1

CONST_INT ciScriptedCutsceneBitset_RenderingSyncSceneCam 							0
CONST_INT ciScriptedCutsceneBitset_ForceLocalUseofSyncSceneCam  					1
CONST_INT ciScriptedCutsceneBitset_PlayedCutsceneSyncScene							2
CONST_INT ciScriptedCutsceneBitset_PlayedOneShotSound_0								3
CONST_INT ciScriptedCutsceneBitset_RunningCutsceneSyncSceneLocally					4
CONST_INT ciScriptedCutsceneBitset_sCutsceneSyncSceneDataGameplayAssigned			5
CONST_INT ciScriptedCutsceneBitset_sCutsceneSyncSceneDataGameplayAssignedPostWarp	6
CONST_INT ciScriptedCutsceneBitset_sCutsceneSyncSceneClearedArea					7
CONST_INT ciScriptedCutsceneBitset_sCutsceneSyncSceneAudioScenePlayed				8
CONST_INT ciScriptedCutsceneBitset_sCutsceneSyncSceneConversationProcessed			9
CONST_INT ciScriptedCutsceneBitset_sCutsceneSyncSceneStartMusicEvent				10
CONST_INT ciScriptedCutsceneBitset_sCutsceneSyncSceneStopMusicEvent					11
CONST_INT ciScriptedCutsceneBitset_sCutsceneSyncSceneUseExrtaVehicleTaskTimer		12
CONST_INT ciScriptedCutsceneBitset_EarlySetOutOfMpCutscene							13

INT iScriptedCutsceneSyncSceneBS

FMMC_CUTSCENE_SYNC_SCENE_DATA sCutsceneSyncSceneDataGameplay

SCRIPT_TIMER tdEnterCoverEndingCutsceneTimeOut

// Just reuse consts.
CONST_INT ciCUTSCENE_EVENT_GENERIC_0		0
INT iCutsceneEventBS

ENUM SCRIPTED_CUTSCENE_END_WARP_LOCAL_STATE
	SCUT_EWLS_INIT,
	SCUT_EWLS_WARP,
	SCUT_EWLS_WAIT_FOR_VEHICLE,
	SCUT_EWLS_WAIT_FOR_INTERIOR,
	SCUT_EWLS_END
ENDENUM

STRUCT SCRIPTED_CUTSCENE_END_WARP_LOCAL
	SCRIPTED_CUTSCENE_END_WARP_LOCAL_STATE eState
	VECTOR vWarpCoord
	FLOAT fWarpHeading
	VEHICLE_INDEX vehTarget
	VEHICLE_SEAT eCurrentSeat
ENDSTRUCT
SCRIPTED_CUTSCENE_END_WARP_LOCAL sScriptedCutsceneEndWarp

	
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Ped Companion
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

ENUM PED_COMPANION_TASK_STATE
	
	PED_COMPANION_TASK_STATE_INIT = 0,
	PED_COMPANION_TASK_STATE_WAIT,
	PED_COMPANION_TASK_STATE_GOTO_POINT,
	PED_COMPANION_TASK_STATE_FOLLOW_INTO_VEHICLE,
	PED_COMPANION_TASK_STATE_FOLLOW_AS_GROUP_MEMBER,
	PED_COMPANION_TASK_STATE_COMBAT,
	PED_COMPANION_TASK_STATE_WARP,
	PED_COMPANION_TASK_STATE_CLEANUP
ENDENUM

CONST_INT ciCOMPANION_BITSET_CLOSE_DOOR_AFTER_SYNC_SCENE 0
CONST_INT ciCOMPANION_BITSET_AT_DOOR_FOR_SYNC_SCENE		 1
CONST_INT ciCOMPANION_BITSET_START_GOTO_FOR_SYNC_SCENE	 2
CONST_INT ciCOMPANION_BITSET_INSIDE_VEHICLE				 3
CONST_INT ciCOMPANION_BITSET_OPEN_DOOR_FOR_EXIT			 4
CONST_INT ciCOMPANION_BITSET_DOING_SPECIAL_ENTER_VEH	 5
CONST_INT ciCOMPANION_BITSET_DOING_SPECIAL_LEAVE_VEH	 6

STRUCT PED_COMPANION_TASK_DATA	
	
	INT iPed = -1
	NETWORK_INDEX pedIndex
	
	PLAYER_INDEX targetEntityFriend
	NETWORK_INDEX targetEntityFriendVehicle
	NETWORK_INDEX targetEntityFriendVehicleCached
	NETWORK_INDEX targetEntityEnemy
	
	PED_COMPANION_TASK_STATE ePedCompanionTaskState
	INT iSyncScene = -1
	INT iBS
	INT iTimeStamp_VehFailsafe
	INT iTimeStamp_EnterVeh
	INT iTimeStamp_MigrationRequest
	
	VECTOR vWarpLocation
	FLOAT fWarpHeading
ENDSTRUCT

INT iPedIsACompanion[ciFMMC_PED_BITSET_SIZE]
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Ped Ammo Drops
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT ciFMMC_MAX_PED_AMMO_DROPS 10
STRUCT PED_AMMO_DROP_DATA
	PICKUP_INDEX piAmmoPickup
	SCRIPT_TIMER tdLifeSpan
ENDSTRUCT
PED_AMMO_DROP_DATA sLocalPedAmmoDrop[ciFMMC_MAX_PED_AMMO_DROPS]
INT iStaggeredPedAmmoDrop
MODEL_NAMES mnPedAmmoDropModel = PROP_LD_AMMO_PACK_02

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Ped Speech triggering / Spooked / Aggro
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FMMC_SCRIPTED_ANIM_CONVERSATION_DATA sCurrentScriptedAnimConversationData
structPedsForConversation sScriptedAnimConversation_ConversationStruct
INT iScriptedAnimConversationState = ciScriptedAnimConversationState_Off

//BITSET USED TO TRIGGER PED SPEECH
INT iPedSpeechTriggeredBitset[FMMC_MAX_PEDS_BITSET]
INT iPedSpeechTriggeredBitsetNonReset[FMMC_MAX_PEDS_BITSET]
INT iPedSpeechAgroBitset[FMMC_MAX_PEDS_BITSET]
INT iPedDialogueRangeActive[FMMC_MAX_PEDS_BITSET]
INT iPedInitSpeechBitset[FMMC_MAX_PEDS_BITSET]
INT iPedSpeechAimBitset[FMMC_MAX_PEDS_BITSET]
INT iPedHasCharged[FMMC_MAX_PEDS_BITSET]

INT iPedAgroStageZero[FMMC_MAX_PEDS_BITSET]
INT iPedAgroStageOne[FMMC_MAX_PEDS_BITSET]
INT iPedAgroStageTwo[FMMC_MAX_PEDS_BITSET]

INT iPedForceSpookCancelTaskChecks[FMMC_MAX_PEDS_BITSET]

SCRIPT_TIMER PedAgroTimer[FMMC_MAX_PEDS]
SCRIPT_TIMER PedDelayDialogueTimer[FMMC_MAX_PEDS]
PED_INDEX DialoguePedToLookAt[FMMC_MAX_PEDS]
//used to swap between Idle Animation and Special Animation tasks
INT iPedPerformingSpecialAnimBS[FMMC_MAX_PEDS_BITSET]

INT iPedStartedSecondaryAnimBS[FMMC_MAX_PEDS_BITSET]
INT iPedCompletedSecondaryAnimBS[FMMC_MAX_PEDS_BITSET]
INT iPedStartedSpecialSecondaryAnimBS[FMMC_MAX_PEDS_BITSET]
INT iPedCompletedSpecialSecondaryAnimBS[FMMC_MAX_PEDS_BITSET]

//used to say if a ped has played their breakout animation (and probably shouldn't go back)
//INT ipedAnimBreakoutBS[FMMC_MAX_PEDS_BITSET] - now replaced by g_iFMMCPedAnimBreakoutBS
//used to make peds more perceptive during combat
INT iPedPerceptionBitset[FMMC_MAX_PEDS_BITSET]
//used to make peds less likely to warp into seats.
INT iPedSeatPriorityBitset[FMMC_MAX_PEDS_BITSET]
//used to task peds to turn towards the player when they hear them
INT iPedHeardBitset[FMMC_MAX_PEDS_BITSET]
//used to store whether a ped has seen a dead body or not
INT iPedSeenDeadBodyBitset[FMMC_MAX_PEDS_BITSET]
INT iDeadPedIDSeen[FMMC_MAX_PEDS] //Stores the PedID of the dead ped that this ped (in the array) has seen

//Set once a ped doing a cover task has gone into combat, so we know once this combat is finished we can go back to the cover task
INT iPedCoverTask_ReachedCombat[FMMC_MAX_PEDS_BITSET]

INT iPedGotoWaitBitset[FMMC_MAX_PEDS_BITSET]

INT iPedGotoAssProgress[FMMC_MAX_PEDS]

INT iPedGotoAssProgressOverrideProgress[FMMC_MAX_PEDS]
INT iPedGotoAssProgressOverridePool[FMMC_MAX_PEDS]
INT iPedGotoAssProgressOverrideInterrupt[FMMC_MAX_PEDS_BITSET]
INT iPedGotoAssProgressOverrideEventFired[FMMC_MAX_PEDS_BITSET]
INT iPedGotoAssProgressOverrideEventComplete[FMMC_MAX_PEDS_BITSET]

INT iPedGotoAssProgressFirstTime[FMMC_MAX_PEDS_BITSET]

INT iPedGotoAssCompletedWaitBitset[FMMC_MAX_PEDS_BITSET]
INT iPedGotoAssCompletedWaitPlayerVehExitBitset[FMMC_MAX_PEDS_BITSET]
INT iPedGotoAssCompletedAchieveHeadingBitset[FMMC_MAX_PEDS_BITSET]
INT iPedGotoAssCompletedWaitPlayIdleBitset[FMMC_MAX_PEDS_BITSET]
INT iPedGotoAssCompletedWaitCombatBitset[FMMC_MAX_PEDS_BITSET]
INT iPedGotoAssCompletedLeaveVehicleBitset[FMMC_MAX_PEDS_BITSET]
INT iPedGotoAssCompletedLandVehicleBitset[FMMC_MAX_PEDS_BITSET]
INT iPedGotoAssCompletedRappelFromVehBitset[FMMC_MAX_PEDS_BITSET]
INT iPedGotoAssCompletedHoverHeliBitset[FMMC_MAX_PEDS_BITSET]
INT iPedGotoAssCompletedEnterVehicleBitset[FMMC_MAX_PEDS_BITSET]
INT iPedGotoAssCompletedCleanupBitset[FMMC_MAX_PEDS_BITSET]
INT iPedGotoAssCompletedCombatGotoBitset[FMMC_MAX_PEDS_BITSET]
INT iPedGotoAssUsedWaitCombatBitset[FMMC_MAX_PEDS_BITSET]
INT iPedGotoAssCompletedCustomScenarioBitset[FMMC_MAX_PEDS_BITSET]
INT iPedGotoAssCompletedDisableLockOnBitset[FMMC_MAX_PEDS_BITSET]

INT iPedGotoAssInvincibilityBitset[FMMC_MAX_PEDS_BITSET]

INT iPedShouldBeForcedToCleanup[FMMC_MAX_PEDS_BITSET]

INT iPedLocalSpookedBitset[FMMC_MAX_PEDS_BITSET]
INT iPedLocalAggroedBitset[FMMC_MAX_PEDS_BITSET]
INT iPedLocalCancelTasksBitset[FMMC_MAX_PEDS_BITSET]
INT iPedLocalCheckedSpawnedAggroed[FMMC_MAX_PEDS_BITSET]
INT iPedZoneAggroed[FMMC_MAX_PEDS_BITSET]

INT iPedInvincibleOnRule[FMMC_MAX_PEDS_BITSET]

INT iPedLocalBoatSpeedSetEnteredOverrideRangeBoat[FMMC_MAX_PEDS_BITSET]
INT iPedLocalBoatSpeedSetOverrideSpeedBoat[FMMC_MAX_PEDS_BITSET]
INT iPedLocalBoatSpeedSetRegularSpeedBoat[FMMC_MAX_PEDS_BITSET]

//Events for spooked checks (spooked checks happen in a staggered loop, so check for these events every frame and then check this bitset in the staggered loop check):
INT iPedLocalReceivedEvent_ShotFiredORThreatResponse[FMMC_MAX_PEDS_BITSET]
INT iPedLocalReceivedEvent_SeenMeleeActionOrPedKilled[FMMC_MAX_PEDS_BITSET]
INT iPedLocalReceivedEvent_HurtOrKilled[FMMC_MAX_PEDS_BITSET]
INT iPedLocalReceivedEvent_VehicleDamageFromPlayer[FMMC_MAX_PEDS_BITSET]

// Ped Vehicle Crash Reactions
INT iPedAmbientSpeechDisabledForCrashReaction[FMMC_MAX_PEDS_BITSET]

//MOVE PED SPOOK BACK HERE

//Ped aggro reasons:
CONST_INT ciSPOOK_NONE								0
CONST_INT ciSPOOK_PLAYER_WITH_WEAPON				1
CONST_INT ciSPOOK_PLAYER_BLOCKED_VEHICLE			2
CONST_INT ciSPOOK_PLAYER_CAUSED_EXPLOSION			3
CONST_INT ciSPOOK_PLAYER_POTENTIAL_RUN_OVER			4
CONST_INT ciSPOOK_PLAYER_AIMED_AT					5
CONST_INT ciSPOOK_SHOTS_FIRED						6
CONST_INT ciSPOOK_DAMAGED							7
CONST_INT ciSPOOK_PLAYER_TOUCHING_ME				8
CONST_INT ciSPOOK_PLAYER_CAR_CRASH					9
CONST_INT ciSPOOK_PLAYER_TOUCHING_MY_VEHICLE		10
CONST_INT ciSPOOK_PLAYER_HEARD						11
CONST_INT ciSPOOK_SEEN_BODY							12
CONST_INT ciSPOOK_SEEN_PLAYER_NOT_IN_SECONDARY_ANIM	13
CONST_INT ciSPOOK_VEHICLE_ATTACHED					14
CONST_INT ciSPOOK_PLAYER_GENERIC					15

#IF IS_DEBUG_BUILD
BOOL bVerifiedCSModels

DEBUG_MOVERS_STRUCT sDebugMoversStruct

INT iPedsSpookedDebugWindow
INT iPedSpookOrderDebugWindow[FMMC_MAX_PEDS]
INT iPedSpookReasonDebugWindow[FMMC_MAX_PEDS]
INT iPedAggroOrderDebugWindow[FMMC_MAX_PEDS]
INT iPedsAggroedDebugWindow

INT iPedBSDebugWindow_AggroedFromAll[ciFMMC_PED_BITSET_SIZE]
INT iPedBSDebugWindow_SpookedFromAll[ciFMMC_PED_BITSET_SIZE]
#ENDIF


SCRIPT_TIMER tdSpookRangeTimer[FMMC_MAX_PEDS]
INT iPedLocalShouldHaveBlip[FMMC_MAX_PEDS_BITSET]
INT iPedProtectedFromWater[FMMC_MAX_PEDS_BITSET]
INT iPedCannotBeTargetedThisRule[FMMC_MAX_PEDS_BITSET]
INT iPedIgnoringSmallPitchChanges[FMMC_MAX_PEDS_BITSET]


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Shared Goto System ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Non-ped entity variables used in the Shared Goto system. 											  --------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT ciMAX_SHARED_ASSOCIATED_GOTO_TASK_DATA		6 // Needs to equal the highest number out of all the systems using these variables. Currently only drones use it. (ciMAX_FMMC_DRONES_ACTIVE). If multiple systems end up using these ints we will need to write something to "get free index", and set/clear them.
CONST_INT ciMAX_SHARED_ASSOCIATED_GOTO_TASK_DATA_BS		1

STRUCT LOCAL_SHARED_ASSOCIATED_GOTO_TASK_DATA
	// Indexes for Goto
	INT iProgress[ciMAX_SHARED_ASSOCIATED_GOTO_TASK_DATA]
	INT iProgressOverrideProgress[ciMAX_SHARED_ASSOCIATED_GOTO_TASK_DATA]
	INT iProgressOverridePool[ciMAX_SHARED_ASSOCIATED_GOTO_TASK_DATA]
	
	// Overall Goto
	INT iProgressOverrideInterrupt[ciMAX_SHARED_ASSOCIATED_GOTO_TASK_DATA_BS]
	INT iProgressOverrideEventFired[ciMAX_SHARED_ASSOCIATED_GOTO_TASK_DATA_BS]
	INT iProgressOverrideEventComplete[ciMAX_SHARED_ASSOCIATED_GOTO_TASK_DATA_BS]
	INT iReassignBitset[ciMAX_SHARED_ASSOCIATED_GOTO_TASK_DATA_BS]	
	
	// Toggles for overall goto
	INT iStartedGotoBitset[ciMAX_SHARED_ASSOCIATED_GOTO_TASK_DATA_BS]		
	INT iCompletedArrivedBitset[ciMAX_SHARED_ASSOCIATED_GOTO_TASK_DATA_BS]
	INT iCompletedWaitBitset[ciMAX_SHARED_ASSOCIATED_GOTO_TASK_DATA_BS]	
	INT iCompletedAchieveHeadingBitset[ciMAX_SHARED_ASSOCIATED_GOTO_TASK_DATA_BS]
	
	SCRIPT_TIMER tdWaitTimeStamp[ciMAX_SHARED_ASSOCIATED_GOTO_TASK_DATA]
ENDSTRUCT

LOCAL_SHARED_ASSOCIATED_GOTO_TASK_DATA sLocalSharedAssGotoTaskData

CONST_FLOAT ciFMMCDRONE_ACCEPTABLE_HEADING			4.5
CONST_FLOAT ciFMMCDRONE_BASE_SPEED					2.0
CONST_FLOAT ciFMMCDRONE_WOBBLE_RESET_THRESHOLD		0.2
CONST_FLOAT ciFMMCDRONE_ARRIVAL_THRESHOLD			0.23

CONST_INT ciFMMCDRONE_PATHFINDING_DATA_OUT_OF_DATE_TIME		10000

CONST_INT ciFMMCDRONE_COMBAT_TARGET_SWITCH					5250
CONST_INT ciFMMCDRONE_COMBAT_REFRESH_TIMER					1000
CONST_INT ciFMMCDRONE_COMBAT_LOS_LOST_PATHFINDING_TIMER		3000
CONST_FLOAT ciFMMCDRONE_COMBAT_ARRIVAL_RADIUS				5.0

CONST_FLOAT ciFMMCDRONE_ASCEND_DESCEND_QUICKLY_VELOCITY	50.0
CONST_FLOAT ciFMMCDRONE_ASCEND_DESCEND_GENTLY_VELOCITY	20.0
CONST_FLOAT ciFMMCDRONE_ASCEND_DESCEND_HEIGHT_EQUALISATION_THRESHOLD	1.25

// Base
CONST_FLOAT ciFMMCDRONE_BASE_SPEED_UP_VALUE				0.4
CONST_FLOAT ciFMMCDRONE_BASE_SPEED_DOWN_VALUE			0.4
// Combat
CONST_FLOAT ciFMMCDRONE_COMBAT_SPEED_UP_VALUE			0.6
CONST_FLOAT ciFMMCDRONE_COMBAT_SPEED_DOWN_VALUE			0.8
// Self Destruct
CONST_FLOAT ciFMMCDRONE_SELF_DESTRUCT_SPEED_UP_VALUE	0.6
CONST_FLOAT ciFMMCDRONE_SELF_DESTRUCT_SPEED_DOWN_VALUE	0.1

#IF IS_DEBUG_BUILD
BOOL bFMMcDroneDebugServer
BOOL bFMMcDroneDebugPosition
BOOL bFMMcDroneDebugPathfinding
BOOL bFMMcDroneDebugCombat
#ENDIF

CONST_INT ciFMMCDRONE_BS_RotationAdjusted			0
CONST_INT ciFMMCDRONE_BS_DroneOnFloor				1
CONST_INT ciFMMCDRONE_BS_Dead						2
CONST_INT ciFMMCDRONE_BS_Host_ModelSwapReserveObj	3
CONST_INT ciFMMCDRONE_BS_Lost_LOS					4

STRUCT LOCAL_FMMC_DRONE_WEAPON_DATA
	
	INT iAmmo
	INT iReloadingTimeStamp
	INT iShotTimeStamp
	
ENDSTRUCT

CONST_INT ciFMMCDRONE_PATHS_RIGHT					0
CONST_INT ciFMMCDRONE_PATHS_LEFT					1
CONST_INT ciFMMCDRONE_PATHS_MAX						2

CONST_INT ciFMMCDRONE_WAYPOINTS_MAX					15

STRUCT LOCAL_FMMC_DRONE_PATHFINDING_DATA	
	SHAPETEST_INDEX stiDroneWaypointChecks
	VECTOR vLastShapetestWaypointCollision
	VECTOR vDroneWaypoints[ciFMMCDRONE_WAYPOINTS_MAX]	
	FLOAT fAdjustmentAngle[ciFMMCDRONE_WAYPOINTS_MAX]
	BOOL bCompletedPath
ENDSTRUCT

STRUCT LOCAL_FMMC_DRONE_DATA
	INT iBS	
	VECTOR vHoverWobbleDir		
		
	VECTOR vTarget
	VECTOR vTargetLookAt
	VECTOR vLastShapetestCollision
	VECTOR vDefensivePoint	
	FLOAT fDistToStart
	FLOAT fDistToGoto
	FLOAT fArrivalRadius
	FLOAT fAccel = 0.25
	
	INT iTimeStampCollision
	INT iTimeStampTargetSwitch
	INT iTimeStampCorrectHeight	
	PLAYER_INDEX piPlayerTarget
	PED_INDEX pedPlayerTarget
	
	INT iCurrentWaypoint = 0
	INT iCurrentPath = -1
	INT iTimeStampValidWaypoints
	LOCAL_FMMC_DRONE_WEAPON_DATA sWeaponLocal[ciMAX_FMMC_DRONE_WEAPONS]
	
	INT iSoundID_SelfDestruction
	INT iTimeStampSwapModel
	
	LOCAL_FMMC_DRONE_PATHFINDING_DATA sPathfinding[ciFMMCDRONE_PATHS_MAX]
	LOCAL_FMMC_DRONE_PATHFINDING_DATA sPath
	
	SHAPETEST_INDEX stiDroneCollisionChecks
	INT iTimeStampShapetestTimer
	
	INT iTimeStampLostLOS
	INT iTimeStampContinueWithPath
	
	INT iFlightLoopSFX = -1		
ENDSTRUCT

LOCAL_FMMC_DRONE_DATA sLocalFmmcDroneData[ciMAX_FMMC_DRONES_ACTIVE]

STRUCT FMMC_DRONE_STATE	
	INT iIndex = -1
	VECTOR vPos
	VECTOR vRot
	FLOAT fHead	
		
	OBJECT_INDEX oiIndex
	NETWORK_INDEX niIndex
	
	BOOL bExists
	BOOL bAlive
	BOOL bBroken
	BOOL bHaveControl
	BOOL bActionActivated	
	BOOL bHealthThreshActivated
	
	MODEL_NAMES mn
	INT iEntityID
	INT iEntityType
ENDSTRUCT

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: (SAS) Stealth and Aggro Settings System -------------------------------------------------------------------------------------------------------------------------
// ##### Description: Most of the Spook and Aggro related functions shared between Client and Server are declared here.   --------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FLOAT fSASArousedSuspicionTime[FMMC_MAX_PEDS]
INT iPedAggroedWithScriptTrigger[FMMC_MAX_PEDS_BITSET]
INT iPedAggroedSASChecks[FMMC_MAX_PEDS_BITSET]
INT iPedAggroedWithScriptTriggerDelayed[FMMC_MAX_PEDS_BITSET]
INT iPedSASBlipCone[FMMC_MAX_PEDS_BITSET]
INT iPedSASCurrentlyActive[FMMC_MAX_PEDS_BITSET]
INT iPedSASCleanedup[FMMC_MAX_PEDS_BITSET]
INT iPedSASInitialised[FMMC_MAX_PEDS_BITSET]
INT iPedSASFinished[FMMC_MAX_PEDS_BITSET]
INT iPedSASBlipColour[FMMC_MAX_PEDS_BITSET]
INT iPedSASWithinSight[FMMC_MAX_PEDS_BITSET]
INT iPedSASUsingTrueSight[FMMC_MAX_PEDS_BITSET]
INT iPedSASTriggeredLocally[FMMC_MAX_PEDS_BITSET]

// For staggered processing.
CONST_INT ciSAS_PED_DELAYED_SIGHT_CHECK_RESET_STAGGER		0
CONST_INT ciSAS_PED_INSTANT_SIGHT_CHECK_RESET_STAGGER		3	
CONST_INT ciSAS_PED_LOS_SIGHT_CHECK_RESET_STAGGER			6
CONST_INT ciSAS_PED_RELOOP_SIGHT_CHECK_STAGGER				9

INT iSASPlayerInPerceptionAreaBS[FMMC_MAX_PEDS_BITSET]
INT iSASPlayerInPerceptionAreaCheckedBS[FMMC_MAX_PEDS_BITSET]
INT iSASPlayerInPerceptionInstantAreaBS[FMMC_MAX_PEDS_BITSET]
INT iSASPlayerInPerceptionInstantAreaCheckedBS[FMMC_MAX_PEDS_BITSET]
INT iSASPlayerInLOSInstantAreaBS[FMMC_MAX_PEDS_BITSET]
INT iSASPlayerInLOSInstantAreaCheckedBS[FMMC_MAX_PEDS_BITSET]
INT iSAS_StaggerIterator = -1

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Objective end / failure
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//Objective end reasons
CONST_INT OBJ_END_REASON_LOC_CAPTURED				0
CONST_INT OBJ_END_REASON_PED_CAPTURED				1	
CONST_INT OBJ_END_REASON_VEH_CAPTURED				2
CONST_INT OBJ_END_REASON_OBJ_CAPTURED				3
CONST_INT OBJ_END_REASON_PED_DELIVERED				4	
CONST_INT OBJ_END_REASON_VEH_DELIVERED				5
CONST_INT OBJ_END_REASON_OBJ_DELIVERED				6
CONST_INT OBJ_END_REASON_PED_DEAD					7	
CONST_INT OBJ_END_REASON_VEH_DEAD					8
CONST_INT OBJ_END_REASON_OBJ_DEAD					9
CONST_INT OBJ_END_REASON_PLAYERS_KILLED				10
CONST_INT OBJ_END_REASON_PROTECTED_PED              11
CONST_INT OBJ_END_REASON_PROTECTED_VEH              12
CONST_INT OBJ_END_REASON_PROTECTED_OBJ              13
CONST_INT OBJ_END_REASON_ARV_LOC             		14
CONST_INT OBJ_END_REASON_ARV_PED             		15
CONST_INT OBJ_END_REASON_ARV_VEH             		16
CONST_INT OBJ_END_REASON_ARV_OBJ             		17
CONST_INT OBJ_END_REASON_TIME_EXPIRED             	18
CONST_INT OBJ_END_REASON_TARGET_SCORE               19
CONST_INT OBJ_END_REASON_PHOTO_LOC					20
CONST_INT OBJ_END_REASON_PHOTO_PED					21
CONST_INT OBJ_END_REASON_PHOTO_VEH					22
CONST_INT OBJ_END_REASON_PHOTO_OBJ					23
CONST_INT OBJ_END_REASON_HACK_OBJ                   24
CONST_INT OBJ_END_REASON_OUT_OF_LIVES               25
CONST_INT OBJ_END_REASON_PED_ARRIVED                26
CONST_INT OBJ_END_REASON_TEAM_GONE                  27
CONST_INT OBJ_END_REASON_LOST_COPS                  28
CONST_INT OBJ_END_REASON_PLAYERS_GOT_MASKS          29

CONST_INT OBJ_END_REASON_PED_AGRO                   31
CONST_INT OBJ_END_REASON_PED_FOUND_BODY				32

CONST_INT OBJ_END_REASON_HEIST_LEADER_LEFT          34
CONST_INT OBJ_END_REASON_HEIST_TEAM_GONE			35
CONST_INT OBJ_END_REASON_HEIST_TEAM_GONE_T0         36
CONST_INT OBJ_END_REASON_HEIST_TEAM_GONE_T1			37
CONST_INT OBJ_END_REASON_HEIST_TEAM_GONE_T2			38
CONST_INT OBJ_END_REASON_HEIST_TEAM_GONE_T3			39
CONST_INT OBJ_END_REASON_LVE_LOC             		40
CONST_INT OBJ_END_REASON_GAINED_WANTED 				41
CONST_INT OBJ_END_REASON_NO_AMMO                    42
CONST_INT OBJ_END_REASON_LOCATE_WRONG_ANGLE         43
CONST_INT OBJ_END_REASON_MOD_SHOP_CLOSED			44
CONST_INT OBJ_END_REASON_PLAYERS_GONE_TO			45
CONST_INT OBJ_END_REASON_LOOT_GRABBED				46
CONST_INT OBJ_END_REASON_PED_DAMAGED				47
CONST_INT OBJ_END_REASON_VEH_DAMAGED				48
CONST_INT OBJ_END_REASON_OBJ_DAMAGED				49
CONST_INT OBJ_END_REASON_POINTS_THRESHOLD_MET		50
CONST_INT OBJ_END_REASON_POINTS_RULE_HELD			51

ENUM eFailMissionEnum 
	 mFail_none 					= 0
	,mFail_LOC_CAPTURED				
	,mFail_PED_CAPTURED		
	,mFail_VEH_CAPTURED		
	,mFail_OBJ_CAPTURED		
	,mFail_PED_DELIVERED		
	,mFail_VEH_DELIVERED		
	,mFail_OBJ_DELIVERED		
	,mFail_PED_DEAD			
	,mFail_VEH_DEAD			
	,mFail_OBJ_DEAD					//10		
	,mFail_PLAYERS_KILLED		
	,mFail_PROTECTED_PED     
	,mFail_PROTECTED_VEH     
	,mFail_PROTECTED_OBJ     
	,mFail_ARV_LOC           
	,mFail_ARV_PED           
	,mFail_ARV_VEH           
	,mFail_ARV_OBJ           
	,mFail_TIME_EXPIRED   
	,mFail_MULTI_TIME_EXPIRED		//20 
	,mFail_TARGET_SCORE     
	,mFail_PHOTO_LOC			
	,mFail_PHOTO_PED			
	,mFail_PHOTO_VEH			
	,mFail_PHOTO_OBJ			
	,mFail_HACK_OBJ          
	,mFail_OUT_OF_LIVES      
	,mFail_PED_ARRIVED       	
	,mFail_TEAM_GONE         
	,mFail_LOST_COPS         		//30
	,mFail_PLAYERS_GOT_MASKS 
	,mFail_CHARM_PED         
	,mFail_PED_AGRO_T0   
	,mFail_PED_AGRO_T1 
	,mFail_PED_AGRO_T2 
	,mFail_PED_AGRO_T3 
	,mFail_PED_FOUND_BODY		
	,mFail_PLAYERS_ARRESTED  
	,mFail_HEIST_LEADER_LEFT 
	,mFail_HEIST_TEAM_GONE			//40
	,mFail_HEIST_TEAM_GONE_T0 	 
	,mFail_HEIST_TEAM_GONE_T1
	,mFail_HEIST_TEAM_GONE_T2
	,mFail_HEIST_TEAM_GONE_T3
	,mFail_YOU_LEFT
	,mFail_LVE_LOC     
	,mFail_OUT_OF_BOUNDS 
	,mFail_GAINED_WANTED 		
	,mFail_NO_AMMO           
	,mFail_LOCATE_WRONG_ANGLE  		// 50
	,mFail_T0MEMBER_DIED
	,mFail_T1MEMBER_DIED
	,mFail_T2MEMBER_DIED
	,mFail_T3MEMBER_DIED
	,mFail_HEIST_TEAM_T0_FAILED 	
	,mFail_HEIST_TEAM_T1_FAILED 
	,mFail_HEIST_TEAM_T2_FAILED 
	,mFail_HEIST_TEAM_T3_FAILED
	,mFail_MOD_SHOP_CLOSED
	,mFail_GAPS_IN_TEAMS			//60
	,mFail_CEO_GONE
	,mFail_MC_PRESIDENT_GONE
	,mFail_SCORE_CANT_BE_MATCHED
	,mFail_ALL_TEAMS_FAIL
	,mFail_OTHER_TEAM_HAS_ALL_OBJECTS
	,mFail_TOWERS_ARE_DESTROYED
	,mFail_LOBBY_LEADER_GONE
	,max_MissionFails //68
ENDENUM
//eFailMissionEnum eCurrentTeamFail[FMMC_MAX_TEAMS] //moved to broadcast data 3

//player fail reasons to end before team
CONST_INT PLAYER_FAIL_REASON_BOUNDS             0
CONST_INT PLAYER_FAIL_OUT_OF_LIVES              1





// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Arena Wars
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//Arena Spawn Fix
VECTOR vArenaSpawnVec
FLOAT fArenaSpawnHead
ENUM eArenaSpawnFixStage
	eArenaSpawnFix_Idle = 0,
	eArenaSpawnFix_FadeOut = 1,
	eArenaSpawnFix_Warp = 2,
	eArenaSpawnFix_Wait = 3,
	eArenaSpawnFix_Sync = 4,
	eArenaSpawnFix_FadeIn = 5,
	eArenaSpawnFix_Progress = 6
ENDENUM
SCRIPT_TIMER tdArenaSpawnFixTimer
SCRIPT_TIMER tdArenaSpawnFixSafety
SCRIPT_TIMER tdArenaSpawnFadeInTimer




// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: CCTV
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

ENUM CCTV_EVENT_TYPE
	CCTV_EVENT_TYPE__SPOTTED_PLAYER,
	CCTV_EVENT_TYPE__SPOTTED_PED_BODY,
	CCTV_EVENT_TYPE__REPORTED_BY_PED
ENDENUM

CONST_FLOAT cfCCTVTurnSpeedScalar	12.5
CONST_FLOAT cfCCTVRotationTargetBuffer		5.0
CONST_INT MAX_NUM_CCTV_CAM 32
FLOAT fCCTVCamInitialHeading[MAX_NUM_CCTV_CAM]
CONST_INT ci_CCTV_FINISHED_TURN_CAM_0 0
ENUM CCTVCameraTurnState
	CamState_Wait, 		//0
	CamState_Turning 	//1
ENDENUM

INT iSeenCCTVCameraSpottedSomethingHelpTextBS
INT iCCTVCameraSpottedPlayerBS
INT iCCTVCameraSpottedBodyBS
INT iCCTVCameraReportedByPedBS
INT iSoundAlarmCCTV[MAX_NUM_CCTV_CAM]
INT iSoundAlarmType[MAX_NUM_CCTV_CAM]

SCRIPT_TIMER CCTVCameraTurnTimer[MAX_NUM_CCTV_CAM]
SCRIPT_TIMER CCTVSpottingPlayerTimer[MAX_NUM_CCTV_CAM]
SCRIPT_TIMER CCTVReportedByPedTimer[MAX_NUM_CCTV_CAM]
INT iCCTVCameraSpottedPlayer[MAX_NUM_CCTV_CAM]

SCRIPT_TIMER CCTVSpottingBodyTimer[MAX_NUM_CCTV_CAM]
INT iCCTVCameraSpottedPedBody[MAX_NUM_CCTV_CAM]

FLOAT fCCTVCameraCachedGroundZ[MAX_NUM_CCTV_CAM]

SCRIPT_TIMER CCTVTaserReEnablingTimer[MAX_NUM_CCTV_CAM]
INT iCCTVReachedDestinationBS

INT iCCTVCamsCurrentlyTasered = 0

INT iStaggeredCCTVPedToCheck[MAX_NUM_CCTV_CAM]
INT iCCTVConeBitSet
INT iCCTVBrokenBS
CONST_INT ci_CCTV_CONE_BLIP_ADDED_0		0

CONST_FLOAT cf_CCTV_BaseLightIntensity		2.0
CONST_FLOAT cf_CCTV_FlickerLightIntensity	14.0
CONST_FLOAT cf_CCTV_TaserFlashIntensity		29.0
CONST_FLOAT cf_CCTV_LightLerpSpeed			10.0
CONST_FLOAT cf_CCTV_TaserLightFadeSpeed		31.50
CONST_FLOAT cf_CCTV_ReenableFlickerTime		0.500
//FLOAT fCCTV_CurrentLightIntensity[MAX_NUM_CCTV_CAM]
//FLOAT fCCTV_CurrentTargetLightIntensity[MAX_NUM_CCTV_CAM]

INT iCCTVStaggeredDialogueTriggerIndex
INT iCCTVInDialogueRangeBS[ciMAX_DIALOGUE_BIT_SETS]

INT iObjCachedCamIndexes[FMMC_MAX_NUM_OBJECTS]
INT iDynoPropCachedCamIndexes[FMMC_MAX_NUM_DYNOPROPS]

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entity Audio
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INT iObjectCachedAudioBitset_PROCESSED_ON_CREATE
INT iObjectCachedAudioBitset_PROCESSED_ON_DESTROY

INT iInteractableCachedAudioBitset_PROCESSED_ON_CREATE[FMMC_MAX_INTERACTABLE_BITSET]
INT iInteractableCachedAudioBitset_PROCESSED_ON_DESTROY[FMMC_MAX_INTERACTABLE_BITSET]

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Fuse Minigame
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT ciFUSE_MINIAME_INDEXES__FUSEBOX	0
CONST_INT ciFUSE_MINIAME_INDEXES__FUSE_1	1
CONST_INT ciFUSE_MINIAME_INDEXES__FUSE_2	2
CONST_INT ciFUSE_MINIAME_INDEXES__FUSE_3	3
CONST_INT ciFUSE_MINIAME_INDEXES__FUSE_4	4
CONST_INT ciFUSE_MINIAME_INDEXES__MAX		5

INT iFuseMinigameObjIndexes[ciFUSE_MINIAME_INDEXES__MAX]

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Celebration screen
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//Celebration Screen
STRUCT CELEBRATION_STATS
	PLAYER_INDEX winningPlayer			//The index of the overall winner (or the top player from the winning team).
	INT iLocalPlayerCash				
	INT iLocalPlayerScore				//This is the score used to determine if this player beat a challenge for this mission. It may be a time value rather than score, but either can be put in here.
	INT iLocalPlayerJobPoints			
	INT iLocalPlayerXP					
	INT iLocalPlayerBetWinnings			//How much money the player won/lost on bets
	INT iLocalPlayerPosition			//If in teams this is just the position of the player's team, otherwise it's the player's leaderboard position.
	BOOL bPassedMission					
	BOOL bIsDraw						//I don't know if you can draw a mission but there's a screen for it so just in case you can then this variable covers it.
	STRING strFailReason
	TEXT_LABEL_63 strFailLiteral
	TEXT_LABEL_63 strFailLiteral2
ENDSTRUCT

BOOL bCreatedCustomCelebrationCam

CONST_INT ciCELEBRATION_BIT_SET_SCRIPTED_CUT_NEEDS_CLEANED_UP 			0
CONST_INT ciCELEBRATION_BIT_SET_bCelebrationSummaryComplete				1
CONST_INT ciCELEBRATION_BIT_SET_bStartedCelebrationBeforeCutsceneEnded	2
CONST_INT ciCELEBRATION_BIT_SET_bCelebrationUnfrozeSkycam				3
CONST_INT ciCELEBRATION_BIT_SET_bDoDelayedCelebrationFreeze				4
CONST_INT ciCELEBRATION_BIT_SET_bClosedScPage							5
CONST_INT ciCELEBRATION_BIT_SET_TriggerHeistWinnerCutscene				6
CONST_INT ciCELEBRATION_BIT_SET_TriggeredHeistWinnerCutscene			7
CONST_INT ciCELEBRATION_BIT_SET_TriggerEndCelebration					8
CONST_INT ciCELEBRATION_BIT_SET_EndCelebrationFinished					9
CONST_INT ciCELEBRATION_BIT_SET_TriggeredCelebrationStreamingRequests	10
CONST_INT ciCELEBRATION_BIT_SET_StoppedAllPostFxForSwitch				11
CONST_INT ciCELEBRATION_BIT_SET_TriggeredRenderPhasePause				12
CONST_INT ciCELEBRATION_BIT_SET_ClearedTasksForOutOfVehicle				13
CONST_INT ciCELEBRATION_BIT_SET_NeedToCheckForApartmentWalkInComplete	15
CONST_INT ciCELEBRATION_BIT_SET_NotDoingApartmentInvite					16
CONST_INT ciCELEBRATION_BIT_SET_FrozenRenderPhasesForFail				17
CONST_INT ciCELEBRATION_BIT_SET_PlacedPlayerForLowriderPostScene		18

#IF IS_DEBUG_BUILD

FUNC STRING GET_CELEBRATION_BIT_SET_NAME(INT iBit)
	SWITCH iBit
		CASE ciCELEBRATION_BIT_SET_SCRIPTED_CUT_NEEDS_CLEANED_UP 			RETURN "SCRIPTED_CUT_NEEDS_CLEANED_UP"
		CASE ciCELEBRATION_BIT_SET_bCelebrationSummaryComplete 				RETURN "bCelebrationSummaryComplete"
		CASE ciCELEBRATION_BIT_SET_bStartedCelebrationBeforeCutsceneEnded 	RETURN "bStartedCelebrationBeforeCutsceneEnded"
		CASE ciCELEBRATION_BIT_SET_bCelebrationUnfrozeSkycam 				RETURN "bCelebrationUnfrozeSkycam"
		CASE ciCELEBRATION_BIT_SET_bDoDelayedCelebrationFreeze 				RETURN "bDoDelayedCelebrationFreeze"
		CASE ciCELEBRATION_BIT_SET_bClosedScPage 							RETURN "bClosedScPage"
		CASE ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene 				RETURN "TriggerHeistwinnerCutscene"
		CASE ciCELEBRATION_BIT_SET_TriggeredHeistwinnerCutscene 			RETURN "TriggeredHeistwinnerCutscene"
		CASE ciCELEBRATION_BIT_SET_TriggerEndCelebration 					RETURN "TriggerEndCelebration"
		CASE ciCELEBRATION_BIT_SET_EndCelebrationFinished 					RETURN "EndCelebrationFinished"
		CASE ciCELEBRATION_BIT_SET_TriggeredCelebrationStreamingRequests 	RETURN "TriggeredCelebrationStreamingRequests"
		CASE ciCELEBRATION_BIT_SET_StoppedAllPostFxForSwitch 				RETURN "StoppedAllPostFxForSwitch"
		CASE ciCELEBRATION_BIT_SET_TriggeredRenderPhasePause 				RETURN "TriggeredRenderPhasePause"
		CASE ciCELEBRATION_BIT_SET_ClearedTasksForOutOfVehicle 				RETURN "ClearedTasksForOutOfVehicle"
		CASE ciCELEBRATION_BIT_SET_NeedToCheckForApartmentWalkInComplete 	RETURN "NeedToCheckForApartmentWalkInComplete"
		CASE ciCELEBRATION_BIT_SET_NotDoingApartmentInvite 					RETURN "NotDoingApartmentInvite"
		CASE ciCELEBRATION_BIT_SET_FrozenRenderPhasesForFail 				RETURN "FrozenRenderPhasesForFail"
		CASE ciCELEBRATION_BIT_SET_PlacedPlayerForLowriderPostScene 		RETURN "PlacedPlayerForLowriderPostScene"
		DEFAULT RETURN "invalid" 
	ENDSWITCH
ENDFUNC

#ENDIF

INT iCelebrationBitSet
CELEBRATION_STATS sCelebrationStats
CELEBRATION_SCREEN_DATA sCelebrationData
CAMERA_INDEX camEndScreen

// For winner scene. 
INT iCelebrationTeamNameIsNotLiteral

//Participant rankings for Heists end screen

STRUCT MEDAL_RANKINGS_STRUCT
	BOOL bOnMission = FALSE
	INT iPartIndex = -1
	INT iScore = 0
ENDSTRUCT 

CONST_INT ci_MEDALRANK_PLATINUM 	0
CONST_INT ci_MEDALRANK_GOLD			1
CONST_INT ci_MEDALRANK_SILVER		2
CONST_INT ci_MEDALRANK_BRONZE		3
CONST_INT ci_MEDALRANK_BAD			4
CONST_INT ci_MEDALRANK_MAX			ci_MEDALRANK_BAD

INT iPartMedalRanks[ci_MEDALRANK_BAD]

STRUCT sEarlyCelebrationData
	BOOL bShouldShowPrematurely = FALSE
	INT iTeam 					= -1
	INT iTeamWinningPoints 		= 0
ENDSTRUCT

ENUM HEIST_CELEBRATION_PLAYER_VARIATIONS_STATE
	HCPV_SetPedVariations = 0,
	HCPV_WaitForStreamingRequests,
	HCPV_WaitForOtherPlayers,
	HCPV_WaitForVariationUpdates,
	HCPV_Finished
ENDENUM
HEIST_CELEBRATION_PLAYER_VARIATIONS_STATE eHeistCelebrationPlayerVariationsState = HCPV_SetPedVariations
SCRIPT_TIMER timerCelebReceivedAllPlayerVariationUpdates

INT iFreezeCamForPlaceDcamStage

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Pickups/Weapons
// ##### Description: Placed Pickup vars and consts
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CONST_FLOAT cfPickupDropFromVehicleVelocity		10.0
CONST_FLOAT cfPickupDropFromVehicleDistance		1.0
INT iPickupDroppedFromVehicleBS

INT iPickupUncollectableBS[FMMC_MAX_WEAPON_BITSET]

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Objects
// ##### Description: Placed Object vars and consts
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INT iLastDeliveredContinuityObjectThisRule = -1
INT iContinuityLastHeldObjectIndex = -1

INT iObjectToPickUpASAP = -1
INT iObjWasAttachedEarlyBS

INT iCachedCarriedObjectsBS
INT iObjDeliveryOverridesObjToUse = -1
INT iSentObjectCollectionTickerBS

CONST_INT ciObjectSafeDropPoints	2
CONST_FLOAT cfObjectSafeDropPointRequiredDistance	7.5
VECTOR vObjectLastSafeDropPoint[FMMC_MAX_NUM_OBJECTS][ciObjectSafeDropPoints]
CONST_FLOAT cfObjectDangerousDropPointRequiredDistance	15.0
VECTOR vObjectLastDangerousDropPoint[FMMC_MAX_NUM_OBJECTS]
INT iObjectInDangerousPositionBS
INT iObjOnGroundOutOfBoundsBS
INT iObjNeedsToCheckOutOfBoundsBS
SCRIPT_TIMER stObjectReturnToSafeDropPointCooldownTimer[FMMC_MAX_NUM_OBJECTS]
CONST_INT ciObjectReturnToSafeDropPointCooldownTimer_Length		10000

INT iObjectHasBeenPlacedIntoInteriorBS

INT iObjOverrideInvisibilitySettingBS

INT iFlammableObjScriptedDamageStartedBS

PTFX_ID ptObjectPTFX[FMMC_MAX_NUM_OBJECTS]

INT iIgnoreObjectBlipRangeBS

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Dynoprop
// ##### Description: Placed Dynoprop vars and consts
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
MODEL_NAMES mnCachedDynopropModelNames[FMMC_MAX_NUM_DYNOPROPS]
INT iBreakOnFloorDynopropInMotionBS
INT iBreakOnFloorDynopropBrokenBS

INT iDynopropHasBeenPlacedIntoInteriorBS

PTFX_ID ptDynopropPTFX[FMMC_MAX_NUM_DYNOPROPS]

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Interiors
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT ciROOM_KEY_ISLAND_HEIST_VAULT	1976745188

CONST_INT ciINTERIOR_HASH_MUSIC_LOCKER	-247740272

INTERIOR_INSTANCE_INDEX iCurrentInterior 
//INTERIOR_INSTANCE_INDEX iMocapInterior //used for 2104332
INTERIOR_INSTANCE_INDEX iCutInterior  //Used for non-apartment interiors

INTERIOR_INSTANCE_INDEX interiorWarp[MAX_BUILDING_WARPS]

VECTOR vSubmarineInteriorCoords = <<2047.0, 2942.0, -61.9>>
VECTOR vHangarInteriorCoords = <<-1266.8022, -3014.8491, -49.4903>>
BOOL bIsDisplacedInteriorSet = FALSE
INT iCommonDisplacementInterior = -1

INTERIOR_INSTANCE_INDEX iArenaInterior_VIPLoungeIndex

CONST_INT ci_IPL_INTERIOR_LOAD_SAFETY_TIMEOUT 20000
CONST_INT ci_MAX_INTERIORS_TO_LOAD 10
INT iInteriorInitializedBS = 0
INT iInteriorInitializingExtrasBS[INTERIOR_BITSETS]

CONST_INT ciWAIT_FOR_INTERIOR_SCRIPTS_TIMEOUT	15000
SCRIPT_TIMER tdWaitForInteriorScriptsTimeout

BOOL bIgnoreInteriorCheckForSprinting = FALSE

INTERIOR_INSTANCE_INDEX interiorCasinoApartmentIndex

INTERIOR_INSTANCE_INDEX iInteriorIndexCasinoMainCH
INTERIOR_INSTANCE_INDEX iInteriorIndexCasinoTunnel
INTERIOR_INSTANCE_INDEX iInteriorIndexCasinoBackArea
INTERIOR_INSTANCE_INDEX iInteriorIndexCasinoHotelFloor
INTERIOR_INSTANCE_INDEX iInteriorIndexCasinoLoadingBay
INTERIOR_INSTANCE_INDEX iInteriorIndexCasinoVault
INTERIOR_INSTANCE_INDEX iInteriorIndexCasinoUtilityLift
INTERIOR_INSTANCE_INDEX iInteriorIndexCasinoUtilityShaft
INTERIOR_INSTANCE_INDEX iInteriorIndexMusicLocker

CONST_INT ciMAX_MUSIC_LOCKER_LIGHTS 6
OBJECT_INDEX oiMusicLockerLights[ciMAX_MUSIC_LOCKER_LIGHTS]
MODEL_NAMES mnMusicLockerLight = INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_club_emis_rig_10"))
CONST_INT ciMusicLockerRoomKey -1357334417
VECTOR vMusicLockerLocation = <<1550, 250, -50>>

CONST_INT ciMansionSearchHash 142194897

CONST_INT ciMISSION_MOC_STATE__INIT						0
CONST_INT ciMISSION_MOC_STATE__REGISTER_RENDER_TARGETS	1
CONST_INT ciMISSION_MOC_STATE__LINK_RENDER_TARGETS		2
CONST_INT ciMISSION_MOC_STATE__PROCESS					3
INT iMissionMOCState = ciMISSION_MOC_STATE__INIT
INT iMissionMOCRenderTargetID_TV = -1
INT iMissionMOCRenderTargetID_Monitor1 = -1
INT iMissionMOCRenderTargetID_Monitor2 = -1
INT iMissionMOCRenderTargetID_Monitor3 = -1
MISSION_MOC_PROPS sMissionMOCProps

STRUCT INTERIOR_DESTRUCTION_VARS
	INTERIOR_INSTANCE_INDEX intInteriorToDestroy
	SCRIPT_TIMER stInteriorDestructionTimer
	INT iCachedInteriorDestructionTimeRemaining
ENDSTRUCT
INTERIOR_DESTRUCTION_VARS sInteriorDestructionVars[ciMAX_INTERIOR_DESTRUCTIONS]
CONST_FLOAT cfInteriorDestruction_TimerDistance		15.0
CONST_INT ciInteriorDestruction_BlockPortalTime		10000
INT iInteriorDestruction_BlockWarpPortalStartPosBS

FMMC_INTERIOR_FIX_STRUCT sInteriorFixStruct


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Bunker Entry
// ##### Description: Anim/Cutscene related simple interior stuff to handle bunker entry.
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT ciREPOSITION_ORIGINAL_ENTITIES_PLAYER_0	0
CONST_INT ciREPOSITION_ORIGINAL_ENTITIES_PLAYER_1	1
CONST_INT ciREPOSITION_ORIGINAL_ENTITIES_PLAYER_2	2
CONST_INT ciREPOSITION_ORIGINAL_ENTITIES_PLAYER_3	3
CONST_INT ciEXTERNAL_CUTSCENE_BITSET_FADED_IN		4
CONST_INT ciEXTERNAL_SET_VEH_ON_GROUND_PROPERLY		5

INT iExternalCutsceneBitset

ENUM EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE
	EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_IDLE,
	EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_LOAD,
	EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP,
	EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN,
	EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_CLEANUP	
ENDENUM
EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE eExternalScriptedCutsceneSequenceState

INT iScriptedCutsceneSimpleInteriorSyncSceneTimeStamp = 0
INT iScriptedCutsceneSimpleInteriorSyncScene = -1
SIMPLE_INTERIOR_ENTRY_ANIM_DATA entryAnimExtCut
SIMPLE_INTERIORS simpInteriorCache

CONST_INT ciSAFETY_TIMER_EXTERNAL_CUTSCENES 	30000
SCRIPT_TIMER tdSafetyTimerExternalCutscenes

CONST_INT ciMINIMAL_CUTSCENE_LENGTH_FOR_OBJECTIVE_PROGRESSION	5000

// Used as a contingency in case we get stuck processing a part of a cutscene.
CONST_INT ciSAFETY_TIMER_CUTSCENES_LENGTH 			100000
CONST_INT ciSAFETY_TIMER_MOPCAP_CUTSCENES_LENGTH 	30000
SCRIPT_TIMER tdSafetyTimerCutsceneLengthLimit
INT iMocapCutsceneSafetyLengthTime
 
OBJECT_INDEX oibunkerEntryDoor
OBJECT_INDEX oiBunkerEntryFrame 

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Beast Mode
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INT iHealthBeforeStealth
INT iBeastPowersUnavailableDisplayCount = 0
SCRIPT_TIMER tdStealthDamageTimer
SCRIPT_TIMER tdLocationBlipTimer
SCRIPT_TIMER tdStealthToggleTimer
INT iBeastAttackSoundID = -1
INT iBeastSprintSoundID = -1
INT iThermalSoundID = -1
INT iNightVisSoundID = -1
INT iStealthSoundID = -1
BOOL bBeastBeenDamaged = FALSE
BOOL bBeastKilledPlayer = FALSE

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Health Drain Zones
// ##### Description: Variables used by Health Drain Zones & related effects and sounds
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_FLOAT fHealthDrainZoneTickSpeed			2.5
FLOAT fHealthDrainZoneDamageBuildup = 0.0

SCRIPT_TIMER stHealthDrainCoughCooldownTimer
INT iHealthDrainCoughAnim = -1
CONST_INT ciHealthDrainCoughCooldownLength						5500
CONST_FLOAT cfHealthDrainCoughChance							0.095
CONST_FLOAT cfHealthDrainDamageBuildupRate						2.0
CONST_INT ciHealthDrainDamagePerTick							12

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Poison Gas
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INT iPoisonGasBS
CONST_FLOAT cfPoisonGasFXTransitionTimeInstant			0.0
CONST_FLOAT cfPoisonGasFXTransitionTimeShort			1.0
CONST_FLOAT cfPoisonGasFXTransitionTimeNormal			10.0
CONST_FLOAT cfPoisonGasFXTransitionTimeExtended			75.0
CONST_INT ciPoisonGasAppliedScreenFX					0
CONST_INT ciPoisonGasActivatedByScript 					1

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Metal Detector
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FLOAT fPedHeadingBeforeMetalDetector[FMMC_MAX_PEDS]
SCRIPT_TIMER stPedMetalDetectorInvestigationTimer[FMMC_MAX_PEDS]
CONST_INT ciPedMetalDetectorInvestigationTime		5500


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Phone EMP
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

ENUM PHONE_EMP_STATE
	PHONE_EMP_STATE__INIT,
	PHONE_EMP_STATE__LOADING_ASSETS,
	PHONE_EMP_STATE__WAITING,
	PHONE_EMP_STATE__HACKING,
	PHONE_EMP_STATE__TRIGGERING,
	PHONE_EMP_STATE__WAITING_TO_HANG_UP,
	PHONE_EMP_STATE__WAITING_TO_DEACTIVATE,
	PHONE_EMP_STATE__DEACTIVATING,
	PHONE_EMP_STATE__FINISHED
ENDENUM
PHONE_EMP_STATE ePhoneEMPState = PHONE_EMP_STATE__INIT

FLOAT fPhoneEMPCharge = 0.0
CONST_FLOAT cfPhoneEMP_ChargeTimeInSeconds		3.0
SCRIPT_TIMER stPhoneEMP_ActivationDelayTimer
SCRIPT_TIMER stPhoneEMP_DeactivationTimer

CONST_INT ciPhoneEMP_EMPActivateDelay	2000
CONST_INT ciPhoneEMP_HangupTime			3500

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Casino Daily Cash Room Door
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

ENUM CASINO_DAILY_CASH_DOOR_STATE
	CASINO_DAILY_CASH_STATE__INIT,
	CASINO_DAILY_CASH_STATE__WAITING,
	CASINO_DAILY_CASH_STATE__OPENING,
	CASINO_DAILY_CASH_STATE__CLOSING,
	CASINO_DAILY_CASH_STATE__TRANSITION_CLOSE,
	CASINO_DAILY_CASH_STATE__TRANSITION_OPEN
ENDENUM
CASINO_DAILY_CASH_DOOR_STATE eCasinoDailyCashState

CONST_FLOAT cfCasinoDailyDoor_DoorOpenDeceleration					0.08


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Rappeling
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

ENUM RAPPEL_CAM_STATE
	RAPPEL_CAM_CREATE,
	RAPPEL_CAM_MASTER,
	RAPPEL_CAM_CLEANUP
ENDENUM

CONST_INT ciRappel_PreCamSetup				4

STRUCT WALL_RAPPEL_STRUCT
	ROPE_INDEX RappelRopeID
	RAPPEL_CAM_STATE eRappelCamState
	CAMERA_INDEX camRappelCam
	BOOL bRappelLookBehindCam
	INT iRappelBitset
	SCRIPT_TIMER tdFadeTimer
ENDSTRUCT
WALL_RAPPEL_STRUCT sWallRappel

SCRIPT_CONTROL_HOLD_TIMER sRappelButtonHold
SCRIPT_TIMER tdRappelStabilizeTimer


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cop decoy
// ##### Description: Blip declarations
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//Cop Decoy
//------------
CONST_INT ci_COP_DECOY_CREATED_BLIP 			0
CONST_INT ci_COP_DECOY_DEDUCT_1_STAR 			1
CONST_INT ci_COP_DECOY_DEDUCT_2_STAR 			2
CONST_INT ci_COP_DECOY_DEDUCTED_STAR_1 			3
CONST_INT ci_COP_DECOY_DEDUCTED_STAR_2 			4
CONST_INT ci_COP_DECOY_ENTERED_AREA 			5
CONST_INT ci_COP_DECOY_FORCE_5_STAR_ENDED 		6
CONST_INT ci_COP_DECOY_SHOWN_HELP_1 			7
CONST_INT ci_COP_DECOY_SHOWN_HELP_2 			8
CONST_INT ci_COP_DECOY_DEDUCTED_STAR_THIS_FRAME	9
CONST_INT ci_COP_DECOY_NEAR_END_DIA_TRIGGER		10

CONST_INT ci_COP_DECOY_TIME_LEFT_FOR_DIA_TRIGGER 15000
INT iCopDecoyBS
INT iCopDecoyDeductFirstStarTime			  = 0
INT iCopDecoyDeductSecondStarTime			  = 0
INT iCopDecoyCurrentFakeHealth				  = 100
INT iCopDecoyFakeHealthState				  = 0

SCRIPT_TIMER tdRegisterDecoyAsTargetTimer
SCRIPT_TIMER tdCopDecoy5StarForceTimer
SCRIPT_TIMER tdBlockWantedConeResponseStationaryTimer

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Zones
// ##### Description: 
// ##### Variables for the Zone system
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INT iZoneCreatedBS
INT iZoneIsTriggeredBS
INT iLocalZoneRemovedBitset
INT iLocalZoneFailedCreationBitset
SCRIPT_TIMER stZoneCreationDelayTimers[FMMC_MAX_NUM_ZONES]
INT iZonesWaitingForCreationBS
INT iRefreshZonesForThisTeam = -1
INT iZoneBS_TriggeredByLinkedZone
INT iZoneBS_RadiusChangeTriggered
INT iZoneBS_SafeToBlip

VECTOR vCachedZonePosition[FMMC_MAX_NUM_ZONES][ciFMMC_ZONE_MAX_POSITIONS]

INT iZoneStaggeredTeamToCheck

CONST_FLOAT cfGroundedZoneUndergroundOffset		2.0

// Vars for counting up players in Zones
CONST_INT ciParticipantsTriggeringZoneBS_Player1								0
CONST_INT ciParticipantsTriggeringZoneBS_Player2								1
CONST_INT ciParticipantsTriggeringZoneBS_Player3								2
CONST_INT ciParticipantsTriggeringZoneBS_Player4								3
CONST_INT ciParticipantsTriggeringZoneBS_Player5								4
CONST_INT ciParticipantsTriggeringZoneBS_Player6								5
CONST_INT ciParticipantsTriggeringZoneBS_Player7								6
CONST_INT ciParticipantsTriggeringZoneBS_Player8								7
CONST_INT ciParticipantsTriggeringZoneBS_Player9								8
CONST_INT ciParticipantsTriggeringZoneBS_Player10								9
CONST_INT ciParticipantsTriggeringZoneBS_Player11								10
CONST_INT ciParticipantsTriggeringZoneBS_Player12								11
CONST_INT ciParticipantsTriggeringZoneBS_Player13								12
CONST_INT ciParticipantsTriggeringZoneBS_Player14								13
CONST_INT ciParticipantsTriggeringZoneBS_Player15								14
CONST_INT ciParticipantsTriggeringZoneBS_Player16								15
CONST_INT ciParticipantsTriggeringZoneBS_Player17								16
CONST_INT ciParticipantsTriggeringZoneBS_Player18								17
CONST_INT ciParticipantsTriggeringZoneBS_Player19								18
CONST_INT ciParticipantsTriggeringZoneBS_Player20								19
CONST_INT ciParticipantsTriggeringZoneBS_Player21								20
CONST_INT ciParticipantsTriggeringZoneBS_Player22								21
CONST_INT ciParticipantsTriggeringZoneBS_Player23								22
CONST_INT ciParticipantsTriggeringZoneBS_Player24								23
CONST_INT ciParticipantsTriggeringZoneBS_Player25								24
CONST_INT ciParticipantsTriggeringZoneBS_Player26								25
CONST_INT ciParticipantsTriggeringZoneBS_Player27								26
CONST_INT ciParticipantsTriggeringZoneBS_Player28								27
CONST_INT ciParticipantsTriggeringZoneBS_Player29								28
CONST_INT ciParticipantsTriggeringZoneBS_Player30								29

INT iZoneTriggeringPlayerCount[FMMC_MAX_NUM_ZONES]
INT iZoneTriggeringPlayerCountByTeam[FMMC_MAX_NUM_ZONES][FMMC_MAX_TEAMS]
INT iParticipantsTriggeringZoneBS[FMMC_MAX_NUM_ZONES]
INT iCachedZoneIsTriggeredNetworkedBS
INT iCachedZoneIsTriggeredNetworkedBS_Temp
INT iZoneTriggeringPlayerCount_StaggeredIterator = 0

// Stores the Zone index of the Zones which are currently triggering - can store one of each type
INT iCurrentTriggeringZone[ciFMMC_ZONE_TYPE__MAX]
INT iCurrentNumActiveZones[ciFMMC_ZONE_TYPE__MAX]

// Attached Zones // // // // // //
VECTOR vSmoothAttachedZonePos[ciFMMC_ZONE_MAX_SMOOTH_ATTACH_ZONES]
FLOAT fSmoothAttachedZoneDistance[ciFMMC_ZONE_MAX_SMOOTH_ATTACH_ZONES]

// Cached Entities // // // // // //
VEHICLE_INDEX viCachedMaxSpeedZoneVehicle

// Spawn Area Zones // // // // // //
INT iCurrentSpawnAreaZone = -1
INT iSpawnAreaZoneThisFrame = -1

FLOAT fCurrentClosestSpawnAreaZoneDistance = -1.0

CONST_INT ciZoneSpawnArea_CooldownLength	5000
CONST_FLOAT cfZoneSpawnArea_RefreshSpawnAreaDistance	5.0
SCRIPT_TIMER stZoneSpawnArea_CooldownTimer

VECTOR vZoneSpawnArea_FaceCoordsOnRespawn

// Drop-Off Zones // // // // // //
INT iCurrentDropOffZone[FMMC_MAX_TEAMS]
VECTOR vCurrentDropOffZoneCachedCentre[FMMC_MAX_TEAMS]

// Zone Timers // // // // // // // // //
SCRIPT_TIMER stZoneTimers[FMMC_MAX_NUM_ZONES]
INT iZoneTimersCompletedBS
INT iZoneTimersHiddenBS
SCRIPT_TIMER stZoneResetTimer

// Anti-Zones // // // // // // // // //
INT iAntiZone_ConnectedZonesCanTriggerBS

// Race Pit Zone // // // //
INT iRacePitZoneInUseBS
INT iCurrentRacePitZone = -1
SCRIPT_TIMER stRacePitZoneTimer
INT iRacePitZoneCooldown
CONST_INT ciRacePitZoneCooldownLength	4000
INT iRacePitZoneSoundID = -1
CONST_FLOAT cfRacePitZoneAttachedHeight					5.0

// Lives Depletion Zone // // // //
SCRIPT_TIMER stLivesDepletionGraceTimer
SCRIPT_TIMER stLivesDepletionTimer

// pop areas to turn off ambients
INT iPopMultiArea[FMMC_MAX_NUM_ZONES]

DECAL_ID diPetrolDecals[FMMC_MAX_NUM_ZONES]

INT iBitsetVehicleGeneratorsActiveArea
INT iBitsetSetRoadsArea
INT iDispatchBlockZone[FMMC_MAX_NUM_ZONES]
INT iZoneWaterCalmingQuadID[FMMC_MAX_NUM_ZONES]
INT iGPSDisabledZones[FMMC_MAX_NUM_ZONES]

// Radius Change
FLOAT fZoneRadiusOverride[FMMC_MAX_NUM_ZONES]
FLOAT fZoneSizeMultiplier[FMMC_MAX_NUM_ZONES]
SCRIPT_TIMER td_ZoneRadiusOverrideTimer[FMMC_MAX_NUM_ZONES]

// zones blocking gang chase spawn
INT iZoneIndicesOfTypeBlockGangChaseSpawn[ciFMMC_BLOCK_GANG_CHASE_SPAWN_ZONES_MAX]

INT iZoneIndicesOfTypeRuleEntityPrereq[ciFMMC_RULE_ENTITY_PREREQ_ZONE_MAX]
INT iRuleEntityPrereqZoneCount = 0
INT iTempEntitiesInZoneBS = 0

// Air Defence
SCRIPT_TIMER stAirDefenceZone_TimeSpentInside
SCRIPT_TIMER stAirDefenceZone_WarningShotTimer
INT iAirDefenceZone_UsingTimersBS
CONST_INT ciAirDefenceZone_WarningShotTimer_Length		4000

SCRIPT_TIMER tdExplodePlayerZoneTimer[FMMC_MAX_NUM_ZONES]
INT iNavBlockerZone[FMMC_MAX_NUM_ZONES]

// Spook In Radius
SCRIPT_TIMER tdRadiusSpookZoneTimer[FMMC_MAX_NUM_ZONES]

INT iWaveDampingZoneBitset
INT iAlertPedsZoneBitSet

INT iSpawnOcclusionIndex[FMMC_MAX_NUM_ZONES]

SCENARIO_BLOCKING_INDEX bipopScenarioArea[FMMC_MAX_NUM_ZONES]
INT iLocalGearZoneNoneSet

WEAPON_TYPE wtWeaponBlockZonePreviousWeapon = WEAPONTYPE_INVALID

FLOAT fBlockedRunningZoneSpeed
FLOAT fMaxRunningZoneSpeed

CONST_INT	LBOOLZONE_PLAYER_JUST_LEFT_DEFENSE_SPHERE					0 // + iEveryFrameZoneLoop
INT iSpawnProtectionZoneDefenceBS

INT iFMMCAirDefenceZoneArea[FMMC_MAX_NUM_ZONES]
INT iMetalDetectorZoneHasBeenAlertedBS
INT iMetalDetectorZoneDisabledBitset

VECTOR vZoneRandomOffset[FMMC_MAX_NUM_ZONES]
SCRIPT_TIMER tdZoneBlipFlashTimer[FMMC_MAX_NUM_ZONES]
FLOAT fCurrentFadeTime[FMMC_MAX_NUM_ZONES]
INT iZoneBlipFadingBS
INT iZoneBlipHiddenBS

FLOAT fZoneRadiusCached[FMMC_MAX_NUM_ZONES]
INT iZoneNeedsResizingBS

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Bounds
// ##### Description: 
// ##### Variables for the Bounds system
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CONST_INT ciRULE_BOUNDS_BS__DONT_PROCESS_BOUNDS_THIS_FRAME					0
CONST_INT ciRULE_BOUNDS_BS__WAIT_FOR_NEXT_COMPLETED_ZONE_STAGGERED_LOOP		1 // This is set when new Bounds Zones appear/disappear or on a new rule, and is a way of getting the Bounds system to wait until we have done a full Zones staggered loop before processing the Zone data
CONST_INT ciRULE_BOUNDS_BS__WAITING_FOR_ZONE_STAGGERED_LOOP_LOOP_POINT		2 // This is used to ensure that we process all of the Zones that we need to before processing connected Bounds.
CONST_INT ciRULE_BOUNDS_BS__BLOCK_BOUNDS_EVERY_FRAME_PROCESSING				3 // This is used to block the every-frame functions temporarily until the staggered function has given the all-clear
CONST_INT ciRULE_BOUNDS_BS__DISPLAYING_BIG_MESSAGE							4 // This is used to identify which particular set of Bounds is displaying the Big Message/Shard

CONST_FLOAT cfRULE_BOUNDS_MAX_BLIP_DISTANCE		9999999.0

STRUCT RULE_BOUNDS_RUNTIME_VARS_STRUCT
	INT iRuleBoundsBS = 0
	
	INT iRuleBoundsActiveZoneCount = 0 
	INT iRuleBoundsPendingActiveZoneCount = 0 
	
	INT iRuleBoundsCurrentTriggeredZoneCount = 0 // This is the value used outside of the Zones staggered loop, and is set to iRuleBoundsPendingTriggeredZoneCount at the end of the Zones staggered loop.
	INT iRuleBoundsPendingTriggeredZoneCount = 0 // Over the course of the Zones staggered loop, this value is incremented when the player is inside one of these Bounds's Zones.
	
	VECTOR vRuleBoundsCentrePos
	VECTOR vRuleBoundsPendingCentrePos
	INT iRuleBoundsFirstActiveZone = -1
	INT iRuleBoundsFirstActiveZone_Temp = -1
	INT iRuleBoundsLatestBoundsZoneProcessed = -1
	
	INT iRuleBoundsFullZoneStaggeredLoop_LoopPoint = -1
	
	BLIP_INDEX biOutOfBoundsBlips[FMMC_MAX_NUM_ZONES] // Basic point blips used as GPS blips. These are NOT the area blips - that is covered by the Bounds Zone(s).
	INT iOutOfBoundsBlipStaggeredIndex = 0
	FLOAT fOutOfBoundsBlip_CurrentClosestDistance = cfRULE_BOUNDS_MAX_BLIP_DISTANCE
	INT iOutOfBoundsBlip_CurrentClosestBlipIndex = -1
	INT iOutOfBoundsBlip_CurrentClosestBlipIndex_Temp = -1
	INT iOutOfBoundsBlipsCreated = 0
	INT iOutOfBoundsBlipUsingGPS = -1
	
	SCRIPT_TIMER stOutOfBoundsTimer
ENDSTRUCT

RULE_BOUNDS_RUNTIME_VARS_STRUCT sRuleBoundsRuntimeVars[ciMAX_RULE_BOUNDS_PER_RULE]

CONST_INT ciSHARED_RULE_BOUNDS_BS__BLOCK_OBJECTIVE_THIS_FRAME		0
CONST_INT ciSHARED_RULE_BOUNDS_BS__BLOCKED_OBJECTIVE_LAST_FRAME		1
CONST_INT ciSHARED_RULE_BOUNDS_BS__DISPLAYING_BIG_MESSAGE			2
INT iSharedRuleBoundsBS = 0

INT iRuleBoundsCurrentlyOnHoldBS

INT iBoundsTimerSound = -1

TEXT_LABEL_63 tlOutOfBoundsObjectiveText
INT iBoundsToShowObjectiveText = -1

CONST_INT ci_START_WARNING_FOR_TOO_MANY_PLAYERS_INBOUNDS	700
CONST_INT ci_EXPLODE_FOR_TOO_MANY_PLAYERS_INBOUNDS			6000
SCRIPT_TIMER tdTooManyPlayersInBoundsTimer

CONST_INT ciKeepVehicleInBoundsTimer 15000
SCRIPT_TIMER tdKeepVehicleInBounds[FMMC_MAX_VEHICLES]

SCRIPT_TIMER tdOutOfBoundsWarpTimer

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: GPS
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT ci_TOTAL_DISTANCE_CHECK_BARS 		5

INT iGPSTrackingMeterRegisteredPedBitset[FMMC_MAX_PEDS_BITSET]	//CEIL(FMMX_MAX_PEDS / 32)
INT iGPSTrackingMeterPedIndex[ci_TOTAL_DISTANCE_CHECK_BARS]
VECTOR vGPSTrackingMeterStart[ci_TOTAL_DISTANCE_CHECK_BARS]
VECTOR vGPSTrackingMeterEnd[ci_TOTAL_DISTANCE_CHECK_BARS]
INT iGPSTrackingMeterDistCache[ci_TOTAL_DISTANCE_CHECK_BARS]
CONST_INT ciTimerDistanceBarDelay	1000
SCRIPT_TIMER timerDistanceBarDelay


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Hint Cam
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INT	iNearestTarget = -1
INT	iNearestTargetTemp
INT	iNearestTargetType = ci_TARGET_NONE
INT	iNearestTargetTypeTemp
FLOAT fNearestTargetDist2 = MAX_FLOAT
FLOAT fNearestTargetDist2Temp = MAX_FLOAT
CHASE_HINT_CAM_STRUCT sHintCam
CHASE_HINT_CAM_STRUCT sIntroHintCam

INT iLastFramesNearestTarget = -1
INT iLastFramesTargetType = ci_TARGET_NONE

CONST_FLOAT cfHINT_CAM_CUTOFF_RANGE_DEFAULT 		400.0
CONST_FLOAT cfHINT_CAM_CUTOFF_RANGE_AIR_VEHICLE	 	800.0

CONST_FLOAT cfHINT_CAM_MIN_COORD_DISTANCE			30.0

//Forced Hint Cam
INT iForcedHintCamTargetType = -1
INT iForcedHintCamTargetID = -1
CHASE_HINT_CAM_STRUCT sForcedHintCam
CONST_FLOAT cfFORCED_HINT_CAM_CUTOFF_RANGE_DEFAULT	400.0
INT iForcedHintCamTime = ciFMMC_DEFAULT_FORCED_HINT_CAM_LENGTH
SCRIPT_TIMER tdForcedHintCamReset

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Forced Sniper
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INT iForceSniperScopeTargetPed = -1
INT iForceSniperScopeTime = 0
SCRIPT_TIMER tdForceSniperScopeTimer

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Altitude Bar
// ##### Description: 
// ##### This local data is useed for the Altitude Bar set on a rule.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT ciALTITUDE_BAR_LOCAL_UPDATE_NOW					0
CONST_INT ciALTITUDE_BAR_LOCAL_NOT_IN_SPECIFIC_VEHICLE		1
CONST_INT ciALTITUDE_BAR_PLAYED_ARMED_SFX					2
CONST_INT ciALTITUDE_BAR_PREV_RULE_USED_ALTITUDE_SYSTEMS	3

STRUCT ALTITUDE_BAR_LOCAL_DATA
	INT iBS
	VEHICLE_INDEX vehToUse
	FLOAT fAltitude	
	INT iProxyParticipant = - 1// iExplodeDriverPart	
	SCRIPT_TIMER tdAltitudeUpdateTimer
	FLOAT fFailTimer
	
	INT iCountdownSound = -1
ENDSTRUCT
ALTITUDE_BAR_LOCAL_DATA sAltitudeBarLocal

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cargobob
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INT SoundIDCargoBob = -1
INT iStartCargoBobSound = -1
STRING sCargoBobAudioScene

SCRIPT_TIMER cargobobDetachTimer

CONST_INT	ciCARGOBOB_RETRY_TEST_LIMIT				100
INT			iTestCargobobExistsCount = 1

INT iDZCargobobRampSoundID = -1

INT iVehCargoCollisionDoneBS

PTFX_ID ptfx_cargoplane_moving_clouds
PTFX_ID ptfx_cargoplane_wind_and_smoke

INT iObjectiveVehicleAttachedToCargobobBS


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Audio
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
INT iWorldSoundID_1 = -1
INT iWorldSoundID_2 = -1
INT iWorldSoundID_3 = -1
VECTOR vWorldAlarmPos_1 = <<0.0, 0.0, 0.0>>
VECTOR vWorldAlarmPos_2 = <<0.0, 0.0, 0.0>>

INT iTenSecondBeeps = -1

CONST_INT FMMC_MAX_NUM_RADIO_EMITTERS	10
CONST_INT RADIO_EMITTER_TYPE_OBJECT		0
CONST_INT RADIO_EMITTER_TYPE_PROP		1
STRUCT RADIO_EMITTER_STRUCT
	INT iRadioEmitterObjectIndex = -1
	INT iRadioEmitterObjectType = -1
	INT iRadioStation = 0
ENDSTRUCT
INT iStaggeredRadioEmitter
INT iRadioEmitterBS

CONST_INT ciEntityLoopingAudioIDPoolSize	5
STRUCT ENTITY_LOOPING_AUDIO_VARS
	INT iEntityLoopingAudioID = -1
	NETWORK_INDEX niEntityNetworkIndex
ENDSTRUCT
ENTITY_LOOPING_AUDIO_VARS sEntityLoopingAudioVars[ciEntityLoopingAudioIDPoolSize]

ENUM CASINO_PENTHOUSE_PARTY_MUSIC
	CPPM_LOAD,
	CPPM_START_HALLWAY,
	CPPM_PLAYING_HALLWAY,
	CPPM_TRANSITION_PARTY,
	CPPM_PLAYING_PARTY,
	CPPM_MUSIC_ENDED
ENDENUM
CASINO_PENTHOUSE_PARTY_MUSIC ePenthousePartyMusic
INT iPartySoundID = -1
CONST_INT ciPenthouseInterior 274689
CONST_INT ciCasinoHotelFloor 277505
VECTOR vPenthouseDoorCoord = <<2518.1069, -292.3750, -40.1220>>
VECTOR vPenthouseMusicCoord = <<945.715, 8.47, 116.4>>

ENUM BILLIONAIRE_PARTY_MUSIC
	BPM_FREEZE,
	BPM_TRIGGER,
	BPM_PLAY,
	BPM_PLAYING,
	BPM_END_MUSIC,
	BPM_IDLE
ENDENUM
BILLIONAIRE_PARTY_MUSIC eBillionairePartyMusic
VECTOR vBillionaireMansionCoord = <<-1550.5, 119.3, 55.5>>
CONST_FLOAT fBillionaireUnpauseRange 125.0

INT iMissionSpecificScriptedAudio

CONST_INT ciMissionSpecificScriptedAudio_Party_Promoter 	0
CONST_INT ciMissionSpecificScriptedAudio_Billionaire		1
CONST_INT ciMissionSpecificScriptedAudio_DontFuckWithDre 	2
CONST_INT ciMissionSpecificScriptedAudio_SeedCapital		3
CONST_INT ciMissionSpecificScriptedAudio_RogueDrones		4

INT iDrePedForScriptedDialogue = -1

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: React to Smoke
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

ANIM_DATA sReactToSmokeAnimData
INT iReactToSmokeReactVariation
INT iReactToSmokeAnim

INT iRogueDronesPlayerDruggedLoopSoundID = -1

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Players Wake Up
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

ANIM_DATA sPlayersWakeUpAnimData
CAM_VIEW_MODE ePlayersWakeUpCachedCameraView

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cargo Plane Finale
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

VEHICLE_INDEX viCargoPlane

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Camera Shaking
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SCRIPT_TIMER td_CameraShakingOnRuleTimer
SCRIPT_TIMER td_SecondaryCameraShakeIntervalTimer
SCRIPT_TIMER td_SecondaryCameraShakeDurationTimer
CONST_INT ciCameraShakeIntervalVariationMin 2000
CONST_INT ciCameraShakeIntervalVariationMax 5000

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Dialogue
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INT iLocalDialoguePriority = -1
INT iDialogueProgress
INT iDialogueProgressLastPlayed = -1
INT iDialogueProgressCached
INT iLocalDialoguePending[ciMAX_DIALOGUE_BIT_SETS]
INT iLocalDialoguePlayedBS[ciMAX_DIALOGUE_BIT_SETS] //Bitset that stores whether dialogue has been played or not locally (added for 2193702)
INT iLocalDialogueSkippedBS[ciMAX_DIALOGUE_BIT_SETS] 
INT iLocalDialogueBlockedTempBS[ciMAX_DIALOGUE_BIT_SETS] 
INT iLocalDialoguePlayingBS[ciMAX_DIALOGUE_BIT_SETS]
INT iDialogueKilledBS[ciMAX_DIALOGUE_BIT_SETS]
INT iOldDialoguePoints[FMMC_MAX_TEAMS]
INT iDialogueVehicleOwnedBS[ciMAX_DIALOGUE_BIT_SETS]
INT iDialogueVehicleNotOwnedBS[ciMAX_DIALOGUE_BIT_SETS]
INT iDialogueProgressSFX1Played[ciMAX_DIALOGUE_BIT_SETS]
INT iDialogueProgressSFX2Played[ciMAX_DIALOGUE_BIT_SETS]
INT iLocalDialogueBlockedBS[ciMAX_DIALOGUE_BIT_SETS]
INT iLocalDialogueBlockedByEntityBS[ciMAX_DIALOGUE_BIT_SETS]
INT iLocalDialoguePlayRemotelyWithPriority[ciMAX_DIALOGUE_BIT_SETS]
INT iLocalDialogueForceCustomAppend[ciMAX_DIALOGUE_BIT_SETS]
INT iLocalDialogueForceSkip[ciMAX_DIALOGUE_BIT_SETS]
INT iLocalDialogueHangUpPlayedBS[ciMAX_DIALOGUE_BIT_SETS]

#IF IS_DEBUG_BUILD
INT iLocalDialogueForcePlay[ciMAX_DIALOGUE_BIT_SETS]
#ENDIF

CONST_INT ciDIALOGUE_TRIGGER_OBJECTIVE_BLOCKER_FAIL_SAFE_TIME	40000
SCRIPT_TIMER tdDialogueTriggerObjectiveBlockerFailSafe

INT iDialogueBlockedConditionBS
INT iDialogueKillsThisRule[FMMC_MAX_RULES]
INT iDialogueKillsThisRuleSet

INT iDialogueObjectiveObjectDroppedBS
INT iDialogueObjectiveObjectDroppedAtStaggeredIndex = -1

INT iDialogueHasUsedPortalStartBS = 0
INT iDialogueHasUsedPortalEndBS = 0

SCRIPT_TIMER tdDialogueTimer[FMMC_MAX_DIALOGUES]
structPedsForConversation speechPedStruct
INT iCurrentSpeaker
TEXT_LABEL_23 tlt63_DialogueBlock1
TEXT_LABEL_23 tlt63_DialogueBlock2

//To check whether ped has said hi already
INT iDialogueHiBS[ciMAX_DIALOGUE_BIT_SETS]

INT iPlayedDialogueForVehHackBS = 0

SCRIPT_TIMER tdTimeSinceLastDialoguePlayed

CONST_INT ciDialogueInstantLoopCondition_AirStrikeUnavailable			0
CONST_INT ciDialogueInstantLoopCondition_AirStrikeVehicleDestroyed		1
CONST_INT ciDialogueInstantLoopCondition_HeliBackupUnavailable			2
CONST_INT ciDialogueInstantLoopCondition_SupportSniperUnavailable		3
CONST_INT ciDialogueInstantLoopCondition_VehicleHealthThreshold			4
CONST_INT ciDialogueInstantLoopCondition_InvalidPlayerAbilityActivated	5
CONST_INT ciDialogueInstantLoopCondition_SniperAiming					6
CONST_INT ciDialogueInstantLoopCondition_CrashedIntoEnemyVehicle		7
CONST_INT ciDialogueInstantLoopCondition_CrashedIntoAnEntity			8
CONST_INT ciDialogueInstantLoopCondition_MAX							9

CONST_INT ciMAX_DIALOGUE_TRIGGERS_PER_INSTANT_LOOP_CONDITION			12

INT iDialogueTriggersWithInstantLoopCondition[ciDialogueInstantLoopCondition_MAX][ciMAX_DIALOGUE_TRIGGERS_PER_INSTANT_LOOP_CONDITION]
INT ciDialogueInstantLoopConditionBS //Reset this after dialogue is process each frame

CONST_INT ciDT_TUNER_VEHICLE_MUSCLE		0
CONST_INT ciDT_TUNER_VEHICLE_EURO		1
CONST_INT ciDT_TUNER_VEHICLE_JDM		2

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: World
// ##### Description: Variables for world related functionality
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SCENARIO_BLOCKING_INDEX sbiCasinoValet
VECTOR vValetPos = <<911.56, 50.45, 78.78>>

eARTIFICIAL_LIGHTS_STATE eLocalArtificialLightsState = eARTIFICIAL_LIGHTS_STATE__DEFAULT // When the local state 
INT iArtificialLightsOffTCIndex = -1
INTERIOR_INSTANCE_INDEX intEMPSpecifiedInterior = NULL
SCRIPT_TIMER stEMPTimeActive
INT iMaxEMPDuration = -1

CONST_INT ciLIGHTS_AMBIENT_ZONE_STATE__AZ_DLC_HEISTS_BIOLAB					0
CONST_INT ciLIGHTS_AMBIENT_ZONE_STATE__iz_dlc_xm_int_silo_02_Control		1
INT iLightsAmbientZoneBS[1]

INT iPlayerTriggeredEMPPedReactedBS[ciFMMC_PED_BITSET_SIZE]

INT iPedEMPRandomAnimationIndex[FMMC_MAX_PEDS]
CONST_INT ciPlayerTriggeredEMP_PedAnims_Flashlight		1
CONST_INT ciPlayerTriggeredEMP_PedAnims_NoFlashlight	2
CONST_INT ciPlayerTriggeredEMP_PedAnims_Unassigned		0

CONST_INT ciTIME_OF_DAY_EARLY_EVENING 	19
CONST_INT ciTIME_OF_DAY_PRE_DAWN		5

CONST_INT ciIslandAirDefenceTurrets					3
INT iIslandAirDefenceTurretStaggeredIndex
INT iIslandAirDefenceTurretAudioID[ciIslandAirDefenceTurrets]
INT iIslandAirDefenceTurretPlayingDisabledSoundBS
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Loch Santos Monster
// ##### Description: Variables for the Nessie easter egg
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CONST_INT ciLSM_State_Init			0
CONST_INT ciLSM_State_Idle			1
CONST_INT ciLSM_State_Splash		2
CONST_INT ciLSM_State_Descending 	3
CONST_INT ciLSM_State_Cleanup	 	4
INT iLSM_State
SCRIPT_TIMER tdLSM_Timer
CONST_FLOAT cfLSM_DescendAmount 10.0
CONST_INT ciLSM_DescendTime 3500
CONST_FLOAT ciLSM_DescendRotationRate 8.0


				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicles
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INT iCachedMOCIndex = -1

INT iPersonalVehicleSlotCache = -1

INT iDamageVehicleObjectiveRequiresCheckBS[FMMC_MAX_TEAMS]
INT iCurrentVehHealthPercentageCachedBS // so we don't have to manually loop and clear the Cache at the start of a frame
FLOAT fCurrentHealthPercentageCache[FMMC_MAX_VEHICLES]

INT iVehicleHasBeenPlacedIntoInteriorBS

CONST_INT ciVEHICLE_IN_VEHICLE_STATE__INACTIVE												-1
CONST_INT ciVEHICLE_IN_VEHICLE_STATE__PREPARING												0
CONST_INT ciVEHICLE_IN_VEHICLE_STATE__WAITING_FOR_PEDS										1
CONST_INT ciVEHICLE_IN_VEHICLE_STATE__ATTACHING												2
CONST_INT ciVEHICLE_IN_VEHICLE_STATE__WAITING_TO_EMERGE										3
CONST_INT ciVEHICLE_IN_VEHICLE_STATE__GET_CONTAINER_VEHICLE_READY_FOR_VEHICLE_DETACH		4
CONST_INT ciVEHICLE_IN_VEHICLE_STATE__DETACH_AND_OPEN_CONTAINER_VEHICLE_WHEN_READY			5
CONST_INT ciVEHICLE_IN_VEHICLE_STATE__EMERGE_WHEN_READY										6
CONST_INT ciVEHICLE_IN_VEHICLE_STATE__EMERGING												7
CONST_INT ciVEHICLE_IN_VEHICLE_STATE__WAITING_TO_CLOSE_DOOR									8
CONST_INT ciVEHICLE_IN_VEHICLE_STATE__EMERGE_ENDING											9
CONST_INT ciVEHICLE_IN_VEHICLE_STATE__EMERGE_COMPLETE										10

INT iVehicleInVehicleState = ciVEHICLE_IN_VEHICLE_STATE__INACTIVE
SCRIPT_TIMER tdVehicleExitingVehicleStateTimer
CONST_INT ciVehicleExitingVehicleStateTimer_UnlockDoorDelayLength		2000
CONST_INT ciVehicleExitingVehicleStateTimer_EmergeDelayLength			1000
CONST_INT ciVehicleExitingVehicleStateTimer_IgnoreAirCheckLength		3000
CONST_INT ciVehicleExitingVehicleStateTimer_IgnoreDoorOpenCheckLength	4000
CONST_INT ciVehicleExitingVehicleStateTimer_IgnoreAllWheelsCheckLength	7000
CONST_INT ciVehicleExitingVehicleStateTimer_CloseDoorLength				2000
CONST_FLOAT cfVehicleExitingVehicleDoorCloseSpeed 				1.5

SCRIPT_TIMER tdVehicleExitingVehicleClearVelocityTimer
CONST_INT ciVehicleExitingVehicleClearVelocityTimer_Length			1000

SCRIPT_TIMER tdVehicleExitingVehicleReleaseControlFailsafeTimer
CONST_INT ciVehicleExitingVehicleReleaseControlFailsafeTimer_Length			5000

INT iVehicleInVehicleEmergePeds = 0

INT iRemoteVehicleExplosionInitiator = -1

INT iVehBS_HealthThresholdMet
INT iVehBS_HealthThresholdEnded

CONST_INT ciVEHICLE_DRIVEBY_SEQUENCE_STATE__INACTIVE						0
CONST_INT ciVEHICLE_DRIVEBY_SEQUENCE_STATE__START_ENTERING_BACK_SEATS		1
CONST_INT ciVEHICLE_DRIVEBY_SEQUENCE_STATE__ENTERING_BACK_SEATS				2
CONST_INT ciVEHICLE_DRIVEBY_SEQUENCE_STATE__ENTER_DRIVEBY_SEATS				3
CONST_INT ciVEHICLE_DRIVEBY_SEQUENCE_STATE__MAINTAIN_DRIVEBY				4
INT iVehicleDrivebySequenceState = ciVEHICLE_DRIVEBY_SEQUENCE_STATE__INACTIVE
INT iVehicleDrivebySeatHopTimestamp
CONST_INT ciVehicleDrivebySeatHopDelayLength		2000

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Trains
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INT iTrainEnabledHighPrecisionBlendingBS
INT iTrainInMotionBS

SCRIPT_TIMER tdTrainFullyStoppedTimer[FMMC_MAX_TRAINS]
CONST_INT ciTrainFullyStoppedTimerDelayLength	3000

CONST_INT ciFMMC_TRAIN_STOP_TIME 5000

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Mission Entity Local Vars
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// # Objects
// # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

STRUCT structMissionObjectsLocalVars
	BLIP_RUNTIME_VARS_STRUCT sBlipRuntimeVars
ENDSTRUCT
structMissionObjectsLocalVars sMissionObjectsLocalVars[FMMC_MAX_NUM_OBJECTS]

// # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// # Interactables
// # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

STRUCT structMissionInteractableLocalVars
	BLIP_RUNTIME_VARS_STRUCT sBlipRuntimeVars
ENDSTRUCT
structMissionInteractableLocalVars sMissionInteractablesLocalVars[FMMC_MAX_NUM_INTERACTABLES]

// # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// # Peds
// # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

ENUM PED_SYNC_SCENE_STATE
	PED_SYNC_SCENE_STATE_INIT = 0,
	PED_SYNC_SCENE_STATE_PROCESS_START,
	PED_SYNC_SCENE_STATE_PROCESS_PHASE_2,	
	PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT,
	PED_SYNC_SCENE_STATE_PROCESS_PHASE_3,
	PED_SYNC_SCENE_STATE_PROCESS_PHASE_4,
	PED_SYNC_SCENE_STATE_PROCESS_PHASE_5,
	PED_SYNC_SCENE_STATE_PROCESS_PHASE_6,
	PED_SYNC_SCENE_STATE_PROCESS_PHASE_7,
	PED_SYNC_SCENE_STATE_PROCESS_PHASE_8,
	PED_SYNC_SCENE_STATE_PROCESS_PHASE_9,
	PED_SYNC_SCENE_STATE_PROCESS_PHASE_10,
	PED_SYNC_SCENE_STATE_PROCESS_PHASE_11,
	PED_SYNC_SCENE_STATE_PROCESS_PHASE_12,
	PED_SYNC_SCENE_STATE_PROCESS_PHASE_13,
	PED_SYNC_SCENE_STATE_CLEANUP,
	PED_SYNC_SCENE_STATE_PROCESS_END
ENDENUM

CONST_INT FMMC_MAX_PED_SCENE_SYNCED_PEDS 6

CONST_INT ci_MissionPedBS_iSyncSceneAnimationFinished						0
CONST_INT ci_MissionPedBS_ChangedRelationshipGroup							1
CONST_INT ci_MissionPedBS_SyncScene_PlayedSound_1							2
CONST_INT ci_MissionPedBS_iSyncScenePlayedBreakoutDialogue					3
CONST_INT ci_MissionPedBS_iSyncScenePlayedIntroDialogue						5
CONST_INT ci_MissionPedBS_iSyncSceneRequestRuleProgress						8
CONST_INT ci_MissionPedBS_PerformingGrappelFromHeli							9
CONST_INT ci_MissionPedBS_iSyncSceneAnimationReady 							10
CONST_INT ci_MissionPedBS_iSyncSceneAnimationNeedResync						11
CONST_INT ci_MissionPedBS_RollingStart										12
CONST_INT ci_MissionPedBS_ForcedBlip										13
CONST_INT ci_MissionPedBS_HasBeenShotInTheHead								14
CONST_INT ci_MissionPedBS_HasBeenHit										15
CONST_INT ci_MissionPedBS_HasBeenShotAnim									16
CONST_INT ci_MissionPedBS_SyncSceneForceBreakout							17
CONST_INT ci_MissionPedBS_ShootAnimGun										18
CONST_INT ci_MissionPedBS_HasFirstBreakoutForcedDialogue					19
CONST_INT ci_MissionPedBS_PerformedSpecialCaseTransitionIntoSpecialAnim		20
CONST_INT ci_MissionPedBS_ProcessingAmbientAnimation						21
CONST_INT ci_MissionPedBS_SyncScene_LockedMigration							22
CONST_INT ci_MissionPedBS_StartedInContainerVehicleWithInvincibility		23
CONST_INT ci_MissionPedBS_SyncScene_TriggeredMusicEvent						24
CONST_INT ci_MissionPedBS_SyncScene_ProgressedAnimStageThisRule				25
CONST_INT ci_MissionPedBS_SyncScene_DoProgressedAnimStageThisRuleEvent		26
CONST_INT ci_MissionPedBS_SyncScene_CleanupSyncSceneEarly					27

CONST_INT ciMissionPedSyncSceneObjects_DJ_Bag		0
CONST_INT ciMissionPedAnimObject					0
CONST_INT ciMissionPedSyncSceneObjects_MAX			1

STRUCT structMissionPedsLocalVars

	INT iSyncPedPartOwner = -1
	INT iSyncScene = -1
	PED_SYNC_SCENE_STATE ePedSyncSceneState = PED_SYNC_SCENE_STATE_INIT
	INT iSyncedPeds[FMMC_MAX_PED_SCENE_SYNCED_PEDS]
	INT iPedBS	
	
	INT iTimeStampPedResyncFailure
	
	INT iTimeStampSyncSceneTimeout_1
	INT iTimeStampSyncSceneTimeout_2
	INT iTimeStampSyncSceneTimeout_3
	
	INT iTimeStampSASDisguiseGracePeriodPartial
	INT iTimeStampSASDisguiseGracePeriodFull
	
	INT iHealthCached
	ENTITY_INDEX eiLastHitBy
	
	OBJECT_INDEX oiAnimObjs[ciMissionPedSyncSceneObjects_MAX]
	NETWORK_INDEX niAnimObjs[ciMissionPedSyncSceneObjects_MAX]
	
	INT iRandomAnimLastPlayed
	
	BLIP_RUNTIME_VARS_STRUCT sBlipRuntimeVars
	
ENDSTRUCT
structMissionPedsLocalVars sMissionPedsLocalVars[FMMC_MAX_PEDS]

// # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// # Vehicles
// # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

STRUCT structMissionVehiclesLocalVars
	BLIP_RUNTIME_VARS_STRUCT sBlipRuntimeVars
ENDSTRUCT
structMissionVehiclesLocalVars sMissionVehiclesLocalVars[FMMC_MAX_VEHICLES]

// # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// # Props
// # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//STRUCT structMissionPropsLocalVars
//	BLIP_RUNTIME_VARS_STRUCT sBlipRuntimeVars
//ENDSTRUCT
//structMissionPropsLocalVars sMissionPropsLocalVars[FMMC_MAX_NUM_PROPS]

// # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// # Dyno Props
// # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

STRUCT structMissionDynoPropsLocalVars
	BLIP_RUNTIME_VARS_STRUCT sBlipRuntimeVars
ENDSTRUCT
structMissionDynoPropsLocalVars sMissionDynoPropsLocalVars[FMMC_MAX_NUM_DYNOPROPS]

// # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// # Pickups
// # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

STRUCT structMissionPickupsLocalVars
	BLIP_RUNTIME_VARS_STRUCT sBlipRuntimeVars
ENDSTRUCT
structMissionPickupsLocalVars sMissionPickupsLocalVars[FMMC_MAX_WEAPONS]

// # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// # Locations
// # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

STRUCT structMissionLocationsLocalVars
	BLIP_RUNTIME_VARS_STRUCT sBlipRuntimeVars
ENDSTRUCT
structMissionLocationsLocalVars sMissionLocationsLocalVars[FMMC_MAX_GO_TO_LOCATIONS]

// # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// # Dummy Blips
// # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

STRUCT structMissionDummyBlipsLocalVars
	BLIP_RUNTIME_VARS_STRUCT sBlipRuntimeVars
ENDSTRUCT
structMissionDummyBlipsLocalVars sMissionDummyBlipsLocalVars[FMMC_MAX_DUMMY_BLIPS]

// # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// # Trains
// # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

STRUCT structMissionTrainsLocalVars
	BLIP_RUNTIME_VARS_STRUCT sBlipRuntimeVars
ENDSTRUCT
structMissionTrainsLocalVars sMissionTrainsLocalVars[FMMC_MAX_TRAINS]

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entity Checkpoint Continuity
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
INT iEntityCheckpointContinuityProcessedCheckpointBS // Keeps track of which checkpoints the entity checkpoint continuity system has already processed to ensure we don't repeatedly register positions/headings etc

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Player Model Swapping
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
MODEL_NAMES mnPlayerModelToSwapTo
MODEL_NAMES mnPlayerModelSwapFrom

CONST_INT ciModelSwap_WAIT				0
CONST_INT ciModelSwap_LOAD				1
CONST_INT ciModelSwap_PTFX				2
CONST_INT ciModelSwap_NEW_MODEL			3
CONST_INT ciModelSwap_FREEMODE_MODEL	4
CONST_INT ciModelSwap_FREEMODE_RESTORE	5
CONST_INT ciModelSwap_CLEANUP			6
INT iModelSwapState = ciModelSwap_WAIT


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Player Jobs & Mission Stages
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// Jobs the player can complete
CONST_INT JOB_COMP_DELIVER_PED                  0
CONST_INT JOB_COMP_DELIVER_VEH 					1
CONST_INT JOB_COMP_DELIVER_OBJ 					2
CONST_INT JOB_COMP_ARRIVE_LOC 					3
CONST_INT JOB_COMP_ARRIVE_VEH					4
CONST_INT JOB_COMP_ARRIVE_OBJ					5
CONST_INT JOB_COMP_ARRIVE_PED 					6
CONST_INT JOB_COMP_PHOTO_LOC 					15
CONST_INT JOB_COMP_PHOTO_VEH					16
CONST_INT JOB_COMP_PHOTO_OBJ					17
CONST_INT JOB_COMP_PHOTO_PED 					18
CONST_INT JOB_COMP_HACK_OBJ                     19

// Local mission stages
CONST_INT	CLIENT_MISSION_STAGE_NONE					-1 //In between objectives
CONST_INT	CLIENT_MISSION_STAGE_SETUP					0
CONST_INT	CLIENT_MISSION_STAGE_KILL_PED				1
CONST_INT	CLIENT_MISSION_STAGE_PROTECT_PED			2
CONST_INT	CLIENT_MISSION_STAGE_COLLECT_PED			3
CONST_INT	CLIENT_MISSION_STAGE_DELIVER_PED			4
CONST_INT	CLIENT_MISSION_STAGE_KILL_VEH				5
CONST_INT	CLIENT_MISSION_STAGE_PROTECT_VEH			6
CONST_INT	CLIENT_MISSION_STAGE_COLLECT_VEH			7
CONST_INT	CLIENT_MISSION_STAGE_DELIVER_VEH			8 
CONST_INT	CLIENT_MISSION_STAGE_KILL_OBJ				9
CONST_INT	CLIENT_MISSION_STAGE_PROTECT_OBJ			10
CONST_INT	CLIENT_MISSION_STAGE_COLLECT_OBJ			11
CONST_INT	CLIENT_MISSION_STAGE_DELIVER_OBJ			12
CONST_INT   CLIENT_MISSION_STAGE_GOTO_LOC				13
CONST_INT   CLIENT_MISSION_STAGE_CAPTURE_AREA  			14
CONST_INT   CLIENT_MISSION_STAGE_CAPTURE_PED   			15
CONST_INT   CLIENT_MISSION_STAGE_CAPTURE_VEH   			16
CONST_INT   CLIENT_MISSION_STAGE_CAPTURE_OBJ   			17
CONST_INT   CLIENT_MISSION_STAGE_GOTO_PED				18
CONST_INT   CLIENT_MISSION_STAGE_GOTO_VEH				19
CONST_INT   CLIENT_MISSION_STAGE_GOTO_OBJ				20
CONST_INT   CLIENT_MISSION_STAGE_KILL_PLAYERS  			21
CONST_INT   CLIENT_MISSION_STAGE_KILL_TEAM0   	 		22
CONST_INT   CLIENT_MISSION_STAGE_KILL_TEAM1				23
CONST_INT   CLIENT_MISSION_STAGE_KILL_TEAM2				24
CONST_INT   CLIENT_MISSION_STAGE_KILL_TEAM3				25
CONST_INT   CLIENT_MISSION_STAGE_PHOTO_LOC				26
CONST_INT   CLIENT_MISSION_STAGE_PHOTO_PED   			27
CONST_INT   CLIENT_MISSION_STAGE_PHOTO_VEH    			28
CONST_INT   CLIENT_MISSION_STAGE_PHOTO_OBJ    			29
CONST_INT   CLIENT_MISSION_STAGE_HACK_COMP    	 		30
CONST_INT   CLIENT_MISSION_STAGE_GLASS_CUTTING 	 		31
CONST_INT   CLIENT_MISSION_STAGE_DAMAGE_PED 			32
CONST_INT 	CLIENT_MISSION_STAGE_DAMAGE_VEH				33
CONST_INT	CLIENT_MISSION_STAGE_INTERACT_WITH			34
CONST_INT 	CLIENT_MISSION_STAGE_DAMAGE_OBJ				35
CONST_INT 	CLIENT_MISSION_STAGE_LOOT_THRESHOLD			36
CONST_INT   CLIENT_MISSION_STAGE_GET_MASK       		37
CONST_INT   CLIENT_MISSION_STAGE_SCRIPTED_CUTSCENE 		38
CONST_INT   CLIENT_MISSION_STAGE_LEAVE_LOC     			45
CONST_INT	CLIENT_MISSION_STAGE_PHONE_BULLSHARK 		46
CONST_INT	CLIENT_MISSION_STAGE_PHONE_AIRSTRIKE 		47
CONST_INT	CLIENT_MISSION_STAGE_HACK_DRILL      		48
CONST_INT	CLIENT_MISSION_STAGE_GOTO_ANY_PLAYER		49
CONST_INT	CLIENT_MISSION_STAGE_GOTO_TEAM0				50
CONST_INT	CLIENT_MISSION_STAGE_GOTO_TEAM1				51
CONST_INT	CLIENT_MISSION_STAGE_GOTO_TEAM2				52
CONST_INT	CLIENT_MISSION_STAGE_GOTO_TEAM3				53
CONST_INT 	CLIENT_MISSION_STAGE_ENTER_SAFE_COMBINATION 54
CONST_INT  	CLIENT_MISSION_STAGE_UNDERWATER_WELDING		55
CONST_INT  	CLIENT_MISSION_STAGE_INTERROGATION			56
CONST_INT 	CLIENT_MISSION_STAGE_HACKING				57
CONST_INT	CLIENT_MISSION_STAGE_RESULTS				58
CONST_INT	CLIENT_MISSION_STAGE_TERMINATE_DELAY 		59
CONST_INT  	CLIENT_MISSION_STAGE_HACK_FINGERPRINT_KEYPAD 60
CONST_INT	CLIENT_MISSION_STAGE_LEAVE_PED				61
CONST_INT	CLIENT_MISSION_STAGE_LEAVE_VEH				62
CONST_INT	CLIENT_MISSION_STAGE_LEAVE_OBJ				63
CONST_INT   CLIENT_MISSION_STAGE_POINTS_THRESHOLD		64
CONST_INT	CLIENT_MISSION_STAGE_PED_GOTO_LOCATION		65
CONST_INT	CLIENT_MISSION_STAGE_HOLDING_RULE			66
CONST_INT	CLIENT_MISSION_STAGE_BEAMHACK_VEH			67
CONST_INT	CLIENT_MISSION_STAGE_INSERT_FUSE			68
CONST_INT	CLIENT_MISSION_STAGE_END					69


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Game States
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT GAME_STATE_INI 						0
CONST_INT GAME_STATE_INTRO_CUTSCENE             1
CONST_INT GAME_STATE_FORCED_TRIP_SKIP			2
CONST_INT GAME_STATE_SOLO_PLAY		            3
CONST_INT GAME_STATE_TEAM_PLAY		            4
CONST_INT GAME_STATE_CAMERA_BLEND	            5
CONST_INT GAME_STATE_RUNNING					6
CONST_INT GAME_STATE_MISSION_OVER				7
CONST_INT GAME_STATE_LEAVE						8
CONST_INT GAME_STATE_END						9

ENUM eIntroState
	eIntroState_VALIDATE_LOBBY_LEADER,
	eIntroState_PLAYER_PED_MODEL_SWAP,
	eIntroState_LOAD_ASSETS_AND_OUTFITS,
	eIntroState_WAIT_FOR_CORONA,
	eIntroState_LOAD_ASSETS_AND_OUTFITS_POST_ALT_VARS,
	eIntroState_PROCESS_SPECTATOR,	
	eIntroState_ASSIGN_TEAM,
	eIntroState_SELECT_START_POSITION,
	eIntroState_CREATE_ZONES,
	eIntroState_WARP_TO_START_POSITION,
	eIntroState_CREATE_LOCAL_ENTITIES,
	eIntroState_FINAL_INITIALISATION
ENDENUM
eIntroState eIntroProgress = eIntroState_VALIDATE_LOBBY_LEADER

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Mission Controller Profiler - DEBUG ONLY
// ##### Description: Vars and consts for the Mission Controller profiling debug view
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#IF IS_DEBUG_BUILD

TWEAK_FLOAT fProfilerPosX 1.0 		//Right Side
TWEAK_FLOAT fProfilerPosY 0.075		//Top Side

TWEAK_FLOAT fProfilerScaleX 0.2
TWEAK_FLOAT fProfilerScaleY 0.115

TWEAK_FLOAT fProfilerTextPaddingX 0.005
TWEAK_FLOAT fProfilerTextPaddingY 0.007
TWEAK_FLOAT fProfilerTextTitleSpacingY 0.025
TWEAK_FLOAT fProfilerTextSpacingY 0.0175
TWEAK_FLOAT fProfilerTextTitleScale 0.275
TWEAK_FLOAT fProfilerTextScale 0.22
TWEAK_FLOAT fProfilerBarHeight 0.004
TWEAK_FLOAT fProfilerBarSpacingY 0.006
TWEAK_FLOAT fProfilerBarIndicatorHeight 0.008
TWEAK_FLOAT fProfilerBarIndicatorWidth 0.002

CONST_INT ciFMMC_PROFILER_DEFAULT_ROWS 2

TWEAK_INT iProfilerBGAlpha 150

TWEAK_INT ciFMMC_PROFILER_RESET_INTERVAL 250

INT iProfilerRowsDrawn = 0

CONST_INT ciFMMC_PROFILER_OVERALL 			0
CONST_INT ciFMMC_PROFILER_PLAYERS 			1
CONST_INT ciFMMC_PROFILER_ENTITIES 			2
CONST_INT ciFMMC_PROFILER_PEDS				3
CONST_INT ciFMMC_PROFILER_VEHICLES			4	
CONST_INT ciFMMC_PROFILER_OBJECTS			5
CONST_INT ciFMMC_PROFILER_DYNOPROPS			6
CONST_INT ciFMMC_PROFILER_INTERACTABLES		7
CONST_INT ciFMMC_PROFILER_TRAINS			8
CONST_INT ciFMMC_PROFILER_LOCATIONS			9
CONST_INT ciFMMC_PROFILER_LOCAL_PLAYER		10
CONST_INT ciFMMC_PROFILER_GANGCHASE			11
CONST_INT ciFMMC_PROFILER_DIALOGUE			12
CONST_INT ciFMMC_PROFILER_ZONES				13
CONST_INT ciFMMC_PROFILER_BOUNDS			14
CONST_INT ciFMMC_PROFILER_PROPS				15
CONST_INT ciFMMC_PROFILER_DOORS				16
CONST_INT ciFMMC_PROFILER_DUMMYBLIPS		17
CONST_INT ciFMMC_PROFILER_PICKUPS			18
CONST_INT ciFMMC_PROFILER_CUSTOM			19 //Leave just before MAX
CONST_INT ciFMMC_PROFILER_MAX				20

STRUCT MISSION_CONTROLLER_PROFILER_DATA
	INT iProfilerUpdateCount
	FLOAT fProfilerUpdateSum
	FLOAT fProfilerUpdate
	FLOAT fProfilerUpdatePeak
	INT iPeakFrame
	BOOL bStarted = FALSE
	FLOAT fProfilerCurrentFrameTime
ENDSTRUCT
MISSION_CONTROLLER_PROFILER_DATA sProfilerData[ciFMMC_PROFILER_MAX]

BOOL bShowProfiler
BOOL bMissionControllerProfilerPeakPrints

INT iTempProfilerActivePeds
INT iProfilerActivePeds

INT iTempProfilerActiveVehicles
INT iProfilerActiveVehicles

INT iTempProfilerActiveTrains
INT iProfilerActiveTrains

INT iTempProfilerActiveObjects
INT iProfilerActiveObjects

INT iTempProfilerActiveDynoProps
INT iProfilerActiveDynoProps

INT iTempProfilerActiveInteractables
INT iProfilerActiveInteractables

#ENDIF

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Proximity Spawning Debug View - DEBUG ONLY
// ##### Description: Vars and consts for the Proximity Spawning Debug View 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#IF IS_DEBUG_BUILD
	
CONST_INT ciPROX_SPAWNDEBUG_ENTITY_TYPE_NONE		0
CONST_INT ciPROX_SPAWNDEBUG_ENTITY_TYPE_PEDS		1
CONST_INT ciPROX_SPAWNDEBUG_ENTITY_TYPE_DYNOPROPS	2
CONST_INT ciPROX_SPAWNDEBUG_ENTITY_TYPE_VEHICLES	3

INT iProxSpawnDebug_EntityTypePrevious = ciPROX_SPAWNDEBUG_ENTITY_TYPE_NONE
INT iProxSpawnDebug_EntityType = ciPROX_SPAWNDEBUG_ENTITY_TYPE_NONE
INT iProxSpawnDebug_EntityIndex = 0

TEXT_WIDGET_ID twidProxSpawnDebug_Settings
TEXT_WIDGET_ID twidProxSpawnDebug_EntityStatus
TEXT_WIDGET_ID twidProxSpawnDebug_ProxSpawnStatus
FLOAT fProxSpawnDebug_LocalDistance = 0.0

BOOL bProxSpawnDebug_ForcePlayersReqAny = FALSE

CONST_INT ciProxSpawnDebug_MAX_PLAYERS 4
BOOL bProxSpawnDebug_EnablePlayerPositionSpoofing
VECTOR vProxSpawnDebug_PlayerOverride[ciProxSpawnDebug_MAX_PLAYERS]
BOOL bProxSpawnDebug_PlayerOverrideUpdate[ciProxSpawnDebug_MAX_PLAYERS]

STRUCT PROXSPAWN_DEBUG_SPOOFING_STRUCT
	INT iProximitySpawning_PedSpawnRangeBS[FMMC_MAX_PEDS_BITSET]
	INT iProximitySpawning_PedCleanupRangeBS[FMMC_MAX_PEDS_BITSET]
	
	INT iProximitySpawning_DynoPropSpawnRangeBS
	INT iProximitySpawning_DynoPropCleanupRangeBS
	
	INT iProximitySpawning_VehSpawnRangeBS
	INT iProximitySpawning_VehCleanupRangeBS
ENDSTRUCT 
PROXSPAWN_DEBUG_SPOOFING_STRUCT sProxSpawnDebug_PlayerOverrideData[ciProxSpawnDebug_MAX_PLAYERS]

#ENDIF

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Debug Only variables
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD

SCRIPT_TIMER tdExtraScriptDebugOnBugEntryTimer
CONST_INT ciEXTRA_SCRIPT_DEBUG_ON_BUG_ENTRY_TIME 10000

//Test mode only vehicle weapons
VEHICLE_WEAPONS_DATA sVehicleWeaponsData

INT iLastPerformanceCheck
CONST_INT ciPERFORMANCE_METRIC_INTERVAL 10000
CONST_FLOAT cfPERFORMANCE_METRIC_SCRIPT_BUDGET 3.0

ENUM eColour
	eColour_Red,
	eColour_Orange,
	eColour_White,
	eColour_Green,
		
	eColour_MAX
ENDENUM

STRUCT SCRIPT_EVENT_DATA_DEBUG_OBJECTIVE_SKIP_FOR_TEAM
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iTeam
	INT iSkipObjective
	INT iNewTeam
ENDSTRUCT

CONST_INT ciMAX_RECORDED_GAME_STATES 	3
CONST_INT ciMAX_RECORDED_CLIENT_STATES 	4

INT iGameStates[ ciMAX_RECORDED_GAME_STATES ]
INT iClientStates[ ciMAX_RECORDED_CLIENT_STATES ]
INT iPreviousRule = -2

ENUM eControllerDebugState
	eControllerDebugState_OFF,
	eControllerDebugState_OVERVIEW,
	eControllerDebugState_CUTSCENES,
	eControllerDebugState_CAPTURELOC,
	eControllerDebugState_ENTITYSTUFF,
	eControllerDebugState_PLAYER,
	eControllerDebugState_VEHICLES,
	eControllerDebugState_END
ENDENUM

BOOL bDebugBlipPrints
BOOL bDebugBoundsPrints

BOOL bSingleItemCashGrab
BOOL bDisableIWCam

//Commandlines
BOOL bInVehicleDebug 
BOOL bPlacedMissionVehiclesDebugHealth
BOOL bobjectiveVehicleOnCargobobDebug
BOOL bAllowAllMissionsWithTwoPlayers
BOOL bTeamColourDebug
BOOL bFMMCOverrideMultiRuleTimer
BOOL bJS3226522
BOOL bAllowGangOpsWithoutBoss
BOOL bHandcuff
BOOL bMCScriptAIAdditionalPrints
BOOL bMCScriptAISpammyPrints
BOOL bExpProjDebug
BOOL bObjectGotoDebug
BOOL bMultiPersonVehiclePrints
BOOL bSkipSVMIntro
BOOL bAllowTripSkipNoPlay
BOOL bIdleKickPrints
BOOL bDebugPrintCustomText
BOOL bOverrideCCTVRotDebug
BOOL bsc3378817
BOOL bHeistHashedMac
BOOL bAutoForceRestartNoCorona
BOOL bPhoneEMPDebug
BOOL bMissionYachtDebug
BOOL bHealthDrainZoneDebug
BOOL bObjectTaserDebug
BOOL bZoneTimerDebug
BOOL bInteractWithDebug
BOOL bEntityInteriorDebug
BOOL bCCTVDebug
BOOL bDoorDebug
BOOL bDirectionalDoorDebug
BOOL bDynoPropSpam
BOOL bObjDebug
BOOL bHumaneSyncLockDebug
BOOL bZoneDebug
BOOL bDontFrameStaggeredDebug
BOOL bPrintExtraObjectiveCompletion
BOOL bBoundsDebug
BOOL bInteractableDebug
BOOL bWorldPropDebug
BOOL bUnderwaterTunnelWeldDebug
BOOL bMissionControllerProfilerEnabled
BOOL bBlockBranchingOnAggro
BOOL bBlockBranchingOnAggroKeepStealthDialogue
BOOL bAllFMMCPrereqsSet
BOOL bDialogueTriggerDebug
BOOL bPantherDebug
BOOL bEnterSafeCombinationDebug
BOOL bGlassCuttingMinigameDebug
BOOL bInterrogationMinigameDebug
BOOL bObjectLastSafeDropPointDebug
BOOL bScriptedFlammableEntityDebug
BOOL bInteriorDestructionDebug
BOOL bEntityCheckpointContinuityDebug
BOOL bBlockAllDialogue
BOOL bCompanionDebug
BOOL bScriptedAnimConversationDebug
BOOL bDrivebyDebug
BOOL bDebugForceCutsceneMinimumShotsProcessing
BOOL bDebugAlwaysAllowMocapSkipping

//XML Overrides
BOOL bXMLOverrideActive
STRING stXMLOverridePath

ENUM FMMC_XML_DUMP_FLAGS
	XMLDUMP_ALL 				= 0,
	XMLDUMP_CONTINUITY			= 1,
	XMLDUMP_ALTVARS				= 2,
	XMLDUMP_ISLANDHEIST			= 3
ENDENUM

BOOL bXMLOverride_Continuity
BOOL bXMLOverride_AltVars
BOOL bXMLOverride_IslandHeist

// Make your own as and when necessary. Nothing stopping two systems using the same value if they are lightweight.
CONST_INT ciFRAME_STAGGERED_DEBUG_PLAYER_SPAWNING	10
CONST_INT ciFRAME_STAGGERED_DEBUG_PED				30
CONST_INT ciFRAME_STAGGERED_DEBUG_VEH				31
CONST_INT ciFRAME_STAGGERED_DEBUG_OBJ				32

eControllerDebugState debugState = eControllerDebugState_OFF

SCRIPT_TIMER DebugPassFailWaitTimer

INT iPlayerPressedF = -1
INT iPlayerPressedS = -1

MissionStageMenuTextStruct sTeamSkipStruct[FMMC_MAX_RULES + 2]
INT iSkipTeam = -1
BOOL bUsedDebugMenu
BOOL bSyncScenePedDebugDisplay

//For prints
TEXT_LABEL_31 tl31ScriptName				= "FM_Mission_Controler.sc"

CONST_INT LDEBUGBS_FIRST_DONTFAILONDEATH_CHECK	0
CONST_INT LDEBUGBS_TEAM_CHANGED_WAIT_FOR_OBJTXT	1

INT iLocalDebugBS

SCRIPT_TIMER tdDontFailOnDeathDelayTimer

BOOL boutputdebugspam = TRUE
BOOL bAddToGroupPrints = FALSE
BOOL bGotoLocPrints = FALSE
BOOL bStopVehDeadCleanup = FALSE
BOOL bPlaneDropoffPrints = FALSE
BOOL bDoPedCountPrints
BOOL bPlayerInDropOffPrints
BOOL bEnableDamageTrackerOnNetworkID
BOOL b4517649Debug = FALSE
BOOL bHideObjects = FALSE

BOOL bLBDHold

BOOL bDebugCircuitHack

INT iDebugRewardCash
INT iDebugCashPickup
INT iDebugXPDelVeh
INT iDebugXPDelObj
INT iAllKillXPBonus
INT iAllHeadshotXPPlayer
INT iAllHeadshotXPTeam
INT iDebugTotalRewardXP
INT iDebugXPLivesBonus
INT iDebugXPGiven
INT iDebugXPInvite
INT iDebugXPDailyWin
INT iDebugXP5Plays
INT iDebugDrillOutfitCheckOverride = -1

BOOL bDebugIsThermiteDebuggingEnabled
BOOL bDebugEnableThermiteReplay

INT iPedGivenDebugName[FMMC_MAX_PEDS_BITSET]

FLOAT fDebugDrillCamNearDofStart = -1.0
FLOAT fDebugDrillCamNearDofEnd = -1.0
FLOAT fDebugDrillCamFarDofStart = -1.0
FLOAT fDebugDrillCamFarDofEnd = -1.0

BOOL bDisableDeathsCount

BOOL bActivateArtificialLightsDebug
BOOL bPrintArticificalLightTimerValue
INT iDebugArtificialLightsOffDuration = -1

BOOL bWdGangChaseDebug
BOOL bWdLoseGangBackupDebug
BOOL bWdDropOffMarker = FALSE
BOOL bWdMultiTimer
BOOL bWdSeatPreferenceDebug

BOOL bWdPauseGameTimerRag
BOOL bWdPauseGameTimer
BOOL bWdPauseGameTimerStarted

BOOL bWdIncrementTimer
BOOL bWdDecrementTimer

BOOL bDebugCrossTheLineHUDPrints = FALSE

VECTOR vIWCamOffsetOverride
VECTOR vIWCamRotOverride

FLOAT fMinimapXOffset = 0.0
FLOAT fMinimapYOffset = 0.0

#ENDIF


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Local Bitsets
// ##### Description: Local bitset and bits
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//ADD ANY NEW iLocalBoolCheckXs TO PROCESS_BG_GLOBALS_PASS_OVER

CONST_INT ciPOPULATE_RESULTS								0
CONST_INT LBOOL_HIT_MOCAP_FAIL_SAFE							1
CONST_INT PLAYER_DEATH_TOGGLE   							2
CONST_INT LBOOL_COUNTED_FINAL_DEATH   						3
CONST_INT TEMP_TEAM_0_ACTIVE								4
CONST_INT TEMP_TEAM_1_ACTIVE								5
CONST_INT TEMP_TEAM_2_ACTIVE    							6
CONST_INT TEMP_TEAM_3_ACTIVE    							7
CONST_INT WAS_NOT_THE_HOST      							8
CONST_INT LBOOL_TRIGGER_CTF_END_MUSIC 						9
CONST_INT LBOOL_TRIGGER_CTF_PRE_END_MUSIC 					10
CONST_INT CHECK_SPEC_STATUS     							11
//CONST_INT FREE  											12
CONST_INT LBOOL_LB_FINISHED   								13
CONST_INT LBOOL_MISSION_SET_NOT_JOINABLE  					14
CONST_INT LBOOL_HAD_MORE_THAN_1_LIFE 						16
CONST_INT LBOOL_TDF_PLAY_IN_RANGE							17
CONST_INT LBOOL_ON_LAST_LIFE    							18
CONST_INT LBOOL_CARGOBOB_DROP   							19
CONST_INT LBOOL_MUSIC_CHANGED   							20
CONST_INT LBOOL_HAD_MORE_THAN_1_PACKAGE 					21
CONST_INT LBOOL_DPAD_HELP_DISP  							22
CONST_INT LBOOL_WITH_CARRIER_TEMP   						23
CONST_INT LBOOL_SHOW_ALL_PLAYER_BLIPS 						24
CONST_INT LBOOL_MISSION_READY_TO_WARP_START_POSITION 		25
CONST_INT LBOOL_SETTING_AFTER_DEATH_WANTED 					26
CONST_INT LBOOL_CURRENT_OBJECTIVE_LOSE_GANG_CHASE			27
CONST_INT LBOOL_IN_GROUP_PED_VEH_TEMP 						28
CONST_INT LBOOL_MADE_WANTED_EASY    						29
CONST_INT LBOOL_SAVED_EOM_VEHICLE    						30
CONST_INT LBOOL_WITH_CARRIER_FLAG 							31

INT iLocalBoolCheck

CONST_INT  LBOOL2_CLEAR_CELEBRATION_SCREEN 					0
CONST_INT  LBOOL2_LB_CAM_READY           					1
CONST_INT  LBOOL2_HANDLER_HELP_TEXT_NEEDED 					2
CONST_INT  LBOOL2_ACTIVATE_MOCAP_CUTSCENE 					3
CONST_INT  LBOOL2_ACTIVATE_MOCAP_CUTSCENE_OVER 				4
CONST_INT  LBOOL2_HACKING_TICKER_SENT     					5
CONST_INT  LBOOL2_WAS_IN_DEEP_WATER     					6
CONST_INT  LBOOL2_WAS_IN_FIRST_PERSON    					7
CONST_INT  LBOOL2_CHECKED_ZONE 								8
CONST_INT  LBOOL2_MOCAP_PLAYER_PLAYING_ANIM 				9 
CONST_INT  LBOOL2_STARTED_UPDATE       						10
CONST_INT  LBOOL2_WHOLE_UPDATE_DONE    						11
CONST_INT  LBOOL2_REQUEST_UPDATE       						12
CONST_INT  LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP 				13
CONST_INT  LBOOL2_CUTSCENE_WARP_DONE     					14
CONST_INT  LBOOL2_CUTSCENE_STREAMED      					15
CONST_INT  LBOOL2_MISSION_START_STAY_STILL 					16
CONST_INT  LBOOL2_BEAST_MODE_ACTIVE_ON_MISSION				17
CONST_INT  LBOOL2_LINE_MARKER_ACTIVE_ON_MISSION				18
CONST_INT  LBOOL2_CLEAR_END_BIG_MESSAGE						20
CONST_INT  LBOOL2_NORMAL_END            					21
CONST_INT  LBOOL2_WEATHER_SET            					22
CONST_INT  LBOOL2_FLUSH_FEED             					23
CONST_INT  LBOOL2_MY_TEAM_NAME_IS_CREW    					24
CONST_INT  LBOOL2_PED_DROPPED_OFF        					25
CONST_INT  LBOOL2_PASSED_MISSION          					26
CONST_INT  LBOOL2_SPAWN_SAFETY_FALLBACK  					27
CONST_INT  LBOOL2_START_SAFE_CRACK        					29
CONST_INT  LBOOL2_USE_CRATE_DELIVERY_DELAY 					30
CONST_INT  LBOOL2_BRING_VEHICLE_TO_HALT_CALLED 				31

INT iLocalBoolCheck2

CONST_INT  LBOOL3_MUSIC_PLAYING								0
CONST_INT  LBOOL3_TEAM_INTRO								1
CONST_INT  LBOOL3_RADAR_INIT								2
//FREE														3
//FREE														4
//FREE														5
CONST_INT  LBOOL3_SHOWN_RESULTS								6
CONST_INT  LBOOL3_LATE_SPECTATE								7
CONST_INT  LBOOL3_WAIT_END_SPECTATE							8
CONST_INT  LBOOL3_KILL_SPECTATE								9
CONST_INT  LBOOL3_NEW_JOB_SCREENS							10
CONST_INT  LBOOL3_MOVE_TO_END								11
CONST_INT  LBOOL3_EXPLOSIVE_ZONE_ACTIVE						12
CONST_INT  LBOOL3_CLEANED_UP_CAM							13
CONST_INT  LBOOL3_FAKE_CAM_MADE								14
CONST_INT  LBOOL3_REGISTERED_SHARED_RTS_FOR_LATE_JOINERS	15
CONST_INT  LBOOL3_GAMER_HANDLE_SET							16
//CONST_INT  FREE											17
CONST_INT  LBOOL3_DISPLAY_SUDDEN_DEATH						18
CONST_INT  LBOOL3_DISABLE_GPSANDHINTCAM						19
CONST_INT  LBOOL3_SAFE_FROM_WANTED_BOUNDS1					20
CONST_INT  LBOOL3_SAFE_FROM_WANTED_BOUNDS2					21
CONST_INT  LBOOL3_MISSION_CONTAINS_A_PLACED_YACHT			22
CONST_INT  LBOOL3_ADDED_DIALOGUE_PED						25
//FREE
CONST_INT  LBOOL3_SET_RESPAWN_WEAPON						27
CONST_INT  LBOOL3_WANTED_BOUNDS1_TOGGLE						28
CONST_INT  LBOOL3_WANTED_BOUNDS2_TOGGLE						29
CONST_INT  LBOOL3_OUTOFBOUNDS_TESTMODE						30
CONST_INT  LBOOL3_VELUM2_DOOR_MOVING						31

INT iLocalBoolCheck3

CONST_INT  LBOOL4_TRIGGER_ALARMS							0
CONST_INT  LBOOL4_T0GOTWANTED								2
CONST_INT  LBOOL4_T1GOTWANTED								3
CONST_INT  LBOOL4_T2GOTWANTED								4
CONST_INT  LBOOL4_T3GOTWANTED								5
CONST_INT  LBOOL4_T0LOSTWANTED								6
CONST_INT  LBOOL4_T1LOSTWANTED								7
CONST_INT  LBOOL4_T2LOSTWANTED								8
CONST_INT  LBOOL4_T3LOSTWANTED								9
CONST_INT  LBOOL4_DELIVERY_WAIT								10
CONST_INT  LBOOL4_INTRO_CUT_RULE_RENDERPHASE_PAUSED			11
CONST_INT  LBOOL4_OWN_MASK									12
CONST_INT  LBOOL4_IN_MASK_SHOP								13
CONST_INT  LBOOL4_BROWSING_SHOP								14
CONST_INT  LBOOL4_HEIST_HELP_TEXT_1							15
CONST_INT  LBOOL4_SCRIPTED_CUT_RENDER_CAMS_CALLED			23
CONST_INT  LBOOL4_MOCAP_START_FADE_IN_DONE					24
CONST_INT  LBOOL4_SCRIPTED_CUT_NEEDS_CLEANED_UP				25
CONST_INT  LBOOL4_CURRENT_OBJECTIVE_LOSE_WANTED				26
CONST_INT  LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY			28
CONST_INT  LBOOL4_WAIT_FOR_RESPAWN_IN_VEH					29
CONST_INT  LBOOL4_INTRO_CUT_RULE_RENDERPHASE_UNPAUSED		30
CONST_INT  LBOOL4_RESPAWN_SHOULD_FORCE_AREA_CHECK			31

INT iLocalBoolCheck4

CONST_INT 	LBOOL5_MOCAP_END_FADE_OUT_DONE					0
CONST_INT	LBOOL5_TEMP_ANYONE_IN_APPARTMENT			    7
CONST_INT	LBOOL5_GPS_DELIVER_ROUTE_SET					8
CONST_INT	LBOOL5_PED_INVOLVED_IN_CUTSCENE			    	10
CONST_INT	LBOOL5_SKIP_ARTIFICIAL_LIGHT_SOUNDS		    	11
CONST_INT	LBOOL5_SET_UP_STRAND_DETAILS_AT_END_OF_MISSION	12
CONST_INT	LBOOL5_MOCAP_REMOVED_LOCAL_PLAYER_MASK			13
CONST_INT	LBOOL5_MID_MISSION_MOCAP      			    	14
CONST_INT	LBOOL5_PED_ACTION_MODE							16

CONST_INT	LBOOL5_KEEP_FORCE_EVERYONE_FROM_MY_CAR			18
CONST_INT	LBOOL5_SCRIPT_FAKE_WANTED						19
CONST_INT	LBOOL5_DISPATCHOFF								20
CONST_INT	LBOOL5_OBJCOMP_KILLCHASEHINTCAM_1				21
CONST_INT	LBOOL5_OBJCOMP_KILLCHASEHINTCAM_2				22
CONST_INT	LBOOL5_WAS_IN_DROP_OFF							23
CONST_INT	LBOOL5_BLOCK_TURNING_OFF_ARTIFICIAL_LIGHTS_EOM	24
CONST_INT	LBOOL5_TDF_PLAY_LOSE_FIRST						25
CONST_INT	LBOOL5_HUD_COLOUR_RESET							26
CONST_INT	LBOOL5_GIVEN_SPAWN_VEHICLE_STARTING_VELOCITY	27
CONST_INT	LBOOL5_SHOW_CARRIER_IPL_MAP						28
//CONST_INT	FREE
CONST_INT	LBOOL5_TDF_PLAY_OUT_OF_RANGE					30
CONST_INT	LBOOL5_STARTED_COUNTDOWN_TIMER_SOUNDS			31

INT iLocalBoolCheck5

CONST_INT	LBOOL6_SHOW_RETRY_HELP      					0
CONST_INT	LBOOL6_SOMEONE_NEEDS_ZONE_ROCKETS				1
CONST_INT	LBOOL6_DISABLECONTROLTIMER_COMPLETE				2
CONST_INT	LBOOL6_IN_A_LOCATE								3
CONST_INT   LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO			4
CONST_INT   LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO           5
CONST_INT	LBOOL6_NEED_TO_CALCULATE_POSITION_AHEAD			6
CONST_INT	LBOOL6_DIALOGUE_ANIM_NEEDED						7
CONST_INT	LBOOL6_IN_MY_PRIORITY_LEAVE_LOC					8
CONST_INT 	LBOOL6_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC 		9
CONST_INT 	LBOOL6_TEAM_1_NEEDS_TO_WAIT_FOR_LEAVE_LOC 		10
CONST_INT 	LBOOL6_TEAM_2_NEEDS_TO_WAIT_FOR_LEAVE_LOC 		11
CONST_INT 	LBOOL6_TEAM_3_NEEDS_TO_WAIT_FOR_LEAVE_LOC 		12
CONST_INT	LBOOL6_SHOW_YACHT2_IPL_MAP						13
CONST_INT	LBOOL6_SAVED_ROUNDS_LBD_DATAa					14
CONST_INT	LBOOL6_SAVED_ROUNDS_LBD_DATAb					15
CONST_INT	LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME		17
CONST_INT	LBOOL6_TEAM_1_HAS_NEW_PRIORITY_THIS_FRAME		18
CONST_INT	LBOOL6_TEAM_2_HAS_NEW_PRIORITY_THIS_FRAME		19
CONST_INT	LBOOL6_TEAM_3_HAS_NEW_PRIORITY_THIS_FRAME		20
CONST_INT	LBOOL6_TEAM_0_HAS_NEW_MIDPOINT_THIS_FRAME		21
CONST_INT	LBOOL6_TEAM_1_HAS_NEW_MIDPOINT_THIS_FRAME		22
CONST_INT	LBOOL6_TEAM_2_HAS_NEW_MIDPOINT_THIS_FRAME		23
CONST_INT	LBOOL6_TEAM_3_HAS_NEW_MIDPOINT_THIS_FRAME		24
CONST_INT	LBOOL6_BLOCK_SAVE_EOM_CAR_WAS_DELIVERED			25
CONST_INT	LBOOL6_SCRIPTED_CUT_READY_FOR_PHONE_INTRO		26
CONST_INT	LBOOL6_HIDE_JOB_POINTS_CELEB_SCREEN				27
CONST_INT	LBOOL6_USING_NIGHTVISION_WHEN_ENTERING_CUTSCENE	28
CONST_INT	LBOOL6_HIDE_RADAR								29
CONST_INT	LBOOL6_SCRIPTED_CUT_DONE_FOCUS_INTRO			30
CONST_INT	LBOOL6_SCRIPTED_CUT_START_FOCUS_INTRO			31

INT iLocalBoolCheck6 

CONST_INT 	LBOOL7_ADDED_JP									0
CONST_INT	LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED			1
CONST_INT	LBOOL7_BLOCK_TOD_EVERY_FRAME					2
CONST_INT	LBOOL7_REFRESH_PLAYER_BLIPS						3
CONST_INT	LBOOL7_SECURITY_CAM_FILTER_USED					4
CONST_INT	LBOOL7_OUTFIT_ON_RULE_PROCESSING				5
CONST_INT	LBOOL7_SLIPSTREAM_SOUND_ACTIVE					6
CONST_INT	LBOOL7_SLIPSTREAM_ACTIVE					    7
CONST_INT	LBOOL7_HIDE_MOCAP_PLAYERS						8
CONST_INT	LBOOL7_HOST_APPLIED_NEW_PRIORITY_MIDFRAME		9 // This gets set on the host when the iCurrentHighestPriority is updated - used to stop things from spawning as it's a new rule, and things should clean up first
CONST_INT	LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE		10
CONST_INT	LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF			11
CONST_INT	LBOOL7_SET_END_MISSION_WEAPON					12
CONST_INT	LBOOL7_PRE_MAIN_WAS_A_SPECTATOR_CACHE			13
CONST_INT	LBOOL7_WAS_VEH_RADIO_ENABLED_AT_CS_START		14
CONST_INT	LBOOL7_CUTSCENE_PHONE_SAFETY_TRIGGERED			15
CONST_INT 	LBOOL7_DRAW_BLACK_RECTANGLE						16
CONST_INT	LBOOL7_IGNORE_RESPAWN_VISIBILITY_CHECKS			17
CONST_INT	LBOOL7_WATER_CALMING_QUAD_YACHT_CREATED			18
CONST_INT	LBOOL7_BIOLAB_EMP_SETTINGS_DONE			        19
CONST_INT	LBOOL7_SET_PLAYER_CONTROL_FOR_BRIDGE_MOCAP		20
CONST_INT	LBOOL7_SHOULD_CUTSCENE_DISABLE_PHONE_THIS_FRAME	21
CONST_INT	LBOOL7_DONE_CUTSCENE_HACKING_INTRO				22
CONST_INT	LBOOL7_DONE_CUTSCENE_HACKING_INTRO_INIT			23
CONST_INT	LBOOL7_HACKING_MINIGAME_COMPLETE				24
CONST_INT	LBOOL7_A_TEAM_HAS_A_NEW_PRIORITY_THIS_FRAME		25
CONST_INT	LBOOL7_A_TEAM_HAS_NEW_MIDPOINT_THIS_FRAME		26
CONST_INT	LBOOL7_RESET_ALL_HACKING_MINIGAMES_END_CALLED	27

INT iLocalBoolCheck7

CONST_INT	LBOOL8_HANDLE_PLAYLIST_POSITION_DATA_WRITE_CALLED	0
CONST_INT	LBOOL8_INITIAL_IPL_SET_UP_COMPLETE					1
CONST_INT	LBOOL8_CLEARED_HEADSHOTS							2
CONST_INT 	LBOOL8_DONE_HEIST_AWARD_HELP_CHECK					3
CONST_INT	LBOOL8_SHOULD_CELEBRATION_BE_SHOWN_PREMATURELY		4
CONST_INT	LBOOL8_REQUEST_LOAD_FMMC_MODELS_STAGGERED_DONE		5
CONST_INT	LBOOL8_skipped_to_mid_mission_cutscene_stage_5		6
CONST_INT	LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE				7
CONST_INT	LBOOL8_PLAYER_EXIT_STATE_CALLED						8
CONST_INT	LBOOL8_HEIST_DRILL_ASSET_LOADED						9
CONST_INT	LBOOL8_OUTRO_DONE									10
CONST_INT	LBOOL8_OUTRO_OK_TO_DO								11
CONST_INT	LBOOL8_ENTITIES_UNFROZEN_AT_START_OF_MISSION		12
CONST_INT	LBOOL8_NO_MOCAP_AFTER_THIS_DROPOFF					13
CONST_INT	LBOOL8_HAS_START_POSITION_WARPED					14
CONST_INT	LBOOL8_NAVMESH_REGION_REQUESTED						15
CONST_INT	LBOOL8_INTERACTABLE_CONTINUITY_INIT_DONE			16
CONST_INT	LBOOL8_OBJECT_CONTINUITY_INIT_DONE					17

INT iLocalBoolCheck8

CONST_INT	LBOOL9_FORCE_STREAM_CUTSCENE        				0
CONST_INT	LBOOL9_TIME_SET        								1
CONST_INT	LBOOL9_DO_INSTANT_DIALOGUE_LOOP						2
CONST_INT	LBOOL9_MIDPOINT_AUDIO_MIX_PROCESSED					3
CONST_INT 	LBOOL9_HAS_FIRST_OBJECTIVE_TEXT_BEEN_SHOWN			4
CONST_INT	LBOOL9_HEADSHOTS_REGENERATED						5
CONST_INT	LBOOL9_PRE_MOCAP_CINE_TRIGGERED						6
CONST_INT	LBOOL9_SET_IGNORE_NO_GPS_FLAG						7
CONST_INT 	LBOOL9_SHOULD_STOP_VEHICLE							8
CONST_INT 	LBOOL9_AT_LEAST_ONE_GANG_CHASE_UNIT_SPAWNED				9
CONST_INT	LBOOL9_CAN_SHOW_MANUAL_RESPAWN_HELP_TEXT			10
CONST_INT	LBOOL9_SCTV_LOADDED_FMMC_BLOCK						11
CONST_INT	LBOOL9_MASK_REPLACED_WITH_HELMET					12
CONST_INT	LBOOL9_TEMP_WAS_IN_LOCATION_THIS_FRAME				13
CONST_INT	LBOOL9_MEDAL_RANKINGS_DONE							14
CONST_INT	LBOOL9_COPIED_CELEBRATION_DATA_EVENT_1				15
CONST_INT	LBOOL9_COPIED_CELEBRATION_DATA_EVENT_2				16

INT iLocalBoolCheck9

CONST_INT	LBOOL10_ALL_SHOPS_LOCKED							0
CONST_INT	LBOOL10_PLAYER_IN_TURRET_SEAT						1
CONST_INT	LBOOL10_PLAYER_WEARING_MASK							2
CONST_INT	LBOOL10_PAC_FIN_PARACHUTE_GIVEN						3
CONST_INT	LBOOL10_PLAYER_ACTION_MODE_RESET					4
CONST_INT	LBOOL10_VEH_SPAWN_AT_SECOND_POSITION				5
CONST_INT	LBOOL10_PED_SPAWN_AT_SECOND_POSITION				6
CONST_INT	LBOOL10_OBJ_SPAWN_AT_SECOND_POSITION				7
CONST_INT	LBOOL10_IN_MAP_BOUNDS								8
CONST_INT	LBOOL10_DROP_OFF_LOC_FADE_OUT						9
CONST_INT 	LBOOL10_DROP_OFF_LOC_DISPLAY						10
CONST_INT	LBOOL10_HUMANE_FLARE_PROCESSING_DONE				11
CONST_INT 	LBOOL10_STOP_CAR_FOREVER							12
CONST_INT 	LBOOL10_DRIVING_AND_CARRYING_PED					13
CONST_INT	LBOOL10_HEIST_HELP_TEXT_AVI_RASHKOVSKY				14
CONST_INT	LBOOL10_LOCAL_PLAYER_IN_CUTSCENE					15
CONST_INT	LBOOL10_HUMANE_FLARE_CREATED						16
CONST_INT	LBOOL10_PLAYED_CELEB_PRE_LOAD_FOR_HUD_CLEAR			17
CONST_INT	LBOOL10_RESPAWNING_DURING_FAIL						18
CONST_INT	LBOOL10_PLACED_REVERSE_ANGLE_CAMERA					19
CONST_INT	LBOOL10_NEED_TO_CHECK_IN_VEH_FOR_DUMMY_BLIPS		20
CONST_INT	LBOOL10_FINGERPRINT_HACK_CHECKED					21
CONST_INT	LBOOL10_FINGERPRINT_HACK_USED						22
CONST_INT	LBOOL10_VOLTAGE_HACK_CHECKED						23
CONST_INT	LBOOL10_VOLTAGE_HACK_USED							24
CONST_INT	LBOOL10_VEHICLE_SWAPS_CHECKED						25
CONST_INT	LBOOL10_VEHICLE_SWAPS_USED							26
CONST_INT	LBOOL10_GRATE_CUTTING_USED							26
CONST_INT	LBOOL10_GRATE_CUTTING_CHECKED						27
CONST_INT 	LBOOL10_PROCESSED_END_OF_MISSION_AWARDS				28

INT iLocalBoolCheck10

CONST_INT	LBOOL11_WAS_IN_VEH_FOR_DUMMY_BLIPS						0
CONST_INT	LBOOL11_NEED_TO_CHECK_IN_VEH_FOR_DUMMY_BLIPS_TEMP		1
CONST_INT	LBOOL11_STOP_MISSION_FAILING_DUE_TO_EARLY_CELEBRATION	2
CONST_INT	LBOOL11_BIG_MAP_WAS_ZOOMED_IN_FOR_CARRIER				3
CONST_INT	LBOOL11_SET_SHOULD_STOP_IN_DROP_OFF						4
CONST_INT 	LBOOL11_WAS_ATTACHED_TO_CARGOBOB						5
CONST_INT	LBOOL11_BIG_MAP_DO_ZOOM_IN_FOR_CARRIER					6
CONST_INT 	LBOOL11_IN_VALID_VEHICLE_FOR_WANTED_BOUNDS1				7
CONST_INT 	LBOOL11_IN_VALID_VEHICLE_FOR_WANTED_BOUNDS2				8
CONST_INT	LBOOL11_RETRY_CHECKPOINT_3								9
CONST_INT	LBOOL11_RETRY_CHECKPOINT_4								10
CONST_INT	LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME				11 // This gets set on the host when the midpoint is updated - used to stop things from spawning as it's a new midpoint, and things should clean up first
CONST_INT	LBOOL11_MASK_EQUIP_ON_QUICK_RESTART			        	12
CONST_INT	LBOOL11_BLOCK_LOOK_AT_PLAYER_TASK			        	13
CONST_INT	LBOOL11_CARRIER_LIGHTS_PRELOADED						14
CONST_INT	LBOOL11_PLAYER_IS_RESPAWNING							15
CONST_INT	LBOOL11_IGNORE_CHECKPOINTS_DUE_TO_SKIP_TEAM_0			16
CONST_INT	LBOOL11_IGNORE_CHECKPOINTS_DUE_TO_SKIP_TEAM_1			17
CONST_INT	LBOOL11_IGNORE_CHECKPOINTS_DUE_TO_SKIP_TEAM_2			18
CONST_INT	LBOOL11_IGNORE_CHECKPOINTS_DUE_TO_SKIP_TEAM_3			19
CONST_INT	LBOOL11_FORCE_SNIPER_SCOPE								20
CONST_INT	LBOOL11_FORCE_SNIPER_SCOPE_ACTIVE						21
CONST_INT	LBOOL11_FORCE_SNIPER_SCOPE_ACTIVE_AIMING				22
CONST_INT	LBOOL11_FORCE_SNIPER_SCOPE_ACTIVE_ZOOMING				23
CONST_INT	LBOOL11_VEHICLE_CACHING_REQUIRED_CHECKED_TEAM_0			24
CONST_INT	LBOOL11_VEHICLE_CACHING_REQUIRED_CHECKED_TEAM_1			25
CONST_INT	LBOOL11_VEHICLE_CACHING_REQUIRED_CHECKED_TEAM_2			26
CONST_INT	LBOOL11_VEHICLE_CACHING_REQUIRED_CHECKED_TEAM_3			27
CONST_INT	LBOOL11_VEHICLE_CACHING_REQUIRED_TEAM_0					28
CONST_INT	LBOOL11_VEHICLE_CACHING_REQUIRED_TEAM_1					29
CONST_INT	LBOOL11_VEHICLE_CACHING_REQUIRED_TEAM_2					30
CONST_INT	LBOOL11_VEHICLE_CACHING_REQUIRED_TEAM_3					31

INT iLocalBoolCheck11

CONST_INT	LBOOL12_OBJ_RES_LAPTOP				                0
CONST_INT	LBOOL12_OBJ_RES_BAG 				                1
CONST_INT	LBOOL12_OBJ_RES_MOBILE_PHONE 				        4
CONST_INT	LBOOL12_OBJ_RES_DRILL_WALL_AND_DOOR 				5
CONST_INT	LBOOL12_OBJ_RES_KEYCARD 				            6
CONST_INT	LBOOL12_OBJ_RES_UNDERWATER_FLARE 				    7
CONST_INT	LBOOL12_OBJ_RES_DRILL            			        9
CONST_INT	LBOOL12_OBJ_RES_DRILL_BAG            			    10
CONST_INT	LBOOL12_OBJ_RES_DRILL_BOX            			    11
CONST_INT	LBOOL12_OBJ_RES_DRILL_HOLE            			    12
CONST_INT	LBOOL12_OBJ_RES_THERMITE_CHARGE           			13
CONST_INT	LBOOL12_OBJ_RES_THERMITE_CHARGE_FLASH               14
CONST_INT	LBOOL12_OBJ_RES_MONEY                               15
CONST_INT	LBOOL12_RETRY_CHECKPOINT_1							16
CONST_INT	LBOOL12_RETRY_CHECKPOINT_2							17
CONST_INT	LBOOL12_DONE_HELMET_VISOR_UP_ON_INIT				18
CONST_INT	LBOOL12_TREVOR_COVER_ANIMS_LOADED					20
CONST_INT	LBOOL12_STATION_DRUNK_PED_STREAM_STARTED            23
CONST_INT   LBOOL12_SERVER_STILL_THINKS_IM_HACKING				24
CONST_INT   LBOOL12_SERVER_STILL_THINKS_IM_HACKING_TEMP			25
CONST_INT	LBOOL12_SERVER_TEAM_0_IS_HACKING_TEMP				26
CONST_INT	LBOOL12_SERVER_TEAM_1_IS_HACKING_TEMP				27
CONST_INT	LBOOL12_SERVER_TEAM_2_IS_HACKING_TEMP				28
CONST_INT	LBOOL12_SERVER_TEAM_3_IS_HACKING_TEMP				29
CONST_INT	LBOOL12_LOBBY_LEADER_QUIT_YACHT_CHANGED				30
CONST_INT	LBOOL12_USE_HUNTER_VISION							31

INT iLocalBoolCheck12

CONST_INT	LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE					0
CONST_INT	LBOOL13_IN_BLOCK_AND_CLEAR_WANTED_LEVEL_ZONE		1
CONST_INT	LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE_TEMP				2
CONST_INT	LBOOL13_NOT_SAFE_FOR_ALTERNATE_CAM					3
CONST_INT	LBOOL13_UNTOGGLED_RENDERPHASES_BEFORE_LEADERBOARD	4
CONST_INT	LBOOL13_DISPLAYED_INTRO_SHARD						5
CONST_INT	LBOOL13_CONTROL_PAD_COLOUR_SHOULD_BE_RESET			6
CONST_INT   LBOOL13_FIRST_STAGGERED_LOOP_DONE_CLIENTSIDE        7
CONST_INT	LBOOL13_RESPAWNBOUNDS1_CHOSEN_IN_FRONT				8
CONST_INT	LBOOL13_RESPAWNBOUNDS1_CHOSEN_NOT_IN_FRONT			9
CONST_INT	LBOOL13_RESPAWNBOUNDS2_CHOSEN_IN_FRONT				8
CONST_INT	LBOOL13_RESPAWNBOUNDS2_CHOSEN_NOT_IN_FRONT			9
CONST_INT	LBOOL13_DRAW_DISTANCE_TO_CROSS_LINE					10
CONST_INT	LBOOL13_MIN_SPEED_EXPLODE_BAR_INCREASING			12
CONST_INT	LBOOL13_EARLY_STREAM_RESPAWN_BAR					13
CONST_INT	LBOOL13_DONE_DIALOGUE_LOOP_AFTER_COPS_SPOTTED		14
CONST_INT	LBOOL13_SHOW_HEIST_SPECTATE_HELP					15
CONST_INT	LBOOL13_PRIORTISED_FVJ_HELPTEXT						16
CONST_INT	LBOOL13_PRIORTISED_THANKSGIVING_HELPTEXT			17
CONST_INT	LBOOL13_SHOWN_TEAM_SHARD							18
CONST_INT	LBOOL13_PLAYED_STOP_HUD_SFX							19
CONST_INT	LBOOL13_PLAYERINPEDBLOCKINGZONE						20
CONST_INT	LBOOL13_PLAYERINPEDBLOCKINGZONE_NEWMAXWANTEDSET		21
CONST_INT	LBOOL13_CANADDPEDTOGROUP_JOINGROUPFROMVAN_DOORSBLOWN	22
CONST_INT	LBOOL13_CANADDPEDTOGROUP_JOINGROUPFROMVAN_STANDARDCHECK	23
CONST_INT  	LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2			24
CONST_INT	LBOOL13_REINITTED_MUSIC_FOR_ENTERING_SPECTATE		25
CONST_INT	LBOOL13_HWN_SWITCHOVER_COUNTDOWN_PLAYED				26
CONST_INT	LBOOL13_HWN_ENDGAME_COUNTDOWN_PLAYED				27
CONST_INT	LBOOL13_HWN_COUNTDOWN_SWITCHED						28
CONST_INT	LBOOL13_MY_TEAM_HAS_A_NEW_LAP						31

INT iLocalBoolCheck13

CONST_INT	LBOOL14_OTHER_TEAM_IN_BEAST_MODE					0
CONST_INT	LBOOL14_IN_BEAST_MODE								1
CONST_INT	LBOOL14_BEAST_STEALTH_TOGGLE						2
CONST_INT	LBOOL14_IN_BEAST_COSTUME							3
CONST_INT	LBOOL14_BEAST_IS_STEALTHED							4
CONST_INT	LBOOL14_ALLOW_LESTER_SNAKE_MINIGAME					5
CONST_INT	LBOOL14_HAS_FORCED_OFF_LOCK_ON						6
CONST_INT	LBOOL14_CAN_SHOW_BEAST_POWERS_UNAVAILABLE_HELP_TEXT	7
CONST_INT 	LBOOL14_BLIP_ON_LOCATE								8
CONST_INT	LBOOL14_DAMAGED_BY_SUPER_JUMP						9
CONST_INT	LBOOL14_VFX_CAPTURE_OWNER							10
CONST_INT	LBOOL14_BEAST_SFX_LOADED							11
CONST_INT	LBOOL14_DONE_BEAST_LANDING_SOUND					13
CONST_INT	LBOOL14_INSURGENT_GUN_SEAT_HELP						14
CONST_INT	LBOOL14_HAS_STARTED_APT_SUDDEN_DEATH_MUSIC			15
CONST_INT	LBOOL14_HAS_KILLED_APT_SUDDEN_DEATH_MUSIC			16
CONST_INT	LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC				17
CONST_INT	LBOOL14_HAS_KILLED_APT_COUNTDOWN_MUSIC				18
CONST_INT	LBOOL14_COUNTDOWN_EARLY_MISSION_END					19
CONST_INT	LBOOL14_APT_STARTED_PRE_COUNTDOWN					20
CONST_INT	LBOOL14_DZ_STARTED_CARGOBOB_SOUNDS					21
CONST_INT	LBOOL14_PLAYED_5S_COUNTDOWN_ON_30S_SETTING_CD		22
CONST_INT	LBOOL14_OUT_OF_BOUNDS_DEATHFAIL						23 //suddendeath version
CONST_INT	LBOOL14_VEHICLE_ROCKET_ASSET_REQUEST				24
CONST_INT 	LBOOL14_THERMALVISION_INITIALISED					25
CONST_INT	LBOOL14_RESET_HUNTER_VISION							26
CONST_INT	LBOOL14_HUNTER_VISION_COOLDOWN						27
CONST_INT	LBOOL14_TRIGGER_HUNTED_RESPAWN_EFFECT				28
CONST_INT	LBOOL14_TRIGGER_HUNTED_DEATH_EFFECT					29

INT iLocalBoolCheck14

CONST_INT	LBOOL15_HAS_STARTED_APT_SUDDEN_DEATH_END			0

CONST_INT	LBOOL15_IS_OUT_OF_BOUNDS_SHARD_DISPLAYED			2
CONST_INT	LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_ROCKETS			3
CONST_INT	LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_BOOST			4
CONST_INT	LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_REPAIR			5
CONST_INT	LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_SPIKES			6
CONST_INT  	LBOOL15_IS_SUDDEN_DEATH_SOUND_PLAYING				7
CONST_INT   LBOOL15_HAS_ELIMINATION_COUNTDOWN_FINISHED			8
CONST_INT	LBOOL15_IS_SUDDEN_DEATH_BLIP_SETUP					9
CONST_INT	LBOOL15_SSDS_SUDDEN_DEATH_STARTED					10
CONST_INT	LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC				11
CONST_INT	LBOOL15_HAS_KILLED_VAL_COUNTDOWN_MUSIC				12
CONST_INT	LBOOL15_TURN_BACK_ON_RADIO_CONTROL					13
CONST_INT	LBOOL15_ROUNDS_MISSION_END_SET_UP					14
CONST_INT	LBOOL15_DRAIN_HEALTH_INITIALISED					15
CONST_INT	LBOOL15_SUDDEN_DEATH_VALENTINES_ONE_HIT_KILL_INIT	16
CONST_INT	LBOOL15_CALL_SWAP_TEAM_LOGIC						17
CONST_INT	LBOOL15_HAS_STARTED_VAL_SUDDEN_DEATH_END			18
CONST_INT	LBOOL15_HAS_STARTED_VAL_SUDDEN_DEATH_FADE			19
CONST_INT	LBOOL15_HAS_LOWERED_CB_RAMP_1						20
CONST_INT	LBOOL15_HAS_LOWERED_CB_RAMP_2						21
CONST_INT	LBOOL15_HAS_LOWERED_CB_RAMP_3						22
CONST_INT	LBOOL15_HAS_LOWERED_CB_RAMP_4						23
CONST_INT	LBOOL15_HAS_LOWERED_CB_RAMP_5						24
CONST_INT	LBOOL15_HAS_LOWERED_CB_RAMP_6						25
CONST_INT	LBOOL15_HAS_LOWERED_CB_RAMP_7						26
CONST_INT	LBOOL15_HAS_LOWERED_CB_RAMP_8						27
CONST_INT	LBOOL15_RESET_30S_CD_BOOLS_NEXT_RULE				28
CONST_INT	LBOOL15_CALLED_POPULATE_FAIL_RESULTS				29

INT iLocalBoolCheck15

CONST_INT	LBOOL16_PLAYED_10S_SD_SFX							0
CONST_INT	LBOOL16_PLAYED_5S_SD_SFX							1
CONST_INT	LBOOL16_TDF_PLAY_TEAM_TAKES_LEAD					2
CONST_INT	LBOOL16_BLIP_ON_COLLECT								3
CONST_INT	LBOOL16_HAS_STOPPED_MUSIC_ON_LEADERBOARD			4
CONST_INT	LBOOL16_TEAM_0_IS_PAST_THEIR_FIRST_RULE				5
CONST_INT	LBOOL16_TEAM_1_IS_PAST_THEIR_FIRST_RULE				6
CONST_INT	LBOOL16_TEAM_2_IS_PAST_THEIR_FIRST_RULE				7
CONST_INT	LBOOL16_TEAM_3_IS_PAST_THEIR_FIRST_RULE				8
CONST_INT	LBOOL16_HAS_SENT_RUGBY_PICKUP_SOUND_EVENT			9
CONST_INT 	LBOOL16_HAS_STARTED_BALL_CARRIER_LOOP_SOUND			10
CONST_INT	LBOOL16_HAS_PLAYED_IAO_PICKUP_SOUND					11
CONST_INT	LBOOL16_IS_DIVE_SCORE_ANIM_IN_PROGRESS				12
CONST_INT	LBOOL16_DIVE_SCORE_SCREEN_FX_ACTIVE					13
CONST_INT	LBOOL16_IAO_BLOCK_PACKAGE_SOUNDS					14
CONST_INT	LBOOL16_TDF_PLAY_PLAYER_ENTERS_CHECKPOINT			15
CONST_INT	LBOOL16_WEAPON_PICKUPS_BLOCKED						16
CONST_INT	LBOOL16_SWAN_DIVE_ANIM_VALIDATED					17
CONST_INT	LBOOL16_SUDDEN_DEATH_REASON_SHARD_SENT				18
CONST_INT 	LBOOL16_YOU_ARE_THE_LAST_DELIVERER					19
CONST_INT	LBOOL16_INVINCIBLE_DURING_POINT_CAM					20
CONST_INT	LBOOL16_SUDDEN_DEATH_RUN_PLAYER_SCORE_UPDATE		21
CONST_INT	LBOOL16_CHANGE_PLAYER_TEAM_ALIVE_VARS_SETTER		22
CONST_INT	LBOOL16_SHOULD_SHOW_TEAM_SWITCH_SHARD				23
CONST_INT	LBOOL16_VEHICLE_MARKED_FOR_CLEAN_UP					24
CONST_INT	LBOOL16_SUDDEN_DEATH_REQUEST_PLAYER_SCORE_UPDATE	25
CONST_INT	LBOOL16_FORCE_BLIP_ALL_PLAYERS_TRIGGERED			26

INT iLocalBoolCheck16

CONST_INT	LBOOL17_HAS_PLAYED_PP_SUDDEN_DEATH_ANNOUNCER		1
CONST_INT	LBOOL17_PROPS_HAVE_BEEN_HIDDEN						2
CONST_INT	LBOOL17_HUD_HIDDEN_AT_START_ONCE					3
CONST_INT	LBOOL17_CLEANUP_BEAST_MODE_ON_RESPAWN				4
//CONST_INT	FREE												5
CONST_INT	LBOOL17_TEAM_SWAP_PICKUP_PROCESS					6
CONST_INT	LBOOL17_PICKUP_STAGGERED_LOOP_INDEX_SET		7
CONST_INT	LBOOL17_IS_VW_SHARD_VISIBLE							8
CONST_INT	LBOOL17_DR_REQUESTED_ANIM							9
CONST_INT	LBOOL17_KILLED_CONVERSATIONS_FOR_PP_SD_ANN			10

INT iLocalBoolCheck17

CONST_INT	LBOOL18_RULE_CHANGE_SHARD_FIRST_RULE				0
CONST_INT	LBOOL18_IS_SPEED_BOOST_FILTER_DISABLED				1
CONST_INT	LBOOL18_GUNSMITH_REPLENISH_AMMO_REQUIRED			2
CONST_INT	LBOOL18_MELEE_DAMAGE_MODIFIER_RESPAWN				3
CONST_INT	LBOOL18_GUNSMITH_EQUIPPING_WEAPON					4
CONST_INT	LBOOL18_TDF_PLAY_BOOST_SOUND						5
CONST_INT	LBOOL18_SD_TIMECYCLE_SET							6
//FREE
//FREE
CONST_INT	LBOOL18_TLAD_JUDGEMENT_DAY_PTFX_ACTIVE				9
//FREE
CONST_INT	LBOOL18_ENABLE_GUNSMITH_AUDIO_SOUNDS				11
CONST_INT	LBOOL18_WEAPONS_ENABLED_SHARD_PLAY					12
CONST_INT	LBOOL18_CHECK_FOR_DEATH_OBJ_TEXT_REPRINT			13
CONST_INT	LBOOL18_DIED_AND_REQUIRES_OBJ_TEXT_REPRINT			14
CONST_INT	LBOOL18_LOADED_TOUR_DE_FORCE_SOUNDS					15
CONST_INT	LBOOL18_GUNSMITH_NEW_WEAPON_READY					16
CONST_INT	LBOOL18_RULE_CHANGE_SHARD_PLAYED_TEAM_0				17
CONST_INT	LBOOL18_RULE_CHANGE_SHARD_PLAYED_TEAM_1				18
CONST_INT	LBOOL18_RULE_CHANGE_SHARD_PLAYED_TEAM_2				19
CONST_INT	LBOOL18_RULE_CHANGE_SHARD_PLAYED_TEAM_3				20
CONST_INT	LBOOL18_WEAPON_LOADOUT_READY						21

INT iLocalBoolCheck18

CONST_INT	LBOOL19_PLAY_LEADER_SLIPSTREAM_AUDIO					0
CONST_INT	LBOOL19_GUNSMITH_WEAPON_EQUIP_EFFECTS_BLOCKER			1
CONST_INT	LBOOL19_EQUIP_REQUIRES_PTFX								2
CONST_INT	LBOOL19_NEED_TO_SHOW_LAP_SHARD							3
CONST_INT	LBOOL19_SHOULD_EXTRA_HUD_BE_SHOWING						4
CONST_INT	LBOOL19_RACE_SPLIT_TIMES_STARTED						5
CONST_INT	LBOOL19_CHECKED_DROPOFF_CUTSCENE						6
CONST_INT	LBOOL19_SET_UP_VEHICLE_FOR_STUNT_RACE					7
CONST_INT	LBOOL19_SHOW_LAST_LAP_HELP_TEXT							8
CONST_INT	LBOOL19_SLIPSTREAM_PASSED_CHECKPOINT					9
CONST_INT	LBOOL19_MUSIC_COUNTDOWN_INITIATED						10
CONST_INT 	LBOOL19_YACHT_AQUARIS_ASSETS_REQUESTED					11
CONST_INT 	LBOOL19_YACHT_AQUARIS_ASSETS_LOADED						12
CONST_INT 	LBOOL19_AUTO_PARACHUTE_ZONE_ACTIVATED					13
CONST_INT 	LBOOL19_CACHE_PLAYER_POS_FOR_GPS_TRACKING				14
CONST_INT 	LBOOL19_TRIGGERED_CONTAINER_INVESTIGATION				15

INT iLocalBoolCheck19

CONST_INT	LBOOL20_INITIAL_SCORE_COUNT_DONE						0
CONST_INT	LBOOL20_JUGGERNAUT_LEAVING_SWAP_DONE					1
CONST_INT	LBOOL20_OBJ_RES_CONTAINER_PICK_UP				        2
CONST_INT	LBOOL20_TURF_WAR_DISABLE_PROP_ROUND_END					3
CONST_INT	LBOOL20_HACKING_LOOP_STARTED							4
CONST_INT	LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC					5
CONST_INT 	LBOOL20_HAS_KILLED_IE_COUNTDOWN_MUSIC					6
CONST_INT	LBOOL20_PLAYER_PROOFS_ALTERED							7
CONST_INT	LBOOL20_JUGGERNAUT_SUIT_MOVEMENT_SET					8
CONST_INT	LBOOL20_START_TURFWAR_TIMER_COUNTDOWN					9
CONST_INT	LBOOL20_CARGOBOB_POSITIONED								10
CONST_INT	LBOOL20_CARGOBOB_CATCH_FINISHED							11
CONST_INT	LBOOL20_TURNED_OFF_RADIO_FOR_END_CUTSCENE				12
CONST_INT	LBOOL20_PARTICLE_FX_OVERRIDE_COCAINE					13
CONST_INT	LBOOL20_SKIP_HELP_TEXT									14
CONST_INT	LBOOL20_SHOWN_BAG_FULL_HELP								15
CONST_INT	LBOOL20_INTERROGATION_USING_ALT_ENDING					16

INT iLocalBoolCheck20

CONST_INT	LBOOL21_SET_CAR_HIGHER_JUMP								0
CONST_INT	LBOOL21_USING_CUSTOM_RESPAWN_POINTS						1
CONST_INT	LBOOL21_SHOULD_CLEAR_CUSTOM_RESPAWN_POINTS				2
CONST_INT	LBOOL21_DUNE4_RAMP_BUGGY_SIDE_IMPULSE_ACTIVE			3
CONST_INT	LBOOL21_VEHICLE_SWAP_RETRACTABLE_WHEEL_BUTTON			4
CONST_INT	LBOOL21_VEHICLE_DAMAGE_MODIFIER_OUTPUT_RESPAWN			5
CONST_INT	LBOOL21_VEHICLE_DAMAGE_MODIFIER_INTAKE_RESPAWN			6
CONST_INT	LBOOL21_VEHICLE_HEALTH_UDPATED							7
CONST_INT	LBOOL21_PARTICLE_FX_OVERRIDE_BOX_PILE					8

INT iLocalBoolCheck21

CONST_INT	LBOOL22_SWITCHED_ROCKET_TYPE							0
CONST_INT	LBOOL22_SWITCHED_ROCKET_AMMO							1
CONST_INT	LBOOL22_VEHICLE_AMMO_RELOAD_ENGAGED						2
CONST_INT	LBOOL22_USING_CUSTOM_VEHICLE_AMMO						3
CONST_INT	LBOOL22_START_PAUSE_CAM_FOR_RESET						4

INT iLocalBoolCheck22

CONST_INT	LBOOL23_VEHICLE_SET_EXPLOSION_PROOF					 	0
CONST_INT	LBOOL23_CLEANUP_OUTFIT_ON_RESPAWN					 	1
CONST_INT	LBOOL23_SOMEONE_IS_HACKING_A_VEHICLE					2
CONST_INT	LBOOL23_HAS_PLAYER_BEEN_GIVEN_A_REBREATHER				3
CONST_INT	LBOOL23_LOCATE_TEAM_SWAP_REQUESTED						4
CONST_INT	LBOOL23_LOCATE_TEAM_SWAP_INITIALISED					5
CONST_INT	LBOOL23_BLIP_FORCED_ON_BY_ZONE							6
CONST_INT	LBOOL23_TEAM_SWAP_IN_MISSION							6
CONST_INT	LBOOL23_SPEED_BAR_AIRSTRIKE_CALLED						7
CONST_INT	LBOOL23_AIRSTRIKE_SPEED_BAR_FIRED						8
CONST_INT	LBOOL23_AIRSTRIKE_FIFTY_PERCENT_STRIKE					9
CONST_INT	LBOOL23_DIED_BEFORE_SUDDEN_DEATH						10

INT iLocalBoolCheck23

CONST_INT	LBOOL24_AIRSTRIKE_IN_PROGRESS							0
CONST_INT	LBOOL24_LAST_PLAYER_ALIVE								1
CONST_INT	LBOOL24_AIRSTRIKE_FINAL_SHOT_FIRED						2
CONST_INT	LBOOL24_VEHICLE_ALLOW_MOVEMENT_WHILE_INACTIVE			3
CONST_INT 	LBOOL24_SPECIAL_ABILITY_REFILLED						4
CONST_INT 	LBOOL24_IN_CREATOR_TRAILER								5
CONST_INT	LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP						6
CONST_INT	LBOOL24_AIRSTRIKE_DIALOGUE_TRIGGERED					8
CONST_INT	LBOOL24_BLOCK_OBJECTIVE_TEXT_UNTIL_RULE_CHANGE			9
CONST_INT	LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK				10
CONST_INT	LBOOL24_SHOW_VEHICLE_RESPAWN_TEXT						11
CONST_INT	LBOOL24_FLASH_PED_BLIP_ACTIVE							12
CONST_INT	LBOOL24_FLASH_PED_BLIP_EXPIRED							13
CONST_INT	LBOOL24_SUDDEN_DEATH_DROP_PICKUPS						14
CONST_INT	LBOOL24_PULSED_CIRCLE_HUD_TIMER							15
CONST_INT	LBOOL24_MANUAL_RESPAWN_DROP_PICKUPS						16

INT iLocalBoolCheck24

CONST_INT	LBOOL25_ALTERNATIVE_GD_SCORE_ADDED						0
CONST_INT	LBOOL25_VEHICLE_WEAPONS_HOMING_DISABLED					1
CONST_INT	LBOOL25_SUDDEN_DEATH_SPAWN_PROTECTION_DISABLED			2
CONST_INT	LBOOL25_FORCE_INTERIOR_ALT_1_MUSIC_TO_PLAY				3
CONST_INT	LBOOL25_FORCED_INTO_HEIST_SPECTATE						4
CONST_INT	LBOOL25_DRIVER_DIED_OR_RESPAWNED_INTO_NEW_VEH			5
CONST_INT	LBOOL25_SAVED_ACTUAL_VEHICLE_SEAT_TO_MULTI_VEH_DATA		6
CONST_INT	LBOOL25_INITIALIZED_SEATING_DATA_FOR_MULTI_VEH			7
CONST_INT	LBOOL25_LOCAL_PLAYER_PROOFS_NEEDS_REFRESH				8
CONST_INT	LBOOL25_CUTSCENE_BAIL_FROM_MISSION_FAILURE				9
CONST_INT	LBOOL25_MANAGE_LOCAL_PLAYER_INIT_CALLED					10
CONST_INT	LBOOL25_BLOCK_MANUAL_RESPAWN_DUE_TO_VEHICLE				12
CONST_INT	LBOOL25_CANNOT_BE_DAMAGED_DUE_TO_VEHICLE				13
CONST_INT	LBOOL25_RESET_ALL_PROPS_ON_RENDERPHASE_PAUSE			14
CONST_INT	LBOOL25_SUMO_RUN_TIMER_DELAY							15
CONST_INT	LBOOL25_HOLDING_CONTROL									17
CONST_INT	LBOOL25_PASSENGER_MANUALLY_RESPAWNING					18
CONST_INT	LBOOL25_CLEANUP_RESPAWN_SPAWN_AT_START_NO_VEH			19
CONST_INT	LBOOL25_MUSIC_STARTED_DRE_TRACK							20
CONST_INT	LBOOL25_MUSIC_STARTED_DRE_SHOOTOUT						21
CONST_INT	LBOOL25_PLAYER_SET_FORCED_STEALTH_THIS_RULE				22
CONST_INT	LBOOL25_USING_CAMERA_SHAKING_ON_RULE					23
CONST_INT	LBOOL25_FINISHED_CAMERA_SHAKING_ON_RULE					24
CONST_INT	LBOOL25_JUMPED_FROM_OPPRESSOR_SKY_DIVE					25
CONST_INT	LBOOL25_A_PLAYER_HAS_SUPER_ALERTED_METAL_DETECTOR		28
CONST_INT	LBOOL25_STROMBERG_FORCE_GPS_ACTIVE						29
CONST_INT	LBOOL25_CLEAR_FAKE_CONE_ARRAY_CALLED					30 
CONST_INT	LBOOL25_ORBITAL_CANNON_DIALOGUE_AIRSTRIKE				31

INT iLocalBoolCheck25

CONST_INT	LBOOL26_INTERIOR_AUDIO_MOUNTAINBASE						0
CONST_INT	LBOOL26_INTERIOR_AUDIO_FACILITY_PROPERTY				1
CONST_INT	LBOOL26_INTERIOR_AUDIO_SUBMARINE						2
CONST_INT	LBOOL26_SCUBA_GEAR_ON									3
CONST_INT	LBOOL26_FIRST_STAGGERED_LOOP_OF_VEH_BODY				4
CONST_INT	LBOOL26_HARD_TARGET_OUTFIT_CHANGED						5
CONST_INT	LBOOL26_HARD_TARGET_CHANGED_SOUND						6
CONST_INT	LBOOL26_OOB_ON_FOOT_EXPLODE_DONE						8
CONST_INT	LBOOL26_STARTED_HOTWIRE_HACK							9
CONST_INT	LBOOL26_STARTED_BEAM_HACK								10
CONST_INT	LBOOL26_PUT_BACK_INTO_STEALTH_MODE_WEP_CHANGE			11
CONST_INT	LBOOL26_PUT_BACK_INTO_STEALTH_MODE_OUTFIT_CHANGE		12
CONST_INT	LBOOL26_PLAYED_OUTFIT_CHANGE_PTFX						13
CONST_INT	LBOOL26_PARACHUTE_OVERRIDDEN_TO_TEAM_COLOUR				14
CONST_INT  	LBOOL26_FORCED_SEARCH_WANTED_AND_SEEN					15
CONST_INT	LBOOL26_CELEBRATION_SCREEN_OUTFIT_CHANGE				16
CONST_INT	LBOOL26_ENDING_HUD_UPDATED								18
CONST_INT  	LBOOL26_DISABLED_LAW_DISPATCH_FOR_FORCED_SEARCH			20
CONST_INT	LBOOL26_CELEBRATION_SCREEN_OUTFIT_CHANGE_BROADCAST		21
CONST_INT	LBOOL26_SERVUROSERV_LOSING_SIGNAL						22
CONST_INT	LBOOL26_MINIGUN_DEFENCE_MODIFIED						23
CONST_INT	LBOOL26_OVERRIDING_SEAT_PREF_SPECIAL_VEH				24
CONST_INT	LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE			26
CONST_INT	LBOOL26_DOWNLOAD_PAUSE_STARTED							28
CONST_INT	LBOOL26_DUMMY_BLIP_OVERRIDE								29
CONST_INT	LBOOL26_BEEN_UNDERWATER									30
CONST_INT	LBOOL26_STARTED_ANIMATION_FOR_BEAM_HACK					31


INT iLocalBoolCheck26

CONST_INT	LBOOL27_BAG_REMOVED_FOR_BACK_GEAR						0
CONST_INT	LBOOL27_WINNER_LOSER_SHARD_RENDERPHASE_INITIAL_PAUSE	1
CONST_INT	LBOOL27_IN_CREATOR_TRAILER_OLD_VALUE					2
CONST_INT	LBOOL27_JUST_REACHED_QUICK_RESTART_POINT_1				3
CONST_INT	LBOOL27_JUST_REACHED_QUICK_RESTART_POINT_2				4
CONST_INT	LBOOL27_JUST_REACHED_QUICK_RESTART_POINT_3				5
CONST_INT	LBOOL27_JUST_REACHED_QUICK_RESTART_POINT_4				6
CONST_INT	LBOOL27_STROMBERG_TRANSFORM_SWEETENED					7
CONST_INT	LBOOL27_STORED_NIGHTVISION_STATE						8
CONST_INT	LBOOL27_SCRIPTED_CUTSCENE_LOCALLY_HIDE_PLAYERS			9
CONST_INT	LBOOL27_SCRIPTED_CUTSCENE_SYNCSCENE_STARTED				10
CONST_INT	LBOOL27_FLYING_TOO_HIGH									11
CONST_INT	LBOOL27_WAS_HARD_TARGET									12
CONST_INT	LBOOL27_BOMB_BAY_HELP_TEXT_PLAYED						13
CONST_INT	LBOOL27_WEAPON_HAS_BEEN_HIDDEN_BY_INTERACTION			14
CONST_INT	LBOOL27_BOMB_BAY_HELP_TEXT_PLAYED_OPEN					15
CONST_INT	LBOOL27_DELUXO_SOUND_UNLOCK_HOVER_PLAYED				16
CONST_INT	LBOOL27_DELUXO_SOUND_UNLOCK_FLIGHT_PLAYED				17
CONST_INT	LBOOL27_HAVE_HIDDEN_DUMMY_BLIPS							18
CONST_INT	LBOOL27_RED_FILTER_SOUND_PLAYED							19
CONST_INT	LBOOL27_USED_A_COKE_GRAB_MINIGAME						20
CONST_INT	LBOOL27_PROCESS_BURNING_VANS							21
CONST_INT	LBOOL27_INTERROGATION_READY_FOR_OBJECTIVE_TEXT			22
CONST_INT	LBOOL27_CACHED_LOCAL_PLAYER_HEALTH_VALUES				23
CONST_INT	LBOOL27_PLAYER_WEAPON_DAMAGE_MODIFIER_APPLIED			24
CONST_INT	LBOOL27_PLAYER_TAKEDOWN_DEFENSE_MODIFIER_APPLIED		25
CONST_INT	LBOOL27_CAN_PROCESS_CACHED_VEH_HEALING_EVENTS			26
CONST_INT	LBOOL27_PLAYER_IN_RANGE_OF_THE_CACHED_VEHICLE			27
CONST_INT	LBOOL27_CACHED_VEHICLE_RANGE_CHECK_SKIP_BLIP_COLOUR	    28
CONST_INT	LBOOL27_DISPLAYING_RESURRECTION_SHARD					29
CONST_INT	LBOOL27_DISPLAY_RESURRECTION_SHARD_FRIENDLY_TEAM		30
CONST_INT	LBOOL27_DISPLAY_RESURRECTION_SHARD_ENEMY_TEAM			31
INT iLocalBoolCheck27

CONST_INT LBOOL28_DISABLE_CASINO_PEDS_DURING_CUTSCENE				0
CONST_INT LBOOL28_CLEARED_PERSONAL_VEHICLE_INITIAL_HEIST2_NODES		1
CONST_INT LBOOL28_STARTED_DOWNLOAD_SND								2
CONST_INT LBOOL28_EXPEDITE_DIALOGUE_TRIGGERS_ENABLED				3
CONST_INT LBOOL28_PRELOAD_OUTFIT_CHANGE_AT_END_OF_CUTSCENE			4
CONST_INT LBOOL28_PREV_AVENGER_PILOT_STATUS							5
CONST_INT LBOOL28_USING_PROP_CLEANUP_FUNCTIONALITY					6
CONST_INT LBOOL28_SIREN_ACTIVE_MANUAL_RESPAWN						7
CONST_INT LBOOL28_FORCED_MANUAL_RESPAWN								8
CONST_INT LBOOL28_TRAP_DOOR_LEAVE_AREA_SHARD						9
CONST_INT LBOOL28_LAST_TIMER_RESET_FOR_SD							10
CONST_INT LBOOL28_USING_PV											11
CONST_INT LBOOL28_ALPHA_RESET_DONE									12
CONST_INT LBOOL28_PLAYED_INTRO_CUTSCENE_RULE						13
CONST_INT LBOOL28_MANUALLY_HIDDEN_HUD								14
CONST_INT LBOOL28_STARTING_SPECTATOR_IN_TOP_DOWN					15
CONST_INT LBOOL28_CAN_TEAMS_ACTIVATE_OTHER_TEAMS_RESPAWN_POINTS		16
CONST_INT LBOOL28_OTHER_TEAM_ACTIVATED_MY_RESPAWN_POINTS			17
CONST_INT LBBOOL28_HAS_DONE_SUDDEN_DEATH_BOUNDS_CHECK				18
CONST_INT LBBOOL28_HIDE_RADAR_THIS_FRAME							19
CONST_INT LBBOOL28_LADDERS_DISABLED_BY_RULE_OPTION					20
CONST_INT LBOOL28_HAS_INTRO_CUTSCENE_RULE							21
CONST_INT LBOOL28_FOUND_LOBBY_LEADER								22
CONST_INT LBOOL28_DISABLE_LOCAL_PLAYER_FIRE_PROOF					23
CONST_INT LBOOL28_SET_UP_ON_RULE_TEAM_REL_GROUP_TEAM_0				24
CONST_INT LBOOL28_SET_UP_ON_RULE_TEAM_REL_GROUP_TEAM_1				25
CONST_INT LBOOL28_SET_UP_ON_RULE_TEAM_REL_GROUP_TEAM_2				26
CONST_INT LBOOL28_SET_UP_ON_RULE_TEAM_REL_GROUP_TEAM_3				27

INT iLocalBoolCheck28

CONST_INT LBOOL29_SAVING_FALLOUT_FLAG									0
CONST_INT LBOOL29_PLAYED_RUNNING_BACK_FLASH_SFX							1
CONST_INT LBOOL29_MISSION_INTRO_SCENE_READY_FOR_JOB_TEXT				2
CONST_INT LBOOL29_SHOULD_TURN_RADIO_BACK_ON_AFTER_ROUND_RESTART			3
CONST_INT LBOOL29_DONE_STUNTING_PACK_ENDING_FIREWORKS					4
CONST_INT LBOOL29_REMOTE_DETONATE_VEHICLE_CHECKED_THIS_RULE				5
CONST_INT LBOOL29_MARKED_FOR_SHUNT_OBJ_DROP								6
CONST_INT LBOOL29_ARENA_VEHICLE_WEAPON_SET								7
CONST_INT LBOOL29_POST_SHARD_SCORE_UPDATE								8
CONST_INT LBOOL29_PROCESSED_SCRIPTED_CUTSCENE_LATE_WARP					9
CONST_INT LBOOL29_FADE_OUT_DUE_TO_WARP_PORTAL_USED_BY_REMOTE_PLAYER		10
CONST_INT LBOOL29_DO_DELIVER_FX											11
CONST_INT LBOOL29_ARENA_SPAWN_FIX_HAS_BEEN_DONE							12
CONST_INT LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE		13
CONST_INT LBOOL29_BOUNDS_OOB_BLIP_HAS_FORCED_GPS						14
CONST_INT LBOOL29_MODSHOPS_AVAILABLE_FOR_REPAIR							15
CONST_INT LBOOL29_REMOTE_DETONATE_VEHICLE_IN_THIS_MISSION				16
CONST_INT LBOOL29_CASINO_BLIP_DISPLACEMENT_DONE							17
CONST_INT LBOOL29_USING_MODEL_SWAP_AND_CUTSCENE_REQUIRES_ORIGINAL_PED	18
CONST_INT LBOOL29_REFRESH_INSTANT_DIALOGUE_LOOP							19
CONST_INT LBOOL29_MUSIC_LOCKER_MUSIC_STARTED							20
CONST_INT LBOOL29_KEEP_TASKS_AFTER_CUTSCENE								21
CONST_INT LBOOL29_MusicLockerLightsCreated								22
CONST_INT LBOOL29_DreScriptedVoiceLinePlayed							23
CONST_INT LBOOL29_MissionTrackStartedRestart							24
CONST_INT LBOOL29_HalloweenAdversary2022ProximitySoundsBroadcasted		25
CONST_INT LBOOL29_HalloweenAdversary2022ResetProximitySounds			26
CONST_INT LBOOL29_HalloweenAdversary2022ThermalHelpTextDisplayed		27
CONST_INT LBOOL29_LIGHTNING_STRIKE_BLIPPING_ENABLED_FOR_THE_ENEMY_TEAM	28
CONST_INT LBOOL29_WARP_PORTALS_ARE_USING_TRANSFORMS_CHECKED				29
CONST_INT LBOOL29_WARP_PORTALS_ARE_USING_TRANSFORMS						30
CONST_INT LBOOL29_RULE_OUTFIT_CHANGE_REQUEST							31

INT iLocalBoolCheck29

INT iLocalBoolCheck30
CONST_INT LBOOL30_HIDDEN_RESPAWN_BAR										0
CONST_INT LBOOL30_CONTESTANT_TURRET_BLOCKED									1
CONST_INT LBOOL30_SETUP_NEW_INTRO_COORDS									2
CONST_INT LBOOL30_PLAYED_ARENA_WARS_AUDIO_TRACK								3
CONST_INT LBOOL30_SHOULD_RESET_VEHICLE_MINES_BLOCK_1						4
CONST_INT LBOOL30_SPEC_SPEC_FAKE_MINIROUND_TRANSITION_ACTIVATED				5
CONST_INT LBOOL30_RETURNING_A_FLAG											6
CONST_INT LBOOL30_OBJECTIVE_HAS_BEEN_REPAIR_CAR								7
CONST_INT LBOOL30_BMBFB_BALLS_HAVE_BEEN_UNEVEN								8
CONST_INT LBOOL30_PROCESSED_LEAVE_VEHICLE_FOR_CUTSCENE						9
CONST_INT LBOOL30_OBJECTIVE_BLOCKER_REQUESTED_THIS_FRAME					10
CONST_INT LBOOL30_CUTSCENE_PLAYED_CLOTHING_ANIM								11
CONST_INT LBOOL30_REQUESTED_MIGRATION_RIGHTS_RELEASED_FROM_HOST_FOR_VEH 	12
CONST_INT LBOOL30_SHOW_CUSTOM_WAIT_TEXT										13
CONST_INT LBOOL30_SET_PLAYER_INTO_COVER_START_POSITION						14
CONST_INT LBOOL30_HAS_BEEN_SET_INTO_COVER_START_POSITION					15
CONST_INT LBOOL30_HAS_CORRECTED_COVER_START_CAMERA_HEADING					16
CONST_INT LBOOL30_HAS_CORRECTED_COVER_START_PLAYER_HEADING					17
CONST_INT LBOOL30_DISPATCH_BLOCKED_BY_ZONE									18
CONST_INT LBOOL30_REAPPLY_DISPATCH_SERVICES_ON_ZONE_EXIT					19
CONST_INT LBOOL30_PERFORMING_INTERACT_WITH_CUTSCENE_LEAD_IN					20
CONST_INT LBOOL30_FORCED_HINT_CAM_TRIGGERED									21
CONST_INT LBOOL30_FORCED_HINT_CAM_ACTIVE									22
CONST_INT LBOOL30_WARPED_FROM_EXIT_STATE									23
CONST_INT LBOOL30_MOCAP_FOCUS_ENTITY_SET									24
CONST_INT LBOOL30_PRIORITY_VEHICLE_IS_AVENGER								25
CONST_INT LBOOL30_MANUAL_RESPAWN_USING_TEAM_VEHICLE							26
CONST_INT LBOOL30_CAN_ANY_TEAM_RESPAWN_FROM_SPECTATE						27
CONST_INT LBOOL30_FLAME_EFFECT_SPAWNED										28
CONST_INT LBOOL30_BLOCK_WAIT_IN_SPECTATE_UNTIL_PLAYER_RESPAWNS				29
CONST_INT LBOOL30_WAIT_IN_SPECTATE_ENABLED									30
CONST_INT LBOOL30_OK_TO_START_ACCELERATED_FILTER_TRANSITION_OUT				31

INT iLocalBoolCheck31
CONST_INT LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH		0
CONST_INT LBOOL31_PRIZED_VEHICLE_FROZEN								1
CONST_INT LBOOL31_USED_AIR_DEFENCE_PTFX								2
CONST_INT LBOOL31_LOCK_CASINO_DOORS									3
CONST_INT LBOOL31_PRIZED_VEHICLE_HIDDEN								4
CONST_INT LBOOL31_AGGRO_HELPTEXT_PLAYED								5
CONST_INT LBOOL31_END_CUTSCENE_STARTED								6
CONST_INT LBOOL31_USING_NEAREST_PLACED_VEHICLE_WARP					7
CONST_INT LBOOL31_CASINO_MISSION_CONTROLLER_BLIPS_HIDDEN			8
CONST_INT LBOOL31_HAS_TRIGGERED_ALL_CCTV_CAMERAS					9
CONST_INT LBOOL31_APPLIED_INTRO_GAIT								10
CONST_INT LBOOL31_PLAYER_RESURRECTED_FOR_END_CUTSCENE				11
CONST_INT LBOOL31_PRIZED_VEHICLE_SHOULD_HIDE						12
CONST_INT LBOOL31_NOT_STREAMING_CUTSCENE							13
CONST_INT LBOOL31_CANCEL_LOAD_SCENE_FOR_END_CUTSCENE_WARP			14
CONST_INT LBOOL31_STARTED_LOAD_SCENE_FOR_SCRIPT_INIT				15
CONST_INT LBOOL31_FINISHED_LOAD_SCENE_FOR_SCRIPT_INIT				16
CONST_INT LBOOL31_ENDED_SKY_CAM_FOR_LOAD_SCENE_FOR_SCRIPT_INIT		17
CONST_INT LBOOL31_SHOULD_USE_SKY_CAM_FADE_LOAD_SCENE				18
CONST_INT LBOOL31_REQUEST_CASINO_INTERIOR_SCRIPT					19
CONST_INT LBOOL31_CLEARED_INTERIOR_FLAG_FOR_ENTITY					20
CONST_INT LBOOL31_CARGOBOB_CARRYING_OBJ_VEHICLE_TRIGGERED			21
CONST_INT LBOOL31_SOMEONE_IS_RAPPELLING								22
CONST_INT LBOOL31_PHONE_EMP_IS_ACTIVE								23
CONST_INT LBOOL31_PLACED_PEDS_PERFORMED_ROLLING_START				24
CONST_INT LBOOL31_TURNED_ON_LIGHTS_OFF_TC_MODIFIER					25
CONST_INT LBOOL31_USING_SPAWN_AREA_ZONE_FOR_RESPAWN_HEADING			26
CONST_INT LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK			27
CONST_INT LBOOL31_INITIALISED_MOC_INTERIOR_MAP						28
CONST_INT LBOOL31_APPLIED_CUSTOM_ZONE_WALK_STYLE					29
CONST_INT LBOOL31_ANY_PED_DESTROYED_EVENT_RECEIVED_FOR_DIALOGUE		30
CONST_INT LBOOL31_CUTSCENE_RE_REQUESTED_AFTER_GAMEPLAY  			31

INT iLocalBoolCheck32
CONST_INT LBOOL32_HAS_CUT_PAINTING_ANIMATION_STARTED				0
CONST_INT LBOOL32_FORCED_MISSION_START_MOVING_FORWARD				1
CONST_INT LBOOL32_NO_NEED_FOR_DIALOGUE_EVERY_FRAME_PROCESSING		2
CONST_INT LBOOL32_SENT_BAG_FULL_TICKER								3
CONST_INT LBOOL32_HAS_CUT_PAINTING_GOT_LOCAL_COPY					4
CONST_INT LBOOL32_SET_STUN_GUN_AS_LIMITED							5
CONST_INT LBOOL32_LIMITED_STUN_GUN_FULLY_INITIALISED				6
CONST_INT LBOOL32_MINIGAME_HELP_TEXT_SHOWING_THIS_FRAME				7
CONST_INT LBOOL32_TURN_OFF_VISUAL_AIDS_ASAP							8
CONST_INT LBOOL32_DONE_FIRST_COUGH									9
CONST_INT LBOOL32_STARTED_FINGERPRINT_CLONE							10
CONST_INT LBOOL32_STARTED_ORDER_UNLOCK								11
CONST_INT LBOOL32_CUT_PAINTING_IS_CUT_ANIMATION_IN_PROGRESS			12
CONST_INT LBOOL32_CUT_PAINTING_IS_IDLE_ANIMATION_IN_PROGRESS		13
CONST_INT LBOOL32_AIR_DEFENCE_SHOWN_SHARD							14
CONST_INT LBOOL32_UNLOCKED_DOOR_WITH_SWIPECARD						15
CONST_INT LBOOL32_FINGERPRINT_HACK_ANIM_USE_ALT						16
CONST_INT LBOOL32_USING_DRONE										17
CONST_INT LBOOL32_COMPLETED_CASINO_VAULT_DOOR_BOMB_PLANT			18
CONST_INT LBOOL32_VAULT_DRILL_ASSET_LOADED							19
CONST_INT LBOOL32_SHOULD_USE_CUSTOM_HELP_WAIT_TEXT_TAXI				20 
CONST_INT LBOOL32_CASINO_HEIST_MISSION_CONTROLLER_BLIPS_HIDDEN		21 
CONST_INT LBOOL32_SCRIPTED_CUTSCENE_PLAYING_ANIM					22
CONST_INT LBOOL32_INITIALISED_DOORS									23
CONST_INT LBOOL32_IN_INVERSE_LEAVE_AREA_LOCATION					24
CONST_INT LBOOL32_SCRIPTED_CUTSCENE_PLAYING_ANIM_WARM_UP			25
CONST_INT LBOOL32_TUNER_INTRO_SCENE_REV_1							26
CONST_INT LBOOL32_TUNER_INTRO_SCENE_REV_2							27
CONST_INT LBOOL32_LOCKED_MIGRATION_FOR_VEHICLE_IN_VEHICLE			28
CONST_INT LBOOL32_APARTMENT_RADIO_MUTE								29
CONST_INT LBOOL32_INTERRUPTED_INTIMIDATION_DIALOGUE_WITH_REACTION	30
CONST_INT LBOOL32_PLAYED_INTIMIDATION_NO_HIT_DIALOGUE				31

INT iLocalBoolCheck33
CONST_INT LBOOL33_PLAYED_COUGH_SOUND_EFFECT							0
CONST_INT LBOOL33_DRONE_PICKUP_SPAWNED								1
CONST_INT LBOOL33_ZOOMED_IN_FOR_CASINO_ROOF							2
CONST_INT LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST						3
CONST_INT LBOOL33_DRONE_TRANQUILISER_AMMO_ASSIGNED					4
CONST_INT LBOOL33_MELEE_PCF_IS_RULE_BLOCKED							5
CONST_INT LBOOL33_SET_PLAYER_INTO_COVER_START_POSITION_FORCE_LEFT	6
CONST_INT LBOOL33_HEAVY_ARMOUR_CHECK								7
CONST_INT LBOOL33_EMP_BLOCKING_DRONE_ACTIVE							8
CONST_INT LBOOL33_DRONE_PICKUP_COLLECTED							9
CONST_INT LBOOL33_SHOULD_CREATE_PICKUP								10
CONST_INT LBOOL33_USING_ACTION_MODE									11
CONST_INT LBOOL33_USING_ACTION_MODE_FORCE_OFF						12
CONST_INT LBOOL33_WEAPON_FORCED_EQUIPPED_THIS_RULE					13
CONST_INT LBOOL33_SET_IGNORED_WHEN_WANTED							14
CONST_INT LBOOL33_FLASHLIGHT_NEEDS_TO_BE_TURNED_ON					15
CONST_INT LBOOL33_PLAYER_DISABLED_THEIR_OWN_FLASHLIGHT				16
CONST_INT LBOOL33_PLAYER_SEEN_REBREATHER_CONTROLS_HELPTEXT			17
CONST_INT LBOOL33_SET_TIME_TO_LOSE_COPS_CLEAR_MODE					18
CONST_INT LBOOL33_SHOULD_PROCESS_END_CUTSCENE						19
CONST_INT LBOOL33_SHOULD_SHOW_AND_POPULATE_RESULTS					20
CONST_INT LBOOL33_RESULTS_POPULATED									21
CONST_INT LBOOL33_TRIGGERED_CELEBRATION_SCREEN_RENDERPHASE_PAUSE	22
CONST_INT LBOOL33_KEEP_WEAPON_HOLSTERED_UNTIL_SWITCH_OR_FIRE		23
CONST_INT LBOOL33_STARTED_MISSION_INTRO_SCENE						24
CONST_INT LBOOL33_INFINITE_PARACHUTES								25
CONST_INT LBOOL33_FINISHED_MISSION_INTRO_SCENE						26
CONST_INT LBOOL33_QUICK_RESTART_MISSION_INTRO_SCENE_PAUSE			27
CONST_INT LBOOL33_LOCAL_PLAYER_BEEN_PROPERLY_SUBMERGED_THIS_RULE	28
CONST_INT LBOOL33_DELAYED_ADD_SPAWN_POINTS							29
CONST_INT LBOOL33_SHOW_FULL_CAPACITY_HUD							30
CONST_INT LBOOL33_FINISHED_LOOT_GRAB								31

INT iLocalBoolCheck34
CONST_INT LBOOL34_LOCH_SANTOS_MONSTER_CLEANED_UP					0
CONST_INT LBOOL34_TUNNEL_WELDING_MINIGAME__STARTED					1
CONST_INT LBOOL34_TUNNEL_WELDING_MINIGAME__PASSED					2
CONST_INT LBOOL34_WAS_MISSION_LAUNCHED_THROUGH_Z_MENU				3
CONST_INT LBOOL34_CHECKED_WAS_LAUNCHED_THROUGH_Z_MENU				4
CONST_INT LBOOL34_ENTER_SAFE_COMBINATION_MINIGAME__STARTED			5
CONST_INT LBOOL34_ENTER_SAFE_COMBINATION_MINIGAME__ONGOING			6
CONST_INT LBOOL34_ENTER_SAFE_COMBINATION_MINIGAME__PASSED			7
CONST_INT LBOOL34_VOLTAGE_MINIGAME__STARTED							8
CONST_INT LBOOL34_VOLTAGE_MINIGAME__FAILED							9
CONST_INT LBOOL34_VOLTAGE_MINIGAME__PASSED							10
CONST_INT LBOOL34_GLASS_CUTTING_MINIGAME__PASSED					11
CONST_INT LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES				12
CONST_INT LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES_TRIGGERED	13
CONST_INT LBOOL34_LOCH_SANTOS_MONSTER_DIVING						14
CONST_INT LBOOL34_PLAYER_ABILITY_HELI_BACKUP_HAS_BEEN_ACTIVE		15
CONST_INT LBOOL34_PLAYING_END_CUTSCENE_MUSIC						16
CONST_INT LBOOL34_ISLAND_LIGHTS_HAVE_TURNED_OFF						17
CONST_INT LBOOL34_REMOVED_TAKE_ON_NO_LIVES_DEATH					18
CONST_INT LBOOL34_MAIN_TARGET_HELD									19
CONST_INT LBOOL34_MAP_ZOOM_CHANGED									20
CONST_INT LBOOL34_BLOCK_EMP_SOUNDS_DUE_TO_SPECIFIED_INTERIOR		21
CONST_INT LBOOL34_USING_INTERIOR_DESTRUCTION						22
CONST_INT LBOOL34_PRELOAD_BUNKER_FOR_USE_IN_CUTSCENE				23
CONST_INT LBOOL34_ALT_VARS_APPLIED									24
CONST_INT LBOOL34_INITIAL_REQUESTED_PERSONAL_VEHICLE				25
CONST_INT LBOOL34_SPAWN_GROUPS_CHANGED_REVALIDATE_SPAWN_POINTS		26
CONST_INT LBOOL34_LOCAL_PLAYER_CANCEL_CALL_ANIMATION_AFTER_DELAY	27
CONST_INT LBOOL34_SPAWN_GROUP_CLEAR_REQUEST_READY					28
CONST_INT LBOOL34_SPAWN_GROUP_NEW_RESET_REQUEST_READY				29
CONST_INT LBOOL34_REGISTER_ENTITY_CHECKPOINT_CONTINUITY_THIS_FRAME	30
CONST_INT LBOOL34_SPAWN_GROUPS_CHANGED_REVALIDATE_ZONES				31

INT iLocalBoolCheck35
CONST_INT LBOOL35_SECUROHACK_REQUEST_STOP							0
CONST_INT LBOOL35_SECUROHACK_ACTIVE									1
CONST_INT LBOOL35_SECUROHACK_LOSING_SIGNAL							2
CONST_INT LBOOL35_STARTED_REACT_TO_SMOKE_ANIM						3
CONST_INT LBOOL35_REACT_TO_SMOKE_COUGH_FIRED_THIS_EVENT				4
CONST_INT LBOOL35_STARTED_PLAYERS_WAKE_UP_ANIM						5
CONST_INT LBOOL35_CLEARED_AREA_AT_THE_END_OF_CUTSCENE				6
CONST_INT LBOOL35_FIB_OFFICE_INTERIOR_ZOOM_ENABLED					7
CONST_INT LBOOL35_STARTED_BRUTEFORCE								8
CONST_INT LBOOL35_SETUP_REACT_TO_SMOKE								9
CONST_INT LBOOL35_SETUP_PLAYERS_WAKE_UP								10
CONST_INT LBOOL35_PLAYING_PLAYERS_WAKE_UP_ANIM						11
CONST_INT LBOOL35_STARTED_ULP_BUNKER_AMBIENT_AUDIO					12
CONST_INT LBOOL35_ISLAND_HEIST_LEADER_CHEAT_CHECK_DONE				13
CONST_INT LBOOL35_LOADED_REACT_TO_SMOKE_ANIM						14
CONST_INT LBOOL35_LOADED_PLAYERS_WAKE_UP_ANIM						15
CONST_INT LBOOL35_PLAYED_CUTSCENE_SHOT_REACHED_TICKER				16
CONST_INT LBOOL35_PLAYED_LOW_BODY_HEALTH_SOUND_FX					17
CONST_INT LBOOL35_APPLIED_ROOFTOP_TIMECYCLE_MOD						18
CONST_INT LBOOL35_APPLY_LIGHTS_OFF_TC_MODIFIER_FOR_SPECTATOR		19
CONST_INT LBOOL35_FORCE_ANIM_UPDATE_FOR_CELEB_SCREEN				20
CONST_INT LBOOL35_ATTACHED_CLONES_FOR_LIFT_SCENE					21
CONST_INT LBOOL35_SET_UP_INITIAL_PED_AND_VEHICLE_DENSITIES			22
CONST_INT LBOOL35_HIDE_DUMMY_BLIPS_ON_MAP_WHILE_IN_INTERIOR			23
CONST_INT LBOOL35_SKIPPING_BLIP_CREATION_FOR_CACHED_VEHICLE			24
CONST_INT LBOOL35_USING_FMMC_CREATED_CACHED_VEHICLE					25
CONST_INT LBOOL35_USING_TOD_BASED_ON_RULE_TIME						26
CONST_INT LBOOL35_USING_TOD_BASED_ON_RULE							27
CONST_INT LBOOL35_USING_CLOWN_BLOOD_VFX								28
CONST_INT LBOOL35_USING_ALIEN_BLOOD_VFX								29
CONST_INT LBOOL35_USING_CLOWN_DEATH_AUDIO							30
CONST_INT LBOOL35_MINIMAP_ZOOMED_IN									31

INT iLocalBoolCheck36
CONST_INT LBOOL36_SECONDARY_CAMERA_SHAKE_FINISHED					0
//ADD ANY NEW iLocalBoolCheckXs TO PROCESS_BG_GLOBALS_PASS_OVER

CONST_INT ciMISSION_INTRO_TUNER_INTERP_TIME							1500
CONST_INT ciMISSION_INTRO_TUNER_OUTRO_DELAY							0
CONST_INT ciMISSION_INTRO_TUNER_OUTRO_DIFFERENCE_FOR_PLAYER_ACCEL	400

CONST_INT ciMISSION_INTRO_INTERP_TIME							1500
CONST_INT ciMISSION_INTRO_OUTRO_DELAY							0
CONST_INT ciMISSION_INTRO_OUTRO_DIFFERENCE_FOR_PLAYER_ACCEL		400

SCRIPT_TIMER tdMissionIntroInterpTimer

CONST_INT ciMISSION_INTRO_SYNC_SPEED_REV_2_CUE		4350
CONST_INT ciMISSION_INTRO_SYNC_SPEED_REV_1_CUE		2650
CONST_INT ciMISSION_INTRO_SYNC_SPEED_DELAY			2000
SCRIPT_TIMER tdMissionIntroSyncSpeedDelay
VECTOR vStartDriveToPosition
FLOAT fCachedDriveToPositionDistance

ENUM MISSION_INTRO_STAGE
	MISSION_INTRO_STAGE_INIT = 0,
	MISSION_INTRO_STAGE_ONE,
	MISSION_INTRO_STAGE_TWO,
	MISSION_INTRO_STAGE_FINISHED
ENDENUM

MISSION_INTRO_STAGE eMissionIntroStage

// As and when needed implement the other entity types to set this after their first full staggered loop.
// These should be set in POST_STAGGERED_LOOP_CLIENT.
INT iEntityFirstStaggeredLoopComplete
CONST_INT ci_ZONES_FIRST_STAGGERED_LOOP_COMPLETE				0
CONST_INT ci_BOUNDS_FIRST_STAGGERED_LOOP_COMPLETE				1

INT iWorldAlarmTypeBitset

OBJECT_INDEX oiCachedCayoPericoSpeakerToUse
CONST_FLOAT cfFindSpeakerDistance	125.0

INT iCachedHunterVisionUnusedTime = 0

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Local Reset Bitsets
// ##### Description: Local reset bitset and bits
// ##### These bitsets are cleared at the begining of each frame.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INT iLocalBoolResetCheck1
CONST_INT LBOOL_RESET1_SHOWING_VEH_HEALTH_BARS							0
CONST_INT LBOOL_RESET1_USE_CUSTOM_SPAWN_POINTS							1
CONST_INT LBOOL_RESET1_SINGLE_FRAME_COUNT_SCORES_ENABLED				2
CONST_INT LBOOL_RESET1_OBJECTIVE_TEXT_OVERRIDDEN						3
CONST_INT LBOOL_RESET1_CHECKED_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE_T0	4
CONST_INT LBOOL_RESET1_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE_T0		5
CONST_INT LBOOL_RESET1_CHECKED_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE_T1	4
CONST_INT LBOOL_RESET1_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE_T1		5
CONST_INT LBOOL_RESET1_CHECKED_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE_T2	4
CONST_INT LBOOL_RESET1_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE_T2		5
CONST_INT LBOOL_RESET1_CHECKED_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE_T3	4
CONST_INT LBOOL_RESET1_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE_T3		5
CONST_INT LBOOL_RESET1_SHOW_WANTED_DELAY_HUD_FAKE_WANTED_TIMER			6
CONST_INT LBOOL_RESET1_SHOW_WANTED_DELAY_HUD_REAL_WANTED_TIMER			7
CONST_INT LBOOL_RESET1_HUD_SHOULD_BE_HIDDEN_THIS_FRAME					8
CONST_INT LBOOL_RESET1_HUD_SHOULD_BE_HIDDEN_THIS_FRAME_CHECKED			9
CONST_INT LBOOL_RESET1_CHECK_REAL_TO_FAKE_WANTED_SWAP					10
CONST_INT LBOOL_RESET1_BLOCK_REAL_TO_FAKE_WANTED_SWAP					11
CONST_INT LBOOL_RESET1_FORCE_NO_WANTED_FLASH							12
CONST_INT LBOOL_RESET1_FULL_ZONES_AND_BOUNDS_LOOP_REQUIRED				13
CONST_INT LBOOL_RESET1_SECUROHACK_REQUEST_ACTIVATION					14
CONST_INT LBOOL_RESET1_BEAMHACK_IN_VEH_VALID							15

INT iPedRespawnIsBlockedBS[FMMC_MAX_PEDS_BITSET]
INT iPedRespawnIsBlockedThisFrameBS[FMMC_MAX_PEDS_BITSET]
INT iPedShouldRespawnNowBS[FMMC_MAX_PEDS_BITSET]
INT iPedShouldCleanupThisFrameBS[FMMC_MAX_PEDS_BITSET]
INT iPedShouldNotCleanupThisFrameBS[FMMC_MAX_PEDS_BITSET]

INT iVehRespawnIsBlockedBS
INT iVehRespawnIsBlockedThisFrameBS
INT iVehShouldRespawnNowBS
INT iVehShouldCleanupThisFrameBS
INT iVehShouldNotCleanupThisFrameBS

INT iObjRespawnIsBlockedBS
INT iObjRespawnIsBlockedThisFrameBS
INT iObjShouldRespawnNowBS
INT iObjShouldCleanupThisFrameBS
INT iObjShouldNotCleanupThisFrameBS

INT iDynoRespawnIsBlockedBS
INT iDynoRespawnIsBlockedThisFrameBS
INT iDynoShouldRespawnNowBS
INT iDynoShouldCleanupThisFrameBS
INT iDynoShouldNotCleanupThisFrameBS

INT iPropRespawnIsBlockedBS[FMMC_MAX_PROP_BITSET]
INT iPropRespawnIsBlockedThisFrameBS[FMMC_MAX_PROP_BITSET]
INT iPropShouldRespawnNowBS[FMMC_MAX_PROP_BITSET]
INT iPropShouldCleanupThisFrameBS[FMMC_MAX_PROP_BITSET]
INT iPropShouldNotCleanupThisFrameBS[FMMC_MAX_PROP_BITSET]

INT iZoneRespawnIsBlockedBS
INT iZoneRespawnIsBlockedThisFrameBS
INT iZoneShouldRespawnNowBS
INT iZoneShouldCleanupThisFrameBS
INT iZoneShouldNotCleanupThisFrameBS

INT iRespawnPointIsBlockedBS[FMMC_MAX_TEAMS][FMMC_MAX_TEAM_SPAWN_POINT_BITSET]
INT iRespawnPointIsBlockedThisFrameBS[FMMC_MAX_TEAMS][FMMC_MAX_TEAM_SPAWN_POINT_BITSET]
INT iRespawnPointShouldBeValidNowBS[FMMC_MAX_TEAMS][FMMC_MAX_TEAM_SPAWN_POINT_BITSET]

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Server Broadcast Data
// ##### Description: Server Broadcast data structs, declarations and bitset bits.
// ##### This data can be read by all participants but only the script host can write.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//server bitset consts
CONST_INT SBBOOL_TEAM_0_ACTIVE			 0
CONST_INT SBBOOL_TEAM_1_ACTIVE			 1
CONST_INT SBBOOL_TEAM_2_ACTIVE			 2
CONST_INT SBBOOL_TEAM_3_ACTIVE			 3
CONST_INT SBBOOL_SKIP_TEAM_0_UPDATE		 4
CONST_INT SBBOOL_SKIP_TEAM_1_UPDATE		 5
CONST_INT SBBOOL_SKIP_TEAM_2_UPDATE		 6
CONST_INT SBBOOL_SKIP_TEAM_3_UPDATE		 7
//FREE									 8
CONST_INT SSBOOL_TEAM0_FINISHED 9
CONST_INT SSBOOL_TEAM1_FINISHED 10
CONST_INT SSBOOL_TEAM2_FINISHED 11
CONST_INT SSBOOL_TEAM3_FINISHED 12
CONST_INT SBBOOL_SKIP_PARTICIPANT_UPDATE 13
CONST_INT SBBOOL_FIRST_UPDATE_DONE 14
CONST_INT SBBOOL_MISSION_OVER 16
CONST_INT SBBOOL_ALL_JOINED_START 17
CONST_INT SBBOOL_SET_NOT_JOINABLE 18
CONST_INT SBBOOL_EVERYONE_RUNNING 19
CONST_INT SSBOOL_TEAM0_FAILED	  20
CONST_INT SSBOOL_TEAM1_FAILED	  21
CONST_INT SSBOOL_TEAM2_FAILED	  22
CONST_INT SSBOOL_TEAM3_FAILED	  23
CONST_INT SSBOOL_ALL_READY_INTRO_CUT  24
CONST_INT SBBOOL_CUTSCENE_TEAM0_PLAYERS_SELECTED 25
CONST_INT SBBOOL_CUTSCENE_TEAM1_PLAYERS_SELECTED 26
CONST_INT SBBOOL_CUTSCENE_TEAM2_PLAYERS_SELECTED 27
CONST_INT SBBOOL_CUTSCENE_TEAM3_PLAYERS_SELECTED 28


//server team fail check bitsets
CONST_INT SBBOOL1_RECALCULATE_OBJECTIVE_LOGIC_TEAM_0	0
CONST_INT SBBOOL1_RECALCULATE_OBJECTIVE_LOGIC_TEAM_1	1
CONST_INT SBBOOL1_RECALCULATE_OBJECTIVE_LOGIC_TEAM_2	2
CONST_INT SBBOOL1_RECALCULATE_OBJECTIVE_LOGIC_TEAM_3	3
CONST_INT SBBOOL1_TEAM0_NEEDS_KILL						4
CONST_INT SBBOOL1_TEAM1_NEEDS_KILL						5
CONST_INT SBBOOL1_TEAM2_NEEDS_KILL						6
CONST_INT SBBOOL1_TEAM3_NEEDS_KILL						7
CONST_INT SBBOOL1_TEAM_0_WAS_ACTIVE						8
CONST_INT SBBOOL1_TEAM_1_WAS_ACTIVE						9
CONST_INT SBBOOL1_TEAM_2_WAS_ACTIVE						10
CONST_INT SBBOOL1_TEAM_3_WAS_ACTIVE						11
CONST_INT SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_0			13
CONST_INT SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_1			14
CONST_INT SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_2			15
CONST_INT SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_3			16
CONST_INT SBBOOL1_PROGRESS_OBJECTIVE_FOR_TEAM_0			17
CONST_INT SBBOOL1_PROGRESS_OBJECTIVE_FOR_TEAM_1			18
CONST_INT SBBOOL1_PROGRESS_OBJECTIVE_FOR_TEAM_2			19
CONST_INT SBBOOL1_PROGRESS_OBJECTIVE_FOR_TEAM_3			20
CONST_INT SBBOOL1_SUDDEN_DEATH        					21
CONST_INT SBBOOL1_ALL_PLAYERS_IN_DISGUISE				22			
CONST_INT SBBOOL1_HEIST_ANY_PARTICIPANT_INSIDE_ANY_MP_PROPERTY 23
CONST_INT SBBOOL1_CREATED_SCRIPTED_CUTSCENE_SYNC_SCENE	24
// FREE FREE FREE FREE FREE FREE FREE FREE FREE 		25
// FREE FREE FREE FREE FREE FREE FREE FREE FREE 		26
// FREE FREE FREE FREE FREE FREE FREE FREE FREE 		27
CONST_INT SBBOOL1_AGGRO_WILL_FAIL_FOR_TEAM_0			28//If set, the bits above mean it'll fail soon
CONST_INT SBBOOL1_AGGRO_WILL_FAIL_FOR_TEAM_1			29
CONST_INT SBBOOL1_AGGRO_WILL_FAIL_FOR_TEAM_2			30
CONST_INT SBBOOL1_AGGRO_WILL_FAIL_FOR_TEAM_3			31

CONST_INT SBBOOL2_AGGRO_FAIL_DUE_TO_BODY_FOUND_TEAM_0	0
CONST_INT SBBOOL2_AGGRO_FAIL_DUE_TO_BODY_FOUND_TEAM_1	1
CONST_INT SBBOOL2_AGGRO_FAIL_DUE_TO_BODY_FOUND_TEAM_2	2
CONST_INT SBBOOL2_AGGRO_FAIL_DUE_TO_BODY_FOUND_TEAM_3	3
CONST_INT SBBOOL2_FIRST_UPDATE_STARTED       			4
CONST_INT SBBOOL2_NEW_PARTICIPANT_LOOP_STARTED       	5
CONST_INT SBBOOL2_NEW_PARTICIPANT_LOOP_FINISHED      	6
CONST_INT SBBOOL2_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC 	7
CONST_INT SBBOOL2_TEAM_1_NEEDS_TO_WAIT_FOR_LEAVE_LOC 	8
CONST_INT SBBOOL2_TEAM_2_NEEDS_TO_WAIT_FOR_LEAVE_LOC 	9
CONST_INT SBBOOL2_TEAM_3_NEEDS_TO_WAIT_FOR_LEAVE_LOC 	10
CONST_INT SBBOOL2_ROUNDS_PAUSE_LBD_SORT					12
CONST_INT SBBOOL2_ROUNDS_LBD_GO							13		
CONST_INT SBBOOL2_ROUNDS_LBD_DO							14
CONST_INT SBBOOL2_LOCH_SANTOS_MONSTER_SPOTTED			15
// FREE FREE FREE FREE FREE FREE FREE FREE FREE FREE FREE FREE FREE FREE 	16
CONST_INT SBBOOL2_HEIST_DRILL_ASSET_REQUEST 			17
// FREE FREE FREE FREE FREE FREE FREE FREE FREE FREE FREE FREE FREE FREE 	19
// FREE FREE FREE FREE FREE FREE FREE FREE FREE FREE FREE FREE FREE FREE 	20
CONST_INT SBBOOL2_SERVER_INITIALISATION_TIMED_OUT		21
CONST_INT SBBOOL2_TEAM_0_IS_HACKING						22
CONST_INT SBBOOL2_TEAM_1_IS_HACKING						23
CONST_INT SBBOOL2_TEAM_2_IS_HACKING						24
CONST_INT SBBOOL2_TEAM_3_IS_HACKING						25
CONST_INT SBBOOL2_MISSION_ENDING_CRITICAL_PLAYER_LEFT	26
CONST_INT SBBOOL2_STARTED_COUNTDOWN						27
CONST_INT SBBOOL2_FINISHED_COUNTDOWN					28
CONST_INT SBBOOL2_SUDDEN_DEATH_MOREINLOC				29
CONST_INT SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES			30
CONST_INT SBBOOL2_MISSION_ENDING_BECAUSE_PLAYERS_LEFT	31

CONST_INT SBBOOL3_RSGDATA_INITIALISED						0
CONST_INT SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN				1
CONST_INT SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES				2
CONST_INT SBBOOL3_SUDDEN_DEATH_IN_AND_OUT					3
CONST_INT SBBOOL3_TOO_FEW_PLAYERS_FOR_NEXT_ROUND			4
CONST_INT SBBOOL3_SUDDEN_DEATH_POWER_PLAY					9
CONST_INT SBBOOL3_SUDDEN_DEATH_TARGET						10
CONST_INT SBBOOL3_MULTIRULE_TIMER_ENDED_GAME				11
CONST_INT SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD					12
CONST_INT SBBOOL3_LBD_DISPLAYED								13
CONST_INT SBBOOL3_LBD_STOP									14
CONST_INT SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN		15
//FREE	 											    	16
//FREE	 											    	17
//FREE	 											    	18
//FREE	 											    	19
//FREE 														20
CONST_INT SBBOOL3_MINIGAME_OBJ_TARGET_BEING_INVESTIGATED    21
CONST_INT SBBOOL3_MINIGAME_TARGET_FOUND						26
CONST_INT SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE			27
CONST_INT SBBOOL3_RESPAWN_ALL_TEAM_MEMBERS_TEAM_0			28
CONST_INT SBBOOL3_RESPAWN_ALL_TEAM_MEMBERS_TEAM_1			29
CONST_INT SBBOOL3_RESPAWN_ALL_TEAM_MEMBERS_TEAM_2			30	
CONST_INT SBBOOL3_RESPAWN_ALL_TEAM_MEMBERS_TEAM_3			31

CONST_INT SBBOOL4_ALL_PLAYERS_IN_ANY_LOCATE_PASSED			30
CONST_INT SBBOOL4_SUDDEN_DEATH_JUGGERNAUT					31

CONST_INT SBBOOL5_RETRY_CURRENT_RULE_TEAM_0					4
CONST_INT SBBOOL5_RETRY_CURRENT_RULE_TEAM_1					5
CONST_INT SBBOOL5_RETRY_CURRENT_RULE_TEAM_2					6
CONST_INT SBBOOL5_RETRY_CURRENT_RULE_TEAM_3					7
CONST_INT SBBOOL5_ALL_RESPAWN_RULE_PEDS_READY				12
CONST_INT SBBOOL5_ALL_RESPAWN_RULE_VEHS_READY				13
CONST_INT SBBOOL5_ALL_RESPAWN_RULE_OBJS_READY				14
CONST_INT SBBOOL5_ALL_RESPAWN_RULE_DPROPS_READY				15
CONST_INT SBBOOL5_ENTITIES_CAN_RESPAWN_ON_RULE_CHANGE		16

CONST_INT SBBOOL6_CONTROL_TIME_STOPPED_T0					0
CONST_INT SBBOOL6_CONTROL_TIME_STOPPED_T1					1
CONST_INT SBBOOL6_CONTROL_TIME_STOPPED_T2					2
CONST_INT SBBOOL6_CONTROL_TIME_STOPPED_T3					3
CONST_INT SBBOOL6_USING_SYNCED_SCORE_TIMER					20
CONST_INT SBBOOL6_ALLOW_GANGCHASE_AFTER_SPEED_FAIL			21
CONST_INT SBBOOL6_UPDATED_WEATHER_AND_TIME_FROM_CORONA		27
CONST_INT SBBOOL6_TIME_DEDUCTED_INTO_30S					30
	
CONST_INT SBBOOL7_SET_ALL_PLACED_PEDS_INTO_COVER					0
CONST_INT SBBOOL7_BLOCK_RULE_TIMER_START_DUE_TO_SCORE_FOR_TEAM_0	5
CONST_INT SBBOOL7_BLOCK_RULE_TIMER_START_DUE_TO_SCORE_FOR_TEAM_1	6
CONST_INT SBBOOL7_BLOCK_RULE_TIMER_START_DUE_TO_SCORE_FOR_TEAM_2	7
CONST_INT SBBOOL7_BLOCK_RULE_TIMER_START_DUE_TO_SCORE_FOR_TEAM_3	8
CONST_INT SBBOOL7_BLOCK_MULTI_TIMER_START_DUE_TO_SCORE_FOR_TEAM_0	9
CONST_INT SBBOOL7_BLOCK_MULTI_TIMER_START_DUE_TO_SCORE_FOR_TEAM_1	10
CONST_INT SBBOOL7_BLOCK_MULTI_TIMER_START_DUE_TO_SCORE_FOR_TEAM_2	11
CONST_INT SBBOOL7_BLOCK_MULTI_TIMER_START_DUE_TO_SCORE_FOR_TEAM_3	12
CONST_INT SBBOOL7_MULTI_RULE_TIMER_ROUND_RESTART_ENDED				15
CONST_INT SBBOOL7_USING_LIMITED_PLAYERS_IN_BOUNDS					16
CONST_INT SBBOOL7_MULTIRULE_TIMER_REINIT							27	//FMMC2020 - Never set but flagged as being reusable
CONST_INT SBBOOL7_YACHT_CUTSCENE_RUNNING							28
CONST_INT SBBOOL7_YACHT_MOVED_TO_DESTINATION						29
CONST_INT SBBOOL7_CCTV_TEAM_AGGROED_PEDS							31

CONST_INT SBBOOL8_ALLOW_RAPPELLING_FROM_HELICOPTERS					0
CONST_INT SBBOOL8_COP_DECOY_STARTED									1
CONST_INT SBBOOL8_COP_DECOY_EXPIRED									2
CONST_INT SBBOOL8_COP_DECOY_FLEEING									3
CONST_INT SBBOOL8_COP_DECOY_DEDUCT_2_STAR							4
CONST_INT SBBOOL8_HOST_MISSION_VARIATION_DATA_APPLIED				5
CONST_INT SBBOOL8_A_PED_HAS_BEEN_STUNNED							6
CONST_INT SBBOOL8_A_PED_HAS_BEEN_TRANQUILIZED						7
CONST_INT SBBOOL8_VAULT_DRILL_ASSET_REQUEST 						8
CONST_INT SBBOOL8_VAULT_DRILL_ASSET_REQUEST_LASER					9
CONST_INT SBBOOL8_APPLIED_SCRIPT_CLOUD_CONTENT_FIXES				10
CONST_INT SBBOOL8_FORCE_RAPPELLING_FROM_HELICOPTERS					11
CONST_INT SBBOOL8_FINISHED_INTRO_CUTSCENE							12
CONST_INT SBBOOL8_HOST_MISSION_VARIATION_DATA_PRE_APPLIED			13
CONST_INT SBBOOL8_PREVENT_CUTSCENE_PRIMARY_PLAYER_FROM_BEING_ADDED	14
CONST_INT SBBOOL8_SKIP_CUTSCENE_TRIGGERED						15
CONST_INT SBBOOL8_ZONE_TRIGGERED_AGGRO_INDEX_PREREQS_CHECK			16
CONST_INT SBBOOL8_RULE_CHANGE_TRIGGERED_AGGRO_INDEX_PREREQS_CHECK	17
CONST_INT SBBOOL8_PREREQ_CHANGE_TRIGGERED_AGGRO_INDEX_PREREQS_CHECK	18
CONST_INT SBBOOL8_A_PED_HAS_BEEN_EMPED								19

CONST_INT SBBOOL_HACK_FIND_PLAYER			0
CONST_INT SBBOOL_HACK_FAIL_ON_FAIL			2
CONST_INT SBBOOL_HACK_DO_HARD				3
CONST_INT SBBOOL_HACK_PASSES_RULE			5

#IF IS_DEBUG_BUILD
CONST_INT SBDEBUG_DONTFAILWHENOUTOFLIVES	0
#ENDIF

ENUM ePropClaimingState
	ePropClaimingState_Unclaimed			= 0,
	ePropClaimingState_Claimed_Team_0 		= 1,
	ePropClaimingState_Claimed_Team_1 		= 2,
	ePropClaimingState_Claimed_Team_2 		= 3,
	ePropClaimingState_Claimed_Team_3 		= 4,
	ePropClaimingState_Contested			= 5,
	ePropClaimingState_Disabled				= 6
ENDENUM

CONST_INT ciTEAM0_CUTSCENE_SEEN_BY_TEAM0 			0
CONST_INT ciTEAM0_CUTSCENE_SEEN_BY_TEAM1 			1
CONST_INT ciTEAM0_CUTSCENE_SEEN_BY_TEAM2 			2
CONST_INT ciTEAM0_CUTSCENE_SEEN_BY_TEAM3 			3
 
CONST_INT ciTEAM1_CUTSCENE_SEEN_BY_TEAM0 			4
CONST_INT ciTEAM1_CUTSCENE_SEEN_BY_TEAM1 			5
CONST_INT ciTEAM1_CUTSCENE_SEEN_BY_TEAM2 			6
CONST_INT ciTEAM1_CUTSCENE_SEEN_BY_TEAM3 			7

CONST_INT ciTEAM2_CUTSCENE_SEEN_BY_TEAM0 			8
CONST_INT ciTEAM2_CUTSCENE_SEEN_BY_TEAM1 			9
CONST_INT ciTEAM2_CUTSCENE_SEEN_BY_TEAM2 			10
CONST_INT ciTEAM2_CUTSCENE_SEEN_BY_TEAM3 			11
								  
CONST_INT ciTEAM3_CUTSCENE_SEEN_BY_TEAM0 			12
CONST_INT ciTEAM3_CUTSCENE_SEEN_BY_TEAM1 			13
CONST_INT ciTEAM3_CUTSCENE_SEEN_BY_TEAM2 			14
CONST_INT ciTEAM3_CUTSCENE_SEEN_BY_TEAM3 			15

//-------------------------------------------------------

CONST_INT SB_CHECKPOINT_STRAND_1					0
CONST_INT SB_CHECKPOINT_1							1
CONST_INT SB_CHECKPOINT_2							2
CONST_INT SB_CHECKPOINT_3							3
CONST_INT SB_CHECKPOINT_4							4
CONST_INT SB_CP1_MRT_1								5
CONST_INT SB_CP1_MRT_2								6
CONST_INT SB_CP1_MRT_3								7
CONST_INT SB_CP1_MRT_4								8
CONST_INT SB_CP2_MRT_1								9
CONST_INT SB_CP2_MRT_2								10
CONST_INT SB_CP2_MRT_3								11
CONST_INT SB_CP2_MRT_4								12
CONST_INT SB_CP3_MRT_1								13
CONST_INT SB_CP3_MRT_2								14
CONST_INT SB_CP3_MRT_3								15
CONST_INT SB_CP3_MRT_4								16
CONST_INT SB_CP4_MRT_1								17
CONST_INT SB_CP4_MRT_2								18
CONST_INT SB_CP4_MRT_3								19
CONST_INT SB_CP4_MRT_4								20
CONST_INT SB_CP_QUICK_RESTART_MRT_FOR_TEAM_0		21
CONST_INT SB_CP_QUICK_RESTART_MRT_FOR_TEAM_1		22
CONST_INT SB_CP_QUICK_RESTART_MRT_FOR_TEAM_2		23
CONST_INT SB_CP_QUICK_RESTART_MRT_FOR_TEAM_3		24

CONST_INT ciFMMCSpawnAreaTimeout	10000

CONST_INT ciMAX_BURNING_VEHICLE_HEALTH 50000

STRUCT SERVER_CASH_GRAB_DATA
	INT iCashGrabTotal[FMMC_MAX_NUM_INTERACTABLES]
	INT iCashGrabBasePilesGrabbed[FMMC_MAX_NUM_INTERACTABLES]
	INT iCashGrabBonusPilesGrabbed[FMMC_MAX_NUM_INTERACTABLES]
	INT iLeaverCashRemovedBS
ENDSTRUCT

STRUCT SERVER_FLASH_FADE_ENTITY_DATA
	NETWORK_INDEX niEntitiesDoingFlashFade[MAX_FLASH_FADE_ENTITIES]
	INT iFlashFadeEndAlpha[MAX_FLASH_FADE_ENTITIES]
	INT iEntityDoingFlashFadeBS = 0
	INT iPlayersDoingFlashFadeBS[MAX_FLASH_FADE_ENTITIES]
	INT iNumOfEntsUsingFlashFade = 0
	INT iFlashFadePartEntIndex[MAX_NUM_MC_PLAYERS]
ENDSTRUCT

// Much Less data usage than simply arraying a "Warp Index" for each entity type by that type's max array placeable value.
STRUCT WARP_ON_DAMAGE_TRACKER_DATA	
	INT iWarpFromDamageTracker_EntityType[ciWARP_FROM_DAMAGE_TRACKER_MAX]
	INT iWarpFromDamageTracker_EntityIndex[ciWARP_FROM_DAMAGE_TRACKER_MAX]
	INT iWarpFromDamageTracker_WarpIndex[ciWARP_FROM_DAMAGE_TRACKER_MAX]
ENDSTRUCT

STRUCT MC_ServerBroadcastData
	INT iServerGameState
	INT iServerBitSet
	INT iServerBitSet1
	INT iServerBitSet2
	INT iServerBitSet3
	INT iServerBitSet4
	INT iServerBitSet5
	INT iServerBitSet6
	INT iServerBitSet7
	INT iServerBitSet8
	
	INT iServerObjectBeingInvestigatedBitSet
	
	INT iServerPassBitSet[FMMC_MAX_TEAMS]
	INT iServerAggroBS[FMMC_MAX_TEAMS]
	INT iServerCCTVAlertedPlayerBS[FMMC_MAX_TEAMS]
	INT iServerCCTVAlertedPedBodyBS[FMMC_MAX_TEAMS]
	INT iServerCCTVTriggeredByPedsBS[FMMC_MAX_TEAMS]
	
	INT iNumPlayerRuleHighestPriority[FMMC_MAX_TEAMS]
	INT iNumLocHighestPriority[FMMC_MAX_TEAMS]
	INT iNumPedHighestPriority[FMMC_MAX_TEAMS]
	INT iNumDeadPedHighestPriority[FMMC_MAX_TEAMS] //Very minimal usage - FMMC2020
	INT iNumVehHighestPriority[FMMC_MAX_TEAMS]
	INT iNumVehSeatsHighestPriority[FMMC_MAX_TEAMS] //Added to but never used? - FMMC2020
	INT iNumObjHighestPriority[FMMC_MAX_TEAMS]
	INT iCurrentPlayerRule[FMMC_MAX_TEAMS]
	
	INT iMaxNumPriorityEntitiesThisRule[FMMC_MAX_TEAMS]
	
	INT iRequiredDeliveries[FMMC_MAX_TEAMS]
	
	INT iNumPedHighestPriorityHeld[FMMC_MAX_TEAMS]
	INT iNumVehHighestPriorityHeld[FMMC_MAX_TEAMS]
	INT iDriverPart[FMMC_MAX_VEHICLES]
	INT iNumObjHighestPriorityHeld[FMMC_MAX_TEAMS]
	
	INT iNumPriorityRespawnPed[FMMC_MAX_TEAMS]
	INT iNumPriorityRespawnVeh[FMMC_MAX_TEAMS]
	INT iNumPriorityRespawnObj[FMMC_MAX_TEAMS]
	
	INT iNumObjDelAtPriority[FMMC_MAX_TEAMS][FMMC_MAX_RULES] //Never checked - FMMC2020
	INT iAnyColObjRespawnAtPriority[FMMC_MAX_TEAMS]
	INT iObjDelTeam[FMMC_MAX_NUM_OBJECTS]
	
	INT iTimeObjectTeamControlBarCache[FMMC_MAX_TEAMS]
	
	INT iMaxLoopSize
	INT iNumPlayerRuleCreated
	INT iNumLocCreated
	INT iNumPedCreated
	INT iNumVehCreated
	INT iNumObjCreated
	INT iNumTrainCreated
	INT iNumPedsSpawned //Includes respawns and gang backup

	INT iTargetType[FMMC_MAX_RULES][FMMC_MAX_TEAMS] 
	INT iMissionCriticalObj[FMMC_MAX_TEAMS]

	INT iLocteamFailBitset[FMMC_MAX_GO_TO_LOCATIONS]
	INT iLocOwner[FMMC_MAX_GO_TO_LOCATIONS]
	INT iPedAtYourHolding[FMMC_MAX_TEAMS][FMMC_MAX_PEDS_BITSET]
	INT iPedHoldPartPoints
	INT iPedTeamFailBitset[FMMC_MAX_PEDS]
	INT	iMissionCriticalPed[FMMC_MAX_TEAMS][FMMC_MAX_PEDS_BITSET]
	
	INT iDeadPedPhotoBitset[FMMC_MAX_TEAMS][FMMC_MAX_PEDS_BITSET]
	INT iCopPed[FMMC_MAX_PEDS_BITSET]
	INT iVehAtYourHolding[FMMC_MAX_TEAMS]
	INT iVehHoldPartPoints
	INT iVehTeamFailBitset[FMMC_MAX_VEHICLES]
	INT iMissionCriticalVeh[FMMC_MAX_TEAMS]
	INT iAmbientOverrideVehicle
	INT iAmbientOverrideVehicleSetup
	
	INT iObjAtYourHolding[FMMC_MAX_TEAMS]
	INT iObjHoldPartPoints[FMMC_MAX_NUM_OBJECTS]
	INT iObjteamFailBitset[FMMC_MAX_NUM_OBJECTS]
	
	INT iPlayerRuleLimit[FMMC_MAX_RULES][FMMC_MAX_TEAMS]
	INT iPlayerRuleBitset[FMMC_MAX_RULES][FMMC_MAX_TEAMS]
	
	INT isearchingforPed = -1 
	INT isearchingforObj = -1 
	INT isearchingforVeh = -1 
	INT iSpawnArea = -1
	SCRIPT_TIMER tdSpawnAreaTimeout
	INT iVehSpawnBitset
	INT iTrainSpawnBitset
	INT iPedRespawnVehBitset
	INT iPedFirstSpawnVehBitset
	INT iObjSpawnPedBitset[FMMC_MAX_PEDS_BITSET]
	INT iPedDelayRespawnBitset[FMMC_MAX_PEDS_BITSET]
	INT iVehDelayRespawnBitset
	
	INT iPedCleanup_NeedOwnershipBS[FMMC_MAX_PEDS_BITSET]
	INT iVehCleanup_NeedOwnershipBS // For server to request control when needing to cleanup a vehicle
	INT iObjCleanup_NeedOwnershipBS
	INT iInteractableCleanup_NeedOwnershipBS[FMMC_MAX_INTERACTABLE_BITSET]
	INT iTrainCleanup_NeedOwnershipBS
	INT iDynoPropCleanup_NeedOwnershipBS
	INT iVehAboutToCleanup_BS // For server to request control when needing to cleanup a vehicle
	INT iRuleReCapture[FMMC_MAX_TEAMS][FMMC_MAX_RULES]
	
	NETWORK_INDEX niTargetID[FMMC_MAX_PEDS]
	PLAYER_INDEX TargetPlayerID[FMMC_MAX_PEDS]

	INT iPartWarpingIn = -1 //The participant currently doing the apartment warp door exit animation
	
	SCRIPT_TIMER tdMissionLengthTimer
	INT iTotalMissionEndTime
	
	SCRIPT_TIMER timeServerCountdown
	
	INT iNumberOfTeams
	INT iNumActiveTeams
	INT iNumberOfPlayingTeams
	INT iFailTeam = FMMC_MAX_TEAMS
	INT iNumSpectators
	INT iNumberOfPart[FMMC_MAX_TEAMS]
	INT iTotalNumPart
	INT iNumberOfPlayingPlayers[FMMC_MAX_TEAMS]
	INT iNumberOfPlayersAndSpectators[FMMC_MAX_TEAMS]
	INT iTotalPlayingCoopPlayers[FMMC_MAX_TEAMS]
	INT iNumberOfLBPlayers[FMMC_MAX_TEAMS]
	INT iNumOfWanted[FMMC_MAX_TEAMS]
	INT iNumOfFakeWanted[FMMC_MAX_TEAMS]
	INT iNumOfHighestWanted[FMMC_MAX_TEAMS]
	INT iNumOfHighestFakeWanted[FMMC_MAX_TEAMS]
	INT iNumOfMasks[FMMC_MAX_TEAMS]
	FLOAT fLowestLootCapacity[FMMC_MAX_TEAMS]
	INT iTeamVehNumPlayers[FMMC_MAX_TEAMS]
	INT iTeamUniqueVehHeldNum[FMMC_MAX_TEAMS]
	INT iPriorityLocation[FMMC_MAX_TEAMS]
	
	INT iPartDeathEventBS // These two bits are used in server processing to get players swapping between teams on kill/death
	INT iPartNotAliveBS
	
	INT iCheckpointBitset
	
	INT iNextObjective[FMMC_MAX_TEAMS]
	INT iNextMission
	INT iEndMocapVariation = -1
	INT iCurrentExtraObjective[MAX_NUM_EXTRA_OBJECTIVE_ENTITIES][FMMC_MAX_TEAMS]
	
	INT iPedNearNoPlayer[FMMC_MAX_TEAMS][FMMC_MAX_PEDS_BITSET]
	INT iPedNearDropOff[FMMC_MAX_TEAMS][FMMC_MAX_PEDS_BITSET]
	INT iTeamScore[FMMC_MAX_TEAMS]
	INT iScoreOnThisRule[FMMC_MAX_TEAMS]
	INT iPlayerScore[MAX_NUM_MC_PLAYERS]
	INT iNumPlayerKills[MAX_NUM_MC_PLAYERS] 
	INT iNumPedKills[MAX_NUM_MC_PLAYERS]
	INT iNumPlayerDeaths[MAX_NUM_MC_PLAYERS]
	INT iNumHeadshots[MAX_NUM_MC_PLAYERS]
	INT iTeamPlayerKills[FMMC_MAX_TEAMS][FMMC_MAX_TEAMS]
	INT iKillScore[MAX_NUM_MC_PLAYERS]
	INT iTeamArrested[FMMC_MAX_TEAMS]
	INT iTeamCriticalAmmo[FMMC_MAX_TEAMS]
	
	INT iHostRefreshDpadValue
	
	INT iPedDelivered[FMMC_MAX_PEDS_BITSET]
	INT iPedSpookedBitset[FMMC_MAX_PEDS_BITSET]
	INT iPedAggroedBitset[FMMC_MAX_PEDS_BITSET]
	INT iPedAggroedFromEventBitset[FMMC_MAX_PEDS_BITSET]
	INT iPedSpookedByDeadBodyBitset[FMMC_MAX_PEDS_BITSET]
	INT iPedTaskActive[FMMC_MAX_PEDS_BITSET]
	INT iPedTaskStopped[FMMC_MAX_PEDS_BITSET]
	INT iPedSecondaryTaskActive[FMMC_MAX_PEDS_BITSET]
	INT iPedSecondaryTaskCompleted[FMMC_MAX_PEDS_BITSET]
	INT iPedAtGotoLocBitset[FMMC_MAX_PEDS_BITSET]
	INT iPedTasksArrivalResultsBS[FMMC_MAX_PEDS_BITSET]
	INT iPedCancelTasksBitset[FMMC_MAX_PEDS_BITSET]
	INT iPedAtSecondaryGotoLocBitset[FMMC_MAX_PEDS_BITSET]
	INT iPedShouldFleeDropOff[FMMC_MAX_PEDS_BITSET]
	INT iPedFirstSpawnBitset[FMMC_MAX_PEDS_BITSET]
	INT	iPedGunOnRuleGiven[FMMC_MAX_PEDS_BITSET] //Used so that we can check if a dead ped had been given a GunOnRule gun (on death their inventory clears, so we can't check if they had the gun or not)
	INT iPedCombatStyleChanged[FMMC_MAX_PEDS_BITSET]
	INT iPedDoneDefensiveAreaGoto[FMMC_MAX_PEDS_BITSET]
	INT iPedExistsForDamageRule[FMMC_MAX_PEDS_BITSET]
	
	INT iPedInvolvedInMiniGameBitSet[FMMC_MAX_PEDS_BITSET]
	
	INT iObjectiveMidPointBitset[FMMC_MAX_TEAMS]
	INT iObjectivePedAggroedBitset[FMMC_MAX_TEAMS] //Bitset to say if a ped has been aggroed for a certain team on a certain rule - the rule is the bitset index
	
	INT iProcessJobCompBitset
	
	INT iAverageMissionTime[FMMC_MAX_TEAMS]
	INT iMaxObjectives[FMMC_MAX_TEAMS]
	
	SCRIPT_TIMER tdMidpointFailTimer[FMMC_MAX_TEAMS]
	
	INT iObjCarrier[FMMC_MAX_NUM_OBJECTS]
	INT iObjCapturer[FMMC_MAX_NUM_OBJECTS]
	MC_PAUSABLE_TIMER sObjCaptureTimer[FMMC_MAX_NUM_OBJECTS]
	
	INT iVehRequestPart[FMMC_MAX_VEHICLES]
	INT iObjHackPart[FMMC_MAX_NUM_OBJECTS]
	
	INT iObjVaultDrillIDs[FMMC_MAX_VAULT_DRILL_GAMES]
	INT iObjVaultDrillDiscs[FMMC_MAX_VAULT_DRILL_GAMES]
	INT iObjVaultDrillProgress[FMMC_MAX_VAULT_DRILL_GAMES]
		
	// Termination timer
	SCRIPT_TIMER TerminationTimer
	
	#IF IS_DEBUG_BUILD	
		INT iDebugBitSet
		INT ijSkipBitset
	#ENDIF
	
	INT iReasonForObjEnd[FMMC_MAX_TEAMS]
	INT iPartCausingFail[FMMC_MAX_TEAMS]
	INT iEntityCausingFail[FMMC_MAX_TEAMS]	
	INT iAggroCountdownTimeStamp[FMMC_MAX_TEAMS][ciMISSION_AGGRO_INDEX_MAX]
	SCRIPT_TIMER tdCopSpottedFailTimer
	INT iAggroCountdownTimerSlack[FMMC_MAX_TEAMS] //add on some more time if they've got more people to kill before it fails
	
	INT iTeamCivillianKills[FMMC_MAX_TEAMS]
	INT iTeamEnemyKills[FMMC_MAX_TEAMS]
	INT iTeamKillScore[FMMC_MAX_TEAMS]
	
	INT iCaptureBarPercentage[FMMC_MAX_TEAMS]
	INT iSpawnPointsUsed[FMMC_MAX_TEAMS]
	
	INT iWinningTeam = TEAM_INVALID	
	INT iSecondTeam = TEAM_INVALID	
	INT iThirdTeam = TEAM_INVALID	
	INT iLosingTeam = TEAM_INVALID	
	
	INT iLastTeamAlive = -1
		
	INT iVariableEntityRespawns
	INT iDifficulty
	INT iNumStartingPlayers[FMMC_MAX_TEAMS]
	INT iTotalNumStartingPlayers
	SCRIPT_TIMER tdInitialisationTimeout
	SCRIPT_TIMER tdProcessedPreGameTimeout
	
	INT iWeather	= -1
	INT iTimeOfDay	= -1
	INT iProgressTime_Hours = -1
	INT iProgressTime_Minutes = -1
	FLOAT fVehicleDensity  = -1.0
	FLOAT fPedDensity	  = -1.0
	INT iPolice
	INT iOptionsMenuBitSet
	INT iWasHackObj
	INT iIsHackContainer
	INT iOpenHackContainer
	
	INT iCrateBrokenBitset
	INT iCrateDestructionProcessed
	INT iFragableCrate
	INT iFragableCrateCreated
	
	// CUTSCENES
	INT iLastCompletedGoToLocation = -1
	INT iCutsceneID[FMMC_MAX_TEAMS]
	INT iCutsceneStreamingIndex[FMMC_MAX_TEAMS]
	INT iCutsceneStarted[FMMC_MAX_TEAMS]
	INT iCutsceneFinished[FMMC_MAX_TEAMS]
	INT iCutscenePlayerBitset[FMMC_MAX_TEAMS]
	PLAYER_INDEX piCutscenePlayerIDs[FMMC_MAX_TEAMS][FMMC_MAX_CUTSCENE_PLAYERS]

	// End Cutscene
	INT iEndCutsceneNum[FMMC_MAX_TEAMS]

	// ????
	INT iNumCutscenePlayers[FMMC_MAX_TEAMS]
	INT iMaxNumCutscenePlayers[FMMC_MAX_TEAMS]
	INT iNumCutscenePlayersFinished[FMMC_MAX_TEAMS]
	INT iNumOtherTeamsConsideredForTeamCutscene // This stores which teams should view another team's cutscene
	
	VECTOR vTeamCentreStartPoint[FMMC_MAX_TEAMS]
	
	SERVER_EOM_VARS sServerFMMC_EOM
	
	INT iAreaTimer[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]	
	INT iHoldTimer[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
	INT iAreaTimerStartedBS[FMMC_MAX_TEAMS]
	SCRIPT_TIMER tdCapturePointsTimer
	
	SCRIPT_TIMER tdCutsceneStartTime[FMMC_MAX_TEAMS]
	
	INT tdMultiTimerStartBitset[FMMC_MAX_TEAMS]

	INT	iGotoLocationDataTakeoverTime[FMMC_MAX_TEAMS][FMMC_MAX_RULES]
	INT	iPedTakeoverTime[FMMC_MAX_TEAMS][FMMC_MAX_RULES]
	INT	iVehTakeoverTime[FMMC_MAX_TEAMS][FMMC_MAX_RULES]
	INT	iObjTakeoverTime[FMMC_MAX_TEAMS][FMMC_MAX_RULES]
	
	INT cutscene_vehicle_count = 0
	INT iSpawnScene = -1
	INT iSpawnShot = -1
	
	INT iVehicleRappelSeatsBitset[FMMC_MAX_VEHICLES]
	
	SERVER_CASH_GRAB_DATA sCashGrab
	
	INT iEndCutscene
	VECTOR vEndCutsceneOverridePosition
	FLOAT fEndCutsceneOverrideHeading
	INT iEndCutsceneConcatBS
	
	eFailMissionEnum 			eCurrentTeamFail[FMMC_MAX_TEAMS]	
	
	SCRIPT_TIMER iBetweenRoundsTimer
	
	INT iCashGrabRuleActive[FMMC_MAX_TEAMS]
	INT iThermiteRuleActive[FMMC_MAX_TEAMS]
	
	INT iSessionScriptEventKey
	
	INT iPointsGivenToPass[FMMC_MAX_TEAMS]
	
	INT iGranularCurrentPoints[FMMC_MAX_TEAMS]
	
	INT iUndeliveredVehCleanupBS[FMMC_MAX_TEAMS]
	INT iCleanedUpUndeliveredVehiclesBS
	
	INT iVehicleRespawnPool
	
	INT iVehicleNotSpawningDueToRandomBS //Passed into create & load but never checked? - FMMC2020
	INT iAllPlayersInUniqueVehiclesBS[FMMC_MAX_TEAMS]
	
	SCRIPT_TIMER tdSuddenDeath_ShrinkingBoundsHUDTimer[FMMC_MAX_TEAMS]
	
	INT iCachedMissionScoreLimit
	INT iCachedInitialPoints
	
	INT iServerBS_PassRuleVehAfterRespawn
	
	INT iHardTargetPointer[FMMC_MAX_TEAMS] //Should be gone with _Main - FMMC2020
	INT iHardTargetParticipant[FMMC_MAX_TEAMS] //Should be gone with _Main - FMMC2020
	
	CCTVCameraTurnState eCCTVCamState[MAX_NUM_CCTV_CAM]
	INT iCameraNumber[MAX_NUM_CCTV_CAM]
	FMMC_ELECTRONIC_ENTITY_TYPE eCameraType[MAX_NUM_CCTV_CAM]
	INT CCTV_TURNED_BITSET
	
	INT iControlPedTimerOffset[FMMC_MAX_PEDS]
	INT iControlVehTimerOffset[FMMC_MAX_VEHICLES]
	
	INT iAllPlayersInSeparateVehiclesBS[FMMC_MAX_TEAMS]
	
	SCRIPT_TIMER stCBwaitTimer
	
	INT iDynoPropShouldRespawnNowBS
	
	INT iNumTeamRuleAttempts[FMMC_MAX_TEAMS]
	
	SCRIPT_TIMER stVehicleDoorsOpenTimer
	INT iOpenVehicleDoorsBitset = 0
	
	INT iCargobobAttachedBitset = 0
	INT iCargobobDettachedBitset = 0
	INT iCargobobShouldDetachBitset = 0
	
	SCRIPT_TIMER tdControlObjTeamTimer[FMMC_MAX_TEAMS]
	
	INT iObjTeamCarrier
	INT iObjCaptured
	
	INT iOverriddenScriptedCutsceneRule[FMMC_MAX_TEAMS]
	INT iOverriddenScriptedCutsceneIndex[FMMC_MAX_TEAMS]
	
	INT iTeamFailRequests[FMMC_MAX_TEAMS]
	INT iIncrementRemotePlayerScoreRequests[ciMAX_SERVER_INCREMENT_SCORE_REQUESTS]
	
	INT iIntroTimeStamp
	
	SERVER_FLASH_FADE_ENTITY_DATA sFlashFade
	
	INT iObjectActionActivatedBS
	INT iObjectHealthThreshActivatedBS
	
	INT iDrone_EntityType[ciMAX_FMMC_DRONES_ACTIVE]
	INT iDrone_EntityIndex[ciMAX_FMMC_DRONES_ACTIVE]
	INT iDrone_AssociatedGotoPoolIndex[ciMAX_FMMC_DRONES_ACTIVE]
	INT iDrone_AssociatedGotoProgress[ciMAX_FMMC_DRONES_ACTIVE]
	INT iDrone_AssociatedGotoComplete
	INT iDrone_Retask
	INT iDrone_State[ciMAX_FMMC_DRONES_ACTIVE]
	
	INT iEntitiesInPreReqZoneBS
	
	SCRIPT_TIMER stAttachmentConsTriggerTimer[FMMC_MAX_NUM_OBJECTS]
	INT iProcessedAttachmentConsequence
	
	WARP_ON_DAMAGE_TRACKER_DATA sWarpOnDamageTracker
	
ENDSTRUCT

MC_ServerBroadcastData MC_serverBD

STRUCT MC_ServerBroadcastData1

	INT iControlVehTimer[FMMC_MAX_VEHICLES]
	INT iControlObjTimer[FMMC_MAX_NUM_OBJECTS]
	INT iControlPedTimer[FMMC_MAX_PEDS]
	
	INT inumberOfTeamInArea[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
	INT iNumberOfTeamInAllAreas[FMMC_MAX_TEAMS]
	INT inumberOfTeamInAnyArea[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
	INT iNumberOfTeamInLeaveArea[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
	INT iNumberOfTeamInAnyLeaveAreas[FMMC_MAX_TEAMS]
	INT	iNumTeamControlKills[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
	INT	iOldTeamControlkills[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
	INT iNumControlKills[MAX_NUM_MC_PLAYERS]
		
	FMMC_MISSION_SERVER_DATA_STRUCT sFMMC_SBD
	NETWORK_INDEX niMinigameCrateDoors[ciMAX_MINIGAME_CRATE_DOORS]	// This holds the network indexes for the doors on the minigame crate objects
	NETWORK_INDEX niYachtVehicles[ciYACHT_MAX_YACHTS][NUMBER_OF_PRIVATE_YACHT_VEHICLES]
	
	FMMC_CREATED_COUNT_DATA sCreatedCount
	
	INT iNumGangChaseCandidates[FMMC_MAX_TEAMS]
	INT iNumGangChaseUnitsChasing[FMMC_MAX_TEAMS]
	INT iNumGangChaseUnitsSpawned[FMMC_MAX_TEAMS][FMMC_MAX_RULES]
	INT iGangChaseAreaBitset[FMMC_MAX_TEAMS]
	INT iCurrentGangChaseUnitSpawn = -1
	INT iVariableNumBackupAllowed[FMMC_MAX_TEAMS]
	GANG_CHASE_DATA sGangChase[ciMAX_GANG_CHASE_VEHICLES]
	INT iNextGangChaseRespawnTime = 0
	INT iGangChasePedTargetOverride = -1
	
	INT iPlayerForHackingMG = -1
	INT iPlayerHacking = -1
	INT iVehicleForHackingMG = -1
	INT iServerHackMGBitset	
	
	INT iTotalPackages = -1
	
	INT iPTLDroppedPackageOwner[FMMC_MAX_NUM_OBJECTS]			//FMMC2020 rename to generic
	INT iPTLDroppedPackageLastTeam[FMMC_MAX_NUM_OBJECTS]		//FMMC2020 rename to generic
	INT iPTLDroppedPackageLastOwner[FMMC_MAX_NUM_OBJECTS]		//FMMC2020 rename to generic
	
	INT iGOFoundryHostageSynchSceneID = -1
	
	INT iObjDeliveredOnceBS // Bits to say if an object has been delivered
	
	INT iHackDialogueCompletedBS[ciMAX_DIALOGUE_BIT_SETS]
	
	INT iObjectiveProgressionHeldUpBitset[FMMC_MAX_TEAMS] // Bitset to say if a rule should be held up for a certain team (e.g. for circuit hack minigame)
	
	INT iNumCutsceneSpectatorPlayers[ FMMC_MAX_TEAMS ]	
	
	NETWORK_INDEX niDZSpawnVehicle[FMMC_MAX_TEAMS*2] //Aerial spawn vehicle, 2 per team
	NETWORK_INDEX niDZSpawnVehPed[FMMC_MAX_TEAMS*2] //Ped to sit in each spawn veh
	
	PLAYER_INDEX piTimeBarWinners[5]	//5 players max
	
	INT iCoronaTeamLives[FMMC_MAX_TEAMS]
	
	INT iBSVehicleStopForever = 0
	INT iBSVehicleStopDuration = 0
	SCRIPT_TIMER timeVehicleStop
	INT iVehStopTimeToWait
	
	FLOAT fCarnageBar_BarFilledHeliTarget = -1.0 									// Add fCarnageBar_FillRate incrementally
	FLOAT fCarnageBar_BarFilledCarTarget = -1.0									// Add fCarnageBar_FillRate incrementally
	FLOAT fCarnageBar_FillRate													// Added onto BarFilledTarget every X amount of seconds, in order to prevent BD spam.
	FLOAT fCarnageBar_PedKilledForFill											// Cleared when added to fCarnageBar_BarFilledTarget
	FLOAT fCarnageBar_VehDestroyedForFill										// Cleared when added to fCarnageBar_BarFilledTarget
	FLOAT fCarnageBar_StuntPerformedForFill										// Cleared when added to fCarnageBar_BarFilledTarget
	INT iPartLookingAtCar = 0
	
	INT iPedFixationTaskEnded[FMMC_MAX_PEDS_BITSET]
	INT iPedFixationTaskStarted[FMMC_MAX_PEDS_BITSET]
	INT iVehicleTheftWantedTriggered
	
	INT iVehicleBlockedWantedConeResponseBS
	INT iVehicleBlockedWantedConeResponseSeenIllegalBS
	INT iDoorUpdateOffRuleBS
	
	FMMC_MISSION_CONTINUITY_VARS sMissionContinuityVars
	
	SCRIPT_TIMER stCopDecoyActiveTimer
	
	INT iTeamThermalCharges[FMMC_MAX_TEAMS]
	
	INT iCurrentCutPaintingSubAnim[ciFMMC_MAX_PAINTING_INDEX]
	
	INT iCurrentVaultDoorPlantedExplosives_LeftSide = 0
	INT iCurrentVaultDoorPlantedExplosives_RightSide = 0
	
	INT iScriptedCutsceneSyncScene = -1
	INT iScriptedCutsceneSyncSceneInvisibilityBS
	FLOAT fSyncSceneAnimDurationForPhaseFadeoutTime
	
	INT iRandomNextObjectiveSeed[FMMC_MAX_TEAMS]
	
ENDSTRUCT

MC_ServerBroadcastData1 MC_serverBD_1

STRUCT MC_ServerBroadcastData2
	TEXT_LABEL_63 tParticipantNames[MAX_NUM_MC_PLAYERS]
	INT iPedGotoProgress[FMMC_MAX_PEDS]
	INT iAssociatedGotoPoolIndex[FMMC_MAX_PEDS]
	INT iPartPedFollows[FMMC_MAX_PEDS]
	INT iOldPartPedFollows[FMMC_MAX_PEDS]
	INT iPedState[FMMC_MAX_PEDS]
	INT iOldPedState[FMMC_MAX_PEDS]
	INT iAssociatedAction[FMMC_MAX_PEDS]
	INT iTargetID[FMMC_MAX_PEDS]	
	
	INT iCurrentPedRespawnLives[FMMC_MAX_PEDS]
	INT iCurrentVehRespawnLives[FMMC_MAX_VEHICLES]
	INT iCurrentObjRespawnLives[FMMC_MAX_NUM_OBJECTS]
	
	//Can use iObjSpawnPedBitset for peds
	INT iVehSpawnedOnce
	INT iObjSpawnedOnce
	INT iVehIsDestroyed

	STRUCT_ARTIFICIAL_LIGHTS_DATA sArtificialLightsServerData
	
	INT iClobberedScore[ FMMC_MAX_TEAMS ]
	
	INT iNumPartPlayingAndFinished[ FMMC_MAX_TEAMS ]
	
	INT iCamObjDestroyed
	
	NETWORK_INDEX niDrillMinigameWall
	NETWORK_INDEX niDrillMinigameDoor
	
	FMMC_ARENA_TRAPS_HOST_INFO sTrapInfo_Host
	
	CELEB_SERVER_DATA sCelebServer
		
	INT iAggroText_PedSpooked		= -1
	INT iAggroText_PartCausingSpook = -1
	INT iAggroText_SpookReason		= -1
	
ENDSTRUCT
MC_ServerBroadcastData2 MC_serverBD_2

INT iPopulationHandle = -1
BOOL bVehicleGenerator = FALSE

INT iWeatherTransitionStartTime

INT iEntityPTFXTypeInUseBS
 
STRUCT MC_ServerBroadcastData3

	INT iRoundLbdProgress
	INT iWinningPlayerForRounds = -1
	PLAYER_INDEX iWinningPlayerForRoundsId
	SCENARIO_BLOCKING_INDEX 	scenBlockServer = NULL
	
	INT iPedSpawnDelay[FMMC_MAX_PEDS]
	INT iVehSpawnDelay[FMMC_MAX_VEHICLES]
	INT iObjSpawnDelay[FMMC_MAX_NUM_OBJECTS]
	
	INT iProximitySpawning_PedSpawnAllBS[FMMC_MAX_PEDS_BITSET]
	INT iProximitySpawning_PedSpawnAnyBS[FMMC_MAX_PEDS_BITSET]
	INT iProximitySpawning_PedSpawnOneBS[FMMC_MAX_PEDS_BITSET]
	INT iProximitySpawning_PedCleanupBS[FMMC_MAX_PEDS_BITSET]
	
	INT iProximitySpawning_DynoPropSpawnAllBS
	INT iProximitySpawning_DynoPropSpawnAnyBS
	INT iProximitySpawning_DynoPropSpawnOneBS
	INT iProximitySpawning_DynoPropCleanupBS
	
	INT iProximitySpawning_VehSpawnAllBS
	INT iProximitySpawning_VehSpawnAnyBS
	INT iProximitySpawning_VehSpawnOneBS
	INT iProximitySpawning_VehCleanupBS
	
	FLOAT fSumoSuddenDeathRadius
	INT fSumoSuddenDeathTimeToGetIn
	
	INT iTeamToScoreLast = -1
	VECTOR vVIPMarker
	
	INT iPropDestroyedBS[FMMC_MAX_PROP_BITSET]	//CEIL(FMMC_MAX_NUM_PROPS / 32)
	
	INT iTaggedEntityBitset
	INT iTaggedEntityType[FMMC_MAX_TAGGED_ENTITIES]
	INT iTaggedEntityIndex[FMMC_MAX_TAGGED_ENTITIES]
	INT iTaggedEntityGangChaseUnit[FMMC_MAX_TAGGED_ENTITIES]
	
	#IF IS_DEBUG_BUILD	
		INT iMultiObjectiveTimeLimitDebugCache[FMMC_MAX_TEAMS]
		INT iRuleObjectiveTimeLimitDebugCache[FMMC_MAX_TEAMS]
		INT iMissionLengthTimerDebugCache
	#ENDIF
	INT iRuleObjectiveTimeLimitKeepSyncCache[FMMC_MAX_TEAMS]
		
	SCRIPT_TIMER tdObjectiveLimitTimer[FMMC_MAX_TEAMS]
	SCRIPT_TIMER tdMultiObjectiveLimitTimer[FMMC_MAX_TEAMS]
	SCRIPT_TIMER tdLimitTimer
	INT iTimerPenalty[FMMC_MAX_TEAMS]
	INT iMultiObjectiveTimeLimit[FMMC_MAX_TEAMS]	
	
	INT iNumberOfPlayersInRuleBounds[FMMC_MAX_TEAMS][ciMAX_RULE_BOUNDS_PER_RULE]
	INT iPlayerBoundAllowedBitset[ciMAX_RULE_BOUNDS_PER_RULE]
	
	SCRIPT_TIMER tdTagOutCooldown[FMMC_MAX_TEAMS]
	INT iServerTagTeamPartTurn[FMMC_MAX_TEAMS]
	INT iServerTagTeamPartTimeTagged[NUM_NETWORK_PLAYERS]
	
	INT iArena_Lighting = -1
	
	VECTOR vCratePickupCoords[FMMC_MAX_NUM_OBJECTS][ciMAX_CRATE_PICKUPS]
	
	//Trip Skip Data
	TRIP_SKIP_SERVER_BD TSServerData[FMMC_MAX_TEAMS]
	TRIP_SKIP_SERVER_BD FTSServerData
	
	INT iSuddenDeathTargetRound
	
	SCRIPT_TIMER tdTeamMissionTime[FMMC_MAX_TEAMS] // A timer counting how long this team has been playing
	INT iTeamMissionTime[FMMC_MAX_TEAMS]
	
	INT iAdditionalTeamLives[FMMC_MAX_TEAMS]
	INT iTeamLivesOverride[FMMC_MAX_TEAMS]
	
	INT iGhostingPlayerVehiclesVeh = -1			// [fmmc2020] seems to be on a separate generic option. Weird.

	INT	iNumberOfPlayersLeftLeaveEntity[FMMC_MAX_TEAMS][FMMC_MAX_PEDS] // Peds are largest entity array for objectives.
	
	INT iCurrentTrainLocation[FMMC_MAX_TRAINS]
	INT iTrainStopTime[FMMC_MAX_TRAINS]
	
	RADIO_EMITTER_STRUCT sRadioEmitters[FMMC_MAX_NUM_RADIO_EMITTERS]
	
	INT iSyncedMediaPlayerStartPoint = 0
	
	PLAYER_INDEX playerLocOwner[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
	
ENDSTRUCT

MC_ServerBroadcastData3 MC_serverBD_3

NEXT_JOB_STRUCT nextJobStruct

STRUCT VEHICLE_SWAP_STRUCT
	MODEL_NAMES 		mnVehicleModelSwap
	INT 				iVehicleModelSwap
	INT 				iVehicleHealthSwap
	
	INT 				iVehicleTurretSwap
	INT 				iVehicleArmourSwap
	INT 				iVehicleAirCounterSwap
	INT 				iVehicleBombBaySwap
	INT 				iVehicleExhaustSwap
	
	INT 				iVehicleAirCounterCooldown = -1
ENDSTRUCT

STRUCT DOOR_SERVER_DATA
	INT iHostDirectionalDoorUnlockedBS	
ENDSTRUCT

STRUCT INTERACTABLE_SERVER_DATA
	INT iInteractable_InteractingParticipant[FMMC_MAX_NUM_INTERACTABLES]
	INT iInteractable_CompletedBS[ciINTERACTABLE_LONG_BS_LENGTH]
ENDSTRUCT

STRUCT PED_CLONING_SERVER_DATA
	INT iNumOfEntsToClone = 0
	PLAYER_INDEX playerSource[FMMC_MAX_PEDS]
	NETWORK_INDEX niTarget[FMMC_MAX_PEDS]
ENDSTRUCT

STRUCT BURNING_VEHICLE_ARRAY_DATA
	PTFX_ID ptfxVehicleFireFX_Front
	PTFX_ID ptfxVehicleFireFX_Back
	FLOAT fBurningVehicleHealth
	FLOAT fVehicleStartHealth
	INT iLifeTime
ENDSTRUCT

STRUCT BURNING_VEHICLE_DATA
	BURNING_VEHICLE_ARRAY_DATA sVehicle[FMMC_MAX_VEHICLES]
	INT iBurningVehiclePTFXBitset
	INT iBurningVehicleExtinguishedBitset
ENDSTRUCT

STRUCT MC_ServerBroadcastData4
	INT iCurrentHighestPriority[FMMC_MAX_TEAMS]
	INT iPreviousHighestPriority[FMMC_MAX_TEAMS]
	INT iTimesRevalidatedObjectives[FMMC_MAX_TEAMS] // This is so that we don't get job comp events that move a mission back to the start, but then the same event from a different player completing that objective again in the future
	INT iPlayerRule[FMMC_MAX_RULES][FMMC_MAX_TEAMS] 
	INT iPlayerRulePriority[FMMC_MAX_RULES][FMMC_MAX_TEAMS] 
	INT	iGotoLocationDataRule[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
	INT iGotoLocationDataPriority[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS] 
	INT	iPedRule[FMMC_MAX_PEDS][FMMC_MAX_TEAMS]
	INT iPedPriority[FMMC_MAX_PEDS][FMMC_MAX_TEAMS]
	INT	iVehRule[FMMC_MAX_VEHICLES][FMMC_MAX_TEAMS]
	INT iVehPriority[FMMC_MAX_VEHICLES][FMMC_MAX_TEAMS]
	INT	iObjRule[FMMC_MAX_NUM_OBJECTS][FMMC_MAX_TEAMS]
	INT iObjPriority[FMMC_MAX_NUM_OBJECTS][FMMC_MAX_TEAMS]
	
	INT iCompanionPedIndex[FMMC_MAX_COMPANIONS]	
	
	INT iPlayerRuleMissionLogic[FMMC_MAX_TEAMS]
	INT iLocMissionLogic[FMMC_MAX_TEAMS]
	INT iPedMissionLogic[FMMC_MAX_TEAMS]
	INT iVehMissionLogic[FMMC_MAX_TEAMS]
	INT iObjMissionLogic[FMMC_MAX_TEAMS]
	INT iObjMissionSubLogic[FMMC_MAX_TEAMS]
	
	INT iPedCurrentPriorityGotoLocCompletedBS[FMMC_MAX_PEDS]
		
	INT iTeamSwapLockFlag
	INT iTargetScoreMultiplierSetting
	
	sFMMC_RandomSpawnGroupStruct rsgSpawnSeed
	sFMMC_RandomSpawnGroupStruct rsgSpawnSeedCachedMissionStart
	INT iSpawnGroupsBuiltInPlay_TempSpawnSeedBS
			
	INT iCapturePedRequestPartBS[ciFMMC_PED_BITSET_SIZE]
	INT iCapturePedBeingCapturedBS[ciFMMC_PED_BITSET_SIZE]
	INT iPedExplodeKillOnCaptureBS[ciFMMC_PED_BITSET_SIZE]
	INT iPedClearFailOnGoTo[ciFMMC_PED_BITSET_SIZE]
	INT iPedSpeedOverride[ciFMMC_PED_BITSET_SIZE]
	INT iPedHeightOverride[ciFMMC_PED_BITSET_SIZE]
	INT iPedHasAttachedObject[ciFMMC_PED_BITSET_SIZE]
	INT iPedFleeDueToVehicleHealth[ciFMMC_PED_BITSET_SIZE]
	INT iPedFleeDueToZone[ciFMMC_PED_BITSET_SIZE]
	INT iCaptureTimestamp
	
	INT iCaptureVehRequestPartBS
	INT iVehExplodeKillOnCaptureBS
	INT iVehUnlockOnCapture
	INT iVehMadeInvincibleBS
	
	INT iFirstCheckpointRule[FMMC_MAX_TEAMS]
	INT iSecondCheckpointRule[FMMC_MAX_TEAMS]
	INT iThirdCheckpointRule[FMMC_MAX_TEAMS]
	INT iFourthCheckpointRule[FMMC_MAX_TEAMS]
	
	SCRIPT_TIMER tdPedRuleSkipTimer
	INT iDialogueRangeBitset[ciMAX_DIALOGUE_BIT_SETS]
	
	BURNING_VEHICLE_DATA sBurningVehicle
	
	INT iNumberOfFiresExtinguished[FMMC_MAX_TEAMS]
		
	VEHICLE_SWAP_STRUCT sVehicleSwaps[FMMC_MAX_TEAMS][FMMC_MAX_RULES]		// [fmmc2020] - Vehicle swaps are a good system but a huge amount of data is wasted if we don't need it in the 2020 controller.
	
	INT iGangChasePedsKilledThisRule
	
	INT iVehHackedReadyForDeathBS
	INT iVehHackeDeathCountedBS
	INT iHackingTargetsRemaining
	
	INT iHackingFails
	INT iFacilityPosVehSpawned									// [fmmc2020] - Doesn't have to explicitly relate to facility but instead any kind of property. Should be renamed along with it's associated option.
	
	INT iAvengerHoldTransitionBlockedCount[FMMC_MAX_TEAMS]
	PLAYER_INDEX piGangBossID									// [fmmc2020] - We should aim to use this in the place of any checks where we're calling freemode wrappers.	
	
	INT iPedKilledBS[ciFMMC_PED_BITSET_SIZE]
	INT iObjectDoNotBlipBS
	INT iObjectsDestroyedForSpecialHUDBitset[FMMC_MAX_TEAMS]
	INT iObjectsDestroyedForSpecialHUD[FMMC_MAX_TEAMS]
	INT iObjectsRequiredForSpecialHUD[FMMC_MAX_TEAMS]
	
	SCRIPT_TIMER tdIncrementalRuleFailTimer[FMMC_MAX_TEAMS]
	INT iRuleTimerFailElapsedTime[FMMC_MAX_TEAMS]
	
	INT iDestroyedTurretBitSet
	INT iTransitionSessionMultirule_TimeStamp[FMMC_MAX_TEAMS]
	INT iTransitionSessionMultirule_Checkpoint[FMMC_MAX_TEAMS]
	
	FLOAT fVehSpeed
	INT iExplodeUpdateIter
	FLOAT fExplodeProgress
	
	DOOR_SERVER_DATA sDoorServerData
	INT iInteractWith_BombPlantParticipant = -1
	
	INTERACTABLE_SERVER_DATA sIntServerData
	
	INT iAltVarsBS[ciFMMC_ALT_VAR_MAX_BS] // This is used for when Freemode Global Structs do not have a corresponding Player/Server BD. We will convert their data into a readable BITSET format for the Variations Headers.
	
	INT iLocationCleanedUpEarlyBitset
	INT iLocationInputTriggeredBitset
	INT iLocationWaitingForInputBitset
	
	INT iVehicleImmovableBitset
	INT iVehicleHadImmovableSetBitset
		
	INT iAutoTriggerAfterTimeStampStart[FMMC_MAX_WARP_PORTALS]
	INT iAutoTriggerAfterTimeStampEnd[FMMC_MAX_WARP_PORTALS]
	
	//If we need to reduce the stack hit from this functionality then we need to array PED_CLONING_SERVER_DATA by a certain amount, 
	//and add a new variable inside of it to keep track of the iPed creator index this clone data belongs to, which will need to be cleaned up when finished.
	//The problem with doing this is that the max number of peds that could potentially require this functionality at once is 50, and the optimisation would result in saving on 10 ints.
	PED_CLONING_SERVER_DATA sPedCloningData
ENDSTRUCT
MC_ServerBroadcastData4 MC_serverBD_4

//HIGH PRIORITY SERVERBD GOES HERE IF/WHEN WE HAVE A USE FOR IT

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Player Broadcast Data
// ##### Description: Player Broadcast data structs, declarations and bitset bits.
// ##### This data can be read by all participants but each player can only write to their own data.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT 	PBBOOL_PRESSED_F 							0
CONST_INT	PBBOOL_PRESSED_S 							1
CONST_INT	PBBOOL_PRESSED_J 							2
CONST_INT	PBBOOL_CLR_SRV_OBJ_BIT 						3
CONST_INT	PBBOOL_OBJECTIVE_BLOCKER 					4
CONST_INT   PBBOOL_FINISHED       						5
CONST_INT   PBBOOL_START_SPECTATOR						6
CONST_INT 	PBBOOL_PLAYER_FAIL 							7
CONST_INT 	PBBOOL_PLAYER_OUT_OF_LIVES					8
CONST_INT 	PBBOOL_CHASE_TARGET    						9
CONST_INT   PBBOOL_ALL_HEADSHOTS   						10
CONST_INT   PBBOOL_WITH_CARRIER   						11
CONST_INT   PBBOOL_TERMINATED_SCRIPT 					12
CONST_INT	PBBOOL_CLIENT_STATE_INITIALISED				13
CONST_INT	PBBOOL_FINISHED_INTRO_CUTSCENE				14
CONST_INT   PBBOOL_IN_GROUP_PED_VEH  					15
CONST_INT   PBBOOL_ON_LEADERBOARD     					16
CONST_INT   PBBOOL_REQUEST_CUTSCENE_PLAYERS 			17
CONST_INT   PBBOOL_LAST_LIFE            				18
CONST_INT   PBBOOL_EARLY_END_SPECTATOR  				19
CONST_INT   PBBOOL_WEARING_MASK         				20
CONST_INT   PBBOOL_HEIST_HOST           				21
CONST_INT	PBBOOL_HEIST_I_AM_INSIDE_ANY_MP_PROPERTY 	22
CONST_INT	PBBOOL_WAS_THE_LOBBY_LEADER				 	23
CONST_INT   PBBOOL_ANY_SPECTATOR         				24
CONST_INT   PBBOOL_LOCK_SYNC_STARTED     				25
CONST_INT	PBBOOL_I_AM_DROWNING		 				26
CONST_INT	PBBOOL_DROPPED_OUT_OF_SECONDARY_ANIM		27
CONST_INT	PBBOOL_ZONE_ROCKETS_REQUIRED				28
CONST_INT	PBBOOL_HEIST_SPECTATOR 						29
CONST_INT	PBBOOL_PLAYING_CUTSCENE 					30
CONST_INT	PBBOOL_READY_FOR_INTRO_CUTSCENE_RULE		31

CONST_INT	PBBOOL2_REACHED_GAME_STATE_RUNNING					0
CONST_INT	PBBOOL2_COMPLETED_CELEBRATION_STREAMING_REQUESTS	1
CONST_INT	PBBOOL2_ALL_PARTICIPANTS_LAUNCHED_CELEB_SCRIPT		2
CONST_INT	PBBOOL2_HACKING_MINIGAME_COMPLETED					3
CONST_INT	PBBOOL2_HACKING_INTRO_COMPLETED						4
CONST_INT	PBBOOL2_PLAYING_MOCAP_CUTSCENE						5
CONST_INT	PBBOOL2_STARTED_CUTSCENE							6
CONST_INT	PBBOOL2_FINISHED_CUTSCENE                           7
CONST_INT	PBBOOL2_HACKING_MINIGAME_FAILED						8
CONST_INT	PBBOOL2_PLAYER_DEATH_CAUSED_FAIL					10
CONST_INT	PBBOOL2_REACHED_GAME_STATE_INI						11
CONST_INT	PBBOOL2_I_AM_SPECTATOR_WATCHING_CUTSCENE			12
CONST_INT	PBBOOL2_COMPLETED_POPULATION_OF_CELEB_GLOBALS		13
CONST_INT	PBBOOL2_PLAYER_OUT_BOUNDS_FAIL						14
CONST_INT	PBBOOL2_PLAYER_LEAVE_AREA_FAIL						15
CONST_INT	PBBOOL2_OUT_OF_BOUNDS_0								19
CONST_INT	PBBOOL2_OUT_OF_BOUNDS_1								20
CONST_INT	PBBOOL2_OUT_OF_BOUNDS_2								21
CONST_INT	PBBOOL2_LOSE_GANG_CHASE								22
CONST_INT	PBBOOL2_USE_FAKE_PHONE								23
CONST_INT	PBBOOL2_TASK_PLAYER_CUTANIM_INTRO					24
CONST_INT	PBBOOL2_IN_DROP_OFF									26
CONST_INT	PBBOOL2_DROPPING_OFF								27
CONST_INT	PBBOOL2_IN_MID_MISSION_CUTSCENE						28
CONST_INT	PBBOOL2_I_HAVE_SEEN_BENNY_GARAGE_POST_MISSION_SCENE	30
CONST_INT 	PBBOOL2_CACHE_RESPAWN_VEHICLE						31

//iClientBitSet3

CONST_INT	PBBOOL3_RUGBY_GOAL_TARGET_MISSED					0
CONST_INT	PBBOOL3_RUGBY_GOAL_TARGET_HIT						1
CONST_INT	PBBOOL3_SPAWN_PROTECTION_ON							2
CONST_INT	PBBOOL3_ALL_TEAM_MEMBERS_WITHIN_REQDIST				3
CONST_INT	PBBOOL3_LIGHTS_OFF									4
CONST_INT	PBBOOL3_TRANSFORMED_INTO_JUGGERNAUT					14
CONST_INT	PBBOOL3_TRANSFORMED_INTO_BEAST						15
CONST_INT	PBBOOL3_UPGRADED_WEP_VEHICLE						16
CONST_INT	PBBOOL3_HAS_OBJECT_INVENTORY						17
CONST_INT	PBBOOL3_PROCESSED_PRE_GAME							18
CONST_INT	PBBOOL3_TURNED_ON_LIGHTS_OFF_TC_MODIFIER			19
CONST_INT	PBBOOL3_WAS_JUGGERNAUT_ON_DEATH						20
CONST_INT	PBBOOL3_INITIATED_VEHICLE_SEAT_SWAP					23
CONST_INT	PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE			24
CONST_INT	PBBOOL3_DATA_CHANGING_SEAT_FINAL					25
CONST_INT	PBBOOL3_DATA_CHANGING_SEAT_DONE						26
CONST_INT	PBBOOL3_DATA_CHANGING_SEAT_GAVE_CONSENT				27
CONST_INT	PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN			28
CONST_INT	PBBOOL3_USING_BIRDS_EYE_CAMERA						29
CONST_INT	PBBOOL3_CALL_CACHED_VEHICLE_AND_SKIP_FLASH_FADE		30
CONST_INT	PBBOOL3_FINISH_FLASH_FADE_PROC						31

//iClientBitSet4
CONST_INT 	PBBOOL4_PLAYER_IS_USING_SCRIPTED_GUN_CAM			0
CONST_INT	PBBOOL4_PLAYER_IS_AIMING_AT_PROTECT_PED				1
CONST_INT	PBBOOL4_IS_CURRENT_OBJECTIVE_REPAIR_CAR				2
CONST_INT	PBBOOL4_TREAT_AS_WANTED								3
CONST_INT   PBBOOL4_SPEED_FAIL_TRIGGERED						4
CONST_INT   PBBOOL4_REQUEST_AVENGER_HOLD_TRANSITION_BLOCK		5
CONST_INT   PBBOOL4_BLOCKING_AVENGER_HOLD_TRANSITION			6
CONST_INT	PBBOOL4_SPAWNED_IN_A_PLACED_VEHICLE					7
CONST_INT 	PBBOOL4_USING_WARP_PORTAL_FLY_CAM					8
CONST_INT 	PBBOOL4_FINISHED_WARP_PORTAL_FLY_CAM				9
CONST_INT 	PBBOOL4_START_POSITION_SET							13
CONST_INT 	PBBOOL4_FMMC_YACHTS_READY							14
CONST_INT 	PBBOOL4_SYNC_PLAYERS_FOR_END_OF_CUTSCENE			15
CONST_INT	PBBOOL4_RUNNING_WALL_RAPPEL_NOT_ON_RAPPEL_RULE		16
CONST_INT 	PBBOOL4_REACHED_CUT_PROCESS_SHOTS					17
CONST_INT	PBBOOL4_RAPELLING_FROM_HELI							18
CONST_INT	PBBOOL4_READY_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE		20
CONST_INT	PBBOOL4_REGISTERED_ENTITIES_FOR_END_MOCAP_PASS_OVER	21
CONST_INT	PBBOOL4_MISSION_CURRENT_OBJECTIVE_FAILED_BY_BOUNDS	22
CONST_INT	PBBOOL4_CHANGED_INTO_DISGUISE						23
CONST_INT	PBBOOL4_RESET_SPAWN_GROUP_CACHED_VARS_ON_THIS_REQUEST	24
CONST_INT	PBBOOL4_CLIENT_ASSIGNED_LATE_CORONA_DATA			25
CONST_INT	PBBOOL4_CLIENT_READY_FOR_SPECIAL_MISSION_INTRO		26
CONST_INT 	PBBOOL4_FINISHED_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE	27
CONST_INT 	PBBOOL4_ASSIGNED_ALL_CACHED_PLAYER_PEDS				28
CONST_INT 	PBBOOL4_USING_WARP_PORTAL_FLY_CAM_BACK_IN			29
CONST_INT 	PBBOOL4_PLAYER_SET_ON_FIRE							30
CONST_INT 	PBBOOL4_CACHE_VEHICLE_COMPLETED						31

//iClientBitSet5
CONST_INT 	PBBOOL5_APPLY_SERVER_CACHED_CLONE_HEAD_BLEND		0

//Assuming that ciMAX_GANG_CHASE_VEHICLES = 5
CONST_INT	PBGANGSPAWN_UNIT_0						0
CONST_INT	PBGANGSPAWN_UNIT_1						1
CONST_INT	PBGANGSPAWN_UNIT_2						2
CONST_INT	PBGANGSPAWN_UNIT_3						3
CONST_INT	PBGANGSPAWN_UNIT_4						4
CONST_INT	PBGANGSPAWN_UNIT_0_TRY_FORWARDS			5
CONST_INT	PBGANGSPAWN_UNIT_1_TRY_FORWARDS			6
CONST_INT	PBGANGSPAWN_UNIT_2_TRY_FORWARDS			7
CONST_INT	PBGANGSPAWN_UNIT_3_TRY_FORWARDS			8
CONST_INT	PBGANGSPAWN_UNIT_4_TRY_FORWARDS			9
CONST_INT	PBGANGSPAWN_UNIT_0_SUCCEED_FORWARDS		10
CONST_INT	PBGANGSPAWN_UNIT_1_SUCCEED_FORWARDS		11
CONST_INT	PBGANGSPAWN_UNIT_2_SUCCEED_FORWARDS		12
CONST_INT	PBGANGSPAWN_UNIT_3_SUCCEED_FORWARDS		13
CONST_INT	PBGANGSPAWN_UNIT_4_SUCCEED_FORWARDS		14
CONST_INT	PBGANGSPAWN_UNIT_0_AIR_FAILED_HELISPAWN	15
CONST_INT	PBGANGSPAWN_UNIT_1_AIR_FAILED_HELISPAWN	16
CONST_INT	PBGANGSPAWN_UNIT_2_AIR_FAILED_HELISPAWN	17
CONST_INT	PBGANGSPAWN_UNIT_3_AIR_FAILED_HELISPAWN	18
CONST_INT	PBGANGSPAWN_UNIT_4_AIR_FAILED_HELISPAWN	19
CONST_INT	PBGANGSPAWN_UNIT_0_SPAWN_BEHIND			20
CONST_INT	PBGANGSPAWN_UNIT_1_SPAWN_BEHIND			21
CONST_INT	PBGANGSPAWN_UNIT_2_SPAWN_BEHIND			22
CONST_INT	PBGANGSPAWN_UNIT_3_SPAWN_BEHIND			23
CONST_INT	PBGANGSPAWN_UNIT_4_SPAWN_BEHIND			24

CONST_INT 	GOTO_HIT_SOUND_DEFAULT					0
CONST_INT	GOTO_HIT_SOUND_ALT						1
CONST_INT	GOTO_HIT_SOUND_BOMBDISARM				3

CONST_INT	ciBS_MAN_RESPAWN_STARTED								0
CONST_INT	ciBS_MAN_RESPAWN_ENTERED_VEHICLE						1
CONST_INT	ciBS_MAN_RESPAWN_PASSENGERS_LEFT_VEHICLE				2
CONST_INT	ciBS_MAN_RESPAWN_FINISHED								3
CONST_INT	ciBS_MAN_RESPAWN_FORCED									4

CONST_INT 	ci_CARNAGEBAR_BS_LOOKING_AT_CAR					0
CONST_INT 	ci_CARNAGEBAR_BS_IS_USING_HELI_CAM				1
CONST_INT 	ci_CARNAGEBAR_BS_IS_DRIVER_OF_HELI				2
CONST_INT 	ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR				3

STRUCT MC_PlayerBroadcastData
 
	// Clients start here
	INT iClientLogicStage = CLIENT_MISSION_STAGE_SETUP
	INT iTeam

	INT iGameState

	INT iCurrentLoc =-1
	INT iInAnyLoc = -1
	INT iLeaveLoc = -1
	INT iPedNear = -1
	INT iVehNear = -1
	INT iObjNear = -1
	
	INT iPedLeftBS[ciFMMC_PED_BITSET_SIZE]
	INT iVehLeftBS = 0
	INT iObjLeftBS = 0
	
	INT iPartNear = -1	
	INT iNearDialogueTrigger = -1
	
	INT iLocPhoto =-1
	INT iPedPhoto = -1
	INT iVehPhoto = -1
	INT iObjPhoto = -1
	
	INT iVehRequest = -1 
	
	INT iObjHacking = -1 
	INT iObjHacked = -1 			// Only used by iLocalPart and iPartToUse [fmmc2020] refactor ?
	INT iHackFailBitset 			// Only used by iLocalPart and iPartToUse [fmmc2020] refactor ?
	INT iBeginHackFailBitset	 	// Only used by iLocalPart and iPartToUse [fmmc2020] refactor ?
	INT iRequestHackAnimBitset 		// Only used by iLocalPart and iPartToUse [fmmc2020] refactor ?
	INT iSafeSyncSceneID[FMMC_MAX_NUM_OBJECTS]
	
	INT iPedCarryCount
	INT iVehCarryCount
	INT iObjCarryCount
	
	INT iPlayerScore
	INT iKillScore
	INT iMedalRankScore
	
	SCRIPT_TIMER tdMissionTime
	INT iMissionEndTime
	
	INT iRoundEndTime
	
	INT iReasonForPlayerFail
	
	INT iVehDeliveryId = -1

	INT iNumPlayerKills
	INT iNumHeadshots
	INT iStartHits
	INT iStartShots
	INT iNumPlayerDeaths
	INT iNumControlKills
	INT iNumPedKills
	INT iCivilianKills
	INT iVehDamage
	INT iPlayerDamage
	INT iVehDestroyed
	INT iAmbientCopsKilled
	INT iEndAccuracy = -1
	INT iEndHealthLoss = -1
	INT iStartStealthKills
	INT iStealthKills = -1
	FLOAT fKillDeathRatio
	FLOAT fDamageDealt
	FLOAT fDamageTaken
	INT iCashReward
	INT iRewardXP
	INT iWanted
	INT iFakeWanted
	INT iBet
	INT iHackTime
	INT iNumHacks
	INT iNumWantedLose
	INT iWantedTime
	INT iNumWantedStars
	INT iLatestHits
	INT iLatestShots
	INT iCriticalWeaponAmmo
	INT iAssists
	INT iPureKills //Kills without minus points
	
	INT iObjectiveTypeCompleted =-1
	
	INT iPedFollowingBitset[FMMC_MAX_PEDS_BITSET]
	INT iVehCapturing = -1
	INT iVehDestroyBitset
	
	INT iObjCapturing = -1
	
	INT iClientBitSet
	INT iClientBitSet2
	INT iClientBitSet3
	INT iClientBitSet4
	INT iClientBitSet5
	
	VECTOR vGangChaseSpawnCoords
	INT iGangChaseChosenType = -1
	INT iGangChaseSpawnBitset
	
	INT iCutsceneFinishedBitset
	
	INT iMedalObjectives = 0
	INT iMedalInteractables = 0
	INT iMedalEquipment = 0
	
	FLOAT fContainerOpenRatio = 0.0
	
	INT iSynchSceneID = -1
	NETWORK_INDEX netMiniGameObj
	NETWORK_INDEX netMiniGameObj2
	NETWORK_INDEX netMiniGameObj3
	
	INT iSyncAnimProgress

	ANIM_NEW_STAGE iSyncRoundCutsceneStage
	SKYSWOOP iSyncRoundPostCutsceneStage
	JOB_TEAM_CUT_STAGE iSyncRoundTeamCutsceneStage
	JOB_INTRO_CUT_STAGE iSyncRoundIntroCutsceneStage
	BOOL bSyncCutsceneDone

	#IF IS_DEBUG_BUILD
		BOOL bPressedF					
		BOOL bPressedS
	#ENDIF
	
	INT iOutfit = -1
	INT iStartingOutfit = -1 
	
	INT iHackMGDialogue = -1 // Dialogue that triggers the currently playing snake minigame for this player
	
	INT iRestartInPV = -1
	
	//Trip Skip Data
	TRIP_SKIP_PLAYER_BD TSPlayerData
	TRIP_SKIP_PLAYER_BD FTSPlayerData

	INT iDialoguePlayedBS[ciMAX_DIALOGUE_BIT_SETS]	 //Bitset that stores whether dialogue has been played or not
	
	INT iPartIAmSpectating = -1
	
	VECTOR vPositionAhead // Saves a safe position in front of the player (along their GPS route) for things to spawn
	
	INT iMusicState
	
	INT iBeastAlpha = -1
	
	INT iSuddenDeathBS
	SUMO_SUDDEN_DEATH_STAGE_BD eSumoSuddenDeathStage
		
	BOOL bIsOverSpeedThreshold					// [fmmc2020] refactor - Change to a ClientBitset
	BOOL bHasPlayerReceivedBattleEvent = FALSE	// [fmmc2020] refactor - Change to a ClientBitset
	BOOL bCelebrationScreenIsActive = FALSE		// [fmmc2020] refactor - Change to a ClientBitset
	BOOL bPlayedFirstDialogue					// [fmmc2020] refactor - Change to a ClientBitset
	
	BOOL bSwapTeamStarted = FALSE				// [fmmc2020] refactor - Change to a ClientBitset
	BOOL bSwapTeamFinished = FALSE				// [fmmc2020] refactor - Change to a ClientBitset
	
	FLOAT fCaptureTime
	
	INT iKillAllEnemiesPennedInUpdateTimer
	INT iKillAllEnemiesPennedInBS
	INT iAdditionalPlayerLives
	
	// Adversary Mode / Versus Mode specific stuff left over [FMMC2020] Refactor - Kept in it because it might have general use.
	INT iSlipstreamTime
	INT iCheckpointsPassed
	INT iBestLapTime
	INT iFastestLapOfMission	
	INT iDamageToJuggernaut

	INT iCurrentPropHit[ci_PLAYER_CLAIM_PROP_MAX]
	
	INT iRequestCapturePedBS[FMMC_MAX_PEDS_BITSET]
	INT iAlreadyBeenCapturingPedBS[FMMC_MAX_PEDS_BITSET]
	INT iRequestCaptureVehBS
	INT iAlreadyBeenCapturingVehBS
		
	INT iManualRespawnBitset
		
	INT iGranularObjCapturingBS
	INT iGranularCurrentPoints
	INT iGranularTotalPoints
	
	FLOAT fBombVehicleSpeed
	INT iMinSpeed_ExplodeProgress
	INT iLastVeh = -1
	FLOAT fCurrentAltitude
	FLOAT fCurrentAltitudeFailTime
	
	INT iPackagesAtHolding = 0
	INT iNumberOfDeliveries = 0
	INT iPersonalLocatePoints = 0
	
	INT iVehicleCurrentForRestart = -3			// [fmmc2020] refactor - Needs re-implementing
	INT iVehicleCurrentSeatForRestart = -3		// [fmmc2020] refactor - Needs re-implementing
	
	INT iHackingFails
	
	INT iCurrentHeistCompletionStat = -1
	INT iSpecificHeistCompletionStatBS[HBCA_BS_MISSION_CONTROLLER_MAX_LONG]
	
	// Zones
	INT iZoneIsTriggeredBS_Networked // Occasionally gets updated to match iZoneIsTriggeredBS to ensure we don't update PlayerBD too often.
	
	// Interactables
	INT iCurrentInteractables[ciINTERACTABLE_MAX_ONGOING_INTERACTIONS] // An array of indexes for the Interactables that the player is currently interacting with whether they be background or foreground Interactions
	INT iOngoingInteractionCount = 0
	INT iInteractablesLocallyCompletedBS[ciINTERACTABLE_LONG_BS_LENGTH]
	SYNCLOCK_INTERACTION_STATE eSyncLockInteractionState = SYNCLOCK_INTERACTION_STATE__OFF
	
	// Prerequisites
	INT iPrerequisiteBS[ciPREREQ_Bitsets]
		
	LOOT_BAG_STRUCT sLootBag
	
	INT iLocationInSecondaryLocation
	
	INT iWarpPortalEntered = -1
		
	INT iMyRaceModelChoice
	MODEL_NAMES mnMyRaceModel
	
	INT iCustomPedModelIndex = -1
	
	eARTIFICIAL_LIGHTS_STATE eLightsStateToUse
	
ENDSTRUCT

MC_PlayerBroadcastData MC_playerBD[MAX_NUM_MC_PLAYERS]

STRUCT MC_PlayerBroadcastData_1

	INT iInteractWithSyncedScene = -1
	NETWORK_INDEX niInteractWith_CurrentSpawnedProps[ciINTERACT_WITH__MAX_SPAWNED_PROPS]
	NETWORK_INDEX niInteractWith_PersistentProps[ciINTERACT_WITH__MAX_PERSISTENT_PROPS]
	INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE eiInteractWith_PlantExplosivesState
	INT iInteractWith_NumSpawnedProps = 0
	INT iInteractWith_NumPersistentProps = 0
	
	INT iDamageToPedsForMedal
	INT iDamageToVehsForMedal
		
	INT iLocBitset
	
	SCRIPT_TIMER tdComboTimer
	INT iStuntComboMultiplier=1
	
	MODEL_NAMES mnCoronaVehicle
	INT iDisplaySlot
	
	INT iObjectsReturned = 0
	eArenaSpawnFixStage eArenaSpawnStage
	INT iCarnageBarBS
	INT iVehicleOwnerNeededToRepair				= -1
	INT iVehicleNeededToRepair					= -1
	
	INT iExplosionDangerZoneInsideBS = 0
	
	INT iKeypadThermiteInRangeBS = 0
	INT iThermitePerPlayerRemaining = -1
	
	MP_HEIST_GEAR_ENUM eHeistGearBagType = HEIST_GEAR_SPORTS_BAG
	MODEL_NAMES mnHeistGearBag = HEI_P_M_BAG_VAR22_ARM_S	
	
	INT AltVars_BS[ciFMMC_ALT_VAR_MAX_BS]
	
	INT iProximitySpawning_PedSpawnRangeBS[FMMC_MAX_PEDS_BITSET]
	INT iProximitySpawning_PedCleanupRangeBS[FMMC_MAX_PEDS_BITSET]
	
	INT iProximitySpawning_DynoPropSpawnRangeBS
	INT iProximitySpawning_DynoPropCleanupRangeBS
	
	INT iProximitySpawning_VehSpawnRangeBS
	INT iProximitySpawning_VehCleanupRangeBS
	
	BOOL bPlayerAbilityIsActive[PA_MAX]
	
	INT iLobbyHostOwnedVehicle_Colour = -1
	INT iLobbyHostOwnedVehicle_Extra = -1 //Flag in the case of the submarine, can be reused for other things
	
	INT iDoorAnimDynopropInitialisedBS
		
	PED_COMPANION_TASK_DATA	sPedCompanionTaskData[FMMC_MAX_COMPANIONS]
	
	FLASH_FADE_STRUCT sFlashFade[MAX_FLASH_FADE_ENTITIES]
	PTFX_ID ptfxSmokeParticle
	CACHED_VEHICLE_STRUCT sCachedVehicleData
	NETWORK_INDEX niCachedVehiclesForCleanup[MAX_NUM_OF_CACHED_VEHICLES_FOR_CLEANUP]
	INT iNumOfCachedVehiclesForCleanup = 0
	INT iCachedPartNumInTeam = -1
ENDSTRUCT

MC_PlayerBroadcastData_1 MC_playerBD_1[MAX_NUM_MC_PLAYERS]

//HIGH PRIORITY PLAYERBD GOES HERE IF/WHEN WE HAVE A USE FOR IT



