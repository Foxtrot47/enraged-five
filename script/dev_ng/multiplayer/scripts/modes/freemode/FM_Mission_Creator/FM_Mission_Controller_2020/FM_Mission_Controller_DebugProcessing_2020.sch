// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Debug -------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: The aim of this header is to ensure that all debug processing functions, loops and calculations are contained within, and outside of the normal script files.   
// ##### The functions here should have access to call any function in script as well as main debug process functions which are declared inside of here.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD

USING "FM_Mission_Controller_EventProcessing_2020.sch"

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Audio --------------------------------------------------------------------------
// ##### Description: Debug functions for audio related things ----------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Cutscenes ----------------------------------------------------------------------
// ##### Description: Debug functions for cutscenes related things ------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Dialogue Trigger ---------------------------------------------------------------
// ##### Description: Debug functions for dialogue trigger related things -----------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Doors --------------------------------------------------------------------------
// ##### Description: Debug functions for door related things -----------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Ending -------------------------------------------------------------------------
// ##### Description: Debug functions for ending related things ---------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Events -------------------------------------------------------------------------
// ##### Description: Debug functions for event related things ----------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Init --------------------------------------------------------------------------
// ##### Description: Debug functions for init related things -----------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Proximity Spawning -------------------------------------------------------------
// ##### Description: Debug functions for Proximity Spawning ------------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC PROCESS_PROXIMITY_SPAWNING_DEBUG()
	
	IF iProxSpawnDebug_EntityTypePrevious != iProxSpawnDebug_EntityType
		iProxSpawnDebug_EntityIndex = 0
		fProxSpawnDebug_LocalDistance = 0.0
	ENDIF

	INT iSpawnRange, iCleanupRange, iPlayersRequired
	BOOL bSpawned, bCanSpawn, bShouldCleanup
	
	SWITCH iProxSpawnDebug_EntityType
	
		CASE ciPROX_SPAWNDEBUG_ENTITY_TYPE_NONE
		
			iProxSpawnDebug_EntityIndex = 0
			
			IF iProxSpawnDebug_EntityTypePrevious != iProxSpawnDebug_EntityType
				SET_CONTENTS_OF_TEXT_WIDGET(twidProxSpawnDebug_Settings, "")
				SET_CONTENTS_OF_TEXT_WIDGET(twidProxSpawnDebug_EntityStatus, "")
				SET_CONTENTS_OF_TEXT_WIDGET(twidProxSpawnDebug_ProxSpawnStatus, "")
			ENDIF
			
			iProxSpawnDebug_EntityTypePrevious = iProxSpawnDebug_EntityType
			EXIT
			
		BREAK
		
		CASE ciPROX_SPAWNDEBUG_ENTITY_TYPE_PEDS
			
			IF iProxSpawnDebug_EntityIndex >= MC_serverBD.iNumPedCreated
				iProxSpawnDebug_EntityIndex = MC_serverBD.iNumPedCreated - 1
			ENDIF
			
			IF iProxSpawnDebug_EntityIndex < 0
				iProxSpawnDebug_EntityIndex = 0
			ENDIF
			
			iSpawnRange = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iProxSpawnDebug_EntityIndex].sProximitySpawningData.iSpawnRange
			iCleanupRange = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iProxSpawnDebug_EntityIndex].sProximitySpawningData.iCleanupRange
			iPlayersRequired = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iProxSpawnDebug_EntityIndex].sProximitySpawningData.iRequiredPlayers
			
			IF bLocalPlayerPedOk
				fProxSpawnDebug_LocalDistance = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, MC_GET_PED_SPAWN_LOCATION_FOR_PROXIMITY_SPAWNING(iProxSpawnDebug_EntityIndex))
			ELSE
				fProxSpawnDebug_LocalDistance = 0.0
			ENDIF
			
			bSpawned = NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iProxSpawnDebug_EntityIndex])
			bCanSpawn = NOT IS_PLAYER_PROXIMITY_BLOCKING_SPAWN_ARRAYED(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iProxSpawnDebug_EntityIndex].sProximitySpawningData, iProxSpawnDebug_EntityIndex, MC_serverBD_3.iProximitySpawning_PedSpawnAllBS, MC_serverBD_3.iProximitySpawning_PedSpawnAnyBS, MC_serverBD_3.iProximitySpawning_PedSpawnOneBS, MC_serverBD_3.iProximitySpawning_PedCleanupBS)
			bShouldCleanup = SHOULD_CLEANUP_DUE_TO_PLAYER_PROXIMITY_ARRAYED(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iProxSpawnDebug_EntityIndex].sProximitySpawningData, iProxSpawnDebug_EntityIndex, MC_serverBD_3.iProximitySpawning_PedCleanupBS)

		BREAK
		
		CASE ciPROX_SPAWNDEBUG_ENTITY_TYPE_DYNOPROPS
			
			IF iProxSpawnDebug_EntityIndex >= g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps
				iProxSpawnDebug_EntityIndex = g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps - 1
			ENDIF
			
			IF iProxSpawnDebug_EntityIndex < 0
				iProxSpawnDebug_EntityIndex = 0
			ENDIF
			
			iSpawnRange = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iProxSpawnDebug_EntityIndex].sProximitySpawningData.iSpawnRange
			iCleanupRange = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iProxSpawnDebug_EntityIndex].sProximitySpawningData.iCleanupRange
			iPlayersRequired = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iProxSpawnDebug_EntityIndex].sProximitySpawningData.iRequiredPlayers
			
			IF bLocalPlayerPedOk
				fProxSpawnDebug_LocalDistance = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iProxSpawnDebug_EntityIndex].vPos)
			ELSE
				fProxSpawnDebug_LocalDistance = 0.0
			ENDIF
			
			bSpawned = NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_DYNOPROP_NET_ID(iProxSpawnDebug_EntityIndex))
			bCanSpawn = NOT IS_PLAYER_PROXIMITY_BLOCKING_SPAWN(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iProxSpawnDebug_EntityIndex].sProximitySpawningData, iProxSpawnDebug_EntityIndex, MC_serverBD_3.iProximitySpawning_DynoPropSpawnAllBS, MC_serverBD_3.iProximitySpawning_DynoPropSpawnAnyBS, MC_serverBD_3.iProximitySpawning_DynoPropSpawnOneBS, MC_serverBD_3.iProximitySpawning_DynoPropCleanupBS)
			bShouldCleanup = SHOULD_CLEANUP_DUE_TO_PLAYER_PROXIMITY(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iProxSpawnDebug_EntityIndex].sProximitySpawningData, iProxSpawnDebug_EntityIndex, MC_serverBD_3.iProximitySpawning_DynoPropCleanupBS)
			
		BREAK
		
		CASE ciPROX_SPAWNDEBUG_ENTITY_TYPE_VEHICLES
			
			IF iProxSpawnDebug_EntityIndex >= g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
				iProxSpawnDebug_EntityIndex = g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1
			ENDIF
			
			IF iProxSpawnDebug_EntityIndex < 0
				iProxSpawnDebug_EntityIndex = 0
			ENDIF
			
			iSpawnRange = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iProxSpawnDebug_EntityIndex].sProximitySpawningData.iSpawnRange
			iCleanupRange = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iProxSpawnDebug_EntityIndex].sProximitySpawningData.iCleanupRange
			iPlayersRequired = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iProxSpawnDebug_EntityIndex].sProximitySpawningData.iRequiredPlayers
			
			IF bLocalPlayerPedOk
				fProxSpawnDebug_LocalDistance = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, MC_GET_VEH_SPAWN_LOCATION_FOR_PROXIMITY_SPAWNING(iProxSpawnDebug_EntityIndex))
			ELSE
				fProxSpawnDebug_LocalDistance = 0.0
			ENDIF
			
			bSpawned = NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iProxSpawnDebug_EntityIndex])
			bCanSpawn = NOT IS_PLAYER_PROXIMITY_BLOCKING_SPAWN(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iProxSpawnDebug_EntityIndex].sProximitySpawningData, iProxSpawnDebug_EntityIndex, MC_serverBD_3.iProximitySpawning_VehSpawnAllBS, MC_serverBD_3.iProximitySpawning_VehSpawnAnyBS, MC_serverBD_3.iProximitySpawning_VehSpawnOneBS, MC_serverBD_3.iProximitySpawning_VehCleanupBS)
			bShouldCleanup = SHOULD_CLEANUP_DUE_TO_PLAYER_PROXIMITY(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iProxSpawnDebug_EntityIndex].sProximitySpawningData, iProxSpawnDebug_EntityIndex, MC_serverBD_3.iProximitySpawning_VehCleanupBS)
			
		BREAK
		
	ENDSWITCH
	
	TEXT_LABEL_63 tl63 = ""
	
	IF iSpawnRange = 0
		tl63 += "None"
		SET_CONTENTS_OF_TEXT_WIDGET(twidProxSpawnDebug_Settings, tl63)
		SET_CONTENTS_OF_TEXT_WIDGET(twidProxSpawnDebug_EntityStatus, "")
		SET_CONTENTS_OF_TEXT_WIDGET(twidProxSpawnDebug_ProxSpawnStatus, "")
		fProxSpawnDebug_LocalDistance = 0.0
	ELSE
		tl63 += "Spawn: "
		tl63 += iSpawnRange
		tl63 += "m, Cleanup: "
		tl63 += iCleanupRange
		tl63 += "m, Players: "
		IF iPlayersRequired = ciPROXIMITY_SPAWNING_REQUIRED_PLAYERS_ANY
			tl63 += "ANY"
		ELIF iPlayersRequired = ciPROXIMITY_SPAWNING_REQUIRED_PLAYERS_ALL
			tl63 += "ALL"
		ELIF iPlayersRequired = ciPROXIMITY_SPAWNING_REQUIRED_PLAYERS_MULTIPLE
			tl63 += "MULTI"
		ENDIF
		SET_CONTENTS_OF_TEXT_WIDGET(twidProxSpawnDebug_Settings, tl63)
		
		IF bSpawned
			SET_CONTENTS_OF_TEXT_WIDGET(twidProxSpawnDebug_EntityStatus, "Spawned")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(twidProxSpawnDebug_EntityStatus, "Not Spawned")
		ENDIF
		
		tl63 = "Spawn: "
		IF bCanSpawn
			tl63 += "Yes"
		ELSE
			tl63 += "No"
		ENDIF
		tl63 += ", Cleanup: "
		IF bShouldCleanup
			tl63 += "Yes"
		ELSE
			tl63 += "No"
		ENDIF
		SET_CONTENTS_OF_TEXT_WIDGET(twidProxSpawnDebug_ProxSpawnStatus, tl63)
	ENDIF
	
	iProxSpawnDebug_EntityTypePrevious = iProxSpawnDebug_EntityType
	
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Local Player -------------------------------------------------------------------------
// ##### Description: Debug functions for local player related things -----------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------


// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Main -------------------------------------------------------------------------
// ##### Description: Debug functions for main related things -----------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC PHONE_EMP_DEBUG()	

	IF NOT IS_PHONE_EMP_AVAILABLE()
		EXIT
	ENDIF
	
	IF NOT IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD1)
		EXIT
	ENDIF

	TEXT_LABEL_63 tlDebugText
	
	tlDebugText = "ePhoneEMPState: "
	tlDebugText += ENUM_TO_INT(ePhoneEMPState)
	DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.35, 0.35, 0.5>>, 0, 255, 255)
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: ObjectMinigames ----------------------------------------------------------------
// ##### Description: Debug functions for minigame related things -------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Players --------------------------------------------------------------------------
// ##### Description: Debug functions for player related things -----------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC MAINTAIN_PLAYER_ID_CHANGE_CHECK()
	IF LocalPlayer != PLAYER_ID()
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] MAINTAIN_PLAYER_ID_CHANGE_CHECK - LocalPlayer != PLAYER_ID()")
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] LocalPlayer: ", NATIVE_TO_INT(LocalPlayer))
		PRINTLN("[RCC MISSION] PLAYER_ID(): ", NATIVE_TO_INT(PLAYER_ID()))
		PRINTLN("[RCC MISSION] player model: ",GET_MODEL_NAME_FOR_DEBUG(GET_PLAYER_MODEL()))
		ASSERTLN("This is very BAD - [RCC MISSION] - LocalPlayer != PLAYER_ID()")
	ENDIF
	
	IF LocalPlayerPed != PLAYER_PED_ID()
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] MAINTAIN_PLAYER_ID_CHANGE_CHECK - LocalPlayerPed != PLAYER_PED_ID()")
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] LocalPlayerPed : ", NATIVE_TO_INT(LocalPlayerPed))
		PRINTLN("[RCC MISSION] PLAYER_PED_ID(): ", NATIVE_TO_INT(PLAYER_PED_ID()))
		PRINTLN("[RCC MISSION] player model: ",GET_MODEL_NAME_FOR_DEBUG(GET_PLAYER_MODEL()))
		ASSERTLN("This is very BAD - [RCC MISSION] - LocalPlayerPed != PLAYER_PED_ID()")
	ENDIF
	
	IF iLocalPart != PARTICIPANT_ID_TO_INT()
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] MAINTAIN_PLAYER_ID_CHANGE_CHECK - iLocalPart != PARTICIPANT_ID_TO_INT()")
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] iLocalPart             : ", iLocalPart)
		PRINTLN("[RCC MISSION] PARTICIPANT_ID_TO_INT(): ", PARTICIPANT_ID_TO_INT())
		ASSERTLN("This is very BAD - [RCC MISSION] - iLocalPart != PARTICIPANT_ID_TO_INT()")
	ENDIF
ENDPROC


// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Rules -------------------------------------------------------------------------
// ##### Description: Debug functions for rule related things -----------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC MAINTAIN_MISSION_RESTART_DEBUG()
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_Z, KEYBOARD_MODIFIER_CTRL_SHIFT, "NO")
		PRINTLN("[JS] CRTL + SHIFT + Z PRESSED - RESTARTING MISSION")
		g_debugDisplayState = DEBUG_DISP_REPLAY
		g_SkipCelebAndLbd = TRUE
	ENDIF
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Vehicle -------------------------------------------------------------------------
// ##### Description: Debug functions for vehicle related things -----------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC PROCESS_BURNING_VEHICLE_DEBUG()
	
	IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_PROCESS_BURNING_VANS)
		EXIT
	ENDIF
	
	IF NOT IS_DEBUG_KEY_JUST_PRESSED(KEY_V, KEYBOARD_MODIFIER_CTRL_SHIFT, "Water Bomb Vehicles")
		EXIT
	ENDIF
	
	INT iVeh
	FOR iVeh = 0 TO MC_serverBD.iNumVehCreated - 1
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_SPAWN_BURNING)
			RELOOP
		ENDIF
	
		IF NOT IS_BIT_SET(MC_serverBD_4.sBurningVehicle.iBurningVehiclePTFXBitset, iVeh)
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD_4.sBurningVehicle.iBurningVehicleExtinguishedBitset, iVeh)
			RELOOP
		ENDIF
		
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			RELOOP
		ENDIF
		
		VEHICLE_INDEX viVehicle = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
		IF NOT IS_VEHICLE_DRIVEABLE(viVehicle)
			RELOOP
		ENDIF
		
		PRINTLN("[BURNING_VEHICLE] PROCESS_BURNING_VEHICLE_DEBUG - Vehicle: ", iVeh, " being debug water bombed.")
		ADD_EXPLOSION(GET_ENTITY_COORDS(viVehicle), EXP_TAG_BOMB_WATER, 1.0)
		                
	ENDFOR
	
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Zones --------------------------------------------------------------------------
// ##### Description: Debug functions for zone related things -----------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: MissionVariation Debug Functions --------------------------------------------------------------------------------------------------------------------------------
// ##### Description: All of the Debug functions called by the mission variations system.																		------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Widgets ------------------------------------------------------------------------
// ##### Description: Debug functions widgets ---------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC CREATE_MC_WIDGETS()

	INT i
	TEXT_LABEL_31 dummy
	
	FMMC_WIDGET_GROUP = START_WIDGET_GROUP( " Mission Controller") 
		SET_BEAM_HACK_PARENT_WIDGET_GROUP(FMMC_WIDGET_GROUP)
		SET_HOTWIRE_PARENT_WIDGET_GROUP(FMMC_WIDGET_GROUP)
		ADD_WIDGET_BOOL("bOutputDebug", boutputdebugspam)
		ADD_WIDGET_BOOL("Stop undriveable vehicle cleanup:", bStopVehDeadCleanup)
		ADD_WIDGET_BOOL("bDisableDeathsCount:", bDisableDeathsCount)
		ADD_WIDGET_BOOL("Add ped to group prints",bAddToGroupPrints)
		ADD_WIDGET_BOOL("Go to locate prints",bGotoLocPrints)
		ADD_WIDGET_BOOL("Plane dropoff prints",bPlaneDropoffPrints)

		START_WIDGET_GROUP("Profiler")
			ADD_WIDGET_FLOAT_SLIDER("Pos Y", fProfilerPosY, 0, 1, 0.0001)
			ADD_WIDGET_FLOAT_SLIDER("Scale X", fProfilerScaleX, 0, 1, 0.0001)
			ADD_WIDGET_FLOAT_SLIDER("Scale Y", fProfilerScaleY, 0, 1, 0.0001)
			ADD_WIDGET_INT_SLIDER("Alpha", iProfilerBGAlpha, 0, 255, 1)
			
			ADD_WIDGET_FLOAT_SLIDER("fProfilerTextPaddingX", fProfilerTextPaddingX, 0, 1, 0.0001)
			ADD_WIDGET_FLOAT_SLIDER("fProfilerTextPaddingY", fProfilerTextPaddingY, 0, 1, 0.0001)
			ADD_WIDGET_FLOAT_SLIDER("fProfilerTextSpacingY", fProfilerTextSpacingY, 0, 1, 0.0001)
			ADD_WIDGET_FLOAT_SLIDER("fProfilerTextTitleSpacingY", fProfilerTextTitleSpacingY, 0, 1, 0.0001)
			ADD_WIDGET_FLOAT_SLIDER("fProfilerTextScale", fProfilerTextScale, 0, 1, 0.0001)
			ADD_WIDGET_FLOAT_SLIDER("fProfilerTextTitleScale", fProfilerTextTitleScale, 0, 1, 0.0001)
			ADD_WIDGET_FLOAT_SLIDER("fProfilerBarHeight", fProfilerBarHeight, 0, 1, 0.0001)
			
			ADD_WIDGET_FLOAT_SLIDER("fProfilerBarIndicatorHeight", fProfilerBarIndicatorHeight, 0, 1, 0.0001)
			ADD_WIDGET_FLOAT_SLIDER("fProfilerBarIndicatorWidth", fProfilerBarIndicatorWidth, 0, 1, 0.0001)
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("ProxSpawnDebug")
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("None")
				ADD_TO_WIDGET_COMBO("Peds")
				ADD_TO_WIDGET_COMBO("DynoProps")				
				ADD_TO_WIDGET_COMBO("Vehicles")
			STOP_WIDGET_COMBO("Entity Type: ", iProxSpawnDebug_EntityType)
			
			ADD_WIDGET_INT_SLIDER("Entity Index: ", iProxSpawnDebug_EntityIndex, 0, FMMC_MAX_PEDS, 1) //Update if we use an entity with a higher cap than peds
			
			twidProxSpawnDebug_Settings = ADD_TEXT_WIDGET("Settings: ")
			twidProxSpawnDebug_EntityStatus = ADD_TEXT_WIDGET("Entity Status: ")
			twidProxSpawnDebug_ProxSpawnStatus = ADD_TEXT_WIDGET("ProxSpawn Status: ")
			SET_CONTENTS_OF_TEXT_WIDGET(twidProxSpawnDebug_Settings, "")
			SET_CONTENTS_OF_TEXT_WIDGET(twidProxSpawnDebug_EntityStatus, "")
			SET_CONTENTS_OF_TEXT_WIDGET(twidProxSpawnDebug_ProxSpawnStatus, "")
			
			ADD_WIDGET_FLOAT_READ_ONLY("Local Player Distance: ", fProxSpawnDebug_LocalDistance)
		
			START_WIDGET_GROUP("Extra Debugging")
			
				ADD_WIDGET_BOOL("Force All Req 1 Player", bProxSpawnDebug_ForcePlayersReqAny)

				ADD_WIDGET_BOOL("Enable Player Position Spoofing", bProxSpawnDebug_EnablePlayerPositionSpoofing)
				
				ADD_WIDGET_VECTOR_SLIDER("Player 1 Override", vProxSpawnDebug_PlayerOverride[0], -16000, 16000, 0.5)
				ADD_WIDGET_BOOL("Set to Local Player pos (1)", bProxSpawnDebug_PlayerOverrideUpdate[0])
				
				ADD_WIDGET_VECTOR_SLIDER("Player 2 Override", vProxSpawnDebug_PlayerOverride[1], -16000, 16000, 0.5)
				ADD_WIDGET_BOOL("Set to Local Player pos (2)", bProxSpawnDebug_PlayerOverrideUpdate[1])
			
				ADD_WIDGET_VECTOR_SLIDER("Player 3 Override", vProxSpawnDebug_PlayerOverride[2], -16000, 16000, 0.5)
				ADD_WIDGET_BOOL("Set to Local Player pos (3)", bProxSpawnDebug_PlayerOverrideUpdate[2])
			
				ADD_WIDGET_VECTOR_SLIDER("Player 4 Override", vProxSpawnDebug_PlayerOverride[3], -16000, 16000, 0.5)
				ADD_WIDGET_BOOL("Set to Local Player pos (4)", bProxSpawnDebug_PlayerOverrideUpdate[3])
			
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_BOOL("Hide Objects", bHideObjects)
		
		ADD_WIDGET_BOOL("Turn on Damage Tracking for all vehicle Network IDs", bEnableDamageTrackerOnNetworkID)
		
		ADD_WIDGET_BOOL( "Draw Respawn Point Debug ", bCustomRespawnPointsDebug)
		
		ADD_WIDGET_BOOL( "Pause Rule and MultiRule Timers ", bWdPauseGameTimerRag)
		
		ADD_WIDGET_BOOL( "Extra Leaderboard Prints", bEnableExtraLeaderboardPrints)
		
		START_WIDGET_GROUP("Reusable")
			ADD_WIDGET_VECTOR_SLIDER("vRagPlacementVector", vRagPlacementVector, -5, 5, 0.005)
			
			ADD_WIDGET_VECTOR_SLIDER("vRagRotationVector", vRagRotationVector, -180, 180, 0.005)
			
			ADD_WIDGET_FLOAT_SLIDER("Interact-With Hit Camera Shake Scalar", fInteractWithHitShakeScalar, 0.0, 5.0, 0.005)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Interact-With Camera")
			ADD_WIDGET_BOOL("Disable Interact-With Camera", bDisableIWCam)
			ADD_WIDGET_VECTOR_SLIDER("Camera Offset", vIWCamOffsetOverride, -100, 100, 0.05)
			ADD_WIDGET_VECTOR_SLIDER("Camera Rotation", vIWCamRotOverride, -180, 180, 0.05)
		STOP_WIDGET_GROUP()
			
		START_WIDGET_GROUP( " Leaderboard")
		    FOR i = 0 TO (MAX_NUM_DM_PLAYERS - 1)
		          dummy = "LB "
		          dummy += i
		          START_WIDGET_GROUP(dummy)
		                ADD_WIDGET_INT_READ_ONLY("iParticipant", g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant)
		                ADD_WIDGET_INT_READ_ONLY("iTeam", g_MissionControllerserverBD_LB.sleaderboard[i].iTeam)
		                ADD_WIDGET_INT_READ_ONLY("iTeamScore",g_MissionControllerserverBD_LB.sleaderboard[i].iTeamScore)
						ADD_WIDGET_INT_READ_ONLY("iplayerScore", g_MissionControllerserverBD_LB.sleaderboard[i].iPlayerScore)                                   
		                ADD_WIDGET_INT_READ_ONLY("iKills", g_MissionControllerserverBD_LB.sleaderboard[i].iKills)                                   
		                ADD_WIDGET_INT_READ_ONLY("iHeadshots", g_MissionControllerserverBD_LB.sleaderboard[i].iHeadshots)
		                ADD_WIDGET_INT_READ_ONLY("iDeaths", g_MissionControllerserverBD_LB.sleaderboard[i].iDeaths)
		                ADD_WIDGET_INT_READ_ONLY("iRank", g_MissionControllerserverBD_LB.sleaderboard[i].iRank)
		          STOP_WIDGET_GROUP()
		    ENDFOR
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Player In Drop Off")
			ADD_WIDGET_BOOL("Do Player In Drop Off Prints", bPlayerInDropOffPrints)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Ped Count")
			ADD_WIDGET_BOOL("Do Count Prints", bDoPedCountPrints)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Round Lbd")
			 ADD_WIDGET_INT_READ_ONLY("MC_ServerBroadcastData3.iRoundLbdProgress", MC_serverBD_3.iRoundLbdProgress) 
			 ADD_WIDGET_INT_READ_ONLY("iRoundsLbdStage", iRoundsLbdStage)
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("Leaderboard Data")		
			ADD_WIDGET_INT_READ_ONLY("iLBPlayerSelection", iLBPlayerSelection)
			ADD_WIDGET_INT_SLIDER("g_i_ActualLeaderboardPlayerSelected", g_i_ActualLeaderboardPlayerSelected, -100, 9999, 1)
			ADD_WIDGET_INT_SLIDER("g_i_LeaderboardRowCounter", g_i_LeaderboardRowCounter, -100, 9999, 1)
			ADD_WIDGET_INT_SLIDER("g_i_LeaderboardShift", g_i_LeaderboardShift, -100, 9999, 1)
			ADD_WIDGET_INT_READ_ONLY("iNumSpectators", MC_serverBD.iNumSpectators)
			ADD_WIDGET_INT_READ_ONLY("iNumActiveTeams", MC_serverBD.iNumActiveTeams)
			ADD_WIDGET_INT_READ_ONLY("iNumberOfLBPlayers[0]", MC_serverBD.iNumberOfLBPlayers[0])
		STOP_WIDGET_GROUP()
	  
		START_WIDGET_GROUP("XP and CASH") 
			ADD_WIDGET_INT_READ_ONLY("Total Mission Cash", iDebugRewardCash)
			ADD_WIDGET_INT_READ_ONLY("CASH from pickup ", iDebugCashPickup)
			ADD_WIDGET_INT_READ_ONLY("XP deliver Vehicle", iDebugXPDelVeh)
			ADD_WIDGET_INT_READ_ONLY("XP for killing all enemies", iAllKillXPBonus)
			ADD_WIDGET_INT_READ_ONLY("XP for you only headshot", iAllHeadshotXPPlayer)
			ADD_WIDGET_INT_READ_ONLY("XP for team only headshot", iAllHeadshotXPTeam)
			ADD_WIDGET_INT_READ_ONLY("Mission reward XP", iDebugTotalRewardXP)
			ADD_WIDGET_INT_READ_ONLY("XP Lives Bonus", iDebugXPLivesBonus)
			ADD_WIDGET_INT_READ_ONLY("FINAL XP given", iDebugXPGiven)
			ADD_WIDGET_INT_READ_ONLY("accepted invite XP", iDebugXPInvite)
			ADD_WIDGET_INT_READ_ONLY("daily win XP", iDebugXPDailyWin)
			ADD_WIDGET_INT_READ_ONLY("Playerd 5 missions XP", iDebugXP5Plays)
		STOP_WIDGET_GROUP()
		
		start_widget_group("LBD_CS")
			ADD_WIDGET_BOOL("bLBDHold: ", bLBDHold)
			add_widget_float_slider("fRoundsOffset", fRoundsOffset, -2000.0, 2000.0, 0.1)
		STOP_WIDGET_GROUP()

		START_WIDGET_GROUP("Gang Chase")
			ADD_WIDGET_BOOL("Gang Chase Debug", bWdGangChaseDebug)
			ADD_WIDGET_BOOL("Lose gang chase debug", bWdLoseGangBackupDebug)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Seat Preference")
			ADD_WIDGET_BOOL("Seat Preference Debug", bWdSeatPreferenceDebug)
		STOP_WIDGET_GROUP()

		START_WIDGET_GROUP("Drop off Marker")
			ADD_WIDGET_BOOL("Drop Off marker debug", bWdDropOffMarker)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Multi rule timer")
			ADD_WIDGET_BOOL("Debug multi-rule timer", bWdMultiTimer)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Minimap")
			ADD_WIDGET_FLOAT_SLIDER("X Offset", fMinimapXOffset, -50.0, 50.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("X Offset", fMinimapYOffset, -50.0, 50.0, 0.1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP( "Script Spammy Prints" )
			ADD_WIDGET_BOOL( "Enable Spammy Prints", bEnableScriptAudioSpammyPrints )
			ADD_WIDGET_BOOL( "Enable Blip Prints", bDebugBlipPrints )
			ADD_WIDGET_BOOL( "Enable Bounds Prints", bDebugBoundsPrints )	
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Placed Entities")
			ADD_WIDGET_BOOL("Placed Vehicle Health & Info", bPlacedMissionVehiclesDebugHealth)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Abilities")
			ADD_WIDGET_BOOL("Show Abilities Debug", sLocalPlayerAbilities.bShowAbilityDebug)
			
			ADD_WIDGET_BOOL("Skip Ability Cooldowns", sLocalPlayerAbilities.bDebugSkipCooldowns)
			
			ADD_WIDGET_BOOL("Ignore Ability Prerequisites", sLocalPlayerAbilities.bDebugIgnorePrereqs)
			
			START_WIDGET_GROUP("Availability")
				ADD_WIDGET_BOOL("Make Air Strike Available", sLocalPlayerAbilities.sAbilities[PA_AIR_STRIKE].bDebugAvailable)
				ADD_WIDGET_BOOL("Make Heavy Loadout Available", sLocalPlayerAbilities.sAbilities[PA_HEAVY_LOADOUT].bDebugAvailable)
				ADD_WIDGET_BOOL("Make Heli Backup Available", sLocalPlayerAbilities.sAbilities[PA_HELI_BACKUP].bDebugAvailable)
				ADD_WIDGET_BOOL("Make Recon Drone Available", sLocalPlayerAbilities.sAbilities[PA_RECON_DRONE].bDebugAvailable)			
				ADD_WIDGET_BOOL("Make Support Sniper Available", sLocalPlayerAbilities.sAbilities[PA_SUPPORT_SNIPER].bDebugAvailable)
				ADD_WIDGET_BOOL("Make Vehicle Repair Available", sLocalPlayerAbilities.sAbilities[PA_VEHICLE_REPAIR].bDebugAvailable)
				ADD_WIDGET_BOOL("Make Ride Along Available", sLocalPlayerAbilities.sAbilities[PA_RIDE_ALONG].bDebugAvailable)
				ADD_WIDGET_BOOL("Make Ambush Available", sLocalPlayerAbilities.sAbilities[PA_AMBUSH].bDebugAvailable)
				ADD_WIDGET_BOOL("Make Distraction Available", sLocalPlayerAbilities.sAbilities[PA_DISTRACTION].bDebugAvailable)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Activation")
				ADD_WIDGET_BOOL("Activate Air Strike", sLocalPlayerAbilities.sAbilities[PA_AIR_STRIKE].bDebugActivate)
				ADD_WIDGET_BOOL("Activate Heavy Loadout", sLocalPlayerAbilities.sAbilities[PA_HEAVY_LOADOUT].bDebugActivate)
				ADD_WIDGET_BOOL("Activate Heli Backup", sLocalPlayerAbilities.sAbilities[PA_HELI_BACKUP].bDebugActivate)
				ADD_WIDGET_BOOL("Activate Recon Drone", sLocalPlayerAbilities.sAbilities[PA_RECON_DRONE].bDebugActivate)			
				ADD_WIDGET_BOOL("Activate Support Sniper", sLocalPlayerAbilities.sAbilities[PA_SUPPORT_SNIPER].bDebugActivate)
				ADD_WIDGET_BOOL("Activate Vehicle Repair", sLocalPlayerAbilities.sAbilities[PA_VEHICLE_REPAIR].bDebugActivate)
				ADD_WIDGET_BOOL("Activate Ride Along", sLocalPlayerAbilities.sAbilities[PA_RIDE_ALONG].bDebugActivate)
				ADD_WIDGET_BOOL("Activate Ambush", sLocalPlayerAbilities.sAbilities[PA_AMBUSH].bDebugActivate)
				ADD_WIDGET_BOOL("Activate Distraction", sLocalPlayerAbilities.sAbilities[PA_DISTRACTION].bDebugActivate)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Support Sniper")
				ADD_WIDGET_BOOL("Show Sniper Debug", sLocalPlayerAbilities.sSupportSniperData.bShowSniperAbilityDebug)
				ADD_WIDGET_BOOL("Target acquired", sLocalPlayerAbilities.sSupportSniperData.bHasValidTargetDebug)				
				ADD_WIDGET_BOOL("Player Spotted", sLocalPlayerAbilities.sSupportSniperData.bPlayerSpottedDebug)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Heavy Loadout")
				ADD_WIDGET_BOOL("Show Heavy Loadout Debug", sLocalPlayerAbilities.sHeavyLoadoutData.bShowHeavyLoadoutDebug)
			STOP_WIDGET_GROUP()
						
		STOP_WIDGET_GROUP()
		
		IF CONTENT_IS_USING_ARENA()
			START_WIDGET_GROUP("Arena")
				ADD_WIDGET_BOOL("g_bMissionOver", g_bMissionOver)
				ADD_WIDGET_INT_SLIDER("iDisplaySlot", MC_playerBD_1[iLocalPart].iDisplaySlot, 0, 10000, 1)
				CREATE_FMMC_ARENA_WIDGETS()
			STOP_WIDGET_GROUP()
		ENDIF
		
		IF BAG_CAPACITY__IS_ENABLED()
			START_WIDGET_GROUP("Bag Capacity")
				ADD_WIDGET_BOOL("Enable On-Screen Debug", bBagCapacityOnScreenDebug)
				ADD_WIDGET_FLOAT_SLIDER("Cash Weight", ciBAG_CAPACITY_WEIGHT__CASH, 0.0, 1000.0, 10.0)
				ADD_WIDGET_FLOAT_SLIDER("Coke Weight", ciBAG_CAPACITY_WEIGHT__COKE, 0.0, 1000.0, 10.0)
				ADD_WIDGET_FLOAT_SLIDER("Weed Weight", ciBAG_CAPACITY_WEIGHT__WEED, 0.0, 1000.0, 10.0)
				ADD_WIDGET_FLOAT_SLIDER("Gold Weight", ciBAG_CAPACITY_WEIGHT__GOLD, 0.0, 1000.0, 10.0)
				ADD_WIDGET_FLOAT_SLIDER("Painting Weight", ciBAG_CAPACITY_WEIGHT__PAINTING, 0.0, 1000.0, 10.0)
			STOP_WIDGET_GROUP()
		ENDIF
		
		START_WIDGET_GROUP("Local Player Outfit")
			ADD_WIDGET_INT_READ_ONLY("Current Outfit: ", MC_playerBD[iLocalPart].iOutfit)
			ADD_WIDGET_INT_READ_ONLY("Cached Outfit: ", iCachedPlayerOutfit)
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("None")
				ADD_TO_WIDGET_COMBO("Bugstar")
				ADD_TO_WIDGET_COMBO("Maintenance")
				ADD_TO_WIDGET_COMBO("Gruppe Sechs")
				ADD_TO_WIDGET_COMBO("Celebrity")
				ADD_TO_WIDGET_COMBO("FireFighter")
				ADD_TO_WIDGET_COMBO("N.O.O.S.E")
				ADD_TO_WIDGET_COMBO("High Roller")
				ADD_TO_WIDGET_COMBO("Island Guard")
				ADD_TO_WIDGET_COMBO("Island Smuggler")
			STOP_WIDGET_COMBO("Debug Override: ", iDebug_OutfitValueOverride)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Ped Goto Drawing	---------------------------------------------------------------
// ##### Description: Drawing of mission controller debug window --------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC DEBUG_DRAW_PED_CURRENT_ASSOCIATED_GOTO_POINTS(ASSOCIATED_GOTO_TASK_DATA &sAssociatedGotoTaskData, INT iPed, INT iPoolOverrideIndex, VECTOR vOriginPos, INT iCurrentGoToLocation)
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	INT iNumberOfPlacedGotos = GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(iPoolOverrideIndex, sAssociatedGotoTaskData)	
	FLOAT fTempx, fTempy
	
	FLOAT fSine = (150*GET_ANIMATED_SINE_VALUE(10.0, 0.5))
	INT iSine = ROUND(fSine)
	INT iNumberRed, iNumberGreen, iNumberBlue, iNumberAlpha, iCylinderRed, iCylinderGreen, iCylinderBlue, iCylinderAlpha
	
	iNumberRed 		= 100 + iSine
	iNumberGreen 	= 100 + iSine
	iNumberBlue		= 100 + iSine
	iNumberAlpha 	= 100 + iSine
	
	IF iPoolOverrideIndex > -1
		iCylinderRed	= 120 + PICK_INT(TRUE, (iSine/5), 0)
		iCylinderBlue	= 110 + PICK_INT(TRUE, (iSine/5), 0)
		iCylinderGreen 	= 30 + PICK_INT(TRUE, (iSine/5), 0)
		iCylinderAlpha 	= 200
	ELSE		
		iCylinderRed 	= 0 + PICK_INT(TRUE, (iSine/5), 0)
		iCylinderBlue 	= 175 + PICK_INT(TRUE, (iSine/5), 0)
		iCylinderGreen 	= 0 + PICK_INT(TRUE, (iSine/5), 0)
		iCylinderAlpha 	= 200
	ENDIF
		
	INT j, iR, iG, iB
	FOR j = 0 TO iNumberOfPlacedGotos - 1
		VECTOR vPos = GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(iPoolOverrideIndex, sAssociatedGotoTaskData, j)
				
		IF j = iCurrentGoToLocation			
			IF GET_SCREEN_COORD_FROM_WORLD_COORD(vPos, fTempx, fTempy)
			OR GET_SCREEN_COORD_FROM_WORLD_COORD(vOriginPos, fTempx, fTempy)
				DRAW_LINE(vOriginPos + <<0,0,0.5>>, vPos + <<0,0,0.5>>, 0, 255, 0, 125)
			ENDIF
			
			DRAW_MARKER(MARKER_ARROW, vPos + (<<0, 0, 3.0>>), (<<0, 0, 0.0>>), (<<180, 0, 90>>), (<<1.5, 1.5, 1.5>>), 0, 255, 0, 75)
		ENDIF
		
		IF GET_SCREEN_COORD_FROM_WORLD_COORD(vPos, fTempx, fTempy)
			DRAW_MARKER(MARKER_CYLINDER, vPos + (<<0, 0, -0.35>>), <<0,0,0>>, <<0,0,0>>, <<1.5,1.5,1.5>>, iCylinderRed, iCylinderGreen, iCylinderBlue, iCylinderAlpha)
		ENDIF
		
		INT iNum = ENUM_TO_INT(MARKER_NUM_0) + j
		IF NOT IS_VECTOR_ZERO(vPos)
		AND GET_SCREEN_COORD_FROM_WORLD_COORD(vPos, fTempx, fTempy)
			VECTOR vOffsetPos
			IF j <= 9
				IF GET_SCREEN_COORD_FROM_WORLD_COORD(vPos, fTempx, fTempy)
					DRAW_MARKER(INT_TO_ENUM(MARKER_TYPE, iNum), vPos + (<<0, 0, 0.9>>), (<<0,0,0>>), (<<0, 0, GET_RENDERING_CAM_HEADING()>>), (<<0.75, 0.75, 0.75>>), iNumberRed, iNumberGreen, iNumberBlue, iNumberAlpha)
				ENDIF
			ELSE
				vOffsetPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, GET_RENDERING_CAM_HEADING(), <<-0.4, 0.0, 0.0>>)
				DRAW_MARKER(MARKER_NUM_1, vOffsetPos + <<0, 0, 0.9>>, <<0, 0, 0>>, <<0, 0, GET_RENDERING_CAM_HEADING()>>, <<0.75, 0.75, 0.75>>, iNumberRed, iNumberGreen, iNumberBlue, iNumberAlpha)
				vOffsetPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, GET_RENDERING_CAM_HEADING(), <<0.4, 0.0, 0.0>>)
				DRAW_MARKER(INT_TO_ENUM(MARKER_TYPE, iNum-10), vOffsetPos + <<0, 0, 0.9>>, <<0, 0, 0>>, <<0, 0, GET_RENDERING_CAM_HEADING()>>, <<0.75, 0.75, 0.75>>, iNumberRed, iNumberGreen, iNumberBlue, iNumberAlpha)
			ENDIF
			IF iNumberOfPlacedGotos = 1
				DRAW_MARKER(MARKER_CYLINDER, vPos, <<0,0,0>>, <<0, 0, 0>>, <<1.5, 1.5, 1.5>>, iCylinderRed, iCylinderGreen, iCylinderBlue, iCylinderAlpha)
			ENDIF
		ENDIF
		
		IF iNumberOfPlacedGotos > 0	
			IF j < iNumberOfPlacedGotos - 1
				VECTOR vNext = GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(iPoolOverrideIndex, sAssociatedGotoTaskData, j+1)
							
				IF NOT IS_VECTOR_ZERO(vNext)
					IF GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(iPoolOverrideIndex, sAssociatedGotoTaskData, j), fTempx, fTempy)
					OR GET_SCREEN_COORD_FROM_WORLD_COORD(vNext, fTempx, fTempy)
						DRAW_LINE(vPos + <<0,0,0.5>>, vNext + <<0,0,0.5>>, 255, 0, 0, 125)
					ENDIF
				ENDIF
			ELSE				
				IF IS_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_BIT_SET(iPoolOverrideIndex, sAssociatedGotoTaskData, ciENTIRE_TASK_BITSET_LOOP_GOTO)
				AND j = iNumberOfPlacedGotos - 1
					INT iIndex = 0
					IF GET_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_LOOP_GOTO_START_INDEX(iPoolOverrideIndex, sAssociatedGotoTaskData) > -1
						iIndex = GET_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_LOOP_GOTO_START_INDEX(iPoolOverrideIndex, sAssociatedGotoTaskData)
					ENDIF
					IF GET_SCREEN_COORD_FROM_WORLD_COORD(vPos, fTempx, fTempy)
					OR GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(iPoolOverrideIndex, sAssociatedGotoTaskData, iIndex), fTempx, fTempy)
						DRAW_LINE(vPos + <<0,0,0.5>>, GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(iPoolOverrideIndex, sAssociatedGotoTaskData, iIndex) + <<0, 0, 0.5>>, 255, 0, 0, 125)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_SCREEN_COORD_FROM_WORLD_COORD(vPos, fTempx, fTempy)
			FLOAT fSize = GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_ARRIVAL_RADIUS(iPoolOverrideIndex, sAssociatedGotoTaskData, j)
			IF fSize != 0.0
				DRAW_MARKER(MARKER_SPHERE, GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(iPoolOverrideIndex, sAssociatedGotoTaskData, j), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<fSize, fSize, fSize>>, 255, 153, 153, 125, FALSE, FALSE, EULER_YXZ, FALSE, NULL_STRING(), NULL_STRING(), TRUE)
			ENDIF
		ENDIF
		
		VECTOR vPlayerActivatedPosition = GET_ASSOCIATED_GOTO_TASK_DATA__PLAYER_ACTIIVATED_POSITION(iPoolOverrideIndex, sAssociatedGotoTaskData, j)
		INT iPlayerActivatedRadius = GET_ASSOCIATED_GOTO_TASK_DATA__PLAYER_ACTIIVATED_RADIUS(iPoolOverrideIndex, sAssociatedGotoTaskData, j)
		
		IF NOT IS_VECTOR_ZERO(vPlayerActivatedPosition)
		AND GET_SCREEN_COORD_FROM_WORLD_COORD(vPlayerActivatedPosition, fTempx, fTempy)
			IF iPlayerActivatedRadius != 0.0
				DRAW_MARKER(MARKER_SPHERE, vPlayerActivatedPosition, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<iPlayerActivatedRadius, iPlayerActivatedRadius, iPlayerActivatedRadius>>, 255, 153, 153, 125, FALSE, FALSE, EULER_YXZ, FALSE, NULL_STRING(), NULL_STRING())
			ENDIF
			DRAW_MARKER(INT_TO_ENUM(MARKER_TYPE, iNum), vPlayerActivatedPosition + <<0, 0, 0.9>>, <<0, 0,0>>, <<0, 0, GET_RENDERING_CAM_HEADING()>>, <<0.75, 0.75, 0.75>>, 255, 153, 153, 200)
		ENDIF
		
		IF j = iCurrentGoToLocation
			iR = 50
			iG = 255
			iB = 50
		ELSE
			iR = 255
			iG = 255
			iB = 255
		ENDIF
		
		IF IS_VECTOR_ZERO(vPos)
			RELOOP
		ENDIF
		
		TEXT_LABEL_31 tl31
		
		tl31 = "Ped "
		tl31 += iPed
		IF iPoolOverrideIndex > -1
			tl31 += " | Pool "
			tl31 += iPoolOverrideIndex
		ENDIF
		DRAW_DEBUG_TEXT_ABOVE_COORDS(vPos, tl31, 3.0, iR, iG, iB, 255)
		
		tl31 = "Goto "
		tl31 += j
		DRAW_DEBUG_TEXT_ABOVE_COORDS(vPos, tl31, 2.0, iR, iG, iB, 255)
	ENDFOR
ENDPROC

// For drones.
PROC DEBUG_PROCESS_OBJ_GOTO_INFO_AND_MARKERS()
	
	BOOL bProcess = FALSE
	
	INT i
	IF sContentOverviewDebug.iEntityStateDrawObjDebugBS[0] != 0 // bitset size is only 1 atm.
		bProcess = TRUE
	ENDIF
	
	IF NOT bProcess
		EXIT
	ENDIF
		
	VECTOR vTemp
	OBJECT_INDEX ObjTemp
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects-1
	
		IF NOT FMMC_IS_LONG_BIT_SET(sContentOverviewDebug.iEntityStateDrawObjDebugBS, i)
			RELOOP
		ENDIF
	
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)])
			RELOOP
		ENDIF
		
		ObjTemp = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)])
		vTemp = GET_ENTITY_COORDS(ObjTemp, FALSE)
		
		INT iDrone = GET_DRONE_INDEX_FROM_ENTITY_ID_AND_TYPE(i, CREATION_TYPE_OBJECTS)
		IF iDrone = -1
			iDrone = 0
		ENDIF
		DEBUG_DRAW_PED_CURRENT_ASSOCIATED_GOTO_POINTS(g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData, i, MC_serverBD_2.iAssociatedGotoPoolIndex[i], vTemp, MC_serverBD.iDrone_AssociatedGotoProgress[iDrone])
		
	ENDFOR
		
ENDPROC

PROC DEBUG_PROCESS_PED_GOTO_INFO_AND_MARKERS()
	
	BOOL bProcess = FALSE
	
	INT i
	FOR i = 0 TO ciFMMC_PED_BITSET_SIZE-1
		IF sContentOverviewDebug.iEntityStateDrawPedDebugBS[i] != 0
			bProcess = TRUE
		ENDIF
	ENDFOR
	
	IF NOT bProcess
		EXIT
	ENDIF
		
	VECTOR vTemp
	PED_INDEX pedTemp
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1
	
		IF NOT FMMC_IS_LONG_BIT_SET(sContentOverviewDebug.iEntityStateDrawPedDebugBS, i)
			RELOOP
		ENDIF
	
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[i])
			RELOOP
		ENDIF
		
		pedTemp = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])
		vTemp = GET_ENTITY_COORDS(pedTemp, FALSE)
		DEBUG_DRAW_PED_CURRENT_ASSOCIATED_GOTO_POINTS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData, i, MC_serverBD_2.iAssociatedGotoPoolIndex[i], vTemp, MC_serverBD_2.iPedGotoProgress[i])
		
	ENDFOR
		
ENDPROC

PROC DEBUG_PROCESS_PED_DEFENSIVE_AREAS_INFO_AND_MARKERS()
	
	
	BOOL bProcess = FALSE
	
	INT i
	FOR i = 0 TO ciFMMC_PED_BITSET_SIZE-1
		IF sContentOverviewDebug.iEntityStateDrawPedDebugBS[i] != 0
			bProcess = TRUE
		ENDIF
	ENDFOR
	
	IF NOT bProcess
		EXIT
	ENDIF
	
	TEXT_LABEL_31 tl31
	PED_INDEX pedTemp
	VECTOR vPos
	INT iMarkerRed, iMarkerGreen, iMarkerBlue, iMarkerAlpha
	iMarkerRed = 0
	iMarkerGreen = 0
	iMarkerBlue = 0
	iMarkerAlpha = 0
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1
	
		IF NOT FMMC_IS_LONG_BIT_SET(sContentOverviewDebug.iEntityStateDrawPedDebugBS, i)
			RELOOP
		ENDIF
	
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[i])
			RELOOP
		ENDIF
		
		pedTemp = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])		
		vPos = GET_ENTITY_COORDS(pedTemp, FALSE)		
		
		GET_HUD_COLOUR(HUD_COLOUR_BLUE, iMarkerRed, iMarkerGreen, iMarkerBlue, iMarkerAlpha)
		iMarkerAlpha = 100
		IF IS_PED_DEFENSIVE_AREA_ACTIVE(pedTemp, FALSE)
			DRAW_MARKER(MARKER_SPHERE, GET_PED_DEFENSIVE_AREA_POSITION(pedTemp, FALSE) + (<<0, 0, -0.35>>), <<0,0,0>>, <<0,0,0>>, <<1.5, 1.5 ,1.5>>, iMarkerRed, iMarkerGreen, iMarkerBlue, iMarkerAlpha)
		ENDIF
		
		GET_HUD_COLOUR(HUD_COLOUR_BLUELIGHT, iMarkerRed, iMarkerGreen, iMarkerBlue, iMarkerAlpha)
		iMarkerAlpha = 100
		IF IS_PED_DEFENSIVE_AREA_ACTIVE(pedTemp, TRUE)
			DRAW_MARKER(MARKER_SPHERE, GET_PED_DEFENSIVE_AREA_POSITION(pedTemp, TRUE) + (<<0, 0, -0.35>>), <<0,0,0>>, <<0,0,0>>, <<1.5, 1.5, 1.5>>, iMarkerRed, iMarkerGreen, iMarkerBlue, iMarkerAlpha)
		ENDIF
		
		IF NOT IS_VECTOR_ZERO(GET_PED_DEFENSIVE_AREA_POSITION(pedTemp, FALSE))			
			DRAW_LINE(vPos, GET_PED_DEFENSIVE_AREA_POSITION(pedTemp, FALSE), 255, 0, 0, 125)
			tl31 = "Def Area (Primary) "	
			DRAW_DEBUG_TEXT_ABOVE_COORDS( GET_PED_DEFENSIVE_AREA_POSITION(pedTemp, FALSE), tl31, 1.0, 255, 255, 255, 255)
		ENDIF
		
		IF NOT IS_VECTOR_ZERO(GET_PED_DEFENSIVE_AREA_POSITION(pedTemp, TRUE))
			DRAW_LINE(vPos, GET_PED_DEFENSIVE_AREA_POSITION(pedTemp, TRUE), 255, 0, 0, 125)
			tl31 = "Def Area (Secondary) "		
			DRAW_DEBUG_TEXT_ABOVE_COORDS(GET_PED_DEFENSIVE_AREA_POSITION(pedTemp, TRUE), tl31, 1.0, 255, 255, 255, 255)
		ENDIF
	ENDFOR
	
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Debug Help						-----------------------------------------------
// ##### Description: Information for debug controls in MC --------------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------
PROC DRAW_MISSION_START_DEBUG_HELP()
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
	OR sContentOverviewDebug.eDebugWindow != CONTENT_OVERVIEW_DEBUG_WINDOW_NONE
	OR g_bEnableMCDebugHelp
		EXIT
	ENDIF
		
	INT iPrint
	TEXT_LABEL_63 tl63 = ""	
	FLOAT fSpacingY = 0.02
	VECTOR vPos = <<cfDEBUG_INFO_START_LEFT_X, cfDEBUG_INFO_START_LEFT_Y-(fSpacingY*2), 0.0>>
	VECTOR vNewPos
	
	// Title/Intro anim so that we know this exists.
	IF g_bMissionClientGameStateRunning
		IF NOT HAS_NET_TIMER_STARTED(tdMissionRunning_HelpWindow)
			START_NET_TIMER(tdMissionRunning_HelpWindow)
		ENDIF
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdMissionRunning_HelpWindow, ciMISSION_RUNNING_HELP_WINDOW_DELAY)		
			
			// Has to have lines and spheres already drawing in order to show up.		
			vNewPos = <<vPos.x, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
			tl63 = "Press [Numpad Plus] to display MC Debug List."
			SET_TEXT_SCALE(0.325, 0.325)
			SET_TEXT_COLOUR(255, 255, 255, 255)
			SET_TEXT_DROPSHADOW(6, 0, 0, 0, 255)
			PROCESS_TYPE_WRITER_TEXT_ANIMATION(tdTypeWriter_TextTyping_DebugHelp, tdTypeWriter_TextFullyDrawn_DebugHelp, 3000, 80, vNewPos, iTypeWriterCharCounter_DebugHelp, tl63, FALSE, FALSE)
			
		ENDIF
	ENDIF
	
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Debug Controls Help				-----------------------------------------------
// ##### Description: Information for debug controls in MC --------------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC DRAW_DEBUG_CONTENT_CONTROLS()
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_ADD, KEYBOARD_MODIFIER_NONE, "HelpText")
		IF g_bEnableMCDebugHelp
			g_bEnableMCDebugHelp = FALSE			
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
		ELSE		
			g_bEnableMCDebugHelp = TRUE
		ENDIF
	ENDIF
	
	IF NOT g_bEnableMCDebugHelp
		EXIT
	ENDIF
	
	INT iPrint
	TEXT_LABEL_63 tl63 = ""	
	FLOAT fSpacingY = 0.02
	VECTOR vPos = <<cfDEBUG_INFO_START_LEFT_X, cfDEBUG_INFO_START_LEFT_Y, 0.0>>
	VECTOR vNewPos
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)

	iPrint = -1
	vNewPos = <<vPos.x, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
	tl63 = "Close/Open: [Numpad Plus]"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 255, 255)
	iPrint = 0	
	
	vNewPos = <<vPos.x, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
	tl63 = "----------------- Debug Key Controls ------------------"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 255, 255)
	iPrint++	
	
	// Debug Windows
	vNewPos = <<vPos.x, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
	tl63 = "Toggle Conversation:           [Numpad Minus]"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 255, 255)
	iPrint++
	
	vNewPos = <<vPos.x, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
	tl63 = "Toggle Spawn Group:            [ALT Numpad Minus]"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 255, 255)
	iPrint++
	
	vNewPos = <<vPos.x, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
	tl63 = "Toggle Spawning:               [CTRL Numpad Minus]"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 255, 255)
	iPrint++
	
	vNewPos = <<vPos.x, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
	tl63 = "Toggle Aggro Indexes:          [SHIFT Numpad Minus]"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 255, 255)
	iPrint++
	
	vNewPos = <<vPos.x, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
	tl63 = "Sync Scene Ped Debug:          [ALT F8]"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 255, 255)
	iPrint++	
	
	vNewPos = <<vPos.x, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
	tl63 = "Toggle Mission State Debug:    [6]"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 255, 255)
	iPrint++
	
	vNewPos = <<vPos.x, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
	tl63 = "MC Performance Profiler:       [SHIFT CTRL 1]"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 255, 255)
	iPrint++		
	// ------
	
	
	// Debug Key Info
	iPrint++
	vNewPos = <<vPos.x, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
	tl63 = "Timer -30 Seconds:             [ALT 8]"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 255, 255)
	iPrint++
	vNewPos = <<vPos.x, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
	tl63 = "Timer Pause:                   [ALT 9]"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 255, 255)
	iPrint++
	vNewPos = <<vPos.x, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
	tl63 = "Timer +30 Seconds:             [ALT 0]"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 255, 255)
	iPrint++
	
	vNewPos = <<vPos.x, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
	tl63 = "Skip/Warp To Objective:        [J]"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 255, 255)
	iPrint++	
	vNewPos = <<vPos.x, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
	tl63 = "Pass Mission:                  [S]"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 255, 255)
	iPrint++	
	vNewPos = <<vPos.x, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
	tl63 = "Fail Mission:                  [F]"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 255, 255)
	iPrint++
	vNewPos = <<vPos.x, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
	tl63 = "Quick Restart Mission:         [SHIFT CTRL Z]"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 255, 255)
	iPrint++	
	vNewPos = <<vPos.x, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
	tl63 = "Unpause Renderphase:           [7]"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 255, 255)
	iPrint++	
	vNewPos = <<vPos.x, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
	tl63 = "Fade In Screen:                [SHIFT 7]"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 255, 255)
	iPrint++
	// ------
	
	vNewPos = <<vPos.x, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
	tl63 = "-------------------------------------------------------"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 255, 255)
	iPrint++
	
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Ped Sync Scene 					-----------------------------------------------
// ##### Description: Information related to Spawn Groups and Sub -------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC DEBUG_SHOW_SYNC_SCENE_STATES()
		
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F8, KEYBOARD_MODIFIER_ALT, "Sync Scene Ped Debug")
		IF bSyncScenePedDebugDisplay		
			bSyncScenePedDebugDisplay = FALSE
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
		ELSE
			bSyncScenePedDebugDisplay = TRUE			
		ENDIF
	ENDIF
	
	IF NOT bSyncScenePedDebugDisplay
		EXIT
	ENDIF
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)

	INT iPrint
	TEXT_LABEL_63 tl63 = ""	
	VECTOR vNewPos	
	FLOAT fX, fY
	
	fX = cfDEBUG_INFO_START_RIGHT_X
	fY = cfDEBUG_INFO_START_RIGHT_Y
	
	PROCESS_DEBUG_MOVER_FOR_TYPE(sDebugMoversStruct, ciDEBUG_ON_SCREEN_MOVERS_REUSABLE, fX, fY, 0.25, 0.025, TRUE, FALSE, TRUE)
	
	vNewPos = <<fX, fY+(0.03*iPrint), fY+(0.015*iPrint)>>
	
	tl63 = "------- Ped Sync Scene Debug (indexed by 0) -------"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 0, 200, 0, 255)
	iPrint++
	iPrint++
	
	INT iSyncedPeds = 0
	INT iPeds = 0
	INT iAmountOfSyncedPeds
	FOR iPeds = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1
	
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_ServerBD_1.sFMMC_SBD.niPed[iPeds])
			RELOOP
		ENDIF
		
		BOOL bIsPerformingSpecialAnim
		bIsPerformingSpecialAnim = FMMC_IS_LONG_BIT_SET(iPedPerformingSpecialAnimBS, iPeds)				
		
		INT iAnimation	
		IF bIsPerformingSpecialAnim
			iAnimation = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPeds].iSpecialAnim
		ELSE
			iAnimation = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPeds].iIdleAnim
		ENDIF
		
		IF iAnimation = 0
			RELOOP
		ENDIF
		
		FOR iSyncedPeds = 0 TO FMMC_MAX_PED_SCENE_SYNCED_PEDS-1
			IF sMissionPedsLocalVars[iPeds].iSyncedPeds[iSyncedPeds] > -1
				iAmountOfSyncedPeds++
			ENDIF
		ENDFOR
		
		vNewPos = <<fX, fY+(0.0125*iPrint), fY+(0.0125*iPrint)>>
		
		tl63 = "Ped "
		tl63 += iPeds
		tl63 += " | "
		tl63 = "Anim "
		tl63 += GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation)		
		DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 0, 200, 0, 255)
		iPrint++
		
		vNewPos = <<fX, fY+(0.0125*iPrint), fY+(0.0125*iPrint)>>
		
		tl63 = "State: "
		tl63 += GET_PED_SYNC_SCENE_STATE_NAME_PRINT(sMissionPedsLocalVars[iPeds].ePedSyncSceneState)
		DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 0, 200, 0, 255)
		iPrint++
		
		vNewPos = <<fX, fY+(0.0125*iPrint), fY+(0.0125*iPrint)>>
		
		tl63 = "Scene ID: "
		tl63 += sMissionPedsLocalVars[iPeds].iSyncScene	
		tl63 += "| Peds Involved: "
		tl63 += iAmountOfSyncedPeds
		DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 0, 200, 0, 255)
		iPrint++
		iPrint++
	ENDFOR	
	
	vNewPos = <<fX, fY+(0.0125*iPrint), fY+(0.0125*iPrint)>>
	tl63 = "---------------------------------"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 0, 200, 0, 255)
	iPrint++
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Aggro Indexes 					-----------------------------------------------
// ##### Description: Information related to Spawn Groups and Sub -------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC PROCESS_PRINT_AGGRO_INDEX_INFORMATION()
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_SUBTRACT, KEYBOARD_MODIFIER_SHIFT, "AggroIndexDebug")
		IF g_bEnableAggroIndexDebug		
			g_bEnableAggroIndexDebug = FALSE			
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
		ELSE
			BOOL bPedStateDebugWasEnabled
			IF sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS
				bPedStateDebugWasEnabled = TRUE
			ENDIF
			
			g_bEnableAggroIndexDebug = TRUE
			
			IF bPedStateDebugWasEnabled
				sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT g_bEnableAggroIndexDebug
		EXIT
	ENDIF
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)

	INT iPrint
	TEXT_LABEL_63 tl63 = ""		
	FLOAT fX, fY
	VECTOR vNewPos
	
	fX = cfDEBUG_INFO_START_RIGHT_X
	fY = cfDEBUG_INFO_START_RIGHT_Y
	
	PROCESS_DEBUG_MOVER_FOR_TYPE(sDebugMoversStruct, ciDEBUG_ON_SCREEN_MOVERS_AGGRO_INDEXES, fX, fY, 0.25, 0.025, TRUE, FALSE, TRUE)
	
	vNewPos = <<fX, fY+(0.03*iPrint), fY+(0.015*iPrint)>>
	
	tl63 = "----- Aggro Index Debug (indexed by 0) -----"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 0, 200, 0, 255)
	iPrint++
	iPrint++
	
	INT i
	
	FOR i = 0 TO ciMISSION_AGGRO_INDEX_MAX-1
		vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
				
		tl63 = "Aggro Index "
		tl63 += i
		
		IF IS_BIT_SET(MC_serverBD.iServerAggroBS[0], i)
			tl63 += " : Triggered"
			DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 200, 0, 0, 255)
		ELIF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[0][i])
			INT iTime = MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD.iAggroCountdownTimeStamp[0][i])
			tl63 += " : Delay Started, "
			tl63 += iTime
			tl63 += "ms"
			DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 200, 200, 0, 255)			
		ELSE
			tl63 += " : Not Active... "	
			DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 0, 200, 0, 255)
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD.iServerCCTVAlertedPlayerBS[0], i)
			tl63 += "   | CCTV Spotted Player"
			DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 200, 0, 0, 255)
			
		ELIF IS_BIT_SET(MC_serverBD.iServerCCTVAlertedPedBodyBS[0], i)
			tl63 += "   | CCTV Spotted Player"
			DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 200, 0, 0, 255)
			
		ELIF IS_BIT_SET(MC_serverBD.iServerCCTVTriggeredByPedsBS[0], i)
			tl63 += "   | Ped Triggered CCTV"
			DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 200, 0, 0, 255)
		ENDIF
		
		iPrint++
		iPrint++
	ENDFOR
	
	vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
	tl63 = "---------------------------------"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 0, 200, 0, 255)
	iPrint++
	
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Spawn Groups 					------------------------------------------------
// ##### Description: Information related to Spawn Groups and Sub --------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC PROCESS_PRINT_SPAWN_GROUP_INFORMATION()
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_SUBTRACT, KEYBOARD_MODIFIER_ALT, "SpawnGroupDebug")
		IF g_bEnableSpawnGroupDebug
			g_bEnableSpawnGroupDebug = FALSE
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
		ELSE
			g_bEnableSpawnGroupDebug = TRUE
		ENDIF
	ENDIF
	
	IF NOT g_bEnableSpawnGroupDebug
		EXIT
	ENDIF
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)

	INT iPrint
	TEXT_LABEL_63 tl63 = ""
	VECTOR vNewPos
	FLOAT fX, fY
	
	fX = cfDEBUG_INFO_START_RIGHT_X
	fY = cfDEBUG_INFO_START_RIGHT_Y
	
	PROCESS_DEBUG_MOVER_FOR_TYPE(sDebugMoversStruct, ciDEBUG_ON_SCREEN_MOVERS_SPAWN_GROUPS, fX, fY, 0.25, 0.025, TRUE, FALSE, TRUE)
	
	vNewPos = <<fX, fY+(0.03*iPrint), fY+(0.015*iPrint)>>
	
	tl63 = "------- SpawnGroup Debug (indexed by 1) -------"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 0, 255, 255, 255)
	iPrint++
	
	IF g_iMissionForcedSpawnGroup != 0
		vNewPos = <<fX, fY+(0.03*iPrint), fY+(0.015*iPrint)>>
		tl63 = "<!> RAG/DEBUG Values are being forced <!>"
		DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 0, 0, 255)
		iPrint++		
		iPrint++
	ENDIF
	
	vNewPos = <<fX, fY+(0.03*iPrint), fY+(0.015*iPrint)>>
	tl63 = "rsgSpawnSeed.iMiscBS: "
	tl63 += MC_serverBD_4.rsgSpawnSeed.iMiscBS
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 0, 255, 255, 255)
	iPrint++
	iPrint++
	iPrint++
	
	INT i, ii
	
	FOR i = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
		vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
		tl63 = ""
		tl63 += i
		
		IF IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, i)
			IF i >= 9
				tl63 += " : Active "
			ELSE			
				tl63 += " : Active "
			ENDIF
		ELSE
			IF i >= 9
				tl63 += " : Not Active..... "
			ELSE			
				tl63 += " : Not Active..... "
			ENDIF
			
		ENDIF
		
		IF MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[i] != 0
			tl63 += "with Subs:   "
		ENDIF
		
		FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
			IF IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[i], ii)
				IF ii > 0
					tl63 += ", "
				ENDIF
				tl63 += ii				
			ENDIF
		ENDFOR
		DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 0, 255, 255, 255)
		
		iPrint++
	ENDFOR

	vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
	tl63 = "---------------------------------"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 0, 255, 255, 255)
	iPrint++
	
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Spawn Points					------------------------------------------------
// ##### Description: Information related to Spawn Groups and Sub --------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC MAINTAIN_CUSTOM_RESPAWN_STATUS_DEBUG(INT iTeam, INT iPoint, BOOL bForceDeactivated = FALSE)
	VECTOR vCoord
	FLOAT fActivationRadius 
	
	vCoord 				= g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iPoint].vPos
	fActivationRadius 	= GET_ACTIVATION_RADIUS_RESPAWN_POINT_FROM_INDEX(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iPoint].iActivationRadius)
	
	IF IS_VECTOR_ZERO(vCoord)
		EXIT
	ENDIF
	
	INT iStatus = 0	
	IF FMMC_IS_LONG_BIT_SET(iBSRespawnActiveFromRadius[iTeam], iPoint)
		iStatus = 1
	ENDIF	
	IF FMMC_IS_LONG_BIT_SET(iBSRespawnDeactivated[iTeam], iPoint)
		iStatus = 2
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iPoint].iSpawnBitset, ci_SpawnBS_Cannot_Be_Deactived_Ever)
	AND FMMC_IS_LONG_BIT_SET(iBSRespawnActiveFromRadius[iTeam], iPoint)
	OR g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iPoint].iActivationRadius = 0
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iPoint].iSpawnBitset, ci_SpawnBS_Cannot_Be_Deactived_Ever)
		iStatus = 3
	ENDIF
	
	INT rStatus, gStatus, bStatus, aStatus
	IF IS_SPAWN_POINT_A_VALID_RESPAWN_POINT(iPoint, iTeam)
	AND NOT bForceDeactivated
		rStatus=0 	gStatus=255 	bStatus=0 	aStatus=255
	ELSE
		rStatus=255 	gStatus=0 	bStatus=0 	aStatus=255
	ENDIF
	
	// Status Representation.
	DRAW_DEBUG_SPHERE(<<vCoord.x, vCoord.y, vCoord.z+1>>, 0.4, rStatus, gStatus, bStatus, aStatus)
	
	// Actual Radius for activation
	IF fActivationRadius > 0
		DRAW_DEBUG_SPHERE(vCoord, fActivationRadius, 150, 150, 150, 100)	
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iPoint].iSpawnBitset, ci_SpawnBS_Deactivate_All_Others_When_Activated)
		DRAW_DEBUG_SPHERE(<<vCoord.x, vCoord.y, vCoord.z+2>>, 0.4, 200, 200, 100, aStatus)
	ENDIF
		
	IF bForceDeactivated
		DRAW_DEBUG_TEXT_ABOVE_COORDS(vCoord, "Disabled by Spawn Area", 2.0, rStatus, gStatus, bStatus, aStatus)
	ELIF (IS_SPAWN_POINT_A_VALID_START_POINT(iPoint, iTeam) AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iPoint].iSpawnBitset, ci_SpawnBS_Initial_Start_Spawn_Point) AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iPoint].iSpawnBitset, ci_SpawnBS_Treat_As_Respawn_Point))
	OR IS_SPAWN_POINT_A_VALID_RESPAWN_POINT(iPoint, iTeam)
	OR IS_SPAWN_POINT_A_VALID_CHECKPOINT(iPoint, iTeam)
		DRAW_DEBUG_TEXT_ABOVE_COORDS(vCoord, "Valid", 2.0, rStatus, gStatus, bStatus, aStatus)
	ELSE
		DRAW_DEBUG_TEXT_ABOVE_COORDS(vCoord, "Not Valid", 2.0, rStatus, gStatus, bStatus, aStatus)
	ENDIF
	
	TEXT_LABEL_63 tl63 = ""
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iPoint].iSpawnBitset, ci_SpawnBS_Initial_Start_Spawn_Point)
		tl63 += "| Start |"
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iPoint].iSpawnBitset, ci_SpawnBS_Treat_As_Respawn_Point)
		IF IS_STRING_NULL_OR_EMPTY(tl63)
			tl63 += "| "
		ENDIF
		tl63 += "Respawn |"
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iPoint].iSpawnBitset, ci_SpawnBS_Checkpoint_Spawn_Point_0)
		IF IS_STRING_NULL_OR_EMPTY(tl63)
			tl63 += "| "
		ENDIF
		tl63 += "CP1 |"
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iPoint].iSpawnBitset, ci_SpawnBS_Checkpoint_Spawn_Point_1)
		IF IS_STRING_NULL_OR_EMPTY(tl63)
			tl63 += "| "
		ENDIF
		tl63 += "CP2 |"
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iPoint].iSpawnBitset, ci_SpawnBS_Checkpoint_Spawn_Point_2)
		IF IS_STRING_NULL_OR_EMPTY(tl63)
			tl63 += "| "
		ENDIF
		tl63 += "CP3 |"
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iPoint].iSpawnBitset, ci_SpawnBS_Checkpoint_Spawn_Point_3)
		IF IS_STRING_NULL_OR_EMPTY(tl63)
			tl63 += "| "
		ENDIF
		tl63 += "CP4 |"	
	ENDIF
	IF NOT IS_STRING_NULL_OR_EMPTY(tl63)
		DRAW_DEBUG_TEXT_ABOVE_COORDS(vCoord, tl63, 1.5, rStatus, gStatus, bStatus, aStatus)
	ENDIF
	
	IF iStatus = 1
		DRAW_DEBUG_TEXT_ABOVE_COORDS(vCoord, "Active From Radius", 1.0, rStatus, gStatus, bStatus, aStatus)
	ELIF iStatus = 2
		DRAW_DEBUG_TEXT_ABOVE_COORDS(vCoord, "Deactivated From Radius", 1.0, rStatus, gStatus, bStatus, aStatus)
	ENDIF
	
	TEXT_LABEL_15 tl15 = "Point "
	tl15 += iPoint
	DRAW_DEBUG_TEXT_ABOVE_COORDS(vCoord, tl15, 0.75, rStatus, gStatus, bStatus, aStatus)
	
	IF MC_ServerBD.iNumberOfTeams > 1
		SWITCH iTeam
			CASE 0		DRAW_DEBUG_TEXT_ABOVE_COORDS(vCoord, "Team 0", 2.5, rStatus, gStatus, bStatus, aStatus)			BREAK
			CASE 1		DRAW_DEBUG_TEXT_ABOVE_COORDS(vCoord, "Team 1", 2.5, rStatus, gStatus, bStatus, aStatus)			BREAK
			CASE 2		DRAW_DEBUG_TEXT_ABOVE_COORDS(vCoord, "Team 2", 2.5, rStatus, gStatus, bStatus, aStatus) 		BREAK
			CASE 3		DRAW_DEBUG_TEXT_ABOVE_COORDS(vCoord, "Team 3", 2.5, rStatus, gStatus, bStatus, aStatus) 		BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC PROCESS_CUSTOM_RESPAWN_DEBUG(BOOL bForceDeactivated = FALSE)
	IF NOT bCustomRespawnPointsDebug
		EXIT
	ENDIF
	
	INT iTeamSpawnPointCounter
	INT iTeam = PICK_INT(sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_NONE, MC_playerBD[iLocalPart].iteam, sContentOverviewDebug.iTeamIndexSelected)
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	FOR iTeamSpawnPointCounter = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTeam]-1
		IF iRule < FMMC_MAX_RULES			
			MAINTAIN_CUSTOM_RESPAWN_STATUS_DEBUG(iTeam, iTeamSpawnPointCounter, bForceDeactivated)
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_PRINT_SPAWN_POINT_INFORMATION()
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_SUBTRACT, KEYBOARD_MODIFIER_CTRL, "SpawnPointDebug")
		IF bShowOnScreenCustomRespawnPointDebug			
			bShowOnScreenCustomRespawnPointDebug = FALSE
			bCustomRespawnPointsDebug = FALSE
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
		ELSE			
			bShowOnScreenCustomRespawnPointDebug = TRUE
			bCustomRespawnPointsDebug = TRUE
		ENDIF
	ENDIF
	
	IF NOT bShowOnScreenCustomRespawnPointDebug
		EXIT
	ENDIF
	
	BOOL bSpawnAreaActive
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	INT iArea = 0
	//FOR iArea = 0 TO MAX_NUMBER_OF_MISSION_SPAWN_AREAS-1
		IF g_SpawnData.MissionSpawnDetails.SpawnArea[iArea].bIsActive
			bSpawnAreaActive = TRUE
			//BREAKLOOP
		ENDIF
	//ENDFOR

	INT iPrint
	TEXT_LABEL_63 tl63 = ""			
	VECTOR vNewPos
	FLOAT fX, fY
		
	fX = cfDEBUG_INFO_START_RIGHT_X
	fY = cfDEBUG_INFO_START_RIGHT_Y
	
	PROCESS_DEBUG_MOVER_FOR_TYPE(sDebugMoversStruct, ciDEBUG_ON_SCREEN_MOVERS_CUSTOM_SPAWN_POINTS, fX, fY, 0.25, 0.025, TRUE, FALSE, TRUE)
	
	vNewPos = <<fX, fY+(0.03*iPrint), fY+(0.015*iPrint)>>
	
	tl63 = "------- Spawning Debug (indexed by 0) -------"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 0, 255)
	iPrint++
	iPrint++
	
	vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
	tl63 = "Last Death Location = "
	tl63 += "<<"
	tl63 += ROUND(vDebugSpawnLastDeathLocation.x)
	tl63 += ", "
	tl63 += ROUND(vDebugSpawnLastDeathLocation.y)
	tl63 += ", "
	tl63 += ROUND(vDebugSpawnLastDeathLocation.z)
	tl63 += ">>"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 0, 255)
	iPrint++
	
	vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
	tl63 = "Last Death Heading = "
	tl63 += ROUND(fDebugSpawnLastDeathHeading)
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 255, 255, 0, 255)
	iPrint++
	iPrint++
	
	vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
	tl63 = "Last Spawn Location = "
	tl63 += "<<"
	tl63 += ROUND(vDebugSpawnLastSpawnLocation.x)
	tl63 += ", "
	tl63 += ROUND(vDebugSpawnLastSpawnLocation.y)
	tl63 += ", "
	tl63 += ROUND(vDebugSpawnLastSpawnLocation.z)
	tl63 += ">>"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos,  255, 255, 0, 255)
	iPrint++
	
	vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
	tl63 = "Last Spawn Heading = "
	tl63 += ROUND(fDebugSpawnLastSpawnHeading)
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos,  255, 255, 0, 255)
	iPrint++
	iPrint++
	
	
	vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
	tl63 = "--------------"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos,  255, 255, 0, 255)
	iPrint++
	iPrint++
	
	SPAWN_AREA SpArea
	INT i	
	IF bSpawnAreaActive
		
		vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
		tl63 = "Using Spawn Areas"
		DRAW_DEBUG_TEXT_2D(tl63, vNewPos,  255, 255, 0, 255)
		iPrint++
		
		SpArea = g_SpawnData.MissionSpawnDetails.SpawnArea[iArea]
		
		//FOR iArea = 0 TO MAX_NUMBER_OF_MISSION_SPAWN_AREAS-1		
			
			// Missions currently only use Index 0 out of 4.
			iArea = 0
			
			//IF NOT SpArea.bIsActive
				//RELOOP
			//ENDIF
			
			vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
			tl63 = "Using Spawn Area 1:"
			
			IF SpArea.iShape = SPAWN_AREA_SHAPE_BOX
				DRAW_BOX(SpArea.vCoords1, SpArea.vCoords2, 200, 200, 200, 150)
				tl63 += " Axis Aligned Box"
				
				VECTOR vTemp = (SpArea.vCoords1+SpArea.vCoords2)
				vTemp.x /= 2
				vTemp.y /= 2
				vTemp.z /= 2
				DRAW_DEBUG_TEXT_ABOVE_COORDS(vTemp, "Spawn Box", 1.5, 100, 250, 100, 200)
				
			ELIF SpArea.iShape = SPAWN_AREA_SHAPE_CIRCLE
				DRAW_MARKER(MARKER_SPHERE, SpArea.vCoords1, (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>), (<<SpArea.fFloat, SpArea.fFloat, SpArea.fFloat>>), 200, 200, 200, 150)
				tl63 += " Sphere"
				
				DRAW_DEBUG_TEXT_ABOVE_COORDS(SpArea.vCoords1, "Spawn Sphere", 1.5, 100, 250, 100, 200)
				
			ELIF SpArea.iShape = SPAWN_AREA_SHAPE_ANGLED
				DRAW_ANGLED_AREA_FROM_FACES(SpArea.vCoords1, SpArea.vCoords2, SpArea.fFloat, 200, 200, 200, 150)
				tl63 += " Angled Area"
				
				VECTOR vTemp = (SpArea.vCoords1+SpArea.vCoords2)
				vTemp.x /= 2
				vTemp.y /= 2
				vTemp.z /= 2
				DRAW_DEBUG_TEXT_ABOVE_COORDS(vTemp, "Spawn Angled Area", 1.5, 100, 250, 100, 200)
				
			ENDIF
			
			DRAW_DEBUG_TEXT_2D(tl63, vNewPos,  255, 255, 0, 255)
			iPrint++
			iPrint++
		//ENDFOR
		
	ELIF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_USING_CUSTOM_RESPAWN_POINTS)				
		vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
		tl63 = "PREVIOUS Spawn Points Active: "
		FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[MC_PlayerBD[iLocalPart].iTeam]-1
			IF FMMC_IS_LONG_BIT_SET(iDebugPreviousSpawnPointsActiveBS, i)
				tl63 += i
				tl63 += ", "
			ENDIF
		ENDFOR
		DRAW_DEBUG_TEXT_2D(tl63, vNewPos,  255, 255, 0, 255)
		iPrint++
		
		vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
		tl63 = "CURRENT Spawn Points Active: "
		FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[MC_PlayerBD[iLocalPart].iTeam]-1
			IF FMMC_IS_LONG_BIT_SET(iDebugSpawnPointsActiveBS, i)
				tl63 += i
				tl63 += ", "
			ENDIF
		ENDFOR
		DRAW_DEBUG_TEXT_2D(tl63, vNewPos,  255, 255, 0, 255)
		iPrint++
		iPrint++	
	
	ELIF g_SpawnData.MissionSpawnGeneralBehaviours.DefaultSpawnLocation = SPAWN_LOCATION_NEAR_DEATH			
		vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
		tl63 = "Using Default / Near Death default spawning"
		DRAW_DEBUG_TEXT_2D(tl63, vNewPos,  255, 255, 0, 255)
		iPrint++
		iPrint++
		
	ELSE
		vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
		tl63 = "Using Ambient DefaultSpawnLocation ENUM_TO_INT: "
		tl63 += ENUM_TO_INT(g_SpawnData.MissionSpawnGeneralBehaviours.DefaultSpawnLocation)
		DRAW_DEBUG_TEXT_2D(tl63, vNewPos,  255, 255, 0, 255)
		iPrint++
		iPrint++
		
	ENDIF
		
	vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
	tl63 = "---------------------------------"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos,  255, 255, 0, 255)
	iPrint++
		
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Ped Companions -----------------------------------------------------------------
// ##### Description: Drawing of mission controller debug window --------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC PROCESS_PED_COMPANION_DEBUG()
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_5, KEYBOARD_MODIFIER_CTRL, "CompanionDebug")
		IF bCompanionDebug
			bCompanionDebug = FALSE
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
		ELSE			
			bCompanionDebug = TRUE
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		ENDIF
	ENDIF
	
	IF NOT bCompanionDebug
		EXIT
	ENDIF

	INT iPrint
	TEXT_LABEL_63 tl63 = ""		
	VECTOR vNewPos
	FLOAT fX, fY	
	
	fX = cfDEBUG_INFO_START_RIGHT_X
	fY = cfDEBUG_INFO_START_RIGHT_Y
	
	PROCESS_DEBUG_MOVER_FOR_TYPE(sDebugMoversStruct, ciDEBUG_ON_SCREEN_MOVERS_REUSABLE, fX, fY, 0.25, 0.025, TRUE, FALSE, TRUE)
	
	vNewPos = <<fX, fY+(0.03*iPrint), fY+(0.015*iPrint)>>
	
	tl63 = "------- Ped Companion Debug (indexed by 0) -------"
	DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 0, 200, 0, 255)
	iPrint++
	iPrint++
	
	INT i	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[i])
			RELOOP
		ENDIF
		
		IF MC_serverBD_2.iPedState[i] != ciTASK_COMPANION
			RELOOP
		ENDIF
		
		INT iCompanionIndex = GET_PED_COMPANION_INDEX(i)		
				
		vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
		tl63 = "Companion Ped: "
		tl63 += i
		DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 0, 255, 255, 255)
		iPrint++
				
		vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
		tl63 = "Companion Index: "
		tl63 += iCompanionIndex
		DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 0, 255, 255, 255)
		iPrint++
		
		vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
		tl63 = "Companion State: "
		tl63 += GET_COMPANION_TASK_STATE_NAME(MC_playerBD_1[iLocalPart].sPedCompanionTaskData[iCompanionIndex].ePedCompanionTaskState)
		DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 0, 255, 255, 255)
		iPrint++
		
		IF MC_playerBD_1[iLocalPart].sPedCompanionTaskData[iCompanionIndex].targetEntityFriend != INVALID_PLAYER_INDEX()
			vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
			tl63 = "Companion Following: "
			tl63 += GET_PLAYER_NAME(MC_playerBD_1[iLocalPart].sPedCompanionTaskData[iCompanionIndex].targetEntityFriend)
			DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 0, 255, 255, 255)
			iPrint++
		ELSE
			vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
			tl63 = "Companion Following: none"			
			DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 0, 255, 255, 255)
			iPrint++
		ENDIF
		
		vNewPos = <<fX, fY+(0.015*iPrint), fY+(0.015*iPrint)>>
		IF IS_PED_IN_GROUP(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i]))
			tl63 = "Companion In Group: Yes"
		ELSE
			tl63 = "Companion In Group: No"
		ENDIF		
		DRAW_DEBUG_TEXT_2D(tl63, vNewPos, 0, 255, 255, 255)
		iPrint++
	ENDFOR
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Content Overview Debug Window  -------------------------------------------------
// ##### Description: Drawing of mission controller debug window --------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
	TEXT_LABEL_63 tl63_Name
	tl63_Name = "~bold~~g~Local Participant Index:   "
	tl63_Name += iLocalPart
	DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
	
	tl63_Name = "~bold~~g~Local Player Name:   "
	tl63_Name += GET_PLAYER_NAME(PLAYER_ID())
	DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
	
	tl63_Name = "~bold~~g~Local Player Team:   "
	tl63_Name += MC_PlayerBD[iLocalPart].iTeam
	DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
	
	tl63_Name = "~bold~~g~Local Player Rule:   "
	tl63_Name += MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iTeam]
	DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
ENDPROC

PROC PROCESS_GANG_CHASE_STATE_DEBUG_WINDOW()
	
	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
		
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 1

	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)	

		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
				
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1		
				
		tl63_Name = "Gang Chase Should Be Active: "
		tl63_Name += PICK_STRING(bDebugGangChaseActiveForMyTeam, "Yes", "No")
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
				
		tl63_Name = "Lose Gang Chase Objective: "
		tl63_Name += PICK_STRING(IS_CURRENT_OBJECTIVE_LOSE_GANG_CHASE(), "Yes", "No")
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		
		tl63_Name = "Units Chasing Team "
		tl63_Name += MC_PlayerBD[iLocalPart].iTeam
		tl63_Name += ": "
		tl63_Name += MC_serverBD_1.iNumGangChaseUnitsChasing[MC_PlayerBD[iLocalPart].iTeam]
		tl63_Name += " / "
		tl63_Name += iDebugGangChaseIntensity
		
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		
		IF MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
			tl63_Name = "Spawned for Team "
			tl63_Name += MC_PlayerBD[iLocalPart].iTeam
			tl63_Name += " This Rule: "
			tl63_Name += MC_serverBD_1.iNumGangChaseUnitsSpawned[MC_PlayerBD[iLocalPart].iTeam][MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iTeam]]
			tl63_Name += " / "
			IF iDebugGangChaseTotalSpawns = HIGHEST_INT
				tl63_Name += "Infinite" 
			ELSE
				tl63_Name += iDebugGangChaseTotalSpawns 
			ENDIF
			DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		ENDIF
		
	ENDIF
	
	// Main Draw Loop -------------------------
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	INT iRowsDrawn = 0
	INT i = 0
	FOR i = 0 TO ciMAX_GANG_CHASE_VEHICLES - 1
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (iRowsDrawn / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * iRowsDrawn) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (iRowsDrawn / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
		
		// Warp Click Event (Player Ped)		
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_1, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Camera)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_2, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")

		TEXT_LABEL_63 tl63_EntityName = "  Unit: "
		tl63_EntityName += i
		tl63_EntityName += "  Gang Type: "
		tl63_EntityName += MC_serverBD_1.sGangChase[i].iGangType
		
		IF NOT IS_BIT_SET(iDebugGangChaseUnitDeadBS, i)						
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		// Alive
			sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
		ELIF MC_serverBD_1.sGangChase[i].iGangChaseState = ciGANG_CHASE_UNIT_STATE_WAIT_FOR_CREATION
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200) 	// Spawning
		ELSE
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)		// Inactive
		ENDIF
		
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_EntityName, fEntityNamePosX, fEntityNamePosY, fTextSize)
		
		iRowsDrawn++
	ENDFOR	
	
ENDPROC

PROC PROCESS_WARP_PORTALS_STATE_DEBUG_WINDOW()

	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tl63_EntityName
	TEXT_LABEL_63 tl63_EntityKey1
	TEXT_LABEL_63 tl63_EntityKey2
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 2
	SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		CASE 0
			tl63_EntityKey1 = "Key: Creator ID : Positon : Linked To"
		BREAK
		CASE 1
			tl63_EntityKey1 = "Key: Creator ID : Attach Type : Attach ID"
		BREAK
	ENDSWITCH
		
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)
	
		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
				
		tl63_Name = "Number of Placed Warp Portals: "
		tl63_Name += sWarpPortal.iMaxNumberOfPortals
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 1, 0)
				
		tl63_Name = "Number of Active Warp Portals: "
		tl63_Name += sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 1, 0)
						
		// Legend and Key at the bottom -------------
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey1, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey2, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Exists / Active", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.01)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Spawns On Later Rule", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.11)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Dead / Spawn Blocked", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.21)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.1+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.2+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)
	ENDIF
	
	// Main Draw Loop -------------------------
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	INT i = 0
	FOR i = 0 TO sWarpPortal.iMaxNumberOfPortals-1
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * i) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
		
		// Warp Click Event (Player Ped)		
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_1, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Camera)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_2, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
				
		tl63_EntityName = "  W.Port: "
		tl63_EntityName += i
			
		IF g_FMMC_STRUCT.sWarpPortals[i].iLinkedPortal = -1
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)
			
		ELIF IS_PORTAL_ACTIVE_AND_AVAILABLE(i, MC_PlayerBD[iLocalPart].iTeam, GET_TEAM_CURRENT_RULE(MC_PlayerBD[iLocalPart].iTeam), FALSE, MC_GET_WARP_PORTAL_START_POSITION(i), MC_GET_WARP_PORTAL_END_POSITION(i) #IF IS_DEBUG_BUILD , FALSE #ENDIF)	
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		// Alive			
			sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
		ELSE
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200) 	// Not Spawned yet but can.			
		ENDIF
		
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0
				tl63_EntityName += " : "
				VECTOR vTemp
				vTemp = MC_GET_WARP_PORTAL_START_POSITION(i)
				
				tl63_EntityName += " : "
				tl63_EntityName += "<"
				tl63_EntityName += ROUND(vTemp.x)
				tl63_EntityName += ", "
				tl63_EntityName += ROUND(vTemp.y)
				tl63_EntityName += ", "
				tl63_EntityName += ROUND(vTemp.z)
				tl63_EntityName += ">"
				
				tl63_EntityName += " : "
				tl63_EntityName += "Link: "
				tl63_EntityName += g_FMMC_STRUCT.sWarpPortals[i].iLinkedPortal
			BREAK
			CASE 1	
				IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkType != ciENTITY_TYPE_NONE
				AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex != -1
				AND DOES_ENTITY_EXIST(GET_FMMC_ENTITY(g_FMMC_STRUCT.sWarpPortals[i].iAttachToEntityType, g_FMMC_STRUCT.sWarpPortals[i].iAttachToEntityID))
					tl63_EntityName += " : "
					tl63_EntityName += "Attached to"
					tl63_EntityName += " : "
					TEXT_LABEL_15 tlEntityType
					tlEntityType = "FMMC_ENTITY_"
					tlEntityType += g_FMMC_STRUCT.sWarpPortals[i].iAttachToEntityType
					STRING sNameT
					sNameT = GET_FILENAME_FOR_AUDIO_CONVERSATION(tlEntityType)					
					tl63_EntityName += sNameT
					tl63_EntityName += " "
					tl63_EntityName += g_FMMC_STRUCT.sWarpPortals[i].iAttachToEntityID
				ENDIF
			BREAK
		ENDSWITCH
		
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_EntityName, fEntityNamePosX, fEntityNamePosY, fTextSize)
	ENDFOR
ENDPROC

PROC PROCESS_TEAM_SPAWN_POINTS_STATE_DEBUG_WINDOW()
	
	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
	
	INT i
	TEXT_LABEL_63 tl63_EntityName
	TEXT_LABEL_63 tl63_EntityKey1
	TEXT_LABEL_63 tl63_EntityKey2
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 2
	SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		CASE 0
			tl63_EntityKey1 = "Key: Creator ID : Pos : Heading : Vehicle : Seat"
		BREAK
		CASE 1
			tl63_EntityKey1 = "Key: Creator ID : Attach Type : Attach ID"
		BREAK
	ENDSWITCH
	
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)
	
		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
		
		tl63_Name += "Number of Placed Team Spawn Points: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[sContentOverviewDebug.iTeamIndexSelected]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		tl63_Name = "Number of Valid Spawn Points: "
		tl63_Name += sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 1, 0)
		
		// Middle Extra Info Draw ---------------
		
		sContentOverviewDebug.iLegendPrintY = 0
		
		tl63_Name = "Local Player Only Info: "
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		tl63_Name = "Last Spawn Location: "
		tl63_Name += "<"
		tl63_Name += ROUND(vDebugSpawnLastSpawnLocation.x)
		tl63_Name += ", "
		tl63_Name += ROUND(vDebugSpawnLastSpawnLocation.y)
		tl63_Name += ", "
		tl63_Name += ROUND(vDebugSpawnLastSpawnLocation.z)
		tl63_Name += ">"
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		tl63_Name = "Last Spawn Heading: "
		tl63_Name += ROUND(fDebugSpawnLastSpawnHeading)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)	
		
		tl63_Name = "Last Died Location: "
		tl63_Name += "<"
		tl63_Name += ROUND(vDebugSpawnLastDeathLocation.x)
		tl63_Name += ", "
		tl63_Name += ROUND(vDebugSpawnLastDeathLocation.y)
		tl63_Name += ", "
		tl63_Name += ROUND(vDebugSpawnLastDeathLocation.z)	
		tl63_Name += ">"
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		tl63_Name = "Last Died Heading: "
		tl63_Name += ROUND(fDebugSpawnLastDeathHeading)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 1)	
				
		sContentOverviewDebug.iLegendPrintY = 0
		BOOL bSpawnAreaActive		
		SPAWN_AREA SpArea
		INT iArea = 0	
		IF g_SpawnData.MissionSpawnDetails.SpawnArea[iArea].bIsActive
			bSpawnAreaActive = TRUE		
		ENDIF
		
		IF bSpawnAreaActive
			tl63_Name = "Using Spawn Areas"
			DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)							
			
			SpArea = g_SpawnData.MissionSpawnDetails.SpawnArea[iArea]						
			IF SpArea.iShape = SPAWN_AREA_SHAPE_BOX
				DRAW_BOX(SpArea.vCoords1, SpArea.vCoords2, 200, 200, 200, 150)
				tl63_Name = "Axis Aligned Box"
				DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)				
				
				VECTOR vTemp = (SpArea.vCoords1+SpArea.vCoords2)
				vTemp.x /= 2
				vTemp.y /= 2
				vTemp.z /= 2
				tl63_Name = "Pos: "
				tl63_Name += "<"
				tl63_Name += ROUND(vTemp.x)
				tl63_Name += ", "
				tl63_Name += ROUND(vTemp.y)
				tl63_Name += ", "
				tl63_Name += ROUND(vTemp.z)
				tl63_Name += ">"
				DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
				DRAW_DEBUG_TEXT_ABOVE_COORDS(vTemp, "Axis Aligned Spawn Box", 1.5, 100, 250, 100, 200)
				
			ELIF SpArea.iShape = SPAWN_AREA_SHAPE_CIRCLE
				DRAW_MARKER(MARKER_SPHERE, SpArea.vCoords1, (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>), (<<SpArea.fFloat, SpArea.fFloat, SpArea.fFloat>>), 200, 200, 200, 150)				
				tl63_Name = "Sphere"
				DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)			
				
				tl63_Name = "Pos: "
				tl63_Name += "<"
				tl63_Name += ROUND(SpArea.vCoords1.x)
				tl63_Name += ", "
				tl63_Name += ROUND(SpArea.vCoords1.y)
				tl63_Name += ", "
				tl63_Name += ROUND(SpArea.vCoords1.z)
				tl63_Name += ">"
				DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
				DRAW_DEBUG_TEXT_ABOVE_COORDS(SpArea.vCoords1, "Spawn Sphere", 1.5, 100, 250, 100, 200)
				
			ELIF SpArea.iShape = SPAWN_AREA_SHAPE_ANGLED
				DRAW_ANGLED_AREA_FROM_FACES(SpArea.vCoords1, SpArea.vCoords2, SpArea.fFloat, 200, 200, 200, 150)
				tl63_Name = "Angled Area"
				DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)				
				
				VECTOR vTemp = (SpArea.vCoords1+SpArea.vCoords2)
				vTemp.x /= 2
				vTemp.y /= 2
				vTemp.z /= 2
				tl63_Name = "Pos: "
				tl63_Name += "<"
				tl63_Name += ROUND(vTemp.x)
				tl63_Name += ", "
				tl63_Name += ROUND(vTemp.y)
				tl63_Name += ", "
				tl63_Name += ROUND(vTemp.z)
				tl63_Name += ">"
				DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
				DRAW_DEBUG_TEXT_ABOVE_COORDS(vTemp, "Spawn Angled Area", 1.5, 100, 250, 100, 200)
					
			ENDIF			
		
		ELIF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_USING_CUSTOM_RESPAWN_POINTS)				
			
			tl63_Name = "PREVIOUS Spawn Points Active: "
			FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[sContentOverviewDebug.iTeamIndexSelected]-1
				IF FMMC_IS_LONG_BIT_SET(iDebugPreviousSpawnPointsActiveBS, i)
					tl63_Name += i
					tl63_Name += ", "
				ENDIF
			ENDFOR			
			DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
						
			tl63_Name = "CURRENT Spawn Points Active: "
			FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[sContentOverviewDebug.iTeamIndexSelected]-1
				IF FMMC_IS_LONG_BIT_SET(iDebugSpawnPointsActiveBS, i)
					tl63_Name += i
					tl63_Name += ", "
				ENDIF
			ENDFOR
			DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		ELIF g_SpawnData.MissionSpawnGeneralBehaviours.DefaultSpawnLocation = SPAWN_LOCATION_NEAR_DEATH						
			tl63_Name = "Using Default / Near Death default spawning"
			DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
			
		ELSE
			tl63_Name = "Using Ambient DefaultSpawnLocation ENUM_TO_INT: "
			tl63_Name += ENUM_TO_INT(g_SpawnData.MissionSpawnGeneralBehaviours.DefaultSpawnLocation)
			DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)			
			
		ENDIF		
		
		// Legend and Key at the bottom -------------	
		sContentOverviewDebug.iLegendPrintY = 5
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey1, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey2, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Valid", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.01)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Not Valid Yet", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.11)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Blocked", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.21)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.1+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.2+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)
	ENDIF
	
	// Main Draw Loop -------------------------
	
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[sContentOverviewDebug.iTeamIndexSelected]-1
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * i) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
		
		// Warp Click Event (Camera)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_1, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Player Ped)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_2, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		tl63_EntityName = "  Point: "
		tl63_EntityName += i
			
		IF FMMC_IS_LONG_BIT_SET(iRespawnPointIsBlockedBS[sContentOverviewDebug.iTeamIndexSelected], i)
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)		// Blocked
			
		ELIF IS_SPAWN_POINT_A_VALID_RESPAWN_POINT(i, sContentOverviewDebug.iTeamIndexSelected, TRUE)
		 	DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		// Valid
		 	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
		 	
		ELSE
		 	DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200) 	// Not Valid Yet
						
		ENDIF
				
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0
				VECTOR vTemp
				FLOAT fTemp
				vTemp = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sContentOverviewDebug.iTeamIndexSelected][i].vPos
				fTemp = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sContentOverviewDebug.iTeamIndexSelected][i].fHead
				
				REPOSITION_TEAM_SPAWN_POINT_BASED_ON_ENTITY(vTemp, fTemp, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sContentOverviewDebug.iTeamIndexSelected][i].iMoveToEntityIndex, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sContentOverviewDebug.iTeamIndexSelected][i].iMoveToEntityType)
				
				tl63_EntityName += " : "
				tl63_EntityName += "<"
				tl63_EntityName += ROUND(vTemp.x)
				tl63_EntityName += ", "
				tl63_EntityName += ROUND(vTemp.y)
				tl63_EntityName += ", "
				tl63_EntityName += ROUND(vTemp.z)
				tl63_EntityName += ">"
				 
				tl63_EntityName += " : "
				tl63_EntityName += ROUND(fTemp)
				tl63_EntityName += " : "
				tl63_EntityName += g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sContentOverviewDebug.iTeamIndexSelected][i].iVehicle
				
				tl63_EntityName += " : "
				tl63_EntityName += GET_SEAT_NAME_FOR_FORCE_OPTION(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sContentOverviewDebug.iTeamIndexSelected][i].iSeat)			
			BREAK
			CASE 1
				tl63_EntityName += " : "
				tl63_EntityName += g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sContentOverviewDebug.iTeamIndexSelected][i].iMoveToEntityType
				
				tl63_EntityName += " : "
				tl63_EntityName += g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sContentOverviewDebug.iTeamIndexSelected][i].iMoveToEntityIndex
			BREAK
		ENDSWITCH
		
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_EntityName, fEntityNamePosX, fEntityNamePosY, fTextSize)
	ENDFOR
ENDPROC

PROC PROCESS_DIALOGUE_TRIGGER_STATE_DEBUG_WINDOW()

	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tl63_EntityName
	TEXT_LABEL_63 tl63_EntityKey1
	TEXT_LABEL_63 tl63_EntityKey2
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 2
	SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		CASE 0
			tl63_EntityKey1 = "Trigger Key: Creator ID : Block : Root : Playing"
		BREAK
		CASE 1
			tl63_EntityKey1 = "Trigger Key: Creator ID : Conversation/Help"
		BREAK
	ENDSWITCH
	
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)
	
		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
				
		tl63_Name += "Number of Placed Triggers: "
		tl63_Name += g_FMMC_STRUCT.iNumberOfDialogueTriggers
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 1, 0)
		tl63_Name = "Number of Current Triggers Played: "
		tl63_Name += sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 2)
		
		// Middle Extra Info Draw ---------------
		
		tl63_Name = "Conversation Status: "
		IF NOT IS_CONVERSATION_STATUS_FREE()
		OR IS_SCRIPTED_CONVERSATION_ONGOING()
			tl63_Name += "Playing"
		ELSE
			tl63_Name += "Waiting"
		ENDIF
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		tl63_Name = "Last Played Dialogue: "
		tl63_Name += iDialogueProgressLastPlayed
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		tl63_Name = "Dialogue Progress: "
		tl63_Name += iDialogueProgress
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
			
		// Legend and Key at the bottom -------------	
		sContentOverviewDebug.iLegendPrintY++		
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey1, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey2, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Played", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.01)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Not Played", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.11)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Blocked", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.21)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.1+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.2+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)
	ENDIF
	
	// Main Draw Loop -------------------------
	
	INT iTrigStart, iTrigEnd
	IF g_FMMC_STRUCT.iNumberOfDialogueTriggers > 60
		IF sContentOverviewDebug.iToggleEntitiesDueToTooMuchTextDraws = 1
			iTrigStart = 60
			iTrigEnd = g_FMMC_STRUCT.iNumberOfDialogueTriggers
		ELSE	
			iTrigStart = 0
			iTrigEnd = 60
		ENDIF
	ELSE
		iTrigStart = 0
		iTrigEnd = g_FMMC_STRUCT.iNumberOfDialogueTriggers
	ENDIF
	
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	INT i = 0
	FOR i = iTrigStart TO iTrigEnd-1
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * ((i-iTrigStart) / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * (i-iTrigStart)) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * ((i-iTrigStart) / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))				
				
		// Click Event Play/Trigger
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_1, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_PLAY_OR_TRIGGER, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Click Event Reset Trigger
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_2, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_RESET, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Camera)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_3, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Player Ped)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_4, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		tl63_EntityName = "  Trig: "
		tl63_EntityName += i
			
		IF (HAS_DIALOGUE_INDEX_PLAYED(i) OR IS_BIT_SET(iLocalDialoguePlayingBS[i/32], i%32))
		AND (NOT IS_BIT_SET(iLocalDialogueSkippedBS[i/32], i%32) AND NOT IS_BIT_SET(iLocalDialogueBlockedTempBS[i/32], i%32))
		 	DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		// Alive			
		 	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
		 	
		ELIF NOT IS_BIT_SET(iLocalDialogueSkippedBS[i/32], i%32)
		 	DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200) 	// Not Spawned yet but can.
			
		ELSE
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)		// Dead/Can't Spawn
			
		ENDIF
				
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0				
				tl63_EntityName += " : "
				tl63_EntityName += g_FMMC_STRUCT.sDialogueTriggers[i].tlBlock 	
				 
				tl63_EntityName += " : "				
				tl63_EntityName += g_FMMC_STRUCT.sDialogueTriggers[i].tlRoot
					
				IF iDialogueProgressLastPlayed = i
				AND (NOT IS_CONVERSATION_STATUS_FREE()
				OR IS_SCRIPTED_CONVERSATION_ONGOING())
					tl63_EntityName += " : "
					tl63_EntityName += "Playing this"
				ENDIF
			BREAK
			CASE 1	
				IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset, iBS_DialogueHelpText)
				OR IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset2, iBS_Dialogue2HelpTextNGOnly)	
					tl63_EntityName += " : "
					tl63_EntityName += "Help Text"
				ELSE
					tl63_EntityName += " : "
					tl63_EntityName += "Conversation"
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sDialogueTriggers[i].tlDialogueTriggerAdditionalHelpText)
					tl63_EntityName += " : "
					tl63_EntityName += "Help Text"
				ENDIF
			BREAK
		ENDSWITCH
		
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_EntityName, fEntityNamePosX, fEntityNamePosY, fTextSize)
	ENDFOR
ENDPROC

PROC PROCESS_LOCATION_STATE_DEBUG_WINDOW()

	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tl63_EntityName
	TEXT_LABEL_63 tl63_EntityKey1
	TEXT_LABEL_63 tl63_EntityKey2
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 2
	SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		CASE 0
			tl63_EntityKey1 = "Location Key: Creator ID : Distance : Inside : Requires Veh"			
		BREAK
		CASE 1
			tl63_EntityKey1 = "Location Key: Creator ID : Nearest Locate : Player Count Req."
		BREAK
	ENDSWITCH
		
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)
	
		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
		
		tl63_Name += "Number of Placed Locations: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 1, 0)
		
		tl63_Name = "Number of Current Locations: "
		tl63_Name += sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 0, 2)
		
		// Middle Extra Info Draw ---------------
		tl63_Name = "Current Player Location: "
		tl63_Name = MC_playerBD[iPartToUse].iCurrentLoc		
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		
		// Legend and Key at the bottom -------------
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey1, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey2, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Exists / Active", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.01)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Spawns On Later Rule", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.11)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Dead / Spawn Blocked", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.21)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.1+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.2+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)
	ENDIF
	
	// Main Draw Loop -------------------------
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	INT i = 0
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]-1
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * i) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
		
		// Warp Click Event (Player Ped)		
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_1, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Camera)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_2, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		
		tl63_EntityName = "  Loc: "
		tl63_EntityName += i
			
		IF MC_serverBD_4.iGotoLocationDataRule[i][0] != FMMC_OBJECTIVE_LOGIC_NONE
		AND MC_serverBD_4.iGotoLocationDataPriority[i][0] < FMMC_MAX_RULES
		AND MC_serverBD_4.iGotoLocationDataPriority[i][0] <= iCurrentHighPriority[0]		
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		// Alive			
			sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
			
		ELIF MC_serverBD_4.iGotoLocationDataRule[i][0] != FMMC_OBJECTIVE_LOGIC_NONE
		AND MC_serverBD_4.iGotoLocationDataPriority[i][0] < FMMC_MAX_RULES
		AND MC_serverBD_4.iGotoLocationDataPriority[i][0] > iCurrentHighPriority[0]
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200) 	// Not Spawned yet but can.
			
		ELSE
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)		// Dead/Can't Spawn
			
		ENDIF
		
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0
				tl63_EntityName += " : "
				tl63_EntityName += "Distance: "
				tl63_EntityName += ROUND(SQRT(GET_DIST2_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, GET_LOCATION_VECTOR(i))))
				
				IF IS_LOCAL_PLAYER_IN_LOCATION(i,  0)
					tl63_EntityName += " : "
					tl63_EntityName += "Inside"
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iLocBS3, ciLoc_BS3_SpecificVehModelFullBlocker)
					tl63_EntityName += " : "
					IF IS_PLAYER_IN_REQUIRED_LOCATION_VEHICLE(PlayerPedToUse, 0, i)
						tl63_EntityName += "Correct Req. Veh"
					ELSE
						tl63_EntityName += "Wrong Req. Veh"
					ENDIF
				ENDIF
			BREAK
			CASE 1	
				IF iNearestTargetTemp = i
					tl63_EntityName += " : "
					tl63_EntityName += "This is Nearest Loc!"
				ENDIF
				
				IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iWholeTeamAtLocation[0] = ciGOTO_LOCATION_WHOLE_TEAM
					tl63_EntityName += " : "
					tl63_EntityName += "Req. Whole Team"
				ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iWholeTeamAtLocation[0] = ciGOTO_LOCATION_ALL_PLAYERS
					tl63_EntityName += " : "
					tl63_EntityName += "Req. All Players"
				ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iWholeTeamAtLocation[0] >= ciGOTO_LOCATION_2_PLAYERS
					tl63_EntityName += " : "
					tl63_EntityName += "Req. "
					tl63_EntityName += g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iWholeTeamAtLocation[0]
					tl63_EntityName += "Players"
				ELSE
					tl63_EntityName += " : "
					tl63_EntityName += "Standard"
				ENDIF				
			BREAK
		ENDSWITCH
		
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_EntityName, fEntityNamePosX, fEntityNamePosY, fTextSize)
	ENDFOR
ENDPROC

PROC PROCESS_ZONE_STATE_DEBUG_WINDOW()

	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tl63_EntityName
	TEXT_LABEL_63 tl63_EntityKey1
	TEXT_LABEL_63 tl63_EntityKey2
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 2
	SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		CASE 0
			tl63_EntityKey1 = "Zone Key: Creator ID : Triggered : Blocked"			
		BREAK
		CASE 1
			tl63_EntityKey1 = "Zone Key: Creator ID : Zone Name"
		BREAK		
		CASE 2
			tl63_EntityKey1 = "Zone Key: Creator ID : Number of Players triggering"
		BREAK
	ENDSWITCH
	
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)
	
		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
		
		tl63_Name += "Number of Placed Zones: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfZones
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 1, 0)
		
		tl63_Name = "Number of Current Zones: "
		tl63_Name += sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 0, 2)
	
		// Middle Extra Info Draw ---------------
		
		
		// Legend and Key at the bottom -------------
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey1, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey2, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		sContentOverviewDebug.iLegendPrintX = 0		
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Exists / Active", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.01)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Spawns On Later Rule", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.11)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Dead / Spawn Blocked", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.21)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.1+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.2+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)
	ENDIF
	
	// Main Draw Loop -------------------------
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	INT i = 0	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfZones-1
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * i) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))

		// Warp Click Event (Player Ped)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_1, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Camera)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_2, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Click Event (Show Debug)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_3, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_DRAW_MARKER_DEBUG, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		tl63_EntityName = "  Zone: "
		tl63_EntityName += i		
			
		IF DOES_ZONE_EXIST(i)
		 	DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		// Alive			
		 	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
			
		ELIF NOT IS_ZONE_PAST_VALID_RULE(i)
		AND NOT IS_BIT_SET(iLocalZoneRemovedBitset, i)
		 	DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200) 	// Not Spawned yet but can.
			
		ELSE
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)		// Dead/Can't Spawn
			
		ENDIF
				
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0
			
				IF DOES_ZONE_EXIST(i)
				AND NOT IS_ZONE_TRIGGERABLE(i)
					tl63_EntityName += " : "
					tl63_EntityName += "Can't trigger yet"
				ELIF IS_ZONE_TRIGGERING(i)
					tl63_EntityName += " : "
					tl63_EntityName += "Triggering"
				ENDIF
				
				IF IS_ZONE_TRIGGER_BLOCKED(i)
					tl63_EntityName += " : "
					tl63_EntityName += "Blocked"
				ENDIF
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkType != ciENTITY_TYPE_NONE
				AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex != -1
				AND DOES_ENTITY_EXIST(GET_FMMC_ENTITY(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkType, g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex, g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLink_TrainCarriage))
					tl63_EntityName += " : "
					tl63_EntityName += "Attached to"
					tl63_EntityName += " : "
					TEXT_LABEL_15 tlEntityType
					tlEntityType = "FMMC_ENTITY_"
					tlEntityType += g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkType
					STRING sZoneName
					sZoneName = GET_FILENAME_FOR_AUDIO_CONVERSATION(tlEntityType)					
					tl63_EntityName += sZoneName
					tl63_EntityName += " "
					tl63_EntityName += g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex
				ENDIF
			BREAK
			CASE 1
				TEXT_LABEL_15 tlZoneType
				tlZoneType = "FMMC_ZN_TY"
				tlZoneType += GET_FMMC_ZONE_TYPE(i)
				STRING sZoneName
				sZoneName = GET_FILENAME_FOR_AUDIO_CONVERSATION(tlZoneType)

				tl63_EntityName += " : "
				tl63_EntityName += sZoneName
			BREAK
			CASE 2	
				IF GET_NUM_PLAYERS_TRIGGERING_ZONE(i) != 0
					tl63_EntityName += " : "
					tl63_EntityName += "Triggered by "
					tl63_EntityName += GET_NUM_PLAYERS_TRIGGERING_ZONE(i)
					tl63_EntityName += " Players"
				ENDIF
			BREAK
		ENDSWITCH
		
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_EntityName, fEntityNamePosX, fEntityNamePosY, fTextSize)
	ENDFOR
ENDPROC

PROC PROCESS_PROP_STATE_DEBUG_WINDOW()
	
	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
		
	TEXT_LABEL_63 tl63_EntityName
	TEXT_LABEL_63 tl63_EntityKey1
	TEXT_LABEL_63 tl63_EntityKey2
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 2
	SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		CASE 0
			tl63_EntityKey1 = "Prop Key: Creator ID : Model Name"
			tl63_EntityKey2 = ""
		BREAK
		CASE 1
			tl63_EntityKey1 = "Prop Key: Creator ID : Health"
			tl63_EntityKey2 = ""
		BREAK
	ENDSWITCH
			
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)		
		
		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
		
		tl63_Name += "Number of Placed Prop: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfProps
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 1, 0)		
		
		tl63_Name = "Number of Current Prop: "
		tl63_Name += sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 0, 2)
		
		// Middle Extra Info Draw ---------------
		
		
		// Legend and Key at the bottom -------------
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++		
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey1, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey2, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		sContentOverviewDebug.iLegendPrintX = 0			
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Exists / Active", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.01)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Spawns On Later Rule", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.11)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Dead / Spawn Blocked", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.21)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.1+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.2+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)
	ENDIF
	
	// Main Draw Loop -------------------------
	
	INT iPropStart, iPropEnd
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfProps > 100
		IF sContentOverviewDebug.iToggleEntitiesDueToTooMuchTextDraws = 1
			iPropStart = 100
			iPropEnd = g_FMMC_STRUCT_ENTITIES.iNumberOfProps
		ELSE	
			iPropStart = 0
			iPropEnd = 100
		ENDIF
	ELSE
		iPropStart = 0
		iPropEnd = g_FMMC_STRUCT_ENTITIES.iNumberOfProps
	ENDIF
	
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	OBJECT_INDEX tempEntity	
	INT i = 0
	FOR i = iPropStart TO iPropEnd-1		
		tempEntity = oiProps[i]
		
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize*0.8
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * ((i-iPropStart) / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * (i-iPropStart)) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * ((i-iPropStart) / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
		
		// Warp Click Event (Player Ped)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_1, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Camera)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_2, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		tl63_EntityName = "  Prop: "
		tl63_EntityName += i
		
		IF NOT IS_ENTITY_DEAD(tempEntity)																							
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		// Alive		
			sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
			
		ELIF CAN_PROP_STILL_SPAWN_IN_MISSION(i)
		 	DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200) 	// Not Spawned yet but can.
						
		ELSE			
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)		// Dead/Can't Spawn anymore
			
		ENDIF
		
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0			
				tl63_EntityName += " : "
				tl63_EntityName += FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn)	
			BREAK
			CASE 1
				IF NOT IS_ENTITY_DEAD(tempEntity)
					INT iHealth
					FLOAT fHealthCurrent
					fHealthCurrent = TO_FLOAT(GET_ENTITY_HEALTH(tempEntity))
					FLOAT fHealthMax
					fHealthMax = TO_FLOAT(GET_ENTITY_MAX_HEALTH(tempEntity))
					FLOAT fPercent
					fPercent = (fHealthCurrent / fHealthMax) * 100
					iHealth = ROUND(fPercent)
					
					tl63_EntityName += " : "
					tl63_EntityName += iHealth
					tl63_EntityName += "%"
				ENDIF
			BREAK
		ENDSWITCH
				
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_EntityName, fEntityNamePosX, fEntityNamePosY, fTextSize)		
	ENDFOR
ENDPROC

PROC PROCESS_DYNOPROP_STATE_DEBUG_WINDOW()

	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tl63_EntityName
	TEXT_LABEL_63 tl63_EntityKey1
	TEXT_LABEL_63 tl63_EntityKey2
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 3
	SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		CASE 0
			tl63_EntityKey1 = "Dyno Prop Key: Creator ID : NET ID"
			tl63_EntityKey2 = ""
		BREAK
		CASE 1
			tl63_EntityKey1 = "Dyno Prop Key: Creator ID : Model Name : Health"
			tl63_EntityKey2 = ""
		BREAK
		CASE 2
			tl63_EntityKey1 = "Dyno Prop Key: Creator ID : Net ID : Network Ownership"
			tl63_EntityKey2 = ""
		BREAK
	ENDSWITCH
		
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)	
	
		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
		
		tl63_Name += "Number of Placed Dyno Prop: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 1, 0)
		
		tl63_Name = "Number of Current Dyno Prop: "
		tl63_Name += sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 0, 2)
		
		// Middle Extra Info Draw ---------------
		
		
		// Legend and Key at the bottom -------------
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
				
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey1, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey2, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		sContentOverviewDebug.iLegendPrintX = 0		
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Exists / Active", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.01)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Spawns On Later Rule", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.11)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Dead / Spawn Blocked", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.21)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.1+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.2+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)
	ENDIF
	
	// Main Draw Loop -------------------------
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	OBJECT_INDEX tempEntity
	NETWORK_INDEX netEntity	
	INT i = 0
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps-1		
		netEntity = MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_DYNOPROP_NET_ID_INDEX(i)]
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netEntity)
			tempEntity = NET_TO_OBJ(netEntity)			
		ELSE
			tempEntity = NULL
		ENDIF
		
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * i) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
		
		// Warp Click Event (Player Ped)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_1, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Camera)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_2, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		tl63_EntityName = "  Dyno: "
		tl63_EntityName += i
		IF NOT IS_ENTITY_DEAD(tempEntity) AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)	
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		// Alive		
			sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
			
		ELIF CAN_DYNOPROP_STILL_SPAWN_IN_MISSION(i)
		 	DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200) 	// Not Spawned yet but can.
		
		ELSE																														
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)		// Dead/Can't Spawn anymore
			
		ENDIF
		
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0
				tl63_EntityName += " ("
				IF DOES_ENTITY_EXIST(tempEntity)
				AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += NETWORK_ENTITY_GET_OBJECT_ID(tempEntity)
				ELSE
					tl63_EntityName += "--"
				ENDIF
				tl63_EntityName += ")"
				
				IF iDynopropCachedCamIndexes[i] != -1
					tl63_EntityName += " : "
					tl63_EntityName += " Cam Index: "
					tl63_EntityName += iDynopropCachedCamIndexes[i]
				ENDIF
				
				IF HAS_OBJECT_BEEN_BROKEN(tempEntity, FALSE)
					tl63_EntityName += " : "
					tl63_EntityName += " Broken"
				ENDIF
			BREAK
			CASE 1
				tl63_EntityName += " : "
				tl63_EntityName += FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn)
				
				IF NOT IS_ENTITY_DEAD(tempEntity)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iPropHealthOverride != -1
						INT iHealth
						FLOAT fHealthCurrent
						fHealthCurrent = TO_FLOAT(GET_ENTITY_HEALTH(tempEntity))
						FLOAT fHealthMax
						fHealthMax = TO_FLOAT(GET_ENTITY_MAX_HEALTH(tempEntity))
						FLOAT fPercent
						fPercent = (fHealthCurrent / fHealthMax) * 100
						iHealth = ROUND(fPercent)
						
						tl63_EntityName += " : "
						tl63_EntityName += iHealth
						tl63_EntityName += "%"
					ELSE
						tl63_EntityName += " : "
						tl63_EntityName += "100%"
					ENDIF
				ENDIF
			BREAK
			CASE 2
				tl63_EntityName += " : "
				tl63_EntityName += "(NetID "
				IF DOES_ENTITY_EXIST(tempEntity)
				AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += NETWORK_ENTITY_GET_OBJECT_ID(tempEntity)
				ELSE
					tl63_EntityName += "--"
				ENDIF
				tl63_EntityName += ")"
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += " : "
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempEntity)
						tl63_EntityName += "Locally Owned"
					ELSE
						tl63_EntityName += "Remotely Owned"
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
				
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_EntityName, fEntityNamePosX, fEntityNamePosY, fTextSize)
	ENDFOR
ENDPROC

PROC PROCESS_INTERACTABLE_STATE_DEBUG_WINDOW()

	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF

	TEXT_LABEL_63 tl63_EntityName
	TEXT_LABEL_63 tl63_EntityKey1
	TEXT_LABEL_63 tl63_EntityKey2
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 3
	SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		CASE 0
			tl63_EntityKey1 = "Interactable Key: Creator ID : NET ID : Completion : "
			tl63_EntityKey2 = "Player Using"
		BREAK
		CASE 1
			tl63_EntityKey1 = "Interactable Key: Creator ID : Model Name : Health"
			tl63_EntityKey2 = ""
		BREAK
		CASE 2
			tl63_EntityKey1 = "Interactable Key: Creator ID : Net ID : Network Ownership"
			tl63_EntityKey2 = ""
		BREAK
	ENDSWITCH
		
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)			
	
		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
		
		tl63_Name += "Number of Placed Interactables: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 1, 0)
		
		tl63_Name = "Number of Current Interactables: "
		tl63_Name += sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 0, 2)
		
		// Middle Extra Info Draw ---------------
		sContentOverviewDebug.iLegendPrintY++
		
		// Legend and Key at the bottom -------------
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey1, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey2, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		sContentOverviewDebug.iLegendPrintX = 0		
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Exists / Active", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.01)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Spawns On Later Rule", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.11)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Dead / Spawn Blocked", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.21)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.1+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.2+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)
	ENDIF
	
	// Main Draw Loop -------------------------
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	STRING sPlayerName = ""	
	OBJECT_INDEX tempEntity
	NETWORK_INDEX netEntity	
	INT i = 0
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables-1		
		sPlayerName = ""
		
		netEntity = MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_INTERACTABLE_NET_ID_INDEX(i)]
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netEntity)
			tempEntity = NET_TO_OBJ(netEntity)			
		ELSE
			tempEntity = NULL
		ENDIF
		
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * i) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
		
		// Warp Click Event (Player Ped)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_1, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Camera)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_2, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		tl63_EntityName = "  Int: "
		tl63_EntityName += i
		IF NOT IS_ENTITY_DEAD(tempEntity) AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		// Alive
			sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
			
		ELIF CAN_INTERACTABLE_STILL_SPAWN_IN_MISSION(i)
		 	DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200) 	// Not Spawned yet but can.
		
		ELSE																														
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)		// Dead/Can't Spawn anymore
			
		ENDIF
		
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0
				tl63_EntityName += " ("
				IF DOES_ENTITY_EXIST(tempEntity)
				AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += NETWORK_ENTITY_GET_OBJECT_ID(tempEntity)
				ELSE
					tl63_EntityName += "--"
				ENDIF
				tl63_EntityName += ")"
				
				IF IS_INTERACTABLE_COMPLETE(i)
					tl63_EntityName += " : "
					tl63_EntityName += "Complete"
				ELSE
					tl63_EntityName += " : "
					tl63_EntityName += "Not Done"
				ENDIF
				
				INT iPartUsing
				iPartUsing = GET_INTERACTABLE_CURRENT_USER(i)
				IF iPartUsing > -1
					tl63_EntityName += " : "
					
					PARTICIPANT_INDEX piPart
					piPart = INT_TO_PARTICIPANTINDEX(iPartUsing)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
						PLAYER_INDEX piPlayer
						piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
						sPlayerName = GET_PLAYER_NAME(piPlayer)
						tl63_EntityName += sPlayerName
					ELSE
						tl63_EntityName += "Invalid"
					ENDIF
					
					tl63_EntityName += " is Using"
				ELSE
					tl63_EntityName += " : "
					tl63_EntityName += "None Using"
				ENDIF
			BREAK
			CASE 1
				tl63_EntityName += " : "
				tl63_EntityName += FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].mnInteractable_Model)
				
				IF NOT IS_ENTITY_DEAD(tempEntity)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_Health != -1
						INT iHealth
						FLOAT fHealthCurrent
						fHealthCurrent = TO_FLOAT(GET_ENTITY_HEALTH(tempEntity))
						FLOAT fHealthMax
						fHealthMax = TO_FLOAT(GET_ENTITY_MAX_HEALTH(tempEntity))
						FLOAT fPercent
						fPercent = (fHealthCurrent / fHealthMax) * 100
						iHealth = ROUND(fPercent)
						
						tl63_EntityName += " : "
						tl63_EntityName += iHealth
						tl63_EntityName += "%"
					ELSE
						tl63_EntityName += " : "
						tl63_EntityName += "100%"
					ENDIF
				ENDIF
			BREAK
			CASE 2
				tl63_EntityName += " : "
				tl63_EntityName += "(NetID "
				IF DOES_ENTITY_EXIST(tempEntity)
				AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += NETWORK_ENTITY_GET_OBJECT_ID(tempEntity)
				ELSE
					tl63_EntityName += "--"
				ENDIF
				tl63_EntityName += ")"
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += " : "
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempEntity)
						tl63_EntityName += "Locally Owned"
					ELSE
						tl63_EntityName += "Remotely Owned"
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
				
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_EntityName, fEntityNamePosX, fEntityNamePosY, fTextSize)
	ENDFOR
ENDPROC

PROC PROCESS_OBJECTS_STATE_DEBUG_WINDOW()
	
	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tl63_EntityName
	TEXT_LABEL_63 tl63_EntityKey1
	TEXT_LABEL_63 tl63_EntityKey2	
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 3
	SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		CASE 0
			tl63_EntityKey1 = "Object Key: Creator ID : Net ID : Portable : Minigame"
			tl63_EntityKey2 = "+ Camera Index : Attach"
		BREAK
		CASE 1
			tl63_EntityKey1 = "Object Key: Creator ID : Model Name : Respawns : Health"
			tl63_EntityKey2 = ""
		BREAK
		CASE 2
			tl63_EntityKey1 = "Object Key: Creator ID : Net ID : Network Ownership"
			tl63_EntityKey2 = ""
		BREAK
	ENDSWITCH
		
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)			
		
		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()	
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
		
		tl63_Name += "Number of Placed Objects: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfObjects
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 1, 0)
		
		tl63_Name = "Number of Current Objects: "
		tl63_Name += sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 0, 2)
		
		// Middle Extra Info Draw ---------------
		tl63_Name = "Player Object Near: "
		IF MC_playerBD[iPartToUse].iObjNear = -1
			tl63_Name += "None"
		ELSE
			tl63_Name += MC_playerBD[iPartToUse].iObjNear
		ENDIF
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		// Legend and Key at the bottom -------------
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey1, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey2, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		sContentOverviewDebug.iLegendPrintX = 0		
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Exists / Active", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.01)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Spawns On Later Rule", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.11)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Dead / Spawn Blocked", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.21)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.1+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.2+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)
	ENDIF
	
	// Main Draw Loop -------------------------
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	STRING sPlayerName = ""	
	OBJECT_INDEX tempEntity
	NETWORK_INDEX netEntity	
	INT i = 0
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects-1		
		sPlayerName = ""
		
		netEntity = MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)]
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netEntity)
			tempEntity = NET_TO_OBJ(netEntity)			
		ELSE
			tempEntity = NULL
		ENDIF
		
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * i) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
		
		// Warp Click Event (Player Ped)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_1, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Camera)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_2, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Click Event (Show Debug)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_3, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_DRAW_MARKER_DEBUG, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		tl63_EntityName = "  Obj: "
		tl63_EntityName += i
		
		IF NOT IS_ENTITY_DEAD(tempEntity) AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		// Alive						
			sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
			
		ELIF MC_serverBD_2.iCurrentObjRespawnLives[i] > 0
		AND CAN_OBJ_STILL_SPAWN_IN_MISSION(i)
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200) 	// Not Spawned yet but can.
		
		ELSE																												
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)		// Dead/Can't Spawn anymore
			
		ENDIF
				
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0
			
				tl63_EntityName += " ("
				IF DOES_ENTITY_EXIST(tempEntity)
				AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += NETWORK_ENTITY_GET_OBJECT_ID(tempEntity)
				ELSE
					tl63_EntityName += "--"
				ENDIF
				tl63_EntityName += ")"
				
				IF SHOULD_PLACED_OBJECT_BE_PORTABLE(i)
					tl63_EntityName += " : "
					tl63_EntityName += "Portable"
				ELSE
					tl63_EntityName += " : "
					tl63_EntityName += "Stationary"
				ENDIF
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectMinigame != ciFMMC_OBJECT_MINIGAME_TYPE__NONE
					tl63_EntityName += " : "
					tl63_EntityName += "Minigame"
				ENDIF
				
				IF iObjCachedCamIndexes[i] != -1
					tl63_EntityName += " : "
					tl63_EntityName += " Cam Index: "
					tl63_EntityName += iObjCachedCamIndexes[i]
				ENDIF
						
				IF NOT IS_ENTITY_DEAD(tempEntity)
				
					ENTITY_INDEX attachEnt
					attachEnt = GET_ENTITY_ATTACHED_TO(tempEntity)
					
					IF IS_ENTITY_A_PED(attachEnt)			
						PED_INDEX attachPed
						attachPed = GET_PED_INDEX_FROM_ENTITY_INDEX(attachEnt)
						IF NOT IS_PED_INJURED(attachPed)
							
							INT iPed
							iPed = IS_ENTITY_A_MISSION_CREATOR_ENTITY(attachPed)
							IF iPed > -1
								tl63_EntityName += " + Ped "
								tl63_EntityName += iPed
								
							ELIF IS_PED_A_PLAYER(attachPed)
								PLAYER_INDEX piPlayer 
								piPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(attachPed)
								sPlayerName = GET_PLAYER_NAME(piPlayer)
								tl63_EntityName += " : "
								tl63_EntityName += sPlayerName
								
							ENDIF
						ENDIF
					ELIF IS_ENTITY_A_VEHICLE(attachEnt)
						VEHICLE_INDEX attachVeh 
						attachVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(attachEnt)
						IF IS_VEHICLE_DRIVEABLE(attachVeh)
							INT iVeh
							iVeh = IS_ENTITY_A_MISSION_CREATOR_ENTITY(attachVeh)
							IF iVeh > -1
								tl63_EntityName += " + Veh "
								tl63_EntityName += iVeh			
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 1				
				tl63_EntityName += " : "
				tl63_EntityName += FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn)
				
				tl63_EntityName += " : "				
				tl63_EntityName += MC_serverBD_2.iCurrentObjRespawnLives[i]
				
				IF NOT IS_ENTITY_DEAD(tempEntity)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectHealth != -1
						INT iHealth
						FLOAT fHealthCurrent
						fHealthCurrent = TO_FLOAT(GET_ENTITY_HEALTH(tempEntity))
						FLOAT fHealthMax
						fHealthMax = TO_FLOAT(GET_ENTITY_MAX_HEALTH(tempEntity))
						FLOAT fPercent
						fPercent = (fHealthCurrent / fHealthMax) * 100
						iHealth = ROUND(fPercent)
						
						tl63_EntityName += " : "
						tl63_EntityName += iHealth
						tl63_EntityName += "%"
					ELSE
						tl63_EntityName += " : "
						tl63_EntityName += "100%"
					ENDIF
				ENDIF
			BREAK
			CASE 2
				tl63_EntityName += " : "
				tl63_EntityName += "(NetID "
				IF DOES_ENTITY_EXIST(tempEntity)
				AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += NETWORK_ENTITY_GET_OBJECT_ID(tempEntity)
				ELSE
					tl63_EntityName += "--"
				ENDIF
				tl63_EntityName += ")"
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += " : "
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempEntity)
						tl63_EntityName += "Locally Owned"
					ELSE
						tl63_EntityName += "Remotely Owned"
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
			
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_EntityName, fEntityNamePosX, fEntityNamePosY, fTextSize)	
	ENDFOR
ENDPROC

PROC PROCESS_VEHICLE_STATE_DEBUG_WINDOW()
	
	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tl63_EntityName
	TEXT_LABEL_63 tl63_EntityKey1
	TEXT_LABEL_63 tl63_EntityKey2
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 3
	SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		CASE 0
			tl63_EntityKey1 = "Vehicle Key: Creator ID : Net ID : Vehicle Type : Driver"
			tl63_EntityKey2 = "Name/ID"
		BREAK
		CASE 1
			tl63_EntityKey1 = "Vehicle Key: Creator ID : Model Name : Respawns : Health"
			tl63_EntityKey2 = ""
		BREAK
		CASE 2
			tl63_EntityKey1 = "Vehicle Key: Creator ID : Net ID : Network Ownership"
			tl63_EntityKey2 = ""
		BREAK
	ENDSWITCH
	
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)
	
		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
		
		tl63_Name += "Number of Placed Vehicles: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 1, 0)
		
		tl63_Name = "Number of Current Vehicles: "
		tl63_Name += sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 0, 2)
		
		// Middle Extra Info Draw ---------------
		tl63_Name = "Player Vehicle Near: "
		IF MC_playerBD[iPartToUse].iVehNear = -1
			tl63_Name += "None"
		ELSE
			tl63_Name += MC_playerBD[iPartToUse].iVehNear
		ENDIF
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		// Legend and Key at the bottom -------------
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey1, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey2, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		sContentOverviewDebug.iLegendPrintX = 0		
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Exists / Active", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.01)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Spawns On Later Rule", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.11)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Dead / Spawn Blocked", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.21)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.1+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.2+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)
	ENDIF
	
	// Main Draw Loop -------------------------
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	STRING sPlayerName = ""	
	VEHICLE_INDEX tempEntity
	NETWORK_INDEX netEntity	
	INT i = 0
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1		
		sPlayerName = ""
		
		netEntity = MC_serverBD_1.sFMMC_SBD.niVehicle[i]
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netEntity)
			tempEntity = NET_TO_VEH(netEntity)			
		ELSE
			tempEntity = NULL
		ENDIF
		
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * i) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
		
		// Warp Click Event (Player Ped)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_1, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Camera)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_2, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		tl63_EntityName = "  Veh: "
		tl63_EntityName += i
			
		IF IS_VEHICLE_DRIVEABLE(tempEntity) AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		// Alive
			sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
		
		ELIF MC_serverBD_2.iCurrentVehRespawnLives[i] > 0
		AND CAN_VEH_STILL_SPAWN_IN_MISSION(i)
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200) 	// Not Spawned yet but can.
		
		ELSE
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)		// Dead/Can't Spawn anymore
			
		ENDIF
		
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0				
				tl63_EntityName += " ("
				IF DOES_ENTITY_EXIST(tempEntity)
				AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += NETWORK_ENTITY_GET_OBJECT_ID(tempEntity)
				ELSE
					tl63_EntityName += "--"
				ENDIF
				tl63_EntityName += ")"
				
				
				IF IS_THIS_MODEL_A_CAR(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					tl63_EntityName += " : "
					tl63_EntityName += "Car"
				ELIF IS_THIS_MODEL_A_QUADBIKE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					tl63_EntityName += " : "
					tl63_EntityName += "Quadbike"
				ELIF IS_THIS_MODEL_A_PLANE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					tl63_EntityName += " : "
					tl63_EntityName += "Plane"
				ELIF IS_THIS_MODEL_A_JETSKI(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					tl63_EntityName += " : "
					tl63_EntityName += "Jetski"
				ELIF IS_THIS_MODEL_A_BOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					tl63_EntityName += " : "
					tl63_EntityName += "Boat"
				ELIF IS_THIS_MODEL_A_BIKE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					tl63_EntityName += " : "
					tl63_EntityName += "Motorbike"
				ELIF IS_THIS_MODEL_A_BICYCLE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					tl63_EntityName += " : "
					tl63_EntityName += "Bicycle"
				ELIF IS_THIS_MODEL_A_HELI(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					tl63_EntityName += " : "
					tl63_EntityName += "Heli"
				ELIF IS_THIS_MODEL_A_TRUCK(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					tl63_EntityName += " : "
					tl63_EntityName += "Truck"
				ELSE
					tl63_EntityName += " : "
					tl63_EntityName += "Vehicle"		
				ENDIF
			
				IF IS_VEHICLE_DRIVEABLE(tempEntity)
					PED_INDEX tempPed
					tempPed = GET_PED_IN_VEHICLE_SEAT(tempEntity, VS_DRIVER)
					IF NOT IS_PED_INJURED(tempPed)
						
						INT iPed
						iPed = IS_ENTITY_A_MISSION_CREATOR_ENTITY(tempPed)
						IF iPed > -1
							tl63_EntityName += " + Ped "
							tl63_EntityName += iPed
							
						ELIF IS_PED_A_PLAYER(tempPed)
							PLAYER_INDEX piPlayer
							piPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(tempPed)
							sPlayerName = GET_PLAYER_NAME(piPlayer)
							tl63_EntityName += " : "
							tl63_EntityName += sPlayerName
							
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 1				
				tl63_EntityName += " : "
				tl63_EntityName += FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
				
				tl63_EntityName += " : "
				tl63_EntityName += MC_serverBD_2.iCurrentVehRespawnLives[i]
				
				IF IS_VEHICLE_DRIVEABLE(tempEntity)
					INT iHealth
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iHealthDamageCombinedEntityIndex != -1
						iHealth = ROUND(MC_GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(tempEntity, i))
					ELSE
						iHealth = ROUND(GET_CURRENT_HEALTH_PERCENTAGES_OF_VEHICLE_AND_LINKED_VEHICLES(i))
					ENDIF
					
					tl63_EntityName += " : "
					tl63_EntityName += iHealth
					tl63_EntityName += "%"					
				ENDIF
			BREAK
			CASE 2
				tl63_EntityName += " : "
				tl63_EntityName += "(NetID "
				IF DOES_ENTITY_EXIST(tempEntity)
				AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += NETWORK_ENTITY_GET_OBJECT_ID(tempEntity)
				ELSE
					tl63_EntityName += "--"
				ENDIF
				tl63_EntityName += ")"
								
				IF NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += " : "
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempEntity)
						tl63_EntityName += "Locally Owned"
					ELSE
						tl63_EntityName += "Remotely Owned"
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_EntityName, fEntityNamePosX, fEntityNamePosY, fTextSize)
	ENDFOR
ENDPROC

PROC PROCESS_PED_STATE_DEBUG_WINDOW()
		
	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tl63_EntityName
	TEXT_LABEL_63 tl63_EntityKey1
	TEXT_LABEL_63 tl63_EntityKey2
	TEXT_LABEL_63 tl63_Name	
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 4
	SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		CASE 0
			tl63_EntityKey1 = "Ped Key: Creator ID : Net ID : Stealth and Aggro : "
			tl63_EntityKey2 = "SP = Spooked Order : AG = Aggroed Order : Task Name : Goto Num"
		BREAK
		CASE 1
			tl63_EntityKey1 = "Ped Key: Creator ID : Model Name : Respawns : Health"
			tl63_EntityKey2 = ""
		BREAK
		CASE 2
			tl63_EntityKey1 = "Ped Key: Creator ID : Net ID : Network Ownership"
			tl63_EntityKey2 = ""
		BREAK		
		CASE 3
			tl63_EntityKey1 = "Ped Key: Creator ID : In Veh ID : In Veh Net ID"
			tl63_EntityKey2 = ""
		BREAK	
	ENDSWITCH			
	
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)
	
		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()		
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
		
		tl63_Name += "Number of Placed Peds: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfPeds		
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 1, 0)
		
		tl63_Name = "Number of Current Peds: "
		tl63_Name += sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 0, 2)
						
		// Middle Extra Info Draw ---------------
		tl63_Name = "Player Ped Near: "
		IF MC_playerBD[iPartToUse].iPedNear = -1
			tl63_Name += "None"
		ELSE
			tl63_Name += MC_playerBD[iPartToUse].iPedNear
		ENDIF
		
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)		
			
		tl63_Name = "Player Causing aggro fail: "
		tl63_Name += MC_serverBD.iPartCausingFail[0]	
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)		
			
		tl63_Name = "Has Team Aggroed Default Index: "
		IF HAS_TEAM_TRIGGERED_AGGRO(0, ciMISSION_AGGRO_INDEX_DEFAULT)
			tl63_Name += " Yes   "
		ELSE
			tl63_Name += " No    "
		ENDIF
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
				
		tl63_Name = "[SHIFT NUMPAD MINUS] to bring up Aggro Indexes: "
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 2)		
		
		// Legend and Key at the bottom -------------		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey1, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey2, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		sContentOverviewDebug.iLegendPrintX = 0		
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Exists / Active", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.01)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Spawns On Later Rule", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.11)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Dead / Spawn Blocked", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.21)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.1+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.2+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)
	ENDIF
	
	// Main Draw Loop -------------------------
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	STRING sEntityState = ""
	PED_INDEX tempEntity
	NETWORK_INDEX netEntity
	
	INT i = 0
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1		
		netEntity = MC_serverBD_1.sFMMC_SBD.niPed[i]
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netEntity)
			tempEntity = NET_TO_PED(netEntity)
		ELSE
			tempEntity = NULL
		ENDIF
		
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * i) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
		
		// Warp Click Event (Player Ped)		
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_1, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Camera)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_2, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Click Event (Show Debug)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_3, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_DRAW_MARKER_DEBUG, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "", DEFAULT, DEFAULT, FMMC_IS_LONG_BIT_SET(sContentOverviewDebug.iEntityStateDrawPedDebugBS, i))
		
		tl63_EntityName = "  Ped: "
		tl63_EntityName += i
		
		IF NOT IS_PED_INJURED(tempEntity) AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)								
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		// Alive
			sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
			
		ELIF MC_serverBD_2.iCurrentPedRespawnLives[i] > 0
		AND CAN_PED_STILL_SPAWN_IN_MISSION(i)		
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200) 	// Not Spawned yet but can.			
			
		ELSE
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)		// Dead/Can't Spawn anymore		
			
		ENDIF
		
		sEntityState = ""
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0	
				tl63_EntityName += " ("
				IF DOES_ENTITY_EXIST(tempEntity)
				AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += NETWORK_ENTITY_GET_OBJECT_ID(tempEntity)
				ELSE
					tl63_EntityName += "--"
				ENDIF
				tl63_EntityName += ")"
				
				IF NOT IS_PED_INJURED(tempEntity)
					IF FMMC_IS_LONG_BIT_SET(iPedSASInitialised, i)
					AND NOT FMMC_IS_LONG_BIT_SET(iPedSASFinished, i)
						tl63_EntityName += " (SAS)"
					ENDIF
					
					IF iPedSpookReasonDebugWindow[i] > 0
					AND NOT FMMC_IS_LONG_BIT_SET(iPedBSDebugWindow_SpookedFromAll, i)
					AND NOT IS_STRING_NULL_OR_EMPTY(GET_PED_SPOOK_REASON_FOR_DEBUG(iPedSpookReasonDebugWindow[i]))
						tl63_EntityName += " : "
						tl63_EntityName += GET_PED_SPOOK_REASON_FOR_DEBUG(iPedSpookReasonDebugWindow[i])
					ENDIF
					
					IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedBitset, i)
						tl63_EntityName += " : "
						IF iPedSpookOrderDebugWindow[i] > 0
							tl63_EntityName += iPedSpookOrderDebugWindow[i]
						ELSE
							tl63_EntityName += "X"
						ENDIF
						
						tl63_EntityName += " SP"
					ENDIF
					
					IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAggroedBitset, i)
						tl63_EntityName += " : "
						IF iPedAggroOrderDebugWindow[i] > 0
							tl63_EntityName += iPedAggroOrderDebugWindow[i]
						ELSE
							tl63_EntityName += "X"
						ENDIF
							
						tl63_EntityName += " AG"
					ENDIF					
					
					tl63_EntityName += " : "
					TEXT_LABEL_15 tl15EntityState
					tl15EntityState = GET_PED_STATE_NAME(MC_serverBD_2.iPedState[i])
					sEntityState = TEXT_LABEL_TO_STRING(tl15EntityState)
					
					tl63_EntityName += sEntityState	
			
					IF MC_serverBD_2.iPedState[i] = ciTASK_GOTO_COORDS
						tl63_EntityName += " : "
						tl63_EntityName += MC_serverBD_2.iPedGotoProgress[i]
					ENDIF
				ENDIF
			BREAK
			CASE 1
				tl63_EntityName += " : "
				tl63_EntityName += FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn)
				
				tl63_EntityName += " : "
				tl63_EntityName += MC_serverBD_2.iCurrentPedRespawnLives[i]
				
				IF NOT IS_PED_INJURED(tempEntity)				
					tl63_EntityName += " : "
					tl63_EntityName += ROUND(GET_PED_HEALTH_PERCENTAGE(tempEntity))
					tl63_EntityName += "%"
				ENDIF
			BREAK
			CASE 2
				tl63_EntityName += " : "
				tl63_EntityName += "(NetID "
				IF DOES_ENTITY_EXIST(tempEntity)
				AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += NETWORK_ENTITY_GET_OBJECT_ID(tempEntity)
				ELSE
					tl63_EntityName += "--"
				ENDIF
				tl63_EntityName += ")"
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += " : "
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempEntity)
						tl63_EntityName += "Locally Owned"
					ELSE
						tl63_EntityName += "Remotely Owned"
					ENDIF
				ENDIF
			BREAK
			CASE 3
				
				IF IS_PED_IN_ANY_VEHICLE(tempEntity)
				
					tl63_EntityName += " + In Veh "
					
					VEHICLE_INDEX tempVeh
					INT iVeh 
					
					tempVeh = GET_VEHICLE_PED_IS_IN(tempEntity)
					iVeh = IS_ENTITY_A_MISSION_CREATOR_ENTITY(tempVeh)
					
					IF iVeh > -1
						tl63_EntityName += iVeh					
						tl63_EntityName += " : "
						tl63_EntityName += "(NetID "
						IF DOES_ENTITY_EXIST(tempVeh)
						AND NETWORK_DOES_NETWORK_ID_EXIST(NETWORK_GET_NETWORK_ID_FROM_ENTITY(tempVeh))
							tl63_EntityName += NETWORK_ENTITY_GET_OBJECT_ID(tempVeh)
							tl63_EntityName += ")"
						ELSE
							tl63_EntityName += "--"
						ENDIF
					ELSE
						tl63_EntityName += "(non-Mission)"
					ENDIF
				ENDIF				
			BREAK
		ENDSWITCH
		
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_EntityName, fEntityNamePosX, fEntityNamePosY, fTextSize)
			
	ENDFOR

ENDPROC

PROC PROCESS_OVERVIEW_STATE_DEBUG_WINDOW()
		
	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
		
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 6

	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)	

		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
		
		sContentOverviewDebug.iLegendPrintX = 0
		
		DRAW_LOCAL_PLAYER_INTERIOR_INFO_DEBUG_STATE_LEGEND(PLAYER_PED_ID())	
		
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
		
		tl63_Name = "Number of Placed Peds: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		
		tl63_Name = "Number of Placed Vehicles: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		
		tl63_Name = "Number of Placed Objects: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfObjects
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		
		tl63_Name = "Number of Placed Interactables: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		
		tl63_Name = "Number of Placed Dyno Prop: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)		
			
		tl63_Name = "Number of Placed Prop: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfProps
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)		
		
		tl63_Name = "Number of Placed Zones: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfZones
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		
		tl63_Name = "Number of Placed Locations: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 1, 0)
		sContentOverviewDebug.iLegendPrintY = 0		
			
		tl63_Name = "Number of Placed Dialogue Triggers: "
		tl63_Name += g_FMMC_STRUCT.iNumberOfDialogueTriggers
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)		
		
		tl63_Name = "Number of Team Spawn Points[0]: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		
		tl63_Name = "Number of Team Spawn Points[1]: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		
		tl63_Name = "Number of Team Spawn Points[2]: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[2]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		
		tl63_Name = "Number of Team Spawn Points[3]: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[3]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		
		tl63_Name = "Number of Placed Warp Portals: "
		tl63_Name += sWarpPortal.iMaxNumberOfPortals
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		sContentOverviewDebug.iLegendPrintY = 0		
	ENDIF
	
	// Main Draw Loop -------------------------
	
	BOOL bForceSmallText, bMono, bHideEntryRect	
	STRING sTemp = ""
	INT ii = 0
	INT iTeam, iPlayer
	INT iPreReq
	INT iSpawnGroup, iSubSpawnGroup
	INT i = 0
	FOR i = 0 TO GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow, sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)])
		
		bForceSmallText = FALSE
		bMono = FALSE
		bHideEntryRect = FALSE
		sContentOverviewDebug.fBaseTextSize = 0.225		
		tl63_Name = ""
		sTemp = ""
		
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize*1.1
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow, sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * i) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
	
		INT iCurrentRow
		iCurrentRow = 0		
		
		// Could make an iSpacing variable that's only incremented when we have a non empty string for a draw call. for now leaving as is.
		
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "~bold~Mission Ambient Info"
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Mission Name: "
					tl63_Name += g_FMMC_STRUCT.tl63MissionName
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)					
					tl63_Name = g_FMMC_STRUCT.tl63MissionDecription[0]
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = g_FMMC_STRUCT.tl63MissionDecription[1]
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = g_FMMC_STRUCT.tl63MissionDecription[2]
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = g_FMMC_STRUCT.tl63MissionDecription[3]
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = g_FMMC_STRUCT.tl63MissionDecription[4]
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = g_FMMC_STRUCT.tl63MissionDecription[5]
				ENDIF				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = g_FMMC_STRUCT.tl63MissionDecription[6]
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = g_FMMC_STRUCT.tl63MissionDecription[7]
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "FMMC_MS_W0_"
					tl63_Name += MC_serverBD.iWeather
					sTemp = GET_FILENAME_FOR_AUDIO_CONVERSATION(tl63_Name)
					tl63_Name = "Weather: "
					tl63_Name += sTemp	
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "LOB_TIME_DAY_"
					tl63_Name += MC_serverBD.iTimeOfDay
					sTemp = GET_FILENAME_FOR_AUDIO_CONVERSATION(tl63_Name)
					tl63_Name = "Time Of Day: "
					tl63_Name += sTemp	
					
					INT iSecs, iMins, iHrs
					iSecs = 0
					iMins = 0
					iHrs = 0
					NETWORK_GET_GLOBAL_CLOCK(iHrs, iMins, iSecs)					
					tl63_Name += " ("
					tl63_Name += iHrs
					tl63_Name += ":"
					tl63_Name += iMins
					tl63_Name += ":"
					tl63_Name += iSecs		
					tl63_Name += ")"
				ENDIF				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Music Playing: "
					IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_MUSIC_PLAYING)
						tl63_Name += "Yes"
					ENDIF
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "FMMC_MUST_"
					tl63_Name += g_FMMC_STRUCT.iAudioScore
					sTemp = GET_FILENAME_FOR_AUDIO_CONVERSATION(tl63_Name)
					tl63_Name = "Audio Score: "
					tl63_Name += sTemp	
					tl63_Name += " ("
					tl63_Name += g_FMMC_STRUCT.iAudioScore
					tl63_Name += ")"
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Music Mood: "
					sTemp = GET_MOOD_NAME_FOR_SONG(g_FMMC_STRUCT.iAudioScore, MC_PlayerBD[iLocalPart].iMusicState)
					tl63_Name += sTemp	
					tl63_Name += " ("
					tl63_Name += MC_PlayerBD[iLocalPart].iMusicState
					tl63_Name += ")"
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Alt Vars Config: "
					tl63_Name = "MC_AVA_CON_"
					tl63_Name += ENUM_TO_INT(g_FMMC_STRUCT.sPoolVars.eConfig)
					sTemp = GET_FILENAME_FOR_AUDIO_CONVERSATION(tl63_Name)	
					tl63_Name = "Alt Vars Config: "
					tl63_Name += sTemp
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Alt Vars Bitset:"
					FOR ii = 0 TO ciFMMC_ALT_VAR_MAX_BS-1
						tl63_Name += " "
						tl63_Name += MC_ServerBD_4.iAltVarsBS[ii] 
					ENDFOR
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF			
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF					
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF					
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF					
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				
				
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "~bold~Cutscene Data"
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Cutscene ID Playing: "
					tl63_Name += MC_ServerBD.iCutsceneID[MC_playerBD[iLocalPart].iTeam]
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Cutscene ID Streamed: "
					tl63_Name += MC_ServerBD.iCutsceneStreamingIndex[MC_playerBD[iLocalPart].iTeam]
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Number Should Be Watching: "
					tl63_Name += GET_NUMBER_OF_PEOPLE_SHOULD_BE_WATCHING_CUTSCENE(MC_playerBD[iLocalPart].iTeam)
				ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Number Finished Watching: "
					tl63_Name += GET_NUMBER_OF_PEOPLE_FINISHED_CUTSCENE(MC_playerBD[iLocalPart].iTeam)
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Mocap Name: "
					tl63_Name += sMocapCutscene
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Mocap Streaming State: "
					tl63_Name += GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS_NAME(eMocapRunningCutsceneProgress)
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Mocap Cutscene State: "
					tl63_Name += GET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS_NAME(eMocapManageCutsceneProgress)
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Mocap Has Cutscene Loaded: "
					tl63_Name += PICK_STRING(HAS_CUTSCENE_LOADED(), "TRUE", "FALSE")
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Scripted Cutscene State: "
					tl63_Name += GET_SCRIPTED_CUTSCENE_STATE_PROGRESS_NAME(eScriptedCutsceneProgress)
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Scripted Current Camera Shot: "
					tl63_Name += iCamShot
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "FMMC_CUT"		
					tl63_Name += MC_ServerBD.iEndCutscene
					sTemp = GET_FILENAME_FOR_AUDIO_CONVERSATION(tl63_Name)	
					tl63_Name = "End Cutscene: "
					tl63_Name += sTemp
				ENDIF
			BREAK			
			CASE 1							
				FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
					IF iTeam = 0
						IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
							tl63_Name = "~bold~Team Data and Stats (1)"
						ENDIF
					ELSE
						IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
							// Gap
						ENDIF
					ENDIF
					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~bold~Team: "
						tl63_Name += iTeam
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Previous Rule: "
						tl63_Name += MC_ServerBD_4.iPreviousHighestPriority[iTeam]
					ENDIF	
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Rule: "
						tl63_Name += MC_ServerBD_4.iCurrentHighestPriority[iTeam]
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = ""
						IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
							tl63_Name += g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective[MC_ServerBD_4.iCurrentHighestPriority[iTeam]]
						ELSE
							tl63_Name += "Invalid"
						ENDIF
					ENDIF
										
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Rule Score: "
						tl63_Name += MC_ServerBD.iScoreOnThisRule[iTeam]
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Rule Score Target: "						
						IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
						AND MC_serverBD_4.iCurrentHighestPriority[iTeam] > -1
							tl63_Name += g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[MC_serverBD_4.iCurrentHighestPriority[iTeam]]
						ELSE
							tl63_Name += "N/A"
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Total Score: "
						tl63_Name += MC_ServerBD.iTeamScore[iTeam]
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Total Target Score: "
						tl63_Name += g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore
					ENDIF
										
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Mid Point Reached: "
						IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], MC_serverBD_4.iCurrentHighestPriority[iTeam])
							tl63_Name += "TRUE"
						ELSE
							tl63_Name += "FALSE"
						ENDIF
					ENDIF
						
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)			
						tl63_Name = "Rule Timer: "
						IF GET_OBJECTIVE_TIME_LIMIT_FOR_CURRENT_RULE(iTeam) = 0
							tl63_Name += "Unlimited"
						ELIF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[iteam])
							tl63_Name += GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iteam])
							tl63_Name += " : "
							tl63_Name += GET_OBJECTIVE_TIME_LIMIT_FOR_CURRENT_RULE(iTeam)
						ELSE
							tl63_Name += "Not Started"
							tl63_Name += " : "
							tl63_Name += GET_OBJECTIVE_TIME_LIMIT_FOR_CURRENT_RULE(iTeam)
						ENDIF
					ENDIF
					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Multi Rule Timer: "
						IF GET_MULTIRULE_TIMER_TIME_LIMIT_FOR_RULE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam]) = 0
							tl63_Name += "Unlimited"
						ELIF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdMultiObjectiveLimitTimer[iteam])
							tl63_Name += GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iteam])
							tl63_Name += ": "
							tl63_Name += GET_MULTIRULE_TIMER_TIME_LIMIT_FOR_RULE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam])
						ELSE
							tl63_Name += "Not Started"
							tl63_Name += " : "
							tl63_Name += GET_MULTIRULE_TIMER_TIME_LIMIT_FOR_RULE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam])
						ENDIF
					ENDIF
					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Number Starting Players: "
						tl63_Name += MC_ServerBD.iNumStartingPlayers[iTeam]
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Number Playing Players: "
						tl63_Name += MC_ServerBD.iNumberOfPlayingPlayers[iTeam]
					ENDIF	
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Checkpoint Reached: "
						tl63_Name += GET_CURRENT_CHECKPOINT_INDEX() // GET_CURRENT_CHECKPOINT_MRT_INDEX(iTeam)
					ENDIF					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Number Of Players Wanted: "
						tl63_Name += MC_ServerBD.iNumOfWanted[iTeam]
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)				
						tl63_Name = "Number Of Players Fake Wanted: "
						tl63_Name += MC_ServerBD.iNumOfFakeWanted[iTeam]
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Highest Wanted Level: "
						tl63_Name += MC_ServerBD.iNumOfHighestWanted[iTeam]
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Highest Fake Wanted Level: "
						tl63_Name += MC_ServerBD.iNumOfHighestFakeWanted[iTeam]
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF
				ENDFOR
			BREAK
			CASE 2					
				FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
					IF iTeam = 0
						IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
							tl63_Name = "~bold~Team Data and Stats (2)"
						ENDIF
					ELSE
						IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
							// Gap
						ENDIF
					ENDIF
					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~bold~Team: "
						tl63_Name += iTeam
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "iNumPlayerRuleHighestPriority: "
						tl63_Name += MC_serverBD.iNumPlayerRuleHighestPriority[iTeam]
					ENDIF	
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Player Rule Type: "
						IF MC_serverBD.iNumPlayerRuleHighestPriority[iTeam] > 0
							tl63_Name += GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(GET_PLAYER_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC(iTeam))
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)						
						tl63_Name = "~r~iNumPedHighestPriority: "
						tl63_Name += MC_serverBD.iNumPedHighestPriority[iTeam]
					ENDIF	
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~r~Ped Rule Type: "
						IF MC_serverBD.iNumPedHighestPriority[iTeam] > 0
							tl63_Name += GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(GET_PED_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC(iTeam))
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~b~iNumVehHighestPriority: "
						tl63_Name += MC_serverBD.iNumVehHighestPriority[iTeam]
					ENDIF	
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~b~Veh Rule Type: "
						IF MC_serverBD.iNumVehHighestPriority[iTeam] > 0
							tl63_Name += GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(GET_VEH_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC(iTeam))
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~g~iNumObjHighestPriority: "
						tl63_Name += MC_serverBD.iNumObjHighestPriority[iTeam]
					ENDIF	
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~g~Obj Rule Type: "
						IF MC_serverBD.iNumObjHighestPriority[iTeam] > 0
							tl63_Name += GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(GET_OBJ_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC(iTeam))
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~y~iNumLocHighestPriority: "
						tl63_Name += MC_serverBD.iNumLocHighestPriority[iTeam]
					ENDIF	
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)						
						tl63_Name = "~y~iNumberOfTeamInAllAreas: "
						tl63_Name += MC_serverBD_1.iNumberOfTeamInAllAreas[iTeam]
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~y~Loc Rule Type: "
						IF MC_serverBD.iNumLocHighestPriority[iTeam] > 0
							tl63_Name += GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(GET_LOC_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC(iTeam))
						ENDIF
					ENDIF
					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF	
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF	
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF
				ENDFOR
			BREAK
			CASE 3
				FOR iPlayer = 0 TO 3 // "Mission Players" max is 4.
					INT iPart
					iPart = GET_ACTIVE_PARTICIPANT_INDEX_FROM_NUMBER(iPlayer)
					IF iPlayer = 0
						IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
							tl63_Name = "~bold~Mission Player Data and Stats: "
						ENDIF
					ELSE
						IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
							// Gap
						ENDIF
					ENDIF
								
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~bold~Player "
						tl63_Name += iPlayer						
					ENDIF					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~bold~Participant: "
						tl63_Name += iPart
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = "~bold~Name: "
							tl63_Name += MC_ServerBD_2.tParticipantNames[iPart]
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)					
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = ""
							tl63_Name += GET_GAME_STATE_STRING(MC_playerBD[iPart].iGameState)
						ENDIF
					ENDIF		
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = ""
							tl63_Name += GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(GET_MC_CLIENT_MISSION_STAGE(iPart))
						ENDIF
					ENDIF										
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = "Score: "
							tl63_Name += MC_ServerBD.iPlayerScore[iPart]
						ENDIF
					ENDIF
						
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = "iCurrentLoc: "
							tl63_Name += MC_PlayerBD[iPart].iCurrentLoc
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = "iInAnyLoc: "
							tl63_Name += MC_PlayerBD[iPart].iInAnyLoc
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = "iLeaveLoc: "
							tl63_Name += MC_PlayerBD[iPart].iLeaveLoc
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = "iPedNear: "
							tl63_Name += MC_PlayerBD[iPart].iPedNear
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = "iVehNear: "
							tl63_Name += MC_PlayerBD[iPart].iVehNear
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = "iObjNear: "
							tl63_Name += MC_PlayerBD[iPart].iObjNear
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = "iObjHacking: "
							tl63_Name += MC_PlayerBD[iPart].iObjHacking
						ENDIF
					ENDIF
				
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = "Started Cut: "
							tl63_Name += PICK_STRING(IS_BIT_SET(MC_PlayerBD[iPart].iClientBitset2, PBBOOL2_STARTED_CUTSCENE), "Yes", "No")
							tl63_Name += "  :  Finished Cut: "
							tl63_Name += PICK_STRING(IS_BIT_SET(MC_PlayerBD[iPart].iClientBitset2, PBBOOL2_FINISHED_CUTSCENE), "Yes", "No")
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = "Interacting With: "
							INT iInteractable
							FOR iInteractable = 0 TO ciINTERACTABLE_MAX_ONGOING_INTERACTIONS-1							
								IF MC_PlayerBD[iPart].iCurrentInteractables[iInteractable] != -1
									tl63_Name += MC_PlayerBD[iPart].iCurrentInteractables[iInteractable]
								ENDIF
							ENDFOR						
						ENDIF
					ENDIF
					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF
					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF
					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF
					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF
				ENDFOR
			BREAK
			CASE 4						
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "~bold~Prerequisites~s~ | ~g~ Set ~s~ / ~r~ Unset ~s~"
					fTextSize*=1.5
				ENDIF
				INT k
				FOR k = 0 TO 20
					IF iPreReq <= ciPREREQ_Max-1
						IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)							
							INT j
							FOR j = 0 TO 5
								IF iPreReq >= ciPREREQ_Max
									BREAKLOOP
								ENDIF
								
								IF IS_PREREQUISITE_COMPLETED(iPreReq)
									tl63_Name += "~g~"
									tl63_Name += iPrereq
									tl63_Name += "~s~"
								ELSE
									tl63_Name += "~r~"
									tl63_Name += iPrereq
									tl63_Name += "~s~"
								ENDIF
								
								IF iPrereq > 9									
									tl63_Name += "		 "
								ELSE
									tl63_Name += "		  "
								ENDIF
								
								// Have to hack these buttons in to show 5 buttons for 1 text label as opposed to using the text functionality inside CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON due to the code limit of text we can render..
								// Toggle Pre-req
								CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_PREREQ_COLUMN_1_OF_10_NARROW+j, iPreReq, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_TOGGLE_PRE_REQ, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "", DEFAULT, MENU_ICON_DUMMY, IS_PREREQUISITE_COMPLETED(iPreReq))
								
								bForceSmallText = TRUE
								bMono = TRUE
								bHideEntryRect = TRUE
								iPreReq++
							ENDFOR							
						ENDIF
					ENDIF
				ENDFOR
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)		
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)		
					// Gap
				ENDIF
			BREAK
			CASE 5
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "~bold~Spawn Groups		 Sub Spawn Groups~s~		~g~Set~s~ / ~r~Unset~s~"
					fTextSize*=1.5
					bHideEntryRect = TRUE
				ENDIF
				INT iGroup, iButtonPlace
				iGroup = 0
				iButtonPlace = 0
				FOR iSpawnGroup = 0 TO FMMC_MAX_RANDOM_ENTITY_SPAWN_GROUPS
					IF iSpawnGroup <= FMMC_MAX_RANDOM_ENTITY_SPAWN_GROUPS-1					
						IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
							
							iGroup = (iSpawnGroup*20)
							
							tl63_Name = ""
							IF IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, iSpawnGroup)
								tl63_Name += "~g~"
								tl63_Name += iSpawnGroup
								tl63_Name += "~s~"
							ELSE
								tl63_Name += "~r~"
								tl63_Name += iSpawnGroup
								tl63_Name += "~s~"
							ENDIF
							
							// Toggle Spawn Group
							CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_NARROW_INCREMENTING_LEFT_TO_RIGHT+iButtonPlace, iGroup, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_TOGGLE_SPAWN_GROUP, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, tl63_Name, fTextSize, MENU_ICON_DUMMY, IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, iSpawnGroup))
							iButtonPlace = 0
							tl63_Name = ""
														
							bHideEntryRect = TRUE
							
							FOR iSubSpawnGroup = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
								IF iSubSpawnGroup <= ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
										
									tl63_Name = ""
									IF iSubSpawnGroup = 0 OR iSubSpawnGroup = 7 OR iSubSpawnGroup = 15
										IF IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iSpawnGroup], iSubSpawnGroup)
											tl63_Name += "~g~"
											tl63_Name += iSubSpawnGroup
											tl63_Name += "~s~"
										ELSE
											tl63_Name += "~r~"
											tl63_Name += iSubSpawnGroup
											tl63_Name += "~s~"
										ENDIF
									ENDIF
										
									// Toggle Sub Spawn Group
									CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_NARROW_INCREMENTING_LEFT_TO_RIGHT+iButtonPlace+4, iGroup, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_TOGGLE_SPAWN_GROUP_SUBGROUP, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, tl63_Name, fTextSize, MENU_ICON_DUMMY, IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iSpawnGroup], iSubSpawnGroup))
									iButtonPlace++																		
									tl63_Name = ""
									
									bHideEntryRect = TRUE
									iGroup++							
								ENDIF
							ENDFOR
							// Using the button to draw the text.							
						ENDIF
					ENDIF
				ENDFOR
			BREAK
		ENDSWITCH
		
		IF NOT IS_STRING_NULL_OR_EMPTY(tl63_Name)
		AND NOT bHideEntryRect
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX/2, sContentOverviewDebug.fStatusRectY/2, 255, 255, 255, 175)				
		ENDIF
		
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_Name, fEntityNamePosX, fEntityNamePosY, fTextSize, bForceSmallText, DEFAULT, bMono)
	ENDFOR

ENDPROC

PROC PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_MAIN_FOR_SPECIFIC_CONTENT()

	SET_DEFAULT_DEBUG_WINDOW_FOR_SCRIPT(CONTENT_OVERVIEW_DEBUG_WINDOW_MISSION_OVERVIEW)
	
	// MISSION CONTROLLER 2020 SPECIFIC ----
	SWITCH sContentOverviewDebug.eDebugWindow
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_MISSION_OVERVIEW
			PROCESS_OVERVIEW_STATE_DEBUG_WINDOW()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS
			PROCESS_PED_STATE_DEBUG_WINDOW()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES
			PROCESS_VEHICLE_STATE_DEBUG_WINDOW()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS
			PROCESS_OBJECTS_STATE_DEBUG_WINDOW()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_INTERACTABLE
			PROCESS_INTERACTABLE_STATE_DEBUG_WINDOW()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP
			PROCESS_DYNOPROP_STATE_DEBUG_WINDOW()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PROP
			PROCESS_PROP_STATE_DEBUG_WINDOW()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES
			PROCESS_ZONE_STATE_DEBUG_WINDOW()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS
			PROCESS_LOCATION_STATE_DEBUG_WINDOW()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER
			PROCESS_DIALOGUE_TRIGGER_STATE_DEBUG_WINDOW()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_TEAM_SPAWN_POINTS
			PROCESS_TEAM_SPAWN_POINTS_STATE_DEBUG_WINDOW()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_WARP_PORTALS
			PROCESS_WARP_PORTALS_STATE_DEBUG_WINDOW()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_GANG_CHASE
			PROCESS_GANG_CHASE_STATE_DEBUG_WINDOW()
		BREAK
	ENDSWITCH
	// MISSION CONTROLLER 2020 SPECIFIC ----
ENDPROC

PROC PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT()
	
	INT i, ii
	
	SWITCH sContentOverviewDebug.eEntityStateClickEvent		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX
			
			FLOAT fHead
			VECTOR vWarp
			VECTOR vWarpOffset
			NETWORK_INDEX niEnt
			ENTITY_INDEX entID
			
			SWITCH sContentOverviewDebug.eDebugWindowTypeSelected			
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS
					niEnt = MC_serverBD_1.sFMMC_SBD.niPed[sContentOverviewDebug.iEntityIndexSelected]
					IF NETWORK_DOES_NETWORK_ID_EXIST(niEnt)
						entID = NET_TO_ENT(niEnt)
						IF DOES_ENTITY_EXIST(entID)
							vWarp = GET_ENTITY_COORDS(entID)
							fHead = GET_ENTITY_HEADING(entID)-180
							vWarpOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vWarp, fHead, <<2.0, 0.0, 0.0>>)
						ENDIF
					ENDIF
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES
					niEnt = MC_serverBD_1.sFMMC_SBD.niVehicle[sContentOverviewDebug.iEntityIndexSelected]
					IF NETWORK_DOES_NETWORK_ID_EXIST(niEnt)
						entID = NET_TO_ENT(niEnt)
						IF DOES_ENTITY_EXIST(entID)
							vWarp = GET_ENTITY_COORDS(entID)
							fHead = GET_ENTITY_HEADING(entID)-180
							vWarpOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vWarp, fHead, <<2.0, 0.0, 0.0>>)
						ENDIF
					ENDIF
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS
					niEnt = MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(sContentOverviewDebug.iEntityIndexSelected)]
					IF NETWORK_DOES_NETWORK_ID_EXIST(niEnt)
						entID = NET_TO_ENT(niEnt)
						IF DOES_ENTITY_EXIST(entID)
							vWarp = GET_ENTITY_COORDS(entID)
							fHead = GET_ENTITY_HEADING(entID)-180
							vWarpOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vWarp, fHead, <<2.0, 0.0, 0.0>>)
						ENDIF
					ENDIF
				BREAK			
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_INTERACTABLE
					niEnt = GET_INTERACTABLE_NET_ID(sContentOverviewDebug.iEntityIndexSelected)
					IF NETWORK_DOES_NETWORK_ID_EXIST(niEnt)
						entID = NET_TO_ENT(niEnt)
						IF DOES_ENTITY_EXIST(entID)
							vWarp = GET_ENTITY_COORDS(entID)
							fHead = GET_ENTITY_HEADING(entID)-180
							vWarpOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vWarp, fHead, <<2.0, 0.0, 0.0>>)
						ENDIF
					ENDIF
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP
					niEnt = MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_DYNOPROP_NET_ID_INDEX(sContentOverviewDebug.iEntityIndexSelected)]
					IF NETWORK_DOES_NETWORK_ID_EXIST(niEnt)
						entID = NET_TO_ENT(niEnt)
						IF DOES_ENTITY_EXIST(entID)
							vWarp = GET_ENTITY_COORDS(entID)
							fHead = GET_ENTITY_HEADING(entID)-180
							vWarpOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vWarp, fHead, <<2.0, 0.0, 0.0>>)
						ENDIF
					ENDIF
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PROP
					entID = oiProps[sContentOverviewDebug.iEntityIndexSelected]
					IF DOES_ENTITY_EXIST(entID)
						vWarp = GET_ENTITY_COORDS(entID)
						fHead = GET_ENTITY_HEADING(entID)-180
						vWarpOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vWarp, fHead, <<0.0, 0.0, 2.0>>)
					ENDIF
				BREAK	
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES
					vWarp = GET_ZONE_RUNTIME_CENTRE(sContentOverviewDebug.iEntityIndexSelected)
					fHead = 0.0
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS
					vWarp = GET_LOCATION_VECTOR(sContentOverviewDebug.iEntityIndexSelected)
					fHead = 0.0
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER
					vWarp = MC_GET_DIALOGUE_TRIGGER_POSITION(sContentOverviewDebug.iEntityIndexSelected)
					fHead = 0.0
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_TEAM_SPAWN_POINTS
					vWarp = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sContentOverviewDebug.iTeamIndexSelected][sContentOverviewDebug.iEntityIndexSelected].vPos
					fHead = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sContentOverviewDebug.iTeamIndexSelected][sContentOverviewDebug.iEntityIndexSelected].fHead					
					REPOSITION_TEAM_SPAWN_POINT_BASED_ON_ENTITY(vWarp, fHead, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sContentOverviewDebug.iTeamIndexSelected][sContentOverviewDebug.iEntityIndexSelected].iMoveToEntityIndex, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sContentOverviewDebug.iTeamIndexSelected][sContentOverviewDebug.iEntityIndexSelected].iMoveToEntityType)				
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_WARP_PORTALS
					vWarp = MC_GET_WARP_PORTAL_START_POSITION(sContentOverviewDebug.iEntityIndexSelected)
					fHead = MC_GET_WARP_PORTAL_START_HEADING(sContentOverviewDebug.iEntityIndexSelected)
					vWarpOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vWarp, fHead, <<0.0, 0.0, 1.0>>)
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_GANG_CHASE
					niEnt = niDebugGangChaseTargetEntity[sContentOverviewDebug.iEntityIndexSelected]
					IF NETWORK_DOES_NETWORK_ID_EXIST(niEnt)
						entID = NET_TO_ENT(niEnt)
						IF DOES_ENTITY_EXIST(entID)
							vWarp = GET_ENTITY_COORDS(entID)
							fHead = GET_ENTITY_HEADING(entID)-180
							vWarpOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vWarp, fHead, <<2.0, 0.0, 0.0>>)
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
			
			IF IS_VECTOR_ZERO(vWarp)
				CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
				EXIT
			ENDIF
			
			IF sContentOverviewDebug.eEntityStateClickEvent = CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX	
				IF NOT IS_VECTOR_ZERO(vWarpOffset)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpOffset)
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarp)
				ENDIF
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fHead)
				
			ELIF sContentOverviewDebug.eEntityStateClickEvent = CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX				
				
				IF DOES_CAM_EXIST(sContentOverviewDebug.ciEntityStateCamera)
					DESTROY_CAM(sContentOverviewDebug.ciEntityStateCamera)
				ENDIF
			
				VECTOR vCamPos, vCamPosOffset
				INT iMinOffset, iMaxOffset
				INT iRand
				iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
				
				IF sContentOverviewDebug.eDebugWindowTypeSelected	!= CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES
					iMinOffset = 2
					iMaxOffset = 5
				ELSE
					iMinOffset = 5
					iMaxOffset = 8
				ENDIF
				
				SWITCH iRand
					CASE 0
						vCamPosOffset = <<-TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), -TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), TO_FLOAT(GET_RANDOM_INT_IN_RANGE(1, 3))>>
					BREAK
					CASE 1
						vCamPosOffset = <<TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), TO_FLOAT(GET_RANDOM_INT_IN_RANGE(1, 3))>>
					BREAK
					CASE 2
						vCamPosOffset = <<-TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), TO_FLOAT(GET_RANDOM_INT_IN_RANGE(1, 3))>>
					BREAK
					CASE 3
						vCamPosOffset = <<TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), -TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), TO_FLOAT(GET_RANDOM_INT_IN_RANGE(1, 3))>>
					BREAK
				ENDSWITCH
				
				vCamPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vWarp, fHead, vCamPosOffset)
					
				sContentOverviewDebug.ciEntityStateCamera = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamPos, <<0.0, 0.0, 0.0>>, 70.0)				
				
				SET_CAM_ACTIVE(sContentOverviewDebug.ciEntityStateCamera, TRUE)
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE, 1500, DEFAULT, TRUE)
				
				SET_CAM_FOV(sContentOverviewDebug.ciEntityStateCamera, 70.0)
							
				IF DOES_ENTITY_EXIST(entID)
					POINT_CAM_AT_ENTITY(sContentOverviewDebug.ciEntityStateCamera, entID, <<0.0, 0.0, 0.0>>)
					ATTACH_CAM_TO_ENTITY(sContentOverviewDebug.ciEntityStateCamera, entID, vCamPosOffset, TRUE)
					SET_FOCUS_ENTITY(entID)
				ELSE
					SET_FOCUS_POS_AND_VEL(vWarp, <<0.0, 0.0, 0.0>>)					
				ENDIF
			ENDIF
			
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_PLAY_OR_TRIGGER
			SWITCH sContentOverviewDebug.eDebugWindowTypeSelected				
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_INTERACTABLE
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PROP									
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS
					
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER
					FMMC_SET_LONG_BIT(iLocalDialogueForcePlay, sContentOverviewDebug.iEntityIndexSelected)
				BREAK	
			ENDSWITCH
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_RESET		
			SWITCH sContentOverviewDebug.eDebugWindowTypeSelected				
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_INTERACTABLE
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PROP									
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS
					
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER
					
					FMMC_CLEAR_LONG_BIT(iLocalDialoguePending, sContentOverviewDebug.iEntityIndexSelected)
					FMMC_CLEAR_LONG_BIT(iLocalDialoguePlayedBS, sContentOverviewDebug.iEntityIndexSelected) 
					FMMC_CLEAR_LONG_BIT(iLocalDialogueSkippedBS, sContentOverviewDebug.iEntityIndexSelected) 
					FMMC_CLEAR_LONG_BIT(iLocalDialogueBlockedTempBS, sContentOverviewDebug.iEntityIndexSelected) 
					FMMC_CLEAR_LONG_BIT(iLocalDialoguePlayingBS, sContentOverviewDebug.iEntityIndexSelected)
					FMMC_CLEAR_LONG_BIT(iDialogueKilledBS, sContentOverviewDebug.iEntityIndexSelected)
					FMMC_CLEAR_LONG_BIT(iDialogueVehicleOwnedBS, sContentOverviewDebug.iEntityIndexSelected)
					FMMC_CLEAR_LONG_BIT(iDialogueVehicleNotOwnedBS, sContentOverviewDebug.iEntityIndexSelected)
					FMMC_CLEAR_LONG_BIT(iDialogueProgressSFX1Played, sContentOverviewDebug.iEntityIndexSelected)
					FMMC_CLEAR_LONG_BIT(iDialogueProgressSFX2Played, sContentOverviewDebug.iEntityIndexSelected)
					FMMC_CLEAR_LONG_BIT(iLocalDialogueBlockedBS, sContentOverviewDebug.iEntityIndexSelected)
					FMMC_CLEAR_LONG_BIT(iLocalDialogueBlockedByEntityBS, sContentOverviewDebug.iEntityIndexSelected)
					FMMC_CLEAR_LONG_BIT(iLocalDialoguePlayRemotelyWithPriority, sContentOverviewDebug.iEntityIndexSelected)
					FMMC_CLEAR_LONG_BIT(iLocalDialogueForceCustomAppend, sContentOverviewDebug.iEntityIndexSelected)
					FMMC_CLEAR_LONG_BIT(iLocalDialogueForceSkip, sContentOverviewDebug.iEntityIndexSelected)
					FMMC_CLEAR_LONG_BIT(iLocalDialogueHangUpPlayedBS, sContentOverviewDebug.iEntityIndexSelected)
					FMMC_CLEAR_LONG_BIT(iLocalDialogueForcePlay, sContentOverviewDebug.iEntityIndexSelected)
					
					RESET_NET_TIMER(tdDialogueTimer[sContentOverviewDebug.iEntityIndexSelected])
					CLEAR_BIT(MC_playerBD[iLocalPart].iDialoguePlayedBS[sContentOverviewDebug.iEntityIndexSelected/32],sContentOverviewDebug.iEntityIndexSelected%32)
					
				BREAK				
			ENDSWITCH
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_DRAW_MARKER_DEBUG
			
			IF sContentOverviewDebug.iEntityIndexSelected <= -1
				CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
				EXIT
			ENDIF
			
			SWITCH sContentOverviewDebug.eDebugWindowTypeSelected
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS
					
					IF sContentOverviewDebug.iEntityIndexSelected >= FMMC_MAX_PEDS
						CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
						EXIT
					ENDIF
					
					IF NOT FMMC_IS_LONG_BIT_SET(sContentOverviewDebug.iEntityStateDrawPedDebugBS, sContentOverviewDebug.iEntityIndexSelected)
						FMMC_SET_LONG_BIT(sContentOverviewDebug.iEntityStateDrawPedDebugBS, sContentOverviewDebug.iEntityIndexSelected)
					ELSE
						FMMC_CLEAR_LONG_BIT(sContentOverviewDebug.iEntityStateDrawPedDebugBS, sContentOverviewDebug.iEntityIndexSelected)	
					ENDIF
					
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS
				
					IF sContentOverviewDebug.iEntityIndexSelected >= FMMC_MAX_NUM_OBJECTS
						CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
						EXIT
					ENDIF
				
					IF NOT FMMC_IS_LONG_BIT_SET(sContentOverviewDebug.iEntityStateDrawObjDebugBS, sContentOverviewDebug.iEntityIndexSelected)
						FMMC_SET_LONG_BIT(sContentOverviewDebug.iEntityStateDrawObjDebugBS, sContentOverviewDebug.iEntityIndexSelected)
					ELSE
						FMMC_CLEAR_LONG_BIT(sContentOverviewDebug.iEntityStateDrawObjDebugBS, sContentOverviewDebug.iEntityIndexSelected)	
					ENDIF
					
				BREAK			
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_INTERACTABLE
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PROP
				BREAK	
				
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES
					
					IF sContentOverviewDebug.iEntityIndexSelected >= FMMC_MAX_NUM_ZONES
						CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
						EXIT
					ENDIF
					
					IF NOT FMMC_IS_LONG_BIT_SET(sContentOverviewDebug.iEntityStateDrawZoneDebugBS, sContentOverviewDebug.iEntityIndexSelected)
						FMMC_SET_LONG_BIT(sContentOverviewDebug.iEntityStateDrawZoneDebugBS, sContentOverviewDebug.iEntityIndexSelected)
					ELSE
						FMMC_CLEAR_LONG_BIT(sContentOverviewDebug.iEntityStateDrawZoneDebugBS, sContentOverviewDebug.iEntityIndexSelected)	
					ENDIF
					
				BREAK
				
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER
				BREAK
			ENDSWITCH
			
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_ENABLE_ALL_WINDOW_DEBUG
			
			SWITCH sContentOverviewDebug.eDebugWindowTypeSelected
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_TEAM_SPAWN_POINTS
					PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("CLICKED", "", vBlankDbg)
					IF bCustomRespawnPointsDebug
						bCustomRespawnPointsDebug = FALSE
					ELSE
						bCustomRespawnPointsDebug = TRUE
					ENDIF
				BREAK
			ENDSWITCH
				
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_TOGGLE_PRE_REQ
			IF NOT IS_PREREQUISITE_COMPLETED(sContentOverviewDebug.iEntityIndexSelected)
				SET_PREREQUISITE_COMPLETED(sContentOverviewDebug.iEntityIndexSelected, FALSE)
			ELIF IS_PREREQUISITE_COMPLETED(sContentOverviewDebug.iEntityIndexSelected)
				RESET_PREREQUISITE_COMPLETED(sContentOverviewDebug.iEntityIndexSelected, FALSE)
			ENDIF
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_TOGGLE_SPAWN_GROUP			
			ii = sContentOverviewDebug.iEntityIndexSelected
			WHILE ii >= 20
				ii -= 20
				i ++
			ENDWHILE
			
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ToggleSpawnGroup, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, i)
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_TOGGLE_SPAWN_GROUP_SUBGROUP			
			ii = sContentOverviewDebug.iEntityIndexSelected
			WHILE ii >= 20
				ii -= 20
				i ++
			ENDWHILE			
			
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ToggleSubpawnGroup, ii, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, i)
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
	ENDSWITCH
			
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Mission Controller Debug Window ------------------------------------------------
// ##### Description: Drawing of mission controller debug window --------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC GO_DOWN_A_LINE( VECTOR& vDrawPos )
	FLOAT iYIncrement = 0.015
	VECTOR vIncrement = << 0, iYIncrement, 0 >>
	vDrawPos += vIncrement
ENDPROC

FUNC VECTOR GET_TEXT_COLOUR( eColour eTextColour )
	VECTOR vColour = << 0,0,0 >>
	
	SWITCH eTextColour
		CASE eColour_Red
			vColour = << 255, 0, 0 >>
		BREAK
		
		CASE eColour_Orange
			vColour = << 255, 140, 50 >>
		BREAK
		
		CASE eColour_Green
			vColour = << 50, 255, 100 >>
		BREAK
		
		CASE eColour_White
			vColour = << 255, 255, 255 >>
		BREAK
		
	ENDSWITCH
	
	RETURN vColour
ENDFUNC

PROC DRAW_COLOURED_DEBUG_TEXT( STRING strToPrint, VECTOR vDrawPos, VECTOR vColour )
	IF NOT IS_STRING_NULL_OR_EMPTY( strToPrint )
		DRAW_DEBUG_TEXT_2D( strToPrint, vDrawPos, FLOOR( vColour.x ), FLOOR( vColour.y ), FLOOR( vColour.z ), 255 )
	ENDIF
ENDPROC

PROC PRINT_DEBUG_TRUEFALSE_LINE( BOOL bPredicate, STRING strTrue, STRING strFalse, eColour eTextColour, VECTOR& vDrawPos )

	TEXT_LABEL_63 strToPrint = ""
	
	IF bPredicate
		strToPrint = strTrue
	ELSE
		strToPrint = strFalse
	ENDIF
	
	GO_DOWN_A_LINE( vDrawPos )

	DRAW_COLOURED_DEBUG_TEXT( strToPrint, vDrawPos, GET_TEXT_COLOUR( eTextColour ) )
ENDPROC

PROC PRINT_DEBUG_VECTOR_WITH_TEXT( STRING strLine, VECTOR vVectorToPrint, eColour eTextColour, VECTOR& vDrawPos )

	TEXT_LABEL_63 strToPrint = strLine
	
	strToPrint += " <<"
	strToPrint += FLOOR( vVectorToPrint.x )
	strToPrint += ","
	strToPrint += FLOOR( vVectorToPrint.y )
	strToPrint += ","
	strToPrint += FLOOR( vVectorToPrint.z )
	strToPrint += ">>"
	
	GO_DOWN_A_LINE( vDrawPos )
	
	DRAW_COLOURED_DEBUG_TEXT( strToPrint, vDrawPos, GET_TEXT_COLOUR( eTextColour ) )
	
ENDPROC

PROC PRINT_DEBUG_LINE( STRING strToPrint, VECTOR& vDrawPos, eColour eTextColour )
	
	GO_DOWN_A_LINE( vDrawPos )
	DRAW_COLOURED_DEBUG_TEXT( strToPrint, vDrawPos, GET_TEXT_COLOUR( eTextColour ) )
ENDPROC

PROC CLEANUP_MENU_SKIP()

	INT iRule
	MissionStageMenuTextStruct sEmptyTeamSkipStruct
	FOR iRule = 0 TO (FMMC_MAX_RULES-1)
		sTeamSkipStruct[iRule]= sEmptyTeamSkipStruct
	ENDFOR
	iSkipTeam = -1
	bDrawMenu = FALSE

ENDPROC

PROC HANDLE_DEBUG_WINDOW_KEYBOARD()
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT)
		SWITCH debugState
			CASE eControllerDebugState_OVERVIEW
				debugState = eControllerDebugState_CUTSCENES
			BREAK
			
			CASE eControllerDebugState_CUTSCENES
				debugState = eControllerDebugState_CAPTURELOC
			BREAK
			
			CASE eControllerDebugState_CAPTURELOC
				debugState = eControllerDebugState_ENTITYSTUFF
			BREAK
			
			CASE eControllerDebugState_ENTITYSTUFF
				debugState = eControllerDebugState_PLAYER
			BREAK
			
			CASE eControllerDebugState_PLAYER
				debugState = eControllerDebugState_VEHICLES
			BREAK
			
			CASE eControllerDebugState_VEHICLES
				debugState = eControllerDebugState_END
			BREAK
			
			CASE eControllerDebugState_END
				debugState = eControllerDebugState_OVERVIEW
			BREAK
			
		ENDSWITCH
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_LEFT)
		SWITCH debugState
			CASE eControllerDebugState_OVERVIEW
				debugState = eControllerDebugState_END
			BREAK
			
			CASE eControllerDebugState_CUTSCENES
				debugState = eControllerDebugState_OVERVIEW
			BREAK
			
			CASE eControllerDebugState_CAPTURELOC
				debugState = eControllerDebugState_CUTSCENES
			BREAK
			
			CASE eControllerDebugState_ENTITYSTUFF
				debugState = eControllerDebugState_CAPTURELOC
			BREAK
			
			CASE eControllerDebugState_PLAYER
				debugState = eControllerDebugState_ENTITYSTUFF
			BREAK
			
			CASE eControllerDebugState_VEHICLES
				debugState = eControllerDebugState_PLAYER
			BREAK
			
			CASE eControllerDebugState_END
				debugState = eControllerDebugState_VEHICLES
			BREAK
			
		ENDSWITCH
	ENDIF
ENDPROC

PROC DRAW_DEBUG_HELP()
	FLOAT iXStartPos = 0.03
	FLOAT iYStartPos = 0.09
	
	VECTOR vDrawPos = << iXStartPos, iYStartPos, 0 >>
	
	PRINT_DEBUG_LINE( "~~ MISSION CONTROLLER DEBUG ~~", vDrawPos, eColour_Orange )
	
	SWITCH debugState
		CASE eControllerDebugState_OVERVIEW
			PRINT_DEBUG_LINE( "<< End | OVERVIEW | Apartments >>", vDrawPos, eColour_Green )
		BREAK
		
		CASE eControllerDebugState_CUTSCENES
			PRINT_DEBUG_LINE( "<< Apartments | CUTSCENES | CaptureLoc >>", vDrawPos, eColour_Green )
		BREAK
		
		CASE eControllerDebugState_CAPTURELOC
			PRINT_DEBUG_LINE( "<< Cutscenes | CAPTURELOC | EntityStuff >>", vDrawPos, eColour_Green )
		BREAK
		
		CASE eControllerDebugState_ENTITYSTUFF
			PRINT_DEBUG_LINE( "<< CaptureLoc | ENTITYSTUFF | Player >>", vDrawPos, eColour_Green )
		BREAK
		
		CASE eControllerDebugState_PLAYER
			PRINT_DEBUG_LINE( "<< Entitystuff | PLAYER | Vehicles >>", vDrawPos, eColour_Green )
		BREAK
		
		CASE eControllerDebugState_VEHICLES
			PRINT_DEBUG_LINE( "<< Player | VEHICLES | End >>", vDrawPos, eColour_Green )
		BREAK
		
		CASE eControllerDebugState_END
			PRINT_DEBUG_LINE( "<< Vehicles | END | Overview >>", vDrawPos, eColour_Green )
		BREAK
		
	ENDSWITCH
ENDPROC

PROC MAINTAIN_RECORD_OF_PREVIOUS_GAMESTATES()
	INT iLoop
	
	INT iTeam = MC_playerBD[ iPartToUse ].iteam
	INT iCurrentRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	
	IF iPreviousRule != iCurrentRule
		FOR iLoop = 0 TO ciMAX_RECORDED_CLIENT_STATES - 2
			iClientStates[ iLoop ] = iClientStates[ iLoop + 1 ]
		ENDFOR
		iClientStates[ ciMAX_RECORDED_CLIENT_STATES - 1 ] = GET_MC_CLIENT_MISSION_STAGE( iPartToUse )
		iPreviousRule = iCurrentRule
	ENDIF
	
	IF iGameStates[ ciMAX_RECORDED_GAME_STATES - 1 ] != MC_playerBD[iLocalPart].iGameState
		FOR iLoop = 0 TO ciMAX_RECORDED_GAME_STATES - 2
			iGameStates[ iLoop ] = iGameStates[ iLoop + 1 ]
		ENDFOR
		iGameStates[ ciMAX_RECORDED_GAME_STATES - 1 ] = MC_playerBD[iLocalPart].iGameState
	ENDIF

ENDPROC

PROC DRAW_STATE_RECORD( VECTOR& vDrawPos )

	TEXT_LABEL_63 sTemp
	INT iLoop
	
	INT iIterator
	
	sTemp = "Client Mission Stages "
	PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
	
	sTemp = "Current: >> "
	sTemp += GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(GET_MC_CLIENT_MISSION_STAGE(iPartToUse))
	PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_Orange )
	
	iIterator = ciMAX_RECORDED_CLIENT_STATES - 1
	
	FOR iLoop = 0 TO ciMAX_RECORDED_CLIENT_STATES - 1
		sTemp = "Previous: >> "
		sTemp += GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG( iClientStates[ iIterator ] )
		PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
		iIterator--
	ENDFOR
	
	vDrawPos += << 0, 0.015, 0 >>
	
	sTemp = "Game States "
	PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
	
	sTemp = "Current: * "
	sTemp += GET_GAME_STATE_STRING( MC_playerBD[iLocalPart].iGameState )
	PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
	
	iIterator = ciMAX_RECORDED_GAME_STATES - 1
	
	FOR iLoop = 0 TO ciMAX_RECORDED_GAME_STATES - 1
		sTemp = "Previous: * "
		sTemp += GET_GAME_STATE_STRING( iGameStates[ iIterator ] )
		PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
		iIterator--
	ENDFOR
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Performance
// ##### Description: Debug to monitor the performance of the script
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC FLOAT GET_SCRIPT_BUDGET_PERCENTAGE(FLOAT fUpdateTime)
	RETURN (fUpdateTime / cfPERFORMANCE_METRIC_SCRIPT_BUDGET) * 100
ENDFUNC

PROC PROCESS_PERFORMANCE_METRIC_LOGGING()
	
	IF NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()) < iLastPerformanceCheck + ciPERFORMANCE_METRIC_INTERVAL
		EXIT
	ENDIF
	
	THREADID thread = GET_ID_OF_THIS_THREAD()
	FLOAT fUpdateTime = METRICS_GET_UPDATE_TIME_FOR_THREAD(thread)
	FLOAT fAvgUpdateTime = METRICS_GET_AVERAGE_UPDATE_TIME_FOR_THREAD(thread)
	FLOAT fPeakUpdateTime = METRICS_GET_PEAK_UPDATE_TIME_FOR_THREAD(thread)
	
	PRINTLN("[PERFORMANCE_METRICS] - Update Time: ", fUpdateTime, " -  % Budget: ", GET_SCRIPT_BUDGET_PERCENTAGE(fUpdateTime))
	PRINTLN("[PERFORMANCE_METRICS] - Average Update Time: ", fAvgUpdateTime, " -  % Budget: ", GET_SCRIPT_BUDGET_PERCENTAGE(fAvgUpdateTime))
	PRINTLN("[PERFORMANCE_METRICS] - Peak Update Time = ", fPeakUpdateTime, " -  % Budget: ", GET_SCRIPT_BUDGET_PERCENTAGE(fPeakUpdateTime))
	
	PRINTLN("[PERFORMANCE_METRICS] - Is Host: ", BOOL_TO_STRING(bIsLocalPlayerHost))
	PRINTLN("[PERFORMANCE_METRICS] - Client Mission Stage: ", GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(GET_MC_CLIENT_MISSION_STAGE(iLocalPart)))
	
	iLastPerformanceCheck = NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE())
		
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicle Weapons
// ##### Description: Processing of plane bombs, air countermeasures and mines. Normally handled in freemode.sc - processed from here in test mode only.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC DEBUG_PROCESS_VEHICLE_WEAPONS()

	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		EXIT
	ENDIF
	
	MAINTAIN_VEHICLE_MINES(sVehicleWeaponsData)
	
	MAINTAIN_AIR_COUNTERMEASURES(sVehicleWeaponsData)
	
	MAINTAIN_AIR_BOMBS(sVehicleWeaponsData)

ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Process Debug main functions	 --------------------------------------------------
// ##### Description: Main debug function calls -------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC PROCESS_MAIN_MISSION_CONTROLLER_DEBUG()

	iPrintNum = 0
			
	IF iPlayerPressedF != -1 
		PRINTLN("[RCC MISSION]  player pressed F: ",iPlayerPressedF)
	ENDIF
	IF iPlayerPressedS != -1
		PRINTLN("[RCC MISSION] player pressed S: ",iPlayerPressedS) 
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PRESSED_F)
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_USED_F_FAIL, ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION, "F FAIL mission Debug has been used.")		
		#ENDIF
	ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PRESSED_S)
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_USED_S_PASS, ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION, "S PASS mission Debug has been used.")
		#ENDIF
	ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_7)
		TOGGLE_RENDERPHASES(TRUE)
	ENDIF
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_7, KEYBOARD_MODIFIER_SHIFT, "FadeIn")
		DO_SCREEN_FADE_IN(200)
	ENDIF
	
	//Do/Don't Fail On Death debug, apostrophe key:
	IF NOT IS_BIT_SET(iLocalDebugBS, LDEBUGBS_FIRST_DONTFAILONDEATH_CHECK)
		
		IF NOT HAS_NET_TIMER_STARTED(tdDontFailOnDeathDelayTimer)
			IF MC_playerBD[iLocalPart].iGameState = GAME_STATE_RUNNING
			AND GET_MC_SERVER_GAME_STATE() = GAME_STATE_RUNNING
			AND Is_MP_Objective_Text_On_Display() // Has the HUD come ups
				
				PARTICIPANT_INDEX serverPart = NETWORK_GET_HOST_OF_THIS_SCRIPT()
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(serverPart)
					INT iServerPart = NATIVE_TO_INT(serverPart)
					
					IF (iServerPart >= 0) AND (iServerPart < MAX_NUM_MC_PLAYERS)
						IF MC_playerBD[iServerPart].iGameState = GAME_STATE_RUNNING
							PRINTLN("[RCC MISSION][FailOnDeath] Init - I'm in and the server is in, start the timer!")
							REINIT_NET_TIMER(tdDontFailOnDeathDelayTimer)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdDontFailOnDeathDelayTimer) >= 3000
				IF NOT IS_BIT_SET(MC_serverBD.iDebugBitSet, SBDEBUG_DONTFAILWHENOUTOFLIVES)
				AND g_bIgnoreOutOfLivesFailure
					//The server thinks we should fail when out of lives but I don't, tell the server to sort it out
					
					PRINTLN("[RCC MISSION][FailOnDeath] Init - Server doesn't have SBDEBUG_DONTFAILWHENOUTOFLIVES set, but I have g_bIgnoreOutOfLivesFailure set, broadcast it out!")
					BROADCAST_FMMC_OVERRIDE_LIVES_FAIL(TRUE)
					
				ELIF IS_BIT_SET(MC_serverBD.iDebugBitSet, SBDEBUG_DONTFAILWHENOUTOFLIVES)
					
					PLAYER_INDEX serverPlayer = INVALID_PLAYER_INDEX()
					PARTICIPANT_INDEX serverPart = NETWORK_GET_HOST_OF_THIS_SCRIPT()
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(serverPart) // should be true for us to reach this point
						serverPlayer = NETWORK_GET_PLAYER_INDEX(serverPart)
					ENDIF
					
					PRINTLN("[RCC MISSION][FailOnDeath] Init - Server has SBDEBUG_DONTFAILWHENOUTOFLIVES, print the ticker and synchronise g_bIgnoreOutOfLivesFailure by setting it TRUE")
					PRINT_TICKER_WITH_PLAYER_NAME_AND_STRING("TICK_CHEATED", serverPlayer, "TICK_CHEAT_OOL1")
					g_bIgnoreOutOfLivesFailure = TRUE
				ENDIF
				
				PRINTLN("[RCC MISSION][FailOnDeath] Init - Initialisation completed, g_bIgnoreOutOfLivesFailure = ",g_bIgnoreOutOfLivesFailure)
				
				SET_BIT(iLocalDebugBS, LDEBUGBS_FIRST_DONTFAILONDEATH_CHECK)
				RESET_NET_TIMER(tdDontFailOnDeathDelayTimer)
			ENDIF
			
		ENDIF
	ELSE
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_APOSTROPHE)
			IF g_bIgnoreOutOfLivesFailure
				PRINTLN("[RCC MISSION][FailOnDeath] Broadcast out that g_bIgnoreOutOfLivesFailure should be false")
				BROADCAST_FMMC_OVERRIDE_LIVES_FAIL(FALSE)
			ELSE
				PRINTLN("[RCC MISSION][FailOnDeath] Broadcast out that g_bIgnoreOutOfLivesFailure should be true")
				//g_bIgnoreOutOfLivesFailure = TRUE
				BROADCAST_FMMC_OVERRIDE_LIVES_FAIL(TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	//Home key requests to be the host of the mission controller script
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_HOME)
		IF NOT bIsLocalPlayerHost
			NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT()
		ENDIF
	ENDIF
	
	g_iDialogueProgressLastPlayed = iDialogueProgressLastPlayed
	
	DRAW_MISSION_START_DEBUG_HELP()
	
	DRAW_DEBUG_CONTENT_CONTROLS()
	
	PROCESS_PRINT_AGGRO_INDEX_INFORMATION()
	
	PROCESS_PRINT_SPAWN_GROUP_INFORMATION()
	
	PROCESS_PRINT_SPAWN_POINT_INFORMATION()
	
	PROCESS_CUSTOM_RESPAWN_DEBUG(g_SpawnData.MissionSpawnDetails.SpawnArea[0].bIsActive)
	
	DEBUG_SHOW_SYNC_SCENE_STATES()
	
	PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW(&PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_MAIN_FOR_SPECIFIC_CONTENT, &PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT)
	
	PROCESS_RUNTIME_ERROR_ALERT_ON_SCREEN(g_bMissionClientGameStateRunning AND sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_NONE)
	
	DEBUG_PROCESS_PED_GOTO_INFO_AND_MARKERS()
	
	DEBUG_PROCESS_OBJ_GOTO_INFO_AND_MARKERS()
	
	DEBUG_PROCESS_PED_DEFENSIVE_AREAS_INFO_AND_MARKERS()
	
	PROCESS_PED_COMPANION_DEBUG()
	
	MAINTAIN_EOM_DATA_DUMP(TRUE)
		
	MAINTAIN_PLAYER_ID_CHANGE_CHECK()			
	
	//Check the status of IS_A_STRAND_MISSION_BEING_INITIALISED every so often and assert if it's wrong
	IF GET_GAME_TIMER() % 1000 < 50
		IF NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
		AND IS_A_STRAND_MISSION_BEING_INITIALISED()
		AND IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23NextContentID[0]) 
		AND IS_STRING_NULL_OR_EMPTY(g_sTransitionSessionData.sStrandMissionData.tl23NextContentID)
			PRINTLN("[TS] [MSRAND] - IS_A_STRAND_MISSION_BEING_INITIALISED and it shouldn't be")
			SCRIPT_ASSERT("IS_A_STRAND_MISSION_BEING_INITIALISED and it shouldn't be")
			CLEAR_THIS_A_STRAND_MISSION_BEING_INITIALISED()
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC DRAW_WORLD_PROP_COVER_BLOCKING_ZONE()
	
	IF NOT IS_DEBUG_KEY_PRESSED(KEY_0, KEYBOARD_MODIFIER_CTRL_SHIFT, "Cover Block Doors.")
		EXIT
	ENDIF
	
	INT iProp
	MODEL_NAMES mnProp
	VECTOR vPos
	VECTOR vMax, vMin
	
	FOR iProp = 0 TO GET_FMMC_MAX_WORLD_PROPS()-1
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sWorldProps[iProp].iBitset, ciFMMC_WorldPropHidden)
			RELOOP
		ENDIF
				
		mnProp = g_FMMC_STRUCT_ENTITIES.sWorldProps[iProp].mn
		vPos = g_FMMC_STRUCT_ENTITIES.sWorldProps[iProp].vPos
		
		IF IS_VECTOR_ZERO(vPos)		
			RELOOP
		ENDIF
		
		IF NOT IS_MODEL_VALID(mnProp)
			RELOOP
		ENDIF
		
		GET_MODEL_DIMENSIONS(mnProp, vMin, vMax)
		
		vMin *= 2.0
		vMax *= 2.0
	
		IF vMax.x > vMax.y
			vMax.y = vMax.x
		ELSE
			vMax.x = vMax.y
		ENDIF
		
		IF vMin.x > vMin.y
			vMin.x = vMin.y
		ELSE
			vMin.y = vMin.x
		ENDIF
		
		DRAW_MARKER(MARKER_BOXES, vPos, (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>), vMax, 200, 50, 50, 200)			
	ENDFOR
ENDPROC
#ENDIF

// ABILITY DEBUG

PROC GET_PLAYER_ABILITY_DEBUG_COLOUR(PLAYER_ABILITY_STATE eState, INT& iR, INT& iG, INT& iB, INT& iA)
	SWITCH eState
		CASE PAS_ACTIVE	// WHITE
			iR = 255
			iG = 255
			iB = 255
			iA = 255
		BREAK
		CASE PAS_COOLDOWN // BLUE
			iR = 0
			iG = 0
			iB = 255
			iA = 255
		BREAK		
		CASE PAS_READY	// GREEN
			iR = 0
			iG = 255
			iB = 0
			iA = 255
		BREAK
		CASE PAS_UNAVAILABLE // RED
			iR = 255
			iG = 0
			iB = 0
			iA = 255
		BREAK
  	ENDSWITCH

ENDPROC

PROC DRAW_ABILITY_DEBUG_RECT(INT iAbilityIndex, FLOAT fRectX, FLOAT fRectY)
	INT iR, iB, iG, iA
	GET_PLAYER_ABILITY_DEBUG_COLOUR(sLocalPlayerAbilities.sAbilities[iAbilityIndex].eState, iR, iG, iB, iA)
	DRAW_RECT(fRectX, fRectY, cfAbilityDebugRectSize, cfAbilityDebugRectSize, iR, iG, iB, iA)
ENDPROC

PROC GET_SNIPER_STATE_DEBUG_COLOUR(SUPPORT_SNIPER_STATE eState, INT& iR, INT& iG, INT& iB, INT& iA)
	SWITCH eState
		CASE SS_INACTIVE // BLACK
			iR = 0
			iG = 0
			iB = 0
			iA = 255
		BREAK
		CASE SS_DEPLOY // BLUE
			iR = 0
			iG = 0
			iB = 255
			iA = 255
		BREAK
		CASE SS_READY // GREEN
			iR = 0
			iG = 255
			iB = 0
			iA = 255
		BREAK
		CASE SS_AIM // ORANGE
			iR = 127
			iG = 127
			iB = 0
			iA = 255
		BREAK
		CASE SS_FIRE // RED
			iR = 255
			iG = 0
			iB = 0
			iA = 255
		BREAK
	  ENDSWITCH
ENDPROC

PROC DRAW_SNIPER_ABILITY_DEBUG(FLOAT fRectX, FLOAT fRectY)
	INT iR, iB, iG, iA
	FLOAT xOffset = cfAbilityDebugRectSize * 1.5
	GET_SNIPER_STATE_DEBUG_COLOUR(sLocalPlayerAbilities.sSupportSniperData.eSSState, iR, iG, iB, iA)
	DRAW_RECT(fRectX + xOffset, fRectY, cfAbilityDebugRectSize, cfAbilityDebugRectSize, iR, iG, iB, iA)
ENDPROC

PROC GET_HEAVY_LOADOUT_STATE_DEBUG_COLOUR(HEAVY_LOADOUT_STATE eState, INT& iR, INT& iG, INT& iB, INT& iA)
SWITCH eState
		CASE HL_INACTIVE // BLACK
			iR = 0
			iG = 0
			iB = 0
			iA = 255
		BREAK
		CASE HL_SET_UP_CONTAINER // BLUE
			iR = 0
			iG = 0
			iB = 255
			iA = 255
		BREAK
		CASE HL_SPAWN_CONTAINER // GREEN
			iR = 0
			iG = 255
			iB = 0
			iA = 255
		BREAK
		CASE HL_DROP_CONTAINER // YELLOW
			iR = 255
			iG = 255
			iB = 0
			iA = 255
		BREAK
		CASE HL_SPAWN_CONTENTS // ORANGE
			iR = 255
			iG = 127
			iB = 0
			iA = 255
		BREAK
		CASE HL_FINISHED // RED
			iR = 255
			iG = 0
			iB = 0
			iA = 255
		BREAK
	  ENDSWITCH
ENDPROC

PROC DRAW_HEAVY_LOADOUT_ABILITY_DEBUG(FLOAT fRectX, FLOAT fRectY)
	INT iR, iB, iG, iA
	FLOAT xOffset = cfAbilityDebugRectSize * 1.5
	GET_HEAVY_LOADOUT_STATE_DEBUG_COLOUR(sLocalPlayerAbilities.sHeavyLoadoutData.eHLState, iR, iG, iB, iA)
	DRAW_RECT(fRectX + xOffset, fRectY, cfAbilityDebugRectSize, cfAbilityDebugRectSize, iR, iG, iB, iA)
ENDPROC

PROC PROCESS_PLAYER_ABILIY_DEBUG()
	IF NOT sLocalPlayerAbilities.bShowAbilityDebug
		EXIT
	ENDIF
	
	FLOAT fRectYOffset = 0.1
	FLOAT fRectYPos = 0.1
	FLOAT fRectXPos = 0.1
	INT i = 0
	REPEAT PA_MAX i
		DRAW_ABILITY_DEBUG_RECT(i, fRectXPos, fRectYPos)
		
		IF (INT_TO_ENUM(FMMC_PLAYER_ABILITY_TYPES, i) = PA_SUPPORT_SNIPER)
		AND sLocalPlayerAbilities.sSupportSniperData.bShowSniperAbilityDebug
			DRAW_SNIPER_ABILITY_DEBUG(fRectXPos, fRectYPos)
		ENDIF
		
		IF (INT_TO_ENUM(FMMC_PLAYER_ABILITY_TYPES, i) = PA_HEAVY_LOADOUT)
		AND sLocalPlayerAbilities.sHeavyLoadoutData.bShowHeavyLoadoutDebug
			DRAW_HEAVY_LOADOUT_ABILITY_DEBUG(fRectXPos, fRectYPos)
		ENDIF
		
		fRectYPos += fRectYOffset
	ENDREPEAT
ENDPROC

PROC DRAW_BAG_CAPACITY_DEBUG_TEXT(TEXT_LABEL_63 tl63, INT &iRow)
	VECTOR vScreenPos = <<0.79, 0.2, 0.0>>
	vScreenPos.y += 0.015*iRow
	DRAW_DEBUG_TEXT_2D(tl63, vScreenPos)
	iRow++
ENDPROC

PROC PROCESS_BAG_CAPACITY_DEBUG()
	
	IF !bBagCapacityOnScreenDebug
		EXIT
	ENDIF
	
	INT iRow
	TEXT_LABEL_63 tl63
	
	tl63 = "Max Capacity: "
	tl63 += BAG_CAPACITY__GET_MAX_CAPACITY()
	DRAW_BAG_CAPACITY_DEBUG_TEXT(tl63, iRow)
	
	tl63 = "Current Capacity: "
	tl63 += FLOAT_TO_STRING(MC_playerBD[iLocalPart].sLootBag.fCurrentBagCapacity)
	DRAW_BAG_CAPACITY_DEBUG_TEXT(tl63, iRow)
	
	tl63 = "Current Capacity Cash: "
	tl63 += FLOAT_TO_STRING(MC_playerBD[iLocalPart].sLootBag.fCurrentCapacity[ciLOOT_TYPE_CASH])
	DRAW_BAG_CAPACITY_DEBUG_TEXT(tl63, iRow)
	tl63 = "Cash Weight: "
	tl63 += FLOAT_TO_STRING( BAG_CAPACITY__GET_LOOT_WEIGHT_FOR_GRAB(DUMMY_MODEL_FOR_SCRIPT, ciLOOT_TYPE_CASH))
	DRAW_BAG_CAPACITY_DEBUG_TEXT(tl63, iRow)
	tl63 = "Grabbed Cash: "
	tl63 += sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarBlocks_Cash]
	DRAW_BAG_CAPACITY_DEBUG_TEXT(tl63, iRow)
	
	tl63 = "Current Capacity Coke: "
	tl63 += FLOAT_TO_STRING(MC_playerBD[iLocalPart].sLootBag.fCurrentCapacity[ciLOOT_TYPE_COKE])
	DRAW_BAG_CAPACITY_DEBUG_TEXT(tl63, iRow)
	tl63 = "Coke Weight: "
	tl63 += FLOAT_TO_STRING( BAG_CAPACITY__GET_LOOT_WEIGHT_FOR_GRAB(DUMMY_MODEL_FOR_SCRIPT, ciLOOT_TYPE_COKE))
	DRAW_BAG_CAPACITY_DEBUG_TEXT(tl63, iRow)
	tl63 = "Grabbed Coke: "
	tl63 += sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarBlocks_Coke]
	DRAW_BAG_CAPACITY_DEBUG_TEXT(tl63, iRow)
	
	tl63 = "Current Capacity Weed: "
	tl63 += FLOAT_TO_STRING(MC_playerBD[iLocalPart].sLootBag.fCurrentCapacity[ciLOOT_TYPE_WEED])
	DRAW_BAG_CAPACITY_DEBUG_TEXT(tl63, iRow)
	tl63 = "Weed Weight: "
	tl63 += FLOAT_TO_STRING( BAG_CAPACITY__GET_LOOT_WEIGHT_FOR_GRAB(DUMMY_MODEL_FOR_SCRIPT, ciLOOT_TYPE_WEED))
	DRAW_BAG_CAPACITY_DEBUG_TEXT(tl63, iRow)
	tl63 = "Grabbed Weed: "
	tl63 += sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarBlocks_Weed]
	DRAW_BAG_CAPACITY_DEBUG_TEXT(tl63, iRow)
	
	tl63 = "Current Capacity Gold: "
	tl63 += FLOAT_TO_STRING(MC_playerBD[iLocalPart].sLootBag.fCurrentCapacity[ciLOOT_TYPE_GOLD])
	DRAW_BAG_CAPACITY_DEBUG_TEXT(tl63, iRow)
	tl63 = "Gold Weight: "
	tl63 += FLOAT_TO_STRING( BAG_CAPACITY__GET_LOOT_WEIGHT_FOR_GRAB(DUMMY_MODEL_FOR_SCRIPT, ciLOOT_TYPE_GOLD))
	DRAW_BAG_CAPACITY_DEBUG_TEXT(tl63, iRow)
	tl63 = "Grabbed Gold: "
	tl63 += sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarBlocks_Gold]
	DRAW_BAG_CAPACITY_DEBUG_TEXT(tl63, iRow)
	
	tl63 = "Current Capacity Painting: "
	tl63 += FLOAT_TO_STRING(MC_playerBD[iLocalPart].sLootBag.fCurrentCapacity[ciLOOT_TYPE_PAINTING])
	DRAW_BAG_CAPACITY_DEBUG_TEXT(tl63, iRow)
	tl63 = "Painting Weight: "
	tl63 += FLOAT_TO_STRING( BAG_CAPACITY__GET_LOOT_WEIGHT_FOR_GRAB(DUMMY_MODEL_FOR_SCRIPT, ciLOOT_TYPE_PAINTING))
	DRAW_BAG_CAPACITY_DEBUG_TEXT(tl63, iRow)
	tl63 = "Grabbed Painting: "
	tl63 += sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarBlocks_Paint]
	DRAW_BAG_CAPACITY_DEBUG_TEXT(tl63, iRow)
	
ENDPROC

PROC PROCESS_DEBUG_OUTFIT_CHANGE()

	IF iDebug_OutfitValueOverrideLast = iDebug_OutfitValueOverride
		EXIT
	ENDIF
	
	IF iDebug_OutfitValueOverride != ciDEBUG_OUTFIT_NONE
		IF iCachedPlayerOutfit = -1
			iCachedPlayerOutfit = MC_playerBD[iLocalPart].iOutfit
			PRINTLN("PROCESS_DEBUG_OUTFIT_CHANGE - iCachedPlayerOutfit set to: ", iCachedPlayerOutfit)
		ENDIF
		SWITCH iDebug_OutfitValueOverride
			CASE ciDEBUG_OUTFIT_BUGSTAR
				MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_BUGSTARS_0)
			BREAK
			CASE ciDEBUG_OUTFIT_MAINTENANCE
				MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_MAINTENANCE_0)
			BREAK
			CASE ciDEBUG_OUTFIT_GRUPPE
				MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_GRUPPE_SECHS_0)
			BREAK
			CASE ciDEBUG_OUTFIT_CELEB
				MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_CELEB_0)
			BREAK
			CASE ciDEBUG_OUTFIT_FIREFIGHTER
				MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIREFIGHTER_0)
			BREAK
			CASE ciDEBUG_OUTFIT_NOOSE
				MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_NOOSE_0)
			BREAK
			CASE ciDEBUG_OUTFIT_HIGHROLL
				MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_HIGH_ROLLER_0)
			BREAK
			CASE ciDEBUG_OUTFIT_ISLANDGUARD
				MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_ISLAND_HEIST_GUARD_0)
			BREAK
			CASE ciDEBUG_OUTFIT_SMUGGLER
				MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_ISLAND_HEIST_SMUGGLER_0)
			BREAK
			CASE ciDEBUG_OUTFIT_ULP_MAINTENANCE
				MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_MAINTENANCE_0)
			BREAK
		ENDSWITCH
		iDebug_OutfitValueOverrideLast = iDebug_OutfitValueOverride
		PRINTLN("PROCESS_DEBUG_OUTFIT_CHANGE - MC_playerBD[iLocalPart].iOutfit is now set to: ", MC_playerBD[iLocalPart].iOutfit)
	ELSE
		IF iCachedPlayerOutfit != -1
		AND MC_playerBD[iLocalPart].iOutfit != iCachedPlayerOutfit
			MC_playerBD[iLocalPart].iOutfit = iCachedPlayerOutfit
			iCachedPlayerOutfit = -1
			PRINTLN("PROCESS_DEBUG_OUTFIT_CHANGE - MC_playerBD[iLocalPart].iOutfit set back to: ", MC_playerBD[iLocalPart].iOutfit)
		ENDIF
		iDebug_OutfitValueOverrideLast = iDebug_OutfitValueOverride
	ENDIF
ENDPROC

FUNC BOOL DEBUG_CHECK_FOR_CS_MODEL_NAME_STRING(STRING sName)

	IF IS_STRING_NULL_OR_EMPTY(sName)
		RETURN FALSE
	ENDIF
	
	STRING sFirstN

	IF GET_LENGTH_OF_LITERAL_STRING(sName) >= 3
		sFirstN = GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(sName, 3)	
		IF ARE_STRINGS_EQUAL(sFirstN, "cs_")
			RETURN TRUE
		ENDIF
	ENDIF
	IF GET_LENGTH_OF_LITERAL_STRING(sName) >= 4
		sFirstN = GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(sName, 4)
		IF ARE_STRINGS_EQUAL(sFirstN, "cs2_")
		OR ARE_STRINGS_EQUAL(sFirstN, "csb_")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DEBUG_CHECK_THERE_ARE_NO_CS_MODELS_USED_FOR_GAMEPLAY()
	
	IF bVerifiedCSModels
		EXIT
	ENDIF
	
	IF NOT g_bMissionClientGameStateRunning
		EXIT
	ENDIF
	
	INT i, iCutscene
	INT iCustomType[20]
	INT iCustomIndex[20]	
	INT iCustomCount, iCustom
		
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1
		IF DEBUG_CHECK_FOR_CS_MODEL_NAME_STRING(FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn))
			iCustomType[iCustomCount] = CREATION_TYPE_PEDS
			iCustomIndex[iCustomCount] = i
			iCustomCount++
		ENDIF
	ENDFOR	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1
		IF DEBUG_CHECK_FOR_CS_MODEL_NAME_STRING(FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn))
			iCustomType[iCustomCount] = CREATION_TYPE_VEHICLES
			iCustomIndex[iCustomCount] = i
			iCustomCount++
		ENDIF
	ENDFOR		
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects-1
		IF DEBUG_CHECK_FOR_CS_MODEL_NAME_STRING(FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn))
			iCustomType[iCustomCount] = CREATION_TYPE_OBJECTS
			iCustomIndex[iCustomCount] = i
			iCustomCount++
		ENDIF
	ENDFOR
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps-1
		IF DEBUG_CHECK_FOR_CS_MODEL_NAME_STRING(FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn))
			iCustomType[iCustomCount] = CREATION_TYPE_DYNOPROPS
			iCustomIndex[iCustomCount] = i
			iCustomCount++
		ENDIF
	ENDFOR
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1
		IF DEBUG_CHECK_FOR_CS_MODEL_NAME_STRING(FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn))
			iCustomType[iCustomCount] = CREATION_TYPE_PROPS
			iCustomIndex[iCustomCount] = i
			iCustomCount++
		ENDIF
	ENDFOR
	
	BOOL bCustomIndexApproved = FALSE
	FOR iCustom = 0 TO iCustomCount-1
		
		bCustomIndexApproved = FALSE
		
		IF iCustom >= 20	   
			#IF IS_DEBUG_BUILD
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_IGNORE, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "There are far too many Cutscene models being used in this mission, this needs to be addressed!")
			#ENDIF			
			BREAKLOOP
		ENDIF
		
		FOR iCutscene = 0 TO _POST_CASINO_HEIST__MAX_SCRIPTED_CUTSCENES-1	
			FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES-1
				INT iType = g_fmmc_struct.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType
				INT iIndex = g_fmmc_struct.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex			
				
				IF iType = iCustomType[iCustom]
				AND iIndex = iCustomIndex[iCustom]
					bCustomIndexApproved = TRUE					
				ENDIF
			ENDFOR
		ENDFOR
		
		IF NOT bCustomIndexApproved
			
			// Do Error
			IF iCustomType[iCustom] = CREATION_TYPE_PEDS
				ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_IGNORE, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_PED, "Model is reserved for Cutscenes but is being used outside of them!", iCustomIndex[iCustom])
			ELIF iCustomType[iCustom] = CREATION_TYPE_VEHICLES
				ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_IGNORE, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_VEH, "Model is reserved for Cutscenes but is being used outside of them!", iCustomIndex[iCustom])
			ELIF iCustomType[iCustom] = CREATION_TYPE_OBJECTS
				ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_IGNORE, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_OBJ, "Model is reserved for Cutscenes but is being used outside of them!", iCustomIndex[iCustom])
			ELIF iCustomType[iCustom] = CREATION_TYPE_DYNOPROPS
				ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_IGNORE, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_DYNO_PROP, "Model is reserved for Cutscenes but is being used outside of them!", iCustomIndex[iCustom])
			ELIF iCustomType[iCustom] = CREATION_TYPE_PROPS
				ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_IGNORE, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_PROP, "Model is reserved for Cutscenes but is being used outside of them!", iCustomIndex[iCustom])
			ENDIF
			
		ENDIF
	ENDFOR
		
	bVerifiedCSModels = TRUE
		
ENDPROC

//fmmc2020 - can likely fuse these functions together
//fmmc2020 - change this function to call all other debug functions - this is the main call for debug processing
//fmmc2020 - see PROCESS_MAIN_MISSION_CONTROLLER_DEBUG()
PROC PROCESS_DEBUG()
	
	PROCESS_PERFORMANCE_METRIC_LOGGING()
	
	PROCESS_MISSION_CONTROLLER_PROFILING()
	
	DEBUG_PROCESS_VEHICLE_WEAPONS()
	
	#IF IS_DEBUG_BUILD
	MC_UPDATE_PLAYER_RESPAWN_STATE()
	#ENDIF
	
	PROCESS_BURNING_VEHICLE_DEBUG()
	
	DRAW_WORLD_PROP_COVER_BLOCKING_ZONE()
	
	PROCESS_PROXIMITY_SPAWNING_DEBUG()
	
	PROCESS_PLAYER_ABILIY_DEBUG()
	
	PROCESS_LOGGING_ON_BUG_ENTRY()
	
	PROCESS_XML_OVERRIDE_TEXT()
	
	PROCESS_BAG_CAPACITY_DEBUG()
	
	PROCESS_DEBUG_OUTFIT_CHANGE()
	
	DEBUG_CHECK_THERE_ARE_NO_CS_MODELS_USED_FOR_GAMEPLAY()
	
	IF bAllFMMCPrereqsSet
		INT iPreReq = 0
		FOR iPreReq = 0 TO ciPREREQ_Bitsets - 1
			g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iPrerequisiteBS[iPreReq] = -1
		ENDFOR
	ENDIF
	
	//[FMMC2020] - Everything below needs sorted out
	
	INT i		

	INT iHealthRequiredForRule
	INT iTeam = MC_playerBD[iPartToUse].iteam
	
	IF bUsedDebugMenu
	AND sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_NONE
		SET_TEXT_SCALE(0.6800, 0.6800)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		SET_TEXT_CENTRE(TRUE)
		SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)
		SET_TEXT_EDGE(0, 0, 0, 0, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.0450, "STRING", "USED SKIP MENU!!")
	ENDIF
	
	PROCESS_MAIN_MISSION_CONTROLLER_DEBUG()

	IF iSpectatorTarget = -1
	
		IF IS_BIT_SET(MC_serverBD.ijSkipBitset,iPartToUse)
			CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_PRESSED_J)
		ENDIF
				
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K) 
			SWITCH GET_MC_CLIENT_MISSION_STAGE(iPartToUse)
				CASE CLIENT_MISSION_STAGE_KILL_PED
					FOR i = 0 TO (FMMC_MAX_PEDS-1)
						IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
							IF MC_serverBD_4.iPedRule[i][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL
								SET_ENTITY_HEALTH(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i]),0)
							ENDIF
						ENDIF
					ENDFOR
				BREAK
				
				CASE CLIENT_MISSION_STAGE_DAMAGE_PED
					FOR i = 0 TO (FMMC_MAX_PEDS-1)
						IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
							IF MC_serverBD_4.iPedRule[i][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_DAMAGE								
								iHealthRequiredForRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iHealthDamageRequiredForRule
								IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
								AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPriorityEntityHealthThresholdOverride[MC_serverBD_4.iCurrentHighestPriority[iTeam]] != 0
									iHealthRequiredForRule = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPriorityEntityHealthThresholdOverride[MC_serverBD_4.iCurrentHighestPriority[iTeam]]
								ENDIF
								SET_ENTITY_HEALTH(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i]), iHealthRequiredForRule)
							ENDIF
						ENDIF
					ENDFOR
				BREAK
				
				CASE CLIENT_MISSION_STAGE_KILL_VEH
					FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
						AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
							IF MC_serverBD_4.ivehRule[i][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL
								PRINTLN("[RCC MISSION] PROCESS_DEBUG - Calling NETWORK_EXPLODE_VEHICLE on veh ",i," as K key was pressed")
								NETWORK_EXPLODE_VEHICLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
							ENDIF
						ENDIF
					ENDFOR
				BREAK
				
				CASE CLIENT_MISSION_STAGE_DAMAGE_VEH
					FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
						AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
							IF MC_serverBD_4.iVehRule[i][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_DAMAGE
								iHealthRequiredForRule = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iHealthDamageRequiredForRule
								IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
								AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPriorityEntityHealthThresholdOverride[MC_serverBD_4.iCurrentHighestPriority[iTeam]] != 0
									iHealthRequiredForRule = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPriorityEntityHealthThresholdOverride[MC_serverBD_4.iCurrentHighestPriority[iTeam]]
								ENDIF
								SET_ENTITY_HEALTH(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]), iHealthRequiredForRule)
							ENDIF
						ENDIF
					ENDFOR
				BREAK
				
				CASE CLIENT_MISSION_STAGE_KILL_OBJ
					FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
						IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(i))
						AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(GET_OBJECT_NET_ID(i)))
							IF MC_serverBD_4.iObjRule[i][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL
								SET_ENTITY_HEALTH(NET_TO_OBJ(GET_OBJECT_NET_ID(i)),0)
							ENDIF
						ENDIF
					ENDFOR
				BREAK
				
				CASE CLIENT_MISSION_STAGE_DAMAGE_OBJ
					FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
						IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(i))
						AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(GET_OBJECT_NET_ID(i)))
							IF MC_serverBD_4.iObjRule[i][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_DAMAGE
								iHealthRequiredForRule = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iHealthDamageRequiredForRule
								IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
								AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPriorityEntityHealthThresholdOverride[MC_serverBD_4.iCurrentHighestPriority[iTeam]] != 0
									iHealthRequiredForRule = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPriorityEntityHealthThresholdOverride[MC_serverBD_4.iCurrentHighestPriority[iTeam]]
								ENDIF
								SET_ENTITY_HEALTH(NET_TO_OBJ(GET_OBJECT_NET_ID(i)), iHealthRequiredForRule)
							ENDIF
						ENDIF
					ENDFOR
				BREAK
				
			ENDSWITCH
		ENDIF
		
		IF iSkipTeam = -1
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_1) 
				PRINTLN("[RCC MISSION] PROCESS_DEBUG - Calling populate MissionStageMenuTextStruct for team 0 ")
				iSkipTeam = 0
			ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_2) 
				PRINTLN("[RCC MISSION] PROCESS_DEBUG - Calling populate MissionStageMenuTextStruct for team 1 ")
				iSkipTeam = 1
			ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_3) 
				PRINTLN("[RCC MISSION] PROCESS_DEBUG - Calling populate MissionStageMenuTextStruct for team 2 ")
				iSkipTeam = 2
			ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_4) 
				PRINTLN("[RCC MISSION] PROCESS_DEBUG - Calling populate MissionStageMenuTextStruct for team 3 ")
				iSkipTeam = 3
			ENDIF
			
			IF iSkipTeam > -1
				bDrawMenu = TRUE
				
				INT iLoop, iRule
				
				FOR iLoop = 0 TO (FMMC_MAX_RULES + 1)
					IF iLoop = 0
						sTeamSkipStruct[iLoop].sTxtLabel = "Join This Team"
						sTeamSkipStruct[iLoop].bSelectable = TRUE
					ELIF iLoop = 1
						sTeamSkipStruct[iLoop].sTxtLabel = "---Objectives---"
						sTeamSkipStruct[iLoop].bSelectable = FALSE
					ELIF iLoop >= 2
						
						iRule = iLoop - 2 // Goes from 0 to FMMC_MAX_RULES - 1
						
						TEXT_LABEL_63 tl63CustomGodText = g_FMMC_STRUCT.sFMMCEndConditions[iSkipTeam].tl63Objective[iRule]
						
						IF iRule < MC_serverBD_4.iCurrentHighestPriority[iSkipTeam]
							sTeamSkipStruct[iLoop].sTxtLabel = "("
							sTeamSkipStruct[iLoop].sTxtLabel += tl63CustomGodText
							sTeamSkipStruct[iLoop].sTxtLabel += ")"
							sTeamSkipStruct[iLoop].bSelectable = TRUE
							PRINTLN("[RCC MISSION] PROCESS_DEBUG - objective already complete: ",iRule, " for team: ",iSkipTeam)
						ELIF iRule = MC_serverBD_4.iCurrentHighestPriority[iSkipTeam]
							sTeamSkipStruct[iLoop].sTxtLabel = "**"
							sTeamSkipStruct[iLoop].sTxtLabel += tl63CustomGodText
							sTeamSkipStruct[iLoop].sTxtLabel += "**"
							sTeamSkipStruct[iLoop].bSelectable = TRUE
							PRINTLN("[RCC MISSION] PROCESS_DEBUG - current objective: ",iRule, " for team: ",iSkipTeam)
						ELIF iRule <= MC_serverBD.iMaxObjectives[iSkipTeam] 
							sTeamSkipStruct[iLoop].sTxtLabel = tl63CustomGodText
							sTeamSkipStruct[iLoop].bSelectable = TRUE
							PRINTLN("[RCC MISSION] PROCESS_DEBUG - objective still valid: ",iRule, " for team: ",iSkipTeam)
						ELSE
							sTeamSkipStruct[iLoop].bSelectable = FALSE
							PRINTLN("[RCC MISSION] PROCESS_DEBUG - objective does not exist: ",iRule, " for team: ",iSkipTeam)
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ELSE
			STRING sMenuTitle
			IF iSkipTeam = 0
				sMenuTitle = "TEAM 1"
			ELIF iSkipTeam = 1
				sMenuTitle = "TEAM 2"
			ELIF iSkipTeam = 2
				sMenuTitle = "TEAM 3"
			ELIF iSkipTeam = 3
				sMenuTitle = "TEAM 4"
			ENDIF
			
			INT iStageToSkipTo
			
			IF LAUNCH_MISSION_STAGE_MENU(sTeamSkipStruct, iStageToSkipTo, 0, FALSE, sMenuTitle, FALSE)
				IF iStageToSkipTo = 0 // Swap to team has been selected
					IF iSkipTeam != MC_playerBD[iPartToUse].iteam
						PRINTLN("[RCC MISSION] PROCESS_DEBUG - LAUNCH_MISSION_STAGE_MENU called for a change from my team ",MC_playerBD[iPartToUse].iteam," to new team: ", iSkipTeam)
						
						IF bIsLocalPlayerHost
							STOP_MISSION_FAILING_AS_TEAM_SWAPS(MC_playerBD[iPartToUse].iteam, iSkipTeam)
						ELSE
							BROADCAST_DEBUG_OBJECTIVE_SKIP_FOR_TEAM(MC_playerBD[iPartToUse].iteam, -1, iSkipTeam)
						ENDIF
						
						CHANGE_PLAYER_TEAM(iSkipTeam, TRUE)
						
						//Get the objective text updating:
						Clear_Any_Objective_Text()
						
						SET_BIT(iLocalDebugBS, LDEBUGBS_TEAM_CHANGED_WAIT_FOR_OBJTXT)
						
						iLastRule = -1
						
						REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_DEBUG - LAUNCH_MISSION_STAGE_MENU" #ENDIF )
					ENDIF
				ELIF iStageToSkipTo >= 2 // Jump to a rule has been selected
					
					iStageToSkipTo -= 2
					
					IF iStageToSkipTo != MC_serverBD_4.iCurrentHighestPriority[iSkipTeam]
						IF bIsLocalPlayerHost
							BOOL bPreviousObjective = FALSE
							
							IF iStageToSkipTo < MC_serverBD_4.iCurrentHighestPriority[iSkipTeam]
								PRINTLN("[RCC MISSION] LAUNCH_MISSION_STAGE_MENU - team = ",iSkipTeam, " objective to skip back to: ",iStageToSkipTo)
								bPreviousObjective = TRUE
							ELSE
								PRINTLN("[RCC MISSION] LAUNCH_MISSION_STAGE_MENU - team = ",iSkipTeam, " objective to skip to: ",iStageToSkipTo)
								iStageToSkipTo--
							ENDIF
							
							INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iSkipTeam, iStageToSkipTo, FALSE, TRUE, bPreviousObjective)
							
							//SET_PROGRESS_OBJECTIVE_FOR_TEAM(iSkipTeam, TRUE, iStageToSkipTo)
						ELSE
							BROADCAST_DEBUG_OBJECTIVE_SKIP_FOR_TEAM(iSkipTeam, iStageToSkipTo)
						ENDIF
					ENDIF
										
					#IF IS_DEBUG_BUILD
					ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_USED_1_MENU_RULE_SKIP, ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION, "Rule Skip Menu has been used.")		
					#ENDIF
				ENDIF
				
				bUsedDebugMenu = TRUE
				CLEANUP_MENU_SKIP()
			ELSE
				
				SWITCH iSkipTeam
					CASE 0
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_1) 
							PRINTLN("[RCC MISSION] PROCESS_DEBUG - CLEANUP_MENU_SKIP for team 0")
							CLEANUP_MENU_SKIP()
						ENDIF
					BREAK
					CASE 1
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_2) 
							PRINTLN("[RCC MISSION] PROCESS_DEBUG - CLEANUP_MENU_SKIP for team 1")
							CLEANUP_MENU_SKIP()
						ENDIF
					BREAK
					CASE 2
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_3) 
							PRINTLN("[RCC MISSION] PROCESS_DEBUG - CLEANUP_MENU_SKIP for team 2")
							CLEANUP_MENU_SKIP()
						ENDIF
					BREAK
					CASE 3
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_4) 
							PRINTLN("[RCC MISSION] PROCESS_DEBUG - CLEANUP_MENU_SKIP for team 3")
							CLEANUP_MENU_SKIP()
						ENDIF
					BREAK
				ENDSWITCH
				
			ENDIF
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) 
			IF bPlayerToUseOK
				
				IF IS_LOCAL_PLAYER_INTERACTING()
					PRINTLN("[Interactables][Interactable ", GET_FOREGROUND_INTERACTABLE_INDEX(), "] PROCESS_DEBUG | J-Skipping foreground interaction!")
					SET_INTERACTABLE_COMPLETE(GET_FOREGROUND_INTERACTABLE_INDEX())
					BAIL_FOREGROUND_INTERACTION_ASAP()
					NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
					EXIT
				ENDIF
				
				#IF IS_DEBUG_BUILD
				ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_USED_J_SKIP, ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION, "Local Player used J Skip Debug.")		
				#ENDIF
				
				SET_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_PRESSED_J)
				
				IF IS_CURRENT_OBJECTIVE_COLLECT_OBJECT()
					INT iObjToCollect = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iObjRequiredForRulePass[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]]
	
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_OBJECT_NET_ID(iObjToCollect))
						IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, NET_TO_OBJ(GET_OBJECT_NET_ID(iObjToCollect))) > 10
							IF NOT IS_ENTITY_ATTACHED(NET_TO_OBJ(GET_OBJECT_NET_ID(iObjToCollect)))
								J_SKIP_MP(GET_ENTITY_COORDS(NET_TO_OBJ(GET_OBJECT_NET_ID(iObjToCollect))), << 6.0, 6.0, 6.0 >>)
								EXIT
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				SWITCH GET_MC_CLIENT_MISSION_STAGE(iPartToUse)

					CASE CLIENT_MISSION_STAGE_GOTO_LOC
					CASE CLIENT_MISSION_STAGE_CAPTURE_AREA
					CASE CLIENT_MISSION_STAGE_PHOTO_LOC
						FOR i = 0 TO (FMMC_MAX_GO_TO_LOCATIONS-1)
							IF MC_serverBD_4.iGotoLocationDataPriority[i][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, GET_LOCATION_VECTOR(i)) > 10
									J_SKIP_MP(GET_LOCATION_VECTOR(i), << 4.0, 4.0, 4.0 >>)
									EXIT
								ENDIF
							ENDIF
						ENDFOR
					BREAK


					CASE CLIENT_MISSION_STAGE_KILL_PED
					CASE CLIENT_MISSION_STAGE_DAMAGE_PED
					CASE CLIENT_MISSION_STAGE_PROTECT_PED
					CASE CLIENT_MISSION_STAGE_COLLECT_PED
					CASE CLIENT_MISSION_STAGE_GOTO_PED
					CASE CLIENT_MISSION_STAGE_CAPTURE_PED
					CASE CLIENT_MISSION_STAGE_PHOTO_PED					
					
						FOR i = 0 TO (FMMC_MAX_PEDS-1)
							IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
								IF MC_serverBD_4.iPedPriority[i][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
									IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])) > 10
										IF NOT IS_PED_IN_GROUP(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i]))
											J_SKIP_MP(GET_ENTITY_COORDS(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])), << 4.0, 4.0, 4.0 >>, FALSE )
											EXIT
										ENDIF
									ELSE
										IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_KILL_PED
											IF MC_serverBD_4.iPedRule[i][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL
												SET_ENTITY_HEALTH(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i]),0)
												EXIT
											ENDIF
										ENDIF
										IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_DAMAGE_PED
											IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
												IF MC_serverBD_4.iPedRule[i][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_DAMAGE
													iHealthRequiredForRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iHealthDamageRequiredForRule
													IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
													AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPriorityEntityHealthThresholdOverride[MC_serverBD_4.iCurrentHighestPriority[iTeam]] != 0
														iHealthRequiredForRule = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPriorityEntityHealthThresholdOverride[MC_serverBD_4.iCurrentHighestPriority[iTeam]]
													ENDIF
													SET_ENTITY_HEALTH(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i]), iHealthRequiredForRule)
													
													EXIT
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						
						FOR i = 0 TO (FMMC_MAX_PEDS-1)
							IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
								IF MC_serverBD_4.iPedPriority[i][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
									IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])) > 10
										J_SKIP_MP(GET_ENTITY_COORDS(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])), << 4.0, 4.0, 4.0 >>,FALSE)
										EXIT
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						
					BREAK
					
					CASE CLIENT_MISSION_STAGE_PED_GOTO_LOCATION
						INT iAssVec
						iAssVec = 0
						FOR i = 0 TO (FMMC_MAX_PEDS-1)
							IF MC_serverBD.iNumPedHighestPriority[iTeam] <= 0
							OR MC_serverBD_4.iPedPriority[i][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam]
							OR MC_serverBD_4.iPedRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_PED_GOTO_LOCATION
								IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])) > 10
									J_SKIP_MP(GET_ENTITY_COORDS(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])), << 4.0, 4.0, 4.0 >>, FALSE)
								ENDIF
								FOR iAssVec = 0 TO GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(MC_serverBD_2.iAssociatedGotoPoolIndex[i], g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData, FALSE) - 1
									SET_BIT(MC_ServerBD_4.iPedCurrentPriorityGotoLocCompletedBS[i], iAssVec)
								ENDFOR
								EXIT
							ENDIF
						ENDFOR
					BREAK
					
					CASE CLIENT_MISSION_STAGE_DELIVER_PED
					
						IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
							FOR i = 0 TO (FMMC_MAX_PEDS-1)
								IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
									IF IS_PED_GROUP_MEMBER(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i]), giPlayerGroup)
										SET_ENTITY_COORDS(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i]),GET_DROP_OFF_CENTER())
									ENDIF
								ENDIF
							ENDFOR
							J_SKIP_MP(GET_DROP_OFF_CENTER(), << 6.0, 6.0, 6.0 >>,FALSE)
						ENDIF
						
					BREAK
					
					CASE CLIENT_MISSION_STAGE_KILL_VEH
					CASE CLIENT_MISSION_STAGE_PROTECT_VEH
					CASE CLIENT_MISSION_STAGE_COLLECT_VEH
					CASE CLIENT_MISSION_STAGE_GOTO_VEH
					CASE CLIENT_MISSION_STAGE_CAPTURE_VEH
					CASE CLIENT_MISSION_STAGE_PHOTO_VEH
					CASE CLIENT_MISSION_STAGE_DAMAGE_VEH
					
						FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
							IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
							AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
								IF MC_serverBD_4.iVehPriority[i][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
									IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])) > 10
										IF IS_VEHICLE_SEAT_FREE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
											J_SKIP_MP(GET_ENTITY_COORDS(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])), << 6.0, 6.0, 6.0 >>)
											EXIT
										ENDIF
									ELSE
										IF MC_serverBD_4.ivehRule[i][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL
											PRINTLN("[RCC MISSION] PROCESS_DEBUG - Calling NETWORK_EXPLODE_VEHICLE on veh ",i," as J key was pressed")
											NETWORK_EXPLODE_VEHICLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
											EXIT
										ELIF MC_serverBD_4.ivehRule[i][MC_playerBD[iPartToUse].iteam] =  FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
											IF IS_VEHICLE_SEAT_FREE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
												TASK_ENTER_VEHICLE(LocalPlayerPed, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]), 1, VS_DRIVER, PEDMOVE_RUN, ECF_WARP_PED)
												EXIT
											ENDIF
										ENDIF
										IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_DAMAGE_VEH
											IF MC_serverBD_4.iVehRule[i][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_DAMAGE
												iHealthRequiredForRule = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iHealthDamageRequiredForRule
												IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
												AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPriorityEntityHealthThresholdOverride[MC_serverBD_4.iCurrentHighestPriority[iTeam]] != 0
													iHealthRequiredForRule = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPriorityEntityHealthThresholdOverride[MC_serverBD_4.iCurrentHighestPriority[iTeam]]
												ENDIF
												SET_ENTITY_HEALTH(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]), iHealthRequiredForRule)
												EXIT
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						
						FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
							IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
							AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
								IF MC_serverBD_4.iVehPriority[i][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
									IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])) > 10
										J_SKIP_MP(GET_ENTITY_COORDS(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])), << 6.0, 6.0, 6.0 >>)
										EXIT
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						
					BREAK

					CASE CLIENT_MISSION_STAGE_DELIVER_VEH
					
						IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
							IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, GET_DROP_OFF_CENTER()) > 5	
									J_SKIP_MP(GET_DROP_OFF_CENTER(), << 6.0, 6.0, 6.0 >>,TRUE)
								ENDIF
							ENDIF
						ENDIF
						
					BREAK
					
					CASE CLIENT_MISSION_STAGE_KILL_OBJ
					CASE CLIENT_MISSION_STAGE_PROTECT_OBJ
					CASE CLIENT_MISSION_STAGE_COLLECT_OBJ
					CASE CLIENT_MISSION_STAGE_GOTO_OBJ
					CASE CLIENT_MISSION_STAGE_CAPTURE_OBJ
					CASE CLIENT_MISSION_STAGE_HACK_COMP
					CASE CLIENT_MISSION_STAGE_PHOTO_OBJ
					CASE CLIENT_MISSION_STAGE_HACK_DRILL
					CASE CLIENT_MISSION_STAGE_INTERACT_WITH
					CASE CLIENT_MISSION_STAGE_GLASS_CUTTING
					CASE CLIENT_MISSION_STAGE_UNDERWATER_WELDING
					CASE CLIENT_MISSION_STAGE_INTERROGATION
					CASE CLIENT_MISSION_STAGE_ENTER_SAFE_COMBINATION
					CASE CLIENT_MISSION_STAGE_DAMAGE_OBJ
						FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
							IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(i))
								IF NOT IS_ENTITY_DEAD(NET_TO_OBJ(GET_OBJECT_NET_ID(i)))
									IF MC_serverBD_4.iObjPriority[i][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
										IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, NET_TO_OBJ(GET_OBJECT_NET_ID(i))) > 10
											IF NOT IS_ENTITY_ATTACHED(NET_TO_OBJ(GET_OBJECT_NET_ID(i)))
												J_SKIP_MP(GET_ENTITY_COORDS(NET_TO_OBJ(GET_OBJECT_NET_ID(i))), << 6.0, 6.0, 6.0 >>)
												EXIT
											ENDIF
										ELSE
											IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_KILL_OBJ
												IF MC_serverBD_4.iObjRule[i][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL
													SET_ENTITY_HEALTH(NET_TO_OBJ(GET_OBJECT_NET_ID(i)),0)
													EXIT
												ENDIF
											ELIF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) =  CLIENT_MISSION_STAGE_HACK_COMP
												IF MC_playerBD[iPartToUse].iObjHacking != -1
													HACKING_J_SKIP()
													EXIT
												ENDIF
											ELIF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_INTERACT_WITH
												MC_playerBD[iPartToUse].iObjHacked = i
												EXIT
												
											ELIF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_GLASS_CUTTING
											AND MC_playerBD[iPartToUse].iObjHacking = -1
												MC_playerBD[iPartToUse].iObjHacked = i
												EXIT
											
											ELIF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_INTERROGATION
											AND MC_playerBD[iPartToUse].iObjHacking = -1
												MC_playerBD[iPartToUse].iObjHacked = i
												EXIT
												
											ELIF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_UNDERWATER_WELDING
											AND MC_playerBD[iPartToUse].iObjHacking = -1
												MC_playerBD[iPartToUse].iObjHacked = i
												EXIT
												
											ELIF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_ENTER_SAFE_COMBINATION
											AND MC_playerBD[iPartToUse].iObjHacking = -1
												MC_playerBD[iPartToUse].iObjHacked = i
												EXIT
											ELIF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_DAMAGE_OBJ
												IF MC_serverBD_4.iObjRule[i][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_DAMAGE
													iHealthRequiredForRule = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iHealthDamageRequiredForRule
													IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
													AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPriorityEntityHealthThresholdOverride[MC_serverBD_4.iCurrentHighestPriority[iTeam]] != 0
														iHealthRequiredForRule = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPriorityEntityHealthThresholdOverride[MC_serverBD_4.iCurrentHighestPriority[iTeam]]
													ENDIF
													SET_ENTITY_HEALTH(NET_TO_OBJ(GET_OBJECT_NET_ID(i)), iHealthRequiredForRule)
													EXIT
												ENDIF
											ENDIF
										ENDIF
									ENDIF 
								ENDIF
							ENDIF
						ENDFOR		
						
						FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
							IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(i))
								IF NOT IS_ENTITY_DEAD(NET_TO_OBJ(GET_OBJECT_NET_ID(i)))
									IF MC_serverBD_4.iObjPriority[i][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
										IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, NET_TO_OBJ(GET_OBJECT_NET_ID(i))) > 10
											J_SKIP_MP(GET_ENTITY_COORDS(NET_TO_OBJ(GET_OBJECT_NET_ID(i))), << 1.0, 1.0, 1.0 >>)
											EXIT
										ENDIF
									ENDIF 
								ENDIF
							ENDIF
						ENDFOR		
						
						IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_CAPTURE_OBJ
							IF MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_TEAMS
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetNine[MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE9_USE_GRANULAR_CAPTURE)
									MC_PlayerBD[iLocalPart].iGranularCurrentPoints += 999999
								ENDIF
							ENDIF
						ENDIF
						
					BREAK
					
					
					CASE CLIENT_MISSION_STAGE_DELIVER_OBJ
					
						IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, GET_DROP_OFF_CENTER()) > 50	
								J_SKIP_MP(GET_DROP_OFF_CENTER(), << 6.0, 6.0, 6.0 >>,TRUE)
							ENDIF
						ENDIF
						
					BREAK

				ENDSWITCH		
			ENDIF
		ENDIF
	ENDIF
	
	MAINTAIN_MISSION_RESTART_DEBUG()
	
	IF bPhoneEMPDebug
		PHONE_EMP_DEBUG()
	ENDIF
	
			
	vRagPlacementVector_Prev = vRagPlacementVector
	vRagRotationVector_Prev = vRagRotationVector
ENDPROC

/// PURPOSE:
///    Called before the main loop to initialise the following debug
///    Widget Creation
///    Commandline Caching
PROC INITIALISE_DEBUG()

	//Create all Mission Controller RAG widgets.
	CREATE_MC_WIDGETS()
	
	CACHE_COMMANDLINES()
	
	LOAD_XML_OVERRIDES()
	
	//Init Test Mode Vehicle Weapons
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		INITIALIZE_VEHICLE_WEAPONS(sVehicleWeaponsData)
	ENDIF
	
ENDPROC

#ENDIF


