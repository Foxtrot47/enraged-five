// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Entities ----------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script Name: FM_Mission_Controller_2020.sc ------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Manages the creation, processing and deletion of all entities (including "abstract entities" such as cutscenes and zones)
// ##### Processes each entity type's staggered loop and every frame loop
// ##### Processes the "all-entity" staggered loop used for objective progression.
// ##### All entity type specific processing should be added to the appropriate header
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Locations_2020.sch"
USING "FM_Mission_Controller_PedsServer_2020.sch"
USING "FM_Mission_Controller_PedsClient_2020.sch"
USING "FM_Mission_Controller_Vehicles_2020.sch"
USING "FM_Mission_Controller_Interactables_2020.sch"
USING "FM_Mission_Controller_Objects_2020.sch"
USING "FM_Mission_Controller_Props_2020.sch"
USING "FM_Mission_Controller_World_2020.sch"
USING "FM_Mission_Controller_Cutscenes_2020.sch"
USING "FM_Mission_Controller_Zones_2020.sch"
USING "FM_Mission_Controller_Bounds_2020.sch"
USING "FM_Mission_Controller_Pickups_2020.sch"
USING "FM_Mission_Controller_Trains_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Placed PTFX
// ##### Description: Functions related to particle effects that are placed in the Creator.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC BOOL SHOULD_CREATE_PLACED_PTFX(INT iPTFX)
	BOOL bReturn = FALSE	
	INT iTeam = -1
	INT iTeamLoop
	INT iTeamMaxLoop = -1
	INT iTeamCreator = g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iTeam
	INT iRuleFrom = g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iRuleFrom
	INT iRuleTo = g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iRuleTo	
	INT iCurrentRule
	
	IF iTeamCreator >= 0
		iTeam = iTeamCreator
		iTeamMaxLoop = iTeamCreator
	ELIF iTeamCreator = -1
		iTeam = 0
		iTeamMaxLoop = (MC_serverBD.iNumberOfTeams - 1)
	ENDIF
	IF iRuleTo = -1
		iRuleTo = FMMC_MAX_RULES
	ENDIF
	
	FOR iTeamLoop = 0 TO iTeamMaxLoop
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iPTFXBs, ciPLACED_PTFX_SpawnOnlyForLocalTeam)
		OR MC_PlayerBD[iPartToUse].iTeam = iTeam
			iCurrentRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]	
			IF iCurrentRule >= iRuleFrom
			AND iCurrentRule < iRuleTo
				bReturn = TRUE
			ENDIF
			iTeam++
		ENDIF
	ENDFOR
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iPTFXBS, ciPLACED_PTFX_MCVarConfig_BlockSpawnUnlessCompletedPrereqMission_1)
		bReturn = FALSE
	ENDIF
	
	RETURN bReturn
ENDFUNC

FUNC BOOL SHOULD_CLEANUP_PLACED_PTFX(INT iPTFX)
	BOOL bReturn = FALSE	
	INT iTeam = -1
	INT iTeamLoop
	INT iTeamMaxLoop = -1
	INT iTeamCreator = g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iTeam
	INT iRuleFrom = g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iRuleFrom
	INT iRuleTo = g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iRuleTo	
	INT iCurrentRule
	
	IF iTeamCreator >= 0
		iTeam = iTeamCreator
		iTeamMaxLoop = iTeamCreator
	ELIF iTeamCreator = -1
		iTeam = 0
		iTeamMaxLoop = (MC_serverBD.iNumberOfTeams - 1)
	ENDIF
	IF iRuleTo = -1
		iRuleTo = FMMC_MAX_RULES
	ENDIF
	
	FOR iTeamLoop = 0 TO iTeamMaxLoop
		iCurrentRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]	
		IF iCurrentRule < iRuleFrom
		OR iCurrentRule >= iRuleTo
			bReturn = TRUE
		ENDIF
		iTeam++
	ENDFOR
	
	IF g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iTimeToPlayFor > 0
	AND HAS_NET_TIMER_EXPIRED(sPlacedPtfxLocal[iPTFX].tdPtfxStarted, g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iTimeToPlayFor * 1000)
		bReturn = TRUE
	ENDIF
	
	RETURN bReturn
ENDFUNC

FUNC BOOL PLAY_PLACED_PTFX_ASSOCIATED_SOUND_FX(INT iPTFX)
	BOOL bNetworked = IS_BIT_SET(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iPTFXBS, ciPLACED_PTFX_isNetworked)
	VECTOR vPos		 	= g_FMMC_STRUCT.sPlacedPTFX[iPTFX].vStartCoord
	TEXT_LABEL_63 tl63_EffectName = g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl63EffectName
	TEXT_LABEL_63 tl63_SoundBank
	TEXT_LABEL_63 tl63_SoundName
	TEXT_LABEL_63 tl63_SoundSet	
	
	BUILD_PLACED_PTFX_ASSOCIATED_SOUND_FX(tl63_EffectName, tl63_SoundBank, tl63_SoundName, tl63_SoundSet)
	
	PRINTLN("[LM][Placed_PTFX] - PLAY_PLACED_PTFX_ASSOCIATED_SOUND_FX - tl63_SoundBank: ", tl63_SoundBank, " tl63_SoundName: ", tl63_SoundName, " tl63_SoundSet: ", tl63_SoundSet)
	
	IF IS_STRING_NULL_OR_EMPTY(tl63_SoundName)
		PRINTLN("[LM][Placed_PTFX] - PLAY_PLACED_PTFX_ASSOCIATED_SOUND_FX - Does not have an associated SFX.")
		RETURN TRUE
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(tl63_SoundBank)
	OR REQUEST_SCRIPT_AUDIO_BANK(tl63_SoundBank)
		IF sPlacedPtfxLocal[iPTFX].iPTFXSoundID = -1
			sPlacedPtfxLocal[iPTFX].iPTFXSoundID = GET_SOUND_ID()
		ENDIF
		PRINTLN("[LM][Placed_PTFX] - PLAY_PLACED_PTFX_ASSOCIATED_SOUND_FX - Playing with SoundID: ", sPlacedPtfxLocal[iPTFX].iPTFXSoundID)
		PLAY_SOUND_FROM_COORD(sPlacedPtfxLocal[iPTFX].iPTFXSoundID, tl63_SoundName, vPos, tl63_SoundSet, bNetworked)
		RETURN TRUE
	ENDIF
	
	PRINTLN("[LM][Placed_PTFX] - PLAY_PLACED_PTFX_ASSOCIATED_SOUND_FX - tl63_SoundBank: ", tl63_SoundBank, " Waiting for it to be loaded...")
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_PLACED_PTFX_EVERY_FRAME()
	INT iPTFX
	FOR iPTFX = 0 TO g_FMMC_STRUCT.iNumPlacedPTFX-1
		IF iPTFX < FMMC_MAX_PLACED_PTFX
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iPTFXBS, ciPLACED_PTFX_MCVarConfig_BlockSpawnUnlessCompletedPrereqMission_1)
				IF IS_BIT_SET(sPlacedPtfxLocal[iPTFX].iPtfxBS, ci_PLACED_PTFX_Loaded)
				AND NOT IS_BIT_SET(sPlacedPtfxLocal[iPTFX].iPtfxBS, ci_PLACED_PTFX_CleanedUp)
					BOOL bLocalOnly = (NOT IS_BIT_SET(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iPTFXBS, ciPLACED_PTFX_isNetworked))
					BOOL bLooped = IS_BIT_SET(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iPTFXBS, ciPLACED_PTFX_isLooped)
					FLOAT fScale = g_FMMC_STRUCT.sPlacedPTFX[iPTFX].fScale
				
					IF NOT IS_BIT_SET(sPlacedPtfxLocal[iPTFX].iPtfxBS, ci_PLACED_PTFX_Created)
						
						// Create...				
						IF SHOULD_CREATE_PLACED_PTFX(iPTFX)					
							PRINTLN("[LM][Placed_PTFX] iPTFX: ", iPTFX, " created using Effect Name: ", g_FMMC_STRUCT.sPlacedPtfx[iPTFX].tl63EffectName, " bLooped: ", bLooped, " bLocalOnly: ", bLocalOnly, " at COORD: ", g_FMMC_STRUCT.sPlacedPTFX[iPTFX].vStartCoord, " for time: ", g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iTimeToPlayFor)
							
							IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl63AssetName)
								PRINTLN("[LM][Placed_PTFX] iPTFX: ", iPTFX, " Calling to use Asset Name: ", g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl63AssetName)
								USE_PARTICLE_FX_ASSET(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl63AssetName)
							ENDIF
							
							IF fScale < 0.1
							OR fScale > 20.0
								PRINTLN("[LM][Placed_PTFX] iPTFX: ", iPTFX, " fScale is invalid, setting to 1 (fScale: ", fScale, ")")
								fScale = 1.0
							ENDIF
							
							IF NOT bLooped
								IF bLocalOnly
									START_PARTICLE_FX_NON_LOOPED_AT_COORD(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl63EffectName, g_FMMC_STRUCT.sPlacedPTFX[iPTFX].vStartCoord, g_FMMC_STRUCT.sPlacedPTFX[iPTFX].vStartRot, fScale)
								ELIF bIsLocalPlayerHost
									START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl63EffectName, g_FMMC_STRUCT.sPlacedPTFX[iPTFX].vStartCoord, g_FMMC_STRUCT.sPlacedPTFX[iPTFX].vStartRot, fScale)
								ENDIF
							ELSE
								sPlacedPtfxLocal[iPTFX].ptfxID = START_PARTICLE_FX_LOOPED_AT_COORD(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl63EffectName, g_FMMC_STRUCT.sPlacedPTFX[iPTFX].vStartCoord, g_FMMC_STRUCT.sPlacedPTFX[iPTFX].vStartRot, fScale, DEFAULT, DEFAULT, DEFAULT, bLocalOnly)
								
								IF g_FMMC_STRUCT.sPlacedPTFX[iPTFX].fEvolution != 0.0
									SET_PARTICLE_FX_LOOPED_EVOLUTION(sPlacedPtfxLocal[iPTFX].ptfxID, g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl15EvolutionName, g_FMMC_STRUCT.sPlacedPTFX[iPTFX].fEvolution)
									PRINTLN("[Placed_PTFX][PTFX ", iPTFX, "] PROCESS_PLACED_PTFX_EVERY_FRAME - Setting evolution ",g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl15EvolutionName," on creation to ", g_FMMC_STRUCT.sPlacedPTFX[iPTFX].fEvolution)
								ENDIF
							
							ENDIF
							
							IF PLAY_PLACED_PTFX_ASSOCIATED_SOUND_FX(iPTFX)
								SET_BIT(sPlacedPtfxLocal[iPTFX].iPtfxBS,  ci_PLACED_PTFX_SFX)
							ENDIF
							
							SET_BIT(sPlacedPtfxLocal[iPTFX].iPtfxBS, ci_PLACED_PTFX_Created)
							RESET_NET_TIMER(sPlacedPtfxLocal[iPTFX].tdPtfxStarted)
							START_NET_TIMER(sPlacedPtfxLocal[iPTFX].tdPtfxStarted)		
						ENDIF
					ELSE
					
						// Process...
						IF NOT SHOULD_CLEANUP_PLACED_PTFX(iPTFX)
							IF NOT IS_BIT_SET(sPlacedPtfxLocal[iPTFX].iPtfxBS,  ci_PLACED_PTFX_SFX)
								IF PLAY_PLACED_PTFX_ASSOCIATED_SOUND_FX(iPTFX)
									SET_BIT(sPlacedPtfxLocal[iPTFX].iPtfxBS,  ci_PLACED_PTFX_SFX)
								ENDIF
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iPTFXBS, ciPLACED_PTFX_EvolutionTiedToFilterStrength)
								SET_PARTICLE_FX_LOOPED_EVOLUTION(sPlacedPtfxLocal[iPTFX].ptfxID, g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl15EvolutionName, fFilterStrength)
							ENDIF
							
						ELSE
							// Cleanup...
							IF bLooped
								IF DOES_PARTICLE_FX_LOOPED_EXIST(sPlacedPtfxLocal[iPTFX].ptfxID)
									PRINTLN("[LM][Placed_PTFX] - iPTFX: ", iPTFX, " Stopping PTFX: ", g_FMMC_STRUCT.sPlacedPtfx[iPTFX].tl63EffectName)
									STOP_PARTICLE_FX_LOOPED(sPlacedPtfxLocal[iPTFX].ptfxID)
								ENDIF
							ELSE
								
							ENDIF
							CLEANUP_PLACED_PTFX_ASSOCIATED_SOUND_FX(iPTFX)
							RESET_NET_TIMER(sPlacedPtfxLocal[iPTFX].tdPtfxStarted)
							CLEAR_BIT(sPlacedPtfxLocal[iPTFX].iPtfxBS, ci_PLACED_PTFX_Created)
							CLEAR_BIT(sPlacedPtfxLocal[iPTFX].iPtfxBS,  ci_PLACED_PTFX_SFX)
							SET_BIT(sPlacedPtfxLocal[iPTFX].iPtfxBS, ci_PLACED_PTFX_CleanedUp)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Other Shared PTFX
// ##### Description: Functions related to particle effect systems shared between entity types.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC CREATE_CUSTOM_DEATH_EXPLOSION_PTFX_FOR_ENTITY(ENTITY_INDEX eiEntity)
	MODEL_NAMES mnEntityModel = GET_ENTITY_MODEL(eiEntity)
	USE_PARTICLE_FX_ASSET(GET_PTFX_ASSET_NAME_FOR_ENTITY_CUSTOM_DEATH_EXPLOSION(mnEntityModel))
	FLOAT fScale = GET_PTFX_SCALE_FOR_ENTITY_CUSTOM_DEATH_EXPLOSION(mnEntityModel)
	
	VECTOR vExplosionCoords = GET_ENTITY_COORDS(eiEntity, FALSE)
	START_PARTICLE_FX_NON_LOOPED_AT_COORD(GET_PTFX_FX_NAME_FOR_ENTITY_CUSTOM_DEATH_EXPLOSION(mnEntityModel), vExplosionCoords, <<0,0,0>>, fScale)
	PRINTLN("[EntityExplosion] Started custom death explosion PTFX for ", FMMC_GET_MODEL_NAME_FOR_DEBUG(mnEntityModel), " at ", vExplosionCoords)	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Generic Render Targets
// ##### Description: Functions related to the shared render target system which can be used by all entity types.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC SET_SHARED_RENDERTARGET_STATE(SHARED_RT_VARS& sRTVars, INT iSharedRTIndex, INT iState)
	UNUSED_PARAMETER(iSharedRTIndex)
	sRTVars.iSharedRT_State = iState
	PRINTLN("[SharedRTs][RT ", iSharedRTIndex, "] SET_SHARED_RENDERTARGET_STATE | Setting state to ", sRTVars.iSharedRT_State)
ENDPROC

PROC PERFORM_SHARED_RENDERTARGET_INIT_FUNCTIONALITY(SHARED_RT_VARS& sRTVars, INT iSharedRTIndex)
	
	UNUSED_PARAMETER(iSharedRTIndex)
	
	PRINTLN("[SharedRTs][RT ", iSharedRTIndex, "] PERFORM_SHARED_RENDERTARGET_INIT_FUNCTIONALITY | Performing init! sRTVars.sInitMethodName: ", sRTVars.sInitMethodName)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sRTVars.sInitMethodName)
		BEGIN_SCALEFORM_MOVIE_METHOD(sRTVars.siScaleformIndex, sRTVars.sInitMethodName)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

PROC PERFORM_SHARED_RENDERTARGET_START_FUNCTIONALITY(SHARED_RT_VARS& sRTVars, INT iSharedRTIndex)
	
	UNUSED_PARAMETER(iSharedRTIndex)
	
	PRINTLN("[SharedRTs][RT ", iSharedRTIndex, "] PERFORM_SHARED_RENDERTARGET_START_FUNCTIONALITY | Performing generic Start!")
	SET_BIT(sRTVars.iSharedRTBitset, ciSHARED_RT_BITSET__GENERIC_STARTED)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sRTVars.sScaleformName)
	AND ARE_STRINGS_EQUAL(sRTVars.sScaleformName, "NAS_UPLOAD")
		BEGIN_SCALEFORM_MOVIE_METHOD(sRTVars.siScaleformIndex, "SHOW_UPLOADING_SCREEN")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

PROC PERFORM_SHARED_RENDERTARGET_COMPLETE_FUNCTIONALITY(SHARED_RT_VARS& sRTVars, INT iSharedRTIndex)
	
	UNUSED_PARAMETER(iSharedRTIndex)
	
	PRINTLN("[SharedRTs][RT ", iSharedRTIndex, "] PERFORM_SHARED_RENDERTARGET_COMPLETE_FUNCTIONALITY | Performing generic Complete!")
	SET_BIT(sRTVars.iSharedRTBitset, ciSHARED_RT_BITSET__GENERIC_COMPLETED)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sRTVars.sScaleformName)
	AND ARE_STRINGS_EQUAL(sRTVars.sScaleformName, "NAS_UPLOAD")
		BEGIN_SCALEFORM_MOVIE_METHOD(sRTVars.siScaleformIndex, "SHOW_COMPLETE_SCREEN")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

PROC PROCESS_SHARED_RENDERTARGET_FILL_BAR_PROGRESS(FMMC_ENTITY_RENDER_TARGET_OPTIONS& sCreatorOptions)
	
	IF IS_BIT_SET(sCreatorOptions.iSharedRT_Bitset, ciSharedRTBitset_MakeBarStuttery)
	AND NOT HAS_LOCAL_PLAYER_MULTIRULE_TIMER_EXPIRED(TRUE)
		IF GET_RANDOM_FLOAT_IN_RANGE(0.0, 1.0) > 0.12
			// Don't progress at all this frame
			EXIT
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sCreatorOptions.iSharedRT_Bitset, ciSharedRTBitset_MatchMultiruleTimer)
		IF IS_LOCAL_PLAYER_MULTIRULE_TIMER_RUNNING(TRUE)
			// Make the bar progress match the time progressed through the current multi-rule timer
			fSharedRT_FillBarProgress = 1.0 - (TO_FLOAT(GET_LOCAL_PLAYER_MULTIRULE_TIMER_TIME_REMAINING(TRUE)) / TO_FLOAT(GET_LOCAL_PLAYER_CURRENT_MULTIRULE_TIMER_TIME_LIMIT(TRUE)))
			
		ELIF GET_LOCAL_PLAYER_CURRENT_MULTIRULE_TIMER_TIME_LIMIT(TRUE) > 0
		AND HAS_LOCAL_PLAYER_MULTIRULE_TIMER_EXPIRED(TRUE)
			// The multi-rule timer has expired! This bar will be full or almost full by this point anyway, but we'll fill it to the top here just to be sure
			fSharedRT_FillBarProgress = 1.0
		
		ELSE
			// The timer isn't currently running. Empty the bar.
			fSharedRT_FillBarProgress = 0.0
			
		ENDIF
	ELSE
		// Make the bar progress by itself
		fSharedRT_FillBarProgress += GET_FRAME_TIME() * cfSharedRT_FillBarSpeed
		
	ENDIF
	
ENDPROC

PROC PROCESS_SHARED_RENDERTARGET_FILL_BAR(SHARED_RT_VARS& sRTVars, FMMC_ENTITY_RENDER_TARGET_OPTIONS& sCreatorOptions)
	IF IS_BIT_SET(sRTVars.iSharedRTBitset, ciSHARED_RT_BITSET__GENERIC_STARTED)
	AND NOT IS_BIT_SET(sRTVars.iSharedRTBitset, ciSHARED_RT_BITSET__GENERIC_COMPLETED)
		
		PROCESS_SHARED_RENDERTARGET_FILL_BAR_PROGRESS(sCreatorOptions)
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sRTVars.siScaleformIndex, sRTVars.sUpdateFillableBarMethodName)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fSharedRT_FillBarProgress)
		END_SCALEFORM_MOVIE_METHOD()
	
		IF fSharedRT_FillBarProgress >= 1.0
			IF bIsLocalPlayerHost
				BROADCAST_FMMC_SHARED_RENDERTARGET_EVENT(ciSHARED_RT_EVENT_TYPE__GENERIC_COMPLETE, sRTVars.iEntityType, sRTVars.iEntityIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(sCreatorOptions.iSharedRT_Bitset, ciSharedRTBitset_ShowBarOnHUD)
			INT iProgressForMeter = ROUND(fSharedRT_FillBarProgress * 100)
			DRAW_GENERIC_METER(iProgressForMeter, 100, sRTVars.sProgressBarTitle, DEFAULT, DEFAULT, HUDORDER_TOP)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SHARED_RENDERTARGET_TIMER_PROGRESS(SHARED_RT_VARS& sRTVars, FMMC_ENTITY_RENDER_TARGET_OPTIONS& sCreatorOptions)
	IF IS_BIT_SET(sCreatorOptions.iSharedRT_Bitset, ciSharedRTBitset_MatchMultiruleTimer)
		IF IS_LOCAL_PLAYER_MULTIRULE_TIMER_RUNNING()
			// Make the timer match the time progressed through the current multi-rule timer
			iSharedRT_TimerTimeToDisplay[sRTVars.iSplitRenderTargetIndex] = GET_LOCAL_PLAYER_MULTIRULE_TIMER_TIME_REMAINING()
			
		ELIF HAS_LOCAL_PLAYER_MULTIRULE_TIMER_EXPIRED()
			iSharedRT_TimerTimeToDisplay[sRTVars.iSplitRenderTargetIndex] = 0
			
		ENDIF
	ELIF IS_BIT_SET(sCreatorOptions.iSharedRT_Bitset, ciSharedRTBitset_MatchRuleTimer)
		IF IS_LOCAL_PLAYER_OBJECTIVE_TIMER_RUNNING()
			// Make the timer match the time progressed through the current rule timer
			iSharedRT_TimerTimeToDisplay[sRTVars.iSplitRenderTargetIndex] = GET_LOCAL_PLAYER_OBJECTIVE_TIMER_TIME_REMAINING()
			
		ELIF HAS_LOCAL_PLAYER_OBJECTIVE_TIMER_EXPIRED()
			iSharedRT_TimerTimeToDisplay[sRTVars.iSplitRenderTargetIndex] = 0
			
		ENDIF
	ELIF sRTVars.iMatchInteriorDestructionTimerIndex > -1
		iSharedRT_TimerTimeToDisplay[sRTVars.iSplitRenderTargetIndex] = GET_INTERIOR_DESTRUCTION_TIME_REMAINING(sRTVars.iMatchInteriorDestructionTimerIndex)
		
	ELSE
		// Make the bar progress by itself
		iSharedRT_TimerTimeToDisplay[sRTVars.iSplitRenderTargetIndex] -= FLOOR(GET_FRAME_TIME() * 1000)
		
	ENDIF
	
ENDPROC

PROC PROCESS_SHARED_RENDERTARGET_TIMER(SHARED_RT_VARS& sRTVars, FMMC_ENTITY_RENDER_TARGET_OPTIONS& sCreatorOptions)
	IF IS_BIT_SET(sRTVars.iSharedRTBitset, ciSHARED_RT_BITSET__GENERIC_STARTED)
	AND NOT IS_BIT_SET(sRTVars.iSharedRTBitset, ciSHARED_RT_BITSET__GENERIC_COMPLETED)
		
		PROCESS_SHARED_RENDERTARGET_TIMER_PROGRESS(sRTVars, sCreatorOptions)
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sRTVars.siScaleformIndex, sRTVars.sUpdateTimerMethodName)
			IF sRTVars.iSplitRenderTargetIndex > -1
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sRTVars.iSplitRenderTargetIndex)
			ENDIF
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSharedRT_TimerTimeToDisplay[sRTVars.iSplitRenderTargetIndex])
		END_SCALEFORM_MOVIE_METHOD()
	
		IF iSharedRT_TimerTimeToDisplay[sRTVars.iSplitRenderTargetIndex] != -1
		AND iSharedRT_TimerTimeToDisplay[sRTVars.iSplitRenderTargetIndex] <= 0
			IF bIsLocalPlayerHost
				BROADCAST_FMMC_SHARED_RENDERTARGET_EVENT(ciSHARED_RT_EVENT_TYPE__GENERIC_COMPLETE, sRTVars.iEntityType, sRTVars.iEntityIndex)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SHARED_RENDERTARGET_OPTIONS(SHARED_RT_VARS& sRTVars)

	FMMC_ENTITY_RENDER_TARGET_OPTIONS sCreatorOptions = GET_ENTITY_RENDER_TARGET_OPTIONS(sRTVars.iEntityType, sRTVars.iEntityIndex)
	
	// Generic "Start"
	IF HAVE_PLAYERS_PROGRESSED_TO_RULE(sCreatorOptions.iSharedRT_GenericStartOnRule, sCreatorOptions.iSharedRT_GenericStartOnRuleTeam)
	AND NOT IS_BIT_SET(sRTVars.iSharedRTBitset, ciSHARED_RT_BITSET__GENERIC_STARTED)
		IF bIsLocalPlayerHost
			BROADCAST_FMMC_SHARED_RENDERTARGET_EVENT(ciSHARED_RT_EVENT_TYPE__GENERIC_START, sRTVars.iEntityType, sRTVars.iEntityIndex)
		ENDIF
		
		SET_BIT(sRTVars.iSharedRTBitset, ciSHARED_RT_BITSET__GENERIC_STARTED)
	ENDIF
	
	IF DOES_SCALEFORM_FEATURE_A_FILLABLE_BAR(sRTVars)
		PROCESS_SHARED_RENDERTARGET_FILL_BAR(sRTVars, sCreatorOptions)
	ENDIF
	
	IF DOES_SCALEFORM_FEATURE_A_TIMER(sRTVars)
		PROCESS_SHARED_RENDERTARGET_TIMER(sRTVars, sCreatorOptions)
	ENDIF
ENDPROC

PROC DRAW_SHARED_RENDERTARGET(SHARED_RT_VARS& sRTVars)
	
	SET_TEXT_RENDER_ID(sRTVars.iSharedRT_ID)
	SET_SCRIPT_GFX_ALIGN(UI_ALIGN_IGNORE, UI_ALIGN_IGNORE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	
	#IF IS_DEBUG_BUILD
	IF HAS_RAG_PLACEMENT_OR_ROTATION_VECTOR_CHANGED()
		sRTVars.fCentreX = vRagPlacementVector.x
		sRTVars.fCentreY = vRagPlacementVector.y
		
		sRTVars.fWidth = vRagRotationVector.x
		sRTVars.fHeight = vRagRotationVector.y
	ENDIF
	#ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sRTVars.sTextureDict)
		DRAW_SPRITE_NAMED_RENDERTARGET(sRTVars.sTextureDict, sRTVars.sTextureName, sRTVars.fCentreX, sRTVars.fCentreY, sRTVars.fWidth, sRTVars.fHeight, 0.0, 255, 255, 255, 255)
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sRTVars.sScaleformName)
		PRINTLN("[SharedRTs_SPAM] DRAW_SHARED_RENDERTARGET | Drawing at ", sRTVars.fCentreX, ", ", sRTVars.fCentreY, " || ", sRTVars.fWidth, ", ", sRTVars.fHeight)
		DRAW_SCALEFORM_MOVIE(sRTVars.siScaleformIndex, sRTVars.fCentreX, sRTVars.fCentreY, sRTVars.fWidth, sRTVars.fHeight, 255, 255, 255, 255)
	ENDIF
	
	RESET_SCRIPT_GFX_ALIGN()
	SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
	
ENDPROC

FUNC BOOL REGISTER_SHARED_RENDERTARGET(SHARED_RT_VARS& sRTVars, INT iSharedRTIndex)
	
	UNUSED_PARAMETER(iSharedRTIndex)
	
	IF IS_NAMED_RENDERTARGET_REGISTERED(sRTVars.sRenderTargetName)
		RETURN TRUE
	ENDIF
	
	IF REGISTER_NAMED_RENDERTARGET(sRTVars.sRenderTargetName)
		PRINTLN("[SharedRTs][RT ", iSharedRTIndex, "] REGISTER_SHARED_RENDERTARGET | Registered ", sRTVars.sRenderTargetName)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL LOAD_SHARED_RENDERTARGET(SHARED_RT_VARS& sRTVars, INT iSharedRTIndex)
	
	UNUSED_PARAMETER(iSharedRTIndex)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sRTVars.sTextureDict)
		REQUEST_STREAMED_TEXTURE_DICT(sRTVars.sTextureDict)
		IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sRTVars.sTextureDict)
			PRINTLN("[SharedRTs][RT ", iSharedRTIndex, "] LOAD_SHARED_RENDERTARGET | Loading texture dict: ", sRTVars.sTextureDict)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sRTVars.sScaleformName)
		sRTVars.siScaleformIndex = REQUEST_SCALEFORM_MOVIE(sRTVars.sScaleformName)	
		IF NOT HAS_SCALEFORM_MOVIE_LOADED(sRTVars.siScaleformIndex)
			PRINTLN("[SharedRTs][RT ", iSharedRTIndex, "] LOAD_SHARED_RENDERTARGET | Loading scaleform movie: ", sRTVars.sScaleformName)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL LINK_SHARED_RENDERTARGET(SHARED_RT_VARS& sRTVars, INT iSharedRTIndex)
	
	UNUSED_PARAMETER(iSharedRTIndex)
	
	IF sRTVars.iSharedRT_ID > -1
		RETURN TRUE
	ENDIF

	IF NOT IS_NAMED_RENDERTARGET_LINKED(sRTVars.mnRenderTargetModel)
		LINK_NAMED_RENDERTARGET(sRTVars.mnRenderTargetModel)
		
		#IF IS_DEBUG_BUILD
		PRINTLN("[SharedRTs][RT ", iSharedRTIndex, "] LINK_SHARED_RENDERTARGET | Linking render target for ", FMMC_GET_MODEL_NAME_FOR_DEBUG(sRTVars.mnRenderTargetModel))
		#ENDIF
	ELSE
		IF sRTVars.iSharedRT_ID = -1
			sRTVars.iSharedRT_ID = GET_NAMED_RENDERTARGET_RENDER_ID(sRTVars.sRenderTargetName)
			PRINTLN("[SharedRTs][RT ", iSharedRTIndex, "] LINK_SHARED_RENDERTARGET | Setting passed-in iRenderTargetID to ", sRTVars.iSharedRT_ID)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC INIT_SHARED_RENDERTARGET_VARS(SHARED_RT_VARS& sRTVars)
	FILL_RENDER_TARGET_VARS_FROM_MODEL_NAME(sRTVars, sRTVars.mnRenderTargetModel)
ENDPROC

FUNC BOOL SHOULD_SHARED_RENDERTARGET_START(SHARED_RT_VARS& sRTVars, INT iSharedRTIndex)
	
	UNUSED_PARAMETER(iSharedRTIndex)

	FMMC_ENTITY_RENDER_TARGET_OPTIONS sCreatorOptions = GET_ENTITY_RENDER_TARGET_OPTIONS(sRTVars.iEntityType, sRTVars.iEntityIndex)
	
	IF sCreatorOptions.iSharedRT_DisplayOnRule > -1
		IF NOT HAVE_PLAYERS_PROGRESSED_TO_RULE(sCreatorOptions.iSharedRT_DisplayOnRule, sCreatorOptions.iSharedRT_DisplayOnRuleTeam)
			PRINTLN("[SharedRTs_SPAM][RT_SPAM ", iSharedRTIndex, "] SHOULD_SHARED_RENDERTARGET_START | Waiting for team ", sCreatorOptions.iSharedRT_DisplayOnRuleTeam, " to reach rule ", sCreatorOptions.iSharedRT_DisplayOnRule)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DRAW_SHARED_RENDERTARGET(INT iSharedRenderTarget, SHARED_RT_VARS& sRTVars)
	
	UNUSED_PARAMETER(iSharedRenderTarget)
	ENTITY_INDEX eiEntity = GET_FMMC_ENTITY(sRTVars.iEntityType, sRTVars.iEntityIndex)
	
	IF DOES_ENTITY_EXIST(eiEntity)
		IF IS_ENTITY_ON_SCREEN(eiEntity)
		OR IS_BIT_SET(sRTVars.iSharedRTBitset, ciSHARED_RT_BITSET__DONT_CARE_IF_ON_SCREEN)
			RETURN TRUE
		ELSE
			PRINTLN("[SharedRTs_SPAM][RT_SPAM ", iSharedRenderTarget, "] SHOULD_DRAW_SHARED_RENDERTARGET | Entity isn't on screen!")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_SHARED_RENDERTARGET(INT iSharedRenderTarget, SHARED_RT_VARS& sRTVars)

	SWITCH sRTVars.iSharedRT_State
		CASE ciSHARED_RT_STATE__OFF
			IF SHOULD_SHARED_RENDERTARGET_START(sRTVars, iSharedRenderTarget)
				SET_SHARED_RENDERTARGET_STATE(sRTVars, iSharedRenderTarget, ciSHARED_RT_STATE__INIT_VARS)
			ENDIF
		BREAK
		
		CASE ciSHARED_RT_STATE__INIT_VARS
			INIT_SHARED_RENDERTARGET_VARS(sRTVars)
			SET_SHARED_RENDERTARGET_STATE(sRTVars, iSharedRenderTarget, ciSHARED_RT_STATE__LOAD)
		BREAK
		
		CASE ciSHARED_RT_STATE__LOAD
			IF LOAD_SHARED_RENDERTARGET(sRTVars, iSharedRenderTarget)
				SET_SHARED_RENDERTARGET_STATE(sRTVars, iSharedRenderTarget, ciSHARED_RT_STATE__REGISTER)
			ENDIF
		BREAK
		
		CASE ciSHARED_RT_STATE__REGISTER
			IF REGISTER_SHARED_RENDERTARGET(sRTVars, iSharedRenderTarget)
				SET_SHARED_RENDERTARGET_STATE(sRTVars, iSharedRenderTarget, ciSHARED_RT_STATE__LINK)
			ENDIF
		BREAK
		
		CASE ciSHARED_RT_STATE__LINK
			IF LINK_SHARED_RENDERTARGET(sRTVars, iSharedRenderTarget)
				PERFORM_SHARED_RENDERTARGET_INIT_FUNCTIONALITY(sRTVars, iSharedRenderTarget)
				SET_SHARED_RENDERTARGET_STATE(sRTVars, iSharedRenderTarget, ciSHARED_RT_STATE__PROCESS)
			ENDIF
		BREAK
		
		CASE ciSHARED_RT_STATE__PROCESS
			PROCESS_SHARED_RENDERTARGET_OPTIONS(sRTVars)
			
			IF SHOULD_DRAW_SHARED_RENDERTARGET(iSharedRenderTarget, sRTVars)
				DRAW_SHARED_RENDERTARGET(sRTVars)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC REGISTER_SHARED_RENDERTARGETS_FOR_LATE_JOINERS()

	PRINTLN("[SharedRTs] REGISTER_SHARED_RENDERTARGETS_FOR_LATE_JOINERS | Registering all render targets for current existing entities!")
	
	INT iProp
	FOR iProp = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps - 1
		IF DOES_MODEL_HAVE_A_SHARED_RENDER_TARGET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn)
		AND DOES_ENTITY_EXIST(GET_FMMC_ENTITY(ciENTITY_TYPE_PROPS, iProp))
			ADD_SHARED_RENDERTARGET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn, ciENTITY_TYPE_PROPS, iProp)
		ENDIF
	ENDFOR
	
	INT iDynoProp
	FOR iDynoProp = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps - 1
		IF DOES_MODEL_HAVE_A_SHARED_RENDER_TARGET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].mn)
		AND DOES_ENTITY_EXIST(GET_FMMC_ENTITY(ciENTITY_TYPE_DYNO_PROPS, iDynoProp))
			ADD_SHARED_RENDERTARGET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].mn, ciENTITY_TYPE_DYNO_PROPS, iDynoProp)
		ENDIF
	ENDFOR
	
	INT iObject
	FOR iObject = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1
		IF DOES_MODEL_HAVE_A_SHARED_RENDER_TARGET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].mn)
		AND DOES_ENTITY_EXIST(GET_FMMC_ENTITY(ciENTITY_TYPE_OBJECT, iObject))
			ADD_SHARED_RENDERTARGET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].mn, ciENTITY_TYPE_OBJECT, iObject)
		ENDIF
	ENDFOR
	
	SET_BIT(iLocalBoolCheck3, LBOOL3_REGISTERED_SHARED_RTS_FOR_LATE_JOINERS)
ENDPROC

PROC PROCESS_ACTIVE_SHARED_RENDERTARGETS()

	IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_LATE_SPECTATE)
	OR bIsSCTV
		IF NOT IS_BIT_SET(iLocalBoolCheck3, LBOOL3_REGISTERED_SHARED_RTS_FOR_LATE_JOINERS)
			REGISTER_SHARED_RENDERTARGETS_FOR_LATE_JOINERS()
		ENDIF
	ENDIF
			
	INT iSharedRenderTarget = 0 
	FOR iSharedRenderTarget = 0 TO iCurrentSharedRTs - 1
		PROCESS_SHARED_RENDERTARGET(iSharedRenderTarget, sSharedRTs[iSharedRenderTarget])
	ENDFOR
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Model Swaps
// ##### Description: Functions related to FMMC Model Swapping functionality.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC CLEANUP_ACTIVE_MODEL_SWAP(INT iActiveModelSwap)

	PRINTLN("[ModelSwaps][ModelSwap ", iActiveModelSwap, "] CLEANUP_ACTIVE_MODEL_SWAP | Cleaning up model swap slot ", iActiveModelSwap)
	INT iOtherModelSwap
	FOR iOtherModelSwap = iActiveModelSwap TO iCurrentActiveModelSwaps - 2
		// Shuffle this one down...
		sActiveModelSwaps[iOtherModelSwap] = sActiveModelSwaps[iOtherModelSwap + 1]
	ENDFOR
	
	// Empty the one on the end
	sActiveModelSwaps[iCurrentActiveModelSwaps - 1].eModelSwapState = MODEL_SWAP_STATE__INVALID
	sActiveModelSwaps[iCurrentActiveModelSwaps - 1].bPerformModelSwapNow = FALSE
	sActiveModelSwaps[iCurrentActiveModelSwaps - 1].sModelSwapConfig.iModelSwapHash = -1
ENDPROC

FUNC BOOL IS_MODEL_SWAP_ENTITY_NETWORKED(FMMC_MODEL_SWAP_ENTITY_DATA& sModelSwapEntity)
	RETURN sModelSwapEntity.eModelSwapType = MODEL_SWAP_ENTITY_TYPE__NETWORKED_OBJECT
ENDFUNC
FUNC BOOL IS_MODEL_SWAP_ENTITY_LOCAL(FMMC_MODEL_SWAP_ENTITY_DATA& sModelSwapEntity)
	RETURN NOT IS_MODEL_SWAP_ENTITY_NETWORKED(sModelSwapEntity)
ENDFUNC

FUNC BOOL WAS_MODEL_SWAP_SUCCESSFUL(OBJECT_INDEX oiSwapObj, BOOL bHide, BOOL bCollisionOnly)

	IF bCollisionOnly
		IF bHide
			RETURN GET_ENTITY_COLLISION_DISABLED(oiSwapObj)
		ELSE
			RETURN NOT GET_ENTITY_COLLISION_DISABLED(oiSwapObj)
		ENDIF
	ENDIF
	
	IF bHide
		RETURN GET_ENTITY_ALPHA(oiSwapObj) < 1
	ELSE
		RETURN GET_ENTITY_ALPHA(oiSwapObj) = 255
	ENDIF
	
ENDFUNC

FUNC BOOL HIDE_OR_UNHIDE_MODEL_SWAP_PROP(INT iModelSwapIndex, OBJECT_INDEX oiSwapObj, FMMC_MODEL_SWAP_ENTITY_DATA& sModelSwapEntity, BOOL bHide, BOOL bCollisionOnly = FALSE)
	
	UNUSED_PARAMETER(iModelSwapIndex)
	
	IF NOT bCollisionOnly
		IF bHide
			PRINTLN("[ModelSwaps][ModelSwap ", iModelSwapIndex, "] HIDE_OR_UNHIDE_MODEL_SWAP_PROP | Hiding ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(oiSwapObj)))
			SET_ENTITY_ALPHA(oiSwapObj, 0, FALSE)
		ELSE
			PRINTLN("[ModelSwaps][ModelSwap ", iModelSwapIndex, "] HIDE_OR_UNHIDE_MODEL_SWAP_PROP | Revealing ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(oiSwapObj)))
			SET_ENTITY_ALPHA(oiSwapObj, 255, FALSE)
		ENDIF
	ENDIF
	
	IF IS_MODEL_SWAP_ENTITY_LOCAL(sModelSwapEntity)
	OR NETWORK_HAS_CONTROL_OF_ENTITY(oiSwapObj)
		
		IF bCollisionOnly
			// Remove decals here - If we've changed the collision of the object, it's very possible that without doing this we'll be leaving behind bullet holes etc floating in the air
			PRINTLN("[ModelSwaps][ModelSwap ", iModelSwapIndex, "] HIDE_OR_UNHIDE_MODEL_SWAP_PROP | Clearing decals on ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(oiSwapObj)))
			
			IF sModelSwapEntity.mnModelSwapPropModelName = INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_Cont_Coll_01a"))
				// Only clear decals on the front of the container - the ones facing towards the player
				REMOVE_DECALS_FROM_OBJECT_FACING(oiSwapObj, -GET_ENTITY_FORWARD_VECTOR(LocalPlayerPed))
			ELSE
				// Default decal removal
				REMOVE_DECALS_FROM_OBJECT(oiSwapObj)
			ENDIF
		ENDIF
		
		IF bHide
			PRINTLN("[ModelSwaps][ModelSwap ", iModelSwapIndex, "] HIDE_OR_UNHIDE_MODEL_SWAP_PROP | Disabling collision on ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(oiSwapObj)))
			SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiSwapObj, FALSE)
		ELSE
			PRINTLN("[ModelSwaps][ModelSwap ", iModelSwapIndex, "] HIDE_OR_UNHIDE_MODEL_SWAP_PROP | Enabling collision on ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(oiSwapObj)))
			SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiSwapObj, TRUE)
		ENDIF
		
		FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiSwapObj)
		
	ELSE
		PRINTLN("[ModelSwaps][ModelSwap ", iModelSwapIndex, "] HIDE_OR_UNHIDE_MODEL_SWAP_PROP | I don't have control of ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(oiSwapObj)))
		
	ENDIF
	
	// Check if the swap was successful. Doing it this way allows remote clients to notice if it was successful as well in cases where we're relying on the network rather than doing it all locally (collision etc)
	IF WAS_MODEL_SWAP_SUCCESSFUL(oiSwapObj, bHide, bCollisionOnly)
		PRINTLN("[ModelSwaps][ModelSwap ", iModelSwapIndex, "] HIDE_OR_UNHIDE_MODEL_SWAP_PROP | Swap was successful this frame! oiSwapObj: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(oiSwapObj)))
		RETURN TRUE
	ELSE
		PRINTLN("[ModelSwaps][ModelSwap ", iModelSwapIndex, "] HIDE_OR_UNHIDE_MODEL_SWAP_PROP | Swap was unsuccessful this frame! oiSwapObj: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(oiSwapObj)))
		RETURN FALSE
	ENDIF
	
ENDFUNC

FUNC OBJECT_INDEX CREATE_MODEL_SWAP_PROP(INT iActiveModelSwap, OBJECT_INDEX oiObjectToClone, FMMC_MODEL_SWAP_ENTITY_DATA& sModelSwapEntity, BOOL bHideOriginalObject = FALSE)

	UNUSED_PARAMETER(iActiveModelSwap) // Only used in Debug
	
	MODEL_NAMES mnModelToUse = sModelSwapEntity.mnModelSwapPropModelName
	PRINTLN("[ModelSwaps][ModelSwap ", iActiveModelSwap, "] CREATE_MODEL_SWAP_PROP | Creating model swap prop ", FMMC_GET_MODEL_NAME_FOR_DEBUG(mnModelToUse))
	OBJECT_INDEX oiNewModelSwapProp = CREATE_OBJECT(mnModelToUse, GET_ENTITY_COORDS(oiObjectToClone), FALSE, FALSE, TRUE)
	
	IF DOES_ENTITY_EXIST(oiNewModelSwapProp)
		FREEZE_ENTITY_POSITION(oiNewModelSwapProp, TRUE)
		SET_ENTITY_DYNAMIC(oiNewModelSwapProp, FALSE)
		SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiNewModelSwapProp, FALSE)
		SET_ENTITY_NO_COLLISION_ENTITY(oiNewModelSwapProp, oiObjectToClone, FALSE)
		SET_ENTITY_ROTATION(oiNewModelSwapProp, GET_ENTITY_ROTATION(oiObjectToClone))
		
		IF IS_ENTITY_ATTACHED(oiObjectToClone)
			ENTITY_INDEX oiOriginalObjectAttachedEntity = GET_ENTITY_ATTACHED_TO(oiObjectToClone)
			
			IF DOES_ENTITY_EXIST(oiOriginalObjectAttachedEntity)
				SET_ENTITY_NO_COLLISION_ENTITY(oiNewModelSwapProp, oiOriginalObjectAttachedEntity, FALSE)
				PRINTLN("[ModelSwaps][ModelSwap ", iActiveModelSwap, "] CREATE_MODEL_SWAP_PROP | Setting model swap prop ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(oiNewModelSwapProp)), " to have no collision with attached entity ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(oiOriginalObjectAttachedEntity)))
			ENDIF
		ENDIF
		
		IF bHideOriginalObject
			// Hide the original networked object so our local clone can stand visibly in its place
			SET_ENTITY_ALPHA(oiObjectToClone, 0, FALSE)
			PRINTLN("[ModelSwaps][ModelSwap ", iActiveModelSwap, "] CREATE_MODEL_SWAP_PROP | Hiding original object!")
		ELSE
			// Hide this new clone ready for when we wanna swap it in
			HIDE_OR_UNHIDE_MODEL_SWAP_PROP(iActiveModelSwap, oiNewModelSwapProp, sModelSwapEntity, TRUE, IS_BIT_SET(sActiveModelSwaps[iActiveModelSwap].sModelSwapConfig.iModelSwapBS, ciMODEL_SWAP_BS__COLLISION_ONLY))
		ENDIF
	ENDIF
	
	RETURN oiNewModelSwapProp
ENDFUNC

FUNC BOOL PREPARE_MODEL_SWAP(INT iActiveModelSwap)
	
	BOOL bReady = TRUE
	
	OBJECT_INDEX oiSwapOut = GET_MODEL_SWAP_ENTITY_TO_SWAP_OUT(iActiveModelSwap, TRUE)
	
	IF NOT DOES_ENTITY_EXIST(oiSwapOut)
		PRINTLN("[ModelSwaps][ModelSwap ", iActiveModelSwap, "] PREPARE_MODEL_SWAP | oiSwapOut doesn't exist!")
		RETURN FALSE
	ENDIF
	
	IF sActiveModelSwaps[iActiveModelSwap].sModelSwapConfig.sEntityToSwapOut.eModelSwapType = MODEL_SWAP_ENTITY_TYPE__MODEL_SWAP_PROP
		// We're creating a local version of this networked object to swap out locally later!
		IF DOES_ENTITY_EXIST(sActiveModelSwaps[iActiveModelSwap].oiModelSwapProp_SwapOut)
			PRINTLN("[ModelSwaps][ModelSwap ", iActiveModelSwap, "] PREPARE_MODEL_SWAP | Model Swap Prop (Local clone to swap out) is ready!")
		ELSE
			sActiveModelSwaps[iActiveModelSwap].oiModelSwapProp_SwapOut = CREATE_MODEL_SWAP_PROP(iActiveModelSwap, oiSwapOut, sActiveModelSwaps[iActiveModelSwap].sModelSwapConfig.sEntityToSwapOut, TRUE)
			bReady = FALSE
		ENDIF
	ENDIF
	
	IF sActiveModelSwaps[iActiveModelSwap].sModelSwapConfig.sEntityToSwapIn.eModelSwapType = MODEL_SWAP_ENTITY_TYPE__MODEL_SWAP_PROP
		IF DOES_ENTITY_EXIST(sActiveModelSwaps[iActiveModelSwap].oiModelSwapProp_SwapIn)
			PRINTLN("[ModelSwaps][ModelSwap ", iActiveModelSwap, "] PREPARE_MODEL_SWAP | Model Swap Prop (Local object to swap in) is ready!")
		ELSE
			sActiveModelSwaps[iActiveModelSwap].oiModelSwapProp_SwapIn = CREATE_MODEL_SWAP_PROP(iActiveModelSwap, oiSwapOut, sActiveModelSwaps[iActiveModelSwap].sModelSwapConfig.sEntityToSwapIn)
			bReady = FALSE
		ENDIF
	ENDIF
	
	RETURN bReady
ENDFUNC

FUNC BOOL PERFORM_MODEL_SWAP(INT iActiveModelSwap)
	
	OBJECT_INDEX oiSwapOut
	OBJECT_INDEX oiSwapIn
	BOOL bLocal = FALSE
	IF !sActiveModelSwaps[iActiveModelSwap].bReverse
		oiSwapOut = GET_MODEL_SWAP_ENTITY_TO_SWAP_OUT(iActiveModelSwap)
		oiSwapIn = GET_MODEL_SWAP_ENTITY_TO_SWAP_IN(iActiveModelSwap)
		bLocal = IS_MODEL_SWAP_ENTITY_LOCAL(sActiveModelSwaps[iActiveModelSwap].sModelSwapConfig.sEntityToSwapIn)
	ELSE
		oiSwapIn = GET_MODEL_SWAP_ENTITY_TO_SWAP_OUT(iActiveModelSwap)
		oiSwapOut = GET_MODEL_SWAP_ENTITY_TO_SWAP_IN(iActiveModelSwap)
		bLocal = IS_MODEL_SWAP_ENTITY_LOCAL(sActiveModelSwaps[iActiveModelSwap].sModelSwapConfig.sEntityToSwapOut)
	ENDIF
	
	// When swapping the second object in
	IF sActiveModelSwaps[iActiveModelSwap].eModelSwapState = MODEL_SWAP_STATE__SWAPPING_REVEAL
		
		PRINTLN("[ModelSwaps][ModelSwap ", iActiveModelSwap, "] PERFORM_MODEL_SWAP | Swapping in the new model")
		
		IF DOES_ENTITY_EXIST(oiSwapIn)
			IF bLocal 
			OR NETWORK_HAS_CONTROL_OF_ENTITY(oiSwapIn)
				IF IS_ENTITY_ATTACHED(oiSwapIn)
					PRINTLN("[ModelSwaps][ModelSwap ", iActiveModelSwap, "] PERFORM_MODEL_SWAP | Detaching oiSwapIn!")
					DETACH_ENTITY(oiSwapIn)
				ENDIF
				
				SET_ENTITY_COORDS_NO_OFFSET(oiSwapIn, GET_ENTITY_COORDS(oiSwapOut, FALSE))
				SET_ENTITY_ROTATION(oiSwapIn, GET_ENTITY_ROTATION(oiSwapOut), DEFAULT, FALSE)
				
				IF IS_BIT_SET(sActiveModelSwaps[iActiveModelSwap].sModelSwapConfig.iModelSwapBS, ciMODEL_SWAP_BS__COLLISION_ONLY)
					PRINTLN("[ModelSwaps][ModelSwap ", iActiveModelSwap, "] PERFORM_MODEL_SWAP | Re-attaching oiSwapIn!")
					ATTACH_ENTITY_TO_ENTITY(oiSwapIn, oiSwapOut, 0, <<0,0,0>>, <<0,0,0>>, DEFAULT, DEFAULT, TRUE)
				ENDIF
			ENDIF
				
			IF !sActiveModelSwaps[iActiveModelSwap].bReverse
				IF HIDE_OR_UNHIDE_MODEL_SWAP_PROP(iActiveModelSwap, oiSwapIn, sActiveModelSwaps[iActiveModelSwap].sModelSwapConfig.sEntityToSwapIn, FALSE, IS_BIT_SET(sActiveModelSwaps[iActiveModelSwap].sModelSwapConfig.iModelSwapBS, ciMODEL_SWAP_BS__COLLISION_ONLY))
					// Swapped in the new object successfully!
				ELSE
					RETURN FALSE
				ENDIF
			ELSE
				IF HIDE_OR_UNHIDE_MODEL_SWAP_PROP(iActiveModelSwap, oiSwapIn, sActiveModelSwaps[iActiveModelSwap].sModelSwapConfig.sEntityToSwapOut, FALSE, IS_BIT_SET(sActiveModelSwaps[iActiveModelSwap].sModelSwapConfig.iModelSwapBS, ciMODEL_SWAP_BS__COLLISION_ONLY))
					// Swapped in the new object successfully!
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[ModelSwaps][ModelSwap ", iActiveModelSwap, "] PERFORM_MODEL_SWAP | oiSwapIn doesn't exist!")
			RETURN FALSE
			
		ENDIF
		
	ELIF sActiveModelSwaps[iActiveModelSwap].eModelSwapState = MODEL_SWAP_STATE__SWAPPING_HIDE
	
		PRINTLN("[ModelSwaps][ModelSwap ", iActiveModelSwap, "] PERFORM_MODEL_SWAP | Swapping out the original model!")
		
		// When swapping the original object out
		IF DOES_ENTITY_EXIST(oiSwapOut)
			
			IF !sActiveModelSwaps[iActiveModelSwap].bReverse
				IF HIDE_OR_UNHIDE_MODEL_SWAP_PROP(iActiveModelSwap, oiSwapOut, sActiveModelSwaps[iActiveModelSwap].sModelSwapConfig.sEntityToSwapOut, TRUE, IS_BIT_SET(sActiveModelSwaps[iActiveModelSwap].sModelSwapConfig.iModelSwapBS, ciMODEL_SWAP_BS__COLLISION_ONLY))
					// Swapped out the original object successfully!
				ELSE
					RETURN FALSE
				ENDIF
			ELSE
				IF HIDE_OR_UNHIDE_MODEL_SWAP_PROP(iActiveModelSwap, oiSwapOut, sActiveModelSwaps[iActiveModelSwap].sModelSwapConfig.sEntityToSwapIn, TRUE, IS_BIT_SET(sActiveModelSwaps[iActiveModelSwap].sModelSwapConfig.iModelSwapBS, ciMODEL_SWAP_BS__COLLISION_ONLY))
					// Swapped out the original object successfully!
					sActiveModelSwaps[iActiveModelSwap].bReverse = FALSE
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[ModelSwaps][ModelSwap ", iActiveModelSwap, "] PERFORM_MODEL_SWAP | oiSwapOut doesn't exist!")
			RETURN FALSE
			
		ENDIF
		
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC STRING GET_MODEL_SWAP_STATE_STRING(MODEL_SWAP_STATE eState)
	SWITCH eState
		CASE MODEL_SWAP_STATE__INVALID
			RETURN "MODEL_SWAP_STATE__INVALID"
		BREAK
		
		CASE MODEL_SWAP_STATE__PREPARING
			RETURN "MODEL_SWAP_STATE__PREPARING"
		BREAK
		
		CASE MODEL_SWAP_STATE__READY
			RETURN "MODEL_SWAP_STATE__READY"
		BREAK
		
		CASE MODEL_SWAP_STATE__SWAPPING_REVEAL
			RETURN "MODEL_SWAP_STATE__SWAPPING_REVEAL"
		BREAK
		
		CASE MODEL_SWAP_STATE__SWAPPING_HIDE
			RETURN "MODEL_SWAP_STATE__SWAPPING_HIDE"
		BREAK
		
		CASE MODEL_SWAP_STATE__COMPLETE
			RETURN "MODEL_SWAP_STATE__COMPLETE"
		BREAK
	ENDSWITCH
	
	RETURN "INVALID STATE"
ENDFUNC

PROC SET_ACTIVE_MODEL_SWAP_STATE(INT iActiveModelSwap, MODEL_SWAP_STATE eNewState)
	sActiveModelSwaps[iActiveModelSwap].eModelSwapState = eNewState
	PRINTLN("[ModelSwaps][ModelSwap ", iActiveModelSwap, "] SET_ACTIVE_MODEL_SWAP_STATE | Setting state to ", GET_MODEL_SWAP_STATE_STRING(eNewState))
ENDPROC

PROC PROCESS_ACTIVE_MODEL_SWAP(INT iActiveModelSwap)

	SWITCH sActiveModelSwaps[iActiveModelSwap].eModelSwapState
		CASE MODEL_SWAP_STATE__PREPARING
			IF PREPARE_MODEL_SWAP(iActiveModelSwap)
				SET_ACTIVE_MODEL_SWAP_STATE(iActiveModelSwap, MODEL_SWAP_STATE__READY)
			ENDIF
		BREAK
		
		CASE MODEL_SWAP_STATE__READY
			IF sActiveModelSwaps[iActiveModelSwap].bPerformModelSwapNow
				SET_ACTIVE_MODEL_SWAP_STATE(iActiveModelSwap, MODEL_SWAP_STATE__SWAPPING_REVEAL)
				PROCESS_ACTIVE_MODEL_SWAP(iActiveModelSwap)
			ENDIF
		BREAK
		
		CASE MODEL_SWAP_STATE__SWAPPING_REVEAL
			IF PERFORM_MODEL_SWAP(iActiveModelSwap)
				SET_ACTIVE_MODEL_SWAP_STATE(iActiveModelSwap, MODEL_SWAP_STATE__SWAPPING_HIDE)
				PROCESS_ACTIVE_MODEL_SWAP(iActiveModelSwap)
			ENDIF
		BREAK
		
		CASE MODEL_SWAP_STATE__SWAPPING_HIDE
			IF PERFORM_MODEL_SWAP(iActiveModelSwap)
				SET_ACTIVE_MODEL_SWAP_STATE(iActiveModelSwap, MODEL_SWAP_STATE__COMPLETE)
			ENDIF
		BREAK
		
		CASE MODEL_SWAP_STATE__COMPLETE
			IF sActiveModelSwaps[iActiveModelSwap].bReverse
				SET_ACTIVE_MODEL_SWAP_STATE(iActiveModelSwap, MODEL_SWAP_STATE__READY)
				PROCESS_ACTIVE_MODEL_SWAP(iActiveModelSwap)
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_ACTIVE_MODEL_SWAPS()
	INT iActiveModelSwap
	FOR iActiveModelSwap = 0 TO iCurrentActiveModelSwaps - 1
		PROCESS_ACTIVE_MODEL_SWAP(iActiveModelSwap)
	ENDFOR
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Tagging
// ##### Description: Entity tagging functionality that applies to multiple entity types at once
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL DOES_ENTITY_HAVE_TAGGING_ENABLED(ENTITY_TYPE etEntityType, ENTITY_INDEX HitEntity, BOOL bCheckIfAlreadyTagged)
		IF DOES_ENTITY_EXIST(HitEntity)
		SWITCH etEntityType
			CASE ET_PED
				PED_INDEX piHitEntity
				piHitEntity = GET_PED_INDEX_FROM_ENTITY_INDEX(hitEntity)
				IF DOES_ENTITY_EXIST(piHitEntity)
					INT iPed
					FOR iPed = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
							IF piHitEntity = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetNine, ciPed_BSNine_CanBeTagged)
									IF (NOT bCheckIfAlreadyTagged) OR (bCheckIfAlreadyTagged AND NOT IS_ENTITY_ALREADY_TAGGED(ENUM_TO_INT(etEntityType), iPed))
										
										RETURN TRUE
										BREAKLOOP
									ENDIF
								ELSE
									IF IS_PED_IN_ANY_VEHICLE(piHitEntity)
										IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(piHitEntity))
											VEHICLE_INDEX tempVeh
											tempVeh = GET_VEHICLE_PED_IS_IN(piHitEntity)
											INT iVeh
											FOR iVeh = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1
												IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
													IF tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
														IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_CAN_BE_TAGGED)
														AND ((NOT bCheckIfAlreadyTagged)
															OR (bCheckIfAlreadyTagged AND NOT IS_ENTITY_ALREADY_TAGGED(ENUM_TO_INT(GET_ENTITY_TYPE(tempVeh)), iVeh)))
															RETURN TRUE
															BREAKLOOP
														ENDIF
													ENDIF
												ENDIF
											ENDFOR
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			BREAK
			CASE ET_VEHICLE
				VEHICLE_INDEX viHitEntity
				viHitEntity = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(hitEntity)
				IF DOES_ENTITY_EXIST(viHitEntity)
					INT iVeh
					FOR iVeh = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
							IF viHitEntity = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_CAN_BE_TAGGED)
								AND ((NOT bCheckIfAlreadyTagged)
									OR (bCheckIfAlreadyTagged AND NOT IS_ENTITY_ALREADY_TAGGED(ENUM_TO_INT(etEntityType), iVeh)))
									RETURN TRUE
									BREAKLOOP
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			BREAK
			CASE ET_OBJECT
				OBJECT_INDEX oiHitEntity
				oiHitEntity = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(hitEntity)
				IF DOES_ENTITY_EXIST(oiHitEntity)
					INT iProp, iObj
					BOOL bFound
					FOR iProp = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1
						IF DOES_ENTITY_EXIST(oiProps[iProp])
							IF oiHitEntity = oiProps[iProp]
								bFound = TRUE
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset2, ciFMMC_PROP2_CAN_BE_TAGGED)
								AND ((NOT bCheckIfAlreadyTagged)
									OR (bCheckIfAlreadyTagged AND NOT IS_ENTITY_ALREADY_TAGGED(ENUM_TO_INT(etEntityType), iProp)))
									RETURN TRUE
									BREAKLOOP
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
					IF NOT bFound
						FOR iObj = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects-1
							IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iObj))
								IF oiHitEntity = NET_TO_OBJ(GET_OBJECT_NET_ID(iObj))
									IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_CanBeTagged)
									AND ((NOT bCheckIfAlreadyTagged)
									OR (bCheckIfAlreadyTagged AND NOT IS_ENTITY_ALREADY_TAGGED(4, iObj)))
										RETURN TRUE
										BREAKLOOP
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF

	RETURN FALSE
ENDFUNC

PROC PROCESS_BLIP_TAGGED_ENTITY(ENTITY_INDEX HitEntity, INT iEntityIndexToUse, INT iHitEntityType, INT iGangChaseUnit = -1)
	INT iIndex
	FOR iIndex = 0 TO FMMC_MAX_TAGGED_ENTITIES-1
		PRINTLN("PROCESS_BLIP_TAGGED_ENTITY - Checking ", iIndex)
		IF NOT IS_BIT_SET(MC_serverBD_3.iTaggedEntityBitset, iIndex)
		AND NOT IS_ENTITY_ALREADY_TAGGED(iHitEntityType, iEntityIndexToUse)
			PRINTLN("PROCESS_BLIP_TAGGED_ENTITY - bit not set")
			IF DOES_ENTITY_EXIST(HitEntity)
				PRINTLN("PROCESS_BLIP_TAGGED_ENTITY - Exists")
				PRINTLN("PROCESS_BLIP_TAGGED_ENTITY - iEntityIndexToUse: ", iEntityIndexToUse, " iHitEntityType: ",iHitEntityType)
				BROADCAST_FMMC_TAGGED_ENTITY(iIndex, iHitEntityType, iEntityIndexToUse, iGangChaseUnit)
				PLAY_SOUND_FRONTEND(-1, "tag_entity", "dlc_xm_heists_iaa_morgue_sounds", false)
				BREAKLOOP
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_TAG_ENTITY_PLACED_PEDS(PED_INDEX piHitEntity, ENTITY_INDEX HitEntity)
	
	INT iPed
	FOR iPed = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			IF piHitEntity = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetNine, ciPed_BSNine_CanBeTagged)
					PRINTLN("[RCC MISSION] PROCESS_TAG_ENTITY - Hit Placed PED ", iPed, " tagging now")
					PROCESS_BLIP_TAGGED_ENTITY(HitEntity, iPed, ENUM_TO_INT(GET_ENTITY_TYPE(HitEntity)))
									
					BREAKLOOP
				ELSE
					PRINTLN("[RCC MISSION][TAGGING] PROCESS_TAG_ENTITY - Hit PED ", iPed, " can't tag as not set up for it.")
					IF IS_PED_IN_ANY_VEHICLE(piHitEntity)
						PRINTLN("[RCC MISSION][TAGGING] PROCESS_TAG_ENTITY - Checking the vehicle ped ", iPed," is in.")
						IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(piHitEntity))
							VEHICLE_INDEX tempVeh 
							tempVeh = GET_VEHICLE_PED_IS_IN(piHitEntity)
						
							INT iVeh
							FOR iVeh = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1
								IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
									IF tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_CAN_BE_TAGGED)
											PRINTLN("[RCC MISSION][TAGGING] PROCESS_TAG_ENTITY - Hit VEHICLE ", iVeh, " tagging now")
											PROCESS_BLIP_TAGGED_ENTITY(tempVeh, iVeh, ENUM_TO_INT(GET_ENTITY_TYPE(tempVeh)))
											BREAKLOOP
										ELSE
											PRINTLN("[RCC MISSION][TAGGING] PROCESS_TAG_ENTITY - Hit VEHICLE ", iVeh, " can't tag as not set up for it.")
										ENDIF
									ENDIF
								ENDIF
							ENDFOR
						
						ENDIF
					ENDIF

				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC PROCESS_TAG_ENTITY_GANG_CHASE_PEDS(PED_INDEX piHitEntity, ENTITY_INDEX HitEntity)
	INT iGangChaseUnit, iPed
	FOR iGangChaseUnit = 0 TO ciMAX_GANG_CHASE_VEHICLES - 1
		FOR iPed = 0 TO ciMAX_GANG_CHASE_PEDS_PER_VEHICLE - 1
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
				RELOOP
			ENDIF
			
			IF NOT (piHitEntity = NET_TO_PED(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed]))
				RELOOP
			ENDIF
			
			PRINTLN("[RCC MISSION] PROCESS_TAG_ENTITY_GANG_CHASE_PEDS - Hit Gang Chase PED ", iPed, " tagging now")
			PROCESS_BLIP_TAGGED_ENTITY(HitEntity, iPed, ENUM_TO_INT(GET_ENTITY_TYPE(HitEntity)), iGangChaseUnit)
									
			BREAKLOOP
			
		ENDFOR
	ENDFOR
	
	
ENDPROC

PROC PROCESS_TAG_ENTITY(ENTITY_TYPE etEntityType, ENTITY_INDEX HitEntity)
	PRINTLN("[RCC MISSION][TAGGING] PROCESS_TAG_ENTITY - Calling PROCESS_TAG_ENTITY")
	IF DOES_ENTITY_EXIST(HitEntity)
		SWITCH etEntityType
			CASE ET_PED
				PRINTLN("[RCC MISSION][TAGGING] PROCESS_TAG_ENTITY - Processing a PED")
				PED_INDEX piHitEntity
				piHitEntity = GET_PED_INDEX_FROM_ENTITY_INDEX(hitEntity)
				IF DOES_ENTITY_EXIST(piHitEntity)
					PROCESS_TAG_ENTITY_PLACED_PEDS(piHitEntity, HitEntity)
					PROCESS_TAG_ENTITY_GANG_CHASE_PEDS(piHitEntity, HitEntity)
				ENDIF
			BREAK
			CASE ET_VEHICLE
				PRINTLN("[RCC MISSION][TAGGING] PROCESS_TAG_ENTITY - Processing a VEHICLE")
				VEHICLE_INDEX viHitEntity
				viHitEntity = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(hitEntity)
				IF DOES_ENTITY_EXIST(viHitEntity)
					INT iVeh
					FOR iVeh = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
							IF viHitEntity = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_CAN_BE_TAGGED)
									PRINTLN("[RCC MISSION][TAGGING] PROCESS_TAG_ENTITY - Hit VEHICLE ", iVeh, " tagging now")
									PROCESS_BLIP_TAGGED_ENTITY(HitEntity, iVeh, ENUM_TO_INT(GET_ENTITY_TYPE(HitEntity)))
									BREAKLOOP
								ELSE
									PRINTLN("[RCC MISSION][TAGGING] PROCESS_TAG_ENTITY - Hit VEHICLE ", iVeh, " can't tag as not set up for it.")
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			BREAK
			CASE ET_OBJECT
				PRINTLN("[RCC MISSION][TAGGING] PROCESS_TAG_ENTITY - Processing a PROP/OBJECT")
				OBJECT_INDEX oiHitEntity
				oiHitEntity = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(hitEntity)
				INT iTempEntityType
				IF DOES_ENTITY_EXIST(oiHitEntity)
					INT iProp, iObj
					BOOL bFound
					FOR iProp = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1
						IF DOES_ENTITY_EXIST(oiProps[iProp])
							IF oiHitEntity = oiProps[iProp]
								bFound = TRUE
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset2, ciFMMC_PROP2_CAN_BE_TAGGED)
									PRINTLN("[RCC MISSION][TAGGING] PROCESS_TAG_ENTITY - Hit PROP ", iProp, " tagging now")
									iTempEntityType = 3
									PROCESS_BLIP_TAGGED_ENTITY(HitEntity, iProp, iTempEntityType)
									BREAKLOOP
								ELSE
									PRINTLN("[RCC MISSION][TAGGING] PROCESS_TAG_ENTITY - Hit PROP ", iProp, " can't tag as not set up for it.")
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
					IF NOT bFound
						FOR iObj = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects-1
							IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iObj))
								IF oiHitEntity = NET_TO_OBJ(GET_OBJECT_NET_ID(iObj))
									IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_CanBeTagged)
										PRINTLN("[RCC MISSION][TAGGING] PROCESS_TAG_ENTITY - Hit OBJECT ", iObj, " tagging now")
										iTempEntityType = 4
										PROCESS_BLIP_TAGGED_ENTITY(HitEntity, iObj, iTempEntityType)
										BREAKLOOP
									ELSE
										PRINTLN("[RCC MISSION][TAGGING] PROCESS_TAG_ENTITY - Hit OBJECT ", iObj, " can't tag as not set up for it.")
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC PROCESS_ENTITY_MARKER_TAGGING()

	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule > -1
	AND iRule < FMMC_MAX_RULES
	
		
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_FORCE_CLEAN_UP_ALL_TAGS)
			PRINTLN("PROCESS_ENTITY_MARKER_TAGGING - FORCE CLEANING UP ALL TAGS THIS FRAME. Rule: ", iRule)
			IF bIsLocalPlayerHost
				MC_serverBD_3.iTaggedEntityBitset = 0
				PRINTLN("PROCESS_ENTITY_MARKER_TAGGING - Resetting MC_serverBD_3.iTaggedEntityBitset")
			ENDIF
			
			INT iTaggedEntities
			FOR iTaggedEntities = 0 TO FMMC_MAX_TAGGED_ENTITIES - 1
				IF bIsLocalPlayerHost
					MC_serverBD_3.iTaggedEntityType[iTaggedEntities] = 0
					MC_serverBD_3.iTaggedEntityIndex[iTaggedEntities] = -1
					MC_serverBD_3.iTaggedEntityGangChaseUnit[iTaggedEntities] = -1
				ENDIF
				
				IF DOES_BLIP_EXIST(biTaggedEntity[iTaggedEntities])
					REMOVE_BLIP(biTaggedEntity[iTaggedEntities])
					PRINTLN("PROCESS_ENTITY_MARKER_TAGGING - Removing Tag - ", iTaggedEntities)
				ENDIF
			ENDFOR
			
		ENDIF
		
		IF IS_TARGET_TAGGING_ENABLED_THIS_RULE(iTeam, iRule)
			
			// get help text
			STRING sHelpText = GET_HELP_TEXT_FOR_TARGET_TAGGING()
			
			IF (IS_PLAYER_FREE_AIMING(LocalPlayer) OR IS_PLAYER_TARGETTING_ANYTHING(LocalPlayer))
				
				//Results
				SHAPETEST_STATUS stStatusTagTest
				VECTOR vPos, vNormal
				//Probe
				VECTOR vStart, vEnd
				FLOAT fLineDistance
				
				vStart = GET_FINAL_RENDERED_CAM_COORD()
				
				fLineDistance = 500
				vEnd = GET_VECTOR_INFRONT_OF_CURRENT_RENDERING_CAM(vStart, fLineDistance)
				
				stStatusTagTest = GET_SHAPE_TEST_RESULT(stiTagShapeTest, iTagShapeHitSomething, vPos, vNormal, TagTestEntity)
				IF stStatusTagTest = SHAPETEST_STATUS_RESULTS_READY
					
					IF DOES_ENTITY_EXIST(TagTestEntity)
					AND iTagShapeHitSomething = 1
						PRINTLN("[RCC MISSION] PROCESS_ENTITY_MARKER_TAGGING - Shape test results are ready")
					ENDIF

				ELIF stStatusTagTest = SHAPETEST_STATUS_NONEXISTENT
					IF iTagShapeHitSomething != 1
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelpText)
							HIDE_HELP_TEXT_THIS_FRAME()
						ENDIF
					ENDIF
					
					INT iLOSFlags = SCRIPT_INCLUDE_VEHICLE + SCRIPT_INCLUDE_PED + SCRIPT_INCLUDE_OBJECT
					stiTagShapeTest = START_SHAPE_TEST_LOS_PROBE(vStart, vEnd, iLOSFlags, DEFAULT, SCRIPT_SHAPETEST_OPTION_IGNORE_NO_COLLISION | SCRIPT_SHAPETEST_OPTION_IGNORE_SEE_THROUGH | SCRIPT_SHAPETEST_OPTION_IGNORE_GLASS)		
				ENDIF
				
				IF DOES_ENTITY_EXIST(TagTestEntity)
				AND iTagShapeHitSomething = 1
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
					
					IF DOES_ENTITY_HAVE_TAGGING_ENABLED(GET_ENTITY_TYPE(TagTestEntity), TagTestEntity, TRUE)
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelpText)
							PRINT_HELP(sHelpText, -1)
						ENDIF
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelpText)
							HIDE_HELP_TEXT_THIS_FRAME()
						ENDIF
					ENDIF										
					
					IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, GET_CONTROL_FOR_TARGET_TAGGING())					
						PRINTLN("[RCC MISSION][TAGGING] PROCESS_ENTITY_MARKER_TAGGING - Button Pressed, attempting to tag")
						PRINTLN("[RCC MISSION][TAGGING] PROCESS_ENTITY_MARKER_TAGGING - aimEntity type: ", ENUM_TO_INT(GET_ENTITY_TYPE(TagTestEntity)))
						PROCESS_TAG_ENTITY(GET_ENTITY_TYPE(TagTestEntity), TagTestEntity)
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelpText)
						HIDE_HELP_TEXT_THIS_FRAME()
					ENDIF
				ENDIF
			ELSE
				IF iTagShapeHitSomething > 0
					iTagShapeHitSomething = 0
				ENDIF
				IF TagTestEntity != NULL
					TagTestEntity = NULL
				ENDIF
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelpText)
					HIDE_HELP_TEXT_THIS_FRAME()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Every Frame Entity Processing
// ##### Description: Runs the every frame loops and processing for each entity type.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_ENTITIES_PRE_EVERY_FRAME()

	INT iTeam
	
	// reset local player player carry countF
	iMyPedCarryCount = 0
	
	iInAnyLocTemp = -1
	
	iAHighPriorityVehicle = -1
	iTempCaptureLocation = -1
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		iTempPriorityLocation[iTeam] = -1
		
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
			iFirstHighPriorityVehicleThisRule[iTeam] = -1
		ENDIF
	ENDFOR
	
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_FINISHED)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_START_SPECTATOR)
		MC_playerBD[iLocalPart].iObjectiveTypeCompleted = -1
	ENDIF
	
	IF MC_playerBD[iLocalPart].iObjectiveTypeCompleted = -1
		MC_playerBD[iLocalPart].iCurrentLoc = -1
		MC_playerBD[iLocalPart].iPedNear = -1
		MC_playerBD[iLocalPart].iVehNear = -1
		MC_playerBD[iLocalPart].iObjNear = -1
		PRINTLN("[RCC MISSION] EVERY_FRAME_PRE_ENTITY_PROCESSING - MC_playerBD[iLocalPart].iObjectiveTypeCompleted = -1, resetting iCurrentLoc, iPedNear, iVehNear, iObjNear")
	ENDIF
	
	// reset local player vehicle carry count
	iMyVehicleCarryCount = 0
	
	// reset local player object carry count
	iMyObjectCarryCount = 0
	
	iLowestLeaveLocationPriority = FMMC_PRIORITY_IGNORE
	
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_IN_MY_PRIORITY_LEAVE_LOC)
		SET_BIT(iLocalBoolCheck9, LBOOL9_TEMP_WAS_IN_LOCATION_THIS_FRAME)
	ELSE
		CLEAR_BIT(iLocalBoolCheck9, LBOOL9_TEMP_WAS_IN_LOCATION_THIS_FRAME)
	ENDIF
	
	CLEAR_BIT(iLocalBoolCheck6,LBOOL6_IN_MY_PRIORITY_LEAVE_LOC)
	CLEAR_BIT(iLocalBoolCheck12, LBOOL12_SERVER_STILL_THINKS_IM_HACKING_TEMP)
	
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
		iVehTargetingCheckedThisRuleBS = 0 //Recheck all vehicle targeting!
		CLEAR_BIT(iLocalBoolCheck6, LBOOL6_DISABLECONTROLTIMER_COMPLETE)
		RESET_NET_TIMER(tdDisableControlTimer)
	ENDIF
	
	CLEAR_BIT(iLocalBoolCheck13, LBOOL13_DRAW_DISTANCE_TO_CROSS_LINE)
	CLEAR_BIT(iLocalBoolCheck13, LBOOL13_PLAYERINPEDBLOCKINGZONE)
	
	fTeamMembers_ReqDist = 0.0
	
	IF bIsLocalPlayerHost

		FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
			MC_serverBD.iNumVehSeatsHighestPriority[iTeam] = 0
		ENDFOR
		
		IF iLocalGOFoundryHostageSynchSceneID != -1
		AND MC_serverBD_1.iGOFoundryHostageSynchSceneID != iLocalGOFoundryHostageSynchSceneID
			MC_serverBD_1.iGOFoundryHostageSynchSceneID = iLocalGOFoundryHostageSynchSceneID
			PRINTLN("[RCC MISSION][HostageSync] EVERY_FRAME_PRE_ENTITY_PROCESSING - Set host iGOFoundryHostageSynchSceneID as ",iLocalGOFoundryHostageSynchSceneID)
		ENDIF
		
	ENDIF	
	
	INT iPedLong
	FOR iPedLong = 0 TO FMMC_MAX_PEDS_BITSET-1
		iPedShouldProcessIdleAnim_CheckedThisFrame[iPedLong] = 0
		iPedShouldProcessSpecialAnim_CheckedThisFrame[iPedLong] = 0
		iPedShouldProcessIdleAnim_ResultThisFrame[iPedLong] = 0
		iPedShouldProcessSpecialAnim_ResultThisFrame[iPedLong] = 0
	ENDFOR
	
	PROCESS_ENTITY_CHECKPOINT_CONTINUITY_PRE_EVERY_FRAME_CLIENT()
	
ENDPROC

PROC PROCESS_ENTITIES_POST_EVERY_FRAME()
	
	IF IS_BIT_SET(iLocalBoolCheck12, LBOOL12_SERVER_STILL_THINKS_IM_HACKING_TEMP)
		IF NOT IS_BIT_SET(iLocalBoolCheck12, LBOOL12_SERVER_STILL_THINKS_IM_HACKING)
			PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - Set bit LBOOL12_SERVER_STILL_THINKS_IM_HACKING")
			SET_BIT(iLocalBoolCheck12, LBOOL12_SERVER_STILL_THINKS_IM_HACKING)
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck12, LBOOL12_SERVER_STILL_THINKS_IM_HACKING)
			PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - Clear bit LBOOL12_SERVER_STILL_THINKS_IM_HACKING")
			CLEAR_BIT(iLocalBoolCheck12, LBOOL12_SERVER_STILL_THINKS_IM_HACKING)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_IN_MY_PRIORITY_LEAVE_LOC) != IS_BIT_SET(iLocalBoolCheck9, LBOOL9_TEMP_WAS_IN_LOCATION_THIS_FRAME)
		REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_LOCATION, FALSE #IF IS_DEBUG_BUILD , "PROCESS_ENTITIES_POST_EVERY_FRAME - in/out of location changed" #ENDIF )
	ENDIF
	
	IF MC_serverBD.iPolice > 1
		IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_PLAYERINPEDBLOCKINGZONE)
			IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_PLAYERINPEDBLOCKINGZONE_NEWMAXWANTEDSET)
				PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - Entering a ped's wanted blocking zone, call SET_MAX_WANTED_LEVEL to my current wanted level of ",GET_PLAYER_WANTED_LEVEL(LocalPlayer))
				SET_MAX_WANTED_LEVEL(GET_PLAYER_WANTED_LEVEL(LocalPlayer))
				SET_BIT(iLocalBoolCheck13, LBOOL13_PLAYERINPEDBLOCKINGZONE_NEWMAXWANTEDSET)
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_PLAYERINPEDBLOCKINGZONE_NEWMAXWANTEDSET)
				IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE) OR IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_AND_CLEAR_WANTED_LEVEL_ZONE)
					
					INT iMaxWanted = 5
					
					IF GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice) > 0
						iMaxWanted = GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice)
					ENDIF
					
					PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - Leaving a ped's wanted blocking zone, call SET_MAX_WANTED_LEVEL back to mission max of ",iMaxWanted)
					SET_MAX_WANTED_LEVEL(iMaxWanted)
				ENDIF
				
				CLEAR_BIT(iLocalBoolCheck13, LBOOL13_PLAYERINPEDBLOCKINGZONE_NEWMAXWANTEDSET)
			ENDIF
		ENDIF
	ENDIF
	
	IF iSpectatorTarget = -1
		
		MC_playerBD[iPartToUse].iInAnyLoc = iInAnyLocTemp
		
		IF MC_playerBD[iPartToUse].iPedCarryCount != iMyPedCarryCount
			MC_playerBD[iPartToUse].iPedCarryCount = iMyPedCarryCount
			PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MC_playerBD[iPartToUse].iPedCarryCount updated to ",MC_playerBD[iPartToUse].iPedCarryCount,", iPartToUse ",iPartToUse)
		ENDIF
		
		IF MC_playerBD[iPartToUse].iVehDeliveryId = -1
			IF MC_playerBD[iPartToUse].iVehCarryCount != iMyVehicleCarryCount
				MC_playerBD[iPartToUse].iVehCarryCount = iMyVehicleCarryCount
				PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MC_playerBD[iPartToUse].iVehCarryCount updated to ",MC_playerBD[iPartToUse].iVehCarryCount,", iPartToUse ",iPartToUse)
			ENDIF
		ENDIF
		
		IF MC_playerBD[iPartToUse].iObjCarryCount != iMyObjectCarryCount
			MC_playerBD[iPartToUse].iObjCarryCount = iMyObjectCarryCount
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
			AND MC_playerBD[iPartToUse].iObjCarryCount = 0
			AND (shouldCleanupObjectTransformationNow != NULL AND CALL shouldCleanupObjectTransformationNow())
				IF cleanupObjectTransformations != NULL
					CALL cleanupObjectTransformations()
				ENDIF
			ENDIF
			IF MC_playerBD[iPartToUse].iObjCarryCount = 0
				CLEANUP_OBJECT_INVENTORIES()
			ENDIF
			PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MC_playerBD[iPartToUse].iObjCarryCount updated to ",MC_playerBD[iPartToUse].iObjCarryCount,", iPartToUse ",iPartToUse)
		ENDIF
		
	ELIF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset, PBBOOL_HEIST_SPECTATOR)
		MC_playerBD[iLocalPart].iInAnyLoc = iInAnyLocTemp
	ENDIF
	
	IF bIsLocalPlayerHost
		INT iTeam
		
		FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			IF MC_serverBD.iPriorityLocation[iTeam] != iTempPriorityLocation[iTeam]
				MC_serverBD.iPriorityLocation[iTeam] = iTempPriorityLocation[iTeam]
				PRINTLN("[RCC MISSION] MC_serverBD.iPriorityLocation[iteam]    updated to: ",MC_serverBD.iPriorityLocation[iTeam] ," for team: ",iTeam)
			ENDIF
		ENDFOR
	ENDIF
	
	IF i_Pri_Sta_IG2_AudioStreamPedID != -1
		BOOL bKillStream
		IF NETWORK_DOES_NETWORK_ID_EXIST( MC_serverBD_1.sFMMC_SBD.niPed[i_Pri_Sta_IG2_AudioStreamPedID] )
			
			PED_INDEX pedTemp = NET_TO_PED( MC_serverBD_1.sFMMC_SBD.niPed[i_Pri_Sta_IG2_AudioStreamPedID] )
			IF NOT IS_PED_INJURED( pedTemp )
		
				IF NOT IS_BIT_SET( iLocalBoolCheck11, LBOOL12_STATION_DRUNK_PED_STREAM_STARTED )
				 
					IF LOAD_STREAM( "CUTSCENES_MPH_PRI_STA_IG2_SYNC_MASTERED_ONLY" )
						
						IF IS_ENTITY_PLAYING_ANIM( pedTemp, "anim@heists@prison_heiststation@cop_reactions", "drunk_talk", ANIM_SCRIPT )
							PLAY_STREAM_FROM_PED( pedTemp )	
							SET_BIT( iLocalBoolCheck11, LBOOL12_STATION_DRUNK_PED_STREAM_STARTED )
							PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MPH_PRI_STA_IG2 - Played stream from ped = ", i_Pri_Sta_IG2_AudioStreamPedID, " stream = CUTSCENES_MPH_PRI_STA_IG2_SYNC_MASTERED_ONLY" )
						ENDIF
						
					ELSE
						PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MPH_PRI_STA_IG2 - Waiting for audio stream PRI_STA_IG2_AudioStream(for drunk lady ped) to load" )
					ENDIF
					
				ELSE
					
					// Ped not playing either of the drunk anims, stop the audio stream (they must have been interrupted)
					IF GET_SCRIPT_TASK_STATUS( pedTemp, SCRIPT_TASK_PERFORM_SEQUENCE ) != PERFORMING_TASK
					OR ( NOT IS_ENTITY_PLAYING_ANIM( pedTemp, "anim@heists@prison_heiststation@cop_reactions", "drunk_talk", ANIM_SCRIPT )
					AND NOT IS_ENTITY_PLAYING_ANIM( pedTemp, "anim@heists@prison_heiststation@cop_reactions", "drunk_idle", ANIM_SCRIPT ) )
						PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MPH_PRI_STA_IG2 - drunk_idle and drunk_talk are not playing, stop the stream " )
						bKillStream = TRUE
					ENDIF
					
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MPH_PRI_STA_IG2  ped injured/dead, stop stream" )
				bKillStream = TRUE
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MPH_PRI_STA_IG2 - ped does not exist, stop stream" )
			bKillStream = TRUE
		ENDIF
		
		IF bKillStream
		OR ( IS_BIT_SET( iLocalBoolCheck11, LBOOL12_STATION_DRUNK_PED_STREAM_STARTED ) AND NOT IS_STREAM_PLAYING() )
		
			PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MPH_PRI_STA_IG2 - bKillStream = ", BOOL_TO_STRING(bKillStream), " LBOOL12_STATION_DRUNK_PED_STREAM_STARTED = ", BOOL_TO_STRING( IS_BIT_SET( iLocalBoolCheck11, LBOOL12_STATION_DRUNK_PED_STREAM_STARTED ) ), " IS_STREAM_PLAYING() = ", BOOL_TO_STRING(IS_STREAM_PLAYING()), ", cleaning up stream" )
		
			IF IS_STREAM_PLAYING()
				STOP_STREAM()
				PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MPH_PRI_STA_IG2 - STOP_STREAM() called" )
			ELSE
				PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MPH_PRI_STA_IG2 - STOP_STREAM() not called, wasn't playing a stream." )
			ENDIF
			
			CLEAR_BIT( iLocalBoolCheck11, LBOOL12_STATION_DRUNK_PED_STREAM_STARTED )
			i_Pri_Sta_IG2_AudioStreamPedID = -1
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
		
		IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
		AND NOT HAS_TEAM_FAILED(MC_playerBD[iLocalPart].iteam)
		AND GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
						
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitsetThree[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]],ciBS_RULE3_DISABLE_SHOPS)
				IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_ALL_SHOPS_LOCKED)
					SET_ALL_SHOP_LOCATES_ARE_BLOCKED(TRUE, TRUE)
					SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
					//Done individually so the mod shops can be unblocked when required
					HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_CARMOD, TRUE)
					HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_CLOTHES, TRUE)
					HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_GUN, TRUE)
					HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_HAIRDO, TRUE)
					HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_PERSONAL_CARMOD, TRUE)
					HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_TATTOO, TRUE)
					UPDATE_ALL_SHOP_BLIPS()
					SET_BIT(iLocalBoolCheck10, LBOOL10_ALL_SHOPS_LOCKED)
					PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - SET_ALL_SHOP_LOCATES_ARE_BLOCKED updated to TRUE ")
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_ALL_SHOPS_LOCKED)
					SET_ALL_SHOP_LOCATES_ARE_BLOCKED(FALSE, TRUE)
					SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
					HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_CARMOD, FALSE)
					HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_CLOTHES, TRUE)
					HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_GUN, FALSE)
					HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_HAIRDO, FALSE)
					HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_PERSONAL_CARMOD, FALSE)
					HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_TATTOO, FALSE)
					UPDATE_ALL_SHOP_BLIPS()
					PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - SET_ALL_SHOP_LOCATES_ARE_BLOCKED updated to FALSE ")
					CLEAR_BIT(iLocalBoolCheck10, LBOOL10_ALL_SHOPS_LOCKED)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitset[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]], ciBS_RULE_BLOCK_MECHANIC_DELIVERY)
				IF NOT IS_MECHANIC_DELIVERY_BLOCKED()
					BLOCK_MECHANIC_DELIVERY(TRUE)
				ENDIF
			ELSE
				IF IS_MECHANIC_DELIVERY_BLOCKED()
					BLOCK_MECHANIC_DELIVERY(FALSE)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitset[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]],ciBS_RULE_ENABLE_SPECTATE)
				IF IS_HEIST_SPECTATE_DISABLED()
					DISABLE_HEIST_SPECTATE(FALSE)
				ENDIF
			ELSE
				IF NOT IS_HEIST_SPECTATE_DISABLED()
					PRINTLN("PIM - HEIST SPECTATE - DISABLE_HEIST_SPECTATE - A")
					DISABLE_HEIST_SPECTATE(TRUE)
				ENDIF
				IF USING_HEIST_SPECTATE()
					PRINTLN("PIM - HEIST SPECTATE - CLEANUP_HEIST_SPECTATE - A")
					CLEANUP_HEIST_SPECTATE()
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_HEIST_SPECTATE_DISABLED()
				PRINTLN("PIM - HEIST SPECTATE - DISABLE_HEIST_SPECTATE - B")
				DISABLE_HEIST_SPECTATE(TRUE)
			ENDIF
			IF USING_HEIST_SPECTATE()
				PRINTLN("PIM - HEIST SPECTATE - CLEANUP_HEIST_SPECTATE - B")
				CLEANUP_HEIST_SPECTATE()
			ENDIF
		ENDIF
		
		CLEAR_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF)
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_MINIGAME_HELP_TEXT_SHOWING_THIS_FRAME)
		CLEAR_BIT(iLocalBoolCheck32, LBOOL32_MINIGAME_HELP_TEXT_SHOWING_THIS_FRAME)
	ENDIF

	RESET_SPOOK_AT_COORD_IN_RADIUS()
	
	IF HAS_NET_TIMER_STARTED(tdPropColourChangeDelayTimer)
	AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdPropColourChangeDelayTimer, (g_FMMC_STRUCT.iPropColourChangeDelay * 1000))
		PRINTLN("[PROPS] PROCESS_ENTITIES_POST_EVERY_FRAME - Resetting Net Timer for Prop Colour Change Delay.")
		RESET_NET_TIMER(tdPropColourChangeDelayTimer)
	ENDIF
	
	PROCESS_ENTITY_CHECKPOINT_CONTINUITY_EVERY_FRAME_CLIENT()
	
	IF GET_FRAME_COUNT() % 2 = 0
		iCCTVStaggeredDialogueTriggerIndex++
		
		IF iCCTVStaggeredDialogueTriggerIndex >= g_FMMC_STRUCT.iNumberOfDialogueTriggers
			iCCTVStaggeredDialogueTriggerIndex = 0
		ENDIF
	ENDIF

ENDPROC
 
PROC PROCESS_ENTITIES_EVERY_FRAME()
	
	PROCESS_ENTITIES_PRE_EVERY_FRAME()
	
	INT i
	
	//LOCATIONS
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_LOCATIONS)
	#ENDIF
	IF SHOULD_PROCESS_LOCATIONS_THIS_FRAME()
		PROCESS_LOCATES_PRE_EVERY_FRAME()
		FOR i = 0 TO MC_serverBD.iNumLocCreated - 1
			PROCESS_LOCATION_EVERY_FRAME_CLIENT(i)
			PROCESS_LOCATION_EVERY_FRAME_SERVER(i)
		ENDFOR
		PROCESS_LOCATES_POST_EVERY_FRAME()
	ENDIF
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_LOCATIONS)
	#ENDIF
	
	//PEDS
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_PEDS)
	#ENDIF
	PROCESS_PED_PRE_EVERY_FRAME()
	FOR i = 0 TO MC_serverBD.iNumPedCreated - 1
		PROCESS_PED_EVERY_FRAME_CLIENT(i)
		PROCESS_PED_EVERY_FRAME_SERVER(i)		
		IF NOT FMMC_IS_LONG_BIT_SET(iFlaggedForHighPriorityProcessingNextFrameBS, i)
			FMMC_CLEAR_LONG_BIT(iFlaggedForHighPriorityProcessingBS, i)
		ENDIF
	ENDFOR
		
	PROCESS_PED_POST_EVERY_FRAME()
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_PEDS)
	#ENDIF
	
	//VEHICLES
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_VEHICLES)
	#ENDIF
	PROCESS_VEH_PRE_EVERY_FRAME()
	FOR i = 0 TO MC_serverBD.iNumVehCreated - 1
		PROCESS_VEH_EVERY_FRAME_CLIENT(i)
		PROCESS_VEH_EVERY_FRAME_SERVER(i)
	ENDFOR
	PROCESS_VEH_POST_EVERY_FRAME()
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_VEHICLES)
	#ENDIF
	
	//TRAINS
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_TRAINS)
	#ENDIF
	PROCESS_TRAIN_PRE_EVERY_FRAME()
	FOR i = 0 TO MC_serverBD.iNumTrainCreated - 1
		PROCESS_TRAIN_EVERY_FRAME_CLIENT(i)
		PROCESS_TRAIN_EVERY_FRAME_SERVER(i)
	ENDFOR
	PROCESS_TRAIN_POST_EVERY_FRAME()
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_TRAINS)
	#ENDIF
	
	//INTERACTABLES
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_INTERACTABLES)
	#ENDIF
	PROCESS_INTERACTABLE_PRE_EVERY_FRAME_CLIENT()
	
	PROCESS_INTERACTABLE_APPROACH_EVERY_FRAME_CLIENT(iClosestInteractable)
	PROCESS_ONGOING_INTERACTABLE_INTERACTIONS_EVERY_FRAME_CLIENT()
	
	IF SHOULD_PROCESS_ONE_FRAME_INTERACTABLE_LOOP_THIS_FRAME()
		FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables - 1
			PROCESS_INTERACTABLE_ONE_FRAME_LOOP_CLIENT(i)
			PROCESS_INTERACTABLE_ONE_FRAME_LOOP_SERVER(i)
		ENDFOR
	ENDIF
	
	PROCESS_INTERACTABLE_POST_EVERY_FRAME_CLIENT()
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_INTERACTABLES)
	#ENDIF
	
	//OBJECTS
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_OBJECTS)
	#ENDIF
	PROCESS_OBJ_PRE_EVERY_FRAME()
	FOR i = 0 TO MC_serverBD.iNumObjCreated - 1
		PROCESS_OBJ_EVERY_FRAME_CLIENT(i)
		PROCESS_OBJ_EVERY_FRAME_SERVER(i)
	ENDFOR
	PROCESS_OBJ_POST_EVERY_FRAME()
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_OBJECTS)
	#ENDIF
	
	//PROPS
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_PROPS)
	#ENDIF
	FOR i = 0 TO iNumEveryFrameProps - 1
		PROCESS_PROP_EVERY_FRAME_CLIENT(iEveryFramePropIndices[i])
	ENDFOR
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_PROPS)
	#ENDIF
	
	//DYNOPROPS
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_DYNOPROPS)
	#ENDIF
	PROCESS_DYNOPROPS_PRE_EVERY_FRAME()
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps - 1
		PROCESS_DYNOPROP_EVERY_FRAME_CLIENT(i)
		PROCESS_DYNOPROP_EVERY_FRAME_SERVER(i)
	ENDFOR
	PROCESS_DYNOPROPS_POST_EVERY_FRAME()
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_DYNOPROPS)
	#ENDIF
	
	//ZONES
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_ZONES)
	#ENDIF
	PROCESS_ZONE_PRE_EVERY_FRAME()
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfZones - 1
		PROCESS_ZONE_EVERY_FRAME_CLIENT(i)
		PROCESS_ZONE_EVERY_FRAME_SERVER(i)
	ENDFOR
	PROCESS_ZONE_POST_EVERY_FRAME()
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_ZONES)
	#ENDIF
	
	//DOORS
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_DOORS)
	#ENDIF
	PROCESS_DOOR_PRE_EVERY_FRAME()
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDoors - 1
		PROCESS_DOOR_EVERY_FRAME_CLIENT(i)
		PROCESS_DOOR_EVERY_FRAME_SERVER(i)
	ENDFOR
	PROCESS_DOOR_POST_EVERY_FRAME()
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_DOORS)
	#ENDIF
	
	//BOUNDS
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_BOUNDS)
	#ENDIF
	PROCESS_FMMC_RULE_BOUNDS_PRE_EVERY_FRAME()
	FOR i = 0 TO ciMAX_RULE_BOUNDS_PER_RULE - 1
		PROCESS_FMMC_RULE_BOUNDS_EVERY_FRAME(i, g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].sRuleBounds[GET_LOCAL_PLAYER_CURRENT_RULE()][i])
	ENDFOR
	PROCESS_FMMC_RULE_BOUNDS_POST_EVERY_FRAME()
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_BOUNDS)
	#ENDIF
	
	//DUMMY BLIPS
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_DUMMYBLIPS)
	#ENDIF
	PROCESS_DUMMY_BLIP_PRE_EVERY_FRAME()
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDummyBlips-1
		PROCESS_EVERY_FRAME_DUMMY_BLIPS(i)
	ENDFOR
	PROCESS_DUMMY_BLIP_POST_EVERY_FRAME()
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_DUMMYBLIPS)
	#ENDIF
	
	//PICKUPS
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_PICKUPS)
	#ENDIF
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons-1
		PROCESS_EVERY_FRAME_PICKUPS(i)
	ENDFOR
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_PICKUPS)
	#ENDIF
	
	PROCESS_ENTITIES_POST_EVERY_FRAME()
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Staggered Entity Processing
// ##### Description: Runs the staggered loops for each entity type.
// ##### Each entity type has it's own staggered loop. This will process a set number of entities each frame.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

TYPEDEF PROC PROCESS_ENTITY_STAGGERED_CLIENT(INT iEntity)
TYPEDEF PROC PROCESS_ENTITY_STAGGERED_SERVER(INT iEntity)

PROC PROCESS_ENTITY_STAGGERED_LOOP(INT &iIterator, INT iMaxEntities, 
								PROCESS_ENTITY_STAGGERED_CLIENT clientProcessing,
								PROCESS_ENTITY_STAGGERED_SERVER serverProcessing,
								INT iIterationsPerFrame = 1) 
	
	IF iMaxEntities <= 0
		EXIT
	ENDIF
	
	INT i
	FOR i = 0 TO iIterationsPerFrame - 1
	
		IF clientProcessing != NULL
			CALL clientProcessing(iIterator)
		ENDIF
		
		IF serverProcessing != NULL
			CALL serverProcessing(iIterator)
		ENDIF
		
		iIterator++
		
		IF iIterator >= iMaxEntities
			iIterator = 0
			BREAKLOOP
		ENDIF
		
	ENDFOR
	
ENDPROC

TYPEDEF PROC PROCESS_PRE_ENTITY_STAGGERED()
TYPEDEF PROC PROCESS_POST_ENTITY_STAGGERED()

PROC PROCESS_ENTITY_STAGGERED_LOOP_WITH_PRE_POST_PROCESSING(INT &iIterator, INT iMaxEntities, 
								PROCESS_ENTITY_STAGGERED_CLIENT clientProcessing,
								PROCESS_ENTITY_STAGGERED_SERVER serverProcessing,
								PROCESS_PRE_ENTITY_STAGGERED preProcessing,
								PROCESS_POST_ENTITY_STAGGERED postProcessing,
								INT iIterationsPerFrame = 1)
	
	IF iMaxEntities <= 0
		EXIT
	ENDIF
	
	IF iIterator = 0
		IF preProcessing != NULL
			CALL preProcessing()
		ENDIF
	ENDIF
	
	PROCESS_ENTITY_STAGGERED_LOOP(iIterator, iMaxEntities, clientProcessing, serverProcessing, iIterationsPerFrame)
	
	IF iIterator = 0
		IF postProcessing != NULL
			CALL postProcessing()
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_ENTITIES_STAGGERED()
	
	PROCESS_ENTITY_STAGGERED_SERVER nullServerProcessing
	PROCESS_POST_ENTITY_STAGGERED nullPostProcessing
	
	//LOCATIONS
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_LOCATIONS)
	#ENDIF
	PROCESS_ENTITY_STAGGERED_LOOP_WITH_PRE_POST_PROCESSING(iStaggeredLocationIterator, MC_serverBD.iNumLocCreated,
				&PROCESS_LOCATION_STAGGERED_CLIENT, &PROCESS_LOCATION_STAGGERED_SERVER,
				&PROCESS_LOCATION_PRE_STAGGERED_SERVER, &PROCESS_LOCATION_POST_STAGGERED_SERVER)
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_LOCATIONS)
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_PEDS)
	#ENDIF
	//PEDS
	PROCESS_ENTITY_STAGGERED_LOOP(iStaggeredPedIterator, MC_serverBD.iNumPedCreated,
				&PROCESS_PED_STAGGERED_CLIENT, &PROCESS_PED_STAGGERED_SERVER)
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_PEDS)
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_VEHICLES)
	#ENDIF
	//VEHICLES
	PROCESS_ENTITY_STAGGERED_LOOP_WITH_PRE_POST_PROCESSING(iStaggeredVehicleIterator, MC_serverBD.iNumVehCreated,
				&PROCESS_VEH_STAGGERED_CLIENT, &PROCESS_VEH_STAGGERED_SERVER,
				&PROCESS_VEH_PRE_STAGGERED_SERVER, &PROCESS_VEH_POST_STAGGERED_SERVER)
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_VEHICLES)
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_TRAINS)
	#ENDIF
	//TRAINS
	PROCESS_ENTITY_STAGGERED_LOOP(iStaggeredTrainIterator, MC_serverBD.iNumTrainCreated,
				&PROCESS_TRAIN_STAGGERED_CLIENT, &PROCESS_TRAIN_STAGGERED_SERVER)
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_TRAINS)
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_INTERACTABLES)
	#ENDIF
	//INTERACTABLES
	PROCESS_ENTITY_STAGGERED_LOOP_WITH_PRE_POST_PROCESSING(iStaggeredInteractableIterator, g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables,
				&PROCESS_INTERACTABLE_STAGGERED_CLIENT, &PROCESS_INTERACTABLE_STAGGERED_SERVER,
				&PROCESS_INTERACTABLE_PRE_STAGGERED_CLIENT, &PROCESS_INTERACTABLE_POST_STAGGERED_CLIENT, ciINTERACTABLES_PER_FRAME)
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_INTERACTABLES)
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_OBJECTS)
	#ENDIF
	//OBJECTS
	PROCESS_ENTITY_STAGGERED_LOOP_WITH_PRE_POST_PROCESSING(iStaggeredObjectIterator, MC_serverBD.iNumObjCreated,
				&PROCESS_OBJ_STAGGERED_CLIENT, &PROCESS_OBJ_STAGGERED_SERVER,
				&PROCESS_OBJ_PRE_STAGGERED, &PROCESS_OBJ_POST_STAGGERED)
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_OBJECTS)
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_PROPS)
	#ENDIF
	//PROPS
	PROCESS_ENTITY_STAGGERED_LOOP_WITH_PRE_POST_PROCESSING(iStaggeredPropIterator, g_FMMC_STRUCT_ENTITIES.iNumberOfProps,
				&PROCESS_PROP_STAGGERED_CLIENT, nullServerProcessing,
				&PROCESS_PROP_PRE_STAGGERED_CLIENT, nullPostProcessing,
				ciPROPS_PER_FRAME)
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_PROPS)
	#ENDIF
	
	//DYNOPROPS
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_DYNOPROPS)
	#ENDIF
	PROCESS_ENTITY_STAGGERED_LOOP(iStaggeredDynoPropIterator, g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps,
				&PROCESS_DYNOPROP_STAGGERED_CLIENT, nullServerProcessing)
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_DYNOPROPS)
	#ENDIF	
	
	//ZONES
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_ZONES)
	#ENDIF
	PROCESS_ENTITY_STAGGERED_LOOP_WITH_PRE_POST_PROCESSING(iStaggeredZoneIterator, g_FMMC_STRUCT_ENTITIES.iNumberOfZones,
				&PROCESS_ZONE_STAGGERED_CLIENT, &PROCESS_ZONE_STAGGERED_SERVER, 
				&PROCESS_ZONE_PRE_STAGGERED_CLIENT, &PROCESS_ZONE_POST_STAGGERED_CLIENT, 
				PICK_INT(IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_FULL_ZONES_AND_BOUNDS_LOOP_REQUIRED), g_FMMC_STRUCT_ENTITIES.iNumberOfZones, ciZONES_PER_FRAME))
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_ZONES)
	#ENDIF
	
	//BOUNDS
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_BOUNDS)
	#ENDIF
	PROCESS_ENTITY_STAGGERED_LOOP_WITH_PRE_POST_PROCESSING(iStaggeredBoundsIterator, ciMAX_RULE_BOUNDS_PER_RULE,
				&PROCESS_BOUNDS_STAGGERED_CLIENT, nullServerProcessing, 
				&PROCESS_BOUNDS_PRE_STAGGERED_CLIENT, &PROCESS_BOUNDS_POST_STAGGERED_CLIENT,
				PICK_INT(IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_FULL_ZONES_AND_BOUNDS_LOOP_REQUIRED), ciMAX_RULE_BOUNDS_PER_RULE, ciBOUNDS_PER_FRAME))
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_BOUNDS)
	#ENDIF
	
	//DOORS
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_DOORS)
	#ENDIF
	PROCESS_ENTITY_STAGGERED_LOOP(iStaggeredDoorIterator, g_FMMC_STRUCT_ENTITIES.iNumberOfDoors,
				&PROCESS_DOOR_STAGGERED_CLIENT, &PROCESS_DOOR_STAGGERED_SERVER, ciDOORS_PER_FRAME)
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_DOORS)
	#ENDIF
	
	//PICKUPS
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_PICKUPS)
	#ENDIF
	PROCESS_ENTITY_STAGGERED_LOOP(iStaggeredWeaponIterator, g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons,
				&PROCESS_PICKUPS_STAGGERED_CLIENT, nullServerProcessing)
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_PICKUPS)
	#ENDIF
	
	PROCESS_ENTITY_STAGGERED_LOOP(iStaggeredPedAmmoDrop, ciFMMC_MAX_PED_AMMO_DROPS,
				&PROCESS_PED_DROPPED_AMMO, nullServerProcessing)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Objective Entity Processing
// ##### Description: Functions that run the processing of entities for rule progression.
// ##### These are run in a staggered loop based on the entity type with the most placed entities.
// ##### Each entity of every type below is processed once per loop.
// ##### Peds, Vehicles, Objects, Locates and Players
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_SERVER_PRE_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM(INT iTeam)
	
	PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_PRE_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - Initializing all temp variables for team: ", iTeam)
	
	iCurrentHighPriority[iTeam] = FMMC_PRIORITY_IGNORE
	iOldHighPriority[iTeam] = FMMC_PRIORITY_IGNORE

	iNumHighPriorityLoc[iTeam] = 0
	itempLocMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
	
	iNumHighPriorityPlayerRule[iTeam] = 0
	itempPlayerRuleMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
	itempCurrentPlayerRule[iTeam] = -1
	
	iNumHighPriorityPed[iTeam] = 0
	iNumHighPriorityDeadPed[iTeam] = 0
	
	iTempNumPriorityRespawnPed[iTeam] = 0
	itempPedMissionLogic[iTeam] =FMMC_OBJECTIVE_LOGIC_NONE
	
	iNumHighPriorityVeh[iTeam] = 0
	iTempNumPriorityRespawnVeh[iTeam] = 0
	itempvehMissionLogic[iTeam] =FMMC_OBJECTIVE_LOGIC_NONE
	
	iNumHighPriorityObj[iTeam] = 0
	iTempNumPriorityRespawnObj[iTeam] = 0
	itempObjMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
	itempObjMissionSubLogic[iTeam] = ci_HACK_SUBLOGIC_NONE
	
//	iNumHighPriorityTrain[iTeam] = 0
//	itempTrainMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
		
ENDPROC

PROC PROCESS_PRE_OBJECTIVE_ENTITY_STAGGERED_LOOP()
	
	PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_PRE_OBJECTIVE_ENTITY_STAGGERED_LOOP - Staggered Loop Started")
	
	iPlacedVehicleIAmIn = -1
	iNearestTargetTemp = -1
	iNearestTargetTypeTemp = ci_TARGET_NONE
	fNearestTargetDist2Temp = MAX_FLOAT
		
	IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_OBJCOMP_KILLCHASEHINTCAM_1)
		CLEAR_BIT(iLocalBoolCheck5, LBOOL5_OBJCOMP_KILLCHASEHINTCAM_1)
		SET_BIT(iLocalBoolCheck5, LBOOL5_OBJCOMP_KILLCHASEHINTCAM_2)
	ELSE
		CLEAR_BIT(iLocalBoolCheck5, LBOOL5_OBJCOMP_KILLCHASEHINTCAM_2)
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_REQUEST_UPDATE)
		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_PRE_OBJECTIVE_ENTITY_STAGGERED_LOOP - Starting Requested Updated")
		SET_BIT(iLocalBoolCheck2,LBOOL2_STARTED_UPDATE)
	ENDIF
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	iTempEntitiesInZoneBS = 0
	
	INT iTeam
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		PROCESS_SERVER_PRE_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM(iTeam)
	ENDFOR
	
ENDPROC

PROC PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM(INT iTeam)
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] != iCurrentHighPriority[iTeam] 
		RESET_NET_TIMER(MC_serverBD_3.tdObjectiveLimitTimer[iTeam])
		MC_serverBD_3.iTimerPenalty[iTeam] = 0
		MC_serverBD_4.iCurrentHighestPriority[iTeam] = iCurrentHighPriority[iTeam]
		MC_serverBD.iScoreOnThisRule[iTeam] = 0
		MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam] = 0
		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD_4.iCurrentHighestPriority[iTeam] updated to: ",MC_serverBD_4.iCurrentHighestPriority[iTeam]," for team: ",iTeam) 
		
		IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
			MC_serverBD.iRequiredDeliveries[iTeam] = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRequiredDeliveries[MC_serverBD_4.iCurrentHighestPriority[iTeam]]
			PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD.iRequiredDeliveries[iTeam] updated to: ",MC_serverBD.iRequiredDeliveries[iTeam]," for team: ",iTeam) 
		ENDIF
		
		SET_BIT(iLocalBoolCheck7, LBOOL7_HOST_APPLIED_NEW_PRIORITY_MIDFRAME)
	ENDIF
	
	IF MC_serverBD.iNumLocHighestPriority[iTeam] != iNumHighPriorityLoc[iTeam]
		MC_serverBD.iNumLocHighestPriority[iTeam] = iNumHighPriorityLoc[iTeam]
		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD.iNumLocHighestPriority[iTeam] updated to: ",MC_serverBD.iNumLocHighestPriority[iTeam]," for team: ",iTeam) 
		IF MC_serverBD.iNumLocHighestPriority[iTeam] != 0 
		AND MC_serverBD.iNumLocHighestPriority[iTeam] > MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam]
			MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam] = MC_serverBD.iNumLocHighestPriority[iTeam]
			PRINTLN("[OBJECTIVE_ENTITIES] iMaxNumPriorityEntitiesThisRule (locations): ", MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam], " for team: ",iTeam) 
		ENDIF
	ENDIF
	
	IF MC_serverBD_4.iLocMissionLogic[iTeam] != iTempLocMissionLogic[iTeam]
		MC_serverBD_4.iLocMissionLogic[iTeam] = iTempLocMissionLogic[iTeam]
		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD_4.iLocMissionLogic[iTeam] updated to: ",MC_serverBD_4.iLocMissionLogic[iTeam]," for team: ",iTeam)
	ENDIF
	
	IF MC_serverBD.iNumPlayerRuleHighestPriority[iTeam] != iNumHighPriorityPlayerRule[iTeam]
		MC_serverBD.iNumPlayerRuleHighestPriority[iTeam] = iNumHighPriorityPlayerRule[iTeam]
		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD.iNumPlayerRuleHighestPriority[iTeam] updated to: ",MC_serverBD.iNumPlayerRuleHighestPriority[iTeam]," for team: ",iTeam)
	ENDIF
	
	IF MC_serverBD_4.iPlayerRuleMissionLogic[iTeam] != iTempPlayerRuleMissionLogic[iTeam]
		MC_serverBD_4.iPlayerRuleMissionLogic[iTeam] = iTempPlayerRuleMissionLogic[iTeam]
		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD_4.iPlayerRuleMissionLogic[iTeam] updated to: ",MC_serverBD_4.iPlayerRuleMissionLogic[iTeam]," for team: ",iTeam) 
	ENDIF
	
	IF MC_serverBD.iCurrentPlayerRule[iTeam] != iTempCurrentPlayerRule[iTeam]
		MC_serverBD.iCurrentPlayerRule[iTeam] = iTempCurrentPlayerRule[iTeam]
		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD.iCurrentPlayerRule[iTeam] updated to: ",MC_serverBD.iCurrentPlayerRule[iTeam]," for team: ",iTeam) 
	ENDIF
	
	IF MC_serverBD.iNumPedHighestPriority[iTeam] != iNumHighPriorityPed[iTeam] 
		MC_serverBD.iNumPedHighestPriority[iTeam] = iNumHighPriorityPed[iTeam] 
		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD.iNumPedHighestPriority[iTeam]  updated to: ",MC_serverBD.iNumPedHighestPriority[iTeam]," for team: ",iTeam)
		
		IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
		AND MC_serverBD.iNumPedHighestPriority[iTeam] > 0
			MC_serverBD.iRequiredDeliveries[iTeam] = IMIN(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRequiredDeliveries[MC_serverBD_4.iCurrentHighestPriority[iTeam]],MC_serverBD.iNumPedHighestPriority[iTeam])
			PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD.iRequiredDeliveries[iTeam] updated to: ",MC_serverBD.iRequiredDeliveries[iTeam]," for team: ",iTeam) 
		ENDIF
		
		IF MC_serverBD.iNumPedHighestPriority[iTeam] != 0 
		AND MC_serverBD.iNumPedHighestPriority[iTeam] > MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam]
			MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam] = MC_serverBD.iNumPedHighestPriority[iTeam]
			PRINTLN("[OBJECTIVE_ENTITIES] iMaxNumPriorityEntitiesThisRule (peds): ", MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam], " for team: ",iTeam) 
		ENDIF
	ENDIF
	
	IF MC_serverBD.iNumDeadPedHighestPriority[iTeam]  !=  iNumHighPriorityDeadPed[iTeam] 
		MC_serverBD.iNumDeadPedHighestPriority[iTeam]  =  iNumHighPriorityDeadPed[iTeam] 
		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD.iNumDeadPedHighestPriority[iTeam]  updated to: ",MC_serverBD.iNumDeadPedHighestPriority[iTeam]," for team: ",iTeam) 
	ENDIF
	
	IF MC_serverBD_4.iPedMissionLogic[iTeam] != iTempPedMissionLogic[iTeam]
		MC_serverBD_4.iPedMissionLogic[iTeam] = iTempPedMissionLogic[iTeam]
		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD_4.iPedMissionLogic[iTeam] updated to: ",MC_serverBD_4.iPedMissionLogic[iTeam]," for team: ",iTeam) 
	ENDIF
	
	IF MC_serverBD.iNumVehHighestPriority[iTeam] != iNumHighPriorityVeh[iTeam] 
		MC_serverBD.iNumVehHighestPriority[iTeam] = iNumHighPriorityVeh[iTeam]
		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD.iNumVehHighestPriority[iTeam]   updated to: ",MC_serverBD.iNumVehHighestPriority[iTeam]," for team: ",iTeam)
		
		IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
		AND MC_serverBD.iNumVehHighestPriority[iTeam] > 0
			MC_serverBD.iRequiredDeliveries[iTeam] = IMIN(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRequiredDeliveries[MC_serverBD_4.iCurrentHighestPriority[iTeam]],MC_serverBD.iNumVehHighestPriority[iTeam])
			PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD.iRequiredDeliveries[iTeam] updated to: ",MC_serverBD.iRequiredDeliveries[iTeam]," for team: ",iTeam)
		ENDIF
		
		IF MC_serverBD.iNumVehHighestPriority[iTeam] != 0 
		AND MC_serverBD.iNumVehHighestPriority[iTeam] > MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam]
			MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam] = MC_serverBD.iNumVehHighestPriority[iTeam]
			PRINTLN("[OBJECTIVE_ENTITIES] iMaxNumPriorityEntitiesThisRule (vehicles): ", MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam], " for team: ",iTeam) 
		ENDIF
		
	ENDIF
	
	IF MC_serverBD_4.iVehMissionLogic[iTeam] != iTempVehMissionLogic[iTeam]
		MC_serverBD_4.iVehMissionLogic[iTeam] = iTempVehMissionLogic[iTeam]
		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD_4.iVehMissionLogic[iTeam]   updated to: ",MC_serverBD_4.iVehMissionLogic[iTeam]," for team: ",iTeam) 
	ENDIF
	
	IF MC_serverBD.iNumObjHighestPriority[iTeam]  != iNumHighPriorityObj[iTeam] 
		MC_serverBD.iNumObjHighestPriority[iTeam]  = iNumHighPriorityObj[iTeam] 
		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD.iNumObjHighestPriority[iTeam]  updated to: ",MC_serverBD.iNumObjHighestPriority[iTeam]," for team: ",iTeam) 
		
		IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
		AND MC_serverBD.iNumObjHighestPriority[iTeam] > 0
			MC_serverBD.iRequiredDeliveries[iTeam] = IMIN(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRequiredDeliveries[MC_serverBD_4.iCurrentHighestPriority[iTeam]],MC_serverBD.iNumObjHighestPriority[iTeam])
			PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD.iRequiredDeliveries[iTeam] updated to: ",MC_serverBD.iRequiredDeliveries[iTeam]," for team: ",iTeam)
		ENDIF
		
		IF MC_serverBD.iNumObjHighestPriority[iTeam] != 0 
		AND MC_serverBD.iNumObjHighestPriority[iTeam] > MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam]
			MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam] = MC_serverBD.iNumObjHighestPriority[iTeam]
			PRINTLN("[OBJECTIVE_ENTITIES] iMaxNumPriorityEntitiesThisRule (objects): ", MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam], " for team: ",iTeam) 
		ENDIF
	ENDIF
	
	IF MC_serverBD_4.iObjMissionLogic[iTeam] != iTempObjMissionLogic[iTeam]
		MC_serverBD_4.iObjMissionLogic[iTeam] = iTempObjMissionLogic[iTeam]
		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD_4.iObjMissionLogic[iTeam]  updated to: ",MC_serverBD_4.iObjMissionLogic[iTeam]," for team: ",iTeam)
	ENDIF
	
	IF MC_serverBD_4.iObjMissionSubLogic[iTeam] != iTempObjMissionSubLogic[iTeam]
		MC_serverBD_4.iObjMissionSubLogic[iTeam] = iTempObjMissionSubLogic[iTeam]
		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD_4.iObjMissionSubLogic[iTeam]  updated to: ",MC_serverBD_4.iObjMissionSubLogic[iTeam]," for team: ",iTeam) 
	ENDIF
	
	IF MC_serverBD.iNumPriorityRespawnPed[iTeam]  !=  iTempNumPriorityRespawnPed[iTeam] 
		MC_serverBD.iNumPriorityRespawnPed[iTeam]  =  iTempNumPriorityRespawnPed[iTeam] 
		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD.iNumPriorityRespawnPed[iTeam]   updated to: ",MC_serverBD.iNumPriorityRespawnPed[iTeam]," for team: ",iTeam)
	ENDIF

	IF MC_serverBD.iNumPriorityRespawnVeh[iTeam]  !=  iTempNumPriorityRespawnVeh[iTeam] 
		MC_serverBD.iNumPriorityRespawnVeh[iTeam]  =  iTempNumPriorityRespawnVeh[iTeam] 
		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD.iNumPriorityRespawnVeh[iTeam]    updated to: ",MC_serverBD.iNumPriorityRespawnVeh[iTeam]," for team: ",iTeam) 
	ENDIF

	IF MC_serverBD.iNumPriorityRespawnObj[iTeam]  !=  iTempNumPriorityRespawnObj[iTeam] 
		MC_serverBD.iNumPriorityRespawnObj[iTeam]  =  iTempNumPriorityRespawnObj[iTeam] 
		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD.iNumPriorityRespawnObj[iTeam]    updated to: ",MC_serverBD.iNumPriorityRespawnObj[iTeam]," for team: ",iTeam) 
	ENDIF
		
//	IF MC_serverBD.iNumTrainHighestPriority[iTeam] != iNumHighPriorityTrain[iTeam]
//		MC_serverBD.iNumTrainHighestPriority[iTeam] = iNumHighPriorityTrain[iTeam]
//		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD.iNumTrainHighestPriority[iTeam] updated to: ",MC_serverBD.iNumTrainHighestPriority[iTeam]," for team: ",iTeam) 
//		IF MC_serverBD.iNumTrainHighestPriority[iTeam] != 0 
//		AND MC_serverBD.iNumTrainHighestPriority[iTeam] > MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam]
//			MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam] = MC_serverBD.iNumTrainHighestPriority[iTeam]
//			PRINTLN("[OBJECTIVE_ENTITIES] iMaxNumPriorityEntitiesThisRule (trains): ", MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam], " for team: ",iTeam) 
//		ENDIF
//	ENDIF
//	
//	IF MC_serverBD_4.iTrainMissionLogic[iTeam] != iTempTrainMissionLogic[iTeam]
//		MC_serverBD_4.iTrainMissionLogic[iTeam] = iTempTrainMissionLogic[iTeam]
//		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD_4.iTrainMissionLogic[iTeam] updated to: ", MC_serverBD_4.iTrainMissionLogic[iTeam], " for team: ", iTeam)
//	ENDIF

	IF iTempEntitiesInZoneBS != MC_serverBD.iEntitiesInPreReqZoneBS
		MC_serverBD.iEntitiesInPreReqZoneBS = iTempEntitiesInZoneBS
		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM - MC_serverBD.iEntitiesInPreReqZoneBS updated to: ", MC_serverBD.iEntitiesInPreReqZoneBS) 
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_SKIP_TEAM_OBJECTIVE_ENTITY_UPDATE(INT iTeam)
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_SKIP_TEAM_0_UPDATE + iTeam)
		CLEAR_BIT(MC_serverBD.iServerBitSet, SBBOOL_SKIP_TEAM_0_UPDATE + iTeam)
		PRINTLN("[OBJECTIVE_ENTITIES] SHOULD_SKIP_TEAM_OBJECTIVE_ENTITY_UPDATE - Team: ", iTeam, " skipping update this loop.")
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC INT GET_RANDOM_RULE_TO_MOVE_TO_FOR_TEAM(INT iTeam, INT iPriority)
	
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[OBJECTIVE_PROGRESSION] GET_RANDOM_RULE_TO_MOVE_TO_FOR_TEAM - Called for Team: ", iTeam, " Priority: ", iPriority)
	
	INT iNewRule = FMMC_MAX_RULES
	
	INT iRandomNextRuleBS = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRandomNextRuleBS[iPriority]
		
	IF iRandomNextRuleBS = 0
		RETURN iNewRule
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iPriority], ciBS_RULE12_IGNORE_RANDOM_RULE_FOR_OBJECTIVE_FAIL)
	AND MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TIME_EXPIRED
		RETURN iNewRule
	ENDIF
		
	INT iCount = COUNT_SET_BITS(iRandomNextRuleBS)
	INT iBitChoice
				
	IF iCount > 1
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_KeepRandomNextObjectiveSeed)
		
			SET_RANDOM_SEED(MC_serverBD_1.iRandomNextObjectiveSeed[iTeam] + iPriority)
		
		//Setting the random seed to iSessionScriptEventKey - a network synced random number generated at the start of each session
		//This means we'll always get the same sequence of pseudo-random numbers sharing the session, but it will be different each round
		ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iPriority], ciBS_RULE9_USE_COMMON_RANDOM_NEXT_OBJECTIVE_SEED)
			IF MC_serverBD_1.iRandomNextObjectiveSeed[iTeam] = 0
				SET_RANDOM_SEED(MC_ServerBD.iSessionScriptEventKey)
				PRINTLN("[OBJECTIVE_PROGRESSION] GET_RANDOM_RULE_TO_MOVE_TO_FOR_TEAM - SET_RANDOM_SEED - MC_ServerBD.iSessionScriptEventKey = ", MC_ServerBD.iSessionScriptEventKey)
				MC_serverBD_1.iRandomNextObjectiveSeed[iTeam] = GET_RANDOM_INT_IN_RANGE(1000, 65535)
				PRINTLN("[OBJECTIVE_PROGRESSION] GET_RANDOM_RULE_TO_MOVE_TO_FOR_TEAM - GET_RANDOM_INT_IN_RANGE - iRandomNextObjectiveSeed[", iTeam, "] = ", MC_serverBD_1.iRandomNextObjectiveSeed[iTeam])
			ELSE
				SET_RANDOM_SEED(MC_serverBD_1.iRandomNextObjectiveSeed[iTeam])
				PRINTLN("[OBJECTIVE_PROGRESSION] GET_RANDOM_RULE_TO_MOVE_TO_FOR_TEAM - SET_RANDOM_SEED - iRandomNextObjectiveSeed[", iTeam, "] = ", MC_serverBD_1.iRandomNextObjectiveSeed[iTeam])
				MC_serverBD_1.iRandomNextObjectiveSeed[iTeam] = GET_RANDOM_INT_IN_RANGE(1000, 65535)
				PRINTLN("[OBJECTIVE_PROGRESSION] GET_RANDOM_RULE_TO_MOVE_TO_FOR_TEAM - GET_RANDOM_INT_IN_RANGE - iRandomNextObjectiveSeed[", iTeam, "] = ", MC_serverBD_1.iRandomNextObjectiveSeed[iTeam])
			ENDIF
		ENDIF
		
		iBitChoice = GET_RANDOM_INT_IN_RANGE(1000, ((iCount + 1) * 1000))
		PRINTLN("[OBJECTIVE_PROGRESSION] GET_RANDOM_RULE_TO_MOVE_TO_FOR_TEAM - iCount = ", iCount)
		PRINTLN("[OBJECTIVE_PROGRESSION] GET_RANDOM_RULE_TO_MOVE_TO_FOR_TEAM - GET_RANDOM_INT_IN_RANGE iBitChoice = ", iBitChoice)
		iBitChoice = FLOOR(TO_FLOAT(iBitChoice) / 1000.0)
		PRINTLN("[OBJECTIVE_PROGRESSION] GET_RANDOM_RULE_TO_MOVE_TO_FOR_TEAM - FLOOR iBitChoice = ", iBitChoice)
	ELSE
		iBitChoice = 1
	ENDIF
				
	INT i, iTempCount = 0
	FOR i = 0 TO FMMC_MAX_RULES - 1
		IF IS_BIT_SET(iRandomNextRuleBS, i)
			iTempCount++
			
			IF iTempCount = iBitChoice
				iNewRule = i
				BREAKLOOP
			ENDIF
		ENDIF
		
		IF iTempCount >= iCount
			BREAKLOOP
		ENDIF
	ENDFOR
	
	RETURN iNewRule

ENDFUNC

PROC PROCESS_NEAREST_TARGET_UPDATE()
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	FLOAT fDropOffDist2
	IF DOES_BLIP_EXIST(DeliveryBlip)
		VECTOR vDropOffCenter = GET_DROP_OFF_CENTER(TRUE)
		IF NOT IS_VECTOR_ZERO(vDropOffCenter)
			fDropOffDist2 = GET_DIST2_BETWEEN_ENTITY_AND_COORD(PlayerPedToUse, vDropOffCenter)
			iNearestTargetTypeTemp = ci_TARGET_DROP_OFF
			fNearestTargetDist2Temp = fDropOffDist2
			iNearestTargetTemp = 0
			PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_NEAREST_TARGET_UPDATE - Setting iNearestTargetTemp to 0. fNearestTargetDistTemp = ", fNearestTargetDist2Temp)
		ENDIF
	ELIF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_DUMMY_BLIP_OVERRIDE)	
	AND iDummyBlipCurrentOverride > -1
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] = g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipCurrentOverride].iRule
			IF NOT IS_VECTOR_ZERO(GET_DUMMY_OVERRIDE_CENTER(iDummyBlipCurrentOverride))
				fDropOffDist2 = GET_DIST2_BETWEEN_ENTITY_AND_COORD(PlayerPedToUse, GET_DUMMY_OVERRIDE_CENTER(iDummyBlipCurrentOverride))
				IF fDropOffDist2 < fNearestTargetDist2Temp
					iNearestTargetTypeTemp = ci_TARGET_DROP_OFF
					fNearestTargetDist2Temp = fDropOffDist2
					iNearestTargetTemp = 0
					PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_NEAREST_TARGET_UPDATE - Dummy Blip Override - Setting iNearestTargetTemp to 0. fNearestTargetDist2Temp = ", fNearestTargetDist2Temp)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iNearestTarget = iNearestTargetTemp
	AND iNearestTargetType = iNearestTargetTypeTemp
		
		//Target hasn't changed so always update the distance
		fNearestTargetDist2 = fNearestTargetDist2Temp
		
	ELSE
		IF iNearestTargetTemp = -1
		OR iNearestTargetTypeTemp = ci_TARGET_NONE
			PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_NEAREST_TARGET_UPDATE - Updated to no target")
			iNearestTarget = iNearestTargetTemp
			iNearestTargetType = iNearestTargetTypeTemp
			EXIT
		ENDIF
	
		FLOAT fNearestTargetThreshold2 = POW(TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iNearestTargetThreshold[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]]), 2)
		IF iNearestTargetType != iNearestTargetTypeTemp
		OR (fNearestTargetThreshold2 = 0 OR fNearestTargetDist2Temp + fNearestTargetThreshold2 < fNearestTargetDist2)
			
			IF iNearestTarget != iNearestTargetTemp
				iNearestTarget = iNearestTargetTemp
				PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_NEAREST_TARGET_UPDATE - hint iNearestTarget updated to: ", iNearestTarget)
			ENDIF
			
			IF iNearestTargetType != iNearestTargetTypeTemp
				iNearestTargetType = iNearestTargetTypeTemp
				PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_NEAREST_TARGET_UPDATE - hint iNearestTargetType updated to: ", iNearestTargetType)
			ENDIF
			
			fNearestTargetDist2 = fNearestTargetDist2Temp
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP()
	
	PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP - Staggered Loop Ended")
	
	PROCESS_NEAREST_TARGET_UPDATE()
	
	IF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_STARTED_UPDATE)
		SET_BIT(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)
		CLEAR_BIT(iLocalBoolCheck2,LBOOL2_REQUEST_UPDATE)
		CLEAR_BIT(iLocalBoolCheck2,LBOOL2_STARTED_UPDATE)
		PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP - Requested Updated Done")
	ENDIF
		
	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	
	//##### Host only from this point on #####

	INT iPedBSLoop = 0
	FOR iPedBSLoop = 0 TO FMMC_MAX_PEDS_BITSET - 1
		MC_serverBD.iPedDelayRespawnBitset[iPedBSLoop] = 0			
	ENDFOR
	
	MC_serverBD.iVehDelayRespawnBitset = 0
	
	INT iTeam
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
	
		IF SHOULD_SKIP_TEAM_OBJECTIVE_ENTITY_UPDATE(iTeam)
			RELOOP
		ENDIF
		
		INT iOldHighestPriority = MC_serverBD_4.iCurrentHighestPriority[iTeam]
			
		PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM(iTeam)
		
		IF (MC_serverBD.iMaxObjectives[iTeam] - MC_serverBD_4.iCurrentHighestPriority[iTeam]) <= 1
			SET_BIT(MC_serverBD.iServerBitSet, SBBOOL_SET_NOT_JOINABLE)
		ENDIF	
		
		IF MC_serverBD_4.iCurrentHighestPriority[iTeam] = iOldHighestPriority
			RELOOP
		ENDIF
			
		IF iOldHighestPriority < 0
		OR iOldHighestPriority >= FMMC_MAX_RULES
			RELOOP
		ENDIF
		
		MC_serverBD_4.iPreviousHighestPriority[iTeam] = iOldHighestPriority
											
		//Picking a random rule to move to:
		INT iRandomNextRuleBS = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRandomNextRuleBS[iOldHighestPriority]
		
		IF iRandomNextRuleBS = 0
			RELOOP
		ENDIF
		
		INT iNewRule = GET_RANDOM_RULE_TO_MOVE_TO_FOR_TEAM(iTeam, iOldHighestPriority)
		
		IF iNewRule < FMMC_MAX_RULES
			IF iNewRule > MC_ServerBD_4.iCurrentHighestPriority[iTeam]
				PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP - Picked new rule for team ",iTeam," just finishing objective ",iOldHighestPriority,": new rule = ",iNewRule)
				INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iTeam, iNewRule - 1)
				REQUEST_RECALCULATE_OBJECTIVE_LOGIC_FOR_TEAM(iTeam)
			ELIF iNewRule < MC_ServerBD_4.iCurrentHighestPriority[iTeam]
				PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP - Picked new rule for team ",iTeam," just finishing objective ",iOldHighestPriority,": new rule (RETURNING TO AN OLDER RULE!) = ",iNewRule)
				INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iTeam, iNewRule, DEFAULT, DEFAULT, TRUE)
				REQUEST_RECALCULATE_OBJECTIVE_LOGIC_FOR_TEAM(iTeam)
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP - Failed to apply new rule for team ",iTeam," just finishing objective ",iOldHighestPriority,": new unapplied rule = ",iNewRule,", current rule = ",MC_serverBD_4.iCurrentHighestPriority[iTeam])
			#ENDIF
			ENDIF
		ENDIF
		
	ENDFOR
	
ENDPROC

PROC PROCESS_CLIENT_OBJECTIVE_ENTITIES(FMMC_OBJECT_STATE &sObjState)
	
	IF MC_serverBD.iNumLocCreated > 0
	AND MC_serverBD.iNumLocCreated <= FMMC_MAX_GO_TO_LOCATIONS
	AND iObjectiveEntityIterator < MC_serverBD.iNumLocCreated	
		PROCESS_LOCATION_OBJECTIVES_CLIENT(iObjectiveEntityIterator)
	ENDIF
	
	IF MC_serverBD.iNumPedCreated > 0
	AND MC_serverBD.iNumPedCreated <= FMMC_MAX_PEDS
	AND iObjectiveEntityIterator < MC_serverBD.iNumPedCreated
		PROCESS_PED_OBJECTIVES_CLIENT(iObjectiveEntityIterator)
	ENDIF
	
	IF MC_serverBD.iNumVehCreated > 0
	AND MC_serverBD.iNumVehCreated <= FMMC_MAX_VEHICLES
	AND iObjectiveEntityIterator < MC_serverBD.iNumVehCreated		
		PROCESS_VEH_OBJECTIVES_CLIENT(iObjectiveEntityIterator)
	ENDIF
	
	IF MC_serverBD.iNumObjCreated > 0	
	AND MC_serverBD.iNumObjCreated <= FMMC_MAX_NUM_OBJECTS
	AND iObjectiveEntityIterator < MC_serverBD.iNumObjCreated	
		FILL_FMMC_OBJECT_STATE_STRUCT(sObjState, iObjectiveEntityIterator, TRUE)
		PROCESS_OBJ_OBJECTIVES_CLIENT(sObjState)
	ENDIF
	
	IF MC_serverBD.iNumTrainCreated > 0	
	AND MC_serverBD.iNumTrainCreated <= FMMC_MAX_TRAINS
	AND iObjectiveEntityIterator < MC_serverBD.iNumTrainCreated	
		PROCESS_TRAIN_OBJECTIVES_CLIENT(iObjectiveEntityIterator)
	ENDIF
	
ENDPROC

PROC PROCESS_SERVER_OBJECTIVE_ENTITIES(FMMC_OBJECT_STATE &sObjState)

	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	
	INT iTeam
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		
		IF MC_serverBD.iNumPlayerRuleCreated > 0
		AND MC_serverBD.iNumPlayerRuleCreated <= FMMC_MAX_RULES
		AND iObjectiveEntityIterator < MC_serverBD.iNumPlayerRuleCreated	
			PROCESS_PLAYER_OBJECTIVES_SERVER(iObjectiveEntityIterator, iTeam)
		ENDIF		

		IF MC_serverBD.iNumLocCreated > 0
		AND MC_serverBD.iNumLocCreated <= FMMC_MAX_GO_TO_LOCATIONS
		AND iObjectiveEntityIterator < MC_serverBD.iNumLocCreated	
			PROCESS_LOCATION_OBJECTIVES_SERVER(iObjectiveEntityIterator, iTeam)
		ENDIF
		
		IF MC_serverBD.iNumPedCreated > 0
		AND MC_serverBD.iNumPedCreated <= FMMC_MAX_PEDS
		AND iObjectiveEntityIterator < MC_serverBD.iNumPedCreated
			PROCESS_PED_OBJECTIVES_SERVER(iObjectiveEntityIterator, iTeam)
		ENDIF
		
		IF MC_serverBD.iNumVehCreated > 0
		AND MC_serverBD.iNumVehCreated <= FMMC_MAX_VEHICLES
		AND iObjectiveEntityIterator < MC_serverBD.iNumVehCreated		
			
			PROCESS_VEH_OBJECTIVES_SERVER(iObjectiveEntityIterator, iTeam)
		ENDIF
		
		IF MC_serverBD.iNumObjCreated > 0	
		AND MC_serverBD.iNumObjCreated <= FMMC_MAX_NUM_OBJECTS
		AND iObjectiveEntityIterator < MC_serverBD.iNumObjCreated	
			FILL_FMMC_OBJECT_STATE_STRUCT(sObjState, iObjectiveEntityIterator, TRUE)
			PROCESS_OBJ_OBJECTIVES_SERVER(sObjState, iTeam)
		ENDIF
		
		IF MC_serverBD.iNumTrainCreated > 0
		AND MC_serverBD.iNumTrainCreated <= FMMC_MAX_TRAINS
		AND iObjectiveEntityIterator < MC_serverBD.iNumTrainCreated		
			PROCESS_TRAIN_OBJECTIVES_SERVER(iObjectiveEntityIterator, iTeam)
		ENDIF
		
	ENDFOR
	
ENDPROC

PROC PROCESS_OBJECTIVE_ENTITIES()
	
	FMMC_OBJECT_STATE sObjState
	
	INT i
	FOR i = 0 TO ciOBJECTIVE_ENTITIES_PER_FRAME - 1
		
		IF iObjectiveEntityIterator = 0		
			PROCESS_PRE_OBJECTIVE_ENTITY_STAGGERED_LOOP()
		ENDIF
		
		PROCESS_CLIENT_OBJECTIVE_ENTITIES(sObjState)
		PROCESS_SERVER_OBJECTIVE_ENTITIES(sObjState)
		
		iObjectiveEntityIterator++
		
		IF iObjectiveEntityIterator >= MC_serverBD.iMaxLoopSize 
			iObjectiveEntityIterator = 0
			PROCESS_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP()
			
			//Don't go back to to the start of the loop until next frame.
			BREAKLOOP
		ENDIF
		
	ENDFOR
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: PROCESS ENTITIES
// ##### Description: The central entity processing function
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
				
PROC PROCESS_ENTITIES()
	
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_ENTITIES)
	#ENDIF
	
	PROCESS_OBJECTIVE_ENTITIES()
			
	PROCESS_ENTITIES_STAGGERED()
	
	PROCESS_ENTITIES_EVERY_FRAME()
		
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_ENTITIES)
	#ENDIF
	
ENDPROC
