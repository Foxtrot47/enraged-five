// ##### ------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Variations Utility -----------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc ----------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Shared Creator / Controller logic relating to the Alternate Variables System          
// ##### ------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020                                                                                     
// ##### ------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "fm_mission_controller_include_2020.sch"
	
// ##### FIXER DLC Server Bitsets - These decide what index is selected for Alternate Variable globals. They should reflect Lobby Decisions, Freemode Event Outcomes, Character Stats, etc. 
// Server Bitset: iAltVarsBS
CONST_INT ciMC_ALT_VAR_FIXER_BS_SyncedData	0


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cutscene Overrides																														 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_MC_ALT_VAR_FIXER_PROPERTY_INDEX_CHOSEN_FOR_CUTSCENE()
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1
		RETURN -1
	ENDIF
	
	SWITCH GET_PLAYERS_OWNED_FIXER_HQ(GB_GET_LOCAL_PLAYER_GANG_BOSS())
		CASE FIXER_HQ_HAWICK
			RETURN ciARRAY_INT_ALT_VAR_FIXER_CUTSCENE_INDEX_FOR_PROPERTY_HAWICK
		BREAK
		CASE FIXER_HQ_ROCKFORD
			RETURN ciARRAY_INT_ALT_VAR_FIXER_CUTSCENE_INDEX_FOR_PROPERTY_ROCKFORD
		BREAK
		CASE FIXER_HQ_SEOUL
			RETURN ciARRAY_INT_ALT_VAR_FIXER_CUTSCENE_INDEX_FOR_PROPERTY_SEOUL
		BREAK
		CASE FIXER_HQ_VESPUCCI
			RETURN ciARRAY_INT_ALT_VAR_FIXER_CUTSCENE_INDEX_FOR_PROPERTY_VESPUCCI
		BREAK
	ENDSWITCH
	
	RETURN -1
	
ENDFUNC

PROC PROCESS_MC_ALT_VAR_FIXER_OVERRIDE_RULE_CUTSCENE_INDEX_WITH_PROPERTY_LOCATION(INT iTeam, INT iRule)

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_Fixer_Rule_Flag_UsePoolVarForCutsceneIndex)
		EXIT
	ENDIF
	
	INT iIndex = GET_MC_ALT_VAR_FIXER_PROPERTY_INDEX_CHOSEN_FOR_CUTSCENE()
	
	IF iIndex = -1
		PRINTLN("[LM][AltVarsSystem][Cutscene][Rule: ", iRule, "] - PROCESS_MC_ALT_VAR_FIXER_OVERRIDE_RULE_CUTSCENE_INDEX_WITH_PROPERTY_LOCATION - Not overriding. Bunker iIndex: ", iIndex)
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Cutscene][Rule: ", iRule, "] - PROCESS_MC_ALT_VAR_FIXER_OVERRIDE_RULE_CUTSCENE_INDEX_WITH_PROPERTY_LOCATION - Bunker iIndex: ", iIndex)
	
	INT iMod = g_FMMC_STRUCT.sPoolVars.iInt[iIndex]
	
	IF iMod = -1
		PRINTLN("[LM][AltVarsSystem][Cutscene][Rule: ", iRule, "] - PROCESS_MC_ALT_VAR_FIXER_OVERRIDE_RULE_CUTSCENE_INDEX_WITH_PROPERTY_LOCATION - Not overriding. Cutscene is iMod: ", iMod)
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Cutscene][Rule: ", iRule, "] - PROCESS_MC_ALT_VAR_FIXER_OVERRIDE_RULE_CUTSCENE_INDEX_WITH_PROPERTY_LOCATION - Overriding. Cutscene is iMod: ", iMod)
	
	INT iPlayerRule = 0
	FOR iPlayerRule = 0 TO FMMC_MAX_RULES-1
		
		PRINTLN("Spam - PROCESS_MC_ALT_VAR_FIXER_OVERRIDE_RULE_CUTSCENE_INDEX_WITH_PROPERTY_LOCATION - iRule: ", iRule, " iPlayerRule: ", iPlayerRule, " MC_serverBD_4.iPlayerRulePriority: ", MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam])
	
		IF MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam] = iRule
			iRule = iPlayerRule
			PRINTLN("Spam - PROCESS_MC_ALT_VAR_FIXER_OVERRIDE_RULE_CUTSCENE_INDEX_WITH_PROPERTY_LOCATION - BREAKING LOOP")
			BREAKLOOP
		ENDIF
		
	ENDFOR
	
	// The Cutscene Index to play.
 	g_FMMC_STRUCT.sPlayerRuleData[iRule].iPlayerRuleLimit[iteam] = iMod
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Spawn Groups																															 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_MC_ALT_VAR_FIXER_DEFINED_SPAWN_GROUP_SET(SPAWN_GROUP_DEFINE_ACTIVATION eSpawnGroupActivationType)
	
	SWITCH eSpawnGroupActivationType
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_FIXER_PROPERTY_FIXER_HQ_HAWICK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_FIXER_PROPERTY_FIXER_HQ_ROCKFORD
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_FIXER_PROPERTY_FIXER_HQ_SEOUL
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_FIXER_PROPERTY_FIXER_HQ_VESPUCCI
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DOES_MC_ALT_VAR_FIXER_DEFINED_SPAWN_GROUP_MATCH_WITH_CHOSEN(SPAWN_GROUP_DEFINE_ACTIVATION eSpawnGroupActivationType)
	
	SWITCH eSpawnGroupActivationType
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_FIXER_PROPERTY_FIXER_HQ_HAWICK			
			IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1
				RETURN FALSE
			ENDIF
			#IF FEATURE_FIXER
			RETURN GET_PLAYERS_OWNED_FIXER_HQ(GB_GET_LOCAL_PLAYER_GANG_BOSS()) = FIXER_HQ_HAWICK			
			#ENDIF			
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_FIXER_PROPERTY_FIXER_HQ_ROCKFORD
			IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1
				RETURN FALSE
			ENDIF
			#IF FEATURE_FIXER
			RETURN GET_PLAYERS_OWNED_FIXER_HQ(GB_GET_LOCAL_PLAYER_GANG_BOSS()) = FIXER_HQ_ROCKFORD
			#ENDIF
		BREAK		
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_FIXER_PROPERTY_FIXER_HQ_SEOUL
			IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1
				RETURN FALSE
			ENDIF
			#IF FEATURE_FIXER
			RETURN GET_PLAYERS_OWNED_FIXER_HQ(GB_GET_LOCAL_PLAYER_GANG_BOSS()) = FIXER_HQ_SEOUL
			#ENDIF
		BREAK		
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_FIXER_PROPERTY_FIXER_HQ_VESPUCCI
			IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1
				RETURN FALSE
			ENDIF
			#IF FEATURE_FIXER
			RETURN GET_PLAYERS_OWNED_FIXER_HQ(GB_GET_LOCAL_PLAYER_GANG_BOSS()) = FIXER_HQ_VESPUCCI
			#ENDIF			
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_MC_ALT_VAR_FIXER_SUB_SPAWN_GROUP_ACTIVATION(INT iSpawnGroup, INT iSubSpawnGroup)	
	
	IF NOT IS_MC_ALT_VAR_FIXER_DEFINED_SPAWN_GROUP_SET(g_FMMC_STRUCT.eRandomEntitySpawnSub_DefineActivation[iSpawnGroup][iSubSpawnGroup])
		EXIT
	ENDIF
	
	IF NOT DOES_MC_ALT_VAR_FIXER_DEFINED_SPAWN_GROUP_MATCH_WITH_CHOSEN(g_FMMC_STRUCT.eRandomEntitySpawnSub_DefineActivation[iSpawnGroup][iSubSpawnGroup])
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][SpawnGroup: ", iSpawnGroup, "][SubSpawnGroup: ", iSubSpawnGroup, "][DefinedAction: ", GET_SPAWN_GROUP_DEFINE_ACTIVATION_NAME_FOR_PRINT(g_FMMC_STRUCT.eRandomEntitySpawnSub_DefineActivation[iSpawnGroup][iSubSpawnGroup]), "] - PROCESS_MC_ALT_VAR_FIXER_SUB_SPAWN_GROUP_ACTIVATION - Setting Sub Spawn Group to SPAWN_GROUP_DEFINE_ACTIVATION_FORCED_ON")
	
	g_FMMC_STRUCT.eRandomEntitySpawnSub_DefineActivation[iSpawnGroup][iSubSpawnGroup] = SPAWN_GROUP_DEFINE_ACTIVATION_FORCED_ON
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_FIXER_SPAWN_GROUP_ACTIVATION(INT iSpawnGroup)	
	
	IF NOT IS_MC_ALT_VAR_FIXER_DEFINED_SPAWN_GROUP_SET(g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[iSpawnGroup])
		EXIT
	ENDIF
	
	IF NOT DOES_MC_ALT_VAR_FIXER_DEFINED_SPAWN_GROUP_MATCH_WITH_CHOSEN(g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[iSpawnGroup])
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][SpawnGroup: ", iSpawnGroup, "][DefinedAction: ", GET_SPAWN_GROUP_DEFINE_ACTIVATION_NAME_FOR_PRINT(g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[iSpawnGroup]), "] - PROCESS_MC_ALT_VAR_FIXER_SPAWN_GROUP_ACTIVATION - Setting Spawn Group to SPAWN_GROUP_DEFINE_ACTIVATION_FORCED_ON")
	
	g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[iSpawnGroup] = SPAWN_GROUP_DEFINE_ACTIVATION_FORCED_ON
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Spawn Block Flags																													 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_MC_ALT_VAR_FIXER_SPAWN_BLOCKING_FLAG_SET(SPAWN_CONDITION_FLAG eSpawnConditionFlag)
	
	SWITCH eSpawnConditionFlag
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_A // left here for example
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DOES_MC_ALT_VAR_FIXER_FLAGGED_MATCH_WITH_CHOSEN(SPAWN_CONDITION_FLAG eSpawnConditionFlag)
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1	
		PRINTLN("[LM][AltVarsSystem] - DOES_MC_ALT_VAR_FIXER_FLAGGED_MATCH_WITH_CHOSEN - GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1 - RETURNING FALSE")
		RETURN FALSE
	ENDIF
		
	SWITCH eSpawnConditionFlag
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_A 		// left here for example
			RETURN IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_A))
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_MC_ALT_VAR_FIXER_PED_SPAWN_BLOCKING_FLAG_CHECKS(INT iPed)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1	
		IF NOT IS_MC_ALT_VAR_FIXER_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_FIXER_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "][Flag: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].eSpawnConditionFlag[iFlag]), "] - PROCESS_MC_ALT_VAR_FIXER_PED_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET	
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_FIXER_VEHICLE_SPAWN_BLOCKING_FLAG_CHECKS(INT iVeh)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
			
		IF NOT IS_MC_ALT_VAR_FIXER_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_FIXER_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "][iFlag: ", iFlag, "] - PROCESS_MC_ALT_VAR_FIXER_VEHICLE_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_FIXER_OBJECT_SPAWN_BLOCKING_FLAG_CHECKS(INT iObj)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1	
		IF NOT IS_MC_ALT_VAR_FIXER_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_FIXER_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][Object: ", iObj, "][iFlag: ", iFlag, "] - PROCESS_MC_ALT_VAR_FIXER_OBJECT_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_FIXER_INTERACTABLE_SPAWN_BLOCKING_FLAG_CHECKS(INT iInteractable)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1		
		IF NOT IS_MC_ALT_VAR_FIXER_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_FIXER_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "][iFlag: ", iFlag, "] - PROCESS_MC_ALT_VAR_FIXER_INTERACTABLE_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_FIXER_PROP_SPAWN_BLOCKING_FLAG_CHECKS(INT iProp)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1		
		IF NOT IS_MC_ALT_VAR_FIXER_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_FIXER_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][Prop: ", iProp, "][iFlag: ", iFlag, "] - PROCESS_MC_ALT_VAR_FIXER_PROP_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_FIXER_DYNO_PROP_SPAWN_BLOCKING_FLAG_CHECKS(INT iDynoProp)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1		
		IF NOT IS_MC_ALT_VAR_FIXER_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_FIXER_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][DynoProp: ", iDynoProp, "][iFlag: ", iFlag, "] - PROCESS_MC_ALT_VAR_FIXER_DYNO_PROP_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_FIXER_TEAM_SPAWN_POINT_SPAWN_BLOCKING_FLAG_CHECKS(INT iSpawnPoint, INT iTeam)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1	
		IF NOT IS_MC_ALT_VAR_FIXER_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_FIXER_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		PRINTLN("[LM][AltVarsSystem][SpawnPoint: ", iSpawnPoint, "][iTeam: ", iTeam, "][iFlag: ", iFlag, "] - PROCESS_MC_ALT_VAR_FIXER_TEAM_SPAWN_POINT_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_FIXER_ZONE_SPAWN_BLOCKING_FLAG_CHECKS(INT iZone)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1	
		IF NOT IS_MC_ALT_VAR_FIXER_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_FIXER_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][Zone: ", iZone, "][Flag: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].eSpawnConditionFlag[iFlag]), "] - PROCESS_MC_ALT_VAR_FIXER_ZONE_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET	
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_FIXER_DIALOGUE_TRIGGERS_SPAWN_BLOCKING_FLAG_CHECKS(INT iDialogueTrigger)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1	
		IF NOT IS_MC_ALT_VAR_FIXER_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_FIXER_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][DialogueTrigger: ", iDialogueTrigger, "][Flag: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].eSpawnConditionFlag[iFlag]), "] - PROCESS_MC_ALT_VAR_FIXER_DIALOGUE_TRIGGERS_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET	
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_FIXER_WEAPON_SPAWN_BLOCKING_FLAG_CHECKS(INT iWeapon)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1	
		IF NOT IS_MC_ALT_VAR_FIXER_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_FIXER_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][Weapon: ", iWeapon, "][Flag: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].eSpawnConditionFlag[iFlag]), "] - PROCESS_MC_ALT_VAR_FIXER_WEAPON_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET	
	ENDFOR
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: (SERVER) AltVars Start up Process Server ------------------------------------------------------------------------------------------------------------------------
// ##### Description: This is called to decide certain variables before all the variations globals are applied to the mission globals. Done only by Server.		------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_FIXER()
	
	FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_FIXER_BS_SyncedData)	

	#IF FEATURE_FIXER 
	
	#ENDIF
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_FIXER_FORCE_INVENTORY_BASED_ON_FIXER_ARMORY()

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Fixer_Generic_Flag_ForceInventoryBasedOnFixerArmory)
		EXIT
	ENDIF
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = INVALID_PLAYER_INDEX()
		PRINTLN("[LM][AltVarsSystem] - PROCESS_MC_ALT_VAR_FIXER_FORCE_INVENTORY_BASED_ON_FIXER_ARMORY - No Gang Boss.")
		EXIT
	ENDIF
	
	INT iIndex = -1	
	
	IF HAS_PLAYER_PURCHASED_FIXER_HQ_UPGRADE_ARMORY(GB_GET_LOCAL_PLAYER_GANG_BOSS())
		iIndex = 1
	ELSE
		iIndex = 0	
	ENDIF
	
	INT iTeam = 0
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		
		PRINTLN("[LM][AltVarsSystem][Team: ", iTeam, "] - PROCESS_MC_ALT_VAR_FIXER_FORCE_INVENTORY_BASED_ON_FIXER_ARMORY - Forcing iStartingInventoryIndex to: ", iIndex)
	
		iInventory_Starting[iTeam] = iIndex
		
	ENDFOR
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: (CLIENT) AltVars Main Process / Loops ---------------------------------------------------------------------------------------------------------------------------
// ##### Description: All the global overrides and trigger checks are carried out here.											 								------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_SPAWN_GROUPS_FIXER()
	INT i, ii

	FOR i = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
		PROCESS_MC_ALT_VAR_FIXER_SPAWN_GROUP_ACTIVATION(i)
		FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
			PROCESS_MC_ALT_VAR_FIXER_SUB_SPAWN_GROUP_ACTIVATION(i, ii)
		ENDFOR
	ENDFOR
ENDPROC 

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_GLOBALS_FIXER()
	
	INT i, ii, iii	
	
	FOR i = 0 TO FMMC_MAX_RULES-1
		FOR ii = 0 TO FMMC_MAX_TEAMS-1
			PROCESS_MC_ALT_VAR_FIXER_OVERRIDE_RULE_CUTSCENE_INDEX_WITH_PROPERTY_LOCATION(ii, i)
		ENDFOR
	ENDFOR
	
	PROCESS_MC_ALT_VAR_FIXER_FORCE_INVENTORY_BASED_ON_FIXER_ARMORY()
	
	FOR i = 0 TO FMMC_MAX_INVENTORIES-1
		FOR ii = 0 TO FMMC_MAX_INVENTORY_WEAPONS-1 
			FOR iii = 0 TO FMMC_MAX_WEAPON_COMPONENTS-1
				
			ENDFOR
		ENDFOR	
	ENDFOR
	
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_PEDS_FIXER()
	INT i
	FOR i = 0 TO FMMC_MAX_PEDS-1
		PROCESS_MC_ALT_VAR_FIXER_PED_SPAWN_BLOCKING_FLAG_CHECKS(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_VEHICLES_FIXER()
	INT i
	FOR i = 0 TO FMMC_MAX_VEHICLES-1	
		PROCESS_MC_ALT_VAR_FIXER_VEHICLE_SPAWN_BLOCKING_FLAG_CHECKS(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_TRAINS_FIXER()
	INT i
	FOR i = 0 TO FMMC_MAX_TRAINS-1	
	
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_OBJECTS_FIXER()
	INT i
	FOR i = 0 TO FMMC_MAX_NUM_OBJECTS-1
		PROCESS_MC_ALT_VAR_FIXER_OBJECT_SPAWN_BLOCKING_FLAG_CHECKS(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_DYNOPROPS_FIXER()
	INT i
	FOR i = 0 TO FMMC_MAX_NUM_DYNOPROPS-1
		PROCESS_MC_ALT_VAR_FIXER_DYNO_PROP_SPAWN_BLOCKING_FLAG_CHECKS(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_PROPS_FIXER()
	INT i
	FOR i = 0 TO GET_FMMC_MAX_NUM_PROPS()-1
		PROCESS_MC_ALT_VAR_FIXER_PROP_SPAWN_BLOCKING_FLAG_CHECKS(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_DIALOGUE_TRIGGERS_FIXER()
	INT i
	FOR i = 0 TO FMMC_MAX_DIALOGUES-1
		PROCESS_MC_ALT_VAR_FIXER_DIALOGUE_TRIGGERS_SPAWN_BLOCKING_FLAG_CHECKS(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_ZONES_FIXER()
	INT i
	FOR i = 0 TO FMMC_MAX_NUM_ZONES - 1			
		PROCESS_MC_ALT_VAR_FIXER_ZONE_SPAWN_BLOCKING_FLAG_CHECKS(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_DUMMY_BLIPS_FIXER()
	INT i
	FOR i = 0 TO FMMC_MAX_DUMMY_BLIPS - 1
	
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_PICKUPS_FIXER()
	INT i
	FOR i = 0 TO FMMC_MAX_WEAPONS-1
		PROCESS_MC_ALT_VAR_FIXER_WEAPON_SPAWN_BLOCKING_FLAG_CHECKS(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_SPAWN_POINTS_FIXER()
	INT i, ii
	FOR ii = 0 TO FMMC_MAX_TEAMS-1
		FOR i = 0 TO FMMC_MAX_TEAMSPAWNPOINTS-1
			PROCESS_MC_ALT_VAR_FIXER_TEAM_SPAWN_POINT_SPAWN_BLOCKING_FLAG_CHECKS(i, ii)
		ENDFOR
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_INTERACTABLES_FIXER()
	INT i
	FOR i = 0 TO FMMC_MAX_NUM_INTERACTABLES-1
		PROCESS_MC_ALT_VAR_FIXER_INTERACTABLE_SPAWN_BLOCKING_FLAG_CHECKS(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_PRE_SPAWN_GROUP_SETTINGS_FIXER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_SPAWN_GROUPS_FIXER()
	
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FIXER()
	
	#IF FEATURE_FIXER 
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1
		PRINTLN("[LM][AltVarsSystem] - ###########################################################################")	
		PRINTLN("[LM][AltVarsSystem] - ###### Something has gone seriously wrong. We have no valid boss INT ######")		
		PRINTLN("[LM][AltVarsSystem] - ###########################################################################")
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_NO_GANGBOSS, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "No Gang Boss, value is -1. This will cause a lot of ALT VARS To function improperly.")
		#ENDIF
	ELSE
		PRINTLN("[LM][AltVarsSystem] - Boss INT: ", GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT())
	ENDIF
		
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_GLOBALS_FIXER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_SPAWN_POINTS_FIXER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_PEDS_FIXER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_OBJECTS_FIXER()
		
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_VEHICLES_FIXER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_TRAINS_FIXER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_DYNOPROPS_FIXER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_PROPS_FIXER()
		
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_DIALOGUE_TRIGGERS_FIXER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_ZONES_FIXER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_DUMMY_BLIPS_FIXER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_PICKUPS_FIXER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_INTERACTABLES_FIXER()
	
	#ENDIF
	
ENDPROC
