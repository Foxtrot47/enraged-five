// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Players ----------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### Processing of all connected players
// ##### TODO - Remove player loops from almost all the functions in here
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020 -------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Include_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Misc -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
///PURPOSE: Checks player scores/kills/headshots/deaths and adds any new changes into their Team's score/kills/headshots/deaths variable in the server broadcast data
PROC COUNT_PLAYER_SCORES(INT iParticipant)

	INT iTeam = MC_Playerbd[iParticipant].iTeam
	INT iChange

	IF iTeam <= -1
		EXIT
	ENDIF
	
	IF MC_serverBD.iPlayerScore[iParticipant] != MC_Playerbd[iParticipant].iPlayerScore
		iChange = (MC_Playerbd[iParticipant].iPlayerScore - MC_serverBD.iPlayerScore[iParticipant])
		PRINTLN("[Players][Participant: ", iParticipant, "]  COUNT_PLAYER_SCORES -  iChange is ", iChange, "MC_Playerbd[iParticipant].iPlayerScore is ", MC_Playerbd[iParticipant].iPlayerScore)
		IF IS_PARTICIPANT_A_SPECTATOR(iParticipant)
			iChange = 0
			IF IS_PARTICIPANT_A_SPECTATOR(iParticipant)
				PRINTLN("[Players][Participant: ", iParticipant, "]  COUNT_PLAYER_SCORES -  IS_PARTICIPANT_A_SPECTATOR = iChange is ", iChange)
			ENDIF
		ELSE
			PRINTLN("[Players][Participant: ", iParticipant, "]  COUNT_PLAYER_SCORES -  MC_serverBD.iPlayerScore: ", MC_serverBD.iPlayerScore[iParticipant], " = MC_Playerbd[iParticipant].iPlayerScore: ", MC_Playerbd[iParticipant].iPlayerScore)
			MC_serverBD.iPlayerScore[iParticipant] = MC_Playerbd[iParticipant].iPlayerScore
			
			PRINTLN("[Players][Participant: ", iParticipant, "]  COUNT_PLAYER_SCORES -  MC_serverBD.iTeamScore: ", MC_serverBD.iTeamScore[iTeam], " = MC_serverBD.iTeamScore[iTeam] + iChange: ", MC_serverBD.iTeamScore[iTeam] + iChange)
			MC_serverBD.iTeamScore[iTeam] = MC_serverBD.iTeamScore[iTeam] + iChange
		ENDIF
		
		IF iChange != 0
			PRINTLN("[Players][Participant: ", iParticipant, "]  COUNT_PLAYER_SCORES -  team: ", iTeam, " score changed to: ", MC_serverBD.iTeamScore[iTeam])
			PRINTLN("[Players][Participant: ", iParticipant, "]  COUNT_PLAYER_SCORES -  ipart: ", iParticipant, " server score increased to: ", MC_serverBD.iPlayerScore[iParticipant]) 
			
			//So we can track who scored last
			MC_serverBD_3.iTeamToScoreLast = iTeam
		ENDIF
	ENDIF

	// Update the ped kill counts if the players have a different one to the server
	IF (MC_serverBD.iNumPlayerKills[iParticipant] + MC_serverBD.iNumPedKills[iParticipant]) < (MC_Playerbd[iParticipant].iNumPlayerKills + MC_Playerbd[iParticipant].iNumPedKills)
		SET_TEAM_KILLS(iTeam, GET_TEAM_KILLS(iTeam) + ((MC_Playerbd[iParticipant].iNumPlayerKills + MC_Playerbd[iParticipant].iNumPedKills) - (MC_serverBD.iNumPlayerKills[iParticipant] + MC_serverBD.iNumPedKills[iParticipant])))
		MC_serverBD.iNumPlayerKills[iParticipant] = MC_Playerbd[iParticipant].iNumPlayerKills
		MC_serverBD.iNumPedKills[iParticipant] = MC_Playerbd[iParticipant].iNumPedKills
		PRINTLN("[Players][Participant: ", iParticipant, "]  COUNT_PLAYER_SCORES -  team: ", iTeam, " kills increased to: ", GET_TEAM_KILLS(iTeam)) 
	ENDIF
	
	// Update the player death counts if the players have a different one to the server
	IF MC_serverBD.iNumPlayerDeaths[iParticipant] < MC_Playerbd[iParticipant].iNumPlayerDeaths
		SET_TEAM_DEATHS(iTeam, GET_TEAM_DEATHS(iTeam) + (MC_Playerbd[iParticipant].iNumPlayerDeaths - MC_serverBD.iNumPlayerDeaths[iParticipant]))
		MC_serverBD.iNumPlayerDeaths[iParticipant] = MC_Playerbd[iParticipant].iNumPlayerDeaths
		PRINTLN("[Players][Participant: ", iParticipant, "]  COUNT_PLAYER_SCORES -  team: ", iTeam, " deaths increased to: ", GET_TEAM_DEATHS(iTeam))
		PRINTLN("[Players][Participant: ", iParticipant, "]  COUNT_PLAYER_SCORES -  iParticipant: ", iParticipant, " deaths increased to: ", MC_serverBD.iNumPlayerDeaths[iParticipant])
	ENDIF
	
	// Update the kill score counts if the players have a different one to the server
	IF MC_serverBD.iKillScore[iParticipant] < MC_Playerbd[iParticipant].iKillScore
		MC_serverBD.iTeamKillScore[iTeam]  = MC_serverBD.iTeamKillScore[iTeam] + (MC_Playerbd[iParticipant].iKillScore - MC_serverBD.iKillScore[iParticipant])
		MC_serverBD.iKillScore[iParticipant] = MC_Playerbd[iParticipant].iKillScore
		PRINTLN("[Players][Participant: ", iParticipant, "]  COUNT_PLAYER_SCORES -  team: ", iTeam, " iKillScore increased to: ", MC_serverBD.iTeamKillScore[iTeam])
	ENDIF

	// Update the headshots counts if the players have a different one to the server
	IF MC_serverBD.iNumHeadshots[iParticipant] < MC_Playerbd[iParticipant].iNumHeadshots
		SET_TEAM_HEADSHOTS(iTeam, GET_TEAM_HEADSHOTS(iTeam) + (MC_Playerbd[iParticipant].iNumHeadshots - MC_serverBD.iNumHeadshots[iParticipant]))
		MC_serverBD.iNumHeadshots[iParticipant] = MC_Playerbd[iParticipant].iNumHeadshots
		PRINTLN("[Players][Participant: ", iParticipant, "]  COUNT_PLAYER_SCORES -  team: ", iTeam, " headshots increased to: ", GET_TEAM_HEADSHOTS(iTeam))
	ENDIF
	
ENDPROC		
		
FUNC BOOL SHOULD_RUN_FULL_SCORE_COUNT_THIS_FRAME()
	
	IF IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_SINGLE_FRAME_COUNT_SCORES_ENABLED)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_SUDDEN_DEATH_REQUEST_PLAYER_SCORE_UPDATE)
		RETURN TRUE
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_PerformInitialScoreCount)
	AND NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_INITIAL_SCORE_COUNT_DONE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL PROCESS_PLAYER_TEAM_SWAP()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLOBBER_PLAYER_ON_TEAM_SWAP)
		//No Team Swaps
		RETURN FALSE
	ENDIF

	IF bIsLocalPlayerHost		
		INT iPart
		
		PRINTLN("[PLAYER_LOOP] - PROCESS_PLAYER_TEAM_SWAP")
		FOR iPart = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
			IF IS_BIT_SET(MC_serverBD_4.iTeamSwapLockFlag, iPart) 
				IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
					CLEAR_BIT(MC_serverBD_4.iTeamSwapLockFlag, iPart)
					MC_serverBD.iHostRefreshDpadValue++
					PRINTLN("[MMacK][TeamSwaps] iTeamSwapLockFlag = CLEAR 4", iPart)
					PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Processing Team Swap - PLAYER GONE")
				ENDIF
				
				IF MC_PlayerBD[iPart].bSwapTeamStarted
					PRINTLN("[MMacK][TeamSwaps] iTeamSwapLockFlag Started for ", iPart)
				ENDIF
				
				IF MC_PlayerBD[iPart].bSwapTeamFinished
					CLEAR_BIT(MC_serverBD_4.iTeamSwapLockFlag, iPart)
					MC_serverBD.iHostRefreshDpadValue++
					PRINTLN("[MMacK][TeamSwaps] iTeamSwapLockFlag finished for ", iPart)
					PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Processing Team Swap - PLAYER FINISHED SWAP")
				ENDIF
			ENDIF
		ENDFOR
		
		IF MC_serverBD_4.iTeamSwapLockFlag != 0
			PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Processing Team Swap - We got swappers")
			SET_BIT(MC_serverBD.iServerBitSet, SBBOOL_SKIP_PARTICIPANT_UPDATE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_MISSION_TEAM_COUNTS(MC_ServerBroadcastData_LB &serverBDpassed_LB, MC_ServerBroadcastData &serverBDpassed, INT &iTeamWinCount, INT &iTeam2Count, INT &iTeam3Count)		//	, MC_PlayerBroadcastData &playerBDPassed[], INT &iTeam4Count

	INT unsortedTeamCounts[FMMC_MAX_TEAMS]
	
	INT loop = 0
	WHILE loop < FMMC_MAX_TEAMS
		unsortedTeamCounts[loop] = 0
		loop++
	ENDWHILE
	
	iTeamWinCount = 0
	iTeam2Count = 0
	iTeam3Count = 0
	
	INT i, iPlayerTeam
	PRINTLN("[PLAYER_LOOP] - GET_MISSION_TEAM_COUNTS")
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF SHOULD_PLAYER_BE_DISPLAYED_ON_MISSION_DPAD_DOWN_MENU(i, serverBDpassed_LB)	//	, playerBDPassed
			iPlayerTeam = serverBDpassed_LB.sleaderboard[i].iTeam

			IF iPlayerTeam <> -1
				IF (iPlayerTeam >= 0) AND (iPlayerTeam < FMMC_MAX_TEAMS)
					unsortedTeamCounts[iPlayerTeam]++
				ELSE
					SCRIPT_ASSERT("[RCC MISSION] GET_MISSION_TEAM_COUNTS - player team index is out of the range 0 to 3")
				ENDIF					
			ENDIF
		ENDIF
	ENDREPEAT

	IF serverBDpassed.iWinningTeam <> -1
		iTeamWinCount = unsortedTeamCounts[serverBDpassed.iWinningTeam]
	ENDIF
	IF serverBDpassed.iSecondTeam <> -1
		iTeam2Count = unsortedTeamCounts[serverBDpassed.iSecondTeam]
	ENDIF
	IF serverBDpassed.iThirdTeam <> -1
		iTeam3Count = unsortedTeamCounts[serverBDpassed.iThirdTeam]
	ENDIF
	
	PRINTLN("[RCC MISSION] [CS_DPAD] GET_MISSION_TEAM_COUNTS: Ordered Team Counts ", serverBDpassed.iWinningTeam, " has ", iTeamWinCount, ", ", serverBDpassed.iSecondTeam, " has ", iTeam2Count)
	PRINTLN("[RCC MISSION] [CS_DPAD] ", serverBDpassed.iThirdTeam, " has ", iTeam3Count)	//	, ", ", serverBDpassed.iFourthTeam, " has ", iTeam4Count)
	
ENDPROC

FUNC BOOL GET_TEAM_VEHICLE_FOR_SPEED_CHECK(INT iTeam, VEHICLE_INDEX &returnVeh, INT iSpecificVehIndex)
	
	iExplodeDriverPart = -1
	
	//Check yo'self:
	IF iTeam = MC_playerBD[iPartToUse].iteam
	AND IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
	AND iSpecificVehIndex = -1
		returnVeh = GET_VEHICLE_PED_IS_USING(PlayerPedToUse)
		iExplodeDriverPart = iPartToUse
		RETURN TRUE
	ENDIF
	
	
	//Check everyone else:
	INT iPart
	INT iPlayersChecked
	INT iSlowestPart = -1
	FLOAT fSlowestSpeed = 9999
	
	FLOAT fTempSpeed
	PLAYER_INDEX tempPlayer
	VEHICLE_INDEX tempVeh
	VEHICLE_INDEX tempTrailerVeh
	PRINTLN("[PLAYER_LOOP] - GET_TEAM_VEHICLE_FOR_SPEED_CHECK 2")
	FOR iPart = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
		IF (MC_playerBD[iPart].iteam = iTeam)
		AND (NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR))
		AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
			
			iPlayersChecked++
			
			tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
			
			IF IS_NET_PLAYER_OK(tempplayer)
				PED_INDEX tempPed = GET_PLAYER_PED(tempplayer)
				
				IF IS_PED_IN_ANY_VEHICLE(tempPed)
				
					tempVeh = GET_VEHICLE_PED_IS_USING(tempPed)
					
					IF IS_ENTITY_ALIVE(tempVeh)
						//Get the player moving slowest on this team:
						fTempSpeed = ABSF(GET_ENTITY_SPEED(tempVeh))
					ENDIF
					
					IF iSpecificVehIndex != -1
						PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK - Checking a specific vehicle")
						
						
						
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iSpecificVehIndex])
						
							IF IS_VEHICLE_ATTACHED_TO_TRAILER(tempVeh)
								GET_VEHICLE_TRAILER_VEHICLE(tempVeh, tempTrailerVeh)
								PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  - Vehicle has a trailer attached")
							ENDIF
							
							IF tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iSpecificVehIndex])
							OR tempTrailerVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iSpecificVehIndex])
								PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  - Vehicle is valid")
								
								IF tempTrailerVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iSpecificVehIndex])
									returnVeh = tempTrailerVeh
									PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  DEBUG - returning trailer: ",NATIVE_TO_INT(returnVeh))
								ELSE
									returnVeh = tempVeh
									PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  - returning drivable vehicle: ", NATIVE_TO_INT(returnVeh))
								ENDIF
								PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  - MODEL NAME: ", GET_CREATOR_NAME_FOR_VEHICLE_MODEL(GET_ENTITY_MODEL(returnVeh)))
								iExplodeDriverPart = iPart
								PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  - returning part: ", iPart)
								

								RETURN TRUE
							ENDIF
						ENDIF
						
					ELIF fTempSpeed < fSlowestSpeed
						iSlowestPart = iPart
						fSlowestSpeed = fTempSpeed
					ENDIF
				ENDIF
			ENDIF
			
			IF iPlayersChecked >= MC_serverBD.iNumberOfPlayingPlayers[iTeam]
				PRINTLN("[PLAYER_LOOP] - GET_TEAM_VEHICLE_FOR_SPEED_CHECK")
				BREAKLOOP
				IF iSpecificVehIndex != -1
					PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK - Checking a specific vehicle")
					
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iSpecificVehIndex])
					
						VEHICLE_INDEX viSpecificVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iSpecificVehIndex])
						
						IF IS_ENTITY_ALIVE(viSpecificVeh)
							IF IS_VEHICLE_A_TRAILER(viSpecificVeh)
								IF IS_ENTITY_ALIVE(GET_ENTITY_ATTACHED_TO(viSpecificVeh))
									tempVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO(viSpecificVeh))
									tempTrailerVeh = viSpecificVeh
								ENDIF
							ENDIF
						ENDIF
										
						IF tempTrailerVeh = viSpecificVeh
						OR tempVeh = viSpecificVeh
							PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  - Vehicle is valid")
							
							IF tempTrailerVeh = viSpecificVeh
								returnVeh = tempTrailerVeh
								PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  DEBUG - returning trailer: ",NATIVE_TO_INT(returnVeh))
							ELSE
								returnVeh = tempVeh
								PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  - returning drivable vehicle: ", NATIVE_TO_INT(returnVeh))
							ENDIF
							PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  - MODEL NAME: ", GET_CREATOR_NAME_FOR_VEHICLE_MODEL(GET_ENTITY_MODEL(returnVeh)))

							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDFOR
	
	IF iSlowestPart != -1
		returnVeh = GET_VEHICLE_PED_IS_USING(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSlowestPart))))
		iExplodeDriverPart = iSlowestPart
		RETURN TRUE
	ENDIF
	
	IF iSpecificVehIndex != -1
		PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK - Checking a specific vehicle")
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iSpecificVehIndex])
		
			PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  - Vehicle is valid, returning")
			
			returnVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iSpecificVehIndex])

			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Leaderboards -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_TEAM_FINISH_POSITION_FOR_LBD(INT iteam, INT iNumTeams)
	
	IF iNumTeams = 4
		IF MC_serverBD.iWinningTeam = iteam
			RETURN 1
		ELIF MC_serverBD.iSecondTeam = iteam
			RETURN 2
		ELIF MC_serverBD.iThirdTeam = iteam
			RETURN 3
		ELSE
			RETURN 4
		ENDIF
	ELIF iNumTeams = 3
		IF MC_serverBD.iWinningTeam = iteam
			RETURN 1
		ELIF MC_serverBD.iSecondTeam = iteam
			RETURN 2
		ELSE
			RETURN 3
		ENDIF
	ELIF iNumTeams = 2
		IF MC_serverBD.iWinningTeam = iteam
			RETURN 1
		ELSE
			RETURN 2
		ENDIF
	ELSE
		RETURN 1
	ENDIF

	RETURN 1
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Objectives -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL ARE_ANY_VALID_PLAYERS_DRIVING_DELIVERY_VEHICLE()
	
	INT iPart
	PRINTLN("[PLAYER_LOOP] - ARE_ANY_VALID_PLAYERS_DRIVING_DELIVERY_VEHICLE")
	FOR iPart = 0 TO NUM_NETWORK_PLAYERS-1
		PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iPart)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
			IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
			AND MC_PlayerBD[iPart].iTeam = MC_PlayerBD[iPartToUse].iTeam
			AND iPartToUse != iPart
				IF MC_playerBD[iPart].iVehNear > -1
					PRINTLN("[RCC MISSION][ModShopProg] - iPart: ", iPart, " MC_playerBD[iPart].iVehNear: ", MC_playerBD[iPart].iVehNear)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_READY_TO_DELIVER_ANY_SPECIFIC_VEHICLE(INT iTeam, PED_INDEX piPlayer)
	INT iLoc
	
	FOR iLoc = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] - 1 //The team array for this does not work
		
		IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]
			IF MC_serverBD_4.iGotoLocationDataRule[iLoc][iTeam] = FMMC_OBJECTIVE_LOGIC_GO_TO
				MODEL_NAMES mnVeh = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].mnVehicleNeeded[iTeam]
				IF mnVeh != DUMMY_MODEL_FOR_SCRIPT
				AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_SpecificVehModelFullBlocker)
					IF IS_PED_IN_ANY_VEHICLE(piPlayer)
						IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(piPlayer)) = mnVeh
							//Returning TRUE because we're in the right vehicle
							RETURN TRUE
						ENDIF
					ENDIF
					
				ELSE
					//Returning TRUE because there's no vehicle set for this rule
					RETURN TRUE
				ENDIF
			ELSE
				//Returning TRUE because this isn't FMMC_OBJECTIVE_LOGIC_GO_TO
				RETURN TRUE 
			ENDIF
		ELSE
			RELOOP
		ENDIF
	ENDFOR
	
	//Returning TRUE because we've looped through all of them and not encountered any issues
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TEAMMATE_READY_TO_DELIVER_SPECIFIC_VEHICLE(INT iTeam)

	IF MC_serverBD.iPriorityLocation[iTeam] != -1
		PLAYER_INDEX tempPlayer
		PARTICIPANT_INDEX tempPart
		INT iparticipant
		MODEL_NAMES mnVeh = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_serverBD.iPriorityLocation[iTeam]].mnVehicleNeeded[iTeam]
		IF mnVeh != DUMMY_MODEL_FOR_SCRIPT
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_serverBD.iPriorityLocation[iTeam]].iLocBS3, ciLoc_BS3_SpecificVehModelFullBlocker)
			PRINTLN("[PLAYER_LOOP] - IS_TEAMMATE_READY_TO_DELIVER_SPECIFIC_VEHICLE")
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
				tempPart = INT_TO_PARTICIPANTINDEX(iparticipant)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
					IF tempPlayer != LocalPlayer
						IF IS_NET_PLAYER_OK(tempPlayer)
							IF MC_playerBD[iparticipant].iteam = iTeam
								PED_INDEX playerPed = GET_PLAYER_PED(tempPlayer)
								
								IF IS_PED_IN_ANY_VEHICLE(playerPed)
								AND (NOT IS_REMOTE_PLAYER_IN_NON_CLONED_VEHICLE(tempPlayer)) // Need to make sure we have this vehicle cloned on our machine, or the checks below won't work
									
									VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_USING(playerPed)
									
									IF DOES_ENTITY_EXIST(tempVeh) // url:bugstar:2214433
									AND GET_ENTITY_MODEL(tempVeh) = mnVeh
										RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_CARRYING_OBJECT_FOR_OVERHEAD(INT iPart, INT iTeam, INT iRule)
	
	IF MC_playerBD[iPart].iObjCarryCount > 0
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjRequiredForRulePass[iRule] > -1
	AND MC_serverBD.iObjCarrier[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjRequiredForRulePass[iRule]] = iPart
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_CUSTOM_OBJECT_CARRIER_PLAYER(FMMC_PLAYER_STATE &sPlayerState, INT iTeam)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_CarrierAlerts)
		EXIT
	ENDIF
	
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]

	IF !sPlayerState.bParticipantActive
		EXIT
	ENDIF
	
	IF sPlayerState.iIndex = iPartToUse
		EXIT
	ENDIF
	
	IF iRule < FMMC_MAX_RULES
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitSetSix[iRule], ciBS_RULE6_GET_DELIVER_ICON)
			
			IF IS_PLAYER_CARRYING_OBJECT_FOR_OVERHEAD(sPlayerState.iIndex, iTeam, iRule)
				IF NOT IS_PLAYER_USING_CUSTOM_SPRITE_BLIP(sPlayerState.piPlayerIndex)
					SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(sPlayerState.piPlayerIndex, RADAR_TRACE_PACKAGE, TRUE)
				ENDIF
				IF IS_MP_GAMER_TAG_ACTIVE(sPlayerState.iIndex)
					SET_ICON_VISABILITY(sPlayerState.iIndex, MP_TAG_PACKAGES, TRUE)
				ENDIF
				
			ELSE
				IF IS_PLAYER_USING_CUSTOM_SPRITE_BLIP(sPlayerState.piPlayerIndex)
					SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(sPlayerState.piPlayerIndex, RADAR_TRACE_PACKAGE, FALSE)
					IF IS_MP_GAMER_TAG_ACTIVE(sPlayerState.iIndex)
						SET_ICON_VISABILITY(sPlayerState.iIndex, MP_TAG_PACKAGES, FALSE)
					ENDIF
				ENDIF
			ENDIF
		
		ENDIF
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Special Player States & Abilities -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### Juggernaut, Beast etc.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//Remove hud and local player stuff
PROC PROCESS_TEAM_REGEN()
	INT iTeam = MC_PlayerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	BOOL bIncreasedRegen = FALSE
	BOOL bDecreasedDamage = FALSE
	BOOL bShowingHUD = FALSE
	
	IF iRule < FMMC_MAX_RULES
		IF (g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sRegenStruct[ iRule ].fDistance > 0.0
			OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_TEAM_REGEN_FRIENDLY))
		AND NOT (ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
			AND IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES))	//Don't process team regen in sudden death
		AND bPedToUseOk
			PRINTLN("[JS][TEAMREGEN] - PROCESS_TEAM_REGEN - Distance > 0.0 (", g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sRegenStruct[ iRule ].fDistance,") AND player is OK")
			INT iRegenLoop
			PRINTLN("[PLAYER_LOOP] - PROCESS_TEAM_REGEN")
			FOR iRegenLoop = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() -1
				IF iRegenLoop != iPartToUse
					PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iRegenLoop)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
						BOOL bFriendlyTeamRegen = FALSE
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_TEAM_REGEN_FRIENDLY)
							IF DOES_TEAM_LIKE_TEAM(iTeam, MC_PlayerBD[iRegenloop].iteam)
								PRINTLN("[JS][TEAMREGEN] Friendly team and distance = ", g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iRegenloop].iteam ].sRegenStruct[ iRule ].fDistance)
								bFriendlyTeamRegen = TRUE
							ENDIF
						ENDIF
						IF iTeam = MC_PlayerBD[iRegenloop].iteam
						OR (bFriendlyTeamRegen 
							AND g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iRegenloop].iteam ].sRegenStruct[ iRule ].fDistance > 0.0)
							PRINTLN("[JS][TEAMREGEN] - PROCESS_TEAM_REGEN - Player ", iRegenLoop ," is on my team/friendly (",MC_PlayerBD[iRegenloop].iteam ," , ", iTeam,")")
							PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
							IF NOT IS_PLAYER_SPECTATING(tempPlayer)
								IF IS_ENTITY_DEAD(GET_PLAYER_PED(tempPlayer))
								AND g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iRegenloop].iteam ].sRegenStruct[ iRule ].fDistance > 0.0
									IF DOES_BLIP_EXIST(biRegenBlip)
										REMOVE_BLIP(biRegenBlip)
										PRINTLN("[JS][TEAMREGEN] - PROCESS_TEAM_REGEN - Player dead, removing blip")
									ENDIF
								ELIF VDIST2(GET_ENTITY_COORDS(PlayerPedToUse), GET_ENTITY_COORDS(GET_PLAYER_PED(tempPlayer))) < POW(g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iRegenloop].iteam ].sRegenStruct[ iRule ].fDistance, 2)
									SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(PlayerToUse, TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iRegenloop].iteam ].sRegenStruct[ iRule ].iRegenRate))
									IF GET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(PlayerToUse) != (TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iRegenloop].iteam ].sRegenStruct[ iRule ].iMaxRegen) / 100)
										SET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(PlayerToUse, (TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iRegenloop].iteam ].sRegenStruct[ iRule ].iMaxRegen) / 100))
									ENDIF
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iRegenloop].iteam].iRuleBitsetSeven[iRule], ciBS_RULE7_TEAM_REGEN_DAMAGE)
										SET_PLAYER_WEAPON_DEFENSE_MODIFIER(LocalPlayer, FREEMODE_PLAYER_WEAPON_DEFENSE/2)
										SET_PLAYER_MELEE_WEAPON_DEFENSE_MODIFIER(LocalPlayer, FREEMODE_PLAYER_MELEE_WEAPON_DEFENSE/2)
										PRINTLN("[JS][TEAMREGEN] - PROCESS_TEAM_REGEN - Denfence modifier applied")
										bDecreasedDamage = TRUE
									ENDIF
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iRegenloop].iteam ].iRuleBitSetSix[ iRule ], ciBS_RULE6_TEAM_REGEN_HUD)
										PROCESS_REGEN_HUD(TRUE, tempPlayer, MC_PlayerBD[iRegenloop].iteam, iRule)
										bShowingHUD = TRUE
									ELSE
										IF DOES_BLIP_EXIST(biRegenBlip)
											REMOVE_BLIP(biRegenBlip)
										ENDIF
									ENDIF
									bIncreasedRegen = TRUE
									PRINTLN("[JS][TEAMREGEN] - PROCESS_TEAM_REGEN - Player in range")
								ELSE
									PRINTLN("[JS][TEAMREGEN] - PROCESS_TEAM_REGEN - Player not in range")
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iRegenloop].iteam ].iRuleBitSetSix[ iRule ], ciBS_RULE6_TEAM_REGEN_HUD)
										PROCESS_REGEN_HUD(FALSE, tempPlayer, MC_PlayerBD[iRegenloop].iteam, iRule)
										bShowingHUD = TRUE
									ELSE
										IF DOES_BLIP_EXIST(biRegenBlip)
											REMOVE_BLIP(biRegenBlip)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			IF bIncreasedRegen = FALSE
				RESET_TEAM_HEALTH_REGEN()
			ENDIF
			IF bDecreasedDamage = FALSE
				RESET_TEAM_DAMAGE_MOD()
			ENDIF
			IF bShowingHUD = FALSE
				IF DOES_BLIP_EXIST(biRegenBlip)
					REMOVE_BLIP(biRegenBlip)
				ENDIF
			ENDIF
		ELSE 
			IF DOES_BLIP_EXIST(biRegenBlip)
				REMOVE_BLIP(biRegenBlip)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_ALL_JUGGERNAUTS()
	INT iPart
	INT iPlayersChecked
	INT iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
	
	PRINTLN("[PLAYER_LOOP] - PROCESS_ALL_JUGGERNAUTS")
	FOR iPart = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
		IF ((NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
				iPlayersChecked++
				IF IS_PARTICIPANT_A_JUGGERNAUT(iPart)
					PED_INDEX juggernautPed = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
					IF NOT IS_ENTITY_DEAD(juggernautPed)
						SET_PED_RESET_FLAG(juggernautPed, PRF_DisableActionMode, TRUE) 
						SET_PED_RESET_FLAG(juggernautPed, PRF_DisableMeleeHitReactions,TRUE)
						IF NOT IS_BIT_SET(iSoundsSetForJuggernaut, iPart)
							SET_PED_USING_ACTION_MODE(juggernautPed, FALSE)
							IF iPart != iPartToUse
								ADD_ENTITY_TO_AUDIO_MIX_GROUP(juggernautPed, "DLC_IE_JN_JN_Player_Group")
							ENDIF
							USE_FOOTSTEP_SCRIPT_SWEETENERS(juggernautPed, TRUE, HASH("DLC_IE_JN_Footstep_Sounds"))
							
							PRINTLN("[JS] PROCESS_ALL_JUGGERNAUTS Part ",iPart," added to mixgroup")
							SET_BIT(iSoundsSetForJuggernaut, iPart)
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(iSoundsSetForJuggernaut, iPart)
						PED_INDEX juggernautPed = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
						IF NOT IS_ENTITY_DEAD(juggernautPed)
							PRINTLN("[JS] PROCESS_ALL_JUGGERNAUTS Part ",iPart," removed from mixgroup")
							CLEAR_BIT(iSoundsSetForJuggernaut, iPart)
							USE_FOOTSTEP_SCRIPT_SWEETENERS(juggernautPed, FALSE, HASH("DLC_IE_JN_Footstep_Sounds"))
							IF iPart != iPartToUse
								REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(juggernautPed)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF iPlayersChecked >= iPlayersToCheck
			BREAKLOOP
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_JUGGERNAUT_EVERY_FRAME()
	PROCESS_ALL_JUGGERNAUTS()
	IF bLocalPlayerOK
		IF IS_PARTICIPANT_A_JUGGERNAUT(iLocalPart)
			IF NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_JUGGERNAUT_SUIT_MOVEMENT_SET)
				PRINTLN("[JS] PROCESS_JUGGERNAUT_EVERY_FRAME - Setting movement")
				SET_PED_MOVEMENT_CLIPSET(LocalPlayerPed, "ANIM_GROUP_MOVE_BALLISTIC")
			    SET_PED_STRAFE_CLIPSET(LocalPlayerPed, "MOVE_STRAFE_BALLISTIC")
			    SET_WEAPON_ANIMATION_OVERRIDE(LocalPlayerPed, HASH("BALLISTIC"))
				SET_BIT(iLocalBoolCheck20, LBOOL20_JUGGERNAUT_SUIT_MOVEMENT_SET)
			ENDIF
			SET_PED_STEALTH_MOVEMENT(LocalPlayerPed, FALSE)
			SET_PLAYER_CAN_USE_COVER(LocalPlayer, FALSE)
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisablePlayerCombatRoll, TRUE)
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableMeleeWeaponSelection, TRUE)
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisablePlayerJumping, TRUE)
			
			IF NOT HAS_NET_TIMER_STARTED(tdJuggernautVFX)
				START_NET_TIMER(tdJuggernautVFX)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(tdJuggernautVFX, ciJUGGERNAUT_VFX_INTERVAL)
					USE_PARTICLE_FX_ASSET("scr_impexp_jug")
					START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_ie_jug_mask_steam", LocalPlayerPed, vJuggernautVFXPos, vJuggernautVFXRot, BONETAG_HEAD)
					REINIT_NET_TIMER(tdJuggernautVFX)
				ENDIF
			ENDIF
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			
			
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_JUGGERNAUT_SUIT_MOVEMENT_SET)
				PRINTLN("[JS] PROCESS_JUGGERNAUT_EVERY_FRAME - Clearing movement")
				SET_PLAYER_CAN_USE_COVER(LocalPlayer, TRUE)
				RESET_PED_MOVEMENT_CLIPSET(LocalPlayerPed)
				RESET_PED_STRAFE_CLIPSET(LocalPlayerPed)
				SET_WEAPON_ANIMATION_OVERRIDE(LocalPlayerPed, HASH("Default"))
				CLEAR_BIT(iLocalBoolCheck20, LBOOL20_JUGGERNAUT_SUIT_MOVEMENT_SET)
			ENDIF
			IF HAS_NET_TIMER_STARTED(tdJuggernautVFX)
				RESET_NET_TIMER(tdJuggernautVFX)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Blips -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_CUSTOM_PLAYER_BLIPS(FMMC_PLAYER_STATE &sPlayerState, INT iTeam)
	
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]

	IF !sPlayerState.bParticipantActive
		EXIT
	ENDIF

	IF IS_PED_IN_ANY_VEHICLE(sPlayerState.piPedIndex)
		VEHICLE_INDEX viPedVeh = GET_VEHICLE_PED_IS_IN(sPlayerState.piPedIndex)
		IF IS_VEHICLE_DRIVEABLE(viPedVeh)
			INT iVehID = IS_ENTITY_A_MISSION_CREATOR_ENTITY(viPedVeh)
			IF iVehID >= 0
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehID].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_USE_SKULL_BLIP)
					IF sPlayerState.iIndex != iPartToUse // Don't do this to yourself
						SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(sPlayerState.piPlayerIndex, RADAR_TRACE_BOUNTY_HIT, TRUE)
					ENDIF
					IF DOES_BLIP_EXIST(biVehBlip[iVehID])
						REMOVE_BLIP(biVehBlip[iVehID])
					ENDIF
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehID].iVehBitsetFour,  ciFMMC_VEHICLE4_VEHICLE_INHERIT_OCCUPIERS_BLIP_COLOUR)
						//need to set this value so we can get custom coloured objective text based on team colour.
						iVehCapTeam = MC_playerBD[sPlayerState.iIndex].iTeam
					ENDIF
					iVehPartIn[sPlayerState.iIndex] = iVehID
					iPartInVeh[iVehID] = sPlayerState.iIndex 
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	//FMMC2020 - this bit is weird - seems to do the same as above but only if the above bit runs?
	IF iVehPartIn[sPlayerState.iIndex] != -1
	AND iPartInVeh[iVehPartIn[sPlayerState.iIndex]] != -1
		#IF IS_DEBUG_BUILD
		IF bDebugBlipPrints
			PRINTLN("[RCC MISSION] PROCESS_CUSTOM_PLAYER_BLIPS iVehPartIn[iPart] =", iVehPartIn[sPlayerState.iIndex])
			
			PRINTLN("[RCC MISSION] PROCESS_CUSTOM_PLAYER_BLIPS iPartInVeh[iVehPartIn[iPart]] =", iPartInVeh[iVehPartIn[sPlayerState.iIndex]])
		ENDIF #ENDIF	
		
		PARTICIPANT_INDEX currentParticipantInVeh = INT_TO_PARTICIPANTINDEX(iPartInVeh[iVehPartIn[sPlayerState.iIndex]])
		IF NETWORK_IS_PARTICIPANT_ACTIVE(currentParticipantInVeh)
			PLAYER_INDEX currentPlayerInVeh = NETWORK_GET_PLAYER_INDEX(currentParticipantInVeh)
			PED_INDEX CurrentPlayerPedInVehIndex = GET_PLAYER_PED(currentPlayerInVeh)

			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehPartIn[sPlayerState.iIndex]])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehPartIn[sPlayerState.iIndex]])
			AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehPartIn[sPlayerState.iIndex]]))
				#IF IS_DEBUG_BUILD
				IF bDebugBlipPrints
					PRINTLN("[RCC MISSION] PROCESS_CUSTOM_PLAYER_BLIPS checking to see if not in veh")
				ENDIF #ENDIF
				
				IF NOT IS_PED_IN_VEHICLE(CurrentPlayerPedInVehIndex, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehPartIn[sPlayerState.iIndex]]), TRUE)
					// Don't do this to yourself
					IF iPartInVeh[iVehPartIn[sPlayerState.iIndex]] != iPartToUse
						SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(currentPlayerInVeh, RADAR_TRACE_BOUNTY_HIT, FALSE)
						#IF IS_DEBUG_BUILD
						IF bDebugBlipPrints
							PRINTLN("[RCC MISSION] PROCESS_CUSTOM_PLAYER_BLIPS deactivating blip for ", sPlayerState.iIndex)
						ENDIF #ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						IF bDebugBlipPrints
							PRINTLN("[RCC MISSION] PROCESS_CUSTOM_PLAYER_BLIPS ", sPlayerState.iIndex ," is not in vehicle but iPart = iPartToUse so not removing blip")
						ENDIF #ENDIF
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehPartIn[sPlayerState.iIndex]].iVehBitsetFour,  ciFMMC_VEHICLE4_VEHICLE_INHERIT_OCCUPIERS_BLIP_COLOUR)
						//Clear this so text goes back to blue for veh
						iVehCapTeam = -1
						#IF IS_DEBUG_BUILD
						IF bDebugBlipPrints
							PRINTLN("[RCC MISSION] PLAYER INSIDE SKULL BLIP VEH - PROCESS_CUSTOM_PLAYER_BLIPS- Setting - iVehCapTeam:" ,iVehCapTeam)
						ENDIF #ENDIF
					ENDIF
					
					iPartInVeh[iVehPartIn[sPlayerState.iIndex]] = -1
					iVehPartIn[sPlayerState.iIndex] = - 1
					#IF IS_DEBUG_BUILD
					IF bDebugBlipPrints
						PRINTLN("[RCC MISSION] PLAYER NOT INSIDE SKULL BLIP VEH - PROCESS_CUSTOM_PLAYER_BLIPS - Removing player blip - TEAM_BLIP_RED_SKULL (team: ",iTeam,") (iPart: ",sPlayerState.iIndex)
					ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	 				

	IF sPlayerState.iIndex = iPartToUse
		EXIT
	ENDIF

	IF g_FMMC_STRUCT.iTeamBlipType[iTeam] = TEAM_BLIP_DEFAULT
	AND g_FMMC_STRUCT.iTeamColourOverride[iTeam] = -1
		EXIT
	ENDIF

	IF !sPlayerState.bPlayerOK
		CLEAR_BIT(iCustomPlayerBlipSetBS, sPlayerState.iIndex)
		EXIT
	ENDIF

	IF IS_BIT_SET(iCustomPlayerBlipSetBS, sPlayerState.iIndex)
		EXIT
	ENDIF
				
	INT iBlipColour = BLIP_COLOUR_DEFAULT
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_1_UsingNewBlipSpriteData+iTeam)
		SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(sPlayerState.piPlayerIndex, INT_TO_ENUM(BLIP_SPRITE, g_FMMC_STRUCT.iTeamBlipType[iTeam]), TRUE)
		EXIT
	ELSE							
		SWITCH g_FMMC_STRUCT.iTeamBlipType[iTeam]
			
			CASE TEAM_BLIP_RED_SKULL
				#IF IS_DEBUG_BUILD
				IF bDebugBlipPrints
					PRINTLN("[RCC MISSION] PROCESS_CUSTOM_PLAYER_BLIPS - (team: ",iTeam,") (iPart: ",sPlayerState.iIndex)
				ENDIF #ENDIF

				IF MC_playerBD[sPlayerState.iIndex].iTeam = iTeam
					#IF IS_DEBUG_BUILD
					IF bDebugBlipPrints
						PRINTLN("[RCC MISSION] PROCESS_CUSTOM_PLAYER_BLIPS - Add custom player blip - TEAM_BLIP_RED_SKULL (team: ",iTeam,") (iPart: ",sPlayerState.iIndex) 
					ENDIF #ENDIF
					SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(sPlayerState.piPlayerIndex, RADAR_TRACE_BOUNTY_HIT, TRUE)
				ELSE
					#IF IS_DEBUG_BUILD
					IF bDebugBlipPrints
						PRINTLN("[RCC MISSION] PROCESS_CUSTOM_PLAYER_BLIPS - Remove custom player blip - TEAM_BLIP_RED_SKULL (team: ",iTeam,") (iPart: ",sPlayerState.iIndex) 
					ENDIF #ENDIF
					SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(sPlayerState.piPlayerIndex, RADAR_TRACE_BOUNTY_HIT, FALSE)
				ENDIF
				
			BREAK
			
			CASE TEAM_BLIP_BEAST
				#IF IS_DEBUG_BUILD
				IF bDebugBlipPrints
					PRINTLN("[RCC MISSION] PROCESS_CUSTOM_PLAYER_BLIPS - (team: ",iTeam,") (iPart: ",sPlayerState.iIndex)
				ENDIF #ENDIF
				
				IF iRule < FMMC_MAX_RULES
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitSetFive[iRule], ciBS_RULE5_ENABLE_BEAST_MODE)
						SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(sPlayerState.piPlayerIndex, RADAR_TRACE_BEAST, TRUE)
						#IF IS_DEBUG_BUILD
						IF bDebugBlipPrints
							PRINTLN("[RCC MISSION] PROCESS_CUSTOM_PLAYER_BLIPS - Add custom player blip - TEAM_BLIP_BEAST (team: ",iTeam,") (iPart: ",sPlayerState.iIndex)
						ENDIF #ENDIF
					ENDIF
				ENDIF
			BREAK

			CASE TEAM_BLIP_HOLDING_PACKAGE
				#IF IS_DEBUG_BUILD
				IF bDebugBlipPrints
					PRINTLN("[RCC MISSION] PROCESS_CUSTOM_PLAYER_BLIPS - (team: ",iTeam,") (iPart: ",sPlayerState.iIndex)
				ENDIF #ENDIF
				IF iRule < FMMC_MAX_RULES
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule],ciBS_RULE4_DISPLAY_ICON_FOR_CARRIER)
						SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(sPlayerState.piPlayerIndex, RADAR_TRACE_RACEFLAG, TRUE)
						#IF IS_DEBUG_BUILD
						IF bDebugBlipPrints
							PRINTLN("[RCC MISSION] PROCESS_CUSTOM_PLAYER_BLIPS - Add custom player blip - TEAM_BLIP_HOLDING_PACKAGE (team: ",iTeam,") (iPart: ",sPlayerState.iIndex)
						ENDIF #ENDIF
					ENDIF
				ENDIF
				
			BREAK
			
			CASE TEAM_BLIP_JUGGERNAUT
			
				#IF IS_DEBUG_BUILD
				IF bDebugBlipPrints
					PRINTLN("[RCC MISSION] PROCESS_CUSTOM_PLAYER_BLIPS - (team: ",iTeam,") (iPart: ",sPlayerState.iIndex)
				ENDIF #ENDIF
				IF iRule < FMMC_MAX_RULES
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2, ciBS2_JUGGERNAUTS)
						SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(sPlayerState.piPlayerIndex, RADAR_TRACE_JUGG, TRUE)
						#IF IS_DEBUG_BUILD
						IF bDebugBlipPrints
							PRINTLN("[RCC MISSION] PROCESS_CUSTOM_PLAYER_BLIPS - Add custom player blip - TEAM_BLIP_JUGGERNAUT (team: ",iTeam,") (iPart: ",sPlayerState.iIndex)
						ENDIF #ENDIF
					ENDIF
				ENDIF
			BREAK

		ENDSWITCH
	ENDIF
	
	//Custom Player Blip colors
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_ALT_PLAYER_BLIPS_COLOURS)
		IF MC_playerBD[sPlayerState.iIndex].iTeam != iTeam
			SET_CUSTOM_BLIP_ALPHA_FOR_PLAYER(sPlayerState.piPlayerIndex,170,TRUE)
			#IF IS_DEBUG_BUILD
			IF bDebugBlipPrints
				PRINTLN("[RCC MISSION][AW CUSTOM BLIP] ciUSE_ALT_PLAYER_BLIPS_COLOURS - blip set to 170 alpha ")
			ENDIF #ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.iTeamColourOverride[iTeam] != -1
		iBlipColour = GET_BLIP_COLOUR_FROM_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, PlayerToUse))
	ENDIF
	
	IF iBlipColour != BLIP_COLOUR_DEFAULT
		SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(sPlayerState.piPlayerIndex, iBlipColour, TRUE)
	ENDIF
							
	SET_BIT(iCustomPlayerBlipSetBS, sPlayerState.iIndex)

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Zones ----------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC PROCESS_TRACKING_ALL_PLAYER_IN_ZONE_COUNTS(FMMC_PLAYER_STATE &sPlayerState)
	IF IS_BIT_SET(MC_playerBD[sPlayerState.iIndex].iZoneIsTriggeredBS_Networked, iZoneTriggeringPlayerCount_StaggeredIterator)
		iZoneTriggeringPlayerCount[iZoneTriggeringPlayerCount_StaggeredIterator]++
		IF MC_playerBD[sPlayerState.iIndex].iTeam >= 0 AND MC_playerBD[sPlayerState.iIndex].iTeam < FMMC_MAX_TEAMS
			iZoneTriggeringPlayerCountByTeam[iZoneTriggeringPlayerCount_StaggeredIterator][MC_playerBD[sPlayerState.iIndex].iTeam]++
		ENDIF
		SET_BIT(iCachedZoneIsTriggeredNetworkedBS_Temp, iZoneTriggeringPlayerCount_StaggeredIterator)
		
		IF NOT IS_BIT_SET(iParticipantsTriggeringZoneBS[iZoneTriggeringPlayerCount_StaggeredIterator], sPlayerState.iIndex)
			SET_BIT(iParticipantsTriggeringZoneBS[iZoneTriggeringPlayerCount_StaggeredIterator], sPlayerState.iIndex)
			PRINTLN("[Zones_SPAM][Zone_SPAM ", iZoneTriggeringPlayerCount_StaggeredIterator, "] PROCESS_TRACKING_ALL_PLAYER_IN_ZONE_COUNTS | Noting that Participant ", sPlayerState.iIndex, " is triggering Zone ", iZoneTriggeringPlayerCount_StaggeredIterator)
		ENDIF
	ELSE
		IF IS_BIT_SET(iParticipantsTriggeringZoneBS[iZoneTriggeringPlayerCount_StaggeredIterator], sPlayerState.iIndex)
			CLEAR_BIT(iParticipantsTriggeringZoneBS[iZoneTriggeringPlayerCount_StaggeredIterator], sPlayerState.iIndex)
			PRINTLN("[Zones_SPAM][Zone_SPAM ", iZoneTriggeringPlayerCount_StaggeredIterator, "] PROCESS_TRACKING_ALL_PLAYER_IN_ZONE_COUNTS | Noting that Participant ", sPlayerState.iIndex, " is no longer triggering Zone ", iZoneTriggeringPlayerCount_StaggeredIterator)
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Interact-With --------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC PROCESS_PLAYER_INTERACT_WITH_PROP_RENDERTARGET__RETRO_FINGERPRINT_HACK(FMMC_PLAYER_STATE &sPlayerState, OBJECT_INDEX oiSpawnedProp)

	UNUSED_PARAMETER(sPlayerState)
	UNUSED_PARAMETER(oiSpawnedProp)
	
	IF IS_PHONE_ONSCREEN()
		PRINTLN("[InteractWith] PROCESS_PLAYER_INTERACT_WITH_PROP_RENDERTARGET | Exiting early because our own phone is on screen")
		EXIT
	ENDIF

	REQUEST_STREAMED_TEXTURE_DICT("MPFClone_Retro_BACKGROUND")
	IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPFClone_Retro_BACKGROUND")
		IF sInteractWithVars.iInteractWith_RenderTargetIDs[ciInteractWith_RenderTarget_PhoneHackA] = -1
			GET_MOBILE_PHONE_RENDER_ID(sInteractWithVars.iInteractWith_RenderTargetIDs[ciInteractWith_RenderTarget_PhoneHackA])
			PRINTLN("[InteractWith] PROCESS_PLAYER_INTERACT_WITH_PROP_RENDERTARGET | iInteractWith_RenderTargetIDs[0] is now ", sInteractWithVars.iInteractWith_RenderTargetIDs[ciInteractWith_RenderTarget_PhoneHackA])
		ELSE
			SET_TEXT_RENDER_ID(sInteractWithVars.iInteractWith_RenderTargetIDs[ciInteractWith_RenderTarget_PhoneHackA])
			DRAW_SPRITE("MPFClone_Retro_BACKGROUND", "FINGERPRINT_HACKING_MINIGAME_BACKGROUND", 0.5, 0.5, 1, 1, 90.0, 0, 255, 0, 255)
			SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID()) // Return the render index back to what it was before this
		ENDIF
	ELSE
		PRINTLN("[InteractWith] PROCESS_PLAYER_INTERACT_WITH_PROP_RENDERTARGET | Requesting MPFClone_Retro_BACKGROUND")
	ENDIF
	
	SET_BIT(sInteractWithVars.iInteractWith_RenderTargetIDUsedThisFrameBS, ciInteractWith_RenderTarget_PhoneHackA)
ENDPROC

PROC PROCESS_PLAYER_INTERACT_WITH_PROP_RENDERTARGET(FMMC_PLAYER_STATE &sPlayerState, OBJECT_INDEX oiSpawnedProp)
	UNUSED_PARAMETER(sPlayerState)
	
	IF GET_ENTITY_MODEL(oiSpawnedProp) = prop_phone_ing
		PROCESS_PLAYER_INTERACT_WITH_PROP_RENDERTARGET__RETRO_FINGERPRINT_HACK(sPlayerState, oiSpawnedProp)
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_INTERACT_WITH_STAGGERED_PROP_RENDERTARGET_CLEANUP()
	IF sInteractWithVars.iInteractWith_RenderTargetIDs[ciInteractWith_RenderTarget_PhoneHackA] > -1
	AND NOT IS_BIT_SET(sInteractWithVars.iInteractWith_RenderTargetIDUsedThisFrameBS, ciInteractWith_RenderTarget_PhoneHackA)
		PRINTLN("[InteractWith] PROCESS_PLAYER_INTERACT_WITH_STAGGERED_PROP_RENDERTARGET_CLEANUP | Setting MPFClone_Retro_BACKGROUND as no longer needed")
		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPFClone_Retro_BACKGROUND")
		sInteractWithVars.iInteractWith_RenderTargetIDs[ciInteractWith_RenderTarget_PhoneHackA] = -1
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_INTERACT_WITH_PROPS_STAGGERED(FMMC_PLAYER_STATE &sPlayerState)

	OBJECT_INDEX oiSpawnedProp = GET_INTERACT_WITH_SPAWNED_PROP(sInteractWithVars.iInteractWith_RemotePlayerPropStaggeredIndex, sPlayerState.iIndex)
	
	// Cleanup if the player leaves
	IF sPlayerState.bParticipantActive = FALSE
		IF DOES_ENTITY_EXIST(oiSpawnedProp)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(oiSpawnedProp)
				IF IS_OBJECT_AN_INTERACT_WITH_PERSISTENT_PROP(oiSpawnedProp)
					PRINTLN("[InteractWith] PROCESS_PLAYER_INTERACT_WITH_PROPS | Choosing not to clean up participant ", sPlayerState.iIndex, "'s spawned prop ", sInteractWithVars.iInteractWith_RemotePlayerPropStaggeredIndex, " because it's also a persistent prop")
				ELSE
					PRINTLN("[InteractWith] PROCESS_PLAYER_INTERACT_WITH_PROPS | Cleaning up participant ", sPlayerState.iIndex, "'s spawned prop ", sInteractWithVars.iInteractWith_RemotePlayerPropStaggeredIndex)
					DELETE_OBJECT(oiSpawnedProp)
				ENDIF
			ELSE
				NETWORK_REQUEST_CONTROL_OF_ENTITY(oiSpawnedProp)
				PRINTLN("[InteractWith] PROCESS_PLAYER_INTERACT_WITH_PROPS | Trying to clean up participant ", sPlayerState.iIndex, "'s spawned prop ", sInteractWithVars.iInteractWith_RemotePlayerPropStaggeredIndex, " but we don't have control")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_INTERACT_WITH_EXISTING_PROPS_EVERY_FRAME(FMMC_PLAYER_STATE &sPlayerState)
	INT iSpawnedProp
	FOR iSpawnedProp = 0 TO MC_playerBD_1[sPlayerState.iIndex].iInteractWith_NumSpawnedProps - 1
		IF NETWORK_DOES_NETWORK_ID_EXIST(GET_INTERACT_WITH_SPAWNED_PROP_NET_ID(iSpawnedProp, sPlayerState.iIndex))
			OBJECT_INDEX oiSpawnedProp = GET_INTERACT_WITH_SPAWNED_PROP(iSpawnedProp, sPlayerState.iIndex)
			
			// Called here so that we can apply render targets to our own props as well
			PROCESS_PLAYER_INTERACT_WITH_PROP_RENDERTARGET(sPlayerState, oiSpawnedProp)
			
			IF GET_ENTITY_MODEL(oiSpawnedProp) = GET_INTERACT_WITH_BAG_MODEL_TO_USE()
				PRELOAD_DUFFEL_BAG_HEIST_GEAR(sPlayerState.piPedIndex, sPlayerState.iIndex)
				
				IF IS_BIT_SET(iInteractWith_SwapOutParticipantPropBagBS, sPlayerState.iIndex)
					APPLY_DUFFEL_BAG_HEIST_GEAR(sPlayerState.piPedIndex, sPlayerState.iIndex)
					
					IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sPlayerState.piPedIndex)
						SET_ENTITY_ALPHA(oiSpawnedProp, 0, FALSE)
						CLEAR_BIT(iInteractWith_SwapOutParticipantPropBagBS, sPlayerState.iIndex)
					ENDIF
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF bInteractWithDebug
				TEXT_LABEL_63 tlVisualDebug = "R SP "
				tlVisualDebug += iSpawnedProp
				DRAW_DEBUG_TEXT(tlVisualDebug, GET_ENTITY_COORDS(oiSpawnedProp), 255, 255, 255, 255)
				DRAW_DEBUG_LINE(GET_ENTITY_COORDS(LocalPlayerPed), GET_ENTITY_COORDS(oiSpawnedProp))
			ENDIF
			#ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_PLAYER_INTERACT_WITH_PROPS(FMMC_PLAYER_STATE &sPlayerState)
	
	PROCESS_PLAYER_INTERACT_WITH_EXISTING_PROPS_EVERY_FRAME(sPlayerState)
	
	PROCESS_PLAYER_INTERACT_WITH_PROPS_STAGGERED(sPlayerState)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Wanted -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC BOOL SHOULD_ANY_PLAYERS_ON_TEAM_BE_CONSIDERED_WANTED(INT iTeamToCheck)
	
	IF NOT IS_TEAM_ACTIVE(iTeamToCheck)
		RETURN FALSE
	ENDIF
	
	INT iPart = -1
	DO_PARTICIPANT_LOOP_FLAGS eFlags = GET_PARTICIPANT_LOOP_FLAGS_TO_CHECK_SINGLE_TEAM(iTeamToCheck)
	WHILE DO_PARTICIPANT_LOOP(iPart, eFlags)
		IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet4, PBBOOL4_TREAT_AS_WANTED)
			PRINTLN("SHOULD_ANY_PLAYERS_ON_TEAM_BE_CONSIDERED_WANTED - Participant ", iPart, " is considered Wanted!")
			RETURN TRUE
		ENDIF
	ENDWHILE
	
	RETURN FALSE
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Spawn Groups
// ##### Description: 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
PROC PROCESS_SPAWN_GROUP_DATA_RESET_SERVER()

	IF NOT IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iMiscBS, ciRandomSpawnGroupBS_RequiresReset)
	AND IS_BIT_SET(iLocalBoolCheck34, LBOOL34_SPAWN_GROUP_NEW_RESET_REQUEST_READY)
		IF IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iMiscBS, ciRandomSpawnGroupBS_RequestResetPending)
			PRINTLN("[SpawnGroups] - PROCESS_SPAWN_GROUP_DATA_RESET_SERVER - Clearing ciRandomSpawnGroupBS_RequestResetPending and Setting ciRandomSpawnGroupBS_RequiresReset")
			SET_BIT(MC_serverBD_4.rsgSpawnSeed.iMiscBS, ciRandomSpawnGroupBS_RequiresReset)
			CLEAR_BIT(MC_serverBD_4.rsgSpawnSeed.iMiscBS, ciRandomSpawnGroupBS_RequestResetPending)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck34, LBOOL34_SPAWN_GROUP_CLEAR_REQUEST_READY)
	AND IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iMiscBS, ciRandomSpawnGroupBS_RequiresReset)
		PRINTLN("[SpawnGroups] - PROCESS_SPAWN_GROUP_DATA_RESET_SERVER - Clearing ciRandomSpawnGroupBS_RequiresReset")
		CLEAR_BIT(MC_serverBD_4.rsgSpawnSeed.iMiscBS, ciRandomSpawnGroupBS_RequiresReset)
	ENDIF	
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Host Only -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_GANG_CHASE_PLAYER_CONDITIONS(PED_INDEX piPlayerPed, INT iParticipant)
	
	IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_CHASE_TARGET)
		//Player isn't a potential Gang Chase Target - No need to process
		EXIT
	ENDIF
	
	INT iTeam = MC_PlayerBD[iParticipant].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD_1.iGangChaseAreaBitset[iTeam], iRule)
		//Gang Chase conditions already fulfilled
		EXIT
	ENDIF
	
	IF IS_PED_INJURED(piPlayerPed)
		EXIT
	ENDIF
	
	VECTOR vPos1 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vGangTriggerPos1[iRule]
	VECTOR vPos2 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vGangTriggerPos2[iRule]
	FLOAT fWidth = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fGangTriggerWidth[iRule]	
	
	SWITCH g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupAreaType[iRule]
		
		CASE ciGANGCHASE_AREA_OFF
				
			PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_PLAYER_CONDITIONS - Participant: ", iParticipant, " is a potential target. No area conditions - Enabling Gang Chase.")
			SET_BIT(MC_serverBD_1.iGangChaseAreaBitset[iTeam], iRule)
				
		BREAK
		
		CASE ciGANGCHASE_INSIDE_AREA
		
			IF IS_ENTITY_IN_ANGLED_AREA(piPlayerPed, vPos1, vPos2, fWidth)
				PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_PLAYER_CONDITIONS - Participant: ", iParticipant, " is a potential target and inside the area - Enabling Gang Chase.")
				SET_BIT(MC_serverBD_1.iGangChaseAreaBitset[MC_PlayerBD[iParticipant].iteam],MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iParticipant].iteam])
			ENDIF
		
		BREAK
		
		CASE ciGANGCHASE_OUTSIDE_AREA
					
			IF NOT IS_ENTITY_IN_ANGLED_AREA(piPlayerPed, vPos1, vPos2, fWidth)
				PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_PLAYER_CONDITIONS - Participant: ", iParticipant, " is a potential target and outside the area - Enabling Gang Chase.")
				SET_BIT(MC_serverBD_1.iGangChaseAreaBitset[MC_PlayerBD[iParticipant].iteam],MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iParticipant].iteam])
			ENDIF
			
		BREAK
	
	ENDSWITCH

ENDPROC
		
FUNC INT GET_PLAYER_SCORE_FOR_LEADERBOARD(INT iPart)
	
	IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_TIME
		
		INT iTime
		
		IF HAS_NET_TIMER_STARTED(MC_Playerbd[iPart].tdMissionTime)
			iTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_Playerbd[iPart].tdMissionTime)
		ELSE
			iTime = MC_Playerbd[iPart].iMissionEndTime
		ENDIF
		
		RETURN iTime * -1
		
	ENDIF
	
	RETURN MC_serverBD.iPlayerScore[iPart] 
	
ENDFUNC
				
FUNC BOOL IS_PLAYER_BEATING_PLAYER_FOR_LEADERBOARD(INT iPart0, INT iPart1)
	
	//Score
	INT iScore0 = GET_PLAYER_SCORE_FOR_LEADERBOARD(iPart0)
	INT iScore1 = GET_PLAYER_SCORE_FOR_LEADERBOARD(iPart1)
	IF iScore0 > iScore1
		RETURN TRUE
	ELIF iScore0 < iScore1
		RETURN FALSE
	ENDIF
	
	//Kills
	INT iKills0 = MC_PlayerBD[iPart0].iNumPlayerKills + MC_PlayerBD[iPart0].iNumPedKills
	INT iKills1 = MC_PlayerBD[iPart1].iNumPlayerKills + MC_PlayerBD[iPart1].iNumPedKills
	IF iKills0 > iKills1
		RETURN TRUE
	ELIF iKills0 < iKills1
		RETURN FALSE
	ENDIF
	
	//Deaths
	IF MC_PlayerBD[iPart0].iNumPlayerDeaths < MC_PlayerBD[iPart1].iNumPlayerDeaths
		RETURN TRUE
	ELIF MC_PlayerBD[iPart0].iNumPlayerDeaths > MC_PlayerBD[iPart1].iNumPlayerDeaths
		RETURN FALSE
	ENDIF
	
	//Headshots		
	IF MC_playerBD[iPart0].iNumHeadshots > MC_playerBD[iPart1].iNumHeadshots 
		RETURN TRUE
	ELIF MC_playerBD[iPart0].iNumHeadshots < MC_playerBD[iPart1].iNumHeadshots 
		RETURN FALSE
	ENDIF
	
	//KDR
	IF MC_playerBD[iPart0].fKillDeathRatio > MC_playerBD[iPart1].fKillDeathRatio
		RETURN TRUE
	ELIF MC_playerBD[iPart0].fKillDeathRatio < MC_playerBD[iPart1].fKillDeathRatio
		RETURN FALSE
	ENDIF
	
	//Damage Dealt
	IF MC_playerBD[iPart0].fDamageDealt > MC_playerBD[iPart1].fDamageDealt
		RETURN TRUE
	ELIF MC_playerBD[iPart0].fDamageDealt < MC_playerBD[iPart1].fDamageDealt
		RETURN FALSE
	ENDIF
	
	//Damage Taken
	IF MC_PlayerBD[iPart0].fDamageTaken < MC_PlayerBD[iPart1].fDamageTaken
		RETURN TRUE
	ELIF MC_PlayerBD[iPart0].fDamageTaken > MC_PlayerBD[iPart1].fDamageTaken
		RETURN FALSE
	ENDIF	
	
	//XP Earned
	IF MC_playerBD[iPart0].iRewardXP > MC_playerBD[iPart1].iRewardXP
		RETURN TRUE
	ELIF MC_playerBD[iPart0].iRewardXP < MC_playerBD[iPart1].iRewardXP
		RETURN FALSE
	ENDIF
	
	//Cash Earned
	IF MC_playerBD[iPart0].iCashReward > MC_playerBD[iPart1].iCashReward
		RETURN TRUE
	ELIF MC_playerBD[iPart0].iCashReward < MC_playerBD[iPart1].iCashReward
		RETURN FALSE
	ENDIF
	
	//Team Index
	IF MC_playerBD[iPart0].iTeam < MC_playerBD[iPart1].iTeam
		RETURN TRUE
	ELIF MC_playerBD[iPart0].iTeam > MC_playerBD[iPart1].iTeam
		RETURN FALSE
	ENDIF
	
	//Participant Index
	IF iPart0 < iPart1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

///   (SERVER ONLY: Maintains the leaderboard data)
PROC SERVER_MAINTAIN_MC_LEADERBOARD_FOR_PARTICIPANT(FMMC_PLAYER_STATE &sPlayerState, THE_LEADERBOARD_STRUCT& leaderBoard[])
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_ROUNDS_PAUSE_LBD_SORT)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_LBD_STOP)
		EXIT
	ENDIF
	
	INT iParticipant = sPlayerState.iIndex
	INT iPlayer = NATIVE_TO_INT(sPlayerState.piPlayerIndex)
	BOOL bPlayerSCTV = FALSE

	THE_LEADERBOARD_STRUCT EmptyLeaderBoardStruct, StoredLeaderBoardStruct, TempLeaderBoardStruct
	EmptyLeaderBoardStruct.playerID = INVALID_PLAYER_INDEX()
	BOOL bParticipantRemoved = FALSE
	INT iTempNumberOfLBPlayers[FMMC_MAX_TEAMS]
	INT iDpadScores[NUM_NETWORK_PLAYERS]
	INT iCurrentRank = 1
	BOOL bParticipantInserted = FALSE
	
	INT i
	FOR i = 0 TO NUM_NETWORK_PLAYERS - 1
	
		// For dpad lbd updating					
		iDpadScores[i] = 0
		iDpadScores[i] = (iDpadScores[i] + leaderBoard[i].iPlayerScore)

		IF leaderBoard[i].iParticipant != -1
			IF leaderBoard[i].iTeam !=-1
				iTempNumberOfLBPlayers[leaderBoard[i].iTeam]++
			ENDIF
		ENDIF

		IF NOT bParticipantRemoved
			IF g_FMMC_STRUCT.iMissionEndType != ciMISSION_SCORING_TYPE_TIME
				IF (leaderBoard[i].iParticipant = iParticipant)
					IF NOT sPlayerState.bParticipantActive AND (leaderBoard[i].iplayerscore = -1)
					OR sPlayerState.bParticipantActive
					OR (IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet,PBBOOL_ANY_SPECTATOR)
						AND NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet,PBBOOL_HEIST_SPECTATOR))
						leaderBoard[i] = EmptyLeaderBoardStruct// clear the current participants data.
						bParticipantRemoved = TRUE// sets that this participant has been removed
					ENDIF
				ENDIF
			ELSE
				IF (leaderBoard[i].iParticipant = iParticipant)
					IF NOT sPlayerState.bParticipantActive 
					AND NOT IS_BIT_SET(leaderBoard[i].iLbdBitSet, ciLBD_BIT_FINISHED)
					OR sPlayerState.bParticipantActive
						leaderBoard[i] = EmptyLeaderBoardStruct// clear the current participants data.
						bParticipantRemoved = TRUE// sets that this participant has been removed
					ENDIF
				ENDIF
			ENDIF
		ELSE // this is the participant after the previously removed one
			TempLeaderBoardStruct = leaderBoard[i] //backup this participants data
			leaderBoard[i-1] = 	TempLeaderBoardStruct //set the position down one to the current participant
			leaderBoard[i] = EmptyLeaderBoardStruct // clear the current participants old data
		ENDIF
		
	ENDFOR
	
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		MC_serverBD.iNumberOfLBPlayers[i] = iTempNumberOfLBPlayers[i]
	ENDFOR
	
	IF sPlayerState.piPlayerIndex = INVALID_PLAYER_INDEX()
	OR (!sPlayerState.bPlayerActive
	AND NOT IS_BIT_SET(MC_Playerbd[iParticipant].iClientBitSet,PBBOOL_TERMINATED_SCRIPT))
	OR IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet,PBBOOL_START_SPECTATOR)	
		SET_STOP_PROCESSING_LBD_BIT()
		EXIT
	ENDIF
	
	//Add this participant to the leader board
	FOR i = 0 TO NUM_NETWORK_PLAYERS - 1
		
		IF bParticipantInserted // this runs on the participants after the one that has just been inserted.
			// shift everyone else after the insertion position in the leaderboard down
			TempLeaderBoardStruct = leaderBoard[i] // backup this participants data
			leaderBoard[i] = StoredLeaderBoardStruct  // put the previous participant into this slot			
			StoredLeaderBoardStruct = TempLeaderBoardStruct //store the original part in this slots data into a struct ready to be put in next slot
			RELOOP
		ENDIF
	
		BOOL bGoodToInsert = TRUE
		
		IF leaderBoard[i].iPlayerScore < 0
		AND NOT sPlayerState.bParticipantActive
			RELOOP
		ENDIF
		
		IF leaderBoard[i].playerID != sPlayerState.piPlayerIndex
		AND leaderBoard[i].playerID != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(leaderBoard[i].playerID, FALSE, FALSE)
				IF NOT sPlayerState.bSCTV
				AND NOT IS_PLAYER_SCTV(leaderBoard[i].playerID) // 2110613, I think (PBBOOL_START_SPECTATOR) gets cleared
					IF leaderBoard[i].iplayerscore >= 0
					OR NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(leaderBoard[i].iParticipant))
						IF IS_PLAYER_BEATING_PLAYER_FOR_LEADERBOARD(leaderBoard[i].iParticipant, iParticipant)
							iCurrentRank++
							bGoodToInsert = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
							
		// are we still good to insert this person into the leader board?
		IF (bGoodToInsert)
		AND NOT bPlayerSCTV	
			StoredLeaderBoardStruct = leaderBoard[i] //backup the old participants data that was in this position
			//update the leaderboard for this participant in the new position.
			leaderBoard[i].iParticipant = iParticipant
			leaderBoard[i].playerID = sPlayerState.piPlayerIndex
			leaderBoard[i].iTeam =MC_PlayerBD[iParticipant].iteam
			leaderBoard[i].iTeamScore = GET_TEAM_SCORE_FOR_DISPLAY(MC_PlayerBD[iParticipant].iteam)
			IF HAS_NET_TIMER_STARTED(MC_Playerbd[iParticipant].tdMissionTime)
				leaderBoard[i].iMissionTime = (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_Playerbd[iParticipant].tdMissionTime) / 1000) * 1000 //Round to nearest second to avoid asserting
			ELSE
				leaderBoard[i].iMissionTime = MC_Playerbd[iParticipant].iMissionEndTime
			ENDIF

			leaderBoard[i].iPlayerScore= MC_serverBD.iPlayerScore[iParticipant]	
								
			leaderBoard[i].iKills = (MC_PlayerBD[iParticipant].iNumPlayerKills + MC_PlayerBD[iParticipant].iNumPedKills)
			leaderBoard[i].iDeaths = MC_PlayerBD[iParticipant].iNumPlayerDeaths

			leaderBoard[i].fKDRatio = MC_playerBD[iParticipant].fKillDeathRatio
			leaderBoard[i].iAssists = MC_PlayerBD[iParticipant].iAssists
			leaderBoard[i].iHeadshots = MC_playerBD[iParticipant].iNumHeadshots 
			leaderBoard[i].fDamageDealt		= MC_playerBD[iParticipant].fDamageDealt 
			leaderBoard[i].fDamageTaken		= MC_playerBD[iParticipant].fDamageTaken 
			leaderBoard[i].iRoundsScore = GlobalplayerBD_FM_2[iParticipant].sJobRoundData.iRoundScore
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciLBD_SHOW_TIME_COLUMN)
				
				IF (leaderBoard[i].iMissionTime != ROUND(MC_playerBD[iParticipant].fCaptureTime * 1000))
					leaderBoard[i].iMissionTime = ROUND(MC_playerBD[iParticipant].fCaptureTime * 1000) 
				ENDIF
				
				IF g_FMMC_STRUCT.iLDB_ShowEndTimeForTeam != 0
					IF IS_BIT_SET(g_FMMC_STRUCT.iLDB_ShowEndTimeForTeam, MC_PlayerBD[iParticipant].iTeam)
						IF HAS_TEAM_PASSED_MISSION(MC_PlayerBD[iParticipant].iTeam)
						AND g_FMMC_STRUCT.iRuleTimeToShowOnLeaderboard[MC_PlayerBD[iParticipant].iTeam] > -1
						AND g_FMMC_STRUCT.iRuleTimeToShowOnLeaderboardTeam[MC_PlayerBD[iParticipant].iTeam] > -1
						AND MC_Playerbd[iParticipant].iRoundEndTime = 0
							leaderBoard[i].iMissionTime = GET_OBJECTIVE_TIME_LIMIT_FOR_RULE(g_FMMC_STRUCT.iRuleTimeToShowOnLeaderboardTeam[MC_PlayerBD[iParticipant].iTeam], g_FMMC_STRUCT.iRuleTimeToShowOnLeaderboard[MC_PlayerBD[iParticipant].iTeam])
							PRINTLN("SERVER_MAINTAIN_MC_LEADERBOARD_FOR_PARTICIPANT - Overridden iRoundEndTime for ", iParticipant, " to ", leaderBoard[i].iMissionTime)
						ELSE
							leaderBoard[i].iMissionTime = MC_Playerbd[iParticipant].iRoundEndTime
						ENDIF
					ELSE
						leaderBoard[i].iMissionTime = 0
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet,PBBOOL_FINISHED)
				SET_BIT(leaderBoard[i].iLbdBitSet, ciLBD_BIT_FINISHED)
			ELSE
				CLEAR_BIT(leaderBoard[i].iLbdBitSet, ciLBD_BIT_FINISHED)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciSHOW_ONLY_KILLS_ON_LEADERBOARD)
				leaderBoard[i].iKills = MC_playerBD[iParticipant].iPureKills
			ENDIF						
			
			leaderBoard[i].iBadgeRank = GlobalplayerBD_FM[iPlayer].scoreData.iRank
			
			leaderBoard[i].iJobCash = MC_playerBD[iParticipant].iCashReward
			
			leaderBoard[i].iJobXP	= MC_playerBD[iParticipant].iRewardXP

			leaderBoard[i].iBets = MC_playerBD[iParticipant].iBet
			
			leaderBoard[i].mnVehModel		= MC_playerBD_1[iParticipant].mnCoronaVehicle
			
			leaderBoard[i].iRank = iCurrentRank
			
			bParticipantInserted = TRUE
			
		ENDIF
		
	ENDFOR
	
	SET_STOP_PROCESSING_LBD_BIT()
	
ENDPROC

// FMMC2020 - Refactor - This function might only be temporary until the functions within are refactored to properly utilize existing participant loops.
PROC PROCESS_PARTICIPANT_SYSTEMS_EVERY_FRAME()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
		PROCESS_JUGGERNAUT_EVERY_FRAME()
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Every Frame Player Processing
// ##### Description: 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
PROC PROCESS_PLAYERS_PRE_EVERY_FRAME()
	
	INT iTeam
	
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		IF bIsLocalPlayerHost
			iVehHeldBS[iTeam] = 0
			iTempNumVehHighestPriorityHeld[iTeam] = 0
			iTempTeamVehNumPlayers[iteam] = 0
			tempiNumOfMasks[iteam] = 0
			iTempTeamVehicle[iteam] = -1
			tempiNumCutscenePlayersFinished[iTeam] = 0
			tempiNumCutsceneSpactatorPlayers[iTeam]  = 0
			tempiNumPartPlayingAndFinished[iTeam] = 0
			iPlayersInSeparateVehicles[iTeam] = 0
			iUniqueVehiclesInDropOff[iTeam] = 0
			iTempGranularCurrentPoints[iTeam] = 0
			iAvengerHoldTransitionBlockedCountTemp[iTeam] = 0
		ENDIF
		
		iPlayersInAnyLocate = 0
		
		iTempHackingFails = 0
		
		iVehHeld_NotMe_BS[iTeam] = 0
		iUniqueVehicleBS[iTeam] = 0
		iUniqueVehiclesHeld[iTeam] = 0
		
		CLEAR_BIT(iLocalBoolCheck12, LBOOL12_SERVER_TEAM_0_IS_HACKING_TEMP + iTeam)
		
		CLEAR_BIT(iLocalBoolCheck, LBOOL_WITH_CARRIER_TEMP)
		
		IF IS_MISSION_TEAM_LIVES()
		OR IS_MISSION_COOP_TEAM_LIVES()
			iTeamLivesHUD[iTeam] = GET_FMMC_MISSION_LIVES_FOR_LOCAL_PLAYER()
		ENDIF
		
		iZoneTriggeringPlayerCountByTeam[iZoneTriggeringPlayerCount_StaggeredIterator][iTeam] = 0
	ENDFOR
	
	// Cache the values we have as a host.
	IF bIsLocalPlayerHost
		IF MC_ServerBD.iCachedMissionScoreLimit != g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit
			MC_ServerBD.iCachedMissionScoreLimit = g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit
		ENDIF
		IF MC_ServerBD.iCachedInitialPoints != g_FMMC_STRUCT.iInitialPoints[0]
			MC_ServerBD.iCachedInitialPoints = g_FMMC_STRUCT.iInitialPoints[0]
		ENDIF

	ENDIF
	
	// Assign them for Spectators. This is necessary to fix Spectators not having their globals equal the corona values because they don't run: SAVE_CORONA_OPTIONS_TO_GLOBALS
	// url:bugstar:3732587...
	IF bIsAnySpectator
		IF g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit != MC_ServerBD.iCachedMissionScoreLimit
			g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit = MC_ServerBD.iCachedMissionScoreLimit
		ENDIF
		IF g_FMMC_STRUCT.iInitialPoints[0] != MC_ServerBD.iCachedInitialPoints
			g_FMMC_STRUCT.iInitialPoints[0] = MC_ServerBD.iCachedInitialPoints
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_ALLOW_GANGCHASE_AFTER_SPEED_FAIL)
		CLEAR_BIT(MC_serverBD.iServerBitSet6, SBBOOL6_ALLOW_GANGCHASE_AFTER_SPEED_FAIL)
	ENDIF
	
	iTeamMembers_WithinReqDist = 0
	bAvengerHoldTransitionBlocked = FALSE
	
	iZoneTriggeringPlayerCount[iZoneTriggeringPlayerCount_StaggeredIterator] = 0
	
	IF DOES_DOOR_HAVE_ANIMATION_DYNOPROP(iDoorAnimDynopropAllPlayerStaggeredDoorIterator)
		// Set this bit here, and it'll be un-set in the player loop if any players haven't initialised this door's dynoprop yet
		SET_BIT(iDoorAnimDynopropInitialisedForAllPlayersBS, iDoorAnimDynopropAllPlayerStaggeredDoorIterator)
	ENDIF
	
	sInteractWithVars.iInteractWith_RenderTargetIDUsedThisFrameBS = 0
	CLEAR_LONG_BITSET(iInteractable_TempOccupiedBS)
	
	IF SHOULD_RUN_FULL_SCORE_COUNT_THIS_FRAME()
		SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_SINGLE_FRAME_COUNT_SCORES_ENABLED)
	ENDIF	
	
	CLEAR_BIT(iLocalBoolCheck31, LBOOL31_SOMEONE_IS_RAPPELLING)

	SET_BIT(iLocalBoolCheck34, LBOOL34_SPAWN_GROUP_NEW_RESET_REQUEST_READY)
	SET_BIT(iLocalBoolCheck34, LBOOL34_SPAWN_GROUP_CLEAR_REQUEST_READY)
ENDPROC

PROC PROCESS_PLAYERS_POST_EVERY_FRAME()

	INT iTeam
	
	IF bIsLocalPlayerHost
		FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
			
			IF MC_serverBD.iTeamVehNumPlayers[iTeam] != iTempTeamVehNumPlayers[iTeam]
				MC_serverBD.iTeamVehNumPlayers[iTeam] = iTempTeamVehNumPlayers[iTeam]
				PRINTLN("[RCC MISSION] MC_serverBD.iTeamVehNumPlayers updated to: ",MC_serverBD.iTeamVehNumPlayers[iteam]," for iteam = ",iteam)
				
				IF MC_serverBD.iTeamVehNumPlayers[iTeam] > 0
				AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
				AND NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], MC_serverBD_4.iCurrentHighestPriority[iTeam])
					IF MC_serverBD.iNumVehHighestPriority[iTeam] > 0
					AND iTempTeamVehicle[iTeam] != -1
					AND MC_serverBD_4.iVehPriority[iTempTeamVehicle[iTeam]][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam]
						
						INT iVeh = iTempTeamVehicle[iTeam]
						
						IF MC_serverBD_4.ivehRule[iVeh][iTeam] = FMMC_OBJECTIVE_LOGIC_GO_TO
							
							BOOL bCheckOnRule = TRUE
							INT iTeamsBS = GET_VEHICLE_GOTO_TEAMS(iTeam, iVeh, bCheckOnRule)
							IF (iTeamsBS != 0)
								
								INT iTeamLoop2
								
								FOR iTeamLoop2 = 0 TO (MC_serverBD.iNumberOfTeams - 1)
									IF iTeamLoop2 = iTeam
										IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], MC_serverBD_4.iVehPriority[iVeh][iTeam])
											PRINTLN("[RCC MISSION] POST_EVERYFRAME_PARTLOOP_PROCESSING - Multiple players needed at vehicle, midpoint iObjectiveMidPointBitset set for team ",iteam,", rule ",MC_serverBD_4.iVehPriority[iVeh][iTeam])
											SET_BIT(MC_serverBD.iObjectiveMidPointBitset[iTeam], MC_serverBD_4.iVehPriority[iVeh][iTeam])
											SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
										ENDIF
									ELIF IS_BIT_SET(iTeamsBS, iTeamLoop2)
										IF MC_serverBD_4.iVehPriority[iVeh][iTeamLoop2] < FMMC_MAX_RULES
										AND ((NOT bCheckOnRule) OR (MC_serverBD_4.iVehPriority[iVeh][iTeamLoop2] <= MC_serverBD_4.iCurrentHighestPriority[iTeamLoop2]))
										AND (MC_serverBD_4.ivehRule[iVeh][iTeamLoop2] = MC_serverBD_4.ivehRule[iVeh][iTeam])
										AND NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeamLoop2], MC_serverBD_4.iVehPriority[iVeh][iTeamLoop2])
											PRINTLN("[RCC MISSION] POST_EVERYFRAME_PARTLOOP_PROCESSING -  Multiple players needed at vehicle, midpoint iObjectiveMidPointBitset set for team ",iTeamLoop2,", rule ",MC_serverBD_4.iVehPriority[iVeh][iTeamLoop2])
											SET_BIT(MC_serverBD.iObjectiveMidPointBitset[iTeamLoop2], MC_serverBD_4.iVehPriority[iVeh][iTeamLoop2])
											SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
										ENDIF
									ENDIF
								ENDFOR
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF				
			ENDIF
			
			IF MC_serverBD.iTeamUniqueVehHeldNum[iTeam] != iUniqueVehiclesHeld[iTeam]
				MC_serverBD.iTeamUniqueVehHeldNum[iTeam] = iUniqueVehiclesHeld[iTeam]
			ENDIF
			
			IF MC_serverBD.iTeamVehNumPlayers[iTeam] > 0
			AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES)
					IF MC_serverBD.iNumVehHighestPriority[iTeam] > 0
					AND iTempTeamVehicle[iTeam] != -1
					AND MC_serverBD_4.iVehPriority[iTempTeamVehicle[iTeam]][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam]
						INT iVeh = iTempTeamVehicle[iTeam]
						IF MC_serverBD_4.ivehRule[iVeh][iTeam] = FMMC_OBJECTIVE_LOGIC_GO_TO
						OR MC_serverBD_4.ivehRule[iVeh][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
							BOOL bCheckOnRule = TRUE
							INT iTeamsBS = GET_VEHICLE_GOTO_TEAMS(iTeam, iVeh, bCheckOnRule)
							INT iPlayersToCheck = 0
							INT iTotalPlayersInSeparateVehicles = 0
							IF (iTeamsBS != 0)
								INT iTeamLoop2
								FOR iTeamLoop2 = 0 TO (MC_serverBD.iNumberOfTeams - 1)
									iPlayersToCheck += MC_serverBD.iNumberOfPlayingPlayers[iTeamLoop2]
									iTotalPlayersInSeparateVehicles += iPlayersInSeparateVehicles[iTeamLoop2]
								ENDFOR
								IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[MC_serverBD_4.iVehPriority[iTempTeamVehicle[iTeam]][iTeam]] = FMMC_TARGET_SCORE_PLAYER_HALF
									iPlayersToCheck = GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[MC_serverBD_4.iVehPriority[iTempTeamVehicle[iTeam]][iTeam]], iPlayersToCheck, MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam])
									PRINTLN("[JS] POST_EVERYFRAME_PARTLOOP_PROCESSING - Separate vehicles - we need half of the players in vehicles")
								ENDIF
								PRINTLN("[JS] POST_EVERYFRAME_PARTLOOP_PROCESSING - Separate vehicles needed: ",iPlayersToCheck," we have ", iTotalPlayersInSeparateVehicles)
								IF iTotalPlayersInSeparateVehicles >= iPlayersToCheck
								AND NOT IS_BIT_SET(MC_serverBD.iAllPlayersInSeparateVehiclesBS[iTeam], MC_serverBD_4.iCurrentHighestPriority[iTeam])
									PRINTLN("[JS] POST_EVERYFRAME_PARTLOOP_PROCESSING - setting separate veh bit for team", iTeam, " and rule ", MC_serverBD_4.iCurrentHighestPriority[iTeam])
									SET_BIT(MC_serverBD.iAllPlayersInSeparateVehiclesBS[iTeam], MC_serverBD_4.iCurrentHighestPriority[iTeam])
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE9_PROGRESS_GAD_AT_SAME_TIME)
					IF MC_serverBD.iNumVehHighestPriority[iTeam] > 0
					AND iTempTeamVehicle[iTeam] != -1
					AND MC_serverBD_4.iVehPriority[iTempTeamVehicle[iTeam]][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam]
						INT iVeh = iTempTeamVehicle[iTeam]
						IF MC_serverBD_4.ivehRule[iVeh][iTeam] = FMMC_OBJECTIVE_LOGIC_GO_TO
						OR MC_serverBD_4.ivehRule[iVeh][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
							INT iVehiclesRequired = GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[MC_serverBD_4.iCurrentHighestPriority[iTeam]], MC_serverBD.iNumberOfPlayingPlayers[iTeam], MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam]) / GET_FMMC_POINTS_FOR_TEAM(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam])
							INT iTotalUniqueVehicles = 0
							INT iTeamLoop2
							FOR iTeamLoop2 = 0 TO (MC_serverBD.iNumberOfTeams - 1)
								iTotalUniqueVehicles += iUniqueVehiclesInDropOff[iTeamLoop2]
							ENDFOR
							PRINTLN("[JS] POST_EVERYFRAME_PARTLOOP_PROCESSING - unique vehicles needed: ",iVehiclesRequired," we have ", iTotalUniqueVehicles)
							IF iTotalUniqueVehicles >= iVehiclesRequired
							AND NOT IS_BIT_SET(MC_serverBD.iAllPlayersInUniqueVehiclesBS[iTeam], MC_serverBD_4.iCurrentHighestPriority[iTeam])
								PRINTLN("[JS] POST_EVERYFRAME_PARTLOOP_PROCESSING - setting unique veh bit for team", iTeam, " and rule ", MC_serverBD_4.iCurrentHighestPriority[iTeam])
								SET_BIT(MC_serverBD.iAllPlayersInUniqueVehiclesBS[iTeam], MC_serverBD_4.iCurrentHighestPriority[iTeam])
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE9_ALL_PLAYERS_IN_ANY_LOCATE)
					IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_ALL_PLAYERS_IN_ANY_LOCATE_PASSED)					
						INT iNumberOfLocatePlayers = 0
						INT iTeamLoop2
						FOR iTeamLoop2 = 0 TO (MC_serverBD.iNumberOfTeams - 1)
							IF MC_serverBD_4.iCurrentHighestPriority[iTeamLoop2] < FMMC_MAX_RULES
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop2].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[iTeamLoop2]], ciBS_RULE9_ALL_PLAYERS_IN_ANY_LOCATE)
									iNumberOfLocatePlayers += MC_serverBD.iNumberOfPlayingPlayers[iTeamLoop2]
								ENDIF
							ENDIF
						ENDFOR
						
						IF iPlayersInAnyLocate >= iNumberOfLocatePlayers
							IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)							
								SET_BIT(MC_serverBD.iServerBitSet4, SBBOOL4_ALL_PLAYERS_IN_ANY_LOCATE_PASSED)
								FOR iTeamLoop2 = 0 TO (MC_serverBD.iNumberOfTeams - 1)
									IF IS_TEAM_ACTIVE(iTeamLoop2)
										IF MC_serverBD_4.iCurrentHighestPriority[iTeamLoop2] < FMMC_MAX_RULES
											IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop2].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[iTeamLoop2]], ciBS_RULE9_ALL_PLAYERS_IN_ANY_LOCATE)
												INT iPointsToGiveAnyLocate = 1
												iPointsToGiveAnyLocate = GET_FMMC_POINTS_FOR_TEAM(iTeamLoop2, MC_serverBD_4.iCurrentHighestPriority[iTeamLoop2])
												INCREMENT_SERVER_TEAM_SCORE(iTeamLoop2, MC_serverBD_4.iCurrentHighestPriority[iTeamLoop2], iPointsToGiveAnyLocate)
											ENDIF
										ENDIF
									ENDIF
								ENDFOR	
								FOR iTeamLoop2 = 0 TO (MC_serverBD.iNumberOfTeams - 1)
									IF IS_TEAM_ACTIVE(iTeamLoop2)
										IF MC_serverBD_4.iCurrentHighestPriority[iTeamLoop2] < FMMC_MAX_RULES
											IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop2].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[iTeamLoop2]], ciBS_RULE9_ALL_PLAYERS_IN_ANY_LOCATE)
												SET_PROGRESS_OBJECTIVE_FOR_TEAM(iTeamLoop2, TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDFOR			
								PRINTLN("[JS] All players in any locate - passing rule")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTakeProgressionAmount[MC_serverBD_4.iCurrentHighestPriority[iTeam]] != 0
					IF GET_TOTAL_CASH_GRAB_TAKE() > g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTakeProgressionAmount[MC_serverBD_4.iCurrentHighestPriority[iTeam]]
						ASSERTLN("[JS] iTakeProgressionAmount - Replace with new player rule!")
						PRINTLN("[JS] iTakeProgressionAmount - Replace with new player rule!")
						SET_PROGRESS_OBJECTIVE_FOR_TEAM(iTeam, TRUE)
						PRINTLN("POST_EVERYFRAME_PARTLOOP_PROCESSING - Progressing Rule due to take. ", GET_TOTAL_CASH_GRAB_TAKE() ," > ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTakeProgressionAmount[MC_serverBD_4.iCurrentHighestPriority[iTeam]])
					ENDIF
				ENDIF
			ENDIF
			
			IF MC_serverBD.iGranularCurrentPoints[iTeam] < iTempGranularCurrentPoints[iTeam]
				MC_serverBD.iGranularCurrentPoints[iTeam] = iTempGranularCurrentPoints[iTeam]
				PRINTLN("[RCC MISSION][GRANCAP] MC_serverBD.iGranularCurrentPoints[iTeam] for team: ",iTeam," = ",MC_serverBD.iGranularCurrentPoints[iTeam])
			ENDIF
		
			IF MC_serverBD.iNumVehHighestPriorityHeld[iTeam] != iTempNumVehHighestPriorityHeld[iTeam]
				MC_serverBD.iNumVehHighestPriorityHeld[iTeam] = iTempNumVehHighestPriorityHeld[iTeam]
				PRINTLN("[RCC MISSION] MC_serverBD.iNumVehHighestPriorityHeld updated to: ",MC_serverBD.iNumVehHighestPriorityHeld[iteam]," for iteam = ",iteam)
			ENDIF
			
			IF MC_serverBD.iNumOfMasks[iteam] != tempiNumOfMasks[iteam]
				MC_serverBD.iNumOfMasks[iteam] = tempiNumOfMasks[iteam]
				PRINTLN("[RCC MISSION] MC_serverBD.iNumOfMasks updated to: ",MC_serverBD.iNumOfMasks[iteam]," for iteam = ",iteam)
			ENDIF
			
			IF MC_serverBD.iNumCutscenePlayersFinished[iteam] != tempiNumCutscenePlayersFinished[iteam]
				MC_serverBD.iNumCutscenePlayersFinished[iteam] = tempiNumCutscenePlayersFinished[iteam]
				CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] MC_serverBD.iNumCutscenePlayersFinished updated to: ", MC_serverBD.iNumCutscenePlayersFinished[iteam]," for iteam = ",iteam)
			ENDIF
			
			IF MC_serverBD_1.iNumCutsceneSpectatorPlayers[iTeam] != tempiNumCutsceneSpactatorPlayers[iTeam]
				MC_serverBD_1.iNumCutsceneSpectatorPlayers[iTeam] = tempiNumCutsceneSpactatorPlayers[iTeam]
				CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] MC_serverBD.iNumCutsceneSpectatorPlayers for team ", iteam," updated to: ", MC_serverBD_1.iNumCutsceneSpectatorPlayers[iTeam])
			ENDIF
			
			IF MC_ServerBD_2.iNumPartPlayingAndFinished[iTeam] != tempiNumPartPlayingAndFinished[iTeam]
				MC_ServerBD_2.iNumPartPlayingAndFinished[iTeam] = tempiNumPartPlayingAndFinished[iTeam]
				CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] MC_ServerBD_2.iNumPartPlayingAndFinished for team ", iTeam," updated to: ", MC_ServerBD_2.iNumPartPlayingAndFinished[iTeam])
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck12, LBOOL12_SERVER_TEAM_0_IS_HACKING_TEMP + iTeam)
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_IS_HACKING + iTeam)
					SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_IS_HACKING + iTeam)
				ENDIF
			ELSE
				IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_IS_HACKING + iTeam)
					PRINTLN("[RCC MISSION] MC_serverBD.iServerBitset2, SBBOOL2_TEAM_",iTeam,"_IS_HACKING cleared")
					CLEAR_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_IS_HACKING + iTeam)
				ENDIF
			ENDIF
			
			IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
				INT iSelectTime = GET_MIDPOINT_FAIL_TIME_LIMIT_FOR_CURRENT_RULE(iTeam)

				IF iSelectTime != FMMC_OBJECTIVE_TIME_UNLIMITED
				AND IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],MC_serverBD_4.iCurrentHighestPriority[iTeam])
				AND MC_serverBD.iNumVehHighestPriorityHeld[iTeam] < 1			
					IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdMidpointFailTimer[iTeam])
						REINIT_NET_TIMER(MC_serverBD.tdMidpointFailTimer[iTeam])
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT,"MC_MPF_SPL")
					ELSE
						IF HAS_NET_TIMER_EXPIRED(MC_serverBD.tdMidpointFailTimer[iTeam], iSelectTime)
							REQUEST_SET_TEAM_FAILED(iTeam, mFail_TIME_EXPIRED)
							MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TIME_EXPIRED							
							SET_TEAM_OBJECTIVE_OUTCOME(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam],FALSE)	
						ENDIF
					ENDIF
				ELSE
					RESET_NET_TIMER(MC_serverBD.tdMidpointFailTimer[iTeam])
				ENDIF
			ENDIF
			
			IF MC_serverBD_4.iAvengerHoldTransitionBlockedCount[iteam] != iAvengerHoldTransitionBlockedCountTemp[iteam]
				MC_serverBD_4.iAvengerHoldTransitionBlockedCount[iteam] = iAvengerHoldTransitionBlockedCountTemp[iteam]
				PRINTLN("[RCC MISSION] MC_serverBD_4.iAvengerHoldTransitionBlockedCount  updated to: ", MC_serverBD_4.iAvengerHoldTransitionBlockedCount[iteam]) 
			ENDIF
			
		ENDFOR
		
		IF iTempHackingFails > GET_HACK_FAILS()
			SET_HACK_FAILS(iTempHackingFails)
		ENDIF
		
		PROCESS_SPAWN_GROUP_DATA_RESET_SERVER()
	ENDIF
	
	IF iSpectatorTarget = -1
		IF IS_BIT_SET(iLocalBoolCheck, LBOOL_WITH_CARRIER_TEMP)
			#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_WITH_CARRIER)
				PRINTLN("[RCC MISSION] POST_PARTICIPANT_PROCESSING - Set bit PBBOOL_WITH_CARRIER")
			ENDIF
			#ENDIF
			
			SET_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_WITH_CARRIER)
		ELSE
			IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_WITH_CARRIER)
				PRINTLN("[RCC MISSION] POST_PARTICIPANT_PROCESSING - Clear bit PBBOOL_WITH_CARRIER")
				
				CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_WITH_CARRIER)
			ENDIF
		ENDIF
		
		IF fTeamMembers_ReqDist > 0
		AND iTeamMembers_WithinReqDist >= (MC_serverBD.iNumberOfPlayingPlayers[MC_playerBD[iLocalPart].iteam] - 1) // minus one, as the local player is definitely close enough to themselves
			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset3, PBBOOL3_ALL_TEAM_MEMBERS_WITHIN_REQDIST)
				PRINTLN("[RCC MISSION] Setting PBBOOL3_ALL_TEAM_MEMBERS_WITHIN_REQDIST, fTeamMembers_ReqDist = ",fTeamMembers_ReqDist,", iTeamMembers_WithinReqDist = ",iTeamMembers_WithinReqDist)
				SET_BIT(MC_playerBD[iLocalPart].iClientBitset3, PBBOOL3_ALL_TEAM_MEMBERS_WITHIN_REQDIST)
			ENDIF
		ELSE
			IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset3, PBBOOL3_ALL_TEAM_MEMBERS_WITHIN_REQDIST)
				PRINTLN("[RCC MISSION] Clearing PBBOOL3_ALL_TEAM_MEMBERS_WITHIN_REQDIST, fTeamMembers_ReqDist = ",fTeamMembers_ReqDist,", iTeamMembers_WithinReqDist = ",iTeamMembers_WithinReqDist)
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitset3, PBBOOL3_ALL_TEAM_MEMBERS_WITHIN_REQDIST)
			ENDIF
		ENDIF
		
		IF bAvengerHoldTransitionBlocked != IS_AVENGER_HOLD_TRANSITION_BLOCKED()
			SET_AVENGER_HOLD_TRANSITION_BLOCKED(bAvengerHoldTransitionBlocked)
		ENDIF
		
	ENDIF
	
	iZoneTriggeringPlayerCount_StaggeredIterator++
	IF iZoneTriggeringPlayerCount_StaggeredIterator >= g_FMMC_STRUCT_ENTITIES.iNumberOfZones
		iZoneTriggeringPlayerCount_StaggeredIterator = 0
		
		iCachedZoneIsTriggeredNetworkedBS = iCachedZoneIsTriggeredNetworkedBS_Temp
		iCachedZoneIsTriggeredNetworkedBS_Temp = 0
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(iDoorAnimDynopropInitialisedForAllPlayersBS, iDoorAnimDynopropAllPlayerStaggeredDoorIterator)
		PRINTLN("[RCC MISSION][Doors_SPAM][Door_SPAM ", iDoorAnimDynopropAllPlayerStaggeredDoorIterator, "][DoorDynoprops_SPAM] PROCESS_CLIENT_EVERY_FRAME_PLAYER - Door ", iDoorAnimDynopropAllPlayerStaggeredDoorIterator, "'s dynoprop has been initialised by all players!")
	ENDIF
	#ENDIF
	iDoorAnimDynopropAllPlayerStaggeredDoorIterator++
	IF iDoorAnimDynopropAllPlayerStaggeredDoorIterator >= g_FMMC_STRUCT_ENTITIES.iNumberOfDoors
		iDoorAnimDynopropAllPlayerStaggeredDoorIterator = 0
	ENDIF
	
	COPY_INT_ARRAY_VALUES(iInteractable_TempOccupiedBS, iInteractable_OccupiedBS)
	PRINTLN("[Interactables_SPAM] PROCESS_PLAYERS_POST_EVERY_FRAME | Setting iInteractable_OccupiedBS to ", iInteractable_OccupiedBS[0], " / ", iInteractable_OccupiedBS[1])
	
	sInteractWithVars.iInteractWith_RemotePlayerPropStaggeredIndex++
	IF sInteractWithVars.iInteractWith_RemotePlayerPropStaggeredIndex >= ciINTERACT_WITH__MAX_SPAWNED_PROPS
		PROCESS_PLAYER_INTERACT_WITH_STAGGERED_PROP_RENDERTARGET_CLEANUP()
		sInteractWithVars.iInteractWith_RemotePlayerPropStaggeredIndex = 0
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_SINGLE_FRAME_COUNT_SCORES_ENABLED)
		SET_BIT(iLocalBoolCheck20, LBOOL20_INITIAL_SCORE_COUNT_DONE)
	ENDIF

ENDPROC	

FUNC BOOL SHOULD_PROCESS_PREVENT_TAKEDOWNS_ON_PLAYER(INT iPart)
	IF bIsAnySpectator
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
		RETURN FALSE
	ENDIF
	
	INT iTeam = MC_PlayerBD[iPart].iTeam
	
	IF iTeam <= -1
	AND iTeam > FMMC_MAX_TEAMS
		RETURN FALSE
	ENDIF
	
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_PREVENT_TAKEDOWNS_FOR_TEAM_THIS_RULE)		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_PREVENT_TAKEDOWNS_ON_PLAYER(FMMC_PLAYER_STATE &sPlayerState)

	IF !sPlayerState.bPedAlive
		EXIT
	ENDIF

	IF NOT SHOULD_PROCESS_PREVENT_TAKEDOWNS_ON_PLAYER(sPlayerState.iIndex)
		EXIT
	ENDIF
	
	SET_PED_RESET_FLAG(sPlayerState.piPedIndex, PRF_PreventAllStealthKills, TRUE)
	SET_PED_RESET_FLAG(sPlayerState.piPedIndex, PRF_PreventAllMeleeTakedowns, TRUE)
	
	PRINTLN("[LM] PROCESS_PREVENT_TAKEDOWNS_ON_PLAYER - Preventing Takedowns from being performed on iPart: ", sPlayerState.iIndex)
	
ENDPROC

PROC PROCESS_CLIENT_EVERY_FRAME_INACTIVE_PLAYER(FMMC_PLAYER_STATE &sPlayerState)
	PROCESS_PLAYER_INTERACT_WITH_PROPS(sPlayerState)
ENDPROC

PROC PROCESS_CLIENT_EVERY_FRAME_PLAYER(FMMC_PLAYER_STATE &sPlayerState)
	
	INT iParticipant = sPlayerState.iIndex
	
	IF IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_SINGLE_FRAME_COUNT_SCORES_ENABLED)
		COUNT_PLAYER_SCORES(iParticipant)
	ENDIF
	
	IF !sPlayerState.bParticipantActive
		//Participant not active - Exit!
		PROCESS_CLIENT_EVERY_FRAME_INACTIVE_PLAYER(sPlayerState)
		EXIT
	ENDIF
	
	INT iTeam = MC_playerBD[iParticipant].iTeam
	
	IF MC_playerBD[iParticipant].iVehNear != -1
		IF iParticipant != iPartToUse
			SET_BIT(iVehHeld_NotMe_BS[iTeam], MC_playerBD[iParticipant].iVehNear)
		ENDIF
		IF NOT IS_BIT_SET(iUniqueVehicleBS[iTeam], MC_playerBD[iParticipant].iVehNear)
			SET_BIT(iUniqueVehicleBS[iTeam], MC_playerBD[iParticipant].iVehNear)
			iUniqueVehiclesHeld[iTeam]++
		ENDIF
	ENDIF
	
	IF iParticipant != iLocalPart
		
		IF sPlayerState.bPedAlive
		
			IF bLocalPlayerOK
			AND NOT IS_BIT_SET(iLocalBoolCheck, LBOOL_WITH_CARRIER_TEMP)//Only need to do this once per loop
				IF MC_playerBD[iParticipant].iPedCarryCount > 0 
				OR MC_playerBD[iParticipant].iObjCarryCount > 0 
				OR MC_playerBD[iParticipant].iVehCarryCount > 0
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
						IF IS_PED_IN_ANY_VEHICLE(sPlayerState.piPedIndex, TRUE)
							IF GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE) = GET_VEHICLE_PED_IS_IN(sPlayerState.piPedIndex, TRUE) 
								SET_BIT(iLocalBoolCheck, LBOOL_WITH_CARRIER_TEMP)
								iTempPartEscorting = iParticipant
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			BOOL bProcessedRemotePlayerThermiteObjectsThisFrame = FALSE
			
			IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
				IF IS_BIT_SET(MC_serverBD.iCashGrabRuleActive[iTeam], MC_serverBD_4.iCurrentHighestPriority[iTeam])
				OR IS_BIT_SET(MC_serverBD.iThermiteRuleActive[iTeam], MC_serverBD_4.iCurrentHighestPriority[iTeam])
					//Sync scene for Cash Grab and Thermite checks
					INT iLocalSyncScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iParticipant].iSynchSceneID)
					
					INT iOrderedPart = GET_ORDERED_PARTICIPANT_NUMBER(iParticipant)
					
					IF iOrderedPart != -1
						//Cash grab checks:
						IF IS_BIT_SET(MC_serverBD.iCashGrabRuleActive[iTeam], MC_serverBD_4.iCurrentHighestPriority[iTeam])
						AND NOT IS_BIT_SET(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_REMOTE_BAG_COMPONENT_REMOVED)
							IF processRemotePlayerCashGrabObjects != NULL
								CALL processRemotePlayerCashGrabObjects(sPlayerState.piPedIndex, iParticipant, iLocalSyncScene)
							ENDIF
						ENDIF
					ENDIF
					
					//Perform the same checks for the thermite minigame.
					IF IS_BIT_SET(MC_serverBD.iThermiteRuleActive[iTeam], MC_serverBD_4.iCurrentHighestPriority[iTeam])
						IF processRemotePlayerThermiteObjects != NULL
							CALL processRemotePlayerThermiteObjects(iParticipant, sPlayerState.piPedIndex, iLocalSyncScene)
						ENDIF
						bProcessedRemotePlayerThermiteObjectsThisFrame = TRUE
					ENDIF
				ENDIF
				
				IF NOT bProcessedRemotePlayerThermiteObjectsThisFrame
					//Run thermite minigame checks if processing the thermite minigame off-rule.
					BOOL bProcessOffRuleThermiteObjects = FALSE
					INT i
					REPEAT ciMAX_LOCAL_THERMITE_PTFX i
						IF IS_BIT_SET(iThermiteLocalBitset[i], THERMITE_LOCAL_BITSET_IS_ANY_THERMITE_PROCESSING)
							bProcessOffRuleThermiteObjects = TRUE
						ENDIF
					ENDREPEAT
				
					IF bProcessOffRuleThermiteObjects
						INT iLocalSyncScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iParticipant].iSynchSceneID)
						IF processRemotePlayerThermiteObjects != NULL
							CALL processRemotePlayerThermiteObjects(iParticipant, sPlayerState.piPedIndex, iLocalSyncScene)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF fTeamMembers_ReqDist > 0
				IF iSpectatorTarget = -1
				AND iTeam = MC_playerBD[iLocalPart].iTeam
					IF (NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_ANY_SPECTATOR)
						OR IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
					AND NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_PLAYER_FAIL)
						
						IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
							IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_FINISHED)
								
								VECTOR vLocal = GET_ENTITY_COORDS(LocalPlayerPed)
								VECTOR vPlayer = GET_FMMC_PLAYER_COORDS(sPlayerState)
								
								IF VDIST2(vLocal, vPlayer) <= (fTeamMembers_ReqDist * fTeamMembers_ReqDist)
									iTeamMembers_WithinReqDist++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF	
	
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	IF iRule < FMMC_MAX_RULES	
		//Process Incendiary Ammo
		IF sPlayerState.bPlayerOK 
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_USE_INCENDIARY_AMMO) 
			SET_FIRE_AMMO_THIS_FRAME(sPlayerState.piPlayerIndex)
		ENDIF
	
		IF sPlayerState.bPedAlive				
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_IGNORED_WHEN_WANTED)
				SET_BIT(iPlayerPedIgnoredWhenWanted, iParticipant)
				SET_PED_COMBAT_ATTRIBUTES(sPlayerState.piPedIndex, CA_IGNORED_BY_OTHER_PEDS_WHEN_WANTED, TRUE)
				PRINTLN("[RCC MISSION] [wanted] ciBS_RULE14_IGNORED_WHEN_WANTED set. Setting CA_IGNORED_BY_OTHER_PEDS_WHEN_WANTED on iPart: ", iParticipant)
			ELIF IS_BIT_SET(iPlayerPedIgnoredWhenWanted, iParticipant)
				CLEAR_BIT(iPlayerPedIgnoredWhenWanted, iParticipant)
				SET_PED_COMBAT_ATTRIBUTES(sPlayerState.piPedIndex, CA_IGNORED_BY_OTHER_PEDS_WHEN_WANTED, FALSE)
				PRINTLN("[RCC MISSION] [wanted] ciBS_RULE14_IGNORED_WHEN_WANTED no longer set. Clearing blocked CA_IGNORED_BY_OTHER_PEDS_WHEN_WANTED on iPart: ", iParticipant)
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE6_HIDE_OVERHEAD_HEALTH_BAR)
			IF NOT IS_BIT_SET(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iHideHealthBarsBitset[0], iParticipant)
				SET_BIT(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iHideHealthBarsBitset[0], iParticipant)						
			ENDIF
		ELSE
			IF IS_BIT_SET(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iHideHealthBarsBitset[0], iParticipant)
				CLEAR_BIT(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iHideHealthBarsBitset[0], iParticipant)
			ENDIF
		ENDIF
	ENDIF
	
	//Check if the player has changed team
	IF iTeam != TeamChangeTracker[iParticipant]
		PRINTLN("[Players][Participant: ", iParticipant, "] PROCESS_CLIENT_EVERY_FRAME_PLAYER -  Player has changed team! - Old team: ", TeamChangeTracker[iParticipant], " New team: ", iTeam)
		CLEAR_BIT(iCustomPlayerBlipSetBS, iParticipant)
		TeamChangeTracker[iParticipant] = iTeam
	ENDIF
	
	PROCESS_PREVENT_TAKEDOWNS_ON_PLAYER(sPlayerState)
	
	PROCESS_CUSTOM_PLAYER_BLIPS(sPlayerState, MC_playerBD[iParticipant].iteam) //dont use spectator team...
	
	PROCESS_CUSTOM_OBJECT_CARRIER_PLAYER(sPlayerState, iTeam)
	
	PROCESS_TRACKING_ALL_PLAYER_IN_ZONE_COUNTS(sPlayerState)
	
	PROCESS_PLAYER_INTERACT_WITH_PROPS(sPlayerState)
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
		IF sPlayerState.bParticipantActive
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE6_SHOW_ALL_TEAMS_PLAYERS_LIVES_HUD)
				SET_TEAM_LIVES_REMAINING(iParticipant, iTeam)
			ENDIF
		ENDIF
	ENDIF
	
	// Any player is signallying to block the avenger transition
	IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet4, PBBOOL4_REQUEST_AVENGER_HOLD_TRANSITION_BLOCK)
		bAvengerHoldTransitionBlocked = TRUE
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet4, PBBOOL4_RAPELLING_FROM_HELI)
		SET_BIT(iLocalBoolCheck31, LBOOL31_SOMEONE_IS_RAPPELLING)
	ENDIF
	
	// Interactables //
	IF IS_THIS_PARTICIPANT_INTERACTING(iParticipant)
		INT iInteractableLoop
		FOR iInteractableLoop = 0 TO MC_playerBD[iParticipant].iOngoingInteractionCount - 1
			FMMC_SET_LONG_BIT(iInteractable_TempOccupiedBS, MC_playerBD[iParticipant].iCurrentInteractables[iInteractableLoop])
		ENDFOR
	ENDIF
	
	// Prevent interacting with the same Interactable
	IF IS_LOCAL_PLAYER_INTERACTING(DEFAULT)
		IF IS_THIS_PARTICIPANT_INTERACTING(iParticipant, GET_FOREGROUND_INTERACTABLE_INDEX())
			// This participant is interacting with the same Interactable as us!
			IF iParticipant > iLocalPart
				SET_BIT(iSharedInteractableBS, ciSHARED_INTERACTABLE_BS__BAIL_FOREGROUND_INTERACTION_DUE_TO_MULTI_INTERACT)
				PRINTLN("[Interactables][Interactable ", GET_FOREGROUND_INTERACTABLE_INDEX(), "] PROCESS_CLIENT_EVERY_FRAME_PLAYER - Participant ", iParticipant, " is interacting with the same foreground Interaction as us! Setting ciSHARED_INTERACTABLE_BS__BAIL_FOREGROUND_INTERACTION_DUE_TO_MULTI_INTERACT")
			ELSE
				// We'll just keep going since we take priority
			ENDIF
		ENDIF
	ENDIF
	
	// Door dynoprops
	IF DOES_DOOR_HAVE_ANIMATION_DYNOPROP(iDoorAnimDynopropAllPlayerStaggeredDoorIterator)
	AND NOT IS_BIT_SET(MC_playerBD_1[iParticipant].iDoorAnimDynopropInitialisedBS, iDoorAnimDynopropAllPlayerStaggeredDoorIterator)
		CLEAR_BIT(iDoorAnimDynopropInitialisedForAllPlayersBS, iDoorAnimDynopropAllPlayerStaggeredDoorIterator)
		PRINTLN("[RCC MISSION][Doors][Door ", iDoorAnimDynopropAllPlayerStaggeredDoorIterator, "][DoorDynoprops] PROCESS_CLIENT_EVERY_FRAME_PLAYER - Participant ", iParticipant, " hasn't initialised dynoprop for door ", iDoorAnimDynopropAllPlayerStaggeredDoorIterator)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		
		IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_PRESSED_F)
		AND iPlayerPressedF = -1
			// If the Host was the person who F-failed, we want to wait for the data to propagate
			// to all the clients first before failing ourselves
			IF bIsLocalPlayerHost AND NOT HAS_NET_TIMER_STARTED(DebugPassFailWaitTimer)
				REINIT_NET_TIMER(DebugPassFailWaitTimer)
			ELIF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(DebugPassFaiLWaitTimer) > 500
			OR NOT bIsLocalPlayerHost
				iPlayerPressedF = iParticipant
				PRINTLN("[Players][Participant: ", iParticipant, "] PROCESS_CLIENT_EVERY_FRAME_PLAYER - DEBUG FAIL - Another player pressed F, part ", iPlayerPressedF)
			ENDIF
		ENDIF
		
		// If the Host was the person who S-passed, we want to wait for the data to propagate
		// to all the clients first before passing ourselves - important for end-mission logic!
		IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_PRESSED_S)
		AND iPlayerPressedS = -1
			IF bIsLocalPlayerHost AND NOT HAS_NET_TIMER_STARTED(DebugPassFailWaitTimer)
				REINIT_NET_TIMER(DebugPassFailWaitTimer)
			ELIF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(DebugPassFaiLWaitTimer) > 500
			OR NOT bIsLocalPlayerHost
				iPlayerPressedS = iParticipant
				PRINTLN("[Players][Participant: ", iParticipant, "] PROCESS_CLIENT_EVERY_FRAME_PLAYER - DEBUG PASS - Another player pressed S, part ", iPlayerPressedS)
			ENDIF
		ENDIF
		
	#ENDIF
	
ENDPROC

PROC PROCESS_SERVER_EVERY_FRAME_PLAYER(FMMC_PLAYER_STATE &sPlayerState)

	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF !sPlayerState.bParticipantActive
		//Participant not active - Exit!
		EXIT
	ENDIF
	
	INT iParticipant = sPlayerState.iIndex
	INT iTeam = MC_playerBD[iParticipant].iTeam
	
	IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_CLR_SRV_OBJ_BIT)
	AND IS_BIT_SET(MC_serverBD.iProcessJobCompBitset, iParticipant)
		PRINTLN("[Players][Participant: ", iParticipant, "] PROCESS_SERVER_EVERY_FRAME_PLAYER - Clearing MC_serverBD.iProcessJobCompBitset for part ", iParticipant, " as PBBOOL_CLR_SRV_OBJ_BIT is set")
		CLEAR_BIT(MC_serverBD.iProcessJobCompBitset, iParticipant)
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_ALLOW_GANGCHASE_AFTER_SPEED_FAIL)
		IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet4, PBBOOL4_SPEED_FAIL_TRIGGERED)
			PRINTLN("[Players][Participant: ", iParticipant, "] PROCESS_SERVER_EVERY_FRAME_PLAYER - Setting SBBOOL6_ALLOW_GANGCHASE_AFTER_SPEED_FAIL part: ", iParticipant)
			SET_BIT(MC_serverBD.iServerBitSet6, SBBOOL6_ALLOW_GANGCHASE_AFTER_SPEED_FAIL)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_ANY_SPECTATOR)
		INT iSpecPart = GET_SPECTATOR_TARGET_PARTICIPANT_OF_PARTICIPANT(iParticipant)
		
		IF iSpecPart != -1
			iTeam = MC_playerBD[iSpecPart].iTeam
		ENDIF		
	ENDIF
	
	IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_ANY_SPECTATOR)
		IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND sPlayerState.bParticipantActive
			tempiNumPartPlayingAndFinished[iTeam]++
		ENDIF
	ENDIF
	
	iTempHackingFails += MC_playerBD[iParticipant].iHackingFails

	IF sPlayerState.bParticipantActive
		IF MC_PlayerBD[iParticipant].iGranularCurrentPoints > 0
			iTempGranularCurrentPoints[MC_Playerbd[iParticipant].iTeam] += MC_PlayerBD[iParticipant].iGranularCurrentPoints
		ENDIF
	ENDIF
	
	IF MC_PlayerBD[iParticipant].iObjCapturing != -1
	AND MC_serverBD.iObjCapturer[MC_PlayerBD[iParticipant].iObjCapturing] = -1
		PRINTLN("[CAPTURE][Players][Participant: ", iParticipant, "] PROCESS_SERVER_EVERY_FRAME_PLAYER - Player is capturing object: ", MC_PlayerBD[iParticipant].iObjCapturing)
		MC_serverBD.iObjCapturer[MC_PlayerBD[iParticipant].iObjCapturing] = iParticipant
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE9_ALL_PLAYERS_IN_ANY_LOCATE)
			IF sPlayerState.bParticipantActive
			AND IS_BIT_SET(MC_playerBD[iParticipant].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
				IF MC_playerBD[iParticipant].iCurrentLoc != -1
					iPlayersInAnyLocate++
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_playerBD[iParticipant].iVehNear != -1
		
		INT iVeh = MC_playerBD[iParticipant].iVehNear
		
		IF iTempTeamVehicle[iTeam] = -1
			iTempTeamVehicle[iTeam] = iVeh
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF bInVehicleDebug
				PRINTLN("[Players][Participant: ", iParticipant, "] PROCESS_SERVER_EVERY_FRAME_PLAYER - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH about to count iTempTeamVehNumPlayers for part ", iParticipant, " who is on team ", iTeam)
			ENDIF
		#ENDIF
		
		IF MC_serverBD_4.iVehPriority[iVeh][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam]
		AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
			IF sPlayerState.bParticipantActive
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[MC_serverBD_4.iVehPriority[iVeh][iTeam]], ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES)
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[MC_serverBD_4.iVehPriority[iVeh][iTeam]], ciBS_RULE9_PROGRESS_GAD_AT_SAME_TIME)	
					iTempTeamVehNumPlayers[iTeam]++
				ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[MC_serverBD_4.iVehPriority[iVeh][iTeam]], ciBS_RULE9_PROGRESS_GAD_AT_SAME_TIME)
					IF NOT IS_BIT_SET(iUniqueVehicleBS[iTeam], iVeh)
						iTempTeamVehNumPlayers[iTeam]++
						IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_ANY_SPECTATOR)
							IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
								IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
								OR MC_playerBD[iParticipant].iVehDeliveryId != -1
								OR IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet2, PBBOOL2_DROPPING_OFF)
									PRINTLN("[Players][Participant: ", iParticipant, "] PROCESS_SERVER_EVERY_FRAME_PLAYER - is in vehicle: ", iVeh, " in the drop off")
									iUniqueVehiclesInDropOff[iTeam]++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(iVehHeld_NotMe_BS[iTeam], iVeh) // - as this will get set a bit further down in this function
						iTempTeamVehNumPlayers[iTeam]++
					ENDIF
					BOOL bCheckOnRule = TRUE
					IF IS_BIT_SET(GET_VEHICLE_GOTO_TEAMS(iTeam, iVeh, bCheckOnRule), iTeam)
						IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_ANY_SPECTATOR)
							IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
								IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
								OR MC_playerBD[iParticipant].iVehDeliveryId != -1
								OR IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet2, PBBOOL2_DROPPING_OFF)
									PRINTLN("[JS] - PROCESS_SERVER_EVERY_FRAME_PLAYER - Part ", iParticipant, " is in a vehicle in the drop off")
									iPlayersInSeparateVehicles[iTeam]++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD
				IF bInVehicleDebug
					PRINTLN("[Players][Participant: ", iParticipant, "] PROCESS_SERVER_EVERY_FRAME_PLAYER - Part was in vehicle, iTempTeamVehNumPlayers[iTeam] = ", iTempTeamVehNumPlayers[iTeam])
				ENDIF
			#ENDIF
		ELSE
			
			#IF IS_DEBUG_BUILD
				IF bInVehicleDebug
					PRINTLN("[Players][Participant: ", iParticipant, "] PROCESS_SERVER_EVERY_FRAME_PLAYER - Not increasing iTempTeamVehNumPlayers, MC_serverBD_4.iVehPriority[iVeh][iTeam] = ", MC_serverBD_4.iVehPriority[iVeh][iTeam], " > MC_serverBD_4.iCurrentHighestPriority[iTeam] = ", MC_serverBD_4.iCurrentHighestPriority[iTeam])
				ENDIF
			#ENDIF
			
		ENDIF
		
		IF sPlayerState.bParticipantActive
			IF NOT IS_BIT_SET(iVehHeldBS[iTeam], iveh)
				IF (shouldVehicleCountAsHeldForTeam != NULL AND CALL shouldVehicleCountAsHeldForTeam(iveh, iTeam))
					iTempNumVehHighestPriorityHeld[iTeam]++
					SET_BIT(iVehHeldBS[iTeam], MC_playerBD[iParticipant].iVehNear)
					
					INT iTeamLoop
					
					FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams-1)
						IF iTeamLoop != iTeam
							IF DOES_TEAM_LIKE_TEAM(iTeamLoop, iTeam)
								IF (shouldVehicleCountAsHeldForTeam != NULL AND CALL shouldVehicleCountAsHeldForTeam(iveh, iTeamLoop))
									iTempNumVehHighestPriorityHeld[iTeamLoop]++
									SET_BIT(iVehHeldBS[iTeamLoop], iveh)
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			IF bInVehicleDebug
				PRINTLN("[Players][Participant: ", iParticipant, "] PROCESS_SERVER_EVERY_FRAME_PLAYER - MC_playerBD[iParticipant].iVehNear = -1 for part ", iParticipant, " who is on team ", iTeam)
			ENDIF
		#ENDIF
	ENDIF
	
	IF MC_playerBD[iParticipant].iObjHacking != -1
	AND NOT IS_BIT_SET(MC_playerBD[iParticipant].iHackFailBitset, MC_playerBD[iParticipant].iObjHacking)
		SET_BIT(iLocalBoolCheck12, LBOOL12_SERVER_TEAM_0_IS_HACKING_TEMP + iTeam)
	ENDIF
	
	IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_ANY_SPECTATOR)
	OR IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_HEIST_SPECTATOR)
		IF sPlayerState.bParticipantActive
			IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_WEARING_MASK)
				tempiNumOfMasks[iTeam]++
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet2, PBBOOL2_STARTED_CUTSCENE)
		IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet2, PBBOOL2_FINISHED_CUTSCENE)
			tempiNumCutscenePlayersFinished[iTeam]++
			PRINTLN("[Players][Participant: ", iParticipant, "] PROCESS_SERVER_EVERY_FRAME_PLAYER - iTeam: ", iTeam, " Increasing tempiNumCutscenePlayersFinished to ", tempiNumCutscenePlayersFinished[iTeam])
		ELSE
			PRINTLN("[Players][Participant: ", iParticipant, "] PROCESS_SERVER_EVERY_FRAME_PLAYER - iTeam: ", iTeam, " Started a Cutscene but has not finished one yet.")
		ENDIF
	ELSE
		IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet2, PBBOOL2_FINISHED_CUTSCENE)			
			PRINTLN("[Players][Participant: ", iParticipant, "] PROCESS_SERVER_EVERY_FRAME_PLAYER - iTeam: ", iTeam, " PBBOOL2_FINISHED_CUTSCENE is set when we have not started a cutscene, dirty data!")
			#IF IS_DEBUG_BUILD
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_CUTSCENE_START_FINISH_DATA_IS_DIRTY, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, " Dirty Start/Finish data for Participant # ", iParticipant)
			#ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet2, PBBOOL2_I_AM_SPECTATOR_WATCHING_CUTSCENE)
		IF sPlayerState.bParticipantActive
			INT iPlayerSpectating = GET_SPECTATOR_TARGET_PARTICIPANT_OF_PARTICIPANT(iParticipant)
			IF iPlayerSpectating > -1
				INT iSpectatingTeam = MC_PlayerBD[iPlayerSpectating].iTeam
				tempiNumCutsceneSpactatorPlayers[iSpectatingTeam]++
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet4, PBBOOL4_BLOCKING_AVENGER_HOLD_TRANSITION)
		iAvengerHoldTransitionBlockedCountTemp[MC_playerBD[iParticipant].iTeam]++
	ENDIF
		
	IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet4, PBBOOL4_RESET_SPAWN_GROUP_CACHED_VARS_ON_THIS_REQUEST)
		CLEAR_BIT(iLocalBoolCheck34, LBOOL34_SPAWN_GROUP_NEW_RESET_REQUEST_READY)
	ELSE		
		CLEAR_BIT(iLocalBoolCheck34, LBOOL34_SPAWN_GROUP_CLEAR_REQUEST_READY)	
	ENDIF
	
ENDPROC
		
PROC PROCESS_PLAYERS_EVERY_FRAME()

	PROCESS_PLAYERS_PRE_EVERY_FRAME()
	
	INT i
	FMMC_PLAYER_STATE sEmptyStruct
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1

		FMMC_PLAYER_STATE sPlayerState = sEmptyStruct
		FILL_FMMC_PLAYER_STATE_STRUCT(sPlayerState, i)
		
		PROCESS_CLIENT_EVERY_FRAME_PLAYER(sPlayerState)
		PROCESS_SERVER_EVERY_FRAME_PLAYER(sPlayerState)
		
	ENDFOR
	
	PROCESS_PLAYERS_POST_EVERY_FRAME()
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Staggered Player Processing
// ##### Description: 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
PROC PROCESS_PLAYERS_PRE_STAGGERED()
	
	INT iTeam
	INT iLoc

	IF bIsLocalPlayerHost
		CLEAR_BIT(iLocalBoolCheck,TEMP_TEAM_0_ACTIVE)
		CLEAR_BIT(iLocalBoolCheck,TEMP_TEAM_1_ACTIVE)
		CLEAR_BIT(iLocalBoolCheck,TEMP_TEAM_2_ACTIVE)
		CLEAR_BIT(iLocalBoolCheck,TEMP_TEAM_3_ACTIVE)
		
		IF IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_EVERYONE_RUNNING)
			SET_BIT(MC_serverBD.iServerBitSet2,SBBOOL2_FIRST_UPDATE_STARTED)
		ENDIF
		SET_BIT(MC_serverBD.iServerBitSet2,SBBOOL2_NEW_PARTICIPANT_LOOP_STARTED)
		
		CLEAR_BIT(iLocalBoolCheck5,LBOOL5_TEMP_ANYONE_IN_APPARTMENT)
		
		FOR iTeam = 0  TO (FMMC_MAX_TEAMS-1)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableTripSkipForMission)
				//SEPARATE TEAM TRIP SKIPS
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciTRIP_SKIP_ALL)
					#IF IS_DEBUG_BUILD IF IS_KEYBOARD_KEY_PRESSED(KEY_9) PRINTLN("TRIP SKIP - SERVER - PRE_TRIP_SKIP_SERVER_LOOP - TEAM ", iTeam, " iTotalPlayers = ", MC_serverBD.iNumberOfPart[iTeam]) ENDIF #ENDIF
					PRE_TRIP_SKIP_SERVER_LOOP(MC_serverBD_3.TSServerData[iTeam], MC_serverBD.iNumberOfPart[iTeam])
				ENDIF
			ENDIF
			tempiNumCutscenePlayers[iTeam] = 0
			tempiNumberOfPlayers[iTeam] = 0
			tempiNumberOfPlayersAndSpectators[iTeam] = 0
			iTempTotalMissionTime[iTeam] = 0
			itempChaseTargets[iTeam] = 0
			tempiNumberOfPart[iTeam] = 0
			tempiNumOfWanted[iTeam] = 0
			tempiNumOfFakeWanted[iTeam] = 0
			itempTeamCriticalAmmo[iTeam] = 0
			tempiTeamCivillianKills[iTeam] = 0
			tempiTeamEnemyKills[iTeam] = 0
			fTempLowestLootCapacity[iTeam] = MAX_FLOAT
			tempiNumOfHighestWanted[iTeam] = 0
			tempiNumOfHighestFakeWanted[iTeam] = 0

			FOR iLoc = 0 TO (MC_serverBD.iNumLocCreated-1)
				tempNumberOfTeamInArea[iLoc][iTeam] = 0
				tempNumberOfTeamInAnyArea[iLoc][iTeam] = 0
				tempNumberOfTeamInLeaveArea[iLoc][iTeam] = 0
			ENDFOR
		
			INT iEntity = 0
			FOR iEntity = 0 TO (MC_serverBD.iMaxLoopSize-1)
				iTempNumberOfPlayersLeftLeaveEntity[iTeam][iEntity] = 0
			ENDFOR
			
			tempNumberOfTeamInAnyLeaveAreas[iTeam] = 0
			
			IF SHOULD_ANY_PLAYERS_ON_TEAM_BE_CONSIDERED_WANTED(iTeam)
				tempiNumOfFakeWanted[iTeam]++
				tempiNumOfWanted[iTeam]++
				PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_PRE_STAGGERED - Adding to tempiNumOfWanted and tempiNumOfFakeWanted because a relevant player is dead")
			ENDIF
		ENDFOR

		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableTripSkipForMission)
			//WHOLE CREW TRIP SKIP
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciTRIP_SKIP_ALL)
				#IF IS_DEBUG_BUILD IF IS_KEYBOARD_KEY_PRESSED(KEY_9) PRINTLN("TRIP SKIP - SERVER - PRE_TRIP_SKIP_SERVER_LOOP - WHOLE CREW TRIP SKIP - iTotalPlayers = ", MC_serverBD.iNumberOfPart[0] + MC_serverBD.iNumberOfPart[1] + MC_serverBD.iNumberOfPart[2] + MC_serverBD.iNumberOfPart[3]) ENDIF #ENDIF
				PRE_TRIP_SKIP_SERVER_LOOP(MC_serverBD_3.TSServerData[0], MC_serverBD.iNumberOfPart[0] + MC_serverBD.iNumberOfPart[1] + MC_serverBD.iNumberOfPart[2] + MC_serverBD.iNumberOfPart[3])
			ENDIF
		ENDIF
		
		PROCESS_SERVER_PROXIMITY_SPAWNING_PRE_PLAYER_LOOP()
		
	ENDIF
	
	IF iSpectatorTarget = -1
				
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
			IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iCriticalWeapon[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]] != 0
				WEAPON_TYPE tempweapon = INT_TO_ENUM(WEAPON_TYPE,g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iCriticalWeapon[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]])
				IF HAS_PED_GOT_WEAPON(LocalPlayerPed, tempweapon)
					MC_playerBD[iPartToUse].iCriticalWeaponAmmo = GET_AMMO_IN_PED_WEAPON(LocalPlayerPed,tempweapon) 
				ELSE
					MC_playerBD[iPartToUse].iCriticalWeaponAmmo = 0
				ENDIF
			ELSE
				MC_playerBD[iPartToUse].iCriticalWeaponAmmo = 0
			ENDIF
		ENDIF
		
	ENDIF

	fFurthestTargetDistTemp = 0.0
	iTempPartEscorting = -1
	itempPartNear = -1
	
	CLEAR_BIT(iLocalBoolCheck,LBOOL_IN_GROUP_PED_VEH_TEMP)
	CLEAR_BIT(iLocalBoolCheck6,LBOOL6_SOMEONE_NEEDS_ZONE_ROCKETS)
	
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		iNewRule2[iTeam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	ENDFOR
	
ENDPROC

PROC PROCESS_PLAYERS_POST_STAGGERED()
	
	FLOAT fDeaths
	FLOAT fKills

	IF bIsLocalPlayerHost
	
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_SKIP_PARTICIPANT_UPDATE)
			
			INT iLoc
			INT iTeam
			INT iNumActiveTeams
			INT iNumberOfPlayingTeams
			
			FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
				
				IF IS_BIT_SET(iLocalBoolCheck, TEMP_TEAM_0_ACTIVE + iTeam)
					#IF IS_DEBUG_BUILD
					IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_TEAM_0_ACTIVE + iTeam)
						PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - Team: ", iTeam, " is now active")
					ENDIF
					#ENDIF
					
					SET_BIT(MC_serverBD.iServerBitSet, SBBOOL_TEAM_0_ACTIVE + iTeam)
					SET_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_TEAM_0_WAS_ACTIVE + iTeam)
					iNumActiveTeams = iTeam + 1
					iNumberOfPlayingTeams++
				ELSE
					#IF IS_DEBUG_BUILD
					IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_TEAM_0_ACTIVE + iTeam)
						PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - Team: ", iTeam, " is now inactive")
					ENDIF
					#ENDIF
					
					CLEAR_BIT(MC_serverBD.iServerBitSet, SBBOOL_TEAM_0_ACTIVE + iTeam)
				ENDIF
			
			ENDFOR
			
			IF IS_BIT_SET(iLocalBoolCheck5,LBOOL5_TEMP_ANYONE_IN_APPARTMENT)
				SET_BIT(MC_serverBD.iServerBitSet1,SBBOOL1_HEIST_ANY_PARTICIPANT_INSIDE_ANY_MP_PROPERTY)
			ELSE
				CLEAR_BIT(MC_serverBD.iServerBitSet1,SBBOOL1_HEIST_ANY_PARTICIPANT_INSIDE_ANY_MP_PROPERTY)
			ENDIF
			
			IF iNumActiveTeams > MC_serverBD.iNumActiveTeams
				MC_serverBD.iNumActiveTeams = iNumActiveTeams
				PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD.iNumActiveTeams updated to ", MC_serverBD.iNumActiveTeams, " iNumActiveTeams: ", iNumActiveTeams)
			ENDIF
			
			IF iNumberOfPlayingTeams != MC_serverBD.iNumberOfPlayingTeams
				MC_serverBD.iNumberOfPlayingTeams = iNumberOfPlayingTeams
				PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD.iNumberOfPlayingTeams updated to ", MC_serverBD.iNumberOfPlayingTeams, " iNumberOfPlayingTeams: ", iNumberOfPlayingTeams)
			ENDIF
			
			FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
				
				LEGACY_PROCESS_TEAM_RESTART_CHECKPOINTS(iTeam)
				
				INT iTotalTeamInAreas = 0
				
				FOR iloc = 0 TO (MC_serverBD.iNumLocCreated-1)
					IF MC_serverBD_1.inumberOfTeamInArea[iloc][iTeam] != tempNumberOfTeamInArea[iloc][iTeam]
						MC_serverBD_1.inumberOfTeamInArea[iloc][iTeam] = tempNumberOfTeamInArea[iloc][iTeam]
						PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD_1.inumberOfTeamInArea[iloc][iTeam] updated to: ", MC_serverBD_1.inumberOfTeamInArea[iloc][iTeam], " for loc ", iloc, " for team: ", iTeam) 
					ENDIF
					
					iTotalTeamInAreas += MC_serverBD_1.inumberOfTeamInArea[iloc][iTeam]
					
					IF MC_serverBD_1.inumberOfTeamInAnyArea[iloc][iTeam] != tempNumberOfTeamInAnyArea[iloc][iTeam]
						MC_serverBD_1.inumberOfTeamInAnyArea[iloc][iTeam] = tempNumberOfTeamInAnyArea[iloc][iTeam]
						PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD_1.inumberOfTeamInAnyArea[iloc][iTeam] updated to: ", MC_serverBD_1.inumberOfTeamInAnyArea[iloc][iTeam], " for loc ", iloc, " for team: ", iTeam) 
					ENDIF
					
					IF tempNumberOfTeamInLeaveArea[iLoc][iTeam] = 0
						IF MC_serverBD_4.iGotoLocationDataRule[iLoc][iTeam] = FMMC_OBJECTIVE_LOGIC_LEAVE_LOCATION
							IF ARE_ANY_PLAYERS_ON_TEAM_DEAD_OR_BLOCKING_OBJECTIVE(iTeam)
								tempNumberOfTeamInLeaveArea[iLoc][iTeam]++
								tempNumberOfTeamInAnyLeaveAreas[iTeam]++
								PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD_1.inumberOfTeamInLeaveArea[iloc][iTeam] incrememnted by 1 for loc ", iloc, " for team: ", iTeam) 
							ENDIF
						ENDIF
					ENDIF
					
					IF MC_serverBD_1.iNumberOfTeamInLeaveArea[iloc][iTeam] != tempNumberOfTeamInLeaveArea[iloc][iTeam]
						MC_serverBD_1.iNumberOfTeamInLeaveArea[iloc][iTeam] = tempNumberOfTeamInLeaveArea[iloc][iTeam]
						PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD_1.iNumberOfTeamInLeaveArea[iloc][iTeam] updated to: ", MC_serverBD_1.iNumberOfTeamInLeaveArea[iloc][iTeam], " for loc ", iloc, " for team: ", iTeam) 
					ENDIF
				ENDFOR
				
				INT iEntity = 0
				FOR iEntity = 0 TO (MC_serverBD.iMaxLoopSize-1)
					IF MC_serverBD_3.iNumberOfPlayersLeftLeaveEntity[iTeam][iEntity] != iTempNumberOfPlayersLeftLeaveEntity[iTeam][iEntity]
						MC_serverBD_3.iNumberOfPlayersLeftLeaveEntity[iTeam][iEntity] = iTempNumberOfPlayersLeftLeaveEntity[iTeam][iEntity]
						PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD_1.iNumberOfPlayersLeftLeaveEntity[iTeam][iEntity] updated to: ", MC_serverBD_3.iNumberOfPlayersLeftLeaveEntity[iTeam][iEntity], " for team: ", iTeam) 
					ENDIF
				ENDFOR
				
				IF MC_serverBD_1.iNumberOfTeamInAnyLeaveAreas[iTeam] != tempNumberOfTeamInAnyLeaveAreas[iTeam]
					MC_serverBD_1.iNumberOfTeamInAnyLeaveAreas[iTeam] = tempNumberOfTeamInAnyLeaveAreas[iTeam]
					PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD_1.iNumberOfTeamInAnyLeaveAreas[iTeam] updated to: ", MC_serverBD_1.iNumberOfTeamInAnyLeaveAreas[iTeam], " for team: ", iTeam) 
				ENDIF
				
				IF MC_serverBD_1.iNumberOfTeamInAllAreas[iTeam] != iTotalTeamInAreas
					MC_serverBD_1.iNumberOfTeamInAllAreas[iTeam] = iTotalTeamInAreas
					PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD_1.iNumberOfTeamInAllAreas[iTeam] updated to ",MC_serverBD_1.iNumberOfTeamInAllAreas[iTeam], " for team: ", iTeam)
				ENDIF
				
				IF MC_serverBD.iNumberOfPlayingPlayers[iTeam] != tempiNumberOfPlayers[iTeam]
					MC_serverBD.iNumberOfPlayingPlayers[iTeam] = tempiNumberOfPlayers[iTeam]
					PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD.iNumberOfPlayingPlayers[iTeam] for team: ", iTeam," = ", MC_serverBD.iNumberOfPlayingPlayers[iTeam])
				ENDIF
				
				IF MC_serverBD.iNumberOfPlayersAndSpectators[iTeam] != tempiNumberOfPlayersAndSpectators[iTeam]
					MC_serverBD.iNumberOfPlayersAndSpectators[iTeam] = tempiNumberOfPlayersAndSpectators[iTeam]
					PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD.iNumberOfPlayersAndSpectators[iTeam] for team: ", iTeam," = ", MC_serverBD.iNumberOfPlayersAndSpectators[iTeam])
				ENDIF

				IF MC_serverBD.iTeamCivillianKills[iTeam] != tempiTeamCivillianKills[iTeam]
					MC_serverBD.iTeamCivillianKills[iTeam] = tempiTeamCivillianKills[iTeam]
					PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD.iTeamCivillianKills[iTeam] updated to ", MC_serverBD.iTeamCivillianKills[iTeam], " for team: ",iTeam)
				ENDIF
				
				IF MC_serverBD.iTeamEnemyKills[iTeam] != tempiTeamEnemyKills[iTeam]
					MC_serverBD.iTeamEnemyKills[iTeam] = tempiTeamEnemyKills[iTeam]
					PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD.iTeamEnemyKills[iTeam] updated to ", MC_serverBD.iTeamEnemyKills[iTeam], " for team: ",iTeam)
				ENDIF
				
				IF MC_serverBD.iTeamCriticalAmmo[iTeam] != itempTeamCriticalAmmo[iTeam]
					MC_serverBD.iTeamCriticalAmmo[iTeam] = itempTeamCriticalAmmo[iTeam]
					PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD.iTeamCriticalAmmo[iTeam] for team: ",iTeam," = ",MC_serverBD.iTeamCriticalAmmo[iTeam])
					IF IS_BIT_SET(MC_serverBD.iServerBitSet2,SBBOOL2_NEW_PARTICIPANT_LOOP_FINISHED)
						IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
							IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCriticalWeapon[MC_serverBD_4.iCurrentHighestPriority[iTeam]] != 0
								IF MC_serverBD.iTeamCriticalAmmo[iTeam] = 0									
									PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MISSION FAILED FOR TEAM:",iTeam," BECAUSE THEY HAVE NO AMMO LEFT FOR CRITICAL WEAPON ON RULE: ",MC_serverBD_4.iCurrentHighestPriority[iTeam])
									MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_NO_AMMO
									SET_TEAM_OBJECTIVE_OUTCOME(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam],FALSE)										
									SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam],OBJ_END_REASON_NO_AMMO,TRUE)
									BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(OBJ_END_REASON_NO_AMMO,iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam],FALSE,TRUE)
									REQUEST_SET_TEAM_FAILED(iTeam,mFail_NO_AMMO)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF MC_serverBD_1.iNumGangChaseCandidates[iTeam] != itempChaseTargets[iTeam]
					MC_serverBD_1.iNumGangChaseCandidates[iTeam] = itempChaseTargets[iTeam]	
					PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD_1.iNumGangChaseCandidates[iTeam] for team: ",iTeam," = ",MC_serverBD_1.iNumGangChaseCandidates[iTeam]) 
				ENDIF
				MC_serverBD.iAverageMissionTime[iTeam] = (iTempTotalMissionTime[iTeam]/MC_serverBD.iNumberOfLBPlayers[iTeam])
				
				IF MC_serverBD.iNumberOfPart[iTeam] !=tempiNumberOfPart[iTeam]
					MC_serverBD.iNumberOfPart[iTeam] =tempiNumberOfPart[iTeam]
					PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD.iNumberOfPart updated to: ",MC_serverBD.iNumberOfPart[iTeam]," for iTeam = ",iTeam) 
				ENDIF
				
				IF MC_serverBD.iNumOfWanted[iTeam] !=tempiNumOfWanted[iTeam]
				OR tempiHighestPriority[iTeam] != MC_serverBD_4.iCurrentHighestPriority[iTeam]
					tempiHighestPriority[iTeam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]
					MC_serverBD.iNumOfWanted[iTeam] =tempiNumOfWanted[iTeam]
					
					PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD.iNumOfWanted updated to: ",MC_serverBD.iNumOfWanted[iTeam]," for iTeam = ",iTeam) 
					PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - tempiHighestPriority[iTeam] updated to: ",tempiHighestPriority[iTeam]," for iTeam = ",iTeam) 
					IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE_FAIL_ON_WANTED )
							IF MC_serverBD.iNumOfWanted[iTeam] > 0								
								PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MISSION FAILED FOR TEAM:",iTeam," BECAUSE THEY GAINED A WANTED: ",MC_serverBD_4.iCurrentHighestPriority[iTeam])
								MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_GAINED_WANTED
								SET_TEAM_OBJECTIVE_OUTCOME(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam],FALSE)
								SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam],OBJ_END_REASON_GAINED_WANTED,TRUE)
								BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(OBJ_END_REASON_GAINED_WANTED,iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam],FALSE,TRUE)
								REQUEST_SET_TEAM_FAILED(iTeam,mFail_GAINED_WANTED)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF MC_serverBD.iNumOfFakeWanted[iTeam] != tempiNumOfFakeWanted[iTeam]
					MC_serverBD.iNumOfFakeWanted[iTeam] = tempiNumOfFakeWanted[iTeam]
					PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD.iNumOfFakeWanted[", iTeam, "] updated to: ", MC_serverBD.iNumOfFakeWanted[iTeam], " for Team = ", iTeam)
				ENDIF
				
				IF MC_serverBD.iNumOfHighestWanted[iTeam] != tempiNumOfHighestWanted[iTeam]
					MC_serverBD.iNumOfHighestWanted[iTeam] = tempiNumOfHighestWanted[iTeam]
					PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD.iNumOfHighestWanted[", iTeam, "] updated to: ",  MC_serverBD.iNumOfHighestWanted[iTeam], " for Team = ", iTeam)
				ENDIF
				
				IF MC_serverBD.iNumOfHighestFakeWanted[iTeam] != tempiNumOfHighestFakeWanted[iTeam]
					MC_serverBD.iNumOfHighestFakeWanted[iTeam] = tempiNumOfHighestFakeWanted[iTeam]
					PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD.iNumOfHighestFakeWanted[", iTeam, "] updated to: ", MC_serverBD.iNumOfHighestFakeWanted[iTeam], " for Team = ", iTeam)
				ENDIF
				
				IF MC_serverBD.fLowestLootCapacity[iTeam] != fTempLowestLootCapacity[iTeam]
					MC_serverBD.fLowestLootCapacity[iTeam] = fTempLowestLootCapacity[iTeam]
					PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD.fLowestLootCapacity[", iTeam, "] updated to: ", MC_serverBD.fLowestLootCapacity[iTeam], " for Team = ", iTeam)
				ENDIF

				// This updates the server's number of cutscene players to the number currently on the cutscene
				IF MC_serverBD.iNumCutscenePlayers[iTeam] != tempiNumCutscenePlayers[iTeam]
				
					MC_serverBD.iNumCutscenePlayers[iTeam] = tempiNumCutscenePlayers[iTeam]
					PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD.iNumCutscenePlayers[", iTeam, "] updated to: ",MC_serverBD.iNumCutscenePlayers[iTeam], " current rule:  ", MC_serverBD_4.iCurrentHighestPriority[iTeam])
					IF MC_serverBD.iNumCutscenePlayers[iTeam] >  MC_serverBD.iMaxNumCutscenePlayers[iTeam]
					 	MC_serverBD.iMaxNumCutscenePlayers[iTeam] = MC_serverBD.iNumCutscenePlayers[iTeam]
						PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - MC_serverBD.iMaxNumCutscenePlayers[", iTeam, "]  updated to: ", MC_serverBD.iMaxNumCutscenePlayers[iTeam]) 
					ENDIF
				ENDIF

			ENDFOR
			
			FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
				MC_serverBD.iTotalPlayingCoopPlayers[iTeam] = GET_COOP_TEAMS_TOTAL_ALIVE_PLAYERS(iTeam)
				
				IF MC_serverBD_4.iCurrentHighestPriority[0] < FMMC_MAX_RULES
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciTRIP_SKIP_ALL)
					OR iTeam = 0
						IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
							#IF IS_DEBUG_BUILD PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - TRIP SKIP - SERVER - POST_TRIP_SKIP_SERVER_LOOP - TEAM ", iTeam, " ON RULE ", MC_serverBD_4.iCurrentHighestPriority[iTeam], " ciBS_RULE2_USE_TRIP_SKIP = ", PICK_STRING(IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE2_USE_TRIP_SKIP), "TRUE", "FALSE")) #ENDIF
							POST_TRIP_SKIP_SERVER_LOOP(MC_serverBD_3.TSServerData[iTeam], IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE2_USE_TRIP_SKIP))
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
			MC_serverBD.iTotalNumPart = MC_serverBD.iNumberOfPart[0] + MC_serverBD.iNumberOfPart[1] + MC_serverBD.iNumberOfPart[2] +MC_serverBD.iNumberOfPart[3]
				
			IF IS_BIT_SET(MC_serverbd.iServerBitSet2, SBBOOL2_FIRST_UPDATE_STARTED)
				SET_BIT(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
			ENDIF
			
			PROCESS_SERVER_PROXIMITY_SPAWNING_POST_PLAYER_LOOP()
			
		ENDIF
		
		CLEAR_BIT(MC_serverBD.iServerBitSet, SBBOOL_SKIP_PARTICIPANT_UPDATE)
		
		IF IS_BIT_SET(MC_serverBD.iServerBitSet2,SBBOOL2_NEW_PARTICIPANT_LOOP_STARTED)
			SET_BIT(MC_serverBD.iServerBitSet2,SBBOOL2_NEW_PARTICIPANT_LOOP_FINISHED)
		ENDIF		
	ENDIF
		
	IF iSpectatorTarget = -1
		
		IF IS_BIT_SET(iLocalBoolCheck,LBOOL_IN_GROUP_PED_VEH_TEMP)
			#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_IN_GROUP_PED_VEH)
				PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - Set bit PBBOOL_IN_GROUP_PED_VEH")
			ENDIF
			#ENDIF
			SET_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_IN_GROUP_PED_VEH)
		ELSE
			#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_IN_GROUP_PED_VEH)
				PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - Clear bit PBBOOL_IN_GROUP_PED_VEH")
			ENDIF
			#ENDIF
			CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_IN_GROUP_PED_VEH)
		ENDIF
		
		fDeaths = TO_FLOAT(MC_playerBD[iPartToUse].iNumPlayerDeaths)
		fKills = TO_FLOAT(MC_PlayerBD[iPartToUse].iNumPlayerKills + MC_PlayerBD[iPartToUse].iNumPedKills)
		MC_playerBD[iPartToUse].fKillDeathRatio = GET_KD_RATIO(fDeaths,fKills)
		
		IF MC_playerBD[iPartToUse].fDamageTaken != g_f_MyDamageTaken
			IF g_f_MyDamageTaken >= 0
				MC_playerBD[iPartToUse].fDamageTaken = g_f_MyDamageTaken
				PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - fDamageTaken = ", MC_playerBD[iPartToUse].fDamageTaken)
			ENDIF
		ENDIF
		IF MC_playerBD[iLocalPart].fDamageDealt != g_f_MyDamageDealt
			IF g_f_MyDamageDealt >= 0
				MC_playerBD[iLocalPart].fDamageDealt = g_f_MyDamageDealt
				PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - fDamageDealt = ", MC_playerBD[iLocalPart].fDamageDealt)
			ENDIF
		ENDIF
		
		IF MC_playerBD[iPartToUse].iPartNear != itempPartNear
			MC_playerBD[iPartToUse].iPartNear = itempPartNear
			PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - updating MC_playerBD[iPartToUse].iPartNear to ",MC_playerBD[iPartToUse].iPartNear)
		ENDIF
		
		//If rule is using ciBS_RULE13_FORCE_SEARCH_NO_AMBIENT_COPS check if player is fully wanted or in the searching state, used when respawning.
		IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iPartToUse].iTeam].iRuleBitsetThirteen[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iTeam]], ciBS_RULE13_FORCE_SEARCH_NO_AMBIENT_COPS)
				IF bPlayerToUseOK
				AND bLocalPlayerOK //Double check death incase player died after bPlayerToUseOK was set. 
				AND GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iRespawnState = RESPAWN_STATE_PLAYING //Ensure we've fully respawned before checking,
					IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_FORCED_SEARCH_WANTED_AND_SEEN)
						IF IS_WANTED_AND_HAS_BEEN_SEEN_BY_COPS(LocalPlayer)
							SET_BIT(iLocalBoolCheck26, LBOOL26_FORCED_SEARCH_WANTED_AND_SEEN)
							PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - part ",iPartToUse," has been seen by the cops! Setting LBOOL26_FORCED_SEARCH_WANTED_AND_SEEN")
						ENDIF
					ELSE
						IF NOT IS_WANTED_AND_HAS_BEEN_SEEN_BY_COPS(LocalPlayer)
							CLEAR_BIT(iLocalBoolCheck26, LBOOL26_FORCED_SEARCH_WANTED_AND_SEEN)
							PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - part ",iPartToUse," no longer seen by the cops! Clearing LBOOL26_FORCED_SEARCH_WANTED_AND_SEEN")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iPartEscorting != iTempPartEscorting
		iPartEscorting = iTempPartEscorting
		PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - updating iPartEscorting = ",iPartEscorting)
	ENDIF
		
	IF fFurthestTargetDist !=  fFurthestTargetDistTemp
		fFurthestTargetDist =  fFurthestTargetDistTemp
		IF fFurthestTargetDist < 50
			fFurthestTargetDist = 50
		ENDIF
		IF fFurthestTargetDist > 400
			fFurthestTargetDist = 400
		ENDIF
		fFurthestTargetDist = fFurthestTargetDist
	ENDIF
	
	//Check if we should Block Masks and Gear
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_DisablePIMOutfitSwap)
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE2_DISABLE_MASK_AND_GEAR_CHANGING)
				PRINTLN("[Mask] Blocking mask and gear changing in PI menu")
				SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_MASKS_AND_GEAR)
			ELSE
				IF NOT IS_PIM_NIGHTVISION_BUTTON_DISABLED()
					CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_MASKS_AND_GEAR)
					PRINTLN("[Mask] Un-Blocking mask and gear changing in PI menu")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_STRAND_MISSION_READY_TO_START_DOWNLOAD()
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_DelayNextMissionPreload)
		INT iTeam	
		FOR iTeam = 0 TO (FMMC_MAX_TEAMS-1)		
			IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= (MC_serverBD.iMaxObjectives[iTeam]-1)
				SET_STRAND_MISSION_READY_TO_START_DOWNLOAD(MC_serverBD.iNextMission)
				PRINTLN("[MSRAND][Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - SET_STRAND_MISSION_READY_TO_START_DOWNLOAD TRUE ")
				BREAKLOOP
			ENDIF
		ENDFOR
	ENDIF
	
	INT iObj
	FOR iObj = 0 TO (FMMC_MAX_NUM_OBJECTS -1)
		
		IF IS_BIT_SET(iThermiteKeypadInRangeBS, iObj)
			IF NOT IS_BIT_SET(MC_playerBD_1[iPartToUse].iKeypadThermiteInRangeBS, iObj)
				SET_BIT(MC_playerBD_1[iPartToUse].iKeypadThermiteInRangeBS, iObj)
			ENDIF
		ELSE
			IF IS_BIT_SET(MC_playerBD_1[iPartToUse].iKeypadThermiteInRangeBS, iObj)
				CLEAR_BIT(MC_playerBD_1[iPartToUse].iKeypadThermiteInRangeBS, iObj)
			ENDIF
		ENDIF
	ENDFOR
	
	INT iTeamToUse = MC_playerBD[iPartToUse].iTeam
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeamToUse].iTeamHackFails > 0
		IF NOT HAS_TEAM_TRIGGERED_AGGRO(iTeamToUse, g_FMMC_STRUCT.sFMMCEndConditions[iTeamToUse].iAggroIndexBS_TeamHackFails) 
			IF GET_HACK_FAILS() >= g_FMMC_STRUCT.sFMMCEndConditions[iTeamToUse].iTeamHackFails
				BROADCAST_FMMC_TRIGGER_AGGRO_FOR_TEAM(iTeamToUse, g_FMMC_STRUCT.sFMMCEndConditions[iTeamToUse].iAggroIndexBS_TeamHackFails)
				PRINTLN("[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - Triggering Aggro for team: ", iTeamToUse, " due to team iHackingFails >= ", g_FMMC_STRUCT.sFMMCEndConditions[iTeamToUse].iTeamHackFails)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_FIRST_STAGGERED_LOOP_DONE_CLIENTSIDE)
		INT iTeam	
		FOR iTeam = 0 TO (FMMC_MAX_TEAMS-1)	
			FILL_TEAM_NAMES(iTeam, PlayerToUse)
		ENDFOR
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_FIRST_STAGGERED_LOOP_DONE_CLIENTSIDE)
		SET_BIT(iLocalBoolCheck13, LBOOL13_FIRST_STAGGERED_LOOP_DONE_CLIENTSIDE)
		CPRINTLN(DEBUG_MISSION, "[Players_Staggered] - PROCESS_PLAYERS_POST_STAGGERED - Setting LBOOL13_FIRST_STAGGERED_LOOP_DONE_CLIENTSIDE")
	ENDIF
	
	SET_BIT(iLocalBoolCheck3,LBOOL3_RADAR_INIT)
	
ENDPROC

FUNC BOOL HAS_CATCH_FIRE_TIMER_EXPIRED(INT iTeam, INT iRule)
	INT iCurrentTimeDifference = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdCatchFireDelay)
	INT iCatchFireDelay = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCatchFireDelay[iRule]
	
	IF iCurrentTimeDifference > iCatchFireDelay
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_STOP_FIRE_TIMER_EXPIRED(INT iTeam, INT iRule)
	INT iCurrentTimeDifference = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdCatchFireDelay)
	INT iStopFireDelay = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iStopFireDelay[iRule]
	
	IF iCurrentTimeDifference > iStopFireDelay
		RESET_NET_TIMER(tdStopFireDelay)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PROCESS_SET_FIRE_TO_THE_LOCAL_PLAYER(FMMC_PLAYER_STATE &sPlayerState)
	IF sPlayerState.iIndex != iLocalPart
		EXIT
	ENDIF

	IF NOT sPlayerState.bPedAlive OR NOT sPlayerState.bPlayerActive
		EXIT
	ENDIF	
		
	//Is any of the "set fire" options enabled?
	IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].fCatchFireDistance[GET_TEAM_CURRENT_RULE(MC_playerBD[iLocalPart].iTeam)] = 0.0
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_SET_ON_FIRE)
			RESET_NET_TIMER(tdCatchFireDelay)
			PRINTLN("[SET FIRE] PROCESS_SET_FIRE_TO_THE_LOCAL_PLAYER - Stop entity fire due to rule change")
			ENTITY_INDEX eiPlayer = CONVERT_TO_ENTITY(sPlayerState.piPedIndex)
			IF IS_ENTITY_ALIVE(eiPlayer)
				STOP_ENTITY_FIRE(eiPlayer)			
				IF NOT IS_ENTITY_ON_FIRE(eiPlayer)
					CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_SET_ON_FIRE)
				ENDIF
			ENDIF
		ENDIF
		EXIT
	ENDIF
	
	BOOL bSetFire = FALSE
	ENTITY_INDEX eiPlayer = CONVERT_TO_ENTITY(sPlayerState.piPedIndex)	
	INT iRule = GET_TEAM_CURRENT_RULE(MC_playerBD[iLocalPart].iTeam)
	
	IF NOT IS_ENTITY_ALIVE(eiPlayer)
		EXIT
	ENDIF
	
	//Asserts due to fireproof options
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset, ciBS_Player_FlameProofFlag)
		ASSERTLN("PROCESS_SET_FIRE_TO_THE_LOCAL_PLAYER - Trying to set fire to a fireproof entity! Please disable fireproof option in Mission Details > Team options > Team settings > Team Player proofs")
		EXIT
	ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_SET_TEAM_AS_FIREPROOF)
		ASSERTLN("PROCESS_SET_FIRE_TO_THE_LOCAL_PLAYER - Trying to set fire to a fireproof entity! Please disable 'Set team as fireproof' option on rule ", iRule, ", for team ", MC_playerBD[iLocalPart].iTeam)
		EXIT
	ENDIF
	
	//Set Fire Based On Range
	IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].fCatchFireDistance[iRule] > 0.0
		FLOAT fCatchFireDistance = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].fCatchFireDistance[iRule]
		INT iTeamToCheck
		FOR iTeamToCheck = 0 TO FMMC_MAX_TEAMS - 1
			IF iTeamToCheck = MC_playerBD[iLocalPart].iTeam
				RELOOP
			ENDIF
			
			INT iPlayerIndex = GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(eiPlayer, iTeamToCheck)
			
			IF iPlayerIndex < 0
				RELOOP
			ENDIF
			
			PLAYER_INDEX piTempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerIndex))
			
			IF IS_NET_PLAYER_OK(piTempPlayer) AND GET_DISTANCE_BETWEEN_PLAYERS(LocalPlayer, piTempPlayer) < fCatchFireDistance
				//Create or update the radius blip
				IF DOES_BLIP_EXIST(biCachedVehicleRangeCheckBlip)
					SET_BLIP_COORDS(biCachedVehicleRangeCheckBlip, GET_PLAYER_COORDS(piTempPlayer))
				ELSE 
					biCachedVehicleRangeCheckBlip = ADD_BLIP_FOR_RADIUS(GET_PLAYER_COORDS(piTempPlayer), fCatchFireDistance)
					SET_BLIP_ALPHA(biCachedVehicleRangeCheckBlip, 150)
					SET_BLIP_COLOUR(biCachedVehicleRangeCheckBlip, BLIP_COLOUR_RED)
				ENDIF
				bSetFire = TRUE
				BREAKLOOP
			ELIF DOES_BLIP_EXIST(biCachedVehicleRangeCheckBlip)
				REMOVE_BLIP(biCachedVehicleRangeCheckBlip)
			ENDIF				
		ENDFOR
	ENDIF
	
	//Kill the player when they are low on health so the other player gets a kill
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_SET_ON_FIRE)	
	AND GET_PED_HEALTH_PERCENTAGE(localPlayerPed) <= 10
		
		PRINTLN("[SET FIRE] PROCESS_SET_FIRE_TO_THE_LOCAL_PLAYER - Stop entity fire")
		STOP_ENTITY_FIRE(eiPlayer)
		
		IF NOT IS_ENTITY_ON_FIRE(eiPlayer)
			RESET_NET_TIMER(tdCatchFireDelay)
			RESET_NET_TIMER(tdStopFireDelay)
			CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_SET_ON_FIRE)			
		ENDIF
		
		EXIT
	ENDIF
	
	//Set fire
	IF bSetFire
		IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_SET_ON_FIRE)
		AND NOT IS_ENTITY_ON_FIRE(eiPlayer)
			IF NOT HAS_NET_TIMER_STARTED(tdCatchFireDelay)
				START_NET_TIMER(tdCatchFireDelay)
				EXIT
			ENDIF
		
			IF HAS_CATCH_FIRE_TIMER_EXPIRED(MC_playerBD[iLocalPart].iTeam, iRule)
				PRINTLN("[SET FIRE] PROCESS_SET_FIRE_TO_THE_LOCAL_PLAYER - Set fire to the local player")
				START_ENTITY_FIRE(eiPlayer)
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_SET_ON_FIRE)
				EXIT
			ENDIF
		ENDIF
		
		//Temp fix for the fire ptfx stopping too early on remote machines
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_SET_ON_FIRE)
			STOP_ENTITY_FIRE(eiPlayer)
			START_ENTITY_FIRE(eiPlayer)
		ENDIF
	ENDIF
	
	//Stop fire
	IF !bSetFire
		RESET_NET_TIMER(tdCatchFireDelay)
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_SET_ON_FIRE)
			IF NOT HAS_NET_TIMER_STARTED(tdStopFireDelay)
				START_NET_TIMER(tdStopFireDelay)
				EXIT
			ENDIF
		
			IF HAS_STOP_FIRE_TIMER_EXPIRED(MC_playerBD[iLocalPart].iTeam, iRule)
				PRINTLN("[SET FIRE] PROCESS_SET_FIRE_TO_THE_LOCAL_PLAYER - Stop entity fire")
				STOP_ENTITY_FIRE(eiPlayer)
				
				IF NOT IS_ENTITY_ON_FIRE(eiPlayer)
					CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_SET_ON_FIRE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CLIENT_STAGGERED_PLAYER(FMMC_PLAYER_STATE &sPlayerState)
	
	INT iParticipant = sPlayerState.iIndex
	INT iTeam = MC_playerBD[iParticipant].iTeam
	
	IF sPlayerState.bPlayerActive
		
		IF itempPartNear = -1 // If we haven't already found somebody we're near
		AND iSpectatorTarget = -1
		AND iParticipant != iLocalPart
		AND MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iLocalPart].iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
			
			INT iLogic = MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iLocalPart].iTeam]
			
			IF iLogic = FMMC_OBJECTIVE_LOGIC_GO_TO
			OR ((iLogic >= FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM0) AND (iLogic <= FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM3))
				
				BOOL bValidTeam
				
				IF iLogic = FMMC_OBJECTIVE_LOGIC_GO_TO
					bValidTeam = TRUE
				ELIF ((iLogic >= FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM0) AND (iLogic <= FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM3))
					INT iTargetTeam = ENUM_TO_INT(iLogic) - ENUM_TO_INT(FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM0)
					
					IF iTeam = iTargetTeam
						bValidTeam = TRUE
					ENDIF
				ENDIF
				
				IF bValidTeam
					IF bLocalPlayerPedOk
					AND sPlayerState.bPlayerOk
						VECTOR vOtherPlayer = GET_FMMC_PLAYER_COORDS(sPlayerState)
						VECTOR vMe = GET_ENTITY_COORDS(LocalPlayerPed)
						
						IF MC_serverBD.iCurrentPlayerRule[MC_playerBD[iLocalPart].iTeam] != -1
							
							INT iDistance = g_FMMC_STRUCT.sPlayerRuleData[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iLocalPart].iTeam]].iPlayerRuleLimit[MC_playerBD[iLocalPart].iTeam]
							
							IF VDIST2(vMe, vOtherPlayer) <= (iDistance * iDistance)
								itempPartNear = iParticipant
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)
			IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_FINISHED)
			AND NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_START_SPECTATOR)
				FLOAT fPlayerDist = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(LocalPlayerPed), GET_FMMC_PLAYER_COORDS(sPlayerState))
				IF fPlayerDist > fFurthestTargetDistTemp
					fFurthestTargetDistTemp = fPlayerDist
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck3, LBOOL3_GAMER_HANDLE_SET)
				IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_START_SPECTATOR)
				AND NOT sPlayerState.bSCTV
					PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_CLIENT_STAGGERED_PLAYER - MC_serverBD_2.tParticipantNames[iParticipant] = ", MC_serverBD_2.tParticipantNames[iParticipant])
					STORE_GAMER_HANDLES(lbdVars, sPlayerState.piPlayerIndex, iParticipant)
					SET_BIT(iLocalBoolCheck3, LBOOL3_GAMER_HANDLE_SET)
				ENDIF
			ENDIF
		
			IF IS_STRING_NULL_OR_EMPTY(g_sMission_TeamName[iTeam])
				IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_ANY_SPECTATOR)
				AND NOT sPlayerState.bSCTV
					FILL_TEAM_NAMES(iTeam, sPlayerState.piPlayerIndex)
				ENDIF
			ENDIF
			
			POPULATE_HEADSHOTS_LBD(sPlayerState.piPlayerIndex)
		ENDIF
		
		VEHICLE_INDEX vehIndex
		INT iVeh = -1
		BOOL bDisplay = FALSE
		INT iHealth = -1
		IF sPlayerState.bPedAlive
		AND IS_PED_IN_ANY_VEHICLE(sPlayerState.piPedIndex)
			vehIndex = GET_VEHICLE_PED_IS_IN(sPlayerState.piPedIndex)
			iVeh = IS_ENTITY_A_MISSION_CREATOR_ENTITY(vehIndex)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_HIGH_SPEED_EDGE_FALL_DETECTION)
				SET_NETWORK_ENABLE_HIGH_SPEED_EDGE_FALL_DETECTION(vehIndex, TRUE)
			ENDIF
		ENDIF
		
		IF iVeh > -1
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetEight, ciFMMC_VEHICLE8_GAMER_TAG_HEALTH)
			iHealth = FLOOR(MC_GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(vehIndex, iVeh, GET_TOTAL_STARTING_PLAYERS(), IS_VEHICLE_A_TRAILER(vehIndex)))
			bDisplay = TRUE
		ELIF iVeh = -1
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitSet4, ciBS4_TEAM_VEHICLE_GAMER_TAG_HEALTH)
			iHealth = FLOOR(MC_GET_RESPAWN_VEHICLE_TOTAL_HEALTH_PERCENTAGE(vehIndex, iTeam))
			bDisplay = TRUE
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_ShowVehicleHealthOverheadForLocalPlayer)
			IF bDisplay
				IF IS_MP_GAMER_TAG_ACTIVE(iParticipant)
					SET_MP_GAMER_TAGS_SHOULD_USE_POINTS_HEALTH(iParticipant, TRUE)
					SET_MP_GAMER_TAGS_POINT_HEALTH(iParticipant, iHealth, 100)
				ENDIF
			ELSE
				IF IS_MP_GAMER_TAG_ACTIVE(iParticipant)
					SET_MP_GAMER_TAGS_SHOULD_USE_POINTS_HEALTH(iParticipant, FALSE)
				ENDIF
			ENDIF
		ENDIF
		
		// Updating the container doors for the blow-doors hack minigame
		IF(MC_playerBD[iParticipant].fContainerOpenRatio > MC_playerBD[iLocalPart].fContainerOpenRatio)		
			MC_playerBD[iLocalPart].fContainerOpenRatio = MC_playerBD[iParticipant].fContainerOpenRatio
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_OVERHEAD_HEALTH)
			IF IS_MP_GAMER_TAG_ACTIVE(iParticipant)
			AND sPlayerState.bPedAlive
				IF (iParticipant != iLocalPart OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_ShowVehicleHealthOverheadForLocalPlayer))
				AND IS_PED_IN_ANY_VEHICLE(sPlayerState.piPedIndex)
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciEXPLODE_VEHICLE_ON_ZERO_HEALTH)
						INT iBodyHealth = ROUND(GET_VEHICLE_BODY_HEALTH(GET_VEHICLE_PED_IS_IN(sPlayerState.piPedIndex)))
						
						SET_MP_GAMER_TAGS_SHOULD_USE_POINTS_HEALTH(iParticipant, TRUE)
						SET_MP_GAMER_TAGS_POINT_HEALTH(iParticipant, iBodyHealth,  ROUND(GET_FMMC_PLAYER_VEHICLE_BODY_HEALTH_FOR_TEAM(MC_PlayerBD[iParticipant].iTeam)))
					ELSE
						SET_MP_GAMER_TAGS_SHOULD_USE_VEHICLE_HEALTH(iParticipant, TRUE)
					ENDIF
				ELSE
					SET_MP_GAMER_TAGS_SHOULD_USE_VEHICLE_HEALTH(iParticipant, FALSE)
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF

	IF (MC_playerBD[iParticipant].iPedCarryCount > 0 
	OR MC_playerBD[iParticipant].iObjCarryCount > 0 
	OR MC_playerBD[iParticipant].iVehCarryCount > 0)
		//The below was previously wrapped in bPlayerToUseOK for no apparent reason
		IF MC_playerBD[iParticipant].iWanted > 0
		OR MC_playerBD[iParticipant].iFakeWanted > 0
			IF NOT IS_BIT_SET(iWasPlayerWantedBS, iParticipant)
				SET_BIT(iWasPlayerWantedBS, iParticipant)
				SET_BIT(iTeamCarrierWantedBS, ciTCWBS_T0GOT + iTeam)
				PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_CLIENT_STAGGERED_PLAYER - SET_BIT(iTeamCarrierW@ntedBS, ciTCWBS_T0GOT + iTeam)")
			ENDIF
		ELSE
			IF IS_BIT_SET(iWasPlayerWantedBS, iParticipant)
				CLEAR_BIT(iWasPlayerWantedBS, iParticipant)
				SET_BIT(iTeamCarrierWantedBS, ciTCWBS_T0LOST + iTeam)
				PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_CLIENT_STAGGERED_PLAYER - SET_BIT(iTeamCarrierW@ntedBS, ciTCWBS_T0LOST + iTeam)")
			ENDIF
		ENDIF
		
		IF iOldRule2[iTeam] != iNewRule2[iTeam]
			PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_CLIENT_STAGGERED_PLAYER - iOldRule2[iTeam] != iNewRule2[iTeam]")
			CLEAR_BIT(iTeamCarrierWantedBS, ciTCWBS_T0GOT + iTeam)
			PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_CLIENT_STAGGERED_PLAYER - CLEAR_BIT(iTeamCarrierW@ntedBS, ciTCWBS_T0GOT + iTeam)")
			CLEAR_BIT(iTeamCarrierWantedBS, ciTCWBS_T0LOST + iTeam)
			PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_CLIENT_STAGGERED_PLAYER - CLEAR_BIT(iTeamCarrierW@ntedBS, ciTCWBS_T0LOST + iTeam)")
		ENDIF
	ENDIF
	
	PROCESS_SET_FIRE_TO_THE_LOCAL_PLAYER(sPlayerState)
	
ENDPROC

PROC PROCESS_SERVER_STAGGERED_PLAYER(FMMC_PLAYER_STATE &sPlayerState)

	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	INT iParticipant = sPlayerState.iIndex
	
	IF sPlayerState.bParticipantActive
		IF sPlayerState.bPlayerOK
			IF IS_BIT_SET(MC_serverBD.iPartNotAliveBS, iParticipant)
				PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - Player is okay again, clear iPartNotAliveBS and iPartDeathEventBS")
				CLEAR_BIT(MC_serverBD.iPartDeathEventBS, iParticipant)
				CLEAR_BIT(MC_serverBD.iPartNotAliveBS, iParticipant)
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(MC_serverBD.iPartNotAliveBS, iParticipant)
				PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - Player is not okay, set iPartNotAliveBS")
				SET_BIT(MC_serverBD.iPartNotAliveBS, iParticipant)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetSixteen, ciDISABLE_RECPATURE_OF_LOCATES_WHEN_LEAVING_ONE)	
		IF HAS_NET_TIMER_STARTED(tdDisableLocateRecaptureTimer[iParticipant])
			IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdDisableLocateRecaptureTimer[iParticipant], ci_DISABLE_LOCATE_RECAPTURE_TIME)
				RESET_NET_TIMER(tdDisableLocateRecaptureTimer[iParticipant])
				PRINTLN("[Delay Recapture][Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - Resetting Timer tdDisableLocateRecaptureTimer")
			ENDIF		
		ENDIF
	ENDIF
	
	IF GET_MC_CLIENT_GAME_STATE(iParticipant) = GAME_STATE_RUNNING
		COUNT_PLAYER_SCORES(iParticipant)
	ENDIF
	
	IF sPlayerState.bParticipantActive
	
		PROCESS_SERVER_PROXIMITY_SPAWNING_FOR_PLAYER(iParticipant)
	
		IF sPlayerState.bPlayerOK
		
			IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_START_SPECTATOR)
			AND NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_PLAYER_FAIL)
			AND NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_FINISHED)
				itempTeamCriticalAmmo[MC_playerBD[iParticipant].iTeam] = itempTeamCriticalAmmo[MC_playerBD[iParticipant].iTeam] + MC_playerBD[iParticipant].iCriticalWeaponAmmo
			ENDIF
			
			IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_HEIST_I_AM_INSIDE_ANY_MP_PROPERTY)
				SET_BIT(iLocalBoolCheck5, LBOOL5_TEMP_ANYONE_IN_APPARTMENT)
			ENDIF
			
			//Find player for hack minigame
			IF IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_FIND_PLAYER)
				IF NOT IS_PLAYER_SPECTATING(sPlayerState.piPlayerIndex)
					IF sPlayerState.bPedAlive
						IF MC_serverBD_1.iVehicleForHackingMG != -1
							NETWORK_INDEX niHackVehicle = MC_serverBD_1.sFMMC_SBD.niVehicle[MC_serverBD_1.iVehicleForHackingMG]
							IF NETWORK_DOES_NETWORK_ID_EXIST(niHackVehicle)
								VEHICLE_INDEX viHackVehicle = NET_TO_VEH(niHackVehicle)
								IF IS_PED_IN_VEHICLE(sPlayerState.piPedIndex, viHackVehicle)
									IF NOT IS_VEHICLE_SEAT_FREE(viHackVehicle, VS_DRIVER)
										IF GET_SEAT_PED_IS_IN(sPlayerState.piPedIndex) != VS_DRIVER
											//Found suitable player
											MC_serverBD_1.iPlayerForHackingMG = iParticipant
											CLEAR_BIT(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_FIND_PLAYER)
											PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - MC_serverBD_1.iPlayerForHackingMG = ", MC_serverBD_1.iPlayerForHackingMG)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
		// Server grabbing player names for leaderboards 1640658
		IF IS_STRING_NULL_OR_EMPTY(MC_serverBD_2.tParticipantNames[iParticipant])
		AND NOT IS_STRING_NULL_OR_EMPTY(GET_PLAYER_NAME(sPlayerState.piPlayerIndex))
			IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_START_SPECTATOR)
			AND NOT sPlayerState.bSCTV
				MC_serverBD_2.tParticipantNames[iParticipant] = GET_PLAYER_NAME(sPlayerState.piPlayerIndex)
				PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - MC_serverBD_2.tParticipantNames[iParticipant] = ", MC_serverBD_2.tParticipantNames[iParticipant])
			ENDIF
		ENDIF
		
		IF MC_playerBD[iParticipant].iGameState < GAME_STATE_MISSION_OVER
			IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet4, PBBOOL4_IS_CURRENT_OBJECTIVE_REPAIR_CAR)
			AND NOT HAS_TEAM_FAILED(MC_playerBD[iParticipant].iTeam)
				INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_Playerbd[iParticipant].iTeam]
				IF iRule < FMMC_MAX_RULES

					INT iSpookID = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iParticipant].iTeam].iMinVehHealthSpookID[iRule]
					IF iSpookID != -1
						PRINTLN("[SpookVeh][SpookAggro][Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - The player cant get close to vehicle ", iSpookID)
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iSpookID])
							PRINTLN("[MMacK][SpookVeh][SpookAggro] vehicle exists")
							VEHICLE_INDEX viTempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iSpookID])
							
							IF GET_DISTANCE_BETWEEN_ENTITIES(viTempVeh, sPlayerState.piPedIndex) < g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iParticipant].iTeam].iMinVehHealthSpookDist[iRule]
								PRINTLN("[SpookVeh][SpookAggro][Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - TOO CLOSE! SPOOKED BY PLAYER")
								MC_serverBD.iPartCausingFail[MC_playerBD[iParticipant].iTeam] = iParticipant
								REQUEST_SET_TEAM_FAILED(MC_playerBD[iParticipant].iTeam, INT_TO_ENUM(eFailMissionEnum, ENUM_TO_INT(mFail_PED_AGRO_T0) + MC_playerBD[iParticipant].iTeam), TRUE, iParticipant)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_REQUEST_CUTSCENE_PLAYERS)			
						
			IF NOT IS_BIT_SET(MC_serverBD.iCutsceneFinished[MC_playerBD[iParticipant].iTeam], GET_TEAM_CURRENT_RULE(MC_playerBD[iParticipant].iTeam))
				
				// Print who just requested the cutscene.
				#IF IS_DEBUG_BUILD
				IF NOT IS_BIT_SET(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_0 + MC_playerBD[iParticipant].iTeam)
					PRINTLN("[Cutscenes] - Setting SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_0 + ", MC_playerBD[iParticipant].iTeam, " because of iPart: ", iParticipant)
				ENDIF
				#ENDIF
				
				SET_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_0 + MC_playerBD[iParticipant].iTeam)			
			ELSE
				// We should only sometimes see this print for a few frames before PBBOOL_REQUEST_CUTSCENE_PLAYERS has a chance to broadcast as cleared over the network.
				PRINTLN("[Cutscenes] - Part: ", iParticipant, " is still requesting a cutscene, but the cutscene rule has been set as completed already.")
			ENDIF
			
		ENDIF
		
		IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_TIME
			IF HAS_NET_TIMER_STARTED(MC_Playerbd[iParticipant].tdMissionTime)
				iTempTotalMissionTime[MC_Playerbd[iParticipant].iTeam] = iTempTotalMissionTime[MC_Playerbd[iParticipant].iTeam] + GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_Playerbd[iParticipant].tdMissionTime)
			ELSE
				iTempTotalMissionTime[MC_Playerbd[iParticipant].iTeam] = iTempTotalMissionTime[MC_Playerbd[iParticipant].iTeam] + MC_Playerbd[iParticipant].iMissionEndTime
			ENDIF
		ENDIF
		
		IF MC_playerBD[iParticipant].iObjHacking != -1
			IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iHackFailBitset, MC_playerBD[iParticipant].iObjHacking)
				IF MC_serverBD.iObjHackPart[MC_playerBD[iParticipant].iObjHacking] = -1
					IF MC_serverBD_4.iCurrentHighestPriority[MC_Playerbd[iParticipant].iTeam] < FMMC_MAX_RULES
					AND NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_Playerbd[iParticipant].iTeam], MC_serverBD_4.iCurrentHighestPriority[MC_Playerbd[iParticipant].iTeam])
						SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_Playerbd[iParticipant].iTeam], MC_serverBD_4.iCurrentHighestPriority[MC_Playerbd[iParticipant].iTeam])
						SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
						PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - setting at hacking midpoint iObjectiveMidPointBitset for objective: ", MC_serverBD_4.iCurrentHighestPriority[MC_Playerbd[iParticipant].iTeam], " for team: ", MC_Playerbd[iParticipant].iTeam)
					ENDIF
					MC_serverBD.iObjHackPart[MC_playerBD[iParticipant].iObjHacking] = iParticipant
					PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - MC_serverBD.iObjHackPart = ", iParticipant, " for object = ", MC_playerBD[iParticipant].iObjHacking)
				ENDIF	
			ELSE
				IF MC_serverBD.iObjHackPart[MC_playerBD[iParticipant].iObjHacking] != -1
					PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - fail bitset server setting MC_serverBD.iObjHackPart = -1 ", iParticipant, " for object = ", MC_playerBD[iParticipant].iObjHacking)
					MC_serverBD.iObjHackPart[MC_playerBD[iParticipant].iObjHacking] = -1
				ENDIF
			ENDIF 
		ENDIF
		
		IF MC_playerBD[iParticipant].iVehRequest != -1
			IF MC_serverBD.iVehRequestPart[MC_playerBD[iParticipant].iVehRequest] = -1
				MC_serverBD.iVehRequestPart[MC_playerBD[iParticipant].iVehRequest] = iParticipant
				PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - MC_serverBD.iVehRequestPart = ", iParticipant, " for vehicle = ", MC_playerBD[iParticipant].iVehRequest)
			ENDIF
		ENDIF
		
		IF (NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_ANY_SPECTATOR) OR IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
		AND NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_PLAYER_FAIL)

			IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
					
				tempiNumberOfPlayersAndSpectators[MC_Playerbd[iParticipant].iTeam]++
				
				IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_FINISHED)
			
					PROCESS_GANG_CHASE_PLAYER_CONDITIONS(sPlayerState.piPedIndex, iParticipant)

					IF MC_serverBD_4.iCurrentHighestPriority[MC_Playerbd[iParticipant].iTeam] < FMMC_MAX_RULES
						//Count number of players started the cutscene
						IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_PLAYING_CUTSCENE)
							tempiNumCutscenePlayers[MC_Playerbd[iParticipant].iTeam]++
						ENDIF
						
						IF MC_playerBD[iParticipant].iWanted > 0
							tempiNumOfWanted[MC_Playerbd[iParticipant].iTeam]++
						ENDIF
						
						IF MC_playerBD[iParticipant].iFakeWanted > 0
							tempiNumOfFakeWanted[MC_Playerbd[iParticipant].iTeam]++
						ENDIF
						
						IF MC_playerBD[iParticipant].iWanted > tempiNumOfHighestWanted[MC_Playerbd[iParticipant].iTeam]
							tempiNumOfHighestWanted[MC_Playerbd[iParticipant].iTeam] = MC_playerBD[iParticipant].iWanted
						ENDIF
						
						IF MC_playerBD[iParticipant].iFakeWanted > tempiNumOfHighestFakeWanted[MC_Playerbd[iParticipant].iTeam]
							tempiNumOfHighestFakeWanted[MC_Playerbd[iParticipant].iTeam] = MC_playerBD[iParticipant].iFakeWanted
						ENDIF						
						
						IF MC_playerBD[iParticipant].sLootBag.fCurrentBagCapacity < fTempLowestLootCapacity[MC_Playerbd[iParticipant].iTeam]
							fTempLowestLootCapacity[MC_Playerbd[iParticipant].iTeam] = MC_playerBD[iParticipant].sLootBag.fCurrentBagCapacity
						ENDIF
						
						IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_Playerbd[iParticipant].iTeam], MC_serverBD_4.iCurrentHighestPriority[MC_Playerbd[iParticipant].iTeam])
							IF ((MC_playerBD[iParticipant].iPedCarryCount > 0) AND (MC_serverBD.iNumPedHighestPriority[MC_playerBD[iParticipant].iTeam] > 0))
							OR ((MC_playerBD[iParticipant].iObjCarryCount > 0) AND (MC_serverBD.iNumObjHighestPriority[MC_playerBD[iParticipant].iTeam] > 0))
							OR ((MC_playerBD[iParticipant].iVehCarryCount > 0) AND (MC_serverBD.iNumVehHighestPriority[MC_playerBD[iParticipant].iTeam] > 0))
								SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_Playerbd[iParticipant].iTeam], MC_serverBD_4.iCurrentHighestPriority[MC_Playerbd[iParticipant].iTeam])
								SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
								PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - setting at carrying midpoint iObjectiveMidPointBitset for objective: ", MC_serverBD_4.iCurrentHighestPriority[MC_Playerbd[iParticipant].iTeam], " for team: ", MC_Playerbd[iParticipant].iTeam)
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iParticipant].iTeam].iRuleBitsetFour[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iParticipant].iTeam]], ciBS_RULE4_FAIL_IF_DETECTED_BY_POLICE)
						AND NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdCopSpottedFailTimer)
							IF IS_WANTED_AND_HAS_BEEN_SEEN_BY_COPS(sPlayerState.piPlayerIndex)
								PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - player has been seen by the cops! Starting tdCopSpottedFailTimer")
								REINIT_NET_TIMER(MC_serverBD.tdCopSpottedFailTimer)
								MC_serverBD.iPartCausingFail[MC_playerBD[iParticipant].iTeam] = iParticipant
							ENDIF
						ENDIF
					ENDIF
					
					tempiNumberOfPart[MC_Playerbd[iParticipant].iTeam]++
									
					IF IS_BIT_SET(MC_Playerbd[iParticipant].iClientBitSet, PBBOOL_CHASE_TARGET)
						itempChaseTargets[MC_Playerbd[iParticipant].iTeam]++
					ENDIF
					
					IF NOT IS_OBJECTIVE_BLOCKED_FOR_PARTICIPANT(iParticipant)
						IF MC_playerBD[iParticipant].iInAnyLoc !=-1
							tempNumberOfTeamInAnyArea[MC_playerBD[iParticipant].iInAnyLoc][MC_Playerbd[iParticipant].iTeam]++
						ENDIF
					ENDIF
					
					IF MC_playerBD[iParticipant].iNearDialogueTrigger !=-1
						SET_BIT(MC_serverBD_4.iDialogueRangeBitset[MC_playerBD[iParticipant].iNearDialogueTrigger/32], MC_playerBD[iParticipant].iNearDialogueTrigger%32)
						PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - SET_BiT(MC_serverBD_4.iDialogueRangeBitset) MC_playerBD[iParticipant].iNearDialogueTrigger = ", MC_playerBD[iParticipant].iNearDialogueTrigger)
					ENDIF
					
					IF MC_playerBD[iParticipant].iLeaveLoc != -1
						IF MC_serverBD_4.iGotoLocationDataRule[MC_playerBD[iParticipant].iLeaveLoc][MC_Playerbd[iParticipant].iTeam] = FMMC_OBJECTIVE_LOGIC_LEAVE_LOCATION
							tempNumberOfTeamInLeaveArea[MC_playerBD[iParticipant].iLeaveLoc][MC_Playerbd[iParticipant].iTeam]++
							tempNumberOfTeamInAnyLeaveAreas[MC_Playerbd[iParticipant].iTeam]++
						ENDIF
					ENDIF
												
					INT iEntity = 0
					FOR iEntity = 0 TO (MC_serverBD.iMaxLoopSize-1)
						IF GET_MC_CLIENT_MISSION_STAGE(iParticipant) = CLIENT_MISSION_STAGE_LEAVE_PED
							IF iEntity < FMMC_MAX_PEDS
								IF FMMC_IS_LONG_BIT_SET(MC_Playerbd[iParticipant].iPedLeftBS, iEntity)
									IF MC_serverBD_4.iPedRule[iEntity][MC_Playerbd[iParticipant].iTeam] = FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY
										iTempNumberOfPlayersLeftLeaveEntity[MC_Playerbd[iParticipant].iTeam][iEntity] ++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF GET_MC_CLIENT_MISSION_STAGE(iParticipant) = CLIENT_MISSION_STAGE_LEAVE_VEH
							IF iEntity < FMMC_MAX_VEHICLES
								IF IS_BIT_SET(MC_Playerbd[iParticipant].iVehLeftBS, iEntity)
									IF MC_serverBD_4.iVehRule[iEntity][MC_Playerbd[iParticipant].iTeam] = FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY
										iTempNumberOfPlayersLeftLeaveEntity[MC_Playerbd[iParticipant].iTeam][iEntity] ++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF GET_MC_CLIENT_MISSION_STAGE(iParticipant) = CLIENT_MISSION_STAGE_LEAVE_OBJ
							IF iEntity < FMMC_MAX_NUM_OBJECTS
								IF IS_BIT_SET(MC_Playerbd[iParticipant].iObjLeftBS, iEntity)
									IF MC_serverBD_4.iObjRule[iEntity][MC_Playerbd[iParticipant].iTeam] = FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY
										iTempNumberOfPlayersLeftLeaveEntity[MC_Playerbd[iParticipant].iTeam][iEntity] ++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR	
										
					tempiTeamCivillianKills[MC_Playerbd[iParticipant].iTeam] += MC_playerBD[iParticipant].iCivilianKills
					tempiTeamEnemyKills[MC_Playerbd[iParticipant].iTeam] += MC_serverBD.iNumPedKills[iParticipant]
					
					IF MC_playerBD[iParticipant].iCurrentLoc !=-1
					AND NOT HAS_NET_TIMER_STARTED(tdDisableLocateRecaptureTimer[iParticipant])
						IF (MC_serverBD_4.iCurrentHighestPriority[MC_Playerbd[iParticipant].iTeam] < FMMC_MAX_RULES)
						AND (MC_serverBD_4.iGotoLocationDataPriority[MC_playerBD[iParticipant].iCurrentLoc][MC_Playerbd[iParticipant].iTeam] = MC_serverBD_4.iCurrentHighestPriority[MC_Playerbd[iParticipant].iTeam])
							IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_Playerbd[iParticipant].iTeam], MC_serverBD_4.iGotoLocationDataPriority[MC_playerBD[iParticipant].iCurrentLoc][MC_Playerbd[iParticipant].iTeam])
								SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_Playerbd[iParticipant].iTeam], MC_serverBD_4.iGotoLocationDataPriority[MC_playerBD[iParticipant].iCurrentLoc][MC_Playerbd[iParticipant].iTeam])
								SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
								PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - setting at location midpoint iObjectiveMidPointBitset for objective: ", MC_serverBD_4.iGotoLocationDataPriority[MC_playerBD[iParticipant].iCurrentLoc][MC_Playerbd[iParticipant].iTeam], " for team: ", MC_Playerbd[iParticipant].iTeam)
							ENDIF
						ENDIF
						
						IF NOT IS_OBJECTIVE_BLOCKED_FOR_PARTICIPANT(iParticipant)				
							tempNumberOfTeamInArea[MC_playerBD[iParticipant].iCurrentLoc][MC_Playerbd[iParticipant].iTeam]++				
							
							IF MC_serverBD_4.iLocMissionLogic[MC_playerBD[iParticipant].iTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
								IF MC_serverBD_1.iNumControlKills[iParticipant] < MC_Playerbd[iParticipant].iNumControlKills
									MC_serverBD_1.iNumTeamControlKills[MC_playerBD[iParticipant].iCurrentLoc][MC_Playerbd[iParticipant].iTeam] = MC_serverBD_1.iNumTeamControlKills[MC_playerBD[iParticipant].iCurrentLoc][MC_Playerbd[iParticipant].iTeam]  + (MC_Playerbd[iParticipant].iNumControlKills - MC_serverBD_1.iNumControlKills[iParticipant])
									PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - MC_serverBD_1.iNumTeamControlKills: ") NET_PRINT_INT(MC_serverBD_1.iNumTeamControlKills[MC_playerBD[iParticipant].iCurrentLoc][MC_Playerbd[iParticipant].iTeam]) NET_NL()
									MC_serverBD_1.iNumControlKills[iParticipant] = MC_Playerbd[iParticipant].iNumControlKills
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					INT iActiveTeam = MC_Playerbd[iParticipant].iTeam
					
					IF (!sPlayerState.bPlayerOK OR IS_BIT_SET(MC_serverBD.iPartDeathEventBS, iParticipant)) // Sometimes the death event comes through before the player returns as not okay, so use this bitset here
					AND (MC_serverBD_4.iCurrentHighestPriority[iActiveTeam] < FMMC_MAX_RULES)
						INT iNewTeam = g_FMMC_STRUCT.sFMMCEndConditions[iActiveTeam].iSwapToTeamOnDeath[MC_serverBD_4.iCurrentHighestPriority[iActiveTeam]]
						
						IF iNewTeam != -1
						AND iNewTeam != iActiveTeam
							PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - will be joining ", iNewTeam, " because they're dead and they should swap to this team, set that team as active - current team ", iActiveTeam, " + rule ", MC_serverBD_4.iCurrentHighestPriority[iActiveTeam])
							
							//Just to be safe, set their old team's bit too:
							SET_BIT(iLocalBoolCheck, TEMP_TEAM_0_ACTIVE + iActiveTeam)
							
							iActiveTeam = iNewTeam
						ENDIF
					ENDIF

					tempiNumberOfPlayers[iActiveTeam]++
					SET_BIT(iLocalBoolCheck, TEMP_TEAM_0_ACTIVE + iActiveTeam)
										
					//Backup hacking minigame logic (these should be handled by events):
					IF MC_serverBD_1.iPlayerForHackingMG = -1
						IF NOT IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_FIND_PLAYER)
						AND MC_playerBD[iParticipant].iHackMGDialogue != -1
							PROCESS_SERVER_CIRCUIT_HACK_MG_INIT(MC_playerBD[iParticipant].iHackMGDialogue)
						ENDIF
					ELIF MC_serverBD_1.iPlayerForHackingMG = iParticipant
						IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet2, PBBOOL2_HACKING_MINIGAME_COMPLETED)
							PROCESS_SERVER_CIRCUIT_HACK_MG_END(MC_playerBD[iParticipant].iTeam, TRUE)
						ELIF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet2, PBBOOL2_HACKING_MINIGAME_FAILED)
							PROCESS_SERVER_CIRCUIT_HACK_MG_END(MC_playerBD[iParticipant].iTeam, FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
					
		ELSE	
		
			IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
			AND IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
				tempiNumberOfPlayersAndSpectators[MC_playerBD[iParticipant].iTeam]++
			ENDIF
		
			IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_PLAYER_FAIL)
			AND NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_ANY_SPECTATOR)//url:bugstar:4866729
			AND NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_HEIST_SPECTATOR)
				PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - has PBBOOL_PLAYER_FAIL set!")
				
				IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
					
					INT iTeamLives = ciFMMC_UNLIMITED_LIVES
					
					IF IS_MISSION_TEAM_LIVES()
						iTeamlives = GET_FMMC_MISSION_LIVES_FOR_TEAM(MC_playerBD[iParticipant].iTeam)
						
						IF iTeamlives != ciFMMC_UNLIMITED_LIVES
							MC_serverBD.iPartCausingFail[MC_playerBD[iParticipant].iTeam] = iParticipant
							PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - is the first we come across with PBBOOL_PLAYER_OUT_OF_LIVES with team lives for team ", MC_playerBD[iParticipant].iTeam, ", set them as causing fail")
						ENDIF
					ELIF IS_MISSION_COOP_TEAM_LIVES()
						iTeamlives = GET_FMMC_MISSION_LIVES_FOR_COOP_TEAMS()
						
						IF iTeamlives != ciFMMC_UNLIMITED_LIVES
							
							INT iTeamLoop
							
							FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
								IF DOES_TEAM_LIKE_TEAM(iTeamLoop, MC_playerBD[iParticipant].iTeam)
									MC_serverBD.iPartCausingFail[iTeamLoop] = iParticipant
									PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - is the first we come across with PBBOOL_PLAYER_OUT_OF_LIVES with coop team lives for team ", MC_playerBD[iParticipant].iTeam, ", set them as causing fail for friendly team ", iTeamLoop)
								ENDIF
							ENDFOR
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
				
		IF !sPlayerState.bPlayerActive
			IF IS_BIT_SET(MC_serverBD.iProcessJobCompBitset, iParticipant)
				PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - Clearing MC_serverBD.iProcessJobCompBitset for part ", iParticipant, " as net player isn't okay")
				CLEAR_BIT(MC_serverBD.iProcessJobCompBitset, iParticipant)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableTripSkipForMission)
			INT iTripSkipTeam
			IF MC_serverBD_4.iCurrentHighestPriority[0] < FMMC_MAX_RULES
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciTRIP_SKIP_ALL)
					FOR iTripSkipTeam = 0 TO (FMMC_MAX_TEAMS - 1)
						IF IS_TEAM_ACTIVE(iTripSkipTeam)
							PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP_SERVER_LOOP - TEAM CHECK ", iTripSkipTeam, " PARTICIPANT TEAM", MC_playerBD[iParticipant].iTeam,  " iParticipant ", iParticipant, " Name ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))), "______________")
							IF MC_playerBD[iParticipant].iTeam = iTripSkipTeam
								PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP_SERVER_LOOP - TEAM ", iTripSkipTeam, " iParticipant ", iParticipant, " Name ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))))
								MAINTAIN_TRIP_SKIP_SERVER_LOOP(MC_serverBD_3.TSServerData[iTripSkipTeam], MC_playerBD[iParticipant].TSPlayerData, iParticipant, IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_HEIST_HOST))
							ENDIF
						ENDIF
					ENDFOR
				ELSE
					PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP_SERVER_LOOP - WHOLE CREW TRIP SKIP - ", " iParticipant ", iParticipant, " Name ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))))
					MAINTAIN_TRIP_SKIP_SERVER_LOOP(MC_serverBD_3.TSServerData[0], MC_playerBD[iParticipant].TSPlayerData, iParticipant, IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_HEIST_HOST))
				ENDIF
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			MAINTAIN_SERVER_J_SKIP(iParticipant)
		#ENDIF
		
	ELSE
		IF MC_playerBD[iParticipant].iObjHacking != -1
			IF MC_serverBD.iObjHackPart[MC_playerBD[iParticipant].iObjHacking] = iParticipant
				MC_serverBD.iObjHackPart[MC_playerBD[iParticipant].iObjHacking] = -1
				PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - MC_serverBD.iObjHackPart for obj ", MC_playerBD[iParticipant].iObjHacking, " being reset to -1 because part ", iParticipant, " is no longer active")
			ENDIF
		ENDIF
		
		IF MC_playerBD[iParticipant].iVehRequest != -1
			IF MC_serverBD.iVehRequestPart[MC_playerBD[iParticipant].iVehRequest] = iParticipant
				MC_serverBD.iVehRequestPart[MC_playerBD[iParticipant].iVehRequest] = -1
				PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - MC_serverBD.iVehRequestPart for veh ", MC_playerBD[iParticipant].iVehRequest, " being reset to -1 because part ", iParticipant, " is no longer active")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD.iProcessJobCompBitset, iParticipant)
			PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - Clearing MC_serverBD.iProcessJobCompBitset for part ", iParticipant, " as part no longer active")
			CLEAR_BIT(MC_serverBD.iProcessJobCompBitset, iParticipant)
		ENDIF
		
		IF MC_playerBD[iParticipant].sLootBag.iCashGrabbed != 0
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_UseIndividualBagValues)
		AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
		AND NOT IS_BIT_SET(MC_ServerBD.sCashGrab.iLeaverCashRemovedBS, iParticipant)
			SET_TOTAL_CASH_GRAB_TAKE(CLAMP_INT(GET_TOTAL_CASH_GRAB_TAKE() - MC_playerBD[iParticipant].sLootBag.iCashGrabbed, 0, GET_TOTAL_CASH_GRAB_TAKE()))
			SET_BIT(MC_ServerBD.sCashGrab.iLeaverCashRemovedBS, iParticipant)
			PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - Removing grabbed cash from the total take for part ", iParticipant, " as part no longer active")
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND (NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_ANY_SPECTATOR)
			OR IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
			PRINTLN("[Players_Staggered][Participant: ", iParticipant, "] PROCESS_SERVER_STAGGERED_PLAYER - Non-spectator part has previously reached game state running, but is no longer active: ", iParticipant)
		ENDIF
		#ENDIF
	ENDIF
	
	SERVER_MAINTAIN_MC_LEADERBOARD_FOR_PARTICIPANT(sPlayerState, g_MissionControllerserverBD_LB.sleaderboard)
	
ENDPROC
		
PROC PROCESS_PLAYERS_STAGGERED()
	
	IF iStaggeredPlayerIterator = 0
		PROCESS_PLAYERS_PRE_STAGGERED()
	ENDIF
	
	FMMC_PLAYER_STATE sPlayerState
	FILL_FMMC_PLAYER_STATE_STRUCT(sPlayerState, iStaggeredPlayerIterator)
	
	PROCESS_CLIENT_STAGGERED_PLAYER(sPlayerState)
	PROCESS_SERVER_STAGGERED_PLAYER(sPlayerState)
	
	iStaggeredPlayerIterator++
	
	IF iStaggeredPlayerIterator >= GET_MAXIMUM_PLAYERS_ON_MISSION()
		iStaggeredPlayerIterator = 0
		PROCESS_PLAYERS_POST_STAGGERED()
	ENDIF
	
ENDPROC

PROC PROCESS_SINGLE_FRAME_PARTICIPANT_UPDATE()
	
	PROCESS_PLAYERS_PRE_STAGGERED()
	
	INT iPartLoop = 0
	FMMC_PLAYER_STATE sEmptyStruct
	FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1

		FMMC_PLAYER_STATE sPlayerState = sEmptyStruct
		FILL_FMMC_PLAYER_STATE_STRUCT(sPlayerState, iPartLoop)
		
		PROCESS_CLIENT_STAGGERED_PLAYER(sPlayerState)
		PROCESS_SERVER_STAGGERED_PLAYER(sPlayerState)
		
	ENDFOR
	
	PROCESS_PLAYERS_POST_STAGGERED()
	
ENDPROC
	
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: PROCESS PLAYERS
// ##### Description: The central player processing function
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_PLAYERS()

	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_PLAYERS)
	#ENDIF
	
	PROCESS_PLAYERS_EVERY_FRAME()
	
	PROCESS_PLAYERS_STAGGERED()
	
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_PLAYERS)
	#ENDIF

ENDPROC
