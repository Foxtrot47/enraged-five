// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Ending -----------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description:
// ##### Mission End Stage Processing
// ##### Final Leaderboard
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Include_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Strand Mission and Quick Restart Processing ----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// #####  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_SYNC_CHECKPOINT_TRANSITION_VARIABLES()
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
		EXIT
	ENDIF
	
	INT iTeam = 0 	
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		IF g_TransitionSessionNonResetVars.iRetryMultiRuleTimeStamp[iTeam] != MC_ServerBD_4.iTransitionSessionMultirule_TimeStamp[iTeam]
		AND MC_ServerBD_4.iTransitionSessionMultirule_TimeStamp[iTeam] != 0 // We don't want to clear these when the mission starts, or this hasn't yet been set.
			PRINTLN("[PROCESS_SYNC_CHECKPOINT_TRANSITION_VARIABLES] Updating iRetryMultiRuleTimeStamp to = ", MC_ServerBD_4.iTransitionSessionMultirule_TimeStamp[iTeam])
			g_TransitionSessionNonResetVars.iRetryMultiRuleTimeStamp[iTeam] = MC_ServerBD_4.iTransitionSessionMultirule_TimeStamp[iTeam]			
		ENDIF
		
		IF g_TransitionSessionNonResetVars.iRetryMultiRuleTimeStamp_Checkpoint[iTeam] != MC_ServerBD_4.iTransitionSessionMultirule_Checkpoint[iTeam]
		AND MC_ServerBD_4.iTransitionSessionMultirule_Checkpoint[iTeam] != 0 // We don't want to clear these when the mission starts, or this hasn't yet been set.
			PRINTLN("[PROCESS_SYNC_CHECKPOINT_TRANSITION_VARIABLES] Updating iRetryMultiRuleTimeStamp_Checkpoint to = ", MC_ServerBD_4.iTransitionSessionMultirule_Checkpoint[iTeam])
			g_TransitionSessionNonResetVars.iRetryMultiRuleTimeStamp_Checkpoint[iTeam] = MC_ServerBD_4.iTransitionSessionMultirule_Checkpoint[iTeam]
		ENDIF
	ENDFOR
ENDPROC

PROC CACHE_VEHICLE_SEAT_FOR_CONTINUITY()

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_VehicleSeatTracking)
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(LocalPlayerPed)
		EXIT
	ENDIF
	
	IF IS_ENTITY_DEAD(LocalPlayerPed)
		EXIT
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		EXIT
	ENDIF
		
	VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
	
	INT iVeh = IS_VEH_A_MISSION_CREATOR_VEH(veh)
	
	IF iVeh < 0
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iContinuityID = -1
		EXIT
	ENDIF
	
	sMissionLocalContinuityVars.iEndVehicleContinuityId = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iContinuityID
	sMissionLocalContinuityVars.eEndVehicleSeat = GET_SEAT_PED_IS_IN(localPlayerPed)
	
	PRINTLN("[JS][CONTINUITY] - CACHE_VEHICLE_SEAT_FOR_CONTINUITY - iEndVehicleContinuityId = ", sMissionLocalContinuityVars.iEndVehicleContinuityId)
	PRINTLN("[JS][CONTINUITY] - CACHE_VEHICLE_SEAT_FOR_CONTINUITY - eEndVehicleSeat = ", ENUM_TO_INT(sMissionLocalContinuityVars.eEndVehicleSeat))
	
ENDPROC

PROC MAINTAIN_PLAYER_CONTROL_AT_END_OF_STRAND_CUTSCENE()
	
	IF bIsAnySpectator
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_SET_PLAYER_CONTROL_FOR_BRIDGE_MOCAP)
		EXIT
	ENDIF
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		EXIT
	ENDIF
	
	IF GET_STRAND_MISSION_TRANSITION_TYPE() != FMMC_BRANCHING_MISSION_TRANSITION_TYPE_MOCAP
		EXIT
	ENDIF
	
	IF MC_serverBD.iNextMission < FMMC_MAX_STRAND_MISSIONS
	AND MC_serverBD.iNextMission >= 0
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23NextContentID[MC_serverBD.iNextMission])
		IF g_FMMC_STRUCT.eBranchingTransitionType[MC_serverBD.iNextMission] != FMMC_BRANCHING_MISSION_TRANSITION_TYPE_MOCAP
			EXIT
		ENDIF
	ENDIF
	
	IF NOT IS_CUTSCENE_PLAYING()
		NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
		SET_BIT(iLocalBoolCheck7, LBOOL7_SET_PLAYER_CONTROL_FOR_BRIDGE_MOCAP)
		PRINTLN("[RCC MISSION][MSRAND] - MAINTAIN_PLAYER_CONTROL_AT_END_OF_STRAND_CUTSCENE")
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: RP Reward Calculation ----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC GIVE_LOCAL_PLAYER_FINAL_RP_REWARD_BONUS()
	INT iMinutesPlayed = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer) / 60000
	INT iFinalRP = g_sMPTunables.iGR_ADVERSARY_FINAL_OBJECTIVE_BONUS * iMinutesPlayed
	//Set the bonus XP here
	iBonusXP = iFinalRP
	PRINTLN("[JS] GIVE_LOCAL_PLAYER_FINAL_RP_REWARD_BONUS - Minutes Played = ", iMinutesPlayed, " Multiplier = ", g_sMPTunables.iGR_ADVERSARY_FINAL_OBJECTIVE_BONUS, " Final RP = ", iFinalRP)
	GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, LocalPlayerPed,"XPT_MEDIUMT", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_MISSION_PASS, iFinalRP)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Leaderboards ----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// #####  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL HAS_LEADERBOARD_TIMER_EXPIRED()	
	
	IF NOT HAS_NET_TIMER_STARTED(tdForceEndTimer)
		IF NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING() 
			START_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
		ENDIF
		
		REINIT_NET_TIMER(tdForceEndTimer)
	ELSE
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdForceEndTimer) > VIEW_LEADERBOARD_TIME
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC INT GET_LEADERBOARD_INDEX_OF_TOP_PLAYER_ON_TEAM(INT iteam)
	INT i = 0
	REPEAT COUNT_OF(g_MissionControllerserverBD_LB.sleaderboard) i
		INT iParticipant = g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant
	
		IF iParticipant > -1
			IF MC_Playerbd[iParticipant].iteam = iteam
				RETURN i
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN 0
ENDFUNC

PROC HANDLE_PLAYLIST_POSITION_DATA_WRITE()

	INT i
	IF IS_PLAYER_ON_A_PLAYLIST(LocalPlayer)
		SET_PLAYLIST_LEADERBOARD_ACTIVE(TRUE)
	ENDIF
	IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_TIME	
		FOR i = 0 TO NUM_NETWORK_PLAYERS - 1
			IF g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant > -1
				WRITE_MISSION_LB_DATA_TO_GLOBALS(g_MissionControllerserverBD_LB.sleaderboard[i].playerID,g_MissionControllerserverBD_LB.sleaderboard[i].iTeam,GET_TEAM_FINISH_POSITION(g_MissionControllerserverBD_LB.sleaderboard[i].iTeam),-1,g_MissionControllerserverBD_LB.sleaderboard[i].iMissionTime,g_MissionControllerserverBD_LB.sleaderboard[i].iKills,g_MissionControllerserverBD_LB.sleaderboard[i].iDeaths,g_MissionControllerserverBD_LB.sleaderboard[i].iHeadshots,DUMMY_MODEL_FOR_SCRIPT,MCT_METALLIC,0,MC_playerBD[g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant].iMedalRankScore)
			ENDIF
		ENDFOR
	ELSE
		FOR i = 0 TO NUM_NETWORK_PLAYERS - 1
			IF g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant > -1
				WRITE_MISSION_LB_DATA_TO_GLOBALS(g_MissionControllerserverBD_LB.sleaderboard[i].playerID,g_MissionControllerserverBD_LB.sleaderboard[i].iTeam,GET_TEAM_FINISH_POSITION(g_MissionControllerserverBD_LB.sleaderboard[i].iTeam),g_MissionControllerserverBD_LB.sleaderboard[i].iplayerScore,-1,g_MissionControllerserverBD_LB.sleaderboard[i].iKills,g_MissionControllerserverBD_LB.sleaderboard[i].iDeaths,g_MissionControllerserverBD_LB.sleaderboard[i].iHeadshots,DUMMY_MODEL_FOR_SCRIPT,MCT_METALLIC,0,MC_playerBD[g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant].iMedalRankScore)
			ENDIF
		ENDFOR
	ENDIF
		
ENDPROC

PROC WRITE_TO_MISSION_SC_LB(BOOL bJustSum = FALSE)
	
	TEXT_LABEL_31 categoryNames[1] 
   	TEXT_LABEL_23 uniqueIdentifiers[1]
	INT iPosition
	
	// Don't write if spectating
	IF bIsSCTV
		PRINTLN("WRITE_TO_MISSION_SC_LB bypassed because IS_PLAYER_SCTV")
		EXIT
	ENDIF
	
	INT i
	
	PRINTLN("WRITE_TO_MISSION_SC_LB ") 
	
	IF NOT ARE_STRINGS_EQUAL(g_FMMC_STRUCT.tl31LoadedContentID,"")
		categoryNames[0] = "Mission" 
  		uniqueIdentifiers[0] = g_FMMC_STRUCT.tl31LoadedContentID
		FOR i = 0 TO (MAX_NUM_DM_PLAYERS - 1)
	        IF g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant = iLocalPart
				iPosition = i	
				i = MAX_NUM_DM_PLAYERS
	        ENDIF
		ENDFOR
		
		IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_TIME
			
			// Begin setting the leaderboard data types
			IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_MISSIONS_BY_TIME, uniqueIdentifiers, categoryNames, 1)
				
				//*SCORE_COLUMN( AGG_MAX ) : COLUMN_ID_LB_SCORE - LBCOLUMNTYPE_INT64
			   	//KILLS( AGG_LAST ) : COLUMN_ID_LB_KILLS - LBCOLUMNTYPE_INT32
			   	//DEATHS( AGG_LAST ) : COLUMN_ID_LB_DEATHS - LBCOLUMNTYPE_INT32
			    //KILL_DEATH_RATIO( AGG_LAST ) : COLUMN_ID_LB_KILL_DEATH_RATIO - LBCOLUMNTYPE_DOUBLE
			    //TOTAL_TIME( AGG_SUM ) : COLUMN_ID_LB_TOTAL_TIME - LBCOLUMNTYPE_INT64
			    //TOTAL_KILLS( AGG_SUM ) : COLUMN_ID_LB_KILLS - LBCOLUMNTYPE_INT32
			    //TOTAL_DEATHS( AGG_SUM ) : COLUMN_ID_LB_DEATHS - LBCOLUMNTYPE_INT32
			    //TOTAL_KILL_DEATH_RATIO( AGG_SUM ) : COLUMN_ID_LB_KILL_DEATH_RATIO - LBCOLUMNTYPE_DOUBLE
				IF NOT  bJustSum 

					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,-g_MissionControllerserverBD_LB.sleaderboard[iPosition].iMissionTime,0)
					
					// Write kills
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_KILLS,g_MissionControllerserverBD_LB.sleaderboard[iPosition].iKills,0)
					
					// Write death
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_DEATHS,g_MissionControllerserverBD_LB.sleaderboard[iPosition].iDeaths,0)

				ENDIF
				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_TOTAL_TIME,g_MissionControllerserverBD_LB.sleaderboard[iPosition].iMissionTime,0)
					
					// Write kills
				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_KILLS,g_MissionControllerserverBD_LB.sleaderboard[iPosition].iKills,0)

				// Write death
				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_DEATHS,g_MissionControllerserverBD_LB.sleaderboard[iPosition].iDeaths,0)
				
				//1061136 UN_COMMENT WHEN 1098154 
	        	LEADERBOARDS_WRITE_ADD_COLUMN_LONG(LB_INPUT_COL_MATCH_ID,g_MissionControllerserverBD_LB.iHashMAC ,g_MissionControllerserverBD_LB.iMatchTimeID)
			ENDIF
		ELSE
			IF NOT bJustSum 
				IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_MISSIONS_BY_BEST_SCORE,uniqueIdentifiers,categoryNames,1,-1,TRUE)
				
					INT iScoreCap = g_sMPtunables.iGLOBAL_LEADERBOARD_CAP_CONTACT_MISSIONS
					IF IS_THIS_A_VERSUS_MISSION()
						iScoreCap = g_sMPtunables.iGLOBAL_LEADERBOARD_CAP_ADVERSARY_MODES
					ENDIF
					IF MC_PlayerBD[iLocalPart].iMedalRankScore <= iScoreCap	
					OR iScoreCap = 0
						WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_FREEMODE_MISSIONS_BY_BEST_SCORE),LB_INPUT_COL_SCORE,MC_PlayerBD[iLocalPart].iMedalRankScore,0)
					ENDIF
					
					// Write kills
					WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_FREEMODE_MISSIONS_BY_BEST_SCORE),LB_INPUT_COL_KILLS,g_MissionControllerserverBD_LB.sleaderboard[iPosition].iKills,0)
							
					// Write death
					WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_FREEMODE_MISSIONS_BY_BEST_SCORE),LB_INPUT_COL_DEATHS,g_MissionControllerserverBD_LB.sleaderboard[iPosition].iDeaths,0)
	        		LEADERBOARDS_WRITE_ADD_COLUMN_LONG(LB_INPUT_COL_MATCH_ID,g_MissionControllerserverBD_LB.iHashMAC ,g_MissionControllerserverBD_LB.iMatchTimeID)
				ENDIF
			ENDIF
		ENDIF
		
		// NOW PASS THE DATA IN FOR SUBMISSION
		PRINTLN("[RCC MISSION]  -<>- WRITE_TO_MISSION_SC_LB: Score: ",g_MissionControllerserverBD_LB.sleaderboard[iPosition].iPlayerScore) 
		PRINTLN("[RCC MISSION]  -<>- WRITE_TO_MISSION_SC_LB: Kills: ",g_MissionControllerserverBD_LB.sleaderboard[iPosition].iKills) 
		PRINTLN("[RCC MISSION]  -<>- WRITE_TO_MISSION_SC_LB: Deaths: ",g_MissionControllerserverBD_LB.sleaderboard[iPosition].iDeaths) 
	ELSE
		PRINTLN("[RCC MISSION] WRITE_TO_MISSION_SC_LB: failed g_FMMC_STRUCT.tl31LoadedContentID = ",g_FMMC_STRUCT.tl31LoadedContentID) 
		PRINTLN("[RCC MISSION] WRITE_TO_MISSION_SC_LB: failed GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].tl23CurrentMissionOwner = ", GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].tl23CurrentMissionOwner)
		SCRIPT_ASSERT("Mission trying to write to leaderboard but mission name or creator name not set. See conor")
	ENDIF
ENDPROC


FUNC BOOL DONE_RANK_PREDICTION_READ()
	
	INt iSubtype = -1
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET() 
	AND IS_THIS_A_RSTAR_ACTIVITY()
		SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(scLB_control,GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].iCurrentMissionType,
			g_FMMC_STRUCT.tl31LoadedContentID,
			g_FMMC_STRUCT.tl63MissionName,iSubtype)//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].tl31MissionName)
	ELSE
		RETURN TRUE
	ENDIF
	
	// 2012085
	IF NOT HAS_NET_TIMER_STARTED(timerSocialClubReadTime)
		START_NET_TIMER( timerSocialClubReadTime )
	ELSE
		IF HAS_NET_TIMER_EXPIRED(timerSocialClubReadTime, RANK_PREDICTION_TIMEOUT)
			WRITE_TO_MISSION_SC_LB(FALSE)
			#IF IS_DEBUG_BUILD
				PRINTLN("DONE_RANK_PREDICTION_READ, WRITE_LEADERBOARD_DATA (RANK_PREDICTION_TIMEOUT) " ) 
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ( scLB_rank_predict.bFinishedRead AND NOT scLB_rank_predict.bFinishedWrite )
		IF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_PASSED_MISSION)
			WRITE_TO_MISSION_SC_LB(FALSE)
		ELSE
			WRITE_TO_MISSION_SC_LB(FALSE)
		ENDIF
		scLB_rank_predict.bFinishedWrite = TRUE
	ENDIF
	
	IF GET_RANK_PREDICTION_DETAILS(scLB_control)
		sclb_useRankPrediction = TRUE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: RESULTS ----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SAVE_EOM_VEHCILE()
	
	IF IS_BIT_SET(iLocalBoolCheck, LBOOL_SAVED_EOM_VEHICLE)
		EXIT
	ENDIF
	SET_BIT(iLocalBoolCheck, LBOOL_SAVED_EOM_VEHICLE)
	
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = -1
	OR g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = -1
		PRINTLN("[RCC MISSION] - SAVE_EOM_VEHCILE - not saving vehicle, iPostMissionSceneId is valid.")
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_BLOCK_SAVE_EOM_CAR_WAS_DELIVERED)		
		IF NOT IS_THIS_A_VERSUS_MISSION()
			SAVE_MY_EOM_VEHICLE_DATA(NULL, FALSE, TRUE, 5)			
		ENDIF		
	ELSE
		PRINTLN("[RCC MISSION] - SAVE_EOM_VEHCILE - not saving vehicle, I am in it; received event and LBOOL6_BLOCK_SAVE_EOM_CAR_WAS_DELIVERED is set.")
	ENDIF
	
ENDPROC

FUNC STRING GET_REASON_FOR_PLAYER_FAIL(INT ireason)

	SWITCH ireason
	
		CASE PLAYER_FAIL_OUT_OF_LIVES
			RETURN "LLIVEEXP"
		BREAK
		CASE PLAYER_FAIL_REASON_BOUNDS
			RETURN "POUTBND"
		BREAK
		
	ENDSWITCH

	RETURN "PLYFAIL"
	
ENDFUNC

FUNC BOOL IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID(INT iTeam)
	
	IF iTeam < FMMC_MAX_TEAMS
	AND iTeam >= 0
		
		IF (MC_serverBD.iEntityCausingFail[iteam] > -1)
			
			if MC_serverBD.eCurrentTeamFail[iTeam] = mFail_PED_AGRO_T0
				IF (MC_serverBD.iEntityCausingFail[0] < FMMC_MAX_PEDS)
				AND (MC_serverBD.iEntityCausingFail[0] > -1)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[0]].iCustomPedName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[0]].iCustomPedName])
						RETURN TRUE
					ENDIF
				ELSE	
					RETURN FALSE
				ENDIF
			endif
			if MC_serverBD.eCurrentTeamFail[iTeam] = mFail_PED_AGRO_T1
				IF (MC_serverBD.iEntityCausingFail[1] < FMMC_MAX_PEDS)
				AND (MC_serverBD.iEntityCausingFail[1] > -1)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[1]].iCustomPedName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[1]].iCustomPedName])
						RETURN TRUE
					ENDIF
				ELSE	
					RETURN FALSE
				ENDIF
			endif
			if MC_serverBD.eCurrentTeamFail[iTeam] = mFail_PED_AGRO_T2
				IF (MC_serverBD.iEntityCausingFail[2] < FMMC_MAX_PEDS)
				AND (MC_serverBD.iEntityCausingFail[2] > -1)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[2]].iCustomPedName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[2]].iCustomPedName])
						RETURN TRUE
					ENDIF
				ELSE	
					RETURN FALSE
				ENDIF
			endif
			if MC_serverBD.eCurrentTeamFail[iTeam] = mFail_PED_AGRO_T3
				IF (MC_serverBD.iEntityCausingFail[3] < FMMC_MAX_PEDS)
				AND (MC_serverBD.iEntityCausingFail[3] > -1)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[3]].iCustomPedName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[3]].iCustomPedName])
						RETURN TRUE
					ENDIF
				ELSE	
					RETURN FALSE
				ENDIF
			endif
			
			IF MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PED_ARRIVED
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PED_CAPTURED
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PED_DEAD
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PED_DELIVERED
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PED_FOUND_BODY
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PHOTO_PED
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PROTECTED_PED
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID MC_serverBD.iEntityCausingFail[iteam]: ", MC_serverBD.iEntityCausingFail[iteam])
				IF MC_serverBD.iEntityCausingFail[iteam] > -1
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID iCustomPedName: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[iteam]].iCustomPedName)
				ENDIF
				
				IF (MC_serverBD.iEntityCausingFail[iteam] < FMMC_MAX_PEDS)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[iteam]].iCustomPedName != -1)					
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID tlCustomPedName: ", g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[iteam]].iCustomPedName])
					
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[iteam]].iCustomPedName])
						RETURN TRUE
					ENDIF
				ELSE	
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF MC_serverBD.eCurrentTeamFail[iTeam] = mfail_VEH_CAPTURED
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_VEH_DELIVERED
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_VEH_DEAD
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PROTECTED_VEH
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PHOTO_VEH
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID MC_serverBD.iEntityCausingFail[iteam]: ", MC_serverBD.iEntityCausingFail[iteam])
				IF MC_serverBD.iEntityCausingFail[iteam] > -1
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID iCustomVehName: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_serverBD.iEntityCausingFail[iteam]].iCustomVehName)
				ENDIF
				
				IF (MC_serverBD.iEntityCausingFail[iteam] < FMMC_MAX_VEHICLES)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_serverBD.iEntityCausingFail[iteam]].iCustomVehName != -1)
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID tlCustomVehName: ", g_FMMC_STRUCT_ENTITIES.tlCustomVehName[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_serverBD.iEntityCausingFail[iteam]].iCustomVehName])
					
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomVehName[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_serverBD.iEntityCausingFail[iteam]].iCustomVehName])
						RETURN TRUE
					ENDIF
				ELSE	
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF MC_serverBD.eCurrentTeamFail[iTeam] = mfail_OBJ_CAPTURED
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_OBJ_DELIVERED
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_OBJ_DEAD
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PROTECTED_OBJ
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PHOTO_OBJ
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_HACK_OBJ
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID MC_serverBD.iEntityCausingFail[iteam]: ", MC_serverBD.iEntityCausingFail[iteam])
				IF MC_serverBD.iEntityCausingFail[iteam] > -1
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID iCustomObjName: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_serverBD.iEntityCausingFail[iteam]].iCustomObjName)
				ENDIF
				
				IF (MC_serverBD.iEntityCausingFail[iteam] < FMMC_MAX_NUM_OBJECTS)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_serverBD.iEntityCausingFail[iteam]].iCustomObjName != -1)
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID tlCustomObjName: ", g_FMMC_STRUCT_ENTITIES.tlCustomObjName[g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_serverBD.iEntityCausingFail[iteam]].iCustomObjName])
					
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomObjName[g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_serverBD.iEntityCausingFail[iteam]].iCustomObjName])
						RETURN TRUE
					ENDIF
				ELSE	
					RETURN FALSE
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID RETURN FALSE")
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_CUSTOM_ENTITY_NAME_END_TEXT_VALID(INT iTeam)
	
	IF iTeam < FMMC_MAX_TEAMS
	AND iTeam >= 0
		
		IF (MC_serverBD.iEntityCausingFail[iteam] > -1)
			
			IF ireasonObjEnd[iteam] 	= OBJ_END_REASON_PED_AGRO
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PED_ARRIVED
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PED_CAPTURED
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PED_DEAD
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PED_DELIVERED
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PED_FOUND_BODY
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PHOTO_PED
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PROTECTED_PED
				IF (MC_serverBD.iEntityCausingFail[iteam] < FMMC_MAX_PEDS)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[iteam]].iCustomPedName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[iteam]].iCustomPedName])
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF ireasonObjEnd[iteam] 	= OBJ_END_REASON_VEH_CAPTURED
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_VEH_DELIVERED
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_VEH_DEAD
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PROTECTED_VEH
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PHOTO_VEH
				IF (MC_serverBD.iEntityCausingFail[iteam] < FMMC_MAX_VEHICLES)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_serverBD.iEntityCausingFail[iteam]].iCustomVehName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomVehName[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_serverBD.iEntityCausingFail[iteam]].iCustomVehName])
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF ireasonObjEnd[iteam] 	= OBJ_END_REASON_OBJ_CAPTURED
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_OBJ_DELIVERED
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_OBJ_DEAD
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PROTECTED_OBJ
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PHOTO_OBJ
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_HACK_OBJ
				IF (MC_serverBD.iEntityCausingFail[iteam] < FMMC_MAX_NUM_OBJECTS)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_serverBD.iEntityCausingFail[iteam]].iCustomObjName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomObjName[g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_serverBD.iEntityCausingFail[iteam]].iCustomObjName])
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_END_TEXT_VALID RETURN FALSE")
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_CUSTOM_END_TEXT_VALID(INT iTeam,bool bpass = true)
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_END_TEXT_VALID iTeam: ", iTeam, " bPass: ", bPass)
	
	// if we have an entity causing the fail then check it has a custom name
	IF (MC_serverBD.iEntityCausingFail[iteam] > -1)
		CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_END_TEXT_VALID MC_serverBD.iEntityCausingFail[iteam]: ", MC_serverBD.iEntityCausingFail[iteam])
		
		if bpass
			IF IS_CUSTOM_ENTITY_NAME_END_TEXT_VALID(iTeam)
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_END_TEXT_VALID RETURN TRUE (1)")
				RETURN TRUE
			ENDIF
		else		
			IF IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID(iTeam)
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_END_TEXT_VALID RETURN TRUE (2)")
				RETURN TRUE
			ENDIF			
		endif
	ELSE
		CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_END_TEXT_VALID MC_serverBD.iEntityCausingFail[iteam] = -1")
		
		//check if a custom fail for timer expired is being used.
		IF MC_serverBD.eCurrentTeamFail[iTeam] = mFail_MULTI_TIME_EXPIRED			
			IF NOT IS_STRING_NULL_OR_EMPTY(g_fmmc_struct.tl63MultiRuleTimerFailString[iTeam])
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_END_TEXT_VALID RETURN TRUE (3)")
				RETURN TRUE
			ENDIF	
		ELIF MC_serverBD.eCurrentTeamFail[iTeam] = mFail_TIME_EXPIRED			
			IF iObjEndPriority[iTeam] < FMMC_MAX_RULES
			AND iObjEndPriority[iTeam] >= 0					
				If IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRuleBitsetThree[iObjEndPriority[iTeam]],ciBS_RULE3_OBJECTIVE_TIMER_FAIL1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_fmmc_struct.tl63ObjectiveTimerFailString[iTeam][0])
						CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_END_TEXT_VALID RETURN TRUE (4)")
						RETURN TRUE
					ENDIF
				endif	
				if IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRuleBitsetThree[iObjEndPriority[iTeam]],ciBS_RULE3_OBJECTIVE_TIMER_FAIL2)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_fmmc_struct.tl63ObjectiveTimerFailString[iTeam][1])
						CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_END_TEXT_VALID RETURN TRUE (5)")
						RETURN TRUE
					ENDIF			
				endif
			ENDIF
		ELSE	
			if not (MC_serverBD.eCurrentTeamFail[iTeam] >= mFail_PED_AGRO_T0
			and MC_serverBD.eCurrentTeamFail[iTeam] <= mFail_PED_AGRO_T3)
			// if not entity do we have a custom objective we failed with
				IF iTeam < FMMC_MAX_TEAMS
				AND iTeam >= 0
					IF iObjEndPriority[iTeam] < FMMC_MAX_RULES
					AND iObjEndPriority[iTeam] >= 0
						IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl23ObjSingular[iObjEndPriority[iTeam]])
							CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_END_TEXT_VALID RETURN TRUE (6)")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			endif
		endif		
		
	ENDIF
	
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_END_TEXT_VALID RETURN FALSE")
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CUSTOM_FAIL_REASON_VALID(INT iTeam)
	// make sure custom strings are used for these kinds of fails.
	if not (MC_serverBD.eCurrentTeamFail[iTeam]  >=  mFail_T0MEMBER_DIED		
	and MC_serverBD.eCurrentTeamFail[iTeam] <=  mFail_T3MEMBER_DIED)
	and not (MC_serverBD.eCurrentTeamFail[iTeam]  >=  mFail_HEIST_TEAM_GONE_T0		
	and MC_serverBD.eCurrentTeamFail[iTeam] <=  mFail_HEIST_TEAM_GONE_T3)
	and not (MC_serverBD.eCurrentTeamFail[iTeam]  >=  mFail_HEIST_TEAM_T0_FAILED	
	and MC_serverBD.eCurrentTeamFail[iTeam] <=  mFail_HEIST_TEAM_T3_FAILED)	
	and not (MC_serverBD.eCurrentTeamFail[iTeam] = mFail_TEAM_GONE)
	AND NOT (MC_serverBD.eCurrentTeamFail[iTeam] = mFail_CEO_GONE)
	AND NOT (MC_serverBD.eCurrentTeamFail[iTeam] = mFail_MC_PRESIDENT_GONE)
	AND NOT (MC_serverBD.eCurrentTeamFail[iTeam] = mFail_SCORE_CANT_BE_MATCHED)
	AND NOT (MC_serverBD.eCurrentTeamFail[iTeam] = mFail_LOBBY_LEADER_GONE)
		IF iTeam < FMMC_MAX_TEAMS
		AND iTeam >= 0
		AND iObjEndPriority[iTeam] < FMMC_MAX_RULES
		AND iObjEndPriority[iTeam] >= 0
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63FailReason[iObjEndPriority[iTeam]])
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL] IS_CUSTOM_FAIL_REASON_VALID (TRUE) : custom fail available for team on this rule: ",iObjEndPriority[iTeam])
				RETURN TRUE
			ELSE	
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL] IS_CUSTOM_FAIL_REASON_VALID (FALSE) : custom fail not available for team on this rule: ",iObjEndPriority[iTeam])
			ENDIF
		ENDIF	
	ELSE
		CPRINTLN(DEBUG_MISSION,"[CV_FAIL] IS_CUSTOM_FAIL_REASON_VALID (FALSE) : custom fail block by fail type: ",enum_to_int(MC_serverBD.eCurrentTeamFail[iTeam]))
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC TEXT_LABEL_63 GET_OBJECTIVE_END_FAIL_ENTITY_NAME(INT iTeam, BOOL bCapitalise, BOOL bCustomEntityNameOnly = FALSE)
	
	TEXT_LABEL_63 strLiteral
	BOOL bCustomEntityName = FALSE
	
	IF (MC_serverBD.iEntityCausingFail[iteam] > -1)
		switch MC_serverBD.eCurrentTeamFail[iTeam]
			case mFail_PED_AGRO_T0
				IF (MC_serverBD.iEntityCausingFail[0] > -1)
				AND (MC_serverBD.iEntityCausingFail[0] < FMMC_MAX_PEDS)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[0]].iCustomPedName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[0]].iCustomPedName])
						bCustomEntityName = TRUE
						strLiteral = g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[0]].iCustomPedName]
					ENDIF
				ENDIF
			break 
			case mFail_PED_AGRO_T1
				IF (MC_serverBD.iEntityCausingFail[1] > -1)
				AND (MC_serverBD.iEntityCausingFail[1] < FMMC_MAX_PEDS)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[1]].iCustomPedName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[1]].iCustomPedName])
						bCustomEntityName = TRUE
						strLiteral = g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[1]].iCustomPedName]
					ENDIF
				ENDIF
			break
			case mFail_PED_AGRO_T2
				IF (MC_serverBD.iEntityCausingFail[2] > -1)
				AND (MC_serverBD.iEntityCausingFail[2] < FMMC_MAX_PEDS)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[2]].iCustomPedName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[2]].iCustomPedName])
						bCustomEntityName = TRUE
						strLiteral = g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[2]].iCustomPedName]
					ENDIF
				ENDIF
			break
			case mFail_PED_AGRO_T3
				IF (MC_serverBD.iEntityCausingFail[3] > -1)
				AND (MC_serverBD.iEntityCausingFail[3] < FMMC_MAX_PEDS)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[3]].iCustomPedName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[3]].iCustomPedName])
						bCustomEntityName = TRUE
						strLiteral = g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[3]].iCustomPedName]
					ENDIF
				ENDIF
			break
			case mfail_PED_ARRIVED
			case mfail_PED_CAPTURED
			case mfail_PED_DEAD
			case mfail_PED_DELIVERED
			case mfail_PED_FOUND_BODY
			case mfail_PHOTO_PED
			case mfail_PROTECTED_PED
				IF (MC_serverBD.iEntityCausingFail[iteam] < FMMC_MAX_PEDS)
				AND  (MC_serverBD.iEntityCausingFail[iteam] > -1)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[iteam]].iCustomPedName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[iteam]].iCustomPedName])
						bCustomEntityName = TRUE
						strLiteral = g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[iTeam]].iCustomPedName]
					ENDIF
				ENDIF
			break
			
			case mfail_VEH_CAPTURED
			case mfail_VEH_DELIVERED
			case mfail_VEH_DEAD
			case mfail_PROTECTED_VEH
			case mfail_PHOTO_VEH
				IF (MC_serverBD.iEntityCausingFail[iteam] < FMMC_MAX_VEHICLES)
				AND  (MC_serverBD.iEntityCausingFail[iteam] > -1)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_serverBD.iEntityCausingFail[iteam]].iCustomVehName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomVehName[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_serverBD.iEntityCausingFail[iteam]].iCustomVehName])
						bCustomEntityName = TRUE
						strLiteral = g_FMMC_STRUCT_ENTITIES.tlCustomVehName[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_serverBD.iEntityCausingFail[iteam]].iCustomVehName]
					ENDIF
				ENDIF
			BREAK
			
			case mfail_OBJ_CAPTURED
			case mfail_OBJ_DELIVERED
			case mfail_OBJ_DEAD
			case mfail_PROTECTED_OBJ
			case mfail_PHOTO_OBJ
			case mfail_HACK_OBJ
				IF (MC_serverBD.iEntityCausingFail[iteam] < FMMC_MAX_NUM_OBJECTS)
				AND  (MC_serverBD.iEntityCausingFail[iteam] > -1)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_serverBD.iEntityCausingFail[iteam]].iCustomObjName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomObjName[g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_serverBD.iEntityCausingFail[iteam]].iCustomObjName])
						bCustomEntityName = TRUE
						strLiteral = g_FMMC_STRUCT_ENTITIES.tlCustomObjName[g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_serverBD.iEntityCausingFail[iteam]].iCustomObjName]
					ENDIF
				ENDIF
			BREAK			
		endswitch
	Else
					
		IF MC_serverBD.eCurrentTeamFail[iTeam] = mFail_MULTI_TIME_EXPIRED
			IF NOT IS_STRING_NULL_OR_EMPTY(g_fmmc_struct.tl63MultiRuleTimerFailString[iTeam])
				strLiteral = g_fmmc_struct.tl63MultiRuleTimerFailString[iTeam]
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]GET_OBJECTIVE_END_FAIL_ENTITY_NAME tl63MultiRuleTimerFailString custom text: ",strLiteral)
			ENDIF	
		ELIF MC_serverBD.eCurrentTeamFail[iTeam] = mFail_TIME_EXPIRED			
			IF iObjEndPriority[iTeam] < FMMC_MAX_RULES
			AND iObjEndPriority[iTeam] >= 0							
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRuleBitsetThree[iObjEndPriority[iTeam]],ciBS_RULE3_OBJECTIVE_TIMER_FAIL1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_fmmc_struct.tl63ObjectiveTimerFailString[iTeam][0])
						strLiteral = g_fmmc_struct.tl63ObjectiveTimerFailString[iTeam][0]
						CPRINTLN(DEBUG_MISSION,"[CV_FAIL]GET_OBJECTIVE_END_FAIL_ENTITY_NAME ciBS_RULE3_OBJECTIVE_TIMER_FAIL1 custom text: ",strLiteral)
					ENDIF
				
				ELif IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRuleBitsetThree[iObjEndPriority[iTeam]],ciBS_RULE3_OBJECTIVE_TIMER_FAIL2)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_fmmc_struct.tl63ObjectiveTimerFailString[iTeam][1])
						strLiteral = g_fmmc_struct.tl63ObjectiveTimerFailString[iTeam][1]
						CPRINTLN(DEBUG_MISSION,"[CV_FAIL]GET_OBJECTIVE_END_FAIL_ENTITY_NAME ciBS_RULE3_OBJECTIVE_TIMER_FAIL2 custom text: ",strLiteral)
					ENDIF	
				ENDIF
			ENDIF			
		else		
			IF NOT bCustomEntityNameOnly		
				IF bCapitalise
					strLiteral = GET_STRING_WITH_UPPERCASE_AT_START(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl23ObjSingular[iObjEndPriority[iTeam]])
				ELSE
					strLiteral = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl23ObjSingular[iObjEndPriority[iTeam]]
				ENDIF
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]GET_OBJECTIVE_END_FAIL_ENTITY_NAME no fail entity so grab objective custom text: ",strLiteral)
			ENDIF
		endif
	endif
	
	IF bCustomEntityName
		//If strLiteral is already populated with a custom entity name, and it needs to be capitalised:
		IF bCapitalise
			strLiteral = GET_STRING_WITH_UPPERCASE_AT_START(strLiteral)
		ENDIF
	ENDIF
	
	RETURN strLiteral
	
ENDFUNC

FUNC STRING GET_TEAM_FAIL_REASON(INT iTeam,BOOL& bCapitalise)
		
	IF IS_CUSTOM_FAIL_REASON_VALID(iTeam)			
		RETURN "FAILCUST"		
	ENDIF
	
	BOOL bUseCustomEndText = IS_CUSTOM_END_TEXT_VALID(iTeam,false)
	
	switch MC_serverBD.eCurrentTeamFail[iTeam]
		case  	mFail_LOC_CAPTURED	
			IF bUseCustomEndText
				RETURN "LCAPCUST"
			ELSE
				RETURN "LCAPLOC"
			ENDIF
		break		
		case	mFail_PED_CAPTURED
			if bUseCustomEndText 
				return "LCAPCUST"	
			else 	
				return 	"LCAPPED"		
			endif		
		break
		case	mFail_VEH_CAPTURED
			if bUseCustomEndText 
				return "LCAPCUST"	
			else 	
				return 	"LCAPVEH"		
			endif	
		break
		case	mFail_OBJ_CAPTURED
			if bUseCustomEndText 
				return "LCAPCUST"	
			else 	
				return 	"LCAPOBJ"		
			endif	
		break
		case	mFail_PED_DELIVERED
			IF bUseCustomEndText
				RETURN "LDELCUST"
			ELSE
				RETURN "LDELPED"
			ENDIF		
		break
		case	mFail_VEH_DELIVERED
			IF bUseCustomEndText
				RETURN "LDELCUST"
			ELSE
				RETURN "LDELVEH"
			ENDIF		
		break
		case	mFail_OBJ_DELIVERED
			if bUseCustomEndText 
				return "LDELCUST"	
			else 	
				return 	"LDELOBJ"		
			endif		
		break
		case	mFail_PED_DEAD	
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "LDEADPCUST"
			ELSE
				RETURN "LDEADPED"
			ENDIF
		break
		case	mFail_VEH_DEAD
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "LDESCUST"
			ELSE
				RETURN "LDEADVEH"
			ENDIF	
		break
		case	mFail_OBJ_DEAD
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "LDESCUST"
			ELSE
				RETURN "LDEADOBJ"
			ENDIF	
		break
		case	mFail_PLAYERS_KILLED
			RETURN "LPLYKILL"
		break
		case	mFail_PROTECTED_PED
			IF bUseCustomEndText
				RETURN "LPROCUST"
			ELSE
				RETURN "LPROPED"
			ENDIF	
		break
		case	mFail_PROTECTED_VEH
			IF bUseCustomEndText
				RETURN "LPROCUST"
			ELSE
				RETURN "LPROVEH"
			ENDIF	
		break
		case	mFail_PROTECTED_OBJ
			if bUseCustomEndText 
				return "LPROCUST"	
			else 	
				return 	"LPROOBJ"		
			endif	
		break
		case	mFail_ARV_LOC
			IF bUseCustomEndText
				RETURN "LARVCUST"
			ELSE
				RETURN "LARVLOC"
			ENDIF	
		break
		case	mFail_ARV_PED
			if bUseCustomEndText 
				return "LARVCUST"	
			else 	
				return 	"LARVPED"		
			endif	
		break
		case	mFail_ARV_VEH
			if bUseCustomEndText 
				return "LARVCUST"	
			else 	
				return 	"LARVVEH"		
			endif	
		break
		case	mFail_ARV_OBJ
			if bUseCustomEndText 
				return "LARVCUST"	
			else 	
				return 	"LARVOBJ"		
			endif	
		break
		case	mFail_TIME_EXPIRED
		case	mFail_MULTI_TIME_EXPIRED
			if bUseCustomEndText
				Return "FAILCUST"
			else
				RETURN "LTIMEEXP"
			endif
		break
		
		case	mFail_TARGET_SCORE				
			return 	"LTRGTSCORE"	
		break
		case	mFail_PHOTO_LOC	
		case	mFail_PHOTO_PED	
		case	mFail_PHOTO_VEH	
		case	mFail_PHOTO_OBJ			
			if bUseCustomEndText 
				return "LPHOTOCUST"	
			else 	
				return 	"LPHOTO"		
			endif	
		break
		case	mFail_HACK_OBJ		
			if bUseCustomEndText 
				return "LHACKCUST"	
			else 	
				return 	"LHACK"		
			endif	
		break
		case	mFail_OUT_OF_LIVES		
			RETURN "LLIVESOBJ"	
		break
		case	mFail_PED_ARRIVED		
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "LPEDARVCUST"
			ELSE
				RETURN "LPEDARV"
			ENDIF	
		break
		case	mFail_TEAM_GONE			
			IF NOT DOES_TEAM_LIKE_TEAM(MC_playerBD[iPartToUse].iteam,iTeam)				
				RETURN "LOTHTEAMQ"			
			ELSE				
				RETURN "LMYTEAMQ"				
			ENDIF
		break
		case	mFail_LOST_COPS	
			return 	"LLSECOPS"	
		break
		case	mFail_PLAYERS_GOT_MASKS	
			RETURN "LPLYMASK"
		break
		case	mFail_CHARM_PED			
			if bUseCustomEndText 
				return "LCHARMCUST"	
			else 	
				return 	"LCHARM"		
			endif	
		break
		case	mFail_PED_AGRO_T0	
		case	mFail_PED_AGRO_T1	
		case	mFail_PED_AGRO_T2	
		case	mFail_PED_AGRO_T3	
			IF (MC_serverBD.iPartCausingFail[iTeam] = iPartToUse)			
				IF bUseCustomEndText					
					RETURN "LAGYUCUST"//you alerted ~a~	
				ELSE
					RETURN "LAGGROYOU"//You alerted the target.
				ENDIF				
			ELSE //Somebody else alerted the target~a~:				
				IF bUseCustomEndText					
					RETURN "LAGRPRTC" //~a~ alerted ~a~.
				ELSE
					RETURN "LAGGROPART" // ~a~ alerted the target.
				ENDIF				
			ENDIF
		break
		case	mFail_PED_FOUND_BODY	
			IF bUseCustomEndText	
				bCapitalise = TRUE
				RETURN "LBODYCUST"
			ELSE
				RETURN "LBODYAGGR"
			ENDIF	
		break
		
		case	mFail_HEIST_LEADER_LEFT		
			RETURN "LHEISTLL"	
		break
		case	mFail_HEIST_TEAM_GONE			
			IF ( (MC_serverBD.iPartCausingFail[iTeam] != -1) AND (MC_serverBD.iPartCausingFail[iTeam] != iPartToUse) )
				IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T0) and iTeam = 0)
				OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T1) and iTeam = 1)
				OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T2) and iTeam = 2)
				OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T3) and iTeam = 3)
					RETURN "LHEISTTLFT"
				ELSE
					RETURN "LHSTLFT"
				ENDIF
			ELSE
				RETURN "LMYTEAMQ"
			ENDIF
		break
		case	mFail_HEIST_TEAM_GONE_T0	
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T0)
				RETURN "LHEISTTLFT"
			ELSE
				RETURN "LHSTLFT"
			ENDIF
		break
		case	mFail_HEIST_TEAM_GONE_T1	
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T1)
				RETURN "LHEISTTLFT"
			ELSE
				RETURN "LHSTLFT"
			ENDIF	
		break
		case	mFail_HEIST_TEAM_GONE_T2	
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T2)
				RETURN "LHEISTTLFT"
			ELSE
				RETURN "LHSTLFT"
			ENDIF	
		break
		case	mFail_HEIST_TEAM_GONE_T3	
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T3)
				RETURN "LHEISTTLFT"
			ELSE
				RETURN "LHSTLFT"
			ENDIF	
		break
		case 	mFail_YOU_LEFT
			return "LYOUTEAMLFT"			
		break
		case	mFail_LVE_LOC	
			return 	"LLVELOC"	
		break
		case	mFail_OUT_OF_BOUNDS			
			return "LOOB"				
		break
		
		case	mFail_GAINED_WANTED			
			IF MC_serverBD.iPartCausingFail[iTeam] != -1
				IF MC_serverBD.iPartCausingFail[iTeam] = iPartToUse
					RETURN "GNWTDLYOU" // You were spotted by the Cops
				ELSE
					RETURN "GNWTDLPART" // ~a~ was spotted by the Cops
				ENDIF
			ELSE
				
				RETURN "GAINWANTEDL" // The Cops were alerted
				
			ENDIF
		break
		
		case	mFail_NO_AMMO				
			RETURN "FNOAMMO"
		break
		case	mFail_MOD_SHOP_CLOSED				
			RETURN "FMDSHP"
		break
		case	mFail_LOCATE_WRONG_ANGLE	
			RETURN "LWRONGANGLE"
		break
		case	mFail_T0MEMBER_DIED
		case	mFail_T1MEMBER_DIED
		case	mFail_T2MEMBER_DIED
		case	mFail_T3MEMBER_DIED
			
			if bUseCustomEndText
				bCapitalise = TRUE
				IF MC_serverBD.iPartCausingFail[iTeam] = -1
				and not IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T0 + ( ENUM_TO_INT(MC_serverBD.eCurrentTeamFail[iTeam]) - ENUM_TO_INT(mFail_T0MEMBER_DIED)))
					return "LHMEMDIECUS"
				ELIF MC_serverBD.iPartCausingFail[iTeam] = iLocalPart
					return "LYOUDIED"
				ELSE
					return "LHEISTDIECUS"
				ENDIF
			else
				IF MC_serverBD.iPartCausingFail[iTeam] = -1
					return "LTEAMDIE"
				ELIF MC_serverBD.iPartCausingFail[iTeam] = iLocalPart
					return "LYOUDIED"
				ELSE
					return "LHEISTDIECUS"
				ENDIF
			endif	
		break
		
		CASE	mFail_HEIST_TEAM_T0_FAILED	
		CASE	mFail_HEIST_TEAM_T1_FAILED
		CASE	mFail_HEIST_TEAM_T2_FAILED
		CASE	mFail_HEIST_TEAM_T3_FAILED
		
			BOOL bTeam
			IF MC_serverBD.eCurrentTeamFail[iTeam] = mFail_HEIST_TEAM_T0_FAILED
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T0)
					bTeam = TRUE
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_RemoveTheFromTeamFail_Team0)
					RETURN "LHEISTTG3"
				ENDIF
			ELIF MC_serverBD.eCurrentTeamFail[iTeam] = mFail_HEIST_TEAM_T1_FAILED
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T1)
					bTeam = TRUE
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_RemoveTheFromTeamFail_Team1)
					RETURN "LHEISTTG3"
				ENDIF
			ELIF MC_serverBD.eCurrentTeamFail[iTeam] = mFail_HEIST_TEAM_T2_FAILED
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T2)
					bTeam = TRUE
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_RemoveTheFromTeamFail_Team2)
					RETURN "LHEISTTG3"
				ENDIF
			ELIF MC_serverBD.eCurrentTeamFail[iTeam] = mFail_HEIST_TEAM_T3_FAILED
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T3)
					bTeam = TRUE
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_RemoveTheFromTeamFail_Team3)
					RETURN "LHEISTTG3"
				ENDIF
			ENDIF	
			
			IF !bTeam
				RETURN "LHEISTTG"
			ELSE
				RETURN "LHEISTTG2"
			ENDIF
						
		BREAK
		
		case	mFail_CEO_GONE
			RETURN "LMYCEOQ"
			
		case	mFail_MC_PRESIDENT_GONE
			RETURN "LMYMCPQ"
			
		case	mFail_SCORE_CANT_BE_MATCHED						
			RETURN "LCMOTS"
	
		case	mFail_ALL_TEAMS_FAIL
			RETURN "ATSAF"
		
		case	mFail_OTHER_TEAM_HAS_ALL_OBJECTS
			RETURN "AOWCC"
			
		case	mFail_LOBBY_LEADER_GONE
			RETURN GET_LOBBY_LEADER_GONE_FAIL_TEXT()
	endswitch
	
	RETURN "LMISSOVER"
	
ENDFUNC

FUNC STRING GET_REASON_FOR_OBJECTIVE_END(Bool bpass,INT iTeam,BOOL& bCapitalise)
	if not bpass
		return GET_TEAM_FAIL_REASON(iTeam,bCapitalise)
	endif
	
	BOOL bUseCustomEndText = IS_CUSTOM_END_TEXT_VALID(iTeam)
	
	SWITCH ireasonObjEnd[iTeam]
	
		CASE OBJ_END_REASON_LOC_CAPTURED
			IF bUseCustomEndText
				RETURN "WCAPCUST"
			ELSE
				RETURN "WCAPLOC"
			ENDIF
		BREAK
		
		CASE OBJ_END_REASON_PED_CAPTURED			
			IF bUseCustomEndText
				RETURN "WCAPCUST"
			ELSE
				RETURN "WCAPPED"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_VEH_CAPTURED			
			IF bUseCustomEndText
				RETURN "WCAPCUST"
			ELSE
				RETURN "WCAPVEH"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_OBJ_CAPTURED			
			IF bUseCustomEndText
				RETURN "WCAPCUST"
			ELSE
				RETURN "WCAPOBJ"
			ENDIF			
		BREAK		
		
		CASE OBJ_END_REASON_PED_DELIVERED			
			IF bUseCustomEndText
				RETURN "WDELCUST"
			ELSE
				RETURN "WDELPED"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_VEH_DELIVERED
			IF bUseCustomEndText
				RETURN "WDELCUST"
			ELSE
				RETURN "WDELVEH"
			ENDIF		
		BREAK
		
		CASE OBJ_END_REASON_OBJ_DELIVERED
			IF bUseCustomEndText
				RETURN "WDELCUST"
			ELSE
				RETURN "WDELOBJ"
			ENDIF		
		BREAK
		
		CASE OBJ_END_REASON_PED_DEAD			
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "WDEADPCUST"
			ELSE
				RETURN "WDEADPED"
			ENDIF			
		BREAK	
		
		CASE OBJ_END_REASON_VEH_DEAD			
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "WDESCUST"
			ELSE
				RETURN "WDEADVEH"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_OBJ_DEAD
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "WDESCUST"
			ELSE
				RETURN "WDEADOBJ"
			ENDIF
		BREAK
		
		CASE OBJ_END_REASON_TIME_EXPIRED
			RETURN "WTIMEEXP"			
		BREAK	
		
		CASE OBJ_END_REASON_PLAYERS_KILLED
			RETURN "WPLYKILL"			
		BREAK
		
		CASE OBJ_END_REASON_PLAYERS_GOT_MASKS
			RETURN "WPLYMASK"			
		BREAK
		
		CASE OBJ_END_REASON_PROTECTED_PED
			IF bUseCustomEndText
				RETURN "WPROCUST"
			ELSE
				RETURN "WPROPED"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_PROTECTED_VEH			
			IF bUseCustomEndText
				RETURN "WPROCUST"
			ELSE
				RETURN "WPROVEH"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_PROTECTED_OBJ			
			IF bUseCustomEndText
				RETURN "WPROCUST"
			ELSE
				RETURN "WPROOBJ"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_ARV_LOC			
			IF bUseCustomEndText
				RETURN "WARVCUST"
			ELSE
				RETURN "WARVLOC"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_ARV_PED			
			IF bUseCustomEndText
				RETURN "WARVCUST"
			ELSE
				RETURN "WARVPED"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_ARV_VEH			
			IF bUseCustomEndText
				RETURN "WARVCUST"
			ELSE
				RETURN "WARVVEH"
			ENDIF			
		BREAK	
		
		CASE OBJ_END_REASON_ARV_OBJ			
			IF bUseCustomEndText
				RETURN "WARVCUST"
			ELSE
				RETURN "WARVOBJ"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_OUT_OF_LIVES			
			RETURN "WLIVESOBJ"			
		BREAK
		
		CASE OBJ_END_REASON_TEAM_GONE
			IF NOT DOES_TEAM_LIKE_TEAM(MC_playerBD[iPartToUse].iteam,iTeam)				
				RETURN "WOTHTEAMQ"				
			ELSE				
				RETURN "WMYTEAMQ"				
			ENDIF
		BREAK
		
		CASE OBJ_END_REASON_HEIST_TEAM_GONE
			RETURN "LHEISTTG"
		BREAK
		
		CASE OBJ_END_REASON_HEIST_TEAM_GONE_T0
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T0)
				RETURN "LHEISTTG2"
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_RemoveTheFromTeamFail_Team0)
				RETURN "LHEISTTG3"
			ELSE
				RETURN "LHEISTTG"
			ENDIF
		BREAK
		
		CASE OBJ_END_REASON_HEIST_TEAM_GONE_T1
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T1)
				RETURN "LHEISTTG2"
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_RemoveTheFromTeamFail_Team1)
				RETURN "LHEISTTG3"
			ELSE
				RETURN "LHEISTTG"
			ENDIF
		BREAK
		
		CASE OBJ_END_REASON_HEIST_TEAM_GONE_T2
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T2)
				RETURN "LHEISTTG2"
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_RemoveTheFromTeamFail_Team2)
				RETURN "LHEISTTG3"
			ELSE
				RETURN "LHEISTTG"
			ENDIF
		BREAK
		
		CASE OBJ_END_REASON_HEIST_TEAM_GONE_T3
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T3)
				RETURN "LHEISTTG2"
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_RemoveTheFromTeamFail_Team3)
				RETURN "LHEISTTG3"
			ELSE
				RETURN "LHEISTTG"
			ENDIF
		BREAK
		
		CASE OBJ_END_REASON_HEIST_LEADER_LEFT
			RETURN "LHEISTLL"
		BREAK
		
		CASE OBJ_END_REASON_GAINED_WANTED
			IF MC_serverBD.iPartCausingFail[iTeam] != -1
				IF (MC_serverBD.iPartCausingFail[iTeam] = iPartToUse)
					RETURN "GNWTDLYOU" // You were spotted by the Cops
				ELSE
					RETURN "GNWTDLPART" // ~a~ was spotted by the Cops
				ENDIF
			ELSE
				
				RETURN "GAINWANTEDL"
				
			ENDIF
		BREAK
		
		CASE OBJ_END_REASON_NO_AMMO
			RETURN "FNOAMMO"
		BREAK			

		CASE OBJ_END_REASON_PED_ARRIVED			
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "WPEDARVCUST"
			ELSE
				RETURN "WPEDARV"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_PED_AGRO			
			IF MC_serverBD.iPartCausingFail[iTeam] = -1				
				IF bUseCustomEndText
					bCapitalise = TRUE
					RETURN "WAGGROCUST"
				ELSE
					RETURN "WAGGROPED"
				ENDIF				
			ELIF (MC_serverBD.iPartCausingFail[iTeam] = iPartToUse)				
				IF bUseCustomEndText
					bCapitalise = TRUE
					RETURN "WAGYUCUST"
				ELSE
					RETURN "WAGGROYOU"
				ENDIF				
			ELSE //Somebody else spooked the target:				
				IF bUseCustomEndText
					bCapitalise = TRUE
					RETURN "WAGRPRTC"
				ELSE
					RETURN "WAGGROPART"
				ENDIF				
			ENDIF
		BREAK
		
		CASE OBJ_END_REASON_MOD_SHOP_CLOSED
			RETURN "FMDSHP"
		BREAK
		
		CASE OBJ_END_REASON_PED_FOUND_BODY	
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "WBODYCUST"
			ELSE
				RETURN "WBODYAGGR"
			ENDIF
		BREAK
		
		CASE OBJ_END_REASON_PED_DAMAGED
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "WDAMAPCUST"
			ELSE
				RETURN "LDAMAPED"
			ENDIF	
		BREAK
		
		CASE OBJ_END_REASON_VEH_DAMAGED
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "WDAMAVCUST"
			ELSE
				RETURN "LDAMAVEH"
			ENDIF	
		BREAK
		
		CASE OBJ_END_REASON_OBJ_DAMAGED
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "WDAMAOCUST"
			ELSE
				RETURN "LDAMAOBJ"
			ENDIF	
		BREAK
		
		CASE OBJ_END_REASON_LOCATE_WRONG_ANGLE
			RETURN "LWRONGANGLE"
		BREAK
		
	ENDSWITCH
	
	RETURN "LMISSOVER"
	
ENDFUNC

PROC POPULATE_FAIL_RESULTS()
	
	IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_CALLED_POPULATE_FAIL_RESULTS)
		
		PRINTLN("[RCC MISSION] POPULATE_FAIL_RESULTS - This has already been called before in this mission, exit!")
		EXIT
		
	ENDIF
	
	sCelebrationStats.strFailReason = ""
	sCelebrationStats.strFailLiteral = ""
	sCelebrationStats.strFailLiteral2 = ""
	
	INT iFailTeam
	
	IF bIsSCTV //if spectating use the team that your spectating 
		iFailTeam = MC_playerBD[iPartToUse].iteam			
	ELSE
		iFailTeam = MC_playerBD[iLocalPart].iteam	
	ENDIF
	
	PRINTLN("[RCC MISSION] POPULATE_FAIL_RESULTS - Getting end reasons S0: init fail team is my own, team ",iFailTeam)
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL]before eCurrentTeamFail[",iFailTeam,"] = ",enum_to_int(MC_serverBD.eCurrentTeamFail[iFailTeam]))	
	
	//Check to blame or use same fail for all			
	if MC_serverBD.eCurrentTeamFail[iFailTeam] = mFail_HEIST_TEAM_T0_FAILED
		if (iFailTeam != 1 and  MC_serverBD.eCurrentTeamFail[0] = MC_serverBD.eCurrentTeamFail[1])
		or (iFailTeam != 2 and  MC_serverBD.eCurrentTeamFail[0] = MC_serverBD.eCurrentTeamFail[2])
		or (iFailTeam != 3 and  MC_serverBD.eCurrentTeamFail[0] = MC_serverBD.eCurrentTeamFail[3])
			CPRINTLN(DEBUG_MISSION,"[CV_FAIL]More than one team has failed with ",enum_to_int(MC_serverBD.eCurrentTeamFail[0]) ," change MC_serverBD.eCurrentTeamFail[",iFailTeam,"] to MC_serverBD.eCurrentTeamFail[0]:",enum_to_int(MC_serverBD.eCurrentTeamFail[0]))
			MC_serverBD.eCurrentTeamFail[iFailTeam] = MC_serverBD.eCurrentTeamFail[0]
			//update the fail entity to be the same as the team with the orginal fail.
			IF (MC_serverBD.iEntityCausingFail[0] != -1)
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]update fail entity for team[",iFailTeam ,"] to match team 0's fail entity")
				MC_serverBD.iEntityCausingFail[iFailTeam] = MC_serverBD.iEntityCausingFail[0]
			endif
		endif
	elif MC_serverBD.eCurrentTeamFail[iFailTeam] = mFail_HEIST_TEAM_T1_FAILED
		if (iFailTeam != 0 and  MC_serverBD.eCurrentTeamFail[1] = MC_serverBD.eCurrentTeamFail[0])
		or (iFailTeam != 2 and  MC_serverBD.eCurrentTeamFail[1] = MC_serverBD.eCurrentTeamFail[2])
		or (iFailTeam != 3 and  MC_serverBD.eCurrentTeamFail[1] = MC_serverBD.eCurrentTeamFail[3])
			CPRINTLN(DEBUG_MISSION,"[CV_FAIL]More than one team has failed with ",enum_to_int(MC_serverBD.eCurrentTeamFail[1]) ," change MC_serverBD.eCurrentTeamFail[",iFailTeam,"] to MC_serverBD.eCurrentTeamFail[1]:",enum_to_int(MC_serverBD.eCurrentTeamFail[1]))
			MC_serverBD.eCurrentTeamFail[iFailTeam] = MC_serverBD.eCurrentTeamFail[1]
			//update the fail entity to be the same as the team with the orginal fail.
			IF (MC_serverBD.iEntityCausingFail[1] != -1)
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]update fail entity for team[",iFailTeam ,"] to match team 1's fail entity")
				MC_serverBD.iEntityCausingFail[iFailTeam] = MC_serverBD.iEntityCausingFail[1]
			endif
		endif
	elif MC_serverBD.eCurrentTeamFail[iFailTeam] = mFail_HEIST_TEAM_T2_FAILED
		if (iFailTeam != 1 and  MC_serverBD.eCurrentTeamFail[2] = MC_serverBD.eCurrentTeamFail[1])
		or (iFailTeam != 0 and  MC_serverBD.eCurrentTeamFail[2] = MC_serverBD.eCurrentTeamFail[0])
		or (iFailTeam != 3 and  MC_serverBD.eCurrentTeamFail[2] = MC_serverBD.eCurrentTeamFail[3])
			CPRINTLN(DEBUG_MISSION,"[CV_FAIL]More than one team has failed with ",enum_to_int(MC_serverBD.eCurrentTeamFail[2]) ," change MC_serverBD.eCurrentTeamFail[",iFailTeam,"] to MC_serverBD.eCurrentTeamFail[2]:",enum_to_int(MC_serverBD.eCurrentTeamFail[2]))
			MC_serverBD.eCurrentTeamFail[iFailTeam] = MC_serverBD.eCurrentTeamFail[2]
			//update the fail entity to be the same as the team with the orginal fail.
			IF (MC_serverBD.iEntityCausingFail[2] != -1)
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]update fail entity for team[",iFailTeam ,"] to match team 2's fail entity")
				MC_serverBD.iEntityCausingFail[iFailTeam] = MC_serverBD.iEntityCausingFail[2]
			endif
		endif
	elif MC_serverBD.eCurrentTeamFail[iFailTeam] = mFail_HEIST_TEAM_T3_FAILED
		if (iFailTeam != 1 and  MC_serverBD.eCurrentTeamFail[3] = MC_serverBD.eCurrentTeamFail[1])
		or (iFailTeam != 2 and  MC_serverBD.eCurrentTeamFail[3] = MC_serverBD.eCurrentTeamFail[2])
		or (iFailTeam != 0 and  MC_serverBD.eCurrentTeamFail[3] = MC_serverBD.eCurrentTeamFail[0])
			CPRINTLN(DEBUG_MISSION,"[CV_FAIL]More than one team has failed with ",enum_to_int(MC_serverBD.eCurrentTeamFail[3]) ," change MC_serverBD.eCurrentTeamFail[",iFailTeam,"] to MC_serverBD.eCurrentTeamFail[3]:",enum_to_int(MC_serverBD.eCurrentTeamFail[3]))
			MC_serverBD.eCurrentTeamFail[iFailTeam] = MC_serverBD.eCurrentTeamFail[3]
			//update the fail entity to be the same as the team with the orginal fail.
			IF (MC_serverBD.iEntityCausingFail[3] != -1)
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]update fail entity for team[",iFailTeam ,"] to match team 3's fail entity")
				MC_serverBD.iEntityCausingFail[iFailTeam] = MC_serverBD.iEntityCausingFail[3]
			endif
		endif
	endif
	
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL]after eCurrentTeamFail[",iFailTeam,"] = ",enum_to_int(MC_serverBD.eCurrentTeamFail[iFailTeam]))	
	
	// get the end objective for custom text
	IF iObjEndPriority[iFailTeam] = -1
	or iObjEndPriority[iFailTeam] > FMMC_MAX_RULES
		iObjEndPriority[iFailTeam] = MC_serverBD_4.iCurrentHighestPriority[iFailTeam]
		CPRINTLN(DEBUG_MISSION,"[CV_FAIL] MC_serverBD_4.iCurrentHighestPriority[",iFailTeam,"] being used for iObjEndPriority[",iFailTeam,"] = ",iObjEndPriority[iFailTeam])
	ENDIF
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL] iObjEndPriority[",iFailTeam,"] = ",iObjEndPriority[iFailTeam])

	bool bCapitalise
	//get the end text template depending on current fail end
	sObjEndText = GET_REASON_FOR_OBJECTIVE_END(false,iFailTeam,bCapitalise)
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL]1 GET_REASON_FOR_OBJECTIVE_END: eCurrentTeamFail[",iFailTeam,"] = ",sObjEndText)
	
	BOOL bCustomFail
	
	IF IS_CUSTOM_END_TEXT_VALID(iFailTeam,false)
	OR IS_CUSTOM_FAIL_REASON_VALID(iFailTeam)
		
		IF iObjEndPriority[iFailTeam] < FMMC_MAX_RULES
			CPRINTLN(DEBUG_MISSION,"[CV_FAIL]2 iObjEndPriority[iFailTeam]= ",iObjEndPriority[iFailTeam]," < FMMC_MAX_RULES")
			//check for custom fail reason 
			// death and quit higher priority here
			IF IS_CUSTOM_FAIL_REASON_VALID(iFailTeam)			
				sCelebrationStats.strFailReason = sObjEndText//"STRING" fail reason here should be custom already FAILCUST: ~a~
				sCelebrationStats.strFailLiteral = g_FMMC_STRUCT.sFMMCEndConditions[iFailTeam].tl63FailReason[iObjEndPriority[iFailTeam]]
				bCustomFail = TRUE
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]3 USING CUSTOM FAIL: ",sCelebrationStats.strFailLiteral )
			ELSE
				// fill the ~a~ in the fail texts e.g.Failed to hack the ~a~.
				sCelebrationStats.strFailReason = sObjEndText
				
				SWITCH MC_serverBD.eCurrentTeamFail[iFailTeam]
					
					CASE mFail_T0MEMBER_DIED
					CASE mFail_T1MEMBER_DIED
					CASE mFail_T2MEMBER_DIED
					CASE mFail_T3MEMBER_DIED
						// if member dead then only update fail text to use actual name
						IF MC_serverBD.iPartCausingFail[iFailTeam] != -1
						AND MC_serverBD.iPartCausingFail[iFailTeam] != iLocalPart
							sCelebrationStats.strFailLiteral = MC_serverBD_2.tParticipantNames[MC_serverBD.iPartCausingFail[iFailTeam]]
							CPRINTLN(DEBUG_MISSION,"[CV_FAIL]4 adding player who died: ",sCelebrationStats.strFailLiteral )								
						ENDIF
					BREAK
					
					CASE mFail_PED_AGRO_T0
					CASE mFail_PED_AGRO_T1
					CASE mFail_PED_AGRO_T2
					CASE mFail_PED_AGRO_T3
						//Ped Agro end message check
						//first check if the player caused fail
						if (MC_serverBD.iPartCausingFail[iFailTeam] = iPartToUse)  
							// add 1 literal strings e.g. You spooked ~a~ 
							sCelebrationStats.strFailLiteral = GET_OBJECTIVE_END_FAIL_ENTITY_NAME(iFailTeam,bCapitalise)	// the target name	e.g. the biker
							CPRINTLN(DEBUG_MISSION,"[CV_FAIL]5a Add objective entity title, target: ",sCelebrationStats.strFailLiteral )
						ELSE								
							// add 2 literal strings e.g. ~a~ spooked ~a~ 
							IF (MC_serverBD.iPartCausingFail[iFailTeam] != -1)	//check for player name
								sCelebrationStats.strFailLiteral = MC_serverBD_2.tParticipantNames[MC_serverBD.iPartCausingFail[iFailTeam]] //person who spooked target
								CPRINTLN(DEBUG_MISSION,"[CV_FAIL]5b1 caused by player: ",sCelebrationStats.strFailLiteral)
							endif
							
							if (MC_serverBD.iPartCausingFail[iFailTeam] = -1) //use team name instead
							or IS_STRING_NULL_OR_EMPTY(sCelebrationStats.strFailLiteral)
								if MC_serverBD.eCurrentTeamFail[iFailTeam] =  mFail_PED_AGRO_T0 
									sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(0,false,false)
								elif MC_serverBD.eCurrentTeamFail[iFailTeam] =  mFail_PED_AGRO_T1
									sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(1,false,false)
								elif MC_serverBD.eCurrentTeamFail[iFailTeam] =  mFail_PED_AGRO_T2 
									sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(2,false,false)
								elif MC_serverBD.eCurrentTeamFail[iFailTeam] =  mFail_PED_AGRO_T3 
									sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(3,false,false)
								endif		
								CPRINTLN(DEBUG_MISSION,"[CV_FAIL]5b2 caused by player team: ",sCelebrationStats.strFailLiteral)
							ENDIF
							//target 
							sCelebrationStats.strFailLiteral2 = GET_OBJECTIVE_END_FAIL_ENTITY_NAME(iFailTeam,bCapitalise)		
							CPRINTLN(DEBUG_MISSION,"[CV_FAIL]5c caused by / target: ",sCelebrationStats.strFailLiteral," / ",sCelebrationStats.strFailLiteral2 )
						ENDIF
					BREAK
					
					DEFAULT
						sCelebrationStats.strFailLiteral = GET_OBJECTIVE_END_FAIL_ENTITY_NAME(iFailTeam,bCapitalise) // grab only one ~a~
						CPRINTLN(DEBUG_MISSION,"[CV_FAIL]6 Add custom title from objective: ",iObjEndPriority[iFailTeam]," sCelebrationStats.strFailLiteral: ",sCelebrationStats.strFailLiteral )
					BREAK
					
				ENDSWITCH
				
			ENDIF
			
		ELSE
			//no objective for team currently but custom entity is used
			sCelebrationStats.strFailReason = sObjEndText					
			IF IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID(iFailTeam)
				sCelebrationStats.strFailLiteral = GET_OBJECTIVE_END_FAIL_ENTITY_NAME(iFailTeam,bCapitalise,TRUE)
			ENDIF
			CPRINTLN(DEBUG_MISSION,"[CV_FAIL]7 no objective but custom used: ",sCelebrationStats.strFailLiteral )

		ENDIF
	ELSE
		// not using custom fail text 
		sCelebrationStats.strFailReason = sObjEndText
		CPRINTLN(DEBUG_MISSION,"[CV_FAIL]8a not custom sObjEndText: ",sObjEndText )
		
		// but agro fail message can take a string for who spooked the target or died
		SWITCH MC_serverBD.eCurrentTeamFail[iFailTeam]
			CASE mFail_T0MEMBER_DIED
			CASE mFail_T1MEMBER_DIED
			CASE mFail_T2MEMBER_DIED
			CASE mFail_T3MEMBER_DIED
			CASE mFail_PED_AGRO_T0
			CASE mFail_PED_AGRO_T1
			CASE mFail_PED_AGRO_T2
			CASE mFail_PED_AGRO_T3
			CASE mFail_GAINED_WANTED
				IF (MC_serverBD.iPartCausingFail[iFailTeam] != -1) 
				AND (MC_serverBD.iPartCausingFail[iFailTeam] != iPartToUse)
					sCelebrationStats.strFailLiteral = MC_serverBD_2.tParticipantNames[MC_serverBD.iPartCausingFail[iFailTeam]]
					
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL]8b not custom but agro / died member added: ",sCelebrationStats.strFailLiteral )
				ENDIF
			BREAK
		ENDSWITCH
		
	ENDIF
	
	//not using custom fail
	IF NOT bCustomFail
		CPRINTLN(DEBUG_MISSION,"[CV_FAIL]9 not custom entity checks" )
		
		SWITCH MC_serverBD.eCurrentTeamFail[iFailTeam]
			
			CASE mFail_T0MEMBER_DIED
			CASE mFail_T1MEMBER_DIED
			CASE mFail_T2MEMBER_DIED
			CASE mFail_T3MEMBER_DIED
			CASE mFail_PED_AGRO_T0
			CASE mFail_PED_AGRO_T1
			CASE mFail_PED_AGRO_T2
			CASE mFail_PED_AGRO_T3
			CASE mFail_HEIST_TEAM_GONE_T0
			CASE mFail_HEIST_TEAM_GONE_T1
			CASE mFail_HEIST_TEAM_GONE_T2
			CASE mFail_HEIST_TEAM_GONE_T3
				
				IF IS_STRING_NULL_OR_EMPTY(sCelebrationStats.strFailLiteral)
				OR ((MC_serverBD.eCurrentTeamFail[iFailTeam] >= mFail_HEIST_TEAM_GONE_T0)
					AND (MC_serverBD.eCurrentTeamFail[iFailTeam] <= mFail_HEIST_TEAM_GONE_T3)) // For some reason these fail reasons didn't have a check for the string before
					
					IF (MC_serverBD.iPartCausingFail[iFailTeam] != -1)
					AND NOT IS_STRING_NULL_OR_EMPTY(MC_serverBD_2.tParticipantNames[MC_serverBD.iPartCausingFail[iFailTeam]])
						IF (NOT (MC_serverBD.eCurrentTeamFail[iFailTeam] >= mFail_T0MEMBER_DIED AND MC_serverBD.eCurrentTeamFail[iFailTeam] <= mFail_T3MEMBER_DIED))
						OR MC_serverBD.iPartCausingFail[iFailTeam] != iLocalPart // If we're failing because the local player died we don't need a custom string, we'll just be using "You died"
							sCelebrationStats.strFailLiteral = MC_serverBD_2.tParticipantNames[MC_serverBD.iPartCausingFail[iFailTeam]] //person who spooked target
							CPRINTLN(DEBUG_MISSION,"[CV_FAIL]10a adding player name: ",sCelebrationStats.strFailLiteral)
						ENDIF
					ELSE //use team name instead
						
						SWITCH MC_serverBD.eCurrentTeamFail[iFailTeam]
							CASE mFail_T0MEMBER_DIED
							CASE mFail_PED_AGRO_T0
							CASE mFail_HEIST_TEAM_GONE_T0
								sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(0,false,false)
							BREAK
							CASE mFail_T1MEMBER_DIED
							CASE mFail_PED_AGRO_T1
							CASE mFail_HEIST_TEAM_GONE_T1
								sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(1,false,false)
							BREAK
							CASE mFail_T2MEMBER_DIED
							CASE mFail_PED_AGRO_T2
							CASE mFail_HEIST_TEAM_GONE_T2
								sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(2,false,false)
							BREAK
							CASE mFail_T3MEMBER_DIED
							CASE mFail_PED_AGRO_T3
							CASE mFail_HEIST_TEAM_GONE_T3
								sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(3,false,false)
							BREAK
						ENDSWITCH
						
						CPRINTLN(DEBUG_MISSION,"[CV_FAIL]10b set team name: ",sCelebrationStats.strFailLiteral)
					ENDIF
				ENDIF
				
			BREAK
			
			CASE mFail_GAINED_WANTED
				IF IS_STRING_NULL_OR_EMPTY(sCelebrationStats.strFailLiteral)
				AND (MC_serverBD.iPartCausingFail[iFailTeam] != -1)
					
					IF NOT IS_STRING_NULL_OR_EMPTY(MC_serverBD_2.tParticipantNames[MC_serverBD.iPartCausingFail[iFailTeam]])
						sCelebrationStats.strFailLiteral = MC_serverBD_2.tParticipantNames[MC_serverBD.iPartCausingFail[iFailTeam]] //person who spooked target
						CPRINTLN(DEBUG_MISSION,"[CV_FAIL]11a adding player name for wanted gain: ",sCelebrationStats.strFailLiteral)
					ELSE //use team name instead
						
						sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(MC_playerBD[MC_serverBD.iPartCausingFail[iFailTeam]].iteam,false,false)
						
						CPRINTLN(DEBUG_MISSION,"[CV_FAIL]11b set team name for wanted gain: ",sCelebrationStats.strFailLiteral)
					ENDIF
					
				ENDIF
			BREAK
			
			CASE mFail_HEIST_TEAM_GONE //if your team left fill this to current team
				// set name of who left or use team:
				IF ( (MC_serverBD.iPartCausingFail[iFailTeam] != -1)
				AND (MC_serverBD.iPartCausingFail[iFailTeam] != iPartToUse) )
					sCelebrationStats.strFailLiteral = MC_serverBD_2.tParticipantNames[MC_serverBD.iPartCausingFail[iFailTeam]]
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL]12a adding player who left: ",sCelebrationStats.strFailLiteral )
				ENDIF
			BREAK
			
			// set team name who failed
			CASE mFail_HEIST_TEAM_T0_FAILED
				sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(0,false,false)
			BREAK
			CASE mFail_HEIST_TEAM_T1_FAILED
				sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(1,false,false)
			BREAK
			CASE mFail_HEIST_TEAM_T2_FAILED
				sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(2,false,false)
			BREAK
			CASE mFail_HEIST_TEAM_T3_FAILED
				sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(3,false,false)
			BREAK
			
		ENDSWITCH
		
		if MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_HEIST_TEAM_T0_FAILED
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_HEIST_TEAM_GONE_T0
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_T0MEMBER_DIED
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_PED_AGRO_T0
			g_sJobHeistInfo.m_failureRole  = 0
		elif  MC_serverBD.eCurrentTeamFail[iFailTeam] 	= mFail_HEIST_TEAM_T1_FAILED
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_HEIST_TEAM_GONE_T1
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_T1MEMBER_DIED
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_PED_AGRO_T1
			g_sJobHeistInfo.m_failureRole  = 1
		elif  MC_serverBD.eCurrentTeamFail[iFailTeam] 	= mFail_HEIST_TEAM_T2_FAILED
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_HEIST_TEAM_GONE_T2
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_T2MEMBER_DIED
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_PED_AGRO_T2
			g_sJobHeistInfo.m_failureRole  = 2
		elif  MC_serverBD.eCurrentTeamFail[iFailTeam] 	= mFail_HEIST_TEAM_T3_FAILED
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_HEIST_TEAM_GONE_T3
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_T3MEMBER_DIED
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_PED_AGRO_T3
			g_sJobHeistInfo.m_failureRole  = 3
		ELSE
			// check if all other teams are blaiming failteam, if so then I am the fail role
			bool bFailknown = false
			if iFailTeam = 0
				if (MC_serverBD.eCurrentTeamFail[1] = mFail_HEIST_TEAM_T0_FAILED and MC_serverBD.eCurrentTeamFail[2] = mFail_HEIST_TEAM_T0_FAILED and MC_serverBD.eCurrentTeamFail[3] = mFail_HEIST_TEAM_T0_FAILED)
				or (MC_serverBD.eCurrentTeamFail[1] = mFail_HEIST_TEAM_GONE_T0 and MC_serverBD.eCurrentTeamFail[2] = mFail_HEIST_TEAM_GONE_T0 and MC_serverBD.eCurrentTeamFail[3] = mFail_HEIST_TEAM_GONE_T0)
				or (MC_serverBD.eCurrentTeamFail[1] = mFail_T0MEMBER_DIED and MC_serverBD.eCurrentTeamFail[2] = mFail_T0MEMBER_DIED and MC_serverBD.eCurrentTeamFail[3] = mFail_T0MEMBER_DIED)
				or (MC_serverBD.eCurrentTeamFail[1] = mFail_PED_AGRO_T0 and MC_serverBD.eCurrentTeamFail[2] = mFail_PED_AGRO_T0 and MC_serverBD.eCurrentTeamFail[3] = mFail_PED_AGRO_T0)
					bFailknown = true	
					g_sJobHeistInfo.m_failureRole  = 0
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL] POPULATE_FAIL_RESULTS - all blaming  fail team: ",g_sJobHeistInfo.m_failureRole)
				endif					
			elif iFailTeam = 1
				if (MC_serverBD.eCurrentTeamFail[0] = mFail_HEIST_TEAM_T1_FAILED and MC_serverBD.eCurrentTeamFail[2] = mFail_HEIST_TEAM_T1_FAILED and MC_serverBD.eCurrentTeamFail[3] = mFail_HEIST_TEAM_T1_FAILED)
				or (MC_serverBD.eCurrentTeamFail[0] = mFail_HEIST_TEAM_GONE_T1 and MC_serverBD.eCurrentTeamFail[2] = mFail_HEIST_TEAM_GONE_T1 and MC_serverBD.eCurrentTeamFail[3] = mFail_HEIST_TEAM_GONE_T1)
				or (MC_serverBD.eCurrentTeamFail[0] = mFail_T1MEMBER_DIED and MC_serverBD.eCurrentTeamFail[2] = mFail_T1MEMBER_DIED and MC_serverBD.eCurrentTeamFail[3] = mFail_T1MEMBER_DIED)
				or (MC_serverBD.eCurrentTeamFail[0] = mFail_PED_AGRO_T1 and MC_serverBD.eCurrentTeamFail[2] = mFail_PED_AGRO_T1 and MC_serverBD.eCurrentTeamFail[3] = mFail_PED_AGRO_T1)
					bFailknown = true
					g_sJobHeistInfo.m_failureRole  = 1
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL] POPULATE_FAIL_RESULTS - all blaming  fail team: ",g_sJobHeistInfo.m_failureRole)
				endif				
			elif iFailTeam = 2
				if (MC_serverBD.eCurrentTeamFail[1] = mFail_HEIST_TEAM_T2_FAILED and MC_serverBD.eCurrentTeamFail[0] = mFail_HEIST_TEAM_T2_FAILED and MC_serverBD.eCurrentTeamFail[3] = mFail_HEIST_TEAM_T2_FAILED)
				or (MC_serverBD.eCurrentTeamFail[1] = mFail_HEIST_TEAM_GONE_T2 and MC_serverBD.eCurrentTeamFail[0] = mFail_HEIST_TEAM_GONE_T2 and MC_serverBD.eCurrentTeamFail[3] = mFail_HEIST_TEAM_GONE_T2)
				or (MC_serverBD.eCurrentTeamFail[1] = mFail_T2MEMBER_DIED and MC_serverBD.eCurrentTeamFail[0] = mFail_T2MEMBER_DIED and MC_serverBD.eCurrentTeamFail[3] = mFail_T2MEMBER_DIED)
				or (MC_serverBD.eCurrentTeamFail[1] = mFail_PED_AGRO_T2 and MC_serverBD.eCurrentTeamFail[0] = mFail_PED_AGRO_T2 and MC_serverBD.eCurrentTeamFail[3] = mFail_PED_AGRO_T2)
					bFailknown = true
					g_sJobHeistInfo.m_failureRole  = 2
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL] POPULATE_FAIL_RESULTS - all blaming  fail team: ",g_sJobHeistInfo.m_failureRole)
				endif				
			elif iFailTeam = 3
				if (MC_serverBD.eCurrentTeamFail[1] = mFail_HEIST_TEAM_T3_FAILED and MC_serverBD.eCurrentTeamFail[2] = mFail_HEIST_TEAM_T3_FAILED and MC_serverBD.eCurrentTeamFail[0] = mFail_HEIST_TEAM_T3_FAILED)
				or (MC_serverBD.eCurrentTeamFail[1] = mFail_HEIST_TEAM_GONE_T3 and MC_serverBD.eCurrentTeamFail[2] = mFail_HEIST_TEAM_GONE_T3 and MC_serverBD.eCurrentTeamFail[0] = mFail_HEIST_TEAM_GONE_T3)
				or (MC_serverBD.eCurrentTeamFail[1] = mFail_T3MEMBER_DIED and MC_serverBD.eCurrentTeamFail[2] = mFail_T3MEMBER_DIED and MC_serverBD.eCurrentTeamFail[0] = mFail_T3MEMBER_DIED)
				or (MC_serverBD.eCurrentTeamFail[1] = mFail_PED_AGRO_T3 and MC_serverBD.eCurrentTeamFail[2] = mFail_PED_AGRO_T3 and MC_serverBD.eCurrentTeamFail[0] = mFail_PED_AGRO_T3)
					bFailknown = true
					g_sJobHeistInfo.m_failureRole  = 3
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL] POPULATE_FAIL_RESULTS - all blaming  fail team: ",g_sJobHeistInfo.m_failureRole)
				endif				
			endif				
			if !bFailKnown
				g_sJobHeistInfo.m_failureRole  = 999 //Using 999 instead of iFailTeam as requested from Chiu and Miguel
			endif	
		endif
		PRINTLN("[RCC MISSION] POPULATE_FAIL_RESULTS - g_sJobHeistInfo.m_failureRole - ",g_sJobHeistInfo.m_failureRole )
	ENDIF
	
	SET_BIT(iLocalBoolCheck15, LBOOL15_CALLED_POPULATE_FAIL_RESULTS)
	
	PROCESS_FAIL_REASON_TELEMETRY(MC_serverBD.eCurrentTeamFail[iFailTeam])
	
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL] POPULATE_FAIL_RESULTS - Getting end reasons S5: end reason text: ",sObjEndText)
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL] POPULATE_FAIL_RESULTS - Getting end reasons S5: end reason literal text: ",  sCelebrationStats.strFailLiteral )
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL] POPULATE_FAIL_RESULTS - Getting end reasons S5: end reason literal text 2: ",sCelebrationStats.strFailLiteral2 )
	
ENDPROC

PROC POPULATE_RESULTS()

	BOOL bPass
	INT iLocalPlayerJP = -1
	INT iWinningPlayer = -1

	IF NOT IS_BIT_SET(iLocalBoolCheck, ciPOPULATE_RESULTS)
		
		#IF IS_DEBUG_BUILD
		PRINTLN("[RCC MISSION] - populating results:")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		
		START_NET_TIMER(tdresultstimer)
		
		SET_BIT(iLocalBoolCheck,ciPOPULATE_RESULTS)
		
		SET_MP_DECORATOR_BIT(LocalPlayer, MP_DECORATOR_BS_INVALID_TO_SPECTATE)
		CLEAR_ALL_BIG_MESSAGES()
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_AlwaysAllPlayersSkipNJVS)
			PRINTLN("[RCC MISSION] POPULATE_RESULTS - ciOptionsBS27_AlwaysAllPlayersSkipNJVS is set. Calling: SET_FM_FLOW_MISSION_SKIP_NJVS")
			SET_FM_FLOW_MISSION_SKIP_NJVS()
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_MissionPassAllPlayersSkipNJVS)
		AND HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iteam)
			PRINTLN("[RCC MISSION] POPULATE_RESULTS - Team Passed and ciOptionsBS27_MissionPassAllPlayersSkipNJVS is set. Calling: SET_FM_FLOW_MISSION_SKIP_NJVS")
			SET_FM_FLOW_MISSION_SKIP_NJVS()
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_START_SPECTATOR)
			IF NOT HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iteam)
				POPULATE_FAIL_RESULTS()
				
				IF IS_LOADED_MISSION_TYPE_FLOW_MISSION()
					SET_FM_FLOW_MISSION_SKIP_NJVS()
				ENDIF
			ENDIF
			
			PRINTLN("[RCC MISSION] ___RESULTS___ :QUIT WAS ONLY SPECTATOR")
			
			IF g_bVSMission
				IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
					SET_SHOW_QUICK_RESTART_OPTION()
					g_bAllowJobReplay = TRUE
				ENDIF
			ENDIF
			
			EXIT
		ENDIF
		
		#IF IS_DEBUG_BUILD
			INT iteam
			PRINTLN("[RCC MISSION] *************************************")
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] ________________RESULTS______________")
			PRINTLN("[RCC MISSION] MISSION NAME = ",g_FMMC_STRUCT.tl63DebugMissionName)
			PRINTLN("[RCC MISSION] MY TEAM = ", MC_playerBD[iPartToUse].iteam)
			PRINTLN("[RCC MISSION] GET_TEAM_FINISH_POSITION(MC_playerBD[iPartToUse].iteam) = ", GET_TEAM_FINISH_POSITION(MC_playerBD[iPartToUse].iteam))
			PRINTLN("[RCC MISSION] MC_serverBD.iTeamScore[MC_playerBD[iPartToUse].iteam] = ", MC_serverBD.iTeamScore[MC_playerBD[iPartToUse].iteam])
			PRINTLN("[RCC MISSION] MC_serverBD.iWinningTeam = ", MC_serverBD.iWinningTeam)
			PRINTLN("[RCC MISSION] MC_serverBD.iSecondTeam = ", MC_serverBD.iSecondTeam)
			PRINTLN("[RCC MISSION] MC_serverBD.iThirdTeam = ", MC_serverBD.iThirdTeam)
			PRINTLN("[RCC MISSION] MC_serverBD.iLosingTeam = ", MC_serverBD.iLosingTeam)
			IF MC_serverBD.iWinningTeam > -1
			AND MC_serverBD.iWinningTeam < FMMC_MAX_TEAMS
				PRINTLN("[RCC MISSION] MC_serverBD.iWinningTeam score =  ", MC_serverBD.iTeamScore[MC_serverBD.iWinningTeam])
			ENDIF
			PRINTLN("[RCC MISSION] DOES_TEAM_LIKE_TEAM winners = ", DOES_TEAM_LIKE_TEAM(MC_playerBD[iPartToUse].iteam,MC_serverBD.iWinningTeam))
			PRINTLN("[RCC MISSION] ALL_OTHER_HOSTILE_TEAMS_FAILED = ", ALL_OTHER_HOSTILE_TEAMS_FAILED(MC_playerBD[iPartToUse].iteam))
			
			FOR iteam = 0 TO (FMMC_MAX_TEAMS-1)
				PRINTLN("[RCC MISSION]  ***  TEAM *** ", iteam)
				PRINTLN("[RCC MISSION] GET_TEAM_FINISH_POSITION(iteam) = ", GET_TEAM_FINISH_POSITION(iteam))
				PRINTLN("[RCC MISSION] MC_serverBD.iTeamScore[iteam] = ", MC_serverBD.iTeamScore[iteam])
				
				IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iMissionTargetScore = -1
					IF MC_serverBD.iNumberOfPlayingPlayers[iteam] > 0
						PRINTLN("[RCC MISSION] pass score is number of players = ", MC_serverBD.iNumberOfPlayingPlayers[iteam])
					ELSE
						PRINTLN("[RCC MISSION] pass score is number of players but that is 0 so setting to: ", MC_serverBD.iNumberOfPlayingPlayers[iteam])
					ENDIF
				ELIF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iMissionTargetScore != 0
					PRINTLN("[RCC MISSION] pass score is mission target score = ", g_FMMC_STRUCT.sFMMCEndConditions[iteam].iMissionTargetScore)
				ELSE
					PRINTLN("[RCC MISSION] pass score is mission default score = ", 1)
				ENDIF
			ENDFOR
			
			PRINTLN("[JS] MEDAL - PLAYER SCORE: ", MC_Playerbd[iPartToUse].iPlayerScore)

		#ENDIF

		// clear blips etc
		CLEANUP_ALL_BLIPS()
		CLEAR_PRINTS()
		Clear_Any_Objective_Text()
		CLEAR_HELP()
							
		IF IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciUSE_COUNTDOWN )
			IF MC_serverBD.iReasonForObjEnd[MC_playerBD[iLocalPart].iteam] = OBJ_END_REASON_TARGET_SCORE
			OR IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciUSE_COUNTDOWN )
				TRIGGER_MUSIC_EVENT("MP_DM_COUNTDOWN_KILL")
				PRINTLN("[RCC MISSION] TRIGGER_MUSIC_EVENT(\"MP_DM_COUNTDOWN_KILL\")")
			ENDIF
		ENDIF
		
		IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] TEAM HAS PASSED = ", MC_playerBD[iLocalPart].iteam)
			bPass = TRUE
			SET_BIT(iLocalBoolCheck2,LBOOL2_PASSED_MISSION)
			
			IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
				IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
					MC_SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
					PRINTLN("[RCC MISSION] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - F")
				ENDIF
			ENDIF
			//Call the flow strand mission data
			IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
				SET_FLOW_STRAND_PROGRESS_ON_MISSION_COMPLEATION(bIsAnySpectator, CHECK_TO_SEE_IF_THIS_IS_THE_LAST_STRAND_MISSION())
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
				SCRIPT_TRIGGER_MUSIC_EVENT_FAIL()
				PRINTLN("TRIGGER_MUSIC_EVENT(\"MP_MC_FAIL\") Pr1")
			ENDIF
		ENDIF	

		INT iRootContentID = GET_STRAND_ROOT_CONTENT_ID_HASH()
	
		IF iRootContentID = 0
			PRINTLN("[AMEC][HEIST_MISC] - POPULATE_RESULTS - RootContentID is 0, using g_FMMC_STRUCT data: ", g_FMMC_STRUCT.iRootContentIdHash)
			iRootContentID = g_FMMC_STRUCT.iRootContentIdHash
		ELSE
			PRINTLN("[AMEC][HEIST_MISC] - POPULATE_RESULTS - GET_STRAND_ROOT_CONTENT_ID_HASH() = ", iRootContentID)
		ENDIF
		
		IF bPass
		OR g_bVSMission
			
			INT iLBDIndexWinner = GET_LEADERBOARD_INDEX_OF_TOP_PLAYER_ON_TEAM(MC_playerBD[iLocalPart].iteam)
			INT iteamwinner 	= NATIVE_TO_INT(g_MissionControllerserverBD_LB.sleaderboard[iLBDIndexWinner].playerID)
			PRINTLN("[RCC MISSION] [JOB POINT] -iteamwinner lb index=  ",iteamwinner, " iLBDIndexWinner = ", iLBDIndexWinner)
			PRINTLN("[RCC MISSION] [JOB POINT] local player: ",NATIVE_TO_INT(LocalPlayer))
		
			// Add MVP Job Point
			IF MC_serverBD.iNumActiveTeams > 1	
			AND NOT g_bOnCoopMission
				iLocalPlayerJP = NETWORK_PLAYER_ID_TO_INT()
				IF iLocalPlayerJP <> -1
					GlobalplayerBD_FM[iLocalPlayerJP].sPlaylistVars.iMVPThisJob = iteamwinner
					PRINTLN("[RCC MISSION] [JOB POINT] iLocalPlayerJP = ", iLocalPlayerJP, " iteamwinner = ", iteamwinner)
				ENDIF
			ENDIF

			IF DOES_TEAM_LIKE_TEAM(MC_playerBD[iPartToUse].iteam,MC_serverBD.iWinningTeam)
				sCelebrationStats.iLocalPlayerJobPoints = GET_POINTS_FOR_POSITION(GET_TEAM_FINISH_POSITION(MC_serverBD.iWinningTeam), iLocalPlayerJP)
			ELSE
				sCelebrationStats.iLocalPlayerJobPoints = GET_POINTS_FOR_POSITION(GET_TEAM_FINISH_POSITION(MC_playerBD[iLocalPart].iteam), iLocalPlayerJP)
			ENDIF
			
			IF NOT IS_THIS_A_ROUNDS_MISSION() // SEE (ADD_MISSION_JOB_POINTS_ROUNDS)
			OR SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
				ADD_TO_JOB_POINTS_TOTAL(sCelebrationStats.iLocalPlayerJobPoints)
			ELSE
				PRINTLN("[RCC MISSION] [JOB POINT] LBOOL6_HIDE_JOB_POINTS_CELEB_SCREEN: ")
				SET_BIT(iLocalBoolCheck6, LBOOL6_HIDE_JOB_POINTS_CELEB_SCREEN)
			ENDIF
			
		ENDIF
		
		IF !bPass
			POPULATE_FAIL_RESULTS()
			
			IF IS_LOADED_MISSION_TYPE_FLOW_MISSION()
				SET_FM_FLOW_MISSION_SKIP_NJVS()
			ENDIF
		endif
				
		IF g_bVSMission OR NOT bPass
			IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
				SET_SHOW_QUICK_RESTART_OPTION()
				g_bAllowJobReplay = TRUE
				IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_MISSION_ENDING_CRITICAL_PLAYER_LEFT)
				OR (IS_THIS_A_GANG_MISSION() AND IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_MISSION_ENDING_BECAUSE_PLAYERS_LEFT)) //Gang missions can't be restarted with less than original player count.
					PRINTLN("POPULATE_RESULTS - Blocking Quick Restarts - g_bFMMCCriticalFail")
					g_bFMMCCriticalFail = TRUE
				ENDIF
			ENDIF
		ENDIF
 
		INT iLBPosition		= GET_PLAYERS_LEADERBOARD_POSITION(LocalPlayer)
		INT iTeamLBPosition = GET_PLAYERS_TEAM_LEADERBOARD_POSITION(LocalPlayer, MC_playerBD[iLocalPart].iteam)
		INT iTeamPosition 	= GET_TEAM_FINISH_POSITION(MC_playerBD[iPartToUse].iteam) //(iLBPosition+1)
		
		iLBPositionStore = iLBPosition
		iTeamLBPositionStore = iTeamLBPosition
		
		INT iTeamRankForRewards = iTeamPosition
		
		INT iTeamLoop
		FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 1
			IF DOES_TEAM_LIKE_TEAM( MC_playerBD[iPartToUse].iteam, iTeamLoop ) // Check to see if this is a friendly team
			AND GET_TEAM_FINISH_POSITION( iTeamLoop ) < iTeamRankForRewards // and if they have done better than we have, we use their scores
				iTeamRankForRewards = GET_TEAM_FINISH_POSITION( iTeamLoop )
			ENDIF
		ENDFOR
				
		INT iTotalMissionEndTime = GET_MISSION_TIME_FOR_REWARDS()
		
		g_iRoundsCombinedTime += iTotalMissionEndTime
		PRINTLN("[MMacK][RoundsTimeBonus] g_iRoundsCombinedTime = ", g_iRoundsCombinedTime)
				
		INT iTeamSorting, iTeamToCheckAgainst
		INT iAdjustedPosition[FMMC_MAX_TEAMS]
		BOOL bHasFriendlyTeams
		
		FOR iTeamSorting = 0 TO FMMC_MAX_TEAMS - 1
			iAdjustedPosition[iTeamSorting] = GET_TEAM_FINISH_POSITION(iTeamSorting)
			
			FOR iTeamToCheckAgainst = 0 TO FMMC_MAX_TEAMS - 1
				IF DOES_TEAM_LIKE_TEAM(iTeamSorting, iTeamToCheckAgainst)
					IF iTeamSorting != iTeamToCheckAgainst
						bHasFriendlyTeams = TRUE
					ENDIF
					IF GET_TEAM_FINISH_POSITION(iTeamSorting) > GET_TEAM_FINISH_POSITION(iTeamToCheckAgainst)	
						iAdjustedPosition[iTeamSorting] = GET_TEAM_FINISH_POSITION(iTeamToCheckAgainst)
					ENDIF
				ENDIF
			ENDFOR
		ENDFOR
		
		IF bHasFriendlyTeams
			#IF IS_DEBUG_BUILD
				FOR iTeamSorting = 0 TO FMMC_MAX_TEAMS - 1
					PRINTLN("[MMacK][RoundsSorting] iAdjustedPosition[",iTeamSorting,"] = ", iAdjustedPosition[iTeamSorting])
				ENDFOR
			#ENDIF
		
			FOR iTeamSorting = 0 TO FMMC_MAX_TEAMS - 1
				IF iAdjustedPosition[iTeamSorting] < GET_TEAM_FINISH_POSITION(MC_playerBD[iPartToUse].iteam)
				AND MC_playerBD[iPartToUse].iteam != iTeamSorting
				AND NOT DOES_TEAM_LIKE_TEAM(iTeamSorting, MC_playerBD[iPartToUse].iteam)
					iTeamRankForRewards = iAdjustedPosition[iTeamSorting] + 1
					PRINTLN("[MMacK][RoundsSorting] iTeamRankForRewards = ", iTeamRankForRewards)
				ENDIF
			ENDFOR
		ENDIF
		
		PROCESS_END_OF_MISSION_REWARDS(bPass, iTeamRankForRewards, iTotalMissionEndTime)
		
		iWinningPlayer = GET_LEADERBOARD_INDEX_OF_TOP_WINNER()
		
		IF NOT IS_THIS_A_ROUNDS_MISSION()
		OR (IS_PLAYER_ON_A_PLAYLIST(LocalPlayer) AND SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()) // 2040227
			IF iWinningPlayer > -1
			AND (g_MissionControllerserverBD_LB.sleaderboard[iWinningPlayer].playerID != INVALID_PLAYER_INDEX())
				MC_playerBD[iLocalPart].iBet = BROADCAST_BETTING_MISSION_FINISHED(g_MissionControllerserverBD_LB.sleaderboard[iWinningPlayer].playerID)
			ENDIF
		ENDIF

		sCelebrationStats.winningPlayer = g_MissionControllerserverBD_LB.sleaderboard[iWinningPlayer].playerID
		sCelebrationStats.iLocalPlayerCash = iCashGainedForCelebration
		sCelebrationStats.iLocalPlayerXP = iRPGainedForCelebration
		sCelebrationStats.iLocalPlayerXP += iXPExtraRewardGained
		sCelebrationStats.iLocalPlayerXP = GET_MAX_XP_GAIN_FOR_CELEBRATION_SCREEN(sCelebrationStats.iLocalPlayerXP)
		IF iLBPosition > -1
			sCelebrationStats.iLocalPlayerScore = g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iPlayerScore
			sCelebrationStats.iLocalPlayerPosition = g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iRank
		ENDIF
		
		sCelebrationStats.iLocalPlayerBetWinnings = MC_playerBD[iLocalPart].iBet
		sCelebrationStats.bPassedMission = bPass
		sCelebrationStats.bIsDraw = FALSE // you cannot draw
		
		IF IS_PLAYER_ON_A_PLAYLIST(LocalPlayer)
		AND NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_HANDLE_PLAYLIST_POSITION_DATA_WRITE_CALLED)
			HANDLE_PLAYLIST_POSITION_DATA_WRITE()
			SET_BIT(iLocalBoolCheck6, LBOOL6_SAVED_ROUNDS_LBD_DATAa)
		ENDIF
		
		PROCESS_END_OF_MISSION_TELEMETRY(bPass, iTotalMissionEndTime)
		
		//This stuff was previously in HANDLE_GIVING_END_REWARDS - copied as it was to avoid bugs
		INT isuicides = (GlobalplayerBD_FM[NATIVE_TO_INT(PlayerToUse)].iSuicides - iStartSuicides)
		IF bPass
			IF IS_THIS_A_CONTACT_MISSION()
			#IF FEATURE_FIXER
			AND NOT IS_THIS_A_FIXER_SHORT_TRIP()
			#ENDIF
				SET_PLAYER_RECENTLY_PASSED_MISSION_FOR_CONTACT(Get_Current_Mission_Contact())
			ENDIF
			IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
				iMissionResult = ciFMMC_END_OF_MISSION_STATUS_PASSED
				IF iLBPosition > -1
					IF IS_THIS_A_ROUNDS_MISSION()
						iRoundsLBPosition = iLBPosition
						iRoundsSuicides = isuicides
						PRINTLN("[TEL] * iRoundsLBPosition = ", iLBPosition)
					ELSE
						DEAL_WITH_FM_MATCH_END(FMMC_TYPE_MISSION,g_MissionControllerserverBD_LB.iMatchTimeID,0,iMissionResult,g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iRank,g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iKills,g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iDeaths,isuicides ,iHighKillStreak,g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iTeam,g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iHeadshots, g_mnMyRaceModel, DEFAULT, DEFAULT, MC_serverBD.iDifficulty, 	GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( MC_serverBD.tdMissionLengthTimer ))
					ENDIF
				ENDIF
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MCMWINS, 1)
				INCREMENT_MP_INT_PLAYER_STAT(MPPLY_MCMWIN)				
			ENDIF
			IF DOES_ENTITY_EXIST(LocalPlayerPed)
				IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
					g_bPlayEndOfJobRadioFrontEnd = TRUE
					PRINTLN("[RCC MISSION] Player in a vehicle - called SET_AUDIO_FLAG(TRUE) and SET_MOBILE_PHONE_RADIO_STATE(TRUE).")
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
				iMissionResult = ciFMMC_END_OF_MISSION_STATUS_FAILED
				INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_MC_FAILED)
				IF iLBPosition > -1
					IF IS_THIS_A_ROUNDS_MISSION()
						iRoundsLBPosition = iLBPosition
						iRoundsSuicides = isuicides
						PRINTLN("[TEL] * iRoundsLBPosition = ", iLBPosition)
					ELSE
						DEAL_WITH_FM_MATCH_END(FMMC_TYPE_MISSION,g_MissionControllerserverBD_LB.iMatchTimeID,0,iMissionResult,g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iRank,g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iKills,g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iDeaths,isuicides ,iHighKillStreak,g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iTeam,g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iHeadshots, g_mnMyRaceModel, DEFAULT, DEFAULT, MC_serverBD.iDifficulty, 	GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( MC_serverBD.tdMissionLengthTimer ))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[JS] POPULATE_RESULTS already done")	
	#ENDIF		
	ENDIF
	IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_SAVED_ROUNDS_LBD_DATAb)
		// Write rewards, kills etc. are written in (PROCESS_MISSION_END_STAGE)
		PRINTLN("[TS] [MSROUND] - WRITE_TO_ROUNDS_MISSION_END_LB_DATA 1b) ")
		FLOAT fCashMulti = TO_FLOAT(g_iCachedMatchBonus) * g_sMPTunables.cashMultiplier
		INT iCachedValRounded = ROUND(fCashMulti)
		PRINTLN("[RCC_MISSION] [CASH] - POPULATE_RESULTS iCachedValRounded ", iCachedValRounded)
		WRITE_TO_ROUNDS_MISSION_END_LB_DATA(0,
											0,
											0,	
											MC_playerBD[iLocalPart].iRewardXP,
											MC_playerBD[iLocalPart].iCashReward+iCachedValRounded+MC_playerBD[iLocalPart].iBet, 
											DEFAULT,
											DEFAULT,
											0)
		SET_BIT(iLocalBoolCheck6, LBOOL6_SAVED_ROUNDS_LBD_DATAb)
	ENDIF
	
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Celebration Screen ----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

///PURPOSE: This function checks the rules ahead for other teams to see if, once we've watched this cutscene,
///    we are going to finish the mission anyway. It's used for split missions like Prison: Station and
///    Prison: Wet Work where two teams have different objectives that can be completed in any order
///    
///    RETURNS: an sEarlyCelebrationData struct which holds the team 
///    and the points they would be awarded if they passed all their rules
FUNC sEarlyCelebrationData SHOULD_CELEBRATION_SCREEN_BE_SHOWN_PREMATURELY()
	sEarlyCelebrationData sReturn
	
	sReturn.bShouldShowPrematurely = FALSE
	
	
	INT iMyTeam = MC_playerBD[ iPartToUse ].iteam
	INT iMyCurrentRule = MC_ServerBD_4.iCurrentHighestPriority[ iMyTeam ] + 1 // Check the rule after the cutscene
	
	// Don't care about any of this if we're already failed.
	IF NOT HAS_TEAM_PASSED_MISSION( iMyTeam )
		PRINTLN("[RCC MISSION] We've already failed by the time SHOULD_CELEBRATION_SCREEN_BE_SHOWN_PREMATURELY is called - returning false" )
		RETURN sReturn
	ENDIF

	IF bIsAnySpectator
		INT iSpectatorTargetPart = GET_SPECTATOR_TARGET_PARTICIPANT_OF_PARTICIPANT( iLocalPart )
		IF iSpectatorTargetPart > -1
			iMyTeam = MC_playerBD[ iSpectatorTargetPart ].iTeam
		ENDIF
	ENDIF
	
	sReturn.iTeam = iMyTeam

	IF iMyCurrentRule > MC_serverBD.iMaxObjectives[ iMyTeam ]
		iMyCurrentRule = MC_serverBD.iMaxObjectives[ iMyTeam ]
	ENDIF
	
	INT iPointsForRemainingRules = 0
	
	INT iLoopRules = 0
	INT iLoopTeams = 0
	
	INT iOtherTeamRule = 0
	
	// Loop through all our rules from now until the mission end
	FOR iLoopRules = iMyCurrentRule TO MC_serverBD.iMaxObjectives[ iMyTeam ]
		PRINTLN("[RCC MISSION] Checking rule ", iLoopRules, " for this team for other team progression rules" )

		FOR iLoopTeams = 0 TO FMMC_MAX_TEAMS - 1 // Check all other teams
			iPointsForRemainingRules = 0
			IF IS_TEAM_ACTIVE( iLoopTeams ) AND iLoopTeams != iMyTeam // That are in the mission and aren't us
				IF g_FMMC_STRUCT.sFMMCEndConditions[ iMyTeam ].iObjectiveProgressionLogic[ iLoopRules ][ iLoopTeams ] != -1 // Our team can be progressed by another team
					// Check to see if they've got a progression rule for our current rule - ie could they progress us forwards now
					PRINTLN("[RCC MISSION] Team " ,iLoopTeams,"  has a progression rule " )
				
					FOR iOtherTeamRule = MC_serverBD_4.iCurrentHighestPriority[ iLoopTeams ] TO MC_serverBD.iMaxObjectives[ iLoopTeams ] // And loop all their remaining objectives
						PRINTLN("[RCC MISSION] Checking progression rules for team ", iLoopTeams, " for their rule ",  iOtherTeamRule )
					
						IF iOtherTeamRule >= g_FMMC_STRUCT.sFMMCEndConditions[ iMyTeam ].iObjectiveProgressionLogic[ iLoopRules ][ iLoopTeams ]
							IF iOtherTeamRule < FMMC_MAX_RULES
								IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iMyTeam ].iObjectiveProgMidPointBitset[ iLoopRules ], ciObjProgBS_MoveOnTeamMidpoint_T0 + iLoopTeams )
									
									IF IS_BIT_SET( MC_serverBD.iObjectiveMidPointBitset[ iLoopTeams ], iOtherTeamRule )
									//Or the team has moved past it, so we don't care about the midpoint:
									OR iOtherTeamRule > g_FMMC_STRUCT.sFMMCEndConditions[ iMyTeam ].iObjectiveProgressionLogic[ iLoopRules ][ iLoopTeams ]
										PRINTLN("[RCC MISSION] Midpoint is set" )
										
										iPointsForRemainingRules += GET_FMMC_POINTS_FOR_TEAM( iMyTeam, iLoopRules )
										
										IF iLoopRules = MC_serverBD.iMaxObjectives[ iMyTeam ]
											sReturn.bShouldShowPrematurely = TRUE
											PRINTLN("[RCC MISSION] returning true 1" )
											sReturn.iTeamWinningPoints = iPointsForRemainingRules
											RETURN sReturn
										ENDIF
									ELSE // Or we haven't met that midpoint yet
										PRINTLN("[RCC MISSION] False 1" )
										iOtherTeamRule = MC_serverBD.iMaxObjectives[ iLoopTeams ] // Skip to checking the next team
										sReturn.bShouldShowPrematurely = FALSE
									ENDIF
									
								ELSE // Else it's not a midpoint progression
									PRINTLN("[RCC MISSION] Objective progression logic is true" )
									
									iPointsForRemainingRules += GET_FMMC_POINTS_FOR_TEAM( iMyTeam, iLoopRules )
									IF iLoopRules = MC_serverBD.iMaxObjectives[ iMyTeam ]
										sReturn.bShouldShowPrematurely = TRUE
										PRINTLN("[RCC MISSION] returning true 2" )
										sReturn.iTeamWinningPoints = iPointsForRemainingRules
										RETURN sReturn
									ENDIF
								ENDIF
							ELSE // Else the other team is off the end of their rules
								PRINTLN("[RCC MISSION] Objective progress bitset is set")
								
								iPointsForRemainingRules += GET_FMMC_POINTS_FOR_TEAM( iMyTeam, iLoopRules )
								IF iLoopRules = MC_serverBD.iMaxObjectives[ iMyTeam ]
									sReturn.bShouldShowPrematurely = TRUE
									PRINTLN("[RCC MISSION] returning true 3" )
									sReturn.iTeamWinningPoints = iPointsForRemainingRules
									RETURN sReturn
								ENDIF
							ENDIF
						ELSE // Otherwise their other rule is beyond the current progression rule
							PRINTLN("[RCC MISSION] False 2" )
							sReturn.bShouldShowPrematurely = FALSE
							iOtherTeamRule = MC_serverBD.iMaxObjectives[ iLoopTeams ] // Skip to checking the next team
						ENDIF

					ENDFOR // The other team rule loop
				ELSE // This team does not progress our team this rule
					PRINTLN("[RCC MISSION] Team ", iLoopTeams, " doesn't progress our current rule." )
					
					sReturn.bShouldShowPrematurely = FALSE
					
					// If we've checked all teams for our current rule, and each one doesn't have a progression rule
					// then there's no point in checking our later rules - we already know we're not gonna get progressed
					// so we leave the outer loop here and return false
					// If there WAS a progression rule before now, we would have already returned TRUE
					IF iLoopTeams >= FMMC_MAX_TEAMS - 1
						iLoopRules = MC_serverBD.iMaxObjectives[ iMyTeam ]
						PRINTLN("[RCC MISSION] checked the last team - exiting." )
					ENDIF
					
					// If this team doesn't have a progression rule now, don't bother checking their later rules, check the next team
					iLoopTeams++
				ENDIF
			ENDIF
		ENDFOR // Team loop
	ENDFOR // Our rule loop
	
	// If we're on our last rule, always return true
	IF MC_ServerBD_4.iCurrentHighestPriority[ iMyTeam ] = MC_serverBD.iMaxObjectives[ iMyTeam ]
		sReturn.bShouldShowPrematurely = TRUE
		PRINTLN("[RCC MISSION] we're on our last rule and haven't failed - an early celebration for us!" )
	ENDIF

	PRINTLN("[RCC MISSION] Should the celebration screen be shown prematurely? ", sReturn.bShouldShowPrematurely )
	
	RETURN sReturn
ENDFUNC

FUNC BOOL CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST(INT iParticipant, INT iLbdCount, PLAYER_INDEX &playerId)
	
	BOOL bDoStandardChecks = TRUE
	playerId = INVALID_PLAYER_INDEX()
	
	IF IS_THIS_A_ROUNDS_MISSION()
		IF SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
			PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - IS_THIS_A_ROUNDS_MISSION = TRUE, SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD = TRUE")
			IF g_sTransitionSessionData.sMissionRoundData.piWinner != INVALID_PLAYER_INDEX()
			AND NETWORK_IS_PLAYER_ACTIVE(g_sTransitionSessionData.sMissionRoundData.piWinner)
				INT iWinnerTeam = GET_PLAYER_TEAM(g_sTransitionSessionData.sMissionRoundData.piWinner)
				PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - g_sTransitionSessionData.sMissionRoundData.piWinner != IVALID_PLAYER_INDEX()")
				PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - g_sTransitionSessionData.sMissionRoundData.piWinner = ", NATIVE_TO_INT(g_sTransitionSessionData.sMissionRoundData.piWinner))
				PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - g_sTransitionSessionData.sMissionRoundData.piWinner team = ", iWinnerTeam)
				bDoStandardChecks = FALSE
				IF MC_Playerbd[iParticipant].iteam = iWinnerTeam
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam = ", MC_Playerbd[iParticipant].iteam)
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - teams are the same - returning TRUE.")
					playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
					RETURN TRUE
				ENDIF
				IF DOES_TEAM_LIKE_TEAM(MC_Playerbd[iParticipant].iteam, iWinnerTeam)
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam = ", MC_Playerbd[iParticipant].iteam)
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - DOES_TEAM_LIKE_TEAM = TRUE - returning TRUE.")
					playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
					RETURN TRUE
				ENDIF
				PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam is not same team as winner and does not like team of winner, not adding.")
			ELSE
				IF NOT SHOULD_END_ROUNDS_MISSION_DUE_TO_TEAM_LEAVING()
					SCRIPT_ASSERT("At end of rounds mission not ended for team leaving and sMissionRoundData.piWinner = invalid player")
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD = FALSE")
		ENDIF
	ELSE
		PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - IS_THIS_A_ROUNDS_MISSION = FALSE")
	ENDIF
	
	IF bDoStandardChecks
		PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - bDoStandardChecks is still true, doing standard checks...")
		IF MC_Playerbd[iParticipant].iteam > -1
			PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam = ", MC_Playerbd[iParticipant].iteam)
			IF MC_Playerbd[iParticipant].iteam = MC_serverBD.iWinningTeam
				PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam = ", MC_Playerbd[iParticipant].iteam)
				PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - teams are the same")
				IF HAS_TEAM_PASSED_MISSION(MC_Playerbd[iParticipant].iteam) //1643001 - Don't include top scorers if they failed the mission.
				OR MC_serverBD.eCurrentTeamFail[MC_PlayerBD[iParticipant].iTeam] = mFail_ALL_TEAMS_FAIL // used only in special cases when no winner is declared url:bugstar:3588936
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam - HAS_TEAM_PASSED_MISSION is TRUE, returning TRUE")
					playerId = GET_PLAYER_IN_LEADERBOARD_POS(iLbdCount)
					RETURN TRUE
				ELSE
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam has not passed mission")
				ENDIF
			ELSE
				PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam is not same team as winner")
			ENDIF
			IF DOES_TEAM_LIKE_TEAM(MC_Playerbd[iParticipant].iteam, MC_serverBD.iWinningTeam)
				PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam = ", MC_Playerbd[iParticipant].iteam)
				PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - DOES_TEAM_LIKE_TEAM = TRUE")
				IF HAS_TEAM_PASSED_MISSION(MC_Playerbd[iParticipant].iteam) //1643001 - Don't include top scorers if they failed the mission.
				OR MC_serverBD.eCurrentTeamFail[MC_PlayerBD[iParticipant].iTeam] = mFail_ALL_TEAMS_FAIL // used only in special cases when no winner is declared url:bugstar:3588936
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam - HAS_TEAM_PASSED_MISSION is TRUE, returning TRUE")
					playerId = GET_PLAYER_IN_LEADERBOARD_POS(iLbdCount)
					RETURN TRUE
				ELSE
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam has not passed mission")
				ENDIF
			ELSE
				PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam does not like winning team")
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - participant ", iParticipant, " has not passed checks, not adding to list of winners")
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Creates a list of players that were on the winning team, in order of their position (if it's not a team event then just one player is stored).
FUNC BOOL GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN(PLAYER_INDEX &winningPlayers[MAX_NUM_CELEBRATION_PEDS], INT &iNumberPlayersFound)
	
	INT iNumPlayersFound = 0
	INT i = 0
	
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		RETURN FALSE
	ENDIF
	
	//First wipe the list (default value is zero which is a valid player index).
	REPEAT COUNT_OF(winningPlayers) i
		winningPlayers[i] = INVALID_PLAYER_INDEX()
	ENDREPEAT
	
	IF IS_THIS_A_VERSUS_MISSION()	
		PLAYER_INDEX currentPlayer
		REPEAT COUNT_OF(g_MissionControllerserverBD_LB.sleaderboard) i
			IF iNumPlayersFound < COUNT_OF(winningPlayers)
				INT iParticipant = g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant
				
				#IF IS_DEBUG_BUILD
				INT iPlayer = NATIVE_TO_INT(g_MissionControllerserverBD_LB.sleaderboard[i].playerID)
				STRING strPlayerName
				IF g_MissionControllerserverBD_LB.sleaderboard[i].playerID != INVALID_PLAYER_INDEX()
					IF NETWORK_IS_PLAYER_ACTIVE(g_MissionControllerserverBD_LB.sleaderboard[i].playerID)
						strPlayerName = GET_PLAYER_NAME(g_MissionControllerserverBD_LB.sleaderboard[i].playerID)
					ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD
				IF (GET_FRAME_COUNT() % 100) = 0
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - **************************************** ", i)
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - leaderboard index: ", i)
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - participant at index: ", iParticipant)
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - player ID at index: ", iPlayer)
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - player name at index: ", strPlayerName)
				ENDIF
				#ENDIF
				IF iParticipant > (-1)
					#IF IS_DEBUG_BUILD
					IF (GET_FRAME_COUNT() % 100) = 0
						PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - MC_Playerbd[iParticipant].iteam: ", MC_Playerbd[iParticipant].iteam)
					ENDIF
					#ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD
				IF (GET_FRAME_COUNT() % 100) = 0
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - MC_serverBD.iWinningTeam: ", MC_serverBD.iWinningTeam)
				ENDIF
				#ENDIF
				#ENDIF
				
				IF iParticipant > -1
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
						currentPlayer = INVALID_PLAYER_INDEX()
						IF CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST(iParticipant, i, currentPlayer)
							IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_START_SPECTATOR)
							AND NOT IS_PLAYER_SCTV(currentPlayer)
							AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(currentPlayer)
					
								IF IS_NET_PLAYER_OK(currentPlayer, FALSE)
								
									#IF IS_DEBUG_BUILD
									IF (GET_FRAME_COUNT() % 100) = 0
										TEXT_LABEL_63 tlName = GET_PLAYER_NAME(currentPlayer)
										PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - Player being added to list of winners:")
										PRINTLN("[NETCELEBRATION] Name: ", tlName)
										PRINTLN("[NETCELEBRATION] Array index: ", i)
										PRINTLN("[NETCELEBRATION] iParticipant: ", iParticipant)
										PRINTLN("[NETCELEBRATION] player id: ", NATIVE_TO_INT(currentPlayer))
										PRINTLN("[NETCELEBRATION] MC_Playerbd[iParticipant].iteam: ", MC_Playerbd[iParticipant].iteam)
										PRINTLN("[NETCELEBRATION] MC_serverBD.iWinningTeam: ", MC_serverBD.iWinningTeam)
									ENDIF
									#ENDIF
									winningPlayers[iNumPlayersFound] = currentPlayer
									iNumPlayersFound++
									
									#IF IS_DEBUG_BUILD
									IF (GET_FRAME_COUNT() % 100) = 0
										PRINTLN("[NETCELEBRATION] iNumPlayersFound: ", iNumPlayersFound)
									ENDIF
									#ENDIF
									
								ELSE
									#IF IS_DEBUG_BUILD
									IF (GET_FRAME_COUNT() % 100) = 0
										PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - not grabbing player. IS_NET_PLAYER_OK = FALSE.")
									ENDIF
									#ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
								IF (GET_FRAME_COUNT() % 100) = 0
									IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_START_SPECTATOR)
										PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - not grabbing player. IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_START_SPECTATOR) = TRUE.")
									ENDIF
									IF IS_PLAYER_SCTV(currentPlayer)
										PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - not grabbing player. IS_PLAYER_SCTV = TRUE.")
									ENDIF
									IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(currentPlayer)
										PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - not grabbing player. DID_PLAYER_JOIN_MISSION_AS_SPECTATOR = TRUE.")
									ENDIF
								ENDIF
								#ENDIf
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
	ELSE
		#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 100) = 0
			PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - Not a team event, just grabbing the top player.")
		ENDIF
		#ENDIf
		IF g_MissionControllerserverBD_LB.sleaderboard[0].iParticipant > -1
			winningPlayers[0] = GET_PLAYER_IN_LEADERBOARD_POS(0)
			
			IF IS_NET_PLAYER_OK(winningPlayers[0], FALSE)
				iNumPlayersFound++
			ENDIF
		ENDIF
	ENDIF
	
	iNumberPlayersFound = iNumPlayersFound
	
	IF iNumPlayersFound > 0
		
		RETURN TRUE
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF (GET_FRAME_COUNT() % 100) = 0
		PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - No valid players found.")
	ENDIF
	#ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_CELEBRATION_WINNER_FINISHED(BOOL bKeepDrawingAfterFinished = FALSE)	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_DontShowWinnerRoom)
		PRINTLN("HAS_CELEBRATION_WINNER_FINISHED - Skipping winner screen as ciOptionsBS29_DontShowWinnerRoom is set")
		RETURN TRUE
	ENDIF
	
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		RETURN TRUE
	ENDIF
	
	//Don't do the celebration screen if the player is just spectating.
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_START_SPECTATOR)
	AND NOT bIsSCTV // So we can see the winner scene on the live feed.
		RETURN TRUE
	ENDIF
	
	HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
	
	UNLOAD_IN_GAME_MISSION_SPECIFIC_SCALEFORM()
	
	PLAYER_INDEX winningPlayers[MAX_NUM_CELEBRATION_PEDS]
	BOOL bSuccessfullyGrabbedWinners = GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN(winningPlayers, sCelebrationData.iNumPlayerFoundForWinnerScene)	
	INT iWinnerLBDIndex = GET_LEADERBOARD_INDEX_OF_TOP_WINNER()
	INT iWinningPlayerID = NATIVE_TO_INT(GET_PLAYER_IN_LEADERBOARD_POS(iWinnerLBDIndex))
	PRINTLN("[RCC MISSION] - [NETCELEBRATION] - Grabbing winning player details from top player")
	PRINTLN("[RCC MISSION] - [NETCELEBRATION] - iWinnerLBDIndex: ", iWinnerLBDIndex, " iWinningPlayerID: ", iWinningPlayerID)
	
	TEXT_LABEL_63 tl63_AnimDict, tl63_AnimName
	TEXT_LABEL_63 tl63_IdleAnimDict, tl63_IdleAnimName
	
	IF ( NOT bSuccessfullyGrabbedWinners OR iWinningPlayerID <= (-1) )
		winningPlayers[0] = LocalPlayer
		iWinningPlayerID = NATIVE_TO_INT(LocalPlayer)
		IF NOT sCelebrationData.bCreateWinnerSceneEntities
			sCelebrationData.iDisplayingLocalPlayerAsLoser = 1
			PRINTLN("[RCC MISSION] - [NETCELEBRATION] - could not grab list of winners, setting flag to indicate we are displaying fallback loser screen.")
		ENDIF
	ELSE
		IF NOT sCelebrationData.bCreateWinnerSceneEntities
			sCelebrationData.iDisplayingLocalPlayerAsLoser = 0
			PRINTLN("[RCC MISSION] - [NETCELEBRATION] - grabbed list of winners, setting flag to indicate we are not displaying fallback loser screen.")
		ENDIF
	ENDIF
	
	IF sCelebrationData.bAllowPlayerNameToggles
		
		DRAW_THE_PLAYER_LIST(celebdpadVars)
		MAINTAIN_CELEBRATION_INSTRUCTIONAL_BUTTONS(sCelebrationData)
		CELEB_SET_PLAYERS_INVISIBLE_THIS_FRAME()
				
		DRAW_NG_CELEBRATION_PLAYER_NAMES(	sCelebrationData, sCelebrationData.iDrawNamesStage, sCelebrationData.iNumNamesToDisplay, 
											sCelebrationData.pedWinnerClones, sCelebrationData.tl31_pedWinnerClonesNames, 
											sCelebrationData.eCurrentStage, sCelebrationData.sfCelebration,
											sCelebrationData.playerNameMovies, sCelebrationData.bToggleNames)
	ENDIF
	
	HIDE_PLAYERS_FOR_CELEBRATION_WINNER_SCENE(sCelebrationData) // Fix for B* 2278693 - stop player models being seen during winner scene, only want ot see ped clones.
	
	SWITCH sCelebrationData.eCurrentStage
		CASE CELEBRATION_STAGE_SETUP
			
			CELEBRATION_SCREEN_TYPE eCelebType

			eCelebType = CELEBRATION_STANDARD
			IF g_FMMC_STRUCT.iCelebrationType = FMMC_CELEBRATION_TYPE_ISLAND_HEIST
				eCelebType = CELEBRATION_HEIST
			ENDIF
			PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_WINNER_FINISHED - USING CELEBRATION SCREEN: ", ENUM_TO_INT(eCelebType))
			
			REQUEST_CELEBRATION_SCREEN(sCelebrationData, eCelebType)
			
			IF LOAD_WINNER_SCENE_INTERIOR(sCelebrationData)
			
				IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
				AND NOT ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
				AND CREATE_WINNER_SCENE_ENTITIES(MC_serverBD_2.sCelebServer, sCelebrationData, winningPlayers, DEFAULT, NOT bSuccessfullyGrabbedWinners, NULL, FALSE, FALSE, FALSE, FALSE, DUMMY_MODEL_FOR_SCRIPT, GlobalplayerBD[iWinningPlayerID].iInteractionAnim, FALSE)				
										
					GET_CELEBRATION_ANIM_TO_PLAY(GlobalplayerBD[iWinningPlayerID].iInteractionAnim, IS_PED_MALE(sCelebrationData.pedWinnerClones[0]), FALSE, tl63_AnimDict, tl63_AnimName, INT_TO_ENUM(INTERACTION_ANIM_TYPES, GlobalplayerBD[iWinningPlayerID].iInteractionType), INT_TO_ENUM(CREW_INTERACTIONS, GlobalplayerBD[iWinningPlayerID].iCrewAnim), FALSE, MC_playerBD[iWinningPlayerID].iCustomPedModelIndex)
					
					BOOL bMale
					bMale = IS_PED_MALE(sCelebrationData.pedWinnerClones[0])
					IF MC_playerBD[iWinningPlayerID].iCustomPedModelIndex != -1
						bMale = TRUE
						PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_WINNER_FINISHED - bMale = TRUE because of custom model")
					ENDIF
					GET_CELEBRATION_IDLE_ANIM_TO_USE(sCelebrationData, tl63_IdleAnimDict, tl63_IdleAnimName, bMale)
					
					REQUEST_ANIM_DICT(tl63_AnimDict)
					REQUEST_ANIM_DICT(tl63_IdleAnimDict)
					
					IF (HAS_ANIM_DICT_LOADED(tl63_AnimDict) AND HAS_ANIM_DICT_LOADED(tl63_IdleAnimDict))
					OR HAS_NG_FAILSAFE_TIMER_EXPIRED(sCelebrationData)
						sCelebrationData.fAnimLength = GET_ANIM_DURATION(tl63_AnimDict, tl63_AnimName)
						PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_WINNER_FINISHED - fAnimLength = ", sCelebrationData.fAnimLength)
						RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
						START_NET_TIMER(sCelebrationData.sCelebrationTimer)
						RESET_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
						START_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
						
						STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "SUMMARY")
						STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "EARLYDEATH")
						FLASH_CELEBRATION_SCREEN(sCelebrationData, 0.33, 0.15, 0.33)
						DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData)

						//Cache the winner ID for later use in the anims, so that if the player leaves we still have the clone and anim array index.
						IF bSuccessfullyGrabbedWinners
						AND iWinningPlayerID > -1
							sCelebrationData.iWinnerPlayerID = iWinningPlayerID
						ENDIF

						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - Started flash.")						
						
						SET_BIT(iLocalBoolCheck7, LBOOL7_BLOCK_TOD_EVERY_FRAME)						
						
						sCelebrationData.eCurrentStage = CELEBRATION_STAGE_DOING_FLASH
					ENDIF
					
				ELSE
					
					IF ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
						IF NOT HAS_NET_TIMER_STARTED(sCelebrationData.timerDeathPostFxDelay)
							START_NET_TIMER(sCelebrationData.timerDeathPostFxDelay)
							PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, started timer timerDeathPostFxDelay.")
						ELSE
							IF HAS_NET_TIMER_EXPIRED(sCelebrationData.timerDeathPostFxDelay, 1000)
								PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, timer timerDeathPostFxDelay >= 1000. Calling CLEAR_KILL_STRIP_DEATH_EFFECTS.")
								CLEAR_KILL_STRIP_DEATH_EFFECTS()
							ENDIF
						ENDIF	
					ENDIF
					
				ENDIF
				
			ENDIF
			
			//In case the transition from the previous screen is tight: keep drawing to prevent pops.
			IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
				DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData)
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_DOING_FLASH
			DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData)
			
			IF (HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTransitionTimer, 375) OR IS_SCREEN_FADED_OUT())
				RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
				START_NET_TIMER(sCelebrationData.sCelebrationTimer)
				
				SET_SKYFREEZE_CLEAR(TRUE)
				SET_SKYBLUR_CLEAR()
				ANIMPOSTFX_STOP_ALL()
				
				IF bLocalPlayerOK
					SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
					REMOVE_PED_HELMET(LocalPlayerPed, TRUE) // url:bugstar:2244903 - remove helmet mid-flash.
				ENDIF
				
				START_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
				
				sCelebrationData.bUseFullBodyWinnerAnim = SHOULD_WINNER_SCREEN_USE_FULL_BODY_ANIM(sCelebrationData, iWinningPlayerID)
				
				PLACE_WINNER_SCENE_CAMERA(MC_serverBD_2.sCelebServer, sCelebrationData, camEndScreen, DEFAULT, iWinningPlayerID, sCelebrationData.bUseFullBodyWinnerAnim)
				FORCE_WEATHER_AND_TIME_FOR_CELEBRATION_WINNER()
				MC_serverBD.iTimeOfDay = TIME_OFF
				
				IF ARE_LIGHTS_TURNED_OFF()
					TURN_ON_ARTIFICIAL_LIGHTS_LOCALLY(FALSE)
					PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - Turning lights back on for celebration winner screen.")
				ENDIF
				
				TURN_OFF_ACTIVE_FILTERS()
			
				IF GET_REQUESTINGNIGHTVISION()
				OR GET_USINGNIGHTVISION()
					ENABLE_NIGHTVISION(VISUALAID_OFF)
					DISABLE_NIGHT_VISION_CONTROLLER(TRUE) // Added under instruction of Alwyn for B*2196822
				ENDIF
		
				PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - Transitioning to winner screen.")

				sCelebrationData.eCurrentStage = CELEBRATION_STAGE_TRANSITIONING
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_TRANSITIONING
			
			DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData)
			HIDE_PROBLEM_MODELS_FOR_CELEBRATION_SCREEN()
			
			IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, 375)
				SET_HIDE_PLAYERS_FOR_CELEBRATION_WINNER_SCENE(sCelebrationData)
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, 750)
				STRING strScreenName, strBackgroundColour
				TEXT_LABEL_63 strWinnerName
				TEXT_LABEL_63 strCrewName
				JOB_WIN_STATUS eJobWinStatus
				
				//Retrieve all the required stats for the race end screen.
				strScreenName = "WINNER"
				strWinnerName = ""
				strCrewName = ""
				
				IF winningPlayers[0] != INVALID_PLAYER_INDEX()
				AND g_MissionControllerserverBD_LB.sleaderboard[iWinnerLBDIndex].iParticipant > -1
				AND bSuccessfullyGrabbedWinners
					strWinnerName = GET_PLAYER_NAME(winningPlayers[0]) //MC_serverBD_2.tParticipantNames[g_MissionControllerserverBD_LB.sleaderboard[iWinnerLBDIndex].iParticipant]
					strCrewName = GET_CREW_NAME_FOR_CELEBRATION_SCREEN(winningPlayers[0])
				ENDIF
				PRINTLN("[HAS_CELEBRATION_WINNER_FINISHED][RH]1 The winning player name is ", strWinnerName)
				sCelebrationData.iEstimatedScreenDuration = CELEBRATION_SCREEN_STAT_WIPE_TIME * 2 
				
				IF sCelebrationStats.bPassedMission
					eJobWinStatus = JOB_STATUS_WIN
					strBackgroundColour = "HUD_COLOUR_FRIENDLY"
					
					PLAY_MISSION_COMPLETE_AUDIO("FRANKLIN_BIG_01")
				ELSE
					//Use the winner status in all cases except if the winner ped couldn't be retrieved.
					IF bSuccessfullyGrabbedWinners
						eJobWinStatus = JOB_STATUS_WIN
					ELSE
						eJobWinStatus = JOB_STATUS_LOSE
					ENDIF
					strBackgroundColour = "HUD_COLOUR_NET_PLAYER1"
					PLAY_MISSION_COMPLETE_AUDIO("GENERIC_FAILED")
				ENDIF
				
				//Build the screen
				CREATE_CELEBRATION_SCREEN(sCelebrationData, strScreenName, strBackgroundColour)
				SET_CELEBRATION_SCREEN_STAT_DISPLAY_TIME(sCelebrationData, CELEBRATION_SCREEN_WINNER_STAT_DISPLAY_TIME - CELEBRATION_SCREEN_STAT_WIPE_TIME)
				
				IF winningPlayers[1] != INVALID_PLAYER_INDEX() //1637254 - If multiple players won then don't show specifics on the winner display.
					strWinnerName = ""
					
					IF MC_serverBD.iWinningTeam > -1
						strWinnerName = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(MC_serverBD.iWinningTeam, FALSE, FALSE, TRUE) // g_sMission_TeamName[MC_serverBD.iWinningTeam]
					ENDIF
					
					PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - Adding team name: ", strWinnerName)
				
					ADD_WINNER_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, eJobWinStatus, strWinnerName, "", "", sCelebrationStats.iLocalPlayerBetWinnings, DEFAULT, TRUE, FALSE, IS_BIT_SET(iCelebrationTeamNameIsNotLiteral, MC_serverBD.iWinningTeam))
				ELSE
					ADD_WINNER_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, eJobWinStatus, "", strCrewName, strWinnerName, sCelebrationStats.iLocalPlayerBetWinnings)
				ENDIF 
					
				PRINTLN("[HAS_CELEBRATION_WINNER_FINISHED][RH]2 The winning player name is ", strWinnerName)
				sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_WINNER_STAT_DISPLAY_TIME //Winner screen is guaranteed.
				
				//Add extra time if betting is shown.
				IF sCelebrationStats.iLocalPlayerBetWinnings != 0
					sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_WINNER_STAT_DISPLAY_TIME
				ENDIF
				
				sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_WIPE_TIME * 2
				
				ADD_BACKGROUND_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName)
				SHOW_CELEBRATION_SCREEN(sCelebrationData, strScreenName)
				
				ADJUST_ESTIMATED_TIME_BASED_ON_ANIM_LENGTH(sCelebrationData)
				
				RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
				START_NET_TIMER(sCelebrationData.sCelebrationTimer)
				
				RESET_CELEBRATION_PRE_LOAD(sCelebrationData)
				
				PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - Starting playback of winner screen.")
				MP_TEXT_CHAT_DISABLE(TRUE)
				sCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_PLAYING			
			VECTOR vCamRot
			vCamRot = GET_FINAL_RENDERED_CAM_ROT()
		
			//Don't draw the screen if we're in skycam.
			IF ABSF(vCamRot.x) < 45.0
				
				DRAW_CELEBRATION_SCREEN_USING_3D_BACKGROUND(sCelebrationData, <<0.0, -0.5, 0.0>>, <<0.0, 0.0, 0.0>>, <<10.0, 5.0, 5.0>>)
				
				DRAW_BLACK_RECT_FOR_WINNER_SCREEN_END_TRANSITION(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sCelebrationData.sCelebrationTimer.Timer), 
																 sCelebrationData.iEstimatedScreenDuration)
																 
				//Block the switch PostFX as it comes on during the winner screen.
				ANIMPOSTFX_STOP_ALL()
				RESET_ADAPTATION(1)
			ENDIF
			
			//Once the timer finishes then progress: this depends on how many elements were added to the screen.
			IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, sCelebrationData.iEstimatedScreenDuration)
				
				IF NOT bKeepDrawingAfterFinished
					sCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED
				ENDIF
				
				RESET_WEATHER_AND_TIME_AFTER_CELEBRATION_WINNER()
				
				IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					VECTOR vCamCoords
					vCamCoords = GET_CAM_COORD(camEndScreen)
					vCamCoords.z = 1000.0
					SET_CAM_COORD(camEndScreen, vCamCoords)
					SET_ENTITY_COORDS(LocalPlayerPed, vCamCoords, FALSE)
					FREEZE_ENTITY_POSITION(LocalPlayerPed, TRUE)
					PRINTLN("[NETCELEBRATION] - put winner cam and player ped at ", vCamCoords)
				ENDIF
				
				sCelebrationData.bAllowPlayerNameToggles = FALSE
				MP_TEXT_CHAT_DISABLE(FALSE)
				RETURN TRUE
			
			ELSE
			
				sCelebrationData.bAllowPlayerNameToggles = TRUE
				
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_FINISHED
			MP_TEXT_CHAT_DISABLE(FALSE)
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	//1645969 - The anim needs to work if the winning player leaves half way through, their ID will be cached so we only need to check the following:
	// - The clone was created successfully.
	// - The clone is not the player.
	// - The clone is the player and the player won the race (i.e. we're not doing the fallback "Loser" screen).
	IF sCelebrationData.eCurrentStage > CELEBRATION_STAGE_SETUP
	AND sCelebrationData.eCurrentStage < CELEBRATION_STAGE_FINISHED
		IF sCelebrationData.iDisplayingLocalPlayerAsLoser = 0
			IF DOES_ENTITY_EXIST(sCelebrationData.pedWinnerClones[0])
				IF NOT IS_PED_INJURED(sCelebrationData.pedWinnerClones[0])
					IF sCelebrationData.pedWinnerClones[0] != LocalPlayerPed
					OR (sCelebrationData.pedWinnerClones[0] = LocalPlayerPed AND bSuccessfullyGrabbedWinners)
						IF DOES_CAM_EXIST(camEndScreen)
							IF IS_CAM_RENDERING(camEndScreen)
								UPDATE_CELEBRATION_WINNER_ANIMS(sCelebrationData, sCelebrationData.pedWinnerClones[0], sCelebrationData.iWinnerPlayerID, 0, FALSE, FALSE, FALSE, PAIRED_CELEBRATION_GENDER_COMBO_NOT_SET, -1, -1, 200, FALSE, FALSE, MC_playerBD[iWinningPlayerID].iCustomPedModelIndex)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL OK_TO_DO_MISSION_OUTRO()
	
	IF bIsAnySpectator
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_A_VERSUS_MISSION()
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_CELEBRATION_SUMMARY_FINISHED(BOOL bDoEarlyDeathScreen = FALSE)	
	
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		RETURN TRUE
	ENDIF
	
	IF MC_serverBD.iNextMission < FMMC_MAX_STRAND_MISSIONS
	AND MC_serverBD.iNextMission >= 0
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23NextContentID[MC_serverBD.iNextMission])
		//Strand mission is going to be initialised soon
		IF g_FMMC_STRUCT.eBranchingTransitionType[MC_serverBD.iNextMission] != FMMC_BRANCHING_MISSION_TRANSITION_TYPE_MOCAP
		AND sCelebrationStats.bPassedMission
			PRINTLN("[RCC MISSION] - [NETCELEBRATION][MSRAND] - HAS_CELEBRATION_SUMMARY_FINISHED - TRUE (Airlock or Fade Out strand transition")
			RETURN TRUE
		ENDIF
	ENDIF
	
	//Don't do a celebration screen if the player died early on an LTS (just go straight to spectator).
	IF bDoEarlyDeathScreen
		IF IS_THIS_A_VERSUS_MISSION()
			PRINTLN("[RCC MISSION] - [NETCELEBRATION] - Early death screen - This is a VS mission, heading straight to spectator cam")
			sCelebrationData.eCurrentStage = CELEBRATION_STAGE_WAITING_FOR_SPECTATOR_CAM
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (bDoEarlyDeathScreen AND sCelebrationData.eCurrentStage > CELEBRATION_STAGE_SETUP)
	OR NOT bDoEarlyDeathScreen
		HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
		PRINTLN("[RCC MISSION] - [NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED - HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME")
	ENDIF
	
	IF sCelebrationData.eCurrentStage = CELEBRATION_STAGE_SETUP
		IF IS_THIS_A_ROUNDS_MISSION()
		AND NOT AM_I_AT_END_OF_ROUNDS_MISSION()
		AND NOT SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
			PRINTLN("[RCC MISSION] - [NETCELEBRATION] - IS_THIS_A_ROUNDS_MISSION = TRUE, AM_I_AT_END_OF_ROUNDS_MISSION = FALSE, SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD = FALSE. Waiting until we can validly call SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	//Don't do the celebration screen if the player is just spectating.
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_START_SPECTATOR)
		PRINTLN("[RCC MISSION] - [NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED - PBBOOL_START_SPECTATOR")
		RETURN TRUE
	ENDIF
	
	NET_SET_PLAYER_CONTROL_FLAGS NSPC_CLEAR_TASKS_NO_RAGDOLL = INT_TO_ENUM(NET_SET_PLAYER_CONTROL_FLAGS, PICK_INT(IS_PED_RAGDOLL(LocalPlayerPed), 0, ENUM_TO_INT(NSPC_CLEAR_TASKS)))
		
	IF IS_PLAYER_IN_CREATOR_AIRCRAFT(GET_PLAYER_INDEX())
	AND g_iInteriorTurretSeat != -1
		PRINTLN("[RCC MISSION] - [NETCELEBRATION] - DO NOT CLEAR TASKS as we are in an aircraft seat.")
		NSPC_CLEAR_TASKS_NO_RAGDOLL = INT_TO_ENUM(NET_SET_PLAYER_CONTROL_FLAGS, 0)
	ENDIF
	
	SWITCH sCelebrationData.eCurrentStage
		CASE CELEBRATION_STAGE_SETUP
		
			CELEBRATION_SCREEN_TYPE eCelebType
			
			//If it's a round mission and we need to display the LB the make sure we add the JPs
			IF IS_THIS_A_ROUNDS_MISSION()
			AND SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
			AND	IS_BIT_SET(iLocalBoolCheck6, LBOOL6_HIDE_JOB_POINTS_CELEB_SCREEN)
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - CLEAR_BIT(iLocalBoolCheck6, LBOOL6_HIDE_JOB_POINTS_CELEB_SCREEN)")	
				CLEAR_BIT(iLocalBoolCheck6, LBOOL6_HIDE_JOB_POINTS_CELEB_SCREEN)
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - ADD_TO_JOB_POINTS_TOTAL - sCelebrationStats.iLocalPlayerJobPoints")	
				ADD_TO_JOB_POINTS_TOTAL(sCelebrationStats.iLocalPlayerJobPoints)
			ENDIF
			
			eCelebType = CELEBRATION_STANDARD
			IF g_FMMC_STRUCT.iCelebrationType = FMMC_CELEBRATION_TYPE_ISLAND_HEIST
				eCelebType = CELEBRATION_HEIST
			ENDIF
			PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SUMMARY_FINISHED - USING CELEBRATION SCREEN: ", ENUM_TO_INT(eCelebType))
				
			REQUEST_CELEBRATION_SCREEN(sCelebrationData, eCelebType)
			
			INT iWinnerLBDIndex
			iWinnerLBDIndex = -1
			BOOL bFetchedStatsFromServer, bPlayerInSameCrewAsWinner, bDelayShard
			GAMER_HANDLE sWinnerHandle
			
			IF IS_THIS_A_ROUNDS_MISSION()
			AND SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
			AND g_sTransitionSessionData.sMissionRoundData.piWinner != INVALID_PLAYER_INDEX()
				iWinnerLBDIndex = GET_LEADERBOARD_INDEX_OF_PLAYER(g_sTransitionSessionData.sMissionRoundData.piWinner)
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - IS_THIS_A_ROUNDS_MISSION = TRUE")	
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD = TRUE")	
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - sTransitionSessionData.sMissionRoundData.piWinner != INVALID_PLAYER_INDEX()")	
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - set iWinnerLBDIndex = GET_LEADERBOARD_INDEX_OF_PLAYER(g_sTransitionSessionData.sMissionRoundData.piWinner) = ", iWinnerLBDIndex) 	
			ELSE
				#IF IS_DEBUG_BUILD
				IF NOT IS_THIS_A_ROUNDS_MISSION()
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - IS_THIS_A_ROUNDS_MISSION = FALSE")	
				ENDIF
				IF NOT SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD = FALSE")	
				ENDIF
				IF g_sTransitionSessionData.sMissionRoundData.piWinner = INVALID_PLAYER_INDEX()
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - sTransitionSessionData.sMissionRoundData.piWinner = INVALID_PLAYER_INDEX()")	
				ENDIF
				#ENDIF
				iWinnerLBDIndex = GET_LEADERBOARD_INDEX_OF_TOP_WINNER()
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - set iWinnerLBDIndex = GET_LEADERBOARD_INDEX_OF_TOP_WINNER()", iWinnerLBDIndex) 	
			ENDIF
			
			IF bDoEarlyDeathScreen
				bFetchedStatsFromServer = TRUE
				bPlayerInSameCrewAsWinner = FALSE
			ELSE
				IF iWinnerLBDIndex > -1
					IF g_MissionControllerserverBD_LB.sleaderboard[iWinnerLBDIndex].iParticipant > -1
						sWinnerHandle = lbdVars.aGamer[g_MissionControllerserverBD_LB.sleaderboard[iWinnerLBDIndex].iParticipant] //This should be able to grab handles for players that have left already left the game.
					ENDIF
				ENDIF
				bPlayerInSameCrewAsWinner = IS_PLAYER_IN_SAME_CREW_AS_ME(sWinnerHandle, bFetchedStatsFromServer)
			ENDIF
	
			//1633762 - If passing a mission then have a delay between the flash and the shard.
			IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bStartedCelebrationBeforeCutsceneEnded)
			AND NOT bDoEarlyDeathScreen
			AND sCelebrationStats.bPassedMission
				IF HAS_NET_TIMER_STARTED(sCelebrationData.sCelebrationTransitionTimer)
					IF NOT HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTransitionTimer, 650)
						bDelayShard = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF sCelebrationStats.bPassedMission
			AND SHOULD_CELEBRATION_SCREEN_SKIP_NJVS_AND_LB()
				PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SUMMARY_FINISHED - Calling SET_TRANSITION_SESSIONS_SKIP_LB_AND_NJVS") 	
				SET_TRANSITION_SESSIONS_SKIP_LB_AND_NJVS()
			ENDIF
			
			// This might cause issues where we see under the world but we need this here so players are not stuck in a Black Screen.
			// Normally happens because we're sent to spectator and before we get a target we end the mission.
			IF IS_SCREEN_FADED_OUT()
			AND eScriptedCutsceneProgress != SCRIPTEDCUTPROG_FADEOUT_AND_WARP
				PRINTLN("[LM][url:bugstar:4372431 - HAS_CELEBRATION_SCREEN_FINISHED - Emergancy Screen Fade In. Celebration Screen should start.")
				DO_SCREEN_FADE_IN_FOR_TRANSITION(250)
			ENDIF
				
			IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
			AND bFetchedStatsFromServer
			AND (bDoEarlyDeathScreen OR CONTROL_UPDATING_CHALLENGES())
			AND NOT bDelayShard
			AND (IS_SCREEN_FADED_IN() AND NOT IS_SCREEN_FADING_OUT() AND NOT IS_SCREEN_FADED_OUT())
			AND NOT ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
			
				STRING strScreenName, strBackgroundColour
				INT iCurrentRP, iCurrentLvl, iNextLvl, iRPToReachCurrentLvl, iRPToReachNextLvl, iChallengePart, iTotalChallengeParts
				BOOL bWonChallengePart
			
				//Retrieve all the required stats for the race end screen.
				strBackgroundColour = "HUD_COLOUR_BLACK"
				
				IF bDoEarlyDeathScreen
					strScreenName = "EARLYDEATH"
				ELSE
					strScreenName = "SUMMARY"
					iCurrentRP = GET_PLAYER_XP(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())) - sCelebrationStats.iLocalPlayerXP
					iCurrentLvl = GET_FM_RANK_FROM_XP_VALUE(iCurrentRP) //GET_PLAYER_GLOBAL_RANK(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())) //We don't want the current level, but their level before the XP was given.
					iNextLvl = iCurrentLvl + 1
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - iCurrentRP = ", iCurrentRP, ", iCurrentLvl = ", iCurrentLvl, ", iNextLvl = ", iNextLvl)	
					iRPToReachCurrentLvl = GET_XP_NEEDED_FOR_FM_RANK(iCurrentLvl)
					iRPToReachNextLvl = GET_XP_NEEDED_FOR_FM_RANK(iNextLvl)
					iChallengePart = g_sCurrentPlayListDetails.iPlaylistProgress
					iTotalChallengeParts = g_sCurrentPlayListDetails.iLength
					sCelebrationData.iEstimatedScreenDuration = CELEBRATION_SCREEN_STAT_WIPE_TIME * 2
					
					IF IS_THIS_A_ROUNDS_MISSION_FOR_CELEBRATION()
					#IF IS_DEBUG_BUILD
					OR g_bFakeRound
					#ENDIF
						
						IF g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartRp = 0
							g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartRp = iCurrentRP 
						ENDIF
						
						IF g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartLevel = 0
							g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartLevel = GET_FM_RANK_FROM_XP_VALUE(g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartRp)
						ENDIF
						
						g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRpGained += sCelebrationStats.iLocalPlayerXP
						g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iJobPoints = sCelebrationStats.iLocalPlayerJobPoints
						g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iCash += sCelebrationStats.iLocalPlayerCash
						
						IF SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
						#IF IS_DEBUG_BUILD
						OR g_bFakeEndOfMatch
						#ENDIF
							
							g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iEndLvl = GET_FM_RANK_FROM_XP_VALUE(GET_PLAYER_XP(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())))
							g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRPToReachStartLvl = GET_XP_NEEDED_FOR_FM_RANK(g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartLevel)
							g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRPToReachEndLvl = GET_XP_NEEDED_FOR_FM_RANK(g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iEndLvl)
							
							g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iNextLvl = (g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartLevel + 1)
							g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRptoReachNextLvl = GET_XP_NEEDED_FOR_FM_RANK(g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iNextLvl)
							
						ENDIF
					ENDIF
					
				ENDIF				
				
				BOOL bBlackOverride
				INT iCreateStatWallAudioType
				
				//Build the screen
				CREATE_CELEBRATION_SCREEN(sCelebrationData, strScreenName, strBackgroundColour, bBlackOverride, iCreateStatWallAudioType)
				
				IF bDoEarlyDeathScreen
				
					//Early death screen is for missions where you can run out of lives: single screen indicating fail then transition to spectator cam.
					PRINTLN("[NETCELEBRATION] - [RCC MISSION] - call 0 to ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN.")
					ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CELEBRATION_JOB_TYPE_MISSION_EARLY_FAIL, sCelebrationStats.bPassedMission, 
															 sCelebrationStats.strFailReason, sCelebrationStats.strFailLiteral, sCelebrationStats.strFailLiteral2)
					
					sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
				ELSE
					IF IS_THIS_A_VERSUS_MISSION()
						PRINTLN("[NETCELEBRATION] - [RCC MISSION] - call 2 to ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN.")
						ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CELEBRATION_JOB_TYPE_VS_MISSION, sCelebrationStats.bPassedMission, 
															 "", "", "")
					ELSE
						PRINTLN("[NETCELEBRATION] - [RCC MISSION] - call 3 to ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN.")
						ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CELEBRATION_JOB_TYPE_MISSION, sCelebrationStats.bPassedMission, 
																sCelebrationStats.strFailReason, sCelebrationStats.strFailLiteral, sCelebrationStats.strFailLiteral2)
					ENDIF
					
					sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
					
					#IF IS_DEBUG_BUILD
					BOOL bRoundsMissionForCelTemp, bIsRoundsMission, bDisplayRoundsLb
					bRoundsMissionForCelTemp = IS_THIS_A_ROUNDS_MISSION_FOR_CELEBRATION()
					bIsRoundsMission = IS_THIS_A_ROUNDS_MISSION()
					bDisplayRoundsLb = SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
					PRINTLN("[NETCELEBRATION] - IS_THIS_A_ROUNDS_MISSION_FOR_CELEBRATION = ", bRoundsMissionForCelTemp)
					PRINTLN("[NETCELEBRATION] - IS_THIS_A_ROUNDS_MISSION = ", bIsRoundsMission)
					PRINTLN("[NETCELEBRATION] - SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD = ", bDisplayRoundsLb)
					#ENDIF
					
					IF NOT IS_THIS_A_ROUNDS_MISSION_FOR_CELEBRATION()
					#IF IS_DEBUG_BUILD
					AND NOT g_bFakeRound
					#ENDIF
					
						IF IS_PLAYLIST_DOING_HEAD_TO_HEAD()
							ADD_CHALLENGE_PART_RESULT_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CHALLENGE_PART_HEAD_TO_HEAD, iChallengePart, iTotalChallengeParts, bPlayerInSameCrewAsWinner)
							sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
						ELIF IS_PLAYLIST_DOING_CHALLENGE()
							IF sCelebrationStats.iLocalPlayerScore >= g_sCurrentPlayListDetails.sLoadedMissionDetails[g_sCurrentPlayListDetails.iCurrentPlayListPosition].iBestScore
								bWonChallengePart = TRUE
							ENDIF
						
							ADD_CHALLENGE_PART_RESULT_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CHALLENGE_PART_CREW, iChallengePart, iTotalChallengeParts, bWonChallengePart)
							sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
						ENDIF
						
						IF sCelebrationStats.iLocalPlayerCash != 0
							PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED - calling: ADD_CASH_TO_CELEBRATION_SCREEN, passing: $", sCelebrationStats.iLocalPlayerCash)
							ADD_CASH_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, sCelebrationStats.iLocalPlayerCash)
							sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME //If displaying cash do an extra screen.
						ENDIF
						
						IF NOT SHOULD_CELEBRATION_SCREEN_SKIP_JOB_POINTS()
							IF sCelebrationStats.iLocalPlayerCash != 0
								PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED - calling: ADD_JOB_POINTS_TO_CELEBRATION_SCREEN, passing: JP", sCelebrationStats.iLocalPlayerJobPoints)
								ADD_JOB_POINTS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, sCelebrationStats.iLocalPlayerJobPoints)
							ELSE
								PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED - calling: ADD_JOB_POINTS_TO_CELEBRATION_SCREEN, passing: JP", sCelebrationStats.iLocalPlayerJobPoints)
								ADD_JOB_POINTS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, sCelebrationStats.iLocalPlayerJobPoints, TRUE, FALSE)
							ENDIF
						ENDIF
						
						PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED - calling: ADD_REP_POINTS_AND_RANK_BAR_TO_CELEBRATION_SCREEN, passing: RP", sCelebrationStats.iLocalPlayerXP)
						ADD_REP_POINTS_AND_RANK_BAR_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, sCelebrationStats.iLocalPlayerXP, iCurrentRP, 
													   	   iRPToReachCurrentLvl, iRPToReachNextLvl, iCurrentLvl, iNextLvl)
						sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME * 2 //Two screens are guaranteed.
						
						IF iCurrentRP + sCelebrationStats.iLocalPlayerXP > iRPToReachNextLvl
							sCelebrationData.iEstimatedScreenDuration += ((CELEBRATION_SCREEN_STAT_DISPLAY_TIME / 2) + (CELEBRATION_SCREEN_STAT_DISPLAY_TIME / 4))
						ENDIF
					
					ELSE
						
						IF SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
						#IF IS_DEBUG_BUILD
						OR g_bFakeEndOfMatch
						#ENDIF
						
							IF g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iCash != 0
								PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED - ROUNDS MISSION - calling: ADD_CASH_TO_CELEBRATION_SCREEN, passing: $", g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iCash)
								ADD_CASH_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iCash)
								sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME //If displaying cash do an extra screen.
							ENDIF
							
							IF NOT SHOULD_CELEBRATION_SCREEN_SKIP_JOB_POINTS()
								IF g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iCash != 0
									PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED - ROUNDS MISSION - calling: ADD_JOB_POINTS_TO_CELEBRATION_SCREEN, passing: JP", g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iJobPoints)
									ADD_JOB_POINTS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iJobPoints)
								ELSE
									PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED - ROUNDS MISSION - calling: ADD_JOB_POINTS_TO_CELEBRATION_SCREEN, passing: JP", g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iJobPoints)
									ADD_JOB_POINTS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iJobPoints, TRUE, FALSE)
								ENDIF
							ENDIF
							
							PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED - ROUNDS MISSION - calling: ADD_REP_POINTS_AND_RANK_BAR_TO_CELEBRATION_SCREEN, passing: RP", g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRpGained)
							ADD_REP_POINTS_AND_RANK_BAR_TO_CELEBRATION_SCREEN(	sCelebrationData, 
																				strScreenName, 
																				g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRpGained,
																				g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartRp,
																				g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRPToReachStartLvl,
																				g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRptoReachNextLvl,
																				g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartLevel,
																				g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iNextLvl)
							sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME * 2 //Two screens are guaranteed.
							
							IF g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iEndLvl > g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartLevel
								sCelebrationData.iEstimatedScreenDuration += ((CELEBRATION_SCREEN_STAT_DISPLAY_TIME / 2) + (CELEBRATION_SCREEN_STAT_DISPLAY_TIME / 4))
							ENDIF
							
						ENDIF
						
					ENDIF
						
				ENDIF

				ADD_BACKGROUND_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, -1, 2)
				SHOW_CELEBRATION_SCREEN(sCelebrationData, strScreenName)
				
				SET_CELEBRATION_SCREEN_AS_ACTIVE(TRUE)
				
				RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
				START_NET_TIMER(sCelebrationData.sCelebrationTimer)
			
				IF NOT bDoEarlyDeathScreen
					START_AUDIO_SCENE("MP_JOB_CHANGE_RADIO_MUTE")
					PRINTLN("[WJK] - called START_AUDIO_SCENE(MP_JOB_CHANGE_RADIO_MUTE).")
					
					SET_CHAT_ENABLED_FOR_CELEBRATION_SCREEN(TRUE)
				ELSE
					SET_BIT(sSaveOutVars.iBitSet,ciRATINGS_PLAYED)
				
					//If this is an early death screen then keep everything on screen indefinitely.
					ADD_PAUSE_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, 15000)
				ENDIF
								
				IF OK_TO_DO_MISSION_OUTRO()
					IF NOT sCelebrationStats.bPassedMission
					OR (bLocalPlayerOK AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed))
						SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bDoDelayedCelebrationFreeze)
					ENDIF
				ELSE
					IF bDoEarlyDeathScreen
						SET_SKYFREEZE_FROZEN()
						
						//These aren't called in advance when dying (so the WASTED message can display).
						HANG_UP_AND_PUT_AWAY_PHONE()
						CLOSE_WEB_BROWSER()
						HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
					ELSE
						SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bDoDelayedCelebrationFreeze)
					ENDIF
				ENDIF
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
					START_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
				ENDIF
				
				IF IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_SCENE")
					STOP_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
				ENDIF
				
				IF IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_PP_SCENE")
					STOP_AUDIO_SCENE("MP_LEADERBOARD_PP_SCENE")
				ENDIF
				
				IF bLocalPlayerOK
					g_bMissionRemovedWanted = TRUE
					SET_EVERYONE_IGNORE_PLAYER(LocalPlayer,TRUE)
					SET_PLAYER_WANTED_LEVEL(LocalPlayer,0)
					SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
					
					//Don't turn player control off if we didn't do the freeze, otherwise there will be a visible pop.
					IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bDoDelayedCelebrationFreeze)
					AND NOT OK_TO_DO_MISSION_OUTRO()
						IF DID_SPEC_BAILED_FOR_TRANSITION()							
							NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS_NO_RAGDOLL | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
						ELSE
							NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_CLEAR_TASKS_NO_RAGDOLL | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
						ENDIF
						FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, TRUE)
					ENDIF
				ENDIF
				
				CLEAR_HELP()
				
				PRINTLN("[NETCELEBRATION] Adding stats ---------------------------")
				PRINTLN("strScreenName ", strScreenName)
				PRINTLN("iCurrentRP ", iCurrentRP)
				PRINTLN("iCurrentLvl ", iCurrentLvl)
				PRINTLN("iNextLvl ", iNextLvl)
				PRINTLN("iRPToReachCurrentLvl ", iRPToReachCurrentLvl)
				PRINTLN("iRPToReachNextLvl ", iRPToReachNextLvl)
				PRINTLN("iChallengePart ", iChallengePart)
				PRINTLN("iTotalChallengeParts ", iTotalChallengeParts)
				PRINTLN("sCelebrationData.iEstimatedScreenDuration ", sCelebrationData.iEstimatedScreenDuration)
				PRINTLN("---------------------------------------------------------")
				
				RESET_CELEBRATION_PRE_LOAD(sCelebrationData)
			
				sCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING
				
			ELSE
				
				PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - cannot process celebration screen yet.")
				
				#IF IS_DEBUG_BUILD
					
					IF NOT HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - Waiting on HAS_CELEBRATION_SCREEN_LOADED")
					ENDIF
					
					IF NOT bFetchedStatsFromServer
						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - Waiting on stats to be fetched.")
					ENDIF
					
					IF NOT (bDoEarlyDeathScreen OR CONTROL_UPDATING_CHALLENGES())
						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - (bDoEarlyDeathScreen OR CONTROL_UPDATING_CHALLENGES()) = FALSE.")
						IF NOT bDoEarlyDeathScreen
							PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - bDoEarlyDeathScreen = FALSE.")
						ENDIF
						IF NOT CONTROL_UPDATING_CHALLENGES()
							PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - Waiting on CONTROL_UPDATING_CHALLENGES")
						ENDIF
					ENDIF
					
					IF bDelayShard
						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - bDelayShard = TRUE.")
					ENDIF
					
					IF NOT IS_SCREEN_FADED_IN()
						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - IS_SCREEN_FADED_IN() = FALSE.")
					ENDIF
					
				#ENDIF
				
				IF ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
					IF NOT HAS_NET_TIMER_STARTED(sCelebrationData.timerDeathPostFxDelay)
						START_NET_TIMER(sCelebrationData.timerDeathPostFxDelay)
						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, started timer timerDeathPostFxDelay.")
					ELSE
						IF HAS_NET_TIMER_EXPIRED(sCelebrationData.timerDeathPostFxDelay, 1000)
							PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, timer timerDeathPostFxDelay >= 1000. Calling CLEAR_KILL_STRIP_DEATH_EFFECTS.")
							CLEAR_KILL_STRIP_DEATH_EFFECTS()
						ENDIF
					ENDIF
				ENDIF
					
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_PLAYING
		
			DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData, FALSE)
			
			//Delay the freeze so it syncs with the screen coming down.
			IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bDoDelayedCelebrationFreeze)
				PRINTLN("[RCC MISSION] - Doing ciCELEBRATION_BIT_SET_bDoDelayedCelebrationFreeze")
				IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, CELEBRATION_SCREEN_STAT_WIPE_TIME)
					SET_SKYFREEZE_FROZEN()
					CLEAR_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bDoDelayedCelebrationFreeze)
					
					VEHICLE_INDEX PlayerVehicle 
					
					IF PLAYER_IS_THE_DRIVER_IN_A_CAR()
						PlayerVehicle = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(PlayerVehicle)
							SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE(PlayerVehicle, TRUE)
						ENDIF
					ENDIF
					
					IF DID_SPEC_BAILED_FOR_TRANSITION()
						NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS_NO_RAGDOLL | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
					ELSE
						NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_CLEAR_TASKS_NO_RAGDOLL | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
					ENDIF
					FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, TRUE)
					
					IF bLocalPlayerOK
						IF DOES_ENTITY_EXIST(PlayerVehicle)
							IF NOT IS_ENTITY_DEAD(PlayerVehicle)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(PlayerVehicle)
									SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bDoEarlyDeathScreen
				//Allow enough time for the screen to come down before trying to preload the spectator cam (to avoid framerate issues).
				IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, CELEBRATION_SCREEN_STAT_WIPE_TIME * 2)
					PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - Early death screen is active, end early to allow spectator transition.")
					
					RETURN TRUE
				ENDIF
			ELSE
				//If we're going straight from the summary to a winner screen then do an early flash to help the transition.
				IF NOT sCelebrationData.bTriggeredEarlyFlash
					IF NOT bDoEarlyDeathScreen
						IF IS_THIS_A_VERSUS_MISSION()
							//Detach the cam on the player if they're stationary to stop the weird swings if the player turns slightly.
							IF bLocalPlayerOK
							AND DOES_CAM_EXIST(camEndScreen)
								IF GET_ENTITY_SPEED(LocalPlayerPed) < 0.1
									DETACH_CAM(camEndScreen)
									STOP_CAM_POINTING(camEndScreen)
								ENDIF
							ENDIF
						
							IF HAS_CELEBRATION_SHARD_FINISHED_FALL_AWAY()
								IF SHOULD_POSTFX_BE_WINNER_VERSION()
									PLAY_CELEB_WIN_POST_FX()
								ELSE
									PLAY_CELEB_LOSE_POST_FX()
								ENDIF
								
								IF IS_THIS_ROCKSTAR_MISSION_NEW_HALLOWEEN_ADVERSARY_2022(g_FMMC_STRUCT.iAdversaryModeType)
								AND IS_AUDIO_SCENE_ACTIVE("Halloween_Adversary_Main_Mixscene")
									STOP_AUDIO_SCENE("Halloween_Adversary_Main_Mixscene")
									PRINTLN("[NETCELEBRATION] Stopping \"Halloween_Adversary_Main_Mixscene\" audio scene")
								ENDIF
								
								START_CELEBRATION_CAMERA_ZOOM_TRANSITION(camEndScreen)
								sCelebrationData.bTriggeredEarlyFlash = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				SCRIPT_TIMER celebrationEndTimer
				INT celebrationEndTime
				
				
				celebrationEndTimer = sCelebrationData.sCelebrationTimer
				celebrationEndTime = sCelebrationData.iEstimatedScreenDuration
				
				// B* 2167120 - stop player falling through world - it's messing with the streaming.
				IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_ClearedTasksForOutOfVehicle)
					IF HAS_NET_TIMER_STARTED(celebrationEndTimer)
						IF HAS_NET_TIMER_EXPIRED(celebrationEndTimer, 6000)
							IF DOES_ENTITY_EXIST(LocalPlayerPed)
								IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
								AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
									IF NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
										SET_ENTITY_VISIBLE(LocalPlayerPed, FALSE)
										PRINTLN("[RCC MISSION] - SET_ENTITY_VISIBLE(LocalPlayerPed, FALSE), so other players don't see player pop out of vehicle when tasks are cleared immediately.")
										CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, TRUE)
										PRINTLN("[RCC MISSION] - screen frozen, safe, clearing ped tasks immediately so we can freeze the ped position. Call 0.")
										SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_ClearedTasksForOutOfVehicle)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Once the timer finishes then progress: this depends on how many elements were added to the screen.
				IF (HAS_NET_TIMER_STARTED(celebrationEndTimer)
				AND HAS_NET_TIMER_EXPIRED(celebrationEndTimer, celebrationEndTime)
				AND HAS_CELEBRATION_SHARD_FINISHED_FALL_AWAY()) 
				OR (IS_BIT_SET(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL) 
				AND HAS_CELEBRATION_SHARD_FINISHED_FALL_AWAY()) //TEMP FIX
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - Sending to sCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED")
					sCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED					
				ENDIF
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_FINISHED
			
			// 	Kill cinematic mode
			PRINTLN("[NETCELEBRATION] - [SAC] - [RCC MISSION] - SET_CINEMATIC_MODE_ACTIVE(FALSE) - deactivating cinematic camera at end of celebration")
			SET_CINEMATIC_MODE_ACTIVE(FALSE)
			
			IF IS_THIS_A_ROUNDS_MISSION_FOR_CELEBRATION()
			#IF IS_DEBUG_BUILD
			OR g_bFakeRound
			#ENDIF
				IF SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
				#IF IS_DEBUG_BUILD
				OR g_bFakeEndOfMatch
				#ENDIF
					
					RESET_GLOBAL_CELEBRATION_ROUNDS_DATA()
				
				ENDIF
			ENDIF
			
			IF NOT HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
				PRINTLN("[NETCELEBRATION] - [SAC] - [RCC MISSION] - team failed mission. Print 1.")
				IF IS_A_STRAND_MISSION_BEING_INITIALISED()
					PRINTLN("[NETCELEBRATION] - [SAC] - [RCC MISSION] - IS_A_STRAND_MISSION_BEING_INITIALISED() = TRUE. Print 1.")
					IF HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
						PRINTLN("[NETCELEBRATION] - [SAC] - [RCC MISSION] - HAS_FIRST_STRAND_MISSION_BEEN_PASSED() = TRUE. Print 1.")
						CLEANUP_STAND_ALONE_CELEBRATION_SCREEN_GLOBALS()
					ENDIF
				ELSE
					PRINTLN("[NETCELEBRATION] - [SAC] - [RCC MISSION] - IS_A_STRAND_MISSION_BEING_INITIALISED() = FALSE. Print 1.")
					CLEANUP_STAND_ALONE_CELEBRATION_SCREEN_GLOBALS()
				ENDIF
			ENDIF
					
			RETURN TRUE
			
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC SET_DO_MISSION_OUTRO_ANIM()

	IF OK_TO_DO_MISSION_OUTRO()
		PRINTLN("[RCC MISSION] DO_OUTROS, LBOOL8_OUTRO_OK_TO_DO ")
		SET_BIT(ilocalboolcheck8, LBOOL8_OUTRO_OK_TO_DO)
	ENDIF
ENDPROC

PROC DO_OUTROS()	
	IF NOT IS_BIT_SET(ilocalboolcheck8, LBOOL8_OUTRO_DONE)
		
		IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
			ANIM_OUTRO_PREP(jobIntroData)
			REQUEST_ANIMATIONS_FOR_INTRO(jobIntroData)

			IF IS_BIT_SET(ilocalboolcheck8, LBOOL8_OUTRO_OK_TO_DO)

				IF HAS_STARTED_NEW_OUTRO(jobIntroData)
					PRINTLN("[CS_ANIM] [RCC MISSION] DO_OUTROS, YES")
					SET_BIT(ilocalboolcheck8, LBOOL8_OUTRO_DONE)
				ENDIF
			ENDIF
		ENDIF
	ELIF IS_BIT_SET(ilocalboolcheck8, LBOOL8_OUTRO_DONE)
		CLEANUP_OUTRO_ANIM_IF_NOT_SUITABLE(jobIntroData)	
	ENDIF
ENDPROC

FUNC BOOL IS_MISSION_OVER_REASON_DONE()
	
	IF HAS_NET_TIMER_STARTED(tdovertimer)
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdovertimer) > GET_TIME_FOR_BIG_MESSAGE(BIG_MESSAGE_MISSION_OVER) 
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Early End Spectator --------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC CLEANUP_EARLY_END_AND_SPECTATE()
	PRINTLN("[NETCELEBRATION] Out of lives celebration screen has finished.")
	
	IF detachAnyPortablePickups != NULL
		CALL detachAnyPortablePickups()
	ENDIF
	
	IF NOT bIsAnySpectator
	
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			//Allows NSPC_NO_COLLISION to work
			CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
		ENDIF
		
		IF IS_ENTITY_ATTACHED(LocalPlayerPed)
			//Allows NSPC_FREEZE_POSITION to work
			DETACH_ENTITY(LocalPlayerPed)
		ENDIF
	
		NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
		
	ENDIF
	
	IF GET_PED_PARACHUTE_STATE(LocalPlayerPed) <> PPS_INVALID
		CLEAR_PED_TASKS(LocalPlayerPed)
	ENDIF
	
	INT i
	
	PRINTLN("[PLAYER_LOOP] - CLEANUP_EARLY_END_AND_SPECTATE")
	FOR i = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
		PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			PLAYER_INDEX playerIndex = NETWORK_GET_PLAYER_INDEX(tempPart)
			
			NETWORK_OVERRIDE_SEND_RESTRICTIONS(playerIndex, FALSE)
			PRINTLN("[VoiceChat] CLEANUP_EARLY_END_AND_SPECTATE - Setting All Players NETWORK_OVERRIDE_SEND_RESTRICTIONS to FALSE... Name = ", GET_PLAYER_NAME(playerIndex))
			NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS(playerIndex, FALSE)
			PRINTLN("[VoiceChat] CLEANUP_EARLY_END_AND_SPECTATE - Setting All Players NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS to FALSE... Name = ", GET_PLAYER_NAME(playerIndex))
		ENDIF
	ENDFOR
	
	//Don't clean up the screen just yet, this will be done during the spectator transition.
	PRINTLN("[NETCELEBRATION] - [RCC MISSION] - CLEANUP_CELEBRATION_SCREEN call 4.")

	g_bEndOfMissionCleanUpHUD = FALSE
	SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_EARLY_END_SPECTATOR)
	SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
	HIDE_SPECTATOR_BUTTONS(FALSE)
	HIDE_SPECTATOR_HUD(FALSE)
	
	SET_MP_DECORATOR_BIT(LocalPlayer, MP_DECORATOR_BS_INVALID_TO_SPECTATE)
	REPLAY_PREVENT_RECORDING_THIS_FRAME()
	SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM)
	SET_BIG_MESSAGE_ENABLED_WHEN_SPECTATING(TRUE)
	SET_BIT(iLocalBoolCheck3,LBOOL3_WAIT_END_SPECTATE)
	CLEAR_BIT(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
	CLEAN_UP_ALTITUDE_SYSTEMS()	
ENDPROC

FUNC BOOL SHOULD_MOVE_ON_TO_SPECTATE_AFTER_DEATH()
	
	IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdwastedtimer) > 3000
		
		PRINTLN("[RCC MISSION] SHOULD_MOVE_ON_TO_SPECTATE_AFTER_DEATH - Move on after 3 seconds of wasted timer")
		RETURN TRUE
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC MANAGE_EARLY_END_AND_SPECTATE()
	
	STRING sNull = NULL

	IF NOT HAS_NET_TIMER_STARTED(tdovertimer)

		IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
		OR IS_BIT_SET(iLocalBoolCheck14, LBOOL14_OUT_OF_BOUNDS_DEATHFAIL) // This is set when the player is failed for being out of lives, but we also kill them!
			
			IF NOT HAS_NET_TIMER_STARTED(tdwastedtimer)
				IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
					INITIALISE_CELEBRATION_SCREEN_DATA(sCelebrationData)
					
					CELEBRATION_SCREEN_TYPE eCelebType
					eCelebType = CELEBRATION_STANDARD
					IF g_FMMC_STRUCT.iCelebrationType = FMMC_CELEBRATION_TYPE_ISLAND_HEIST
						eCelebType = CELEBRATION_HEIST
					ENDIF
					PRINTLN("[Celebrations_Internal] - MANAGE_EARLY_END_AND_SPECTATE - USING CELEBRATION SCREEN: ", ENUM_TO_INT(eCelebType))
				
					REQUEST_CELEBRATION_SCREEN(sCelebrationData, eCelebType)

				ENDIF
				
				IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
					SET_MANUAL_RESPAWN_STATE(MRS_EXIT_SPECTATOR_AFTERLIFE)
					DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, DSCF_BAIL_FOR_TRANSITION)
					RESET_GLOBAL_SPECTATOR_STRUCT()
				ENDIF
				
				PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] [NETCELEBRATION] Player out of lives, starting celebration screen")
				REINIT_NET_TIMER(tdwastedtimer)
				
				FIND_PREFERRED_SPECTATOR_TARGET()
				
				SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(TRUE)
				
			ELSE
				IF SHOULD_MOVE_ON_TO_SPECTATE_AFTER_DEATH()
					IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()						
						IF HAS_CELEBRATION_SUMMARY_FINISHED(TRUE)
							PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] celebration summary finished, cleaning up" )
							
							IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
							AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
								PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] starting tdovertimer" )
								REINIT_NET_TIMER(tdovertimer)
							ENDIF
							
							
						ELSE
							PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] HAS_CELEBRATION_SUMMARY_FINISHED FALSE" )
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] strand mission cleanup" )
						IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
						AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
							PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] starting tdovertimer (strand)" )
							REINIT_NET_TIMER(tdovertimer)
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] waiting for tdwastedtimer - fading out = ",IS_SCREEN_FADING_OUT(),", fade out = ",IS_SCREEN_FADED_OUT(),"; tdwastedtimer = ",GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdwastedtimer))
				ENDIF
			ENDIF
		ELSE
			CLEANUP_KILL_STRIP()
			CLEAR_KILL_STRIP_DEATH_EFFECTS()
			CLEAR_ALL_BIG_MESSAGES()
			
			BROADCAST_FMMC_SPAWN_PROTECTION_ALPHA(FALSE, iLocalPart, GET_FRAME_COUNT())			
			
			IF HAS_LOCAL_PLAYER_FAILED()
				IF MC_playerBD[iPartToUse].iReasonForPlayerFail != PLAYER_FAIL_REASON_BOUNDS
				AND NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_MISSION_OVER,  -1, GET_REASON_FOR_PLAYER_FAIL(MC_playerBD[iPartToUse].iReasonForPlayerFail),sNull,HUD_COLOUR_WHITE)
				ENDIF
				PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] game end mission over - player fail") NET_NL()
			ELSE
				IF HAS_TEAM_FAILED(MC_playerBD[iPartToUse].iteam)
					PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] game end mission over - team failed") NET_NL()
					
					IF NOT bIsAnySpectator //Stops players getting fail shards after jipping in
						
						POPULATE_FAIL_RESULTS()
						
						IF g_sJobHeistInfo.m_failureReason != ENUM_TO_INT(mFail_OUT_OF_BOUNDS)
							
							IF NOT IS_STRING_NULL_OR_EMPTY(sCelebrationStats.strFailLiteral2)
								CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] Early end fail results - Trying to create a big message with two literal strings!")
								PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] Early end fail results - Trying to create a big message with two literal strings! strFailLiteral2 = ",sCelebrationStats.strFailLiteral2)
							ENDIF
							
							IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
								SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_MISSION_OVER, sObjEndText, sCelebrationStats.strFailLiteral, HUD_COLOUR_WHITE)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] game end mission over - results pending") 
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_playerBD[iLocalPart].tdMissionTime) > 3500
					OR GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam]) > 3500
						IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
							SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_MISSION_OVER, -1, "MC_END_PEND",sNull,HUD_COLOUR_WHITE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
				SET_MANUAL_RESPAWN_STATE(MRS_EXIT_SPECTATOR_AFTERLIFE)
				DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, DSCF_BAIL_FOR_TRANSITION)
				RESET_GLOBAL_SPECTATOR_STRUCT() 
			ENDIF
					
			FIND_PREFERRED_SPECTATOR_TARGET()
			SET_PED_DIES_IN_WATER(LocalPlayerPed, FALSE)
			
			IF detachAnyPortablePickups != NULL
				CALL detachAnyPortablePickups()
			ENDIF
			
			REINIT_NET_TIMER(tdovertimer)
			
		ENDIF
	ELSE		
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdovertimer) > 4000
			PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] Timer: Expired")			
			IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
				IF NOT IS_SCREEN_FADING_OUT()
				AND NOT IS_SCREEN_FADED_OUT()
					PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] moving to spectator (but fading out first...)" )
					DO_SCREEN_FADE_OUT(500)
				ELSE
					IF IS_SCREEN_FADED_OUT()
						PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] moving to spectator" )
						CLEANUP_EARLY_END_AND_SPECTATE()
					ENDIF
				ENDIF
			ELSE
				CLEAR_ALL_BIG_MESSAGES()
				PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] stuck before spectate - waiting for big message to stop")
			ENDIF
		ELSE			
			PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdovertimer), " / ", " 4000 Continuing to draw Celeb Screen.")
			
			IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
				DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData, FALSE)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Mission End Stage ----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_WALK_TO_MOCAP_COORDS()
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD_4.iInteractWith_BombPlantParticipant = iLocalPart
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_PERFORMING_INTERACT_WITH_CUTSCENE_LEAD_IN()
		RETURN FALSE
	ENDIF
	
	IF MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_DEFAULT
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitset4, ci_CSBS4_DisableWalkToEndMocapLocationTask)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC ADD_MISSION_JOB_POINTS_ROUNDS()
	INT iPoints
	INT iLocalPlayerJP = -1
	
	IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_ADDED_JP)
		IF IS_CORONA_READY_TO_START_WITH_JOB()
			IF IS_THIS_A_ROUNDS_MISSION()
			AND SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
			
				INT iLBDIndexWinner = GET_LEADERBOARD_INDEX_OF_TOP_PLAYER_ON_TEAM(MC_playerBD[iLocalPart].iteam)
				INT iteamwinner 	= NATIVE_TO_INT(g_MissionControllerserverBD_LB.sleaderboard[iLBDIndexWinner].playerID)
				PRINTLN("[RCC MISSION] [JOB POINT] ADD_MISSION_JOB_POINTS_ROUNDS -iteamwinner lb index=  ",iteamwinner, " iLBDIndexWinner = ", iLBDIndexWinner)
				PRINTLN("[RCC MISSION] [JOB POINT] ADD_MISSION_JOB_POINTS_ROUNDS local player: ",NATIVE_TO_INT(LocalPlayer))
					
				// Add MVP Job Point
				IF MC_serverBD.iNumActiveTeams > 1	
				AND NOT g_bOnCoopMission
					iLocalPlayerJP = NETWORK_PLAYER_ID_TO_INT()
					IF iLocalPlayerJP <> -1
						GlobalplayerBD_FM[iLocalPlayerJP].sPlaylistVars.iMVPThisJob = iteamwinner
						PRINTLN("[RCC MISSION] [JOB POINT] ADD_MISSION_JOB_POINTS_ROUNDS iLocalPlayerJP = ", iLocalPlayerJP, " iteamwinner = ", iteamwinner)
					ENDIF
				ENDIF
			
				IF DOES_TEAM_LIKE_TEAM(MC_playerBD[iPartToUse].iteam,MC_serverBD.iWinningTeam)
					iPoints = GET_POINTS_FOR_POSITION(GET_TEAM_FINISH_POSITION(MC_serverBD.iWinningTeam), iLocalPlayerJP)
				ELSE
					iPoints = GET_POINTS_FOR_POSITION(GET_TEAM_FINISH_POSITION(MC_playerBD[iLocalPart].iteam), iLocalPlayerJP)
				ENDIF
				sCelebrationStats.iLocalPlayerJobPoints = iPoints
				PRINTLN("[RCC MISSION][JOB POINT] ADD_MISSION_JOB_POINTS_ROUNDS :  ", iPoints)
				
				SET_BIT(iLocalBoolCheck7, LBOOL7_ADDED_JP)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_I_JOIN_SMALLEST_TEAM(INT iLBPosition, BOOL bAlwaysSwap = FALSE)

	BOOL bJoin = FALSE
	
	INT iSmallestTeam = FIND_SMALLEST_TEAM_FOR_SWAP_TO_SMALLEST_TEAM_MISSION()
	INT iSmallestTeamPlayers = 0
	
	PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - iSmallestTeam = ",iSmallestTeam)
	
	IF iSmallestTeam != -1
		
		iSmallestTeamPlayers = g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iSmallestTeam]
		
		PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - iSmallestTeamPlayers = ",iSmallestTeamPlayers)
		
		INT iMyPosition = -1
		INT iLBPos = 0
		
		INT i = 0
		REPEAT COUNT_OF(g_MissionControllerserverBD_LB.sleaderboard) i
			PLAYER_INDEX tempPlayer = g_MissionControllerserverBD_LB.sleaderboard[i].playerID
			
			IF tempPlayer != INVALID_PLAYER_INDEX()
			AND g_MissionControllerserverBD_LB.sleaderboard[i].iTeam != -1
				PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - Leaderboard pos ",i," contains participant ",g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant," from team ",g_MissionControllerserverBD_LB.sleaderboard[i].iTeam)
				IF g_MissionControllerserverBD_LB.sleaderboard[i].iTeam = MC_serverBD.iWinningTeam
				AND HAS_TEAM_PASSED_MISSION(g_MissionControllerserverBD_LB.sleaderboard[i].iTeam)
					PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - w winning team ",MC_serverBD.iWinningTeam)
					IF tempPlayer = LocalPlayer
						iMyPosition = iLBPos
						PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - it's me, position in winning team is ",iMyPosition)
						i = COUNT_OF(g_MissionControllerserverBD_LB.sleaderboard) //Break out!
					ELSE
						PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - it's not me, position in winning team is ",iLBPos)
						iLBPos++
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF iMyPosition != -1
			IF iMyPosition < iSmallestTeamPlayers
				PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - TRUE - My position on the winning team is high enough to join! iMyPosition (",iMyPosition,") < iSmallestTeamPlayers (",iSmallestTeamPlayers,")")
				bJoin = TRUE
			ELSE
				PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - My position on the winning team is too low: iMyPosition (",iMyPosition,") >= iSmallestTeamPlayers (",iSmallestTeamPlayers,")")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - I'm not on the winning team")
			IF iLBPos = 0 // No winning team, pick from who came top on the leaderboard
				PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - Nobody was on the winning team! (MC_serverBD.iWinningTeam = ",MC_serverBD.iWinningTeam,")")
				IF iLBPosition < iSmallestTeamPlayers
					PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - TRUE - but my position on the leaderboard is high enough anyway! iLBPosition (",iLBPosition,") < iSmallestTeamPlayers (",iSmallestTeamPlayers,")")
					bJoin = TRUE
				ELSE
					PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - My position on the winning team is too low: iLBPosition (",iLBPosition,") >= iSmallestTeamPlayers (",iSmallestTeamPlayers,")")
				ENDIF
			ELIF bAlwaysSwap AND (iSmallestTeam != GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen)
				IF iLBPosition < iSmallestTeamPlayers
					PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - TRUE - bAlwaysSwap - but my position on the leaderboard is high enough anyway! iLBPosition (",iLBPosition,") < iSmallestTeamPlayers (",iSmallestTeamPlayers,")")
					bJoin = TRUE
				ELSE
					PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - bAlwaysSwap - My position on the winning team is too low: iLBPosition (",iLBPosition,") >= iSmallestTeamPlayers (",iSmallestTeamPlayers,")")
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	IF bAlwaysSwap AND (iSmallestTeam = GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen)
		bJoin = FALSE
	ENDIF
	
	
	PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - ***** Returning ",bJoin," *****")
	
	RETURN bJoin
	
ENDFUNC

FUNC BOOL HAVE_ALL_PLAYERS_FINISHED()

INT iparticipant
PARTICIPANT_INDEX tempPart
PLAYER_INDEX tempPlayer

	PRINTLN("[PLAYER_LOOP] - HAVE_ALL_PLAYERS_FINISHED")
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
		tempPart = INT_TO_PARTICIPANTINDEX(iparticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
			
			IF NOT bIsSCTV
			AND GET_PLAYER_TEAM(tempPlayer) != -1
				IF GET_MC_CLIENT_MISSION_STAGE(iparticipant) < CLIENT_MISSION_STAGE_RESULTS
					PRINTLN("[RCC MISSION] HAVE_ALL_PLAYERS_FINISHED, returning FALSE - GET_MC_CLIENT_MISSION_STAGE < CLIENT_MISSION_STAGE_RESULTS (", GET_MC_CLIENT_MISSION_STAGE(iparticipant),") for part = ",iparticipant)
					RETURN FALSE
				ELSE
					PRINTLN("[RCC MISSION] HAVE_ALL_PLAYERS_FINISHED - GET_MC_CLIENT_MISSION_STAGE >= CLIENT_MISSION_STAGE_RESULTS (", GET_MC_CLIENT_MISSION_STAGE(iparticipant),") for part = ",iparticipant)
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] HAVE_ALL_PLAYERS_FINISHED - Part ",iparticipant," is an SCTV spectator")
			ENDIF
			
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
	
ENDFUNC

//FMMC2020 - THIS IS SPECIFICALLY END CUTSCENES  
FUNC BOOL SHOULD_PLAY_END_MOCAP()
		
	PRINTLN("[RCC MISSION] SHOULD_PLAY_END_MOCAP - [NETCELEBRATION] - MC_ServerBD.iEndCutscene: ", MC_ServerBD.iEndCutscene, " MC_serverBD.iEndCutsceneNum[ ", MC_playerBD[iPartToUse].iteam, " ]: ", MC_serverBD.iEndCutsceneNum[ MC_playerBD[iPartToUse].iteam ])
	
	#IF IS_DEBUG_BUILD
	IF MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_PLACE_HOLDER
		IF MC_serverBD.iNextMission < FMMC_MAX_STRAND_MISSIONS
		AND MC_serverBD.iNextMission >= 0
		AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23NextContentID[MC_serverBD.iNextMission])
			IF g_FMMC_STRUCT.eBranchingTransitionType[MC_serverBD.iNextMission] = FMMC_BRANCHING_MISSION_TRANSITION_TYPE_FADEOUT
			OR g_FMMC_STRUCT.eBranchingTransitionType[MC_serverBD.iNextMission] = FMMC_BRANCHING_MISSION_TRANSITION_TYPE_AIRLOCK
				PRINTLN("[RCC MISSION] SHOULD_PLAY_END_MOCAP - [NETCELEBRATION] - Returning False due to FADEOUT or AIRLOCK transition being set. MC_serverBD.iNextMission: ", MC_serverBD.iNextMission)
				RETURN FALSE
			ENDIF
		ENDIF
		
		PRINTLN("[RCC MISSION] SHOULD_PLAY_END_MOCAP - [NETCELEBRATION] - Returning True (1)")
		
		RETURN TRUE
	ENDIF
	#ENDIF
	
	FLOAT fInclusionRange = GET_CUTSCENE_PLAYER_INCLUSION_RANGE(-1, FMMCCUT_ENDMOCAP)
	VECTOR vEndCutsceneCoord = GET_END_CUTSCENE_COORDS()
		
	IF NOT IS_VECTOR_ZERO(vEndCutsceneCoord)
		IF MC_serverBD.iEndCutsceneNum[MC_playerBD[iPartToUse].iteam] != -1
		AND (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, vEndCutsceneCoord) < fInclusionRange
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_PlayEndCutsceneForAllPlayers))
			PRINTLN("[RCC MISSION] SHOULD_PLAY_END_MOCAP - [NETCELEBRATION] - Returning True (2)")
			RETURN TRUE
		ENDIF
		
		IF MC_serverBD.iEndCutsceneNum[MC_playerBD[iPartToUse].iteam] != -1
			IF bIsSCTV
				PRINTLN("[RCC MISSION] SHOULD_PLAY_END_MOCAP - [NETCELEBRATION] - Returning True (3)")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[RCC MISSION] SHOULD_PLAY_END_MOCAP - [NETCELEBRATION] - Returning False...")

	RETURN FALSE
	
ENDFUNC

PROC SELECT_ENDING_TYPE()

	INT iMyTeam = MC_playerBD[iPartToUse].iteam

	IF NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE )
	AND NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_NORMAL_END )
		IF HAS_TEAM_PASSED_MISSION( iMyTeam )
		AND HAS_TEAM_FINISHED( iMyTeam )
#IF IS_DEBUG_BUILD
		OR iPlayerPressedS != -1 AND !g_bVSMission 
#ENDIF			
			IF MISSION_HAS_VALID_MOCAP()			
				IF SHOULD_PLAY_END_MOCAP()
					IF bLocalPlayerPedOk AND NOT bIsSCTV
						IF SHOULD_WALK_TO_MOCAP_COORDS()
							VECTOR vCoords = GET_END_CUTSCENE_COORDS()
							IF NOT IS_VECTOR_ZERO(vCoords)
								TASK_GO_TO_COORD_ANY_MEANS( LocalPlayerPed, vCoords, PEDMOVEBLENDRATIO_WALK, NULL )
							ENDIF
						ENDIF
					ENDIF
					SET_BIT( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE )
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][SELECT_ENDING_TYPE] end mission mocap server number: ", MC_serverBD.iEndCutsceneNum[ iMyTeam ] )
					IF NOT IS_STRING_EMPTY( sMocapCutscene)
						IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED() AND NOT IS_THIS_IS_A_STRAND_MISSION()
						OR ( HAS_FIRST_STRAND_MISSION_BEEN_PASSED() AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED() )
							MC_SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][SELECT_ENDING_TYPE] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - H" )
						ENDIF
					ENDIF
					SET_BIT( MPGlobals.iWatchedMissionMocapBitset, WATCHED_NON_TUTORIAL_MISSION_MOCAP )
					SET_BIT( MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_REQUEST_CUTSCENE_PLAYERS )
					BROADCAST_CUTSCENE_PLAYER_REQUEST(iMyTeam, FMMCCUT_ENDMOCAP)
					SET_BIT(iLocalBoolCheck31, LBOOL31_END_CUTSCENE_STARTED)
					PRINTLN("[LM][url:bugstar:5609784][SELECT_ENDING_TYPE] - Setting PBBOOL_REQUEST_CUTSCENE_PLAYERS (4)")
				ELSE
					SET_BIT( iLocalBoolCheck2, LBOOL2_NORMAL_END )
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][SELECT_ENDING_TYPE] normal end show results - pass & should not play mocap" )
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
				AND NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC)
				AND NOT (IS_THIS_IS_A_STRAND_MISSION() AND NOT IS_THIS_THE_LAST_IN_A_SERIES_OF_STRAND_MISSIONS())
					MC_SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][SELECT_ENDING_TYPE] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - J" )
				ENDIF
				SET_BIT( iLocalBoolCheck2, LBOOL2_NORMAL_END )
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][SELECT_ENDING_TYPE] normal end show results - pass no valid mocap" )
			ENDIF
		ELSE // The team has not finished the mission
			IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
				SCRIPT_TRIGGER_MUSIC_EVENT_FAIL()
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][SELECT_ENDING_TYPE] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_FAIL) Set1" )
			ENDIF
			SET_BIT( iLocalBoolCheck2, LBOOL2_NORMAL_END )
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][SELECT_ENDING_TYPE] normal end show results - fail" )
		ENDIF
	ELSE
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][SELECT_ENDING_TYPE] normal end show results - NOT LBOOL2_ACTIVATE_MOCAP_CUTSCENE" )
	ENDIF

ENDPROC

PROC GIVE_PLAYER_AI_FLY_TASK()

	VECTOR vTarget = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS( LocalPlayerPed, << 0, 200, 100 >> )
	VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN( LocalPlayerPed )
	FLOAT fSpeed = GET_ENTITY_SPEED(LocalPlayerPed)
	//Max cruise speed is 120, having this at 120 still asserts...
	IF fSpeed > 119.0
		fSpeed = 119.0
	ENDIF
	IF IS_PED_IN_ANY_HELI( LocalPlayerPed )
		IF GET_SEAT_PED_IS_IN( LocalPlayerPed ) = VS_DRIVER //Only return true if ped is the pilot.
			TASK_HELI_MISSION( LocalPlayerPed, vehPlayer, NULL, NULL, vTarget, MISSION_GOTO, fSpeed, 1.0, 0.0, 50, 50 )
		ENDIF
	ELIF IS_PED_IN_ANY_PLANE( LocalPlayerPed )
		IF GET_SEAT_PED_IS_IN( LocalPlayerPed ) = VS_DRIVER //Only return true if ped is the pilot.
			TASK_PLANE_MISSION( LocalPlayerPed, vehPlayer, NULL, NULL, vTarget, MISSION_GOTO, fSpeed, 1.0, 0.0, 50, 50 )
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_CONTINUE_TO_MISSION_END(INT iEndDelay)

	IF HAS_NET_TIMER_STARTED(sCelebrationData.timerCelebPreLoadPostFxBeenPlaying)
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sCelebrationData.timerCelebPreLoadPostFxBeenPlaying) > ciMissionEndEffectDelay
			RETURN TRUE
		ENDIF
	ELSE
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdresultdelay) > iEndDelay
		OR IS_BIT_SET(iLocalBoolCheck2,LBOOL2_ACTIVATE_MOCAP_CUTSCENE)
			RETURN TRUE
		ENDIF
	ENDIF	

	RETURN FALSE
	
ENDFUNC

PROC MAKE_PLAYER_INVINCIBLE_ONCE_MISSION_IS_OVER()

	SET_ENTITY_INVINCIBLE( LocalPlayerPed, TRUE )
	SET_ENTITY_PROOFS( LocalPlayerPed, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE )
	
ENDPROC

//Check that there's enough players for this round
FUNC BOOL ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND()
	PRINTLN("[TS] - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND ")
	//If it's a rounds mission
	IF g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds != 0
	
		INT iTeamCount[FMMC_MAX_TEAMS], iPartLoop
		PLAYER_INDEX playerID
				
		PRINTLN("[PLAYER_LOOP] - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND")
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPartLoop
			//If they are active
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLoop))
				//Get player ID
				playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLoop))	
				IF playerID != INVALID_PLAYER_INDEX()
					INT iTeam = GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].sClientCoronaData.iTeamChosen
					//Not an array over run
					IF iTeam != -1
					AND iTeam < FMMC_MAX_TEAMS
					AND NOT IS_PLAYER_SCTV(playerID)
						//Incrament the count of that team
						iTeamCount[iTeam]++
						PRINTLN("[TS] - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND iTeamCount[", iTeam, "] = ", iTeamCount[iTeam])
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Loop the number of teams
		REPEAT g_FMMC_STRUCT.iNumberOfTeams iPartLoop
			PRINTLN("[TS] - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND iTeamCount[", iPartLoop, "] = ", iTeamCount[iPartLoop])
			PRINTLN("[TS] - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND g_FMMC_STRUCT.iNumPlayersPerTeam[", iPartLoop, "] = ", g_FMMC_STRUCT.iNumPlayersPerTeam[iPartLoop])
			//If the team count is less than the min
			IF iTeamCount[iPartLoop] < g_FMMC_STRUCT.iNumPlayersPerTeam[iPartLoop]
				PRINTLN("[TS] * - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND() - iTeamCount[", iPartLoop, "] < g_FMMC_STRUCT.iNumPlayersPerTeam[", iPartLoop, "]")
				#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] * - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND() - iTeamCount[", iPartLoop, "] < g_FMMC_STRUCT.iNumPlayersPerTeam[", iPartLoop, "]")#ENDIF
				SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_TOO_FEW_PLAYERS_FOR_NEXT_ROUND)
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_RESPAWNING_BEFORE_FADE_IN()
	
	IF GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iRespawnState < RESPAWN_STATE_LOAD_SCENE
		PRINTLN("[RCC MISSION] - IS_PLAYER_RESPAWNING_BEFORE_FADE_IN - iRespawnState < RESPAWN_STATE_LOAD_SCENE, returning TRUE")
		RETURN TRUE
	ELIF GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iRespawnState = RESPAWN_STATE_LOAD_SCENE
		IF (g_SpawnData.bIsDoingLoadScene)
			PRINTLN("[RCC MISSION] - IS_PLAYER_RESPAWNING_BEFORE_FADE_IN - iRespawnState = RESPAWN_STATE_LOAD_SCENE, and is still doing load scene")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
 
ENDFUNC

PROC CLEAN_UP_MANUAL_RESPAWN()
	IF manualRespawnState = eManualRespawnState_FADING_OUT
	OR manualRespawnState = eManualRespawnState_FADING_IN
		DO_SCREEN_FADE_IN( 100 )
		REVEAL_PLAYER_AND_VEHICLE_FOR_WARP( )
		manualRespawnState = eManualRespawnState_OKAY_TO_SPAWN
	ELIF manualRespawnState = eManualRespawnState_RESPAWNING
		DO_SCREEN_FADE_IN( 100 )
		NET_SET_PLAYER_CONTROL( LocalPlayer, TRUE )
		REVEAL_PLAYER_AND_VEHICLE_FOR_WARP( )
		manualRespawnState = eManualRespawnState_OKAY_TO_SPAWN
	ENDIF
ENDPROC
