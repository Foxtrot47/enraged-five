// ##### ------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Variations Utility -----------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc ----------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Shared Creator / Controller logic relating to the Alternate Variables System          
// ##### ------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020                                                                                     
// ##### ------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "fm_mission_controller_include_2020.sch"
	
// ##### Tuner DLC Server Bitsets - These decide what index is selected for Alternate Variable globals. They should reflect Lobby Decisions, Freemode Event Outcomes, Character Stats, etc. 
// Server Bitset: iAltVarsBS
CONST_INT ciMC_ALT_VAR_TUNER_BS_SyncedData	0

#IF FEATURE_TUNER 	

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Bunker Helper Functions
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT ciTUNER_BUNKER_LOCATION_GRAND_SENORA	0
CONST_INT ciTUNER_BUNKER_LOCATION_GRAPESEED		1
CONST_INT ciTUNER_BUNKER_LOCATION_RATON_CANYON	2
CONST_INT ciTUNER_BUNKER_LOCATION_HARMONY		3

FUNC INT GET_MC_ALT_VAR_TUNER_BUNKER_CHOSEN_FROM_PREP()
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1
		RETURN -1
	ENDIF
	
	/*IF IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iBunkerIndex, 0) 
		RETURN ciTUNER_BUNKER_LOCATION_GRAND_SENORA
	ELIF IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iBunkerIndex, 1) 
		RETURN ciTUNER_BUNKER_LOCATION_GRAPESEED
	ELIF IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iBunkerIndex, 2) 
		RETURN ciTUNER_BUNKER_LOCATION_RATON_CANYON
	ELIF IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iBunkerIndex, 3) 
		RETURN ciTUNER_BUNKER_LOCATION_HARMONY
	ENDIF*/
	
	RETURN GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iBunkerIndex
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Autoshop Helper Functions
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_MC_ALT_VAR_TUNER_AUTOSHOP_LOCATION_INDEX()
	
	SIMPLE_INTERIORS siAutoshop = GET_OWNED_AUTO_SHOP_SIMPLE_INTERIOR(GB_GET_LOCAL_PLAYER_GANG_BOSS()) 
	
	SWITCH siAutoshop
		CASE SIMPLE_INTERIOR_AUTO_SHOP_LA_MESA
			RETURN 0
		BREAK
		CASE SIMPLE_INTERIOR_AUTO_SHOP_STRAWBERRY
			RETURN 1
		BREAK
		CASE SIMPLE_INTERIOR_AUTO_SHOP_BURTON
			RETURN 2
		BREAK
		CASE SIMPLE_INTERIOR_AUTO_SHOP_RANCHO
			RETURN 3
		BREAK
		CASE SIMPLE_INTERIOR_AUTO_SHOP_MISSION_ROW
			RETURN 4
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entity Positions	Based on Autoshop																										 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_TUNER_VEH_POSITION_FORCED_BY_AUTOSHOP(INT iVeh)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAltVarsBitsetVeh, ciAltVarsBS_Tuner_Vehicles_Flag_Position_By_Autoshop)
		EXIT
	ENDIF
	
	INT iIndex = GET_MC_ALT_VAR_TUNER_AUTOSHOP_LOCATION_INDEX()
	
	IF iIndex = -1
		PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEH_POSITION_FORCED_BY_AUTOSHOP - Autoshop index is -1.")
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEH_POSITION_FORCED_BY_AUTOSHOP - Setting new position to: ", iIndex, " Vector: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings.vPosition[iIndex])
	
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vPos = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings.vPosition[iIndex]
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHead = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings.fHeading[iIndex]
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicle Drop off Based on Elevator																										 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT ciTUNER_ELEVATOR_PASS_LOCATION_0	5 //Temp Replace with freemode const/enum when it exists.
CONST_INT ciTUNER_ELEVATOR_PASS_LOCATION_1	6
CONST_INT ciTUNER_ELEVATOR_PASS_LOCATION_2	7

FUNC INT GET_MC_ALT_VAR_TUNER_ELEVATOR_DROP_OFF_CHOSEN_FROM_PREP()
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1
		RETURN -1
	ENDIF
	
	IF GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iElevatorPassBS = 0
		RETURN ciTUNER_ELEVATOR_PASS_LOCATION_0
	ELIF GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iElevatorPassBS = 1
		RETURN ciTUNER_ELEVATOR_PASS_LOCATION_1
	ELIF GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iElevatorPassBS = 2
		RETURN ciTUNER_ELEVATOR_PASS_LOCATION_2
	ENDIF
	
	RETURN -1
ENDFUNC

PROC PROCESS_MC_ALT_VAR_TUNER_VEHICLE_DROP_OFF_OVERRIDE_BY_ELEVATOR_PASS(INT iVeh)
		
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAltVarsBitsetVeh, ciAltVarsBS_Tuner_Vehicles_Flag_Dropoff_Position_From_ElevatorPass)
		EXIT
	ENDIF
	
	INT iIndex = GET_MC_ALT_VAR_TUNER_ELEVATOR_DROP_OFF_CHOSEN_FROM_PREP()
	
	IF iIndex = -1
		PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_DROP_OFF_OVERRIDE_BY_ELEVATOR_PASS - Returning FALSE due to iIndex = -1")
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_DROP_OFF_OVERRIDE_BY_ELEVATOR_PASS - Assigning Warp Location Index: ", iIndex, " Position: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings.vPosition[iIndex], " for override.")
	
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vDropOffOverride = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings.vPosition[iIndex]
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicle restart checkpoint position Based on Elevator																				 	 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_TUNER_VEHICLE_MOVE_CHECKPOINT_INDEX_BASED_ON_ELEVATOR_PASS(INT iVeh)

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAltVarsBitsetVeh, ciAltVarsBS_Tuner_Vehicles_Flag_CheckpointIndex_Position_From_ElevatorPass)
		EXIT
	ENDIF
	
	INT iIndex = GET_MC_ALT_VAR_TUNER_ELEVATOR_DROP_OFF_CHOSEN_FROM_PREP()
	
	iIndex -= ciTUNER_ELEVATOR_PASS_LOCATION_0
	
	IF iIndex <= -1
		PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_MOVE_CHECKPOINT_INDEX_BASED_ON_ELEVATOR_PASS - Returning FALSE due to iIndex = -1")
		EXIT
	ENDIF
		
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings.iCheckPointPositionIDStart = 8
	
	PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_MOVE_CHECKPOINT_INDEX_BASED_ON_ELEVATOR_PASS - Checkpoint Warp Position Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings.iCheckPointPositionIDStart, ", adding: ", iIndex)
	
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings.iCheckPointPositionIDStart += iIndex
	
	SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_DontIncrementCheckpointIndex)
		
	PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_MOVE_CHECKPOINT_INDEX_BASED_ON_ELEVATOR_PASS - Checkpoint Position will be: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings.vPosition[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings.iCheckPointPositionIDStart], ", adding: ", iIndex)	
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Clone Freemode Tanker Vehicle onto placed																								 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_MC_ALT_VAR_TUNER_TANKER_TRUCK_FROM_FREEMODE_AS_INDEX()
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1
		RETURN -1
	ENDIF
	
	RETURN GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iMethTankerBS
	
ENDFUNC

PROC PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_TANKER_TRUCK_FROM_FREEMODE(INT iVeh)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAltVarsBitsetVeh, ciAltVarsBS_Tuner_Vehicles_Flag_Clone_From_Freemode_Tanker)
		EXIT
	ENDIF
	
	INT iIndex = GET_MC_ALT_VAR_TUNER_TANKER_TRUCK_FROM_FREEMODE_AS_INDEX()
		
	IF iIndex = -1		
		PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_TANKER_TRUCK_FROM_FREEMODE - iIndex = -1. Exitting.")
		EXIT
	ENDIF
	
	MODEL_NAMES mnModel = GET_MODEL_FROM_METH_TRUCK_INT(iIndex)
	
	IF mnModel = DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_TANKER_TRUCK_FROM_FREEMODE - mnModel = DUMMY_MODEL_FOR_SCRIPT. Exitting.")
		EXIT
	ENDIF
	
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = mnModel
	
	PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_TANKER_TRUCK_FROM_FREEMODE - Assigning Tanker Index: ", iIndex, " Model: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(mnModel))
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Clone Personal Vehicle onto placed																													 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL GET_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE_IS_SPAWN_POINT_A_VALID_START_POINT(INT iSpawnpoint, INT iTeam = -1)
	
	IF iTeam = -1
		iTeam = MC_playerBD[iLocalPart].iteam
	ENDIF

	IF MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].eSpawnConditionFlag, TRUE, FALSE, 0, MC_PlayerBD[iPartToUse].iTeam)
		PRINTLN("[LM][AltVarsSystem]- iSpawnpoint: ", iSpawnpoint, " PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE - Returning FALSE due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")
		RETURN FALSE
	ENDIF
	
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iSpawnPoint, eSGET_TeamSpawnPoint, iTeam)
		PRINTLN("[LM][AltVarsSystem] - iSpawnpoint: ", iSpawnpoint, " PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE -  Returning FALSE due to MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP")
		RETURN FALSE
	ENDIF
		
	PRINTLN("[LM][AltVarsSystem - iSpawnpoint: ", iSpawnpoint, " PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE - Returning ", IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].iSpawnBitset, ci_SpawnBS_Initial_Start_Spawn_Point))
	RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].iSpawnBitset, ci_SpawnBS_Initial_Start_Spawn_Point)
ENDFUNC


PROC PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE(INT iVeh)
	
	INT iPart = -1
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAltVarsBitsetVeh, ciAltVarsBS_Tuner_Vehicles_Flag_Clone_From_Lobby_Vehicle_For_Player_0)
		iPart = 0
		PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE - Cloning for iPart: ", iPart)
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAltVarsBitsetVeh, ciAltVarsBS_Tuner_Vehicles_Flag_Clone_From_Lobby_Vehicle_For_Player_1)
		iPart = 1
		PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE - Cloning for iPart: ", iPart)
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAltVarsBitsetVeh, ciAltVarsBS_Tuner_Vehicles_Flag_Clone_From_Lobby_Vehicle_For_Player_2)
		iPart = 2
		PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE - Cloning for iPart: ", iPart)
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAltVarsBitsetVeh, ciAltVarsBS_Tuner_Vehicles_Flag_Clone_From_Lobby_Vehicle_For_Player_3)
		iPart = 3
		PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE - Cloning for iPart: ", iPart)
	ENDIF
	
	IF iPart = -1
		EXIT
	ENDIF
	
	IF NOT MC_SHOULD_VEH_SPAWN_AT_START(iVeh, FALSE)
		PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE - Vehicle is not spawning at the start. Exitting")
		EXIT
	ENDIF
	
	// Make sure we get the real part number, incase there are gaps.
	iPart = GET_ACTIVE_PARTICIPANT_INDEX_FROM_NUMBER(iPart)
	
	PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE - Actual Participant Number: ", iPart)
	
	IF iPart = -1
		EXIT
	ENDIF
	
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = MC_playerBD[iPart].mnMyRaceModel	
	
	PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE - Overriding Model Name: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(MC_playerBD[iPart].mnMyRaceModel))
	
	IF IS_PLAYER_SELECTING_CUSTOM_VEHICLE(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
		
		PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE - Selecting Custom Vehicle - Setting iVehCloneFromLobbyBS")
		
		SET_BIT(iVehCloneFromLobbyBS, iVeh)
		
		IF iPart = iLocalPart
			PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE - Setting iVehicleLocalPartNeedsToCloneBS, iLocalPart: ", iLocalPart, " Setting iVehSlot: ", MC_playerBD[iPart].iMyRaceModelChoice, " Caching iVehSlot: ", iPersonalVehicleSlotCache)
			SET_BIT(iVehicleLocalPartNeedsToCloneBS, iVeh)
			SET_LAST_USED_VEHICLE_SLOT(MC_playerBD[iPart].iMyRaceModelChoice)			
		ENDIF
		
	ELSE
		PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE - Stock vehicle chosen - Not Setting iVehCloneFromLobbyBS.")
								
		IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = iPart
			PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE - Using Sessanta's Mod Preset, as we are gang boss, iPart: ", iPart)
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iModPreset = 0
		ELSE
			PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE - Using  Grey look-a-like Mod Preset, as we are not the gang boss, iPart: ", iPart)
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iModPreset = 1
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE_OLD(INT iVeh, INT &iPartsAssigned)

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAltVarsBitsetVeh, ciAltVarsBS_Tuner_Vehicles_Flag_Clone_From_Lobby_Vehicle)
		EXIT
	ENDIF	
	
	IF NOT MC_SHOULD_VEH_SPAWN_AT_START(iVeh, FALSE)
		PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE_OLD - Vehicle is not spawning at the start. Exitting")
		EXIT
	ENDIF
	
	INT iPartToAssign = GET_ACTIVE_PARTICIPANT_INDEX_FROM_NUMBER(iPartsAssigned)
	INT i
	INT iValidCount
	
	IF iPartToAssign = -1
		PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE_OLD - iParticipant: ", iPartToAssign, " No more participants to assign to this vehicle.")
		EXIT
	ENDIF
	
	FOR i = 0 TO FMMC_MAX_TEAMSPAWNPOINTS-1
		IF MC_ServerBD_4.iCurrentHighestPriority[0] < FMMC_MAX_RULES
			
			IF GET_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE_IS_SPAWN_POINT_A_VALID_START_POINT(i, 0)
								
				IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][i].iVehicle = -1
					PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - iParticipant: ", iPartToAssign, " - iSpawnpoint: ", i, " Spawn Point has no Vehicle Assigned. Reloop.")
					RELOOP
				ENDIF
				
				// Found participants spawn point.
				IF iValidCount = iPartsAssigned
					
					IF MC_playerBD[iPartToAssign].mnMyRaceModel = DUMMY_MODEL_FOR_SCRIPT
						PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - iPartToAssign: ", iPartToAssign, " - iSpawnpoint: ", i, " PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE_OLD - Vehicle Matches Exitting as g_mnMyRaceModel = DUMMY_MODEL_FOR_SCRIPT.")
						RELOOP
					ENDIF
					
					PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - iPartToAssign: ", iPartToAssign, " - iSpawnpoint: ", i, " PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE_OLD - Vehicle Matches Assigning new model name: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(MC_playerBD[iPartToAssign].mnMyRaceModel))
					g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = MC_playerBD[iPartToAssign].mnMyRaceModel
					g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][i].iVehicle = iVeh
					
					IF IS_PLAYER_SELECTING_CUSTOM_VEHICLE(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToAssign)))
						PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE_OLD - Setting iVehCloneFromLobbyBS")
						IF iPartToAssign = iLocalPart
							SET_BIT(iVehicleLocalPartNeedsToCloneBS, iVeh)
							iPersonalVehicleSlotCache = CURRENT_SAVED_VEHICLE_SLOT()
							SET_LAST_USED_VEHICLE_SLOT(MC_playerBD[iPartToAssign].iMyRaceModelChoice)
							PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE_OLD - Setting iVehicleLocalPartNeedsToCloneBS, iLocalPart: ", iLocalPart, " Setting iVehSlot: ", MC_playerBD[iPartToAssign].iMyRaceModelChoice, " Caching iVehSlot: ", iPersonalVehicleSlotCache)							
							
							IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAltVarsBitsetVeh, ciAltVarsBS_Tuner_Vehicles_Flag_Set_Up_Spawn_Points_For_Placed_Or_PV)
								PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE_OLD - Setting Spawn points to act for Personal Vehicle")
								g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][i].iVehicle = -1
								SET_BIT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][i].iSpawnBitSet, ci_SpawnBS_SpawnInPersonalVehicleAtStart)
								SET_BIT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][i].iSpawnBitSet, ci_SpawnBS_AllowPersonalVehicleToSpawnAtStart)
								SET_BIT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][i].iSpawnBitSet, ci_SpawnBS_EnablePersonalVehiclesForMission)
								SET_BIT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][i].iSpawnBitSet, ci_SpawnBS_RepositionPlayerPostVehicle)
								CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][i].iSpawnBitSet, ci_SpawnBS_ReEnablePersonalVehiclesIfLostStartingVehicle)
							ENDIF
						ENDIF
						SET_BIT(iVehCloneFromLobbyBS, iVeh)
					ELSE
						PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE_OLD - Not Setting iVehCloneFromLobbyBS. Stock vehicle chosen.")
						
						IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = iPartToAssign
							PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE_OLD - Using Sessanta's Mod Preset, as we are gang boss, iPart: ", iPartToAssign)
							g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iModPreset = 0
						ELSE
							PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE_OLD - Using  Grey look-a-like Mod Preset, as we are not the gang boss, iPart: ", iPartToAssign)
							g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iModPreset = 1
						ENDIF
						
						IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAltVarsBitsetVeh, ciAltVarsBS_Tuner_Vehicles_Flag_Set_Up_Spawn_Points_For_Placed_Or_PV)
							PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE_OLD - Setting Spawn points to act for Placed Vehicle")
							SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_PERSONAL_VEH)
							CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][i].iSpawnBitSet, ci_SpawnBS_SpawnInPersonalVehicleAtStart)
							CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][i].iSpawnBitSet, ci_SpawnBS_AllowPersonalVehicleToSpawnAtStart)
							CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][i].iSpawnBitSet, ci_SpawnBS_EnablePersonalVehiclesForMission)
							CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][i].iSpawnBitSet, ci_SpawnBS_RepositionPlayerPostVehicle)
							SET_BIT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][i].iSpawnBitSet, ci_SpawnBS_ReEnablePersonalVehiclesIfLostStartingVehicle)
						ENDIF
					ENDIF
					
					iPartsAssigned ++
					
					BREAKLOOP
					
				ELSE
					iValidCount++
					PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - iPartToAssign: ", iPartToAssign, " - iSpawnpoint: ", i, " PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE_OLD - not valid, incrementing validCount to: ", iValidCount, " iPlayerTeamSlot: ", iPartsAssigned)
				ENDIF
				
			ELSE
				PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - iPartToAssign: ", iPartToAssign, " - iSpawnpoint: ", i, " PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE_OLD - Team spawn point ", i, " isn't valid")
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Player Ability Vehicles																										 			-----------------------
// ##### Description: Entities involved in Player Abilities require a bit of randomisation (model names, mods, equipped weapons etc			 					-----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_TUNER_VEHICLE_PLAYER_ABILITY_RANDOMISATION(INT iVeh)
		
	IF NOT IS_ENTITY_USING_THIS_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].eSpawnConditionFlag, SPAWN_CONDITION_FLAG_RIDE_ALONG_ABILITY_ACTIVATED)
	AND NOT IS_ENTITY_USING_THIS_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].eSpawnConditionFlag, SPAWN_CONDITION_FLAG_AMBUSH_ABILITY_ACTIVATED)
	AND NOT IS_ENTITY_USING_THIS_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].eSpawnConditionFlag, SPAWN_CONDITION_FLAG_DISTRACTION_ABILITY_ACTIVATED)
		EXIT
	ENDIF
	
	INT iMinPreset = 0
	INT iMaxPreset = 6
	
	IF IS_ENTITY_USING_THIS_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].eSpawnConditionFlag, SPAWN_CONDITION_FLAG_AMBUSH_ABILITY_ACTIVATED)
		// This vehicle needs to have an open boot! The banshee doesn't have a boot that opens, so we can't choose one of those.
		iMinPreset = 3
		SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet, ENUM_TO_INT(SC_DOOR_BOOT))
		SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_KEEP_DOORS_OPEN)
		CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_DOOR_TRUNC_LOCKED)
	ENDIF
	
	INT iRandomPreset = GET_RANDOM_INT_IN_RANGE(iMinPreset, iMaxPreset)
	
	PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_PLAYER_ABILITY_RANDOMISATION - Assigning Random Preset ", iRandomPreset, " || iMinPreset: ", iMinPreset, " / iMaxPreset: ", iMaxPreset)
	
	SWITCH iRandomPreset
		// Banshee
		CASE 0
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = BANSHEE2
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iModPreset = 1
		BREAK
		CASE 1
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = BANSHEE2
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iModPreset = 2
		BREAK
		CASE 2
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = BANSHEE2
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iModPreset = 3
		BREAK
		
		// Sultan
		CASE 3
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = SULTANRS
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iModPreset = 1
		BREAK
		CASE 4
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = SULTANRS
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iModPreset = 2
		BREAK
		CASE 5
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = SULTANRS
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iModPreset = 3
		BREAK
		
	ENDSWITCH
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Player Ability Peds																										 				------------------------
// ##### Description: Entities involved in Player Abilities require a bit of randomisation (model names, mods, equipped weapons etc			 					------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_TUNER_PED_PLAYER_ABILITY_RANDOMISATION(INT iPed)
		
	IF NOT IS_ENTITY_USING_THIS_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].eSpawnConditionFlag, SPAWN_CONDITION_FLAG_RIDE_ALONG_ABILITY_ACTIVATED)
	AND NOT IS_ENTITY_USING_THIS_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].eSpawnConditionFlag, SPAWN_CONDITION_FLAG_AMBUSH_ABILITY_ACTIVATED)
	AND NOT IS_ENTITY_USING_THIS_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].eSpawnConditionFlag, SPAWN_CONDITION_FLAG_DISTRACTION_ABILITY_ACTIVATED)
		EXIT
	ENDIF
	
	INT iRandomPreset = GET_RANDOM_INT_IN_RANGE(0, 10)	
		
	SWITCH iRandomPreset
		// Female
		CASE 0
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].mn = INT_TO_ENUM(MODEL_NAMES, HASH("g_f_importexport_01"))
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iModelVariation = 1
		BREAK
		CASE 1
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].mn = INT_TO_ENUM(MODEL_NAMES, HASH("g_f_importexport_01"))
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iModelVariation = 2
		BREAK
		CASE 2
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].mn = INT_TO_ENUM(MODEL_NAMES, HASH("g_f_importexport_01"))
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iModelVariation = 3
		BREAK
		CASE 3
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].mn = INT_TO_ENUM(MODEL_NAMES, HASH("g_f_importexport_01"))
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iModelVariation = 4
		BREAK
		CASE 4
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].mn = INT_TO_ENUM(MODEL_NAMES, HASH("g_f_importexport_01"))
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iModelVariation = 5
		BREAK
		
		// Male
		CASE 5
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].mn = INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iModelVariation = 1
		BREAK
		CASE 6
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].mn = INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iModelVariation = 2
		BREAK
		CASE 7
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].mn = INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iModelVariation = 3
		BREAK
		CASE 8
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].mn = INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iModelVariation = 4
		BREAK
		CASE 9
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].mn = INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iModelVariation = 5
		BREAK
	ENDSWITCH
	
	PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "] - PROCESS_MC_ALT_VAR_TUNER_PED_PLAYER_ABILITY_RANDOMISATION - Assigning Random Preset ", iRandomPreset, " || ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].mn), " / Variation: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iModelVariation)
	
	INT iRandomWeaponIndex = GET_RANDOM_INT_IN_RANGE(0, 3)
	SWITCH iRandomWeaponIndex
		CASE 0
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].gun = WEAPONTYPE_PISTOL
		BREAK
		CASE 1
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].gun = WEAPONTYPE_MICROSMG
		BREAK
		CASE 2
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].gun = WEAPONTYPE_DLC_MINISMG
		BREAK
	ENDSWITCH
	
	PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "] - PROCESS_MC_ALT_VAR_TUNER_PED_PLAYER_ABILITY_RANDOMISATION - Giving ped random weapon || iRandomWeaponIndex: ", iRandomWeaponIndex)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Train Easter Egg																										 							----------------
// ##### Description: If a particular Spawn Group/Subspawn Group is active, we need to swap some train settings around to match the different props that are spawned.	----------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC PROCESS_MC_ALT_VAR_TUNER_TRAIN_EASTER_EGG(INT iTrain)
	IF IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, 2)
	AND IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[2], 9)
		CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iPlacedTrain_CarriageExtrasToUseBS[4], 3)
		SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iPlacedTrain_CarriageExtrasToUseBS[4], 1)
		
		PRINTLN("[AltVarsSystem][Train ", iTrain, "] - PROCESS_MC_ALT_VAR_TUNER_TRAIN_EASTER_EGG - Easter Egg is active! Swapped train settings to match")
	ELSE
		PRINTLN("[AltVarsSystem][Train ", iTrain, "] - PROCESS_MC_ALT_VAR_TUNER_TRAIN_EASTER_EGG - Easter Egg isn't active this time")
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Spawn Groups																															 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_MC_ALT_VAR_TUNER_DEFINED_SPAWN_GROUP_SET(SPAWN_GROUP_DEFINE_ACTIVATION eSpawnGroupActivationType)
	
	SWITCH eSpawnGroupActivationType
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_TUNER_ROBBERY_BUNKER_GRAND_SENORA_DESERT
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_TUNER_ROBBERY_BUNKER_GRAPESEED
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_TUNER_ROBBERY_BUNKER_RATON_CANYON
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_TUNER_ROBBERY_BUNKER_HARMONY
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_TUNER_ROBBERY_ELEVATOR_PASS_0
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_TUNER_ROBBERY_ELEVATOR_PASS_1
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_TUNER_ROBBERY_ELEVATOR_PASS_2
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DOES_MC_ALT_VAR_TUNER_DEFINED_SPAWN_GROUP_MATCH_WITH_CHOSEN(SPAWN_GROUP_DEFINE_ACTIVATION eSpawnGroupActivationType)
		
	SWITCH eSpawnGroupActivationType
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_TUNER_ROBBERY_BUNKER_GRAND_SENORA_DESERT
			RETURN GET_MC_ALT_VAR_TUNER_BUNKER_CHOSEN_FROM_PREP() = ciTUNER_BUNKER_LOCATION_GRAND_SENORA
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_TUNER_ROBBERY_BUNKER_GRAPESEED
			RETURN GET_MC_ALT_VAR_TUNER_BUNKER_CHOSEN_FROM_PREP() = ciTUNER_BUNKER_LOCATION_GRAPESEED
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_TUNER_ROBBERY_BUNKER_RATON_CANYON
			RETURN GET_MC_ALT_VAR_TUNER_BUNKER_CHOSEN_FROM_PREP() = ciTUNER_BUNKER_LOCATION_RATON_CANYON
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_TUNER_ROBBERY_BUNKER_HARMONY
			RETURN GET_MC_ALT_VAR_TUNER_BUNKER_CHOSEN_FROM_PREP() = ciTUNER_BUNKER_LOCATION_HARMONY
		BREAK		
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_TUNER_ROBBERY_ELEVATOR_PASS_0
			RETURN GET_MC_ALT_VAR_TUNER_ELEVATOR_DROP_OFF_CHOSEN_FROM_PREP() = ciTUNER_ELEVATOR_PASS_LOCATION_0
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_TUNER_ROBBERY_ELEVATOR_PASS_1
			RETURN GET_MC_ALT_VAR_TUNER_ELEVATOR_DROP_OFF_CHOSEN_FROM_PREP() = ciTUNER_ELEVATOR_PASS_LOCATION_1
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_TUNER_ROBBERY_ELEVATOR_PASS_2
			RETURN GET_MC_ALT_VAR_TUNER_ELEVATOR_DROP_OFF_CHOSEN_FROM_PREP() = ciTUNER_ELEVATOR_PASS_LOCATION_2
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_MC_ALT_VAR_TUNER_SUB_SPAWN_GROUP_ACTIVATION(INT iSpawnGroup, INT iSubSpawnGroup)	
	
	IF NOT IS_MC_ALT_VAR_TUNER_DEFINED_SPAWN_GROUP_SET(g_FMMC_STRUCT.eRandomEntitySpawnSub_DefineActivation[iSpawnGroup][iSubSpawnGroup])
		EXIT
	ENDIF
	
	IF NOT DOES_MC_ALT_VAR_TUNER_DEFINED_SPAWN_GROUP_MATCH_WITH_CHOSEN(g_FMMC_STRUCT.eRandomEntitySpawnSub_DefineActivation[iSpawnGroup][iSubSpawnGroup])
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][SpawnGroup: ", iSpawnGroup, "][SubSpawnGroup: ", iSubSpawnGroup, "][DefinedAction: ", GET_SPAWN_GROUP_DEFINE_ACTIVATION_NAME_FOR_PRINT(g_FMMC_STRUCT.eRandomEntitySpawnSub_DefineActivation[iSpawnGroup][iSubSpawnGroup]), "] - PROCESS_MC_ALT_VAR_TUNER_SUB_SPAWN_GROUP_ACTIVATION - Setting Sub Spawn Group to SPAWN_GROUP_DEFINE_ACTIVATION_FORCED_ON")
	
	g_FMMC_STRUCT.eRandomEntitySpawnSub_DefineActivation[iSpawnGroup][iSubSpawnGroup] = SPAWN_GROUP_DEFINE_ACTIVATION_FORCED_ON
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_TUNER_SPAWN_GROUP_ACTIVATION(INT iSpawnGroup)	
	
	IF NOT IS_MC_ALT_VAR_TUNER_DEFINED_SPAWN_GROUP_SET(g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[iSpawnGroup])
		EXIT
	ENDIF
	
	IF NOT DOES_MC_ALT_VAR_TUNER_DEFINED_SPAWN_GROUP_MATCH_WITH_CHOSEN(g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[iSpawnGroup])
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][SpawnGroup: ", iSpawnGroup, "][DefinedAction: ", GET_SPAWN_GROUP_DEFINE_ACTIVATION_NAME_FOR_PRINT(g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[iSpawnGroup]), "] - PROCESS_MC_ALT_VAR_TUNER_SPAWN_GROUP_ACTIVATION - Setting Spawn Group to SPAWN_GROUP_DEFINE_ACTIVATION_FORCED_ON")
	
	g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[iSpawnGroup] = SPAWN_GROUP_DEFINE_ACTIVATION_FORCED_ON
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Spawn Block Flags																													 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_MC_ALT_VAR_TUNER_SPAWN_BLOCKING_FLAG_SET(SPAWN_CONDITION_FLAG eSpawnConditionFlag)
	
	SWITCH eSpawnConditionFlag
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_A
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_B
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_C
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_D
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_E
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_F
		CASE SPAWN_CONDITION_FLAG_NOTJAMMEDFLEECABANK_A
		CASE SPAWN_CONDITION_FLAG_NOTJAMMEDFLEECABANK_B
		CASE SPAWN_CONDITION_FLAG_NOTJAMMEDFLEECABANK_C
		CASE SPAWN_CONDITION_FLAG_NOTJAMMEDFLEECABANK_D
		CASE SPAWN_CONDITION_FLAG_NOTJAMMEDFLEECABANK_E
		CASE SPAWN_CONDITION_FLAG_NOTJAMMEDFLEECABANK_F
		CASE SPAWN_CONDITION_FLAG_BLOCK_SPAWN_IF_JAMMEDFLEECABANK_ALL	
		CASE SPAWN_CONDITION_FLAG_BLOCK_SPAWN_IF_NOTJAMMEDFLEECABANK_ALL
		CASE SPAWN_CONDITION_FLAG_TRUCK_WEAPONS
		CASE SPAWN_CONDITION_FLAG_TRUCK_INSURGENT
		CASE SPAWN_CONDITION_FLAG_TRUCK_AA_TRAILER	
		CASE SPAWN_CONDITION_FLAG_NOT_TRUCK_WEAPONS
		CASE SPAWN_CONDITION_FLAG_NOT_TRUCK_INSURGENT
		CASE SPAWN_CONDITION_FLAG_NOT_TRUCK_AA_TRAILER	
		CASE SPAWN_CONDITION_FLAG_METH_TANKER_PHANTOM
		CASE SPAWN_CONDITION_FLAG_METH_TANKER_HAULER
		CASE SPAWN_CONDITION_FLAG_METH_TANKER_PHANTOM_WEDGE		
		CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_LA_MESA 	
		CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_STRAWBERRY 
		CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_BURTON 	
		CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_RANCHO 	
		CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_MISSION_ROW
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_ANY	
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DOES_MC_ALT_VAR_TUNER_FLAGGED_MATCH_WITH_CHOSEN(SPAWN_CONDITION_FLAG eSpawnConditionFlag)
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1	
		PRINTLN("[LM][AltVarsSystem] - DOES_MC_ALT_VAR_TUNER_FLAGGED_MATCH_WITH_CHOSEN - GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1 - RETURNING FALSE")
		RETURN FALSE
	ENDIF
		
	SWITCH eSpawnConditionFlag
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_A 		
			RETURN IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_A))
		BREAK
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_B 		
			RETURN IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_B))
		BREAK
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_C		
			RETURN IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_C))
		BREAK
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_D		
			RETURN IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_D))
		BREAK
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_E		
			RETURN IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_E))
		BREAK
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_F		
			RETURN IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_F))
		BREAK
		CASE SPAWN_CONDITION_FLAG_NOTJAMMEDFLEECABANK_A 		
			RETURN NOT IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_A))
		BREAK
		CASE SPAWN_CONDITION_FLAG_NOTJAMMEDFLEECABANK_B 		
			RETURN NOT IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_B))
		BREAK
		CASE SPAWN_CONDITION_FLAG_NOTJAMMEDFLEECABANK_C		
			RETURN NOT IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_C))
		BREAK
		CASE SPAWN_CONDITION_FLAG_NOTJAMMEDFLEECABANK_D		
			RETURN NOT IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_D))
		BREAK
		CASE SPAWN_CONDITION_FLAG_NOTJAMMEDFLEECABANK_E		
			RETURN NOT IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_E))
		BREAK
		CASE SPAWN_CONDITION_FLAG_NOTJAMMEDFLEECABANK_F		
			RETURN NOT IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_F))
		BREAK
		CASE SPAWN_CONDITION_FLAG_BLOCK_SPAWN_IF_JAMMEDFLEECABANK_ALL	
			IF IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_A))
			AND IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_B))
			AND IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_C))
			AND IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_D))
			AND IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_E))
			AND IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_F))
				RETURN FALSE
			ELSE
				RETURN TRUE
			ENDIF
		BREAK
		CASE SPAWN_CONDITION_FLAG_BLOCK_SPAWN_IF_NOTJAMMEDFLEECABANK_ALL
			IF NOT IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_A))
			OR NOT IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_B))
			OR NOT IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_C))
			OR NOT IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_D))
			OR NOT IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_E))
			OR NOT IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_F))
				RETURN FALSE
			ELSE
				RETURN TRUE
			ENDIF
		BREAK
		CASE SPAWN_CONDITION_FLAG_TRUCK_WEAPONS
			RETURN IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iBunkerTruckBS, 1) // url:bugstar:7021436 | Value grabbed from info in this bug.
		BREAK
		CASE SPAWN_CONDITION_FLAG_TRUCK_INSURGENT
			RETURN IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iBunkerTruckBS, 2) // url:bugstar:7021436 | Value grabbed from info in this bug.
		BREAK
		CASE SPAWN_CONDITION_FLAG_TRUCK_AA_TRAILER
			RETURN IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iBunkerTruckBS, 0) // url:bugstar:7021436 | Value grabbed from info in this bug.
		BREAK
		CASE SPAWN_CONDITION_FLAG_NOT_TRUCK_WEAPONS
			RETURN NOT IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iBunkerTruckBS, 1) // url:bugstar:7021436 | Value grabbed from info in this bug.
		BREAK
		CASE SPAWN_CONDITION_FLAG_NOT_TRUCK_INSURGENT
			RETURN NOT IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iBunkerTruckBS, 2) // url:bugstar:7021436 | Value grabbed from info in this bug.
		BREAK
		CASE SPAWN_CONDITION_FLAG_NOT_TRUCK_AA_TRAILER
			RETURN NOT IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iBunkerTruckBS, 0) // url:bugstar:7021436 | Value grabbed from info in this bug.
		BREAK
		CASE SPAWN_CONDITION_FLAG_METH_TANKER_PHANTOM
			RETURN IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iMethTankerBS, 0) 
		BREAK
		CASE SPAWN_CONDITION_FLAG_METH_TANKER_HAULER
			RETURN IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iMethTankerBS, 1) 
		BREAK
		CASE SPAWN_CONDITION_FLAG_METH_TANKER_PHANTOM_WEDGE
			RETURN IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iMethTankerBS, 2) 
		BREAK		
		CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_LA_MESA 	
			RETURN (GET_MC_ALT_VAR_TUNER_AUTOSHOP_LOCATION_INDEX() = 0)
		BREAK
		CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_STRAWBERRY 
			RETURN (GET_MC_ALT_VAR_TUNER_AUTOSHOP_LOCATION_INDEX() = 1)
		BREAK
		CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_BURTON 	
			RETURN (GET_MC_ALT_VAR_TUNER_AUTOSHOP_LOCATION_INDEX() = 2)
		BREAK
		CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_RANCHO 	
			RETURN (GET_MC_ALT_VAR_TUNER_AUTOSHOP_LOCATION_INDEX() = 3)
		BREAK
		CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_MISSION_ROW
			RETURN (GET_MC_ALT_VAR_TUNER_AUTOSHOP_LOCATION_INDEX() = 4)
		BREAK		
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_ANY
			IF IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_A))
			OR IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_B))
			OR IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_C))
			OR IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_D))
			OR IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_E))
			OR IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_F))
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		BREAK	
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_MC_ALT_VAR_TUNER_PED_SPAWN_BLOCKING_FLAG_CHECKS(INT iPed)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1	
		IF NOT IS_MC_ALT_VAR_TUNER_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_TUNER_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "][Flag: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].eSpawnConditionFlag[iFlag]), "] - PROCESS_MC_ALT_VAR_TUNER_PED_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET	
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_TUNER_VEHICLE_SPAWN_BLOCKING_FLAG_CHECKS(INT iVeh)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
			
		IF NOT IS_MC_ALT_VAR_TUNER_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_TUNER_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "][iFlag: ", iFlag, "] - PROCESS_MC_ALT_VAR_TUNER_VEHICLE_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_TUNER_OBJECT_SPAWN_BLOCKING_FLAG_CHECKS(INT iObj)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1	
		IF NOT IS_MC_ALT_VAR_TUNER_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_TUNER_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][Object: ", iObj, "][iFlag: ", iFlag, "] - PROCESS_MC_ALT_VAR_TUNER_OBJECT_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_TUNER_INTERACTABLE_SPAWN_BLOCKING_FLAG_CHECKS(INT iInteractable)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1		
		IF NOT IS_MC_ALT_VAR_TUNER_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_TUNER_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "][iFlag: ", iFlag, "] - PROCESS_MC_ALT_VAR_TUNER_INTERACTABLE_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_TUNER_PROP_SPAWN_BLOCKING_FLAG_CHECKS(INT iProp)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1		
		IF NOT IS_MC_ALT_VAR_TUNER_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_TUNER_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][Prop: ", iProp, "][iFlag: ", iFlag, "] - PROCESS_MC_ALT_VAR_TUNER_PROP_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_TUNER_DYNO_PROP_SPAWN_BLOCKING_FLAG_CHECKS(INT iDynoProp)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1		
		IF NOT IS_MC_ALT_VAR_TUNER_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_TUNER_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][DynoProp: ", iDynoProp, "][iFlag: ", iFlag, "] - PROCESS_MC_ALT_VAR_TUNER_DYNO_PROP_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_TUNER_TEAM_SPAWN_POINT_SPAWN_BLOCKING_FLAG_CHECKS(INT iSpawnPoint, INT iTeam)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1	
		IF NOT IS_MC_ALT_VAR_TUNER_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_TUNER_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		PRINTLN("[LM][AltVarsSystem][SpawnPoint: ", iSpawnPoint, "][iTeam: ", iTeam, "][iFlag: ", iFlag, "] - PROCESS_MC_ALT_VAR_TUNER_TEAM_SPAWN_POINT_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_TUNER_ZONE_SPAWN_BLOCKING_FLAG_CHECKS(INT iZone)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1	
		IF NOT IS_MC_ALT_VAR_TUNER_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_TUNER_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][Zone: ", iZone, "][Flag: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].eSpawnConditionFlag[iFlag]), "] - PROCESS_MC_ALT_VAR_TUNER_ZONE_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET	
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_TUNER_DIALOGUE_TRIGGERS_SPAWN_BLOCKING_FLAG_CHECKS(INT iDialogueTrigger)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1	
		IF NOT IS_MC_ALT_VAR_TUNER_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_TUNER_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][DialogueTrigger: ", iDialogueTrigger, "][Flag: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].eSpawnConditionFlag[iFlag]), "] - PROCESS_MC_ALT_VAR_TUNER_DIALOGUE_TRIGGERS_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET	
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_TUNER_WEAPON_SPAWN_BLOCKING_FLAG_CHECKS(INT iWeapon)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1	
		IF NOT IS_MC_ALT_VAR_TUNER_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_TUNER_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][Weapon: ", iWeapon, "][Flag: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].eSpawnConditionFlag[iFlag]), "] - PROCESS_MC_ALT_VAR_TUNER_WEAPON_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET	
	ENDFOR
ENDPROC



// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cutscene Overrides																														 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_MC_ALT_VAR_TUNER_BUNKER_INDEX_CHOSEN_FOR_CUTSCENE()
	SWITCH GET_MC_ALT_VAR_TUNER_BUNKER_CHOSEN_FROM_PREP()
		CASE ciTUNER_BUNKER_LOCATION_GRAND_SENORA
			RETURN ciARRAY_INT_ALT_VAR_TUNER_BUNKER_GRAND_SENORA_DESERT_CUTSCENE_INDEX
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_GRAPESEED
			RETURN ciARRAY_INT_ALT_VAR_TUNER_BUNKER_GRAPESEED_CUTSCENE_INDEX
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_RATON_CANYON
			RETURN ciARRAY_INT_ALT_VAR_TUNER_BUNKER_RATON_CANYON_CUTSCENE_INDEX
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_HARMONY
			RETURN ciARRAY_INT_ALT_VAR_TUNER_BUNKER_HARMONY_CUTSCENE_INDEX
		BREAK
	ENDSWITCH
	RETURN -1
ENDFUNC

PROC PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_CUTSCENE_INDEX_WITH_BUNKER_LOCATION(INT iTeam, INT iRule)

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_Tuner_Rule_Flag_Use_Bunker_Location_For_Cutscene_Index)
		EXIT
	ENDIF
	
	INT iIndex = GET_MC_ALT_VAR_TUNER_BUNKER_INDEX_CHOSEN_FOR_CUTSCENE()
	
	IF iIndex = -1
		PRINTLN("[LM][AltVarsSystem][Cutscene][Rule: ", iRule, "] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_CUTSCENE_INDEX_WITH_BUNKER_LOCATION - Not overriding. Bunker iIndex: ", iIndex)
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Cutscene][Rule: ", iRule, "] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_CUTSCENE_INDEX_WITH_BUNKER_LOCATION - Bunker iIndex: ", iIndex)
	
	INT iMod = g_FMMC_STRUCT.sPoolVars.iInt[iIndex]
	
	IF iMod = -1
		PRINTLN("[LM][AltVarsSystem][Cutscene][Rule: ", iRule, "] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_CUTSCENE_INDEX_WITH_BUNKER_LOCATION - Not overriding. Cutscene is iMod: ", iMod)
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Cutscene][Rule: ", iRule, "] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_CUTSCENE_INDEX_WITH_BUNKER_LOCATION - Overriding. Cutscene is iMod: ", iMod)
	
	INT iPlayerRule = 0
	FOR iPlayerRule = 0 TO FMMC_MAX_RULES-1
		
		PRINTLN("Spam - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_CUTSCENE_INDEX_WITH_BUNKER_LOCATION - iRule: ", iRule, " iPlayerRule: ", iPlayerRule, " MC_serverBD_4.iPlayerRulePriority: ", MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam])
	
		IF MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam] = iRule
			iRule = iPlayerRule
			PRINTLN("Spam - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_CUTSCENE_INDEX_WITH_BUNKER_LOCATION - BREAKING LOOP")
			BREAKLOOP
		ENDIF
		
	ENDFOR
	
	// The Cutscene Index to play.
 	g_FMMC_STRUCT.sPlayerRuleData[iRule].iPlayerRuleLimit[iteam] = iMod
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Player Abilities																																 ---------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_PLAYER_ABILITY_REPAIR(INT iTeam, INT iRule)

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_Tuner_Rule_Flag_Player_Ability_Repair_Req_Lvl_10_Car_Club)
		EXIT
	ENDIF
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1	
		PRINTLN("[LM][AltVarsSystem][Team: ", iTeam, "][Rule: ", iRule, "] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_PLAYER_ABILITY_REPAIR - GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1 EXITTING...")
		EXIT
	ENDIF
	
	IF HAS_PLAYER_UNLOCKED_ABILITY_REPAIR_WITH_CAR_CLUB_REP_TIER(GB_GET_LOCAL_PLAYER_GANG_BOSS())
		PRINTLN("[LM][AltVarsSystem][Team: ", iTeam, "][Rule: ", iRule, "] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_PLAYER_ABILITY_REPAIR - Player has unlocked the Repair Ability. ")
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Team: ", iTeam, "][Rule: ", iRule, "] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_PLAYER_ABILITY_REPAIR - Player has NOT unlocked the Repair Ability. Clearing availability.")
	
	CLEAR_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_PLAYER_ABILITY_VEHICLE_REPAIR)
ENDPROC 

PROC PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_PLAYER_ABILITY_DISTRACTION(INT iTeam, INT iRule)

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_Tuner_Rule_Flag_Player_Ability_Distraction_Req_Lvl_15_Car_Club)
		EXIT
	ENDIF
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1	
		PRINTLN("[LM][AltVarsSystem][Team: ", iTeam, "][Rule: ", iRule, "] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_PLAYER_ABILITY_DISTRACTION - GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1 EXITTING...")
		EXIT
	ENDIF
	
	IF HAS_PLAYER_UNLOCKED_ABILITY_DISTRACTION_WITH_CAR_CLUB_REP_TIER(GB_GET_LOCAL_PLAYER_GANG_BOSS())
		PRINTLN("[LM][AltVarsSystem][Team: ", iTeam, "][Rule: ", iRule, "] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_PLAYER_ABILITY_DISTRACTION - Player has unlocked the Distraction Ability. ")
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Team: ", iTeam, "][Rule: ", iRule, "] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_PLAYER_ABILITY_DISTRACTION - Player has NOT unlocked the Distraction Ability. Clearing availability.")
	
	CLEAR_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_PLAYER_ABILITY_DISTRACTION)
ENDPROC 

PROC PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_PLAYER_ABILITY_AMBUSH(INT iTeam, INT iRule)

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_Tuner_Rule_Flag_Player_Ability_Ambush_Req_Lvl_25_Car_Club)
		EXIT
	ENDIF
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1	
		PRINTLN("[LM][AltVarsSystem][Team: ", iTeam, "][Rule: ", iRule, "] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_PLAYER_ABILITY_AMBUSH - GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1 EXITTING...")
		EXIT
	ENDIF
	
	IF HAS_PLAYER_UNLOCKED_ABILITY_AMBUSH_WITH_CAR_CLUB_REP_TIER(GB_GET_LOCAL_PLAYER_GANG_BOSS())
		PRINTLN("[LM][AltVarsSystem][Team: ", iTeam, "][Rule: ", iRule, "] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_PLAYER_ABILITY_AMBUSH - Player has unlocked the Ambush Ability. ")
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Team: ", iTeam, "][Rule: ", iRule, "] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_PLAYER_ABILITY_AMBUSH - Player has NOT unlocked the Ambush Ability. Clearing availability.")
	
	CLEAR_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_PLAYER_ABILITY_AMBUSH)	
ENDPROC 

PROC PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_PLAYER_ABILITY_RIDEALONG(INT iTeam, INT iRule)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_Tuner_Rule_Flag_Player_Ability_RideAlong_Req_Lvl_30_Car_Club)
		EXIT
	ENDIF
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1	
		PRINTLN("[LM][AltVarsSystem][Team: ", iTeam, "][Rule: ", iRule, "] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_PLAYER_ABILITY_RIDEALONG - GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1 EXITTING...")
		EXIT
	ENDIF
	
	IF HAS_PLAYER_UNLOCKED_ABILITY_RIDE_ALONG_WITH_CAR_CLUB_REP_TIER(GB_GET_LOCAL_PLAYER_GANG_BOSS())
		PRINTLN("[LM][AltVarsSystem][Team: ", iTeam, "][Rule: ", iRule, "] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_PLAYER_ABILITY_RIDEALONG - Player has unlocked the Ride Along Ability. ")
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Team: ", iTeam, "][Rule: ", iRule, "] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_PLAYER_ABILITY_RIDEALONG - Player has NOT unlocked the Ride along Ability. Clearing availability.")
	
	CLEAR_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_PLAYER_ABILITY_RIDE_ALONG)
ENDPROC 

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Bunker Interior Overrides																														 ---------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL GET_MC_ALT_VAR_TUNER_BUNKER_USING_STANDARD_SET(INT iBunker)
	SWITCH iBunker
		CASE ciTUNER_BUNKER_LOCATION_GRAND_SENORA
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grand_Senora_STANDARD_SET)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_GRAPESEED
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grapeseed_STANDARD_SET)	
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_RATON_CANYON
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Raton_Canyon_STANDARD_SET)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_HARMONY
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Harmony_STANDARD_SET)
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_MC_ALT_VAR_TUNER_BUNKER_USING_UPGRADE_SET(INT iBunker)
	SWITCH iBunker
		CASE ciTUNER_BUNKER_LOCATION_GRAND_SENORA
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grand_Senora_UPGRADE_SET)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_GRAPESEED
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grapeseed_UPGRADE_SET)	
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_RATON_CANYON
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Raton_Canyon_UPGRADE_SET)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_HARMONY
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Harmony_UPGRADE_SET)
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_MC_ALT_VAR_TUNER_BUNKER_USING_STYLE_A(INT iBunker)
	SWITCH iBunker
		CASE ciTUNER_BUNKER_LOCATION_GRAND_SENORA
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grand_Senora_STYLE_A)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_GRAPESEED
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grapeseed_STYLE_A)	
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_RATON_CANYON
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Raton_Canyon_STYLE_A)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_HARMONY
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Harmony_STYLE_A)
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_MC_ALT_VAR_TUNER_BUNKER_USING_STYLE_B(INT iBunker)
	SWITCH iBunker
		CASE ciTUNER_BUNKER_LOCATION_GRAND_SENORA
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grand_Senora_STYLE_B)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_GRAPESEED
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grapeseed_STYLE_B)	
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_RATON_CANYON
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Raton_Canyon_STYLE_B)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_HARMONY
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Harmony_STYLE_B)
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_MC_ALT_VAR_TUNER_BUNKER_USING_STYLE_C(INT iBunker)
	SWITCH iBunker
		CASE ciTUNER_BUNKER_LOCATION_GRAND_SENORA
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grand_Senora_STYLE_C)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_GRAPESEED
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grapeseed_STYLE_C)	
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_RATON_CANYON
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Raton_Canyon_STYLE_C)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_HARMONY
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Harmony_STYLE_C)
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_MC_ALT_VAR_TUNER_BUNKER_USING_OFFICE_UPGRADE_SET(INT iBunker)
	SWITCH iBunker
		CASE ciTUNER_BUNKER_LOCATION_GRAND_SENORA
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grand_Senora_OFFICE_UPGRADE_SET)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_GRAPESEED
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grapeseed_OFFICE_UPGRADE_SET)	
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_RATON_CANYON
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Raton_Canyon_OFFICE_UPGRADE_SET)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_HARMONY
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Harmony_OFFICE_UPGRADE_SET)
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_MC_ALT_VAR_TUNER_BUNKER_USING_OFFICE_BLOCKER_SET(INT iBunker)
	SWITCH iBunker
		CASE ciTUNER_BUNKER_LOCATION_GRAND_SENORA
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grand_Senora_OFFICE_BLOCKER_SET)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_GRAPESEED
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grapeseed_OFFICE_BLOCKER_SET)	
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_RATON_CANYON
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Raton_Canyon_OFFICE_BLOCKER_SET)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_HARMONY
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Harmony_OFFICE_BLOCKER_SET)
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_MC_ALT_VAR_TUNER_BUNKER_USING_GUN_RANGE_BLOCKER_SET(INT iBunker)
	SWITCH iBunker
		CASE ciTUNER_BUNKER_LOCATION_GRAND_SENORA
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grand_Senora_GUN_RANGE_BLOCKER_SET)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_GRAPESEED
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grapeseed_GUN_RANGE_BLOCKER_SET)	
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_RATON_CANYON
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Raton_Canyon_GUN_RANGE_BLOCKER_SET)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_HARMONY
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Harmony_GUN_RANGE_BLOCKER_SET)
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_MC_ALT_VAR_TUNER_BUNKER_USING_GUN_LOCKER_UPGRADE(INT iBunker)
	SWITCH iBunker
		CASE ciTUNER_BUNKER_LOCATION_GRAND_SENORA
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grand_Senora_GUN_LOCKER_UPGRADE)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_GRAPESEED
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grapeseed_GUN_LOCKER_UPGRADE)	
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_RATON_CANYON
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Raton_Canyon_GUN_LOCKER_UPGRADE)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_HARMONY
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Harmony_GUN_LOCKER_UPGRADE)
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_MC_ALT_VAR_TUNER_BUNKER_USING_STANDARD_SECURITY_SET(INT iBunker)
	SWITCH iBunker
		CASE ciTUNER_BUNKER_LOCATION_GRAND_SENORA
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grand_Senora_STANDARD_SECURITY_SET)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_GRAPESEED
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grapeseed_STANDARD_SECURITY_SET)	
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_RATON_CANYON
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Raton_Canyon_STANDARD_SECURITY_SET)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_HARMONY
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Harmony_STANDARD_SECURITY_SET)
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_MC_ALT_VAR_TUNER_BUNKER_USING_SECURITY_UPGRADE(INT iBunker)
	SWITCH iBunker
		CASE ciTUNER_BUNKER_LOCATION_GRAND_SENORA
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grand_Senora_SECURITY_UPGRADE)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_GRAPESEED
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grapeseed_SECURITY_UPGRADE)	
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_RATON_CANYON
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Raton_Canyon_SECURITY_UPGRADE)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_HARMONY
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Harmony_SECURITY_UPGRADE)
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_MC_ALT_VAR_TUNER_BUNKER_USING_GUN_WALL_BLOCKER(INT iBunker)
	SWITCH iBunker
		CASE ciTUNER_BUNKER_LOCATION_GRAND_SENORA
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grand_Senora_GUN_WALL_BLOCKER)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_GRAPESEED
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grapeseed_GUN_WALL_BLOCKER)	
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_RATON_CANYON
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Raton_Canyon_GUN_WALL_BLOCKER)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_HARMONY
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Harmony_GUN_WALL_BLOCKER)
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_MC_ALT_VAR_TUNER_BUNKER_USING_STANDARD_SET_MORE(INT iBunker)
	SWITCH iBunker
		CASE ciTUNER_BUNKER_LOCATION_GRAND_SENORA
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grand_Senora_STANDARD_SET_MORE)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_GRAPESEED
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grapeseed_STANDARD_SET_MORE)	
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_RATON_CANYON
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Raton_Canyon_STANDARD_SET_MORE)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_HARMONY
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Harmony_STANDARD_SET_MORE)
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_MC_ALT_VAR_TUNER_BUNKER_USING_UPGRADE_SET_MORE(INT iBunker)
	SWITCH iBunker
		CASE ciTUNER_BUNKER_LOCATION_GRAND_SENORA
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grand_Senora_UPGRADE_SET_MORE)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_GRAPESEED
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Grapeseed_UPGRADE_SET_MORE)	
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_RATON_CANYON
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Raton_Canyon_UPGRADE_SET_MORE)
		BREAK
		CASE ciTUNER_BUNKER_LOCATION_HARMONY
			RETURN FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Bunker_Harmony_UPGRADE_SET_MORE)
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_BUNKER_WITH_FREEMODE_DATA()

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_Tuner_Generic_Flag_Use_AltVar_Bunker_Data)
		EXIT
	ENDIF
	
	INT iIndex = GET_MC_ALT_VAR_TUNER_BUNKER_CHOSEN_FROM_PREP()
	
	IF iIndex = -1
		PRINTLN("[LM][AltVarsSystem] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_BUNKER_WITH_FREEMODE_DATA - Not overriding. Bunker iIndex: ", iIndex)
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_BUNKER_WITH_FREEMODE_DATA - Overriding. Bunker iIndex: ", iIndex)
	
	SET_BIT(g_FMMC_STRUCT.iInteriorBS, INTERIOR_BUNKER)
	
	IF GET_MC_ALT_VAR_TUNER_BUNKER_USING_STANDARD_SET(iIndex)	
		PRINTLN("[LM][AltVarsSystem] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_BUNKER_WITH_FREEMODE_DATA - Setting ciBUNKER_STANDARD_SET")	
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STANDARD_SET)
	ENDIF
	
	IF GET_MC_ALT_VAR_TUNER_BUNKER_USING_UPGRADE_SET(iIndex)
		PRINTLN("[LM][AltVarsSystem] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_BUNKER_WITH_FREEMODE_DATA - Setting ciBUNKER_UPGRADE_SET")	
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_UPGRADE_SET)
	ENDIF
	
	IF GET_MC_ALT_VAR_TUNER_BUNKER_USING_STYLE_A(iIndex)
		PRINTLN("[LM][AltVarsSystem] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_BUNKER_WITH_FREEMODE_DATA - Setting ciBUNKER_STYLE_A")	
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STYLE_A)
	ENDIF
	
	IF GET_MC_ALT_VAR_TUNER_BUNKER_USING_STYLE_B(iIndex)
		PRINTLN("[LM][AltVarsSystem] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_BUNKER_WITH_FREEMODE_DATA - Setting ciBUNKER_STYLE_B")	
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STYLE_B)
	ENDIF
	
	IF GET_MC_ALT_VAR_TUNER_BUNKER_USING_STYLE_C(iIndex)
		PRINTLN("[LM][AltVarsSystem] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_BUNKER_WITH_FREEMODE_DATA - Setting ciBUNKER_STYLE_C")	
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STYLE_C)
	ENDIF
	
	IF GET_MC_ALT_VAR_TUNER_BUNKER_USING_OFFICE_UPGRADE_SET(iIndex)
		PRINTLN("[LM][AltVarsSystem] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_BUNKER_WITH_FREEMODE_DATA - Setting ciBUNKER_OFFICE_UPGRADE_SET")	
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_OFFICE_UPGRADE_SET)
	ENDIF
	
	IF GET_MC_ALT_VAR_TUNER_BUNKER_USING_OFFICE_BLOCKER_SET(iIndex)
		PRINTLN("[LM][AltVarsSystem] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_BUNKER_WITH_FREEMODE_DATA - Setting ciBUNKER_OFFICE_BLOCKER_SET")	
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_OFFICE_BLOCKER_SET)
	ENDIF
	
	IF GET_MC_ALT_VAR_TUNER_BUNKER_USING_GUN_RANGE_BLOCKER_SET(iIndex)
		PRINTLN("[LM][AltVarsSystem] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_BUNKER_WITH_FREEMODE_DATA - Setting ciBUNKER_GUN_RANGE_BLOCKER_SET")	
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_GUN_RANGE_BLOCKER_SET)
	ENDIF
	
	IF GET_MC_ALT_VAR_TUNER_BUNKER_USING_GUN_LOCKER_UPGRADE(iIndex)
		PRINTLN("[LM][AltVarsSystem] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_BUNKER_WITH_FREEMODE_DATA - Setting ciBUNKER_GUN_LOCKER_UPGRADE")	
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_GUN_LOCKER_UPGRADE)
	ENDIF
	
	IF GET_MC_ALT_VAR_TUNER_BUNKER_USING_STANDARD_SECURITY_SET(iIndex)
		PRINTLN("[LM][AltVarsSystem] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_BUNKER_WITH_FREEMODE_DATA - Setting ciBUNKER_STANDARD_SECURITY_SET")	
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STANDARD_SECURITY_SET)
	ENDIF
	
	IF GET_MC_ALT_VAR_TUNER_BUNKER_USING_SECURITY_UPGRADE(iIndex)
		PRINTLN("[LM][AltVarsSystem] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_BUNKER_WITH_FREEMODE_DATA - Setting ciBUNKER_SECURITY_UPGRADE")	
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_SECURITY_UPGRADE)
	ENDIF
	
	IF GET_MC_ALT_VAR_TUNER_BUNKER_USING_GUN_WALL_BLOCKER(iIndex)
		PRINTLN("[LM][AltVarsSystem] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_BUNKER_WITH_FREEMODE_DATA - Setting ciBUNKER_GUN_WALL_BLOCKER")	
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_GUN_WALL_BLOCKER)
	ENDIF
	
	IF GET_MC_ALT_VAR_TUNER_BUNKER_USING_STANDARD_SET_MORE(iIndex)
		PRINTLN("[LM][AltVarsSystem] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_BUNKER_WITH_FREEMODE_DATA - Setting ciBUNKER_STANDARD_SET_MORE")	
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciBUNKER_STANDARD_SET_MORE)
	ENDIF
	
	IF GET_MC_ALT_VAR_TUNER_BUNKER_USING_UPGRADE_SET_MORE(iIndex)
		PRINTLN("[LM][AltVarsSystem] - PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_BUNKER_WITH_FREEMODE_DATA - Setting ciBUNKER_UPGRADE_SET_MORE")	
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciBUNKER_UPGRADE_SET_MORE)
	ENDIF	
	
ENDPROC
#ENDIF

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Dummy Blip Overrides																														 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL GET_MC_ALT_VAR_TUNER_HAS_BANK_BEEN_JAMMED(INT i)
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1	
		PRINTLN("[LM][AltVarsSystem][DummyBlip: ", i, "] - GET_MC_ALT_VAR_TUNER_HAS_BANK_BEEN_JAMMED - NO GANG BOSS!!!")	
		RETURN FALSE
	ENDIF
	
	SWITCH i
		CASE 0
			RETURN IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_A))
		BREAK
		CASE 1
			RETURN IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_B))
		BREAK
		CASE 2		
			RETURN IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_C))
		BREAK
		CASE 3		
			RETURN IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_D))
		BREAK
		CASE 4	
			RETURN IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_E))
		BREAK
		CASE 5
			RETURN IS_BIT_SET(GlobalPlayerBD_Flow[GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()].Tuner.iJammerBS, ENUM_TO_INT(eSIGNALJAMMERSFLEECABANK_F))
		BREAK
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC INT GET_MC_ALT_VAR_TUNER_DUMMY_BLIP_BANK_INDEX_FLAGGED(INT iDummyBlip)
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iAltVarsBitsetDummyBlip, ciAltVarsBS_Tuner_DummyBlips_Flag_JammedBank_Overrides_OutlineColour_A)
		RETURN 0
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iAltVarsBitsetDummyBlip, ciAltVarsBS_Tuner_DummyBlips_Flag_JammedBank_Overrides_OutlineColour_B)
		RETURN 1
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iAltVarsBitsetDummyBlip, ciAltVarsBS_Tuner_DummyBlips_Flag_JammedBank_Overrides_OutlineColour_C)
		RETURN 2
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iAltVarsBitsetDummyBlip, ciAltVarsBS_Tuner_DummyBlips_Flag_JammedBank_Overrides_OutlineColour_D)
		RETURN 3
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iAltVarsBitsetDummyBlip, ciAltVarsBS_Tuner_DummyBlips_Flag_JammedBank_Overrides_OutlineColour_E)
		RETURN 4
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iAltVarsBitsetDummyBlip, ciAltVarsBS_Tuner_DummyBlips_Flag_JammedBank_Overrides_OutlineColour_F)
		RETURN 5
	ENDIF
	
	RETURN -1
ENDFUNC

PROC PROCESS_MC_ALT_VAR_TUNER_DUMMY_BLIP_OVERRIDE_BLIP_OUTLINE_BASED_ON_JAMMED_BANK(INT iDummyBlip)
	
	INT iIndex = GET_MC_ALT_VAR_TUNER_DUMMY_BLIP_BANK_INDEX_FLAGGED(iDummyBlip)		
	IF iIndex = -1
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][DummyBlip: ", iDummyBlip, "] - PROCESS_MC_ALT_VAR_TUNER_DUMMY_BLIP_OVERRIDE_BLIP_OUTLINE_BASED_ON_JAMMED_BANK - Flagged as Bank Index: ", iIndex)

	IF NOT GET_MC_ALT_VAR_TUNER_HAS_BANK_BEEN_JAMMED(iIndex)
		PRINTLN("[LM][AltVarsSystem][DummyBlip: ", iDummyBlip, "] - PROCESS_MC_ALT_VAR_TUNER_DUMMY_BLIP_OVERRIDE_BLIP_OUTLINE_BASED_ON_JAMMED_BANK - Bank has been jammed, Applying RADAR_TRACE_OVERLAY_JAMMED")
		g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].vPos = <<0.0, 0.0, 0.0>>
	ELSE
		PRINTLN("[LM][AltVarsSystem][DummyBlip: ", iDummyBlip, "] - PROCESS_MC_ALT_VAR_TUNER_DUMMY_BLIP_OVERRIDE_BLIP_OUTLINE_BASED_ON_JAMMED_BANK - Bank has not been jammed, hiding dummmy blip overlay.")
		g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData.iBlipSpriteOverride = ENUM_TO_INT(RADAR_TRACE_OVERLAY_JAMMED)
	ENDIF	
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: (SERVER) AltVars Start up Process Server ------------------------------------------------------------------------------------------------------------------------
// ##### Description: This is called to decide certain variables before all the variations globals are applied to the mission globals. Done only by Server.		------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_TUNER()
	
	FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_TUNER_BS_SyncedData)	

	#IF FEATURE_TUNER 
	
	#ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: (CLIENT) AltVars Main Process / Loops ---------------------------------------------------------------------------------------------------------------------------
// ##### Description: All the global overrides and trigger checks are carried out here.											 								------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#IF FEATURE_TUNER 

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_SPAWN_GROUPS_TUNER()
	INT i, ii

	FOR i = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
		PROCESS_MC_ALT_VAR_TUNER_SPAWN_GROUP_ACTIVATION(i)
		FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
			PROCESS_MC_ALT_VAR_TUNER_SUB_SPAWN_GROUP_ACTIVATION(i, ii)
		ENDFOR
	ENDFOR
ENDPROC 

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_GLOBALS_TUNER()
	
	INT i, ii, iii	
	
	FOR i = 0 TO FMMC_MAX_RULES-1
		FOR ii = 0 TO FMMC_MAX_TEAMS-1
			PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_CUTSCENE_INDEX_WITH_BUNKER_LOCATION(ii, i)
			PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_PLAYER_ABILITY_REPAIR(ii, i)
			PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_PLAYER_ABILITY_DISTRACTION(ii, i)
			PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_PLAYER_ABILITY_AMBUSH(ii, i)
			PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_RULE_PLAYER_ABILITY_RIDEALONG(ii, i)
		ENDFOR
	ENDFOR
		
	FOR i = 0 TO FMMC_MAX_INVENTORIES-1
		FOR ii = 0 TO FMMC_MAX_INVENTORY_WEAPONS-1 
			FOR iii = 0 TO FMMC_MAX_WEAPON_COMPONENTS-1
			
			ENDFOR
		ENDFOR	
	ENDFOR
	
	PROCESS_MC_ALT_VAR_TUNER_OVERRIDE_BUNKER_WITH_FREEMODE_DATA()
	
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_PEDS_TUNER()
	INT i
	FOR i = 0 TO FMMC_MAX_PEDS-1
		PROCESS_MC_ALT_VAR_TUNER_PED_SPAWN_BLOCKING_FLAG_CHECKS(i)
		PROCESS_MC_ALT_VAR_TUNER_PED_PLAYER_ABILITY_RANDOMISATION(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_VEHICLES_TUNER()
	INT i
	INT iPartsAssigned = 0
	FOR i = 0 TO FMMC_MAX_VEHICLES-1	
		PROCESS_MC_ALT_VAR_TUNER_VEHICLE_SPAWN_BLOCKING_FLAG_CHECKS(i)
		PROCESS_MC_ALT_VAR_TUNER_VEH_POSITION_FORCED_BY_AUTOSHOP(i)
		PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE_OLD(i, iPartsAssigned)
		PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_PERSONAL_VEHICLE(i)
		PROCESS_MC_ALT_VAR_TUNER_VEHICLE_DROP_OFF_OVERRIDE_BY_ELEVATOR_PASS(i)		
		PROCESS_MC_ALT_VAR_TUNER_VEHICLE_PLAYER_ABILITY_RANDOMISATION(i)	
		PROCESS_MC_ALT_VAR_TUNER_VEHICLE_CLONE_TANKER_TRUCK_FROM_FREEMODE(i)
		PROCESS_MC_ALT_VAR_TUNER_VEHICLE_MOVE_CHECKPOINT_INDEX_BASED_ON_ELEVATOR_PASS(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_TRAINS_TUNER()
	INT i
	FOR i = 0 TO FMMC_MAX_TRAINS-1	
		PROCESS_MC_ALT_VAR_TUNER_TRAIN_EASTER_EGG(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_OBJECTS_TUNER()
	INT i
	FOR i = 0 TO FMMC_MAX_NUM_OBJECTS-1
		PROCESS_MC_ALT_VAR_TUNER_OBJECT_SPAWN_BLOCKING_FLAG_CHECKS(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_DYNOPROPS_TUNER()
	INT i
	FOR i = 0 TO FMMC_MAX_NUM_DYNOPROPS-1
		PROCESS_MC_ALT_VAR_TUNER_DYNO_PROP_SPAWN_BLOCKING_FLAG_CHECKS(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_PROPS_TUNER()
	INT i
	FOR i = 0 TO GET_FMMC_MAX_NUM_PROPS()-1
		PROCESS_MC_ALT_VAR_TUNER_PROP_SPAWN_BLOCKING_FLAG_CHECKS(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_DIALOGUE_TRIGGERS_TUNER()
	INT i
	FOR i = 0 TO FMMC_MAX_DIALOGUES-1
		PROCESS_MC_ALT_VAR_TUNER_DIALOGUE_TRIGGERS_SPAWN_BLOCKING_FLAG_CHECKS(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_ZONES_TUNER()
	INT i
	FOR i = 0 TO FMMC_MAX_NUM_ZONES - 1			
		PROCESS_MC_ALT_VAR_TUNER_ZONE_SPAWN_BLOCKING_FLAG_CHECKS(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_DUMMY_BLIPS_TUNER()
	INT i
	FOR i = 0 TO FMMC_MAX_DUMMY_BLIPS - 1
		PROCESS_MC_ALT_VAR_TUNER_DUMMY_BLIP_OVERRIDE_BLIP_OUTLINE_BASED_ON_JAMMED_BANK(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_PICKUPS_TUNER()
	INT i
	FOR i = 0 TO FMMC_MAX_WEAPONS-1
		PROCESS_MC_ALT_VAR_TUNER_WEAPON_SPAWN_BLOCKING_FLAG_CHECKS(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_SPAWN_POINTS_TUNER()
	INT i, ii
	FOR ii = 0 TO FMMC_MAX_TEAMS-1
		FOR i = 0 TO FMMC_MAX_TEAMSPAWNPOINTS-1
			PROCESS_MC_ALT_VAR_TUNER_TEAM_SPAWN_POINT_SPAWN_BLOCKING_FLAG_CHECKS(i, ii)
		ENDFOR
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_INTERACTABLES_TUNER()
	INT i
	FOR i = 0 TO FMMC_MAX_NUM_INTERACTABLES-1
		PROCESS_MC_ALT_VAR_TUNER_INTERACTABLE_SPAWN_BLOCKING_FLAG_CHECKS(i)
	ENDFOR
ENDPROC

#ENDIF

PROC PROCESS_APPLY_ALT_VAR_PRE_SPAWN_GROUP_SETTINGS_TUNER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_SPAWN_GROUPS_TUNER()
	
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_TUNER()
	
	#IF FEATURE_TUNER 
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1
		PRINTLN("[LM][AltVarsSystem] - ###########################################################################")	
		PRINTLN("[LM][AltVarsSystem] - ###### Something has gone seriously wrong. We have no valid boss INT ######")		
		PRINTLN("[LM][AltVarsSystem] - ###########################################################################")
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_NO_GANGBOSS, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "No Gang Boss, value is -1. This will cause a lot of ALT VARS To function improperly.")
		#ENDIF
	ELSE
		PRINTLN("[LM][AltVarsSystem] - Boss INT: ", GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT())
	ENDIF
		
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_GLOBALS_TUNER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_SPAWN_POINTS_TUNER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_PEDS_TUNER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_OBJECTS_TUNER()
		
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_VEHICLES_TUNER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_TRAINS_TUNER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_DYNOPROPS_TUNER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_PROPS_TUNER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_DIALOGUE_TRIGGERS_TUNER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_ZONES_TUNER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_DUMMY_BLIPS_TUNER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_PICKUPS_TUNER()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_INTERACTABLES_TUNER()
	
	#ENDIF
	
ENDPROC
