// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Bounds ------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc ----------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: All logic relating to bounds.                                                                                                                                                                                                                                                             
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020                                                                                       
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


USING "FM_Mission_Controller_Include_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: GENERAL HELPER FUNCTIONS ---------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to bounds blips and bounds names --------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC VECTOR GET_BOUNDS_RUNTIME_CENTRE(INT iBoundsIndex)
	RETURN sRuleBoundsRuntimeVars[iBoundsIndex].vRuleBoundsCentrePos
ENDFUNC

FUNC INT GET_MISSION_BOUNDS_TEAMS_BITSET(INT iBoundsIndex)
	
	INT iTeamBS
	INT iTeamLoop
	
	FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].sRuleBounds[GET_LOCAL_PLAYER_CURRENT_RULE()][iBoundsIndex].iBoundsBS, ciBounds_OnlyOneMemberOfTeam0Needed + iTeamLoop)
			SET_BIT(iTeamBS, iTeamLoop)
		ENDIF
	ENDFOR
	
	RETURN iTeamBS
ENDFUNC

FUNC BOOL IS_ANYONE_ON_NEEDED_TEAM_IN_BOUNDS(INT iBoundsIndex)
	
	INT iTeamBS = GET_MISSION_BOUNDS_TEAMS_BITSET(iBoundsIndex)
	IF iTeamBS = 0
		RETURN FALSE
	ENDIF
	
	DO_PARTICIPANT_LOOP_FLAGS eFlags = GET_PARTICIPANT_LOOP_FLAGS_FROM_VALID_TEAM_BITSET(iTeamBS)
	INT iPart
	WHILE DO_PARTICIPANT_LOOP(iPart, eFlags)
		IF NOT IS_PARTICIPANT_OUT_OF_BOUNDS(iPart, iBoundsIndex)
			PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] IS_ANYONE_ON_NEEDED_TEAM_IN_BOUNDS | Participant ", iPart, " is in bounds!")
			RETURN TRUE
		ENDIF
	ENDWHILE
	
	RETURN FALSE
	
ENDFUNC

FUNC TEXT_LABEL_63 GET_BOUNDS_OOB_STRING(RULE_BOUNDS_STRUCT& sBounds)
	IF IS_CUSTOM_STRING_LIST_STRING_VALID(sBounds.iOutOfBoundsFailStringIndex)
		RETURN GET_CUSTOM_STRING_LIST_TEXT_LABEL(sBounds.iOutOfBoundsFailStringIndex)
	ENDIF
	
	RETURN g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM(TRUE)].tl63BoundsFailMessage[GET_LOCAL_PLAYER_CURRENT_RULE(TRUE)]
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: BLIPS & NAMES ---------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to bounds blips and bounds names --------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_CLOSEST_OOB_BLIP_INDEX(INT iBoundsIndex)
	RETURN sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlip_CurrentClosestBlipIndex
ENDFUNC

PROC HANDLE_BOUNDS_OOB_BLIP_COLOUR(INT iBoundsIndex, BLIP_INDEX& blipIndex)
	
	IF NOT DOES_BLIP_EXIST(blipIndex)
		EXIT
	ENDIF
	
	INT iBlipColour = BLIP_COLOUR_YELLOW
	
	IF GET_BOUNDS_FIRST_ACTIVE_ZONE(iBoundsIndex) > -1
		iBlipColour = g_FMMC_STRUCT_ENTITIES.sPlacedZones[GET_BOUNDS_FIRST_ACTIVE_ZONE(iBoundsIndex)].iBlipColor
	ENDIF
	
	PRINTLN("[Bounds] HANDLE_BOUNDS_OOB_BLIP_COLOUR | Setting out-of-bounds blip to use colour: ", iBlipColour)
	SET_BLIP_COLOUR(blipIndex, iBlipColour)
ENDPROC

FUNC BOOL SHOULD_OOB_BOUNDS_BLIP_HAVE_GPS(INT iBoundsIndex, RULE_BOUNDS_STRUCT& sBounds, INT iOOBBlipIndexToUse)
	IF IS_BIT_SET(sBounds.iBoundsBS, ciBounds_UseGPSForBlip)
		RETURN TRUE
	ENDIF
	
	IF iOOBBlipIndexToUse = GET_CLOSEST_OOB_BLIP_INDEX(iBoundsIndex)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC HANDLE_BOUNDS_OOB_BLIP_HEIGHT_THRESHOLD(RULE_BOUNDS_STRUCT& sBounds, BLIP_INDEX& blipIndex)

	IF DOES_BLIP_EXIST(blipIndex)
		IF (sBounds.iBoundsBlipHeightThreshold = BLIP_HEIGHT_THRESHOLD_DEFAULT)
			PRINTLN("[HANDLE_BOUNDS_OOB_BLIP_HEIGHT_THRESHOLD] Setting bounds blip to use default height threshold")
			SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(blipIndex, FALSE)
			SET_BLIP_SHORT_HEIGHT_THRESHOLD(blipIndex, FALSE)
		ELIF (sBounds.iBoundsBlipHeightThreshold = BLIP_HEIGHT_THRESHOLD_EXTENDED)
			PRINTLN("[HANDLE_BOUNDS_OOB_BLIP_HEIGHT_THRESHOLD] Setting bounds blip to use extended height threshold")
			SET_BLIP_SHORT_HEIGHT_THRESHOLD(blipIndex, FALSE)
			SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(blipIndex, TRUE)
		ELIF (sBounds.iBoundsBlipHeightThreshold = BLIP_HEIGHT_THRESHOLD_SHORT)
			PRINTLN("[HANDLE_BOUNDS_OOB_BLIP_HEIGHT_THRESHOLD] Setting bounds blip to use short height threshold")
			SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(blipIndex, FALSE)
			SET_BLIP_SHORT_HEIGHT_THRESHOLD(blipIndex, TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_OUT_OF_BOUNDS_BLIP(INT iBoundsIndex, INT iOOBBlipIndex)
	IF DOES_BLIP_EXIST(sRuleBoundsRuntimeVars[iBoundsIndex].biOutOfBoundsBlips[iOOBBlipIndex])
		PRINTLN("[Bounds][RuleBounds ", iBoundsIndex, "] REMOVE_OUT_OF_BOUNDS_BLIP | Removing blip for Zone ", iOOBBlipIndex)
		REMOVE_BLIP(sRuleBoundsRuntimeVars[iBoundsIndex].biOutOfBoundsBlips[iOOBBlipIndex])
		sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlipsCreated--
		
		IF sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlipUsingGPS = iOOBBlipIndex
			PRINTLN("[Bounds][RuleBounds ", iBoundsIndex, "] REMOVE_OUT_OF_BOUNDS_BLIP | Disabling SET_FORCE_SHOW_GPS because we're removing the blip currently using GPS")
			SET_FORCE_SHOW_GPS(FALSE)
			sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlipUsingGPS = -1
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_REMOVING_OOB_BLIPS_STAGGERED(INT iBoundsIndex, BOOL bIncrementStaggerCounter = FALSE)
	INT iOOBBlipIndexToUse = sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlipStaggeredIndex
	IF DOES_BLIP_EXIST(sRuleBoundsRuntimeVars[iBoundsIndex].biOutOfBoundsBlips[iOOBBlipIndexToUse])
		PRINTLN("[Bounds][RuleBounds ", iBoundsIndex, "][OOB Blip ", iOOBBlipIndexToUse, "] PROCESS_REMOVING_OOB_BLIPS_STAGGERED - Removing OOB blip ", iOOBBlipIndexToUse)
		REMOVE_OUT_OF_BOUNDS_BLIP(iBoundsIndex, iOOBBlipIndexToUse)
	ENDIF
	
	IF bIncrementStaggerCounter
		sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlipStaggeredIndex++
		IF sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlipStaggeredIndex >= FMMC_MAX_NUM_ZONES
			sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlipStaggeredIndex = 0
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: RENDER ON MINIMAP -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to the rendering of bounds on the minimap -----------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//[KH][SSDS] Sumo sudden death blip drawing and updating
PROC RENDER_SUDDEN_DEATH_AREA_BOUNDS_ON_MINIMAP(INT iTeam, INT iRule)

	UNUSED_PARAMETER(iTeam)
	UNUSED_PARAMETER(iRule)
	
	//Set up the blip for this first, make sure old blip isn't used
	IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_IS_SUDDEN_DEATH_BLIP_SETUP)
		IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)
			IF vSphereSpawnPosition.x != 0 AND vSphereSpawnPosition.y != 0 AND vSphereSpawnPosition.z != 0
				IF DOES_BLIP_EXIST(SuddenDeathAreaRadiusBlip)
					REMOVE_BLIP(SuddenDeathAreaRadiusBlip)
				ENDIF
				
				PRINTLN("[RCC MISSION] RENDER_SUDDEN_DEATH_AREA_BOUNDS_ON_MINIMAP : vSphereSpawnPosition", vSphereSpawnPosition)
				PRINTLN("[RCC MISSION] RENDER_SUDDEN_DEATH_AREA_BOUNDS_ON_MINIMAP : fCurrentSphereRadius", g_FMMC_STRUCT.fSphereStartRadius)
				
				SuddenDeathAreaRadiusBlip = ADD_BLIP_FOR_RADIUS( vSphereSpawnPosition, g_FMMC_STRUCT.fSphereStartRadius)
				SET_BLIP_SCALE(SuddenDeathAreaRadiusBlip, g_FMMC_STRUCT.fSphereStartRadius)
				SET_BLIP_ALPHA(SuddenDeathAreaRadiusBlip, 120)
				HANDLE_BOUNDS_OOB_BLIP_COLOUR(0, SuddenDeathAreaRadiusBlip)
				SET_BIT(iLocalBoolCheck15, LBOOL15_IS_SUDDEN_DEATH_BLIP_SETUP) 
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(SuddenDeathAreaRadiusBlip)
				REMOVE_BLIP(SuddenDeathAreaRadiusBlip)
			ENDIF
		
			PRINTLN("[RCC MISSION] RENDER_SUDDEN_DEATH_AREA_BOUNDS_ON_MINIMAP : vSphereSpawnPoint", g_FMMC_STRUCT.vSphereSpawnPoint)
			PRINTLN("[RCC MISSION] RENDER_SUDDEN_DEATH_AREA_BOUNDS_ON_MINIMAP : fCurrentSphereRadius", g_FMMC_STRUCT.fSphereStartRadius)
			
			SuddenDeathAreaRadiusBlip = ADD_BLIP_FOR_RADIUS( g_FMMC_STRUCT.vSphereSpawnPoint, g_FMMC_STRUCT.fSphereStartRadius)
			SET_BLIP_SCALE(SuddenDeathAreaRadiusBlip, g_FMMC_STRUCT.fSphereStartRadius)
			SET_BLIP_ALPHA(SuddenDeathAreaRadiusBlip, 120)
			HANDLE_BOUNDS_OOB_BLIP_COLOUR(0, SuddenDeathAreaRadiusBlip)
			SET_BIT(iLocalBoolCheck15, LBOOL15_IS_SUDDEN_DEATH_BLIP_SETUP) 
		ENDIF
	ENDIF
ENDPROC




// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: EXPLODE ON BOUNDS FAIL ------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to making th eplayer explode when out of bounds -----------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL(INT iTeam, INT iRule, BOOL bReadyForSpectate = FALSE, BOOL bForceExplodeOnRuleCheck = FALSE)
	
	IF HAS_LOCAL_PLAYER_FINISHED_OR_REACHED_END()
		EXIT
	ENDIF
	
	IF SHOULD_EXPLODE_ON_BOUNDS_FAIL(iTeam, iRule)
	OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_EXPLODE_ON_RULE_END_BOUNDS_FAIL)
	OR bForceExplodeOnRuleCheck	
		IF bLocalPlayerPedOK
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
				
				VEHICLE_INDEX viVehiclePlayerIsIn = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				
				IF bReadyForSpectate
					SET_BIT(iLocalBoolCheck14, LBOOL14_OUT_OF_BOUNDS_DEATHFAIL)
					SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP)
				ENDIF
				
				//[KH] If player is in a vehicle and it is set to be explosion proof, remove this before exploding
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciVEHICLE_EXPLOSION_PROOF)
				OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_WEAPON_BULLETPROOF_PLAYER)
					SET_ENTITY_PROOFS(viVehiclePlayerIsIn, FALSE, FALSE, FALSE, FALSE, FALSE)
				ENDIF
				
				IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)				
					SET_ENTITY_INVINCIBLE(LocalPlayerPed, FALSE)
				ENDIF
				
				SET_ENTITY_INVINCIBLE(viVehiclePlayerIsIn, FALSE)
				SET_ENTITY_PROOFS(LocalPlayerPed, FALSE, FALSE, FALSE, FALSE, FALSE)
				
				IF DOES_ENTITY_EXIST(viVehiclePlayerIsIn)
					NETWORK_EXPLODE_VEHICLE(viVehiclePlayerIsIn)
				ENDIF
				
				
				PRINTLN("[RCC MISSION] EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL - calling NETWORK_EXPLODE_VEHICLE on player vehicle due to bounds fail")
			ELSE
				IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_OOB_ON_FOOT_EXPLODE_DONE)
					VECTOR vPlayer = GET_ENTITY_COORDS(LocalPlayerPed)
					
					FLOAT fGroundZ
					IF NOT IS_ENTITY_IN_AIR(LocalPlayerPed)
					AND GET_GROUND_Z_FOR_3D_COORD(vPlayer, fGroundZ)
						vPlayer.Z = fGroundZ
					ELSE
						VECTOR vGround = GET_PED_BONE_COORDS(LocalPlayerPed, BONETAG_R_FOOT, <<0, 0, -0.1>>)
						
						IF NOT IS_VECTOR_ZERO(vGround)
							vPlayer = vGround
						ENDIF
					ENDIF
					
					IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
						SET_ENTITY_INVINCIBLE(LocalPlayerPed, FALSE)
					ENDIF
					
					SET_ENTITY_PROOFS(LocalPlayerPed, FALSE, FALSE, FALSE, FALSE, FALSE)
					IF NOT IS_PED_SWIMMING(LocalPlayerPed)
						ADD_EXPLOSION(vPlayer, EXP_TAG_GRENADELAUNCHER, 1.0)
					
						SET_PED_TO_RAGDOLL(LocalPlayerPed, 5000, 10000, TASK_RELAX)
					
						VECTOR vVelocity = GET_ENTITY_VELOCITY(LocalPlayerPed)
						SET_ENTITY_VELOCITY(LocalPlayerPed, vVelocity + <<0, 0, 15>>)
					ELSE
						SET_ENTITY_HEALTH(LocalPlayerPed, 0)
					ENDIF
					
					PRINTLN("[RCC MISSION] EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL - calling ADD_EXPLOSION at player coords ",vPlayer)
					
					SET_BIT(iLocalBoolCheck26, LBOOL26_OOB_ON_FOOT_EXPLODE_DONE)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciNEGATIVE_SUICIDE_SCORE)
			BROADCAST_FMMC_PLAYER_COMMITED_SUICIDE(iTeam)
			
			//Negative score
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciNEGATIVE_SUICIDE_SCORE)
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciNEGATIVE_SUICIDE_FALL_IS_SUICIDE)
				IF MC_playerBD[iPartToUse].iNumPlayerKills > 0
					MC_playerBD[iPartToUse].iNumPlayerKills--
					PRINTLN("[JS] - EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL SUICIDE POINTS - decrementing local player's kills")
				ENDIF
			
				IF MC_serverBD.iNumPlayerRuleHighestPriority[MC_playerBD[iPartToUse].iteam] > 0 
					IF MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_PLAYERS // this high priority kill player objective
						IF MC_playerBD[iPartToUse].iPlayerScore > 0
							PRINTLN("EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL SUICIDE POINTS - decrementing local player's score (KILL_PLAYERS)")
							REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(-1)							
						ENDIF
						
					ELIF MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM0	
					OR MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM1	
					OR MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM2
					OR MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM3
						IF MC_playerBD[iPartToUse].iPlayerScore > 0
						AND MC_Playerbd[iPartToUse].iKillScore > 0
							PRINTLN("[JS] - EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL SUICIDE POINTS - decrementing local player's score (KILL_TEAM)")
							REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(-1,TRUE)							
						ENDIF
					ENDIF	
				ELSE
					IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill > 0
						IF MC_playerBD[iPartToUse].iPlayerScore >= g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill
							PRINTLN("[JS] - EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL SUICIDE POINTS - iPoints per kill = ", g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill)
							REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_BY((g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill * -1),TRUE)							
						ENDIF
					ENDIF
				ENDIF
			ENDIF

		ENDIF
		
	ENDIF
	
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: HEALTH REGEN IN BOUNDS ------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to player health regeneration wwhen in bounds -------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_VEH_HEALTH_REGEN_FOR_BOUNDS(VEHICLE_INDEX vehToRegen = NULL, INT iCapOverride = 0)
	IF NOT HAS_NET_TIMER_STARTED(tdVehicleRegenTimer)
		START_NET_TIMER(tdVehicleRegenTimer)
	ENDIF
	
	VEHICLE_INDEX vehPlayer	
	IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
	ENDIF	
	IF DOES_ENTITY_EXIST(vehToRegen)
		vehPlayer = vehToRegen
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayer)
		FLOAT fTotal = GET_FMMC_PLAYER_VEHICLE_BODY_HEALTH_FOR_TEAM(MC_PlayerBD[iLocalPart].iteam)
		IF iPlacedVehicleIAmIn > -1
			fTotal = TO_FLOAT(GET_FMMC_VEHICLE_MAX_HEALTH(iPlacedVehicleIAmIn))
		ENDIF
		
		INT iRegenCap = iRegenCapFromBounds		
		IF iCapOverride != 0
			iRegenCap = iCapOverride
		ENDIF
				
		FLOAT fCappedTotal = (fTotal * (TO_FLOAT(iRegenCap)/100))
		FLOAT fVehicleCurrentBodyHP = GET_VEHICLE_BODY_HEALTH(vehPlayer)
		
		PRINTLN("[LM][PROCESS_BOUNDS_VEHICLE_REGEN] (before) HP Checks - iPlacedVehicleIAmIn: ", iPlacedVehicleIAmIn, " fCappedTotal: ", fCappedTotal, " fTotal: ", fTotal, " fVehicleCurrentBodyHP: ", fVehicleCurrentBodyHP, " iRegenCap: ", iRegenCap)
		
		IF fVehicleCurrentBodyHP < fCappedTotal
		AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdVehicleRegenTimer, ci_REGEN_VEHICLE_AFTER_THIS_TIME)
		
			fVehicleCurrentBodyHP += (fTotal * 0.04)
			
			IF fVehicleCurrentBodyHP > fCappedTotal
				fVehicleCurrentBodyHP = fCappedTotal
			ENDIF
						
			SET_VEHICLE_BODY_HEALTH(vehPlayer, fVehicleCurrentBodyHP)
			RESET_NET_TIMER(tdVehicleRegenTimer)
			PRINTLN("[LM][PROCESS_BOUNDS_VEHICLE_REGEN] Healing Vehicle!")
			PRINTLN("[LM][PROCESS_BOUNDS_VEHICLE_REGEN] (after) HP Checks - fVehicleCurrentBodyHP: ", GET_VEHICLE_BODY_HEALTH(vehPlayer))
			
			IF fVehicleCurrentBodyHP >= fTotal
				SET_VEHICLE_FIXED(vehToRegen)
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[LM][PROCESS_BOUNDS_VEHICLE_REGEN] - Veh not driveable.....")
	ENDIF
	
ENDPROC

PROC PROCESS_BOUNDS_VEHICLE_REGEN(INT iBoundsIndex, RULE_BOUNDS_STRUCT& sBounds)

	UNUSED_PARAMETER(iBoundsIndex)

	IF sBounds.iVehicleRegenAndCap > 0
		iRegenCapFromBounds = sBounds.iVehicleRegenAndCap
		PROCESS_VEH_HEALTH_REGEN_FOR_BOUNDS()
	ENDIF
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: HANDLE BOUNDS TIMERS AND TEXT -----------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to bounds timers and bounds text --------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_END_OF_RULE_BOUNDS_CHECK(INT iTeam, INT iRule)	
	
	INT iTimeLimitToUse
	INT iRemainingTime

	IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		iTimeLimitToUse = GET_SUDDEN_DEATH_TIME_LIMIT_FOR_RULE(iTeam, iRule)
		iRemainingTime = GET_SUDDEN_DEATH_TIME_REMAINING(iTeam, iRule)
	ELSE
		iTimeLimitToUse = GET_OBJECTIVE_TIME_LIMIT_FOR_RULE(iTeam, iRule)
		iRemainingTime = GET_OBJECTIVE_TIMER_TIME_REMAINING(iTeam, iRule)
	ENDIF
	
	// [FIX_2020_CONTROLLER] LBOOL25_SUMO_RUN_TIMER_DELAY is incorrectly named and is used outside of sumo run mode. fmmc2020		
	IF DOES_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(iTeam)
		CLEAR_BIT(iLocalBoolCheck25, LBOOL25_SUMO_RUN_TIMER_DELAY)
		PRINTLN("[PROCESS_END_OF_RULE_BOUNDS_CHECK] CLEAR_BIT LBOOL25_SUMO_RUN_TIMER_DELAY")
	ENDIF
	
	// This is to add a delay to the check, as previosuly MC_serverBD_3.tdObjectiveLimitTimer was becoming out of sync with the current rule and not resetting.
	IF iRemainingTime < iTimeLimitToUse / 2
	AND NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_SUMO_RUN_TIMER_DELAY)	
		SET_BIT(iLocalBoolCheck25, LBOOL25_SUMO_RUN_TIMER_DELAY)	
		PRINTLN("[PROCESS_END_OF_RULE_BOUNDS_CHECK] SET_BIT LBOOL25_SUMO_RUN_TIMER_DELAY")
	ENDIF
	
	PRINTLN("[PROCESS_END_OF_RULE_BOUNDS_CHECK] iRemainingTime ", iRemainingTime, " iTimeLimitToUse: ",iTimeLimitToUse)
	
	IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_SUMO_RUN_TIMER_DELAY)
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_EXPLODE_ON_RULE_END_BOUNDS_FAIL) 
		
		IF iRemainingTime >= iTimeLimitToUse
		AND NOT (ARE_WE_CURRENTLY_IN_SUDDEN_DEATH() AND IS_BIT_SET(iLocalBoolCheck28,LBBOOL28_HAS_DONE_SUDDEN_DEATH_BOUNDS_CHECK))
			IF iSpectatorTarget = -1
			AND NOT bIsAnySpectator
				IF bLocalPlayerPedOK
				AND NOT IS_PLAYER_RESPAWNING(LocalPlayer)
					PRINTLN("[PROCESS_END_OF_RULE_BOUNDS_CHECK][RH] Exploding on rule end due to rule: ", iRule)
					EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL(iTeam, iRule, DEFAULT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF 
ENDPROC 

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: PROCESS MISSION BOUNDS ------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: This checks whether we're in/out of any bounds -------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_PROCESS_ANY_BOUNDS_THIS_FRAME()
	
	IF manualRespawnState > eManualRespawnState_OKAY_TO_SPAWN
		PRINTLN("[Bounds_SPAM] SHOULD_PROCESS_ANY_BOUNDS_THIS_FRAME | Returning FALSE because we're manually respawning")
		RETURN FALSE
	ENDIF

	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		PRINTLN("[Bounds_SPAM] SHOULD_PROCESS_ANY_BOUNDS_THIS_FRAME | Returning FALSE due to SBBOOL_MISSION_OVER")
		RETURN FALSE
	ENDIF
	
	IF NOT bLocalPlayerPedOk
		PRINTLN("[Bounds_SPAM] SHOULD_PROCESS_ANY_BOUNDS_THIS_FRAME | Returning FALSE due to bLocalPlayerPedOk being FALSE")
		RETURN FALSE
	ENDIF
	
	IF GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iRespawnState != RESPAWN_STATE_PLAYING
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
		PRINTLN("[Bounds_SPAM] SHOULD_PROCESS_ANY_BOUNDS_THIS_FRAME | Returning FALSE due to iRespawnState being ", GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iRespawnState)
		RETURN FALSE
	ENDIF
	
	IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState != 0
		PRINTLN("[Bounds_SPAM] SHOULD_PROCESS_ANY_BOUNDS_THIS_FRAME | Local player ped is warping!")
		RETURN FALSE
	ENDIF
	
	IF g_bMissionOver
		PRINTLN("[Bounds_SPAM] SHOULD_PROCESS_ANY_BOUNDS_THIS_FRAME | Returning FALSE due to g_bMissionOver")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(iEntityFirstStaggeredLoopComplete, ci_ZONES_FIRST_STAGGERED_LOOP_COMPLETE)
		PRINTLN("[Bounds_SPAM] SHOULD_PROCESS_ANY_BOUNDS_THIS_FRAME | Need to wait for ci_ZONES_FIRST_STAGGERED_LOOP_COMPLETE")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC COUNT_PLAYERS_IN_BOUNDS(INT iBoundsIndex)
	
	INT iRule
	INT iTeam
	INT iPart
	INT iPlayersCount[FMMC_MAX_TEAMS]
	INT iPlayersAllowedCount[FMMC_MAX_TEAMS]
	PLAYER_INDEX piPlayer = NULL
	PED_INDEX pedIndex = NULL
	PARTICIPANT_INDEX partIndex = NULL 
		
	PRINTLN("[PLAYER_LOOP] - COUNT_PLAYERS_IN_BOUNDS 2")
	FOR iPart = 0 TO NUM_NETWORK_PLAYERS-1
		partIndex = INT_TO_PARTICIPANTINDEX(iPart)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(partIndex)
			IF IS_BIT_SET(MC_serverBD_3.iPlayerBoundAllowedBitset[iBoundsIndex], iPart)
				IF MC_PlayerBD[iPart].iTeam > -1
					iPlayersAllowedCount[MC_PlayerBD[iPart].iTeam]++
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
		
	PRINTLN("[PLAYER_LOOP] - COUNT_PLAYERS_IN_BOUNDS")
	FOR iPart = 0 TO NUM_NETWORK_PLAYERS-1
		iTeam = MC_PlayerBD[iPart].iTeam
		iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule < FMMC_MAX_RULES
			IF NOT IS_PARTICIPANT_INSIDE_BOUNDS(iPart, iBoundsIndex)
				IF IS_BIT_SET(MC_serverBD_3.iPlayerBoundAllowedBitset[iBoundsIndex], iPart)
					PRINTLN("[LM][PROCESS_SERVER_COUNT_PLAYERS_IN_BOUNDS][BoundsLimiter] - iPart: ", iPart, " on Team: ", iTeam, " ||CLEARING|| Flagged as allowed in bounds 1.")
					CLEAR_BIT(MC_serverBD_3.iPlayerBoundAllowedBitset[iBoundsIndex], iPart)
				ENDIF
			ELSE
				PRINTLN("[LM][PROCESS_SERVER_COUNT_PLAYERS_IN_BOUNDS][BoundsLimiter] - iPart: ", iPart, " on Team: ", iTeam, " Is currently inside Play Area Bounds 1. Are they allowed: ", IS_BIT_SET(MC_serverBD_3.iPlayerBoundAllowedBitset[iBoundsIndex], iPart))
			ENDIF
			
			IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_HEIST_SPECTATOR)
				partIndex = INT_TO_PARTICIPANTINDEX(iPart)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(partIndex)			
					piPlayer = NETWORK_GET_PLAYER_INDEX(partIndex)		
					
					IF iRule < FMMC_MAX_RULES
						IF NOT IS_PLAYER_RESPAWNING(piPlayer)
							pedIndex = GET_PLAYER_PED(piPlayer)
							
							IF NOT IS_PED_INJURED(pedIndex)
								IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleBounds[iRule][iBoundsIndex].iMaxPlayersAllowedInBounds > 0
								AND IS_PARTICIPANT_INSIDE_BOUNDS(iPart, iBoundsIndex)
									iPlayersCount[iTeam]++
									
									IF NOT IS_BIT_SET(MC_serverBD_3.iPlayerBoundAllowedBitset[iBoundsIndex], iPart)
									AND iPlayersCount[iTeam] <= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleBounds[iRule][iBoundsIndex].iMaxPlayersAllowedInBounds
									AND iPlayersAllowedCount[iTeam] < g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleBounds[iRule][iBoundsIndex].iMaxPlayersAllowedInBounds
										PRINTLN("[LM][PROCESS_SERVER_COUNT_PLAYERS_IN_BOUNDS][BoundsLimiter] - iPart: ", iPart, " on Team: ", iTeam, " ||SETTING|| Flagged as allowed in bounds 1 (Name: ", GET_PLAYER_NAME(piPlayer), ")")
										SET_BIT(MC_serverBD_3.iPlayerBoundAllowedBitset[iBoundsIndex], iPart)
									ENDIF
								ENDIF
							ENDIF					
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		IF MC_serverBD_3.iNumberOfPlayersInRuleBounds[iTeam][iBoundsIndex] != iPlayersCount[iTeam]
			PRINTLN("[LM][PROCESS_SERVER_COUNT_PLAYERS_IN_BOUNDS][BoundsLimiter] - iTeam: ", iTeam, " Number of Players in bounds ", iBoundsIndex, ": ", MC_serverBD_3.iNumberOfPlayersInRuleBounds[iTeam][iBoundsIndex])
			MC_serverBD_3.iNumberOfPlayersInRuleBounds[iTeam][iBoundsIndex] = iPlayersCount[iTeam]
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_SERVER_COUNT_PLAYERS_IN_BOUNDS()
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_USING_LIMITED_PLAYERS_IN_BOUNDS)
		EXIT
	ENDIF
	
	INT iBoundsIndex
	FOR iBoundsIndex = 0 TO (ciMAX_RULE_BOUNDS_PER_RULE - 1)
		COUNT_PLAYERS_IN_BOUNDS(iBoundsIndex)
	ENDFOR
ENDPROC

FUNC BOOL SHOULD_FMMC_BOUNDS_CLEANUP(INT iBoundsIndex, RULE_BOUNDS_STRUCT& sBounds)
	
	IF sBounds.iRuleBoundsType = ciRULE_BOUNDS_TYPE__OFF
		PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] SHOULD_FMMC_BOUNDS_CLEANUP | These bounds are turned off!")
		RETURN TRUE
	ENDIF
	
	IF IS_LONG_BITSET_EMPTY(sBounds.iZonesToUseBS)
		PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] SHOULD_FMMC_BOUNDS_CLEANUP | These bounds don't have a zone to use!")
		RETURN TRUE
	ENDIF
	
	IF sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsActiveZoneCount = 0
		PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] SHOULD_FMMC_BOUNDS_CLEANUP | These bounds have no active Zones!")
		RETURN TRUE
	ENDIF
	
	IF GET_LOCAL_PLAYER_CURRENT_RULE() >= FMMC_MAX_RULES
	OR HAS_TEAM_FINISHED(GET_LOCAL_PLAYER_TEAM())
	OR IS_BIT_SET(MC_playerBD[GET_BOUNDS_PART_TO_USE()].iClientBitSet, PBBOOL_FINISHED)
		PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] SHOULD_FMMC_BOUNDS_CLEANUP | Finished!")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[GET_BOUNDS_PART_TO_USE()].iClientBitSet2, PBBOOL2_PLAYER_OUT_BOUNDS_FAIL)
		PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] SHOULD_FMMC_BOUNDS_CLEANUP | Failed!")
		RETURN TRUE
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] SHOULD_FMMC_BOUNDS_CLEANUP | IS_CUTSCENE_PLAYING!")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_RESPAWNING(GET_BOUNDS_PLAYER_TO_USE())
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
		PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] SHOULD_FMMC_BOUNDS_CLEANUP | Player is respawning!")
		RETURN TRUE
	ENDIF
	
	IF manualRespawnState != eManualRespawnState_OKAY_TO_SPAWN
	AND manualRespawnState != eManualRespawnState_WAITING_AFTER_RESPAWN
		PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] SHOULD_FMMC_BOUNDS_CLEANUP | Player is manually respawning!")
		RETURN TRUE
	ENDIF
	
	IF IS_PED_DEAD_OR_DYING(GET_BOUNDS_PED_TO_USE())
		PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] SHOULD_FMMC_BOUNDS_CLEANUP | GET_BOUNDS_PED_TO_USE is dead!")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iRuleBitsetFive[GET_LOCAL_PLAYER_CURRENT_RULE()], ciBS_RULE5_ACTIVATE_BOUNDS_SUDDEN_DEATH)
	AND NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] SHOULD_FMMC_BOUNDS_CLEANUP | Don't activate the bounds until sudden death!")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_CREATOR_AIRCRAFT(LocalPlayer)
		// Act as if we're in bounds
		PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] SHOULD_FMMC_BOUNDS_CLEANUP | Player is inside an aircraft interior!")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_FMMC_BOUNDS_CLEANUP(INT iBoundsIndex)
	
	IF HAS_NET_TIMER_STARTED(sRuleBoundsRuntimeVars[iBoundsIndex].stOutOfBoundsTimer)
		PRINTLN("[Bounds][RB ", iBoundsIndex, "] PROCESS_FMMC_BOUNDS_CLEANUP | Resetting OOB vars!")
		RESET_RULE_BOUNDS_OUT_OF_BOUNDS_VARS(iBoundsIndex)
	ENDIF
	
	IF sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFirstActiveZone > -1
		sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFirstActiveZone = -1
		PRINTLN("[Bounds][RB ", iBoundsIndex, "] PROCESS_FMMC_BOUNDS_CLEANUP | Clearing sRuleBoundsRuntimeVars[", iBoundsIndex, "].iRuleBoundsFirstActiveZone!")
	ENDIF
	
	PROCESS_REMOVING_OOB_BLIPS_STAGGERED(iBoundsIndex, TRUE)
	
ENDPROC

PROC KILL_PLAYER_ON_BOUNDS_FAIL()

	IF HAS_LOCAL_PLAYER_FINISHED_OR_REACHED_END()
		EXIT	
	ENDIF
	
	IF g_FMMC_STRUCT.iShowPackageHudForTeam != 0
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciNEGATIVE_SUICIDE_SCORE)
		
		//Negative score
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciNEGATIVE_SUICIDE_SCORE)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciNEGATIVE_SUICIDE_FALL_IS_SUICIDE)
			IF MC_playerBD[iPartToUse].iNumPlayerKills > 0
				MC_playerBD[iPartToUse].iNumPlayerKills--
				PRINTLN("[PROCESS_MISSION_BOUNDS][JS] - SUICIDE POINTS - decrementing local player's kills")
			ENDIF
		
			IF MC_serverBD.iNumPlayerRuleHighestPriority[GET_LOCAL_PLAYER_TEAM()] > 0 
				PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - more than 0 highest ") 
				IF MC_serverBD_4.iPlayerRuleMissionLogic[GET_LOCAL_PLAYER_TEAM()] = FMMC_OBJECTIVE_LOGIC_KILL_PLAYERS // this high priority kill player objective
					IF MC_playerBD[iPartToUse].iPlayerScore > 0
						REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(-1)															
					ENDIF
				ELIF MC_serverBD_4.iPlayerRuleMissionLogic[GET_LOCAL_PLAYER_TEAM()] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM0	
				OR MC_serverBD_4.iPlayerRuleMissionLogic[GET_LOCAL_PLAYER_TEAM()] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM1	
				OR MC_serverBD_4.iPlayerRuleMissionLogic[GET_LOCAL_PLAYER_TEAM()] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM2
				OR MC_serverBD_4.iPlayerRuleMissionLogic[GET_LOCAL_PLAYER_TEAM()] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM3
					IF MC_playerBD[iPartToUse].iPlayerScore > 0
					AND MC_Playerbd[iPartToUse].iKillScore > 0
						PRINTLN("[PROCESS_MISSION_BOUNDS][JS] - SUICIDE POINTS - decrementing local player's score")
						REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(-1,TRUE)															
					ENDIF
				ENDIF	
			ELSE
				IF g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iPointsPerKill > 0
					IF MC_playerBD[iPartToUse].iPlayerScore >= g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iPointsPerKill
						PRINTLN("[PROCESS_MISSION_BOUNDS][JS] - SUICIDE POINTS - iPoints per kill = ", g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iPointsPerKill)
						REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_BY((g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iPointsPerKill * -1),TRUE)															
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	BROADCAST_FMMC_PLAYER_COMMITED_SUICIDE(GET_LOCAL_PLAYER_TEAM())
	SET_ENTITY_HEALTH(LocalPlayerPed, 0)
ENDPROC


PROC PROCESS_FMMC_OUT_OF_BOUNDS_FAIL(INT iBoundsIndex, RULE_BOUNDS_STRUCT& sBounds)
	
	UNUSED_PARAMETER(iBoundsIndex) // For building on Release
	
	IF bIsAnySpectator
	OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet4, PBBOOL4_MISSION_CURRENT_OBJECTIVE_FAILED_BY_BOUNDS)	
		EXIT
	ENDIF
	
	PRINTLN("[Bounds][RB ", iBoundsIndex, "] PROCESS_FMMC_OUT_OF_BOUNDS_FAIL!!")
	CLEAR_ENTITY_LAST_DAMAGE_ENTITY(LocalPlayerPed)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iRuleBitsetFourteen[GET_LOCAL_PLAYER_CURRENT_RULE()], ciBS_RULE14_BOUNDS_FAIL_OBJECTIVE_INSTEAD_OF_MISSION)	
		PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] PROCESS_FMMC_OUT_OF_BOUNDS_FAIL | ciBS_RULE14_BOUNDS_FAIL_OBJECTIVE_INSTEAD_OF_MISSION")
		SET_BIT(MC_playerBD[iPartToUse].iClientBitSet4, PBBOOL4_MISSION_CURRENT_OBJECTIVE_FAILED_BY_BOUNDS)
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_RequestFailRule, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, GET_LOCAL_PLAYER_CURRENT_RULE())
	
	ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iRuleBitsetFour[GET_LOCAL_PLAYER_CURRENT_RULE()], ciBS_RULE4_KILL_PLAYER_ON_BOUNDS_FAIL)
		
		PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] PROCESS_FMMC_OUT_OF_BOUNDS_FAIL | ciBS_RULE4_KILL_PLAYER_ON_BOUNDS_FAIL")
		
		IF IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
			EXIT
		ENDIF
		
		IF bLocalPlayerPedOK
		AND NOT IS_PLAYER_RESPAWNING(LocalPlayer)
			IF SHOULD_EXPLODE_ON_BOUNDS_FAIL(GET_LOCAL_PLAYER_TEAM(), GET_LOCAL_PLAYER_CURRENT_RULE())
				EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL(GET_LOCAL_PLAYER_TEAM(), GET_LOCAL_PLAYER_CURRENT_RULE())
			ELSE
				KILL_PLAYER_ON_BOUNDS_FAIL()
			ENDIF
		ENDIF
		
	ELSE
	
		PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] PROCESS_FMMC_OUT_OF_BOUNDS_FAIL | Failing the player now")
		SET_BIT(MC_playerBD[iPartToUse].iClientBitSet4, PBBOOL4_MISSION_CURRENT_OBJECTIVE_FAILED_BY_BOUNDS)
		
		IF NOT IS_BIT_SET(MC_playerBD[GET_BOUNDS_PART_TO_USE()].iClientBitSet, PBBOOL_PLAYER_FAIL)
			IF sBounds.iRuleBoundsType = ciRULE_BOUNDS_TYPE__LEAVE_AREA
				PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] Setting PBBOOL2_PLAYER_LEAVE_AREA_FAIL")
				SET_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_PLAYER_LEAVE_AREA_FAIL)
			ELSE
				PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] Setting PBBOOL2_PLAYER_OUT_BOUNDS_FAIL")					
				SET_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_PLAYER_OUT_BOUNDS_FAIL)
			ENDIF
			
			SET_LOCAL_PLAYER_FAILED(2)
			MC_playerBD[iPartToUse].iReasonForPlayerFail = PLAYER_FAIL_REASON_BOUNDS
			EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL(GET_LOCAL_PLAYER_TEAM(), GET_LOCAL_PLAYER_CURRENT_RULE(), TRUE)
		ENDIF
		
	ENDIF
ENDPROC

FUNC BLIP_INDEX CREATE_OUT_OF_BOUNDS_BLIP(INT iBoundsIndex, INT iZoneIndex)

	BLIP_INDEX biOOBBlip
	
	SWITCH g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].sRuleBounds[GET_LOCAL_PLAYER_CURRENT_RULE()][iBoundsIndex].iBoundsBlipStyle
		CASE ciRULE_BOUNDS_BLIP_STYLE__AVERAGE_POSITION
			biOOBBlip = ADD_BLIP_FOR_COORD(GET_BOUNDS_RUNTIME_CENTRE(iBoundsIndex))
		BREAK

		CASE ciRULE_BOUNDS_BLIP_STYLE__SEPARATE_BLIP_FOR_EACH_ZONE
			biOOBBlip = ADD_BLIP_FOR_COORD(GET_ZONE_RUNTIME_CENTRE(iZoneIndex))
		BREAK
		
		CASE ciRULE_BOUNDS_BLIP_STYLE__CLOSEST_ZONE_ONLY
			biOOBBlip = ADD_BLIP_FOR_COORD(GET_ZONE_RUNTIME_CENTRE(iZoneIndex))
		BREAK
	ENDSWITCH

	SET_BLIP_PRIORITY(biOOBBlip, BLIPPRIORITY_HIGH_HIGHEST)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iRuleBitsetNine[GET_LOCAL_PLAYER_CURRENT_RULE()], ciBS_RULE9_BOUNDS_DISABLE_RADAR_EDGE_BLIP)
		SET_BLIP_AS_SHORT_RANGE(biOOBBlip, TRUE)
	ENDIF
	
	RETURN biOOBBlip
ENDFUNC

FUNC BOOL SHOULD_BOUNDS_SHOW_OOB_BLIP_WHEN_OUT_OF_BOUNDS(RULE_BOUNDS_STRUCT& sBounds)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iRuleBitset[GET_LOCAL_PLAYER_CURRENT_RULE()], ciBS_RULE_HIDE_BLIP_ON_BOUNDS_FAIL)
		// OOB Blip disabled for Bounds globally on this Rule
		RETURN FALSE
	ENDIF
	
	UNUSED_PARAMETER(sBounds) // For a future per-bounds setting
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_THIS_BOUNDS_ZONE_HAVE_OOB_BLIP(INT iBoundsIndex, RULE_BOUNDS_STRUCT& sBounds, INT iBoundsZoneIndex)

	IF NOT IS_THIS_ZONE_SET_AS_BOUNDS_ON_CURRENT_RULE(iBoundsZoneIndex, iBoundsIndex)
		//PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "][Zone_SPAM ", iBoundsZoneIndex, "] SHOULD_THIS_BOUNDS_ZONE_HAVE_OOB_BLIP - Returning FALSE due to IS_THIS_ZONE_SET_AS_BOUNDS_ON_CURRENT_RULE")
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ZONE_EXIST(iBoundsZoneIndex)
		//PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "][Zone_SPAM ", iBoundsZoneIndex, "] SHOULD_THIS_BOUNDS_ZONE_HAVE_OOB_BLIP - Returning FALSE because zone ", iBoundsZoneIndex, " doesn't exist")
		RETURN FALSE
	ENDIF
	
	IF sBounds.iBoundsBlipStyle = ciRULE_BOUNDS_BLIP_STYLE__AVERAGE_POSITION
		IF iBoundsZoneIndex = GET_BOUNDS_FIRST_ACTIVE_ZONE(iBoundsIndex)
			//PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "][Zone_SPAM ", iBoundsZoneIndex, "] SHOULD_THIS_BOUNDS_ZONE_HAVE_OOB_BLIP - Returning TRUE because this (", iBoundsZoneIndex, ") is GET_BOUNDS_FIRST_ACTIVE_ZONE(iBoundsIndex)")
			RETURN TRUE
		//ELSE
			//PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "][Zone_SPAM ", iBoundsZoneIndex, "] SHOULD_THIS_BOUNDS_ZONE_HAVE_OOB_BLIP - This isn't iRuleBoundsFirstActiveZone")
		ENDIF
		
	ELIF sBounds.iBoundsBlipStyle = ciRULE_BOUNDS_BLIP_STYLE__SEPARATE_BLIP_FOR_EACH_ZONE
		RETURN TRUE
		
	ELIF sBounds.iBoundsBlipStyle = ciRULE_BOUNDS_BLIP_STYLE__CLOSEST_ZONE_ONLY
		IF iBoundsZoneIndex = GET_CLOSEST_OOB_BLIP_INDEX(iBoundsIndex)
			//PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "][Zone_SPAM ", iBoundsZoneIndex, "] SHOULD_THIS_BOUNDS_ZONE_HAVE_OOB_BLIP - Returning TRUE because iBoundsZoneIndex = GET_CLOSEST_OOB_BLIP_INDEX")
			RETURN TRUE
		//ELSE
			//PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "][Zone_SPAM ", iBoundsZoneIndex, "] SHOULD_THIS_BOUNDS_ZONE_HAVE_OOB_BLIP - GET_CLOSEST_OOB_BLIP_INDEX(iBoundsIndex): ", GET_CLOSEST_OOB_BLIP_INDEX(iBoundsIndex))
		ENDIF
	ENDIF
	
	//PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "][Zone_SPAM ", iBoundsZoneIndex, "] SHOULD_THIS_BOUNDS_ZONE_HAVE_OOB_BLIP - Returning FALSE")
	RETURN FALSE
ENDFUNC

// Proceses the circular blip which is created at the centre of bounds while out of bounds, NOT the area/radius blip, which is handled by the Bounds Zones blipping themselves the usual Zones way.
PROC PROCESS_OUT_OF_BOUNDS_BLIPS_STAGGERED(INT iBoundsIndex, RULE_BOUNDS_STRUCT& sBounds)
	
	INT iOOBBlipIndexToUse = sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlipStaggeredIndex
	BLIP_INDEX biBlipToUse = sRuleBoundsRuntimeVars[iBoundsIndex].biOutOfBoundsBlips[iOOBBlipIndexToUse]
	
	IF NOT SHOULD_BOUNDS_SHOW_OOB_BLIP_WHEN_OUT_OF_BOUNDS(sBounds)
		IF DOES_BLIP_EXIST(biBlipToUse)
			PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "][OOB Blip ", iOOBBlipIndexToUse, "] PROCESS_OUT_OF_BOUNDS_BLIPS_STAGGERED - Removing OOB blip. OOB blips are disabled for this rule due to SHOULD_BOUNDS_SHOW_OOB_BLIP_WHEN_OUT_OF_BOUNDS")
			REMOVE_OUT_OF_BOUNDS_BLIP(iBoundsIndex, iOOBBlipIndexToUse)
		ENDIF
		
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iOOBBlipIndexToUse].iZoneBS2, ciFMMC_ZONEBS2_BOUNDS_DISABLE_OOB_BLIP_FOR_ZONE)
		EXIT
	ENDIF
	
	IF IS_THIS_ZONE_SET_AS_BOUNDS_ON_CURRENT_RULE(iOOBBlipIndexToUse, iBoundsIndex)
	AND DOES_ZONE_EXIST(iOOBBlipIndexToUse)
		FLOAT fDistanceToThisBoundsZone = VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), GET_ZONE_RUNTIME_CENTRE(iOOBBlipIndexToUse))
		IF fDistanceToThisBoundsZone < sRuleBoundsRuntimeVars[iBoundsIndex].fOutOfBoundsBlip_CurrentClosestDistance
			sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlip_CurrentClosestBlipIndex_Temp = iOOBBlipIndexToUse
			sRuleBoundsRuntimeVars[iBoundsIndex].fOutOfBoundsBlip_CurrentClosestDistance = fDistanceToThisBoundsZone
			PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "][OOB Blip ", iOOBBlipIndexToUse, "] PROCESS_OUT_OF_BOUNDS_BLIPS_STAGGERED - Setting sRuleBoundsRuntimeVars[", iBoundsIndex, "].iOutOfBoundsBlip_CurrentClosestBlipIndex_Temp to ", sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlip_CurrentClosestBlipIndex_Temp)
		ENDIF
	ENDIF
	
	IF SHOULD_THIS_BOUNDS_ZONE_HAVE_OOB_BLIP(iBoundsIndex, sBounds, iOOBBlipIndexToUse)
		IF NOT DOES_BLIP_EXIST(biBlipToUse)
			PRINTLN("[Bounds][RuleBounds ", iBoundsIndex, "][OOB Blip ", iOOBBlipIndexToUse, "] PROCESS_OUT_OF_BOUNDS_BLIPS_STAGGERED - Creating OOB blip ", iOOBBlipIndexToUse)
			sRuleBoundsRuntimeVars[iBoundsIndex].biOutOfBoundsBlips[iOOBBlipIndexToUse] = CREATE_OUT_OF_BOUNDS_BLIP(iBoundsIndex, iOOBBlipIndexToUse)
			sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlipsCreated++
			HANDLE_BOUNDS_OOB_BLIP_HEIGHT_THRESHOLD(sBounds, biBlipToUse)
			HANDLE_BOUNDS_OOB_BLIP_COLOUR(iBoundsIndex, biBlipToUse)
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(biBlipToUse)
			PRINTLN("[Bounds][RuleBounds ", iBoundsIndex, "][OOB Blip ", iOOBBlipIndexToUse, "] PROCESS_OUT_OF_BOUNDS_BLIPS_STAGGERED - Removing OOB blip ", iOOBBlipIndexToUse)
			REMOVE_OUT_OF_BOUNDS_BLIP(iBoundsIndex, iOOBBlipIndexToUse)
		ENDIF
	ENDIF
	
	IF DOES_BLIP_EXIST(biBlipToUse)
		IF SHOULD_OOB_BOUNDS_BLIP_HAVE_GPS(iBoundsIndex, sBounds, iOOBBlipIndexToUse)
			IF sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlipUsingGPS = -1
				SET_BLIP_ROUTE(biBlipToUse, TRUE)
				SET_FORCE_SHOW_GPS(TRUE)
				sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlipUsingGPS = iOOBBlipIndexToUse
				PRINTLN("[Bounds][RuleBounds ", iBoundsIndex, "][OOB Blip ", iOOBBlipIndexToUse, "] PROCESS_OUT_OF_BOUNDS_BLIPS_STAGGERED - Setting up GPS for bounds blip")
			ENDIF
		ELSE
			IF sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlipUsingGPS = iOOBBlipIndexToUse
				SET_BLIP_ROUTE(biBlipToUse, FALSE)
				SET_FORCE_SHOW_GPS(FALSE)
				sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlipUsingGPS = -1
				PRINTLN("[Bounds][RuleBounds ", iBoundsIndex, "][OOB Blip ", iOOBBlipIndexToUse, "] PROCESS_OUT_OF_BOUNDS_BLIPS_STAGGERED - Clearing GPS for bounds blip")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_OUT_OF_BOUNDS_BLIPS_EVERY_FRAME(INT iBoundsIndex)
	
	INT iZoneToUse = sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFirstActiveZone
	
	IF iZoneToUse > -1
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneToUse].iZoneBS, ciFMMC_ZONEBS_ATTACH_ZONE_TO_LINKED_ENTITY)
		// Process moving blips for Bounds Zones that move
		IF DOES_BLIP_EXIST(sRuleBoundsRuntimeVars[iBoundsIndex].biOutOfBoundsBlips[iZoneToUse])
			SET_BLIP_COORDS(sRuleBoundsRuntimeVars[iBoundsIndex].biOutOfBoundsBlips[iZoneToUse], GET_ZONE_RUNTIME_CENTRE(iZoneToUse))
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_OUT_OF_BOUNDS_ON_SCREEN_TIMER(INT iBoundsIndex, RULE_BOUNDS_STRUCT& sBounds)
	
	INT iTeam = GET_LOCAL_PLAYER_TEAM()
	INT iRule = GET_LOCAL_PLAYER_CURRENT_RULE()	
	
	IF NOT DO_THESE_BOUNDS_HAVE_A_TIME_LIMIT(sBounds, TRUE)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_FORCE_BOUNDS_TIMER)
		IF IS_SPECTATOR_HUD_HIDDEN()
		OR IS_SPECTATOR_SHOWING_NEWS_HUD()
		OR SHOULD_HIDE_THE_HUD_THIS_FRAME()
			EXIT
		ENDIF
	ENDIF
	
	TEXT_LABEL_15 sBoundsTimerLabel = "MC_MBF"
	HUDORDER eHudOrder = HUDORDER_TOP
	HUD_COLOURS eHudColour = HUD_COLOUR_WHITE
	INT ms = GET_BOUNDS_TIME_REMAINING(iBoundsIndex, sBounds)
	
	IF ms < 0
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_OUT_OF_RANGE_SHARD)
		sBoundsTimerLabel = "BM_OO_RANGE"
	ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_KILL_PLAYER_ON_BOUNDS_FAIL)
		sBoundsTimerLabel = "BM_OO_BOUNDS"
	ENDIF
	
	IF ms < 30000
		eHudColour = HUD_COLOUR_RED
	ENDIF
						
	IF GET_BITS_IN_RANGE(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_DRAW_OUT_OF_BOUNDS_TIMER_LIST_T1, ciBS_RULE5_DRAW_OUT_OF_BOUNDS_TIMER_LIST_T4) = 0
		DRAW_GENERIC_TIMER(ms, sBoundsTimerLabel, 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, eHudOrder, FALSE, eHudColour, HUDFLASHING_NONE, 0, FALSE, eHudColour)
	ENDIF
	
ENDPROC

PROC PROCESS_OUT_OF_BOUNDS_SHARD(INT iBoundsIndex, RULE_BOUNDS_STRUCT& sBounds)
	
	IF NOT IS_BIT_SET(sBounds.iBoundsBS, ciBounds_ShowOOBShard)
		EXIT
	ENDIF
	
	INT iTeam = GET_LOCAL_PLAYER_TEAM()
	INT iRule = GET_LOCAL_PLAYER_CURRENT_RULE()
	INT iR, iG, iB, iA
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2, ciBS2_STOP_GO_HUD)
	OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciOPTIONSBS17_ONESHOT_BOUNDS_SHARD) AND IS_BIT_SET(iSharedRuleBoundsBS, ciSHARED_RULE_BOUNDS_BS__DISPLAYING_BIG_MESSAGE))
		EXIT
	ENDIF

	TEXT_LABEL_63 tlCustomShardText = GET_BOUNDS_OOB_STRING(sBounds)
	BIG_MESSAGE_TYPE eBoundsMessageType
	
	SWITCH sBounds.iRuleBoundsType
		CASE ciRULE_BOUNDS_TYPE__PLAY_AREA
			eBoundsMessageType = BIG_MESSAGE_OO_BOUNDS
		BREAK
		
		CASE ciRULE_BOUNDS_TYPE__LEAVE_AREA
			eBoundsMessageType = BIG_MESSAGE_LEAVE_AREA
		BREAK
	ENDSWITCH
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_WRONG_WAY_SHARD)
		tlCustomShardText = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63WrongWayMessage[iRule]
	ENDIF
	
	IF NOT IS_BIT_SET(iSharedRuleBoundsBS, ciSHARED_RULE_BOUNDS_BS__DISPLAYING_BIG_MESSAGE)
		
		PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] PROCESS_OUT_OF_BOUNDS_SHARD - Displaying shard of type ", eBoundsMessageType, " with custom text: ", tlCustomShardText)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(tlCustomShardText)
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_OVERRIDE_FRIENDLY_WITH_GANG)
				GET_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, GET_BOUNDS_PLAYER_TO_USE()), iR, iG, iB, iA)
				SET_SCRIPT_VARIABLE_HUD_COLOUR(iR, iG, iB, iA)
				PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] PROCESS_OUT_OF_BOUNDS_SHARD - PRE change: ", tlCustomShardText)
				REPLACE_STRING_IN_STRING(tlCustomShardText, "~f~", "~v~")
				PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] PROCESS_OUT_OF_BOUNDS_SHARD - POST change: ", tlCustomShardText, " ~v~ colour: ", iR, ", ", iG, ", ", iB, ", ", iA)
			ENDIF
			
			//"MC_LTS_OBJ_C" is a totally empty string, made up entirely of the literal component (tlCustomShardText) passed in below
			SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(eBoundsMessageType, "MC_LTS_OBJ_C", tlCustomShardText, DEFAULT, 999999)
		ELSE
			SETUP_NEW_BIG_MESSAGE(eBoundsMessageType, DEFAULT, DEFAULT, DEFAULT, 999999)
		ENDIF
		
		SET_BIT(iSharedRuleBoundsBS, ciSHARED_RULE_BOUNDS_BS__DISPLAYING_BIG_MESSAGE) // Set the shared bit to say that a set of Bounds is displaying a shard
		SET_BIT(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsBS, ciRULE_BOUNDS_BS__DISPLAYING_BIG_MESSAGE) // Set this bit to say that this particular set of bounds (iBoundsIndex) is the one displaying the shard
	ENDIF
ENDPROC

PROC PROCESS_OUT_OF_BOUNDS_SOUNDS(INT iBoundsIndex, RULE_BOUNDS_STRUCT& sBounds)
	
	FLOAT fSeconds = GET_BOUNDS_TIME_REMAINING(iBoundsIndex, sBounds) / 1000.0
	
	IF DO_THESE_BOUNDS_HAVE_A_TIME_LIMIT(sBounds, TRUE)
	AND NOT IS_SOUND_ID_VALID(iBoundsTimerSound)
		iBoundsTimerSound = GET_SOUND_ID()
		
		IF SHOULD_EXPLODE_ON_BOUNDS_FAIL(GET_LOCAL_PLAYER_TEAM(), GET_LOCAL_PLAYER_CURRENT_RULE())
			PLAY_SOUND_FROM_ENTITY(iBoundsTimerSound, "Explosion_Timer", PlayerPedToUse, "DLC_Lowrider_Relay_Race_Sounds", TRUE, 20)
		ELSE
			PLAY_SOUND_FRONTEND(iBoundsTimerSound, "Out_of_Bounds", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
		ENDIF
		
		SET_VARIABLE_ON_SOUND(iBoundsTimerSound, "Time", fSeconds)
	ENDIF
ENDPROC

PROC PROCESS_OUT_OF_BOUNDS_OBJECTIVE_TEXT(INT iBoundsIndex, RULE_BOUNDS_STRUCT& sBounds)

	IF NOT IS_BIT_SET(sBounds.iBoundsBS, ciBounds_ShowOOBObjectiveText)
		EXIT
	ENDIF
	
	IF iBoundsToShowObjectiveText = -1
		
		TEXT_LABEL_63 tl63CustomBoundsMessageToUse = GET_BOUNDS_OOB_STRING(sBounds)
		
		IF IS_STRING_NULL_OR_EMPTY(tl63CustomBoundsMessageToUse)
			// Default "Out of Bounds" message
			tlOutOfBoundsObjectiveText = GET_FILENAME_FOR_AUDIO_CONVERSATION("MC_RTN_OBJM")
		ELSE
			tlOutOfBoundsObjectiveText = tl63CustomBoundsMessageToUse
		ENDIF
		
		iBoundsToShowObjectiveText = iBoundsIndex
		PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] SET_BOUNDS_TO_SHOW_OBJECTIVE_TEXT | Setting iBoundsToShowObjectiveText to ", iBoundsToShowObjectiveText, " and tlOutOfBoundsObjectiveText to ", tlOutOfBoundsObjectiveText)
	ENDIF
ENDPROC

PROC PROCESS_OUT_OF_BOUNDS_TIMER(INT iBoundsIndex, RULE_BOUNDS_STRUCT& sBounds)

	IF NOT DO_THESE_BOUNDS_HAVE_A_TIME_LIMIT(sBounds)
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(sRuleBoundsRuntimeVars[iBoundsIndex].stOutOfBoundsTimer)
		REINIT_NET_TIMER(sRuleBoundsRuntimeVars[iBoundsIndex].stOutOfBoundsTimer)
		PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] PROCESS_FMMC_OUT_OF_BOUNDS | Starting stOutOfBoundsTimer")
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(sRuleBoundsRuntimeVars[iBoundsIndex].stOutOfBoundsTimer, GET_BOUNDS_TIME_LIMIT(sBounds))
		// Bounds timer has expired!
		PROCESS_FMMC_OUT_OF_BOUNDS_FAIL(iBoundsIndex, sBounds)
	ELSE
		// Waiting for bounds timer...
	ENDIF
ENDPROC

PROC PROCESS_FMMC_OUT_OF_BOUNDS(INT iBoundsIndex, RULE_BOUNDS_STRUCT& sBounds)
	
	IF NOT IS_BIT_SET(sBounds.iBoundsBS, ciBounds_DontBlockObjectiveOutOfBounds)
		SET_BIT(iSharedRuleBoundsBS, ciSHARED_RULE_BOUNDS_BS__BLOCK_OBJECTIVE_THIS_FRAME)
	ENDIF
	
	PROCESS_OUT_OF_BOUNDS_BLIPS_STAGGERED(iBoundsIndex, sBounds)
	PROCESS_OUT_OF_BOUNDS_BLIPS_EVERY_FRAME(iBoundsIndex)
	PROCESS_OUT_OF_BOUNDS_ON_SCREEN_TIMER(iBoundsIndex, sBounds)
	PROCESS_OUT_OF_BOUNDS_SOUNDS(iBoundsIndex, sBounds)
	PROCESS_OUT_OF_BOUNDS_OBJECTIVE_TEXT(iBoundsIndex, sBounds)
	PROCESS_OUT_OF_BOUNDS_SHARD(iBoundsIndex, sBounds)
	PROCESS_OUT_OF_BOUNDS_TIMER(iBoundsIndex, sBounds)
	
ENDPROC

PROC PROCESS_FMMC_WITHIN_BOUNDS(INT iBoundsIndex, RULE_BOUNDS_STRUCT& sBounds)
	
	PROCESS_BOUNDS_VEHICLE_REGEN(iBoundsIndex, sBounds)
	PROCESS_REMOVING_OOB_BLIPS_STAGGERED(iBoundsIndex)
ENDPROC

FUNC BOOL SHOULD_LOCAL_PLAYER_BE_CONSIDERED_OUT_OF_BOUNDS(INT iBoundsIndex, RULE_BOUNDS_STRUCT& sBounds)

	IF IS_ANYONE_ON_NEEDED_TEAM_IN_BOUNDS(iBoundsIndex)
		// Someone on one of the specified teams is in the bounds, so we're fine even if we're personally out of bounds
		PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] SHOULD_LOCAL_PLAYER_BE_CONSIDERED_OUT_OF_BOUNDS | IS_ANYONE_ON_NEEDED_TEAM_IN_BOUNDS")
		RETURN FALSE
	ENDIF
	
	IF sBounds.MNIgnoreLeaveAreaVeh != DUMMY_MODEL_FOR_SCRIPT
		IF IS_PED_IN_ANY_VEHICLE(playerPedToUse)
			IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(playerPedToUse)) = sBounds.MNIgnoreLeaveAreaVeh
				PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] SHOULD_LOCAL_PLAYER_BE_CONSIDERED_OUT_OF_BOUNDS | Local player is in vehicle exempt from being out of bounds")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	IF sBounds.mnOnlyAffectVeh != DUMMY_MODEL_FOR_SCRIPT
		IF IS_PED_IN_ANY_VEHICLE(playerPedToUse)
			IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(playerPedToUse)) != sBounds.mnOnlyAffectVeh
				PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] SHOULD_LOCAL_PLAYER_BE_CONSIDERED_OUT_OF_BOUNDS | Local player is not in a vehicle that the bounds care about")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF ARE_BOUNDS_ACTIVE(iBoundsIndex)
		IF sBounds.iRuleBoundsType = ciRULE_BOUNDS_TYPE__PLAY_AREA
			IF sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsCurrentTriggeredZoneCount = 0 // Player isn't in any relevant Zones
				PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] SHOULD_LOCAL_PLAYER_BE_CONSIDERED_OUT_OF_BOUNDS | Local player isn't in a Bounds Zone that these bounds care about")
				RETURN TRUE
			ENDIF
			
		ELIF sBounds.iRuleBoundsType = ciRULE_BOUNDS_TYPE__LEAVE_AREA
			IF sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsCurrentTriggeredZoneCount > 0 // Player is in at least one of the relevant Zones
				PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] SHOULD_LOCAL_PLAYER_BE_CONSIDERED_OUT_OF_BOUNDS | Local player is in a Leave Area")
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		// A Bounds Zone hasn't been created yet.
		PRINTLN("[Bounds_SPAM][SPAM_RuleBounds ", iBoundsIndex, "] SHOULD_LOCAL_PLAYER_BE_CONSIDERED_OUT_OF_BOUNDS | There aren't any active bounds!")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_THERE_TOO_MANY_PLAYERS_IN_BOUNDS(INT iBoundsIndex, RULE_BOUNDS_STRUCT& sBounds)

	IF sBounds.iMaxPlayersAllowedInBounds <= 0
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD_3.iPlayerBoundAllowedBitset[iBoundsIndex], iLocalPart)
		RETURN FALSE
	ENDIF

	IF NOT IS_LOCAL_PLAYER_OUT_OF_BOUNDS(iBoundsIndex)
	AND MC_serverBD_3.iNumberOfPlayersInRuleBounds[GET_LOCAL_PLAYER_TEAM()][iBoundsIndex] > sBounds.iMaxPlayersAllowedInBounds
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_TOO_MANY_PLAYERS_IN_BOUNDS(INT iBoundsIndex)
	
	IF ARE_THERE_TOO_MANY_PLAYERS_IN_BOUNDS(iBoundsIndex)
	AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
	AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_HEIST_SPECTATOR)
	AND bLocalPlayerPedOK
		IF NOT HAS_NET_TIMER_STARTED(tdTooManyPlayersInBoundsTimer)
			PRINTLN("[LM][PROCESS_MISSION_BOUNDS][BoundsLimiter] - Too many players in bounds, and we are not flagged as allowed!!")
			START_NET_TIMER(tdTooManyPlayersInBoundsTimer)		
		ENDIF
		
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdTooManyPlayersInBoundsTimer, ci_START_WARNING_FOR_TOO_MANY_PLAYERS_INBOUNDS)
			PRINTLN("[LM][PROCESS_MISSION_BOUNDS][BoundsLimiter] - Too many players in bounds, start the shard (", ci_START_WARNING_FOR_TOO_MANY_PLAYERS_INBOUNDS, "sec passed)")				
			IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_MIDSIZED)
				SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_GENERIC_MIDSIZED, "", "", DEFAULT, 999999, "MC_LVE_OBJC")
			ENDIF
			
			INT ms = (ci_EXPLODE_FOR_TOO_MANY_PLAYERS_INBOUNDS - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdTooManyPlayersInBoundsTimer))
			HUD_COLOURS eHudColour = HUD_COLOUR_WHITE
			
			IF ms < 30000
				eHudColour = HUD_COLOUR_RED
			ENDIF
			
			DRAW_GENERIC_TIMER(ms, "MC_LVE_OBJC", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_TOP, FALSE, eHudColour, HUDFLASHING_FLASHRED, 0, FALSE, eHudColour)
				
			IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdTooManyPlayersInBoundsTimer, ci_EXPLODE_FOR_TOO_MANY_PLAYERS_INBOUNDS)
				PRINTLN("[LM][PROCESS_MISSION_BOUNDS][BoundsLimiter] - Too many players in bounds, Explode (", ci_EXPLODE_FOR_TOO_MANY_PLAYERS_INBOUNDS, " sec passed)")
				EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL(GET_LOCAL_PLAYER_TEAM(), GET_LOCAL_PLAYER_CURRENT_RULE(), DEFAULT, TRUE)
				RESET_NET_TIMER(tdTooManyPlayersInBoundsTimer)
											
				IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_MIDSIZED)
					CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_MIDSIZED)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(tdTooManyPlayersInBoundsTimer)
			PRINTLN("[LM][PROCESS_MISSION_BOUNDS][BoundsLimiter] - tdTooManyPlayersInBoundsTimer Reset Net Timer")
			RESET_NET_TIMER(tdTooManyPlayersInBoundsTimer)
			IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_MIDSIZED)
				CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_MIDSIZED)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_CHECK_OOB_FOR_THESE_BOUNDS_THIS_FRAME(INT iBoundsIndex)
	
	IF IS_BIT_SET(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsBS, ciRULE_BOUNDS_BS__WAIT_FOR_NEXT_COMPLETED_ZONE_STAGGERED_LOOP)
		// Waiting for a Zones staggered loop to complete
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_FMMC_RULE_BOUNDS_EVERY_FRAME(INT iBoundsIndex, RULE_BOUNDS_STRUCT& sBounds)
	
	#IF IS_DEBUG_BUILD
	DRAW_BOUNDS_DEBUG(iBoundsIndex, sBounds)
	#ENDIF
	
	IF IS_BIT_SET(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsBS, ciRULE_BOUNDS_BS__BLOCK_BOUNDS_EVERY_FRAME_PROCESSING)
	AND NOT HAS_NET_TIMER_STARTED(sRuleBoundsRuntimeVars[iBoundsIndex].stOutOfBoundsTimer) // If the timer is on-screen and sounds are already playing etc, let them continue. Exiting here is just to prevent the player "becoming" out of bounds/back in bounds, more than anything else
		PRINTLN("[Bounds_SPAM][RuleBounds_SPAM ", iBoundsIndex, "] PROCESS_FMMC_RULE_BOUNDS_EVERY_FRAME | Every-Frame processing is blocked right now")
		EXIT
	ENDIF
	
	IF SHOULD_RULE_BOUNDS_WAIT_FOR_FULL_STAGGERED_LOOP(iBoundsIndex)
		SET_RULE_BOUNDS_TO_WAIT_FOR_FULL_STAGGERED_LOOP(iBoundsIndex)
	ENDIF
	
	IF SHOULD_FMMC_BOUNDS_CLEANUP(iBoundsIndex, sBounds)
		PROCESS_FMMC_BOUNDS_CLEANUP(iBoundsIndex)
		EXIT
	ENDIF
	
	IF NOT SHOULD_PROCESS_ANY_BOUNDS_THIS_FRAME()
		EXIT
	ENDIF
	
	IF IS_LOCAL_PLAYER_OUT_OF_BOUNDS(iBoundsIndex)
		PROCESS_FMMC_OUT_OF_BOUNDS(iBoundsIndex, sBounds)
	ELSE
		PROCESS_FMMC_WITHIN_BOUNDS(iBoundsIndex, sBounds)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iRuleBitsetEleven[GET_LOCAL_PLAYER_CURRENT_RULE()], ciBS_RULE11_EXPLODE_ON_RULE_END_BOUNDS_FAIL) 
		IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
			SET_BIT(iLocalBoolCheck28,LBBOOL28_HAS_DONE_SUDDEN_DEATH_BOUNDS_CHECK)
		ENDIF
	ENDIF
	
	sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlipStaggeredIndex++
	IF sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlipStaggeredIndex >= FMMC_MAX_NUM_ZONES
	
		IF sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlip_CurrentClosestBlipIndex != sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlip_CurrentClosestBlipIndex_Temp
			sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlip_CurrentClosestBlipIndex = sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlip_CurrentClosestBlipIndex_Temp
			PRINTLN("[Bounds_SPAM][RuleBounds_SPAM ", iBoundsIndex, "] PROCESS_FMMC_RULE_BOUNDS_EVERY_FRAME | Setting iOutOfBoundsBlip_CurrentClosestBlipIndex to ", sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlip_CurrentClosestBlipIndex)
		ENDIF
		
		sRuleBoundsRuntimeVars[iBoundsIndex].fOutOfBoundsBlip_CurrentClosestDistance = cfRULE_BOUNDS_MAX_BLIP_DISTANCE
		sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlip_CurrentClosestBlipIndex_Temp = -1
		sRuleBoundsRuntimeVars[iBoundsIndex].iOutOfBoundsBlipStaggeredIndex = 0
	ENDIF
ENDPROC

PROC PROCESS_FMMC_RULE_BOUNDS_PRE_EVERY_FRAME()
	IF IS_BIT_SET(iSharedRuleBoundsBS, ciSHARED_RULE_BOUNDS_BS__BLOCK_OBJECTIVE_THIS_FRAME)
		SET_BIT(iSharedRuleBoundsBS, ciSHARED_RULE_BOUNDS_BS__BLOCKED_OBJECTIVE_LAST_FRAME)
		CLEAR_BIT(iSharedRuleBoundsBS, ciSHARED_RULE_BOUNDS_BS__BLOCK_OBJECTIVE_THIS_FRAME)
	ELSE
		CLEAR_BIT(iSharedRuleBoundsBS, ciSHARED_RULE_BOUNDS_BS__BLOCKED_OBJECTIVE_LAST_FRAME)
	ENDIF
ENDPROC

PROC PROCESS_FMMC_RULE_BOUNDS_POST_EVERY_FRAME()
	IF IS_BIT_SET(iSharedRuleBoundsBS, ciSHARED_RULE_BOUNDS_BS__BLOCK_OBJECTIVE_THIS_FRAME)
		IF NOT IS_OBJECTIVE_BLOCKED()
			PRINTLN("[RCC MISSION][Bounds_SPAM] PROCESS_FMMC_RULE_BOUNDS_POST_EVERY_FRAME | Clearing objective blips")
			CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM0_TARGET)
			CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM1_TARGET)
			CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM2_TARGET)
			CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM3_TARGET)
			CLEANUP_OBJECTIVE_BLIPS()
		ENDIF
			
		PRINTLN("[RCC MISSION][Bounds_SPAM] PROCESS_FMMC_RULE_BOUNDS_POST_EVERY_FRAME | Blocking objective")
		BLOCK_OBJECTIVE_THIS_FRAME()
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_IS_OUT_OF_BOUNDS(INT iBoundsIndex)
	IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_OUT_OF_BOUNDS_0 + iBoundsIndex)
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_OUT_OF_BOUNDS_0 + iBoundsIndex)
		PRINTLN("[Bounds][RB ", iBoundsIndex, "] SET_LOCAL_PLAYER_IS_OUT_OF_BOUNDS | Player is no longer within Rule Bounds ", iBoundsIndex)
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_IS_WITHIN_BOUNDS(INT iBoundsIndex)
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_OUT_OF_BOUNDS_0 + iBoundsIndex)
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_OUT_OF_BOUNDS_0 + iBoundsIndex)
		PRINTLN("[Bounds][RB ", iBoundsIndex, "] SET_LOCAL_PLAYER_IS_WITHIN_BOUNDS | Player is back within Rule Bounds ", iBoundsIndex)
		
		RESET_RULE_BOUNDS_OUT_OF_BOUNDS_VARS(iBoundsIndex)
	ENDIF
ENDPROC

PROC PROCESS_BOUNDS_STAGGERED_CLIENT(INT iBoundsIndex)
	
	BOOL bBoundsAreValid = ARE_BOUNDS_VALID(iBoundsIndex)
	
	IF NOT SHOULD_PROCESS_ANY_BOUNDS_THIS_FRAME()
		IF bBoundsAreValid
			SET_BIT(iRuleBoundsCurrentlyOnHoldBS, iBoundsIndex)
		ENDIF
		
		EXIT
	ENDIF
	
	IF SHOULD_RULE_BOUNDS_WAIT_FOR_FULL_STAGGERED_LOOP(iBoundsIndex)
		SET_RULE_BOUNDS_TO_WAIT_FOR_FULL_STAGGERED_LOOP(iBoundsIndex)
		
		IF bBoundsAreValid
			SET_BIT(iRuleBoundsCurrentlyOnHoldBS, iBoundsIndex)
		ENDIF
	ENDIF
	
	IF SHOULD_CHECK_OOB_FOR_THESE_BOUNDS_THIS_FRAME(iBoundsIndex)
	
		IF IS_BIT_SET(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsBS, ciRULE_BOUNDS_BS__BLOCK_BOUNDS_EVERY_FRAME_PROCESSING)
			CLEAR_BIT(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsBS, ciRULE_BOUNDS_BS__BLOCK_BOUNDS_EVERY_FRAME_PROCESSING)
			PRINTLN("[Bounds][RB ", iBoundsIndex, "] PROCESS_BOUNDS_STAGGERED_CLIENT | Clearing ciRULE_BOUNDS_BS__BLOCK_BOUNDS_EVERY_FRAME_PROCESSING because these Bounds are now safe to check")
		ENDIF
		
		IF SHOULD_LOCAL_PLAYER_BE_CONSIDERED_OUT_OF_BOUNDS(iBoundsIndex, g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].sRuleBounds[GET_LOCAL_PLAYER_CURRENT_RULE()][iBoundsIndex])
			SET_LOCAL_PLAYER_IS_OUT_OF_BOUNDS(iBoundsIndex)
		ELSE
			SET_LOCAL_PLAYER_IS_WITHIN_BOUNDS(iBoundsIndex)
		ENDIF
		
		// Clear iRuleBoundsCurrentlyOnHoldBS because these Bounds are safe to check if they're able to reach this point
		CLEAR_BIT(iRuleBoundsCurrentlyOnHoldBS, iBoundsIndex)
		
	ENDIF
ENDPROC

PROC PROCESS_BOUNDS_PRE_STAGGERED_CLIENT()
	
ENDPROC

PROC PROCESS_BOUNDS_POST_STAGGERED_CLIENT()
	IF NOT IS_BIT_SET(iEntityFirstStaggeredLoopComplete, ci_BOUNDS_FIRST_STAGGERED_LOOP_COMPLETE)
		IF ARE_ANY_RULE_BOUNDS_ON_HOLD()
			PRINTLN("[Bounds] PROCESS_BOUNDS_STAGGERED_CLIENT | Not setting ci_BOUNDS_FIRST_STAGGERED_LOOP_COMPLETE yet due to iRuleBoundsCurrentlyOnHoldBS being ", iRuleBoundsCurrentlyOnHoldBS)
			
		ELIF g_FMMC_STRUCT_ENTITIES.iNumberOfZones > 0
		AND NOT IS_BIT_SET(iEntityFirstStaggeredLoopComplete, ci_ZONES_FIRST_STAGGERED_LOOP_COMPLETE)
			PRINTLN("[Bounds] PROCESS_BOUNDS_STAGGERED_CLIENT | Not setting ci_BOUNDS_FIRST_STAGGERED_LOOP_COMPLETE yet due to ci_ZONES_FIRST_STAGGERED_LOOP_COMPLETE not being set yet")
			
		ELSE
			SET_BIT(iEntityFirstStaggeredLoopComplete, ci_BOUNDS_FIRST_STAGGERED_LOOP_COMPLETE)
			PRINTLN("[Bounds] PROCESS_BOUNDS_STAGGERED_CLIENT | Setting ci_BOUNDS_FIRST_STAGGERED_LOOP_COMPLETE now")
		ENDIF
	ENDIF
ENDPROC
