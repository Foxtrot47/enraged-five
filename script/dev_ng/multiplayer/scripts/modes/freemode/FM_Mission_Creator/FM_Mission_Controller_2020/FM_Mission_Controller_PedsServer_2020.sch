// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Peds Server -------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Header that houses the Server Processing and Decision making for creator placed Peds															  		  
// ##### Staggered Objective Processing (iPed)																																  
// ##### Staggered Processing (iPed)																																		  
// ##### Every Frame Processing (iPed)																																		  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_PedsShared_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Ped Objective Failure
// ##### Description: Functions for processing the failure of an objective/mission due to peds
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Called on the frame HAVE_MC_END_CONDITIONS_BEEN_MET returns true
///    to decide if the mission should fail due some ped condition/state
PROC PROCESS_FINAL_PED_FAILURE_CHECKS()
	
	INT iTeam
	BOOL bHaveAllTeamsFailed = TRUE
	BOOL bAnyAggroTimerHasStarted = FALSE
	
	
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		
		IF NOT HAS_TEAM_FAILED(iTeam)
			bHaveAllTeamsFailed = FALSE
		ENDIF
		
		IF HAS_AGGRO_INDEX_TIMER_STARTED_FROM_BITSET(iTeam, ciMISSION_AGGRO_INDEX_ALL)
			bAnyAggroTimerHasStarted = TRUE
		ENDIF
		
		IF bAnyAggroTimerHasStarted
		AND NOT bHaveAllTeamsFailed
			BREAKLOOP
		ENDIF
		
	ENDFOR
	
	IF bHaveAllTeamsFailed
	OR NOT bAnyAggroTimerHasStarted
		//All teams have failed already and no aggro timers are running - no need to check additional fail conditions.
		EXIT
	ENDIF
	
	IF IS_LONG_BITSET_EMPTY(MC_serverBD.iPedAggroedBitset)
		//No aggrod peds - no need to check additional fail conditions.
		EXIT
	ENDIF
			
	INT iTempBS[FMMC_MAX_PEDS_BITSET]
	COPY_INT_ARRAY_VALUES(MC_serverBD.iPedAggroedBitset, iTempBS)
			
	INT iPed
	FOR iPed = 0 TO MC_serverBD.iNumPedCreated - 1
		
		IF IS_LONG_BITSET_EMPTY(iTempBS)
			BREAKLOOP
		ENDIF
		
		IF NOT FMMC_IS_LONG_BIT_SET(iTempBS, iPed)
			RELOOP
		ENDIF
		
		PRINTLN("[END_CONDITIONS] PROCESS_FINAL_PED_FAILURE_CHECKS - Ped ", iPed, " is still aggroed (but not necessarily alive)")
			
		FMMC_CLEAR_LONG_BIT(iTempBS, iPed)
			
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwo,ciPed_BSTwo_AggroDoesntFail)
			RELOOP
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThree, ciPed_BSThree_AggroFailHasDelay)
			RELOOP
		ENDIF
			
		IF IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			RELOOP
		ENDIF
					
		PRINTLN("[END_CONDITIONS] PROCESS_FINAL_PED_FAILURE_CHECKS - Ped ", iPed, " is still aggroed and alive!")
					
		FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			
			IF HAS_TEAM_FAILED(iTeam)
				RELOOP
			ENDIF
			
			PRINTLN("[END_CONDITIONS] PROCESS_FINAL_PED_FAILURE_CHECKS - Ped ", iPed, " causing mission failure for team: ", iTeam)
				
			IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedByDeadBodyBitset, iPed)
				MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_AGRO
				REQUEST_SET_TEAM_FAILED(iTeam,int_to_enum(eFailMissionEnum,enum_to_int(mFail_PED_AGRO_T0)+iTeam),false,MC_serverBD.iPartCausingFail[iTeam])										
			ELSE
				MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_FOUND_BODY
				REQUEST_SET_TEAM_FAILED(iTeam,mFail_PED_FOUND_BODY)										
			ENDIF
			
			MC_serverBD.iEntityCausingFail[iTeam] = iPed
							
		ENDFOR
		
		BREAKLOOP
					
	ENDFOR
	
ENDPROC

/// PURPOSE: This function checks if the current vehicle crash condition was met.
FUNC BOOL WAS_CRASH_VEHICLE_CONDITION_MET(CRASH_VEHICLE_STRUCT &sCrashVehData, FLOAT &fVehicleHealthPercentage #IF IS_DEBUG_BUILD ,FMMC_PED_STATE &sPedState #ENDIF )
	//Using Damage
	IF sCrashVehData.eCrashVehicleOption = FMMC_CRASH_VEH_BASED_ON_VEH_DAMAGE
		IF sCrashVehData.iOptionAssociatedValue <= 100 - FLOOR(fVehicleHealthPercentage)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - WAS_CRASH_VEHICLE_CONDITION_MET returned TRUE due to vehicle damage. Veh Damage = ", (100 - FLOOR(fVehicleHealthPercentage)), " | Target value = ", sCrashVehData.iOptionAssociatedValue)
			RETURN TRUE
		ENDIF
	
	//Using rule
	ELIF sCrashVehData.eCrashVehicleOption = FMMC_CRASH_VEH_BASED_ON_RULE
		//Check for a specific team
		IF sCrashVehData.iAssociatedTeam != -1
	 		IF sCrashVehData.iOptionAssociatedValue <= MC_serverBD_4.iCurrentHighestPriority[sCrashVehData.iAssociatedTeam]
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - WAS_CRASH_VEHICLE_CONDITION_MET returned TRUE as the associated team reached the objective. Team ", sCrashVehData.iAssociatedTeam, " | Objective ", MC_serverBD_4.iCurrentHighestPriority[sCrashVehData.iAssociatedTeam])
				RETURN TRUE
			ENDIF
		//Check for all teams
		ELSE
			INT i
			REPEAT FMMC_MAX_TEAMS i
				IF sCrashVehData.iOptionAssociatedValue  <= MC_serverBD_4.iCurrentHighestPriority[i]
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - WAS_CRASH_VEHICLE_CONDITION_MET returned TRUE as one of the teams reached the objective. Team ", i, " | Objective ", MC_serverBD_4.iCurrentHighestPriority[i])
					RETURN TRUE
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	RETURN FALSE	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Server only Ped Utility Functions  ------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Server only Helper Functions    									 								  --------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC PROCESS_IN_VEHICLE_BEHAVIOUR_SERVER(FMMC_PED_STATE &sPedState)
	IF sPedState.bInjured
	OR NOT sPedState.bIsInAnyVehicle
		EXIT
	ENDIF
	IF IS_ENTITY_ALIVE(sPedState.vehIndexPedIsIn)
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iVehicle > -1
			
			FLOAT fPercentageThreshhold
			FLOAT fPedVehicleHealthPercentage = MC_GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(sPedState.vehIndexPedIsIn, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iVehicle, GET_TOTAL_STARTING_PLAYERS())
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedGotoHeightOffset != -101
			AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedGoToHeightOffsetAtHealth != -1
				
				fPercentageThreshhold = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedGoToHeightOffsetAtHealth)
				
				PRINTLN("[LM][TASK_GO_TO_COORD_ANY_MEANS_etc][iPedGotoHeightOffset] PROCESS_PED_BRAIN - MC_GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE: ", fPedVehicleHealthPercentage, " fPercentageThreshhold: ", fPercentageThreshhold, " iVehicle: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iVehicle)
				
				IF fPedVehicleHealthPercentage < fPercentageThreshhold
					IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iPedHeightOverride, sPedState.iIndex)				
						PRINTLN("[LM][TASK_GO_TO_COORD_ANY_MEANS_etc][iPedGotoHeightOffset] PROCESS_PED_BRAIN - SETTING VEHICLE TO BE RE-TASKED - MC_GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE")
						FMMC_SET_LONG_BIT(MC_serverBD_4.iPedHeightOverride, sPedState.iIndex)
						SET_PED_RETASK_DIRTY_FLAG(sPedState)
						
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_15 pedStateName2 = GET_PED_STATE_NAME(MC_serverBD_2.iPedState[sPedState.iIndex])								
						PRINTLN("[LM][RCC MISSION][iPedGotoHeightOffset] PROCESS_PED_BRAIN - iPed: ", sPedState.iIndex, " GET_PED_STATE_NAME: ", pedStateName2, " Before")
						#ENDIF
					
						SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK)
						
						#IF IS_DEBUG_BUILD
						pedStateName2 = GET_PED_STATE_NAME(MC_serverBD_2.iPedState[sPedState.iIndex])								
						PRINTLN("[LM][RCC MISSION][iPedGotoHeightOffset] PROCESS_PED_BRAIN - iPed: ", sPedState.iIndex, " GET_PED_STATE_NAME: ", pedStateName2, " After")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Vehicle crash rule
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sCrashVehicleData.eCrashVehicleOption != FMMC_CRASH_VEH_DISABLED
				IF WAS_CRASH_VEHICLE_CONDITION_MET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sCrashVehicleData, fPedVehicleHealthPercentage #IF IS_DEBUG_BUILD ,sPedState #ENDIF )
					SET_PED_STATE(sPedState.iIndex, ciTASK_CRASH_VEHICLE)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - should go into Crash Vehicle State")
					EXIT
				ENDIF
			ENDIF		
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedFleeOnVehicleHealthPercentage != 0

				fPercentageThreshhold = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedFleeOnVehicleHealthPercentage)
				
				PRINTLN("[LM][FLEE][iPedFleeOnVehicleHealthPercentage] PROCESS_PED_BRAIN - MC_GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE: ", fPedVehicleHealthPercentage, " fPercentageThreshhold: ", fPercentageThreshhold, " iVehicle: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iVehicle)
				
				IF fPedVehicleHealthPercentage < fPercentageThreshhold
					IF MC_serverBD_2.iPedState[sPedState.iIndex] = ciTASK_GOTO_COORDS
					OR MC_serverBD_2.iPedState[sPedState.iIndex] = ciTASK_GOTO_ENTITY
					OR MC_serverBD_2.iPedState[sPedState.iIndex] = ciTASK_GOTO_PLAYER
						IF  MC_serverBD_2.iPedState[sPedState.iIndex] != ciTASK_ABORT_GOTO_AND_FLEE
						AND MC_serverBD_2.iPedState[sPedState.iIndex] != ciTASK_FLEE
							SET_PED_STATE(sPedState.iIndex, ciTASK_ABORT_GOTO_AND_FLEE)
							FMMC_SET_LONG_BIT(MC_serverBD_4.iPedFleeDueToVehicleHealth, sPedState.iIndex)
							PRINTLN("[LM][TASK_FLEE][RCC MISSION][iPedFleeOnVehicleHealthPercentage] PROCESS_PED_BRAIN - iPed: ", sPedState.iIndex, " set to state FLEE (ciTASK_ABORT_GOTO_AND_FLEE)")
						ENDIF
					ELSE
						IF MC_serverBD_2.iPedState[sPedState.iIndex] != ciTASK_FLEE
							SET_PED_STATE(sPedState.iIndex, ciTASK_FLEE)
							FMMC_SET_LONG_BIT(MC_serverBD_4.iPedFleeDueToVehicleHealth, sPedState.iIndex)
							PRINTLN("[LM][TASK_FLEE][RCC MISSION][iPedFleeOnVehicleHealthPercentage] PROCESS_PED_BRAIN - iPed: ", sPedState.iIndex, " set to state FLEE (ciTASK_FLEE)")
						ENDIF
					ENDIF					
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetEighteen, ciPed_BSEighteen_FleeIfLastPedInVehicle)
			IF MC_serverBD_2.iPedState[sPedState.iIndex] != ciTASK_FLEE
			AND GET_VEHICLE_NUMBER_OF_PASSENGERS(sPedState.vehIndexPedIsIn, TRUE, FALSE) <= 1
				PRINTLN("[LM][TASK_FLEE][RCC MISSION][iPedFleeOnVehicleHealthPercentage] PROCESS_PED_BRAIN - ciPed_BSEighteen_FleeIfLastPedInVehicle - Last ped in the vehicle, sending to flee task.")
				SET_PED_STATE(sPedState.iIndex, ciTASK_FLEE)
			ENDIF
		ENDIF
		
		// If the ped isn't a driver, and there is a turret on the vehicle, and they have the option set, then they will switch to it.
		IF SHOULD_PED_SHUFFLE_TO_TURRET_SEAT_FROM_BEING_ALERTED(sPedState.iIndex, sPedState.pedIndex)
		AND GET_PED_IN_VEHICLE_SEAT(sPedState.vehIndexPedIsIn, VS_DRIVER) != sPedState.pedIndex
			VEHICLE_SEAT seatTurret = INT_TO_ENUM(VEHICLE_SEAT, GetFirstFreeSeatOfVehicle(sPedState.vehIndexPedIsIn, TRUE, FALSE, TRUE))
			
			PRINTLN("[LM][ShuffleTurretSeatAlerted] PROCESS_PED_BRAIN - Seat Selected: ", ENUM_TO_INT(seatTurret), " Option is set that we want to shuffle to an available turret seat when alerted.")
			PRINTLN("[LM][ShuffleTurretSeatAlerted] PROCESS_PED_BRAIN - IS_TURRET_SEAT (desired): ", IS_TURRET_SEAT(sPedState.vehIndexPedIsIn, seatTurret))
			PRINTLN("[LM][ShuffleTurretSeatAlerted] PROCESS_PED_BRAIN - IS_TURRET_SEAT (we're in): ", IS_TURRET_SEAT(sPedState.vehIndexPedIsIn, GET_SEAT_PED_IS_IN(sPedState.pedIndex, TRUE)))
			PRINTLN("[LM][ShuffleTurretSeatAlerted] PROCESS_PED_BRAIN - GET_PED_IN_VEHICLE_SEAT: ", GET_PED_IN_VEHICLE_SEAT(sPedState.vehIndexPedIsIn, seatTurret) != sPedState.pedIndex)
			
			IF IS_TURRET_SEAT(sPedState.vehIndexPedIsIn, seatTurret)
			AND IS_VEHICLE_SEAT_FREE(sPedState.vehIndexPedIsIn, seatTurret, TRUE)
			AND NOT IS_TURRET_SEAT(sPedState.vehIndexPedIsIn, GET_SEAT_PED_IS_IN(sPedState.pedIndex, TRUE))
			AND GET_PED_IN_VEHICLE_SEAT(sPedState.vehIndexPedIsIn, seatTurret) != sPedState.pedIndex
				SCRIPTTASKSTATUS TaskStatus = GET_SCRIPT_TASK_STATUS(sPedState.pedIndex, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT)
				
				IF (TaskStatus != WAITING_TO_START_TASK)
				AND (TaskStatus != PERFORMING_TASK)
					PRINTLN("[LM][ShuffleTurretSeatAlerted] PROCESS_PED_BRAIN - SETTING VEHICLE TO BE RE-TASKED - SHOULD_PED_SHUFFLE_TO_TURRET_SEAT_FROM_BEING_ALERTED")
					SET_PED_STATE(sPedState.iIndex, ciTASK_SHUFFLE_VEHICLE_SEAT_TO_TURRET)
				ELSE
					PRINTLN("[LM][RCC MISSION][ShuffleTurretSeatAlerted] PROCESS_PED_BRAIN - iPed: ", sPedState.iIndex, " Already Performing Task.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PED_ZONE_BEHAVIOUR(FMMC_PED_STATE &sPedState)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iAssociatedFleeZone != -1
		IF IS_ZONE_TRIGGERING(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iAssociatedFleeZone)
		AND MC_serverBD_2.iPedState[sPedState.iIndex] != ciTASK_FLEE
			SET_PED_STATE(sPedState.iIndex, ciTASK_FLEE)
			FMMC_SET_LONG_BIT(MC_serverBD_4.iPedFleeDueToZone, sPedState.iIndex)
			PRINTLN("[TASK_FLEE][RCC MISSION] PROCESS_PED_ZONE_BEHAVIOUR - iPed: ", sPedState.iIndex, " set to state FLEE")
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Objective Server Functions  -------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Aggro and Spook server authorative functions.   									  ------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_PED_KILLERS_TEAM(PED_INDEX killerPed, INT iPedID)

	PLAYER_INDEX tempPlayer
	PARTICIPANT_INDEX tempPart
	INT ipart

	IF DOES_ENTITY_EXIST(killerPed)
		IF IS_PED_A_PLAYER(killerPed)
			tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(killerPed)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(tempPlayer)
				tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
				ipart = NATIVE_TO_INT(tempPart)
				IF MC_serverBD_4.iPedPriority[iPedID][MC_playerBD[ipart].iTeam] = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[ipart].iTeam]
					IF MC_serverBD_4.iPedPriority[iPedID][MC_playerBD[ipart].iTeam] < FMMC_MAX_RULES
						IF MC_serverBD_4.iPedRule[iPedID][MC_playerBD[ipart].iTeam] = FMMC_OBJECTIVE_LOGIC_KILL OR MC_serverBD_4.iPedRule[iPedID][MC_playerBD[ipart].iTeam] = FMMC_OBJECTIVE_LOGIC_DAMAGE
							RETURN MC_playerBD[ipart].iTeam
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN -1

ENDFUNC

FUNC BOOL DOES_PED_MEET_SCORING_REQUIREMENTS_FOR_CURRENT_RULE(INT iPed, INT iTeam)
	
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	IF iRule >= FMMC_MAX_RULES
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_PED_SCORE_VALID_ONLY_IF_HOLDING_OBJECT)
		IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iPedHasAttachedObject, iPed)
			PRINTLN("DOES_PED_MEET_SCORING_REQUIREMENTS_FOR_CURRENT_RULE - Ped: ", iPed, " is not carrying an object - FALSE")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC GIVE_TEAM_POINTS_FOR_PED_DAMAGE(INT& iPedID, PED_INDEX Victimped, PED_INDEX killerPed = NULL)

	INT iTeam
	INT iRule
	INT ikillerTeam = -1
	
	IF bIsLocalPlayerHost
		
		IF iPedID = -2
			iPedid = IS_ENTITY_A_MISSION_CREATOR_ENTITY(Victimped)
		ENDIF
		
		IF iPedid >= 0
			ikillerTeam = GET_PED_KILLERS_TEAM(killerPed, iPedid)
			FOR iTeam = 0 TO (MC_serverBD.iNumActiveTeams-1)
				IF iKillerTeam != iTeam
					iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
					IF iRule < FMMC_MAX_RULES
						IF NOT DOES_PED_MEET_SCORING_REQUIREMENTS_FOR_CURRENT_RULE(iPedID, iTeam)
							RELOOP
						ENDIF
					
						IF MC_serverBD_4.iPedPriority[iPedid][iTeam] < FMMC_MAX_RULES
							IF MC_serverBD_4.iPedPriority[iPedid][iTeam] = iRule
								IF MC_serverBD_4.iPedRule[iPedid][iTeam] = FMMC_OBJECTIVE_LOGIC_DAMAGE
									INCREMENT_SERVER_TEAM_SCORE(iTeam, MC_serverBD_4.iPedPriority[iPedid][iTeam], GET_FMMC_POINTS_FOR_TEAM(iTeam, MC_serverBD_4.iPedPriority[iPedid][iTeam]))
									PRINTLN("GIVE_TEAM_POINTS_FOR_PED_DAMAGE ADDING TO TEAM SCORE for team: ", iTeam, " PED was killed : ", iPedid)
									BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iPedPriority[iPedid][iTeam], iTeam)
								ELSE
									PRINTLN("GIVE_TEAM_POINTS_FOR_PED_DAMAGE rule not kill ped for team: ", iTeam, " PED was Hurt below thresh-hold : ", iPedid)
								ENDIF
							ELSE
								PRINTLN("GIVE_TEAM_POINTS_FOR_PED_DAMAGE priority not current ped for team: ", iTeam, " PED was Hurt below thresh-hold : ", iPedid)
							ENDIF
						ELSE
							PRINTLN("GIVE_TEAM_POINTS_FOR_PED_DAMAGE priority ignore ped for team: ", iTeam, " PED was Hurt below thresh-hold : ", iPedid)
						ENDIF
						
						INT iCustomPoints = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedid].iPedPointsToGive
						INT iRuleStart = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedid].iPedGivePointsOnRuleStart
						INt iRuleEnd = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedid].iPedGivePointsOnRuleEnd
						
						IF iCustomPoints > 0
							PRINTLN("GIVE_TEAM_POINTS_FOR_PED_KILL - iPed: ", iPedID, " iTeam: ", iTeam, " iRule: ", iRule, " iCustomPoints: ", iCustomPoints, " iRuleStart: ", iRuleStart, " iRuleEnd: ", iRuleEnd)
							
							IF iRule >= iRuleStart
							AND (iRule < iRuleEnd OR iRuleEnd = -1)
								INCREMENT_SERVER_TEAM_SCORE(iTeam, iRule, iCustomPoints)
								PRINTLN("GIVE_TEAM_POINTS_FOR_PED_DAMAGE ADDING TO TEAM SCORE for team: ", iTeam," PED was Hurt below thresh-hold : ",iPedid)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ELSE
			PRINTLN("GIVE_TEAM_POINTS_FOR_PED_DAMAGE not mission ped for team: ", iTeam," PED was killed : ",iPedid)
		ENDIF
	ENDIF


ENDPROC

PROC GIVE_TEAM_POINTS_FOR_PED_KILL(INT& iPedID, PED_INDEX Victimped,PED_INDEX killerPed = NULL)

	INT iTeam
	INT iRule
	INT ikillerTeam = -1
	
	IF bIsLocalPlayerHost
		
		IF iPedID = -2
			iPedid = IS_ENTITY_A_MISSION_CREATOR_ENTITY(Victimped)
		ENDIF
		
		IF iPedid >= 0
			ikillerTeam = GET_PED_KILLERS_TEAM(killerPed,iPedid)
			FOR iTeam = 0 TO (MC_serverBD.iNumActiveTeams-1)
				IF iKillerTeam != iTeam
					iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
					IF iRule < FMMC_MAX_RULES
						IF NOT DOES_PED_MEET_SCORING_REQUIREMENTS_FOR_CURRENT_RULE(iPedID, iTeam)
							RELOOP
						ENDIF
						
						IF MC_serverBD_4.iPedPriority[iPedid][iTeam] < FMMC_MAX_RULES
							IF MC_serverBD_4.iPedPriority[iPedid][iTeam] = iRule
								IF MC_serverBD_4.iPedRule[iPedid][iTeam] = FMMC_OBJECTIVE_LOGIC_KILL
									BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iPedPriority[iPedid][iTeam], iTeam)
								ENDIF
							ENDIF
						ENDIF
					
						INT iCustomPoints = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedid].iPedPointsToGive
						INT iRuleStart = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedid].iPedGivePointsOnRuleStart
						INt iRuleEnd = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedid].iPedGivePointsOnRuleEnd
						
						IF iCustomPoints > 0
							PRINTLN("GIVE_TEAM_POINTS_FOR_PED_KILL - iPed: ", iPedID, " iTeam: ", iTeam, " iRule: ", iRule, " iCustomPoints: ", iCustomPoints, " iRuleStart: ", iRuleStart, " iRuleEnd: ", iRuleEnd)
							
							IF iRule >= iRuleStart
							AND (iRule < iRuleEnd OR iRuleEnd = -1)
								INCREMENT_SERVER_TEAM_SCORE(iTeam, iRule, iCustomPoints)
								PRINTLN("ADDING TO TEAM SCORE for team: ", iTeam," PED was killed : ",iPedid)
								IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_FIXER) //url:bugstar:7423028 - Remove DLC Check in PROC GIVE_TEAM_POINTS_FOR_PED_KILL(INT& iPedID, PED_INDEX Victimped,PED_INDEX killerPed = NULL)
									EXIT
								ENDIF
							ENDIF
						ENDIF
					
						IF MC_serverBD_4.iPedPriority[iPedid][iTeam] < FMMC_MAX_RULES
							IF MC_serverBD_4.iPedPriority[iPedid][iTeam] = iRule
								IF MC_serverBD_4.iPedRule[iPedid][iTeam] = FMMC_OBJECTIVE_LOGIC_KILL
									INCREMENT_SERVER_TEAM_SCORE(iTeam,MC_serverBD_4.iPedPriority[iPedid][iTeam],GET_FMMC_POINTS_FOR_TEAM(iTeam,MC_serverBD_4.iPedPriority[iPedid][iTeam]))
									PRINTLN("ADDING TO TEAM SCORE for team: ", iTeam," PED was killed : ",iPedid)
								ELSE
									PRINTLN("rule not kill ped for team: ", iTeam," PED was killed : ",iPedid)
								ENDIF
							ELSE
								PRINTLN("priority not current ped for team: ", iTeam," PED was killed : ",iPedid)
							ENDIF
						ELSE
							PRINTLN("priority ignore ped for team: ", iTeam," PED was killed : ",iPedid)
						ENDIF
						
					ENDIF
				ENDIF
			ENDFOR
		ELSE
			PRINTLN("not mission ped for team: ", iTeam," PED was killed : ",iPedid)
		ENDIF
	ENDIF


ENDPROC

FUNC BOOL IS_TEAM_ON_RIGHT_RULE_TO_DO_PED_ARRIVAL_RESULTS(INT iTeam, INT iPed, BOOL bFailCheck = FALSE)
	
	BOOL bOnRightRule = FALSE
	INT iPedRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iGotoProgressesRule[iTeam]
	INT iTeamRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iPedRule >= 0
		IF (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSix, ciPed_BSSix_GotoProgressesRule_InvalidateFutureRule_T0 + iTeam) AND NOT bFailCheck)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSix, ciPed_BSSix_GotoProgressesRule_InvalidateAllRulesUpToAndIncl_T0 + iTeam)
			IF iTeamRule <= iPedRule
				PRINTLN("[RCC MISSION] IS_TEAM_ON_RIGHT_RULE_TO_DO_PED_ARRIVAL_RESULTS - Return TRUE for team ",iTeam,", ped ",iPed,", invalidatefuturerule or invalidateallrulesuptoandincl set - iTeamRule ",iTeamRule," <= iPedRule ",iPedRule)
				bOnRightRule = TRUE
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[RCC MISSION] IS_TEAM_ON_RIGHT_RULE_TO_DO_PED_ARRIVAL_RESULTS - Return FALSE for team ",iTeam,", ped ",iPed," as invalidatefuturerule or invalidateallrulesuptoandincl set - iTeamRule ",iTeamRule," > iPedRule ",iPedRule)
			#ENDIF
			ENDIF
		ELSE
			IF iTeamRule = iPedRule
				PRINTLN("[RCC MISSION] IS_TEAM_ON_RIGHT_RULE_TO_DO_PED_ARRIVAL_RESULTS - Return TRUE for team ",iTeam,", ped ",iPed,", same rule check - iTeamRule ",iTeamRule," = iPedRule ",iPedRule)
				bOnRightRule = TRUE
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[RCC MISSION] IS_TEAM_ON_RIGHT_RULE_TO_DO_PED_ARRIVAL_RESULTS - Return FALSE for team ",iTeam,", ped ",iPed,", same rule check - iTeamRule ",iTeamRule," != iPedRule ",iPedRule)
			#ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION] IS_TEAM_ON_RIGHT_RULE_TO_DO_PED_ARRIVAL_RESULTS - Return TRUE for team ",iTeam,", ped ",iPed,", as iPedRule is -1 - iTeamRule ",iTeamRule)
		bOnRightRule = TRUE
	ENDIF
	
	RETURN bOnRightRule
	
ENDFUNC

FUNC BOOL DOES_TEAM_HAVE_PHOTO_DEAD_RULE(INT iPed,INT iTeam)

	IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[iTeam], iPed)
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_PED_DEATH_CAUSE_FAIL(INT iPed, INT iTeam, INT iPriority)

	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
		IF IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			IF NOT DOES_TEAM_HAVE_PHOTO_DEAD_RULE(iPed,iTeam)
				IF iPriority < FMMC_MAX_RULES
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamFailBitset,iPriority)
						RETURN TRUE
					ENDIF
				ENDIF
				IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iMissionCriticalPed[iTeam], iPed)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF iPriority < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamFailBitset,iPriority)
				RETURN TRUE
			ENDIF
		ENDIF
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iMissionCriticalPed[iTeam], iPed)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC RUN_TEAM_PED_FAIL_CHECKS(INT iPed,INT iTeam,INT ipriority)

	INT iTeamrepeat
	BOOL bFailMission
		
	IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iPed], iTeam)
	AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iMissionCriticalRemovalRule[iTeam] > 0)
	AND MC_serverBD_4.iCurrentHighestPriority[iTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iMissionCriticalRemovalRule[iTeam]
		FMMC_CLEAR_LONG_BIT(MC_serverBD.iMissionCriticalPed[iTeam], iPed)
		CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iPed], iTeam)
		PRINTLN("[RCC MISSION] RUN_TEAM_PED_FAIL_CHECKS - Ped ",iPed," set as no longer critical for team ",iTeam," as they have reached iMissionCriticalRemovalRule ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iMissionCriticalRemovalRule[iTeam])
	ENDIF
	
	IF (IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iPed],iTeam) AND NOT (FMMC_IS_LONG_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[iTeam], iPed)))
	OR NOT (NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPed]) AND FMMC_IS_LONG_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[iTeam], iPed)) //If it doesn't exist
		
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_DEAD
		
		PRINTLN("[RCC MISSION]  OBJ_END_REASON_PED_DEAD set: ",iPed) 
		FOR iTeamrepeat = 0 TO (MC_serverBD.iNumberOfTeams-1)
			IF iTeam = 0
				IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iPed],SBBOOL1_TEAM0_NEEDS_KILL)
					IF DOES_TEAM_LIKE_TEAM(iTeamrepeat,iTeam)
						CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iPed],iTeamrepeat)
						FMMC_CLEAR_LONG_BIT(MC_serverBD.iMissionCriticalPed[iTeamrepeat], iPed)
					ENDIF
				ENDIF
			ELIF iTeam = 1
				IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iPed],SBBOOL1_TEAM1_NEEDS_KILL)
					IF DOES_TEAM_LIKE_TEAM(iTeamrepeat,iTeam)
						CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iPed],iTeamrepeat)
						FMMC_CLEAR_LONG_BIT(MC_serverBD.iMissionCriticalPed[iTeamrepeat], iPed)
					ENDIF
				ENDIF
			ELIF iTeam = 2
				IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iPed],SBBOOL1_TEAM2_NEEDS_KILL)
					IF DOES_TEAM_LIKE_TEAM(iTeamrepeat,iTeam)
						CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iPed],iTeamrepeat)
						FMMC_CLEAR_LONG_BIT(MC_serverBD.iMissionCriticalPed[iTeamrepeat], iPed)
					ENDIF
				ENDIF
			ELIF iTeam = 3
				IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iPed],SBBOOL1_TEAM3_NEEDS_KILL)
					IF DOES_TEAM_LIKE_TEAM(iTeamrepeat,iTeam)
						CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iPed],iTeamrepeat)
						FMMC_CLEAR_LONG_BIT(MC_serverBD.iMissionCriticalPed[iTeamrepeat], iPed)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
		IF iTeam = 0
			IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iPed],SBBOOL1_TEAM0_NEEDS_KILL)
				SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority,TRUE)
				PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iPed,iTeam,TRUE)
			ELSE
				IF SHOULD_PED_DEATH_CAUSE_FAIL(iPed,iTeam,ipriority)
					bFailMission = TRUE
					
					REQUEST_SET_TEAM_FAILED(iTeam,mFail_PED_DEAD,true)

					PRINTLN("[RCC MISSION] MISSION FAILED FOR TEAM 0 BECAUSE PED DEAD: ",iPed)
					MC_serverBD.iEntityCausingFail[iTeam] = iPed
				ENDIF
				PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iPed,iTeam,FALSE)
				SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority,FALSE)
			ENDIF
		ELIF iTeam = 1
			IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iPed],SBBOOL1_TEAM1_NEEDS_KILL)	
				SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority,TRUE)
				PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iPed,iTeam,TRUE)
			ELSE
				IF SHOULD_PED_DEATH_CAUSE_FAIL(iPed,iTeam,ipriority)	
					bFailMission = TRUE
					
					REQUEST_SET_TEAM_FAILED(iTeam,mFail_PED_DEAD,true)
					PRINTLN("[RCC MISSION] MISSION FAILED FOR TEAM 1 BECAUSE PED DEAD: ",iPed) 
					MC_serverBD.iEntityCausingFail[iTeam] = iPed
				ENDIF
				PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iPed,iTeam,FALSE)
				SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority,FALSE)
			ENDIF
		ELIF iTeam = 2
			IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iPed],SBBOOL1_TEAM2_NEEDS_KILL)	
				SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority,TRUE)
				PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iPed,iTeam,TRUE)
			ELSE
				IF SHOULD_PED_DEATH_CAUSE_FAIL(iPed,iTeam,ipriority)		
					bFailMission = TRUE
					
					REQUEST_SET_TEAM_FAILED(iTeam,mFail_PED_DEAD,true)
					PRINTLN("[RCC MISSION] MISSION FAILED FOR TEAM 2 BECAUSE PED DEAD: ",iPed) 
					MC_serverBD.iEntityCausingFail[iTeam] = iPed
				ENDIF
				PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iPed,iTeam,FALSE)
				SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority,FALSE)
			ENDIF
		ELIF iTeam = 3
			IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iPed],SBBOOL1_TEAM3_NEEDS_KILL)
				PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iPed,iTeam,TRUE)
				SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority,TRUE)
			ELSE
				IF SHOULD_PED_DEATH_CAUSE_FAIL(iPed,iTeam,ipriority)
					bFailMission = TRUE
					
					REQUEST_SET_TEAM_FAILED(iTeam,mFail_PED_DEAD,true)
					PRINTLN("[RCC MISSION] MISSION FAILED FOR TEAM 3 BECAUSE PED DEAD: ",iPed)
					MC_serverBD.iEntityCausingFail[iTeam] = iPed
				ENDIF
				PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iPed,iTeam,FALSE)
				SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority,FALSE)
			ENDIF
		ENDIF
		
		SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,ipriority,MC_serverBD.iReasonForObjEnd[iTeam],bFailMission)
		BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam],iTeam,ipriority,DID_TEAM_PASS_OBJECTIVE(iTeam,ipriority),bFailMission)			
		CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iPed],iTeam)		
	ENDIF

ENDPROC

FUNC BOOL HAVE_ALL_PEDS_TRIGGERED_PASS_RULE_CONSQ(INT iTeam, INT iPed)
	INT i
	FOR i = 0 TO MC_serverBD.iNumPedCreated-1
		IF i != iPed
			IF IS_LONG_BIT_SET(g_FMMC_STRUCT.iGoToPedCompletesRuleDoubleBS[iTeam], i) //Used Externally IS_LONG_BIT_SET
				IF IS_TEAM_ON_RIGHT_RULE_TO_DO_PED_ARRIVAL_RESULTS(iTeam, i)
					IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAtGotoLocBitset, i)
					OR NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedTasksArrivalResultsBS, i)	
						PRINTLN("[JS] HAVE_ALL_PEDS_TRIGGERED_PASS_RULE_CONSQ - Team ", iTeam, " is waiting on ped ", i)
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	PRINTLN("[JS] HAVE_ALL_PEDS_TRIGGERED_PASS_RULE_CONSQ - Team ", iTeam, " can progress")
	RETURN TRUE
ENDFUNC



// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Aggro and Task Activation  --------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Server processing and decision making for creator placed peds.   									  --------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL CHECK_PLAYERS_BEING_HOSTILE_IN_RANGE_OF_PED(INT iPed, FLOAT fRange)
	INT iPlayersToCheck, iPlayersChecked, i

	iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
	
	PED_INDEX pedToCheck
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
		pedToCheck = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
	ELSE
		RETURN FALSE
	ENDIF

	PRINTLN("[PLAYER_LOOP] - CHECK_PLAYERS_BEING_HOSTILE_IN_RANGE_OF_PED")
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
		IF NOT (IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)
		OR IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
			
			PRINTLN("[LM] CHECK_PLAYERS_BEING_HOSTILE_IN_RANGE_OF_PED - Checking iPed: ", iPed, " against iPlayer: ", i)
			
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				
				PRINTLN("[LM] CHECK_PLAYERS_BEING_HOSTILE_IN_RANGE_OF_PED - active")
				
				iPlayersChecked++
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX( tempPart )
				
				IF IS_NET_PLAYER_OK(tempPlayer)
					
					PRINTLN("[LM] CHECK_PLAYERS_BEING_HOSTILE_IN_RANGE_OF_PED - ok")
					
					PED_INDEX tempPed = GET_PLAYER_PED(tempPlayer)
					IF NOT IS_PED_INJURED(tempPed) 

						PRINTLN("[LM] CHECK_PLAYERS_BEING_HOSTILE_IN_RANGE_OF_PED - not injured")
						
						IF VDIST2(GET_ENTITY_COORDS(tempPed), GET_ENTITY_COORDS(pedToCheck)) < POW(fRange,2)							
							PRINTLN("[LM] CHECK_PLAYERS_BEING_HOSTILE_IN_RANGE_OF_PED - iPed: ", iPed, " iPlayer is now in range: ", i)
							IF IS_PED_SHOOTING(tempPed)
							OR IS_PED_IN_COMBAT(tempPed)
							OR IS_PED_PERFORMING_MELEE_ACTION(tempPed)
								PRINTLN("[LM] CHECK_PLAYERS_BEING_HOSTILE_IN_RANGE_OF_PED - iPed: ", iPed, " iPlayer IS BEING HOSTILE ", i)
								RETURN TRUE
							ELSE
								PRINTLN("[LM] CHECK_PLAYERS_BEING_HOSTILE_IN_RANGE_OF_PED - iPed: ", iPed, " iPlayer is not doing anything hostile: ", i)
							ENDIF
						ELSE
							PRINTLN("[LM] CHECK_PLAYERS_BEING_HOSTILE_IN_RANGE_OF_PED - iPed: ", iPed, " iPlayer is Out of Range: ", i)
						ENDIF
					ENDIF
				ENDIF
				IF iPlayersChecked >= iPlayersToCheck 
					BREAKLOOP
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

	RETURN FALSE
ENDFUNC

// Peds set up to have an "AggroRule" from the creator used to use IF NOT SHOULD_AGRO_FAIL_FOR_TEAM(iPed, iTeam) which made them unable to use the skiprule logic.
// This function fixes that issue while maintaining old content/old setups.
FUNC BOOL IS_PED_RULE_SKIP_VALID_ON_THIS_RULE(INT iPed, INT iTeam)
	IF SHOULD_AGRO_FAIL_FOR_TEAM(iped, iTeam)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
FUNC BOOL DOES_VEHICLE_PASS_SECONDARY_TASK_ACTIVATION_CONDITIONS(INT iPed, PED_INDEX ThisPed)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFourteen, ciPed_BSFourteen_SecondaryGoto_WaitForVehToBeEmptyOfPlayers)
		IF IS_PED_IN_ANY_VEHICLE(thisPed)
			VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(thisPed)
			IF IS_VEHICLE_DRIVEABLE(tempVeh)
				PED_INDEX pedInVeh
				INT iSeat
				INT iPassengerSeats = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(tempVeh)
				
				FOR iSeat = -1 TO (iPassengerSeats - 1)
					pedInVeh = GET_PED_IN_VEHICLE_SEAT(tempVeh, INT_TO_ENUM(VEHICLE_SEAT, iSeat))
					
					IF NOT IS_PED_INJURED(pedInVeh)
						IF IS_PED_A_PLAYER(pedInVeh)
							RETURN FALSE
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC


FUNC BOOL IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED(ENTITY_INDEX tempEnt, INT iPedChecking, INT iFleeDistance = 150, INT iEntityType = ciRULE_TYPE_NONE, INT iEntityID = -1 #IF IS_DEBUG_BUILD, BOOL bDoDebug = FALSE #ENDIF)
	
	BOOL bInDropOff = FALSE
	INT iTeamLoop
	BOOL bDropOffOverride
	VECTOR vDropOff
	
	#IF IS_DEBUG_BUILD
		BOOL bDebugInDropOff 
		FLOAT fDist
		VECTOR vEntCoords
		IF bDoDebug
			PRINTLN("[RCC MISSION] IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED called on entity type ",iEntityType," and ID ",iEntityID," with iPedChecking = ", iPedChecking, " iFleeDistance = ", iFleeDistance, "; Callstack:")
			DEBUG_PRINTCALLSTACK()
		ENDIF
	#ENDIF
	
	SWITCH iEntityType
		CASE ciRULE_TYPE_VEHICLE
			IF iEntityID >= 0
			AND iEntityID < FMMC_MAX_VEHICLES
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityID].vDropOffOverride)
					vDropOff = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityID].vDropOffOverride
					bDropOffOverride = TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_OBJECT
			IF iEntityID >= 0
			AND iEntityID < FMMC_MAX_NUM_OBJECTS
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityID].vObjDropOffOverride)
					vDropOff = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityID].vObjDropOffOverride
					bDropOffOverride = TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedChecking].iTeam[iTeamLoop] = ciPed_RELATION_SHIP_DISLIKE
		OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedChecking].iTeam[iTeamLoop] = ciPed_RELATION_SHIP_HATE
			
			IF NOT bDropOffOverride
				vDropOff = GET_DROP_OFF_CENTER_FOR_TEAM(DEFAULT, iTeamLoop)
			ENDIF
			
			IF (MC_serverBD_4.iCurrentHighestPriority[iTeamLoop] < FMMC_MAX_RULES)
			AND ( IS_ENTITY_IN_DROP_OFF_AREA(tempEnt, iTeamLoop, MC_serverBD_4.iCurrentHighestPriority[iTeamLoop], iEntityType, iEntityID)
				OR (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(tempEnt, vDropOff) < iFleeDistance) )
				
				#IF IS_DEBUG_BUILD
					IF bDoDebug
						IF (MC_serverBD_4.iCurrentHighestPriority[iTeamLoop] < FMMC_MAX_RULES)
							bDebugInDropOff = IS_ENTITY_IN_DROP_OFF_AREA(tempEnt, iTeamLoop, MC_serverBD_4.iCurrentHighestPriority[iTeamLoop], iEntityType, iEntityID)
							fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(tempEnt, vDropOff) 
							vEntCoords = GET_ENTITY_COORDS(tempEnt)
							PRINTLN("[RCC MISSION] IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED returning TRUE for iPedChecking = ", iPedChecking, " iTeamLoop = ", iTeamLoop, " IS_ENTITY_IN_DROP_OFF_AREA = ", bDebugInDropOff, " Dist from drop off = ", fDist, " Drop off centre = ", vDropOff, " Entity coords = ", vEntCoords)
						ENDIF
					ENDIF
				#ENDIF
				
				bInDropOff = TRUE
				iTeamLoop = MC_serverBD.iNumberOfTeams // Break out!
				
			ELSE
				#IF IS_DEBUG_BUILD
					IF bDoDebug
						IF (MC_serverBD_4.iCurrentHighestPriority[iTeamLoop] < FMMC_MAX_RULES)
							bDebugInDropOff = IS_ENTITY_IN_DROP_OFF_AREA(tempEnt, iTeamLoop, MC_serverBD_4.iCurrentHighestPriority[iTeamLoop], iEntityType, iEntityID)
							fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(tempEnt, vDropOff) 
							vEntCoords = GET_ENTITY_COORDS(tempEnt)
							PRINTLN("[RCC MISSION] IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED failing drop off check for iPedChecking = ", iPedChecking, " iTeamLoop = ", iTeamLoop, " IS_ENTITY_IN_DROP_OFF_AREA = ", bDebugInDropOff, " Dist from drop off = ", fDist, " Drop off centre = ", vDropOff, " Entity coords = ", vEntCoords)
						ENDIF
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
		IF bDoDebug
			PRINTLN("[RCC MISSION] IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED iPedChecking = ", iPedChecking, " returning ", bInDropOff)
		ENDIF
	#ENDIF
	
	RETURN bInDropOff
	
ENDFUNC

FUNC NETWORK_INDEX GET_PED_CUSTOM_TARGET_NETWORK_ID(INT iPed, INT iType, INT iID, INT iAction)
	
	INT iFleeFromDropOffDist = 150
	PED_INDEX ThisPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
	NETWORK_INDEX niTarget = NULL
	
	IF IS_PED_IN_ANY_HELI(ThisPed) OR IS_PED_IN_ANY_PLANE(ThisPed)
		iFleeFromDropOffDist = 350
	ENDIF
	
	PRINTLN("[Peds][Ped ", iPed, "] - GET_PED_CUSTOM_TARGET_NETWORK_ID - iType: ", iType, ", iID: ", iID, ", iAction: ", iAction)
	
	SWITCH iType
		CASE ciRULE_TYPE_PED
			IF iID < FMMC_MAX_PEDS
				niTarget = MC_serverBD_1.sFMMC_SBD.niPed[iID]
								
				IF NETWORK_DOES_NETWORK_ID_EXIST(niTarget)
					PED_INDEX tempPed
					tempPed = NET_TO_PED(niTarget)
					
					IF NOT IS_PED_INJURED(tempPed)
					AND ( IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitset, ciPed_BS_DontFleeDropOffs) OR (NOT IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED(tempPed, iPed, iFleeFromDropOffDist, ciRULE_TYPE_PED, iID)) )
						RETURN niTarget
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_VEHICLE
			IF iID < FMMC_MAX_VEHICLES
				niTarget = MC_serverBD_1.sFMMC_SBD.niVehicle[iID]
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(niTarget)
					
					VEHICLE_INDEX tempVeh
					tempVeh = NET_TO_VEH(niTarget)
					
					IF IS_VEHICLE_DRIVEABLE(tempVeh)
					AND ( IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitset, ciPed_BS_DontFleeDropOffs) OR (NOT IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED(tempVeh, iPed, iFleeFromDropOffDist, ciRULE_TYPE_VEHICLE, iID)) )
						
						BOOL bVehicleUsed
						
						IF iAction = ciACTION_GOTO
						AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedEscortRange = 0	AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vPedHeliEscortOffset))
						AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwelve, ciPed_BSTwelve_DoNotTryToEnterGoToTargetVehicle)
							VEHICLE_SEAT vsSeat
							vsSeat = GET_VEHICLE_SEAT_PED_SHOULD_ENTER(tempVeh, iPed)
							PRINTLN("[Peds][Ped ", iPed, "] - GET_PED_CUSTOM_TARGET_NETWORK_ID - Ped ",iPed,", vsSeat", vsSeat)
							IF vsSeat = VS_ANY_PASSENGER
								bVehicleUsed = TRUE // No free seats
							ENDIF
						ENDIF
						
						IF NOT bVehicleUsed // If the ped is able to get inside the vehicle
							RETURN niTarget
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_OBJECT
			IF iID < FMMC_MAX_NUM_OBJECTS
				niTarget = GET_OBJECT_NET_ID(iID)
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(niTarget)
					
					OBJECT_INDEX tempObj
					tempObj = NET_TO_OBJ(niTarget)
					
					IF NOT IS_ENTITY_DEAD(tempObj)
					AND ( IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitset, ciPed_BS_DontFleeDropOffs) OR (NOT IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED(tempObj, iPed, iFleeFromDropOffDist, ciRULE_TYPE_OBJECT, iID)) )
						RETURN niTarget
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE ciRULE_TYPE_TRAIN
			IF iID < FMMC_MAX_TRAINS
				niTarget = MC_serverBD_1.sFMMC_SBD.niTrain[iID]
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(niTarget)
					VEHICLE_INDEX tempTrain
					tempTrain = NET_TO_VEH(niTarget)
					
					IF IS_ENTITY_ALIVE(tempTrain)
					AND ( IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitset, ciPed_BS_DontFleeDropOffs) OR (NOT IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED(tempTrain, iPed, iFleeFromDropOffDist, ciRULE_TYPE_TRAIN, iID)) )
						RETURN niTarget
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN NULL
	
ENDFUNC

FUNC BOOL HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY(INT iEntityType, INT iEntityID)
	
	IF iEntityType = ciRULE_TYPE_NONE
	OR iEntityID = -1
		RETURN FALSE
	ENDIF
	
	SWITCH iEntityType
		
		CASE ciRULE_TYPE_PED
			PRINTLN("[TargetEntity] HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY - ciRULE_TYPE_PED")
			IF (iEntityID >= 0) AND (iEntityID < FMMC_MAX_PEDS)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntityID].iFollow = ciPed_FOLLOW_ON
				AND MC_serverBD_2.iPartPedFollows[iEntityID] != -1
					PRINTLN("[TargetEntity] HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY - Returning TRUE, MC_serverBD_2.iPartPedFollows[iEntityID] = ",MC_serverBD_2.iPartPedFollows[iEntityID])
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_VEHICLE
			PRINTLN("[TargetEntity] HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY - ciRULE_TYPE_VEHICLE")
			IF (iEntityID >= 0) AND (iEntityID < FMMC_MAX_VEHICLES)
				
				NETWORK_INDEX niEntity
				niEntity = MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID]
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niEntity)
					
					PED_INDEX tempDriver
					tempDriver = GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(niEntity))
					
					IF NOT IS_PED_INJURED(tempDriver)
						IF IS_PED_A_PLAYER(tempDriver)
							PRINTLN("[MMacK][TargetEntity] HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY - A player is in the driver's seat!")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_OBJECT
			PRINTLN("[TargetEntity] HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY - ciRULE_TYPE_OBJECT")
			IF MC_serverBD.iObjCarrier[iEntityID] != -1
				PRINTLN("[TargetEntity] HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY - MC_serverBD.iObjCarrier[iEntityID] = ",MC_serverBD.iObjCarrier[iEntityID])
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_GOTO
			PRINTLN("[TargetEntity] HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY - ciRULE_TYPE_GOTO")
			
			BOOL bSomeoneInArea
			INT iTeam
			
			FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
				IF MC_serverBD_1.inumberOfTeamInArea[iEntityID][iTeam] > 0
					bSomeoneInArea = TRUE
					iTeam = MC_serverBD.iNumberOfTeams
				ENDIF
			ENDFOR
			
			IF bSomeoneInArea
				PRINTLN("[TargetEntity] HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY - Someone is in the area!")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_TRAIN
			PRINTLN("[TargetEntity] HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY - ciRULE_TYPE_TRAIN")
			IF (iEntityID >= 0) AND (iEntityID < FMMC_MAX_TRAINS)
				
				NETWORK_INDEX niEntity
				niEntity = MC_serverBD_1.sFMMC_SBD.niTrain[iEntityID]
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niEntity)
					
					PED_INDEX tempDriver
					tempDriver = GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(niEntity))
					
					IF NOT IS_PED_INJURED(tempDriver)
						IF IS_PED_A_PLAYER(tempDriver)
							PRINTLN("[TargetEntity] HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY - A player is in the driver's seat!")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_GOT_CONTROL_OF_PRIMARY_CUSTOM_TARGET_ENTITY(INT iPed)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEleven, ciPed_BSEleven_DeactivatePrimaryTaskWhenPlayerControlsTarget)
		RETURN HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPrimaryTargetOverrideType, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPrimaryTargetOverrideID)
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_PLAYER_GOT_CONTROL_OF_SECONDARY_CUSTOM_TARGET_ENTITY(INT iPed)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSeven, ciPed_BSSeven_DeactivateSecondaryTaskWhenPlayerControlsTarget)
		RETURN HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryTargetOverrideType, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryTargetOverrideID)
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC INT GET_PED_OBJECTIVE_TARGET_LOCATION(INT iPed, INT iRule, INT iTeam)
	
	INT i
	FLOAT fmaxdist = 999999
	INT ilocation = -1
	
	IF MC_serverBD.iNumLocCreated > 0
	AND MC_serverBD.iNumLocCreated <= FMMC_MAX_GO_TO_LOCATIONS
		FOR i = 0 TO (MC_serverBD.iNumLocCreated-1)
			IF IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE(ciRULE_TYPE_GOTO,i,iTeam,iRule)
				IF NOT IS_VECTOR_ZERO(GET_LOCATION_VECTOR(i))
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed]),GET_LOCATION_VECTOR(i)) < fmaxdist
						ilocation = i
						fmaxdist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed]),GET_LOCATION_VECTOR(i))
					ENDIF
				ELSE
					NET_PRINT("vector is 0") NET_NL()
				ENDIF
			ELSE
				NET_PRINT(" location is not valid priority: ") NET_PRINT_INT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iPriority[iTeam]) NET_PRINT(" associated objective : ") NET_PRINT_INT(iRule) NET_PRINT(" associated team : ") NET_PRINT_INT(iTeam) NET_NL()
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN ilocation
	
ENDFUNC

FUNC PLAYER_INDEX GET_PED_OBJECTIVE_TARGET_PLAYER(INT iPed, INT iTeam, INT iPriority, INT iCustomTargetTeam = -1)

	PLAYER_INDEX TargetPlayerID = INVALID_PLAYER_INDEX()
	INT iTargetTeam = -1
	
	IF iCustomTargetTeam = -1
		NET_PRINT("iPriority is: ") NET_PRINT_INT(iPriority) NET_PRINT("for ped: ") NET_PRINT_INT(iPed)  NET_NL()
		NET_PRINT("iTeam is: ") NET_PRINT_INT(iTeam) NET_PRINT("for ped: ") NET_PRINT_INT(iPed)  NET_NL()
		NET_PRINT("iRule selected is: ") NET_PRINT_INT(g_FMMC_STRUCT.sPlayerRuleData[iPriority].iRule[iTeam]) NET_PRINT("for ped: ") NET_PRINT_INT(iPed)  NET_NL()
		
		IF g_FMMC_STRUCT.sPlayerRuleData[iPriority].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_KILL_PLAYERS
			iTargetTeam = -1
		ELIF g_FMMC_STRUCT.sPlayerRuleData[iPriority].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM0
			iTargetTeam = 0
		ELIF g_FMMC_STRUCT.sPlayerRuleData[iPriority].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM1
			iTargetTeam = 1
		ELIF g_FMMC_STRUCT.sPlayerRuleData[iPriority].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM2
			iTargetTeam = 2
		ELIF g_FMMC_STRUCT.sPlayerRuleData[iPriority].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM3
			iTargetTeam = 3
		ENDIF
	ELSE
		iTargetTeam = iCustomTargetTeam
	ENDIF
	
	NET_PRINT("target team selected is: ") NET_PRINT_INT(iTargetTeam) NET_PRINT("for ped: ") NET_PRINT_INT(iPed)  NET_NL()	
	
	ENTITY_INDEX entPed = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
	
	IF iTargetTeam != -1
		TargetPlayerID = INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(entPed, iTargetTeam))
	ELSE
		TargetPlayerID = INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY(entPed))
	ENDIF
	
	NET_PRINT("target player selected is: ") NET_PRINT_INT(NATIVE_TO_INT(TargetPlayerID)) NET_PRINT("for ped: ") NET_PRINT_INT(iPed)  NET_NL()	
	
	RETURN TargetPlayerID
			
ENDFUNC

FUNC BOOL ARE_ALL_TARGETS_DEAD(FMMC_PED_STATE &sPedState)
	
	INT iPed = sPedState.iIndex
	
	INT i
	FMMC_OBJECT_STATE sObjState
	FMMC_VEHICLE_STATE sVehState
	
	SWITCH MC_serverBD.iTargetType[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedObjective][g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedTeam]

		CASE ci_TARGET_PED
		
			IF MC_serverBD.iNumPedCreated > 0
			AND MC_serverBD.iNumPedCreated <= FMMC_MAX_PEDS
				FOR i = 0 TO (MC_serverBD.iNumPedCreated-1)
					IF i != iPed
						IF IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE(ciRULE_TYPE_PED,i,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedTeam,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedObjective)
							IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
							OR ( (MC_serverBD_2.iCurrentPedRespawnLives[i] > 0) AND SHOULD_PED_RESPAWN_NOW(sPedState) )
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
			
		BREAK
		
		CASE ci_TARGET_VEHICLE
			IF MC_serverBD.iNumVehCreated > 0
			AND MC_serverBD.iNumVehCreated <= FMMC_MAX_VEHICLES
				FOR i = 0 TO (MC_serverBD.iNumVehCreated-1)
					IF IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE(ciRULE_TYPE_VEHICLE, i, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedObjective)
						FILL_FMMC_VEHICLE_STATE_STRUCT(sVehState, i, TRUE)
						IF sVehState.bDrivable
						OR (GET_VEHICLE_RESPAWNS(i) > 0 AND SHOULD_VEH_RESPAWN_NOW(sVehState))
							RETURN FALSE
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		BREAK
		
		CASE ci_TARGET_OBJECT
			IF MC_serverBD.iNumObjCreated > 0
			AND MC_serverBD.iNumObjCreated <= FMMC_MAX_NUM_OBJECTS
				FOR i = 0 TO (MC_serverBD.iNumObjCreated-1)
					IF IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE(ciRULE_TYPE_OBJECT,i,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedTeam,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedObjective)
						IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(i))
						OR ( (MC_serverBD_2.iCurrentObjRespawnLives[i] > 0) AND SHOULD_OBJ_RESPAWN_NOW(i, sObjState) )
							RETURN FALSE
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN TRUE


ENDFUNC

FUNC NETWORK_INDEX GET_PED_OBJECTIVE_TARGET_NET_ID(INT iPed, INT iRule, INT iTeam)

	INT i
	FLOAT fTempDist2
	FLOAT fmaxdist2 = 999999999
	NETWORK_INDEX TargetNetID= NULL
	PED_INDEX ThisPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
	VECTOR vPed = GET_ENTITY_COORDS(ThisPed)
	
	PED_INDEX tempPed
	VEHICLE_INDEX tempVeh
	OBJECT_INDEX tempObj
	
	BOOL bVehicleUsed
	BOOL bDontFleeDropOffs = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitset, ciPed_BS_DontFleeDropOffs)
	INT iFleeFromDropOffDist = 150
	
	IF IS_PED_IN_ANY_HELI(ThisPed) OR IS_PED_IN_ANY_PLANE(ThisPed)
		iFleeFromDropOffDist = 350
	ENDIF
	
	PRINTLN("[RCC MISSION] GET_PED_OBJECTIVE_TARGET_NET_ID - iPed: ", iPed, " MC_serverBD.iTargetType: ", MC_serverBD.iTargetType[iRule][iTeam])
	
	SWITCH MC_serverBD.iTargetType[iRule][iTeam]
		
		CASE ci_TARGET_PED
			
			IF MC_serverBD.iNumPedCreated > 0
			AND MC_serverBD.iNumPedCreated <= FMMC_MAX_PEDS
				FOR i = 0 TO (MC_serverBD.iNumPedCreated-1)
					IF i != iPed
						IF IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE(ciRULE_TYPE_PED,i,iTeam,iRule)
							IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
								
								tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])
								
								fTempDist2 = VDIST2(vPed, GET_ENTITY_COORDS(tempPed))
								
								IF fTempDist2 < fmaxdist2
								AND ( bDontFleeDropOffs
									 OR (NOT IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED(tempPed, iPed, iFleeFromDropOffDist, ciRULE_TYPE_PED, i)))
									TargetNetID = MC_serverBD_1.sFMMC_SBD.niPed[i]
									fmaxdist2 = fTempDist2
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
			
		BREAK
		
		CASE ci_TARGET_VEHICLE
			IF MC_serverBD.iNumVehCreated > 0
			AND MC_serverBD.iNumVehCreated <= FMMC_MAX_VEHICLES
				FOR i = 0 TO (MC_serverBD.iNumVehCreated-1)
					IF IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE(ciRULE_TYPE_VEHICLE, i, iTeam, iRule)
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
							
							tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
							
							IF IS_VEHICLE_DRIVEABLE(tempVeh)
								
								bVehicleUsed = FALSE
								
								IF MC_serverBD_2.iAssociatedAction[iPed] = ciACTION_GOTO
								AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedEscortRange = 0
								AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vPedHeliEscortOffset)
									
									IF GET_PED_PREFERRED_SEAT_FROM_CREATOR_OPTION_INT(iPed) = ciFMMC_SeatPreference_None
										//Still use this checking the driver seat here for legacy missions
										tempPed = GET_PED_IN_VEHICLE_SEAT(tempVeh)
										
										IF NOT IS_PED_INJURED(tempPed)
											IF NOT IS_PED_A_PLAYER(tempPed)
												bVehicleUsed = TRUE
											ENDIF
										ENDIF
									ELSE
										VEHICLE_SEAT vsSeat
										vsSeat = GET_VEHICLE_SEAT_PED_SHOULD_ENTER(tempVeh, iPed)
										
										IF vsSeat = VS_ANY_PASSENGER
											bVehicleUsed = TRUE // No free seats
										ENDIF
									ENDIF
								ENDIF
								
								NET_PRINT("[RCC MISSION] GET_PED_OBJECTIVE_TARGET_NET_ID iAssociatedAction: ") NET_PRINT_INT(MC_serverBD_2.iAssociatedAction[iPed]) NET_NL()
								PRINTLN("[RCC MISSION] GET_PED_OBJECTIVE_TARGET_NET_ID Checking niVehicle i = ", i, " bVehicleUsed = ", bVehicleUsed, " iFleeFromDropOffDist = ", iFleeFromDropOffDist)
								
								IF NOT bVehicleUsed
									
									fTempDist2 = VDIST2(vPed, GET_ENTITY_COORDS(tempVeh))
									
									IF fTempDist2 < fmaxdist2
									AND ( bDontFleeDropOffs
										 OR (NOT IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED(tempVeh, iPed, iFleeFromDropOffDist, ciRULE_TYPE_VEHICLE, i #IF IS_DEBUG_BUILD, TRUE #ENDIF)))
										TargetNetID = MC_serverBD_1.sFMMC_SBD.niVehicle[i]
										
										fmaxdist2 = fTempDist2
										
										PRINTLN("[RCC MISSION] GET_PED_OBJECTIVE_TARGET_NET_ID assigning target vehicle number: ", i, " Don't flee bit set = ",  IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitset, ciPed_BS_DontFleeDropOffs), " new fmaxdist2 = ", fmaxdist2)
									#IF IS_DEBUG_BUILD
									ELSE
										IF fTempDist2 >= fmaxdist2
											PRINTLN("[RCC MISSION] GET_PED_OBJECTIVE_TARGET_NET_ID Rejecting niVehicle i = ", i, " because fTempDist2 = ", fTempDist2, " >= fmaxdist2 = ", fmaxdist2 )
										ENDIF
									#ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] GET_PED_OBJECTIVE_TARGET_NET_ID veh ", i, " priority is not equal for iTeam: ", iTeam)
					ENDIF
				ENDFOR
			ELSE
				NET_PRINT("[RCC MISSION] GET_PED_OBJECTIVE_TARGET_NET_ID veh created is 0") NET_NL()
			ENDIF
		BREAK
		
		CASE ci_TARGET_OBJECT
			IF MC_serverBD.iNumObjCreated > 0
			AND MC_serverBD.iNumObjCreated <= FMMC_MAX_NUM_OBJECTS
				FOR i = 0 TO (MC_serverBD.iNumObjCreated - 1)
					IF IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE(ciRULE_TYPE_OBJECT,i,iTeam,iRule)
						IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(i))
							
							tempObj = NET_TO_OBJ(GET_OBJECT_NET_ID(i))
							
							IF NOT IS_ENTITY_DEAD(tempObj)
								
								fTempDist2 = VDIST2(vPed, GET_ENTITY_COORDS(tempObj))
								
								IF fTempDist2 < fmaxdist2
								AND ( bDontFleeDropOffs
									 OR (NOT IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED(tempObj, iPed, iFleeFromDropOffDist, ciRULE_TYPE_OBJECT, i)))
									TargetNetID = GET_OBJECT_NET_ID(i)
									fmaxdist2 = fTempDist2
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN TargetNetID
	
ENDFUNC

FUNC BOOL DOES_PED_HAVE_VALID_CUSTOM_TARGET(INT iPed, INT iCustomType, INT iCustomID, BOOL bForceNewTarget, BOOL& bWillSpawn)
	
	BOOL bHasTarget
	
	bWillSpawn = FALSE
	
	PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_CUSTOM_TARGET - iCustomType ", iCustomType)
	SWITCH iCustomType
		
		CASE ciRULE_TYPE_GOTO
			IF MC_serverBD_2.iTargetID[iPed] = -1
			OR bForceNewTarget
				IF iCustomID < FMMC_MAX_GO_TO_LOCATIONS
					MC_serverBD_2.iTargetID[iPed] = iCustomID
					bHasTarget = TRUE
				ENDIF
			ELSE
				bHasTarget = TRUE
			ENDIF
			
			IF bHasTarget
				SET_PED_STATE(iPed,ciTASK_GOTO_COORDS)
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_PLAYER
			IF MC_serverBD.TargetPlayerID[iPed] = INVALID_PLAYER_INDEX()
			OR bForceNewTarget
				MC_serverBD.TargetPlayerID[iPed] = GET_PED_OBJECTIVE_TARGET_PLAYER(iPed, -1, -1, iCustomID)
				
				IF MC_serverBD.TargetPlayerID[iPed] != INVALID_PLAYER_INDEX()
					bHasTarget = TRUE
				ENDIF
			ELSE
				bHasTarget = TRUE
			ENDIF
			
			IF bHasTarget
				IF MC_serverBD_2.iAssociatedAction[iPed] = ciACTION_GOTO
					SET_PED_STATE(iPed,ciTASK_GOTO_PLAYER)
				ELIF MC_serverBD_2.iAssociatedAction[iPed] = ciACTION_DEFEND
					SET_PED_STATE(iPed,ciTASK_DEFEND_PLAYER)
				ELIF MC_serverBD_2.iAssociatedAction[iPed] = ciACTION_ATTACK
					SET_PED_STATE(iPed,ciTASK_ATTACK_PLAYER)
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_CUSTOM_TARGET - has a valid target, but no MC_serverBD_2.iAssociatedAction[iPed]")
				#ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_PED
		CASE ciRULE_TYPE_VEHICLE
		CASE ciRULE_TYPE_OBJECT
		CASE ciRULE_TYPE_TRAIN
			IF MC_serverBD.niTargetID[iPed] = NULL
			OR bForceNewTarget
				
				MC_serverBD.niTargetID[iPed] = GET_PED_CUSTOM_TARGET_NETWORK_ID(iPed, iCustomType, iCustomID, MC_serverBD_2.iAssociatedAction[iPed])
				
				IF MC_serverBD.niTargetID[iPed] != NULL
					bHasTarget = TRUE
				ELSE
					
					INT iRespawnCount, iSpawnType, iSpawnTeam, iSpawnRule
					
					SWITCH iCustomType
						CASE ciRULE_TYPE_PED
							iRespawnCount = MC_serverBD_2.iCurrentPedRespawnLives[iCustomID]
							iSpawnTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCustomID].iAssociatedTeam
							iSpawnRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCustomID].iAssociatedTeam
							iSpawnType = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCustomID].iAssociatedSpawn
						BREAK
						CASE ciRULE_TYPE_VEHICLE
							iRespawnCount = GET_VEHICLE_RESPAWNS(iCustomID)
							iSpawnTeam = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCustomID].iAssociatedTeam
							iSpawnRule = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCustomID].iAssociatedObjective
							iSpawnType = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCustomID].iAssociatedSpawn
						BREAK
						CASE ciRULE_TYPE_OBJECT
							iRespawnCount = MC_serverBD_2.iCurrentObjRespawnLives[iCustomID]
							iSpawnTeam = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCustomID].iAssociatedTeam
							iSpawnRule = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCustomID].iAssociatedObjective
							iSpawnType = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCustomID].iAssociatedSpawn
						BREAK
						CASE ciRULE_TYPE_TRAIN
							iRespawnCount = 1 // Can spawn mid-mission but technically does not have "respawns".
							iSpawnTeam = g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iCustomID].iTrain_AssociatedTeam
							iSpawnRule = g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iCustomID].iTrain_AssociatedRule
							iSpawnType = g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iCustomID].iTrain_AssociatedRuleSpawnLimit
						BREAK
					ENDSWITCH
					
					IF iRespawnCount > 0
						IF iSpawnTeam = -1
						OR iSpawnRule = -1
						OR (iSpawnType != ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY)
						OR (MC_serverBD_4.iCurrentHighestPriority[iSpawnTeam] <= iSpawnRule) // If the entity will only spawn during one rule, check that we haven't passed that rule yet
							bWillSpawn = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				bHasTarget = TRUE
			ENDIF
			
			IF bHasTarget
				IF MC_serverBD_2.iAssociatedAction[iPed] = ciACTION_GOTO
					SET_PED_STATE(iPed,ciTASK_GOTO_ENTITY)
					PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_CUSTOM_TARGET - got valid target - goto")
				ELIF MC_serverBD_2.iAssociatedAction[iPed] = ciACTION_DEFEND
					SET_PED_STATE(iPed,ciTASK_DEFEND_ENTITY)
					PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_CUSTOM_TARGET - got valid target - defend")
				ELIF MC_serverBD_2.iAssociatedAction[iPed] = ciACTION_ATTACK
					SET_PED_STATE(iPed,ciTASK_ATTACK_ENTITY)
					PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_CUSTOM_TARGET - got valid target - attack")
				ELIF MC_serverBD_2.iAssociatedAction[iPed] = ciACTION_AIM_AT
					SET_PED_STATE(iPed,ciTASK_AIM_AT_ENTITY)
					PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_CUSTOM_TARGET - got valid target - aim at")
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_CUSTOM_TARGET - has a valid target, but no MC_serverBD_2.iAssociatedAction[iPed]")
				#ENDIF
				ENDIF
			ELSE
				PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_CUSTOM_TARGET - has no valid target")
			ENDIF
		BREAK
		
	ENDSWITCH
	
	PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_CUSTOM_TARGET - bHasTarget: ", bHasTarget, " bWillSpawn: ", bWillSpawn)
	
	RETURN bHasTarget
	
ENDFUNC

FUNC BOOL SET_PED_HAS_VALID_GOTO(FMMC_PED_STATE &sPedState)

	IF SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(sPedState, DEFAULT #IF IS_DEBUG_BUILD , 12 #ENDIF)	
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - SET_PED_HAS_VALID_GOTO - Returning false as SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK is true")
		RETURN FALSE
	ENDIF
	
	INT iPed = sPedState.iIndex
	
	IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAtGotoLocBitset, iPed)
	
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.sPointData[0].vPosition)
			
			IF MC_serverBD_2.iPedGotoProgress[iPed] < 0
			OR MC_serverBD_2.iPedGotoProgress[iPed] >= MAX_ASSOCIATED_GOTO_TASKS
				PRINTLN("[Peds][Ped ", iPed, "] - SET_PED_HAS_VALID_GOTO - Returning false as MC_serverBD_2.iPedGotoProgress[iPed] = ", MC_serverBD_2.iPedGotoProgress[iPed])	
				RETURN FALSE
			ENDIF
			
			INT iAssociatedRule = GET_ASSOCIATED_GOTO_TASK_DATA__ASSOCIATED_RULE(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed])
			VECTOR vPlayerActivatedPosition = GET_ASSOCIATED_GOTO_TASK_DATA__PLAYER_ACTIIVATED_POSITION(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed])
			INT iPlayerActivatedRadius = GET_ASSOCIATED_GOTO_TASK_DATA__PLAYER_ACTIIVATED_RADIUS(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed])
			
			BOOL bDoGoto			
			BOOL bDependsOnRule = (iAssociatedRule != -1) AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedTeam != -1)
			BOOL bDependsOnArea = (NOT IS_VECTOR_ZERO(vPlayerActivatedPosition)) AND (iPlayerActivatedRadius > 0)
			
			IF (bDependsOnRule OR bDependsOnArea)
				
				PRINTLN("[Peds][Ped ", iPed, "] - SET_PED_HAS_VALID_GOTO - bDependsOnRule: ", bDependsOnRule, " bDependsOnArea: ", bDependsOnArea, " iAssociatedRule: ", iAssociatedRule)
				
				IF bDependsOnRule
					IF (MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedTeam] >= iAssociatedRule)
						IF bDependsOnArea 
							IF IS_ANY_FMMC_PLAYER_WITHIN_AREA(vPlayerActivatedPosition, iPlayerActivatedRadius)
								bDoGoto = TRUE
							ENDIF
						ELSE
							bDoGoto = TRUE
						ENDIF
					ELSE						
						PRINTLN("[Peds][Ped ", iPed, "] - SET_PED_HAS_VALID_GOTO - Rule fail.")
					ENDIF
				ELIF bDependsOnArea
					IF IS_ANY_FMMC_PLAYER_WITHIN_AREA(vPlayerActivatedPosition, iPlayerActivatedRadius)
						bDoGoto = TRUE
					ENDIF
				ENDIF
				
			ELSE
				bDoGoto = TRUE
			ENDIF
			
			
			IF bDoGoto
				IF MC_serverBD_2.iPedState[iPed] != ciTASK_GOTO_COORDS
					SET_PED_STATE(iPed, ciTASK_GOTO_COORDS)
				ENDIF
				MC_serverBD_2.iTargetID[iPed] = ciINITIAL_GOTO
				PRINTLN("[Peds][Ped ", iPed, "] - SET_PED_HAS_VALID_GOTO - Returning TRUE")
				RETURN TRUE
			ENDIF
			
		ELSE
			PRINTLN("[Peds][Ped ", iPed, "] - SET_PED_HAS_VALID_GOTO - Returning false as vAssociatedVector = VECTOR ZERO")	
			FMMC_SET_LONG_BIT(MC_serverBD.iPedAtGotoLocBitset, iPed)
		ENDIF
	ELSE
		PRINTLN("[Peds][Ped ", iPed, "] - SET_PED_HAS_VALID_GOTO - Returning false as iPedAtGotoLocBitset is set")
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SET_PED_HAS_VALID_SECONDARY_GOTO(FMMC_PED_STATE &sPedState)
	
	IF SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(sPedState, DEFAULT #IF IS_DEBUG_BUILD , 11 #ENDIF)
		RETURN FALSE
	ENDIF
	
	INT iPed = sPedState.iIndex
	
	IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAtSecondaryGotoLocBitset, iPed)
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vSecondaryVector)
			
			IF MC_serverBD_2.iPedState[iPed] != ciTASK_GOTO_COORDS
				SET_PED_STATE(iPed,ciTASK_GOTO_COORDS)
			ENDIF
			
			PRINTLN("[RCC MISSION] SET_PED_HAS_VALID_SECONDARY_GOTO - ped ",iPed," set ped to do secondary goto and cancel primary goto by setting iPedAtGotoLocBitset")
			FMMC_SET_LONG_BIT(MC_serverBD.iPedAtGotoLocBitset, iPed)
			MC_serverBD_2.iTargetID[iPed] = ciSECONDARY_GOTO
			
			RETURN TRUE
		ELSE
			FMMC_SET_LONG_BIT(MC_serverBD.iPedAtSecondaryGotoLocBitset, iPed)
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_PED_FINISHED_GOTO_INTERRUPT(INT iPed)

	INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryTeam
	INT iRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryObjective // MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	PRINTLN("[RCC MISSION] PROCESS_PED_FINISHED_GOTO_INTERRUPT - Ped ", iPed, " has finished their goto interrupt!|| Rule: ", iRule, " for team ", iTeam)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwelve, ciPed_BSTwelve_PassRuleWhenFinishingSecondaryGoto)
		PRINTLN("[RCC MISSION] PROCESS_PED_FINISHED_GOTO_INTERRUPT - Progress ped rule objective for team ", iTeam, " because ped ", iPed, " finished GoTo, MC_serverBD_4.iCurrentHighestPriority[iTeam] ", MC_serverBD_4.iCurrentHighestPriority[iTeam], ", iRule ", iRule)
		
		INVALIDATE_SINGLE_OBJECTIVE(iTeam, iRule, TRUE, TRUE)
		SET_IGNORE_CHECKPOINT_PROCESSING_FOR_RULE_SKIP(iTeam)
		
		SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, iRule, OBJ_END_REASON_PED_ARRIVED)
		BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(OBJ_END_REASON_PED_ARRIVED, iTeam, iRule, DID_TEAM_PASS_OBJECTIVE(iTeam, iRule))
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwelve, ciPed_BSTwelve_FailMissionWhenFinishingSecondaryGoto)
	
		PRINTLN("[RCC MISSION] PROCESS_PED_FINISHED_GOTO_INTERRUPT - Failing mission on rule ", iRule, " for team ", iTeam)
		
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_ARRIVED
		
		REQUEST_SET_TEAM_FAILED(iTeam, mFail_PED_ARRIVED)
		
		MC_serverBD.iEntityCausingFail[iTeam] = iPed
		SET_TEAM_OBJECTIVE_OUTCOME(iTeam, iRule, FALSE)
		SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, iRule, MC_serverBD.iReasonForObjEnd[iTeam],TRUE)
		BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, iRule, DID_TEAM_PASS_OBJECTIVE(iTeam, iRule), TRUE)
	ENDIF
	
ENDPROC

FUNC BOOL HAS_ANY_TEAM_HIT_PED_TASK_ACTIVATION(INT iPed)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCSTaskActivationSceneIndex > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCSTaskActivationShotIndex > -1
		// If the ped's tasks are supposed to start on a cutscene, then that takes priority over the rest of the associated objective stuff
		IF MC_serverBD.iSpawnScene = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCSTaskActivationSceneIndex
		AND MC_serverBD.iSpawnShot >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCSTaskActivationShotIndex
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedObjective != -1
		//The logic here is the same as for spawning, so use the same function:
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_PEDS, iPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedActionStart, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedAlwaysForceSpawnOnRule, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAggroIndexBS_Entity_Ped, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedObjectiveEnd)
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondAssociatedTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondAssociatedObjective != -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_PEDS, iPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondActionStart, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondAlwaysForceSpawnOnRule, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAggroIndexBS_Entity_Ped, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedObjectiveEnd)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iThirdAssociatedTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iThirdAssociatedObjective != -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_PEDS, iPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iThirdActionStart, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iThirdAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iThirdAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iThirdScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iThirdAlwaysForceSpawnOnRule, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAggroIndexBS_Entity_Ped, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedObjectiveEnd)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFourthAssociatedTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFourthAssociatedObjective != -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_PEDS, iPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFourthActionStart, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFourthAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFourthAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFourthScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFourthAlwaysForceSpawnOnRule, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAggroIndexBS_Entity_Ped, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedObjectiveEnd)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL HAS_ANY_TEAM_HIT_PED_SECONDARY_TASK_ACTIVATION(INT iPed)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryObjective != -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_PEDS, iPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryActionStart, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryObjective, DEFAULT, DEFAULT, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAggroIndexBS_Entity_Ped)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Checks if the ped is going in/out of range to be tasked with Actions focusing on this target entity
/// PARAMS:
///    tempPed - The ped index of the ped doing the targetting
///    iPed - The creator index of the ped doing the targetting
///    Don't stop bLeaving - Hold on to that feeling. Set this to TRUE if you're trying to see if the ped is going to leave this radius and should be un-tasked again.
///    iTriggerDistance - The distance a ped needs to be near to their custom target to be able to do their tasks.
///    iCustomType - The rule type of their target (choose from ciRULE_TYPE_NONE/PED/VEHICLE/etc)
///    iCustomID - The creator ID of their target.
/// RETURNS:
///    If the ped is within range to be tasked with Actions focusing on this target entity
FUNC BOOL IS_PED_WITHIN_RANGE_OF_CUSTOM_TARGET_ENTITY(PED_INDEX tempPed, INT iPed, BOOL bLeaving, INT iTriggerDistance, INT iCustomType, INT iCustomID)
	
	BOOL bInRange
	UNUSED_PARAMETER(iPed)
	
	IF iTriggerDistance != -1
	AND iCustomType != ciRULE_TYPE_NONE
	AND iCustomID != -1
		VECTOR vPed = GET_ENTITY_COORDS(tempPed)
		
		VECTOR vEntity = GET_FMMC_ENTITY_LOCATION(vPed, iCustomType, iCustomID, tempPed)
		
		IF NOT IS_VECTOR_ZERO(vEntity)
			
			FLOAT fDist2
			
			IF bLeaving
				FLOAT fDistToAdd = iTriggerDistance * 0.25
				
				IF fDistToAdd < 5
					fDistToAdd = 5
				ENDIF
				
				fDist2 = (iTriggerDistance + fDistToAdd) * (iTriggerDistance + fDistToAdd)
			ELSE
				fDist2 = TO_FLOAT(iTriggerDistance * iTriggerDistance)
			ENDIF
			
			IF VDIST2(vPed, vEntity) <= fDist2
				PRINTLN("[RCC MISSION] IS_PED_WITHIN_RANGE_OF_CUSTOM_TARGET_ENTITY - Returning TRUE for ped ",iPed," & iCustomType ",iCustomType,", iCustomID ",iCustomID,", bLeaving ",bLeaving)
				bInRange = TRUE
			ELSE
				PRINTLN("[RCC MISSION] IS_PED_WITHIN_RANGE_OF_CUSTOM_TARGET_ENTITY - Returning FALSE for ped ",iPed," & iCustomType ",iCustomType,", iCustomID ",iCustomID,", bLeaving ",bLeaving)
			ENDIF
		ENDIF
	ELSE
		bInRange = TRUE
	ENDIF
	
	RETURN bInRange
	
ENDFUNC

FUNC BOOL IS_PED_WITHIN_RANGE_OF_PRIMARY_CUSTOM_TARGET_ENTITY(PED_INDEX tempPed, INT iPed, BOOL bLeaving)
	
	RETURN IS_PED_WITHIN_RANGE_OF_CUSTOM_TARGET_ENTITY(tempPed, iPed, bLeaving, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPrimaryActionTriggerDistanceFromTarget, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPrimaryTargetOverrideType, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPrimaryTargetOverrideID)
	
ENDFUNC

FUNC BOOL IS_PED_WITHIN_RANGE_OF_SECONDARY_CUSTOM_TARGET_ENTITY(PED_INDEX tempPed, INT iPed, BOOL bLeaving)
	
	RETURN IS_PED_WITHIN_RANGE_OF_CUSTOM_TARGET_ENTITY(tempPed, iPed, bLeaving, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryActionTriggerDistanceFromTarget, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryTargetOverrideType, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryTargetOverrideID)
	
ENDFUNC

FUNC BOOL IS_CUSTOM_TARGET_ENTITY_DEAD(PED_INDEX tempPed, INT iCustomType, INT iCustomID)
	
	SWITCH iCustomType
		
		CASE ciRULE_TYPE_GOTO
			IF iCustomID < FMMC_MAX_GO_TO_LOCATIONS
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_PLAYER
			IF iCustomID < FMMC_MAX_TEAMS
				IF MC_serverBD.iNumberOfPlayingPlayers[iCustomID] > 0
					
					INT iPlayer
					iPlayer = GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(tempPed, iCustomID)
					
					IF iPlayer != -1
						
						PLAYER_INDEX tempPlayer
						tempPlayer = INT_TO_PLAYERINDEX(iPlayer)
						
						IF NOT IS_PLAYER_DEAD(tempPlayer)
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_OBJECT
			IF iCustomID < FMMC_MAX_NUM_OBJECTS
				IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iCustomID))
					
					OBJECT_INDEX tempObj
					tempObj = NET_TO_OBJ(GET_OBJECT_NET_ID(iCustomID))
					
					IF IS_ENTITY_ALIVE(tempObj)
						IF IS_BIT_SET(MC_serverBD.iFragableCrate, iCustomID)
						OR IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCustomID].mn)
							IF NOT IS_OBJECT_BROKEN_AND_VISIBLE(tempObj)
								RETURN FALSE
							ENDIF
						ELSE
							RETURN FALSE
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(MC_serverBD_2.iObjSpawnedOnce, iCustomID)
						// If the object hasn't spawned yet, it can't have died
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_PED
			IF iCustomID < FMMC_MAX_PEDS
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iCustomID])
					
					PED_INDEX targetPed
					targetPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iCustomID])
					
					IF NOT IS_PED_INJURED(targetPed)
						RETURN FALSE
					ENDIF
				ELSE
					IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iObjSpawnPedBitset, iCustomID)
						// If the ped hasn't spawned yet, it can't have died
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_VEHICLE
			IF iCustomID < FMMC_MAX_VEHICLES
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iCustomID])
					
					VEHICLE_INDEX tempVeh
					tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iCustomID])
					
					IF IS_VEHICLE_DRIVEABLE(tempVeh)
						RETURN FALSE
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(MC_serverBD_2.iVehSpawnedOnce, iCustomID)
						// If the vehicle hasn't spawned yet, it can't have died
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
				
		CASE ciRULE_TYPE_TRAIN
			IF iCustomID < FMMC_MAX_TRAINS
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niTrain[iCustomID])
					
					VEHICLE_INDEX tempVeh
					tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niTrain[iCustomID])
					
					IF IS_ENTITY_ALIVE(tempVeh)
						RETURN FALSE
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(MC_serverBD.iTrainSpawnBitset, iCustomID)
						// If the vehicle hasn't spawned yet, it can't have died
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_PRIMARY_CUSTOM_TARGET_ENTITY_IN_CORRECT_ALIVE_STATE_FOR_TASK(PED_INDEX ThisPed, INT iPed, BOOL bLeaving)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPrimaryTargetOverrideType = ciRULE_TYPE_NONE
	OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPrimaryTargetOverrideID = -1
		RETURN TRUE
	ENDIF
	
	IF NOT bLeaving
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEleven, ciPed_BSEleven_ActivatePrimaryTaskWhenCustomTargetDead)
			IF NOT IS_CUSTOM_TARGET_ENTITY_DEAD(ThisPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPrimaryTargetOverrideType, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPrimaryTargetOverrideID)
				RETURN FALSE
			ENDIF
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEleven, ciPed_BSEleven_DeactivatePrimaryTaskWhenCustomTargetDead)
			IF IS_CUSTOM_TARGET_ENTITY_DEAD(ThisPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPrimaryTargetOverrideType, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPrimaryTargetOverrideID)
				RETURN FALSE
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEleven, ciPed_BSEleven_DeactivatePrimaryTaskWhenCustomTargetDead)
			IF IS_CUSTOM_TARGET_ENTITY_DEAD(ThisPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPrimaryTargetOverrideType, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPrimaryTargetOverrideID)
				PRINTLN("[RCC MISSION] IS_PRIMARY_CUSTOM_TARGET_ENTITY_IN_CORRECT_ALIVE_STATE_FOR_TASK - Returning FALSE for ped ",iPed," w bLeaving TRUE, target is dead")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_PRIMARY_CUSTOM_TARGET_ENTITY_IN_CORRECT_STATE_FOR_TASK(INT iPed, PED_INDEX ThisPed, BOOL bLeaving)
	
	IF NOT bLeaving
		IF NOT IS_PED_WITHIN_RANGE_OF_PRIMARY_CUSTOM_TARGET_ENTITY(ThisPed, iPed, FALSE)
			RETURN FALSE
		ENDIF
		
		IF HAS_PLAYER_GOT_CONTROL_OF_PRIMARY_CUSTOM_TARGET_ENTITY(iPed)
			RETURN FALSE
		ENDIF
		
		IF NOT IS_PRIMARY_CUSTOM_TARGET_ENTITY_IN_CORRECT_ALIVE_STATE_FOR_TASK(ThisPed, iPed, FALSE)
			RETURN FALSE
		ENDIF
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEleven, ciPed_BSEleven_DeactivatePrimaryTaskWhenOutOfTriggerDistance)
			IF NOT IS_PED_WITHIN_RANGE_OF_PRIMARY_CUSTOM_TARGET_ENTITY(ThisPed, iPed, TRUE)
				PRINTLN("[RCC MISSION] IS_PRIMARY_CUSTOM_TARGET_ENTITY_IN_CORRECT_STATE_FOR_TASK - Returning FALSE for ped ",iPed," w bLeaving TRUE, out of range of target")
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF HAS_PLAYER_GOT_CONTROL_OF_PRIMARY_CUSTOM_TARGET_ENTITY(iPed) // Checks if the option to do this is set before doing anything silly
			PRINTLN("[RCC MISSION] IS_PRIMARY_CUSTOM_TARGET_ENTITY_IN_CORRECT_STATE_FOR_TASK - Returning FALSE for ped ",iPed," w bLeaving TRUE, player has control of target")
			RETURN FALSE
		ENDIF
		
		IF NOT IS_PRIMARY_CUSTOM_TARGET_ENTITY_IN_CORRECT_ALIVE_STATE_FOR_TASK(ThisPed, iPed, TRUE)
			PRINTLN("[RCC MISSION] IS_PRIMARY_CUSTOM_TARGET_ENTITY_IN_CORRECT_STATE_FOR_TASK - Returning FALSE for ped ",iPed," w bLeaving TRUE, target is not in correct alive state")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_PED_STOP_TASKS(INT iPed)
	
	BOOL bStopTasks = FALSE
	
	INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iStopTasksTeam
	INT iRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iStopTasksRule
	
	IF iTeam != -1
	AND iRule != -1
		IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= iRule			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwelve, ciPed_BSTwelve_ClearPedTasksWhenTrailerDetached)
				IF FMMC_IS_LONG_BIT_SET(iPedTrailerDetachedBitset, iPed)
					bStopTasks = TRUE
				ENDIF
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFour, ciPed_BSFour_StopTasks_OnMidpoint)
					IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iRule)
						bStopTasks = TRUE
					ENDIF
				ELSE
					bStopTasks = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bStopTasks
	
ENDFUNC

PROC MANAGE_PED_ARRIVAL_RESULTS(INT iPed, BOOL bFinishingGoto)
	
	INT iTeam
	INT iPedRule
	
	BOOL bCheckResults = FALSE
	
	BOOL bTriggerOnTaskCompletion = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSix, ciPed_BSSix_PedArrivalResultsTriggerOnTaskCompletion)
	BOOL bPedTaskArrivalResult = FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedTasksArrivalResultsBS, iPed) // so we don't run this more than once
	
	IF bFinishingGoto	
		IF NOT bTriggerOnTaskCompletion
			bCheckResults = TRUE
		ENDIF
	ELSE
		IF bTriggerOnTaskCompletion
		AND NOT bPedTaskArrivalResult
			bCheckResults = TRUE
		ENDIF
	ENDIF
	
	PRINTLN("[Peds][Ped ", iPed, "][Task][SERVER] - MANAGE_PED_ARRIVAL_RESULTS - Processing for Ped: ", iPed)
	PRINTLN("[Peds][Ped ", iPed, "][Task][SERVER] - MANAGE_PED_ARRIVAL_RESULTS - 		bCheckResults: ", bCheckResults)
	PRINTLN("[Peds][Ped ", iPed, "][Task][SERVER] - MANAGE_PED_ARRIVAL_RESULTS - 		bFinishingGoTo: ", bFinishingGoTo)
	PRINTLN("[Peds][Ped ", iPed, "][Task][SERVER] - MANAGE_PED_ARRIVAL_RESULTS - 		bPedTaskArrivalResult: ", bPedTaskArrivalResult)
	PRINTLN("[Peds][Ped ", iPed, "][Task][SERVER] - MANAGE_PED_ARRIVAL_RESULTS - 		bTriggerOnTaskCompletion: ", bTriggerOnTaskCompletion)
	
	IF bCheckResults
	
		FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
			
			iPedRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iGotoProgressesRule[iTeam]
			
			IF IS_LONG_BIT_SET(g_FMMC_STRUCT.iCriticalGoToPedDoubleBS[iTeam], iPed) //Used Externally IS_LONG_BIT_SET
			AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iPedClearFailOnGoTo, iPed)
			
				PRINTLN("[Peds][Ped ", iPed, "][Task][SERVER] - MANAGE_PED_ARRIVAL_RESULTS - Looking for fail on go to complete, iTeam: ", iTeam, ", iPed: ", iPed)
				
				IF IS_TEAM_ON_RIGHT_RULE_TO_DO_PED_ARRIVAL_RESULTS(iTeam, iPed, TRUE)
					MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_ARRIVED
					
					REQUEST_SET_TEAM_FAILED(iTeam,mFail_PED_ARRIVED)
					MC_serverBD.iEntityCausingFail[iTeam] = iPed
					SET_TEAM_OBJECTIVE_OUTCOME(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam],FALSE)
					SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam],MC_serverBD.iReasonForObjEnd[iTeam],TRUE)
					BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam],iTeam,mc_serverBD_4.iCurrentHighestPriority[iTeam],DID_TEAM_PASS_OBJECTIVE(iTeam,mc_serverBD_4.iCurrentHighestPriority[iTeam]),TRUE)
					
					PRINTLN("[Peds][Ped ", iPed, "][Task][SERVER] - MANAGE_PED_ARRIVAL_RESULTS - MISSION FAILED FOR TEAM:",iTeam," because ped ",iPed," arrived at goto loc, mc_serverBD_4.iCurrentHighestPriority[iTeam] ",mc_serverBD_4.iCurrentHighestPriority[iTeam],", iPedRule ",iPedRule)
				ENDIF
				
			ELIF IS_LONG_BIT_SET(g_FMMC_STRUCT.iGoToPedCompletesRuleDoubleBS[iTeam], iPed) //Used Externally IS_LONG_BIT_SET
			
				PRINTLN("[Peds][Ped ", iPed, "][Task][SERVER] - MANAGE_PED_ARRIVAL_RESULTS - Looking for pass on go to complete, iTeam: ", iTeam, ", iPed: ", iPed)
				
				IF IS_TEAM_ON_RIGHT_RULE_TO_DO_PED_ARRIVAL_RESULTS(iTeam, iPed)
					IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
					AND ((IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE8_WAIT_FOR_ALL_PEDS_CONSQ) 
						AND HAVE_ALL_PEDS_TRIGGERED_PASS_RULE_CONSQ(iTeam, iPed))
					OR NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE8_WAIT_FOR_ALL_PEDS_CONSQ))
						IF iPedRule != -1
						AND MC_serverBD_4.iCurrentHighestPriority[iTeam] != iPedRule
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSix, ciPed_BSSix_GotoProgressesRule_InvalidateFutureRule_T0 + iTeam)
								//Invalidate a future rule:
								PRINTLN("[Peds][Ped ", iPed, "][Task][SERVER] - MANAGE_PED_ARRIVAL_RESULTS - Progress future ped rule objective for team ",iTeam," because ped ",iPed," finished GoTo, MC_serverBD_4.iCurrentHighestPriority[iTeam] ",MC_serverBD_4.iCurrentHighestPriority[iTeam],", iPedRule ",iPedRule)
								
								INVALIDATE_SINGLE_OBJECTIVE(iTeam, iPedRule, TRUE)
								
								SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, iPedRule, OBJ_END_REASON_PED_ARRIVED)
								BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(OBJ_END_REASON_PED_ARRIVED, iTeam, iPedRule,DID_TEAM_PASS_OBJECTIVE(iTeam, iPedRule))
								
							ELSE //IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSix, ciPed_BSSix_GotoProgressesRule_InvalidateAllRulesUpToAndIncl_T0 + iTeam)
								//Invalidate all rules up to & including:
								PRINTLN("[Peds][Ped ", iPed, "][Task][SERVER] - MANAGE_PED_ARRIVAL_RESULTS - Progress objectives up to & incl ped rule for team ",iTeam," because ped ",iPed," finished GoTo, MC_serverBD_4.iCurrentHighestPriority[iTeam] ",MC_serverBD_4.iCurrentHighestPriority[iTeam],", iPedRule ",iPedRule)
								
								INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iTeam, iPedRule, TRUE)
								
								MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_ARRIVED
								SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, iPedRule, MC_serverBD.iReasonForObjEnd[iTeam])
								BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, iPedRule,DID_TEAM_PASS_OBJECTIVE(iTeam, iPedRule))
							ENDIF
						ELSE
							PRINTLN("[Peds][Ped ", iPed, "][Task][SERVER] - MANAGE_PED_ARRIVAL_RESULTS - Progress current objective for team ",iTeam," because ped ",iPed," finished GoTo, MC_serverBD_4.iCurrentHighestPriority[iTeam] ",MC_serverBD_4.iCurrentHighestPriority[iTeam],", iPedRule ",iPedRule)
							INVALIDATE_SINGLE_OBJECTIVE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], TRUE, TRUE)
							SET_TEAM_OBJECTIVE_OUTCOME(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam], TRUE)
							MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_ARRIVED
							SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam],MC_serverBD.iReasonForObjEnd[iTeam])
							BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam],iTeam,mc_serverBD_4.iCurrentHighestPriority[iTeam],DID_TEAM_PASS_OBJECTIVE(iTeam,mc_serverBD_4.iCurrentHighestPriority[iTeam]))
						ENDIF
						SET_IGNORE_CHECKPOINT_PROCESSING_FOR_RULE_SKIP(iTeam)
					ENDIF
					
				ENDIF
				
			ELSE
				PRINTLN("[Peds][Ped ", iPed, "][Task][SERVER] - MANAGE_PED_ARRIVAL_RESULTS - No pass/fail on go to complete for iTeam: ", iTeam, ", iPed: ", iPed)
				PRINTLN("[Peds][Ped ", iPed, "][Task][SERVER] - MANAGE_PED_ARRIVAL_RESULTS - 		GoTo complete Fail: ", IS_LONG_BIT_SET(g_FMMC_STRUCT.iCriticalGoToPedDoubleBS[iTeam], iPed)) //Used Externally IS_LONG_BIT_SET
				PRINTLN("[Peds][Ped ", iPed, "][Task][SERVER] - MANAGE_PED_ARRIVAL_RESULTS - 		GoTo complete Clear Fail: ", FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iPedClearFailOnGoTo, iPed))
				PRINTLN("[Peds][Ped ", iPed, "][Task][SERVER] - MANAGE_PED_ARRIVAL_RESULTS - 		GoTo complete Pass Rule: ", IS_LONG_BIT_SET(g_FMMC_STRUCT.iGoToPedCompletesRuleDoubleBS[iTeam], iPed)) //Used Externally IS_LONG_BIT_SET
			ENDIF
		ENDFOR	
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSecondaryTaskActive, iPed)
		IF NOT FMMC_IS_LONG_BIT_SET(iPedCompletedSecondaryGotoBS, iPed)
			PROCESS_PED_FINISHED_GOTO_INTERRUPT(iPed)
			FMMC_SET_LONG_BIT(iPedCompletedSecondaryGotoBS, iPed)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPed_BSEight_DeactivateSecondaryTaskWhenCompleted)
	AND FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSecondaryTaskActive, iPed)
		FMMC_SET_LONG_BIT(MC_serverBD.iPedSecondaryTaskCompleted, iPed)
		FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedSecondaryTaskActive, iPed)
		MC_serverBD.TargetPlayerID[iPed] = INVALID_PLAYER_INDEX()
		MC_serverBD.niTargetID[iPed] = NULL
		SET_PED_STATE(iPed,ciTASK_CHOOSE_NEW_TASK)
		PRINTLN("[Peds][Ped ", iPed, "][Task][SERVER] - MANAGE_PED_ARRIVAL_RESULTS - Deactivating secondary task, ped: ", iPed)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThirteen, ciPed_BSThirteen_AllowRappellingOnGoToArrival)
		SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_ALLOW_RAPPELLING_FROM_HELICOPTERS)
		PRINTLN("[Peds][Ped ", iPed, "][Task][SERVER] - MANAGE_PED_ARRIVAL_RESULTS - Setting MC_serverBD.iServerBitSet8, SBBOOL8_ALLOW_RAPPELLING_FROM_HELICOPTERS")
	ENDIF
	
	IF bFinishingGoto
		FMMC_SET_LONG_BIT(MC_serverBD.iPedAtGotoLocBitset, iPed)
	ELSE
		FMMC_SET_LONG_BIT(MC_serverBD.iPedTasksArrivalResultsBS, iPed)
	ENDIF
	
ENDPROC

/// PURPOSE: This function is the main target choosing function, it will find a target for the ped if it can - bForceNewTarget forces
///    the ped to refresh its current target (whether or not the current target is still active or not); bPrimaryOnly / bSecondaryOnly
///    choose whether the function should check for targets regarding the ped's primary / secondary Actions respectively
FUNC BOOL DOES_PED_HAVE_VALID_TARGET(FMMC_PED_STATE &sPedState, BOOL bForceNewTarget = FALSE, BOOL bPrimaryOnly = FALSE, BOOL bSecondaryOnly = FALSE, BOOL bIncludeRespawningTarget = FALSE)
		
	INT iPed = sPedState.iIndex
	
	IF SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(sPedState, DEFAULT #IF IS_DEBUG_BUILD , 10 #ENDIF)
		RETURN FALSE
	ENDIF
	
	INT iRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedObjective
	INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedTeam
	INT iAction = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedAction
	
	INT iTargetOverrideType = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPrimaryTargetOverrideType
	INT iTargetOverrideID = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPrimaryTargetOverrideID
	
	IF NOT bPrimaryOnly
	AND ( ( FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSecondaryTaskActive, iPed)
		    AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryAction != -1 )
		 OR bSecondaryOnly )
		
		iRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryObjective
		iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryTeam
		iAction = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryAction
		
		iTargetOverrideType = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryTargetOverrideType
		iTargetOverrideID = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryTargetOverrideID
	ENDIF
	
	PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - iTeam: ", iTeam, " irule: ", iRule, " MC_serverBD_2.iTargetID[iPed]: ", MC_serverBD_2.iTargetID[iPed], 
			" iTargetOverrideType:, ", iTargetOverrideType,", iTargetOverrideID: ", iTargetOverrideID,", iAction: ",iAction)
	
	PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - bForceNewTarget: ", bForceNewTarget, ", bPrimaryOnly: ", bPrimaryOnly, ", bSecondaryOnly: ", bSecondaryOnly, ", bIncludeRespawningTarget: ", bIncludeRespawningTarget)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedFixationTarget > -1
	AND IS_RULE_VALID_FOR_PED_FIXATION_STATE(iPed)
		IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_1.iPedFixationTaskEnded, iPed)		
			PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - [PED_FIXATE] - iPed: ", iPed, " iPed Fixate Target: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedFixationTarget, " Checking Target")
			MC_serverBD.niTargetID[iPed] = MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedFixationTarget]
			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD.niTargetID[iPed])
				PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - [PED_FIXATE] - iPed: ", iPed, " iPed Fixate Target: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedFixationTarget, " Setting ciTASK_ATTACK_ENTITY")
				SET_PED_STATE(iPed, ciTASK_ATTACK_ENTITY)
				RETURN TRUE
			ELSE
				MC_serverBD.niTargetID[iPed] = NULL
				PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - [PED_FIXATE] - iPed: ", iPed, " iPed Fixate Target: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedFixationTarget, " Target is NULL")
			ENDIF
		ELSE
			PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - [PED_FIXATE] - iPed: ", iPed, " iPed Fixate Target: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedFixationTarget, " Fixation has ended though!")
		ENDIF
	ENDIF
		
	IF iTargetOverrideType != ciRULE_TYPE_NONE
	AND iTargetOverrideID != -1
	AND iAction != ciACTION_NOTHING
		
		PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - Overrides ---")
		
		IF iAction = ciACTION_COMBAT_MODE
			SET_PED_STATE(iPed,ciTASK_GENERAL_COMBAT #IF IS_DEBUG_BUILD , 0 #ENDIF )
			RETURN TRUE
		ENDIF
		
		BOOL bWillSpawn
		BOOL bValidTarget = DOES_PED_HAVE_VALID_CUSTOM_TARGET(iPed, iTargetOverrideType, iTargetOverrideID, bForceNewTarget, bWillSpawn)
		
		IF (NOT bValidTarget) // If the target doesn't exist right now
		AND (NOT bWillSpawn) // and if the target won't be valid later
		AND iAction = ciACTION_ATTACK
			MANAGE_PED_ARRIVAL_RESULTS(iPed, FALSE)
		ENDIF
		
		IF bIncludeRespawningTarget
		AND NOT bValidTarget
			RETURN bWillSpawn
		ENDIF
		
		RETURN bValidTarget
		
	ENDIF
	
	
	IF iRule != -1
	AND iTeam != -1
	AND iAction != ciACTION_NOTHING		
		
		IF iAction = ciACTION_PRIMARY_GOTO
			PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - Checking for Primary Goto.")
			IF SET_PED_HAS_VALID_GOTO(sPedState)				
				PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - Returning True (PRIMARY GOTO)")
				RETURN TRUE
			ENDIF
		ENDIF		
		IF iAction = ciACTION_COMBAT_MODE
			SET_PED_STATE(iPed,ciTASK_GENERAL_COMBAT #IF IS_DEBUG_BUILD , 1 #ENDIF )
			PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - Returning True (1)")
			RETURN TRUE
		ENDIF
		IF iAction = ciACTION_THROW_PROJECTILES
			SET_PED_STATE(iPed,ciTASK_THROW_PROJECTILES #IF IS_DEBUG_BUILD , 1 #ENDIF )
			PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - Returning True (2)")
			RETURN TRUE
		ENDIF
		
		IF MC_serverBD.iTargetType[iRule][iTeam] != ci_TARGET_NONE
			IF MC_serverBD.iTargetType[iRule][iTeam] = ci_TARGET_LOCATION
				PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - MC_serverBD.iTargetType[iRule][iTeam] = ci_TARGET_LOCATION")
				
				IF MC_serverBD_2.iTargetID[iPed] = -1
				OR bForceNewTarget
					MC_serverBD_2.iTargetID[iPed] = GET_PED_OBJECTIVE_TARGET_LOCATION(iPed, iRule, iTeam)
					IF MC_serverBD_2.iTargetID[iPed] = -1
						MC_serverBD.iTargetType[iRule][iTeam] = ci_TARGET_NONE
						PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - MC_serverBD.iTargetType[iRule][iTeam] = ci_TARGET_NONE")
					ELSE
						SET_PED_STATE(iPed,ciTASK_GOTO_COORDS)
						PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - Returning True (3)")
						RETURN TRUE
					ENDIF
				ELSE
					SET_PED_STATE(iPed,ciTASK_GOTO_COORDS)
					PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - Returning True (4)")
					RETURN TRUE
				ENDIF
			ELIF MC_serverBD.iTargetType[iRule][iTeam] = ci_TARGET_PLAYER
				PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - MC_serverBD.iTargetType[iRule][iTeam] = ci_TARGET_PLAYER")
				
				IF MC_serverBD.TargetPlayerID[iPed] = INVALID_PLAYER_INDEX()
				OR bForceNewTarget
					MC_serverBD.TargetPlayerID[iPed] = GET_PED_OBJECTIVE_TARGET_PLAYER(iPed, iTeam, iRule)
					IF MC_serverBD.TargetPlayerID[iPed] = INVALID_PLAYER_INDEX()
						PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - Target = INVALID_PLAYER_INDEX")
						MC_serverBD.iTargetType[iRule][iTeam] = ci_TARGET_NONE
					ELSE
						IF MC_serverBD_2.iAssociatedAction[iPed] = ciACTION_GOTO
							SET_PED_STATE(iPed,ciTASK_GOTO_PLAYER)
						ELIF MC_serverBD_2.iAssociatedAction[iPed] =ciACTION_DEFEND
							SET_PED_STATE(iPed,ciTASK_DEFEND_PLAYER)
						ELIF MC_serverBD_2.iAssociatedAction[iPed] =ciACTION_ATTACK
							SET_PED_STATE(iPed,ciTASK_ATTACK_PLAYER)
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - has a valid target, but no MC_serverBD_2.iAssociatedAction[iPed]")
						#ENDIF
						ENDIF
						PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - Returning True (5)")
						RETURN TRUE
					ENDIF
				ELSE
					PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - Other...")
					
					IF MC_serverBD_2.iAssociatedAction[iPed] =ciACTION_GOTO
						SET_PED_STATE(iPed,ciTASK_GOTO_PLAYER)
					ELIF MC_serverBD_2.iAssociatedAction[iPed] =ciACTION_DEFEND
						SET_PED_STATE(iPed,ciTASK_DEFEND_PLAYER)
					ELIF MC_serverBD_2.iAssociatedAction[iPed] =ciACTION_ATTACK
						SET_PED_STATE(iPed,ciTASK_ATTACK_PLAYER)
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - has a valid target, but no MC_serverBD_2.iAssociatedAction[iPed]")
					#ENDIF
					ENDIF
					
					PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - Returning True (6)")
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD.niTargetID[iPed] = NULL
				OR bForceNewTarget
					PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - MC_serverBD.niTargetID[iPed] = NULL or bForceNewTarget")
					
					MC_serverBD.niTargetID[iPed] = GET_PED_OBJECTIVE_TARGET_NET_ID(iPed, iRule, iTeam)
					IF MC_serverBD.niTargetID[iPed] = NULL
						PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - MC_serverBD.niTargetID[iPed] = NULL")
						
						IF ARE_ALL_TARGETS_DEAD(sPedState)
							
							IF MC_serverBD_2.iAssociatedAction[iPed] = ciACTION_ATTACK
								MANAGE_PED_ARRIVAL_RESULTS(iPed, FALSE)
							ENDIF
							
							MC_serverBD.iTargetType[iRule][iTeam] = ci_TARGET_NONE
							PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - all targets dead for team ",iTeam,"'s rule ",iRule,", setting type to none")
						ELSE
							IF bIncludeRespawningTarget
								PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - targets respawning for team ",iTeam,"'s rule ",iRule,", return TRUE")
								PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - Returning True (7)")	
								RETURN TRUE
							ENDIF
						ENDIF
					ELSE
						IF MC_serverBD_2.iAssociatedAction[iPed] =ciACTION_GOTO
							SET_PED_STATE(iPed,ciTASK_GOTO_ENTITY)
							PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - got valid target - goto")
						ELIF MC_serverBD_2.iAssociatedAction[iPed] =ciACTION_DEFEND
							SET_PED_STATE(iPed,ciTASK_DEFEND_ENTITY)
							PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - got valid target - defend")
						ELIF MC_serverBD_2.iAssociatedAction[iPed] =ciACTION_ATTACK
							SET_PED_STATE(iPed,ciTASK_ATTACK_ENTITY)
							PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - got valid target - attack")
						ELIF MC_serverBD_2.iAssociatedAction[iPed] =ciACTION_AIM_AT
							SET_PED_STATE(iPed,ciTASK_AIM_AT_ENTITY)
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - has a valid target, but no MC_serverBD_2.iAssociatedAction[iPed]")
						#ENDIF
						ENDIF
						
						PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - Returning True (8)")	
						RETURN TRUE
					ENDIF
				ELSE
					IF MC_serverBD_2.iAssociatedAction[iPed] =ciACTION_GOTO
						SET_PED_STATE(iPed,ciTASK_GOTO_ENTITY)
						PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - got valid target - goto")
					ELIF MC_serverBD_2.iAssociatedAction[iPed] =ciACTION_DEFEND
						SET_PED_STATE(iPed,ciTASK_DEFEND_ENTITY)
						PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - got valid target - defend")
					ELIF MC_serverBD_2.iAssociatedAction[iPed] =ciACTION_ATTACK
						SET_PED_STATE(iPed,ciTASK_ATTACK_ENTITY)
						PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - got valid target - attack")
					ELIF MC_serverBD_2.iAssociatedAction[iPed] =ciACTION_AIM_AT
						SET_PED_STATE(iPed,ciTASK_AIM_AT_ENTITY)
						PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - got valid target - aim at")
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - has a valid target, but no MC_serverBD_2.iAssociatedAction[iPed]")
					#ENDIF
					ENDIF
					
					PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - Returning True (9)")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	PRINTLN("[Peds][Ped ", iPed, "] - DOES_PED_HAVE_VALID_TARGET - DOES_PED_HAVE_VALID_TARGET - Returning FALSE")
	
	RETURN FALSE

ENDFUNC

/// PURPOSE: This function is the main Ped State choosing function, it will find what state the ped should be doing right now by
///    checking through the ped's potential Actions and checking for any potential Targets
PROC PROCESS_PED_TASK_NOTHING_BRAIN(FMMC_PED_STATE &sPedState)
	
	INT iPed = sPedState.iIndex
			
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSeventeen, ciPed_BSSeventeen_TrainDriverTasking)
		IF SET_PED_HAS_VALID_GOTO(sPedState)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - (Train Driver!) has a valid primary goto. Will just let the gotos come to me.")
			EXIT
		ENDIF
		SET_PED_STATE(iPed, ciTASK_DRIVE_TRAIN #IF IS_DEBUG_BUILD , 1 #ENDIF )
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - should go into Drive Train State")
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedFixationTarget > -1
	AND FMMC_IS_LONG_BIT_SET(MC_ServerBD_1.iPedFixationTaskEnded, iPed)
	AND NOT FMMC_IS_LONG_BIT_SET(iPedFixationStateCleanedUpBS, iPed)
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - Exitting. Waiting for the Body to cleanup fixation settings. Likely stuck in combat...")
		EXIT	
	ENDIF
	
	IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedTaskActive, iPed)
	AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSecondaryTaskActive, iPed)
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - shouldn't start any tasks because none have been activated")
		EXIT
	ENDIF
	
	IF DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim)
	AND IS_PED_ANIMATION_PLAYING(sPedState.pedIndex, iPed)
		//We're animating and we need to do a special break out, don't start any tasks!
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - shouldn't start any tasks because performing an animation that requires a breakout")
		EXIT
	ENDIF
	
	IF MC_serverBD_2.iPartPedFollows[iPed] != -1
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSix, ciPed_BSSix_AllowTaskingWhileGrouped)
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - shouldn't start any tasks because MC_serverBD_2.iPartPedFollows[iPed] = ",MC_serverBD_2.iPartPedFollows[iPed])
		EXIT
	ENDIF
	
	IF IS_PED_AT_ANY_TEAM_HOLDING(iPed)
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - shouldn't start any tasks because IS_PED_AT_ANY_TEAM_HOLDING returns TRUE")
		EXIT
	ENDIF
	
	//Secondary tasks:
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSecondaryTaskActive, iPed)
		
		IF SET_PED_HAS_VALID_SECONDARY_GOTO(sPedState)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - has a valid secondary goto")
			EXIT
		ENDIF
		
		IF DOES_PED_HAVE_VALID_TARGET(sPedState, FALSE, FALSE, TRUE, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEleven, ciPed_BSEleven_WaitForTaskTargetToSpawn))
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - has a valid secondary target; ciPed_BSEleven_WaitForTaskTargetToSpawn = ",IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEleven, ciPed_BSEleven_WaitForTaskTargetToSpawn))
			EXIT
		ENDIF
		
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedFixationTarget > -1
		IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_1.iPedFixationTaskStarted, iPed)	
		AND NOT FMMC_IS_LONG_BIT_SET(iPedFixationStateCleanedUpBS, iPed)	
		AND IS_RULE_VALID_FOR_PED_FIXATION_STATE(iPed)
		AND NOT SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(sPedState, DEFAULT #IF IS_DEBUG_BUILD , 9 #ENDIF)
			SET_PED_STATE(iPed, ciTASK_ATTACK_ENTITY)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - should go into ciTASK_ATTACK_ENTITY iPedFixationTarget: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedFixationTarget)
			EXIT
		ENDIF
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedTaskActive, iPed)
		
		IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedTaskStopped, iPed)
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEighteen, ciPed_BSEighteen_PrioritizeActionOverPrimaryGoto)
				IF SET_PED_HAS_VALID_GOTO(sPedState)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - has a valid primary goto")
					EXIT
				ENDIF
			ENDIF
			
			IF SHOULD_PED_GO_TO_COVER_STATE(sPedState)
			
				IF sPedState.bIsOnPlacedTrain
				AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSeventeen, ciPed_BSSeventeen_TrainCombatTasking)
					SET_PED_STATE(iPed, ciTASK_COMBAT_TRAIN #IF IS_DEBUG_BUILD , 2 #ENDIF )
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - should go into combat state - ON TRAIN (1)")
					EXIT
				ENDIF 
			
				SET_PED_STATE(iPed, ciTASK_TAKE_COVER)
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - should go into cover state")
				EXIT
			ENDIF
			
			IF DOES_PED_HAVE_VALID_TARGET(sPedState, FALSE, TRUE, FALSE, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEleven, ciPed_BSEleven_WaitForTaskTargetToSpawn))
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - has a valid primary target; ciPed_BSEleven_WaitForTaskTargetToSpawn = ",IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEleven, ciPed_BSEleven_WaitForTaskTargetToSpawn))
				EXIT
			ENDIF
						
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEighteen, ciPed_BSEighteen_PrioritizeActionOverPrimaryGoto)
				IF SET_PED_HAS_VALID_GOTO(sPedState)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - has a valid primary goto")
					EXIT
				ENDIF
			ENDIF
			
		ENDIF
		
		IF SHOULD_PED_GO_FROM_NOTHING_STATE_TO_COMBAT_STATE(sPedState)
			
			IF sPedState.bIsOnPlacedTrain
			AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSeventeen, ciPed_BSSeventeen_TrainCombatTasking)
				SET_PED_STATE(iPed, ciTASK_COMBAT_TRAIN #IF IS_DEBUG_BUILD , 3 #ENDIF )
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - should go into combat state - ON TRAIN (2)")
				EXIT
			ENDIF 
						
			SET_PED_STATE(iPed, ciTASK_GENERAL_COMBAT #IF IS_DEBUG_BUILD , 3 #ENDIF )
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - should go into combat state")
			EXIT
		ENDIF
		
		IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedTaskStopped, iPed)
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fRange = cfMAX_PATROL_RANGE
				IF SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(sPedState, DEFAULT #IF IS_DEBUG_BUILD , 8 #ENDIF)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - should not go to wander state as they should prioritise their anim")
					EXIT
				ENDIF
				SET_PED_STATE(iPed,ciTASK_WANDER)
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - should go into wander state")
				EXIT
			ENDIF
			
		ENDIF
		
		IF NOT sPedState.bIsInAnyVehicle
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim = ciPED_IDLE_ANIM__NONE
			AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim = ciPED_IDLE_ANIM__NONE
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitset,ciPed_BS_CanMoveBeforeCombat)
					IF SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(sPedState, DEFAULT #IF IS_DEBUG_BUILD , 7 #ENDIF)
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - should not go to defend state as they should prioritise their anim")
						EXIT
					ENDIF
					SET_PED_STATE(iPed,ciTASK_DEFEND_AREA)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - should go into defend state")
					EXIT
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEleven, ciPed_BSEleven_CanHaveDefensiveAreaInVehicle)
				SET_PED_STATE(iPed,ciTASK_DEFEND_AREA_IN_VEHICLE)
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_TASK_NOTHING_BRAIN - should go into defend (vehicle version) state")
				EXIT
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROCESS_SIGHT_FOR_LEAVE_SIGHT_AREA(FMMC_PED_STATE &sPedState, BOOL &bSightCheck, BOOL bInArea, BOOL bHasVisual)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetSixteen, ciPED_BSSixteen_ActivateTaskOnLeaveAreaWithSight)
		IF bInArea
		AND bHasVisual
			IF NOT FMMC_IS_LONG_BIT_SET(iPedPlayerExittedAngledAreaTriggered, sPedState.iIndex)
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - MANAGE_PED_TASK_ACTIVATION - ped ",sPedState.iIndex,"; nearest player is in activation area and LOS but we are set up to wait until they leave. bSightCheck = FALSE")
				FMMC_SET_LONG_BIT(iPedPlayerExittedAngledArea, sPedState.iIndex)
				bSightCheck = FALSE
			ELSE
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - MANAGE_PED_TASK_ACTIVATION - ped ",sPedState.iIndex,"; Player has left the Area while we had LOS before, so triggering bSightCheck = TRUE")
				bSightCheck = TRUE
			ENDIF
		ELSE 
			IF NOT bHasVisual
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - MANAGE_PED_TASK_ACTIVATION - ped ",sPedState.iIndex,"; Player is not in the area and we do not have visual. Clearing the seen in area flag.")
				FMMC_CLEAR_LONG_BIT(iPedPlayerExittedAngledAreaTriggered, sPedState.iIndex)
			ENDIF
			
			IF FMMC_IS_LONG_BIT_SET(iPedPlayerExittedAngledArea, sPedState.iIndex) // Seen us in the area during this period of time with LOS, and watched us just leave it.
			AND bHasVisual
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - MANAGE_PED_TASK_ACTIVATION - ped ",sPedState.iIndex,"; Player has left the Area and we have line of sight.")
				FMMC_SET_LONG_BIT(iPedPlayerExittedAngledAreaTriggered, sPedState.iIndex)
				bSightCheck = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TRIGGER_PED_TASK_ACTIVATION(FMMC_PED_STATE &sPedState)
		
	PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - TRIGGER_PED_TASK_ACTIVATION - Task Activation Triggered  With associated action: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iAssociatedAction)
	
	FMMC_SET_LONG_BIT(MC_serverBD.iPedTaskActive, sPedState.iIndex)
	MC_serverBD_2.iAssociatedAction[sPedState.iIndex] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iAssociatedAction
	
	IF SHOULD_PED_STOP_TASKS(sPedState.iIndex)
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - TRIGGER_PED_TASK_ACTIVATION - Tasks should stop immediately!!")
		FMMC_SET_LONG_BIT(MC_serverBD.iPedTaskStopped, sPedState.iIndex)
		FMMC_SET_LONG_BIT(MC_serverBD.iPedAtGotoLocBitset, sPedState.iIndex)
	ELSE
		IF MC_serverBD_2.iPedState[sPedState.iIndex] != ciTASK_CHOOSE_NEW_TASK
			IF NOT IS_BIT_SET(sPedState.iBitset, FMMC_PED_STATE_IN_COMBAT)
			AND CAN_PED_TASKS_BE_CLEARED(sPedState.pedIndex)
				CLEAR_PED_TASKS(sPedState.pedIndex)
			ENDIF
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - TRIGGER_PED_TASK_ACTIVATION - clearing ped task, Task Activation Triggered") 
			SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK)
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_PED_TASK_ACTIVATION(FMMC_PED_STATE &sPedState)

	IF SHOULD_PED_SUPPRESS_NON_SCRIPT_TRIGGERED_AGGRO_EVENTS(sPedState)
	AND IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_BlockTaskActivationWhileStealthSystemActive)
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - MANAGE_PED_TASK_ACTIVATION - PED: ",sPedState.iIndex, " EXITTING - We are currently suppressing Aggro Events until a Script Event is sent out.")
		EXIT
	ENDIF
	
	IF SHOULD_STAY_IN_FIXATION_STATE(sPedState.iIndex)
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - MANAGE_PED_TASK_ACTIVATION - PED: ",sPedState.iIndex, " EXITTING - We are currently FIXATED.")
		EXIT
	ENDIF
	
	PED_INDEX ThisPed = sPedState.pedIndex
	BOOL bActivateTask,bSightCheck
	INT itempPlayer
	PLAYER_INDEX tempPlayer
	PED_INDEX PlayerPed
	
	PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - MANAGE_PED_TASK_ACTIVATION - PED: ", sPedState.iIndex, " Checking if we should activate tasks.")
	
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].vActivationAAPos1)
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].vActivationAAPos2)
	AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].fActivationAAWidth != 0)
		
		itempPlayer = GET_NEAREST_PLAYER_TO_ENTITY(ThisPed)
		tempPlayer = INT_TO_PLAYERINDEX(itempPlayer)
		IF tempPlayer != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(tempPlayer)
				PlayerPed = GET_PLAYER_PED(tempPlayer)
				
				BOOL bInArea = IS_ENTITY_IN_ANGLED_AREA(PlayerPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].vActivationAAPos1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].vActivationAAPos2, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].fActivationAAWidth)
				BOOL bHasVisualOfPlayer
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitset, ciPed_BS_ActivationSight)
					IF HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(sPedState.iIndex, ThisPed, PlayerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
						IF IS_TARGET_PED_IN_PERCEPTION_AREA(ThisPed,PlayerPed) AND NOT IS_STEALTH_AND_AGGRO_SYSTEMS_CURRENTLY_ACTIVE(sPedState)
						OR FMMC_IS_LONG_BIT_SET(iPedSASWithinSight, sPedState.iIndex) 						
							bHasVisualOfPlayer = TRUE
						ENDIF
					ENDIF
				ELSE						
					bHasVisualOfPlayer = TRUE
				ENDIF	
				
				IF bInArea
				OR (SHOULD_PED_START_COMBAT(sPedState) AND NOT SHOULD_PED_SUPPRESS_NON_SCRIPT_TRIGGERED_AGGRO_EVENTS(sPedState))
					
					#IF IS_DEBUG_BUILD
					IF NOT bInArea
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - MANAGE_PED_TASK_ACTIVATION - ped ",sPedState.iIndex,"; nearest player isn't in activation area , but ped should start combat!")
					ENDIF
					#ENDIF
					
					IF bHasVisualOfPlayer
						bSightCheck = TRUE
					ENDIF
								
				ENDIF	
					
				// Left Area
				PROCESS_SIGHT_FOR_LEAVE_SIGHT_AREA(sPedState, bSightCheck, bInArea, bHasVisualOfPlayer)
			ENDIF
		ENDIF
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iActivationRange != 0
		
		itempPlayer = GET_NEAREST_PLAYER_TO_ENTITY(ThisPed)
		tempPlayer = INT_TO_PLAYERINDEX(itempPlayer)
		IF tempPlayer != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(tempPlayer)
				PlayerPed = GET_PLAYER_PED(tempPlayer)
				
				FLOAT fActivateSquared = TO_SQUARED_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iActivationRange)
				FLOAT fDist2 
				
				IF IS_LOCAL_PLAYER_USING_DRONE()
					OBJECT_INDEX oiDrone = GET_LOCAL_DRONE_PROP_OBJ_INDEX()
					fDist2 = VDIST2(GET_FMMC_PED_COORDS(sPedState), GET_ENTITY_COORDS(oiDrone))
					
					IF fDist2 <= fActivateSquared
					OR (SHOULD_PED_START_COMBAT(sPedState) AND NOT SHOULD_PED_SUPPRESS_NON_SCRIPT_TRIGGERED_AGGRO_EVENTS(sPedState))
						
						#IF IS_DEBUG_BUILD
						IF NOT (fDist2 <= fActivateSquared)
							PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - MANAGE_PED_TASK_ACTIVATION - ped ",sPedState.iIndex,"; nearest player isn't close enough (DRONE), but ped should start combat!")
						ENDIF
						#ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitset, ciPed_BS_ActivationSight)
							IF HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(sPedState.iIndex, ThisPed, oiDrone, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
								bSightCheck = TRUE
							ENDIF
						ELSE
							bSightCheck = TRUE
						ENDIF
					ENDIF
				
				ENDIF
				
				BOOL bInArea
				BOOL bHasVisualOfPlayer
				
				//Awareness checks will not trigger if already triggered from the drone.
				IF NOT bSightCheck
					fDist2 = GET_FMMC_DIST2_FROM_LOCAL_PLAYER_PED(sPedState)
					
					bInArea = (fDist2 <= fActivateSquared)
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitset, ciPed_BS_ActivationSight)
						IF HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(sPedState.iIndex, ThisPed, PlayerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
							IF IS_TARGET_PED_IN_PERCEPTION_AREA(ThisPed,PlayerPed) AND NOT IS_STEALTH_AND_AGGRO_SYSTEMS_CURRENTLY_ACTIVE(sPedState)
							OR FMMC_IS_LONG_BIT_SET(iPedSASWithinSight, sPedState.iIndex) 
								bHasVisualOfPlayer = TRUE
							ENDIF
						ENDIF
					ELSE
						bHasVisualOfPlayer = TRUE
					ENDIF
					
					IF bInArea
					OR (SHOULD_PED_START_COMBAT(sPedState) AND NOT SHOULD_PED_SUPPRESS_NON_SCRIPT_TRIGGERED_AGGRO_EVENTS(sPedState))
						
						#IF IS_DEBUG_BUILD
						IF (fDist2 > POW(TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iActivationRange), 2.0))
							PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - MANAGE_PED_TASK_ACTIVATION - ped ",sPedState.iIndex,"; nearest player isn't close enough (should be within ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iActivationRange,"m), but ped should start combat!")
						ENDIF
						#ENDIF
							
						IF bHasVisualOfPlayer
							bSightCheck = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				// Left Area
				IF NOT IS_LOCAL_PLAYER_USING_DRONE()
					PROCESS_SIGHT_FOR_LEAVE_SIGHT_AREA(sPedState, bSightCheck, bInArea, bHasVisualOfPlayer)
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - MANAGE_PED_TASK_ACTIVATION - PED: ", sPedState.iIndex, " default case bSightCheck = TRUE.")
		bSightCheck = TRUE
	ENDIF
	
	IF IS_USING_FRIENDLY_PEDS_NEARBY_FOR_ACTIVATION(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iTaskTriggerFriendliesCount, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].fTaskTriggerFriendliesRange)
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - MANAGE_PED_TASK_ACTIVATION - Ped is using logic to check how many nearby friendly peds we have.")
		IF NOT ARE_ENOUGH_FRIENDLY_PEDS_NEARBY_FOR_ACTIVATION(sPedState, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iTaskTriggerFriendliesCount, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].fTaskTriggerFriendliesRange)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - MANAGE_PED_TASK_ACTIVATION - Not enough peds in friendly range. bActivateTask = FALSE")
			bSightCheck = FALSE
			bActivateTask = FALSE
		ENDIF
	ENDIF
	
	IF bSightCheck
		IF HAS_ANY_TEAM_HIT_PED_TASK_ACTIVATION(sPedState.iIndex)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - MANAGE_PED_TASK_ACTIVATION - PED: ", sPedState.iIndex, " bActivateTask = TRUE")
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedTeam ", 		 		g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iAssociatedTeam)
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedObjective ", 			g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iAssociatedObjective)
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedActionStart ", 		g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iAssociatedActionStart)
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedScoreRequired ",		g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iAssociatedScoreRequired)
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondAssociatedTeam ", 		g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iSecondAssociatedTeam)
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondAssociatedObjective ", 	g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iSecondAssociatedObjective)
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iThirdAssociatedTeam ", 			g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iThirdAssociatedTeam)
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iThirdAssociatedObjective ", 	g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iThirdAssociatedObjective)
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFourthAssociatedTeam ", 		g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iFourthAssociatedTeam)
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFourthAssociatedObjective ", 	g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iFourthAssociatedObjective)
			
			bActivateTask = TRUE
		ELSE
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - MANAGE_PED_TASK_ACTIVATION - PED: ", sPedState.iIndex, " HAS_ANY_TEAM_HIT_PED_TASK_ACTIVATION = FALSE.")
		ENDIF
	ENDIF
	
	IF bActivateTask
	AND IS_PRIMARY_CUSTOM_TARGET_ENTITY_IN_CORRECT_STATE_FOR_TASK(sPedState.iIndex, ThisPed, FALSE)		
		TRIGGER_PED_TASK_ACTIVATION(sPedState)		
	ENDIF

ENDPROC

PROC MANAGE_PED_SECONDARY_TASK_ACTIVATION(INT iPed, PED_INDEX ThisPed)
	
	IF HAS_ANY_TEAM_HIT_PED_SECONDARY_TASK_ACTIVATION(iPed)
	AND IS_PED_WITHIN_RANGE_OF_SECONDARY_CUSTOM_TARGET_ENTITY(ThisPed, iPed, FALSE)
	AND (NOT HAS_PLAYER_GOT_CONTROL_OF_SECONDARY_CUSTOM_TARGET_ENTITY(iPed))
	AND DOES_VEHICLE_PASS_SECONDARY_TASK_ACTIVATION_CONDITIONS(iPed, ThisPed)
		FMMC_SET_LONG_BIT(MC_serverBD.iPedSecondaryTaskActive, iPed)
		PRINTLN("[Peds][Ped ", iPed, "][Task][SERVER] - Secondary Task Activation Triggered for ped. iSecondaryObjective: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryObjective, " iSecondaryTeam: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryTeam)
		
		MC_ServerBD_2.iAssociatedAction[iPed] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryAction
		MC_serverBD.niTargetID[iPed] = NULL
		MC_serverBD_2.iTargetID[iPed] = -1
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].mn = PLAYER_TWO
			FMMC_CLEAR_LONG_BIT(iPedAnimBitset, iPed)
			FMMC_CLEAR_LONG_BIT(iPedPerformingSpecialAnimBS, iPed)
			FMMC_CLEAR_LONG_BIT(g_iFMMCPedAnimBreakoutBS, iPed)
			
			BROADCAST_FMMC_ANIM_STOPPED(iPed, TRUE, TRUE)
		ENDIF
		
		IF MC_serverBD_2.iPedState[iPed] != ciTASK_CHOOSE_NEW_TASK
			
			IF NOT IS_PED_IN_COMBAT(ThisPed)
			AND CAN_PED_TASKS_BE_CLEARED(ThisPed)
				CLEAR_PED_TASKS(ThisPed)
			ENDIF
			
			PRINTLN("[Peds][Ped ", iPed, "][Task][SERVER] - clearing ped task, Secondary Task Activation Triggered: ",iPed)
			SET_PED_STATE(iPed,ciTASK_CHOOSE_NEW_TASK)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: This function runs as a backup if spooked/aggro/cancel tasks event was lost
PROC RUN_SERVER_PED_BACKUP_SPOOK_CANCEL_TASKS_LOGIC(FMMC_PED_STATE &sPedState)

	INT iPed = sPedState.iIndex
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSix, ciPed_BSSix_SpookOnSpawn)
	AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedBitset, iPed)
	AND NOT FMMC_IS_LONG_BIT_SET(iPedHitByStunGun, iPed)
			
		#IF IS_DEBUG_BUILD 
		SET_PED_IS_SPOOKED_DEBUG(iPed)
		#ENDIF
		FMMC_SET_LONG_BIT(iPedLocalSpookedBitset, iPed)
		FMMC_SET_LONG_BIT(MC_serverBD.iPedSpookedBitset, iPed)
		PRINTLN("[RCC MISSION][SpookAggro] RUN_SERVER_PED_BACKUP_SPOOK_CANCEL_TASKS_LOGIC - Ped ",iPed," spooked as ped is set to spook on spawn!")
		
		BROADCAST_FMMC_PED_SPOOKED(iPed)
	ENDIF
	
	//SPOOKED/CANCEL TASKS/AGGRO CHECKS - all this stuff should just be a backup, it should hopefully just run when the event is received
	
	IF FMMC_IS_LONG_BIT_SET(iPedLocalSpookedBitset, iPed)
		IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedBitset, iPed)
			//Spooked:
			PRINTLN("[RCC MISSION][SpookAggro] RUN_SERVER_PED_BACKUP_SPOOK_CANCEL_TASKS_LOGIC - Ped ",iPed," spooked as iPedLocalSpookedBitset is set")
			#IF IS_DEBUG_BUILD 
			SET_PED_IS_SPOOKED_DEBUG(iPed)
			#ENDIF
			FMMC_SET_LONG_BIT(MC_serverBD.iPedSpookedBitset, iPed)
		ENDIF
		
		IF MC_serverBD_2.iPedState[iPed] != ciTASK_FLEE
			IF NOT DOES_PED_HAVE_VALID_GOTO(iPed)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFlee = ciPed_FLEE_ON
					IF MC_serverBD_2.iPartPedFollows[iPed] =-1
						IF SHOULD_PED_BE_GIVEN_TASK(sPedState.pedIndex, iPed, SCRIPT_TASK_PLAY_ANIM, TRUE)
						AND SHOULD_PED_BE_GIVEN_TASK(sPedState.pedIndex, iPed, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE)
						OR NOT DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim)
							SET_PED_STATE(iPed,ciTASK_FLEE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedBitset, iPed)
			//Unspooked:
			FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedSpookedBitset, iPed)
			
			PRINTLN("[RCC MISSION][SpookAggro] RUN_SERVER_PED_BACKUP_SPOOK_CANCEL_TASKS_LOGIC - Ped ",iPed," was spooked on Server, but not locally and we ARE the server so clearing it.")
			
			IF NOT sPedState.bInjured
				PROCESS_PED_TASK_NOTHING_BRAIN(sPedState)
			ENDIF
			
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThree,ciPed_BSThree_CancelTasksToCombatPlayers) 
	AND FMMC_IS_LONG_BIT_SET(iPedLocalCancelTasksBitset, iPed)
	AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedCancelTasksBitset, iPed)
		PROCESS_SERVER_PED_CANCEL_TASKS(iPed)
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(iPedLocalAggroedBitset, iPed)
	AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAggroedBitset, iPed)
		//Aggro:
		TRIGGER_AGGRO_SERVERDATA_ON_PED(sPedState)
	ENDIF
	
ENDPROC

//[FMMC_2020] - This needs a big tidy
PROC PROCESS_SERVER_PED_CAPTURING(FMMC_PED_STATE &sPedState)
	
	IF NOT sPedState.bExists
		EXIT
	ENDIF
	
	INT iPed = sPedState.iIndex
	
	INT iTeam
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPed_BSEight_UseDistanceToCapture)	 	
		PARTICIPANT_INDEX tempPart
		PLAYER_INDEX tempPlayer
		INT ipart
		INT igroupteam
		INT ipriority[FMMC_MAX_TEAMS]
		BOOL bwasobjective[FMMC_MAX_TEAMS]
		
		INT iPartsChecked = 0
		INT iTotalPartsToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
		#IF IS_DEBUG_BUILD
		IF bJS3226522
			PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING - iTotalPartsToCheck: ", iTotalPartsToCheck)
		ENDIF
		#ENDIF
		INT iPartThatHasControl = -1

		IF NOT sPedState.bInjured
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPed_BSEight_ResetCaptureOnOutOfRange)
			AND MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iControlPedTimerOffset[iPed])
				PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING Timer: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlPedTimer[iPed]))
				PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING Offset Timer: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD.iControlPedTimerOffset[iPed]))
				MC_serverBD_1.iControlPedTimer[iPed] += MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD.iControlPedTimerOffset[iPed])
				PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING New Timer: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlPedTimer[iPed]))
				MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iControlPedTimerOffset[iPed])
			ENDIF
			
			INT iPartToCheck = -1
			IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iCapturePedRequestPartBS, iPed)
				//Check who asked to hack
				PRINTLN("[PLAYER_LOOP] - PROCESS_SERVER_PED_CAPTURING 2")
				FOR iPartToCheck = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartToCheck))
					AND NOT (IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_ANY_SPECTATOR)
						OR IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
					AND NOT IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet,PBBOOL_PLAYER_FAIL)
					AND IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
					AND NOT IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_FINISHED)
						iPartsChecked++
						IF FMMC_IS_LONG_BIT_SET(MC_playerBD[iPartToCheck].iRequestCapturePedBS, iPed)
							FMMC_SET_LONG_BIT(MC_serverBD_4.iCapturePedRequestPartBS, iPed)
							igroupteam = MC_playerBD[iPartToCheck].iTeam
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPed_BSEight_SpookOnHack)
							AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedBitset, iPed)
								PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING - Spooking ped ", iPed)
								FMMC_SET_LONG_BIT(iPedLocalSpookedBitset, iPed)
								BROADCAST_FMMC_PED_SPOOKED(iPed, ciSPOOK_PLAYER_CAR_CRASH, iPartToCheck)
							ENDIF
							PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING - Participant: ", iPartToCheck," has requested to capture the ped: ", iPed)
							BREAKLOOP
						ENDIF
						IF iPartsChecked >= iTotalPartsToCheck
							#IF IS_DEBUG_BUILD
							IF bJS3226522
								PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING - Parts Checked ", iPartsChecked)
							ENDIF
							#ENDIF
							BREAKLOOP
						ENDIF
					ELSE
						IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartToCheck))
							#IF IS_DEBUG_BUILD
							IF bJS3226522
								PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING - Participant: ", iPartToCheck," NOT ACTIVE ped: ", iPed)
							ENDIF
							#ENDIF
						ENDIF
						IF (IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_ANY_SPECTATOR)
						OR IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
							#IF IS_DEBUG_BUILD
							IF bJS3226522
								PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING - Participant: ", iPartToCheck," SPECTATING ped: ", iPed)
							ENDIF
							#ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ELSE
				BOOL bPartHasControl = FALSE
				PRINTLN("[PLAYER_LOOP] - PROCESS_SERVER_PED_CAPTURING")
				FOR iPartToCheck = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartToCheck))
					AND NOT (IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_ANY_SPECTATOR)
						OR IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
					AND NOT IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet,PBBOOL_PLAYER_FAIL)
					AND IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
					AND NOT IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_FINISHED)
						iPartsChecked++
						IF FMMC_IS_LONG_BIT_SET(MC_playerBD[iPartToCheck].iRequestCapturePedBS, iPed)
							iPartThatHasControl = iPartToCheck
							bPartHasControl = TRUE
							PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING - Participant: ", iPartToCheck," is still in control - ped: ", iPed)
							BREAKLOOP
						ENDIF
						IF iPartsChecked >= iTotalPartsToCheck
							#IF IS_DEBUG_BUILD
							IF bJS3226522
								PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING - Parts Checked ", iPartsChecked)
							ENDIF
							#ENDIF
							BREAKLOOP
						ENDIF
					ELSE
						IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartToCheck))
							#IF IS_DEBUG_BUILD
							IF bJS3226522
								PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING - Participant: ", iPartToCheck," NOT ACTIVE ped: ", iPed)
							ENDIF
							#ENDIF
						ENDIF
						IF (IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_ANY_SPECTATOR)
						OR IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
							#IF IS_DEBUG_BUILD
							IF bJS3226522
								PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING - Participant: ", iPartToCheck," SPECTATING ped: ", iPed)
							ENDIF
							#ENDIF
						ENDIF
					ENDIF
				ENDFOR
				IF !bPartHasControl
					PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING - Participant: ", iPartToCheck," has gone out of range, clearing bits - ped: ", iPed)
					FMMC_CLEAR_LONG_BIT(MC_serverBD_4.iCapturePedRequestPartBS, iPed)
				ENDIF
			ENDIF

			IF iPartThatHasControl > -1
			
				tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartThatHasControl))
			
				IF NATIVE_TO_INT(tempPlayer) !=-1
					IF IS_NET_PLAYER_OK(tempPlayer)
						
						tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
						
						IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
							
							ipart = NATIVE_TO_INT(tempPart)
							
							IF MC_serverBD_2.iPartPedFollows[iPed] != ipart
								IF NOT IS_BIT_SET(MC_playerBD[ipart].iClientBitSet,PBBOOL_FINISHED)
								AND NOT IS_BIT_SET(MC_playerBD[ipart].iClientBitSet,PBBOOL_START_SPECTATOR) 
								AND FMMC_IS_LONG_BIT_SET(MC_playerBD[ipart].iPedFollowingBitset, iPed)
									PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING - Setting ped ",iPed," with MC_serverBD_2.iPartPedFollows[iPed] = ",ipart,", old iPartPedFollows = ",MC_serverBD_2.iPartPedFollows[iPed])
									MC_serverBD_2.iPartPedFollows[iPed] = ipart
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			EXIT
		ENDIF
		
		IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iPedExplodeKillOnCaptureBS, iPed) 
			IF IS_PED_IN_GROUP(sPedState.pedIndex)
			OR iPartThatHasControl != -1
				IF MC_serverBD_2.iPartPedFollows[iPed] != -1
					
					tempPart = INT_TO_PARTICIPANTINDEX(MC_serverBD_2.iPartPedFollows[iPed])
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					AND NOT IS_BIT_SET(MC_playerBD[MC_serverBD_2.iPartPedFollows[iPed]].iClientBitSet,PBBOOL_FINISHED)
						
						tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
						
						IF IS_NET_PLAYER_OK(tempPlayer)
							
							igroupteam = MC_playerBD[MC_serverBD_2.iPartPedFollows[iPed]].iTeam
							
							IF SHOULD_PED_COUNT_AS_HELD_FOR_TEAM(iPed,igroupteam)
							OR iPartThatHasControl != -1
								iNumHighPriorityPedHeld[igroupteam]++
								
								FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
									IF iTeam != igroupteam
										IF DOES_TEAM_LIKE_TEAM(iTeam,igroupteam)
											IF SHOULD_PED_COUNT_AS_HELD_FOR_TEAM(iPed,iTeam)
											OR iPartThatHasControl != -1
												iNumHighPriorityPedHeld[iTeam]++
											ENDIF
										ENDIF
									ENDIF
								ENDFOR
							ENDIF
							
							IF MC_serverBD_4.iPedRule[iPed][igroupTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
								IF MC_serverBD_2.iOldPartPedFollows[iPed] = MC_serverBD_2.iPartPedFollows[iPed] 
									IF MC_serverBD_4.iPedPriority[iPed][igroupTeam] < FMMC_MAX_RULES
										IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
											MC_START_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
											NET_PRINT("[JS] PROCESS_SERVER_PED_CAPTURING starting control timer for ped: ")  NET_PRINT_INT(iPed) NET_NL()
										ELSE
											IF (MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlPedTimer[iPed]) > MC_serverBD.iPedTakeoverTime[igroupTeam][MC_serverBD_4.iPedPriority[iPed][igroupTeam]])
												BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_CON_PED,0,igroupTeam,-1,tempplayer,ci_TARGET_PED,iPed)												
												
												REQUEST_INCREMENT_SERVER_REMOTE_PLAYER_SCORE(MC_serverBD_2.iPartPedFollows[iPed], GET_FMMC_POINTS_FOR_TEAM(igroupTeam,MC_serverBD_4.iPedPriority[iPed][igroupTeam]), igroupTeam, MC_serverBD_4.iPedPriority[iPed][igroupTeam])
																								
												IF MC_serverBD.iRuleReCapture[igroupTeam][MC_serverBD_4.iPedPriority[iPed][igroupTeam]] <= 0
													IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFollow = ciPed_FOLLOW_OFF
														PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING Control ped ",iPed," getting cleaned up as iRuleReCapture is zero for team ",igroupTeam,", resetting MC_serverBD_2.iPartPedFollows[iPed] to -1")
														FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS, iPed)
														IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPed_BSEight_UseDistanceToCapture)
															CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
														ENDIF
														MC_serverBD_2.iPartPedFollows[iPed] = -1
														MC_serverBD_2.iOldPartPedFollows[iPed]  = -1
													ENDIF
												ENDIF
												
												IF MC_serverBD_2.iCurrentPedRespawnLives[iPed] <= 0	
												AND MC_serverBD.iRuleReCapture[igroupTeam][MC_serverBD_4.iPedPriority[iPed][igroupTeam]] <= 0
													FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
														ipriority[iTeam] = MC_serverBD_4.iPedPriority[iPed][iTeam]
														
														IF iTeam=igroupTeam
															IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iPed],iTeam)
																CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iPed],iTeam)
																FMMC_CLEAR_LONG_BIT(MC_serverBD.iMissionCriticalPed[iTeam], iPed)
																NET_PRINT("[JS] PROCESS_SERVER_PED_CAPTURING OBJECTIVE NOT FAILED FOR TEAM: ")NET_PRINT_INT(igroupTeam) NET_PRINT(" BECAUSE THEY CAPTURED PED: ") NET_PRINT_INT(iPed) NET_NL()
																bwasobjective[iTeam] = TRUE
															ENDIF
														ELSE
															IF DOES_TEAM_LIKE_TEAM(iTeam,igroupTeam)
																IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iPed],iTeam)
																	CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iPed],iTeam)
																	FMMC_CLEAR_LONG_BIT(MC_serverBD.iMissionCriticalPed[iTeam], iPed)
																	bwasobjective[iTeam] = TRUE
																ENDIF
															ENDIF
														ENDIF
													ENDFOR
													
													FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
														IF iTeam!=igroupTeam
															IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iPed],iTeam)
																IF ipriority[iTeam]  < FMMC_MAX_RULES
																OR FMMC_IS_LONG_BIT_SET(MC_serverBD.iMissionCriticalPed[iTeam], iPed)
																	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamFailBitset,ipriority[iTeam])
																	OR FMMC_IS_LONG_BIT_SET(MC_serverBD.iMissionCriticalPed[iTeam], iPed)
																		
																		REQUEST_SET_TEAM_FAILED(iTeam,mFail_PED_CAPTURED)
																	
																		NET_PRINT("[JS] PROCESS_SERVER_PED_CAPTURING MISSION FAILED FOR TEAM:")NET_PRINT_INT(iTeam) NET_PRINT(" BECAUSE PED CAPTURED: ") NET_PRINT_INT(iPed) NET_NL()
																		MC_serverBD.iEntityCausingFail[iTeam] = iPed
																	ENDIF
																ENDIF
																
																bwasobjective[iTeam] = TRUE
																CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iPed],iTeam)
															ENDIF
															
															IF DOES_TEAM_LIKE_TEAM(iTeam,igroupTeam)
																SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iTeam],TRUE)
																PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iPed,iTeam,TRUE)
															ELSE
																SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iTeam],FALSE)
																PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iPed,iTeam,FALSE)
															ENDIF
														ELSE
															SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iTeam],TRUE)
															PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iPed,iTeam,TRUE)
														ENDIF
														MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_CAPTURED
														
													ENDFOR
												ENDIF
												
												IF MC_serverBD_4.iPedPriority[iPed][igroupTeam] < FMMC_MAX_RULES
													IF MC_serverBD.iRuleReCapture[igroupTeam][MC_serverBD_4.iPedPriority[iPed][igroupTeam]] <= 0
														IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFollow = ciPed_FOLLOW_ON
															FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
																PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iPed,iTeam,FALSE)
															ENDFOR
														ENDIF
													ENDIF
												ENDIF
												
												IF MC_serverBD_4.iPedPriority[iPed][igroupTeam] < FMMC_MAX_RULES
													IF MC_serverBD.iRuleReCapture[igroupTeam][MC_serverBD_4.iPedPriority[iPed][igroupTeam]] != UNLIMITED_CAPTURES_KILLS
														MC_serverBD.iRuleReCapture[igroupTeam][MC_serverBD_4.iPedPriority[iPed][igroupTeam]]--
													ENDIF
												ENDIF
												
												FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
													IF bwasobjective[iTeam] 
														IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_PED,iTeam,ipriority[iTeam])
															SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,ipriority[iTeam],MC_serverBD.iReasonForObjEnd[iTeam])
															BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam],iTeam,ipriority[iTeam],DID_TEAM_PASS_OBJECTIVE(iTeam,ipriority[iTeam]))
														ENDIF
													ENDIF
												ENDFOR
												
												IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPed_BSEight_KillExplodeOnCapture)
													FMMC_SET_LONG_BIT(MC_serverBD_4.iPedExplodeKillOnCaptureBS, iPed)
												ELSE
													IF NOT sPedState.bInjured	
														VEHICLE_INDEX vehPedIsIn
														INT iVehPedIsIn = -1
														IF sPedState.bIsInAnyVehicle
															vehPedIsIn = sPedState.vehIndexPedIsIn
															iVehPedIsIn = IS_ENTITY_A_MISSION_CREATOR_ENTITY(vehPedIsIn)
														ENDIF
														IF MC_serverBD.iPedteamFailBitset[iPed] > 0
														OR (iVehPedIsIn != -1 AND MC_serverBD.iVehteamFailBitset[iVehPedIsIn] > 0)
															FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
																FMMC_CLEAR_LONG_BIT(MC_serverBD.iMissionCriticalPed[iTeam], iPed)
																CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iPed],iTeam)
																IF iVehPedIsIn != -1
																	CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iVehPedIsIn)
																	CLEAR_BIT(MC_serverBD.iVehteamFailBitset[iVehPedIsIn], iTeam)
																ENDIF
															ENDFOR
														ENDIF
													ENDIF
												ENDIF
												
												FMMC_SET_LONG_BIT(MC_serverBD_4.iPedClearFailOnGoTo, iPed)
												
												MC_serverBD_4.iCaptureTimestamp = 0

												NET_PRINT("[JS] PROCESS_SERVER_PED_CAPTURING Control complete for ped: ")  NET_PRINT_INT(iPed) NET_NL()
												MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
												MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iControlPedTimerOffset[iPed])
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPed_BSEight_ResetCaptureOnOutOfRange)
										MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
									ELSE
										IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iControlPedTimerOffset[iPed])
										AND MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
											PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING - ped ",iPed," starting offset timer 6")
											PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING Timer: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlPedTimer[iPed]))
											MC_REINIT_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iControlPedTimerOffset[iPed])
										ENDIF
									ENDIF
									MC_serverBD_2.iOldPartPedFollows[iPed] = MC_serverBD_2.iPartPedFollows[iPed]
								ENDIF
							ELSE
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPed_BSEight_ResetCaptureOnOutOfRange)
									MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
								ELSE
									IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iControlPedTimerOffset[iPed])
									AND MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
										PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING - ped ",iPed," starting offset timer 5")
										PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING Timer: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlPedTimer[iPed]))
										MC_REINIT_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iControlPedTimerOffset[iPed])
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF MC_serverBD_2.iPartPedFollows[iPed] != -1
								PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING - ped ",iPed," gets MC_serverBD_2.iPartPedFollows[iPed] reset to -1 as leader player is not okay")
							ENDIF
							
							MC_serverBD_2.iPartPedFollows[iPed] = -1
							MC_serverBD_2.iOldPartPedFollows[iPed]  = -1
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPed_BSEight_ResetCaptureOnOutOfRange)
								MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
							ELSE
								IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iControlPedTimerOffset[iPed])
								AND MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
									PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING - ped ",iPed," starting offset timer 4")
									PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING Timer: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlPedTimer[iPed]))
									MC_REINIT_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iControlPedTimerOffset[iPed])
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF MC_serverBD_2.iPartPedFollows[iPed] != -1
							PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING - ped ",iPed," gets MC_serverBD_2.iPartPedFollows[iPed] reset to -1 as leader isn't active/is finished")
						ENDIF
						
						MC_serverBD_2.iPartPedFollows[iPed] = -1
						MC_serverBD_2.iOldPartPedFollows[iPed]  = -1
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPed_BSEight_ResetCaptureOnOutOfRange)
							MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
						ELSE
							IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iControlPedTimerOffset[iPed])
							AND MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
								PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING - ped ",iPed," starting offset timer 3")
								PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING Timer: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlPedTimer[iPed]))
								MC_REINIT_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iControlPedTimerOffset[iPed])
							ENDIF
						ENDIF
					ENDIF
				ELSE
					MC_serverBD_2.iOldPartPedFollows[iPed]  = -1
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPed_BSEight_ResetCaptureOnOutOfRange)
						MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
					ELSE
						IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iControlPedTimerOffset[iPed])
						AND MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
							PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING - ped ",iPed," starting offset timer 2")
							PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING Timer: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlPedTimer[iPed]))
							MC_REINIT_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iControlPedTimerOffset[iPed])
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF MC_serverBD_2.iPartPedFollows[iPed] != -1
					PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING - ped ",iPed," gets MC_serverBD_2.iPartPedFollows[iPed] reset to -1 as is no longer in a group")
				ENDIF
				
				MC_serverBD_2.iPartPedFollows[iPed] = -1
				MC_serverBD_2.iOldPartPedFollows[iPed]  = -1
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPed_BSEight_ResetCaptureOnOutOfRange)
					MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
				ELSE
					IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iControlPedTimerOffset[iPed])
					AND MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
						PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING - ped ",iPed," starting offset timer 1")
						PRINTLN("[JS] PROCESS_SERVER_PED_CAPTURING Timer: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlPedTimer[iPed]))
						MC_REINIT_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iControlPedTimerOffset[iPed])
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF sPedState.bExists
		AND NOT sPedState.bInjured
		AND IS_PED_IN_GROUP(sPedState.pedIndex)
			IF MC_serverBD_2.iPartPedFollows[iPed] != -1
				PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(MC_serverBD_2.iPartPedFollows[iPed])
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				AND NOT IS_BIT_SET(MC_playerBD[MC_serverBD_2.iPartPedFollows[iPed]].iClientBitSet,PBBOOL_FINISHED)
					PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
					IF IS_NET_PLAYER_OK(tempPlayer)
						
						INT igroupteam = MC_playerBD[MC_serverBD_2.iPartPedFollows[iPed]].iTeam
						
						IF SHOULD_PED_COUNT_AS_HELD_FOR_TEAM(iPed, igroupteam)
							iNumHighPriorityPedHeld[igroupteam]++
							FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
								IF iTeam != igroupteam
									IF DOES_TEAM_LIKE_TEAM(iTeam,igroupteam)
										IF SHOULD_PED_COUNT_AS_HELD_FOR_TEAM(iPed,iTeam)
											iNumHighPriorityPedHeld[iTeam]++
										ENDIF
									ENDIF
								ENDIF
							ENDFOR
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PROCESS_PART_FOR_PED_GROUP_FOLLOWING(FMMC_PED_STATE &sPedState, PARTICIPANT_INDEX tempPart, PLAYER_INDEX tempPlayer)
	IF MC_serverBD_2.iPartPedFollows[sPedState.iIndex] = -1		
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_IN_GROUP(sPedState.pedIndex)
		#IF IS_DEBUG_BUILD
			IF MC_serverBD_2.iPartPedFollows[sPedState.iIndex] != -1
				PRINTLN("[RCC MISSION] SHOULD_PROCESS_PART_FOR_PED_GROUP_FOLLOWING - ped ", sPedState.iIndex," gets MC_serverBD_2.iPartPedFollows[iPed] reset to -1 as is no longer in a group")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
	OR IS_BIT_SET(MC_playerBD[MC_serverBD_2.iPartPedFollows[sPedState.iIndex]].iClientBitSet, PBBOOL_FINISHED)
		#IF IS_DEBUG_BUILD
			IF MC_serverBD_2.iPartPedFollows[sPedState.iIndex] != -1
				PRINTLN("[RCC MISSION] SHOULD_PROCESS_PART_FOR_PED_GROUP_FOLLOWING - ped ", sPedState.iIndex," gets MC_serverBD_2.iPartPedFollows[iPed] reset to -1 as leader isn't active/is finished")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(tempPlayer)		
		#IF IS_DEBUG_BUILD
			IF MC_serverBD_2.iPartPedFollows[sPedState.iIndex] != -1
				PRINTLN("[RCC MISSION] SHOULD_PROCESS_PART_FOR_PED_GROUP_FOLLOWING - ped ", sPedState.iIndex," gets MC_serverBD_2.iPartPedFollows[iPed] reset to -1 as leader player is not okay")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MANAGE_SERVER_GROUPING_DISTANCE_AND_CONTROL_DATA(FMMC_PED_STATE &sPedState)

	IF NOT sPedState.bExists
	OR sPedState.bInjured	
		EXIT
	ENDIF
	
	INT iPed = sPedState.iIndex
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPed_BSEight_UseDistanceToCapture)	
		EXIT
	ENDIF

	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX tempPlayer
	INT iTeam
	INT ipart
	INT igroupteam
	INT ipriority[FMMC_MAX_TEAMS]
	BOOL bwasobjective[FMMC_MAX_TEAMS]

	tempPlayer = GET_PLAYER_PED_IS_FOLLOWING(sPedState.pedIndex)

	IF tempPlayer != INVALID_PLAYER_INDEX()
		IF IS_NET_PLAYER_OK(tempPlayer)
			
			tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				
				ipart = NATIVE_TO_INT(tempPart)
				
				IF MC_serverBD_2.iPartPedFollows[iPed] != ipart
					IF NOT IS_BIT_SET(MC_playerBD[ipart].iClientBitSet, PBBOOL_FINISHED)
					AND NOT IS_BIT_SET(MC_playerBD[ipart].iClientBitSet, PBBOOL_START_SPECTATOR)
					AND FMMC_IS_LONG_BIT_SET(MC_playerBD[ipart].iPedFollowingBitset, iPed)
						PRINTLN("[RCC MISSION] MANAGE_SERVER_GROUPING_DISTANCE_AND_CONTROL_DATA - Setting ped ",iPed," with MC_serverBD_2.iPartPedFollows[iPed] = ",ipart,", old iPartPedFollows = ",MC_serverBD_2.iPartPedFollows[iPed])
						MC_serverBD_2.iPartPedFollows[iPed] = ipart
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF SHOULD_PROCESS_PART_FOR_PED_GROUP_FOLLOWING(sPedState, tempPart, tempPlayer)
	
		igroupteam = MC_playerBD[MC_serverBD_2.iPartPedFollows[iPed]].iTeam		
		
		IF MC_serverBD_4.iPedRule[iPed][igroupTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
			IF MC_serverBD_2.iOldPartPedFollows[iPed] = MC_serverBD_2.iPartPedFollows[iPed] 
				IF MC_serverBD_4.iPedPriority[iPed][igroupTeam] < FMMC_MAX_RULES
					IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
						MC_START_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
						NET_PRINT("starting control timer for ped: ")  NET_PRINT_INT(iPed) NET_NL()
					ELSE
						IF MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlPedTimer[iPed]) > MC_serverBD.iPedTakeoverTime[igroupTeam][MC_serverBD_4.iPedPriority[iPed][igroupTeam]] 
							BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_CON_PED,0,igroupTeam,-1,tempplayer,ci_TARGET_PED,iPed)
							
							REQUEST_INCREMENT_SERVER_REMOTE_PLAYER_SCORE(MC_serverBD_2.iPartPedFollows[iPed], GET_FMMC_POINTS_FOR_TEAM(igroupTeam,MC_serverBD_4.iPedPriority[iPed][igroupTeam]), igroupTeam, MC_serverBD_4.iPedPriority[iPed][igroupTeam])										

							IF MC_serverBD.iRuleReCapture[igroupTeam][MC_serverBD_4.iPedPriority[iPed][igroupTeam]] <= 0
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFollow = ciPed_FOLLOW_OFF
									PRINTLN("[RCC MISSION] Control ped ",iPed," getting cleaned up as iRuleReCapture is zero for team ",igroupTeam,", resetting MC_serverBD_2.iPartPedFollows[iPed] to -1")
									FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS, iPed)
									IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPed_BSEight_UseDistanceToCapture)
										CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
									ENDIF
									MC_serverBD_2.iPartPedFollows[iPed] = -1
									MC_serverBD_2.iOldPartPedFollows[iPed]  = -1
								ENDIF
							ENDIF
							
							IF MC_serverBD_2.iCurrentPedRespawnLives[iPed] <= 0	
							AND MC_serverBD.iRuleReCapture[igroupTeam][MC_serverBD_4.iPedPriority[iPed][igroupTeam]] <= 0
								FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
									ipriority[iTeam] = MC_serverBD_4.iPedPriority[iPed][iTeam]
									
									IF iTeam=igroupTeam
										IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iPed],iTeam)
											CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iPed],iTeam)
											FMMC_CLEAR_LONG_BIT(MC_serverBD.iMissionCriticalPed[iTeam], iPed)
											NET_PRINT("OBJECTIVE NOT FAILED FOR TEAM: ")NET_PRINT_INT(igroupTeam) NET_PRINT(" BECAUSE THEY CAPTURED PED: ") NET_PRINT_INT(iPed) NET_NL()
											bwasobjective[iTeam] = TRUE
										ENDIF
									ELSE
										IF DOES_TEAM_LIKE_TEAM(iTeam,igroupTeam)
											IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iPed],iTeam)
												CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iPed],iTeam)
												FMMC_CLEAR_LONG_BIT(MC_serverBD.iMissionCriticalPed[iTeam], iPed)
												bwasobjective[iTeam] = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDFOR
								
								FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
									IF iTeam!=igroupTeam
										IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iPed],iTeam)
											IF ipriority[iTeam]  < FMMC_MAX_RULES
											OR FMMC_IS_LONG_BIT_SET(MC_serverBD.iMissionCriticalPed[iTeam], iPed)
												IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamFailBitset,ipriority[iTeam])
												OR FMMC_IS_LONG_BIT_SET(MC_serverBD.iMissionCriticalPed[iTeam], iPed)		
											
													REQUEST_SET_TEAM_FAILED(iTeam,mFail_PED_CAPTURED)																
													NET_PRINT(" MISSION FAILED FOR TEAM:")NET_PRINT_INT(iTeam) NET_PRINT(" BECAUSE PED CAPTURED: ") NET_PRINT_INT(iPed) NET_NL()
													MC_serverBD.iEntityCausingFail[iTeam] = iPed
												ENDIF
											ENDIF
											
											bwasobjective[iTeam] = TRUE
											CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iPed],iTeam)
										ENDIF
										
										IF DOES_TEAM_LIKE_TEAM(iTeam,igroupTeam)
											SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iTeam],TRUE)
											PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iPed,iTeam,TRUE)
										ELSE
											SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iTeam],FALSE)
											PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iPed,iTeam,FALSE)
										ENDIF
									ELSE
										SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iTeam],TRUE)
										PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iPed,iTeam,TRUE)
									ENDIF
									MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_CAPTURED
									
								ENDFOR
							ENDIF
							
							IF MC_serverBD_4.iPedPriority[iPed][igroupTeam] < FMMC_MAX_RULES
								IF MC_serverBD.iRuleReCapture[igroupTeam][MC_serverBD_4.iPedPriority[iPed][igroupTeam]] <= 0
									IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFollow = ciPed_FOLLOW_ON
										FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
											PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iPed,iTeam,FALSE)
										ENDFOR
									ENDIF
								ENDIF
							ENDIF
							
							IF MC_serverBD_4.iPedPriority[iPed][igroupTeam] < FMMC_MAX_RULES
								IF MC_serverBD.iRuleReCapture[igroupTeam][MC_serverBD_4.iPedPriority[iPed][igroupTeam]] != UNLIMITED_CAPTURES_KILLS
									MC_serverBD.iRuleReCapture[igroupTeam][MC_serverBD_4.iPedPriority[iPed][igroupTeam]]--
								ENDIF
							ENDIF
							
							FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
								IF bwasobjective[iTeam] 
									IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_PED,iTeam,ipriority[iTeam])
										SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,ipriority[iTeam],MC_serverBD.iReasonForObjEnd[iTeam])
										BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam],iTeam,ipriority[iTeam],DID_TEAM_PASS_OBJECTIVE(iTeam,ipriority[iTeam]))
									ENDIF
								ENDIF
							ENDFOR
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPed_BSEight_KillExplodeOnCapture)
								FMMC_SET_LONG_BIT(MC_serverBD_4.iPedExplodeKillOnCaptureBS, iPed)
							ENDIF
					
							MC_serverBD_4.iCaptureTimestamp = 0

							NET_PRINT("Control complete for ped: ")  NET_PRINT_INT(iPed) NET_NL()
							MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
						ENDIF
					ENDIF
				ENDIF
			ELSE
				MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
				MC_serverBD_2.iOldPartPedFollows[iPed] = MC_serverBD_2.iPartPedFollows[iPed]
			ENDIF
		ELSE
			MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
		ENDIF
	ELSE
		MC_serverBD_2.iPartPedFollows[iPed] = -1
		MC_serverBD_2.iOldPartPedFollows[iPed]  = -1
		MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Spawning and Respawning mission ped  ----------------------------------------------------------------------------------------------------------------------------
// ##### Description: Handles the processing and helper functions called to spawn/respawn a mission creator ped.  		  --------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE: This resets SOME of the ped with index iPed's state back to their defaults
PROC RESET_PED_SERVER_STATE_DATA( INT iPed )

	IF MC_serverBD.isearchingforped != -1
		PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] Clearing iSearchingForPed: ", MC_serverBD.isearchingforPed)
		MC_serverBD.isearchingforPed = -1
	ENDIF
	
	MC_serverBD.niTargetID[ iPed ] 	= NULL				// learing their target ID
	MC_serverBD_2.iPedState[ iPed ] = ciTASK_CHOOSE_NEW_TASK	// resetting their state
	MC_serverBD_2.iPedGotoProgress[ iPed ] = 0			// resetting the goto progress
	vEntityRespawnLoc = <<0, 0, 0>>
	
	FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedDelivered, iPed) // They haven't been delivered
	CLEAR_BIT( MC_serverBD.iPedNearDropOff[ 0 ][ GET_LONG_BITSET_INDEX( iPed ) ], GET_LONG_BITSET_BIT( iPed ) )
	CLEAR_BIT( MC_serverBD.iPedNearDropOff[ 1 ][ GET_LONG_BITSET_INDEX( iPed ) ], GET_LONG_BITSET_BIT( iPed ) )
	CLEAR_BIT( MC_serverBD.iPedNearDropOff[ 2 ][ GET_LONG_BITSET_INDEX( iPed ) ], GET_LONG_BITSET_BIT( iPed ) )
	CLEAR_BIT( MC_serverBD.iPedNearDropOff[ 3 ][ GET_LONG_BITSET_INDEX( iPed ) ], GET_LONG_BITSET_BIT( iPed ) )
	FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedShouldFleeDropOff, iPed)
	FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedAtGotoLocBitset, iPed) // They need to redo their goto
	FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedAtSecondaryGotoLocBitset, iPed)
	
	#IF IS_DEBUG_BUILD
	IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iObjSpawnPedBitset, iPed)
		PRINTLN("[RCC MISSION] RESET_PED_SERVER_STATE_DATA - Setting iObjSpawnPedBitset for ped ",iPed)
	ENDIF
	#ENDIF
	
	FMMC_SET_LONG_BIT(MC_serverBD.iObjSpawnPedBitset, iPed)
	
	FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedGunOnRuleGiven, iPed) // They need to be given a gun again
	
ENDPROC

FUNC FLOAT GET_RESPAWN_RANGE_CHECK(INT iPed)
	
	SWITCH iPedSpawnFailCount[iPed]
	
		CASE 0
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fPedRespawnRangePlayerSight
		BREAK
		
		CASE 1
			RETURN 60.0
		BREAK
		
		CASE 2
			RETURN 50.0
		BREAK
		
		CASE 3
			RETURN 40.0
		BREAK
		
		CASE 4
			RETURN 30.0
		BREAK
		
		CASE 5
			RETURN 20.0
		BREAK
		
		CASE 6
			RETURN 10.0
		BREAK
		
		CASE 7
			RETURN 5.0
		BREAK
		
	ENDSWITCH
	
	RETURN 1.0

ENDFUNC

FUNC FLOAT GET_IS_PED_VISIBLE_NEAR_PLAYER_RADIUS_FOR_RESPAWN(INT iPed)
	RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fPedRespawnRangePlayerNear
ENDFUNC	   

FUNC BOOL IS_PED_VISIBLE_FOR_RESPAWN(INT iPed)

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitset,ciPed_BS_IgnoreVisCheck)
		PRINTLN("[RCC MISSION] ignore vis check on ped:",iPed)
		RETURN FALSE
	ELSE
		INT iTemp
		IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(MC_GET_PED_SPAWN_LOCATION(iPed, -1, iTemp), 6, 1, 1, 5, TRUE, TRUE, TRUE, GET_RESPAWN_RANGE_CHECK(iPed), FALSE, -1, TRUE, GET_IS_PED_VISIBLE_NEAR_PLAYER_RADIUS_FOR_RESPAWN(iPed))
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL WILL_PED_EVER_BE_HOSTILE(INT iPed)
	
	IF FMMC_IS_LONG_BIT_SET(iPedZoneAggroed, iPed)
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFlee = ciPed_FLEE_ON
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCombatStyle = ciPed_AGRESSIVE
	OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCombatStyle = ciPed_BERSERK
	OR ( g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCombatStyle = ciPed_DEFENSIVE AND IS_PED_ARMED_BY_FMMC(iPed, TRUE) )
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCombatStyleChange_Team != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCombatStyleChange_Rule != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCombatStyleChange_NewStyle != -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCombatStyleChange_NewStyle = ciPed_AGRESSIVE
		OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCombatStyleChange_NewStyle = ciPed_BERSERK
		OR ( g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCombatStyleChange_NewStyle = ciPed_DEFENSIVE AND IS_PED_ARMED_BY_FMMC(iPed, TRUE) )
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC SET_PED_TO_SPAWN_NEXT(INT iPed)
	
	PRINTLN("[RCC MISSION] SET_PED_TO_SPAWN_NEXT - Called with ped ",iPed)
	
	IF MC_serverBD.isearchingforped != iPed		
		MC_serverBD.isearchingforPed = iPed	
		PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] Assigning iSearchingForPed: ", MC_serverBD.isearchingforPed)
	ENDIF
	
	iPedSpawnFailDelay[iPed] = -1
	iPedSpawnFailCount[iPed] = 0
	FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedDelayRespawnBitset, iPed)
	
ENDPROC

PROC PROCESS_CLEANUP_OLD_PED_VEHICLE(INT iVeh, INT iPed, BOOL bRespawnInFreshVeh)
	IF iVeh != -1
	AND bRespawnInFreshVeh
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iVehicleSeat = ENUM_TO_INT(VS_DRIVER)
			IF NOT IS_BIT_SET(MC_serverBD.iPedRespawnVehBitset, iVeh)
			AND IS_BIT_SET(MC_serverBD.iPedFirstSpawnVehBitset, iVeh)
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
					PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - needs a new copy of vehicle ",iveh,", cleanup old one")
					CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
					
					IF IS_BIT_SET(MC_serverBD.iVehSpawnBitset, iVeh)
						PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - Clear iVehSpawnBitset for veh ",iveh,", cleaned up for a new copy")
						CLEAR_BIT(MC_serverBD.iVehSpawnBitset, iVeh)
					ENDIF
				ENDIF
				
				PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - cleaned up vehicle ",iveh,", set iPedRespawnVehBitset")
				SET_BIT(MC_serverBD.iPedRespawnVehBitset, iVeh)
			ELSE
				PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - in veh: ", iVeh, " bit (ciPed_BSTwo_RespawnInNewVehicle) has been set. We are waiting for the vehicle to be respawned.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC GET_MISSION_PED_RESPAWN_VEHICLE_DATA(INT iPed, INT iVeh, VEHICLE_INDEX &tempVeh, VEHICLE_SEAT &tempseat, BOOL &bVehSpawn, BOOL &bVehFull)
	
// Do we want to place this ped in a vehicle?
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
	AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
		
		tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
		
		IF NOT IS_ANY_PLAYER_IN_VEHICLE(tempVeh)
			IF NOT IS_VEHICLE_FULL(tempVeh)
				FLOAT froll = GET_ENTITY_ROLL(tempVeh)
				IF froll > -35.0
				AND froll < 35.0

					bVehSpawn = TRUE
					PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - should spawn in vehicle ",iveh,", seat ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iVehicleSeat)
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iVehicleSeat = -3
						tempseat = MC_GET_FIRST_FREE_VEHICLE_SEAT(tempVeh)
						PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - doesn't have a seat preference set, pick first free seat - tempseat = ", ENUM_TO_INT(tempseat))
					ELSE
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iVehicleSeat = VS_ANCHORED_1
						OR IS_VEHICLE_SEAT_FREE(tempVeh,INT_TO_ENUM(VEHICLE_SEAT,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iVehicleSeat))
							tempseat = INT_TO_ENUM(VEHICLE_SEAT,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iVehicleSeat)
							PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - has a seating preference, and the seat is free - tempseat = ", ENUM_TO_INT(tempseat))
						ELSE
							tempseat = MC_GET_FIRST_FREE_VEHICLE_SEAT(tempVeh)
							PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - has a seating preference, but the seat isn't free so get first free seat - tempseat = ", ENUM_TO_INT(tempseat))
						ENDIF
					ENDIF
					
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - not spawning in vehicle ",iveh,", froll is over 35 - froll = ",froll)
				#ENDIF
				ENDIF
			
			ELSE
				PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - not spawning in vehicle ",iveh,", vehicle is full")			
				bVehFull = TRUE
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - not spawning in vehicle ",iveh,", a player is inside")
		#ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - not spawning in vehicle ",iveh,", net ID doesn't exist")
		ELSE
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - not spawning in vehicle ",iveh,", undriveable")
		ENDIF
	#ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_MISSION_PED_RESPAWN_VEHICLE_READY(INT iPed, INT iVeh, VEHICLE_INDEX &tempVeh, BOOL bRespawnInFreshVeh)

	// This bit gets set if the vehicle we're spawning into exists
	IF NOT IS_BIT_SET(MC_serverBD.iVehSpawnBitset, iVeh)
		PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - calling create respawn ped in vehicle but vehicle not created, so waiting for veh: ",iVeh)
		RETURN FALSE
	ELSE
		IF bRespawnInFreshVeh
		AND MC_serverBD_2.iCurrentPedRespawnLives[iPed] <= MC_serverBD_2.iCurrentVehRespawnLives[iVeh]
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - waiting for veh: ", iVeh, " to respawn fresh. Ped Lives: ", MC_serverBD_2.iCurrentPedRespawnLives[iPed], " Veh Lives: ", MC_serverBD_2.iCurrentVehRespawnLives[iVeh])
			RETURN FALSE		
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iVehicleSeat != ENUM_TO_INT(VS_DRIVER)
		AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iVehicleSeat != -3
			
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
				
				tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
				
				IF IS_VEHICLE_SEAT_FREE(tempVeh, VS_DRIVER) AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iForcedVehicleSeat = -1 // -1 means not set.
					PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - calling create respawn ped in vehicle but driver not yet spawned, so waiting for veh: ",iVeh)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE	
	
ENDFUNC

FUNC BOOL SHOULD_MISSION_PED_RESPAWN_IN_VEHICLE(INT iPed, INT iVeh)
	IF NOT SHOULD_PED_SPAWN_IN_VEHICLE(iPed)
		RETURN FALSE
	ENDIF
	
	FMMC_VEHICLE_STATE sVehState
	FILL_FMMC_VEHICLE_STATE_STRUCT(sVehState, iVeh)
	IF SHOULD_CLEANUP_VEH(sVehState)
		RETURN FALSE
	ENDIF
		
	RETURN TRUE
ENDFUNC

FUNC FLOAT GET_MISSION_PED_MINIMUM_SPAWN_DISTANCE_FROM_PLAYER(INT iPed)

	IF IS_ENTITY_USING_THIS_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].eSpawnConditionFlag, SPAWN_CONDITION_FLAG_AMBUSH_ABILITY_ACTIVATED)
		// Let the Ambush ability ensure they're not spawned too close, rather than moving the peds away
		RETURN 1.0
	ENDIF
	
	RETURN 40.0
ENDFUNC

PROC CLEANUP_PED_RESPAWNING_DATA(FMMC_PED_STATE &sPedState, BOOL bResetFails = TRUE)
	
	CLEANUP_GENERAL_RESPAWNING_DATA(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn)
	
	iSpawnPoolPedIndex = -1
	
	MC_serverBD.isearchingforPed = -1
	
	IF bResetFails
		PRINTLN("[MCSpawning][Peds][Ped ", sPedState.iIndex, "] CLEANUP_PED_RESPAWNING_DATA - bResetFails = true. Resetting Ped Spawn Fail Count and Fail Delay.")
		iPedSpawnFailCount[sPedState.iIndex] = 0
		iPedSpawnFailDelay[sPedState.iIndex] = -1
	ENDIF
	
ENDPROC

FUNC BOOL RESPAWN_MISSION_PED(FMMC_PED_STATE &sPedState)
	
	INT iPed = sPedState.iIndex
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].mn = DUMMY_MODEL_FOR_SCRIPT
		RETURN FALSE
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
		RETURN TRUE
	ENDIF
		
	FLOAT fspawnhead
	VEHICLE_INDEX tempVeh
	PED_INDEX tempPed
	VEHICLE_SEAT tempseat
	BOOL bRespawnInFreshVeh = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwo, ciPed_BSTwo_RespawnInNewVehicle)
	INT iVeh = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iVehicle
	BOOL bVehSpawn
	BOOL bIninterior
	BOOL bIgnoreAreaCheck
	BOOL bShouldForceSpawn = SHOULD_FORCE_IMMEDIATE_ENTITY_SPAWN(iPed, CREATION_TYPE_PEDS)
	
	MODEL_NAMES mnPedModelToUse = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].mn
	PED_INDEX piPedToClone = NULL	
	IF IS_PLACED_PED_A_PLAYER_CLONE(mnPedModelToUse, iPed)
		piPedToClone = GET_PLAYER_PED(SWAP_PED_MODEL_FOR_A_PLAYER_CLONE_AND_GET_TARGET_PLAYER_INDEX(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed], mnPedModelToUse))
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - Calling for Model Name: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(mnPedModelToUse))
	#ENDIF
	
	IF iPedSpawnFailDelay[iPed] != -1
	OR FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedDelayRespawnBitset, iPed)
		PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - Delay Respawn is set to: ", iPedSpawnFailDelay[iPed])		
		IF (iPedSpawnFailDelay[iPed] >= 1000)
			iPedSpawnFailDelay[iPed] = -1
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - Reached iPedSpawnFailDelay of 1 second, resetting spawn delay timer")
		ELIF iPedSpawnFailDelay[iPed] != -1
			iPedSpawnFailDelay[iPed] += ROUND(fLastFrameTime * 1000)
		ENDIF		
		RETURN FALSE
	ENDIF
	
	PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - requesting model for ped in respawn")
	REQUEST_MODEL(mnPedModelToUse)
	IF NOT HAS_MODEL_LOADED(mnPedModelToUse)
		PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - FALSE - Ped Model still loading.")		
		RETURN FALSE
	ENDIF
		
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iInventoryBS != 0
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iInventoryBS,ciPed_INV_Briefcase_prop_ld_case_01)
		REQUEST_MODEL(PROP_LD_CASE_01)
		REQUEST_ANIM_DICT("weapons@misc@jerrycan@mp_male")
		IF (NOT HAS_MODEL_LOADED(PROP_LD_CASE_01))
		OR (NOT HAS_ANIM_DICT_LOADED("weapons@misc@jerrycan@mp_male"))
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] RESPAWN_MISSION_PED - FALSE - Ped Inventory Jerry Can Anim not loaded.")			
			RETURN FALSE
		ENDIF		
	ENDIF
	
	IF mnPedModelToUse = INT_TO_ENUM(MODEL_NAMES, HASH("U_M_Y_Juggernaut_01"))
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSixteen, ciPED_BSSixteen_UseJuggernautMoveset)	
		IF NOT HAVE_JUGGERNAUT_ANIMS_LOADED_FOR_CREATION()		
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] RESPAWN_MISSION_PED - FALSE - juggernaut anims ")		
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF mnPedModelToUse = IG_LESTERCREST
		REQUEST_CLIP_SET("move_heist_lester")
		IF NOT HAS_CLIP_SET_LOADED("move_heist_lester")
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - FAILING ON - lester ped clip set")	
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFive, ciPed_BSFive_StartDead)
		REQUEST_ANIM_DICT(MC_GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iDeadAnim))
		IF NOT HAS_ANIM_DICT_LOADED(MC_GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iDeadAnim))
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] RESPAWN_MISSION_PED - FAILING ON - anim dict ", MC_GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iDeadAnim))
			RETURN FALSE
		ENDIF
	ENDIF
		
	IF NOT CAN_REGISTER_MISSION_PEDS(1)
	AND ARE_WE_WAITING_FOR_PEDS_TO_CLEAN_UP()
		#IF IS_DEBUG_BUILD	
		IF NOT CAN_REGISTER_MISSION_PEDS(1)
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - failing to spawn ped because of CAN_REGISTER_MISSION_PEDS ", iPed)
		ENDIF
		IF ARE_WE_WAITING_FOR_PEDS_TO_CLEAN_UP()
			IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
				PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - failing to spawn because of ARE_WE_WAITING_FOR_PEDS_TO_CLEAN_UP: new priority / midpoint this frame")
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HOST_APPLIED_NEW_PRIORITY_MIDFRAME)
			OR IS_BIT_SET(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
				PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - failing to spawn because of ARE_WE_WAITING_FOR_PEDS_TO_CLEAN_UP: new priority / midpoint set by host")
			ENDIF
			IF MC_serverBD.iPedCleanup_NeedOwnershipBS[0] != 0
			OR MC_serverBD.iPedCleanup_NeedOwnershipBS[1] != 0
			OR MC_serverBD.iPedCleanup_NeedOwnershipBS[2] != 0
				PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - failing to spawn because of ARE_WE_WAITING_FOR_PEDS_TO_CLEAN_UP: iPedCleanup_NeedOwnershipBS[0] = ",MC_serverBD.iPedCleanup_NeedOwnershipBS[0],", iPedCleanup_NeedOwnershipBS[1] = ",MC_serverBD.iPedCleanup_NeedOwnershipBS[1],", MC_serverBD.iPedCleanup_NeedOwnershipBS[2] = ",MC_serverBD.iPedCleanup_NeedOwnershipBS[2])
			ENDIF
		ENDIF
		#ENDIF			
		RETURN FALSE
	ENDIF

	IF NOT CAN_RUN_ENTITY_SPAWN_POS_SEARCH(ciEntitySpawnSearchType_Peds, iPed)
		PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - failing to spawn because attempting to spawn one of the following: ped ", MC_serverBD.isearchingforPed," / veh ", MC_serverBD.isearchingforVeh ," / obj ", MC_serverBD.isearchingforObj )		
		RETURN FALSE
	ENDIF

	PROCESS_CLEANUP_OLD_PED_VEHICLE(iVeh, iPed, bRespawnInFreshVeh)
	
	IF SHOULD_MISSION_PED_RESPAWN_IN_VEHICLE(iPed, iVeh)
		PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - spawning  into vehicle ", iVeh, " bRespawnInFreshVeh: ", bRespawnInFreshVeh)
		
		IF NOT IS_MISSION_PED_RESPAWN_VEHICLE_READY(iPed, iVeh, tempVeh, bRespawnInFreshVeh)
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - spawning into vehicle ", iVeh, " But IS_MISSION_PED_RESPAWN_VEHICLE_READY = FALSE")
			
			INT iTeam
			BOOL bCrit
			
			//Check if this ped is marked as critical for any teams
			FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
				IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iMissionCriticalPed[iTeam], iPed)
					bCrit = TRUE
				ENDIF
			ENDFOR
			
			IF NOT bCrit
				IF iPedSpawnFailCount[iPed] < 4
					iPedSpawnFailCount[iPed]++
				ENDIF
				iPedSpawnFailDelay[iPed] = 0 //Start the delay timer
				PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - vehicle spawn failed, setting iPedSpawnFailDelay counting")
			ELSE
				iPedSpawnFailCount[iPed]++
			ENDIF
			
			FMMC_SET_LONG_BIT(MC_serverBD.iPedDelayRespawnBitset, iPed)
			
			IF MC_serverBD.isearchingforped != -1
				PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - Clearing iSearchingForPed: ", MC_serverBD.isearchingforPed)
				MC_serverBD.isearchingforPed = -1
			ENDIF
			
			vEntityRespawnLoc = <<0, 0, 0>>
			SpawnInterior = NULL			
		ELSE
			
			IF MC_serverBD.isearchingforped != iPed
				MC_serverBD.isearchingforped = iPed
				PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - Assigning iSearchingForPed: ", MC_serverBD.isearchingforPed)
			ENDIF
			
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
				
				BOOL bVehFull
				
				GET_MISSION_PED_RESPAWN_VEHICLE_DATA(iPed, iVeh, tempVeh, tempseat, bVehSpawn, bVehFull)
				
				IF bVehFull
					PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - Not able to spawn into iVeh: ", iVeh, " because it's full.. Clearing respawn data so that the system can spawn something else.")
				
					CLEANUP_PED_RESPAWNING_DATA(sPedState, FALSE)
					
					IF iPedSpawnFailCount[iPed] < 4
						iPedSpawnFailCount[iPed]++
					ENDIF
					iPedSpawnFailDelay[iPed] = 0
					
					#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("MISSION CONTROLLER - RESPAWN_MISSION_PED - A Ped is attempting to respawn into a Vehicle which is full. This should not be set up like this in the Creator.")
					ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_NO_VEHICLES_PLACE_IN_NEAREST, ENTITY_RUNTIME_ERROR_TYPE_WARNING_PED, "Stuck trying to spawn in vehicle # - check settings!", iPed, iVeh)
					#ENDIF
				ENDIF
				
				IF NOT bVehSpawn
				OR bVehFull
					PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - Not able to spawn into iVeh: ", iVeh, " yet. Returning false.")
					RETURN FALSE
				ENDIF
				
				PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - spawning into vehicle ", iVeh, " seat = ",ENUM_TO_INT(tempseat))
				
				BOOL bSpawned = FALSE
				
				IF ENUM_TO_INT(tempSeat) = VS_ANCHORED_1
					INT iTemp
					vEntityRespawnLoc = GET_PED_FMMC_SPAWN_LOCATION(iPed, bIgnoreAreaCheck, iTemp)
					FLOAT heading = MC_GET_PED_SPAWN_HEADING(iPed, -1, IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PED_SPAWN_AT_SECOND_POSITION), 0)
					IF FMMC_CREATE_NET_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed], PEDTYPE_MISSION, mnPedModelToUse, vEntityRespawnLoc, heading )
						VECTOR offset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]), vEntityRespawnLoc)
						offset = offset + <<0.0,0.0, 1.85*0.5>>
						heading = heading - GET_ENTITY_HEADING(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
						
						ATTACH_ENTITY_TO_ENTITY(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed]), NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]), 0, offset, <<0,0,heading>>, TRUE, TRUE)
						bSpawned = TRUE											
					ENDIF
				ELSE
					IF NOT IS_VEHICLE_SEAT_FREE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]), tempseat)
						tempseat = MC_GET_FIRST_FREE_VEHICLE_SEAT(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
					ENDIF
					IF FMMC_CREATE_NET_PED_IN_VEHICLE(MC_serverBD_1.sFMMC_SBD.niPed[iPed], MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh], PEDTYPE_MISSION, mnPedModelToUse,tempseat)
						bSpawned = TRUE
					ENDIF
				ENDIF
				
				IF bSpawned
					PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - spawning into vehicle ", iVeh, " and vehicle has now spawned!")

					FILL_FMMC_PED_STATE_STRUCT(sPedState, iPed, TRUE)
					
					//Make sure the AI blip struct is clear for this index.
					AI_BLIP_STRUCT emptyAIBlipStruct
					biHostilePedBlip[iPed] = emptyAIBlipStruct
					
					tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
					
					MC_SET_UP_PED(tempPed, iPed, DEFAULT, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
					
					IF piPedToClone != NULL
						FMMC_CLONE_PED(tempPed, piPedToClone)
						SET_MODEL_AS_NO_LONGER_NEEDED(mnPedModelToUse)
					ENDIF
							
					IF iVeh != -1
					AND bRespawnInFreshVeh
						IF IS_BIT_SET(MC_serverBD.iPedRespawnVehBitset, iVeh)
							CLEAR_BIT(MC_serverBD.iPedRespawnVehBitset, iVeh)
							PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - in veh: ", iVeh, " Clearing Bit iPedRespawnVehBitset (ciPed_BSTwo_RespawnInNewVehicle)")
						ENDIF
						
						IF NOT IS_BIT_SET(MC_serverBD.iPedFirstSpawnVehBitset, iVeh)
							SET_BIT(MC_serverBD.iPedFirstSpawnVehBitset, iVeh)
							PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - in veh: ", iVeh, " SET Bit iPedFirstSpawnVehBitset (ciPed_BSTwo_RespawnInNewVehicle)")
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
					FMMC_SET_LONG_BIT(iPedGivenDebugName, iPed)
					CLEAR_PED_IS_SPOOKED_DEBUG(iPed)
					CLEAR_PED_IS_AGGROD_DEBUG(iPed)
					#ENDIF
					
					CLEANUP_PED_RESPAWNING_DATA(sPedState)
					
					// Clear some of this ped's data
					RESET_PED_SERVER_STATE_DATA( iPed )
					
					FMMC_CLEAR_LONG_BIT(iPedDisableReactionUntilTaskBitset, iPed)
					
					PROCESS_GIVING_PED_WEAPON_ON_RULE(sPedState)
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFourteen, ciPED_BSFourteen_DontCryForHelpWhenStunned)
						SET_PED_CONFIG_FLAG(tempPed, PCF_DontCryForHelpOnStun, TRUE)
					ENDIF
					
					PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE(sPedState)
					
					SET_ENTITY_VISIBLE(tempPed,TRUE)
									
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iTeam[MC_playerBD[iPartToUse].iTeam] = ciPed_RELATION_SHIP_DISLIKE
					OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iTeam[MC_playerBD[iPartToUse].iTeam] = ciPed_RELATION_SHIP_HATE
						IF WILL_PED_EVER_BE_HOSTILE(iPed)
							MC_serverBD.iNumPedsSpawned++
						ENDIF
					ENDIF

					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFive, ciPed_BSFive_StartDead)
						REMOVE_ANIM_DICT(MC_GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iDeadAnim))
					ENDIF

					GET_NEXT_SPAWNING_PED_AFTER_VEHICLE_SPAWN(iveh)										
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwelve, ciPed_BSTwelve_SpawnPedInCover)
						IF NOT IS_PED_IN_ANY_VEHICLE(tempPed)
							VECTOR vCoverCoords = GET_ENTITY_COORDS(tempPed)
							PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - Entering Cover.")
							SET_PED_TO_LOAD_COVER(tempPed, TRUE)
							COVERPOINT_INDEX cpIndex = GET_CLOSEST_COVER_POINT_TO_LOCATION(GET_ENTITY_COORDS(tempPed), vCoverCoords)
							TASK_PUT_PED_DIRECTLY_INTO_COVER(tempPed, vCoverCoords, -1, TRUE, DEFAULT, TRUE, FALSE, cpIndex)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(tempPed, TRUE)
						ENDIF
					ENDIF
					
					START_ENTITY_PTFX_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sEntitySpawnPTFX,  GET_ENTITY_COORDS(tempPed),  GET_ENTITY_ROTATION(tempPed))
					PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - spawning into vehicle ", iVeh, " RETURNING TRUE")					
					RETURN TRUE
				ELSE
					PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - Attempting to create ped & vehicle still...")
				ENDIF
			ELSE
				PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - spawning into vehicle ", iVeh, " But we do not have control of Vehicle. Requesting Control.")
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			ENDIF
		ENDIF		
	ELSE		
		IF MC_serverBD.isearchingforped != iPed
			MC_serverBD.isearchingforped = iPed
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - Assigning iSearchingForPed: ", MC_serverBD.isearchingforPed)
		ENDIF
		
		IF iEntityRespawnType != ci_TARGET_PED
		OR iEntityRespawnID != iPed
			vEntityRespawnLoc = <<0, 0, 0>>
			SpawnInterior = NULL
			iEntityRespawnType = ci_TARGET_PED
			iEntityRespawnID = iPed
		ENDIF
		
		IF IS_VECTOR_ZERO(vEntityRespawnLoc)
			CLEAR_BIT(iLocalBoolCheck10, LBOOL10_PED_SPAWN_AT_SECOND_POSITION)
			vEntityRespawnLoc = GET_PED_FMMC_SPAWN_LOCATION(iPed, bIgnoreAreaCheck, iEntityRespawnRandomSelected)
						
			IF bIgnoreAreaCheck
				SET_BIT(iLocalBoolCheck4, LBOOL4_RESPAWN_SHOULD_FORCE_AREA_CHECK)
			ELSE
				CLEAR_BIT(iLocalBoolCheck4, LBOOL4_RESPAWN_SHOULD_FORCE_AREA_CHECK)
			ENDIF
			
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - Grabbed vEntityRespawnLoc: ", vEntityRespawnLoc, " with bIgnoreAreaCheck: ", bIgnoreAreaCheck)
		ELSE
			bIgnoreAreaCheck = IS_BIT_SET(iLocalBoolCheck4, LBOOL4_RESPAWN_SHOULD_FORCE_AREA_CHECK)
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sTrainAttachmentData.iTrainIndex != -1
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sTrainAttachmentData.vCarriageOffsetPosition)		
			bShouldForceSpawn = TRUE
		ENDIF
		
		IF NOT bIgnoreAreaCheck
		AND NOT NETWORK_ENTITY_AREA_DOES_EXIST(MC_serverBD.iSpawnArea)
		AND NOT CAN_SPAWN_AND_PLAY_ANIM_CLOSE_TO_VEHICLE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim)
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - Using a Network Area Check.")
			MC_serverBD.iSpawnArea = NETWORK_ADD_ENTITY_AREA((vEntityRespawnLoc-<<0.35,0.35,0.35>>),(vEntityRespawnLoc+<<0.35,0.35,0.35>>))
			REINIT_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
			RETURN FALSE
		ENDIF
	
		IF NOT (bIgnoreAreaCheck
		OR NETWORK_ENTITY_AREA_HAVE_ALL_REPLIED(MC_serverBD.iSpawnArea)
		OR CAN_SPAWN_AND_PLAY_ANIM_CLOSE_TO_VEHICLE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim)
		OR bShouldForceSpawn)
			IF (NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdSpawnAreaTimeout))
			OR GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdSpawnAreaTimeout) >= ciFMMCSpawnAreaTimeout
				
				PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - waited for everyone to reply back on entity area for over 10s, delete and try again")
				NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
				MC_serverBD.iSpawnArea = -1
				RESET_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
				
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - waiting for everyone to reply back on entity area, waited for ",GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdSpawnAreaTimeout),"ms")
			#ENDIF
			ENDIF			
			RETURN FALSE
		ENDIF
		
		IF (NOT bIgnoreAreaCheck
		AND NOT NETWORK_ENTITY_AREA_IS_OCCUPIED(MC_serverBD.iSpawnArea)	
		AND NOT IS_PED_VISIBLE_FOR_RESPAWN(iPed))
		OR (CAN_SPAWN_AND_PLAY_ANIM_CLOSE_TO_VEHICLE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim) AND NOT IS_PED_VISIBLE_FOR_RESPAWN(iPed))
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPed_BSEight_ForceSpawnAtAuthoredCoords)
		OR bShouldForceSpawn
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - is spawning with Creator Authored Coords.")
		ELSE
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - Grabbing some appropriate COORDS.")
			
			IF SpawnInterior = NULL
				SpawnInterior =  GET_INTERIOR_AT_COORDS(vEntityRespawnLoc)
			ENDIF					
			IF SpawnInterior = NULL
				bIninterior = FALSE
			ELSE
				bIninterior = TRUE
			ENDIF
			
			SPAWN_SEARCH_PARAMS SpawnSearchParams
			SpawnSearchParams.bConsiderInteriors = bIninterior
			SpawnSearchParams.fMinDistFromPlayer = GET_MISSION_PED_MINIMUM_SPAWN_DISTANCE_FROM_PLAYER(iPed)
			
			IF bIgnoreAreaCheck
				SpawnSearchParams.vFacingCoords = vEntityRespawnLoc
				//Want to spawn outside of a range from vEntityRespawnLoc:
				SpawnSearchParams.vAvoidCoords[0] = vEntityRespawnLoc
				SpawnSearchParams.fAvoidRadius[0] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fRespawnMinRange
			ENDIF
			
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - Calling GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY and vEntityRespawnLoc: ", vEntityRespawnLoc, " fRespawnRange: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fRespawnRange, " fAvoidRadius: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fRespawnMinRange)
			
			IF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vEntityRespawnLoc, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fRespawnRange, vEntityRespawnLoc, fspawnhead, SpawnSearchParams)
				
				PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - New vEntityRespawnLoc: ", vEntityRespawnLoc)
				
				IF NOT IS_OBJECTIVE_PED(iPed)
				AND NOT IS_CARRIER_PED(iPed)
					
					FLOAT fVehRad = 6.0 * (1.0 / (iPedSpawnFailCount[iPed] + 1))
					FLOAT fVisionRangeCheck = GET_RESPAWN_RANGE_CHECK(iPed)
					
					IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(vEntityRespawnLoc, fVehRad, 1, 1, 1, TRUE, TRUE, TRUE, fVisionRangeCheck, FALSE, -1, TRUE, (fVisionRangeCheck / 2.0))								
						PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - coords visible so delaying ped spawn.")																							
						
						CLEANUP_PED_RESPAWNING_DATA(sPedState, FALSE)
					
						INT iTeam
						BOOL crit
						
						//Check if this ped is marked as critical for any teams
						FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
							IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iMissionCriticalPed[iTeam], iPed)
								crit = TRUE
							ENDIF
						ENDFOR
						
						IF NOT crit
							IF iPedSpawnFailCount[iPed] < 4
								iPedSpawnFailCount[iPed]++
							ENDIF
							iPedSpawnFailDelay[iPed] = 0 //Start the delay timer
							PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - coords visible, setting iPedSpawnFailDelay timer running")
						ELSE
							iPedSpawnFailCount[iPed]++
						ENDIF								
						FMMC_SET_LONG_BIT(MC_serverBD.iPedDelayRespawnBitset, iPed)
						RETURN FALSE
					ELSE
						PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - Spawning with new found safe position.")
					ENDIF					
				ENDIF				
			ELSE
				PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - Waiting for GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY...")
				RETURN FALSE
			ENDIF
		ENDIF

		PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - spawning with coords: ",vEntityRespawnLoc, ", forceSpawning: ", IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPed_BSEight_ForceSpawnAtAuthoredCoords))

		IF FMMC_CREATE_NET_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed], PEDTYPE_MISSION, mnPedModelToUse, vEntityRespawnLoc, 
					MC_GET_PED_SPAWN_HEADING(iPed, -1, IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PED_SPAWN_AT_SECOND_POSITION), iEntityRespawnRandomSelected),
					DEFAULT, DEFAULT, DEFAULT, bShouldForceSpawn)
			
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - spawning FMMC_CREATE_NET_PED TRUE")
			
			FILL_FMMC_PED_STATE_STRUCT(sPedState, iPed, TRUE)
			
			//Make sure the AI blip struct is clear for this index.
			AI_BLIP_STRUCT emptyAIBlipStruct
			biHostilePedBlip[iPed] = emptyAIBlipStruct
	
			tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			
			MC_SET_UP_PED(tempPed, iPed, DEFAULT)
			
			IF piPedToClone != NULL
				FMMC_CLONE_PED(tempPed, piPedToClone)
				SET_MODEL_AS_NO_LONGER_NEEDED(mnPedModelToUse)
			ENDIF
			
			IF iVeh != -1
			AND bRespawnInFreshVeh
				IF IS_BIT_SET(MC_serverBD.iPedRespawnVehBitset, iVeh) 
					CLEAR_BIT(MC_serverBD.iPedRespawnVehBitset, iVeh)
					PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - in veh: ", iVeh, " Clearing Bit iPedRespawnVehBitset (ciPed_BSTwo_RespawnInNewVehicle)")
				ENDIF
				
				IF NOT IS_BIT_SET(MC_serverBD.iPedFirstSpawnVehBitset, iVeh)
					SET_BIT(MC_serverBD.iPedFirstSpawnVehBitset, iVeh)
					PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - in veh: ", iVeh, " SET Bit iPedFirstSpawnVehBitset (ciPed_BSTwo_RespawnInNewVehicle)")
				ENDIF
			ENDIF

			#IF IS_DEBUG_BUILD
			FMMC_SET_LONG_BIT(iPedGivenDebugName, iPed)
			CLEAR_PED_IS_SPOOKED_DEBUG(iPed)
			CLEAR_PED_IS_AGGROD_DEBUG(iPed)
			#ENDIF
					
			RESET_PED_SERVER_STATE_DATA( iPed )
			
			FMMC_CLEAR_LONG_BIT(iPedDisableReactionUntilTaskBitset, iPed)
						
			PROCESS_GIVING_PED_WEAPON_ON_RULE(sPedState)
			
			PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE(sPedState)
						
			CLEANUP_PED_RESPAWNING_DATA(sPedState)					
			
			SET_ENTITY_VISIBLE(tempPed,TRUE)
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFive, ciPed_BSFive_StartDead)
				REMOVE_ANIM_DICT(MC_GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iDeadAnim))
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFourteen, ciPED_BSFourteen_DontCryForHelpWhenStunned)
				SET_PED_CONFIG_FLAG(tempPed, PCF_DontCryForHelpOnStun, TRUE)
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iTeam[MC_playerBD[iPartToUse].iTeam] = ciPed_RELATION_SHIP_DISLIKE
			OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iTeam[MC_playerBD[iPartToUse].iTeam] = ciPed_RELATION_SHIP_HATE
				IF WILL_PED_EVER_BE_HOSTILE(iPed)
					MC_serverBD.iNumPedsSpawned++
				ENDIF
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sTrainAttachmentData.iTrainIndex != -1
			AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sTrainAttachmentData.vCarriageOffsetPosition)
				VECTOR vPedCoord
				VECTOR vPedRot
				
				MC_GET_TRAIN_ATTACHMENT_POINT_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sTrainAttachmentData, vPedCoord, vPedRot)
				
				FLOAT fTempZ
				IF GET_GROUND_Z_FOR_3D_COORD(vPedCoord, fTempZ)
					vPedCoord.z = fTempZ
				ENDIF
				
				SET_ENTITY_HEADING(tempPed, vPedRot.z)
				SET_ENTITY_COORDS(tempPed, vPedCoord)
				
				PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - placed on a Train - Setting vPedCoord: ", vPedCoord, "  Heading: ", vPedRot.z)
			ENDIF
						
			IF MC_PROCESS_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sWarpLocationSettings, CREATION_TYPE_PEDS, iPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpawnGroup, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpawnSubGroupBS, tempPed)	
				MC_REMOVE_FMMC_PED_DEFENSIVE_AREA(tempPed, iPed, TRUE, TRUE)
				MC_SET_UP_FMMC_PED_DEFENSIVE_AREA(tempPed, iPed, GET_ENTITY_COORDS(tempPed, FALSE), TRUE)	
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwelve, ciPed_BSTwelve_SpawnPedInCover)
				IF NOT IS_PED_IN_ANY_VEHICLE(tempPed)					
					VECTOR vCoverCoords = GET_ENTITY_COORDS(tempPed)
					PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - Entering Cover.")
					COVERPOINT_INDEX cpIndex = GET_CLOSEST_COVER_POINT_TO_LOCATION(GET_ENTITY_COORDS(tempPed), vCoverCoords)
					SET_PED_TO_LOAD_COVER(tempPed, TRUE)
					TASK_PUT_PED_DIRECTLY_INTO_COVER(tempPed, vCoverCoords, -1, TRUE, DEFAULT, TRUE, FALSE, cpIndex)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(tempPed, TRUE)
				ENDIF
			ENDIF
			
			START_ENTITY_PTFX_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sEntitySpawnPTFX,  GET_ENTITY_COORDS(tempPed),  GET_ENTITY_ROTATION(tempPed))
			
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - SPAWNED and on foot RETURNING TRUE")
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Main Server Task Processing Functions  -------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Server task processing functions for creator placed peds.   									  --------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_TASK_CHOOSE_NEW_TASK_SERVER(FMMC_PED_STATE &sPedState)

	PROCESS_PED_TASK_NOTHING_BRAIN(sPedState)
	
ENDPROC 

FUNC BOOL SHOULD_COMBAT_OR_ALTERTNESS_FORCE_GOTO_TASK_TO_FINISH_EARLY(FMMC_PED_STATE &sPedState)
	
	IF IS_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, ciENTIRE_TASK_BITSET_LOOP_GOTO)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetFour, ciPed_BSFour_StayInTasks_On_Aggro) 
	AND SHOULD_PED_START_COMBAT(sPedState) 
	AND NOT IS_STEALTH_AND_AGGRO_SYSTEMS_CURRENTLY_ACTIVE(sPedState)
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_TASK_GOTO_COORDS_SERVER - SHOULD_COMBAT_OR_ALTERTNESS_FORCE_GOTO_TASK_TO_FINISH_EARLY is true")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_TASK_GOTO_COORD_FINISH_EARLY(FMMC_PED_STATE &sPedState)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetEight, ciPed_BSEight_UseDistanceToCapture)
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD_2.iTargetID[sPedState.iIndex] = -1
	OR MC_serverBD_2.iPartPedFollows[sPedState.iIndex] != -1	
	OR SHOULD_COMBAT_OR_ALTERTNESS_FORCE_GOTO_TASK_TO_FINISH_EARLY(sPedState)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_TASK_GOTO_COORDS_SERVER(FMMC_PED_STATE &sPedState, FMMC_PED_VEHICLE_DATA &sPedVehicleData)
			
	IF SHOULD_TASK_GOTO_COORD_FINISH_EARLY(sPedState)
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_TASK_GOTO_COORDS_SERVER - SHOULD_TASK_GOTO_COORD_FINISH_EARLY is true")
	
		IF MC_serverBD_2.iTargetID[sPedState.iIndex] != ciSECONDARY_GOTO
			FMMC_SET_LONG_BIT(MC_serverBD.iPedAtGotoLocBitset, sPedState.iIndex)
		ELSE
			FMMC_SET_LONG_BIT(MC_serverBD.iPedAtSecondaryGotoLocBitset, sPedState.iIndex)
		ENDIF
		
		SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK)
	ELSE
		BOOL bArrived = HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION(sPedState, sPedVehicleData)
		
		IF bArrived
			IF MC_serverBD_2.iTargetID[sPedState.iIndex] != ciSECONDARY_GOTO
				
				BOOL bWasOnDefensiveAreaGoto, bGoToIsLooped
				
				IF SHOULD_PED_DO_DEFENSIVE_AREA_GOTO(sPedState.pedIndex, sPedState.iIndex)
					bWasOnDefensiveAreaGoto = TRUE
				ENDIF
								
				IF SHOULD_PED_MOVE_ON_FROM_GOTO_COORDS_STATE(sPedState.iIndex, bGoToIsLooped, TRUE)
					MANAGE_PED_ARRIVAL_RESULTS(sPedState.iIndex, TRUE)
					
					MC_serverBD_2.iTargetID[sPedState.iIndex] = -1
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_TASK_GOTO_COORDS_SERVER PED finished all gotos: ", sPedState.iIndex)
					
					IF bWasOnDefensiveAreaGoto
						FMMC_SET_LONG_BIT(MC_serverBD.iPedDoneDefensiveAreaGoto, sPedState.iIndex)
					ENDIF
					
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_TASK_GOTO_COORDS_SERVER PED calling PROCESS_PED_TASK_NOTHING_BRAIN to see what state we should go to now: ", sPedState.iIndex)
					PROCESS_PED_TASK_NOTHING_BRAIN(sPedState)
				ELSE
					
					IF bGoToIsLooped
						INT iIndex = 0
						IF GET_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_LOOP_GOTO_START_INDEX(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData) > -1
							iIndex = GET_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_LOOP_GOTO_START_INDEX(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData)
						ENDIF
						MC_serverBD_2.iPedGotoProgress[sPedState.iIndex] = iIndex
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_TASK_GOTO_COORDS_SERVER PED ", sPedState.iIndex, " finished all gotos but set to loop (setting MC_serverBD_2.iPedGotoProgress[iPed] back to: ", iIndex, ")")
					ENDIF
					
					//Do we need to wait for the next goto? Set their state to nothing, and if they need to get back on their Go To
					// then the PROCESS_PED_TASK_NOTHING_BRAIN check will get them back onto the GoTo. Otherwise the function will
					// shunt them into any other task they might want to perform.
					SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK)
					PROCESS_PED_TASK_NOTHING_BRAIN(sPedState)
					
					IF MC_serverBD_2.iPedState[sPedState.iIndex] != ciTASK_GOTO_COORDS
						
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_TASK_GOTO_COORDS_SERVER PED finished goto, need to wait before starting next stage: ", sPedState.iIndex)
						
						IF bWasOnDefensiveAreaGoto
							FMMC_SET_LONG_BIT(MC_serverBD.iPedDoneDefensiveAreaGoto, sPedState.iIndex)
						ENDIF
						
					ELSE
						IF bWasOnDefensiveAreaGoto
						AND NOT SHOULD_PED_DO_DEFENSIVE_AREA_GOTO(sPedState.pedIndex, sPedState.iIndex) // Done with defensive area goto
							FMMC_SET_LONG_BIT(MC_serverBD.iPedDoneDefensiveAreaGoto, sPedState.iIndex)
						ENDIF
					ENDIF
					
				ENDIF
			ELSE
				IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAtSecondaryGotoLocBitset, sPedState.iIndex)
					FMMC_SET_LONG_BIT(MC_serverBD.iPedAtSecondaryGotoLocBitset, sPedState.iIndex)
					FMMC_CLEAR_LONG_BIT(iPedShouldNotCleanupThisFrameBS, sPedState.iIndex)
					MC_serverBD_2.iTargetID[sPedState.iIndex] = -1
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_TASK_GOTO_COORDS_SERVER PED finished secondary goto: ",sPedState.iIndex)
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetNine, ciPed_BSNine_CleanupOnSecondaryTaskCompleted)
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_TASK_GOTO_COORDS_SERVER | PROCESS_PED_FINISHED_GOTO_INTERRUPT -Cleaning up ped early because ciPed_BSNine_CleanupOnSecondaryTaskCompleted: ", sPedState.iIndex)
						IF SHOULD_CLEANUP_PED(sPedState)
						AND CLEANUP_PED_EARLY(sPedState)
							SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK)
							EXIT
						ELSE
							SCRIPT_ASSERT("PROCESS_TASK_GOTO_COORDS_SERVER | PROCESS_PED_FINISHED_GOTO_INTERRUPT - I was expecting CLEANUP_PED_EARLY to return TRUE, because ciPed_BSNine_CleanupOnSecondaryTaskCompleted")
							PROCESS_PED_TASK_NOTHING_BRAIN(sPedState)
						ENDIF
					ELSE
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_TASK_GOTO_COORDS_SERVER | PROCESS_PED_FINISHED_GOTO_INTERRUPT - PED calling PROCESS_PED_TASK_NOTHING_BRAIN to see what state we should go to now: ", sPedState.iIndex)
						PROCESS_PED_TASK_NOTHING_BRAIN(sPedState)
					ENDIF
					
				ENDIF
			ENDIF
		ELSE
			// url:bugstar:2105607
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetFive, ciPed_BSFive_AbortGoToAndFleeIfVehicleIsStuck)
			AND sPedState.bIsInAnyVehicle
				
				MODEL_NAMES mnVeh 
				mnVeh = GET_ENTITY_MODEL(sPedState.vehIndexPedIsIn)
				
				IF NOT (IS_THIS_MODEL_A_BOAT(mnVeh) OR IS_THIS_MODEL_A_PLANE(mnVeh) OR IS_THIS_MODEL_A_HELI(mnVeh))
				AND IS_CONTROLLED_VEHICLE_UNABLE_TO_GET_TO_ROAD(sPedState.pedIndex)
					MC_ServerBD_2.iAssociatedAction[sPedState.iIndex] = ciACTION_NOTHING 
					MC_serverBD.niTargetID[sPedState.iIndex] = NULL 
					MC_serverBD_2.iTargetID[sPedState.iIndex] = -1 
					SET_PED_STATE(sPedState.iIndex, ciTASK_ABORT_GOTO_AND_FLEE)
					FMMC_SET_LONG_BIT(MC_serverBD.iPedAtGotoLocBitset, sPedState.iIndex)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_TASK_GOTO_COORDS_SERVER Vehicle unable to get to road, ped ", sPedState.iIndex, " set to ciTASK_ABORT_GOTO_AND_FLEE")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC 

PROC PROCESS_TASK_DEFEND_AREA_SERVER(FMMC_PED_STATE &sPedState)
	IF sPedState.bIsInAnyVehicle
		SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK)
	ELSE
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedBitset, sPedState.iIndex)
		OR SHOULD_PED_START_COMBAT(sPedState)
		OR HAS_PED_BEEN_SPOOKED(sPedState)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - [SpookAggro] PROCESS_TASK_DEFEND_AREA_SERVER Ped ", sPedState.iIndex, " Cancelling ciTASK_DEFEND_AREA Tasks.")
			SET_PED_STATE(sPedState.iIndex, ciTASK_GENERAL_COMBAT #IF IS_DEBUG_BUILD , 6 #ENDIF )
		ENDIF
	ENDIF
ENDPROC 

PROC PROCESS_TASK_ATTACK_ENTITY_SERVER(FMMC_PED_STATE &sPedState)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedFixationTarget = -1
	OR FMMC_IS_LONG_BIT_SET(MC_serverBD_1.iPedFixationTaskEnded, sPedState.iIndex)
		EXIT
	ENDIF

	FMMC_SET_LONG_BIT(MC_serverBD_1.iPedFixationTaskStarted, sPedState.iIndex)
	
	MC_serverBD.niTargetID[sPedState.iIndex] = MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedFixationTarget]
	
	BOOL bFinished
	PED_INDEX piTarget
	
	PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - [PED_FIXATE] - PROCESS_TASK_ATTACK_ENTITY_SERVER - iPed: ", sPedState.iIndex, " Checking if ped should still be fixated... Range: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].fPedFixationInterruptPlayerRange, " On Target: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedFixationTarget)
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD.niTargetID[sPedState.iIndex])
		piTarget = NET_TO_PED(MC_serverBD.niTargetID[sPedState.iIndex])
		IF IS_PED_INJURED(piTarget)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - [PED_FIXATE] - PROCESS_TASK_ATTACK_ENTITY_SERVER - iPed: ", sPedState.iIndex, " Fixate Ped Dead Clearing Task. Choose new task.")
			bFinished = TRUE
		ENDIF
	ELSE
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - [PED_FIXATE] - PROCESS_TASK_ATTACK_ENTITY_SERVER - iPed: ", sPedState.iIndex, " Fixate Ped: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedFixationTarget, " No Longer Exists. Clearing Task. Choose new task.")
		bFinished = TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].fPedFixationInterruptPlayerRange > -1.0
		IF CHECK_PLAYERS_BEING_HOSTILE_IN_RANGE_OF_PED(sPedState.iIndex, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].fPedFixationInterruptPlayerRange)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - [PED_FIXATE] - PROCESS_TASK_ATTACK_ENTITY_SERVER - iPed: ", sPedState.iIndex, " Fixate Ped Player Interrupted Clearing Task. Choose new task.")
			bFinished = TRUE
		ENDIF
	ENDIF
	
	IF bFinished
		MC_ServerBD_2.iAssociatedAction[sPedState.iIndex] = ciACTION_NOTHING
		MC_serverBD.niTargetID[sPedState.iIndex] = NULL
		MC_serverBD_2.iTargetID[sPedState.iIndex] = -1
		SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK)
		SET_PED_RETASK_DIRTY_FLAG(sPedState)
		FMMC_SET_LONG_BIT(MC_serverBD_1.iPedFixationTaskEnded, sPedState.iIndex)
	ENDIF
	
ENDPROC 

PROC PROCESS_TASK_DEFEND_ENTITY_SERVER(FMMC_PED_STATE &sPedState)
	UNUSED_PARAMETER(sPedState)
ENDPROC 

PROC PROCESS_TASK_GENERAL_COMBAT_SERVER(FMMC_PED_STATE &sPedState)
	
	IF sPedState.bIsOnPlacedTrain
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetSeventeen, ciPed_BSSeventeen_TrainCombatTasking)
		SET_PED_STATE(sPedState.iIndex, ciTASK_COMBAT_TRAIN #IF IS_DEBUG_BUILD , 5 #ENDIF )
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_TASK_GENERAL_COMBAT_SERVER - Ped ", sPedState.iIndex, " should go into combat state - ON TRAIN instead of general combat.")
		EXIT
	ENDIF 

	IF SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(sPedState, TRUE #IF IS_DEBUG_BUILD , 6 #ENDIF)
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_TASK_GENERAL_COMBAT_SERVER Ped ", sPedState.iIndex, " Cancelling Combat Tasks for Animation.")
		FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedSecondaryTaskActive, sPedState.iIndex)
		MC_ServerBD_2.iAssociatedAction[sPedState.iIndex] = ciACTION_NOTHING
		MC_serverBD.niTargetID[sPedState.iIndex] = NULL
		MC_serverBD_2.iTargetID[sPedState.iIndex] = -1
		IF IS_BIT_SET(sPedState.iBitset, FMMC_PED_STATE_IN_COMBAT)
		AND CAN_PED_TASKS_BE_CLEARED(sPedState.pedIndex)
			CLEAR_PED_TASKS(sPedState.pedIndex)
		ENDIF
		SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK)
	ENDIF
	
ENDPROC 

PROC PROCESS_TASK_WANDER_SERVER(FMMC_PED_STATE &sPedState)
	IF NOT sPedState.bIsInAnyVehicle
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedBitset, sPedState.iIndex)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - [SpookAggro] PROCESS_TASK_WANDER_SERVER Ped ", sPedState.iIndex, " Cancelling ciTASK_WANDER Tasks.")
			SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK)
		ENDIF
	ENDIF
ENDPROC 

PROC PROCESS_TASK_GOTO_ENTITY_SERVER(FMMC_PED_STATE &sPedState, FMMC_PED_TARGET_DATA &sPedTargetData)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedEscortRange = 0
	AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].vPedHeliEscortOffset)
		IF IS_ENTITY_A_VEHICLE(sPedTargetData.tempTarget)
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetTwelve, ciPed_BSTwelve_DoNotTryToEnterGoToTargetVehicle)
				IF sPedState.bIsInAnyVehicle
					
					MANAGE_PED_ARRIVAL_RESULTS(sPedState.iIndex, FALSE)
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].fRange = cfMAX_PATROL_RANGE
						SET_PED_STATE(sPedState.iIndex, ciTASK_WANDER)
					ELSE
						IF 	NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetFive, ciPED_BSFive_PreventCombatTaskingWhileDoingGoto)
						AND SHOULD_PED_START_COMBAT(sPedState)
							PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - [SpookAggro] PROCESS_TASK_GOTO_ENTITY_SERVER Ped ", sPedState.iIndex, " Cancelling ciTASK_GOTO_ENTITY Tasks.")
							SET_PED_STATE(sPedState.iIndex, ciTASK_GENERAL_COMBAT #IF IS_DEBUG_BUILD ,5 #ENDIF )
						ENDIF
					ENDIF
				ELSE
					VEHICLE_SEAT vsSeat
					vsSeat = GET_VEHICLE_SEAT_PED_SHOULD_ENTER(sPedTargetData.targetVeh, sPedState.iIndex)
					
					IF vsSeat = VS_ANY_PASSENGER
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_TASK_GOTO_ENTITY_SERVER Ped ",sPedState.iIndex, " trying to enter a vehicle, but the vehicle is full - return to choose new task state")
						MC_serverBD.niTargetID[sPedState.iIndex] = NULL
						SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK)
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetSix, ciPed_BSSix_PedArrivalResultsTriggerOnTaskCompletion)
				AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedTasksArrivalResultsBS, sPedState.iIndex)
					VECTOR vTarget
					IF sPedTargetData.bExists
					AND IS_VEHICLE_DRIVEABLE(sPedTargetData.targetVeh)
						vTarget = GET_ENTITY_COORDS(sPedTargetData.targetVeh)
					ENDIF
													
					IF NOT IS_VECTOR_ZERO(vTarget)
						FLOAT fTargetDist
						fTargetDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(sPedState.pedIndex, vTarget)
						FLOAT fGotoRange
						fGotoRange = 10
						
						IF sPedState.bIsInAnyVehicle
							IF IS_PED_IN_ANY_PLANE(sPedState.pedIndex)
								fGotoRange = 130
							ELIF IS_PED_IN_ANY_HELI(sPedState.pedIndex)
								fGotoRange = 60
							ENDIF
						ENDIF
						
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedEscortRange != 0
							fGotoRange = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedEscortRange)
						ENDIF
						
						IF fTargetDist <= FMAX(fGotoRange, 15.0)
							MANAGE_PED_ARRIVAL_RESULTS(sPedState.iIndex, FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELIF IS_ENTITY_AN_OBJECT(sPedTargetData.tempTarget)
			IF IS_ENTITY_ATTACHED_TO_ENTITY(sPedTargetData.tempTarget, sPedState.pedIndex)
				
				MANAGE_PED_ARRIVAL_RESULTS(sPedState.iIndex, FALSE)
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].fRange = cfMAX_PATROL_RANGE
					SET_PED_STATE(sPedState.iIndex, ciTASK_WANDER)
				ENDIF
			ENDIF
		ELSE
			//Peds and locations:
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetSix, ciPed_BSSix_PedArrivalResultsTriggerOnTaskCompletion)
			AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedTasksArrivalResultsBS, sPedState.iIndex)
				VECTOR vTarget
				
				IF IS_ENTITY_A_PED(sPedTargetData.tempTarget)
					vTarget = GET_ENTITY_COORDS(sPedTargetData.tempTarget)
				ELIF MC_serverBD_2.iTargetID[sPedState.iIndex] != -1
					vTarget = GET_LOCATION_VECTOR(MC_serverBD_2.iTargetID[sPedState.iIndex])
				ENDIF
				
				IF NOT IS_VECTOR_ZERO(vTarget)
					FLOAT fTargetDist
					fTargetDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(sPedState.pedIndex, vTarget)
					FLOAT fGotoRange
					fGotoRange = 10
					
					IF sPedState.bIsInAnyVehicle
						IF IS_PED_IN_ANY_PLANE(sPedState.pedIndex)
							fGotoRange = 130
						ELIF IS_PED_IN_ANY_HELI(sPedState.pedIndex)
							fGotoRange = 60
						ENDIF
					ENDIF
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedEscortRange != 0
						fGotoRange = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedEscortRange)
					ENDIF
					
					IF fTargetDist <= FMAX(fGotoRange, 15.0)
						MANAGE_PED_ARRIVAL_RESULTS(sPedState.iIndex, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
ENDPROC 

PROC PROCESS_TASK_GOTO_PLAYER_SERVER(FMMC_PED_STATE &sPedState)
	IF IS_NET_PLAYER_OK(MC_serverBD.TargetPlayerID[sPedState.iIndex])
		IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(MC_serverBD.TargetPlayerID[sPedState.iIndex])
			MC_serverBD.TargetPlayerID[sPedState.iIndex] = NULL
			SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK)
		ELIF MC_serverBD_2.iPedState[sPedState.iIndex] = ciTASK_GOTO_PLAYER
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetSix, ciPed_BSSix_PedArrivalResultsTriggerOnTaskCompletion)
			AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedTasksArrivalResultsBS, sPedState.iIndex)
				
				BOOL bArrived
				PED_INDEX playerPed
				playerPed = GET_PLAYER_PED(MC_serverBD.TargetPlayerID[sPedState.iIndex])
				
				IF NOT IS_PED_IN_ANY_VEHICLE(playerPed)
					
					FLOAT fGotoRange
					fGotoRange = ciPed_ARRIVE_GOTO_RANGE
					
					IF sPedState.bIsInAnyVehicle
						IF IS_PED_IN_ANY_PLANE(sPedState.pedIndex)
							fGotoRange = ciPed_PLANE_ARRIVE_GOTO_RANGE
						ELIF (IS_PED_IN_ANY_BOAT(sPedState.pedIndex) OR IS_PED_IN_ANY_HELI(sPedState.pedIndex))
							fGotoRange = ciPed_BOAT_HELI_ARRIVE_GOTO_RANGE
						ENDIF
					ENDIF
					
					FLOAT fTargetDist
					fTargetDist = GET_DISTANCE_BETWEEN_ENTITIES(sPedState.pedIndex, playerPed)
					
					IF fTargetDist <= fGotoRange
						bArrived = TRUE
					ENDIF
				ELSE
					IF IS_PED_IN_VEHICLE(sPedState.pedIndex, GET_VEHICLE_PED_IS_IN(playerPed))
						bArrived = TRUE
					ENDIF
				ENDIF
				
				IF bArrived
					MANAGE_PED_ARRIVAL_RESULTS(sPedState.iIndex, FALSE)
				ENDIF
				
			ENDIF
		ENDIF
	ELSE
		MC_serverBD.TargetPlayerID[sPedState.iIndex] = NULL
		SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK)
	ENDIF
ENDPROC 

PROC PROCESS_TASK_ATTACK_PLAYER_SERVER(FMMC_PED_STATE &sPedState)
	IF IS_NET_PLAYER_OK(MC_serverBD.TargetPlayerID[sPedState.iIndex])
		IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(MC_serverBD.TargetPlayerID[sPedState.iIndex])
			MC_serverBD.TargetPlayerID[sPedState.iIndex] = NULL
			SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK)
		ELIF MC_serverBD_2.iPedState[sPedState.iIndex] = ciTASK_GOTO_PLAYER
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetSix, ciPed_BSSix_PedArrivalResultsTriggerOnTaskCompletion)
			AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedTasksArrivalResultsBS, sPedState.iIndex)
				
				BOOL bArrived
				PED_INDEX playerPed
				playerPed = GET_PLAYER_PED(MC_serverBD.TargetPlayerID[sPedState.iIndex])
				
				IF NOT IS_PED_IN_ANY_VEHICLE(playerPed)					
					FLOAT fGotoRange
					fGotoRange = ciPed_ARRIVE_GOTO_RANGE
					
					IF sPedState.bIsInAnyVehicle
						IF IS_PED_IN_ANY_PLANE(sPedState.pedIndex)
							fGotoRange = ciPed_PLANE_ARRIVE_GOTO_RANGE
						ELIF (IS_PED_IN_ANY_BOAT(sPedState.pedIndex) OR IS_PED_IN_ANY_HELI(sPedState.pedIndex))
							fGotoRange = ciPed_BOAT_HELI_ARRIVE_GOTO_RANGE
						ENDIF
					ENDIF
					
					FLOAT fTargetDist
					fTargetDist = GET_DISTANCE_BETWEEN_ENTITIES(sPedState.pedIndex, playerPed)
					
					IF fTargetDist <= fGotoRange
						bArrived = TRUE
					ENDIF
				ELSE
					IF IS_PED_IN_VEHICLE(sPedState.pedIndex, GET_VEHICLE_PED_IS_IN(playerPed))
						bArrived = TRUE
					ENDIF
				ENDIF
				
				IF bArrived
					MANAGE_PED_ARRIVAL_RESULTS(sPedState.iIndex, FALSE)
				ENDIF
				
			ENDIF
		ENDIF
	ELSE
		MC_serverBD.TargetPlayerID[sPedState.iIndex] = NULL
		SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK)
	ENDIF
ENDPROC 

PROC PROCESS_TASK_DEFEND_PLAYER_SERVER(FMMC_PED_STATE &sPedState)
	IF IS_NET_PLAYER_OK(MC_serverBD.TargetPlayerID[sPedState.iIndex])
		IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(MC_serverBD.TargetPlayerID[sPedState.iIndex])
			MC_serverBD.TargetPlayerID[sPedState.iIndex] = NULL
			SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK)
		ELIF MC_serverBD_2.iPedState[sPedState.iIndex] = ciTASK_GOTO_PLAYER
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetSix, ciPed_BSSix_PedArrivalResultsTriggerOnTaskCompletion)
			AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedTasksArrivalResultsBS, sPedState.iIndex)
				BOOL bArrived
				PED_INDEX playerPed
				playerPed = GET_PLAYER_PED(MC_serverBD.TargetPlayerID[sPedState.iIndex])
				
				IF NOT IS_PED_IN_ANY_VEHICLE(playerPed)
					FLOAT fGotoRange
					fGotoRange = ciPed_ARRIVE_GOTO_RANGE
					
					IF sPedState.bIsInAnyVehicle
						IF IS_PED_IN_ANY_PLANE(sPedState.pedIndex)
							fGotoRange = ciPed_PLANE_ARRIVE_GOTO_RANGE
						ELIF (IS_PED_IN_ANY_BOAT(sPedState.pedIndex) OR IS_PED_IN_ANY_HELI(sPedState.pedIndex))
							fGotoRange = ciPed_BOAT_HELI_ARRIVE_GOTO_RANGE
						ENDIF
					ENDIF
					
					FLOAT fTargetDist
					fTargetDist = GET_DISTANCE_BETWEEN_ENTITIES(sPedState.pedIndex, playerPed)
					
					IF fTargetDist <= fGotoRange
						bArrived = TRUE
					ENDIF
				ELSE
					IF IS_PED_IN_VEHICLE(sPedState.pedIndex, GET_VEHICLE_PED_IS_IN(playerPed))
						bArrived = TRUE
					ENDIF
				ENDIF
				
				IF bArrived
					MANAGE_PED_ARRIVAL_RESULTS(sPedState.iIndex, FALSE)
				ENDIF
				
			ENDIF
		ENDIF
	ELSE
		MC_serverBD.TargetPlayerID[sPedState.iIndex] = NULL
		SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK)
	ENDIF
ENDPROC 

PROC PROCESS_TASK_FLEE_SERVER(FMMC_PED_STATE &sPedState)
	UNUSED_PARAMETER(sPedState)
ENDPROC 

PROC PROCESS_TASK_AIM_AT_ENTITY_SERVER(FMMC_PED_STATE &sPedState)
	UNUSED_PARAMETER(sPedState)
ENDPROC 

PROC PROCESS_TASK_TAKE_COVER_SERVER(FMMC_PED_STATE &sPedState)
	UNUSED_PARAMETER(sPedState)
ENDPROC 

PROC PROCESS_TASK_FOUND_BODY_RESPONSE_SERVER(FMMC_PED_STATE &sPedState)
	UNUSED_PARAMETER(sPedState)
ENDPROC 

PROC PROCESS_TASK_HUNT_FOR_PLAYER_SERVER(FMMC_PED_STATE &sPedState)
	UNUSED_PARAMETER(sPedState)
ENDPROC 

PROC PROCESS_TASK_ABORT_GOTO_AND_FLEE_SERVER(FMMC_PED_STATE &sPedState)
	FMMC_SET_LONG_BIT(MC_serverBD.iPedTaskStopped, sPedState.iIndex)
	PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_TASK_ABORT_GOTO_AND_FLEE_SERVER - iPed: ", sPedState.iIndex, " Stopping tasks")
	
	IF NOT sPedState.bIsInAnyVehicle
		SET_PED_STATE(sPedState.iIndex, ciTASK_FLEE)
	ENDIF
ENDPROC 

PROC PROCESS_TASK_THROW_PROJECTILES_SERVER(FMMC_PED_STATE &sPedState)
	UNUSED_PARAMETER(sPedState)
ENDPROC 

PROC PROCESS_TASK_SHUFFLE_VEHICLE_SEAT_TO_TURRET_SERVER(FMMC_PED_STATE &sPedState)

	IF sPedState.bInjured
		EXIT
	ENDIF
		
	IF sPedState.bIsInAnyVehicle
		SCRIPTTASKSTATUS TaskStatus 
		TaskStatus = GET_SCRIPT_TASK_STATUS(sPedState.pedIndex, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT)
		
		IF (TaskStatus != WAITING_TO_START_TASK)
		AND (TaskStatus != PERFORMING_TASK)
			
		ELSE	
			SET_PED_RETASK_DIRTY_FLAG(sPedState)
			SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - [ShuffleTurretSeatAlerted] PROCESS_PED_BRAIN - iPed: ", sPedState.iIndex, " Finished task. Choose new Task.")
		ENDIF
	ELSE
		SET_PED_RETASK_DIRTY_FLAG(sPedState)
		SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK)
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - [ShuffleTurretSeatAlerted] PROCESS_PED_BRAIN - iPed: ", sPedState.iIndex, " Not in a vehicle. Choose new task.")
	ENDIF
	
ENDPROC 

PROC PROCESS_TASK_DEFEND_AREA_IN_VEHICLE_SERVER(FMMC_PED_STATE &sPedState)
	IF NOT sPedState.bIsInAnyVehicle
		SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK)
	ENDIF
ENDPROC 

PROC PROCESS_TASK_COWER_SERVER(FMMC_PED_STATE &sPedState)
	// Added for a special case but will implement options in the creator to activate.
	UNUSED_PARAMETER(sPedState)
	// SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK)
ENDPROC

PROC PROCESS_TASK_DRIVE_TRAIN_SERVER(FMMC_PED_STATE &sPedState)
	IF NOT sPedState.bIsOnPlacedTrain
		SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK #IF IS_DEBUG_BUILD , 1 #ENDIF )
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_TASK_COMBAT_TRAIN_SERVER - Ped ", sPedState.iIndex, " Not on a train anymore, go to choose new task.")
		EXIT
	ENDIF 
ENDPROC

PROC PROCESS_TASK_COMBAT_TRAIN_SERVER(FMMC_PED_STATE &sPedState)
	IF NOT sPedState.bIsOnPlacedTrain
		SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK #IF IS_DEBUG_BUILD , 1 #ENDIF )
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_TASK_COMBAT_TRAIN_SERVER - Ped ", sPedState.iIndex, " Not on a train anymore, go to choose new task.")
		EXIT
	ENDIF 
ENDPROC

PROC PROCESS_TASK_COMPANION_SERVER(FMMC_PED_STATE &sPedState)
		
	//Assign and manage the companion indexes.
	INT iFreeCompanionIndex = GET_FREE_PED_COMPANION_INDEX()
	INt iCurrentCompanionIndex = GET_PED_COMPANION_INDEX(sPedState.iIndex)	
	IF iFreeCompanionIndex != -1
	AND iCurrentCompanionIndex = -1
		ASSIGN_PED_COMPANION_INDEX(iFreeCompanionIndex, sPedState.iIndex)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF iFreeCompanionIndex = -1
	AND iCurrentCompanionIndex = -1		
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_NO_MORE_COMPANION_IDS, ENTITY_RUNTIME_ERROR_TYPE_WARNING_PED, "is trying to be set as a Companion but there are no more IDs available.", sPedState.iIndex)			
	ENDIF
	#ENDIF
		
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Main Server Processing Functions  -------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Server processing and decision making for creator placed peds.   									  --------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_PED_DELETION(FMMC_PED_STATE &sPedState)
	INT iPed = sPedState.iIndex
	IF sPedState.bExists
		IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iRespawnOnRule, -1)
			IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedCleanup_NeedOwnershipBS, iPed)						
				IF NOT sPedState.bHasControl
					NETWORK_REQUEST_CONTROL_OF_ENTITY(sPedState.pedIndex)
					PRINTLN("[RCC MISSION] PROCESS_PED_DELETION - Requesting control of ped ", iPed," for cleanup")
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_PED_DELETION - Deleting ped ", iPed)
					DELETE_FMMC_PED(sPedState)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PEDS_RESETTING(FMMC_PED_STATE &sPedState)
	INT iPed = sPedState.iIndex
	
	IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iRespawnOnRule, -1)
		INT iTeam
		FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			IF MC_serverBD_4.iCurrentHighestPriority[iTeam] <= 0	
				RELOOP
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
				RELOOP
			ENDIF
			
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamRuleAttempts > 1
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_RETRY_CURRENT_RULE_TEAM_0 + iTeam)			
				RELOOP
			ENDIF
			
			IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iRespawnOnRule, iTeam, FALSE)
				FMMC_SET_LONG_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS, iPed)
				FMMC_SET_LONG_BIT(MC_serverBD.iPedFirstSpawnBitset, iPed)
				FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedAggroedBitset, iPed)
				FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedSpookedBitset, iPed)
				
				MC_serverBD_2.iPedState[iPed] = ciTASK_CHOOSE_NEW_TASK
				MC_serverBD_2.iOldPedState[iPed] = ciTASK_CHOOSE_NEW_TASK
				IF MC_serverBD_2.iCurrentPedRespawnLives[iPed] < 1
					MC_serverBD_2.iCurrentPedRespawnLives[iPed] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iRespawnOnRuleLives
					PRINTLN("PROCESS_PEDS_RESETTING - Ped ", iPed, " now has ", MC_serverBD_2.iCurrentPedRespawnLives[iPed], " lives")
				ENDIF
				
				PRINTLN( "[JS] Cleaning up ped to respawn on rule change ", iPed )
				BREAKLOOP
			ENDIF			
		ENDFOR
	ENDIF
ENDPROC

PROC PROCESS_PEDS_RESPAWNING(FMMC_PED_STATE &sPedState)
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		EXIT
	ENDIF
	
	INT iPed = sPedState.iIndex
	
	IF MC_serverBD_2.iCurrentPedRespawnLives[iPed] <= 0
	OR sPedState.bExists
		IF MC_serverBD.isearchingforPed = iPed	
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - PROCESS_PEDS_RESPAWNING - Clearing iSearchingForPed: ", MC_serverBD.isearchingforPed)
			MC_serverBD.isearchingforPed = -1
		ENDIF
		
		IF MC_serverBD_2.iCurrentPedRespawnLives[iPed] <= 0
			PROCESS_PEDS_RESETTING(sPedState)
		ENDIF
		
		EXIT
	ENDIF
		
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedCleanup_NeedOwnershipBS, iPed)
		IF NOT SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iRespawnOnRule, -1)
			CASSERTLN(DEBUG_CONTROLLER,"[MCSpawning][Peds][Ped ", iPed, "] - PROCESS_PEDS_RESPAWNING - We were trying to request ownership of for cleanup no longer exists!")
		ENDIF
		PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - PROCESS_PEDS_RESPAWNING - We were trying to request ownership of for cleanup no longer exists!")
		FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS, iPed)
	ENDIF
	
	IF SHOULD_PED_RESPAWN_NOW(sPedState)
		IF HAS_PED_SPAWN_DELAY_EXPIRED(iPed)
			IF RESPAWN_MISSION_PED(sPedState)
			
				FMMC_CLEAR_LONG_BIT(iPedShouldRespawnNowBS, iPed)
				
				MC_serverBD_2.iPedState[iPed] = ciTASK_CHOOSE_NEW_TASK
				
				SET_PED_SHOULD_BE_HIGH_PRIORITY_PED_NEXT_FRAME(sPedState, TRUE)
				
				MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_3.iPedSpawnDelay[iPed])
				
				IF DOES_PED_HAVE_PLAYER_VARIABLE_SETTING(iPed)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedObjective > -1
					AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedTeam > -1
						IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedTeam] < FMMC_MAX_RULES
							MC_serverBD_1.sCreatedCount.iNumPedCreated[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedObjective]++
							PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - PROCESS_PEDS_RESPAWNING - Added to iNumPedCreated, is now: ", MC_serverBD_1.sCreatedCount.iNumPedCreated[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedObjective], " for iRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedObjective)
						ENDIF
					ENDIF
				ENDIF
				IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iRespawnOnRule, -1)
					INT iTeam
					FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
							SET_BIT(MC_serverBD.iPedteamFailBitset[iPed],iTeam)
							BREAKLOOP
						ENDIF
					ENDFOR
				ENDIF	
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwentyTwo, ciPED_BSTwentyTwo_AggroOnSpawn)
					PRINTLN("[MCSpawning][Peds][Ped ", iPed, "][SpookAggro] - PROCESS_PEDS_RESPAWNING - ciPED_BSTwentyTwo_AggroOnSpawn is set. Setting iPedAggroedBitset!")
					FMMC_SET_LONG_BIT(MC_serverBD.iPedAggroedBitset, iPed)
				ENDIF
				
				IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedFirstSpawnBitset, iPed)
					
					IF SHOULD_PED_RESET_ANIMS_AFTER_RESPAWNING(sPedState)						
						PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - PROCESS_PEDS_RESPAWNING - Resetting Anims instantly on Respawn.")
						FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedTaskActive, sPedState.iIndex)
						FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedTaskStopped, sPedState.iIndex)
						FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedSecondaryTaskActive, sPedState.iIndex)
						FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedSecondaryTaskCompleted, sPedState.iIndex)
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, sPedState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, -1)						
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, sPedState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_INIT))						
					ENDIF
						
					IF SHOULD_PED_START_TASK_AFTER_RESPAWNING(sPedState)	
						PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - PROCESS_PEDS_RESPAWNING - Triggering Tasks instantly on Respawn.")												
						FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedTaskActive, sPedState.iIndex)
						FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedTaskStopped, sPedState.iIndex)
						FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedSecondaryTaskActive, sPedState.iIndex)
						FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedSecondaryTaskCompleted, sPedState.iIndex)
						TRIGGER_PED_TASK_ACTIVATION(sPedState)
					ELIF NOT SHOULD_PED_RESET_ANIMS_AFTER_RESPAWNING(sPedState)
						PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - Putting ped into combat immediately with ciTASK_GENERAL_COMBAT, this isn't the ped's first time spawning (iPedFirstSpawnBitset is already cleared)")
						SET_PED_HIGHLY_PERCEPTIVE(sPedState.pedIndex,TRUE)
						FMMC_SET_LONG_BIT(MC_serverBD.iPedTaskStopped, iPed)
						SET_PED_STATE(sPedState.iIndex, ciTASK_GENERAL_COMBAT)						
					ENDIF						
				ELSE
					FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedFirstSpawnBitset, iPed)
					PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - RESPAWN_MISSION_PED - This is the peds first time spawning, clear iPedFirstSpawnBitset")
				ENDIF
				
				MC_serverBD_2.iCurrentPedRespawnLives[iPed]--
				IF GET_ENTITY_STARTING_RESPAWN_LIVES(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedRespawnLives) = UNLIMITED_RESPAWNS
				AND MC_serverBD_2.iCurrentPedRespawnLives[iPed] < 1
					MC_serverBD_2.iCurrentPedRespawnLives[iPed] = UNLIMITED_RESPAWNS
				ENDIF
				
				PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - PROCESS_PEDS_RESPAWNING - has finished respawning. Lives Left: ", MC_serverBD_2.iCurrentPedRespawnLives[iPed])
				
			ENDIF
		ELSE
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "]  - PROCESS_PEDS_RESPAWNING - Waiting for the DELAY...")
		ENDIF
	ELSE
		IF MC_serverBD.isearchingforPed = iPed
			PRINTLN("[MCSpawning][Peds][Ped ", iPed, "] - Clearing iSearchingForPed: ", MC_serverBD.isearchingforPed)
			MC_serverBD.isearchingforPed = -1
		ENDIF
		
		IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME()
		AND (NOT CAN_PED_STILL_SPAWN_IN_MISSION(iPed))
			
			MC_serverBD.iPedteamFailBitset[iPed] = 0
			
			INT iTeam
			
			FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
				MC_serverBD_4.iPedPriority[iPed][iTeam] = FMMC_PRIORITY_IGNORE
				MC_serverBD_4.iPedRule[iPed][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
				FMMC_CLEAR_LONG_BIT(MC_serverBD.iMissionCriticalPed[iTeam], iPed)
			ENDFOR
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL HAS_PED_PASSED_LEAVE_CONDITIONS_FOR_RULE(FMMC_PED_STATE &sPedState, INT iTeam)
	IF MC_serverBD_4.iPedRule[sPedState.iIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY	
	AND GET_MC_CLIENT_MISSION_STAGE(iLocalPart) = CLIENT_MISSION_STAGE_LEAVE_PED
		INT iNumberOfPlayersLeft = GET_PLAYERS_LEFT_LEAVE_OBJECTIVE_ENTITY(iTeam, sPedState.iIndex)
		INT iNumberOfPlayersRequiredToLeave = GET_NUMBER_OF_PLAYERS_REQUIRED_TO_LEAVE_ENTITY(iTeam)
		
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - iNumberOfPlayersLeft: ", iNumberOfPlayersLeft, " iNumberOfPlayersRequiredToLeave: ", iNumberOfPlayersRequiredToLeave)
		
		IF iNumberOfPlayersLeft >= iNumberOfPlayersRequiredToLeave
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PROCESS_PED_GOTO_LOCATION_OBJECTIVE(FMMC_PED_STATE &sPedState)
	
	INT iTeam = 0
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			
		IF HAS_TEAM_FAILED(iTeam)
		OR MC_serverBD.iNumPedHighestPriority[iTeam] <= 0
		OR MC_serverBD_4.iPedPriority[sPedState.iIndex][iTeam] != MC_serverBD_4.iCurrentHighestPriority[iTeam]
		OR MC_serverBD_4.iPedRule[sPedState.iIndex][iTeam] != FMMC_OBJECTIVE_LOGIC_PED_GOTO_LOCATION
			RELOOP
		ENDIF
		
		IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(iRuleStartedTimeStamp)		
		OR NOT MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(iRuleStartedTimeStamp, ciRULE_STARTED__PED_GOTO_OBJECTIVE_GRACE_PERIOD)			
			RELOOP
		ENDIF
		
		#IF IS_DEBUG_BUILD
		BOOL bNoObjectiveCompletionGotosSet = TRUE
		#ENDIF
			
		BOOL bReloop
		INT i = 0
		FOR i = 0 TO GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, FALSE) - 1
			
			#IF IS_DEBUG_BUILD
			IF bNoObjectiveCompletionGotosSet
			AND IS_ASSOCIATED_GOTO_COMPLETE_CURRENT_GOTO_OBJECTIVE_ENABLED(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], i, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData.sPointData, MC_serverBD_4.iCurrentHighestPriority[iTeam])
				bNoObjectiveCompletionGotosSet = FALSE
			ENDIF
			#ENDIF
			
			IF NOT sPedState.bInjured
			AND IS_ASSOCIATED_GOTO_COMPLETE_CURRENT_GOTO_OBJECTIVE_ENABLED(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], i, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData.sPointData, MC_serverBD_4.iCurrentHighestPriority[iTeam])
			AND NOT IS_BIT_SET(MC_ServerBD_4.iPedCurrentPriorityGotoLocCompletedBS[sPedState.iIndex], i)
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_GOTO_LOCATION_OBJECTIVE - Waiting for Goto Location: ", i, " to be completed.")
				bReloop = TRUE
				BREAKLOOP
				
			ENDIF
			
		ENDFOR
		
		#IF IS_DEBUG_BUILD
		IF bNoObjectiveCompletionGotosSet
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_PED_GO_TO_LOCATION_INCORRECT_SET_UP, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_PED, "Does not have any valid rules set up to objective complete! Will Skip Ped Objective!", sPedState.iIndex)
		ENDIF
		#ENDIF
		
		IF bReloop
			RELOOP
		ENDIF
				
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_GOTO_LOCATION_OBJECTIVE - Not waiting for any more locates. Passing objective.")
			
		SET_TEAM_OBJECTIVE_OUTCOME(iTeam, MC_serverBD_4.iPedPriority[sPedState.iIndex][iTeam], !sPedState.bInjured)
	
		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED, sPedState.iIndex, iTeam, !sPedState.bInjured)
		
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_ARRIVED
		
		SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, MC_serverBD_4.iPedPriority[sPedState.iIndex][iTeam], MC_serverBD.iReasonForObjEnd[iTeam], !sPedState.bInjured)
		
		BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, MC_serverBD_4.iPedPriority[sPedState.iIndex][iTeam], DID_TEAM_PASS_OBJECTIVE(iTeam, MC_serverBD_4.iPedPriority[sPedState.iIndex][iTeam]))	
	ENDFOR
	
ENDPROC

PROC PROCESS_PED_DAMAGE_ENTITY_OBJECTIVE(FMMC_PED_STATE &sPedState)
	INT iTeam = 0
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			
		IF HAS_TEAM_FAILED(iTeam)
		OR MC_serverBD.iNumPedHighestPriority[iTeam] <= 0
		OR MC_serverBD_4.iPedPriority[sPedState.iIndex][iTeam] > MC_serverBD_4.iCurrentHighestPriority[iTeam]
		OR MC_serverBD_4.iPedRule[sPedState.iIndex][iTeam] != FMMC_OBJECTIVE_LOGIC_DAMAGE
			RELOOP
		ENDIF
		
		IF sPedState.bExists
			FMMC_SET_LONG_BIT(MC_serverBD.iPedExistsForDamageRule, sPedState.iIndex)
		ENDIF
		
		IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedExistsForDamageRule, sPedState.iIndex)		
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_DAMAGE_ENTITY_OBJECTIVE - Waiting for ped to spawn and exist.")
			RELOOP
		ENDIF		
		
		FLOAT fCurrentPercent					
		
		IF sPedState.bExists
		AND NOT sPedState.bInjured
			fCurrentPercent = (TO_FLOAT(GET_ENTITY_HEALTH(sPedState.pedIndex)-100) / TO_FLOAT(GET_ENTITY_MAX_HEALTH(sPedState.pedIndex)-100)) * 100
		ELSE			
			fCurrentPercent = 0
		ENDIF
		
		INT iHealthRequiredForRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iHealthDamageRequiredForRule
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPriorityEntityHealthThresholdOverride[MC_serverBD_4.iCurrentHighestPriority[iTeam]] != 0
			iHealthRequiredForRule = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPriorityEntityHealthThresholdOverride[MC_serverBD_4.iCurrentHighestPriority[iTeam]]
		ENDIF
			
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_DAMAGE_ENTITY_OBJECTIVE - fCurrentPercent: ", fCurrentPercent, " thresh-hold to pass objective: ", iHealthRequiredForRule)
				
		IF fCurrentPercent > iHealthRequiredForRule
			RELOOP
		ENDIF
	
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_DAMAGE_ENTITY_OBJECTIVE - Below the thresh-hold to pass objective")
		
		SET_TEAM_OBJECTIVE_OUTCOME(iTeam, MC_serverBD_4.iPedPriority[sPedState.iIndex][iTeam], !sPedState.bInjured)
	
		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED, sPedState.iIndex, iTeam, !sPedState.bInjured)
		
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_DAMAGED
		
		SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, MC_serverBD_4.iPedPriority[sPedState.iIndex][iTeam], MC_serverBD.iReasonForObjEnd[iTeam], !sPedState.bInjured)
		
		BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, MC_serverBD_4.iPedPriority[sPedState.iIndex][iTeam], DID_TEAM_PASS_OBJECTIVE(iTeam, MC_serverBD_4.iPedPriority[sPedState.iIndex][iTeam]))		
	ENDFOR
ENDPROC

/// PURPOSE: Handles Ped State and other server logic for the mission peds
PROC PROCESS_PED_BRAIN(FMMC_PED_STATE &sPedState)
	
	INT iPed = sPedState.iIndex	
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(iPedsServerProcessedThisFrame, iPed)
		EXIT
	ENDIF
	
	FMMC_SET_LONG_BIT(iPedsServerProcessedThisFrame, iPed)
	
	INT iTeam,iPart
	BOOL bcheckteamfail
	
	IF sPedState.bExists
		IF sPedState.bInjured
			
			//This is a backup, it should already have been handled during the damage event
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFive, ciPed_BSFive_AggroWhenHurtOrKilled)
			AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedBitset, iPed)
			AND NOT FMMC_IS_LONG_BIT_SET(iPedHitByStunGun, iPed) 
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - SPOOKED because found dead and has ciPed_BSFive_AggroWhenHurtOrKilled set (damage event was probably missed)")
				
				#IF IS_DEBUG_BUILD 
				SET_PED_IS_SPOOKED_DEBUG(iPed)
				#ENDIF
				FMMC_SET_LONG_BIT(iPedLocalSpookedBitset, iPed) // broadcast
				FMMC_SET_LONG_BIT(MC_serverBD.iPedSpookedBitset, iPed)
				BROADCAST_FMMC_PED_SPOOKED(iPed)
				
				FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
					IF SHOULD_AGRO_FAIL_FOR_TEAM(iPed, iTeam)
						FMMC_SET_LONG_BIT(iPedLocalAggroedBitset, iPed)
						TRIGGER_AGGRO_ON_PED(sPedState)
						iTeam = MC_serverBD.iNumberOfTeams // Break out, we found an aggro team!
					ENDIF
				ENDFOR
				
				FMMC_SET_LONG_BIT(iPedLocalReceivedEvent_HurtOrKilled, iPed)
			ENDIF
			
			IF MC_serverBD_2.iPartPedFollows[iPed] != -1
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - getting MC_serverBD_2.iPartPedFollows[iPed] reset to -1 as ped is injured")
			ENDIF
			
			PROCESS_CLEAN_UP_COMPANION_SERVER(iPed)
			
			MC_serverBD_2.iPartPedFollows[iPed] = -1
			MC_serverBD_2.iOldPartPedFollows[iPed] = -1
			MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[iPed])
			
			BOOL bCleanupOnDeath = SHOULD_PED_CLEANUP_ON_DEATH(sPedState)
			
			IF bCleanupOnDeath					
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFifteen, ciPED_BSFifteen_DeletePedOnDeathImmediately)
					IF NOT sPedState.bHasControl
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
						FMMC_SET_LONG_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS, iPed)
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER][CLEANUP ON DEATH] - PROCESS_PED_BRAIN - Deleting dead ped but we do not have control of the entity. Requesting Control.")
					ELSE
						FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS, iPed)
						DELETE_FMMC_PED(sPedState)
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER][CLEANUP ON DEATH] - PROCESS_PED_BRAIN - Deleting ded ped immediately due to ciPED_BSFifteen_DeletePedOnDeathImmediately flag being set.")
					ENDIF
				ELSE
					FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS, iPed)
					CLEANUP_NET_ID(sPedState.niIndex)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER][CLEANUP ON DEATH] - PROCESS_PED_BRAIN - Dead ped marked for cleanup.")
				ENDIF
				
				IF MC_serverBD_2.iCurrentPedRespawnLives[iPed] <= 0
				AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_1.sCreatedCount.iSkipPedBitset, iPed)
					PRINTLN( "[Peds][Ped ", sPedState.iIndex, "][SERVER][CLEANUP ON DEATH] - PROCESS_PED_BRAIN - bcheckteamfail = TRUE - ped dead & cleanup on death")
					bcheckteamfail = TRUE
				ELSE
					//PRINTLN( "[RCC MISSION] PROCESS_PED_BRAIN iPed: ", iPed, " - bcheckteamfail = FALSE - MC_serverBD_2.iCurrentPedRespawnLives[iPed] = ", MC_serverBD_2.iCurrentPedRespawnLives[iPed], ", MC_serverBD_1.sCreatedCount.iSkipPedBitset = ", FMMC_IS_LONG_BIT_SET(MC_serverBD_1.sCreatedCount.iSkipPedBitset, iPed))
				ENDIF
			ELSE
				FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPriority[iTeam] > -1
					AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPriority[iTeam] < FMMC_MAX_RULES
						IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPriority[iTeam])
						AND FMMC_IS_LONG_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[iTeam], iPed)
							SET_BIT(MC_serverBD.iObjectiveMidPointBitset[iTeam],g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPriority[iTeam])
							SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
							PRINTLN( "[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - ped dead photo rule so setting midpoint iObjectiveMidPointBitset for team ",iTeam,", rule ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPriority[iTeam])
						ENDIF
					ENDIF
				ENDFOR
				
				PRINTLN( "[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - bcheckteamfail = TRUE - ped dead & DONT cleanup on death")
				bcheckteamfail = TRUE
			ENDIF
			
			//Aggro countdown (grace period) clearing:
			IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAggroedBitset, iPed)
				FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedAggroedBitset, iPed)
				
				FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
					//If it's not too late already:
					IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_WILL_FAIL_FOR_TEAM_0 + iTeam)
						IF HAS_AGGRO_INDEX_TIMER_STARTED_FROM_BITSET(iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAggroIndexBS_Entity_Ped)
						AND SHOULD_AGRO_FAIL_FOR_TEAM(iPed,iTeam)
							
							BOOL bResetTimer = TRUE
							
							//is there anyone else this team cares about?
							IF (MC_serverBD.iPedAggroedBitset[0] > 0)
							OR (MC_serverBD.iPedAggroedBitset[1] > 0)
								
								INT iPedLoop
								FOR iPedLoop = 0 TO (MC_serverBD.iNumPedCreated - 1)
									IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAggroedBitset, iPedLoop)
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedLoop].iPedBitsetThree,ciPed_BSThree_AggroFailHasDelay)
										AND SHOULD_AGRO_FAIL_FOR_TEAM(iPedLoop,iTeam)
											PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - Checking for leftover aggro countdown peds - found ped ",iPedLoop," for team ",iTeam)
											bResetTimer = FALSE
											iPedLoop = MC_serverBD.iNumPedCreated //Break out into a special value which gets caught below
										ENDIF
									ENDIF
								ENDFOR
							ENDIF
							
							IF bResetTimer
								//Therefore we have nobody left who needs a timer!
								PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - No more aggro countdown peds found for team ",iTeam,", clearing timer...")
								MC_serverBD.iPartCausingFail[iTeam] = -1
								MC_serverBD.iEntityCausingFail[iTeam] = -1
								STOP_AGGRO_INDEX_TIMER_FROM_BITSET(iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAggroIndexBS_Entity_Ped)
								MC_serverBD.iAggroCountdownTimerSlack[iTeam] = 0
							ELSE
								PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - More aggro countdown peds found for team ",iTeam,", adding a bit of slack")
								MC_serverBD.iAggroCountdownTimerSlack[iTeam] += 500
							ENDIF
							
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
			

		ELSE // Else if this ped isn't injured
			IF SHOULD_CLEANUP_PED(sPedState)
			AND CLEANUP_PED_EARLY(sPedState)
				EXIT
			ENDIF
			
			IF SHOULD_CLEANUP_DUE_TO_PLAYER_PROXIMITY_ARRAYED(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sProximitySpawningData, iPed, MC_serverBD_3.iProximitySpawning_PedCleanupBS)
				IF CLEANUP_PED_EARLY(sPedState, FALSE, TRUE)
					PRINTLN("[PROXSPAWN_SERVER][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - Cleaning up ped due to proximity!")
					MC_serverBD_2.iCurrentPedRespawnLives[iPed]++
					EXIT
				ENDIF
			ENDIF
		ENDIF
	ELSE		
		IF MC_serverBD_2.iCurrentPedRespawnLives[iPed] <= 0
		AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_1.sCreatedCount.iSkipPedBitset, iPed)
			bcheckteamfail = TRUE
			PRINTLN( "[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - Ped bcheckteamfail = TRUE - ped doen't exist")
		ENDIF
	ENDIF
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitset, ciPed_BS_DontFleeDropOffs)
			//	PRINTLN("[RCC MISSION] FMMC_MAX_RULES near drop off for ped: ",iPed)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iTeam[iTeam] = ciPed_RELATION_SHIP_DISLIKE
				OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iTeam[iTeam] = ciPed_RELATION_SHIP_HATE
			//		PRINTLN("[RCC MISSION] Rel group near drop off for ped: ",iPed)
					IF NOT IS_VECTOR_ZERO(GET_DROP_OFF_CENTER_FOR_TEAM(DEFAULT,iTeam))
				//		PRINTLN("[RCC MISSION] Drop off ok near drop off for ped: ",iPed)
						IF sPedState.bExists
						AND NOT sPedState.bInjured
					//		PRINTLN("[RCC MISSION] Not injured near drop off for ped: ",iPed)
							VECTOR vDropOff = GET_DROP_OFF_CENTER_FOR_TEAM(DEFAULT, iTeam)
							FLOAT fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(sPedState.pedIndex, vDropOff)
							BOOL bInaHeliOrPlane = IS_PED_IN_ANY_HELI(sPedState.pedIndex) OR IS_PED_IN_ANY_PLANE(sPedState.pedIndex) 
							
							IF ( bInaHeliOrPlane AND fDist < 350)
							OR ( NOT bInaHeliOrPlane  AND fDist < 150)
						//		PRINTLN("[RCC MISSION] Dist ok near drop off for ped: ",iPed)
								IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedNearDropOff[iTeam], iPed)
									PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - iPedNearDropOff not set near drop off for ped: ",iPed, " fDist = ", fDist, " vDropOff = ", vDropOff, " bInaHeliOrPlane = ", bInaHeliOrPlane)
									//If there isn't some other target I can focus on (only for helicopters/planes), flee!
									
									NETWORK_INDEX niOldTarget = MC_serverBD.niTargetID[iPed]
									
									IF ( IS_PED_IN_ANY_HELI(sPedState.pedIndex) OR IS_PED_IN_ANY_PLANE(sPedState.pedIndex) )
									AND DOES_PED_HAVE_VALID_TARGET(sPedState, TRUE)
										PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - in a heli near drop off for ped")
										IF niOldTarget != MC_serverBD.niTargetID[iPed]
											PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - niOldTarget != MC_serverBD.niTargetID[iPed] near drop off for ped")
											//New target, retask the ped:
											SET_PED_RETASK_DIRTY_FLAG(sPedState)
										ENDIF
									ELSE		// url:bugstar:2214442 - Heli's now take off when a player is within activation range. ST 29/01/15.
										IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitset, ciPed_BS_DontFleeDropOffs)
											FMMC_SET_LONG_BIT(MC_serverBD.iPedNearDropOff[iTeam], iPed)									
											FMMC_SET_LONG_BIT(MC_serverBD.iPedShouldFleeDropOff, iPed)
											PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - Setting near drop off for ped")
										ENDIF
									ENDIF
								ENDIF
							ELSE
								//PRINTLN("[RCC MISSION] Dist NOT ok near drop off for ped: ",iPed, " fDist =  ", fDist)
								#IF IS_DEBUG_BUILD
								IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedNearDropOff[iTeam], iPed)
									PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - Clearing near drop off for ped")
								ENDIF
								#ENDIF
								FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedNearDropOff[iTeam], iPed)
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedNearDropOff[iTeam], iPed)
								PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - Clearing near drop off for ped")
							ENDIF
							#ENDIF
							FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedNearDropOff[iTeam], iPed)
							FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedShouldFleeDropOff, iPed)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// Ensure RUN_TEAM_PED_FAIL_CHECKS is called below when the ped has been "left" on a leave entity rule.
			IF sPedState.bExists
				IF HAS_PED_PASSED_LEAVE_CONDITIONS_FOR_RULE(sPedState, iTeam)				
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - No player is near Ped: ", sPedState.iIndex, " bcheckteamfail = TRUE")
					bcheckteamfail = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF bcheckteamfail
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitset, ciPed_BS_Ped_Arrived_On_Death)
				IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAtGotoLocBitset, iPed)
					MANAGE_PED_ARRIVAL_RESULTS(iPed, TRUE)
				ENDIF
			ENDIF
			
			IF (MC_serverBD_4.iPedPriority[iPed][iTeam] < FMMC_MAX_RULES
			OR FMMC_IS_LONG_BIT_SET(MC_serverBD.iMissionCriticalPed[iTeam], iPed))
			AND NOT HAS_TEAM_FAILED(iTeam)
				PRINTLN( "[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - bcheckteamfail is TRUE, Running RUN_TEAM_PED_FAIL_CHECKS, PedCritical: ", BOOL_TO_STRING(FMMC_IS_LONG_BIT_SET(MC_serverBD.iMissionCriticalPed[iTeam], iPed)))
				RUN_TEAM_PED_FAIL_CHECKS(iPed,iTeam,MC_serverBD_4.iPedPriority[iPed][iTeam])
			ELSE
				PRINTLN( "[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - bcheckteamfail is TRUE, skipping fail checks - ",
				" PedCritical: ", BOOL_TO_STRING(FMMC_IS_LONG_BIT_SET(MC_serverBD.iMissionCriticalPed[iTeam], iPed)), 
				", MC_serverBD_4.iPedPriority[iPed][iTeam]: ", MC_serverBD_4.iPedPriority[iPed][iTeam],
				", HAS_TEAM_FAILED(iTeam) = ", BOOL_TO_STRING(HAS_TEAM_FAILED(iTeam)))
			ENDIF
			
		ENDIF
		
		INT iRuleToSkipTo = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedSkipToRuleWhenAggro[iTeam]		
		
		IF sPedState.bExists
			
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - Ped Exists - iRuleToSkipTo: ", iRuleToSkipTo)
			
			IF iRuleToSkipTo > -1 AND iRuleToSkipTo > MC_serverBD_4.iCurrentHighestPriority[iTeam] AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
							
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - iPedAggroedBitset" , FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAggroedBitset, iPed))
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - iObjectivePedAggroedBitset", IS_BIT_SET(MC_serverBD.iObjectivePedAggroedBitset[iTeam],MC_serverBD_4.iCurrentHighestPriority[iTeam]))
				
				IF (IS_BIT_SET(MC_serverBD.iObjectivePedAggroedBitset[iTeam],MC_serverBD_4.iCurrentHighestPriority[iTeam])
				OR FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAggroedBitset, iPed))
				AND IS_PED_RULE_SKIP_VALID_ON_THIS_RULE(iPed, iTeam)
				AND NOT SHOULD_PED_SUPPRESS_NON_SCRIPT_TRIGGERED_AGGRO_EVENTS(sPedState)
				
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - In here... bSkipNow = TRUE")
					
					BOOL bSkipNow = TRUE
				
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThree,ciPed_BSThree_AggroFailHasDelay)
						IF HAS_NET_TIMER_STARTED(MC_ServerBD_4.tdPedRuleSkipTimer)
							IF NOT HAS_NET_TIMER_EXPIRED(MC_ServerBD_4.tdPedRuleSkipTimer, ciPed_AGGRO_RULE_SKIP_DELAY)
								PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - In here... FALSE 1 ")
								bSkipNow = FALSE
							ENDIF
						ELSE
							REINIT_NET_TIMER(MC_ServerBD_4.tdPedRuleSkipTimer)
							PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - starting aggro skip timer for Ped ")
							bSkipNow = FALSE
						ENDIF
					ENDIF
					
					IF bSkipNow							
						INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iTeam, iRuleToSkipTo - 1, FALSE, FALSE, FALSE)
						SET_IGNORE_CHECKPOINT_PROCESSING_FOR_RULE_SKIP(iTeam)
						
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - Ped Is aggro'd so we have skipped to rule ", MC_serverBD_4.iCurrentHighestPriority[iTeam], " for team ", iTeam)
						RESET_NET_TIMER(MC_ServerBD_4.tdPedRuleSkipTimer)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Aggro countdown (grace period) fail logic:
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_WILL_FAIL_FOR_TEAM_0 + iTeam)
		AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThree, ciPed_BSThree_AggroFailHasDelay)
			AND FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAggroedBitset, iPed)
			AND NOT SHOULD_PED_SUPPRESS_NON_SCRIPT_TRIGGERED_AGGRO_EVENTS(sPedState)
				IF NOT sPedState.bInjured
				AND HAS_AGGRO_INDEX_TIMER_STARTED_FROM_BITSET(iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAggroIndexBS_Entity_Ped)
				AND SHOULD_AGRO_FAIL_FOR_TEAM(iPed,iTeam)
					IF (GET_FIRST_AGGRO_INDEX_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME_FROM_BITSET(iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAggroIndexBS_Entity_Ped) > 3500 + MC_serverBD.iAggroCountdownTimerSlack[iTeam]) //3.5 seconds
						
						SET_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET(iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAggroIndexBS_Entity_Ped)
						
						IF iTeam > -1 AND iTeam < FMMC_MAX_TEAMS						
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE13_DISABLE_CCTV_CAMERAS_TRIGGERING)			
							AND NOT FMMC_IS_LONG_BIT_SET(iPedHitByStunGun, iPed)
							AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSeventeen, ciPed_BSSeventeen_DisableTriggeringCCTVOnSameAggroIndexes)
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAggroIndexBS_Entity_Ped = ciMISSION_AGGRO_INDEX_DEFAULT
									PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro][SERVER] - PROCESS_PED_BRAIN - Ped Setting SBBOOL7_CCTV_TEAM_AGGROED_PEDS")
									SET_BIT(MC_serverBD.iServerBitSet7, SBBOOL7_CCTV_TEAM_AGGROED_PEDS)
								ELSE
									PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro][SERVER] - PROCESS_PED_BRAIN - Ped Calling SET_TEAM_AS_TRIGGERED_CCTV_FROM_PEDS_AGGRO_BITSET with their aggro BS.")
									SET_TEAM_AS_TRIGGERED_CCTV_FROM_PEDS_AGGRO_BITSET(iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAggroIndexBS_Entity_Ped)
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThree, ciPed_BSThree_AggroFailHasDelay)
							IF NOT sPedState.bInjured
								PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro][SERVER] - PROCESS_PED_BRAIN - Ped is setting iObjectivePedAggroedBitset with ruke: ", MC_serverBD_4.iCurrentHighestPriority[iTeam])
								SET_BIT(MC_serverBD.iObjectivePedAggroedBitset[iTeam], MC_serverBD_4.iCurrentHighestPriority[iTeam])
							ENDIF
						ENDIF
						
						//Alert all other aggro peds if creator setting tells us to:
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFive, ciPed_BSFive_AggroAlertsAllOtherAggroPeds)
							PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro][SERVER] - PROCESS_PED_BRAIN - Ped aggro timer run out, now alert all other aggro peds!")
							ALERT_ALL_AGGRO_PEDS(iTeam)
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEleven, ciPed_BSEleven_FailDelayForWanted)
							IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
								PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro][SERVER] - PROCESS_PED_BRAIN - Ped aggro timer run out, set iObjectivePedAggroedBitset for team: ", iTeam," rule: ", MC_serverBD_4.iCurrentHighestPriority[iTeam])
								SET_BIT(MC_serverBD.iObjectivePedAggroedBitset[iTeam], MC_serverBD_4.iCurrentHighestPriority[iTeam])
							ENDIF
						ENDIF
						
						IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwo, ciPed_BSTwo_AggroDoesntFail)
							
							IF MC_serverBD.iPartCausingFail[iTeam] = -1
								MC_serverBD.iPartCausingFail[iTeam] = GET_PART_PED_IS_IN_COMBAT_WITH(sPedState.pedIndex)
								PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro][SERVER] - PROCESS_PED_BRAIN - GET_PART_PED_IS_IN_COMBAT_WITH - Ped is in combat with part ", MC_serverBD.iPartCausingFail[iTeam])
							
								IF MC_serverBD.iPartCausingFail[iTeam] = -1
									MC_serverBD.iPartCausingFail[iTeam] =  GET_PART_CLOSEST_TO_ALERTED_PED(sPedState.pedIndex)
									PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro][SERVER] - PROCESS_PED_BRAIN - GET_PART_CLOSEST_TO_ALERTED_PED - Getting part ", MC_serverBD.iPartCausingFail[iTeam], " as a backup")
								ENDIF
							ENDIF
							
							MC_serverBD.iEntityCausingFail[iTeam] = iPed
							
							IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedByDeadBodyBitset, iPed)
								MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_AGRO
							ELSE
								MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_FOUND_BODY
								SET_BIT(MC_serverBD.iServerBitSet2,SBBOOL2_AGGRO_FAIL_DUE_TO_BODY_FOUND_TEAM_0 + iTeam)
							ENDIF
							
							SET_TEAM_OBJECTIVE_OUTCOME(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam],FALSE)
							SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam], MC_serverBD.iReasonForObjEnd[iTeam], TRUE)
							BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam],iTeam, mc_serverBD_4.iCurrentHighestPriority[iTeam], DID_TEAM_PASS_OBJECTIVE(iTeam, mc_serverBD_4.iCurrentHighestPriority[iTeam]), TRUE)
							
							REINIT_AGGRO_INDEX_TIMER_FROM_BITSET(iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAggroIndexBS_Entity_Ped)
							SET_BIT(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_WILL_FAIL_FOR_TEAM_0 + iTeam)
							
							PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro][SERVER] - PROCESS_PED_BRAIN - MISSION IS GOING TO FAIL FOR TEAM:",iTeam," BECAUSE PED AGGRO COUNTDOWN RAN OUT: ",iPed)
						ELSE
							STOP_AGGRO_INDEX_TIMER_FROM_BITSET(iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAggroIndexBS_Entity_Ped)
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF MC_serverBD_4.iPedPriority[iPed][iTeam] < FMMC_MAX_RULES
			
			#IF IS_DEBUG_BUILD
			IF bDoPedCountPrints
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - iNumHighPriorityPed - iPed = ", iPed, ", iTeam = ", iTeam, " ped priority = ", MC_serverBD_4.iPedPriority[iPed][iTeam])
			ENDIF
			#ENDIF
			
			IF NOT bcheckteamfail OR (bcheckteamfail AND FMMC_IS_LONG_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[iTeam], iPed))
				
				#IF IS_DEBUG_BUILD
				IF bDoPedCountPrints
				BOOL bDeadPhotoBitSet = FMMC_IS_LONG_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[iTeam], iPed)
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - iNumHighPriorityPed - iPed = ", iPed, ", iTeam = ", iTeam, " bcheckteamfail = ", bcheckteamfail, ", iDeadPedPhotoBitset = ", bDeadPhotoBitSet)
				ENDIF
				#ENDIF
				
				IF MC_serverBD_4.iPedPriority[iPed][iTeam] <= iCurrentHighPriority[iTeam]
					
					#IF IS_DEBUG_BUILD
					IF bDoPedCountPrints
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - iNumHighPriorityPed - iPed = ", iPed, ", iTeam = ", iTeam, " iCurrentHighPriority = ", iCurrentHighPriority[iTeam])
					ENDIF
					#ENDIF
					
					IF MC_serverBD_4.iPedRule[iPed][iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
						
						#IF IS_DEBUG_BUILD
						IF bDoPedCountPrints
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - iNumHighPriorityPed - iPed = ", iPed, ", iTeam = ", iTeam, " iPedRule = ", MC_serverBD_4.iPedRule[iPed][iTeam])
						ENDIF
						#ENDIF
						
						IF MC_serverBD_4.iPedRule[iPed][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
						
							IF MC_serverBD_4.iPedPriority[iPed][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam]
								IF NOT sPedState.bInjured
									IF IS_ENTITY_IN_DROP_OFF_AREA(sPedState.pedIndex, iTeam, MC_serverBD_4.iPedPriority[iPed][iTeam], ciRULE_TYPE_PED, iPed)
										IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAtYourHolding[iTeam], iPed)
											FMMC_SET_LONG_BIT(MC_serverBD.iPedAtYourHolding[iTeam], iPed)
											IF MC_serverBD_2.iPartPedFollows[iPed] > -1
												IF MC_PlayerBD[MC_serverBD_2.iPartPedFollows[iPed]].iTeam = iTeam
													SET_BIT(MC_serverBD.iPedHoldPartPoints,MC_serverBD_2.iPartPedFollows[iPed])
													REQUEST_INCREMENT_SERVER_REMOTE_PLAYER_SCORE(MC_serverBD_2.iPartPedFollows[iPed], GET_FMMC_POINTS_FOR_TEAM(iTeam,MC_serverBD_4.iPedPriority[iPed][iTeam]), iTeam, MC_serverBD_4.iPedPriority[iPed][iTeam])														
												ENDIF
											ENDIF
											PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - Setting at your holding (MC_serverBD.iPedAtYourHolding) for team ",iTeam,", ped ",iPed," as in drop off")
										ENDIF
									ELSE
										IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAtYourHolding[iTeam], iPed)
											FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedAtYourHolding[iTeam], iPed)
											PRINTLN("[PLAYER_LOOP] - PROCESS_PED_BRAIN 2")
											FOR ipart = 0 TO (NUM_NETWORK_PLAYERS-1)
												IF IS_BIT_SET(MC_serverBD.iPedHoldPartPoints,ipart)
													REQUEST_INCREMENT_SERVER_REMOTE_PLAYER_SCORE(ipart, (-1 * GET_FMMC_POINTS_FOR_TEAM(iTeam,MC_serverBD_4.iPedPriority[iPed][iTeam])), iTeam, MC_serverBD_4.iPedPriority[iPed][iTeam])														
													CLEAR_BIT(MC_serverBD.iPedHoldPartPoints,ipart)
												ENDIF
											ENDFOR
											PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - Clearing at your holding (MC_serverBD.iPedAtYourHolding) for team ",iTeam,", ped ",iPed," as no longer in drop off")
										ENDIF
									ENDIF
								ELSE
									IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAtYourHolding[iTeam], iPed)
										INCREMENT_SERVER_TEAM_SCORE(iTeam,MC_serverBD_4.iPedPriority[iPed][iTeam], -GET_FMMC_POINTS_FOR_TEAM(iTeam,MC_serverBD_4.iPedPriority[iPed][iTeam]))
										FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedAtYourHolding[iTeam], iPed)
										PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - Clearing at your holding (MC_serverBD.iPedAtYourHolding) for team ",iTeam,", ped ",iPed," as ped is injured")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						IF bDoPedCountPrints
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - iNumHighPriorityPed* - iPed = ", iPed, ", iTeam = ", iTeam, " iPedRule = ", MC_serverBD_4.iPedRule[iPed][iTeam], " = FMMC_OBJECTIVE_LOGIC_NONE")
						ENDIF
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					IF bDoPedCountPrints
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - iNumHighPriorityPed* - iPed = ", iPed, ", iTeam = ", iTeam, " iCurrentHighPriority = ", iCurrentHighPriority[iTeam], " > high priority.")
					ENDIF
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				IF bDoPedCountPrints
				BOOL bDeadPhotoBitSet = FMMC_IS_LONG_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[iTeam], iPed)
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - iNumHighPriorityPed* - iPed = ", iPed, ", iTeam = ", iTeam, " bcheckteamfail = ", bcheckteamfail, ", iDeadPedPhotoBitset = ", bDeadPhotoBitSet)
				ENDIF
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF bDoPedCountPrints
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - iNumHighPriorityPed* - iPed = ", iPed, ", iTeam = ", iTeam, " ped priority = ", MC_serverBD_4.iPedPriority[iPed][iTeam])
			ENDIF
			#ENDIF
		ENDIF
		
		IF sPedState.bExists
			IF MC_serverBD_4.iPedPriority[iPed][iTeam] < FMMC_MAX_RULES
			AND MC_serverBD_4.iPedPriority[iPed][iTeam] > -1
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[MC_serverBD_4.iPedPriority[iPed][iTeam]], ciBS_RULE10_PASS_RULE_THROUGH_TAGGING)
					IF IS_ENTITY_ALREADY_TAGGED(ENUM_TO_INT(ET_PED),iPed)
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - PED HAS BEEN TAGGED. Calling PROGRESS_SPECIFIC_OBJECTIVE")
						INVALIDATE_SINGLE_OBJECTIVE(iTeam, MC_ServerBD_4.iPedPriority[iPed][iTeam], TRUE)
						SET_IGNORE_CHECKPOINT_PROCESSING_FOR_RULE_SKIP(iTeam)
					ENDIF
				ENDIF
				
				IF MC_serverBD_4.iPedRule[iPed][iTeam] = FMMC_OBJECTIVE_LOGIC_PROTECT
				AND MC_serverBD_4.iPedPriority[iPed][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam]
				AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[MC_serverBD_4.iPedPriority[iPed][iTeam]], ciBS_RULE10_PROGRESS_PROTECT_WHEN_AIMING_AT_PED)
					INT i, iPartsChecked = 0
					
					FOR i = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
						IF MC_playerBD[i].iTeam = iTeam
							IF ((NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_HEIST_SPECTATOR))
							AND (NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL))
							AND IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
							AND (NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_FINISHED))
								IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
									
									iPartsChecked++
									
									IF IS_BIT_SET(MC_PlayerBD[i].iClientBitSet4, PBBOOL4_PLAYER_IS_AIMING_AT_PROTECT_PED)
										PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - PED is being aimed at by part ", i, " on team ",iTeam, ", calling PROGRESS_SPECIFIC_OBJECTIVE")
										PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED, iPed, iTeam, TRUE)
										BREAKLOOP
									ENDIF
									
									IF iPartsChecked > MC_serverBD.iNumberOfPlayingPlayers[iTeam]
										BREAKLOOP
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
		
	ENDFOR
	
	MANAGE_SERVER_GROUPING_DISTANCE_AND_CONTROL_DATA(sPedState)

	IF sPedState.bExists
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedSpeedRange != -1
		OR SHOULD_PED_SPEED_UP_FROM_BEING_ALERTED(iPed, sPedState.pedIndex)
			IF NOT IS_BIT_SET(MC_serverBD_4.iPedSpeedOverride[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - Ped's speed is to be overridden if a player is within ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedSpeedRange, " ciPed_BSNine_Speed_Override_On_Spooked_Alerted): ", IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetNine, ciPed_BSNine_Speed_Override_On_Spooked_Alerted))
				IF CHECK_PLAYERS_IN_RANGE_OF_PED(iPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedSpeedRange)
				OR SHOULD_PED_SPEED_UP_FROM_BEING_ALERTED(iPed, sPedState.pedIndex)
					SET_BIT(MC_serverBD_4.iPedSpeedOverride[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetNine, ciPed_BSNine_SpeedOverrideAutoDeactivate)
				IF NOT CHECK_PLAYERS_IN_RANGE_OF_PED(iPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedSpeedRange)
				AND NOT SHOULD_PED_SPEED_UP_FROM_BEING_ALERTED(iPed, sPedState.pedIndex)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - Ped's speed override deactivated (out of range) - ciPed_BSNine_Speed_Override_On_Spooked_Alerted): ", IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetNine, ciPed_BSNine_Speed_Override_On_Spooked_Alerted))
					CLEAR_BIT(MC_serverBD_4.iPedSpeedOverride[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
	OR FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedInvolvedInMiniGameBitSet, iPed)
		EXIT
	ENDIF
	
	IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedTaskActive, iPed)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwo, ciPed_BSTwo_TriggerTasksOnAggro)
			MANAGE_PED_TASK_ACTIVATION(sPedState)
		ENDIF
	ELSE
		IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedTaskStopped, iPed)
	
			IF NOT SHOULD_PED_STOP_TASKS(iPed)
				IF NOT IS_PRIMARY_CUSTOM_TARGET_ENTITY_IN_CORRECT_STATE_FOR_TASK(iPed, sPedState.pedIndex, TRUE)
					
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - dropping out of primary task")
					
					FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedTaskActive, iPed)
					MC_ServerBD_2.iAssociatedAction[iPed] = ciACTION_NOTHING
					MC_serverBD.niTargetID[iPed] = NULL
					MC_serverBD_2.iTargetID[iPed] = -1
					
					IF MC_serverBD_2.iPedState[iPed] != ciTASK_CHOOSE_NEW_TASK
						IF NOT IS_BIT_SET(sPedState.iBitset, FMMC_PED_STATE_IN_COMBAT)
						AND CAN_PED_TASKS_BE_CLEARED(sPedState.pedIndex)
							CLEAR_PED_TASKS(sPedState.pedIndex)
						ENDIF
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - clearing ped task, dropped out of primary task")
						SET_PED_STATE(iPed,ciTASK_CHOOSE_NEW_TASK)
					ENDIF
					
				ELSE
					IF NOT (MC_serverBD_2.iPedState[iPed] = ciTASK_CHOOSE_NEW_TASK OR MC_serverBD_2.iPedState[iPed] = ciTASK_GOTO_COORDS)
					AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAtGotoLocBitset, iPed)
						//Shouldn't drop out of these states:
						IF NOT (MC_serverBD_2.iPedState[iPed] = ciTASK_HUNT_FOR_PLAYER OR MC_serverBD_2.iPedState[iPed] = ciTASK_FOUND_BODY_RESPONSE)
							//Ped hasn't finished their Go To yet, but isn't running it - check if they need to get back on that:
							PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - hasn't yet finished GoTo, checking to see if they should resume it")
							PROCESS_PED_TASK_NOTHING_BRAIN(sPedState)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				FMMC_SET_LONG_BIT(MC_serverBD.iPedTaskStopped, iPed)
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - Task Stopped reached for ped")
				
				IF MC_serverBD_2.iPedState[iPed] = ciTASK_GOTO_COORDS
				AND SHOULD_PED_DO_DEFENSIVE_AREA_GOTO(sPedState.pedIndex, iPed)
					FMMC_SET_LONG_BIT(MC_serverBD.iPedDoneDefensiveAreaGoto, iPed)
				ENDIF
				
				MC_ServerBD_2.iAssociatedAction[iPed] = ciACTION_NOTHING
				MC_serverBD.niTargetID[iPed] = NULL
				MC_serverBD_2.iTargetID[iPed] = -1
				FMMC_SET_LONG_BIT(MC_serverBD.iPedAtGotoLocBitset, iPed)
				
				IF MC_serverBD_2.iPedState[iPed] != ciTASK_CHOOSE_NEW_TASK
					IF NOT IS_BIT_SET(sPedState.iBitset, FMMC_PED_STATE_IN_COMBAT)
					AND CAN_PED_TASKS_BE_CLEARED(sPedState.pedIndex)
						CLEAR_PED_TASKS(sPedState.pedIndex)
					ENDIF
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - clearing ped task, Task Stopped trigger hit")
					SET_PED_STATE(iPed,ciTASK_CHOOSE_NEW_TASK)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSecondaryTaskCompleted, iPed)
		IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSecondaryTaskActive, iPed)
			MANAGE_PED_SECONDARY_TASK_ACTIVATION(iPed, sPedState.pedIndex)
		ELSE
			IF (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSix, ciPed_BSSix_DeactivateSecondaryTaskWhenOutOfTriggerDistance) AND (NOT IS_PED_WITHIN_RANGE_OF_SECONDARY_CUSTOM_TARGET_ENTITY(sPedState.pedIndex, iPed, TRUE)))
			OR HAS_PLAYER_GOT_CONTROL_OF_SECONDARY_CUSTOM_TARGET_ENTITY(iPed) // Checks the bit for this setting being on before doing anything silly
				
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - dropping out of secondary task")
				
				FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedSecondaryTaskActive, iPed)
				MC_ServerBD_2.iAssociatedAction[iPed] = ciACTION_NOTHING
				MC_serverBD.niTargetID[iPed] = NULL
				MC_serverBD_2.iTargetID[iPed] = -1
				
				IF MC_serverBD_2.iPedState[iPed] != ciTASK_CHOOSE_NEW_TASK
					IF NOT IS_BIT_SET(sPedState.iBitset, FMMC_PED_STATE_IN_COMBAT)
					AND CAN_PED_TASKS_BE_CLEARED(sPedState.pedIndex)
						CLEAR_PED_TASKS(sPedState.pedIndex)
					ENDIF
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - clearing ped task, dropped out of secondary task")
					SET_PED_STATE(iPed,ciTASK_CHOOSE_NEW_TASK)
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_serverBD_2.iOldPedState[iPed] != MC_serverBD_2.iPedState[iPed]
		
		IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedDelivered, iPed)
		AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedInvolvedInMiniGameBitSet, iPed)
			IF 	NOT IS_BIT_SET(sPedState.iBitset, FMMC_PED_STATE_IN_COMBAT)
			AND CAN_PED_TASKS_BE_CLEARED(sPedState.pedIndex)
			AND (NOT FMMC_IS_LONG_BIT_SET(iPedHitByStunGun, iPed) AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sPedState.pedIndex, WEAPONTYPE_STUNGUN))
				SET_PED_RETASK_DIRTY_FLAG(sPedState)
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - clearing ped task new state")
			ENDIF
		ENDIF
		MC_serverBD_2.iOldPedState[iPed] = MC_serverBD_2.iPedState[iPed]
	ENDIF
	
	IF MC_serverBD_2.iPedState[iPed] != ciTASK_CHOOSE_NEW_TASK
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSix, ciPed_BSSix_AllowTaskingWhileGrouped)
		AND MC_serverBD_2.iPartPedFollows[iPed] != -1
			IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedDelivered, iPed)
				IF CAN_PED_TASKS_BE_CLEARED(sPedState.pedIndex)
					SET_PED_RETASK_DIRTY_FLAG(sPedState)
				ENDIF
			ENDIF
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - clearing ped task, following")
			SET_PED_STATE(iPed,ciTASK_CHOOSE_NEW_TASK)
		ENDIF
	ENDIF
	
	FMMC_PED_TARGET_DATA sPedTargetData
	GET_PED_TARGET_DATA(sPedState, sPedTargetData)
	
	FMMC_PED_VEHICLE_DATA sPedVehicleData
	GET_PED_CURRENT_VEHICLE_DATA(sPedState, sPedVehicleData)
		
	IF MC_serverBD.niTargetID[iPed] != NULL
		IF sPedTargetData.bExists
			IF IS_ENTITY_DEAD(sPedTargetData.tempTarget)
				IF CAN_PED_TASKS_BE_CLEARED(sPedState.pedIndex)
					SET_PED_RETASK_DIRTY_FLAG(sPedState)
				ENDIF
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - clearing ped task, target ent dead") 
				SET_PED_STATE(iPed, ciTASK_CHOOSE_NEW_TASK)
				MC_serverBD.niTargetID[iPed] = NULL
			ENDIF
		ELSE
			IF CAN_PED_TASKS_BE_CLEARED(sPedState.pedIndex)
				SET_PED_RETASK_DIRTY_FLAG(sPedState)
			ENDIF
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - clearing ped task, target ent not exist") 
			SET_PED_STATE(iPed, ciTASK_CHOOSE_NEW_TASK)
			MC_serverBD.niTargetID[iPed] = NULL
		ENDIF
	ENDIF
	
	PROCESS_FORCE_SWITCH_TO_COMPANION_STATE(sPedState)
	
	PROCESS_IN_VEHICLE_BEHAVIOUR_SERVER(sPedState)	
	
	RUN_SERVER_PED_BACKUP_SPOOK_CANCEL_TASKS_LOGIC(sPedState)
	
	PROCESS_PED_ZONE_BEHAVIOUR(sPedState)
	
#IF IS_DEBUG_BUILD
	TEXT_LABEL_15 pedStateName = GET_PED_STATE_NAME(MC_serverBD_2.iPedState[iPed])
	PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SERVER] - PROCESS_PED_BRAIN - (SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(sPedState.pedIndex), " State: ", pedStateName)
#ENDIF

	SWITCH MC_serverBD_2.iPedState[iPed]
				
		CASE ciTASK_CHOOSE_NEW_TASK			
			PROCESS_TASK_CHOOSE_NEW_TASK_SERVER(sPedState)			
		BREAK

		CASE ciTASK_GOTO_COORDS			
			PROCESS_TASK_GOTO_COORDS_SERVER(sPedState, sPedVehicleData)
		BREAK
		
		CASE ciTASK_SHUFFLE_VEHICLE_SEAT_TO_TURRET			
			PROCESS_TASK_SHUFFLE_VEHICLE_SEAT_TO_TURRET_SERVER(sPedState)			
		BREAK
		
		CASE ciTASK_ATTACK_ENTITY			
			PROCESS_TASK_ATTACK_ENTITY_SERVER(sPedState)			
		BREAK
		
		CASE ciTASK_GOTO_ENTITY			
			PROCESS_TASK_GOTO_ENTITY_SERVER(sPedState, sPedTargetData)				
		BREAK
		
		CASE ciTASK_GOTO_PLAYER
			PROCESS_TASK_GOTO_PLAYER_SERVER(sPedState)
		BREAK
		
		CASE ciTASK_DEFEND_PLAYER
			PROCESS_TASK_DEFEND_PLAYER_SERVER(sPedState)
		BREAK
		
		CASE ciTASK_ATTACK_PLAYER
			PROCESS_TASK_ATTACK_PLAYER_SERVER(sPedState)
		BREAK
				
		CASE ciTASK_GENERAL_COMBAT
			PROCESS_TASK_GENERAL_COMBAT_SERVER(sPedState)
		BREAK
		
		CASE ciTASK_DEFEND_AREA	
			PROCESS_TASK_DEFEND_AREA_SERVER(sPedState)
		BREAK
		
		CASE ciTASK_DEFEND_AREA_IN_VEHICLE
			PROCESS_TASK_DEFEND_AREA_IN_VEHICLE_SERVER(sPedState)
		BREAK
		
		CASE ciTASK_ABORT_GOTO_AND_FLEE
			PROCESS_TASK_ABORT_GOTO_AND_FLEE_SERVER(sPedState)
		BREAK
		
		CASE ciTASK_WANDER
			PROCESS_TASK_WANDER_SERVER(sPedState)
		BREAK
		
		CASE ciTASK_DEFEND_ENTITY
			PROCESS_TASK_DEFEND_ENTITY_SERVER(sPedState)
		BREAK
		
		CASE ciTASK_FLEE
			PROCESS_TASK_FLEE_SERVER(sPedState)
		BREAK
		
		CASE ciTASK_AIM_AT_ENTITY
			PROCESS_TASK_AIM_AT_ENTITY_SERVER(sPedState)
		BREAK
		
		CASE ciTASK_TAKE_COVER
			PROCESS_TASK_TAKE_COVER_SERVER(sPedState)
		BREAK
		
		CASE ciTASK_FOUND_BODY_RESPONSE
			PROCESS_TASK_FOUND_BODY_RESPONSE_SERVER(sPedState)
		BREAK
		
		CASE ciTASK_HUNT_FOR_PLAYER
			PROCESS_TASK_HUNT_FOR_PLAYER_SERVER(sPedState)
		BREAK
		
		CASE ciTASK_THROW_PROJECTILES
			PROCESS_TASK_THROW_PROJECTILES_SERVER(sPedState)
		BREAK
		
		CASE ciTASK_COWER
			PROCESS_TASK_COWER_SERVER(sPedState)
		BREAK
				
		CASE ciTASK_DRIVE_TRAIN	
			PROCESS_TASK_DRIVE_TRAIN_SERVER(sPedState)
		BREAK
		
		CASE ciTASK_COMBAT_TRAIN	
			PROCESS_TASK_COMBAT_TRAIN_SERVER(sPedState)
		BREAK
		
		CASE ciTASK_COMPANION
			PROCESS_TASK_COMPANION_SERVER(sPedState)
		BREAK
	ENDSWITCH
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Ped Every Frame Processing
// ##### Description: Server Ped Every Frame Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_PED_EVERY_FRAME_SERVER(INT iPed)
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	FMMC_PED_STATE sPedState
	FILL_FMMC_PED_STATE_STRUCT(sPedState, iPed)
	
	// Pre-Logic
	PROCESS_PED_DELETION(sPedState)
	
	PROCESS_PEDS_RESPAWNING(sPedState)
	
	PROCESS_ADDING_HIGH_PRIORITY_PEDS(sPedState)
		
	PROCESS_PED_SHOULD_MOVE_ON_TO_SPECIFIC_GOTO_PROGRESS_INDEX_SERVER(sPedState)
	
	PROCESS_PED_GOTO_LOCATION_OBJECTIVE(sPedState)
	
	PROCESS_PED_DAMAGE_ENTITY_OBJECTIVE(sPedState)
	
	// Regular Processing.
	IF IS_PED_FLAGGED_AS_HIGH_PRIORITY_PED(sPedState)
		PROCESS_PED_BRAIN(sPedState)
	ENDIF
		
	PROCESS_SERVER_PED_CAPTURING(sPedState)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Ped Staggered Processing
// ##### Description: Server Ped Staggered Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_PED_STAGGERED_SERVER(INT iPed)
	FMMC_PED_STATE sPedState
	FILL_FMMC_PED_STATE_STRUCT(sPedState, iPed)
	PROCESS_PED_BRAIN(sPedState)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Ped Objective Processing
// ##### Description: Server Ped Objective Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_PED_IN_PREREQ_ZONES(INT iPed)
	
	IF iRuleEntityPrereqZoneCount <= 0
		EXIT
	ENDIF
	
	FMMC_PED_STATE sPedState //Should be passed through same as objects but its a bit change
	FILL_FMMC_PED_STATE_STRUCT(sPedState, iPed)
	
	IF NOT sPedState.bExists
		EXIT
	ENDIF
	
	IF sPedState.bInjured
		EXIT
	ENDIF
	
	INT i 
	FOR i = 0 TO iRuleEntityPrereqZoneCount - 1
		IF iZoneIndicesOfTypeRuleEntityPrereq[i] = -1
			RELOOP
		ENDIF
		IF NOT DOES_ZONE_EXIST(iZoneIndicesOfTypeRuleEntityPrereq[i])
			RELOOP
		ENDIF
		
		IF ARE_COORDS_IN_FMMC_ZONE(GET_FMMC_PED_COORDS(sPedState), iZoneIndicesOfTypeRuleEntityPrereq[i])
#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(iTempEntitiesInZoneBS, iZoneIndicesOfTypeRuleEntityPrereq[i])
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "] PROCESS_PED_IN_PREREQ_ZONES - Ped is within zone: ", iZoneIndicesOfTypeRuleEntityPrereq[i])
			ENDIF
#ENDIF
			SET_BIT(iTempEntitiesInZoneBS, iZoneIndicesOfTypeRuleEntityPrereq[i])
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneIndicesOfTypeRuleEntityPrereq[i]].iZoneBS2, ciFMMC_ZONEBS2_CHECK_SAME_INTERIOR)
			IF GET_FMMC_PED_INTERIOR(sPedState) != NULL
			AND GET_INTERIOR_AT_COORDS(GET_ZONE_RUNTIME_CENTRE(iZoneIndicesOfTypeRuleEntityPrereq[i])) = GET_FMMC_PED_INTERIOR(sPedState)
#IF IS_DEBUG_BUILD
				IF NOT IS_BIT_SET(iTempEntitiesInZoneBS, iZoneIndicesOfTypeRuleEntityPrereq[i])
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "] PROCESS_PED_IN_PREREQ_ZONES - Ped is in the same interior as zone: ", iZoneIndicesOfTypeRuleEntityPrereq[i])
				ENDIF
#ENDIF
				SET_BIT(iTempEntitiesInZoneBS, iZoneIndicesOfTypeRuleEntityPrereq[i])
			ENDIF
		ENDIF
	ENDFOR

ENDPROC	

PROC PROCESS_PED_OBJECTIVES_SERVER(INT iPed, INT iTeam)

	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	BOOL bInjured = IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
	
	IF MC_serverBD_2.iCurrentPedRespawnLives[iPed] <= 0
	AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[iTeam], iPed)	
	
		IF bInjured
			EXIT
		ENDIF
		
	ENDIF

	IF MC_serverBD_4.iPedRule[iPed][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iPedPriority[iPed][iTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iPedPriority[iPed][iTeam] > iCurrentHighPriority[iTeam]
		//Has an objective but the team has not reached it yet
		EXIT
	ENDIF
	
	PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_PED_OBJECTIVES_SERVER - Ped: ", iPed, " Team: ", iTeam, " Objective is valid - Assigning. Priority: ",
				MC_serverBD_4.iPedPriority[iPed][iTeam], " Rule: ", MC_serverBD_4.iPedRule[iPed][iTeam])
	
	IF MC_serverBD_4.iPedPriority[iPed][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam]
		iTempNumPriorityRespawnPed[iTeam] += MC_serverBD_2.iCurrentPedRespawnLives[iPed]
		PROCESS_PED_IN_PREREQ_ZONES(iPed)
	ENDIF
						
	iCurrentHighPriority[iTeam] = MC_serverBD_4.iPedPriority[iPed][iTeam]
	iTempPedMissionLogic[iTeam] = MC_serverBD_4.iPedRule[iPed][iTeam]
	
	IF iOldHighPriority[iTeam] > iCurrentHighPriority[iTeam]
		iNumHighPriorityPed[iTeam] = 0
		iNumHighPriorityDeadPed[iTeam] = 0
		iOldHighPriority[iTeam] = iCurrentHighPriority[iTeam]
	ENDIF

	iNumHighPriorityPed[iTeam]++
	
	IF bInjured
	AND FMMC_IS_LONG_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[iTeam], iPed)
		iNumHighPriorityDeadPed[iTeam]++
	ENDIF
						
	RESET_TEMP_OBJECTIVE_VARS(iTeam, ciRULE_TYPE_PED)
	
ENDPROC
