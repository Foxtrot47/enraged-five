// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - HUD Processing ----------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### Processes displaying various HUD elements
// ##### Lower right HUD, GPS & Hint cam, shards etc.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020 -------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_ObjectiveText_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Misc -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### Filters, vehicle/weapon HUDs etc.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC MANAGE_CAM_PHONE_LEGEND()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iCurrentRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF (iCurrentRule < FMMC_MAX_RULES)
		IF iTeam != -1
			IF iTeam < FMMC_MAX_TEAMS
				IF g_FMMC_STRUCT.bRemoveUnwantedCamPhoneOptions = FALSE
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iCurrentRule], ciBS_RULE4_STREAMLINE_CAM_HELP)
						g_FMMC_STRUCT.bRemoveUnwantedCamPhoneOptions = TRUE
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iCurrentRule], ciBS_RULE4_STREAMLINE_CAM_HELP)
						g_FMMC_STRUCT.bRemoveUnwantedCamPhoneOptions = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_MANUAL_HUD_TOGGLE()
	IF CAN_MANUALLY_TOGGLE_HUD()
		CONTROL_ACTION caToggleButton = INPUT_CONTEXT
				
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, caToggleButton)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, caToggleButton)
			IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_MANUALLY_HIDDEN_HUD)
				CLEAR_BIT(iLocalBoolCheck28, LBOOL28_MANUALLY_HIDDEN_HUD)
			ELSE
				SET_BIT(iLocalBoolCheck28, LBOOL28_MANUALLY_HIDDEN_HUD)
			ENDIF
			
			PRINTLN("PROCESS_MANUAL_HUD_TOGGLE - Swapping LBOOL28_MANUALLY_HIDDEN_HUD")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HIDE_PLAYER_RADAR()
	
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	
	IF iTeam < -1
	OR iTeam >= FMMC_MAX_TEAMS
		EXIT
	ENDIF
	
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
		
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSixteen[iRule], ciBS_RULE16_HIDE_PLAYER_RADAR)
		EXIT
	ENDIF
	
	HIDE_RADAR_THIS_FRAME()
ENDPROC

PROC MAINTAIN_THE_CURRENT_HUD()

	IF SHOULD_HIDE_THE_HUD_THIS_FRAME()
		IF bIsAnySpectator
			PRINTLN("[RCC MISSION] [MMacK][EOM] END OF MISSION HUD CLEARUP IN PROGRESS - spectator")
			HIDE_SPECTATOR_BUTTONS(TRUE)
			HIDE_SPECTATOR_HUD(TRUE)
		ENDIF
		
		DISABLE_HUD()
		PRINTLN("[RCC MISSION] Clearing HUD this frame" )
		
	ELSE
		IF bIsAnySpectator AND NOT IS_CUTSCENE_PLAYING() 
			HIDE_SPECTATOR_BUTTONS(FALSE)
			HIDE_SPECTATOR_HUD(FALSE)
		ENDIF
	ENDIF
	
	PROCESS_MANUAL_HUD_TOGGLE()
	
	IF NOT bIsAnySpectator
		IF IS_PLAYER_DEAD(LocalPlayer)
			CLEAR_INVENTORY_BOX_CONTENT()
			DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
		ENDIF
	ENDIF
	
	PROCESS_HIDE_PLAYER_RADAR()
	
	PROCESS_TEAMS_HUD()
ENDPROC

PROC DRAW_VIP_MARKER(VECTOR vDrawPosition)

	DestinationBlip = ADD_BLIP_FOR_COORD(vDrawPosition)
	SET_BLIP_SPRITE(DestinationBlip, GET_WAYPOINT_BLIP_ENUM_ID())
	SET_BLIP_SCALE(DestinationBlip, GB_BLIP_SIZE)
	SET_BLIP_PRIORITY(DestinationBlip, BLIPPRIORITY_HIGHEST)
	SET_BLIP_NAME_FROM_TEXT_FILE(DestinationBlip, "VIP_BLIP")
	SET_BLIP_COLOUR(DestinationBlip, BLIP_COLOUR_PURPLE)
	SET_BLIP_ROUTE(DestinationBlip, TRUE)
	PRINTLN( " [RCC MISSION] - DRAW_VIP_MARKER - BLIP ADDED -  vDrawPosition ", vDrawPosition)

ENDPROC

PROC MANAGE_VIP_MARKER_DRAWING()
	VECTOR vDrawPosition = MC_serverBD_3.vVIPMarker
	
	//Check if it needs updating
	IF DOES_BLIP_EXIST(DestinationBlip)
		IF NOT ARE_VECTORS_EQUAL(MC_serverBD_3.vVIPMarker, GET_BLIP_COORDS(DestinationBlip))
			REMOVE_BLIP(DestinationBlip)
			PRINTLN( " [RCC MISSION] - MANAGE_VIP_MARKER_DRAWING - NEEDS UPDATING  ")
		ENDIF
		
		IF ARE_VECTORS_EQUAL(MC_serverBD_3.vVIPMarker, vNULLMARKER)
			REMOVE_BLIP(DestinationBlip)
			PRINTLN( " [RCC MISSION] - MANAGE_VIP_MARKER_DRAWING - VIP REMOVED MARKER  ")
		ENDIF
		
		IF bLocalPlayerPedOK
			IF IS_ENTITY_AT_COORD(LocalPlayerPed,vDrawPosition,DEFAULT_LOCATES_SIZE_VECTOR())
				REMOVE_BLIP(DestinationBlip)
				PRINTLN( " [RCC MISSION] - MANAGE_VIP_MARKER_DRAWING - REMOVING BLIP PLAYER AT COORD ")
			ENDIF
		ENDIF
		
	ENDIF
	
	//Add Blip and Checkpoint
	IF NOT DOES_BLIP_EXIST(DestinationBlip)
		IF NOT ARE_VECTORS_EQUAL(vDrawPosition, <<0, 0, 0>>)
		AND NOT ARE_VECTORS_EQUAL(vDrawPosition, <<0, 0, -2000>>)
		AND NOT ARE_VECTORS_EQUAL(vDrawPosition, vNULLMARKER)
			
			IF bLocalPlayerPedOK
				IF NOT IS_ENTITY_AT_COORD(LocalPlayerPed,vDrawPosition,DEFAULT_LOCATES_SIZE_VECTOR())
					DRAW_VIP_MARKER(vDrawPosition)
				ENDIF
			ELSE
				DRAW_VIP_MARKER(vDrawPosition)
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

//For use with Entourage so the VIP can share their blip
PROC BROADCAST_VIP_MARKER_FROM_MENU()
	VECTOR vWaypoint
	IF IS_WAYPOINT_ACTIVE()
		vWaypoint = GET_BLIP_INFO_ID_COORD(GET_FIRST_BLIP_INFO_ID(GET_WAYPOINT_BLIP_ENUM_ID()))
		PRINTLN("[RCC MISSION] GET_VIP_MARKER_FROM_MENU  Waypoint active = ", vWaypoint)
	ELSE
		IF bSentDrawMarkerRequest
			BROADCAST_VIP_MARKER_POSITION(vNULLMARKER)
			vLastStoredWaypoint = vNULLMARKER
			bSentDrawMarkerRequest = FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vWaypoint)
		//only update if the position has changed
		IF NOT ARE_VECTORS_EQUAL(vWaypoint,vLastStoredWaypoint)
			vWaypoint.z = GET_APPROX_HEIGHT_FOR_AREA(vWaypoint.x-5, vWaypoint.y-5, vWaypoint.x+5, vWaypoint.y+5)
			BROADCAST_VIP_MARKER_POSITION(vWaypoint)
			vLastStoredWaypoint = vWaypoint
			PRINTLN("[RCC MISSION] GET_VIP_MARKER_FROM_MENU  vLastStoredWaypoint = ", vLastStoredWaypoint)
			bSentDrawMarkerRequest = TRUE
		ENDIF
	ENDIF
	
ENDPROC

//for use only with entourage - expand for further functionality
PROC SHARE_MARKER_WITH_SPECIFIED_TEAM()
	
	//iAssassinTeam = 0 
	INT iBodyguardTeam = 1
	INT iVIPTeam  = 2
	
	
	//Only do this if the creator setting is on 
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iBodyguardTeam].iTeamBitset, ciBS_TEAM_MARKER_VISIBILITY_T2)

		//Get VIP team
		//Get Bodyguard Team
		//Listen for when he sets a marker - all the time
		//Broadcast data to all players - if it changes
		//Add blip for appropriate teams - if it changes

		//If player is the VIP 
		IF MC_playerBD[iPartToUse].iteam = iVIPTeam
			BROADCAST_VIP_MARKER_FROM_MENU()
		ENDIF
		
		//If player is on the bodyguard team
		IF MC_playerBD[iPartToUse].iteam = iBodyguardTeam
			MANAGE_VIP_MARKER_DRAWING()
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL HAS_LOCAL_PLAYER_PRESSED_INPUT_FOR_FILTER()
	RETURN (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT) OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT))
			AND NOT IS_PAUSE_MENU_ACTIVE()
			AND NOT IS_PHONE_ONSCREEN()
			AND NOT bIsAnySpectator
			AND NOT IS_CUSTOM_MENU_ON_SCREEN()
ENDFUNC

PROC PROCESS_ACCELERATED_TRANSITION_OUT_OF_FILTER_REQUIREMENTS(INT iAcceleratedCleanupTrigger)
	
	SWITCH iAcceleratedCleanupTrigger
		CASE FMMC_FILTER_ACCEL_GROUNDED
			IF IS_ENTITY_IN_AIR(PlayerPedToUse)
				SET_BIT(iLocalBoolCheck30, LBOOL30_OK_TO_START_ACCELERATED_FILTER_TRANSITION_OUT)
				PRINTLN("[FILTERS][ACCEL] PROCESS_ACCELERATED_TRANSITION_OUT_OF_FILTER_REQUIREMENTS - On ground requirement met, player has been off the ground.")
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL SHOULD_FILTER_USE_ACCELERATED_TRANSITION_OUT(INT iAcceleratedCleanupTrigger)
	
	PROCESS_ACCELERATED_TRANSITION_OUT_OF_FILTER_REQUIREMENTS(iAcceleratedCleanupTrigger)
	
	SWITCH iAcceleratedCleanupTrigger
		CASE FMMC_FILTER_ACCEL_GROUNDED
			IF NOT IS_ENTITY_IN_AIR(PlayerPedToUse)			
				IF IS_BIT_SET(iLocalBoolCheck30, LBOOL30_OK_TO_START_ACCELERATED_FILTER_TRANSITION_OUT)
					PRINTLN("[FILTERS][ACCEL] SHOULD_FILTER_USE_ACCELERATED_TRANSITION_OUT - Not in the air anymore")
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL PROCESS_ACCELERATED_TRANSITION_OUT_OF_FILTER(INT iCleanupTime)
			
	IF NOT HAS_NET_TIMER_STARTED(tdFilterAcceleratedFadeOutTimer)
		REINIT_NET_TIMER(tdFilterAcceleratedFadeOutTimer)
		RESET_NET_TIMER(tdFilterFadeTimer)
		fFilterStrengthCached = fFilterStrength
		PRINTLN("[FILTERS][ACCEL] PROCESS_TRANSITION_OUT_OF_FILTER - Starting timer for fast cleanup and stopping old timer")
		PRINTLN("[FILTERS][ACCEL] PROCESS_TRANSITION_OUT_OF_FILTER - fFilterStrengthCached recached to: ", fFilterStrengthCached)
		RETURN FALSE
	ENDIF
	 
	INT iDuration = ROUND(TO_FLOAT(iCleanupTime) * fFilterStrengthCached) * 1000
	IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdFilterAcceleratedFadeOutTimer, iDuration)
		PRINTLN("[FILTERS][ACCEL] PROCESS_TRANSITION_OUT_OF_FILTER - Creator Fade out: ", iCleanupTime, " fFilterStrengthCached: ", fFilterStrengthCached)
		PRINTLN("[FILTERS][ACCEL] PROCESS_TRANSITION_OUT_OF_FILTER - iDuration: ", iDuration, " Calc: ", TO_FLOAT(iCleanupTime) * fFilterStrengthCached)
		FLOAT fTimeToRemove = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdFilterAcceleratedFadeOutTimer)) / iDuration
		fTimeToRemove *= fFilterStrengthCached
		fTimeToRemove = CLAMP(fTimeToRemove, -0.1, fFilterStrengthCached)
		PRINTLN("[FILTERS][ACCEL] PROCESS_TRANSITION_OUT_OF_FILTER - fTimeToRemove: ", fTimeToRemove)
		
		FLOAT fStrengthDecrease = fFilterStrengthCached - fTimeToRemove
		
		fFilterStrength = fStrengthDecrease
		PRINTLN("[FILTERS][ACCEL] PROCESS_TRANSITION_OUT_OF_FILTER - fFilterStrength: ", fFilterStrength, " fStrengthDecrease: ", fStrengthDecrease, " Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdFilterAcceleratedFadeOutTimer))
		SET_TIMECYCLE_MODIFIER_STRENGTH(fFilterStrength)
		IF fFilterStrength <= 0.0
			PRINTLN("[FILTERS][ACCEL] PROCESS_TRANSITION_OUT_OF_FILTER - Filter is at ", fFilterStrength, " strength")
			fFilterStrength = 0.0
			CLEAR_TIMECYCLE_MODIFIER()
			CLEAR_BIT(iLocalBoolCheck30, LBOOL30_OK_TO_START_ACCELERATED_FILTER_TRANSITION_OUT)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL PROCESS_TRANSITION_OUT_OF_FILTER(INT iTeam, INT iRule)
	
	IF GET_TIMECYCLE_MODIFIER_INDEX() = -1
		PRINTLN("[FILTERS] PROCESS_TRANSITION_OUT_OF_FILTER - No active timecycle")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_FILTER_USE_ACCELERATED_TRANSITION_OUT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].iFilterAccelOutType)
		RETURN PROCESS_ACCELERATED_TRANSITION_OUT_OF_FILTER(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].iFilterAccelOutTime)
	ENDIF
		
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].iFilterFadeOutDuration != 0
	AND bPlayerToUseOK
		IF fFilterStrengthCached = -1.0
			fFilterStrengthCached = fFilterStrength
			REINIT_NET_TIMER(tdFilterFadeTimer)
		ELSE
			INT iDuration = ROUND(TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].iFilterFadeOutDuration) * fFilterStrengthCached) * 1000
			PRINTLN("[FILTERS] PROCESS_TRANSITION_OUT_OF_FILTER - Creator Fade out: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].iFilterFadeOutDuration, " fFilterStrengthCached: ", fFilterStrengthCached)
			PRINTLN("[FILTERS] PROCESS_TRANSITION_OUT_OF_FILTER - iDuration: ", iDuration, " Calc: ", TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].iFilterFadeOutDuration) * fFilterStrengthCached)
			IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdFilterFadeTimer, iDuration)
				
				FLOAT fTimeToRemove = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdFilterFadeTimer)) / iDuration
				fTimeToRemove *= fFilterStrengthCached
				fTimeToRemove = CLAMP(fTimeToRemove, -0.1, fFilterStrengthCached)
				PRINTLN("[FILTERS] PROCESS_TRANSITION_OUT_OF_FILTER - fTimeToRemove: ", fTimeToRemove)
				
				FLOAT fStrengthDecrease = fFilterStrengthCached - fTimeToRemove
				
				fFilterStrength = fStrengthDecrease
				PRINTLN("[FILTERS] PROCESS_TRANSITION_OUT_OF_FILTER - fFilterStrength: ", fFilterStrength, " fStrengthDecrease: ", fStrengthDecrease, " Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdFilterFadeTimer))
				SET_TIMECYCLE_MODIFIER_STRENGTH(fFilterStrength)
				IF fFilterStrength <= 0.0
					PRINTLN("PROCESS_TRANSITION_OUT_OF_FILTER - Filter is at ", fFilterStrength, " strength")
					fFilterStrength = 0.0
					CLEAR_TIMECYCLE_MODIFIER()
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
		fFilterStrength = 0.0
		FLOAT fTransitionTime = 1.0
		IF MISSION_HAS_VALID_MOCAP()
		AND NOT IS_RULE_INDEX_VALID(GET_TEAM_CURRENT_RULE(GET_LOCAL_PLAYER_TEAM(TRUE), FALSE))
		AND g_FMMC_STRUCT.sEndMocapSceneData.fEndCutsceneTimeCycleFade != 1.0
			fTransitionTime = g_FMMC_STRUCT.sEndMocapSceneData.fEndCutsceneTimeCycleFade
			PRINTLN("[FILTERS] PROCESS_TRANSITION_OUT_OF_FILTER - using transition time from end cutscene value: ", fTransitionTime)
		ENDIF
		SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(fTransitionTime)
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

PROC PROCESS_ACTIVE_FILTER_CLEANUP(INT iTeam, INT iRule)
	IF bFilterIsOn
		IF IS_ANY_FILTER_ACTIVE()
			iActiveFilter = 0
			iFilterBS = 0
			IF PROCESS_TRANSITION_OUT_OF_FILTER(iTeam, iRule)
				TURN_OFF_NON_ACTIVE_FILTERS(iActiveFilter)
				bFilterIsOn = FALSE
				fFilterStrengthCached = -1.0
				PRINTLN("[FILTERS] PROCESS_FILTERS - Clearing filters as bPlayerToUseOK = FALSE")
			ENDIF
		ELSE
			iActiveFilter = 0
			iFilterBS = 0
			bFilterIsOn = FALSE
			PRINTLN("[FILTERS] PROCESS_FILTERS - No filters are active, not disabling")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_FILTER_TRIGGER_CONDITION_BEEN_MET(INT iTeam, INT iRule)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule],  ciBS_RULE6_FILTERS_FORCE_FILTER)
		PRINTLN("HAS_FILTER_TRIGGER_CONDITION_BEEN_MET - Team ", iTeam," Rule ", iRule," - Forced filter")
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLowHealthFilter[iRule] > 0
		IF (GET_ENTITY_HEALTH(LocalPlayerPed) - 100) < (GET_ENTITY_MAX_HEALTH(LocalPlayerPed) - 100) / g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLowHealthFilter[iRule]
			PRINTLN("HAS_FILTER_TRIGGER_CONDITION_BEEN_MET - Team ", iTeam," Rule ", iRule," - Low health filter")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_FADE_IN_FILTER_STRENGTH(INT iTeam, INT iRule)
	
	INT iFadeIn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].iFilterFadeInDuration * 1000
	
	IF NOT HAS_NET_TIMER_STARTED(tdFilterFadeTimer)
		REINIT_NET_TIMER(tdFilterFadeTimer)
		PRINTLN("[FILTERS] PROCESS_FADE_IN_FILTER_STRENGTH - Starting fade timer")
	ELIF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdFilterFadeTimer, iFadeIn)
		FLOAT fStrength = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdFilterFadeTimer)) / iFadeIn
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].fFilterMaxStrength != 0.0
			fFilterStrength = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].fFilterMaxStrength * fStrength
			IF fFilterStrength > g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].fFilterMaxStrength
				fFilterStrength = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].fFilterMaxStrength
			ENDIF
		ELSE
			fFilterStrength = fStrength
		ENDIF
		PRINTLN("[FILTERS] PROCESS_FADE_IN_FILTER_STRENGTH - fFilterStrength: ", fFilterStrength)
	ENDIF
	
ENDPROC

FUNC INT GET_CURRENT_FILTER_SCORE()
	RETURN GET_FILTER_SCORE_FROM_TEAMS() - iCachedFilterScoreCount
ENDFUNC

PROC PROCESS_POINTS_BASED_FILTER_STRENGTH(INT iTeam, INT iRule)
	INT iPointsToFull = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].iFilterScoreTarget
	INT iCurrentPoints = GET_CURRENT_FILTER_SCORE()
	
	IF iCurrentPoints = 0
		EXIT
	ENDIF
	
	FLOAT fCreatorMax = 1.0
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].fFilterMaxStrength != 0.0
		fCreatorMax = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].fFilterMaxStrength
	ENDIF
	
	INT iSpeed = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].iFilterFadeInDuration
	FLOAT fStrengthMultiplier = CLAMP(iCurrentPoints / TO_FLOAT(iPointsToFull), 0.0, 1.0)
	FLOAT fCurrentmaxStrength = CLAMP(fCreatorMax * fStrengthMultiplier, 0.0, 1.0)
	FLOAT fSpeed = 1.0 / iSpeed
	INTERP_FLOAT_TO_TARGET_AT_SPEED(fFilterStrength, fCurrentmaxStrength, fSpeed)
	fFilterStrength = CLAMP(fFilterStrength, 0.0, 1.0)
	
	#IF IS_DEBUG_BUILD
	IF (GET_FRAME_COUNT() % 30) = 0
		PRINTLN("[FILTERS] PROCESS_POINTS_BASED_FILTER_STRENGTH - iCurrentPoints: ", iCurrentPoints)
		PRINTLN("[FILTERS] PROCESS_POINTS_BASED_FILTER_STRENGTH - fSpeed: ", fSpeed)
		PRINTLN("[FILTERS] PROCESS_POINTS_BASED_FILTER_STRENGTH - fCurrentmaxStrength: ", fCurrentmaxStrength)
		PRINTLN("[FILTERS] PROCESS_POINTS_BASED_FILTER_STRENGTH - fFilterStrength: ", fFilterStrength)
	ENDIF
	#ENDIF
ENDPROC

PROC PROCESS_ACTIVE_FILTER_STRENGTH(INT iTeam, INT iRule)
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].iFilterFadeInDuration = 0
	
		RESET_NET_TIMER(tdFilterFadeTimer)
		
		fFilterStrength = 1.0
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].fFilterMaxStrength != 0.0
			fFilterStrength = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].fFilterMaxStrength
		ENDIF
		
	ELSE
	
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].iFilterScoreTarget != 0
			PROCESS_POINTS_BASED_FILTER_STRENGTH(iTeam, iRule)
		ELSE
			PROCESS_FADE_IN_FILTER_STRENGTH(iTeam, iRule)
		ENDIF
				
	ENDIF
	
	IF fFilterStrength >= 0.0
		SET_TIMECYCLE_MODIFIER_STRENGTH(fFilterStrength)
	ENDIF
	
ENDPROC

PROC PROCESS_ACTIVE_FILTERS(INT iTeam, INT iRule)
	
	IF NOT IS_ANY_FILTER_ACTIVE()
		EXIT
	ENDIF
		
	PROCESS_ACTIVE_FILTER_STRENGTH(iTeam, iRule)
	
ENDPROC

PROC SWITCH_ON_FILTER(INT iTeam, INT iRule)
	
	SWITCH iActiveFilter
		DEFAULT
		CASE ciFILTER_OFF
			IF NOT IS_BIT_SET(iFilterBS, iActiveFilter)
				IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
					SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(1.0)
					iFilterBS = 0
					SET_BIT(iFilterBS, iActiveFilter)
					PRINTLN("[FILTERS] SWITCH_ON_FILTER - filters de-activated")
					PRINTLN("[FILTERS] SWITCH_ON_FILTER - Transitioning out of timecycle modifier")
				ENDIF
			ENDIF
		BREAK
		
		//Timecycle Filters
		CASE ciFILTER_BITSET_BLUEISH
		CASE ciFILTER_BITSET_OLDTV
		CASE ciFILTER_BITSET_SEPIA
		CASE ciFILTER_BITSET_BLACKWHITE
		CASE ciFILTER_BITSET_TRIPPIN
		CASE ciFILTER_BITSET_LOWSAT
		CASE ciFILTER_BITSET_BLUEISHPURP
		CASE ciFILTER_BITSET_NOIRE
		CASE ciFILTER_BITSET_HIGHSAT
		CASE ciFILTER_BITSET_NIGHTVISION
		CASE ciFILTER_BITSET_STONED
		CASE ciFILTER_BITSET_SHORT_TRIP
		CASE ciFILTER_BITSET_DARK_PARTY
			IF NOT IS_BIT_SET(iFilterBS, iActiveFilter)
				CLEAR_TIMECYCLE_MODIFIER()
				SET_TRANSITION_TIMECYCLE_MODIFIER(GET_FILTER_NAME(iActiveFilter), 1.0)
				iFilterBS = 0
				SET_BIT(iFilterBS, iActiveFilter)
				PRINTLN("[FILTERS] SWITCH_ON_FILTER - ",GET_FILTER_NAME(iActiveFilter)," filter activated")
				
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].iFilterFadeInDuration > 0
					SET_TIMECYCLE_MODIFIER_STRENGTH(0.0)
					PRINTLN("SWITCH_ON_FILTER - Timecycle strength set to 0.0 for fade in")
				ENDIF
			ENDIF
		BREAK
		
		//PostFX Filters
		CASE ciFILTER_BITSET_WHITE
		CASE ciFILTER_BITSET_ORANGE
		CASE ciFILTER_BITSET_PURPLE
		CASE ciFILTER_BITSET_GREEN
		CASE ciFILTER_BITSET_PINK
		CASE ciFILTER_BITSET_DEADLINE_NEON
			IF NOT IS_BIT_SET(iFilterBS, iActiveFilter)
				iFilterBS = 0
				SET_BIT(iFilterBS, iActiveFilter)
			ENDIF
			IF NOT bIsAnySpectator
				IF NOT ANIMPOSTFX_IS_RUNNING(GET_FILTER_NAME(iActiveFilter))
					ANIMPOSTFX_PLAY("InchPickup", 0, TRUE)
					PRINTLN("[KH][FILTERS] SWITCH_ON_FILTER - ",GET_FILTER_NAME(iActiveFilter)," filter activated")
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF iActiveFilter > -1
		bFilterIsOn = TRUE
	ELSE
		bFilterIsOn = FALSE
	ENDIF

ENDPROC

PROC PROCESS_FILTERS()
	INT iTeam = GET_LOCAL_PLAYER_TEAM(TRUE)
	INT iRule = GET_LOCAL_PLAYER_CURRENT_RULE(TRUE)
	
	IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
		EXIT
	ENDIF
	
	IF NOT IS_RULE_INDEX_VALID(iRule)
		EXIT
	ENDIF
	
	IF NOT bPlayerToUseOK
		PROCESS_ACTIVE_FILTER_CLEANUP(iTeam, iRule)
		EXIT
	ENDIF
	
	IF NOT SHOULD_FILTERS_BE_PROCESSED(iTeam, iRule)
		PROCESS_ACTIVE_FILTER_CLEANUP(iTeam, iRule)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_FILTERS_PLAYER_SELECTION)
						
		IF iCachedFilter != 0
			iActiveFilter = iCachedFilter
		ENDIF
		
		IF HAS_LOCAL_PLAYER_PRESSED_INPUT_FOR_FILTER()
			iActiveFilter = GET_NEXT_VALID_FILTER(iActiveFilter)

			IF IS_FILTER_A_TIMECYCLE_MODIFIER(iActiveFilter)
				bTimecycleTransitionComplete = FALSE
				PRINTLN("[FILTERS] PROCESS_FILTERS - TIMECYCLE SELECTED ", iActiveFilter)
			ENDIF
				
			iCachedFilter = iActiveFilter
		ENDIF
	ENDIF
	
	IF !bTimecycleTransitionComplete
		IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
			bTimecycleTransitionComplete = TRUE
		ENDIF
	ENDIF
	
	IF HAS_FILTER_TRIGGER_CONDITION_BEEN_MET(iTeam, iRule)
		iActiveFilter = 0
		iActiveFilter = GET_NEXT_VALID_FILTER(iActiveFilter)
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(tdTeamSwapPassiveModeTimer)
	AND g_MultiplayerSettings.g_bTempPassiveModeSetting
		IF NOT (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdTeamSwapPassiveModeTimer) >= 5000)
			iActiveFilter = 11
			PRINTLN("[FILTERS] PROCESS_FILTERS - Passive mode filter")
		ENDIF
	ENDIF

	IF IS_ANY_FILTER_ACTIVE()
		IF NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_IS_SPEED_BOOST_FILTER_DISABLED)
			SET_SPEED_BOOST_EFFECT_DISABLED(TRUE)
			PRINTLN("[FILTERS] PROCESS_FILTERS - Setting LBOOL18_IS_SPEED_BOOST_FILTER_DISABLED")
			SET_BIT(iLocalBoolCheck18, LBOOL18_IS_SPEED_BOOST_FILTER_DISABLED)
		ENDIF
		
		//Reduce the bloom on pickups if the neon filter is on
		IF ANIMPOSTFX_IS_RUNNING("DeadlineNeon")
			SET_LIGHT_OVERRIDE_MAX_INTENSITY_SCALE(fBloomScale)
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_IS_SPEED_BOOST_FILTER_DISABLED)
			SET_SPEED_BOOST_EFFECT_DISABLED(FALSE)
			PRINTLN("[FILTERS] PROCESS_FILTERS - Clearing LBOOL18_IS_SPEED_BOOST_FILTER_DISABLED")
			CLEAR_BIT(iLocalBoolCheck18, LBOOL18_IS_SPEED_BOOST_FILTER_DISABLED)
		ENDIF
	ENDIF
	
	TURN_OFF_NON_ACTIVE_FILTERS(iActiveFilter)
	
	SWITCH_ON_FILTER(iTeam, iRule)
	
	PROCESS_ACTIVE_FILTERS(iTeam, iRule)
	
ENDPROC

FUNC BOOL REDO_NEWS_FILTER()
	TEXT_LABEL_15 tl15Line0, tl15Line1
	GET_NEWS_HUD_TEXT_LABELS(tl15Line0, tl15Line1)
	sfFilter = REQUEST_SCALEFORM_MOVIE("BREAKING_NEWS")
	IF HAS_SCALEFORM_MOVIE_LOADED(sfFilter)
		DISPLAY_RADAR(FALSE)						
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sfFilter, "SET_TEXT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("SPC_TXT_DFLT")
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sfFilter, "CLEAR_SCROLL_TEXT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sfFilter, "SET_SCROLL_TEXT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_LABEL_WITH_PLAYER_NAME("MPTV_TICK0", GET_PLAYER_NAME(LocalPlayer))
		END_SCALEFORM_MOVIE_METHOD()
		
		GAMER_HANDLE gamerPlayer
		NETWORK_CLAN_DESC crewPlayer
		BEGIN_SCALEFORM_MOVIE_METHOD(sfFilter, "SET_SCROLL_TEXT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1)
			gamerPlayer = GET_GAMER_HANDLE_PLAYER(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed))
			IF IS_PLAYER_IN_ACTIVE_CLAN(gamerPlayer)
				crewPlayer = GET_PLAYER_CREW(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_LABEL_WITH_TL63("MPTV_TICK1", GET_CREW_CLAN_NAME(gamerPlayer, crewPlayer))
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPTV_TICK2")
			ENDIF
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sfFilter, "SET_SCROLL_TEXT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(2)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tl15Line0)
		END_SCALEFORM_MOVIE_METHOD()
		BEGIN_SCALEFORM_MOVIE_METHOD(sfFilter, "SET_SCROLL_TEXT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(3)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tl15Line1)
		END_SCALEFORM_MOVIE_METHOD()
			
		DISPLAY_RADAR(FALSE)
		
		fNewsFilterScrollEntry = 0
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sfFilter, "DISPLAY_SCROLL_TEXT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fNewsFilterScrollEntry)
		END_SCALEFORM_MOVIE_METHOD()
		
		timeNewsFilterNextUpdate = GET_NETWORK_TIME()
		
		PRINTLN("[LM] PROCESS_CAMERA_FILTER - REDO_NEWS_FILTER")
		
		screenRT = GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID()
		
		CLEAR_REDO_NEWS_HUD_DUE_TO_EVENT()		
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MANAGE_CAMERA_FILTER_RENDERING()
	SWITCH eCurrentFilter
		CASE SPEC_HUD_FILTER_NEWS
		CASE SPEC_HUD_FILTER_GTAOTV
			IF NOT IS_SPECTATOR_RUNNING_CUTSCENE()
			AND NOT g_bCelebrationScreenIsActive
			AND NOT IS_CUTSCENE_ACTIVE()
				IF iFilterStage = 99
					IF HAS_SCALEFORM_MOVIE_LOADED(sfFilter)
						SET_TEXT_RENDER_ID (screenRT)
						DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfFilter, 255,255,255,0)
						IF NOT IS_RADAR_HIDDEN()
							DISPLAY_RADAR(FALSE)
						ENDIF
						THEFEED_HIDE_THIS_FRAME()
						HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
 
 // copied from SCTV system.
PROC PROCESS_CAMERA_FILTER()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_EnableWeazelNewsCamFilterForHeli)
		EXIT
	ENDIF
	
	IF !bLocalPlayerPedOK
		g_bUsingTurretHeliCam = FALSE
		PRINTLN("[LM] PROCESS_CAMERA_FILTER - Player is dead - g_bUsingTurretHeliCam = FALSE")
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		g_bUsingTurretHeliCam = FALSE
		PRINTLN("[LM] PROCESS_CAMERA_FILTER - Player is not in a vehicle - g_bUsingTurretHeliCam = FALSE")
	ENDIF
		
	IF NOT IS_LOCAL_PLAYER_USING_TURRET_CAM()
	AND iFilterStage = -1
		EXIT
	ENDIF
	
	PRINTLN("[LM] PROCESS_CAMERA_FILTER - Processing iFilterStage: ", iFilterStage)
	
	IF IS_BIT_SET(iFilterBitSet, ci_MCFILTER_BIT_REFRESH_FILTER)
		iFilterStage = 0
		PRINTLN("[LM] PROCESS_CAMERA_FILTER - eDesiredFilter = ", ENUM_TO_INT(iFilterStage))
		CLEAR_BIT(iFilterBitSet, ci_MCFILTER_BIT_REFRESH_FILTER)
		PRINTLN("[LM] PROCESS_CAMERA_FILTER - ci_MCFILTER_BIT_REFRESH_FILTER CLEARED")
	ENDIF
	
	//option
	eDesiredFilter = SPEC_HUD_FILTER_NEWS
	
	SWITCH iFilterStage
		CASE -1
			IF IS_LOCAL_PLAYER_USING_TURRET_CAM()
				iFilterStage++
			ENDIF
		BREAK
		
		CASE 0	//noise on
			SET_TIMECYCLE_MODIFIER("CAMERA_secuirity_FUZZ")
			PLAY_SOUND_FRONTEND(iSoundChangeFilter, "Change_Cam", "MP_CCTV_SOUNDSET")
			iFilterChangeTime = GET_GAME_TIMER()
			iFilterStage++
			PRINTLN("[LM] PROCESS_CAMERA_FILTER - noise on, iFilterStage++")
		BREAK
		CASE 1	//cleanup current
			BOOL bFinished
			SWITCH eCurrentFilter
				CASE SPEC_HUD_FILTER_SPEC_1
				CASE SPEC_HUD_FILTER_SPEC_2
				CASE SPEC_HUD_FILTER_SPEC_3
				CASE SPEC_HUD_FILTER_SPEC_4
				CASE SPEC_HUD_FILTER_SPEC_5
				CASE SPEC_HUD_FILTER_SPEC_6
				CASE SPEC_HUD_FILTER_SPEC_7
				CASE SPEC_HUD_FILTER_SPEC_8
				CASE SPEC_HUD_FILTER_SPEC_9
				CASE SPEC_HUD_FILTER_SPEC_10
					bFinished = TRUE
				BREAK
				CASE SPEC_HUD_FILTER_NEWS
					DISPLAY_RADAR(TRUE)
					SET_SCTV_TICKER_SCORE_ON()
					SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfFilter)
					bFinished = TRUE
				BREAK
				CASE SPEC_HUD_FILTER_GTAOTV
					IF NETWORK_IS_ACTIVITY_SESSION()
						SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfFilter)
						DISPLAY_RADAR(TRUE)						
						SET_SCTV_TICKER_UI_OFF()
						bFinished = TRUE
					ENDIF
				BREAK
			ENDSWITCH
			IF bFinished
			AND IS_LOCAL_PLAYER_USING_TURRET_CAM()
				PRINTLN("[LM] PROCESS_CAMERA_FILTER - cleanup current, iFilterStage++")
				iFilterStage++
			ELIF NOT IS_LOCAL_PLAYER_USING_TURRET_CAM()
				PRINTLN("[LM] PROCESS_CAMERA_FILTER - cleanup current, iFilterStage = -1")
				CLEAR_TIMECYCLE_MODIFIER()
				iFilterStage = -1
			ENDIF
		BREAK
		CASE 2	//load in desired
			SWITCH eDesiredFilter				
				CASE SPEC_HUD_FILTER_SPEC_1
				CASE SPEC_HUD_FILTER_SPEC_2
				CASE SPEC_HUD_FILTER_SPEC_3
				CASE SPEC_HUD_FILTER_SPEC_4
				CASE SPEC_HUD_FILTER_SPEC_5
				CASE SPEC_HUD_FILTER_SPEC_6
				CASE SPEC_HUD_FILTER_SPEC_7
				CASE SPEC_HUD_FILTER_SPEC_8
				CASE SPEC_HUD_FILTER_SPEC_9
				CASE SPEC_HUD_FILTER_SPEC_10
					bFinished = TRUE
				BREAK
				CASE SPEC_HUD_FILTER_NEWS
					IF REDO_NEWS_FILTER()
						bFinished = TRUE
					ENDIF
				BREAK
				CASE SPEC_HUD_FILTER_GTAOTV
					IF NETWORK_IS_ACTIVITY_SESSION()
						DISPLAY_RADAR(FALSE)
						bFinished = TRUE
					ENDIF
				BREAK
			ENDSWITCH
			IF bFinished				
				PRINTLN("[LM] PROCESS_CAMERA_FILTER - load in desired, specData.specHUDData.iFilterStage++")
				iFilterStage++
			ENDIF
		BREAK
		CASE 3 //wait for timer
			IF GET_GAME_TIMER() - iFilterChangeTime > SPEC_HUD_FILTER_CHANGE_TIME
				PRINTLN("[LM] PROCESS_CAMERA_FILTER - wait for timer, specData.specHUDData.iFilterStage++")
				iFilterStage++
			ENDIF
		BREAK
		CASE 4	//noise off
			SWITCH eDesiredFilter
				CASE SPEC_HUD_FILTER_SPEC_1
					SET_TIMECYCLE_MODIFIER("spectator1")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_2
					SET_TIMECYCLE_MODIFIER("spectator2")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_3
					SET_TIMECYCLE_MODIFIER("spectator3")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_4
					SET_TIMECYCLE_MODIFIER("spectator4")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_5
					SET_TIMECYCLE_MODIFIER("spectator5")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_6
					SET_TIMECYCLE_MODIFIER("spectator6")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_7
					SET_TIMECYCLE_MODIFIER("spectator7")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_8
					SET_TIMECYCLE_MODIFIER("spectator8")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_9
					SET_TIMECYCLE_MODIFIER("spectator9")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_10
					SET_TIMECYCLE_MODIFIER("spectator10")
				BREAK
				
				CASE SPEC_HUD_FILTER_NEWS
				CASE SPEC_HUD_FILTER_GTAOTV
					SET_TIMECYCLE_MODIFIER("spectator1")
				BREAK
			ENDSWITCH
			
			// If we are no longer using a filter
			
			IF iSoundChangeFilter <> -1
				STOP_SOUND(iSoundChangeFilter)
				RELEASE_SOUND_ID(iSoundChangeFilter)
				iSoundChangeFilter = -1
			ENDIF
			PRINTLN("[LM] PROCESS_CAMERA_FILTER - noise off, specData.specHUDData.iFilterStage++")
			iFilterStage++
		BREAK
		CASE 5	//set current to desired
			eCurrentFilter = eDesiredFilter			
			PRINTLN("[LM] PROCESS_CAMERA_FILTER - set current to desired, specData.specHUDData.iFilterStage = 99")
			iFilterStage = 99
		BREAK
		CASE 99	//Run the filter
			
			MANAGE_CAMERA_FILTER_RENDERING()
			
			IF eCurrentFilter = SPEC_HUD_FILTER_NEWS
											
				IF IS_BIT_SET(iFilterBitSet, ci_MCFILTER_BIT_REFRESH_NEWS_FILTER)
					REDO_NEWS_FILTER()
				ENDIF
				
				IF NETWORK_IS_GAME_IN_PROGRESS()					
					DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfFilter, 255, 255, 255, 255)
					
					IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), timeNewsFilterNextUpdate) >= SPEC_HUD_NEWS_TICKER_TIME_UPDATE
						DISPLAY_RADAR(FALSE)
						
						fNewsFilterScrollEntry++
						
						IF fNewsFilterScrollEntry >= SPEC_HUD_NUMBER_OF_NEWS_TICKER
							fNewsFilterScrollEntry = 0
						ENDIF
						
						BEGIN_SCALEFORM_MOVIE_METHOD(sfFilter, "DISPLAY_SCROLL_TEXT")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fNewsFilterScrollEntry)
						END_SCALEFORM_MOVIE_METHOD()
						
						timeNewsFilterNextUpdate = GET_NETWORK_TIME()
						
						PRINTLN("[LM] PROCESS_CAMERA_FILTER - News Update... fNewsFilterScrollEntry: ", fNewsFilterScrollEntry)
					ELSE
						PRINTLN("[LM] PROCESS_CAMERA_FILTER - News Update... Time until next update: ", 5000-GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), timeNewsFilterNextUpdate))
					ENDIF
				ENDIF
			ENDIF
			IF eCurrentFilter = SPEC_HUD_FILTER_GTAOTV
			
			ENDIF
			IF NOT IS_LOCAL_PLAYER_USING_TURRET_CAM()
				iFilterStage = 1
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_GET_AND_DELIVER_FILTER()
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]

	BOOL bHoldingObj = FALSE
	
	
	IF iRule < FMMC_MAX_RULES
	AND NOT g_bMissionEnding
	AND NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_DIVE_SCORE_SCREEN_FX_ACTIVE)
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetSix[ iRule ], ciBS_RULE6_GET_DELIVER_RED_FILTER)		
			INT iobj
			FOR iobj = 0 TO ( MC_serverBD.iNumObjCreated - 1 )
				IF MC_serverBD.iObjCarrier[iobj] = iLocalPart
					IF NOT ANIMPOSTFX_IS_RUNNING("CrossLine")
					AND NOT IS_BIT_SET(GlobalPlayerBD_SCTV[NATIVE_TO_INT(LocalPlayer)].iGeneralBitSet, GPBDS_GBI_USING_NIGHTVISION)
						PRINTLN("[JS] - GET_AND_DELIVER_FILTER - CrossLine - Turning on red filter FX")
						ANIMPOSTFX_PLAY("CrossLine", 0, TRUE)
					ENDIF
					bHoldingObj = TRUE
				ENDIF
			ENDFOR
			IF bHoldingObj = FALSE
				IF ANIMPOSTFX_IS_RUNNING("CrossLine")
					PRINTLN("[JS] - GET_AND_DELIVER_FILTER - CrossLine - Turning off red filter FX")
					ANIMPOSTFX_STOP("CrossLine")
				ENDIF
			ENDIF
			
			IF bHoldingObj = TRUE
			AND IS_BIT_SET(GlobalPlayerBD_SCTV[NATIVE_TO_INT(LocalPlayer)].iGeneralBitSet, GPBDS_GBI_USING_NIGHTVISION)
				IF ANIMPOSTFX_IS_RUNNING("CrossLine")
					PRINTLN("[JS] - GET_AND_DELIVER_FILTER - CrossLine - Turning off red filter FX as nightvision is on")
					ANIMPOSTFX_STOP("CrossLine")
				ENDIF		
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Phone -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### Filters, vehicle/weapon HUDs etc.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


FUNC BOOL SHOULD_SEND_TEXT()

	IF IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_HEIST_ANY_PARTICIPANT_INSIDE_ANY_MP_PROPERTY)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC


FUNC BOOL SHOULD_TEXT_MESSAGE_TRIGGER_FROM_DIALOGUE_TRIGGER(INT iSMS)
	BOOL bDTsendsSMS = FALSE
	IF g_FMMC_STRUCT.sSMS[iSMS].iTriggerFromDT != -1
		IF FMMC_IS_LONG_BIT_SET(iLocalDialoguePlayingBS, g_FMMC_STRUCT.sSMS[iSMS].iTriggerFromDT)
			PRINTLN("[ML][SHOULD_TEXT_MESSAGE_TRIGGER_FROM_DIALOGUE_TRIGGER] Setting should dialogue trigger send SMS to TRUE")
			bDTsendsSMS = TRUE
		ENDIF
	ENDIF
	RETURN bDTsendsSMS
ENDFUNC

PROC PROCESS_TEXT_MESSAGES()
	
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_DIALOGUE)
	#ENDIF
	
	enumCharacterList tempContact
	INT i,inumstring
	BOOL bSendText

	IF SHOULD_SEND_TEXT()
		IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_FINISHED)	
		AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_START_SPECTATOR)
			FOR i = 0 TO (FMMC_MAX_NUM_SMS-1)
				IF NOT IS_BIT_SET(iMyLocalSentTextMsgBitset, i)
					IF g_FMMC_STRUCT.sSMS[i].iTeam	= MC_playerBD[iPartToUse].iteam
					OR g_FMMC_STRUCT.sSMS[i].iSendToTeams = FMMC_SMS_SEND_TO_ALL
						IF g_FMMC_STRUCT.sSMS[i].iTeam > -1
							IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT.sSMS[i].iTeam]  < FMMC_MAX_RULES
								IF (MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT.sSMS[i].iTeam] = g_FMMC_STRUCT.sSMS[i].iRule
								AND (g_FMMC_STRUCT.sSMS[i].iPointsRequired = -1 OR (MC_serverBD.iTeamScore[g_FMMC_STRUCT.sSMS[i].iTeam] >= g_FMMC_STRUCT.sSMS[i].iPointsRequired)))	
								OR SHOULD_TEXT_MESSAGE_TRIGGER_FROM_DIALOGUE_TRIGGER(i)
									PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - iPointsRequired: ", g_FMMC_STRUCT.sSMS[i].iPointsRequired, " iTeamScore: ", MC_serverBD.iTeamScore[g_FMMC_STRUCT.sSMS[i].iTeam])
									IF g_FMMC_STRUCT.sSMS[i].iTiming = ciFMMC_SMS_MID_RULE
										IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[g_FMMC_STRUCT.sSMS[i].iTeam],MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT.sSMS[i].iTeam])
											IF g_FMMC_STRUCT.sSMS[i].iDelaySeconds = 0
												PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - bSendText 1")
												bSendText = TRUE
											ELSE
												IF NOT HAS_NET_TIMER_STARTED(tdTextDelay[i])
													REINIT_NET_TIMER(tdTextDelay[i])
												ELSE
													IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdTextDelay[i]) > (1000*g_FMMC_STRUCT.sSMS[i].iDelaySeconds)
														PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - bSendText 2")
														bSendText = TRUE
													ENDIF
												ENDIF
											ENDIF	
										ENDIF
									ELSE
										IF g_FMMC_STRUCT.sSMS[i].iDelaySeconds = 0
											PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - bSendText 3")
											bSendText = TRUE
										ELSE
											IF NOT HAS_NET_TIMER_STARTED(tdTextDelay[i])
												REINIT_NET_TIMER(tdTextDelay[i])
											ELSE
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ResetSMSTimerAtStartOfEachRule)
													IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
														RESET_NET_TIMER(tdTextDelay[i])
														RELOOP
														PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - Resetting Text timer due to rule progression")
													ENDIF
												ENDIF
												IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdTextDelay[i]) > (1000*g_FMMC_STRUCT.sSMS[i].iDelaySeconds)
													PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - bSendText 4")
													bSendText = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									IF bSendText
									
										PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT.sSMS[i].iTeam] = g_FMMC_STRUCT.sSMS[i].iRule,   MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT.sSMS[i].iTeam] = ", MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT.sSMS[i].iTeam], "  g_FMMC_STRUCT.sSMS[i].iRule = ",  g_FMMC_STRUCT.sSMS[i].iRule, " g_FMMC_STRUCT.sSMS[i].iTeam = ", g_FMMC_STRUCT.sSMS[i].iTeam, " i =" , i )
										
										INT iTempBS = 0
										SET_BIT(iTempBS, MP_COMMS_MODIFIER_IGNORE_RECENT_INVITE)
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciSMS_CONTACT_UNKNOWN)
											tempContact = CHAR_ARTHUR
										ELIF g_FMMC_STRUCT.sSMS[i].iContactCharacter != -1
											tempContact = GET_INCOMING_CONTACT_CHARACTER_FROM_CREATOR(g_FMMC_STRUCT.sSMS[i].iContactCharacter)
										ELSE
											tempContact = Get_enumCharacterList_From_ContactAsInt(g_FMMC_STRUCT.iContactCharEnum)
										ENDIF
										
										IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sSMS[i].tl63Message[0])
											inumstring++
										ENDIF
										IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sSMS[i].tl63Message[1])
											inumstring++
										ENDIF
										
										IF IS_BIT_SET(g_FMMC_STRUCT.sSMS[i].iBitset, ciSMS_BITSET_BLOCK_DELETION)
											SET_BIT(iTempBS, MP_COMMS_MODIFIER_COMMS_LOCKED)
										ENDIF
										
										PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - Sending text... inumstring: ", inumstring, " iTempBS: ", iTempBS)
										
										IF IS_BIT_SET(g_FMMC_STRUCT.sSMS[i].iBitset, ciSMS_BITSET_TREAT_AS_TEXT_LABEL)
											IF g_FMMC_STRUCT.sSMS[i].iExtraInfo = ciSMS_EXTRA_INFO_SAFE_COMBINATION
												TEXT_LABEL_23 tl23 = ""
												INT iA = 0, iB = 0, iC = 0
												GET_RANDOM_SAFE_CRACK_COMBINATION(iA, iB, iC)
												IF iA < 10
													tl23 += "0"
												ENDIF
												tl23 += iA
												tl23 += "-"
												IF iB < 10
													tl23 += "0"
												ENDIF
												tl23 += iB
												tl23 += "-"
												IF iC < 10
													tl23 += "0"
												ENDIF
												tl23 += iC
												IF Request_MP_Comms_Txtmsg_With_Components(tempContact, g_FMMC_STRUCT.sSMS[i].tl63Message[0], " ", TRUE, NO_INT_SUBSTRING_COMPONENT_VALUE, tl23, DEFAULT, DEFAULT, iTempBS)
													SET_BIT(iMyLocalSentTextMsgBitset, i)
													PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - Sending text ... ciSMS_EXTRA_INFO_SAFE_COMBINATION Text message number ", i )
													RESET_NET_TIMER(tdTextDelay[i])
													bSendText = FALSE
												ENDIF
											ELIF inumstring = 1
												IF Request_MP_Comms_Txtmsg(tempContact, g_FMMC_STRUCT.sSMS[i].tl63Message[0], DEFAULT, iTempBS)
													SET_BIT(iMyLocalSentTextMsgBitset, i)
													PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - Sending text... inumstring = 1 Text message number ", i )
													RESET_NET_TIMER(tdTextDelay[i])
													bSendText = FALSE
												ENDIF
											ELIF inumstring = 2
												IF Request_MP_Comms_Txtmsg_With_Components(tempContact,g_FMMC_STRUCT.sSMS[i].tl63Message[0], " ", TRUE,NO_INT_SUBSTRING_COMPONENT_VALUE,g_FMMC_STRUCT.sSMS[i].tl63Message[1], DEFAULT, DEFAULT, iTempBS)
													SET_BIT(iMyLocalSentTextMsgBitset, i)
													PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - Sending text ... inumstring = 2 Text message number ", i )
													RESET_NET_TIMER(tdTextDelay[i])
													bSendText = FALSE
												ENDIF
											ENDIF
										ELSE
											IF inumstring = 1
												IF Request_MP_Comms_Txtmsg_With_Components(tempContact,"MIS_CUST_TXT2", " ", TRUE,NO_INT_SUBSTRING_COMPONENT_VALUE,g_FMMC_STRUCT.sSMS[i].tl63Message[0], DEFAULT, DEFAULT, iTempBS)
													SET_BIT(iMyLocalSentTextMsgBitset, i)
													PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - Sending text (literal)... inumstring = 1 Text message number ", i )
													RESET_NET_TIMER(tdTextDelay[i])
													bSendText = FALSE
												ENDIF
											ELIF inumstring = 2
												IF Request_MP_Comms_Txtmsg_With_Components(tempContact,"MIS_CUST_TXT3", " ", TRUE,NO_INT_SUBSTRING_COMPONENT_VALUE,g_FMMC_STRUCT.sSMS[i].tl63Message[0],g_FMMC_STRUCT.sSMS[i].tl63Message[1], DEFAULT, iTempBS)
													SET_BIT(iMyLocalSentTextMsgBitset, i)
													PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - Sending text (literal)... inumstring = 2 Text message number", i )
													RESET_NET_TIMER(tdTextDelay[i])
													bSendText = FALSE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_DIALOGUE)
	#ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Radar/Minimap -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// #####  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC CONTROL_RADAR_ZOOM(BOOL bInstant = FALSE)

FLOAT fZoomLevel

	IF NOT bInstant
		IF fOldFurthestTargetDist > fFurthestTargetDist
			IF (fOldFurthestTargetDist - fFurthestTargetDist) > 5
				fOldFurthestTargetDist = (fOldFurthestTargetDist - cfRADAR_ZOOM_SPEED)
			ENDIF
		ELSE
			IF (fFurthestTargetDist - fOldFurthestTargetDist) > 5
				fOldFurthestTargetDist = (fOldFurthestTargetDist + cfRADAR_ZOOM_SPEED)
			ENDIF
		ENDIF
		fZoomLevel = (fOldFurthestTargetDist/cfRADAR_ZOOM_ADJUST)
		
		// Cap the zoom level
		IF (fZoomLevel >= cfRADAR_ZOOM_CAP)
			fZoomLevel = cfRADAR_ZOOM_CAP
		ELIF (fZoomLevel < cfRADAR_ZOOM_CAP_MIN)
			fZoomLevel = cfRADAR_ZOOM_CAP_MIN
		ENDIF

		// Allow debug tweak of radar zoom 
		// Script/ FreeMode/Les Widgets/Radar Zoom /Radar Zoom 
		#IF IS_DEBUG_BUILD
		IF (f_i_RadarZoom > -1.0)
			SET_RADAR_ZOOM_TO_DISTANCE(f_i_RadarZoom)
		ELSE
		#ENDIF
		//PRINTLN("SET_RADAR_ZOOM_TO_DISTANCE : ",fZoomLevel) 
		IF DO_RADAR_ZOOM()
			SET_RADAR_ZOOM_TO_DISTANCE(fZoomLevel)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
	ELSE
		fOldFurthestTargetDist = fFurthestTargetDist
		fFurthestTargetDistTemp = fFurthestTargetDist
		fZoomLevel = (fOldFurthestTargetDist/cfRADAR_ZOOM_ADJUST)
		//PRINTLN("SET_RADAR_ZOOM_TO_DISTANCE : ",fZoomLevel)
		IF DO_RADAR_ZOOM()
			SET_RADAR_ZOOM_TO_DISTANCE(fZoomLevel)
		ENDIF
	ENDIF

ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Blipping -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// #####  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


///PURPOSE: This returns true if the the inputted team's index should be flashing as set in
///    the creator
FUNC BOOL SHOULD_THIS_PLAYERS_BLIP_FLASH( INT iTeam )
	BOOL bReturn = FALSE
	
	IF   IS_BIT_SET( g_FMMC_STRUCT.iRuleOptionsBitSet, ciFLASH_BLIP_TEAM0 ) AND iTeam = 0
		bReturn = TRUE
	ELIF IS_BIT_SET( g_FMMC_STRUCT.iRuleOptionsBitSet, ciFLASH_BLIP_TEAM1 ) AND iTeam = 1
		bReturn = TRUE
	ELIF IS_BIT_SET( g_FMMC_STRUCT.iRuleOptionsBitSet, ciFLASH_BLIP_TEAM2 ) AND iTeam = 2
		bReturn = TRUE
	ELIF IS_BIT_SET( g_FMMC_STRUCT.iRuleOptionsBitSet, ciFLASH_BLIP_TEAM3 ) AND iTeam = 3
		bReturn = TRUE
	ENDIF

	RETURN bReturn
ENDFUNC

///PURPOSE: This returns true if any of the teams blips should flash
FUNC BOOL SHOULD_ANY_TEAM_BLIPS_FLASH()
	BOOL bReturn = FALSE
	
	INT iTeamLoop = 0
	
	FOR iTeamLoop = 0 TO 3
		IF SHOULD_THIS_PLAYERS_BLIP_FLASH( iTeamLoop )
			bReturn = TRUE
		ENDIF
	ENDFOR
	
	RETURN bReturn
ENDFUNC

///PUPOSE: This function sets any applicable team's blips flashing by looping through
///    all participants and setting their blips to flash 
PROC SET_TEAM_BLIPS_FLASHING()
	INT iParticipant = 0
	
	PRINTLN("[PLAYER_LOOP] - SET_TEAM_BLIPS_FLASHING")
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE( INT_TO_NATIVE( PARTICIPANT_INDEX, iParticipant ) )
			
			PARTICIPANT_INDEX partId 	= INT_TO_NATIVE( PARTICIPANT_INDEX, iParticipant )
			PLAYER_INDEX 	playerId 	= NETWORK_GET_PLAYER_INDEX( partId )
			INT				iPlayerTeam = GET_PLAYER_TEAM( playerID )
			
			PRINTLN("SET_TEAM_BLIPS_FLASHING - iParticipant: ", iParticipant)
			PRINTLN("SET_TEAM_BLIPS_FLASHING - iPlayerTeam: ", iPlayerTeam)

			// Flash this player's blip if they're on the same team
			IF SHOULD_THIS_PLAYERS_BLIP_FLASH( iPlayerTeam )
				PRINTLN("SET_TEAM_BLIPS_FLASHING SHOULD_THIS_PLAYERS_BLIP_FLASH - playerId: ", GET_PLAYER_TEAM( playerID ))
				FLASH_BLIP_FOR_PLAYER( playerId, TRUE, -1 )
			ENDIF
			
		ENDIF
	ENDREPEAT
	
ENDPROC

///PUPOSE: This function sets any applicable team's blips to a set colour by looping through
///    all participants and setting their blips to flash 
PROC SET_PLAYER_ALTERNATIVE_BLIP_COLOURS()
	
	PRINTLN("[LH][BLIP_COLOURS] - Setting player blips to an alternate colour.")
	INT iParticipant = 0
	PRINTLN("[PLAYER_LOOP] - SET_PLAYER_ALTERNATIVE_BLIP_COLOURS")
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE( INT_TO_NATIVE( PARTICIPANT_INDEX, iParticipant ) )
			
			PARTICIPANT_INDEX partId 	= INT_TO_NATIVE( PARTICIPANT_INDEX, iParticipant )
			PLAYER_INDEX 	playerId 	= NETWORK_GET_PLAYER_INDEX( partId )
			INT				iPlayerTeam = GET_PLAYER_TEAM( playerID )
			INT iBlipColour
			
			IF iPlayerTeam < FMMC_MAX_TEAMS AND iPlayerTeam >= 0
				IF g_FMMC_STRUCT.iTeamColourOverride[iPlayerTeam] != -1
					iBlipColour = GET_BLIP_COLOUR_FROM_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iPlayerTeam, playerId))
				ENDIF
			ENDIF
			
			IF iBlipColour != BLIP_COLOUR_DEFAULT
				SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerId, iBlipColour, TRUE)
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE: If we're on a VS mission, display the blips of the enemy team for 5 seconds unless a flag is set
///    ALSO set a team's blips flashing if needed
PROC SET_PLAYER_BLIPS_DISPLAY_AT_START_OF_VS_MISSION()

	IF g_bVSMission
	AND NOT IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSet, ciHIDE_VERSUS_BLIPS )
		CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][BLIP] Turning on all player blips at the start of VS mission. Setting 5 second timer.")
		SHOW_ALL_PLAYER_BLIPS(TRUE)
		SET_ALL_PLAYER_BLIPS_LONG_RANGE(TRUE)
		START_NET_TIMER( tdPlayerBlipsTimer )
	ENDIF 
	
	IF SHOULD_ANY_TEAM_BLIPS_FLASH()
		SET_TEAM_BLIPS_FLASHING()
	ENDIF
	
	IF NOT IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSet, ciHIDE_VERSUS_BLIPS )
		INT iTeam = 0
		BOOL bSetAltTeams
		REPEAT FMMC_MAX_TEAMS iTeam
			IF g_FMMC_STRUCT.iTeamColourOverride[iTeam] != -1
				bSetAltTeams = TRUE
			ENDIF
		ENDREPEAT
		IF bSetAltTeams
			SET_PLAYER_ALTERNATIVE_BLIP_COLOURS()
		ENDIF
	ENDIF

ENDPROC

PROC SET_SPAWN_VEHICLE_BLIP_SETTINGS(VEHICLE_INDEX veh, INT iPrimaryBlipColour)
	
	// Should this blip be rotated?
	IF SHOULD_BLIP_BE_MANUALLY_ROTATED(biSpawnVehicle)
		CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Detected blip should be manually rotated.")
		SET_BLIP_ROTATION(biSpawnVehicle, ROUND(GET_ENTITY_HEADING(veh)))
	ENDIF	
	
	// should we show the height indicator?
	BOOL bUseExtendedHeightThreshold
	SHOW_HEIGHT_ON_BLIP(biSpawnVehicle, SHOULD_SHOW_HEIGHT_ON_BLIP(biSpawnVehicle, bUseExtendedHeightThreshold))
	SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(biSpawnVehicle, bUseExtendedHeightThreshold)
	
	CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Setting primary colour to ", iPrimaryBlipColour)
	SET_BLIP_COLOUR(biSpawnVehicle, iPrimaryBlipColour )
	SET_BLIP_SCALE(biSpawnVehicle, BLIP_SIZE_NETWORK_VEHICLE )
	
	IF IS_SPECIAL_VEHICLE(veh)
		SET_BLIP_PRIORITY( biSpawnVehicle, BLIPPRIORITY_HIGH_HIGHEST ) 
	ELSE
		SET_BLIP_PRIORITY( biSpawnVehicle, BLIPPRIORITY_HIGHEST ) 
	ENDIF	
	CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Finished configuring spawn blip.")
ENDPROC

PROC MAINTAIN_SPAWN_VEHICLE_BLIP()
	IF bManageSpawnVehicleBlip
		IF IS_VEHICLE_DRIVEABLE(vehSpawnVehicle)
			IF GET_VEHICLE_ENGINE_HEALTH(vehSpawnVehicle) > 0
				IF bLocalPlayerPedOK
					IF NOT IS_PLAYER_SPECTATING( LocalPlayer )
						IF NOT IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
							
							IF NOT DOES_BLIP_EXIST(biSpawnVehicle)
								CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Removing any existing blips on the spawn veh ahead of spawn blip creation.")
								REMOVE_ANY_EXISTING_BLIP_FROM_ENTITY(vehSpawnVehicle)
								
								CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Creating blip for team spawn vehicle.")
								biSpawnVehicle = ADD_BLIP_FOR_ENTITY(vehSpawnVehicle)
								
								BLIP_SPRITE eSprite
								// What blip sprite should we be dealing with?
								eSprite = GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(GET_ENTITY_MODEL(vehSpawnVehicle), FALSE, IS_VEHICLE_EMPTY_AND_STATIONARY(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(vehSpawnVehicle)))
								
								// Set the correct blip sprite.
								IF NOT (eSprite = GET_STANDARD_BLIP_ENUM_ID())
									CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Setting blip sprite as ", GET_BLIP_SPRITE_DEBUG_STRING(eSprite), ".")
									
									SET_BLIP_SPRITE(biSpawnVehicle, eSprite)
									SET_CUSTOM_BLIP_NAME_FROM_MODEL(biSpawnVehicle, GET_ENTITY_MODEL(vehSpawnVehicle))
								ENDIF	
								
								SET_SPAWN_VEHICLE_BLIP_SETTINGS(vehSpawnVehicle, BLIP_COLOUR_BLUEDARK)
							ENDIF
							
						//Player is in the team vehicle.
						ELIF DOES_BLIP_EXIST(biSpawnVehicle)
							CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Player is in the spawn vehicle. Removing blip.")
							REMOVE_BLIP(biSpawnVehicle)
						ENDIF
						
					//Player is spectating.
					ELIF DOES_BLIP_EXIST(biSpawnVehicle)
						CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Player is spectating. Removing blip.")
						REMOVE_BLIP(biSpawnVehicle)
					ENDIF
					
				//Player is injured.
				ELIF DOES_BLIP_EXIST(biSpawnVehicle)
					CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Player is injured. Removing blip.")
					REMOVE_BLIP(biSpawnVehicle)
				ENDIF
				
			//Player's vehicle has a broken engine.
			ELIF DOES_BLIP_EXIST(biSpawnVehicle)
				CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Vehicle engine health too low. Removing blip.")
				REMOVE_BLIP(biSpawnVehicle)
			ENDIF
			
		//Vehicle destroyed.
		ELIF DOES_BLIP_EXIST(biSpawnVehicle)
			CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Spawn vehicle is destroyed. Removing blip.")
			REMOVE_BLIP(biSpawnVehicle)
		ENDIF
		
	//Blip management deactivated.
	ELIF DOES_BLIP_EXIST(biSpawnVehicle)
		CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Spawn vehicle blip management deactivated. Removing blip.")
		REMOVE_BLIP(biSpawnVehicle)
	ENDIF
ENDPROC


PROC TEAMS_TO_BLIP(INT iLocalPlayerTeam, INT iPartTeam)
	IF iLocalPlayerTeam != iPartTeam
		IF iPartTeam = 0
			IF NOT IS_BIT_SET( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM0_TARGET )
				SET_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM0_TARGET )
			ENDIF
		ELIF iPartTeam = 1
			IF NOT IS_BIT_SET( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM1_TARGET )
				SET_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM1_TARGET )
			ENDIF
		ELIF iPartTeam = 2
			IF NOT IS_BIT_SET( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM2_TARGET )
				SET_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM2_TARGET )
			ENDIF
		ELIF iPartTeam = 3
			IF NOT IS_BIT_SET( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM3_TARGET )
				SET_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM3_TARGET )
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Blip of Speed creator setting - blips the player when they go above a certain speed in a vehicle
PROC PROCESS_BLIP_ON_VEHICLE_SPEED()
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetSix[iRule], ciBS_RULE6_BLIP_ON_SPEED_ENABLED)

			INT i = 0	
			PRINTLN("[PLAYER_LOOP] - PROCESS_BLIP_ON_VEHICLE_SPEED")
			FOR i = 0 TO (NUM_NETWORK_PLAYERS - 1)
				INT iScratchTeam = MC_playerBD[i].iteam
				IF iScratchTeam < FMMC_MAX_TEAMS
				AND iScratchTeam != -1
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[i].iteam].iRuleBitsetSix[iRule], ciBS_RULE6_BLIP_ON_SPEED_ENABLED)
						//make sure you're not blipping yourself
						//IF i != iPartToUse
						INT iScratchRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[i].iteam]
							
						IF iScratchRule < FMMC_MAX_RULES
							INT iBlipTime = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[i].iteam].iBlipOnSpeedTime[iScratchRule]
							INT iThresholdSpeed = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[i].iteam].iBlipOnSpeedThreshold[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[i].iteam]]
							
							//if either value is 0, ignore that team as they want to see the blips but don't want to show them to other teams
							IF iThresholdSpeed != 0 OR iBlipTime != 0
							INT iPartToCheck = NATIVE_TO_INT(INT_TO_PARTICIPANTINDEX(i))
								//check that the player is in a car before trying to blip them
								IF MC_playerBD[iPartToCheck].bIsOverSpeedThreshold
									//if you're checking yourself and you're over the threshold, flash your arrow blip
									IF iPartToCheck = iPartToUse
										FLASH_MY_PLAYER_ARROW_RED(TRUE, -1, HIGHEST_INT, 0)
									ENDIF
									TEAMS_TO_BLIP(MC_playerBD[iPartToUse].iteam, MC_playerBD[i].iteam)
										
									IF NOT IS_BIT_SET(iBlippingPlayerBS, iPartToCheck)
										SET_BIT(iBlippingPlayerBS, iPartToCheck)
									ELSE
										IF HAS_NET_TIMER_STARTED(biBlipOnSpeedTimer[i])
											RESET_NET_TIMER(biBlipOnSpeedTimer[i])
										ENDIF
									ENDIF
								ELSE
									IF IS_BIT_SET(iBlippingPlayerBS, iPartToCheck)
										//if they're not over the speed and they've been blipped, start the timer to remove the blip
										IF NOT HAS_NET_TIMER_STARTED(biBlipOnSpeedTimer[i])
											PRINTLN("[KH][BOS] PROCESS_BLIP_ON_VEHICLE_SPEED - PART: ", iPartToUse, " is under the specified speed, starting timer to remove blip")
											START_NET_TIMER(biBlipOnSpeedTimer[i])
										ENDIF
										INT iTimeElapsed = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(biBlipOnSpeedTimer[i])
											
										IF iTimeElapsed > (iBlipTime * 1000)
											PRINTLN("[KH][BOS] PROCESS_BLIP_ON_VEHICLE_SPEED - PART: ", iPartToUse, " has been under the speed specified for too long, removing blip and resetting timer")
											RESET_NET_TIMER(biBlipOnSpeedTimer[i])
											CLEAR_BIT(iBlippingPlayerBS, iPartToCheck)
											IF iPartToCheck = iPartToUse
												FLASH_MY_PLAYER_ARROW_RED(FALSE)
											ENDIF
										ELSE
											TEAMS_TO_BLIP(MC_playerBD[iPartToUse].iteam, MC_playerBD[i].iteam)
										ENDIF	
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_ALWAYS_SHOW_ONE_ENEMY_BLIP()

	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_ALWAYS_SHOW_AT_LEAST_1_ENEMY_BLIP)
		EXIT
	ENDIF
	
	IF !bLocalPlayerPedOK
		EXIT
	ENDIF
		
	IF iPedBlipStaggeredLoop > -1
	AND iPedBlipStaggeredLoop < MC_serverBD.iNumPedCreated
			
		PRINTLN("[LM][PROCESS_ALWAYS_SHOW_ONE_ENEMY_BLIP] - iPedBlipStaggeredLoop: ", iPedBlipStaggeredLoop, " Custom blip override bitset: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedBlipStaggeredLoop].sPedBlipStruct.sTeamBlip[iTeam].iBlipOverrideBitSet)
		
		IF SHOULD_ENTITY_BE_BLIPPED_THROUGH_OVERRIDES(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedBlipStaggeredLoop].sPedBlipStruct)
			PED_INDEX tempPed
			NETWORK_INDEX niEntity
			niEntity = MC_serverBD_1.sFMMC_SBD.niPed[iPedBlipStaggeredLoop]	
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niEntity)
				tempPed = NET_TO_PED(niEntity)
				
				IF NOT IS_PED_INJURED(tempPed)
					
					FLOAT fDist
					fDist = VDIST2(vPlayerPosCached, GET_ENTITY_COORDS(tempPed))
					IF fDist < fPedAlwaysShowOneBlipDistCache
						fPedAlwaysShowOneBlipDistCache = fDist
						iPedAlwaysShowOneBlipCache = iPedBlipStaggeredLoop
						PRINTLN("[LM][PROCESS_ALWAYS_SHOW_ONE_ENEMY_BLIP] - iPedBlipStaggeredLoop: ", iPedBlipStaggeredLoop, " fDist: ", fDist)
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		
		iPedBlipStaggeredLoop++
		
	ELSE
		
		PRINTLN("[LM][PROCESS_ALWAYS_SHOW_ONE_ENEMY_BLIP] - DONE STAGGERED - iPedAlwaysShowOneBlipCache: ", iPedAlwaysShowOneBlipCache, " fPedAlwaysShowOneBlipDistCache: ", fPedAlwaysShowOneBlipDistCache)
	
		iPedAlwaysShowOneBlipChosen = iPedAlwaysShowOneBlipCache		
		vPlayerPosCached = GET_ENTITY_COORDS(localPlayerPed)
		iPedBlipStaggeredLoop = 0
		fPedAlwaysShowOneBlipDistCache = 9999999
		iPedAlwaysShowOneBlipCache = -1
	ENDIF
	
	IF iPedAlwaysShowOneBlipChosen > -1
		PRINTLN("[LM][PROCESS_ALWAYS_SHOW_ONE_ENEMY_BLIP] - Blip - iPedAlwaysShowOneBlipChosen: ", iPedAlwaysShowOneBlipChosen)		
		SET_BIT(sMissionPedsLocalVars[iPedAlwaysShowOneBlipChosen].iPedBS, ci_MissionPedBS_ForcedBlip)
	ENDIF
		
ENDPROC

FUNC BOOL HAS_CHANGED_DELIVERY_BLIP_SPRITE()

	BLIP_SPRITE CorrectBlipSprite
	// should we change the sprite?
	CorrectBlipSprite = GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(GET_ENTITY_MODEL(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iDeliveryBlip_veh])))	
	IF NOT (CorrectBlipSprite = GET_STANDARD_BLIP_ENUM_ID())
	AND NOT (GET_BLIP_SPRITE(DeliveryBlip) = CorrectBlipSprite)
	
		CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] HAS_CHANGED_DELIVERY_BLIP_SPRITE -  changing to sprite blip ", GET_BLIP_SPRITE_DEBUG_STRING(CorrectBlipSprite), " this iDeliveryBlip_veh ", iDeliveryBlip_veh, ".")

		// if we change the sprite we need to re-apply the settings.
		SET_BLIP_SPRITE(DeliveryBlip, CorrectBlipSprite)
		SET_CUSTOM_BLIP_NAME_FROM_MODEL(DeliveryBlip, GET_ENTITY_MODEL(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iDeliveryBlip_veh])))
		RETURN(TRUE)
	ENDIF
	
	RETURN(FALSE)

ENDFUNC

///PURPOSE: This function adds a blip in for a get & deliver
///    bBlipChange will refresh the blip (remove and re-add on the next call)
PROC ADD_DELIVERY_BLIP( BOOL bBlipChange = FALSE )

	iDeliveryBlip_veh = -1

	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE6_HIDE_DROPOFF_BLIP)
			IF NOT IS_OBJECTIVE_BLOCKED()
				IF NOT DOES_BLIP_EXIST(DeliveryBlip)
						
					VECTOR vDropoff = GET_DROP_OFF_CENTER(TRUE)
					
					IF NOT IS_VECTOR_ZERO(vDropoff)
						IF (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iDropOffType[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] != ciFMMC_DROP_OFF_TYPE_VEHICLE)
							IF NOT DOES_BLIP_EXIST(DeliveryBlip)
								DeliveryBlip = ADD_BLIP_FOR_COORD(vDropoff)
							ENDIF
							PRINTLN("[RCC MISSION] Adding delivery blip at coords: ",vDropoff)
							SET_BLIP_PRIORITY(DeliveryBlip,BLIPPRIORITY_OVER_CENTRE_BLIP)
							iDelblipPriority = mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
							
							IF GET_PLAYER_CARRY_COUNT() > 0
							OR (IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER) OR (MC_playerBD[iPartToUse].iVehNear != -1))
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iDropOffGPSBitSet, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
								AND NOT USING_HEIST_SPECTATE()
								AND LocalPlayerCurrentInterior = NULL
									PRINTLN("[MMacK][GPSRoute] 1")
									SET_BLIP_ROUTE(DeliveryBlip,TRUE)
									PRINTLN("[RCC MISSION] setting player drop off route")
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION] setting drop off blip alpha")
								SET_BLIP_ALPHA(DeliveryBlip,150)
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE3_IGNORE_GPS_BLOCKING)
								SET_IGNORE_NO_GPS_FLAG(TRUE)
								PRINTLN("[RCC MISSION] ADD_DELIVERY_BLIP called SET_IGNORE_NO_GPS_FLAG TRUE ", vDropoff)
								SET_BIT(iLocalBoolCheck9, LBOOL9_SET_IGNORE_NO_GPS_FLAG)
							ENDIF
														
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE6_DROP_OFF_HOUSE_BLIP)
								SET_BLIP_SPRITE(DeliveryBlip,RADAR_TRACE_CAPTURE_THE_FLAG_BASE)
								SET_BLIP_COLOUR(DeliveryBlip,BLIP_COLOUR_YELLOW)
								SET_BLIP_NAME_FROM_TEXT_FILE(DeliveryBlip,"BLIP_0")
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitSetTen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE10_USE_TEAM_COLOUR_ON_DELIVERY_BLIP)
								SET_BLIP_COLOUR(DeliveryBlip, GET_INT_FROM_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iPartToUse].iteam, PlayerToUse)))	
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetThirteen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE13_DISABLE_DELIVERY_BLIP_HEIGHT_INDICATOR)
								PRINTLN("[MJL] About to set show height on blip to FALSE")
								SHOW_HEIGHT_ON_BLIP(DeliveryBlip, FALSE)
							ELSE
								PRINTLN("[MJL] About to set show height on blip to TRUE")
								SHOW_HEIGHT_ON_BLIP(DeliveryBlip, TRUE)
							ENDIF
							
						ELSE //We're dropping off at a vehicle
							
							INT iVeh = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iDropOff_Vehicle[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]
							
							IF iVeh > -1
							AND (NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
							AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
							
								IF NOT IS_BIT_SET(iVehBlipCreatedThisFrameBitset, iVeh)
									CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH]  Removing any existing blips on vehicle ", iVeh, " ahead of delivery blip creation.")
									REMOVE_ANY_EXISTING_BLIP_FROM_ENTITY(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
									
									IF DOES_BLIP_EXIST(biVehBlip[iVeh])
										CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH]  Veh: ", iVeh, " still has a blip, removing it")
										REMOVE_BLIP(biVehBlip[iVeh])
									ENDIF
									
									IF NOT DOES_BLIP_EXIST(DeliveryBlip)
										DeliveryBlip = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
									ENDIF
									iDeliveryBlip_veh = iVeh
									
									IF HAS_CHANGED_DELIVERY_BLIP_SPRITE()
										CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] changed delivery blip to a sprite ", iVeh, " this frame (", GET_FRAME_COUNT(), ").")
									ENDIF
									
									//Blocks another kind of vehicle blip attempting to be made on this vehicle this frame.
									CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Flagging to block any more new blips being created on veh ", iVeh, " this frame (", GET_FRAME_COUNT(), ").")
									SET_BIT(iVehBlipCreatedThisFrameBitset, iVeh)
									
									SET_BLIP_COLOUR_FROM_HUD_COLOUR(DeliveryBlip,HUD_COLOUR_BLUEDARK)
									SET_BLIP_PRIORITY(DeliveryBlip,BLIPPRIORITY_OVER_CENTRE_BLIP)
									SET_BIT( iLocalBoolCheck5, LBOOL5_GPS_DELIVER_ROUTE_SET )	// url:bugstar:2195395
									iDelblipPriority = mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
								ELSE
									CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Blocking vehicle delivery blip creation this frame as another blip has just been made on the vehicle.")
								ENDIF
								
							ENDIF
							
						ENDIF
					ENDIF
				ELSE // Else the blip exists
					IF iDelblipPriority != mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
					OR bBlipChange
					OR MPGlobalsAmbience.R2Pdata.bOnRaceToPoint != bonImpromptuRace
						PRINTLN("[RCC MISSION] removing drop off blip")
						REMOVE_DELIVERY_BLIP()
						iDelblipPriority = mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
						bonImpromptuRace = MPGlobalsAmbience.R2Pdata.bOnRaceToPoint 
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF iDelblipPriority != mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
			OR bBlipChange
				PRINTLN("[RCC MISSION] Current blip hidden, removing previous")
				REMOVE_DELIVERY_BLIP()
				iDelblipPriority = mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
			ENDIF
		ENDIF
	ENDIF

ENDPROC

///PURPOSE: This function is used to clear GPS when a vehicle get & deliver rule progresses
///    to "help your team-mates deliver the vehicle"
FUNC BOOL SHOULD_CLEAR_DELIVERY_BLIP_ON_SECONDARY_OBJECTIVE_TEXT()
	BOOL bReturn = FALSE
	
	INT iTeam = MC_playerBD[ iPartToUse ].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]

	IF GET_MC_CLIENT_MISSION_STAGE( iPartToUse ) = CLIENT_MISSION_STAGE_COLLECT_VEH
		IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule ], ciBS_RULE_HIDE_GPS_ON_SECONDARY )
		AND MC_serverBD.iNumVehHighestPriorityHeld[ iTeam ] >= MC_serverBD.iNumVehHighestPriority[ iTeam ]				
		AND ( (g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] != ciFMMC_DROP_OFF_TYPE_VEHICLE )
			 OR ( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOff_Vehicle[ iRule ] = -1 ) )
			bReturn = TRUE
		ENDIF
	ENDIF
	
	RETURN bReturn
ENDFUNC

PROC UPDATE_DELIVERY_BLIP_SPRITE()

	IF (iDeliveryBlip_veh > -1)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iDeliveryBlip_veh])
			IF DOES_ENTITY_EXIST(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iDeliveryBlip_veh]))		
			
				// should we change the sprite?
				IF HAS_CHANGED_DELIVERY_BLIP_SPRITE()
					// if we change the sprite we need to re-apply the settings, as basically it's a new blip that gets created.

					//Blocks another kind of vehicle blip attempting to be made on this vehicle this frame.
					CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] UPDATE_DELIVERY_BLIP_SPRITE -  Flagging to block any more new blips being created on veh ", iDeliveryBlip_veh, " this frame (", GET_FRAME_COUNT(), ").")
					SET_BIT(iVehBlipCreatedThisFrameBitset, iDeliveryBlip_veh)
					
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(DeliveryBlip,HUD_COLOUR_BLUEDARK)
					SET_BLIP_PRIORITY(DeliveryBlip,BLIPPRIORITY_OVER_CENTRE_BLIP)
				ENDIF

				// Should this blip be rotated?
				IF SHOULD_BLIP_BE_MANUALLY_ROTATED(DeliveryBlip)
					SET_BLIP_ROTATION(DeliveryBlip, ROUND(GET_ENTITY_HEADING_FROM_EULERS(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iDeliveryBlip_veh]))))
				ENDIF
			
			ELSE
				iDeliveryBlip_veh = -1		
			ENDIF
		ELSE
			iDeliveryBlip_veh = -1
		ENDIF
	ENDIF
	
ENDPROC

///PURPOSE: This function will turn GPS on and off for the delivery blip
///    depending on the "GPS Clear Within" option. This option defaults to "always on" 
///    which will force the blip to always have a GPS route every frame.
///    
///    NB:	If iDistanceToCheck = -1, there's no GPS
///    		IF iDistanceToCheck = 0, there's always GPS
///    		IF iDistanceToCheck > 0, that's the distance in meters at which the GPS is cleared
PROC MANAGE_DELIVERY_BLIP()

	INT iCurrentTeam = MC_playerBD[ iPartToUse ].iteam
	INT iCurrentRule = mc_serverBD_4.iCurrentHighestPriority[ iCurrentTeam ]
	
	IF iCurrentRule < FMMC_MAX_RULES
		INT iDistanceToCheck 	= g_FMMC_STRUCT.sFMMCEndConditions[ iCurrentTeam ].iGPSDrop[ iCurrentRule ]
		
		IF DOES_BLIP_EXIST( DeliveryBlip )
			IF iDistanceTocheck = -1 		// -1 means "always turn GPS off"
			OR SHOULD_CLEAR_DELIVERY_BLIP_ON_SECONDARY_OBJECTIVE_TEXT()
			
				println("[MJM] iDistanceTocheck = -1 ")
				CLEAR_BIT( iLocalBoolCheck5, LBOOL5_GPS_DELIVER_ROUTE_SET )	// Make sure this is clear so it doesn't effect any other deliver rules
				SET_BLIP_ROUTE( DeliveryBlip, FALSE )
		
			ELIF iDistanceTocheck = 0		// 0 means "always leave the GPS on". This is the default of this setting
			AND NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_GPS_DELIVER_ROUTE_SET ) 
			AND NOT USING_HEIST_SPECTATE()	
			AND LocalPlayerCurrentInterior = NULL
				println("[MJM] iDistanceTocheck = 0 ")
				PRINTLN("[MMacK][GPSRoute] 2")
				SET_BLIP_ROUTE( DeliveryBlip, TRUE )
				SET_BIT( iLocalBoolCheck5, LBOOL5_GPS_DELIVER_ROUTE_SET )
			
			ELIF iDistanceToCheck > 0		// Otherwise the value is the min dist between the team and dest where the GPS should turn off
				println("[MJM] iDistanceToCheck > 0 ")
				PED_INDEX tempped = PlayerPedToUse
				
				FLOAT fDistancecToDestination = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( tempped, GET_DROP_OFF_CENTER_FOR_TEAM( FALSE, iCurrentTeam, iPartToUse ) )
				
				IF FLOOR( fDistancecToDestination ) < iDistanceToCheck
					println("[MJM] FLOOR( fDistancecToDestination ) < iDistanceToCheck ")
					CLEAR_BIT( iLocalBoolCheck5, LBOOL5_GPS_DELIVER_ROUTE_SET )
					SET_BLIP_ROUTE( DeliveryBlip, FALSE )
				
				ELIF NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_GPS_DELIVER_ROUTE_SET ) // Ensure setting the rotue only gets called once
				AND NOT USING_HEIST_SPECTATE()
					println("[MJM] NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_GPS_DELIVER_ROUTE_SET )")
					PRINTLN("[MMacK][GPSRoute] 3")
					SET_BLIP_ROUTE( DeliveryBlip, TRUE )
					SET_BIT( iLocalBoolCheck5, LBOOL5_GPS_DELIVER_ROUTE_SET )
				ENDIF
			ENDIF // End of the GPS route type check 
			
			// if the delivery blip is on a vehicle check if it's got the correct sprite and correct heading etc.			
			UPDATE_DELIVERY_BLIP_SPRITE()
			
		ENDIF // End of the check to see if the blip exists
	ENDIF // End of the check to make sure we only check the rules that exist - array overrun otherwise
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Help Text -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// #####  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_DPAD_SPECIFIC_HELP_TEXT()		

	IF IS_BIT_SET(iLocalBoolCheck,LBOOL_DPAD_HELP_DISP)
		EXIT
	ENDIF
	
	IF MC_serverBD.iTotalNumPart <= 1
		EXIT
	ENDIF
	
	IF bIsAnySpectator
		EXIT
	ENDIF

	IF HAS_FIRST_STRAND_MISSION_BEEN_PASSED() 
	OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
		EXIT
	ENDIF
	
	IF IS_INTRO_CUTSCENE_RULE_ACTIVE()
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		EXIT
	ENDIF
	
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		EXIT
	ENDIF

	IF IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciHWN_PRIORTISE_HELPTEXT) 
	AND NOT IS_BIT_SET( iLocalBoolCheck13, LBOOL13_PRIORTISED_FVJ_HELPTEXT )
		IF MC_PlayerBD[iPartToUse].iteam = 0
			// "Press ~INPUT_CONTEXT~ when aiming to turn the shotgun's flashlight on and off."
			PRINT_HELP("Lowrider_Help_21")
		ELSE
			// "Turn the flashlight on by aiming with ~INPUT_AIM~."
			IF NOT IS_CELLPHONE_CAMERA_IN_USE()
				PRINTLN("Printing Flashlight Help (2)")
				PRINT_HELP("Lowrider_Help_25")
			ENDIF
		ENDIF
		
		SET_BIT( iLocalBoolCheck13, LBOOL13_PRIORTISED_FVJ_HELPTEXT )
	ELIF IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciPRIORTISE_THANKSGIVING_HELPTEXT) 
	AND NOT IS_BIT_SET( iLocalBoolCheck13, LBOOL13_PRIORTISED_THANKSGIVING_HELPTEXT )
	
		// "Keep moving or you will become visible on the Radar to other players. When visible, your Radar arrow will turn red."
		PRINT_HELP("Lowrider_Help_28")
		SET_BIT( iLocalBoolCheck13, LBOOL13_PRIORTISED_THANKSGIVING_HELPTEXT )
	ELSE
		IF IS_BIT_SET(MC_serverBD.iOptionsMenuBitSet,ciNO_POINTS_HUD)
			// "~s~Press ~INPUT_FRONTEND_DOWN~ to view the players on the current Job."
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciSKIP_LEADERBOARD_HELP_TEXT)
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_SimulateSinglePlayerHUD)
				PRINT_HELP("MC_SCORE_H1")
			ENDIF
		ELSE
			// "~s~Press ~INPUT_FRONTEND_DOWN~ to view players on the job."
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciSKIP_PLAYERS_ON_JOB_HELP)
				PRINT_HELP("MC_SCORE_H")
			ENDIF
		ENDIF
		
		SET_BIT(iLocalBoolCheck,LBOOL_DPAD_HELP_DISP)
	ENDIF
	
ENDPROC

PROC PROCESS_HIDE_SPECIFIC_HELP_TEXT_IN_PHONE()
	IF IS_CELLPHONE_CAMERA_IN_USE()
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OPPT_HELP_1")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OPPT_HELP_2")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OPPT_HELP_3")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OPPT_HELP_4")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_SCORE_H1")
			HIDE_HELP_TEXT_THIS_FRAME()
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_SHOW_AGGRO_HELP_TEXT(BOOL &bPedInjured)
	IF MC_serverBD_2.iAggroText_PartCausingSpook != -1
	AND MC_serverBD_2.iAggroText_PedSpooked != -1
	AND MC_serverBD_2.iAggroText_SpookReason != -1
		IF HAS_TEAM_TRIGGERED_AGGRO(MC_playerBD[iLocalPart].iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD_2.iAggroText_PedSpooked].iAggroIndexBS_Entity_Ped)
		AND FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAggroedBitset, MC_serverBD_2.iAggroText_PedSpooked)
			bPedInjured = IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[MC_serverBD_2.iAggroText_PedSpooked]) 
			IF NOT bPedInjured
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SHOW_PED_AGGRO_HELP_TEXT()
	INT iTeam = MC_playerBD[iLocalPart].iTeam
	INT iCurrentRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]
	
	IF iCurrentRule > -1 AND iCurrentRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iCurrentRule], ciBS_RULE13_RULE_SHOW_PLAYER_SPOTTED_HELPTEXT)
			
			BOOL bPedInjured = FALSE
			IF SHOULD_SHOW_AGGRO_HELP_TEXT(bPedInjured)
			
				IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_AGGRO_HELPTEXT_PLAYED)
					IF MC_serverBD_2.iAggroText_SpookReason = ciSPOOK_SEEN_BODY
						PRINTLN("[ML][PED_BODY_SPOTTED_HELP_TEXT] SHOW_PED_AGGRO_HELP_TEXT - Ped spotted corpse")	
						PRINT_HELP("SPOTEDPEDB", 5000)
						SET_BIT(iLocalBoolCheck31, LBOOL31_AGGRO_HELPTEXT_PLAYED)
						CLEAR_AGGRO_HELP_TEXT_DATA()
					ENDIF
					
					IF MC_serverBD_2.iAggroText_SpookReason = ciSPOOK_NONE
					OR MC_serverBD_2.iAggroText_SpookReason = ciSPOOK_PLAYER_HEARD
						PARTICIPANT_INDEX partTriggerer = INT_TO_PARTICIPANTINDEX(MC_serverBD_2.iAggroText_PartCausingSpook)
						PLAYER_INDEX piTriggerer = NETWORK_GET_PLAYER_INDEX(partTriggerer)	
						PRINT_HELP_WITH_PLAYER_NAME("SPOTEDPEDC", GET_PLAYER_NAME(piTriggerer), HUD_COLOUR_WHITE, 5000)
						SET_BIT(iLocalBoolCheck31, LBOOL31_AGGRO_HELPTEXT_PLAYED)
						CLEAR_AGGRO_HELP_TEXT_DATA()
						PRINTLN("[ML][PED_BODY_SPOTTED_HELP_TEXT] SHOW_PED_AGGRO_HELP_TEXT  Player seen or heard")
					ENDIF
				ENDIF
			ELSE
				IF bIsLocalPlayerHost
				AND MC_serverBD_2.iAggroText_PedSpooked != -1
					IF bPedInjured
						CLEAR_AGGRO_HELP_TEXT_DATA()
					ENDIF
				ENDIF
			ENDIF
		ELIF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_AGGRO_HELPTEXT_PLAYED)
			CLEAR_BIT(iLocalBoolCheck31, LBOOL31_AGGRO_HELPTEXT_PLAYED)
			CLEAR_AGGRO_HELP_TEXT_DATA()
		ENDIF
	
	ENDIF
ENDPROC


/// PURPOSE: Displays a help message explaining how missions that use lives work, will only display once (sets a stat bool).
///    The content of the message is "You will fail if all the players on your team are killed when there are no team lives left."
PROC HANDLE_LIVES_HELP()

	IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_DISPLAYED_LIVES_HELP)
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_BlockLivesHelptext)
			IF MC_serverBD.iNumberOfPlayingPlayers[MC_PlayerBD[iLocalPart].iTeam] > 1
				PRINT_HELP("LIVES_HLP")
			ELSE
				PRINT_HELP("LIVES_HLPS")
			ENDIF
			SET_PACKED_STAT_BOOL(PACKED_MP_DISPLAYED_LIVES_HELP,TRUE)
		ENDIF
	ENDIF

ENDPROC

PROC MAINTAIN_HANDLER_HELP_TEXT()

	IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_HANDLER_HELP_TEXT_NEEDED)
	
		INT 			iStatInt
		VEHICLE_INDEX 	veh
		
		IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_HANDLER4)
		
			iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT4)
			
			IF NOT IS_BIT_SET(iStatInt, biNmh4_Handler)			
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT GET_IS_LEADERBOARD_CAM_ACTIVE_OR_INTERPING()
						IF bLocalPlayerPedOk
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
									veh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
									IF GET_ENTITY_MODEL(veh) = HANDLER
										IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_HANDLER1)
											PRINT_HELP("FM_IHELP_HND1") // Position the container handler over a container using ~INPUT_SCRIPT_LEFT_AXIS_Y~.
											SET_BIT(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_HANDLER1)
											PRINTLN("[MAINTAIN_HANDLER_HELP_TEXT] [dsw] Set BI_FM_NMH5_HANDLER1 ")
										ELIF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_HANDLER2)
											PRINT_HELP("FM_IHELP_HND2") // Press ~INPUT_VEH_HEADLIGHT~ to pick up the container.
											SET_BIT(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_HANDLER2)
											PRINTLN("[MAINTAIN_HANDLER_HELP_TEXT] [dsw] Set BI_FM_NMH5_HANDLER2 ")
										ELIF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_HANDLER3)
											PRINT_HELP("FM_IHELP_HND3") // Once the handler frame is lowered press ~INPUT_VEH_HEADLIGHT~ to release the container.
											SET_BIT(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_HANDLER3)
											PRINTLN("[MAINTAIN_HANDLER_HELP_TEXT] [dsw] Set BI_FM_NMH5_HANDLER3 ")
										ELIF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_HANDLER4)
											PRINT_HELP("FM_IHELP_HND4") // Use ~INPUT_SCRIPT_LEFT_AXIS_Y~ to move the handler frame.
											SET_BIT(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_HANDLER4)
											SET_BIT(iStatInt, biNmh4_Handler)
											SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT4, iStatInt)
											PRINTLN("[MAINTAIN_HANDLER_HELP_TEXT] [dsw] Set BI_FM_NMH5_HANDLER4 ")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF		
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Shards/Center Text -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// #####  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//[FMMC_2020] - Seems odd that this needs called every frame for the first staggered part loop. Reduce this?
PROC INIT_RADAR_ZOOM_LEVEL()

	FLOAT fPlayerDist

	INT iParticipant = -1
	WHILE DO_PARTICIPANT_LOOP(iParticipant, DPLF_CHECK_PLAYER_OK | DPLF_FILL_PLAYER_IDS)
	
		fPlayerDist = GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, piParticipantLoop_PedIndex)
		IF fPlayerDist > fFurthestTargetDistTemp
			fFurthestTargetDistTemp = fPlayerDist
			fOldFurthestTargetDist = fPlayerDist
			fFurthestTargetDist = fPlayerDist
			PRINTLN("INIT_RADAR_ZOOM_LEVEL, CONTROL_RADAR_ZOOM, fFurthestTargetDistTemp = ", fFurthestTargetDistTemp)
			PRINTLN("INIT_RADAR_ZOOM_LEVEL, CONTROL_RADAR_ZOOM, fOldFurthestTargetDist = ", fOldFurthestTargetDist)
			PRINTLN("INIT_RADAR_ZOOM_LEVEL, CONTROL_RADAR_ZOOM, fFurthestTargetDist = ", fFurthestTargetDist)
		ENDIF
		
	ENDWHILE
	
ENDPROC

FUNC TEXT_LABEL_15 GET_CREATOR_NAME_TEXT_LABEL_15_FOR_SHARD(INT iTeam)

	TEXT_LABEL_15 tlTeamName = "COR_TEAM_"
	tlTeamName += g_FMMC_STRUCT.iTeamNames[iTeam]
	
	
	
	IF g_FMMC_STRUCT.iTeamNames[iTeam] = ciTEAM_NAME_MAX
		RETURN g_FMMC_STRUCT.tlCustomName[iTeam]
	ENDIF
	
	RETURN tlTeamName

ENDFUNC

FUNC STRING GET_CREATOR_NAME_STRING_FOR_SHARD(INT iTeam)

	STRING sTeamName
	TEXT_LABEL_63 tl63TeamName = "COR_TEAM_"

	tl63TeamName += g_FMMC_STRUCT.iTeamNames[iTeam]
		
	IF g_FMMC_STRUCT.iTeamNames[iTeam] = ciTEAM_NAME_MAX
		TEXT_LABEL_63 originalString
		TEXT_LABEL_15 tl15 = "FMMC_MN"
		
		originalString = g_FMMC_STRUCT.tlCustomName[iTeam]
		PRINTLN("[MMacK][TeamName] g_FMMC_STRUCT.tlCustomName[iTeam] = ", g_FMMC_STRUCT.tlCustomName[iTeam])
		RETURN GET_CROPPED_LITERAL_STRING_FOR_MENU(tl15, originalString, 1.0)
	ENDIF
	
	sTeamName = GET_FILENAME_FOR_AUDIO_CONVERSATION(tl63TeamName)
	
	PRINTLN("[MMacK][TeamName] iTeam = ", iTeam)
	PRINTLN("[MMacK][TeamName] g_FMMC_STRUCT.iTeamNames[iTeam] = ", g_FMMC_STRUCT.iTeamNames[iTeam])
	
	RETURN sTeamName
	
ENDFUNC

/// PURPOSE:
///    This is the function that draws the intro shard for VS missions
PROC SHOW_TEAM_SHARD_AT_START_OF_VS_MISSION()
	
	TEXT_LABEL_31 sTeamName
	TEXT_LABEL_63 sCustomMessage
	INT iTeam = MC_playerBD[iLocalPart].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam] 

	IF g_bVSMission
	OR IS_FAKE_MULTIPLAYER_MODE_SET()
		
		IF HAS_TEAM_GOT_CREATOR_NAME(iTeam) //iLocalPart
			sTeamName = GET_CREATOR_NAME_STRING_FOR_SHARD(iTeam)
		ENDIF
		
		PRINTLN("[RCC MISSION] SHOW_TEAM_SHARD_AT_START_OF_VS_MISSION - sTeamName = ", sTeamName)
		PRINTLN("[RCC MISSION] SHOW_TEAM_SHARD_AT_START_OF_VS_MISSION - g_sMission_TeamName 0 = ", g_sMission_TeamName[0])
		PRINTLN("[RCC MISSION] SHOW_TEAM_SHARD_AT_START_OF_VS_MISSION - g_sMission_TeamName 1 = ", g_sMission_TeamName[1])
		PRINTLN("[RCC MISSION] SHOW_TEAM_SHARD_AT_START_OF_VS_MISSION - g_sMission_TeamName 2 = ", g_sMission_TeamName[2])
		PRINTLN("[RCC MISSION] SHOW_TEAM_SHARD_AT_START_OF_VS_MISSION - g_sMission_TeamName 3 = ", g_sMission_TeamName[3])
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sTeamName)
			PRINTLN("[RCC MISSION] SHOW_TEAM_SHARD_AT_START_OF_VS_MISSION - sTeamName = ", sTeamName)
			
			IF iRule < FMMC_MAX_RULES
				sCustomMessage = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63VsShardMessage[iRule]
			ENDIF
			
			TEXT_LABEL_15 sTeamLabel = GET_CREATOR_NAME_TEXT_LABEL_15_FOR_SHARD(iTeam)
			STRING sTeamShardLabel = "VS_TEAM_SHARD"
			
			IF g_FMMC_STRUCT.iTeamNames[iTeam] = 24
				sTeamShardLabel = "VS_TEAM_SHARD_A" //Use alternate shard text which doesn't include the word "team" at the end, because the team name already says "team"
				PRINTLN("SHOW_TEAM_SHARD_AT_START_OF_VS_MISSION - Using alternate shard text because g_FMMC_STRUCT.iTeamNames[iTeam] is ", g_FMMC_STRUCT.iTeamNames[iTeam])
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sCustomMessage)
				
				PRINTLN("[RCC MISSION] SHOW_TEAM_SHARD_AT_START_OF_VS_MISSION - Using custom shard message = ",sCustomMessage)
				SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_GENERIC_TEXT, "FMMC_MN", sCustomMessage, DEFAULT, DEFAULT, TEXT_LABEL_TO_STRING(sTeamLabel))
			ELIF iRule < FMMC_MAX_RULES
			AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule] ,ciBS_RULE4_DISPLAY_TEAM_REMINDER)
			AND NOT bIsAnySpectator
				
				PLAYER_INDEX piPlayerName[3]
				INT iParticipant
				INT iCurrPlayer = 0
				
				PRINTLN("[PLAYER_LOOP] - SHOW_TEAM_SHARD_AT_START_OF_VS_MISSION")
				FOR iParticipant = 0 TO (MAX_NUM_MC_PLAYERS - 1)
					PARTICIPANT_INDEX partId 	= INT_TO_PARTICIPANTINDEX(iParticipant)
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE( partId )
						
						PLAYER_INDEX 	playerId 	= NETWORK_GET_PLAYER_INDEX( partId )
						INT				iPlayerTeam = GET_PLAYER_TEAM( playerID )
						
						IF iPlayerTeam = iTeam
						AND playerID != LocalPlayer
						AND iCurrPlayer < 3 // We can only display 3 people on the shard!
							piPlayerName[iCurrPlayer] = playerId
							iCurrPlayer++
							
							IF iCurrPlayer >= 3
								BREAKLOOP
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				
				PRINTLN("[RCC MISSION] SHOW_TEAM_SHARD_AT_START_OF_VS_MISSION - Using Team Reminder shard, number of other players to display = ",iCurrPlayer)
				HUD_COLOURS hcTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, LocalPlayer)
				
				SWITCH iCurrPlayer
					CASE 3
						SETUP_NEW_BIG_MESSAGE_WITH_3_PLAYERS(BIG_MESSAGE_GENERIC_TEXT, piPlayerName[0], piPlayerName[1], piPlayerName[2], DEFAULT, "MULTIPLAY_VS3", TEXT_LABEL_TO_STRING(sTeamLabel), hcTeamColour, 5000)
					BREAK
					CASE 2
						SETUP_NEW_BIG_MESSAGE_WITH_2_PLAYERS(BIG_MESSAGE_GENERIC_TEXT, piPlayerName[0], piPlayerName[1], DEFAULT, "MULTIPLAY_VS2", TEXT_LABEL_TO_STRING(sTeamLabel), hcTeamColour, 5000)
					BREAK
					CASE 1
						SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GENERIC_TEXT, piPlayerName[0], DEFAULT, "MULTIPLAY_VS1", TEXT_LABEL_TO_STRING(sTeamLabel), hcTeamColour, 5000)
					BREAK
					CASE 0
						SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GENERIC_TEXT, TEXT_LABEL_TO_STRING(sTeamLabel), sTeamShardLabel, TEXT_LABEL_TO_STRING(sTeamLabel), DEFAULT, DEFAULT, DEFAULT, DEFAULT, hcTeamColour)
					BREAK
				ENDSWITCH
				
			ELSE
				PRINTLN("[RCC MISSION] SHOW_TEAM_SHARD_AT_START_OF_VS_MISSION - Using generic shard, 'You are on the ~a~ team'")
				SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GENERIC_TEXT, TEXT_LABEL_TO_STRING(sTeamLabel), sTeamShardLabel, TEXT_LABEL_TO_STRING(sTeamLabel), DEFAULT)
			ENDIF
		ENDIF

	ENDIF
ENDPROC

PROC ROLLING_SHARD_SCORE_UPDATE()
	IF MC_serverBD.iTeamScore[0] != iStoreLastTeam0Score OR MC_serverBD.iTeamScore[1] != iStoreLastTeam1Score
		PRINTLN("ROLLING_SHARD_SCORE_UPDATE - Cached score didn't match current score. Updating.")
		UPDATE_STORED_TEAM_SCORE_FOR_SHARD()
	ENDIF
ENDPROC

PROC PROCES_SHARD_TEXT_PLAYED(INT iTeam)
	SET_BIT(iLocalBoolCheck18, LBOOL18_RULE_CHANGE_SHARD_PLAYED_TEAM_0 + iTeam)
	SET_BIT(iLocalBoolCheck18, LBOOL18_RULE_CHANGE_SHARD_FIRST_RULE)
ENDPROC

PROC PROCESS_SHARD_TEXT()
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
			
	IF NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_RULE_CHANGE_SHARD_PLAYED_TEAM_0 + MC_playerBD[iPartToUse].iteam)
	OR HAS_NET_TIMER_STARTED(stDelayShard)
	OR NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_RULE_CHANGE_SHARD_FIRST_RULE)
		IF iRule < FMMC_MAX_RULES
		AND (bLocalPlayerPedOK OR bIsAnySpectator)
			PROCES_SHARD_TEXT_PLAYED(iTeam)
			INT shardText = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iShardText[iRule]
			sLastShardText = GET_FMMC_SHARD_STRING_FROM_CREATOR_INT(shardText)
			
			IF shardText > 0
				//custom shards for different levels
				IF (shardText > ciSTART_CUSTOM_LEVEL_SHARDS_WITH_STRAPLINES
				AND shardText < ciEND_CUSTOM_LEVEL_SHARDS_WITH_STRAPLINES)
				OR (shardText > ciSTART_CUSTOM_TLAD_SHARD_WITH_STRAPLINE
				AND shardText < ciEND_CUSTOM_TLAD_SHARD_WITH_STRAPLINE)
					IF shardText = ciVEHICLE_SWAP_SHARD_TEXT
						IF MC_serverBD_4.sVehicleSwaps[iTeam][iRule].mnVehicleModelSwap != DUMMY_MODEL_FOR_SCRIPT
						AND NOT g_bMissionEnding
						AND NOT g_bMissionOver
							SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_GENERIC_TEXT, "STRING", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].mnVehicleModelSwap[MC_serverBD_4.iCurrentHighestPriority[iTeam]]), DEFAULT, DEFAULT, "SHD_NVEH")
						ENDIF
					ELSE
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, GET_FMMC_SHARD_STRING_FROM_CREATOR_INT(shardText), GET_FMMC_SHARD_STRAPLINE_FROM_CREATOR_INT(shardText))
					ENDIF
				ELIF shardText >= ciEND_CUSTOM_LEVEL_SHARDS_WITH_STRAPLINES
				AND shardText <= ciEND_CUSTOM_LEVEL_SHARD_TEXT
				OR shardText = ci10_CUSTOM_LEVEL_SHARD_TEXT
					IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
					AND NOT HAS_TEAM_FINISHED(iTeam)
					AND NOT HAS_TEAM_FAILED(iTeam)
					AND NOT HAS_MULTIRULE_TIMER_EXPIRED(iTeam)
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT,  GET_FMMC_SHARD_STRING_FROM_CREATOR_INT(shardText))
						PRINTLN("[RCC MISSION] Playing shard Level Up/Progression shard")
					ENDIF
				ELSE
					RESET_NET_TIMER(stDelayShard) //This is a bypass for the new rule check, so shouldn't run unless we are doing the above option.
					PRINTLN("[RCC MISSION] PROCESS_SHARD_TEXT - Displaying shard ",shardText," for team ",iTeam," / rule ",iRule)
					
					IF IS_THIS_ROCKSTAR_MISSION_NEW_HALLOWEEN_ADVERSARY_2022(g_FMMC_STRUCT.iAdversaryModeType)
					AND shardText = 4
					AND NOT IS_PART_WAITING_IN_SPECTATE(iLocalPart)
						IF MC_playerBD[iLocalPart].iTeam = ciHALLOWEEN_ADVERSARY_HUNTED_TEAM
							//Hunted
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "GR_SUDDEN", "GR_SUDDEN_HU")
						ELSE
							//Hunter
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "GR_SUDDEN", "GR_SUDDEN_GR")
						ENDIF
					ELSE
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, GET_FMMC_SHARD_STRING_FROM_CREATOR_INT(shardText))
					ENDIF
					
				ENDIF
			ENDIF		
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSHOW_LAST_DELIVERER_SHARD)
	
		IF piLastDeliverer != INVALID_PLAYER_INDEX()
		AND ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
			PRINTLN("[KH] PROCESS_SHARD_TEXT - STARTING TIMER")
			IF NOT HAS_NET_TIMER_STARTED(tdRugby_TryShardTimer)
				REINIT_NET_TIMER(tdRugby_TryShardTimer)
			ENDIF
			
			IF piLastDeliverer = LocalPlayer
				IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_YOU_ARE_THE_LAST_DELIVERER)
					SET_BIT(iLocalBoolCheck16, LBOOL16_YOU_ARE_THE_LAST_DELIVERER)
				ENDIF
			ENDIF
			
			piLastDeliverer = INVALID_PLAYER_INDEX()
		ENDIF
		
		IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
		AND IS_BIT_SET(iLocalBoolCheck29, LBOOL29_POST_SHARD_SCORE_UPDATE)
			ROLLING_SHARD_SCORE_UPDATE()
			CLEAR_BIT(iLocalBoolCheck29, LBOOL29_POST_SHARD_SCORE_UPDATE)
		ENDIF

		IF piLastDeliverer != INVALID_PLAYER_INDEX()
		AND (MC_serverBD.iTeamScore[0] != (iStoreLastTeam0Score) OR MC_serverBD.iTeamScore[1] != (iStoreLastTeam1Score))
			
			PRINTLN("[RCC MISSION] PROCESS_SHARD_TEXT Score difference.")
			
			IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
			OR bShownScoreShardInSuddenDeath = FALSE
			
				PRINTLN("[RCC MISSION] Not in SD.")
				
				IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
											
					piLastDeliverer = INVALID_PLAYER_INDEX()
					UPDATE_STORED_TEAM_SCORE_FOR_SHARD()
						
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC REQUEST_COUNTDOWN_UI(MISSION_INTRO_COUNTDOWN_UI &uiToRequest)
	uiToRequest.uiCountdown = REQUEST_SCALEFORM_MOVIE("COUNTDOWN")
ENDPROC

PROC CLEAN_COUNTDOWN(MISSION_INTRO_COUNTDOWN_UI &uiToUpdate)
	uiToUpdate.iBitFlags = 0
	CANCEL_TIMER(uiToUpdate.CountdownTimer)
ENDPROC

FUNC BOOL IS_COUNTDOWN_OK_TO_PROCEED() 
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_FINISHED_COUNTDOWN)

		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRACE_STYLE_INTRO)		
			PRINTLN("[MMacK][321][IS_COUNTDOWN_OK_TO_PROCEED] ciRACE_STYLE_INTRO SET")
			IF bIsLocalPlayerHost
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_STARTED_COUNTDOWN)

				INT iPlayer = 0
				PRINTLN("[PLAYER_LOOP] - IS_COUNTDOWN_OK_TO_PROCEED")
				FOR iPlayer = 0 TO NUM_NETWORK_PLAYERS-1						
					IF INT_TO_NATIVE(PLAYER_INDEX, iPlayer) != INVALID_PLAYER_INDEX()
					AND NETWORK_IS_PLAYER_ACTIVE(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))
						IF MC_playerBD[iLocalPart].iGameState < GAME_STATE_RUNNING
							PRINTLN("[MMacK][321][IS_COUNTDOWN_OK_TO_PROCEED] - Waiting for iPlayer: ", iPlayer, " | ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayer)))
							RETURN FALSE
						ENDIF
					ENDIF
				ENDFOR
			
				SCRIPT_TIMER sTimerTemp
			
				IF NOT HAS_NET_TIMER_STARTED(sTimerTemp)
					START_NET_TIMER(sTimerTemp) 
				ENDIF
				
				REINIT_NET_TIMER(MC_serverBD.timeServerCountdown)
					
				MC_serverBD.timeServerCountdown = GET_NET_TIMER_OFFSET(sTimerTemp, ciSYNC_RACE_COUNTDOWN_TIMER)
				
				SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_STARTED_COUNTDOWN)
				
				PRINTLN("[MMacK][321][IS_COUNTDOWN_OK_TO_PROCEED] Setting SBBOOL2_STARTED_COUNTDOWN")
			ENDIF
			
			IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_STARTED_COUNTDOWN)
				
				SCRIPT_TIMER sTimerTemp2
				
				IF NOT HAS_NET_TIMER_STARTED(sTimerTemp2)
					START_NET_TIMER(sTimerTemp2) 
					PRINTLN("[MMacK][321][IS_COUNTDOWN_OK_TO_PROCEED], START_NET_TIMER")
				ENDIF
				
				// Reinit multi rule timer...
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_ReinitMultiruleTimersDuringCountdown)
					IF bIsLocalPlayerHost					
					AND IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_MULTIRULE_TIMER_REINIT)
						IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.timeServerCountdown) > -500
							PRINTLN("[LM][321][IS_COUNTDOWN_OK_TO_PROCEED] - ciOptionsBS22_EnableGameMastersMode Resetting Multi Rule Timers for Game Masters Mode.")
							RESET_NET_TIMER(MC_serverBD_3.tdMultiObjectiveLimitTimer[0])
							RESET_NET_TIMER(MC_serverBD_3.tdMultiObjectiveLimitTimer[1])
							START_NET_TIMER(MC_serverBD_3.tdMultiObjectiveLimitTimer[0])
							START_NET_TIMER(MC_serverBD_3.tdMultiObjectiveLimitTimer[1])
							CLEAR_BIT(MC_serverBD.iServerBitSet7, SBBOOL7_MULTIRULE_TIMER_REINIT)
						ELSE
							PRINTLN("[LM][321][IS_COUNTDOWN_OK_TO_PROCEED] - ciOptionsBS22_EnableGameMastersMode Waiting for 1 second on countdown.")
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_MULTIRULE_TIMER_REINIT)
						PRINTLN("[LM][321][IS_COUNTDOWN_OK_TO_PROCEED] - Clearing Sounds.")
						CLEAR_BIT(iLocalBoolCheck3, LBOOL3_MUSIC_PLAYING)
						CLEAR_BIT(iLocalBoolCheck14, LBOOL14_APT_STARTED_PRE_COUNTDOWN)
						CLEAR_BIT(iLocalBoolCheck5, LBOOL5_STARTED_COUNTDOWN_TIMER_SOUNDS)
						CLEAR_BIT(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
						CLEAR_BIT(iLocalBoolCheck14, LBOOL14_COUNTDOWN_EARLY_MISSION_END)	
					ENDIF
				ENDIF
								
				IF IS_NET_TIMER_MORE_THAN(sTimerTemp2, MC_serverBD.timeServerCountdown)

					PRINTLN("[MMacK][321][IS_COUNTDOWN_OK_TO_PROCEED], COUNT_DOWN_HAS_EXPIRED")
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_Reset_321GO_OnRoundRestart)
						RELEASE_COUNTDOWN_UI(cduiIntro)
						CLEAN_COUNTDOWN(cduiIntro)
					ENDIF
					
					IF NOT bIsAnySpectator
						NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
					ENDIF
					
					IF bIsLocalPlayerHost
						RESET_NET_TIMER(MC_serverBD.timeServerCountdown)						
						SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_FINISHED_COUNTDOWN)
					ENDIF
					
					PRINTLN("[MMacK][321][IS_COUNTDOWN_OK_TO_PROCEED], RETURNING TRUE AS TIMER EXPIRED ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
					RETURN TRUE
				ENDIF	
			ENDIF
			
			RETURN FALSE
		ENDIF
	ELSE
		PRINTLN("[MMacK][321][IS_COUNTDOWN_OK_TO_PROCEED], RETURNING TRUE AS SBBOOL2_FINISHED_COUNTDOWN IS SET ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_COUNTDOWN_NUMBER(MISSION_INTRO_COUNTDOWN_UI &uiToSet, INT iSecond)
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(uiToSet.uiCountdown, "SET_MESSAGE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")
			ADD_TEXT_COMPONENT_INTEGER(ABSI(iSecond))
		END_TEXT_COMMAND_SCALEFORM_STRING()
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iR)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iG)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iB)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SHOW_GO_SPLASH(MISSION_INTRO_COUNTDOWN_UI &uiToUpdate)
	IF NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_Go)
	AND HAS_SCALEFORM_MOVIE_LOADED(uiToUpdate.uiCountdown)
		PRINTLN("[MMacK][321GO] Played Go UNNATURAL")
		SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_Go)

		INT iR, iG, iB, iA
		GET_HUD_COLOUR(HUD_COLOUR_GREEN, iR, iG, iB, iA)
		
		BEGIN_SCALEFORM_MOVIE_METHOD(uiToUpdate.uiCountdown, "SET_MESSAGE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CNTDWN_GO")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iR)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iG)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iB)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		END_SCALEFORM_MOVIE_METHOD()
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciRACE_BOOST_START) 
			IF NOT HAS_NET_TIMER_STARTED(g_FMMC_STRUCT.stBoostTimer)
				PRINTLN("[JS] SHOW_GO_SPLASH - Boost timer")
				START_NET_TIMER(g_FMMC_STRUCT.stBoostTimer)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciENABLE_GO_AIRHORN)
			PLAY_SOUND_FRONTEND(-1, "Airhorn", "DLC_TG_Running_Back_Sounds" , FALSE)
		ENDIF
				
		IF bLocalPlayerPedOK
		AND NOT bIsAnySpectator
			NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC DISPLAY_INTRO_COUNTDOWN(MISSION_INTRO_COUNTDOWN_UI &uiToUpdate)
	IF HAS_SCALEFORM_MOVIE_LOADED(uiToUpdate.uiCountdown)
		IF NOT IS_TIMER_STARTED(uiToUpdate.CountdownTimer)
			RESTART_TIMER_NOW(uiToUpdate.CountdownTimer)
		ENDIF
		CLEAR_BIT(iLocalBoolCheck13, LBOOL13_DISPLAYED_INTRO_SHARD)
		
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(uiToUpdate.uiCountdown, 255, 255, 255, 100)
		
		INT iTimerVal = FLOOR(GET_TIMER_IN_SECONDS(uiToUpdate.CountdownTimer))
		INT iSeconds = ABSI(iTimerVal - 3)
		
		IF IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_1)
			IF uiToUpdate.iFrameCount >= 5
				IF NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI_SoundGo)
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciENABLE_GO_AIRHORN)
						PLAY_SOUND_FRONTEND(-1, "GO", "HUD_MINI_GAME_SOUNDSET", FALSE) 
						SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI_SoundGo)
						STOP_STREAM()
					ENDIF
				ENDIF
			ELSE
				uiToUpdate.iFrameCount++
			ENDIF
		ENDIF
		
		IF processDisplacementArenaInteriorMc != NULL
			CALL processDisplacementArenaInteriorMc()
		ENDIF
		
		PRINTLN("[DISPLAY_INTRO_COUNTDOWN] - Attempting to play countdown.")
		
		IF NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_Go)
			IF (iSeconds = 3) AND NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_3)			
				SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_3)
				PLAY_SOUND_FRONTEND(-1, "3_2_1", "HUD_MINI_GAME_SOUNDSET", FALSE)
				SET_COUNTDOWN_NUMBER(uiToUpdate, iSeconds)
				IF NOT bIsAnySpectator
					NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
				ENDIF
			ELIF (iSeconds = 2) AND NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_2)
				SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_2)
				PLAY_SOUND_FRONTEND(-1, "3_2_1", "HUD_MINI_GAME_SOUNDSET", FALSE)			
				SET_COUNTDOWN_NUMBER(uiToUpdate, iSeconds)
				IF NOT bIsAnySpectator
					NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
				ENDIF
			ELIF (iSeconds = 1) AND NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_1)
				SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_1)
				PLAY_SOUND_FRONTEND(-1, "3_2_1", "HUD_MINI_GAME_SOUNDSET", FALSE)
				SET_COUNTDOWN_NUMBER(uiToUpdate, iSeconds)
				IF NOT bIsAnySpectator
					NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
				ENDIF
			ELIF (iSeconds = 0) AND NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_Go)
				SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_Go)
				PRINTLN("[MMacK][321GO] Played Go NATURAL")
				INT iR, iG, iB, iA
				GET_HUD_COLOUR(HUD_COLOUR_GREEN, iR, iG, iB, iA)
				
				BEGIN_SCALEFORM_MOVIE_METHOD(uiToUpdate.uiCountdown, "SET_MESSAGE")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CNTDWN_GO")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iR)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iG)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iB)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
				END_SCALEFORM_MOVIE_METHOD()
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciRACE_BOOST_START) 
					IF NOT HAS_NET_TIMER_STARTED(g_FMMC_STRUCT.stBoostTimer)
						PRINTLN("[JS] DISPLAY_INTRO_COUNTDOWN - Boost timer")
						START_NET_TIMER(g_FMMC_STRUCT.stBoostTimer)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciENABLE_GO_AIRHORN)
					PLAY_SOUND_FRONTEND(-1, "Airhorn", "DLC_TG_Running_Back_Sounds" , FALSE)
				ENDIF
				
				IF NOT bIsAnySpectator
					NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[PROCESS_MISSION_COUNTDOWN_TIMER] LIMIT RACE CONROLS - Scaleform movie not loaded.")
	ENDIF
ENDPROC


/// PURPOSE:
///    Creates the custom wasted string or makes sure the custom wasted string is shown correctly
/// PARAMS:
///    iTeam - Takes the victims team to check if they have any custom wasted shards on
PROC PROCESS_PLAY_CUSTOM_SHARD()
	INT i = 0
	FOR i = 0 TO ciMAX_PLAY_SHARD_REQUESTS-1
		IF IS_PLAY_CUSTOM_SHARD_REQUESTED(i)
			
			//Clear all big messages before calling a new wasted shard
			SWITCH sPlayShardData[i].eShardType
				CASE PLAY_SHARD_TYPE_CUSTOM_WASTED
				
					CLEAR_ALL_BIG_MESSAGES()
							
					IF sPlayShardData[i].iKiller >= 0
						STRING sTeamName
						HUD_COLOURS hcKillerTeamColour						
						TEXT_LABEL_15 tlTempTitle
						
						sTeamName = TEXT_LABEL_TO_STRING(g_sMission_TeamName[MC_playerBD[sPlayShardData[i].iKiller].iteam])
						hcKillerTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[sPlayShardData[i].iKiller].iteam, INT_TO_PLAYERINDEX(sPlayShardData[i].iKiller))
						
						IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tlCustomWastedTitle[MC_playerBD[sPlayShardData[i].iVictim].iteam])
							tlTempTitle = g_FMMC_STRUCT.tlCustomWastedTitle[MC_playerBD[sPlayShardData[i].iVictim].iteam]
						ELSE
							tlTempTitle = "RESPAWN_W_MP"
						ENDIF						
						
						SWITCH g_FMMC_STRUCT.iCustomWastedShardType[sPlayShardData[i].iVictimTeam] 
							CASE ciCUSTOM_WASTED_SHARD_NONE
								SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_CUSTOM_FAILED, "",  g_FMMC_STRUCT.tlCustomWastedStrapline[sPlayShardData[i].iVictimTeam], g_FMMC_STRUCT.tlCustomWastedTitle[sPlayShardData[i].iVictimTeam], -1, -1, -1, "")
							BREAK
							CASE ciCUSTOM_WASTED_SHARD_BE_MY_VALENTINE
								IF sPlayShardData[i].iKiller != iLocalPart
								AND sPlayShardData[i].iKiller != -3
									PRINTLN("[KH] -----> CUSTOM WASTED SHARD | TEAM MATE WAS KILLED BY THE ENEMY TEAM:", sPlayShardData[i].iKiller)
									SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_CUSTOM_FAILED, g_FMMC_STRUCT.tlCustomWastedStrapline[sPlayShardData[i].iVictimTeam], sTeamName, hcKillerTeamColour, DEFAULT, tlTempTitle)
								ELSE
									PRINTLN("[KH] -----> CUSTOM WASTED SHARD | TEAM MATE COMMITED SUICIDE/DIDN'T GET KILLED BY THE OTHER TEAM:", sPlayShardData[i].iKiller)
									SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_CUSTOM_FAILED, "", "", tlTempTitle, -1, -1, -1, "")
								ENDIF
							BREAK
							CASE ciCUSTOM_WASTED_SHARD_SUMO
								PRINTLN("[KH] -----> CUSTOM WASTED SHARD | SUMO PRESET | KNOCKED OUT", sPlayShardData[i].iKiller)
								SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_CUSTOM_FAILED, "", "", g_FMMC_STRUCT.tlCustomWastedTitle[sPlayShardData[i].iVictimTeam], -1, -1, -1, "")
							BREAK
							CASE ciCUSTOM_WASTED_HEALTH_DEPLETED
								PRINTLN("[KH] -----> CUSTOM WASTED SHARD | HEALTH 0 PRESET | You ran out of health")
								SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_CUSTOM_FAILED, g_FMMC_STRUCT.tlCustomWastedStrapline[sPlayShardData[i].iVictimTeam], "", DEFAULT, DEFAULT, "RESPAWN_W_MP")
							BREAK
						ENDSWITCH
						
					//If there was no killer - if the player was blown up by an explosion, killed my ambient ped etc.
					ELSE
					
						SWITCH g_FMMC_STRUCT.iCustomWastedShardType[sPlayShardData[i].iVictimTeam]
						CASE ciCUSTOM_WASTED_SHARD_SUMO
							PRINTLN("[KH] -----> CUSTOM WASTED SHARD | SUMO PRESET | KNOCKED OUT", sPlayShardData[i].iKiller)
							SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_CUSTOM_FAILED, "", "", g_FMMC_STRUCT.tlCustomWastedTitle[sPlayShardData[i].iVictimTeam], -1, -1, -1, "")
							BREAK
						CASE ciCUSTOM_WASTED_HEALTH_DEPLETED
							PRINTLN("[KH] -----> CUSTOM WASTED SHARD | HEALTH 0 PRESET | You ran out of health")
							SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_CUSTOM_FAILED, g_FMMC_STRUCT.tlCustomWastedStrapline[sPlayShardData[i].iVictimTeam], "", DEFAULT, DEFAULT, "RESPAWN_W_MP")
							BREAK
						DEFAULT
							PRINTLN("[KH] -----> CUSTOM WASTED SHARD | TEAM MATE KILLED BY WORLD/GENERIC WASTED TEXT:", sPlayShardData[i].iKiller)
							SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_CUSTOM_FAILED, "", "", "RESPAWN_W_MP", -1, -1, -1, "")
							BREAK
						ENDSWITCH
						
					ENDIF					
				BREAK
			ENDSWITCH			
			
			RESET_PLAY_CUSTOM_SHARD_REQUEST_INDEX(i)
		ENDIF
	ENDFOR
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Lower Right HUD ------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_PLAYER_RESPAWN_BAR_HUD(INT iTeam, INT iRule)
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule],ciBS_RULE12_HIDE_RESPAWN_BAR)
		SET_BIT(iLocalBoolCheck30, LBOOL30_HIDDEN_RESPAWN_BAR)
		g_bMissionHideRespawnBar = TRUE		
	ELSE
		IF g_bMissionHideRespawnBar
		AND IS_BIT_SET(iLocalBoolCheck30, LBOOL30_HIDDEN_RESPAWN_BAR)
			CLEAR_BIT(iLocalBoolCheck30, LBOOL30_HIDDEN_RESPAWN_BAR)
			g_bMissionHideRespawnBar = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_EXPLOSION_TIMER_HUD()
	IF iExplodeLocalTimer != -1
		DRAW_GENERIC_TIMER(iExplodeLocalTimer, "SB_OOP" ,0,TIMER_STYLE_DONTUSEMILLISECONDS,-1,PODIUMPOS_NONE,HUDORDER_TOP,FALSE,HUD_COLOUR_RED,HUDFLASHING_NONE,0,FALSE,HUD_COLOUR_RED)
	ENDIF
ENDPROC

FUNC INT GET_TAKE_VALUE_FOR_HUD()
	INT iTeam = MC_playerBD[iLocalPart].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]

	IF (IS_RULE_INDEX_VALID(iRule)
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_OBJ_CARRY_HUD)
	AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjRequiredForRulePass[iRule] != -1)
		IF MC_serverBD.iObjCarrier[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjRequiredForRulePass[iRule]] != -1
			SET_BIT(iLocalBoolCheck34, LBOOL34_MAIN_TARGET_HELD)
		ELSE
			CLEAR_BIT(iLocalBoolCheck34, LBOOL34_MAIN_TARGET_HELD)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck34, LBOOL34_MAIN_TARGET_HELD)
		RETURN GET_TOTAL_CASH_GRAB_TAKE() + GET_MAIN_TARGET_VALUE_FOR_TAKE_HUD()
	ENDIF
	
	RETURN GET_TOTAL_CASH_GRAB_TAKE()
	
ENDFUNC

PROC DRAW_CASH_GRAB_TAKE_HUD()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_HideTakeHUD)
		EXIT
	ENDIF
	
	IF NOT ( g_bMissionEnding OR g_bMissionOver )
		IF g_TransitionSessionNonResetVars.bDisplayCashGrabTake
			SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
			
			IF ( bDrawCashGrabTakeRed )
				DRAW_GENERIC_SCORE(GET_TAKE_VALUE_FOR_HUD(), "MONEY_HELD", 1000, HUD_COLOUR_RED, HUDORDER_DONTCARE, FALSE, "HUD_CASH")
				IF ( iDrawCashGrabTakeRedTime = 0 )
					iDrawCashGrabTakeRedTime = GET_GAME_TIMER()
				ELSE
					IF GET_GAME_TIMER() - iDrawCashGrabTakeRedTime > iDrawCashGrabTakeRedDuration
						bDrawCashGrabTakeRed = FALSE
					ENDIF
				ENDIF
			ELSE
				DRAW_GENERIC_SCORE(GET_TAKE_VALUE_FOR_HUD(), "MONEY_HELD", 1000, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, "HUD_CASH")
			ENDIF
			SET_BIT(g_TransitionSessionNonResetVars.iAirlockHUDBitset, ciFMMCAirlockHUDBS_TakeShown)
			IF IS_BIT_SET(g_TransitionSessionNonResetVars.iAirlockHUDBitset, ciFMMCAirlockHUDBS_ShowTake)
				g_TransitionSessionNonResetVars.iTotalCashGrabTakeForHUD = GET_TOTAL_CASH_GRAB_TAKE()
			ENDIF
		ELSE
			//Lukasz: for spectating players, set the flag if not set once some money is collected, see B*2133931
			IF IS_PLAYER_SPECTATING(LocalPlayer)
				IF GET_TOTAL_CASH_GRAB_TAKE() > 0
					g_TransitionSessionNonResetVars.bDisplayCashGrabTake = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_COKE_GRAB_HUD()
	
	INT iClientStage = GET_MC_CLIENT_MISSION_STAGE(iPartToUse)
	IF iClientStage = CLIENT_MISSION_STAGE_GOTO_OBJ
		g_TransitionSessionNonResetVars.bDrawCokeGrabHud = TRUE
	ENDIF
	
	INT iNumTrolleys = 4
	IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_USED_A_COKE_GRAB_MINIGAME)
		INT iNumParts = NETWORK_GET_NUM_PARTICIPANTS()
		IF iNumParts = 3
			iNumTrolleys = 3
		ENDIF
		iMaxCashLocalPlayerCanCarry = (iNumTrolleys * 450000) / NETWORK_GET_NUM_PARTICIPANTS()
		PRINTLN("[KH] DRAW_COKE_GRAB_HUD - iMaxCashLocalPlayerCanCarry: ", iMaxCashLocalPlayerCanCarry)
	ENDIF
	
	IF NOT g_bMissionEnding
	AND g_TransitionSessionNonResetVars.bDrawCokeGrabHud
		DRAW_GENERIC_METER(GET_TOTAL_CASH_GRAB_TAKE(), (450000 * iNumTrolleys), "DRUG_GRAB", DEFAULT, DEFAULT, HUDORDER_TOP, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
		DRAW_GENERIC_METER(g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed, iMaxCashLocalPlayerCanCarry, "DRUG_GRAB_B", DEFAULT, DEFAULT, HUDORDER_TOP, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
	ENDIF
	
ENDPROC

PROC DISPLAY_PLAYER_KILL_HUD()

	INT itotalplayerkills
	INT iPlayersToKill

	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF bPlayerToUseOK
		AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_FINISHED)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE_HIDE_HUD)
			INT istage = GET_MC_CLIENT_MISSION_STAGE(iPartToUse)
			IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
				DRAW_KILLSTREAK_DISPLAY(GlobalplayerBD_FM[NATIVE_TO_INT(PlayerToUse)].iKillStreak) 
			ENDIF
			
			IF MC_serverBD.iCurrentPlayerRule[MC_playerBD[iPartToUse].iteam] < 0
				EXIT
			ENDIF
			
			IF MC_serverBD.iPlayerRuleLimit[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iPartToUse].iteam]][MC_playerBD[iPartToUse].iteam] != UNLIMITED_CAPTURES_KILLS
				IF istage = CLIENT_MISSION_STAGE_KILL_PLAYERS
					itotalplayerkills = MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartToUse].iteam][0] + MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartToUse].iteam][1] 
					+ MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartToUse].iteam][2] + MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartToUse].iteam][3] 
					iPlayersToKill = MC_serverBD.iPlayerRuleLimit[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iPartToUse].iteam]][MC_playerBD[iPartToUse].iteam] -itotalplayerkills
					IF iPlayersToKill > 0
						DRAW_GENERIC_BIG_NUMBER(iPlayersToKill,"MC_PKILLS")
					ENDIF
				ELIF istage = CLIENT_MISSION_STAGE_KILL_TEAM0
					iPlayersToKill = MC_serverBD.iPlayerRuleLimit[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iPartToUse].iteam]][MC_playerBD[iPartToUse].iteam] -MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartToUse].iteam][0] 
					IF iPlayersToKill > 0
						DRAW_GENERIC_BIG_NUMBER(iPlayersToKill,"MC_PKILLS")
					ENDIF
				ELIF istage = CLIENT_MISSION_STAGE_KILL_TEAM1
					iPlayersToKill = MC_serverBD.iPlayerRuleLimit[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iPartToUse].iteam]][MC_playerBD[iPartToUse].iteam] -MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartToUse].iteam][1] 
					IF iPlayersToKill > 0
						DRAW_GENERIC_BIG_NUMBER(iPlayersToKill,"MC_PKILLS")
					ENDIF
				ELIF istage = CLIENT_MISSION_STAGE_KILL_TEAM2
					iPlayersToKill = MC_serverBD.iPlayerRuleLimit[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iPartToUse].iteam]][MC_playerBD[iPartToUse].iteam] -MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartToUse].iteam][2] 
					IF  iPlayersToKill > 0
						DRAW_GENERIC_BIG_NUMBER(iPlayersToKill,"MC_PKILLS")
					ENDIF
				ELIF istage = CLIENT_MISSION_STAGE_KILL_TEAM3
					iPlayersToKill = MC_serverBD.iPlayerRuleLimit[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iPartToUse].iteam]][MC_playerBD[iPartToUse].iteam] -MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartToUse].iteam][3] 
					IF iPlayersToKill > 0
						DRAW_GENERIC_BIG_NUMBER(iPlayersToKill,"MC_PKILLS")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC DRAW_ENTITY_RESPAWN_RECAPTURE_HUD()

	STRING recaptext

	IF bPlayerToUseOK
	AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_FINISHED)
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE_HIDE_HUD)
				IF MC_serverBD.iRuleReCapture[MC_playerBD[iPartToUse].iteam][MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] > 0
					IF MC_serverBD.iNumLocHighestPriority[MC_playerBD[iPartToUse].iteam] > 0
						recaptext = "MC_URCAPL"
					ELIF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iPartToUse].iteam] > 0
						recaptext = "MC_URCAPP"
					ELIF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iPartToUse].iteam] > 0
						recaptext = "MC_URCAPV"
					ELIF MC_serverBD.iNumOBJHighestPriority[MC_playerBD[iPartToUse].iteam] > 0
						recaptext = "MC_URCAPO"
					ENDIF
					
					IF MC_serverBD.iRuleReCapture[MC_playerBD[iPartToUse].iteam][MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] >= UNLIMITED_CAPTURES_KILLS
						//DRAW_GENERIC_BIG_NUMBER(-1,recaptext,-1,HUD_COLOUR_WHITE,HUDORDER_DONTCARE,FALSE,"",HUD_COLOUR_WHITE,TRUE)
					ELSE
						DRAW_GENERIC_BIG_NUMBER(MC_serverBD.iRuleReCapture[MC_playerBD[iPartToUse].iteam][MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] ],recaptext)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC MAINTAIN_STAMINA_BAR()
	INT iTeam 
	INT iPriority 
	
	IF iPartToUse > -1
	AND iPartToUse < NUM_NETWORK_PLAYERS
		iTeam = MC_playerBD[iPartToUse].iteam
		iPriority = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
	ENDIF

	IF iTeam > -1
	AND iPriority > -1 AND iPriority < FMMC_MAX_RULES
	AND NOT g_bMissionEnding
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iPriority], ciBS_RULE2_SHOW_STAMINA_BAR)
			DRAW_STAMINA_BAR_FOR_PED(LocalPlayerPed, DEFAULT)
		ENDIF
	ENDIF	
ENDPROC

PROC PROCESS_LOCAL_ALTITUDE_HUD()
	
	FLOAT fAltitude
	FLOAT fAltitudeMin
	FLOAT fAltitudeThreshold
	FLOAT fAltitudeMaximum
	INT iFailTime
	FLOAT fFailTimer
	INT iTeam
	INT iRule
	BOOL bIsLiteralString = TRUE
	TEXT_LABEL_63 tl63_Main
	TEXT_LABEL_63 tl63_Cons
	
	HUD_COLOURS eHUDColour	
	HUDORDER eHUDOrder = HUDORDER_SEVENTHBOTTOM		
	 
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)		
		iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule >= FMMC_MAX_RULES
			RELOOP
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iBS, ciRuleMeteredBehaviour_Enabled + iTeam)
			RELOOP
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iBS, ciRuleMeteredBehaviour_BarVisibility_T1 + iTeam)		
			RELOOP
		ENDIF
		
		IF iTeam = MC_PlayerBD[iPartToUse].iTeam
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iBS, ciRuleMeteredBehaviour_RequiresVehicle)
			AND NOT DOES_ENTITY_EXIST(sAltitudeBarLocal.vehToUse)		
				SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
				RELOOP
			ENDIF
		
			IF NOT IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__ALTIMETER_SYSTEM_ZONE)
				RELOOP
			ENDIF
		ENDIF
		
		fAltitude 					= GET_PLAYER_ALTITUDE_FROM_TEAM(iTeam)		
		tl63_Main					= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].tl63_MainBar
		tl63_Cons					= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].tl63_ConsequenceBar
		fAltitudeMin				= TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iMinimum)
		fAltitudeThreshold			= TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iThreshold)
		fAltitudeMaximum			= TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iMaximum)
		iFailTime 					= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iFailTime
		
		INT iBarPercentage, iBarPercentageMax, iBarThreshPercentage
		FLOAT fBarDiff, fBar, fBarMax, fBarThresh
		
		IF fAltitudeMaximum = 0
			IF (fAltitude > fAltitudeMin AND fAltitudeMin > fAltitudeThreshold)
				fAltitude = fAltitudeMin
			ENDIF
		ELIF fAltitudeMaximum != 0
			IF (fAltitude > fAltitudeMin AND fAltitudeMin > fAltitudeMaximum)
				fAltitude = fAltitudeMin
			ENDIF
		ENDIF
		IF fAltitudeMaximum = 0
			IF (fAltitude < fAltitudeMin AND fAltitudeMin < fAltitudeThreshold)
				fAltitude = fAltitudeMin
			ENDIF
		ELIF fAltitudeMaximum != 0
			IF (fAltitude < fAltitudeMin AND fAltitudeMin < fAltitudeMaximum)
				fAltitude = fAltitudeMin
			ENDIF
		ENDIF
		
		IF fAltitudeMin < 0
			fBarDiff = ABSF(fAltitudeMin)
		ELSE
			fBarDiff -= fAltitudeMin
		ENDIF
		
		fBar 				= (fAltitude					+ fBarDiff)
		
		IF fAltitudeMaximum = 0
			fBarMax 		= (fAltitudeThreshold			+ fBarDiff)
		ELIF fAltitudeMaximum != 0
			fBarThresh		= (fAltitudeThreshold			+ fBarDiff)
			fBarMax 		= (fAltitudeMaximum				+ fBarDiff)	
		ENDIF		
				
		fBar 				 = ABSF(fBar)
		fBarMax 			 = ABSF(fBarMax)
		fBarThresh 			 = ABSF(fBarThresh)
		
		iBarPercentage 		 = ROUND((fBar/fBarMax)*100)
		iBarThreshPercentage = ROUND((fBarThresh/fBarMax)*10)*10
		iBarPercentageMax 	 = 100	
				
		IF fAltitudeMaximum != 0
			IF iBarPercentage >= iBarThreshPercentage
				eHUDColour = HUD_COLOUR_RED
			ELIF iBarPercentage >= iBarThreshPercentage-ROUND(TO_FLOAT(iBarThreshPercentage)*0.25)
				eHUDColour = HUD_COLOUR_ORANGE
			ELIF iBarPercentage >= iBarThreshPercentage-ROUND(TO_FLOAT(iBarThreshPercentage)*0.5)
				eHUDColour = HUD_COLOUR_YELLOW	
			ELIF iBarPercentage >= iBarThreshPercentage-ROUND(TO_FLOAT(iBarThreshPercentage)*0.75)
				eHUDColour = HUD_COLOUR_YELLOWLIGHT
			ENDIF
		ELSE
			IF iBarPercentage >= 100
				eHUDColour = HUD_COLOUR_RED
			ELIF iBarPercentage >= 60		
				eHUDColour = HUD_COLOUR_ORANGE
			ELIF iBarPercentage >= 40
				eHUDColour = HUD_COLOUR_YELLOW			
			ELIF iBarPercentage >= 20
				eHUDColour = HUD_COLOUR_YELLOWLIGHT
			ENDIF
		ENDIF
		
		PERCENTAGE_METER_LINE percentLine = GET_PERCENTAGE_METER_LINE_FROM_PERCENTAGE_INT(iBarThreshPercentage)
		
		// Might want to add some Lerp Values here so that iBarPercentage is a cached valued lerped between frames.
		DRAW_GENERIC_METER(iBarPercentage, iBarPercentageMax, tl63_Main, eHUDColour, DEFAULT, eHUDOrder, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0, bIsLiteralString, percentLine)
				
		// Fail Timer ----------------
		IF iFailTime > 0
			fFailTimer = GET_PLAYER_ALTITUDE_FAIL_TIMER_FROM_TEAM(iTeam)		
			fBar = fFailTimer
			fBarMax = TO_FLOAT(iFailTime)
			
			iBarPercentage = ROUND((fBar/fBarMax)*100)
			iBarPercentageMax = 100	
			
			IF fFailTimer > 0
				eHUDColour = HUD_COLOUR_RED
				DRAW_GENERIC_METER(iBarPercentage, iBarPercentageMax, tl63_Cons, eHUDColour, DEFAULT, eHUDOrder, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0, bIsLiteralString)
			ENDIF
		ENDIF
		
		IF iTeam = MC_PlayerBD[iPartToUse].iTeam	
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iBS, ciRuleMeteredBehaviour_DrawAltimeter)
				SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(fAltitude)
			ENDIF
		ENDIF
	ENDFOR
	
	
	// More settings will need to be added so that the player can grab remote player's fFailTimer and fAltitude PlayerBD vars and draw it for remote teams and also do other things here like play SFX.
	/*
		#IF IS_DEBUG_BUILD
		PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("fAltitudeMin: ", "", vBlankDbg, DEFAULT, fAltitudeMin)
		PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("fAltitude: ", "", vBlankDbg, DEFAULT, fAltitude)
		PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("fAltitude: ", "", vBlankDbg, DEFAULT, fAltitudeMaximum)
		PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("iMeterThresh: ", "", vBlankDbg, DEFAULT, fAltitudeThreshold)
		
		PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("----------------------: ", "", vBlankDbg, DEFAULT)
		
		PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("fBarDiff: ", "", vBlankDbg, DEFAULT, fBarDiff)
		PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("fBar: ", "", vBlankDbg, DEFAULT, fBar)		
		PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("fBarThresh: ", "", vBlankDbg, DEFAULT, fBarThresh)
		PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("fBarMax: ", "", vBlankDbg, DEFAULT, fBarMax)
		
		PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("----------------------: ", "", vBlankDbg, DEFAULT)
		
		PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("iBarPercentage: ", "", vBlankDbg, iBarPercentage)
		PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("iBarPercentage: ", "", vBlankDbg, iBarPercentage)
		PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("fBarThreshPercentage: ", "", vBlankDbg, iBarThreshPercentage)
		#ENDIF
		*/
ENDPROC

PROC DRAW_VEHICLE_MIN_SPEED_EXPLOSION_CHECK_HUD()
	
	INT iTeam
	INT iRule
	
	BOOL bIsLiteralString = FALSE
	VEHICLE_INDEX tempVeh
	TEXT_LABEL_63 tl63_Temp
	INT iMinSpeed
	INT iMeterMax
	FLOAT fVehSpeed
	HUDORDER eHUDOrder = HUDORDER_DONTCARE
	HUDORDER eHUDOrder_Explode = HUDORDER_DONTCARE
	STRING sSoundSet = "GTAO_Speed_Race_Sounds"
	
	BOOL bUseAltitude, bUseNegativeSpeedMinValueForAltitude
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		
		iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule < FMMC_MAX_RULES
		
			bUseAltitude = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_USE_ALTITUDE_SPEED_BAR)
			bUseNegativeSpeedMinValueForAltitude = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed[iRule] < 0 AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_MeterMax[iRule] < 0
						
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_SPEEDBAR_T0 + iTeam)
				
				IF IS_PLAYER_IN_CREATOR_TRAILER(LocalPlayer)
				OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerToUse)
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_AIRSTRIKE_ON_SPEED_FAIL)
					AND IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SPEED_BAR_AIRSTRIKE_CALLED)
					AND NOT bDisableExplosionBarKill
						IF HAS_NET_TIMER_STARTED(tdAirstrikeExplodeTimer)
							IF HAS_NET_TIMER_EXPIRED(tdAirstrikeExplodeTimer, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iSpeedFailTimerLength)							
								STOP_SOUND(iSoundIDVehBeacon)
							ENDIF
						ELSE							
							START_NET_TIMER(tdAirstrikeExplodeTimer)
						ENDIF
					ENDIF
				ENDIF
				
				iMinSpeed = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed[iRule] 
				
				iMeterMax = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_MeterMax[iRule]
				
				IF GET_TEAM_VEHICLE_FOR_SPEED_CHECK(iTeam, tempVeh, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_SpecificVehicle[iRule])
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE11_SPEED_BAR_NOT_STEALTHED)
						IF DOES_ENTITY_EXIST(tempVeh)
						AND GET_ENTITY_MODEL(tempVeh) = AKULA
						AND NOT ARE_FOLDING_WINGS_DEPLOYED(tempVeh)
							PRINTLN("[AltitudeSpeedBar] - DRAW_VEHICLE_MIN_SPEED_EXPLOSION_CHECK_HUD - Akula not in stealth mode")
							EXIT
						ENDIF
					ENDIF
					
					BOOL bHasRealSpeed = TRUE
					IF IS_ENTITY_ALIVE(tempVeh) 
						IF VDIST2(GET_ENTITY_COORDS(LocalPlayerPed, FALSE),GET_ENTITY_COORDS(tempVeh, FALSE)) > POW(250,2)
						OR IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR()
							IF IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR()
								PRINTLN("[AltitudeSpeedBar] - Setting bHasRealSpeed false. Inside an interior")
							ELSE
								PRINTLN("[AltitudeSpeedBar] - Setting bHasRealSpeed false. Player far away from vehicle")
							ENDIF
							bHasRealSpeed = FALSE
						ENDIF
					ELSE
						PRINTLN("[AltitudeSpeedBar] - Setting bHasRealSpeed false. Vehicle is dead.")
						bHasRealSpeed = FALSE
					ENDIF
					
					IF bHasRealSpeed
						IF bUseAltitude
							IF IS_VEHICLE_SUPPOSED_TO_BE_IN_WATER(tempVeh)
								fVehSpeed = GET_VEHICLE_ALTITUDE(tempVeh)
							ELSE
								fVehSpeed = GET_PLANE_ALTITUDE(tempVeh)
							ENDIF
						ELSE
							fVehSpeed = ABSF(GET_ENTITY_SPEED(tempVeh))
						ENDIF
						IF bExplodeSpeedUpdateThisFrame
							MC_playerBD[iLocalPart].fBombVehicleSpeed = fVehSpeed
						ENDIF
						fVehSpeedForAudioTrigger = fVehSpeed
					ELSE
						IF NOT (NOT IS_ENTITY_ALIVE(tempVeh)
						AND IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh))
							
							IF iExplodeDriverPart != -1
								fVehSpeed = MC_playerBD[iExplodeDriverPart].fBombVehicleSpeed
							ENDIF
							MC_playerBD[iLocalPart].fBombVehicleSpeed = 0.0
						ENDIF
					ENDIF
										
					PRINTLN("[AltitudeSpeedBar] 3a - iExplodeDriverPart = ",iExplodeDriverPart,", tempVeh = ",NATIVE_TO_INT(tempVeh),", fVehSpeed = ",fVehSpeed)
					
					IF NOT bUseNegativeSpeedMinValueForAltitude
						IF iMeterMax <= 0
							IF tempVeh != NULL
							AND NOT bUseAltitude
								iMeterMax = ROUND(GET_VEHICLE_ESTIMATED_MAX_SPEED(tempVeh))
							ELSE
								iMeterMax = iMinSpeed * 4
							ENDIF
						ENDIF
					ENDIF
					
					PRINTLN("[AltitudeSpeedBar] 3b - iMeterMax = ",iMeterMax," (globals value = ",g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_MeterMax[iRule],")")
					
					IF NOT bUseAltitude
						IF iTeam = MC_playerBD[iPartToUse].iteam
							tl63_Temp = "SPDBR_SPEED"
						ELSE
							PED_INDEX driverPed = GET_PED_IN_VEHICLE_SEAT(tempVeh)
							
							IF (NOT IS_PED_INJURED(driverPed))
							AND IS_PED_A_PLAYER(driverPed)
								PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(driverPed)
								tl63_Temp = GET_PLAYER_NAME(tempplayer)
								bIsLiteralString = TRUE
							ELSE
								IF IS_BIT_SET(iCelebrationTeamNameIsNotLiteral, iTeam)
									tl63_Temp = TEXT_LABEL_TO_STRING(g_sMission_TeamName[iteam])
								ELSE
									tl63_Temp = g_sMission_TeamName[iteam]
									bIsLiteralString = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						tl63_Temp = "SPDBR_ALT"
					ENDIF
					
					//url:bugstar:2503087
					eHUDOrder_Explode = HUDORDER_EIGHTHBOTTOM
					eHUDOrder = HUDORDER_SEVENTHBOTTOM
					
					//Scaling to get the min speed line at a multiple of 10% along the meter (as we've only got textures at 10% intervals)
					INT iLine 
					
					PRINTLN("[AltitudeSpeedBar] (Before) iMeterMax: ", iMeterMax, " iMinSpeed: ", iMinSpeed, " fVehSpeed: ", fVehSpeed)
					
					IF bUseNegativeSpeedMinValueForAltitude
						iMinSpeed = ABSI(iMinSpeed)
						iMeterMax = ABSI(iMeterMax)
						
						iLine = CEIL((TO_FLOAT(iMinSpeed) / TO_FLOAT(iMeterMax)) * 10) * 10 // 0 - 100 in steps of 10
						iMeterMax = CEIL(TO_FLOAT(iMinSpeed) * (100.0 / TO_FLOAT(iLine) ) )
						fVehSpeed = (iMeterMax-ABSF(fVehSpeed))
					ELSE
						iLine = CEIL((TO_FLOAT(iMinSpeed) / TO_FLOAT(iMeterMax)) * 10) * 10 // 0 - 100 in steps of 10
						iMeterMax = CEIL(TO_FLOAT(iMinSpeed) * (100.0 / TO_FLOAT(iLine) ) )
					ENDIF
					
					PERCENTAGE_METER_LINE percentLine = GET_PERCENTAGE_METER_LINE_FROM_PERCENTAGE_INT(ABSI(iLine))
									
					PRINTLN("[AltitudeSpeedBar] (After) iLine: ", iLine, " iMeterMax: ", iMeterMax, " iMinSpeed: ", iMinSpeed, " fVehSpeed: ", fVehSpeed)
					
					FLOAT fBarMax = TO_FLOAT(iMeterMax)
					FLOAT fBarFill
					
					IF fBarMax < 0
						fBarFill = CLAMP(fVehSpeed, fBarMax, 0)
					ELSE
						fBarFill = CLAMP(fVehSpeed, 0, fBarMax)
					ENDIF
					
					PRINTLN("[AltitudeSpeedBar] (After) fBarFill: ", fBarFill)
					
					IF iLastMinSpeed[iTeam] != iMinSpeed
					OR iLastMeterMax[iTeam] != iMeterMax
						fMinSpeedLerpProgress[iTeam] += 2 * fLastFrameTime // 0.5s to lerp
						PRINTLN("[AltitudeSpeedBar] LERPING! fMinSpeedLerpProgress = ", fMinSpeedLerpProgress[iTeam])
						
						IF fMinSpeedLerpProgress[iTeam] >= 1.0
							fMinSpeedLerpProgress[iTeam] = 0
							iLastMinSpeed[iTeam] = iMinSpeed
							iLastMeterMax[iTeam] = iMeterMax
						ELSE
							FLOAT fOldBarFill
							IF TO_FLOAT(iLastMeterMax[iTeam]) < 0
								fOldBarFill = CLAMP(fVehSpeed, TO_FLOAT(iLastMeterMax[iTeam]), 0)
							ELSE
								fOldBarFill = CLAMP(fVehSpeed, 0, TO_FLOAT(iLastMeterMax[iTeam]))
							ENDIF
							
							PRINTLN("[AltitudeSpeedBar] Before: fOldBarFill (bar fill with old meter max) = ",fOldBarFill,", fBarFill (bar fill with new meter max) = ",fBarFill)
							PRINTLN("[AltitudeSpeedBar] Before: iLastMeterMax (old meter max) = ",iLastMeterMax[iTeam],", fBarMax (new meter max) = ",fBarMax)
							
							fBarFill = LERP_FLOAT(fOldBarFill, fBarFill, fMinSpeedLerpProgress[iTeam])
							fBarMax = LERP_FLOAT(TO_FLOAT(iLastMeterMax[iTeam]), fBarMax, fMinSpeedLerpProgress[iTeam])
							
							PRINTLN("[AltitudeSpeedBar] After: fBarFill = ",fBarFill,", fBarMax = ",fBarMax)
						ENDIF
					ENDIF
					
					HUD_COLOURS eHUDColour
					BOOL bProcessExplosionProgress
					
					IF NOT bUseAltitude
						IF fVehSpeed <= iMinSpeed
							bProcessExplosionProgress = TRUE
						ENDIF
					ENDIF
					
					IF (bUseAltitude AND fVehSpeed >= iMinSpeed)
						bProcessExplosionProgress = TRUE
					ENDIF
										
					IF bProcessExplosionProgress
						SET_BIT(iLocalBoolCheck27, LBOOL27_FLYING_TOO_HIGH)
						eHUDColour = HUD_COLOUR_RED
					ELSE
						CLEAR_BIT(iLocalBoolCheck27, LBOOL27_FLYING_TOO_HIGH)
						eHUDColour = HUD_COLOUR_GREEN
					ENDIF
					
					PRINTLN("[AltitudeSpeedBar] (draw time!) - ROUND(fBarFill * 100) = ",ROUND(fBarFill * 100),", ROUND(fBarMax * 100) = ",ROUND(fBarMax * 100))
					
					BOOL bDrawAltimeter = bUseAltitude
					
					VEHICLE_INDEX tempSpecificVeh
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_SpecificVehicle[iRule] > -1
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_SpecificVehicle[iRule]])
							tempSpecificVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_SpecificVehicle[iRule]])
							
							IF IS_PED_IN_VEHICLE(LocalPlayerPed, tempSpecificVeh)
								bDrawAltimeter = TRUE
								PRINTLN("[AltitudeSpeedBar] bDrawAltimeter to TRUE because we're in the specific vehicle")
							ELSE
								bDrawAltimeter = FALSE
								PRINTLN("[AltitudeSpeedBar] bDrawAltimeter to FALSE because we're not in the specific vehicle")
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						bDrawAltimeter = FALSE
						SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
						PRINTLN("[AltitudeSpeedBar] bDrawAltimeter to FALSE because we're not in ANY vehicle")
					ENDIF
					
					IF NOT bDrawAltimeter
						SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
					ELSE												
						IF DOES_ENTITY_EXIST(tempVeh)
							PRINTLN("[AltitudeSpeedBar] Drawing SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT with min speed of ", TO_FLOAT(iMinSpeed))
							
							IF IS_VEHICLE_SUPPOSED_TO_BE_IN_WATER(tempVeh)
								iMinSpeed = ROUND(GET_VEHICLE_ALTITUDE(tempVeh))
							ELSE
								iMinSpeed = ROUND(GET_PLANE_ALTITUDE(tempVeh))
							ENDIF
							
							IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							AND (IS_PED_IN_ANY_HELI(LocalPlayerPed) OR IS_PED_IN_ANY_PLANE(LocalPlayerPed) OR IS_VEHICLE_SUPPOSED_TO_BE_IN_WATER(tempVeh))
								IF TO_FLOAT(iMinSpeed) < 0
									SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0) // Does not currently work below 0, need code support.
								ELSE
									SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(TO_FLOAT(iMinSpeed), TRUE)
								ENDIF
								
								bShowingAltitudeLimit = TRUE
							ELSE
								SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
								bShowingAltitudeLimit = FALSE
							ENDIF	
						
							PRINTLN("[AltitudeSpeedBar] fBarFill: ", fBarFill)
							PRINTLN("[AltitudeSpeedBar] fBarMax: ", fBarMax)			
						ELSE
							SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
							PRINTLN("[AltitudeSpeedBar] Not drawing altimeter because the vehicle we want to base it on doesn't exist")
						ENDIF
					ENDIF
					
					// Have an option (if needed) that prevents showing it as driver - As that was old/legacy controller functionality. Based on presumptions.
					IF IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerToUse)
					OR (DOES_ENTITY_EXIST(tempVeh) AND IS_PED_IN_VEHICLE(PlayerPedToUse, tempVeh))
						IF bUseNegativeSpeedMinValueForAltitude
							DRAW_GENERIC_METER(ROUND(fBarFill * 100), ROUND(fBarMax * 100), tl63_Temp, eHUDColour, DEFAULT, eHUDOrder, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0, bIsLiteralString, percentLine)
						ELSE
							DRAW_GENERIC_METER(ROUND(fBarFill * 100), ROUND(fBarMax * 100), tl63_Temp, eHUDColour, DEFAULT, eHUDOrder, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0, bIsLiteralString, percentLine)
						ENDIF
					ENDIF
									
					IF iExplodeDriverPart = iLocalPart
					OR IS_VEHICLE_A_TRAILER(tempSpecificVeh)
					OR ((iSpectatorTarget != -1) AND (iExplodeDriverPart = iSpectatorTarget))
					OR (iExplodeDriverPart != -1 AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_SpecificVehicle[iRule] != -1 AND MC_playerBD[iExplodeDriverPart].iteam = MC_playerBD[iLocalPart].iteam)
						///EXPLODE BAR:
						
						FLOAT fBDProgress
						BOOL bInDangerZone = FALSE
						
						IF iExplodeDriverPart != -1
							fBDProgress =  TO_FLOAT(MC_playerBD[iExplodeDriverPart].iMinSpeed_ExplodeProgress)
						ELSE
							fBDProgress = fRealExplodeProgress
						ENDIF	
						
						IF SHOULD_USE_LOCAL_MINIMUM_VEH_SPEED_CHECK()
						
							FLOAT fDisplayMultiplier = 1.0075 // Make the bar display a value slightly ahead of what it actually is, so that the player sees the bar being full for a few frames before exploding, rather than exploding with the bar at about 95% visually
							
							IF fDisplayExplodeProgress < fBDProgress * fDisplayMultiplier
								bInDangerZone = TRUE
							ENDIF
							
							fDisplayExplodeProgress = LERP_FLOAT(fDisplayExplodeProgress, fBDProgress, 2.0 * GET_FRAME_TIME())
							fDisplayExplodeProgress *= fDisplayMultiplier
						ELSE
							IF fDisplayExplodeProgress != fBDProgress
								IF fDisplayExplodeProgress < fBDProgress
									bInDangerZone = TRUE
									fDisplayExplodeProgress = CLAMP( fDisplayExplodeProgress + (((fLastFrameTime*1.25) / TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iFailSpeedTimer[iRule])) * 100), 0, fBDProgress)
								ELSE
									fDisplayExplodeProgress = CLAMP( fDisplayExplodeProgress - (((fLastFrameTime*1.25) / TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iFailSpeedTimer[iRule])) * 100), fBDProgress, 100)
								ENDIF
								
							ENDIF
						ENDIF
					
						PRINTLN("[AltitudeSpeedBar] Spectator player Explode Percentage =  ",fBDProgress)
						
						IF NOT bIsAnySpectator
							PROCESS_SPEED_BAR_SOUNDS(bInDangerZone, tempVeh, sSoundSet)
						ENDIF
				
						IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SPEED_BAR_AIRSTRIKE_CALLED)
							fDisplayExplodeProgress = 100.0
							PRINTLN("[AltitudeSpeedBar] Setting explode percentage to 100, airstrike called")
						ENDIF
						
						PRINTLN("[AltitudeSpeedBar] Drawing Explode Percentage = ", fDisplayExplodeProgress)
						
						TEXT_LABEL_63 tl63 = "SPDBR_EXPLD"
						BOOL bLiteral = FALSE
						IF MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
						AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iPartToUse].iTeam].iRuleBitsetNine[MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iTeam]], ciBS_RULE9_EXPLODE_BAR_TEAM_TARGET_STRING)
							tl63 = g_FMMC_STRUCT.tl23CustomTargetString[MC_PlayerBD[iPartToUse].iTeam]
							bLiteral = TRUE
						ENDIF
						
						DRAW_GENERIC_METER(ROUND(fDisplayExplodeProgress), 100, tl63, HUD_COLOUR_RED, DEFAULT, eHUDOrder_Explode, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bLiteral)
						
						#IF IS_DEBUG_BUILD
						IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
							TEXT_LABEL_63 tlDebug = "DisplayProgress = "
							tlDebug += FLOAT_TO_STRING(fDisplayExplodeProgress)
							draw_debug_text_2d(tlDebug, <<0.2, 0.2, 0.0>>)
							
							TEXT_LABEL_63 tlDebug2 = "Real Progress = "
							tlDebug2 += FLOAT_TO_STRING(fRealExplodeProgress)
							draw_debug_text_2d(tlDebug2, <<0.2, 0.265, 0.0>>)
						ENDIF
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDFOR	
ENDPROC

FUNC BOOL PLAYER_STATE_LIST_SORT(PLAYER_STATE_LIST_DATA &playerStateListData)
	INT i
	
	INT iSwapTeam = -1
	
	STRING sPlayerName[MAX_NUM_MC_PLAYERS]
	
	FOR i = 0 TO playerStateListData.iNumberOfPlayersToCheck - 1
		IF playerStateListData.piPlayerStateList[i] != INVALID_PLAYER_INDEX()
			sPlayerName[i] = GET_PLAYER_NAME(playerStateListData.piPlayerStateList[i])
			
			//Place the local player and their team at the top of Player State List
			IF playerStateListData.bSortLocalPlayerTeamTop
				IF NOT IS_STRING_NULL_OR_EMPTY(sPlayerName[i]) AND NOT IS_STRING_NULL_OR_EMPTY(GET_PLAYER_NAME(PlayerToUse))
					IF ARE_STRINGS_EQUAL(sPlayerName[i], GET_PLAYER_NAME(PlayerToUse))
						playerStateListData.uiSlot[0].sName = sPlayerName[i]
						playerStateListData.uiSlot[0].iValue = playerStateListData.iPlayerStateList[i]
						
						IF i > playerStateListData.iNumberOfPlayersToDisplayPerTeam - 1
							iSwapTeam = FLOOR(TO_FLOAT(i) / TO_FLOAT(playerStateListData.iNumberOfPlayersToDisplayPerTeam))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	INT iCurrentSlotTeam[FMMC_MAX_TEAMS]
	
	FOR i = 0 TO playerStateListData.iNumberOfTeamsToCheck - 1
		iCurrentSlotTeam[i] = i * playerStateListData.iNumberOfPlayersToDisplayPerTeam
	ENDFOR
	
	IF playerStateListData.bSortLocalPlayerTeamTop
		iCurrentSlotTeam[0] = 1	//Local player has slot 0...
	ENDIF
	
	FOR i = 0 TO playerStateListData.iNumberOfTeamsToCheck - 1
		playerStateListData.hcTeam[i] = GET_HUD_COLOUR_FOR_FMMC_TEAM(i, PlayerToUse)
	ENDFOR
	
	IF iSwapTeam != -1
		HUD_COLOURS hcTemp = playerStateListData.hcTeam[0]
		playerStateListData.hcTeam[0] = playerStateListData.hcTeam[iSwapTeam]
		playerStateListData.hcTeam[iSwapTeam] = hcTemp
	ENDIF
	
	PRINTLN("[PLAYER_LOOP] - PLAYER_STATE_LIST_SORT")
	FOR i = 0 TO MAX_NUM_MC_PLAYERS - 1
		IF (playerStateListData.bSortLocalPlayerTeamTop
		AND NOT IS_STRING_NULL_OR_EMPTY(sPlayerName[i]) 
		AND NOT IS_STRING_NULL_OR_EMPTY(playerStateListData.uiSlot[0].sName) 
		AND NOT ARE_STRINGS_EQUAL(sPlayerName[i], playerStateListData.uiSlot[0].sName))	//Not local player
		OR (NOT playerStateListData.bSortLocalPlayerTeamTop)
			INT iTeamIndex = FLOOR(TO_FLOAT(i) / TO_FLOAT(playerStateListData.iNumberOfPlayersToDisplayPerTeam))
			
			IF iSwapTeam != -1
				IF iTeamIndex = 0
					iTeamIndex = iSwapTeam
				ELIF iTeamIndex = iSwapTeam
					iTeamIndex = 0
				ENDIF
			ENDIF
			
			playerStateListData.uiSlot[iCurrentSlotTeam[iTeamIndex]].sName = sPlayerName[i]
			playerStateListData.uiSlot[iCurrentSlotTeam[iTeamIndex]].iValue = playerStateListData.iPlayerStateList[i]
			
			iCurrentSlotTeam[iTeamIndex]++
		ENDIF
	ENDFOR
	
	IF NOT IS_STRING_NULL_OR_EMPTY(playerStateListData.uiSlot[0].sName)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC PLAYER_STATE_LIST_DISPLAY(PLAYER_STATE_LIST_DATA &playerStateListData, INT iEventTimerDisplay, HUD_COLOURS EventTimerColour, BOOL bIsTimerNotDistance = FALSE)
	BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER(playerStateListData.uiSlot[0].sName, playerStateListData.uiSlot[0].iValue,
									   playerStateListData.uiSlot[1].sName, playerStateListData.uiSlot[1].iValue,
									   playerStateListData.uiSlot[2].sName, playerStateListData.uiSlot[2].iValue,
									   playerStateListData.uiSlot[3].sName, playerStateListData.uiSlot[3].iValue,
									   playerStateListData.uiSlot[4].sName, playerStateListData.uiSlot[4].iValue,
									   playerStateListData.uiSlot[5].sName, playerStateListData.uiSlot[5].iValue,
									   playerStateListData.uiSlot[6].sName, playerStateListData.uiSlot[6].iValue,
									   playerStateListData.uiSlot[7].sName, playerStateListData.uiSlot[7].iValue,
									   playerStateListData.hcTeam[0], playerStateListData.hcTeam[1],
									   iEventTimerDisplay,
									   EventTimerColour,
									   "MC_TIMEOL", 
									   bIsAnySpectator, 
									   bIsTimerNotDistance)
ENDPROC

PROC UPDATE_THERMITE_REMAINING_HUD(BOOL bShouldHideHUD)
	
	IF bShouldHideHUD
		EXIT
	ENDIF
	
	IF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__STEALTH
	OR g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
		IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iGenericTrackingBitset, ciContinuityGenericTrackingBS_DroppedToDirect)
			//Don't display the thermite hud if we've dropped down to the direct branch.
			EXIT
		ENDIF
	ENDIF
	
	INT iTeam = MC_playerBD[iLocalPart].iTeam
	INT iCurrentRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]
	
	IF iCurrentRule < FMMC_MAX_RULES
		IF NOT ( g_bMissionEnding OR g_bMissionOver )
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iCurrentRule], ciBS_RULE13_SHOW_THERMITE_REMAINING_HUD)
			
			INT iRemainingCharges
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_PER_PLAYER_THERMAL_CHARGES)
				iRemainingCharges = MC_playerBD_1[iPartToUse].iThermitePerPlayerRemaining
			ELSE
				iRemainingCharges = MC_serverBD_1.iTeamThermalCharges[iTeam]
			ENDIF
			
			SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
			
			IF iRemainingCharges < 0
				DRAW_GENERIC_SCORE(99, "FMMC_THERM_CHGS", DEFAULT, HUD_COLOUR_WHITE, HUDORDER_TOP) //Infinite
			ELIF iRemainingCharges = 0
				DRAW_GENERIC_SCORE(iRemainingCharges, "FMMC_THERM_CHGS", DEFAULT, HUD_COLOUR_RED, HUDORDER_TOP) //Red 0
			ELSE	
				DRAW_GENERIC_SCORE(iRemainingCharges, "FMMC_THERM_CHGS", DEFAULT, HUD_COLOUR_WHITE, HUDORDER_TOP) //White
			ENDIF
			SET_BIT(g_TransitionSessionNonResetVars.iAirlockHUDBitset, ciFMMCAirlockHUDBS_LivesShown)
			IF IS_BIT_SET(g_TransitionSessionNonResetVars.iAirlockHUDBitset, ciFMMCAirlockHUDBS_ShowThermite)
				g_TransitionSessionNonResetVars.iPlayerThermalChargesForHUD = iRemainingCharges
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_INCREMENTAL_RULE_FAIL_TIMER()
	
	INT iTeam, iTeamLoop, iRule	
	iTeam = MC_PlayerBD[iPartToUse].iteam	
	iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]	
	
	IF bIsLocalPlayerHost		
		FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
			IF IS_TEAM_ACTIVE(iTeamLoop)
				iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeamLoop]
				
				// Migration...
				IF iModifiedTimeElapsed[iTeamLoop] < MC_ServerBD_4.iRuleTimerFailElapsedTime[iTeamLoop]
					iModifiedTimeElapsed[iTeamLoop] = MC_ServerBD_4.iRuleTimerFailElapsedTime[iTeamLoop]
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF (GET_FRAME_COUNT() % 10) = 0
					PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - Before_________________________________________________ for iTeam: ", iTeamLoop)
				ENDIF
				#ENDIF
				
				IF iRule < FMMC_MAX_RULES
					IF iFTTimeElapsedLastFrame > 0
					AND NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeamLoop)
					AND g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].sRuleIncTimerFail[iRule].iStartTime > 0
						IF NOT HAS_NET_TIMER_STARTED(MC_ServerBD_4.tdIncrementalRuleFailTimer[iTeamLoop])
							START_NET_TIMER(MC_ServerBD_4.tdIncrementalRuleFailTimer[iTeamLoop])
						ENDIF
						INT iTimeMax = g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].sRuleIncTimerFail[iRule].iStartTime
						INT iTimeElapsed = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_ServerBD_4.tdIncrementalRuleFailTimer[iTeamLoop])
						INT iTimeLastFrame = GET_GAME_TIMER() - iFTTimeElapsedLastFrame
						INT iTimeLastFrameMod
						FLOAT fMult = (TO_FLOAT(iCurrentPlacedPedsAlive) / TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].sRuleIncTimerFail[iRule].iPlacedPedAmountThresh))
						FLOAT fTimeProgressModifier = 1.0
						
						#IF IS_DEBUG_BUILD
						IF (GET_FRAME_COUNT() % 10) = 0
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - iTimeElapsed: ", iTimeElapsed)
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - iCurrentPlacedPedsAlive: ", iCurrentPlacedPedsAlive)
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - iTimeLastFrame: ", iTimeLastFrame)			
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - fMult: ", fMult)
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - Thresh: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].sRuleIncTimerFail[iRule].iPlacedPedAmountThresh)
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - Rate to * by Mult: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].sRuleIncTimerFail[iRule].fPlacedPedAmountRateInc)
						ENDIF
						#ENDIF
						
						IF fMult > 1
						AND g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].sRuleIncTimerFail[iRule].fPlacedPedAmountRateInc <> 0
							fTimeProgressModifier += g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].sRuleIncTimerFail[iRule].fPlacedPedAmountRateInc * fMult
							
							IF fTimeProgressModifier < 0.0
								fTimeProgressModifier = 0.05
								PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - fTimeProgressModifier is: ", fTimeProgressModifier, " Capping at 0.05")							
								ASSERTLN("[LM] - fTimeProgerssModifier is too low, capping to 0.05. Content, please adjust the Rule Fail Timer settings.")
								#IF IS_DEBUG_BUILD
								ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_INCREMENTAL_RULE_TIMER_TOO_LOW, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "time progress modifier < 0.05. Content, please adjust the Rule Fail Timer settings")
								#ENDIF
							ENDIF
							
							iTimeLastFrameMod = ROUND((TO_FLOAT(iTimeLastFrame) * fTimeProgressModifier))
																
							iModifiedTimeElapsed[iTeamLoop] += (iTimeLastFrameMod-iTimeLastFrame)
						ENDIF
																	
						IF (GET_FRAME_COUNT() % 30) = 0
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - ADDING TO SERVER BD NOW ")
							MC_ServerBD_4.iRuleTimerFailElapsedTime[iTeamLoop] = iModifiedTimeElapsed[iTeamLoop]
						ENDIF
							
						iTimeElapsed += iModifiedTimeElapsed[iTeamLoop]
						
						#IF IS_DEBUG_BUILD	
						IF (GET_FRAME_COUNT() % 10) = 0
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - After_________________________________________________")
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - iTimeLastFrameMod: ", iTimeLastFrameMod)
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - iModifiedTimeElapsed: ", iModifiedTimeElapsed[iTeamLoop])
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - fTimeProgressModifier: ", fTimeProgressModifier)
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - iTimeElapsed: ", iTimeElapsed)			
						ENDIF
						#ENDIF
						
						IF iTimeElapsed > iTimeMax
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].sRuleIncTimerFail[iRule].iIncTimerBS, ciBS_INC_TIMER_SHOULD_PASS_INSTEAD)
								SET_BIT(MC_serverBD.iServerBitSet,SSBOOL_TEAM0_FINISHED + iTeamLoop)
								PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - Passing Mission...")
							ELSE							
								REQUEST_SET_TEAM_FAILED(iTeamLoop)
								PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - Failing Mission...")
							ENDIF
							
							iModifiedTimeElapsed[iTeamLoop] = 0
							RESET_NET_TIMER(MC_ServerBD_4.tdIncrementalRuleFailTimer[iTeamLoop])
							MC_ServerBD_4.iRuleTimerFailElapsedTime[iTeamLoop] = 0
						ENDIF
					ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].sRuleIncTimerFail[iRule].iStartTime <= 0
						IF HAS_NET_TIMER_STARTED(MC_ServerBD_4.tdIncrementalRuleFailTimer[iTeamLoop])
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - Cleaning up... 1")
							RESET_NET_TIMER(MC_ServerBD_4.tdIncrementalRuleFailTimer[iTeamLoop])
							MC_ServerBD_4.iRuleTimerFailElapsedTime[iTeamLoop] = 0
						ENDIF					
						IF iModifiedTimeElapsed[iTeamLoop] != 0
							iModifiedTimeElapsed[iTeamLoop] = 0
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	IF iRule < FMMC_MAX_RULES
	AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleIncTimerFail[iRule].iStartTime > 0
	AND MC_ServerBD_4.iRuleTimerFailElapsedTime[iTeam] != 0
	AND HAS_NET_TIMER_STARTED(MC_ServerBD_4.tdIncrementalRuleFailTimer[iTeam])	
	AND NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
		INT iTimeMax = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleIncTimerFail[iRule].iStartTime
		INT iTimeElapsed = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_ServerBD_4.tdIncrementalRuleFailTimer[iTeam]) + MC_ServerBD_4.iRuleTimerFailElapsedTime[iTeam]
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleIncTimerFail[iRule].iIncTimerBS, ciBS_INC_TIMER_ENABLE_CHOPPY_MODE)
			iFTCurrentTimerProgress[iTeam] = ROUND(iFTCurrentTimerProgress[iTeam] +@ (iTimeElapsed - iFTCurrentTimerProgress[iTeam]))
		ELSE
			IF NOT HAS_NET_TIMER_STARTED(tdUpdateProgressBarChoppy)
				START_NET_TIMER(tdUpdateProgressBarChoppy)
			ENDIf
			
			IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdUpdateProgressBarChoppy, 3000)
				iFTCurrentTimerProgress[iTeam] = iTimeElapsed
				RESET_NET_TIMER(tdUpdateProgressBarChoppy)
				PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - Choppy update time. iTimeElapsed: ", iTimeElapsed)		
			ENDIF
		ENDIF
		
		IF HAS_TEAM_FAILED(iTeam)
		OR HAS_TEAM_FINISHED(MC_playerBD[iPartToUse].iteam)
		OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED )
		OR GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
		OR IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
			iFTCurrentTimerProgress[iTeam] = iTimeMax
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleIncTimerFail[iRule].iIncTimerBS, ciBS_INC_TIMER_DEPLETE_INSTEAD_OF_FILL)
			DRAW_GENERIC_METER((iTimeMax-iFTCurrentTimerProgress[iTeam]), iTimeMax, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63ObjectiveEntityHUDMessage[iRule])
		ELSE
			DRAW_GENERIC_METER(iFTCurrentTimerProgress[iTeam], iTimeMax, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63ObjectiveEntityHUDMessage[iRule])
		ENDIF
	ENDIF
	
	// Store the game timer so we can calculate how much time has passed from one frame to the other.
	iFTTimeElapsedLastFrame = GET_GAME_TIMER()
ENDPROC

PROC PROCESS_ALL_TEAM_MULTIRULE_TIMER_HUD()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_SHOW_ALL_TEAM_MULTIRULE)
	OR g_bMissionEnding
		EXIT
	ENDIF

	INT iTeamLoop
	TEXT_LABEL_63 tlTeamName
	
	FOR iTeamLoop = FMMC_MAX_TEAMS-1 TO 0 STEP -1
		tlTeamName = g_sMission_TeamName[iTeamLoop]
		IF IS_MULTIRULE_TIMER_RUNNING(iTeamLoop)
			IF iTeamLoop != GET_LOCAL_PLAYER_TEAM()
				IF IS_TEAM_ACTIVE(iTeamLoop)		
					DRAW_GENERIC_TIMER(GET_CURRENT_MULTIRULE_TIMER_TIME_REMAINING(iTeamLoop), tlTeamName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamLoop, PlayerToUse), DEFAULT, DEFAULT, DEFAULT,GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamLoop, PlayerToUse), TRUE)
				ENDIF
			ELSE
				DRAW_GENERIC_TIMER(GET_CURRENT_MULTIRULE_TIMER_TIME_REMAINING(GET_LOCAL_PLAYER_TEAM()), tlTeamName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_TOP, DEFAULT, GET_HUD_COLOUR_FOR_FMMC_TEAM(GET_LOCAL_PLAYER_TEAM(), PlayerToUse), DEFAULT, DEFAULT, DEFAULT, GET_HUD_COLOUR_FOR_FMMC_TEAM(GET_LOCAL_PLAYER_TEAM(), PlayerToUse), TRUE)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC DISPLAY_VEHICLE_RESPAWN_POOL()
	IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
		INT iTeam = MC_playerBD[iPartToUse].iTeam
		INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
		IF iRule < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_SHOW_VEHICLE_POOL_HUD)
				DRAW_GENERIC_BIG_NUMBER(MC_ServerBD.iVehicleRespawnPool, "VEH_RSP_R", DEFAULT, HUD_COLOUR_WHITE, HUDORDER_FIFTHBOTTOM)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL IS_HEALTH_BAR_ON_FOR_TEAM(INT iTeam)
	BOOL bVisible = FALSE
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		SWITCH iTeam
			CASE 0
				bVisible = (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeam1HealthBar, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]) OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset3, ciBS3_SHOW_TEAM_HEALTH_BARS_TEAM_0+iTeam))
			BREAK
			CASE 1
				bVisible = (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeam2HealthBar, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]) OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset3, ciBS3_SHOW_TEAM_HEALTH_BARS_TEAM_0+iTeam))
			BREAK
			CASE 2
				bVisible = (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeam3HealthBar, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]) OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset3, ciBS3_SHOW_TEAM_HEALTH_BARS_TEAM_0+iTeam))
			BREAK
			CASE 3
				bVisible = (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeam4HealthBar, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]) OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset3, ciBS3_SHOW_TEAM_HEALTH_BARS_TEAM_0+iTeam))
			BREAK
		ENDSWITCH
	ENDIF
	RETURN bVisible
ENDFUNC

PROC PROCESS_PLAYER_HEALTH_BAR_DISPLAY_FOR_RULE()

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_ALL_PLAYERS_HEALTH_BARS_ON_TEAM)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_ALL_PLAYERS_HEALTH_BARS_ON_TEAM_FOR_VEHICLE)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TEAM_SETTINGS_HEALTH_BARS)
		EXIT
	ENDIF

	INT iTeam
	INT iCount
	INT iCountOfPlayingPlayers = MC_ServerBD.iNumberOfPlayingPlayers[0] + MC_ServerBD.iNumberOfPlayingPlayers[1] + MC_ServerBD.iNumberOfPlayingPlayers[2] + MC_ServerBD.iNumberOfPlayingPlayers[3]
	
	IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()		
		BOOL bShowBar
		INT iPart
		PRINTLN("[PLAYER_LOOP] - PROCESS_PLAYER_HEALTH_BAR_DISPLAY_FOR_RULE")
		FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
			iTeam = MC_playerBD[iPart].iTeam
			bShowBar = TRUE
			
			IF IS_HEALTH_BAR_ON_FOR_TEAM(iTeam)				
				PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					PLAYER_INDEX playerToShow = NETWORK_GET_PLAYER_INDEX(tempPart)
					PED_INDEX PedToShow = GET_PLAYER_PED(playerToShow)
					VEHICLE_INDEX vehToShow
					STRING sTeamTextLabel
					BOOL bLiteral = TRUE
					MODEL_NAMES mnPedModel = GET_PLAYERS_PED_MODEL(playerToShow)
					
					HUD_COLOURS eHudColour
					IF mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("P_Franklin_02"))
						sTeamTextLabel = "HB_FRANKLIN"
						bLiteral = FALSE
						eHudColour = HUD_COLOUR_WHITE
					ELIF mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("IG_LamarDavis_02"))
						sTeamTextLabel = "HB_LAMAR"
						bLiteral = FALSE
						eHudColour = HUD_COLOUR_WHITE
					ELSE
						sTeamTextLabel = GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(tempPart))
						eHudColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, PlayerToUse)
					ENDIF
						
					INT HealthToShow = 0
					INT HealthMaxToShow
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_ALL_PLAYERS_HEALTH_BARS_ON_TEAM_FOR_VEHICLE)
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset3, ciBS3_SHOW_TEAM_HEALTH_BARS_VEHICLE_TEAM_0+iTeam)
						IF IS_ENTITY_ALIVE(PedToShow)
							HealthToShow = GET_ENTITY_HEALTH(PedToShow) - 100
						ENDIF
						
						HealthMaxToShow = GET_ENTITY_MAX_HEALTH(PedToShow) - 100
					ELSE
						IF IS_PED_IN_ANY_VEHICLE(PedToShow)
							vehToShow = GET_VEHICLE_PED_IS_IN(PedToShow)
							
							IF GET_PED_IN_VEHICLE_SEAT(vehToShow, VS_DRIVER) != PedToShow
								bShowBar = FALSE
							ENDIF
							
							IF IS_ENTITY_ALIVE(vehToShow)
								HealthToShow = ROUND(GET_VEHICLE_HEALTH_PERCENTAGE(vehToShow))
							ENDIF
							
							HealthMaxToShow = 100
						ELSE
							IF IS_ENTITY_ALIVE(PedToShow)
								HealthToShow = GET_ENTITY_HEALTH(PedToShow) - 100
							ENDIF
							
							HealthMaxToShow = GET_ENTITY_MAX_HEALTH(PedToShow) - 100
						ENDIF
					ENDIF
					
					IF eHudColour = HUD_COLOUR_WHITE
					AND (HealthToShow / TO_FLOAT(HealthMaxToShow)) < 0.4
						eHudColour = HUD_COLOUR_RED
					ENDIF
					
					IF bShowBar
						DRAW_GENERIC_METER(HealthToShow, HealthMaxToShow, sTeamTextLabel, eHudColour, -1, INT_TO_ENUM(HUDORDER, (-(iTeam+1))+8), DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bLiteral)
					ENDIF
					
					iCount++
				ENDIF
			ENDIF
			
			IF iCount >= iCountOfPlayingPlayers
				BREAKLOOP
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_HEALTH_BAR_DISPLAY()
	IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
	AND NOT IS_ENTITY_DEAD(PlayerPedToUse)
		INT iTeam
		FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
			IF IS_HEALTH_BAR_ON_FOR_TEAM(iTeam)
				IF MC_ServerBD_4.iCurrentHighestPriority[ iTeam ] < FMMC_MAX_RULES
					IF NOT IS_STRING_NULL_OR_EMPTY(g_sMission_TeamName[iteam])
						IF iPartForTeamHealthBar[iTeam] = -1
							INT iPart
							PRINTLN("[PLAYER_LOOP] - PROCESS_PLAYER_HEALTH_BAR_DISPLAY")
							FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
								IF MC_playerBD[iPart].iTeam = iTeam
									iPartForTeamHealthBar[iTeam] = iPart
									BREAKLOOP
								ENDIF
							ENDFOR
						ELSE
						ENDIF
						PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPartForTeamHealthBar[iTeam])
						IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
							PED_INDEX PedToShow = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(tempPart))
							IF IS_ENTITY_ALIVE(PedToShow)
								STRING sTeamTextLabel
								HUDORDER eHudOrder = HUDORDER_DONTCARE
								IF iTeam = 0 OR iTeam = 2
									sTeamTextLabel = "OJUGG_HEALTH"
									eHudOrder = HUDORDER_THIRDBOTTOM
								ELIF iTeam = 1 OR iTeam = 3
									sTeamTextLabel = "PJUGG_HEALTH"
									eHudOrder = HUDORDER_FOURTHBOTTOM
								ENDIF
								DRAW_GENERIC_METER(GET_ENTITY_HEALTH(PedToShow) - 100, GET_ENTITY_MAX_HEALTH(PedToShow) - 100, sTeamTextLabel, GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, PlayerToUse), -1, eHudOrder)
							ENDIF
						ELSE
							iPartForTeamHealthBar[iTeam] = -1
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		ENDFOR
	ENDIF
ENDPROC

PROC PROCESS_RULE_ATTEMPTS_DISPLAY()
	IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
		INT iTeam = MC_playerBD[iPartToUse].iteam
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamRuleAttempts > 1
			INT iAttemptsLeft = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamRuleAttempts - MC_serverBD.iNumTeamRuleAttempts[iTeam]
			BOOL bLastAttempt = (MC_serverBD.iNumTeamRuleAttempts[iTeam] = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamRuleAttempts - 1)
			HUD_COLOURS eHudColour = HUD_COLOUR_WHITE
			IF bLastAttempt
				eHudColour = HUD_COLOUR_RED
				DRAW_GENERIC_BIG_NUMBER(iAttemptsLeft - 1,"MC_TATTEMP",30000,eHudColour,HUDORDER_DONTCARE,FALSE,"",eHudColour, FALSE, HUDFLASHING_FLASHWHITE, 1)
			ELSE
				DRAW_GENERIC_BIG_NUMBER(iAttemptsLeft - 1,"MC_TATTEMP",-1,eHudColour,HUDORDER_DONTCARE,FALSE,"",eHudColour)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL SHOULD_DRAW_PACKAGE_HUD()
	IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam] > 0
	OR IS_BIT_SET(iLocalBoolCheck,LBOOL_HAD_MORE_THAN_1_PACKAGE)
		IF MC_serverBD_4.iObjMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER 
		OR MC_serverBD_4.iObjMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
			IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
				IF NOT IS_SPECTATOR_HUD_HIDDEN()
				AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
				AND NOT g_bEndOfMissionCleanUpHUD
					IF NOT IS_OBJECTIVE_BLOCKED()
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE_HIDE_HUD)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DRAW_PACKAGE_HUD()

	INT imaxpackages
	INT ipackage
	BOOL bMyTeam[FMMC_MAX_NUM_OBJECTS]
	HUD_COLOURS CarryColour[FMMC_MAX_NUM_OBJECTS]
	HUD_COLOURS TextColour1,TextColour2
	
	INT iMyTeam = MC_playerBD[iPartToUse].iteam
	
	IF bPlayerToUseOK
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iMyTeam ]
		
		IF NOT SHOULD_USE_CUSTOM_STRING_FOR_HUD(iMyTeam,iRule)
			IF iHUDPriority != MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] 
				iObjectiveMaxPickup =0
				CLEAR_BIT(iLocalBoolCheck,LBOOL_HAD_MORE_THAN_1_PACKAGE)
				iHUDPriority = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] 
				FOR ipackage = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
					DeliverColour[ipackage] = HUD_COLOUR_BLACK
				ENDFOR
				iPackageHUDBitset = 0
				ipackageSlot = 0
			ENDIF
			
			IF iObjectiveMaxPickup < MC_serverBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam]
				iObjectiveMaxPickup = MC_serverBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam]
				IF iObjectiveMaxPickup > 1
					SET_BIT(iLocalBoolCheck,LBOOL_HAD_MORE_THAN_1_PACKAGE)
					PRINTLN("setting more than 1 package bitset")
				ENDIF
				IF iObjectiveMaxPickup > 8
					iObjectiveMaxPickup = 8
				ENDIF
			ENDIF
			
			IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
				IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMaxDeliveryEntitiesCanCarry[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]  > 0
					IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam] <= g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMaxDeliveryEntitiesCanCarry[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] 
						imaxpackages = MC_serverBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam]
					ELSE
						imaxpackages = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMaxDeliveryEntitiesCanCarry[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] 
					ENDIF	
				ELSE
					imaxpackages = MC_serverBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam]
				ENDIF
				
				IF imaxpackages > 8
					imaxpackages = 8
				ENDIF

				IF imaxpackages > 1
				OR IS_BIT_SET(iLocalBoolCheck,LBOOL_HAD_MORE_THAN_1_PACKAGE)
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE6_BLOCK_PACKAGE_HUD)
						IF NOT IS_BIT_SET(MC_serverBD.iAnyColObjRespawnAtPriority[MC_playerBD[iPartToUse].iteam],MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
						AND MC_serverBD_4.iObjMissionLogic[MC_playerBD[iPartToUse].iteam] != FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD	
							FOR ipackage = 0 TO (imaxpackages-1)
								IF ipackage < MC_PlayerBd[iparttouse].iObjCarryCount
									CarryColour[ipackage] = HUD_COLOUR_BLUEDARK
								ELSE
									CarryColour[ipackage] = HUD_COLOUR_BLACK
								ENDIF
							ENDFOR
							
							FOR ipackage = 0 TO (iObjectiveMaxPickup-1)
								IF NOT IS_BIT_SET(iPackageHUDBitset, ipackage)
									IF DeliverColour[ipackageSlot] = HUD_COLOUR_BLACK
										IF ipackageSlot <= (iObjectiveMaxPickup-1)
											IF MC_serverBD.iObjDelTeam[ipackage] !=-1
												IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[ipackage].iPriority[MC_playerBD[iPartToUse].iteam] = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
													IF MC_playerBD[iPartToUse].iteam = MC_serverBD.iObjDelTeam[ipackage]
														DeliverColour[ipackageSlot] = HUD_COLOUR_BLUEDARK
														bMyTeam[ipackageSlot] = TRUE
													ELSE
														DeliverColour[ipackageSlot] = GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_serverBD.iObjDelTeam[ipackage], PlayerToUse)
													ENDIF
													
													ipackageSlot++
													
													SET_BIT(iPackageHUDBitset, ipackage)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDFOR
							
							TextColour1 = HUD_COLOUR_PURE_WHITE
							TextColour2 = HUD_COLOUR_BLUEDARK
							
							IF imaxpackages > 1
								DRAW_TWO_PACKAGES_EIGHT_HUD(imaxpackages,"MC_OBJCAR",iObjectiveMaxPickup,"MC_OBJDEL",CarryColour[0],CarryColour[1],CarryColour[2],CarryColour[3],CarryColour[4],CarryColour[5],CarryColour[6],CarryColour[7],
								DeliverColour[0],DeliverColour[1],DeliverColour[2],DeliverColour[3],DeliverColour[4],DeliverColour[5],DeliverColour[6],DeliverColour[7],-1,TextColour1,TextColour2 ,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE, bMyTeam[0],bMyTeam[1],bMyTeam[2],bMyTeam[3],bMyTeam[4],bMyTeam[5],bMyTeam[6],bMyTeam[7])
							ELSE
								DRAW_ONE_PACKAGES_EIGHT_HUD(iObjectiveMaxPickup,"MC_OBJDEL", FALSE,DeliverColour[0],DeliverColour[1],DeliverColour[2],DeliverColour[3],DeliverColour[4],DeliverColour[5],DeliverColour[6],DeliverColour[7],-1,TextColour1,bMyTeam[0],bMyTeam[1],bMyTeam[2],bMyTeam[3],bMyTeam[4],bMyTeam[5],bMyTeam[6],bMyTeam[7])
							ENDIF
						ELSE
							IF imaxpackages > 1
								FOR ipackage = 0 TO (imaxpackages-1)
									IF ipackage < MC_PlayerBd[iparttouse].iObjCarryCount
										CarryColour[ipackage] = HUD_COLOUR_BLUEDARK
									ELSE
										CarryColour[ipackage] = HUD_COLOUR_BLACK
									ENDIF
								ENDFOR
								
								TextColour1 = HUD_COLOUR_PURE_WHITE
								
								DRAW_ONE_PACKAGES_EIGHT_HUD(imaxpackages,"MC_OBJCAR", FALSE,CarryColour[0],CarryColour[1],CarryColour[2],CarryColour[3],CarryColour[4],CarryColour[5],CarryColour[6],CarryColour[7],-1,TextColour1)
							ENDIF
						ENDIF
					ELSE
						IF g_FMMC_STRUCT.iShowPackageHudForTeam > 0
							//For in and out - to deal with Jippin scoring issues - hardcoded o
							INT iNumofPackages = 8 - MC_serverBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam]
							DRAW_GENERIC_BIG_DOUBLE_NUMBER(iNumofPackages, 8 , "OVHEAD_PACKA")
						ELSE
							//default 
							IF iGetDeliverMax = -1
								iGetDeliverMax = MC_serverBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam]
							ENDIF
							INT iNumofPackages = iGetDeliverMax - MC_serverBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam]
							DRAW_GENERIC_BIG_DOUBLE_NUMBER(iNumofPackages, iGetDeliverMax , "OVHEAD_PACKA")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC DISPLAY_OUT_OF_BOUNDS_TIMER_LIST()

	ASSERTLN("OUT OF BOUNDS TIMER LIST FUNCTIONALITY IS CURRENTLY UNAVAILABLE NEEDS TO BE REFACTORED")
	
	PLAYER_STATE_LIST_DATA playerStateListData
	
	PLAYER_STATE_LIST_INIT(playerStateListData)
	
	INT iParticipant
	
	PRINTLN("[PLAYER_LOOP] - DISPLAY_OUT_OF_BOUNDS_TIMER_LIST")
	FOR iParticipant = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
		PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(iParticipant)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
			PLAYER_INDEX playerIndex = NETWORK_GET_PLAYER_INDEX(participantIndex)
			
			INT iTeam = GET_PLAYER_TEAM(playerIndex)
			INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
			
			IF iTeam != -1		
			AND iRule < FMMC_MAX_RULES
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iTeam], ciBS_RULE5_DRAW_OUT_OF_BOUNDS_TIMER_LIST_T1 + iTeam)
					IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_ANY_SPECTATOR)
					AND NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_HEIST_SPECTATOR)
						IF ARE_BOUNDS_ACTIVE(0)
							INT iOutOfBoundsTimerSet
							
							IF IS_PARTICIPANT_OUT_OF_BOUNDS(iParticipant, 0)
								//INT iTimeLimit
								//iTimeLimit = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iBoundsPriTimer[MC_serverBD_4.iCurrentHighestPriority[iTeam]]
								//iOutOfBoundsTimerSet = (iTimeLimit - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_playerBD[iParticipant].tdBoundstimer)) / 1000
							ELSE
								iOutOfBoundsTimerSet = -1
							ENDIF
							
							PLAYER_STATE_LIST_ADD_PLAYER(playerStateListData, iParticipant, iOutOfBoundsTimerSet)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF PLAYER_STATE_LIST_SORT(playerStateListData)
		INT iMaxMissionTime = GET_OBJECTIVE_TIME_LIMIT_FOR_LOCAL_PLAYER_CURRENT_RULE()
		INT iMissionTimePassed = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iPartToUse].iTeam])
		
		INT iTimeRemaining = iMaxMissionTime - iMissionTimePassed
		
		HUD_COLOURS hcTimer = HUD_COLOUR_WHITE
		
		IF iTimeRemaining < 0
			iTimeRemaining = GET_LOCAL_PLAYER_OBJECTIVE_TIMER_TIME_REMAINING()
		ENDIF
		
		IF iTimeRemaining < (30 * 1000)
			hcTimer = HUD_COLOUR_RED
		ENDIF
		
		INT iTimer = -1
		
		IF GET_OBJECTIVE_TIME_LIMIT_FOR_LOCAL_PLAYER_CURRENT_RULE() > 0
			iTimer = CLAMP_INT( iTimeRemaining, 0, HIGHEST_INT )
		ENDIF
		
		PLAYER_STATE_LIST_DISPLAY(playerStateListData, iTimer, hcTimer, TRUE)
	ENDIF
ENDPROC

PROC PROCESS_OUT_OF_BOUNDS_TIMER_LIST()
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	
	IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED)
	AND IS_TEAM_ACTIVE(iTeam)
	AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
	AND (NOT g_bMissionEnding OR bIsAnySpectator)
		IF GET_BITS_IN_RANGE(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE5_DRAW_OUT_OF_BOUNDS_TIMER_LIST_T1, ciBS_RULE5_DRAW_OUT_OF_BOUNDS_TIMER_LIST_T4) > 0
			DISPLAY_OUT_OF_BOUNDS_TIMER_LIST()
		ENDIF 
	ENDIF
ENDPROC

PROC DISPLAY_VEHICLE_HEALTH_HUD(VEHICLE_INDEX vehOverride = NULL, BOOL bForceGreenWhenNotMax = FALSE)
	
	INT iVehicleHealth
	INT iMaxHealth = 100
	INT iMinHealthRed = 25
	INT	iVeh = -1
	//check if in a vehicle 
	IF bPedToUseOk
	AND NOT IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_SHOWING_VEH_HEALTH_BARS)
	AND NOT IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
		IF IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
		OR DOES_ENTITY_EXIST(vehOverride)
		
			INT iTeam = MC_playerBD[iPartToUse].iteam
			VEHICLE_INDEX vehIndex 
			
			IF NOT DOES_ENTITY_EXIST(vehOverride)
				vehIndex = GET_VEHICLE_PED_IS_IN(PlayerPedToUse,FALSE)
				IF IS_ENTITY_ALIVE(vehIndex)
					iVeh = IS_ENTITY_A_MISSION_CREATOR_ENTITY(vehIndex)
					IF iVeh > -1
						iVehicleHealth = FLOOR(MC_GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(vehIndex, iVeh, GET_TOTAL_STARTING_PLAYERS(), IS_VEHICLE_A_TRAILER(vehIndex)))
					ELSE
						iVehicleHealth = ROUND(GET_VEHICLE_BODY_HEALTH(vehIndex))
						iMaxHealth = ROUND(GET_FMMC_PLAYER_VEHICLE_BODY_HEALTH_FOR_TEAM(iTeam))
						PRINTLN("(1) [DISPLAY_VEHICLE_HEALTH_HUD] iVehicleHealth: ",iVehicleHealth," iMaxHealth: ",iMaxHealth)	
					ENDIF
				ENDIF
				
				IF GET_ENTITY_MODEL(vehIndex) = RCBANDITO		
					PRINTLN("[DISPLAY_VEHICLE_HEALTH_HUD] Inside Bandito, exitting... ")	
					EXIT
				ENDIF
			ELSE
				vehIndex = vehOverride
				IF IS_ENTITY_ALIVE(vehIndex)
					iVehicleHealth = ROUND(GET_VEHICLE_BODY_HEALTH(vehIndex))
					iMaxHealth = ROUND(GET_FMMC_PLAYER_VEHICLE_BODY_HEALTH_FOR_TEAM(iTeam))
					PRINTLN("(2) [DISPLAY_VEHICLE_HEALTH_HUD] iVehicleHealth: ",iVehicleHealth," iMaxHealth: ",iMaxHealth)	
				ENDIF
			ENDIF
					
			IF DOES_ENTITY_EXIST(vehIndex)
			AND IS_ENTITY_ALIVE(vehIndex)
				HUD_COLOURS hudColour = HUD_COLOUR_WHITE
				
				IF iVehicleHealth < iMinHealthRed
					hudColour = HUD_COLOUR_RED
				ENDIF
				
				IF bForceGreenWhenNotMax
					FLOAT fTotal = GET_FMMC_PLAYER_VEHICLE_BODY_HEALTH_FOR_TEAM(iTeam)
					FLOAT fCappedTotal = (fTotal * (TO_FLOAT(iRegenCapFromBounds)/100))
					FLOAT fVehicleCurrentBodyHP = GET_VEHICLE_BODY_HEALTH(vehIndex)
					IF fVehicleCurrentBodyHP < fCappedTotal
						hudColour = HUD_COLOUR_GREEN
					ENDIF
				ENDIF
				
				HUDORDER eHUDOrder = HUDORDER_FIFTHBOTTOM
				
				DRAW_GENERIC_METER(iVehicleHealth, iMaxHealth, "VEH_HEALTH", hudColour, -1, eHUDOrder, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("(3) [DISPLAY_VEHICLE_HEALTH_HUD] iVehicleHealth: ",iVehicleHealth," iMaxHealth: ",iMaxHealth)						
				#ENDIF
				
				SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_SHOWING_VEH_HEALTH_BARS)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_HEALTH_HUD()
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_VEHICLE_HEALTH_HUD)
		IF NOT IS_BIT_SET( MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED )
		AND IS_TEAM_ACTIVE( iTeam )
		AND MC_serverBD_4.iCurrentHighestPriority[ iTeam ] < FMMC_MAX_RULES
		AND NOT g_bMissionEnding
		AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Using_RC_CAR)
			DISPLAY_VEHICLE_HEALTH_HUD()
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_ENTITY_DESTROYED_OBJECTS_HUD(INT iTeam, INT iRule)
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectsDestroyedRuleHudAmount[iRule] > 0
		
		IF bIsLocalPlayerHost
		AND MC_ServerBD_4.iObjectsRequiredForSpecialHUD[iTeam] !=  g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectsDestroyedRuleHudAmount[iRule]
			PRINTLN("PROCESS_ENTITY_DESTROYED_OBJECTS_HUD - Updating iObjectsRequiredForSpecialHUD to ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectsDestroyedRuleHudAmount[iRule])
			MC_ServerBD_4.iObjectsRequiredForSpecialHUD[iTeam] = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectsDestroyedRuleHudAmount[iRule]
		ENDIF
		
		INT iMaxAmount = MC_ServerBD_4.iObjectsRequiredForSpecialHUD[iTeam]
		INT iAmount = iMaxAmount - MC_ServerBD_4.iObjectsDestroyedForSpecialHUD[iTeam]
		
		DRAW_GENERIC_BIG_NUMBER(iAmount, "MC_GTRGS_HUD")
	ENDIF
ENDPROC

PROC PROCESS_ENTITY_DESTROYED_HUD(INT iTeam, INT iRule)
	PROCESS_ENTITY_DESTROYED_OBJECTS_HUD(iTeam, iRule)
ENDPROC

PROC PROCESS_CARNAGE_BAR_SERVER()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_CarnageBarFilled_PassRule_CarnageFull)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_CarnageBarFilled_PassRule_HeliFootageFull)
		EXIT
	ENDIF
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
		
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_IS_CARNAGE_BAR_ACTIVE)
		EXIT
	ENDIF
			
	BOOL bAddSpeedPoints
	INT i, iHeliPlayers
	FLOAT fSpeedPointsPercent
	PARTICIPANT_INDEX piPlayerPart
	PLAYER_INDEX piPlayerCar
	PED_INDEX PedPlayerCar
	VEHICLE_INDEX vehPlayerCar	
	
	INT iPlayerCount = 0
	INT iMaxPlayers = GET_NUMBER_OF_PLAYING_PLAYERS()
	PRINTLN("[PLAYER_LOOP] - PROCESS_CARNAGE_BAR_SERVER")
	FOR i = 0 TO NUM_NETWORK_PLAYERS-1
		PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(i)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)			
			IF IS_BIT_SET(MC_playerBD_1[i].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_USING_HELI_CAM)
			AND IS_BIT_SET(MC_ServerBD_1.iPartLookingAtCar, i)
				iHeliPlayers++
			ELIF IS_BIT_SET(MC_playerBD_1[i].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR)
				piPlayerPart = INT_TO_PARTICIPANTINDEX(i)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(piPlayerPart)
					piPlayerCar = NETWORK_GET_PLAYER_INDEX(piPlayerPart)
					pedPlayerCar = GET_PLAYER_PED(piPlayerCar)
					IF NOT IS_PED_INJURED(pedPlayerCar)
						IF IS_PED_IN_ANY_VEHICLE(pedPlayerCar)
							vehPlayerCar = GET_VEHICLE_PED_IS_IN(pedPlayerCar)
							FLOAT fSpeedPercent = GET_VEHICLE_CURRENT_SPEED_PERCENTAGE(vehPlayerCar, (!IS_FAKE_MULTIPLAYER_MODE_SET()))
							IF (fSpeedPercent*100) > g_FMMC_STRUCT.sCarnageBar.fCarSpeedThreshold
								bAddSpeedPoints = TRUE
								fSpeedPointsPercent += (fSpeedPercent-(g_FMMC_STRUCT.sCarnageBar.fCarSpeedThreshold/100)) / (1.0-(g_FMMC_STRUCT.sCarnageBar.fCarSpeedThreshold/100))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		iPlayerCount++
		IF iPlayerCount >= iMaxPlayers
			BREAKLOOP
		ENDIF
	ENDFOR
	
	IF NOT HAS_NET_TIMER_STARTED(tdCarnageBar_FillIncrementTimer)
		START_NET_TIMER(tdCarnageBar_FillIncrementTimer)
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdCarnageBar_FillIncrementTimer, 2000)
		REINIT_NET_TIMER(tdCarnageBar_FillIncrementTimer)

		FLOAT fSpeedPoints
		IF bAddSpeedPoints			
			fSpeedPoints = (g_FMMC_STRUCT.sCarnageBar.fCarSpeedFillRate * fSpeedPointsPercent)
		ENDIF
		FLOAT fPedPoints = (g_FMMC_STRUCT.sCarnageBar.fCarPedKillFillRate * MC_ServerBD_1.fCarnageBar_PedKilledForFill)
		FLOAT fVehPoints = (g_FMMC_STRUCT.sCarnageBar.fCarVehKillFillRate * MC_ServerBD_1.fCarnageBar_VehDestroyedForFill)
		FLOAT fStuntPoints = (g_FMMC_STRUCT.sCarnageBar.fCarStuntPerformFillRate * MC_ServerBD_1.fCarnageBar_StuntPerformedForFill)
		MC_ServerBD_1.fCarnageBar_BarFilledCarTarget += -g_FMMC_STRUCT.sCarnageBar.fHeliDrainRate + (fSpeedPoints + fPedPoints + fVehPoints + fStuntPoints)						
		MC_ServerBD_1.fCarnageBar_PedKilledForFill = 0
		MC_ServerBD_1.fCarnageBar_VehDestroyedForFill = 0
		MC_ServerBD_1.fCarnageBar_StuntPerformedForFill = 0
		
		IF MC_ServerBD_1.fCarnageBar_BarFilledCarTarget < 0
			MC_ServerBD_1.fCarnageBar_BarFilledCarTarget = 0
		ELIF MC_ServerBD_1.fCarnageBar_BarFilledCarTarget > 100
			MC_ServerBD_1.fCarnageBar_BarFilledCarTarget = 100
		ENDIF
		
		PRINTLN("[LM][CARNAGE_BAR] - (Server) New Target Car Bar Fill: ", MC_ServerBD_1.fCarnageBar_BarFilledCarTarget, " fSpeedPoints: ", fSpeedPoints, " fPedPoints: ", fPedPoints, " fVehPoints: ", fVehPoints, " fStuntPoints: ", fStuntPoints, " fHeliDrainRate: ", g_FMMC_STRUCT.sCarnageBar.fHeliDrainRate)
		
		FLOAT fHeliPoints = 0.0
		IF MC_ServerBD_1.fCarnageBar_BarFilledCarTarget >= g_FMMC_STRUCT.sCarnageBar.fHeliTargetPositiveThreshold				
		AND iHeliPlayers > 0
			fHeliPoints = g_FMMC_STRUCT.sCarnageBar.fHeliFillRate * iHeliPlayers
		ENDIF
		MC_ServerBD_1.fCarnageBar_BarFilledHeliTarget += -g_FMMC_STRUCT.sCarnageBar.fHeliDrainRate + fHeliPoints
		
		IF MC_ServerBD_1.fCarnageBar_BarFilledHeliTarget < 0
			MC_ServerBD_1.fCarnageBar_BarFilledHeliTarget = 0
		ELIF MC_ServerBD_1.fCarnageBar_BarFilledHeliTarget >= 100
			MC_ServerBD_1.fCarnageBar_BarFilledHeliTarget = 100		
		ENDIF
		
		PRINTLN("[LM][CARNAGE_BAR] - (Server) New Target Heli Bar Fill: ", MC_ServerBD_1.fCarnageBar_BarFilledHeliTarget, " fHeliPoints: ", fHeliPoints, " fHeliDrainRate: ", g_FMMC_STRUCT.sCarnageBar.fHeliDrainRate, " iHeliPlayers: ", iHeliPlayers)
				
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_CarnageBarFilled_PassRule_CarnageFull)
		AND MC_ServerBD_1.fCarnageBar_BarFilledCarTarget >= 100
			IF NOT HAS_NET_TIMER_STARTED(tdCarnageBar_MoveOnRuleTimer)
				START_NET_TIMER(tdCarnageBar_MoveOnRuleTimer)
				PRINTLN("[LM][CARNAGE_BAR] - (Server) Starting New Rule Timer now... (Carnage Bar)")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_CarnageBarFilled_PassRule_HeliFootageFull)
		AND MC_ServerBD_1.fCarnageBar_BarFilledHeliTarget >= 100
			IF NOT HAS_NET_TIMER_STARTED(tdCarnageBar_MoveOnRuleTimer)
				START_NET_TIMER(tdCarnageBar_MoveOnRuleTimer)
				PRINTLN("[LM][CARNAGE_BAR] - (Server) Starting New Rule Timer now... (Heli Footage)")
			ENDIF
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(tdCarnageBar_MoveOnRuleTimer)
	AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdCarnageBar_MoveOnRuleTimer, 3000)
		PRINTLN("[LM][CARNAGE_BAR] - (Server) Timer Expired Moving rule on....")
		
		RESET_NET_TIMER(tdCarnageBar_MoveOnRuleTimer)
		
		MC_ServerBD_1.fCarnageBar_BarFilledCarTarget = 0
		MC_ServerBD_1.fCarnageBar_BarFilledHeliTarget = 0
		
		MC_ServerBD_1.fCarnageBar_PedKilledForFill = 0
		MC_ServerBD_1.fCarnageBar_VehDestroyedForFill = 0
		MC_ServerBD_1.fCarnageBar_StuntPerformedForFill = 0

		FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
			SET_PROGRESS_OBJECTIVE_FOR_TEAM(iTeam, TRUE)
		ENDFOR
	ENDIF
ENDPROC

PROC PROCESS_CARNAGE_BAR_CLIENT()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_CarnageBarFilled_PassRule_CarnageFull)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_CarnageBarFilled_PassRule_HeliFootageFull)
		EXIT
	ENDIF
	
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
		
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_IS_CARNAGE_BAR_ACTIVE)
		EXIT
	ENDIF

	BOOL bLookingAtCar

	IF bLocalPlayerPedOK
	AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)		
		MODEL_NAMES mnVeh = GET_ENTITY_MODEL(vehPlayer)
		BOOL bDriver = GET_PED_IN_VEHICLE_SEAT(vehPlayer, VS_DRIVER) = localPlayerPed
				
		IF bDriver
			IF NOT IS_THIS_MODEL_A_HELI(mnVeh)
			AND NOT IS_THIS_MODEL_A_PLANE(mnVeh)				
				BOOL bValidVehicle = ARE_WE_IN_A_VALID_CARNAGE_BAR_VEHICLE()
				IF NOT IS_BIT_SET(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR)
				AND bValidVehicle
					SET_BIT(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR)
					PRINTLN("[LM][CARNAGE_BAR] - (Client) Setting ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR")
				ENDIF
				
				IF IS_ENTITY_IN_AIR(vehPlayer)
					IF NOT HAS_NET_TIMER_STARTED(tdCarnageBar_BigAirTimer)
						START_NET_TIMER(tdCarnageBar_BigAirTimer)
					ENDIF					
					IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdCarnageBar_BigAirTimer, 3500)
						PRINTLN("[LM][CARNAGE_BAR] - (Client) Performed Stunt: Airtime 3.5 seconds.")
						RESET_NET_TIMER(tdCarnageBar_BigAirTimer)
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedContentEventType_CarnageBarPerformedStuntAir)
					ENDIF
				ELSE
					IF HAS_NET_TIMER_STARTED(tdCarnageBar_BigAirTimer)
						RESET_NET_TIMER(tdCarnageBar_BigAirTimer)
					ENDIF
				ENDIF
			ELIF IS_THIS_MODEL_A_HELI(mnVeh)
				IF NOT IS_BIT_SET(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_HELI)
					SET_BIT(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_HELI)
					PRINTLN("[LM][CARNAGE_BAR] - (Client) Setting ci_CARNAGEBAR_BS_IS_DRIVER_OF_HELI")
				ENDIF
			ELSE
				IF IS_BIT_SET(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_HELI)
					CLEAR_BIT(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_HELI)
					PRINTLN("[LM][CARNAGE_BAR] - (Client) Clearing ci_CARNAGEBAR_BS_IS_DRIVER_OF_HELI")
				ENDIF
				IF IS_BIT_SET(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR)
					CLEAR_BIT(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR)
					PRINTLN("[LM][CARNAGE_BAR] - (Client) Clearing ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR")
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR)
			CLEAR_BIT(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR)
			PRINTLN("[LM][CARNAGE_BAR] - (Client) Clearing ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR")
		ENDIF
	ENDIF
	
	IF IS_LOCAL_PLAYER_USING_TURRET_CAM()
		IF NOT IS_BIT_SET(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_USING_HELI_CAM)
			SET_BIT(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_USING_HELI_CAM)
			PRINTLN("[LM][CARNAGE_BAR] - (Client) Setting ci_CARNAGEBAR_BS_IS_USING_HELI_CAM")
		ENDIF
		
		PARTICIPANT_INDEX partPlayerCarTarget
		PLAYER_INDEX piPlayerCarTarget
		PED_INDEX pedPlayerCarTarget
		VEHICLE_INDEX vehPlayerCarTarget
		
		INT i = 0
		INT iPlayerCount = 0
		INT iMaxPlayers = GET_NUMBER_OF_PLAYING_PLAYERS()
		PRINTLN("[PLAYER_LOOP] - CARNAGE_BAR")
		FOR i = 0 TO NUM_NETWORK_PLAYERS-1
			partPlayerCarTarget = INT_TO_PARTICIPANTINDEX(i)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(partPlayerCarTarget)
				IF IS_BIT_SET(MC_playerBD_1[i].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR)
							
					PRINTLN("[LM][CARNAGE_BAR] - (Client) iPlayerCarTarget: ", i)
		
					piPlayerCarTarget = NETWORK_GET_PLAYER_INDEX(partPlayerCarTarget)
					pedPlayerCarTarget = GET_PLAYER_PED(piPlayerCarTarget)
					
					IF NOT IS_PED_INJURED(pedPlayerCarTarget)
						IF IS_PED_IN_ANY_VEHICLE(pedPlayerCarTarget)
							vehPlayerCarTarget = GET_VEHICLE_PED_IS_IN(pedPlayerCarTarget)
							
							IF IS_VEHICLE_DRIVEABLE(vehPlayerCarTarget)
								REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")
								IF HAS_STREAMED_TEXTURE_DICT_LOADED("helicopterhud")
																		
									FLOAT fBoxDrawSizeX = 0.025
									FLOAT fBoxDrawSizeY = 0.03
									FLOAT xVehPos = 0
									FLOAT yVehPos = 0
									INT r = 0
									INT g = 0
									INT b = 0
									INT a = 0
									
									SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)	
									SET_DRAW_ORIGIN(GET_ENTITY_COORDS(vehPlayerCarTarget), FALSE)	
									GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(vehPlayerCarTarget), xVehPos, yVehPos)
									GET_HUD_COLOUR(HUD_COLOUR_GREEN, r, g, b, a)									
									CLEAR_DRAW_ORIGIN()
									
									DRAW_SPRITE("helicopterhud", "hud_corner", 0.4, 0.4, fBoxDrawSizeX, fBoxDrawSizeY, 0, r, g, b, a, TRUE)
									DRAW_SPRITE("helicopterhud", "hud_corner", 0.6, 0.4, fBoxDrawSizeX, fBoxDrawSizeY, 90, r, g, b, a, TRUE)
									DRAW_SPRITE("helicopterhud", "hud_corner", 0.4, 0.6, fBoxDrawSizeX, fBoxDrawSizeY, 270, r, g, b, a, TRUE)
									DRAW_SPRITE("helicopterhud", "hud_corner", 0.6, 0.6, fBoxDrawSizeX, fBoxDrawSizeY, 180, r, g, b, a, TRUE)
									
									PRINTLN("[LM][CARNAGE_BAR] - (Client) Drawing Boxes - xVehPos: ", xVehPos, " yVehPos: ", yVehPos)
																		
									IF xVehPos > 0.4 AND xVehPos < 0.6
									AND yVehPos > 0.4 AND yVehPos < 0.6
										DRAW_BOX_AROUND_TARGET_VEHICLE(vehPlayerCarTarget, DEFAULT, HUD_COLOUR_GREEN)
										PRINTLN("[LM][CARNAGE_BAR] - (Client) bLookingAtCar = TRUE")
										bLookingAtCar = TRUE
									ELSE
										DRAW_BOX_AROUND_TARGET_VEHICLE(vehPlayerCarTarget, DEFAULT, HUD_COLOUR_RED)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				iPlayerCount++
				IF iPlayerCount >= iMaxPlayers
					BREAKLOOP
				ENDIF
			ENDIF
		ENDFOR
	ELSE
		IF IS_BIT_SET(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_USING_HELI_CAM)
			CLEAR_BIT(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_USING_HELI_CAM)			
			PRINTLN("[LM][CARNAGE_BAR] - (Client) Clearing ci_CARNAGEBAR_BS_IS_USING_HELI_CAM")
		ENDIF
	ENDIF
	
	IF bLookingAtCar
		RESET_NET_TIMER(tdCarnageBar_LookingAtCarResetTimer)
		IF NOT IS_BIT_SET(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_LOOKING_AT_CAR)
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedContentEventType_HeliCamLookingAtCar)
			SET_BIT(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_LOOKING_AT_CAR)
			PRINTLN("[LM][CARNAGE_BAR] - (Client) Resetting Timer: tdCarnageBar_LookingAtCarResetTimer")
			PRINTLN("[LM][CARNAGE_BAR] - (Client) Setting ci_CARNAGEBAR_BS_LOOKING_AT_CAR")
		ENDIF
	ELSE
		IF NOT HAS_NET_TIMER_STARTED(tdCarnageBar_LookingAtCarResetTimer)
			START_NET_TIMER(tdCarnageBar_LookingAtCarResetTimer)
			PRINTLN("[LM][CARNAGE_BAR] - (Client) Starting Timer: tdCarnageBar_LookingAtCarResetTimer")
		ENDIF		
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdCarnageBar_LookingAtCarResetTimer, 400)
			IF IS_BIT_SET(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_LOOKING_AT_CAR)
				BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedContentEventType_HeliCamStoppedLookingAtCar)
				CLEAR_BIT(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_LOOKING_AT_CAR)
				PRINTLN("[LM][CARNAGE_BAR] - (Client) Setting ci_CARNAGEBAR_BS_LOOKING_AT_CAR")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CARNAGE_BAR_HUD()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_CarnageBarFilled_PassRule_CarnageFull)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_CarnageBarFilled_PassRule_HeliFootageFull)
		EXIT
	ENDIF	
	
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
		
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_IS_CARNAGE_BAR_ACTIVE)
		EXIT
	ENDIF
	
	IF MC_ServerBD_1.fCarnageBar_BarFilledHeliTarget > -1
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_CarnageBar_UseHeliCam)
		INTERP_FLOAT_TO_TARGET_AT_SPEED(fCarnageBar_BarFilledHeliHUD, MC_ServerBD_1.fCarnageBar_BarFilledHeliTarget, 0.3)		
		INT iCarnageBar_BarFilledHeliHUD = ROUND(fCarnageBar_BarFilledHeliHUD)
		
		TEXT_LABEL_63 tl63 = "CRNBAR_HELI"
		DRAW_GENERIC_METER(iCarnageBar_BarFilledHeliHUD, 100, tl63, HUD_COLOUR_WHITE, DEFAULT, HUDORDER_THIRDBOTTOM, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 
			DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iCarnageBar_BarFilledHeliHUD)
	ENDIF
	
	IF MC_ServerBD_1.fCarnageBar_BarFilledCarTarget > -1
		INTERP_FLOAT_TO_TARGET_AT_SPEED(fCarnageBar_BarFilledCarHUD, MC_ServerBD_1.fCarnageBar_BarFilledCarTarget, 0.3)		
				
		INT iCarnageBar_BarFilledCarHUD = ROUND(fCarnageBar_BarFilledCarHUD)		
		INT iConst = ROUND(g_FMMC_Struct.sCarnageBar.fHeliTargetPositiveThreshold)
		PERCENTAGE_METER_LINE PercentageLine = GET_PERCENTAGE_METER_LINE_FROM_PERCENTAGE_INT(iConst)
		IF iConst = 100
			PercentageLine = PERCENTAGE_METER_LINE_NONE
		ENDIF
		
		TEXT_LABEL_63 tl63 = "CRNBAR_CAR"
		DRAW_GENERIC_METER(iCarnageBar_BarFilledCarHUD, 100, tl63, HUD_COLOUR_WHITE, DEFAULT, HUDORDER_THIRDBOTTOM, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT,
			DEFAULT, PercentageLine, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iCarnageBar_BarFilledCarHUD)
	ENDIF
ENDPROC


PROC MAINTAIN_GPS_DISTANCE_METER_BAR(INT iBar, INT iRule)
	INT iPed = iGPSTrackingMeterPedIndex[iBar]
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
		INT iHudOrder = iBar
		
		TEXT_LABEL_15 tl15 = "DISTMET_HUD"
		
		IF IS_BLIP_SPRITE_OVERRIDE_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sPedBlipStruct)
			tl15 = "DISTMET_HUD_"
			tl15 += g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sPedBlipStruct.iBlipSpriteOverride
			
			iHudOrder = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sPedBlipStruct.iBlipSpriteOverride
		ENDIF
		
		INT fDistMax = ROUND(VDIST(vGPSTrackingMeterStart[iBar], vGPSTrackingMeterEnd[iBar]))
		INT fCurrentProgress = ROUND(VDIST(GET_ENTITY_COORDS(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])), vGPSTrackingMeterEnd[iBar]))
	
		IF fCurrentProgress <= iGPSTrackingMeterDistCache[iBar]
			iGPSTrackingMeterDistCache[iBar] = fCurrentProgress
		ENDIF
		
		FLOAT fPercent = 0
		fPercent = (TO_FLOAT(iGPSTrackingMeterDistCache[iBar]) / fDistMax) * 100.0
		INT iTimeToFlash = -1
		IF fPercent < g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iHUDFlashPercentage[iRule]
			iTimeToFlash = 999999
		ENDIF
		
		IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iPedClearFailOnGoTo, iped)
			IF NOT HAS_NET_TIMER_STARTED(timerDistanceBarDelay)
				REINIT_NET_TIMER(timerDistanceBarDelay)
			ELIF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timerDistanceBarDelay) > ciTimerDistanceBarDelay
				DRAW_GENERIC_METER(iGPSTrackingMeterDistCache[iBar], fDistMax, tl15, HUD_COLOUR_RED, iTimeToFlash, HUDORDER_NINETHBOTTOM - INT_TO_ENUM(HUDORDER, iHudOrder))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_RUINER_AMMO_UI()
	IF IS_BIT_SET(iLocalBoolCheck22, LBOOL22_USING_CUSTOM_VEHICLE_AMMO)
		INT iTeam = MC_PlayerBD[iLocalPart].iteam
		INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
			
		IF iTeam < FMMC_MAX_TEAMS
		AND iRule < FMMC_MAX_RULES
			IF bLocalPlayerPedOK
			AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
				
				VEHICLE_INDEX playerVeh = GET_VEHICLE_PED_IS_IN(localPlayerPed)
				WEAPON_TYPE wtCurrentRuinerWeapon
				
				IF GET_CURRENT_PED_VEHICLE_WEAPON(localPlayerPed, wtCurrentRuinerWeapon)
					IF wtCurrentRuinerWeapon = WEAPONTYPE_DLC_VEHICLE_RUINER_BULLET
						IF iRuinerMGAmmo > -1
							DRAW_GENERIC_BIG_DOUBLE_NUMBER(GET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 0), iRuinerMGAmmoMax, "AMMO_RUIN_MG")
						ENDIF
					ELIF wtCurrentRuinerWeapon = WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET
					AND IS_PLAYER_VEHICLE_WEAPON_TOGGLED_TO_NON_HOMING(LocalPlayer)
						IF iRuinerRocketsAmmo > -1
							DRAW_GENERIC_BIG_DOUBLE_NUMBER(GET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 1), iRuinerRocketsAmmoMax, "AMMO_RUIN_RO")
						ENDIF
					ELIF wtCurrentRuinerWeapon = WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET
					AND NOT IS_PLAYER_VEHICLE_WEAPON_TOGGLED_TO_NON_HOMING(LocalPlayer)
						IF iRuinerRocketsHomingAmmo > -1
							DRAW_GENERIC_BIG_DOUBLE_NUMBER(GET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 1), iRuinerRocketsHomingAmmoMax, "AMMO_RUIN_HR")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DISPLAY_COUNTERMEASURE_COOLDOWN()

	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
	AND NOT IS_PED_DEAD_OR_DYING(PlayerPedToUse)
	AND NOT IS_PLAYER_RESPAWNING(LocalPlayer)
	
		BOOL bUsingChaff = GET_VEHICLE_MOD(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), MOD_BUMPER_F) =  0
		INT iCooldownLength = 2500
		SCRIPT_TIMER stCooldownTimer
		
		IF bUsingChaff
			stCooldownTimer = GET_CHAFF_COOLDOWN_TIMER()
			iCooldownLength = g_sMPTunables.iSMUGGLER_CHAFF_COOLDOWN + g_sMPTunables.iSMUGGLER_CHAFF_DURATION
							
			IF g_bOverrideChaffCooldown
				iCooldownLength = g_sMPTunables.iSMUGGLER_CHAFF_DURATION + g_iChaffCooldown
			ENDIF
			
			PRINTLN("[HUD] Using chaff, Cooldown length is ", iCooldownLength)
		ELSE
			stCooldownTimer = GET_FLARE_COOLDOWN_TIMER()
			iCooldownLength = 2500
		
			IF g_bOverrideFlareCooldown
				iCooldownLength = g_iFlareCooldown
			ENDIF
			
			PRINTLN("[HUD] Using flares, Cooldown length is ", iCooldownLength)
		ENDIF

		IF HAS_NET_TIMER_STARTED(stCooldownTimer)
			INT iTimeSinceLastFired = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stCooldownTimer)
			INT iMaxCooldown = iCooldownLength
			TEXT_LABEL_15 tl15 = "CM_COOLDOWN_UI"
			
			INT iNum = (iMaxCooldown - iTimeSinceLastFired)
			PRINTLN("[HUD] Countermeasure Cooldown meter: ", iNum, " / ", iMaxCooldown)
			
			DRAW_GENERIC_METER(iMaxCooldown - iNum, iMaxCooldown, tl15, HUD_COLOUR_ORANGE)
		ENDIF
	ENDIF
	
ENDPROC

//[FMMC2020] Split this up between HUD and local player  - function should be split up so that the HUD stuff is called somewhere separate to where we call DROP_MONEY_IF_HIT_EVERY
///PURPOSE: This function handles all of the functionality to do with dropping money
PROC MANAGE_CASH_GRAB_TAKE_AND_DROP(BOOL bHidingHud = FALSE)
	
	IF NOT IS_CUTSCENE_PLAYING()

		IF GET_TOTAL_CASH_GRAB_TAKE() > 0
			DROP_MONEY_IF_HIT_EVERY()
		ENDIF
		
		//display grabbed and dropped money on the hud
		IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_USED_A_COKE_GRAB_MINIGAME)
			IF bShouldProcessCokeGrabHud
				DRAW_COKE_GRAB_HUD()
			ELSE
				//IF IS_PLAYER_SPECTATING(LocalPlayer)
					bShouldProcessCokeGrabHud = TRUE
				//ENDIF
			ENDIF
		ELSE
			IF NOT bHidingHud
				DRAW_CASH_GRAB_TAKE_HUD()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Objective Text -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// #####  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC MANAGE_HIDING_HUD_WAITING_FOR_GODTEXT()
	IF GET_MC_SERVER_GAME_STATE() < GAME_STATE_RUNNING AND NOT IS_BIT_SET( iLocalBoolCheck9, LBOOL9_HAS_FIRST_OBJECTIVE_TEXT_BEEN_SHOWN )
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Hiding HUD this frame because godtext hasn't been shown yet.")
		DISABLE_HUD()
	ENDIF
ENDPROC

FUNC BOOL HOLD_UP_VEHHACK_OBJECTIVE_TEXT(INT iObjTxtPartToUse)
	INT i
	
	FOR i = 0 TO FMMC_MAX_VEHICLES - 1
		IF IS_BIT_SET(MC_PlayerBD[iObjTxtPartToUse].iVehDestroyBitset, i)
		AND NOT IS_BIT_SET(iPlayedDialogueForVehHackBS, i)
			PRINTLN("[SecuroHack][ObjectiveText][GDTEXT] - HACK VEH - Blocking objective text due to veh ", i)
			REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_VEHICLE, FALSE #IF IS_DEBUG_BUILD , "HOLD_UP_VEHHACK_OBJECTIVE_TEXT" #ENDIF )
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC TEXT_LABEL_63 GET_CUSTOM_OBJECTIVE_TEXT_LABEL(INT iTeam, INT iRule, TEXT_LABEL_63 tl63CustomObjText)
	
	TEXT_LABEL_63 tl63ObjectiveText
	
	tl63ObjectiveText = tl63CustomObjText
	
	INT iR, iG, iB,iA
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_OVERRIDE_FRIENDLY_WITH_GANG)
		GET_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam,PlayerToUse),iR, iG, iB,iA)
		SET_SCRIPT_VARIABLE_HUD_COLOUR(iR, iG, iB,iA)
		PRINTLN("[RCC MISSION] PRE change: ", tl63ObjectiveText)
		REPLACE_STRING_IN_STRING(tl63ObjectiveText,"~f~", "~v~")
		PRINTLN("[RCC MISSION] POST change: ", tl63ObjectiveText, " ~v~ colour: ",iR,", ", iG,", ", iB,", ",iA)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_OBJECTIVE_TEXT_USES_RSG_OVERRIDE)
	AND g_FMMC_STRUCT.iRandomEntitySpawn_MaxNumber > 0
	AND RETURN_INT_CUSTOM_VARIABLE_VALUE(g_FMMC_STRUCT.iRandomEntitySpawn_RandomNumbersToUse) != 0
		
		INT i
		
		FOR i = 0 TO (g_FMMC_STRUCT.iRandomEntitySpawn_MaxNumber - 1)
			IF IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, i)
				IF i < ciMAX_NUM_OBJECTIVETEXT_RSG_OVERRIDES
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl63ObjectiveText_SpawnGroupOverride[i])
						tl63ObjectiveText = g_FMMC_STRUCT.tl63ObjectiveText_SpawnGroupOverride[i]
						i = g_FMMC_STRUCT.iRandomEntitySpawn_MaxNumber // Break out!
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] GET_CUSTOM_OBJECTIVE_TEXT_LABEL - Trying to check objective text for a spawn group > ciMAX_NUM_OBJECTIVETEXT_RSG_OVERRIDES!")
					PRINTLN("[RCC MISSION] GET_CUSTOM_OBJECTIVE_TEXT_LABEL - Trying to check objective text for a spawn group > ciMAX_NUM_OBJECTIVETEXT_RSG_OVERRIDES! i = ",i)					
					ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_OBJECTIVE_TEXT_OVERRIDE_LIMIT, ENTITY_RUNTIME_ERROR_TYPE_WARNING_SPAWN_POINT, "Trying to check objective text for a spawn group > ciMAX_NUM_OBJECTIVETEXT_RSG_OVERRIDES")
				#ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
	ENDIF
	
	
	
	RETURN tl63ObjectiveText
	
ENDFUNC

FUNC BOOL SHOULD_SHOW_BOUNDS_OBJECTIVE_TEXT()
	
	IF iBoundsToShowObjectiveText = -1
		// No bounds want to show objective text right now anyway
		RETURN FALSE
	ENDIF
	
	IF NOT DO_THESE_BOUNDS_HAVE_A_TIME_LIMIT(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].sRuleBounds[GET_LOCAL_PLAYER_CURRENT_RULE()][iBoundsToShowObjectiveText], TRUE)
		
		// These bounds aren't critical, so some other stuff can take priority instead if needed
		
		IF IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__OBJECTIVE_TEXT_OVERRIDE)
		AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[GET_TRIGGERING_ZONE_INDEX(ciFMMC_ZONE_TYPE__OBJECTIVE_TEXT_OVERRIDE)].iZoneCustomStringListSelection > -1
			// Zone taking priority
			RETURN FALSE
		ENDIF
		
		IF iClosestInteractable > -1
		AND NOT IS_STRING_NULL_OR_EMPTY(tlInteractableObjectiveText)
			// Interactable taking priority
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_SHOW_GAIN_WANTED_LEVEL_OBJECTIVE_TEXT()
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
	
	IF iRule < FMMC_MAX_RULES
		PRINTLN("[ML][SHOULD_SHOW_GAIN_WANTED_LEVEL_OBJECTIVE_TEXT] Number of stars = ", GET_PLAYER_WANTED_LEVEL(LocalPlayer), "   Creator set wanted requirement = ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedLevelReq[iRule])
		IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iWantedLevelReq[iRule] > -2
		AND IS_OBJECTIVE_BLOCKED()
		AND GET_PLAYER_WANTED_LEVEL(LocalPlayer) <= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedLevelReq[iRule]		// [ML] Creator set wanted level 0 = 1 star requried
			PRINTLN("[MJL][SHOULD_SHOW_GAIN_WANTED_LEVEL_OBJECTIVE_TEXT] Returning TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	PRINTLN("[MJL][SHOULD_SHOW_GAIN_WANTED_LEVEL_OBJECTIVE_TEXT] Returning FALSE")
	
	RETURN FALSE
ENDFUNC

FUNC TEXT_LABEL_63 PROCESS_OBJECTIVE_TEXT_COLOUR_CHANGE(TEXT_LABEL_63 tl63ObjectiveText)

	IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_CAPTURE_AREA
	AND IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
		IF NOT IS_STRING_NULL_OR_EMPTY(tl63ObjectiveText)
	        TEXT_LABEL_63 tl63LastChar, sTemp
			BOOL bColourChar = FALSE
			INT i = 0
			FOR i = 0 TO GET_LENGTH_OF_LITERAL_STRING(tl63ObjectiveText)-1
				IF i > 0
					IF ARE_STRINGS_EQUAL(tl63LastChar, "~")
						bColourChar = TRUE
					ENDIF
				ENDIF
				IF bColourChar
					IF g_iTheWinningTeam = -1
						//Show Yellow = Tied
						tl63LastChar = "y"
					ELIF g_iTheWinningTeam = 0
						//Team 0 - Orange
						tl63LastChar = "o"
					ELIF g_iTheWinningTeam = 1
						//Team 1 - Green
						tl63LastChar = "g"
					ELIF g_iTheWinningTeam = 2
						//Team 2 - Pink
						tl63LastChar = "q"
					ELIF g_iTheWinningTeam = 3
						//Team 3 - Purple
						tl63LastChar = "p"
					ENDIF
					sTemp += tl63LastChar
					sTemp += GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tl63ObjectiveText, i+1, GET_LENGTH_OF_LITERAL_STRING(tl63ObjectiveText))
	       			RETURN sTemp   
				ELSE
		        	tl63LastChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tl63ObjectiveText, i, i+1)
				ENDIF
				
		        sTemp += tl63LastChar
			ENDFOR
	        RETURN sTemp     
	    ENDIF
	ENDIF
	RETURN tl63ObjectiveText
ENDFUNC

FUNC BOOL PRINT_CUSTOM_OBJECTIVE(TEXT_LABEL_63 tl63CustomGodText)

	tl63CustomGodText = PROCESS_OBJECTIVE_TEXT_COLOUR_CHANGE(tl63CustomGodText)
	IF NOT IS_STRING_NULL_OR_EMPTY(tl63CustomGodText)
		IF NOT Has_This_MP_Objective_Text_User_Created_Been_Received(tl63CustomGodText)
			PRINTLN("[RCC MISSION] PRINT_CUSTOM_OBJECTIVE -inserted text is: ",tl63CustomGodText)
			PRINTLN("[RCC MISSION] PRINT_CUSTOM_OBJECTIVE -iclientstage = ",GET_MC_CLIENT_MISSION_STAGE(iPartToUse))
			Print_Objective_Text_User_Created(tl63CustomGodText)
			DEBUG_PRINTCALLSTACK()
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL PRINT_NORMAL_OBJECTIVE(STRING sobjective)

	IF NOT IS_STRING_NULL_OR_EMPTY(sobjective)
		IF NOT Has_This_MP_Objective_Text_Been_Received(sobjective)
			PRINTLN("PRINT_NORMAL_OBJECTIVE : ",sobjective)
			Print_Objective_Text(sobjective)
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL PRINT_COLOURED_OBJECTIVE(STRING sobjective,HUD_COLOURS HudColour)

INT R,G,B,A

	IF NOT IS_STRING_NULL_OR_EMPTY(sobjective)
		IF NOT Has_This_MP_Objective_Text_Been_Received(sobjective)
			GET_HUD_COLOUR(HudColour,R,G,B,A)
			SET_SCRIPT_VARIABLE_HUD_COLOUR(R,G,B,A)
			PRINTLN("[RCC MISSION] MY TEAM: ",MC_playerBD[iPartToUse].iteam)
			PRINTLN("[RCC MISSION] PRINT_COLOURED_OBJECTIVE HUD COLOUR: ",sobjective,ENUM_TO_INT(HudColour))

			Print_Objective_Text(sobjective)
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL PRINT_COLOURED_TEAM_OBJECTIVE( STRING sobjective, INT iteam )

	HUD_COLOURS teamcolour

	INT R,G,B,A

	IF NOT IS_STRING_NULL_OR_EMPTY(sobjective)
		IF NOT Has_This_MP_Objective_Text_With_User_Created_String_Been_Received(sobjective,g_sMission_TeamName[iteam])
			
			teamcolour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iteam, PlayerToUse)
			
			PRINTLN("[RCC MISSION] MY TEAM: ",MC_playerBD[iPartToUse].iteam)
			PRINTLN("[RCC MISSION] OTHER TEAM: ",iteam)
			PRINTLN("[RCC MISSION] PRINT_COLOURED_TEAM_OBJECTIVE WITH HUD COLOUR: ",sobjective,ENUM_TO_INT(teamcolour))

			GET_HUD_COLOUR(teamcolour,R,G,B,A)
			SET_SCRIPT_VARIABLE_HUD_COLOUR(R,G,B,A)

			TEXT_LABEL_31 LocalTitle
			
			IF IS_BIT_SET( iCelebrationTeamNameIsNotLiteral, iTeam )
				LocalTitle = TEXT_LABEL_TO_STRING( g_sMission_TeamName[ iTeam ] )
			ELSE
				LocalTitle = g_sMission_TeamName[iteam]
			ENDIF
			
			Print_Objective_Text_With_User_Created_String( sobjective, LocalTitle )
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL PRINT_CUSTOM_COLOURED_OBJECTIVE(TEXT_LABEL_63 tl63CustomGodText,HUD_COLOURS HudColour)

	INT R,G,B,A

	IF NOT IS_STRING_NULL_OR_EMPTY(tl63CustomGodText)
		IF NOT Has_This_MP_Objective_Text_User_Created_Been_Received(tl63CustomGodText)
			GET_HUD_COLOUR(HudColour,R,G,B,A)
			SET_SCRIPT_VARIABLE_HUD_COLOUR(R,G,B,A)
			
			PRINTLN("[RCC MISSION] MY TEAM: ",MC_playerBD[iPartToUse].iteam)
			PRINTLN("[RCC MISSION] PRINT_CUSTOM_COLOURED_OBJECTIVE WITH HUD COLOUR: ",tl63CustomGodText,ENUM_TO_INT(HudColour))

			Print_Objective_Text_User_Created(tl63CustomGodText)
		ENDIF
		RETURN TRUE
	ENDIF

	RETURN FALSE
	
ENDFUNC

FUNC BOOL PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE(TEXT_LABEL_63 tl63CustomGodText,INT iteam)

HUD_COLOURS teamcolour

INT R,G,B,A

	IF NOT IS_STRING_NULL_OR_EMPTY(tl63CustomGodText)
		IF NOT Has_This_MP_Objective_Text_User_Created_Been_Received(tl63CustomGodText)
			
			teamcolour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iteam, PlayerToUse)
			
			PRINTLN("[RCC MISSION] MY TEAM: ",MC_playerBD[iPartToUse].iteam)
			PRINTLN("[RCC MISSION] OTHER TEAM: ",iteam)
			PRINTLN("[RCC MISSION] PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE WITH HUD COLOUR: ",tl63CustomGodText,ENUM_TO_INT(teamcolour))
			GET_HUD_COLOUR(teamcolour,R,G,B,A)
			SET_SCRIPT_VARIABLE_HUD_COLOUR(R,G,B,A)

			Print_Objective_Text_User_Created(tl63CustomGodText)

		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL PRINT_OBJECTIVE_WITH_CUSTOM_STRING(STRING sMainText,STRING sInsertedText)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sMainText)
		IF NOT Has_This_MP_Objective_Text_With_User_Created_String_Been_Received(sMainText,sInsertedText)
			
			PRINTLN("[RCC MISSION] PRINT_OBJECTIVE_WITH_CUSTOM_STRING -inserted text is: ",sInsertedText)
			
			Print_Objective_Text_With_User_Created_String(sMainText,sInsertedText)
			
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL CURRENT_OBJECTIVE_WAITING_FOR_DIALOGUE_TO_FINISH()
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	BOOL bWaitingOnDialogue
	TEXT_LABEL_23 tlSpeaker = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	IF NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
	AND NOT ARE_STRINGS_EQUAL(tlSpeaker,"NULL")
		PRINTLN("[MMacK][DiagWait] Subtitles are off, and someone is speaking")	
		RETURN FALSE
	ENDIF
	
	IF iRule < FMMC_MAX_RULES
		INT iDiagToWaitFor = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDialogueToWaitFor[iRule]
		IF iDiagToWaitFor > -1
		AND NOT IS_VECTOR_ZERO(MC_GET_DIALOGUE_TRIGGER_POSITION(iDiagToWaitFor))
			IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iDialoguePlayedBS[iDiagToWaitFor/32],iDiagToWaitFor%32)
			AND NOT IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDiagToWaitFor].iDialogueBitset,iBS_DialogueNotHeardByTeam0 + iTeam)
				PRINTLN("[MMacK][DiagWait] Not yet played dialogue ",iDiagToWaitFor,", so cant show the objective")	
				bWaitingOnDialogue = TRUE
				
				IF Is_MP_Objective_Text_On_Display()
					Clear_Any_Objective_Text()
				ENDIF
			ELSE
				
				TEXT_LABEL_23 tlConvLbl = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				
				IF NOT ARE_STRINGS_EQUAL(tlConvLbl,"NULL")
					bWaitingOnDialogue = TRUE
					IF NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
						IF Is_MP_Objective_Text_On_Display()
							Clear_Any_Objective_Text()
						ENDIF
					ELSE
						Clear_Any_Objective_Text_From_This_Script()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		INT iDiagToWaitForMidPoint = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDialogueToWaitForMidPoint[iRule]
		IF iDiagToWaitForMidPoint > -1
		AND NOT IS_VECTOR_ZERO(MC_GET_DIALOGUE_TRIGGER_POSITION(iDiagToWaitForMidPoint))
			IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iRule)
				IF NOT FMMC_IS_LONG_BIT_SET(MC_playerBD[iPartToUse].iDialoguePlayedBS, iDiagToWaitForMidPoint)
				AND NOT IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDiagToWaitForMidPoint].iDialogueBitset,iBS_DialogueNotHeardByTeam0 + iTeam)
					PRINTLN("[MMacK][DiagWait] Not yet played dialogue ",iDiagToWaitForMidPoint,", so cant show the objective MIDPOINT")	
					bWaitingOnDialogue = TRUE
					
					IF Is_MP_Objective_Text_On_Display()
						Clear_Any_Objective_Text()
					ENDIF
				ELSE
					
					TEXT_LABEL_23 tlConvLbl = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
					
					IF NOT ARE_STRINGS_EQUAL(tlConvLbl,"NULL")
						bWaitingOnDialogue = TRUE
						IF NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							IF Is_MP_Objective_Text_On_Display()
								Clear_Any_Objective_Text()
							ENDIF
						ELSE
							Clear_Any_Objective_Text_From_This_Script()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bWaitingOnDialogue
ENDFUNC

/// PURPOSE:
///    Lose Gang Chase objective text is different depending on the chasing gang 
FUNC STRING GET_LOSE_GANG_CHASE_OBJECTIVE_TEXT()
	STRING sObj
	
	INT iTeam = MC_playerBD[ iPartToUse].iteam
	
	// get the unit index of the most recent gang chase unit
	INT iGangChaseIterator
	INT iGangChaseUnitIndex = -1
	FOR iGangChaseIterator = 0 TO ciMAX_GANG_CHASE_VEHICLES - 1
		IF GET_GANG_CHASE_UNIT_GANG_TYPE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], iGangChaseIterator) != ciBACKUP_TYPE_NONE
			iGangChaseUnitIndex = iGangChaseIterator
			BREAKLOOP
		ENDIF
	ENDFOR
	
	IF iGangChaseUnitIndex < 0
		NET_SCRIPT_ASSERT("GET_LOSE_GANG_CHASE_OBJECTIVE_TEXT - no valid gang chase is active")
		RETURN sObj
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[iteam]], ciBS_RULE3_GANG_CHASE_ALT_OBJECTIVE_TEXT)		
		SWITCH MC_serverBD_1.sGangChase[iGangChaseUnitIndex].iGangType
			CASE ciBACKUP_TYPE_MERRYWEATHER      			sObj = "MC_LSE_MER"			BREAK
			CASE ciBACKUP_TYPE_LOST              			sObj = "MC_LSE_LOST"		BREAK
			CASE ciBACKUP_TYPE_LOST_SLAMVAN2        	   	sObj = "MC_LSE_LOST"		BREAK
			CASE ciBACKUP_TYPE_VAGOS             			sObj = "MC_LSE_VAG"			BREAK
			CASE ciBACKUP_TYPE_FAMILIES          			sObj = "MC_LSE_FAM"			BREAK
			CASE ciBACKUP_TYPE_PROFESSIONALS     			sObj = "MC_LSE_PROF"		BREAK
			CASE ciBACKUP_TYPE_BOAT_PROFESSIONALS_DINGHY	sObj = "MC_LSE_PROF"		BREAK
			CASE ciBACKUP_TYPE_BALLAS            			sObj = "MC_LSE_BALL"		BREAK
			CASE ciBACKUP_TYPE_SALVA             			sObj = "MC_LSE_SALV"		BREAK
			CASE ciBACKUP_TYPE_ALTRUISTS         			sObj = "MC_LSE_ALTR"		BREAK
			CASE ciBACKUP_TYPE_FIB               			sObj = "MC_LSE_FIB"			BREAK
			CASE ciBACKUP_TYPE_ONEIL_BROS        			sObj = "MC_LSE_ONL"			BREAK
			CASE ciBACKUP_TYPE_KOREANS           			sObj = "MC_LSE_KOR"			BREAK
			CASE ciBACKUP_TYPE_BOAT_KOREANS_DINGHY       	sObj = "MC_LSE_KOR"			BREAK
			CASE ciBACKUP_TYPE_MERRYWEATHER_MESAS			sObj = "MC_LSE_MER"			BREAK
			CASE ciBACKUP_TYPE_KOREANS2          			sObj = "MC_LSE_KOR"			BREAK
			CASE ciBACKUP_TYPE_ARMY							sObj = "MC_LSE_ARMY"		BREAK
			CASE ciBACKUP_TYPE_ARMY2						sObj = "MC_LSE_ARMY"		BREAK
			CASE ciBACKUP_TYPE_LOWRIDER_VAGOS_BLA			sObj = "MC_LSE_VAG"		    BREAK
			CASE ciBACKUP_TYPE_LOWRIDER_VAGOS_EMP			sObj = "MC_LSE_VAG"		    BREAK
			CASE ciBACKUP_TYPE_LOWRIDER_BALLAS		  	  	sObj = "MC_LSE_BALL"		BREAK
			CASE ciBACKUP_TYPE_NIGHTSHARK_BOGDAN_GOON
			CASE ciBACKUP_TYPE_INSURGENT_BOGDAN_GOON
			CASE ciBACKUP_TYPE_INSURGENT2_BOGDAN_GOON
			CASE ciBACKUP_TYPE_AIR_BUZZ_AVON_GOON
			CASE ciBACKUP_TYPE_NIGHTSHARK_AVON_GOON
			CASE ciBACKUP_TYPE_DUBSTA3_AVON_GOON
				sObj = "MC_LSE_BG"
				BREAK
			CASE ciBACKUP_TYPE_KAMACHO_PROFESSIONALS		sObj = "MC_LSE_PROF"		BREAK
			CASE ciBACKUP_TYPE_CONTENDER_PROFESSIONALS		sObj = "MC_LSE_PROF"		BREAK
			CASE ciBACKUP_TYPE_CARACARA_HILLBILLY			sObj = "MC_LSE_HILL"		BREAK
				
			DEFAULT
				sObj = "MC_LSE_GEN"
			BREAK	
		ENDSWITCH
	ELSE		
		SWITCH MC_serverBD_1.sGangChase[iGangChaseUnitIndex].iGangType
			CASE ciBACKUP_TYPE_MERRYWEATHER      			sObj = "MC_TKO_MER"			BREAK
			CASE ciBACKUP_TYPE_LOST              			sObj = "MC_TKO_LOST"		BREAK
			CASE ciBACKUP_TYPE_LOST_SLAMVAN2        	   	sObj = "MC_TKO_LOST"		BREAK
			CASE ciBACKUP_TYPE_VAGOS             			sObj = "MC_TKO_VAG"			BREAK
			CASE ciBACKUP_TYPE_FAMILIES          			sObj = "MC_TKO_FAM"			BREAK
			CASE ciBACKUP_TYPE_PROFESSIONALS     			sObj = "MC_TKO_PROF"		BREAK
			CASE ciBACKUP_TYPE_BOAT_PROFESSIONALS_DINGHY 	sObj = "MC_TKO_PROF"		BREAK
			CASE ciBACKUP_TYPE_BALLAS            			sObj = "MC_TKO_BALL"		BREAK
			CASE ciBACKUP_TYPE_SALVA             			sObj = "MC_TKO_SALV"		BREAK
			CASE ciBACKUP_TYPE_ALTRUISTS         			sObj = "MC_TKO_ALTR"		BREAK
			CASE ciBACKUP_TYPE_FIB               			sObj = "MC_TKO_FIB"			BREAK
			CASE ciBACKUP_TYPE_ONEIL_BROS        			sObj = "MC_TKO_ONL"			BREAK
			CASE ciBACKUP_TYPE_KOREANS           			sObj = "MC_TKO_KOR"			BREAK
			CASE ciBACKUP_TYPE_MERRYWEATHER_MESAS			sObj = "MC_TKO_MER"			BREAK
			CASE ciBACKUP_TYPE_BOAT_KOREANS_DINGHY    	 	sObj = "MC_TKO_KOR"			BREAK
			CASE ciBACKUP_TYPE_KOREANS2          			sObj = "MC_TKO_KOR"			BREAK
			CASE ciBACKUP_TYPE_ARMY							sObj = "MC_TKO_ARMY"		BREAK
			CASE ciBACKUP_TYPE_ARMY2						sObj = "MC_TKO_ARMY"		BREAK
			CASE ciBACKUP_TYPE_LOWRIDER_VAGOS_BLA			sObj = "MC_TKO_VAG"		    BREAK
			CASE ciBACKUP_TYPE_LOWRIDER_VAGOS_EMP			sObj = "MC_TKO_VAG"		    BREAK
			CASE ciBACKUP_TYPE_LOWRIDER_BALLAS		  	  	sObj = "MC_TKO_BALL"		BREAK
			CASE ciBACKUP_TYPE_NIGHTSHARK_BOGDAN_GOON
			CASE ciBACKUP_TYPE_INSURGENT_BOGDAN_GOON
			CASE ciBACKUP_TYPE_INSURGENT2_BOGDAN_GOON
			CASE ciBACKUP_TYPE_AIR_BUZZ_AVON_GOON
			CASE ciBACKUP_TYPE_NIGHTSHARK_AVON_GOON
			CASE ciBACKUP_TYPE_DUBSTA3_AVON_GOON
				sObj = "MC_TKO_BG"		
				BREAK
			CASE ciBACKUP_TYPE_KAMACHO_PROFESSIONALS		sObj = "MC_TKO_PROF"		BREAK
			CASE ciBACKUP_TYPE_CONTENDER_PROFESSIONALS		sObj = "MC_TKO_PROF"		BREAK			
			CASE ciBACKUP_TYPE_CARACARA_HILLBILLY			sObj = "MC_TKO_HILL"		BREAK
			
			DEFAULT
				sObj = "MC_TKO_GEN"
			BREAK	
		ENDSWITCH
	ENDIF
	RETURN sObj
ENDFUNC

FUNC BOOL DOES_RULE_REQUIRE_A_SPECIFIC_VEHICLE(INT iLoc = -1)
	
	IF iLoc = -1
		iLoc = MC_serverBD.iPriorityLocation[MC_playerBD[iPartToUse].iteam]
	ENDIF
	
	IF iLoc != -1
	AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].mnVehicleNeeded[GET_LOCAL_PLAYER_TEAM(TRUE)] != DUMMY_MODEL_FOR_SCRIPT
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_SpecificVehModelFullBlocker)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_SHOW_CUSTOM_LOCATION_OBJECTIVE_TEXT(INT iLoc, INT iPart)
	IF NOT IS_BIT_SET(MC_playerBD[iPart].iLocationInSecondaryLocation, iLoc)
		PRINTLN("SHOULD_SHOW_CUSTOM_LOCATION_OBJECTIVE_TEXT - iLoc: ", iLoc, " Not in secondary Location")
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocTriggerInput.iInputRequired != LOCATIONS_TRIGGER_INPUT_INVALID
	AND IS_BIT_SET(MC_serverBD_4.iLocationWaitingForInputBitset, iLoc)
		PRINTLN("SHOULD_SHOW_CUSTOM_LOCATION_OBJECTIVE_TEXT - iLoc: ", iLoc, " Not waiting for input")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL PROCESS_EXTERNAL_OBJECTIVE_TEXT_OVERRIDES(BOOL& bObjectiveDisp, BOOL& bCustomObjective)

	IF iClosestInteractable > -1 // Current Interactable Objective Text
	AND NOT IS_STRING_NULL_OR_EMPTY(tlInteractableObjectiveText)
		IF PRINT_CUSTOM_OBJECTIVE(tlInteractableObjectiveText)
			#IF IS_DEBUG_BUILD
			IF bDebugPrintCustomText
				PRINTLN("[RCC MISSION][Interactables_SPAM] PROCESS_EXTERNAL_OBJECTIVE_TEXT_OVERRIDES - Overridden to be ", tlInteractableObjectiveText)
			ENDIF
			#ENDIF
			
			bObjectiveDisp = TRUE
			bCustomObjective = TRUE
			RETURN TRUE
		ENDIF
		
	ELIF IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__OBJECTIVE_TEXT_OVERRIDE) // Objective Text Zone
	AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[GET_TRIGGERING_ZONE_INDEX(ciFMMC_ZONE_TYPE__OBJECTIVE_TEXT_OVERRIDE)].iZoneCustomStringListSelection > -1
		
		IF NOT IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_OVERRIDE)
			#IF IS_DEBUG_BUILD
			IF bDebugPrintCustomText
				PRINTLN("[RCC MISSION][Zones][Zone ", GET_TRIGGERING_ZONE_INDEX(ciFMMC_ZONE_TYPE__OBJECTIVE_TEXT_OVERRIDE), "] PROCESS_EXTERNAL_OBJECTIVE_TEXT_OVERRIDES - waiting for update.")
			ENDIF
			#ENDIF
			
			REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_EXTERNAL_OBJECTIVE_TEXT_OVERRIDES - about to start showing zone text" #ENDIF )
			CLEAR_REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_OVERRIDE)
			
			bObjectiveDisp = TRUE
			bCustomObjective = TRUE
			RETURN TRUE
		ENDIF
		
		TEXT_LABEL_63 tlZoneObjectiveText = GET_ZONE_STRING_LIST_STRING(GET_TRIGGERING_ZONE_INDEX(ciFMMC_ZONE_TYPE__OBJECTIVE_TEXT_OVERRIDE))

		IF PRINT_CUSTOM_OBJECTIVE(tlZoneObjectiveText)
			#IF IS_DEBUG_BUILD
			IF bDebugPrintCustomText
				PRINTLN("[RCC MISSION][Zones][Zone ", GET_TRIGGERING_ZONE_INDEX(ciFMMC_ZONE_TYPE__OBJECTIVE_TEXT_OVERRIDE), "] PROCESS_EXTERNAL_OBJECTIVE_TEXT_OVERRIDES - Overridden to be '", tlZoneObjectiveText, "'")
			ENDIF
			#ENDIF
			
			bObjectiveDisp = TRUE
			bCustomObjective = TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MISSION_READY_TO_PROCESS_OBJECTIVE_TEXT()
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfZones > 0
	AND NOT IS_BIT_SET(iEntityFirstStaggeredLoopComplete, ci_ZONES_FIRST_STAGGERED_LOOP_COMPLETE)
		PRINTLN("IS_MISSION_READY_TO_PROCESS_OBJECTIVE_TEXT | Waiting for ci_ZONES_FIRST_STAGGERED_LOOP_COMPLETE")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(iEntityFirstStaggeredLoopComplete, ci_BOUNDS_FIRST_STAGGERED_LOOP_COMPLETE)
		PRINTLN("IS_MISSION_READY_TO_PROCESS_OBJECTIVE_TEXT | Waiting for ci_BOUNDS_FIRST_STAGGERED_LOOP_COMPLETE")
		RETURN FALSE
	ENDIF
	
	IF g_OfficeHeliDockData.bDoingCutscene
		PRINTLN("IS_MISSION_READY_TO_PROCESS_OBJECTIVE_TEXT | Blocked by g_OfficeHeliDockData.bDoingCutscene")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_OBJECTIVE_TEXT_BE_BLOCKED_BY_MINIGAME_THIS_FRAME()
	IF sInteractWithVars.eInteractWith_CurrentState != IW_STATE_IDLE
	AND IS_BIT_SET(sInteractWithVars.iInteractWith_Bitset, ciInteractWith_Bitset__HideObjectiveTextForThisAnim)
		PRINTLN("[InteractWith_SPAM] SHOULD_OBJECTIVE_TEXT_BE_BLOCKED_BY_MINIGAME_THIS_FRAME - Setting bBlockObjectiveTextThisFrame to TRUE || Blocked by ciInteractWith_Bitset__HideObjectiveTextForThisAnim")
		RETURN TRUE
	ENDIF
	
	IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_INTERROGATION
		IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_INTERROGATION_READY_FOR_OBJECTIVE_TEXT)
			PRINTLN("[Interrogation_SPAM] SHOULD_OBJECTIVE_TEXT_BE_BLOCKED_BY_MINIGAME_THIS_FRAME - Setting bBlockObjectiveTextThisFrame to TRUE || Blocked by LBOOL27_INTERROGATION_READY_FOR_OBJECTIVE_TEXT")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// [FIX_2020_CONTROLLER] - Shared Script Print logging Tag... 
// [FIX_2020_CONTROLLER] - We really need to write a new function that supports 5+ objective texts for each rule type and well-docment how each one of thee trigger.
// There are so many hacks in here to get objective text to play, and really we should just have clear events that trigger each one for each rule. This is a mess, see down.
// manages the god text for the mission objectives.
PROC PROCESS_OBJECTIVE_TEXT()
	TEXT_LABEL_63 tl63CustomGodText
	TEXT_LABEL_63 tl63CustomGodText1
	TEXT_LABEL_63 tl63CustomGodText2
	BOOL bobjectivedisp
	BOOL bcustomobjective 
	BOOL bDispHelpObjective
	BOOL bNoSecondObjective
	BOOL bPlural
	INT iTeam, i
	BOOL bFoundBlip
	BOOL bBlockObjectiveTextThisFrame = FALSE
	INT iNumberOfPlayersLeft
	INT iNumberOfPlayersRequiredToLeave
	INT iAvengerDriver = -1
	BOOL bAvengerHasDriver = FALSE

	INT iObjTxtPartToUse = iPartToUse
	PLAYER_INDEX ObjTxtPlayerToUse = PlayerToUse
	
	IF (bIsAnySpectator AND GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_SETUP)
		PRINTLN("PROCESS_OBJECTIVE_TEXT - Setting bBlockObjectiveTextThisFrame to TRUE || Blocking objective text because we're a spectator the player we're watching hasn't started playing yet")
		bBlockObjectiveTextThisFrame = TRUE
	ENDIF
	
	IF sWarpPortal.eWarpPortalState > eWarpPortalState_Idle
	AND sWarpPortal.eWarpPortalState < eWarpPortalState_Cleanup
		bBlockObjectiveTextThisFrame = TRUE
		REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - Using a warp portal" #ENDIF )
	ENDIF
	
	IF SHOULD_OBJECTIVE_TEXT_BE_BLOCKED_BY_MINIGAME_THIS_FRAME()
		bBlockObjectiveTextThisFrame = TRUE
	ENDIF
	
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]	

	IF (NOT HAS_TEAM_FINISHED( MC_playerBD[ iObjTxtPartToUse ].iteam ))
	AND (NOT IS_BIT_SET( MC_playerBD[ iObjTxtPartToUse ].iClientBitSet, PBBOOL_FINISHED ))
	AND (NOT IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet2, PBBOOL2_PLAYER_OUT_BOUNDS_FAIL))
	AND NOT IS_CUTSCENE_PLAYING()
	AND NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
	AND (NOT (bIsAnySpectator AND GET_SPECTATOR_CURRENT_FOCUS_PED() != GET_SPECTATOR_DESIRED_FOCUS_PED()))
	AND NOT (bBlockObjectiveTextThisFrame)
	AND (NOT g_bCelebrationScreenIsActive OR bIsAnySpectator)
	AND (NOT g_bMissionEnding OR bIsAnySpectator)
	AND NOT IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
		
		IF (GET_FRAME_COUNT() % 30) = 0
			PRINTLN("PROCESS_OBJECTIVE_TEXT - We are okay to do Objective Text Stuff...")
		ENDIF
		
		PROCESS_DPAD_SPECIFIC_HELP_TEXT()
		
		IF IS_CELLPHONE_CAMERA_IN_USE()
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("Lowrider_Help_25")
				CLEAR_HELP(TRUE)
			ENDIF
		ENDIF
		
		IF IS_CIRCUIT_HACKING_MINIGAME_RUNNING(hackingMinigameData)
			EXIT
		ENDIF
		
		// Force Target Score bottom Right HUD.
		IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_FORCED_TARGET_SCORE_HUD)
			AND NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
			AND bPlayerToUseOk
				INT iTargetScore = g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit - g_FMMC_STRUCT.iInitialPoints[0]
				DRAW_GENERIC_BIG_NUMBER(iTargetScore, "MC_OSCRLIMY", -1, HUD_COLOUR_WHITE,HUDORDER_SEVENTHBOTTOM)
			ENDIF
		ENDIF
		
		IF iRule < FMMC_MAX_RULES
			PROCESS_ENTITY_DESTROYED_HUD(MC_playerBD[iObjTxtPartToUse].iteam, iRule)
		ENDIF
		
		MANAGE_DELIVERY_BLIP()		
		
		PROCESS_SHARD_TEXT()
		
		PROCESS_CARNAGE_BAR_HUD()
	
		MAINTAIN_RUINER_AMMO_UI()
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_COUNTERMEASURE_UI)
			DISPLAY_COUNTERMEASURE_COOLDOWN()
		ENDIF
		
		IF iRule < FMMC_MAX_RULES
			IF bLocalPlayerPedOK
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetEight[iRule], ciBS_RULE8_GPS_TRACKING_DISTANCE_METER)
					IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
						INT iBars = 0
						FOR iBars = 0 TO ci_TOTAL_DISTANCE_CHECK_BARS-1
							IF iGPSTrackingMeterPedIndex[iBars] > -1
								IF DOES_PED_REQUIRE_DISTANCE_TRACKING(iGPSTrackingMeterPedIndex[iBars], iRule)
									IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iGPSTrackingMeterPedIndex[iBars]])
										IF NOT IS_PED_INJURED(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iGPSTrackingMeterPedIndex[iBars]]))
											MAINTAIN_GPS_DISTANCE_METER_BAR(iBars, iRule)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF bIsAnySpectator
			IF iLastSpectatedTarget != iSpectatorTarget
			OR iLastPartToUse != iObjTxtPartToUse
				iLastSpectatedTarget = iSpectatorTarget
				iLastPartToUse = iObjTxtPartToUse
				REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - iLastSpectatedTarget != iSpectatorTarget" #ENDIF )
			ENDIF
		ENDIF
		
		IF MC_playerBD_1[iLocalPart].iVehicleNeededToRepair != iOldMissionVehicleNeededToRepair
			PRINTLN("[RCC MISSION][ModShopProg] - iVehicleNeededToRepair changed to: ", MC_playerBD_1[iLocalPart].iVehicleNeededToRepair, " Update Objective Text")
			iOldMissionVehicleNeededToRepair = MC_playerBD_1[iLocalPart].iVehicleNeededToRepair
			REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - Clearing old Roam Spec Override" #ENDIF )
		ENDIF
		
		IF iRule < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_TEAM_VEHICLE_DROP_OFF_OBJ_TEXT)
				IF ARE_ANY_VALID_PLAYERS_DRIVING_DELIVERY_VEHICLE()
					IF NOT IS_BIT_SET(iLocalboolCheck31, LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH)
						PRINTLN("[LM][UOBJ - OBJECT][SPEC_SPEC] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - Setting LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH")
						SET_BIT(iLocalboolCheck31, LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH)
						REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH" #ENDIF )
					ENDIF
				ELSE
					IF IS_BIT_SET(iLocalboolCheck31, LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH) 
						PRINTLN("[LM][UOBJ - OBJECT][SPEC_SPEC] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - Clearing LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH")
						CLEAR_BIT(iLocalboolCheck31, LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH) 
						REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH (not)" #ENDIF )
					ENDIF
				ENDIF
			ELIF IS_BIT_SET(iLocalboolCheck31, LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH) 
				PRINTLN("PROCESS_OBJECTIVE_TEXT - LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH left on from previous rule.")
				CLEAR_BIT(iLocalboolCheck31, LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH) 
			ENDIF
		ENDIF
		
		PROCESS_RESETTING_OVERRIDDEN_OBJECTIVE_TEXT_DATA()
		
		IF NOT SHOULD_SHOW_BOUNDS_OBJECTIVE_TEXT()
		
			IF (GET_FRAME_COUNT() % 30) = 0
				PRINTLN("PROCESS_OBJECTIVE_TEXT - Checking the current objective conditions...")
			ENDIF
			
			IF MC_playerBD[iObjTxtPartToUse].iWanted != iPrevWantedForHUD
				iPrevWantedForHUD = MC_playerBD[iObjTxtPartToUse].iWanted
				REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_WANTED, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - iPrevWantedForHUD changed" #ENDIF )
			ENDIF
		
			IF IS_CURRENT_OBJECTIVE_LOSE_WANTED()
				
				IF MC_playerBD[iObjTxtPartToUse].iWanted > 0
					PRINT_NORMAL_OBJECTIVE( "MC_LOSE_COPS" )
				ELSE
					IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_WANTED)
						// Else if the whole team/all friendly players have to lose the wanted level
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE_WANTED_TEAM_OBJ )
						OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE_WANTED_COOP_TEAMS_OBJ )
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE8_OVERRIDE_FRIENDLY_WITH_GANG)
								INT iR, iG, iB, iA
								GET_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam,LocalPlayer),iR, iG, iB,iA)
								SET_SCRIPT_VARIABLE_HUD_COLOUR(iR, iG, iB,iA)
								PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - Using gang colour lose police text. Colour: ", iR,", ", iG, ", ", iB)
								PRINT_NORMAL_OBJECTIVE("MC_HLOSE_G_COP")
							ELSE
								PRINT_NORMAL_OBJECTIVE("MC_HLOSE_COP")
							ENDIF
						ENDIF
					ENDIF
				ENDIF

				IF iSpectatorTarget = -1
					IF NOT IS_OBJECTIVE_BLOCKED()
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM0_TARGET)
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM1_TARGET)
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM2_TARGET)
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM3_TARGET)
						PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, Clearing g_FMMC_STRUCT.iTargetTeamBlipBitset")
					ENDIF
					
					BLOCK_OBJECTIVE_THIS_FRAME()
				ENDIF
				
				INT iStage = GET_MC_CLIENT_MISSION_STAGE(iObjTxtPartToUse)
				BOOL bCleanUpBlips = TRUE
				IF (iStage = CLIENT_MISSION_STAGE_COLLECT_OBJ
				OR iStage = CLIENT_MISSION_STAGE_COLLECT_VEH
				OR iStage = CLIENT_MISSION_STAGE_COLLECT_PED)
					IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[ MC_playerBD[ iObjTxtPartToUse ].iteam ]], ciBS_RULE3_OVERRIDE_COLLECTION_TEXT)
						bCleanUpBlips = FALSE
					ENDIF
				ENDIF
				
				IF bCleanUpBlips
					CLEANUP_OBJECTIVE_BLIPS()
				ENDIF
				
				REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - IS_CURRENT_OBJECTIVE_LOSE_WANTED" #ENDIF )
				CLEAR_REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_WANTED)
				SET_BIT(iLocalBoolCheck3, LBOOL3_DISABLE_GPSANDHINTCAM)
				
			ELIF CURRENT_OBJECTIVE_WAITING_FOR_DIALOGUE_TO_FINISH()
				
				REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - CURRENT_OBJECTIVE_WAITING_FOR_DIALOGUE_TO_FINISH" #ENDIF )
				
				SET_BIT(iLocalBoolCheck3, LBOOL3_DISABLE_GPSANDHINTCAM)
			
			ELIF IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_OBJECTIVE_TEXT_OVERRIDDEN)
			AND iCustomObjectiveTextOverride != -1
				
				TEXT_LABEL_63 tlCustomObjText = GET_CUSTOM_STRING_LIST_TEXT_LABEL(iCustomObjectiveTextOverride)
				IF NOT Is_This_The_Current_Objective_Text(tlCustomObjText)							
					Clear_Any_Objective_Text()
					PRINT_CUSTOM_OBJECTIVE(tlCustomObjText)	
				ENDIF
				bobjectivedisp = TRUE
				bcustomobjective = TRUE
				
				REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - Text Overridden" #ENDIF )
			
			ELIF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_SHOULD_USE_CUSTOM_HELP_WAIT_TEXT_TAXI)
								
				PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, LBOOL32_SHOULD_USE_CUSTOM_HELP_WAIT_TEXT_TAXI")
				TEXT_LABEL_63 tlWaitText
				tlWaitText = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl63WaitMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]]
				tlWaitText = GET_CUSTOM_OBJECTIVE_TEXT_LABEL(MC_playerBD[iObjTxtPartToUse].iteam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam], tlWaitText) 
				
				IF NOT IS_STRING_NULL_OR_EMPTY(tlWaitText)
					PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, LBOOL32_SHOULD_USE_CUSTOM_HELP_WAIT_TEXT_TAXI - We should be printing wait message.")
					IF NOT Is_This_The_Current_Objective_Text(tlWaitText)
						PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, LBOOL32_SHOULD_USE_CUSTOM_HELP_WAIT_TEXT_TAXI - printing wait message.")
						Clear_Any_Objective_Text()
						PRINT_CUSTOM_OBJECTIVE(tlWaitText)
					ENDIF
					bobjectivedisp = TRUE
					bcustomobjective = TRUE
					
					REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - IS_CURRENT_OBJECTIVE_REPAIR_VEHICLE" #ENDIF )
					
				ENDIF
				
			ELIF IS_CURRENT_OBJECTIVE_REPAIR_VEHICLE()
				PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, CURRENT_OBJECTIVE_REPAIR_CAR")
				BOOL bCanDisplayRepairObjective = TRUE
				
				IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_DELIVER_VEH
				AND NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[iObjTxtPartToUse].iTeam], iRule)
					bCanDisplayRepairObjective = FALSE
					PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, CURRENT_OBJECTIVE_REPAIR_CAR || bCanDisplayRepairObjective = FALSE because we're not properly at the midpoint yet")
				ENDIF
				
				IF bCanDisplayRepairObjective
					// This option utilizes wait text separately. We have default repair text so we can use that instead of the customwait one.
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iVehRuleWaitForTeamsBS[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]][MC_playerBD[iObjTxtPartToUse].iTeam], ciFMMC_VehRuleTeamBS_ALL_TEAM_GOTO)
						TEXT_LABEL_63 tlWaitText
						tlWaitText = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl63WaitMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]]
						IF NOT IS_STRING_NULL_OR_EMPTY(tlWaitText)					
							PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, CURRENT_OBJECTIVE_REPAIR_CAR - We should be printing wait message.")
							IF NOT Is_This_The_Current_Objective_Text(tlWaitText)							
								PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, CURRENT_OBJECTIVE_REPAIR_CAR - printing wait message.")
								Clear_Any_Objective_Text()
								PRINT_CUSTOM_OBJECTIVE(tlWaitText)	
							ENDIF
							bobjectivedisp = TRUE
							bcustomobjective = TRUE
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, CURRENT_OBJECTIVE_REPAIR_CAR - Blocking Cusatom Wait Text - Using it for ciFMMC_VehRuleTeamBS_ALL_TEAM_GOTO.")
					ENDIF
					
					IF NOT bcustomobjective
						IF (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetThirteen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE13_BLOCK_PROGRESSION_BF_MODSHOP_RESPRAY)
						OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetThirteen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE13_BLOCK_PROGRESSION_BF_MODSHOP_LICENSE_CHNG))
						AND IS_OBJECTIVE_BLOCKED()
							TEXT_LABEL_63 tl63_ModShop = "OBJ_MODSHP_BLK"
							PRINT_NORMAL_OBJECTIVE(tl63_ModShop)
						ELIF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl23ObjSingular[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]])
							PRINT_OBJECTIVE_WITH_CUSTOM_STRING("OBJ_REP_C",g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl23ObjSingular[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]])
						ELSE
							IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[i].tl63Objective2[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]])
								PRINTLN("[JS] - PROCESS_OBJECTIVE_TEXT - CURRENT_OBJECTIVE_REPAIR_CAR - Printing Custom Text 2.")
								TEXT_LABEL_63 tl63 = GET_CUSTOM_OBJECTIVE_TEXT_LABEL(MC_playerBD[iObjTxtPartToUse].iTeam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam], g_FMMC_STRUCT.sFMMCEndConditions[i].tl63Objective2[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]])
								PRINT_CUSTOM_OBJECTIVE(tl63)
								bobjectivedisp = TRUE
								bcustomobjective = TRUE				
							ELSE
								PRINT_NORMAL_OBJECTIVE("OBJ_REPCR")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_ForceGpsToNearestModShop)
					IF NOT IS_PLAYER_IN_ANY_SHOP()
						BLIP_INDEX tempBlip = GET_CLOSEST_BLIP_INFO_ID(RADAR_TRACE_CAR_MOD_SHOP)
						IF DOES_BLIP_EXIST(tempBlip)
							IF biModshopBlip != tempBlip
							PRINTLN("[ML] changing nearest mod shop blip handle")
								CLEANUP_MODSHOP_BLIP_SETTINGS()
								biModshopBlip = tempBlip
							ENDIF						
						
							IF NOT DOES_BLIP_HAVE_GPS_ROUTE(biModshopBlip)
								SET_BLIP_ROUTE(biModshopBlip, TRUE)
								SET_BLIP_ROUTE_COLOUR(biModshopBlip, BLIP_COLOUR_YELLOW)
								SET_BLIP_COLOUR(biModshopBlip, BLIP_COLOUR_YELLOW)								
								SET_BLIP_AS_SHORT_RANGE(biModshopBlip, FALSE)
							ENDIF
						ELSE // Blip doesn't exist
							CLEANUP_MODSHOP_BLIP_SETTINGS()
						ENDIF
					ELSE // Player is not in a shop					
						CLEANUP_MODSHOP_BLIP_SETTINGS()
					ENDIF
				ENDIF
				
				CLEANUP_OBJECTIVE_BLIPS()
				
				IF iSpectatorTarget = -1
					IF NOT IS_OBJECTIVE_BLOCKED()
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM0_TARGET)
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM1_TARGET)
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM2_TARGET)
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM3_TARGET)
						PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, Clearing g_FMMC_STRUCT.iTargetTeamBlipBitset")
					ENDIF
					
					BLOCK_OBJECTIVE_THIS_FRAME()
				ENDIF
						
				REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - IS_CURRENT_OBJECTIVE_REPAIR_VEHICLE" #ENDIF )
				SET_BIT(iLocalBoolCheck3, LBOOL3_DISABLE_GPSANDHINTCAM)
				
			ELIF IS_CURRENT_OBJECTIVE_COLLECT_OBJECT()
			
				TEXT_LABEL_63 tlWaitText
				tlWaitText = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl63WaitMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]]
				IF NOT IS_STRING_NULL_OR_EMPTY(tlWaitText)					
					PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, IS_CURRENT_OBJECTIVE_COLLECT_OBJECT - We should be printing wait message.")
					IF NOT Is_This_The_Current_Objective_Text(tlWaitText)							
						PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, IS_CURRENT_OBJECTIVE_COLLECT_OBJECT - printing wait message.")
						Clear_Any_Objective_Text()
						PRINT_CUSTOM_OBJECTIVE(tlWaitText)	
					ENDIF
					bobjectivedisp = TRUE
					bcustomobjective = TRUE
				ENDIF
				
				IF !bcustomobjective 
					PRINT_NORMAL_OBJECTIVE("MC_C_OBJ")
				ENDIF
				
				IF iSpectatorTarget = -1
					IF NOT IS_OBJECTIVE_BLOCKED()
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM0_TARGET)
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM1_TARGET)
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM2_TARGET)
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM3_TARGET)
						PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, Setting objective blocker - Clearing g_FMMC_STRUCT.iTargetTeamBlipBitset / IS_CURRENT_OBJECTIVE_COLLECT_OBJECT")
					ENDIF
					
					BLOCK_OBJECTIVE_THIS_FRAME()
				ENDIF
			
				REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - IS_CURRENT_OBJECTIVE_COLLECT_OBJECT" #ENDIF )
			
			ELIF SHOULD_SHOW_GAIN_WANTED_LEVEL_OBJECTIVE_TEXT()
				
				INT iGainWantedLevelRequirement = GET_GAIN_WANTED_LEVEL()
				PRINTLN("[MJL] Wanted level = ", iGainWantedLevelRequirement)
				TEXT_LABEL_15 tl15 = "MC_GAIN_WANT_"
				tl15 += iGainWantedLevelRequirement
				PRINT_NORMAL_OBJECTIVE(tl15)		
			ELIF IS_CURRENT_OBJECTIVE_LOSE_GANG_CHASE()
				IF iRule < FMMC_MAX_RULES
				AND IS_CUSTOM_STRING_LIST_STRING_VALID(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangOverrideObjectiveTextCustomString[iRule])
					PRINT_CUSTOM_OBJECTIVE(GET_CUSTOM_STRING_LIST_TEXT_LABEL(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangOverrideObjectiveTextCustomString[iRule]))
				ELSE
					STRING sLoseBackupObjText = GET_LOSE_GANG_CHASE_OBJECTIVE_TEXT()
					PRINT_NORMAL_OBJECTIVE(sLoseBackupObjText)
				ENDIF
				
				IF iSpectatorTarget = -1
					IF NOT IS_OBJECTIVE_BLOCKED()
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM0_TARGET)
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM1_TARGET)
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM2_TARGET)
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM3_TARGET)
						PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, Setting objective blocker - Clearing g_FMMC_STRUCT.iTargetTeamBlipBitset / IS_CURRENT_OBJECTIVE_LOSE_GANG_CHASE")
					ENDIF
					
					BLOCK_OBJECTIVE_THIS_FRAME()
				ENDIF
				
				CLEANUP_OBJECTIVE_BLIPS()
				
				REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - IS_CURRENT_OBJECTIVE_LOSE_GANG_CHASE" #ENDIF )
				SET_BIT(iLocalBoolCheck3, LBOOL3_DISABLE_GPSANDHINTCAM)
							
			ELIF (IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES) AND ARE_WE_CURRENTLY_IN_SUDDEN_DEATH())

				PRINT_NORMAL_OBJECTIVE("MC_K_ALLTNC") //no colour variant - B* 2605701
				
				IF ANIMPOSTFX_IS_RUNNING("CrossLine")
					PRINTLN("[JS] - CrossLine - Turning off red filter FX")
					ANIMPOSTFX_STOP("CrossLine")
					ANIMPOSTFX_PLAY("CrossLineOut", 0, FALSE)
				ENDIF
				
				REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES" #ENDIF )
				SET_BIT(iLocalBoolCheck3, LBOOL3_DISABLE_GPSANDHINTCAM)			
				
			ELIF (IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN) AND ARE_WE_CURRENTLY_IN_SUDDEN_DEATH())
			
				HUD_COLOURS colourToUse = HUD_COLOUR_YELLOW
				
				IF IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
					IF MC_playerBD[iObjTxtPartToUse].eSumoSuddenDeathStage = eSSDS_INSIDE_SPHERE
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSUDDEN_DEATH_THAT_ENDS_WHEN_ONLY_ONE_TEAM_ALIVE)
							IF colourToUse != HUD_COLOUR_YELLOW
								PRINT_COLOURED_OBJECTIVE("SUMOR_FTO", colourToUse)
							ELSE
								PRINT_NORMAL_OBJECTIVE("SUMO_FTO")
							ENDIF
						ELSE
							PRINT_NORMAL_OBJECTIVE("SUMO_SIA") 
						ENDIF
					ENDIF
				ELSE
					IF MC_playerBD[iObjTxtPartToUse].eSumoSuddenDeathStage = eSSDS_TIMER_GET_INSIDE_SPHERE
					AND GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
						IF colourToUse != HUD_COLOUR_YELLOW
							PRINT_COLOURED_OBJECTIVE("SUMOR_GIA", colourToUse)
						ELSE
							PRINT_NORMAL_OBJECTIVE("SUMO_GIA")
						ENDIF
					ENDIF
				ENDIF

				REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN" #ENDIF )
				SET_BIT(iLocalBoolCheck3, LBOOL3_DISABLE_GPSANDHINTCAM) 
					
			ELIF (IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN) AND ARE_WE_CURRENTLY_IN_SUDDEN_DEATH())
				
				PRINT_NORMAL_OBJECTIVE("MC_K_ALLTNC")
				
				REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN" #ENDIF )
				SET_BIT(iLocalBoolCheck3, LBOOL3_DISABLE_GPSANDHINTCAM) 
				
			ELIF (IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES) AND ARE_WE_CURRENTLY_IN_SUDDEN_DEATH())

				PRINT_NORMAL_OBJECTIVE("MC_K_ALLTNC")
				
				REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES" #ENDIF )
				SET_BIT(iLocalBoolCheck3, LBOOL3_DISABLE_GPSANDHINTCAM)
				
			ELIF (IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_TARGET) AND ARE_WE_CURRENTLY_IN_SUDDEN_DEATH())

				PRINT_NORMAL_OBJECTIVE("MC_K_SUDRGT")
				
				REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_TARGET" #ENDIF )
				SET_BIT(iLocalBoolCheck3, LBOOL3_DISABLE_GPSANDHINTCAM)
			
			ELIF HAS_NET_TIMER_STARTED(MC_serverBD.tdCopSpottedFailTimer)
				
				IF iSpectatorTarget = -1
					IF NOT IS_OBJECTIVE_BLOCKED()
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM0_TARGET)
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM1_TARGET)
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM2_TARGET)
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM3_TARGET)
						PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, Setting objective blocker - Clearing g_FMMC_STRUCT.iTargetTeamBlipBitset / tdCopSpottedFailTimer has started")
					ENDIF
					
					BLOCK_OBJECTIVE_THIS_FRAME()
				ENDIF
				
				CLEANUP_OBJECTIVE_BLIPS()
				
				REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - tdCopSpottedFailTimer" #ENDIF )
				SET_BIT(iLocalBoolCheck3, LBOOL3_DISABLE_GPSANDHINTCAM)
				
				IF Has_Any_MP_Objective_Text_Been_Received()
					Clear_Any_Objective_Text()
				ENDIF
				
				PRINTLN("[RCC MISSION] [MB] Clear_Any_Objective_Text() 5 - tdCopSpottedFailTimer is running")
				
			ELIF NOT IS_OBJECTIVE_BLOCKED_FOR_PARTICIPANT(iObjTxtPartToUse)
				
				CLEAR_BIT(iLocalBoolCheck3,LBOOL3_DISABLE_GPSANDHINTCAM)
				
				INT iclientstage = GET_MC_CLIENT_MISSION_STAGE(iObjTxtPartToUse)
				
				IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iObjTxtPartToUse].iteam)
					bDelayedClientCheck = TRUE
					REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - New Priority" #ENDIF )
				ELSE
					//do clientlogic check a frame late, host will always have this correct but remotes do this earlier
					IF bDelayedClientCheck OR bHasTripSkipJustFinished
						bDelayedClientCheck = FALSE
						bHasTripSkipJustFinished = FALSE
						REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - bDelayedClientCheck OR bHasTripSkipJustFinished" #ENDIF )
					ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF IS_BIT_SET(iLocalDebugBS, LDEBUGBS_TEAM_CHANGED_WAIT_FOR_OBJTXT)
					IF Is_MP_Objective_Text_On_Display()
						CLEAR_BIT(iLocalDebugBS, LDEBUGBS_TEAM_CHANGED_WAIT_FOR_OBJTXT)
					ELSE
						REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - LDEBUGBS_TEAM_CHANGED_WAIT_FOR_OBJTXT" #ENDIF )
					ENDIF
				ENDIF
				#ENDIF
				
				IF MC_playerBD[iObjTxtPartToUse].iObjectiveTypeCompleted = -1
					
					IF iNumFreeVehicles != MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iTeam]
						iNumFreeVehicles = MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iTeam]
						REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_VEHICLE, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - iNumFreeVehicles updated" #ENDIF )
					ENDIF
					
					IF getNumberOfFreeSeatsInHighPriorityVehiclesForTeam != NULL
						IF iNumFreeSeats != CALL getNumberOfFreeSeatsInHighPriorityVehiclesForTeam(MC_playerBD[iObjTxtPartToUse].iteam)
							iNumFreeSeats = CALL getNumberOfFreeSeatsInHighPriorityVehiclesForTeam(MC_playerBD[iObjTxtPartToUse].iteam)
							REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_VEHICLE, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - iNumFreeSeats updated" #ENDIF )
						ENDIF
					ENDIF
					
					BOOL bBlipChange = FALSE
					
					IF iOldClientStageObjText != iclientstage						
						IF iOldClientStageObjText = CLIENT_MISSION_STAGE_DELIVER_VEH
						AND iclientstage = CLIENT_MISSION_STAGE_COLLECT_VEH
							//If the car we're leaving isn't all team:
							BOOL bCheckOnRule // To let us use GET_VEHICLE_GOTO_TEAMS here
							IF iVehJustIn != -1
							AND GET_VEHICLE_GOTO_TEAMS(MC_playerBD[iObjTxtPartToUse].iTeam, iVehJustIn, bCheckOnRule) = 0
								REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_VEHICLE, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - iVehJustIn updated" #ENDIF )
								bBlipChange = TRUE
							ENDIF
							iVehJustIn = -1
							iOldClientStageObjText = iclientstage
						ELSE
							IF iOldClientStageObjText = CLIENT_MISSION_STAGE_COLLECT_VEH
							AND iclientstage = CLIENT_MISSION_STAGE_DELIVER_VEH
								iVehJustIn = MC_playerBD[iObjTxtPartToUse].iVehNear
								bBlipChange = TRUE
							ELSE
								iVehJustIn = -1
							ENDIF
							iOldClientStageObjText = iclientstage
						ENDIF
					ENDIF
					
					IF iclientstage = CLIENT_MISSION_STAGE_DELIVER_VEH
					OR iclientstage = CLIENT_MISSION_STAGE_COLLECT_VEH
					OR iclientstage = CLIENT_MISSION_STAGE_GOTO_VEH
						IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER) != IS_BIT_SET(iLocalBoolCheck27, LBOOL27_IN_CREATOR_TRAILER_OLD_VALUE)
							IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER)
								SET_BIT(iLocalBoolCheck27, LBOOL27_IN_CREATOR_TRAILER_OLD_VALUE)
								REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_VEHICLE, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - Entered Veh Interior" #ENDIF )
							ELSE
								CLEAR_BIT(iLocalBoolCheck27, LBOOL27_IN_CREATOR_TRAILER_OLD_VALUE)
								REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_VEHICLE, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - Left Veh Interior" #ENDIF )
							ENDIF
						ENDIF
					ENDIF
					
					IF iclientstage = CLIENT_MISSION_STAGE_CAPTURE_AREA
						IF MC_playerBD[iObjTxtPartToUse].iCurrentLoc != iPrevCurrentLoc
							iPrevCurrentLoc = MC_playerBD[iPartToUse].iCurrentLoc
							IF NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_REQUEST_UPDATE)
								CLEAR_BIT(iLocalBoolCheck2, LBOOL2_WHOLE_UPDATE_DONE)
								SET_BIT(iLocalBoolCheck2, LBOOL2_REQUEST_UPDATE)
								PRINTLN("[JS] requesting update as iCurrentLoc changed")
							ENDIF
						ENDIF
					ENDIF
					
					IF iclientstage = CLIENT_MISSION_STAGE_CAPTURE_VEH
						IF iOldVehDestroyBS != MC_PlayerBD[iObjTxtPartToUse].iVehDestroyBitset
							iOldVehDestroyBS = MC_PlayerBD[iObjTxtPartToUse].iVehDestroyBitset
							REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_VEHICLE, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - iOldVehDestroyBS updated" #ENDIF )
						ENDIF
						IF iOldHackTargetsRemaining != MC_serverBD_4.iHackingTargetsRemaining 
							iOldHackTargetsRemaining = MC_serverBD_4.iHackingTargetsRemaining 
							REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_VEHICLE, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - iOldHackTargetsRemaining updated" #ENDIF )
						ENDIF
						IF iCachedVehCapturing != MC_playerBD[iObjTxtPartToUse].iVehCapturing
							iCachedVehCapturing = MC_playerBD[iObjTxtPartToUse].iVehCapturing
							REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_VEHICLE, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - iCachedVehCapturing updated" #ENDIF )
						ENDIF
						IF iOldHackingVehInRange != iHackingVehInRangeBitSet
							iOldHackingVehInRange = iHackingVehInRangeBitSet
							REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_VEHICLE, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - iOldHackingVehInRange updated" #ENDIF )
						ENDIF
					ENDIF
					
					IF iOldVeh != MC_playerBD[iObjTxtPartToUse].iVehNear
						#IF IS_DEBUG_BUILD
						IF bDebugPrintCustomText
							PRINTLN("[JS] iOldVeh = ", iOldVeh)
							PRINTLN("[JS] MC_playerBD[iObjTxtPartToUse].iVehNear = ", MC_playerBD[iObjTxtPartToUse].iVehNear)
						ENDIF
						#ENDIF
						iOldVeh = MC_playerBD[iObjTxtPartToUse].iVehNear
						REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_VEHICLE, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - iVehNear updated" #ENDIF )
					ENDIF
					
					IF inumfreePeds != MC_serverBD.iNumPedHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam]
						inumfreePeds = MC_serverBD.iNumPedHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam]
						REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_PED, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - inumfreePeds updated" #ENDIF )
					ENDIF
					
					IF inumfreeObjects != MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam]
						inumfreeObjects = MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam]
						REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_OBJECT, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - inumfreeObjects updated" #ENDIF )
					ENDIF
					
					IF iPrevObjCapturing != MC_playerBD[iObjTxtPartToUse].iObjCapturing
						iPrevObjCapturing = MC_playerBD[iObjTxtPartToUse].iObjCapturing
						REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_OBJECT, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - iObjCapturing updated" #ENDIF )
					ENDIF
					
					IF IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet, PBBOOL_WITH_CARRIER)
						IF NOT IS_BIT_SET(iLocalBoolCheck, LBOOL_WITH_CARRIER_FLAG)
							SET_BIT(iLocalBoolCheck, LBOOL_WITH_CARRIER_FLAG)
							REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - LBOOL_WITH_CARRIER_FLAG changed" #ENDIF )
						ENDIF
					ELSE
						IF IS_BIT_SET(iLocalBoolCheck, LBOOL_WITH_CARRIER_FLAG)
							CLEAR_BIT(iLocalBoolCheck, LBOOL_WITH_CARRIER_FLAG)
							REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - LBOOL_WITH_CARRIER_FLAG changed (not)" #ENDIF )
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
					OR IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet2, PBBOOL2_DROPPING_OFF)
					OR iDeliveryVehForcingOut != -1
						IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_WAS_IN_DROP_OFF)
							SET_BIT(iLocalBoolCheck5, LBOOL5_WAS_IN_DROP_OFF)
							REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - LBOOL5_WAS_IN_DROP_OFF changed" #ENDIF )
						ENDIF
					ELSE
						IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_WAS_IN_DROP_OFF)
							CLEAR_BIT(iLocalBoolCheck5, LBOOL5_WAS_IN_DROP_OFF)
							REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - LBOOL5_WAS_IN_DROP_OFF changed (not)" #ENDIF )
						ENDIF
					ENDIF
					
					FOR i = 0 TO ciFMMC_PED_BITSET_SIZE - 1
						IF MC_playerBD[iLocalPart].iPedLeftBS[i] != iPedLeftBSCache[i]
							iPedLeftBSCache[i] = MC_playerBD[iLocalPart].iPedLeftBS[i]
							REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_PED, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - iPedLeftBSCache updated" #ENDIF )
						ENDIF
					ENDFOR
					
					IF MC_playerBD[iLocalPart].iVehLeftBS != iVehLeftBSCache
						iVehLeftBSCache = MC_playerBD[iLocalPart].iVehLeftBS
						REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_VEHICLE, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - iVehLeftBSCache updated" #ENDIF )
					ENDIF
					
					IF MC_playerBD[iLocalPart].iObjLeftBS != iObjLeftBSCache
						iObjLeftBSCache = MC_playerBD[iLocalPart].iObjLeftBS 
						REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_OBJECT, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - iObjLeftBSCache updated" #ENDIF )
					ENDIF

					IF (iclientstage = CLIENT_MISSION_STAGE_DAMAGE_PED OR iclientstage = CLIENT_MISSION_STAGE_PED_GOTO_LOCATION)
					AND iPedCountCache != MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
						iPedCountCache = MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
						REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_PED, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - iPedCountCache updated" #ENDIF )
					ENDIF
					
					IF iclientstage = CLIENT_MISSION_STAGE_DAMAGE_VEH
					AND iVehCountCache != MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
						iVehCountCache = MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
						REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_VEHICLE, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - iVehCountCache updated" #ENDIF )
					ENDIF
					
					IF iclientstage = CLIENT_MISSION_STAGE_DAMAGE_OBJ
					AND iObjCountCache != MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
						iObjCountCache = MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
						REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_OBJECT, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - iObjCountCache updated" #ENDIF )
					ENDIF
					
					IF NOT Is_MP_Objective_Text_On_Display()
						REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, TRUE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - No Objective Text Shown" #ENDIF )
					ENDIF
					
					CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM0_TARGET)
					CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM1_TARGET)
					CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM2_TARGET)
					CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM3_TARGET)
						
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciUSE_MP_BLIPPING_FOR_KILL_PLAYER_RULE)
						IF iclientstage = CLIENT_MISSION_STAGE_KILL_PLAYERS
							IF MC_playerBD[iObjTxtPartToUse].iteam != 0
							AND NOT DOES_TEAM_LIKE_TEAM(MC_playerBD[iObjTxtPartToUse].iteam,0)
								SET_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM0_TARGET)
							ENDIF
							IF MC_playerBD[iObjTxtPartToUse].iteam != 1
							AND NOT DOES_TEAM_LIKE_TEAM(MC_playerBD[iObjTxtPartToUse].iteam,1)
								SET_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM1_TARGET)
							ENDIF
							IF MC_playerBD[iObjTxtPartToUse].iteam != 2
							AND NOT DOES_TEAM_LIKE_TEAM(MC_playerBD[iObjTxtPartToUse].iteam,2)
								SET_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM2_TARGET)
							ENDIF
							IF MC_playerBD[iObjTxtPartToUse].iteam != 3
							AND NOT DOES_TEAM_LIKE_TEAM(MC_playerBD[iObjTxtPartToUse].iteam,3)
								SET_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM3_TARGET)
							ENDIF
						ELIF iclientstage = CLIENT_MISSION_STAGE_KILL_TEAM0
							SET_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM0_TARGET)
						ELIF iclientstage = CLIENT_MISSION_STAGE_KILL_TEAM1
							SET_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM1_TARGET)
						ELIF iclientstage = CLIENT_MISSION_STAGE_KILL_TEAM2
							SET_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM2_TARGET)
						ELIF iclientstage = CLIENT_MISSION_STAGE_KILL_TEAM3
							SET_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM3_TARGET)
						ENDIF
					ENDIF
					
					IF iRule < FMMC_MAX_RULES
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetSix[iRule], ciBS_RULE6_BLIP_ON_SPEED_ENABLED)
							PROCESS_PLAYER_CURRENT_SPEED()
							PROCESS_BLIP_ON_VEHICLE_SPEED()
						ENDIF
					ENDIF					
					
					// this is where the server will update its data based upon the player broadcast data MC_playerBD[iPartToUse].iObjectiveTypeCompleted
					IF iRule < FMMC_MAX_RULES
						FOR iteam = 0  TO (FMMC_MAX_TEAMS-1)
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRuleBitsetFour[iRule] ,ciBS_RULE4_ALWAYS_BLIP_DELIVERY_POINT)
								ADD_DELIVERY_BLIP(bBlipChange)
							ENDIF
						ENDFOR
					ENDIF
					
					INT iMyTeam = MC_playerBD[iObjTxtPartToUse].iteam
					
					IF iclientstage != CLIENT_MISSION_STAGE_DELIVER_OBJ
					AND iclientstage != CLIENT_MISSION_STAGE_DELIVER_VEH
					AND iclientstage != CLIENT_MISSION_STAGE_DELIVER_PED
					AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
											
						IF iclientstage = CLIENT_MISSION_STAGE_COLLECT_PED
							IF MC_serverBD.iNumPedHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] 
								REMOVE_DELIVERY_BLIP()
							ELSE
								IF IS_BIT_SET(iObjectiveTextRequestDelayBitset, ciOBJECTIVE_TEXT_DELAY_PED) //[FMMC2020] Not needed or use something else? Odd piggy backing on this
								OR NOT DOES_BLIP_EXIST(DeliveryBlip)
									ADD_DELIVERY_BLIP()
								ENDIF
							ENDIF
						ELIF iclientstage = CLIENT_MISSION_STAGE_COLLECT_VEH
							BOOL bAvengerDeliveryBlip
							INT iAvengerDriverBlip
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen,ciENABLE_AVENGER_INTERIOR_ENTRY)
							AND IS_BIT_SET(iLocalBoolCheck30, LBOOL30_PRIORITY_VEHICLE_IS_AVENGER)
								bAvengerDeliveryBlip = IS_ANY_PLAYER_IN_AVENGER_PILOT_SEAT(iAvengerDriverBlip)
							ENDIF
							
							IF (MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumvehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
							OR (amIWaitingForPlayersToEnterVeh != NULL AND CALL amIWaitingForPlayersToEnterVeh())							
							AND NOT ((MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumvehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]) 
							AND IS_BIT_SET(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER) AND bAvengerDeliveryBlip))
								REMOVE_DELIVERY_BLIP()
							ELSE
								IF IS_BIT_SET(iObjectiveTextRequestDelayBitset, ciOBJECTIVE_TEXT_DELAY_VEHICLE) //[FMMC2020] Not needed or use something else? Odd piggy backing on this
								OR NOT DOES_BLIP_EXIST(DeliveryBlip)
									ADD_DELIVERY_BLIP(bBlipChange)
								ENDIF
							ENDIF
							
						ELIF iclientstage = CLIENT_MISSION_STAGE_COLLECT_OBJ
							
						
							IF MC_serverBD.iNumObjHighestPriorityHeld[ iMyTeam ] < MC_serverBD.iNumobjHighestPriority[ iMyTeam ] // There are still objects left to pick up that aren't picked up
							AND MC_playerBD[iPartToUse].iObjCarryCount = 0 AND NOT IS_BIT_SET( MC_playerBD[iObjTxtPartToUse].iClientBitSet, PBBOOL_WITH_CARRIER )
								REMOVE_DELIVERY_BLIP()
							ELSE
								IF IS_BIT_SET(iObjectiveTextRequestDelayBitset, ciOBJECTIVE_TEXT_DELAY_OBJECT) //[FMMC2020] Not needed or use something else? Odd piggy backing on this
								OR NOT DOES_BLIP_EXIST(DeliveryBlip)
									ADD_DELIVERY_BLIP()
								ENDIF
							ENDIF
						ELSE
							IF iMyTeam < FMMC_MAX_TEAMS
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iRuleBitsetFour[iRule] ,ciBS_RULE4_ALWAYS_BLIP_DELIVERY_POINT)
									REMOVE_DELIVERY_BLIP()
								ENDIF
							ENDIF
						ENDIF
					
					ELSE
						ADD_DELIVERY_BLIP(bBlipChange)
					ENDIF
	
					IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
						tl63CustomGodText = GET_CUSTOM_OBJECTIVE_TEXT_LABEL(MC_playerBD[iObjTxtPartToUse].iteam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam], g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].tl63Objective[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]])
						tl63CustomGodText1 = GET_CUSTOM_OBJECTIVE_TEXT_LABEL(MC_playerBD[iObjTxtPartToUse].iteam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam], g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].tl63Objective1[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]])
						tl63CustomGodText2 = GET_CUSTOM_OBJECTIVE_TEXT_LABEL(MC_playerBD[iObjTxtPartToUse].iteam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam],g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].tl63Objective2[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]]) 
					ENDIF
					
					#IF IS_DEBUG_BUILD
					IF bDebugPrintCustomText
					PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - iclientstage = ", GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(iclientstage))
					PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - iPartToUse = ", iObjTxtPartToUse)
					PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - MC_playerBD[iPartToUse].iteam = ", MC_playerBD[iObjTxtPartToUse].iteam)
					PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] = ", MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam])
					PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - tl63CustomGodText = '", tl63CustomGodText, "' tl63CustomGodText1 = '", tl63CustomGodText1, "' tl63CustomGodText2 = '", tl63CustomGodText2, "'")	
					ENDIF
					#ENDIF
									
					BOOL bSkipObjectiveTextUpdate = FALSE
					
					IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES						
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE9_USE_ALTERNATIVE_TEXT_IN_MOC)
						AND NOT IS_STRING_NULL_OR_EMPTY(tl63CustomGodText2)	
							IF IS_PLAYER_IN_CREATOR_TRAILER(ObjTxtPlayerToUse)
							OR ((IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen,ciENABLE_AVENGER_INTERIOR_ENTRY) AND IS_BIT_SET(iLocalBoolCheck30, LBOOL30_PRIORITY_VEHICLE_IS_AVENGER))
							AND IS_PLAYER_IN_CREATOR_AIRCRAFT(ObjTxtPlayerToUse)
							AND (isTeammateReadyToDeliverSpecificVehicle != NULL AND NOT CALL isTeammateReadyToDeliverSpecificVehicle(MC_playerBD[iObjTxtPartToUse].iteam))
							AND IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE))
								IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
									#IF IS_DEBUG_BUILD
									IF bDebugPrintCustomText
										PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - Overridden as in MOC")	
									ENDIF
									#ENDIF
									bobjectivedisp = TRUE
									bcustomobjective = TRUE
									bSkipObjectiveTextUpdate = TRUE
								ENDIF
							ENDIF
						ENDIF
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetEleven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE11_SECONDARY_TEXT_IN_ORBITAL_CANNON)
						AND NOT IS_STRING_NULL_OR_EMPTY(tl63CustomGodText1)	
							IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(ObjTxtPlayerToUse)].iMissionModeBit, ciMISSION_USING_ORBITAL_CANNON)
								IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
									#IF IS_DEBUG_BUILD
									IF bDebugPrintCustomText
										PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - Overridden as in Orbital Canon")	
									ENDIF
									#ENDIF
									bobjectivedisp = TRUE
									bcustomobjective = TRUE
									bSkipObjectiveTextUpdate = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF PROCESS_EXTERNAL_OBJECTIVE_TEXT_OVERRIDES(bObjectiveDisp, bCustomObjective)
						bSkipObjectiveTextUpdate = TRUE
					ENDIF
					
					IF MC_serverBD.iCurrentPlayerRule[MC_playerBD[iObjTxtPartToUse].iteam] > -1
					AND MC_serverBD.iCurrentPlayerRule[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
					AND MC_serverBD_4.iPlayerRulePriority[ MC_serverBD.iCurrentPlayerRule[MC_playerBD[iObjTxtPartToUse].iteam]][MC_playerBD[iObjTxtPartToUse].iteam] = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
					AND MC_serverBD_4.iPlayerRule[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iObjTxtPartToUse].iteam]][MC_playerBD[iObjTxtPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE
					AND MC_serverBD.iPlayerRuleLimit[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iObjTxtPartToUse].iteam]][MC_playerBD[iObjTxtPartToUse].iteam] >= 0
					AND iclientstage != CLIENT_MISSION_STAGE_SCRIPTED_CUTSCENE	
						#IF IS_DEBUG_BUILD
						IF bDebugPrintCustomText
							PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - Skipping Objective Text - Cutscene Rule, not on correct client stage though")	
						ENDIF
						#ENDIF
						IF Is_MP_Objective_Text_On_Display()
							Clear_Any_Objective_Text()
						ENDIF
						bobjectivedisp = TRUE
						bcustomobjective = TRUE
						bSkipObjectiveTextUpdate = TRUE
					ENDIF
										
					IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_BLOCK_OBJECTIVE_TEXT_UNTIL_RULE_CHANGE)
						#IF IS_DEBUG_BUILD
						IF bDebugPrintCustomText
							PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - Skipping Objective Text - LBOOL24_BLOCK_OBJECTIVE_TEXT_UNTIL_RULE_CHANGE")	
						ENDIF
						#ENDIF
						IF Is_MP_Objective_Text_On_Display()
							Clear_Any_Objective_Text()
						ENDIF
						bobjectivedisp = TRUE
						bcustomobjective = TRUE
					ENDIF
					
					IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
					AND (NOT IS_STRING_NULL_OR_EMPTY( tl63CustomGodText )
					OR NOT IS_STRING_NULL_OR_EMPTY( tl63CustomGodText1 ) 
					OR NOT IS_STRING_NULL_OR_EMPTY( tl63CustomGodText2 ) )
					AND NOT g_FMMC_STRUCT.bPlayerLangNotEqualCreated
					AND NOT bSkipObjectiveTextUpdate
					AND NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_BLOCK_OBJECTIVE_TEXT_UNTIL_RULE_CHANGE)
					AND NOT bobjectivedisp
					AND NOT bcustomobjective
						SWITCH iclientstage
							
							CASE CLIENT_MISSION_STAGE_CAPTURE_AREA
							CASE CLIENT_MISSION_STAGE_PHOTO_LOC
								IF MC_playerBD[iPartToUse].iCurrentLoc != -1
									IF PRINT_CUSTOM_OBJECTIVE(PROCESS_OBJECTIVE_TEXT_COLOUR_CHANGE(tl63CustomGodText1))
										bobjectivedisp=TRUE
										bcustomobjective = TRUE
									ELSE
										bNoSecondObjective = TRUE
									ENDIF
								ELSE
									IF MC_serverBD.iNumLocHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] <= 1
									AND NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)
									AND iclientstage = CLIENT_MISSION_STAGE_CAPTURE_AREA
										bobjectivedisp = TRUE
										bcustomobjective = TRUE
										PRINTLN("[JS] CONTROL_AREA - waiting for update")
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GOTO_LOC
								
								IF MC_PlayerBD[iPartToUse].iteam < FMMC_MAX_TEAMS
									IF MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iteam] < FMMC_MAX_RULES
									
										IF iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]
											IF NOT IS_PLAYER_READY_TO_DELIVER_ANY_SPECIFIC_VEHICLE(MC_playerBD[iObjTxtPartToUse].iteam, GET_PLAYER_PED(ObjTxtPlayerToUse)) 
												IF (isTeammateReadyToDeliverSpecificVehicle != NULL AND CALL isTeammateReadyToDeliverSpecificVehicle(MC_playerBD[iObjTxtPartToUse].iteam))
												AND IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)															
													IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
														bobjectivedisp=TRUE
														bcustomobjective = TRUE
													ELSE
														bNoSecondObjective = TRUE
													ENDIF
												ELSE
													IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
														bobjectivedisp=TRUE
														bcustomobjective = TRUE
													ELSE
														bNoSecondObjective = TRUE
													ENDIF
												ENDIF
											ELIF MC_playerBD[iObjTxtPartToUse].iCurrentLoc != -1
												INT iLoc
												iLoc = MC_playerBD[iObjTxtPartToUse].iCurrentLoc
												
												IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iMyTeam] != ciGOTO_LOCATION_INDIVIDUAL
													IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_LOCATION)
														IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iMyTeam] != ciGOTO_LOCATION_WHOLE_TEAM
														OR MC_serverBD.iNumberOfPlayingPlayers[iMyTeam] > 1																	
															IF SHOULD_SHOW_CUSTOM_LOCATION_OBJECTIVE_TEXT(iLoc, iObjTxtPartToUse)
																IF DOES_RULE_REQUIRE_A_SPECIFIC_VEHICLE(iLoc)
																	IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
																		bobjectivedisp = TRUE
																		bcustomobjective = TRUE
																	ELSE
																		bNoSecondObjective = TRUE
																	ENDIF
																ELSE
																	IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
																		bobjectivedisp = TRUE
																		bcustomobjective = TRUE
																	ELSE
																		bNoSecondObjective = TRUE
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
													ELSE
														bcustomobjective = TRUE
														bobjectivedisp = TRUE
													ENDIF
												ENDIF
											ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iPartToUse].iteam].iRuleBitsetFourteen[MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iteam]], ciBS_RULE14_OVERRIDE_TEXT_ON_OUTFIT_SWAP)
											AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_CHANGED_INTO_DISGUISE)
											AND PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)	
												bobjectivedisp = TRUE
												bcustomobjective = TRUE
												PRINTLN("[RCC MISSION][PROCESS_OBJECTIVE_TEXT] - Primary text overidden as we have changed outfits")
											ENDIF
										ELSE
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_LEAVE_LOC
							
								IF iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam] //bit of a hack to sync this
								AND IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_NEW_PARTICIPANT_LOOP_FINISHED) // Leave areas aren't processed until this is true
									IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_LOCATION)
										IF (NOT IS_BIT_SET(iLocalBoolCheck6,LBOOL6_IN_MY_PRIORITY_LEAVE_LOC)) //I'm out of the location!
										AND NOT (IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_EARLY_END_SPECTATOR)
										AND MC_playerBD[iPartToUse].iLeaveLoc != -1)
											IF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)
											AND IS_BIT_SET(MC_serverBD.iServerBitSet2,SBBOOL2_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + MC_playerBD[iObjTxtPartToUse].iteam)
											AND MC_serverBD.iNumberOfPlayingPlayers[MC_playerBD[iObjTxtPartToUse].iteam] > 1
												
												IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
													bobjectivedisp = TRUE
													bcustomobjective = TRUE
												ELSE
													bNoSecondObjective = TRUE
												ENDIF
												
											ELSE

												IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)
													PRINTLN("[MMacK][LeaveLoc] LBOOL2_WHOLE_UPDATE_DONE")
												ENDIF
												
												IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet2,SBBOOL2_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + MC_playerBD[iObjTxtPartToUse].iteam)
													PRINTLN("[MMacK][LeaveLoc] SBBOOL2_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC")
												ENDIF

												IF NOT IS_PLAYER_IN_VEHICLE_WITH_ALL_OF_TEAM(MC_ServerBD.iNumberOfPlayingPlayers[ MC_playerBD[iObjTxtPartToUse].iteam ])
													IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
														bobjectivedisp = TRUE
														bcustomobjective = TRUE
													ELSE
														bNoSecondObjective = TRUE
													ENDIF
												ELSE
													bobjectivedisp = TRUE
													bcustomobjective = TRUE
												ENDIF
											ENDIF
										ENDIF
									ELSE
										bobjectivedisp = TRUE
										bcustomobjective = TRUE
									ENDIF
								ELSE
									IF Is_MP_Objective_Text_On_Display()
										Clear_Any_Objective_Text()
									ENDIF
									
									PRINTLN("[MMacK][LeaveLoc] CLEAR!")
									
									bobjectivedisp = TRUE
									bcustomobjective = TRUE
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_PHOTO_PED
							
								IF MC_playerBD[iObjTxtPartToUse].iPedNear != -1
									IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[MC_playerBD[iObjTxtPartToUse].iteam], MC_playerBD[iObjTxtPartToUse].iPedNear)
									OR	(IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[MC_playerBD[iObjTxtPartToUse].iPedNear]) AND FMMC_IS_LONG_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[MC_playerBD[iObjTxtPartToUse].iteam], MC_playerBD[iObjTxtPartToUse].iPedNear))
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
											bobjectivedisp=TRUE
											bcustomobjective = TRUE
										ELSE
											bNoSecondObjective = TRUE
										ENDIF
									ENDIF
								ELSE
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iPhotoCanBeDead,MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam])
										IF MC_serverBD.iNumDeadPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] >= 1
											IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
												bobjectivedisp=TRUE
												bcustomobjective = TRUE
											ELSE
												bDispHelpObjective=TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_PHOTO_VEH
								IF MC_playerBD[iPartToUse].iVehNear != -1
									IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
										bobjectivedisp = TRUE
										bcustomobjective = TRUE
									ELSE
										bNoSecondObjective = TRUE
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_PHOTO_OBJ
								
								IF MC_playerBD[iObjTxtPartToUse].iObjNear != -1
									IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
										bobjectivedisp=TRUE
										bcustomobjective = TRUE
									ELSE
										bNoSecondObjective = TRUE
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_CAPTURE_PED
							CASE CLIENT_MISSION_STAGE_COLLECT_PED
							CASE CLIENT_MISSION_STAGE_DELIVER_PED
								IF iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam] //bit of a hack to sync this
									iTeam = MC_playerBD[iObjTxtPartToUse].iteam
									IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_PED)
									
										#IF IS_DEBUG_BUILD
										IF bDebugPrintCustomText
											PRINTLN("[JS] [GDTEXT] - PED - IS_OBJECTIVE_TEXT_READY_TO_UPDATE TRUE")
										ENDIF
										#ENDIF
										
										IF IS_BIT_SET(iLocalBoolCheck30, LBOOL30_SHOW_CUSTOM_WAIT_TEXT)
											TEXT_LABEL_63 tlWaitText
											tlWaitText = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl63WaitMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]]
											IF PRINT_CUSTOM_OBJECTIVE(tlWaitText)
												bobjectivedisp = TRUE
												bcustomobjective = TRUE
											ENDIF
										ELIF MC_playerBD[iObjTxtPartToUse].iPedCarryCount > 0
										OR IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
										OR IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_IN_GROUP_PED_VEH)
											#IF IS_DEBUG_BUILD
											IF bDebugPrintCustomText
												IF MC_playerBD[iObjTxtPartToUse].iPedCarryCount > 0
													PRINTLN("[JS] [GDTEXT] - PED - iPedCarryCount > 0")
												ENDIF
												IF IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
													PRINTLN("[JS] [GDTEXT] - PED - PBBOOL_WITH_CARRIER")
												ENDIF
												IF IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_IN_GROUP_PED_VEH)
													PRINTLN("[JS] [GDTEXT] - PED - PBBOOL_IN_GROUP_PED_VEH")
												ENDIF
											ENDIF
											#ENDIF
											IF (MC_serverBD.iRequiredDeliveries[iTeam] <= 1)
											OR (MC_serverBD.iNumPedHighestPriorityHeld[iTeam] >= MC_serverBD.iRequiredDeliveries[iTeam])
												#IF IS_DEBUG_BUILD
												IF bDebugPrintCustomText
													IF MC_serverBD.iRequiredDeliveries[iTeam] <= 1
														PRINTLN("[JS] [GDTEXT] - PED - MC_serverBD.iRequiredDeliveries[iTeam] <= 1")
													ENDIF
													IF MC_serverBD.iNumPedHighestPriorityHeld[iTeam] >= MC_serverBD.iRequiredDeliveries[iTeam]
														PRINTLN("[JS] [GDTEXT] - PED - MC_serverBD.iNumPedHighestPriorityHeld[iTeam] >= MC_serverBD.iRequiredDeliveries[iTeam]")
													ENDIF
												ENDIF
												#ENDIF
												IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
													bobjectivedisp=TRUE
													bcustomobjective = TRUE
												ELSE
													bNoSecondObjective = TRUE
												ENDIF
											ENDIF
										ELIF MC_serverBD.iNumPedHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
											#IF IS_DEBUG_BUILD
											IF bDebugPrintCustomText
												PRINTLN("[JS] [GDTEXT] - PED - MC_serverBD.iNumPedHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]")
											ENDIF
											#ENDIF
											IF MC_serverBD.iTotalPlayingCoopPlayers[MC_playerBD[iObjTxtPartToUse].iteam] > 1
												#IF IS_DEBUG_BUILD
												IF bDebugPrintCustomText
													PRINTLN("[JS] [GDTEXT] - PED - MC_serverBD.iTotalPlayingCoopPlayers[MC_playerBD[iObjTxtPartToUse].iteam] > 1")
												ENDIF
												#ENDIF
												IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
													bobjectivedisp=TRUE
													bcustomobjective = TRUE
												ELSE
													bDispHelpObjective=TRUE
												ENDIF
											ENDIF
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
										IF bDebugPrintCustomText
											PRINTLN("[JS] [GDTEXT] - PED - IS_OBJECTIVE_TEXT_READY_TO_UPDATE FALSE")
										ENDIF
										#ENDIF
										bobjectivedisp=TRUE
										bcustomobjective = TRUE
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GOTO_VEH
							
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_AVENGER_INTERIOR_ENTRY)
								AND IS_BIT_SET(iLocalBoolCheck30, LBOOL30_PRIORITY_VEHICLE_IS_AVENGER)								
									bAvengerHasDriver = IS_ANY_PLAYER_IN_AVENGER_PILOT_SEAT(iAvengerDriver)
								ENDIF
								
								IF bAvengerHasDriver != IS_BIT_SET(iLocalBoolCheck28, LBOOL28_PREV_AVENGER_PILOT_STATUS)
									IF bAvengerHasDriver
										SET_BIT(iLocalBoolCheck28, LBOOL28_PREV_AVENGER_PILOT_STATUS)
									ELSE
										CLEAR_BIT(iLocalBoolCheck28, LBOOL28_PREV_AVENGER_PILOT_STATUS)
									ENDIF
									REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_VEHICLE, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - bAvengerHasDriver" #ENDIF )
								ENDIF
								
								IF iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]
									IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_VEHICLE)
								
										IF MC_playerBD[iObjTxtPartToUse].iVehNear != -1
										OR ((IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_AVENGER_INTERIOR_ENTRY) AND IS_BIT_SET(iLocalBoolCheck30, LBOOL30_PRIORITY_VEHICLE_IS_AVENGER))
										AND bAvengerHasDriver
										AND MC_playerBD[iAvengerDriver].iVehNear != -1
										AND (NOT (MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iTeam] >= MC_serverBD.iNumvehHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]))
										AND IS_BIT_SET(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER))
											BOOL bCheckOnRule // To let us use GET_VEHICLE_GOTO_TEAMS here	
											INT iVehNearPart
											IF MC_playerBD[iObjTxtPartToUse].iVehNear = -1
												iVehNearPart = iAvengerDriver
											ELSE
												iVehNearPart = iObjTxtPartToUse
											ENDIF
											IF (GET_VEHICLE_GOTO_TEAMS(MC_playerBD[iObjTxtPartToUse].iteam,MC_playerBD[iVehNearPart].iVehNear,bCheckOnRule) > 0)
												PRINTLN("[MMacK][FleecaFinaleGoto] GoToRule Custom text")
												IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
													bobjectivedisp = TRUE
													bcustomobjective = TRUE
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[JS] [GDTEXT] - GOTO VEH - Printing tl63CustomGodText1")
													ENDIF
													#ENDIF
												ELSE
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[JS] [GDTEXT] - GOTO VEH - Failed to print tl63CustomGodText1")
													ENDIF
													#ENDIF
													bNoSecondObjective = TRUE
												ENDIF
											#IF IS_DEBUG_BUILD
											ELSE
												IF bDebugPrintCustomText
													PRINTLN("[JS] [GDTEXT] - GOTO VEH - GET_VEHICLE_GOTO_TEAMS = 0")
												ENDIF
											#ENDIF
											ENDIF
										#IF IS_DEBUG_BUILD
										ELSE
											IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].tl63WrongWayMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]])
												IF NOT bAvengerHasDriver
												AND IS_BIT_SET(iLocalBoolCheck30, LBOOL30_PRIORITY_VEHICLE_IS_AVENGER)
												AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_AVENGER_INTERIOR_ENTRY)												
												AND IS_BIT_SET(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER)
													#IF IS_DEBUG_BUILD
														IF bDebugPrintCustomText
															PRINTLN("[JS] [GDTEXT] - Cover Blown/Avenger Wrong way text GOTO")
														ENDIF
													#ENDIF
													IF PRINT_CUSTOM_OBJECTIVE(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].tl63WrongWayMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]])
														bobjectivedisp = TRUE
														bcustomobjective = TRUE
													ENDIF
												ENDIF
											ENDIF
											IF bDebugPrintCustomText
												PRINTLN("[JS] [GDTEXT] - GOTO VEH - MC_playerBD[iObjTxtPartToUse].iVehNear = -1")
											ENDIF
										#ENDIF
										ENDIF
										
									ELIF MC_playerBD[iObjTxtPartToUse].iteam < FMMC_MAX_TEAMS
									AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetNine[iRule], ciBS_RULE9_USE_ALTERNATIVE_GOTO_VEH_TEXT)
										bNoSecondObjective = TRUE
									ELSE
										#IF IS_DEBUG_BUILD
										IF bDebugPrintCustomText
											PRINTLN("[JS] [GDTEXT] - GOTO VEH - Not ready, not ciBS_RULE9_USE_ALTERNATIVE_GOTO_VEH_TEXT")
										ENDIF
										#ENDIF
										bobjectivedisp = TRUE
										bcustomobjective = TRUE
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
									IF bDebugPrintCustomText
										PRINTLN("[JS] [GDTEXT] - GOTO VEH - iLastRule Else")
									ENDIF
									#ENDIF
									bobjectivedisp = TRUE
									bcustomobjective = TRUE
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_CAPTURE_VEH
								
								IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam] < FMMC_MAX_RULES
								AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].iRuleBitsetEleven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]], ciBS_RULE11_SECUROHACK_REQUIRES_KILL)
									IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_VEHICLE)
										IF MC_serverBD_4.iHackingTargetsRemaining > 0
											IF MC_PlayerBD[iObjTxtPartToUse].iVehDestroyBitset != 0
												IF NOT HOLD_UP_VEHHACK_OBJECTIVE_TEXT(iObjTxtPartToUse)
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[JS] [GDTEXT] - HACK VEH - Kill (Alternate) 1")
													ENDIF
													#ENDIF
													IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
														bobjectivedisp = TRUE
														bcustomobjective = TRUE
													ELSE
														bNoSecondObjective = TRUE
													ENDIF
												ELSE
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[GDTEXT] - HACK VEH - Clearing objective text due to dialogue not being done!")
													ENDIF
													#ENDIF
													
													IF Is_MP_Objective_Text_On_Display()
														Clear_Any_Objective_Text()
													ENDIF
														
													bobjectivedisp = TRUE
													bcustomobjective = TRUE
												ENDIF
											ELSE
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING) 
												AND MC_playerBD[iObjTxtPartToUse].iVehCapturing != -1
												AND IS_BIT_SET(iHackingVehInRangeBitSet, MC_playerBD[iObjTxtPartToUse].iVehCapturing)
													#IF IS_DEBUG_BUILD
														IF bDebugPrintCustomText
															PRINTLN("[JS] [GDTEXT] - HACK VEH - Complete the hack (secondary) iHackingVehInRangeBitSet = ", iHackingVehInRangeBitSet, " MC_playerBD[iObjTxtPartToUse].iVehCapturing: ", MC_playerBD[iObjTxtPartToUse].iVehCapturing)
														ENDIF
													#ENDIF
													IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
														bobjectivedisp = TRUE
														bcustomobjective = TRUE
													ELSE
														bNoSecondObjective = TRUE
													ENDIF
												ELSE
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[JS] [GDTEXT] - HACK VEH - Hack a target (Primary)")
													ENDIF
													#ENDIF
													IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
														bobjectivedisp = TRUE
														bcustomobjective = TRUE
													ELSE
														bNoSecondObjective = TRUE
													ENDIF
												ENDIF
											ENDIF
										ELSE
											#IF IS_DEBUG_BUILD
											IF bDebugPrintCustomText
												PRINTLN("[JS] [GDTEXT] - HACK VEH - Kill (Alternate) 2")
											ENDIF
											#ENDIF
											IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
												bobjectivedisp = TRUE
												bcustomobjective = TRUE
											ELSE
												bNoSecondObjective = TRUE
											ENDIF
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
										IF bDebugPrintCustomText
											PRINTLN("[JS] [GDTEXT] - HACK VEH  - Not ready")
										ENDIF
										#ENDIF
										bobjectivedisp = TRUE
										bcustomobjective = TRUE
									ENDIF
								ELSE
									// has vehicle objective colour been overriden
									IF iVehCapTeam != -1  
										IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_VEHICLE)
										OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
										OR bIsAnySpectator
											IF (MC_playerBD[iObjTxtPartToUse].iVehNear != -1)
											OR IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
											OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING) 
											AND MC_playerBD[iObjTxtPartToUse].iVehCapturing != -1
											AND IS_BIT_SET(iHackingVehInRangeBitSet, MC_playerBD[iObjTxtPartToUse].iVehCapturing))
												IF PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE(tl63CustomGodText1,iVehCapTeam)
													bobjectivedisp=TRUE
													bcustomobjective = TRUE
												ELSE
													bNoSecondObjective = TRUE
												ENDIF
											ELIF MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumvehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
											OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING) 
											AND IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SOMEONE_IS_HACKING_A_VEHICLE))
												IF MC_serverBD.iTotalPlayingCoopPlayers[MC_playerBD[iObjTxtPartToUse].iteam] > 1
													IF PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE(tl63CustomGodText2,iVehCapTeam)
														bobjectivedisp=TRUE
														bcustomobjective = TRUE
													ELSE
														bDispHelpObjective=TRUE
													ENDIF
												ENDIF
											ENDIF
										ELSE
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF
									ELSE
										IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_VEHICLE)
										OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
										OR bIsAnySpectator
											IF (MC_playerBD[iObjTxtPartToUse].iVehNear != -1)
											OR IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
											OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING) 
											AND MC_playerBD[iObjTxtPartToUse].iVehCapturing != -1
											AND IS_BIT_SET(iHackingVehInRangeBitSet, MC_playerBD[iObjTxtPartToUse].iVehCapturing))
												IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
													bobjectivedisp=TRUE
													bcustomobjective = TRUE
												ELSE
													bNoSecondObjective = TRUE
												ENDIF
											ELIF MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumvehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
											OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING) 
											AND IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SOMEONE_IS_HACKING_A_VEHICLE))
												IF MC_serverBD.iTotalPlayingCoopPlayers[MC_playerBD[iObjTxtPartToUse].iteam] > 1
													IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
														bobjectivedisp=TRUE
														bcustomobjective = TRUE
													ELSE
														bDispHelpObjective=TRUE
													ENDIF
												ENDIF
											ENDIF
										ELSE
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_COLLECT_VEH
							CASE CLIENT_MISSION_STAGE_DELIVER_VEH
							
								iTeam = MC_playerBD[iObjTxtPartToUse].iteam
								BOOL bSkipUpdateDelay
								bSkipUpdateDelay = FALSE
								
								#IF IS_DEBUG_BUILD
								IF bDebugPrintCustomText
									PRINTLN("[JS] [GDTEXT] - On a Get and Deliver")
								ENDIF
								#ENDIF
								
								IF IS_BIT_SET(iLocalBoolCheck30, LBOOL30_PRIORITY_VEHICLE_IS_AVENGER)
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_AVENGER_INTERIOR_ENTRY)									
										bAvengerHasDriver = IS_ANY_PLAYER_IN_AVENGER_PILOT_SEAT(iAvengerDriver)
									ENDIF
									
									IF bAvengerHasDriver != IS_BIT_SET(iLocalBoolCheck28, LBOOL28_PREV_AVENGER_PILOT_STATUS)
										IF bAvengerHasDriver
											SET_BIT(iLocalBoolCheck28, LBOOL28_PREV_AVENGER_PILOT_STATUS)
										ELSE
											CLEAR_BIT(iLocalBoolCheck28, LBOOL28_PREV_AVENGER_PILOT_STATUS)
										ENDIF
										REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_VEHICLE, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - bAvengerHasDriver: GET AND DELIVER!" #ENDIF )
									ENDIF
								ENDIF
								
								IF iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]								
									IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_VEHICLE)
									OR bSkipUpdateDelay									
										IF ((MC_playerBD[iObjTxtPartToUse].iVehNear != -1) OR IS_BIT_SET(iLocalboolCheck31, LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH))
										OR ((NOT (MC_serverBD.iNumVehHighestPriorityHeld[iTeam] >= MC_serverBD.iNumvehHighestPriority[iTeam]))
										AND IS_BIT_SET(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER)
										AND bAvengerHasDriver)
											#IF IS_DEBUG_BUILD
											IF bDebugPrintCustomText
												PRINTLN("[JS] [GDTEXT] - iVehNear is valid: ", MC_playerBD[iObjTxtPartToUse].iVehNear)
											ENDIF
											#ENDIF
											
											IF (amIWaitingForPlayersToEnterVeh != NULL AND NOT CALL amIWaitingForPlayersToEnterVeh(iAvengerDriver))
												#IF IS_DEBUG_BUILD
												IF bDebugPrintCustomText
													PRINTLN("[JS] [GDTEXT] - NOT AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH")
												ENDIF
												#ENDIF
												IF (( NOT ( IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF) OR IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF) OR (iDeliveryVehForcingOut != -1) ) )
												OR AM_I_IN_A_BOUNCING_PLANE_OR_HELI())
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[JS] [GDTEXT] - Not in the drop off")
													ENDIF
													#ENDIF
													IF (MC_serverBD.iRequiredDeliveries[iTeam] <= 1)
													OR (MC_serverBD.iNumVehHighestPriorityHeld[iTeam] >= MC_serverBD.iRequiredDeliveries[iTeam])
														#IF IS_DEBUG_BUILD
														IF bDebugPrintCustomText
															PRINTLN("[JS] [GDTEXT] - Attempting to print tl63CustomGodText1")
														ENDIF
														#ENDIF
														IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
															bobjectivedisp=TRUE
															bcustomobjective = TRUE
														ELSE
															bNoSecondObjective = TRUE
														ENDIF
													ENDIF
												ELSE
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[JS] [GDTEXT] - In the drop off")
													ENDIF
													#ENDIF
													IF (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES) 
													AND NOT IS_BIT_SET(MC_serverBD.iAllPlayersInSeparateVehiclesBS[MC_playerBD[iObjTxtPartToUse].iteam], MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]))
													OR (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]], ciBS_RULE9_PROGRESS_GAD_AT_SAME_TIME)
													AND NOT IS_BIT_SET(MC_ServerBD.iAllPlayersInUniqueVehiclesBS[MC_playerBD[iObjTxtPartToUse].iteam], MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]))
													OR (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].iRuleBitsetTen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]], ciBS_RULE10_PROGRESS_RULE_ALL_DRIVER_ACCEL)
													AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_DROPPING_OFF))
													AND NOT IS_BIT_SET(MC_PlayerBD[iPartToUse].iClientBitSet4, PBBOOL4_SPAWNED_IN_A_PLACED_VEHICLE)
														#IF IS_DEBUG_BUILD
														IF bDebugPrintCustomText
															PRINTLN("[JS] [GDTEXT] - Separate vehicles but not everyone is in the drop off, attempting to print tl63CustomGodText2")
														ENDIF
														#ENDIF
														IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
															bobjectivedisp = TRUE
															bcustomobjective = TRUE
														ELSE
															bNoSecondObjective = TRUE
														ENDIF
													ELSE
														#IF IS_DEBUG_BUILD
														IF bDebugPrintCustomText
															PRINTLN("[JS] [GDTEXT] - Not Separate vehicles or separate vehicles and everyone is here (rule is about to progress), clearing text if shown")
														ENDIF
														#ENDIF
														IF Is_MP_Objective_Text_On_Display()
															Clear_Any_Objective_Text()
														ENDIF
														
														bobjectivedisp = TRUE
														bcustomobjective = TRUE
													ENDIF
												ENDIF
											ELSE
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_AVENGER_INTERIOR_ENTRY)
												AND IS_BIT_SET(iLocalBoolCheck30, LBOOL30_PRIORITY_VEHICLE_IS_AVENGER)
												AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetTen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE10_PROGRESS_RULE_ALL_DRIVER_ACCEL)	
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[JS] [GDTEXT] - Avenger waiting for players - tl63CustomGodText1")
													ENDIF
													#ENDIF
													IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
														bobjectivedisp=TRUE
														bcustomobjective = TRUE
													ENDIF
												ELSE
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[JS] [GDTEXT] - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH = TRUE")
													ENDIF
													#ENDIF
													bNoSecondObjective = TRUE
												ENDIF
											ENDIF
											
										// [FIX_2020_CONTROLLER] - Oh my lord.
										//							^^^^^^^
										ELIF MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumvehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
										OR (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE9_PROGRESS_GAD_AT_SAME_TIME)
										AND iUniqueVehiclesHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iTargetScore[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], MC_serverBD.iNumberOfPlayingPlayers[MC_playerBD[iPartToUse].iteam], MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam]) / GET_FMMC_POINTS_FOR_TEAM(MC_playerBD[iObjTxtPartToUse].iteam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]))	
										AND NOT IS_BIT_SET(MC_PlayerBD[iPartToUse].iClientBitSet4, PBBOOL4_SPAWNED_IN_A_PLACED_VEHICLE)
											#IF IS_DEBUG_BUILD
											IF bDebugPrintCustomText
												PRINTLN("[JS] [GDTEXT] - iNumVehHighestPriorityHeld (",MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam],") >= iNumvehHighestPriority(",MC_serverBD.iNumvehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam],")")
											ENDIF
											#ENDIF
											IF MC_serverBD.iTotalPlayingCoopPlayers[MC_playerBD[iObjTxtPartToUse].iteam] > 1
												#IF IS_DEBUG_BUILD
												IF bDebugPrintCustomText
													PRINTLN("[JS] [GDTEXT] - iTotalPlayingCoopPlayers > 1")
												ENDIF
												#ENDIF
												IF iDeliveryVehForcingOut = -1
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[JS] [GDTEXT] - Attempting to print tl63CustomGodText2")
													ENDIF
													#ENDIF
													IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
														bobjectivedisp=TRUE
														bcustomobjective = TRUE
													ELSE
														#IF IS_DEBUG_BUILD
														IF bDebugPrintCustomText
															PRINTLN("[JS] [GDTEXT] - Failed to print tl63CustomGodText2")
														ENDIF
														#ENDIF
														
														bDispHelpObjective=TRUE
													ENDIF
												ELSE
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[JS] [GDTEXT] - iDeliveryVehForcingOut != -1 Clearing text if shown")
													ENDIF
													#ENDIF
													IF Is_MP_Objective_Text_On_Display()
														Clear_Any_Objective_Text()
													ENDIF
													
													bobjectivedisp = TRUE
													bcustomobjective = TRUE
												ENDIF
											ENDIF
										ELSE
											#IF IS_DEBUG_BUILD
											IF bDebugPrintCustomText
												PRINTLN("[JS] [GDTEXT] - iVehNear is invalid")
											ENDIF
											#ENDIF
											
											IF Is_MP_Objective_Text_On_Display()
											AND iclientstage = CLIENT_MISSION_STAGE_DELIVER_VEH//b* 2216433
												#IF IS_DEBUG_BUILD
												IF bDebugPrintCustomText
													PRINTLN("[JS] [GDTEXT] - Text is already shown and we are on the deliver stage")
												ENDIF
												#ENDIF
												bobjectivedisp = TRUE
												bcustomobjective = TRUE
											ENDIF
																						
											IF IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
											OR IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF)
											OR iDeliveryVehForcingOut != -1
												bobjectivedisp = TRUE
												bcustomobjective = TRUE
											ENDIF
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
										IF bDebugPrintCustomText
											PRINTLN("[JS] [GDTEXT] - update not ready ")
										ENDIF
										#ENDIF
										bobjectivedisp = TRUE
										bcustomobjective = TRUE
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
									IF bDebugPrintCustomText
										PRINTLN("[JS] [GDTEXT] - Clearing obj text as it probably isn't valid...")
									ENDIF
									#ENDIF
									IF Is_MP_Objective_Text_On_Display()
										Clear_Any_Objective_Text()
									ENDIF
									bobjectivedisp = TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_CAPTURE_OBJ
							CASE CLIENT_MISSION_STAGE_COLLECT_OBJ
							CASE CLIENT_MISSION_STAGE_DELIVER_OBJ
								
								iTeam = MC_playerBD[iObjTxtPartToUse].iteam
								
								#IF IS_DEBUG_BUILD
								IF bDebugPrintCustomText
									PRINTLN("[PROCESS_OBJECTIVE_TEXT] iClientstage = ", GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(iclientstage))
									PRINTLN("[PROCESS_OBJECTIVE_TEXT] iObjTxtPartToUse = ", iObjTxtPartToUse)
									PRINTLN("[PROCESS_OBJECTIVE_TEXT] iTeam = ", iTeam)
									PRINTLN("[PROCESS_OBJECTIVE_TEXT] iLastRule = ", iLastRule)
									PRINTLN("[PROCESS_OBJECTIVE_TEXT] MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam] = ", MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam])
								ENDIF
								#ENDIF
								IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_OBJECT)
									IF iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]
										IF MC_playerBD[iObjTxtPartToUse].iObjCarryCount > 0
										OR IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet, PBBOOL_WITH_CARRIER)
										OR MC_playerBD[iObjTxtPartToUse].iObjCapturing != -1
											IF (NOT (IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
											OR IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF)))
												IF (MC_serverBD.iRequiredDeliveries[iTeam] <= 1)
												OR (MC_serverBD.iNumObjHighestPriorityHeld[iTeam] >= MC_serverBD.iRequiredDeliveries[iTeam])
													IF MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] > 0
													OR MC_playerBD[iObjTxtPartToUse].iObjCarryCount > 0
													OR MC_playerBD[iObjTxtPartToUse].iObjCapturing != -1
														IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
															bobjectivedisp = TRUE
															bcustomobjective = TRUE
														ELSE
															bNoSecondObjective = TRUE
														ENDIF
													ENDIF
												ENDIF
											ELSE
												IF Is_MP_Objective_Text_On_Display()
													Clear_Any_Objective_Text()
												ENDIF
												bobjectivedisp = TRUE
												bcustomobjective = TRUE
											ENDIF
										ELIF MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumobjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
												IF MC_serverBD.iTotalPlayingCoopPlayers[MC_playerBD[iObjTxtPartToUse].iteam] > 1
													IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
														bobjectivedisp = TRUE
														bcustomobjective = TRUE
													ELSE
														bDispHelpObjective = TRUE
													ENDIF
												ENDIF
											
										ENDIF
									ELSE
										bobjectivedisp = TRUE
										bcustomobjective = TRUE
									ENDIF
								ELSE
									bobjectivedisp = TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							
									
							CASE CLIENT_MISSION_STAGE_HACK_COMP
							CASE CLIENT_MISSION_STAGE_HACK_DRILL
							
								IF iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam] 
									IF MC_playerBD[iObjTxtPartToUse].iObjHacking != -1
									OR IS_BIT_SET(iLocalBoolCheck12, LBOOL12_SERVER_STILL_THINKS_IM_HACKING)
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
											bobjectivedisp=TRUE
											bcustomobjective = TRUE
										ELSE
											bNoSecondObjective = TRUE
										ENDIF
									ELSE
										IF MC_serverBD.iNumberOfPlayingPlayers[MC_playerBD[iObjTxtPartToUse].iteam] > 1
											
											IF ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1)
												AND (MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumobjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]))
											OR ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] = 1)
												AND IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_IS_HACKING + MC_playerBD[iObjTxtPartToUse].iTeam))
												
												IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
													bobjectivedisp=TRUE
													bcustomobjective = TRUE
												ELSE
													bDispHelpObjective=TRUE
												ENDIF
											ENDIF
											
										ENDIF
									ENDIF
								ELSE
									bobjectivedisp=TRUE
									bcustomobjective = TRUE
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_INTERACT_WITH
							
								IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_OBJECT)
									SWITCH sInteractWithVars.iInteractWith_ObjectiveTextToUse
										CASE ciObjectiveText_Primary
											IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
												bcustomobjective = TRUE
											ENDIF
										BREAK
										
										CASE ciObjectiveText_Secondary
											IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
												bcustomobjective = TRUE
												bobjectivedisp = TRUE
											ENDIF
										BREAK
										
										CASE ciObjectiveText_AltSecondary
											IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
												bcustomobjective = TRUE
												bobjectivedisp = TRUE
											ENDIF
										BREAK
									ENDSWITCH
									
								ELSE
									bobjectivedisp = TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_UNDERWATER_WELDING
								IF IS_LOCAL_PLAYER_ABLE_TO_DO_LEADER_RESTRICTED_INTERACTION()
									IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
										bcustomobjective = TRUE										
									ENDIF
								ELSE
									IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
										bcustomobjective = TRUE
										bobjectivedisp = TRUE
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GLASS_CUTTING
								IF MC_playerBD[iObjTxtPartToUse].iObjHacking = -1
								AND IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_IS_HACKING + MC_playerBD[iObjTxtPartToUse].iTeam)
									IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
										bcustomobjective = TRUE
										bobjectivedisp = TRUE
									ENDIF
								ELSE
									IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
										bcustomobjective = TRUE
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_INTERROGATION
								IF NOT IS_LOCAL_PLAYER_ABLE_TO_DO_LEADER_RESTRICTED_INTERACTION()
									IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
										bcustomobjective = TRUE
										bobjectivedisp = TRUE
									ENDIF
								ELSE
									IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
										bcustomobjective = TRUE
									ENDIF
								ENDIF
							BREAK

							CASE CLIENT_MISSION_STAGE_ENTER_SAFE_COMBINATION
								IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
									bcustomobjective = TRUE
								ENDIF
							BREAK								
					
							CASE CLIENT_MISSION_STAGE_POINTS_THRESHOLD
								IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
									bcustomobjective = TRUE
								ENDIF
							BREAK

							
							CASE CLIENT_MISSION_STAGE_HACK_FINGERPRINT_KEYPAD
								
								PRINTLN("[FingerprintKeypadHack] MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]: ", MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam])
								PRINTLN("[FingerprintKeypadHack] MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam]: ", MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam])
								
								IF MC_playerBD[iLocalPart].iObjHacking = -1
									IF ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1)
									AND (MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumobjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]))
									OR ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] = 1)
									AND IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_IS_HACKING + MC_playerBD[iObjTxtPartToUse].iTeam))
									AND fkhaFingerprintKeypadHackAnimState != FKHA_STATE_LOOP_PRE_EXIT
											
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
											bcustomobjective = TRUE
											bobjectivedisp = TRUE
											PRINTLN("[FingerprintKeypadHack] Printing 'watch your team do the hack' objective text")
										ELSE
											PRINTLN("[FingerprintKeypadHack] PRINT_CUSTOM_OBJECTIVE returned FALSE")
										ENDIF
									ELSE
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
											bcustomobjective = TRUE
										ENDIF
									ENDIF
								ELSE
									PRINTLN("[FingerprintKeypadHack] Not doing PRINT_CUSTOM_OBJECTIVE because my iObjHacking isn't -1")
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_HACKING
								
								PRINTLN("[HackObjText] MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]: ", MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam])
								PRINTLN("[HackObjText] MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam]: ", MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam])
								
								IF MC_playerBD[iLocalPart].iObjHacking = -1
									IF ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1)
									AND (MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumobjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]))
									OR ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] = 1)
									AND IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_IS_HACKING + MC_playerBD[iObjTxtPartToUse].iTeam))
											
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
											bcustomobjective = TRUE
											bobjectivedisp = TRUE
											PRINTLN("Printing 'watch your team do the hack' objective text")
										ELSE
											PRINTLN("PRINT_CUSTOM_OBJECTIVE returned FALSE")
										ENDIF
									ELSE
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
											bcustomobjective = TRUE
										ENDIF
									ENDIF
								ELSE
									PRINTLN("Not doing PRINT_CUSTOM_OBJECTIVE because my iObjHacking isn't -1")
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_BEAMHACK_VEH
								IF IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_BEAMHACK_IN_VEH_VALID)
									IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
										bcustomobjective = TRUE
									ENDIF
								ELIF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_STARTED_BEAM_HACK)
									IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
										bcustomobjective = TRUE
										bobjectivedisp = TRUE
									ENDIF
								ELSE
									IF GET_TOTAL_PLAYING_PLAYERS() > 1
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
											bcustomobjective = TRUE
											bobjectivedisp = TRUE
										ENDIF
									ELSE
										IF PRINT_CUSTOM_OBJECTIVE(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl63WaitMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]])
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_INSERT_FUSE
								IF iMinigameObjPreReqBlockerBS != 0 //We haven't picked up the final fuse
									IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
										bcustomobjective = TRUE
									ENDIF
								ELSE
									IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam] > 1
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
											bcustomobjective = TRUE
											bobjectivedisp = TRUE
										ENDIF
									ELSE
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
											bcustomobjective = TRUE
											bobjectivedisp = TRUE
										ENDIF
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_KILL_PLAYERS
								IF PRINT_CUSTOM_COLOURED_OBJECTIVE(tl63CustomGodText,HUD_COLOUR_RED)
									bobjectivedisp=TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_KILL_TEAM0
								IF PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE(tl63CustomGodText,0)
									bobjectivedisp=TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							CASE CLIENT_MISSION_STAGE_KILL_TEAM1
								IF PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE(tl63CustomGodText,1)
									bobjectivedisp=TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							CASE CLIENT_MISSION_STAGE_KILL_TEAM2
								IF PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE(tl63CustomGodText,2)
									bobjectivedisp=TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK

							CASE CLIENT_MISSION_STAGE_KILL_TEAM3
								IF PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE(tl63CustomGodText,3)
									bobjectivedisp=TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GOTO_ANY_PLAYER
								IF PRINT_CUSTOM_COLOURED_OBJECTIVE(tl63CustomGodText,HUD_COLOUR_RED)
									bobjectivedisp = TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GOTO_TEAM0
							CASE CLIENT_MISSION_STAGE_GOTO_TEAM1
							CASE CLIENT_MISSION_STAGE_GOTO_TEAM2
							CASE CLIENT_MISSION_STAGE_GOTO_TEAM3
								IF PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE(tl63CustomGodText, (iclientstage - CLIENT_MISSION_STAGE_GOTO_TEAM0)) // iclientstage - CLIENT_MISSION_STAGE_GOTO_TEAM0 = the team we're supposed to be going to!
									bobjectivedisp=TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_KILL_OBJ
								IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
									bobjectivedisp=TRUE
									bcustomobjective = TRUE									
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GET_MASK
								IF NOT bIsSCTV
									IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
										bobjectivedisp=TRUE
										bcustomobjective = TRUE
									ELSE
										bNoSecondObjective = TRUE
									ENDIF
								ELSE
									bobjectivedisp = TRUE
									bcustomobjective = TRUE
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_LOOT_THRESHOLD
								
								IF MC_serverBD.iCurrentPlayerRule[MC_playerBD[iObjTxtPartToUse].iTeam] > -1
								AND MC_serverBD.iCurrentPlayerRule[MC_playerBD[iObjTxtPartToUse].iTeam] < FMMC_MAX_RULES
									IF IS_BIT_SET(MC_serverBD.iPlayerRuleBitset[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iObjTxtPartToUse].iTeam]][MC_playerBD[iObjTxtPartToUse].iTeam], ciPLAYER_RULE_BITSET_UseBagCapacityForLootThreshold)
									AND ROUND((MC_playerBD[iObjTxtPartToUse].sLootBag.fCurrentBagCapacity / BAG_CAPACITY__GET_MAX_CAPACITY()) * 100) >= MC_serverBD.iPlayerRuleLimit[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iObjTxtPartToUse].iTeam]][MC_playerBD[iObjTxtPartToUse].iTeam]
										IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_PLAYER)
											IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
												bobjectivedisp = TRUE
												bcustomobjective = TRUE
											ENDIF 
										ELSE
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF
									ELSE
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF 
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_PHONE_BULLSHARK
							CASE CLIENT_MISSION_STAGE_PHONE_AIRSTRIKE
								
								//Custom phone rule objective text goes here
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_HOLDING_RULE
								IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
									bobjectivedisp = TRUE
									bcustomobjective = TRUE
								ENDIF 
							BREAK
							
							CASE CLIENT_MISSION_STAGE_PROTECT_OBJ
								IF PRINT_CUSTOM_OBJECTIVE( tl63CustomGodText )
									bobjectivedisp = TRUE
									bcustomobjective = TRUE
								ENDIF 
							BREAK
							
							CASE CLIENT_MISSION_STAGE_SCRIPTED_CUTSCENE
								
								bNoSecondObjective = TRUE
								
								IF IS_BIT_SET( iLocalBoolCheck8, LBOOL8_skipped_to_mid_mission_cutscene_stage_5 )
								OR IS_BIT_SET( MC_playerBD[iObjTxtPartToUse].iClientBitSet2, PBBOOL2_FINISHED_CUTSCENE ) // B* 4043147
									IF PRINT_CUSTOM_OBJECTIVE( tl63CustomGodText )
										bobjectivedisp = TRUE
										bcustomobjective = TRUE
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_RESULTS
								#IF IS_DEBUG_BUILD
								IF bDebugPrintCustomText
									PRINTLN("[JS] [GDTEXT] - CLIENT_MISSION_STAGE_RESULTS - CUSTOM")
								ENDIF
								#ENDIF
								Clear_Any_Objective_Text()
								bobjectivedisp = TRUE
								bcustomobjective = TRUE
							BREAK							
								
							CASE CLIENT_MISSION_STAGE_LEAVE_PED								
								iTeam = MC_playerBD[iObjTxtPartToUse].iteam
								iNumberOfPlayersLeft = GET_PLAYERS_LEFT_LEAVE_OBJECTIVE_ENTITY(iTeam, MC_playerBD[iObjTxtPartToUse].iPedNear)
								iNumberOfPlayersRequiredToLeave = GET_NUMBER_OF_PLAYERS_REQUIRED_TO_LEAVE_ENTITY(iTeam)									
								IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_PED)
								AND iLastRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
									IF MC_playerBD[iObjTxtPartToUse].iPedNear = -1
									AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE15_LEAVE_ENTITY_OBJECTIVE_ENTIRE_TEAM)
									AND iNumberOfPlayersLeft >= iNumberOfPlayersRequiredToLeave
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF
										
									ELIF MC_serverBD.iNumPedHighestPriority[iTeam] > 1
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF
									ENDIF
								ENDIF
							BREAK
								
							CASE CLIENT_MISSION_STAGE_LEAVE_VEH
								iTeam = MC_playerBD[iObjTxtPartToUse].iteam
								iNumberOfPlayersLeft = GET_PLAYERS_LEFT_LEAVE_OBJECTIVE_ENTITY(iTeam, MC_playerBD[iObjTxtPartToUse].iVehNear)
								iNumberOfPlayersRequiredToLeave = GET_NUMBER_OF_PLAYERS_REQUIRED_TO_LEAVE_ENTITY(iTeam)
								IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_VEHICLE)
								AND iLastRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]																								
									IF MC_playerBD[iObjTxtPartToUse].iVehNear = -1
									AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE15_LEAVE_ENTITY_OBJECTIVE_ENTIRE_TEAM)
									AND iNumberOfPlayersLeft >= iNumberOfPlayersRequiredToLeave
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF
										
									ELIF MC_serverBD.iNumVehHighestPriority[iTeam] > 1
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF										
									ENDIF									
								ENDIF
							BREAK	
							
							CASE CLIENT_MISSION_STAGE_LEAVE_OBJ
								iTeam = MC_playerBD[iObjTxtPartToUse].iteam
								iNumberOfPlayersLeft = GET_PLAYERS_LEFT_LEAVE_OBJECTIVE_ENTITY(iTeam, MC_playerBD[iObjTxtPartToUse].iVehNear)
								iNumberOfPlayersRequiredToLeave = GET_NUMBER_OF_PLAYERS_REQUIRED_TO_LEAVE_ENTITY(iTeam)
								IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_OBJECT)
								AND iLastRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]																	
									IF MC_playerBD[iObjTxtPartToUse].iObjNear = -1
									AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE15_LEAVE_ENTITY_OBJECTIVE_ENTIRE_TEAM)
									AND iNumberOfPlayersLeft >= iNumberOfPlayersRequiredToLeave
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF
										
									ELIF MC_serverBD.iNumObjHighestPriority[iTeam] > 1
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF										
									ENDIF									
								ENDIF
							BREAK
							
							
							CASE CLIENT_MISSION_STAGE_PED_GOTO_LOCATION								
								iTeam = MC_playerBD[iObjTxtPartToUse].iteam
								
								IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_PED)
								AND iLastRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
									IF MC_serverBD.iNumPedHighestPriority[iTeam] = 1
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF
										
									ELIF MC_serverBD.iNumPedHighestPriority[iTeam] > 1
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_DAMAGE_PED								
								iTeam = MC_playerBD[iObjTxtPartToUse].iteam
								
								IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_PED)
								AND iLastRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
									IF MC_serverBD.iNumPedHighestPriority[iTeam] = 1
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF
										
									ELIF MC_serverBD.iNumPedHighestPriority[iTeam] > 1
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF
									ENDIF
								ENDIF
							BREAK
								
							CASE CLIENT_MISSION_STAGE_DAMAGE_VEH
								iTeam = MC_playerBD[iObjTxtPartToUse].iteam
								
								IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_VEHICLE)
								AND iLastRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]																								
									IF MC_serverBD.iNumVehHighestPriority[iTeam] = 1
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF
										
									ELIF MC_serverBD.iNumVehHighestPriority[iTeam] > 1
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF										
									ENDIF
								ELSE
									bobjectivedisp = TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK	
							
							CASE CLIENT_MISSION_STAGE_DAMAGE_OBJ
								iTeam = MC_playerBD[iObjTxtPartToUse].iteam
								
								IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_OBJECT)
								AND iLastRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]																	
									IF MC_serverBD.iNumObjHighestPriority[iTeam] = 1
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF
										
									ELIF MC_serverBD.iNumObjHighestPriority[iTeam] > 1
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF										
									ENDIF									
								ENDIF
							BREAK
							
						ENDSWITCH
												
						#IF IS_DEBUG_BUILD
						IF bDebugPrintCustomText
							PRINTLN("[PROCESS_OBJECTIVE_TEXT] bobjectivedisp = ", bobjectivedisp)
							PRINTLN("[PROCESS_OBJECTIVE_TEXT] bDispHelpObjective = ", bobjectivedisp)
							PRINTLN("[PROCESS_OBJECTIVE_TEXT] bNoSecondObjective = ", bNoSecondObjective)
							IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl63WaitMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]])
								PRINTLN("[PROCESS_OBJECTIVE_TEXT] tl63WaitMessage = ", g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl63WaitMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]])
							ELSE
								PRINTLN("[PROCESS_OBJECTIVE_TEXT] tl63WaitMessage = EMPTY")
							ENDIF
						ENDIF
						#ENDIF
						
						IF iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam] //add's a frame delay on rule changes
							IF NOT bobjectivedisp
							AND NOT bDispHelpObjective
							AND NOT bNoSecondObjective
								IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
									#IF IS_DEBUG_BUILD
									IF bDebugPrintCustomText
										PRINTLN("[PROCESS_OBJECTIVE_TEXT] PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText) = ", tl63CustomGodText)
									ENDIF
									#ENDIF
									bcustomobjective = TRUE
								ENDIF
							ENDIF
						ELSE
							bcustomobjective = TRUE
						ENDIF
						
					ENDIF
					
					IF (NOT bcustomobjective)
					AND iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]
						SWITCH iclientstage
							
							CASE CLIENT_MISSION_STAGE_GOTO_LOC
								
								IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
									IF (isPlayerInRequiredLocationVehicle != NULL AND NOT CALL isPlayerInRequiredLocationVehicle(GET_PLAYER_PED(ObjTxtPlayerToUse), MC_playerBD[iObjTxtPartToUse].iteam, MC_playerBD[iObjTxtPartToUse].iCurrentLoc))
										PRINT_NORMAL_OBJECTIVE("MC_WRG_VEH")
									ELSE
										IF MC_playerBD[iObjTxtPartToUse].iCurrentLoc = -1
											IF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)
											AND NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iObjTxtPartToUse].iteam)
												IF MC_serverBD.iNumLocHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
													PRINT_NORMAL_OBJECTIVE("MC_GOTO_LOCS")
												ELSE
													PRINT_NORMAL_OBJECTIVE("MC_GOTO_LOC")
												ENDIF
											ENDIF
										ELSE
											IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iObjTxtPartToUse].iCurrentLoc].iWholeTeamAtLocation[MC_playerBD[iObjTxtPartToUse].iteam] != ciGOTO_LOCATION_INDIVIDUAL
												IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iObjTxtPartToUse].iCurrentLoc].iWholeTeamAtLocation[MC_playerBD[iObjTxtPartToUse].iteam] != ciGOTO_LOCATION_WHOLE_TEAM
												OR MC_serverBD.iNumberOfPlayingPlayers[MC_playerBD[iObjTxtPartToUse].iteam] > 1
													IF IS_BIT_SET(MC_playerBD[iLocalPart].iLocationInSecondaryLocation, MC_playerBD[iObjTxtPartToUse].iCurrentLoc)
														PRINT_NORMAL_OBJECTIVE("MC_WAIT_T")
													ELSE
														IF MC_serverBD.iNumLocHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
															PRINT_NORMAL_OBJECTIVE("MC_GOTO_LOCS")
														ELSE
															PRINT_NORMAL_OBJECTIVE("MC_GOTO_LOC")
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GOTO_PED
							
								IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									PRINT_NORMAL_OBJECTIVE("MC_GOTO_PEDS")
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_GOTO_PED")
								ENDIF
								
							BREAK
								
							CASE CLIENT_MISSION_STAGE_LEAVE_PED
							
								IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									PRINT_NORMAL_OBJECTIVE("MC_LEAV_PEDS")
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_LEAV_PED")
								ENDIF
								
							BREAK
								
							CASE CLIENT_MISSION_STAGE_LEAVE_VEH
							
								IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									PRINT_NORMAL_OBJECTIVE("MC_LEAV_VEHS")
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_LEAV_VEH")
								ENDIF
								
							BREAK	
							
							CASE CLIENT_MISSION_STAGE_LEAVE_OBJ
							
								IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									PRINT_NORMAL_OBJECTIVE("MC_LEAV_OBJS")
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_LEAV_OBJ")
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_LEAVE_LOC
								
								IF IS_BIT_SET(iLocalBoolCheck6,LBOOL6_IN_MY_PRIORITY_LEAVE_LOC)
									PRINT_NORMAL_OBJECTIVE("MC_LVELOC")
								ELSE
									IF IS_BIT_SET(MC_serverBD.iServerBitSet2,SBBOOL2_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + MC_playerBD[iObjTxtPartToUse].iteam)
										IF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)
											PRINT_NORMAL_OBJECTIVE("MC_WAIT_TL")
										ENDIF
									ENDIF
								ENDIF
							
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GOTO_VEH
								
								IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_VEHICLE)
									#IF IS_DEBUG_BUILD
									IF bDebugPrintCustomText
										PRINTLN("[JS] [GDTEXT] - GOTO VEH DEFAULT - Ready to update")
									ENDIF
									#ENDIF
									IF MC_playerBD[iObjTxtPartToUse].iVehNear != -1
										#IF IS_DEBUG_BUILD
										IF bDebugPrintCustomText
											PRINTLN("[JS] [GDTEXT] - GOTO VEH DEFAULT - iVehNear OK")
										ENDIF
										#ENDIF
										BOOL bCheckOnRule // To let us use GET_VEHICLE_GOTO_TEAMS here
										IF (GET_VEHICLE_GOTO_TEAMS(MC_playerBD[iObjTxtPartToUse].iteam,MC_playerBD[iObjTxtPartToUse].iVehNear,bCheckOnRule) > 0)
											#IF IS_DEBUG_BUILD
											IF bDebugPrintCustomText
												PRINTLN("[JS] [GDTEXT] - GOTO VEH DEFAULT - GET_VEHICLE_GOTO_TEAMS OK")
											ENDIF
											#ENDIF
											PRINT_NORMAL_OBJECTIVE("MC_VEH_WT")
										ENDIF
									ELSE
										IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
											#IF IS_DEBUG_BUILD
											IF bDebugPrintCustomText
												PRINTLN("[JS] [GDTEXT] - GOTO VEH DEFAULT - PLURAL")
											ENDIF
											#ENDIF
											PRINT_NORMAL_OBJECTIVE("MC_GOTO_VEHS")
										ELSE
											#IF IS_DEBUG_BUILD
											IF bDebugPrintCustomText
												PRINTLN("[JS] [GDTEXT] - GOTO VEH DEFAULT - SINGULAR")
											ENDIF
											#ENDIF
											PRINT_NORMAL_OBJECTIVE("MC_GOTO_VEH")
										ENDIF
									ENDIF
								#IF IS_DEBUG_BUILD
								ELSE
									IF bDebugPrintCustomText
										PRINTLN("[JS] [GDTEXT] - GOTO VEH DEFAULT - NOT ready to update")
									ENDIF
								#ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GOTO_OBJ
							
								IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									PRINT_NORMAL_OBJECTIVE("MC_GOTO_OBJS")
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_GOTO_OBJ")
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_CAPTURE_AREA
							
								IF MC_serverBD.iNumLocHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									IF MC_playerBD[iObjTxtPartToUse].iCurrentLoc =-1
										PRINT_NORMAL_OBJECTIVE("MC_GO_AREAS")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_CON_AREAS")
									ENDIF
								ELSE
									IF MC_playerBD[iPartToUse].iCurrentLoc =-1
										PRINT_NORMAL_OBJECTIVE("MC_GO_AREA")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_CON_AREA")
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_CAPTURE_PED
							
								IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									IF (MC_playerBD[iObjTxtPartToUse].iPedCarryCount = 0
									AND NOT IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
									AND NOT IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_IN_GROUP_PED_VEH))
										IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_PED)
											IF MC_serverBD.iNumPedHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] 
												PRINT_NORMAL_OBJECTIVE("MC_CON_PEDS")
											ELSE					
												PRINT_NORMAL_OBJECTIVE("MC_HCON_PEDS")	
											ENDIF
										ENDIF
									ELSE
										IF MC_playerBD[iObjTxtPartToUse].iPedCarryCount = 1
											PRINT_NORMAL_OBJECTIVE("MC_MCON_PED")	
										ELSE
											PRINT_NORMAL_OBJECTIVE("MC_MCON_PEDS")	
										ENDIF
									ENDIF
								ELIF  MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] = 1
									IF (MC_playerBD[iObjTxtPartToUse].iPedCarryCount = 0
									AND NOT IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
									AND NOT IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_IN_GROUP_PED_VEH))
										IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_PED)
											IF MC_serverBD.iNumPedHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] 
												PRINT_NORMAL_OBJECTIVE("MC_CON_PED")	
											ELSE
												PRINT_NORMAL_OBJECTIVE("MC_HCON_PED")	
											ENDIF
										ENDIF
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_MCON_PED")	
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_CAPTURE_VEH
								
								
								IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_VEHICLE)
								OR bIsAnySpectator
									IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										IF MC_playerBD[iObjTxtPartToUse].iVehNear = -1
										AND NOT IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
											IF MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
												//Capture the vehicles
												PRINT_NORMAL_OBJECTIVE("MC_CON_VEHS")
											ELSE
												//Help your team
												PRINT_NORMAL_OBJECTIVE("MC_HCON_VEHS")
											ENDIF
										ELSE
											//Maintain control
											PRINT_NORMAL_OBJECTIVE("MC_MCON_VEHS")
										ENDIF
									ELIF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] = 1
										IF MC_playerBD[iPartToUse].iVehNear = -1
										AND NOT IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
											IF MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
												//Capture the vehicle
												PRINT_NORMAL_OBJECTIVE("MC_CON_VEH")
											ELSE
												//Help your team
												PRINT_NORMAL_OBJECTIVE("MC_HCON_VEH")
											ENDIF
										ELSE
											//Maintain control
											PRINT_NORMAL_OBJECTIVE("MC_MCON_VEH")
										ENDIF
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_CAPTURE_OBJ
							
								IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									IF MC_playerBD[iObjTxtPartToUse].iObjCarryCount = 0
									AND NOT IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
									AND MC_playerBD[iLocalPart].iObjCapturing = -1
										IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_OBJECT)
											IF MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
												PRINT_NORMAL_OBJECTIVE("MC_CON_OBJS")
											ELSE
												PRINT_NORMAL_OBJECTIVE("MC_HCON_OBJS")
											ENDIF
										ENDIF
									ELSE
										IF MC_playerBD[iObjTxtPartToUse].iObjCarryCount = 1
										OR MC_playerBD[iLocalPart].iObjCapturing != -1
											PRINT_NORMAL_OBJECTIVE("MC_MCON_OBJ")
										ELSE
											PRINT_NORMAL_OBJECTIVE("MC_MCON_OBJS")
										ENDIF
									ENDIF
								ELIF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] = 1
									IF MC_playerBD[iObjTxtPartToUse].iObjCarryCount = 0
									AND NOT IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
									AND MC_playerBD[iLocalPart].iObjCapturing = -1
										IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_OBJECT)
											IF MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
												PRINT_NORMAL_OBJECTIVE("MC_CON_OBJ")
											ELSE
												PRINT_NORMAL_OBJECTIVE("MC_HCON_OBJ")
											ENDIF
										ENDIF
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_MCON_OBJ")
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_KILL_PED								
								IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									FOR i = 0 TO FMMC_MAX_PEDS-1
										IF DOES_BLIP_EXIST(biPedBlip[i]) //only show kill text if something to kill											
											PRINT_NORMAL_OBJECTIVE("MC_K_PEDS")
											i = FMMC_MAX_PEDS
											bFoundBlip = TRUE
										ENDIF
									ENDFOR
									
									IF NOT bFoundBlip
										IF Is_MP_Objective_Text_On_Display()
											Clear_Any_Objective_Text()
										ENDIF
									ENDIF
								ELSE
									IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 0
										PRINT_NORMAL_OBJECTIVE("MC_K_PED")
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_PED_GOTO_LOCATION
								IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									PRINT_NORMAL_OBJECTIVE("MC_GTO_PEDS")
								ELIF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 0
									PRINT_NORMAL_OBJECTIVE("MC_GTO_PED")									
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_DAMAGE_PED
								IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									FOR i = 0 TO FMMC_MAX_PEDS-1
										IF DOES_BLIP_EXIST(biPedBlip[i]) //only show kill text if something to kill											
											PRINT_NORMAL_OBJECTIVE("MC_DMG_PEDS")											
											bFoundBlip = TRUE
											BREAKLOOP
										ENDIF
									ENDFOR
									
									IF NOT bFoundBlip
										IF Is_MP_Objective_Text_On_Display()
											Clear_Any_Objective_Text()
										ENDIF
									ENDIF
								ELSE
									IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 0
										PRINT_NORMAL_OBJECTIVE("MC_DMG_PED")
									ENDIF
								ENDIF
							BREAK
														
							CASE CLIENT_MISSION_STAGE_DAMAGE_VEH
								IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_VEHICLE)
									IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										FOR i = 0 TO FMMC_MAX_VEHICLES-1
											IF DOES_BLIP_EXIST(biVehBlip[i]) //only show kill text if something to kill											
												PRINT_NORMAL_OBJECTIVE("MC_DMG_VEHS")
												bFoundBlip = TRUE
												BREAKLOOP
											ENDIF
										ENDFOR
										
										IF NOT bFoundBlip
											IF Is_MP_Objective_Text_On_Display()
												Clear_Any_Objective_Text()
											ENDIF
										ENDIF
									ELSE
										IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 0
											PRINT_NORMAL_OBJECTIVE("MC_DMG_VEH")
										ENDIF
									ENDIF
								ENDIF
							BREAK
														
							CASE CLIENT_MISSION_STAGE_DAMAGE_OBJ
								IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									FOR i = 0 TO FMMC_MAX_NUM_OBJECTS-1
										IF DOES_BLIP_EXIST(biObjBlip[i]) //only show kill text if something to kill											
											PRINT_NORMAL_OBJECTIVE("MC_DMG_OBJS")
											bFoundBlip = TRUE
											BREAKLOOP
										ENDIF
									ENDFOR
									
									IF NOT bFoundBlip
										IF Is_MP_Objective_Text_On_Display()
											Clear_Any_Objective_Text()
										ENDIF
									ENDIF
								ELSE
									IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 0
										PRINT_NORMAL_OBJECTIVE("MC_DMG_OBJ")
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_KILL_VEH
							
								IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									PRINT_NORMAL_OBJECTIVE("MC_K_VEHS")
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_K_VEH")
								ENDIF
								
							BREAK 
							
							CASE CLIENT_MISSION_STAGE_KILL_OBJ
								IF iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam] //bit of a hack to sync this
									IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										PRINT_NORMAL_OBJECTIVE("MC_K_OBJS")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_K_OBJ")
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_PROTECT_PED
							
								IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									PRINT_NORMAL_OBJECTIVE("MC_P_PEDS")
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_P_PED")
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_PROTECT_VEH
							
								IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									PRINT_NORMAL_OBJECTIVE("MC_P_VEHS")
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_P_VEH")
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_PROTECT_OBJ
								IF iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]
									IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										PRINT_NORMAL_OBJECTIVE("MC_P_OBJS")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_P_OBJ")
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_COLLECT_PED
								
								IF MC_serverBD.iNumPedHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
									IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										PRINT_NORMAL_OBJECTIVE("MC_C_PEDS")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_C_PED")
									ENDIF
								ELSE
									IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 0
										IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_PED)
										AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
											//Differentiate between dropping off at a locate or at a vehicle:
											IF (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] != ciFMMC_DROP_OFF_TYPE_VEHICLE)
											OR (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOff_Vehicle[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] = -1)
												
												IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
													PRINT_NORMAL_OBJECTIVE("MC_HD_PEDS")
												ELSE
													PRINT_NORMAL_OBJECTIVE("MC_HD_PED")
												ENDIF
												
											ELSE
												
												IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
													PRINT_NORMAL_OBJECTIVE("MC_HD_PEDS_V")
												ELSE
													PRINT_NORMAL_OBJECTIVE("MC_HD_PED_V")
												ENDIF
												
											ENDIF
											
										ENDIF
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_DELIVER_PED
							
								IF iPartEscorting != -1
									IF IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER) AND MC_playerBD[iPartEscorting].iPedCarryCount > 1
										bPlural = TRUE
									ENDIF
								ENDIF
								IF MC_playerBD[iObjTxtPartToUse].iPedCarryCount > 1
									bPlural = TRUE
								ENDIF
								
								IF NOT (IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
										OR IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF))
								AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
									IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_PED)
										//Differentiate between dropping off at a locate or at a vehicle:
										IF (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] != ciFMMC_DROP_OFF_TYPE_VEHICLE)
										OR (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOff_Vehicle[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] = -1)
											
											IF bPlural
												PRINT_NORMAL_OBJECTIVE("MC_D_PEDS")
											ELSE
												PRINT_NORMAL_OBJECTIVE("MC_D_PED")
											ENDIF
											
										ELSE
										
											IF bPlural
												PRINT_NORMAL_OBJECTIVE("MC_D_PEDS_V")
											ELSE
												PRINT_NORMAL_OBJECTIVE("MC_D_PED_V")
											ENDIF
											
										ENDIF
									ENDIF
								ELSE
									IF Is_MP_Objective_Text_On_Display()
										Clear_Any_Objective_Text()
									ENDIF
								ENDIF

							BREAK

							
							CASE CLIENT_MISSION_STAGE_COLLECT_VEH

								IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_VEHICLE)								
									IF (amIWaitingForPlayersToEnterVeh != NULL AND CALL amIWaitingForPlayersToEnterVeh())
										IF (((IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]], ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES) 
										OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]], ciBS_RULE9_PROGRESS_GAD_AT_SAME_TIME))
										AND IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE))
										OR NOT (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]], ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES) OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]], ciBS_RULE9_PROGRESS_GAD_AT_SAME_TIME)))
											IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl63WaitMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]])
												TEXT_LABEL_63 tlWaitText
												tlWaitText = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl63WaitMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]]
												
												IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_OVERRIDE_FRIENDLY_WITH_GANG)
													INT iR, iG, iB, iA
													GET_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam,LocalPlayer),iR, iG, iB,iA)
													SET_SCRIPT_VARIABLE_HUD_COLOUR(iR, iG, iB,iA)
													PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH - PRE change: ", tlWaitText)
													REPLACE_STRING_IN_STRING(tlWaitText,"~f~", "~v~")
													PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH - POST change: ", tlWaitText, " ~v~ colour: ",iR,", ", iG,", ", iB,", ",iA)
												ENDIF
												
												PRINT_CUSTOM_OBJECTIVE(tlWaitText)
											ELSE
												IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl23ObjSingular[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]])
												AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].iRuleBitsetFour[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]], ciBS_RULE4_ALT_TEAM_TEXT)
													IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE2_BOARD_VEHICLE) 
														PRINT_OBJECTIVE_WITH_CUSTOM_STRING("MC_WTCLBRD",g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl23ObjSingular[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]])
													ELSE
														PRINT_OBJECTIVE_WITH_CUSTOM_STRING("MC_WTCLCUST",g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl23ObjSingular[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]])
													ENDIF
												ELSE
													IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].iRuleBitsetFour[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]], ciBS_RULE4_ALT_TEAM_TEXT)
														PRINT_NORMAL_OBJECTIVE("MC_WTCLPRT")
													ELSE
														PRINT_NORMAL_OBJECTIVE("MC_WTCL")
													ENDIF
												ENDIF
											ENDIF
											
											IF DOES_BLIP_EXIST(DeliveryBlip)
												REMOVE_DELIVERY_BLIP()
											ENDIF
										ENDIF
									ELSE

										IF MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
											IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
												PRINT_NORMAL_OBJECTIVE("MC_C_VEHS")
											ELSE
												//Collect the vehicle
												PRINT_NORMAL_OBJECTIVE("MC_C_VEH")
											ENDIF
										ELSE
											IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 0
												IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
													//Differentiate between dropping off at a locate or at a vehicle:
													IF (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] != ciFMMC_DROP_OFF_TYPE_VEHICLE)
													OR (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOff_Vehicle[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] = -1)
														
														IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
															//Help your team deliver the vehicles
															PRINT_NORMAL_OBJECTIVE("MC_HD_VEHS")
														ELSE
															//Help your team deliver the vehicle
															PRINT_NORMAL_OBJECTIVE("MC_HD_VEH")
														ENDIF
														
													ELSE
														
														IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
															PRINT_NORMAL_OBJECTIVE("MC_HD_VEHS_V")
														ELSE
															PRINT_NORMAL_OBJECTIVE("MC_HD_VEH_V")
														ENDIF
														
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_DELIVER_VEH
								IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_VEHICLE)
									IF NOT ( IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF) OR IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF) OR (iDeliveryVehForcingOut != -1) )
										IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
											//Differentiate between dropping off at a locate or at a vehicle:
											IF (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] != ciFMMC_DROP_OFF_TYPE_VEHICLE)
											OR (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOff_Vehicle[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] = -1)
												
												IF bPlural
													PRINT_NORMAL_OBJECTIVE("MC_D_VEHS") //This text doesn't exist, but the player can't have more than one car
												ELSE
													//Deliver the vehicle
													PRINT_NORMAL_OBJECTIVE("MC_D_VEH")
												ENDIF
												
											ELSE
												
												IF bPlural
													PRINT_NORMAL_OBJECTIVE("MC_D_VEHS_V")
												ELSE
													PRINT_NORMAL_OBJECTIVE("MC_D_VEH_V")
												ENDIF
												
											ENDIF
										ENDIF								
									ELSE
										IF Is_MP_Objective_Text_On_Display()
											Clear_Any_Objective_Text()
										ENDIF
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_COLLECT_OBJ
							
								IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_OBJECT)
									IF MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] 
										IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
											PRINT_NORMAL_OBJECTIVE("MC_C_OBJS")
										ELSE
											PRINT_NORMAL_OBJECTIVE("MC_C_OBJ")
										ENDIF
									ELSE
										IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 0
											IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
												//Differentiate between dropping off at a locate or at a vehicle:
												IF (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] != ciFMMC_DROP_OFF_TYPE_VEHICLE)
												OR (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOff_Vehicle[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] = -1)
													IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
														PRINT_NORMAL_OBJECTIVE("MC_HD_OBJS")
													ELSE
														PRINT_NORMAL_OBJECTIVE("MC_HD_OBJ")
													ENDIF
												ELSE
													IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
														PRINT_NORMAL_OBJECTIVE("MC_HD_OBJS_V")
													ELSE
														PRINT_NORMAL_OBJECTIVE("MC_HD_OBJ_V")
													ENDIF
												ENDIF
												
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_DELIVER_OBJ
							
								IF iPartEscorting !=-1
									IF IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER) AND MC_playerBD[iPartEscorting].iObjCarryCount > 1
										bPlural = TRUE
									ENDIF
								ENDIF
								IF MC_playerBD[iObjTxtPartToUse].iObjCarryCount > 1
									bPlural = TRUE
								ENDIF
							
								IF NOT (IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
										OR IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF))
								AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
									//Differentiate between dropping off at a locate or at a vehicle:
									IF (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] != ciFMMC_DROP_OFF_TYPE_VEHICLE)
									OR (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOff_Vehicle[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] = -1)
										IF bPlural
											PRINT_NORMAL_OBJECTIVE("MC_D_OBJS")
										ELSE
											PRINT_NORMAL_OBJECTIVE("MC_D_OBJ")
										ENDIF
									ELSE
										IF bPlural
											PRINT_NORMAL_OBJECTIVE("MC_D_OBJS_V")
										ELSE
											PRINT_NORMAL_OBJECTIVE("MC_D_OBJ_V")
										ENDIF
									ENDIF
										
								ELSE
									IF Is_MP_Objective_Text_On_Display()
										Clear_Any_Objective_Text()
									ENDIF		
								ENDIF

							BREAK
							
							CASE CLIENT_MISSION_STAGE_KILL_PLAYERS
								PRINTLN("CLIENT_MISSION_STAGE_KILL_PLAYERS - here 2")
								PRINT_COLOURED_OBJECTIVE("MC_K_ALLT",HUD_COLOUR_RED)
							BREAK
							
							CASE CLIENT_MISSION_STAGE_KILL_TEAM0
								IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_MY_TEAM_NAME_IS_CREW)
									PRINT_COLOURED_TEAM_OBJECTIVE("MC_K_T1",0)
								ELSE
									PRINT_COLOURED_TEAM_OBJECTIVE("MC_K_C1",0)
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_KILL_TEAM1
								IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_MY_TEAM_NAME_IS_CREW)
									PRINT_COLOURED_TEAM_OBJECTIVE("MC_K_T1",1)
								ELSE
									PRINT_COLOURED_TEAM_OBJECTIVE("MC_K_C1",1)
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_KILL_TEAM2
								IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_MY_TEAM_NAME_IS_CREW)
									PRINT_COLOURED_TEAM_OBJECTIVE("MC_K_T1",2)
								ELSE
									PRINT_COLOURED_TEAM_OBJECTIVE("MC_K_C1",2)
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_KILL_TEAM3
								IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_MY_TEAM_NAME_IS_CREW)
									PRINT_COLOURED_TEAM_OBJECTIVE("MC_K_T1",3)
								ELSE
									PRINT_COLOURED_TEAM_OBJECTIVE("MC_K_C1",3)
								ENDIF
							BREAK
						
							
							CASE CLIENT_MISSION_STAGE_GOTO_ANY_PLAYER
								PRINT_COLOURED_OBJECTIVE("MC_GO_ALLT",HUD_COLOUR_RED)
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GOTO_TEAM0
							CASE CLIENT_MISSION_STAGE_GOTO_TEAM1
							CASE CLIENT_MISSION_STAGE_GOTO_TEAM2
							CASE CLIENT_MISSION_STAGE_GOTO_TEAM3
								PRINT_COLOURED_TEAM_OBJECTIVE("MC_GO_TNUM", iclientstage - CLIENT_MISSION_STAGE_GOTO_TEAM0)
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GET_MASK
							
								IF NOT bIsSCTV
									IF NOT IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WEARING_MASK)
										IF IS_BIT_SET(iLocalBoolCheck4,LBOOL4_OWN_MASK)
											PRINT_NORMAL_OBJECTIVE("MC_GET_MASK")
										ELSE
											IF NOT IS_BIT_SET(iLocalBoolCheck4,LBOOL4_IN_MASK_SHOP)
												PRINT_NORMAL_OBJECTIVE("MC_GO_MASK")
											ELSE
												PRINT_NORMAL_OBJECTIVE("MC_BUY_MASK")
											ENDIF
										ENDIF
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_WAIT_MASK")
									ENDIF
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_WAIT_MASK")
								ENDIF
									
							BREAK
							
							CASE CLIENT_MISSION_STAGE_LOOT_THRESHOLD
								
								IF MC_serverBD.iCurrentPlayerRule[MC_playerBD[iObjTxtPartToUse].iTeam] > -1
								AND MC_serverBD.iCurrentPlayerRule[MC_playerBD[iObjTxtPartToUse].iTeam] < FMMC_MAX_RULES
									IF IS_BIT_SET(MC_serverBD.iPlayerRuleBitset[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iObjTxtPartToUse].iTeam]][MC_playerBD[iObjTxtPartToUse].iTeam], ciPLAYER_RULE_BITSET_UseBagCapacityForLootThreshold)
									AND ROUND((MC_playerBD[iObjTxtPartToUse].sLootBag.fCurrentBagCapacity / BAG_CAPACITY__GET_MAX_CAPACITY()) * 100) >= MC_serverBD.iPlayerRuleLimit[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iObjTxtPartToUse].iTeam]][MC_playerBD[iObjTxtPartToUse].iTeam]
										IF IS_OBJECTIVE_TEXT_READY_TO_UPDATE(ciOBJECTIVE_TEXT_DELAY_PLAYER)
											PRINT_NORMAL_OBJECTIVE("MC_LOOT_W")
										ENDIF
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_LOOT")
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_PHOTO_LOC
							
								IF MC_playerBD[iObjTxtPartToUse].iCurrentLoc = -1
									IF MC_serverBD.iNumLocHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										PRINT_NORMAL_OBJECTIVE("MC_GOTO_LOCS")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_GOTO_LOC")
									ENDIF
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_PH_LOC")
								ENDIF
								
							BREAK
							CASE CLIENT_MISSION_STAGE_PHOTO_PED
							
								IF MC_playerBD[iObjTxtPartToUse].iPedNear = -1
									IF (MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES) //Just to check when we're calling this after having completed the objective, but before cleaning up
									AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iPhotoCanBeDead,MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam])
										IF MC_serverBD.iNumDeadPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] >= 1
											IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
												PRINT_NORMAL_OBJECTIVE("MC_GO_DPEDS")
											ELSE
												PRINT_NORMAL_OBJECTIVE("MC_GO_DPED")
											ENDIF
										ELSE								
											IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
												PRINT_NORMAL_OBJECTIVE("MC_K_PEDS")
											ELSE
												PRINT_NORMAL_OBJECTIVE("MC_K_PED")
											ENDIF
										ENDIF
									ELSE
										IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
											PRINT_NORMAL_OBJECTIVE("MC_GOTO_PEDS")
										ELSE
											PRINT_NORMAL_OBJECTIVE("MC_GOTO_PED")
										ENDIF
									ENDIF
								ELSE
									IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[MC_playerBD[iObjTxtPartToUse].iteam], MC_playerBD[iObjTxtPartToUse].iPedNear)
										PRINT_NORMAL_OBJECTIVE("MC_PH_PED")
									ELSE
										IF IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[MC_playerBD[iObjTxtPartToUse].iPedNear]) 
											PRINT_NORMAL_OBJECTIVE("MC_PHD_PED")
										ELSE
											PRINT_NORMAL_OBJECTIVE("MC_K_PED")
										ENDIF
									ENDIF
										
								ENDIF
								
							BREAK
							CASE CLIENT_MISSION_STAGE_PHOTO_VEH
							
								IF MC_playerBD[iObjTxtPartToUse].iVehNear = -1
									IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										PRINT_NORMAL_OBJECTIVE("MC_GO_VEHSP")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_GO_VEHP")
									ENDIF
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_PH_VEH")
								ENDIF

								
							BREAK
							CASE CLIENT_MISSION_STAGE_PHOTO_OBJ
								
								IF MC_playerBD[iObjTxtPartToUse].iObjNear = -1
									IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										PRINT_NORMAL_OBJECTIVE("MC_GOTO_OBJS")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_GOTO_OBJ")
									ENDIF
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_PH_OBJ")
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_HACK_COMP
								
								IF MC_playerBD[iObjTxtPartToUse].iObjHacking = -1
								AND NOT IS_BIT_SET(iLocalBoolCheck12, LBOOL12_SERVER_STILL_THINKS_IM_HACKING)
									
									IF ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1)
										AND (MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumobjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]))
									OR ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] = 1)
										AND IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_IS_HACKING + MC_playerBD[iObjTxtPartToUse].iTeam))
										
										IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
											PRINT_NORMAL_OBJECTIVE("MC_GOTO_COMPS")
										ELSE
											PRINT_NORMAL_OBJECTIVE("MC_GOTO_COMP")
										ENDIF
										
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_HHACK_COMP")
									ENDIF
								ELSE
									IF MC_playerBD[iObjTxtPartToUse].iObjHacking > -1
										PRINT_NORMAL_OBJECTIVE("MC_HACK_COMP")
									ELSE
										PRINTLN("Not doing PRINT_NORMAL_OBJECTIVE because iObjHacking is -1")
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_INTERACT_WITH
								PRINT_NORMAL_OBJECTIVE("MC_INTRCT")
							BREAK		
							
							CASE CLIENT_MISSION_STAGE_ENTER_SAFE_COMBINATION
								PRINT_NORMAL_OBJECTIVE("MC_SAFE_CODE")
							BREAK
							
							CASE CLIENT_MISSION_STAGE_POINTS_THRESHOLD
								PRINT_NORMAL_OBJECTIVE("FXR_OT_PTS")
							BREAK
								
							CASE CLIENT_MISSION_STAGE_UNDERWATER_WELDING
								IF IS_LOCAL_PLAYER_ABLE_TO_DO_LEADER_RESTRICTED_INTERACTION()
									PRINT_NORMAL_OBJECTIVE("MC_WELDING")
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_WELDINGW")
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GLASS_CUTTING
								IF MC_playerBD[iObjTxtPartToUse].iObjHacking = -1
								AND IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_IS_HACKING + MC_playerBD[iObjTxtPartToUse].iTeam)
									PRINT_NORMAL_OBJECTIVE("MC_CUTGLASSW")
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_CUTGLASS")
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_INTERROGATION
								IF IS_LOCAL_PLAYER_ABLE_TO_DO_LEADER_RESTRICTED_INTERACTION()
									PRINT_NORMAL_OBJECTIVE("MC_INTERRO_A")
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_INTERRO_B")
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_HACKING
							
								IF ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1)
										AND (MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumobjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]))
									OR ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] = 1)
										AND IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_IS_HACKING + MC_playerBD[iObjTxtPartToUse].iTeam))
										
										PRINT_NORMAL_OBJECTIVE("MC_TMHCK_PROT")
										PRINTLN("Printing 'watch your team do the hack' objective text (DEFAULT)")
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_HCKING")
								ENDIF
										
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_INSERT_FUSE
								IF iMinigameObjPreReqBlockerBS != 0 //We haven't picked up the final fuse
									PRINT_NORMAL_OBJECTIVE("MC_IF_P")
								ELSE
									IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam] > 1
										PRINT_NORMAL_OBJECTIVE("MC_IF_SP")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_IF_S")
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_BEAMHACK_VEH
								IF IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_BEAMHACK_IN_VEH_VALID)
									PRINT_NORMAL_OBJECTIVE("MC_BMVH")
								ELIF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_STARTED_BEAM_HACK)
									PRINT_NORMAL_OBJECTIVE("MC_BMVC")
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_BMVW")
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_HACK_FINGERPRINT_KEYPAD
							
								IF ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1)
								AND (MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumobjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]))
								OR ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] = 1)
								AND IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_IS_HACKING + MC_playerBD[iObjTxtPartToUse].iTeam))
								AND fkhaFingerprintKeypadHackAnimState != FKHA_STATE_LOOP_PRE_EXIT		
									PRINT_NORMAL_OBJECTIVE("MC_TMHCK_PROT")
									PRINTLN("[FingerprintKeypadHack] Printing 'watch your team do the hack' objective text (DEFAULT)")
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_HCKING")
								ENDIF
										
							BREAK
							
							CASE CLIENT_MISSION_STAGE_HACK_DRILL
							
								IF MC_playerBD[iObjTxtPartToUse].iObjHacking = -1
									IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										PRINT_NORMAL_OBJECTIVE("MC_GO_DRILLS")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_GO_DRILL")
									ENDIF
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_DRILL")
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_PHONE_BULLSHARK
							CASE CLIENT_MISSION_STAGE_PHONE_AIRSTRIKE
								
								//Default phone rule objective text goes here
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_SCRIPTED_CUTSCENE
								
								IF IS_BIT_SET( iLocalBoolCheck8, LBOOL8_skipped_to_mid_mission_cutscene_stage_5 )
									PRINT_NORMAL_OBJECTIVE( "MC_SKIP_TXT" )
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_HOLDING_RULE
								IF Is_MP_Objective_Text_On_Display()
									PRINTLN("[JS] [GDTEXT] - CLIENT_MISSION_STAGE_HOLDING_RULE - No custom text set so clearing.")
									Clear_Any_Objective_Text()
								ENDIF	
							BREAK
							
							CASE CLIENT_MISSION_STAGE_RESULTS
								#IF IS_DEBUG_BUILD
								IF bDebugPrintCustomText
									PRINTLN("[JS] [GDTEXT] - CLIENT_MISSION_STAGE_RESULTS - NORMAL")
								ENDIF
								#ENDIF
								Clear_Any_Objective_Text()
							BREAK
							
						ENDSWITCH
					ENDIF
					
					PROCESS_COMPLETED_OBJECTIVE_TEXT_DELAY_TIMERS()
					
				ELSE
					IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iObjTxtPartToUse].iteam)
						IF Has_Any_MP_Objective_Text_Been_Received()
							Clear_Any_Objective_Text()
						ENDIF
					ENDIF
					
					PRINTLN("[RCC MISSION] [MB] Clear_Any_Objective_Text() 2")
				ENDIF				
			ELSE
				IF NOT PROCESS_EXTERNAL_OBJECTIVE_TEXT_OVERRIDES(bObjectiveDisp, bCustomObjective)
					//Objective blocker is set:
					REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - Objective blocker" #ENDIF )
					
					IF Has_Any_MP_Objective_Text_Been_Received()
						Clear_Any_Objective_Text()
					ENDIF
					PRINTLN("[RCC MISSION] [MB] Clear_Any_Objective_Text() 3")
				ENDIF
			ENDIF
		ELSE
			
			IF SHOULD_SHOW_BOUNDS_OBJECTIVE_TEXT()
				// "MC_LTS_OBJ_C" is just ~a~
				IF PRINT_OBJECTIVE_WITH_CUSTOM_STRING("MC_LTS_OBJ_C", tlOutOfBoundsObjectiveText)
				
					#IF IS_DEBUG_BUILD
					IF bDebugPrintCustomText
						PRINTLN("[RCC MISSION][Bounds][RB ", iBoundsToShowObjectiveText, "] PROCESS_OBJECTIVE_TEXT - Objective text overriden by bounds ", iBoundsToShowObjectiveText, " to display ", tlOutOfBoundsObjectiveText)
					ENDIF
					#ENDIF
					
					bobjectivedisp = TRUE
					bcustomobjective = TRUE
				ENDIF
			ENDIF
			
			//I'm on the wrong side of the mission bounds:
			REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_TEXT - wrong side of the mission bounds" #ENDIF )
		ENDIF
		
	ELIF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_DIVE_SCORE_SCREEN_FX_ACTIVE)
		PROCESS_SHARD_TEXT()
		
	ELSE // IF we've finished OR PBBOOL_FINISHED is set OR PBBOOL2_PLAYER_OUT_BOUNDS_FAIL is true OR a cutscene is playing
	// So here would be all the instances where we want to clear objective text
	
		IF Has_Any_MP_Objective_Text_Been_Received()
			Clear_Any_Objective_Text()
		ENDIF
		PRINTLN("[RCC MISSION] [MB] Clear_Any_Objective_Text() 4")
	
		//==========================
		
		CLEANUP_OBJECTIVE_BLIPS()
	ENDIF
	
	IF MC_playerBD[iPartToUse].iteam > -1
		IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
		OR IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		OR HAS_TEAM_FINISHED(MC_playerBD[iPartToUse].iteam)
		OR HAS_TEAM_FAILED(MC_playerBD[iPartToUse].iteam)
		OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED )
		OR SHOULD_HIDE_THE_HUD_THIS_FRAME()
			IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_TEXT)
			OR IS_BIG_MESSAGE_ALREADY_REQUESTED(BIG_MESSAGE_GENERIC_TEXT)
				TEXT_LABEL_15 tl15 = GET_BIG_MESSAGE_TEXT_BEING_DRAWN()
				STRING sStringToCheck
				sStringToCheck = TEXT_LABEL_TO_STRING(tl15)
				
				PRINTLN("[RCC MISSION][shard] sLastShardText: ", sLastShardText)
				PRINTLN("[RCC MISSION][shard] sStringToCheck: ", sStringToCheck)
				IF NOT IS_STRING_NULL_OR_EMPTY(sLastShardText)
				AND NOT IS_STRING_NULL_OR_EMPTY(sStringToCheck)
					IF ARE_STRINGS_EQUAL(sLastShardText, sStringToCheck)
						CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_TEXT)
						PRINTLN("[RCC MISSION][shard] Clearing Shard Text as the mission is over. sLastShardText: ", sLastShardText)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
			
	IF IS_CELLPHONE_CAMERA_IN_USE()
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("Lowrider_Help_25")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_SCORE_H1")				
			CLEAR_HELP(TRUE)				
		ENDIF
	ENDIF
	
	iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Leaderboards -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// #####  MOST OF THIS NEEDS SPLIT UP - This header should only draw. Players should sort the leaderboard and set the data to be drawn here
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_APPEAR_CROSSED_OUT(PLAYER_INDEX playerId, INT iParticipant)

	IF NOT IS_NET_PLAYER_OK(playerId)
		#IF IS_DEBUG_BUILD
			IF bOutputDebugspam
				PRINTLN("[JS] SHOULD_APPEAR_CROSSED_OUT - NOT IS_NET_PLAYER_OK(playerId)")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF iParticipant > - 1
	AND iParticipant < MAX_NUM_MC_PLAYERS
		IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet2, PBBOOL2_PLAYER_OUT_BOUNDS_FAIL)
			#IF IS_DEBUG_BUILD
				IF bOutputDebugspam
					PRINTLN("[JS] SHOULD_APPEAR_CROSSED_OUT - IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet2, PBBOOL2_PLAYER_OUT_BOUNDS_FAIL")
				ENDIF
			#ENDIF
			RETURN TRUE
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
			#IF IS_DEBUG_BUILD
				IF bOutputDebugspam
					PRINTLN("[JS] SHOULD_APPEAR_CROSSED_OUT - IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)")
				ENDIF
			#ENDIF
			RETURN TRUE
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_ANY_SPECTATOR)
			#IF IS_DEBUG_BUILD
				IF bOutputDebugspam
					PRINTLN("[JS] SHOULD_APPEAR_CROSSED_OUT - IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_ANY_SPECTATOR)")
				ENDIF
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC DO_THE_MICS()
	INT i
	INT iIcon
	PLAYER_INDEX PlayerId
	BOOL bCrossedOut
	PRINTLN("[PLAYER_LOOP] - DO_THE_MICS")
	REPEAT NUM_NETWORK_PLAYERS i
		PlayerId = INT_TO_PLAYERINDEX(i)
		IF PlayerId <> INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(playerId, FALSE)
				IF dpadVars.sDpadMics[i].iRow <> -1
					IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerId)
					AND NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(playerId))				
						bCrossedOut = SHOULD_APPEAR_CROSSED_OUT(playerId, NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerId)))
						iIcon = GET_DPAD_PLAYER_VOICE_ICON(dpadVars.sDpadMics[i].iVoiceChatState, bCrossedOut)
						SET_PLAYER_MIC(scaleformDpadMovie, dpadVars.sDpadMics[i].iRow, iIcon, GlobalplayerBD_FM[i].scoreData.iRank) 	// SET_ICON
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


PROC POPULATE_DPAD()

	INT i
	INT irow, iPlayer, iRank
	INT iTeamSlotCounter[FMMC_MAX_TEAMS]
	INT iTeam1Count,iTeam2Count,iTeam3Count
	STRING strHeadshotTxd = ""
	TEXT_LABEL_15 tlCrewTag
	BOOL bHeadshot,bShowKills
	PEDHEADSHOT_ID pedHeadshot
	PLAYER_INDEX playerId
	INT iRowIndexToDisplay = -1
	
	REPEAT FMMC_MAX_TEAMS i
		iTeamSlotCounter[i] = 0
	ENDREPEAT
	
	LBD_SUB_MODE lbdSubMode

	GET_MISSION_TEAM_COUNTS(g_MissionControllerserverBD_LB, MC_serverBD, iTeam1Count, iTeam2Count, iTeam3Count)		//	, playerBDPassed, iTeam4Count
	
	BOOL bIsDpadLbd = TRUE

	IF MC_serverBD.iNumberOfTeams = 1 
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_ShowHeadshotsOnDpadDownLeaderboard)
		bHeadshot = TRUE
	ENDIF
	
	EMPTY_DPAD_MENU_IF_REQUIRED(scaleformDpadMovie)
	g_i_NumDpadLbdPlayers = 0
	
	INT iPartForDpad
	PLAYER_INDEX playerToUseForDPad
	IF MC_PlayerBD[iLocalPart].iTeam = -1
		iPartForDpad = iPartToUse
		playerToUseForDPad = PlayerToUse
	ELSE
		iPartForDpad = iLocalPart
		playerToUseForDPad = LocalPlayer
	ENDIF
	
	PRINTLN("[PLAYER_LOOP] - POPULATE_DPAD")
	REPEAT NUM_NETWORK_PLAYERS i
		IF SHOULD_PLAYER_BE_DISPLAYED_ON_MISSION_DPAD_DOWN_MENU(i, g_MissionControllerserverBD_LB)	//	MC_playerBD

			dpadVars.tl63LeaderboardPlayerNames = MC_serverBD_2.tParticipantNames[g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant]
			playerId = g_MissionControllerserverBD_LB.sleaderboard[i].playerID
			iPlayer = NATIVE_TO_INT(playerId)
			// Rank
			iRank = GlobalplayerBD_FM[iPlayer].scoreData.iRank
			
			dpadVars.mpIconActive = ICON_EMPTY
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciHARD_TARGET_ENABLE_MODE)
			AND IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerId)].iMissionModeBit, ciADVERSARY_IS_A_HARD_TARGET)
				dpadVars.mpIconActive = DPAD_DOWN_TARGET
			ELSE
				dpadVars.mpIconActive = ICON_EMPTY
			ENDIF
			
			IF SHOULD_APPEAR_CROSSED_OUT(playerId, NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerId)))
				SET_BIT(dpadVars.iDpadBitSet, ciDPAD_PLAYER_DEAD)
			ELSE
				CLEAR_BIT(dpadVars.iDpadBitSet, ciDPAD_PLAYER_DEAD)
			ENDIF
			
			IF g_MissionControllerserverBD_LB.sleaderboard[i].iTeam !=-1

				iRow = GET_ROW_TO_DRAW_PLAYERS(g_MissionControllerserverBD_LB.sleaderboard[i].iTeam,MC_serverBD.iNumActiveTeams,iTeamSlotCounter[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam],
												iTeam1Count,iTeam2Count,iTeam3Count,0,0,0,0,MC_serverBD.iWinningTeam,MC_serverBD.iSecondTeam,MC_serverBD.iThirdTeam,0,0,0,0,MC_serverBD.iLosingteam,bIsDpadLbd, FALSE)		

				dpadVars.iTeamRow = GET_TEAM_BARS_ROW(g_MissionControllerserverBD_LB.sleaderboard[i].iTeam,MC_serverBD.iNumActiveTeams,iTeam1Count,iTeam2Count,iTeam3Count,0,0,0,0,0,
														MC_serverBD.iWinningTeam,MC_serverBD.iSecondTeam,MC_serverBD.iThirdTeam,0,0,0,0,MC_serverBD.iLosingteam,bIsDpadLbd)
				
				// Row colours
				IF (DOES_TEAM_LIKE_TEAM(MC_playerBD[iPartForDpad].iteam, g_MissionControllerserverBD_LB.sleaderboard[i].iTeam)
				OR (playerToUseForDPad = playerId AND MC_PlayerBD[iPartForDpad].iTeam = -1))
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciFRIENDLY_TEAMS_SAME_COLOUR)
						PRINTLN("[3199690] dpadVars.hudColour 1 ")
						dpadVars.hudColour = HUD_COLOUR_FRIENDLY
					ELSE
						PRINTLN("[3199690] dpadVars.hudColour 2 ")
						dpadVars.hudColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(g_MissionControllerserverBD_LB.sleaderboard[i].iTeam, playerToUseForDPad, TRUE)
					ENDIF
				ELSE
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciFRIENDLY_TEAMS_SAME_COLOUR)
						PRINTLN("[3199690] dpadVars.hudColour 3 ")
						dpadVars.hudColour = HUD_COLOUR_NET_PLAYER1
					ELSE
						PRINTLN("[3199690] dpadVars.hudColour 4 ")
						dpadVars.hudColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(g_MissionControllerserverBD_LB.sleaderboard[i].iTeam, playerToUseForDPad)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(MC_serverBD.iOptionsMenuBitSet,ciNO_POINTS_HUD)
				AND NOT DPAD_FORCE_SCORES()
					dpadVars.iScore = -1
				ELSE

					IF bShowKills
						dpadVars.iScore = g_MissionControllerserverBD_LB.sleaderboard[i].iKills
					ELSE
						dpadVars.iScore = g_MissionControllerserverBD_LB.sleaderboard[i].iPlayerScore
					ENDIF
				ENDIF	
				
				IF iRow = dpadVars.iTeamRow
					iRowIndexToDisplay = GET_TEAM_FINISH_POSITION(g_MissionControllerserverBD_LB.sleaderboard[i].iTeam)
				ELSE
					iRowIndexToDisplay = -2	//	Don't display the team rank for any rows except the top row for that team
				ENDIF
				
				eHeadshotOrPositionNumber headshotOrPosition = DISPLAY_POSITION_NUMBER
				IF bHeadshot
					// Attempt to get player headshot
					pedHeadshot = Get_HeadshotID_For_Player(playerId)
					strHeadshotTxd = ""
					IF pedHeadshot <> NULL
						
						strHeadshotTxd = GET_PEDHEADSHOT_TXD_STRING(pedHeadshot)
						PRINTLN("[RCC MISSION] [CS_DPAD] OK strHeadshotTxd = ", strHeadshotTxd)
					ELSE
						PRINTLN("[RCC MISSION] [CS_DPAD] NULL strHeadshotTxd = ", strHeadshotTxd)
					ENDIF
					
					headshotOrPosition = DISPLAY_HEADSHOT
					iRowIndexToDisplay = -1
				ENDIF

				//POPULATE_SCALEFORM_DPAD_ROWS(GET_LEADERBOARD_SUB_MODE(),dpadVars,scaleformDpadMovie,iRow,iTeamRank,GET_CREW_TAG_DPAD_LBD(iPlayer), strHeadshotTxd)
				
				PRINTLN("[RCC MISSION] [CS_DPAD] FM_Mission_Controler about to call POPULATE_NEW_FREEMODE_DPAD_ROWS with row = ", iRow, " player name = ", dpadVars.tl63LeaderboardPlayerNames, 
						" iParticipant = ", g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant, " iPlayer = ", iPlayer, " iRowIndexToDisplay = ", iRowIndexToDisplay, 
						" MC_serverBD.iNumActiveTeams = ", MC_serverBD.iNumActiveTeams)
				
				TEXT_LABEL_63 tlJobRole
				BOOL bLiteral

				IF MC_serverBD.iNumberOfTeams > 1
					IF g_FMMC_STRUCT.iTeamNames[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam] = ciTEAM_NAME_MAX
					AND NOT IS_BIT_SET(g_iTeamNameBitSetPlayerName, g_MissionControllerserverBD_LB.sleaderboard[i].iTeam)
						bLiteral = FALSE
					ELSE
						bLiteral = TRUE
					ENDIF
					tlJobRole = g_sMission_TeamName[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam]
				ENDIF
				
				IF ARE_STRINGS_EQUAL(dpadVars.tl63LeaderboardPlayerNames, tlJobRole)
					tlJobRole = ""
					IF HAS_TEAM_GOT_CREATOR_NAME(g_MissionControllerserverBD_LB.sleaderboard[i].iTeam)
						bLiteral = FALSE
						tlJobRole = GET_CREATOR_NAME_STRING(g_MissionControllerserverBD_LB.sleaderboard[i].iTeam)
					ENDIF
				ENDIF
				
				IF NOT DOES_TEXT_LABEL_EXIST(tlJobRole)
					bLiteral = TRUE					
				ENDIF
				
				INT iMissionSplit = 0

				IF iMissionSplit != 0
				OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciENABLE_SPLIT_TIMES)
					lbdSubMode = SUB_MISSION_RACE
					POPULATE_NEW_FREEMODE_DPAD_ROWS(lbdSubMode, scaleformDpadMovie, dpadVars, iRow, tlCrewTag, strHeadshotTxd, iRank, iMissionSplit, dpadVars.hudColour, headshotOrPosition, iRowIndexToDisplay, 
														tlJobRole, bLiteral, DEFAULT, DEFAULT, NATIVE_TO_INT(playerId))
				
				ELSE
					lbdSubMode = GET_LEADERBOARD_SUB_MODE()

					POPULATE_NEW_FREEMODE_DPAD_ROWS(lbdSubMode, scaleformDpadMovie, dpadVars, iRow, tlCrewTag, strHeadshotTxd, iRank, dpadVars.iScore, dpadVars.hudColour, headshotOrPosition, iRowIndexToDisplay, 
														tlJobRole, bLiteral, DEFAULT, DEFAULT, NATIVE_TO_INT(playerId))
				ENDIF
				g_i_NumDpadLbdPlayers ++
				
				IF playerId = LocalPlayer
					dpadVars.iPlayersRow = (iTeamSlotCounter[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam] + dpadVars.iTeamRow)
				ENDIF
				
				// Store out mics row
				dpadVars.sDpadMics[iPlayer].iRow = iRow
				
				iTeamSlotCounter[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam]++
			ENDIF
		ENDIF
	ENDREPEAT
	
	RESET_DPAD_REFRESH()
	CLEAR_BIT(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)

ENDPROC

PROC DRAW_DPAD_LEADERBOARD()
	
	INT iTotalNumPlayers = MC_serverBD.iNumberOfLBPlayers[0]+MC_serverBD.iNumberOfLBPlayers[1]+MC_serverBD.iNumberOfLBPlayers[2]+  MC_serverBD.iNumberOfLBPlayers[3] 
	
	LBD_SUB_MODE lbdSubMode = GET_LEADERBOARD_SUB_MODE()
	
	REFRESH_DPAD_WHEN_DATA_CHANGES(MC_serverBD.iHostRefreshDpadValue, iClientRefreshDpadValue)

	IF MC_serverBD.iNumActiveTeams > 0
		IF SHOULD_DPAD_LBD_DISPLAY(scaleformDpadMovie, lbdSubMode,dpadVars, iTotalNumPlayers, DEFAULT, SWAP_SPECTATOR_LIST_FOR_DPAD())
			SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
	
			SWITCH dpadVars.iDrawProgress
			
				CASE DPAD_INIT
					
					RESET_DPAD()
					
					IF NOT IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)		
						HIGHLIGHT_LOCAL_PLAYER(dpadVars, scaleformDpadMovie)
						DISPLAY_VIEW(scaleformDpadMovie)			// DISPLAY_VIEW
						SET_BIT(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
					ENDIF
					
					// Draw the mics
					DO_THE_MICS()													// SET_ICON
					
					REQUEST_ADDITIONAL_TEXT("FMMC", ODDJOB_TEXT_SLOT)
					IF HAS_THIS_ADDITIONAL_TEXT_LOADED("FMMC", ODDJOB_TEXT_SLOT)
						dpadVars.iDrawProgress = DPAD_POPULATE
					ENDIF
					
					IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_ROW_HIGHLIGHT)
						CLEAR_BIT(dpadVars.iDpadBitSet, ciDPAD_ROW_HIGHLIGHT)
					ENDIF
				BREAK
			
				CASE DPAD_POPULATE

					POPULATE_DPAD()													// SET_DATA_SLOT
			
					dpadVars.iDrawProgress = DPAD_DRAW
				BREAK
				
				CASE DPAD_DRAW
				
					IF SHOULD_DPAD_REFRESH()
						RESET_DPAD()
						POPULATE_DPAD()												// SET_DATA_SLOT
					ENDIF

				
					IF NOT IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
						DISPLAY_VIEW(scaleformDpadMovie)			// DISPLAY_VIEW
						SET_BIT(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
					ENDIF
					
					HIGHLIGHT_LOCAL_PLAYER(dpadVars, scaleformDpadMovie)
					
					IF MAINTAIN_DPAD_VOICE_CHAT(dpadVars)
						DO_THE_MICS()												// SET_ICON
					ENDIF		
				BREAK
			ENDSWITCH
			
			IF HAS_SCALEFORM_MOVIE_LOADED(scaleformDpadMovie)
				DRAW_SCALEFORM_MOVIE(scaleformDpadMovie, SDPAD_X, SDPAD_Y, SDPAD_W, SDPAD_H, 255, 255, 255, 255)
			ENDIF
			
		ENDIF
	ENDIF
	
	RESET_SCRIPT_GFX_ALIGN()
ENDPROC

///   (SERVER ONLY: This is basically a copy of SERVER_MAINTAIN_MC_LEADERBOARD_FOR_PARTICIPANT - it sorts the final cumulative Rounds leaderboard)
PROC SERVER_MAINTAIN_ROUNDS_LBD(INT iParticipant, THE_LEADERBOARD_STRUCT& leaderBoard[])

	INT i,itime,itime2,iPlayer
	PLAYER_INDEX PlayerId = INVALID_PLAYER_INDEX()
	THE_LEADERBOARD_STRUCT EmptyLeaderBoardStruct
	THE_LEADERBOARD_STRUCT StoredLeaderBoardStruct
	THE_LEADERBOARD_STRUCT TempLeaderBoardStruct
	BOOL bParticipantRemoved = FALSE
	BOOL bPartActive = FALSE
	BOOL bPlayerOk = FALSE
	INT iTempNumberOfLBPlayers[FMMC_MAX_TEAMS]
	PARTICIPANT_INDEX TempPart

	TempPart = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
	
	IF NETWORK_IS_PARTICIPANT_ACTIVE(TempPart)
		bPartActive = TRUE
		PlayerId = NETWORK_GET_PLAYER_INDEX(TempPart)
		IF IS_NET_PLAYER_OK(PlayerId, FALSE, FALSE)
			bPlayerOk = TRUE
			iPlayer = NATIVE_TO_INT(PlayerId)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_LBD_STOP)
		EXIT
	ENDIF
	
	PRINTLN("[PLAYER_LOOP] - SERVER_MAINTAIN_ROUNDS_LBD 2")
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i 

		IF leaderBoard[i].iParticipant != -1
			IF leaderBoard[i].iTeam !=-1
				iTempNumberOfLBPlayers[leaderBoard[i].iTeam]++
			ELSE
				PRINTLN("[JS] SERVER_MAINTAIN_ROUNDS_LBD - Not adding ", i ," part ", leaderBoard[i].iParticipant, " no team index")
			ENDIF
		ELSE
			PRINTLN("[JS] SERVER_MAINTAIN_ROUNDS_LBD - Not adding ", i ," no part index")
		ENDIF

		IF NOT bParticipantRemoved
			IF g_FMMC_STRUCT.iMissionEndType != ciMISSION_SCORING_TYPE_TIME
				IF (leaderBoard[i].iParticipant = iParticipant)
					IF ((NOT bPartActive) AND (leaderBoard[i].iplayerscore = -1))
					OR bPartActive
					OR IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet,PBBOOL_START_SPECTATOR)
						leaderBoard[i] = EmptyLeaderBoardStruct// clear the current participants data.
						bParticipantRemoved = TRUE// sets that this participant has been removed
					ENDIF
				ENDIF
			ELSE
				IF (leaderBoard[i].iParticipant = iParticipant)
					IF NOT bPartActive 
					AND NOT IS_BIT_SET(leaderBoard[i].iLbdBitSet, ciLBD_BIT_FINISHED)
					OR bPartActive
						leaderBoard[i] = EmptyLeaderBoardStruct// clear the current participants data.
						bParticipantRemoved = TRUE// sets that this participant has been removed
						
						IF NOT bPartActive
							PRINTLN("[JS] SERVER_MAINTAIN_ROUNDS_LBD - Setting bParticipantRemoved for participant ", i, " due to bPartActive")
						ENDIF
						IF NOT IS_BIT_SET(leaderBoard[i].iLbdBitSet, ciLBD_BIT_FINISHED)
							PRINTLN("[JS] SERVER_MAINTAIN_ROUNDS_LBD - Setting bParticipantRemoved for participant ", i, " due to ciLBD_BIT_FINISHED")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE // this is the participant after the previously removed one
			TempLeaderBoardStruct = leaderBoard[i] //backup this participants data
			leaderBoard[i-1] = 	TempLeaderBoardStruct //set the position down one to the current participant
			leaderBoard[i] = EmptyLeaderBoardStruct // clear the current participants old data
		ENDIF
	ENDREPEAT
	
	MC_serverBD.iNumberOfLBPlayers[0] = iTempNumberOfLBPlayers[0]
	MC_serverBD.iNumberOfLBPlayers[1] = iTempNumberOfLBPlayers[1]
	MC_serverBD.iNumberOfLBPlayers[2] = iTempNumberOfLBPlayers[2]
	MC_serverBD.iNumberOfLBPlayers[3] = iTempNumberOfLBPlayers[3]
	PRINTLN("SERVER_MAINTAIN_ROUNDS_LBD - MC_serverBD.iNumberOfLBPlayers[0]: ", MC_serverBD.iNumberOfLBPlayers[0])
	PRINTLN("SERVER_MAINTAIN_ROUNDS_LBD - MC_serverBD.iNumberOfLBPlayers[1]: ", MC_serverBD.iNumberOfLBPlayers[1])
	PRINTLN("SERVER_MAINTAIN_ROUNDS_LBD - MC_serverBD.iNumberOfLBPlayers[2]: ", MC_serverBD.iNumberOfLBPlayers[2])
	PRINTLN("SERVER_MAINTAIN_ROUNDS_LBD - MC_serverBD.iNumberOfLBPlayers[3]: ", MC_serverBD.iNumberOfLBPlayers[3])
	
	INT iCurrentRank = 1
	INT ileaderboardpartscore
	INT irankpartscore
	// add this participant to the leader board
	BOOL bParticipantInserted = FALSE
	PRINTLN("[PLAYER_LOOP] - SERVER_MAINTAIN_ROUNDS_LBD")
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NOT (bParticipantInserted)
			BOOL bGoodToInsert = TRUE
			IF (leaderBoard[i].iplayerscore >=0)
			OR bPartActive
				IF PlayerId <> INVALID_PLAYER_INDEX()
					IF bPlayerOk
					OR IS_BIT_SET(MC_Playerbd[iParticipant].iClientBitSet,PBBOOL_TERMINATED_SCRIPT)
						IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet,PBBOOL_START_SPECTATOR)	
							IF (leaderBoard[i].playerID <> PlayerId) AND (leaderBoard[i].playerID <> INVALID_PLAYER_INDEX())
								IF IS_NET_PLAYER_OK(leaderBoard[i].playerID, FALSE, FALSE)
									IF (leaderBoard[i].iplayerscore >=0)
									OR NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, leaderBoard[i].iParticipant))
										IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_TIME
											IF HAS_NET_TIMER_STARTED(MC_Playerbd[leaderBoard[i].iParticipant].tdMissionTime)
												itime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_Playerbd[leaderBoard[i].iParticipant].tdMissionTime)
											ELSE
												itime = MC_Playerbd[leaderBoard[i].iParticipant].iMissionEndTime
											ENDIF
											IF HAS_NET_TIMER_STARTED(MC_Playerbd[iParticipant].tdMissionTime)
												itime2 = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_Playerbd[iParticipant].tdMissionTime)
											ELSE
												itime2 = MC_Playerbd[iParticipant].iMissionEndTime
											ENDIF
											ileaderboardpartscore = (-1*itime)
											irankpartscore = (-1*itime2)
										ELSE
											ileaderboardpartscore 	= GlobalplayerBD_FM_2[leaderBoard[i].iParticipant].sJobRoundData.iScore
											irankpartscore 			= GlobalplayerBD_FM_2[iParticipant].sJobRoundData.iScore
										ENDIF
										// compare their position with all other parts				
										// Who has most points?
										
										IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, leaderBoard[i].iParticipant))
											PRINTNL()
											PRINTLN("_______________________________________________________________")
											PRINTLN("2536896, iRoundScore           = ", GlobalplayerBD_FM_2[leaderBoard[i].iParticipant].sJobRoundData.iRoundScore, " Player = ", 
													GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(leaderBoard[i].iParticipant))))
											PRINTLN("2536896, iRoundScore           = ", GlobalplayerBD_FM_2[iParticipant].sJobRoundData.iRoundScore, " Player = ", 
													GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))))
											PRINTLN("2536896, ileaderboardpartscore = ", ileaderboardpartscore, " Player = ", 
													GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(leaderBoard[i].iParticipant))))
											PRINTLN("2536896, irankpartscore        = ", irankpartscore, " Player = ", 
													GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))))
											PRINTLN("_______________________________________________________________")
											PRINTNL()
										ENDIF
										
										// Compare rounds won because this is the round leaderboard
										IF GlobalplayerBD_FM_2[leaderBoard[i].iParticipant].sJobRoundData.iRoundScore > GlobalplayerBD_FM_2[iParticipant].sJobRoundData.iRoundScore
											iCurrentRank++
											bGoodToInsert = FALSE
										ELIF GlobalplayerBD_FM_2[leaderBoard[i].iParticipant].sJobRoundData.iRoundScore = GlobalplayerBD_FM_2[iParticipant].sJobRoundData.iRoundScore
											IF ileaderboardpartscore > irankpartscore
												iCurrentRank++
												bGoodToInsert = FALSE
												//PRINTLN("SCORE BETTER SO ADDING TO POSITION for LB PART: ",leaderBoard[i].iParticipant, "SCORE IS", ileaderboardpartscore,"OTHER PART: ",iParticipant," SCORE IS: ",irankpartscore)
											ELIF ileaderboardpartscore = irankpartscore
												//PRINTLN("SCORE EQUAL SO MOVING ON for LB PART: ",leaderBoard[i].iParticipant, "SCORE IS", ileaderboardpartscore,"OTHER PART: ",iParticipant," SCORE IS: ",irankpartscore)
												// score the same - who has most kills?
																								
												IF GlobalplayerBD_FM_2[leaderBoard[i].iParticipant].sJobRoundData.iKills  > GlobalplayerBD_FM_2[iParticipant].sJobRoundData.iKills
													iCurrentRank++
													bGoodToInsert = FALSE
												ELIF GlobalplayerBD_FM_2[leaderBoard[i].iParticipant].sJobRoundData.iKills  = GlobalplayerBD_FM_2[iParticipant].sJobRoundData.iKills
													// this player has same kills - who has least deaths?
													IF GlobalplayerBD_FM_2[leaderBoard[i].iParticipant].sJobRoundData.iDeaths < GlobalplayerBD_FM_2[iParticipant].sJobRoundData.iDeaths
														iCurrentRank++
														bGoodToInsert = FALSE
														//player has same deaths who has most headshots?
													ELIF GlobalplayerBD_FM_2[leaderBoard[i].iParticipant].sJobRoundData.iDeaths = GlobalplayerBD_FM_2[iParticipant].sJobRoundData.iDeaths

														IF GlobalplayerBD_FM_2[leaderBoard[i].iParticipant].sJobRoundData.iRP > GlobalplayerBD_FM_2[iParticipant].sJobRoundData.iRP
															iCurrentRank++
															bGoodToInsert = FALSE
														ELIF GlobalplayerBD_FM_2[leaderBoard[i].iParticipant].sJobRoundData.iRP = GlobalplayerBD_FM_2[iParticipant].sJobRoundData.iRP	
															IF GlobalplayerBD_FM_2[leaderBoard[i].iParticipant].sJobRoundData.iCash > GlobalplayerBD_FM_2[iParticipant].sJobRoundData.iCash 
																iCurrentRank++
																bGoodToInsert = FALSE
															ELIF GlobalplayerBD_FM_2[leaderBoard[i].iParticipant].sJobRoundData.iCash = GlobalplayerBD_FM_2[iParticipant].sJobRoundData.iCash 			
																IF MC_playerBD[leaderBoard[i].iParticipant].iteam > MC_playerBD[leaderBoard[i].iParticipant].iteam
																	iCurrentRank++
																	bGoodToInsert = FALSE
																ELIF MC_playerBD[leaderBoard[i].iParticipant].iteam = MC_playerBD[leaderBoard[i].iParticipant].iteam
																	IF leaderBoard[i].iParticipant < iParticipant
																		iCurrentRank++
																		bGoodToInsert = FALSE
																	ENDIF		
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF	
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							// are we still good to insert this person into the leader board?
							IF (bGoodToInsert)
								
								StoredLeaderBoardStruct = leaderBoard[i] //backup the old participants data that was in this position
								//update the leaderboard for this participant in the new position.
								leaderBoard[i].iParticipant = iParticipant
								leaderBoard[i].playerID 	= PlayerId
								leaderBoard[i].iTeam 		= MC_PlayerBD[iParticipant].iteam
								leaderBoard[i].iTeamScore 	= GET_TEAM_SCORE_FOR_DISPLAY(MC_PlayerBD[iParticipant].iteam)
								
								// Rounds lbd data
								leaderBoard[i].iMissionTime = GlobalplayerBD_FM_2[iParticipant].sJobRoundData.iTime
								
								leaderBoard[i].iDeaths 		= GlobalplayerBD_FM_2[iParticipant].sJobRoundData.iDeaths 
								leaderBoard[i].iAssists		= GlobalplayerBD_FM_2[iParticipant].sJobRoundData.iAssists
								leaderBoard[i].iRoundsScore = GlobalplayerBD_FM_2[iParticipant].sJobRoundData.iRoundScore
								leaderBoard[i].iJobCash 	= GlobalplayerBD_FM_2[iParticipant].sJobRoundData.iCash 	
								leaderBoard[i].iJobXP 		= GlobalplayerBD_FM_2[iParticipant].sJobRoundData.iRP 								

								leaderBoard[i].iPlayerScore	= GlobalplayerBD_FM_2[iParticipant].sJobRoundData.iScore 
								leaderBoard[i].iKills 		= GlobalplayerBD_FM_2[iParticipant].sJobRoundData.iKills 

								leaderBoard[i].fKDRatio 	= MC_playerBD[iParticipant].fKillDeathRatio
								leaderBoard[i].iHeadshots 	= MC_playerBD[iParticipant].iNumHeadshots 
								leaderBoard[i].fDamageDealt	= MC_playerBD[iParticipant].fDamageDealt 
								leaderBoard[i].fDamageTaken	= MC_playerBD[iParticipant].fDamageTaken 
																
								IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet,PBBOOL_FINISHED)
									SET_BIT(leaderBoard[i].iLbdBitSet, ciLBD_BIT_FINISHED)
								ELSE
									CLEAR_BIT(leaderBoard[i].iLbdBitSet, ciLBD_BIT_FINISHED)
								ENDIF
								
								leaderBoard[i].iBadgeRank 	= GlobalplayerBD_FM[iPlayer].scoreData.iRank								

								leaderBoard[i].iBets 		= MC_playerBD[iParticipant].iBet
								
								leaderBoard[i].iRank 		= iCurrentRank
								
								bParticipantInserted 		= TRUE
								
								#IF IS_DEBUG_BUILD
								IF bEnableExtraLeaderboardPrints
									PRINTLN("[LM][LeaderBoardPrintout] - RoundTotal Leaderboard - i  							= ", i)
									PRINTLN("[LM][LeaderBoardPrintout] - RoundTotal Leaderboard - Name 							= ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(i)) )
									PRINTLN("[LM][LeaderBoardPrintout] - RoundTotal Leaderboard - iParticipant  				= ", leaderBoard[i].iParticipant)
									PRINTLN("[LM][LeaderBoardPrintout] - RoundTotal Leaderboard - iTeam 						= ", leaderBoard[i].iTeam)
									PRINTLN("[LM][LeaderBoardPrintout] - RoundTotal Leaderboard - iTeamScore 					= ", leaderBoard[i].iTeamScore)
									PRINTLN("[LM][LeaderBoardPrintout] - RoundTotal Leaderboard - PlayerScore 					= ", leaderBoard[i].iPlayerScore)	
									PRINTLN("[LM][LeaderBoardPrintout] - RoundTotal Leaderboard - RoundsScore 					= ", leaderBoard[i].iRoundsScore)
									PRINTLN("[LM][LeaderBoardPrintout] - RoundTotal Leaderboard - Kills 						= ", leaderBoard[i].iKills)
									PRINTLN("[LM][LeaderBoardPrintout] - RoundTotal Leaderboard - iSpecificAdversaryModeInt		= ", leaderBoard[i].iSpecificAdversaryModeInt)
									PRINTLN("[LM][LeaderBoardPrintout] - RoundTotal Leaderboard - iDeaths 						= ", leaderBoard[i].iDeaths)
									PRINTLN("[LM][LeaderBoardPrintout] - RoundTotal Leaderboard - iAssists						= ", leaderBoard[i].iAssists)
									PRINTLN("[LM][LeaderBoardPrintout] - RoundTotal Leaderboard - iRoundsScore 					= ", leaderBoard[i].iRoundsScore)
									PRINTLN("[LM][LeaderBoardPrintout] - RoundTotal Leaderboard - iJobCash 						= ", leaderBoard[i].iJobCash)
									PRINTLN("[LM][LeaderBoardPrintout] - RoundTotal Leaderboard - iJobXP 						= ", leaderBoard[i].iJobXP)
								ENDIF
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE // this runs on the participants after the one that has just been inserted.
			// shift everyone else after the insertion position in the leaderboard down
			TempLeaderBoardStruct = leaderBoard[i] // backup this participants data
			leaderBoard[i] = StoredLeaderBoardStruct  // put the previous participant into this slot			
			StoredLeaderBoardStruct = TempLeaderBoardStruct //store the original part in this slots data into a struct ready to be put in next slot
		ENDIF
	ENDREPEAT
	
	SET_STOP_PROCESSING_LBD_BIT()
ENDPROC

PROC PROCESS_ROUNDS_LBD()
	INT iPartLoop = 0
	PRINTLN("[PLAYER_LOOP] - PROCESS_ROUNDS_LBD")
	FOR iPartLoop = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
		SERVER_MAINTAIN_ROUNDS_LBD(iPartLoop, g_MissionControllerserverBD_LB.sleaderboard)
	ENDFOR
ENDPROC

FUNC BOOL SHOULD_DRAW_ROUNDS_LBD()
	RETURN IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_ROUNDS_LBD_DO)
ENDFUNC


PROC UNTOGGLE_RENDERPHASES_FOR_LEADERBOARD()

	IF NOT IS_BIT_SET( iLocalBoolCheck13, LBOOL13_UNTOGGLED_RENDERPHASES_BEFORE_LEADERBOARD )
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] We've not untoggled the renderphases yet - untoggle them" )
		TOGGLE_RENDERPHASES( TRUE )
		SET_BIT( iLocalBoolCheck13, LBOOL13_UNTOGGLED_RENDERPHASES_BEFORE_LEADERBOARD )
	ENDIF

ENDPROC

PROC DO_ROUND_LBD_TIMER(TIME_DATATYPE iTimer)

	IF ON_NEW_JOB_VOTE_SCREEN()
		EXIT
	ENDIF

	INT ms = ciROUNDS_LBD_TIMEOUT - ABSI(GET_TIME_DIFFERENCE(iTimer, GET_NETWORK_TIME()))
	INT sec = ROUND(ms * 0.001)

	STRING stTimer = "END_LBD_CONT"
	SPRITE_PLACEMENT aSprite = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
	STRING sProfile = GET_PROFILE_BUTTON_NAME()
	
	STRING sInvalidButtonTime
	STRING sProfileButtonName = GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
	
	SWITCH iTimerProgressLbd
	
		CASE 0		
			IF sec > 0
				PRINTLN("FMMC EOM - DO_ROUND_LBD_TIMER, iTimerProgressLbd = 1 ")					
				iTimerProgressLbd = 1
			ENDIF
		BREAK
		
		CASE 1
			IF LOAD_MENU_ASSETS()
				REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(nextJobStruct.scaleformStruct)
				ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_INT_AS_TIME(sInvalidButtonTime, stTimer, ms, nextJobStruct.scaleformStruct) 	// time
				ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(sProfileButtonName, sProfile, nextJobStruct.scaleformStruct)							// profile

				nextJobStruct.iUpdateTimer = sec
				PRINTLN("FMMC EOM - DO_ROUND_LBD_TIMER, iTimerProgressLbd = 2 ")					
				iTimerProgressLbd = 2
			ENDIF
		BREAK
		
		CASE 2
			IF sec >= 0
				IF nextJobStruct.iUpdateTimer <> sec
					UPDATE_SCALEFORM_INSTRUCTION_BUTTON_SPINNER(siLbdHelp, ms, nextJobStruct.scaleformStruct)
					nextJobStruct.iUpdateTimer = sec
				ENDIF
			ELSE
				PRINTLN("FMMC EOM - DO_ROUND_LBD_TIMER, iTimerProgressLbd = 3 ")	
				iTimerProgressLbd = 1
			ENDIF
			
			RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(siLbdHelp, aSprite, nextJobStruct.scaleformStruct, SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(nextJobStruct.scaleformStruct)) 
		BREAK
		
	ENDSWITCH
ENDPROC

PROC DO_RESTART_HELP_MISSION()
	STRING sRestarthelp
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF SHOULD_SHOW_QUICK_RESTART_OPTION()
			IF IS_BIT_SET(iLocalBoolCheck6,LBOOL6_SHOW_RETRY_HELP)
				DRAW_HUD_OVER_FADE_THIS_FRAME()
			ELSE
				IF g_bAllowJobReplay
					IF NOT IS_STRING_NULL_OR_EMPTY(sRestarthelp)
						DRAW_HUD_OVER_FADE_THIS_FRAME()
						PRINTLN("[RCC MISSION] PRINTING FAIL RETRY HELP") 
						PRINT_HELP(sRestarthelp)
						SET_BIT(iLocalBoolCheck6,LBOOL6_SHOW_RETRY_HELP)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	UNUSED_PARAMETER( sRestarthelp )
ENDPROC

FUNC BOOL DRAW_LBD_THUMB_COUNT()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_HideLikeDislikeDisplay)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC DISPLAY_LEADERBOARD()

	IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_SCTV_LOADDED_FMMC_BLOCK)
	AND (bIsSCTV OR DID_I_JOIN_MISSION_AS_SPECTATOR())
		REQUEST_ADDITIONAL_TEXT("FMMC", ODDJOB_TEXT_SLOT)
		IF HAS_THIS_ADDITIONAL_TEXT_LOADED("FMMC", ODDJOB_TEXT_SLOT)
			SET_BIT(iLocalBoolCheck9, LBOOL9_SCTV_LOADDED_FMMC_BLOCK)
		ELSE
			EXIT	
		ENDIF
	ENDIF
	
	IF MC_serverBD.eCurrentTeamFail[MC_playerBD[iPartToUse].iTeam] = mFail_GAPS_IN_TEAMS
		PRINTLN("[JS] DISPLAY_LEADERBOARD - Gaps in team, no leaderboard")
		EXIT
	ENDIF
	
	INT iSlotCounter
	INT iTeamSlotCounter[FMMC_MAX_TEAMS]
	
	INT irow
	INT iTeamRow = -1
	INT iTeamNamesRow = -1
	
	INT i
	INT iscore,iteamtotal,inumcolumns, iKills, iDeaths
	INT iTeam1Count, iTeam2Count, iTeam3Count,iTeam4Count
	LBD_SUB_MODE submode
	submode = GET_LEADERBOARD_SUB_MODE()
	INT iLocalTeam, iEnemyTeam
	INT iLbdPlayer
	BOOL bDrawTeamRow[FMMC_MAX_TEAMS]
	BOOL bDrawPlayersRow[FMMC_MAX_TEAMS]
	BOOL bDrawPlayersRowUsed[FMMC_MAX_TEAMS]
	BOOL bMultiTeam
	INT iSpectatorNumber
	INT itotalLBPlayers
	STRING sSpectatorName
	
	INT iLbdBitSetRowan
	
	inumcolumns = GET_NUM_LBD_COLUMNS(submode)
	
	PRINTLN("[2184572] DISPLAY_LEADERBOARD ")
	
	INT iWinningTeam = MC_serverBD.iWinningTeam
	INT iSecondTeam
	INT iThirdTeam
	INT iLosingTeam
	INT iXPColumn
	INT iColumn1, iColumn2, iColumn3
	
	IF HAVE_ALL_COMMON_LEADERBOARD_ELEMENTS_LOADED(LBPlacement)

		RENDER_LEADERBOARD(lbdVars, LBPlacement, submode, DRAW_LBD_THUMB_COUNT(), GET_LEADERBOARD_TITLE(submode, g_FMMC_STRUCT.tl63MissionName), inumcolumns, -1)

	   	IF MC_serverBD.iNumActiveTeams = 1
			IF MC_serverBD.iWinningTeam <> -1
				iTeam1Count = MC_serverBD.iNumberOfLBPlayers[MC_serverBD.iWinningTeam]
			ENDIF
		ELIF MC_serverBD.iNumActiveTeams = 2
			IF MC_serverBD.iWinningTeam <> -1
				iTeam1Count = MC_serverBD.iNumberOfLBPlayers[MC_serverBD.iWinningTeam]
			ENDIF
			IF MC_serverBD.iLosingTeam <> -1
			 	iTeam2Count = MC_serverBD.iNumberOfLBPlayers[MC_serverBD.iLosingTeam]
			ENDIF
		ELIF MC_serverBD.iNumActiveTeams = 3
			IF MC_serverBD.iWinningTeam <> -1
				iTeam1Count = MC_serverBD.iNumberOfLBPlayers[MC_serverBD.iWinningTeam]
			ENDIF
			IF MC_serverBD.iSecondTeam <> -1
				iTeam2Count = MC_serverBD.iNumberOfLBPlayers[MC_serverBD.iSecondTeam]
			ENDIF
			IF MC_serverBD.iLosingTeam <> -1
				iTeam3Count = MC_serverBD.iNumberOfLBPlayers[MC_serverBD.iLosingTeam]
			ENDIF
		ELSE
			IF MC_serverBD.iWinningTeam <> -1
				iTeam1Count = MC_serverBD.iNumberOfLBPlayers[MC_serverBD.iWinningTeam]
			ENDIF
			IF MC_serverBD.iSecondTeam <> -1
				iTeam2Count = MC_serverBD.iNumberOfLBPlayers[MC_serverBD.iSecondTeam]
			ENDIF
			IF MC_serverBD.iThirdTeam <> -1
				iTeam3Count = MC_serverBD.iNumberOfLBPlayers[MC_serverBD.iThirdTeam]
			ENDIF
			IF MC_serverBD.iLosingTeam <> -1
			    iTeam4Count = MC_serverBD.iNumberOfLBPlayers[MC_serverBD.iLosingTeam]
			ENDIF
		ENDIF
		itotalLBPlayers =(iTeam1Count+iTeam2Count+iTeam3Count+iTeam4Count)
		
		IF MC_serverBD.iNumActiveTeams > 1	
		AND NOT g_bOnCoopMission
			bMultiTeam = TRUE
		ENDIF
		
		INT iNumLBDTeams = MC_serverBD.iNumActiveTeams
		PRINTLN("2471055, A iNumLBDTeams = ", iNumLBDTeams)
		
		IF g_bOnCoopMission
			iNumLBDTeams = 1
		ENDIF
		
		INT iTeamScoreTotal[FMMC_MAX_TEAMS]
		INT iStoredTeamToUse[FMMC_MAX_TEAMS]
		
		IF bMultiTeam
		
			iNumLBDTeams = 0
			iTeam1Count = 0
			iTeam2Count = 0
			iTeam3Count = 0
			iTeam4Count = 0
		
			INT iTeamLoop, iTeamLoop2
			REPEAT FMMC_MAX_TEAMS iTeamLoop 
				REPEAT FMMC_MAX_TEAMS iTeamLoop2
					IF NOT IS_BIT_SET(iLbdBitSetRowan, iTeamLoop2)

						IF DOES_TEAM_LIKE_TEAM(iTeamLoop, iTeamLoop2)
						
							IF submode = SUB_MISSIONS_NO_HUD
							OR submode = SUB_MISSIONS_LTS
								IF IS_NO_SCORE_NO_KILL_LBD_SET()
									iteamtotal = GET_TEAM_DEATHS(iTeamLoop2)
									PRINTLN("[2237720] iteamtotal, iTeamDeaths = ", iteamtotal)
								ELSE
									iteamtotal = GET_TEAM_KILLS(iTeamLoop2)
									PRINTLN("[2237720] iteamtotal, iTeamkills = ", iteamtotal)
								ENDIF
								
							ELIF submode = SUB_MISSIONS_VERSUS
								IF SHOW_SCORE_COLUMN_ON_LEADERBOARD()
								OR HIDE_KILLS_DEATHS_COLUMN_LBD()
									IF g_FMMC_STRUCT.iLDB_ShowEndTimeForTeam != 0
										iteamtotal = GET_TEAM_KILLS(iTeamLoop2)
										PRINTLN("[2237720] A iteamtotal, iTeamkills = ", iteamtotal)
									ELSE
										PRINTLN("[2237720] B MC_serverBD.iTeamScore[iTeamLoop2] = ", GET_TEAM_SCORE_FOR_DISPLAY(iTeamLoop2))	
										PRINTLN("[2237720] B g_FMMC_STRUCT.iInitialPoints[iTeamLoop2] = ", g_FMMC_STRUCT.iInitialPoints[iTeamLoop2])
										iteamtotal = GET_TEAM_SCORE_FOR_DISPLAY(iTeamLoop2) - g_FMMC_STRUCT.iInitialPoints[iTeamLoop2]
										PRINTLN("[2237720] B iteamtotal, iteamtotal = ", iteamtotal)			
									ENDIF
								ELSE
									IF IS_NO_SCORE_NO_KILL_LBD_SET()
										iteamtotal = GET_TEAM_DEATHS(iTeamLoop2)
										PRINTLN("[2237720] C iteamtotal, iTeamDeaths = ", GET_TEAM_DEATHS(iTeamLoop2))
									ELSE
										iteamtotal = GET_TEAM_KILLS(iTeamLoop2)
										PRINTLN("[2237720] D iteamtotal, iTeamkills = ", GET_TEAM_KILLS(iTeamLoop2))
									ENDIF
								ENDIF
							ELSE							
								iteamtotal = GET_TEAM_SCORE_FOR_DISPLAY(iTeamLoop2) - g_FMMC_STRUCT.iInitialPoints[iTeamLoop2]
								PRINTLN("[2237720] F iteamtotal, iInitialPoints = ", g_FMMC_STRUCT.iInitialPoints[iTeamLoop2])
								PRINTLN("[2237720] F iteamtotal, iTeamScore = ", iteamtotal)
							ENDIF
						
							// Counting the teams ///////////////////////////////////////////////////////
							IF DOES_TEAM_LIKE_TEAM(iTeamLoop, MC_serverBD.iWinningTeam)
								iTeam1Count = (iTeam1Count + MC_serverBD.iNumberOfLBPlayers[iTeamLoop2])
								SET_BIT(iLbdBitSetRowan, iTeamLoop2)
								iStoredTeamToUse[iTeamLoop2] = iTeamLoop
								PRINTLN("[2471055] BBB iTeam1Count = ", iTeam1Count)
								
								iTeamScoreTotal[iTeamLoop] = (iTeamScoreTotal[iTeamLoop] + iteamtotal)
							ELIF DOES_TEAM_LIKE_TEAM(iTeamLoop, MC_serverBD.iSecondTeam)
								iTeam2Count = (iTeam2Count + MC_serverBD.iNumberOfLBPlayers[iTeamLoop2])
								SET_BIT(iLbdBitSetRowan, iTeamLoop2)
								iStoredTeamToUse[iTeamLoop2] = iTeamLoop
								PRINTLN("[2471055] BBB iTeam2Count = ", iTeam2Count)
								
								iTeamScoreTotal[iTeamLoop] = (iTeamScoreTotal[iTeamLoop] + iteamtotal)
							ELIF DOES_TEAM_LIKE_TEAM(iTeamLoop, MC_serverBD.iThirdTeam)
								iTeam3Count = (iTeam3Count + MC_serverBD.iNumberOfLBPlayers[iTeamLoop2])
								SET_BIT(iLbdBitSetRowan, iTeamLoop2)
								iStoredTeamToUse[iTeamLoop2] = iTeamLoop
								PRINTLN("[2471055] BBB iTeam3Count = ", iTeam3Count)
								
								iTeamScoreTotal[iTeamLoop] = (iTeamScoreTotal[iTeamLoop] + iteamtotal)
								
								
							ELIF DOES_TEAM_LIKE_TEAM(iTeamLoop, MC_serverBD.iLosingTeam)
							
								iTeam4Count = (iTeam4Count + MC_serverBD.iNumberOfLBPlayers[iTeamLoop2])
								SET_BIT(iLbdBitSetRowan, iTeamLoop2)
								iStoredTeamToUse[iTeamLoop2] = iTeamLoop
								PRINTLN("[2471055] BBB iTeam4Count = ", iTeam4Count)
								
								iTeamScoreTotal[iTeamLoop] = (iTeamScoreTotal[iTeamLoop] + iteamtotal)
							ENDIF
							///////////////////////////////////////////////////////////////////////////////
						ENDIF
					ENDIF
				ENDREPEAT
			ENDREPEAT	
			
			PRINTLN("[2471055] //iTeam1Count = ", iTeam1Count)
			PRINTLN("[2471055] //iTeam2Count = ", iTeam2Count)
			PRINTLN("[2471055] //iTeam3Count = ", iTeam3Count)	
			PRINTLN("[2471055] //iTeam4Count = ", iTeam4Count)	
			PRINTLN("[2471055] //iLosingTeam = ", MC_serverBD.iLosingTeam)
			
			iSecondTeam = MC_serverBD.iSecondTeam
			iThirdTeam = MC_serverBD.iThirdTeam
			iLosingTeam = MC_serverBD.iLosingTeam
								
			IF iTeam3Count > 0
				
				IF iTeam2Count = 0
					iTeam2Count = iTeam3Count
					iTeam3Count = 0
					
					iSecondTeam = MC_serverBD.iThirdTeam
					PRINTLN("[2471055] moving team 3 into slot 2 = ")
				ENDIF
			ENDIF
			IF iTeam4Count > 0			
				
				IF iTeam3Count = 0
					iTeam3Count = iTeam4Count
					iTeam4Count = 0
					
					iThirdTeam = MC_serverBD.iLosingTeam
					PRINTLN("[2471055] moving team 4 into slot 3 = ")
				ENDIF
				
				IF iTeam2Count = 0
					IF iTeam3Count > 0
						iTeam2Count = iTeam3Count
						iTeam3Count = 0
						PRINTLN("[2471055] moving team 3 into slot 2 = ")
						iSecondTeam = MC_serverBD.iThirdTeam
					ENDIF
					IF iTeam4Count > 0
						iTeam2Count = iTeam4Count
						iTeam4Count = 0
						PRINTLN("[2471055] moving team 4 into slot 2 = ")
						iSecondTeam = MC_serverBD.iLosingTeam
					ENDIF
				ENDIF
				
				IF iTeam1Count = 0
					IF iTeam2Count > 0
						iTeam1Count = iTeam2Count
						iTeam2Count = 0
						iWinningTeam = MC_serverBD.iSecondTeam
						PRINTLN("[2471055] moving team 2 into slot 1 = ")
					ENDIF		
					IF iTeam3Count > 0
						iTeam2Count = iTeam3Count
						iTeam3Count = 0
						iSecondTeam = MC_serverBD.iThirdTeam
						PRINTLN("[2471055] moving team 3 into slot 2 = ")
					ENDIF
					IF iTeam4Count > 0
						iTeam3Count = iTeam4Count
						iTeam4Count = 0
						iThirdTeam = MC_serverBD.iLosingTeam
						PRINTLN("[2471055] moving team 4 into slot 3 = ")
					ENDIF
				ENDIF

			ENDIF
		
			IF iTeam1Count > 0
				iNumLBDTeams ++
			ENDIF
			IF iTeam2Count > 0
				iNumLBDTeams ++
			ENDIF
			IF iTeam3Count > 0
				iNumLBDTeams ++
			ENDIF
			IF iTeam4Count > 0
				iNumLBDTeams ++
			ENDIF

			PRINTLN("[2471055] iTeam1Count = ", iTeam1Count)
			PRINTLN("[2471055] iTeam2Count = ", iTeam2Count)
			PRINTLN("[2471055] iTeam3Count = ", iTeam3Count)	
			PRINTLN("[2471055] iTeam4Count = ", iTeam4Count)	
			PRINTLN("[2471055] iNumLbdTeams = ", iNumLbdTeams)
		ENDIF
		
		INT iPlayerCount = (MC_serverBD.iNumberOfLBPlayers[0] + MC_serverBD.iNumberOfLBPlayers[1] + MC_serverBD.iNumberOfLBPlayers[2] +MC_serverBD.iNumberOfLBPlayers[3])
		
		IF g_FinalRoundsLbd
			iNumLBDTeams = 1
			PRINTLN("[SCROLL_DEBUG], g_FinalRoundsLbd, iNumLBDTeams = 1  ")
		ENDIF
		TRACK_LBD_ROW_FOR_SCROLL(lbdVars, iNumLBDTeams, (iPlayerCount + MC_serverBD.iNumSpectators),  iNumLBDTeams > 0 AND MC_serverBD.iNumSpectators = 0)

		PROCESS_LBD_SCROLL_LOGIC(lbdVars, timeScrollDelay, iLBPlayerSelection, iTeamRowBit, iTeamNameRowBit, (iPlayerCount + MC_serverBD.iNumSpectators), iNumLBDTeams, bMultiTeam,
									MC_serverBD.iNumSpectators)	 

		GRAB_SPECTATOR_PLAYER_SELECTED(g_MissionControllerserverBD_LB.sleaderboard, iSavedRow)
		
		PRINTLN("[PLAYER_LOOP] - DISPLAY_LEADERBOARD")
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i			

			// Draw spectators
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))				
				PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
				INT iPlayer = NATIVE_TO_INT(playerId)
				
				IF IS_MODEL_VALID(g_MissionControllerserverBD_LB.sleaderboard[i].mnVehModel)
					lbdVars.tlRaceVeh	= GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_MissionControllerserverBD_LB.sleaderboard[i].mnVehModel)
					PRINTLN("[RACE_VEH_NAME] tlRaceVeh = ", lbdVars.tlRaceVeh, " i = ", i)
				ELSE
					PRINTLN("[RACE_VEH_NAME] IS_MODEL_VALID FALSE for i = ", i)
				ENDIF
				
				INT iSpectatorRow = GET_SPECTATOR_ROW(( bMultiTeam AND NOT g_FinalRoundsLbd ), itotalLBPlayers, iNumLBDTeams, iSpectatorNumber)
				IF iPlayer <> -1
					IF IS_NET_PLAYER_OK(PlayerId,FALSE)
						IF IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_START_SPECTATOR)
							sSpectatorName = GET_PLAYER_NAME(playerId)
							
							// 2222842
							IF NOT g_FinalRoundsLbd
							OR IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_ROUNDS_LBD_GO)
								DRAW_SPECTATOR_ROW(lbdVars, LBPlacement, submode, playerId, sSpectatorName, GlobalplayerBD_FM[NATIVE_TO_INT(PlayerId)].scoreData.iRank, iSpectatorRow, i)	
							ENDIF
							
							iSavedSpectatorRow[iPlayer] = iSpectatorRow	
							
							IF iSpectatorRow < NUM_NETWORK_PLAYERS
								SET_BIT(iSpectatorBit, iSpectatorRow)
							ENDIF
							
							iSpectatorNumber ++
							
							#IF IS_DEBUG_BUILD
							PRINTNL()
							PRINTLN("2476044 DRAW_SPECTATOR_ROW______")
							PRINTNL()
							PRINTLN("2476044 sSpectatorName     = ", sSpectatorName)
							PRINTLN("2476044 iSpectatorNumber   = ", iSpectatorNumber)
							PRINTLN("2476044 iSavedSpectatorRow = ", iSavedSpectatorRow[iPlayer])
							PRINTLN("2476044 iNumActiveTeams    = ", iNumLBDTeams)
							PRINTLN("2476044 itotalLBPlayers    = ", itotalLBPlayers)
							PRINTLN("2476044 iPlayerCount       = ", iPlayerCount)
							IF bMultiTeam
								PRINTLN("2476044 bMultiTeam TRUE")
							ELSE
								PRINTLN("2476044 bMultiTeam FALSE")
							ENDIF
							PRINTNL()
							PRINTLN("________________________")
							PRINTNL()
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF g_FinalRoundsLbd
				bMultiTeam = FALSE
			ENDIF
			IF bMultiTeam
				IF g_MissionControllerserverBD_LB.sleaderboard[i].iTeam !=-1
					IF g_MissionControllerserverBD_LB.sleaderboard[i].playerID <> INVALID_PLAYER_INDEX()
						IF g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant <> -1
							IF NOT IS_BIT_SET(MC_playerBD[g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant].iClientBitSet ,PBBOOL_START_SPECTATOR)
						
								IF NOT bDrawPlayersRow[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam]
									IF bDrawPlayersRowUsed[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam] = FALSE
										bDrawPlayersRow[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam] = TRUE
										bDrawPlayersRowUsed[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam] = TRUE
									ENDIF
								ELSE
									bDrawPlayersRow[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam] = FALSE
								ENDIF
								
								// Hiding position numbers on end lbd
								IF iTeam1Count <= 1
								AND iTeam2Count <= 1
								AND iTeam3Count <= 1
								AND iTeam4Count <= 1
									lbdVars.bHidePos = TRUE
								ELSE
									IF bDrawPlayersRow[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam]
										lbdVars.bHidePos = FALSE
									ELSE
										lbdVars.bHidePos = TRUE
									ENDIF
								ENDIF
								
								// Thumb stuff
								iLbdPlayer = NATIVE_TO_INT(g_MissionControllerserverBD_LB.sleaderboard[i].playerID)
								CHECK_LBD_THUMBS(lbdVars, iLbdPlayer)

								// Team row colours
								IF DOES_TEAM_LIKE_TEAM(MC_playerBD[iPartToUse].iteam, g_MissionControllerserverBD_LB.sleaderboard[i].iTeam)
								OR PlayerToUse = g_MissionControllerserverBD_LB.sleaderboard[i].playerID
								OR MC_playerBD[iPartToUse].iteam = g_MissionControllerserverBD_LB.sleaderboard[i].iTeam
									iLocalTeam = MC_playerBD[iPartToUse].iteam
									iEnemyTeam = MC_playerBD[iPartToUse].iteam
								ELSE
									iLocalTeam = MC_playerBD[iPartToUse].iteam
									iEnemyTeam = iStoredTeamToUse[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam]
								ENDIF	
								
								TEXT_LABEL_63 tlJobRole = ""
								
								tlJobRole = g_sMission_TeamName[iStoredTeamToUse[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam]]
								
								IF HAS_TEAM_GOT_CREATOR_NAME(iStoredTeamToUse[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam])
									tlJobRole = GET_CREATOR_NAME_STRING(iStoredTeamToUse[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam])
									IF ARE_STRINGS_EQUAL(tlJobRole,"NULL")
										tlJobRole = g_sMission_TeamName[iStoredTeamToUse[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam]]
									ENDIF
								ENDIF
									
								IF NOT IS_STRING_NULL_OR_EMPTY(tlJobRole) 
								AND NOT ARE_STRINGS_EQUAL(tlJobRole,"NULL")
									lbdVars.tl63TeamNames = tlJobRole
									PRINTLN("[TM_NAME] = ", lbdVars.tl63TeamNames, " team = ", iStoredTeamToUse[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam])
								ENDIF
								
								
//								PRINTLN("[CS_LBD] iStoredTeamToUse[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam] = ", iStoredTeamToUse[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam], " i = ", i)	
															
								iRow = GET_ROW_TO_DRAW_PLAYERS(iStoredTeamToUse[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam],	
															iNumLbdTeams,
															iTeamSlotCounter[iStoredTeamToUse[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam]],
															iTeam1Count,
															iTeam2Count,
															iTeam3Count,
															0,
															0,
															0,
															0,
															iWinningTeam,
															iSecondTeam,
															iThirdTeam,
															-1,
															-1,
															-1,
															-1,
															iLosingTeam,
															FALSE,
															bMultiTeam)
															
								iSavedRow[iLbdPlayer] = iRow
								
								iTeamRow = GET_TEAM_BARS_ROW(iStoredTeamToUse[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam],iNumLbdTeams,iTeam1Count,iTeam2Count,iTeam3Count,iTeam4Count,0,0,0,0,MC_serverBD.iWinningTeam,iSecondTeam,iThirdTeam,-1, -1, -1, -1,iLosingteam,FALSE)
								
								iTeamNamesRow = GET_TEAM_NAME_ROWS(iStoredTeamToUse[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam],iNumLbdTeams,iTeamSlotCounter[iStoredTeamToUse[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam]],iTeam1Count,iTeam2Count,iTeam3Count,iTeam4Count,0,0,0,MC_serverBD.iWinningTeam,iSecondTeam,iThirdTeam,-1, -1, -1, -1)	
								
								IF iTeamRow > -1
								AND iTeamRow < 32
									SET_BIT(iTeamRowBit, iTeamRow)		
								ENDIF
								
								IF iTeamNamesRow > -1
								AND iTeamNamesRow < 32						
									SET_BIT(iTeamNameRowBit, iTeamNamesRow)		
								ENDIF
								
								IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_TIME
									iscore = g_MissionControllerserverBD_LB.sleaderboard[i].iMissionTime
								ELSE
									iscore = g_MissionControllerserverBD_LB.sleaderboard[i].iPlayerScore
								ENDIF
								
								SET_LBD_SELECTION_TO_PLAYER_POS(lbdVars, 
								g_MissionControllerserverBD_LB.sleaderboard[i].playerID, 
								iLBPlayerSelection, 
								iRow, 
								iPlayerCount)

								INT iTeamScoreToShow = iTeamScoreTotal[iStoredTeamToUse[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam]]
								INT iRound = g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed
								INT iPlayer = NATIVE_TO_INT(g_MissionControllerserverBD_LB.sleaderboard[i].playerID)
														
								IF IS_THIS_A_ROUNDS_MISSION()
									
									// Tally up the scores at the final rounds lbd
									IF g_FinalRoundsLbd
										iTeamScoreToShow = g_i_TeamScoreRounds[iPlayer]
										
										PRINTLN("[2237720] g_FinalRoundsLbd TRUE, iTeamScoreToShow = ", iTeamScoreToShow, " iPlayer = ", iPlayer)
									ELSE				
										IF NOT IS_BIT_SET(iRoundsBitSet, iRound)
										
											// Add to team totals
											g_i_TeamScoreRounds[iPlayer] += iTeamScoreToShow
											
											PRINTLN("[2237720] iPlayer = ", iPlayer, " score = ", iTeamScoreToShow, " total = ", g_i_TeamScoreRounds[iPlayer], " iNumberOfRoundsPlayed = ", iRound)
											
											SET_BIT(iRoundsBitSet, iRound)
										ENDIF
									ENDIF
									
									// Tally up the scores at the final rounds lbd for each player.
									IF g_FinalRoundsLbd
										iScore = g_i_PlayerTilesScoreRounds[iPlayer]
										PRINTLN("[LAND_GRAB_LDB] A g_FinalRoundsLbd TRUE, iTilesScoreToShow = ", iScore, " iPlayer = ", iPlayer)
									ELSE
										IF NOT IS_BIT_SET(iRoundsTilePlayerBitSet, iPlayer)
											g_i_PlayerTilesScoreRounds[iPlayer] += iScore

											PRINTLN("[LAND_GRAB_LDB] A iPlayer = ", iPlayer, " Tiles = ", iScore, " total = ", g_i_PlayerTilesScoreRounds[iPlayer], " iNumberOfRoundsPlayed = ", iRound)
											
											SET_BIT(iRoundsTilePlayerBitSet, iPlayer)										
										ENDIF
									ENDIF
									
									#IF IS_DEBUG_BUILD
										IF submode = SUB_MISSIONS_VERSUS
											PRINTLN("[2237720] SUB_MISSIONS_VERSUS ")
										ELIF submode = SUB_MISSIONS_NO_HUD
											PRINTLN("[2237720] SUB_MISSIONS_NO_HUD ")
										ELIF submode = SUB_MISSIONS_LTS
											PRINTLN("[2237720] SUB_MISSIONS_LTS ")
										ENDIF
									#ENDIF
								ENDIF
								
								// Cap
								IF iteamtotal < 0
									iteamtotal = 0
								ENDIF
								
								#IF IS_DEBUG_BUILD
								IF bTeamColourDebug
									PRINTLN("[RE] [TEAMCOLOUR] - g_MissionControllerserverBD_LB.sleaderboard[", i, "].iTeam = ", g_MissionControllerserverBD_LB.sleaderboard[i].iTeam)
									PRINTLN("[RE] [TEAMCOLOUR] - iStoredTeamToUse[g_MissionControllerserverBD_LB.sleaderboard[", i, "].iTeam] = ", iStoredTeamToUse[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam])
								ENDIF
								#ENDIF
								
								#IF IS_DEBUG_BUILD
								PRINTLN("2645594, Player = ", GET_PLAYER_NAME(g_MissionControllerserverBD_LB.sleaderboard[i].playerID), " iLocalTeam = ", iLocalTeam, " iEnemyTeam = ", iEnemyTeam)
								#ENDIF
								
								///////////////////
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciLBD_SHOW_TIME_COLUMN)
									iscore = g_MissionControllerserverBD_LB.sleaderboard[i].iMissionTime
								ENDIF
								
								PRINTLN("3107327 B iTeamScoreToShow = ", iTeamScoreToShow)							
								
								iKills = g_MissionControllerserverBD_LB.sleaderboard[i].iKills
								iDeaths = g_MissionControllerserverBD_LB.sleaderboard[i].iDeaths
								
								PRINTLN("TEAM 5446432, iscore = ", iscore)
								PRINTLN("TEAM 5446432, iKills = ", iKills)
								PRINTLN("TEAM 5446432, iDeaths = ", iDeaths)
								PRINTLN("TEAM 5446432, iJobXP = ", g_MissionControllerserverBD_LB.sleaderboard[i].iJobXP)
								
								iColumn1 = iscore
								iColumn2 = iKills 
								iColumn3 = iDeaths
								
								iXPColumn = g_MissionControllerserverBD_LB.sleaderboard[i].iJobXP
								// Add up column2 scores
								IF iPlayer >= 0
									IF NOT IS_BIT_SET(iRoundsTilePlayerBitSet2, iPlayer)	
										IF IS_THIS_A_ROUNDS_MISSION()
											g_i_PlayerTilesScoreRoundsColumn2[iPlayer] += iColumn2

											PRINTLN("[5519247] g_i_PlayerTilesScoreRoundsColumn2 iPlayer = ", iPlayer, " Tiles = ", iScore, " total = ", g_i_PlayerTilesScoreRoundsColumn2[iPlayer], " iNumberOfRoundsPlayed = ", iRound)
											
											SET_BIT(iRoundsTilePlayerBitSet2, iPlayer)	
										ENDIF
									ENDIF
								ENDIF						
								
								#IF IS_DEBUG_BUILD
								IF bEnableExtraLeaderboardPrints
									PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (1) - i  							= ", i)
									PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (1) - Name 						= ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayer)))
									PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (1) - iParticipant  				= ", g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant)
									PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (1) - iTeam 						= ", g_MissionControllerserverBD_LB.sleaderboard[i].iTeam)
									PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (1) - iTeamScore 					= ", iTeamScoreToShow)
									PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (1) - PlayerScore 					= ", iscore)						
									PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (1) - Kills 						= ", iKills)
									PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (1) - iSpecificAdversaryModeInt	= ", g_MissionControllerserverBD_LB.sleaderboard[i].iSpecificAdversaryModeInt)
									PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (1) - iDeaths 						= ", iDeaths)
									PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (1) - iAssists						= ", g_MissionControllerserverBD_LB.sleaderboard[i].iAssists)
									PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (1) - iRoundsScore 				= ", g_MissionControllerserverBD_LB.sleaderboard[i].iRoundsScore)
									PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (1) - iJobCash 					= ", g_MissionControllerserverBD_LB.sleaderboard[i].iJobCash)
									PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (1) - iJobXP 						= ", iXPColumn)
									PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (1) - iColumn1 					= ", iColumn1)
									PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (1) - iColumn2 					= ", iColumn2)
									PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (1) - iColumn3 					= ", iColumn3)
								ENDIF
								#ENDIF
								
								POPULATE_COLUMN_LBD(lbdVars,
											LBPlacement, 
											submode,
											GET_VOTE_FOR_PLAYER(g_MissionControllerserverBD_LB.sleaderboard[i].playerID,g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant),
											MC_serverBD_2.tParticipantNames,
											inumcolumns,
											g_MissionControllerserverBD_LB.sleaderboard[i].fKDRatio,
											iColumn1,
											iColumn2,
											iColumn3,
											iXPColumn,
											g_MissionControllerserverBD_LB.sleaderboard[i].iJobCash,
											g_MissionControllerserverBD_LB.sleaderboard[i].iBets,
											iLocalTeam,
											iEnemyTeam,
											iRow,
											g_MissionControllerserverBD_LB.sleaderboard[i].iRank, 
											g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant,
											g_MissionControllerserverBD_LB.sleaderboard[i].iBadgeRank,
											g_MissionControllerserverBD_LB.sleaderboard[i].playerID,
											TRUE,
											bDrawTeamRow[iStoredTeamToUse[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam]],
											iTeamScoreToShow,
											iTeamRow,
											GET_TEAM_FINISH_POSITION_FOR_LBD(iStoredTeamToUse[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam], iNumLBDTeams),
											g_MissionControllerserverBD_LB.sleaderboard[i].iRoundsScore,
											iTeamNamesRow,
											iStoredTeamToUse[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam],
											g_MissionControllerserverBD_LB.sleaderboard[i].iAssists,
											g_MissionControllerserverBD_LB.sleaderboard[i].iTeam)
									

								iTeamSlotCounter[iStoredTeamToUse[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam]]++
							ELSE
								PRINTLN("[url:bugstar:3138681][JS] 1")
							ENDIF
						ELSE
							PRINTLN("[url:bugstar:3138681][JS] 2")
						ENDIF
					ELSE
						PRINTLN("[url:bugstar:3138681][JS] 3")
					ENDIF
				ELSE
					PRINTLN("[url:bugstar:3138681][JS] 4")
				ENDIF										
			ELSE
				IF (g_MissionControllerserverBD_LB.sleaderboard[i].playerID <> INVALID_PLAYER_INDEX())
					IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_TIME
						iscore = g_MissionControllerserverBD_LB.sleaderboard[i].iMissionTime
					ELSE
						iscore = g_MissionControllerserverBD_LB.sleaderboard[i].iPlayerScore
					ENDIF

					iLbdPlayer = NATIVE_TO_INT(g_MissionControllerserverBD_LB.sleaderboard[i].playerID)
					iSavedRow[iLbdPlayer] = iSlotCounter
					
					iTeamRow = itotalLBPlayers
					//iTeamRow = GET_TEAM_BARS_ROW(g_MissionControllerserverBD_LB.sleaderboard[i].iTeam,1,itotalLBPlayers,iTeam2Count,iTeam3Count,iTeam4Count,0,0,0,0,MC_serverBD.iWinningTeam,MC_serverBD.iSecondTeam,MC_serverBD.iThirdTeam,-1, -1, -1, -1,MC_serverBD.iLosingteam,FALSE)
				
					// Hide position numbers 1426352
					IF g_MissionControllerserverBD_LB.sleaderboard[i].iTeam !=-1
						IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_TIME
							IF g_bOnCoopMission
								iteamtotal = ((MC_serverBD.iAverageMissionTime[0] + MC_serverBD.iAverageMissionTime[1] + MC_serverBD.iAverageMissionTime[2] + MC_serverBD.iAverageMissionTime[3])/MC_serverBD.iNumActiveTeams)
							ELSE
								iteamtotal = MC_serverBD.iAverageMissionTime[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam]
							ENDIF
						ELSE
							IF submode = SUB_MISSIONS_NO_HUD
							OR submode = SUB_MISSIONS_LTS
								IF g_bOnCoopMission
									iteamtotal = (GET_TEAM_KILLS(0) + GET_TEAM_KILLS(1) + GET_TEAM_KILLS(2) + GET_TEAM_KILLS(3))
								ELSE
									iteamtotal = GET_TEAM_KILLS(g_MissionControllerserverBD_LB.sleaderboard[i].iTeam)
								ENDIF
							ELSE
								IF g_bOnCoopMission
									iteamtotal = (GET_TEAM_SCORE_FOR_DISPLAY(0) + GET_TEAM_SCORE_FOR_DISPLAY(1) + GET_TEAM_SCORE_FOR_DISPLAY(2) + GET_TEAM_SCORE_FOR_DISPLAY(3))
								ELSE
									iteamtotal = GET_TEAM_SCORE_FOR_DISPLAY(g_MissionControllerserverBD_LB.sleaderboard[i].iTeam)
								ENDIF
							ENDIF
						ENDIF
						IF NOT bDrawPlayersRow[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam]
							bDrawPlayersRow[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam] = TRUE
						ELSE
							bDrawPlayersRow[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam] = FALSE
						ENDIF
						IF bDrawPlayersRow[g_MissionControllerserverBD_LB.sleaderboard[i].iTeam]
							lbdVars.bHidePos = FALSE
						ELSE
							lbdVars.bHidePos = TRUE
						ENDIF
					ENDIF
					
					SET_LBD_SELECTION_TO_PLAYER_POS(lbdVars, 
					g_MissionControllerserverBD_LB.sleaderboard[i].playerID, 
					iLBPlayerSelection, 
					iSlotCounter, 
					iPlayerCount)
			
					// Cap
					IF iteamtotal < 0
						iteamtotal = 0
					ENDIF
					
					CHECK_LBD_THUMBS(lbdVars, iLbdPlayer)
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciLBD_SHOW_TIME_COLUMN)
						iscore = g_MissionControllerserverBD_LB.sleaderboard[i].iMissionTime
					ENDIF
										
					iKills = g_MissionControllerserverBD_LB.sleaderboard[i].iKills
					iDeaths = g_MissionControllerserverBD_LB.sleaderboard[i].iDeaths
					
					PRINTLN("5446432, iscore  = ", iscore)
					PRINTLN("5446432, iKills  = ", iKills)
					PRINTLN("5446432, iDeaths = ", iDeaths)
					PRINTLN("5446432, iJobXP  = ", g_MissionControllerserverBD_LB.sleaderboard[i].iJobXP)
					
					iColumn1 = iscore
					iColumn2 = iKills 
					iColumn3 = iDeaths
					
					iXPColumn = g_MissionControllerserverBD_LB.sleaderboard[i].iJobXP
					
					#IF IS_DEBUG_BUILD
					IF bEnableExtraLeaderboardPrints
						PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (2) - i  							= ", i)
						PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (2) - Name 						= ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(i)))
						PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (2) - iParticipant  				= ", g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant)
						PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (2) - iTeam 						= ", g_MissionControllerserverBD_LB.sleaderboard[i].iTeam)
						PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (2) - PlayerScore 					= ", iscore)						
						PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (2) - Kills 						= ", iKills)
						PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (2) - iSpecificAdversaryModeInt	= ", g_MissionControllerserverBD_LB.sleaderboard[i].iSpecificAdversaryModeInt)
						PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (2) - iDeaths 						= ", iDeaths)
						PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (2) - iAssists						= ", g_MissionControllerserverBD_LB.sleaderboard[i].iAssists)
						PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (2) - iRoundsScore 				= ", g_MissionControllerserverBD_LB.sleaderboard[i].iRoundsScore)
						PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (2) - iJobCash 					= ", g_MissionControllerserverBD_LB.sleaderboard[i].iJobCash)
						PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (2) - iJobXP 						= ", iXPColumn)
						PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (2) - iColumn1 					= ", iColumn1)
						PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (2) - iColumn2 					= ", iColumn2)
						PRINTLN("[LM][LeaderBoardPrintout] - Display Leaderboard (2) - iColumn3 					= ", iColumn3)
					ENDIF
					#ENDIF

					POPULATE_COLUMN_LBD(lbdVars,
									LBPlacement, 
									submode,
									GET_VOTE_FOR_PLAYER(g_MissionControllerserverBD_LB.sleaderboard[i].playerID,g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant),
									MC_serverBD_2.tParticipantNames,
									inumcolumns,
									g_MissionControllerserverBD_LB.sleaderboard[i].fKDRatio,
									iColumn1,
									iColumn2,
									iColumn3,
									iXPColumn,
									g_MissionControllerserverBD_LB.sleaderboard[i].iJobCash,
									g_MissionControllerserverBD_LB.sleaderboard[i].iBets,
									iLocalTeam,
									iEnemyTeam,
									iSlotCounter,
									g_MissionControllerserverBD_LB.sleaderboard[i].iRank, 
									g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant,
									g_MissionControllerserverBD_LB.sleaderboard[i].iBadgeRank,
									g_MissionControllerserverBD_LB.sleaderboard[i].playerID,
									FALSE, // NOT A TEAM LBD, use bMultiTeam above
									bDrawTeamRow[0],
									iteamtotal,
									iTeamRow,
									0,
									g_MissionControllerserverBD_LB.sleaderboard[i].iRoundsScore,
									0,
									-1,
									g_MissionControllerserverBD_LB.sleaderboard[i].iAssists)
									
									
				ENDIF
				
				iSlotCounter++							
			ENDIF
		ENDREPEAT
		
		IF MC_serverBD.iNumSpectators <> iSpectatorNumber
			MC_serverBD.iNumSpectators = iSpectatorNumber
		ENDIF
	ELSE
		PRINTLN("[2184572] DISPLAY_LEADERBOARD, HAVE_ALL_COMMON_LEADERBOARD_ELEMENTS_LOADED (false) ")
	ENDIF
	
	// Set that the leaderboard has been displayed once
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_LBD_DISPLAYED)	
		PRINTLN("[RCC MISSION] [PLY] DISPLAY_LEADERBOARD, SBBOOL3_LBD_DISPLAYED ")
		SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_LBD_DISPLAYED)
	ENDIF
ENDPROC

PROC MANAGE_LEADERBOARD_DISPLAY(BOOL bRounds = FALSE)
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		EXIT
	ENDIF

	IF g_i_ActualLeaderboardPlayerSelected > -1
	AND g_i_ActualLeaderboardPlayerSelected < 32
	AND IS_BIT_SET(iSpectatorBit, g_i_ActualLeaderboardPlayerSelected)
		HANDLE_SPECTATOR_PLAYER_CARDS(lbdVars, iSavedSpectatorRow)
	ELSE
		BOOL bJobSpectating = (GET_SPECTATOR_HUD_STAGE(g_BossSpecData.specHUDData) = SPEC_HUD_STAGE_LEADERBOARD)
		HANDLE_THE_PLAYER_CARDS(g_MissionControllerserverBD_LB.sleaderboard, lbdVars, iSavedRow, bJobSpectating)
	ENDIF
	
	IF g_i_ActualLeaderboardPlayerSelected = -1
		#IF IS_DEBUG_BUILD
		PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE()
		#ENDIF
		g_i_ActualLeaderboardPlayerSelected = 0
	ENDIF
	
	IF NOT sEndOfMission.bActiveSCLeaderboard	
		DISPLAY_LEADERBOARD()
		PRINTLN("[2184572] MANAGE_LEADERBOARD_DISPLAY, DISPLAY_LEADERBOARD ")
		IF bRounds = FALSE
			DO_RESTART_HELP_MISSION()
		ENDIF
	ELSE
		DRAW_SC_SCALEFORM_LEADERBOARD(scLB_scaleformID,scLB_control)
	ENDIF
ENDPROC

PROC DISPLAY_ROUNDS_LBD()

	IF NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_NEW_JOB_SCREENS)
		DO_ROUND_LBD_TIMER(stLeaderboardRounds.Timer)
	ENDIF
	
	IF SHOULD_DRAW_ROUNDS_LBD()
		IF NOT g_FinalRoundsLbd
			g_FinalRoundsLbd = TRUE
			PRINTLN("[2460167] [CS_RNDS] DISPLAY_ROUNDS_LBD, g_FinalRoundsLbd = TRUE  ")
		ENDIF
	ENDIF

	// CLIENT LOGIC
	SWITCH iRoundsLbdStage
		CASE 0
			IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_ROUNDS_LBD_GO)
				IF SHOULD_END_ROUNDS_MISSION_DUE_TO_TEAM_LEAVING()
					MC_playerBD[iLocalPart].iBet = BROADCAST_BETTING_MISSION_FINISHED_TIED()
				ELSE
					IF MC_serverBD_3.iWinningPlayerForRounds > -1
						MC_playerBD[iLocalPart].iBet = BROADCAST_BETTING_MISSION_FINISHED(MC_serverBD_3.iWinningPlayerForRoundsId)
					ELSE
						PRINTLN("[2460167] [CS_RNDS] DISPLAY_ROUNDS_LBD, MC_serverBD_3.iWinningPlayerForRounds =  ", MC_serverBD_3.iWinningPlayerForRounds)
						EXIT
					ENDIF	
				ENDIF	
				LBPlacement.bHudScreenInitialised = FALSE
				lbdVars.bPlayerRowGrabbed = FALSE
				iRoundsLbdStage++
				iTeamRowBit = 0
				START_NET_TIMER(stLeaderboardRounds)
				PRINTLN("[2460167] [CS_RNDS] DISPLAY_ROUNDS_LBD, iRoundsLbdStage =  ", iRoundsLbdStage)
			ENDIF
		BREAK
		
		CASE 1
			IF HAS_NET_TIMER_EXPIRED(stLeaderboardRounds, ciROUNDS_LBD_TIMEOUT)
				iRoundsLbdStage++
				PRINTLN("[2460167] [CS_RNDS] DISPLAY_ROUNDS_LBD, iRoundsLbdStage =  ", iRoundsLbdStage)
			ELSE
				UNTOGGLE_RENDERPHASES_FOR_LEADERBOARD()
				MANAGE_LEADERBOARD_DISPLAY(TRUE)
			ENDIF
		BREAK
		
		CASE 2
			IF SHOULD_RUN_NEW_VOTING_SYSTEM(sEndOfMission.iQuitProgress, MC_serverBD.sServerFMMC_EOM.iVoteStatus)
			AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].bQuitJob
				SET_READY_FOR_NEXT_JOBS(nextJobStruct)
				IF NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_NEW_JOB_SCREENS)
					BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("FMMC_PLYLOAD")
					END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
				ENDIF
				SET_BIT(iLocalBoolCheck3,LBOOL3_NEW_JOB_SCREENS)
				PRINTLN("[2460167] [CS_RNDS] DISPLAY_ROUNDS_LBD, SHOULD_RUN_NEW_VOTING_SYSTEM, LBOOL3_NEW_JOB_SCREENS ")
			ELSE
				SET_BIT(iLocalBoolCheck3,LBOOL3_MOVE_TO_END)
				PRINTLN("[2460167] [CS_RNDS] DISPLAY_ROUNDS_LBD, LBOOL3_MOVE_TO_END ")
			ENDIF
			SET_ON_LBD_GLOBAL(FALSE)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC SERVER_ROUNDS_LBD_LOGIC()

	SWITCH MC_serverBD_3.iRoundLbdProgress

		CASE 0
			IF SHOULD_DRAW_ROUNDS_LBD()
				SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_ROUNDS_PAUSE_LBD_SORT) // Stop calls to (SERVER_MAINTAIN_MC_LEADERBOARD_FOR_PARTICIPANT)
				MC_serverBD_3.iRoundLbdProgress++
				PRINTLN("[2460167] [CS_RNDS] [HOST] SERVER_ROUNDS_LBD_LOGIC, SBBOOL2_ROUNDS_PAUSE_LBD_SORT)  ")
			ENDIF
		BREAK
		
		CASE 1
			MC_serverBD_3.iWinningPlayerForRounds = GET_LEADERBOARD_INDEX_OF_TOP_WINNER()
			IF MC_serverBD_3.iWinningPlayerForRounds = -1
				MC_serverBD_3.iWinningPlayerForRounds = 0
			ENDIF
			MC_serverBD_3.iWinningPlayerForRoundsId = g_MissionControllerserverBD_LB.sleaderboard[MC_serverBD_3.iWinningPlayerForRounds].playerID
			CLEAR_THE_LEADERBOARD_STRUCT_SERVER_DATA(FALSE)
			PROCESS_ROUNDS_LBD()
			MC_serverBD_3.iRoundLbdProgress++
			PRINTLN("[2460167] [CS_RNDS] [HOST] SERVER_ROUNDS_LBD_LOGIC, >>> 2 ")
		BREAK
		
		CASE 2 
			PROCESS_ROUNDS_LBD()
			IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_ROUNDS_LBD_GO)
				PRINTLN("[2460167] [CS_RNDS] [HOST] SERVER_ROUNDS_LBD_LOGIC, SBBOOL2_ROUNDS_LBD_GO  ")
				SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_ROUNDS_LBD_GO)
			ENDIF
		BREAK
	
	ENDSWITCH
ENDPROC

FUNC INT GET_SPECIFIED_LBD_PLAYER(PLAYER_INDEX playerID)
	INT i
	FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
		IF g_MissionControllerserverBD_LB.sleaderboard[i].playerID = playerID
			RETURN i
		ENDIF
	ENDFOR
	
	RETURN 0
ENDFUNC

PROC PROCESS_SPECTATOR_PLAYER_LISTS_FOR_MISSION()
	#IF IS_DEBUG_BUILD
	BOOL bSpamPrints = bsc3378817
	#ENDIF

	INT iSpectatorId = GET_SPECTATOR_LIST_PLAYER_ID_FOR_THIS_FRAME(g_BossSpecData)
	
	#IF IS_DEBUG_BUILD
	IF bSpamPrints
		PRINTLN("[sc3378817] iSpectatorId = ", iSpectatorId)
	ENDIF
	#ENDIF
	
	PLAYER_INDEX playerPosition = INT_TO_PLAYERINDEX(iSpectatorId)
	
	IF IS_NET_PLAYER_OK(playerPosition, FALSE)
	
		#IF IS_DEBUG_BUILD
		IF bSpamPrints
			PRINTLN("[sc3378817] MAINTAIN_SPECTATOR_PLAYER_LIST_POSITION a, playerPosition = ", GET_PLAYER_NAME(playerPosition))
		ENDIF
		#ENDIF
	
		MAINTAIN_SPECTATOR_PLAYER_LIST_POSITION(g_BossSpecData, GET_SPECIFIED_LBD_PLAYER(playerPosition))
		
		#IF IS_DEBUG_BUILD
		IF bSpamPrints
			PRINTLN("[sc3378817] MAINTAIN_SPECTATOR_PLAYER_LIST_POSITION a, GET_SPECIFIED_LBD_PLAYER = ", GET_SPECIFIED_LBD_PLAYER(playerPosition))
		ENDIF
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF bSpamPrints
			PRINTLN("[sc3378817] MAINTAIN_SPECTATOR_PLAYER_LIST_POSITION b,  ")
		ENDIF
		#ENDIF
		
		MAINTAIN_SPECTATOR_PLAYER_LIST_POSITION(g_BossSpecData, -1)
	ENDIF		
	
	IF g_bVSMission
	AND GET_SPECTATOR_LIST_TEAM_FOR_THIS_FRAME(g_BossSpecData) < MC_serverBD.iNumActiveTeams
		INT iSpecTeamId = GET_SPECTATOR_LIST_TEAM_FOR_THIS_FRAME(g_BossSpecData)
		INT iTeamPos = GET_TEAM_FINISH_POSITION(iSpecTeamId)
		
		#IF IS_DEBUG_BUILD
		IF bSpamPrints
			PRINTLN("[sc3378817] MAINTAIN_SPECTATOR_TEAM_LIST_POSITION a, iSpecTeamId = ", iSpecTeamId, " iTeamPos = ", iTeamPos, " MC_serverBD.iNumActiveTeams = ", MC_serverBD.iNumActiveTeams)
		ENDIF
		#ENDIF
		
		MAINTAIN_SPECTATOR_TEAM_LIST_POSITION(g_BossSpecData, iTeamPos)
	ELSE
	
		#IF IS_DEBUG_BUILD
		IF bSpamPrints
			PRINTLN("[sc3378817] MAINTAIN_SPECTATOR_TEAM_LIST_POSITION b,  ")
		ENDIF
		#ENDIF
	
		MAINTAIN_SPECTATOR_TEAM_LIST_POSITION(g_BossSpecData, -1)
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)
	AND IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		SET_LBD_BOOL(TRUE)
	ELSE
		SET_LBD_BOOL(FALSE)
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: "Circle" HUD ------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


FUNC STRING GET_PPG_SCALEFORM_STATE_AS_STRING(PPGenericState eState)
	SWITCH eState
		CASE PPG_REQUEST_ASSETS	RETURN "PPG_REQUEST_ASSETS"
		CASE PPG_INIT			RETURN "PPG_INIT"
		CASE PPG_RUNNING		RETURN "PPG_RUNNING"
	ENDSWITCH
	RETURN "State invalid, maybe GET_PPG_SCALEFORM_STATE_AS_STRING needs updated"
ENDFUNC

PROC SET_PPG_SCALEFORM_STATE(PPGenericState eState)
	PRINTLN("[JS][PPGHUD] - SET_PPG_SCALEFORM_STATE - State changed from ",
			GET_PPG_SCALEFORM_STATE_AS_STRING(ePPGenericState), " TO ",
			GET_PPG_SCALEFORM_STATE_AS_STRING(eState))
	ePPGenericState = eState
ENDPROC

FUNC BOOL SHOULD_HIDE_CIRCLE_SCORE_HUD_TIMER()
	
	IF IS_RP_BAR_ACTIVE_IN_ARENA_BOX()
		PRINTLN("[LM] SHOULD_HIDE_CIRCLE_SCORE_HUD_TIMER - RETURN TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_CIRCLE_SCORE_HUD()
	INT i
	SWITCH ePPGenericState
		CASE PPG_REQUEST_ASSETS
			IF HAS_SCALEFORM_MOVIE_LOADED(sfiPPGenericMovie)
				SET_PPG_SCALEFORM_STATE(PPG_INIT)
			ELSE
				PRINTLN("[JT][HTHUD] - PROCESS_CIRCLE_SCORE_HUD - Requesting Scaleform Movie")
				sfiPPGenericMovie = REQUEST_SCALEFORM_MOVIE("POWER_PLAY_GENERIC")
			ENDIF
		BREAK
		
		CASE PPG_INIT
			FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_TEAM")
					INT iHudColour
					iHudColour = ENUM_TO_INT(GET_HUD_COLOUR_FOR_FMMC_TEAM(i, PlayerToUse))
					PRINTLN("[JT][HTHUD] - PROCESS_CIRCLE_SCORE_HUD - Adding team: ", i, " hud colour: ", iHudColour, " PlayerToUse: ", GET_PLAYER_NAME(PlayerToUse), " PlayerTeam: ", GET_PLAYER_TEAM(PlayerToUse) )
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHudColour)
				END_SCALEFORM_MOVIE_METHOD()
			ENDFOR
			
			FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_ICON")
					PRINTLN("[JT][HTHUD] - PROCESS_CIRCLE_SCORE_HUD - Adding icon for team: ", i, " icon: ", ciPPGICON_SCORE_RING)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ciPPGICON_SCORE_RING)
				END_SCALEFORM_MOVIE_METHOD()
			ENDFOR
					
			SET_PPG_SCALEFORM_STATE(PPG_RUNNING)
		BREAK
		
		CASE PPG_RUNNING
			FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "SET_ICON_SCORE")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i)	//Icon Index
					
					IF IS_TEAM_ACTIVE(i)
						INT iScoreToShow
						iScoreToShow = GET_TEAM_SCORE_FOR_DISPLAY(i) - g_FMMC_STRUCT.iInitialPoints[i]
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScoreToShow) //Score to show
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					ENDIF
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i) //Team index for colour
				END_SCALEFORM_MOVIE_METHOD()
				
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "SET_ICON_METER")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i) //Icon Index
					INT j
					FOR j = 0 TO MC_serverBD.iNumberOfTeams - 1
						IF i = j
						AND IS_TEAM_ACTIVE(i)
						AND NOT HAS_TEAM_FAILED(i)
						
							FLOAT fScorePercentage
							INT iCurrentScore 
							iCurrentScore = GET_TEAM_SCORE_FOR_DISPLAY(i) - g_FMMC_STRUCT.iInitialPoints[i]
							INT iTargetScore
							INT iRule 
							iRule = MC_serverBD_4.iCurrentHighestPriority[i]
							
							IF iRule < FMMC_MAX_RULES
								IF g_FMMC_STRUCT.sFMMCEndConditions[i].iMissionTargetScore > 0
									iTargetScore = g_FMMC_STRUCT.sFMMCEndConditions[i].iMissionTargetScore
								ELSE
									iTargetScore = GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[i].iTargetScore[0], MC_ServerBD.iNumberOfPlayingPlayers[i], MC_serverBD.iMaxNumPriorityEntitiesThisRule[i])
								ENDIF
								fScorePercentage =  TO_FLOAT(iCurrentScore) / TO_FLOAT(iTargetScore)
								
							ENDIF
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fScorePercentage)
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0)
						ENDIF
					ENDFOR
				END_SCALEFORM_MOVIE_METHOD()
				
				IF NOT IS_RP_BAR_ACTIVE_IN_ARENA_BOX()
					DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_RANKBAR) 
				ENDIF
			ENDFOR
		BREAK	
	ENDSWITCH
	
	IF ePPGenericState > PPG_REQUEST_ASSETS
	AND NOT SHOULD_HIDE_CIRCLE_SCORE_HUD_TIMER()
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH( "appinternet" ) ) < 1 )
		OR NOT IS_BROWSER_OPEN()
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfiPPGenericMovie, 255, 255, 255, 255)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CIRCLE_TIMER()

	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF

	BOOL bCountdownActive
	fRadialPulsTimePassed = 0.0
	fRadialPulsTimeMax = 0.0
	bShowRadialPuse = FALSE
	
	//Timer
	IF MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam] != 0
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciCIRCLE_TIMER_IGNORE_MULTIRULE)
		IF IS_LOCAL_PLAYER_MULTIRULE_TIMER_RUNNING()
			fRadialPulsTimePassed = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam]))
			fRadialPulsTimeMax = TO_FLOAT(MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam])
			bCountdownActive = IS_FINAL_COUNTDOWN_ACTIVE(iTeam, iRule, GET_LOCAL_PLAYER_MULTIRULE_TIMER_TIME_REMAINING())
		ELSE
			fRadialPulsTimePassed = 0.0
			fRadialPulsTimeMax = 0.0
		ENDIF
	ELSE
		IF IS_OBJECTIVE_TIMER_RUNNING(iTeam) //Rule timer 
			fRadialPulsTimePassed = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iTeam]))
			fRadialPulsTimeMax = TO_FLOAT(GET_OBJECTIVE_TIME_LIMIT_FOR_RULE(iTeam, iRule))
			bCountdownActive = IS_FINAL_COUNTDOWN_ACTIVE(iTeam, iRule, GET_OBJECTIVE_TIMER_TIME_REMAINING(iTeam, iRule))
		ELSE
			fRadialPulsTimePassed = 0.0
			fRadialPulsTimeMax = 0.0
		ENDIF
	ENDIF

	IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
	AND (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSUDDEN_DEATH_THAT_ENDS_WHEN_ONLY_ONE_TEAM_ALIVE)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES))
		IF HAS_NET_TIMER_STARTED(MC_ServerBD.tdSuddenDeath_ShrinkingBoundsHUDTimer[iTeam])
			fRadialPulsTimePassed = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_ServerBD.tdSuddenDeath_ShrinkingBoundsHUDTimer[iTeam]))
			fRadialPulsTimeMax = TO_FLOAT(g_FMMC_STRUCT.iSphereShrinkTime)
			bCountdownActive = IS_FINAL_COUNTDOWN_ACTIVE(iTeam, iRule, GET_REMAINING_TIME_ON_SUDDEN_DEATH_SHRINKING_BOUNDS(iTeam))
			PRINTLN("[PROCESS_CIRCLE_TIMER] Switching to Sudden Death Timer (Shrinking Bounds) --- fTimePassed: ", fRadialPulsTimePassed, " fTimeMax: ", fRadialPulsTimeMax)
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iObjectiveSuddenDeathTimeLimit[iRule] > 0
			fRadialPulsTimePassed = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iteam]) - MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam])
			fRadialPulsTimeMax = TO_FLOAT(GET_SUDDEN_DEATH_TIME_LIMIT_FOR_RULE(iTeam, iRule))
			INT iTimeRemaining 
			iTimeRemaining = ROUND(fRadialPulsTimeMax - fRadialPulsTimePassed)
			bCountdownActive = IS_FINAL_COUNTDOWN_ACTIVE(iTeam, iRule, iTimeRemaining)
			PRINTLN("[PROCESS_CIRCLE_TIMER] Switching to Sudden Death Timer (multirule) --- fTimePassed: ", fRadialPulsTimePassed, " fTimeMax: ", fRadialPulsTimeMax)
		ENDIF
	ENDIF
	
	IF fRadialPulsTimePassed >= fRadialPulsTimeMax
		fRadialPulsTimePassed = fRadialPulsTimeMax
	ENDIF
	
	PRINTLN("[PROCESS_CIRCLE_TIMER] Timer --- fTimePassed: ", fRadialPulsTimePassed, " fTimeMax: ", fRadialPulsTimeMax)
	
	IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_PULSED_CIRCLE_HUD_TIMER)
		IF NOT HAS_NET_TIMER_STARTED(tdCircleHUDPulseTimer)
			START_NET_TIMER(tdCircleHUDPulseTimer)
			PRINTLN("[PROCESS_CIRCLE_TIMER] Start pulse timer")
		ENDIF
		IF HAS_NET_TIMER_STARTED(tdCircleHUDPulseTimer)
			IF HAS_NET_TIMER_EXPIRED(tdCircleHUDPulseTimer, ci_CIRCLE_HUD_PULSE_TIME)
				RESET_NET_TIMER(tdCircleHUDPulseTimer)
				CLEAR_BIT(iLocalBoolCheck24, LBOOL24_PULSED_CIRCLE_HUD_TIMER)
				PRINTLN("[PROCESS_CIRCLE_TIMER] Reset pulse timer")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
		iRadialPulseCounter = 0
		PRINTLN("[PROCESS_CIRCLE_TIMER] Reset Pulse counter new rule")
	ENDIF
			
	IF fRadialPulsTimePassed < fRadialPulsTimeMax
	AND fRadialPulsTimePassed > 0
	AND (IS_OBJECTIVE_TIMER_RUNNING(iTeam) OR IS_MULTIRULE_TIMER_RUNNING(iTeam))
	AND NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
		IF bCountDownActive
		AND NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_PULSED_CIRCLE_HUD_TIMER)
		AND iRadialPulseCounter < 5
			SET_BIT(iLocalBoolCheck24, LBOOL24_PULSED_CIRCLE_HUD_TIMER)
			iRadialPulseCounter++
			bShowRadialPuse = TRUE
			PRINTLN("[PROCESS_CIRCLE_TIMER] Show/Play pulse HUD/SOUND: iRadialPulseCounter: ", iRadialPulseCounter)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_CIRCLE_TIMER_SOUND()

	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	//RESET
	BOOL bSuddenDeath = ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
	OR bSuddenDeath
		IF NOT bSuddenDeath 
		OR NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_LAST_TIMER_RESET_FOR_SD)
			PLAY_SOUND_FRONTEND(-1, "TIMER_RADIAL_Reset", "DLC_BTL_SM_Remix_Soundset", FALSE)
			PRINTLN("[PROCESS_CIRCLE_TIMER][PROCESS_CIRCLE_TIMER_SOUND] PLAY_SOUND_FRONTEND TIMER_RADIAL_Reset")
			
			IF bSuddenDeath
				SET_BIT(iLocalBoolCheck28, LBOOL28_LAST_TIMER_RESET_FOR_SD)
				PRINTLN("[PROCESS_CIRCLE_TIMER][PROCESS_CIRCLE_TIMER_SOUND] PULSE CHECK: Setting LBOOL28_LAST_TIMER_RESET_FOR_SD")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Shows a single circle icon to be used as a timer. This will prioritise the mutlirule timer over the rule timer.
PROC PROCESS_CIRCLE_TIMER_HUD()

	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]	
	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF

	SWITCH ePPGenericState
		CASE PPG_REQUEST_ASSETS
			IF HAS_SCALEFORM_MOVIE_LOADED(sfiPPGenericMovie)
				SET_PPG_SCALEFORM_STATE(PPG_INIT)
			ELSE
				PRINTLN("[JT][HTHUD] - PROCESS_CIRCLE_TIMER_HUD - Requesting Scaleform Movie")
				sfiPPGenericMovie = REQUEST_SCALEFORM_MOVIE("POWER_PLAY_GENERIC")
			ENDIF
		BREAK
		
		CASE PPG_INIT
			
			BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_TEAM")			
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_WHITE))
			END_SCALEFORM_MOVIE_METHOD()
			BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_TEAM")			
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_RED))
			END_SCALEFORM_MOVIE_METHOD()
			BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_ICON")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ciPPGICON_BLANK)
			END_SCALEFORM_MOVIE_METHOD()
			
			SET_PPG_SCALEFORM_STATE(PPG_RUNNING)
		BREAK
		
		CASE PPG_RUNNING
						
			BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "SET_ICON_TIMER")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) //Icon index
				
				IF fRadialPulsTimePassed <= ((fRadialPulsTimeMax/4)*3)
				AND fRadialPulsTimePassed >= 0
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fRadialPulsTimePassed / fRadialPulsTimeMax) //Value
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0) //Value
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fRadialPulsTimePassed / fRadialPulsTimeMax) //Value
				ENDIF
			END_SCALEFORM_MOVIE_METHOD()
			
			
			IF bShowRadialPuse
				BEGIN_SCALEFORM_MOVIE_METHOD (sfiPPGenericMovie, "PULSE_ICON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				END_SCALEFORM_MOVIE_METHOD()
				PRINTLN("[PROCESS_CIRCLE_TIMER][PROCESS_CIRCLE_TIMER_HUD] Showed Pulse Icon")
			ENDIF
		BREAK	
	ENDSWITCH
	
	IF ePPGenericState > PPG_REQUEST_ASSETS
	AND NOT SHOULD_HIDE_CIRCLE_SCORE_HUD_TIMER()
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH( "appinternet" ) ) < 1 )
		OR NOT IS_BROWSER_OPEN()
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfiPPGenericMovie, 255, 255, 255, 255)
		ENDIF
	ENDIF
ENDPROC

PROC CIRCLE_HUD_SET_UP_METERED_TEAM_SCORE(INT iIconIndex, INT iTeam)
	
	IF IS_TEAM_ACTIVE(iTeam)
	AND NOT HAS_TEAM_FAILED(iTeam)
		PRINTLN("[JT][CIRCLE HUD] Calling CIRCLE_HUD_SET_UP_METERED_TEAM_SCORE for team: ", iTeam)
		BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "SET_ICON_SCORE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIconIndex)	//Icon Index
			INT iScoreToShow
			iScoreToShow = GET_TEAM_SCORE_FOR_DISPLAY(iTeam) - g_FMMC_STRUCT.iInitialPoints[iTeam]
			
			// This is required due to a Score reset/Cache system which sets the teamscore back to 0. iPoints given to pass would make it appear as -1 here.
			IF iScoreToShow <= -1
			AND MC_ServerBD.iPointsGivenToPass[iTeam] > 0
				iScoreToShow = 0
			ENDIF
			
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScoreToShow) //Score to show
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIconIndex) //Team index for colour
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "SET_ICON_METER")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIconIndex) //Icon Index
			INT j
			FOR j = 0 TO MC_serverBD.iNumberOfTeams
				INT iTeamToUse = j
				IF iTeamToUse > CEIL(TO_FLOAT(MC_serverBD.iNumberOfTeams)/2)
					iTeamToUse -= 1
				ENDIF
				
				IF iTeam = iTeamToUse
				AND IS_TEAM_ACTIVE(iTeam)
				AND NOT HAS_TEAM_FAILED(iTeam)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(GET_SCORE_PERCENTAGE_FOR_HUD(iTeam))
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0)
				ENDIF
			ENDFOR
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

PROC CIRCLE_HUD_SET_UP_TIMER_ICON(INT iIconIndex, INT iTeam)
	INT iRule = GET_TEAM_CURRENT_RULE(iTeam)
	FLOAT fTimePassed, fTimeMax
	
	IF iRule < FMMC_MAX_RULES
		IF MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam] != 0
		AND NOT (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciCIRCLE_TIMER_IGNORE_MULTIRULE) AND IS_OBJECTIVE_TIMER_RUNNING(iTeam))
			IF IS_MULTIRULE_TIMER_RUNNING(iTeam)
				fTimePassed = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam]))
				fTimeMax = TO_FLOAT(MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam])
			ELSE
				fTimePassed = 0.0
				fTimeMax = 0.0
			ENDIF
		ELSE
			IF IS_OBJECTIVE_TIMER_RUNNING(iTeam) //Rule timer 
				fTimePassed = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iTeam]))
				fTimeMax = TO_FLOAT(GET_OBJECTIVE_TIME_LIMIT_FOR_RULE(iTeam, iRule))
			ELSE
				fTimePassed = 0.0
				fTimeMax = 0.0
			ENDIF
		ENDIF
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "SET_ICON_TIMER")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIconIndex) //Icon index
		INT iIndex
		FOR iIndex = 0 TO MC_serverBD.iNumberOfTeams
			IF iIndex = CEIL(TO_FLOAT(MC_serverBD.iNumberOfTeams)/2)
				IF fTimePassed / fTimeMax < 0.85
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTimePassed / fTimeMax) //Value
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTimePassed / fTimeMax) //Value
				ENDIF
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0)
			ENDIF
		ENDFOR

	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC CIRCLE_HUD_ADD_TEAM_COLOUR_AND_ICON(INT iTeam)
	
	IF IS_TEAM_ACTIVE(iTeam)
	AND NOT HAS_TEAM_FAILED(iTeam)
		PRINTLN("[JT][CIRCLE HUD] Calling CIRCLE_HUD_ADD_TEAM_COLOUR_AND_ICON for team: ", iTeam)
		BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_TEAM")
			INT iHudColour
			iHudColour = ENUM_TO_INT(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, PlayerToUse))
			
			PRINTLN("[JT][HTHUD] - PROCESS_CIRCLE_SCORE_HUD - Adding team: ", iTeam, " hud colour: ", iHudColour)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHudColour)
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_ICON")
			PRINTLN("[JT][HTHUD] - PROCESS_CIRCLE_SCORE_HUD - Adding icon for team: ", iTeam, " icon: ", ciPPGICON_SCORE_RING)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ciPPGICON_SCORE_RING)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC
	
PROC PROCESS_CIRCLE_SCORE_HUD_WITH_TIMER()
	
	SWITCH ePPGenericState
		CASE PPG_REQUEST_ASSETS
			IF HAS_SCALEFORM_MOVIE_LOADED(sfiPPGenericMovie)
				SET_PPG_SCALEFORM_STATE(PPG_INIT)
			ELSE
				PRINTLN("[JT][HTHUD] - PROCESS_CIRCLE_SCORE_HUD_WITH_TIMER - Requesting Scaleform Movie")
				sfiPPGenericMovie = REQUEST_SCALEFORM_MOVIE("POWER_PLAY_GENERIC")
			ENDIF
		BREAK
		
		CASE PPG_INIT
			
			IF MC_serverBD.iNumberOfTeams <= 2
				PRINTLN("[JT][HTHUD] - PROCESS_CIRCLE_SCORE_HUD_WITH_TIMER - <= 2 teams: ", MC_serverBD.iNumberOfTeams)
				//Team 0
				CIRCLE_HUD_ADD_TEAM_COLOUR_AND_ICON(0)
				//Timer
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_TEAM")			
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_WHITE))
				END_SCALEFORM_MOVIE_METHOD()
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_ICON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ciPPGICON_BLANK)
				END_SCALEFORM_MOVIE_METHOD()
				//Team 1
				CIRCLE_HUD_ADD_TEAM_COLOUR_AND_ICON(1)
				
				//Adding Red Timer "team"
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_TEAM")			
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_RED))
				END_SCALEFORM_MOVIE_METHOD()
			ELSE
				PRINTLN("[JT][HTHUD] - PROCESS_CIRCLE_SCORE_HUD_WITH_TIMER - More Than 2 teams: ", MC_serverBD.iNumberOfTeams)
				//Team 0
				CIRCLE_HUD_ADD_TEAM_COLOUR_AND_ICON(0)
				//Team 1
				CIRCLE_HUD_ADD_TEAM_COLOUR_AND_ICON(1)
				//Timer
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_TEAM")			
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_WHITE))
				END_SCALEFORM_MOVIE_METHOD()
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_ICON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ciPPGICON_BLANK)
				END_SCALEFORM_MOVIE_METHOD()
				//Team 2
				CIRCLE_HUD_ADD_TEAM_COLOUR_AND_ICON(2)
				//Team 3
				CIRCLE_HUD_ADD_TEAM_COLOUR_AND_ICON(3)
				
			ENDIF
					
			SET_PPG_SCALEFORM_STATE(PPG_RUNNING)
		BREAK
		
		CASE PPG_RUNNING
			INT iIconIndex, iTeamToUse

			FOR iIconIndex = 0 TO MC_serverBD.iNumberOfTeams
				iTeamToUse = iIconIndex
				IF iTeamToUse > CEIL(TO_FLOAT(MC_serverBD.iNumberOfTeams)/2)
					iTeamToUse -= 1
				ENDIF
				
				IF iIconIndex = CEIL(TO_FLOAT(MC_serverBD.iNumberOfTeams)/2)
					CIRCLE_HUD_SET_UP_TIMER_ICON(iIconIndex, MC_playerBD[iPartToUse].iteam)
				ELSE
					IF iTeamToUse < FMMC_MAX_TEAMS
						CIRCLE_HUD_SET_UP_METERED_TEAM_SCORE(iIconIndex,iTeamToUse)
					ENDIF
				ENDIF
			ENDFOR
			
		BREAK	
	ENDSWITCH
	
	IF ePPGenericState > PPG_REQUEST_ASSETS
	AND NOT SHOULD_HIDE_CIRCLE_SCORE_HUD_TIMER()
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH( "appinternet" ) ) < 1 )
		OR NOT IS_BROWSER_OPEN()
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfiPPGenericMovie, 255, 255, 255, 255)
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: GPS & Hint Cam ------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_QUICK_GPS_AND_HINT_CAM_BE_BLOCKED_THIS_FRAME()
	
	INT iTeam = MC_PlayerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		RETURN TRUE
	ENDIF
	
	IF !bLocalPlayerPedOk
		RETURN TRUE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		RETURN TRUE
	ENDIF
	
	IF iNearestTarget != iLastFramesNearestTarget
	OR iNearestTargetType != iLastFramesTargetType
		RETURN TRUE
	ENDIF
		
	IF iNearestTargetType = ci_TARGET_NONE
	OR iNearestTarget = -1
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_FOCUS_INTRO)
	AND NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_FOCUS_INTRO)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_DISABLE_GPSANDHINTCAM)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_OBJCOMP_KILLCHASEHINTCAM_1)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_OBJCOMP_KILLCHASEHINTCAM_2)
		RETURN TRUE
	ENDIF
	
	IF iSpectatorTarget != -1
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
	AND iRule > 0
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_MIDPOINT_THIS_FRAME + iTeam)
	AND iRule > 0
		RETURN TRUE
	ENDIF
	
	IF IS_OBJECTIVE_BLOCKED()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC VECTOR GET_DROP_OFF_BLIP_COORD()

	IF DOES_BLIP_EXIST(DeliveryBlip)
		RETURN GET_DROP_OFF_CENTER(TRUE)
	ELIF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_DUMMY_BLIP_OVERRIDE)
		RETURN GET_DUMMY_OVERRIDE_CENTER(iDummyBlipCurrentOverride) 
	ENDIF
				
	RETURN <<0, 0, 0>>
				
ENDFUNC

FUNC BOOL SHOULD_QUICK_GPS_BE_BLOCKED_THIS_FRAME()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_BlockQuickGPS)
		RETURN TRUE
	ENDIF
	
	IF SHOULD_QUICK_GPS_AND_HINT_CAM_BE_BLOCKED_THIS_FRAME()
		RETURN TRUE
	ENDIF
	
	INT iTeam = MC_PlayerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam] //Checked valid above
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_TOGGLE_QUICKGPS)
		RETURN TRUE
	ENDIF
	
	SWITCH iNearestTargetType
	
		CASE ci_TARGET_DROP_OFF
			
			IF IS_VECTOR_ZERO(GET_DROP_OFF_BLIP_COORD())
				RETURN TRUE
			ENDIF
			
		BREAK
	
		CASE ci_TARGET_LOCATION
		
			IF IS_VECTOR_ZERO(GET_LOCATION_VECTOR(iNearestTarget))
				RETURN TRUE
			ENDIF
			
		BREAK
		
		CASE ci_TARGET_PED
		
			IF IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iNearestTarget])
				RETURN TRUE
			ENDIF
			
		BREAK
		
		CASE ci_TARGET_VEHICLE
		
			IF NOT IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[iNearestTarget])
				RETURN TRUE
			ENDIF
			
		BREAK
		
		CASE ci_TARGET_OBJECT
		
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iNearestTarget))
				RETURN TRUE
			ENDIF
			
			IF IS_ENTITY_DEAD(NET_TO_ENT(GET_OBJECT_NET_ID(iNearestTarget)))
				RETURN TRUE
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL PROCESS_QUICK_GPS()
		
		
	IF SHOULD_QUICK_GPS_BE_BLOCKED_THIS_FRAME()
		
		CLEAN_QUICK_GPS()
		
		RETURN FALSE
		
	ENDIF
	
	MPGlobalsAmbience.bIsGPSDropOff = FALSE
	
	SWITCH iNearestTargetType
	
		CASE ci_TARGET_DROP_OFF
			MPGlobalsAmbience.vQuickGPS = GET_DROP_OFF_BLIP_COORD()
			MPGlobalsAmbience.bIsGPSDropOff = TRUE
		BREAK
	
		CASE ci_TARGET_LOCATION
			MPGlobalsAmbience.vQuickGPS = GET_LOCATION_VECTOR(iNearestTarget)
		BREAK
		
		CASE ci_TARGET_PED
			MPGlobalsAmbience.vQuickGPS = GET_ENTITY_COORDS(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niPed[iNearestTarget]))
		BREAK
		
		CASE ci_TARGET_VEHICLE
			MPGlobalsAmbience.vQuickGPS = GET_ENTITY_COORDS(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[iNearestTarget]))
		BREAK
		
		CASE ci_TARGET_OBJECT
			MPGlobalsAmbience.vQuickGPS = GET_ENTITY_COORDS(NET_TO_ENT(GET_OBJECT_NET_ID(iNearestTarget)))
		BREAK
		
	ENDSWITCH
					
	MPGlobalsAmbience.iQuickGPSTeam = MC_PlayerBD[iPartToUse].iTeam
	MPGlobalsAmbience.iQuickGPSPriority = MC_serverBD_4.iCurrentHighestPriority[MPGlobalsAmbience.iQuickGPSTeam] //Checked valid above
				
	RETURN TRUE

ENDFUNC

FUNC BOOL IS_SPECIAL_HINT_CAM_ACTIVE()
	
	IF IS_BIT_SET(iLocalBoolCheck30, LBOOL30_HAS_BEEN_SET_INTO_COVER_START_POSITION)
	AND (NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_HAS_CORRECTED_COVER_START_CAMERA_HEADING) OR NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_HAS_CORRECTED_COVER_START_PLAYER_HEADING))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC VEHICLE_SEAT GET_VEHICLE_SEAT_FOR_HINT_CAM(VEHICLE_INDEX viVehicle)
	
	VEHICLE_SEAT vsSeat = GET_SEAT_PED_IS_IN(LocalPlayerPed, TRUE)
	
	MODEL_NAMES mnVeh = GET_ENTITY_MODEL(viVehicle)
	
	SWITCH mnVeh
		CASE THRUSTER
			vsSeat = VS_DRIVER
		BREAK
	ENDSWITCH
	
	RETURN vsSeat
	
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_SEAT_BLOCK_HINT_CAM(VEHICLE_INDEX viVehicle, VEHICLE_SEAT vsSeat)
	
	IF NOT IS_VEHICLE_DRIVEABLE(viVehicle)
		RETURN TRUE
	ENDIF
	
	IF IS_TURRET_SEAT(viVehicle, vsSeat)
		RETURN TRUE
	ENDIF

	MODEL_NAMES mnVeh = GET_ENTITY_MODEL(viVehicle)
	
	SWITCH mnVeh
		CASE APC
		CASE AKULA
			IF vsSeat != VS_DRIVER
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE RIOT2
			IF HAS_VEHICLE_GOT_MOD(viVehicle, MOD_ROOF) 
			AND GET_VEHICLE_MOD(viVehicle, MOD_ROOF) != -1
				IF vsSeat = VS_FRONT_RIGHT
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC FLOAT GET_HINT_CAM_DISTANCE_CUTOFF(BOOL bTargetIsDropOffVehicle, INT iTargetVeh)

	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		RETURN cfHINT_CAM_CUTOFF_RANGE_DEFAULT
	ENDIF
	
	IF IS_PED_IN_ANY_HELI(LocalPlayerPed)
	OR IS_PED_IN_ANY_PLANE(LocalPlayerPed)
		RETURN cfHINT_CAM_CUTOFF_RANGE_AIR_VEHICLE
	ENDIF

	MODEL_NAMES mnVeh = DUMMY_MODEL_FOR_SCRIPT
	IF iNearestTargetType = ci_TARGET_VEHICLE
	AND IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[iNearestTarget])
		mnVeh = GET_ENTITY_MODEL(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[iNearestTarget]))
	ELIF bTargetIsDropOffVehicle
		mnVeh = GET_ENTITY_MODEL(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[iTargetVeh]))
	ENDIF
	
	IF IS_THIS_MODEL_A_HELI(mnVeh)
	OR IS_THIS_MODEL_A_PLANE(mnVeh)
		RETURN cfHINT_CAM_CUTOFF_RANGE_AIR_VEHICLE
	ENDIF

	RETURN cfHINT_CAM_CUTOFF_RANGE_DEFAULT
	
ENDFUNC

FUNC BOOL SHOULD_HINT_CAMS_BE_BLOCKED_THIS_FRAME()

	IF bIsAnySpectator
		RETURN TRUE
	ENDIF

	IF !bLocalPlayerPedOk
		RETURN TRUE
	ENDIF
	
	IF MPGlobalsAmbience.bDisableMissionHintCam
		RETURN TRUE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		RETURN TRUE
	ENDIF

	IF IS_SPECIAL_HINT_CAM_ACTIVE()
		RETURN TRUE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		RETURN TRUE
	ENDIF
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
		RETURN TRUE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		RETURN TRUE
	ENDIF
	
	IF IS_CINEMATIC_CAM_RENDERING()
		RETURN TRUE
	ENDIF
	
	IF g_bBombCamActive
	OR g_bPlayerViewingTurretHud
	OR g_heligunCamActive
		#IF IS_DEBUG_BUILD
			IF NOT HAS_NET_TIMER_STARTED(stHintCamTemporarilyDisableTimer)
				PRINTLN("[NEAREST_TARGET] SHOULD_HINT_CAMS_BE_BLOCKED_THIS_FRAME - Starting stHintCamTemporarilyDisableTimer")
			ENDIF
		#ENDIF
		REINIT_NET_TIMER(stHintCamTemporarilyDisableTimer)
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(stHintCamTemporarilyDisableTimer)
		IF HAS_NET_TIMER_EXPIRED(stHintCamTemporarilyDisableTimer, ciHintCamTemporarilyDisableTimer_Length)
			RESET_NET_TIMER(stHintCamTemporarilyDisableTimer)
			PRINTLN("[NEAREST_TARGET] SHOULD_HINT_CAMS_BE_BLOCKED_THIS_FRAME - Resetting stHintCamTemporarilyDisableTimer")
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PlayerPedToUse, TRUE)
	
		IF GET_SCRIPT_TASK_STATUS(PlayerPedToUse, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) = PERFORMING_TASK
			RETURN TRUE
		ENDIF
		
		IF GET_PED_RESET_FLAG(PlayerPedToUse, PRF_IsSeatShuffling)
			RETURN TRUE
		ENDIF
		
		VEHICLE_INDEX viVehicle = GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE)
		VEHICLE_SEAT vsSeat = GET_VEHICLE_SEAT_FOR_HINT_CAM(viVehicle)
		IF SHOULD_VEHICLE_SEAT_BLOCK_HINT_CAM(viVehicle, vsSeat)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_OBJECTIVE_HINT_CAM_BE_BLOCKED_THIS_FRAME(BOOL &bTargetIsDropOffVehicle)
	
	IF SHOULD_HINT_CAMS_BE_BLOCKED_THIS_FRAME()
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck30, LBOOL30_FORCED_HINT_CAM_ACTIVE)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_BlockHintCam)
		RETURN TRUE
	ENDIF
	
	IF SHOULD_QUICK_GPS_AND_HINT_CAM_BE_BLOCKED_THIS_FRAME()
		RETURN TRUE
	ENDIF
	
	INT iTeam = MC_PlayerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam] //Checked valid above
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_TOGGLE_HINTCAM)
		RETURN TRUE
	ENDIF
	
	INT iVeh = -1
	IF iNearestTargetType = ci_TARGET_DROP_OFF
	AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_VEHICLE
		iVeh = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iRule] 
		IF iVeh >= 0
		AND IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			bTargetIsDropOffVehicle = TRUE
		ENDIF
	ENDIF
	
	FLOAT fHintCamCutoff = GET_HINT_CAM_DISTANCE_CUTOFF(bTargetIsDropOffVehicle, iVeh)
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_HALF_HINT_CAM_FOCUS_RANGE)
		fHintCamCutoff /= 2
	ENDIF
	IF fNearestTargetDist2Temp < MAX_FLOAT
	AND fNearestTargetDist2 > POW(fHintCamCutoff, 2)
		RETURN TRUE
	ENDIF
	
	SWITCH iNearestTargetType
	
		CASE ci_TARGET_DROP_OFF
			IF bTargetIsDropOffVehicle
				IF NOT IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iRule]])
					RETURN TRUE
				ENDIF
			ELSE
				VECTOR vDropOffCoord
				vDropOffCoord = GET_DROP_OFF_BLIP_COORD()
				IF IS_VECTOR_ZERO(vDropOffCoord)
				OR GET_DIST2_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, vDropOffCoord) < POW(cfHINT_CAM_MIN_COORD_DISTANCE, 2)
					RETURN TRUE
				ENDIF
			ENDIF						
		BREAK	
	
		CASE ci_TARGET_LOCATION
			IF  MC_playerBD[iPartToUse].iCurrentLoc = iNearestTarget
			OR GET_DIST2_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, GET_LOCATION_VECTOR(iNearestTarget)) < POW(cfHINT_CAM_MIN_COORD_DISTANCE, 2)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE ci_TARGET_PED
			IF IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iNearestTarget])
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE ci_TARGET_VEHICLE
			IF NOT IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[iNearestTarget])
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE ci_TARGET_OBJECT
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iNearestTarget))
			OR IS_ENTITY_DEAD(NET_TO_ENT(GET_OBJECT_NET_ID(iNearestTarget)))
				RETURN TRUE
			ENDIF
		BREAK
	
	ENDSWITCH
	
	IF NOT IS_PED_IN_ANY_VEHICLE(PlayerPedToUse, TRUE)
		RETURN FALSE
	ENDIF
	
	//Player In Vehicle
	
	IF iNearestTargetType = ci_TARGET_VEHICLE
		IF IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[iNearestTarget])
			VEHICLE_INDEX tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iNearestTarget])
			IF IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_FORCED_HINT_CAM()
	
	IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_FORCED_HINT_CAM_TRIGGERED)
		EXIT
	ENDIF
	
	IF iForcedHintCamTargetType = -1
	OR iForcedHintCamTargetID = -1
		PRINTLN("[FORCED_HINT] PROCESS_FORCED_HINT_CAM - Invalid Data")
		CLEAR_FORCED_HINT_CAM_VARS()
		EXIT
	ENDIF
	
	IF SHOULD_HINT_CAMS_BE_BLOCKED_THIS_FRAME()
		PRINTLN("[FORCED_HINT] PROCESS_FORCED_HINT_CAM - Hint Cams Blocked")
		CLEAR_FORCED_HINT_CAM_VARS()
		EXIT
	ENDIF
	
	g_bBlockHintCamUsageInMPMission = FALSE 
	
	IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_FORCED_HINT_CAM_ACTIVE)
		PRINTLN("[FORCED_HINT] PROCESS_FORCED_HINT_CAM - Pointing at Target. ID: ", iForcedHintCamTargetID, " Type: ", iForcedHintCamTargetType)
		KILL_CHASE_HINT_CAM(sForcedHintCam)
		FORCE_CHASE_HINT_CAM(sForcedHintCam, TRUE)
		SET_CUSTOM_CHASE_HINT_CAM_INTERP_OUT_TIME(sForcedHintCam, iForcedHintCamTime)
		REINIT_NET_TIMER(tdForcedHintCamReset)
		SET_BIT(iLocalBoolCheck30, LBOOL30_FORCED_HINT_CAM_ACTIVE)
	ELIF HAS_NET_TIMER_STARTED_AND_EXPIRED(tdForcedHintCamReset, iForcedHintCamTime + iCHASE_HINT_INTERP_IN_TIME + 1000) //A little extra to avoid weirdness
		PRINTLN("[FORCED_HINT] PROCESS_FORCED_HINT_CAM - Expired")
		CLEAR_FORCED_HINT_CAM_VARS()
		EXIT
	ENDIF
	
	ENTITY_INDEX eiEntity = GET_ENTITY_FROM_LINKED_ENTITY_TYPE(iForcedHintCamTargetType, iForcedHintCamTargetID)
	
	IF eiEntity != NULL
	AND NOT IS_ENTITY_DEAD(eiEntity)
		IF GET_DIST2_BETWEEN_ENTITIES(eiEntity, LocalPlayerPed) > POW(cfFORCED_HINT_CAM_CUTOFF_RANGE_DEFAULT, 2.0)
			PRINTLN("[FORCED_HINT] PROCESS_FORCED_HINT_CAM - Entity Too Far")
			CLEAR_FORCED_HINT_CAM_VARS()
			EXIT
		ENDIF
	
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
	 		CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(sForcedHintCam, eiEntity, DEFAULT, DEFAULT, DEFAULT, FALSE)
		ELSE
			CONTROL_ENTITY_CHASE_HINT_CAM_ON_FOOT(sForcedHintCam, eiEntity, DEFAULT, DEFAULT, DEFAULT, FALSE)
		ENDIF
	ELSE
		VECTOR vCoords = GET_COORDS_FROM_LINKED_ENTITY_TYPE(iForcedHintCamTargetType, iForcedHintCamTargetID)
		IF IS_VECTOR_ZERO(vCoords)
			PRINTLN("[FORCED_HINT] PROCESS_FORCED_HINT_CAM - Invalid Coords")
			CLEAR_FORCED_HINT_CAM_VARS()
			EXIT
		ENDIF
		
		IF GET_DIST2_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, vCoords) > POW(cfFORCED_HINT_CAM_CUTOFF_RANGE_DEFAULT, 2.0)
			PRINTLN("[FORCED_HINT] PROCESS_FORCED_HINT_CAM - Coords Too Far")
			CLEAR_FORCED_HINT_CAM_VARS()
			EXIT
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
	 		CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE(sForcedHintCam, vCoords, DEFAULT, DEFAULT, DEFAULT, FALSE)
		ELSE
			CONTROL_COORD_CHASE_HINT_CAM_ON_FOOT(sForcedHintCam, vCoords, DEFAULT, DEFAULT, DEFAULT, FALSE)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL PROCESS_HINT_CAM()
	
	BOOL bTargetIsDropOffVehicle = FALSE
	
	//Global used in: x:\gta5\script\dev_ng\singleplayer\include\public\chase_hint_cam.sch
	g_bBlockHintCamUsageInMPMission = FALSE 
	
	IF SHOULD_OBJECTIVE_HINT_CAM_BE_BLOCKED_THIS_FRAME(bTargetIsDropOffVehicle)
		
		IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_FORCED_HINT_CAM_ACTIVE)
			g_bBlockHintCamUsageInMPMission = TRUE
			
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_HNT")
				CLEAR_HELP()
			ENDIF
			
			KILL_CHASE_HINT_CAM(sHintCam)
		ENDIF
		
		RETURN FALSE
		
	ENDIF
	
	INT iTeam = MC_PlayerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam] //Checked valid above		

	SWITCH iNearestTargetType
	
		CASE ci_TARGET_DROP_OFF
			IF bTargetIsDropOffVehicle
				CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(sHintCam, NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iRule]]))
			ELSE
				CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE( sHintCam, GET_DROP_OFF_BLIP_COORD(), "", HINTTYPE_NO_FOV)
			ENDIF						
		BREAK	
	
		CASE ci_TARGET_LOCATION
			CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE(sHintCam, GET_LOCATION_VECTOR(iNearestTarget), "", HINTTYPE_NO_FOV)
		BREAK
		
		CASE ci_TARGET_PED
			CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(sHintCam, NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niPed[iNearestTarget]))
		BREAK
		
		CASE ci_TARGET_VEHICLE
			CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(sHintCam, NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[iNearestTarget]))
		BREAK
		
		CASE ci_TARGET_OBJECT
			CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(sHintCam, NET_TO_ENT(GET_OBJECT_NET_ID(iNearestTarget)))
		BREAK
	
	ENDSWITCH
		
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_NEAREST_TARGET_HUD()

	IF iLastFramesNearestTarget = -1
		iLastFramesNearestTarget = iNearestTarget 
	ENDIF
	
	IF iLastFramesTargetType = ci_TARGET_NONE
		iLastFramesTargetType = iNearestTargetType
	ENDIF
	
	BOOL bHintCamAllowed = PROCESS_HINT_CAM()
	BOOL bQuickGPSAllowed = PROCESS_QUICK_GPS()
	
	IF !bHintCamAllowed
	AND !bQuickGPSAllowed
		iLastFramesNearestTarget = -1
		iLastFramesTargetType = ci_TARGET_NONE
	ENDIF
				
ENDPROC

PROC FMMC_DRAW_GENERIC_METER_WITH_SECONDARY_BAR(STRING sTitle, INT iNumber, INT iMaxNumber, FLOAT fSecondarybarPercentage, HUDORDER hoHudOrder = HUDORDER_DONTCARE, HUD_COLOURS hcTitleColour = HUD_COLOUR_WHITE, HUD_COLOURS hcBarColour = HUD_COLOUR_WHITE, HUD_COLOURS hcSecondaryBarColour = HUD_COLOUR_GREYLIGHT)
	DRAW_GENERIC_METER(iNumber, iMaxNumber, sTitle, hcBarColour, DEFAULT, hoHudOrder, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, hcTitleColour, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, hcSecondaryBarColour, fSecondarybarPercentage, TRUE)
ENDPROC

PROC SET_UP_LOOT_BAG_UI_ELEMENT(LOOT_BAG_ELEMENT &eBagElement, STRING sLabel, INT iLootType, HUDORDER eHUDOrder)
	eBagElement.sLabel = sLabel
	eBagElement.fSpaceUsedPerecnt = MC_playerBD[iLocalPart].sLootBag.fCurrentCapacity[iLootType] / BAG_CAPACITY__GET_MAX_CAPACITY()
	eBagElement.fSpaceUsedPerecnt *= 100
	eBagElement.fSpaceUsedPerecnt = TO_FLOAT(ROUND(eBagElement.fSpaceUsedPerecnt))
	eBagElement.eHUDOrder = eHUDOrder
ENDPROC

PROC BAG_CAPACITY__PROCESS_CAPACITY_HUD()
	
	IF NOT BAG_CAPACITY__IS_ENABLED()
		EXIT
	ENDIF
	
	IF SHOULD_HIDE_THE_HUD_THIS_FRAME()
		EXIT
	ENDIF
	
	IF BAG_CAPACITY__SHOULD_WAIT_TO_SHOW_HUD()
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_DisableExpandingBagHUD)
		IF IS_FAKE_MULTIPLAYER_MODE_SET()
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_MULTIPLAYER_INFO)
				TOGGLE_BIT(iLocalBoolCheck33, LBOOL33_SHOW_FULL_CAPACITY_HUD)
			ENDIF
		ELSE
			IF GET_DPADDOWN_ACTIVATION_STATE() != DPADDOWN_NONE//HAS_NET_TIMER_STARTED(MPGlobalsHud.iOnMissionOverlayTimer)
				IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_SHOW_FULL_CAPACITY_HUD)
					SET_BIT(iLocalBoolCheck33, LBOOL33_SHOW_FULL_CAPACITY_HUD)
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_SHOW_FULL_CAPACITY_HUD)
					CLEAR_BIT(iLocalBoolCheck33, LBOOL33_SHOW_FULL_CAPACITY_HUD)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	INT iGrabbed = FLOOR(MC_playerBD[iPartToUse].sLootBag.fCurrentBagCapacity)
	INT iFlashTime = -1
	HUD_COLOURS hcBar = HUD_COLOUR_WHITE
	TEXT_LABEL_63 tlLootUILabel = "MC_BAG_TL"
	
	IF g_FMMC_STRUCT.iBagCapacityCustomLabel > -1
		tlLootUILabel = GET_CUSTOM_STRING_LIST_TEXT_LABEL(g_FMMC_STRUCT.iBagCapacityCustomLabel)
	ENDIF
	
	IF NOT IS_BIT_SET(sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_BagFull)
		IF BAG_CAPACITY__IS_BAG_FULL()
			IF NOT HAS_NET_TIMER_STARTED(tdBagCapacity_FullTimer)
				REINIT_NET_TIMER(tdBagCapacity_FullTimer)
			ELIF HAS_NET_TIMER_EXPIRED(tdBagCapacity_FullTimer, ciBagCapacity_FulTimerLength)
				SET_BIT(sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_BagFull)
			ENDIF
			iFlashTime = ciBagCapacity_FulTimerLength
			hcBar = HUD_COLOUR_GREEN
			
			TEXT_LABEL_63 tlBagFullTextLabelToUse = "FMMC_BAG_F"
			IF g_FMMC_STRUCT.iBagFullCustomLabel > -1
				tlBagFullTextLabelToUse = GET_CUSTOM_STRING_LIST_TEXT_LABEL(g_FMMC_STRUCT.iBagFullCustomLabel)
			ENDIF
			PRINT_HELP(tlBagFullTextLabelToUse, ciBagCapacity_FulTimerLength)
		ENDIF
	ELSE
		hcBar = HUD_COLOUR_GREEN
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetFifteen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE15_FAKE_FULL_CAPACITY)
		iGrabbed = BAG_CAPACITY__GET_MAX_CAPACITY()
		hcBar    = HUD_COLOUR_GREEN
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_SHOW_FULL_CAPACITY_HUD)
		DRAW_GENERIC_METER(iGrabbed, BAG_CAPACITY__GET_MAX_CAPACITY(), tlLootUILabel, hcBar, iFlashTime, HUDORDER_SECONDBOTTOM, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT,
						   DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(GFX_ORDER_BEFORE_HUD_PRIORITY_HIGH))
	ELSE
		FLOAT fPercent = TO_FLOAT(iGrabbed) / BAG_CAPACITY__GET_MAX_CAPACITY()
		fPercent *= 100
		LOOT_BAG_UI_DATA sLootData
		sLootData.sBagLabel = TEXT_LABEL_TO_STRING(tlLootUILabel)
		sLootData.iItems = ciLOOT_TYPE_MAX
		sLootData.fSpaceFilledPercent = fPercent
		sLootData.eBagHUDOrder = HUDORDER_SEVENTHBOTTOM
		SET_UP_LOOT_BAG_UI_ELEMENT(sLootData.element[ciLOOT_TYPE_CASH], "H4P_INT2_CASH_T", ciLOOT_TYPE_CASH, HUDORDER_SIXTHBOTTOM)
		SET_UP_LOOT_BAG_UI_ELEMENT(sLootData.element[ciLOOT_TYPE_COKE], "H4P_INT2_COKE_T", ciLOOT_TYPE_COKE, HUDORDER_FIFTHBOTTOM)
		SET_UP_LOOT_BAG_UI_ELEMENT(sLootData.element[ciLOOT_TYPE_WEED], "H4P_INT2_WEED_T", ciLOOT_TYPE_WEED, HUDORDER_FOURTHBOTTOM)
		SET_UP_LOOT_BAG_UI_ELEMENT(sLootData.element[ciLOOT_TYPE_GOLD], "H4P_INT2_GOLD_T", ciLOOT_TYPE_GOLD, HUDORDER_THIRDBOTTOM)
		SET_UP_LOOT_BAG_UI_ELEMENT(sLootData.element[ciLOOT_TYPE_PAINTING], "H4P_INT2_PAIN_T", ciLOOT_TYPE_PAINTING, HUDORDER_SECONDBOTTOM)
		DRAW_LOOT_BAG_UI(sLootData)
	ENDIF
ENDPROC

PROC SET_HUD_DATA_FOR_OBJECT_CARRYING_HUD(INT iTeam, HUD_COLOURS &hcDot[], BOOL &bCross[], INT &iLimit)
	
	iLimit = MC_serverBD.iNumObjHighestPriority[iTeam]
	
	INT iDot
	FOR iDot = 0 TO MC_serverBD.iNumObjHighestPriority[iTeam]
		
		IF iDot >= COUNT_OF(hcDot)
			EXIT
		ENDIF
	
		hcDot[iDot] = HUD_COLOUR_WHITE
		
		bCross[iDot] = MC_serverBD.iNumObjHighestPriorityHeld[iTeam] > iDot
		
	ENDFOR
	
	IF iLimit >= COUNT_OF(hcDot)
		EXIT
	ENDIF

	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	IF iRule < FMMC_MAX_RULES
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjRequiredForRulePass[iRule] > -1
			hcDot[iLimit] = HUD_COLOUR_WHITE
			bCross[iLimit] = MC_serverBD.iObjCarrier[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjRequiredForRulePass[iRule]] != -1
			iLimit++
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_OBJ_CARRYING_HUD()

	IF SHOULD_HIDE_THE_HUD_THIS_FRAME()
		EXIT
	ENDIF
	
	INT iRule = GET_LOCAL_PLAYER_CURRENT_RULE(TRUE)

	IF IS_RULE_INDEX_VALID(iRule)
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM(TRUE)].iRuleBitsetFourteen[iRule], ciBS_RULE14_OBJ_CARRY_HUD)	
		INT iTeam = GET_LOCAL_PLAYER_TEAM(TRUE)
		HUD_COLOURS hcDot[8]
		BOOL bCross[8]
		INT iLimit
		HUDORDER carryHudOrder = HUDORDER_TOP
		
		SET_HUD_DATA_FOR_OBJECT_CARRYING_HUD(iTeam, hcDot, bCross, iLimit)
		
		TEXT_LABEL_15 tlLabelToUse = "FMMC_OCH_TRGT"
		IF g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM(TRUE)].iObjCarryHUDCustomLabel[iRule] > -1
			tlLabelToUse = GET_CUSTOM_STRING_LIST_TEXT_LABEL(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM(TRUE)].iObjCarryHUDCustomLabel[iRule])
		ENDIF
		
		DRAW_ONE_PACKAGES_EIGHT_HUD(iLimit, tlLabelToUse, FALSE, hcDot[0], hcDot[1], hcDot[2], hcDot[3], hcDot[4], hcDot[5], hcDot[6], hcDot[7], DEFAULT, DEFAULT, bCross[0], bCross[1], bCross[2], bCross[3], bCross[4], bCross[5], bCross[6], bCross[7], carryHudOrder)
	ENDIF
ENDPROC

FUNC STRING GET_HUD_TITLE_FOR_MISSION_EQUIPMENT_TYPE(INT iMissionEquipmentType)

	SWITCH iMissionEquipmentType
		CASE ciCUSTOM_MISSIONEQUIPMENT_MODEL__BOLTCUTTERS	RETURN "FMMC_MEH_BC"
		CASE ciCUSTOM_MISSIONEQUIPMENT_MODEL__GRAPPLEHOOK	RETURN "FMMC_MEH_GH"
		CASE ciCUSTOM_MISSIONEQUIPMENT_MODEL__DUFFEL		RETURN "FMMC_MEH_DF"
		CASE ciCUSTOM_MISSIONEQUIPMENT_MODEL__ENVELOPE		RETURN "FMMC_MEH_EN"
		CASE ciCUSTOM_MISSIONEQUIPMENT_MODEL__KEYCARD_A		RETURN "FMMC_MEH_KC"
		CASE ciCUSTOM_MISSIONEQUIPMENT_MODEL__KEY_RING		RETURN "FMMC_MEH_KR"
		CASE ciCUSTOM_MISSIONEQUIPMENT_MODEL__PAPER_CODES	RETURN "FMMC_MEH_PC"
		CASE ciCUSTOM_MISSIONEQUIPMENT_MODEL__KEYCARD_B		RETURN "FMMC_MEH_KC"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC PROCESS_MISSION_EQUIPMENT_COLLECTION_HUD()
	
	IF iMissionEquipment_DisplayCollectionHUDForEquipmentTypeBS = 0
		EXIT
	ENDIF
	
	IF SHOULD_HIDE_THE_HUD_THIS_FRAME()
	OR IS_PED_INJURED(LocalPlayerPed)
	OR (IS_PLAYER_RESPAWNING(LocalPlayer) AND NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE))
		EXIT
	ENDIF
	
	INT iMissionEquipmentType = 0
	FOR iMissionEquipmentType = 0 TO ciCUSTOM_MISSIONEQUIPMENT_MODEL__MAX - 1
	
		IF NOT IS_BIT_SET(iMissionEquipment_DisplayCollectionHUDForEquipmentTypeBS, iMissionEquipmentType)
			RELOOP
		ENDIF
		
		IF iMissionEquipmentPickupsMax[iMissionEquipmentType] = 0
		OR iMissionEquipmentPickupsCollected[iMissionEquipmentType] = 0 // Only show this HUD after players have picked up at least one of the equipment type 
			RELOOP
		ENDIF
		
		INT iCollected = iMissionEquipmentPickupsCollected[iMissionEquipmentType]
		HUDORDER eOrder = HUDORDER_TOP
		HUD_COLOURS eDotColour = HUD_COLOUR_WHITE
				
		DRAW_ONE_PACKAGES_EIGHT_HUD(iMissionEquipmentPickupsMax[iMissionEquipmentType], GET_HUD_TITLE_FOR_MISSION_EQUIPMENT_TYPE(iMissionEquipmentType), FALSE, eDotColour, eDotColour, eDotColour, eDotColour, eDotColour, eDotColour, eDotColour, eDotColour, DEFAULT, DEFAULT, (0 < iCollected), (1 < iCollected), (2 < iCollected), (3 < iCollected), (4 < iCollected), (5 < iCollected), (6 < iCollected), (7 < iCollected), eOrder)
	ENDFOR
ENDPROC

PROC PROCESS_SUPPORT_SNIPER_TIMER()
	// is support sniper ability active?
	IF IS_PLAYER_ABILITY_ACTIVE(PA_SUPPORT_SNIPER)
	AND SHOULD_SUPPORT_SNIPER_SHOW_TIMER()
		// get active support sniper timer & time limit			
		TIMER_INFO_STRUCT sTimerInfo
		IF GET_SUPPORT_SNIPER_TIMER_INFO(sTimerInfo)
			INT iRemainingTime = sTimerInfo.iTimerDuration
			iRemainingTime -= GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sTimerInfo.tTimer)
			DRAW_GENERIC_TIMER(iRemainingTime, sTimerInfo.sTimerTitle, 0, TIMER_STYLE_USEMILLISECONDS)
		ENDIF				
	ENDIF
ENDPROC

PROC PROCESS_WANTED_DELAY_HUD()
	
	INT iRule = GET_LOCAL_PLAYER_CURRENT_RULE(TRUE, FALSE)
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedDelayHUDString[iRule] = -1 
		EXIT
	ENDIF
	
	IF SHOULD_HIDE_THE_HUD_THIS_FRAME()
		EXIT
	ENDIF
	
	INT iRemainingTime = -1
	
	IF IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_SHOW_WANTED_DELAY_HUD_FAKE_WANTED_TIMER)
		iRemainingTime = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedDelay[iRule] - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdFakeWantedDelayTimer)
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_SHOW_WANTED_DELAY_HUD_REAL_WANTED_TIMER)
		iRemainingTime = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedDelay[iRule] - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdWantedDelayTimer)
	ENDIF
	
	IF iRemainingTime < 0
		CLEAR_10_SECOND_COUNTDOWN_BEEPS()
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tl63 = GET_CUSTOM_STRING_LIST_TEXT_LABEL(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedDelayHUDString[iRule])
	
	IF iRemainingTime > 10000 //10 secs to match sound effect
		DRAW_GENERIC_TIMER(iRemainingTime, tl63)
	ELSE
		PLAY_10_SECOND_COUNTDOWN_BEEPS()
		DRAW_GENERIC_TIMER(iRemainingTime, tl63, DEFAULT, DEFAULT, 999999999, DEFAULT, DEFAULT, DEFAULT,
					HUD_COLOUR_RED, HUDFLASHING_FLASHWHITE, 1)
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: "Objective Limits" HUD
// ##### Description: Rule timer and target score HUD copied from PROCESS_OBJECTOVE_LIMITS
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC HUD_COLOURS GET_HUD_COLOUR_FROM_TIME_REMAINING(INT iTimeRemaining)
	IF iTimeRemaining < 30000
		RETURN HUD_COLOUR_RED
	ENDIF
	
	RETURN HUD_COLOUR_WHITE
ENDFUNC

FUNC INT GET_TEAM_HUD_VISIBILITY_BITSET(INT iTeam)
	SWITCH MC_playerBD[iPartToUse].iTeam
		CASE 0 RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeam1HUDVisibility
		CASE 1 RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeam2HUDVisibility
		CASE 2 RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeam3HUDVisibility
		CASE 3 RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeam4HUDVisibility
	ENDSWITCH
	
	ASSERTLN("GET_TEAM_HUD_VISIBILITY_BITSET - INVALID TEAM")
	RETURN -1
ENDFUNC

PROC PROCESS_OBJECTIVE_LIMITS_HUD()
	
	INT iTeam
	HUD_COLOURS eCarryColour[ciFMMC_MAX_HUD_PACKAGES]
	BOOL bMyTeam[ciFMMC_MAX_HUD_PACKAGES]
	HUD_COLOURS eHudColour
	INT iScore
	INT iScoreLimit
	INT iTeamScore
	INT iMS
	TEXT_LABEL_63 tl63LocalTitle
	BOOL bLiteral = TRUE
	BOOL bDrawnTimerAlready = FALSE

	SHOW_PED_AGGRO_HELP_TEXT()
	
	IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED)
		EXIT
	ENDIF

	IF !bPlayerToUseOK
		EXIT
	ENDIF

	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF SHOULD_HIDE_THE_HUD_THIS_FRAME() //Used to be IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
		EXIT
	ENDIF
			
	IF IS_PLAYLIST_DOING_CHALLENGE()
		IF IS_THIS_A_SCORE_BASED_MISSION(FMMC_TYPE_MISSION)
			DRAW_GENERIC_SCORE(GET_CURRENT_MISSION_TIME_SCORE_TO_BEAT(), "MC_CHALSCR")
		ELSE
			DRAW_GENERIC_TIMER(GET_CURRENT_MISSION_TIME_SCORE_TO_BEAT(), "MC_CHALTIME")
		ENDIF
	ENDIF
	
	IF NOT bDrawnTimerAlready
		IF HAS_NET_TIMER_STARTED(MC_serverBD.tdMidpointFailTimer[MC_playerBD[iPartToUse].iTeam])
		AND NOT HAS_TEAM_FAILED(MC_playerBD[iPartToUse].iTeam)
			iMS = GET_MIDPOINT_FAIL_TIME_LIMIT_FOR_LOCAL_PLAYER_CURRENT_RULE() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMidpointFailTimer[MC_playerBD[iPartToUse].iTeam])
			IF iMS >= 0
				IF NOT IS_SPECTATOR_SHOWING_NEWS_HUD() 
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE_HIDE_HUD)	
				AND NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_DRAW_DISTANCE_TO_CROSS_LINE)
					bDrawnTimerAlready = TRUE
					DRAW_GENERIC_TIMER(iMS, "MC_TIMEOL", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, GET_HUD_COLOUR_FROM_TIME_REMAINING(iMS))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
				
	IF IS_LOCAL_PLAYER_OBJECTIVE_TIMER_RUNNING()
	OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciMULTIRULE_TIMER_SUDDEN_DEATH) AND IS_LOCAL_PLAYER_MULTIRULE_TIMER_RUNNING())
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciMULTIRULE_TIMER_SUDDEN_DEATH)
			iMS = GET_LOCAL_PLAYER_OBJECTIVE_TIMER_TIME_REMAINING()
		ELSE
			iMS = GET_LOCAL_PLAYER_MULTIRULE_TIMER_TIME_REMAINING()
		ENDIF

		IF iMS >= 0
		OR (NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH() AND DOES_MISSION_HAVE_SUDDEN_DEATH_ENDING())
			
			IF NOT bDrawnTimerAlready
				IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_DRAW_DISTANCE_TO_CROSS_LINE)
					IF iMS <= 0
						iMS = 0
					ENDIF
					IF NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE_HIDE_HUD)
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_FOOTBALL_HUD)
							bDrawnTimerAlready = TRUE
							DRAW_GENERIC_TIMER(iMS, "MC_TIMEVF", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, GET_HUD_COLOUR_FROM_TIME_REMAINING(iMS))
						ELSE
							IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_DRAW_DISTANCE_TO_CROSS_LINE)
								IF NOT g_b_OnLeaderboard = TRUE
									INT iRedTimerFlashTime = -1
									eHudColour = GET_HUD_COLOUR_FROM_TIME_REMAINING(iMS)
									IF fRedTimerTime > 0
										eHudColour = HUD_COLOUR_RED
										iRedTimerFlashTime = HIGHEST_INT
										fRedTimerTime -= GET_FRAME_TIME()
									ENDIF
										
									IF IS_STRING_EMPTY(g_FMMC_STRUCT.tl23CustomTimerString)
										bDrawnTimerAlready = TRUE
										DRAW_GENERIC_TIMER(iMS, "MC_TIMEOL", DEFAULT, DEFAULT, iRedTimerFlashTime, DEFAULT, HUDORDER_BOTTOM, DEFAULT, eHudColour)
									ELSE
										bDrawnTimerAlready = TRUE
										DRAW_GENERIC_TIMER(iMS, g_FMMC_STRUCT.tl23CustomTimerString, DEFAULT, DEFAULT, iRedTimerFlashTime, DEFAULT, HUDORDER_BOTTOM, DEFAULT, eHudColour, DEFAULT, DEFAULT, DEFAULT, DEFAULT, !IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_CustomTimerNameIsTextLabel))
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELIF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()						
			
			IF IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_OUT()
				HUD_COLOURS eHudColourBounds = HUD_COLOUR_YELLOW
				IF GET_BOUNDS_FIRST_ACTIVE_ZONE(ciRULE_BOUNDS_INDEX__PRIMARY) > -1
					eHudColourBounds = GET_HUD_COLOUR_FROM_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sPlacedZones[GET_BOUNDS_FIRST_ACTIVE_ZONE(0)].iBlipColor)
				ENDIF
				DISPLAY_SUDDEN_DEATH_SHARD(eHudColourBounds)
			ENDIF
			
			IF NOT bDrawnTimerAlready
				IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_DRAW_DISTANCE_TO_CROSS_LINE)
					IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iObjectiveSuddenDeathTimeLimit[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]] > 0
						iMS = GET_SUDDEN_DEATH_TIME_REMAINING(MC_playerBD[iPartToUse].iTeam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam])
						IF iMS < 0
							iMS = 0
						ENDIF
						IF NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
						AND NOT (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE_HIDE_HUD)
						AND NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdSuddenDeath_ShrinkingBoundsHUDTimer[iTeam]))
							IF (NOT IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN) OR IS_BIT_SET(MC_playerBD[iPartToUse].iKillAllEnemiesPennedInBS, ciKAE_ELIMINATION_TIMER_FINISHED))
							OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciDISABLE_BOUNDS_TIMER_OVERRIDE_FOR_SUDDEN_DEATH)
								bDrawnTimerAlready = TRUE
								DRAW_GENERIC_TIMER(iMS, "MC_TIMEOL", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, GET_HUD_COLOUR_FROM_TIME_REMAINING(iMS))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
					
	PROCESS_OUT_OF_BOUNDS_TIMER_LIST()
	
	IF IS_LOCAL_PLAYER_MULTIRULE_TIMER_RUNNING()
	AND NOT (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciMULTIRULE_TIMER_SUDDEN_DEATH) AND ARE_WE_CURRENTLY_IN_SUDDEN_DEATH())
		IF NOT bDrawnTimerAlready
			iMS = GET_LOCAL_PLAYER_MULTIRULE_TIMER_TIME_REMAINING()
			IF iMS >= 0
				IF NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
				AND ((NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE_HIDE_HUD)) 
				OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE_OVERRIDE_HIDEHUD_SHOW_MULTIRULE_TIMER))
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_FOOTBALL_HUD)
					AND NOT bDrawnTimerAlready
						bDrawnTimerAlready = TRUE
						DRAW_GENERIC_TIMER(iMS, "MC_TIMEVF", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, GET_HUD_COLOUR_FROM_TIME_REMAINING(iMS))
					ELSE
						IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_DRAW_DISTANCE_TO_CROSS_LINE)
						AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_SHOW_ALL_TEAM_MULTIRULE)	
							IF IS_STRING_EMPTY(g_FMMC_STRUCT.tl23CustomTimerString)
								bDrawnTimerAlready = TRUE
								DRAW_GENERIC_TIMER(iMS, "MC_TIMEOL", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, GET_HUD_COLOUR_FROM_TIME_REMAINING(iMS))
							ELSE
								bDrawnTimerAlready = TRUE
								DRAW_GENERIC_TIMER(iMS, g_FMMC_STRUCT.tl23CustomTimerString,  DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, GET_HUD_COLOUR_FROM_TIME_REMAINING(iMS), DEFAULT, DEFAULT, DEFAULT, DEFAULT, !IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_CustomTimerNameIsTextLabel))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TEAM_SCORE_ON_BLIPS)
		ASSERTLN("FMMC2020 - PROCESS_OBJECTIVE_LIMITS - SHOW_POINTS_ON_TEAM_BLIP")
		//SHOW_POINTS_ON_TEAM_BLIP()
	ENDIF					
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciHUD_ENABLE_TOGGLE_SHOW_BONUS_HUD) 
	AND NOT IS_LOCAL_PLAYER_SPECTATOR()
	AND bLocalPlayerPedOK
	
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
			TOGGLE_BIT(iLocalBoolCheck19, LBOOL19_SHOULD_EXTRA_HUD_BE_SHOWING)
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck19, LBOOL19_SHOULD_EXTRA_HUD_BE_SHOWING)
		
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetFive[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE5_INDIVIDUAL_SCORES)
			AND (IS_TEAM_ACTIVE(MC_playerBD[iPartToUse].iTeam) OR NOT HAS_TEAM_FAILED(MC_playerBD[iPartToUse].iTeam))
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetSeven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE7_HIDE_POINTS_UI_IN_SD)
					IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
						DRAW_INDIVIDUAL_PLAYER_SCORE()
					ENDIF
				ELSE
					DRAW_INDIVIDUAL_PLAYER_SCORE()
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iRuleBitsetSeven[g_CreatorsSelDetails.iRow], ciBS_RULE7_SHOW_INDIVIDUAL_KILLS)
			AND (IS_TEAM_ACTIVE(MC_playerBD[iPartToUse].iTeam) OR NOT HAS_TEAM_FAILED(MC_playerBD[iPartToUse].iTeam))
				DRAW_INDIVIDUAL_PLAYER_KILLS()
			ENDIF
			
		ENDIF
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetFive[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE5_INDIVIDUAL_SCORES)
		AND (IS_TEAM_ACTIVE(MC_playerBD[iPartToUse].iTeam) OR NOT HAS_TEAM_FAILED(MC_playerBD[iPartToUse].iTeam))
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetSeven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE7_HIDE_POINTS_UI_IN_SD)
				IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
					DRAW_INDIVIDUAL_PLAYER_SCORE()
				ENDIF
			ELSE
				DRAW_INDIVIDUAL_PLAYER_SCORE()
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iRuleBitsetSeven[g_CreatorsSelDetails.iRow], ciBS_RULE7_SHOW_INDIVIDUAL_KILLS)
		AND (IS_TEAM_ACTIVE(MC_playerBD[iPartToUse].iTeam) OR NOT HAS_TEAM_FAILED(MC_playerBD[iPartToUse].iTeam))
			DRAW_INDIVIDUAL_PLAYER_KILLS()
		ENDIF
	ENDIF
	
	INT iOtherTeamsHUDBS, iTeamScoreboards
		
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		IF iTeam = MC_playerBD[iPartToUse].iTeam
			RELOOP
		ENDIF
		
		IF  MC_serverBD_4.iCurrentHighestPriority[iTeam] >=  FMMC_MAX_RULES
			RELOOP
		ENDIF
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[MC_serverBD_4.iCurrentHighestPriority[iTeam]] = FMMC_TARGET_SCORE_OFF
			RELOOP
		ENDIF
			
		IF IS_BIT_SET(GET_TEAM_HUD_VISIBILITY_BITSET(iTeam), MC_serverBD_4.iCurrentHighestPriority[iTeam])
			SET_BIT(iOtherTeamsHUDBS, iTeam)
		ENDIF
			
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iTeamBitset, ciBS_TEAM_SHOW_SCOREBOARD_T0 + iTeam)
		AND WAS_TEAM_EVER_ACTIVE(iTeam)
			iTeamScoreboards++
		ENDIF
		
	ENDFOR
		
	IF iTeamScoreboards > 0
		IF NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
		AND NOT (IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_TARGET) AND ARE_WE_CURRENTLY_IN_SUDDEN_DEATH())
			BOOL bActiveTeamsValid = TRUE
			
			FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
				IF IS_TEAM_ACTIVE(iTeam)
				AND MC_serverBD_4.iCurrentHighestPriority[iTeam] >= FMMC_MAX_RULES
					bActiveTeamsValid = FALSE
				ENDIF
			ENDFOR
			
			IF bActiveTeamsValid
				INT iTeamReverse
				FOR iTeamReverse = MC_serverBD.iNumberOfTeams - 1 TO 0 STEP -1 
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iTeamBitset, ciBS_TEAM_SHOW_SCOREBOARD_T0 + iTeamReverse)
						RELOOP
					ENDIF
					IF NOT WAS_TEAM_EVER_ACTIVE(iTeamReverse)
						RELOOP
					ENDIF
					IF  MC_serverBD_4.iCurrentHighestPriority[iTeamReverse] >= FMMC_MAX_RULES
						RELOOP
					ENDIF
					IF HAS_TEAM_FAILED(iTeamReverse)
						RELOOP
					ENDIF
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamReverse].iRuleBitsetTen[MC_serverBD_4.iCurrentHighestPriority[iTeamReverse]], ciBS_RULE10_BLOCK_THIS_TEAM_SCORE_UI_ON_THIS_RULE)	
						DRAW_TEAM_SCOREBOARD_HUD(iTeamReverse, (iTeamScoreboards > 1))
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
	
	INT iTargetScore = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iTargetScore[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]]
	IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_POINTS_THRESHOLD
	AND MC_serverBD.iCurrentPlayerRule[MC_playerBD[iPartToUse].iTeam] != -1
		iTargetScore = MC_serverBD.iPlayerRuleLimit[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]
	ENDIF
	
	IF iTargetScore != FMMC_TARGET_SCORE_OFF
	OR iOtherTeamsHUDBS != 0
		
		IF NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE_HIDE_HUD)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetTen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE10_HIDE_BOTTOM_RIGHT_SCORE)
			IF (NOT IS_BIT_SET(MC_serverBD.iOptionsMenuBitSet, ciNO_POINTS_HUD) OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE3_FORCE_POINTS_HUD))
			OR iOtherTeamsHUDBS != 0
				FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
				
					IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= FMMC_MAX_RULES
						RELOOP
					ENDIF
					
					IF (IS_BIT_SET(iOtherTeamsHUDBS, iTeam)
					OR (NOT IS_BIT_SET(MC_serverBD.iOptionsMenuBitSet, ciNO_POINTS_HUD) OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ MC_playerBD[iPartToUse].iTeam ].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE3_FORCE_POINTS_HUD)))	
						BOOL bDisplay = TRUE
						
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetFour[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE4_COMBINE_TEAMS_HUD) 
						AND iTeam != 0
							bDisplay = FALSE
						ENDIF
						
						IF IS_CELLPHONE_CAMERA_IN_USE()
							IF IS_PHONE_ONSCREEN()
								SET_PHONE_UNDER_HUD_THIS_FRAME()
							ELSE
								SET_PHONE_UNDER_HUD_NO_RISE_THIS_FRAME()
							ENDIF
							SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()										
						ENDIF 

						IF IS_FAKE_MULTIPLAYER_MODE_SET()
							iTeam = MC_playerBD[iPartToUse].iTeam
						ENDIF
						
						IF bDisplay
						AND iTargetScore != FMMC_TARGET_SCORE_OFF
							
							IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_POINTS_THRESHOLD
								iScoreLimit = iTargetScore
							ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[MC_serverBD_4.iCurrentHighestPriority[iTeam]] = FMMC_TARGET_SCORE_PLAYER
								iScoreLimit = MC_serverBD.iNumberOfPlayingPlayers[iTeam]
							ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[MC_serverBD_4.iCurrentHighestPriority[iTeam]] = FMMC_TARGET_SCORE_PLAYER_HALF
								iScoreLimit = IMAX(FLOOR(MC_serverBD.iNumberOfPlayingPlayers[iTeam] / 2.0), 1)
							ELSE
								iScoreLimit = ROUND(GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[MC_serverBD_4.iCurrentHighestPriority[iTeam]], MC_ServerBD.iNumberOfPlayingPlayers[iTeam], MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam])*GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam]))
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetFifteen[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE15_DISPLAY_TOTAL_SCORE)
								iTeamScore = MC_serverBD.iTeamScore[iTeam]
							ELSE
								iTeamScore = MC_serverBD.iScoreOnThisRule[iTeam]
							ENDIF
							
							IF (SHOULD_USE_CUSTOM_STRING_FOR_HUD(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam])
							OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ MC_playerBD[iPartToUse].iTeam ].iRuleBitsetFour[MC_serverBD_4.iCurrentHighestPriority[ MC_playerBD[iPartToUse].iTeam ]], ciBS_RULE4_COMBINE_TEAMS_HUD))
							AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetEleven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE11_DONT_COMBINE_TEAM_SCORES)
								INT iTeam2
								FOR iTeam2 = 0 TO MC_serverBD.iNumberOfTeams - 1
									IF DOES_TEAM_LIKE_TEAM(iTeam, iTeam2)
									AND iTeam != iTeam2
										IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetFifteen[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE15_DISPLAY_TOTAL_SCORE)
											iTeamScore += MC_serverBD.iTeamScore[iTeam2]
										ELSE
											iTeamScore += MC_serverBD.iScoreOnThisRule[iTeam2]
										ENDIF
									ENDIF
								ENDFOR
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciALTERNATIVE_HOLDING_PACKAGE_SCORING)
								iTeamScore = GET_NUMBER_OF_OBJECTS_AT_TEAM_HOLDING(iTeam)
							ENDIF
							
							IF iScoreLimit > ciFMMC_MAX_HUD_PACKAGES
							OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE6_BLOCK_PACKAGE_HUD)
								g_b_ChangePlayerNameToTeamName = TRUE
								
								IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCustomScoreHUDColour[MC_serverBD_4.iCurrentHighestPriority[iTeam]] > -1
									eHudColour = INT_TO_ENUM(HUD_COLOURS, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCustomScoreHUDColour[MC_serverBD_4.iCurrentHighestPriority[iTeam]])
								ELIF SHOULD_USE_CUSTOM_STRING_FOR_HUD(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam])
									eHudColour = HUD_COLOUR_WHITE
								ELSE
									eHudColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, PlayerToUse)
								ENDIF
								
								INT iNoTeams
								FOR iNoTeams = 0 TO MC_serverBD.iNumberOfTeams - 1
									IF MC_serverBD_4.iCurrentHighestPriority[iNoTeams] >= FMMC_MAX_RULES
										RELOOP
									ENDIF
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iNoTeams].iRuleBitSetFive[MC_serverBD_4.iCurrentHighestPriority[iNoTeams]], ciBS_RULE5_ENABLE_BEAST_MODE)
										eHudColour = HUD_COLOUR_YELLOW
									ENDIF
								ENDFOR
								
								IF(iScoreLimit > 9)
									SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
								ENDIF
								
								IF SHOULD_USE_CUSTOM_STRING_FOR_HUD(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam])
									tl63LocalTitle = TEXT_LABEL_TO_STRING(g_FMMC_STRUCT.tl23CustomTargetString[iTeam])
								ElIF IS_BIT_SET(iCelebrationTeamNameIsNotLiteral, iTeam)
									tl63LocalTitle = TEXT_LABEL_TO_STRING(g_sMission_TeamName[iTeam])
									IF NOT IS_BIT_SET(g_iTeamNameBitSetPlayerName, iTeam)
										bLiteral = FALSE
									ENDIF
								ELSE
									tl63LocalTitle = g_sMission_TeamName[iTeam]
								ENDIF
								
								INT iPlayersOnTeam = MC_serverBD.iNumberOfPlayingPlayers[iTeam]
								
								IF IS_TEAM_ACTIVE(iTeam)
									IF iPlayersOnTeam > 0
										IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetFifteen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE15_SHOW_POINTS_HUD_AS_METER)
											DRAW_GENERIC_METER(iTeamScore, iScoreLimit, tl63LocalTitle, eHudColour, DEFAULT, HUDORDER_SEVENTHBOTTOM, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bLiteral)
										ELSE
											DRAW_GENERIC_BIG_DOUBLE_NUMBER(iTeamScore, iScoreLimit, tl63LocalTitle, DEFAULT, eHudColour, HUDORDER_SEVENTHBOTTOM, bLiteral)
										ENDIF
									ENDIF
								ENDIF
								
							ELSE 
								FOR iScore = 0 TO ciFMMC_MAX_HUD_PACKAGES - 1
									bMyTeam[iScore] = FALSE
									IF iScore < iTeamScore
										IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCustomScoreHUDColour[MC_serverBD_4.iCurrentHighestPriority[iTeam]] > -1
											eCarryColour[iScore] = INT_TO_ENUM(HUD_COLOURS, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCustomScoreHUDColour[MC_serverBD_4.iCurrentHighestPriority[iTeam]])
										ELSE
											eCarryColour[iScore] = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, PlayerToUse)
										ENDIF
										
										IF iTeam = MC_playerBD[iPartToUse].iTeam
										AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ MC_playerBD[iPartToUse].iTeam ].iRuleBitsetFour[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE4_COMBINE_TEAMS_HUD)
											bMyTeam[iScore] = TRUE
										ENDIF
									ELSE
										eCarryColour[iScore] = HUD_COLOUR_BLACK
									ENDIF
								ENDFOR
								
								IF SHOULD_USE_CUSTOM_STRING_FOR_HUD(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam])
									eHudColour = HUD_COLOUR_WHITE
								ELSE
									eHudColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, PlayerToUse)
								ENDIF
								
								g_b_ChangePlayerNameToTeamName = TRUE
								
								IF SHOULD_USE_CUSTOM_STRING_FOR_HUD(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam])
									tl63LocalTitle = TEXT_LABEL_TO_STRING(g_FMMC_STRUCT.tl23CustomTargetString[iTeam])
								ELSE
									IF IS_BIT_SET(iCelebrationTeamNameIsNotLiteral, iTeam)
										tl63LocalTitle = TEXT_LABEL_TO_STRING(g_sMission_TeamName[iTeam])
										bLiteral = FALSE
									ELSE
										tl63LocalTitle = g_sMission_TeamName[iTeam]
									ENDIF
								ENDIF	
																			
								IF MC_ServerBD.iScoreOnThisRule[MC_PlayerBD[iPartToUse].iTeam] != iScoreUICached[MC_PlayerBD[iPartToUse].iTeam]												
									iScoreUICached[MC_PlayerBD[iPartToUse].iTeam] = MC_ServerBD.iScoreOnThisRule[MC_PlayerBD[iPartToUse].iTeam]
								ENDIF
																	
								IF MC_serverBD_4.iObjMissionLogic[ MC_playerBD[iPartToUse].iTeam ] != FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
									DRAW_ONE_PACKAGES_EIGHT_HUD(iScoreLimit, tl63LocalTitle, bLiteral, eCarryColour[0], eCarryColour[1], eCarryColour[2], eCarryColour[3], eCarryColour[4], eCarryColour[5], eCarryColour[6], eCarryColour[7],
												DEFAULT, eHudColour, bMyTeam[0], bMyTeam[1], bMyTeam[2], bMyTeam[3], bMyTeam[4], bMyTeam[5], bMyTeam[6], bMyTeam[7], HUDORDER_SEVENTHBOTTOM, TRUE)						
								ELSE
									DRAW_ONE_PACKAGES_EIGHT_HUD(iScoreLimit, tl63LocalTitle, bLiteral, eCarryColour[0], eCarryColour[1], eCarryColour[2], eCarryColour[3], eCarryColour[4], eCarryColour[5], eCarryColour[6], eCarryColour[7],
												DEFAULT, eHudColour, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_SEVENTHBOTTOM, TRUE)						
								ENDIF
								
							ENDIF
						ENDIF
						
						IF IS_FAKE_MULTIPLAYER_MODE_SET()
						OR SHOULD_USE_CUSTOM_STRING_FOR_HUD(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam])
							BREAKLOOP
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
		
		IF iTargetScore != FMMC_TARGET_SCORE_OFF
			IF NOT bDrawnTimerAlready
			AND HAS_NET_TIMER_STARTED(MC_serverBD_3.tdLimitTimer)
				iMS = GET_MISSION_END_TIME_FROM_MENU_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionTimeLimit) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdLimitTimer)
				IF iMS > 0
					IF NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE_HIDE_HUD)
						bDrawnTimerAlready = TRUE
						DRAW_GENERIC_TIMER(iMS, "MC_TIMEL", DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, GET_HUD_COLOUR_FROM_TIME_REMAINING(iMS), DEFAULT, DEFAULT, DEFAULT, GET_HUD_COLOUR_FROM_TIME_REMAINING(iMS))
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_BIT_SET(MC_serverBD.iOptionsMenuBitSet, ciNO_POINTS_HUD)
				IF g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit != ciFMMC_MISSION_END_POINTS_IGNORE
					IF MC_serverBD.iWinningTeam != TEAM_INVALID
						IF NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
							IF(iScoreLimit > 9)
								SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
							ENDIF

							IF MC_serverBD.iWinningTeam = MC_playerBD[iPartToUse].iTeam
								DRAW_GENERIC_BIG_DOUBLE_NUMBER(MC_serverBD.iTeamScore[MC_serverBD.iWinningTeam], g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit, "MC_MSCRLIMY")
							ELSE
								DRAW_GENERIC_BIG_DOUBLE_NUMBER(MC_serverBD.iTeamScore[MC_serverBD.iWinningTeam], g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit, "MC_MSCRLIME")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_TIME		
		IF NOT bDrawnTimerAlready
			IF bPlayerToUseOK
				IF HAS_NET_TIMER_STARTED(MC_Playerbd[iPartToUse].tdMissionTime)
					iMS = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_Playerbd[iPartToUse].tdMissionTime)
					IF iMS > 0
						IF NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
						AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE_HIDE_HUD)	
							bDrawnTimerAlready = TRUE
							DRAW_GENERIC_TIMER(iMS, "MC_MISSTIME")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetFour[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE4_SHOW_ALL_TEAMS_PLAYERS_PLAYING_HUD)
		IF NOT g_b_OnLeaderboard = TRUE
			DRAW_FMMC_PLAYERS_REMAINING_HUD()
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE6_SHOW_ALL_TEAMS_PLAYERS_LIVES_HUD)
		IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE7_HIDE_LIVES_HUD_ON_SUDDEN_DEATH)
				DRAW_FMMC_TEAM_LIVES_REMAINING_HUD()
			ENDIF
		ELSE
			DRAW_FMMC_TEAM_LIVES_REMAINING_HUD()
		ENDIF
	ENDIF
		
	IF g_FMMC_STRUCT.iShowPackageHudForTeam != -1
		DRAW_FMMC_OBJECTS_LEFT_TO_DEFEND(g_FMMC_STRUCT.iShowPackageHudForTeam)
	ENDIF

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Bottom-Right Healthbars ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to drawing entity health bars in the bottom right -----------------------------------------------------------------------------------------------------
// ##### See HUDUtility for additional functionality-----------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC DRAW_ENTITY_HEALTHBAR(ENTITY_HEALTHBAR_VARS &sEntityHealthbarVars, INT iTeam, INT iRule)
	
	IF NOT SHOULD_DRAW_ENTITY_HEALTHBAR(sEntityHealthbarVars, iTeam, iRule)
		EXIT
	ENDIF
	
	IF SHOULD_HEALTHBAR_BE_HIDDEN_AT_CURRENT_PERCENTAGE(sEntityHealthbarVars)
		EXIT
	ENDIF
	
	INT iPercentage = sEntityHealthbarVars.iPercentageToDisplay
	IF IS_BIT_SET(sEntityHealthbarVars.iEntityHealthbarConfigBS, ciEntityHealthbarConfigBS_InvertPercentage)
		iPercentage = 100 - sEntityHealthbarVars.iPercentageToDisplay
	ENDIF
	
	// Draw the healthbar
	DRAW_GENERIC_METER(iPercentage, 100, sEntityHealthbarVars.tlHealthbarTitle, sEntityHealthbarVars.iHealthbarColour, -1, HUDORDER_DONTCARE, DEFAULT, DEFAULT, sEntityHealthbarVars.bIsPlayerName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sEntityHealthbarVars.bIsLiteralString, sEntityHealthbarVars.ePercentageMeterLine)
	
ENDPROC

PROC PROCESS_DRAWING_HEALTHBARS()
	
	IF iCurrentHealthBars = 0
		EXIT
	ENDIF
	
	INT i
	
	IF SHOULD_HIDE_THE_HUD_THIS_FRAME() 
		FOR i = 0 TO ciFMMC_MAX_ENTITY_HEALTHBARS - 1
			IF sHealthBars[i].eiEntity = NULL
				RELOOP
			ENDIF
			RESET_ENTITY_HEALTHBAR_STRUCT(sHealthBars[i])
			iCurrentHealthBars--
		ENDFOR
		EXIT
	ENDIF
	
	INT iTeam = GET_LOCAL_PLAYER_TEAM(TRUE)
	INT iRule = GET_TEAM_CURRENT_RULE(iTeam, FALSE)
	
	FOR i = 0 TO ciFMMC_MAX_ENTITY_HEALTHBARS - 1
		IF sHealthBars[i].eiEntity = NULL
			RELOOP
		ENDIF
		DRAW_ENTITY_HEALTHBAR(sHealthBars[i], iTeam, iRule)
		IF GET_FRAME_COUNT() > sHealthBars[i].iFrameAdded
			RESET_ENTITY_HEALTHBAR_STRUCT(sHealthBars[i])
			iCurrentHealthBars--
		ENDIF
		
		IF iCurrentHealthBars = 0
			BREAKLOOP
		ENDIF
	ENDFOR
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Main HUD Processing
// ##### Description: Called every frame to process the HUD
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_HUD_EVERY_FRAME()
	// FMMC2020 - Functionalise and put in a MAIN HUD processing function. --------------------------------- start	
	INT iTeamlives, iplayerlives, ihudLives
	HUD_COLOURS eHudColour
	IF IS_MISSION_TEAM_LIVES()
		iTeamlives = GET_FMMC_MISSION_LIVES_FOR_TEAM(MC_playerBD[iPartToUse].iteam)
		IF iTeamlives != ciFMMC_UNLIMITED_LIVES
			IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_FINISHED)
				HANDLE_LIVES_HELP()
				ihudLives = (iTeamlives - GET_TEAM_DEATHS(MC_playerBD[iPartToUse].iTeam))
				eHudColour = HUD_COLOUR_WHITE
				IF ihudLives <= 0
					eHudColour = HUD_COLOUR_RED
					ihudLives = 0
					#IF IS_DEBUG_BUILD
					IF IS_BIT_SET(MC_serverBD.iDebugBitset, SBDEBUG_DONTFAILWHENOUTOFLIVES)
						ihudLives = (iTeamlives - GET_TEAM_DEATHS(MC_playerBD[iPartToUse].iTeam))
					ENDIF
					#ENDIF
				ENDIF
				IF SHOULD_SHOW_TEAM_LIVES()
				AND NOT IS_MY_TEAM_LIVES_SHOWING()				
					IF ihudLives = 0
						DRAW_GENERIC_BIG_NUMBER(ihudLives,"MC_TLIVES",30000,eHudColour,HUDORDER_DONTCARE,FALSE,"",eHudColour,FALSE,HUDFLASHING_FLASHWHITE,1)
					ELSE
						DRAW_GENERIC_BIG_NUMBER(ihudLives,"MC_TLIVES",-1,eHudColour,HUDORDER_DONTCARE,FALSE,"",eHudColour)
					ENDIF
					IF IS_BIT_SET(g_TransitionSessionNonResetVars.iAirlockHUDBitset, ciFMMCAirlockHUDBS_ShowLives)
						g_TransitionSessionNonResetVars.iTeamLivesRemainingForHUD = ihudLives
					ENDIF
					SET_BIT(g_TransitionSessionNonResetVars.iAirlockHUDBitset, ciFMMCAirlockHUDBS_LivesShown)
				ENDIF
			ENDIF
		ENDIF
					
	ELIF IS_MISSION_COOP_TEAM_LIVES()
		iTeamlives = GET_FMMC_MISSION_LIVES_FOR_COOP_TEAMS()
		IF iTeamlives != ciFMMC_UNLIMITED_LIVES
			IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_FINISHED)
				HANDLE_LIVES_HELP()
				ihudLives = (iTeamlives - GET_COOP_TEAMS_TOTAL_DEATHS(MC_playerBD[iPartToUse].iTeam))
				eHudColour = HUD_COLOUR_WHITE
				IF ihudLives <= 0
					eHudColour = HUD_COLOUR_RED
					ihudLives = 0
					#IF IS_DEBUG_BUILD
					IF IS_BIT_SET(MC_serverBD.iDebugBitset, SBDEBUG_DONTFAILWHENOUTOFLIVES)
						ihudLives = (iTeamlives - GET_COOP_TEAMS_TOTAL_DEATHS(MC_playerBD[iPartToUse].iTeam))
					ENDIF
					#ENDIF
				ENDIF
				IF SHOULD_SHOW_TEAM_LIVES()
				AND NOT IS_MY_TEAM_LIVES_SHOWING()
					IF ihudLives = 0
						DRAW_GENERIC_BIG_NUMBER(ihudLives,"MC_TLIVES",30000,eHudColour,HUDORDER_DONTCARE,FALSE,"",eHudColour,FALSE,HUDFLASHING_FLASHWHITE,1)
					ELSE
						DRAW_GENERIC_BIG_NUMBER(ihudLives,"MC_TLIVES",-1,eHudColour,HUDORDER_DONTCARE,FALSE,"",eHudColour)
					ENDIF
					IF IS_BIT_SET(g_TransitionSessionNonResetVars.iAirlockHUDBitset, ciFMMCAirlockHUDBS_ShowLives)
						g_TransitionSessionNonResetVars.iTeamLivesRemainingForHUD = ihudLives
					ENDIF
					SET_BIT(g_TransitionSessionNonResetVars.iAirlockHUDBitset, ciFMMCAirlockHUDBS_LivesShown)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		iplayerlives = GET_FMMC_MISSION_LIVES_FOR_LOCAL_PLAYER()
		IF iplayerlives != ciFMMC_UNLIMITED_LIVES 
			IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_FINISHED)
				HANDLE_LIVES_HELP()
				ihudLives = (iplayerlives - MC_PlayerBD[iPartToUse].iNumPlayerDeaths)
				eHudColour = HUD_COLOUR_WHITE
				IF ihudLives <= 0
					eHudColour = HUD_COLOUR_RED
					ihudLives = 0
					#IF IS_DEBUG_BUILD
					IF IS_BIT_SET(MC_serverBD.iDebugBitset, SBDEBUG_DONTFAILWHENOUTOFLIVES)
						ihudLives = (iplayerlives - MC_PlayerBD[iPartToUse].iNumPlayerDeaths)
					ENDIF
					#ENDIF
				ENDIF
				IF iplayerlives > 1
					SET_BIT(iLocalBoolCheck,LBOOL_HAD_MORE_THAN_1_LIFE)
				ENDIF
				IF SHOULD_SHOW_TEAM_LIVES()
					IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
						IF IS_BIT_SET(iLocalBoolCheck,LBOOL_HAD_MORE_THAN_1_LIFE)
						AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE6_SHOW_ALL_TEAMS_PLAYERS_LIVES_HUD)
							IF ihudLives = 0
								DRAW_GENERIC_BIG_NUMBER(ihudLives,"MC_LIVES",30000,eHudColour,HUDORDER_DONTCARE,FALSE,"",eHudColour,FALSE,HUDFLASHING_FLASHWHITE,1)
							ELSE
								DRAW_GENERIC_BIG_NUMBER(ihudLives,"MC_LIVES",-1,eHudColour,HUDORDER_DONTCARE,FALSE,"",eHudColour)
							ENDIF
							SET_BIT(g_TransitionSessionNonResetVars.iAirlockHUDBitset, ciFMMCAirlockHUDBS_LivesShown)
							IF IS_BIT_SET(g_TransitionSessionNonResetVars.iAirlockHUDBitset, ciFMMCAirlockHUDBS_ShowLives)
								g_TransitionSessionNonResetVars.iTeamLivesRemainingForHUD = ihudLives
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_SPECTATOR_HUD_HIDDEN()
	AND NOT g_bMissionEnding
		PROCESS_LOCAL_ALTITUDE_HUD()
		DRAW_VEHICLE_MIN_SPEED_EXPLOSION_CHECK_HUD()
	ENDIF
	
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	IF iTeam >= 0 AND iTeam < FMMC_MAX_TEAMS
		INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
		IF iRule >= 0 AND iRule < FMMC_MAX_RULES	
			PROCESS_PLAYER_RESPAWN_BAR_HUD(iTeam,iRule)
		ENDIF
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES	
		MAINTAIN_STAMINA_BAR()		
	ENDIF
	
	PROCESS_GET_AND_DELIVER_FILTER()
	
	PROCESS_PLAY_CUSTOM_SHARD()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_ALL_PLAYERS_HEALTH_BARS_ON_TEAM)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_ALL_PLAYERS_HEALTH_BARS_ON_TEAM_FOR_VEHICLE)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TEAM_SETTINGS_HEALTH_BARS)
		PROCESS_PLAYER_HEALTH_BAR_DISPLAY()
	ENDIF
	
	PROCESS_PLAYER_HEALTH_BAR_DISPLAY_FOR_RULE()
	
	IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer)
	OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer) AND (PlayerToUse != LocalPlayer)
	OR g_eSpecialSpectatorState != SSS_IDLE	
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciUSE_CIRCLE_SCORE_HUD)
		AND NOT (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciSD_DISABLE_CIRCULAR_TIMER) AND ARE_WE_CURRENTLY_IN_SUDDEN_DEATH())
		AND ((NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()) OR (g_bMissionEnding AND NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_ENDING_HUD_UPDATED)))
			IF (bPlayerToUseOK OR bIsAnySpectator)
			AND GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
				PROCESS_CIRCLE_SCORE_HUD()	
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciUSE_CIRCLE_HUD_TIMER)
		AND (bPlayerToUseOK OR bIsAnySpectator)
		AND GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
		AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
			PROCESS_CIRCLE_TIMER()
			IF (NOT SHOULD_HIDE_THE_HUD_THIS_FRAME() 
			 OR (g_bMissionEnding AND NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_ENDING_HUD_UPDATED)))
			AND NOT (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciSD_DISABLE_CIRCULAR_TIMER)
			 AND ARE_WE_CURRENTLY_IN_SUDDEN_DEATH())
				PROCESS_CIRCLE_TIMER_HUD()
			ENDIF
			PROCESS_CIRCLE_TIMER_SOUND()
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_CIRCLE_HUD_SCORES_WITH_TIMER)
		AND NOT (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciSD_DISABLE_CIRCULAR_TIMER) AND ARE_WE_CURRENTLY_IN_SUDDEN_DEATH())
		AND ((NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()) OR (g_bMissionEnding AND NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_ENDING_HUD_UPDATED)))
			IF (bPlayerToUseOK OR (bIsAnySpectator OR g_eSpecialSpectatorState != SSS_IDLE))
			AND GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
				PROCESS_CIRCLE_SCORE_HUD_WITH_TIMER()
			ENDIF
		ENDIF
		
		PROCESS_ALL_TEAM_MULTIRULE_TIMER_HUD()
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_USE_FLOATING_SCORE)
			TARGET_DRAW_FLOATING_SCORES(sFloatingScore)
		ENDIF
	ENDIF
			
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iMissionModeBit, ciMISSION_USING_ORBITAL_CANNON)
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
	AND g_bMissionPlacedOrbitalCannon
	AND NOT IS_SCREEN_FADING_OUT()		
		DRAW_PLACED_PED_TRACKERS()
		DRAW_PLACED_VEHICLE_TRACKERS()
	ENDIF
	
	SHOW_PED_AGGRO_HELP_TEXT()
	
ENDPROC

PROC PROCESS_SIMULATE_SP_HUD()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_SimulateSinglePlayerHUD)
		EXIT
	ENDIF
	
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
	
	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Securoserv App
// ##### Description: Functions for displaying the securoserv app
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_SECUROSERV_APP_OPEN()
	RETURN GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("appSecuroHack")) > 0 AND IS_PHONE_ONSCREEN(TRUE)
ENDFUNC

FUNC BOOL CLEAN_UP_SECUROSERV_HACKING_LOOP()
	IF NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
		RETURN FALSE
	ENDIF
	
	STOP_SOUND(iHackingLoopSoundID)
	RELEASE_SOUND_ID(iHackingLoopSoundID)
	iHackingLoopSoundID = -1
	CLEAR_BIT(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
	RETURN TRUE
ENDFUNC

PROC RESET_SECUROSERV_APP()
	iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
	iHackPercentage = 0
	IF CLEAN_UP_SECUROSERV_HACKING_LOOP()
		IF IS_SECUROSERV_APP_OPEN()
			PLAY_SOUND_FRONTEND(-1, "Hack_Stop", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
		ENDIF
	ENDIF
	RESET_NET_TIMER(tdHackingComplete)
	CLEAR_BIT(iLocalBoolCheck35, LBOOL35_SECUROHACK_ACTIVE)
ENDPROC

PROC SET_SECUROSERV_HACKING_ACTIVE()
	SET_BIT(iLocalBoolCheck35, LBOOL35_SECUROHACK_ACTIVE)
	CLEAR_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_SECUROHACK_REQUEST_ACTIVATION)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciFORCE_OBJECTIVE_MID_POINT_ON_HACK)
		IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[iLocalPart].iTeam], MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam])
			BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam], MC_playerBD[iLocalPart].iTeam)
		ENDIF
	ENDIF
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_SECUROSERV_HACK())
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_SECUROSERV_HACK(TRUE))
		CLEAR_HELP()
	ENDIF
ENDPROC

PROC PROCESS_SECUROSERV_APP()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
		EXIT
	ENDIF
	
	IF SHOULD_HIDE_THE_HUD_THIS_FRAME()
		EXIT
	ENDIF
	
	IF NOT bLocalPlayerPedOk
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_SECUROHACK_REQUEST_STOP)
		IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_SECUROHACK_ACTIVE)
			RESET_SECUROSERV_APP()
		ENDIF
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_SECUROSERV_HACK())
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_SECUROSERV_HACK(TRUE))
			CLEAR_HELP()
		ENDIF
		CLEAR_BIT(iLocalBoolCheck35, LBOOL35_SECUROHACK_REQUEST_STOP)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck35, LBOOL35_SECUROHACK_ACTIVE)
	
		IF NOT IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_SECUROHACK_REQUEST_ACTIVATION)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_SECUROSERV_HACK())
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_SECUROSERV_HACK(TRUE))
				CLEAR_HELP()
			ENDIF
			EXIT
		ENDIF
	
		IF IS_FAKE_MULTIPLAYER_MODE_SET()
			SET_SECUROSERV_HACKING_ACTIVE()
			PRINTLN("[SECUROHACK] PROCESS_SECUROSERV_APP - Test Mode, setting as active")
		ELSE
			
			IF IS_PED_BEING_STUNNED(LocalPlayerPed)
			OR IS_PLAYER_RESPAWNING(LocalPlayer)
			OR IS_PED_DEAD_OR_DYING(LocalPlayerPed)
				EXIT
			ENDIF
			
			IF IS_SECUROSERV_APP_OPEN()
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_SECUROSERV_HACK())
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_SECUROSERV_HACK(TRUE))
					CLEAR_HELP()
				ENDIF
				SET_SECUROSERV_HACKING_ACTIVE()
				PRINTLN("[SECUROHACK] PROCESS_SECUROSERV_APP - App Open, setting as active")
			ELSE
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
				
				IF IS_PHONE_ONSCREEN()
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_SECUROSERV_HACK())
						CLEAR_HELP()
					ENDIF
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_SECUROSERV_HACK(TRUE))
						PRINT_HELP_FOREVER(GET_HELP_TEXT_FOR_SECUROSERV_HACK(TRUE))
					ENDIF
				ELIF NOT IS_BIT_SET(BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPDUMMYAPP0)
				AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("appSecuroHack")) = 0
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_SECUROSERV_HACK(TRUE))
						CLEAR_HELP()
					ENDIF
					
					IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
						LAUNCH_CELLPHONE_APPLICATION(AppDummyApp0, TRUE, TRUE, FALSE)
						PRINTLN("[SECUROHACK] PROCESS_SECUROSERV_APP - Launching App")
					ELSE
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_SECUROSERV_HACK())
							PRINT_HELP_FOREVER(GET_HELP_TEXT_FOR_SECUROSERV_HACK())
						ENDIF
					ENDIF
				ENDIF
				
				EXIT
				
			ENDIF
		ENDIF
	ENDIF
	
	//App Active
	
	BOOL bPlaySounds = IS_SECUROSERV_APP_OPEN()
	
	IF NOT bPlaySounds
		CLEAN_UP_SECUROSERV_HACKING_LOOP()
	ENDIF	
	
	IF NOT HAS_NET_TIMER_STARTED(tdHackingComplete)
		IF iHackPercentage >= 100
			IF bPlaySounds
				IF CLEAN_UP_SECUROSERV_HACKING_LOOP()
					iHackSuccessSoundProgress++
					PLAY_SOUND_FRONTEND(-1, "Hack_Stop", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
				ENDIF
			ENDIF
				
			PRINTLN("[SECUROHACK] PROCESS_SECUROSERV_APP - Hack Complete")
			START_NET_TIMER(tdHackingComplete)
			iHackStage = ciSECUROSERV_HACK_STAGE_HACK_COMPLETE
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_SECUROHACK_LOSING_SIGNAL)
			AND iHackStage != ciSECUROSERV_HACK_STAGE_LOSING_SIGNAL
				iHackStage = ciSECUROSERV_HACK_STAGE_LOSING_SIGNAL
				CLEAN_UP_SECUROSERV_HACKING_LOOP()
			ELIF iHackStage = ciSECUROSERV_HACK_STAGE_LOSING_SIGNAL
				IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_SECUROHACK_ACTIVE)
					iHackStage = ciSECUROSERV_HACK_STAGE_HACKING
				ELSE
					iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
				ENDIF
			ENDIF
		
			IF iHackStage = ciSECUROSERV_HACK_STAGE_HACKING
				IF NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
					IF iHackingLoopSoundID = -1
						iHackingLoopSoundID = GET_SOUND_ID()
					ENDIF
					
					PLAY_SOUND_FRONTEND(iHackingLoopSoundID, "Hack_Loop", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
					SET_BIT(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
				ENDIF
				SET_VARIABLE_ON_SOUND(iHackingLoopSoundID, "percentageComplete", TO_FLOAT(iHackPercentage) / 100)
			ENDIF
		ENDIF		
	ELSE
		IF iHackPercentage >= 100
		 	IF HAS_NET_TIMER_EXPIRED(tdHackingComplete, ciHACKING_COMPLETE_DELAY)
				IF bPlaySounds
				AND iHackSuccessSoundProgress > -1
					IF NOT IS_BIT_SET(iHackSuccessSoundPlayedBS, iHackSuccessSoundProgress)	
						PLAY_SOUND_FRONTEND(-1, "Hack_Complete", "DLC_IE_SVM_Voltic2_Hacking_Sounds")	
						SET_BIT(iHackSuccessSoundPlayedBS, iHackSuccessSoundProgress)
					ENDIF
				ENDIF
				iHackPercentage = 0
			ENDIF
		ENDIF
		
		IF HAS_NET_TIMER_EXPIRED(tdHackingComplete, ciHACKING_COMPLETE_TIME)
			IF iHackStage != ciSECUROSERV_HACK_STAGE_HACKING
			AND iHackStage != ciSECUROSERV_HACK_STAGE_LOSING_SIGNAL
				IF bPlaySounds
					IF NOT IS_HACK_STOP_SOUND_BLOCKED()
						PLAY_SOUND_FRONTEND(-1, "Hack_Stop", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
					ENDIF
				ENDIF
				
				HANG_UP_AND_PUT_AWAY_PHONE()
				RESET_SECUROSERV_APP()
			ENDIF
			RESET_NET_TIMER(tdHackingComplete)
		ENDIF
	ENDIF
	
ENDPROC



// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Custom HUDs
// ##### Description: Bottom Right HUDs content can make in the creator
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT _GET_MAX_SCORE_FOR_CUSTOM_HUD(INT iTeam, INT iRule, INT iCriteria, INT iCustomHud, INT &iMaxScore)

	IF iMaxScore != -1
		RETURN iMaxScore
	ENDIF
	IF HAS_NET_TIMER_STARTED(tdCustomHUDReset)
		RETURN -1
	ENDIF
	
	SWITCH iCriteria
		CASE FMMC_CUSTOM_HUD_CRITERIA_RULE_OBJECTIVE
			IF MC_serverBD.iNumObjHighestPriority[iTeam] > 0
				iMaxScore = MC_serverBD.iNumObjHighestPriority[iTeam]
			ELIF MC_serverBD.iNumPlayerRuleHighestPriority[iTeam] > 0
				iMaxScore = MC_serverBD.iNumPlayerRuleHighestPriority[iTeam]
			ELIF MC_serverBD.iNumLocHighestPriority[iTeam] > 0
				iMaxScore = MC_serverBD.iNumLocHighestPriority[iTeam]
			ELIF MC_serverBD.iNumVehHighestPriority[iTeam] > 0
				iMaxScore = MC_serverBD.iNumVehHighestPriority[iTeam]
			ELIF MC_serverBD.iNumPedHighestPriority[iTeam] > 0
				iMaxScore = MC_serverBD.iNumPedHighestPriority[iTeam]
			ENDIF
		BREAK
		CASE FMMC_CUSTOM_HUD_CRITERIA_RULE_SCORE
		CASE FMMC_CUSTOM_HUD_CRITERIA_TOTAL_RULE_SCORE
			INT iTargetScore 
			iTargetScore = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[iRule]
			IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_POINTS_THRESHOLD
			AND MC_serverBD.iCurrentPlayerRule[iTeam] != -1
				iTargetScore = MC_serverBD.iPlayerRuleLimit[MC_serverBD.iCurrentPlayerRule[iTeam]][iTeam]
			ENDIF
			iMaxScore = ROUND(GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(iTargetScore, MC_ServerBD.iNumberOfPlayingPlayers[iTeam], MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam]) * GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING(iTeam, iRule))
		BREAK
		CASE FMMC_CUSTOM_HUD_CRITERIA_PREREQS
			iMaxScore = FMMC_COUNT_SET_BITS_LONG_BITSET(g_FMMC_STRUCT.sCustomHUDs[iCustomHud].iPreReqs)
		BREAK
		DEFAULT
			ASSERTLN("[CustomHUD ", iCustomHud, "] _GET_MAX_SCORE_FOR_CUSTOM_HUD - Invalid Criteria: ", g_FMMC_STRUCT.sCustomHUDs[iCustomHud].iCriteria)
			PRINTLN("[CustomHUD ", iCustomHud, "] _GET_MAX_SCORE_FOR_CUSTOM_HUD - Invalid Criteria: ", g_FMMC_STRUCT.sCustomHUDs[iCustomHud].iCriteria)
			iMaxScore = -1
		BREAK
	ENDSWITCH
	
	RETURN iMaxScore

ENDFUNC

FUNC INT GET_MAX_SCORE_FOR_CUSTOM_HUD(INT iTeam, INT iRule, INT iCustomHud)
	
	INT iCriteria = g_FMMC_STRUCT.sCustomHUDs[iCustomHud].iCriteria
	IF iCriteria = FMMC_CUSTOM_HUD_CRITERIA_PREREQS_AND_OBJ
		iCriteria = FMMC_CUSTOM_HUD_CRITERIA_RULE_OBJECTIVE
	ENDIF
	
	RETURN _GET_MAX_SCORE_FOR_CUSTOM_HUD(iTeam, iRule, iCriteria, iCustomHud, sCustomHUDStructs[iCustomHud].iMaxScore)
	
ENDFUNC

FUNC INT GET_MAX_SCORE_FOR_CUSTOM_HUD_SECONDARY(INT iTeam, INT iRule, INT iCustomHud)
	
	INT iCriteria = -1
	IF g_FMMC_STRUCT.sCustomHUDs[iCustomHud].iCriteria = FMMC_CUSTOM_HUD_CRITERIA_PREREQS_AND_OBJ
		iCriteria = FMMC_CUSTOM_HUD_CRITERIA_PREREQS
	ENDIF
	
	IF iCriteria = -1
		RETURN 0
	ENDIF
	
	RETURN _GET_MAX_SCORE_FOR_CUSTOM_HUD(iTeam, iRule, iCriteria, iCustomHud, sCustomHUDStructs[iCustomHud].iMaxScoreSecondary)
	
ENDFUNC

FUNC INT _GET_CURRENT_SCORE_FOR_CUSTOM_HUD(INT iTeam, INT iCriteria, INT iCustomHud, INT iMaxScore)
	
	IF iMaxScore = -1
		ASSERTLN("[CustomHUD ", iCustomHud, "] _GET_CURRENT_SCORE_FOR_CUSTOM_HUD - CALL GET_MAX_SCORE_FOR_CUSTOM_HUD FIRST")
		PRINTLN("[CustomHUD ", iCustomHud, "] _GET_CURRENT_SCORE_FOR_CUSTOM_HUD - CALL GET_MAX_SCORE_FOR_CUSTOM_HUD FIRST")
		RETURN 0
	ENDIF
	
	INT i, iCompletedPrereqs[ciPREREQ_Bitsets]
	
	SWITCH iCriteria
		CASE FMMC_CUSTOM_HUD_CRITERIA_RULE_OBJECTIVE
			IF MC_serverBD.iNumObjHighestPriority[iTeam] > 0
				IF MC_serverBD_4.iObjMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
					RETURN MC_serverBD.iNumObjHighestPriorityHeld[iTeam]
				ELSE
					RETURN iMaxScore - MC_serverBD.iNumObjHighestPriority[iTeam]
				ENDIF
			ELIF MC_serverBD.iNumPlayerRuleHighestPriority[iTeam] > 0
				RETURN iMaxScore - MC_serverBD.iNumPlayerRuleHighestPriority[iTeam]
			ELIF MC_serverBD.iNumLocHighestPriority[iTeam] > 0
				RETURN iMaxScore - MC_serverBD.iNumLocHighestPriority[iTeam]
			ELIF MC_serverBD.iNumVehHighestPriority[iTeam] > 0
				IF MC_serverBD_4.iVehMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
					RETURN MC_serverBD.iNumVehHighestPriorityHeld[iTeam]
				ELSE
					RETURN iMaxScore - MC_serverBD.iNumVehHighestPriority[iTeam]
				ENDIF
			ELIF MC_serverBD.iNumPedHighestPriority[iTeam] > 0
				IF MC_serverBD_4.iPedMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
					RETURN MC_serverBD.iNumPedHighestPriorityHeld[iTeam]
				ELSE
					RETURN iMaxScore - MC_serverBD.iNumPedHighestPriority[iTeam]
				ENDIF
			ENDIF
		BREAK
		CASE FMMC_CUSTOM_HUD_CRITERIA_RULE_SCORE
			RETURN MC_serverBD.iScoreOnThisRule[iTeam]
		CASE FMMC_CUSTOM_HUD_CRITERIA_TOTAL_RULE_SCORE
			RETURN MC_serverBD.iTeamScore[iTeam]
		BREAK
		CASE FMMC_CUSTOM_HUD_CRITERIA_PREREQS
			FOR i = 0 TO ciPREREQ_Bitsets - 1
				iCompletedPrereqs[i] = g_FMMC_STRUCT.sCustomHUDs[iCustomHud].iPreReqs[i] & MC_playerBD[iPartToUse].iPrerequisiteBS[i]
			ENDFOR
			RETURN FMMC_COUNT_SET_BITS_LONG_BITSET(iCompletedPrereqs)
		BREAK
	ENDSWITCH
	
	ASSERTLN("[CustomHUD ", iCustomHud, "] _GET_CURRENT_SCORE_FOR_CUSTOM_HUD - Invalid Criteria: ", iCriteria)
	PRINTLN("[CustomHUD ", iCustomHud, "] _GET_CURRENT_SCORE_FOR_CUSTOM_HUD - Invalid Criteria: ", iCriteria)
	RETURN -1
	
ENDFUNC

FUNC INT GET_CURRENT_SCORE_FOR_CUSTOM_HUD(INT iTeam, INT iCustomHud)
	
	IF HAS_NET_TIMER_STARTED(tdCustomHUDReset)
		RETURN sCustomHUDStructs[iCustomHud].iScore
	ENDIF
	
	INT iCriteria = g_FMMC_STRUCT.sCustomHUDs[iCustomHud].iCriteria
	IF iCriteria = FMMC_CUSTOM_HUD_CRITERIA_PREREQS_AND_OBJ
		iCriteria = FMMC_CUSTOM_HUD_CRITERIA_RULE_OBJECTIVE
	ENDIF
	
	sCustomHUDStructs[iCustomHud].iScore = _GET_CURRENT_SCORE_FOR_CUSTOM_HUD(iTeam, iCriteria, iCustomHud, sCustomHUDStructs[iCustomHud].iMaxScore)
	RETURN sCustomHUDStructs[iCustomHud].iScore
	
ENDFUNC

FUNC INT GET_CURRENT_SCORE_FOR_CUSTOM_HUD_SECONDARY(INT iTeam, INT iCustomHud)
	
	IF HAS_NET_TIMER_STARTED(tdCustomHUDReset)
		RETURN sCustomHUDStructs[iCustomHud].iScoreSecondary
	ENDIF
	
	INT iCriteria = -1
	IF g_FMMC_STRUCT.sCustomHUDs[iCustomHud].iCriteria = FMMC_CUSTOM_HUD_CRITERIA_PREREQS_AND_OBJ
		iCriteria = FMMC_CUSTOM_HUD_CRITERIA_PREREQS
	ENDIF
	
	IF iCriteria = -1
		RETURN 0
	ENDIF
	
	sCustomHUDStructs[iCustomHud].iScoreSecondary = _GET_CURRENT_SCORE_FOR_CUSTOM_HUD(iTeam, iCriteria, iCustomHud, sCustomHUDStructs[iCustomHud].iMaxScoreSecondary)
	RETURN sCustomHUDStructs[iCustomHud].iScoreSecondary
	
ENDFUNC


FUNC HUD_COLOURS GET_HUD_COLOUR_FOR_CUSTOM_HUD(INT iTeam, INT iCustomHud)
	IF sCustomHUDStructs[iCustomHud].eHUDColour != HUD_COLOUR_PURE_WHITE
		RETURN sCustomHUDStructs[iCustomHud].eHUDColour
	ENDIF
	
	SWITCH g_FMMC_STRUCT.sCustomHUDs[iCustomHud].iColour
		CASE FMMC_CUSTOM_HUD_COLOUR_DEFAULT
			sCustomHUDStructs[iCustomHud].eHUDColour = HUD_COLOUR_WHITE
		BREAK
		CASE FMMC_CUSTOM_HUD_COLOUR_MATCH_RULE_ENTITY
			IF MC_serverBD.iNumObjHighestPriority[iTeam] > 0
				IF MC_serverBD_4.iObjMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_KILL
				OR MC_serverBD_4.iObjMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_DAMAGE
				OR MC_serverBD_4.iObjMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_DESTROY
					sCustomHUDStructs[iCustomHud].eHUDColour = HUD_COLOUR_RED
				ELSE
					sCustomHUDStructs[iCustomHud].eHUDColour = HUD_COLOUR_GREEN
				ENDIF
			ELIF MC_serverBD.iNumPlayerRuleHighestPriority[iTeam] > 0
				sCustomHUDStructs[iCustomHud].eHUDColour = HUD_COLOUR_WHITE
			ELIF MC_serverBD.iNumLocHighestPriority[iTeam] > 0
				sCustomHUDStructs[iCustomHud].eHUDColour = HUD_COLOUR_YELLOW
			ELIF MC_serverBD.iNumVehHighestPriority[iTeam] > 0
				IF MC_serverBD_4.iVehMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_KILL
				OR MC_serverBD_4.iVehMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_DAMAGE
				OR MC_serverBD_4.iVehMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_DESTROY
					sCustomHUDStructs[iCustomHud].eHUDColour = HUD_COLOUR_RED
				ELSE
					sCustomHUDStructs[iCustomHud].eHUDColour = HUD_COLOUR_BLUEDARK
				ENDIF
			ELIF MC_serverBD.iNumPedHighestPriority[iTeam] > 0
				IF MC_serverBD_4.iPedMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_KILL
				OR MC_serverBD_4.iPedMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_DAMAGE
				OR MC_serverBD_4.iPedMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_DESTROY
					sCustomHUDStructs[iCustomHud].eHUDColour = HUD_COLOUR_RED
				ELSE
					sCustomHUDStructs[iCustomHud].eHUDColour = HUD_COLOUR_FRIENDLY
				ENDIF
			ENDIF
		BREAK
		CASE FMMC_CUSTOM_HUD_COLOUR_ENEMY
			sCustomHUDStructs[iCustomHud].eHUDColour = HUD_COLOUR_RED
		BREAK
		DEFAULT
			ASSERTLN("[CustomHUD ", iCustomHud, "] GET_HUD_COLOUR_FOR_CUSTOM_HUD - Invalid Colour: ", g_FMMC_STRUCT.sCustomHUDs[iCustomHud].iColour)
			PRINTLN("[CustomHUD ", iCustomHud, "] GET_HUD_COLOUR_FOR_CUSTOM_HUD - Invalid Colour: ", g_FMMC_STRUCT.sCustomHUDs[iCustomHud].iColour)
			sCustomHUDStructs[iCustomHud].eHUDColour = HUD_COLOUR_PURE_WHITE
		BREAK
	ENDSWITCH
	
	RETURN sCustomHUDStructs[iCustomHud].eHUDColour
ENDFUNC

PROC DRAW_CUSTOM_HUD_NUMBER(INT iScore, INT iMaxScore, HUD_COLOURS eHUDColour, TEXT_LABEL_23 tl23Label)

	DRAW_GENERIC_BIG_DOUBLE_NUMBER(iScore, iMaxScore , tl23Label, DEFAULT, eHUDColour)
	
ENDPROC

PROC DRAW_CUSTOM_HUD_METER(INT iScore, INT iMaxScore, HUD_COLOURS eHUDColour, TEXT_LABEL_23 tl23Label)

	DRAW_GENERIC_METER(iScore, iMaxScore, tl23Label, eHUDColour)
	
ENDPROC

PROC DRAW_CUSTOM_HUD_PACKAGE_FILLED(INT iScore, INT iMaxScore, HUD_COLOURS eHUDColour, TEXT_LABEL_23 tl23Label)

	IF iMaxScore > ciFMMC_MAX_HUD_PACKAGES
		DRAW_CUSTOM_HUD_NUMBER(iScore, iMaxScore, eHUDColour, tl23Label)
		EXIT
	ENDIF
	
	INT i
	HUD_COLOURS eBoxColours[ciFMMC_MAX_HUD_PACKAGES]
	FOR i = 0 TO ciFMMC_MAX_HUD_PACKAGES - 1
		IF i < iScore
			eBoxColours[i] = eHUDColour
		ELSE
			eBoxColours[i] = HUD_COLOUR_GREYLIGHT
		ENDIF
	ENDFOR
	
	DRAW_ONE_PACKAGES_EIGHT_HUD(iMaxScore, tl23Label, DEFAULT, 
				eBoxColours[0], eBoxColours[1], eBoxColours[2], eBoxColours[3], eBoxColours[4], eBoxColours[5], eBoxColours[6], eBoxColours[7])
	
ENDPROC

PROC DRAW_CUSTOM_HUD_PACKAGE_CROSSED(INT iScore, INT iMaxScore, HUD_COLOURS eHUDColour, TEXT_LABEL_23 tl23Label)
	
	IF iMaxScore > ciFMMC_MAX_HUD_PACKAGES
		DRAW_CUSTOM_HUD_NUMBER(iScore, iMaxScore, eHUDColour, tl23Label)
		EXIT
	ENDIF
	
	INT i
	HUD_COLOURS eBoxColours[ciFMMC_MAX_HUD_PACKAGES]
	BOOL bCrossed[ciFMMC_MAX_HUD_PACKAGES]
	FOR i = 0 TO ciFMMC_MAX_HUD_PACKAGES - 1
		eBoxColours[i] = eHUDColour
		bCrossed[i] = i < iScore
	ENDFOR
	
	DRAW_ONE_PACKAGES_EIGHT_HUD(iMaxScore, tl23Label, DEFAULT, 
				eBoxColours[0], eBoxColours[1], eBoxColours[2], eBoxColours[3], eBoxColours[4], eBoxColours[5], eBoxColours[6], eBoxColours[7], 
				DEFAULT, DEFAULT,
				bCrossed[0], bCrossed[1], bCrossed[2], bCrossed[3], bCrossed[4], bCrossed[5], bCrossed[6], bCrossed[7])
	
	
ENDPROC

PROC DRAW_CUSTOM_HUD_PACKAGE_PREREQ(INT iScore, INT iMaxScore, INT iScoreSecondary, INT iMaxScoreSecondary, HUD_COLOURS eHUDColour, TEXT_LABEL_23 tl23Label)
	
	IF iMaxScore > ciFMMC_MAX_HUD_PACKAGES
	OR iMaxScoreSecondary > ciFMMC_MAX_HUD_PACKAGES
		DRAW_CUSTOM_HUD_NUMBER(iScore, iMaxScore, eHUDColour, tl23Label)
		DRAW_CUSTOM_HUD_NUMBER(iScoreSecondary, iMaxScoreSecondary, eHUDColour, tl23Label)
		EXIT
	ENDIF
	
	INT i
	HUD_COLOURS eBoxColours[ciFMMC_MAX_HUD_PACKAGES]
	BOOL bCrossed[ciFMMC_MAX_HUD_PACKAGES]
	FOR i = 0 TO ciFMMC_MAX_HUD_PACKAGES - 1
		IF i < iScoreSecondary
			eBoxColours[i] = eHUDColour
		ELSE
			eBoxColours[i] = HUD_COLOUR_GREYLIGHT
		ENDIF
		bCrossed[i] = i < iScore
	ENDFOR
	
	DRAW_ONE_PACKAGES_EIGHT_HUD(iMaxScore, tl23Label, DEFAULT, 
				eBoxColours[0], eBoxColours[1], eBoxColours[2], eBoxColours[3], eBoxColours[4], eBoxColours[5], eBoxColours[6], eBoxColours[7], 
				DEFAULT, DEFAULT,
				bCrossed[0], bCrossed[1], bCrossed[2], bCrossed[3], bCrossed[4], bCrossed[5], bCrossed[6], bCrossed[7])
	
	
ENDPROC

PROC PROCESS_CUSTOM_HUD()
	
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF iOldPriority[iTeam] != MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	OR IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		PRINTLN("[CustomHUD] PROCESS_CUSTOM_HUD - Starting timer to reset custom HUDs")
		REINIT_NET_TIMER(tdCustomHUDReset)
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(tdCustomHUDReset)
	AND HAS_NET_TIMER_EXPIRED(tdCustomHUDReset, ciCUSTOM_HUD_RESET_TIMER)
		RESET_CUSTOM_HUD_STRUCTS()
		RESET_NET_TIMER(tdCustomHUDReset)
		PRINTLN("[CustomHUD] PROCESS_CUSTOM_HUD - Resetting custom HUDs")
	ENDIF
	
	IF SHOULD_HIDE_THE_HUD_THIS_FRAME() 
		EXIT
	ENDIF
	
	INT i
	FOR i = 0 TO FMMC_MAX_CUSTOM_HUDS - 1
		
		IF g_FMMC_STRUCT.sCustomHUDs[i].iCriteria = FMMC_CUSTOM_HUD_CRITERIA_NONE
			RELOOP
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCustomHUDBS[iRule], i)
			RELOOP
		ENDIF
		
		INT iMaxScore = GET_MAX_SCORE_FOR_CUSTOM_HUD(iTeam, iRule, i)
		IF iMaxScore <= 0
			RELOOP
		ENDIF
		INT iScore = GET_CURRENT_SCORE_FOR_CUSTOM_HUD(iTeam, i)
		INT iMaxScoreSecondary = GET_MAX_SCORE_FOR_CUSTOM_HUD_SECONDARY(iTeam, iRule, i)
		INT iScoreSecondary = GET_CURRENT_SCORE_FOR_CUSTOM_HUD_SECONDARY(iTeam, i)
		HUD_COLOURS eHUDColour = GET_HUD_COLOUR_FOR_CUSTOM_HUD(iTeam, i)
		TEXT_LABEL_23 tl23Label = GET_CUSTOM_STRING_LIST_TEXT_LABEL(g_FMMC_STRUCT.sCustomHUDs[i].iCustomStringTextLabel)
		
		SWITCH g_FMMC_STRUCT.sCustomHUDs[i].iType
			CASE FMMC_CUSTOM_HUD_TYPE_NUMBER
				DRAW_CUSTOM_HUD_NUMBER(iScore, iMaxScore, eHUDColour, tl23Label)
			BREAK
			CASE FMMC_CUSTOM_HUD_TYPE_PACKAGE_FILLED
				DRAW_CUSTOM_HUD_PACKAGE_FILLED(iScore, iMaxScore, eHUDColour, tl23Label)
			BREAK
			CASE FMMC_CUSTOM_HUD_TYPE_PACKAGE_CROSSED
				DRAW_CUSTOM_HUD_PACKAGE_CROSSED(iScore, iMaxScore, eHUDColour, tl23Label)
			BREAK
			CASE FMMC_CUSTOM_HUD_TYPE_PACKAGE_PREREQ
				IF g_FMMC_STRUCT.sCustomHUDs[i].iCriteria = FMMC_CUSTOM_HUD_CRITERIA_PREREQS_AND_OBJ
					DRAW_CUSTOM_HUD_PACKAGE_PREREQ(iScore, iMaxScore, iScoreSecondary, iMaxScoreSecondary, eHUDColour, tl23Label)
				ENDIF
			BREAK
			CASE FMMC_CUSTOM_HUD_TYPE_METER
				DRAW_CUSTOM_HUD_METER(iScore, iMaxScore, eHUDColour, tl23Label)
			BREAK
		ENDSWITCH
		
	ENDFOR
	
ENDPROC

PROC PROCESS_HUD()

	PROCESS_SIMULATE_SP_HUD()
	
	PROCESS_OBJECTIVE_LIMITS_HUD()
	
	PROCESS_DRAWING_HEALTHBARS()
	
	IF IS_MISSION_READY_TO_PROCESS_OBJECTIVE_TEXT()
		PROCESS_OBJECTIVE_TEXT()
	ENDIF
	
	PROCESS_FORCED_HINT_CAM()
	
	PROCESS_SECUROSERV_APP()
	
	PROCESS_CUSTOM_HUD()
	
	IF g_bReenableHUDCheckJohnThinksIsBad //Bin this Pack 2 2022 if no issues
		IF IS_LOCAL_PLAYER_PLAYING_CUTSCENE()	
			EXIT
		ENDIF
	ENDIF
	
	PROCESS_HUD_EVERY_FRAME()
				
	PROCESS_NEAREST_TARGET_HUD()
	
	BAG_CAPACITY__PROCESS_CAPACITY_HUD()
	
	PROCESS_SUPPORT_SNIPER_TIMER()
	
	PROCESS_OBJ_CARRYING_HUD()
	
	PROCESS_MISSION_EQUIPMENT_COLLECTION_HUD()
	
	PROCESS_WANTED_DELAY_HUD()

ENDPROC
