// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Main --------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### Processes the Mission Controller Client and Server State Machines 
// ##### All additional processing should be added to the appropriate header 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020 -------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
#IF IS_DEBUG_BUILD
USING "FM_Mission_Controller_DebugProcessing_2020.sch"
#ENDIF
#IF NOT IS_DEBUG_BUILD
USING "FM_Mission_Controller_EventProcessing_2020.sch"
#ENDIF 

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Cache at the start of the frame -------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### Main Local Player Caching Function
// ##### Called by all connected players every frame. 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC INIT_TEMP_FUNCTION_CALLBACKS()
	amIWaitingForPlayersToEnterVeh = &AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH
	shouldCleanupObjectTransformationNow = &SHOULD_CLEANUP_OBJECT_TRANSFORMATION_NOW
	cleanupObjectTransformations = &CLEANUP_OBJECT_TRANSFORMATIONS
	detachAnyPortablePickups = &DETACH_ANY_PORTABLE_PICKUPS
	
	//FMMC2020 - John's Mass Callback Mess
	getNextSpawningPedAfterVehicleSpawn = &GET_NEXT_SPAWNING_PED_AFTER_VEHICLE_SPAWN
	setPedToSpawnNext = &SET_PED_TO_SPAWN_NEXT
	getVehicleBeingDroppedOff = &GET_VEHICLE_BEING_DROPPED_OFF
	detachAndResetAnyPickups = &DETACH_AND_RESET_ANY_PICKUPS
	detachPortablePickupFromSpecificPed = &DETACH_PORTABLE_PICKUP_FROM_SPECIFIC_PED
	processRemotePlayerThermiteObjects = &PROCESS_REMOTE_PLAYER_THERMITE_OBJECTS
	processRemotePlayerCashGrabObjects = &PROCESS_REMOTE_PLAYER_CASH_GRAB_OBJECTS
	shouldVehicleCountAsHeldForTeam = &SHOULD_VEHICLE_COUNT_AS_HELD_FOR_TEAM
	drawExplosionTimerHud = &DRAW_EXPLOSION_TIMER_HUD
	spookPedsAtCoordInRadius = &SPOOK_PEDS_AT_COORD_IN_RADIUS
	processEveryFramePlayerActionMode = &PROCESS_EVERY_FRAME_PLAYER_ACTION_MODE
	fillTeamNames = &FILL_TEAM_NAMES
	isPlayerInRequiredLocationVehicle = &IS_PLAYER_IN_REQUIRED_LOCATION_VEHICLE
	isTeammateReadyToDeliverSpecificVehicle = &IS_TEAMMATE_READY_TO_DELIVER_SPECIFIC_VEHICLE
	getNumberOfFreeSeatsInHighPriorityVehiclesForTeam = &GET_NUMBER_OF_FREE_SEATS_IN_HIGH_PRIORITY_VEHICLES_FOR_TEAM
	setLocateToDisplay = &SET_LOCATE_TO_DISPLAY
	processDisplacementArenaInteriorMc = &PROCESS_DISPLACEMENT_ARENA_INTERIOR_MC
	setLocateToFadeOut = &SET_LOCATE_TO_FADE_OUT
	updateLocateFadeStruct = &UPDATE_LOCATE_FADE_STRUCT
	taskPerformAmbientAnimation = &TASK_PERFORM_AMBIENT_ANIMATION
	createDrillMinigameProps = &CREATE_DRILL_MINIGAME_PROPS
	shouldPedRespawnNowCallback = &SHOULD_PED_RESPAWN_NOW
ENDPROC

PROC PROCESS_PRE_FRAME_NATIVE_CACHING()
	
	PROCESS_CACHING_NETWORK_HOST()
	
	PROCESS_CACHING_LOCAL_PLAYER_STATE()
	
	PROCESS_CACHING_LOCAL_PARTICIPANT_STATE()
	
	PROCESS_CACHING_LOCAL_PLAYER_INTERIOR_STATE()
	
	CACHE__LOBBY_HOST_PLAYER_INDEX()
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Client Processing Helpers
// ##### Description: Functions used by client processing that currently cannot go in another header.
// ##### These should be split up and moved to the correct headers eventually.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_SERVER_CHECKPOINT_INITIALISATION_FOR_TEAM(INT iTeam)
	
	IF NOT MC_SHOULD_WE_START_FROM_CHECKPOINT()
		EXIT
	ENDIF
	
	INT iCheckpoint = MC_GET_MISSION_STARTING_CHECKPOINT()
		
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_CHECKPOINT_INITIALISATION_FOR_TEAM - Team ", iTeam, " Restarting at checkpoint: ", iCheckpoint)

	SETUP_RULES_FOR_CHECKPOINT(iTeam, iCheckpoint)
	
	IF g_TransitionSessionNonResetVars.iRetryMultiRuleTimeStamp[iTeam] > 0
		IF g_TransitionSessionNonResetVars.iRetryMultiRuleTimeStamp_Checkpoint[iTeam] = iCheckpoint + 1 // 0 is default (none), 1 is checkpoint 1, 2 is checkpoint 2, etc
			PRINTLN("[INIT_SERVER] PROCESS_SERVER_CHECKPOINT_INITIALISATION_FOR_TEAM - Team ", iTeam, " Retry Multirule Timestamp: ", g_TransitionSessionNonResetVars.iRetryMultiRuleTimeStamp[iTeam])
			SET_BIT(MC_serverBD.iCheckpointBitset, SB_CP_QUICK_RESTART_MRT_FOR_TEAM_0 + iTeam)
		ENDIF
	ENDIF
		
ENDPROC

PROC PROCESS_SERVER_TEAM_INITIALISATION(INT iTeam, INT &iLocationBS, INT &iPedBS[FMMC_MAX_PEDS_BITSET], INT &iVehicleBS, INT &iObjectBS, INT &iTrainBS)

	PROCESS_SERVER_PRE_PRIORITY_INITIALISATION_FOR_TEAM(iTeam, iLocationBS, iPedBS, iVehicleBS, iObjectBS, iTrainBS)
	
	IF iTeam >= MC_serverBD.iNumberOfTeams
		EXIT
	ENDIF
	
	IF NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
	AND NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
		SET_VARIABLE_TEAM_LIVES(iTeam, GET_VARIABLE_NUMBER_OF_TEAM_LIVES_TO_START_WITH(iTeam))
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_TEAM_INITIALISATION - Team ", iTeam, " Variable Lives (Creator): ", GET_VARIABLE_TEAM_LIVES(iTeam))
	ENDIF
	
	PROCESS_SERVER_CHECKPOINT_INITIALISATION_FOR_TEAM(iTeam)
			
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_TEAM_INITIALISATION - Team ", iTeam, " Current Highest Priority: ", MC_serverBD_4.iCurrentHighestPriority[iTeam])
			
	PROCESS_SERVER_POST_PRIORITY_INITIALISATION_FOR_TEAM(iTeam)
	
ENDPROC

PROC INITIALISE_VEHICLE_SWAPS_FROM_RULE_DATA()

	INT iTeam = 0
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1		
		INT iRule = 0		
		FOR iRule = 0 TO FMMC_MAX_RULES - 1			
			MC_serverBD_4.sVehicleSwaps[iTeam][iRule].mnVehicleModelSwap = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].mnVehicleModelSwap[iRule]			
			MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleTurretSwap = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehicleTurretSwap[iRule]
			MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleArmourSwap = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehicleArmourSwap[iRule]
			MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleAirCounterSwap = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehicleAirCounterSwap[iRule]
			MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleExhaustSwap = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehicleExhaustSwap[iRule]
			MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleBombBaySwap = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehicleBombBaySwap[iRule]
		ENDFOR
	ENDFOR
	
ENDPROC

FUNC BOOL INIT_SERVER_DATA()

	IF NOT SETUP_PLAYSTATS_CREATE_MATCH_HISTORY_ID_2()
		PRINTLN("[INIT_SERVER] INIT_SERVER_DATA - Waiting for Play Stats Match History")
		RETURN FALSE
	ENDIF
	
	PRINTLN("[INIT_SERVER] INIT_SERVER_DATA - Initialising Server Data")
	
	MC_serverBD.iSessionScriptEventKey = NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()) / GET_RANDOM_INT_IN_RANGE(1, 10)
	PRINTLN("[INIT_SERVER] INIT_SERVER_DATA - Session Script Event Key: ", MC_serverBD.iSessionScriptEventKey)
	
	PROCESS_SERVER_TEAM_NUMBER_INITIALISATION()
	
	PROCESS_SERVER_NON_RESET_TRANSITION_VARS_INITIALISATION()
	
	IF MC_serverBD.iNumberOfTeams <= 0
		PRINTLN("[INIT_SERVER] INIT_SERVER_DATA - Invalid Team Count")
		RETURN FALSE
	ENDIF
	
	INT iRule, iTeam
	INT iPlayerRuleBS, iLocationBS, iPedBS[FMMC_MAX_PEDS_BITSET], iVehicleBS, iObjectBS, iTrainBS
	
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		
		PROCESS_SERVER_PRE_RULE_INITIALISATION(iTeam)
	
		FOR iRule = 0 TO FMMC_MAX_RULES - 1
			PROCESS_SERVER_RULE_INITIALISATION(iTeam, iRule, iPlayerRuleBS)
		ENDFOR
	
		PROCESS_SERVER_TEAM_INITIALISATION(iTeam, iLocationBS, iPedBS, iVehicleBS, iObjectBS, iTrainBS)
		
	ENDFOR
	
	PROCESS_SERVER_DYNOPROP_INITIALISATION()

	PROCESS_SERVER_PARTICIPANT_INITIALISATION()
	
	MC_ServerBD.iEndCutscene = g_FMMC_STRUCT.iEndCutscene
	PRINTLN("[INIT_SERVER] INIT_SERVER_DATA - End Cutscene: ", MC_ServerBD.iEndCutscene)
	
	IF MC_DOES_END_CUTSCENE_USE_RANDOM_CONCAT_BS(MC_ServerBD.iEndCutscene)
		MC_SET_END_CUTSCENE_RANDOM_CONCAT_BS(MC_ServerBD.iEndCutscene, MC_ServerBD.iEndCutsceneConcatBS)		
		PRINTLN("[INIT_SERVER] INIT_SERVER_DATA - iEndCutsceneConcatBS: ", MC_ServerBD.iEndCutsceneConcatBS)
	ENDIF
	
	PRINTLN("[INIT_SERVER] INIT_SERVER_DATA - Match Time ID: ", g_MissionControllerserverBD_LB.iMatchTimeID)
	PRINTLN("[INIT_SERVER] INIT_SERVER_DATA - Hash MAC: ", g_MissionControllerserverBD_LB.iHashMAC)
	
	MC_serverBD.sServerFMMC_EOM.bAllowedToVote = FALSE
	
	IF g_FMMC_STRUCT.iVehicleRespawnPool > 0
		MC_ServerBD.iVehicleRespawnPool = GET_ENTITY_STARTING_RESPAWN_LIVES(g_FMMC_STRUCT.iVehicleRespawnPool)
		PRINTLN("[INIT_SERVER] INIT_SERVER_DATA - Vehicle Respawn Pool: ", MC_ServerBD.iVehicleRespawnPool)
	ENDIF
	
	PROCESS_SERVER_CORONA_SETTINGS_INITIALISATION()
	
	#IF IS_DEBUG_BUILD
		PROCESS_SERVER_DEBUG_INITIALISATION()
	#ENDIF
	
	BROADCAST_FMMC_UPDATE_DOOR_LINKED_OBJS_EVENT(iDoorHasLinkedEntityBS)
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionTimeLimit != ciFMMC_MISSION_END_TIME_IGNORE
		REINIT_NET_TIMER(MC_serverBD_3.tdLimitTimer)
	ENDIF
	
	PROCESS_SERVER_MAX_LOOP_SIZE_INITIALISATION()
	
	INITIALISE_CELEBRATION_SCREEN_DATA_SERVER(MC_serverBD_2.sCelebServer)
	
	INITIALISE_VEHICLE_SWAPS_FROM_RULE_DATA()
	
	MC_ServerBD_4.piGangBossID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
	PRINTLN("[INIT_SERVER] INIT_SERVER_DATA - Gang Boss: ", NATIVE_TO_INT(MC_ServerBD_4.piGangBossID))
	
	MC_serverBD_3.iSyncedMediaPlayerStartPoint = GET_RANDOM_INT_IN_RANGE(0, 6) * 30000
	
	RETURN TRUE
	
ENDFUNC	

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Client Processing ----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### Client State Machine Processing. 
// ##### Called by all connected players every frame. 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets the Client Game State
/// PARAMS:
///    iState - The state to move to
PROC SET_MC_CLIENT_GAME_STATE(INT iState)
	
	IF MC_playerBD[iLocalPart].iGameState != iState
		
		//[FMMC2020] MOVE TO DEBUG
		#IF IS_DEBUG_BUILD
			SWITCH iState
				CASE GAME_STATE_INI 	
					PRINTLN("[RCC MISSION] Previous Game State = ")
					PRINT_CLIENT_GAME_STATE(GET_MC_CLIENT_GAME_STATE(iLocalPart))
					NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION] SET_CLIENT_GAME_STATE  >>> GAME_STATE_INI  ",tl31ScriptName) NET_NL()
				BREAK
				CASE GAME_STATE_INTRO_CUTSCENE 	
					PRINTLN("[RCC MISSION] Previous Game State = ")
					PRINT_CLIENT_GAME_STATE(GET_MC_CLIENT_GAME_STATE(iLocalPart))
					NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION] SET_CLIENT_GAME_STATE  >>> GAME_STATE_INTRO_CUTSCENE  ",tl31ScriptName) NET_NL()
				BREAK
				CASE GAME_STATE_FORCED_TRIP_SKIP	
					PRINTLN("[RCC MISSION] Previous Game State = ")
					PRINT_CLIENT_GAME_STATE(GET_MC_CLIENT_GAME_STATE(iLocalPart))
					NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION] SET_CLIENT_GAME_STATE  >>> GAME_STATE_FORCED_TRIP_SKIP  ",tl31ScriptName) NET_NL()
				BREAK
				CASE GAME_STATE_SOLO_PLAY	
					PRINTLN("[RCC MISSION] Previous Game State = ")
					PRINT_CLIENT_GAME_STATE(GET_MC_CLIENT_GAME_STATE(iLocalPart))
					NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION] SET_CLIENT_GAME_STATE  >>> GAME_STATE_SOLO_PLAY  ",tl31ScriptName) NET_NL()
				BREAK
				CASE GAME_STATE_TEAM_PLAY	
					PRINTLN("[RCC MISSION] Previous Game State = ")
					PRINT_CLIENT_GAME_STATE(GET_MC_CLIENT_GAME_STATE(iLocalPart))
					NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION] SET_CLIENT_GAME_STATE  >>> GAME_STATE_TEAM_PLAY  ",tl31ScriptName) NET_NL()
				BREAK
				CASE GAME_STATE_CAMERA_BLEND	
					PRINTLN("[RCC MISSION] Previous Game State = ")
					PRINT_CLIENT_GAME_STATE(GET_MC_CLIENT_GAME_STATE(iLocalPart))
					NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION] SET_CLIENT_GAME_STATE  >>> GAME_STATE_CAMERA_BLEND  ",tl31ScriptName) NET_NL()
				BREAK
				CASE GAME_STATE_RUNNING	
					PRINTLN("[RCC MISSION] Previous Game State = ")
					PRINT_CLIENT_GAME_STATE(GET_MC_CLIENT_GAME_STATE(iLocalPart))
					NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION] SET_CLIENT_GAME_STATE  >>> GAME_STATE_RUNNING  ",tl31ScriptName) NET_NL()
				BREAK
				CASE GAME_STATE_MISSION_OVER	
					PRINTLN("[RCC MISSION] Previous Game State = ")
					PRINT_CLIENT_GAME_STATE(GET_MC_CLIENT_GAME_STATE(iLocalPart))
					NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION] SET_CLIENT_GAME_STATE  >>> GAME_STATE_MISSION_OVER  ",tl31ScriptName) NET_NL()
				BREAK
				CASE GAME_STATE_LEAVE
					PRINTLN("[RCC MISSION] Previous Game State = ")
					PRINT_CLIENT_GAME_STATE(GET_MC_CLIENT_GAME_STATE(iLocalPart))
					NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION] SET_CLIENT_GAME_STATE  >>> GAME_STATE_LEAVE  ",tl31ScriptName) NET_NL()
				BREAK
				CASE GAME_STATE_END		
					PRINTLN("[RCC MISSION] Previous Game State = ")
					PRINT_CLIENT_GAME_STATE(GET_MC_CLIENT_GAME_STATE(iLocalPart))
					NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION] SET_CLIENT_GAME_STATE  >>> GAME_STATE_END  ",tl31ScriptName) NET_NL()
				BREAK
				DEFAULT
					SCRIPT_ASSERT("SET_CLIENT_GAME_STATE Invalid option MISSION CREATOR")
				BREAK
			ENDSWITCH
			
			DEBUG_PRINTCALLSTACK()
		#ENDIF
		
		//[FMMC2020] change to having a "On Change" function call for each state (as required)
		IF iState = GAME_STATE_RUNNING
			//DEFINE_LBD_NAMES_ARRAY(lbdVars, MC_serverBD_2.tParticipantNames, TRUE)
			SET_DAILY_OBJECTIVE_TICKERS_ENABLED_ON_MISSION(TRUE)
			SET_EVERYONE_IGNORE_PLAYER(LocalPlayer, FALSE)
			GRAB_GAMER_HANDLES(lbdVars)
			Unpause_Objective_Text()
			SET_DISABLE_RANK_UP_MESSAGE(FALSE)

			IF g_bVSMission
				IF bIsSCTV
					SET_SCTV_TICKER_SCORE_ON()
				ENDIF
			ENDIF
			
			IF HAVE_SPECTATOR_COORDS_BEEN_OVERRIDDEN()
				CLEAR_SPECTATOR_OVERRIDE_COORDS()
			ENDIF

			IF NOT g_bMissionReadyForAM_VEHICLE_SPAWN
				g_bMissionReadyForAM_VEHICLE_SPAWN = TRUE //AM_VEHICLE_SPAWN will only run if in a mission if this global is true
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== Mission === g_bMissionReadyForAM_VEHICLE_SPAWN = TRUE")
				#ENDIF
			ENDIF
			
			IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_PERSONAL_VEH_INITIAL_SPAWN) 
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_PERSONAL_VEH_CHECKPOINT_0)
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_PERSONAL_VEH_CHECKPOINT_1)
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciOptionsBS17_DisablePersonalVehOnCheckpoint_2)
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_DisablePersonalVehOnCheckpoint_3))
			AND NOT IS_PERSONAL_VEHICLE_FUNCTIONALITY_DISABLED_IN_MISSIONS(TRUE)
				DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION(FALSE)
			ENDIF

			
			SET_BIT(MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		ENDIF
		
		MC_playerBD[iLocalPart].iGameState = iState
		
	ENDIF
	
ENDPROC

PROC PROCESS_CLIENT_GAME_STATE_INI()

	//[FMMC2020] move all of these calls into _Init or the appropriate headers' game state ini processing function
	INT i, iTeam
	
	LOAD_MC_TEXT_BLOCK()
			
	LOAD_PARTICLE_FX()
	
	LOAD_ACTION_ANIMS()
	
	IF NOT IS_PED_INJURED(localPlayerPed)
	AND IS_PED_SWIMMING(localPlayerPed)
		CLEAR_PED_TASKS(localPlayerPed)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_TRAINS_AND_TRAMS)
		SET_RANDOM_TRAINS(FALSE)
		DELETE_ALL_TRAINS() //Non-mission only
		PRINTLN("[GAME_STATE_INI] PROCESS_CLIENT_GAME_STATE_INI - Clearing Trains")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_DisableAmbientBoats)
		SET_RANDOM_BOATS_MP(FALSE)
		PRINTLN("[GAME_STATE_INI] PROCESS_CLIENT_GAME_STATE_INI - Clearing Ambient Boats")
	ENDIF
	
	g_bEndOfMissionCleanUpHUD = FALSE
	g_bCelebrationScreenIsActive = FALSE
	g_bIgnoreCelebrationScreenPV = FALSE
	
	// INIT HUD
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRACE_STYLE_INTRO)
	OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset2, ciBS2_STOP_GO_HUD)
		REQUEST_COUNTDOWN_UI(cduiIntro)
	ENDIF
	   
	INT iPart = -1
	FMMC_PLAYER_STATE sEmptyStruct
	DO_PARTICIPANT_LOOP_FLAGS eFlags = DPLF_EXCLUDE_LOCAL_PART
	WHILE DO_PARTICIPANT_LOOP(iPart, eFlags)
		
		FMMC_PLAYER_STATE sPlayerState = sEmptyStruct
		FILL_FMMC_PLAYER_STATE_STRUCT(sPlayerState, iPart)
		
		PROCESS_CUSTOM_PLAYER_BLIPS(sPlayerState, MC_playerBD[iPart].iTeam)
	
	ENDWHILE
	                
	//Hide every thing
	MC_HIDE_ALL_MISSION_ENTITIES_LOCALLY_FOR_STRAND_MISSION()
	
	IF IS_CORONA_READY_TO_START_WITH_JOB()		
		CLIENT_ASSIGN_LATE_CORONA_DATA()		
	ENDIF
	
	//Wait till the flags are ready for the script to progress			
	IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_REQUEST_LOAD_FMMC_MODELS_STAGGERED_DONE)
	AND IS_A_STRAND_MISSION_BEING_INITIALISED()
		IF NOT bIsLocalPlayerHost // If we're the server, this stuff will happen in the server version of this down below (RUN_ALL_STAGGERED_MC_HOST_FUNCTIONS)
			PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_INI] REQUEST_LOAD_FMMC_MODELS_STAGGERED being called")
			IF RUN_ALL_STAGGERED_MC_CLIENT_FUNCTIONS( iRelGroupStaggerVars, iNumTimesCalledRelGroupFunc )
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_INI] RUN_ALL_STAGGERED_MC_CLIENT_FUNCTIONS RETURNING TRUE") 
				SET_BIT(iLocalBoolCheck8, LBOOL8_REQUEST_LOAD_FMMC_MODELS_STAGGERED_DONE)
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_INI] Not calling RUN_ALL_STAGGERED_MC_CLIENT_FUNCTIONS RETURNING, we're the server") 
		#ENDIF
		ENDIF
	ELSE
		SET_BIT(MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_INI)
		PROCESS_SCRIPT_INIT()
	ENDIF
	
	IF IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
		PROCESS_AIRLOCK_BOTTOM_RIGHT_HUD()
	ENDIF
	
	// INIT DOORS
	IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_INITIALISED_DOORS)
		INIT_DOORS()
		SET_BIT(iLocalBoolCheck32, LBOOL32_INITIALISED_DOORS)
	ENDIF
	
	// INIT ZONES
	IF eIntroProgress > eIntroState_WAIT_FOR_CORONA
		CREATE_ZONES(GET_PRE_ASSIGNMENT_PLAYER_TEAM())
	ENDIF
	
	// INIT PEDS
	FOR i = 0 TO (FMMC_MAX_PEDS-1)
		iPedSpawnFailDelay[i] = -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iTeam[MC_playerBD[iLocalPart].iteam] = ciPED_RELATION_SHIP_HATE
		OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iTeam[MC_playerBD[iLocalPart].iteam] = ciPED_RELATION_SHIP_DISLIKE
			IF WILL_PED_EVER_BE_HOSTILE(i)
				IF SHOULD_PED_SPAWN_IN_VEHICLE(i)
					IF NOT IS_BIT_SET(iEnemyVehBitSet,g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle)
						SET_BIT(iEnemyVehBitSet,g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_PED_A_COP_MC(i)
				IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[ i ] )
					SET_PED_CONFIG_FLAG(NET_TO_PED( MC_serverBD_1.sFMMC_SBD.niPed[ i ] ), PCF_DontBlipNotSynced, TRUE)
					PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - setting ped not to cop blip since friendly: ",i)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	// INIT VEHS
	FOR i = 0 TO (FMMC_MAX_VEHICLES - 1)
		iVehSpawnFailDelay[i] = -1
	ENDFOR
	
	// INIT OBJECTS
	FMMC_OBJECT_STATE sObjState
	FOR i = 0 TO FMMC_MAX_NUM_OBJECTS - 1
	
		FILL_FMMC_OBJECT_STATE_STRUCT(sObjState, i, TRUE)
		
		IF IS_BIT_SET(MC_serverBD.iFragableCrate, i)
		OR IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn)
			IF sObjState.bObjExists
				IF NOT IS_DAMAGE_TRACKER_ACTIVE_ON_NETWORK_ID(sObjState.niIndex)
					ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(sObjState.niIndex, TRUE)
					PRINTLN("[Objects][Object ", i, "] MANAGE_LOCAL_PLAYER_INIT - Damage tracking activated on crate: ",i)
				ENDIF
			ENDIF
		ENDIF

		FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
			OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
			OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn = PROP_CONTR_03B_LD
					SET_BIT(iLocalBoolCheck2, LBOOL2_HANDLER_HELP_TEXT_NEEDED)
				ENDIF
				PRINTLN("[Objects][Object ", i, "] MANAGE_LOCAL_PLAYER_INIT - Setting bit LBOOL2_HANDLER_HELP_TEXT_NEEDED")
			ENDIF
		ENDFOR
		
		IF sObjState.bHaveControlOfObj
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRule[MC_playerBD[iLocalPart].iTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
			OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRule[MC_playerBD[iLocalPart].iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
			OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRule[MC_playerBD[iLocalPart].iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
				IF SHOULD_PLACED_OBJECT_BE_PORTABLE(i)
					IF NOT IS_BIT_SET(MC_serverBD.iFragableCrate, i)
					AND NOT IS_BIT_SET(MC_serverBD.iWasHackObj, i)
						SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(sObjState, MC_playerBD[iLocalPart].iteam)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		MC_playerBD[iLocalPart].iSafeSyncSceneID[i] = -1
	ENDFOR
	
	// INIT LOCAL PLAYER
	PROCESS_PLAYER_MASK_INIT()
	
	IF NOT IS_PLAYER_SPECTATING(LocalPlayer)
		IF MC_serverBD.iNumVehCreated > 0
		AND MC_serverBD.iNumVehCreated <= FMMC_MAX_VEHICLES
			FOR i = 0 TO (MC_serverBD.iNumVehCreated-1)
				CHECK_SEATING_PREFERENCES(i)
			ENDFOR
		ENDIF
	ENDIF
	
	INIT_LOCAL_PLAYER_ABILITIES()
	
	// INIT WORLD	
	INIT_WORLD()
	
	PROCESS_INITIAL_INTERACTABLE_CONTINUITY()
		
	IF IS_FORCED_TRIP_SKIP_AVAILABLE()
		SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS()
		SET_UP_INSTANT_DIALOGUE_LOOP()
		SET_MC_CLIENT_GAME_STATE(GAME_STATE_FORCED_TRIP_SKIP)
	ELSE
		IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitset, PBBOOL_CLIENT_STATE_INITIALISED)			
			SET_UP_INSTANT_DIALOGUE_LOOP()
			IF SHOULD_CLIENT_MOVE_TO_INTRO_CUTSCENE()
				SET_MC_CLIENT_GAME_STATE(GAME_STATE_INTRO_CUTSCENE)
			ELSE
				SET_MC_CLIENT_GAME_STATE(GAME_STATE_RUNNING)
			ENDIF
		ELSE
			PRINTLN("[PROCESS_CLIENT][GAME_STATE_INI] - Waiting for PBBOOL_CLIENT_STATE_INITIALISED")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_EarlyEndSpectatorTracking)
	AND IS_BIT_SET(sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_LivesFailSpectator)
		IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_EARLY_END_SPECTATOR)
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
			FIND_PREFERRED_SPECTATOR_TARGET()
			CLEANUP_EARLY_END_AND_SPECTATE()
			SET_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RESET_STATE)
			IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
			    IF IS_SAFE_TO_TURN_ON_SPECTATOR_CAM()
					ACTIVATE_SPECTATOR_CAM(g_BossSpecData, SPEC_MODE_PLAYERS_RESPAWN_EVENT, NULL, ASCF_HIDE_LOCAL_PLAYER|ASCF_QUIT_SCREEN)
			    ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_CLIENT_GAME_STATE_INTRO_CUTSCENE()
	
	//[FMMC2020] move all of these calls into _Init intro cutscene processing function
	
	PROCESS_FREEZE_MICROPHONE_AT_PLAYER_FOR_MISSION()
			
	PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_INTRO_CUTSCENE][MMacK][IntroSync] GAME_STATE_INTRO_CUTSCENE")
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	MC_SET_ALL_MISSION_ENTITIES_VISIBLITY(TRUE)
	SET_PLAYER_INTO_COVER_AFTER_INITIAL_SPAWNING()	
	
	PRE_PROCESS_WORLD_PROPS()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_START_IN_CUTSCENE)		
		IF PROCESS_INTRO_RULE_CUTSCENE()
			SET_MC_CLIENT_GAME_STATE(GAME_STATE_RUNNING)
		ENDIF
		
		EXIT
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_TEAM_INTRO)
			PROCESS_TEAM_CUTSCENE()
		ELSE
			PROCESS_INTRO_CUTSCENE()
		ENDIF
	ENDIF
	
	IF IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
		PROCESS_AIRLOCK_BOTTOM_RIGHT_HUD()
	ENDIF
	
	IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitset, PBBOOL_FINISHED_INTRO_CUTSCENE)
		SET_MC_CLIENT_GAME_STATE(GAME_STATE_RUNNING)		
	ELIF SHOULD_INTRO_CUTSCENE_MOVE_TO_SOLO_PLAY()	
		SET_MC_CLIENT_GAME_STATE(GAME_STATE_SOLO_PLAY)
	ENDIF
ENDPROC

PROC PROCESS_CLIENT_GAME_STATE_FORCED_TRIP_SKIP()
	
	//[FMMC2020] This is mainly server processing based on a client state (weird)
	//[FMMC2020] Loop should be in _Players - add PROCESS_PLAYERS_FORCED_TRIP_SKIP()
	
	PARTICIPANT_INDEX tempPart
	
	//HOST ONLY
	IF bIsLocalPlayerHost
		//SET CAR
		IF MC_serverBD_3.FTSServerData.viCar = NULL
			IF MC_serverBD_3.FTSServerData.iStage > TSS_INIT
				IF DOES_ENTITY_EXIST(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[MC_serverBD_3.FTSServerData.iCar]))
					MC_serverBD_3.FTSServerData.viCar = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[MC_serverBD_3.FTSServerData.iCar])
					PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_FORCED_TRIP_SKIP] FORCED TRIP SKIP - viCar SET TO VEHICLE ", MC_serverBD_3.FTSServerData.iCar, " - CarID = ", NATIVE_TO_INT(MC_serverBD_3.FTSServerData.viCar))
				ELSE
					PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_FORCED_TRIP_SKIP] FORCED TRIP SKIP - CAR DOESN'T YET EXIST - iCar = ", MC_serverBD_3.FTSServerData.iCar)
				ENDIF
			ENDIF
		ENDIF
		
		//PRE LOOP
		PRE_TRIP_SKIP_SERVER_LOOP(MC_serverBD_3.FTSServerData, MC_serverBD.iNumberOfPart[0] + MC_serverBD.iNumberOfPart[1] + MC_serverBD.iNumberOfPart[2] + MC_serverBD.iNumberOfPart[3])
		//LOOP
		INT iTripSkipPart
		PRINTLN("[PLAYER_LOOP] - PROCESS_CLIENT_GAME_STATE_FORCED_TRIP_SKIP")
		REPEAT NUM_NETWORK_PLAYERS iTripSkipPart
			IF NETWORK_IS_PARTICIPANT_ACTIVE(temppart)
				MAINTAIN_TRIP_SKIP_SERVER_LOOP(MC_serverBD_3.FTSServerData, MC_playerBD[iTripSkipPart].FTSPlayerData, iTripSkipPart, IS_BIT_SET(MC_playerBD[iTripSkipPart].iClientBitSet, PBBOOL_HEIST_HOST))
			ENDIF
		ENDREPEAT
		POST_TRIP_SKIP_SERVER_LOOP(MC_serverBD_3.FTSServerData, TRUE, TRUE)
	ENDIF
	
	//MAIN
	MAINTAIN_FORCED_TRIP_SKIP_CLIENT(MC_serverBD_3.FTSServerData, MC_playerBD[iLocalPart].FTSPlayerData, FTSLocalData, bHasTripSkipJustFinished)
	IF bIsLocalPlayerHost
		MAINTAIN_FORCED_TRIP_SKIP_SERVER(MC_serverBD_3.FTSServerData)
	ENDIF
	
	//DONE CHECK
	IF MC_playerBD[iLocalPart].FTSPlayerData.iStage >= TSS_FINISHED
		PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_FORCED_TRIP_SKIP] FORCED TRIP SKIP - DONE - GO TO GAME STATE RUNNING")
		SET_MC_CLIENT_GAME_STATE(GAME_STATE_RUNNING)
	ENDIF
	IF IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
		PROCESS_AIRLOCK_BOTTOM_RIGHT_HUD()
	ENDIF
	
ENDPROC

PROC PROCESS_CLIENT_GAME_STATE_SOLO_PLAY()

	//[FMMC2020] should be called from _Init intro cutscene processing
	//[FMMC2020] should be called from _Init a lot of this looks like it could be moved to a shared function used by both solo and team

	CLEAR_PLAYER_WANTED_LEVEL(LocalPlayer)
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	
	IF HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO()
		MC_SET_ALL_MISSION_ENTITIES_VISIBLITY(TRUE)
		PRE_PROCESS_WORLD_PROPS()
		
		MC_playerBD[iLocalPart].iSyncRoundCutsceneStage = jobIntroData.sNewAnimData.eStage 
		MC_playerBD[iLocalPart].iSyncRoundIntroCutsceneStage = jobIntroData.jobIntroStage
		MC_playerBD[iLocalPart].iSyncRoundTeamCutsceneStage = jobIntroData.jobTeamCutStage						
		MC_playerBD[iLocalPart].iSyncRoundPostCutsceneStage = GET_SKYSWOOP_STAGE()
		MC_playerBD[iLocalPart].bSyncCutsceneDone = jobIntroData.bReadyToMoveIntoGameplay
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciPRE_GAME_EMP)
			g_bFMMCLightsTurnedOff = FALSE
		ENDIF
		
		IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
			
			IF IS_EVERYONE_READY_FOR_NEXT_ROUND()
				ARE_ALL_PLAYERS_READY_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION()
			ENDIF
	
			IF DOES_CAM_EXIST(jobIntroData.jobIntroCam)
			AND IS_EVERYONE_READY_FOR_NEXT_ROUND()
			AND HAS_SERVER_TIME_PASSED_ROUND(6000)
				IF ARE_ALL_PLAYERS_READY_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION()
					SET_SKYSWOOP_DOWN(jobIntroData.jobIntroCam)
					PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_SOLO_PLAY][ROUNDCAM] GAME_STATE_SOLO_PLAY calling SET_SKYSWOOP_DOWN ")
				ELSE
					PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_SOLO_PLAY][ROUNDCAM] GAME_STATE_SOLO_PLAY ARE_ALL_PLAYERS_READY_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION = FALSE, wait ")
				ENDIF
			ENDIF
			
			INT iServerTimeOut 
			iServerTimeOut = 25000
			IF HAS_NET_TIMER_EXPIRED(RoundCutsceneTimeOut, iServerTimeOut, TRUE)
				PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_SOLO_PLAY][ROUNDCAM] GAME_STATE_SOLO_PLAY timed out, running skycamdown iServerTimeOut: ", iServerTimeOut)
			
				SET_SKYSWOOP_DOWN(jobIntroData.jobIntroCam)
			ENDIF
			
			IF iSyncRoundLastFrame != GET_SKYSWOOP_STAGE()
				IF IS_SKYCAM_ON_LAST_CUT()
				AND GET_SKYSWOOP_STAGE() != SKYSWOOP_NONE
					HAS_SERVER_TIME_PASSED_ROUND()
				ENDIF
			ENDIF
			iSyncRoundLastFrame = GET_SKYSWOOP_STAGE()
			
			PROCESS_HOLD_UP_LOGIC_BETWEEN_ROUNDS()					
			IF IS_EVERYONE_READY_FOR_GAMEPLAY()
				PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_TEAM_PLAY][ROUNDCAM] IS_EVERYONE_READY_FOR_GAMEPLAY() = TRUE - Calling LOWER_CARGOBOB_RAMP_FOR_INTRO ")
				LOWER_CARGOBOB_RAMP_FOR_INTRO()
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_SOLO_PLAY][ROUNDCAM] g_b_HoldupSkycamBetweenRounds = FALSE GAME_STATE_SOLO_PLAY GAME_STATE_TEAM_PLAY GET_MC_SERVER_GAME_STATE() = GAME_STATE_END ")
			g_b_HoldupSkycamBetweenRounds = FALSE
			SET_MC_CLIENT_GAME_STATE(GAME_STATE_CAMERA_BLEND)
		ENDIF
		
		IF BUSYSPINNER_IS_ON()
			IF IS_SKYSWOOP_AT_GROUND()
				BUSYSPINNER_OFF()
			ENDIF
		ENDIF
		
		IF CONTENT_IS_USING_ARENA()
			IF PROCESS_ARENA_SPAWN_FIX()
				IF NOT IS_LOCAL_PLAYER_SPAWNING_IN_ARENA_BOX_SEAT()
				AND NOT IS_PLAYER_IN_ARENA_BOX_SEAT()
					IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_SETUP_NEW_INTRO_COORDS)
						SET_BIT(iLocalBoolCheck30, LBOOL30_SETUP_NEW_INTRO_COORDS)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[PROCESS_CLIENT][GAME_STATE_SOLO_PLAY] - PROCESS_ARENA_SPAWN_FIX - Exiting")
				g_b_HoldupCutsceneBetweenRounds = TRUE
				EXIT
			ENDIF
		ENDIF							
		
		IF PROCESS_DEATHMATCH_STARTING_CUT(jobIntroData) //PROCESS_JOB_OVERVIEW_CUT(jobIntroData,FALSE,FALSE,MC_playerBD[iLocalPart].iteam)
			PRINTLN("[RCC MISSION][ROUNDCAM] JOB INTRO DONE - MOVE TO RUNNING")
			IF SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
				CLEAR_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
			ENDIF
			g_b_HoldupSkycamBetweenRounds = FALSE	// [NG-INTEGRATE] Next-gen only.
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRACE_STYLE_INTRO)
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciDISABLE_TEAM_VERSUS_SHARD)
				SHOW_TEAM_SHARD_AT_START_OF_VS_MISSION()
			ENDIF
			SET_PLAYER_BLIPS_DISPLAY_AT_START_OF_VS_MISSION()
			RESET_NET_TIMER( RoundCutsceneTimeOut )	// [NG-INTEGRATE] Next-gen only.
			CLEANUP_CELEBRATION_SCALEFORMS( jobIntroData )
			
			IF IS_THIS_THE_FIRST_ROUND_OR_NOT_A_ROUNDS_MISSION()
				APPLY_CORONA_ALCOHOL_AND_SMOKE_EFFECTS( FALSE, FALSE, TRUE )
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_SOLO_PLAY] Applying corona drunk effects" )
			ELSE
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_SOLO_PLAY] Not applying corona drunk effects as it's a rounds mission and past the first round." )
			ENDIF
			
			SET_MC_CLIENT_GAME_STATE(GAME_STATE_CAMERA_BLEND)
		ELSE
			PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_SOLO_PLAY][CS_ANIM][ROUNDCAM] GAME_STATE_SOLO_PLAY,, PROCESS_DEATHMATCH_STARTING_CUT = false ") 
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_SOLO_PLAY][CS_ANIM][ROUNDCAM] GAME_STATE_SOLO_PLAY,, HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO = false ") 

	ENDIF
	IF IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
		PROCESS_AIRLOCK_BOTTOM_RIGHT_HUD()
	ENDIF
	
ENDPROC

PROC PROCESS_CLIENT_GAME_STATE_TEAM_PLAY()

	//[FMMC2020] should be called from _Init intro cutscene processing
	//[FMMC2020] should be called from _Init a lot of this looks like it could be moved to a shared function used by both solo and team

	CLEAR_PLAYER_WANTED_LEVEL(LocalPlayer)
	HIDE_HUD_AND_RADAR_THIS_FRAME()

	IF HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO()
	
		MC_SET_ALL_MISSION_ENTITIES_VISIBLITY(TRUE)
		PRE_PROCESS_WORLD_PROPS()
		
		MC_playerBD[iLocalPart].iSyncRoundCutsceneStage = jobIntroData.sNewAnimData.eStage 
		MC_playerBD[iLocalPart].iSyncRoundIntroCutsceneStage = jobIntroData.jobIntroStage
		MC_playerBD[iLocalPart].iSyncRoundTeamCutsceneStage = jobIntroData.jobTeamCutStage						
		MC_playerBD[iLocalPart].iSyncRoundPostCutsceneStage = GET_SKYSWOOP_STAGE()
		MC_playerBD[iLocalPart].bSyncCutsceneDone = jobIntroData.bReadyToMoveIntoGameplay		
	
		IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
	
			IF IS_EVERYONE_READY_FOR_NEXT_ROUND()
				ARE_ALL_PLAYERS_READY_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION()
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciPRE_GAME_EMP)
				g_bFMMCLightsTurnedOff = FALSE
			ENDIF
			
			IF DOES_CAM_EXIST(jobIntroData.jobIntroCam)
			AND IS_EVERYONE_READY_FOR_NEXT_ROUND()
			AND HAS_SERVER_TIME_PASSED_ROUND(6000)
				IF ARE_ALL_PLAYERS_READY_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION()
					SET_SKYSWOOP_DOWN(jobIntroData.jobIntroCam)
				ELSE
					PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_TEAM_PLAY][ROUNDCAM] GAME_STATE_TEAM_PLAY ARE_ALL_PLAYERS_READY_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION = FALSE, wait ")
				ENDIF
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(RoundCutsceneTimeOut, 25000, TRUE)
				PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_TEAM_PLAY][ROUNDCAM] GAME_STATE_TEAM_PLAY timed out, running skycamdown ")
			
				SET_SKYSWOOP_DOWN(jobIntroData.jobIntroCam)
			ENDIF
			
			IF iSyncRoundLastFrame != GET_SKYSWOOP_STAGE()
				IF IS_SKYCAM_ON_LAST_CUT()
				AND GET_SKYSWOOP_STAGE() != SKYSWOOP_NONE
					HAS_SERVER_TIME_PASSED_ROUND()
				ENDIF
			ENDIF
			iSyncRoundLastFrame = GET_SKYSWOOP_STAGE()
			
			PROCESS_HOLD_UP_LOGIC_BETWEEN_ROUNDS()					
			IF IS_EVERYONE_READY_FOR_GAMEPLAY()
				PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_TEAM_PLAY][ROUNDCAM] IS_EVERYONE_READY_FOR_GAMEPLAY() = TRUE - Calling LOWER_CARGOBOB_RAMP_FOR_INTRO ")
				LOWER_CARGOBOB_RAMP_FOR_INTRO()
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_TEAM_PLAY][ROUNDCAM] g_b_HoldupSkycamBetweenRounds = FALSE GAME_STATE_TEAM_PLAY GAME_STATE_TEAM_PLAY GET_MC_SERVER_GAME_STATE() = GAME_STATE_END ")
			g_b_HoldupSkycamBetweenRounds = FALSE
			SET_MC_CLIENT_GAME_STATE(GAME_STATE_CAMERA_BLEND)
		ENDIF
	
		IF BUSYSPINNER_IS_ON()
			IF IS_SKYSWOOP_AT_GROUND()
				BUSYSPINNER_OFF()
			ENDIF
		ENDIF
		
		IF PROCESS_NEW_TEAM_OVERVIEW_CUT(jobIntroData,MC_serverBD.vTeamCentreStartPoint[MC_PlayerBD[iLocalPart].iTeam])
			PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_TEAM_PLAY][ROUNDCAM] MOVING TO JOB INTRO") 
			RESET_NET_TIMER(tdSafetyTimer)
			RESET_NET_TIMER(RoundCutsceneTimeOut)	// [NG-INTEGRATE] Next-gen only.
			IF SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
				CLEAR_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
			ENDIF
			g_b_HoldupSkycamBetweenRounds = FALSE	// [NG-INTEGRATE] Next-gen only.
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRACE_STYLE_INTRO)
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciDISABLE_TEAM_VERSUS_SHARD)
				SHOW_TEAM_SHARD_AT_START_OF_VS_MISSION()
			ENDIF
			SET_PLAYER_BLIPS_DISPLAY_AT_START_OF_VS_MISSION()
			
			CLEANUP_CELEBRATION_SCALEFORMS(jobIntroData)
			IF IS_THIS_THE_FIRST_ROUND_OR_NOT_A_ROUNDS_MISSION()
				APPLY_CORONA_ALCOHOL_AND_SMOKE_EFFECTS( FALSE, FALSE, TRUE )
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_TEAM_PLAY] Applying corona drunk effects" )
			ELSE
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_TEAM_PLAY] Not applying corona drunk effects as it's a rounds mission and past the first round." )
			ENDIF
			
			CLEANUP_JOB_INTRO_CUTSCENE(jobIntroData) 
			CLEANUP_NEW_ANIMS(jobIntroData)
			
			SET_MC_CLIENT_GAME_STATE(GAME_STATE_CAMERA_BLEND)
		ELSE
		
			PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_TEAM_PLAY][CS_ANIM][ROUNDCAM] GAME_STATE_TEAM_PLAY,, PROCESS_NEW_TEAM_OVERVIEW_CUT = false ") 
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_TEAM_PLAY][CS_ANIM][ROUNDCAM] GAME_STATE_TEAM_PLAY,, HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO = false ") 

	ENDIF
	IF IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
		PROCESS_AIRLOCK_BOTTOM_RIGHT_HUD()
	ENDIF
	
ENDPROC

PROC PROCESS_CLIENT_GAME_STATE_CAMERA_BLEND()
	
	//[FMMC2020]- all of this should be moved to _init 
	//				- camera stuff to intro cutscene processing
	//				- the rest in a move to game state running wrapper
	
	PRINTLN("[PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] - ENTERED GAME_STATE_CAMERA_BLEND")
						
	CLEAR_PLAYER_WANTED_LEVEL(LocalPlayer)
	CLEAR_AREA_OF_PEDS(g_vTeamStartPos, 50)
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	bLocalHandBrakeOnFlag = FALSE

	MC_SET_ALL_MISSION_ENTITIES_VISIBLITY(TRUE)
	
	PRE_PROCESS_WORLD_PROPS()
	
	IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
		IF PROCESS_DEATHMATCH_BLEND_OUT(jobIntroData)
			IF IS_BLEND_OK_TO_PROCEED(LocalPlayer)
				IF NOT IS_BIT_SET( GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY)
					IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
						
						PLAY_SOUND_FRONTEND(-1,"Hit","RESPAWN_SOUNDSET", FALSE)
						
						PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] ANIMPOSTFX_PLAY, GAME_STATE_CAMERA_BLEND ")
						ANIMPOSTFX_PLAY("MinigameEndNeutral", 0, FALSE)
						
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] Moving from GAME_STATE_CAMERA_BLEND to GAME_STATE_RUNNING" )
						
						CLEANUP_CELEBRATION_SCALEFORMS( jobIntroData )
						
						CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam,FMMC_TYPE_MISSION,TRUE)
				
						DO_CORONA_POLICE_SETTINGS()
						
						IF IS_COUNTDOWN_OK_TO_PROCEED()
							IF IS_SKYSWOOP_AT_GROUND()
								NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
							ENDIF
						ENDIF
					
						IF USE_RACE_VEHICLE_CAMS()
							SET_PAUSE_INTRO_DROP_AWAY(FALSE)
							THEFEED_RESUME()
						ENDIF
						
						SET_ON_JOB_INTRO(FALSE)
						
						SET_HIDOF_OVERRIDE(FALSE,FALSE,0,0,0,0)
					
						CHECK_EMERGENCY_CLEANUP()								
						
						HEIST_STOP_ANIM_TASK(jobIntroData, MC_playerBD[iPartToUse].iTeam)

						SET_GAMEPLAY_CAM_RELATIVE_HEADING()

						CLEANUP_JOB_INTRO_CUTSCENE(jobIntroData)
						
						CLEANUP_NEW_ANIMS(jobIntroData)
						
						START_CAMERA_BLEND_AUDIO_SCENES_AND_SOUNDS()
						
						LOWER_CARGOBOB_RAMP_FOR_INTRO_BACKUP()
						
						SET_MC_CLIENT_GAME_STATE(GAME_STATE_RUNNING)
					ENDIF
				ELSE
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY - IS SET" )
				ENDIF
			ELSE
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] IS_BLEND_OK_TO_PROCEED - FALSE" )
			ENDIF
		ELSE
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] PROCESS_DEATHMATCH_BLEND_OUT - FALSE" )
		ENDIF
	ELSE
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] Dirty server data, this mission will be broken." )
		SCRIPT_ASSERT("This mission is broken, there is some dirty server data.")				
		SET_MC_CLIENT_GAME_STATE(GAME_STATE_RUNNING)
	ENDIF
	IF IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
		PROCESS_AIRLOCK_BOTTOM_RIGHT_HUD()
	ENDIF
	
ENDPROC

FUNC BOOL PROCESS_MISSION_COUNTDOWN_TIMER()
	IF NOT (bIsSCTV OR DID_I_JOIN_MISSION_AS_SPECTATOR())
		
		INT iTeam = MC_playerBD[iPartToUse].iteam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF NOT IS_COUNTDOWN_OK_TO_PROCEED()
			
			IF NOT g_bDisableVehicleMines
				DISABLE_VEHICLE_MINES(TRUE)
				PRINTLN("[LM][PROCESS_MISSION_COUNTDOWN_TIMER] - LIMIT RACE CONROLS - Disabling Vehicle Mines.")
			ENDIF
						
			PRINTLN("[PROCESS_MISSION_COUNTDOWN_TIMER] LIMIT RACE CONROLS - Processing Countdown.")

			DISPLAY_INTRO_COUNTDOWN(cduiIntro)
			
			//Allow for some player control during the race countdown
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRACE_STYLE_INTRO)
				IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_STARTED_COUNTDOWN)
					IF NOT IS_BITMASK_AS_ENUM_SET(cduiIntro.iBitFlags, CNTDWN_UI2_Played_Go)
						IF NOT bIsAnySpectator
							
							// Due to Hover Mode issues.
							IF bLocalPlayerPedOK
							AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
							AND GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(localPlayerPed)) = DELUXO
								NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
							ENDIF
							
							PRINTLN("[PROCESS_MISSION_COUNTDOWN_TIMER] LIMIT RACE CONROLS - DISABLE_CONTROL_THIS_FRAME_ENABLE_HORN_AND_HOOD")
							DISABLE_CONTROL_THIS_FRAME_ENABLE_HORN_AND_HOOD(FALSE,IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_ALLOW_REV_ON_COUNTDOWN),FALSE,TRUE,IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciLBD_ALLOW_HYDRO_FOR_COUNTDOWN))
							bLocalHandBrakeOnFlag = TRUE
						ENDIF							
					ELSE										
						PRINTLN("[PROCESS_MISSION_COUNTDOWN_TIMER] LIMIT RACE CONROLS - SBBOOL2_FINISHED_COUNTDOWN")
						SET_VEHICLE_HANDBRAKE_STATE(FALSE)
						bLocalHandBrakeOnFlag = FALSE
					ENDIF
				ELSE
					PRINTLN("[PROCESS_MISSION_COUNTDOWN_TIMER] LIMIT RACE CONROLS - SBBOOL2_STARTED_COUNTDOWN not set")
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciRACE_BOOST_START) 
					IF HAS_NET_TIMER_STARTED(g_FMMC_STRUCT.stBoostTimer)
						DO_VEH_BOOST()
					ENDIF
					UPDATE_VEHICLE_BOOSTING()
				ENDIF
			ENDIF
			
			RETURN FALSE
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRACE_STYLE_INTRO)	
				IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_DISPLAYED_INTRO_SHARD)
					IF IS_BITMASK_AS_ENUM_SET(cduiIntro.iBitFlags, CNTDWN_UI2_Played_Go)
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_Reset_321GO_OnRoundRestart)
							RELEASE_COUNTDOWN_UI(cduiIntro)
							CLEAN_COUNTDOWN(cduiIntro)
						ENDIF
						SET_BIT(iLocalBoolCheck13, LBOOL13_DISPLAYED_INTRO_SHARD)
					ENDIF
					
					IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_STARTED_COUNTDOWN)
					AND cduiIntro.iBitFlags != 0
					
						//if we have a lag issue on countdown, this might not cleanup properly							
						IF bLocalHandBrakeOnFlag = TRUE
							SET_VEHICLE_HANDBRAKE_STATE(FALSE)
							PRINTLN("[PROCESS_MISSION_COUNTDOWN_TIMER] LIMIT RACE CONROLS - SET_VEHICLE_HANDBRAKE_STATE - set to false")
							bLocalHandBrakeOnFlag = FALSE
						ENDIF
													
						IF bIsLocalPlayerHost
							RESET_NET_TIMER(MC_serverBD.timeServerCountdown)
							SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_FINISHED_COUNTDOWN)
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_DISPLAYED_INTRO_SHARD)
						SHOW_GO_SPLASH(cduiIntro)
						IF g_bDisableVehicleMines
							DISABLE_VEHICLE_MINES(FALSE)
							PRINTLN("[LM][PROCESS_MISSION_COUNTDOWN_TIMER] - LIMIT RACE CONROLS - Re-Enabling Vehicle Mines.")
						ENDIF
					ENDIF
					
					IF (iRule < FMMC_MAX_RULES)
					AND NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_SHOWN_TEAM_SHARD)
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciDISABLE_TEAM_VERSUS_SHARD)
						//SHOW_TEAM_SHARD_AT_START_OF_VS_MISSION()
						SET_BIT(iLocalBoolCheck13, LBOOL13_SHOWN_TEAM_SHARD) //if there's a massive lag on GO, this can happen twice
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bLocalHandBrakeOnFlag = TRUE
		IF SET_VEHICLE_HANDBRAKE_STATE(FALSE)
			PRINTLN("[PROCESS_MISSION_COUNTDOWN_TIMER] LIMIT RACE CONROLS - SET_VEHICLE_HANDBRAKE_STATE - set to false - bLocalHandBrakeOnFlag = FALSE")
			bLocalHandBrakeOnFlag = FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC 

/// PURPOSE:
///    There are a number of global variables that are used throughout the script
///    like PlayerToUse and iSpectatorTarget. These variables are refreshed at the top of each frame
///    by this function
PROC PROCESS_SPECTATOR_MODES_AND_LOCAL_PLAYER_VARIABLES() 
	
	PED_INDEX tempPed
	PLAYER_INDEX tempPlayer
	PARTICIPANT_INDEX tempPart
	BOOL bspectatorvalid
	BOOL bSpectatorDeactivate = FALSE

	INT iPreviousFrameSpectatorTarget = iSpectatorTarget

	iSpectatorTarget = -1

	IF g_bVSMission
		IF bIsSCTV
			UPDATE_SCTV_TICKER_SCORE_BASED_ON_WHO_IM_SPECTATING_DM_LEADERBOARD_STRUCT(g_MissionControllerserverBD_LB.sleaderboard, MC_ServerBD_2.tParticipantNames)	
		ENDIF
	ENDIF

	IF bIsAnySpectator
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			UPDATE_CELEBRATION_SCREEN_WHILE_SPECTATING()
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_HEIST_SPECTATOR)
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_ANY_SPECTATOR)
	ENDIF
	
	CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_HEIST_SPECTATOR)
	
	IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_HEADSHOTS_REGENERATED)
		BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS()) //Make sure headshots are updated to the job outfits.
		SET_BIT(iLocalBoolCheck9, LBOOL9_HEADSHOTS_REGENERATED)
		CPRINTLN(DEBUG_MISSION, "Regenerating local players headshot. LBOOL9_HEADSHOTS_REGENERATED")
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_LATE_SPECTATE)
	OR IS_BIT_SET(iLocalBoolCheck3, LBOOL3_WAIT_END_SPECTATE)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== MISSION === LBOOL3_WAIT_END_SPECTATE/LBOOL3_LATE_SPECTATE")
		#ENDIF
		IF NOT IS_BIT_SET(iLocalBoolCheck3, LBOOL3_KILL_SPECTATE)
			
			IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SPECTATOR, "=== MISSION === GAME_STATE_END")
				#ENDIF
				bSpectatorDeactivate = TRUE
				IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
				AND GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iRespawnState = RESPAWN_STATE_PLAYING
					
					PRINTLN("[TMSUnderMap] 10058 - Is player dead? ", IS_ENTITY_DEAD(LocalPlayerPed), " / iRespawnState = ", GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iRespawnState)
					
					IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SPECTATOR, "=== MISSION === IS_THIS_SPECTATOR_CAM_ACTIVE")
						#ENDIF
						SET_MANUAL_RESPAWN_STATE(MRS_EXIT_SPECTATOR_AFTERLIFE)
						
						//If we want to do a transition from spectator to celebration screen then freeze and play PostFX.
						IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
						AND sCelebrationData.eCurrentStage = CELEBRATION_STAGE_WAITING_FOR_SPECTATOR_CAM
							PRINTLN("[TMSUnderMap] 10069 - If we want to do a transition from spectator to celebration screen then freeze and play PostFX.")
							DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, DSCF_BAIL_FOR_TRANSITION | DSCF_PAUSE_RENDERPHASE)
							RESET_CELEBRATION_SCREEN_STATE(sCelebrationData)
							
							IF SHOULD_POSTFX_BE_WINNER_VERSION(FALSE)
								PLAY_CELEB_WIN_POST_FX()
							ELSE
								PLAY_CELEB_LOSE_POST_FX()
							ENDIF
							
							PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS", FALSE)
							
							PRINTLN("[NETCELEBRATION][TMSUnderMap] Spectator cam has finished, transitioning to celebration screen.")
						ELSE
							PRINTLN("[NETCELEBRATION][TMSUnderMap] 10083 - Else, just calling deactivate")
							DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, DSCF_BAIL_FOR_TRANSITION)
						ENDIF

						NET_SET_PLAYER_CONTROL( LocalPlayer, FALSE, NSPC_PREVENT_VISIBILITY_CHANGES | NSPC_PREVENT_COLLISION_CHANGES | NSPC_FREEZE_POSITION )
						SET_BIT(iLocalBoolCheck3,LBOOL3_KILL_SPECTATE)
						PRINTLN("[TMSUnderMap] 10087 - Spectator cam IS active")
					ELSE
						IF sCelebrationData.eCurrentStage = CELEBRATION_STAGE_SETUP
						AND IS_SCREEN_FADED_OUT()
							SET_BIT(iLocalBoolCheck3,LBOOL3_KILL_SPECTATE)
							// Fade in elsewhere. - url:bugstar:4340977  CL 17208353
							// DO_SCREEN_FADE_IN(1)
							CPRINTLN(DEBUG_SPECTATOR, "=== MISSION === LBOOL3_KILL_SPECTATE SET IN FAILSAFE")
							PRINTLN("[NETCELEBRATION][TMSUnderMap] LBOOL3_KILL_SPECTATE SET IN FAILSAFE")
						ENDIF
						
						PRINTLN("[TMSUnderMap] 10097 - Spectator cam is not active")
					ENDIF	
				ELSE
					IF IS_A_SPECTATOR_CAM_ACTIVE()
						PRINTLN("[TMSUnderMap] 10094 - Is player dead? ", IS_ENTITY_DEAD(LocalPlayerPed), " / iRespawnState = ", GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iRespawnState)
						SET_MANUAL_RESPAWN_STATE(MRS_NULL)
						RESET_GAME_STATE_ON_DEATH()
						SET_BIT(iLocalBoolCheck11, LBOOL11_PLAYER_IS_RESPAWNING)
						FORCE_RESPAWN(TRUE)
						
						PRINTLN("[TMSUnderMap] Calling DEACTIVATE_SPECTATOR_CAM with DSCF_PAUSE_RENDERPHASE to prevent seeing under the map")
						DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, DSCF_PAUSE_RENDERPHASE)
					ENDIF
				ENDIF
			ELSE
				//Spectator HUD
				tempPed = GET_SPECTATOR_SELECTED_PED() 
				IF IS_PED_ALLOWED_TO_BE_VIEWED_BY_SPEC_DATA(g_BossSpecData, tempPed)
					IF IS_PED_A_PLAYER(tempPed)
						tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(tempPed)
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(tempPlayer)
							tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
							iSpectatorTarget = NATIVE_TO_INT(tempPart)
							SET_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_ANY_SPECTATOR)
							PRINTLN("[TMSUnderMap] 10119 - In here")
						ENDIF
					ENDIF
				ENDIF

				IF DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE(g_BossSpecData.specCamData) 		//New flag for "do you want to quit" screen, DaveyG 22/11/2012				
					bSpectatorDeactivate = TRUE
					IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
					AND GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iRespawnState = RESPAWN_STATE_PLAYING
						IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
							#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SPECTATOR, "=== MISSION === DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE()")
							#ENDIF
							
							PRINTLN("[TMSUnderMap] 10132 - About to toggle renderphases")
							TOGGLE_RENDERPHASES( FALSE )
							DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, DSCF_BAIL_FOR_TRANSITION)
							NET_SET_PLAYER_CONTROL( LocalPlayer, FALSE, NSPC_PREVENT_VISIBILITY_CHANGES | NSPC_PREVENT_COLLISION_CHANGES | NSPC_PREVENT_FROZEN_CHANGES) // added NSPC_PREVENT_FROZEN_CHANGES, should already be frozen as a spectator... url:bugstar:5198307
							
						ENDIF	
						
						PRINTLN("[TMSUnderMap] 10139 - In here")
						
						RESET_STRAND_MISSION_DATA()
						RESET_ROUND_MISSION_DATA(DEFAULT, DEFAULT, FALSE)
						
						SET_MC_CLIENT_GAME_STATE(GAME_STATE_LEAVE)
					ELSE
						SET_MANUAL_RESPAWN_STATE(MRS_NULL)
						PRINTLN("[TMSUnderMap] 10145 - RESET_GAME_STATE_ON_DEATH() being called because this spectator cam wants to leave, you're dead or iRespawnState isn't playing")
						PRINTLN("[TMSUnderMap] 10145 - Is player dead? ", IS_ENTITY_DEAD(LocalPlayerPed), " / iRespawnState = ", GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iRespawnState)
						RESET_GAME_STATE_ON_DEATH()
						FORCE_RESPAWN(TRUE)
					ENDIF
				ENDIF
				
				IF NOT bSpectatorDeactivate
					// Turn on the spectator camera.
					IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
					    IF IS_SAFE_TO_TURN_ON_SPECTATOR_CAM()
							BOOL bSkipSpectator											
							IF bSkipSpectator = FALSE
								//If a celebration screen is playing then we want to preload the cam in the background then unfreeze when ready, UPDATE_CELEBRATION_SCREEN_WHILE_SPECTATING handles this.
								IF sCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING
								AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
									ACTIVATE_SPECTATOR_CAM(g_BossSpecData, SPEC_MODE_PLAYERS_RESPAWN_EVENT, NULL, ASCF_HIDE_LOCAL_PLAYER|ASCF_CELEBRATION_SCREEN|ASCF_QUIT_SCREEN)
									PRINTLN("[TMSUnderMap] 10185 - In here")
								ELSE
									SET_SKYFREEZE_CLEAR(TRUE)
									ACTIVATE_SPECTATOR_CAM(g_BossSpecData, SPEC_MODE_PLAYERS_RESPAWN_EVENT, NULL, ASCF_HIDE_LOCAL_PLAYER|ASCF_QUIT_SCREEN)
									PRINTLN("[TMSUnderMap] 10188 - In here")
								ENDIF
							ENDIF
					    ENDIF
					ENDIF
				ENDIF

				IF iSpectatorTarget !=-1
					bspectatorvalid = TRUE
				ENDIF
			ENDIF
		ENDIF
		
	ELIF bIsSCTV
		tempPed = GET_SPECTATOR_SELECTED_PED()
		IF IS_PED_ALLOWED_TO_BE_VIEWED_BY_SPEC_DATA(g_BossSpecData, tempPed)
			IF IS_PED_A_PLAYER(tempPed)
				tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(tempPed)
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(tempPlayer)
					tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
					iSpectatorTarget = NATIVE_TO_INT(tempPart)
					SET_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_ANY_SPECTATOR)
					PRINTLN("[TMSUnderMap] 10211 - In here, spectator target is ", iSpectatorTarget)
				ENDIF
			ENDIF
		ELSE
			iSpectatorTarget = iLocalPart
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_ANY_SPECTATOR)
			tempPart = PARTICIPANT_ID()
			tempPlayer = LocalPlayer
			PRINTLN("[TMSUnderMap] 10219 - In here, spectator target is ME")
		ENDIF
		IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_QUIT_REQUESTED)
			SCRIPT_TRIGGER_MUSIC_EVENT_FAIL()
			PRINTLN("TRIGGER_MUSIC_EVENT(\"MP_MC_FAIL\") Psmalrv1")
			DO_SCREEN_FADE_OUT(50)
			MC_SCRIPT_CLEANUP(FALSE)
			PRINTLN("[TMSUnderMap] 10226 - In here due to GLOBAL_SPEC_BS_QUIT_REQUESTED")
		ELSE
			IF iSpectatorTarget !=-1
				bspectatorvalid = TRUE
				PRINTLN("[TMSUnderMap] 10230 - Setting bSpectatorValid to TRUE")
			ENDIF
		ENDIF
		
	ELIF USING_HEIST_SPECTATE()
		tempPed = GET_SPECTATOR_SELECTED_PED()
		IF IS_PED_ALLOWED_TO_BE_VIEWED_BY_SPEC_DATA(g_BossSpecData, tempPed)
			IF IS_PED_A_PLAYER(tempPed)
				tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(tempPed)
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(tempPlayer)
					tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
					iSpectatorTarget = NATIVE_TO_INT(tempPart)
					PRINTLN("[TMSUnderMap] 10242 - Heist spectate - target is ", iSpectatorTarget)					
				ENDIF
			ENDIF
		ELSE
			iSpectatorTarget = iLocalPart
			tempPart = PARTICIPANT_ID()
			tempPlayer = LocalPlayer
			PRINTLN("[TMSUnderMap] 10248 - Heist spectate - target is me")
		ENDIF
		IF iSpectatorTarget !=-1
			bspectatorvalid = TRUE
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_HEIST_SPECTATOR)
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_ANY_SPECTATOR)			
		ENDIF		
	ENDIF
	
	
	// Drawing lbd for spectators
	IF IS_THIS_SPECTATOR_CAM_RUNNING(g_BossSpecData.specCamData)
	OR bIsSCTV
		PRINTLN("[PROCESS_SPECTATOR_MODES_AND_LOCAL_PLAYER_VARIABLES][LEADERBOARD] - Spectator")
		
	    IF IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)
		AND NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_NEW_JOB_SCREENS) 
		AND NOT IS_CUTSCENE_PLAYING()
			PRINTLN("[PROCESS_SPECTATOR_MODES_AND_LOCAL_PLAYER_VARIABLES][LEADERBOARD] - Calling MANAGE_LEADERBOARD_DISPLAY")
			
			UNTOGGLE_RENDERPHASES_FOR_LEADERBOARD()
			MANAGE_LEADERBOARD_DISPLAY()
			
			IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_HUD_COLOUR_RESET)
				PRINTLN("LBOOL5_HUD_COLOUR_RESET ")
				SET_BIT(iLocalBoolCheck5, LBOOL5_HUD_COLOUR_RESET)
			ENDIF
		ELSE
			PRINTLN("[PROCESS_SPECTATOR_MODES_AND_LOCAL_PLAYER_VARIABLES][LEADERBOARD] - Calling Reset")
			
			SET_ON_LBD_GLOBAL(FALSE)
	    ENDIF
	ENDIF
	
	IF bspectatorvalid
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			iPartToUse = iSpectatorTarget
			PlayerToUse = tempPlayer
		ELSE
			iSpectatorTarget = -1
			iPartToUse = PARTICIPANT_ID_TO_INT()
			PlayerToUse = PLAYER_ID()			
		ENDIF
	ELSE
		iSpectatorTarget = -1
		iPartToUse = PARTICIPANT_ID_TO_INT()
		PlayerToUse = PLAYER_ID()
	ENDIF
	
	IF IS_NET_PLAYER_OK(PlayerToUse)
		bPlayerToUseOK = TRUE
		PlayerPedToUse = GET_PLAYER_PED(PlayerToUse)
		IF NOT IS_PED_INJURED(PlayerPedToUse)
			bPedToUseOk = TRUE
		ELSE
			PRINTLN("[panic] bPlayerToUseOK = FALSE 1")
			bPedToUseOk = FALSE
		ENDIF
	ELSE
		PRINTLN("[panic] bPlayerToUseOK = FALSE 2")
		bPlayerToUseOK = FALSE
	ENDIF
	
	IF bspectatorvalid
	AND iPreviousFrameSpectatorTarget != iSpectatorTarget
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] The spectator target has changed to another valid participant. Deleting all object blips to make CTF blips correct again!" )
		
		INT iClearLoop
		REPEAT COUNT_OF( biObjBlip ) iClearLoop 
			REMOVE_BLIP( biObjBlip[ iClearLoop ] )
		ENDREPEAT
		
		INT iteam
		FOR iteam = 0  TO (FMMC_MAX_TEAMS-1)
			IF DOES_BLIP_EXIST(HomeBlip[iteam])
				REMOVE_BLIP(HomeBlip[iteam])
			ENDIF
		ENDFOR
		
	ENDIF
	
	IF MC_playerBD[iLocalPart].iPartIAmSpectating != iSpectatorTarget
		PRINTLN("[RCC MISSION] PROCESS_SPECTATOR_MODES_AND_LOCAL_PLAYER_VARIABLES - Updating MC_playerBD[iLocalPart].iPartIAmSpectating from ",MC_playerBD[iLocalPart].iPartIAmSpectating," to ",iSpectatorTarget)
		MC_playerBD[iLocalPart].iPartIAmSpectating = iSpectatorTarget
	ENDIF
	
ENDPROC



// FMMC 2020 Might want to have a new "ENDING" state where this is called, so we can change states from within main but have the bulk of processing in _ending.

PROC PROCESS_MISSION_END_STAGE()
	
	INT iEndDelay,iteam
	
	BOOL bDelayEnd = FALSE
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_AVENGER_INTERIOR_ENTRY)
	AND MC_serverBD.iEndCutsceneNum[MC_playerBD[iPartToUse].iteam] = -1
	
		IF IS_PLAYER_IN_CREATOR_AIRCRAFT(LocalPlayer)
			IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = INVALID_PLAYER_INDEX()
			OR (GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX() AND NOT NETWORK_IS_PLAYER_A_PARTICIPANT(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
				TRIGGER_EXIT_FROM_SIMPLE_INTERIOR_NOW()
				PRINTLN("PROCESS_MISSION_END_STAGE - Attempting to leave avenger")
			ENDIF
			PRINTLN("PROCESS_MISSION_END_STAGE - Delaying mission end due to player in avenger")
			bDelayEnd = TRUE
			IF NOT g_bLeavingVehInteriorForMCEnd
				g_bLeavingVehInteriorForMCEnd = TRUE
			ENDIF
		ENDIF
		
		IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
			IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].SimpleInteriorBD.iBSThree,  BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_KICK_EVERY_ONE_OUT_OF_ARMORY_AIRCRAFT) 
					SET_EMPTY_AVENGER_HOLD(TRUE)
					PRINTLN("PROCESS_MISSION_END_STAGE - Setting BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_KICK_EVERY_ONE_OUT_OF_ARMORY_AIRCRAFT")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
	AND NOT bDelayEnd
		g_bLeavingVehInteriorForMCEnd = FALSE
		g_bMissionOver = TRUE
		CLEANUP_WHITEOUT_RESPAWNING_FADING()
		CLEANUP_BLOCK_WASTED_KILLSTRIP_SCREEN()
		
		IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
			SET_POST_MISSION_SPAWN_SEQUENCE(g_FMMC_STRUCT.eCreatorPostMissionSpawnScenarioPass)
		ELSE
			SET_POST_MISSION_SPAWN_SEQUENCE(g_FMMC_STRUCT.eCreatorPostMissionSpawnScenarioFail)
		ENDIF
		
		IF NOT IS_STRAND_MISSION_READY_TO_START_DOWNLOAD()
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_DelayNextMissionPreload)
			SET_STRAND_MISSION_READY_TO_START_DOWNLOAD(MC_serverBD.iNextMission)
			PRINTLN("PROCESS_MISSION_END_STAGE - SET_STRAND_MISSION_READY_TO_START_DOWNLOAD TRUE ")
		ENDIF
		
		IF MC_playerBD[iLocalPart].iMedalRankScore = 0
			PROCESS_LOCAL_PLAYER_END_ACCURACY()
			PROCESS_LOCAL_PLAYER_END_STEALTH_KILLS()
			PROCESS_LOCAL_PLAYER_END_HEALTH()
			PRINTLN("PROCESS_MISSION_END_STAGE - Calling GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT")
			MC_playerBD[iLocalPart].iMedalRankScore = GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT(iLocalPart)
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
			g_bMissionOver = TRUE
			PRINTLN("PROCESS_MISSION_END_STAGE - g_bMissionOver - SET")
		ELSE
			IF g_bMissionOver
				g_bMissionOver = FALSE
			ENDIF
		ENDIF	
		
		PROCESS_PASS_OVER_MOCAP_PLAYER_PED_ORIGINAL_CLONES()
		
		MP_FORCE_TERMINATE_INTERNET() 

		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		AND GET_SEAT_PED_IS_IN(LocalPlayerPed) = VS_DRIVER
			IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed, FALSE))
				IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed, FALSE)) = DELUXO
					PRINTLN("PROCESS_MISSION_END_STAGE() - SET_VEHICLE_ENGINE_ON = FALSE (Not doing this as it's Deluxo.)") 	
				ELSE
					PRINTLN("PROCESS_MISSION_END_STAGE() - SET_VEHICLE_ENGINE_ON = FALSE")
					SET_VEHICLE_ENGINE_ON(GET_VEHICLE_PED_IS_IN(LocalPlayerPed, FALSE), FALSE, TRUE, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_UNLOCK)
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_UNLOCK)
			PRINTLN("PROCESS_MISSION_END_STAGE() - BIG_MESSAGE_UNLOCK") 
		ENDIF

		IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
		AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_KILLED_APT_COUNTDOWN_MUSIC)
		
			IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_COUNTDOWN_EARLY_MISSION_END)
				PRINTLN("PROCESS_MISSION_END_STAGE - Killed Countdown music due to early end")
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciUSE_FREEMODE_30S_COUNTDOWN)
					TRIGGER_MUSIC_EVENT("FM_COUNTDOWN_30S_KILL")
				ELSE
					TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
				ENDIF
			ENDIF
			
			SET_BIT(iLocalBoolCheck14, LBOOL14_HAS_KILLED_APT_COUNTDOWN_MUSIC)
			PRINTLN("PROCESS_MISSION_END_STAGE - Triggering TRIGGER_MUSIC_EVENT('APT_FADE_IN_RADIO')")
			TRIGGER_MUSIC_EVENT("APT_FADE_IN_RADIO")
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC)
		AND NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_KILLED_VAL_COUNTDOWN_MUSIC)

			IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_COUNTDOWN_EARLY_MISSION_END)
				PRINTLN("PROCESS_MISSION_END_STAGE - Killed Countdown music due to early end")
				TRIGGER_MUSIC_EVENT("VAL2_COUNTDOWN_30S_KILL")
			ENDIF
			
			SET_BIT(iLocalBoolCheck15, LBOOL15_HAS_KILLED_VAL_COUNTDOWN_MUSIC)
			PRINTLN("PROCESS_MISSION_END_STAGE - Triggering TRIGGER_MUSIC_EVENT('VAL2_FADE_IN_RADIO')")
			TRIGGER_MUSIC_EVENT("VAL2_FADE_IN_RADIO")
		ENDIF

		IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_KILLED_APT_SUDDEN_DEATH_MUSIC)
		AND ( IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_SUDDEN_DEATH_MUSIC)
			  OR IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_APT_SUDDEN_DEATH_END) )
			SET_BIT(iLocalBoolCheck14, LBOOL14_HAS_KILLED_APT_SUDDEN_DEATH_MUSIC)
			PRINTLN("PROCESS_MISSION_END_STAGE - TRIGGER_MUSIC_EVENT('APT_SUDDEN_DEATH_MUSIC_KILL')")
			TRIGGER_MUSIC_EVENT("APT_SUDDEN_DEATH_MUSIC_KILL")
			sudden_death_music_state = eSuddenDeathState_END
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HAS_KILLED_IE_COUNTDOWN_MUSIC)
		AND IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC)

			SET_BIT(iLocalBoolCheck14, LBOOL14_HAS_KILLED_APT_SUDDEN_DEATH_MUSIC)
			SET_BIT(iLocalBoolCheck15, LBOOL15_HAS_KILLED_VAL_COUNTDOWN_MUSIC)
			SET_BIT(iLocalBoolCheck20, LBOOL20_HAS_KILLED_IE_COUNTDOWN_MUSIC)
			
			IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_COUNTDOWN_EARLY_MISSION_END)
				PRINTLN("PROCESS_MISSION_END_STAGE - Killed Countdown music due to early end")
				TRIGGER_MUSIC_EVENT("IE_COUNTDOWN_30S_KILL")								
			ENDIF
			
			PRINTLN("PROCESS_MISSION_END_STAGE - Triggering TRIGGER_MUSIC_EVENT('IE_FADE_IN_RADIO')")
			TRIGGER_MUSIC_EVENT("IE_FADE_IN_RADIO")
		ENDIF		
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciTURN_OFF_VEH_SPAWN)
			PRINTLN("PROCESS_MISSION_END_STAGE - suppressing vehicle model FALSE SUPPRESS_ALL_HELIS")
			VALIDATE_NUMBER_MODELS_SUPRESSED(SUPPRESS_ALL_HELIS(FALSE)*-1)
		ENDIF
		
		SET_MISSION_OVER_AND_ON_LB_FOR_SCTV()
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour,ciLOUD_PLANES)
			IF IS_AUDIO_SCENE_ACTIVE("Speed_Race_Airport_Scene")
				STOP_AUDIO_SCENE("Speed_Race_Airport_Scene")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET( iLocalBoolCheck11, LBOOL11_STOP_MISSION_FAILING_DUE_TO_EARLY_CELEBRATION )
		AND NOT bLocalPlayerOK
		AND NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDidPlayerStartWinnerCelebrationScreenDead
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDidPlayerStartWinnerCelebrationScreenDead = TRUE
			PRINTLN("PROCESS_MISSION_END_STAGE - Setting g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDidPlayerStartWinnerCelebrationScreenDead = TRUE" )
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)
			IF IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT()
				IF IS_PLAYER_RESPAWNING_BEFORE_FADE_IN()
					IF NOT HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
						PRINTLN("PROCESS_MISSION_END_STAGE - IS_PLAYER_RESPAWNING_BEFORE_FADE_IN setting bit LBOOL10_RESPAWNING_DURING_FAIL: do fail celebration early ")
						g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockSpawnFadeInForMissionFail = TRUE
						PRINTLN("PROCESS_MISSION_END_STAGE - [SPAWNING] - [NETCELEBRATION] - set flag g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockSpawnFadeInForMissionFail")
						SET_BIT(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)
					ELSE
						PRINTLN("PROCESS_MISSION_END_STAGE - IS_PLAYER_RESPAWNING_BEFORE_FADE_IN HAS_TEAM_PASSED_MISSION = TRUE")
					ENDIF
				ELSE
					PRINTLN("PROCESS_MISSION_END_STAGE - IS_PLAYER_RESPAWNING_BEFORE_FADE_IN = FALSE")
				ENDIF
			ENDIF
		ENDIF
		
		IF iLocalPart!= -1
			IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_ROUNDS_MISSION_END_SET_UP)
				IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_MISSION_ENDING_BECAUSE_PLAYERS_LEFT)
				OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_TOO_FEW_PLAYERS_FOR_NEXT_ROUND)
				#IF IS_DEBUG_BUILD
				OR iPlayerPressedF != -1
				OR iPlayerPressedS != -1
				#ENDIF
					
					PRINTLN("PROCESS_MISSION_END_STAGE - Clearing rounds data - SBBOOL2_MISSION_ENDING_BECAUSE_PLAYERS_LEFT = ",IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_MISSION_ENDING_BECAUSE_PLAYERS_LEFT),", iPlayerPressedF = ",iPlayerPressedF,", iPlayerPressedS = ",iPlayerPressedS)
					
					#IF IS_DEBUG_BUILD
					IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_MISSION_ENDING_BECAUSE_PLAYERS_LEFT)
					#ENDIF
					
					IF g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds != 0
						IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_HANDLE_PLAYLIST_POSITION_DATA_WRITE_CALLED)
							SET_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
							IF IS_PLAYER_ON_A_PLAYLIST(LocalPlayer)
								HANDLE_PLAYLIST_POSITION_DATA_WRITE()
							ENDIF
							SET_BIT(iLocalBoolCheck8, LBOOL8_HANDLE_PLAYLIST_POSITION_DATA_WRITE_CALLED)
						ENDIF
						RESET_ROUND_MISSION_DATA(DEFAULT, DEFAULT, FALSE)
					ENDIF
					
					#IF IS_DEBUG_BUILD
					ENDIF
					#ENDIF
					
					CLEAR_USE_BACKED_UP_MENU_SELECTION_ON_RESTART()
				ENDIF
				PRINTLN("PROCESS_MISSION_END_STAGE - SET_BIT(iLocalBoolCheck15, LBOOL15_ROUNDS_MISSION_END_SET_UP)")
				SET_BIT(iLocalBoolCheck15, LBOOL15_ROUNDS_MISSION_END_SET_UP)
			ENDIF
			
			IF SET_UP_ROUNDS_DETAILS_AT_END_OF_MISSION(MC_serverBD.iWinningTeam, MC_playerBD[iLocalPart].iteam)
				PRINTLN("PROCESS_MISSION_END_STAGE - SET_BIT(iLocalBoolCheck15, LBOOL15_CALL_SWAP_TEAM_LOGIC)")
				SET_BIT(iLocalBoolCheck15, LBOOL15_CALL_SWAP_TEAM_LOGIC)
			ENDIF
					
			IF IS_BIT_SET(iLocalBoolCheck, ciPOPULATE_RESULTS)
			AND IS_BIT_SET(iLocalBoolCheck15, LBOOL15_CALL_SWAP_TEAM_LOGIC)
				CLEAR_BIT(iLocalBoolCheck15, LBOOL15_CALL_SWAP_TEAM_LOGIC)
				PRINTLN("PROCESS_MISSION_END_STAGE - CLEAR_BIT(iLocalBoolCheck15, LBOOL15_CALL_SWAP_TEAM_LOGIC)")
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciALWAYS_SWAP_SMALL_TEAM)
					PRINTLN("[SST] * - IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciALWAYS_SWAP_SMALL_TEAM)")
					IF SHOULD_I_JOIN_SMALLEST_TEAM(iTeamLBPositionStore, TRUE)
						PRINTLN("PROCESS_MISSION_END_STAGE - [SST] * - SHOULD_I_JOIN_SMALLEST_TEAM")
						SET_VARS_AND_END_OF_ALWAYS_SWAP_SMALLEST_TEAM_MISSION(TRUE)
					ELSE
						SET_VARS_AND_END_OF_ALWAYS_SWAP_SMALLEST_TEAM_MISSION(FALSE)
					ENDIF
				ELSE
					IF SHOULD_I_JOIN_SMALLEST_TEAM(iLBPositionStore)
						PRINTLN("PROCESS_MISSION_END_STAGE - [SST] * - SHOULD_I_JOIN_SMALLEST_TEAM")
						SET_VARS_AND_END_OF_SWAP_TO_SMALLEST_TEAM_MISSION(TRUE)
					ELSE
						SET_VARS_AND_END_OF_SWAP_TO_SMALLEST_TEAM_MISSION(FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_SAVED_ROUNDS_LBD_DATAa)
				// Write kills etc. rewards are written in (POPULATE_RESULTS)
				PRINTLN("PROCESS_MISSION_END_STAGE - [TS] [MSROUND] - WRITE_TO_ROUNDS_MISSION_END_LB_DATA 1a) ")
				INT iScore, iKills, iDeaths

				iScore = MC_serverBD.iPlayerScore[iLocalPart]
				
				INT iTime = 0
				
				//bugstar:3026870 - Kills on Leaderboard taking suicides into account
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciSHOW_ONLY_KILLS_ON_LEADERBOARD)
					iKills = MC_playerBD[iPartToUse].iPureKills
				ELSE
					iKills = MC_PlayerBD[iLocalPart].iNumPlayerKills + MC_PlayerBD[iLocalPart].iNumPedKills
				ENDIF
								
				iDeaths = MC_PlayerBD[iLocalPart].iNumPlayerDeaths
				
				WRITE_TO_ROUNDS_MISSION_END_LB_DATA(iScore,
										iKills,
										iDeaths,	
										0,
										0,
										MC_PlayerBD[iLocalPart].iAssists,
										iTime,
										0)
				SET_BIT(iLocalBoolCheck6, LBOOL6_SAVED_ROUNDS_LBD_DATAa)
			ENDIF
			
			//Write to the playlist LB
			IF IS_PLAYER_ON_A_PLAYLIST(LocalPlayer)
				IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_HANDLE_PLAYLIST_POSITION_DATA_WRITE_CALLED)
				AND SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
					HANDLE_PLAYLIST_POSITION_DATA_WRITE()
					SET_BIT(iLocalBoolCheck8, LBOOL8_HANDLE_PLAYLIST_POSITION_DATA_WRITE_CALLED)
				ENDIF
			ENDIF
			
			//If it's a rounds mission then call DEAL_WITH_FM_MATCH_END
			IF iRoundsLBPosition != -1
			AND (AM_I_AT_END_OF_ROUNDS_MISSION()
			OR SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD())
				PRINTLN("PROCESS_MISSION_END_STAGE - [TEL] * iRoundsLBPosition = ", iRoundsLBPosition)
				DEAL_WITH_FM_MATCH_END(FMMC_TYPE_MISSION,																				//Matchtype									
										g_MissionControllerserverBD_LB.iMatchTimeID,													//posixTime
										0,																								//Variation
										iMissionResult,																					//PassFail
										g_MissionControllerserverBD_LB.sleaderboard[iRoundsLBPosition].iRank,							//LDBD Pos
										g_MissionControllerserverBD_LB.sleaderboard[iRoundsLBPosition].iKills,							//Kills
										g_MissionControllerserverBD_LB.sleaderboard[iRoundsLBPosition].iDeaths,							//Deaths
										iRoundsSuicides,																				//Suicides
										iHighKillStreak,																				//Highest killstreak
										g_MissionControllerserverBD_LB.sleaderboard[iRoundsLBPosition].iTeam,							//Team
										g_MissionControllerserverBD_LB.sleaderboard[iRoundsLBPosition].iHeadshots, 						//Headshots
										g_mnMyRaceModel,																				//Vehicle ID
										DEFAULT,																						//Waves survived
										IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_MISSION_ENDING_BECAUSE_PLAYERS_LEFT),			//Forced round end
										MC_serverBD.iDifficulty,																		//Difficulty
										GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( MC_serverBD.tdMissionLengthTimer ),					//Time
										DEFAULT)																						//DNF
				iRoundsLBPosition = -1																									
			ENDIF 
			
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("PROCESS_MISSION_END_STAGE - SET_UP_ROUNDS_DETAILS_AT_END_OF_MISSION - not being called from the mission controller, iLocalPart is -1")
		#ENDIF	
		ENDIF
		
		IF NOT IS_HEIST_SPECTATE_DISABLED()
			PRINTLN("PROCESS_MISSION_END_STAGE - PIM - HEIST SPECTATE - DISABLE_HEIST_SPECTATE - C")
			DISABLE_HEIST_SPECTATE(TRUE)
		ENDIF
	
	ENDIF
	
	MAINTAIN_CELEBRATION_PRE_LOAD(sCelebrationData)
	
	IF iSpectatorTarget != -1
	AND NOT bIsSCTV
	AND NOT USING_HEIST_SPECTATE()
		EXIT
	ENDIF
	
	// Don't show HUD if the celebration screen has been requested/is being shown
	IF g_bCelebrationScreenIsActive
		IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_SUDDEN_DEATH_REASON_SHARD_SENT)
			IF NOT HAS_NET_TIMER_STARTED(tdresultdelay) OR (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdresultdelay) > iEndDelay)
				DISABLE_HUD()
				HIDE_SPECTATOR_HUD( TRUE )	
			ENDIF
		ENDIF
	ENDIF

	IF NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_SHOWN_RESULTS)
		IF ((HAS_TEAM_FINISHED(MC_playerBD[iPartToUse].iteam) AND NOT HAVE_ALL_TEAMS_FINISHED())
		OR ((HAS_LOCAL_PLAYER_FAILED() AND NOT HAVE_ALL_TEAMS_FINISHED()) AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED())
		OR GET_MC_SERVER_GAME_STATE() = GAME_STATE_END)
		AND NOT bDelayEnd
			IF NOT g_bEndOfMissionCleanUpHUD
				IF NOT HAS_NET_TIMER_STARTED(tdresultdelay) OR (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdresultdelay) > iEndDelay)
					g_bEndOfMissionCleanUpHUD = TRUE
				ENDIF
				PRINTLN("PROCESS_MISSION_END_STAGE - Disabling HUD cos PROCESS_MISSION_END_STAGE")
			ENDIF
			
			IF USING_HEIST_SPECTATE()
				CLEANUP_HEIST_SPECTATE()
				IF NOT IS_LOADED_MISSION_TYPE_FLOW_MISSION()
					PRINTLN("PROCESS_MISSION_END_STAGE - CLEANUP_HEIST_SPECTATE_FOR_NON_HEISTS")
					CLEANUP_HEIST_SPECTATE_FOR_NON_HEISTS()
				ENDIF
			ENDIF
			
			MAKE_PLAYER_INVINCIBLE_ONCE_MISSION_IS_OVER()
			
			IF MC_serverBD_4.iCurrentHighestPriority[ MC_playerBD[iPartToUse].iteam ]	> FMMC_MAX_RULES
			AND NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
				HIDE_HELP_TEXT_THIS_FRAME()
				HUD_SUPPRESS_WEAPON_WHEEL_RESULTS_THIS_FRAME()
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				DISABLE_SELECTOR_THIS_FRAME() 
				CLEAR_INVENTORY_BOX_CONTENT()
				DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				HIDE_SPECTATOR_HUD( TRUE )
				
				IF NOT bIsAnySpectator
					IF SHOULD_TURN_OFF_CONTROL_AT_END_OF_MISSION()
						NET_SET_PLAYER_CONTROL( LocalPlayer, FALSE, NSPC_PREVENT_FROZEN_CHANGES | NSPC_PREVENT_VISIBILITY_CHANGES | NSPC_PREVENT_COLLISION_CHANGES )
					ENDIF
				ENDIF
			ENDIF
											
			g_bMissionRemovedWanted = TRUE
			
			IF IS_PAUSE_MENU_ACTIVE()
				PRINTLN("PROCESS_MISSION_END_STAGE - SET_FRONTEND_ACTIVE(FALSE) - 1")				
				SET_FRONTEND_ACTIVE(FALSE)				
				PRINTLN("PROCESS_MISSION_END_STAGE - SET_FRONTEND_ACTIVE(FALSE) - 2")
			ENDIF
			
			IF IS_SOCIAL_CLUB_ACTIVE()
				IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bClosedScPage)
					CLOSE_SOCIAL_CLUB_MENU()
					PRINTLN("PROCESS_MISSION_END_STAGE - IS_SOCIAL_CLUB_ACTIVE - CLOSE_SOCIAL_CLUB_MENU")				
					SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bClosedScPage)
				ENDIF
				PRINTLN("PROCESS_MISSION_END_STAGE - IS_SOCIAL_CLUB_ACTIVE")				
			ENDIF
			
			IF bIsLocalPlayerHost
				IF MC_serverBD_1.iPlayerForHackingMG != -1
					MC_serverBD_1.iPlayerForHackingMG = -1
					PRINTLN("PROCESS_MISSION_END_STAGE - MC_serverBD_1.iPlayerForHackingMG = -1")
				ENDIF
			ENDIF
			
			IF IS_CIRCUIT_HACKING_MINIGAME_RUNNING(hackingMinigameData)
				CLEANUP_AND_QUIT_CIRCUIT_HACKING_MINIGAME(hackingMinigameData, IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset,SBBOOL_HACK_DO_HARD))
				IF bLocalPlayerPedOk
					SET_PED_CONFIG_FLAG(LocalPlayerPed,PCF_PreventAutoShuffleToDriversSeat,FALSE)
				ENDIF
				PRINTLN("PROCESS_MISSION_END_STAGE - CLEANUP_AND_QUIT_CIRCUIT_HACKING_MINIGAME")
			ENDIF
			
			// Clear up manual respawn
			IF (manualRespawnState != eManualRespawnState_RESPAWNING)
				IF manualRespawnState != eManualRespawnState_OKAY_TO_SPAWN
				OR manualRespawnState != eManualRespawnState_WAITING_AFTER_RESPAWN
					CLEAN_UP_MANUAL_RESPAWN()
				ENDIF
			ENDIF
			
			IF g_bVSMission
				IF bIsSCTV
					SET_SCTV_TICKER_SCORE_OFF()
				ENDIF
			ENDIF
			
			SET_MP_DECORATOR_BIT(LocalPlayer, MP_DECORATOR_BS_INVALID_TO_SPECTATE)
			
			SET_GLOBAL_CLIENT_FINISHED_JOB(TRUE) 
			
			IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
			AND IS_BIT_SET(iLocalBoolCheck4, LBOOL4_SCRIPTED_CUT_NEEDS_CLEANED_UP)
				PRINTLN("PROCESS_MISSION_END_STAGE - Stopping cutscene mission is over.")
				
				//Clear up any scripted cutscene!
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE)
				IF g_iFMMCScriptedCutscenePlaying != -1
					SET_BIT(MC_playerBD[iLocalPart].iCutsceneFinishedBitset, g_iFMMCScriptedCutscenePlaying)
				ENDIF
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_FINISHED_CUTSCENE)
				
				//Stop any active load scenes
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()
				ENDIF
				CLEANUP_SCRIPTED_CUTSCENE(TRUE)
			ENDIF
			
			SET_DAILY_OBJECTIVE_TICKERS_ENABLED_ON_MISSION(FALSE)
			
			IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()	
				ENABLE_NIGHTVISION(VISUALAID_OFF,VISUALAID_SOUND_OFF)
			ENDIF
			
			CLEANUP_PLAYER_VISUAL_AIDS()
			
			IF IS_SOUND_ID_VALID(iBoundsTimerSound)
			AND NOT HAS_SOUND_FINISHED(iBoundsTimerSound)
				STOP_SOUND(iBoundsTimerSound)
			ENDIF
			
			IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
				g_bMissionEnding = TRUE
				SET_MP_DECORATOR_BIT(LocalPlayer, MP_DECORATOR_BS_MISSION_ENDING)
				PRINTLN("PROCESS_MISSION_END_STAGE - MP_DECORATOR_BS_MISSION_ENDING - g_bMissionEnding - SET")
			ELSE
				IF g_bMissionEnding
					g_bMissionEnding = FALSE
				ENDIF
			ENDIF	
			
			IF NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()	
				PRINTLN("PROCESS_MISSION_END_STAGE - Calling KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE 1")
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			
			IF IS_SKYSWOOP_IN_SKY()
				IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_FLUSH_FEED)
					THEFEED_FLUSH_QUEUE()
					 g_b_ReapplyStickySaveFailedFeed = FALSE
					SET_BIT(iLocalBoolCheck2,LBOOL2_FLUSH_FEED)
				ENDIF
				THEFEED_HIDE_THIS_FRAME() 
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(MC_Playerbd[iPartToUse].tdMissionTime)
				MC_Playerbd[iPartToUse].iMissionEndTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_Playerbd[iPartToUse].tdMissionTime)
				PRINTLN("PROCESS_MISSION_END_STAGE - Setting end time for part: ",iPartToUse," to time: ",MC_Playerbd[iPartToUse].iMissionEndTime)
				RESET_NET_TIMER(MC_Playerbd[iPartToUse].tdMissionTime)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iLDB_ShowEndTimeForTeam, GET_LOCAL_PLAYER_TEAM())
			AND HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[GET_LOCAL_PLAYER_TEAM()])
			AND MC_Playerbd[iLocalPart].iRoundEndTime = 0
				MC_Playerbd[iLocalPart].iRoundEndTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[GET_LOCAL_PLAYER_TEAM()])
				PRINTLN("PROCESS_MISSION_END_STAGE - Setting iRoundEndTime to: ",MC_Playerbd[iLocalPart].iRoundEndTime)
			ENDIF
			
			IF ANIMPOSTFX_IS_RUNNING("CrossLine")
				PRINTLN("PROCESS_MISSION_END_STAGE - End of Mission")
				ANIMPOSTFX_STOP("CrossLine")
				ANIMPOSTFX_PLAY("CrossLineOut", 0, FALSE)
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_RESET_ALL_HACKING_MINIGAMES_END_CALLED)
				RESET_ALL_HACKING_MINIGAMES(TRUE)
				SET_BIT(iLocalBoolCheck7, LBOOL7_RESET_ALL_HACKING_MINIGAMES_END_CALLED)
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_SET_END_MISSION_WEAPON)
				WEAPON_TYPE wtWeapon
				GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtWeapon)
				SET_PLAYER_CURRENT_HELD_WEAPON(wtWeapon)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("PROCESS_MISSION_END_STAGE - store weapon = ", GET_WEAPON_NAME(INT_TO_ENUM(WEAPON_TYPE, wtWeapon)))
				#ENDIF
						
				SET_BIT(iLocalBoolCheck7, LBOOL7_SET_END_MISSION_WEAPON)
			ENDIF
			
			IF NOT HAS_NET_TIMER_STARTED(tdresultdelay)
				
				SEND_EVENT_IF_PLAYER_COMPLETES_JOB_IN_UNDER_TEN_SECONDS(MC_serverBD.tdMissionLengthTimer, IS_PLAYER_SPECTATOR_ONLY(LocalPlayer), IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob))

				START_NET_TIMER(tdresultdelay)
				
				REMOVE_REQUESTED_MC_ASSETS()

				SET_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_FINISHED)
				
				PRINTLN("PROCESS_MISSION_END_STAGE - Setting objective blocker")
				BLOCK_OBJECTIVE_THIS_FRAME()
				
				IF IS_PAUSE_MENU_ACTIVE()
					PRINTLN("PROCESS_MISSION_END_STAGE - SET_FRONTEND_ACTIVE(FALSE) - 3")				
					SET_FRONTEND_ACTIVE(FALSE)
				ENDIF
				
				CLEANUP_DPAD_LBD(scaleformDpadMovie)
				
				SAVE_PLAYER_END_JOB_POSITION_FOR_POST_MISSION_CLEANUP()
				IF DOES_ENTITY_EXIST(LocalPlayerPed)
					SET_PERSONAL_VEHICLE_SPAWN_OVERRIDE_VECTOR(GET_ENTITY_COORDS(LocalPlayerPed, FALSE))
				ENDIF
				
				IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
					IF NOT HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iteam)
						g_bFailedMission = TRUE
						IF IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_InitDone)
							SET_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_BailFromInvite)
							PRINTLN("PROCESS_MISSION_END_STAGE - biITA_BailFromInvite enabled mission failed abort invite to apartment")
						ENDIF
						PRINTLN("PROCESS_MISSION_END_STAGE - g_bFailedMission = TRUE")
					ENDIF
				ENDIF
				
				IF NOT g_bFailedMission
					
					CACHE_VEHICLE_SEAT_FOR_CONTINUITY()
					
					IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
						IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_EarlyEndSpectatorTracking)
						AND NOT IS_BIT_SET(sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_LivesFailSpectator)
							PRINTLN("PROCESS_MISSION_END_STAGE - PROCESS_MISSION_END_STAGE - Setting player failed and went to spectate cam continuity. 2")
							SET_BIT(sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_LivesFailSpectator)
						ENDIF
					ENDIF
								
					IF MC_serverBD.iNextMission < FMMC_MAX_STRAND_MISSIONS
					AND MC_serverBD.iNextMission >= 0
					AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23NextContentID[MC_serverBD.iNextMission])
						//Strand mission is going to be initialised soon
						IF g_FMMC_STRUCT.eBranchingTransitionType[MC_serverBD.iNextMission] = FMMC_BRANCHING_MISSION_TRANSITION_TYPE_FADEOUT
							PRINTLN("PROCESS_MISSION_END_STAGE - PROCESS_MISSION_END_STAGE - Fading out")
							DO_SCREEN_FADE_OUT(1000)
							IF bLocalPlayerPedOK
							AND LocalPlayerCurrentInterior = NULL
							AND NOT IS_ENTITY_IN_WATER(localPlayerPed)
								SetPlayerOnGroundProperly(FALSE, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				TRIGGER_CELEBRATION_PRE_LOAD(sCelebrationData, SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED())
				
				IF bPlayerToUseOK
				AND NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_LATE_SPECTATE)
				AND NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_WAIT_END_SPECTATE)
					g_bMissionRemovedWanted = TRUE
					IF NOT bIsSCTV
						SET_EVERYONE_IGNORE_PLAYER(LocalPlayer,TRUE)
						IF NOT IS_THIS_IS_A_STRAND_MISSION()
						OR (IS_THIS_IS_A_STRAND_MISSION() AND IS_BIT_SET( iLocalBoolCheck5, LBOOL5_SET_UP_STRAND_DETAILS_AT_END_OF_MISSION) AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED())
							SET_PLAYER_WANTED_LEVEL(LocalPlayer,0)
							SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
						ENDIF
						IF PLAYER_IS_THE_DRIVER_IN_A_CAR()
							VEHICLE_INDEX PlayerVehicle 
							PlayerVehicle = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(PlayerVehicle)
								SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE(PlayerVehicle, FALSE)
							ENDIF
						ENDIF
						
						IF SHOULD_TURN_OFF_CONTROL_AT_END_OF_MISSION()
						AND NOT bIsAnySpectator
							NET_SET_PLAYER_CONTROL( LocalPlayer, FALSE, NSPC_PREVENT_FROZEN_CHANGES | NSPC_PREVENT_VISIBILITY_CHANGES | NSPC_PREVENT_COLLISION_CHANGES )
						ENDIF
						
						SET_PED_UNABLE_TO_DROWN( LocalPlayerPed )
					ENDIF
					
					// Gives the player an AI task to try and avoid obstacles whilst they have no control
					IF SHOULD_TURN_OFF_CONTROL_AT_END_OF_MISSION()
						IF IS_PLAYER_IN_A_FLYING_VEHICLE()
						 	IF IS_ENTITY_IN_AIR( GET_VEHICLE_PED_IS_IN(LocalPlayerPed ))
								GIVE_PLAYER_AI_FLY_TASK()
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDELAY_MISSION_END) 
					AND MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_DEFAULT
						IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						AND IS_ENTITY_ALIVE(LocalPlayerPed)
							TASK_WANDER_STANDARD(LocalPlayerPed)
						ENDIF
					ENDIF
				ENDIF
			ELSE // Otherwise the end timer has started
				IF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_LATE_SPECTATE)	//jip, want to get out straight away
					iEndDelay = 0
				ELSE
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDELAY_MISSION_END) 
						IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iteam)
							iEndDelay = 1000
						ENDIF
					ELSE
						IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_USE_CRATE_DELIVERY_DELAY)	//delay when detaching crates from cargobob, TODO B*1622362
							iEndDelay = 0
						ELIF MPGlobalsAmbience.R2Pdata.bOnRaceToPoint = TRUE	//DELAY IF RACE OT POINT IS ACTIVE
							iEndDelay = R2P_MISSION_END_DELAY
							MPGlobalsAmbience.R2Pdata.bForceEnd = TRUE
							PRINTLN("PROCESS_MISSION_END_STAGE    ---------->     RACE TO POINT - bForceEnd - MISSION")
						ELSE
							iEndDelay = 1000
						ENDIF
					ENDIF
				ENDIF
					
				IF SHOULD_CONTINUE_TO_MISSION_END(iendDelay)
				AND NOT bDelayEnd					
					
					IF IS_THIS_SPECTATOR_CAM_RUNNING(g_BossSpecData.specCamData)
						PRINTLN("PROCESS_MISSION_END_STAGE - spectator is running") 
					ENDIF
					IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
						PRINTLN("PROCESS_MISSION_END_STAGE - game state is not end") 
					ENDIF
					
					//-- Stop any alarms
					//If it's not a strand mission
					IF NOT IS_THIS_IS_A_STRAND_MISSION()
					//or a strand mission is not being started
					OR (IS_THIS_IS_A_STRAND_MISSION() AND IS_BIT_SET( iLocalBoolCheck5, LBOOL5_SET_UP_STRAND_DETAILS_AT_END_OF_MISSION) AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED())
						INT iAlarm
						FOR iAlarm = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1)
						    IF isoundid[iAlarm] <> -1
								PRINTLN("PROCESS_MISSION_END_STAGE - [ALARM] Stopping alarm ", iAlarm)
						        STOP_SOUND(isoundid[iAlarm])
					        ENDIF
						ENDFOR
						
						// turn off the casino alarm zones
						IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
							PRINTLN("[JS][ALARM] - PROCESS_MISSION_END_STAGE - Clearing Casino Alarm Zones - FreemodeLite")
							SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_H3_Casino_Alarm_Zone_01_Exterior", FALSE, TRUE)
							SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_H3_Casino_Alarm_Zone_02_Interior", FALSE, TRUE)
						ENDIF
					ENDIF
					
					IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
					AND NOT IS_THIS_SPECTATOR_CAM_RUNNING(g_BossSpecData.specCamData)
					AND IS_MISSION_OVER_REASON_DONE()
						
						SELECT_ENDING_TYPE()
						
						SET_BIT(iLocalBoolCheck5, LBOOL5_BLOCK_TURNING_OFF_ARTIFICIAL_LIGHTS_EOM)
						
						IF NOT IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
							SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN)
						ENDIF
						
						IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_CLEAR_CELEBRATION_SCREEN)
						OR IS_FAKE_MULTIPLAYER_MODE_SET()
							IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
								//Don't clean up the screen just yet, this will be done during the spectator transition.
								IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
								AND NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene)
									SWITCH iClearDeathVisualsStage
										CASE 0
											CLEAR_ALL_BIG_MESSAGES()
											iClearDeathVisualsStage++
										BREAK
										CASE 1
											PRINTLN("PROCESS_MISSION_END_STAGE - [NETCELEBRATION] - [RCC MISSION] - CLEANUP_CELEBRATION_SCREEN call 3.")
											STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "EARLYDEATH")
											CLEANUP_CELEBRATION_SCREEN(sCelebrationData, TRUE, "EARLYDEATH")
											SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
											PRINTLN("PROCESS_MISSION_END_STAGE - clear celebrations early death") 
											SET_BIT(iLocalBoolCheck2,LBOOL2_CLEAR_CELEBRATION_SCREEN)
											IF NOT bLocalPlayerOK
												IF NOT IS_CUTSCENE_PLAYING()
												AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE)
													DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN()
												ENDIF
											ENDIF
										BREAK
									ENDSWITCH
								ELSE
									PRINTLN("PROCESS_MISSION_END_STAGE - ciCELEBRATION_BIT_SET_TriggerEndCelebration bit is set, not clearing celebration screen.") 
									SET_BIT(iLocalBoolCheck2,LBOOL2_CLEAR_CELEBRATION_SCREEN)
								ENDIF
							ENDIF
						ENDIF
						
						TURN_ON_ARTIFICIAL_LIGHTS_LOCALLY(FALSE)
						
						// Make the player everything-proof before we do a cutscene
						SET_ENTITY_INVINCIBLE(LocalPlayerPed, TRUE)
						SET_ENTITY_PROOFS(LocalPlayerPed, TRUE, TRUE, TRUE, TRUE, TRUE)						
						
						IF NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_LB_CAM_READY)
							IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_SHOULD_PROCESS_END_CUTSCENE)
								PRINTLN("PROCESS_MISSION_END_STAGE - Setting LBOOL33_SHOULD_PROCESS_END_CUTSCENE")
								SET_BIT(iLocalBoolCheck33, LBOOL33_SHOULD_PROCESS_END_CUTSCENE)
								
								CACHE_VEHICLE_SEAT_FOR_CONTINUITY()
								
								IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
									IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_EarlyEndSpectatorTracking)
									AND NOT IS_BIT_SET(sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_LivesFailSpectator)
										PRINTLN("[JS][CONTINUITY] - PROCESS_MISSION_END_STAGE - Setting player failed and went to spectate cam continuity.")
										SET_BIT(sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_LivesFailSpectator)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							PRINTLN("PROCESS_MISSION_END_STAGE - limbo")
						ENDIF
					ELSE
						PRINTLN("PROCESS_MISSION_END_STAGE - calling spectator stuff")
						IF NOT IS_BIT_SET(iLocalBoolCheck3, LBOOL3_WAIT_END_SPECTATE)
							BOOL bDelaySpectator = FALSE
							
							IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_PLAYER_LEAVE_AREA_FAIL)
							OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_PLAYER_OUT_BOUNDS_FAIL)
								IF NOT HAS_NET_TIMER_STARTED(timerSpectatorDelay)
									REINIT_NET_TIMER(timerSpectatorDelay)
								ENDIF
								
								IF HAS_NET_TIMER_STARTED(timerSpectatorDelay)
									IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timerSpectatorDelay) <= ciTimerSpectatorDelay
										PRINTLN("PROCESS_MISSION_END_STAGE - bDelaySpectator = TRUE")
										bDelaySpectator = TRUE
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT bDelaySpectator
								PRINTLN("PROCESS_MISSION_END_STAGE - Calling MANAGE_EARLY_END_AND_SPECTATE")
								MANAGE_EARLY_END_AND_SPECTATE()
							ENDIF
						ENDIF
					ENDIF
					
				ELSE
					PRINTLN("PROCESS_MISSION_END_STAGE - waiting for results dealy to finish")
				ENDIF
			ENDIF
		ENDIF

	ELSE 
		
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
		ENDIF
		
		IF bLocalPlayerPedOk
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_CannotBeTargetedByAI, TRUE)
		ENDIF
		
		PRINTLN("PROCESS_MISSION_END_STAGE - results true") 
		
		IF NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet,PBBOOL_ON_LEADERBOARD)
			#IF IS_DEBUG_BUILD
				IF g_SkipCelebAndLbd
					SET_BIT(MC_PlayerBD[iLocalPart].iClientBitSet,PBBOOL_ON_LEADERBOARD)
				ENDIF
			#ENDIF
			IF NOT IS_END_OF_ACTIVITY_HUD_ACTIVE()
			OR IS_A_STRAND_MISSION_BEING_INITIALISED()
				IF DONE_RANK_PREDICTION_READ()
					CLEANUP_THUMBS()
					NETWORK_SET_OVERRIDE_SPECTATOR_MODE(TRUE)
					FOR iteam = 0 TO (FMMC_MAX_TEAMS-1)
						NETWORK_OVERRIDE_TEAM_RESTRICTIONS(iteam,TRUE)
					ENDFOR
					CLIENT_SET_UP_EVERYONE_CHAT()
					CLEAR_ALL_BIG_MESSAGES()
					
					IF NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()	
						START_LBD_AUDIO_SCENE()
					ENDIF
					
					IF DID_I_JOIN_MISSION_AS_SPECTATOR()
						IF IS_SCREEN_FADED_OUT()
						OR IS_SCREEN_FADING_OUT() 
							DO_SCREEN_FADE_IN(1)
							PRINTLN("PROCESS_MISSION_END_STAGE - FADING IN FOR LEADERBOARD")
						ENDIF
					ENDIF
					//Kill face to face converstaion
					IF NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()	
						KILL_FACE_TO_FACE_CONVERSATION()
						PRINTLN("PROCESS_MISSION_END_STAGE - KILL_FACE_TO_FACE_CONVERSATION called")
					ENDIF
					SET_BIT(MC_PlayerBD[iLocalPart].iClientBitSet,PBBOOL_ON_LEADERBOARD)
				ELSE
					PRINTLN("PROCESS_MISSION_END_STAGE - waiting for rank prediction")
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_CLEAR_END_BIG_MESSAGE)
					CLEAR_ALL_BIG_MESSAGES()
					PRINTLN("PROCESS_MISSION_END_STAGE - clear end of activity hud of big messages") 
					SET_BIT(iLocalBoolCheck2,LBOOL2_CLEAR_END_BIG_MESSAGE)
				ENDIF
				PRINTLN("PROCESS_MISSION_END_STAGE - waiting for awards to clear") 
			ENDIF
		ELSE		
			PRINTLN("PROCESS_MISSION_END_STAGE - FMMC2020 - DO LEADERBOARD DRAWING IN HUDPROCESSING")
			
//			// Don't draw the normal leaderboard if we are drawing the Rounds one.
			IF NOT SHOULD_DRAW_ROUNDS_LBD()
				IF NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_NEW_JOB_SCREENS)
				AND NOT (SHOULD_MISSION_USE_EXTERNAL_CELEBRATION_SCREEN() AND HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iTeam))
					MANAGE_LEADERBOARD_DISPLAY()
					
					IF NOT SHOULD_MISSION_USE_EXTERNAL_CELEBRATION_SCREEN()
						UNTOGGLE_RENDERPHASES_FOR_LEADERBOARD()
					ENDIF
				ELSE
					SET_ON_LBD_GLOBAL(FALSE)
				ENDIF
			ENDIF
			
			IF IS_PLAYER_ON_A_PLAYLIST(LocalPlayer)
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdresultstimer) > 6000
					IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
						IF SHOULD_ALLOW_THUMB_VOTE()
						AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
							SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
							PRINTLN("PROCESS_MISSION_END_STAGE - SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)")
						ELIF IS_THIS_A_ROUNDS_MISSION()
							SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
							PRINTLN("PROCESS_MISSION_END_STAGE - SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)IS_PLAYER_ON_A_PLAYLIST ")
						ENDIF
					ELSE
						DO_THUMB_VOTING(sSaveOutVars, sEndOfMission, FALSE, HAS_LEADERBOARD_TIMER_EXPIRED(), TRUE)
						//If I joined as a spectator
						IF IS_PLAYER_SPECTATOR_ONLY(LocalPlayer)
						//and the timer expired
						AND HAS_LEADERBOARD_TIMER_EXPIRED()
						//and i've not voted
						AND NOT FM_PLAY_LIST_HAVE_I_VOTED()
							//Set me as
							FM_PLAY_LIST_I_WANT_TO_PLAY_NEXT()
						ENDIF
					ENDIF
					
					BOOL bSpectate		= FALSE
					BOOL bSkipLbd		= FALSE
					BOOL bNotUsedRemove = FALSE
					BOOL bRaceTimeout 	= HAS_LEADERBOARD_TIMER_EXPIRED()
					
					IF CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS(iPlaylistProgress, 
																		sEndOfMission, 
																		tdresultstimer.Timer, 
																		VIEW_LEADERBOARD_TIME, 
																		"END_LBD_CONTN", 
																		bSpectate, 
																		bSkipLbd, 
																		bNotUsedRemove,
																		bRaceTimeout)
																		
						SET_MC_CLIENT_GAME_STATE(GAME_STATE_MISSION_OVER)
						
						SET_BIT(iLocalBoolCheck,LBOOL_LB_FINISHED)
						
						STOP_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
						
						#IF IS_DEBUG_BUILD
						IF HAS_LEADERBOARD_TIMER_EXPIRED()
							PRINTLN("PROCESS_MISSION_END_STAGE - [PLY_LBD] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS, HAS_LEADERBOARD_TIMER_EXPIRED() TRUE ")
						ELSE
							PRINTLN("PROCESS_MISSION_END_STAGE - [PLY_LBD] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS, HAS_LEADERBOARD_TIMER_EXPIRED() FALSE")
						ENDIF
						IF bSkipLbd
							PRINTLN("PROCESS_MISSION_END_STAGE - [PLY_LBD] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS, bSkipLbd TRUE ")
						ELSE
							PRINTLN("PROCESS_MISSION_END_STAGE - [PLY_LBD] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS, bSkipLbd FALSE")
						ENDIF
						IF FM_PLAY_LIST_HAVE_I_VOTED()
							PRINTLN("PROCESS_MISSION_END_STAGE - [PLY_LBD] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS, FM_PLAY_LIST_HAVE_I_VOTED TRUE")
						ELSE
							PRINTLN("PROCESS_MISSION_END_STAGE - [PLY_LBD] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS, FM_PLAY_LIST_HAVE_I_VOTED FALSE")
						ENDIF
						IF HAS_MISSION_END_CONTINUE_BEEN_PRESSED()
							PRINTLN("PROCESS_MISSION_END_STAGE - [PLY_LBD] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS, HAS_MISSION_END_CONTINUE_BEEN_PRESSED TRUE")
						ELSE
							PRINTLN("PROCESS_MISSION_END_STAGE - [PLY_LBD] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS, HAS_MISSION_END_CONTINUE_BEEN_PRESSED FALSE")
						ENDIF
						
						PRINTLN("PROCESS_MISSION_END_STAGE - [PLY_LBD] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS, RETURN TRUE ")
						#ENDIF
						
					ELSE
						PRINTLN("PROCESS_MISSION_END_STAGE - [RCC MISSION] waiting for CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS ")
					ENDIF
				ENDIF
			ELSE
				IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
					IF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_MOVE_TO_END)
						
						THEFEED_FORCE_RENDER_OFF()
						
						SET_MC_CLIENT_GAME_STATE(GAME_STATE_MISSION_OVER)
						
						STOP_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
						
						IF IS_FAKE_MULTIPLAYER_MODE_SET()
							DO_SCREEN_FADE_OUT(500)
						ELSE
							SET_BIT(iLocalBoolCheck, LBOOL_LB_FINISHED)
							PRINTLN("PROCESS_MISSION_END_STAGE - Setting LBOOL_LB_FINISHED")
						ENDIF
					ELSE
						
						PRINTLN("PROCESS_MISSION_END_STAGE - Else LBOOL3_MOVE_TO_END")
						
						IF NOT IS_BIT_SET(iLocalBoolCheck3, LBOOL3_NEW_JOB_SCREENS)
							BOOL bSCLeaderboardAvailable
							IF IS_THIS_A_RSTAR_ACTIVITY()
							AND NOT bIsSCTV
								bSCLeaderboardAvailable = TRUE
							ELSE
								bSCLeaderboardAvailable = FALSE
							ENDIF
							#IF IS_DEBUG_BUILD
							IF g_SkipCelebAndLbd
								sCelebrationStats.bPassedMission = FALSE
								SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART) 
								FM_PLAY_LIST_SET_ME_AS_VOTED()
							ENDIF
							#ENDIF
														
							IF MAINTAIN_END_OF_MISSION_SCREEN(	lbdVars,
																sEndOfMission, 
																MC_serverBD.sServerFMMC_EOM,
																HAS_LEADERBOARD_TIMER_EXPIRED(),
																TRUE,FALSE,TRUE,TRUE,
																bSCLeaderboardAvailable,tdresultstimer.Timer,VIEW_LEADERBOARD_TIME,"END_LBD_CONTN", FALSE,TRUE, SHOULD_RUN_NEW_VOTING_SYSTEM(sEndOfMission.iQuitProgress, MC_serverBD.sServerFMMC_EOM.iVoteStatus), sCelebrationStats.bPassedMission)
							#IF IS_DEBUG_BUILD
							AND NOT bLBDHold
							#ENDIF
								
								// Show a final accumulative Rounds leaderboard (see DISPLAY_ROUNDS_LBD)
								IF (SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
								OR ROUNDS_MISSION_OVER_FOR_JIP_LOCAL_PLAYER(LocalPlayer))
								AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].bQuitJob
								#IF IS_DEBUG_BUILD
								AND NOT g_SkipCelebAndLbd
								#ENDIF
									PRINTLN("PROCESS_MISSION_END_STAGE - SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD / ROUNDS_MISSION_OVER_FOR_JIP_LOCAL_PLAYER")	
									
									IF bIsLocalPlayerHost
										IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_ROUNDS_LBD_DO)
											SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_ROUNDS_LBD_DO)
											PRINTLN("PROCESS_MISSION_END_STAGE - [CS_RNDS] [HOST] SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD, LBOOL6_SAVED_ROUNDS_DO_ROUNDS_LBD ")
										ENDIF
									ENDIF
								ELSE
									IF SHOULD_RUN_NEW_VOTING_SYSTEM(sEndOfMission.iQuitProgress, MC_serverBD.sServerFMMC_EOM.iVoteStatus)
									AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].bQuitJob
										SET_READY_FOR_NEXT_JOBS(nextJobStruct)
										PRINTLN("PROCESS_MISSION_END_STAGE - Setting LBOOL3_NEW_JOB_SCREENS")
										SET_BIT(iLocalBoolCheck3, LBOOL3_NEW_JOB_SCREENS)
									ELSE
										PRINTLN("PROCESS_MISSION_END_STAGE - Setting LBOOL3_MOVE_TO_END")						
										SET_BIT(iLocalBoolCheck3, LBOOL3_MOVE_TO_END)
									ENDIF
								ENDIF
							ELSE
								DO_THUMB_VOTING(sSaveOutVars, sEndOfMission, FALSE, HAS_LEADERBOARD_TIMER_EXPIRED())
								PRINTLN("PROCESS_MISSION_END_STAGE - waiting for LB votes")
							ENDIF
						ELSE
							IF DO_END_JOB_VOTING_SCREEN(g_sMC_serverBDEndJob, nextJobStruct, g_sMC_serverBDEndJob.timerVotingEnds.Timer)
								SET_BIT(iLocalBoolCheck3,LBOOL3_MOVE_TO_END)
							ELSE
								PRINTLN("PROCESS_MISSION_END_STAGE - waiting for EOJ votes")
							ENDIF
						ENDIF
					ENDIF
				ELSE
					PRINTLN("PROCESS_MISSION_END_STAGE - waiting for GAME_STATE_END")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_END_CUTSCENE_STARTED)
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REACHED_CUT_PROCESS_SHOTS)
			START_CELEBRATION_SCREEN()
		ENDIF
	ENDIF
	
	// FMMC2020 - Ripped from MANAGE_END_CUTSCENE. Needs to be put in an appropriate place above. For now it's based on a local bit that's set where the functions where previously called.
	// LEADERBOARD AND CELEBRATION SCREEN PROCESSING...
	// PROCESS ENDING AND RESULTS ? 
	IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_SHOULD_SHOW_AND_POPULATE_RESULTS)
		IF IS_BIT_SET( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_OVER )
		OR IS_BIT_SET( iLocalBoolCheck2, LBOOL2_NORMAL_END )
			IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
				IF NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_SET_UP_STRAND_DETAILS_AT_END_OF_MISSION )
					PROCESS_INITIALISING_STRAND_MISSION()
					IF IS_A_STRAND_MISSION_BEING_INITIALISED()
					AND NOT IS_MISSION_CONTROLLER_READY_FOR_CUT()
						SET_MISSION_CONTROLLER_READY_FOR_CUT()
					ENDIF
				ENDIF
			ENDIF
			
			POPULATE_RESULTS()
			
			IF IS_A_STRAND_MISSION_BEING_INITIALISED()
				SET_TRANSITION_SESSIONS_SKIP_LB_AND_NJVS()
				SET_BIT(iLocalBoolCheck2,LBOOL2_LB_CAM_READY)
				SET_BIT(iLocalBoolCheck3,LBOOL3_SHOWN_RESULTS)
				EXIT
			ENDIF
			
			COPY_CELEBRATION_DATA_TO_GLOBALS()
			
			IF (SHOULD_MISSION_USE_EXTERNAL_CELEBRATION_SCREEN() AND HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam))
			OR IS_FAKE_MULTIPLAYER_MODE_SET()

				IF IS_FAKE_MULTIPLAYER_MODE_SET()
					IF HAS_CELEBRATION_SCREEN_SKIP_FINISHED()
						SET_BIT(iLocalBoolCheck2,LBOOL2_LB_CAM_READY)
						SET_BIT(iLocalBoolCheck3,LBOOL3_SHOWN_RESULTS)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_NORMAL_END)
					g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoFacePlayerCamCut = TRUE
					PRINTLN("PROCESS_MISSION_END_STAGE - [SAC] - LBOOL2_NORMAL_END = TRUE, setting = TRUE.")
				ELIF NOT IS_FAKE_MULTIPLAYER_MODE_SET() AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
					IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene)
						TRIGGER_END_HEIST_WINNER_CELEBRATION()
					ENDIF
				ENDIF
			
			ELSE
			
				IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
				
					// cutscene not running
					IF NOT IS_CUTSCENE_PLAYING()
					AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE)
						SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
						PRINTLN("PROCESS_MISSION_END_STAGE - MANAGE_END_CUTSCENE - SET ciCELEBRATION_BIT_SET_TriggerEndCelebration - no cutscene running")
						PRINTLN("[NETCELEBRATION] - [SAC] - ciCELEBRATION_BIT_SET_TriggerEndCelebration - call 5.")
					ELSE
						
						iTeam = MC_playerBD[iLocalPart].iteam
						IF iTeam >= 0
						AND iTeam < FMMC_MAX_TEAMS
						
							BOOL bInterruptOnFail
							IF MC_serverBD.iCutsceneID[iTeam] >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
							AND MC_serverBD.iCutsceneID[iTeam] < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
								INT iCutscene = MC_serverBD.iCutsceneID[iTeam] - FMMC_GET_MAX_SCRIPTED_CUTSCENES()
								bInterruptOnFail = IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_InterruptOnFail)
								PRINTLN("PROCESS_MISSION_END_STAGE - MANAGE_END_CUTSCENE - ci_CSBS2_InterruptOnFail = TRUE for MOCAP")
							ENDIF
						
							IF bInterruptOnFail
								SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
								PRINTLN("PROCESS_MISSION_END_STAGE - [NETCELEBRATION] - MANAGE_END_CUTSCENE - SET ciCELEBRATION_BIT_SET_TriggerEndCelebration during cutscene, Allowed by bShouldSkipOnFail(ci_CSBS2_InterruptOnFail)")
								PRINTLN("[NETCELEBRATION] - [SAC] - ciCELEBRATION_BIT_SET_TriggerEndCelebration - call 6.")
							ELSE
								PRINTLN("PROCESS_MISSION_END_STAGE - MANAGE_END_CUTSCENE - Not setting celebration trigger - IS_CUTSCENE_PLAYING = ", BOOL_TO_STRING(IS_CUTSCENE_PLAYING()), 
									" PBBOOL_PLAYING_CUTSCENE = ", BOOL_TO_STRING(IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE)))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_EndCelebrationFinished)
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_BlockCameraFacingPlayerAtEnd)
						IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)						
						AND IS_BIT_SET(iLocalBoolCheck2, LBOOL2_NORMAL_END)
							g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoFacePlayerCamCut = TRUE
							PRINTLN("PROCESS_MISSION_END_STAGE - [SAC] - LBOOL2_NORMAL_END = TRUE, setting = TRUE.")							
						ENDIF
					ENDIF
					
					IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId != -1
					AND HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
						PRINTLN("PROCESS_MISSION_END_STAGE - MANAGE_END_CUTSCENE - Not requesting leaderboard cam as we are using iPostMissionSceneId: ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId)
					ELSE
						REQUEST_LEADERBOARD_CAM()
					ENDIF
					
					PRINTLN("PROCESS_MISSION_END_STAGE - MANAGE_END_CUTSCENE - HAS_CELEBRATION_SCREEN_FINISHED - TRUE")
					
					CLEAR_PRINTS()
					
					IF IS_LEADERBOARD_CAM_RENDERING_OR_INTERPING()	
						SET_BIT(iLocalBoolCheck2, LBOOL2_LB_CAM_READY)
						PRINTLN("PROCESS_MISSION_END_STAGE - MANAGE_END_CUTSCENE - HAS_CELEBRATION_SCREEN_FINISHED - Setting LBOOL2_LB_CAM_READY")
					ENDIF
				ELSE
					PRINTLN("PROCESS_MISSION_END_STAGE - MANAGE_END_CUTSCENE - ciCELEBRATION_BIT_SET_EndCelebrationFinished = FALSE")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Detects if the host has migrated to or from the local player
///    Triggers additional single frame processing if the local player becomes the host
PROC MANAGE_HOST_MIGRATION()

	IF bIsLocalPlayerHost
		IF IS_BIT_SET(iLocalBoolCheck,WAS_NOT_THE_HOST)
			
			PROCESS_SINGLE_FRAME_PARTICIPANT_UPDATE()
			
			INT iTeam = 0
			
			FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
				RECALCULATE_OBJECTIVE_LOGIC(iTeam)
			ENDFOR
			
			CWARNINGLN(DEBUG_CONTROLLER, "[MC_HOST_MIGRATED] HOST MIGRATED TO ME") 
			CLEAR_BIT(iLocalBoolCheck, WAS_NOT_THE_HOST)
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iLocalBoolCheck, WAS_NOT_THE_HOST)
			CWARNINGLN(DEBUG_CONTROLLER, "[MC_HOST_MIGRATED] NO LONGER THE HOST - new host = ", NATIVE_TO_INT(NETWORK_GET_HOST_OF_THIS_SCRIPT()))
			SET_BIT(iLocalBoolCheck, WAS_NOT_THE_HOST)
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_RESET_VARIABLES_FOR_NEW_OBJECTIVE(INT iTeam)
	IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_BLOCK_OBJECTIVE_TEXT_UNTIL_RULE_CHANGE)
		PRINTLN("[JS] PROCESS_PRE_MAIN_CHECKS - CLEARING LBOOL24_BLOCK_OBJECTIVE_TEXT_UNTIL_RULE_CHANGE")
		CLEAR_BIT(iLocalBoolCheck24, LBOOL24_BLOCK_OBJECTIVE_TEXT_UNTIL_RULE_CHANGE)
	ENDIF
	
	IF iTeam = MC_playerBD[iLocalPart].iTeam 
		MC_playerBD[iLocalPart].iGranularCurrentPoints = 0
		MC_playerBD[iLocalPart].iGranularObjCapturingBS = 0
		RESET_NET_TIMER(tdGranularCaptureTimer)
		
		IF NOT IS_LONG_BITSET_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iClearPrereqsOnRuleBS[GET_TEAM_CURRENT_RULE(iTeam)])
			RESET_PREREQUISITES_COMPLETED_FROM_LONG_BITSET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iClearPrereqsOnRuleBS[GET_TEAM_CURRENT_RULE(iTeam)])			
		ENDIF
		
		IF NOT IS_LONG_BITSET_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSetPrereqsOnRuleBS[GET_TEAM_CURRENT_RULE(iTeam)])
			SET_PREREQUISITES_COMPLETED_FROM_LONG_BITSET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSetPrereqsOnRuleBS[GET_TEAM_CURRENT_RULE(iTeam)])			
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_MISSION_CURRENT_OBJECTIVE_FAILED_BY_BOUNDS)
		PRINTLN("[NewObjectiveResetVars] - Resetting PBBOOL4_MISSION_CURRENT_OBJECTIVE_FAILED_BY_BOUNDS")
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_MISSION_CURRENT_OBJECTIVE_FAILED_BY_BOUNDS)
	ENDIF
		
	CLEAR_BIT(iLocalBoolCheck30, LBOOL30_PRIORITY_VEHICLE_IS_AVENGER) // If we're delivering an Avenger "AND" another vehicle on the same rule then this bool will need to be reset when the avenger is invalidated. Dubious as to whether this setup should be supported.
	CLEAR_BIT(iLocalBoolCheck18, LBOOL18_RULE_CHANGE_SHARD_PLAYED_TEAM_0 + iTeam)
	CLEAR_BIT(iLocalBoolCheck19, LBOOL19_CHECKED_DROPOFF_CUTSCENE)
	CLEAR_BIT(iLocalBoolCheck33, LBOOL33_LOCAL_PLAYER_BEEN_PROPERLY_SUBMERGED_THIS_RULE)
	
	MC_Playerbd[iLocalPart].iVehLeftBS = 0
	MC_Playerbd[iLocalPart].iObjLeftBS = 0
	INT i = 0
	FOR i = 0 TO ciFMMC_PED_BITSET_SIZE-1
		MC_Playerbd[iLocalPart].iPedLeftBS[i] = 0
	ENDFOR	
	
	FOR i = 0 TO (MC_serverBD.iMaxLoopSize-1)
		IF i < FMMC_MAX_PEDS
			IF bIsLocalPlayerHost
				MC_serverBD_3.iNumberOfPlayersLeftLeaveEntity[iTeam][i] = 0
			ENDIF
			
			iTempNumberOfPlayersLeftLeaveEntity[iTeam][i] = 0
						
			CLEAR_BIT(sMissionPedsLocalVars[i].iPedBS, ci_MissionPedBS_SyncScene_ProgressedAnimStageThisRule)
			CLEAR_BIT(sMissionPedsLocalVars[i].iPedBS, ci_MissionPedBS_SyncScene_DoProgressedAnimStageThisRuleEvent)
		ENDIF
	ENDFOR
	
	IF bIsLocalPlayerHost
		PRINTLN("[NewObjectiveResetVars] - Resetting SBBOOL8_FORCE_RAPPELLING_FROM_HELICOPTERS")
		CLEAR_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_FORCE_RAPPELLING_FROM_HELICOPTERS)
		
		FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1
			MC_ServerBD_4.iPedCurrentPriorityGotoLocCompletedBS[i] = 0
		ENDFOR
	ENDIF
	
	iMinigameObjBlipBlockerBS = 0
	iMinigameObjPreReqBlockerBS = 0
	
	RESET_NET_TIMER(tdScoreAndLapSyncTimer)
	START_NET_TIMER(tdScoreAndLapSyncTimer)
ENDPROC

PROC PROCESS_ON_NEW_OBJECTIVE(INT iTeam)

	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF bIsLocalPlayerHost
		SET_BIT(MC_ServerBD.iServerBitSet8, SBBOOL8_RULE_CHANGE_TRIGGERED_AGGRO_INDEX_PREREQS_CHECK)
		CHECK_ALL_AGGRO_INDEX_PREREQUISITES()
	ENDIF
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iForcedHintCamEntityType[iRule] != ciFMMC_ENTITY_LINK_TYPE_NONE
	AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iForcedHintCamEntityID[iRule] > -1
		PRINTLN("PROCESS_ON_NEW_PRIORITY - Pointing Hint Cam at Target. ID: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iForcedHintCamEntityID[iRule], " Type: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iForcedHintCamEntityType[iRule])
		TRIGGER_FORCED_HINT_CAM(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iForcedHintCamEntityType[iRule], g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iForcedHintCamEntityID[iRule], g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iForcedHintCamDuration[iRule])
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_END_OF_MISSION_INVINCIBILITY)
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		MAKE_PLAYER_INVINCIBLE_ONCE_MISSION_IS_OVER()
		PRINTLN("PROCESS_ON_NEW_PRIORITY - MAKE_PLAYER_INVINCIBLE_ONCE_MISSION_IS_OVER")
	ENDIF
	
	SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_FULL_ZONES_AND_BOUNDS_LOOP_REQUIRED)
	PRINTLN("PROCESS_ON_NEW_PRIORITY - Requesting zone loop")
			
	IF iBoundsToShowObjectiveText != -1
		CLEAR_BOUNDS_OBJECTIVE_TEXT(iBoundsToShowObjectiveText)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_ENABLE_CASINO_PARTY_PEDS)
		PRINTLN("PROCESS_ON_NEW_PRIORITY - g_bTurnOnMissionPenthousePartyPeds = TRUE on this rule")
		g_bTurnOnMissionPenthousePartyPeds = TRUE
	ELSE
		IF g_bTurnOnMissionPenthousePartyPeds 
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_ENABLE_HIDE_CASINO_PARTY_PEDS_DELETION_BEHIND_CUT)
				SET_BIT(iLocalBoolCheck28, LBOOL28_DISABLE_CASINO_PEDS_DURING_CUTSCENE)
				PRINTLN("PROCESS_ON_NEW_PRIORITY - ciBS_RULE15_ENABLE_HIDE_CASINO_PARTY_PEDS_DELETION_BEHIND_CUT enabled. Will set g_bTurnOnMissionPenthousePartyPeds to false during cutscene.")
			ELSE			
				g_bTurnOnMissionPenthousePartyPeds = FALSE
				PRINTLN("PROCESS_ON_NEW_PRIORITY - g_bTurnOnMissionPenthousePartyPeds = FALSE on this rule")
			ENDIF
		ENDIF
	ENDIF
	
	// If Team Lives Override is set to Infinite
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayerLivesPerRule[iRule] = 0
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_LAST_LIFE)
		CLEAR_BIT(iLocalBoolCheck, LBOOL_ON_LAST_LIFE)
		SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN)
	ENDIF
	
	PROCESS_RAGDOLL_ON_RULE(iTeam)
	PROCESS_FADE_OUT_ON_RULE(iTeam)
	PROCESS_FADE_IN_ON_RULE(iTeam)
	
ENDPROC

PROC PROCESS_RELATIONSHIP_GROUP_CHANGE_ON_RULE()
	
	INT iRule
	INT iTeam		
	
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		
		iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
		IF iRule >= FMMC_MAX_RULES
			RELOOP
		ENDIF
		
		IF IS_PLAYER_TEAM_MISSION_RULE_RELATIONSHIP_MODEL_SWAP_BLOCKING_CHANGES(iTeam, iRule)
		
			IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_SET_UP_ON_RULE_TEAM_REL_GROUP_TEAM_0 + iTeam)
				PRINTLN("FM_Relationships - iTeam: ", iTeam, " SET_PLAYER_TEAM_TO_TEAM_MISSION_RELATIONSHIPS_ON_RULE is no longer valid. Clearing LBOOL28_SET_UP_ON_RULE_TEAM_REL_GROUP and resetting rel groups and targeting restrictions.")
				SET_PLAYER_TEAM_TO_TEAM_MISSION_RELATIONSHIPS()
				IF iTeam = MC_PlayerBD[iLocalPart].iTeam
					SET_PED_RELATIONSHIP_GROUP_HASH(LocalPlayerPed, rgfm_PlayerTeam[iTeam])
				ENDIF
				SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS(iRule)
				CLEAR_BIT(iLocalBoolCheck28, LBOOL28_SET_UP_ON_RULE_TEAM_REL_GROUP_TEAM_0 + iTeam)
			ENDIF
		
		ELSE
			
			IF SET_PLAYER_TEAM_TO_TEAM_MISSION_RELATIONSHIPS_ON_RULE(iTeam, iRule)			
				IF NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_SET_UP_ON_RULE_TEAM_REL_GROUP_TEAM_0 + iTeam)
					PRINTLN("FM_Relationships - iTeam: ", iTeam, " SET_PLAYER_TEAM_TO_TEAM_MISSION_RELATIONSHIPS_ON_RULE called and returned true. Setting up rel groups and targeting restrictions.")
					IF iTeam = MC_PlayerBD[iLocalPart].iTeam
						SET_PED_RELATIONSHIP_GROUP_HASH(LocalPlayerPed, rgfm_PlayerTeam[iTeam])
					ENDIF
					SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS(iRule)
					SET_BIT(iLocalBoolCheck28, LBOOL28_SET_UP_ON_RULE_TEAM_REL_GROUP_TEAM_0 + iTeam)
				ENDIF
			ENDIF		
				
		ENDIF
	ENDFOR
	
ENDPROC

PROC PROCESS_CLIENT_GAME_STATE_RUNNING_UNSORTED()
	
	//[FMMC2020] This needs sorted into the appropriate headers
	//			- Each header's main processing function should be called from here and nothing else.
	
	INT i
	INT iTeam
				
	PROCESS_SPECTATOR_MODES_AND_LOCAL_PLAYER_VARIABLES() //Leave this above PROCESS_MAIN_STATE!
	
	CLEAR_BIT(iLocalBoolCheck7, LBOOL7_HOST_APPLIED_NEW_PRIORITY_MIDFRAME)
	CLEAR_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
	CLEAR_BIT(iLocalBoolCheck7, LBOOL7_A_TEAM_HAS_A_NEW_PRIORITY_THIS_FRAME)
	CLEAR_BIT(iLocalBoolCheck7, LBOOL7_A_TEAM_HAS_NEW_MIDPOINT_THIS_FRAME)
	
	FOR iTeam = 0 TO ( MC_serverBD.iNumberOfTeams - 1 )
		CLEAR_BIT(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		CLEAR_BIT(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_MIDPOINT_THIS_FRAME + iTeam)
		
		IF iOldPriority[iTeam] != MC_ServerBD_4.iCurrentHighestPriority[iTeam]
		OR iOldMidpoint[iTeam] != MC_serverBD.iObjectiveMidPointBitset[iTeam]
			IF SHOULD_IGNORE_CHECKPOINT_PROCESSING_DUE_TO_RULE_SKIP(iTeam)
				CLEAR_IGNORE_CHECKPOINT_PROCESSING_FOR_RULE_SKIP(iTeam)
			ELSE
				PROCESS_TEAM_RESTART_CHECKPOINTS(iTeam, iOldPriority[iTeam], iOldMidpoint[iTeam])
				PROCESS_TEAM_RESTART_MULTI_RULE_TIMER_RESETS(iTeam, iOldPriority[iTeam], iOldMidpoint[iTeam])
			ENDIF
		ENDIF
		
		IF iOldPriority[iTeam] != MC_ServerBD_4.iCurrentHighestPriority[iTeam]
			
			IF bIsLocalPlayerHost
				CLEAR_BIT(MC_serverBD.iServerBitSet2,SBBOOL2_NEW_PARTICIPANT_LOOP_STARTED)
				CLEAR_BIT(MC_serverBD.iServerBitSet2,SBBOOL2_NEW_PARTICIPANT_LOOP_FINISHED)
				IF IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_ALL_PLAYERS_IN_ANY_LOCATE_PASSED)
					CLEAR_BIT(MC_serverBD.iServerBitSet4, SBBOOL4_ALL_PLAYERS_IN_ANY_LOCATE_PASSED)
				ENDIF

				//Vehicle density per rule:
				IF MC_serverBD_4.iCurrentHighestPriority[iTeam] > -1
				AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES

					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleTraffic[MC_serverBD_4.iCurrentHighestPriority[iTeam]] != RULE_TRAFFIC_DEFAULT
						MC_serverBD.fVehicleDensity  = GET_MISSION_VEHICLE_DENSITY_FROM_INT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleTraffic[MC_serverBD_4.iCurrentHighestPriority[iTeam]])
					ENDIF
					
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRulePedDensity[MC_serverBD_4.iCurrentHighestPriority[iTeam]] != RULE_TRAFFIC_DEFAULT
						MC_serverBD.fPedDensity = GET_PED_DENSITY_FROM_INT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRulePedDensity[MC_serverBD_4.iCurrentHighestPriority[iTeam]])
					ENDIF
					
				ENDIF
				MC_serverBD.iGranularCurrentPoints[iTeam] = 0

				IF MC_serverBD_4.iCurrentHighestPriority[iTeam] > -1
				AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
					IF MC_serverBD.iUndeliveredVehCleanupBS[iTeam] > 0
						FOR i = 0 TO MC_serverBD.iNumVehCreated - 1
							IF IS_BIT_SET(MC_serverBD.iUndeliveredVehCleanupBS[iTeam], i)
								IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
									INT iteam2
									FOR iteam2 = 0 TO (MC_serverBD.iNumberOfTeams-1)
										CLEAR_BIT(MC_serverBD.iVehteamFailBitset[i],iteam2)
										CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iteam2], i)
									ENDFOR
									IF IS_VEHICLE_USING_RESPAWN_POOL(i)
										SET_VEHICLE_RESPAWNS(i, 0)
									ENDIF
									CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
									SET_BIT(MC_serverBD.iCleanedUpUndeliveredVehiclesBS, i)
									PRINTLN("[JS] PROCESS_PRE_MAIN_CHECKS - Veh ",i," was not delivered last rule, marking as no longer needed")
								ENDIF
							ENDIF
						ENDFOR
						MC_serverBD.iUndeliveredVehCleanupBS[iTeam] = 0
					ENDIF
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE9_CLEANUP_UNDELIVERED_VEHICLES)
						FOR i = 0 TO MC_serverBD.iNumVehCreated - 1
							IF MC_serverBD_4.ivehRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
							AND MC_serverBD_4.iVehPriority[i][iTeam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]
								IF NOT IS_BIT_SET(MC_serverBD.iUndeliveredVehCleanupBS[iTeam], i)
									PRINTLN("[JS] PROCESS_PRE_MAIN_CHECKS - Veh ",i," will be cleaned up if not delivered this rule")
									SET_BIT(MC_serverBD.iUndeliveredVehCleanupBS[iTeam], i)
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
					
					//Option to reset multirule timer of other teams
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[MC_ServerBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE11_INVALIDATE_MULTIRULE_TIMERS)
						PRINTLN("[RCC MISSION][MultiRuleTimer] PROCESS_PRE_MAIN_CHECKS - Team ", iteam, " have reached a rule that resets multirule timers")
						INT iTempTeam
						FOR iTempTeam = 0 TO FMMC_MAX_TEAMS - 1
							MC_serverBD_3.iMultiObjectiveTimeLimit[iTempTeam] = 0
							RESET_NET_TIMER(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTempTeam])
							PRINTLN("[RCC MISSION][MultiRuleTimer] PROCESS_PRE_MAIN_CHECKS - team ", iTempTeam, " having multirule timer reset")
						ENDFOR
					ENDIF
					
				ENDIF
			ENDIF
			
			PROCESS_RESET_VARIABLES_FOR_NEW_OBJECTIVE(iTeam)
			PROCESS_ON_NEW_OBJECTIVE(iTeam)
			
			IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_TEAM_0_IS_PAST_THEIR_FIRST_RULE + iTeam)
			AND iOldPriority[iTeam] != -1
				SET_BIT(iLocalBoolCheck16, LBOOL16_TEAM_0_IS_PAST_THEIR_FIRST_RULE + iTeam)
			ENDIF
									
			iOldPriority[iTeam] = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
			SET_TEAM_HAS_NEW_PRIORITY_THIS_FRAME(iTeam)
			
			CHECK_IF_NEXT_RULE_IS_CUTSCENE_RULE_FOR_ANY_FRIENDLY_TEAM( iteam )
			
			PRINTLN("[RCC MISSION] OBJECTIVE_TEXT - LBOOL6_TEAM_", iTeam, "_HAS_NEW_PRIORITY_THIS_FRAME - SET We are now on Rule Priority: ", MC_serverBD_4.iCurrentHighestPriority[iTeam])					
		ENDIF
		
		IF iOldMidpoint[iTeam] != MC_serverBD.iObjectiveMidPointBitset[iTeam]
			iOldMidpoint[iTeam] = MC_serverBD.iObjectiveMidPointBitset[iTeam]
			SET_BIT(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_MIDPOINT_THIS_FRAME + iTeam)
			PRINTLN("[RCC MISSION] OBJECTIVE_TEXT - LBOOL6_TEAM_", iTeam, "_HAS_NEW_MIDPOINT_THIS_FRAME - SET")
		ENDIF

		//Handle team wanted level
		IF NOT HAS_NET_TIMER_STARTED(tdRespawnTimer)
		OR GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdRespawnTimer) >= 7000
			IF MC_serverBD.iNumOfWanted[iTeam] > 0
			OR MC_serverBD.iNumOfFakeWanted[iTeam] > 0
				PRINTLN("[MJM] MC_serverBD.iNumOfWanted[",iTeam,"] = ",MC_serverBD.iNumOfWanted[iTeam])
				IF NOT IS_BIT_SET(iWasTeamWantedBS,iTeam)
					SET_BIT(iWasTeamWantedBS,iTeam)
					SET_BIT(iLocalBoolCheck4,LBOOL4_T0GOTWANTED + iTeam)
					PRINTLN("[MJM] SET_BIT(iLocalBoolCheck4,LBOOL4_T0GOTWANTED + iTeam)")
				ENDIF
			ELSE
				IF IS_BIT_SET(iWasTeamWantedBS,iTeam)
					CLEAR_BIT(iWasTeamWantedBS,iTeam)
					SET_BIT(iLocalBoolCheck4,LBOOL4_T0LOSTWANTED + iTeam)
					PRINTLN("[MJM] SET_BIT(iLocalBoolCheck4,LBOOL4_T0LOSTWANTED + iTeam)")
				ENDIF
			ENDIF
			IF HAS_NET_TIMER_STARTED(tdRespawnTimer)
				RESET_NET_TIMER(tdRespawnTimer)
			ENDIF
		ENDIF
		
		//Dialogue points:
		IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
		AND (IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
			OR IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_MIDPOINT_THIS_FRAME + iTeam))
			
			//Handle team wanted level
			IF iOldRule2[iTeam] != iNewRule2[iTeam]
				PRINTLN("[MJM] iOldRule2[",iTeam,"] != iNewRule2[",iTeam,"] ",iOldRule2[iTeam]," != ",iNewRule2[iTeam])
				IF MC_serverBD.iNumOfWanted[iTeam] > 0
				OR MC_serverBD.iNumOfFakeWanted[iTeam] > 0
					PRINTLN("[MJM] MC_serverBD.iNumOfWanted[",iTeam,"] = ",MC_serverBD.iNumOfWanted[iTeam])
					SET_BIT(iLocalBoolCheck4,LBOOL4_T0GOTWANTED + iTeam)
					PRINTLN("[MJM] SET_BIT(iLocalBoolCheck4,LBOOL4_T0GOTWANTED + iTeam)")
				ELSE
					CLEAR_BIT(iLocalBoolCheck4,LBOOL4_T0GOTWANTED + iTeam)
					PRINTLN("[MJM] CLEAR_BIT(iLocalBoolCheck4,LBOOL4_T0GOTWANTED + iTeam)")
				ENDIF
				CLEAR_BIT(iLocalBoolCheck4,LBOOL4_T0LOSTWANTED + iTeam)
				iOldRule2[iTeam] = iNewRule2[iTeam]
			ENDIF
							
			IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],MC_serverBD_4.iCurrentHighestPriority[iTeam]) //If we've reached the midpoint
				iOldDialoguePoints[iTeam] = MC_serverBD.iTeamScore[iTeam]
			ENDIF
		ENDIF
		
		//If I'm entering/leaving spectate, re-check all these things:
		IF NOT (MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iTeam] < FMMC_MAX_RULES 
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iTeam].iRuleBitsetTen[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iTeam]], ciBS_RULE10_FORCE_HEIST_SPECTATE))
			IF iSpectatorTarget != -1
				IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_PRE_MAIN_WAS_A_SPECTATOR_CACHE)
					PRINTLN("SPECTATOR IS SETTING THEMSELVES AS BEING ON A NEW PRIORITY 1")
					SET_TEAM_HAS_NEW_PRIORITY_THIS_FRAME(iTeam)
					SET_TEAM_HAS_NEW_MIDPOINT_THIS_FRAME(iTeam)
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_PRE_MAIN_WAS_A_SPECTATOR_CACHE)
					PRINTLN("SPECTATOR IS SETTING THEMSELVES AS BEING ON A NEW PRIORITY 2")
					SET_TEAM_HAS_NEW_PRIORITY_THIS_FRAME(iTeam)
					SET_TEAM_HAS_NEW_MIDPOINT_THIS_FRAME(iTeam)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

	IF ((MC_serverBD.fVehicleDensity != fNewVehicleDensity) OR (MC_serverBD.fPedDensity != fNewPedDensity))
		
		IF iPopulationHandle > -1 //it exists, so better clear it
			PRINTLN("[RCC MISSION][Zones] PROCESS_PRE_MAIN_CHECKS - We have a global density iPopulationHandle of ",iPopulationHandle,", clear the existing densities before re-adding")
			MC_CLEAR_PED_AND_TRAFFIC_DENSITIES(iPopulationHandle, bVehicleGenerator)
			MC_SET_PED_AND_TRAFFIC_DENSITIES(iPopulationHandle, bVehicleGenerator, MC_serverBD.fPedDensity, MC_serverBD.fVehicleDensity)
		ELSE
			PRINTLN("[RCC MISSION][Zones] PROCESS_PRE_MAIN_CHECKS - We have a global density iPopulationHandle of ",iPopulationHandle,", just add the new global density")
			MC_SET_PED_AND_TRAFFIC_DENSITIES(iPopulationHandle, bVehicleGenerator, MC_serverBD.fPedDensity, MC_serverBD.fVehicleDensity)
		ENDIF
		
		IF iZoneCreatedBS != 0
			PRINTLN("[RCC MISSION][Zones] PROCESS_PRE_MAIN_CHECKS - The global density has changed, so we need to re-add any vehicle gens & roads from zones (as they will have just been clobbered by the global change)")
			
			INT iZone
			FOR iZone = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfZones - 1)
				IF DOES_ZONE_EXIST(iZone)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType = ciFMMC_ZONE_TYPE__CLEAR_VEHICLES
					OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType = ciFMMC_ZONE_TYPE__CLEAR_ALL
						SET_VEHICLE_GENERATORS_AND_ROADS_INACTIVE_FOR_ZONE(iZone, TRUE, iBitsetVehicleGeneratorsActiveArea, iBitsetSetRoadsArea, TRUE)
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
		IF bIsLocalPlayerHost
			IF MC_serverBD.fPedDensity = 0
			AND MC_serverBD.fVehicleDensity = 0
				IF MC_serverBD_3.scenBlockServer = NULL
					SERVER_BLOCK_ALL_SCENARIOS(MC_serverBD_3.scenBlockServer)
				ENDIF
			ELSE
				IF MC_serverBD_3.scenBlockServer != NULL
					SERVER_CLEAR_BLOCKING_ALL_SCENARIOS(MC_serverBD_3.scenBlockServer)
				ENDIF
			ENDIF
		ENDIF
		
		fNewPedDensity 		= MC_serverBD.fPedDensity
		fNewVehicleDensity = MC_serverBD.fVehicleDensity
		PRINTLN("[RCC MISSION][Zones] PROCESS_PRE_MAIN_CHECKS - Setting new vehicle density of ", fNewVehicleDensity, ", new ped density of ", fNewPedDensity)
	ENDIF
	
	IF iSpectatorTarget != -1
		SET_BIT(iLocalBoolCheck7, LBOOL7_PRE_MAIN_WAS_A_SPECTATOR_CACHE)
	ELSE
		CLEAR_BIT(iLocalBoolCheck7, LBOOL7_PRE_MAIN_WAS_A_SPECTATOR_CACHE)
		
		// 2172439
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
			
			PRINTLN("[CHAT_FUNC] PROCESS_PRE_MAIN_CHECKS, CLIENT_SET_UP_TEAM_ONLY_CHAT, GET_MISSION_CHAT_PROXIMITY = ", GET_MISSION_CHAT_PROXIMITY())
			CLIENT_SET_UP_TEAM_ONLY_CHAT(GET_MISSION_CHAT_PROXIMITY())
		
			IF IS_CREATOR_VOICE_CHAT_RESTRICTED()
				PRINTLN("[CHAT_FUNC] PROCESS_PRE_MAIN_CHECKS, IS_CREATOR_VOICE_CHAT_RESTRICTED, NETWORK_SET_PROXIMITY_AFFECTS_TEAM(TRUE)")
				NETWORK_SET_PROXIMITY_AFFECTS_TEAM(TRUE)
			ENDIF
			
			IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_IS_AIMING_AT_PROTECT_PED)
				CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_IS_AIMING_AT_PROTECT_PED)
				PRINTLN("[RCC MISSION] PROCESS_PRE_MAIN_CHECKS - Clearing PBBOOL4_PLAYER_IS_AIMING_AT_PROTECT_PED due to new rule")
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
	AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
		IF IS_THIS_MODE_ALLOWING_SPECTATOR_CROSS_TALK()
			ALLOW_SPECTATORS_TO_CHAT(TRUE)
		ENDIF
	ENDIF
	
	IF iVehBlipCreatedThisFrameBitset != 0
		CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle blip creation was blocked for some vehicles last frame. Clearing blocks for new frame.")
		iVehBlipCreatedThisFrameBitset = 0
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_CONTROL_PAD_COLOUR_SHOULD_BE_RESET)
		CLEAR_CONTROL_LIGHT_EFFECT(PLAYER_CONTROL)
		CLEAR_BIT(iLocalBoolCheck13, LBOOL13_CONTROL_PAD_COLOUR_SHOULD_BE_RESET)
	ENDIF
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
	AND NOT g_bInMultiplayer
	AND IS_BIT_SET(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_TRACKIFY)
		g_bInMultiplayer = TRUE
		PRINTLN("[RCC MISSION] PROCESS_PRE_MAIN_CHECKS - Setting g_bInMultiplayer to true in the creator as it was off")
	ENDIF
	
	IF MC_playerBD[iLocalPart].iteam > -1
	AND MC_playerBD[iLocalPart].iteam < FMMC_MAX_TEAMS
	AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] > -1
	AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
		iPrevValidRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]
	ENDIF
	
	fLastFrameTime = GET_FRAME_TIME()

	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_SET_NOT_JOINABLE)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		IF NOT IS_BIT_SET(iLocalBoolCheck, LBOOL_MISSION_SET_NOT_JOINABLE)
			SET_FM_MISSION_AS_NOT_JOINABLE()
			SET_BIT(iLocalBoolCheck,LBOOL_MISSION_SET_NOT_JOINABLE)
		ENDIF
	ENDIF

	MANAGE_HIDING_HUD_WAITING_FOR_GODTEXT()
	
	PROCESS_PLAYER_USING_PLACED_PED_TAXI()
	
	MANAGE_HOST_MIGRATION()
	
	PROCESS_SYNC_CHECKPOINT_TRANSITION_VARIABLES()	
	
	PROCESS_SHOPS_FOR_INSTANCED_CONTENT()
	
	PROCESS_SECUROHACK_OBJECTIVE_CONTROL()	
	
	//Render target for Laptop hack
	PROCESS_HACKING_LAPTOP_SCALEFORM()
		
	PROCESS_ALWAYS_SHOW_ONE_ENEMY_BLIP()
	
	//This function draws the HUD for Kill Player rules	
	IF NOT IS_SPECTATOR_HUD_HIDDEN()
	AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
		DISPLAY_PLAYER_KILL_HUD()
	ENDIF

	IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_LINE_MARKER_ACTIVE_ON_MISSION)
		//Reset Cross the Line data prior to each loop
		PLAYER_STATE_LIST_INIT(playerStateListDataCrossTheLine)
	ENDIF
	
	// Manual respawn vehicles are dealt with separately from creator vehicles
	CHECK_MANUAL_RESPAWN_VEHICLE()
		
	PROCESS_SPECIAL_VEHICLE_WEAPON_GLOBAL_OPTIONS()

	MANAGE_CASH_GRAB_TAKE_AND_DROP(SHOULD_HIDE_THE_HUD_THIS_FRAME())
	MANAGE_DRILLING_AUDIO_REQUESTS()
	UPDATE_THERMITE_DRIP_EFFECT()
	UPDATE_LOCAL_THERMITE_EFFECTS() //url:bugstar:6058740
	UPDATE_THERMITE_REMAINING_HUD(SHOULD_HIDE_THE_HUD_THIS_FRAME())
	MANAGE_VAULT_DRILLING_ASSET_REQUESTS__FMMC()
	UPDATE_LOCAL_VAULT_DRILL_PTFX__FMMC(sVaultDrillData, iLocalPart, MC_serverBD_1.sFMMC_SBD.niNetworkedObject)
	PROCESS_CAMERA_FILTER()
	
	PROCESS_WORLD_ALARMS()
	
	PROCESS_DRONE_USAGE()
	
	PROCESS_INTERIORS_EVERY_FRAME()
	
	PROCESS_IPLS_EVERY_FRAME()
	
	PROCESS_CARNAGE_BAR_SERVER()
	
	PROCESS_CARNAGE_BAR_CLIENT()
		
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		MAINTAIN_MISSION_ORBITAL_CANNON_FOR_CREATOR()
	ENDIF
	
	PROCESS_MISSION_ORBITAL_CANNON()

	PROCESS_LOCAL_PLAYER_ABILITY_STATE__SCRIPT_HOST()
	PROCESS_LOCAL_PLAYER_ABILITY_STATE()

	//Player blip processing	
	PROCESS_PLAYER_BLIPS()

	//This function draws the Package HUD for Get & Deliver / Collect & Hold rules
	IF SHOULD_DRAW_PACKAGE_HUD()
		DRAW_PACKAGE_HUD()
	ENDIF	

	//This function draws the captures HUD for capture rules
	IF NOT IS_SPECTATOR_HUD_HIDDEN()
	AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
		IF NOT IS_OBJECTIVE_BLOCKED()
			DRAW_ENTITY_RESPAWN_RECAPTURE_HUD()
		ENDIF
	ENDIF
	
	//Prop processing - this spawns and cleans up all special props, as well as doing per-prop behaviour, like alarm sounds, flare VFX and monkey behaviours	
	// Start the timer
	IF NOT HAS_NET_TIMER_STARTED(tdspeedBoostFlashTimerCreator)
		START_NET_TIMER(tdspeedBoostFlashTimerCreator)
	ENDIF
	
	// Reset the Timer so that we return false (alpha 100)
	IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdspeedBoostFlashTimerCreator, SPEED_BOOST_FLASH_TIME_CREATOR_OFF)
		RESET_NET_TIMER(tdspeedBoostFlashTimerCreator)
		START_NET_TIMER(tdspeedBoostFlashTimerCreator)
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		PROCESS_CLEANUP_PROPS_EARLY()
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps > 0
	AND g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps <= GET_FMMC_MAX_WORLD_PROPS()
		PROCESS_WORLD_PROP(iWorldPropIterator)
		iWorldPropIterator++
		IF iWorldPropIterator >= g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps
			iWorldPropIterator = 0
		ENDIF
	ENDIF
		
	INT ii
	FOR ii = 0 TO FMMC_MAX_TEAMS-1
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + ii)
			SET_BIT(iLocalBoolCheck25, LBOOL25_RESET_ALL_PROPS_ON_RENDERPHASE_PAUSE)
			PRINTLN("[LM][PROCESS_PROP_EVERY_FRAME] - Setting LBOOL25_RESET_ALL_PROPS_ON_RENDERPHASE_PAUSE")
			BREAKLOOP
		ENDIF
	ENDFOR
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		INT iProp = 0
		FOR iProp = 0 TO FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME-1
			PROCESS_PROP_CLEAN_UP_WITH_FADE(iProp)
		ENDFOR
	
	ELIF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)	
		INT iPropEF
		FOR iPropEF = 0 TO FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME-1
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_PropFadeoutEveryFrameIndex[iPropEF])
				REMOVE_PARTICLE_FX(ptfx_PropFadeoutEveryFrameIndex[iPropEF], TRUE)
			ENDIF
		ENDFOR
	ENDIF
	
	PROCESS_PROP_CLEANUP_LOGIC()
	
	IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_RESET_ALL_PROPS_ON_RENDERPHASE_PAUSE)
	AND NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
		CLEAR_BIT(iLocalBoolCheck25, LBOOL25_RESET_ALL_PROPS_ON_RENDERPHASE_PAUSE)
		PRINTLN("[LM][PROCESS_PROP_EVERY_FRAME] - Clearing LBOOL25_RESET_ALL_PROPS_ON_RENDERPHASE_PAUSE")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_FAST_DROP_OFF)
		IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
			PROCESS_EVERY_FRAME_DROP_OFF()
		ENDIF
	ENDIF
	
	PROCESS_EVERY_FRAME_CAPTURE_LOGIC(iTempCaptureLocation)
	
	PROCESS_PRELOOP_ARENA_TRAPS(sTrapInfo_Local)
	//Dynamic prop (DynoProp) processing - this cleans up dynoprops, as well as handling processing of any special dynoprop behaviours
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps > 0
	AND g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps <= FMMC_MAX_NUM_DYNOPROPS

		PROCESS_PUTTING_OUT_FIREPIT_FIRES(sTrapInfo_Local)
		
	ENDIF

	PROCESS_PLACED_PTFX_EVERY_FRAME()
	
	PROCESS_ACTIVE_MODEL_SWAPS()
	
	PROCESS_ACTIVE_SHARED_RENDERTARGETS()
	
	PROCESS_PLAYER_TEAM_SWAP()	

	PROCESS_MAP_AND_RADAR()

	PROCESS_FILTERS()

	PLAYER_INVENTORY_PROCESSING()

	MAINTAIN_PHONE_HACK_MINIGAME()
		
	MAINTAIN_SPECTATOR_DIALOGUE() // When a player moves to spectate someone now, this function invalidates any dialogue the new spectator target has already played
		
	IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
		PROCESS_VEHICLE_HEALTH_HUD()
	ENDIF
		
	PROCESS_AUDIO()	
	
	IF Is_MP_Objective_Text_On_Display()
		IF NOT IS_BIT_SET( iLocalBoolCheck9, LBOOL9_HAS_FIRST_OBJECTIVE_TEXT_BEEN_SHOWN )
			CPRINTLN( DEBUG_CONTROLLER, "Set LBOOL9_HAS_FIRST_OBJECTIVE_TEXT_BEEN_SHOWN" )
			SET_BIT( iLocalBoolCheck9, LBOOL9_HAS_FIRST_OBJECTIVE_TEXT_BEEN_SHOWN )
		ENDIF
	ENDIF
	
	IF bIsLocalPlayerHost
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciALTERNATIVE_HOLDING_PACKAGE_SCORING)
			IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_ALTERNATIVE_GD_SCORE_ADDED)
				IF IS_BIT_SET(iLocalBoolCheck6, (LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iLocalPart].iteam))
					CLEAR_BIT(iLocalBoolCheck25, LBOOL25_ALTERNATIVE_GD_SCORE_ADDED)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//This function processes mission dialogue triggers - which as well as triggering mission dialogue, now also act as a generic system for triggering
	//  other things, such as the phone hack minigame
		
	PROCESS_DIALOGUE_TRIGGERS_PLAYING_EVERY_FRAME()
	
	PROCESS_DIALOGUE_TRIGGERS()

	PROCESS_TEXT_MESSAGES()
	
	PROCESS_RUNTIME_PREREQ_COUNTERS()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciDISABLE_VALKYRIE_FRONT_GUN)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_VAL")
			CLEAR_HELP(TRUE)
		ENDIF
	ENDIF
	
	INT iTotalNumPlayers = (MC_serverBD.iNumberOfLBPlayers[0]+ MC_serverBD.iNumberOfLBPlayers[1]+ MC_serverBD.iNumberOfLBPlayers[2] + MC_serverBD.iNumberOfLBPlayers[3]) 
	NUM_DPAD_LBD_PLAYERS_IS(iTotalNumPlayers)
	
	//This controls the player list that shows when DPAD_DOWN is pressed
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
	AND IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
	AND IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)
	AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED)
		DRAW_DPAD_LEADERBOARD()
	ENDIF
	
	PROCESS_IAA_ENABLED_GUN_CAMERAS()
	
	IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_IAO_BLOCK_PACKAGE_SOUNDS)
		PROCESS_IN_AND_OUT_PACKAGE_SOUNDS()
	ELSE
		IF HAS_NET_TIMER_STARTED(stBlockPackageSoundsTimer)
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stBlockPackageSoundsTimer) > 500
				CLEAR_BIT(iLocalBoolCheck16, LBOOL16_IAO_BLOCK_PACKAGE_SOUNDS)
				RESET_NET_TIMER(stBlockPackageSoundsTimer)
			ENDIF
		ELSE
			REINIT_NET_TIMER(stBlockPackageSoundsTimer)
		ENDIF
	ENDIF
	
	PROCESS_INCREMENTAL_RULE_FAIL_TIMER()

	IF SHOULD_RUN_NEW_VOTING_SYSTEM(sEndOfMission.iQuitProgress, MC_serverBD.sServerFMMC_EOM.iVoteStatus)
		IF NOT READY_TO_MOVE_TO_NEXT_JOBS(nextJobStruct)
			DO_END_JOB_VOTING_SCREEN(g_sMC_serverBDEndJob, nextJobStruct, g_sMC_serverBDEndJob.timerVotingEnds.Timer)
		ENDIF
	ENDIF

	//For sharing the VIP marker in Entourage
	SHARE_MARKER_WITH_SPECIFIED_TEAM()

	PROCESS_CLIENT_TREAT_AS_WANTED()
	
	PROCESS_TEAM_SWAPPING_ADVERSARY_MODES()			
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciHIDE_DRIVING_ARROW_HELP)
		MAINTAIN_DRIVING_ARROWS()
	ENDIF
	
	IF iNumCreatedDynamicFlarePTFX > 0
		PROCESS_EVERY_FRAME_FLARE_OBJECT()	//FMMC2020 - move to somewhere in _locations
	ENDIF
	
	MANAGE_CAM_PHONE_LEGEND()
						
	MAINTAIN_SPAWN_VEHICLE_BLIP()
	
	PROCESS_TRIP_SKIP_EVERY_FRAME()
	
	PROCESS_QUICK_EQUIP_MASK()
	
	GET_SNACKS_STATUS(MC_playerBD[iPartToUse].iteam)
	
	PROCESS_CUTSCENES_EVERY_FRAME()
	
	PROCESS_MISSION_END_STAGE()
				
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		DISPLAY_ROUNDS_LBD()
	ENDIF
	
	PROCESS_LOCAL_PLAYER_PROOFS()
	
	IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
	OR g_bMissionOver
		IF IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
			PROCESS_AIRLOCK_BOTTOM_RIGHT_HUD(GET_MC_SERVER_GAME_STATE() = GAME_STATE_END)
		ENDIF
	ENDIF
	
	IF bIsLocalPlayerHost
		SERVER_ROUNDS_LBD_LOGIC()
	ENDIF 
	
	PROCESS_PARTICIPANT_SYSTEMS_EVERY_FRAME()
		
	PROCESS_TEAM_REGEN()
	
	IF IS_TARGET_TAGGING_ENABLED()
		PROCESS_ENTITY_MARKER_TAGGING()
	ENDIF

ENDPROC

PROC PROCESS_STAGGERED_MAIN_STATE_CHECKS_ONE()
		
	//Sniperball 
	//FIND_AND_STORE_SNIPERBALL_PACKAGE done in object body. 
	FIND_AND_STORE_NO_WEAPONS_ZONE()
	PROCESS_OBJECTS_TO_EXPLODE_WHEN_OUT_OF_ZONE()
		
ENDPROC
	
PROC PROCESS_STAGGERED_MAIN_STATE_CHECKS_TWO()
	
	PROCESS_SERVER_COUNT_PLAYERS_IN_BOUNDS() // Expensive.
	
ENDPROC
		
PROC PROCESS_STAGGERED_MAIN_STATE_CHECKS_THREE()

	MAINTAIN_HANDLER_HELP_TEXT()
	
	HANDLE_PROPELLORS_WHEN_MISSION_ENDS()

	SET_UP_HIGHER_JUMP_CAR()
	
	PROCESS_RELATIONSHIP_GROUP_CHANGE_ON_RULE()
	
ENDPROC

// Do stuff at the Start of Staggered processing.
PROC PRE_PROCESS_STAGGERED_MAIN_STATE_CHECKS()

ENDPROC

// Do stuff at the end of staggered processing 
PROC POST_PROCESS_STAGGERED_MAIN_STATE_CHECKS()

ENDPROC

PROC PROCESS_STAGGERED_CLIENT_GAME_STATE_RUNNING()	

	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	UNUSED_PARAMETER(iTeam)
	UNUSED_PARAMETER(iRule)

	IF iStaggeredMainStateIterator = 0
		PRE_PROCESS_STAGGERED_MAIN_STATE_CHECKS()
	ENDIF
	
	SWITCH iStaggeredMainStateIterator
		CASE ciFMMC_MAX_MAIN_STATE_STAGGERED_ITERATOR_ONE
			PROCESS_STAGGERED_MAIN_STATE_CHECKS_ONE()
		BREAK
		
		CASE ciFMMC_MAX_MAIN_STATE_STAGGERED_ITERATOR_TWO
			PROCESS_STAGGERED_MAIN_STATE_CHECKS_TWO()
		BREAK
		
		CASE ciFMMC_MAX_MAIN_STATE_STAGGERED_ITERATOR_THREE
			PROCESS_STAGGERED_MAIN_STATE_CHECKS_THREE()
		BREAK		
	ENDSWITCH
	
	iStaggeredMainStateIterator++
	IF iStaggeredMainStateIterator > ciFMMC_MAX_MAIN_STATE_STAGGERED_ITERATOR
		iStaggeredMainStateIterator = 0
		POST_PROCESS_STAGGERED_MAIN_STATE_CHECKS()
	ENDIF
	
ENDPROC

PROC PROCESS_CLIENT_GAME_STATE_RUNNING()
		
	g_bMissionClientGameStateRunning = TRUE
	
	RESET_MICROPHONE_POSITION_FROM_START_TRANSITION()
	
	IF NOT PROCESS_MISSION_COUNTDOWN_TIMER()		
		g_bMission321Done = FALSE	
		EXIT
	ENDIF
	g_bMission321Done = TRUE
	
	PROCESS_STAGGERED_CLIENT_GAME_STATE_RUNNING()	
	
	PROCESS_CLIENT_GAME_STATE_RUNNING_UNSORTED()
		
	PROCESS_CLIENT_WORLD_EVERY_FRAME()
	
	PROCESS_LOCAL_PLAYER()
	
	PROCESS_GANG_CHASE()
		
	PROCESS_PLAYERS()
	
	PROCESS_ENTITIES()
	
	PROCESS_CLIENT_RULES_AND_OBJECTIVES()
	
	PROCESS_HUD()
		
	PROCESS_SPAWN_GROUP_DATA_RESET_CLIENT()
	
ENDPROC

PROC PROCESS_CLIENT_GAME_STATE_MISSION_OVER()

	//[FMMC2020] - Move contents to _Ending

	IF IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
		PROCESS_AIRLOCK_BOTTOM_RIGHT_HUD()
	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
	OR IS_BIT_SET(iLocalBoolCheck, LBOOL_LB_FINISHED)
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			IF NOT g_bCelebrationScreenIsActive
				MC_SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
				PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_MISSION_OVER] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - M")
			ELSE
				PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_MISSION_OVER] MUSIC - not calling TRIGGER_MUSIC_EVENT(MP_MC_STOP) - M, g_bCelebrationScreenIsActive = TRUE, celebration screen will handle this.")
			ENDIF
		ENDIF
		
		MC_SCRIPT_CLEANUP()
		SET_MC_CLIENT_GAME_STATE(GAME_STATE_END) //[FMMC2020] ???? the line above terminates the script...
	ENDIF
	
ENDPROC

PROC PROCESS_CLIENT_GAME_STATE_LEAVE()
	
	//[FMMC2020] - Move contents to _Ending
	
	DISABLE_SPECTATOR_FADES_DURING_THIS_EVENT()
	
	IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC) //[FMMC2020] if this is still needed
		SCRIPT_TRIGGER_MUSIC_EVENT_FAIL()
		PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_LEAVE] - TRIGGER_MUSIC_EVENT(\"MP_MC_FAIL\") - m1")
	ENDIF
	
	DoSkySwoopUpImmediateFadeOutIfNecessary()
	MC_SCRIPT_CLEANUP(FALSE)
	SET_MC_CLIENT_GAME_STATE(GAME_STATE_END) //[FMMC2020] ???? the line above terminates the script...
	
ENDPROC

PROC PROCESS_CLIENT_GAME_STATE_END()

	//Nothing
	
ENDPROC

/// PURPOSE:
///    Re-initializes any variables that need to be reset each frame.
PROC REINIT_EVERY_FRAME_RESET_VARS()
	
	INT i = 0

	iLocalBoolResetCheck1 = 0
	g_bMissionClientGameStateRunning = FALSE	
	
	iSAS_StaggerIterator++
	
	FOR i = 0 TO FMMC_MAX_PEDS_BITSET-1
		iPedsServerProcessedThisFrame[i] = 0
		iPedsClientProcessedThisFrame[i] = 0
		iPedRespawnIsBlockedThisFrameBS[i] = 0
		
		iPedShouldCleanupThisFrameBS[i] = 0
		iPedShouldNotCleanupThisFrameBS[i] = 0
		
		SWITCH iSAS_StaggerIterator
			CASE ciSAS_PED_DELAYED_SIGHT_CHECK_RESET_STAGGER
				iSASPlayerInPerceptionAreaBS[i] = 0
				iSASPlayerInPerceptionAreaCheckedBS[i] = 0
			BREAK
			CASE ciSAS_PED_INSTANT_SIGHT_CHECK_RESET_STAGGER
				iSASPlayerInPerceptionInstantAreaBS[i] = 0
				iSASPlayerInPerceptionInstantAreaCheckedBS[i] = 0
			BREAK
			CASE ciSAS_PED_LOS_SIGHT_CHECK_RESET_STAGGER
				iSASPlayerInLOSInstantAreaBS[i] = 0
				iSASPlayerInLOSInstantAreaCheckedBS[i] = 0
			BREAK
			CASE ciSAS_PED_RELOOP_SIGHT_CHECK_STAGGER
				iSAS_StaggerIterator = -1
			BREAK
		ENDSWITCH		
	ENDFOR
	
	FOR i = 0 TO FMMC_MAX_PROP_BITSET-1
		iPropRespawnIsBlockedThisFrameBS[i] = 0			
		
		iPropShouldCleanupThisFrameBS[i] = 0
		iPropShouldNotCleanupThisFrameBS[i] = 0
	ENDFOR
	
	iVehRespawnIsBlockedThisFrameBS = 0
	iObjRespawnIsBlockedThisFrameBS = 0
	iDynoRespawnIsBlockedThisFrameBS = 0
	iZoneRespawnIsBlockedThisFrameBS = 0
	
	iVehShouldCleanupThisFrameBS = 0
	iVehShouldNotCleanupThisFrameBS = 0
	iObjShouldCleanupThisFrameBS = 0
	iObjShouldNotCleanupThisFrameBS = 0
	iDynoShouldCleanupThisFrameBS = 0
	iDynoShouldNotCleanupThisFrameBS = 0
	iZoneShouldCleanupThisFrameBS = 0
	iZoneShouldNotCleanupThisFrameBS = 0
	
	INT iTeam = 0
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		FOR i = 0 TO FMMC_MAX_TEAM_SPAWN_POINT_BITSET-1
			iRespawnPointIsBlockedThisFrameBS[iTeam][i] = 0
		ENDFOR
	ENDFOR
	
	iCachedCutsceneTeam = -2
	iCachedCutsceneStreamingTeam = -2
	
	PROCESS_RESET_AIRLOCK_HUD_VARS()

	// Cache results of functions from other headers below
	IF GET_MC_CLIENT_GAME_STATE(iLocalPart) = GAME_STATE_RUNNING
		CACHE__SHOULD_USE_CUSTOM_SPAWN_POINTS()
	ENDIF	

	iEntitiesSpawnedThisFrame = 0
	iVehicleSeatingEnforcementSlot = 0
	
	iLocalPlayerInLocationBitset = 0
	iLocalPlayerNotInLocationBitset = 0
	
	iNewWantedThisFrame = -1
	
	iCheckpointCacheBS = 0
	
ENDPROC

/// PURPOSE:
///    Run before the Client State Machine is processed each frame.
PROC PROCESS_PRE_CLIENT_STATE_MACHINE()
	
	//[FMMC2020] - Everything in here should be moved to appropriate headers 
	// 				- Anything checking game state in here needs moved to the appropriate state machine processing.
	
	REINIT_EVERY_FRAME_RESET_VARS()
	
	iStaggeredTeamIterator++
	IF iStaggeredTeamIterator >= MC_serverBD.iNumberOfTeams
		iStaggeredTeamIterator = 0
	ENDIF
	
	IF IS_MISSION_USING_ANY_ALT_VAR()
		IF NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_ALT_VARS_APPLIED)
			PRINTLN("[SCRIPT INIT][AltVarsSystem] - PROCESS_PRE_CLIENT_STATE_MACHINE - Exitting Early as we need to wait for alt vars.")
			EXIT
		ENDIF
	ENDIF
	
	MAINTAIN_ISLAND_HEIST_LEADER_ANTI_CHEAT()
	
	PROCESS_DLC_DIALOGUE_SETTINGS()
	
	SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER()
	
	PROCESS_MISSION_FACTORY_INTERIORS(sMissionFactories, iInteriorInitializingExtrasBS)
	
	PROCESS_INTERIOR_COLLISION_FIXES(sInteriorFixStruct)
	
	FM_MAINTAIN_MISSION_DENSITYS_THIS_FRAME( MC_serverBD.fPedDensity,
											MC_serverBD.fPedDensity, 
											MC_serverBD.fPedDensity,
											MC_serverBD.fVehicleDensity,
											MC_serverBD.fVehicleDensity,
											MC_serverBD.fVehicleDensity,
											MC_serverBD.fVehicleDensity,
											MC_serverBD.fVehicleDensity = 0.0 )	
	IF IS_CELLPHONE_TRACKIFY_IN_USE()
		SET_PHONE_UNDER_HUD_THIS_FRAME()
	ENDIF
	
	MAINTAIN_PLAYER_CONTROL_AT_END_OF_STRAND_CUTSCENE()		
	
	IF IS_BIT_SET(MC_serverBD.iOptionsMenuBitSet, ciGANGS_DISABLED) 
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(LocalPlayer)
	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetTwelve, ciDISABLE_CPO_OVERRIDE)
	OR (IS_PLAYER_TRIGGERED_EMP_CURRENTLY_ACTIVE() AND SHOULD_EMP_IMPACT_PED_VISION(NULL))
		IF g_FMMC_STRUCT.fCopPerceptionOverrideSeeingRange <> 60.0	//Default values
		OR g_FMMC_STRUCT.fCopPerceptionOverridePeripheralRange <> 5.0
		OR (g_FMMC_STRUCT.fCopPerceptionOverrideHearingRange <> 60.0 AND g_FMMC_STRUCT.fCopPerceptionOverrideHearingRange != 0.075) // Prevents a 0.075 copy paste cloud loader error.
		OR g_FMMC_STRUCT.fCopPerceptionOverrideRearViewSeeingRange <> -1.0
		OR (IS_PLAYER_TRIGGERED_EMP_CURRENTLY_ACTIVE() AND SHOULD_EMP_IMPACT_PED_VISION(NULL))		
			FLOAT fSeeingRangeToUse = g_FMMC_STRUCT.fCopPerceptionOverrideSeeingRange
			
			IF IS_PLAYER_TRIGGERED_EMP_CURRENTLY_ACTIVE()
			AND SHOULD_EMP_IMPACT_PED_VISION(NULL)
				fSeeingRangeToUse *= 0.01
				fSeeingRangeToUse *= g_FMMC_STRUCT.sPhoneEMPSettings.iPhoneEMP_PerceptionPercentage
			ENDIF
			
			SET_COP_PERCEPTION_OVERRIDES(fSeeingRangeToUse, 
										 g_FMMC_STRUCT.fCopPerceptionOverridePeripheralRange, 
										 g_FMMC_STRUCT.fCopPerceptionOverrideHearingRange,
										 DEFAULT, 
										 DEFAULT, 
										 DEFAULT, 
										 g_FMMC_STRUCT.fCopPerceptionOverrideRearViewSeeingRange)				
										 
			
			#IF IS_DEBUG_BUILD
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_SET_COP_PERCEPTION_OVERRIDES, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "SET_COP_PERCEPTION_OVERRIDES Called. SAS cop peds forced Sight/Hearing | @/@", DEFAULT, DEFAULT, DEFAULT, fSeeingRangeToUse, g_FMMC_STRUCT.fCopPerceptionOverrideHearingRange)
			#ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.fCopDelayedReportTime <> -1.0	//Default value
		ALLOW_EVASION_HUD_IF_DISABLING_HIDDEN_EVASION_THIS_FRAME(LocalPlayer, g_FMMC_STRUCT.fCopDelayedReportTime)
	ENDIF
	
	PROCESS_DISABLED_RADIO()
		
	PROCESS_MISSION_TIME_OF_DAY_EVERY_FRAME()
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		IF NOT IS_PAUSE_MENU_ACTIVE()
			IF g_FMMC_STRUCT.g_b_QuitTest
				IF IS_SCREEN_FADED_OUT()
					PRINTLN("[RCC MISSION] - g_FMMC_STRUCT.g_b_QuitTest is true so quit")
					MC_SCRIPT_CLEANUP()
				ELSE
					IF NOT IS_SCREEN_FADING_OUT()
						DO_SCREEN_FADE_OUT(500)
						MC_SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
						PRINTLN("[RCC MISSION] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - K")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_RADAR_INIT)
			INIT_RADAR_ZOOM_LEVEL()
			CONTROL_RADAR_ZOOM()
		ELSE
			CONTROL_RADAR_ZOOM()
		ENDIF
	ENDIF
	
	DO_OUTROS()
	
	IF GET_MC_CLIENT_GAME_STATE(iLocalPart) = GAME_STATE_INI
		IF NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
		AND NOT g_bStartedInAirlockTransition
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			THEFEED_HIDE_THIS_FRAME() 
			PRINTLN("radar being hidden: ") 
		ENDIF
	ENDIF
	
	IF IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
	OR g_bStartedInAirlockTransition
		//Hide Area names
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
	ENDIF
	IF NETWORK_IS_GAME_IN_PROGRESS()
	
		//Deal with making sure that a spectator is not host
		MAINTAIN_SPEC_NOT_HOST(iManageSpechost, iDesiredHost)
		 
		IF NOT bIsSCTV						//This MAINTAIN_SPECTATOR is just for player specating, not SCTV
			MAINTAIN_SPECTATOR(g_BossSpecData)
		ENDIF
		
		PROCESS_SPECTATOR_PLAYER_LISTS_FOR_MISSION()			
		
		SET_MISSION_INIT_COMPLETE_GLOBAL()			
		
		// For data dump when mission is ending.
		#IF IS_DEBUG_BUILD
		MAINTAIN_EOM_DATA_DUMP()
        #ENDIF
	ENDIF
	
	PROCESS_DISABLED_CONTROLS()
	
	ADD_MISSION_JOB_POINTS_ROUNDS()

	BLOCK_ACTION_MODE_OPTION(jobIntroData)
	
	IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_ENTITIES_UNFROZEN_AT_START_OF_MISSION)
	AND MC_playerBD[iLocalPart].iGameState >= GAME_STATE_RUNNING
		IF IS_BIT_SET(MC_serverbd.iServerBitSet, SSBOOL_ALL_READY_INTRO_CUT)
			
			IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer)
			AND NOT bIsSCTV
				SET_ALL_MISSION_ENTITIES_UNFROZEN_AND_ABLE_TO_MIGRATE(MC_serverBD_1.sFMMC_SBD)
			ENDIF
			
			SET_BIT(iLocalBoolCheck8, LBOOL8_ENTITIES_UNFROZEN_AT_START_OF_MISSION)
		ENDIF
	ENDIF
		
	PROCESS_CLIENT_BLIMP_SIGNS()
		
	PROCESS_YACHT_FOR_CUTSCENE()
		
	DISABLE_RUINER_ROCKETS()
	
	PROCESS_RENDERPHASE_UNPAUSE_WITH_DELAY()
		
	PROCESS_PLAYER_STOLEN_GOODS_SLOW_DOWN()
	
	PROCESS_SHOWING_AIRLOCK_OBJECTIVE()
	
	PROCESS_INIT_TRIP_SKIP_DATA_FOR_LOCAL_PLAYER()
	
	IF MC_playerBD[iLocalPart].iGameState < GAME_STATE_RUNNING
		SET_EVERYONE_IGNORE_PLAYER(LocalPlayer, TRUE)
		SET_PED_RESET_FLAG(LocalPlayerPed, PRF_CannotBeTargetedByAI, TRUE)
	ENDIF
	
	RENDER_SPAWN_PROTECTION_SPHERES_EARLY()
	
	IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_PLACED_PEDS_PERFORMED_ROLLING_START)
	AND (MC_playerBD[iLocalPart].iGameState = GAME_STATE_RUNNING
	OR MC_playerBD[iLocalPart].iGameState = GAME_STATE_CAMERA_BLEND)
		INT iPed
		IF MC_serverBD.iNumPedCreated > 0
		AND MC_serverBD.iNumPedCreated <= FMMC_MAX_PEDS
			FOR iPed = 0 TO (MC_serverBD.iNumPedCreated-1)
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iped])
					PED_INDEX tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
					PROCESS_PLACED_PED_ROLLING_START(iPed, tempPed)
				ENDIF
			ENDFOR
		ENDIF
		
		SET_BIT(iLocalBoolCheck31, LBOOL31_PLACED_PEDS_PERFORMED_ROLLING_START)
	ENDIF
	
	IF MC_playerBD[iLocalPart].iGameState != GAME_STATE_RUNNING
	AND MC_playerBD[iLocalPart].iGameState != GAME_STATE_MISSION_OVER
	AND MC_playerBD[iLocalPart].iGameState != GAME_STATE_LEAVE
	AND MC_playerBD[iLocalPart].iGameState != GAME_STATE_END
	AND NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitset, PBBOOL_ON_LEADERBOARD)
	AND NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciDISABLE_SPECIAL_VEHICLE_WEAPONS)
				DISABLE_RUINER_WEAPONS_TO_SHOW_COVER_PANEL()
			ENDIF
		
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SPECIAL, TRUE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_JUMP, TRUE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK, TRUE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2, TRUE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN, TRUE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND, TRUE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CAR_JUMP, TRUE)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetTen[0], ciBS_RULE10_DISABLE_DELUXO_HOVER_MODE)
				PRINTLN("[PROCESS_BEFORE_GAME_STATE_RUNNING][RH] disabling Deluxo hover as the first turn is set")
				VEHICLE_INDEX viPedCar = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				IF GET_ENTITY_MODEL(viPedCar) = DELUXO
					IF DOES_ENTITY_EXIST(viPedCar)
						SET_SPECIAL_FLIGHT_MODE_ALLOWED(viPedCar, FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetTen[0], ciBS_RULE10_DISABLE_DELUXO_FLIGHT_MODE)
				PRINTLN("[PROCESS_BEFORE_GAME_STATE_RUNNING][RH] disabling Deluxo flight mode as the first turn is set")
				VEHICLE_INDEX viPedCar = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				IF GET_ENTITY_MODEL(viPedCar) = DELUXO
					IF DOES_ENTITY_EXIST(viPedCar)
						SET_DISABLE_HOVER_MODE_FLIGHT(viPedCar, FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			SET_BIT(iLocalBoolCheck30, LBOOL30_SHOULD_RESET_VEHICLE_MINES_BLOCK_1)
			
			DISABLE_VEHICLE_MINES(TRUE)
			PRINTLN("[RCC MISSION] DISABLE_VEHICLE_MINES(TRUE)")
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck30, LBOOL30_SHOULD_RESET_VEHICLE_MINES_BLOCK_1)
			IF g_bDisableVehicleMines
			AND IS_BITMASK_AS_ENUM_SET(cduiIntro.iBitFlags, CNTDWN_UI2_Played_Go)
				DISABLE_VEHICLE_MINES(FALSE)
				PRINTLN("[RCC MISSION] DISABLE_VEHICLE_MINES(FALSE)")
			ENDIF
			CLEAR_BIT(iLocalBoolCheck30, LBOOL30_SHOULD_RESET_VEHICLE_MINES_BLOCK_1)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iTeam][0].iSpawnBitSet, ci_SpawnBS_AirSpawn)
		VECTOR vTempPos
		vTempPos = GET_ENTITY_COORDS(LocalPlayerPed)
		
		IF VDIST(vTempPos, vDZPlayerSpawnLoc) < 100
		AND NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		ENDIF
	ENDIF
	
	PROCESS_INITIAL_OBJECT_CONTINUITY()
	
	PROCESS_DISABLE_REALTIME_MULTIPLAYER()
	
ENDPROC

/// PURPOSE:
///    Run after the Client State Machine is processed each frame.
PROC PROCESS_POST_CLIENT_STATE_MACHINE()
	
	//[FMMC2020] - Everything in here should be moved to appropriate headers 
	// 				- Anything checking game state in here needs moved to the appropriate state machine processing.

	MAINTAIN_THE_CURRENT_HUD()
		
	PROCESS_CLEEBRATION_SCREENS_EVERY_FRAME()
	
	PROCESS_BG_GLOBALS_PASS_OVER()
		
	PROCESS_INSTANCED_CONTENT_FADE_SCREEN_OUT_FOR_PERIOD_OF_TIME()
		
ENDPROC

PROC PROCESS_CLIENT()

	PROCESS_PRE_CLIENT_STATE_MACHINE()
	
	SWITCH GET_MC_CLIENT_GAME_STATE(iLocalPart)
		
		CASE GAME_STATE_INI
			PROCESS_CLIENT_GAME_STATE_INI()
		BREAK
		
		CASE GAME_STATE_INTRO_CUTSCENE      
			PROCESS_CLIENT_GAME_STATE_INTRO_CUTSCENE()
		BREAK
		
		CASE GAME_STATE_FORCED_TRIP_SKIP	
			PROCESS_CLIENT_GAME_STATE_FORCED_TRIP_SKIP()
		BREAK
		
		CASE GAME_STATE_SOLO_PLAY		    
			PROCESS_CLIENT_GAME_STATE_SOLO_PLAY()
		BREAK
		
		CASE GAME_STATE_TEAM_PLAY		    
			PROCESS_CLIENT_GAME_STATE_TEAM_PLAY()
		BREAK
		
		CASE GAME_STATE_CAMERA_BLEND	    
			PROCESS_CLIENT_GAME_STATE_CAMERA_BLEND()
		BREAK
		
		CASE GAME_STATE_RUNNING				
			PROCESS_CLIENT_GAME_STATE_RUNNING()
		BREAK
		
		CASE GAME_STATE_MISSION_OVER		
			PROCESS_CLIENT_GAME_STATE_MISSION_OVER()
		BREAK
		
		CASE GAME_STATE_LEAVE				
			PROCESS_CLIENT_GAME_STATE_LEAVE()
		BREAK
		
		CASE GAME_STATE_END					
			PROCESS_CLIENT_GAME_STATE_END()
		BREAK
		
	ENDSWITCH
	
	PROCESS_POST_CLIENT_STATE_MACHINE()

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Server Processing ----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### Server State Machine Processing. 
// ##### Called by the script host every frame. 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets the Server Game State
/// PARAMS:
///    iStage - The State to move to
PROC SET_SERVER_GAME_STATE(INT iStage)

	IF MC_serverBD.iServerGameState != iStage
		//[FMMC2020] - move to _Debug
		#IF IS_DEBUG_BUILD
			NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SET_SERVER_GAME_STATE Called on client by mistake")
		
			// Print out the previous game state for logging
			SWITCH iStage
				CASE GAME_STATE_INI
					PRINTLN("[RCC MISSION] Previous Server State = ")
					PRINT_SERVER_GAME_STATE(GET_MC_SERVER_GAME_STATE())
					NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION] SET_SERVER_GAME_STATE  >>> GAME_STATE_INI  ", tl31ScriptName) NET_NL()
				BREAK
				CASE GAME_STATE_RUNNING
					PRINTLN("[RCC MISSION] Previous Server State = ")
					PRINT_SERVER_GAME_STATE(GET_MC_SERVER_GAME_STATE())
					NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION] SET_SERVER_GAME_STATE  >>> GAME_STATE_RUNNING  ", tl31ScriptName) NET_NL()
				BREAK
				CASE GAME_STATE_LEAVE
					PRINTLN("[RCC MISSION] Previous Server State = ")
					PRINT_SERVER_GAME_STATE(GET_MC_SERVER_GAME_STATE())
					NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION] SET_SERVER_GAME_STATE  >>> GAME_STATE_LEAVE  ", tl31ScriptName) NET_NL()
				BREAK
				CASE GAME_STATE_END
					PRINTLN("[RCC MISSION] Previous Server State = ")
					PRINT_SERVER_GAME_STATE(GET_MC_SERVER_GAME_STATE())
					NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION] SET_SERVER_GAME_STATE  >>> GAME_STATE_END  ", tl31ScriptName) NET_NL()
				BREAK
				DEFAULT
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("SET_SERVER_GAME_STATE Invalid option net_deathmatch")
					#ENDIF
				BREAK
			ENDSWITCH
		#ENDIF
		
		MC_serverBD.iServerGameState = iStage
		
	ENDIF
	
ENDPROC

FUNC BOOL READY_TO_LOAD_AND_CREATE_PLACED_ENTITIES()
	
	IF NOT HAVE_SPECIAL_CASE_INTERIORS_LOADED()
		PRINTLN("[RCC MISSION][PROCESS_SERVER] READY_TO_LOAD_AND_CREATE_PLACED_ENTITIES | Waiting for HAVE_INTERIORS_LOADED / Clearing LBOOL8_INITIAL_IPL_SET_UP_COMPLETE")
		CLEAR_BIT(iLocalBoolCheck8, LBOOL8_INITIAL_IPL_SET_UP_COMPLETE)
		RETURN FALSE
	ENDIF
	
	IF USING_FMMC_YACHT()
		IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_FMMC_YACHTS_READY)
			PRINTLN("[RCC MISSION][PROCESS_SERVER] READY_TO_LOAD_AND_CREATE_PLACED_ENTITIES | Waiting for Yacht creation")
			RETURN FALSE
		ENDIF
	ENDIF
	
	PRINTLN("[RCC MISSION][PROCESS_SERVER] READY_TO_LOAD_AND_CREATE_PLACED_ENTITIES | Ready!!")
	RETURN TRUE
ENDFUNC

PROC PROCESS_SERVER_GAME_STATE_INI()
	
	//[FMMC2020] move all of these calls into _Init or the appropriate headers' game state ini processing function
	
	INT iTeam
	
	IF IS_MISSION_USING_ANY_ALT_VAR()
		IF NOT IS_CORONA_READY_TO_START_WITH_JOB()
			PRINTLN("[LM][AltVarsSystem][PROCESS_SERVER] - Corona Not Ready to start the job - Waiting before initializing mission variations vars.")
			EXIT
		ELIF NOT HAVE_ALL_CORONA_PLAYERS_JOINED(tdSafeJoinVariations)
			PRINTLN("[LM][AltVarsSystem][PROCESS_SERVER] - Waiting for all 'Corona Players' to have joined.")
			EXIT
		ELIF NOT HAS_ALT_VAR_GANG_BOSS_BEEN_SET_YET()
			PRINTLN("[LM][AltVarsSystem][PROCESS_SERVER] - Waiting for Gang Boss ID.")
			EXIT
		ELIF IS_ALT_VAR_SERVER_WAITING_FOR_CLIENT_BD_SYNC()
			PRINTLN("[LM][AltVarsSystem][PROCESS_SERVER] - Waiting for Clients to update their playerBD.")
			EXIT
		ELIF NOT HAS_ALT_VAR_SERVER_DATA_BEEN_SET_YET_SERVER()
			PRINTLN("[LM][AltVarsSystem][PROCESS_SERVER] - Initializing Mission Variations Vars.")
			PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA()
		ELSE
			PRINTLN("[LM][AltVarsSystem][PROCESS_SERVER] - Mission Variation Vars Ready to go.")
		ENDIF
	ENDIF
	
	IF MC_serverBD.iTotalNumStartingPlayers = 0
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitset2, SBBOOL2_SERVER_INITIALISATION_TIMED_OUT)
		IF IS_CORONA_READY_TO_START_WITH_JOB()
			IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdInitialisationTimeout)
				PRINTLN("[RCC MISSION][PROCESS_SERVER] Starting MC_serverBD.tdInitialisationTimeout timer")
				REINIT_NET_TIMER(MC_serverBD.tdInitialisationTimeout)
			ELIF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdInitialisationTimeout) >= CORONA_FAILSAFE_TIMEOUT)
				PRINTLN("[RCC MISSION][PROCESS_SERVER] MC_serverBD.tdInitialisationTimeout has passed 30 seconds!")
				RESET_NET_TIMER(MC_serverBD.tdInitialisationTimeout)
				SET_BIT(MC_serverBD.iServerBitset2, SBBOOL2_SERVER_INITIALISATION_TIMED_OUT)
			ENDIF

			FOR iteam = 0 TO (FMMC_MAX_TEAMS-1)
				MC_serverBD.iNumStartingPlayers[iteam] = GET_NUMBER_PLAYERS_JOINING_MISSION(iteam)
				PRINTLN("[RCC MISSION][PROCESS_SERVER] team = ",iteam," MC_serverBD.iNumStartingPlayers = ",MC_serverBD.iNumStartingPlayers[iteam])
			ENDFOR
			
			MC_serverBD.iTotalNumStartingPlayers = MC_serverBD.iNumStartingPlayers[0] + MC_serverBD.iNumStartingPlayers[1] + MC_serverBD.iNumStartingPlayers[2] + MC_serverBD.iNumStartingPlayers[3] 
			PRINTLN("[RCC MISSION][PROCESS_SERVER] MC_serverBD.iTotalNumStartingPlayers = ",MC_serverBD.iTotalNumStartingPlayers) 
			
			IF IS_A_STRAND_MISSION_BEING_INITIALISED()
				PRINTLN("[RCC MISSION][PROCESS_SERVER] RUN_ALL_STAGGERED_MC_HOST_FUNCTIONS being called") 
				RUN_ALL_STAGGERED_MC_HOST_FUNCTIONS( iCurrentRequestStaggerForStrandMission, iRelGroupStaggerVars, iNumTimesCalledRelGroupFunc )
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION][PROCESS_SERVER] Waiting for IS_CORONA_READY_TO_START_WITH_JOB.") 
		ENDIF
	ELSE
		
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(MC_serverBD.iServerBitset2, SBBOOL2_SERVER_INITIALISATION_TIMED_OUT)
			PRINTLN("[RCC MISSION][PROCESS_SERVER] MC_serverBD.iTotalNumStartingPlayers = ", MC_serverBD.iTotalNumStartingPlayers, " but MC_serverBD.tdInitialisationTimeout has passed 30 seconds, move on")
		ENDIF
		#ENDIF
		
		IF NETWORK_IS_IN_MP_CUTSCENE()
			//Any entities created during a cutscene should be networked
			SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
		ENDIF
		
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitset8, SBBOOL8_APPLIED_SCRIPT_CLOUD_CONTENT_FIXES)
		AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			SET_BIT(MC_serverBD.iServerBitset8, SBBOOL8_APPLIED_SCRIPT_CLOUD_CONTENT_FIXES)
			SET_SCRIPT_CLOUD_CONTENT_FIXES()
		ENDIF
		
		// Alt Vars Pre Spawn Groups. ------------
		IF IS_MISSION_USING_ANY_ALT_VAR()
		AND (NOT IS_BIT_SET(MC_serverBD.iServerBitset8, SBBOOL8_HOST_MISSION_VARIATION_DATA_PRE_APPLIED) OR NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_ALT_VARS_APPLIED))
			PRINTLN("[RCC MISSION][PROCESS_SERVER] Calling PROCESS_APPLY_ALT_VAR_PRE_SPAWN_GROUP_SETTINGS - I AM THE ONE WHO HOSTS.")
			PROCESS_APPLY_ALT_VAR_PRE_SPAWN_GROUP_SETTINGS()
			SET_BIT(MC_serverBD.iServerBitset8, SBBOOL8_HOST_MISSION_VARIATION_DATA_PRE_APPLIED)
		ENDIF
		
		// Spawn Groups ---------------
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_RSGDATA_INITIALISED)
			PRINTLN("[RCC MISSION][PROCESS_SERVER] Calling SEED_SPAWN_GROUPS - Setting SBBOOL3_RSGDATA_INITIALISED.")
			SET_BIT(MC_serverBD.iServerBitset3, SBBOOL3_RSGDATA_INITIALISED)
			SEED_SPAWN_GROUPS()								
			INITIALIZE_SPAWN_GROUP_DEPENDENT_SERVER_DATA()		
			SET_BIT(MC_serverBD_4.rsgSpawnSeed.iMiscBS, ciRandomSpawnGroupBS_HasBeenSet)
			CACHE_SERVER_SEEDED_SPAWN_GROUPS_AT_START()
		ENDIF
		
		// Alt Vars Post Spawn Groups. ------------
		IF IS_MISSION_USING_ANY_ALT_VAR()
		AND (NOT IS_BIT_SET(MC_serverBD.iServerBitset8, SBBOOL8_HOST_MISSION_VARIATION_DATA_APPLIED) OR NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_ALT_VARS_APPLIED))
			PRINTLN("[RCC MISSION][PROCESS_SERVER] Calling PROCESS_APPLY_MISSION_VARIATION_SETTINGS - I AM THE ONE WHO HOSTS.")
			PROCESS_APPLY_ALT_VAR_SETTINGS()
			SET_BIT(MC_serverBD.iServerBitset8, SBBOOL8_HOST_MISSION_VARIATION_DATA_APPLIED)
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_REQUEST_LOAD_FMMC_MODELS_STAGGERED_DONE)
		AND IS_A_STRAND_MISSION_BEING_INITIALISED()
			PRINTLN("[RCC MISSION][PROCESS_SERVER] REQUEST_LOAD_FMMC_MODELS_STAGGERED being called") 
			IF RUN_ALL_STAGGERED_MC_HOST_FUNCTIONS( iCurrentRequestStaggerForStrandMission, iRelGroupStaggerVars, iNumTimesCalledRelGroupFunc )
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_SERVER] RUN_ALL_STAGGERED_MC_HOST_FUNCTIONS RETURNING TRUE") 
				SET_BIT(iLocalBoolCheck8, LBOOL8_REQUEST_LOAD_FMMC_MODELS_STAGGERED_DONE)
			ENDIF
			
			PRINTLN("[RCC MISSION][PROCESS_SERVER] NOT LBOOL8_REQUEST_LOAD_FMMC_MODELS_STAGGERED_DONE AND IS_A_STRAND_MISSION_BEING_INITIALISED") 
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_INITIAL_IPL_SET_UP_COMPLETE)
			AND IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_RSGDATA_INITIALISED)
				IF HAVE_ALL_PLAYERS_PROCESSED_PRE_GAME()
					IF READY_TO_LOAD_AND_CREATE_PLACED_ENTITIES()
						
						#IF IS_DEBUG_BUILD
							IF NOT HAS_NET_TIMER_STARTED(tdLoadAndCreateTimer)
								REINIT_NET_TIMER(tdLoadAndCreateTimer)
							ENDIF
						#ENDIF
						
						IF MC_LOAD_AND_CREATE_ALL_ENTITIES()
							
							PROCESS_POST_ENTITY_CREATION()
							
							#IF IS_DEBUG_BUILD
								IF HAS_NET_TIMER_STARTED(tdLoadAndCreateTimer)
								AND NOT bPrintedLoadTime
									PRINTLN("[RCC MISSION][PROCESS_SERVER] MC_LOAD_AND_CREATE_ALL_ENTITIES took ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdLoadAndCreateTimer), "ms to complete")
									RESET_NET_TIMER(tdLoadAndCreateTimer)
									bPrintedLoadTime = TRUE
								ENDIF
							#ENDIF
							
							IF IS_CORONA_READY_TO_START_WITH_JOB()
							OR IS_FAKE_MULTIPLAYER_MODE_SET()
								IF INIT_SERVER_DATA()
									SET_FMMC_MODELS_AS_NOT_NEEDED()
									
									IF IS_A_STRAND_MISSION_BEING_INITIALISED()
										MC_SET_ALL_MISSION_ENTITIES_VISIBLITY(TRUE)
									ENDIF
									
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_StartMissionWithAggro)
										PRINTLN("[RCC MISSION][PROCESS_SERVER] INIT_SERVER_DATA - ciOptionsBS28_StartMissionWithAggro is set - Instantly aggroing relevant peds and systems!") 										
										FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)										
											SET_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET(iTeam, g_FMMC_STRUCT.iAggroIndexBS_iStartMissionWithAggro)
											ALERT_ALL_AGGRO_PEDS(iTeam)									
										ENDFOR
									ENDIF
									
									SET_SERVER_GAME_STATE(GAME_STATE_RUNNING)
								ELSE
									PRINTLN("[RCC MISSION][PROCESS_SERVER] INIT_SERVER_DATA - FALSE") 
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION][PROCESS_SERVER] IS_CORONA_READY_TO_START_WITH_JOB - FALSE")
							ENDIF
							
						ELSE
							PRINTLN("[RCC MISSION][PROCESS_SERVER] MC_LOAD_AND_CREATE_ALL_ENTITIES - FALSE") 
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION][PROCESS_SERVER] HAVE_ALL_PLAYER_PREGAMED - FALSE") 
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_INITIAL_IPL_SET_UP_COMPLETE)
					PRINTLN("[RCC MISSION][PROCESS_SERVER] LBOOL8_INITIAL_IPL_SET_UP_COMPLETE is FALSE") 
				ENDIF
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_RSGDATA_INITIALISED)
					PRINTLN("[RCC MISSION][PROCESS_SERVER] SBBOOL3_RSGDATA_INITIALISED is FALSE") 
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SERVER_GAME_STATE_RUNNING_OLD()

	//[FMMC2020] move all of these calls into the appropriate headers' main processing function
	
	INT iTeam
	INT iParticipant
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_ALL_JOINED_START)
		IF NOT IS_BIT_SET(MC_serverbd.iServerBitSet, SSBOOL_ALL_READY_INTRO_CUT)
			IF SERVER_CHECK_EVERYONE_REACHED_INTRO_CUTSCENE()
				SERVER_CALCULATE_CENTRE_POINT_FOR_TEAMS(MC_serverBD,MC_playerBD)
				SET_BIT(MC_serverbd.iServerBitSet, SSBOOL_ALL_READY_INTRO_CUT)
				SET_ALL_MISSION_ENTITIES_UNFROZEN_AND_ABLE_TO_MIGRATE(MC_serverBD_1.sFMMC_SBD)
				SET_BIT(iLocalBoolCheck8, LBOOL8_ENTITIES_UNFROZEN_AT_START_OF_MISSION)
			ENDIF
		ENDIF				
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_ALL_JOINED_START)
		IF HAVE_ALL_CORONA_PLAYERS_JOINED(tdsafejointimer)
		OR IS_FAKE_MULTIPLAYER_MODE_SET()
		OR IS_BIT_SET(MC_serverBD.iServerBitset2, SBBOOL2_SERVER_INITIALISATION_TIMED_OUT)
			
			#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(MC_serverBD.iServerBitset2, SBBOOL2_SERVER_INITIALISATION_TIMED_OUT)
				PRINTLN("[RCC MISSION][PROCESS_SERVER] HAVE_ALL_CORONA_PLAYERS_JOINED returning TRUE")
			ELSE
				PRINTLN("[RCC MISSION][PROCESS_SERVER] Check HAVE_ALL_CORONA_PLAYERS_JOINED, but server timed out while initialising...")
			ENDIF
			#ENDIF
			
			FOR iteam = 0 TO (FMMC_MAX_TEAMS-1)
				MC_serverBD.iNumStartingPlayers[iteam] = GET_NUMBER_PLAYERS_JOINING_MISSION(iteam)
				PRINTLN("[RCC MISSION][PROCESS_SERVER] team = ",iteam," MC_serverBD.iNumStartingPlayers second grab = ",MC_serverBD.iNumStartingPlayers[iteam])
			ENDFOR
			
			MC_serverBD.iTotalNumStartingPlayers = MC_serverBD.iNumStartingPlayers[0] + MC_serverBD.iNumStartingPlayers[1] + MC_serverBD.iNumStartingPlayers[2] + MC_serverBD.iNumStartingPlayers[3] 
			PRINTLN("[RCC MISSION][PROCESS_SERVER] MC_serverBD.iTotalNumStartingPlayers second grab  = ",MC_serverBD.iTotalNumStartingPlayers) 
			
			SET_BIT(MC_serverBD.iServerBitSet, SBBOOL_ALL_JOINED_START)
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)
								
			// Look for server eFnd conditions
			IF IS_EVERYONE_RUNNING(TRUE)
				PRINTLN("[RCC MISSION][PROCESS_SERVER] IS_EVERYONE_RUNNING RETURNING TRUE")
				
				START_MISSION_TIMERS()
				
				CLEAR_BIT(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
				CLEAR_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_FIRST_UPDATE_STARTED) 
				SET_BIT(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)
				SET_BIT(MC_serverbd.iServerBitSet, SSBOOL_ALL_READY_INTRO_CUT)
				
				FOR iteam = 0 TO (FMMC_MAX_TEAMS-1)
					MC_serverBD.iNumStartingPlayers[iteam] = GET_STARTING_PLAYERS_FOR_TEAM(iteam)
					//FMMC2020 - lets do this in 1 grab if possible 
					PRINTLN("[RCC MISSION][PROCESS_SERVER] team = ",iteam," MC_serverBD.iNumStartingPlayers third grab = ",MC_serverBD.iNumStartingPlayers[iteam])
				ENDFOR
				
				MC_serverBD.iTotalNumStartingPlayers = MC_serverBD.iNumStartingPlayers[0] + MC_serverBD.iNumStartingPlayers[1] + MC_serverBD.iNumStartingPlayers[2] + MC_serverBD.iNumStartingPlayers[3] 
				PRINTLN("[RCC MISSION][PROCESS_SERVER] MC_serverBD.iTotalNumStartingPlayers third grab  = ",MC_serverBD.iTotalNumStartingPlayers) 
				IF HAS_FIRST_STRAND_MISSION_BEEN_PASSED() OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
					IF g_TransitionSessionNonResetVars.iStrandTotalStartingPlayers > 0
						MC_serverBD.iTotalNumStartingPlayers = g_TransitionSessionNonResetVars.iStrandTotalStartingPlayers
						PRINTLN("[RCC MISSION][PROCESS_SERVER] Grabbing old starting players from previous strand part  = ",MC_serverBD.iTotalNumStartingPlayers) 
						FOR iteam = 0 TO (FMMC_MAX_TEAMS-1)
							MC_serverBD.iNumStartingPlayers[iteam] = g_TransitionSessionNonResetVars.iStrandStartingPlayers[iteam]
							PRINTLN("[RCC MISSION][PROCESS_SERVER] team = ",iteam," MC_serverBD.iNumStartingPlayers third grab from previous strand part = ",MC_serverBD.iNumStartingPlayers[iteam])
						ENDFOR
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)				
				
				#IF IS_DEBUG_BUILD
					IF iPlayerPressedF != -1
					OR iPlayerPressedS != -1
						END_MISSION_TIMERS()
						PRINTLN("[RCC MISSION][PROCESS_SERVER] MISSION OVER SETTING TIME TO: ",MC_serverBD.iTotalMissionEndTime )
						SET_BIT(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
						#IF IS_DEBUG_BUILD
						IF iPlayerPressedF != -1
						AND bAutoForceRestartNoCorona
							PRINTLN("[PROCESS_SERVER] FMMC EOM - sc_AutoForceRestartNoCorona - FMMC_EOM_SERVER_END_OF_FRAME_PROCESSING - VOTE PASSED FOR QUICK RESTART")
							MC_serverBD.sServerFMMC_EOM.bVotePassed = TRUE
							MC_serverBD.sServerFMMC_EOM.vWarpPos 	= g_vFMMC_MissionStartPos
							MC_serverBD.sServerFMMC_EOM.contentID	= ""	// Added by Keith for 'Random' 2/7/13 - wasn't used for 'Quick Restart' so NULLifying to prevent confusion
							MC_serverBD.sServerFMMC_EOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART			
							PRINTLN("[PROCESS_SERVER] FMMC EOM - sc_AutoForceRestartNoCorona - MC_serverBD.sServerFMMC_EOM.vWarpPos  = ", MC_serverBD.sServerFMMC_EOM.vWarpPos)
							PRINTLN("[PROCESS_SERVER] FMMC EOM - sc_AutoForceRestartNoCorona - MC_serverBD.sServerFMMC_EOM.contentID = ", MC_serverBD.sServerFMMC_EOM.contentID)
						ENDIF
						#ENDIF 
					ENDIF
				#ENDIF
				
			ELSE
				
				IF HAVE_ALL_PLAYERS_FINISHED()
					IF NOT HAS_NET_TIMER_STARTED(tdeomwaittimer)
						START_NET_TIMER(tdeomwaittimer)
					ENDIF
					
					IF NOT PROCESS_PLAYER_TEAM_SWAP()
					OR (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdeomwaittimer) >= ciTIMESTAMP_FOR_TEAM_SWAP)
						PRINTLN("[RCC MISSION][PROCESS_SERVER] HAVE_ALL_PLAYERS_FINISHED returning TRUE")
						PRINTLN("[PLAYER_LOOP] - PROCESS_SERVER_GAME_STATE_RUNNING_OLD")
						REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
							COUNT_PLAYER_SCORES(iparticipant)
						ENDREPEAT
						
						PROCESS_SERVER_SORTING_WINNING_TEAMS()
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSUMO_ZONE_CAPTURE_BAR_WINNER)
							PRINTLN("[RCC MISSION][PROCESS_SERVER] ciSUMO_ZONE_CAPTURE_BAR_WINNER set, call GIVE_TEAM_ENOUGH_POINTS_TO_PASS for winning team ", MC_serverBD.iWinningTeam)
							GIVE_TEAM_ENOUGH_POINTS_TO_PASS(MC_serverBD.iWinningTeam)
						ENDIF
						
						ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND()
						
						SET_SERVER_GAME_STATE(GAME_STATE_END)
					ELSE
						PRINTLN("[RCC MISSION][PROCESS_SERVER] MISSION WAITING TO SWAP TEAMS!")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_SERVER_GAME_STATE_RUNNING()
	
	PROCESS_SERVER_GAME_STATE_RUNNING_OLD()
	
	PROCESS_SERVER_END_CONDITIONS()
	
	PROCESS_SERVER_RULES_AND_OBJECTIVES()
	
	PROCESS_SERVER_SORTING_WINNING_TEAMS()
	
	PROCESS_SERVER_WORLD_EVERY_FRAME()
	
ENDPROC

PROC PROCESS_SERVER_GAME_STATE_END()
	
	//[FMMC2020] move all of these calls into _Ending
	
	BOOL bEveryoneOnLB
	PARTICIPANT_INDEX tempPart
	INT iParticipant
	
	//Set the data for rounds mission if we're on a round
	SERVER_SET_WINING_TEAM_BROAD_CAST_DATA()
	
	bEveryoneOnLB = TRUE
	FMMC_EOM_SERVER_RESET_PLAYER_COUNTS(sPlayerCounts)
	PRINTLN("[PLAYER_LOOP] - PROCESS_SERVER_GAME_STATE_END")
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
		FMMC_EOM_SERVER_PLAYER_PROCESSING(sPlayerCounts, iparticipant) 
		tempPart = INT_TO_PARTICIPANTINDEX(iparticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			IF NOT IS_BIT_SET(MC_PlayerBD[iparticipant].iClientBitSet,PBBOOL_ON_LEADERBOARD)
				bEveryoneOnLB = FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF bEveryoneOnLB
		MC_serverBD.sServerFMMC_EOM.bAllowedToVote = TRUE
		PRINTLN("[RCC MISSION][PROCESS_SERVER] MC_serverBD.sServerFMMC_EOM.bAllowedToVote = TRUE")
	ELSE
		PRINTLN("[RCC MISSION][PROCESS_SERVER] IF bEveryoneOnLB ELSE")
	ENDIF
	
	FMMC_EOM_SERVER_END_OF_FRAME_PROCESSING(sPlayerCounts, MC_serverBD.sServerFMMC_EOM, FMMC_TYPE_MISSION, 0, 0.5)
	
	IF SHOULD_RUN_NEW_VOTING_SYSTEM(sEndOfMission.iQuitProgress, MC_serverBD.sServerFMMC_EOM.iVoteStatus)
		IF NOT IS_THIS_A_ROUNDS_MISSION()
		OR SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
			SERVER_JOB_VOTE_PROCESSING(g_sMC_serverBDEndJob)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Runs the Server Game State Machine
///    Only run on the script host.
PROC PROCESS_SERVER()
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	SWITCH GET_MC_SERVER_GAME_STATE()
	
		CASE GAME_STATE_INI
			PROCESS_SERVER_GAME_STATE_INI()
		BREAK
		
		CASE GAME_STATE_RUNNING	
			PROCESS_SERVER_GAME_STATE_RUNNING()
		BREAK
		
		CASE GAME_STATE_END	
			PROCESS_SERVER_GAME_STATE_END()
		BREAK
		
	ENDSWITCH

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Bail/Quit Conditions ------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### Checks to see if the script needs to terminate for any reason. 
// ##### Called by all connected players every frame. 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_PLAYER_FAIL_QUIT_TEST_MODE_FOR_CREATOR()
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET() 
		EXIT
	ENDIF
	
	IF NOT g_FMMC_STRUCT.g_b_QuitTest
		EXIT
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		EXIT
	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
		SET_MC_CLIENT_GAME_STATE(GAME_STATE_MISSION_OVER)
	ELIF NOT IS_SCREEN_FADING_OUT()
		DO_SCREEN_FADE_OUT(500)
		ALLOW_MISSION_CREATOR_WARP(TRUE)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks for script bail conditions and terminates the script if they are met.
PROC CHECK_MISSION_BAIL_CONDITIONS()

	// If we have a match end event, bail.
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
			SCRIPT_TRIGGER_MUSIC_EVENT_FAIL()
			PRINTLN("[RCC MISSION] TRIGGER_MUSIC_EVENT(\"MP_MC_FAIL\") Cmbc1")
		ENDIF
		MC_SCRIPT_CLEANUP(FALSE)
	ENDIF
	
	IF SHOULD_PLAYER_LEAVE_MP_MISSION(GET_CORONA_STATUS() = CORONA_STATUS_IDLE) 
		CPRINTLN(DEBUG_MISSION,"[RCC MISSION] SHOULD_PLAYER_LEAVE_MP_MISSION true ")
		SET_BIT(sSaveOutVars.iBitSet,ciRATINGS_QUIT)
		iMissionResult = ciFMMC_END_OF_MISSION_STATUS_CANCELLED
		SCRIPT_TRIGGER_MUSIC_EVENT_FAIL()
		PRINTLN("[RCC MISSION] TRIGGER_MUSIC_EVENT(\"MP_MC_FAIL\") Cmbc2")
		
		IF g_bNeedToCallMatchEndOnQuit
		AND MC_playerBD[iLocalPart].iGameState >= GAME_STATE_RUNNING
			DEAL_WITH_FM_MATCH_END(FMMC_TYPE_MISSION, g_MissionControllerserverBD_LB.iMatchTimeID, DEFAULT, iMissionResult, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, IS_THIS_A_ROUNDS_MISSION(), MC_serverBD.iDifficulty, -1)
		ENDIF
		
		DoSkySwoopUpImmediateFadeOutIfNecessary()
		MC_SCRIPT_CLEANUP(FALSE)
	ENDIF	
	
ENDPROC

/// PURPOSE:
///    Runs all checks that may require the Mission Controller to Bail/Quit
///    Performs Script Cleanup if termination is required.
PROC PROCESS_PRE_FRAME_BAIL_CHECKS()

	//Check if the script should terminate
	CHECK_MISSION_BAIL_CONDITIONS()
	
	//Check if we should quit test mode
	PROCESS_PLAYER_FAIL_QUIT_TEST_MODE_FOR_CREATOR()
	
ENDPROC
