// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Dialogue Triggers -------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script Name: FM_Mission_Controller_2020.sc ------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: All logic triggering, blocking, playing & managing dialogue triggers.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Include_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: DIALOGUE SETTINGS -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to the creation, triggering and managing of dialogue ------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_DLC_DIALOGUE_SETTINGS()
	IF NOT g_bUse_MP_DLC_Dialogue	
			
		SET_USE_DLC_DIALOGUE(TRUE)		
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[Dialogue] - PROCESS_DLC_DIALOGUE_SETTINGS - g_bUse_MP_DLC_Dialogue Has Been Reset. Probably Ambient Script. Calling SET_USE_DLC_DIALOGUE")			
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_IGNORE, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "[Dialogue] - PROCESS_DLC_DIALOGUE_SETTINGS - g_bUse_MP_DLC_Dialogue Has Been Reset.")
		#ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: REPRINT DIALOGUE  -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to the creation, triggering and managing of dialogue ------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC MANAGE_OBJECTIVE_HELP_TEXT_REPRINT()

	// While a timer of 4 seconds is running, check for whether the player dies. This bitset will be set when a print_help is called in TRIGGER_DIALOGUE
	IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_CHECK_FOR_DEATH_OBJ_TEXT_REPRINT)		
		IF HAS_NET_TIMER_STARTED(tdObjHelpDeathCheckTimer)			
			IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdObjHelpDeathCheckTimer, ci_OBJ_HELP_DEATH_CHECK_TIME)				
				IF !bLocalPlayerPedOK
					PRINTLN("[LM][OBJ_TEXT_REPRINT] - Player died during the OBJ text print. So we must reprint it when they are alive.")
					RESET_NET_TIMER(tdObjHelpDeathCheckTimer)
					CLEAR_BIT(iLocalBoolCheck18, LBOOL18_CHECK_FOR_DEATH_OBJ_TEXT_REPRINT)
					SET_BIT(iLocalBoolCheck18, LBOOL18_DIED_AND_REQUIRES_OBJ_TEXT_REPRINT)
				ENDIF
			ENDIF
		ELIF NOT HAS_NET_TIMER_STARTED(tdObjHelpDeathCheckTimer)
			PRINTLN("[LM][OBJ_TEXT_REPRINT] - We tried to print the text, start a timer to check for death.")
			RESET_NET_TIMER(tdObjHelpDeathCheckTimer)
			START_NET_TIMER(tdObjHelpDeathCheckTimer)
		ENDIF
	ENDIF
		
	// We died during 4 (ci_OBJ_HELP_DEATH_CHECK_TIME) seconds of the OBJ help showing. Therefore the player may have missed it, so when they're back alive we're reprinting that bad boy.
	IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_DIED_AND_REQUIRES_OBJ_TEXT_REPRINT)
		PRINTLN("[LM][OBJ_TEXT_REPRINT] - Checking to make sure the player isn't dead...")
		IF bLocalPlayerPedOK
			PRINTLN("[LM][OBJ_TEXT_REPRINT] - Ped is no longer dead.")
			IF IS_SCREEN_FADED_IN()
			AND NOT IS_HUD_HIDDEN()
				IF NOT IS_STRING_NULL_OR_EMPTY(sLabelObjHelpReprint)
					PRINTLN("[LM][OBJ_TEXT_REPRINT] - Reprinting the cached help text.")
					PRINT_HELP(sLabelObjHelpReprint)
				ENDIF
				
				sLabelObjHelpReprint = ""
				CLEAR_BIT(iLocalBoolCheck18, LBOOL18_DIED_AND_REQUIRES_OBJ_TEXT_REPRINT)
			ELSE 
				PRINTLN("[LM][OBJ_TEXT_REPRINT] - Screen/HUD not ready...")
			ENDIF
		ENDIF
	ENDIF
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: CREATE DIALOGUE  ------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to the creation, triggering and managing of dialogue ------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC STRING GET_CORRECT_FILTERS_HELP_STRING()
	IF IS_PC_VERSION()
	AND IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		PRINTLN("[KH] GET_CORRECT_FILTERS_HELP_STRING - PC Version, using keyboard, printing PC filters help")
		RETURN "FLTR_PC_HELP"
	ELSE
		PRINTLN("[KH] GET_CORRECT_FILTERS_HELP_STRING - Printing normal filters help")
		RETURN "FILTERS_HELP"
	ENDIF
ENDFUNC

FUNC BOOL SHOULD_CUSTOM_CHARACTER_BE_APPENDED_TO_DIALOGUE(INT iDialogueTrigger)
	
	IF FMMC_IS_LONG_BIT_SET(iLocalDialogueForceCustomAppend, iDialogueTrigger)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_HEIST_DIALOGUE_TRIGGER_SUFFIX(INT iDialogueTrigger)
	
	INT iPlaythrough
		
	IF GET_CURRENT_HEIST_MISSION(TRUE) > -1
		iPlaythrough = GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER()
		IF iPlaythrough = -1
			iPlaythrough = 0
		ENDIF
		iPlaythrough += 1
		IF iPlayThrough > g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iPlaythroughAppendCap
			iPlayThrough = g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iPlaythroughAppendCap			
		ENDIF
	ENDIF
	
	RETURN iPlaythrough
	
ENDFUNC

FUNC STRING GET_FIXER_DATA_LEAK_DIALOGUE_TRIGGER_SUFFIX()
	
	INT iFixerStrandsCompleted
	IF HAS_PLAYER_COMPLETED_FIXER_STRAND(LocalPlayer, FS_PARTY_PROMOTER)
		iFixerStrandsCompleted++
	ENDIF
	IF HAS_PLAYER_COMPLETED_FIXER_STRAND(LocalPlayer, FS_BILLIONAIRE_GAMES)
		iFixerStrandsCompleted++
	ENDIF
	IF HAS_PLAYER_COMPLETED_FIXER_STRAND(LocalPlayer, FS_HOOD_PASS)
		iFixerStrandsCompleted++
	ENDIF
	
	STRING sSuffix = "A"
	
	SWITCH iFixerStrandsCompleted
		CASE 0
			sSuffix = "A"
		BREAK
		CASE 1
			sSuffix = "B"
		BREAK
		CASE 2
			sSuffix = "C"
		BREAK
	ENDSWITCH
	
	RETURN sSuffix
	
ENDFUNC

FUNC TEXT_LABEL_23 PROCESS_APPENDING_DIALOGUE_TRIGGER_SUFFIXES(STRING sRoot, INT iDialogueTrigger = -1)
	
	TEXT_LABEL_3 tlSpecialCharacter	
	TEXT_LABEL_23 tl23Root = sRoot
	
	IF iDialogueTrigger = -1
		RETURN tl23Root
	ENDIF
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iPlaythroughAppendCap > 0
	
		IF GET_CURRENT_HEIST_MISSION(TRUE) > -1
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - PROCESS_APPENDING_DIALOGUE_TRIGGER_SUFFIXES - Appending heist suffix")
			tl23Root += GET_HEIST_DIALOGUE_TRIGGER_SUFFIX(iDialogueTrigger)
		ELIF IS_THIS_A_FIXER_STORY_MISSION()
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - PROCESS_APPENDING_DIALOGUE_TRIGGER_SUFFIXES - Appending fixer data leak suffix")
			tl23Root += GET_FIXER_DATA_LEAK_DIALOGUE_TRIGGER_SUFFIX()
		ENDIF
		
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - PROCESS_APPENDING_DIALOGUE_TRIGGER_SUFFIXES - Playthrough append string result: ", tl23Root)

	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].tl3AppendOnAggro)
		IF HAS_ANY_TEAM_TRIGGERED_AGGRO(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iAggroIndexBS_Entity_Dialog)
		#IF IS_DEBUG_BUILD 
		AND NOT bBlockBranchingOnAggroKeepStealthDialogue
		#ENDIF
			tl23Root += g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].tl3AppendOnAggro
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - PROCESS_APPENDING_DIALOGUE_TRIGGER_SUFFIXES - Applying aggro suffix: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].tl3AppendOnAggro)			
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iDialogueBitset7, iBS_Dialogue7_AppendCharacter_TunerVehicle_Region)
		tlSpecialCharacter = GET_CHARACTER_TO_APPEND_FOR_TUNER_VEHICLE_REGION()
		IF NOT IS_STRING_NULL_OR_EMPTY(tlSpecialCharacter)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - PROCESS_APPENDING_DIALOGUE_TRIGGER_SUFFIXES - Special Character - Tuner Vehicle Region - tlSpecialCharacter: ", tlSpecialCharacter)			
			tl23Root += tlSpecialCharacter			
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iDialogueBitset7, iBS_Dialogue7_AppendCharacter_TunerVehicle_ModelName)		
		tlSpecialCharacter = GET_CHARACTER_TO_APPEND_FOR_TUNER_VEHICLE_MODEL_NAME()
		IF NOT IS_STRING_NULL_OR_EMPTY(tlSpecialCharacter)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - PROCESS_APPENDING_DIALOGUE_TRIGGER_SUFFIXES - Special Character - Tuner Vehicle Model Name - tlSpecialCharacter: ", tlSpecialCharacter)			
			tl23Root += tlSpecialCharacter			
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].tlAppendCustomCharacter)
		IF SHOULD_CUSTOM_CHARACTER_BE_APPENDED_TO_DIALOGUE(iDialogueTrigger)
			tl23Root += g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].tlAppendCustomCharacter
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - PROCESS_APPENDING_DIALOGUE_TRIGGER_SUFFIXES - Custom suffix: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].tlAppendCustomCharacter)			
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iPlayRandomVariationCap > 0
		INT iVariation = GET_RANDOM_INT_IN_RANGE(1, g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iPlayRandomVariationCap+1)
		
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - PROCESS_APPENDING_DIALOGUE_TRIGGER_SUFFIXES - Applying Variation suffix: ", iVariation)
		
		tl23Root += iVariation
	ENDIF
	
	RETURN tl23Root
	
ENDFUNC


FUNC BOOL CREATE_DIALOGUE(INT iDialogueTrigger, structPedsForConversation& speechPedStructToUse, STRING sBlock, STRING sRoot, enumConversationPriority eConvPriority)
	
	#IF IS_DEBUG_BUILD
		IF bBlockAllDialogue
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - CREATE_DIALOGUE - BLOCKED BY COMMANDLINE")
			RETURN TRUE
		ENDIF
	#ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iDialogueBitset3, iBS_Dialogue3_UsePhoneCall)
		IF GET_INCOMING_CONTACT_CHARACTER_FROM_CREATOR(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iCallCharacter) != MAX_NRM_CHARACTERS_PLUS_DUMMY			
			RETURN CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER(speechPedStructToUse, GET_INCOMING_CONTACT_CHARACTER_FROM_CREATOR(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iCallCharacter), sBlock, sRoot, eConvPriority)
		ENDIF
	ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iDialogueBitset6, iBS_Dialogue6_PlayDialogueAsAmbientSpeech) 
		TEXT_LABEL_31 tl31_Context = GET_FILENAME_FOR_AUDIO_CONVERSATION(sRoot)
		TEXT_LABEL_31 tl31_VoiceName = g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].tlCustomVoice
		VECTOR vPos = MC_GET_DIALOGUE_TRIGGER_POSITION(iDialogueTrigger)
		
		// This is done to support some of the quirks that the dialogue star tools cause when building out contexts. We cannot properly check the parameters so we are going to guess.
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "][Dialogue Triggers][Ambient] - CREATE_DIALOGUE - sRoot: ", sRoot, " and tl31_Context: ", tl31_Context, " appending A so that we can locate a context pointer")
		
		TEXT_LABEL_31 tlRoot
		
		tlRoot = sRoot 
		tlRoot += "A"			
		tl31_Context = GET_FILENAME_FOR_AUDIO_CONVERSATION(tlRoot)
		
		IF ARE_STRINGS_EQUAL(tl31_Context, "NULL")				
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "][Dialogue Triggers][Ambient] - CREATE_DIALOGUE - sRoot: ", sRoot, " and tl31_Context: ", tl31_Context, " is null with Appended value tlRoot: ", tlRoot, " so trying again..")
			tlRoot = sRoot 
			tlRoot += "_1A"
			tl31_Context = GET_FILENAME_FOR_AUDIO_CONVERSATION(tlRoot)				
		ENDIF
		
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "][Dialogue Triggers][Ambient] - CREATE_DIALOGUE - sRoot: ", sRoot, " Appended '", tlRoot, "' Final Result tl31_Context: ", tl31_Context)
	
		SPEECH_PARAMS speechParams = SPEECH_PARAMS_FORCE_NORMAL_CRITICAL
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iDialogueBitset5, iBS_Dialogue5_PlayFromIslandSpeakers)
			OBJECT_INDEX oiSpeaker
			INT iObjStringIndex = g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iObjectModelToTriggerStringID
			IF iObjStringIndex > -1
				TEXT_LABEL_63 tl63ModelString = GET_CUSTOM_STRING_LIST_TEXT_LABEL(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iObjectModelToTriggerStringID)
				PRINTLN("[Dialogue][Ambient] - CREATE_DIALOGUE - Play from closest specified model: ", tl63ModelString)
				oiSpeaker = GET_CLOSEST_SPEAKER_OF_SPECIFIED_MODEL(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(tl63ModelString)))
			ELSE
				PRINTLN("[Dialogue][Ambient] - CREATE_DIALOGUE - Play from island speaker!")
				oiSpeaker = GET_CLOSEST_CAYO_PERICO_SPEAKER(0)
			ENDIF
		
			IF DOES_ENTITY_EXIST(oiSpeaker)
				vPos = GET_ENTITY_COORDS(oiSpeaker)
			ENDIF
			speechParams = SPEECH_PARAMS_FORCE_MEGAPHONE
		ENDIF
		
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "][Dialogue Triggers][Ambient] - CREATE_DIALOGUE - Play ambiently from from Coord - ", vPos, "  tl31_VoiceName: ", tl31_VoiceName, "  sRoot: ", sRoot, " tlRoot: ", tlRoot, " tl31_Context: ", tl31_Context)
		PLAY_AMBIENT_SPEECH_FROM_POSITION(tl31_Context, tl31_VoiceName, vPos, speechParams)
		RETURN TRUE
	ELSE
		RETURN CREATE_CONVERSATION(speechPedStructToUse, sBlock, sRoot, eConvPriority)
	ENDIF
	
	RETURN FALSE
ENDFUNC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: TRIGGER DIALOGUE  -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to the creation, triggering and managing of dialogue ------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL TRIGGER_DIALOGUE()

	INT iPedVoice
	TEXT_LABEL_23 sLabel
	
	IF IS_SCREEN_FADING_OUT()
	OR IS_SCREEN_FADED_OUT()
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - Blocking dialogue trigger Because IS_SCREEN_FADING_OUT OR IS_SCREEN_FADED_OUT (these commands suppress/clear subtitles)")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[ iDialogueProgress ].iDialogueBitset2, iBS_Dialogue2EnableEarlyEnd)
		IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_SKIP_HELP_TEXT)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - Blocking dialogue trigger LBOOL20_SKIP_HELP_TEXT")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[ iDialogueProgress ].iDialogueBitset2, iBS_Dialogue2DontPlayWhenAloneInVeh)
		IF IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
			IF GET_NUM_PEDS_IN_VEHICLE(GET_VEHICLE_PED_IS_IN(PlayerPedToUse)) < 2
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - Blocking dialogue trigger from activating as only 1 person in vehicle")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset3, iBS_Dialogue3_PlayHoverModeSFX)
		IF NOT IS_BIT_SET(iDialogueProgressSFX1Played[iDialogueProgress/32], iDialogueProgress%32)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - dialogue trigger Plays a sound (Flight_Unlock). Returning TRUE")
			PLAY_SOUND_FRONTEND(-1, "Hover_Unlock", "DLC_XM17_IAA_Deluxos_Sounds")
			SET_BIT(iDialogueProgressSFX1Played[iDialogueProgress/32], iDialogueProgress%32)
		ENDIF
		//RETURN TRUE
	ENDIF	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset3, iBS_Dialogue3_PlayFlightModeSFX)
		IF NOT IS_BIT_SET(iDialogueProgressSFX2Played[iDialogueProgress/32], iDialogueProgress%32)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - dialogue trigger Plays a sound (Hover_Unlock). Returning TRUE")
			PLAY_SOUND_FRONTEND(-1, "Flight_Unlock", "DLC_XM17_IAA_Deluxos_Sounds")	
			SET_BIT(iDialogueProgressSFX2Played[iDialogueProgress/32], iDialogueProgress%32)
		ENDIF
		//RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset8, iBS_Dialogue8_ForceHintCam)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - Pointing Hint Cam at Target. ID: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iHintCamEntityIndex, " Type: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iHintCamEntityType)
		TRIGGER_FORCED_HINT_CAM(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iHintCamEntityType, g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iHintCamEntityIndex, g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iHintCamTime)
	ENDIF
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iCustomStringTicker > -1
		TEXT_LABEL_15 tlCustomStringListTicker = GET_CUSTOM_STRING_LIST_TEXT_LABEL(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iCustomStringTicker)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - Printing custom string list ticker on rule start: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iCustomStringTicker, " / ", tlCustomStringListTicker)
		PRINT_TICKER(tlCustomStringListTicker)
	ENDIF
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iCompletePrereqOnStart != ciPREREQ_None
		SET_PREREQUISITE_COMPLETED(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iCompletePrereqOnStart, IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset8, iBS_Dialogue8_CompletePrereqOnStart_LocalOnly))
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialogueHelpText)
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - iBS_DialogueHelpText")
		
		IF IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[ iDialogueProgress ].iDialogueBitset2, iBS_Dialogue2AimFireDriveByOnly )
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - iBS_Dialogue2AimFireDriveByOnly is set")
			IF GET_IS_USING_ALTERNATE_DRIVEBY()
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - player is using alternative control scheme")
				sLabel = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlRoot
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - DISPLAYING HELP TEXT:", sLabel)
				PRINT_HELP(sLabel)
				
			ENDIF
		ENDIF
		IF IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[ iDialogueProgress ].iDialogueBitset2, iBS_Dialogue2FireDriveByOnly)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - iBS_Dialogue2FireDriveByOnly is set")
			IF NOT GET_IS_USING_ALTERNATE_DRIVEBY()
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - player is not using alternative control scheme")
				sLabel = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlRoot
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - DISPLAYING HELP TEXT:", sLabel)
				PRINT_HELP(sLabel)
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[ iDialogueProgress ].iDialogueBitset2, iBS_Dialogue2AimFireDriveByOnly )
		AND NOT IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[ iDialogueProgress ].iDialogueBitset2, iBS_Dialogue2FireDriveByOnly)
			sLabel = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlRoot
			
			IF ARE_STRINGS_EQUAL(sLabel, "FILTERS_HELP")
				sLabel = GET_CORRECT_FILTERS_HELP_STRING()
			ENDIF
			
			IF bLocalPlayerOK
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "][Dialogue Triggers][OBJ_TEXT_REPRINT] - TRIGGER_DIALOGUE TRIGGER_DIALOGUE - NOT iBS_Dialogue2AimFireDriveByOnly, NOT iBS_Dialogue2FireDriveByOnly - DISPLAYING HELP TEXT: ", sLabel)
				sLabelObjHelpReprint = sLabel
				SET_BIT(iLocalBoolCheck18, LBOOL18_CHECK_FOR_DEATH_OBJ_TEXT_REPRINT)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "][Dialogue Triggers][OBJ_TEXT_REPRINT] - TRIGGER_DIALOGUE - Start death check timers and cache the help text for use later.")
			
				PRINT_HELP(sLabel)
			ELSE					
				sLabelObjHelpReprint = sLabel
				SET_BIT(iLocalBoolCheck18, LBOOL18_CHECK_FOR_DEATH_OBJ_TEXT_REPRINT)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "][Dialogue Triggers][OBJ_TEXT_REPRINT] - TRIGGER_DIALOGUE - Start death check timers and cache the help text for use later.")					
			ENDIF
		ENDIF
	
		RETURN TRUE
	ENDIF
	
	PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - ROOT IS ",g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlRoot," Minigame: ",IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialogueTriggerMinigame))
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[ iDialogueProgress ].iDialogueBitset2, iBS_Dialogue2EnableEarlyEnd)
		sTLToSkip =  sLabel
	ELSE
		sTLToSkip =  ""
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset, iBS_DialogueEnableQuickMask)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "][Dialogue Triggers][Mask] - TRIGGER_DIALOGUE - IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset, iBS_DialogueEnableQuickMask) - QUICK EQUIP MASK - LBS_MASK_QUICK_EQUIP_AVAILABLE - SET")
		SET_BIT(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIP_AVAILABLE)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2RemoveMaskPrompt)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "][Dialogue Triggers][Mask] - TRIGGER_DIALOGUE - IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2RemoveMaskPrompt) - QUICK EQUIP MASK - LBS_MASK_QUICK_UNEQUIP_AVAILABLE - SET")
		SET_BIT(iLocalQuickMaskBitSet, LBS_MASK_QUICK_UNEQUIP_AVAILABLE)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2,iBS_Dialogue2TurnOffVehicleLights)
		NETWORK_INDEX netVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[ g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iVehicleTrigger ]
		IF NETWORK_DOES_NETWORK_ID_EXIST(netVeh)
			VEHICLE_INDEX lightVeh = NET_TO_VEH(netVeh)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(lightVeh)
				IF IS_VEHICLE_OK(lightVeh)
					SET_VEHICLE_LIGHTS(lightVeh,FORCE_VEHICLE_LIGHTS_OFF)
					SET_VEHICLE_INTERIORLIGHT(lightVeh,FALSE)
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialogueTriggerMinigame)
		IF MC_playerBD[iLocalPart].iHackMGDialogue = -1
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialogueTriggerMinigame)")
			
			BROADCAST_HEIST_BEGIN_HACK_MINIGAME(ALL_PLAYERS_ON_SCRIPT(TRUE), iDialogueProgress)
			MC_playerBD[iLocalPart].iHackMGDialogue = iDialogueProgress
		ENDIF
	ENDIF
	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2ChangeTOD)
		INT iTeam = MC_playerBD[iPartToUse].iteam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		IF iRule < FMMC_MAX_RULES
			SET_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_ALLOW_TOD_CHANGE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2CallAirstrike)
		IF NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_DIALOGUE_TRIGGERED)
			SET_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_DIALOGUE_TRIGGERED)
			vDialogueAirstrikeCoords = MC_GET_DIALOGUE_TRIGGER_POSITION(iDialogueProgress)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - Setting LBOOL24_AIRSTRIKE_DIALOGUE_TRIGGERED and vDialogueAirstrikeCoords: ", vDialogueAirstrikeCoords)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2CallAirstrikeOrbitalCannon)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - Using orbital Cannon instead.")
				SET_BIT(iLocalBoolCheck25, LBOOL25_ORBITAL_CANNON_DIALOGUE_AIRSTRIKE)
			ELSE
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - Using regular airstrike.")	
				CLEAR_BIT(iLocalBoolCheck25, LBOOL25_ORBITAL_CANNON_DIALOGUE_AIRSTRIKE)
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	
	IF NOT IS_CONVERSATION_STATUS_FREE()
		TEXT_LABEL_23 tlConvLbl = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		
		IF NOT ARE_STRINGS_EQUAL(tlConvLbl,"NULL")
			INT iDialogue = -1
			
			// iDialogueProgressLastPlayed Set the moment we start playing some dialogue. iLocalDialoguePlayingBS to check if we're still playing it.
			IF iDialogueProgressLastPlayed != -1
			AND IS_BIT_SET(iLocalDialoguePlayingBS[iDialogueProgressLastPlayed/32],iDialogueProgressLastPlayed%32)
				TEXT_LABEL_23 tlDLoop
				IF IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgressLastPlayed].tlRoot)
					tlDLoop = GET_DIALOGUE_LABEL_FROM_ID(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgressLastPlayed].iFileID, g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgressLastPlayed].iDialogueID)
				ELSE
					tlDLoop = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgressLastPlayed].tlRoot
					tlDLoop = PROCESS_APPENDING_DIALOGUE_TRIGGER_SUFFIXES(tlDLoop, iDialogueProgressLastPlayed)
				ENDIF
				IF ARE_STRINGS_EQUAL(tlConvLbl, tlDLoop)
					iDialogue = iDialogueProgressLastPlayed
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - dialogue ", iDialogueProgressLastPlayed," is still playing")
				ENDIF
			ENDIF
			
			IF iDialogue != -1 //If the dialogue playing is due to the mission creator
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialogueInterrupts)
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialogueInstantInterrupt)
					IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPriority > g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPriority
						//It's okay to overwrite this currently playing dialogue with the new one - continue on to below
						IF NOT IS_BIT_SET(iDialogueKilledBS[iDialogue/32],iDialogue%32)
							KILL_FACE_TO_FACE_CONVERSATION()
							SET_BIT(iDialogueKilledBS[iDialogue/32],iDialogue%32)
						ENDIF
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - FALSE - Calling KILL_FACE_TO_FACE_CONVERSATION on dialogue: ", iDialogue, " because it has a lower priority.")
						RETURN FALSE
					ELSE
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - FALSE - current dialogue has priority over/equal to new dialogue: ", iDialogue)
						RETURN FALSE
					ENDIF
				ELSE
					IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialogueInstantInterrupt)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						SET_BIT(iLocalBoolCheck9,LBOOL9_DO_INSTANT_DIALOGUE_LOOP)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - new dialogue interrupting old dialogue: ", iDialogue, " IMMEDIATELY")
					ELSE
						KILL_FACE_TO_FACE_CONVERSATION()
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - new dialogue interrupting old dialogue: ", iDialogue)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - current dialogue has no label")
		ENDIF
	ENDIF
	
	IF (g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iFileID = DIALOGUE_FILE_ID_EMPTY AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlBlock))
	OR ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlBlock, "EMPTY")
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE fileID is empty, returning without playing dialogue")
		RETURN TRUE
	ENDIF
	
	TEXT_LABEL_23 sBlock = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlBlock
	IF IS_STRING_NULL_OR_EMPTY(sBlock)
	OR IS_STRING_BLANK_SPACES(sBlock, GET_LENGTH_OF_LITERAL_STRING(sBlock))
		sBlock = GET_DIALOGUE_BLOCK_FROM_ID(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iFileID)
	ENDIF
	PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - sBlock = ", sBlock, " with length ", GET_LENGTH_OF_LITERAL_STRING(sBlock))
	
	TEXT_LABEL_23 sRoot = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlRoot
	sRoot = PROCESS_APPENDING_DIALOGUE_TRIGGER_SUFFIXES(sRoot, iDialogueProgress)
	
	IF IS_STRING_NULL_OR_EMPTY(sRoot)
	OR IS_STRING_BLANK_SPACES(sRoot, GET_LENGTH_OF_LITERAL_STRING(sRoot))
		sRoot = GET_DIALOGUE_LABEL_FROM_ID(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iFileID, g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueID)
		sRoot = PROCESS_APPENDING_DIALOGUE_TRIGGER_SUFFIXES(sRoot, iDialogueProgress)
	ENDIF
	
	PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - sRoot = ", sRoot, " with length ", GET_LENGTH_OF_LITERAL_STRING(sRoot))
	
	IF NOT IS_BIT_SET(iLocalBoolCheck3, LBOOL3_ADDED_DIALOGUE_PED)
				
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset5, iBS_Dialogue5_PlayFromIslandSpeakers)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset6, iBS_Dialogue6_PlayDialogueAsAmbientSpeech)	
			
			FMMC_PED_STATE sPedState
			
			//TEXT_LABEL_23 ThisSpeakListenLabel = sRoot
   			//ThisSpeakListenLabel += "SL"
			INT iLines = 60//ROUND(TO_FLOAT(GET_LENGTH_OF_LITERAL_STRING(ThisSpeakListenLabel))/3)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - iLines = ", iLines)
			INT iLastSpeaker 				= -1
			INT iSpeakerList[FMMC_MAX_SPEAKERS]
			INT i, iList
			
			FOR iList = 0 TO FMMC_MAX_SPEAKERS-1
				iSpeakerList[iList] = -1
			ENDFOR
			REPEAT iLines i
			
				iCurrentSpeaker = GET_SPEAKER_INT_FROM_ROOT(sRoot, FMMC_MAX_SPEAKERS, i)
				
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - iCurrentSpeaker = ", iCurrentSpeaker, " Line: ", i, " iLastSpeaker: ", iLastSpeaker)
				
				IF iCurrentSpeaker = 9999
					i = iLines
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - EXIT LOOP - GONE PAST MAX LINES")
				ENDIF
				
				IF iCurrentSpeaker >= constMaxNum_Conversers
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - iCurrentSpeaker >= constMaxNum_Conversers setting to 0 from ", iCurrentSpeaker)
					iCurrentSpeaker = 0
				ENDIF
				
				IF iCurrentSpeaker != iLastSpeaker
				AND iCurrentSpeaker > 0
					IF NOT DOES_CREATOR_SPEAKER_VOICE_BELONG_TO_A_PARTICIPANT(iCurrentSpeaker)
						IF iLastSpeaker = -1
							iPedVoice = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPedVoice
						ELSE
							IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset3, iBS_Dialogue3_UseAlternatingSpeakers)
								IF iSpeakerList[iLastSpeaker] = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPedVoice
									iPedVoice = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iSecondPedVoice
									PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - getting iPedVoice from Second Ped Voice")
								ELSE
									iPedVoice = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPedVoice
									PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - getting iPedVoice from Primary Ped Voice")
								ENDIF
								IF iSpeakerList[iCurrentSpeaker] = -1
									iSpeakerList[iCurrentSpeaker] = iPedVoice
									PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - Setting iSpeakerList[",iCurrentSpeaker,"] to ", iSpeakerList[iCurrentSpeaker])
								ENDIF
							ELSE
								iPedVoice = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iSecondPedVoice
							ENDIF
						ENDIF
					ENDIF
				
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - g_FMMC_STRUCT.sDialogueTriggers[",iDialogueProgress,"].iPedVoice =  ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPedVoice)
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - g_FMMC_STRUCT.sDialogueTriggers[",iDialogueProgress,"].iSecondPedVoice = ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iSecondPedVoice)
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - Voice for this line iPedVoice = ", iPedVoice) 
					iLastSpeaker = iCurrentSpeaker
					
					TEXT_LABEL_23 sSpeaker = g_FMMC_STRUCT.sSpeakers[iCurrentSpeaker]
					
					IF IS_STRING_NULL_OR_EMPTY(sSpeaker)
						sSpeaker = GET_PED_VOICE_ID(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iFileID, sRoot)
					ENDIF
					
					PED_INDEX SpeakerPed = NULL
					
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - iPedVoice = ", iPedVoice," iDialogueProgress = ",iDialogueProgress, " sSpeaker: ", sSpeaker)
					
					IF iPedVoice != -1
						IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iPedVoice])
							SpeakerPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPedVoice])
							PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - adding speaker ped since iPedVoice = ", iPedVoice)
							
							IF ARE_STRINGS_EQUAL(sRoot,"NAFIN_IG1")
							OR ARE_STRINGS_EQUAL(sRoot,"NAFIN_IG2")
								PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - Playing Trevor vignette.")
								IF NOT IS_PED_IN_COVER(SpeakerPed)
									PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - returning false as NAFIN_IG2 and Trevor is not in cover")
									RETURN FALSE
								ENDIF
							ENDIF
							
							//Start the ped using their phone if we have control!
							IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset, iBS_DialogueUsePhone)
								PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialogueUsePhone) ")
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPedVoice])
									SET_PED_CONFIG_FLAG(SpeakerPed,PCF_PhoneDisableTextingAnimations,FALSE)
									SET_PED_CONFIG_FLAG(SpeakerPed,PCF_PhoneDisableTalkingAnimations,FALSE)
									IF taskPerformAmbientAnimation != NULL
										FILL_FMMC_PED_STATE_STRUCT(sPedState, iPedVoice)
										CALL taskPerformAmbientAnimation(sPedState, ciPED_IDLE_ANIM__PHONE)
									ENDIF
									PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - Calling TASK_PERFORM_AMBIENT_ANIMATION w params SpeakerPed,iPedVoice,ciPED_IDLE_ANIM__PHONE")
								ENDIF
								SET_BIT(iLocalBoolCheck6, LBOOL6_DIALOGUE_ANIM_NEEDED)
							ELIF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPedAnimation != ciPED_IDLE_ANIM__NONE
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPedVoice])
									IF taskPerformAmbientAnimation != NULL
										FILL_FMMC_PED_STATE_STRUCT(sPedState, iPedVoice)
										CALL taskPerformAmbientAnimation(sPedState, g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPedAnimation)
									ENDIF
								ENDIF
								SET_BIT(iLocalBoolCheck6, LBOOL6_DIALOGUE_ANIM_NEEDED)
							ENDIF
						
							
						ENDIF
					ENDIF
					
					INT iSpeakerPart = -1
					IF DOES_CREATOR_SPEAKER_VOICE_BELONG_TO_A_PARTICIPANT(iCurrentSpeaker)
						iSpeakerPart = GET_PARTICIPANT_CREATOR_SPEAKER_VOICE_BELONGS_TO(iCurrentSpeaker)
						
						PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iSpeakerPart)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)							
							PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
							
							IF IS_NET_PLAYER_OK(tempPlayer)
								PED_INDEX tempPed = GET_PLAYER_PED(tempPlayer)
								
								IF NOT IS_PED_INJURED(tempPed)
									SpeakerPed = tempPed
								ENDIF
							ENDIF
						ENDIF
					ENDIF
										
					IF iPedVoice =-1
					OR NOT IS_PED_INJURED(SpeakerPed)
						ADD_PED_FOR_DIALOGUE(speechPedStruct, iCurrentSpeaker, SpeakerPed, sSpeaker)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - ADD_PED_FOR_DIALOGUE(speechPedStruct, ", iCurrentSpeaker, ", PED_INDEX, ", sSpeaker, " ) ")
						SET_BIT(iLocalBoolCheck3, LBOOL3_ADDED_DIALOGUE_PED)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - sBlock = ", sBlock)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - sRoot = ", sRoot)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - iDialogueProgress = ", iDialogueProgress)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - iFileID = ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iFileID)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - iDialogueID = ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueID)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - tlBlock = ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlBlock)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - tlRoot = ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlRoot)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - GET_SPEAKER_INT_FROM_ROOT = ", iCurrentSpeaker)						
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - iSpeakerPart = ", iSpeakerPart)	
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - GET_PED_VOICE_ID = ", sSpeaker)						
					ENDIF
				ENDIF							
			ENDREPEAT
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset6, iBS_Dialogue6_PlayDialogueAsAmbientSpeech)
			SET_BIT(iLocalBoolCheck3, LBOOL3_ADDED_DIALOGUE_PED)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - iDialogueProgress = ", iDialogueProgress)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - tlBlock = ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlBlock)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - tlRoot = ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlRoot)
		ENDIF
	ELSE
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - iCurrentSpeaker = ",  iCurrentSpeaker)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - GET_SPEAKER_INT_FROM_ROOT(sRoot, FMMC_MAX_SPEAKERS) = ",  GET_SPEAKER_INT_FROM_ROOT(sRoot, FMMC_MAX_SPEAKERS))
		IF iCurrentSpeaker != GET_SPEAKER_INT_FROM_ROOT(sRoot, FMMC_MAX_SPEAKERS)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - Cleared  LBOOL3_ADDED_DIALOGUE_PED")
			CLEAR_BIT(iLocalBoolCheck3, LBOOL3_ADDED_DIALOGUE_PED)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_ADDED_DIALOGUE_PED)
		
		enumConversationPriority ConvPriority
		
		SWITCH g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPriority
		CASE 0
			ConvPriority = CONV_PRIORITY_VERY_LOW
			BREAK
		CASE 1
			ConvPriority = CONV_PRIORITY_LOW
			BREAK
		CASE 2
			ConvPriority = CONV_PRIORITY_MEDIUM
			BREAK
		ENDSWITCH
		
		IF CREATE_DIALOGUE(iDialogueProgress, speechPedStruct, sBlock, sRoot, ConvPriority)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE TRUE - created new dialogue ", iDialogueProgress)
			
			//If we have a target ped to look at
			IF ARE_STRINGS_EQUAL(sRoot,"TUSCO_CHAT")
				INT iSpeechPed
				INT iSpeaker1 = -1
				INT iSpeaker2 = -1
				REPEAT constMaxNum_Conversers iSpeechPed
					IF speechPedStruct.PedInfo[iSpeechPed].ActiveInConversation
						IF iSpeaker1 = -1
							iSpeaker1 = iSpeechPed
						ELIF iSpeaker2 = -1
							iSpeaker2 = iSpeechPed
						ELSE
							PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - More than two peds trying to look at each other, chaos imminent")
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF iSpeaker1 > -1 AND iSpeaker2 > -1 AND iSpeaker1 != iSpeaker2
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - iSpeaker1 > -1 AND iSpeaker2 > -1")
					PED_INDEX lookAtPed1, lookAtPed2
					lookAtPed1 = speechPedStruct.PedInfo[iSpeaker1].Index
					lookAtPed2 = speechPedStruct.PedInfo[iSpeaker2].Index
					IF NOT IS_PED_INJURED(lookAtPed1) AND NOT IS_PED_INJURED(lookAtPed2)
						
						INT iPed1 = IS_ENTITY_A_MISSION_CREATOR_ENTITY(lookAtPed1)
						INT iPed2 = IS_ENTITY_A_MISSION_CREATOR_ENTITY(lookAtPed2)
						
						TASK_LOOK_AT_ENTITY(lookAtPed1,lookAtPed2,-1,SLF_WHILE_NOT_IN_FOV,SLF_LOOKAT_VERY_HIGH)
						TASK_LOOK_AT_ENTITY(lookAtPed2,lookAtPed1,-1,SLF_WHILE_NOT_IN_FOV,SLF_LOOKAT_VERY_HIGH)
						FMMC_SET_LONG_BIT(iPedLookAtBS, iPed1)
						FMMC_SET_LONG_BIT(iPedLookAtBS, iPed2)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - Peds ", iPed1, " and ", iPed2, " looking at each other")
					ENDIF
				ENDIF
			ENDIF
			
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - Cleared LBOOL3_ADDED_DIALOGUE_PED")
			CLEAR_BIT(iLocalBoolCheck3, LBOOL3_ADDED_DIALOGUE_PED)
			
			IF NOT MC_playerBD[iLocalPart].bPlayedFirstDialogue
				IF g_FMMC_STRUCT.iTripSkipHelpDialogue = -1
				OR g_FMMC_STRUCT.iTripSkipHelpDialogue = iDialogueProgress
					MC_playerBD[iLocalPart].bPlayedFirstDialogue = TRUE
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - MC_playerBD[iLocalPart].bPlayedFirstDialogue = TRUE")
				ENDIF
			ENDIF
			
			//If clearing help text check for specific help text being set in tlDialogueTriggerAdditionalHelpText, if not then clear all existing.
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset5, iBS_Dialogue5_ClearExistingHelpText)
			AND IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlDialogueTriggerAdditionalHelpText)
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP()
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - iBS_Dialogue5_ClearExistingHelpText is set and help message was being displayed - clearing existing help text.")
				ENDIF
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlDialogueTriggerAdditionalHelpText)
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
				sLabel = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlDialogueTriggerAdditionalHelpText
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset5, iBS_Dialogue5_ClearExistingHelpText)
					IF bLocalPlayerOK
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "][Dialogue Triggers][OBJ_TEXT_REPRINT] - TRIGGER_DIALOGUE TRIGGER_DIALOGUE - DISPLAYING HELP TEXT:", sLabel)
						sLabelObjHelpReprint = sLabel
						SET_BIT(iLocalBoolCheck18, LBOOL18_CHECK_FOR_DEATH_OBJ_TEXT_REPRINT)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "][Dialogue Triggers][OBJ_TEXT_REPRINT] - TRIGGER_DIALOGUE - Start death check timers and cache the help text for use later.(1)")
						PRINT_HELP(sLabel)
					ELSE					
						sLabelObjHelpReprint = sLabel
						SET_BIT(iLocalBoolCheck18, LBOOL18_CHECK_FOR_DEATH_OBJ_TEXT_REPRINT)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "][Dialogue Triggers][OBJ_TEXT_REPRINT] - TRIGGER_DIALOGUE - Start death check timers and cache the help text for use later.(2)")					
					ENDIF
				ELSE //If this specific message is currently displayed then clear it.
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sLabel)
						CLEAR_HELP()
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "][Dialogue Triggers][OBJ_TEXT_REPRINT] - TRIGGER_DIALOGUE - iBS_Dialogue5_ClearExistingHelpText is set and specific help message was being displayed - clearing existing help text.")
					ENDIF
				ENDIF
			ENDIF
			 
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset7, iBS_Dialogue7_BlockHangUp)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - Phone cannot be hung up!! g_BS_DISABLE_INCOMING_OR_OUTGOING_CALL_HANGUP")
				SET_BIT(BitSet_CellphoneDisplay, g_BS_DISABLE_INCOMING_OR_OUTGOING_CALL_HANGUP)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset7, iBS_Dialogue7_DialogueTriggersPhoneCancelAnimContinueCall)
			AND NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_LOCAL_PLAYER_CANCEL_CALL_ANIMATION_AFTER_DELAY)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - TRIGGER_DIALOGUE - iBS_Dialogue7_DialogueTriggersPhoneCancelAnimContinueCall is set. Setting  so that we can cancel the anim once it's played for a given amount of time.")
				SET_BIT(iLocalBoolCheck34, LBOOL34_LOCAL_PLAYER_CANCEL_CALL_ANIMATION_AFTER_DELAY)
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: MANAGE DIALOGUE  ------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to the creation, triggering and managing of dialogue ------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL HAS_DIALOGUE_INDEX_PLAYED(INT iDialogue)
	
	IF IS_BIT_SET(iLocalDialoguePlayedBS[iDialogue/32], iDialogue%32)
	OR IS_BIT_SET(MC_playerBD[iPartToUse].iDialoguePlayedBS[iDialogue/32], iDialogue%32)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_DIALOGUE_TIMER()
	PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - CHECK_DIALOGUE_TIMER iDialogueProgress: ", iDialogueProgress, " Time Elapsed: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdDialogueTimer[iDialogueProgress]), " Time Delay: ", (g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTimeDelay*1000))
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTimeDelay != 0
		IF NOT HAS_NET_TIMER_STARTED(tdDialogueTimer[iDialogueProgress])
			REINIT_NET_TIMER(tdDialogueTimer[iDialogueProgress])
			SET_BIT(iLocalDialoguePlayingBS[iDialogueProgress/32],iDialogueProgress%32)
		ELSE
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdDialogueTimer[iDialogueProgress]) > (g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTimeDelay*1000)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_BE_REPLAYABLE(INT iDialogue)
	RETURN IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_IsReplayable)
ENDFUNC

PROC PROCESS_DIALOGUE_TRIGGERS_PLAYING_EVERY_FRAME()
	
	// Optimization. No need to run this loop if no options are set up to require it.
	IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_NO_NEED_FOR_DIALOGUE_EVERY_FRAME_PROCESSING)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_DIALOGUE)
	#ENDIF
	
	// Process Creator Options.
	BOOL bShutDownLoop = TRUE
	BOOL bDialogueGoingToStart
	INT iTrigger
	FOR iTrigger = 0 TO g_FMMC_STRUCT.iNumberOfDialogueTriggers - 1
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iTrigger].iDialogueBitset3, iBS_Dialogue3_BlockObjectiveWhilePlayingDialogue)						
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iTrigger].iDialogueBitset8, iBS_Dialogue8_BlockObjectiveWhilePlayingDialogueDelay)						
				IF (IS_BIT_SET(iLocalDialoguePlayingBS[iTrigger/32], iTrigger%32)
				OR (HAS_NET_TIMER_STARTED(tdDialogueTimer[iDialogueProgress]) AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdDialogueTimer[iDialogueProgress], g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTimeDelay*1000)))
				AND g_FMMC_STRUCT.sDialogueTriggers[iTrigger].iTimeDelay > 0
					bDialogueGoingToStart = TRUE
				ELSE
					bDialogueGoingToStart = FALSE
				ENDIF
			ENDIF
			
			// Don't shut down the loop if a trigger is playing.
			IF NOT IS_CONVERSATION_STATUS_FREE()
			OR IS_SCRIPTED_CONVERSATION_ONGOING()
			OR bDialogueGoingToStart				
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - PROCESS_DIALOGUE_TRIGGERS_PLAYING_EVERY_FRAME - has option set to block objective while playing - Dialogue ongoing...")				
				bShutDownLoop = FALSE
				
				IF HAS_DIALOGUE_INDEX_PLAYED(iTrigger)
				OR bDialogueGoingToStart					
					IF NOT HAS_NET_TIMER_STARTED(tdDialogueTriggerObjectiveBlockerFailSafe)
					OR NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdDialogueTriggerObjectiveBlockerFailSafe, ciDIALOGUE_TRIGGER_OBJECTIVE_BLOCKER_FAIL_SAFE_TIME)
						IF IS_BIT_SET(iLocalDialoguePlayingBS[iTrigger/32],iTrigger%32)	
						OR bDialogueGoingToStart						
							PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - PROCESS_DIALOGUE_TRIGGERS_PLAYING_EVERY_FRAME - Blocking the Objective due to this trigger being set up to do so.")
							BLOCK_OBJECTIVE_THIS_FRAME()
							Clear_Any_Objective_Text()
							START_NET_TIMER(tdDialogueTriggerObjectiveBlockerFailSafe)
						ENDIF
					ELSE
						RESET_NET_TIMER(tdDialogueTriggerObjectiveBlockerFailSafe)
						CLEAR_BIT(iLocalDialoguePlayingBS[iTrigger/32],iTrigger%32)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - PROCESS_DIALOGUE_TRIGGERS_PLAYING_EVERY_FRAME - Clearing the Objective Blocker as a fail safe has been hit.")
					ENDIF
				ENDIF
			ENDIF
			
			// Don't shutdown the loop if this trigger has not yet played.
			IF NOT IS_BIT_SET(iLocalDialoguePlayingBS[iTrigger/32],iTrigger%32)
			AND NOT HAS_DIALOGUE_INDEX_PLAYED(iTrigger)
				IF bShutDownLoop
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - PROCESS_DIALOGUE_TRIGGERS_PLAYING_EVERY_FRAME - Keeping Loop Alive")
					bShutDownLoop = FALSE
				ENDIF				
			ENDIF
		ENDIF
	ENDFOR
	
	// Shut down the loop.
	IF bShutDownLoop
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - Setting LBOOL32_NO_NEED_FOR_DIALOGUE_EVERY_FRAME_PROCESSING")
		SET_BIT(iLocalBoolCheck32, LBOOL32_NO_NEED_FOR_DIALOGUE_EVERY_FRAME_PROCESSING)	
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_DIALOGUE)
	#ENDIF
	
ENDPROC

PROC SET_DIALOGUE_AS_PLAYED_FOR_CONTINUITY(INT iDialogue)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_DialogueTracking)
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iContinuityId = -1
		EXIT
	ENDIF
	
	IF SHOULD_DIALOGUE_BE_REPLAYABLE(iDialogue)
		EXIT
	ENDIF
	
	FMMC_SET_LONG_BIT(sMissionLocalContinuityVars.iDialogueTrackingBitset, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iContinuityId)
	PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "][Dialogue Triggers][CONTINUITY] - SET_DIALOGUE_AS_PLAYED_FOR_CONTINUITY - Setting dialogue trigger with continuity ID ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iContinuityId, " as played")
	
ENDPROC

PROC MANAGE_DIALOGUE()
	IF CHECK_DIALOGUE_TIMER()
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset, iBS_DialogueNotHeardByTeam0 + MC_playerBD[iPartToUse].iteam)							
			IF TRIGGER_DIALOGUE()
				
				IF iDialogueProgressLastPlayed != -1
					CLEAR_BIT(iLocalDialoguePlayingBS[iDialogueProgressLastPlayed/32], iDialogueProgressLastPlayed%32)	
				ENDIF				
				iDialogueProgressLastPlayed = iDialogueProgress
				
				RESET_NET_TIMER(tdDialogueTimer[iDialogueProgress])
				SET_BIT(MC_playerBD[iLocalPart].iDialoguePlayedBS[iDialogueProgress/32], iDialogueProgress%32)
				SET_BIT(iLocalDialoguePlayingBS[iDialogueProgress/32], iDialogueProgress%32)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - MANAGE_DIALOGUE - Setting MC_playerBD[iLocalPart].iDialoguePlayedBS[",iDialogueProgress/32,"],",iDialogueProgress%32, " Call 1")
				SET_BIT(iLocalDialoguePlayedBS[iDialogueProgress/32], iDialogueProgress%32)				
				CLEAR_BIT(iLocalDialogueHangUpPlayedBS[iDialogueProgressLastPlayed/32], iDialogueProgressLastPlayed%32)
				SET_DIALOGUE_AS_PLAYED_FOR_CONTINUITY(iDialogueProgress)								
				
				IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_EXPEDITE_DIALOGUE_TRIGGERS_ENABLED)
					IF IS_BIT_SET(iLocalDialoguePending[iDialogueProgress/32], iDialogueProgress%32)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - MANAGE_DIALOGUE - iLocalDialoguePending was set it has now played, we can clear it.")
						CLEAR_BIT(iLocalDialoguePending[iDialogueProgress/32], iDialogueProgress%32)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset3, iBS_Dialogue3_BlockObjectiveWhilePlayingDialogue)						
					#IF IS_DEBUG_BUILD
					IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_NO_NEED_FOR_DIALOGUE_EVERY_FRAME_PROCESSING)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - MANAGE_DIALOGUE - Unshutting down the optimisation lock (LBOOL32_NO_NEED_FOR_DIALOGUE_EVERY_FRAME_PROCESSING) for blocking objective via dialogue triggers. Was shut down too early!")
					ENDIF
					#ENDIF
					CLEAR_BIT(iLocalBoolCheck32, LBOOL32_NO_NEED_FOR_DIALOGUE_EVERY_FRAME_PROCESSING)
				ENDIF
				
				REINIT_NET_TIMER(tdTimeSinceLastDialoguePlayed)
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset4, iBS_Dialogue4_BroadcastToAllPlayers)
				AND NOT IS_BIT_SET(iLocalDialoguePlayRemotelyWithPriority[iDialogueProgress/32], iDialogueProgress%32)
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - MANAGE_DIALOGUE - Broadcasting to other players.")					
					SET_BIT(iLocalDialoguePlayRemotelyWithPriority[iDialogueProgress/32], iDialogueProgress%32)
					BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_PlayDialogueTriggerIndexOnRemoteClients, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE, iDialogueProgress)
				ELSE
					IF IS_BIT_SET(iLocalDialoguePlayRemotelyWithPriority[iDialogueProgress/32], iDialogueProgress%32)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - MANAGE_DIALOGUE - Played some dialogue that was broadcast from another player. Resetting iLocalDialoguePlayRemotelyWithPriority...")
						CLEAR_BIT(iLocalDialoguePlayRemotelyWithPriority[iDialogueProgress/32], iDialogueProgress%32)
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_EXPEDITE_DIALOGUE_TRIGGERS_ENABLED)
					IF NOT IS_BIT_SET(iLocalDialoguePending[iDialogueProgress/32], iDialogueProgress%32)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - MANAGE_DIALOGUE - Dialogue iLocalDialoguePending set for iDialogueProgress: ", iDialogueProgress)
						SET_BIT(iLocalDialoguePending[iDialogueProgress/32], iDialogueProgress%32)
					ENDIF
				ENDIF
			ENDIF
		ELSE			
			SET_BIT(MC_playerBD[iLocalPart].iDialoguePlayedBS[iDialogueProgress/32], iDialogueProgress%32)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - MANAGE_DIALOGUE - Setting MC_playerBD[iLocalPart].iDialoguePlayedBS[",iDialogueProgress/32,"],", iDialogueProgress%32, " (", iDialogueProgress, ") Call 2")
			SET_BIT(iLocalDialoguePlayingBS[iDialogueProgress/32], iDialogueProgress%32)
			SET_BIT(iLocalDialoguePlayedBS[iDialogueProgress/32], iDialogueProgress%32)
			SET_DIALOGUE_AS_PLAYED_FOR_CONTINUITY(iDialogueProgress)
			//This dialogue trigger does not play for this team, skip playing it!
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - MANAGE_DIALOGUE - Dialogue checks were processed, but dialogue doesn't play for this team or not loaded for this team - skipping to next dialogue")
		ENDIF
	ENDIF
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: TRIGGER CONDITIONS  ---------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions that can prevent a dialogue trigger from firing and act based on Creator settings.  --------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE(INT iDialogue)
	
	BOOL bShouldTrigger
	
	IF (NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialogueTriggerOnWantedGain) )
	AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialogueTriggerOnWantedLoss) )
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2SeenByCops)
			IF HAS_NET_TIMER_STARTED(MC_serverBD.tdCopSpottedFailTimer)
				bShouldTrigger = TRUE
			ENDIF
		ELSE
			bShouldTrigger = TRUE
		ENDIF
		
	ELSE
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialogueWantedOnlyForCarrier)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - NOT iBS_DialogueWantedOnlyForCarrier")
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialogueTriggerOnWantedGain)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - iBS_DialogueTriggerOnWantedGain")
				
				IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE) //Don't trigger if in an active swap vehicle - url:bugstar:6131153
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_DontRetainWantedStatus)
						IF IS_BIT_SET(iLocalBoolCheck4,LBOOL4_T0GOTWANTED + g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam)
							bShouldTrigger = TRUE					
							PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - Trigger conditions met. GAINED WANTED TEAM dteam: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam)
						ENDIF
					ELSE
						IF GET_PLAYER_WANTED_LEVEL(LocalPlayer) > 0
							bShouldTrigger = TRUE
							PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - Triggering due to iBS_DialogueWantedOnlyForCarrier")
						ENDIF
					ENDIF
				ENDIF
			ELSE //iBS_DialogueTriggerOnWantedLoss is set
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - iBS_DialogueTriggerOnWantedLoss")
				IF IS_BIT_SET(iLocalBoolCheck4,LBOOL4_T0LOSTWANTED + g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam)
					bShouldTrigger = TRUE					
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - Trigger conditions met. LOST WANTED TEAM dteam: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam)
				ENDIF
			ENDIF
		ELSE //we don't care about the whole team, just the carrier
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - iBS_DialogueWantedOnlyForCarrier")
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialogueTriggerOnWantedGain)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - iBS_DialogueTriggerOnWantedGain")
				
				IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE) //Don't trigger if in an active swap vehicle - url:bugstar:6131153
					IF IS_BIT_SET(iTeamCarrierWantedBS, ciTCWBS_T0GOT + g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam)
						bShouldTrigger = TRUE
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - GAINED WANTED CARRIER ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam ," - bShouldTrigger = TRUE")
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - Trigger conditions met. GAINED WANTED CARRIER dteam: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - iBS_DialogueTriggerOnWantedLoss")
				IF IS_BIT_SET(iTeamCarrierWantedBS, ciTCWBS_T0LOST + g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam)
					bShouldTrigger = TRUE					
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - Trigger conditions met. LOST WANTED CARRIER dteam: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
	
ENDFUNC

FUNC BOOL HAS_PED_LEFT_OR_ENTERED_VEHICLE(INT iDialogue)

	BOOL bShouldTrigger
	
	IF (NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialoguePedInsideVehicle) )
	AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialoguePedOutsideVehicle) )
		bShouldTrigger = TRUE
	ELSE
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedVoice != -1 
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PED_LEFT_OR_ENTERED_VEHICLE - iPedVoice != -1")
			NETWORK_INDEX netPed = MC_serverBD_1.sFMMC_SBD.niPed[ g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedVoice ]
			IF NETWORK_DOES_NETWORK_ID_EXIST(netPed)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PED_LEFT_OR_ENTERED_VEHICLE - NETWORK_DOES_NETWORK_ID_EXIST(netPed)")
				IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialoguePedInsideVehicle)
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PED_LEFT_OR_ENTERED_VEHICLE - CHECK INSIDE")
					IF IS_PED_IN_ANY_VEHICLE(NET_TO_PED(netPed))
						bShouldTrigger = TRUE
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PED_LEFT_OR_ENTERED_VEHICLE - Trigger conditions met. IS_PED_IN_ANY_VEHICLE")
					ENDIF
				ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialoguePedOutsideVehicle)
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PED_LEFT_OR_ENTERED_VEHICLE - CHECK OUTSIDE")
					IF NOT IS_PED_IN_ANY_VEHICLE(NET_TO_PED(netPed))
						bShouldTrigger = TRUE
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - - HAS_PED_LEFT_OR_ENTERED_VEHICLE - Trigger conditions met. NOT IS_PED_IN_ANY_VEHICLE")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			bShouldTrigger = TRUE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PED_LEFT_OR_ENTERED_VEHICLE - Trigger conditions met. iPedVoice = -1")
		ENDIF
	ENDIF

	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE(INT iDialogue)
	
	BOOL bShouldTrigger
	
	IF (NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialoguePlayerInsideVehicle) )
	AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialoguePlayerOutsideVehicle) )
		bShouldTrigger = TRUE
	ELSE
		IF bPlayerToUseOK
			PED_INDEX tempPed = PlayerPedToUse
			IF NOT IS_PED_INJURED(tempPed)
				IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialoguePlayerInsideVehicle)
					//Don't process this for players that are not in the same team - url:bugstar:7424481	
					IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam != ciDIALOGUE_TRIGGER_TEAM_ANY
					AND g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam != MC_playerBD[iPartToUse].iTeam
						RETURN FALSE					
					ENDIF					
					
					BOOL bPlayerInOrEnteringVeh = IS_PED_IN_ANY_VEHICLE(tempPed, TRUE)
					BOOL bPlayerSittingInVeh 	= IS_PED_IN_ANY_VEHICLE(tempPed, FALSE)
					IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2RearSeats)
						VEHICLE_SEAT vs_Seat = GET_SEAT_PED_IS_IN(tempPed,TRUE)
						
						// Fixed Assert
						BOOL bInChernobog
						IF bPlayerInOrEnteringVeh
							// Fixes another assert...
							IF bPlayerSittingInVeh
								IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(tempPed)) = CHERNOBOG
									bInChernobog = TRUE
								ENDIF
							ELIF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(tempPed)) = CHERNOBOG
								bInChernobog = TRUE
							ENDIF							
						ENDIF

						IF (bPlayerInOrEnteringVeh 
							AND (vs_Seat = VS_BACK_LEFT 
							OR vs_Seat = VS_BACK_RIGHT 
							OR vs_Seat = VS_EXTRA_LEFT_1
							OR (bInChernobog AND vs_Seat = VS_FRONT_RIGHT)))
						OR (NOT bPlayerInOrEnteringVeh
							AND IS_PLAYER_IN_CREATOR_AIRCRAFT(localPlayer))	
							bShouldTrigger = TRUE							
							PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE - Trigger conditions met. IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2RearSeats)")
						ENDIF
					ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2DriversSeat)
						IF bPlayerInOrEnteringVeh
						AND GET_SEAT_PED_IS_IN(LocalPlayerPed,TRUE) = VS_DRIVER 
							bShouldTrigger = TRUE
							PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE - Trigger conditions met. IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2DriversSeat)")
						ENDIF
					ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_PlayerInFrontPassengersSeat)	
						//Front Passenger Seat Check
						IF bPlayerSittingInVeh					
							VEHICLE_INDEX CurrentVehicle     = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
							VEHICLE_SEAT  CurrentVehicleSeat = GET_SEAT_PED_IS_IN(LocalPlayerPed)	
							
							IF IS_ENTITY_ALIVE(CurrentVehicle)
								IF CurrentVehicleSeat = VS_FRONT_RIGHT
									PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE - Trigger conditions met. IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_PlayerInFrontPassengersSeat)")
									bShouldTrigger = TRUE
								ENDIF							
							ENDIF
						ENDIF						
					ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_TriggerWhenPlayerIsInTurretSeat)	
						//Turret Seat Check
						IF bPlayerSittingInVeh						
							VEHICLE_INDEX CurrentVehicle     = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
							VEHICLE_SEAT  CurrentVehicleSeat = GET_SEAT_PED_IS_IN(LocalPlayerPed)	
							
							IF IS_ENTITY_ALIVE(CurrentVehicle)
								IF IS_TURRET_SEAT(CurrentVehicle, CurrentVehicleSeat)
									PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE - Trigger conditions met. IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_TriggerWhenPlayerIsInTurretSeat)")
									bShouldTrigger = TRUE
								ENDIF							
							ENDIF
						ENDIF
					ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_DriversSeatEmpty)
						//Driver seat free check
						IF bPlayerSittingInVeh
							VEHICLE_INDEX CurrentVehicle = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)							
							IF IS_ENTITY_ALIVE(CurrentVehicle)
								IF IS_VEHICLE_SEAT_FREE(CurrentVehicle, VS_DRIVER, TRUE)
									PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE - Trigger conditions met. IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_DriversSeatEmpty)")
									bShouldTrigger = TRUE
								ENDIF							
							ENDIF
						ENDIF
					ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_FrontPassengersSeatEmpty)				
						//Front Passenger seat free check
						IF bPlayerSittingInVeh
							VEHICLE_INDEX CurrentVehicle = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)													
							IF IS_ENTITY_ALIVE(CurrentVehicle)
								IF IS_VEHICLE_SEAT_FREE(CurrentVehicle, VS_FRONT_RIGHT, TRUE)
									
									PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE - Trigger conditions met. IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_FrontPassengersSeatEmpty)")
									bShouldTrigger = TRUE
								ENDIF							
							ENDIF
						ENDIF
					ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset8, iBS_Dialogue8_VehicleIsFull)	
						//Vehicle is full and the player is inside
						IF bPlayerSittingInVeh
							VEHICLE_INDEX CurrentVehicle = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
							IF  DOES_ENTITY_EXIST(CurrentVehicle) 
							AND IS_VEHICLE_FULL(CurrentVehicle, TRUE)											
								PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE - Trigger conditions met. IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset8, iBS_Dialogue8_VehicleIsFull)")
								bShouldTrigger = TRUE
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE - CHECK INSIDE")
						IF bPlayerSittingInVeh
							bShouldTrigger = TRUE							
							PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE - Trigger conditions met. IS_PED_IN_ANY_VEHICLE()")
						ENDIF
					ENDIF
				ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialoguePlayerOutsideVehicle)
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE - CHECK OUTSIDE")
					IF NOT IS_PED_IN_ANY_VEHICLE(tempPed)
						bShouldTrigger = TRUE
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE - Trigger conditions met. HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE() - NOT IS_PED_IN_ANY_VEHICLE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN bShouldTrigger
ENDFUNC

FUNC STRING GET_DEBUG_NAME_OF_DAMAGER_TYPE(ENTITY_INDEX entToCheck)
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(entToCheck)
		RETURN "a vehicle"
	ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(entToCheck)
		RETURN "a ped"
	ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_OBJECT(entToCheck)
		RETURN "an object"
	ENDIF
	
	RETURN "UNKNOWN"
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_VEHICLE_DAMAGE(INT iDialogue)

	BOOL bShouldTrigger
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleTrigger = -1
	OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleDamage  = -1
		bShouldTrigger = TRUE
	ELSE
		INT iTriggerVehicle = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleTrigger
		
		NETWORK_INDEX netVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[iTriggerVehicle]
		VEHICLE_INDEX viVeh
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(netVeh)			
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_VEHICLE_DAMAGE - NETWORK_DOES_NETWORK_ID_EXIST(netVeh)")	
			viVeh = NET_TO_VEH(netVeh)
			
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset8, iBS_Dialogue8_UseThresholdOrEntityDamage)
				//Use Damage Threshold check
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_VEHICLE_DAMAGE - Using Damage Threshold")			
			
				FLOAT fPercentage = MC_GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(viVeh, iTriggerVehicle, GET_TOTAL_STARTING_PLAYERS())
						
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_VehDamageAboveOrBelow)
					//Above
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_VEHICLE_DAMAGE - iBS_Dialogue3_VehDamageAboveOrBelow is NOT SET (So trigger when taken too much damage) Percentage to use: ", (100 - fPercentage), " fPercentage: ", fPercentage, " g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleDamage: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleDamage)
					IF (100 - fPercentage) >= g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleDamage
					OR IS_ANY_VEHICLE_DOOR_DAMAGED(viVeh)
						bShouldTrigger = TRUE
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_VEHICLE_DAMAGE - Trigger conditions met (above threshold). bShouldTrigger = TRUE")
					ENDIF	
					
					//If we're checking for damage over 100% check if the vehicle is dead or no longer driveable - url:bugstar:6155554
					IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleDamage = 100
						IF DOES_ENTITY_EXIST(viVeh)
							IF IS_ENTITY_DEAD(viVeh)
							OR NOT IS_VEHICLE_DRIVEABLE(viVeh)
								bShouldTrigger = TRUE
								PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_VEHICLE_DAMAGE - Trigger conditions met (above threshold). bShouldTrigger = TRUE")
							ELSE //If we're checking for over 100% damage, do not trigger for just the doors being damaged. url:bugstar:6208238
								IF bShouldTrigger
								AND (100 - fPercentage) < g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleDamage
									bShouldTrigger = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ELSE
					//Below
					//PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_VEHICLE_DAMAGE - iBS_Dialogue3_VehDamageAboveOrBelow is SET (So trigger when not yet taken too much damage)")
					//PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_VEHICLE_DAMAGE - Percentage to use: ", (100 - fPercentage), " fPercentage: ", fPercentage, " g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleDamage: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleDamage)
					IF (100 - fPercentage) < g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleDamage
					AND NOT IS_ANY_VEHICLE_DOOR_DAMAGED(viVeh)
						bShouldTrigger = TRUE
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_VEHICLE_DAMAGE - Trigger conditions met (below threshold).  bShouldTrigger = TRUE")
					ENDIF
				ENDIF
			ELIF IS_ENTITY_ALIVE(viVeh)
				//Use Damaged By Another Entity check
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_VEHICLE_DAMAGE - Using Damaged By Another Entity")
				
				//Are we checking for a specific damager (currently just another vehicle) or any entity?
				IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleDamagerTrigger = -1				
					//Check if we have any damage record
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(viVeh) 
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(viVeh)
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_OBJECT(viVeh)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_VEHICLE_DAMAGE - Trigger conditions met. Veh ", iTriggerVehicle, " was damaged by another entity (", GET_DEBUG_NAME_OF_DAMAGER_TYPE(viVeh) ,").  bShouldTrigger = TRUE")
						bShouldTrigger = TRUE						
				
						//Clear the record, otherwise we will keep triggering this.
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ClearVehicleDamageRecord, iTriggerVehicle)
						
					ELSE
						bShouldTrigger = FALSE
					ENDIF
				ELSE
					//Check if we have a damage record with a specific damager
					INT iDamagerVehicle = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleDamagerTrigger	
					
					IF iDamagerVehicle = iTriggerVehicle
						ASSERTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_VEHICLE_DAMAGE - iVehicleDamagerTrigger and iVehicleTrigger were pointing to the same entity!")
						RETURN FALSE
					ENDIF
					
					NETWORK_INDEX netDamagerVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[iDamagerVehicle]
					VEHICLE_INDEX viDamagerVeh
					
					IF NETWORK_DOES_NETWORK_ID_EXIST(netDamagerVeh)			
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_VEHICLE_DAMAGE - NETWORK_DOES_NETWORK_ID_EXIST(netDamagerVeh)")	
						viDamagerVeh = NET_TO_VEH(netDamagerVeh)					
					
						IF  IS_ENTITY_ALIVE(viDamagerVeh)
						AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(viVeh, viDamagerVeh, FALSE)
							PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_VEHICLE_DAMAGE - Trigger conditions met. Veh ", iTriggerVehicle, " was damaged by Veh ", iDamagerVehicle, ". bShouldTrigger = TRUE")
							bShouldTrigger = TRUE
						ELSE
							bShouldTrigger = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

	ENDIF

	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL HAS_PED_BEEN_DAMAGED_PAST_THRESHOLD(INT iDialogue)

	BOOL bShouldTrigger
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedTrigger = -1
	OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedDamage = -1
		bShouldTrigger = TRUE
	ELSE
		INT iTriggerPed = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedTrigger
		
		NETWORK_INDEX netPed = MC_serverBD_1.sFMMC_SBD.niPed[iTriggerPed]
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(netPed)
			
			PED_INDEX piPed = NET_TO_PED(netPed)
			
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PED_BEEN_DAMAGED_PAST_THRESHOLD - NETWORK_DOES_NETWORK_ID_EXIST(netPed)")	 
			
			FLOAT fPercentage = (TO_FLOAT(GET_ENTITY_HEALTH(piPed)) / TO_FLOAT(GET_ENTITY_MAX_HEALTH(piPed)) * 100.0)
			
			IF (100 - fPercentage) >= g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedDamage
				bShouldTrigger = TRUE
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PED_BEEN_DAMAGED_PAST_THRESHOLD - Trigger conditions met. DAMAGED - bShouldTrigger = TRUE")				
			ENDIF
		ENDIF

	ENDIF

	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_REPAIR_DIALOGUE_BE_TRIGGERED(INT iDialogue)

	BOOL bShouldTrigger = TRUE

	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_DamagedVehicleNowRepaired)
	
		IF IS_CURRENT_OBJECTIVE_REPAIR_VEHICLE()
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_VEHICLE_REPAIR_DIALOGUE_BE_TRIGGERED -Current objective is repair car according to LBOOL31_IS_CURRENT_OBJECTIVE_REPAIR_CAR, setting LBOOL30_OBJECTIVE_HAS_BEEN_REPAIR_CAR")
			SET_BIT(iLocalBoolCheck30, LBOOL30_OBJECTIVE_HAS_BEEN_REPAIR_CAR)
			bShouldTrigger = FALSE
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck30, LBOOL30_OBJECTIVE_HAS_BEEN_REPAIR_CAR)
				IF NOT IS_PLAYER_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD)
					CLEAR_BIT(iLocalBoolCheck30, LBOOL30_OBJECTIVE_HAS_BEEN_REPAIR_CAR)
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_VEHICLE_REPAIR_DIALOGUE_BE_TRIGGERED - Just left repair car objective. Clearing LBOOL31_IS_CURRENT_OBJECTIVE_REPAIR_CAR. Returning SHOULD_VEHICLE_REPAIR_DIALOGUE_BE_TRIGGERED = TRUE")
					bShouldTrigger = TRUE
				ENDIF
			ELSE
				bShouldTrigger = FALSE
			ENDIF
		ENDIF	
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL IS_PLAYER_WEARING_MASK_TO_TRIGGER_DIALOGUE(INT iDialogue)

	BOOL bShouldTrigger
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2,iBS_Dialogue2PlayOnlyWhenInMask)
		bShouldTrigger = TRUE
	ELSE
		IF IS_MP_HEIST_MASK_EQUIPPED(LocalPlayerPed, INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, FMMC_GET_LOCAL_DEFAULT_MASK(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam)))
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_WEARING_MASK_TO_TRIGGER_DIALOGUE - Trigger conditions met.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF

	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL IS_ENOUGH_GANG_CHASE_UNITS_DEAD_TO_TRIGGER_DIALOGUE(INT iDialogue)

	BOOL bShouldTrigger
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iGangChasePedsToKill = -1
		bShouldTrigger = TRUE
	ELSE
		IF MC_serverBD_4.iGangChasePedsKilledThisRule >= g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iGangChasePedsToKill
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_ENOUGH_GANG_CHASE_UNITS_DEAD_TO_TRIGGER_DIALOGUE - Trigger conditions met.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF

	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL IS_PLAYER_FLYING_TOO_HIGH_TO_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_FlyTooHigh)
		bShouldTrigger = TRUE
	ELIF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_FLYING_TOO_HIGH)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_FLYING_TOO_HIGH_TO_TRIGGER_DIALOGUE - Trigger conditions met.")
		bShouldTrigger = TRUE
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL IS_PLAYER_SPAWNING_GANG_CHASE_DUE_TO_FLYING_TOO_HIGH(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_AltitudeGCTriggered)
		bShouldTrigger = TRUE
	ELIF IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_ALLOW_GANGCHASE_AFTER_SPEED_FAIL)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_SPAWNING_GANG_CHASE_DUE_TO_FLYING_TOO_HIGH - Trigger conditions met.")
		bShouldTrigger = TRUE
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL IS_PLAYER_VEH_UNDERWATER_TO_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_SubVehicleUnderwater)
		bShouldTrigger = TRUE
		
	ELIF IS_LOCAL_PED_DRIVING_VEHICLE_UNDERWATER(IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_SubVehicleMode)) //url:bugstar:4187127
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_VEH_UNDERWATER_TO_TRIGGER_DIALOGUE - Trigger conditions met.")
		bShouldTrigger = TRUE
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL IS_PLAYER_VEH_BACK_OUT_OF_WATER_TO_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_SubVehicleOutofWater)
		bShouldTrigger = TRUE
	ELSE
		IF IS_LOCAL_PED_DRIVING_VEHICLE_UNDERWATER()
			SET_BIT(iLocalBoolCheck26, LBOOL26_BEEN_UNDERWATER)
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_BEEN_UNDERWATER)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_VEH_UNDERWATER_TO_TRIGGER_DIALOGUE - Trigger conditions met.")
				bShouldTrigger = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC	

FUNC BOOL HAS_PLAYER_ACTIVATED_THERMAL_VISION(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_ThermalVisionActivated)
		bShouldTrigger = TRUE
	ELSE
		IF GET_PLAYER_VISUAL_AID_MODE() = VISUALAID_THERMAL
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PLAYER_ACTIVATED_THERMAL_VISION - Trigger conditions met.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC	

FUNC BOOL HAS_NOT_REACHED_MIDPOINT_YET_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_NotOnMidPointYet)
		bShouldTrigger = TRUE
	ELSE
		IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iteam] > -1 AND MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iteam] < FMMC_MAX_RULES
			IF NOT HAS_NET_TIMER_STARTED(tdDialogueTimer[iDialogueProgress])
			OR ((GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdDialogueTimer[iDialogueProgress]) > (g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTimeDelay*1000)) AND NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_PlayerBD[iPartToUse].iteam], MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iteam]))
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_NOT_REACHED_MIDPOINT_YET_TRIGGER_DIALOGUE - Trigger conditions met.")
				bShouldTrigger = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL HAS_PLAYERS_NOT_IN_SEPARATE_OBJECTIVE_VEHICLES_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_NotInSameUniqueVeh)
		bShouldTrigger = TRUE
	ELSE
		IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iteam] > -1 AND MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iteam] < FMMC_MAX_RULES
			IF MC_serverBD.iTeamUniqueVehHeldNum[MC_PlayerBD[iPartToUse].iteam] > 1
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PLAYERS_IN_SEPARATE_OBJECTIVE_VEHICLES_TRIGGER_DIALOGUE - Trigger conditions met.")
				bShouldTrigger = TRUE
			ENDIF
		ENDIF		
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC	

FUNC BOOL NOT_BLOCKED_DUE_TO_BEING_ANY_KIND_OF_SPECTATOR(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_NotForAnyKindOfSpectators)
	AND bIsAnySpectator
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - Not showing dialogue because I'm a spectator and iBS_Dialogue3_NotForSpectators is set")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL NOT_BLOCKED_DUE_TO_BEING_SPECTATOR(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_NotForSpectators)
	AND bIsAnySpectator
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - Not showing dialogue because I'm a spectator and iBS_Dialogue3_NotForSpectators is set")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL NOT_BLOCKED_DUE_TO_NOT_CARRYING_OBJ_VEH_WITH_CARGOBOB(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_MustBeCarryingObjVehicle)
		IF iObjectiveVehicleAttachedToCargobobBS != 0
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - NOT_BLOCKED_DUE_TO_NOT_CARRYING_OBJ_VEH_WITH_CARGOBOB - Allowing dialogue because I AM carrying an objective vehicle with my cargobob")
			RETURN TRUE
		ELSE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - NOT_BLOCKED_DUE_TO_NOT_CARRYING_OBJ_VEH_WITH_CARGOBOB - Not showing dialogue because I'm not carrying an objective vehicle with my cargobob")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_SPECIFIC_VEHICLE_OPTION_INSIDE_VEHICLE_VALID(INT iDialogue)
	BOOL bReturn = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_VehSpec_MustBeInsideVehType)
		bReturn = FALSE
		MODEL_NAMES vehModel = INT_TO_ENUM(MODEL_NAMES, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleModel)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_SPECIFIC_VEHICLE_OPTION_INSIDE_VEHICLE_VALID - iVehicleModel: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleModel, " Model: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(vehModel))
		
		IF IS_MODEL_A_VEHICLE(vehModel)
			IF bPedToUseOk
				VEHICLE_INDEX vehIndex
				IF IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
					vehIndex = GET_VEHICLE_PED_IS_IN(PlayerPedToUse)
					IF GET_ENTITY_MODEL(vehIndex) = vehModel
						bReturn = TRUE
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_SPECIFIC_VEHICLE_OPTION_INSIDE_VEHICLE_VALID - Returning TRUE. Player is inside Vehicle Model: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehIndex)))
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_SPECIFIC_VEHICLE_OPTION_INSIDE_VEHICLE_VALID - Vehicle Model not valid. Allowing this trigger to return true.")
		ENDIF
	ENDIF
	
	RETURN bReturn	
ENDFUNC

FUNC BOOL IS_SPECIFIC_VEHICLE_OPTION_OWNS_VEHICLE_VALID(INT iDialogue)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_VehSpec_MustOwnVehType)
		RETURN TRUE
	ENDIF
		
	IF IS_BIT_SET(iDialogueVehicleOwnedBS[iDialogueProgress/32],iDialogue%32)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_SPECIFIC_VEHICLE_OPTION_OWNS_VEHICLE_VALID - Returning TRUE. Vehicle ownership checked earlier")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iDialogueVehicleNotOwnedBS[iDialogueProgress/32],iDialogue%32)
		RETURN FALSE
	ENDIF
		
	MODEL_NAMES vehModel = INT_TO_ENUM(MODEL_NAMES, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleModel)
	IF IS_MODEL_A_VEHICLE(vehModel)
		IF DOES_PLAYER_OWN_THIS_PEGASUS_VEHICLE(vehModel)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_SPECIFIC_VEHICLE_OPTION_OWNS_VEHICLE_VALID - Returning TRUE. Player owns this Pegasus vehicle model.")
			SET_BIT(iDialogueVehicleOwnedBS[iDialogueProgress/32],iDialogue%32)	
			RETURN TRUE
		ENDIF
		IF DOES_PLAYER_OWN_ANY_SAVED_VEHICLES_OF_MODEL(vehModel)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_SPECIFIC_VEHICLE_OPTION_OWNS_VEHICLE_VALID - Returning TRUE. Player owns this vehicle model.")
			SET_BIT(iDialogueVehicleOwnedBS[iDialogueProgress/32],iDialogue%32)	
			RETURN TRUE
		ENDIF	
		
		SET_BIT(iDialogueVehicleNotOwnedBS[iDialogueProgress/32],iDialogue%32)	
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_SPECIFIC_VEHICLE_OPTION_OWNS_VEHICLE_VALID - Returning FALSE. Player does not own this vehicle model.")
	ELSE
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_SPECIFIC_VEHICLE_OPTION_OWNS_VEHICLE_VALID = Not Valid Vehicle Model not valid")
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PLAYER_IN_VEHICLE_OF_PED_VOICE(INT iDialogue)
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_VehSpec_InSameVehAsPedVoice)
		INT iPedVoice = -1
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedVoice > -1
			iPedVoice = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedVoice
		ENDIF
		IF iPedVoice > -1		
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPedVoice])
			AND g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSecondPedVoice > -1
				iPedVoice = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSecondPedVoice
			ENDIF
		ELSE
			iPedVoice = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSecondPedVoice
		ENDIF
		IF iPedVoice > -1
			bShouldTrigger = FALSE
			IF bPedToUseOk
			AND IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPedVoice])
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_IN_VEHICLE_OF_PED_VOICE - Checking iPed: ", iPedVoice, " Is alive and in a vehicle.")
					
					PED_INDEX pedIndex = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPedVoice])
					IF NOT IS_PED_INJURED(pedIndex)
					AND IS_PED_IN_ANY_VEHICLE(pedIndex)
						VEHICLE_INDEX vehPedIsIn = GET_VEHICLE_PED_IS_IN(pedIndex)
						VEHICLE_INDEX vehPlayerIsIn = GET_VEHICLE_PED_IS_IN(PlayerPedToUse)
						
						IF vehPedIsIn = VehPlayerIsIn
							PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_IN_VEHICLE_OF_PED_VOICE - bShouldTrigger = TRUE")
							bShouldTrigger = TRUE
						ELSE
							PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_IN_VEHICLE_OF_PED_VOICE - Player is not in same vehicle as ped for dialogue.")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger	
ENDFUNC

FUNC BOOL HAS_PED_DIED_TO_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedKilled = -1
		bShouldTrigger = TRUE
	ELSE
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iPedKilledBS, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedKilled)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PED_DIED_TO_TRIGGER_DIALOGUE - Trigger conditions met.")
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PED_DIED_TO_TRIGGER_DIALOGUE - dead ped: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedKilled)			
			bShouldTrigger = TRUE
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC	

FUNC BOOL HAS_PLAYER_GOT_CLOSE_ENOUGH_TO_WATER_TO_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWaterDistance = 0
		bShouldTrigger = TRUE
	ELSE
		FLOAT fWaterHeight
		VECTOR vFwd = GET_ENTITY_FORWARD_VECTOR(PlayerPedToUse)
		VECTOR vPlayerPos = GET_ENTITY_COORDS(PlayerPedToUse)
		
		//Check Forwards
		VECTOR vPoint = <<vFwd.x * g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWaterDistance , vFwd.y * g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWaterDistance , vFwd.z>>
		vPoint = vPlayerPos + vPoint
		vPoint.z = vPlayerPos.z + 10
		IF GET_WATER_HEIGHT(vPoint, fWaterHeight)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - water found infront of player, range: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWaterDistance)			
			bShouldTrigger = TRUE
		ENDIF
		IF NOT bShouldTrigger
			vPoint = <<vFwd.y * g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWaterDistance , -vFwd.x * g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWaterDistance , vFwd.z>>
			vPoint = vPlayerPos + vPoint
			vPoint.z = vPlayerPos.z + 10
			IF GET_WATER_HEIGHT(vPoint, fWaterHeight)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - water found to the right of player, range: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWaterDistance)			
				bShouldTrigger = TRUE
			ENDIF
			IF NOT bShouldTrigger
				vPoint = <<-vFwd.y * g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWaterDistance , vFwd.x * g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWaterDistance , vFwd.z>>
				vPoint = vPlayerPos + vPoint
				vPoint.z = vPlayerPos.z + 10
				IF GET_WATER_HEIGHT(vPoint, fWaterHeight)
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - water found to the left of player, range: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWaterDistance)			
					bShouldTrigger = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL HAS_PLAYER_HIT_FIREWALL_TO_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_FirewallHit)
		bShouldTrigger = TRUE
	ELSE
		IF MC_playerBD[iPartToUse].iObjHacking != -1
		AND IS_BIT_SET(iLocalBoolCheck26, LBOOL26_STARTED_BEAM_HACK)
			INT iHackingStruct = GET_HACKING_STRUCT_INDEX_TO_USE(MC_playerBD[iPartToUse].iObjHacking, ciENTITY_TYPE_OBJECT)
			IF iHackingStruct > -1
			AND IS_BIT_SET(sBeamhack[iHackingStruct].iBS, ciGOHACKBS_FIREWALL_HIT)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PLAYER_HIT_FIREWALL_TO_TRIGGER_DIALOGUE - Trigger conditions met.")
				bShouldTrigger = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC	

FUNC BOOL HAS_PLAYER_HIT_PACKET_TO_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_PacketHit)
		bShouldTrigger = TRUE
	ELSE
		IF MC_playerBD[iPartToUse].iObjHacking != -1
		AND IS_BIT_SET(iLocalBoolCheck26, LBOOL26_STARTED_BEAM_HACK)
			INT iHackingStruct = GET_HACKING_STRUCT_INDEX_TO_USE(MC_playerBD[iPartToUse].iObjHacking, ciENTITY_TYPE_OBJECT)
			IF iHackingStruct > -1
			AND IS_BIT_SET(sBeamhack[iHackingStruct].iBS, ciGOHACKBS_PACKET_HIT)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PLAYER_HIT_PACKET_TO_TRIGGER_DIALOGUE - Trigger conditions met.")
				bShouldTrigger = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC	

FUNC BOOL HAS_PLAYER_STARTED_BEAMHACK_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_BeamHackStart)
		bShouldTrigger = TRUE
	ELSE
		IF MC_playerBD[iPartToUse].iObjHacking != -1
		AND IS_BIT_SET(iLocalBoolCheck26, LBOOL26_STARTED_BEAM_HACK)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_PLAYER_STARTED_BEAMHACK_TRIGGER_DIALOGUE - Trigger conditions met.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC	

FUNC BOOL HAVE_ENOUGH_FIRES_BEEN_EXTINGUISHED(INT iDialogue)
	BOOL bShouldTrigger
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iFiresExtinguished = -1
		bShouldTrigger = TRUE
	ELSE
		//[fmmc2020] - iNumberOfFiresExtinguished was removed during a fmmc2020 refactor.
		/*INT iTeamToCheck = MC_playerBD[iPartToUse].iTeam		
		IF MC_serverBD_4.iNumberOfFiresExtinguished[iTeamToCheck] >= g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iFiresExtinguished
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM (", iDialogue, ") - HAVE_ENOUGH_FIRES_BEEN_EXTINGUISHED - Trigger conditions met.")
			bShouldTrigger = TRUE
		ELSE
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM (", iDialogue, ") - HAVE_ENOUGH_FIRES_BEEN_EXTINGUISHED - Team ", iTeamToCheck, " has only put out ", MC_serverBD_4.iNumberOfFiresExtinguished[iTeamToCheck], " fires out of the required ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iFiresExtinguished)
		ENDIF*/
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL HAS_MULTIRULE_TIMER_REACHED_TRIGGER_TIME(INT iDialogue)
	BOOL bShouldTrigger
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iMultiruleTimeToReach = -1
		bShouldTrigger = TRUE
	ELSE
		IF IS_LOCAL_PLAYER_MULTIRULE_TIMER_RUNNING()
			IF GET_LOCAL_PLAYER_MULTIRULE_TIMER_TIME_REMAINING() <= g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iMultiruleTimeToReach
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_MULTIRULE_TIMER_REACHED_TRIGGER_TIME - Trigger conditions met. iMultiruleTimeToReach: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iMultiruleTimeToReach)
				bShouldTrigger = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL HAS_ANY_PREREQUISITE_DIALOGUE_PLAYED(INT iDialogue)

	BOOL bShouldTrigger
	INT iRequiredDialogue = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPrerequisite
	
	IF iRequiredDialogue = -1
		bShouldTrigger = TRUE
	ELSE		
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iDialoguePlayedBS[iRequiredDialogue/32],iRequiredDialogue%32)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_ANY_PREREQUISITE_DIALOGUE_PLAYED - Trigger conditions met for trigger. Prereq = ", iRequiredDialogue)
			bShouldTrigger = TRUE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_OnlyStartDelayAfterPreqreqFinished)
			IF IS_BIT_SET(iLocalDialoguePlayingBS[iRequiredDialogue/32], iRequiredDialogue%32)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - HAS_ANY_PREREQUISITE_DIALOGUE_PLAYED - Trigger conditions for trigger. not yet met because Prereq = ", iRequiredDialogue, " is still playing iBS_Dialogue4_OnlyStartDelayAfterPreqreqFinished and is set.")	
				bShouldTrigger = FALSE
			ENDIF
		ENDIF
	ENDIF

	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL IS_PLAYER_IN_FIRST_PERSON(INT iDialogue)

	BOOL bShouldTrigger
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2,iBS_Dialogue2PlayOnlyInFirstPerson)
		bShouldTrigger = TRUE
	ELSE
		IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_IN_FIRST_PERSON - Trigger conditions met.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF

	RETURN bShouldTrigger

ENDFUNC

FUNC BOOL IS_DIALOGUE_TRIGGER_BLOCKED_FROM_LOCAL_PLAYER_SUBMERGED(INT iDialogue)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_BlockIfCompletelySubmerged)
		IF NOT IS_PED_INJURED(localPlayerPed)
			ENTITY_INDEX eIndex 
			IF IS_PED_IN_ANY_VEHICLE(localPlayerPed, TRUE)
				eIndex = GET_VEHICLE_PED_IS_IN(localPlayerPed, TRUE)
			ELSE
				eIndex = LocalPlayerPed
			ENDIF
			
			IF DOES_ENTITY_EXIST(eIndex)
				IF IS_ENTITY_IN_WATER(eIndex)
				AND GET_ENTITY_SUBMERGED_LEVEL(eIndex) >= 1.0
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_DIALOGUE_TRIGGER_BLOCKED_FROM_LOCAL_PLAYER_SUBMERGED - LocalPlayerPed is in the water. Blocking Dialogue.")
					RETURN TRUE		
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DIALOGUE_TRIGGER_BLOCKED_FROM_WAIT_FOR_PLAYER_TO_HAVE_BEEN_IN_WATER(INT iDialogue)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_BlockUntilBeenSubmergedInWaterThisRule)
		IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_LOCAL_PLAYER_BEEN_PROPERLY_SUBMERGED_THIS_RULE)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_PLAYER_TO_HAVE_BEEN_IN_WATER - LocalPlayerPed Has not yet entered the water. Blocking Dialogue")
			RETURN TRUE
		ENDIF	
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_PREREQ_COUNTER(INT iDialogue)
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueRequiredPreReqCounterCount > 0
		INT iPreReqCounterIndex = GET_REGISTERED_PREREQ_COUNTER_FOR_DIALOGUE_TRIGGER(iDialogue)
		
		IF iPreReqCounterIndex = -1
			REGISTER_PREREQ_COUNTER_FOR_DIALOGUE_TRIGGER(iDialogue)
		ELSE
			IF IS_PREREQ_COUNTER_COMPLETED(iPreReqCounterIndex, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueRequiredPreReqCounterCount)
				// A high enough count of the specified PreReqs have been completed!
				RETURN TRUE
			ENDIF
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_CRASHING_INTO_ENEMY_VEHICLE(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_TriggerOnCrashIntoEnemyVehicle)
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequireCrashIntoSpecificVehicle > -1
			IF DID_A_PLAYER_JUST_CRASH_INTO_THIS_VEHICLE(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequireCrashIntoSpecificVehicle)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_CRASHING_INTO_ENEMY_VEHICLE - Return TRUE - A player just crashed into specified Vehicle ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequireCrashIntoSpecificVehicle)
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ELSE
			IF DID_A_PLAYER_JUST_CRASH_INTO_AN_ENEMY_VEHICLE()
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_CRASHING_INTO_ENEMY_VEHICLE - Return TRUE - A player just crashed into an enemy vehicle")
				RETURN TRUE
			ENDIF
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_LANDING_GEAR_UP(INT iDialogue)

	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset8, iBS_Dialogue8_LandingGearUp)
		
		IF IS_PED_INJURED(LocalPlayerPed)
		OR NOT IS_PLAYER_IN_A_FLYING_VEHICLE()
			RETURN FALSE
		ENDIF
		
		VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		IF IS_ENTITY_DEAD(viPlayerVeh)
			RETURN FALSE
		ENDIF
		
		LANDING_GEAR_STATE eLandingGearState = GET_LANDING_GEAR_STATE(viPlayerVeh)
		IF eLandingGearState = LGS_DEPLOYING
		OR eLandingGearState = LGS_LOCKED_DOWN
		OR eLandingGearState = LGS_BROKEN
			RETURN FALSE
		ENDIF
		
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_PLAYER_OWNED_PROPERTY(INT iDialogue)
	//Is the option disabled?
	IF  g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPropertyTrigger < 0
		RETURN TRUE
	ENDIF	
	/* 
	//Useful debug
	#IF IS_DEBUG_BUILD
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_PLAYER_OWNED_PROPERTY - g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPropertyTrigger = ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPropertyTrigger)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_PLAYER_OWNED_PROPERTY - Fixer Property = ", ENUM_TO_INT(GET_PLAYERS_OWNED_FIXER_HQ(GB_GET_LOCAL_PLAYER_GANG_BOSS())))
	#ENDIF
	*/	
	
	//FIXER (0 - 3):
	//FIXER_HQ_HAWICK
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPropertyTrigger = 0 AND GET_PLAYERS_OWNED_FIXER_HQ(GB_GET_LOCAL_PLAYER_GANG_BOSS()) = FIXER_HQ_HAWICK
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_PLAYER_OWNED_PROPERTY - Return TRUE - Player owns FIXER_HQ_HAWICK")
		RETURN TRUE
	ENDIF		
	//FIXER_HQ_ROCKFORD
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPropertyTrigger = 1 AND GET_PLAYERS_OWNED_FIXER_HQ(GB_GET_LOCAL_PLAYER_GANG_BOSS()) = FIXER_HQ_ROCKFORD
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_PLAYER_OWNED_PROPERTY - Return TRUE - Player owns FIXER_HQ_ROCKFORD")
		RETURN TRUE
	ENDIF
	//FIXER_HQ_SEOUL
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPropertyTrigger = 2 AND GET_PLAYERS_OWNED_FIXER_HQ(GB_GET_LOCAL_PLAYER_GANG_BOSS()) = FIXER_HQ_SEOUL
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_PLAYER_OWNED_PROPERTY - Return TRUE - Player owns FIXER_HQ_SEOUL")
		RETURN TRUE
	ENDIF
	//FIXER_HQ_VESPUCCI
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPropertyTrigger = 3 AND GET_PLAYERS_OWNED_FIXER_HQ(GB_GET_LOCAL_PLAYER_GANG_BOSS()) = FIXER_HQ_VESPUCCI
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_PLAYER_OWNED_PROPERTY - Return TRUE - Player owns FIXER_HQ_VESPUCCI")
		RETURN TRUE
	ENDIF
	//END OF FIXER
	
	PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_PLAYER_OWNED_PROPERTY - Return FALSE - Player doesn't own the property ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPropertyTrigger)
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_CACHED_VEHICLE(INT iDialogue)
	//Distance Check
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].fDistanceFromCachedVeh > -1.0
		IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_CACHE_VEHICLE_COMPLETED)
		AND NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle)
			ENTITY_INDEX eiCachedVeh = NET_TO_ENT(MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle)
			
			IF NOT IS_ENTITY_ALIVE(eiCachedVeh) OR NOT IS_ENTITY_ALIVE(LocalPlayerPed)
				RETURN FALSE
			ENDIF
			
			IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, eiCachedVeh) > g_FMMC_STRUCT.sDialogueTriggers[iDialogue].fDistanceFromCachedVeh
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_CACHED_VEHICLE - Return TRUE - Distance between the player and their cached vehicle is greater than ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].fDistanceFromCachedVeh)
				RETURN TRUE
			ENDIF
		ENDIF
		
		RETURN FALSE
	ENDIF


	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_BASED_ON_CONTROLS(INT iDialogue)
	//Controls check
	IF (IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset8, iBS_Dialogue8_DoNotTriggerWithKeyboardAndMouse) AND IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL))
	OR (IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset8, iBS_Dialogue8_DoNotTriggerWithController) AND NOT IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL))
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_CURRENT_CUTSCENE_CAMERA_SHOT(INT iDialogue)
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCamShotTrigger = -1
		RETURN TRUE
	ENDIF
	
	IF iCamShot = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCamShotTrigger
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_CURRENT_CUTSCENE_CAMERA_SHOT - Return TRUE - Expecting Cam Shot: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCamShotTrigger, " we're on shot: ", iCamShot)
		RETURN TRUE
	ENDIF

	PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_CURRENT_CUTSCENE_CAMERA_SHOT - Return FALSE - Expecting Cam Shot: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCamShotTrigger, " we're on shot: ", iCamShot)

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_BE_BLOCKED_DUE_TO_A_CAMERA_LOCK(INT iDialogue) 
	//Locked To First & Third Person Mode
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset8, iBS_Dialogue8_BlockOnFirstPersonCameraLock) AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset8, iBS_Dialogue8_BlockOnThirdPersonCameraLock)
		IF IS_CAMERA_LOCKED(g_FMMC_STRUCT.iFixedCamera)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_BE_BLOCKED_DUE_TO_A_CAMERA_LOCK- IS_CAMERA_LOCKED - Blocking DT due to a camera lock!")
			RETURN FALSE
		ENDIF
	//Locked To First Person Mode
	ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset8, iBS_Dialogue8_BlockOnFirstPersonCameraLock) AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset8, iBS_Dialogue8_BlockOnThirdPersonCameraLock)
		IF IS_CAMERA_LOCKED_TO_FIRST(g_FMMC_STRUCT.iFixedCamera)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_BE_BLOCKED_DUE_TO_A_CAMERA_LOCK- IS_CAMERA_LOCKED_TO_FIRST - Blocking DT due to a camera lock!")
			RETURN FALSE
		ENDIF
	//Locked To Third Person Mode
	ELIF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset8, iBS_Dialogue8_BlockOnFirstPersonCameraLock) AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset8, iBS_Dialogue8_BlockOnThirdPersonCameraLock)
		IF IS_CAMERA_LOCKED_TO_THIRD(g_FMMC_STRUCT.iFixedCamera)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_BE_BLOCKED_DUE_TO_A_CAMERA_LOCK- IS_CAMERA_LOCKED_TO_THIRD - Blocking DT due to a camera lock!")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_OWNED_WEAPON(INT iDialogue)
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWeaponTypeIndexTrigger != -1
		WEAPON_TYPE wtWeaponTigger = GET_WEAPONTYPE_FROM_CREATOR_INDEX(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWeaponTypeIndexTrigger)
		IF HAS_PED_GOT_WEAPON(localPlayerPed, wtWeaponTigger)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_OWNED_WEAPON - Return TRUE - Player has ", GET_WEAPON_TYPE_NAME_FOR_DEBUG(wtWeaponTigger), ". g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWeaponTypeIndexTrigger = ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWeaponTypeIndexTrigger)
			RETURN TRUE
		ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_INTERACT_WITH_OBJ(INT iDialogue)
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iInteractWithObj > -1
		
		INT iObj = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iInteractWithObj
		
		IF MC_serverBD.iObjHackPart[iObj] > -1
		AND MC_playerBD_1[MC_serverBD.iObjHackPart[iObj]].iInteractWithSyncedScene > -1
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_INTERACT_WITH_OBJ - Return TRUE - Part ", MC_serverBD.iObjHackPart[iObj], " started interaction. iInteractWithSyncedScene = ", MC_playerBD_1[MC_serverBD.iObjHackPart[iObj]].iInteractWithSyncedScene, ", iInteractWithObj = ", iObj)
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SHOULD_PLAYER_COUGH_ON_THIS_DIALOGUE_LINE(INT iDialogue)
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCoughPlayerID = -1
		EXIT
	ENDIF	
	
	INT iPart = GET_ORDERED_PARTICIPANT_NUMBER(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCoughPlayerID)
	PED_INDEX targetPed
	PLAYER_INDEX tempPlayer		
	PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)
	
	IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)							
		tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
					
		IF tempPlayer != LocalPlayer
			PRINTLN("SHOULD_PLAYER_COUGH_ON_THIS_DIALOGUE_LINE - Not a local player!")
			EXIT
		ENDIF
		
		IF IS_NET_PLAYER_OK(tempPlayer)
			targetPed = GET_PLAYER_PED(tempPlayer)
			
			IF IS_PED_INJURED(targetPed)				
				PRINTLN("SHOULD_PLAYER_COUGH_ON_THIS_DIALOGUE_LINE - Ped was injured")
				EXIT
			ENDIF
		ELSE
			EXIT
		ENDIF
	ELSE
		EXIT
	ENDIF
	
	//Wait for a specific line
	IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
	OR GET_CURRENT_SCRIPTED_CONVERSATION_LINE() != g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCoughDialogueLineID
		EXIT
	ENDIF
	
	PRINTLN("SHOULD_PLAYER_COUGH_ON_THIS_DIALOGUE_LINE - Start coughing! iCoughPlayerID = ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCoughPlayerID, ", iCoughDialogueLineID = ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCoughDialogueLineID)
	
	ANIM_DATA sAnimData, sAnimDataNull1, sAnimDataNull2
	sAnimData.type = APT_SINGLE_ANIM
	sAnimData.phase0 = 0.0
	sAnimData.rate0 = 1.0
	sAnimData.filter = GET_HASH_KEY("BONEMASK_UPPERONLY")
	sAnimData.dictionary0 = "anim@fidgets@coughs"
	sAnimData.flags = (AF_UPPERBODY | AF_SECONDARY | AF_ADDITIVE)
	STRING sFacialAnimName
	
	REQUEST_ANIM_DICT("anim@fidgets@coughs")
	REQUEST_ANIM_SET("anim@fidgets@coughs")
	IF NOT HAS_ANIM_DICT_LOADED("anim@fidgets@coughs")
		PRINTLN("SHOULD_PLAYER_COUGH_ON_THIS_DIALOGUE_LINE - Waiting for anims to load")
		EXIT
	ENDIF
	
	//Trigger anim
	IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_DONE_FIRST_COUGH)
		iHealthDrainCoughAnim = GET_RANDOM_INT_IN_RANGE(0, 3)
		
		SWITCH iHealthDrainCoughAnim
			CASE 0	
				sAnimData.anim0 = "COUGH_A"
				sFacialAnimName = "COUGH_A_FACIAL"
			BREAK
			CASE 1	
				sAnimData.anim0 = "COUGH_B"
				sFacialAnimName = "COUGH_B_FACIAL"
			BREAK
			CASE 2	
				sAnimData.anim0 = "COUGH_C"
				sFacialAnimName = "COUGH_C_FACIAL"
			BREAK
		ENDSWITCH
						
		TASK_SCRIPTED_ANIMATION(targetPed, sAnimData, sAnimDataNull1, sAnimDataNull2)
		PLAY_FACIAL_ANIM(targetPed, sFacialAnimName, sAnimData.dictionary0)
		SET_BIT(iLocalBoolCheck32, LBOOL32_DONE_FIRST_COUGH)
		CLEAR_BIT(iLocalBoolCheck33, LBOOL33_PLAYED_COUGH_SOUND_EFFECT)
		PRINTLN("SHOULD_PLAYER_COUGH_ON_THIS_DIALOGUE_LINE - Playing cough anim")
	ENDIF
	
	//Trigger sound
	IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_PLAYED_COUGH_SOUND_EFFECT)				
		STRING sSoundName = "Male_01"
		
		IF IS_PLAYER_PED_FEMALE(tempPlayer)
			sSoundName = "Female_01"
		ENDIF		

		PRINTLN("SHOULD_PLAYER_COUGH_ON_THIS_DIALOGUE_LINE - Playing cough sound effect: ", sSoundName)
		PLAY_SOUND_FROM_ENTITY(-1, sSoundName, targetPed, "dlc_ch_heist_finale_HEALTH_DRAIN_ZONE_coughs_sounds", TRUE, 500)
		SET_BIT(iLocalBoolCheck33, LBOOL33_PLAYED_COUGH_SOUND_EFFECT)		
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_TIME_SINCE_PLAYER_LAST_FIRED_THEIR_WEAPON(INT iDialogue)
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWeaponLastFiredTriggerTime != -1
		IF GET_TIME_IN_SECONDS_SINCE_LOCAL_PLAYER_FIRED_THEIR_WEAPON() >= g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWeaponLastFiredTriggerTime
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_TIME_SINCE_PLAYER_LAST_FIRED_THEIR_WEAPON - Return TRUE - Player hasn't fired any weapons for ", GET_TIME_IN_SECONDS_SINCE_LOCAL_PLAYER_FIRED_THEIR_WEAPON(), "s. g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWeaponLastFiredTriggerTime = ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWeaponLastFiredTriggerTime)
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_ABILITY_AVAILABILITY(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_PlayerPurchasedAnyAbility)
		INT iAbility
		FMMC_PLAYER_ABILITY_TYPES eAbilityType
		FOR iAbility = 0 TO ENUM_TO_INT(PA_MAX) - 1
			eAbilityType = INT_TO_ENUM(FMMC_PLAYER_ABILITY_TYPES, iAbility)
			IF IS_ABILITY_AVAILABLE_FOR_LOCAL_PLAYER(eAbilityType)
			AND IS_ABILITY_AVAILABLE_FOR_CURRENT_RULE(eAbilityType)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_ABILITY_AVAILABILITY - Return TRUE - At least one ability purchased.")
				RETURN TRUE
			ENDIF
		ENDFOR					
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_SUPPORT_SNIPER(INT iDialogue)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_SniperAiming)
		IF FMMC_IS_LONG_BIT_SET(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_PlaySniperAimingDialogue)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_SUPPORT_SNIPER - Return TRUE - Sniper Aiming.")
			FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_PlaySniperAimingDialogue)
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_SniperNegative)
		IF NOT HAS_SUPPORT_SNIPER_MARKED_INVALID_TARGET()
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_SUPPORT_SNIPER - Return FALSE - Sniper has valid target.")
			RETURN FALSE
		ELIF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_SniperConfirmedKill)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_SUPPORT_SNIPER - Return FALSE - Sniper has valid target.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_SniperConfirmedKill)
		IF NOT HAS_SUPPORT_SNIPER_KILLED_TARGET()
			PRINTLN("[Dialogue Triggers] SHOULD_DIALOGUE_TRIGGER_DUE_TO_SUPPORT_SNIPER - Return FALSE - Sniper has not confirmed kill.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_SniperActive) 
		IF NOT IS_PLAYER_ABILITY_ACTIVE(PA_SUPPORT_SNIPER)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_SUPPORT_SNIPER - Return FALSE - Sniper is not active.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_SniperEndedWithNoKill) 
		IF IS_PLAYER_ABILITY_ACTIVE(PA_SUPPORT_SNIPER)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_SUPPORT_SNIPER - Return FALSE - Sniper is still active (waiting to end without making a kill).")
			RETURN FALSE
		ENDIF
		
		IF NOT HAS_SUPPORT_SNIPER_ENDED_WITHOUT_MAKING_KILL()
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_SUPPORT_SNIPER - Return FALSE - Sniper has not ended without making a kill.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_SniperHasBeenUsed)
		IF NOT ABILITY_HAS_BEEN_USED(PA_SUPPORT_SNIPER)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_SUPPORT_SNIPER - Return FALSE - Sniper has not been used.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_SniperMarkedTarget) 
		IF NOT SUPPORT_SNIPER_HAS_TARGET()
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_SUPPORT_SNIPER - Return FALSE - Sniper has no target.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_AIR_STRIKE(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_AirStrikeNotUsed)
		IF NOT ABILITY_HAS_BEEN_USED(PA_AIR_STRIKE)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_AIR_STRIKE - Return TRUE - Air Strike Not Used.")
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_AirStrikeLaunched)
		IF IS_PLAYER_ABILITY_ACTIVE(PA_AIR_STRIKE)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_AIR_STRIKE - Return TRUE - Air Strike Launched.")
			RETURN TRUE			
		ELIF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_AirStrikeBombsDropped)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_AirStrikeBombsDropped)
		IF IS_PLAYER_ABILITY_ACTIVE(PA_AIR_STRIKE)
		AND MPGlobalsAmbience.bAirstrikeExplosionsInProgress
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_AIR_STRIKE - Return TRUE - Air Strike Dropped Bombs.")
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_AirStrikeVehicleDestroyed)
		IF FMMC_IS_LONG_BIT_SET(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_PlayAirStrikeVechileDestroyedDialogue)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_AIR_STRIKE - Return TRUE - Air Strike Vehicle Destroyed.")
			FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_PlayAirStrikeVechileDestroyedDialogue)
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_HEAVY_LOADOUT(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_HeavyLoadoutUsed)
		IF IS_PLAYER_ABILITY_ACTIVE(PA_HEAVY_LOADOUT)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_HEAVY_LOADOUT - Return TRUE - Heavy Loadout Used.")
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_RECON_DRONE(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_ReconDroneUsed)
	OR IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_ReconDroneActive)
		IF IS_PLAYER_ABILITY_ACTIVE(PA_RECON_DRONE)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_RECON_DRONE - Return TRUE - Recon Drone Used/Active.")
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_ReconDroneNotActive)
		IF NOT IS_PLAYER_ABILITY_ACTIVE(PA_RECON_DRONE)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_RECON_DRONE - Return TRUE - Recon Drone has not been used.")
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_HELI_BACKUP(INT iDialogue)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_HeliBackupActive)
		IF NOT IS_PLAYER_ABILITY_ACTIVE(PA_HELI_BACKUP)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_HELI_BACKUP - Return FALSE - Heli Backup has not been used / is inactive.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_HeliBackupUsed)
		IF IS_PLAYER_ABILITY_ACTIVE(PA_HELI_BACKUP)
		OR sLocalPlayerAbilities.sAbilities[ENUM_TO_INT(PA_HELI_BACKUP)].iUseCount = 0
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_HELI_BACKUP - Return FALSE - Heli Backup has not been used / is inactive.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_HeliBackupDamaged)
		IF NOT (HEIST_ISLAND_BACKUP_HELI__GET_HELI_HEALTH() < HEIST_ISLAND_BACKUP_HELI__GET_HELI_MAX_HEALTH())
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_HELI_BACKUP - Return FALSE - Heli Backup has not taken damage.")
			RETURN FALSE
		ENDIF		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_HeliBackupDestroyed)
		IF NOT (HEIST_ISLAND_BACKUP_HELI__GET_HELI_HEALTH() <= 0)
		OR NOT ABILITY_HAS_BEEN_USED(PA_HELI_BACKUP)
		OR NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_PLAYER_ABILITY_HELI_BACKUP_HAS_BEEN_ACTIVE)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_HELI_BACKUP - Return FALSE - Heli Backup has not been destroyed.")
			RETURN FALSE
		ENDIF
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_HeliBackupKilledEnemy)
		IF (mpGlobals.sIslandBackupHeli.iBackupHeliKillCount = 0)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_HELI_BACKUP - Return FALSE - Heli Backup has not killed any enemies.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_HeliBackupNotUsed)
		IF ABILITY_HAS_BEEN_USED(PA_HELI_BACKUP)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_HELI_BACKUP - Return FALSE - Heli Backup has been used.")
			RETURN FALSE
		ENDIF
	ENDIF
	#ENDIF

	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_INVALID_ABILITY_ACTIVATION(INT iDialogue)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_AirStrikeUnavailable)
		IF FMMC_IS_LONG_BIT_SET(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_PlayAirStrikeAttemptedInvalidActivationDialogue)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_INVALID_ABILITY_ACTIVATION - Return TRUE - Invalid Air Strike Activation.")
			FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_PlayAirStrikeAttemptedInvalidActivationDialogue)
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_HeliBackupUnavailable)
		IF FMMC_IS_LONG_BIT_SET(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_PlayHeliBackupAttemptedInvalidActivationDialogue)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_INVALID_ABILITY_ACTIVATION - Return TRUE - Invalid Heli Backup Activation.")
			FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_PlayHeliBackupAttemptedInvalidActivationDialogue)
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_SupportSniperUnavailable)
		IF FMMC_IS_LONG_BIT_SET(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_PlaySupportSniperAttemptedInvalidActivationDialogue)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_INVALID_ABILITY_ACTIVATION - Return TRUE - Invalid Support Sniper Activation.")
			FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_PlaySupportSniperAttemptedInvalidActivationDialogue)
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_AbilityUnavailable)
			
		IF FMMC_IS_LONG_BIT_SET(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_PlayAttemptedInvalidActivationDialogue)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_INVALID_ABILITY_ACTIVATION - Return TRUE - Requested play on invalid activation dialogue.")
			FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_PlayAttemptedInvalidActivationDialogue)
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
		
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_ONE_PLAYER_ALIVE(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_OnePlayerAliveOnTeam)
		IF MC_serverBD.iNumberOfPlayingPlayers[GET_LOCAL_PLAYER_TEAM(TRUE)] > 1
			RETURN FALSE
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_ONE_PLAYER_ALIVE - Return TRUE - One player alive on team ",GET_LOCAL_PLAYER_TEAM(TRUE),".")
		#ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_COMPLETED_INTERACTABLE(INT iDialogue)
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequiredCompletedInteractable = -1
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bDialogueTriggerDebug
		INT iInt = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequiredCompletedInteractable
		DRAW_DEBUG_LINE(MC_GET_DIALOGUE_TRIGGER_POSITION(iDialogue), g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInt].vInteractable_Position)
	ENDIF
	#ENDIF
	
	IF IS_INTERACTABLE_COMPLETE(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequiredCompletedInteractable, DEFAULT, FALSE)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_COMPLETED_INTERACTABLE - Return TRUE - Interactable ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequiredCompletedInteractable, " is complete")
		RETURN TRUE
	ELSE
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_COMPLETED_INTERACTABLE - Return FALSE - Interactable ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequiredCompletedInteractable, " hasn't been completed yet")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_COMPLETED_INTERACTION_TYPE(INT iDialogue)
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequiredCompletedInteractionType = ciInteractableInteraction_None
		RETURN TRUE
	ENDIF
	
	IF IS_INTERACTION_TYPE_COMPLETE(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequiredCompletedInteractionType)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_COMPLETED_INTERACTION_TYPE - Return TRUE - Interaction Type ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequiredCompletedInteractionType, " has been completed at some point")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_UNCOMPLETED_INTERACTABLE(INT iDialogue)
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequiredUncompletedInteractable = -1
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bDialogueTriggerDebug
		INT iInt = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequiredUncompletedInteractable
		DRAW_DEBUG_LINE(MC_GET_DIALOGUE_TRIGGER_POSITION(iDialogue), g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInt].vInteractable_Position)
	ENDIF
	#ENDIF
	
	IF NOT IS_INTERACTABLE_COMPLETE(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequiredUncompletedInteractable, DEFAULT, FALSE)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_UNCOMPLETED_INTERACTABLE - Return TRUE - Interactable ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequiredUncompletedInteractable, " hasn't been completed")
		RETURN TRUE
	ELSE
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_UNCOMPLETED_INTERACTABLE - Return FALSE - Interactable ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequiredUncompletedInteractable, " has already been completed")
	ENDIF
	
	RETURN FALSE
ENDFUNC
   
FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_INTERACTABLE_IN_USE(INT iDialogue)
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequiredInUseInteractable = -1
		RETURN TRUE
	ENDIF
	
	//Check for this dialogue trigger
	IF NOT IS_INTERACTABLE_OCCUPIED(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequiredInUseInteractable)
		RETURN FALSE
	ENDIF
	PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_INTERACTABLE_IN_USE - Has a user")
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_CCTV_RANGE(INT iDialogue)
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].fTriggerInCCTVRange = 0.0
		RETURN TRUE
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(iCCTVInDialogueRangeBS, iDialogue)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_CCTV_RANGE - Return TRUE due to iCCTVInDialogueRangeBS")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_MISSION_EQUIPMENT(INT iDialogue)
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iMissionEquipmentRequiredType = ciCUSTOM_MISSIONEQUIPMENT_MODEL__NONE
		RETURN TRUE
	ENDIF

	IF iMissionEquipmentPickupsCollected[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iMissionEquipmentRequiredType] = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iMissionEquipmentRequiredCount
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_MISSION_EQUIPMENT - Return TRUE - iMissionEquipmentPickupsCollected[", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iMissionEquipmentRequiredType, "] = ", iMissionEquipmentPickupsCollected[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iMissionEquipmentRequiredType])
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_EXTRA_CASH_TAKE(INT iDialogue)
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequiredExtraCashTake = 0
		RETURN TRUE
	ENDIF
	
	IF iExtraTakeFromPlacedCashPickups >= g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequiredExtraCashTake
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_EXTRA_CASH_TAKE - iExtraTakeFromPlacedCashPickups: ", iExtraTakeFromPlacedCashPickups, " / g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequiredExtraCashTake: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRequiredExtraCashTake)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_DROPPING_TARGET_OBJECT_ON_DEATH(INT iDialogue)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_PlayWhenTargetObjectDroppedOnDeath)
		RETURN TRUE
	ENDIF
	
	IF iDialogueObjectiveObjectDroppedBS != 0
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "][Dialogue Triggers][Objects] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_DROPPING_TARGET_OBJECT_ON_DEATH - Return TRUE due to iDialogueObjectiveObjectDroppedBS indicating that one or more objective objects were just dropped on death")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_BOUNDS(INT iDialogue)
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTriggerOutOfBounds = ciRULE_BOUNDS_INDEX__NONE
		RETURN TRUE
	ENDIF

	IF IS_LOCAL_PLAYER_OUT_OF_BOUNDS(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTriggerOutOfBounds)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_BOUNDS - Return TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DIALOGUE_TRIGGERED_SD(INT iDialogue)

	BOOL bShouldTrigger
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2SuddenDeath)
		bShouldTrigger = TRUE
	ELSE
		IF IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_DIALOGUE_TRIGGERED_SD - Trigger conditions met.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF

	RETURN bShouldTrigger

ENDFUNC

FUNC BOOL CAN_VEHICLE_DIAGLOUE_TRIGGER_ON_ANY_RULE(INT iDialogue)
	IF IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset, iBS_DialogueRequireObjectivVehicle )
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle >= 0
		OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle2 >= 0
		OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle3 >= 0
		OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle4 >= 0
		OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle5 >= 0
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_DialogueRequireObjectivVehicleAnyRule)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - CAN_VEHICLE_DIAGLOUE_TRIGGER_ON_ANY_RULE - TRUE")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE	
ENDFUNC

//url:bugstar:2488101 - Can we have the option in creator to set dialogue that plays only when inside of a specific vehicle?
FUNC BOOL IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE(INT iDialogue) 
	//PRINTLN("[JS] - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Called")
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle >= 0
	OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle2 >= 0
	OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle3 >= 0
	OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle4 >= 0
	OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle5 >= 0
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Using specific vehicle rule. iSpecificVehicle: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle,
			"iSpecificVehicle2: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle2,
			"iSpecificVehicle3: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle3,
			"iSpecificVehicle4: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle4,
			"iSpecificVehicle5: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle5)
			
		IF IS_NET_PARTICIPANT_INT_ID_VALID(iPartToUse)
			//If bit iBS_Dialogue8_PlayerInNonrequiredVehicle is set we want to return FALSE when the player is in the specific vehicle as we only care when they are not.
			BOOL bPlayerInSpecificVehicleDisabled = NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset8, iBS_Dialogue8_PlayerInNonrequiredVehicle)
			
			#IF IS_DEBUG_BUILD
				IF NOT bPlayerInSpecificVehicleDisabled
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - iBS_Dialogue8_PlayerInNonrequiredVehicle was set to TRUE therefore iSpecificVehicle checks will always return FALSE when the player is in the specific vehicle as we only care when they are not.")
				ENDIF
			#ENDIF
			
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle >= 0
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle])
					IF IS_PED_IN_THIS_VEHICLE(PlayerPedToUse, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle]))
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)						
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player in the vehicle 1a")
						RETURN bPlayerInSpecificVehicleDisabled
					ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
					AND (GET_VEHICLE_PED_IS_ENTERING(PlayerPedToUse) = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle]) OR HAS_NET_TIMER_STARTED(tdDialogueTimer[iDialogueProgress]))
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player in the vehicle 1b")
						RETURN bPlayerInSpecificVehicleDisabled
					ENDIF
				ENDIF
			ENDIF
			
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle2 >= 0
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle2])
					IF IS_PED_IN_THIS_VEHICLE(PlayerPedToUse, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle2]))
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player in the vehicle 2a")
						RETURN bPlayerInSpecificVehicleDisabled
					ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
					AND (GET_VEHICLE_PED_IS_ENTERING(PlayerPedToUse) = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle2]) OR HAS_NET_TIMER_STARTED(tdDialogueTimer[iDialogueProgress]))
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player in the vehicle 2b")
						RETURN bPlayerInSpecificVehicleDisabled
					ENDIF
				ENDIF
			ENDIF
			
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle3 >= 0
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle3])
					IF IS_PED_IN_THIS_VEHICLE(PlayerPedToUse, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle3]))
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player in the vehicle 3a")
						RETURN bPlayerInSpecificVehicleDisabled
					ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
					AND (GET_VEHICLE_PED_IS_ENTERING(PlayerPedToUse) = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle3]) OR HAS_NET_TIMER_STARTED(tdDialogueTimer[iDialogueProgress]))
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player in the vehicle 3b")
						RETURN bPlayerInSpecificVehicleDisabled
					ENDIF
				ENDIF
			ENDIF
			
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle4 >= 0
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle4])
					IF IS_PED_IN_THIS_VEHICLE(PlayerPedToUse, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle4]))
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player in the vehicle 4a")
						RETURN bPlayerInSpecificVehicleDisabled
					ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
					AND (GET_VEHICLE_PED_IS_ENTERING(PlayerPedToUse) = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle4]) OR HAS_NET_TIMER_STARTED(tdDialogueTimer[iDialogueProgress]))
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player in the vehicle 4b")
						RETURN bPlayerInSpecificVehicleDisabled
					ENDIF
				ENDIF
			ENDIF
			
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle5 >= 0
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle5])
					IF IS_PED_IN_THIS_VEHICLE(PlayerPedToUse, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle5]))
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player in the vehicle 5a")
						RETURN bPlayerInSpecificVehicleDisabled
					ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
					AND (GET_VEHICLE_PED_IS_ENTERING(PlayerPedToUse) = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle5]) OR HAS_NET_TIMER_STARTED(tdDialogueTimer[iDialogueProgress]))
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player in the vehicle 5b")
						RETURN bPlayerInSpecificVehicleDisabled
					ENDIF
				ENDIF
			ENDIF
			
			//Is player in non-required vehicle for the dialogue?
			IF NOT bPlayerInSpecificVehicleDisabled
			AND IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
				//Triggers when the player enters any vehicle but the required one.
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player is in a non-required vehicle.")
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		//PRINTLN("[JS] - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Checking if player is in any objetive vehicle")
		IF MC_playerBD[iPartToUse].iVehCarryCount > 0 
		OR IS_BIT_SET( MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER )
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player is in a objective vehicle 6")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_BLOCKING_IF_WANTED(INT iDialogue)
	IF IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2,iBS_Dialogue2BlockIfWanted )
		IF GET_PLAYER_WANTED_LEVEL( LocalPlayer ) > 0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_ENTERING_ANY_VEHICLE_FOR_DIALOGUE(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyVehicle)
		IF bPedToUseOk			
			IF (IS_PED_TRYING_TO_ENTER_A_LOCKED_VEHICLE(PlayerPedToUse) OR HAS_NET_TIMER_STARTED(tdDialogueTimer[iDialogueProgress]))
			AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_ENTERING_ANY_VEHICLE_FOR_DIALOGUE - Trigger conditions met.")
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PED_IN_RANGE_FOR_DIALOGUE_TRIGGER - We are trying to enter any locked vehicle, playing the dialogue text.")
				RETURN TRUE
			ELIF (IS_PED_IN_ANY_VEHICLE(PlayerPedToUse) OR HAS_NET_TIMER_STARTED(tdDialogueTimer[iDialogueProgress]))
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_ENTERING_ANY_VEHICLE_FOR_DIALOGUE - Trigger conditions met.")
				RETURN TRUE
			ENDIF
		ENDIF
		
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PED_IN_RANGE_FOR_DIALOGUE_TRIGGER(INT iDialogue)
	
	INT i, iPed
	FOR i = 0 TO ciDIALOGUE_PED_IN_RANGE_MAX-1
		iPed = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedInRangeTrigger[i]
		IF iPed > -1	
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PED_IN_RANGE_FOR_DIALOGUE_TRIGGER - is connected to a Ped")
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
				PED_INDEX pedTemp = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
				IF NOT IS_PED_INJURED(pedTemp)
					IF VDIST2(GET_ENTITY_COORDS(pedTemp), MC_GET_DIALOGUE_TRIGGER_POSITION(iDialogue)) <= POW(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].fRadius, 2)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PED_IN_RANGE_FOR_DIALOGUE_TRIGGER - Trigger conditions met.")
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PED_IN_RANGE_FOR_DIALOGUE_TRIGGER - iPed: ", iPed, " is now close enough to the Dialogue Trigger.")						
					ELSE
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PED_IN_RANGE_FOR_DIALOGUE_TRIGGER - iPed: ", iPed, " is not close enough to the Dialogue Trigger.")
						RETURN FALSE
					ENDIF
				ELSE
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PED_IN_RANGE_FOR_DIALOGUE_TRIGGER - iPed: ", iPed, " was injured.")
					RETURN FALSE
				ENDIF
			ELSE
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PED_IN_RANGE_FOR_DIALOGUE_TRIGGER - iPed: ", iPed, " doesn't exist.")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR	
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_ZONES(INT iDialogue)
	
	// Dialogue Triggered by Triggering Zones
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTriggeredByZoneBS != 0
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset8, iBS_Dialogue8_ZoneCheckIsAnAND)
			IF (iCachedZoneIsTriggeredNetworkedBS & g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTriggeredByZoneBS) = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTriggeredByZoneBS
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_ZONES - (iTriggeredByZoneBS) All of the required Zones are triggering! iCachedZoneIsTriggeredNetworkedBS: ", iCachedZoneIsTriggeredNetworkedBS, " / g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTriggeredByZoneBS: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTriggeredByZoneBS)
			ELSE
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_ZONES - (iTriggeredByZoneBS) Not all of the required Zones are triggering! iCachedZoneIsTriggeredNetworkedBS: ", iCachedZoneIsTriggeredNetworkedBS, " / g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTriggeredByZoneBS: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTriggeredByZoneBS)
				RETURN FALSE
			ENDIF
		ELSE
			IF (iCachedZoneIsTriggeredNetworkedBS & g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTriggeredByZoneBS) != 0
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_ZONES - (iTriggeredByZoneBS) At least one of the specified Zones is triggering! iCachedZoneIsTriggeredNetworkedBS: ", iCachedZoneIsTriggeredNetworkedBS, " / g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTriggeredByZoneBS: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTriggeredByZoneBS)
			ELSE
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_ZONES - (iTriggeredByZoneBS) None of the specified Zones are triggering! iCachedZoneIsTriggeredNetworkedBS: ", iCachedZoneIsTriggeredNetworkedBS, " / g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTriggeredByZoneBS: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTriggeredByZoneBS)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	// Dialogue Blocked by Triggering Zones
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iBlockedByZoneBS != 0
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset8, iBS_Dialogue8_ZoneBlockerCheckIsAnAND)
			IF (iCachedZoneIsTriggeredNetworkedBS & g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iBlockedByZoneBS) = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iBlockedByZoneBS
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_ZONES - (iBlockedByZoneBS) All of the required blocker Zones are triggering! iCachedZoneIsTriggeredNetworkedBS: ", iCachedZoneIsTriggeredNetworkedBS, " / g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iBlockedByZoneBS: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iBlockedByZoneBS)
				RETURN FALSE
			ELSE
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_ZONES - (iBlockedByZoneBS) Not all of the required blocker Zones are triggering! iCachedZoneIsTriggeredNetworkedBS: ", iCachedZoneIsTriggeredNetworkedBS, " / g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iBlockedByZoneBS: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iBlockedByZoneBS)
			ENDIF
		ELSE
			IF (iCachedZoneIsTriggeredNetworkedBS & g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iBlockedByZoneBS) != 0
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_ZONES - (iBlockedByZoneBS) At least one of the specified blocker Zones is triggering! iCachedZoneIsTriggeredNetworkedBS: ", iCachedZoneIsTriggeredNetworkedBS, " / g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iBlockedByZoneBS: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iBlockedByZoneBS)
				RETURN FALSE
			ELSE
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_ZONES - (iBlockedByZoneBS) None of the specified blocker Zones are triggering! iCachedZoneIsTriggeredNetworkedBS: ", iCachedZoneIsTriggeredNetworkedBS, " / g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iBlockedByZoneBS: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iBlockedByZoneBS)
			ENDIF
		ENDIF
	ENDIF
	
	// Dialogue Triggered by Triggering a particular Zone (legacy)
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iZoneIndex > -1
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_OnZoneTriggering)
		IF bLocalPlayerPedOk
			IF IS_ZONE_TRIGGERING(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iZoneIndex)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_ZONES - LocalPlayerPed is now in the zone: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iZoneIndex, " for the Dialogue Trigger.")
				RETURN TRUE
			ELSE
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_ZONES - LocalPlayerPed is not in the zone: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iZoneIndex, " for the Dialogue Trigger.")
			ENDIF
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_EMP_DIALOGUE_BE_TRIGGERED(INT iDialogue)
	
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_TriggerWhenEMPUsed)
		IF IS_ANY_EMP_CURRENTLY_ACTIVE()
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_EMP_DIALOGUE_BE_TRIGGERED = TRUE")
			bShouldTrigger = TRUE
		ELSE
			bShouldTrigger = FALSE
		ENDIF	
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL IS_DIALOGUE_USING_DISGUISE_CHECK(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_TriggerBasedOnDisguise)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE(INT iDialogue)
	
	//If we are not using costume checks, immediately pass this bool check.
	IF NOT IS_DIALOGUE_USING_DISGUISE_CHECK(iDialogue)	
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_RequiresAllTeamToHaveDisguise)
	AND NOT ARE_ALL_PARTICIPANTS_ON_TEAM_IN_DISGUISE_OUTFIT(GET_LOCAL_PLAYER_TEAM(TRUE))
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE - FALSE - not all players on team are in the disguise")
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_WEARING_A_BUGSTAR_OUTFIT()
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_TriggerBasedOnDisguise_Bugstar)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE - Wearing bugstar disguise")			
		RETURN TRUE
	ENDIF
	
	IF IS_LOCAL_PLAYER_WEARING_A_MAINTENANCE_OUTFIT()
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_TriggerBasedOnDisguise_Mechanic)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE - Wearing  mechanic disguise")			
		RETURN TRUE
	ENDIF
	
	IF IS_LOCAL_PLAYER_WEARING_A_GRUPPE_SECHS_OUTFIT()
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_TriggerBasedOnDisguise_GruppeSechs)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE - Wearing Gruppe Sechs disguise")			
		RETURN TRUE
	ENDIF
	
	IF IS_LOCAL_PLAYER_WEARING_A_CELEB_OUTFIT()
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_TriggerBasedOnDisguise_Brucie)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE - Wearing Brucie disguise")		
		RETURN TRUE
	ENDIF
	
	IF IS_LOCAL_PLAYER_WEARING_A_FIREMAN_OUTFIT()
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_TriggerBasedOnDisguise_Fireman)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE - Wearing Fireman disguise")		
		RETURN TRUE
	ENDIF
	
	IF IS_LOCAL_PLAYER_WEARING_A_HIGH_ROLLER_OUTFIT()
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_TriggerBasedOnDisguise_HighRoller)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE - Wearing High Roller disguise")		
		RETURN TRUE
	ENDIF
	
	IF IS_LOCAL_PLAYER_WEARING_A_NOOSE_OUTFIT()
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_TriggerBasedOnDisguise_Noose)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE - Wearing Noose disguise")		
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_PLAYER_WEARING_AN_ISLAND_GUARD_OUTFIT(iLocalPart)
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_TriggerBasedOnDisguise_IslandGuard)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE - Wearing Island Guard disguise")		
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_PLAYER_WEARING_AN_ISLAND_SMUGGLER_OUTFIT(iLocalPart)
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_TriggerBasedOnDisguise_IslandSmuggler)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE - Wearing Island Smuggler disguise")		
		RETURN TRUE
	ENDIF
	
	IF NOT IS_THIS_PLAYER_WEARING_AN_ISLAND_GUARD_OUTFIT(iLocalPart)
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_TriggerBasedOnDisguise_NOTIslandGuard)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE - NOT (required) Wearing Island Guard disguise")		
		RETURN TRUE
	ENDIF
	
	IF NOT IS_THIS_PLAYER_WEARING_AN_ISLAND_SMUGGLER_OUTFIT(iLocalPart)
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_TriggerBasedOnDisguise_NOTIslandSmuggler)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE - NOT (required) Wearing Island Smuggler disguise")		
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_PLAYER_WEARING_AN_ULP_MAINTENANCE_OUTFIT(iLocalPart)
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset8, iBS_Dialogue8_TriggerBasedOnDisguise_ULPMaintenance)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE - (required) Wearing ULP Maintenance disguise")		
		RETURN TRUE	
	ENDIF
	
	IF NOT IS_THIS_PLAYER_WEARING_AN_ULP_MAINTENANCE_OUTFIT(iLocalPart)
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset8, iBS_Dialogue8_TriggerBasedOnDisguise_NOTULPMaintenance)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE - NOT (required) Wearing ULP Maintenance disguise")		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PLAYERS_AGGROED_TRIGGER_DIALOGUE(INT iDialogue)
	
	BOOL bShouldTrigger = TRUE

	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_RequiresPlayersToHaveTriggeredAggroed)
	#IF IS_DEBUG_BUILD 
	AND NOT bBlockBranchingOnAggroKeepStealthDialogue
	#ENDIF
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_PLAYERS_AGGROED_TRIGGER_DIALOGUE - Checking if players do have aggro in order to trigger dialogue.")
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam = ciDIALOGUE_TRIGGER_TEAM_ANY
			IF HAS_ANY_TEAM_TRIGGERED_AGGRO(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAggroIndexBS_Entity_Dialog)
				PRINTLN("[LM] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - SHOULD_PLAYERS_AGGROED_TRIGGER_DIALOGUE = TRUE Any Team (1)")
				bShouldTrigger = TRUE
			ELSE
				bShouldTrigger = FALSE
			ENDIF
		ELIF HAS_TEAM_TRIGGERED_AGGRO(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAggroIndexBS_Entity_Dialog)
			PRINTLN("[LM] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - SHOULD_PLAYERS_AGGROED_TRIGGER_DIALOGUE = TRUE Specific Team (2)")
			bShouldTrigger = TRUE
		ELSE
			bShouldTrigger = FALSE
		ENDIF
	ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_RequiresPlayersToNotHaveTriggeredAggroed)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_PLAYERS_AGGROED_TRIGGER_DIALOGUE - Checking if players do not have aggro in order to trigger dialogue.")		
		IF NOT HAS_TEAM_TRIGGERED_AGGRO(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAggroIndexBS_Entity_Dialog)
		#IF IS_DEBUG_BUILD 
		OR bBlockBranchingOnAggroKeepStealthDialogue
		#ENDIF
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_PLAYERS_AGGROED_TRIGGER_DIALOGUE = TRUE (2)")
			bShouldTrigger = TRUE
		ELSE
			bShouldTrigger = FALSE
		ENDIF	
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_ENTITY_EXISTING(INT iDialogue)
	
	BOOL bShouldTrigger = TRUE
	BOOL bCheckDoesNotExist = IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_EntityDoesNotExist)
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex != -1		
		SWITCH	g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsType
			CASE ciENTITY_EXISTS_DT_PEDS
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex])
					IF NOT bCheckDoesNotExist
						bShouldTrigger = FALSE
					ENDIF
				ELIF bCheckDoesNotExist
					bShouldTrigger = FALSE
				ENDIF
				
				IF bShouldTrigger
					IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_PlayWhenEntityExistPedAggroed)
					AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAggroedBitset, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex)
						bShouldTrigger = FALSE
					ENDIF
				ENDIF
			BREAK
			CASE ciENTITY_EXISTS_DT_VEHICLES
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex])
					IF NOT bCheckDoesNotExist
						bShouldTrigger = FALSE
					ENDIF
				ELIF bCheckDoesNotExist
					bShouldTrigger = FALSE
				ENDIF
			BREAK
			CASE ciENTITY_EXISTS_DT_OBJECTS
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex))
					IF NOT bCheckDoesNotExist
						bShouldTrigger = FALSE
					ENDIF
				ELIF bCheckDoesNotExist
					bShouldTrigger = FALSE
				ENDIF
			BREAK			
			CASE ciENTITY_EXISTS_DT_PROPS
				IF NOT DOES_ENTITY_EXIST(oiProps[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex])	
					IF NOT bCheckDoesNotExist
						bShouldTrigger = FALSE
					ENDIF
				ELIF bCheckDoesNotExist
					bShouldTrigger = FALSE
				ENDIF
			BREAK
			CASE ciENTITY_EXISTS_DT_WEAPONS
				IF NOT DOES_PICKUP_EXIST(pipickup[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex])
					IF NOT bCheckDoesNotExist
						bShouldTrigger = FALSE
					ENDIF
				ELIF bCheckDoesNotExist
					bShouldTrigger = FALSE
				ENDIF
			BREAK	
		ENDSWITCH
	ENDIF

	RETURN bShouldTrigger

ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_BEING_GANG_LEADER(INT iDialogue)
	
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_GangBossOnly)
		IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			// All good!
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_BEING_GANG_LEADER - We are the gang boss!")
		ELSE
			bShouldTrigger = FALSE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_BEING_GANG_LEADER - We're not the gang boss!")
		ENDIF
	ENDIF
		
	RETURN bShouldTrigger

ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_CASH_GRAB_TOTAL_TAKE_LIMITS(INT iDialogue)
	
	BOOL bShouldTrigger = TRUE
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCashGrabLowerLimit > 0
		IF GET_TOTAL_CASH_GRAB_TAKE() < g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCashGrabLowerLimit
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_CASH_GRAB_TOTAL_TAKE_LIMITS - Total cash grab is less than the lower limit. Return FALSE")
			bShouldTrigger = FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCashGrabUpperLimit > 0
		IF GET_TOTAL_CASH_GRAB_TAKE() > g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCashGrabUpperLimit
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_CASH_GRAB_TOTAL_TAKE_LIMITS - Total cash grab is more than the upper limit. Return FALSE")
			bShouldTrigger = FALSE
		ENDIF
	ENDIF
		
	RETURN bShouldTrigger

ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_OBJECT_CONTINUITY(INT iDialogue)
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iLinkedObjectContinuityId = -1
		//Not set
		RETURN TRUE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ObjectTracking)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_OBJECT_CONTINUITY - can never trigger - object continuity set but tracking off!")
		RETURN FALSE
	ENDIF
	
	IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iObjectTrackingBitset, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iLinkedObjectContinuityId)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_OBJECT_CONTINUITY - waiting for linked continuity ID to pass")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC
FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_LOCATION_CONTINUITY(INT iDialogue)
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iLinkedLocationContinuityId = -1
		//Not set
		RETURN TRUE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_LocationTracking)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_LOCATION_CONTINUITY - can never trigger - location continuity set but tracking off!")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iLocationTrackingBitset, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iLinkedLocationContinuityId)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_LOCATION_CONTINUITY - waiting for linked continuity ID to pass")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_CONTINUITY(INT iDialogue)
	
	IF NOT SHOULD_DIALOGUE_TRIGGER_DUE_TO_OBJECT_CONTINUITY(iDialogue)
		RETURN FALSE
	ENDIF
	
	IF NOT SHOULD_DIALOGUE_TRIGGER_DUE_TO_LOCATION_CONTINUITY(iDialogue)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_PED_VOICE_INDEX_HAS_LOS_WITH_PLAYER_TRIGGER_DIALOGUE(INT iDialogue)
	
	BOOL bShouldTrigger = TRUE

	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_RequiresLOSWithAPlayerPed)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_PED_VOICE_INDEX_HAS_LOS_WITH_PLAYER_TRIGGER_DIALOGUE - Checking if players do have aggro in order to trigger dialogue.")
		
		PED_INDEX pedToCheck
		INT iPedVoice = -1
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedVoice > -1
			iPedVoice = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedVoice
		ENDIF
		
		IF iPedVoice > -1		
			IF bPedToUseOk
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPedVoice])					
					pedToCheck = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPedVoice])
					
					IF NOT IS_PED_INJURED(pedToCheck)
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_PED_VOICE_INDEX_HAS_LOS_WITH_PLAYER_TRIGGER_DIALOGUE - iPed: ", iPedVoice, " Is alive")	
						
						IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(pedToCheck, PlayerPedToUse)
						AND VDIST2(GET_ENTITY_COORDS(pedToCheck), GET_ENTITY_COORDS(PlayerPedToUse)) < POW(2.0, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].fRadius)
							PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_PED_VOICE_INDEX_HAS_LOS_WITH_PLAYER_TRIGGER_DIALOGUE = TRUE")
							bShouldTrigger = TRUE
						ELSE
							PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_PED_VOICE_INDEX_HAS_LOS_WITH_PLAYER_TRIGGER_DIALOGUE = FALSE")
							bShouldTrigger = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_MORE_CCTVS_TO_BE_DESTROYED(INT iDialogue)
	
	BOOL bShouldTrigger = TRUE
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCCTVsDestroyed > 0
		IF MC_serverBD_2.iCamObjDestroyed >= g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCCTVsDestroyed
			// Destroyed enough cameras!
			bShouldTrigger = TRUE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "][Dialogue Triggers][CCTV_Dialogue] - SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_MORE_CCTVS_TO_BE_DESTROYED - Enough CCTVs have been destroyed!")
		ELSE
			bShouldTrigger = FALSE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "][Dialogue Triggers][CCTV_Dialogue] - SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_MORE_CCTVS_TO_BE_DESTROYED - Players have only destroyed ", MC_serverBD_2.iCamObjDestroyed, " / ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCCTVsDestroyed)
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_CCTV_TO_BE_TASERED(INT iDialogue)
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_RequireCCTVHitByStunGun)
		IF iCCTVCamsCurrentlyTasered > 0
			bShouldTrigger = TRUE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "][Dialogue Triggers][CCTV_Dialogue] - SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_CCTV_TO_BE_TASERED - A CCTV has been tasered!")
		ELSE
			bShouldTrigger = FALSE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "][Dialogue Triggers][CCTV_Dialogue] - SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_CCTV_TO_BE_TASERED - Waiting for a CCTV to be tasered")
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_PLAYER_TO_ALERT_A_CCTV(INT iDialogue)
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_RequirePlayerToBeSpottedByCCTV)
		IF HAVE_CCTV_CAMERAS_WITH_AGGRO_BS_BEEN_ALERTED_BY_PLAYER(MC_PlayerBD[iPartToUse].iTeam, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAggroIndexBS_Entity_Dialog)
		#IF IS_DEBUG_BUILD 
		AND NOT bBlockBranchingOnAggroKeepStealthDialogue
		#ENDIF
			bShouldTrigger = TRUE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "][Dialogue Triggers][CCTV_Dialogue] - SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_PLAYER_TO_ALERT_A_CCTV - A CCTV has caught a player!")
		ELSE
			bShouldTrigger = FALSE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "][Dialogue Triggers][CCTV_Dialogue] - SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_PLAYER_TO_ALERT_A_CCTV - Waiting for a CCTV to catch a player")
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_DEAD_BODY_TO_ALERT_A_CCTV(INT iDialogue)
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_RequireDeadBodyToBeSpottedByCCTV)
		IF HAVE_CCTV_CAMERAS_WITH_AGGRO_BS_SPOTTED_DEAD_BODY(MC_PlayerBD[iPartToUse].iTeam, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAggroIndexBS_Entity_Dialog)
		#IF IS_DEBUG_BUILD 
		AND NOT bBlockBranchingOnAggroKeepStealthDialogue
		#ENDIF
			bShouldTrigger = TRUE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "][Dialogue Triggers][CCTV_Dialogue] - SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_DEAD_BODY_TO_ALERT_A_CCTV - A CCTV has found a dead body")
		ELSE
			bShouldTrigger = FALSE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "][Dialogue Triggers][CCTV_Dialogue] - SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_DEAD_BODY_TO_ALERT_A_CCTV - Waiting for a CCTV to find a dead body")
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_FUSEBOX_TO_BE_TASERED(INT iDialogue)
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_RequireFuseboxHitByStunGun)
		IF iFuseBoxesCurrentlyTasered > 0
			bShouldTrigger = TRUE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "][Dialogue Triggers][CCTV_Dialogue] - SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_FUSEBOX_TO_BE_TASERED - A Fusebox has been tasered!")
		ELSE
			bShouldTrigger = FALSE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "][Dialogue Triggers][CCTV_Dialogue] - SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_FUSEBOX_TO_BE_TASERED - Waiting for a Fusebox to be tasered")
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_ZONE_CONDITION(INT iDialogue)
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iZoneIndex > -1
		
		INT iZone = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iZoneIndex
		
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iZoneTimer_TimeRemaining > 0
			IF IS_ZONE_TIMER_RUNNING(iZone)
				IF GET_ZONE_TIMER_TIME_REMAINING(iZone, TRUE) <= g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iZoneTimer_TimeRemaining
				AND GET_ZONE_TIMER_TIME_REMAINING(iZone, TRUE) > g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iZoneTimer_TimeRemaining - 5
					// In the time window!
				ELSE
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_ZONE_CONDITION - iZone: ", iZone, " || Not in window for iZoneTimer_TimeRemaining || GET_ZONE_TIMER_TIME_REMAINING(iZone, TRUE): ", GET_ZONE_TIMER_TIME_REMAINING(iZone, TRUE))
					RETURN FALSE
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
			
		ELIF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iZoneTimer_TimeRemaining = 0
			IF NOT HAS_ZONE_TIMER_COMPLETED(iZone)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_ZONE_CONDITION - iZone: ", iZone, " || Zone Timer hasn't completed")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_PLAY_FOR_KILL_COUNT(INT iDialogue)
	BOOL bShouldTrigger = TRUE	
	
	INT iTeam = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam
	
	IF iTeam > -1
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		IF iRule < FMMC_MAX_RULES
			
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedsKillRequired > 0
			AND MC_serverBD_4.iCurrentHighestPriority[iTeam] >= g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRule
			
				IF NOT IS_BIT_SET(iDialogueKillsThisRuleSet, iRule)
					iDialogueKillsThisRule[iRule] = GET_TEAM_KILLS(iTeam)
					SET_BIT(iDialogueKillsThisRuleSet, iRule)
				ENDIF
				
				INT iKillsRequired = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedsKillRequired
				INT iKills = (GET_TEAM_KILLS(iTeam) - iDialogueKillsThisRule[iRule])
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset, iBS_DialogueTriggerLater)
					IF iRule != g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRule
						bShouldTrigger = FALSE
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_PLAY_FOR_KILL_COUNT - iTeam:  ", iTeam, " iBS_DialogueTriggerLater NOT set and is not on the required rule.")
					ENDIF
				ENDIF
				
				IF iKillsRequired > 0
					IF iKills < iKillsRequired
						bShouldTrigger = FALSE
						PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_PLAY_FOR_KILL_COUNT - iTeam:  ", iTeam, " has ", iKills, " Kills, out of a required: ", iKillsRequired)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_PLAY_FOR_KILL_COUNT_CIVILLIANS(INT iDialogue)
	BOOL bShouldTrigger = TRUE	
	INT iTeam = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam	
	IF iTeam > -1
		INT iKillsRequired = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCivillianKillRequired
		INT iKills = MC_ServerBD.iTeamCivillianKills[iTeam]
		IF iKillsRequired > -1
			IF iKills < iKillsRequired
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_PLAY_FOR_KILL_COUNT_CIVILLIANS - iTeam:  ", iTeam, " has ", iKills, " Kills, out of a required: ", iKillsRequired)
				bShouldTrigger = FALSE
			ENDIF
		ENDIF
	ENDIF
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_REQUIRE_PHONE_EMP_TO_BE_AVAILABLE(INT iDialogue)
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_RequireEMPToBeAvailable)
		IF IS_PHONE_EMP_AVAILABLE(FALSE)
		AND NOT HAS_PHONE_EMP_BEEN_USED()
			bShouldTrigger = TRUE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "][Dialogue Triggers][PhoneEMP_Dialogue] - SHOULD_DIALOGUE_TRIGGER_REQUIRE_PHONE_EMP_TO_BE_AVAILABLE - Available!")
		ELSE
			bShouldTrigger = FALSE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "][Dialogue Triggers][PhoneEMP_Dialogue] - SHOULD_DIALOGUE_TRIGGER_REQUIRE_PHONE_EMP_TO_BE_AVAILABLE - Either not available or already used")
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_REQUIRE_PHONE_EMP_NOT_FIRED(INT iDialogue)
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_RequirePhoneEMPNotUsed)
		IF HAS_PHONE_EMP_BEEN_USED()
			bShouldTrigger = FALSE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "][Dialogue Triggers][PhoneEMP_Dialogue] - SHOULD_DIALOGUE_TRIGGER_REQUIRE_PHONE_EMP_NOT_FIRED - Phone EMP has been used! Returning FALSE")
		ELSE
			bShouldTrigger = TRUE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "][Dialogue Triggers][PhoneEMP_Dialogue] - SHOULD_DIALOGUE_TRIGGER_REQUIRE_PHONE_EMP_NOT_FIRED - Not used the Phone EMP yet")
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_REQUIRE_DOOR_UNLOCKED_WITH_KEYCARD(INT iDialogue)
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_RequireKeycardUsage)
		IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_UNLOCKED_DOOR_WITH_SWIPECARD)
			bShouldTrigger = TRUE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "][Dialogue Triggers][InteractWith_Dialogue] - SHOULD_DIALOGUE_TRIGGER_REQUIRE_DOOR_UNLOCKED_WITH_KEYCARD - LBOOL32_UNLOCKED_DOOR_WITH_SWIPECARD is set!")
		ELSE
			bShouldTrigger = FALSE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "][Dialogue Triggers][InteractWith_Dialogue] - SHOULD_DIALOGUE_TRIGGER_REQUIRE_DOOR_UNLOCKED_WITH_KEYCARD - Not swiped a door yet")
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_GANG_CHASE_LOST(INT iDialogue)
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset8, iBS_Dialogue8_LoseGangChase)
		IF IS_BIT_SET(iLocalBoolCheck9, LBOOL9_AT_LEAST_ONE_GANG_CHASE_UNIT_SPAWNED)
		AND NOT IS_CURRENT_OBJECTIVE_LOSE_GANG_CHASE()
			bShouldTrigger = TRUE
		ELSE
			bShouldTrigger = FALSE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_GANG_CHASE_LOST - Gang Chase Active or not yet spawned")
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_TRIGGER_ON_PED_TASED(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_RequirePedTased)
		//Waiting for trigger
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_A_PED_HAS_BEEN_STUNNED)
			RETURN FALSE
		ENDIF
		
		//Blocking
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_BlockIfPedHasBeenTased)
		AND IS_BIT_SET(iDialogueBlockedConditionBS, ciDT_Blocked_PedHasBeenTased)
			RETURN FALSE
		ENDIF
		
		IF NOT IS_BIT_SET(iDialogueBlockedConditionBS, ciDT_Blocked_PedHasBeenTased)
			SET_BIT(iDialogueBlockedConditionBS, ciDT_Blocked_PedHasBeenTased)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_TRIGGER_ON_PED_TASED - Setting ciDT_Blocked_PedHasBeenTased")
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC
FUNC BOOL SHOULD_TRIGGER_ON_PED_TRANQUILIZED(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_RequirePedTranquilized)
		
		//Waiting for trigger
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_A_PED_HAS_BEEN_TRANQUILIZED)
			RETURN FALSE
		ENDIF
		
		//Blocking
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_BlockIfPedHasBeenTranquilized)
		AND IS_BIT_SET(iDialogueBlockedConditionBS, ciDT_Blocked_PedHasBeenTranquilized)
			RETURN FALSE
		ENDIF
		
		IF NOT IS_BIT_SET(iDialogueBlockedConditionBS, ciDT_Blocked_PedHasBeenTranquilized)
			SET_BIT(iDialogueBlockedConditionBS, ciDT_Blocked_PedHasBeenTranquilized)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_TRIGGER_ON_PED_TRANQUILIZED - Setting ciDT_Blocked_PedHasBeenTranquilized")
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_TRIGGER_ON_PED_EMPED(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset8, iBS_Dialogue8_RequirePedTased)
		//Waiting for trigger
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_A_PED_HAS_BEEN_EMPED)
			RETURN FALSE
		ENDIF
		
		//Blocking
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset8, iBS_Dialogue8_BlockIfPedHasBeenTranquilized)
		AND IS_BIT_SET(iDialogueBlockedConditionBS, ciDT_Blocked_PedHasBeenEMPed)
			RETURN FALSE
		ENDIF
		
		IF NOT IS_BIT_SET(iDialogueBlockedConditionBS, ciDT_Blocked_PedHasBeenEMPed)
			SET_BIT(iDialogueBlockedConditionBS, ciDT_Blocked_PedHasBeenEMPed)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_TRIGGER_ON_PED_EMPED - Setting ciDT_Blocked_PedHasBeenEMPed")
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC
FUNC BOOL SHOULD_DIALOGUE_TRIGGER_WORK_USING_DIALOGUE_TRIGGER_BLOCKERS(INT iDialogue)
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_BlockWithTriggerBlockerList)
		IF IS_BIT_SET(iLocalDialogueBlockedBS[iDialogue/32], iDialogue%32)
			bShouldTrigger = FALSE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_WORK_USING_DIALOGUE_TRIGGER_BLOCKERS - iDialogue: ", iDialogue, " Has been blocked - bShouldTrigger = FALSE")
		ELSE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_WORK_USING_DIALOGUE_TRIGGER_BLOCKERS - iDialogue: ", iDialogue, " Has not been blocked yet.")
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL IS_DIALOGUE_TRIGGER_BLOCKED_BY_ENTITIES(INT iDialogue)
	
	BOOL bBlocked = IS_BIT_SET(iLocalDialogueBlockedByEntityBS[iDialogue/32], iDialogue%32)
	#IF IS_DEBUG_BUILD
	IF bBlocked
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_DIALOGUE_TRIGGER_BLOCKED_BY_ENTITIES - iDialogue: ", iDialogue, " Blocked!")
	ENDIF
	#ENDIF
	
	RETURN bBlocked
	
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_AT_MAXIMUM_BAG_CAPACITY(INT iDialogue)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_TriggerFullBagCapacity)
		IF MC_playerBD[iLocalPart].sLootBag.fCurrentBagCapacity != BAG_CAPACITY__GET_MAX_CAPACITY()
			RETURN FALSE
		ENDIF
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_AT_MAXIMUM_BAG_CAPACITY - iDialogue: ", iDialogue, " Bag is at maximum capacity")
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_IN_CURRENT_WEATHER(INT iDialogue)
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWeatherType != ciFMMC_WEATHER_OPTION_CURRENT
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWeatherType != MC_serverBD.iWeather
			RETURN FALSE
		ENDIF
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_IN_CURRENT_WEATHER - iDialogue: ", iDialogue, " Weather matches current setting")
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_LOOT_GRAB_START(INT iDialogue)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_LootGrab_Start)
		IF iCashGrabState <= ciCASH_GRAB_STATE_INIT
			RETURN FALSE
		ENDIF
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_LOOT_GRAB_START - iDialogue: ", iDialogue, " Just started Loot Grab")
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_LOOT_GRAB_FINISH(INT iDialogue)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_LootGrab_Finish)
		IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_FINISHED_LOOT_GRAB)
			RETURN FALSE
		ENDIF
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_IN_GRAB_FINISH - iDialogue: ", iDialogue, " Finished Loot Grab")
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_OBJECT_COLLECTION(INT iDialogue)
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iObjectToCollect != -1
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iObjectToCollect != iPlayerCollectedObjectID
		OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeamToCollectObject != iPlayerCollectedObjectTeam
			RETURN FALSE
		ENDIF
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_OBJECT_COLLECTION - Waiting for someone on team: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeamToCollectObject, " to collect object: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iObjectToCollect)
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_NOT_HAVING_TARGET_OBJECT(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_TriggerObjectRequiredNotHeld)
		IF NOT IS_CURRENT_OBJECTIVE_COLLECT_OBJECT()
			RETURN FALSE
		ENDIF
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_NOT_HAVING_TARGET_OBJECT - iDialogue: ", iDialogue, " No one has obj")
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_HAVING_TARGET_OBJECT(INT iDialogue)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_TriggerObjectRequiredHeld)
	
		INT iTeam = MC_playerBD[iPartToUse].iTeam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule < FMMC_MAX_RULES
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjRequiredForRulePass[iRule] < 0
				RETURN FALSE
			ENDIF
			
			IF MC_serverBD.iObjCarrier[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjRequiredForRulePass[iRule]] = -1
				RETURN FALSE
			ENDIF
			
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_HAVING_TARGET_OBJECT - iDialogue: ", iDialogue, " Someone has the obj")
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_LEAVING_AREA_BEFORE_TEAM(INT iDialogue)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_LeaveAreaBeforeTeam)

		IF MC_serverBD.iNumberOfPlayingPlayers[MC_playerBD[iPartToUse].iTeam] = 1
			RETURN FALSE
		ENDIF

		IF IS_BIT_SET(iLocalBoolCheck6,LBOOL6_IN_MY_PRIORITY_LEAVE_LOC)
			RETURN FALSE
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)
			RETURN FALSE
		ENDIF
		
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + MC_playerBD[iPartToUse].iTeam)
			RETURN FALSE
		ENDIF
		
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_LEAVING_AREA_BEFORE_TEAM - iDialogue: ", iDialogue, " Player is out, waiting for team")
		
	ENDIF

	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_LOCAL_PLAYER_OUT_OF_LIVES(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_LocalPlayerOutOfLives)
		IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
			RETURN FALSE
		ENDIF
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_LOCAL_PLAYER_OUT_OF_LIVES - Local player ran out of lives")
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_HAVING_LOOT_IN_BAG(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_LocalPlayerHasLoot)
		//Check for this dialogue trigger
		IF MC_playerBD[iLocalPart].sLootBag.fCurrentBagCapacity != 0.0
			RETURN FALSE
		ENDIF
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_HAVING_LOOT_IN_BAG - Local player has loot in their bag")
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_PREREQS(INT iDialogue)
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueRequiredPrereqs[0] != ciPREREQ_None
	OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBlockerPrereqs[0] != ciPREREQ_None
		INT iPreReq = 0
		FOR iPreReq = 0 TO ciDIALOGUE_MAX_PREREQS - 1
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueRequiredPrereqs[iPreReq] != ciPREREQ_None
			AND NOT IS_PREREQUISITE_COMPLETED(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueRequiredPrereqs[iPreReq])
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_PREREQS | Need to complete PreReq ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueRequiredPrereqs[iPreReq])
				RETURN FALSE
			ENDIF
	
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBlockerPrereqs[iPreReq] != ciPREREQ_None
			AND IS_PREREQUISITE_COMPLETED(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBlockerPrereqs[iPreReq])
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_PREREQS | Blocked due to complete PreReq ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBlockerPrereqs[iPreReq])
				RETURN FALSE
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_TIME_SINCE_LAST_DIALOGUE(INT iDialogue)
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iLastDialogueTimerLength != 0
		IF HAS_NET_TIMER_STARTED(tdTimeSinceLastDialoguePlayed)
			IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdTimeSinceLastDialoguePlayed, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iLastDialogueTimerLength)
				RETURN FALSE
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_TIME_SINCE_LAST_DIALOGUE - Time since last dialogue played has expired")
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_UNDERWATER_WELDING_MINIGAME(INT iDialogue)	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_UnderwaterTunnelWeldingMinigameStart)
		IF NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_TUNNEL_WELDING_MINIGAME__STARTED)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_UNDERWATER_WELDING_MINIGAME - Return FALSE - Game hasn't started yet.")
			RETURN FALSE
		ENDIF
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_UnderwaterTunnelWeldingMinigamePassed)
		IF NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_TUNNEL_WELDING_MINIGAME__PASSED)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_UNDERWATER_WELDING_MINIGAME - Return FALSE - Game hasn't been passed yet.")
			RETURN FALSE
		ENDIF
	ENDIF	
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_SAFE_CRACK_MINIGAME(INT iDialogue)	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_EnterSafeCombinationMinigameStart)
		IF NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_ENTER_SAFE_COMBINATION_MINIGAME__STARTED)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_SAFE_CRACK_MINIGAME - Return FALSE - Game hasn't started yet.")
			RETURN FALSE
		ENDIF
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_EnterSafeCombinationMinigameOngoing)
		IF NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_ENTER_SAFE_COMBINATION_MINIGAME__ONGOING)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_SAFE_CRACK_MINIGAME - Return FALSE - Game isn't ongoing yet.")
			RETURN FALSE
		ENDIF
	ENDIF	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_EnterSafeCombinationMinigamePassed)
		IF NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_ENTER_SAFE_COMBINATION_MINIGAME__PASSED)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_SAFE_CRACK_MINIGAME - Return FALSE - Game hasn't been passed yet.")
			RETURN FALSE
		ENDIF
	ENDIF	
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_VOLTAGE_MINIGAME(INT iDialogue)	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_VoltageMinigameStart)
		IF NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_VOLTAGE_MINIGAME__STARTED)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_VOLTAGE_MINIGAME - Return FALSE - Game hasn't started yet.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_VoltageMinigameFailed)
		IF NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_VOLTAGE_MINIGAME__FAILED)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_VOLTAGE_MINIGAME - Return FALSE - Game hasn't failed yet.")
			RETURN FALSE
		ENDIF
	ENDIF	
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVoltageMinigameFailuresRequired > 0
		IF iNumberOfTimesFailedVoltage < g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVoltageMinigameFailuresRequired
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_VOLTAGE_MINIGAME - Return FALSE - Game hasnt failed enough times yet.")
			RETURN FALSE	
		ENDIF	
	ENDIF
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVoltageMinigameExitsRequired > 0
		IF iNumberOfTimesExitedVoltage < g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVoltageMinigameExitsRequired
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_VOLTAGE_MINIGAME - Return FALSE - Game hasnt been exited enough times yet.")
			RETURN FALSE
		ENDIF		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_VoltageMinigamePassed)
		IF NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_VOLTAGE_MINIGAME__PASSED)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_VOLTAGE_MINIGAME - Return FALSE - Game hasn't been passed yet.")
			RETURN FALSE
		ENDIF
	ENDIF	

	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_FINGERPRINT_CLONE(INT iDialogue)

	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_FingerPrintStarted)
		IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_STARTED_FINGERPRINT_CLONE)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_FINGERPRINT_CLONE - Return FALSE - Game hasn't started.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_FingerPrintFailed)
		IF iFingerprintClone_Fails = 0
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_FINGERPRINT_CLONE - Return FALSE - Game hasn't been failed.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_FingerPrintPassed)
		IF iFingerprintClone_Wins = 0
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_FINGERPRINT_CLONE - Return FALSE - Game hasn't been passed.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iFingerprintPatternsComplete > 0
		IF iFingerprintClone_PatternDone != g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iFingerprintPatternsComplete
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_FINGERPRINT_CLONE - Return FALSE - patterns done: ", iFingerprintClone_PatternDone, " out of ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iFingerprintPatternsComplete)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iFingerprintPatternsFailed > 0
		IF iFingerprintClone_LivesLost != g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iFingerprintPatternsFailed
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_FINGERPRINT_CLONE - Return FALSE - patterns failed: ", iFingerprintClone_LivesLost, " out of ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iFingerprintPatternsFailed)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iFingerprintExitsRequired > 0
		IF iFingerprintClone_Exits != g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iFingerprintExitsRequired
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_FINGERPRINT_CLONE - Return FALSE - exits: ", iFingerprintClone_Exits, " out of ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iFingerprintExitsRequired)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_GLASS_CUTTING_MINIGAME(INT iDialogue)

	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_GlassCuttingStarted)
		IF eGlassCuttingStage = GLASS_CUTTING_STAGE__WAITING_TO_START
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_GLASS_CUTTING_MINIGAME - Return FALSE - Game hasn't started.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_GlassCuttingOngoing)
		IF eGlassCuttingStage = GLASS_CUTTING_STAGE__RUNNING
		AND sInteractWithVars.iInteractWith_SubAnimPreset != ciIW_SUBANIM__GLASSCUT_ENTER
		AND sInteractWithVars.iInteractWith_SubAnimPreset != ciIW_SUBANIM__GLASSCUT_ENTER_ALT
			// Free to trigger!
		ELSE
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_GLASS_CUTTING_MINIGAME - Return FALSE - Game isn't in running state or player is still doing the enter anim")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_GlassCuttingComplete)
		IF NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_GLASS_CUTTING_MINIGAME__PASSED)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_GLASS_CUTTING_MINIGAME - Return FALSE - LBOOL34_GLASS_CUTTING_MINIGAME__PASSED isn't set!")
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_OTHER_PLAYERS_RANGE(INT iDialogue)
		
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRangeCheckType != DT_RANGE_TYPE_NONE		
		INT iPart = -1
		INT iCount
		INT iTeamToCheck = GET_LOCAL_PLAYER_TEAM()
		
		//Team to check override
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRangeCheckTeam != -1						
			//Don't check the same team for iRangeCheckTeam
			IF MC_playerBD[iPartToUse].iTeam = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRangeCheckTeam
				RETURN FALSE
			ENDIF
			
			iTeamToCheck = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRangeCheckTeam
		ENDIF
		
		DO_PARTICIPANT_LOOP_FLAGS eFlags = GET_PARTICIPANT_LOOP_FLAGS_TO_CHECK_SINGLE_TEAM(iTeamToCheck)
		WHILE DO_PARTICIPANT_LOOP(iPart, eFlags | DPLF_CHECK_PED_ALIVE | DPLF_EXCLUDE_LOCAL_PART)
			
			PED_INDEX tempPed = GET_PLAYER_PED_INDEX_FROM_INT(iPart)
			
			IF IS_ENTITY_ALIVE(tempPed)
				VECTOR vPos = GET_ENTITY_COORDS(tempPed)
				
				//PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue,"] SHOULD_DIALOGUE_TRIGGER_DUE_TO_OTHER_PLAYERS_RANGE - vPos: ", vPos, " vLocalPlayerPosition: ", vLocalPlayerPosition, " VDIST2: ", VDIST2(vLocalPlayerPosition, vPos), " frange: ", POW(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].fRangeCheckRange, 2))
				
				IF VDIST2(vLocalPlayerPosition, vPos) > POW(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].fRangeCheckRange, 2) 
 					IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRangeCheckType = DT_RANGE_TYPE_WITHIN
						//PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue,"] SHOULD_DIALOGUE_TRIGGER_DUE_TO_OTHER_PLAYERS_RANGE - DT_RANGE_TYPE_WITHIN - RETURN FALSE")
						RETURN FALSE
					ELIF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRangeCheckType = DT_RANGE_TYPE_OUT_ANY
						iCount++
						//PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue,"] SHOULD_DIALOGUE_TRIGGER_DUE_TO_OTHER_PLAYERS_RANGE - DT_RANGE_TYPE_OUT_ANY - a player is out of range.")
						BREAKLOOP
					ENDIF
				ELSE
					IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRangeCheckType = DT_RANGE_TYPE_OUT
						//PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue,"] SHOULD_DIALOGUE_TRIGGER_DUE_TO_OTHER_PLAYERS_RANGE - DT_RANGE_TYPE_OUT - RETURN FALSE")
						RETURN FALSE
					ELIF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRangeCheckType = DT_RANGE_TYPE_WITHIN_ANY
						iCount++
						//PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue,"] SHOULD_DIALOGUE_TRIGGER_DUE_TO_OTHER_PLAYERS_RANGE - DT_RANGE_TYPE_WITHIN_ANY - a player is in range.")
						BREAKLOOP
					ENDIF
				ENDIF
			ENDIF
		ENDWHILE
		
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRangeCheckType = DT_RANGE_TYPE_WITHIN_ANY
		OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRangeCheckType = DT_RANGE_TYPE_OUT_ANY
			IF iCount = 0
				RETURN FALSE
			ENDIF
		ENDIF

		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_OTHER_PLAYERS_RANGE - All active players match the range check")
	ENDIF
	
	RETURN TRUE
ENDFUNC

   
FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_LOCH_SANTOS_MONSTER(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_LochSantosMonster)
		IF NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_LOCH_SANTOS_MONSTER_DIVING)
			RETURN FALSE
		ENDIF
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_LOCH_SANTOS_MONSTER - Monster started diving")
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLAYER_COUNT_BLOCKING_DIALOGUE_TRIGGER(INT iDialogue)

	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPlayerCountRequirement = 0
		RETURN FALSE
	ENDIF
	
	INT iNumPlayers = PICK_INT(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPlayerCountType = ciDT_PLAYER_COUNT_TYPE_CURRENT, GET_TOTAL_PLAYING_PLAYERS(), GET_TOTAL_STARTING_PLAYERS())
	
	SWITCH g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPlayerCountComparison
		CASE ciDT_PLAYER_COUNT_COMPARISON_EQUALS
			IF iNumPlayers = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPlayerCountRequirement
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciDT_PLAYER_COUNT_COMPARISON_GREATER
			IF iNumPlayers > g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPlayerCountRequirement
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciDT_PLAYER_COUNT_COMPARISON_LESS
			IF iNumPlayers < g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPlayerCountRequirement
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - IS_PLAYER_COUNT_BLOCKING_DIALOGUE_TRIGGER - Blocked due to player count - Req: ",
				g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPlayerCountRequirement,
				", iNumPlayers: ", iNumPlayers,
				", comparison: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPlayerCountComparison,
				", type: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPlayerCountType)
	
	RETURN TRUE
		
ENDFUNC

FUNC BOOL IS_PED_FLEE_TYPE_VALID_FOR_TRIGGER(INT iFleeType, INT iPed)
	SWITCH iFleeType
		CASE ciDT_FLEE_TYPE_VEHICLE_HEALTH
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iPedFleeDueToVehicleHealth, iPed)
		
		CASE ciDT_FLEE_TYPE_ZONE
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iPedFleeDueToZone, iPed)
			
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC
   
FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_PED_FLEEING(INT iDialogue)
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedTrigger != -1
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_FleeingPed)
		IF MC_serverBD_2.iPedState[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedTrigger] != ciTASK_FLEE
		OR NOT IS_PED_FLEE_TYPE_VALID_FOR_TRIGGER(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedFleeType, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedTrigger)
			RETURN FALSE
		ENDIF
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_PED_FLEEING - Ped ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedTrigger, " is fleeing")
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_WARP_PORTAL(INT iDialogue)

	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iLinkedWarpPortalStart != -1
		IF IS_BIT_SET(iDialogueHasUsedPortalStartBS, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iLinkedWarpPortalStart)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_WARP_PORTAL - Player has used the Start of portal ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iLinkedWarpPortalStart)
			RETURN TRUE
		ELSE
			// Player hasn't used the Start of the specified Warp Portal!
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_WARP_PORTAL - Hasn't used the Start of portal ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iLinkedWarpPortalStart)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iLinkedWarpPortalEnd != -1
		IF IS_BIT_SET(iDialogueHasUsedPortalEndBS, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iLinkedWarpPortalEnd)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_WARP_PORTAL - Player has used the End of portal ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iLinkedWarpPortalEnd)
			RETURN TRUE
		ELSE
			// Player hasn't used the End of the specified Warp Portal!
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_WARP_PORTAL - Hasn't used the End of portal ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iLinkedWarpPortalEnd)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_PERSONAL_VEHICLE(INT iDialogue)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_PlayDialogueOnlyWhenInPV)
		
		IF IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
			VEHICLE_INDEX viVeh = GET_VEHICLE_PED_IS_IN(PlayerPedToUse)
			VEHICLE_INDEX viPersonalVeh = PERSONAL_VEHICLE_ID()		
			IF IS_VEHICLE_DRIVEABLE(viVeh)
				IF viVeh = viPersonalVeh
					RETURN TRUE	
				ENDIF
				
				INT iMissionVehicle = IS_ENTITY_A_MISSION_CREATOR_ENTITY(viVeh)
				IF iMissionVehicle > -1
				AND IS_BIT_SET(iVehicleLocalPartNeedsToCloneBS, iMissionVehicle)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_PERSONAL_VEHICLE - Returning False. We're not inside our Personal Vehicle. (Only Play when In PV)")
		
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset7, iBS_Dialogue7_BlockDialogueWhenInPV)
		IF IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
			VEHICLE_INDEX viVeh = GET_VEHICLE_PED_IS_IN(PlayerPedToUse)
			VEHICLE_INDEX viPersonalVeh = PERSONAL_VEHICLE_ID()		
			IF IS_VEHICLE_DRIVEABLE(viVeh)
				IF viVeh = viPersonalVeh
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_PERSONAL_VEHICLE - Returning False. We are inside our Personal Vehicle. (Block When Inside)")
					RETURN FALSE	
				ENDIF
				
				INT iMissionVehicle = IS_ENTITY_A_MISSION_CREATOR_ENTITY(viVeh)
				IF iMissionVehicle > -1
				AND IS_BIT_SET(iVehicleLocalPartNeedsToCloneBS, iMissionVehicle)
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogue, "] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_PERSONAL_VEHICLE - Returning False. We are inside our Personal Vehicle (placed). (Block When Inside)")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: CHECK VALIDITY OF DIALOGUE TRIGGER  -----------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to checking if a specific dilaogue trigger is valid for a specified team ----------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Checks if any of the dialogue triggers have been triggered
/// PARAMS:
///    iteam - Team for which to check if this dialogue trigger is valid
///    
FUNC BOOL IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM(INT iTeam)

	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset7, iBS_Dialogue7_SkipRevalidationAfterTimer)
	AND HAS_NET_TIMER_STARTED(tdDialogueTimer[iDialogueProgress])
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Returned true before, and set up to NOT revalidate after delay.")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[FMMC2020 refactor] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Refactor so there are not so many function calls")
	BOOL bReturn = FALSE

	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
	AND NOT HAS_TEAM_FAILED(MC_playerBD[iPartToUse].iteam)
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		INT iTriggerRule = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iRule
		BOOL bIgnoreRule = FALSE
		
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Starting Checks... iRule: ", iRule, " iTriggerRule: ", iTriggerRule)
		
		IF iTriggerRule = -1
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - iTriggerRule is -1")
			RETURN FALSE
		ENDIF
		
		IF IS_SCRIPTED_ANIM_CONVERSATION_IN_USE()
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Blocked by IS_SCRIPTED_ANIM_CONVERSATION_IN_USE.")
			
			#IF IS_DEBUG_BUILD
			IF bDialogueTriggerDebug
				DRAW_DEBUG_TEXT("Dialogue Triggers blocked by Scripted Anim Conversation", MC_GET_DIALOGUE_TRIGGER_POSITION(iDialogueProgress), 255, 255, 255, 255)
			ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset3, iBS_Dialogue3_IgnoreRule) // This option has been hidden in the creator and not removed from controller in order to preserve old content.
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - iBS_Dialogue3_IgnoreRule is set, ignoring rule checks")
			bIgnoreRule = TRUE
		ENDIF
		
		BOOL bTriggerThisDialogueLater = IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset, iBS_DialogueTriggerLater)
		
		IF IS_BIT_SET(iLocalDialoguePlayRemotelyWithPriority[iDialogueProgress/32],iDialogueProgress%32) 
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Has been set up to trigger remotely with priority. Returning TRUE.")
			RETURN TRUE
		ENDIF
		
		IF NOT IS_PLAYER_COUNT_BLOCKING_DIALOGUE_TRIGGER(iDialogueProgress)
			IF ( NOT bTriggerThisDialogueLater AND iRule = iTriggerRule )
			OR ( bTriggerThisDialogueLater AND iRule >= iTriggerRule )
			OR ( CAN_VEHICLE_DIAGLOUE_TRIGGER_ON_ANY_RULE(iDialogueProgress))
				
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - bTriggerThisDialogueLater: ", bTriggerThisDialogueLater)
				
				IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeamRuleLimit[MC_playerBD[iPartToUse].iteam] = -1
				OR MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] <= g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeamRuleLimit[MC_playerBD[iPartToUse].iteam]			
				
					// [FIX_2020_CONTROLLER] We should break this up a bit so we're not calling so many functions. This is a lot to process.
					//SHELVED - CL 23948032
					IF (NOT IS_CUTSCENE_PLAYING())
					AND (NOT IS_TRIP_SKIP_IN_PROGRESS(MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData))
					//Individual dialogue checks:
					AND NOT IS_BLOCKING_IF_WANTED(iDialogueProgress)
					AND HAS_ANY_PREREQUISITE_DIALOGUE_PLAYED(iDialogueProgress)
					AND HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE(iDialogueProgress) // Checks for triggering on losing/gaining wanted
					AND HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE(iDialogueProgress)
					AND HAS_PED_LEFT_OR_ENTERED_VEHICLE(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_GLASS_CUTTING_MINIGAME(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_VEHICLE_DAMAGE(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_DROPPING_TARGET_OBJECT_ON_DEATH(iDialogueProgress)
					AND IS_PLAYER_WEARING_MASK_TO_TRIGGER_DIALOGUE(iDialogueProgress)
					AND IS_PLAYER_IN_FIRST_PERSON(iDialogueProgress)
					AND IS_PLAYER_ENTERING_ANY_VEHICLE_FOR_DIALOGUE(iDialogueProgress)						
					AND IS_PED_IN_RANGE_FOR_DIALOGUE_TRIGGER(iDialogueProgress)
					AND IS_DIALOGUE_TRIGGERED_SD(iDialogueProgress)
					AND IS_ENOUGH_GANG_CHASE_UNITS_DEAD_TO_TRIGGER_DIALOGUE(iDialogueProgress)
					AND IS_PLAYER_VEH_UNDERWATER_TO_TRIGGER_DIALOGUE(iDialogueProgress)
					AND HAS_PED_DIED_TO_TRIGGER_DIALOGUE(iDialogueProgress)
					AND IS_PLAYER_VEH_BACK_OUT_OF_WATER_TO_TRIGGER_DIALOGUE(iDialogueProgress)
					AND HAS_PLAYER_ACTIVATED_THERMAL_VISION(iDialogueProgress)
					AND HAS_PLAYER_GOT_CLOSE_ENOUGH_TO_WATER_TO_TRIGGER_DIALOGUE(iDialogueProgress)
					AND HAS_PLAYER_STARTED_BEAMHACK_TRIGGER_DIALOGUE(iDialogueProgress)
					AND HAS_PLAYER_HIT_PACKET_TO_TRIGGER_DIALOGUE(iDialogueProgress)
					AND HAS_PLAYER_HIT_FIREWALL_TO_TRIGGER_DIALOGUE(iDialogueProgress)
					AND HAVE_ENOUGH_FIRES_BEEN_EXTINGUISHED(iDialogueProgress)
					AND HAS_MULTIRULE_TIMER_REACHED_TRIGGER_TIME(iDialogueProgress)
					AND IS_PLAYER_FLYING_TOO_HIGH_TO_TRIGGER_DIALOGUE(iDialogueProgress)
					AND IS_PLAYER_SPAWNING_GANG_CHASE_DUE_TO_FLYING_TOO_HIGH(iDialogueProgress)
					AND HAS_NOT_REACHED_MIDPOINT_YET_TRIGGER_DIALOGUE(iDialogueProgress)
					AND HAS_PLAYERS_NOT_IN_SEPARATE_OBJECTIVE_VEHICLES_TRIGGER_DIALOGUE(iDialogueProgress)
					AND NOT_BLOCKED_DUE_TO_BEING_SPECTATOR(iDialogueProgress)
					AND NOT_BLOCKED_DUE_TO_BEING_ANY_KIND_OF_SPECTATOR(iDialogueProgress)
					AND NOT_BLOCKED_DUE_TO_NOT_CARRYING_OBJ_VEH_WITH_CARGOBOB(iDialogueProgress)
					AND IS_SPECIFIC_VEHICLE_OPTION_INSIDE_VEHICLE_VALID(iDialogueProgress)
					AND IS_SPECIFIC_VEHICLE_OPTION_OWNS_VEHICLE_VALID(iDialogueProgress)
					AND IS_PLAYER_IN_VEHICLE_OF_PED_VOICE(iDialogueProgress)
					AND HAS_PED_BEEN_DAMAGED_PAST_THRESHOLD(iDialogueProgress)	
					AND SHOULD_VEHICLE_REPAIR_DIALOGUE_BE_TRIGGERED(iDialogueProgress)
					AND SHOULD_EMP_DIALOGUE_BE_TRIGGERED(iDialogueProgress)
					AND IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE(iDialogueProgress)	
					AND SHOULD_PLAYERS_AGGROED_TRIGGER_DIALOGUE(iDialogueProgress)
					AND SHOULD_PED_VOICE_INDEX_HAS_LOS_WITH_PLAYER_TRIGGER_DIALOGUE(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_MORE_CCTVS_TO_BE_DESTROYED(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_CCTV_TO_BE_TASERED(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_PLAYER_TO_ALERT_A_CCTV(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_DEAD_BODY_TO_ALERT_A_CCTV(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_FUSEBOX_TO_BE_TASERED(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_PLAY_FOR_KILL_COUNT(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_PLAY_FOR_KILL_COUNT_CIVILLIANS(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_REQUIRE_PHONE_EMP_TO_BE_AVAILABLE(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_REQUIRE_DOOR_UNLOCKED_WITH_KEYCARD(iDialogueProgress)	
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_GANG_CHASE_LOST(iDialogueProgress)
					AND SHOULD_TRIGGER_ON_PED_TASED(iDialogueProgress)
					AND SHOULD_TRIGGER_ON_PED_TRANQUILIZED(iDialogueProgress)
					AND SHOULD_TRIGGER_ON_PED_EMPED(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_ENTITY_EXISTING(iDialogueProgress)	
					AND SHOULD_DIALOGUE_TRIGGER_REQUIRE_PHONE_EMP_NOT_FIRED(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_BEING_GANG_LEADER(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_CASH_GRAB_TOTAL_TAKE_LIMITS(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_ZONE_CONDITION(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_CONTINUITY(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_WORK_USING_DIALOGUE_TRIGGER_BLOCKERS(iDialogueProgress)
					AND NOT IS_DIALOGUE_TRIGGER_BLOCKED_BY_ENTITIES(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_ZONES(iDialogueProgress)						
					AND NOT IS_DIALOGUE_TRIGGER_BLOCKED_FROM_LOCAL_PLAYER_SUBMERGED(iDialogueProgress)
					AND NOT IS_DIALOGUE_TRIGGER_BLOCKED_FROM_WAIT_FOR_PLAYER_TO_HAVE_BEEN_IN_WATER(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_ABILITY_AVAILABILITY(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_SUPPORT_SNIPER(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_AIR_STRIKE(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_HEAVY_LOADOUT(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_RECON_DRONE(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_HELI_BACKUP(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_INVALID_ABILITY_ACTIVATION(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_ONE_PLAYER_ALIVE(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_COMPLETED_INTERACTABLE(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_COMPLETED_INTERACTION_TYPE(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_UNCOMPLETED_INTERACTABLE(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_INTERACTABLE_IN_USE(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_AT_MAXIMUM_BAG_CAPACITY(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_IN_CURRENT_WEATHER(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_LOOT_GRAB_START(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_LOOT_GRAB_FINISH(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_OBJECT_COLLECTION(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_NOT_HAVING_TARGET_OBJECT(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_HAVING_TARGET_OBJECT(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_MISSION_EQUIPMENT(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_BOUNDS(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_LOCAL_PLAYER_OUT_OF_LIVES(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_CCTV_RANGE(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_HAVING_LOOT_IN_BAG(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_EXTRA_CASH_TAKE(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_TIME_SINCE_LAST_DIALOGUE(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_UNDERWATER_WELDING_MINIGAME(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_SAFE_CRACK_MINIGAME(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_VOLTAGE_MINIGAME(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_FINGERPRINT_CLONE(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_LOCH_SANTOS_MONSTER(iDialogueProgress)					
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_PREREQS(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_PED_FLEEING(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_WARP_PORTAL(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_PERSONAL_VEHICLE(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_CRASHING_INTO_ENEMY_VEHICLE(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_LANDING_GEAR_UP(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_PLAYER_OWNED_PROPERTY(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_OWNED_WEAPON(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_TIME_SINCE_PLAYER_LAST_FIRED_THEIR_WEAPON(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_INTERACT_WITH_OBJ(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_CACHED_VEHICLE(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_BASED_ON_CONTROLS(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_CURRENT_CUTSCENE_CAMERA_SHOT(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_BE_BLOCKED_DUE_TO_A_CAMERA_LOCK(iDialogueProgress)
					//Add potentially expensive checks below
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_OTHER_PLAYERS_RANGE(iDialogueProgress)
					AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_PREREQ_COUNTER(iDialogueProgress)	
					AND NOT MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].eSpawnConditionFlag, FALSE, FALSE, g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iAggroIndexBS_Entity_Dialog, MC_PlayerBD[iPartToUse].iTeam)
						//PRINTLN("[RCC MISSION][MJM] iDialogueProgress = ",iDialogueProgress," iBS_DialogueRequireObjectivVehicle = ",IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset, iBS_DialogueRequireObjectivVehicle), " vehCarryCount = ",MC_playerBD[iPartToUse].iVehCarryCount," with carrier = ",IS_BIT_SET( MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER )
						// IF we want dialogue to wait until we've collected a vehicle or are with the driver
						
						INT iPointsRequired = RETURN_INT_CUSTOM_VARIABLE_VALUE(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPoint)
						
						IF IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset, iBS_DialogueRequireObjectivVehicle )
						AND IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE(iDialogueProgress)
						OR NOT IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset, iBS_DialogueRequireObjectivVehicle )
							
							INT iCurrentPoints = (MC_serverBD.iTeamScore[iTeam] - iOldDialoguePoints[iTeam])
							
							IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset3, iBS_Dialogue3_UseActualScore)
								iCurrentPoints = MC_serverBD.iTeamScore[iTeam]
							ENDIF
							
							// If we have a radius check
							IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].fRadius != 0.0
								IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam = MC_playerBD[iPartToUse].iteam
								OR g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam = ciDIALOGUE_TRIGGER_TEAM_ANY
									PED_INDEX tempped = PlayerPedToUse
									IF NOT IS_PED_INJURED( tempped )
										IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialoguePedRange)
											IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPedVoice != -1 
												NETWORK_INDEX netPed = MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPedVoice]
												IF NOT IS_NET_PED_INJURED(netPed)
													
													BOOL bOutsideRadiusSetting = IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset3, iBS_Dialogue3_FireWhenPlayerOutsideRadius)
													BOOL bPlayerWithinRadius = (GET_DISTANCE_BETWEEN_ENTITIES(tempped,NET_TO_ENT(netPed)) <= g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].fRadius)
													
													PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - distance to ped ",g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPedVoice," = ",GET_DISTANCE_BETWEEN_ENTITIES(tempped,NET_TO_ENT(netPed)),", fRadius ",g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].fRadius,", bOutsideRadiusSetting ",bOutsideRadiusSetting)
													
													IF ((NOT bOutsideRadiusSetting) AND bPlayerWithinRadius)
													OR (bOutsideRadiusSetting AND (NOT bPlayerWithinRadius))
														MC_playerBD[iLocalPart].iNearDialogueTrigger = iDialogueProgress
														PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - PED MC_playerBD[iLocalPart].iNearDialogueTrigger = iDialogueProgress. iPartToUse = ",iPartToUse)
													ENDIF
												ENDIF
											ENDIF
										ELSE
											#IF IS_DEBUG_BUILD
												VECTOR vTempPed = GET_ENTITY_COORDS(tempPed)
												VECTOR vTemptrig = MC_GET_DIALOGUE_TRIGGER_POSITION(iDialogueProgress)
												PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Dialogue - PlayerPedToUse = ",NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(PlayerPedToUse)),", player coord ",vTempPed,", dialogue coord ", vTemptrig)
											#ENDIF
											
											BOOL bOutsideRadiusSetting = IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset3, iBS_Dialogue3_FireWhenPlayerOutsideRadius)
											BOOL bPlayerWithinRadius = (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(tempped, MC_GET_DIALOGUE_TRIGGER_POSITION(iDialogueProgress)) <= g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].fRadius)
												
											PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - distance to coord ", GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(tempped, MC_GET_DIALOGUE_TRIGGER_POSITION(iDialogueProgress)),", fRadius ",g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].fRadius,", bOutsideRadiusSetting ",bOutsideRadiusSetting, " bPlayerWithinRadius: ", bPlayerWithinRadius)
											
											IF ((NOT bOutsideRadiusSetting) AND bPlayerWithinRadius)
											OR (bOutsideRadiusSetting AND (NOT bPlayerWithinRadius))
												MC_playerBD[iLocalPart].iNearDialogueTrigger = iDialogueProgress
												PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - MC_playerBD[iLocalPart].iNearDialogueTrigger = iDialogueProgress. iPartToUse = ", iPartToUse)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								// Trigger on midpoint
								IF IS_BIT_SET(g_FMMC_STRUCT.biDialogueTriggerAtMidPoint[iDialogueProgress/32], iDialogueProgress%32)
									IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iRule)//MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam])
										IF IS_BIT_SET(MC_serverBD_4.iDialogueRangeBitset[iDialogueProgress/32], iDialogueProgress%32)
										AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset, iBS_DialogueLocalPlayerInRange) 
										OR MC_playerBD[iPartToUse].iNearDialogueTrigger = iDialogueProgress)
											IF iPointsRequired > 0
												IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iRule)//MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam])
												AND iCurrentPoints >= iPointsRequired
													bReturn = true
													PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Dialogue points midpoint range check bReturn = true")
												ENDIF
											ELSE
												bReturn = true
												PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Dialogue midpoint range check bReturn = true")
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF IS_BIT_SET(MC_serverBD_4.iDialogueRangeBitset[iDialogueProgress/32], iDialogueProgress%32)
									AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset, iBS_DialogueLocalPlayerInRange) 
									OR MC_playerBD[iPartToUse].iNearDialogueTrigger = iDialogueProgress)
										IF iPointsRequired > 0
											IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iRule)//MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam])
											AND iCurrentPoints >= iPointsRequired
												bReturn = true
												PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Dialogue points range check bReturn = true iDialogueProgress = ",iDialogueProgress)
											ENDIF
										ELSE
											bReturn = true
											PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Dialogue range check bReturn = true iDialogueProgress = ",iDialogueProgress)
										ENDIF
									ENDIF
								ENDIF
							ELSE // no range check
								IF IS_BIT_SET(g_FMMC_STRUCT.biDialogueTriggerAtMidPoint[iDialogueProgress/32], iDialogueProgress%32)
									IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iRule)
										IF iPointsRequired > 0
											//PRINTLN("[RCC MISSION][MJM] iCurrentPoints (midpoint) = ",iCurrentPoints," iPointsRequired = ",iPointsRequired)
											IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iRule)
											AND iCurrentPoints >= iPointsRequired
												PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Points Target Met.")
												bReturn = true
											ENDIF
										ELSE
											PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Points = 0")
											bReturn = true
										ENDIF
									ENDIF
								ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2TriggerOnRule)
									IF iTriggerRule = iRule
									AND NOT bIgnoreRule
										PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - iBS_Dialogue2TriggerOnRule")
										bReturn = TRUE
									ENDIF
								ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2CheckMinimumSpeed)
									IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed[iRule] < -1
										IF fVehSpeedForAudioTrigger >= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed[iRule]
										OR HAS_EXCEEDED_RULE_ALTITUDE_THRESHOLD(iRule, iTeam, GET_PLAYER_ALTITUDE_FROM_TEAM(iTeam))
											PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Speed check passing")
											PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Current Speed: ", fVehSpeedForAudioTrigger, " Minimum Speed: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed[iRule])
											bReturn = TRUE
										ENDIF
									ELSE
										IF fVehSpeedForAudioTrigger <= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed[iRule]
										OR HAS_EXCEEDED_RULE_ALTITUDE_THRESHOLD(iRule, iTeam, GET_PLAYER_ALTITUDE_FROM_TEAM(iTeam))
											PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Speed check passing")
											PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Current Speed: ", fVehSpeedForAudioTrigger, " Minimum Speed: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed[iRule])
											bReturn = TRUE
										ENDIF
									ENDIF
								ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2InBackOfMOC)
									IF IS_PLAYER_IN_CREATOR_TRAILER(LocalPlayer)
										PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - In back of MOC passing")
										bReturn = TRUE
									ENDIF
								ELSE
									IF iPointsRequired > 0
										//PRINTLN("[RCC MISSION][MJM] iCurrentPoints = ",iCurrentPoints," iPointsRequired = ",iPointsRequired)
										IF (IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iRule)
										OR NOT IS_BIT_SET(g_FMMC_STRUCT.biDialogueTriggerAtMidPoint[iDialogueProgress/32], iDialogueProgress%32))
										AND iCurrentPoints >= iPointsRequired
											PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Points Target Met and On Mid Point.")
											bReturn = true
										ELSE
											IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iRule)
												PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - We're not at the mid point")
											ENDIF
										ENDIF
									ELSE
										IF NOT bIgnoreRule
											PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - NOT bIgnoreRule")
											bReturn = true
										ELSE
											PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - bIgnoreRule is set, so we are ignoring the fact that iCurrentPoints >= iPointsRequired")
										ENDIF
									ENDIF
								ENDIF
							ENDIF // range check								
						ENDIF			
					ENDIF 	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bReturn
		SHOULD_PLAYER_COUGH_ON_THIS_DIALOGUE_LINE(iDialogueProgress)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bDialogueTriggerDebug
		IF bReturn
			TEXT_LABEL_63 tlVisualDebug = "Triggering DT "
			tlVisualDebug += iDialogueProgress
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM | Triggering!!")
			DRAW_DEBUG_TEXT(tlVisualDebug, MC_GET_DIALOGUE_TRIGGER_POSITION(iDialogueProgress), 255, 255, 255, 255)
		ELSE
			TEXT_LABEL_63 tlVisualDebug = "Idle DT "
			tlVisualDebug += iDialogueProgress
			DRAW_DEBUG_TEXT(tlVisualDebug, MC_GET_DIALOGUE_TRIGGER_POSITION(iDialogueProgress), 255, 0, 0, 255)
		ENDIF
	ENDIF
	#ENDIF
	
	RETURN bReturn

ENDFUNC


/// PURPOSE:
///    checks all of the different situations in which dialogue can happen and returns TRUE when valid
///    Some of these situations are:
///    		* Within a range
///			* When a certain score has been reached
///			* On a rule midpoint
///    
FUNC BOOL IS_DIALOGUE_TRIGGER_VALID()
		
	#IF IS_DEBUG_BUILD
	IF FMMC_IS_LONG_BIT_SET(iLocalDialogueForcePlay, iDialogueProgress)		
		RETURN TRUE
	ENDIF	
	#ENDIF
	
	//Check if true for any team
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam = ciDIALOGUE_TRIGGER_TEAM_ANY
		INT iLocalTeam
		
		REPEAT FMMC_MAX_TEAMS iLocalTeam
			IF IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM(iLocalTeam)
				RETURN TRUE
			ENDIF
		ENDREPEAT
		
		RETURN FALSE
	ELSE //Check only the specified team
		RETURN IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam)
	ENDIF
	
ENDFUNC

///PURPOSE: This function returns true if this line of dialogue is meant to be skipped
FUNC BOOL SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED(BOOL &bCanChangeDuringGameplay, INT iDialogueTrigger = -1)
	
	IF iDialogueTrigger = -1
		iDialogueTrigger = iDialogueProgress
	ENDIF
	
	BOOL bReturn = FALSE
	
	#IF IS_DEBUG_BUILD
	IF FMMC_IS_LONG_BIT_SET(iLocalDialogueForcePlay, iDialogueTrigger)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED - Not skipping dialogue. NO FORCING TO PLAY")
		RETURN FALSE
	ENDIF	
	#ENDIF
	
	IF  IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iDialogueBitset, iBS_DialogueSkipOnQuickRestart )
	AND IS_THIS_A_QUICK_RESTART_JOB()
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED - Skipping dialogue. Reason: Quick Restart." )
		bReturn = TRUE
	ENDIF
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iSkipOnRestart > 0
		IF IS_THIS_A_QUICK_RESTART_JOB()
			IF g_TransitionSessionNonResetVars.iTimesRestarted >= g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iSkipOnRestart
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED - Skipping dialogue. Reason: Restarted ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iSkipOnRestart ," or more times." )
				bReturn = TRUE
			ENDIF
		ELSE
			IF g_TransitionSessionNonResetVars.iTimesRestarted > 0
				g_TransitionSessionNonResetVars.iTimesRestarted = 0
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED - Not a quick restart, reseting g_TransitionSessionNonResetVars.iTimesRestarted")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iDialogueBitset2, iBS_Dialogue2SkipOnTripSkip )
	AND MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData.iCost > 0
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED - Skipping dialogue. Reason: Trip Skip." )
		bReturn = TRUE
	ENDIF
	
	//If this should be skipped if we're going to the first check point
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iDialogueBitset2, iBS_Dialogue2SkipOnQuickRestart1)
	AND HAS_MISSION_START_CHECKPOINT_BEEN_SET_OR_PASSED(1)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED - Skipping dialogue. Reason: First Checkpoint." )
		bReturn = TRUE
	ENDIF
	
	//If this should be skipped if we're going to the second check point
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iDialogueBitset2, iBS_Dialogue2SkipOnQuickRestart2)
	AND HAS_MISSION_START_CHECKPOINT_BEEN_SET_OR_PASSED(2)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED - Skipping dialogue. Reason: Second Checkpoint." )
		bReturn = TRUE
	ENDIF
	
	//If this should be skipped if we're going to the second check point
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iDialogueBitset2, iBS_Dialogue2SkipOnQuickRestart3)
	AND HAS_MISSION_START_CHECKPOINT_BEEN_SET_OR_PASSED(3)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED - Skipping dialogue. Reason: Third Checkpoint." )
		bReturn = TRUE
	ENDIF
	
	//If this should be skipped if we're going to the second check point
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iDialogueBitset2, iBS_Dialogue2SkipOnQuickRestart4)
	AND HAS_MISSION_START_CHECKPOINT_BEEN_SET_OR_PASSED(4)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED - Skipping dialogue. Reason: Fourth Checkpoint." )
		bReturn = TRUE
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(iLocalDialogueForceSkip, iDialogueTrigger)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED - Skipping dialogue. Reason: Set as Forced Skip." )
		bReturn = TRUE
	ENDIF
	
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iDialogueTrigger, eSGET_DialogueTrigger, GET_LOCAL_PLAYER_TEAM(TRUE))
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED - Skipping dialogue. Reason: Not a valid spawn group." )
		bReturn = TRUE
		bCanChangeDuringGameplay = TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iDialogueBitset2, iBS_Dialogue2OnlyOnFirstRound)
	AND NOT IS_THIS_THE_FIRST_ROUND_OR_NOT_A_ROUNDS_MISSION()
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED - Skipping dialogue. Reason: iBS_Dialogue2OnlyOnFirstRound." )
		bReturn = TRUE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(MC_serverBD.tdCopSpottedFailTimer)
	AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iDialogueBitset2, iBS_Dialogue2SeenByCops))
	#IF IS_DEBUG_BUILD 
	AND NOT bBlockBranchingOnAggroKeepStealthDialogue
	#ENDIF
		//If the tdCopSpottedFailTimer has begun, then the mission is about to fail - the only dialogue we should play is dialogue that triggers due to this fail type (ie. dialogue with iBS_Dialogue2SeenByCops set)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED - Skipping dialogue. Reason: tdCopSpottedFailTimer + iBS_Dialogue2SeenByCops." )
		bReturn = TRUE
	ENDIF

	IF MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].eSpawnConditionFlag, FALSE, FALSE, g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iAggroIndexBS_Entity_Dialog, MC_PlayerBD[iPartToUse].iTeam)
	AND NOT MC_ARE_SPAWN_CONDITION_FLAGS_MET_DURING_GAMEPLAY(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].eSpawnConditionFlag)		
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueTrigger, "] - SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED - Skipping dialogue. Reason: MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")
		bReturn = TRUE
	ENDIF	
	
	RETURN bReturn
	
ENDFUNC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: PROCESS DIALOGUE  -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to the processing of triggered dialogue -------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//TRUE if dialogue should be heard by your team, or if you should process the checks for other teams
FUNC BOOL SHOULD_TEAM_PROCESS_DIALOGUE_CHECKS()
	
	#IF IS_DEBUG_BUILD
	IF FMMC_IS_LONG_BIT_SET(iLocalDialogueForcePlay, iDialogueProgress)		
		RETURN TRUE
	ENDIF	
	#ENDIF
	
	RETURN NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialogueNotHeardByTeam0 + MC_playerBD[iPartToUse].iteam)
			OR g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam = MC_playerBD[iPartToUse].iteam
			OR g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam = ciDIALOGUE_TRIGGER_TEAM_ANY
ENDFUNC

PROC PROCESS_DIALOGUE_TRIGGER_BLOCKING()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset5, iBS_Dialogue5_BlockWithTriggerBlockerList)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalDialogueBlockedBS[iDialogueProgress/32], iDialogueProgress%32)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - has already been blocked. Exitting process.")
		EXIT
	ENDIF
	
	INT i = 0
	FOR i = 0 TO g_FMMC_STRUCT.iNumberOfDialogueTriggers-1		
		IF IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iBlockingTriggers, i) //Used Externally IS_LONG_BIT_SET
		AND (HAS_DIALOGUE_INDEX_PLAYED(i) OR IS_BIT_SET(iLocalDialoguePlayingBS[i/32],i%32))
		AND g_FMMC_STRUCT.sDialogueTriggers[i].iRule != -1 //B*6196255
			IF NOT IS_BIT_SET(iLocalDialogueBlockedBS[iDialogueProgress/32], iDialogueProgress%32)
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - is now being blocked... due to Dialogue Trigger: ", i)
				SET_BIT(iLocalDialogueBlockedBS[iDialogueProgress/32], iDialogueProgress%32)
				BREAKLOOP
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_PREVIOUS_DIALOGUE()
	If iDialogueProgressLastPlayed > -1
		IF IS_BIT_SET(iLocalDialoguePlayingBS[iDialogueProgressLastPlayed/32],iDialogueProgressLastPlayed%32)
			IF NOT IS_CONVERSATION_STATUS_FREE()
			OR IS_SCRIPTED_CONVERSATION_ONGOING()
			OR (IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgressLastPlayed].iDialogueBitset5, iBS_Dialogue5_PlayFromIslandSpeakers) 
			AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgressLastPlayed].iDialogueBitset8, iBS_Dialogue8_TreatAsStandardDialogue)
			AND IS_ANY_POSITIONAL_SPEECH_PLAYING())
				PRINTLN("[Dialogue Triggers] - PROCESS_PREVIOUS_DIALOGUE - iDialogueProgressLastPlayed: ", iDialogueProgressLastPlayed, " Dialogue ongoing...")
			ELSE
				RESET_NET_TIMER(tdDialogueTriggerObjectiveBlockerFailSafe)
				CLEAR_BIT(iLocalDialoguePlayingBS[iDialogueProgressLastPlayed/32],iDialogueProgressLastPlayed%32)				
				g_bMissionDoNotHangUpFromDialogueHandler = FALSE
				CLEAR_BIT(BitSet_CellphoneDisplay, g_BS_DISABLE_INCOMING_OR_OUTGOING_CALL_HANGUP)
				PRINTLN("[Dialogue Triggers] - PROCESS_PREVIOUS_DIALOGUE - iDialogueProgressLastPlayed: ", iDialogueProgressLastPlayed, " Dialogue Finished. Clearing 'Playing' bit. Clearing: ", g_bMissionDoNotHangUpFromDialogueHandler)
				
				IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgressLastPlayed].iCompletePrereqOnFinish != ciPREREQ_None
					SET_PREREQUISITE_COMPLETED(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgressLastPlayed].iCompletePrereqOnFinish, IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset8, iBS_Dialogue8_CompletePrereqOnFinish_LocalOnly))
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgressLastPlayed].iDialogueBitset7, iBS_Dialogue7_ForcePlayHangUpAfterConversation)
				AND NOT IS_BIT_SET(iLocalDialogueHangUpPlayedBS[iDialogueProgressLastPlayed/32], iDialogueProgressLastPlayed%32)
					PRINTLN("[Dialogue Triggers] - PROCESS_PREVIOUS_DIALOGUE - iDialogueProgressLastPlayed: ", iDialogueProgressLastPlayed, " iBS_Dialogue7_ForcePlayHangUpAfterConversation is on, playing hangup.")
					SET_BIT(iLocalDialogueHangUpPlayedBS[iDialogueProgressLastPlayed/32], iDialogueProgressLastPlayed%32)
					PLAY_SOUND_FRONTEND (-1, "Hang_Up", "Phone_SoundSet_Default")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CURRENT_DIALOGUE()
	
	// Do not remove these helpful debug prints for when Dialogue/Help Text is not playing. Just comment them out.
	// DEBUG_AUDIO_DIALOGUE_TRIGGERS_READOUT(iDialogueProgress) 
		
	IF NOT HAS_DIALOGUE_INDEX_PLAYED(iDialogueProgress)
	OR SHOULD_DIALOGUE_BE_REPLAYABLE(iDialogueProgress)
	
		IF IS_VECTOR_ZERO(MC_GET_DIALOGUE_TRIGGER_POSITION(iDialogueProgress))
			SET_BIT(MC_playerBD[iLocalPart].iDialoguePlayedBS[iDialogueProgress/32],iDialogueProgress%32)
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - Setting MC_playerBD[iPartToUse].iDialoguePlayedBS[",iDialogueProgress/32,"],",iDialogueProgress%32, " Call 3")
			SET_BIT(iLocalDialoguePlayedBS[iDialogueProgress/32],iDialogueProgress%32)
			SET_DIALOGUE_AS_PLAYED_FOR_CONTINUITY(iDialogueProgress)
			//Accidentally loaded a trigger at zero, skip playing it!
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - was loaded with a position of <<0,0,0>> - skipping to next dialogue")
		ELSE			
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset3, iBS_Dialogue3_UsePhoneCall)
				IF GET_INCOMING_CONTACT_CHARACTER_FROM_CREATOR(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iCallCharacter) != MAX_NRM_CHARACTERS_PLUS_DUMMY
				AND GET_INCOMING_CONTACT_CHARACTER_FROM_CREATOR(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iCallCharacter) != CHAR_BLOCKED
					IF NOT IS_CONTACT_IN_PHONEBOOK(GET_INCOMING_CONTACT_CHARACTER_FROM_CREATOR(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iCallCharacter), MULTIPLAYER_BOOK)
						IF NOT IS_BIT_SET(iCharacterAddedToPhoneBook, g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iCallCharacter)
							ADD_CONTACT_TO_PHONEBOOK(GET_INCOMING_CONTACT_CHARACTER_FROM_CREATOR(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iCallCharacter), MULTIPLAYER_BOOK, FALSE)
							SET_BIT(iCharacterAddedToPhoneBook, g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iCallCharacter)
							PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - PROCESS_CURRENT_DIALOGUE - Adding temporary contact to phone book for dialogue trigger ", iDialogueProgress)
						ELSE
							PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - PROCESS_CURRENT_DIALOGUE - Contact not in phone book, but bit has been set.")
							CLEAR_BIT(iCharacterAddedToPhoneBook, g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iCallCharacter)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			BOOL bCanChangeDuringGameplay
			
			//Check if dialogue should be heard by this team:
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iRule != -1
			AND g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam != ciDIALOGUE_TRIGGER_TEAM_OFF
			AND SHOULD_TEAM_PROCESS_DIALOGUE_CHECKS()
			AND NOT SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED(bCanChangeDuringGameplay)
				IF IS_DIALOGUE_TRIGGER_VALID()
					MANAGE_DIALOGUE()
				ENDIF
			ELSE
				IF bCanChangeDuringGameplay
					SET_BIT(iLocalDialogueBlockedTempBS[iDialogueProgress/32],iDialogueProgress%32)
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - PROCESS_CURRENT_DIALOGUE - being flagged as 'BlockedTemp'. A gameplay event can flag it as playable again.")
				ELSE
					SET_BIT(iLocalDialogueSkippedBS[iDialogueProgress/32],iDialogueProgress%32)
					PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - PROCESS_CURRENT_DIALOGUE - being flagged as 'Skipped'. It cannot play later in the mission.")
				ENDIF
				
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - PROCESS_CURRENT_DIALOGUE - Setting MC_playerBD[iPartToUse].iDialoguePlayedBS[",iDialogueProgress/32,"],",iDialogueProgress%32, " Call 4")
				
				SET_BIT(MC_playerBD[iLocalPart].iDialoguePlayedBS[iDialogueProgress/32], iDialogueProgress%32)
				SET_BIT(iLocalDialoguePlayedBS[iDialogueProgress/32], iDialogueProgress%32)				
				SET_DIALOGUE_AS_PLAYED_FOR_CONTINUITY(iDialogueProgress)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam)
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "] - PROCESS_CURRENT_DIALOGUE - Resetting MC_serverBD_4.iGangChasePedsKilledThisRule to 0.")
		MC_serverBD_4.iGangChasePedsKilledThisRule = 0
	ENDIF
	
	IF iDialogueObjectiveObjectDroppedAtStaggeredIndex = iDialogueProgress
		iDialogueObjectiveObjectDroppedBS = 0
		iDialogueObjectiveObjectDroppedAtStaggeredIndex = -1
		PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDialogueProgress, "][Dialogue Triggers][Objects] - PROCESS_CURRENT_DIALOGUE - Clearing iDialogueObjectiveObjectDroppedBS now due to reaching iDialogueObjectiveObjectDroppedAtStaggeredIndex")
	ENDIF
	
ENDPROC

PROC DO_INSTANT_DIALOGUE_LOOP()
		
	INT iCondition
	INT iTrigger
	FOR iCondition = 0 TO ciDialogueInstantLoopCondition_MAX - 1
		IF IS_BIT_SET(ciDialogueInstantLoopConditionBS, iCondition)
			FOR iTrigger = 0 TO ciMAX_DIALOGUE_TRIGGERS_PER_INSTANT_LOOP_CONDITION - 1
				IF iDialogueTriggersWithInstantLoopCondition[iCondition][iTrigger] != -1
					iDialogueProgress = iDialogueTriggersWithInstantLoopCondition[iCondition][iTrigger]
					PROCESS_CURRENT_DIALOGUE()
				ENDIF
			ENDFOR
		ENDIF
	ENDFOR
	
	// this may cause stalls in processing the main loop
	// suggest caching the original value before processing the above loop and restoring here if so
	iDialogueProgress = 0
	ciDialogueInstantLoopConditionBS = 0	
	
ENDPROC

FUNC BOOL IS_INSTANT_DIALOGUE_LOOP_CONDITION_SET(FMMCDialogueTriggersStruct &sDT, INT iInstantLoopCondition)
	SWITCH iInstantLoopCondition
		CASE ciDialogueInstantLoopCondition_AirStrikeUnavailable 
			IF IS_BIT_SET(sDT.iDialogueBitset7, iBS_Dialogue7_AirStrikeUnavailable)
				RETURN TRUE
			ENDIF
		BREAK
		CASE ciDialogueInstantLoopCondition_AirStrikeVehicleDestroyed 
			IF IS_BIT_SET(sDT.iDialogueBitset7, iBS_Dialogue7_AirStrikeVehicleDestroyed)
				RETURN TRUE
			ENDIF
		BREAK
		CASE ciDialogueInstantLoopCondition_HeliBackupUnavailable 
			IF IS_BIT_SET(sDT.iDialogueBitset7, iBS_Dialogue7_HeliBackupUnavailable)
				RETURN TRUE
			ENDIF
		BREAK
		CASE ciDialogueInstantLoopCondition_SupportSniperUnavailable 
			IF IS_BIT_SET(sDT.iDialogueBitset7, iBS_Dialogue7_SupportSniperUnavailable)
				RETURN TRUE
			ENDIF
		BREAK
		CASE ciDialogueInstantLoopCondition_VehicleHealthThreshold 
			IF sDT.iVehicleTrigger != -1
			AND sDT.iVehicleDamage != 0
				RETURN TRUE
			ENDIF
		BREAK
		CASE ciDialogueInstantLoopCondition_InvalidPlayerAbilityActivated
			IF IS_BIT_SET(sDT.iDialogueBitset6, iBS_Dialogue6_AbilityUnavailable)
				RETURN TRUE
			ENDIF
		BREAK
		CASE ciDialogueInstantLoopCondition_SniperAiming
			IF IS_BIT_SET(sDT.iDialogueBitset6, iBS_Dialogue6_SniperAiming)
				RETURN TRUE
			ENDIF
		BREAK
		CASE ciDialogueInstantLoopCondition_CrashedIntoEnemyVehicle
			IF IS_BIT_SET(sDT.iDialogueBitset7, iBS_Dialogue7_TriggerOnCrashIntoEnemyVehicle)
				RETURN TRUE
			ENDIF
		BREAK
		CASE ciDialogueInstantLoopCondition_CrashedIntoAnEntity
			IF  sDT.iVehicleTrigger != -1
			AND sDT.iVehicleDamage  != -1
			AND IS_BIT_SET(sDT.iDialogueBitset8, iBS_Dialogue8_UseThresholdOrEntityDamage)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC SET_UP_INSTANT_DIALOGUE_LOOP()
	
	INT iCurrentIndexForThisCondition[ciDialogueInstantLoopCondition_MAX]
	INT iDTIndex //Dialogue trigger index
	INT iILCIndex //Instant loop condition index
	
	IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_REFRESH_INSTANT_DIALOGUE_LOOP)
		PRINTLN("[Dialogue Triggers] SET_UP_INSTANT_DIALOGUE_LOOP - Mid-mission refresh requested")				
		CLEAR_BIT(iLocalBoolCheck29, LBOOL29_REFRESH_INSTANT_DIALOGUE_LOOP)
	ENDIF
	
	FOR iILCIndex = 0 TO ciDialogueInstantLoopCondition_MAX - 1			
		INITIALISE_INT_ARRAY(iDialogueTriggersWithInstantLoopCondition[iILCIndex], -1)
	ENDFOR
	
	//Check each dialogue trigger for each instant loop condition and record in iDialogueTriggersWithInstantLoopCondition
	FOR iDTIndex = 0 TO FMMC_MAX_DIALOGUES - 1		
		FOR iILCIndex = 0 TO ciDialogueInstantLoopCondition_MAX - 1			
			IF NOT IS_INSTANT_DIALOGUE_LOOP_CONDITION_SET(g_FMMC_STRUCT.sDialogueTriggers[iDTIndex], iILCIndex)
				RELOOP
			ENDIF
			
			BOOL bTempBool = FALSE //Used for function below - don't care for the result
			IF SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED(bTempBool, iDTIndex)
				RELOOP
			ENDIF
			
			IF iCurrentIndexForThisCondition[iILCIndex] >= ciMAX_DIALOGUE_TRIGGERS_PER_INSTANT_LOOP_CONDITION
				ASSERTLN("[Dialogue Triggers][Dialogue Trigger ", iDTIndex, "] SET_UP_INSTANT_DIALOGUE_LOOP - ciMAX_DIALOGUE_TRIGGERS_PER_INSTANT_LOOP_CONDITION (", ciMAX_DIALOGUE_TRIGGERS_PER_INSTANT_LOOP_CONDITION,
				") exceeded for type: ", iILCIndex, ", please add a bug for online tools.")
				PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDTIndex, "] SET_UP_INSTANT_DIALOGUE_LOOP - ciMAX_DIALOGUE_TRIGGERS_PER_INSTANT_LOOP_CONDITION (", ciMAX_DIALOGUE_TRIGGERS_PER_INSTANT_LOOP_CONDITION,
				") exceeded for type: ", iILCIndex, ", please add a bug for online tools.")
				BREAKLOOP
			ENDIF
			
			PRINTLN("[Dialogue Triggers][Dialogue Trigger ", iDTIndex, "] SET_UP_INSTANT_DIALOGUE_LOOP - Assigning Instant Dialogue Trigger - iDTIndex: ", iDTIndex, " iILCIndex: ", iILCIndex, " iCurrentIndexForThisCondition: ", iCurrentIndexForThisCondition[iILCIndex])				
			iDialogueTriggersWithInstantLoopCondition[iILCIndex][iCurrentIndexForThisCondition[iILCIndex]] = iDTIndex
			iCurrentIndexForThisCondition[iILCIndex]++
				
		ENDFOR
	ENDFOR
	
ENDPROC

// [FIX_2020_CONTROLLER] - Add a Log Tag for anything related to dialogue triggers
PROC PROCESS_DIALOGUE_TRIGGERS()
	
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_DIALOGUE)
	#ENDIF

	// If we have any dialogue to play, loop through all triggers to see if any are valid and play it
	IF g_FMMC_STRUCT.iNumberOfDialogueTriggers > 0		
		
		IF HAS_NET_TIMER_EXPIRED(RestartConvTimer,iRestartConvTime)
			
			IF HAS_NET_TIMER_STARTED(MC_serverBD.tdCopSpottedFailTimer)
			AND NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_DONE_DIALOGUE_LOOP_AFTER_COPS_SPOTTED)
				PRINTLN("[Dialogue Triggers] - PROCESS_DIALOGUE_TRIGGERS - Performing a dialogue loop as we've been spotted by the cops!")
				SET_BIT(iLocalBoolCheck9,LBOOL9_DO_INSTANT_DIALOGUE_LOOP)
				SET_BIT(iLocalBoolCheck13, LBOOL13_DONE_DIALOGUE_LOOP_AFTER_COPS_SPOTTED)
			ENDIF
			
			INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
			
			//If new rule, do one frame loop through all dialogue triggers once
			IF iRule < FMMC_MAX_RULES
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetThree[iRule], ciBS_RULE3_LOOP_ALL_DIALOGUE_TRIGGERS)
				AND iLocalDialoguePriority != iRule
					
					PRINTLN("[Dialogue Triggers] - PROCESS_DIALOGUE_TRIGGERS - Dialogue rule change ",iLocalDialoguePriority," to ",iRule," - Looping through all dialogue triggers")
					
					REPEAT g_FMMC_STRUCT.iNumberOfDialogueTriggers iDialogueProgress						
						PROCESS_PREVIOUS_DIALOGUE()
						PROCESS_CURRENT_DIALOGUE()						
					ENDREPEAT
					
					iLocalDialoguePriority = iRule
					iDialogueProgress = 0
					
				//Added to prevent big gaps in dialogue after interrupting
				ELIF IS_BIT_SET(iLocalBoolCheck9,LBOOL9_DO_INSTANT_DIALOGUE_LOOP)
					
					PRINTLN("[Dialogue Triggers] - PROCESS_DIALOGUE_TRIGGERS - LBOOL9_DO_INSTANT_DIALOGUE_LOOP is set - Looping through all dialogue triggers")
					
					REPEAT g_FMMC_STRUCT.iNumberOfDialogueTriggers iDialogueProgress
						PROCESS_PREVIOUS_DIALOGUE()
						PROCESS_CURRENT_DIALOGUE()						
					ENDREPEAT
					
					iDialogueProgress = 0
					CLEAR_BIT(iLocalBoolCheck9,LBOOL9_DO_INSTANT_DIALOGUE_LOOP)
				
				// Added as alternative to the instant loop processing above
				ELIF ciDialogueInstantLoopConditionBS != 0
					PRINTLN("[Dialogue Triggers] - PROCESS_DIALOGUE_TRIGGERS - ciDialogueInstantLoopConditionBS is set - doing instant dialogue loop")					
					DO_INSTANT_DIALOGUE_LOOP()					
	
				ELSE //Otherwise stagger
					
					IF iDialogueProgress < g_FMMC_STRUCT.iNumberOfDialogueTriggers
						PROCESS_PREVIOUS_DIALOGUE()
						PROCESS_CURRENT_DIALOGUE()						
						PROCESS_DIALOGUE_TRIGGER_BLOCKING()
					ELSE
						//Loop back around
						iDialogueProgress = -1
					ENDIF
					
					//Move onto next dialogue for next frame
					iDialogueProgress++
					
					// [LM] Check every frame dialogues that have a delay started OR that have called TRIGGER_DIALOGUE() and is therefore pending.
					// This should expedite the processing of Dialogue Triggers and eliminate 5-7 second iterator related delays we are experiencing.
					// The delays are a result of having 10-15 fps and having to iterate through 60 triggers. 1 second creator assigned delays can become up to 7.
					// We don't care to revalidate as the Dialogue was valid when we triggered it (or started the delay).					
					IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_EXPEDITE_DIALOGUE_TRIGGERS_ENABLED)
						INT iDiag
						iDialogueProgressCached = iDialogueProgress
						REPEAT g_FMMC_STRUCT.iNumberOfDialogueTriggers iDiag
							IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDiag].iDialogueBitset3, iBS_Dialogue3_ExpediteAndDontRevalidate)
							OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_EXPEDITE_AND_DONT_REVALIDATE_DIAG_TRIGGERS)						
								IF NOT HAS_DIALOGUE_INDEX_PLAYED(iDiag)
									IF (g_FMMC_STRUCT.sDialogueTriggers[iDiag].iTimeDelay > 0 AND HAS_NET_TIMER_STARTED(tdDialogueTimer[iDiag]))
									OR IS_BIT_SET(iLocalDialoguePending[iDiag/32],iDiag%32)
										iDialogueProgress = iDiag
										
										BOOL bCheckTimer 
										IF HAS_NET_TIMER_STARTED(tdDialogueTimer[iDiag])
											PRINTLN("[Dialogue Triggers][EXPEDITED] - iDialogueProgress: ", iDialogueProgress, " Timer has started.")
											bCheckTimer = CHECK_DIALOGUE_TIMER()
										ENDIF
										
										IF IS_BIT_SET(iLocalDialoguePending[iDiag/32],iDiag%32)
										OR bCheckTimer
											PRINTLN("[Dialogue Triggers][EXPEDITED] - Expedited Dialogue Trigger Processing - iDialogueProgress: ", iDialogueProgress, " iLocalDialoguePending: ", IS_BIT_SET(iLocalDialoguePending[iDiag/32],iDiag%32), " bCheckTimer: ", bCheckTimer)
											MANAGE_DIALOGUE()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
						iDialogueProgress = iDialogueProgressCached
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_REFRESH_INSTANT_DIALOGUE_LOOP)
			SET_UP_INSTANT_DIALOGUE_LOOP()
		ENDIF
		
		MANAGE_OBJECTIVE_HELP_TEXT_REPRINT()
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_DIALOGUE)
	#ENDIF
	
ENDPROC

//Set dialogue bs = spectator target if spectating
PROC MAINTAIN_SPECTATOR_DIALOGUE()

	IF iLocalSpectateTarget != iSpectatorTarget
		iLocalSpectateTarget = iSpectatorTarget
		PRINTLN("[MJM] MAINTAIN_SPECTATOR_DIALOGUE - Switching to use dialogue for player: ",iSpectatorTarget)
	ENDIF

ENDPROC
