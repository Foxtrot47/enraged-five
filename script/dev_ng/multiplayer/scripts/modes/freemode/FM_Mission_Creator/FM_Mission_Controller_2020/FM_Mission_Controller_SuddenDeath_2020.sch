// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Sudden Death ------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: All logic regarding Sudden Death used to resolve ties in versus/adversary modes.                                                                                                                              
// ##### NOTE: None of this logic is currently used in any content using FM_Mission_Controller_2020 and this requires a refactor when it is eventually used.                                                                                                                           
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020                                                                                       
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Include_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Sudden Death ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_SUDDEN_DEATH_TARGET()
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - PROCESS_SUDDEN_DEATH_TARGET")

	FLOAT fOffset = 10.0
	INT iWidth = 8
	FLOAT fGap = 2.0
	
	INT iNumPlayers
	VECTOR vOffset
	VECTOR vPlayerPlace
	VECTOR vGround
	INT iSubLoop
	FLOAT fColumn
	FLOAT fRowDistance
	INT iLoop
	
	#IF IS_DEBUG_BUILD
	VECTOR vTemp
	#ENDIF
	
	IF NOT DOES_ENTITY_EXIST(objSuddenDeathTarget)
		REQUEST_MODEL(prop_basejump_target_01)
		
		IF HAS_MODEL_LOADED(prop_basejump_target_01)
			objSuddenDeathTarget = CREATE_OBJECT(prop_basejump_target_01, GET_ENTITY_COORDS(oiProps[iSuddenDeathTargetProp]), FALSE)
			
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(objSuddenDeathTarget, TRUE)
			
			ATTACH_ENTITY_TO_ENTITY(objSuddenDeathTarget, oiProps[iSuddenDeathTargetProp], 0, vSuddenDeathTargetCentre, <<90.0, 0.0, 90.0>>)
		ENDIF
	ENDIF
	
	HUD_COLOURS TargetScoreColour[8]
	INT iTeamTarget
	INT iScoreLoop
	
	WEAPON_TYPE wpCurrent
	
	INT iSuddenDeathTargetTeamHit[FMMC_MAX_TEAMS]
	INT iSuddenDeathTargetTeamMissed[FMMC_MAX_TEAMS]
	
	INT iParticipant
	
	FOR iParticipant = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
		IF iParticipant < MAX_NUM_MC_PLAYERS
			PARTICIPANT_INDEX currentParticipant = INT_TO_PARTICIPANTINDEX(iParticipant)
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(currentParticipant)
				IF NOT IS_BIT_SET(MC_playerBD[NATIVE_TO_INT(currentParticipant)].iClientBitSet, PBBOOL_ANY_SPECTATOR)
					IF IS_BIT_SET(MC_playerBD[NATIVE_TO_INT(currentParticipant)].iClientBitSet3, PBBOOL3_RUGBY_GOAL_TARGET_MISSED)
						iSuddenDeathTargetTeamMissed[MC_playerBD[NATIVE_TO_INT(currentParticipant)].iTeam]++
					ENDIF
					IF IS_BIT_SET(MC_playerBD[NATIVE_TO_INT(currentParticipant)].iClientBitSet3, PBBOOL3_RUGBY_GOAL_TARGET_HIT)
						iSuddenDeathTargetTeamHit[MC_playerBD[NATIVE_TO_INT(currentParticipant)].iTeam]++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	SWITCH iSuddenDeathTargetInitStage
		CASE 0
			PRINTLN("[SUDDEN_DEATH_TARGET] Init Stage")
			
			iNumPlayers = g_FMMC_STRUCT.iNumParticipants + 1
			
			vOffset = <<fOffset, 0.0, 5.0>>
			
			FOR iLoop = 0 TO MC_playerBD[iPartToUse].iTeam
				iSubLoop += MC_serverBD.iNumberOfPlayingPlayers[MC_playerBD[iPartToUse].iTeam]
			ENDFOR
			
			iSubLoop += GET_PARTICIPANT_NUMBER_IN_TEAM()
			
			PRINTLN("iSubLoop = ", iSubLoop)
			
			IF iSubLoop >= iNumPlayers - (iNumPlayers % iWidth)
				fColumn = vOffset.Y - (((iNumPlayers % iWidth) / 2) * fGap) + ((iSubLoop % iWidth) * fGap) + ((1 - ((iNumPlayers % iWidth) % 2)) * (fGap / 2))
			ELSE
				fColumn = vOffset.Y - ((iWidth / 2) * fGap) + ((iSubLoop % iWidth) * fGap) + ((1 - (iWidth % 2)) * (fGap / 2))
			ENDIF
			PRINTLN("fColumn = ", fColumn)
			fRowDistance = vOffset.X + (FLOOR(TO_FLOAT(iSubLoop) / TO_FLOAT(iWidth)) * fGap)
			PRINTLN("fRowDistance = ", fRowDistance)
			vPlayerPlace = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiProps[iSuddenDeathTargetProp], <<fRowDistance, fColumn, vOffset.Z>>)
			PRINTLN("vPlayerPlace = ", vPlayerPlace)
			vGround = vPlayerPlace
			
			GET_GROUND_Z_FOR_3D_COORD(vPlayerPlace, vGround.Z, TRUE)
			PRINTLN("vGround = ", vGround)
			
			SETUP_SPECIFIC_SPAWN_LOCATION(vGround, GET_HEADING_BETWEEN_VECTORS(vGround, GET_ENTITY_COORDS(oiProps[iSuddenDeathTargetProp])))
			
			iSuddenDeathTargetInitStage++
		BREAK
		CASE 1
			//Disable Player Movement
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UP_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LEFT_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_RIGHT_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_DOWN_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
			
			GET_CURRENT_PED_WEAPON(localPlayerPed, wpCurrent, FALSE)
			
			IF wpCurrent != WEAPONTYPE_PISTOL
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
			ENDIF
			
			IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_AT_SPECIFIC_COORDS)
				REMOVE_ALL_PED_WEAPONS(LocalPlayerPed)
				
				IF NOT HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_PISTOL)
					GIVE_WEAPON_TO_PED(LocalPlayerPed, WEAPONTYPE_PISTOL, 1, TRUE)
				ENDIF
				
				IF GET_AMMO_IN_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_PISTOL) != 1
					SET_PED_AMMO(LocalPlayerPed, WEAPONTYPE_PISTOL, 1)
				ENDIF
				
				SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_PISTOL, TRUE)
				
				SET_ENTITY_PROOFS(LocalPlayerPed, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
				
				FOR iTeamTarget = 0 TO (FMMC_MAX_TEAMS - 1)
					SET_PED_CAN_BE_TARGETTED_BY_TEAM(LocalPlayerPed, iTeamTarget, FALSE)
				ENDFOR
				
				iSuddenDeathTargetInitStage++
			ENDIF
		BREAK
		CASE 2
			//Disable Player Movement
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UP_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LEFT_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_RIGHT_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_DOWN_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
			
			GET_CURRENT_PED_WEAPON(localPlayerPed, wpCurrent, FALSE)
			
			IF wpCurrent != WEAPONTYPE_PISTOL
			OR IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
			OR IS_BIG_MESSAGE_BEING_DRAWN()
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
			ENDIF
			
			FOR iTeamTarget = 0 TO (FMMC_MAX_TEAMS - 1)
				FOR iScoreLoop = 0 TO 7
					IF iScoreLoop < iSuddenDeathTargetTeamHit[iTeamTarget]
						TargetScoreColour[iScoreLoop] = HUD_COLOUR_GREEN
					ELIF iScoreLoop < iSuddenDeathTargetTeamHit[iTeamTarget] + iSuddenDeathTargetTeamMissed[iTeamTarget]
						TargetScoreColour[iScoreLoop] = HUD_COLOUR_RED
					ELSE
						TargetScoreColour[iScoreLoop] = HUD_COLOUR_BLACK
					ENDIF
				ENDFOR
				
				DRAW_ONE_PACKAGES_EIGHT_HUD(MC_serverBD.iNumberOfPlayingPlayers[iTeamTarget], TEXT_LABEL_TO_STRING(g_sMission_TeamName[iTeamTarget]), FALSE, TargetScoreColour[0], TargetScoreColour[1], TargetScoreColour[2], TargetScoreColour[3], TargetScoreColour[4], TargetScoreColour[5], TargetScoreColour[6],TargetScoreColour[7], -1, GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamTarget, PlayerToUse), DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_SEVENTHBOTTOM, TRUE)
			ENDFOR
			
			IF DOES_ENTITY_EXIST(objSuddenDeathTarget)
				PRINTLN("[SUDDEN_DEATH_TARGET] Move Target")
				
				SWITCH iSuddenDeathTargetMoveStyle
					CASE 0
						vSuddenDeathTargetOffset += vSuddenDeathTargetVelocity * TIMESTEP()
						
						IF vSuddenDeathTargetOffset.Y < 0.0 - vSuddenDeathTargetBounds.Y
						OR vSuddenDeathTargetOffset.Y > vSuddenDeathTargetBounds.Y
							vSuddenDeathTargetVelocity.Y = 0.0 - vSuddenDeathTargetVelocity.Y
						ENDIF
						
						IF vSuddenDeathTargetOffset.Z < 0.0 - vSuddenDeathTargetBounds.Z
						OR vSuddenDeathTargetOffset.Z > vSuddenDeathTargetBounds.Z
							vSuddenDeathTargetVelocity.Z = 0.0 - vSuddenDeathTargetVelocity.Z
						ENDIF
						
						IF vSuddenDeathTargetOffset.Y < 0.0 - vSuddenDeathTargetBounds.Y
							vSuddenDeathTargetOffset.Y = 0.0 - vSuddenDeathTargetBounds.Y
						ELIF vSuddenDeathTargetOffset.Y > vSuddenDeathTargetBounds.Y
							vSuddenDeathTargetOffset.Y = vSuddenDeathTargetBounds.Y
						ENDIF
						
						IF vSuddenDeathTargetOffset.Z < 0.0 - vSuddenDeathTargetBounds.Z
							vSuddenDeathTargetOffset.Z = 0.0 - vSuddenDeathTargetBounds.Z
						ELIF vSuddenDeathTargetOffset.Z > vSuddenDeathTargetBounds.Z
							vSuddenDeathTargetOffset.Z = vSuddenDeathTargetBounds.Z
						ENDIF
						
						PRINTLN("vTargetVelocity = ", vSuddenDeathTargetVelocity)
					BREAK
					CASE 1
						vSuddenDeathTargetOffset.Y = SIN(GET_GAME_TIMER() * fSuddenDeathTargetSinCosSpeed) * fSuddenDeathTargetSinCosScale
						vSuddenDeathTargetOffset.Z = COS(GET_GAME_TIMER() * fSuddenDeathTargetSinCosSpeed) * fSuddenDeathTargetSinCosScale
					BREAK
				ENDSWITCH
				
				ATTACH_ENTITY_TO_ENTITY(objSuddenDeathTarget, oiProps[iSuddenDeathTargetProp], 0, vSuddenDeathTargetCentre + vSuddenDeathTargetOffset, <<90.0, 0.0, 90.0>>)
				PRINTLN("vTargetOffset = ", vSuddenDeathTargetOffset)
				
				#IF IS_DEBUG_BUILD
					vTemp = GET_ENTITY_COORDS(objSuddenDeathTarget)
					PRINTLN("objSuddenDeathTarget = ", vTemp)
				#ENDIF
				
				IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet3, PBBOOL3_RUGBY_GOAL_TARGET_HIT)
				AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet3, PBBOOL3_RUGBY_GOAL_TARGET_MISSED)
					IF HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_PISTOL)
						IF GET_AMMO_IN_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_PISTOL) = 0
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(objSuddenDeathTarget, LocalPlayerPed, FALSE)
								
								SET_BIT(MC_playerBD[iPartToUse].iClientBitSet3, PBBOOL3_RUGBY_GOAL_TARGET_HIT)
								
								CLEAR_ENTITY_LAST_DAMAGE_ENTITY(objSuddenDeathTarget)
							ELSE
								SET_BIT(MC_playerBD[iPartToUse].iClientBitSet3, PBBOOL3_RUGBY_GOAL_TARGET_MISSED)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF iSuddenDeathTargetRoundLast != MC_serverBD_3.iSuddenDeathTargetRound
					CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet3, PBBOOL3_RUGBY_GOAL_TARGET_HIT)
					CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet3, PBBOOL3_RUGBY_GOAL_TARGET_MISSED)
					
					IF NOT HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_PISTOL)
						GIVE_WEAPON_TO_PED(LocalPlayerPed, WEAPONTYPE_PISTOL, 1, TRUE)
					ENDIF
					
					IF GET_AMMO_IN_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_PISTOL) != 1
						SET_PED_AMMO(LocalPlayerPed, WEAPONTYPE_PISTOL, 1)
					ENDIF
					
					SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_PISTOL, TRUE)
					
					iSuddenDeathTargetRoundLast = MC_serverBD_3.iSuddenDeathTargetRound
					
					IF ABSF(vSuddenDeathTargetVelocity.Y) < 10.0 AND ABSF(vSuddenDeathTargetVelocity.Z) < 10.0
						IF vSuddenDeathTargetVelocity.Y < 0
							vSuddenDeathTargetVelocity.Y = 0 - (vSuddenDeathTargetInitialVelocity.Y * (1.0 + (CLAMP_INT(iSuddenDeathTargetRoundLast, 0, 10) / 10.0)))
						ELSE
							vSuddenDeathTargetVelocity.Y = vSuddenDeathTargetInitialVelocity.Y * (1.0 + (CLAMP_INT(iSuddenDeathTargetRoundLast, 0, 10) / 10.0))
						ENDIF
						
						IF vSuddenDeathTargetVelocity.Z < 0
							vSuddenDeathTargetVelocity.Z = 0 - (vSuddenDeathTargetInitialVelocity.Z * (1.0 + (CLAMP_INT(iSuddenDeathTargetRoundLast, 0, 10) / 10.0)))
						ELSE
							vSuddenDeathTargetVelocity.Z = vSuddenDeathTargetInitialVelocity.Z * (1.0 + (CLAMP_INT(iSuddenDeathTargetRoundLast, 0, 10) / 10.0))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL DOES_ANY_PLAYER_HOLD_A_PACKAGE()
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - DOES_ANY_PLAYER_HOLD_A_PACKAGE")

	INT iTeam, iPlayer
	INT iPlayerIndex = 0
	FOR iTeam = 0 TO (MC_serverBD.iNumActiveTeams-1)
		FOR iPlayer = 0 TO (MC_serverBD.iNumberOfPlayingPlayers[iTeam]-1)
			
			IF iPlayerIndex < MAX_NUM_MC_PLAYERS
			AND iTeam < FMMC_MAX_TEAMS
				PRINTLN("DOES_ANY_PLAYER_HOLD_A_PACKAGE - Team arrayed - MC_serverBD.iNumObjHighestPriorityHeld[",iTeam,"] = ", MC_serverBD.iNumObjHighestPriorityHeld[ iTeam ])
				IF MC_serverBD.iNumObjHighestPriorityHeld[ iTeam ] > 0
				
					#IF IS_DEBUG_BUILD
						IF MC_serverBD.iNumObjHighestPriorityHeld[ iTeam ] > 0
							PRINTLN("DOES_ANY_PLAYER_HOLD_A_PACKAGE - team ", iTeam, " holds the package.")
						ENDIF
					#ENDIF
					RETURN TRUE
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("DOES_ANY_PLAYER_HOLD_A_PACKAGE - ", iPlayerIndex, " >= ", MAX_NUM_MC_PLAYERS)
			#ENDIF
			ENDIF
			iPlayerIndex++
		ENDFOR
	ENDFOR
	
	PRINTLN("DOES_ANY_PLAYER_HOLD_A_PACKAGE - Ball is NOT being carried!")
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks to see if there are multiple teams that can move to sudden death
/// RETURNS:
///    TRUE if you have the highest score or same as the another team
FUNC BOOL MULTIPLE_TEAMS_ON_SAME_SCORE(INT &iTeamInLead, BOOL bFailTeamOnLose = TRUE)

	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - MULTIPLE_TEAMS_ON_SAME_SCORE")
	
	INT iTeam
	BOOL bMultipleTeamsOnScore = FALSE
	INT iTeamsOnSameScore = 0
	INT iHighestScore = 0

	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_TEAM_ACTIVE(iTeam)
			IF MC_serverBD.iTeamScore[iTeam] > iHighestScore
				iHighestScore = MC_serverBD.iTeamScore[iTeam]
				iTeamInLead = iTeam
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("[KH] MULTIPLE_TEAMS_ON_SAME_SCORE - HIGHEST SCORE: ", iHighestScore)

	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_TEAM_ACTIVE(iTeam)
			IF MC_serverBD.iTeamScore[iTeam] = iHighestScore
				PRINTLN("[KH] MULTIPLE_TEAMS_ON_SAME_SCORE - TEAM: ", iTeam," has the highest score")
				iTeamsOnSameScore++
			ELSE
				IF bFailTeamOnLose
					PRINTLN("[KH] MULTIPLE_TEAMS_ON_SAME_SCORE - TEAM: ", iTeam," has a lower score than the others, failing")
					REQUEST_SET_TEAM_FAILED(iTeam, mFail_none, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF iTeamsOnSameScore >= 2
		bMultipleTeamsOnScore = TRUE
	ENDIF
	
	RETURN bMultipleTeamsOnScore
	
ENDFUNC

FUNC BOOL MULTIPLE_TEAMS_ON_SAME_CAPTURE_PROGRESS(INT &iTeamInLead, BOOL bFailTeamOnLose = TRUE)

	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - MULTIPLE_TEAMS_ON_SAME_CAPTURE_PROGRESS")
	
	INT iTeam
	BOOL bMultipleTeamsOnSameProgress = FALSE
	INT iTeamsOnSameProgress = 0
	INT iHighestProgress = 0
	
	PRINTLN("[JT] MULTIPLE_TEAMS_ON_SAME_CAPTURE_PROGRESS - HIGHEST PROGRESS: ", iHighestProgress)
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_TEAM_ACTIVE(iTeam)
			IF MC_serverBD.iGranularCurrentPoints[iTeam] > iHighestProgress
				iHighestProgress = MC_serverBD.iGranularCurrentPoints[iTeam]
				iTeamInLead = iTeam
				PRINTLN("[JT] MULTIPLE_TEAMS_ON_SAME_CAPTURE_PROGRESS - Team: ", iTeam," is in the lead with progress of: ", iHighestProgress)
			ENDIF
			PRINTLN("[JT] MULTIPLE_TEAMS_ON_SAME_CAPTURE_PROGRESS - highest progress: ", iHighestProgress)
			
		ENDIF
	ENDFOR
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_TEAM_ACTIVE(iTeam)
			IF bFailTeamOnLose
				PRINTLN("[JT] MULTIPLE_TEAMS_ON_SAME_CAPTURE_PROGRESS - Team: ", iTeam," has lower progress than the others, failing")
				REQUEST_SET_TEAM_FAILED(iTeam, mFail_none, FALSE)
			ENDIF
		ENDIF
	ENDFOR
	
	IF iTeamsOnSameProgress >= 2
		bMultipleTeamsOnSameProgress = TRUE
	ENDIF
	
	RETURN bMultipleTeamsOnSameProgress
	
ENDFUNC

/// PURPOSE:
///    Checks to see if there are multiple teams that can move to sudden death
/// RETURNS:
///    TRUE if you have the highest score or same as the another team
FUNC BOOL MULTIPLE_TEAMS_ON_SAME_RULE_SCORE(INT &iTeamInLead, BOOL bFailTeamOnLose = TRUE)
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - MULTIPLE_TEAMS_ON_SAME_RULE_SCORE")
	
	INT iTeam
	BOOL bMultipleTeamsOnScore = FALSE
	INT iTeamsOnSameScore = 0
	INT iHighestScore = 0

	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_TEAM_ACTIVE(iTeam)
			IF MC_serverBD.iScoreOnThisRule[iTeam] > iHighestScore
				iHighestScore = MC_serverBD.iScoreOnThisRule[iTeam]
				iTeamInLead = iTeam
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("[KH] MULTIPLE_TEAMS_ON_SAME_RULE_SCORE - HIGHEST SCORE: ", iHighestScore)

	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_TEAM_ACTIVE(iTeam)
			IF MC_serverBD.iScoreOnThisRule[iTeam] = iHighestScore
				PRINTLN("[KH] MULTIPLE_TEAMS_ON_SAME_RULE_SCORE - TEAM: ", iTeam," has the highest score")
				iTeamsOnSameScore++
			ELSE
				IF bFailTeamOnLose
					PRINTLN("[KH] MULTIPLE_TEAMS_ON_SAME_RULE_SCORE - TEAM: ", iTeam," has a lower score than the others, failing")
					REQUEST_SET_TEAM_FAILED(iTeam, mFail_none, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF iTeamsOnSameScore >= 2
		bMultipleTeamsOnScore = TRUE
	ENDIF
	
	RETURN bMultipleTeamsOnScore
	
ENDFUNC

FUNC INT GET_THE_WINNING_TEAM()

	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - GET_THE_WINNING_TEAM")
	
	INT i
	INT iCurrentHighScore = -1
	INT iRefToHighScorer = -1
	
	FOR i = 0 TO (MC_serverBD.iNumActiveTeams-1)
		IF IS_TEAM_ACTIVE(i)
			PRINTLN("GET_THE_WINNING_TEAM - iteam: ", i, " score: ", MC_serverBD.iTeamScore[i])
			IF MC_serverBD.iTeamScore[i] > iCurrentHighScore
				iCurrentHighScore = MC_serverBD.iTeamScore[i]
				iRefToHighScorer = i
			ENDIF	
		ENDIF
	ENDFOR
	
	PRINTLN("GET_THE_WINNING_TEAM - Returning iRefToHighScorer: ", iRefToHighScorer)
	
	RETURN iRefToHighScorer
ENDFUNC

FUNC INT GET_TEAM_WITH_MOST_KILLS()

	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - GET_TEAM_WITH_MOST_KILLS")

	INT iHighestKillScore, iRefToHighestTeam, iTeam
	
	iHighestKillScore = 0
	iRefToHighestTeam = -1

	FOR iTeam = 0 TO (MC_serverBD.iNumActiveTeams-1)
		IF IS_TEAM_ACTIVE(iTeam)
			PRINTLN("GET_TEAM_WITH_MOST_KILLS - MC_serverBD.iteamKills[",iTeam,"] = ", GET_TEAM_KILLS(iTeam))
			IF GET_TEAM_KILLS(iTeam) > iHighestKillScore
				iHighestKillScore = GET_TEAM_KILLS(iTeam)
				iRefToHighestTeam = iTeam
			ELIF GET_TEAM_KILLS(iTeam) = iHighestKillScore AND iHighestKillScore != 0
				PRINTLN("GET_TEAM_WITH_MOST_KILLS - Exiting, two score teams are equal")
				RETURN -1
			ENDIF
		ENDIF
	ENDFOR

	PRINTLN("GET_TEAM_WITH_MOST_KILLS - Returning ", iRefToHighestTeam)
	RETURN iRefToHighestTeam
	
ENDFUNC

FUNC INT GET_TEAM_WITH_LEAST_DEATHS()

	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - GET_TEAM_WITH_LEAST_DEATHS")
	
	INT iLowestDeathScore, iRefToLowestTeam, iTeam
	
	iLowestDeathScore = 9999
	iRefToLowestTeam = -1

	FOR iTeam = 0 TO (MC_serverBD.iNumActiveTeams-1)
		IF IS_TEAM_ACTIVE(iTeam)
			PRINTLN("GET_TEAM_WITH_LEAST_DEATHS - MC_serverBD.iTeamDeaths[",iTeam,"] = ", GET_TEAM_DEATHS(iTeam))
			IF GET_TEAM_DEATHS(iTeam) < iLowestDeathScore
				iLowestDeathScore = GET_TEAM_DEATHS(iTeam)
				iRefToLowestTeam = iTeam
			ELIF GET_TEAM_DEATHS(iTeam) = iLowestDeathScore
				PRINTLN("GET_TEAM_WITH_LEAST_DEATHS - Exiting, two score teams are equal")
				RETURN -1
			ENDIF
		ENDIF
	ENDFOR

	PRINTLN("GET_TEAM_WITH_LEAST_DEATHS - Returning ", iRefToLowestTeam)
	RETURN iRefToLowestTeam
ENDFUNC

FUNC INT GET_TEAM_WITH_MOST_HEADSHOTS()

	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - GET_TEAM_WITH_MOST_HEADSHOTS")

	INT iHighestHeadshotScore, iRefToHighestTeam, iTeam
	
	iHighestHeadshotScore = 0
	iRefToHighestTeam = -1

	FOR iTeam = 0 TO (MC_serverBD.iNumActiveTeams-1)
		IF IS_TEAM_ACTIVE(iTeam)
			PRINTLN("GET_TEAM_WITH_MOST_HEADSHOTS - MC_serverBD.iteamHeadshots[",iTeam,"] = ", GET_TEAM_HEADSHOTS(iTeam))
			IF GET_TEAM_DEATHS(iTeam) > iHighestHeadshotScore
				iHighestHeadshotScore = GET_TEAM_HEADSHOTS(iTeam)
				iRefToHighestTeam = iTeam
			ELIF GET_TEAM_HEADSHOTS(iTeam) = iHighestHeadshotScore AND iHighestHeadshotScore != 0
				PRINTLN("GET_TEAM_WITH_MOST_HEADSHOTS - Exiting, two score teams are equal")
				RETURN -1
			ENDIF
		ENDIF
	ENDFOR

	PRINTLN("GET_TEAM_WITH_MOST_HEADSHOTS - Returning ", iRefToHighestTeam)
	RETURN iRefToHighestTeam
ENDFUNC

FUNC BOOL ATTACKING_TEAM_HAS_ALL_PACKAGES(INT& iAttackingTeam)
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - ATTACKING_TEAM_HAS_ALL_PACKAGES")
	
	INT iTeam
	BOOL packageHeld

	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF MC_serverBD_4.iObjMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
			iAttackingTeam = iTeam
			
			PRINTLN("[KH] ATTACKING_TEAM_HAS_ALL_PACKAGES - iAttackingTeam ",iAttackingTeam,", iNumObjHighestPriorityHeld ",MC_serverBD.iNumObjHighestPriorityHeld[ iTeam ],", iNumObjHighestPriority ",MC_serverBD.iNumObjHighestPriority[ iTeam ])
			IF MC_serverBD.iNumObjHighestPriorityHeld[ iTeam ] < MC_serverBD.iNumObjHighestPriority[ iTeam ]
				packageHeld = FALSE
			ELSE
				packageHeld = TRUE
			ENDIF
			
			BREAKLOOP
		ENDIF
	ENDFOR
	
	RETURN packageHeld
	
ENDFUNC

FUNC BOOL DOES_ANY_TEAM_HAVE_ALL_ENEMY_TEAMS_DEAD()
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - DOES_ANY_TEAM_HAVE_ALL_ENEMY_TEAMS_DEAD")
	
	BOOL bAllEnemiesDead = TRUE
	INT iTeam, iTeam2
	
	FOR iTeam = 0 TO (MC_serverBD.iNumActiveTeams - 1)
		IF MC_serverBD.iNumberOfPlayingPlayers[iTeam] > 0
			
			IF iTeam < (MC_serverBD.iNumActiveTeams - 1) // Are there still other teams to check:
				FOR iTeam2 = iTeam TO (MC_serverBD.iNumActiveTeams - 1)
					IF MC_serverBD.iNumberOfPlayingPlayers[iTeam2] > 0
					AND NOT DOES_TEAM_LIKE_TEAM(iTeam, iTeam2)
						//An enemy team is still alive!
						bAllEnemiesDead = FALSE
					ENDIF
				ENDFOR
			ENDIF
			
		ENDIF
	ENDFOR
	
	RETURN bAllEnemiesDead
	
ENDFUNC

FUNC BOOL DOES_ANY_TEAM_HAVE_MORE_PLAYERS_IN_LOCATES(INT& iWinningTeam)
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - DOES_ANY_TEAM_HAVE_MORE_PLAYERS_IN_LOCATES")
	
	INT iPeopleInLocate[FMMC_MAX_TEAMS]
	
	
	INT iPart
	INT iPlayersChecked
	INT iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
	
	FOR iPart = 0 TO (MAX_NUM_MC_PLAYERS - 1)
		IF ((NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
				iPlayersChecked++
				
				IF MC_playerBD[iPart].iCurrentLoc != -1
					iPeopleInLocate[MC_playerBD[iPart].iteam]++
				ENDIF
				
				IF iPlayersChecked >= iPlayersToCheck
					iPart = MAX_NUM_MC_PLAYERS // Break out!
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	
	INT iTeam
	INT iCurrentHighest = -1
	INT iNumTeamsWithHighest
	
	FOR iTeam = 0 TO (MC_serverBD.iNumActiveTeams - 1)
		IF IS_TEAM_ACTIVE(iTeam)
			IF iPeopleInLocate[iteam] > iCurrentHighest
				iCurrentHighest = iPeopleInLocate[iteam]
				iWinningTeam = iTeam
				iNumTeamsWithHighest = 1
			ELIF iPeopleInLocate[iteam] = iCurrentHighest
				iNumTeamsWithHighest++
			ENDIF
		ENDIF
	ENDFOR
	
	IF iNumTeamsWithHighest > 1
		iWinningTeam = -1
		RETURN FALSE
	ELSE
		RETURN TRUE
	ENDIF
	
ENDFUNC

///PURPOSE: This function gets the current highest score for any team
///    then checks to see if any other team has the same score
FUNC BOOL ARE_ANY_WINNING_TEAM_SCORES_EQUAL()
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - ARE_ANY_WINNING_TEAM_SCORES_EQUAL")
	
	INT iteam
	
	PRINTLN("[JJT] ARE_ANY_WINNING_TEAM_SCORES_EQUAL - called")
	
	INT iCurrentHighestScore = 0

	FOR iteam = 0 TO (MC_serverBD.iNumActiveTeams-1)
		IF IS_TEAM_ACTIVE(iteam)
			PRINTLN("ARE_ANY_WINNING_TEAM_SCORES_EQUAL - MC_serverBD.iTeamScore[",iteam,"] = ", MC_serverBD.iTeamScore[iteam], " > iCurrentHighestScore = ", iCurrentHighestScore)
			IF MC_serverBD.iTeamScore[iteam] > iCurrentHighestScore
				iCurrentHighestScore = MC_serverBD.iTeamScore[iteam]
			ENDIF	
		ENDIF
	ENDFOR
	
	INT iTotalTeamsWithSameScore = 0
	
	FOR iteam = 0 TO (MC_serverBD.iNumActiveTeams-1)
		IF IS_TEAM_ACTIVE(iteam)
			PRINTLN("ARE_ANY_WINNING_TEAM_SCORES_EQUAL - ", MC_serverBD.iTeamScore[iteam], " = ", iCurrentHighestScore)
			IF MC_serverBD.iTeamScore[iteam] = iCurrentHighestScore
				iTotalTeamsWithSameScore++
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("ARE_ANY_WINNING_TEAM_SCORES_EQUAL - ", iTotalTeamsWithSameScore)

	RETURN iTotalTeamsWithSameScore > 1
ENDFUNC


PROC PROCESS_CTF_SUDDEN_DEATH_LOGIC(INT iTeam, INT iRule, BOOL& bObjectiveOver)
	UNUSED_PARAMETER(iRule)
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - PROCESS_CTF_SUDDEN_DEATH_LOGIC")
	
	IF NOT ARE_ANY_WINNING_TEAM_SCORES_EQUAL()
		#IF IS_DEBUG_BUILD
		PRINTLN("[RCC MISSION] SERVER TIME LIMIT FOR OBJECTIVE EXPIRED - ALL TEAMS NOT EQUAL: ",iRule," TEAM: ",iteam)
		#ENDIF
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TIME_EXPIRED 
		bObjectiveOver = TRUE
	ENDIF
ENDPROC

PROC PROCESS_KILL_ALL_ENEMIES_SUDDEN_DEATH_LOGIC(INT iTeam, INT iRule, BOOL& bObjectiveOver, BOOL& bObjectivePassed)
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - PROCESS_KILL_ALL_ENEMIES_SUDDEN_DEATH_LOGIC")
	
	UNUSED_PARAMETER(iRule)
	IF DOES_ANY_TEAM_HAVE_ALL_ENEMY_TEAMS_DEAD()
		IF MC_serverBD.iNumberOfPlayingPlayers[iteam] > 0
			PRINTLN("[RCC MISSION] SUDDEN DEATH - All enemy teams are dead for team ",iteam,", give them 1000 points")
			GIVE_TEAM_ENOUGH_POINTS_TO_PASS(iTeam)
			bObjectivePassed = TRUE
		ENDIF
		#IF IS_DEBUG_BUILD
		PRINTLN("[RCC MISSION] SERVER TIME LIMIT FOR OBJECTIVE EXPIRED - ONE TEAM KILLED EVERYONE ELSE, OBJECTIVE: ",iRule," TEAM: ",iteam)
		#ENDIF
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TIME_EXPIRED 
		bObjectiveOver = TRUE
	ENDIF
	
ENDPROC

PROC PROCESS_MORE_IN_LOC_SUDDEN_DEATH_LOGIC(INT iTeam, INT iRule, BOOL& bObjectiveOver, BOOL& bObjectivePassed)
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - PROCESS_MORE_IN_LOC_SUDDEN_DEATH_LOGIC")
	
	UNUSED_PARAMETER(iRule)
	INT iWinningTeam = -1
	
	IF DOES_ANY_TEAM_HAVE_MORE_PLAYERS_IN_LOCATES(iWinningTeam)	
		IF iWinningTeam = iTeam
			PRINTLN("[RCC MISSION] SUDDEN DEATH - Team ",iTeam," have more people in locates than everyone else, give them 1000 points")
			GIVE_TEAM_ENOUGH_POINTS_TO_PASS(iTeam)
			bObjectivePassed = TRUE
		ELSE
			REQUEST_SET_TEAM_FAILED(iteam, mFail_ARV_LOC, FALSE)
		ENDIF
		#IF IS_DEBUG_BUILD
		PRINTLN("[RCC MISSION] SERVER TIME LIMIT FOR OBJECTIVE EXPIRED - ONE TEAM (TEAM ",iWinningTeam,") HAD MORE PLAYERS IN LOCATES, OBJECTIVE: ",iRule," TEAM: ",iteam)
		#ENDIF
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TIME_EXPIRED 
		bObjectiveOver = TRUE
	ENDIF
ENDPROC

PROC PROCESS_SUMO_PENNED_IN_SUDDEN_DEATH_LOGIC(INT iTeam, BOOL& bObjectiveOver, BOOL& bObjectivePassed)
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - PROCESS_SUMO_PENNED_IN_SUDDEN_DEATH_LOGIC")
	
	INT iWinningTeam = -1

	IF IS_ONLY_ONE_TEAM_ALIVE(iWinningTeam)
		IF iWinningTeam = iTeam
			PRINTLN("[RCC MISSION][SD] PROCESS_SUDDEN_DEATH_OBJECTIVE_LOGIC - END ROUND - ONLY 1 TEAM ALIVE: ", iTeam)
			GIVE_TEAM_ENOUGH_POINTS_TO_PASS(iTeam)
			bObjectivePassed = TRUE
		ELSE
			REQUEST_SET_TEAM_FAILED(iteam, mFail_ARV_LOC, FALSE)
		ENDIF
		
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TIME_EXPIRED 
		bObjectiveOver = TRUE
	ENDIF
	
ENDPROC

PROC PROCESS_BE_MY_VALENTINE_SUDDEN_DEATH_LOGIC(INT iTeam, BOOL& bObjectiveOver, BOOL& bObjectivePassed)
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - PROCESS_BE_MY_VALENTINE_SUDDEN_DEATH_LOGIC")
	
	INT iWinningTeam = -1
		
	IF IS_ONLY_ONE_TEAM_ALIVE(iWinningTeam)					
		IF iWinningTeam = iTeam
			PRINTLN("[RCC MISSION] GAME OVER - ONLY 1 TEAM ALIVE: ", iTeam)
			GIVE_TEAM_ENOUGH_POINTS_TO_PASS(iTeam)
			bObjectivePassed = TRUE
		ELSE
			REQUEST_SET_TEAM_FAILED(iTeam, mFail_ARV_LOC, FALSE)
		ENDIF
		
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TIME_EXPIRED 
		bObjectiveOver = TRUE
	ENDIF
ENDPROC

PROC PROCESS_TARGET_SUDDEN_DEATH_LOGIC(INT iTeam, INT iRule, BOOL& bObjectiveOver, BOOL& bObjectivePassed, BOOL bUseMultiRuleTimer = FALSE)
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - PROCESS_TARGET_SUDDEN_DEATH_LOGIC")
	
	INT iTimer = GET_SUDDEN_DEATH_TIME_REMAINING(iTeam, iRule)
	INT iLimit = GET_TOTAL_SUDDEN_DEATH_TIME_LIMIT_FOR_RULE(iTeam, iRule)
					  
	IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		INT iSuddenDeathTargetCurrentPlayers
		INT iSuddenDeathTargetTotalShots
		
		INT iSuddenDeathTargetTeamHit[FMMC_MAX_TEAMS]
		
		INT iParticipant
		
		FOR iParticipant = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
			IF iParticipant < MAX_NUM_MC_PLAYERS
				PARTICIPANT_INDEX currentParticipant = INT_TO_PARTICIPANTINDEX(iParticipant)
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(currentParticipant)
					IF NOT IS_BIT_SET(MC_playerBD[NATIVE_TO_INT(currentParticipant)].iClientBitSet, PBBOOL_ANY_SPECTATOR)
						iSuddenDeathTargetCurrentPlayers++
						
						IF IS_BIT_SET(MC_playerBD[NATIVE_TO_INT(currentParticipant)].iClientBitSet3, PBBOOL3_RUGBY_GOAL_TARGET_HIT)
						OR IS_BIT_SET(MC_playerBD[NATIVE_TO_INT(currentParticipant)].iClientBitSet3, PBBOOL3_RUGBY_GOAL_TARGET_MISSED)
							iSuddenDeathTargetTotalShots++
						ENDIF
						
						IF IS_BIT_SET(MC_playerBD[NATIVE_TO_INT(currentParticipant)].iClientBitSet3, PBBOOL3_RUGBY_GOAL_TARGET_HIT)
							iSuddenDeathTargetTeamHit[MC_playerBD[NATIVE_TO_INT(currentParticipant)].iTeam]++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
		iLimit = GET_SUDDEN_DEATH_TIME_LIMIT_FOR_RULE(iTeam, iRule)
		
		PRINTLN("SUDDEN DEATH TARGET GAME OVER - iSuddenDeathTargetCurrentPlayers =  ", iSuddenDeathTargetCurrentPlayers)
		PRINTLN("SUDDEN DEATH TARGET GAME OVER - iSuddenDeathTargetTotalShots =  ", iSuddenDeathTargetTotalShots)
		
		IF (g_FMMC_STRUCT.sFMMCEndConditions[iteam].iObjectiveSuddenDeathTimeLimit[iRule] > 0 AND iTimer > iLimit)
		OR iSuddenDeathTargetTotalShots = iSuddenDeathTargetCurrentPlayers
			INT iTeamLoop
			
			INT iTeamHighest
			
			INT iHighestScore = 0
			
			FOR iTeamLoop = 0 TO (FMMC_MAX_TEAMS - 1)
				PRINTLN("SUDDEN DEATH TARGET GAME OVER - iSuddenDeathTargetTeamHit[", iTeamLoop, "] =  ", iSuddenDeathTargetTeamHit[iTeamLoop])
				
				IF iSuddenDeathTargetTeamHit[iTeamLoop] > iHighestScore
					iHighestScore = iSuddenDeathTargetTeamHit[iTeamLoop]
					
					iTeamHighest = iTeamLoop
				ENDIF
			ENDFOR
			
			BOOL bTied = FALSE
			
			FOR iTeamLoop = 0 TO (FMMC_MAX_TEAMS - 1)
				IF iTeamLoop != iTeamHighest
					IF iSuddenDeathTargetTeamHit[iTeamLoop] = iHighestScore
						bTied = TRUE
						
						BREAKLOOP
					ENDIF
				ENDIF
			ENDFOR
			
			IF NOT bTied
				PRINTLN("SUDDEN DEATH TARGET GAME OVER - TEAM ", iTeamHighest, " HAS TAKEN THE LEAD - ENDING THE GAME")
				
				IF iTeamHighest = iTeam
					GIVE_TEAM_ENOUGH_POINTS_TO_PASS(iTeam)
					bObjectivePassed = TRUE
				ELSE
					REQUEST_SET_TEAM_FAILED(iTeam, mFail_ARV_LOC, FALSE)
				ENDIF
				
				SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MULTIRULE_TIMER_ENDED_GAME)
				SET_BIT(MC_serverBD.iServerBitSet,SSBOOL_TEAM0_FINISHED)
				SET_BIT(MC_serverBD.iServerBitSet,SSBOOL_TEAM1_FINISHED)
				
				MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TIME_EXPIRED 
				bObjectiveOver = TRUE
			ELSE
				PRINTLN("[SUDDEN_DEATH_TARGET] Reset Stage")
				
				IF bUseMultiRuleTimer
					PRINTLN("[SUDDEN_DEATH_TARGET][MultiRuleTimer] PROCESS_TARGET_SUDDEN_DEATH_LOGIC - Reinit multi rule timer for team ",iTeam)
					REINIT_NET_TIMER(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam])
					MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam].Timer, MC_serverBD_3.iMultiObjectiveTimeLimit[iteam] * -1)
				ELSE
					REINIT_NET_TIMER(MC_serverBD_3.tdObjectiveLimitTimer[iTeam])
					MC_serverBD_3.iTimerPenalty[iTeam] = 0
					MC_serverBD_3.tdObjectiveLimitTimer[iTeam].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdObjectiveLimitTimer[iTeam].Timer, GET_OBJECTIVE_TIME_LIMIT_FOR_RULE(iTeam, iRule) * -1)
				ENDIF
				
				bIncrementSuddenDeathTargetRound = TRUE
			ENDIF
		ENDIF
	ELSE
		IF NOT ARE_ANY_WINNING_TEAM_SCORES_EQUAL()
			IF iTeam = GET_THE_WINNING_TEAM()
				GIVE_TEAM_ENOUGH_POINTS_TO_PASS(iTeam)
				PRINTLN("[JT END SCORE] ARE_ANY_WINNING_TEAM_SCORES_EQUAL")
				bObjectivePassed = TRUE
			ELSE
				REQUEST_SET_TEAM_FAILED(iTeam, mFail_ARV_LOC, FALSE)
			ENDIF
			
			SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MULTIRULE_TIMER_ENDED_GAME)
			MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TIME_EXPIRED 
			bObjectiveOver = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PACKAGE_HELD_SUDDEN_DEATH_LOGIC(INT iTeam, INT iRule, BOOL& bObjectiveOver, BOOL& bObjectivePassed)
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - PROCESS_PACKAGE_HELD_SUDDEN_DEATH_LOGIC")
	
	BOOL bEndGame = FALSE
	
	//In sudden death, progress if no package is held or the sudden death timer in the right hand menu has run its time.
	//We have a special timer to give the data time to sync (for last second scoring). Also get into here if this timer has
	//started, just incase someone else picks it up within the timer safety time
	PRINTLN("SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD(", iTeam, ") - 5")
		
	IF (NOT DOES_ANY_PLAYER_HOLD_A_PACKAGE() OR HAS_NET_TIMER_STARTED(stWaitForScoreUpdate))
	OR HAS_SUDDEN_DEATH_TIMER_EXPIRED(iTeam, iRule)

		IF NOT HAS_NET_TIMER_STARTED(stWaitForScoreUpdate)
		//Shouldn't be needed, but just in case it somehow didn't get stopped in pass logic
		OR (HAS_NET_TIMER_STARTED(stWaitForScoreUpdate) AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stWaitForScoreUpdate) > 50000)
			PRINTLN("SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD(", iTeam, ") - 5.1 ############# TIMER SET")
			REINIT_NET_TIMER(stWaitForScoreUpdate)
		ENDIF
		
		//Hack - wait 500ms to let the score data safely update
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stWaitForScoreUpdate) > 500
			PRINTLN("SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD(", iTeam, ") - 6")
			#IF IS_DEBUG_BUILD
				PRINTLN("SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD(", iTeam, ") - 6.1: ", g_FMMC_STRUCT.sFMMCEndConditions[iteam].iObjectiveSuddenDeathTimeLimit[iRule]
						, ", ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iteam])
						, ", ", MC_serverBD_3.iMultiObjectiveTimeLimit[MC_playerBD[iPartToUse].iteam]
						, ", ", GET_SUDDEN_DEATH_TIME_LIMIT_FOR_RULE(iTeam, iRule))
			#ENDIF
			
			PRINTLN("SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD(", iTeam, ") - bEndGame (2)")
			bEndGame = TRUE
		ELSE
			PRINTLN("Waiting on stWaitForScoreUpdate: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stWaitForScoreUpdate))
		ENDIF
	ENDIF
	
	IF bEndGame
		
		IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_SUDDEN_DEATH_RUN_PLAYER_SCORE_UPDATE)
			SET_BIT(iLocalBoolCheck16, LBOOL16_SUDDEN_DEATH_REQUEST_PLAYER_SCORE_UPDATE)
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_SUDDEN_DEATH_RUN_PLAYER_SCORE_UPDATE)
			
			IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_SUDDEN_DEATH_REQUEST_PLAYER_SCORE_UPDATE)
				CLEAR_BIT(iLocalBoolCheck16, LBOOL16_SUDDEN_DEATH_REQUEST_PLAYER_SCORE_UPDATE)
			ENDIF
			
			IF NOT ARE_ANY_WINNING_TEAM_SCORES_EQUAL()
				INT iWinningTeam = GET_THE_WINNING_TEAM()
				PRINTLN("SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD(", iTeam, ") - 7 - iWinningTeam: ", iWinningTeam)
				
				IF iTeam = iWinningTeam
					GIVE_TEAM_ENOUGH_POINTS_TO_PASS(iTeam)
					bObjectivePassed = TRUE
				ELSE
					REQUEST_SET_TEAM_FAILED(iTeam, mFail_ARV_LOC, FALSE)
				ENDIF
			ELIF GET_TEAM_WITH_MOST_KILLS() != -1
				PRINTLN("SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD(", iTeam, ") - 8")
				INT iTeamWithMostKills = GET_TEAM_WITH_MOST_KILLS()
				
				IF iTeamWithMostKills != -1
					PRINTLN("SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD(", iTeam, ") - iTeamWithMostKills = ", iTeamWithMostKills)
					IF iTeam = iTeamWithMostKills
						GIVE_TEAM_ENOUGH_POINTS_TO_PASS(iTeam)
						bObjectivePassed = TRUE
					ELSE
						REQUEST_SET_TEAM_FAILED(iTeam, mFail_ARV_LOC, FALSE)
					ENDIF
					IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
						IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_SUDDEN_DEATH_REASON_SHARD_SENT)
							BROADCAST_FMMC_SUDDEN_DEATH_REASON(iTeamWithMostKills, ciSUDDEN_DEATH_REASON_KILLS)
							SET_BIT(iLocalBoolCheck16, LBOOL16_SUDDEN_DEATH_REASON_SHARD_SENT)
						ENDIF
					ENDIF
				ENDIF
			ELIF GET_TEAM_WITH_LEAST_DEATHS() != -1
				PRINTLN("SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD(", iTeam, ") - 9")
				INT iTeamWithLeastDeaths = GET_TEAM_WITH_LEAST_DEATHS()
				
				IF iTeamWithLeastDeaths != -1
					PRINTLN("SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD(", iTeam, ") - iTeamWithLeastDeaths = ", iTeamWithLeastDeaths)
					IF iTeam = iTeamWithLeastDeaths
						GIVE_TEAM_ENOUGH_POINTS_TO_PASS(iTeam)
						bObjectivePassed = TRUE
					ELSE
						REQUEST_SET_TEAM_FAILED(iTeam, mFail_ARV_LOC, FALSE)
					ENDIF
					IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
						IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_SUDDEN_DEATH_REASON_SHARD_SENT)
							BROADCAST_FMMC_SUDDEN_DEATH_REASON(iTeamWithLeastDeaths, ciSUDDEN_DEATH_REASON_DEATHS)
							SET_BIT(iLocalBoolCheck16, LBOOL16_SUDDEN_DEATH_REASON_SHARD_SENT)
						ENDIF
					ENDIF
				ENDIF
			ELIF GET_TEAM_WITH_MOST_HEADSHOTS() != -1
				PRINTLN("SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD(", iTeam, ") - 10")
				INT iTeamWithMostHeadshots = GET_TEAM_WITH_MOST_HEADSHOTS()
				
				IF iTeamWithMostHeadshots != -1
					PRINTLN("SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD(", iTeam, ") - iTeamWithMostHeadshots = ", iTeamWithMostHeadshots)
					IF iTeam = iTeamWithMostHeadshots
						GIVE_TEAM_ENOUGH_POINTS_TO_PASS(iTeam)
						bObjectivePassed = TRUE
					ELSE
						REQUEST_SET_TEAM_FAILED(iTeam, mFail_ARV_LOC, FALSE)
					ENDIF
					IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
						IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_SUDDEN_DEATH_REASON_SHARD_SENT)
							BROADCAST_FMMC_SUDDEN_DEATH_REASON(iTeamWithMostHeadshots, ciSUDDEN_DEATH_REASON_HSHOTS)
							SET_BIT(iLocalBoolCheck16, LBOOL16_SUDDEN_DEATH_REASON_SHARD_SENT)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD(", iTeam, ") - 11")
				REQUEST_SET_TEAM_FAILED(iTeam, mFail_ARV_LOC, FALSE)
			ENDIF
			
			PRINTLN("SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD(", iTeam, ") - 12")
			INT i
			FOR i = 0 TO (MC_serverBD.iNumActiveTeams-1)
				IF i <= 3 
					SET_BIT(MC_serverBD.iServerBitSet,SSBOOL_TEAM0_FINISHED + i)
				ENDIF
			ENDFOR
			
			SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MULTIRULE_TIMER_ENDED_GAME)
			MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TIME_EXPIRED 
			
			bObjectiveOver = TRUE
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_IN_AND_OUT_SUDDEN_DEATH_LOGIC(INT iTeam ,BOOL& bObjectiveOver, BOOL& bObjectivePassed)
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - PROCESS_IN_AND_OUT_SUDDEN_DEATH_LOGIC")
	
	INT iAttackingTeam = -1
		
	IF NOT ATTACKING_TEAM_HAS_ALL_PACKAGES(iAttackingTeam)
	
		PRINTLN("[KH] GAME OVER - ATTACKING TEAM ",iAttackingTeam," DOESN'T HAVE ALL THE PACKAGES - we're looking at team: ", iTeam)
		
		IF iAttackingTeam != iTeam
			GIVE_TEAM_ENOUGH_POINTS_TO_PASS(iTeam)
			bObjectivePassed = TRUE
		ELSE
			REQUEST_SET_TEAM_FAILED(iteam, mFail_ARV_LOC, FALSE)
		ENDIF			
		
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TIME_EXPIRED 
		bObjectiveOver = TRUE
	ENDIF
ENDPROC

PROC PROCESS_POWER_PLAY_SUDDEN_DEATH_LOGIC(INT iTeam, BOOL& bObjectiveOver, BOOL& bObjectivePassed)
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - PROCESS_POWER_PLAY_SUDDEN_DEATH_LOGIC")
	
	INT iTeamTakenLead = -1
	
	IF NOT MULTIPLE_TEAMS_ON_SAME_SCORE(iTeamTakenLead)
	AND NOT (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciSUDDEN_DEATH_POWER_PLAY_CAPTURE) AND iTeamTakenLead = -1)
	//AND ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
	
		PRINTLN("[RCC MISSION][SD] GAME OVER - TEAM ",iTeamTakenLead," HAS TAKEN THE LEAD - ENDING THE GAME")
		
		IF iTeamTakenLead = iTeam
			bObjectivePassed = TRUE
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciSUDDEN_DEATH_POWER_PLAY_CAPTURE)
				GIVE_TEAM_ENOUGH_POINTS_TO_PASS(iTeam)
			ENDIF
		ELSE
			REQUEST_SET_TEAM_FAILED(iteam, mFail_none, FALSE)
		ENDIF
		
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TIME_EXPIRED 
		bObjectiveOver = TRUE
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciSUDDEN_DEATH_POWER_PLAY_CAPTURE)
	AND IS_ONLY_ONE_TEAM_ALIVE(iTeamTakenLead)
		PRINTLN("[RCC MISSION][SD] GAME OVER - TEAM ",iTeamTakenLead," IS ONLY ONE REMAINING - ENDING THE GAME")
		
		IF iTeamTakenLead = iTeam
			bObjectivePassed = TRUE
			GIVE_TEAM_ENOUGH_POINTS_TO_PASS(iTeam)
		ELSE
			REQUEST_SET_TEAM_FAILED(iteam, mFail_none, FALSE)
		ENDIF
		
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TIME_EXPIRED 
		bObjectiveOver = TRUE
	ENDIF
	
ENDPROC

PROC PROCESS_KILL_ALL_ENEMIES_PENNED_IN_SUDDEN_DEATH_LOGIC(INT iTeam, BOOL& bObjectiveOver, BOOL& bObjectivePassed)
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - PROCESS_KILL_ALL_ENEMIES_PENNED_IN_SUDDEN_DEATH_LOGIC")
	
	INT iWinningTeam = -1

	IF IS_ONLY_ONE_TEAM_ALIVE(iWinningTeam)
		IF iWinningTeam = iTeam
			PRINTLN("[RCC MISSION][SD] GAME OVER - ONLY 1 TEAM ALIVE: ", iTeam)
			GIVE_TEAM_ENOUGH_POINTS_TO_PASS(iTeam)
			bObjectivePassed = TRUE
		ELSE
			REQUEST_SET_TEAM_FAILED(iTeam, mFail_ARV_LOC, FALSE)
		ENDIF
		
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TIME_EXPIRED 
		bObjectiveOver = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    [KH][SSDS] Process the shrinking sphere with the player's position and remove from the game if they are outside the sphere
PROC PROCESS_SUDDEN_DEATH_SUMO_PENNED_IN()
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - PROCESS_SUDDEN_DEATH_SUMO_PENNED_IN")
	
	INT iR, iG, iB, iA
	FLOAT fA = 0.5
	HUD_COLOURS colourToUse = HUD_COLOUR_YELLOW
	
	GET_HUD_COLOUR(colourToUse, iR, iG, iB, iA)
	
	BLOCK_OBJECTIVE_THIS_FRAME()
	
	IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_SSDS_SUDDEN_DEATH_STARTED)
		PRINTLN("[KH][SSD][LIVE] - SUMO SUDDEN DEATH STARTED")
			
		vSphereSpawnPoint = g_FMMC_STRUCT.vSphereSpawnPoint
		fSphereStartRadius = g_FMMC_STRUCT.fSphereStartRadius
		fSphereShrinkTime = g_FMMC_STRUCT.iSphereShrinkTime
		fSphereEndRadius = g_FMMC_STRUCT.fSphereEndRadius
		fTimeToGetInsideSphere = g_FMMC_STRUCT.iTimeToGetInsideSphere

		//set initial sphere radius
		fCurrentSphereRadius = fSphereStartRadius
		
		SET_BIT(iLocalBoolCheck15, LBOOL15_SSDS_SUDDEN_DEATH_STARTED)
		
		IF iSpectatorTarget != -1
		OR bIsAnySpectator
			IF MC_serverBD_3.fSumoSuddenDeathRadius > 0
				PRINTLN("[KH] PROCESS_SUDDEN_DEATH_SUMO_PENNED_IN - SPECTATOR - INITIALISING LATE START SPHERE SIZE")
				fCurrentSphereRadius = MC_serverBD_3.fSumoSuddenDeathRadius
			ENDIF
			
			IF MC_playerBD[iLocalPart].eSumoSuddenDeathStage != eSSDS_TIMER_GET_INSIDE_SPHERE
				MC_playerBD[iLocalPart].eSumoSuddenDeathStage = eSSDS_TIMER_GET_INSIDE_SPHERE
			ENDIF
			
			IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_HAS_COUNTDOWN_FINISHED)
				IF MC_serverBD_3.fSumoSuddenDeathTimeToGetIn > 0
					fTimeToGetInsideSphere = MC_serverBD_3.fSumoSuddenDeathTimeToGetIn
				ENDIF
				//start the countdown timer for getting inside the sphere
				START_NET_TIMER(stTimeToGetInSphere)
				PRINTLN("[KH] PROCESS_SUDDEN_DEATH_SUMO_PENNED_IN - SPECTATOR - INITIALISING LATE START COUNTDOWN TIMER")
			ELSE
				IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_ELIMINATION_COUNTDOWN_FINISHED)
					SET_BIT(iLocalBoolCheck15, LBOOL15_HAS_ELIMINATION_COUNTDOWN_FINISHED)
					PRINTLN("[KH] PROCESS_SUDDEN_DEATH_SUMO_PENNED_IN - SPECTATOR - SETTING LATE START COUNTDOWN FINISH")
				ENDIF
			ENDIF
		ELSE
			//start the countdown timer for getting inside the sphere
			START_NET_TIMER(stTimeToGetInSphere)
		ENDIF
		
	ENDIF
	
	//aimed at spectators
	IF fTimeToGetInsideSphere = 0
		fTimeToGetInsideSphere = MC_serverBD_3.fSumoSuddenDeathTimeToGetIn
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF HAS_NET_TIMER_STARTED(MC_ServerBD.tdSuddenDeath_ShrinkingBoundsHUDTimer[0])
	AND IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
		PRINTLN("[SUMOSUDDENDEATH] g_FMMC_STRUCT.iSphereShrinkTime = ", g_FMMC_STRUCT.iSphereShrinkTime)
		
		TEXT_LABEL_63 tlDebug = "Time: "
		tlDebug += g_FMMC_STRUCT.iSphereShrinkTime
		tlDebug += " / Time left: "
		tlDebug += GET_REMAINING_TIME_ON_SUDDEN_DEATH_SHRINKING_BOUNDS(0)
		DRAW_DEBUG_TEXT_2D(tlDebug, <<0.5, 0.5, 0.5>>)
		DRAW_DEBUG_TEXT_2D("Timer has started!!", <<0.85, 0.85, 0.5>>)
	ENDIF
	#ENDIF
	
	//time elapsed since starting the countdown to get into the sphere
	INT iTimeElapsed = 0
	iTimeElapsed = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stTimeToGetInSphere)
	INT iTimeElapsedCountdown = fTimeToGetInsideSphere - iTimeElapsed
	
	IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
		//countdown timer should have finished before shrinking the sphere
		IF iTimeElapsed >= fTimeToGetInsideSphere
			//IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_COUNTDOWN_TIMER_FINISHED)
			IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_ELIMINATION_COUNTDOWN_FINISHED)
				PRINTLN("[KH][SSD] - COUNTDOWN TIMER HAS FINISHED")
				SET_BIT(iLocalBoolCheck15, LBOOL15_HAS_ELIMINATION_COUNTDOWN_FINISHED)
				IF iSpectatorTarget = -1
				OR NOT bIsAnySpectator
					IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_HAS_COUNTDOWN_FINISHED)
						SET_BIT(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_HAS_COUNTDOWN_FINISHED)
					ENDIF
				ENDIF
			ENDIF
			
			IF bIsLocalPlayerHost
				INT iTeamLoop
				FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
					IF NOT HAS_NET_TIMER_STARTED(MC_ServerBD.tdSuddenDeath_ShrinkingBoundsHUDTimer[iTeamLoop])
						REINIT_NET_TIMER(MC_ServerBD.tdSuddenDeath_ShrinkingBoundsHUDTimer[iTeamLoop])
						PRINTLN("[SUMOSUDDENDEATH] Starting timer now for team ", iTeamLoop)
					ENDIF
				ENDFOR
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_ELIMINATION_COUNTDOWN_FINISHED)
				IF bIsLocalPlayerHost
					IF fTimeToGetInsideSphere > 0
						IF NOT HAS_NET_TIMER_STARTED(stSumoSDCountdownUpdateTimer)
							REINIT_NET_TIMER(stSumoSDCountdownUpdateTimer)
						ENDIF
						
						IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stSumoSDCountdownUpdateTimer) >= 250
							REINIT_NET_TIMER(stSumoSDCountdownUpdateTimer)
							MC_serverBD_3.fSumoSuddenDeathTimeToGetIn = iTimeElapsedCountdown	
						ENDIF
					ENDIF
				ENDIF
		
				HUD_COLOURS hcTimerColour = HUD_COLOUR_RED
				
				IF bPedToUseOk
					DRAW_GENERIC_TIMER(iTimeElapsedCountdown, "SUMO_ELIM", 0, TIMER_STYLE_USEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_BOTTOM, FALSE, hcTimerColour, HUDFLASHING_NONE, 0, FALSE, hcTimerColour, FALSE, DEFAULT, DEFAULT, TRUE) //B* 2666917
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//----- UPDATE AND DRAW SPHERE -----//

	IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
		//shrink sphere when countdown timer has finished
		IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_ELIMINATION_COUNTDOWN_FINISHED)
		OR IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_HAS_COUNTDOWN_FINISHED)
			ADJUST_BLIP_RADIUS_OVER_TIME()
		ENDIF
		
		//aimed at specator
		IF fCurrentSphereRadius = 0
			fCurrentSphereRadius = MC_serverBD_3.fSumoSuddenDeathRadius
		ENDIF

		IF bIsLocalPlayerHost
			IF fCurrentSphereRadius > 0
				IF NOT HAS_NET_TIMER_STARTED(stSumoSDBubbleUpdateTimer)
					REINIT_NET_TIMER(stSumoSDBubbleUpdateTimer)
				ENDIF
				
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stSumoSDBubbleUpdateTimer) >= ciSUMO_SD_BUBBLE_UPDATE_INTERVAL
					REINIT_NET_TIMER(stSumoSDBubbleUpdateTimer)
					MC_serverBD_3.fSumoSuddenDeathRadius = fCurrentSphereRadius
				ENDIF
			ENDIF
		ENDIF
		
		DRAW_MARKER_SPHERE(vSphereSpawnPoint, fCurrentSphereRadius + fCurrentSphereRadius_VisualOffset, iR, iG, iB, fA)

	ENDIF
	
	//----------------------------------//
		
	//at 10 seconds on the elimination timer start the beeps
	IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
		IF MC_playerBD[iPartToUse].eSumoSuddenDeathStage != eSSDS_PUSHED_OUT_OF_SPHERE
			IF iTimeElapsedCountdown <= 10000 & iTimeElapsedCountdown > 0
				IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_IS_SUDDEN_DEATH_SOUND_PLAYING)
					IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
						SET_BIT(iLocalBoolCheck15, LBOOL15_IS_SUDDEN_DEATH_SOUND_PLAYING)
						
						PRINTLN("[KH][SSD] - START PLAYING ELIMINATION COUNTDOWN SOUND")
						
						IF iOutAreaSound < 0
							iOutAreaSound = GET_SOUND_ID()
						ENDIF
						
						PLAY_SOUND_FRONTEND(iOutAreaSound, "OOB_Timer_Dynamic", "GTAO_FM_Events_Soundset", FALSE)
						
						INT timeToStartTimer
						
						//case to deal with if the time to get inside the sphere is set to be less than 10 seconds
						IF fTimeToGetInsideSphere <= 10000
							timeToStartTimer = fTimeToGetInsideSphere
						ELSE
							timeToStartTimer = iTimeElapsedCountdown
						ENDIF
						
						SET_VARIABLE_ON_SOUND(iOutAreaSound, "Time", (TO_FLOAT(timeToStartTimer))/1000.0)

					ENDIF
				ELSE
					IF IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
						IF NOT HAS_SOUND_FINISHED(iOutAreaSound)
							STOP_SOUND(iOutAreaSound)
							SCRIPT_RELEASE_SOUND_ID(iOutAreaSound)
						ENDIF
						CLEAR_BIT(iLocalBoolCheck15, LBOOL15_IS_SUDDEN_DEATH_SOUND_PLAYING)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT HAS_SOUND_FINISHED(iOutAreaSound)
			STOP_SOUND(iOutAreaSound)
			SCRIPT_RELEASE_SOUND_ID(iOutAreaSound)
		ENDIF
	ENDIF
			
	//Checks for the player being inside the sphere
	IF iSpectatorTarget = -1
	AND NOT bIsAnySpectator
		IF bLocalPlayerPedOK
			IF IS_INSIDE_SPHERE(GET_ENTITY_COORDS(LocalPlayerPed), vSphereSpawnPoint, fCurrentSphereRadius)
				IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
					SET_BIT(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_USE_BLIP_FOR_SHRINKING_AREA)
					IF DOES_BLIP_EXIST(SuddenDeathAreaBlip)
						PRINTLN("[PROCESS_SUDDEN_DEATH_SUMO_PENNED_IN][RH] Shrinking area blip is removed")
						REMOVE_BLIP(SuddenDeathAreaBlip)
					ENDIF
				ENDIF 
			ELSE
				IF IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
					CLEAR_BIT(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE) 
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_USE_BLIP_FOR_SHRINKING_AREA)
					IF NOT DOES_BLIP_EXIST(SuddenDeathAreaBlip)
						PRINTLN("[PROCESS_SUDDEN_DEATH_SUMO_PENNED_IN][RH] Shrinking area blip is created")
						SuddenDeathAreaBlip = CREATE_BLIP_FOR_COORD(vSphereSpawnPoint)
						SET_BLIP_COLOUR(SuddenDeathAreaBlip, GET_BLIP_COLOUR_FROM_HUD_COLOUR(colourToUse))	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_USE_BLIP_FOR_SHRINKING_AREA)
			IF DOES_BLIP_EXIST(SuddenDeathAreaBlip)
				PRINTLN("[PROCESS_SUDDEN_DEATH_SUMO_PENNED_IN][RH] Shrinking area blip is removed")
				REMOVE_BLIP(SuddenDeathAreaBlip)
			ENDIF
		ELSE
			IF NOT DOES_BLIP_EXIST(SuddenDeathAreaBlip)
			AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_USE_BLIP_FOR_SHRINKING_AREA)
				PRINTLN("[PROCESS_SUDDEN_DEATH_SUMO_PENNED_IN][RH] Shrinking area blip is created")
				SuddenDeathAreaBlip = CREATE_BLIP_FOR_COORD(vSphereSpawnPoint)
				SET_BLIP_COLOUR(SuddenDeathAreaBlip, GET_BLIP_COLOUR_FROM_HUD_COLOUR(colourToUse))	
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END			
		//states for each player during the stage
		SWITCH MC_playerBD[iPartToUse].eSumoSuddenDeathStage
			CASE eSSDS_TIMER_GET_INSIDE_SPHERE			
				//IF iTimeElapsed >= fTimeToGetInsideSphere
					IF IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
						PRINTLN("[KH][SSD] - PLAYER GOT TO THE SPHERE IN TIME")
						IF NOT bIsAnySpectator
							MC_playerBD[iPartToUse].eSumoSuddenDeathStage = eSSDS_INSIDE_SPHERE
						ENDIF
					ELSE
						IF iTimeElapsed >= fTimeToGetInsideSphere
							IF NOT bIsAnySpectator
								MC_playerBD[iPartToUse].eSumoSuddenDeathStage = eSSDS_PUSHED_OUT_OF_SPHERE
							ENDIF
						ENDIF
					ENDIF
				//ENDIF
			BREAK
			//after the timer has counted down and now players should be killed if they leave the bubble
			CASE eSSDS_INSIDE_SPHERE	
				IF NOT HAS_SOUND_FINISHED(iOutAreaSound)
					STOP_SOUND(iOutAreaSound)
					SCRIPT_RELEASE_SOUND_ID(iOutAreaSound)
				ENDIF
				IF NOT bIsAnySpectator
					IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
					AND iTimeElapsed >= fTimeToGetInsideSphere
						PRINTLN("[KH][SSD] - WENT OUTSIDE SPHERE BOUNDS: REMOVE PLAYER")
						MC_playerBD[iPartToUse].eSumoSuddenDeathStage = eSSDS_PUSHED_OUT_OF_SPHERE
					ELSE
						IF iTimeElapsed < fTimeToGetInsideSphere
							MC_playerBD[iPartToUse].eSumoSuddenDeathStage = eSSDS_TIMER_GET_INSIDE_SPHERE
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE eSSDS_PUSHED_OUT_OF_SPHERE
				IF NOT bIsAnySpectator
					IF iTimeElapsed >= fTimeToGetInsideSphere
						PRINTLN("[KH][SSD] - PLAYER PUSHED OUT OF SPHERE")
						CLEAR_BIT(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
						SUMO_SUDDEN_DEATH_SET_PLAYER_FAILED_OUTSIDE_SPHERE()
					ELSE
						MC_playerBD[iPartToUse].eSumoSuddenDeathStage = eSSDS_TIMER_GET_INSIDE_SPHERE
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		IF NOT HAS_SOUND_FINISHED(iOutAreaSound)
			STOP_SOUND(iOutAreaSound)
			SCRIPT_RELEASE_SOUND_ID(iOutAreaSound)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN(INT iRule)
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN")
	
	IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
		INT iR, iG, iB, iA
		FLOAT fA = 0.5
		VECTOR vPlayerPos
		
		IF bLocalPlayerPedOK
			vPlayerPos = GET_ENTITY_COORDS(localPlayerPed)
		ENDIF
		
		GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
		
		IF IS_VECTOR_ZERO(vSphereSpawnPosition)	//If for some reason we don't have valid coordinates just use the centre of team 0s bounds...
			IF iRule >= 0
			AND iRule < FMMC_MAX_RULES
				vSphereSpawnPosition = GET_BOUNDS_RUNTIME_CENTRE(0) //(g_FMMC_STRUCT.sFMMCEndConditions[0].sRuleBounds[iRule][0].vPos1 + g_FMMC_STRUCT.sFMMCEndConditions[0].sRuleBounds[iRule][0].vPos2) * 0.5
				GET_GROUND_Z_FOR_3D_COORD(vSphereSpawnPosition, vSphereSpawnPosition.Z)
				PRINTLN("[PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN] vSphereSpawnPosition = <<", vSphereSpawnPosition.X, ", ", vSphereSpawnPosition.Y, ", ", vSphereSpawnPosition.Z, ">>")
			ENDIF
			
			// Ignore Z pos
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_IGNORE_Z_HEIGHT_CHECKS_SD_SHRINKING_BOUNDS)
				vSphereSpawnPosition.Z = vPlayerPos.z
			ENDIF
		ENDIF
		
		//Update timer to that spectators can get an accurate time to start their timer if they join in after sudden death has started
		IF iSpectatorTarget = -1
		AND NOT bIsAnySpectator
			IF NOT HAS_NET_TIMER_STARTED(timeToBeInsideSphereUpdateTimer)
				REINIT_NET_TIMER(timeToBeInsideSphereUpdateTimer)
			ENDIF
				
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timeToBeInsideSphereUpdateTimer) >= 350
				REINIT_NET_TIMER(timeToBeInsideSphereUpdateTimer)
				MC_playerBD[iLocalPart].iKillAllEnemiesPennedInUpdateTimer = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timeToBeInsideSphere)
			ENDIF
		ENDIF
		
		//Effectively sudden death initialisation
		IF NOT HAS_NET_TIMER_STARTED(timeToBeInsideSphere)
			PRINTLN("[PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN] - Sudden death has started, starting timer to get inside sphere")
			IF iSpectatorTarget != -1
			OR bIsAnySpectator
				PRINTLN("[PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN] - Sudden death has started and yet someone has still decided they want to jip in, updating jipper's timer")
				iTimeToBeInsideSphere = g_FMMC_STRUCT.iTimeToGetInsideSphere - MC_playerBD[iPartToUse].iKillAllEnemiesPennedInUpdateTimer
			ELSE
				iTimeToBeInsideSphere = g_FMMC_STRUCT.iTimeToGetInsideSphere
			ENDIF
			
			//set the blips coords for this
			IF DOES_BLIP_EXIST(DeliveryBlip)
				REMOVE_BLIP(DeliveryBlip)
			ENDIF
			
			START_NET_TIMER(timeToBeInsideSphere)
		ELSE
			//If the timer is up
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timeToBeInsideSphere) > iTimeToBeInsideSphere
				//And the local player isn't dead already
				IF bPedToUseOk
					IF iSpectatorTarget = -1
					AND NOT bIsAnySpectator
						IF NOT IS_INSIDE_SPHERE(GET_ENTITY_COORDS(PlayerPedToUse), vSphereSpawnPosition, g_FMMC_STRUCT.fSphereStartRadius)
						AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iKillAllEnemiesPennedInBS, ciKAE_ELIMINATION_TIMER_FINISHED)
							IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_PLAYER_FAIL)
								SET_LOCAL_PLAYER_FAILED(11)
								SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
								SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP)
								CLEAR_ENTITY_LAST_DAMAGE_ENTITY(LocalPlayerPed)
								SET_ENTITY_HEALTH(PlayerPedToUse, 0)
								PRINTLN("[PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN] - Player died, failing them...")
							ENDIF
							
							IF MC_playerBD[iPartToUse].iReasonForPlayerFail != PLAYER_FAIL_REASON_BOUNDS
								MC_playerBD[iPartToUse].iReasonForPlayerFail = PLAYER_FAIL_REASON_BOUNDS
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_MIDSIZED_CUSTOM)
						CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_MIDSIZED_CUSTOM)
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iKillAllEnemiesPennedInBS, ciKAE_ELIMINATION_TIMER_FINISHED)
					SET_BIT(MC_playerBD[iPartToUse].iKillAllEnemiesPennedInBS, ciKAE_ELIMINATION_TIMER_FINISHED)
				ENDIF
			ELSE
				IF bPedToUseOk
					IF NOT IS_INSIDE_SPHERE(GET_ENTITY_COORDS(PlayerPedToUse), vSphereSpawnPosition, g_FMMC_STRUCT.fSphereStartRadius)
						IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_MIDSIZED_CUSTOM)
						AND NOT IS_BIG_MESSAGE_ALREADY_REQUESTED(BIG_MESSAGE_GENERIC_MIDSIZED_CUSTOM)
							PRINTLN("[PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN] - Displaying out of bounds shard")
							
							STRING sStrapline = "MC_RTN_OBJ2"
							
							SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GENERIC_MIDSIZED_CUSTOM, "", sStrapline, "BM_OO_BOUNDS", 999999)
						ENDIF
					ELSE				
						IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_MIDSIZED_CUSTOM)
							PRINTLN("[PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN] - Clearing out of bounds shard")
							CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_MIDSIZED_CUSTOM)
						ENDIF
						
						IF DOES_BLIP_EXIST(SuddenDeathAreaBlip)
							REMOVE_BLIP(SuddenDeathAreaBlip)
						ENDIF
						
						//Set that the player is inside the sphere
						IF iSpectatorTarget = -1
						AND NOT bIsAnySpectator
							IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iKillAllEnemiesPennedInBS, ciKAE_PENNED_IN_INSIDE_SPHERE)
								SET_BIT(MC_playerBD[iPartToUse].iKillAllEnemiesPennedInBS, ciKAE_PENNED_IN_INSIDE_SPHERE)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_INSIDE_SPHERE(GET_ENTITY_COORDS(PlayerPedToUse), vSphereSpawnPosition, g_FMMC_STRUCT.fSphereStartRadius)
					AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iKillAllEnemiesPennedInBS, ciKAE_PENNED_IN_INSIDE_SPHERE)
					AND bPedToUseOk
						INT iTimeElapsedCountdown = iTimeToBeInsideSphere - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timeToBeInsideSphere)
						DRAW_GENERIC_TIMER(iTimeElapsedCountdown, "SUMO_ELIM", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_BOTTOM, FALSE, HUD_COLOUR_RED, HUDFLASHING_NONE, 0, FALSE, HUD_COLOUR_RED, FALSE) //B* 2666917
					ENDIF
				ELSE
					IF iSpectatorTarget = -1
					AND NOT bIsAnySpectator
						IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_DIED_BEFORE_SUDDEN_DEATH)
							IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_PLAYER_FAIL)
								SET_LOCAL_PLAYER_FAILED(12)
								SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
								SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP)
								PRINTLN("[PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN] - Player died, failing them...")
							ENDIF
							
							IF MC_playerBD[iPartToUse].iReasonForPlayerFail != PLAYER_FAIL_REASON_BOUNDS
								MC_playerBD[iPartToUse].iReasonForPlayerFail = PLAYER_FAIL_REASON_BOUNDS
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_MIDSIZED_CUSTOM)
						CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_MIDSIZED_CUSTOM)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Out of area timer once the player is inside the sphere
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iKillAllEnemiesPennedInBS, ciKAE_PENNED_IN_INSIDE_SPHERE)
			IF NOT IS_INSIDE_SPHERE(GET_ENTITY_COORDS(PlayerPedToUse), vSphereSpawnPosition, g_FMMC_STRUCT.fSphereStartRadius)
				INT iTimeElapsedCountdown = ciOUT_OF_AREA_TIME - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(suddenDeathOutOfAreaTimer)
				IF iTimeElapsedCountdown > 0
					DRAW_GENERIC_TIMER(iTimeElapsedCountdown, "BM_OO_BOUNDS", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_TOP, FALSE, HUD_COLOUR_RED, HUDFLASHING_NONE, 0, FALSE, HUD_COLOUR_RED, FALSE) //B* 2666917
					
					INT iSeconds = iTimeElapsedCountdown / 1000
					
					IF IS_SOUND_ID_VALID(iBoundsTimerSound)
					AND HAS_SOUND_FINISHED(iBoundsTimerSound)
						PLAY_SOUND_FRONTEND(iBoundsTimerSound, "Out_of_Bounds", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
						PRINTLN("[PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN] SHOULD_PLAY_OUT_OF_BOUNDS_SOUND Returning TRUE and the sound hasn't started. Playing sound.")
						SET_VARIABLE_ON_SOUND(iBoundsTimerSound, "Time", TO_FLOAT(iSeconds))
					ENDIF
				ENDIF
				IF NOT HAS_NET_TIMER_STARTED(suddenDeathOutOfAreaTimer)
					START_NET_TIMER(suddenDeathOutOfAreaTimer)
				ELSE
					IF iTimeElapsedCountdown <= 0
						IF iSpectatorTarget = -1
						AND NOT bIsAnySpectator
							IF IS_SOUND_ID_VALID(iBoundsTimerSound)
							AND NOT HAS_SOUND_FINISHED(iBoundsTimerSound)
								STOP_SOUND(iBoundsTimerSound)
								PRINTLN("[PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN] SHOULD_PLAY_OUT_OF_BOUNDS_SOUND Returning FALSE and the sound hasn't finished. Force it to stop. FAILED")
							ENDIF
							
							IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_PLAYER_FAIL)
								SET_LOCAL_PLAYER_FAILED(13)
								SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
								SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP)
								SET_ENTITY_HEALTH(PlayerPedToUse, 0)
								
								PRINTLN("[KH] PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN - Player has left the sphere, killing and failing them...")
							ENDIF
							
							IF MC_playerBD[iPartToUse].iReasonForPlayerFail != PLAYER_FAIL_REASON_BOUNDS
								MC_playerBD[iPartToUse].iReasonForPlayerFail = PLAYER_FAIL_REASON_BOUNDS
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_SOUND_ID_VALID(iBoundsTimerSound)
				AND NOT HAS_SOUND_FINISHED(iBoundsTimerSound)
					STOP_SOUND(iBoundsTimerSound)
					PRINTLN("[PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN] SHOULD_PLAY_OUT_OF_BOUNDS_SOUND Returning FALSE and the sound hasn't finished. Force it to stop.")
				ENDIF
				
				IF HAS_NET_TIMER_STARTED(suddenDeathOutOfAreaTimer)
					RESET_NET_TIMER(suddenDeathOutOfAreaTimer)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT DOES_BLIP_EXIST(SuddenDeathAreaBlip)
		AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iKillAllEnemiesPennedInBS, ciKAE_PENNED_IN_INSIDE_SPHERE)
			SuddenDeathAreaBlip = ADD_BLIP_FOR_COORD(vSphereSpawnPosition)
		ENDIF
		
		//Draw the sphere for the players
		DRAW_MARKER_SPHERE(vSphereSpawnPosition, g_FMMC_STRUCT.fSphereStartRadius, iR, iG, iB, fA)
	ELSE
		IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_TEXT)
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_TEXT)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_CLIENT_SUDDEN_DEATH_LOGIC()
	
	IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		EXIT
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - PROCESS_CLIENT_SUDDEN_DEATH_LOGIC")
	
	IF bIsLocalPlayerHost
	AND bIncrementSuddenDeathTargetRound
		MC_serverBD_3.iSuddenDeathTargetRound++
		
		PRINTLN("[SUDDEN_DEATH_TARGET] Increment Round")
		
		bIncrementSuddenDeathTargetRound = FALSE
	ENDIF
	
	//[KH][SSDS]Penned in sudden death type processing
	IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN)
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
			PROCESS_SUDDEN_DEATH_SUMO_PENNED_IN()
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES)
		IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_SUDDEN_DEATH_VALENTINES_ONE_HIT_KILL_INIT)
			SET_ENTITY_HEALTH(localPlayerPed, 101)
			SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(localPlayer, 0.0)
			SET_BIT(iLocalBoolCheck15, LBOOL15_SUDDEN_DEATH_VALENTINES_ONE_HIT_KILL_INIT)
		ENDIF
	ENDIF
	
	//Process sudden death for last team standing inside the sphere
	IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
			PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_GENERIC_SUDDEN_DEATH_LOGIC()
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - PROCESS_GENERIC_SUDDEN_DEATH_LOGIC")
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_SD_DROP_PORTABLE_PICKUPS)
		INT iobj
		FOR iobj = 0 TO ( MC_serverBD.iNumObjCreated - 1 )	
			MC_serverBD.iObjCarrier[iobj] = -1
		ENDFOR
	ENDIF
		
ENDPROC

PROC PROCESS_SUDDEN_DEATH_TIMER_EXPIRED(INT iTeam, INT iRule, BOOL &bObjectiveOver)
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - PROCESS_SUDDEN_DEATH_TIMER_EXPIRED")
	
	IF NOT bObjectiveOver
	AND (IS_BIT_SET(MC_serverBD.iOptionsMenuBitSet,ciUseCTFSuddenDeath) 
	OR IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES) 
	OR IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_MOREINLOC) 
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN) 
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES) 
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_IN_AND_OUT)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_POWER_PLAY)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_JUGGERNAUT))
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)
		PRINTLN("[RCC MISSION] PROCESS_SUDDEN_DEATH_TIMER_EXPIRED - RULE: ", iRule, " TEAM: ", iTeam)
			
		IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iObjectiveSuddenDeathTimeLimit[iRule] > 0
			
			PRINTLN("[RCC MISSION] PROCESS_SUDDEN_DEATH_TIMER_EXPIRED - g_FMMC_STRUCT.sFMMCEndConditions[iteam].iObjectiveSuddenDeathTimeLimit[iRule]: ", g_FMMC_STRUCT.sFMMCEndConditions[iteam].iObjectiveSuddenDeathTimeLimit[iRule])
			IF HAS_NET_TIMER_STARTED( MC_serverBD_3.tdMultiObjectiveLimitTimer[iteam])
			OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN)
				PRINTLN("[RCC MISSION] PROCESS_SUDDEN_DEATH_TIMER_EXPIRED - Either HAS_NET_TIMER_STARTED( MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam]) OR we're doing sumo sudden death. Gonna check HAS_SUDDEN_DEATH_TIMER_EXPIRED")
				
				IF HAS_SUDDEN_DEATH_TIMER_EXPIRED(iTeam, iRule)
					PRINTLN("[RCC MISSION] PROCESS_SUDDEN_DEATH_TIMER_EXPIRED - SERVER SUDDEN DEATH MULTI RULE TIME LIMIT FOR OBJECTIVE EXPIRED: ",iRule," TEAM: ",iteam)
					MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TIME_EXPIRED
					
					IF SHOULD_TEAM_FAIL_FOR_SUDDEN_DEATH_TIMEOUT(iTeam)
						REQUEST_SET_TEAM_FAILED(iteam, mFail_TIME_EXPIRED, FALSE)
						SET_BIT(iTeamsFailedForSDTimeExpired, iteam)
						PRINTLN("[RCC MISSION] PROCESS_SUDDEN_DEATH_TIMER_EXPIRED - SETTING TEAM FAILED SUDDEN DEATH - TIME LIMIT FOR OBJECTIVE EXPIRED: ",iRule," TEAM: ",iteam)
					ENDIF
					
					bObjectiveOver = TRUE
				ENDIF
				
			ELIF GET_SUDDEN_DEATH_TIME_REMAINING(iTeam, iRule) <= 0
				PRINTLN("[RCC MISSION] PROCESS_SUDDEN_DEATH_TIMER_EXPIRED - SERVER SUDDEN DEATH TIME LIMIT FOR OBJECTIVE EXPIRED: ", iRule, " TEAM: ", iteam)
				MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TIME_EXPIRED
				
				IF SHOULD_TEAM_FAIL_FOR_SUDDEN_DEATH_TIMEOUT(iTeam)
					REQUEST_SET_TEAM_FAILED(iteam, mFail_TIME_EXPIRED, FALSE)
					PRINTLN("[RCC MISSION] PROCESS_SUDDEN_DEATH_TIMER_EXPIRED - SETTING TEAM FAILED SUDDEN DEATH - TIME LIMIT FOR OBJECTIVE EXPIRED: ",iRule," TEAM: ",iteam)
				ENDIF
				
				bObjectiveOver = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Processes the sudden death logic - add your bit and function in here
/// PARAMS:
///    iTeam - Current team we're looking at
///    iRule - Current rule we're on
///    bObjectiveOver - Passed by reference so we know the objective is over
///    bObjectivePassed - Passed by reference so we know the objective has been passed by a team
///    bUseMultiRuleTimer - If we are using the multirule timer
PROC PROCESS_SUDDEN_DEATH_OBJECTIVE_LOGIC(INT iTeam, INT iRule, BOOL& bObjectiveOver, BOOL& bObjectivePassed, BOOL bUseMultiRuleTimer = FALSE)
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - PROCESS_SUDDEN_DEATH_OBJECTIVE_LOGIC")
	
	PRINTLN("PROCESS_SUDDEN_DEATH_OBJECTIVE_LOGIC - iTeam: ", iTeam, " | iRule: ", iRule, " | bObjectiveOver: ", bObjectiveOver, " | bObjectivePassed: ", bObjectivePassed, " | bUseMultiRuleTimer: ", bUseMultiRuleTimer)
	PRINTLN("PROCESS_SUDDEN_DEATH_OBJECTIVE_LOGIC - iObjectiveSuddenDeathTimeLimit: ", g_FMMC_STRUCT.sFMMCEndConditions[iteam].iObjectiveSuddenDeathTimeLimit[iRule])
	
	PROCESS_SUDDEN_DEATH_TIMER_EXPIRED(iTeam, iRule, bObjectiveOver)
	
	IF NOT bObjectiveOver
		IF IS_BIT_SET(MC_serverBD.iOptionsMenuBitSet,ciUseCTFSuddenDeath)
			PROCESS_CTF_SUDDEN_DEATH_LOGIC(iTeam, iRule, bObjectiveOver)
		ELIF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES)
			PROCESS_KILL_ALL_ENEMIES_SUDDEN_DEATH_LOGIC(iTeam, iRule, bObjectiveOver, bObjectivePassed)
		ELIF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_MOREINLOC)
			PROCESS_MORE_IN_LOC_SUDDEN_DEATH_LOGIC(iTeam, iRule, bObjectiveOver, bObjectivePassed)
		ELIF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN)
			PROCESS_SUMO_PENNED_IN_SUDDEN_DEATH_LOGIC(iTeam, bObjectiveOver, bObjectivePassed)  //FMMC2020 - Needs renamed
		ELIF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES)
			PROCESS_BE_MY_VALENTINE_SUDDEN_DEATH_LOGIC(iTeam, bObjectiveOver, bObjectivePassed) //FMMC2020 - Needs renamed
		ELIF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_TARGET)
			PROCESS_TARGET_SUDDEN_DEATH_LOGIC(iTeam, iRule, bObjectiveOver, bObjectivePassed, bUseMultiRuleTimer)
		ELIF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD)
			PROCESS_PACKAGE_HELD_SUDDEN_DEATH_LOGIC(iTeam, iRule, bObjectiveOver, bObjectivePassed)
		ELIF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_IN_AND_OUT)
			PROCESS_IN_AND_OUT_SUDDEN_DEATH_LOGIC(iTeam, bObjectiveOver, bObjectivePassed)
		ELIF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_POWER_PLAY)
			PROCESS_POWER_PLAY_SUDDEN_DEATH_LOGIC(iTeam, bObjectiveOver, bObjectivePassed) //FMMC2020 - Needs renamed
		ELIF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)
			PROCESS_KILL_ALL_ENEMIES_PENNED_IN_SUDDEN_DEATH_LOGIC(iTeam, bObjectiveOver, bObjectivePassed)
		ELSE
			PRINTLN("[KH][RCC MISSION][SD] PROCESS_SUDDEN_DEATH_OBJECTIVE_LOGIC - No sudden death bit set for this mode, are you sure we should have got in here?")
		ENDIF
		
		PROCESS_GENERIC_SUDDEN_DEATH_LOGIC()
	ENDIF
ENDPROC

//FMMC2020 - Move back to sudden death section after

/// PURPOSE:
///    Logic for checking whether we should initially go to sudden death - add your bit and logic for determining whether to go to sudden death in that mode
FUNC BOOL SHOULD_PROGRESS_TO_SUDDEN_DEATH(INT iTeam, INT& iTeamAliveAsSuddenDeathStarts)
	
	DEBUG_PRINTCALLSTACK()
	ASSERTLN("FMMC2020 - NOT REFACTORED - SHOULD_PROGRESS_TO_SUDDEN_DEATH")
	
	UNUSED_PARAMETER(iTeam)
	BOOL bShouldStartSuddenDeath = FALSE
	
	INT iWinningTeam = -1
	
	IF IS_BIT_SET(MC_serverBD.iOptionsMenuBitSet,ciUseCTFSuddenDeath)
		PRINTLN("[KH][RCC MISSION][SD] SHOULD_PROGRESS_TO_SUDDEN_DEATH - ciUseCTFSuddenDeath set")
		IF ARE_ANY_WINNING_TEAM_SCORES_EQUAL()
			bShouldStartSuddenDeath = TRUE
		ENDIF
	ELIF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES)
		PRINTLN("[KH][RCC MISSION][SD] SHOULD_PROGRESS_TO_SUDDEN_DEATH - SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES set")
		IF NOT DOES_ANY_TEAM_HAVE_ALL_ENEMY_TEAMS_DEAD()
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_SD_ELIMINATION_ONLY_HIGHEST_DRAWING_TEAMS)
				IF MULTIPLE_TEAMS_ON_SAME_RULE_SCORE(iWinningTeam, FALSE)
					bShouldStartSuddenDeath = TRUE
				ENDIF
			ELSE
				bShouldStartSuddenDeath = TRUE
			ENDIF
		ENDIF
	ELIF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_MOREINLOC)
		PRINTLN("[KH][RCC MISSION][SD] SHOULD_PROGRESS_TO_SUDDEN_DEATH - SBBOOL2_SUDDEN_DEATH_MOREINLOC set")
		IF NOT DOES_ANY_TEAM_HAVE_MORE_PLAYERS_IN_LOCATES(iWinningTeam)
			bShouldStartSuddenDeath = TRUE
		ENDIF
	ELIF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN)
		PRINTLN("[KH][RCC MISSION][SD] SHOULD_PROGRESS_TO_SUDDEN_DEATH - SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN set")
		IF NOT IS_ONLY_ONE_TEAM_ALIVE(iWinningTeam)
			bShouldStartSuddenDeath = TRUE
		ENDIF
	ELIF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES)
		PRINTLN("[KH][RCC MISSION][SD] SHOULD_PROGRESS_TO_SUDDEN_DEATH - SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES set")
		IF NOT IS_ONLY_ONE_TEAM_ALIVE(iWinningTeam)
			bShouldStartSuddenDeath = TRUE
		ENDIF
	ELIF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_TARGET)
		PRINTLN("[KH][RCC MISSION][SD] SHOULD_PROGRESS_TO_SUDDEN_DEATH - SBBOOL3_SUDDEN_DEATH_TARGET set")
		IF ARE_ANY_WINNING_TEAM_SCORES_EQUAL()
			bShouldStartSuddenDeath = TRUE
		ENDIF
	ELIF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD)
		PRINTLN("[KH][RCC MISSION][SD] SHOULD_PROGRESS_TO_SUDDEN_DEATH - SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD set")
		IF ATTACKING_TEAM_HAS_ALL_PACKAGES(iWinningTeam)
		OR DOES_ANY_PLAYER_HOLD_A_PACKAGE()
			bShouldStartSuddenDeath = TRUE
		ENDIF
	ELIF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_IN_AND_OUT)
		PRINTLN("[KH][RCC MISSION][SD] SHOULD_PROGRESS_TO_SUDDEN_DEATH - SBBOOL3_SUDDEN_DEATH_IN_AND_OUT set")
		IF ARE_ANY_WINNING_TEAM_SCORES_EQUAL()
		OR DOES_ANY_PLAYER_HOLD_A_PACKAGE()
			bShouldStartSuddenDeath = TRUE
		ENDIF
	ELIF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_POWER_PLAY)
		PRINTLN("[KH][RCC MISSION][SD] SHOULD_PROGRESS_TO_SUDDEN_DEATH - SBBOOL3_SUDDEN_DEATH_POWER_PLAY set")
		IF MULTIPLE_TEAMS_ON_SAME_SCORE(iWinningTeam)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciSUDDEN_DEATH_POWER_PLAY_CAPTURE)
				IF MULTIPLE_TEAMS_ON_SAME_CAPTURE_PROGRESS(iWinningTeam)
					bShouldStartSuddenDeath = TRUE
				ENDIF
			ELSE
				bShouldStartSuddenDeath = TRUE
			ENDIF
		ENDIF
	ELIF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)
		PRINTLN("[KH][RCC MISSION][SD] SHOULD_PROGRESS_TO_SUDDEN_DEATH - SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN set")
		IF NOT IS_ONLY_ONE_TEAM_ALIVE(iWinningTeam)
			bShouldStartSuddenDeath = TRUE
		ELSE
			iTeamAliveAsSuddenDeathStarts = iWinningTeam
		ENDIF
	ELIF IS_BIT_SET(MC_ServerBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_JUGGERNAUT)
		PRINTLN("[KH][RCC MISSION][SD] SHOULD_PROGRESS_TO_SUDDEN_DEATH - SBBOOL4_SUDDEN_DEATH_JUGGERNAUT set")
		IF MULTIPLE_TEAMS_ON_SAME_SCORE(iWinningTeam)
			bShouldStartSuddenDeath = TRUE
		ENDIF
	ELSE
		PRINTLN("[KH][RCC MISSION][SD] SHOULD_PROGRESS_TO_SUDDEN_DEATH - No sudden death bit set for this mode, are you sure we should have got in here?")
	ENDIF
	
	//We should start sudden death
	IF bShouldStartSuddenDeath
		PRINTLN("[KH][RCC MISSION][SD] SHOULD_PROGRESS_TO_SUDDEN_DEATH - Going to sudden death, setting SBBOOL1_SUDDEN_DEATH")
		SET_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
	ELSE
		PRINTLN("[KH][RCC MISSION][SD] SHOULD_PROGRESS_TO_SUDDEN_DEATH - Not going to sudden death...")
	ENDIF
	
	RETURN bShouldStartSuddenDeath
	
ENDFUNC
