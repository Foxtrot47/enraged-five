// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Event Broadcasting ------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script Name: FM_Mission_Controller_2020.sc ------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Structs and Broadcasts for Mission Controller only events. Events required elsewhere in script too need to be in net_events.	------------------------------------
// ##### If you need to edit a broadcast used in the legacy mission controller, make a copy here 													------------------------------------
// ##### Keep the STRUCT next to the relevant broadcast function 		
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
USING "FM_Mission_Controller_DebugUtility_2020.sch"
#ENDIF
#IF NOT IS_DEBUG_BUILD
USING "FM_Mission_Controller_Utility_2020.sch"
#ENDIF 

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cutscenes -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Event broadcasts for cutscene functionality  ---------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC BROADCAST_EVENT_FMMC_MY_WEAPON_DATA(WEAPON_TYPE wtWeapon)

	SCRIPT_EVENT_DATA_FMMC_MY_WEAPON_DATA sWeaponData
	
	//Set up the event details
	sWeaponData.Details.Type 			= SCRIPT_EVENT_FMMC_MY_WEAPON_DATA
	sWeaponData.Details.FromPlayerIndex = LocalPlayer
	sWeaponData.iParticipant = iLocalPart
	
	IF DOES_PLAYER_HAVE_WEAPON(wtWeapon)
	
		PRINTLN("[Cutscenes] - SEND_MY_WEAPONDATA_TO_OTHER_PLAYERS - DOES_PLAYER_HAVE_WEAPON TRUE - player has weapon = ", GET_WEAPON_NAME( wtWeapon ) )
		sWeaponData.bHasWeapon = TRUE
	
		PED_WEAPONS_MP_STRUCT sWeapons
		GET_PED_WEAPONS_MP(localPlayerPed, sWeapons)

		INT i
		FOR i = 0 TO NUM_PLAYER_PED_WEAPON_SLOTS-1
			IF sWeapons.sWeaponInfo[i].eWeaponType = wtWeapon
				sWeaponData.sWeaponInfo = sWeapons.sWeaponInfo[i]
				BREAKLOOP
			ENDIF
		ENDFOR
		
		FOR i = 0 TO NUMBER_OF_DLC_WEAPONS-1
			IF sWeapons.sDLCWeaponInfo[i].eWeaponType = wtWeapon
				sWeaponData.sWeaponInfo = sWeapons.sDLCWeaponInfo[i]
				BREAKLOOP
			ENDIF
		ENDFOR
		
	ELSE
		sWeaponData.bHasWeapon = FALSE
		PRINTLN("[Cutscenes] - SEND_MY_WEAPONDATA_TO_OTHER_PLAYERS - DOES_PLAYER_HAVE_WEAPON FALSE - player does not have weapon = ", GET_WEAPON_NAME( wtWeapon ) )
	ENDIF
	
	//trigger that event
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // any player could be the host 
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT( SCRIPT_EVENT_QUEUE_NETWORK, sWeaponData, SIZE_OF(sWeaponData), iPlayerFlags )	
	ELSE
		PRINTLN("[Cutscenes] - SEND_MY_WEAPONDATA_TO_OTHER_PLAYERS - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
ENDPROC

/// PURPOSE: Broadcast to all other players that I am playing an animation
/// PARAMS:
///    iAnimationType - Type of animation (Player or Crew)
///    iAnimation - The actual animation to play
PROC BROADCAST_MOCAP_PLAYER_ANIMATION(INT iAnimationType, INT iAnimation, BOOL bPlaying)
	
	EVENT_STRUCT_MOCAP_PLAYER_ANIM Event
	Event.Details.Type 				= SCRIPT_EVENT_FMMC_MOCAP_PLAYER_ANIMATION
	Event.Details.FromPlayerIndex	= LocalPlayer
	Event.iAnimationType 			= iAnimationType
	Event.iAnimation 				= iAnimation
	Event.bPlaying					= bPlaying
		
	INT iSendTo = ALL_PLAYERS(FALSE)
	IF NOT (iSendTo = 0)		
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[RCC MISSION]  BROADCAST_MOCAP_PLAYER_ANIMATION - called...")
			PRINTLN("[RCC MISSION] 					From script: ", GET_THIS_SCRIPT_NAME())
			PRINTLN("[RCC MISSION] 					iAnimationType: ", Event.iAnimationType)
			PRINTLN("[RCC MISSION] 					iAnimation: ", Event.iAnimation)
			PRINTLN("[RCC MISSION] 					bPlaying: ", Event.bPlaying)
		#ENDIF

		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
	ELSE
		PRINTLN("[RCC MISSION] BROADCAST_MOCAP_PLAYER_ANIMATION - playerflags = 0 so not broadcasting") 
	ENDIF	
	
ENDPROC

STRUCT EVENT_STRUCT_MOCAP_PLAYER_REQUEST
	STRUCT_EVENT_COMMON_DETAILS	Details
	INT iTeam
	FMMC_CUTSCENE_TYPE eCutType
ENDSTRUCT

PROC BROADCAST_CUTSCENE_PLAYER_REQUEST(INT iTeam, FMMC_CUTSCENE_TYPE eCutType)
	
	EVENT_STRUCT_MOCAP_PLAYER_REQUEST Event
	Event.Details.Type 				= SCRIPT_EVENT_FMMC_CUTSCENE_PLAYER_REQUEST
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	Event.iTeam 					= iTeam
	Event.eCutType					= eCutType
		
	INT iSendTo = ALL_PLAYERS(TRUE)
	IF NOT (iSendTo = 0)		
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[RCC MISSION]  BROADCAST_CUTSCENE_PLAYER_REQUEST - called...")
			PRINTLN("[RCC MISSION] 					From script: ", GET_THIS_SCRIPT_NAME())
			PRINTLN("[RCC MISSION] 					iTeam: ", Event.iTeam )
		#ENDIF

		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
	ELSE
		PRINTLN("[RCC MISSION] BROADCAST_CUTSCENE_PLAYER_REQUEST - playerflags = 0 so not broadcasting") 
	ENDIF	
	
ENDPROC

STRUCT EVENT_STRUCT_FMMC_PLAY_SYNC_SCENE_FACIAL_ANIM
	STRUCT_EVENT_COMMON_DETAILS	Details
	SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType
	INT iIndex	
	BOOL bUseAlternate
	INT iPart
ENDSTRUCT

PROC BROADCAST_FMMC_PLAY_SYNC_SCENE_FACIAL_ANIM(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType, INT iIndex, BOOL bUseAlternate, INT iPart)
	
	EVENT_STRUCT_FMMC_PLAY_SYNC_SCENE_FACIAL_ANIM Event
	Event.Details.Type = SCRIPT_EVENT_FMMC_PLAY_SYNC_SCENE_FACIAL_ANIM
	Event.Details.FromPlayerIndex = LocalPlayer
	
	Event.eType						= eType
	Event.iIndex 					= iIndex
	Event.bUseAlternate 			= bUseAlternate
	Event.iPart						= iPart
	
	NET_PRINT("-----------------BROADCAST_FMMC_PLAY_SYNC_SCENE_FACIAL_ANIM---------------------------") NET_NL()
	PRINTLN("eType: 		", eType)
	PRINTLN("iIndex: 		", iIndex)
	PRINTLN("bUseAlternate: ", bUseAlternate)
	PRINTLN("iPart: 		", iPart)
	
	INT iSendTo = ALL_PLAYERS(TRUE)
	IF NOT (iSendTo = 0)	
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
	ELSE
		PRINTLN("[RCC MISSION] BROADCAST_CUTSCENE_PLAYER_REQUEST - playerflags = 0 so not broadcasting") 
	ENDIF	
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Peds							------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Event broadcasts for Peds 	------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

STRUCT SCRIPT_EVENT_DATA_FMMC_TRIGGER_AGGRO_FOR_TEAM
	STRUCT_EVENT_COMMON_DETAILS Details
	INT iTeam
	INT iAggroBS
ENDSTRUCT

PROC BROADCAST_FMMC_TRIGGER_AGGRO_FOR_TEAM(INT iTeam, INT iAggroBS = ciMISSION_AGGRO_INDEX_DEFAULT)
	SCRIPT_EVENT_DATA_FMMC_TRIGGER_AGGRO_FOR_TEAM Event
	Event.Details.Type = SCRIPT_EVENT_FMMC_TRIGGER_AGGRO_FOR_TEAM
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	Event.iTeam = iTeam
	Event.iAggroBS = iAggroBS
	
	NET_PRINT("-----------------BROADCAST_FMMC_TRIGGER_AGGRO_FOR_TEAM---------------------------") NET_NL()
	PRINTLN("iTeam: ", iTeam)
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_TRIGGER_AGGRO_FOR_TEAM - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF	
ENDPROC

STRUCT SCRIPT_EVENT_DATA_FMMC_AGGROED_SCRIPT_FORCED_PED
	STRUCT_EVENT_COMMON_DETAILS Details
	INT iPedID
ENDSTRUCT

PROC BROADCAST_FMMC_AGGROED_SCRIPT_FORCED_PED(INT iPed)
	
	SCRIPT_EVENT_DATA_FMMC_AGGROED_SCRIPT_FORCED_PED Event
	
	Event.Details.Type = SCRIPT_EVENT_FMMC_AGGROED_SCRIPT_FORCED_PED
	Event.Details.FromPlayerIndex = PLAYER_ID()
	Event.iPedID = iPed
	
	NET_PRINT("-----------------BROADCAST_FMMC_AGGROED_SCRIPT_FORCED_PED---------------------------") NET_NL()
	NET_PRINT("Event.iPedID = ") NET_PRINT_INT(Event.iPedID) NET_NL()
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // any player could be the host 
	
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_AGGROED_SCRIPT_FORCED_PED - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
	
ENDPROC

   
STRUCT SCRIPT_EVENT_DATA_FMMC_SPAWN_PED_AMMO_PICKUP
	STRUCT_EVENT_COMMON_DETAILS Details
	
	VECTOR vSpawnpos
	INT iIndex
ENDSTRUCT

PROC BROADCAST_FMMC_SPAWN_PED_AMMO_PICKUP(INT iIndex, VECTOR vSpawnPos)
	SCRIPT_EVENT_DATA_FMMC_SPAWN_PED_AMMO_PICKUP Event
	
	Event.Details.Type = SCRIPT_EVENT_FMMC_SPAWN_PED_AMMO_PICKUP
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	Event.iIndex = iIndex
	Event.vSpawnPos = vSpawnPos
	
	NET_PRINT("-----------------BROADCAST_FMMC_SPAWN_PED_AMMO_PICKUP ---------------------------") NET_NL()
	PRINTLN("Event.iIndex: ", Event.iIndex)
	PRINTLN("Event.vSpawnPos: ", Event.vSpawnPos)
	
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_ - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
	
ENDPROC

   
STRUCT SCRIPT_EVENT_DATA_FMMC_REMOVE_PED_AMMO_PICKUP
	STRUCT_EVENT_COMMON_DETAILS Details
	
	INT iIndex
ENDSTRUCT

PROC BROADCAST_FMMC_REMOVE_PED_AMMO_PICKUP(INT iIndex)
	SCRIPT_EVENT_DATA_FMMC_REMOVE_PED_AMMO_PICKUP Event
	
	Event.Details.Type = SCRIPT_EVENT_FMMC_REMOVE_PED_AMMO_PICKUP
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	Event.iIndex = iIndex
	
	NET_PRINT("-----------------BROADCAST_FMMC_REMOVE_PED_AMMO_PICKUP ---------------------------") NET_NL()
	PRINTLN("Event.iIndex: ", Event.iIndex)
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_ - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
	
	
ENDPROC

STRUCT SCRIPT_EVENT_DATA_FMMC_PLAYER_ABILITY_ACTIVATED
	STRUCT_EVENT_COMMON_DETAILS Details
	
	PLAYER_INDEX piActivator
	
ENDSTRUCT

PROC BROADCAST_FMMC_PLAYER_ABILITY_ACTIVATED(PLAYER_INDEX piActivator)
	SCRIPT_EVENT_DATA_FMMC_PLAYER_ABILITY_ACTIVATED Event
	
	Event.Details.Type = SCRIPT_EVENT_FMMC_PLAYER_ABILITY_ACTIVATED
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	Event.piActivator = piActivator
		
	NET_PRINT("-----------------BROADCAST_FMMC_PLAYER_ACTIVATED_ABILITY ---------------------------") NET_NL()
	PRINTLN("BROADCAST_FMMC_PLAYER_ABILITY_ACTIVATED - Event.piActivator: ", NATIVE_TO_INT(Event.piActivator))
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_PLAYER_ACTIVATED_ABILITY - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: CCTV		 -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Event broadcasts for CCTV functionality  		--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

STRUCT SCRIPT_EVENT_DATA_FMMC_CCTV_EVENT
	STRUCT_EVENT_COMMON_DETAILS Details
	
	INT iCam
	CCTV_EVENT_TYPE eEventType
	
	PLAYER_INDEX PlayerSpotted
	INT iPedBodySpotted = -1
	INT iAggroBS
ENDSTRUCT

PROC BROADCAST_FMMC_CCTV_EVENT(INT iCam, CCTV_EVENT_TYPE eEventType, INT iAggroBS, PLAYER_INDEX PlayerSpotted = NULL, INT iPedBodySpotted = -1)
	SCRIPT_EVENT_DATA_FMMC_CCTV_EVENT Event
	Event.Details.Type = SCRIPT_EVENT_FMMC_CCTV_EVENT
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	Event.iCam = iCam
	Event.eEventType = eEventType
	
	Event.PlayerSpotted = PlayerSpotted
	Event.iPedBodySpotted = iPedBodySpotted
	
	Event.iAggroBS = iAggroBS
	
	NET_PRINT("-----------------BROADCAST_FMMC_CCTV_EVENT---------------------------") NET_NL()
	NET_PRINT("Event.iCam = ") NET_PRINT_INT(Event.iCam) NET_NL()
	NET_PRINT("Event.eEventType = ") NET_PRINT_INT(ENUM_TO_INT(Event.eEventType)) NET_NL()
	NET_PRINT("Event.PlayerSpotted = ") NET_PRINT_INT(NATIVE_TO_INT(Event.PlayerSpotted)) NET_NL()
	NET_PRINT("Event.iPedBodySpotted = ") NET_PRINT_INT(Event.iPedBodySpotted) NET_NL()	
	NET_PRINT("Event.iAggroBS = ") NET_PRINT_INT(Event.iAggroBS) NET_NL()
	
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), ALL_PLAYERS_ON_SCRIPT())
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: World		 ---------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Event broadcasts for World functionality  		----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

STRUCT SCRIPT_EVENT_DATA_FMMC_SET_ARTIFICIAL_LIGHTS
	STRUCT_EVENT_COMMON_DETAILS Details
	
	eARTIFICIAL_LIGHTS_STATE eStateToSet
	BOOL bPlaySound
	
	VECTOR vEMPRestrictedInteriorOrigin
	INT iEMPDuration = -1
	
ENDSTRUCT

PROC BROADCAST_FMMC_SET_ARTIFICIAL_LIGHTS(eARTIFICIAL_LIGHTS_STATE eStateToSet, BOOL bPlaySound = TRUE, ENTITY_INDEX eiEMPOrigin = NULL, INT iEMPDuration = -1)
	SCRIPT_EVENT_DATA_FMMC_SET_ARTIFICIAL_LIGHTS Event
	Event.Details.Type = SCRIPT_EVENT_FMMC_SET_ARTIFICIAL_LIGHTS
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	Event.eStateToSet = eStateToSet
	Event.bPlaySound = bPlaySound
	Event.iEMPDuration = iEMPDuration
	
	IF DOES_ENTITY_EXIST(eiEMPOrigin)
		Event.vEMPRestrictedInteriorOrigin = GET_ENTITY_COORDS(eiEMPOrigin)
	ENDIF
	
	NET_PRINT("-----------------BROADCAST_FMMC_SET_ARTIFICIAL_LIGHTS---------------------------") NET_NL()
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_SET_ARTIFICIAL_LIGHTS - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF	
ENDPROC

STRUCT SCRIPT_EVENT_DATA_FMMC_INTERIOR_DESTRUCTION_EVENT
	STRUCT_EVENT_COMMON_DETAILS Details
	INT iInteriorDestructionToStart = -1
ENDSTRUCT

PROC BROADCAST_FMMC_INTERIOR_DESTRUCTION_EVENT(INT iInteriorDestructionToStart = -1)
	SCRIPT_EVENT_DATA_FMMC_INTERIOR_DESTRUCTION_EVENT Event
	Event.Details.Type = SCRIPT_EVENT_FMMC_INTERIOR_DESTRUCTION
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	Event.iInteriorDestructionToStart = iInteriorDestructionToStart
	NET_PRINT("-----------------BROADCAST_FMMC_INTERIOR_DESTRUCTION_EVENT---------------------------") NET_NL()
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_INTERIOR_DESTRUCTION_EVENT - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Interactables		 -------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Event broadcasts for Interactable functionality  		------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC STRING GET_FMMC_INTERACTABLE_EVENT_TYPE_STRING(FMMC_INTERACTABLE_EVENT_TYPE eInteractableEventType)
	SWITCH eInteractableEventType
		CASE FMMC_INTERACTABLE_EVENT_TYPE__PLAYER_STARTED_INTERACTING					RETURN "PLAYER_STARTED_INTERACTING"
		CASE FMMC_INTERACTABLE_EVENT_TYPE__PLAYER_STOPPED_INTERACTING					RETURN "PLAYER_STOPPED_INTERACTING"
		CASE FMMC_INTERACTABLE_EVENT_TYPE__INTERACTION_COMPLETE							RETURN "INTERACTION_COMPLETE"
		CASE FMMC_INTERACTABLE_EVENT_TYPE__INTERACTION_NO_LONGER_COMPLETE				RETURN "INTERACTION_NO_LONGER_COMPLETE"
		CASE FMMC_INTERACTABLE_EVENT_TYPE__RESET_INTERACTABLE							RETURN "RESET_INTERACTABLE"
		CASE FMMC_INTERACTABLE_EVENT_TYPE__SYNCLOCK_ENGAGED								RETURN "SYNCLOCK_ENGAGED"
		CASE FMMC_INTERACTABLE_EVENT_TYPE__SYNCLOCK_DISENGAGED							RETURN "SYNCLOCK_DISENGAGED"
		CASE FMMC_INTERACTABLE_EVENT_TYPE__INTERACTION_COMPLETED_ON_PREVIOUS_MISSION	RETURN "INTERACTION_COMPLETED_ON_PREVIOUS_MISSION"
	ENDSWITCH
	
	RETURN "INVALID EVENT"
ENDFUNC

STRUCT SCRIPT_EVENT_DATA_FMMC_INTERACTABLE_EVENT
	STRUCT_EVENT_COMMON_DETAILS Details
	INT iEventSenderParticipant
	
	FMMC_INTERACTABLE_EVENT_TYPE eInteractableEventType
	INT iInteractable
	INT iInteractableEventBS
ENDSTRUCT

PROC BROADCAST_FMMC_INTERACTABLE_EVENT(FMMC_INTERACTABLE_EVENT_TYPE eInteractableEventType, INT iInteractable, INT iInteractableEventBS = 0)
	SCRIPT_EVENT_DATA_FMMC_INTERACTABLE_EVENT Event
	
	Event.Details.Type = SCRIPT_EVENT_FMMC_INTERACTABLE
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	Event.iEventSenderParticipant = iLocalPart
	Event.eInteractableEventType = eInteractableEventType
	Event.iInteractable = iInteractable
	Event.iInteractableEventBS = iInteractableEventBS
	
	PRINTLN("[Interactables][Interactable ", Event.iInteractable, "] BROADCAST_FMMC_INTERACTABLE_EVENT || eInteractableEventType: ", Event.eInteractableEventType, " / ", GET_FMMC_INTERACTABLE_EVENT_TYPE_STRING(Event.eInteractableEventType), " / Event.iInteractableEventBS: ", Event.iInteractableEventBS)
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // any player could be the host 
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: HUD		 -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Event broadcasts for HUD functionality  		--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENTS aTicker, INT iScore = 0, INT iTeam = -1, INT iSubLogic = -1, PLAYER_INDEX piPlayer = NULL, INT iEntityType = -1, INT iEntityNum = -1, BOOL bIncludeSelf = TRUE, INT iShardOption = 0, BOOL bBlockAudio = FALSE)
	
	DEBUG_PRINTCALLSTACK()
	SCRIPT_EVENT_DATA_TICKER_CUSTOM_MESSAGE TickerMessage
	
	TickerMessage.TickerEvent = aTicker
	TickerMessage.iEntityNum = iEntityNum
	INT iPriority = FMMC_PRIORITY_IGNORE
	BOOL bBroadcast
	INT iReturnTeam = -1
	INT iTeamToUse
	
	IF iTeam > -1
		
		IF iEntityType > -1
		AND iEntityNum > -1
			iPriority = GET_ENTITY_PRIORITY(iTeam, iEntityType, iEntityNum)
		ENDIF
		
		IF iPriority >= FMMC_MAX_RULES
			iPriority = GET_ENTITY_PRIORITY_FOR_ANY_TEAM(iEntityType, iEntityNum, iReturnTeam)
		ENDIF
		
		IF iPriority >= FMMC_MAX_RULES
			iPriority = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		ENDIF
		
		IF iReturnTeam > -1
			iTeamToUse = iReturnTeam
		ELSE
			iTeamToUse = iTeam
		ENDIF
		
		IF iPriority < FMMC_MAX_RULES
			bBroadcast = TRUE
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamToUse].iRuleBitsetFifteen[iPriority], ciBS_RULE15_HIDE_DELIVER_TICKERS)
				IF aTicker = TICKER_EVENT_DELIVER_OBJ
				OR aTicker = TICKER_EVENT_DELIVER_PED
				OR aTicker = TICKER_EVENT_DELIVER_VEH
					bBroadcast = FALSE
				ENDIF	
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamToUse].iRuleBitset[iPriority], ciBS_RULE_HIDE_TICKERS)
				bBroadcast = FALSE
			ENDIF
		ENDIF
		
		PRINTLN("BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS: iEntityType: ", iEntityType, ", iEntityNum: ", iEntityNum, ", iPriority: ", iPriority, ", iteamtouse: ", iTeamToUse, ", bBroadcast: ", bBroadcast)
	ENDIF
	
	IF bBroadcast
		IF iScore > 0
			TickerMessage.dataInt = iScore
		ENDIF
		
		TickerMessage.TeamInt = iTeam
		
		TickerMessage.playerID = piPlayer
		
		TickerMessage.iSubType = iSubLogic
		
		TickerMessage.iTeamToUse = iTeamToUse
		TickerMessage.iRule = iPriority
		TickerMessage.blockAudio = bBlockAudio
		TickerMessage.dataDisplayOption = iShardOption
		BROADCAST_CUSTOM_TICKER_EVENT(TickerMessage, ALL_PLAYERS_ON_SCRIPT(bIncludeSelf))
	ENDIF
	
ENDPROC

STRUCT SCRIPT_EVENT_DATA_FMMC_REMOVE_DUMMY_BLIP
	STRUCT_EVENT_COMMON_DETAILS Details
	INT iDummyBlipIndex
ENDSTRUCT

PROC BROADCAST_FMMC_REMOVE_DUMMY_BLIP(INT iDummyBlipIndex)
	
	SCRIPT_EVENT_DATA_FMMC_REMOVE_DUMMY_BLIP Event
	
	Event.Details.Type = SCRIPT_EVENT_FMMC_REMOVE_DUMMY_BLIP
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	Event.iDummyBlipIndex = iDummyBlipIndex
	
	NET_PRINT("-----------------BROADCAST_FMMC_REMOVE_DUMMY_BLIP ---------------------------") NET_NL()
	PRINTLN("BROADCAST_FMMC_REMOVE_DUMMY_BLIP - Event.iDummyBlipIndex: ", Event.iDummyBlipIndex)
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_ - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
	
ENDPROC
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Grabbed Cash
// ##### Description: Events for grabbing cash mid-mission to add to the take - whether it be from the Cash Grab interaction, pickups in the world or other sources
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

STRUCT SCRIPT_EVENT_DATA_FMMC_REMOVE_GRABBED_CASH
	STRUCT_EVENT_COMMON_DETAILS Details
	INT iCashToRemove
	INT iRedFlashTime
ENDSTRUCT

PROC BROADCAST_FMMC_REMOVE_GRABBED_CASH(INT iCashToRemove, INT iRedFlashTime = 500)
	SCRIPT_EVENT_DATA_FMMC_REMOVE_GRABBED_CASH Event
	
	Event.Details.Type = SCRIPT_EVENT_FMMC_REMOVE_GRABBED_CASH
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	Event.iCashToRemove = iCashToRemove
	Event.iRedFlashTime = iRedFlashTime
	
	NET_PRINT("-----------------BROADCAST_FMMC_REMOVE_GRABBED_CASH---------------------------") NET_NL()
	PRINTLN("[GrabbedCash] BROADCAST_FMMC_REMOVE_GRABBED_CASH - Event.iCashToRemove: ", Event.iCashToRemove)
	PRINTLN("[GrabbedCash] BROADCAST_FMMC_REMOVE_GRABBED_CASH - Event.iRedFlashTime: ", Event.iRedFlashTime)
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("[GrabbedCash] BROADCAST_FMMC_REMOVE_GRABBED_CASH - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
ENDPROC

STRUCT SCRIPT_EVENT_DATA_FMMC_ADD_GRABBED_CASH
	STRUCT_EVENT_COMMON_DETAILS Details
	INT iCashToAdd
	INT iCashSource
ENDSTRUCT

PROC BROADCAST_FMMC_ADD_GRABBED_CASH(INT iCashToAdd, INT iCashSource = ciGRABBED_CASH_SOURCE__MISC)
	SCRIPT_EVENT_DATA_FMMC_ADD_GRABBED_CASH Event
	
	Event.Details.Type = SCRIPT_EVENT_FMMC_ADD_GRABBED_CASH
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	Event.iCashToAdd = iCashToAdd
	Event.iCashSource = iCashSource
	
	NET_PRINT("-----------------BROADCAST_FMMC_ADD_GRABBED_CASH---------------------------") NET_NL()
	PRINTLN("[GrabbedCash] BROADCAST_FMMC_ADD_GRABBED_CASH - Event.iCashToAdd: ", Event.iCashToAdd, " | Event.iCashSource: ", Event.iCashSource)
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("[GrabbedCash] BROADCAST_FMMC_ADD_GRABBED_CASH - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Locations -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Event broadcasts for Locations functionality  --------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

   
STRUCT SCRIPT_EVENT_DATA_FMMC_LOCATION_INPUT_TRIGGERED
	STRUCT_EVENT_COMMON_DETAILS Details
	
	INT iLoc
ENDSTRUCT

PROC BROADCAST_FMMC_LOCATION_INPUT_TRIGGERED(INT iLoc)
	SCRIPT_EVENT_DATA_FMMC_LOCATION_INPUT_TRIGGERED Event
	
	Event.Details.Type = SCRIPT_EVENT_FMMC_LOCATION_INPUT_TRIGGERED
	Event.Details.FromPlayerIndex = PLAYER_ID()
	Event.iLoc = iLoc
	
	NET_PRINT("-----------------BROADCAST_FMMC_LOCATION_INPUT_TRIGGERED ---------------------------") NET_NL()
	PRINTLN("Event.iLoc = ", Event.iLoc)
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_ - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
	
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Pickups
// ##### Description: Structs and broadcast functions for pickups.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
STRUCT SCRIPT_EVENT_DATA_FMMC_MISSION_EQUIPMENT
	STRUCT_EVENT_COMMON_DETAILS Details
	PLAYER_INDEX piEquipmentCollector
	INT iPickupIndex
ENDSTRUCT

PROC BROADCAST_FMMC_MISSION_EQUIPMENT(PLAYER_INDEX piEquipmentCollector, INT iPickupIndex)
	SCRIPT_EVENT_DATA_FMMC_MISSION_EQUIPMENT Event
	
	Event.Details.Type = SCRIPT_EVENT_FMMC_MISSION_EQUIPMENT
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	Event.piEquipmentCollector = piEquipmentCollector
	Event.iPickupIndex = iPickupIndex
	
	NET_PRINT("-----------------BROADCAST_FMMC_MISSION_EQUIPMENT ---------------------------") NET_NL()
	PRINTLN("[MissionEquipment] BROADCAST_FMMC_MISSION_EQUIPMENT | Broadcasting that mission equipment pickup ", iPickupIndex, " has been picked up by me!")
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_ - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
ENDPROC

STRUCT SCRIPT_EVENT_DATA_FMMC_SPAWN_PICKUP
	STRUCT_EVENT_COMMON_DETAILS Details	
	INT iPickupId
	VECTOR vPickupLocation
ENDSTRUCT

PROC BROADCAST_FMMC_SPAWN_PICKUP(INT iPickupId, VECTOR vPickupLocation)
	SCRIPT_EVENT_DATA_FMMC_SPAWN_PICKUP Event
	Event.Details.Type = SCRIPT_EVENT_FMMC_SPAWN_PICKUP
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	Event.iPickupId = iPickupId
	Event.vPickupLocation = vPickupLocation
	
	NET_PRINT("-----------------BROADCAST_FMMC_SPAWN_PICKUP ---------------------------") NET_NL()
	PRINTLN("[MissionEquipment] BROADCAST_FMMC_SPAWN_PICKUP | Broadcasting spawn of pickup with id ", iPickupId)
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_ - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Model Swap
// ##### Description: Structs and broadcast functions for model swap events.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

STRUCT SCRIPT_EVENT_DATA_FMMC_MODEL_SWAP
	STRUCT_EVENT_COMMON_DETAILS Details
	
	MODEL_SWAP_EVENT_TYPE eModelSwapEventType
	FMMC_MODEL_SWAP_CONFIG sModelSwapConfig
ENDSTRUCT

PROC BROADCAST_FMMC_MODEL_SWAP_EVENT(MODEL_SWAP_EVENT_TYPE eModelSwapEventType, FMMC_MODEL_SWAP_CONFIG& sModelSwapConfig)
	SCRIPT_EVENT_DATA_FMMC_MODEL_SWAP Event
	
	Event.Details.Type = SCRIPT_EVENT_FMMC_MODEL_SWAP
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	Event.eModelSwapEventType = eModelSwapEventType
	Event.sModelSwapConfig = sModelSwapConfig
	
	NET_PRINT("-----------------BROADCAST_FMMC_MODEL_SWAP_EVENT ---------------------------") NET_NL()
	SWITCH eModelSwapEventType
		CASE MODEL_SWAP_EVENT_TYPE__ADD
			PRINTLN("[ModelSwaps] Sending a MODEL_SWAP_EVENT_TYPE__ADD event!")
		BREAK
		CASE MODEL_SWAP_EVENT_TYPE__PERFORM
			PRINTLN("[ModelSwaps] Sending a MODEL_SWAP_EVENT_TYPE__PERFORM event!")
		BREAK
		CASE MODEL_SWAP_EVENT_TYPE__REVERSE
			PRINTLN("[ModelSwaps] Sending a MODEL_SWAP_EVENT_TYPE__REVERSE event!")
		BREAK
	ENDSWITCH
			
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_ - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
ENDPROC

FUNC BOOL REGISTER_MODEL_SWAP(FMMC_MODEL_SWAP_CONFIG& sModelSwapConfig)
	IF iCurrentActiveModelSwaps >= ciMAX_MODEL_SWAPS
		ASSERTLN("[ModelSwaps] REGISTER_MODEL_SWAP | All slots are full!")
		PRINTLN("[ModelSwaps] REGISTER_MODEL_SWAP | All slots are full!")
		RETURN FALSE
	ENDIF
	
	IF GET_ACTIVE_MODEL_SWAP_INDEX(sModelSwapConfig.iModelSwapHash) > -1
		PRINTLN("[ModelSwaps] REGISTER_MODEL_SWAP | Model swap ", sModelSwapConfig.iModelSwapHash, " is already registered!")
		RETURN FALSE
	ENDIF
	
	IF sModelSwapConfig.sEntityToSwapOut.iEntityIndex = -1
	AND NOT NETWORK_DOES_NETWORK_ID_EXIST(sModelSwapConfig.sEntityToSwapOut.niEntityNetID)
		PRINTLN("[ModelSwaps] REGISTER_MODEL_SWAP | Trying to register a model swap with no swap-out entity!")
		RETURN FALSE
	ENDIF
	
	BROADCAST_FMMC_MODEL_SWAP_EVENT(MODEL_SWAP_EVENT_TYPE__ADD, sModelSwapConfig)
	RETURN TRUE
ENDFUNC

FUNC BOOL REGISTER_SIMPLE_MODEL_SWAP(INT iModelSwapHash, NETWORK_INDEX niSwapOut, MODEL_NAMES mnModelToSwapIn)
	FMMC_MODEL_SWAP_CONFIG sModelSwapConfig
	sModelSwapConfig.iModelSwapHash = iModelSwapHash
	
	sModelSwapConfig.sEntityToSwapOut.eModelSwapType = MODEL_SWAP_ENTITY_TYPE__NETWORKED_OBJECT
	sModelSwapConfig.sEntityToSwapOut.niEntityNetID = niSwapOut
	
	sModelSwapConfig.sEntityToSwapIn.eModelSwapType = MODEL_SWAP_ENTITY_TYPE__MODEL_SWAP_PROP
	sModelSwapConfig.sEntityToSwapIn.mnModelSwapPropModelName = mnModelToSwapIn
	
	RETURN REGISTER_MODEL_SWAP(sModelSwapConfig)
ENDFUNC

PROC PREPARE_MODEL_SWAP_REVERSE(INT iModelSwapHash)
	FMMC_MODEL_SWAP_CONFIG sModelSwapConfig
	sModelSwapConfig.iModelSwapHash = iModelSwapHash
	BROADCAST_FMMC_MODEL_SWAP_EVENT(MODEL_SWAP_EVENT_TYPE__REVERSE, sModelSwapConfig)
ENDPROC

PROC START_MODEL_SWAP(INT iModelSwapHash)
	FMMC_MODEL_SWAP_CONFIG sModelSwapConfig
	sModelSwapConfig.iModelSwapHash = iModelSwapHash
	BROADCAST_FMMC_MODEL_SWAP_EVENT(MODEL_SWAP_EVENT_TYPE__PERFORM, sModelSwapConfig)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Props
// ##### Description: Structs and broadcast functions for props.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

STRUCT SCRIPT_EVENT_DATA_FMMC_LOCH_SANTOS_MONSTER
	STRUCT_EVENT_COMMON_DETAILS Details
ENDSTRUCT

PROC BROADCAST_FMMC_LOCH_SANTOS_MONSTER()
	SCRIPT_EVENT_DATA_FMMC_LOCH_SANTOS_MONSTER Event
	
	Event.Details.Type = SCRIPT_EVENT_FMMC_LOCH_SANTOS_MONSTER
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	NET_PRINT("-----------------SCRIPT_EVENT_DATA_FMMC_LOCH_SANTOS_MONSTER ---------------------------") NET_NL()
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_ - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
	
	
ENDPROC



// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Objects
// ##### Description: Structs and broadcast functions for objects.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
STRUCT SCRIPT_EVENT_DATA_FMMC_OBJECT_CARRIER_ALERT
	STRUCT_EVENT_COMMON_DETAILS Details
	
	PLAYER_INDEX piCarrier
	
ENDSTRUCT

PROC BROADCAST_FMMC_OBJECT_CARRIER_ALERT(PLAYER_INDEX piCarrier)
	SCRIPT_EVENT_DATA_FMMC_OBJECT_CARRIER_ALERT Event
	
	Event.Details.Type = SCRIPT_EVENT_FMMC_OBJECT_CARRIER_ALERT
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	Event.piCarrier = piCarrier
		
	NET_PRINT("-----------------BROADCAST_FMMC_OBJECT_CARRIER_ALERT ---------------------------") NET_NL()
	PRINTLN("BROADCAST_FMMC_OBJECT_CARRIER_ALERT - Event.piCarrier: ", NATIVE_TO_INT(Event.piCarrier))
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_OBJECT_CARRIER_ALERT - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
ENDPROC


				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicles
// ##### Description: Event broadcasting functions for vehicles
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

   
STRUCT SCRIPT_EVENT_DATA_FMMC_SET_VEHICLE_IMMOVABLE
	STRUCT_EVENT_COMMON_DETAILS Details
	BOOL bImmovable
	BOOL bHasChanged
	INT iVehicle
ENDSTRUCT

PROC BROADCAST_FMMC_SET_VEHICLE_IMMOVABLE(INT iVehicle, BOOL bImmovable, BOOL bHasChanged)
	SCRIPT_EVENT_DATA_FMMC_SET_VEHICLE_IMMOVABLE Event
	
	Event.Details.Type = SCRIPT_EVENT_FMMC_SET_VEHICLE_IMMOVABLE
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	Event.bImmovable = bImmovable
	Event.iVehicle = iVehicle
	Event.bHasChanged = bHasChanged
	
	NET_PRINT("-----------------BROADCAST_FMMC_SET_VEHICLE_IMMOVABLE ---------------------------") NET_NL()
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_SET_VEHICLE_IMMOVABLE - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
	
	
ENDPROC

   
STRUCT SCRIPT_EVENT_DATA_FMMC_REMOTE_VEHICLE_EXPLOSION
	STRUCT_EVENT_COMMON_DETAILS Details
	
	INT iInitiator
ENDSTRUCT

PROC BROADCAST_FMMC_REMOTE_VEHICLE_EXPLOSION(INT iInitiator)
	SCRIPT_EVENT_DATA_FMMC_REMOTE_VEHICLE_EXPLOSION Event
	
	Event.Details.Type = SCRIPT_EVENT_FMMC_REMOTE_VEHICLE_EXPLOSION
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	Event.iInitiator = iInitiator
		
	NET_PRINT("[RemoteExplosive]-----------------BROADCAST_FMMC_REMOTE_VEHICLE_EXPLOSION ---------------------------") NET_NL()
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_REMOTE_VEHICLE_EXPLOSION - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
	
ENDPROC					

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Zones
// ##### Description: Structs and broadcast functions for zones.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
STRUCT SCRIPT_EVENT_DATA_FMMC_SOUND_ZONE
	STRUCT_EVENT_COMMON_DETAILS Details
	INT iSoundType = 0
	VECTOR vSoundLocation
	INT iRange
ENDSTRUCT

PROC BROADCAST_FMMC_SOUND_ZONE_SOUND(INT iSoundType, VECTOR vSoundLocation, INT iRange)
	SCRIPT_EVENT_DATA_FMMC_SOUND_ZONE Event
	
	Event.Details.Type = SCRIPT_EVENT_FMMC_SOUND_ZONE
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	Event.iSoundType = iSoundType
	Event.vSoundLocation = vSoundLocation
	Event.iRange = iRange
	
	NET_PRINT("-----------------BROADCAST_FMMC_SOUND_ZONE_SOUND---------------------------") NET_NL()
	PRINTLN("Event.iSoundType: ", Event.iSoundType)
	PRINTLN("Event.vSoundLocation: ", Event.vSoundLocation)
	PRINTLN("Event.iRange: ", Event.iRange)
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_SOUND_ZONE_SOUND - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
	
	
ENDPROC					
					
STRUCT SCRIPT_EVENT_DATA_FMMC_ZONE_AIR_DEFENCE_SHOT
	STRUCT_EVENT_COMMON_DETAILS Details
	INT iZone
	VECTOR vShotPos
ENDSTRUCT

PROC BROADCAST_FMMC_AIR_DEFENCE_SHOT(INT iZone, VECTOR vShotPos)
	SCRIPT_EVENT_DATA_FMMC_ZONE_AIR_DEFENCE_SHOT Event
	
	Event.Details.Type = SCRIPT_EVENT_FMMC_ZONE_AIR_DEFENCE_SHOT
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	Event.iZone = iZone
	Event.vShotPos = vShotPos
	
	NET_PRINT("-----------------BROADCAST_FMMC_AIR_DEFENCE_SHOT---------------------------") NET_NL()
	PRINTLN("Event.iZone: ", Event.iZone)
	PRINTLN("Event.vShotPos: ", Event.vShotPos)
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_AIR_DEFENCE_SHOT - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
ENDPROC		

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Generic Render Targets
// ##### Description: Functions related to the shared render target system which can be used by all entity types.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
STRUCT SCRIPT_EVENT_DATA_FMMC_SHARED_RENDERTARGET_EVENT
	STRUCT_EVENT_COMMON_DETAILS Details
	
	INT iSharedRTEventType = ciSHARED_RT_EVENT_TYPE__NONE
	
	INT iEntityType = CREATION_TYPE_NONE
	INT iEntityIndex = -1
	MODEL_NAMES mnEntityModel
	
	INT iScaleformMethodType = ciSHARED_RT_EVENT_METHOD__NONE
	INT iScaleformMethodInt
	FLOAT fScaleformMethodFloat
ENDSTRUCT

PROC BROADCAST_FMMC_SHARED_RENDERTARGET_EVENT(INT iSharedRTEventType, INT iEntityType, INT iEntityIndex, INT iScaleformMethodType = ciSHARED_RT_EVENT_METHOD__NONE, MODEL_NAMES mnEntityModel = DUMMY_MODEL_FOR_SCRIPT, INT iScaleformMethodInt = -1, FLOAT fScaleformMethodFloat = 0.0)
	SCRIPT_EVENT_DATA_FMMC_SHARED_RENDERTARGET_EVENT Event
	
	Event.Details.Type = SCRIPT_EVENT_FMMC_SHARED_RENDERTARGET_EVENT
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	Event.iSharedRTEventType = iSharedRTEventType
	Event.iEntityType = iEntityType
	Event.iEntityIndex = iEntityIndex
	Event.mnEntityModel = mnEntityModel
	
	Event.iScaleformMethodType = iScaleformMethodType
	Event.iScaleformMethodInt = iScaleformMethodInt
	Event.fScaleformMethodFloat = fScaleformMethodFloat
	
	NET_PRINT("-----------------BROADCAST_FMMC_SHARED_RENDERTARGET_EVENT---------------------------") NET_NL()
	PRINTLN("[SharedRTs][RT ", GET_SHARED_RENDERTARGET_INDEX_FOR_ENTITY(iEntityType, iEntityIndex), "] BROADCAST_FMMC_SHARED_RENDERTARGET_EVENT | Broadcasting event of type ", iSharedRTEventType, " || Event.iScaleformMethodType: ", Event.iScaleformMethodType, " / Event.iScaleformMethodInt: ", Event.iScaleformMethodInt, " / Event.fScaleformMethodFloat: ", Event.fScaleformMethodFloat)
	DEBUG_PRINTCALLSTACK()
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_SHARED_RENDERTARGET_EVENT - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Audio
// ##### Description: Structs and broadcast functions for zones.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
STRUCT SCRIPT_EVENT_DATA_FMMC_SET_BILLIONAIRE_PARTY_STATE
	STRUCT_EVENT_COMMON_DETAILS Details
	
	BILLIONAIRE_PARTY_MUSIC eNewState
ENDSTRUCT

PROC BROADCAST_FMMC_SET_BILLIONAIRE_PARTY_STATE(BILLIONAIRE_PARTY_MUSIC eNewState)
	SCRIPT_EVENT_DATA_FMMC_SET_BILLIONAIRE_PARTY_STATE Event
	
	Event.Details.Type = SCRIPT_EVENT_FMMC_SET_BILLIONAIRE_PARTY_STATE
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	Event.eNewState = eNewState
	
	NET_PRINT("-----------------BROADCAST_FMMC_SET_BILLIONAIRE_PARTY_STATE ---------------------------") NET_NL()
	PRINTLN("BROADCAST_FMMC_SET_BILLIONAIRE_PARTY_STATE Event.eNewState: ", Event.eNewState)
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_ - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
	
	
ENDPROC
