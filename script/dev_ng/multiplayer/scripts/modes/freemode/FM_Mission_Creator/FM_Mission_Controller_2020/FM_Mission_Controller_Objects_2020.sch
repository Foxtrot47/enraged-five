// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Objects -----------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script Name: FM_Mission_Controller_2020.sc ------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Performs staggered and every frame processing on placed mission objects.  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_ObjectsElectronics_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Object Utility Functions ----------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Basic utility/helper functions that can be used throughout the object system.  -----------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC VECTOR GET_OBJECT_DROPOFF_COORDS(INT iObj)

	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vObjDropOffOverride)
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vObjDropOffOverride
	ENDIF
	
	INT iObjPriority = MC_serverBD_4.iObjPriority[iObj][GET_LOCAL_PLAYER_TEAM(TRUE)]
	IF iObjPriority > -1
		RETURN g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM(TRUE)].vDropOff[iObjPriority]
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

PROC CLEAN_UP_CONTAINER_DOORS()
	
	INT iContainerObjectIndex
	
	IF DO_CRATE_DOORS_EXIST(MC_serverBD_1.niMinigameCrateDoors, FALSE)
		FOR iContainerObjectIndex = 0 TO ciMAX_MINIGAME_CRATE_DOORS - 1
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.niMinigameCrateDoors[iContainerObjectIndex])
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.niMinigameCrateDoors[iContainerObjectIndex])
					DETACH_ENTITY(NET_TO_OBJ(MC_serverBD_1.niMinigameCrateDoors[iContainerObjectIndex]))
				ENDIF
				CLEANUP_NET_ID(MC_serverBD_1.niMinigameCrateDoors[iContainerObjectIndex])
			ENDIF
		ENDFOR
	ELSE
		CPRINTLN(DEBUG_CONTROLLER, "{RCC MISSION] Trying to clean up the hack container doors when we've already done so!")
	ENDIF

ENDPROC

FUNC BOOL IS_CRATE_STUCK(OBJECT_INDEX tempObj,INT iobj)

    CONST_FLOAT SAFE_HANDLER_CONTAINER_ROT              35.0
	VECTOR vHandlerContainerRot = GET_ENTITY_ROTATION(tempObj)

	IF GET_TEAM_HOLDING_PACKAGE(iobj) > -1
		RETURN FALSE
	ENDIF
	
    IF NOT HAS_NET_TIMER_STARTED(tdCrateFailTimer[iobj])
        IF vHandlerContainerRot.y > SAFE_HANDLER_CONTAINER_ROT OR vHandlerContainerRot.y < -SAFE_HANDLER_CONTAINER_ROT
           REINIT_NET_TIMER(tdCrateFailTimer[iobj])
           PRINTLN("starting Fail timer for Y: ",iobj)
        ENDIF
		IF vHandlerContainerRot.x > SAFE_HANDLER_CONTAINER_ROT OR vHandlerContainerRot.x < -SAFE_HANDLER_CONTAINER_ROT
			REINIT_NET_TIMER(tdCrateFailTimer[iobj])
            PRINTLN("starting Fail timer for x: ",iobj)
        ENDIF
    ELSE 
		IF vHandlerContainerRot.y < SAFE_HANDLER_CONTAINER_ROT AND vHandlerContainerRot.y > -SAFE_HANDLER_CONTAINER_ROT
		AND vHandlerContainerRot.x < SAFE_HANDLER_CONTAINER_ROT AND vHandlerContainerRot.x > -SAFE_HANDLER_CONTAINER_ROT 
           RESET_NET_TIMER(tdCrateFailTimer[iobj])
           PRINTLN("STOP Fail timer: ",iobj)
		   RETURN FALSE
        ENDIF
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdCrateFailTimer[iobj]) > 20000
			RETURN TRUE
		ENDIF
    ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_WE_CLEANUP_OBJECT_DUE_TO_IT_BEING_DEAD(INT iObj)	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetThree, cibsOBJ3_DontCleanUpWhenDead)
		PRINTLN("PROCESS_OBJ_BRAIN - SHOULD_WE_CLEANUP_OBJECT_DUE_TO_IT_BEING_DEAD - Refusing to clean up/delete this dead object due to cibsOBJ3_DontCleanUpWhenDead. Object: ", iObj)
		RETURN FALSE
	ENDIF
			
	RETURN TRUE
ENDFUNC

FUNC STRING GET_REMOVAL_PTFX_NAME(MODEL_NAMES tempModel)
	STRING tempName = "scr_mp_generic_dst"

	SWITCH tempModel
		CASE PROP_DRUG_PACKAGE
		CASE PROP_LD_CASE_01
			tempName = "scr_mp_drug_dst"
		BREAK
		
		CASE PROP_LAPTOP_01A
		CASE PROP_BOMB_01
		CASE reh_prop_reh_drone_02a
			tempName = "scr_mp_elec_dst"
		BREAK
	ENDSWITCH
	
	RETURN tempName
ENDFUNC

FUNC BOOL DOES_OBJECT_HAVE_ANY_PRIORITY_FOR_ANY_TEAM(INT iobj)
	
	BOOL bHasPriority = FALSE
	
	INT iTeamLoop
	
	FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF (MC_serverBD_4.iObjPriority[iobj][iTeamLoop] >= 0)
		AND (MC_serverBD_4.iObjPriority[iobj][iTeamLoop] < FMMC_MAX_RULES)
			IF (MC_serverBD.iNumberOfPlayingPlayers[iTeamLoop] > 0)
				bHasPriority = TRUE
				iTeamLoop = MC_serverBD.iNumberOfTeams //Break out, we already know someone has priority left
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN bHasPriority
	
ENDFUNC

FUNC BOOL OBJECT_WAS_DROPPED_IN_END_ZONE(object_INDEX obj)
	
	INT iTeam = GET_LOCAL_PLAYER_TEAM(TRUE)
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]

	INT i = 0
	IF iRule < FMMC_MAX_RULES
		FOR i = 0 TO (FMMC_MAX_TEAMS - 1)
			IF IS_POINT_IN_ANGLED_AREA(GET_ENTITY_COORDS(obj), g_FMMC_STRUCT.sFMMCEndConditions[i].vDropOff[iRule], 
			g_FMMC_STRUCT.sFMMCEndConditions[i].vDropOff2[iRule], 
			g_FMMC_STRUCT.sFMMCEndConditions[i].fDropOffRadius[iRule])
				IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(obj)
					PRINTLN("[KH] OBJECT_WAS_DROPPED_IN_END_ZONE - OBJECT DROPPED IN END ZONE - RESETTING OBJECT TO SPAWN")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//FMMC2020 - Does this need renamed? Used in Explode Object Outside of Zone
PROC FIND_AND_STORE_SNIPERBALL_PACKAGE(FMMC_OBJECT_STATE &sObjState)
	IF iSniperBallObj = -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iExplodeTime != -1
			oiSniperBallPackage = sObjState.objIndex
			iSniperBallObj = sObjState.iIndex
		ENDIF	
	ENDIF
ENDPROC

FUNC BOOL HAS_OBJECT_BEEN_TAKEN_OUT_OF_ZONE(OBJECT_INDEX tempObj, INT iZone)
	
	//Obj attached
	//Obj Outside of zone
	IF DOES_ENTITY_EXIST(tempObj)
		//If object has been dropped
		IF NOT IS_ENTITY_ATTACHED(tempObj)
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0])
				IF NOT IS_ENTITY_IN_ANGLED_AREA(tempObj,g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0],g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1],GET_PLACED_ZONE_RADIUS(iZone))
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_SUDDEN_DEATH_OBJECT_DURING_SUDDEN_DEATH(INT iObj)
	
	IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_HideObjectDuringSuddenDeath)	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_OBJ_CAUSE_FAIL(INT iobj, INT iTeam, INT iPriority)
	
	IF iPriority < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamFailBitset,iPriority)
			RETURN TRUE
		ENDIF
	ENDIF
	IF IS_BIT_SET(MC_serverBD.iMissionCriticalObj[iTeam], iobj)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC INT GET_OBJ_KILLERS_TEAM(PED_INDEX killerPed,INT iObjID)
	
	PLAYER_INDEX tempPlayer
	PARTICIPANT_INDEX tempPart
	INT ipart
	
	PRINTLN("[RCC MISSION] GET_OBJ_KILLERS_TEAM - Called with iObjID ",iObjID)
	
	IF DOES_ENTITY_EXIST(killerPed)
		tempPlayer =NETWORK_GET_PLAYER_INDEX_FROM_PED(killerPed)
		IF NETWORK_IS_PLAYER_A_PARTICIPANT(tempPlayer)
			tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
			ipart =NATIVE_TO_INT(tempPart)
			IF MC_serverBD_4.iObjPriority[iObjID][MC_playerBD[ipart].iTeam] = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[ipart].iTeam]
				IF MC_serverBD_4.iObjPriority[iObjID][MC_playerBD[ipart].iTeam] < FMMC_MAX_RULES
					IF MC_serverBD_4.iObjRule[iObjID][MC_playerBD[ipart].iTeam] = FMMC_OBJECTIVE_LOGIC_KILL
						
						PRINTLN("[RCC MISSION] GET_OBJ_KILLERS_TEAM - killer part ",ipart," on team ",MC_playerBD[ipart].iTeam," has a kill rule, RETURN team ",MC_playerBD[ipart].iTeam)
						RETURN MC_playerBD[ipart].iTeam
						
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[RCC MISSION] GET_OBJ_KILLERS_TEAM - killer part ",ipart," on team ",MC_playerBD[ipart].iTeam," obj rule ",MC_serverBD_4.iObjRule[iObjID][MC_playerBD[ipart].iTeam]," is not a kill rule")
					#ENDIF
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] GET_OBJ_KILLERS_TEAM - killer part ",ipart," on team ",MC_playerBD[ipart].iTeam," obj priority > FMMC_MAX_RULES")
				#ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[RCC MISSION] GET_OBJ_KILLERS_TEAM - killer part ",ipart," on team ",MC_playerBD[ipart].iTeam," obj priority ",MC_serverBD_4.iObjPriority[iObjID][MC_playerBD[ipart].iTeam]," is not current")
			#ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RCC MISSION] GET_OBJ_KILLERS_TEAM - killerPed's player index is not a participant, player ",NATIVE_TO_INT(tempPlayer))
		#ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[RCC MISSION] GET_OBJ_KILLERS_TEAM - No killerPed")
	#ENDIF
	ENDIF
	
	RETURN -1

ENDFUNC

FUNC VECTOR GET_OBJ_FMMC_SPAWN_LOCATION(INT iObj, BOOL& bIgnoreAreaCheck)
	
	VECTOR vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vPos
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjectTrainAttachmentData.iTrainIndex != -1
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjectTrainAttachmentData.vCarriageOffsetPosition)		
		VECTOR vTemp = <<0.0, 0.0, 0.0>>
		MC_GET_TRAIN_ATTACHMENT_POINT_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjectTrainAttachmentData, vSpawnLoc, vTemp)
		PRINTLN("[Objects][Obj ", iObj, "] GET_OBJ_FMMC_SPAWN_LOCATION | Spawning at train-attached position: ", vSpawnLoc)
		RETURN vSpawnLoc
	ENDIF
	
	bIgnoreAreaCheck = FALSE
	
	IF IS_BIT_SET(MC_serverBD_2.iObjSpawnedOnce, iobj)
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vSecondSpawnPos)
		IF GET_RANDOM_INT_IN_RANGE(0, 100) < 50
			SET_BIT(iLocalBoolCheck10, LBOOL10_OBJ_SPAWN_AT_SECOND_POSITION)
			vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vSecondSpawnPos
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iSpawnNearType != ciRULE_TYPE_NONE
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iSpawnNearEntityID > -1
		PRINTLN("[JS] [SPAWNNEAR] - GET_OBJ_FMMC_SPAWN_LOCATION - 1")
		IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitset, cibsOBJ_FirstSpawnNotNearEntity))
		OR IS_BIT_SET(MC_serverBD_2.iObjSpawnedOnce, iobj)
			PRINTLN("[JS] [SPAWNNEAR] - GET_OBJ_FMMC_SPAWN_LOCATION - 2")
			VECTOR vNewSpawn = GET_FMMC_ENTITY_LOCATION(vSpawnLoc, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iSpawnNearType, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iSpawnNearEntityID)
			
			IF NOT IS_VECTOR_ZERO(vNewSpawn)
				vSpawnLoc = vNewSpawn
				PRINTLN("[JS] [SPAWNNEAR] - GET_OBJ_FMMC_SPAWN_LOCATION - 3 vSpawnLoc = ", vSpawnLoc)
				bIgnoreAreaCheck = TRUE
			ENDIF
		ENDIF
		
	ENDIF
		
	RETURN vSpawnLoc
	
ENDFUNC

FUNC BOOL IS_THIS_OBJECT_COLLECT_OBJECT_OBJECTIVE(INT iObj)
	
	IF NOT IS_CURRENT_OBJECTIVE_COLLECT_OBJECT()
		RETURN FALSE
	ENDIF
	
	INT iTeam = GET_LOCAL_PLAYER_TEAM(TRUE)
	INT iRule = GET_LOCAL_PLAYER_CURRENT_RULE(TRUE)
	
	IF iRule < FMMC_MAX_RULES
		RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjRequiredForRulePass[iRule] = iObj
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Object Rule Warps
// ##### Description: Moving the Object to a creator placed position on a rule change.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC WARP_OBJECTS_TO_WARP_LOCATION(FMMC_OBJECT_STATE &sObjState, VECTOR vObjWarpPos, FLOAT fObjWarpHead, INT &iObjWarpedBS[], INT iEventObjWarpedBS)		
	// Should be checked last so that we can return true in this function on the first frame the checks come back positive.
	IF IS_WARP_LOCATION_VISIBLE_TO_ANY_PLAYERS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].sWarpLocationSettings, GET_FMMC_OBJECT_COORDS(sObjState), vObjWarpPos)
		PRINTLN("[Warp_location][Objects][Object ", sObjState.iIndex, "] - PROCESS_PED_ON_RULE_WARP - IS_WARP_LOCATION_VISIBLE_TO_ANY_PLAYERS = TRUE")
		EXIT
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vObjWarpPos)		
		PRINTLN("[Warp_location][Objects][Object ", sObjState.iIndex, "] - PROCESS_OBJECT_ON_RULE_WARP - Warping this Object with COORD: ", vObjWarpPos, " and Heading: ", fObjWarpHead, " Setting iObjectWarpedOnThisRule.")		
		PLAY_WARP_ENTITY_TO_WARP_LOCATION_PTFX(GET_FMMC_OBJECT_COORDS(sObjState), g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].sWarpLocationSettings.sEntityWarpPTFX, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_PlayWarpPTFX_Start))
		PLAY_WARP_ENTITY_TO_WARP_LOCATION_PTFX(vObjWarpPos, g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].sWarpLocationSettings.sEntityWarpPTFX, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_PlayWarpPTFX_End))
		SET_ENTITY_COORDS(sObjState.objIndex, vObjWarpPos, FALSE, TRUE)
		SET_ENTITY_HEADING(sObjState.objIndex, fObjWarpHead)
		FMMC_SET_LONG_BIT(iObjWarpedBS, sObjState.iIndex)
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, iEventObjWarpedBS, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULt, DEFAULT, TRUE, sObjState.iIndex)
	ELSE
		ASSERTLN("[Warp_location][Objects][Object ", sObjState.iIndex, "] - WARP_OBJECTS_TO_WARP_LOCATION - NULL VECTOR POSITION GIVEN. Check the Creator Data. ")
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_IGNORE, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_OBJ, "Set up to warp on rule to a NULL/ZERO vector!", sObjState.iIndex)
		#ENDIF
		FMMC_SET_LONG_BIT(iObjWarpedBS, sObjState.iIndex)
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, iEventObjWarpedBS, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULt, DEFAULT, TRUE, sObjState.iIndex)
	ENDIF
ENDPROC

FUNC BOOL CAN_OBJECT_PROCESS_WARP_LOCATION(FMMC_OBJECT_STATE &sObjState)
	IF NOT sObjState.bObjAlive
		PRINTLN("[Warp_location][Objects][Object ", sObjState.iIndex, "] - CAN_OBJECT_PROCESS_WARP_LOCATION - Has been set to warp on this rule but the Object does not exist or is destroyed...")
		RETURN FALSE
	ENDIF
	
	IF NOT sObjState.bHaveControlOfObj
		PRINTLN("[Warp_location][Objects][Object ", sObjState.iIndex, "] - CAN_OBJECT_PROCESS_WARP_LOCATION - Has been set to warp on this rule but we don't have control over it.")
		RETURN FALSE
	ENDIF
	
	IF NOT READY_TO_PROCESS_ENTITY_WARP()
		PRINTLN("[Warp_location][Objects][Object ", sObjState.iIndex, "] - CAN_OBJECT_PROCESS_WARP_LOCATION - We should perform a warp, but we're not ready to process it yet.")
		RETURN FALSE
	ENDIF	
	RETURN TRUE
ENDFUNC

PROC PROCESS_OBJECT_ON_RULE_WARP(FMMC_OBJECT_STATE &sObjState)
	
	IF NOT SHOULD_ENTITY_PERFORM_A_RULE_WARP_LOCATION_WARP(CREATION_TYPE_OBJECTS, sObjState.iIndex)
		EXIT
	ENDIF
	
	IF NOT CAN_OBJECT_PROCESS_WARP_LOCATION(sObjState)
		EXIT
	ENDIF
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule =	MC_serverBD_4.iCurrentHighestPriority[iTeam] 
	
	FLOAT fObjWarpHead
	VECTOR vObjWarpPos
	
	GET_ENTITY_WARP_LOCATION_ON_THIS_RULE_START(iRule, iTeam, CREATION_TYPE_OBJECTS, sObjState.iIndex, vObjWarpPos, fObjWarpHead)
		
	WARP_OBJECTS_TO_WARP_LOCATION(sObjState, vObjWarpPos, fObjWarpHead, iObjectWarpedOnThisRule, g_ciInstancedcontentEventType_WarpedObjectOnRuleStart)
	
ENDPROC

PROC PROCESS_OBJECT_ON_DAMAGE_WARP(FMMC_OBJECT_STATE &sObjState)

	IF NOT SHOULD_ENTITY_PERFORM_AN_ON_DAMAGE_LOCATION_WARP(CREATION_TYPE_OBJECTS, sObjState.iIndex)
		EXIT
	ENDIF
	
	IF NOT CAN_OBJECT_PROCESS_WARP_LOCATION(sObjState)
		EXIT
	ENDIF
	
	FLOAT fObjWarpHead
	VECTOR vObjWarpPos
	
	GET_ENTITY_WARP_LOCATION_ON_DAMAGED(CREATION_TYPE_OBJECTS, sObjState.iIndex, vObjWarpPos, fObjWarpHead)
		
	WARP_OBJECTS_TO_WARP_LOCATION(sObjState, vObjWarpPos, fObjWarpHead, iObjectWarpedOnDamage, g_ciInstancedcontentEventType_WarpedObjectOnDamageStart)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Process Object Brain --------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Staggered loop used to process logic that only the Script Host can execute.  -------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC DELETE_FMMC_OBJECT(INT iobj)
	
	INT iTeam
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		CLEAR_BIT(MC_serverBD.iObjteamFailBitset[iobj], iTeam)
		CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], iobj)
	ENDFOR
	
	IF IS_THIS_OBJECT_A_HACK_CONTAINER(iObj)
		CPRINTLN(DEBUG_CONTROLLER, "DELETE_FMMC_OBJECT - Object ", iObj, " is a hack container - cleaning up its extra special bits too")
		CLEAN_UP_CONTAINER_DOORS()
	ENDIF
	
	IF SHOULD_DRONE_SYSTEMS_BE_ACTIVE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjDroneData)
		CLEAR_DRONE_INDEX(iObj, CREATION_TYPE_OBJECTS)
	ENDIF
	
	IF NOT SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iRespawnOnRule, -1)
		MC_serverBD_2.iCurrentObjRespawnLives[iobj] = 0
		PRINTLN("[RCC MISSION] DELETE_FMMC_OBJECT - Obj ",iobj," getting cleaned up early - set MC_serverBD_2.iCurrentObjRespawnLives[", iobj, "] = 0")
	ENDIF
	
	OBJECT_INDEX tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(iObj)])
	IF DOES_ENTITY_EXIST(tempObj)
		START_ENTITY_PTFX_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sEntityDespawnPTFX, GET_ENTITY_COORDS(tempObj, FALSE), GET_ENTITY_ROTATION(tempObj))
	ENDIF
	
	DELETE_NET_ID(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(iObj)])
	PRINTLN("[RCC MISSION] DELETE_FMMC_OBJECT - Obj getting cleaned up early - delete, objective: ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCleanupObjective," team: ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCleanupTeam)
	
	CLEAR_BIT(MC_serverBD.iObjCleanup_NeedOwnershipBS, iobj)
	
ENDPROC

PROC REMOVE_OBJECT_HOLDING_POINTS(INT iTeam,INT iobj)

	INT ipart
	IF IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeam],iobj)
		CLEAR_BIT(MC_serverBD.iObjAtYourHolding[iTeam],iobj)
		PRINTLN("[RCC MISSION]  SETTING PACKAGE NOT AT HOLDING: ",iobj," FOR TEAM: ", iTeam)
		BOOL bRemovedPlayerScore
		PRINTLN("[PLAYER_LOOP] - REMOVE_OBJECT_HOLDING_POINTS")
		FOR ipart = 0 TO (NUM_NETWORK_PLAYERS-1)
			IF IS_BIT_SET(MC_serverBD.iObjHoldPartPoints[iobj],ipart)
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_SHARD_ON_OBJECT_STOLEN)
					IF MC_serverBD.iObjCarrier[iObj] != -1
						PRINTLN("[RCC MISSION] BROADCAST_FMMC_STOLEN_FLAG_SHARD - Object ", iObj, " Stolen. Player Score Shard")
					ELSE
						PRINTLN("[RCC MISSION] BROADCAST_FMMC_STOLEN_FLAG_SHARD - MC_serverBD.iObjCarrier[",iObj,"] = -1")
					ENDIF
				ENDIF
				
				REQUEST_INCREMENT_SERVER_REMOTE_PLAYER_SCORE(ipart,(-1 * GET_FMMC_POINTS_FOR_TEAM(iTeam,MC_serverBD_4.iObjPriority[iobj][iTeam])),iTeam,MC_serverBD_4.iObjPriority[iobj][iTeam])
									
				bRemovedPlayerScore = TRUE
				CLEAR_BIT(MC_serverBD.iObjHoldPartPoints[iobj],ipart)
			ENDIF
		ENDFOR
		IF NOT bRemovedPlayerScore
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_SHARD_ON_OBJECT_STOLEN)
				IF MC_serverBD.iObjCarrier[iObj] != -1
					PRINTLN("[RCC MISSION] BROADCAST_FMMC_STOLEN_FLAG_SHARD - Object ", iObj, " Stolen. Team Score Shard")
					
					//[FMMC2020] Make flag stealing more generic rather than being anything to do with Pointless/Stockpile
				//	BROADCAST_FMMC_STOLEN_FLAG_SHARD(INT_TO_PLAYERINDEX(MC_serverBD.iObjCarrier[iObj]), MC_PlayerBD[MC_serverBD.iObjCarrier[iObj]].iTeam, iTeam, iObj, GET_STOCKPILE_LAST_PICKUP_CAPTOR(iObj))
				ELSE
					PRINTLN("[RCC MISSION] BROADCAST_FMMC_STOLEN_FLAG_SHARD - MC_serverBD.iObjCarrier[",iObj,"] = -1")
				ENDIF
			ENDIF
			//Remove points, pass in a negative amount:
			INCREMENT_SERVER_TEAM_SCORE(iTeam, MC_serverBD_4.iObjPriority[iobj][iTeam], -GET_FMMC_POINTS_FOR_TEAM(iTeam,MC_serverBD_4.iObjPriority[iobj][iTeam]))
		ENDIF
	ENDIF

ENDPROC

PROC CLEANUP_OBJECT_RESPAWNING_DATA(FMMC_OBJECT_STATE &sObjState)
	
	CLEANUP_GENERAL_RESPAWNING_DATA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].mn)
	
	MC_serverBD.isearchingforObj = -1
	
	iSpawnPoolObjIndex = -1
	
	iSpawnRandomHeadingToSelect = -1
		
ENDPROC

FUNC BOOL RESPAWN_MISSION_OBJ(FMMC_OBJECT_STATE &sObjState)
	
	INT iObj = sObjState.iIndex
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn = DUMMY_MODEL_FOR_SCRIPT
		RETURN FALSE
	ENDIF
	
	IF sObjState.bObjExists
		RETURN TRUE
	ENDIF
	
	REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn)
	IF NOT HAS_MODEL_LOADED(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn)
		RETURN FALSE
	ENDIF
		
	IF NOT CAN_REGISTER_MISSION_OBJECTS(1)
	OR ARE_WE_WAITING_FOR_OBJECTS_TO_CLEAN_UP()
		#IF IS_DEBUG_BUILD
		IF NOT CAN_REGISTER_MISSION_OBJECTS(1)
			PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - failing on CAN_REGISTER_MISSION_OBJECTS")
		ENDIF
		IF ARE_WE_WAITING_FOR_OBJECTS_TO_CLEAN_UP()
			IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
				PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - failing to spawn because of ARE_WE_WAITING_FOR_OBJECTS_TO_CLEAN_UP: new priority / midpoint this frame")
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HOST_APPLIED_NEW_PRIORITY_MIDFRAME)
			OR IS_BIT_SET(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
				PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - failing to spawn because of ARE_WE_WAITING_FOR_OBJECTS_TO_CLEAN_UP: new priority / midpoint set by host")
			ENDIF
			IF MC_serverBD.iObjCleanup_NeedOwnershipBS != 0
				PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - failing to spawn because of ARE_WE_WAITING_FOR_OBJECTS_TO_CLEAN_UP: iObjCleanup_NeedOwnershipBS = ",MC_serverBD.iObjCleanup_NeedOwnershipBS)
			ENDIF
		ENDIF
		#ENDIF	
		RETURN FALSE
	ENDIF
	
	IF NOT CAN_RUN_ENTITY_SPAWN_POS_SEARCH(ciEntitySpawnSearchType_Objs, iObj)
		#IF IS_DEBUG_BUILD
		PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - failing to spawn because attempting to spawn one of the following: ped ", MC_serverBD.isearchingforPed," / veh ", MC_serverBD.isearchingforVeh ," / obj ", MC_serverBD.isearchingforObj)
		#ENDIF
		RETURN FALSE
	ENDIF
		
	BOOL bPedSpawn, bOnGround, bVisible
	FLOAT fareaSize, fspawnhead
	BOOL bIgnoreAreaCheck
	BOOL bForceSpawn
	BOOL bUsingTrainSpawn
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed >= 0
		IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iObjSpawnPedBitset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed)
			PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - calling create respawn obj on ped but ped not created so waiting for obj: ", iobj, " on ped: ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed)
			vEntityRespawnLoc = <<0, 0, 0>>
			RETURN FALSE
		ENDIF
		IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed])
			
			PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - needs to be placed on ped ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed, ", and they exist so this is fine")
			bPedSpawn = TRUE
			
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - needs to be placed on ped ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed, ", but they are injured or don't exist...")
		#ENDIF
		ENDIF
		
		IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed])
			PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - waiting to have control of attachment ped")
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed])
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAttachParent >= 0
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAttachParentType = CREATION_TYPE_VEHICLES
		IF NOT IS_BIT_SET(MC_serverBD.iVehSpawnBitset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAttachParent)
			PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - calling create respawn obj in vehicle but vehicle not created so waiting for obj: ",iobj," on vehicle: ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent)
			vEntityRespawnLoc = <<0, 0, 0>>
			RETURN FALSE
		ENDIF
		
		IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAttachParent])
			PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - waiting to have control of attachment vehicle")
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAttachParent])
			RETURN FALSE
		ENDIF
		
		bIgnoreAreaCheck = TRUE
	ENDIF
	
	IF MC_serverBD.isearchingforObj != iObj	
		MC_serverBD.isearchingforObj = iObj
		PRINTLN("[MCSpawning][Objects][Object ", iObj, "] Assigning isearchingforVeh: ", MC_serverBD.isearchingforObj)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = PROP_CONTR_03B_LD
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = Prop_Container_LD_PU 
		fareaSize = 8.0
	ELSE
		fareaSize = 0.5
	ENDIF
	
	IF bPedSpawn
		bOnGround = FALSE
	ELSE
		bOnGround = TRUE
	ENDIF
	
	IF SHOULD_OBJECT_BE_INVISIBLE(iObj)
		bVisible = FALSE
	ELSE
		bVisible = TRUE
	ENDIF
	
	IF iEntityRespawnType != ci_TARGET_OBJECT
	OR iEntityRespawnID != iobj
		vEntityRespawnLoc = <<0, 0, 0>>
		iEntityRespawnType = ci_TARGET_OBJECT
		iEntityRespawnID = iobj
	ENDIF
	
	IF IS_VECTOR_ZERO(vEntityRespawnLoc)
		PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - [JS] [SPAWNNEAR] - (1)")
		CLEAR_BIT(iLocalBoolCheck10, LBOOL10_OBJ_SPAWN_AT_SECOND_POSITION)
		vEntityRespawnLoc = GET_OBJ_FMMC_SPAWN_LOCATION(iobj, bIgnoreAreaCheck)
		IF bIgnoreAreaCheck
			SET_BIT(iLocalBoolCheck4, LBOOL4_RESPAWN_SHOULD_FORCE_AREA_CHECK)
		ELSE
			CLEAR_BIT(iLocalBoolCheck4, LBOOL4_RESPAWN_SHOULD_FORCE_AREA_CHECK)
		ENDIF
	ELSE
		PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - [JS] [SPAWNNEAR] - (2)")
		bIgnoreAreaCheck = IS_BIT_SET(iLocalBoolCheck4, LBOOL4_RESPAWN_SHOULD_FORCE_AREA_CHECK)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjectTrainAttachmentData.iTrainIndex != -1
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjectTrainAttachmentData.vCarriageOffsetPosition)		
		bUsingTrainSpawn = TRUE
		bForceSpawn = TRUE
		PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - [JS] [SPAWNNEAR] - bForceSpawn = TRUE")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSet, cibsOBJ_Position_Override)
	AND NOT bUsingTrainSpawn
		bIgnoreAreaCheck = TRUE
		vEntityRespawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vPos
	ENDIF
		
	IF NOT bIgnoreAreaCheck
	AND NOT NETWORK_ENTITY_AREA_DOES_EXIST(MC_serverBD.iSpawnArea)
		MC_serverBD.iSpawnArea = NETWORK_ADD_ENTITY_AREA((vEntityRespawnLoc - <<fareaSize,fareaSize,fareaSize>>),(vEntityRespawnLoc + <<fareaSize,fareaSize,fareaSize>>))
		REINIT_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
		PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - adding spawn area for object")
		RETURN FALSE
	ENDIF
	
	IF NOT (bIgnoreAreaCheck
	OR NETWORK_ENTITY_AREA_HAVE_ALL_REPLIED(MC_serverBD.iSpawnArea))
		IF (NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdSpawnAreaTimeout))
		OR GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdSpawnAreaTimeout) >= ciFMMCSpawnAreaTimeout
			
			PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - waited for everyone to reply back on entity area for over 10s, delete and try again")
			NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
			MC_serverBD.iSpawnArea = -1
			RESET_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
			
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - waiting for everyone to reply back on entity area, waited for ",GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdSpawnAreaTimeout),"ms")
		#ENDIF
		ENDIF
		RETURN FALSE
	ENDIF
		
	IF NOT bIgnoreAreaCheck
	AND (NETWORK_ENTITY_AREA_IS_OCCUPIED(MC_serverBD.iSpawnArea)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitset, cibsOBJ_IgnoreVisCheck))
	AND NOT bForceSpawn
		SPAWN_SEARCH_PARAMS SpawnSearchParams
		SpawnSearchParams.bConsiderInteriors = TRUE
		SpawnSearchParams.fMinDistFromPlayer = 2
		
		FLOAT fSearchRadius = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iobj].fRespawnRange
		
		IF bIgnoreAreaCheck
			SpawnSearchParams.vFacingCoords = vEntityRespawnLoc
			//Want to spawn outside of a range from vEntityRespawnLoc:
			SpawnSearchParams.vAvoidCoords[0] = vEntityRespawnLoc
			SpawnSearchParams.fAvoidRadius[0] = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].fRespawnMinRange
			
			SpawnSearchParams.bConsiderOriginAsValidPoint = FALSE
			SpawnSearchParams.bCloseToOriginAsPossible = FALSE																				
			IF (fSearchRadius < SpawnSearchParams.fAvoidRadius[0])
				fSearchRadius += 10.0
				PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - fSearchRadius was smaller than avoid radius, increasing.")
			ENDIF
		ENDIF
		
		IF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vEntityRespawnLoc, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].fRespawnRange, vEntityRespawnLoc, fspawnhead, SpawnSearchParams)		
			PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - New position found: ", vEntityRespawnLoc)
		ELSE
			PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - waiting for safe coords.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF SHOULD_PLACED_OBJECT_BE_PORTABLE(iObj)
	
		PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - calling create portable pickup object at coords: ", vEntityRespawnLoc)
		// Creates the pickup
		MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(iObj)] = MC_CREATE_PORTABLE_PICKUP_BASED_ON_SETTINGS(iObj, bOnGround, vEntityRespawnLoc)
		
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iObj))
			PRINTLN("[[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - Failed to spawn portable pickup object this frame!")
			RETURN FALSE
		ENDIF
		
		SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(GET_OBJECT_NET_ID(iObj), TRUE)
		MC_SET_UP_OBJ(NET_TO_OBJ(GET_OBJECT_NET_ID(iObj)), iobj, bOnGround, bVisible, IS_BIT_SET(iLocalBoolCheck10, LBOOL10_OBJ_SPAWN_AT_SECOND_POSITION))
		
		IF bPedSpawn
			ATTACH_PORTABLE_PICKUP_TO_PED(NET_TO_OBJ(GET_OBJECT_NET_ID(iObj)),NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed]))
			FMMC_SET_LONG_BIT(MC_serverBD_4.iPedHasAttachedObject, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectAttachmentPed)
			PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - Attaching to ped: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectAttachmentPed)
			SET_BIT(iAICarryBitset,iobj)
		ENDIF
		
		MC_PROCESS_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sWarpLocationSettings, CREATION_TYPE_OBJECTS, iObj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iSpawnGroup, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iSpawnSubGroupBS, NET_TO_OBJ(GET_OBJECT_NET_ID(iObj)))	
		
		INT iTeamLoop
		FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			IF MC_serverBD_4.iObjPriority[iobj][iTeamLoop] > MC_serverBD_4.iCurrentHighestPriority[iTeamLoop]
			OR NOT DOES_OBJECT_RULE_TYPE_REQUIRE_PICKUP(MC_serverBD_4.iObjRule[iobj][iTeamLoop])
				PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - is a respawn object that doesn't require pickup for team ",iTeamLoop)
				SET_TEAM_PICKUP_OBJECT(NET_TO_OBJ(GET_OBJECT_NET_ID(iObj)), iTeamLoop, FALSE)
			ENDIF
		ENDFOR
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent != -1
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent])
				IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent]))
					ATTACH_OBJECT_TO_FLATBED(NET_TO_OBJ(GET_OBJECT_NET_ID(iObj)),NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent]), g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vAttachOffset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vAttachOffsetRotation, TRUE)
					PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - successfully attached to vehicle ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent)
				ELSE
					PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - Unable to attach to vehicle ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent,", as the vehicle is not driveable!")
				ENDIF
			ELSE
				PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - Unable to attach to vehicle ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent,", as the vehicle does not exist!")
			ENDIF
		ENDIF
				
		CLEANUP_OBJECT_RESPAWNING_DATA(sObjState)
		
		START_ENTITY_PTFX_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sEntitySpawnPTFX, vEntityRespawnLoc, GET_ENTITY_ROTATION(NET_TO_ENT(GET_OBJECT_NET_ID(iObj))))
				
		PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ -     ---------->  RESPAWN PICKUP OBJECT CREATED    <----------     ")
		
		RETURN TRUE
	ELSE
		PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - calling create object at coords: ", vEntityRespawnLoc)
		
		IF FMMC_CREATE_NET_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(iObj)], g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn, vEntityRespawnLoc)
			
			MC_SET_UP_OBJ(NET_TO_OBJ(GET_OBJECT_NET_ID(iObj)), iObj, bOnGround, bVisible, IS_BIT_SET(iLocalBoolCheck10, LBOOL10_OBJ_SPAWN_AT_SECOND_POSITION), FALSE, FALSE)
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_MinigameObjAttachToPed)
				IF bPedSpawn
					IF DOES_ENTITY_EXIST(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed]))
					AND DOES_ENTITY_EXIST(NET_TO_OBJ(GET_OBJECT_NET_ID(iObj)))
						ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(GET_OBJECT_NET_ID(iObj)),NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed]), -1, <<0,0,0>>, <<0,0,0>>)
						SET_BIT(iAICarryBitset,iobj)
					ENDIF
				ENDIF
			ENDIF
														
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = Prop_Contr_03b_LD
			OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = prop_container_ld_pu
				SET_ENTITY_LOD_DIST(NET_TO_OBJ(GET_OBJECT_NET_ID(iObj)),500)
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent != -1
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent])
					IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent]))
						ATTACH_OBJECT_TO_FLATBED(NET_TO_OBJ(GET_OBJECT_NET_ID(iObj)),NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent]), g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vAttachOffset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vAttachOffsetRotation, TRUE)
						PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - successfully attached to vehicle ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent)
					ELSE
						PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - Unable to attach to vehicle ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent,", as the vehicle is not driveable!")
					ENDIF
				ELSE
					PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - Unable to attach to vehicle ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent,", as the vehicle does not exist!")
				ENDIF
			ENDIF
			
			MC_PROCESS_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sWarpLocationSettings, CREATION_TYPE_OBJECTS, iObj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iSpawnGroup, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iSpawnSubGroupBS, NET_TO_OBJ(GET_OBJECT_NET_ID(iObj)))	
			
			SET_UP_TROLLEY_STARTING_AMOUNT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn, NET_TO_OBJ(GET_OBJECT_NET_ID(iObj)))											
						
			CLEANUP_OBJECT_RESPAWNING_DATA(sObjState)				
			
			START_ENTITY_PTFX_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sEntitySpawnPTFX, vEntityRespawnLoc, GET_ENTITY_ROTATION(NET_TO_ENT(GET_OBJECT_NET_ID(iObj))))
			
			PRINTLN("[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ -       ---------->  RESPAWN   FMMC MINI GAME OBJECT CREATED    <---------- ")
			
			RETURN TRUE
		ELSE
			PRINTLN("[[MCSpawning][Objects][Object ", iObj, "] RESPAWN_MISSION_OBJ - FMMC_CREATE_NET_OBJ returning false.")
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

PROC PROCESS_OBJ_RESPAWNING(FMMC_OBJECT_STATE &sObjState)

	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		EXIT
	ENDIF
		
	INT iObj = sObjState.iIndex
	IF MC_serverBD_2.iCurrentObjRespawnLives[iObj] > 0
		IF SHOULD_OBJ_RESPAWN_NOW(iObj, sObjState)
		AND HAS_OBJ_RESPAWN_DELAY_EXPIRED(iObj)
			IF RESPAWN_MISSION_OBJ(sObjState)
				
				//Clear then refill Struct
				FMMC_OBJECT_STATE sEmpty
				sObjState = sEmpty
				FILL_FMMC_OBJECT_STATE_STRUCT(sObjState, iObj)
				
				CLEAR_BIT(iObjShouldRespawnNowBS, iObj)
				
				SET_BIT(MC_serverBD_2.iObjSpawnedOnce, iObj)
				
				MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_3.iObjSpawnDelay[iObj])
				
				IF MC_DOES_OBJ_HAVE_PLAYER_VARIABLE_SETTING(iObj)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAssociatedObjective > -1
					AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAssociatedTeam > -1
						IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAssociatedTeam] < FMMC_MAX_RULES
							MC_serverBD_1.sCreatedCount.iNumObjCreated[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAssociatedObjective]++
						ENDIF
					ENDIF
				ENDIF
				
				IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iRespawnOnRule, -1)
					INT iTeam
					FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
							SET_BIT(MC_serverBD.iObjteamFailBitset[iObj],iTeam)
							BREAKLOOP
						ENDIF
					ENDFOR
				ENDIF
				
				IF MC_serverBD_2.iCurrentObjRespawnLives[iObj] != UNLIMITED_RESPAWNS
					
				ENDIF
				
				MC_serverBD_2.iCurrentObjRespawnLives[iObj]--
				IF GET_ENTITY_STARTING_RESPAWN_LIVES(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectRespawnLives) = UNLIMITED_RESPAWNS
				AND MC_serverBD_2.iCurrentObjRespawnLives[iObj] < 1
					MC_serverBD_2.iCurrentObjRespawnLives[iObj] = UNLIMITED_RESPAWNS
				ENDIF
				
				INT iTeam
				FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
					IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
						IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectsDestroyedRuleHudAmount[MC_ServerBD_4.iCurrentHighestPriority[iTeam]] > 0
							IF IS_BIT_SET(MC_ServerBD_4.iObjectsDestroyedForSpecialHUDBitset[iTeam], iObj)
								PRINTLN("[MCSpawning][Objects][Object ", iObj, "] PROCESS_OBJ_RESPAWNING - [iEntityincrement] A new object has been spawned that has been destroyed previously. obj: ", iObj, " Clearing iObjectsDestroyedForSpecialHUDBitset so that we can increment destroyed later")
								CLEAR_BIT(MC_ServerBD_4.iObjectsDestroyedForSpecialHUDBitset[iTeam], iObj)
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ELSE
			IF MC_serverBD.iSearchingForObj != -1	
				PRINTLN("[MCSpawning][Objects][Object ", iObj, "] Clearing isearchingforVeh: ", MC_serverBD.iSearchingForObj)
				MC_serverBD.iSearchingForObj = -1	
			ENDIF
			
			IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME()
			AND (NOT CAN_OBJ_STILL_SPAWN_IN_MISSION(iObj))
				
				MC_serverBD.iObjteamFailBitset[iObj] = 0
				
				INT iTeam
				
				FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
					MC_serverBD_4.iObjPriority[iObj][iTeam] = FMMC_PRIORITY_IGNORE
					MC_serverBD_4.iObjRule[iObj][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
					CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], iObj)
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
	
	IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iRespawnOnRule, -1)
		INT iTeam
		FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			IF MC_serverBD_4.iCurrentHighestPriority[iTeam] > 0
				IF (MC_serverBD_4.iCurrentHighestPriority[iTeam] > 0
				AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam))
				OR IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_RETRY_CURRENT_RULE_TEAM_0 + iTeam)
					IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iRespawnOnRule, iTeam)
						SET_BIT(MC_serverBD.iObjCleanup_NeedOwnershipBS, iObj)
						IF MC_serverBD_2.iCurrentObjRespawnLives[iObj] < 1
							MC_serverBD_2.iCurrentObjRespawnLives[iObj] = 1
						ENDIF
						PRINTLN("[MCSpawning][Objects][Object ", iObj, "] PROCESS_OBJ_RESPAWNING - Cleaning up obj to respawn on rule change ", iObj)
						BREAKLOOP
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC GIVE_TEAM_POINTS_FOR_OBJ_DAMAGE(INT& iObjID, OBJECT_INDEX VictimObj, PED_INDEX killerPed = NULL)

	INT iTeam
	INT iKillerTeam = -1
	
	IF bIsLocalPlayerHost
		IF iobjID = -2
			iobjid = IS_OBJ_A_MISSION_CREATOR_OBJ(VictimObj)
		ENDIF
		
		IF iobjid >= 0
			iKillerTeam = GET_OBJ_KILLERS_TEAM(killerPed, iobjid)
			PRINTLN("[RCC MISSION] GIVE_TEAM_POINTS_FOR_OBJ_DAMAGE - Object ",iObjID," has iKillerTeam ",iKillerTeam)
			FOR iTeam = 0 TO (MC_serverBD.iNumActiveTeams-1)
				IF iKillerTeam != iTeam
					PRINTLN("[RCC MISSION] GIVE_TEAM_POINTS_FOR_OBJ_DAMAGE - Object ",iObjID," has priority ",MC_serverBD_4.iObjPriority[iobjid][iTeam]," & rule ",MC_serverBD_4.iObjRule[iobjid][iTeam]," for team ",iTeam)
					IF MC_serverBD_4.iObjPriority[iobjid][iTeam] < FMMC_MAX_RULES
						IF MC_serverBD_4.iObjPriority[iobjid][iTeam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]
							IF MC_serverBD_4.iObjRule[iobjid][iTeam] = FMMC_OBJECTIVE_LOGIC_DAMAGE
								INCREMENT_SERVER_TEAM_SCORE(iTeam,MC_serverBD_4.iObjPriority[iobjid][iTeam], GET_FMMC_POINTS_FOR_TEAM(iTeam, MC_serverBD_4.iObjPriority[iobjid][iTeam]))
								PRINTLN("[RCC MISSION] GIVE_TEAM_POINTS_FOR_OBJ_DAMAGE - ADDING TO TEAM SCORE for team: ", iTeam," OBJ was Damaged : ",iobjid)
								BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iObjPriority[iobjid][iTeam],iTeam)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RCC MISSION] GIVE_TEAM_POINTS_FOR_OBJ_DAMAGE - Object is not a mission creator object")
		#ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC GIVE_TEAM_POINTS_FOR_OBJ_KILL(INT& iObjID, OBJECT_INDEX VictimObj,PED_INDEX killerPed = NULL)

	INT iTeam
	INT iKillerTeam = -1
	
	IF bIsLocalPlayerHost
		IF iobjID = -2
			iobjid = IS_OBJ_A_MISSION_CREATOR_OBJ(VictimObj)
		ENDIF
		
		IF iobjid >= 0
			iKillerTeam = GET_OBJ_KILLERS_TEAM(killerPed,iobjid)
			PRINTLN("[RCC MISSION] GIVE_TEAM_POINTS_FOR_OBJ_KILL - Object ",iObjID," has iKillerTeam ",iKillerTeam)
			FOR iTeam = 0 TO (MC_serverBD.iNumActiveTeams-1)
				IF iKillerTeam != iTeam
					PRINTLN("[RCC MISSION] GIVE_TEAM_POINTS_FOR_OBJ_KILL - Object ",iObjID," has priority ",MC_serverBD_4.iObjPriority[iobjid][iTeam]," & rule ",MC_serverBD_4.iObjRule[iobjid][iTeam]," for team ",iTeam)
					IF MC_serverBD_4.iObjPriority[iobjid][iTeam] < FMMC_MAX_RULES
						IF MC_serverBD_4.iObjPriority[iobjid][iTeam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]
							IF MC_serverBD_4.iObjRule[iobjid][iTeam] = FMMC_OBJECTIVE_LOGIC_KILL
								INCREMENT_SERVER_TEAM_SCORE(iTeam,MC_serverBD_4.iObjPriority[iobjid][iTeam],GET_FMMC_POINTS_FOR_TEAM(iTeam,MC_serverBD_4.iObjPriority[iobjid][iTeam]))
								PRINTLN("[RCC MISSION] GIVE_TEAM_POINTS_FOR_OBJ_KILL - ADDING TO TEAM SCORE for team: ", iTeam," OBJ was killed : ",iobjid)
								BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iObjPriority[iobjid][iTeam],iTeam)
							ENDIF
						ENDIF
					ENDIF
					
					//Custom points
					INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
					INT iCustomPoints = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjID].iObjPointsToGive
					INT iRuleStart = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjID].iObjGivePointsOnRuleStart
					INt iRuleEnd = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjID].iObjGivePointsOnRuleEnd
					
					IF iCustomPoints > 0
						PRINTLN("GIVE_TEAM_POINTS_FOR_OBJ_KILL - iObj: ", iObjID, " iTeam: ", iTeam, " iRule: ", iRule, " iCustomPoints: ", iCustomPoints, " iRuleStart: ", iRuleStart, " iRuleEnd: ", iRuleEnd)
						
						IF iRule >= iRuleStart
						AND (iRule < iRuleEnd OR iRuleEnd = -1)
							INCREMENT_SERVER_TEAM_SCORE(iTeam, iRule, iCustomPoints)
							PRINTLN("ADDING TO TEAM SCORE for team: ", iTeam," obj was killed : ", iObjID)
						ENDIF
					ENDIF
					//////
				ENDIF
			ENDFOR
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RCC MISSION] GIVE_TEAM_POINTS_FOR_OBJ_KILL - Object is not a mission creator object")
		#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC RUN_TEAM_OBJ_FAIL_CHECKS(INT iobj,INT iTeam,INT ipriority)

	INT iTeamrepeat
	BOOL bFailMission

	IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[iobj],iTeam)
		
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OBJ_DEAD
		
		FOR iTeamrepeat = 0 TO (MC_serverBD.iNumberOfTeams-1)
			IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[iobj],SBBOOL1_TEAM0_NEEDS_KILL + iTeamrepeat)
				IF DOES_TEAM_LIKE_TEAM(iTeam,iTeamrepeat)
					CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] RUN_TEAM_OBJ_FAIL_CHECKS - Team ",iTeamrepeat," needs a kill, clearing objteamfailbitset for team ", iTeam)
					CLEAR_BIT(MC_serverBD.iObjteamFailBitset[iobj],iTeam)
					CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], iobj)
					iTeamrepeat = MC_serverBD.iNumberOfTeams // Break out!
				ENDIF
			ENDIF
		ENDFOR
		
		IF iTeam = 0
			IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[iobj],SBBOOL1_TEAM0_NEEDS_KILL)
				SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority,TRUE)
				PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,iobj,iTeam,TRUE)
			ELSE
				IF SHOULD_OBJ_CAUSE_FAIL(iobj,iTeam,iPriority)
					bFailMission = TRUE
					REQUEST_SET_TEAM_FAILED(iTeam,mFail_OBJ_DEAD)
					MC_serverBD.iEntityCausingFail[iTeam] = iobj
					NET_PRINT(" MISSION FAILED FOR TEAM 0 BECAUSE OBJ DEAD: ") NET_PRINT_INT(iobj) NET_NL()
				ENDIF
				SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority,FALSE)
				PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,iobj,iTeam,FALSE)
			ENDIF
		ELIF iTeam = 1
			IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[iobj],SBBOOL1_TEAM1_NEEDS_KILL)
				SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority,TRUE)
				PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,iobj,iTeam,TRUE)
			ELSE	
				IF SHOULD_OBJ_CAUSE_FAIL(iobj,iTeam,iPriority)
					bFailMission = TRUE
					REQUEST_SET_TEAM_FAILED(iTeam,mFail_OBJ_DEAD)
					MC_serverBD.iEntityCausingFail[iTeam] = iobj
					NET_PRINT(" MISSION FAILED FOR TEAM 1 BECAUSE OBJ DEAD: ") NET_PRINT_INT(iobj) NET_NL()
				ENDIF	
				
				SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority,FALSE)
				PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,iobj,iTeam,FALSE)
			ENDIF
		ELIF iTeam = 2
			IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[iobj],SBBOOL1_TEAM2_NEEDS_KILL)
				SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority,TRUE)
				PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,iobj,iTeam,TRUE)
			ELSE
				IF SHOULD_OBJ_CAUSE_FAIL(iobj,iTeam,iPriority)
					bFailMission = TRUE
					REQUEST_SET_TEAM_FAILED(iTeam,mFail_OBJ_DEAD)
					MC_serverBD.iEntityCausingFail[iTeam] = iobj
					NET_PRINT(" MISSION FAILED FOR TEAM 2 BECAUSE OBJ DEAD: ") NET_PRINT_INT(iobj) NET_NL()
				ENDIF
				
				SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority,FALSE)
				PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,iobj,iTeam,FALSE)
			ENDIF
		ELIF iTeam = 3
			IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[iobj],SBBOOL1_TEAM3_NEEDS_KILL)
				SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority,TRUE)
				PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,iobj,iTeam,TRUE)
			ELSE	
				IF SHOULD_OBJ_CAUSE_FAIL(iobj,iTeam,iPriority)
					bFailMission = TRUE
					REQUEST_SET_TEAM_FAILED(iTeam,mFail_OBJ_DEAD)
					MC_serverBD.iEntityCausingFail[iTeam] = iobj
					NET_PRINT(" MISSION FAILED FOR TEAM 3 BECAUSE OBJ DEAD: ") NET_PRINT_INT(iobj) NET_NL()
				ENDIF
				
				SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority,FALSE)
				PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,iobj,iTeam,FALSE)
			ENDIF
		ENDIF
		SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,ipriority,MC_serverBD.iReasonForObjEnd[iTeam],bFailMission)
		BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam],iTeam,ipriority,DID_TEAM_PASS_OBJECTIVE(iTeam,ipriority),bFailMission)
		CLEAR_BIT(MC_serverBD.iObjteamFailBitset[iobj],iTeam)
	ENDIF
ENDPROC

PROC CLEANUP_CRATE_PICKUPS(FMMC_OBJECT_STATE &sObjState)
	INT iPickup
	INT iObj = sObjState.iIndex
	FOR iPickup = 0 TO (ciMAX_CRATE_PICKUPS-1)
		IF NOT IS_VECTOR_ZERO(MC_serverBD_3.vCratePickupCoords[iObj][iPickup])
		AND SHOULD_CLEANUP_OBJ(sObjState)
			IF DOES_ENTITY_EXIST(piCratePickup[iObj][iPickup].oiPickup)
				DELETE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(piCratePickup[iObj][iPickup])
				PRINTLN("[RCC MISSION] CLEANUP_CRATE_PICKUPS - Cleaning Up Pickup ", iPickup," of Object ", iObj, " early.")
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC SERVER_MANAGE_BREAKABLE_OBJECTS(FMMC_OBJECT_STATE &sObjState)

	INT iObj = sObjState.iIndex
	
	IF NOT IS_BIT_SET(MC_serverBD.iFragableCrate, iObj)
	AND NOT IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iFragableCrate, iObj)
	AND NOT (sObjState.bObjBroken OR !sObjState.bObjAlive)
		//Do nothing
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iFragableCrate, iObj)
	AND NOT IS_BIT_SET(MC_serverBD.iCrateBrokenBitset, iObj)
		//Do nothing - what used to be here is now done in PROCESS_DESTRUCTIBLE_CRATES_SERVER (as that's run every frame to make the pickups appear quicker)
		EXIT
	ENDIF
	
	BOOL bNotIgnore
	INT iParticipant = -1, iTeam
	INT iKillerTeam = -1
	INt iDamage
	PLAYER_INDEX killerPlayer
	
	CLEANUP_CRATE_PICKUPS(sObjState)
		
	IF NOT IS_BIT_SET(MC_serverBD.iCrateDestructionProcessed, iObj)
	AND (NOT sObjState.bObjExists OR IS_ENTITY_VISIBLE(sObjState.objIndex))
		
		#IF IS_DEBUG_BUILD
		IF sObjState.bObjExists
			IF sObjState.bObjBroken
				PRINTLN("[Objects][Object ", sObjState.iIndex, "] SERVER_MANAGE_BREAKABLE_OBJECTS - Object is broken!")
			ENDIF
			IF !sObjState.bObjAlive
				PRINTLN("[Objects][Object ", sObjState.iIndex, "] SERVER_MANAGE_BREAKABLE_OBJECTS - Object is dead!")
			ENDIF
		ENDIF
		#ENDIF
		
		IF sObjState.bObjExists
		AND IS_DAMAGE_TRACKER_ACTIVE_ON_NETWORK_ID(sObjState.niIndex)
			   
			WHILE DO_PARTICIPANT_LOOP(iParticipant, DPLF_FILL_PLAYER_IDS | DPLF_CHECK_PLAYER_OK)
				IF NOT NETWORK_GET_ASSISTED_DAMAGE_OF_ENTITY(piParticipantLoop_PlayerIndex, sObjState.objIndex, iDamage)
					RELOOP
				ENDIF
				
				IF iDamage <= 0
					RELOOP
				ENDIF
				
				PRINTLN("[Objects][Object ", sObjState.iIndex, "] SERVER_MANAGE_BREAKABLE_OBJECTS - Part ", iParticipant, " (on iKillerTeam ", iKillerTeam, ") did damage to the object")
				KillerPlayer = piParticipantLoop_PlayerIndex
				iKillerTeam = MC_playerBD[iParticipant].iTeam
				
			ENDWHILE
								
		ENDIF
		
		IF iKillerTeam > -1
			IF MC_serverBD_4.iObjPriority[iObj][iKillerTeam] <= MC_serverBD_4.iCurrentHighestPriority[iKillerTeam]				
				BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_OBJ, 0, iKillerTeam, -1, KillerPlayer, ci_TARGET_OBJECT, iObj)
				IF MC_serverBD_4.iObjPriority[iObj][iKillerTeam] < FMMC_MAX_RULES
					IF MC_serverBD_4.iObjRule[iObj][iKillerTeam] = FMMC_OBJECTIVE_LOGIC_KILL
						BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iObjPriority[iObj][iKillerTeam], iKillerTeam)
						
						REQUEST_INCREMENT_SERVER_REMOTE_PLAYER_SCORE(iParticipant, GET_FMMC_POINTS_FOR_TEAM(iKillerTeam, MC_serverBD_4.iObjPriority[iObj][iKillerTeam]), iKillerTeam, MC_serverBD_4.iObjPriority[iObj][iKillerTeam])
						PRINTLN("[Objects][Object ", sObjState.iIndex, "] SERVER_MANAGE_BREAKABLE_OBJECTS - Fragable object killed & is current priority for killer team ", iKillerTeam)
					ENDIF
				ENDIF
			ELSE
				FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
					IF MC_serverBD_4.iObjPriority[iObj][iTeam] < FMMC_MAX_RULES
						bNotIgnore = TRUE
					ENDIF
				ENDFOR
				IF bNotIgnore
					PRINTLN("[Objects][Object ", sObjState.iIndex, "] SERVER_MANAGE_BREAKABLE_OBJECTS - Fragable object killed, but isn't current highest priority for killer team ", iKillerTeam)
					BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_OBJ, 0, iKillerTeam, -1, KillerPlayer, ci_TARGET_OBJECT, iObj)
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[Objects][Object ", sObjState.iIndex, "] SERVER_MANAGE_BREAKABLE_OBJECTS - Fragable object killed, but couldn't find a killer team")
			GIVE_TEAM_POINTS_FOR_OBJ_KILL(iObj, sObjState.objIndex)
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD.iFragableCrate, iObj)
			IF sObjState.bObjExists
				PRINTLN("[Objects][Object ", sObjState.iIndex, "] SERVER_MANAGE_BREAKABLE_OBJECTS - Cleaning up fragable crate")
				CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(iObj)]) //Do not use sObjState here
			ENDIF
		ELSE
			FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
				IF MC_serverBD_4.iObjPriority[iObj][iTeam] < FMMC_MAX_RULES
					//Also completes the objective:
					PRINTLN("[Objects][Object ", sObjState.iIndex, "] SERVER_MANAGE_BREAKABLE_OBJECTS - Running fail checks for team ", iTeam, " as fragable object has been destroyed")
					RUN_TEAM_OBJ_FAIL_CHECKS(iObj, iTeam, MC_serverBD_4.iObjPriority[iObj][iTeam])
				ENDIF
			ENDFOR
		ENDIF
		
		SET_BIT(MC_serverBD.iCrateDestructionProcessed, iObj)
	ENDIF
		
ENDPROC

PROC PROCESS_DESTRUCTIBLE_CRATES_SERVER(FMMC_OBJECT_STATE &sObjState)
	
	INT iObj = sObjState.iIndex
	INT ipickup
	
	IF IS_BIT_SET(MC_serverBD.iFragableCrate, iObj)
	AND NOT IS_BIT_SET(MC_serverBD.iCrateBrokenBitset, iObj)
		IF IS_BIT_SET(MC_serverBD.iFragableCrateCreated, iObj)
			IF sObjState.bObjExists
				IF sObjState.bObjBroken
				OR !sObjState.bObjAlive
					FOR ipickup = 0 TO (ciMAX_CRATE_PICKUPS-1)
						IF IS_VECTOR_ZERO(MC_serverBD_3.vCratePickupCoords[iObj][ipickup])
							MC_serverBD_3.vCratePickupCoords[iObj][ipickup] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sObjState.objIndex, <<0.5-(ipickup*0.5), 0, 0.0>>)//0.1>>)
							PRINTLN("[RCC MISSION] PROCESS_DESTRUCTIBLE_CRATES_SERVER - OBJECT BROKEN - vCratePickupCoords was zero for iObj = ", iObj, " ipickup = ", ipickup, " vCratePickupCoords = ", MC_serverBD_3.vCratePickupCoords[iObj][ipickup])
							SET_BIT(MC_serverBD.iCrateBrokenBitset, iObj)
							PRINTLN("[RCC MISSION] PROCESS_DESTRUCTIBLE_CRATES_SERVER - iCrateBrokenBitset SET -  iObj = ", iObj)
						ENDIF
					ENDFOR
				ENDIF
			ELSE
				//-- Still want to get create pickups if object is destroyed.
				IF !sObjState.bObjAlive
					FOR ipickup = 0 TO (ciMAX_CRATE_PICKUPS-1)
						IF IS_VECTOR_ZERO(MC_serverBD_3.vCratePickupCoords[iObj][ipickup])
//							MC_serverBD_3.vCratePickupCoords[iObj][ipickup] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, <<0.5-(ipickup*0.5), 0, 0.0>>)//0.1>>)
//							PRINTLN("[RCC MISSION] PROCESS_DESTRUCTIBLE_CRATES_SERVER - OBJECT DESTROYED - vCratePickupCoords was zero for iObj = ", iObj, " ipickup = ", ipickup, " vCratePickupCoords = ", MC_serverBD_3.vCratePickupCoords[iObj][ipickup])
							SET_BIT(MC_serverBD.iCrateBrokenBitset, iObj)
							PRINTLN("[RCC MISSION] PROCESS_DESTRUCTIBLE_CRATES_SERVER - iCrateBrokenBitset SET -  iObj = ", iObj)
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		
		ELSE
			IF sObjState.bObjAlive
			AND NOT IS_VECTOR_ZERO(GET_FMMC_OBJECT_COORDS(sObjState))
				SET_BIT(MC_serverBD.iFragableCrateCreated, iObj)
				PRINTLN("[RCC MISSION] PROCESS_DESTRUCTIBLE_CRATES_SERVER - iFragableCrateCreated SET - AA -  iObj = ", iObj)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SERVER_CLEAR_OBJ_AT_HOLDING(INT iObj)
	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS -1
		IF IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeam],iobj)
			CLEAR_BIT(MC_serverBD.iObjAtYourHolding[iTeam],iobj)
			PRINTLN("[MC] SERVER_CLEAR_OBJ_AT_HOLDING - Clearing Obj bit: ", iobj, " from team: ", iTeam)
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL CLEANUP_OBJ_EARLY(FMMC_OBJECT_STATE &sObjState)
	
	INT iObj = sObjState.iIndex

	IF SHOULD_CLEANUP_OBJ(sObjState)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitset, cibsOBJ_IgnoreVisCheckCleanup)
			IF NOT sObjState.bHaveControlOfObj
				PRINTLN("[RCC MISSION] CLEANUP_OBJ_EARLY - Requesting control of object ",iObj," for deletion; setting MC_serverBD.iObjCleanup_NeedOwnershipBS bit")
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sObjState.niIndex)
				SET_BIT(MC_serverBD.iObjCleanup_NeedOwnershipBS, iObj)
			ELSE
				PRINTLN("[RCC MISSION] CLEANUP_OBJ_EARLY - Deleting object ", iObj)
				DELETE_FMMC_OBJECT(iObj)
				RETURN TRUE
			ENDIF
		ELSE
			IF sObjState.bHaveControlOfObj
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sObjState.objIndex, FALSE)
			ENDIF
			
			INT iTeam
			FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
				CLEAR_BIT(MC_serverBD.iObjteamFailBitset[iObj], iTeam)
				CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], iObj)
			ENDFOR
			
			IF IS_THIS_OBJECT_A_HACK_CONTAINER(iObj)
				CPRINTLN(DEBUG_CONTROLLER, "Object ", iObj, " is a hack container - cleaning up its extra special bits too")
				CLEAN_UP_CONTAINER_DOORS()
			ENDIF
			
			IF SHOULD_DRONE_SYSTEMS_BE_ACTIVE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].sObjDroneData)
				CLEAR_DRONE_INDEX(sObjState.iIndex, CREATION_TYPE_OBJECTS)
			ENDIF
			
			OBJECT_INDEX tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(iObj)])
			IF DOES_ENTITY_EXIST(tempObj)
				START_ENTITY_PTFX_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sEntityDespawnPTFX, GET_ENTITY_COORDS(tempObj, FALSE), GET_ENTITY_ROTATION(tempObj))
			ENDIF
			
			MC_serverBD_2.iCurrentObjRespawnLives[iObj] = 0 
			PRINTLN("[RCC MISSION] Obj ",iObj," getting cleaned up early - set MC_serverBD_2.iCurrentObjRespawnLives[", iObj, "] = 0. Call 1.")
			CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(iObj)])
			PRINTLN("[RCC MISSION] Obj getting cleaned up early - no longer needed, objective: ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCleanupObjective," team: ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCleanupTeam)
			
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Process Object Body ---------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Staggered loop used to process logic that only the Owner of the netID can execute.  ------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_OBJECT_INVINCIBILITY(FMMC_OBJECT_STATE &sObjState)
	
	IF NOT sObjState.bObjExists
	OR NOT sObjState.bHaveControlOfObj
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iInvincibleOnRulesTeam = -1
		EXIT	
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iInvincibleOnRules = 0
		EXIT	
	ENDIF

	INT iCurrentHighestPriority = MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iInvincibleOnRulesTeam]
	IF iCurrentHighestPriority >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	BOOL bInvincible = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iInvincibleOnRules, iCurrentHighestPriority)			
	
	SET_ENTITY_INVINCIBLE(sObjState.objIndex, bInvincible)
	
	IF bInvincible
		SET_ENTITY_PROOFS(sObjState.objIndex, bInvincible, bInvincible, bInvincible, bInvincible, bInvincible, bInvincible)
	ELSE
		MC_SET_UP_OBJ_PROOFS(sObjState.objIndex, sObjState.iIndex)
	ENDIF
	
	PRINTLN("[Objects][Object ", sObjState.iIndex, "] - PROCESS_VEHICLE_INVINCIBILITY - Setting vehicle to be ", PICK_STRING(bInvincible, "invincible.", "not invincible."))
	
ENDPROC

PROC SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(FMMC_OBJECT_STATE &sObjState, INT iTeam)
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
	OR NOT IS_OBJECT_A_PORTABLE_PICKUP(sObjState.objIndex)
		EXIT
	ENDIF

	INT iObj = sObjState.iIndex
	PRINTLN("[Objects][Object ", iObj, "] SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM - SET_TEAM_PICKUP_OBJECT - model = ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn), " iTeam = ", iTeam)
	
	SET_TEAM_PICKUP_OBJECT(sObjState.objIndex, iTeam, FALSE)
	
	IF MC_serverBD.iObjCarrier[iObj] != iLocalPart
		EXIT
	ENDIF
	
	IF iTeam != GET_LOCAL_PLAYER_TEAM(TRUE)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_SOMEONE)
		
		DETACH_PORTABLE_PICKUP_FROM_PED(sObjState.objIndex)
		
		IF NOT DOES_OBJECT_HAVE_ANY_PRIORITY_FOR_ANY_TEAM(iObj)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSet, cibsOBJ_InvisibleOnFirstDelivery)
		OR g_bMissionEnding
			PRINTLN("[Objects][Object ", sObjState.iIndex, "] SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM - Pilot school flag is no longer needed, setting invisible...")
			
			SET_ENTITY_VISIBLE(sObjState.objIndex, FALSE)
			SET_ENTITY_COLLISION(sObjState.objIndex, FALSE)
			HIDE_PORTABLE_PICKUP_WHEN_DETACHED(sObjState.objIndex, TRUE) 
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL CAN_OBJECT_BE_PICKED_UP_BY_TEAM(INT iObj, INT iTeam)
	
	IF IS_BIT_SET(iObjWasAttachedEarlyBS, iObj)
		// We know it shouldn't be collectable yet, but we want it to be attached early anyway
		PRINTLN("[Objects_SPAM][Object_SPAM ", iObj, "] CAN_OBJECT_BE_PICKED_UP_BY_TEAM | Allowing object to be collectable early due to iObjWasAttachedEarlyBS")
		RETURN TRUE
	ENDIF
	
	IF MC_serverBD_4.iObjPriority[iObj][iTeam] < FMMC_MAX_RULES
		IF MC_serverBD_4.iObjPriority[iObj][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam]
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_AllowPickupForFutureRules)
			IF MC_serverBD_4.iObjRule[iObj][iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
			AND DOES_OBJECT_RULE_TYPE_REQUIRE_PICKUP(MC_serverBD_4.iObjRule[iObj][iTeam], IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeam], iObj))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF (MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
	AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjRequiredForRulePass[MC_serverBD_4.iCurrentHighestPriority[iTeam]] = iObj)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_OBJECT_PICKUP_STATUS(FMMC_OBJECT_STATE &sObjState)

	INT iObj = sObjState.iIndex

	IF !sObjState.bHaveControlOfObj
		EXIT
	ENDIF

	IF IS_BIT_SET(MC_serverBD.iFragableCrate, iObj)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iWasHackObj, iObj)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iObjDeliveredBitset, iObj)
		EXIT
	ENDIF
	
	IF HAS_PLAYER_QUIT_MISSION(iLocalPart)
		EXIT
	ENDIF
	
	INT iTeam
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
	
		IF NOT IS_TEAM_ACTIVE(iTeam)
			PRINTLN("[Objects][Object ", iObj, "] PROCESS_EVERY_FRAME_DROP_OFF - SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM Call 1 Team ", iTeam)
			SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(sObjState, iTeam)
			RELOOP
		ENDIF
		
		IF HAS_TEAM_FAILED(iTeam)
			PRINTLN("[Objects][Object ", iObj, "] PROCESS_EVERY_FRAME_DROP_OFF - SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM Call 2 Team ", iTeam)
			SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(sObjState, iTeam)
			RELOOP
		ENDIF
		
		IF NOT CAN_OBJECT_BE_PICKED_UP_BY_TEAM(iObj, iTeam)
			PRINTLN("[Objects][Object ", iObj, "] PROCESS_EVERY_FRAME_DROP_OFF - SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM Call 3 Team ", iTeam)
			SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(sObjState, iTeam)
			RELOOP
		ENDIF
		
		IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		OR (ARE_WE_CURRENTLY_IN_SUDDEN_DEATH() AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciNEXT_DELIVERY_WINS_SD_ONLY))
			
			PRINTLN("[Objects][Object ", iObj, "] PROCESS_EVERY_FRAME_DROP_OFF - Processing collectable pickup. Team: ", iTeam)
			
			IF (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn != INT_TO_ENUM(MODEL_NAMES, HASH("pil_Prop_Pilot_Icon_01")))
			AND (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn != INT_TO_ENUM(MODEL_NAMES, HASH("prop_ic_cp_bag")))
			AND (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("ar_prop_ar_cp_bag")))
			AND NOT DOES_PICKUP_NEED_BIGGER_RADIUS(iObj)
				SET_ENTITY_LOD_DIST(sObjState.objIndex, 150)
			ENDIF
			
			IF IS_OBJECT_A_PORTABLE_PICKUP(sObjState.objIndex)
				PRINTLN("[Objects][Object ", iObj, "] PROCESS_EVERY_FRAME_DROP_OFF - Setting obj to team pickup for team ", iTeam)
				//SET_ENTITY_ALPHA(sObjState.objIndex,255,FALSE)
				SET_TEAM_PICKUP_OBJECT(sObjState.objIndex, iTeam, TRUE)
				SET_ENTITY_COLLISION(sObjState.objIndex, TRUE)
				PREVENT_COLLECTION_OF_PORTABLE_PICKUP(sObjState.objIndex, FALSE)
			ENDIF
			
			IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
			AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciNEXT_DELIVERY_WINS_SD_ONLY)
				INT iTeamPickup
				FOR iTeamPickup = 0 TO MC_serverBD.iNumberOfTeams - 1
					IF IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeamPickup],iObj)
					AND IS_TEAM_ACTIVE(iTeamPickup)
						PRINTLN("[Objects][Object ", iObj, "] PROCESS_EVERY_FRAME_DROP_OFF - SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM Call 4 Team ", iTeam)
						SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(sObjState, iTeam)
					ENDIF
				ENDFOR
			ENDIF
			
		ENDIF

	ENDFOR
	
ENDPROC

FUNC BOOL IS_EVERY_FRAME_DROP_OFF_OK_TO_DROP()
	
	IF MC_serverBD_4.iObjMissionLogic[GET_LOCAL_PLAYER_TEAM(TRUE)] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER 
		IF IS_ENTITY_IN_AIR(LocalPlayerPed)
			PRINTLN("IS_EVERY_FRAME_DROP_OFF_OK_TO_DROP - In Air")
			RETURN FALSE
		ENDIF
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			IF NOT IS_VEHICLE_ON_ALL_WHEELS(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
				PRINTLN("IS_EVERY_FRAME_DROP_OFF_OK_TO_DROP - Not on 4 wheels")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC

PROC PROCESS_EVERY_FRAME_DROP_OFF()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_FAST_DROP_OFF)
		EXIT
	ENDIF
	
	INT iObj
	FMMC_OBJECT_STATE sObjState
		
	FOR iObj = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1
	
		FILL_FMMC_OBJECT_STATE_STRUCT(sObjState, iObj, TRUE)
		
		IF !sObjState.bObjExists
			RELOOP
		ENDIF
	
		INT iTeam = GET_LOCAL_PLAYER_TEAM()
			
		PROCESS_OBJECT_PICKUP_STATUS(sObjState)
			
		IF sObjState.bHaveControlOfObj
		AND IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_SOMEONE)
		
			IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
				
				IF MC_serverBD_4.iObjPriority[iObj][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam]
					IF NOT HAS_NET_TIMER_STARTED(tdEveryFrameDropOffDelay[iObj])
						PRINTLN("[Objects][Object ", iObj, "] PROCESS_EVERY_FRAME_DROP_OFF - starting tdEveryFrameDropOffDelay")
						REINIT_NET_TIMER(tdEveryFrameDropOffDelay[iObj])
					ELIF HAS_NET_TIMER_EXPIRED(tdEveryFrameDropOffDelay[iObj], ciEVERY_FRAME_DROP_OFF_DELAY_TIME)	
						IF IS_ENTITY_IN_DROP_OFF_AREA(sObjState.objIndex, iTeam, MC_serverBD_4.iObjPriority[iObj][iTeam], ciRULE_TYPE_OBJECT, iObj)
							IF HAS_NET_TIMER_STARTED(tdDropOff)
								IF HAS_NET_TIMER_EXPIRED(tdDropOff, iDropTimer)
									IF IS_EVERY_FRAME_DROP_OFF_OK_TO_DROP()
										PRINTLN("[Objects][Object ", iObj, "] PROCESS_EVERY_FRAME_DROP_OFF - Setting bit for iDetachedObjectSuccessfully.")
										SET_BIT(iDetachedObjectSuccessfully, iObj)								
										SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(sObjState, iTeam)
										SET_BIT(iLocalObjAtHolding, iObj)
										RESET_NET_TIMER(tdDropOff)
										PRINTLN("[Objects][Object ", iObj, "] PROCESS_EVERY_FRAME_DROP_OFF - Setting bit for iLocalObjAtHolding.")
									ENDIF
								ENDIF
							ELSE
								REINIT_NET_TIMER(tdDropOff)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF HAS_TEAM_FAILED(iTeam)
					SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(sObjState, iTeam)
					PRINTLN("[Objects][Object ", iObj, "] PROCESS_EVERY_FRAME_DROP_OFF - Team has failed, dropping pickup now")
				ENDIF
				
			ENDIF
			
		ELSE
		
			IF HAS_NET_TIMER_STARTED(tdEveryFrameDropOffDelay[iObj])
				PRINTLN("[Objects][Object ", iObj, "] PROCESS_EVERY_FRAME_DROP_OFF - resetting tdEveryFrameDropOffDelay")
				RESET_NET_TIMER(tdEveryFrameDropOffDelay[iObj])
			ENDIF
			
			IF IS_BIT_SET(iDetachedObjectSuccessfully, iObj)
				PRINTLN("[Objects][Object ", iObj, "] PROCESS_EVERY_FRAME_DROP_OFF - Detatching portable pickup! We are in drop off area.")
				MC_playerBD[iLocalPart].iNumberOfDeliveries++
				MC_PlayerBD[iLocalPart].iPackagesAtHolding++
				PRINTLN("[Objects][Object ", iObj, "] PROCESS_EVERY_FRAME_DROP_OFF - Incrementing my delivery count to: ", MC_playerBD[iLocalPart].iNumberOfDeliveries)
				CLEAR_BIT(iDetachedObjectSuccessfully, iObj)
				INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
				IF iRule > -1
				AND iRule < FMMC_MAX_RULES
				AND sObjState.bHaveControlOfObj
					IF NOT IS_ENTITY_IN_DROP_OFF_AREA(sObjState.objIndex, iTeam, MC_serverBD_4.iObjPriority[iobj][iTeam], ciRULE_TYPE_OBJECT, iObj)
						VECTOR vTempPos = GET_FMMC_OBJECT_COORDS(sObjState)
						VECTOR vNewPos = GET_SAFE_PICKUP_COORDS(GET_CENTER_OF_AREA(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[iRule], g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[iRule]),0.5,3)
						IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_CYLINDER
							vNewPos = GET_SAFE_PICKUP_COORDS(GET_OBJECT_DROPOFF_COORDS(iObj))
						ENDIF
						vNewPos.z = vTempPos.z
						PRINTLN("[Objects][Object ", iObj, "] PROCESS_EVERY_FRAME_DROP_OFF - Object has been delivered but not in the area. Moving to ", vNewPos)
						SET_ENTITY_COORDS(sObjState.objIndex, vNewPos)
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeam], iObj)
		AND IS_BIT_SET(iLocalObjAtHolding, iObj)
			CLEAR_BIT(iLocalObjAtHolding, iObj)
			PRINTLN("[Objects][Object ", iObj, "] PROCESS_EVERY_FRAME_DROP_OFF - Clearing iLocalObjAtHolding for obj letting server take over")
		ENDIF
			
	ENDFOR
	
ENDPROC

PROC HANDLE_OBJECT_DESTROY_HUD(OBJECT_INDEX Victimobj)
	
	//Objects destroyed HUD
	IF bIsLocalPlayerHost
		INT iobjID = IS_OBJ_A_MISSION_CREATOR_OBJ(Victimobj)
		
		IF iobjID >= 0
			INT iTeam
			
			PRINTLN("[TargetsRemaining] I am the host and I have seen that obj ", iObjID, " has died!")
			
			FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
				IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
				
					PRINTLN("[TargetsRemaining] The objpriority for obj ", iObjID, " for team ", iTeam, " is ", MC_serverBD_4.iObjPriority[iobjID][iTeam], ", while the priority of that team is ", MC_serverBD_4.iCurrentHighestPriority[iTeam])
					
					IF MC_serverBD_4.iObjPriority[iobjID][iTeam] <= FMMC_MAX_RULES
					AND MC_serverBD_4.iObjPriority[iobjID][iTeam] >= MC_serverBD_4.iCurrentHighestPriority[iTeam]
						IF NOT IS_BIT_SET(MC_ServerBD_4.iObjectsDestroyedForSpecialHUDBitset[iTeam], iobjID)																
							SET_BIT(MC_ServerBD_4.iObjectsDestroyedForSpecialHUDBitset[iTeam], iobjID)
							PRINTLN("[TargetsRemaining] [RCC MISSION][PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT][iEntityincrement] HANDLE_OBJECT_DESTROY_HUD - iTeam: ", iTeam, " A new object has been destroyed obj: ", iobjID, " Incrementing iObjectsDestroyedForSpecialHUD to: ", (MC_ServerBD_4.iObjectsDestroyedForSpecialHUD[iTeam]+1))
							MC_ServerBD_4.iObjectsDestroyedForSpecialHUD[iTeam]++
						ELSE
							PRINTLN("[TargetsRemaining] [RCC MISSION][PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT][iEntityincrement] HANDLE_OBJECT_DESTROY_HUD - Bit is already set for obj ", iObjID, " for team ", iTeam)
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_IN_AND_OUT_PACKAGE_SOUNDS()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_IN_AND_OUT_SOUNDS)
	
		INT iTeam = GET_LOCAL_PLAYER_TEAM()
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule < FMMC_MAX_RULES
			IF MC_playerBD[iLocalPart].iObjCarryCount > 0
				IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_HAS_PLAYED_IAO_PICKUP_SOUND)
					BROADCAST_IN_AND_OUT_SFX_PLAY(ciIN_AND_OUT_SOUND_PICKUP, iTeam)
					SET_BIT(iLocalBoolCheck16, LBOOL16_HAS_PLAYED_IAO_PICKUP_SOUND)
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_HAS_PLAYED_IAO_PICKUP_SOUND)
					BROADCAST_IN_AND_OUT_SFX_PLAY(ciIN_AND_OUT_SOUND_DROP, iTeam)
					CLEAR_BIT(iLocalBoolCheck16, LBOOL16_HAS_PLAYED_IAO_PICKUP_SOUND)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_OBJECT_RULE_VISIBILITY(INT iObj)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_VISIBILITY_BY_RULE)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bHideObjects
		EXIT
	ENDIF
	#ENDIF
	
	BOOL bDisplay = FALSE
	INT iTeamLoop
	
	REPEAT MC_ServerBD.iNumberOfTeams iTeamLoop
		CLOGLN(DEBUG_MISSION, CHANNEL_SEVERITY_DEBUG1, "[PROCESS_OBJECT_RULE_VISIBILITY]  New priority for team ",iTeamLoop, " Checking iObj: ",iObj) 
		IF MC_serverBD_4.iObjPriority[iObj][iTeamLoop] = MC_ServerBD_4.iCurrentHighestPriority[iTeamLoop]	
			bDisplay = TRUE
			CLOGLN(DEBUG_MISSION, CHANNEL_SEVERITY_DEBUG1, "[PROCESS_OBJECT_RULE_VISIBILITY] Current highest priority same as object priority ",MC_serverBD_4.iObjPriority[iObj][iTeamLoop]) 
			BREAKLOOP
		ENDIF
	ENDREPEAT

	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iObj))
		CLOGLN(DEBUG_MISSION, CHANNEL_SEVERITY_DEBUG1, "[PROCESS_OBJECT_RULE_VISIBILITY]  Obj: ",iObj," Doesn't exist") 
		EXIT
	ENDIF
	OBJECT_INDEX tempObj = NET_TO_OBJ(GET_OBJECT_NET_ID(iObj))
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
		CLOGLN(DEBUG_MISSION, CHANNEL_SEVERITY_DEBUG1, "[PROCESS_OBJECT_RULE_VISIBILITY]  Obj: ",iObj," Doesn't have control of entity") 
		EXIT
	ENDIF
	IF IS_ENTITY_VISIBLE_TO_SCRIPT(tempObj) = bDisplay
		CLOGLN(DEBUG_MISSION, CHANNEL_SEVERITY_DEBUG1, "[PROCESS_OBJECT_RULE_VISIBILITY]  Obj: ",iObj," Same visibility ",bDisplay) 
		EXIT
	ENDIF
	CLOGLN(DEBUG_MISSION, CHANNEL_SEVERITY_DEBUG1, "[PROCESS_OBJECT_RULE_VISIBILITY] SET_ENTITY_VISIBLE ",bDisplay) 
	SET_ENTITY_VISIBLE(tempObj,bDisplay)
	
ENDPROC

FUNC BOOL IS_OBJECT_VALID_FOR_FLAG_RETURN(FMMC_OBJECT_STATE& sObjState)
	
	IF ARE_VECTORS_ALMOST_EQUAL(GET_FMMC_OBJECT_COORDS(sObjState), g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].vPos, 3)
		// This flag is already more or less at its original position
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_FLAG_RETURN(FMMC_OBJECT_STATE& sObjState)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_EnableObjectReturn)
		EXIT
	ENDIF
	
	IF NOT sObjState.bObjAlive
	OR GET_LOCAL_PLAYER_CURRENT_RULE() > FMMC_MAX_RULES
	OR HAS_TEAM_FINISHED(GET_LOCAL_PLAYER_TEAM())
	OR bIsAnySpectator
	OR NOT bLocalPlayerPedOK
	OR GET_LOCAL_PLAYER_MULTIRULE_TIMER_TIME_REMAINING() < 1000
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iObjPriority[sObjState.iIndex][GET_LOCAL_PLAYER_TEAM()] != FMMC_MAX_RULES
		// This object is one of OUR objectives, something for us to steal, not our own flag for us to return
		EXIT
	ENDIF
	
	IF NOT ARE_MULTIPLE_TEAMS_ON_SAME_SCORE()
		PRINTLN("[Objects] PROCESS_FLAG_RETURN | Multiple teams aren't on the same score, so flag return is disabled for now")
		EXIT
	ENDIF
	
	FLOAT fReturnSize = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iGotoRange)
	VECTOR vPosToCheck = GET_FMMC_OBJECT_COORDS(sObjState)
	
	IF VDIST2(vPosToCheck, GET_ENTITY_COORDS(LocalPlayerPed)) > fReturnSize * fReturnSize
		// Too far away to return it!
		EXIT
	ENDIF
	
	IF NOT IS_OBJECT_VALID_FOR_FLAG_RETURN(sObjState)
		EXIT
	ENDIF
	
	IF NOT sObjState.bHaveControlOfObj
		IF NOT IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_SOMEONE)
			NETWORK_REQUEST_CONTROL_OF_ENTITY(sObjState.objIndex)
			PRINTLN("[Objects] PROCESS_FLAG_RETURN | Requesting control of object: ", sObjState.iIndex, " to return it.")
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_SOMEONE)
			SET_BIT(iLocalBoolCheck30, LBOOL30_RETURNING_A_FLAG)
			PRINTLN("[Objects] PROCESS_FLAG_RETURN | Preparing to return. Setting LBOOL30_RETURNING_A_FLAG")
			SET_TEAM_PICKUP_OBJECT(sObjState.objIndex, GET_LOCAL_PLAYER_TEAM(), TRUE)
			ATTACH_PORTABLE_PICKUP_TO_PED(sObjState.objIndex, LocalPlayerPed)
			
		ELIF IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_LOCAL_PLAYER)
			DETACH_PORTABLE_PICKUP_FROM_PED(sObjState.objIndex)
			BROADCAST_FMMC_RETURNED_FLAG_SHARD(GET_LOCAL_PLAYER_TEAM(), LocalPlayer)
			SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(sObjState, 0)
			SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(sObjState, 1)
			
			VECTOR vDestination = g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].vPos
			vDestination.z += 1
			SET_ENTITY_COORDS(sObjState.objIndex, vDestination)
			SET_ENTITY_ROTATION(sObjState.objIndex, <<0.0,0.0, g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].fHead>>)
			
			MC_playerBD_1[iLocalPart].iObjectsReturned++
			PRINTLN("[Objects] PROCESS_FLAG_RETURN | Flag ", sObjState.iIndex, " returned. Number of flags returned: ", MC_playerBD_1[iLocalPart].iObjectsReturned)
			
			CLEAR_BIT(iLocalBoolCheck30, LBOOL30_RETURNING_A_FLAG)
			PRINTLN("[Objects] PROCESS_FLAG_RETURN | Clearing LBOOL30_RETURNING_A_FLAG")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_GRANULAR_CAPTURE(INT iObj)
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + GET_LOCAL_PLAYER_TEAM())
	OR g_bMissionOver OR g_bMissionEnding
	OR NOT bLocalPlayerPedOk
	OR ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)	
			CLEAR_BIT(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)
			PRINTLN("[JS][GRANCAP] - PROCESS_GRANULAR_CAPTURE - (4) we are no longer carrying obj - ", iObj)
		ENDIF
		EXIT
	ENDIF
	IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iObj))
		ENTITY_INDEX tempObj = NET_TO_ENT(GET_OBJECT_NET_ID(iObj))
		IF IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
			ENTITY_INDEX tempPlayerEnt = GET_ENTITY_ATTACHED_TO(tempObj)
			IF DOES_ENTITY_EXIST(tempPlayerEnt)
				IF GET_PED_INDEX_FROM_ENTITY_INDEX(tempPlayerEnt) = LocalPlayerPed
					IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)
						PRINTLN("[JS][GRANCAP] - PROCESS_GRANULAR_CAPTURE - we are now carrying obj - ", iObj)
						SET_BIT(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)
					ENDIF
				ELSE
					IF IS_BIT_SET(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)
						PRINTLN("[JS][GRANCAP] - PROCESS_GRANULAR_CAPTURE - (1) we are no longer carrying obj - ", iObj)
						CLEAR_BIT(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)
					PRINTLN("[JS][GRANCAP] - PROCESS_GRANULAR_CAPTURE - (2) we are no longer carrying obj - ", iObj)
					CLEAR_BIT(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)
				PRINTLN("[JS][GRANCAP] - PROCESS_GRANULAR_CAPTURE - (3) we are no longer carrying obj - ", iObj)
				CLEAR_BIT(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)
			ENDIF
		ENDIF
	ENDIF

	IF IS_BIT_SET(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)
		IF HAS_NET_TIMER_STARTED(tdGranularCaptureTimer)
			IF HAS_NET_TIMER_EXPIRED(tdGranularCaptureTimer, ciGRANULAR_CAPTURE_TIME)
				iGranularPointsThisFrame++
			ENDIF
		ELSE
			REINIT_NET_TIMER(tdGranularCaptureTimer)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_KEEPING_OBJECT_IN_DROP_OFF(FMMC_OBJECT_STATE& sObjState)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_KeepObjectsInDropOff)
		EXIT
	ENDIF
	
	IF NOT sObjState.bObjExists
	OR NOT sObjState.bHaveControlOfObj
	OR IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_ATTACHED_TO_SOMETHING)
		EXIT
	ENDIF
	
	INT iTeam = iStaggeredTeamIterator
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	VECTOR vObjCoords = GET_FMMC_OBJECT_COORDS(sObjState)

	IF IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeam], sObjState.iIndex)
		IF NOT IS_POINT_IN_ANGLED_AREA(vObjCoords, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[iRule], g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[iRule], g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[iRule])
			VECTOR vDropOffCenter = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[iRule] + g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[iRule]
			vDropOffCenter *= << 0.5, 0.5, 0.5 >>
			
			PRINTLN("[RCC MISSION][Objects][Object ", sObjState.iIndex, "] PROCESS_KEEPING_OBJECT_IN_DROP_OFF | Moving Object ", sObjState.iIndex, " to vDropOffCenter = ", vDropOffCenter)
			SET_ENTITY_COORDS(sObjState.objIndex, vDropOffCenter)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_OBJECT_PREREQS(FMMC_OBJECT_STATE &sObjState)

	IF NOT sObjState.bObjExists
		EXIT
	ENDIF
	
	IF IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_LOCAL_PLAYER)
		IF IS_BIT_SET(iCollectObjectPreReqsAppliedBS, sObjState.iIndex)
			EXIT
		ENDIF

		IF IS_LONG_BITSET_EMPTY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iSetPrereqsOnCollectionBS)
			EXIT
		ENDIF
		   
		PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_PREREQS - Applying PreReqs on collection")
		SET_PREREQUISITES_COMPLETED_FROM_LONG_BITSET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iSetPrereqsOnCollectionBS, TRUE)
		SET_BIT(iCollectObjectPreReqsAppliedBS, sObjState.iIndex)
	ELSE
		IF NOT IS_BIT_SET(iCollectObjectPreReqsAppliedBS, sObjState.iIndex)
			EXIT
		ENDIF
		
		IF IS_LONG_BITSET_EMPTY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iSetPrereqsOnCollectionBS)
			EXIT
		ENDIF
		
		PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_PREREQS - Clearing PreReqs on collection")
		RESET_PREREQUISITES_COMPLETED_FROM_LONG_BITSET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iSetPrereqsOnCollectionBS, TRUE)
		CLEAR_BIT(iCollectObjectPreReqsAppliedBS, sObjState.iIndex)
	ENDIF
	
ENDPROC

PROC PROCESS_OBJECT_PREREQS_NO_RESET(FMMC_OBJECT_STATE &sObjState)

	IF NOT sObjState.bObjExists
		EXIT
	ENDIF
	
	IF IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_LOCAL_PLAYER)
		IF IS_BIT_SET(iCollectObjectPreReqsNoResetAppliedBS, sObjState.iIndex)
			EXIT
		ENDIF

		IF IS_LONG_BITSET_EMPTY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iSetPrereqsOnCollectionNoResetBS)
			EXIT
		ENDIF
		   
		PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_PREREQS_NO_RESET - Applying PreReqs on collection")
		SET_PREREQUISITES_COMPLETED_FROM_LONG_BITSET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iSetPrereqsOnCollectionNoResetBS, TRUE)
		SET_BIT(iCollectObjectPreReqsNoResetAppliedBS, sObjState.iIndex)
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Pickup / Collection
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT DETACH_PORTABLE_PICKUP_FROM_SPECIFIC_PED(ENTITY_INDEX ped, BOOL bStolen = FALSE)
	INT iobj
	OBJECT_INDEX tempObj
	INT iLastDrop = -1
	FOR iobj = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
		IF MC_serverBD.iObjCarrier[iobj] = iPartToUse
			IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iObj))
				tempObj = NET_TO_OBJ(GET_OBJECT_NET_ID(iObj))
				IF IS_ENTITY_ATTACHED_TO_ENTITY(tempObj, ped)					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciPICKUP_DROP_ON_TRIANGLE)
					OR bStolen
						//Just disabling this as soon as it is dropped to prevent instant pickup
						PREVENT_COLLECTION_OF_PORTABLE_PICKUP(tempObj, TRUE, TRUE)
						PRINTLN("[DETACH_PORTABLE_PICKUP_FROM_SPECIFIC_PED][STOLEN] - PREVENT_COLLECTION_OF_PORTABLE_PICKUP called for iObj: ", iObj)
					//	iObjectJustHadStolen = iObj
					ENDIF
					
					PRINTLN("[DETACH_PORTABLE_PICKUP_FROM_SPECIFIC_PED]- DETACH_PORTABLE_PICKUP_FROM_PED called")
					DETACH_PORTABLE_PICKUP_FROM_PED(tempObj)
					
					iLastDrop = iobj
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	PRINTLN("[DETACH_PORTABLE_PICKUP_FROM_SPECIFIC_PED] - Returning iObj ",iLastDrop)
	RETURN iLastDrop
ENDFUNC

PROC DETACH_ANY_PORTABLE_PICKUPS()

	INT iobj
	OBJECT_INDEX tempObj
	
	FOR iobj = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
		IF MC_serverBD.iObjCarrier[iobj] = iPartToUse
			IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iObj))
				tempObj = NET_TO_OBJ(GET_OBJECT_NET_ID(iObj))
				IF IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
					DETACH_PORTABLE_PICKUP_FROM_PED(tempObj)
					IF (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("pil_Prop_Pilot_Icon_01")))
					AND (NOT DOES_OBJECT_HAVE_ANY_PRIORITY_FOR_ANY_TEAM(iobj))
						PRINTLN("[RCC MISSION] DETACH_ANY_PORTABLE_PICKUPS - Pilot school flag (object number ",iobj,") is no longer needed, setting invisible...")
						SET_ENTITY_VISIBLE(tempObj,FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC DETACH_AND_RESET_ANY_PICKUPS()
	
	INT iobj
	OBJECT_INDEX tempObj
	
	FOR iobj = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
		IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iObj))
			tempObj = NET_TO_OBJ(GET_OBJECT_NET_ID(iObj))
			IF IS_ENTITY_ATTACHED_TO_ENTITY(tempObj, LocalPlayerPed)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
				PRINTLN("[KH] DETACH_AND_RESET_ANY_PICKUPS - DETACHING AND RESETTING PICKUP FROM PLAYER")
				DETACH_PORTABLE_PICKUP_FROM_PED(tempObj)
				MC_SET_UP_OBJ(tempObj, iobj, TRUE, TRUE)
			ELSE
				PRINTLN("[KH] DETACH_AND_RESET_ANY_PICKUPS - NOT ATTACHED/OWNER OF ", iObj)
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC PROCESS_PACKAGE_HANDLING()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciPICKUP_DROP_ON_TRIANGLE)
		INT iTeam = GET_LOCAL_PLAYER_TEAM(TRUE)
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule < FMMC_MAX_RULES
			IF bLocalPlayerPedOK
			AND NOT IS_BROWSER_OPEN()
			AND NOT IS_CELLPHONE_CAMERA_IN_USE()
			AND GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					DISPLAY_HELP_TEXT_THIS_FRAME("RUG_HELP_1", FALSE)
				ELSE
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("IBI_HELP_1")
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("IBI_HELP_2")
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_SCORE_HT") 
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_SCORE_H")
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("IBI_HELP_3")
						DISPLAY_HELP_TEXT_THIS_FRAME("RUG_HELP_1", FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
			AND NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_IS_DIVE_SCORE_ANIM_IN_PROGRESS)
				IF NOT HAS_NET_TIMER_STARTED(stPickupTimeoutTimer)
					START_NET_TIMER(stPickupTimeoutTimer)
					PRINTLN("[KH] PROCESS_PACKAGE_HANDLING - STARTING PACKAGE PICKUP TIMEOUT TIMER")
				ELSE
					REINIT_NET_TIMER(stPickupTimeoutTimer)
				ENDIF
				
				DETACH_PORTABLE_PICKUP_FROM_SPECIFIC_PED(LocalPlayerPed)
				
				IF DOES_PLAYER_HAVE_WEAPON(wtCachedLastWeaponHeld)
					SET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCachedLastWeaponHeld, TRUE)
				ELSE
					SET_CURRENT_PED_WEAPON(LocalPlayerPed, GET_BEST_PED_WEAPON(LocalPlayerPed), TRUE)
				ENDIF
				bHasObjectJustBeenDropped = TRUE
			ENDIF
		ENDIF
	ELSE
		DISPLAY_HELP_TEXT_THIS_FRAME("FMMCC_DROP_PACK", FALSE)
		
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
			DETACH_PORTABLE_PICKUP_FROM_SPECIFIC_PED(LocalPlayerPed)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_OBJECT_COLLECTION_BE_BLOCKED_BY_STRICT_PICKUP_DISTANCE(FMMC_OBJECT_STATE& sObjState)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].fObjectStrictPickupDistance < 0.05 // Checks 0.05 to avoid any dodgy float issues
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iObjHasBeenCarriedAtSomePointBS, sObjState.iIndex)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetFour, cibsOBJ4_RetainStrictPickupDistanceWhenDropped)
			// Ignoring strict pickup distance because this object has been carried elsewhere
			RETURN FALSE
		ENDIF
	ENDIF
	
	VECTOR vObjectPos = GET_ENTITY_COORDS(sObjState.objIndex)
	VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
	
	FLOAT fDist = VDIST2(vObjectPos, vPlayerPos)
	FLOAT fStrictPickupDistance = POW(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].fObjectStrictPickupDistance, 2.0)

	IF fDist > fStrictPickupDistance
	AND fDist < 100 // Don't bother proceeding & spamming the logs if the player isn't nearby - the player is too far away to be worrying about the strict pickup distance anyway
		#IF IS_DEBUG_BUILD
		IF bObjDebug
			DRAW_DEBUG_TEXT_2D("Blocked by SHOULD_OBJECT_COLLECTION_BE_BLOCKED_BY_STRICT_PICKUP_DISTANCE", GET_FMMC_OBJECT_COORDS(sObjState), 255, 255, 255, 255)
		ENDIF
		#ENDIF
		
		PRINTLN("[Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] SHOULD_OBJECT_COLLECTION_BE_BLOCKED_BY_STRICT_PICKUP_DISTANCE | Player is further away than this object's strict pickup distance! fDist: ", fDist, " || fObjectStrictPickupDistance: ", fStrictPickupDistance)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_PREVENTING_OBJECT_PICKUP(FMMC_OBJECT_STATE& sObjState)
	
	IF NOT sObjState.bObjExists
		EXIT	
	ENDIF
	
	BOOL bBlockingObjectPickup = FALSE
	BOOL bTriangleHeld = FALSE
	
	//If the player is holding triangle they should not be able to pick the object up
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciHOLD_TRIANGLE_NO_PICKUP)
		IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
		OR (IS_PLAYER_RESPAWNING(LocalPlayer) 
		AND NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE))
			IF IS_PLAYER_RESPAWNING(LocalPlayer)
				PRINTLN("[KH] PROCESS_PREVENTING_OBJECT_PICKUP - PLAYER IS RESPAWNING - PREVENTING OBJECT PICKUP")
			ELSE
				PRINTLN("[KH] PROCESS_PREVENTING_OBJECT_PICKUP - TRIANGLE IS PRESSED - PREVENTING OBJECT PICKUP")
			ENDIF
			bBlockingObjectPickup = TRUE
			bTriangleHeld = TRUE
		ELSE
			bBlockingObjectPickup = FALSE
		ENDIF
	ENDIF
	
	//After the player has dropped the object there should be a cooldown period before they can pick it back up
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciPICKUP_DROP_ON_TRIANGLE)
		IF HAS_NET_TIMER_STARTED(stPickupTimeoutTimer)
			PRINTLN("[KH] PROCESS_PREVENTING_OBJECT_PICKUP - TIMEOUT TIMER HAS STARTED - PREVENTING OBJECT PICKUP")
			INT iTimeElapsed = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stPickupTimeoutTimer)
			INT iTimeout = iPickupTimeoutTime
			
			IF iTimeElapsed > iTimeout
				PRINTLN("[KH] PROCESS_PREVENTING_OBJECT_PICKUP - RESETTING TIMER - ALLOWING OBJECT PICKUP")
				RESET_NET_TIMER(stPickupTimeoutTimer)
				
				IF !bTriangleHeld
					bBlockingObjectPickup = FALSE
				ENDIF
			ELSE
				bBlockingObjectPickup = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	//Stops the player picking up any objects while manually respawning
	IF (manualRespawnState >= eManualRespawnState_FADING_OUT)
	OR HAS_NET_TIMER_STARTED(timerManualRespawn)
		PRINTLN("[KH] PROCESS_PREVENTING_OBJECT_PICKUP - PLAYER IS MANUALLY RESPAWNING - PREVENTING OBJECT PICKUP")
		bBlockingObjectPickup = TRUE
	ENDIF
	
	IF SHOULD_OBJECT_COLLECTION_BE_BLOCKED_BY_STRICT_PICKUP_DISTANCE(sObjState)
		bBlockingObjectPickup = TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bBlockingObjectPickup != bBlockingObjectLastFrame
			PRINTLN("[KH] PROCESS_PREVENTING_OBJECT_PICKUP - bBlockingObjectPickup changed to: ", bBlockingObjectPickup)
		ENDIF

		bBlockingObjectLastFrame = bBlockingObjectPickup
	#ENDIF
	
	IF IS_OBJECT_A_PORTABLE_PICKUP(sObjState.objIndex)
		PREVENT_COLLECTION_OF_PORTABLE_PICKUP(sObjState.objIndex, bBlockingObjectPickup, TRUE)
	ENDIF
	
ENDPROC

PROC MAINTAIN_FROZEN_OBJECTS(FMMC_OBJECT_STATE &sObjState)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSet, cibsOBJ_FreezePosition)
		IF IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_ATTACHED_TO_SOMETHING)
			PRINTLN("[Objects][Object ", sObjState.iIndex, "] MAINTAIN_FROZEN_OBJECTS - Object is attached to something and therefore cannot be frozen")
			EXIT
		ENDIF
	
		FREEZE_ENTITY_POSITION(sObjState.objIndex, TRUE)
	ENDIF
	
ENDPROC

PROC PLAY_EXPLODE_OUT_OF_ZONE_AUDIO(OBJECT_INDEX tempObj)

	IF iSpectatorTarget = -1
	AND IS_SOUND_ID_VALID(iExplodeCountdownSound)
	AND HAS_SOUND_FINISHED(iExplodeCountdownSound)
		PLAY_SOUND_FROM_ENTITY(iExplodeCountdownSound, "Explosion_Timer", tempObj, "DLC_Lowrider_Relay_Race_Sounds", TRUE, 20)
	
		PRINTLN("[RCC MISSION][EXPLODE] PROCESS_OBJECTS_TO_EXPLODE_WHEN_OUT_OF_ZONE")
	ENDIF
	
	IF iSniperBallObj != -1
		IF IS_SOUND_ID_VALID(iExplodeCountdownSound)
			IF NOT HAS_SOUND_FINISHED(iExplodeCountdownSound)
				SET_VARIABLE_ON_SOUND(iExplodeCountdownSound, "Time",  TO_FLOAT(iExplodeLocalTimer / (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iSniperBallObj].iExplodeTime * 1000)))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_OBJECTS_TO_EXPLODE_WHEN_OUT_OF_ZONE()
	
	VECTOR vObjCoord
	
	IF NOT IS_BIT_SET(bsHasObjExploded, iSniperBallObj)
		IF DOES_ENTITY_EXIST(oiSniperBallPackage)
			IF iStoredNoWeaponZone != -1
				IF HAS_OBJECT_BEEN_TAKEN_OUT_OF_ZONE(oiSniperBallPackage,iStoredNoWeaponZone)
					IF iExplodeLocalTimer != -1
						IF CHECK_IF_TIME_PASSED_IN_HAS_EXPIRED(iExplodeLocalTimer, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iSniperBallObj].iExplodeTime)
							PRINTLN("[RCC MISSION][EXPLODE] PROCESS_OBJECTS_TO_EXPLODE_WHEN_OUT_OF_ZONE -  iobj: ", iSniperBallObj, " has been damaged. ")
							vObjCoord = GET_ENTITY_COORDS(oiSniperBallPackage,FALSE)
							ADD_EXPLOSION(vObjCoord,EXP_TAG_BARREL)
							SET_BIT(bsHasObjExploded, iSniperBallObj)
						ENDIF
						PLAY_EXPLODE_OUT_OF_ZONE_AUDIO(oiSniperBallPackage)
						IF drawExplosionTimerHud != NULL
							CALL drawExplosionTimerHud()
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION][EXPLODE] PROCESS_OBJECTS_TO_EXPLODE_WHEN_OUT_OF_ZONE - setting explode timer")
						iExplodeLocalTimer = GET_GAME_TIMER() 
					ENDIF
				ELSE
					IF iExplodeLocalTimer != - 1
						PRINTLN("[RCC MISSION][EXPLODE] PROCESS_OBJECTS_TO_EXPLODE_WHEN_OUT_OF_ZONE - resetting timer")
					ENDIF
					iExplodeLocalTimer = -1
					IF IS_SOUND_ID_VALID(iExplodeCountdownSound)
					AND HAS_SOUND_FINISHED(iExplodeCountdownSound)
						STOP_SOUND(iExplodeCountdownSound)
						RELEASE_SOUND_ID(iExplodeCountdownSound)
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLIENT_MANAGE_FRAG_CRATE_PICKUPS(INT iobj)
	
	IF bIsLocalPlayerHost
		INT ipickup
		INT iamount
	    INT iPlacementFlags = 0
		BOOL bGotAllSpawnPoints = TRUE
		
		IF IS_BIT_SET(MC_serverBD.iCrateBrokenBitset, iobj)
			IF NOT IS_BIT_SET(iLocalCrateBrokenBitset, iobj)
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_MAP))
				FOR ipickup = 0 TO (ciMAX_CRATE_PICKUPS-1)
					IF NOT IS_VECTOR_ZERO(MC_serverBD_3.vCratePickupCoords[iobj][ipickup])
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCrateContents = ciCRATE_CONTENTS_CASH
							IF NOT DOES_ENTITY_EXIST(piCratePickup[iobj][ipickup].oiPickup)
								SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
		  						SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
		  						SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ON_OBJECT))
								PRINTLN("[RCC_MISSION] - DESTRUCTIBLE CRATE PICKUP - Creating CASH pickup : ", ipickup," with value : ",iamount)
								piCratePickup[iobj][ipickup] = CREATE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(PICKUP_MONEY_VARIABLE, MC_serverBD_3.vCratePickupCoords[iobj][ipickup] + <<0,0,0.10>>, iPlacementFlags, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCrateValue, DEFAULT, TRUE, TRUE)
								//SET_ENTITY_VISIBLE(piCratePickup[iobj][ipickup], FALSE)
								PRINTLN("[RCC_MISSION] - DESTRUCTIBLE CRATE PICKUP - SET CASH PICKUP INVISIBLE")
								bGotAllSpawnPoints = FALSE
							ENDIF
						ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCrateContents = ciCRATE_CONTENTS_DRUGS
							IF ipickup < g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCrateValue
								IF NOT DOES_ENTITY_EXIST(piCratePickup[iobj][ipickup].oiPickup)
									REQUEST_MODEL(PROP_DRUG_PACKAGE_02)
				               		IF HAS_MODEL_LOADED(PROP_DRUG_PACKAGE_02)
										SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
			  							SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
		  								SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ON_OBJECT))
										PRINTLN("[RCC_MISSION] - DESTRUCTIBLE CRATE PICKUP - Creating DRUGS pickup : ", ipickup," with quantity : ",iamount)
										piCratePickup[iobj][ipickup] = CREATE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(PICKUP_MONEY_MED_BAG, MC_serverBD_3.vCratePickupCoords[iobj][ipickup] + <<0,0,0.10>>, iPlacementFlags, 500, PROP_DRUG_PACKAGE_02, TRUE, TRUE)
										//SET_ENTITY_VISIBLE(piCratePickup[iobj][ipickup], FALSE)
										PRINTLN("[RCC_MISSION] - DESTRUCTIBLE CRATE PICKUP - SET DRUGS PICKUP INVISIBLE")
									ENDIF
									bGotAllSpawnPoints = FALSE
								ENDIF
							ENDIF
						ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCrateContents = ciCRATE_CONTENTS_WEAPON
							IF NOT DOES_ENTITY_EXIST(piCratePickup[iobj][ipickup].oiPickup)
								//SET_FM_PICKUP_ORIENTATION_FLAGS(iPlacementFlags)
								//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
	  							SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
	  							SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
		  						SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ON_OBJECT))
								
								//Make Health/Armour Full
								IF INT_TO_ENUM(PICKUP_TYPE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCrateValue) = PICKUP_ARMOUR_STANDARD
								OR INT_TO_ENUM(PICKUP_TYPE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCrateValue) = PICKUP_HEALTH_STANDARD
									iAmount = 500
								ELSE	//Get Ammo
									IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAmmoValue = 0
										iAmount = 50
									ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAmmoValue = 1
										iAmount = 100
									ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAmmoValue = 2
										iAmount = 200
									ELSE	//Maximum
										GET_MAX_AMMO(LocalPlayerPed, FMMC_GET_WEAPON_TYPE_FROM_PICKUP_TYPE(INT_TO_ENUM(PICKUP_TYPE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCrateValue), TRUE), iAmount)
										
										PRINTLN("[RCC_MISSION] - DESTRUCTIBLE CRATE PICKUP - Maximum Ammo for Weapon: ", GET_WEAPON_NAME(FMMC_GET_WEAPON_TYPE_FROM_PICKUP_TYPE(INT_TO_ENUM(PICKUP_TYPE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCrateValue), TRUE)), " with Ammo: ", iAmount)
									ENDIF
								ENDIF
								
								PRINTLN("[RCC_MISSION] - DESTRUCTIBLE CRATE PICKUP - Creating WEAPON pickup : ", iPickup," with ammo : ", iAmount)
								piCratePickup[iObj][iPickup] = CREATE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(INT_TO_ENUM(PICKUP_TYPE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCrateValue), MC_serverBD_3.vCratePickupCoords[iObj][iPickup] + <<0,0,0.10>>, iPlacementFlags, iAmount, DEFAULT, TRUE, TRUE)
								PRINTLN("PD PICKUP DEBUG - creating pickup g_FMMC_STRUCT_ENTITIES.sPlacedObject[", iObj, "].iCrateValue is ", GET_STRING_FROM_PICKUP_TYPE(INT_TO_ENUM(PICKUP_TYPE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCrateValue)))
								
								//vPickupPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(NET_TO_OBJ(serverBD.DCrateData[i].NetID), <<0, 0.25-(k*0.25), 0.1>>)
								//serverBD.DCrateData[i].Pickup[k] = CREATE_AMBIENT_PICKUP(serverBD.DCrateData[i].ContentsWeapon, vPickupPos, iPlacementFlags, DEFAULT, DEFAULT, TRUE)
								
								//SET_ENTITY_VISIBLE(piCratePickup[iObj][iPickup], FALSE)
								PRINTLN("[RCC_MISSION] - DESTRUCTIBLE CRATE PICKUP - SET WEAPON PICKUP INVISIBLE")
								bGotAllSpawnPoints = FALSE
							ENDIF
						ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCrateContents = ciCRATE_CONTENTS_AMMO
							IF NOT DOES_ENTITY_EXIST(piCratePickup[iobj][ipickup].oiPickup)
								SET_FM_PICKUP_ORIENTATION_FLAGS(iPlacementFlags)
		  						SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ON_OBJECT))
								IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCrateValue = 0
									iamount = 50
								ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCrateValue = 1
									iamount = 100
								ELSE
									iamount = 200
								ENDIF
								PRINTLN("[RCC_MISSION] - DESTRUCTIBLE CRATE PICKUP - Creating AMMO pickup : ", ipickup," with ammo : ",iamount)
								
								piCratePickup[iobj][ipickup] = CREATE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(PICKUP_AMMO_BULLET_MP , MC_serverBD_3.vCratePickupCoords[iobj][ipickup] + <<0,0,0.10>>, iPlacementFlags,iamount, DEFAULT, TRUE, TRUE)
								//SET_ENTITY_VISIBLE(piCratePickup[iobj][ipickup], FALSE)
								PRINTLN("[RCC_MISSION] - DESTRUCTIBLE CRATE PICKUP - SET AMMO PICKUP INVISIBLE")
								bGotAllSpawnPoints = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				IF bGotAllSpawnPoints
					SET_BIT(iLocalCrateBrokenBitset, iobj)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_PARTICLE_FX_OVERRIDE(INT iObj)
	IF NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_PARTICLE_FX_OVERRIDE_COCAINE)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("imp_Prop_ImpExp_BoxCoke_01"))
			REQUEST_NAMED_PTFX_ASSET("scr_ie_svm_technical2")
			IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_ie_svm_technical2")			
				PRINTLN("[PROCESS_PARTICLE_FX_OVERRIDE] Not loaded scr_ie_svm_technical2 effect asset yet")
			ELSE
				PRINTLN("[PROCESS_PARTICLE_FX_OVERRIDE] Loaded scr_ie_svm_technical2 effect asset")
				PRINTLN("[PROCESS_PARTICLE_FX_OVERRIDE] SET_PARTICLE_FX_OVERRIDE \"ent_dst_polystyrene\" with \"scr_dst_cocaine\"")
				SET_PARTICLE_FX_OVERRIDE("ent_dst_polystyrene", "scr_dst_cocaine")
				SET_BIT(iLocalBoolCheck20, LBOOL20_PARTICLE_FX_OVERRIDE_COCAINE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_MISSION_FUSEBOX_OBJECT(FMMC_OBJECT_STATE& sObjState)

	IF NOT IS_THIS_MODEL_A_FUSEBOX(sObjState.mn)
		EXIT
	ENDIF
	
	VECTOR vZoneCoords
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjLinkedZone > -1
		vZoneCoords = GET_ZONE_RUNTIME_CENTRE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjLinkedZone)
	ENDIF
	
	IF IS_BIT_SET(sObjTaserVars.iHitByTaserBS, sObjState.iIndex)
	OR NOT sObjState.bObjAlive
	
		IF IS_BIT_SET(sObjTaserVars.iReactedToTaserBS, sObjState.iIndex)
			// Already reacted!
			EXIT
		ENDIF
	
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjLinkedZone > -1
			IF NOT IS_BIT_SET(iMetalDetectorZoneDisabledBitset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjLinkedZone)
				PRINTLN("[MetalDetector][Taser] Setting iMetalDetectorZoneDisabledBitset / ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjLinkedZone)
				SET_BIT(iMetalDetectorZoneDisabledBitset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjLinkedZone)
				
				PLAY_SOUND_FROM_COORD(-1, "Metal_Detector_Offline", vZoneCoords, "dlc_ch_heist_finale_security_alarms_sounds", TRUE, 800)
				
				IF NOT sObjState.bObjAlive
					PLAY_SOUND_FROM_COORD(-1, "Security_Box_Offline_Gun", GET_FMMC_OBJECT_COORDS(sObjState), "dlc_ch_heist_finale_security_alarms_sounds", TRUE, 800)
				ENDIF
			ENDIF
		ENDIF
		
		SET_BIT(sObjTaserVars.iReactedToTaserBS, sObjState.iIndex)
	ELSE
		IF IS_BIT_SET(sObjTaserVars.iReactedToTaserBS, sObjState.iIndex)
			IF IS_BIT_SET(iMetalDetectorZoneDisabledBitset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjLinkedZone)
				PRINTLN("[MetalDetector][Taser] Clearing iMetalDetectorZoneDisabledBitset / ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjLinkedZone)
				CLEAR_BIT(iMetalDetectorZoneDisabledBitset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjLinkedZone)
				
				PLAY_SOUND_FROM_COORD(-1, "Metal_Detector_Online", vZoneCoords, "dlc_ch_heist_finale_security_alarms_sounds", TRUE, 800)
			ENDIF
			
			CLEAR_BIT(sObjTaserVars.iReactedToTaserBS, sObjState.iIndex)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_OBJECT_INVENTORIES(FMMC_OBJECT_STATE& sObjState)
	
	IF NOT sObjState.bObjAlive
		EXIT
	ENDIF
	
	//cleanup if they aren't holding an inventory object anymore
	IF sObjState.iIndex = iInventoryObjectIndex
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectInventory > 0
	AND MC_serverBD.iObjCarrier[sObjState.iIndex] != iLocalPart
		CLEANUP_OBJECT_INVENTORIES()
	ENDIF
	
	//give them the inventory if they are holding it
	IF MC_serverBD.iObjCarrier[sObjState.iIndex] = iLocalPart
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectInventory > 0
	AND NOT IS_BIT_SET(iObjDeliveredBitset, sObjState.iIndex)
		IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_HAS_OBJECT_INVENTORY)
			PRINTLN("PROCESS_OBJECT_INVENTORIES - Player should be given an Object Inventory as they are holding Obj ", sObjState.iIndex)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectInventory = 1
				GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_MID_MISSION, WEAPONINHAND_LASTWEAPON_BOTH)
				PRINTLN("PROCESS_OBJECT_INVENTORIES - Giving Player Mid-Mission Inventory 1!")
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectInventory = 2
				GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_MID_MISSION_2, WEAPONINHAND_LASTWEAPON_BOTH)
				PRINTLN("PROCESS_OBJECT_INVENTORIES - Giving Player Mid-Mission Inventory 2!")
			ENDIF
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_HAS_OBJECT_INVENTORY)
			iInventoryObjectIndex = sObjState.iIndex
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_OBJ_NEAR_BE_SET_TO_CARRIED_OBJECT()
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iRuleBitsetNine[GET_LOCAL_PLAYER_CURRENT_RULE()], ciBS_RULE9_USE_GRANULAR_CAPTURE)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_SETTING_OBJ_NEAR(FMMC_OBJECT_STATE& sObjState)
	
	IF NOT sObjState.bObjExists
		EXIT
	ENDIF
	
	// Only check this if this object is an objective right now
	IF MC_serverBD_4.iObjPriority[sObjState.iIndex][GET_LOCAL_PLAYER_TEAM()] != GET_LOCAL_PLAYER_CURRENT_RULE()
		EXIT
	ENDIF

	// Set it to this object if we're carrying it
	IF SHOULD_OBJ_NEAR_BE_SET_TO_CARRIED_OBJECT()
		IF IS_ENTITY_ATTACHED_TO_ENTITY(sObjState.objIndex, LocalPlayerPed)
			PRINTLN("[Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] PROCESS_SETTING_OBJ_NEAR | Setting it to object ", sObjState.iIndex, " because we're carrying it")
			SET_OBJ_NEAR(sObjState.iIndex)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_OBJECT_TRANSFORMATIONS(FMMC_OBJECT_STATE& sObjState)
	
	IF NOT sObjState.bObjAlive
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
		EXIT
	ENDIF
	
	IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTRANSFORM_TO_JUGGERNAUT_ON_SUDDEN_DEATH)
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTRANSFORM_TO_BEAST_ON_SUDDEN_DEATH)
			EXIT
		ENDIF
	ENDIF
	
	ASSERTLN("FMMC2020 - PROCESS_OBJECT_TRANSFORMATIONS - Object transformations need refactored to properly split between localplayer and objects")
//	IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_CLEANUP_OUTFIT_ON_RESPAWN)
//	AND bLocalPlayerPedOk
//		IF NOT APPLY_OUTFIT_TO_LOCAL_PLAYER(INT_TO_ENUM(MP_OUTFIT_ENUM, GET_LOCAL_DEFAULT_OUTFIT(GET_LOCAL_PLAYER_TEAM(TRUE))))
//			CLEAR_BIT(iLocalBoolCheck23, LBOOL23_CLEANUP_OUTFIT_ON_RESPAWN)
//		ENDIF
//	ENDIF
//	IF (iObj = iTransformationObjectIndex
//	AND NOT (IS_BIT_SET(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj) OR MC_playerBD[iLocalPart].iObjNear = iObj))
//	OR (iObj = iTransformationObjectIndex
//	AND (IS_BIT_SET(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj) OR MC_playerBD[iLocalPart].iObjNear = iObj)
//	AND NOT bLocalPlayerPedOk)
//		IF SHOULD_CLEANUP_OBJECT_TRANSFORMATION_NOW()
//			CLEANUP_OBJECT_TRANSFORMATIONS()
//			iTransformationObjectIndex = -1
//		ENDIF
//	ELIF (IS_BIT_SET(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj) OR MC_playerBD[iLocalPart].iObjNear = iObj)
//	AND NOT IS_BIT_SET(iObjDeliveredBitset, iObj)
//	AND bLocalPlayerPedOk
//		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_TransformJuggernaut)
//			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_JUGGERNAUT)
//				IF TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT(DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iJuggernautHealth)
//					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCustomShard > 0
//						BROADCAST_FMMC_DISPLAY_CUSTOM_OBJECT_PICKUP_SHARD(LocalPlayer, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCustomShard, GET_LOCAL_PLAYER_TEAM())
//					ENDIF
//					iTransformationObjectIndex = iObj
//				ENDIF
//			ENDIF
//		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_TransformBeast)
//			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_BEAST)
//				IF TRANSFORM_LOCAL_PLAYER_INTO_BEAST()
//					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCustomShard > 0
//						BROADCAST_FMMC_DISPLAY_CUSTOM_OBJECT_PICKUP_SHARD(LocalPlayer, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCustomShard, GET_LOCAL_PLAYER_TEAM())
//					ENDIF
//					iTransformationObjectIndex = iObj
//				ENDIF
//			ENDIF
//		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_UpgradeVeh)
//			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_UPGRADED_WEP_VEHICLE)
//				UPGRADE_WEAPONISED_VEHICLE()
//				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCustomShard > 0
//					BROADCAST_FMMC_DISPLAY_CUSTOM_OBJECT_PICKUP_SHARD(LocalPlayer, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCustomShard, GET_LOCAL_PLAYER_TEAM())
//				ENDIF
//				iTransformationObjectIndex = iObj
//			ENDIF
//		ENDIF
//	ENDIF
ENDPROC


				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Blips
// ##### Description: Blip processing functions
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC REMOVE_OBJECT_TAG_BLIP(INT iObj, OBJECT_INDEX oiObject)
	IF SHOULD_SHOW_TAGGED_TARGET()
		INT iTagged
		FOR iTagged = 0 TO FMMC_MAX_TAGGED_ENTITIES-1
			IF MC_serverBD_3.iTaggedEntityType[iTagged] = ENUM_TO_INT(GET_ENTITY_TYPE(oiObject))
				IF MC_serverBD_3.iTaggedEntityIndex[iTagged] = iObj
					IF DOES_BLIP_EXIST(biTaggedEntity[iTagged])
						REMOVE_BLIP(biTaggedEntity[iTagged])
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		REMOVE_TAGGED_BLIP(4, iobj)
	ENDIF
ENDPROC

PROC REMOVE_BLIP_FOR_OBJECTIVE_UPDATE(INT iObj)
	IF iLocalCarrierPart[iObj] != MC_serverBD.iObjCarrier[iObj]
	OR iLocalPackageHolding[iObj] != GET_TEAM_HOLDING_PACKAGE(iObj)
		REMOVE_OBJECT_BLIP(iObj)
		iLocalPackageHolding[iObj] = GET_TEAM_HOLDING_PACKAGE(iObj)
		iLocalCarrierPart[iObj] = MC_serverBD.iObjCarrier[iObj] 
		REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_OBJECT, FALSE #IF IS_DEBUG_BUILD , "REMOVE_BLIP_FOR_OBJECTIVE_UPDATE - iLocalCarrierPart changed" #ENDIF )
	ENDIF
ENDPROC

PROC START_OBJECT_BLIP_FLASHES(BLIP_INDEX &biObjectBlip, FMMC_OBJECT_STATE &sObjState)
	IF MC_serverBD.iObjCarrier[sObjState.iIndex] > -1
	AND (g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].sObjBlipStruct.iBlipStyle = FMMC_OBJECTBLIP_CTF
	OR (MC_serverBD_4.iObjRule[sObjState.iIndex][MC_playerBD[iPartToUse].iteam] != FMMC_OBJECTIVE_LOGIC_KILL
	AND MC_serverBD_4.iObjRule[sObjState.iIndex][MC_playerBD[iPartToUse].iteam] != FMMC_OBJECTIVE_LOGIC_MINIGAME))
		IF MC_serverBD.iObjCarrier[sObjState.iIndex] != iPartToUse
		AND GET_ENTITY_MODEL(sObjState.objIndex) != PROP_CS_DUFFEL_01
			SET_BLIP_FLASHES(biObjectBlip, TRUE)
			RESYNC_FLASHING_OBJECT_BLIPS()
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_OBJECT_BE_BLIPPED(FMMC_OBJECT_STATE &sObjState, BLIP_RUNTIME_VARS_STRUCT &sBlipRuntimeVars)

	INT iObj = sObjState.iIndex

	INT iTeam = GET_LOCAL_PLAYER_TEAM(TRUE)
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam] 
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct.iBlipStyle = FMMC_OBJECTBLIP_NONE
		PRINTLN("[Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] SHOULD_OBJECT_BE_BLIPPED - Style set to none")
		RETURN FALSE
	ENDIF
	
	IF SHOULD_ENTITY_BLIP_BE_FORCED_OFF(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_ENTITY_BLIP_BE_FORCED_ON(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct, sObjState.objIndex)
		RETURN TRUE
	ENDIF
	
	OBJECT_INDEX tempObj
	IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iObj))
		tempObj = NET_TO_OBJ(GET_OBJECT_NET_ID(iObj))
	ENDIF
	
	IF NOT SHOULD_ENTITY_BE_BLIPPED(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct, sBlipRuntimeVars, tempObj, FALSE)
		PRINTLN("[Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] SHOULD_OBJECT_BE_BLIPPED - SHOULD_ENTITY_BE_BLIPPED FALSE")
		RETURN FALSE
	ENDIF
	
	IF IS_BLIP_SET_AS_HIDDEN(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct)
		PRINTLN("[Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] SHOULD_OBJECT_BE_BLIPPED - IS_BLIP_SET_AS_HIDDEN")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_UnblipAfterMinigame)
		IF IS_BIT_SET(MC_serverBD_4.iObjectDoNotBlipBS, iObj)
			PRINTLN("[Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] SHOULD_OBJECT_BE_BLIPPED - Unblip after minigame")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_HideBlipInOrbitalCannon)
	AND IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PlayerToUse)].iMissionModeBit, ciMISSION_USING_ORBITAL_CANNON)
		PRINTLN("[Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] SHOULD_OBJECT_BE_BLIPPED - Using orbital cannon")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iMinigameObjBlipBlockerBS, sObjState.iIndex)
		PRINTLN("[Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] SHOULD_OBJECT_BE_BLIPPED - Blocked by minigame requirement")
		RETURN FALSE
	ENDIF
	
	IF iRule < FMMC_MAX_RULES
		IF NOT IS_BIT_SET(iIgnoreObjectBlipRangeBS, iObj)
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_OVERRIDE_BLIP_RANGE)
			AND NOT WAS_PLAYER_PREVIOUSLY_IN_RANGE_OF_BLIP(sBlipRuntimeVars)
				IF NOT IS_PLAYER_WITHIN_ENTITY_BLIP_RANGE(sObjState.objIndex, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct)
					PRINTLN("[Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] SHOULD_OBJECT_BE_BLIPPED - Blip range (2)")
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct.fBlipRange = -1.0
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_OVERRIDE_BLIP_RANGE)
				PRINTLN("[Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] SHOULD_OBJECT_BE_BLIPPED - Not in range to blip")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT WAS_PLAYER_PREVIOUSLY_IN_RANGE_OF_BLIP(sBlipRuntimeVars)
		IF NOT IS_ENTITY_WITHIN_HEIGHT_DIFFERENCE_RANGE_FOR_BLIP(sObjState.objIndex, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct)
			PRINTLN("[Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] SHOULD_OBJECT_BE_BLIPPED - Blip height difference")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_SUDDEN_DEATH_OBJECT_DURING_SUDDEN_DEATH(iObj)
		PRINTLN("[Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] SHOULD_OBJECT_BE_BLIPPED - Hiding during sudden death")
		RETURN FALSE
	ENDIF
		
	//Remove and package blips when we go into this sudden death mode
	IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
	AND IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)
		PRINTLN("[Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] SHOULD_OBJECT_BE_BLIPPED - IN SUDDEN DEATH, SHOULD NOT BLIP PACKAGE")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

PROC PROCESS_OBJ_CAPTURE_BLIP(BLIP_INDEX &biObjectBlip, FMMC_OBJECT_STATE &sObjState)
	
	INT iObj = sObjState.iIndex
	
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
	OR !sObjState.bObjAlive
	OR MC_serverBD_4.iObjPriority[sObjState.iIndex][iTeam] > iRule
	OR MC_serverBD_4.iObjRule[sObjState.iIndex][iTeam] != FMMC_OBJECTIVE_LOGIC_CAPTURE
		IF DOES_BLIP_EXIST(biObjCaptureRangeBlip[iObj])
			REMOVE_BLIP(biObjCaptureRangeBlip[iObj])
		ENDIF
		EXIT
	ENDIF
	
	IF VDIST2_2D_PARAM(vLocalPlayerPosition, GET_FMMC_OBJECT_COORDS(sObjState), NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetFive, cibsOBJ5_CheckZForCapture)) > POW(TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCaptureRange * 2), 2.0)
		IF DOES_BLIP_EXIST(biObjCaptureRangeBlip[iObj])
			REMOVE_BLIP(biObjCaptureRangeBlip[iObj])
		ENDIF
		EXIT
	ENDIF
	
	IF DOES_BLIP_EXIST(biObjectBlip)
		IF NOT DOES_BLIP_EXIST(biObjCaptureRangeBlip[iObj])
			biObjCaptureRangeBlip[iObj] = ADD_BLIP_FOR_RADIUS(GET_FMMC_OBJECT_COORDS(sObjState), TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCaptureRange))
			SET_BLIP_COLOUR(biObjCaptureRangeBlip[iObj], BLIP_COLOUR_YELLOW)
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(biObjCaptureRangeBlip[iObj])
			REMOVE_BLIP(biObjCaptureRangeBlip[iObj])
		ENDIF
	ENDIF
	
	IF NOT DOES_BLIP_EXIST(biObjCaptureRangeBlip[iObj])
		EXIT
	ENDIF
	
	SET_BLIP_COORDS(biObjCaptureRangeBlip[iObj], GET_FMMC_OBJECT_COORDS(sObjState))
	
	IF VDIST2_2D_PARAM(vLocalPlayerPosition, GET_FMMC_OBJECT_COORDS(sObjState), NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetFive, cibsOBJ5_CheckZForCapture)) <= POW(TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCaptureRange), 2.0)
		SET_BLIP_ALPHA(biObjCaptureRangeBlip[iObj], 50)
	ELSE
		SET_BLIP_ALPHA(biObjCaptureRangeBlip[iObj], 255)
	ENDIF
	
ENDPROC

PROC PROCESS_OBJECT_BLIP(BLIP_INDEX &biObjectBlip, FMMC_OBJECT_STATE &sObjState)
	
	INT iObj = sObjState.iIndex

	IF !sObjState.bObjExists
		REMOVE_OBJECT_BLIP(iObj)
		EXIT
	ENDIF
	
	IF !sObjState.bObjAlive
		REMOVE_OBJECT_BLIP(iObj)
		REMOVE_OBJECT_TAG_BLIP(iObj, sObjState.objIndex)
		EXIT
	ENDIF
	
	IF DOES_BLIP_EXIST(biObjectBlip) 
		IF NOT DOES_BLIP_EXIST(GET_BLIP_FROM_ENTITY(sObjState.objIndex))
			REMOVE_BLIP(biObjectBlip)
			PRINTLN("[Objects][Object ", iObj, "] PROCESS_OBJECT_BLIP - removing Object blip - was on old Object")
		ENDIF
	ENDIF
	
	IF NOT SHOULD_OBJECT_BE_BLIPPED(sObjState, sMissionObjectsLocalVars[iObj].sBlipRuntimeVars)
		REMOVE_OBJECT_BLIP(iObj)
		EXIT
	ENDIF
	
	INT iTeam = GET_LOCAL_PLAYER_TEAM(TRUE)
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF NOT IS_OBJECTIVE_BLOCKED()
	OR IS_THIS_OBJECT_COLLECT_OBJECT_OBJECTIVE(iObj)
	OR IS_CURRENT_OBJECTIVE_LOSE_GANG_CHASE()
	OR IS_CURRENT_OBJECTIVE_LOSE_WANTED()
		
		REMOVE_BLIP_FOR_OBJECTIVE_UPDATE(iObj)
		
		IF (MC_serverBD_4.iObjPriority[iObj][iTeam] <= iRule
		AND MC_serverBD_4.iObjPriority[iObj][iTeam] < FMMC_MAX_RULES)
		OR IS_THIS_OBJECT_COLLECT_OBJECT_OBJECTIVE(iObj)
			
			IF NOT DOES_BLIP_EXIST(biObjectBlip)
					
				CREATE_OBJECT_BLIP(sObjState)
				
				START_OBJECT_BLIP_FLASHES(biObjectBlip, sObjState)
				
			ENDIF
			
			IF DOES_BLIP_EXIST(biObjectBlip)
				INT iTeamHolding = GET_TEAM_HOLDING_PACKAGE(iObj)
				IF iTeamHolding > -1
					IF NOT IS_TEAM_ACTIVE(iTeamHolding)
						CLEAR_SECONDARY_COLOUR_FROM_BLIP(biObjectBlip)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_FadeBlipOnHack)

					IF MC_serverBD.iObjHackPart[iObj] = -1
						PRINTLN("[Objects][Object ", iObj, "][ObjectBlips] Setting obj blip alpha back to 1")
						SET_BLIP_ALPHA(biObjectBlip, 255)
					ELSE
						PRINTLN("[Objects][Object ", iObj, "][ObjectBlips] Lowering obj blip alpha because it's currently being hacked")
						SET_BLIP_ALPHA(biObjectBlip, 25)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_HAVE_BLIP_OVERRIDES(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct)
				IF NOT DOES_BLIP_EXIST(biObjectBlip)
					CREATE_OBJECT_BLIP(sObjState)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT DOES_BLIP_EXIST(biObjectBlip)
		EXIT
	ENDIF

	//Blip Exists from this point on!
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitset, cibsOBJ_GPSToLocation)
		IF IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_ATTACHED_TO_SOMETHING)
			IF DOES_BLIP_HAVE_GPS_ROUTE(biObjectBlip)
				SET_BLIP_ROUTE(biObjectBlip, FALSE)
			ENDIF
		ELSE
			IF NOT DOES_BLIP_HAVE_GPS_ROUTE(biObjectBlip)
				SET_BLIP_ROUTE(biObjectBlip, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	PROCESS_ENTITY_BLIP(biObjectBlip, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct, GET_FMMC_OBJECT_COORDS(sObjState), g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAggroIndexBS_Entity_Obj)
	
	IF MC_serverBD_4.iObjPriority[iObj][GET_LOCAL_PLAYER_TEAM(TRUE)] > MC_serverBD_4.iCurrentHighestPriority[GET_LOCAL_PLAYER_TEAM(TRUE)]
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct.iBlipStyle = FMMC_OBJECTBLIP_STANDARD						
			IF NOT DOES_ENTITY_HAVE_BLIP_OVERRIDES(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct)
			AND NOT IS_THIS_OBJECT_COLLECT_OBJECT_OBJECTIVE(sObjState.iIndex)
				REMOVE_OBJECT_BLIP(iObj)
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Process Object Every-Frame --------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Every frame loop for objects to process time sensitive things or that need to be processed every frame -----------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC FILL_DRONE_STATE_FROM_OBJ_STATE(FMMC_OBJECT_STATE &sObjState, FMMC_DRONE_STATE &sDroneState)
		
	sDroneState.oiIndex = sObjState.objIndex
	sDroneState.niIndex = sObjState.niIndex
	
	IF sObjState.bObjExists
		sDroneState.vPos = GET_ENTITY_COORDS(sObjState.objIndex, FALSE)
		sDroneState.vRot = GET_ENTITY_ROTATION(sObjState.objIndex)
		sDroneState.fHead = GET_ENTITY_HEADING(sObjState.objIndex)
	ENDIF
	
	sDroneState.bExists = sObjState.bObjExists
	sDroneState.bAlive = sObjState.bObjAlive
	sDroneState.bBroken = sObjState.bObjBroken OR IS_BIT_SET(sObjTaserVars.iHitByTaserBS, sObjState.iIndex)
	sDroneState.bHaveControl = sObjState.bHaveControlOfObj	
	sDroneState.bActionActivated = IS_BIT_SET(MC_ServerBD.iObjectActionActivatedBS, sObjState.iIndex)
	sDroneState.bHealthThreshActivated = IS_BIT_SET(MC_ServerBD.iObjectHealthThreshActivatedBS, sObjState.iIndex)
	
	sDroneState.mn = sObjState.mn
	
	sDroneState.iEntityID = sObjState.iIndex
	sDroneState.iEntityType = CREATION_TYPE_OBJECTS
			
ENDPROC

PROC PROCESS_FMMC_OBJECT_DRONE_SYSTEMS(FMMC_OBJECT_STATE &sObjState, FMMC_DRONE_DATA &eDroneData)
	
	IF NOT SHOULD_DRONE_SYSTEMS_BE_ACTIVE(eDroneData)
		EXIT
	ENDIF
	
	IF NOT sObjState.bObjExists
		CLEAR_DRONE_INDEX(sObjState.iIndex, CREATION_TYPE_OBJECTS)
		EXIT
	ENDIF
	
	FMMC_DRONE_STATE sDroneState
	FILL_DRONE_STATE_FROM_OBJ_STATE(sObjState, sDroneState)
	
	PROCESS_FMMC_DRONE_SYSTEMS(eDroneData, sDroneState)
	
	// Swapping the Droen with the destroyed version of the prop requires us to reassign the object index, but in order for the Drone system to remain as non-entity type specific as possible 
	// we don't want to pass sObjState into PROCESS_FMMC_DRONE_SYSTEMS. This is a special case in order to prevent a match with sObjState.objIndex being NULL and sObjState.bExists being TRUE.
	// FILL_FMMC_OBJECT_STATE_STRUCT(sObjState, sObjState.iIndex, TRUE) does not work as it exits out if the iIndex is equal already regardless of bResetIfDifferent.
	IF sObjState.objIndex != sDroneState.oiIndex
		sObjState.objIndex = sDroneState.oiIndex
	ENDIF
	
ENDPROC

PROC PROCESS_OBJ_SELF_DESTRUCT_SEQUENCE(INT iObj)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sSelfDestructSettings.iRuleStart = -1
		EXIT
	ENDIF
	
	OBJECT_INDEX tempObj
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iObj))		
		IF HAS_NET_TIMER_STARTED(sObjSelfDestruct[iObj].tdTimer)
			CLEAN_UP_SELF_DESTRUCT_ENTITY(sObjSelfDestruct[iObj])
		ENDIF
		
		EXIT
	ELSE		
		tempObj = NET_TO_OBJ(GET_OBJECT_NET_ID(iObj))
	ENDIF
	
	BOOL bProcess			= SHOULD_ENTITY_PROCESS_SELF_DESTRUCT_SEQUENCE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sSelfDestructSettings, sObjSelfDestruct[iObj], GET_ENTITY_HEALTH_PERCENTAGE(tempObj))
	BOOL bProcessFizzle		= SHOULD_ENTITY_PROCESS_FIZZLE_SELF_DESTRUCT_SEQUENCE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sSelfDestructSettings, sObjSelfDestruct[iObj])
	
	#IF IS_DEBUG_BUILD
		IF bProcess
		OR bProcessFizzle
			IF DOES_ENTITY_EXIST(tempObj)
				PRINTLN("[LM][SELF_DESTRUCT] - Processing the Self Destruction of iObj: ", iObj, " SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(tempObj), " --------------------")
			ELSE
				PRINTLN("[LM][SELF_DESTRUCT] - Processing the Self Destruction of iObj: ", iObj, " Entity Index Does Not Exist")
			ENDIF
		ENDIF
	#ENDIF
	
	IF bProcessFizzle
		PROCESS_ENTITY_FIZZLE_SELF_DESTRUCT_SEQUENCE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sSelfDestructSettings, sObjSelfDestruct[iObj])
	ELIF bProcess
		PROCESS_ENTITY_SELF_DESTRUCT_SEQUENCE(tempObj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sSelfDestructSettings, sObjSelfDestruct[iObj])
	ENDIF
ENDPROC

PROC EXPLODE_SEA_MINE(FMMC_OBJECT_STATE& sObjState, VECTOR vExplosionCoords)
	
	IF sObjState.bHaveControlOfObj		
		IF NOT IS_BIT_SET(iSeaMine_HasExploded, sObjState.iIndex)
			SET_ENTITY_HEALTH(sObjState.objIndex, 1)
			ADD_OWNED_EXPLOSION(LocalPlayerPed, vExplosionCoords, EXP_TAG_MINE_UNDERWATER, 8)
			
			//Makes the mine invisible right away just so it looks a bit slicker rather than disappearing briefly AFTER the explosion
			SET_ENTITY_VISIBLE(sObjState.objIndex, FALSE)
			
			PRINTLN("[Objects] EXPLODE_SEA_MINE | Exploding object ", sObjState.iIndex)
			SET_BIT(iSeaMine_HasExploded, sObjState.iIndex)
		ELSE
			PRINTLN("[Objects] EXPLODE_SEA_MINE | Requesting control of object ", sObjState.iIndex)
		ENDIF	
	ELSE
		NETWORK_REQUEST_CONTROL_OF_ENTITY(sObjState.objIndex)
		PRINTLN("[Objects] EXPLODE_SEA_MINE | Requesting control of object ", sObjState.iIndex)
	ENDIF
ENDPROC

PROC PROCESS_SEA_MINE_BEHAVIOUR(FMMC_OBJECT_STATE& sObjState)

	IF NOT sObjState.bObjAlive
		EXIT
	ENDIF
	
	IF NOT IS_MODEL_A_SEA_MINE(sObjState.mn)
		// Not a sea-mine! Exit now
		EXIT
	ENDIF
	
	MODEL_NAMES mnNoChain = INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Mine_01a"))
	MODEL_NAMES mnShortChain = INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Mine_02a"))
	MODEL_NAMES mnLongChain = INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Mine_03a"))
	
	VECTOR vExplosionCoords = GET_FMMC_OBJECT_COORDS(sObjState)
	FLOAT fExplosionOffset = 0
	FLOAT fExplosionDistance = POW(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].fSeaMineExplodeRadius, 2)
	
	IF sObjState.mn = mnLongChain
		fExplosionOffset = 0
	ELIF sObjState.mn = mnShortChain
		fExplosionOffset = 0
	ELIF sObjState.mn = mnNoChain
		fExplosionOffset = 1
	ENDIF
	
	VECTOR vOffsetVector = GET_ENTITY_UP_VECTOR(sObjState.objIndex) * fExplosionOffset
	vExplosionCoords += vOffsetVector
	
	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
		DRAW_DEBUG_SPHERE(vExplosionCoords, fExplosionDistance, 255, 0, 0, 100)
	ENDIF
	#ENDIF
	
	IF VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), vExplosionCoords) < fExplosionDistance
		EXPLODE_SEA_MINE(sObjState, vExplosionCoords)
	ENDIF
	
ENDPROC

PROC PROCESS_OBJECT_TEAM_BASED_CONTROL_CLIENT(FMMC_OBJECT_STATE& sObjState)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
	OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM(TRUE)].iRuleBitsetNine[MC_serverBD_4.iObjPriority[sObjState.iIndex][GET_LOCAL_PLAYER_TEAM(TRUE)]], ciBS_RULE9_USE_GRANULAR_CAPTURE)
		EXIT
	ENDIF
	
	INT iTeamO
	FOR iTeamO = 0 TO MC_ServerBD.iNumberOfTeams - 1					
		IF IS_TEAM_ACTIVE(iTeamO)
			IF HAS_NET_TIMER_STARTED(MC_serverBD.tdControlObjTeamTimer[iTeamO])										
				PRINTLN("[LM][CONTROL OBJECT] - iTeamO: ", iTeamO, " Timer has started.")
				
				IF iTeamO = MC_serverBD.iObjTeamCarrier
				AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_CONTROL_TIME_STOPPED_T0 + iTeamO)
					PRINTLN("[LM][CONTROL OBJECT] - iTeamO: ", iTeamO, " Is the carrier of the object. Assigning Cached Timer var.")
					iTimeObjectTeamControlBarCached[iTeamO] = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlObjTeamTimer[iTeamO])
				ELSE
					iTimeObjectTeamControlBarCached[iTeamO] = MC_serverBD.iTimeObjectTeamControlBarCache[iTeamO]
				ENDIF
			ELSE
				PRINTLN("[LM][CONTROL OBJECT] - iTeamO: ", iTeamO, " Timer has not started.")
				iTimeObjectTeamControlBarCached[iTeamO] = 0
			ENDIF
													
			PRINTLN("[LM][CONTROL OBJECT] - iTeamO: ", iTeamO, " iTime: ", iTimeObjectTeamControlBarCached[iTeamO])
			PRINTLN("[LM][CONTROL OBJECT] - MAX: ", MC_serverBD.iObjTakeoverTime[iTeamO][MC_serverBD_4.iObjPriority[sObjState.iIndex][GET_LOCAL_PLAYER_TEAM(TRUE)]])
		ENDIF
	ENDFOR
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Object Continuity
// ##### Description: Functions for processing object continuity
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_INITIAL_OBJECT_CONTINUITY()

	IF NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitset, PBBOOL_CLIENT_STATE_INITIALISED)
		EXIT
	ENDIF	
	
	IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_OBJECT_CONTINUITY_INIT_DONE)
		EXIT
	ENDIF
			
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ObjectCarrierTracking)								
		SET_BIT(iLocalBoolCheck8, LBOOL8_OBJECT_CONTINUITY_INIT_DONE)		
		EXIT
	ENDIF
	
	IF sMissionLocalContinuityVars.iObjectHeld = -1
		SET_BIT(iLocalBoolCheck8, LBOOL8_OBJECT_CONTINUITY_INIT_DONE)		
		EXIT
	ENDIF
	
	IF iContinuityLastHeldObjectIndex = -1
		INT i
		FOR i = 0 TO MC_serverBD.iNumObjCreated - 1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iContinuityId = sMissionLocalContinuityVars.iObjectHeld
				iContinuityLastHeldObjectIndex = i
				BREAKLOOP
			ENDIF
		ENDFOR
		IF iContinuityLastHeldObjectIndex = -1
			PRINTLN("[CONTINUITY][Objects] PROCESS_INITIAL_OBJECT_CONTINUITY | No matching object!")
			SET_BIT(iLocalBoolCheck8, LBOOL8_OBJECT_CONTINUITY_INIT_DONE)		
			EXIT
		ENDIF
	ENDIF
	
	NETWORK_INDEX niObj = MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(iContinuityLastHeldObjectIndex)]
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niObj)
		EXIT
	ENDIF
	
	OBJECT_INDEX oiObj = NET_TO_OBJ(niObj)
	
	IF NOT IS_ENTITY_ALIVE(oiObj)
		SET_BIT(iLocalBoolCheck8, LBOOL8_OBJECT_CONTINUITY_INIT_DONE)		
		EXIT
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(oiObj)
		PRINTLN("[CONTINUITY][Objects][Object ", iContinuityLastHeldObjectIndex, "] PROCESS_INITIAL_OBJECT_CONTINUITY | Requesting control of object ", iContinuityLastHeldObjectIndex)
		NETWORK_REQUEST_CONTROL_OF_ENTITY(oiObj)
		EXIT
	ENDIF
	
	PRINTLN("[CONTINUITY][Objects][Object ", iContinuityLastHeldObjectIndex, "] PROCESS_INITIAL_OBJECT_CONTINUITY | Attaching object ", iContinuityLastHeldObjectIndex, " to player.")
	
	ATTACH_PORTABLE_PICKUP_TO_PED(oiObj, LocalPlayerPed)
	
	SET_BIT(iLocalBoolCheck8, LBOOL8_OBJECT_CONTINUITY_INIT_DONE)		
	
ENDPROC	


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Object HUD and UI
// ##### Description: Functions relating to objects HUD systems
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_OBJECT_MARKERS(FMMC_OBJECT_STATE &sObjState)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetFour, cibsOBJ4_ShowObjectMarkerAboveObject)	
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iPlacedMarkerIndex = -1
		EXIT
	ENDIF
	
	IF NOT sObjState.bObjExists
	OR NOT sObjState.bObjAlive
		EXIT
	ENDIF
		
	IF NOT IS_ENTITY_VISIBLE(sObjState.objIndex)	
		EXIT
	ENDIF
	
	INT iIndex = g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iPlacedMarkerIndex
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetFour, cibsOBJ4_ShowObjectMarkerAboveObject)
		iIndex = 0
	ENDIF
	
	PROCESS_PLACED_MARKER(g_FMMC_STRUCT_ENTITIES.sPlacedMarker[iIndex], GET_FMMC_OBJECT_COORDS(sObjState), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetFour, cibsOBJ4_ShowObjectMarkerAboveObject))
	
ENDPROC

PROC PROCESS_OBJECT_CAPTURE_METER(FMMC_OBJECT_STATE& sObjState, INT& iTeam, INT& iRule)

	IF MC_serverBD_4.iObjRule[sObjState.iIndex][iTeam] != FMMC_OBJECTIVE_LOGIC_CAPTURE
	OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_USE_GRANULAR_CAPTURE)
		EXIT
	ENDIF
	
	IF MC_serverBD.iObjCarrier[sObjState.iIndex] != iPartToUse
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
		// We're not carrying anything
		EXIT
	ENDIF
	
	// This is the string that will get displayed on the Control Object HUD
	STRING strControlHUDText = "CON_TIME" // "CONTROL"
	INT iObjectPriority = MC_serverBD_4.iObjPriority[sObjState.iIndex][iTeam]
				
	// Which can get overriden by the word SCORE if required
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_USE_SCORE_TEXT)
		strControlHUDText = "FMMC_CPSCR" // "SCORE"
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
		IF MC_ServerBD.iNumberOfTeams > 3
			HIDE_BOTTOM_RIGHT_CODE_UI()
		ENDIF
		
		INT iTeamO
		FOR iTeamO = 0 TO MC_ServerBD.iNumberOfTeams - 1					
			IF IS_TEAM_ACTIVE(iTeamO)
				IF bPlayerToUseOK
					IF NOT IS_SPECTATOR_HUD_HIDDEN()
					AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_HIDE_HUD)	
					AND NOT g_bMissionEnding
					AND NOT (ARE_WE_CURRENTLY_IN_SUDDEN_DEATH() AND IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_POWER_PLAY))
					AND IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
						TEXT_LABEL_15 tl15_CONTROL
						
						IF iTeamO = GET_LOCAL_PLAYER_TEAM()
							tl15_CONTROL = "DZ_Y_TEAM"
						ELSE
							IF DOES_TEXT_LABEL_EXIST(g_FMMC_STRUCT.tlCustomName[iTeamO])
								tl15_CONTROL = g_FMMC_STRUCT.tlCustomName[iTeamO]
							ELSE
								tl15_CONTROL = "FM_TDM_TEAM_S"
								tl15_CONTROL += iTeamO
							ENDIF
						ENDIF								

						HUD_COLOURS hcTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamO, LocalPlayer)
						HUDORDER eHUDOrder = INT_TO_ENUM(HUDORDER, ENUM_TO_INT(HUDORDER_FOURTHBOTTOM) - iTeamO)
						
						
						DRAW_GENERIC_METER(iTimeObjectTeamControlBarCached[iTeamO],
											MC_serverBD.iObjTakeoverTime[iTeamO][iObjectPriority],
											tl15_CONTROL,
											hcTeamColour,
											-1,
											eHUDOrder, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, hcTeamColour
											)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
	ELSE						
		IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlObjTimer[sObjState.iIndex])
			IF bPlayerToUseOK
				IF NOT IS_SPECTATOR_HUD_HIDDEN()
				AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_HIDE_HUD)
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_NEVER_SHOW_BOTTOM_RIGHT_CAPTURE_BARS)
				AND NOT g_bMissionEnding
				
					DRAW_GENERIC_METER(MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlObjTimer[sObjState.iIndex]),
										MC_serverBD.iObjTakeoverTime[iTeam][iObjectPriority],
										strControlHUDText,
										HUD_COLOUR_GREEN,
										-1
										)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_OBJECT_CARRY_TICKERS_AND_XP(FMMC_OBJECT_STATE& sObjState, INT& iTeam)
	
	IF iSpectatorTarget > -1
		EXIT
	ENDIF
	
	INT iShardOption = ciTICKER_ONLY
	INT iSubLogic = -1
		
	IF MC_serverBD.iObjCarrier[sObjState.iIndex] = iPartToUse
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
	
		IF MC_serverBD.iObjCarrier[sObjState.iIndex] = iPartToUse
		OR IS_BIT_SET(sObjState.iIndex, FMMC_OBJECT_STATE_CARRIED_BY_LOCAL_PLAYER)
			
			IF MC_serverBD_4.iObjRule[sObjState.iIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
			OR MC_serverBD_4.iObjRule[sObjState.iIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
			OR MC_serverBD_4.iObjRule[sObjState.iIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
			OR MC_serverBD_4.iObjRule[sObjState.iIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_PROTECT
			
				IF NOT IS_BIT_SET(iObjCollectHUDTriggeredBS, sObjState.iIndex)
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSet, cibsOBJ_Use_Booty_Collection_Messages)
						isublogic = ciTICKER_BOOTY
						iShardOption = ciTICKER_AND_SHARD
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_EnableObjectStealing)
						IF MC_serverBD_1.iPTLDroppedPackageLastOwner[sObjState.iIndex] != -1
						OR WAS_PICKUP_AT_ANOTHER_TEAM_HOLDING(sObjState.iIndex, GET_LOCAL_PLAYER_TEAM())
							isublogic = ciTICKER_STOLEN
						ENDIF
					ENDIF									
					
					IF NOT IS_BIT_SET(iSentObjectCollectionTickerBS, sObjState.iIndex)
						PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_CARRY_TICKERS_AND_XP | Sending collection ticker!")
						BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_COLLECT_OBJ, 0, GET_LOCAL_PLAYER_TEAM(TRUE), isublogic, INVALID_PLAYER_INDEX(), ci_TARGET_OBJECT, sObjState.iIndex, TRUE, iShardOption, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_IN_AND_OUT_SOUNDS))
						SET_BIT(iSentObjectCollectionTickerBS, sObjState.iIndex)
					ENDIF
					
					IF NOT IS_OBJECT_A_CONTAINER(sObjState.objIndex)
						IF iPackageColXPCount < 20
						AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[MC_serverBD_4.iObjPriority[sObjState.iIndex][iTeam]], ciBS_RULE2_DONT_GIVE_PLAYER_XP)
							IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
								GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,LocalPlayerPed, "XPT_MINORT",XPTYPE_ACTION,XPCATEGORY_COMPLETE_MISSION_DELIVERED_OBJ, g_sMPTunables.ixp_tuneable_collect_package_mission, 1)
							ENDIF
							iPackageColXPCount++
						ENDIF
					ENDIF
					
					SET_BIT(iObjCollectHUDTriggeredBS, sObjState.iIndex)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = -1
			IF IS_BIT_SET(iObjCollectHUDTriggeredBS, sObjState.iIndex)
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSet, cibsOBJ_Use_Booty_Collection_Messages)
					isublogic = ciTICKER_BOOTY
				ENDIF
				
				IF MC_serverBD_4.iObjRule[sObjState.iIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
					IF IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeam], sObjState.iIndex)									
						BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_DELIVER_OBJ,0,iTeam,isublogic,INVALID_PLAYER_INDEX(),ci_TARGET_OBJECT, sObjState.iIndex,TRUE,iShardOption, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_IN_AND_OUT_SOUNDS))
						IF iPackageDelXPCount < 10
						AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[MC_serverBD_4.iObjPriority[sObjState.iIndex][GET_LOCAL_PLAYER_TEAM(TRUE)]], ciBS_RULE2_DONT_GIVE_PLAYER_XP)
							IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
								FLOAT fdelRP = (100*g_sMPTunables.fxp_tunable_Deliver_a_package_bonus)
								INT idelRP = ROUND(fdelRP)
								GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,LocalPlayerPed, "XPT_MINORT",XPTYPE_ACTION,XPCATEGORY_COMPLETE_MISSION_DELIVERED_OBJ, idelRP, 1)
							ENDIF
							iPackageDelXPCount++
						ENDIF
					ELSE
						BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_DROP_OBJ, 0, iTeam, isublogic, INVALID_PLAYER_INDEX(), ci_TARGET_OBJECT, sObjState.iIndex, TRUE, iShardOption, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_IN_AND_OUT_SOUNDS))
					ENDIF
				ELSE
					IF sObjState.bObjExists
						BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_DROP_OBJ, 0, iTeam, isublogic, INVALID_PLAYER_INDEX(), ci_TARGET_OBJECT, sObjState.iIndex, TRUE, iShardOption, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_IN_AND_OUT_SOUNDS))
					ELSE
						PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_CARRY_TICKERS_AND_XP | Not showing TICKER_EVENT_DROP_OBJ ticker due to object no longer existing!")
					ENDIF
				ENDIF
				
				CLEAR_BIT(iObjCollectHUDTriggeredBS, sObjState.iIndex)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_OBJECT_HEALTHBAR(FMMC_OBJECT_STATE& sObjState)

	IF NOT sObjState.bObjExists
	OR NOT sObjState.bObjAlive
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetTwo, cibsOBJ2_EnableHealthBar)
		EXIT
	ENDIF
	
	ENTITY_HEALTHBAR_VARS sEntityHealthbarVars
	sEntityHealthbarVars.iEntityIndex = sObjState.iIndex
	sEntityHealthbarVars.iEntityType = ciENTITY_TYPE_OBJECT
	sEntityHealthbarVars.eiEntity = sObjState.objIndex
	
	sEntityHealthbarVars.iBlipName = g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].sObjBlipStruct.iBlipNameIndex
	
	sEntityHealthbarVars.vEntityCoords = GET_FMMC_OBJECT_COORDS(sObjState)
	sEntityHealthbarVars.iDisplayInRange = g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectHealthBarInRange
	
	sEntityHealthbarVars.iOnlyDisplayOnRule = g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectHealthBarOnRule
	
	INT iTeamToUse = GET_TEAM_TO_USE_FOR_ENTITY_HEALTHBAR(sEntityHealthbarVars, GET_LOCAL_PLAYER_TEAM(TRUE))
	sEntityHealthbarVars.tlHealthbarTitle = GET_ENTITY_HEALTHBAR_TITLE(sEntityHealthbarVars, iTeamToUse, sEntityHealthbarVars.bIsPlayerName, sEntityHealthbarVars.bIsLiteralString)
	sEntityHealthbarVars.iPercentageToDisplay = GET_ENTITY_HEALTHBAR_PERCENTAGE_TO_DISPLAY(sEntityHealthbarVars)
	
	PROCESS_ENTITY_HEALTHBAR_TURN_RED_AT_PERCENT(sEntityHealthbarVars, g_FMMC_STRUCT_ENTITIES.sPlacedObject[sEntityHealthbarVars.iEntityIndex].iHealthBarTurnRedAtPercent)
	
	STORE_ENTITY_HEALTHBAR_STRUCT(sEntityHealthbarVars)
ENDPROC

PROC PROCESS_OBJECT_HUD(FMMC_OBJECT_STATE& sObjState)

	INT iTeam = GET_LOCAL_PLAYER_TEAM(TRUE)
	INT iRule = GET_LOCAL_PLAYER_CURRENT_RULE(TRUE)
	
	PROCESS_OBJECT_MARKERS(sObjState)
	
	PROCESS_OBJECT_HEALTHBAR(sObjState)
	
	PROCESS_OBJECT_CAPTURE_METER(sObjState, iTeam, iRule)
	
	PROCESS_OBJECT_CARRY_TICKERS_AND_XP(sObjState, iTeam)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Object Every Frame Processing
// ##### Description: Client and Server Object Every Frame Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
PROC PROCESS_OBJ_PRE_EVERY_FRAME()
	
	sInteractWithVars.iInteractWith_HelptextGiverIndex = -1
	
	#IF IS_DEBUG_BUILD
	iTempProfilerActiveObjects = 0
	#ENDIF	
	
ENDPROC
			
PROC PROCESS_OBJ_POST_EVERY_FRAME()
	
	#IF IS_DEBUG_BUILD
	iProfilerActiveObjects = iTempProfilerActiveObjects
	#ENDIF		
		
	IF MC_ServerBD_4.iCurrentHighestPriority[GET_LOCAL_PLAYER_TEAM()] < FMMC_MAX_RULES
		IF HAS_NET_TIMER_STARTED(tdGranularCaptureTimer)
			IF HAS_NET_TIMER_EXPIRED(tdGranularCaptureTimer, ciGRANULAR_CAPTURE_TIME)
				IF iGranularPointsThisFrame > 0
					MC_PlayerBD[iLocalPart].iGranularCurrentPoints += iGranularPointsThisFrame
					MC_PlayerBD[iLocalPart].iGranularTotalPoints += iGranularPointsThisFrame
					iGranularPointsThisFrame = 0
				ENDIF
				RESET_NET_TIMER(tdGranularCaptureTimer)
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_ServerBD_4.iCurrentHighestPriority[GET_LOCAL_PLAYER_TEAM(TRUE)] < FMMC_MAX_RULES
		MANAGE_TRACKIFY_LOGIC(IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM(TRUE)].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[GET_LOCAL_PLAYER_TEAM(TRUE)]], ciBS_RULE_ENABLETRACKIFY))
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM(TRUE)].iRuleBitSetFive[MC_serverBD_4.iCurrentHighestPriority[GET_LOCAL_PLAYER_TEAM(TRUE)]], ciBS_RULE5_BEASTMODE_ENABLE_TRACKIFY_ON_BEAST)
			ASSERTLN("FMMC2020 - Here lay PROCESS_TRACKIFY_FOR_BEASTS - Bug Online Tools.")
		ENDIF
	ENDIF
	
	PROCESS_HACKING_CLEANUP()
	
	IF IS_PLAYER_PERFORMING_INTERACT_WITH_CUTSCENE_LEAD_IN()
		IF IS_CUTSCENE_PLAYING()
			IF GET_CUTSCENE_TIME() > ciInteractWith_CutsceneLeadInBufferTime
				PRINTLN("[InteractWith][InteractWithCutsceneLeadIn] PROCESS_OBJ_POST_EVERY_FRAME | Cutscene lead-in complete!")
				CLEANUP_CURRENT_INTERACT_WITH()
				
			ELSE
				PRINTLN("[InteractWith][InteractWithCutsceneLeadIn] PROCESS_OBJ_POST_EVERY_FRAME | Waiting for the cutscene lead-in buffer time to expire!")
				
			ENDIF
			
		ELIF MC_RUN_NET_TIMER_WITH_TIMESTAMP(sInteractWithVars.iInteractWith_CutsceneLeadInBailTimeStamp, ciInteractWith_CutsceneLeadInBailTime)
			PRINTLN("[InteractWith][InteractWithCutsceneLeadIn] PROCESS_OBJ_POST_EVERY_FRAME | Cutscene lead-in was bailed due to taking too long!")
			CLEANUP_CURRENT_INTERACT_WITH()
			
		ELSE
			PRINTLN("[InteractWith][InteractWithCutsceneLeadIn] PROCESS_OBJ_POST_EVERY_FRAME | Interact-With cutscene lead-in currently running!")
			
			FLOAT fInteractWithPhase = 0.0
			IF IS_SYNC_SCENE_RUNNING(MC_playerBD_1[iLocalPart].iInteractWithSyncedScene)
				fInteractWithPhase = GET_SYNC_SCENE_PHASE(MC_playerBD_1[iLocalPart].iInteractWithSyncedScene)
			ENDIF
	
			IF sInteractWithVars.iInteractWith_CachedAnimPreset = ciINTERACT_WITH_PRESET__INTERROGATION
				IF sInterrogationVars.iInterrogationSubAnimProgress = ciIW_SUBANIM__INTERROGATION_STAGE_APOLOGY_A
				OR sInterrogationVars.iInterrogationSubAnimProgress = ciIW_SUBANIM__INTERROGATION_STAGE_APOLOGY_B
					SET_FOLLOW_PED_CAM_THIS_UPDATE("FOLLOW_PED_INTIMIDATION_CAMERA")
				ENDIF
				
			ELIF sInteractWithVars.iInteractWith_CachedAnimPreset = ciINTERACT_WITH_PRESET__STEAL_LAPTOP_FROM_DJ_DESK
				IF NOT IS_BIT_SET(sInteractWithVars.iInteractWith_AudioBS, 0)
				AND DOES_ENTITY_EXIST(sInteractWithVars.piInteractWith_NearbyPedUsed)
					IF fInteractWithPhase >= 0.2
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(sInteractWithVars.piInteractWith_NearbyPedUsed, "FXIG_ELAA", "FIX_PROMOTER", AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE_SHOUTED), TRUE)
						SET_BIT(sInteractWithVars.iInteractWith_AudioBS, 0)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
		IF MC_playerBD[iLocalPart].iObjHacking = -1
		AND sInteractWithVars.eInteractWith_CurrentState != IW_STATE_IDLE
		AND sInteractWithVars.eInteractWith_CurrentState != IW_STATE_PERFORMING_ENTER_ANIMATION
		AND sInteractWithVars.iInteractWith_CachedObjectiveObjIndex > -1
		
			INTERACT_WITH_PARAMS sBailParams
			sBailParams.iInteractWith_AnimPreset = g_FMMC_STRUCT_ENTITIES.sPlacedObject[sInteractWithVars.iInteractWith_CachedObjectiveObjIndex].iObjectInteractionAnim
			sBailParams.iNetworkedObjectIndex = GET_OBJECT_NET_ID_INDEX(sInteractWithVars.iInteractWith_CachedObjectiveObjIndex)
			sBailParams.iObjectiveObjectIndex = sInteractWithVars.iInteractWith_CachedObjectiveObjIndex
			BAIL_INTERACT_WITH(sBailParams)
			
			CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
			PRINTLN("[InteractWith] We're not interacting with anything any more so setting sInteractWithVars.eInteractWith_CurrentState to NONE")
		ENDIF

		IF sInteractWithVars.iInteractWith_LastFrameProcessed < (GET_FRAME_COUNT() - ciInteractWith_PostAnimResetFrameDelay)
			IF sInteractWithVars.eInteractWith_CurrentState != IW_STATE_IDLE
			AND sInteractWithVars.iInteractWith_CachedObjectiveObjIndex > -1
				INTERACT_WITH_PARAMS sBailParams
				sBailParams.iInteractWith_AnimPreset = g_FMMC_STRUCT_ENTITIES.sPlacedObject[sInteractWithVars.iInteractWith_CachedObjectiveObjIndex].iObjectInteractionAnim
				sBailParams.iNetworkedObjectIndex = GET_OBJECT_NET_ID_INDEX(sInteractWithVars.iInteractWith_CachedObjectiveObjIndex)
				sBailParams.iObjectiveObjectIndex = sInteractWithVars.iInteractWith_CachedObjectiveObjIndex
				BAIL_INTERACT_WITH(sBailParams)

				CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
				PRINTLN("[InteractWith] We haven't processed interact-with in ages, so stop interacting now")
			ENDIF
			
			// Removing phone contact for phone activation
			IF IS_CONTACT_IN_PHONEBOOK(CHAR_MP_DETONATEPHONE, MULTIPLAYER_BOOK)
			AND eDetonateOrigin = DETONATE_BOMB_INTERACT_WITH
				REMOVE_DETONATE_BOMB_CONTACT()
				PRINTLN("[InteractWith] Removing CHAR_MP_DETONATEPHONE!")
			ENDIF
		ENDIF
	ENDIF
	
	/////////
	//Beam Hack Animation safeguards
	IF bhaBeamHackAnimState != BHA_STATE_NONE
		IF MC_playerBD[iLocalPart].iObjHacking = -1
			CLEAN_UP_PHYSICAL_BEAM_HACK_ANIM()
		ENDIF
	ENDIF
	
	/////////
	//Fingerprint Keypad Animation safeguards
	IF fkhaFingerprintKeypadHackAnimState != FKHA_STATE_NONE
		IF fkhaFingerprintKeypadHackAnimState  != FKHA_STATE_EXIT
		AND fkhaFingerprintKeypadHackAnimState != FKHA_STATE_SUCCESS_EXIT
		AND fkhaFingerprintKeypadHackAnimState != FKHA_STATE_QUICK_EXIT
			IF MC_playerBD[iLocalPart].iObjHacking = -1
				CLEAN_UP_PHYSICAL_FINGERPRINT_KEYPAD_HACK_ANIM()
			ENDIF
		ELSE
			HANDLE_PHYSICAL_FINGERPRINT_KEYPAD_HACK_ANIM_FALLBACK_CLEANUP()
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_SD_DROP_PORTABLE_PICKUPS) AND ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		IF NOT bIsAnySpectator
			IF NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_SUDDEN_DEATH_DROP_PICKUPS)
				PRINTLN("[RCC MISSION][SD][PROCESS_GENERIC_SUDDEN_DEATH_LOGIC] SUDDEN DEATH - ciENABLE_SD_DROP_PORTABLE_PICKUPS Calling to detach.")
				DETACH_PORTABLE_PICKUP_FROM_SPECIFIC_PED(localPlayerPed)
				DETACH_AND_RESET_ANY_PICKUPS()
				MC_playerBD[iLocalPart].iObjCarryCount = 0
				SET_BIT(iLocalBoolCheck24, LBOOL24_SUDDEN_DEATH_DROP_PICKUPS)
				
				//FMMC2020 - find a better place for this
				INT iObj
				FOR iObj = 0 TO (MC_serverBD.iNumObjCreated - 1)					
					OBJECT_INDEX tempObj
					IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iObj))
						tempObj = NET_TO_OBJ(GET_OBJECT_NET_ID(iObj))
						PREVENT_COLLECTION_OF_PORTABLE_PICKUP(tempObj, TRUE, TRUE)
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
	
	sInteractWithVars.iInteractWith_LastFrameHelptextGiverIndex = sInteractWithVars.iInteractWith_HelptextGiverIndex
	
ENDPROC

PROC PROCESS_OBJECT_CAPTURE_FROM_A_DISTANCE_HUD(FMMC_OBJECT_STATE &sObjState)
		
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
		
	IF MC_serverBD_4.iObjPriority[sObjState.iIndex][iTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_NEVER_SHOW_BOTTOM_RIGHT_CAPTURE_BARS)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_HIDE_HUD)
		EXIT
	ENDIF
	
	IF NOT MC_IS_PAUSABLE_TIMER_RUNNING(MC_serverBD.sObjCaptureTimer[sObjState.iIndex])
		EXIT
	ENDIF
	
	IF SHOULD_HIDE_THE_HUD_THIS_FRAME()
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
	AND MC_serverBD.iObjCapturer[sObjState.iIndex] != iPartToUse
		EXIT
	ENDIF
	
	STRING stLabel = "CON_TIME"
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetFive, cibsOBJ5_UseSecuroServHackingForCapture)
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF MC_IS_PAUSABLE_TIMER_RUNNING(MC_serverBD.sObjCaptureTimer[sObjState.iIndex])
			tl15HackComplete = GET_CUSTOM_STRING_LIST_TEXT_LABEL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iSecuroHackCustomString)
			iHackPercentage = ROUND((MC_GET_PAUSABLE_TIMER_TIME_ELAPSED(MC_serverBD.sObjCaptureTimer[sObjState.iIndex]) / TO_FLOAT(MC_serverBD.iObjTakeoverTime[iTeam][MC_serverBD_4.iObjPriority[sObjState.iIndex][iTeam]])) * 100)
			IF iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
				iHackStage = ciSECUROSERV_HACK_STAGE_HACKING
			ENDIF
		ENDIF
		IF IS_PHONE_ONSCREEN()
			EXIT
		ELSE
			stLabel = "CON_HACK"
		ENDIF
	ENDIF
	
	DRAW_GENERIC_METER(MC_GET_PAUSABLE_TIMER_TIME_ELAPSED(MC_serverBD.sObjCaptureTimer[sObjState.iIndex]),
				MC_serverBD.iObjTakeoverTime[iTeam][MC_serverBD_4.iObjPriority[sObjState.iIndex][iTeam]],
				stLabel, HUD_COLOUR_GREEN)
	
ENDPROC

FUNC BOOL ARE_ADDITIONAL_CAPTURE_REQUIREMENTS_MET(FMMC_OBJECT_STATE &sObjState)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetFive, cibsOBJ5_UseSecuroServHackingForCapture)
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		CLEAR_SECUROSERV_APP_LOSING_SIGNAL()
		IF NOT IS_SECUROSERV_APP_ACTIVATED()
			PRINTLN("[CAPTURE][Objects][Object ", sObjState.iIndex, "] ARE_ADDITIONAL_CAPTURE_REQUIREMENTS_MET - Requires a Securoserv Launch!")
			REQUEST_SECUROSERV_APP_ACTIVATION()
			RETURN FALSE
		ELIF iHackPercentage >= 100
		OR HAS_NET_TIMER_STARTED(tdHackingComplete)
			RETURN FALSE
		ENDIF
	ENDIF
	
	//Button Press?
	//Prereqs?
	//Add here
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_ADDITIONAL_OUT_OF_CAPTURE_RANGE_LOGIC(FMMC_OBJECT_STATE &sObjState)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetFive, cibsOBJ5_UseSecuroServHackingForCapture)
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		VECTOR vObjCoords = GET_FMMC_OBJECT_COORDS(sObjState)
		IF VDIST2_2D_PARAM(vLocalPlayerPosition, vObjCoords, NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetFive, cibsOBJ5_CheckZForCapture)) < POW(TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iCaptureRange) * 1.25, 2.0)
			IF MC_playerBD[iLocalPart].iObjCapturing = sObjState.iIndex
				SET_SECUROSERV_APP_LOSING_SIGNAL()
				iLosingSignalForObj = sObjState.iIndex
			ENDIF
		ELSE
			IF MC_playerBD[iLocalPart].iObjCapturing = sObjState.iIndex
			OR iLosingSignalForObj = sObjState.iIndex
				REQUEST_SECUROSERV_APP_STOP()
				CLEAR_SECUROSERV_APP_LOSING_SIGNAL()
				iLosingSignalForObj = -1
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_OBJECT_CAPTURE_FROM_A_DISTANCE(FMMC_OBJECT_STATE &sObjState)

	IF NOT sObjState.bObjAlive
		IF MC_playerBD[iLocalPart].iObjCapturing = sObjState.iIndex
			PRINTLN("[CAPTURE][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_CAPTURE_FROM_A_DISTANCE - Object is dead, clearing capture.")
			MC_playerBD[iLocalPart].iObjCapturing = -1
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetFive, cibsOBJ5_UseSecuroServHackingForCapture)
				REQUEST_SECUROSERV_APP_STOP()
			ENDIF
		ENDIF
		EXIT
	ENDIF
	
	IF NOT bLocalPlayerOK
		IF MC_playerBD[iLocalPart].iObjCapturing = sObjState.iIndex
			PRINTLN("[CAPTURE][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_CAPTURE_FROM_A_DISTANCE - I am dead, clearing capture.")
			MC_playerBD[iLocalPart].iObjCapturing = -1
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetFive, cibsOBJ5_UseSecuroServHackingForCapture)
				REQUEST_SECUROSERV_APP_STOP()
			ENDIF
		ENDIF
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iCaptureRange = 0
		EXIT
	ENDIF

	PROCESS_OBJECT_CAPTURE_FROM_A_DISTANCE_HUD(sObjState)
	
	IF MC_serverBD.iObjCapturer[sObjState.iIndex] != -1
	AND MC_serverBD.iObjCapturer[sObjState.iIndex] != iLocalPart
		IF MC_playerBD[iLocalPart].iObjCapturing = sObjState.iIndex
			PRINTLN("[CAPTURE][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_CAPTURE_FROM_A_DISTANCE - Someone else is capturing this object, clearing iObjCapturing and bailing.")
			MC_playerBD[iLocalPart].iObjCapturing = -1
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetFive, cibsOBJ5_UseSecuroServHackingForCapture)
				REQUEST_SECUROSERV_APP_STOP()
			ENDIF
		ENDIF
		EXIT
	ENDIF
	
	IF MC_playerBD[iLocalPart].iObjCapturing != -1
	AND MC_playerBD[iLocalPart].iObjCapturing != sObjState.iIndex
		//Capturing another object
		EXIT
	ENDIF
	
	INT iTeam = MC_playerBD[iLocalPart].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		MC_playerBD[iLocalPart].iObjCapturing = -1
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iObjPriority[sObjState.iIndex][iTeam] > iRule
	OR MC_serverBD_4.iObjRule[sObjState.iIndex][iTeam] != FMMC_OBJECTIVE_LOGIC_CAPTURE
		IF MC_playerBD[iLocalPart].iObjCapturing = sObjState.iIndex
			PRINTLN("[CAPTURE][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_CAPTURE_FROM_A_DISTANCE - Object is no longer a priority, clearing capture.")
			MC_playerBD[iLocalPart].iObjCapturing = -1
		ENDIF
		EXIT
	ENDIF
	
	VECTOR vObjCoords = GET_FMMC_OBJECT_COORDS(sObjState)
	
	IF VDIST2_2D_PARAM(vLocalPlayerPosition, vObjCoords, NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetFive, cibsOBJ5_CheckZForCapture)) < POW(TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iCaptureRange), 2.0)
		IF MC_serverBD.iObjCapturer[sObjState.iIndex] = -1
		AND MC_playerBD[iLocalPart].iObjCapturing != sObjState.iIndex
			IF ARE_ADDITIONAL_CAPTURE_REQUIREMENTS_MET(sObjState)
				PRINTLN("[CAPTURE][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_CAPTURE_FROM_A_DISTANCE - Requesting Capture.")
				MC_playerBD[iLocalPart].iObjCapturing = sObjState.iIndex
			ENDIF
		ELIF MC_playerBD[iLocalPart].iObjCapturing = -1
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetFive, cibsOBJ5_UseSecuroServHackingForCapture)
			REQUEST_SECUROSERV_APP_STOP()
		ENDIF
	ELSE
		PROCESS_ADDITIONAL_OUT_OF_CAPTURE_RANGE_LOGIC(sObjState)
		IF MC_playerBD[iLocalPart].iObjCapturing != -1
			PRINTLN("[CAPTURE][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_CAPTURE_FROM_A_DISTANCE - Object is no longer in range, clearing capture.")
			MC_playerBD[iLocalPart].iObjCapturing = -1
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_OBJECT_CAPTURE(FMMC_OBJECT_STATE &sObjState)
	
	PROCESS_OBJECT_CAPTURE_FROM_A_DISTANCE(sObjState)
	
	IF MC_serverBD_4.iObjPriority[sObjState.iIndex][GET_LOCAL_PLAYER_TEAM(TRUE)] >= FMMC_MAX_RULES
	OR MC_serverBD_4.iObjRule[sObjState.iIndex][GET_LOCAL_PLAYER_TEAM()] != FMMC_OBJECTIVE_LOGIC_CAPTURE
	OR NOT sObjState.bObjAlive
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iRuleBitsetNine[MC_serverBD_4.iObjPriority[sObjState.iIndex][GET_LOCAL_PLAYER_TEAM()]], ciBS_RULE9_USE_GRANULAR_CAPTURE)
		PROCESS_GRANULAR_CAPTURE(sObjState.iIndex)
	ENDIF
	
ENDPROC

PROC PROCESS_OBJECT_CAPTURE_SERVER(FMMC_OBJECT_STATE &sObjState)
	
	IF MC_serverBD.iObjCapturer[sObjState.iIndex] != -1
	AND MC_playerBD[MC_serverBD.iObjCapturer[sObjState.iIndex]].iObjCapturing != sObjState.iIndex
		IF MC_IS_PAUSABLE_TIMER_RUNNING(MC_serverBD.sObjCaptureTimer[sObjState.iIndex])
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetFive, cibsOBJ5_KeepCaptureProgress)
				PRINTLN("[CAPTURE][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_CAPTURE_SERVER - Part ", MC_serverBD.iObjCapturer[sObjState.iIndex], " is no longer capturing this object. Pausing")
				MC_PAUSE_PAUSABLE_TIMER(MC_serverBD.sObjCaptureTimer[sObjState.iIndex])
			ELSE
				PRINTLN("[CAPTURE][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_CAPTURE_SERVER - Part ", MC_serverBD.iObjCapturer[sObjState.iIndex], " is no longer capturing this object. Clearing")
				MC_RESET_PAUSABLE_TIMER(MC_serverBD.sObjCaptureTimer[sObjState.iIndex])
			ENDIF
		ENDIF
		MC_serverBD.iObjCapturer[sObjState.iIndex] = -1
	ENDIF
	
	IF MC_serverBD.iObjCapturer[sObjState.iIndex] = -1
		EXIT
	ENDIF
	
	INT iObjectCaptureTeam = MC_playerBD[MC_serverBD.iObjCapturer[sObjState.iIndex]].iTeam
	
	IF MC_serverBD_4.iObjRule[sObjState.iIndex][iObjectCaptureTeam] != FMMC_OBJECTIVE_LOGIC_CAPTURE
		EXIT
	ENDIF
	
	INT iObjectCaptureRule = MC_serverBD_4.iCurrentHighestPriority[iObjectCaptureTeam]
	
	IF MC_serverBD_4.iObjPriority[sObjState.iIndex][iObjectCaptureTeam] > iObjectCaptureRule
		EXIT
	ENDIF
	
	IF NOT MC_IS_PAUSABLE_TIMER_RUNNING(MC_serverBD.sObjCaptureTimer[sObjState.iIndex])
		PRINTLN("[CAPTURE][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_CAPTURE_SERVER - Starting Capture Timer.")
		MC_START_PAUSABLE_TIMER(MC_serverBD.sObjCaptureTimer[sObjState.iIndex])
	ENDIF
	
	IF NOT MC_HAS_PAUSABLE_TIMER_EXPIRED(MC_serverBD.sObjCaptureTimer[sObjState.iIndex], MC_serverBD.iObjTakeoverTime[iObjectCaptureTeam][iObjectCaptureRule])
		EXIT
	ENDIF
	
	INT iSublogic = -1
	PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(MC_serverBD.iObjCapturer[sObjState.iIndex])
	PLAYER_INDEX piCapturer = INVALID_PLAYER_INDEX()
	IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
		piCapturer = NETWORK_GET_PLAYER_INDEX(tempPart)
	ENDIF
	
	BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_CON_OBJ, DEFAULT, iObjectCaptureTeam, iSublogic, piCapturer, ci_TARGET_OBJECT, sObjState.iIndex)
	REQUEST_INCREMENT_SERVER_REMOTE_PLAYER_SCORE(MC_serverBD.iObjCapturer[sObjState.iIndex], GET_FMMC_POINTS_FOR_TEAM(iObjectCaptureTeam, iObjectCaptureRule), iObjectCaptureTeam, iObjectCaptureRule)
	PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT, sObjState.iIndex, iObjectCaptureTeam, TRUE)
	
	MC_RESET_PAUSABLE_TIMER(MC_serverBD.sObjCaptureTimer[sObjState.iIndex])
	MC_serverBD.iObjCapturer[sObjState.iIndex] = -1
	
ENDPROC

PROC PROCESS_OBJECT_TAGGING(FMMC_OBJECT_STATE& sObjState)
	
	IF NOT SHOULD_SHOW_TAGGED_TARGET()
		EXIT
	ENDIF
	
	IF GET_LOCAL_PLAYER_CURRENT_RULE() >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF IS_IT_SAFE_TO_ADD_TAG_BLIP(GET_LOCAL_PLAYER_CURRENT_RULE())
		INT iTagged
		FOR iTagged = 0 TO FMMC_MAX_TAGGED_ENTITIES-1
			IF MC_serverBD_3.iTaggedEntityType[iTagged] = 4
				IF MC_serverBD_3.iTaggedEntityIndex[iTagged] = sObjState.iIndex
					IF IS_ENTITY_ALIVE(sObjState.objIndex)
						VECTOR vCoordsToUse = GET_FMMC_OBJECT_COORDS(sObjState)
						vCoordsToUse.z = (vCoordsToUse.z + 1.0)
						DRAW_TAGGED_MARKER(vCoordsToUse, sObjState.objIndex, 4, sObjState.iIndex)
						
						IF NOT DOES_BLIP_EXIST(biTaggedEntity[iTagged])
						AND NOT DOES_BLIP_EXIST(GET_BLIP_FROM_ENTITY(sObjState.objIndex))
							ADD_TAGGED_BLIP(sObjState.objIndex, 4, sObjState.iIndex, iTagged)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC PROCESS_OBJECT_EXPLODE_FROM_NEARBY_EXPLOSION(FMMC_OBJECT_STATE& sObjState)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].fDestroyExplosionRadius <= 0.0
		EXIT
	ENDIF
	
	IF NOT sObjState.bObjAlive
	OR sObjState.bObjBroken
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iExplosionDestroyedObjectBitSet, sObjState.iIndex)
		IF GET_ENTITY_HEALTH(sObjState.objIndex) = 1
			CLEAR_BIT(iExplosionDestroyedObjectBitSet, sObjState.iIndex)
			PRINTLN("[DestroyExplosionRadius][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_EXPLODE_FROM_NEARBY_EXPLOSION Clearing bit so that player can have another go - turret somehow survived. Obj: ", sObjState.iIndex)
		ENDIF
		
		EXIT
	ENDIF
	
	EXPLOSION_TAG etExplType = EXP_TAG_DONTCARE
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetTwo,  cibsOBJ2_ExplodeOnlyVolatolWide)
		PRINTLN("[DestroyExplosionRadius] Looking for EXP_TAG_BOMB_STANDARD_WIDE for obj ", sObjState.iIndex)
		etExplType = EXP_TAG_BOMB_STANDARD_WIDE
	ENDIF

	ENTITY_INDEX eiExplosionOwner = GET_OWNER_OF_EXPLOSION_IN_SPHERE(etExplType, GET_FMMC_OBJECT_COORDS(sObjState), g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].fDestroyExplosionRadius)
	
	IF DOES_ENTITY_EXIST(eiExplosionOwner)
		IF IS_ENTITY_A_PED(eiExplosionOwner) //ensuring the turrets don't blow themselves up using this
			PRINTLN("[RCC MISSION][DestroyExplosionRadius][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_EXPLODE_FROM_NEARBY_EXPLOSION - Setting entity health to zero. Caught by a ped's explosion in set range: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].fDestroyExplosionRadius)
			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(sObjState.objIndex)
				SET_ENTITY_HEALTH(sObjState.objIndex, 1)
				PRINTLN("[RCC MISSION][DestroyExplosionRadius][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_EXPLODE_FROM_NEARBY_EXPLOSION - I'm currently in control of of turret ", sObjState.iIndex, "! Setting its health to 1 now")
			ELSE
				PRINTLN("[RCC MISSION][DestroyExplosionRadius][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_EXPLODE_FROM_NEARBY_EXPLOSION - I'm not in control of turret obj ", sObjState.iIndex)
			ENDIF
			
			IF GET_PED_INDEX_FROM_ENTITY_INDEX(eiExplosionOwner) = LocalPlayerPed
				ADD_OWNED_EXPLOSION(eiExplosionOwner, GET_FMMC_OBJECT_COORDS(sObjState), EXP_TAG_BARREL, 5)
				PRINTLN("[RCC MISSION][DestroyExplosionRadius][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_EXPLODE_FROM_NEARBY_EXPLOSION - I'm the owner of the explosion near turret ", sObjState.iIndex, "! Creating explosion on the turret now")
			ELSE
				PRINTLN("[RCC MISSION][DestroyExplosionRadius][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_EXPLODE_FROM_NEARBY_EXPLOSION - I'm not the owner of the explosion on obj ", sObjState.iIndex)
			ENDIF
			
			SET_BIT(iExplosionDestroyedObjectBitSet, sObjState.iIndex)
		ELSE
			PRINTLN("[DestroyExplosionRadius][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_EXPLODE_FROM_NEARBY_EXPLOSION | Owner of explosion is not a ped for obj ", sObjState.iIndex)
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC PROCESS_OBJECT_DEBUG(FMMC_OBJECT_STATE& sObjState)
	
	IF NOT sObjState.bObjExists
		EXIT
	ENDIF
	
	IF bObjDebug
		TEXT_LABEL_63 tlEntityDebug = "Obj "
		tlEntityDebug += sObjState.iIndex
		DRAW_DEBUG_TEXT_ABOVE_ENTITY(sObjState.objIndex, tlEntityDebug, 0.6, 255, 255, 255)
	ENDIF
	
	IF bHideObjects
		IF DOES_ENTITY_EXIST(sObjState.objIndex)
			SET_ENTITY_VISIBLE(sObjState.objIndex, FALSE)
		ENDIF
	ENDIF
ENDPROC
#ENDIF

PROC PROCESS_OBJECT_CONTAINER_BEHAVIOUR(FMMC_OBJECT_STATE& sObjState)
	
	IF NOT IS_OBJECT_A_CONTAINER(sObjState.objIndex)
		EXIT
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		VEHICLE_INDEX PlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		IF IS_VEHICLE_DRIVEABLE(PlayerVeh)
			MODEL_NAMES mnVeh = GET_ENTITY_MODEL(PlayerVeh)
			IF mnVeh = HANDLER
				SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_CONTEXT) // Needed for PC to stop the horn sounding in default controls
				IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(PlayerVeh)
					IF IS_HANDLER_FRAME_LINED_UP_WITH_CONTAINER(PlayerVeh, sObjState.objIndex)
						IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdDhandlerTimer) > 1000
							IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_CONTEXT)
								REINIT_NET_TIMER(tdDhandlerTimer)
								ATTACH_CONTAINER_TO_HANDLER_FRAME_WHEN_LINED_UP(PlayerVeh, sObjState.objIndex)
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(iContBitSet, sObjState.iIndex)
						SET_BIT(iDropContBitSet, sObjState.iIndex)
						CLEAR_BIT(iContBitSet, sObjState.iIndex)
					ENDIF
					
				ELSE
					//reset timer each frame when the container is attached to handler frame
					REINIT_NET_TIMER(tdDhandlerTimer)
				ENDIF
				
			ELIF IS_VEHICLE_MODEL_A_CARGOBOB(mnVeh)
				IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(sObjState.objIndex, PlayerVeh)
					IF IS_BIT_SET(iContBitSet, sObjState.iIndex)
						SET_BIT(iDropContBitSet, sObjState.iIndex)
						CLEAR_BIT(iContBitSet, sObjState.iIndex)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//If it was just dropped:
	IF IS_BIT_SET(iDropContBitSet, sObjState.iIndex)
		IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(sObjState.objIndex)
			INT iVeh
			FOR iVeh = 0 TO MC_serverBD.iNumVehCreated
				IF(NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
					VEHICLE_INDEX tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
				
					IF IS_VEHICLE_MODEL(tempVeh, FLATBED)
					OR IS_VEHICLE_MODEL(tempVeh, INT_TO_ENUM(MODEL_NAMES, HASH("WASTELANDER")))
						IF GET_DISTANCE_BETWEEN_ENTITIES(tempVeh, sObjState.objIndex) < 10
							IF IS_VECTOR_IN_AREA(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(tempVeh, GET_ENTITY_COORDS(sObjState.objIndex)), <<0.0000,-2.8000,0.4100>>,<<3,3.5,2.5>>)
								IF IS_VECTOR_IN_AREA(GET_ENTITY_ROTATION(sObjState.objIndex), GET_ENTITY_ROTATION(tempVeh),<<80,80,80>>)
									ATTACH_OBJECT_TO_FLATBED(sObjState.objIndex, tempVeh, g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].vAttachOffset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].vAttachOffsetRotation, TRUE)
									CLEAR_BIT(iDropContBitSet, sObjState.iIndex)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
			IF NOT IS_ENTITY_IN_AIR(sObjState.objIndex)
				CLEAR_BIT(iDropContBitSet, sObjState.iIndex)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_GENERIC_OBJ_HACK_BREAKOUTS(FMMC_OBJECT_STATE& sObjState)
	IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iHackFailBitset, MC_playerBD[iLocalPart].iObjHacking)
		IF g_bMissionEnding
			SET_HACK_FAIL_BIT_SET(4)
			PRINTLN("[RCC MISSION]  setting hack fail bitset - mission finished: ",iLocalPart," for object: ",MC_playerBD[iLocalPart].iObjHacking)
		ENDIF
		IF iSpectatorTarget != -1
			SET_HACK_FAIL_BIT_SET(3)
			PRINTLN("[RCC MISSION]  setting hack fail bitset - iSpectatorTarget != -1, part: ",iPartToUse," for object: ",MC_playerBD[iPartToUse].iObjHacking)
		ENDIF
		IF NOT bPlayerToUseOK
			SET_HACK_FAIL_BIT_SET(2)
			PRINTLN("[RCC MISSION]  setting hack fail bitset - dead, part: ",iPartToUse," for object: ",MC_playerBD[iLocalPart].iObjHacking)
		ELSE
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame != ciFMMC_OBJECT_MINIGAME_TYPE__BEAMHACK_VEHICLE
				IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, sObjState.objIndex) >= 10.0
				AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame != ciFMMC_OBJECT_MINIGAME_TYPE__INTERACT_WITH
				AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame != ciFMMC_OBJECT_MINIGAME_TYPE__USE_THERMAL_CHARGE
				AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame != ciFMMC_OBJECT_MINIGAME_TYPE__INTERROGATION
				AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].fActivationRangeOverride <= 0.0
					SET_HACK_FAIL_BIT_SET(1)
					PRINTLN("[RCC MISSION]  setting hack fail bitset - too far, part: ", iPartToUse, " for object: ", MC_playerBD[iLocalPart].iObjHacking)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF MC_playerBD[iLocalPart].iObjHacking > -1
		AND MC_serverBD.iObjHackPart[MC_playerBD[iLocalPart].iObjHacking] = -1
			RESET_ALL_HACKING_MINIGAMES()
			PRINTLN("[RCC MISSION]  RESET_ALL_HACKING_MINIGAMES 2")
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_OBJ_PHOTO_DATA(INT iObjectIndex)
	IF iObjectIndex != iObjPhotoIndex
		EXIT
	ENDIF
	
	iObjPhotoIndex = -1
	fObjPhotoDistance2 = MAX_FLOAT
	IF MC_playerBD[iLocalPart].iObjNear = iObjectIndex
		CLEANUP_PHOTO_TRACKED_POINT(iObjPhotoTrackedPoint)
		SET_OBJ_NEAR(-1)
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PH_FAST_PHO")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PH_FAST_PHO_2")
		CLEAR_HELP()
	ENDIF
ENDPROC

PROC PROCESS_OBJ_PHOTO_OBJECTIVE_EVERY_FRAME(FMMC_OBJECT_STATE& sObjState)
	
	IF NOT sObjState.bObjExists
		CLEANUP_OBJ_PHOTO_DATA(sObjState.iIndex)
		EXIT
	ENDIF
	
	IF NOT bLocalPlayerPedOk
	OR bIsAnySpectator
		CLEANUP_OBJ_PHOTO_DATA(sObjState.iIndex)
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] >= FMMC_MAX_RULES
		CLEANUP_OBJ_PHOTO_DATA(sObjState.iIndex)
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iObjRule[sObjState.iIndex][MC_playerBD[iLocalPart].iTeam] != FMMC_OBJECTIVE_LOGIC_PHOTO
	OR MC_serverBD_4.iObjPriority[sObjState.iIndex][MC_playerBD[iLocalPart].iTeam] > MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]
		IF iObjPhotoIndex = sObjState.iIndex
			PRINTLN("[PHOTO][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_PHOTO_OBJECTIVE_EVERY_FRAME - No longer photo objective, clearing iObjPhotoIndex.")
			CLEANUP_OBJ_PHOTO_DATA(sObjState.iIndex)
		ENDIF
		EXIT
	ENDIF
	
	IF iObjPhotoIndex = sObjState.iIndex
		
		//Update Distance
		fObjPhotoDistance2 = GET_DIST2_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, GET_FMMC_OBJECT_COORDS(sObjState))
		
		//Process Photo Taking
		IF fObjPhotoDistance2 >= POW(TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iPhotoRange), 2.0)
			//Out of range
			IF MC_playerBD[iLocalPart].iObjNear = sObjState.iIndex
				PRINTLN("[PHOTO][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_PHOTO_OBJECTIVE_EVERY_FRAME - Gone out of range to take photo.")
				CLEANUP_PHOTO_TRACKED_POINT(iObjPhotoTrackedPoint)
				SET_OBJ_NEAR(-1)
			ENDIF
			EXIT
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iRuleBitsetEleven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]], ciBS_RULE11_BLOCK_PHOTO_OBJECT_HELP_TEXT)
			IF NOT IS_PHONE_ONSCREEN()
				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CELLPHONE_QUICKLAUNCH) = 1
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PH_FAST_PHO")
						PRINT_HELP("PH_FAST_PHO")
					ENDIF
				ELSE
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PH_FAST_PHO_2")
						PRINT_HELP("PH_FAST_PHO_2")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		SET_OBJ_NEAR(sObjState.iIndex)
		
		IF NOT DOES_PHOTO_TRACKED_POINT_EXIST(iObjPhotoTrackedPoint)
			//Create new tracked point for current target coords
			CREATE_PHOTO_TRACKED_POINT_FOR_COORD(iObjPhotoTrackedPoint, GET_FMMC_OBJECT_COORDS(sObjState), 1.0)
			vLocalPhotoCoords = GET_FMMC_OBJECT_COORDS(sObjState)
		ELSE
			IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_FMMC_OBJECT_COORDS(sObjState), vLocalPhotoCoords, 0.05) //If the object's moved
				vLocalPhotoCoords = GET_FMMC_OBJECT_COORDS(sObjState)
				SET_TRACKED_POINT_INFO(iObjPhotoTrackedPoint, vLocalPhotoCoords, 1.0)
			ENDIF
			
			IF IS_PHOTO_TRACKED_POINT_VISIBLE(iObjPhotoTrackedPoint)
				IF IS_POINT_IN_CELLPHONE_CAMERA_VIEW(GET_FMMC_OBJECT_COORDS(sObjState), 0.2)
					IF HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
						PRINTLN("[PHOTO][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_PHOTO_OBJECTIVE_EVERY_FRAME - Photo taken, setting iObjPhoto.")
						CLEANUP_PHOTO_TRACKED_POINT(iObjPhotoTrackedPoint)
						SET_OBJ_NEAR(-1)
						MC_playerBD[iLocalPart].iObjPhoto = sObjState.iIndex
						IF SHOULD_PUT_AWAY_PHONE_AFTER_PHOTO(MC_serverBD_4.iObjPriority[sObjState.iIndex][MC_playerBD[iLocalPart].iTeam], ci_TARGET_OBJECT)
							REINIT_NET_TIMER(tdPhotoTakenTimer)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
	FLOAT fDistance2 = GET_DIST2_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, GET_FMMC_OBJECT_COORDS(sObjState))
	
	IF fDistance2 >= fObjPhotoDistance2
		EXIT
	ENDIF
	
	PRINTLN("[PHOTO][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_PHOTO_OBJECTIVE_EVERY_FRAME - Closer than current photo object (", iObjPhotoIndex, ") updating iObjPhotoIndex.")
	fObjPhotoDistance2 = fDistance2
	iObjPhotoIndex = sObjState.iIndex
	CLEANUP_PHOTO_TRACKED_POINT(iObjPhotoTrackedPoint)
	
ENDPROC

FUNC BOOL SHOULD_ALLOW_GOTO_RANGE_PICKUP(FMMC_OBJECT_STATE& sObjState)

	IF GET_LOCAL_PLAYER_CURRENT_RULE() >= FMMC_MAX_RULES
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iMaxDeliveryEntitiesCanCarry[GET_LOCAL_PLAYER_CURRENT_RULE()] > 0
		IF MC_playerBD[iLocalPart].iObjCarryCount >= g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iMaxDeliveryEntitiesCanCarry[GET_LOCAL_PLAYER_CURRENT_RULE()]
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_PICKUP_AT_SPECIFIC_HOLDING(sObjState.iIndex, GET_LOCAL_PLAYER_TEAM())
	OR IS_BIT_SET(iLocalObjAtHolding, sObjState.iIndex)
		RETURN FALSE
	ENDIF
					
	IF IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_SOMEONE)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_OBJECT_GOTO_RANGE(FMMC_OBJECT_STATE& sObjState)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSet, cibsOBJ_UsePickupProximity)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetThree, cibsOBJ3_AlternateProximityPickup)
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iGotoRange != 0
	OR NOT sObjState.bObjExists
	OR NOT sObjState.bHaveControlOfObj
		EXIT
	ENDIF
	
	IF NOT SHOULD_ALLOW_GOTO_RANGE_PICKUP(sObjState)
		EXIT
	ENDIF

	IF SHOULD_ALLOW_GOTO_RANGE_PICKUP(sObjState) 
	AND GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, sObjState.objIndex) <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iGotoRange
	
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetThree, cibsOBJ3_AlternateProximityPickup)
			
			// Do it the alternate way, where the object is simply moved to the player's position and picked up naturally
			VECTOR vTempPlayerCoord = GET_ENTITY_COORDS(LocalPlayerPed)
			VECTOR vObjCoords = GET_FMMC_OBJECT_COORDS(sObjState)
			vTempPlayerCoord.z = vObjCoords.z
			SET_ENTITY_COORDS(sObjState.objIndex, vTempPlayerCoord)
			SET_ENTITY_VISIBLE(sObjState.objIndex, FALSE)
		ELSE
		
			// Attach the object directly to the player
			ATTACH_ENTITY_TO_ENTITY(sObjState.objIndex, LocalPlayerPed, GET_PED_BONE_INDEX(LocalPlayerPed, BONETAG_PH_L_HAND),
							<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, TRUE, TRUE, FALSE)
							
			PLAY_SOUND_FRONTEND(-1, "Bus_Schedule_Pickup", "DLC_PRISON_BREAK_HEIST_SOUNDS", FALSE)
			SET_ENTITY_VISIBLE(sObjState.objIndex, FALSE)
		ENDIF
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetThree, cibsOBJ3_AlternateProximityPickup)
			IF NOT IS_ENTITY_VISIBLE(sObjState.objIndex)
			AND NOT IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_SOMEONE)
				SET_ENTITY_VISIBLE(sObjState.objIndex, TRUE)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_OBJECT_TO_PICK_UP_ASAP(FMMC_OBJECT_STATE& sObjState)
	
	IF iObjectToPickUpASAP != sObjState.iIndex
		EXIT
	ENDIF
	
	IF NOT sObjState.bObjExists
	OR NOT sObjState.bObjAlive
		PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_TO_PICK_UP_ASAP | Obj ", sObjState.iIndex, " doesn't exist or isn't alive!")
		EXIT
	ENDIF
	
	// We want to pick up this object ASAP!
	IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(sObjState.niIndex)
		NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sObjState.niIndex)
		PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_TO_PICK_UP_ASAP | Requesting control of the net ID of obj ", sObjState.iIndex)
		EXIT
	ENDIF
	
	IF IS_ENTITY_ATTACHED_TO_ENTITY(sObjState.objIndex, LocalPlayerPed)
		SET_BIT(iObjWasAttachedEarlyBS, sObjState.iIndex)
		iObjectToPickUpASAP = -1
	ELSE
		ATTACH_PORTABLE_PICKUP_TO_PED(sObjState.objIndex, LocalPlayerPed)
		PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_TO_PICK_UP_ASAP | Attaching obj ", sObjState.iIndex, " to local player now!")
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSet, cibsOBJ_Reset_If_Dropped_Out_Of_Bounds)
			PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_TO_PICK_UP_ASAP | Caching an initial safe drop pos for object due to PROCESS_OBJECT_TO_PICK_UP_ASAP!")
			CACHE_OBJECT_SAFE_DROP_POSITION(sObjState.iIndex, GET_ENTITY_COORDS(LocalPlayerPed), TRUE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_MINIGAME_BLOCKED_BY_EXTRA_REQUIREMENTS(FMMC_OBJECT_STATE &sObjState)
	
	BOOL bReturn = FALSE //Need to process all the conditions for their effects
	
	//Prereqs (all, complete block)
	IF NOT IS_LONG_BITSET_EMPTY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iPreReqsRequiredToUnblock)
		IF NOT ARE_ALL_PREREQUSITES_IN_LONG_BITSET_COMPLETED(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iPreReqsRequiredToUnblock)
			PRINTLN("[Objects][Object ", sObjState.iIndex, "] IS_MINIGAME_BLOCKED_BY_EXTRA_REQUIREMENTS - Need to complete multiple prereqs - BLOCKED")
			SET_BIT(iMinigameObjBlipBlockerBS, sObjState.iIndex)
			bReturn = TRUE
		ELSE
			CLEAR_BIT(iMinigameObjBlipBlockerBS, sObjState.iIndex)
		ENDIF
	ENDIF
	
	//Prereq (any - help and blip)
	IF NOT IS_LONG_BITSET_EMPTY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iPreReqRequiredForMinigame)
		//Maybe move to the top if any requirements set if/when we add more.
		REFRESH_OBJECT_BLIP_COLOURING(sObjState)

		IF NOT (g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iPreReqRequiredForMinigameCount = 1 AND ARE_ANY_PREREQUSITES_IN_LONG_BITSET_COMPLETED(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iPreReqRequiredForMinigame))
		AND NOT (g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iPreReqRequiredForMinigameCount > 1 AND ARE_X_PREREQUSITES_IN_LONG_BITSET_COMPLETED(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iPreReqRequiredForMinigame, g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iPreReqRequiredForMinigameCount))
			IF VDIST2(vLocalPlayerPosition, GET_FMMC_OBJECT_COORDS(sObjState)) < POW(5.0, 2) //This should cover most minigames, unfortunately not standardised
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iPreReqRequiredForMinigameHelpText != -1
					TEXT_LABEL_15 tl15 = GET_CUSTOM_STRING_LIST_TEXT_LABEL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iPreReqRequiredForMinigameHelpText)
					IF NOT IS_THIS_PRINT_BEING_DISPLAYED(tl15)
						PRINT_HELP(tl15)
					ENDIF
				ENDIF
			ENDIF
			
			SET_BIT(iMinigameObjPreReqBlockerBS, sObjState.iIndex)
			PRINTLN("[Objects][Object ", sObjState.iIndex, "] IS_MINIGAME_BLOCKED_BY_EXTRA_REQUIREMENTS - Need to complete a prereq - BLOCKED")
			bReturn = TRUE
		ELSE
			CLEAR_BIT(iMinigameObjPreReqBlockerBS, sObjState.iIndex)
		ENDIF
	ENDIF
	
	RETURN bReturn
ENDFUNC

PROC PROCESS_OBJECTIVE_OBJECT_MINIGAMES_EVERY_FRAME_CLIENT(FMMC_OBJECT_STATE& sObjState)

	IF MC_serverBD_4.iObjRule[sObjState.iIndex][GET_LOCAL_PLAYER_TEAM(TRUE)] != FMMC_OBJECTIVE_LOGIC_MINIGAME
		EXIT
	ENDIF
	
	IF IS_MINIGAME_BLOCKED_BY_EXTRA_REQUIREMENTS(sObjState)
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__FULL_HACK
		ASSERTLN("[FMMC2020] PROCESS_HACKING_MINI_GAME - May not function as intended, probably needs a refactor")
		PROCESS_HACKING_MINI_GAME(sObjState.objIndex, sObjState.iIndex)

	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__CONNECT_HACK
		ASSERTLN("[FMMC2020] PROCESS_HACKING_MINI_GAME(sObjState.objIndex, sObjState.iIndex,TRUE) - May not function as intended, probably needs a refactor")
		PROCESS_HACKING_MINI_GAME(sObjState.objIndex, sObjState.iIndex, TRUE)
	
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__DRILLING
		ASSERTLN("[FMMC2020] PROCESS_DRILL_MINI_GAME - May not function as intended, probably needs a refactor")
		PROCESS_DRILL_MINI_GAME(sObjState.objIndex, sObjState.iIndex)
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__VAULT_DRILLING
		ASSERTLN("[FMMC2020] PROCESS_VAULT_DRILL_MINI_GAME - May not function as intended, probably needs a refactor")
		PROCESS_VAULT_DRILL_MINI_GAME(sObjState.objIndex, sObjState.iIndex)
			
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__LESTER_SNAKE
		ASSERTLN("[FMMC2020] PROCESS_LESTER_SNAKE_GAME_MC - May not function as intended, probably needs a refactor")
		PROCESS_LESTER_SNAKE_GAME_MC(sObjState.objIndex, sObjState.iIndex)
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__BEAM_HACK
		ASSERTLN("[FMMC2020] PROCESS_BEAM_HACK_GAME_MC - May not function as intended, probably needs a refactor")
		PROCESS_BEAM_HACK_GAME_MC(sObjState.objIndex, sObjState.iIndex)
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__INTERACT_WITH
		INTERACT_WITH_PARAMS sInteractWithParams
		FILL_INTERACT_WITH_PARAMS_FROM_OBJECTIVE_OBJECT(sInteractWithParams, sObjState.iIndex, sObjState.objIndex) 
		IF PROCESS_INTERACT_WITH_ANIMATION(sInteractWithParams)
			PRINTLN("[InteractWith_SPAM] PROCESS_OBJECTIVE_OBJECT_MINIGAMES_EVERY_FRAME_CLIENT - PROCESS_INTERACT_WITH_ANIMATION returned TRUE!")
			PROCESS_INTERACT_WITH_OBJECTIVE_MINIGAME_COMPLETED(sInteractWithParams)
		ENDIF
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__HOTWIRE
		ASSERTLN("[FMMC2020] PROCESS_HOTWIRE_GAME_MC - May not function as intended, probably needs a refactor")
		PROCESS_HOTWIRE_GAME_MC(sObjState.objIndex, sObjState.iIndex)
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__ORDER_UNLOCK
		ASSERTLN("[FMMC2020] PROCESS_FINGERPRINT_KEYPAD_HACK_GAME_MC - May not function as intended, probably needs a refactor")
		PROCESS_FINGERPRINT_KEYPAD_HACK_GAME_MC(sObjState.objIndex, sObjState.iIndex, ciFMMC_OBJECT_MINIGAME_TYPE__ORDER_UNLOCK)
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__FINGERPRINT_CLONE
		ASSERTLN("[FMMC2020] PROCESS_FINGERPRINT_KEYPAD_HACK_GAME_MC - May not function as intended, probably needs a refactor")
		PROCESS_FINGERPRINT_KEYPAD_HACK_GAME_MC(sObjState.objIndex, sObjState.iIndex, ciFMMC_OBJECT_MINIGAME_TYPE__FINGERPRINT_CLONE)
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__WELD_UNDERWATER_TUNNEL
		IF PROCESS_UNDERWATER_GRID_WELDING_MINIGAME(sObjState.iIndex, sObjState.objIndex)
			SET_OBJECT_AS_HACKED(sObjState.iIndex)
		ENDIF
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__ENTER_SAFE_COMBINATION
		IF PROCESS_ENTER_SAFE_COMBINATION_MINIGAME(sObjState.iIndex, sObjState.objIndex)
			SET_OBJECT_AS_HACKED(sObjState.iIndex)
		ENDIF
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__GLASS_CUTTING
		IF PROCESS_GLASS_CUTTING_MINIGAME(sObjState.iIndex, sObjState.objIndex)
			SET_OBJECT_AS_HACKED(sObjState.iIndex)
		ENDIF
	
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__INTERROGATION
		IF PROCESS_INTERROGATION_MINIGAME(sObjState.iIndex, sObjState.objIndex)
			SET_OBJECT_AS_HACKED(sObjState.iIndex)
		ENDIF
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__BRUTE_FORCE_ULP
		IF PROCESS_BRUTEFORCE_MINIGAME(sObjState)
			SET_OBJECT_AS_HACKED(sObjState.iIndex)
		ENDIF
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__BEAMHACK_VEHICLE
		IF PROCESS_BEAMHACK_MINIGAME_IN_VEHICLE(sObjState)
			SET_OBJECT_AS_HACKED(sObjState.iIndex)
		ENDIF
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__INSERT_FUSE
		IF PROCESS_INSERT_FUSE_MINIGAME(sObjState)
			SET_OBJECT_AS_HACKED(sObjState.iIndex)
		ENDIF
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__CHECK_FUSEBOX
		IF PROCESS_CHECK_FUSE_BOX_MINIGAME(sObjState)
			SET_OBJECT_AS_HACKED(sObjState.iIndex)
		ENDIF
	ENDIF
	
	IF sInteractWithVars.iInteractWith_LastDisplayedObjText != sInteractWithVars.iInteractWith_ObjectiveTextToUse
		sInteractWithVars.iInteractWith_LastDisplayedObjText = sInteractWithVars.iInteractWith_ObjectiveTextToUse
		REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_OBJECT, FALSE #IF IS_DEBUG_BUILD , "PROCESS_OBJECTIVE_OBJECT_MINIGAMES_EVERY_FRAME_CLIENT - objective text changed" #ENDIF )
	ENDIF
	
ENDPROC

PROC PROCESS_OBJECTIVE_OBJECT_GOTO_EVERY_FRAME_CLIENT(FMMC_OBJECT_STATE& sObjState)
	
	IF (MC_serverBD_4.iObjRule[sObjState.iIndex][GET_LOCAL_PLAYER_TEAM(TRUE)] != FMMC_OBJECTIVE_LOGIC_GO_TO AND MC_serverBD_4.iObjRule[sObjState.iIndex][GET_LOCAL_PLAYER_TEAM(TRUE)] != FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY)
	OR iSpectatorTarget > -1
		EXIT
	ENDIF
	
	// This sets up the progression rule for a go-to rule on an object (which could be a pickup)
	
	// This is the override value for the range at which you have "arrived" at the object
	#IF IS_DEBUG_BUILD
	IF bObjectGotoDebug
		PRINTLN("[JS][ObjGoto] - GOTO Obj - ", sObjState.iIndex)
	ENDIF
	#ENDIF
	
	BOOL bHasGoToRange
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iGotoRange != 0
		INT iMultiRuleNo = GET_ENTITY_MULTIRULE_NUMBER(sObjState.iIndex, GET_LOCAL_PLAYER_TEAM(TRUE), ci_TARGET_OBJECT)
				
		IF (iMultiRuleNo != -1)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSet, cibsOBJ_IgnoreProximityPrimaryRule + iMultiRuleNo)
			bHasGoToRange = TRUE
			//First Person Check added to fix url:bugstar:3130707
			IF ((NOT IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_SOMEONE)) OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON)
			AND (GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, sObjState.objIndex) <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iGotoRange)
				SET_OBJ_NEAR(sObjState.iIndex)
			ELSE
				#IF IS_DEBUG_BUILD
				IF bObjectGotoDebug
					PRINTLN("[JS][ObjGoto] - Distance not within range: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iGotoRange ," > ", GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, sObjState.objIndex))
				ENDIF
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF bObjectGotoDebug
				PRINTLN("[JS][ObjGoto] - cibsOBJ_IgnoreProximityPrimaryRule is set or iMultiRuleNo (",iMultiRuleNo,") is -1")
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[JS][ObjGoto] - PROCESS_OBJECTIVE_OBJECT_GOTO_EVERY_FRAME_CLIENT CALLING.")
	
	IF MC_serverBD_4.iObjRule[sObjState.iIndex][GET_LOCAL_PLAYER_TEAM()] = FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY
	AND GET_MC_CLIENT_MISSION_STAGE(iLocalPart) = CLIENT_MISSION_STAGE_LEAVE_OBJ
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iGotoRange != 0
		AND iSpectatorTarget = -1
			
			INT iMultiRuleNo = GET_ENTITY_MULTIRULE_NUMBER(sObjState.iIndex, MC_playerBD[iPartToUse].iTeam, ci_TARGET_OBJECT)
			
			IF (iMultiRuleNo != -1)
			AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSet, cibsOBJ_IgnoreProximityPrimaryRule + iMultiRuleNo)
				
				FLOAT fDist
				FLOAT fRange
				fDist = VDIST2(GET_FMMC_OBJECT_COORDS(sObjState), GET_ENTITY_COORDS(LocalPlayerPed, FALSE))
				fRange = POW(TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iGotoRange), 2.0)
				
				IF fDist <= fRange
					CLEAR_BIT(MC_playerBD[iLocalPart].iObjLeftBS, sObjState.iIndex)
				ELSE
					SET_BIT(MC_playerBD[iLocalPart].iObjLeftBS, sObjState.iIndex)
				ENDIF
			ELSE
				SET_BIT(MC_playerBD[iLocalPart].iObjLeftBS, sObjState.iIndex)
			ENDIF
		ELSE
			SET_BIT(MC_playerBD[iLocalPart].iObjLeftBS, sObjState.iIndex)
		ENDIF
	ELSE
		CLEAR_BIT(MC_playerBD[iLocalPart].iObjLeftBS, sObjState.iIndex)
	ENDIF

	// Else the range is 0 and therefore we want you to have to pick up the object first
	IF NOT bHasGoToRange
	AND IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_LOCAL_PLAYER)
		SET_OBJ_NEAR(sObjState.iIndex)
	ENDIF
	
ENDPROC

PROC PROCESS_OBJECTIVE_OBJECT_EVERY_FRAME_CLIENT(FMMC_OBJECT_STATE& sObjState)
	
	IF MC_serverBD_4.iObjRule[sObjState.iIndex][GET_LOCAL_PLAYER_TEAM(TRUE)] = FMMC_OBJECTIVE_LOGIC_NONE
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iObjPriority[sObjState.iIndex][GET_LOCAL_PLAYER_TEAM(TRUE)] > GET_LOCAL_PLAYER_CURRENT_RULE(TRUE)
	OR MC_serverBD_4.iObjPriority[sObjState.iIndex][GET_LOCAL_PLAYER_TEAM(TRUE)] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	PROCESS_OBJECT_CONTAINER_BEHAVIOUR(sObjState)
		
	IF sObjState.bObjExists
		
		PROCESS_OBJECTIVE_OBJECT_MINIGAMES_EVERY_FRAME_CLIENT(sObjState)
		PROCESS_OBJECTIVE_OBJECT_GOTO_EVERY_FRAME_CLIENT(sObjState)
		
		IF NOT IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_SOMEONE)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetTwo, cibsOBJ2_MinigameObjAttachToPed)
			
			IF MC_playerBD[iLocalPart].iObjHacking = sObjState.iIndex
				PROCESS_GENERIC_OBJ_HACK_BREAKOUTS(sObjState)
				
			ELIF MC_serverBD.iObjHackPart[sObjState.iIndex] = iPartToUse
				// We're not hacking this object any more, but server data hasn't changed yet to say that we aren't
				SET_BIT(iLocalBoolCheck12, LBOOL12_SERVER_STILL_THINKS_IM_HACKING_TEMP)
			ENDIF
			
			IF iSpectatorTarget = -1
				PROCESS_OBJECT_GOTO_RANGE(sObjState)
			ENDIF
			
			IF IS_BIT_SET(iAICarryBitset, sObjState.iIndex)
		
				//-- Dave W, takes a frame or two for the object to be attached. Don't say it's been dropped if it hasn't been attached yet.
				IF IS_BIT_SET(iAIAttachedBitset, sObjState.iIndex)
					INT isublogic = -1
					
					BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_AI_DROP_OBJ,0,GET_LOCAL_PLAYER_TEAM(TRUE),isublogic,INVALID_PLAYER_INDEX(),ci_TARGET_OBJECT, sObjState.iIndex)
					CLEAR_BIT(iAIAttachedBitset, sObjState.iIndex)
					CLEAR_BIT(iAICarryBitset, sObjState.iIndex)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectAttachmentPed > -1
						FMMC_CLEAR_LONG_BIT(MC_serverBD_4.iPedHasAttachedObject, g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectAttachmentPed)
						PRINTLN("RESPAWN_MISSION_OBJ - obj: ", sObjState.iIndex, " no longer attached to ped: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectAttachmentPed)
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
			//-- Attached
			IF IS_BIT_SET(iAICarryBitset, sObjState.iIndex)
				IF NOT IS_BIT_SET(iAIAttachedBitset, sObjState.iIndex)
					SET_BIT(iAIAttachedBitset, sObjState.iIndex)
					PRINTLN("[RCC MISSION] [dsw] Object attached obj =  ", sObjState.iIndex)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_OBJECT_BE_CONSIDERED_OUT_OF_THESE_CRITICAL_BOUNDS(FMMC_OBJECT_STATE& sObjState, INT iBoundsIndex, RULE_BOUNDS_STRUCT& sBoundsStruct)
	
	UNUSED_PARAMETER(sBoundsStruct)
	INT iTeamToCheck = GET_LOCAL_PLAYER_TEAM(TRUE)
	INT iRuleToCheck = GET_LOCAL_PLAYER_CURRENT_RULE(TRUE)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iBoundsCheckSpecifiedTeam > -1
		iTeamToCheck = g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iBoundsCheckSpecifiedTeam
		iRuleToCheck = GET_TEAM_CURRENT_RULE(iTeamToCheck)
	ENDIF
	
	IF NOT ARE_BOUNDS_CRITICAL(iBoundsIndex, iTeamToCheck, iRuleToCheck)
		// These bounds don't matter to objects
		RETURN FALSE
	ENDIF
	
	BOOL bCheckOutside = (g_FMMC_STRUCT.sFMMCEndConditions[iTeamToCheck].sRuleBounds[iRuleToCheck][iBoundsIndex].iRuleBoundsType = ciRULE_BOUNDS_TYPE__LEAVE_AREA)
	
	INT iZone
	FOR iZone = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfZones - 1
		IF IS_THIS_ZONE_SET_AS_BOUNDS_ON_CURRENT_RULE(iZone, iBoundsIndex, iTeamToCheck)
			IF IS_ENTITY_IN_FMMC_ZONE(sObjState.objIndex, iZone, FALSE, FALSE)
				// Object is inside these bounds
				PRINTLN("[Objects][Object ", sObjState.iIndex, "] SHOULD_OBJECT_BE_CONSIDERED_OUT_OF_THESE_BOUNDS | Object is inside Zone ", iZone, ", part of Bounds ", iBoundsIndex, " of Team ", iTeamToCheck, " bCheckOutside = ", BOOL_TO_STRING(bCheckOutside))
				IF !bCheckOutside
					RETURN FALSE
				ENDIF
			ELSE
				PRINTLN("[Objects][Object ", sObjState.iIndex, "] SHOULD_OBJECT_BE_CONSIDERED_OUT_OF_THESE_BOUNDS | Object is OUTSIDE Zone ", iZone, ", part of Bounds ", iBoundsIndex, " of Team ", iTeamToCheck, " bCheckOutside = ", BOOL_TO_STRING(bCheckOutside))
				IF bCheckOutside
					RETURN FALSE
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[Objects][Object ", sObjState.iIndex, "] SHOULD_OBJECT_BE_CONSIDERED_OUT_OF_THESE_BOUNDS | Zone ", iZone, ", is not part of Bounds ", iBoundsIndex, " of Team ", iTeamToCheck)
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

PROC CACHE_IF_OBJECT_IS_OUT_OF_CRITICAL_BOUNDS(FMMC_OBJECT_STATE& sObjState)
	INT iBoundsIndex
	BOOL bCriticalBoundsExist = FALSE
	
	INT iTeamToCheck = GET_LOCAL_PLAYER_TEAM(TRUE)
	INT iRuleToCheck = GET_LOCAL_PLAYER_CURRENT_RULE(TRUE)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iBoundsCheckSpecifiedTeam > -1
		// If a specific team was chosen in the Creator, check that team here
		iTeamToCheck = g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iBoundsCheckSpecifiedTeam
		iRuleToCheck = GET_TEAM_CURRENT_RULE(iTeamToCheck)
	ENDIF
	
	FOR iBoundsIndex = 0 TO ciMAX_RULE_BOUNDS_PER_RULE - 1
		IF NOT ARE_BOUNDS_CRITICAL(iBoundsIndex, iTeamToCheck, iRuleToCheck)
			RELOOP
		ELSE
			bCriticalBoundsExist = TRUE
		ENDIF
		
		IF SHOULD_OBJECT_BE_CONSIDERED_OUT_OF_THESE_CRITICAL_BOUNDS(sObjState, iBoundsIndex, g_FMMC_STRUCT.sFMMCEndConditions[iTeamToCheck].sRuleBounds[iRuleToCheck][iBoundsIndex])
			IF NOT IS_BIT_SET(iObjOnGroundOutOfBoundsBS, sObjState.iIndex)
				PRINTLN("[Objects][Object ", sObjState.iIndex, "] CHECK_IF_OBJECT_IS_OUT_OF_CRITICAL_BOUNDS | Object is currently out of bounds!")
				SET_BIT(iObjOnGroundOutOfBoundsBS, sObjState.iIndex)
			ENDIF
		ELSE
			IF IS_BIT_SET(iObjOnGroundOutOfBoundsBS, sObjState.iIndex)
				PRINTLN("[Objects][Object ", sObjState.iIndex, "] CHECK_IF_OBJECT_IS_OUT_OF_CRITICAL_BOUNDS | Object is back in bounds!")
				CLEAR_BIT(iObjOnGroundOutOfBoundsBS, sObjState.iIndex)
			ENDIF
		ENDIF
	ENDFOR
	
	IF NOT bCriticalBoundsExist
		IF IS_BIT_SET(iObjOnGroundOutOfBoundsBS, sObjState.iIndex)
			PRINTLN("[Objects][Object ", sObjState.iIndex, "] CHECK_IF_OBJECT_IS_OUT_OF_CRITICAL_BOUNDS | Clearing iObjOnGroundOutOfBoundsBS for this object because there are no critical bounds!")
			CLEAR_BIT(iObjOnGroundOutOfBoundsBS, sObjState.iIndex)
		ENDIF
	ENDIF
ENDPROC

PROC SET_OBJECT_TO_USE_FOR_DELIVERY_OVERRIDES(INT iObj)
	IF iObjDeliveryOverridesObjToUse != iObj
		PRINTLN("[Objects][Object ", iObjDeliveryOverridesObjToUse, "][Object ", iObj, "] SET_OBJECT_TO_USE_FOR_DELIVERY_OVERRIDES | Setting iObjDeliveryOverridesObjToUse to ", iObj)
		iObjDeliveryOverridesObjToUse = iObj
	ENDIF
ENDPROC

PROC ASSIGN_CARRIED_OBJECT_TO_USE_FOR_DELIVERY_OVERRIDES()
	INT iObj
	FOR iObj = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1
	
		IF NOT IS_BIT_SET(iCachedCarriedObjectsBS, iObj)
			// This object isn't carried
			RELOOP
		ENDIF
		
		IF IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vObjDropOffOverride)
			// This object has no overrides
			RELOOP
		ENDIF
		
		SET_OBJECT_TO_USE_FOR_DELIVERY_OVERRIDES(iObj)
		EXIT
	ENDFOR
	
	SET_OBJECT_TO_USE_FOR_DELIVERY_OVERRIDES(-1)
ENDPROC

PROC PROCESS_OBJECT_CARRIED_VARS(FMMC_OBJECT_STATE& sObjState)

	IF IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_LOCAL_PLAYER)
		IF NOT IS_BIT_SET(iCachedCarriedObjectsBS, sObjState.iIndex)
			PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_CARRIED_VARS | Setting iCachedCarriedObjectsBS for object ", sObjState.iIndex)
			SET_BIT(iCachedCarriedObjectsBS, sObjState.iIndex)
			ASSIGN_CARRIED_OBJECT_TO_USE_FOR_DELIVERY_OVERRIDES()
			
			IF IS_BIT_SET(iObjOnGroundOutOfBoundsBS, sObjState.iIndex)
				PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_CARRIED_VARS | Clearing iObjOnGroundOutOfBoundsBS because this object is no longer on the ground!")
				CLEAR_BIT(iObjOnGroundOutOfBoundsBS, sObjState.iIndex)
			ENDIF
			PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_CARRIED_VARS | Sending g_ciInstancedcontentEventType_PickedUpObject event for object ", sObjState.iIndex)
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_PickedUpObject, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE, sObjState.iIndex)
		ENDIF
		
		iMyObjectCarryCount++
		
	ELSE
		IF IS_BIT_SET(iCachedCarriedObjectsBS, sObjState.iIndex)
			PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_CARRIED_VARS | Clearing iCachedCarriedObjectsBS for object ", sObjState.iIndex)
			CLEAR_BIT(iCachedCarriedObjectsBS, sObjState.iIndex)
			IF iObjDeliveryOverridesObjToUse = sObjState.iIndex
				ASSIGN_CARRIED_OBJECT_TO_USE_FOR_DELIVERY_OVERRIDES()
			ENDIF
			SET_OBJECT_NEEDS_TO_RE_CACHE_OUT_OF_BOUNDS(sObjState.iIndex)
			
			IF NOT bLocalPlayerPedOk
				// I was holding this object but dropped it just now due to dying!
				PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_CARRIED_VARS | Sending FMMCObjectDroppedOnDeath event for object ", sObjState.iIndex)
				BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_FMMCObjectDroppedOnDeath, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE, sObjState.iIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iSentObjectCollectionTickerBS, sObjState.iIndex)
			CLEAR_BIT(iSentObjectCollectionTickerBS, sObjState.iIndex)
			PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_CARRIED_VARS | Clearing iSentObjectCollectionTickerBS")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_SOMEONE)
		IF NOT IS_BIT_SET(iObjHasBeenCarriedAtSomePointBS, sObjState.iIndex)
			SET_BIT(iObjHasBeenCarriedAtSomePointBS, sObjState.iIndex)
			PRINTLN("[RCC MISSION][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_EVERY_FRAME_CLIENT | Setting iObjHasBeenCarriedAtSomePointBS for Object ", sObjState.iIndex)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_OBJECT_TRAIN_ATTACHMENT_EVERY_FRAME(FMMC_OBJECT_STATE& sObjState)
	
	IF NOT sObjState.bObjExists
	OR NOT sObjState.bHaveControlOfObj
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].sObjectTrainAttachmentData.iTrainIndex = -1
		// This object isn't set up to be attached to a train in any way
		EXIT
	ENDIF
	
	IF NOT SHOULD_PLACED_OBJECT_BE_PORTABLE(sObjState.iIndex)
		PROCESS_TRAIN_ATTACHMENT(sObjState.objIndex, g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].sObjectTrainAttachmentData, MC_serverBD.iObjHackPart[sObjState.iIndex] = -1)
		
	ELSE
		// Portable objects are handled in the staggered version
	ENDIF
	
ENDPROC

PROC PROCESS_OBJECT_MODEL_SPECIFIC_FUNCTIONALITY(FMMC_OBJECT_STATE& sObjState)
	
	IF sObjState.mn = INT_TO_ENUM(MODEL_NAMES, HASH("imp_Prop_ImpExp_BoxCoke_01"))
		IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_PARTICLE_FX_OVERRIDE_COCAINE)
			IF sObjState.bObjExists
				IF sObjState.bHaveControlOfObj
					IF sObjState.bObjBroken
						IF GET_ENTITY_HEALTH(sObjState.objIndex) > 0
							SET_ENTITY_HEALTH(sObjState.objIndex, 0)
							PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_MODEL_SPECIFIC_FUNCTIONALITY OBJECT = imp_Prop_ImpExp_BoxCoke_01 ... HAS_OBJECT_BEEN_BROKEN = TRUE - Setting health to 0!")
						ENDIF
					ENDIF
				ELSE
					SET_BIT(iCocaineParticleEffectBitset, sObjState.iIndex)
				ENDIF
				
			ELSE
				IF IS_BIT_SET(iCocaineParticleEffectBitset, sObjState.iIndex)
					USE_PARTICLE_FX_ASSET("scr_ie_svm_technical2")
					START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_dst_cocaine", g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].vPos, <<0.0, 0.0, 0.0>>, 1.5)
					CLEAR_BIT(iCocaineParticleEffectBitset, sObjState.iIndex)
				ENDIF
			ENDIF
		ENDIF
		
	ELIF sObjState.mn = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_hdsec"))
		IF sObjState.bObjExists
			IF GET_ENTITY_HEIGHT_ABOVE_GROUND(sObjState.objIndex) < 0.2
			AND GET_ENTITY_HEIGHT_ABOVE_GROUND(sObjState.objIndex) > 0
				IF NOT IS_BIT_SET(iLocalObjectLightReadyForUpdate, sObjState.iIndex)
					SET_BIT(iLocalObjectLightReadyForUpdate, sObjState.iIndex)
					PRINTLN("[RCC MISSION][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_MODEL_SPECIFIC_FUNCTIONALITY | Setting ready bit for object ", sObjState.iIndex)
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalObjectLightUpdated, sObjState.iIndex)
				IF IS_BIT_SET(iLocalObjectLightReadyForUpdate, sObjState.iIndex)
					PRINTLN("[RCC MISSION][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_MODEL_SPECIFIC_FUNCTIONALITY | Updating lights for Sinking bomb ", sObjState.iIndex)
					UPDATE_LIGHTS_ON_ENTITY(sObjState.objIndex)
					SET_BIT(iLocalObjectLightUpdated, sObjState.iIndex)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_OBJECT_VISIBILITY(FMMC_OBJECT_STATE& sObjState)

	IF NOT sObjState.bObjExists
		EXIT
	ENDIF
	
	IF SHOULD_OBJECT_BE_INVISIBLE(sObjState.iIndex)
		IF sObjState.bHaveControlOfObj
		AND IS_ENTITY_VISIBLE(sObjState.objIndex)
			SET_ENTITY_VISIBLE(sObjState.objIndex, FALSE)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_OBJECT_LOD_DIST(FMMC_OBJECT_STATE& sObjState)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjLodDist = -1
		EXIT
	ENDIF
	
	IF GET_ENTITY_LOD_DIST(sObjState.objIndex) != g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjLodDist
		SET_ENTITY_LOD_DIST(sObjState.objIndex, g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjLodDist)
		PRINTLN("[RCC MISSION][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_LOD_DIST | Setting LOD distance of Object to ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjLodDist)
	ENDIF
ENDPROC

PROC PROCESS_OBJECT_ORBITAL_CANNON_BAIL(FMMC_OBJECT_STATE& sObjState)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iOrbitalCannonIndex = -1
		EXIT
	ENDIF
	
	IF NOT sObjState.bObjExists
		IF g_iOrbitalCannonObjectInUse = sObjState.iIndex
			g_bMissionPlacedOrbitalCannon = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC FILL_ELECTRONIC_PARAMS_FROM_OBJ(FMMC_OBJECT_STATE& sObjState, ELECTRONIC_PARAMS& sElectronicParams)	
	sElectronicParams.iIndex = sObjState.iIndex
	sElectronicParams.oiElectronic = sObjState.objIndex
	sElectronicParams.biElectronicBlip = biObjBlip[sObjState.iIndex]
	sElectronicParams.eEntityType = FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
	IF sObjState.bObjExists
		sElectronicParams.vElectronicCoords = GET_FMMC_OBJECT_COORDS(sObjState)	
	ENDIF	
	sElectronicParams.iCamIndex = iObjCachedCamIndexes[sObjState.iIndex]
ENDPROC

PROC PROCESS_OBJ_EVERY_FRAME_CLIENT(INT iObj)
	
	FMMC_OBJECT_STATE sObjState
	FILL_FMMC_OBJECT_STATE_STRUCT(sObjState, iObj)
	FILL_ELECTRONIC_PARAMS_FROM_OBJ(sObjState, sObjState.sObjElectronicParams)	
	
	PROCESS_FMMC_OBJECT_DRONE_SYSTEMS(sObjState, g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].sObjDroneData)
	
	PROCESS_SETTING_OBJ_NEAR(sObjState)
	
	PROCESS_TASERED_ENTITY(sObjTaserVars, g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].sObjElectronicData, sObjState.sObjElectronicParams)
	
	PROCESS_MISSION_FUSEBOX_OBJECT(sObjState)
			
	PROCESS_OBJECT_RULE_VISIBILITY(iObj)
	
	PROCESS_WARP_ON_RULE_DATA_FOR_ENTITY(CREATION_TYPE_OBJECTS, sObjState.iIndex, iObjectShouldWarpThisRuleStart, iObjectWarpedOnThisRule)
	
	PROCESS_OBJ_SELF_DESTRUCT_SEQUENCE(iObj)
	
	PROCESS_OBJ_CAPTURE_BLIP(biObjBlip[iObj], sObjState)
	
	PROCESS_FLAG_RETURN(sObjState)
	
	PROCESS_OBJECT_CAPTURE(sObjState)
	
	PROCESS_OBJ_PHOTO_OBJECTIVE_EVERY_FRAME(sObjState)
	
	PROCESS_OBJECT_TRANSFORMATIONS(sObjState)
	
	PROCESS_OBJECT_INVENTORIES(sObjState)
	
	PROCESS_CCTV_MOVING_CAMERAS(sObjState.sObjElectronicParams, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjElectronicData.sCCTVData, iObjCachedCamIndexes[sObjState.iIndex], sObjState.bObjExists, sObjState.bObjAlive, sObjState.bHaveControlOfObj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iAggroIndexBS_Entity_Obj)
	
	PROCESS_OBJECT_TAGGING(sObjState)
	
	PROCESS_OBJECT_EXPLODE_FROM_NEARBY_EXPLOSION(sObjState)
	
	PROCESS_SEA_MINE_BEHAVIOUR(sObjState)
	
	PROCESS_PARTICLE_FX_OVERRIDE(sObjState.iIndex)
	
	CLIENT_MANAGE_FRAG_CRATE_PICKUPS(sObjState.iIndex)
	
	CLIENT_MANAGE_SAFE_CRACK_ANIM(sObjState.iIndex, sObjState.objIndex)
	
	PROCESS_LEGACY_HACKING_CLEANUPS(sObjState)
	
	PROCESS_OBJECT_TO_PICK_UP_ASAP(sObjState)
	
	PROCESS_PREVENTING_OBJECT_PICKUP(sObjState)
	
	PROCESS_OBJECT_TRAIN_ATTACHMENT_EVERY_FRAME(sObjState)
	
	PROCESS_OBJECTIVE_OBJECT_EVERY_FRAME_CLIENT(sObjState)
	
	PROCESS_OBJECT_TEAM_BASED_CONTROL_CLIENT(sObjState)
	
	PROCESS_OBJECT_CARRIED_VARS(sObjState)
				
	PROCESS_OBJECT_HUD(sObjState)
	
	PROCESS_OBJECT_MODEL_SPECIFIC_FUNCTIONALITY(sObjState)
	
	PROCESS_OBJECT_VISIBILITY(sObjState)
	
	PROCESS_KEEPING_OBJECT_IN_DROP_OFF(sObjState)
	
	PROCESS_OBJECT_PREREQS(sObjState)
	
	PROCESS_OBJECT_PREREQS_NO_RESET(sObjState)

	PROCESS_ENTITY_AUDIO(sObjState.iIndex, sObjState.objIndex, sObjState.bObjAlive, CREATION_TYPE_OBJECTS)
	
	#IF IS_DEBUG_BUILD
	IF sObjState.bObjExists
		iTempProfilerActiveObjects++
	ENDIF
	
	PROCESS_OBJECT_DEBUG(sObjState)
	#ENDIF
ENDPROC

PROC PROCESS_OBJ_STEALING_SERVER(FMMC_OBJECT_STATE& sObjState)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_EnableObjectStealing)
		EXIT
	ENDIF
	
	IF g_bMissionEnding
		EXIT
	ENDIF
	
	IF ((MC_serverBD.iObjCarrier[sObjState.iIndex] != -1
	AND NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(MC_serverBD.iObjCarrier[sObjState.iIndex])))
	OR (MC_serverBD_1.iPTLDroppedPackageOwner[sObjState.iIndex] != -1 
	AND NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(MC_serverBD_1.iPTLDroppedPackageOwner[sObjState.iIndex]))))
		IF NOT IS_PICKUP_AT_ANY_HOLDING(sObjState.iIndex)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(sObjState.objIndex)
				SET_ENTITY_COORDS(sObjState.objIndex, g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].vPos)
				SET_ENTITY_VISIBLE(sObjState.objIndex, TRUE)
				SET_ENTITY_COLLISION(sObjState.objIndex, TRUE)
				MC_serverBD.iObjCarrier[sObjState.iIndex] = -1
				MC_serverBD_1.iPTLDroppedPackageOwner[sObjState.iIndex] = -1
				PRINTLN("[Objects] PROCESS_OBJ_STEALING_SERVER | Moving pickup owned by inactive player back to position!")
			ELSE
				NETWORK_REQUEST_CONTROL_OF_ENTITY(sObjState.objIndex)
				PRINTLN("[Objects] PROCESS_OBJ_STEALING_SERVER | Requesting control of object!")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_OBJECT_TEAM_BASED_CONTROL_SERVER(FMMC_OBJECT_STATE& sObjState)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
		EXIT
	ENDIF

	PRINTLN("[Objects][CONTROL OBJECT] PROCESS_OBJECT_TEAM_BASED_CONTROL_SERVER | - MC_serverBD.iObjTeamCarrier: ",MC_serverBD.iObjTeamCarrier)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetTwo, cibsOBJ2_TeamControlBarObject)
		INT iTeamO
		FOR iTeamO = 0 TO FMMC_MAX_TEAMS-1		
			IF iTeamO != MC_serverBD.iObjTeamCarrier
				PRINTLN("[Objects][CONTROL OBJECT] PROCESS_OBJECT_TEAM_BASED_CONTROL_SERVER | - iTeamO: ", iTeamO, " Does not equal to the carrier.")
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
					IF HAS_NET_TIMER_STARTED(MC_serverBD.tdControlObjTeamTimer[iTeamO])
						
						PRINTLN("[Objects][CONTROL OBJECT] PROCESS_OBJECT_TEAM_BASED_CONTROL_SERVER | - iTeamO: ", iTeamO, " Timer has started and is no longer the carrier, so we need to pause its progress.")
						IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_CONTROL_TIME_STOPPED_T0 + iTeamO)
							MC_serverBD.iTimeObjectTeamControlBarCache[iTeamO] = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlObjTeamTimer[iTeamO])
							SET_BIT(MC_serverBD.iServerBitSet6, SBBOOL6_CONTROL_TIME_STOPPED_T0 + iTeamO)
							PRINTLN("[Objects][CONTROL OBJECT] PROCESS_OBJECT_TEAM_BASED_CONTROL_SERVER | - iTeamO: ", iTeamO, " Cached MC_serverBD.iTimeObjectTeamControlBarCache: ", MC_serverBD.iTimeObjectTeamControlBarCache[iTeamO])
						ENDIF
					ELSE										
						IF IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_CONTROL_TIME_STOPPED_T0 + iTeamO)
							CLEAR_BIT(MC_serverBD.iServerBitSet6, SBBOOL6_CONTROL_TIME_STOPPED_T0 + iTeamO)
						ENDIF
					ENDIF
				ENDIF
			ELSE								
				IF IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_CONTROL_TIME_STOPPED_T0 + iTeamO)
					CLEAR_BIT(MC_serverBD.iServerBitSet6, SBBOOL6_CONTROL_TIME_STOPPED_T0 + iTeamO)
					
					PRINTLN("[Objects][CONTROL OBJECT] PROCESS_OBJECT_TEAM_BASED_CONTROL_SERVER | - Starting the Timer Back Up for iTeamO: ", iTeamO, " Cached MC_serverBD.iTimeObjectTeamControlBarCache: ", MC_serverBD.iTimeObjectTeamControlBarCache[iTeamO])
					PRINTLN("[Objects][CONTROL OBJECT] PROCESS_OBJECT_TEAM_BASED_CONTROL_SERVER | - Before: GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlObjTeamTimer[iTeamO]): ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlObjTeamTimer[iTeamO]))
					
					INT iTimeOffset = (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlObjTeamTimer[iTeamO]) - MC_serverBD.iTimeObjectTeamControlBarCache[iTeamO])
					INT iNewTime = (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlObjTeamTimer[iTeamO]) - iTimeOffset)
					
					PRINTLN("[Objects][CONTROL OBJECT] PROCESS_OBJECT_TEAM_BASED_CONTROL_SERVER | - custom iTimeOffset: ", iTimeOffset)
					PRINTLN("[Objects][CONTROL OBJECT] PROCESS_OBJECT_TEAM_BASED_CONTROL_SERVER | - custom iNewTime: ", iNewTime)
					
					MC_serverBD.tdControlObjTeamTimer[iTeamO].Timer = GET_TIME_OFFSET(MC_serverBD.tdControlObjTeamTimer[iTeamO].Timer, iTimeOffset)
					MC_serverBD.tdControlObjTeamTimer[iTeamO].bInitialisedTimer = TRUE
					
					PRINTLN("[Objects][CONTROL OBJECT] PROCESS_OBJECT_TEAM_BASED_CONTROL_SERVER | - After: GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlObjTeamTimer[iTeamO]): ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlObjTeamTimer[iTeamO]))
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC PROCESS_OBJ_EVERY_FRAME_SERVER(INT iObj)
		
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	FMMC_OBJECT_STATE sObjState
	FILL_FMMC_OBJECT_STATE_STRUCT(sObjState, iObj)
	
	PROCESS_OBJ_STEALING_SERVER(sObjState)
	
	PROCESS_OBJECT_TEAM_BASED_CONTROL_SERVER(sObjState)
	
	PROCESS_DESTRUCTIBLE_CRATES_SERVER(sObjState)
			
	IF sObjState.bObjExists
		
		PROCESS_OBJECT_CAPTURE_SERVER(sObjState)
		
		IF IS_BIT_SET(MC_serverBD.iObjCleanup_NeedOwnershipBS, iobj)
			IF NOT sObjState.bHaveControlOfObj
				IF NOT bIsAnySpectator
					NETWORK_REQUEST_CONTROL_OF_ENTITY(sObjState.objIndex)
					PRINTLN("[RCC MISSION] PROCESS_OBJ_EVERY_FRAME_SERVER - Requesting control of object ", iObj, " for cleanup")
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_OBJ_EVERY_FRAME_SERVER - Can't request control of entity as you are a spectator")
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] PROCESS_OBJ_EVERY_FRAME_SERVER - Deleting object ", iObj)
				DELETE_FMMC_OBJECT(iobj)
			ENDIF
			
			IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iRespawnOnRule, -1)
				IF NOT sObjState.bHaveControlOfObj
					PRINTLN("[RCC MISSION] PROCESS_OBJ_EVERY_FRAME_SERVER - Requesting control of obj ", iObj, " for cleanup - for delete")
					NETWORK_REQUEST_CONTROL_OF_ENTITY(sObjState.objIndex)
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_OBJ_EVERY_FRAME_SERVER - Deleting object ", iObj)
					DELETE_FMMC_OBJECT(iObj)
				ENDIF
				
				SERVER_CLEAR_OBJ_AT_HOLDING(iObj)
			ENDIF
			
		ELIF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
			IF CLEANUP_OBJ_EARLY(sObjState)
				PRINTLN("[RCC MISSION] PROCESS_OBJ_EVERY_FRAME_SERVER - Object ", sObjState.iIndex, " has cleaned up early, new priority/midpoint check")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ObjectDestroyedTracking)
		AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iContinuityId > -1
			IF NOT sObjState.bObjAlive
			OR sObjState.bObjBroken
				IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iObjectDestroyedBitset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iContinuityId)
					FMMC_SET_LONG_BIT(MC_serverBD_1.sMissionContinuityVars.iObjectDestroyedBitset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iContinuityId)
					PRINTLN("[JS][CONTINUITY] PROCESS_OBJ_EVERY_FRAME_SERVER | obj ", sObjState.iIndex, " with continuityID ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iContinuityId, " was destroyed")
				ENDIF
			ENDIF
		ENDIF
				
	ELSE
		IF IS_BIT_SET(MC_serverBD.iObjCleanup_NeedOwnershipBS, sObjState.iIndex)
			PRINTLN("[RCC MISSION] PROCESS_OBJ_EVERY_FRAME_SERVER | Obj ", sObjState.iIndex, " we were trying to request ownership of for cleanup no longer exists!")
			CLEAR_BIT(MC_serverBD.iObjCleanup_NeedOwnershipBS, sObjState.iIndex)
		ENDIF		
	ENDIF
	
	PROCESS_OBJ_RESPAWNING(sObjState)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Object Staggered Processing
// ##### Description: Client and Server Object Staggered Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_OBJ_PRE_STAGGERED()

	INT iTeam
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		iNumHighPriorityObjHeld[iTeam] = 0
	ENDFOR
	
ENDPROC

FUNC BOOL IS_OBJ_CURRENT_POSITION_TOO_CLOSE_TO_PREVIOUS_SAFE_POINT_TO_RECACHE(FMMC_OBJECT_STATE& sObjState)

	IF sWarpPortal.iJustUsedPortalStartBS != 0
		RETURN FALSE
	ENDIF

	RETURN VDIST2(GET_FMMC_OBJECT_COORDS(sObjState), vObjectLastSafeDropPoint[sObjState.iIndex][0]) < cfObjectSafeDropPointRequiredDistance
ENDFUNC

FUNC BOOL IS_OBJ_CURRENT_POSITION_SAFE_TO_CACHE(FMMC_OBJECT_STATE& sObjState)

	IF IS_VECTOR_ZERO(vObjectLastSafeDropPoint[sObjState.iIndex][0])
		// Cache the object's position as it's probably just spawned and will be in a safe position
		RETURN TRUE
	ENDIF
	
	VECTOR vCoordsToCheck = GET_FMMC_OBJECT_COORDS(sObjState)
	
	IF VDIST2(vObjectLastDangerousDropPoint[sObjState.iIndex], vCoordsToCheck) < POW(cfObjectDangerousDropPointRequiredDistance, 2.0)
		PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] IS_OBJ_CURRENT_POSITION_SAFE_TO_CACHE | Not far enough away from coords that were considered dangerous!")
		RETURN FALSE
	ENDIF
		
	IF IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_SOMEONE)
		PED_INDEX piCarrierPed = GET_PED_CARRYING_OBJECT(sObjState.objIndex)
		PARTICIPANT_INDEX piCarrierPart = GET_PARTICIPANT_CARRYING_OBJECT(sObjState.objIndex)
		INT iCarrierPart = NATIVE_TO_INT(piCarrierPart)
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iBoundsCheckSpecifiedTeam > -1
			IF MC_playerBD[iCarrierPart].iTeam != g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iBoundsCheckSpecifiedTeam
				PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] IS_OBJ_CURRENT_POSITION_SAFE_TO_CACHE | Carrier (Participant ", iCarrierPart, ") isn't on the team the object is looking for!")
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF IS_PARTICIPANT_OUT_OF_BOUNDS(iCarrierPart, ciRULE_BOUNDS_INDEX__ANY, TRUE)
			PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] IS_OBJ_CURRENT_POSITION_SAFE_TO_CACHE | Carrier (Participant ", iCarrierPart, ") is out of bounds!")
			RETURN FALSE
		ENDIF
		
		IF iCarrierPart = iLocalPart
			IF IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__KILL_ZONE)
			OR IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__EXPLODE_PLAYER)
			OR IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__EXPLOSION_AREA)
				PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] IS_OBJ_CURRENT_POSITION_SAFE_TO_CACHE | Carrier (Participant ", iCarrierPart, ") is in a dangerous Zone type!")
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(piCarrierPed)
			VEHICLE_INDEX viCarrierVehicle = GET_VEHICLE_PED_IS_USING(piCarrierPed)
			
			IF DOES_ENTITY_EXIST(viCarrierVehicle)
				IF IS_ENTITY_IN_WATER(viCarrierVehicle)
					PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] IS_OBJ_CURRENT_POSITION_SAFE_TO_CACHE | Carrier (Participant ", iCarrierPart, ")'s vehicle is in water!")
					RETURN FALSE
				ENDIF
				
				IF NOT IS_VEHICLE_ON_ALL_WHEELS(viCarrierVehicle)
					PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] IS_OBJ_CURRENT_POSITION_SAFE_TO_CACHE | Carrier (Participant ", iCarrierPart, ") isn't on all wheels!")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_SWIMMING(piCarrierPed)
		OR IS_PED_SWIMMING_UNDER_WATER(piCarrierPed)
			PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] IS_OBJ_CURRENT_POSITION_SAFE_TO_CACHE | Carrier (Participant ", iCarrierPart, ") is swimming!")
			RETURN FALSE
		ENDIF
		
		IF IS_PED_IN_ANY_BOAT(piCarrierPed)
		OR IS_PED_IN_ANY_SUB(piCarrierPed)
			PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] IS_OBJ_CURRENT_POSITION_SAFE_TO_CACHE | Carrier (Participant ", iCarrierPart, ") is in a water vehicle!")
			RETURN FALSE
		ENDIF
		
		IF IS_PED_IN_FLYING_VEHICLE(piCarrierPed)
			PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] IS_OBJ_CURRENT_POSITION_SAFE_TO_CACHE | Carrier (Participant ", iCarrierPart, ") is in a flying vehicle!")
			RETURN FALSE
		ENDIF
		
		IF IS_PED_ON_VEHICLE(piCarrierPed)
			PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] IS_OBJ_CURRENT_POSITION_SAFE_TO_CACHE | Carrier (Participant ", iCarrierPart, ") is standing on a vehicle!")
			RETURN FALSE
		ENDIF
		
		IF IS_ENTITY_IN_AIR(piCarrierPed)
			PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] IS_OBJ_CURRENT_POSITION_SAFE_TO_CACHE | Carrier (Participant ", iCarrierPart, ") is in the air!")
			RETURN FALSE
		ENDIF
		
		IF IS_PED_CLIMBING(piCarrierPed)
			PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] IS_OBJ_CURRENT_POSITION_SAFE_TO_CACHE | Carrier (Participant ", iCarrierPart, ") is climbing!")
			RETURN FALSE
		ENDIF
		
		IF IS_PED_FALLING(piCarrierPed)
			PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] IS_OBJ_CURRENT_POSITION_SAFE_TO_CACHE | Carrier (Participant ", iCarrierPart, ") is falling!")
			RETURN FALSE
		ENDIF
		
		IF IS_PED_LANDING(piCarrierPed)
			PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] IS_OBJ_CURRENT_POSITION_SAFE_TO_CACHE | Carrier (Participant ", iCarrierPart, ") is landing!")
			RETURN FALSE
		ENDIF
		
	ELSE
		IF IS_ENTITY_IN_AIR(sObjState.objIndex)
			PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] IS_OBJ_CURRENT_POSITION_SAFE_TO_CACHE | Object is in the air!")
			RETURN FALSE
		ENDIF
		
		IF IS_ENTITY_IN_WATER(sObjState.objIndex)
			PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] IS_OBJ_CURRENT_POSITION_SAFE_TO_CACHE | Object is in water!")
			RETURN FALSE
		ENDIF
	ENDIF
	
	FLOAT fGroundZ
	VECTOR vGroundNormal
	IF GET_GROUND_Z_AND_NORMAL_FOR_3D_COORD(vCoordsToCheck, fGroundZ, vGroundNormal)
	
		vCoordsToCheck.z = fGroundZ
		
		#IF IS_DEBUG_BUILD
		IF bObjectLastSafeDropPointDebug
			DRAW_DEBUG_LINE(vCoordsToCheck, vCoordsToCheck + vGroundNormal, 0, 255, 0)
		ENDIF
		#ENDIF
		
		IF vGroundNormal.z < 0.7
		OR ABSF(vGroundNormal.x) > 0.35
		OR ABSF(vGroundNormal.y) > 0.35
			PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] IS_OBJ_CURRENT_POSITION_SAFE_TO_CACHE | This area isn't flat enough!")
			RETURN FALSE
		ENDIF
		
		FLOAT fWaterHeight
		FLOAT fUnsafeDistanceFromWater = 5.0
		IF GET_WATER_HEIGHT(vCoordsToCheck, fWaterHeight)
			#IF IS_DEBUG_BUILD
			IF bObjectLastSafeDropPointDebug
				VECTOR vWaterCoords = vCoordsToCheck
				vWaterCoords.z = fWaterHeight
				DRAW_DEBUG_LINE(vWaterCoords, vWaterCoords + vGroundNormal, 0, 0, 255)
			ENDIF
			#ENDIF
		
			IF fGroundZ < (fWaterHeight + fUnsafeDistanceFromWater)
				PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] IS_OBJ_CURRENT_POSITION_SAFE_TO_CACHE | Too close to water!")
				RETURN FALSE
			ENDIF
		ENDIF
		
	ELSE
		PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] IS_OBJ_CURRENT_POSITION_SAFE_TO_CACHE | Couldn't get ground Z!")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_OBJECT_RETURN_TO_LAST_SAFE_DROP_POSITION(FMMC_OBJECT_STATE& sObjState)
	
	#IF IS_DEBUG_BUILD
	IF bObjectLastSafeDropPointDebug
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5)
			PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] SHOULD_OBJECT_RETURN_TO_LAST_SAFE_DROP_POSITION | Returning due to debug!")
			RETURN TRUE
		ENDIF
	ENDIF
	#ENDIF
	
	IF IS_VECTOR_ZERO(vObjectLastSafeDropPoint[sObjState.iIndex][0])
		// We don't have a safe point to return to yet!
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_SOMEONE)
		//PRINTLN("[ObjectSafePoint][Objects][Object ", sObjState.iIndex, "] SHOULD_OBJECT_RETURN_TO_LAST_SAFE_DROP_POSITION | Obj ", sObjState.iIndex, " someone is carrying this object!")
		RETURN FALSE
	ENDIF

	IF IS_OBJ_OUT_OF_BOUNDS(sObjState.iIndex)
		PRINTLN("[ObjectSafePoint][Objects][Object ", sObjState.iIndex, "] SHOULD_OBJECT_RETURN_TO_LAST_SAFE_DROP_POSITION | Obj ", sObjState.iIndex, " is out of bounds!")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iObjectInDangerousPositionBS, sObjState.iIndex)
		PRINTLN("[ObjectSafePoint][Objects][Object ", sObjState.iIndex, "] SHOULD_OBJECT_RETURN_TO_LAST_SAFE_DROP_POSITION | Obj ", sObjState.iIndex, " is in a dangerous position according to iObjectInDangerousPositionBS!")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MOVE_OBJECT_TO_LAST_SAFE_DROP_POSITION(FMMC_OBJECT_STATE& sObjState)

	VECTOR vSafePoint = vObjectLastSafeDropPoint[sObjState.iIndex][ciObjectSafeDropPoints - 1]
	PRINTLN("[ObjectSafePoint][Objects][Object ", sObjState.iIndex, "] MOVE_OBJECT_TO_LAST_SAFE_DROP_POSITION | Moving object ", sObjState.iIndex, " back to its last safe point at ", vSafePoint)
	
	IF IS_ENTITY_ATTACHED(sObjState.objIndex)
		PRINTLN("[ObjectSafePoint][Objects][Object ", sObjState.iIndex, "] MOVE_OBJECT_TO_LAST_SAFE_DROP_POSITION | Detaching object ", sObjState.iIndex, " so we can move it to its last safe point")
		DETACH_ENTITY(sObjState.objIndex, FALSE)
	ENDIF
	
	SET_ENTITY_COORDS(sObjState.objIndex, vSafePoint)
	IF GET_INTERIOR_AT_COORDS(vSafePoint) = NULL
		PRINTLN("[ObjectSafePoint][Objects][Object ", sObjState.iIndex, "] MOVE_OBJECT_TO_LAST_SAFE_DROP_POSITION | Object is not located in an interior so calling PLACE_OBJECT_ON_GROUND_PROPERLY")
		PLACE_OBJECT_ON_GROUND_PROPERLY(sObjState.objIndex)
	ELSE
		PRINTLN("[ObjectSafePoint][Objects][Object ", sObjState.iIndex, "] MOVE_OBJECT_TO_LAST_SAFE_DROP_POSITION | Object is not located in an interior so NOT calling PLACE_OBJECT_ON_GROUND_PROPERLY")
	ENDIF
	
	CLEAR_BIT(iObjOnGroundOutOfBoundsBS, sObjState.iIndex)
	CLEAR_BIT(iObjectInDangerousPositionBS, sObjState.iIndex)
	BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ObjectReturnedToSafePosition, DEFAULT, DEFAULT, sObjState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, vSafePoint.x, vSafePoint.y, vSafePoint.z)
	
ENDPROC

PROC PROCESS_OBJECT_TRAIN_ATTACHMENT_STAGGERED(FMMC_OBJECT_STATE& sObjState)

	IF NOT sObjState.bObjExists
	OR NOT sObjState.bHaveControlOfObj
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].sObjectTrainAttachmentData.iTrainIndex = -1
		// This object isn't set up to be attached to a train in any way
		EXIT
	ENDIF
	
	IF NOT SHOULD_PLACED_OBJECT_BE_PORTABLE(sObjState.iIndex)
		// Standard objects are handled in the every-frame version
		
	ELSE
		
		IF IS_BIT_SET(iObjHasBeenCarriedAtSomePointBS, sObjState.iIndex)
			// This portable object is no longer on the train
			EXIT
		ENDIF
		
		VECTOR vCurrentCoords = GET_FMMC_OBJECT_COORDS(sObjState)
		VECTOR vCurrentRotation = GET_ENTITY_ROTATION(sObjState.objIndex)
		
		VECTOR vDesiredCoords
		VECTOR vDesiredRotation
		MC_GET_TRAIN_ATTACHMENT_POINT_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].sObjectTrainAttachmentData, vDesiredCoords, vDesiredRotation)
		
		IF NOT ARE_VECTORS_EQUAL(vCurrentCoords, vDesiredCoords)
			SET_ENTITY_COORDS(sObjState.objIndex, vDesiredCoords, FALSE)
			PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_TRAIN_ATTACHMENT_STAGGERED | Moving portable object to desired coords! vCurrentCoords: ", vCurrentCoords, " / vDesiredCoords: ", vDesiredCoords)
		ENDIF
		
		IF NOT ARE_ROTATIONS_ALMOST_EQUAL(vCurrentRotation, vDesiredRotation, 0.1)
			SET_ENTITY_ROTATION(sObjState.objIndex, vDesiredRotation, DEFAULT, FALSE)
			PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_TRAIN_ATTACHMENT_STAGGERED | Setting portable object to desired rotation! vCurrentRotation: ", vCurrentRotation, " / vDesiredRotation: ", vDesiredRotation)
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_OBJECT_TRY_TO_CACHE_SAFE_POSITION_NOW(FMMC_OBJECT_STATE& sObjState)

	IF HAS_NET_TIMER_STARTED(stObjectReturnToSafeDropPointCooldownTimer[sObjState.iIndex])
	AND NOT HAS_NET_TIMER_EXPIRED(stObjectReturnToSafeDropPointCooldownTimer[sObjState.iIndex], ciObjectReturnToSafeDropPointCooldownTimer_Length)
		PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] SHOULD_OBJECT_TRY_TO_CACHE_SAFE_POSITION_NOW | Obj ", sObjState.iIndex, " waiting for cooldown timer!!")
		RETURN FALSE
	ENDIF
	
	IF IS_OBJ_CURRENT_POSITION_TOO_CLOSE_TO_PREVIOUS_SAFE_POINT_TO_RECACHE(sObjState)
		// Too close to last safe point! Don't try to cache a new position yet
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_COLLISION_LOADED_AROUND_ENTITY(sObjState.objIndex)
		PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] SHOULD_OBJECT_TRY_TO_CACHE_SAFE_POSITION_NOW | Obj ", sObjState.iIndex, " waiting for collision to load!")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC
//PURPOSE: Handle saving a position into vObjectLastSafeDropPoint that we can move the object to if it ends up somewhere we don't like (e.g out of bounds)
PROC PROCESS_OBJ_RETURNING_TO_LAST_SAFE_DROP_POSITION(FMMC_OBJECT_STATE& sObjState)
	IF SHOULD_OBJECT_RETURN_TO_LAST_SAFE_DROP_POSITION(sObjState)
		IF sObjState.bHaveControlOfObj
			MOVE_OBJECT_TO_LAST_SAFE_DROP_POSITION(sObjState)
		ENDIF
	ELSE
		IF SHOULD_OBJECT_TRY_TO_CACHE_SAFE_POSITION_NOW(sObjState)
			IF IS_OBJ_CURRENT_POSITION_SAFE_TO_CACHE(sObjState)
				CACHE_OBJECT_SAFE_DROP_POSITION(sObjState.iIndex, GET_FMMC_OBJECT_COORDS(sObjState), FALSE)
				
				IF IS_BIT_SET(iObjectInDangerousPositionBS, sObjState.iIndex)
					PRINTLN("[ObjectSafePoint][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_RETURNING_TO_LAST_SAFE_DROP_POSITION | Object ", sObjState.iIndex, " is now safe in its current position!")
					CLEAR_BIT(iObjectInDangerousPositionBS, sObjState.iIndex)
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(iObjectInDangerousPositionBS, sObjState.iIndex)
					vObjectLastDangerousDropPoint[sObjState.iIndex] = GET_FMMC_OBJECT_COORDS(sObjState)
					SET_BIT(iObjectInDangerousPositionBS, sObjState.iIndex)
					PRINTLN("[ObjectSafePoint][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_RETURNING_TO_LAST_SAFE_DROP_POSITION | Object ", sObjState.iIndex, " isn't safe in its current position!")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bObjectLastSafeDropPointDebug
		DRAW_DEBUG_LINE(vObjectLastSafeDropPoint[sObjState.iIndex][0], GET_FMMC_OBJECT_COORDS(sObjState), 0, 255, 0, 130)
		DRAW_DEBUG_SPHERE(vObjectLastSafeDropPoint[sObjState.iIndex][0], 0.5, 0, 255, 0, 130)
		
		DRAW_DEBUG_LINE(vObjectLastSafeDropPoint[sObjState.iIndex][1], vObjectLastSafeDropPoint[sObjState.iIndex][0], 0, 155, 0, 130)
		DRAW_DEBUG_SPHERE(vObjectLastSafeDropPoint[sObjState.iIndex][1], 0.5, 0, 155, 0, 130)
		
		DRAW_DEBUG_LINE(vObjectLastSafeDropPoint[sObjState.iIndex][1], vObjectLastDangerousDropPoint[sObjState.iIndex], 255, 0, 0, 130)
		DRAW_DEBUG_SPHERE(vObjectLastDangerousDropPoint[sObjState.iIndex], 0.5, 255, 0, 0, 100)
	ENDIF
	#ENDIF
ENDPROC

FUNC BOOL SHOULD_ACTION_ATTACHMENT_CONSEQUENCE(OBJECT_INDEX oiObj, INT iObj)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sEntityAttachmentOptions.iAttachmentConsequence = -1
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sEntityAttachmentOptions.iBitset, ciENTITY_ATTACHMENT_BS_DETACH)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sEntityAttachmentOptions.iTriggerAtTime != -1
	
		IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.stAttachmentConsTriggerTimer[iObj])
			REINIT_NET_TIMER(MC_serverBD.stAttachmentConsTriggerTimer[iObj])
		ENDIF
		
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.stAttachmentConsTriggerTimer[iObj]) >= (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sEntityAttachmentOptions.iTriggerAtTime * 1000)
			PRINTLN("[MCSpawning][Objects][Object ", iObj, "] PROCESS_ATTACHMENT_CONSEQUENCE - Triggered by time: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sEntityAttachmentOptions.iTriggerAtTime, " seconds")
			RESET_NET_TIMER(MC_serverBD.stAttachmentConsTriggerTimer[iObj])
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sEntityAttachmentOptions.iTriggerAtRule != -1
		
		INT iTeam
		iTeam = MC_playerBD[iPartToUse].iTeam
		IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sEntityAttachmentOptions.iTriggerAtRule
			PRINTLN("[MCSpawning][Objects][Object ", iObj, "] PROCESS_ATTACHMENT_CONSEQUENCE - Triggered by rule: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sEntityAttachmentOptions.iTriggerAtRule)
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sEntityAttachmentOptions.iTriggerAtHealthPercent != -1
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sEntityAttachmentOptions.iBitset, ciENTITY_ATTACHMENT_BS_USE_PARENT_HEALTH)
			IF IS_ENTITY_ATTACHED(oiObj)
				ENTITY_INDEX eiEntityAttached 
				eiEntityAttached = GET_ENTITY_ATTACHED_TO(oiObj)
				IF DOES_ENTITY_EXIST(eiEntityAttached)
					IF GET_ENTITY_HEALTH_PERCENTAGE(eiEntityAttached) <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sEntityAttachmentOptions.iTriggerAtHealthPercent
						PRINTLN("[MCSpawning][Objects][Object ", iObj, "] PROCESS_ATTACHMENT_CONSEQUENCE - Triggered by health percent of parent: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sEntityAttachmentOptions.iTriggerAtHealthPercent, "%")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			RETURN FALSE
		ENDIF
		
		IF GET_ENTITY_HEALTH_PERCENTAGE(oiObj) <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sEntityAttachmentOptions.iTriggerAtHealthPercent
			PRINTLN("[MCSpawning][Objects][Object ", iObj, "] PROCESS_ATTACHMENT_CONSEQUENCE - Triggered by health percent: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sEntityAttachmentOptions.iTriggerAtHealthPercent, "%")
			RETURN TRUE
		ENDIF
		
	ENDIF

	RETURN FALSE

ENDFUNC

PROC PROCESS_ATTACHMENT_CONSEQUENCE(OBJECT_INDEX oiObj, INT iObj)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sEntityAttachmentOptions.iBitset, ciENTITY_ATTACHMENT_BS_DETACH)
		IF IS_ENTITY_ATTACHED(oiObj)
			PRINTLN("[MCSpawning][Objects][Object ", iObj, "] PROCESS_ATTACHMENT_CONSEQUENCE - ciENTITY_ATTACHMENT_BS_DETACH")
			DETACH_ENTITY(oiObj, TRUE, FALSE)
		ENDIF
	ENDIF
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sEntityAttachmentOptions.iAttachmentConsequence
		
		CASE ciENTITY_ATTACHMENT_CONSEQUENCE_EXPLODE
			PRINTLN("[MCSpawning][Objects][Object ", iObj, "] PROCESS_ATTACHMENT_CONSEQUENCE - ciENTITY_ATTACHMENT_CONSEQUENCE_EXPLODE")
			ADD_EXPLOSION(GET_ENTITY_COORDS(oiObj), EXP_TAG_BARREL, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
		BREAK
		
		CASE ciENTITY_ATTACHMENT_CONSEQUENCE_FIRE
			PRINTLN("[MCSpawning][Objects][Object ", iObj, "] PROCESS_ATTACHMENT_CONSEQUENCE - ciENTITY_ATTACHMENT_CONSEQUENCE_FIRE")
			//PROCESS_SCRIPTED_FLAMMABLE_ENTITY when we have the models for the pack
		BREAK
		
	ENDSWITCH

ENDPROC

PROC PROCESS_OBJECT_ENTITY_ATTACHMENT_CONSEQUENCE(FMMC_OBJECT_STATE &sObjState)
	
	IF NOT sObjState.bObjExists
	OR NOT sObjState.bObjAlive
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iProcessedAttachmentConsequence, sObjState.iIndex)
		EXIT
	ENDIF
	
	IF NOT SHOULD_ACTION_ATTACHMENT_CONSEQUENCE(sObjState.objIndex, sObjState.iIndex)
		EXIT
	ENDIF

	IF NOT sObjState.bHaveControlOfObj
		NETWORK_REQUEST_CONTROL_OF_ENTITY(sObjState.objIndex)
		EXIT
	ENDIF
	
	PROCESS_ATTACHMENT_CONSEQUENCE(sObjState.objIndex, sObjState.iIndex)
	SET_BIT(MC_serverBD.iProcessedAttachmentConsequence, sObjState.iIndex)
	
ENDPROC

PROC PROCESS_OBJECT_ENTITY_ATTACHMENT(INT iObj)
	
	OBJECT_INDEX oiObj = NET_TO_OBJ(GET_OBJECT_NET_ID(iObj))
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent != -1
	AND NOT IS_BIT_SET(MC_serverBD.iProcessedAttachmentConsequence, iObj)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent])
			IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(oiObj, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent]))
				IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent]))
					ATTACH_OBJECT_TO_FLATBED(oiObj,NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent]), g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vAttachOffset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vAttachOffsetRotation, TRUE)
					PRINTLN("[MCSpawning][Objects][Object ", iObj, "] PROCESS_OBJECT_ENTITY_ATTACHMENT - successfully attached to vehicle ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent)
				ELSE
					PRINTLN("[MCSpawning][Objects][Object ", iObj, "] PROCESS_OBJECT_ENTITY_ATTACHMENT - Unable to attach to vehicle ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent,", as the vehicle is not driveable!")
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[MCSpawning][Objects][Object ", iObj, "] PROCESS_OBJECT_ENTITY_ATTACHMENT - Unable to attach to vehicle ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent,", as the vehicle does not exist!")
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_OBJ_STAGGERED_CLIENT(INT iObj)
	
	INT iTeam
	BOOL bNotPickup
	
	FMMC_OBJECT_STATE sObjState
	FILL_FMMC_OBJECT_STATE_STRUCT(sObjState, iObj)
	
	PROCESS_OBJECT_BLIP(biObjBlip[iObj], sObjState)

	PROCESS_SCRIPTED_FLAMMABLE_ENTITY(sObjState.objIndex, sObjState.mn, ptObjectPTFX[sObjState.iIndex])
	
	PROCESS_OBJECT_ORBITAL_CANNON_BAIL(sObjState)
	
	PROCESS_OBJECT_LOD_DIST(sObjState)
	
	IF !sObjState.bObjExists
		IF IS_BIT_SET(g_iMissionObjectHasSpawnedBitset, iObj)			
			CLEAR_BIT(g_iMissionObjectHasSpawnedBitset, iObj)
			PRINTLN("[Objects][Object ", iObj, "] PROCESS_OBJ_BODY - OBJECT NO LONGER EXISTS. Clearing g_iMissionObjectHasSpawnedBitset bit")
		ENDIF
		
		IF MC_playerBD[iLocalPart].iObjHacking = iObj
			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iHackFailBitset,MC_playerBD[iLocalPart].iObjHacking)
				SET_HACK_FAIL_BIT_SET(5)
				PRINTLN("[Objects][Object ", iObj, "] PROCESS_OBJ_BODY - 2 hacking/cracking object destroyed setting fail")
			ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
	//Object exists from this point on!
	
	PROCESS_OBJECT_TRAIN_ATTACHMENT_STAGGERED(sObjState)
	
	PROCESS_OBJECT_INVINCIBILITY(sObjState)
		
	IF !sObjState.bObjAlive	
		IF MC_playerBD[iLocalPart].iObjHacking = iObj
			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iHackFailBitset,MC_playerBD[iLocalPart].iObjHacking)
				SET_HACK_FAIL_BIT_SET(6) 
				PRINTLN("[Objects][Object ", iObj, "] PROCESS_OBJ_BODY - 1 hacking/cracking object destroyed setting fail")
			ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
	//Object is alive from this point on!
	PROCESS_ENTITY_INTERIOR(sObjState.iIndex, sObjState.objIndex, g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iInterior != 0 OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].sWarpLocationSettings.iWarpLocationBS != 0, <<0,0,0>>, iObjectHasBeenPlacedIntoInteriorBS)
	
	IF NOT SHOULD_PLACED_OBJECT_BE_PORTABLE(iObj)
		SET_ENTITY_LOD_DIST(sObjState.objIndex, 500)
		bNotPickup = TRUE
	ENDIF
	
	IF IS_BIT_SET(iObjNeedsToCheckOutOfBoundsBS, sObjState.iIndex)
		CACHE_IF_OBJECT_IS_OUT_OF_CRITICAL_BOUNDS(sObjState)
		CLEAR_BIT(iObjNeedsToCheckOutOfBoundsBS, sObjState.iIndex)
	ENDIF
			
	IF NOT IS_BIT_SET(g_iMissionObjectHasSpawnedBitset, iObj)
		SET_BIT(g_iMissionObjectHasSpawnedBitset, iObj)
		PRINTLN("[Objects][Object ", iObj, "] PROCESS_OBJ_BODY - OBJECT HAS BEEN SPAWNED. Setting g_iMissionObjectHasSpawnedBitset bit")
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor1 > -1
		IF NOT IS_BIT_SET(iDoorHasLinkedEntityBS, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor1)
			SET_BIT(iDoorHasLinkedEntityBS, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor1)
			PRINTLN("[Objects][Object ", iObj, "] [Doors][Door ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor1, "] PROCESS_OBJ_BODY - Setting iDoorHasLinkedEntityBS for door ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor1, " due to object ", iObj, " having it as their primary door")
		ENDIF
	ENDIF
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor2 > -1
		IF NOT IS_BIT_SET(iDoorHasLinkedEntityBS, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor2)
			SET_BIT(iDoorHasLinkedEntityBS, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor2)
			PRINTLN("[Objects][Object ", iObj, "] [Doors][Door ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor2, "] PROCESS_OBJ_BODY - Setting iDoorHasLinkedEntityBS for door ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor2, " due to object ", iObj, " having it as their secondary door")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iObjectBitSet, cibsOBJ_Reset_If_Dropped_Out_Of_Bounds)
		PROCESS_OBJ_RETURNING_TO_LAST_SAFE_DROP_POSITION(sObjState)
	ENDIF
		
	PROCESS_OBJECT_ON_RULE_WARP(sObjState)
	
	PROCESS_OBJECT_ON_DAMAGE_WARP(sObjState)
	
	IF NOT SHOULD_PLACED_OBJECT_BE_PORTABLE(iObj)
		SET_ENTITY_LOD_DIST(sObjState.objIndex, 500)
		bNotPickup = TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct.iBlipStyle = FMMC_OBJECTBLIP_CTF
		IF iCantCollectHelpDisp < 4
			FLOAT fObjDist2 = GET_DIST2_BETWEEN_ENTITIES(LocalPlayerPed, sObjState.objIndex)
			IF fObjDist2 != 0.0 AND fObjDist2 < POW(3.0, 2.0)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND DOES_OBJECT_HAVE_ANY_PRIORITY_FOR_ANY_TEAM(iObj)
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_ShowBlockCollectingOwnFlagHelpText)
					IF (MC_serverBD_4.iObjRule[iObj][GET_LOCAL_PLAYER_TEAM(TRUE)] != FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
					AND MC_serverBD_4.iObjRule[iObj][GET_LOCAL_PLAYER_TEAM(TRUE)] != FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
					AND MC_serverBD_4.iObjRule[iObj][GET_LOCAL_PLAYER_TEAM(TRUE)] != FMMC_OBJECTIVE_LOGIC_CAPTURE)
					OR (MC_serverBD_4.iObjRule[iObj][GET_LOCAL_PLAYER_TEAM(TRUE)]  = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD AND IS_BIT_SET(MC_serverBD.iObjAtYourHolding[GET_LOCAL_PLAYER_TEAM(TRUE)],iObj))
						IF bLocalPlayerPedOK
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciUSE_IN_AND_OUT_HELPTEXT)	
								PRINT_HELP("NOCOLCTF")
							ELSE
								PRINT_HELP("MCPROTPCK")
							ENDIF
						ENDIF
						iCantCollectHelpDisp++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iFragableCrate, iObj)
	OR IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn)
		IF NOT IS_DAMAGE_TRACKER_ACTIVE_ON_NETWORK_ID(sObjState.niIndex)
			ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(sObjState.niIndex, TRUE)
			PRINTLN("[Objects][Object ", iObj, "] PROCESS_OBJ_BODY - Damage tracking activated on crate")
		ENDIF
	ENDIF

	IF !sObjState.bHaveControlOfObj
	
		IF IS_SUDDEN_DEATH_OBJECT_DURING_SUDDEN_DEATH(iObj)
			IF GET_ENTITY_ALPHA(sObjState.objIndex) > 0
				SET_ENTITY_ALPHA(sObjState.objIndex, 0, FALSE)
			ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
	//Object is in our control from this point on!
	
	PROCESS_OBJECT_ENTITY_ATTACHMENT(iObj)
	
	MAINTAIN_FROZEN_OBJECTS(sObjState)
		
	IF IS_SUDDEN_DEATH_OBJECT_DURING_SUDDEN_DEATH(iObj)
		PREVENT_COLLECTION_OF_PORTABLE_PICKUP(sObjState.objIndex, TRUE)
		
		IF IS_ENTITY_VISIBLE(sObjState.objIndex)
			SET_PICKUP_OBJECT_TRANSPARENT_WHEN_UNCOLLECTABLE(sObjState.objIndex, TRUE)
			SET_PICKUP_OBJECT_ALPHA_WHEN_TRANSPARENT(0)	
			SET_ENTITY_ALPHA(sObjState.objIndex, 0, FALSE)
			SET_ENTITY_VISIBLE(sObjState.objIndex, FALSE)
			PRINTLN("[Objects][Object ", iObj, "] PROCESS_OBJ_BODY - SET_ENTITY_VISIBLE true")
		ENDIF
	ENDIF
	
	FIND_AND_STORE_SNIPERBALL_PACKAGE(sObjState)
	
	//resets the packages to its original spawn if it is dropped in an end zone
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciRESET_OBJECT_IN_ENDZONE) 
		IF OBJECT_WAS_DROPPED_IN_END_ZONE(sObjState.objIndex)
			MC_SET_UP_OBJ(sObjState.objIndex, iObj, TRUE, TRUE)
			MOVE_OBJECT_TO_LAST_SAFE_DROP_POSITION(sObjState)
			
			//clear any tickers
			THEFEED_HIDE_THIS_FRAME()
			THEFEED_FLUSH_QUEUE()	
		ENDIF
	ENDIF
	
	IF NOT IS_GRAB_OBJECT_CASH(sObjState.objIndex)
	AND NOT IS_BIT_SET(iObjDeliveredBitset, iObj)
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__NONE
		IF NOT IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_SOMEONE)
			IF ((NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSet, cibsOBJ_InvisibleOnFirstDelivery))
				OR (NOT IS_BIT_SET(MC_serverBD_1.iObjDeliveredOnceBS, iObj)))
			AND NOT IS_ENTITY_VISIBLE(sObjState.objIndex)
				IF DOES_OBJECT_HAVE_ANY_PRIORITY_FOR_ANY_TEAM(iObj)
					IF SHOULD_OBJECT_BE_INVISIBLE(iObj)
						PRINTLN("[Objects][Object ", iObj, "] PROCESS_OBJ_BODY - PREVENTING OBJECT BEING MADE VISIBLE")
						IF IS_BIT_SET(MC_serverBD.iServerBitSet3,SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE)
							SET_ENTITY_VISIBLE(sObjState.objIndex, TRUE)
						ENDIF
					ELSE
						PRINTLN("[Objects][Object ", iObj, "] PROCESS_OBJ_BODY - setting object visible") 
						SET_ENTITY_VISIBLE(sObjState.objIndex, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bNotPickup
	AND NOT IS_BIT_SET(MC_serverBD.iFragableCrate, iObj)
	AND NOT IS_BIT_SET(MC_serverBD.iWasHackObj, iObj)
	AND NOT IS_BIT_SET(iObjDeliveredBitset, iObj)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_FAST_DROP_OFF)
		FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams -1
			
			IF CAN_OBJECT_BE_PICKED_UP_BY_TEAM(iObj, iTeam)
				IF (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn != INT_TO_ENUM(MODEL_NAMES, HASH("pil_Prop_Pilot_Icon_01")))
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn != INT_TO_ENUM(MODEL_NAMES, HASH("prop_ic_cp_bag")))
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn != INT_TO_ENUM(MODEL_NAMES, HASH("ar_prop_ar_cp_bag")))
				AND NOT DOES_PICKUP_NEED_BIGGER_RADIUS(iObj)
					SET_ENTITY_LOD_DIST(sObjState.objIndex, 150)
				ENDIF
				
				IF IS_OBJECT_A_PORTABLE_PICKUP(sObjState.objIndex)
					PRINTLN("[Objects_SPAM][Object_SPAM ", iObj, "] PROCESS_OBJ_BODY - SETTING AS PICKUP OBJECT")
					SET_TEAM_PICKUP_OBJECT(sObjState.objIndex, iTeam, TRUE)
				ENDIF
				
			ELSE
				SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(sObjState, iTeam)
			ENDIF
			
		ENDFOR
	ENDIF
	
	IF bNotPickup
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn != P_V_43_Safe_S
		AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn != prop_container_ld_pu
			VEHICLE_INDEX tempveh
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn = PROP_CONTR_03B_LD // Only this container can be attached to a handler
				tempVeh = FIND_HANDLER_VEHICLE_CONTAINER_IS_ATTACHED_TO(sObjState.objIndex)
			ENDIF
			
			IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(sObjState.objIndex)
				tempveh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO(sObjState.objIndex))
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(tempVeh)
				IF GET_PED_IN_VEHICLE_SEAT(tempVeh) = LocalPlayerPed
					IF MC_serverBD_4.iObjRule[iObj][GET_LOCAL_PLAYER_TEAM(TRUE)]   != FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
					AND (MC_serverBD_4.iObjRule[iObj][GET_LOCAL_PLAYER_TEAM(TRUE)] != FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD OR (MC_serverBD_4.iObjRule[iObj][GET_LOCAL_PLAYER_TEAM(TRUE)]  = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD AND IS_BIT_SET(MC_serverBD.iObjAtYourHolding[GET_LOCAL_PLAYER_TEAM(TRUE)],iObj)))
					AND MC_serverBD_4.iObjRule[iObj][GET_LOCAL_PLAYER_TEAM(TRUE)]  != FMMC_OBJECTIVE_LOGIC_CAPTURE
					AND MC_serverBD_4.iObjRule[iObj][GET_LOCAL_PLAYER_TEAM(TRUE)]  != FMMC_OBJECTIVE_LOGIC_GO_TO
						
						IF GET_ENTITY_MODEL(tempVeh) = HANDLER
							DETACH_CONTAINER_FROM_HANDLER_FRAME(tempVeh)
							PRINTLN("[Objects][Object ", iObj, "] PROCESS_OBJ_BODY - detach being called for handler on crate") 
						ELIF IS_VEHICLE_MODEL_A_CARGOBOB(GET_ENTITY_MODEL(tempVeh))
							DETACH_VEHICLE_FROM_CARGOBOB(tempVeh, tempVeh) //detach anything from this cargobob
							SET_CARGOBOB_PICKUP_MAGNET_ACTIVE(tempVeh, FALSE)
							PRINTLN("[Objects][Object ", iObj, "] PROCESS_OBJ_BODY - detach being called for cargobob on crate") 
						//ELSE
							//DETACH_ENTITY(tempObj,TRUE,FALSE)
						ENDIF
					ELSE
						IF GET_ENTITY_MODEL(tempVeh) = HANDLER
						OR IS_VEHICLE_MODEL_A_CARGOBOB(GET_ENTITY_MODEL(tempVeh))
							IF NOT (IS_BIT_SET(iContBitSet, iObj))
								SET_BIT(iContBitSet, iObj)
								CLEAR_BIT(iDropContBitSet, iObj)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC	

FUNC BOOL HAS_OBJ_PASSED_LEAVE_CONDITIONS_FOR_RULE(FMMC_OBJECT_STATE sObjState, INT iTeam)
	IF MC_serverBD_4.iObjRule[sObjState.iIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY
	AND GET_MC_CLIENT_MISSION_STAGE(iLocalPart) = CLIENT_MISSION_STAGE_LEAVE_OBJ
		INT iNumberOfPlayersLeft = GET_PLAYERS_LEFT_LEAVE_OBJECTIVE_ENTITY(iTeam, sObjState.iIndex)
		INT iNumberOfPlayersRequiredToLeave = GET_NUMBER_OF_PLAYERS_REQUIRED_TO_LEAVE_ENTITY(iTeam)
	
		PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - iNumberOfPlayersLeft: ", iNumberOfPlayersLeft, " iNumberOfPlayersRequiredToLeave: ", iNumberOfPlayersRequiredToLeave)
	 
		IF iNumberOfPlayersLeft >= iNumberOfPlayersRequiredToLeave
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PROCESS_OBJ_DAMAGE_ENTITY_OBJECTIVE(FMMC_OBJECT_STATE &sObjState)
	INT iTeam = 0
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		
		IF HAS_TEAM_FAILED(iTeam)
		OR MC_serverBD.iNumObjHighestPriority[iTeam] <= 0
		OR MC_serverBD_4.iObjPriority[sObjState.iIndex][iTeam] > MC_serverBD_4.iCurrentHighestPriority[iTeam]
		OR MC_serverBD_4.iObjRule[sObjState.iIndex][iTeam] != FMMC_OBJECTIVE_LOGIC_DAMAGE
			RELOOP
		ENDIF
	
		FLOAT fCurrentPercent = (TO_FLOAT(GET_ENTITY_HEALTH(sObjState.objIndex)) / TO_FLOAT(GET_ENTITY_MAX_HEALTH(sObjState.objIndex))) * 100
		
		INT iHealthRequiredForRule = g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iHealthDamageRequiredForRule
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPriorityEntityHealthThresholdOverride[MC_serverBD_4.iCurrentHighestPriority[iTeam]] != 0
			iHealthRequiredForRule = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPriorityEntityHealthThresholdOverride[MC_serverBD_4.iCurrentHighestPriority[iTeam]]
		ENDIF
			
		PRINTLN("[Objects][Object ", sObjState.iIndex, "] - PROCESS_OBJ_DAMAGE_ENTITY_OBJECTIVE - fCurrentPercent: ", fCurrentPercent, " thresh-hold to pass objective: ", iHealthRequiredForRule)
				
		IF fCurrentPercent > iHealthRequiredForRule
			RELOOP
		ENDIF
		
		PRINTLN("[Objects][Object ", sObjState.iIndex, "] - PROCESS_OBJ_DAMAGE_ENTITY_OBJECTIVE - Below the thresh-hold to pass objective")
				
		SET_TEAM_OBJECTIVE_OUTCOME(iTeam, MC_serverBD_4.iObjPriority[sObjState.iIndex][iTeam], sObjState.bObjAlive)
		
		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT, sObjState.iIndex, iTeam, sObjState.bObjAlive)
		
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OBJ_DAMAGED
		
		SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, MC_serverBD_4.iObjPriority[sObjState.iIndex][iTeam], MC_serverBD.iReasonForObjEnd[iTeam], sObjState.bObjAlive)
		
		BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, MC_serverBD_4.iObjPriority[sObjState.iIndex][iTeam], DID_TEAM_PASS_OBJECTIVE(iTeam, MC_serverBD_4.iObjPriority[sObjState.iIndex][iTeam]))
		
	ENDFOR
ENDPROC

PROC PROCESS_OBJECT_TASK_ACTIVATION(FMMC_OBJECT_STATE &sObjState)
	
	INT i = sObjState.iIndex
	
	IF NOT sObjState.bObjExists
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(MC_ServerBD.iObjectActionActivatedBS, i)
			PRINTLN("[Objects][Object ", i, "] - PROCESS_OBJECT_TASK_ACTIVATION - Clearing Task Activation Bit, object no longer exists.")
		ENDIF
		#ENDIF
		CLEAR_BIT(MC_ServerBD.iObjectActionActivatedBS, i)
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_OBJECTS, i, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedAction, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedRuleForceAlwaysSpawnOnScore, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAggroIndexBS_Entity_Obj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedRuleAlwaysSpawnOnScoreEnd)			
			IF NOT IS_BIT_SET(MC_ServerBD.iObjectActionActivatedBS, i)
				PRINTLN("[Objects][Object ", i, "] - PROCESS_OBJECT_TASK_ACTIVATION - TRUE - TASKS HAVE BEEN ACTIVATED (1)")
				SET_BIT(MC_ServerBD.iObjectActionActivatedBS, i)
				SET_FMMC_DRONE_RETASK_WITH_ENTITY_ID_AND_TYPE(i, CREATION_TYPE_OBJECTS)
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_OBJECTS, i, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedAction, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedRuleAlwaysForceSpawnOnScore, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAggroIndexBS_Entity_Obj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedRuleAlwaysSpawnOnScoreEnd)			
			IF NOT IS_BIT_SET(MC_ServerBD.iObjectActionActivatedBS, i)
				PRINTLN("[Objects][Object ", i, "] - PROCESS_OBJECT_TASK_ACTIVATION - TRUE - TASKS HAVE BEEN ACTIVATED (2)")
				SET_BIT(MC_ServerBD.iObjectActionActivatedBS, i)
				SET_FMMC_DRONE_RETASK_WITH_ENTITY_ID_AND_TYPE(i, CREATION_TYPE_OBJECTS)
			ENDIF			
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_OBJECTS, i, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedAction, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedRuleAlwaysForceSpawnOnScore, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAggroIndexBS_Entity_Obj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedRuleAlwaysSpawnOnScoreEnd)			
			IF NOT IS_BIT_SET(MC_ServerBD.iObjectActionActivatedBS, i)
				PRINTLN("[Objects][Object ", i, "] - PROCESS_OBJECT_TASK_ACTIVATION - TRUE - TASKS HAVE BEEN ACTIVATED (3)")
				SET_BIT(MC_ServerBD.iObjectActionActivatedBS, i)
				SET_FMMC_DRONE_RETASK_WITH_ENTITY_ID_AND_TYPE(i, CREATION_TYPE_OBJECTS)
			ENDIF			
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_OBJECTS, i, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedAction, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedRuleAlwaysForceSpawnOnScore, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAggroIndexBS_Entity_Obj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedRuleAlwaysSpawnOnScoreEnd)			
			IF NOT IS_BIT_SET(MC_ServerBD.iObjectActionActivatedBS, i)
				PRINTLN("[Objects][Object ", i, "] - PROCESS_OBJECT_TASK_ACTIVATION - TRUE - TASKS HAVE BEEN ACTIVATED (4)")
				SET_BIT(MC_ServerBD.iObjectActionActivatedBS, i)
				SET_FMMC_DRONE_RETASK_WITH_ENTITY_ID_AND_TYPE(i, CREATION_TYPE_OBJECTS)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_OBJ_STAGGERED_SERVER(INT iObj)

	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	FMMC_OBJECT_STATE sObjState, sEmpty
	FILL_FMMC_OBJECT_STATE_STRUCT(sObjState, iObj)
	
	PED_INDEX tempPed
	PLAYER_INDEX tempPlayer = INVALID_PLAYER_INDEX()
	PARTICIPANT_INDEX tempPart = INVALID_PARTICIPANT_INDEX()
	VEHICLE_INDEX tempVeh

	INT iTeam
	INT iObjTeam
	BOOL bCheckTeamFail, bContainer
	INT iPriority[FMMC_MAX_TEAMS]
	BOOL bWasObjective[FMMC_MAX_TEAMS]
	
	IF MC_serverBD.iObjHackPart[iObj] != -1
		IF MC_playerBD[MC_serverBD.iObjHackPart[iObj]].iObjHacking = -1
			PRINTLN("[Objects][Object ", iObj, "] PROCESS_OBJ_STAGGERED_SERVER - server thinks object is being hacked but participant does not for obj, ipart: ",MC_serverBD.iObjHackPart[iObj])
			MC_serverBD.iObjHackPart[iObj] = -1
		ENDIF
	ENDIF
	
	// manage server package attachment data
	IF sObjState.bObjExists
		
		//Breakables (crates/CCTV cameras):
		SERVER_MANAGE_BREAKABLE_OBJECTS(sObjState) // Also gets called if the object doesn't exist, but corresponding iFragableCrate bit
														// is set. This is to handle the case where the object is destroyed but it should
														// still spawn pickups
		
		IF CLEANUP_OBJ_EARLY(sObjState)
			EXIT
		ENDIF
		
		bContainer = IS_OBJECT_A_CONTAINER(sObjState.objIndex)
		
		IF bContainer
		OR IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_SOMEONE)
			IF NOT bContainer
				ENTITY_INDEX tempPlayerEnt = GET_ENTITY_ATTACHED_TO(sObjState.objIndex)
				IF DOES_ENTITY_EXIST(tempPlayerEnt)
					tempPed = GET_PED_INDEX_FROM_ENTITY_INDEX(tempPlayerEnt)
					IF NOT IS_PED_INJURED(tempPed)
						IF IS_PED_A_PLAYER(tempPed)
							tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(tempPed)
							tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				// This does the container pickup stuff
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn != prop_container_ld_pu
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn = PROP_CONTR_03B_LD
						tempVeh = FIND_HANDLER_VEHICLE_CONTAINER_IS_ATTACHED_TO(sObjState.objIndex)
					ENDIF
					
					IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(sObjState.objIndex)
						tempVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO(sObjState.objIndex))
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(tempVeh)
						tempPed = GET_PED_IN_VEHICLE_SEAT(tempVeh)
						IF NOT IS_PED_INJURED(tempPed)
							IF IS_PED_A_PLAYER(tempPed)
								tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(tempPed)
								tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
							ENDIF
						ENDIF
					ELSE
						IF IS_CRATE_STUCK(sObjState.objIndex, iObj)
						OR IS_ENTITY_IN_WATER(sObjState.objIndex)
							PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - object is returning as stuck or in water so cleaning up")
							FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
								REMOVE_OBJECT_HOLDING_POINTS(iTeam, iObj)
							ENDFOR
							IF NETWORK_HAS_CONTROL_OF_ENTITY(sObjState.objIndex)
								PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - SETTING SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION FALSE on Object")
								SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sObjState.objIndex, FALSE)
							ENDIF
							CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(iObj)])
							sObjState = sEmpty
							FILL_FMMC_OBJECT_STATE_STRUCT(sObjState, iObj)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF tempPlayer != INVALID_PLAYER_INDEX()
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart) 
					IF IS_NET_PLAYER_OK(tempPlayer)
						INT ipart = NATIVE_TO_INT(tempPart)
						IF MC_serverBD.iObjCarrier[iObj] != ipart
							// Package is still free, give it to the player
							IF NOT IS_BIT_SET(MC_serverBD.iObjAtYourHolding[MC_playerBD[ipart].iTeam],iObj)
								PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - Object carrier - Object attached to participant: ",NATIVE_TO_INT(tempPart))
								IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
									MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlObjTimer[iObj])
								ENDIF
																		
								MC_serverBD.iObjCarrier[iObj] = NATIVE_TO_INT(tempPart)
								
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_EnableObjectStealing)
									MC_serverBD_1.iPTLDroppedPackageLastOwner[iObj] = -1
									MC_serverBD_1.iPTLDroppedPackageOwner[iObj] = NATIVE_TO_INT(tempPart)
									PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - STOCKPILE - Setting iPTLDroppedPackageLastOwner[",iObj,"] to -1. Someone has picked up")
									PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - STOCKPILE - Setting iPTLDroppedPackageOwner[",iObj,"] to ", NATIVE_TO_INT(tempPart))
								ENDIF
								
								IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_TransformJuggernaut)
								AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_TransformBeast)
								AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_UpgradeVeh)
									IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCustomShard > 0
										BROADCAST_FMMC_DISPLAY_CUSTOM_OBJECT_PICKUP_SHARD(tempPlayer, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCustomShard, MC_playerBD[ipart].iTeam)
									ENDIF
								ENDIF
								
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_CarrierAlerts)
								AND IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
									BROADCAST_FMMC_OBJECT_CARRIER_ALERT(tempPlayer)
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
							IF MC_serverBD.iObjCarrier[iObj] > -1
								MC_serverBD.iObjTeamCarrier = MC_PlayerBD[MC_serverBD.iObjCarrier[iObj]].iTeam
							ENDIF
						ENDIF
						
						IF (MC_serverBD.iObjCarrier[iObj] > -1
						AND (MC_serverBD.iObjTeamCarrier > -1 OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)))
						OR (MC_serverBD_4.iObjPriority[iObj][iObjTeam] < FMMC_MAX_RULES AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iObjTeam].iRuleBitsetNine[MC_serverBD_4.iObjPriority[iObj][iObjTeam]], ciBS_RULE9_USE_GRANULAR_CAPTURE))
							PRINTLN("[CONTROL OBJECT][Objects_SPAM][Object_SPAM ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - Valid Carrier and Team Carrier Assigned iObjCarrier: ", MC_serverBD.iObjCarrier[iObj], "  iObjTeamCarrier: ", MC_serverBD.iObjTeamCarrier)
						
							iObjTeam = MC_playerBD[MC_serverBD.iObjCarrier[iObj]].iTeam
							IF MC_serverBD_4.iObjRule[iObj][iObjTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
							OR MC_serverBD_4.iObjRule[iObj][iObjTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
							OR MC_serverBD_4.iObjRule[iObj][iObjTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
								IF MC_serverBD_4.iObjPriority[iObj][iObjTeam] <= MC_serverBD_4.iCurrentHighestPriority[iObjTeam]
									iNumHighPriorityObjHeld[iObjTeam]++
									FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
										IF iTeam != iObjTeam
											IF DOES_TEAM_LIKE_TEAM(iTeam, iObjTeam)
												iNumHighPriorityObjHeld[iTeam]++
											ENDIF
										ENDIF
									ENDFOR
								ENDIF
							ENDIF
							
							IF MC_serverBD_4.iObjRule[iObj][iObjTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
							AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCaptureRange = 0
								IF MC_serverBD_4.iObjPriority[iObj][iObjTeam] < FMMC_MAX_RULES
									IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iObjTeam].iRuleBitsetNine[MC_serverBD_4.iObjPriority[iObj][iObjTeam]], ciBS_RULE9_USE_GRANULAR_CAPTURE)
										IF NOT IS_BIT_SET(MC_playerBD[ipart].iClientBitSet,PBBOOL_I_AM_DROWNING)
										AND NOT IS_PLAYER_IN_PLANE_IN_WATER(tempPed)
											IF (NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlObjTimer[iObj]) AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED))
											OR (NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdControlObjTeamTimer[MC_serverBD.iObjTeamCarrier]) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED))
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)												
													START_NET_TIMER(MC_serverBD.tdControlObjTeamTimer[MC_serverBD.iObjTeamCarrier])
													PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - starting control timer for obj: ",iObj, " iTeam: ", MC_serverBD.iObjTeamCarrier)												
												ELSE
													MC_START_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlObjTimer[iObj])
													PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - starting control timer for obj: ",iObj)
												ENDIF
											ELSE
											
												PRINTLN("[[CONTROL OBJECT][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - Checking Timer Expired: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlObjTeamTimer[MC_serverBD.iObjTeamCarrier]) > MC_serverBD.iObjTakeoverTime[iObjTeam][MC_serverBD_4.iObjPriority[iObj][iObjTeam]])
												
												IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_CONTROL_TIME_STOPPED_T0 + iObjTeam)											
													IF (MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlObjTimer[iObj]) > (MC_serverBD.iObjTakeoverTime[iObjTeam][MC_serverBD_4.iObjPriority[iObj][iObjTeam]]) AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED))
													OR (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlObjTeamTimer[MC_serverBD.iObjTeamCarrier]) > (MC_serverBD.iObjTakeoverTime[iObjTeam][MC_serverBD_4.iObjPriority[iObj][iObjTeam]]) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED))

																											
														INT iSublogic = -1
														
														BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_CON_OBJ,
																									0,
																									iObjTeam,
																									iSublogic,
																									tempPlayer,
																									ci_TARGET_OBJECT,
																									iObj)
														
														REQUEST_INCREMENT_SERVER_REMOTE_PLAYER_SCORE(ipart, GET_FMMC_POINTS_FOR_TEAM(iObjTeam,MC_serverBD_4.iObjPriority[iObj][iObjTeam]),iObjTeam,MC_serverBD_4.iObjPriority[iObj][iObjTeam])
														
														IF MC_serverBD.iRuleReCapture[iObjTeam][MC_serverBD_4.iObjPriority[iObj][iObjTeam]] <=0
															PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - control object ",iObj," cleaning up as out of iRuleReCapture for team ",iObjTeam)
															CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(iObj)])
															sObjState = sEmpty
															FILL_FMMC_OBJECT_STATE_STRUCT(sObjState, iObj)
														ENDIF
														
														IF MC_serverBD_2.iCurrentObjRespawnLives[iObj] <=0
														AND MC_serverBD.iRuleReCapture[iObjTeam][MC_serverBD_4.iObjPriority[iObj][iObjTeam]] <=0
															
															FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
																iPriority[iTeam] = MC_serverBD_4.iObjPriority[iObj][iTeam]
																IF iTeam=iObjTeam
																	IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[iObj],iTeam)
																		CLEAR_BIT(MC_serverBD.iObjteamFailBitset[iObj],iTeam)
																		CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], iObj)
																		bWasObjective[iTeam] = TRUE
																		PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - OBJECTIVE NOT FAILED FOR TEAM: ",iObjTeam," BECAUSE THEY CAPTURED OBJ: ",iObj) 
																	ENDIF
																	SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iTeam],TRUE)
																ELSE
																	IF DOES_TEAM_LIKE_TEAM(iTeam,iObjTeam)
																		IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[iObj],iTeam)
																			CLEAR_BIT(MC_serverBD.iObjteamFailBitset[iObj],iTeam)
																			CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], iObj)
																			PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - OBJECTIVE NOT FAILED FOR TEAM: ",iTeam," BECAUSE THEY friend cap obj: ",iObj) 
																			bWasObjective[iTeam] = TRUE
																		ENDIF
																	ENDIF
																ENDIF
															ENDFOR
															FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
																IF iTeam != iObjTeam
																	IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[iObj],iTeam)
																		IF iPriority[iTeam] < FMMC_MAX_RULES
																		OR IS_BIT_SET(MC_serverBD.iMissionCriticalObj[iTeam], iObj)
																			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamFailBitset,iPriority[iTeam])
																			OR IS_BIT_SET(MC_serverBD.iMissionCriticalObj[iTeam], iObj)																				
																				REQUEST_SET_TEAM_FAILED(iTeam,mFail_OBJ_CAPTURED)
																				MC_serverBD.iEntityCausingFail[iTeam] = iObj
																				PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - MISSION FAILED FOR TEAM:",iTeam," BECAUSE OBJ CAPTURED: ",iObj) 
																			ENDIF
																		ENDIF
																		bWasObjective[iTeam] = TRUE
																		CLEAR_BIT(MC_serverBD.iObjteamFailBitset[iObj],iTeam)
																	ENDIF
																	IF DOES_TEAM_LIKE_TEAM(iTeam,iObjTeam)
																		SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iTeam],TRUE)
																		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,iObj,iTeam,TRUE)
																	ELSE
																		SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iTeam],FALSE)
																		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,iObj,iTeam,FALSE)
																	ENDIF
																ELSE
																	SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iTeam],TRUE)
																	PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,iObj,iTeam,TRUE)
																ENDIF
																MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OBJ_CAPTURED
															ENDFOR
														ENDIF
														IF MC_serverBD_4.iObjPriority[iObj][iObjTeam] < FMMC_MAX_RULES
															IF MC_serverBD_4.iObjPriority[iObj][iObjTeam] < FMMC_MAX_RULES
																IF MC_serverBD.iRuleReCapture[iObjTeam][MC_serverBD_4.iObjPriority[iObj][iObjTeam]] !=UNLIMITED_CAPTURES_KILLS
																	MC_serverBD.iRuleReCapture[iObjTeam][MC_serverBD_4.iObjPriority[iObj][iObjTeam]]--
																ENDIF
															ENDIF
														ENDIF
														FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
															IF bWasObjective[iTeam]
																IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_OBJECT,iTeam,iPriority[iTeam])
																	SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,iPriority[iTeam],MC_serverBD.iReasonForObjEnd[iTeam])
																	BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam],iTeam,iPriority[iTeam],DID_TEAM_PASS_OBJECTIVE(iTeam,iPriority[iTeam]))
																ENDIF
															ENDIF
														ENDFOR
														
														PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - Control complete for obj: ",iObj)
														MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlObjTimer[iObj])
														
														IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
															INT iTeamO
															FOR iTeamO = 0 TO FMMC_MAX_TEAMS-1		
																RESET_NET_TIMER(MC_serverBD.tdControlObjTeamTimer[iTeamO])
															ENDFOR
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ELSE // Else we're drowning - reset the timer
											IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
												PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - Part ",iPart," is drowning, disable control countdown for obj: ",iObj)
												MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlObjTimer[iObj])
												
												
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
													IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_TeamControlBarObject)
														INT iTeamO
														FOR iTeamO = 0 TO FMMC_MAX_TEAMS-1	
															PRINTLN("[CONTROL_OBJECT][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - Reseting Team: ", iTeamO, " Timer. (1)")
															RESET_NET_TIMER(MC_serverBD.tdControlObjTeamTimer[iTeamO])
														ENDFOR
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ELSE
										IF MC_serverBD_4.iCurrentHighestPriority[iObjTeam] < FMMC_MAX_RULES
											IF MC_serverBD.iGranularCurrentPoints[iObjTeam] >= GET_FMMC_GRANULAR_CAPTURE_TARGET(MC_serverBD_4.iCurrentHighestPriority[iObjTeam], iObjTeam)
											AND NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()	
												INT iSublogic = -1
												
												BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_CON_OBJ,
																							0,
																							iObjTeam,
																							iSublogic,
																							tempPlayer,
																							ci_TARGET_OBJECT,
																							iObj)
																								
												REQUEST_INCREMENT_SERVER_REMOTE_PLAYER_SCORE(ipart, GET_FMMC_POINTS_FOR_TEAM(iObjTeam,MC_serverBD_4.iObjPriority[iObj][iObjTeam]),iObjTeam,MC_serverBD_4.iObjPriority[iObj][iObjTeam])
												IF MC_serverBD.iRuleReCapture[iObjTeam][MC_serverBD_4.iObjPriority[iObj][iObjTeam]] <=0
													PRINTLN("[GRANCAP][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - control object ",iObj," cleaning up as out of iRuleReCapture for team ",iObjTeam)
													CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(iObj)])
													sObjState = sEmpty
													FILL_FMMC_OBJECT_STATE_STRUCT(sObjState, iObj)
												ENDIF
												IF MC_serverBD_2.iCurrentObjRespawnLives[iObj] <=0
												AND MC_serverBD.iRuleReCapture[iObjTeam][MC_serverBD_4.iObjPriority[iObj][iObjTeam]] <=0
													
													FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
														iPriority[iTeam] = MC_serverBD_4.iObjPriority[iObj][iTeam]
														IF iTeam = iObjTeam
															IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[iObj],iTeam)
																CLEAR_BIT(MC_serverBD.iObjteamFailBitset[iObj],iTeam)
																CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], iObj)
																bWasObjective[iTeam] = TRUE
																PRINTLN("[GRANCAP][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - OBJECTIVE NOT FAILED FOR TEAM: ",iObjTeam," BECAUSE THEY CAPTURED OBJ: ",iObj) 
															ENDIF
															SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iTeam],TRUE)
														ELSE
															IF DOES_TEAM_LIKE_TEAM(iTeam,iObjTeam)
																IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[iObj],iTeam)
																	CLEAR_BIT(MC_serverBD.iObjteamFailBitset[iObj],iTeam)
																	CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], iObj)
																	PRINTLN("[GRANCAP][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - OBJECTIVE NOT FAILED FOR TEAM: ",iTeam," BECAUSE THEY friend cap obj: ",iObj) 
																	bWasObjective[iTeam] = TRUE
																ENDIF
															ENDIF
														ENDIF
													ENDFOR
													FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
														IF iTeam != iObjTeam
															IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[iObj],iTeam)
																IF iPriority[iTeam] < FMMC_MAX_RULES
																OR IS_BIT_SET(MC_serverBD.iMissionCriticalObj[iTeam], iObj)
																	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamFailBitset,iPriority[iTeam])
																	OR IS_BIT_SET(MC_serverBD.iMissionCriticalObj[iTeam], iObj)																		
																		REQUEST_SET_TEAM_FAILED(iTeam,mFail_OBJ_CAPTURED)
																		MC_serverBD.iEntityCausingFail[iTeam] = iObj
																		PRINTLN("[GRANCAP][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - MISSION FAILED FOR TEAM:",iTeam," BECAUSE OBJ CAPTURED: ",iObj) 
																	ENDIF
																ENDIF
																bWasObjective[iTeam] = TRUE
																CLEAR_BIT(MC_serverBD.iObjteamFailBitset[iObj],iTeam)
															ENDIF
															IF DOES_TEAM_LIKE_TEAM(iTeam,iObjTeam)
																SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iTeam],TRUE)
																PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,iObj,iTeam,TRUE)
															ELSE
																SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iTeam],FALSE)
																PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,iObj,iTeam,FALSE)
															ENDIF
														ELSE
															SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iTeam],TRUE)
															PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,iObj,iTeam,TRUE)
														ENDIF
														MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OBJ_CAPTURED
													ENDFOR
												ENDIF
												IF MC_serverBD_4.iObjPriority[iObj][iObjTeam] < FMMC_MAX_RULES
													IF MC_serverBD_4.iObjPriority[iObj][iObjTeam] < FMMC_MAX_RULES
														IF MC_serverBD.iRuleReCapture[iObjTeam][MC_serverBD_4.iObjPriority[iObj][iObjTeam]] !=UNLIMITED_CAPTURES_KILLS
															MC_serverBD.iRuleReCapture[iObjTeam][MC_serverBD_4.iObjPriority[iObj][iObjTeam]]--
														ENDIF
													ENDIF
												ENDIF
												FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
													IF bWasObjective[iTeam]
														IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_OBJECT,iTeam,iPriority[iTeam])
															SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,iPriority[iTeam],MC_serverBD.iReasonForObjEnd[iTeam])
															BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam],iTeam,iPriority[iTeam],DID_TEAM_PASS_OBJECTIVE(iTeam,iPriority[iTeam]))
														ENDIF
													ENDIF
												ENDFOR
												
												PRINTLN("[GRANCAP][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - Control complete for obj: ",iObj)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
									MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlObjTimer[iObj])
									
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_TeamControlBarObject)
											INT iTeamO
											FOR iTeamO = 0 TO FMMC_MAX_TEAMS-1
											PRINTLN("[LM][CONTROL OBJECT] Reseting Team: ", iTeamO, " Timer. (2)")
												RESET_NET_TIMER(MC_serverBD.tdControlObjTeamTimer[iTeamO])
											ENDFOR
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF MC_serverBD.iObjCarrier[iObj]  = NATIVE_TO_INT(tempPart)
							PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - Object no longer carried by participant -they not ok: ",NATIVE_TO_INT(tempPart))
							MC_serverBD.iObjCarrier[iObj] = -1
							
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_EnableObjectStealing)
								MC_serverBD_1.iPTLDroppedPackageLastOwner[iObj] = -1
								IF IS_BIT_SET(MC_serverBD.iObjCaptured, iObj)
									CLEAR_BIT(MC_serverBD.iObjCaptured, iObj)
									PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - Clearing MC_serverBD.iObjCaptured, ", iObj)
								ENDIF
								PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - STOCKPILE - Setting iPTLDroppedPackageLastOwner[",iObj,"] to -1. NOT OK")
							ENDIF
							
							IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
								MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlObjTimer[iObj])
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_TeamControlBarObject)
								IF MC_serverBD.iObjTeamCarrier != -1
									PRINTLN("[CONTROL OBJECT][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - MC_serverBD.iObjTeamCarrier: ", MC_serverBD.iObjTeamCarrier," Cleaning up to -1 because iObj: ", iObj, " no longer carried by participant -they not ok: ",NATIVE_TO_INT(tempPart))
									
									IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
										PRINTLN("[CONTROL OBJECT][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - Reseting Team: ", MC_serverBD.iObjTeamCarrier," Timer.")
										RESET_NET_TIMER(MC_serverBD.tdControlObjTeamTimer[MC_serverBD.iObjTeamCarrier])
									ENDIF
									
									MC_serverBD.iObjTeamCarrier = -1
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF MC_serverBD.iObjCarrier[iObj]  = NATIVE_TO_INT(tempPart)
						PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - Object: ",iObj," no longer carried by participant -they not active: ",NATIVE_TO_INT(tempPart))
						MC_serverBD.iObjCarrier[iObj]  = -1
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_EnableObjectStealing)
							MC_serverBD_1.iPTLDroppedPackageLastOwner[iObj] = -1
							IF IS_BIT_SET(MC_serverBD.iObjCaptured, iObj)
								CLEAR_BIT(MC_serverBD.iObjCaptured, iObj)
								PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - Clearing MC_serverBD.iObjCaptured, ", iObj)
							ENDIF
							PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - STOCKPILE - Setting iPTLDroppedPackageLastOwner[",iObj,"] to -1. NOT ACTIVE")
						ENDIF
						
						IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
							MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlObjTimer[iObj])
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_TeamControlBarObject)
							IF MC_serverBD.iObjTeamCarrier != -1			
								PRINTLN("[CONTROL OBJECT][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - MC_serverBD.iObjTeamCarrier: ", MC_serverBD.iObjTeamCarrier," Cleaning up to -1 because iObj: ", iObj, " no longer carried by participant -they not active: ",NATIVE_TO_INT(tempPart))
								
								IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
									PRINTLN("[CONTROL OBJECT][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - Reseting Team: ", MC_serverBD.iObjTeamCarrier," Timer.")
									RESET_NET_TIMER(MC_serverBD.tdControlObjTeamTimer[MC_serverBD.iObjTeamCarrier])
								ENDIF
								
								MC_serverBD.iObjTeamCarrier = -1
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF MC_serverBD.iObjCarrier[iObj] != -1
					PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - Object not carried by any ped ... 1st call")
					MC_serverBD.iObjCarrier[iObj] = -1
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
						MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlObjTimer[iObj])
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_TeamControlBarObject)
						IF MC_serverBD.iObjTeamCarrier != -1			
							PRINTLN("[CONTROL OBJECT][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - MC_serverBD.iObjTeamCarrier: ", MC_serverBD.iObjTeamCarrier," Cleaning up to -1 because iObj: ", iObj, " not carried by any ped ... 1st call")
							
							IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
								PRINTLN("[CONTROL OBJECT][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - Reseting Team: ", MC_serverBD.iObjTeamCarrier," Timer.")
								RESET_NET_TIMER(MC_serverBD.tdControlObjTeamTimer[MC_serverBD.iObjTeamCarrier])
							ENDIF
							
							MC_serverBD.iObjTeamCarrier = -1
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF MC_serverBD.iObjHackPart[iObj] != -1
				IF MC_serverBD_4.iObjRule[iObj][MC_playerBD[MC_serverBD.iObjHackPart[iObj]].iTeam] = FMMC_OBJECTIVE_LOGIC_MINIGAME
				AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_CrossOffObjectCarryingHUDWhileBeingHacked)
					IF MC_serverBD_4.iObjPriority[iObj][MC_playerBD[MC_serverBD.iObjHackPart[iObj]].iTeam]  <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[MC_serverBD.iObjHackPart[iObj]].iTeam]
						iNumHighPriorityObjHeld[MC_playerBD[MC_serverBD.iObjHackPart[iObj]].iTeam]++
						
						FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
							IF iTeam != MC_playerBD[MC_serverBD.iObjHackPart[iObj]].iTeam
								IF DOES_TEAM_LIKE_TEAM(iTeam,MC_playerBD[MC_serverBD.iObjHackPart[iObj]].iTeam)
									iNumHighPriorityObjHeld[iTeam]++
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
			ENDIF
			
			IF MC_serverBD.iObjCarrier[iObj]   != -1
				PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - Object not carried by any ped ... 2nd call")

				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_EnableObjectStealing)
					IF IS_NET_PARTICIPANT_INT_ID_VALID(MC_serverBD.iObjCarrier[iObj])
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(MC_serverBD.iObjCarrier[iObj]))
							tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(MC_serverBD.iObjCarrier[iObj]))
							IF IS_NET_PLAYER_OK(tempPlayer)
								MC_serverBD_1.iPTLDroppedPackageLastOwner[iObj] = MC_serverBD.iObjCarrier[iObj]
							ELSE
								IF IS_BIT_SET(MC_serverBD.iObjCaptured, iObj)
									CLEAR_BIT(MC_serverBD.iObjCaptured, iObj)
									PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - Clearing MC_serverBD.iObjCaptured, ", iObj)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				MC_serverBD.iObjCarrier[iObj]  = -1
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
					MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlObjTimer[iObj])
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_TeamControlBarObject)
					IF MC_serverBD.iObjTeamCarrier != -1			
						PRINTLN("[CONTROL OBJECT][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - MC_serverBD.iObjTeamCarrier: ", MC_serverBD.iObjTeamCarrier," Cleaning up to -1 because iObj: ", iObj, " not carried by any ped ... 2nd call")
						
						IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
							PRINTLN("[CONTROL OBJECT][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - Reseting Team: ", MC_serverBD.iObjTeamCarrier," Timer.")
							RESET_NET_TIMER(MC_serverBD.tdControlObjTeamTimer[MC_serverBD.iObjTeamCarrier])
						ENDIF
						
						MC_serverBD.iObjTeamCarrier = -1
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
			IF MC_serverBD_4.iObjPriority[iObj][iTeam] < FMMC_MAX_RULES
			AND MC_serverBD_4.iObjPriority[iObj][iTeam] > -1
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[MC_serverBD_4.iObjPriority[iObj][iTeam]], ciBS_RULE10_PASS_RULE_THROUGH_TAGGING)
					IF IS_ENTITY_ALREADY_TAGGED(4,iObj)
						PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - OBJECT HAS BEEN TAGGED. Calling PROGRESS_SPECIFIC_OBJECTIVE")
						INVALIDATE_SINGLE_OBJECTIVE(iTeam, MC_ServerBD_4.iObjPriority[iObj][iTeam], TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
				
	ELSE // Else the network id for this object doesn't exist
	
		IF MC_serverBD.iObjCarrier[iObj] != -1
			PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - Object: ",iObj," does not exist") 
			MC_serverBD.iObjCarrier[iObj] = -1
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
				MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlObjTimer[iObj])
			ENDIF
		ENDIF
		
		//-- Dave W, for 2025269, need to check if any objects that no longer exist used to be fraggable crates that got destroyed 
		IF IS_BIT_SET(MC_serverBD.iFragableCrate, iObj)
			//PRINTLN("[RCC MISSION] [dsw] [crates] Object doesn't exist iObj = ", iObj, " but iFragableCrate bit is set.")
			SERVER_MANAGE_BREAKABLE_OBJECTS(sObjState)
		ENDIF
		
	ENDIF
	
	PROCESS_OBJECT_TASK_ACTIVATION(sObjState)
	
	PROCESS_OBJ_DAMAGE_ENTITY_OBJECTIVE(sObjState)
	
	PROCESS_OBJECT_ENTITY_ATTACHMENT_CONSEQUENCE(sObjState)
	
	// This plays ptfx for objects once they're deleted
	IF sObjState.bObjExists
		IF NOT sObjState.bObjAlive
			MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlObjTimer[iObj])
			
			IF NOT IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn)
			AND SHOULD_WE_CLEANUP_OBJECT_DUE_TO_IT_BEING_DEAD(iObj)
	            IF sObjState.bHaveControlOfObj
					PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - object is dead - play particle fx and delete it's net id")					
					USE_PARTICLE_FX_ASSET("scr_fm_mp_missioncreator")
					START_PARTICLE_FX_NON_LOOPED_AT_COORD(GET_REMOVAL_PTFX_NAME(GET_ENTITY_MODEL(sObjState.objIndex)), GET_FMMC_OBJECT_COORDS(sObjState), <<0,0,0>>)
					DELETE_NET_ID(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(iObj)])
					sObjState = sEmpty
					FILL_FMMC_OBJECT_STATE_STRUCT(sObjState, iObj)
				ELSE
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sObjState.niIndex)
				ENDIF
			ENDIF
			
			MC_serverBD.iObjHackPart[iObj] = -1
			
			IF MC_serverBD_2.iCurrentObjRespawnLives[iObj] <= 0
				IF NOT IS_BIT_SET(MC_serverBD_1.sCreatedCount.iSkipObjBitset, iObj)
					bCheckTeamFail = TRUE
				ELSE
					EXIT // Leave the function
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_ATTACHED_TO_SOMETHING)
				VECTOR vtemp = GET_FMMC_OBJECT_COORDS(sObjState)
				FLOAT fZCheck = -20.0
				
				//Particular objects
				IF GET_ENTITY_MODEL(sObjState.objIndex) = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_hdsec")) //WVM Bomb
					fZCheck = -120.0
				ENDIF
				
				//Interior checks
				IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_BUNKER)
				OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SUB)
				OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_IAA)
				OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_HANGAR)
					fZCheck = -120.0
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciLOWER_OBJECT_CLEANUP)
					fZCheck = -200
				ENDIF
				
				IF vtemp.z < fZCheck
					PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - object is returning as below ",fZCheck," so cleaning up: ", iObj, " object z value was: ", vtemp.z)
					CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(iObj)])
					sObjState = sEmpty
					FILL_FMMC_OBJECT_STATE_STRUCT(sObjState, iObj)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD //Currently does nothing except print - remove if/when reimplemented
		IF IS_BIT_SET(g_iMissionObjectHasSpawnedBitset, iObj)
			FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
				IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
					IF MC_serverBD_4.iObjPriority[iObj][iTeam] <= FMMC_MAX_RULES
					AND MC_serverBD_4.iObjPriority[iObj][iTeam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]
						IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectsDestroyedRuleHudAmount[MC_ServerBD_4.iCurrentHighestPriority[iTeam]] > 0
							IF NOT IS_BIT_SET(MC_ServerBD_4.iObjectsDestroyedForSpecialHUDBitset[iTeam], iObj)
								PRINTLN("[iEntityincrement][Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - An object was cleaned up that spawned, we may have wanted to increment it ? (iObjectsDestroyedForSpecialHUD)")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		#ENDIF //Currently does nothing except print - remove if/when reimplemented
			
		MC_serverBD.iObjHackPart[iObj] = -1
		IF MC_serverBD_2.iCurrentObjRespawnLives[iObj] <= 0
		AND NOT IS_BIT_SET(MC_serverBD_1.sCreatedCount.iSkipObjBitset, iObj)
			bCheckTeamFail = TRUE
		ENDIF
	ENDIF
	
	FOR iTeam = 0 to MC_serverBD.iNumberOfTeams - 1
		// Remove as an objective entity if we are meant to leave it and we are no longer near it.
		IF sObjState.bObjExists
			IF HAS_OBJ_PASSED_LEAVE_CONDITIONS_FOR_RULE(sObjState, iTeam)				
				PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - No player is near Ped: ", iObj, " bcheckteamfail = TRUE")
				bCheckTeamFail = TRUE
			ENDIF
		ENDIF		
		
		IF MC_serverBD_4.iObjPriority[iObj][iTeam] < FMMC_MAX_RULES
		OR IS_BIT_SET(MC_serverBD.iMissionCriticalObj[iTeam], iObj)
			IF bCheckTeamFail
				RUN_TEAM_OBJ_FAIL_CHECKS(iObj,iTeam,MC_serverBD_4.iObjPriority[iObj][iTeam])
			ENDIF
		ENDIF		
		IF MC_serverBD_4.iObjPriority[iObj][iTeam] < FMMC_MAX_RULES
			IF NOT bCheckTeamFail
				IF MC_serverBD_4.iObjRule[iObj][iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
					IF MC_serverBD_4.iObjRule[iObj][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
						IF MC_serverBD_4.iObjPriority[iObj][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam]
							IF IS_ENTITY_IN_DROP_OFF_AREA(sObjState.objIndex, iTeam, MC_serverBD_4.iObjPriority[iObj][iTeam], ciRULE_TYPE_OBJECT, iObj)
								IF NOT IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeam],iObj)
									IF MC_serverBD.iObjCarrier[iObj] > -1
										IF MC_PlayerBD[MC_serverBD.iObjCarrier[iObj]].iTeam = iTeam
											SET_BIT(MC_serverBD.iObjHoldPartPoints[iObj],MC_serverBD.iObjCarrier[iObj])												
											REQUEST_INCREMENT_SERVER_REMOTE_PLAYER_SCORE(MC_serverBD.iObjCarrier[iObj], GET_FMMC_POINTS_FOR_TEAM(iTeam,MC_serverBD_4.iObjPriority[iObj][iTeam]), iTeam, MC_serverBD_4.iObjPriority[iObj][iTeam])
											SET_BIT(MC_serverBD.iObjAtYourHolding[iTeam],iObj)
											SET_BIT(MC_serverBD.iObjCaptured, iObj)
											PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - SETTING PACKAGE AT HOLDING: ",iObj," FOR TEAM: ", iTeam)
										ENDIF
									ELSE
										IF IS_TEAM_ACTIVE(iTeam)
											PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJ_STAGGERED_SERVER - SETTING PACKAGE AT HOLDING: ",iObj," FOR TEAM: ", iTeam)
											SET_BIT(MC_serverBD.iObjAtYourHolding[iTeam],iObj)
											SET_BIT(MC_serverBD.iObjCaptured, iObj)
											IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciALTERNATIVE_HOLDING_PACKAGE_SCORING)
												INCREMENT_SERVER_TEAM_SCORE(iTeam,MC_serverBD_4.iObjPriority[iObj][iTeam],GET_FMMC_POINTS_FOR_TEAM(iTeam,MC_serverBD_4.iObjPriority[iObj][iTeam]))
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF sObjState.bObjExists
								AND IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_SOMEONE)
								OR bContainer
									REMOVE_OBJECT_HOLDING_POINTS(iTeam,iObj)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC PROCESS_OBJ_POST_STAGGERED()
	
	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	
	INT iTeam
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		IF MC_serverBD.iNumObjHighestPriorityHeld[iTeam] != iNumHighPriorityObjHeld[iTeam]
			MC_serverBD.iNumObjHighestPriorityHeld[iTeam] = iNumHighPriorityObjHeld[iTeam] 
			PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_OBJ_POST_STAGGERED - MC_serverBD.iNumObjHighestPriorityHeld[iTeam]   updated to: ",MC_serverBD.iNumObjHighestPriorityHeld[iTeam]," for team: ",iTeam) 
		ENDIF
		
		IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= FMMC_MAX_RULES
			RELOOP
		ENDIF
		
		IF MC_serverBD_4.iObjMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[iTeam]] = ciFMMC_DROP_OFF_TYPE_ALL_COLLECTED
			IF MC_serverBD.iNumObjHighestPriorityHeld[iTeam] = MC_serverBD.iNumObjHighestPriority[iTeam]
				PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_OBJ_POST_STAGGERED - DROP_OFF_TYPE_ALL_COLLECTED - All held, progressing rule.")
				INVALIDATE_SINGLE_OBJECTIVE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], TRUE, TRUE)
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Object Objective Processing
// ##### Description: Client and Server Object Objective Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_OBJ_OBJECTIVES_CLIENT(FMMC_OBJECT_STATE &sObjState)
	
	INT iObj = sObjState.iIndex
	
	IF !sObjState.bObjExists
		EXIT
	ENDIF
	
	IF NOT SHOULD_OBJECT_BE_BLIPPED(sObjState, sMissionObjectsLocalVars[iObj].sBlipRuntimeVars)
	AND NOT SHOULD_HINT_CAM_WORK_FOR_OBJECTIVE_ENTITY_WITHOUT_BLIP(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct)
		EXIT
	ENDIF
	
	INT iTeam = GET_LOCAL_PLAYER_TEAM(TRUE)
	
	IF MC_serverBD_4.iObjPriority[iObj][iTeam] > MC_serverBD_4.iCurrentHighestPriority[iTeam]
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iObjPriority[iObj][iTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iObjRule[iObj][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
		EXIT
	ENDIF
	
	IF MC_serverBD.iObjCarrier[iObj] = iPartToUse
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_WITH_CARRIER)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeam], iObj)
		EXIT
	ENDIF
							
	FLOAT fObjDist2 = GET_DIST2_BETWEEN_ENTITIES(LocalPlayerPed, sObjState.objIndex)
	IF fObjDist2 > fNearestTargetDist2Temp
		EXIT
	ENDIF
	
	iNearestTargetTemp = iObj
	iNearestTargetTypeTemp = ci_TARGET_OBJECT
	fNearestTargetDist2Temp = fObjDist2
	
ENDPROC	

PROC PROCESS_OBJECT_IN_PREREQ_ZONES(FMMC_OBJECT_STATE &sObjState)
	
	IF iRuleEntityPrereqZoneCount <= 0
		EXIT
	ENDIF
	
	IF IS_BIT_SET(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_SOMEONE)
		EXIT
	ENDIF
	
	INT i 
	FOR i = 0 TO iRuleEntityPrereqZoneCount - 1
		IF iZoneIndicesOfTypeRuleEntityPrereq[i] = -1
			RELOOP
		ENDIF
		IF NOT DOES_ZONE_EXIST(iZoneIndicesOfTypeRuleEntityPrereq[i])
			RELOOP
		ENDIF
		
		IF ARE_COORDS_IN_FMMC_ZONE(GET_FMMC_OBJECT_COORDS(sObjState), iZoneIndicesOfTypeRuleEntityPrereq[i])
#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(iTempEntitiesInZoneBS, iZoneIndicesOfTypeRuleEntityPrereq[i])
				PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_IN_PREREQ_ZONES - Object is within zone: ", iZoneIndicesOfTypeRuleEntityPrereq[i])
			ENDIF
#ENDIF
			SET_BIT(iTempEntitiesInZoneBS, iZoneIndicesOfTypeRuleEntityPrereq[i])
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneIndicesOfTypeRuleEntityPrereq[i]].iZoneBS2, ciFMMC_ZONEBS2_CHECK_SAME_INTERIOR)
			IF GET_FMMC_OBJECT_INTERIOR(sObjState) != NULL
			AND GET_INTERIOR_AT_COORDS(GET_ZONE_RUNTIME_CENTRE(iZoneIndicesOfTypeRuleEntityPrereq[i])) = GET_FMMC_OBJECT_INTERIOR(sObjState)
#IF IS_DEBUG_BUILD
				IF NOT IS_BIT_SET(iTempEntitiesInZoneBS, iZoneIndicesOfTypeRuleEntityPrereq[i])
					PRINTLN("[Objects][Object ", sObjState.iIndex, "] PROCESS_OBJECT_IN_PREREQ_ZONES - Object is in the same interior as zone: ", iZoneIndicesOfTypeRuleEntityPrereq[i])
				ENDIF
#ENDIF
				SET_BIT(iTempEntitiesInZoneBS, iZoneIndicesOfTypeRuleEntityPrereq[i])
			ENDIF
		ENDIF
	ENDFOR

ENDPROC

PROC PROCESS_OBJ_OBJECTIVES_SERVER(FMMC_OBJECT_STATE &sObjState, INT iTeam)
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
		
	INT iObj = sObjState.iIndex
	
	IF MC_serverBD_2.iCurrentObjRespawnLives[iObj] <= 0
	
		IF !sObjState.bObjExists
			EXIT
		ENDIF
		
		IF !sObjState.bObjAlive
			EXIT
		ENDIF
		
	ENDIF
	
	IF MC_serverBD_4.iObjRule[iObj][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iObjPriority[iObj][iTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
		
	IF MC_serverBD_4.iObjPriority[iObj][iTeam] > iCurrentHighPriority[iTeam]
		//Has an objective but the team has not reached it yet
		EXIT
	ENDIF
	
	PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_OBJ_OBJECTIVES_SERVER - Object: ", iObj, " Team: ", iTeam, " Objective is valid - Assigning. Priority: ",
				MC_serverBD_4.iObjPriority[iObj][iTeam], " Rule: ", MC_serverBD_4.iVehPriority[iObj][iTeam])
			
	IF MC_serverBD_4.iObjPriority[iObj][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam]
		iTempNumPriorityRespawnObj[iTeam] += MC_serverBD_2.iCurrentObjRespawnLives[iObj]
		PROCESS_OBJECT_IN_PREREQ_ZONES(sObjState)
	ENDIF
	
	iCurrentHighPriority[iTeam] = MC_serverBD_4.iObjPriority[iObj][iTeam]
	iTempObjMissionLogic[iTeam] = MC_serverBD_4.iObjRule[iObj][iTeam]
	iTempObjMissionSubLogic[iTeam] = GET_HACK_SUBLOGIC_FROM_OBJECT(iObj)
	
	IF iOldHighPriority[iTeam] > iCurrentHighPriority[iTeam]
		iNumHighPriorityObj[iTeam] = 0
		iOldHighPriority[iTeam] = iCurrentHighPriority[iTeam]
	ENDIF
	
	iNumHighPriorityObj[iTeam]++
	
	RESET_TEMP_OBJECTIVE_VARS(iTeam, ciRULE_TYPE_OBJECT)
	
	
ENDPROC
