// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - World -------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script Name: FM_Mission_Controller_2020.sc ------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Handles entities in the world not created by us and also some abstract functionality    
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Include_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Door Utility ----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions to be used throughout the door system.  ----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_DOOR_USE_ALT_CONFIGURATION(INT iDoor)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigSwitchers.iConfigSwitchBS, ciDOOR_CONFIG_SWITCHERBS__ONLY_SWITCH_DURING_CUTSCENE)
		IF GET_DOOR_CONFIGURATION_INDEX(iDoor) = ciDOOR_CONFIG__STANDARD // Only check this when going from Standard to Alt - don't go back to Standard when the cutscene ends
			IF NOT g_bInMissionControllerCutscene
			OR NOT IS_CUTSCENE_PLAYING()
				PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "] SHOULD_DOOR_USE_ALT_CONFIGURATION | Blocking due to ciDOOR_CONFIG_SWITCHERBS__ONLY_SWITCH_DURING_CUTSCENE")
				RETURN FALSE
			ELSE
				PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "] SHOULD_DOOR_USE_ALT_CONFIGURATION | Player is watching a cutscene!")
			ENDIF
		ELSE
			PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "] SHOULD_DOOR_USE_ALT_CONFIGURATION | Already in ciDOOR_CONFIG__ALT!")
		ENDIF
	ENDIF
	
	INT iConfigSwitcherTeam = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigSwitchers.iConfigSwitchTeam
	IF iConfigSwitcherTeam > -1
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigSwitchers.iConfigSwitchRuleBS, GET_TEAM_CURRENT_RULE(iConfigSwitcherTeam))
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigSwitchers.iConfigSwitchBS, ciDOOR_CONFIG_SWITCHERBS__SWITCH_AT_MIDPOINT)
				IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iConfigSwitcherTeam], GET_TEAM_CURRENT_RULE(iConfigSwitcherTeam))
					// We're on a rule set to use the Alt config, AND we've passed the rule's midpoint!
					RETURN TRUE
				ENDIF
			ELSE
				// We're on a rule set to use the Alt config!
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigSwitchers.iConfigSwitchZone > -1
		IF IS_ZONE_TRIGGERING(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigSwitchers.iConfigSwitchZone, TRUE)
			PRINTLN("[Doors][Door ", iDoor, "] SHOULD_DOOR_USE_ALT_CONFIGURATION | Using Alt Config due to linked Zone ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigSwitchers.iConfigSwitchZone)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigSwitchers.iConfigSwitchBS, ciDOOR_CONFIG_SWITCHERBS__SWITCH_FROM_PREV_MISSION)
		IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iContinuityId > -1
		AND FMMC_IS_LONG_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iDoorsUseAltConfigBitset, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iContinuityId)
			// Use Alt because the last mission ended with this door using Alt
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigSwitchers.iConfigSwitchBS, ciDOOR_CONFIG_SWITCHERBS__SWITCH_WHEN_LINKED_ENTITY_MISSING)
		IF NOT IS_BIT_SET(iDoorHasLinkedEntityBS, iDoor)
			// Use Alt because this door doesn't have any existing linked entities!
			IF iDoorCurrentConfig[iDoor] != ciDOOR_CONFIG__ALT
				PRINTLN("[Doors][Door ", iDoor, "] SHOULD_DOOR_USE_ALT_CONFIGURATION | Using Alt Config due to having no linked entity!")
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigSwitchers.iConfigSwitchBS, ciDOOR_CONFIG_SWITCHERBS__SWITCH_ON_AGGRO)
		IF HAS_ANY_TEAM_TRIGGERED_AGGRO(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iAggroIndexBS_Entity_Door)
			// Use Alt because this door doesn't have any existing linked entities!
			IF iDoorCurrentConfig[iDoor] != ciDOOR_CONFIG__ALT
				PRINTLN("[Doors][Door ", iDoor, "] SHOULD_DOOR_USE_ALT_CONFIGURATION | Using Alt Config due to aggro!")
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iDoorExternallySetToUseAltConfigBS, iDoor)
		// Use Alt because it's been set to do so externally (by an Interactable etc)
		IF iDoorCurrentConfig[iDoor] != ciDOOR_CONFIG__ALT
			PRINTLN("[Doors][Door ", iDoor, "] SHOULD_DOOR_USE_ALT_CONFIGURATION | Using Alt Config due to iDoorExternallySetToUseAltConfigBS!")
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_RetainAltConfigContinuity)
	AND IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_DoorsUsedAltConfigTracking)
		IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iContinuityId > -1
		AND IS_LONG_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iDoorsUseAltConfigBitset, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iContinuityId)
			// Use Alt Config due to continuity!
			IF iDoorCurrentConfig[iDoor] != ciDOOR_CONFIG__ALT
				PRINTLN("[Continuity][Doors][Door ", iDoor, "] SHOULD_DOOR_USE_ALT_CONFIGURATION | Using Alt Config due to continuity! iContinuityId: ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iContinuityId)
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_DOOR_PHYSICALLY_LOCKED(INT iDoor)
	DOOR_STATE_ENUM eDoorState = DOOR_SYSTEM_GET_DOOR_STATE(GET_DOOR_HASH(iDoor))
	DOOR_STATE_ENUM ePendingDoorState = DOOR_SYSTEM_GET_DOOR_PENDING_STATE(GET_DOOR_HASH(iDoor))
	RETURN eDoorState = DOORSTATE_LOCKED OR ePendingDoorState = DOORSTATE_LOCKED
ENDFUNC

FUNC INT GET_DOOR_CONFIGURATION_INDEX_TO_USE(INT iDoor)
	IF SHOULD_DOOR_USE_ALT_CONFIGURATION(iDoor)
		RETURN ciDOOR_CONFIG__ALT
	ENDIF
	
	RETURN ciDOOR_CONFIG__STANDARD
ENDFUNC


PROC UPDATE_DOOR_CONFIGURATION_INDEX(INT iDoor, INT iConfig = ciDOOR_CONFIG__STANDARD)
	
	IF GET_DOOR_CONFIGURATION_INDEX(iDoor) = iConfig
		EXIT
	ENDIF
	
	iDoorCurrentConfig[iDoor] = iConfig
	SET_BIT(iDoorConfigNeedsToBeAppliedBS, iDoor)
	PRINTLN("[Doors][Door ", iDoor, "] SET_DOOR_CONFIGURATION_INDEX | Door is now using config ", iConfig)
ENDPROC

FUNC BOOL IS_DIRECTIONAL_DOOR_READY_TO_RELOCK(INT iDoor)

	IF NOT IS_DOOR_CLOSED(GET_DOOR_HASH(iDoor))
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC FLOAT GET_AUTOMATIC_DISTANCE_FOR_UNLOCKED_DIRECTIONAL_DOOR(INT iDoor, FMMC_DOOR_CONFIGURATION& sDoorConfig)
	
	IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("prop_gate_prison_01"))
		RETURN sDoorConfig.fAutomaticDistance * 3.0
	ENDIF
	
	RETURN sDoorConfig.fAutomaticDistance
ENDFUNC


FUNC FLOAT GET_AUTOMATIC_DISTANCE_FOR_LOCKED_DIRECTIONAL_DOOR(INT iDoor) 
	
	IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_lobay_gate01"))
		RETURN 2.0
	ENDIF
	
	RETURN 1.0
ENDFUNC

FUNC BOOL IS_PED_VALID_TO_UNLOCK_DIRECTIONAL_DOOR(PED_INDEX tempPed, VECTOR vDoorMiddle)
	
	IF NOT IS_ENTITY_ALIVE(tempPed)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_STILL(tempPed)
	AND NOT IS_PED_IN_ANY_VEHICLE(tempPed)
	AND NOT IS_PED_IN_COMBAT(tempPed)
		RETURN FALSE
	ENDIF
	
	IF GET_ENTITY_SPEED(tempPed) >= 1.0
		VECTOR vPedVelocity = GET_ENTITY_VELOCITY(tempPed)
		VECTOR vPedDirection = NORMALISE_VECTOR(vPedVelocity)
		VECTOR vDirectionToDoorMiddle = NORMALISE_VECTOR(vDoorMiddle - GET_ENTITY_COORDS(tempPed))
		FLOAT fVelocityDOT = DOT_PRODUCT(vPedDirection, vDirectionToDoorMiddle)

		IF fVelocityDOT < 0.65
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_PED_UNLOCK_DIRECTIONAL_DOOR(INT iPed, INT iDoor, VECTOR vUnlockPosition, FLOAT fUnlockDistance, FMMC_DOOR_CONFIGURATION& sDoorConfig)
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
		PED_INDEX tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
		
		IF NOT IS_ENTITY_ALIVE(tempPed)
			RETURN FALSE
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(tempPed)
			fUnlockDistance += (GET_ENTITY_SPEED(tempPed) * 0.5)
		ENDIF
		
		IF IS_PED_VALID_TO_UNLOCK_DIRECTIONAL_DOOR(tempPed, GET_DOOR_MIDDLE(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor], sDoorConfig))
			IF VDIST2(GET_ENTITY_COORDS(tempPed), vUnlockPosition) < POW(fUnlockDistance, 2.0)
				RETURN TRUE
			ELSE
				//PRINTLN("[DirDoorLock_SPAM][Doors_SPAM][Door_SPAM ", iDoor, "] SHOULD_PED_UNLOCK_DIRECTIONAL_DOOR | Ped ", iPed, " is too far away to unlock the door")
			ENDIF
		ELSE
			//PRINTLN("[DirDoorLock_SPAM][Doors_SPAM][Door_SPAM ", iDoor, "] SHOULD_PED_UNLOCK_DIRECTIONAL_DOOR | Ped ", iPed, " isn't valid to unlock the door")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_PED_UNLOCKING_DIRECTIONAL_DOOR(INT iPed, INT iDoor, VECTOR vUnlockPosition, FLOAT fUnlockDistance, FMMC_DOOR_CONFIGURATION& sDoorConfig)
	IF iPed >= g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
	OR iPed < 0
		EXIT
	ENDIF
	
	IF SHOULD_PED_UNLOCK_DIRECTIONAL_DOOR(iPed, iDoor, vUnlockPosition, fUnlockDistance, sDoorConfig)
		#IF IS_DEBUG_BUILD
		IF NOT FMMC_IS_LONG_BIT_SET(iPedsInDirectionalDoorsBS[iDoor], iPed)
			PRINTLN("[DirDoorLock][Doors][Door ", iDoor, "] PROCESS_PED_UNLOCKING_DIRECTIONAL_DOOR | SETTING ", iPed, " in iPedsInDirectionalDoorsBS for door ", iDoor)
		ENDIF
		#ENDIF
		FMMC_SET_LONG_BIT(iPedsInDirectionalDoorsBS[iDoor], iPed)
	ELSE
		#IF IS_DEBUG_BUILD
			IF FMMC_IS_LONG_BIT_SET(iPedsInDirectionalDoorsBS[GET_LONG_BITSET_BIT(iDoor)], iPed)
				PRINTLN("[DirDoorLock][Doors][Door ", iDoor, "] PROCESS_PED_UNLOCKING_DIRECTIONAL_DOOR | Clearing ", iPed, " in iPedsInDirectionalDoorsBS for door ", iDoor)
			ENDIF
		#ENDIF
		FMMC_CLEAR_LONG_BIT(iPedsInDirectionalDoorsBS[iDoor], iPed)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DIRECTIONAL_DOOR_UNLOCK(INT iDoor, FMMC_DOOR_CONFIGURATION& sDoorConfig, VECTOR vUnlockPosition, FLOAT fUnlockDistance)
	
	IF NOT IS_BIT_SET(sDoorConfig.iDoorConfigBS, ciDOOR_CONFIGBS__DIRECTIONAL_ONLY_FOR_PEDS)
		// Unlocked by Player
		IF VDIST2(GET_WORLD_POSITION_OF_ENTITY_BONE(LocalPlayerPed, 0), vUnlockPosition) < (fUnlockDistance * fUnlockDistance)
		AND NOT IS_PED_IN_COVER(LocalPlayerPed)
		AND NOT IS_PED_GOING_INTO_COVER(LocalPlayerPed)
			RETURN TRUE
		ENDIF
		
		// Unlocked by Player's drone
		ENTITY_INDEX eiDrone = GET_LOCAL_DRONE_PROP_OBJ_INDEX()
		IF IS_ENTITY_ALIVE(eiDrone)
			IF VDIST2(GET_ENTITY_COORDS(eiDrone), vUnlockPosition) < (fUnlockDistance * fUnlockDistance)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// Unlocked by placed peds
	INT iPedBS = 0
	FOR iPedBS = 0 TO ciFMMC_PED_BITSET_SIZE - 1
		IF iPedsInDirectionalDoorsBS[iDoor][iPedBS] != 0
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_FMMC_DIRECTIONAL_DOOR_LOCK(INT iDoor, FMMC_DOOR_CONFIGURATION& sDoorConfig)
	
	FMMC_DIRECTIONAL_DOOR_LOCK_TYPE eLockType = sDoorConfig.eDirectionalLockType
	BOOL bCanSendEvent = NOT HAS_NET_TIMER_STARTED(stDirectionalDoorEventTimer)
	FLOAT fUnlockDistance = GET_DOOR_DIRECTIONAL_LOCK_UNLOCK_POSITION_SIZE(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor], sDoorConfig)
	
	IF eLockType = FMMC_DIRECTIONAL_DOOR_LOCK_TYPE__NONE
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bDirectionalDoorDebug
		TEXT_LABEL_63 tlDebugText3D = "D"
		tlDebugText3D += iDoor
		tlDebugText3D += " | H: "
		tlDebugText3D += GET_DOOR_HASH(iDoor)
		tlDebugText3D += " | LUL: "
		tlDebugText3D += GET_STRING_FROM_BOOL(IS_BIT_SET(iDirectionalDoorLocallyUnlockedBS, iDoor))
		tlDebugText3D += " | HUL: "
		tlDebugText3D += GET_STRING_FROM_BOOL(IS_BIT_SET(MC_serverBD_4.sDoorServerData.iHostDirectionalDoorUnlockedBS, iDoor))
		DRAW_DEBUG_TEXT(tlDebugText3D, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos + <<0.0, 0.0, -0.5>>)
		
		tlDebugText3D = " | fDirectionalDoorAutoDistance: "
		tlDebugText3D += FLOAT_TO_STRING(fDirectionalDoorAutoDistance[iDoor])
		DRAW_DEBUG_TEXT(tlDebugText3D, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos + <<0.0, 0.0, -0.95>>)
		
		DRAW_FMMC_DIRECTIONAL_DOOR_LOCK_HUD(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor], sDoorConfig)
	ENDIF
	#ENDIF
	
	IF HAS_NET_TIMER_STARTED(stDirectionalDoorEventTimer)
		IF HAS_NET_TIMER_EXPIRED(stDirectionalDoorEventTimer, ciDirectionalDoorEventCooldownDuration)
			RESET_NET_TIMER(stDirectionalDoorEventTimer)
			PRINTLN("[DirDoorLock][Doors][Door ", iDoor, "] PROCESS_FMMC_DIRECTIONAL_DOOR_LOCK | Resetting stDirectionalDoorEventTimer")
		ENDIF
	ENDIF
				
	/////
	VECTOR vUnlockPosition = GET_DOOR_DIRECTIONAL_LOCK_UNLOCK_POSITION(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor], sDoorConfig)
	
	IF NOT IS_BIT_SET(sDoorConfig.iDoorConfigBS, ciDOOR_CONFIGBS__DIRECTIONAL_OPENS_FOR_HIGH_PRIORITY_PED_ONLY)
		PROCESS_PED_UNLOCKING_DIRECTIONAL_DOOR(iStaggeredPedIterator, iDoor, vUnlockPosition, fUnlockDistance, sDoorConfig)
	ENDIF
	IF sDoorConfig.iDirectionalDoorHighPriorityPed > -1
		PROCESS_PED_UNLOCKING_DIRECTIONAL_DOOR(sDoorConfig.iDirectionalDoorHighPriorityPed, iDoor, vUnlockPosition, fUnlockDistance, sDoorConfig)
	ENDIF
	
	IF SHOULD_DIRECTIONAL_DOOR_UNLOCK(iDoor, sDoorConfig, vUnlockPosition, fUnlockDistance)
		
		IF NOT IS_BIT_SET(iDirectionalDoorLocallyUnlockedBS, iDoor)
			IF bCanSendEvent
				BROADCAST_FMMC_DIRECTIONAL_DOOR_EVENT(iDoor, TRUE, FALSE, GET_AUTOMATIC_DISTANCE_FOR_UNLOCKED_DIRECTIONAL_DOOR(iDoor, sDoorConfig))
				REINIT_NET_TIMER(stDirectionalDoorEventTimer)
				SET_BIT(iDirectionalDoorLocallyUnlockedBS, iDoor)
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bDirectionalDoorDebug
			DRAW_DEBUG_SPHERE(vUnlockPosition, fUnlockDistance, 0, 255, 0, 100)
		ENDIF
		#ENDIF
	ELSE
		IF IS_BIT_SET(iDirectionalDoorLocallyUnlockedBS, iDoor)
			IF IS_DIRECTIONAL_DOOR_READY_TO_RELOCK(iDoor)
				IF bCanSendEvent
					BROADCAST_FMMC_DIRECTIONAL_DOOR_EVENT(iDoor, FALSE, TRUE)
					REINIT_NET_TIMER(stDirectionalDoorEventTimer)
					CLEAR_BIT(iDirectionalDoorLocallyUnlockedBS, iDoor)
				ENDIF
			ENDIF
			
			IF fDirectionalDoorAutoDistance[iDoor] != GET_AUTOMATIC_DISTANCE_FOR_LOCKED_DIRECTIONAL_DOOR(iDoor)
				BROADCAST_FMMC_DIRECTIONAL_DOOR_EVENT(iDoor, FALSE, FALSE, GET_AUTOMATIC_DISTANCE_FOR_LOCKED_DIRECTIONAL_DOOR(iDoor))
				fDirectionalDoorAutoDistance[iDoor] = GET_AUTOMATIC_DISTANCE_FOR_LOCKED_DIRECTIONAL_DOOR(iDoor)
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bDirectionalDoorDebug
			DRAW_DEBUG_SPHERE(vUnlockPosition, fUnlockDistance, 255, 0, 0, 100)
		ENDIF
		#ENDIF
	ENDIF
	
	IF fDirectionalDoorAutoDistance[iDoor] != 0.0
		IF HAS_CONTROL_OF_DOOR(iDoor)
			IF DOOR_SYSTEM_GET_AUTOMATIC_DISTANCE(GET_DOOR_HASH(iDoor)) != fDirectionalDoorAutoDistance[iDoor]
				PRINTLN("[DirDoorLock][Doors][Door ", iDoor, "] PROCESS_FMMC_DIRECTIONAL_DOOR_LOCK | Setting door automatic distance to ", fDirectionalDoorAutoDistance[iDoor])
				DOOR_SYSTEM_SET_AUTOMATIC_DISTANCE(GET_DOOR_HASH(iDoor), fDirectionalDoorAutoDistance[iDoor], IS_DOOR_NETWORKED(iDoor))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UNLOCK_DOOR(INT iDoor, BOOL bInStaggeredLoop = FALSE)
	
	IF bInStaggeredLoop
		PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "] UNLOCK_DOOR | (Staggered Loop) Unlocking door now! Hash: ", GET_DOOR_HASH(iDoor))
	ELSE
		PRINTLN("[Doors][Door ", iDoor, "] UNLOCK_DOOR | Unlocking door now! Hash: ", GET_DOOR_HASH(iDoor))
	ENDIF
	
	IF HAS_CONTROL_OF_DOOR(iDoor)
		DOOR_SYSTEM_SET_DOOR_STATE(GET_DOOR_HASH(iDoor), DOORSTATE_UNLOCKED, IS_DOOR_NETWORKED(iDoor), TRUE)
	ENDIF
ENDPROC

PROC LOCK_DOOR(INT iDoor, BOOL bInStaggeredLoop = FALSE)
	
	IF bInStaggeredLoop
		PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "] LOCK_DOOR | (Staggered Loop) Locking door now! Hash: ", GET_DOOR_HASH(iDoor))
	ELSE
		PRINTLN("[Doors][Door ", iDoor, "] LOCK_DOOR | Locking door now! Hash: ", GET_DOOR_HASH(iDoor))
	ENDIF
	
	IF HAS_CONTROL_OF_DOOR(iDoor)
		DOOR_SYSTEM_SET_DOOR_STATE(GET_DOOR_HASH(iDoor), DOORSTATE_LOCKED, IS_DOOR_NETWORKED(iDoor), TRUE)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DOOR_BE_LOCKED(INT iDoor, FMMC_DOOR_CONFIGURATION& sDoorConfig)
	
	// These two settings take priority when used
	IF sDoorConfig.iBaseLockType = ciDOOR_BASE_LOCK_TYPE__FORCE_UNLOCKED
		PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "] SHOULD_DOOR_BE_LOCKED | Forced unlocked by ciDOOR_BASE_LOCK_TYPE__FORCE_UNLOCKED")
		RETURN FALSE
	ELIF sDoorConfig.iBaseLockType = ciDOOR_BASE_LOCK_TYPE__FORCE_LOCKED
		PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "] SHOULD_DOOR_BE_LOCKED | Forced locked by ciDOOR_BASE_LOCK_TYPE__FORCE_LOCKED")
		RETURN TRUE
	ENDIF
	
	// External unlock/lock reasons (e.g. keypads, minigames)
	IF IS_BIT_SET(iDoorExternallyLockedBS, iDoor)
		PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "] SHOULD_DOOR_BE_LOCKED | Forced locked by iDoorExternallyUnlockedBS")
		RETURN TRUE // Something else in the mission has specifically requested that this door be locked!
		
	ELIF IS_BIT_SET(iDoorExternallyUnlockedBS, iDoor)
		PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "] SHOULD_DOOR_BE_LOCKED | Forced unlocked by iDoorExternallyUnlockedBS")
		RETURN FALSE // Something else in the mission has specifically requested that this door be unlocked!
	ENDIF
	
	// If the door has been swapped out with a dynoprop for an animation, that animation should end with the door closed, so this door should simply stay locked here ready for that
	IF HAS_DOOR_BEEN_SWAPPED_WITH_DYNOPROP(iDoor)
		PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "] SHOULD_DOOR_BE_LOCKED | Locked by HAS_DOOR_BEEN_SWAPPED_WITH_DYNOPROP")
		RETURN TRUE
	ENDIF
	
	// Finally, if nothing else has anything to say about it, use these settings.
	IF sDoorConfig.iBaseLockType = ciDOOR_BASE_LOCK_TYPE__UNLOCKED
		PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "] SHOULD_DOOR_BE_LOCKED | Unlocked by ciDOOR_BASE_LOCK_TYPE__UNLOCKED")
		RETURN FALSE
	ELIF sDoorConfig.iBaseLockType = ciDOOR_BASE_LOCK_TYPE__LOCKED
		PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "] SHOULD_DOOR_BE_LOCKED | Locked by ciDOOR_BASE_LOCK_TYPE__LOCKED")
		RETURN TRUE
		
	ELIF sDoorConfig.iBaseLockType = ciDOOR_BASE_LOCK_TYPE__DIRECTIONAL
		IF IS_BIT_SET(iDirectionalDoorLocallyUnlockedBS, iDoor)
			PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "] SHOULD_DOOR_BE_LOCKED | Unlocked by iDirectionalDoorLocallyUnlockedBS")
			RETURN FALSE
			
		ELIF IS_BIT_SET(MC_serverBD_4.sDoorServerData.iHostDirectionalDoorUnlockedBS, iDoor)
			PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "] SHOULD_DOOR_BE_LOCKED | Unlocked by iHostDirectionalDoorUnlockedBS")
			RETURN FALSE
		ENDIF
	ENDIF
	
	// If in doubt, keep the door locked
	PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "] SHOULD_DOOR_BE_LOCKED | Locked by default")
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_DOOR_LOCK_NEED_A_REFRESH(INT iDoor, BOOL bInStaggeredLoop = FALSE)

	IF bInStaggeredLoop
		RETURN TRUE // Always update doors in the staggered loop
	ENDIF
	
	IF SHOULD_DOOR_BE_LOCKED(iDoor, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[GET_DOOR_CONFIGURATION_INDEX(iDoor)])
		IF NOT IS_THIS_DOOR_PHYSICALLY_LOCKED(iDoor)
			PRINTLN("[Doors][Door ", iDoor, "] DOES_DOOR_LOCK_NEED_A_REFRESH | Door ", iDoor, " needs to be refreshed because it's not physically locked but it should be!")
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_THIS_DOOR_PHYSICALLY_LOCKED(iDoor)
			PRINTLN("[Doors][Door ", iDoor, "] DOES_DOOR_LOCK_NEED_A_REFRESH | Door ", iDoor, " needs to be refreshed because it's physically locked but it should be unlocked!")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_DOOR_LOCK(INT iDoor, FMMC_DOOR_CONFIGURATION& sDoorConfig, BOOL bInStaggeredLoop = FALSE)
	
	IF sDoorConfig.iBaseLockType = ciDOOR_BASE_LOCK_TYPE__DIRECTIONAL
		IF NOT bInStaggeredLoop
			PROCESS_FMMC_DIRECTIONAL_DOOR_LOCK(iDoor, sDoorConfig)
		ENDIF
	ENDIF
	
	IF DOES_DOOR_LOCK_NEED_A_REFRESH(iDoor, bInStaggeredLoop)
		IF SHOULD_DOOR_BE_LOCKED(iDoor, sDoorConfig)
			LOCK_DOOR(iDoor, bInStaggeredLoop)
		ELSE
			UNLOCK_DOOR(iDoor, bInStaggeredLoop)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_DOOR_BE_HELD_OPEN(INT iDoor, FMMC_DOOR_CONFIGURATION& sDoorConfig)
	
	IF SHOULD_DOOR_BE_LOCKED(iDoor, sDoorConfig)
		RETURN FALSE
	ENDIF

	IF sDoorConfig.fAutomaticDistance > 0
	OR sDoorConfig.fAutomaticRate > 0.0
		// This is an automatic door and can't support this behaviour
		RETURN FALSE
	ENDIF
	
	RETURN sDoorConfig.fOpenRatio != 0.0
ENDFUNC

/// PURPOSE:
///    Sets up or removes an existing model swap where appropriate based on the given config. Returns TRUE when the door has changed.
/// PARAMS:
///    iDoor - 
///    sDoorConfig - 
/// RETURNS:
///    
FUNC BOOL APPLY_DOOR_MODEL_SWAP(INT iDoor, FMMC_DOOR_CONFIGURATION& sDoorConfig)

	IF sDoorConfig.iDoorModelSwapStringIndex > -1
		IF NOT IS_BIT_SET(iDoorModelSwappedBS, iDoor)
			PRINTLN("[Doors][Door ", iDoor, "] APPLY_DOOR_CONFIGURATION | Applying model swap from config!")
			
			// Swap the door's model to the new model specified in this config
			CREATE_MODEL_SWAP(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, 1.0, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel, GET_DOOR_MODEL(iDoor), TRUE)
			REMOVE_DOOR_FROM_SYSTEM(GET_DOOR_HASH(iDoor)) // Remove the current door so we can register the new model swap object instead
			SET_DOOR_NEEDS_TO_REFRESH(iDoor)
			
			SET_BIT(iDoorModelSwappedBS, iDoor)
			RETURN TRUE
		ENDIF
		
	ELIF GET_DOOR_MODEL(iDoor) = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel
		IF IS_BIT_SET(iDoorModelSwappedBS, iDoor)
			PRINTLN("[Doors][Door ", iDoor, "] APPLY_DOOR_CONFIGURATION | Clearing model swap from config, going back to the normal model!")
			
			// Return it to normal
			REMOVE_MODEL_SWAP(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, 1.0, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel, GET_DOOR_MODEL(iDoor))
			REMOVE_DOOR_FROM_SYSTEM(GET_DOOR_HASH(iDoor)) // Remove the current door so we can register the new model swap object instead
			SET_DOOR_NEEDS_TO_REFRESH(iDoor)
			
			CLEAR_BIT(iDoorModelSwappedBS, iDoor)
			RETURN TRUE
		ENDIF
		
	ENDIF
		
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_DOOR_SCRIPTED_SOUNDS(INT iDoor, SCRIPTED_DOOR_SOUNDS& sDoorSounds)
	IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_finelevdoor01"))
		sDoorSounds.sDoorOpening = "OPENING"
		sDoorSounds.sDoorClosing = "CLOSING"
		sDoorSounds.sDoorOpened = "OPENED"
		sDoorSounds.sDoorClosed = "Fake_Close"
		sDoorSounds.sDoorSoundset = "Union_Depository_Elevator_Sounds"
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PLAY_DOOR_MOVE_SOUND(INT iDoor, FMMC_DOOR_CONFIGURATION& sDoorConfig)

	FLOAT fCurrentOpenRatio = DOOR_SYSTEM_GET_OPEN_RATIO(GET_DOOR_HASH(iDoor))
	FLOAT fNewTargetOpenRadio = sDoorConfig.fOpenRatio
	FLOAT fTargetRatioDifference = ABSF(fCurrentOpenRatio - fNewTargetOpenRadio)
	
	IF fTargetRatioDifference > 0.1
	
		SCRIPTED_DOOR_SOUNDS sDoorSounds
		IF GET_DOOR_SCRIPTED_SOUNDS(iDoor, sDoorSounds)
			BOOL bPlayOpenSound = (ABSF(fCurrentOpenRatio) < ABSF(fTargetRatioDifference))
			BOOL bPlayClosingSound = (ABSF(fCurrentOpenRatio) > ABSF(fTargetRatioDifference))
			
			IF bPlayOpenSound
				PLAY_SOUND_FROM_COORD(-1, sDoorSounds.sDoorOpening, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, sDoorSounds.sDoorSoundset)
			ELIF bPlayClosingSound
				PLAY_SOUND_FROM_COORD(-1, sDoorSounds.sDoorClosing, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, sDoorSounds.sDoorSoundset)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PLAY_DOOR_FINISH_MOVING_SOUND(INT iDoor, FMMC_DOOR_CONFIGURATION& sDoorConfig)

	BOOL bPlayOpenSound = ABSF(sDoorConfig.fOpenRatio) > 0.5
	
	SCRIPTED_DOOR_SOUNDS sDoorSounds
	IF GET_DOOR_SCRIPTED_SOUNDS(iDoor, sDoorSounds)
		IF bPlayOpenSound
			PLAY_SOUND_FROM_COORD(-1, sDoorSounds.sDoorOpened, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, sDoorSounds.sDoorSoundset)
		ELSE
			PLAY_SOUND_FROM_COORD(-1, sDoorSounds.sDoorClosed, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, sDoorSounds.sDoorSoundset)
		ENDIF
	ENDIF
ENDPROC

PROC APPLY_DOOR_CONFIGURATION(INT iDoor, FMMC_DOOR_CONFIGURATION& sDoorConfig)
	
	PRINTLN("[Doors][Door ", iDoor, "] APPLY_DOOR_CONFIGURATION | Applying door config!")
	
	IF APPLY_DOOR_MODEL_SWAP(iDoor, sDoorConfig)
		// Exit here - a model swap has happened, so the door needs to be fully re-initialised
		EXIT
	ENDIF
	
	IF NOT HAS_CONTROL_OF_DOOR(iDoor)
		PRINTLN("[Doors][Door ", iDoor, "] APPLY_DOOR_CONFIGURATION | Exiting - we don't have control of this door")
		EXIT
	ENDIF
	
	DOOR_SYSTEM_SET_HOLD_OPEN(GET_DOOR_HASH(iDoor), SHOULD_DOOR_BE_HELD_OPEN(iDoor, sDoorConfig))
	DOOR_SYSTEM_SET_SPRING_REMOVED(GET_DOOR_HASH(iDoor), SHOULD_DOOR_BE_HELD_OPEN(iDoor, sDoorConfig), IS_DOOR_NETWORKED(iDoor))
	
	IF NOT IS_BIT_SET(sDoorConfig.iDoorConfigBS, ciDOOR_CONFIGBS__INTERPOLATE_TO_OPEN_RATIO)
		IF SHOULD_DOOR_BE_LOCKED(iDoor, sDoorConfig)
			DOOR_SYSTEM_SET_OPEN_RATIO(GET_DOOR_HASH(iDoor), sDoorConfig.fOpenRatio, IS_DOOR_NETWORKED(iDoor), TRUE)
			PRINTLN("[Doors][Door ", iDoor, "] APPLY_DOOR_CONFIGURATION | Setting door's open ratio to ", sDoorConfig.fOpenRatio)
		ELSE
			// Unlocked doors shouldn't be using open ratios as it causes confusion with some doors and prevents automatic door functionality from kicking in
			// We set the open ratio to 0.0 as it resets script control of the open ratio, and will re-activate any automatic door functionality.
			DOOR_SYSTEM_SET_OPEN_RATIO(GET_DOOR_HASH(iDoor), 0.0, IS_DOOR_NETWORKED(iDoor), TRUE)
			PRINTLN("[Doors][Door ", iDoor, "] APPLY_DOOR_CONFIGURATION | Clearing door's open ratio")
		ENDIF
	ELSE
		PLAY_DOOR_MOVE_SOUND(iDoor, sDoorConfig)
		
	ENDIF
	
	IF sDoorConfig.fAutomaticRate > 0.0
		PRINTLN("[Doors][Door ", iDoor, "] APPLY_DOOR_CONFIGURATION | Setting door's automatic rate to ", sDoorConfig.fAutomaticRate)
		DOOR_SYSTEM_SET_AUTOMATIC_RATE(GET_DOOR_HASH(iDoor), sDoorConfig.fAutomaticRate, IS_DOOR_NETWORKED(iDoor))
	ENDIF
	
	IF sDoorConfig.fAutomaticDistance > 0.0
		PRINTLN("[Doors][Door ", iDoor, "] APPLY_DOOR_CONFIGURATION | Setting door's automatic distance to ", sDoorConfig.fAutomaticDistance)
		DOOR_SYSTEM_SET_AUTOMATIC_DISTANCE(GET_DOOR_HASH(iDoor), sDoorConfig.fAutomaticDistance, IS_DOOR_NETWORKED(iDoor))
	ENDIF
	
	CLEAR_BIT(iDoorConfigNeedsToBeAppliedBS, iDoor)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Door Animation Dynoprops ----------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions that are related to having dynoprops that swap in and out with the real Doors for the purposes of sync-scene animations.  ------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL INIT_DOOR_ANIMATION_DYNOPROP(INT iDoor)
	
	INTERIOR_INSTANCE_INDEX iiDoorInterior = GET_INTERIOR_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
	IF iiDoorInterior != NULL
		IF NOT IS_INTERIOR_READY(iiDoorInterior)
			PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops] INIT_DOOR_ANIMATION_DYNOPROP | Door's interior isn't ready!")
			RETURN FALSE
		ENDIF
		
		IF iiDoorInterior != LocalPlayerCurrentInterior
			PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops] INIT_DOOR_ANIMATION_DYNOPROP | Door's interior isn't relevant yet!")
			RETURN FALSE
		ENDIF
	ENDIF

	OBJECT_INDEX oiDoorDynoprop = GET_DOOR_ANIMATION_DYNOPROP(iDoor)
	OBJECT_INDEX oiDoorObject = GET_DOOR_OBJECT(iDoor)
	
	IF NOT DOES_ENTITY_EXIST(oiCachedDoorObjects[iDoor])
		PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops] INIT_DOOR_ANIMATION_DYNOPROP | Door reference doesn't exist yet!")
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(oiDoorDynoprop)
		PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops] INIT_DOOR_ANIMATION_DYNOPROP | Dynoprop ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iDoorAnimDynopropIndex, " doesn't exist!")
		RETURN FALSE
	ENDIF
	
	IF oiDoorDynoprop = oiDoorObject
		PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops] INIT_DOOR_ANIMATION_DYNOPROP | Haven't found the two door entities correctly!")
		RETURN FALSE
	ENDIF
	
	VECTOR vCurrentDynopropCoords = GET_ENTITY_COORDS(oiDoorDynoprop, FALSE)
	VECTOR vDesiredDynopropCoords = GET_COORDS_FOR_DOOR_ANIMATION_DYNOPROP(iDoor, HAS_DOOR_BEEN_SWAPPED_WITH_DYNOPROP(iDoor))
	
	IF NETWORK_HAS_CONTROL_OF_ENTITY(oiDoorDynoprop)
		// Position the dynoprop with a very small offset, to allow our "find object" natives to tell the difference between the dynoprop and the real door later
		SET_ENTITY_COORDS(oiDoorDynoprop, vDesiredDynopropCoords, FALSE)
		PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops] INIT_DOOR_ANIMATION_DYNOPROP | Moving Dynoprop ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iDoorAnimDynopropIndex, " to ", vDesiredDynopropCoords)
		NETWORK_USE_HIGH_PRECISION_BLENDING(OBJ_TO_NET(oiDoorDynoprop), TRUE)
		
		SET_ENTITY_DYNAMIC(oiDoorDynoprop, FALSE)
		SET_ENTITY_ROTATION(oiDoorDynoprop, GET_ENTITY_ROTATION(oiDoorObject), DEFAULT, FALSE)
		SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiDoorDynoprop, FALSE)
	ENDIF
	
	SET_ENTITY_ALPHA(oiDoorDynoprop, 0, FALSE)
	SET_ENTITY_NO_COLLISION_ENTITY(oiDoorObject, oiDoorDynoprop, FALSE)
	
	PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops] INIT_DOOR_ANIMATION_DYNOPROP | Dynoprop ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iDoorAnimDynopropIndex, " has been initialised on our end! vCurrentDynopropCoords: ", vCurrentDynopropCoords)
	SET_BIT(MC_playerBD_1[iLocalPart].iDoorAnimDynopropInitialisedBS, iDoor)
	RETURN TRUE
	
ENDFUNC

PROC DEACTIVATE_DOOR_ANIMATION_DYNOPROP(INT iDoor)

	IF NOT IS_BIT_SET(MC_playerBD_1[iLocalPart].iDoorAnimDynopropInitialisedBS, iDoor)
		EXIT
	ENDIF
	
	OBJECT_INDEX oiDoorDynoprop = GET_DOOR_ANIMATION_DYNOPROP(iDoor)
	
	IF NOT DOES_ENTITY_EXIST(oiDoorDynoprop)
		PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops] DEACTIVATE_DOOR_ANIMATION_DYNOPROP | Dynoprop ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iDoorAnimDynopropIndex, " doesn't exist!")
		EXIT
	ENDIF
	
	PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops] DEACTIVATE_DOOR_ANIMATION_DYNOPROP | Deactivating door's dynoprop stuff now!")
	
	SET_ENTITY_COORDS(oiDoorDynoprop, GET_COORDS_FOR_DOOR_ANIMATION_DYNOPROP(iDoor, FALSE), FALSE)
	CLEAR_BIT(MC_playerBD_1[iLocalPart].iDoorAnimDynopropInitialisedBS, iDoor)
ENDPROC

PROC PROCESS_DOOR_ANIMATION_DYNOPROP(INT iDoor)
	IF NOT DOES_DOOR_HAVE_ANIMATION_DYNOPROP(iDoor)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(MC_playerBD_1[iLocalPart].iDoorAnimDynopropInitialisedBS, iDoor)
		INIT_DOOR_ANIMATION_DYNOPROP(iDoor)
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(GET_DOOR_OBJECT(iDoor))
		DEACTIVATE_DOOR_ANIMATION_DYNOPROP(iDoor)
		EXIT
	ENDIF
	
	IF HAS_DOOR_BEEN_SWAPPED_WITH_DYNOPROP(iDoor)
		DOOR_SYSTEM_SET_OPEN_RATIO(GET_DOOR_HASH(iDoor), 0.0, IS_DOOR_NETWORKED(iDoor), TRUE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD4)
		PERFORM_NETWORKED_DOOR_ANIMATION_DYNOPROP_SWAP(iDoor, TRUE)
	ELIF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
		PERFORM_NETWORKED_DOOR_ANIMATION_DYNOPROP_SWAP(iDoor, FALSE)
	ENDIF
	#ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Door Init -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions that register and initialise doors.  -------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_DOOR_DO_INTERIOR_LOAD_CHECK(INT iDoor)

	IF IS_BIT_SET(gMC_LocalVariables_VARS.iLocalBGScriptBS, ciLocalBGScriptBS_SkipDoorInteriorChecks)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_ForceInteriorLoadCheck)
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = v_ilev_hd_door_l
	OR g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = v_ilev_garageliftdoor
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_DOOR_BE_INITIALISED(INT iDoor)

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_HideDoorDuringCutscene)
		IF IS_CUTSCENE_PLAYING()
			PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "] CAN_DOOR_BE_INITIALISED | Can't be initialised due to a cutscene currently playing & ciDoorBS_HideDoorDuringCutscene being set!")	
			RETURN FALSE
		ENDIF
		
		IF IS_A_STRAND_MISSION_BEING_INITIALISED()
			PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "] CAN_DOOR_BE_INITIALISED | Can't be initialised due to a strand transition currently happening & ciDoorBS_HideDoorDuringCutscene being set!")	
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF SHOULD_DOOR_DO_INTERIOR_LOAD_CHECK(iDoor)
		VECTOR vInteriorCheckOffset = <<0, 0, 0.35>>
		INTERIOR_INSTANCE_INDEX iiDoorInterior = GET_INTERIOR_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos + vInteriorCheckOffset)
		
		IF IS_VALID_INTERIOR(iiDoorInterior)
			IF NOT IS_INTERIOR_DISABLED(iiDoorInterior)
				IF IS_INTERIOR_READY(iiDoorInterior)
					PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "] CAN_DOOR_BE_INITIALISED | Door is inside interior: ", NATIVE_TO_INT(iiDoorInterior), " which is ready!")
				ELSE
					PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "] CAN_DOOR_BE_INITIALISED | Door is inside interior: ", NATIVE_TO_INT(iiDoorInterior), " which is still loading!")
					RETURN FALSE
				ENDIF
			ELSE
				PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "] CAN_DOOR_BE_INITIALISED | Door is inside interior: ", NATIVE_TO_INT(iiDoorInterior), " but it's disabled so we won't worry about it")
				
			ENDIF
		ELSE
			PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "] CAN_DOOR_BE_INITIALISED | Door isn't inside an interior")
			
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL INIT_DOOR(INT iDoor)
	
	IF HAS_DOOR_BEEN_INITIALISED(iDoor)
		PRINTLN("[Doors][Door ", iDoor, "] INIT_DOOR | ", GET_DOOR_HASH(iDoor), " has already been initialised!")
		RETURN TRUE
	ENDIF
	
	IF NOT CAN_DOOR_BE_INITIALISED(iDoor)
		PRINTLN("[Doors][Door ", iDoor, "] INIT_DOOR | ", GET_DOOR_HASH(iDoor), " returning FALSE straight away due to CAN_DOOR_BE_INITIALISED!")
		RETURN FALSE
	ENDIF
	
	INT iHash
	IF DOOR_SYSTEM_FIND_EXISTING_DOOR(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, GET_DOOR_MODEL(iDoor), iHash)
		PRINTLN("[Doors][Door ", iDoor, "] GET_DOOR_HASH | Found an existing door! It was registered with hash: ", iHash)
		iCachedDoorHashes[iDoor] = iHash
	ENDIF
	
	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(GET_DOOR_HASH(iDoor))
		PRINTLN("[Doors][Door ", iDoor, "] INIT_DOOR | Registering door! Hash: ", GET_DOOR_HASH(iDoor), " // Door model to use: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_DOOR_MODEL(iDoor)), " / g_FMMC_STRUCT_ENTITIES.sSelectedDoor[", iDoor, "].vPos: ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
		ADD_DOOR_TO_SYSTEM(GET_DOOR_HASH(iDoor), GET_DOOR_MODEL(iDoor), g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, FALSE, SHOULD_DOOR_BE_NETWORKED(iDoor))
	ELSE
		PRINTLN("[Doors][Door ", iDoor, "] INIT_DOOR | ", GET_DOOR_HASH(iDoor), " is already registered!")
	ENDIF
	
	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(GET_DOOR_HASH(iDoor))
		PRINTLN("[Doors][Door ", iDoor, "] INIT_DOOR | Door registration failed! Hash: ", GET_DOOR_HASH(iDoor))
		RETURN FALSE
	ENDIF
	
	PRINTLN("[Doors][Door ", iDoor, "] INIT_DOOR | Initialising Door with config ", GET_DOOR_CONFIGURATION_INDEX_TO_USE(iDoor), " || Networked: ", GET_STRING_FROM_BOOL(IS_DOOR_NETWORKED(iDoor)))
	oiCachedDoorObjects[iDoor] = GET_DOOR_OBJECT(iDoor)
	APPLY_DOOR_CONFIGURATION(iDoor, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[GET_DOOR_CONFIGURATION_INDEX_TO_USE(iDoor)])
	PROCESS_DOOR_LOCK(iDoor, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[GET_DOOR_CONFIGURATION_INDEX_TO_USE(iDoor)])
	UPDATE_DOOR_CONFIGURATION_INDEX(iDoor, GET_DOOR_CONFIGURATION_INDEX_TO_USE(iDoor))
	
	IF DOES_DOOR_HAVE_ANIMATION_DYNOPROP(iDoor)
		INIT_DOOR_ANIMATION_DYNOPROP(iDoor)
	ENDIF
	
	SET_BIT(iDoorHasBeenInitialisedBS, iDoor)
	RETURN TRUE
	
ENDFUNC

PROC INIT_DOORS()
	
	PRINTLN("[RCC MISSION][Doors] INIT_DOORS | Initialising doors now!")
	
	IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCMissionBS, iFMMCMissionBS__MISSION_CONTROLLER_IS_CONTROLLING_DOORS)
		SET_BIT(g_TransitionSessionNonResetVars.iFMMCMissionBS, iFMMCMissionBS__MISSION_CONTROLLER_IS_CONTROLLING_DOORS)
		PRINTLN("[RCC MISSION][Doors] INIT_DOORS | Setting iFMMCMissionBS__MISSION_CONTROLLER_IS_CONTROLLING_DOORS")
	ENDIF
	
	INT iDoor
	FOR iDoor = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDoors - 1
		INIT_DOOR(iDoor)
	ENDFOR
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Every-Frame Door Processing -------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Every-frame loop used to process door logic.  --------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_HIDE_DOORS_FOR_CUTSCENES(INT iDoor)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_HideDoorDuringCutscene)
		
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 60 = 0
			PRINTLN("[RCC MISSION][CutsceneDoorHide] PROCESS_HIDE_DOORS_FOR_CUTSCENES - iDoor ", iDoor, " Should hide in cutscenes.")
		ENDIF
		#ENDIF
		
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)			
			IF IS_MODEL_VALID(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel)
				
				IF IS_A_STRAND_MISSION_BEING_INITIALISED()
					PRINTLN("[RCC MISSION][CutsceneDoorHide] PROCESS_HIDE_DOORS_FOR_CUTSCENES - iDoor ", iDoor, " We are in a strand transition. Cleanup the door early so that it does not appear in the cutscene.")
					CLEANUP_DOOR(iDoor)
						
				ELIF (IS_CUTSCENE_PLAYING() OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet4, PBBOOL4_REACHED_CUT_PROCESS_SHOTS))
				AND g_bInMissionControllerCutscene
					
					IF NOT IS_BIT_SET(iDoorBS_HiddenForCutscene, iDoor)
						OBJECT_INDEX oiDoor = GET_WORLD_DOOR_OBJECT(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
						IF DOES_ENTITY_EXIST(oiDoor)
							PRINTLN("[RCC MISSION][CutsceneDoorHide] PROCESS_HIDE_DOORS_FOR_CUTSCENES - In a cutscene, Hiding iDoor ", iDoor)
							SET_ENTITY_ALPHA(oiDoor, 0, FALSE)							
						ELSE
							PRINTLN("[RCC MISSION][CutsceneDoorHide] PROCESS_HIDE_DOORS_FOR_CUTSCENES - In a cutscene, Would hide iDoor ", iDoor, " but no door exists")
						ENDIF
						SET_BIT(iDoorBS_HiddenForCutscene, iDoor)
					ENDIF
				ELIF IS_BIT_SET(iDoorBS_HiddenForCutscene, iDoor)
					OBJECT_INDEX oiDoor = GET_WORLD_DOOR_OBJECT(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)				
					CLEAR_BIT(iDoorBS_HiddenForCutscene, iDoor)
					IF DOES_ENTITY_EXIST(oiDoor)
						PRINTLN("[RCC MISSION][CutsceneDoorHide] PROCESS_HIDE_DOORS_FOR_CUTSCENES - No longer in a cutscene, Showing iDoor ", iDoor)
						SET_ENTITY_ALPHA(oiDoor, 255, FALSE)
					ELSE
						PRINTLN("[RCC MISSION][CutsceneDoorHide] PROCESS_HIDE_DOORS_FOR_CUTSCENES -  No longer in a cutscene, Would show iDoor ", iDoor, " but no door exists")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// WIDGET PATH > Task Tuning Non Final/Combat Tasks Non Final/CCoverDebug/BoundingAreas/RenderBlockingAreas
PROC PROCESS_DOOR_COVER_BLOCKING_ZONE(INT iDoor)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_CreateCoverBlockingAreaAroundDoor)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iDoorCoverAreaBlocked, iDoor)
		EXIT
	ENDIF
	
	MODEL_NAMES mnDoor = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel
	VECTOR vPos = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos
	VECTOR vPos1, vPos2
	VECTOR vMax, vMin	
	
	IF IS_VECTOR_ZERO(vPos)
		PRINTLN("[LM][CoverBlockDoor] - PROCESS_DOOR_COVER_BLOCKING_ZONE - iDoor: ", iDoor, " Cannot Create - Vector is Zero...")
		EXIT
	ENDIF
	
	IF NOT IS_MODEL_VALID(mnDoor)
		PRINTLN("[LM][CoverBlockDoor] - PROCESS_DOOR_COVER_BLOCKING_ZONE - iDoor: ", iDoor, " Cannot Create - Invalid Model Name...")
		EXIT
	ENDIF
	
	GET_MODEL_DIMENSIONS(mnDoor, vMin, vMax)	
	
	PRINTLN("[LM][CoverBlockDoor] - PROCESS_DOOR_COVER_BLOCKING_ZONE - iDoor: ", iDoor, " GET_MODEL_DIMENSIONS - vMin: ", vMin, " vMax: ", vMax)

	vMin *= 1.25
	vMax *= 1.25
	
	IF vMax.x > vMax.y
		vMax.y = vMax.x
	ELSE
		vMax.x = vMax.y
	ENDIF
	
	IF vMin.x > vMin.y
		vMin.x = vMin.y
	ELSE
		vMin.y = vMin.x
	ENDIF

	vPos1 = vPos+vMin
	vPos2 = vPos+vMax
	
	IF ABSF(vMin.x) != vMax.x
		vPos1.x -= (vMax.x)
	ENDIF
	
	IF ABSF(vMin.y) != vMax.y
		vPos1.y -= (vMax.y)
	ENDIF		
		
	vPos1.z -= vMax.z
	vPos2.z += 0.5
	
	ADD_COVER_BLOCKING_AREA(vPos1, vPos2, FALSE, FALSE, TRUE, FALSE)
	SET_BIT(iDoorCoverAreaBlocked, iDoor)
	
	PRINTLN("[LM][CoverBlockDoor] - PROCESS_DOOR_COVER_BLOCKING_ZONE - iDoor: ", iDoor, " Added Cover Blocking Area Between Coords - vPos1: ", vPos1, " vPos2: ", vPos2)
ENDPROC

FUNC BOOL SHOULD_THIS_DOOR_CONFIGURATION_BE_APPLIED_THIS_FRAME(INT iDoor)
	IF IS_BIT_SET(iDoorConfigNeedsToBeAppliedBS, iDoor)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_OPEN_RATIO_INTERPOLATION(INT iDoor, FMMC_DOOR_CONFIGURATION& sDoorConfig)
	FLOAT fCurrentOpenRatio = DOOR_SYSTEM_GET_OPEN_RATIO(GET_DOOR_HASH(iDoor))
	
	FLOAT fDistanceFromTargetOpenRatio = ABSF(fCurrentOpenRatio - sDoorConfig.fOpenRatio)
	FLOAT fSnapDistance = 0.04
	
	FLOAT fAutomaticRateToUse = 0.75
	IF sDoorConfig.fAutomaticRate > 0
		fAutomaticRateToUse = sDoorConfig.fAutomaticRate
	ENDIF
	
	IF fDistanceFromTargetOpenRatio < fSnapDistance
		// Door has already reached its target!
		EXIT
	ENDIF
	
	// Just a basic lerp for now - We don't know how the door will work exactly so we'll see how the asset ends up before making this perfect.
	// We may not have to interpolate the door ourselves at all in the end.
	fCurrentOpenRatio = COSINE_INTERP_FLOAT(fCurrentOpenRatio, sDoorConfig.fOpenRatio, fAutomaticRateToUse * 7.0 * GET_FRAME_TIME())
	PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "] PROCESS_OPEN_RATIO_INTERPOLATION | fCurrentOpenRatio: ", fCurrentOpenRatio, " / fAutomaticRateToUse: ", fAutomaticRateToUse)
	DOOR_SYSTEM_SET_OPEN_RATIO(GET_DOOR_HASH(iDoor), fCurrentOpenRatio, IS_DOOR_NETWORKED(iDoor), TRUE)
	
	IF ABSF(fCurrentOpenRatio - sDoorConfig.fOpenRatio) < fSnapDistance
		// Door has reached its target just now!
		PLAY_DOOR_FINISH_MOVING_SOUND(iDoor, sDoorConfig)
		DOOR_SYSTEM_SET_OPEN_RATIO(GET_DOOR_HASH(iDoor), sDoorConfig.fOpenRatio, IS_DOOR_NETWORKED(iDoor), TRUE)
		PRINTLN("[Doors][Door ", iDoor, "] PROCESS_OPEN_RATIO_INTERPOLATION | Door finished interpolating open ratio to: ", sDoorConfig.fOpenRatio)
	ENDIF
	
ENDPROC

PROC PROCESS_DOOR_CONFIGURATION(INT iDoor, FMMC_DOOR_CONFIGURATION& sDoorConfig)
	
	IF SHOULD_THIS_DOOR_CONFIGURATION_BE_APPLIED_THIS_FRAME(iDoor)
		APPLY_DOOR_CONFIGURATION(iDoor, sDoorConfig)
	ENDIF
	
	// Lock should always be processed as it can be changed in many ways
	PROCESS_DOOR_LOCK(iDoor, sDoorConfig)
	
	IF IS_BIT_SET(sDoorConfig.iDoorConfigBS, ciDOOR_CONFIGBS__INTERPOLATE_TO_OPEN_RATIO)
	AND SHOULD_DOOR_BE_LOCKED(iDoor, sDoorConfig)
		PROCESS_OPEN_RATIO_INTERPOLATION(iDoor, sDoorConfig)
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_DOOR_BE_PROCESSED_THIS_FRAME(INT iDoor)
	
	IF SHOULD_THIS_DOOR_CONFIGURATION_BE_APPLIED_THIS_FRAME(iDoor)
		// If this door's config should change/be reapplied, process it this frame
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_HideDoorDuringCutscene)
	AND (g_bInMissionControllerCutscene OR IS_BIT_SET(iDoorBS_HiddenForCutscene, iDoor) OR IS_A_STRAND_MISSION_BEING_INITIALISED())
		// Either we're in a cutscene and need to hide, or we need to unhide ourselves at some point
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[GET_DOOR_CONFIGURATION_INDEX(iDoor)].iBaseLockType = ciDOOR_BASE_LOCK_TYPE__DIRECTIONAL
		// Directional doors need to be processed every frame
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[GET_DOOR_CONFIGURATION_INDEX(iDoor)].iDoorConfigBS, ciDOOR_CONFIGBS__INTERPOLATE_TO_OPEN_RATIO)
		// Door needs to update this frame to update its open ratio
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_CreateCoverBlockingAreaAroundDoor)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iDoorNeedsRefreshBS, iDoor)
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iDoorAnimDynopropIndex > -1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_DOOR_EVERY_FRAME_CLIENT(INT iDoor)
	
	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(GET_DOOR_HASH(iDoor))
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(GET_DOOR_HASH(iDoor))
			PRINTLN("[Doors][Door ", iDoor, "] PROCESS_DOOR_EVERY_FRAME_CLIENT | Door isn't registered! Clearing data so that it can be re-initialised below. Hash: ", GET_DOOR_HASH(iDoor))
			CLEAR_BIT(iDoorHasBeenInitialisedBS, iDoor)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PROCESS_DOOR_DEBUG(iDoor, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[GET_DOOR_CONFIGURATION_INDEX(iDoor)], GET_DOOR_CONFIGURATION_INDEX(iDoor))
	#ENDIF
	
	IF NOT HAS_DOOR_BEEN_INITIALISED(iDoor)
		// This door hasn't been grabbed & initialised yet!
		IF NOT INIT_DOOR(iDoor)
			// We failed to grab it again
			EXIT
		ENDIF
	ENDIF
	
	// Change the configuration if needed. This doesn't change the door physically, but notifies the Door system that this particular door needs an update this frame.
	UPDATE_DOOR_CONFIGURATION_INDEX(iDoor, GET_DOOR_CONFIGURATION_INDEX_TO_USE(iDoor))
	
	IF NOT SHOULD_DOOR_BE_PROCESSED_THIS_FRAME(iDoor)
		EXIT
	ENDIF
	
	IF HAS_CONTROL_OF_DOOR(iDoor)
		PROCESS_DOOR_CONFIGURATION(iDoor, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[GET_DOOR_CONFIGURATION_INDEX(iDoor)])
	ENDIF
	
	PROCESS_HIDE_DOORS_FOR_CUTSCENES(iDoor)
	PROCESS_DOOR_COVER_BLOCKING_ZONE(iDoor)
	PROCESS_DOOR_ANIMATION_DYNOPROP(iDoor)
	
	CLEAR_BIT(iDoorNeedsRefreshBS, iDoor)
	
ENDPROC

PROC PROCESS_DOOR_PRE_EVERY_FRAME()
	
ENDPROC

PROC PROCESS_DOOR_POST_EVERY_FRAME()
	
ENDPROC

PROC PROCESS_DOOR_EVERY_FRAME_SERVER(INT iDoor)
	UNUSED_PARAMETER(iDoor)
	//Not sure what server needs to do yet
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Staggered Door Processing ---------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Staggered loop used to process door logic.  ----------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_DOOR_CONTINUITY_TRACKING(INT iDoor)
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_DoorsUsedAltConfigTracking)
	AND g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iContinuityId > -1
		IF GET_DOOR_CONFIGURATION_INDEX(iDoor) = ciDOOR_CONFIG__ALT
			#IF IS_DEBUG_BUILD
			IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iDoorsUseAltConfigBitset, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iContinuityId)
				PRINTLN("[JS][CONTINUITY][Doors][Door ", iDoor, "] PROCESS_DOOR_CONTINUITY_TRACKING | Door ", iDoor, " with continuity ID ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iContinuityId, " has used Alt config")
			ENDIF
			#ENDIF
			FMMC_SET_LONG_BIT(MC_serverBD_1.sMissionContinuityVars.iDoorsUseAltConfigBitset, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iContinuityId)
		ELSE
			#IF IS_DEBUG_BUILD
			IF FMMC_IS_LONG_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iDoorsUseAltConfigBitset, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iContinuityId)
				PRINTLN("[JS][CONTINUITY][Doors][Door ", iDoor, "] PROCESS_DOOR_CONTINUITY_TRACKING | Door ", iDoor, " with continuity ID ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iContinuityId, " has gone back to Standard config")
			ENDIF
			#ENDIF
			FMMC_CLEAR_LONG_BIT(MC_serverBD_1.sMissionContinuityVars.iDoorsUseAltConfigBitset, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iContinuityId)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DOOR_STAGGERED_SERVER(INT iDoor)
	PROCESS_DOOR_CONTINUITY_TRACKING(iDoor)
ENDPROC

PROC PROCESS_PREPARING_FOR_UPCOMING_DOOR_MODEL_SWAPS(INT iDoor)
	// Load upcoming model swaps to ensure that the transition is smooth
	INT iConfig
	FOR iConfig = 0 TO ciDOOR_CONFIG__MAX - 1
		MODEL_NAMES mnModelSwapModel = GET_DOOR_MODEL(iDoor, iConfig)
		IF IS_MODEL_VALID(mnModelSwapModel)
			REQUEST_MODEL(mnModelSwapModel)
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_DOOR_STAGGERED_CLIENT(INT iDoor)
	
	IF HAS_DOOR_BEEN_INITIALISED(iDoor)
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(GET_DOOR_HASH(iDoor))
			PRINTLN("[Doors][Door ", iDoor, "] PROCESS_DOOR_STAGGERED_CLIENT | Door isn't registered! Clearing data so that it can be re-initialised later. Hash: ", GET_DOOR_HASH(iDoor))
			CLEAR_BIT(iDoorHasBeenInitialisedBS, iDoor)
			EXIT
		ENDIF
		
		// We can't necessarily trust the natives to tell us the correct state of the lock, so we simply force reapply the lock/unlock here in the staggered loop as a safeguard for those times where code reports the wrong thing and we're left with a locked door that tells us it's unlocked, etc.
		PROCESS_DOOR_LOCK(iDoor, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[GET_DOOR_CONFIGURATION_INDEX_TO_USE(iDoor)], TRUE)
		
		PROCESS_PREPARING_FOR_UPCOMING_DOOR_MODEL_SWAPS(iDoor)
		
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Prize Vehicle  --------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Handles the Prize vehicle within the Casino.  --------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC FLOAT GET_PRIZE_VEHICLE_HEADING()
	RETURN 142.1 //243.5725
ENDFUNC

FUNC VECTOR GET_PRIZE_VEHICLE_POSITION()
	RETURN <<1100.0000, 220.0000, -49.45>>
ENDFUNC

FUNC MODEL_NAMES GET_PRIZE_VEHICLE_MODEL()
	RETURN THRAX
ENDFUNC

PROC MAINTAIN_PRIZE_VEHICLE_MISSION()

	IF NOT DOES_ENTITY_EXIST(objPrizedPodium)
		objPrizedPodium = GET_CLOSEST_OBJECT_OF_TYPE(<<1100.0000, 220.0000, -50.0000>>, 1.0, INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("vw_prop_vw_casino_podium_01a")), FALSE, DEFAULT, FALSE)
	ELSE
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_DisableCasinoPrizeVehicleWheel)
			fPrizedVehicleHeading = fPrizedVehicleHeading +@ 4.0
			
			IF fPrizedVehicleHeading >= 360.0
				fPrizedVehicleHeading -= 360.0
			ENDIF
			
			SET_ENTITY_HEADING(objPrizedPodium, fPrizedVehicleHeading)
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_CasinoDisablePrizeCar)
			IF NOT DOES_ENTITY_EXIST(vehPrizedVehicle)
			
				REQUEST_MODEL(GET_PRIZE_VEHICLE_MODEL())
				
				IF HAS_MODEL_LOADED(GET_PRIZE_VEHICLE_MODEL())
					vehPrizedVehicle = CREATE_VEHICLE(GET_PRIZE_VEHICLE_MODEL(), GET_PRIZE_VEHICLE_POSITION(), GET_PRIZE_VEHICLE_HEADING(), FALSE, FALSE, TRUE)
					
					SET_ENTITY_INVINCIBLE(vehPrizedVehicle, TRUE)
					SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(vehPrizedVehicle, FALSE)
					SET_VEHICLE_FULLBEAM(vehPrizedVehicle, FALSE)
					SET_VEHICLE_LIGHTS(vehPrizedVehicle, FORCE_VEHICLE_LIGHTS_OFF)
					SET_VEHICLE_DOORS_LOCKED(vehPrizedVehicle, VEHICLELOCK_LOCKED)
					SET_VEHICLE_FIXED(vehPrizedVehicle)
			        SET_ENTITY_HEALTH(vehPrizedVehicle, 1000)
			        SET_VEHICLE_ENGINE_HEALTH(vehPrizedVehicle, 1000)
			        SET_VEHICLE_PETROL_TANK_HEALTH(vehPrizedVehicle, 1000)
					SET_VEHICLE_DIRT_LEVEL(vehPrizedVehicle, 0.0)
		            SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehPrizedVehicle, TRUE)
					SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehPrizedVehicle, TRUE)
					SET_ENTITY_CAN_BE_DAMAGED(vehPrizedVehicle, FALSE)
					SET_VEHICLE_RADIO_ENABLED(vehPrizedVehicle, FALSE)
					SET_ENTITY_COLLISION(vehPrizedVehicle, FALSE)
					FREEZE_ENTITY_POSITION(vehPrizedVehicle, TRUE)
					
					VEHICLE_SETUP_STRUCT_MP sData
					
					sData.VehicleSetup.iColour1 = 150
					sData.VehicleSetup.iColour2 = 150
					
					sData.VehicleSetup.iColourExtra1 = 89
					sData.VehicleSetup.iColourExtra2 = 21
					
					sData.iColour5 = 1
					sData.iColour6 = 132
					
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 0
					
					SET_VEHICLE_SETUP_MP(vehPrizedVehicle, sData, FALSE, TRUE, TRUE)
					
					ATTACH_ENTITY_TO_ENTITY(vehPrizedVehicle, objPrizedPodium, -1, <<0.0, 0.0, 0.55>>, <<0.0, 0.0, 0.0>>)
					
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_PRIZE_VEHICLE_MODEL())
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(vehPrizedVehicle)
					IF NOT IS_ENTITY_ATTACHED(vehPrizedVehicle)
						ATTACH_ENTITY_TO_ENTITY(vehPrizedVehicle, objPrizedPodium, -1, <<0.0, 0.0, 0.55>>, <<0.0, 0.0, 0.0>>)
					ENDIF
					
					IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_PRIZED_VEHICLE_SHOULD_HIDE)
						IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_PRIZED_VEHICLE_HIDDEN)
							SET_ENTITY_VISIBLE(vehPrizedVehicle, FALSE)
							SET_ENTITY_ALPHA(vehPrizedVehicle, 0, FALSE)
							SET_BIT(iLocalBoolCheck31, LBOOL31_PRIZED_VEHICLE_HIDDEN)
						ENDIF
					ELSE
						IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_PRIZED_VEHICLE_HIDDEN)
							SET_ENTITY_VISIBLE(vehPrizedVehicle, TRUE)
							RESET_ENTITY_ALPHA(vehPrizedVehicle)
							CLEAR_BIT(iLocalBoolCheck31, LBOOL31_PRIZED_VEHICLE_HIDDEN)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Interior Utility Functions  -------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Contains utility functions used throughout the interior processing functions.  -----------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_SET_MINIMAP_TO_DISPLAY_CURRENT_INTERIOR()
	
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_PlayerBlipsData.bBigMapIsActive
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//FMMC2020 - This needs to be refactored for performance
PROC SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(INTERIOR_INSTANCE_INDEX idxInterior, BLIP_DISPLAY bDisplay, BOOL bCheckAnyInterior = FALSE, 
													BOOL bProcessLocates = FALSE, BOOL bProcessAIBlips = FALSE, BOOL bProcessGetAndDeliver = FALSE, BOOL bProcessDummyBlips = FALSE)
	
	ENTITY_INDEX entIndex
	INT i
	
	PRINTLN("SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR - Looping Through Peds")
	FOR i = 0 TO FMMC_MAX_PEDS-1
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[i])
			IF DOES_BLIP_EXIST(biPedBlip[i])
				entIndex = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niPed[i])
				
				IF DOES_ENTITY_EXIST(entIndex)
				AND (GET_INTERIOR_FROM_ENTITY(entIndex) = idxInterior 
				OR (bCheckAnyInterior AND IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(entIndex))))
					SET_BLIP_DISPLAY(biPedBlip[i], bDisplay)
				ENDIF
			ENDIF
		
			IF bProcessAIBlips
				IF bDisplay = DISPLAY_NOTHING
					CLEANUP_AI_PED_BLIP(biHostilePedBlip[i])
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR - Looping Through Vehicles with bDisplay: ", bDisplay)
	FOR i = 0 TO FMMC_MAX_VEHICLES-1
		IF DOES_BLIP_EXIST(biVehBlip[i])
		AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
			IF NOT bCheckAnyInterior
				entIndex = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[i])	
				IF DOES_ENTITY_EXIST(entIndex)
				AND (GET_INTERIOR_FROM_ENTITY(entIndex) = idxInterior 
				OR (bCheckAnyInterior AND IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(entIndex))))
					SET_BLIP_DISPLAY(biVehBlip[i], bDisplay)
				ENDIF
			ELSE
				SET_BLIP_DISPLAY(biVehBlip[i], bDisplay)
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR - Looping Through Objects")
	FOR i = 0 TO FMMC_MAX_NUM_OBJECTS-1
		IF DOES_BLIP_EXIST(biObjBlip[i])
		AND NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(i))
			IF NOT bCheckAnyInterior
				entIndex = NET_TO_ENT(GET_OBJECT_NET_ID(i))			
				IF DOES_ENTITY_EXIST(entIndex)
				AND (GET_INTERIOR_FROM_ENTITY(entIndex) = idxInterior 
				OR (bCheckAnyInterior AND IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(entIndex))))
					SET_BLIP_DISPLAY(biObjBlip[i], bDisplay)
				ENDIF
			ELSE
				SET_BLIP_DISPLAY(biObjBlip[i], bDisplay)
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR - Looping Through Interactables")
	FOR i = 0 TO FMMC_MAX_NUM_INTERACTABLES-1
		IF DOES_BLIP_EXIST(biInteractableBlip[i])
		AND NETWORK_DOES_NETWORK_ID_EXIST(GET_INTERACTABLE_NET_ID(i))
			IF NOT bCheckAnyInterior
				entIndex = NET_TO_ENT(GET_INTERACTABLE_NET_ID(i))			
				IF DOES_ENTITY_EXIST(entIndex)
				AND (GET_INTERIOR_FROM_ENTITY(entIndex) = idxInterior 
				OR (bCheckAnyInterior AND IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(entIndex))))
					SET_BLIP_DISPLAY(biInteractableBlip[i], bDisplay)
				ENDIF
			ELSE
				SET_BLIP_DISPLAY(biInteractableBlip[i], bDisplay)
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR - Looping Through Pickups")
	FOR i = 0 TO FMMC_MAX_WEAPONS-1
		IF DOES_BLIP_EXIST(biPickup[i])
		AND DOES_PICKUP_EXIST(pipickup[i])
			OBJECT_INDEX puObject = GET_PICKUP_OBJECT(pipickup[i])
			IF DOES_PICKUP_OBJECT_EXIST(pipickup[i])
			AND DOES_ENTITY_EXIST(puObject)
			AND (GET_INTERIOR_FROM_ENTITY(puObject) = idxInterior 
				OR (bCheckAnyInterior AND IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(puObject))))
				SET_BLIP_DISPLAY(biPickup[i], bDisplay)
			ENDIF
		ENDIF
	ENDFOR
	
	IF bProcessLocates
		PRINTLN("SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR - Looping Through Locates")
		FOR i = 0 TO FMMC_MAX_GO_TO_LOCATIONS-1
			IF DOES_BLIP_EXIST(LocBlip[i])
				SET_BLIP_DISPLAY(LocBlip[i], bDisplay)
			ENDIF
		ENDFOR
	ENDIF
	
	IF bProcessGetAndDeliver
		IF DOES_BLIP_EXIST(DeliveryBlip)
			SET_BLIP_DISPLAY(DeliveryBlip, bDisplay)
		ENDIF
	ENDIF
	
	IF bProcessDummyBlips
		FOR i = 0 TO FMMC_MAX_DUMMY_BLIPS-1
			IF DOES_BLIP_EXIST(DummyBlip[i])
				SET_BLIP_DISPLAY(DummyBlip[i], bDisplay)
			ENDIF
			
			IF bDisplay = DISPLAY_NOTHING
				SET_BIT(iLocalBoolCheck35, LBOOL35_HIDE_DUMMY_BLIPS_ON_MAP_WHILE_IN_INTERIOR)
			ELSE
				CLEAR_BIT(iLocalBoolCheck35, LBOOL35_HIDE_DUMMY_BLIPS_ON_MAP_WHILE_IN_INTERIOR)
			ENDIF
		ENDFOR
	ENDIF
	
	IF bDisplay = DISPLAY_NOTHING
		SET_COP_BLIP_SPRITE(RADAR_TRACE_INVALID, 0.0)
	ELSE
		SET_COP_BLIP_SPRITE_AS_STANDARD()
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Special Interior Behaviour  -------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Handles special behaviour for specific interiors such as correcting the minimap or playing sounds.  --------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_MISSION_MOC_STATE(INT iNewMissionMOCState)
	iMissionMOCState = iNewMissionMOCState
	PRINTLN("[MissionMOC] SET_MISSION_MOC_STATE | Setting state to ", iMissionMOCState)
ENDPROC

PROC DRAW_MISSION_MOC_RENDERTARGET(ARMORY_TRUCK_SCREEN_ID eScreenID, INT& iRenderTargetID)
	
	SET_TEXT_RENDER_ID(iRenderTargetID)
	SET_SCRIPT_GFX_ALIGN(UI_ALIGN_IGNORE, UI_ALIGN_IGNORE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	
	DRAW_SPRITE_NAMED_RENDERTARGET(GET_STREAMED_TEXTURE_DICTIONARY(eScreenID), ARMORY_TRUCK_GET_TEXTURE_NAME_FOR_MONITOR(eScreenID), 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
		
	RESET_SCRIPT_GFX_ALIGN()
	SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
	
ENDPROC

FUNC BOOL REGISTER_MISSION_MOC_RENDER_TARGET(ARMORY_TRUCK_SCREEN_ID eScreenID, STRING sRenderTargetName)
	
	UNUSED_PARAMETER(eScreenID)
	
	IF IS_NAMED_RENDERTARGET_REGISTERED(sRenderTargetName)
		RETURN TRUE
	ENDIF
	
	IF REGISTER_NAMED_RENDERTARGET(sRenderTargetName)
		PRINTLN("[MissionMOC] SET_MISSION_MOC_STATE | Registered ", sRenderTargetName)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL LINK_MISSION_MOC_RENDER_TARGET(INT& iRenderTargetID, ARMORY_TRUCK_SCREEN_ID eScreenID, STRING sRenderTargetName)
	MODEL_NAMES eRenderTarget
	GET_RENDER_TARGET_PROP(eScreenID, eRenderTarget)
	
	IF iRenderTargetID != -1
		PRINTLN("[MissionMOC] LINK_MISSION_MOC_RENDER_TARGET | eScreenID: ", eScreenID, " // iRenderTargetID: ", iRenderTargetID)
		RETURN TRUE
	ENDIF

	IF NOT IS_NAMED_RENDERTARGET_LINKED(eRenderTarget)
		LINK_NAMED_RENDERTARGET(eRenderTarget)
		
		#IF IS_DEBUG_BUILD
		PRINTLN("[MissionMOC] LINK_MISSION_MOC_RENDER_TARGET | Linking render target for ", FMMC_GET_MODEL_NAME_FOR_DEBUG(eRenderTarget))
		#ENDIF
	ELSE
		IF iRenderTargetID = -1
			iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID(sRenderTargetName)
			PRINTLN("[MissionMOC] LINK_MISSION_MOC_RENDER_TARGET | Setting passed-in iRenderTargetID to ", iRenderTargetID)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_MISSION_MOC_INTERIOR()
	
	VECTOR vInteriorPos = <<1103.562378, -3000.0, -40.0>>
	
	// Handle the map and TC modifier
	IF LocalPlayerCurrentInterior = GET_INTERIOR_AT_COORDS(vInteriorPos)
		
		// Player is inside the MOC!
		IF SHOULD_SET_MINIMAP_TO_DISPLAY_CURRENT_INTERIOR()
			SET_RADAR_AS_INTERIOR_THIS_FRAME(iLocalPlayerCurrentInteriorHash, vLocalPlayerCurrentInteriorPos.x, vLocalPlayerCurrentInteriorPos.y)
			HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
		ELSE
			HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_INITIALISED_MOC_INTERIOR_MAP)
			SET_INSIDE_VERY_SMALL_INTERIOR(TRUE)
			SET_BIT(iLocalBoolCheck31, LBOOL31_INITIALISED_MOC_INTERIOR_MAP)
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_INITIALISED_MOC_INTERIOR_MAP)
			SET_INSIDE_VERY_SMALL_INTERIOR(FALSE)
			CLEAR_BIT(iLocalBoolCheck31, LBOOL31_INITIALISED_MOC_INTERIOR_MAP)
		ENDIF
		
		EXIT
		
	ENDIF
	
	IF IS_VALID_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorPos))
		BOOL bShowInterior = TRUE
		IF IS_PAUSE_MENU_ACTIVE()
			IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
				bShowInterior = FALSE
			ENDIF
		ELIF g_PlayerBlipsData.bBigMapIsActive
			bShowInterior = FALSE
		ENDIF
		BOOL bStaggerChange = (GET_FRAME_COUNT() % 5) = 0
		IF bShowInterior
			IF bStaggerChange
				SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorPos), DISPLAY_BLIP)
				SET_HIDE_ALL_PLAYER_BLIPS(FALSE)
			ENDIF
		ELSE
			HIDE_PLAYER_ARROW_THIS_FRAME()
			HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
			IF bStaggerChange
				SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorPos), DISPLAY_NOTHING)
				SET_HIDE_ALL_PLAYER_BLIPS(TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	// Set up and process render targets for the monitors
	SWITCH iMissionMOCState
		CASE ciMISSION_MOC_STATE__INIT
			SET_MISSION_MOC_STATE(ciMISSION_MOC_STATE__REGISTER_RENDER_TARGETS)
		BREAK
		
		CASE ciMISSION_MOC_STATE__REGISTER_RENDER_TARGETS
			IF REGISTER_MISSION_MOC_RENDER_TARGET(AT_COMMAND_CENTER_TV_01, GET_RENDER_TARGET(AT_COMMAND_CENTER_TV_01))
			AND REGISTER_MISSION_MOC_RENDER_TARGET(AT_TRAILER_MONITOR_01, GET_RENDER_TARGET(AT_TRAILER_MONITOR_01))
			AND REGISTER_MISSION_MOC_RENDER_TARGET(AT_TRAILER_MONITOR_02, GET_RENDER_TARGET(AT_TRAILER_MONITOR_02))
			AND REGISTER_MISSION_MOC_RENDER_TARGET(AT_TRAILER_MONITOR_03, GET_RENDER_TARGET(AT_TRAILER_MONITOR_03))
				SET_MISSION_MOC_STATE(ciMISSION_MOC_STATE__LINK_RENDER_TARGETS)
			ENDIF
		BREAK
		
		CASE ciMISSION_MOC_STATE__LINK_RENDER_TARGETS
			IF LINK_MISSION_MOC_RENDER_TARGET(iMissionMOCRenderTargetID_TV, AT_COMMAND_CENTER_TV_01, GET_RENDER_TARGET(AT_COMMAND_CENTER_TV_01))
			AND LINK_MISSION_MOC_RENDER_TARGET(iMissionMOCRenderTargetID_Monitor1, AT_TRAILER_MONITOR_01, GET_RENDER_TARGET(AT_TRAILER_MONITOR_01))
			AND LINK_MISSION_MOC_RENDER_TARGET(iMissionMOCRenderTargetID_Monitor2, AT_TRAILER_MONITOR_02, GET_RENDER_TARGET(AT_TRAILER_MONITOR_02))
			AND LINK_MISSION_MOC_RENDER_TARGET(iMissionMOCRenderTargetID_Monitor3, AT_TRAILER_MONITOR_03, GET_RENDER_TARGET(AT_TRAILER_MONITOR_03))
				SET_MISSION_MOC_STATE(ciMISSION_MOC_STATE__PROCESS)
			ENDIF
		BREAK
		
		CASE ciMISSION_MOC_STATE__PROCESS
			DRAW_MISSION_MOC_RENDERTARGET(AT_COMMAND_CENTER_TV_01, iMissionMOCRenderTargetID_TV)
			DRAW_MISSION_MOC_RENDERTARGET(AT_TRAILER_MONITOR_01, iMissionMOCRenderTargetID_Monitor1)
			DRAW_MISSION_MOC_RENDERTARGET(AT_TRAILER_MONITOR_02, iMissionMOCRenderTargetID_Monitor2)
			DRAW_MISSION_MOC_RENDERTARGET(AT_TRAILER_MONITOR_03, iMissionMOCRenderTargetID_Monitor3)
		BREAK
		
	ENDSWITCH
	
ENDPROC

FUNC INTERIOR_INSTANCE_INDEX GET_SILO_INTERIOR_PLAYER_IS_CURRENTLY_IN()
	
	IF LocalPlayerCurrentInterior = GET_INTERIOR_AT_COORDS(<<244.252, 6163.91, -161.423>>) // Silo Lab  -635400705
		RETURN GET_INTERIOR_AT_COORDS(<<244.252, 6163.91, -161.423>>)
	ELIF LocalPlayerCurrentInterior = GET_INTERIOR_AT_COORDS(<<446.172, 5922.13, -157.216>>) // Silo -222705970
		RETURN GET_INTERIOR_AT_COORDS(<<446.172, 5922.13, -157.216>>)
	ELIF LocalPlayerCurrentInterior = GET_INTERIOR_AT_COORDS(<<252.062, 5972.12, -159.102>>) // Silo 1704012289
		RETURN GET_INTERIOR_AT_COORDS(<<252.062, 5972.12, -159.102>>)
	ELIF LocalPlayerCurrentInterior = GET_INTERIOR_AT_COORDS(<<550.948, 5939.26, -157.216>>)  // Silo 1914093948
		RETURN GET_INTERIOR_AT_COORDS(<<550.948, 5939.26, -157.216>>)
	ELIF LocalPlayerCurrentInterior = GET_INTERIOR_AT_COORDS(<<361.0, 6306.0, -159.0>>) // Silo 01
		RETURN GET_INTERIOR_AT_COORDS(<<361.0, 6306.0, -159.0>>)
	ENDIF
	
	INTERIOR_INSTANCE_INDEX iiInteriorBlank
	RETURN iiInteriorBlank
	
ENDFUNC

/// PURPOSE: When playing in the Silo interior, this function will change the map to
///    the map of the correct floor based on the player's Z position
PROC PROCESS_SILO_MINIMAP(VECTOR vPos,INT iInteriorHash) 
	
	VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
	FLOAT fPlayerZ = vPlayerPos.z
	
	INT iFloor = -2
	
	IF fPlayerZ <= -156
		iFloor = 0
	ELIF fPlayerZ <= -152
		iFloor = 1
	ELIF fPlayerZ <= -148
		iFloor = 2
	ELSE
		iFloor = 3
	ENDIF
	
	PRINTLN("[SiloMinimap] Processing Silo Minimap, player Z is ", fPlayerZ, " hash is ", iInteriorHash, " pos is ", vPos, " floor is ", iFloor)
	
	BOOL bShowInterior = TRUE
	INTERIOR_INSTANCE_INDEX iiInteriorIndex = GET_SILO_INTERIOR_PLAYER_IS_CURRENTLY_IN()
	
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	
	IF bShowInterior
		SET_RADAR_AS_INTERIOR_THIS_FRAME(iInteriorHash, vPos.x, vPos.y, 0, iFloor)
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
		IF IS_VALID_INTERIOR(iiInteriorIndex)
			SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(iiInteriorIndex, DISPLAY_BLIP, TRUE, DEFAULT, DEFAULT, DEFAULT, TRUE)
		ENDIF
		HIDE_ALL_PLAYER_BLIPS(FALSE)
	ELSE
		HIDE_PLAYER_ARROW_THIS_FRAME()
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
		IF IS_VALID_INTERIOR(iiInteriorIndex)
			SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(iiInteriorIndex, DISPLAY_NOTHING, TRUE, DEFAULT, DEFAULT, DEFAULT, TRUE)
		ENDIF
		SET_HIDE_ALL_PLAYER_BLIPS(TRUE)
	ENDIF
ENDPROC

PROC PROCESS_FOUNDRY_MINIMAP()

	SET_RADAR_AS_EXTERIOR_THIS_FRAME()
	SET_RADAR_ZOOM_PRECISE(50)
	
	PRINTLN("Processing foundry minimap")
ENDPROC

/// PURPOSE: When playing IAA - Morgue, this function will change the map to
///    the map of the correct floor based on the player's Z position
PROC PROCESS_MORGUE_MINIMAP() 
	
	VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
	FLOAT fPlayerZ = vPlayerPos.z
	
	INT iFloor = -2

	INT iInteriorHash = -1409433418
	
	PRINTLN("Processing Morgue Minimap, player Z is ", fPlayerZ, " hash is ", iInteriorHash)
	
	IF fPlayerZ <= 26
		iFloor = -2
		PRINTLN("Processing Morgue Minimap - Floor -2")
	ELIF fPlayerZ <= 30
		iFloor = -1
		PRINTLN("Processing Morgue Minimap - Floor -1")
	ELIF fPlayerZ <= 32
		iFloor = 0
		PRINTLN("Processing Morgue Minimap - Floor 0")
	ELIF fPlayerZ > 38
		iFloor = 1
		PRINTLN("Processing Morgue Minimap - Floor 1")
	ENDIF
	
	SET_RADAR_AS_INTERIOR_THIS_FRAME(iInteriorHash, 249.719, -1368.729, -40, iFloor)
ENDPROC

PROC PROCESS_IMPORT_WAREHOUSE()

	PRINTLN("PROCESS_IMPORT_WAREHOUSE - calling function...")
	
	INT iInteriorHash = HASH("imp_impexp_intwaremed")
	INT iFloor = 0
	VECTOR vInteriorPos = <<974.9203, -3000.0647, -40.6470>>
	FLOAT fRotation = -0
	
	BOOL bShowInterior = TRUE
	BOOL bStaggerChange = (GET_FRAME_COUNT() % 5) = 0
	
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	
	IF bShowInterior
		SET_RADAR_AS_INTERIOR_THIS_FRAME(iInteriorHash, vInteriorPos.x, vInteriorPos.y, ROUND(fRotation), iFloor)
		SET_RADAR_ZOOM(100)
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorPos), DISPLAY_BLIP)
			SET_HIDE_ALL_PLAYER_BLIPS(FALSE)
		ENDIF
	ELSE
		HIDE_PLAYER_ARROW_THIS_FRAME()
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorPos), DISPLAY_NOTHING)
			SET_HIDE_ALL_PLAYER_BLIPS(TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_WAREHOUSE_UNDRGRND_FACILITY()

	PRINTLN("PROCESS_WAREHOUSE_UNDRGRND_FACILITY - calling function...")
	
	INT iInteriorHash = HASH("imp_impexp_int_02")
	INT iFloor = 0
	VECTOR vInteriorPos = <<969.5376, -3000.4111, -48.6470>>
	FLOAT fRotation = 90
	
	BOOL bShowInterior = TRUE
	BOOL bStaggerChange = (GET_FRAME_COUNT() % 5) = 0
	
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	
	IF bShowInterior
		SET_RADAR_AS_INTERIOR_THIS_FRAME(iInteriorHash, vInteriorPos.x, vInteriorPos.y, ROUND(fRotation), iFloor)
		SET_RADAR_ZOOM(100)
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorPos), DISPLAY_BLIP)
			HIDE_ALL_PLAYER_BLIPS(FALSE)
		ENDIF
	ELSE
		HIDE_PLAYER_ARROW_THIS_FRAME()
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorPos), DISPLAY_NOTHING)
			HIDE_ALL_PLAYER_BLIPS(TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_INTERIOR_MINIMAP(INT iInteriorHash, VECTOR vInteriorPos, FLOAT fXPosOffet = 0.0, FLOAT fYPosOffset = 0.0, BOOL bSmallInterior = FALSE, BOOL bSkipSetRadarInterior = FALSE, BOOL bHideEntities = FALSE, BOOL bRunInteriorNatives = FALSE)

	IF GET_INTERIOR_FROM_ENTITY(PlayerPedToUse) != GET_INTERIOR_AT_COORDS(vInteriorPos)
		EXIT
	ENDIF
	
	INT iFloor = 0
	FLOAT fRotation = -0
	
	BOOL bShowInterior = TRUE
	BOOL bStaggerChange = (GET_FRAME_COUNT() % 5) = 0
	
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
			PRINTLN("PROCESS_INTERIOR_MINIMAP = bShowInterior = FALSE PAUSE")
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
		PRINTLN("PROCESS_INTERIOR_MINIMAP = bShowInterior = FALSE BIG MAP")
	ENDIF
	FLOAT fInteriorX = vInteriorPos.x + fXPosOffet
	FLOAT fInteriorY = vInteriorPos.y + fYPosOffset
	
	#IF IS_DEBUG_BUILD
	IF fMinimapXOffset != 0.0
		fInteriorX += fMinimapXOffset
	ENDIF
	IF fMinimapYOffset != 0.0
		fInteriorY += fMinimapYOffset
	ENDIF
	#ENDIF
	IF bShowInterior
		IF !bSkipSetRadarInterior
			SET_RADAR_AS_INTERIOR_THIS_FRAME(iInteriorHash, fInteriorX, fInteriorY, ROUND(fRotation), iFloor)
		ENDIF
		
		IF !bSkipSetRadarInterior
		OR bRunInteriorNatives
			HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
			
			IF bSmallInterior
				SET_INSIDE_VERY_SMALL_INTERIOR(TRUE)
			ELSE
				SET_RADAR_ZOOM(60)
			ENDIF
		ENDIF
			
		IF bStaggerChange
			SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorPos), DISPLAY_BLIP, DEFAULT, bHideEntities, bHideEntities, bHideEntities)
			SET_HIDE_ALL_PLAYER_BLIPS(FALSE)
		ENDIF
	ELSE
		HIDE_PLAYER_ARROW_THIS_FRAME()
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorPos), DISPLAY_NOTHING, DEFAULT, bHideEntities, bHideEntities, bHideEntities)
			SET_HIDE_ALL_PLAYER_BLIPS(TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_INTERIOR_DISPLACEMENT(INT iInteriorHash, VECTOR vInteriorPos, VECTOR vDisplacePos)
	
	IF iCommonDisplacementInterior != -1
	AND iCommonDisplacementInterior != iInteriorHash
		EXIT
	ENDIF
	
	IF GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) != NULL
	AND GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) = GET_INTERIOR_AT_COORDS(vInteriorPos)
		IF NOT bIsDisplacedInteriorSet
			bIsDisplacedInteriorSet = TRUE
			iCommonDisplacementInterior = iInteriorHash
			SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(NATIVE_TO_INT(GET_INTERIOR_AT_COORDS(vInteriorPos)), vDisplacePos)
			PRINTLN("PROCESS_INTERIOR_DISPLACEMENT - Setting displacement for interior ", iCommonDisplacementInterior)
		ENDIF
	ELSE
		IF bIsDisplacedInteriorSet
			bIsDisplacedInteriorSet = FALSE
			iCommonDisplacementInterior = -1
			CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA()
			PRINTLN("PROCESS_INTERIOR_DISPLACEMENT - Clearing displacement for interior ", iCommonDisplacementInterior)
		ENDIF		
	ENDIF
ENDPROC

PROC PROCESS_SUBMARINE_MINIMAP()
	VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
	FLOAT fPlayerZ = vPlayerPos.z

	INT iInteriorHash = 1912197552
	INT iFloor = 2
	
	IF fPlayerZ >= -63
		iFloor = 2
		PRINTLN("PROCESS_SUBMARINE_MINIMAP Processing Submarine Minimap - Floor 2")
	ELIF fPlayerZ >= -66.5
		iFloor = 1
		PRINTLN("PROCESS_SUBMARINE_MINIMAP Processing Submarine Minimap - Floor 1")
	ELIF fPlayerZ >= -69.5
		iFloor = 0
		PRINTLN("PROCESS_SUBMARINE_MINIMAP Processing Submarine Minimap - Floor 0")
	ENDIF
	
	BOOL bShowInterior = TRUE
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	IF bShowInterior
		SET_RADAR_AS_INTERIOR_THIS_FRAME(iInteriorHash, 512.769, 4851.98, -0, iFloor)
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
	ELSE
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
	ENDIF
ENDPROC

PROC PROCESS_SERVER_FARM_MINIMAP()

	VECTOR vInteriorPos = << 2168.09, 2920.89, -85.8005 >>
	VECTOR vExteriorPos = << 2483.0, -404.2, -85.8005 >>
	INT interiorNameHash = 1979383629  //HASH("xm_x17dlc_int_facility2")
	 
	BOOL bShowInterior = TRUE
	BOOL bStaggerChange = (GET_FRAME_COUNT() % 5) = 0
	
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	
	IF bShowInterior
		SET_RADAR_AS_INTERIOR_THIS_FRAME(interiorNameHash,vInteriorPos.x,vInteriorPos.y, 0, 0)	
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
		//PRINTLN("[Minimap] Processing Server Farm Minimap / ", interiorNameHash, " / ", vInteriorPos)
		IF bStaggerChange
			SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorPos), DISPLAY_BLIP)
		ENDIF
	ELSE
		SET_FAKE_PAUSEMAP_PLAYER_POSITION_THIS_FRAME(vExteriorPos.x,vExteriorPos.y)
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorPos), DISPLAY_NOTHING)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HANGAR_MINIMAP()

	INT iInteriorHash = 638945734
	INT iFloor = 0
	VECTOR vInteriorPos = << -1266.8, -3014.84, -50 >>
	FLOAT fRotation = -0
	
	BOOL bShowInterior = TRUE
	BOOL bStaggerChange = (GET_FRAME_COUNT() % 5) = 0
	
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	
	IF bShowInterior
		SET_RADAR_AS_INTERIOR_THIS_FRAME(iInteriorHash, vInteriorPos.x, vInteriorPos.y, ROUND(fRotation), iFloor)
		SET_RADAR_ZOOM(100)
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorPos), DISPLAY_BLIP)
		ENDIF
	ELSE
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorPos), DISPLAY_NOTHING)
		ENDIF
	ENDIF
	
	PRINTLN("[Minimap] Processing Hangar Minimap / ", iInteriorHash, " / ", vInteriorPos, " / ", fRotation, "  (Hiding exterior map + setting radar zoom to 100)")
ENDPROC

PROC PROCESS_DISPLACEMENT_ARENA_INTERIOR_MC()
	VECTOR vInteriorPos = <<2800.0, -3800.0, 100.0>>
	INT iDynoprop
	BOOL bShowInterior = TRUE
	BOOL bStaggerChange = (GET_FRAME_COUNT() % 5) = 0
	
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	
	IF bShowInterior
		SET_RADAR_AS_INTERIOR_THIS_FRAME(HASH("xs_x18_int_01"),vInteriorPos.x,vInteriorPos.y, 0, GET_ARENA_ENTITY_SET_LEVEL_NUMBER(g_FMMC_STRUCT.sArenaInfo.iArena_Theme, g_FMMC_STRUCT.sArenaInfo.iArena_Variation))	
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorPos), DISPLAY_BLIP)
			
			FOR iDynoprop = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps - 1
				IF NOT DOES_BLIP_EXIST(biTrapCamBlip[iDynoprop])
					RELOOP
				ENDIF
				SET_BLIP_DISPLAY(biTrapCamBlip[iDynoprop], DISPLAY_BLIP)
			ENDFOR
			
			IF DOES_BLIP_EXIST(biBlipTrapCam)
				SET_BLIP_DISPLAY(biBlipTrapCam, DISPLAY_BLIP)
			ENDIF
			
			IF DOES_BLIP_EXIST(DeliveryBlip)
				SET_BLIP_DISPLAY(DeliveryBlip, DISPLAY_BLIP)
			ENDIF
			
		ENDIF
	ELSE
		SET_FAKE_PAUSEMAP_PLAYER_POSITION_THIS_FRAME(ARENA_X, ARENA_Y)
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorPos), DISPLAY_NOTHING)
			
			FOR iDynoprop = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps - 1
				IF NOT DOES_BLIP_EXIST(biTrapCamBlip[iDynoprop])
					RELOOP
				ENDIF
				SET_BLIP_DISPLAY(biTrapCamBlip[iDynoprop], DISPLAY_NOTHING)
			ENDFOR
			
			IF DOES_BLIP_EXIST(biBlipTrapCam)
				SET_BLIP_DISPLAY(biBlipTrapCam, DISPLAY_NOTHING)
			ENDIF
			
			IF DOES_BLIP_EXIST(DeliveryBlip)
				SET_BLIP_DISPLAY(DeliveryBlip, DISPLAY_NOTHING)
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: When playing a mission using the Carrier IPL, this function will show the map and vary the zoom and floor of the map depending
///    on the player's location and exterior/interior-state when the player is in and around the Aircraft Carrier
PROC PROCESS_IPL_CARRIER_MAP(VECTOR vPlayer)
	
	VECTOR vCarrierCentre = <<3014.45, -4661.85, 18.85>>
	
	FLOAT fDistance2 = VDIST2(<<vPlayer.x, vPlayer.y, 0>>, <<vCarrierCentre.x, vCarrierCentre.y, 0>>)
	
	IF NOT IS_BIT_SET(iLocalBoolCheck5,LBOOL5_SHOW_CARRIER_IPL_MAP)
		
		IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_BIG_MAP_WAS_ZOOMED_IN_FOR_CARRIER)
			SET_RADAR_ZOOM_PRECISE(0)
			CLEAR_BIT(iLocalBoolCheck11, LBOOL11_BIG_MAP_WAS_ZOOMED_IN_FOR_CARRIER)
		ENDIF
		
		//Distance check to initialise:
		IF fDistance2 < (1500 * 1500)
			PRINTLN("[RCC MISSION] PROCESS_IPL_CARRIER_MAP, IPL_CARRIER - Within 1500m of Carrier, initialising map")
			SET_BIT(iLocalBoolCheck5,LBOOL5_SHOW_CARRIER_IPL_MAP)
			SET_BIT(iLocalBoolCheck11, LBOOL11_BIG_MAP_DO_ZOOM_IN_FOR_CARRIER)
		ENDIF
	ELSE
		INT iFloor = -2
		BOOL bInternal = FALSE
		
		IF IS_PED_IN_ANY_PLANE(LocalPlayerPed)
			VEHICLE_INDEX pedPlane = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			IF DOES_ENTITY_EXIST(pedPlane)
				IF IS_ENTITY_IN_AIR(pedPlane)
				AND NOT IS_VEHICLE_ON_ALL_WHEELS(pedPlane)
					IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_BIG_MAP_DO_ZOOM_IN_FOR_CARRIER)
						CLEAR_BIT(iLocalBoolCheck11, LBOOL11_BIG_MAP_DO_ZOOM_IN_FOR_CARRIER)
					ENDIF
					iZoomDelayGameTime = GET_GAME_TIMER()
				ELSE
					IF iZoomDelayGameTime > 0
						IF GET_GAME_TIMER() - iZoomDelayGameTime >= 2000
							SET_BIT(iLocalBoolCheck11, LBOOL11_BIG_MAP_DO_ZOOM_IN_FOR_CARRIER)
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ELSE
			SET_BIT(iLocalBoolCheck11, LBOOL11_BIG_MAP_DO_ZOOM_IN_FOR_CARRIER)
		ENDIF
		
		IF (fDistance2 <= (300 * 300))
			
			IF (GET_ROOM_KEY_FROM_ENTITY(PlayerPedToUse) != 0)
				
				bInternal = TRUE
				
				IF vPlayer.z <= 3.0
					iFloor = -1
				ELIF vPlayer.z <= 7.3
					iFloor = 0
				ELIF vPlayer.z <= 12.6
					iFloor = 1
				ELIF vPlayer.z <= 15.724
					iFloor = 2
				ELIF vPlayer.z <= 18.756
					iFloor = 3
				ELIF vPlayer.z <= 21.73
					iFloor = 4
				ELIF vPlayer.z <= 24.633
					iFloor = 5
				ELIF vPlayer.z <= 27.64
					iFloor = 6
				ELIF vPlayer.z <= 32.756
					iFloor = 7
				ELIF vPlayer.z <= 52.938
					iFloor = 8
				ELSE
					iFloor = 2
				ENDIF
				
				IF IS_AUDIO_SCENE_ACTIVE("DLC_HEISTS_BIOLAB_STEAL_EMP_FLYOVER_SCENE")
					PRINTLN("[MJM MISSION] STOP_AUDIO_SCENE(DLC_HEISTS_BIOLAB_STEAL_EMP_FLYOVER_SCENE")
					STOP_AUDIO_SCENE("DLC_HEISTS_BIOLAB_STEAL_EMP_FLYOVER_SCENE")
				ENDIF
			ELSE					
				IF vPlayer.z <= 3.0
					IF IS_POINT_IN_ANGLED_AREA(vPlayer, <<3103.206543, -4805.419434, 5>>, <<3080.537109, -4811.319336, -2>>, 29.5) //Back entrance, dock
					OR IS_POINT_IN_ANGLED_AREA(vPlayer, <<3098.5, -4791.1, 6.6>>, <<3100.8, -4800.0, 0.0>>, 5.5) //Back entrance, stairs
						bInternal = TRUE
						iFloor = -1
					ELIF IS_POINT_IN_ANGLED_AREA(vPlayer, <<3109, -4825, 5>>,<<3082, -4832, -10>>, 130, TRUE) //Back entrance
						iFloor = -1
					ELIF IS_POINT_IN_ANGLED_AREA(vPlayer, <<3098.3, -4748.5, 2.55>>, <<3105.6, -4775.6, 10.3>>, 9.25) //Hole opposite rear platform
						bInternal = TRUE
						iFloor = 0
					ELSE
						iFloor = 2
					ENDIF
				ELIF vPlayer.z <= 11
					IF IS_POINT_IN_ANGLED_AREA(vPlayer, <<3072.1, -4825.9, 3.0>>, <<3107.2, -4810.5, 9.9>>, 28.7) //Back entrance, upper floor
					OR IS_POINT_IN_ANGLED_AREA(vPlayer, <<3082.2, -4659.9, 3.5>>, <<3075.1, -4632.6, 10.3>>, 22.25) //Front side platform
					OR IS_POINT_IN_ANGLED_AREA(vPlayer, <<3063.7, -4809.4, 3.5>>, <<3056.3, -4781.9, 10.3>>, 22.25) //Rear side platform
					OR IS_POINT_IN_ANGLED_AREA(vPlayer, <<3098.3, -4748.5, 2.55>>, <<3105.6, -4775.6, 10.3>>, 9.25) //Hole opposite rear platform
						bInternal = TRUE
						iFloor = 0
					ELIF ( IS_POINT_IN_ANGLED_AREA(vPlayer, <<3103.206543, -4805.419434, 5>>, <<3080.537109, -4811.319336, -2>>, 29.5) //Back entrance, dock
						   OR IS_POINT_IN_ANGLED_AREA(vPlayer, <<3098.5, -4791.1, 6.6>>, <<3100.8, -4800.0, 0.0>>, 5.5) ) //Back entrance, stairs
						bInternal = TRUE
						iFloor = -1
					ELSE
						iFloor = 2
					ENDIF
				ELSE
					iFloor = 2
				ENDIF
			ENDIF
			
			IF g_PlayerBlipsData.bBigMapIsActive
				SET_BIT(iLocalBoolCheck11, LBOOL11_BIG_MAP_WAS_ZOOMED_IN_FOR_CARRIER)
				IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_BIG_MAP_DO_ZOOM_IN_FOR_CARRIER)
					IF bInternal
						SET_RADAR_ZOOM_PRECISE(55.0)
					ELSE
						SET_RADAR_ZOOM_PRECISE(92.0)
					ENDIF
				ELSE
					SET_RADAR_ZOOM_PRECISE(0.0)
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_BIG_MAP_WAS_ZOOMED_IN_FOR_CARRIER)
					SET_RADAR_ZOOM_PRECISE(0)
					CLEAR_BIT(iLocalBoolCheck11, LBOOL11_BIG_MAP_WAS_ZOOMED_IN_FOR_CARRIER)
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_CARRIER_LIGHTS_PRELOADED)
				PRESET_INTERIOR_AMBIENT_CACHE("int_carrier_hanger")
				SET_BIT(iLocalBoolCheck11, LBOOL11_CARRIER_LIGHTS_PRELOADED)
			ENDIF
			
		ELSE					
			IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_BIG_MAP_WAS_ZOOMED_IN_FOR_CARRIER)
				SET_RADAR_ZOOM_PRECISE(0.0)
				CLEAR_BIT(iLocalBoolCheck11, LBOOL11_BIG_MAP_WAS_ZOOMED_IN_FOR_CARRIER)
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_CARRIER_LIGHTS_PRELOADED)
				CLEAR_BIT(iLocalBoolCheck11, LBOOL11_CARRIER_LIGHTS_PRELOADED)
			ENDIF
			
			IF fDistance2 <= (2000 * 2000)
				iFloor = 2
			ELSE
				//Out of range, we could get in other interiors now - so clear this stuff from the map
				PRINTLN("[RCC MISSION] PROCESS_IPL_CARRIER_MAP, IPL_CARRIER - Over 2000m from Carrier, clearing map")
				iFloor = -2
				CLEAR_BIT(iLocalBoolCheck5,LBOOL5_SHOW_CARRIER_IPL_MAP)
			ENDIF
		ENDIF
		
		IF iFloor >= -1
			SET_RADAR_AS_INTERIOR_THIS_FRAME(1596791599,3050.0,-4650.0,DEFAULT,iFloor)
			IF NOT bInternal
				SET_RADAR_AS_EXTERIOR_THIS_FRAME()
			ENDIF
		ENDIF				
	ENDIF
ENDPROC

PROC PROCESS_IPL_YACHT_NEAR_PIER_MAP(VECTOR vPlayer)
	
	VECTOR vYachtCentre = <<-2066.023, -1024.236, 7.95>>
	
	FLOAT fDistance2 = VDIST2(<<vPlayer.x, vPlayer.y, 0>>, <<vYachtCentre.x, vYachtCentre.y, 0>>)

	IF fDistance2 < (70 * 70)
		SET_RADAR_ZOOM_PRECISE(40)
	ELIF fDistance2 > (80 * 80)
		SET_RADAR_ZOOM_PRECISE(0)
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck6,LBOOL6_SHOW_YACHT2_IPL_MAP)
		//Distance check to initialise:
		IF fDistance2 < (300 * 300)
			PRINTLN("[RCC MISSION] PROCESS_IPL_YACHT_NEAR_PIER_MAP, IPL_YACHT2 - Within 300m of Yacht, initialising map")
			SET_BIT(iLocalBoolCheck6,LBOOL6_SHOW_YACHT2_IPL_MAP)
		ENDIF
	ELSE
		INT iFloor = -2
		BOOL bInternal
		
		IF (fDistance2 <= (75 * 75))
			IF IS_POINT_IN_ANGLED_AREA(vPlayer, <<-2130.3, -1003.2, 19.7>>, <<-2016.7, -1040.4, -0.5>>, 17.0)
				
				bInternal = TRUE
				
				IF vPlayer.z <= 4.00
					iFloor = 0
				ELIF vPlayer.z <= 6.7
					iFloor = 1
				ELIF vPlayer.z <= 9.5
					iFloor = 2
				ELIF vPlayer.z <= 13.1
					iFloor = 3
				ELIF vPlayer.z <= 26.3
					iFloor = 4
				ELSE
					iFloor = 2
				ENDIF
			ELSE
				iFloor = 2
			ENDIF
		ELSE
			IF fDistance2 <= (320 * 320)
				iFloor = 2
			ELSE
				//Out of range, we could get in other interiors now - so clear this stuff from the map
				PRINTLN("[RCC MISSION] PROCESS_IPL_YACHT_NEAR_PIER_MAP, IPL_YACHT2 - Over 320m from Yacht, clearing map")
				iFloor = -2
				CLEAR_BIT( iLocalBoolCheck6, LBOOL6_SHOW_YACHT2_IPL_MAP )
			ENDIF
		ENDIF
		
		IF iFloor >= 0
			SET_RADAR_AS_INTERIOR_THIS_FRAME(1906615853,-2066.023, -1024.236,DEFAULT,iFloor)
			IF NOT bInternal
				SET_RADAR_AS_EXTERIOR_THIS_FRAME()
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROCESS_CASINO_BLIPS()
	
	IF !bLocalPlayerPedOK
		EXIT
	ENDIF
	
	INTERIOR_INSTANCE_INDEX iInteriorIndexCasinoMain, iInteriorIndexCasinoCarpark, iInteriorIndexCasinoApartment, iInteriorIndexCasinoGarage
	INTERIOR_DATA_STRUCT structInteriorData	
	BOOL bInsideCasino = TRUE
	VECTOR vExteriorPos = <<955.17, 39.40, 0.0>>
	
	structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO)
	iInteriorIndexCasinoMain = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
	structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_CAR_PARK)
	iInteriorIndexCasinoCarpark = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
	structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_APARTMENT)
	iInteriorIndexCasinoApartment = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
	structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_GARAGE)
	iInteriorIndexCasinoGarage = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
	
	LocalPlayerCurrentInterior = LocalPlayerCurrentInterior
	
	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD4)
		TEXT_LABEL_63 tlDebugText
		tlDebugText = "P: "
		tlDebugText += NATIVE_TO_INT(LocalPlayerCurrentInterior)
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.5, 0.25, 0.5>>)
		
		tlDebugText = "iInteriorIndexCasinoMain: "
		tlDebugText += NATIVE_TO_INT(iInteriorIndexCasinoMain)
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.5, 0.4, 0.5>>)
		
		tlDebugText = "iInteriorIndexCasinoCarpark: "
		tlDebugText += NATIVE_TO_INT(iInteriorIndexCasinoCarpark)
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.5, 0.45, 0.5>>)
		
		tlDebugText = "iInteriorIndexCasinoApartment: "
		tlDebugText += NATIVE_TO_INT(iInteriorIndexCasinoApartment)
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.5, 0.5, 0.5>>)
		
		tlDebugText = "iInteriorIndexCasinoGarage: "
		tlDebugText += NATIVE_TO_INT(iInteriorIndexCasinoGarage)
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.5, 0.55, 0.5>>)
	ENDIF
	#ENDIF
	
	IF (LocalPlayerCurrentInterior != iInteriorIndexCasinoMain
	AND LocalPlayerCurrentInterior != iInteriorIndexCasinoCarpark
	AND LocalPlayerCurrentInterior != iInteriorIndexCasinoApartment
	AND LocalPlayerCurrentInterior != iInteriorIndexCasinoGarage)
	OR LocalPlayerCurrentInterior = NULL
		PRINTLN("[PROCESS_CASINO_BLIPS] - Not inside a casino interior.")
		bInsideCasino = FALSE
		IF IS_BIT_SET(iLocalboolCheck29, LBOOL29_CASINO_BLIP_DISPLACEMENT_DONE)
			CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA()
			CLEAR_BIT(iLocalboolCheck29, LBOOL29_CASINO_BLIP_DISPLACEMENT_DONE)
		ENDIF
	ELSE
		PRINTLN("[PROCESS_CASINO_BLIPS] - inside a casino interior.")
	ENDIF
		
	BOOL bShowInterior = bInsideCasino
	
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalboolCheck29, LBOOL29_CASINO_BLIP_DISPLACEMENT_DONE)
		IF LocalPlayerCurrentInterior = iInteriorIndexCasinoCarpark
			SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(HASH("vw_dlc_casino_carpark"), vExteriorPos)
			SET_BIT(iLocalboolCheck29, LBOOL29_CASINO_BLIP_DISPLACEMENT_DONE)
		ENDIF
	ENDIF
	
	IF bShowInterior
	OR NOT bInsideCasino
		
		IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_CASINO_MISSION_CONTROLLER_BLIPS_HIDDEN)
			SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(LocalPlayerCurrentInterior, DISPLAY_BLIP)
			SET_HIDE_ALL_PLAYER_BLIPS(FALSE)
			CLEAR_BIT(iLocalBoolCheck31, LBOOL31_CASINO_MISSION_CONTROLLER_BLIPS_HIDDEN)
			PRINTLN("[PROCESS_CASINO_BLIPS] - Showing")
		ENDIF
	ELSE
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
				
		IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_CASINO_MISSION_CONTROLLER_BLIPS_HIDDEN)
			SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(LocalPlayerCurrentInterior, DISPLAY_NOTHING)
			SET_HIDE_ALL_PLAYER_BLIPS(TRUE)
			SET_BIT(iLocalBoolCheck31, LBOOL31_CASINO_MISSION_CONTROLLER_BLIPS_HIDDEN)
			PRINTLN("[PROCESS_CASINO_BLIPS] - Hiding")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: When playing Casino Heist, this function will change the map to
///    the map of the correct floor based on the player's Z position
PROC PROCESS_CASINO_HEIST_MINIMAP() 
	
	IF !bLocalPlayerPedOK
		EXIT
	ENDIF
	
	INTERIOR_DATA_STRUCT structInteriorData	
	
	IF iInteriorIndexCasinoMainCH = NULL
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_MAIN)
		iInteriorIndexCasinoMainCH = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Grabbing interior data for iInteriorIndexCasinoMainCH.")
	ENDIF
	
	IF iInteriorIndexCasinoTunnel = NULL
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_TUNNEL)
		iInteriorIndexCasinoTunnel = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Grabbing interior data for iInteriorIndexCasinoTunnel.")
	ENDIF
	
	IF iInteriorIndexCasinoBackArea = NULL
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_BACK_AREA)
		iInteriorIndexCasinoBackArea = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Grabbing interior data for iInteriorIndexCasinoBackArea.")
	ENDIF
	
	IF iInteriorIndexCasinoHotelFloor = NULL
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_HOTEL_FLOOR)
		iInteriorIndexCasinoHotelFloor = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Grabbing interior data for iInteriorIndexCasinoHotelFloor.")
	ENDIF
	
	IF iInteriorIndexCasinoLoadingBay = NULL
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_LOADING_BAY)
		iInteriorIndexCasinoLoadingBay = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Grabbing interior data for iInteriorIndexCasinoLoadingBay.")
	ENDIF
	
	IF iInteriorIndexCasinoVault = NULL
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_VAULT)
		iInteriorIndexCasinoVault = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Grabbing interior data for iInteriorIndexCasinoVault.")
	ENDIF
	
	IF iInteriorIndexCasinoUtilityLift = NULL
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_UTILITY_LIFT)
		iInteriorIndexCasinoUtilityLift = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Grabbing interior data for iInteriorIndexCasinoUtilityLift.")
	ENDIF
	
	IF iInteriorIndexCasinoUtilityShaft = NULL
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_LIFT_SHAFT)
		iInteriorIndexCasinoUtilityShaft = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Grabbing interior data for iInteriorIndexCasinoUtilityShaft.")
	ENDIF
	
	IF LocalPlayerCurrentInterior = NULL
	OR (LocalPlayerCurrentInterior != iInteriorIndexCasinoMainCH
	AND LocalPlayerCurrentInterior != iInteriorIndexCasinoTunnel
	AND LocalPlayerCurrentInterior != iInteriorIndexCasinoBackArea
	AND LocalPlayerCurrentInterior != iInteriorIndexCasinoHotelFloor
	AND LocalPlayerCurrentInterior != iInteriorIndexCasinoLoadingBay
	AND LocalPlayerCurrentInterior != iInteriorIndexCasinoVault
	AND LocalPlayerCurrentInterior != iInteriorIndexCasinoUtilityLift
	AND LocalPlayerCurrentInterior != iInteriorIndexCasinoUtilityShaft)
		PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Not inside a Casino Heist interior.")
		
		IF bIsDisplacedInteriorSet
			bIsDisplacedInteriorSet = FALSE
			CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA()
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST)
			SET_RADAR_ZOOM_PRECISE(0)
			CLEAR_BIT(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST)
		ENDIF
		
		EXIT
	ENDIF
	
	IF NOT bIsDisplacedInteriorSet
	AND g_PlayerBlipsData.DisplacedInteriorThreadID = INT_TO_NATIVE(THREADID, -1)
		VECTOR vCasinoDisplacedCoord = << 965.82, 42.25, 82.0 >>
		bIsDisplacedInteriorSet = TRUE
		SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(NATIVE_TO_INT(iInteriorIndexCasinoMainCH), vCasinoDisplacedCoord)
		SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(NATIVE_TO_INT(iInteriorIndexCasinoTunnel), vCasinoDisplacedCoord)
		SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(NATIVE_TO_INT(iInteriorIndexCasinoBackArea), vCasinoDisplacedCoord)
		SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(NATIVE_TO_INT(iInteriorIndexCasinoHotelFloor), vCasinoDisplacedCoord)
		SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(NATIVE_TO_INT(iInteriorIndexCasinoLoadingBay), vCasinoDisplacedCoord)
		SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(NATIVE_TO_INT(iInteriorIndexCasinoVault), vCasinoDisplacedCoord)
		SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(NATIVE_TO_INT(iInteriorIndexCasinoUtilityLift), vCasinoDisplacedCoord)
		SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(NATIVE_TO_INT(iInteriorIndexCasinoUtilityShaft), vCasinoDisplacedCoord)
	ENDIF
	
	PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - inside a Casino Heist interior.")
	
	INT iInteriorHash = HASH("ch_dlc_casino_FakeHeistComposite") //3616464270
	INT iLevel = 0
	VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
	
	IF IS_LOCAL_PLAYER_USING_DRONE()
		vPlayerPos = GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].vDroneCoords 
	ENDIF
	
	FLOAT fPlayerZ = vPlayerPos.z
	
	PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] Processing Casino Heist Minimap, player Z is ", fPlayerZ, " hash is ", iInteriorHash)
	
	IF fPlayerZ <= -66.5
		IF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__DIRECT
		AND g_sCasinoHeistMissionConfigData.eEntranceChosen = CASINO_HEIST_ACCESS_POINT__SEWER_ENTRANCE
			iLevel = 1
		ELSE
			iLevel = 9 //Version of level 1 without the sewer tunnel.
		ENDIF
	ELIF fPlayerZ <= -61.5
		iLevel = 2
	ELIF fPlayerZ <= -56.5
		iLevel = 3
	ELIF fPlayerZ <= -51
		iLevel = 4
	ELIF fPlayerZ <= -43
		iLevel = 5
	ELIF fPlayerZ <= -35.5
		iLevel = 6
	ELIF fPlayerZ <= -25.5
		iLevel = 7
	ELSE
		iLevel = 8
	ENDIF
	
	PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] iLevel is ", iLevel)
	
	IF iLevel > 0
	AND LocalPlayerCurrentInterior != iInteriorIndexCasinoUtilityShaft
		PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] Calling SET_RADAR_AS_INTERIOR_THIS_FRAME")
		SET_RADAR_AS_INTERIOR_THIS_FRAME(iInteriorHash, 2504.386, -257.22, 0, iLevel)
	ENDIF

	BOOL bShowInterior = TRUE
		
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	
	IF bShowInterior
		IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST)
			IF LocalPlayerCurrentInterior = iInteriorIndexCasinoLoadingBay
				SET_RADAR_ZOOM_PRECISE(0.1)
				SET_BIT(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST)
			ENDIF	
		ELIF LocalPlayerCurrentInterior != iInteriorIndexCasinoLoadingBay
			SET_RADAR_ZOOM_PRECISE(0)
			CLEAR_BIT(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST)
		ELSE
			SET_RADAR_ZOOM_PRECISE(0.1) //Call everyframe
		ENDIF
		
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
		
		IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_CASINO_HEIST_MISSION_CONTROLLER_BLIPS_HIDDEN)
			SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(LocalPlayerCurrentInterior, DISPLAY_BLIP, TRUE, TRUE, TRUE, TRUE)
			SET_HIDE_ALL_PLAYER_BLIPS(FALSE)
			CLEAR_BIT(iLocalBoolCheck32, LBOOL32_CASINO_HEIST_MISSION_CONTROLLER_BLIPS_HIDDEN)
			PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Showing Blips")
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST)
			SET_RADAR_ZOOM_PRECISE(0)
			CLEAR_BIT(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST)
		ENDIF
	
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
		HIDE_PLAYER_ARROW_THIS_FRAME()
		SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(LocalPlayerCurrentInterior, DISPLAY_NOTHING, TRUE, TRUE, TRUE, TRUE)
		
		IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_CASINO_HEIST_MISSION_CONTROLLER_BLIPS_HIDDEN)
			SET_HIDE_ALL_PLAYER_BLIPS(TRUE)
			SET_BIT(iLocalBoolCheck32, LBOOL32_CASINO_HEIST_MISSION_CONTROLLER_BLIPS_HIDDEN)
			PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Hiding Blips")
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_CASINO_ROOF_MINIMAP()
	
	BOOL bZoom = TRUE
		
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bZoom = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bZoom = FALSE
	ENDIF
	
	IF bZoom
		IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_ROOF)
			IF IS_PLAYER_ON_CASINO_ROOF()
				SET_RADAR_ZOOM_PRECISE(0.1)
				SET_BIT(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_ROOF)
			ENDIF
		ELIF NOT IS_PLAYER_ON_CASINO_ROOF()
			SET_RADAR_ZOOM_PRECISE(0)
			CLEAR_BIT(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_ROOF)
		ELSE
			SET_RADAR_ZOOM_PRECISE(0.1) //Call everyframe
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST)
			SET_RADAR_ZOOM_PRECISE(0)
			CLEAR_BIT(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST)
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_MUSIC_LOCKER_MINIMAP()

	VECTOR vInteriorPos = <<1550.0, 250.0, -50.0>>
	VECTOR vExteriorPos = <<988.0, 80.0, 81.0>>
	INT interiorNameHash = HASH("h4_dlc_int_02_h4")
	
	IF LocalPlayerCurrentInterior = NULL
	OR LocalPlayerCurrentInterior != GET_INTERIOR_AT_COORDS(vInteriorPos)
		EXIT
	ENDIF
	 
	BOOL bShowInterior = TRUE
	BOOL bStaggerChange = (GET_FRAME_COUNT() % 5) = 0
	INT iLevel
	
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	
	IF (vLocalPlayerPosition.z <= -48.15)
		iLevel = 0
	ELSE
		iLevel = 1
	ENDIF
	
	PROCESS_INTERIOR_DISPLACEMENT(interiorNameHash, vInteriorPos, vExteriorPos)
	
	IF bShowInterior
		SET_RADAR_AS_INTERIOR_THIS_FRAME(interiorNameHash, vInteriorPos.x, vInteriorPos.y, 0, iLevel)	
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorPos), DISPLAY_BLIP, TRUE, TRUE, TRUE, TRUE)
		ENDIF
	ELSE
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_DISPLAY_OF_ENTITY_BLIPS_INSIDE_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorPos), DISPLAY_NOTHING, TRUE, TRUE, TRUE, TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FIB_OFFICE_MINIMAP()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirty, ciOptionsBS30_EnableFIBOfficeInteriorMinimapZoom)
		EXIT
	ENDIF
	
	INTERIOR_INSTANCE_INDEX iInteriorIndex		
	INTERIOR_DATA_STRUCT structInteriorData	
	structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_FIB_OFFICE)			
	iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
	
	IF LocalPlayerCurrentInterior != iInteriorIndex
		IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_FIB_OFFICE_INTERIOR_ZOOM_ENABLED)
			PRINTLN("PROCESS_FIB_OFFICE_MINIMAP - We're no longer in the FIB office interior")
			SET_INSIDE_VERY_SMALL_INTERIOR(FALSE)
			CLEAR_BIT(iLocalBoolCheck35, LBOOL35_FIB_OFFICE_INTERIOR_ZOOM_ENABLED)
		ENDIF
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck35, LBOOL35_FIB_OFFICE_INTERIOR_ZOOM_ENABLED)
		PRINTLN("PROCESS_FIB_OFFICE_MINIMAP - We're in the FIB office interior")
		SET_INSIDE_VERY_SMALL_INTERIOR(TRUE)
		SET_BIT(iLocalBoolCheck35, LBOOL35_FIB_OFFICE_INTERIOR_ZOOM_ENABLED)
	ENDIF

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Artificial Lights -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Contains some functionality related to turning lights on and off and then applying timecycle mods and playing sound effects to match.  ---------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_PLAY_SOUND_FOR_LIGHTS_OFF()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iPartToUse].iTeam].iRuleBitsetFifteen[GET_TEAM_CURRENT_RULE(MC_PlayerBD[iPartToUse].iTeam)], ciBS_RULE15_BLOCK_TURN_OFF_LIGHTS_SFX)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_ARTIFICIAL_LIGHTS_BE_TURNED_OFF_AGAIN()
		RETURN FALSE
	ENDIF
	
	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_SKIP_ARTIFICIAL_LIGHT_SOUNDS)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck34, LBOOL34_BLOCK_EMP_SOUNDS_DUE_TO_SPECIFIED_INTERIOR)
		RETURN FALSE
	ENDIF
	
	IF g_bFMMCLightsTurnedOff
		RETURN FALSE
	ENDIF
	
	RETURN NOT IS_STRING_NULL_OR_EMPTY(GET_ARTIFICIAL_LIGHTS_SOUNDNAME__LIGHTS_OFF())
ENDFUNC

FUNC BOOL SHOULD_PLAY_SOUND_FOR_LIGHTS_BACK_ON()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iPartToUse].iTeam].iRuleBitsetFifteen[GET_TEAM_CURRENT_RULE(MC_PlayerBD[iPartToUse].iTeam)], ciBS_RULE15_BLOCK_TURN_OFF_LIGHTS_SFX)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_SKIP_ARTIFICIAL_LIGHT_SOUNDS)
		RETURN FALSE
	ENDIF
	
	IF NOT g_bFMMCLightsTurnedOff
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck34, LBOOL34_BLOCK_EMP_SOUNDS_DUE_TO_SPECIFIED_INTERIOR)
		RETURN FALSE
	ENDIF

	RETURN NOT IS_STRING_NULL_OR_EMPTY(GET_ARTIFICIAL_LIGHTS_SOUNDNAME__LIGHTS_BACK_ON())
ENDFUNC

FUNC BOOL SHOULD_CURRENT_INTERIOR_ENFORCE_LIGHTS_OFF_TC_MODIFIER()
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_ISLAND)
		// No suitable interiors on the island
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_LIGHTS_OFF_TC_MODIFIER_BE_APPLIED()
	
	IF IS_PHONE_EMP_CURRENTLY_ACTIVE()		
		PRINTLN("[RCC MISSION][ArtificialLights][CLIENT] - CAN_LIGHTS_OFF_TC_MODIFIER_BE_APPLIED - IS_PHONE_EMP_CURRENTLY_ACTIVE - Return False")
		RETURN FALSE
	ENDIF	
	
	IF (bIsAnySpectator OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_FAIL))
	AND (NOT IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData) OR (IS_SCREEN_FADED_OUT() AND NOT IS_SCREEN_FADING_IN()))
		PRINTLN("[RCC MISSION][ArtificialLights][CLIENT] - CAN_LIGHTS_OFF_TC_MODIFIER_BE_APPLIED - Waiting for Spectator Cam to be active and running - Return False")
		RETURN FALSE
	ENDIF
		
	IF IS_BIT_SET(MC_PlayerBD[iPartToUse].iClientBitset3, PBBOOL3_TURNED_ON_LIGHTS_OFF_TC_MODIFIER)
	AND NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_TURNED_ON_LIGHTS_OFF_TC_MODIFIER)		
		PRINTLN("[RCC MISSION][ArtificialLights][CLIENT] - CAN_LIGHTS_OFF_TC_MODIFIER_BE_APPLIED - Spectator not in sync with spectatoor target - Return TRUE")
		RETURN TRUE
	ENDIF	
	
	IF IS_INTERIOR_SCENE()
	OR IS_LOCAL_PLAYER_WARPING_TO_AN_INTERIOR_LOCATION()
	OR IS_VALID_INTERIOR(GET_INTERIOR_AT_COORDS(GET_ENTITY_COORDS(PlayerPedToUse, FALSE)))
		IF SHOULD_CURRENT_INTERIOR_ENFORCE_LIGHTS_OFF_TC_MODIFIER()
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_CURRENTLY_DAY()
		IF NOT USING_FMMC_YACHT() // For instances where the Yacht is out at sea we want to still use our custom yacht TC modifier even though it's daytime			
			PRINTLN("[RCC MISSION][ArtificialLights][CLIENT] - CAN_LIGHTS_OFF_TC_MODIFIER_BE_APPLIED - Day Time, not in interior - Return False")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_ARTIFICIAL_LIGHTS_AMBIENT_ZONE_STATES()
	
	IF ARE_LIGHTS_TURNED_OFF()
		IF NOT FMMC_IS_LONG_BIT_SET(iLightsAmbientZoneBS, ciLIGHTS_AMBIENT_ZONE_STATE__AZ_DLC_HEISTS_BIOLAB)
			IF IS_PLAYER_IN_BUNKER(LocalPlayer)
			OR iLocalPlayerCurrentInteriorHash = HASH("gr_grdlc_int_02")		
				SET_AMBIENT_ZONE_STATE("AZ_DLC_HEISTS_BIOLAB", TRUE, FALSE)
				PRINTLN("[ArtificialLights] PROCESS_ARTIFICIAL_LIGHTS_AMBIENT_ZONE_STATES | Applying SET_AMBIENT_ZONE_STATE | AZ_DLC_HEISTS_BIOLAB | FALSE")
				FMMC_SET_LONG_BIT(iLightsAmbientZoneBS, ciLIGHTS_AMBIENT_ZONE_STATE__AZ_DLC_HEISTS_BIOLAB)
			ENDIF			
		ENDIF
		
		IF NOT FMMC_IS_LONG_BIT_SET(iLightsAmbientZoneBS, ciLIGHTS_AMBIENT_ZONE_STATE__iz_dlc_xm_int_silo_02_Control)
			IF iLocalPlayerCurrentInteriorHash = HASH("xm_x17dlc_int_silo_02")	
				PRINTLN("[ArtificialLights] PROCESS_ARTIFICIAL_LIGHTS_AMBIENT_ZONE_STATES | Applying SET_AMBIENT_ZONE_STATE | iz_dlc_xm_int_silo_02_Control | FALSE")
				SET_AMBIENT_ZONE_STATE("iz_dlc_xm_int_silo_02_Control", FALSE, TRUE)
				FMMC_SET_LONG_BIT(iLightsAmbientZoneBS, ciLIGHTS_AMBIENT_ZONE_STATE__iz_dlc_xm_int_silo_02_Control)
			ENDIF			
		ENDIF
	ELSE
		IF FMMC_IS_LONG_BIT_SET(iLightsAmbientZoneBS, ciLIGHTS_AMBIENT_ZONE_STATE__iz_dlc_xm_int_silo_02_Control)
			IF iLocalPlayerCurrentInteriorHash != HASH("xm_x17dlc_int_silo_02")	
				PRINTLN("[ArtificialLights] PROCESS_ARTIFICIAL_LIGHTS_AMBIENT_ZONE_STATES | Applying SET_AMBIENT_ZONE_STATE | iz_dlc_xm_int_silo_02_Control | TRUE")
				SET_AMBIENT_ZONE_STATE("iz_dlc_xm_int_silo_02_Control", TRUE, TRUE)
				FMMC_CLEAR_LONG_BIT(iLightsAmbientZoneBS, ciLIGHTS_AMBIENT_ZONE_STATE__iz_dlc_xm_int_silo_02_Control)
			ENDIF			
		ENDIF
	ENDIF
	
ENDPROC

PROC APPLY_LIGHTS_OFF_TC_MODIFIER(BOOL bTurnOnTCModifier, BOOL bFade = FALSE)
	
	PRINTLN("[ArtificialLights] APPLY_LIGHTS_OFF_TC_MODIFIER | called with bTurnOnTCModifier: ", bTurnOnTCModifier, " and bFade: ", bFade)
	
	IF bTurnOnTCModifier
		IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_TURNED_ON_LIGHTS_OFF_TC_MODIFIER)
		
			PRINTLN("[ArtificialLights] APPLY_LIGHTS_OFF_TC_MODIFIER | Applying lights-off TC modifier ", GET_LIGHTS_OFF_TIMECYCLE_MODIFIER(), "! bFade: ", bFade)
			//PUSH_TIMECYCLE_MODIFIER() // To reimplement after more testing when needed
			
			IF bFade
				SET_TRANSITION_TIMECYCLE_MODIFIER(GET_LIGHTS_OFF_TIMECYCLE_MODIFIER(), 2.0)
			ELSE
				SET_TIMECYCLE_MODIFIER(GET_LIGHTS_OFF_TIMECYCLE_MODIFIER())
				SET_TIMECYCLE_MODIFIER_STRENGTH(1.0)
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(GET_LIGHTS_OFF_EXTRA_TIMECYCLE_MODIFIER())
				SET_EXTRA_TCMODIFIER(GET_LIGHTS_OFF_EXTRA_TIMECYCLE_MODIFIER())
			ENDIF
			
			IF IS_PLAYER_IN_BUNKER(LocalPlayer)
			OR iLocalPlayerCurrentInteriorHash = HASH("gr_grdlc_int_02")
				ADD_TCMODIFIER_OVERRIDE("lab_none_exit", "lab_none_exit_OVR")
				ADD_TCMODIFIER_OVERRIDE("lab_none_dark", "lab_none_dark_OVR")
				OVERRIDE_INTERIOR_SMOKE_LEVEL(0.001)				
			ENDIF
			
			SET_BIT(iLocalBoolCheck31, LBOOL31_TURNED_ON_LIGHTS_OFF_TC_MODIFIER)
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_TURNED_ON_LIGHTS_OFF_TC_MODIFIER)
		
			PRINTLN("[ArtificialLights] APPLY_LIGHTS_OFF_TC_MODIFIER | Turning off lights-off TC modifier! bFade: ", bFade)
			
			OVERRIDE_INTERIOR_SMOKE_END()
			
			IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
				IF bFade					
					SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(2.0)
				ELSE				
					CLEAR_TIMECYCLE_MODIFIER()
				ENDIF
			ENDIF
			
			//POP_TIMECYCLE_MODIFIER() // To reimplement after more testing when needed
			
			CLEAR_ALL_TCMODIFIER_OVERRIDES("lab_none_dark")
			CLEAR_ALL_TCMODIFIER_OVERRIDES("lab_none_exit")
			CLEAR_ALL_TCMODIFIER_OVERRIDES("morgue_dark")
			
			CLEAR_BIT(iLocalBoolCheck31, LBOOL31_TURNED_ON_LIGHTS_OFF_TC_MODIFIER)
		ENDIF
	ENDIF
	
	CLEAR_BIT(iLocalBoolCheck35, LBOOL35_APPLY_LIGHTS_OFF_TC_MODIFIER_FOR_SPECTATOR)
	
ENDPROC

PROC PROCESS_LIGHTS_OFF_TC_MODIFIER()
	
	BOOL bFade = TRUE	
	IF (IS_PED_INJURED(localPlayerPed) OR (IS_PLAYER_RESPAWNING(LocalPlayer) AND (NOT IS_FAKE_MULTIPLAYER_MODE_SET() OR IS_FAKE_MULTIPLAYER_MODE_ALLOWING_RESPAWNS())))
	OR bIsAnySpectator
	OR IS_LOCAL_PLAYER_WARPING_TO_AN_INTERIOR_LOCATION()
		bFade = FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_TURNED_ON_LIGHTS_OFF_TC_MODIFIER)
		IF CAN_LIGHTS_OFF_TC_MODIFIER_BE_APPLIED()
			// The TC modifier needs to be re-applied!
			APPLY_LIGHTS_OFF_TC_MODIFIER(TRUE, bFade)
		ENDIF
	ELSE
		IF NOT CAN_LIGHTS_OFF_TC_MODIFIER_BE_APPLIED()
			// The TC modifier shouldn't be on any more even though the lights are off!
			APPLY_LIGHTS_OFF_TC_MODIFIER(FALSE, bFade)
		ENDIF
	ENDIF
ENDPROC

PROC TURN_LIGHTS_OFF_NOW()

	DISABLE_SPECTATOR_FILTER_OPTION(TRUE)
	SET_ARTIFICIAL_VEHICLE_LIGHTS_STATE(FALSE)
	SET_ARTIFICIAL_LIGHTS_STATE(TRUE)
	REINIT_NET_TIMER(stEMPTimeActive)
	
	IF CAN_LIGHTS_OFF_TC_MODIFIER_BE_APPLIED()	
		APPLY_LIGHTS_OFF_TC_MODIFIER(TRUE)
	ENDIF
	
	IF SHOULD_PLAY_SOUND_FOR_LIGHTS_OFF()
		iLightsOffSoundID = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(iLightsOffSoundID, GET_ARTIFICIAL_LIGHTS_SOUNDNAME__LIGHTS_OFF(), GET_ARTIFICIAL_LIGHTS_SOUNDSET__LIGHTS_OFF())
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(GET_LIGHTS_OFF_AUDIO_SCENE_NAME())
		START_AUDIO_SCENE(GET_LIGHTS_OFF_AUDIO_SCENE_NAME())
	ENDIF
	
	iArtificialLightsOffTCIndex = GET_TIMECYCLE_MODIFIER_INDEX()
	g_bFMMCLightsTurnedOff = TRUE
	IF iArtificialLightsOffTCIndex = -1
		CLEAR_BIT(iLocalBoolCheck31, LBOOL31_TURNED_ON_LIGHTS_OFF_TC_MODIFIER)
	ENDIF
	
	PRINTLN("[RCC MISSION][ArtificialLights][CLIENT] - Turning off lights & setting iArtificialLightsOffTCIndex to ", iArtificialLightsOffTCIndex, " || ", GET_LIGHTS_OFF_TIMECYCLE_MODIFIER(), " / ", GET_LIGHTS_OFF_EXTRA_TIMECYCLE_MODIFIER(), " / ", GET_ARTIFICIAL_LIGHTS_SOUNDNAME__LIGHTS_OFF(), " / ", GET_ARTIFICIAL_LIGHTS_SOUNDSET__LIGHTS_OFF(), " / ", GET_LIGHTS_OFF_AUDIO_SCENE_NAME())
	DEBUG_PRINTCALLSTACK()
ENDPROC

PROC TURN_LIGHTS_BACK_ON_NOW()
	
	DISABLE_SPECTATOR_FILTER_OPTION(FALSE)
	SET_ARTIFICIAL_VEHICLE_LIGHTS_STATE(TRUE)
	SET_ARTIFICIAL_LIGHTS_STATE(FALSE)
	RESET_NET_TIMER(stEMPTimeActive)
	
	APPLY_LIGHTS_OFF_TC_MODIFIER(FALSE)
	
	IF SHOULD_PLAY_SOUND_FOR_LIGHTS_BACK_ON()
		iLightsBackOnSoundID = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(iLightsBackOnSoundID, GET_ARTIFICIAL_LIGHTS_SOUNDNAME__LIGHTS_BACK_ON(), GET_ARTIFICIAL_LIGHTS_SOUNDSET__LIGHTS_BACK_ON())
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(GET_LIGHTS_OFF_AUDIO_SCENE_NAME())
		STOP_AUDIO_SCENE(GET_LIGHTS_OFF_AUDIO_SCENE_NAME())
	ENDIF
	
	iArtificialLightsOffTCIndex = -1
	g_bFMMCLightsTurnedOff = FALSE
	PRINTLN("[RCC MISSION][ArtificialLights][CLIENT] - Turning lights back on || ", GET_ARTIFICIAL_LIGHTS_SOUNDNAME__LIGHTS_BACK_ON(), " / ", GET_ARTIFICIAL_LIGHTS_SOUNDSET__LIGHTS_BACK_ON(), " / Turning off scene ", GET_LIGHTS_OFF_AUDIO_SCENE_NAME())
	DEBUG_PRINTCALLSTACK()
ENDPROC

FUNC BOOL PROCESS_TURNING_LIGHTS_OFF()
		
	IF ARE_LIGHTS_TURNED_OFF()
	AND NOT SHOULD_ARTIFICIAL_LIGHTS_BE_TURNED_OFF_AGAIN()
		PROCESS_LIGHTS_OFF_TC_MODIFIER()
		RETURN FALSE
	ENDIF
		
	IF SHOULD_PLAY_SOUND_FOR_LIGHTS_OFF()
		IF NOT REQUEST_SCRIPT_AUDIO_BANK(GET_ARTIFICIAL_LIGHTS_AUDIOBANK__LIGHTS_OFF())
			PRINTLN("[RCC MISSION][ArtificialLights][CLIENT] - PROCESS_TURNING_LIGHTS_OFF waiting for audio bank")
			RETURN FALSE
		ENDIF
	ENDIF
	
	TURN_LIGHTS_OFF_NOW()
	RETURN TRUE
ENDFUNC

FUNC BOOL PROCESS_TURNING_LIGHTS_BACK_ON()
	
	IF NOT ARE_LIGHTS_TURNED_OFF()
		RETURN FALSE
	ENDIF
	
	IF SHOULD_PLAY_SOUND_FOR_LIGHTS_BACK_ON()
		IF NOT REQUEST_SCRIPT_AUDIO_BANK(GET_ARTIFICIAL_LIGHTS_AUDIOBANK__LIGHTS_BACK_ON())
			PRINTLN("[RCC MISSION][ArtificialLights][CLIENT] - PROCESS_TURNING_LIGHTS_BACK_ON waiting for audio bank")
			RETURN FALSE
		ENDIF
	ENDIF
	
	TURN_LIGHTS_BACK_ON_NOW()
	RETURN TRUE
ENDFUNC

PROC PROCESS_ARTIFICIAL_LIGHTS_SERVER(INT iTeam, INT iRule)
	
	INT iTime = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iArtificalLightTimer[iRule]
	
	#IF IS_DEBUG_BUILD
		IF bPrintArticificalLightTimerValue
			PRINTLN("[RCC MISSION][ArtificialLights] PROCESS_ARTIFICIAL_LIGHTS_SERVER | iArtificalLightTimer value = ", iTime)
		ENDIF
		IF bActivateArtificialLightsDebug
			iTime = iDebugArtificialLightsOffDuration
		ENDIF
	#ENDIF
	
	IF iTime = FMMC_ARTIFICIAL_LIGHTS_ALWAYS_ON
		IF HAS_NET_TIMER_STARTED(MC_serverBD_2.sArtificialLightsServerData.sDelayTimer)
		OR MC_serverBD_2.sArtificialLightsServerData.eBaseState != eARTIFICIAL_LIGHTS_STATE__ON
			RESET_NET_TIMER(MC_serverBD_2.sArtificialLightsServerData.sDelayTimer)
			MC_serverBD_2.sArtificialLightsServerData.eBaseState = eARTIFICIAL_LIGHTS_STATE__ON
			PRINTLN("[RCC MISSION][ArtificialLights] PROCESS_ARTIFICIAL_LIGHTS_SERVER | Turning Lights back on!")
		ENDIF
	ENDIF
	
	SWITCH MC_serverBD_2.sArtificialLightsServerData.eBaseState
		CASE eARTIFICIAL_LIGHTS_STATE__ON
			
			IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_2.sArtificialLightsServerData.sDelayTimer)
				IF iTime != FMMC_ARTIFICIAL_LIGHTS_ALWAYS_ON
				AND iTime > 0
					MC_serverBD_2.sArtificialLightsServerData.iDelayDuration = iTime
					START_NET_TIMER(MC_serverBD_2.sArtificialLightsServerData.sDelayTimer)
					MC_serverBD_2.sArtificialLightsServerData.eBaseState = eARTIFICIAL_LIGHTS_STATE__WAITING_TO_TURN_OFF
					
					PRINTLN("[RCC MISSION][ArtificialLights] PROCESS_ARTIFICIAL_LIGHTS_SERVER | team ", iTeam, ", rule ", iRule, ", iDelayDuration = ", MC_serverBD_2.sArtificialLightsServerData.iDelayDuration)
					PRINTLN("[RCC MISSION][ArtificialLights] PROCESS_ARTIFICIAL_LIGHTS_SERVER | started delay timer.")
					PRINTLN("[RCC MISSION][ArtificialLights] PROCESS_ARTIFICIAL_LIGHTS_SERVER | going to waiting to turn off.")
				ENDIF	
			ENDIF
			
		BREAK
		
		CASE eARTIFICIAL_LIGHTS_STATE__WAITING_TO_TURN_OFF
			
			IF HAS_NET_TIMER_EXPIRED(MC_serverBD_2.sArtificialLightsServerData.sDelayTimer, MC_serverBD_2.sArtificialLightsServerData.iDelayDuration)
				MC_serverBD_2.sArtificialLightsServerData.eBaseState = eARTIFICIAL_LIGHTS_STATE__OFF
				PRINTLN("[RCC MISSION][ArtificialLights] PROCESS_ARTIFICIAL_LIGHTS_SERVER | iDelayDuration = ", MC_serverBD_2.sArtificialLightsServerData.iDelayDuration)
				PRINTLN("[RCC MISSION][ArtificialLights] PROCESS_ARTIFICIAL_LIGHTS_SERVER | going to state off.")
			ELSE
				PRINTLN("[RCC MISSION][ArtificialLights] PROCESS_ARTIFICIAL_LIGHTS_SERVER | Waiting for delay timer | Length: ", MC_serverBD_2.sArtificialLightsServerData.iDelayDuration)
			ENDIF
			
		BREAK
		
		CASE eARTIFICIAL_LIGHTS_STATE__OFF
			// In state off. 
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC PROCESS_ARTIFICIAL_LIGHTS_SOUND_EFFECT_CLEANUP(INT& iLightsSoundID, STRING sAudioBank)
	IF iLightsSoundID != -1
		IF HAS_SOUND_FINISHED(iLightsSoundID)
		
			IF SAFE_TO_UNLOAD_THIS_AUDIO_BANK_MID_MISSION(sAudioBank)
				RELEASE_NAMED_SCRIPT_AUDIO_BANK(sAudioBank)
			ENDIF
			
			iLightsSoundID = -1
			PRINTLN("[RCC MISSION][ArtificialLights][CLIENT] - Cleaning up iLightsSoundID and releasing audio bank ", sAudioBank)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_ARTIFICIAL_LIGHTS_SOUND_EFFECTS()
	PROCESS_ARTIFICIAL_LIGHTS_SOUND_EFFECT_CLEANUP(iLightsOffSoundID, GET_ARTIFICIAL_LIGHTS_AUDIOBANK__LIGHTS_OFF())
	PROCESS_ARTIFICIAL_LIGHTS_SOUND_EFFECT_CLEANUP(iLightsBackOnSoundID, GET_ARTIFICIAL_LIGHTS_AUDIOBANK__LIGHTS_BACK_ON())
ENDPROC

PROC PROCESS_ARTIFICIAL_LIGHTS_CLIENT()
	
	IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_BLOCK_TURNING_OFF_ARTIFICIAL_LIGHTS_EOM)	
		EXIT
	ENDIF
			
	eARTIFICIAL_LIGHTS_STATE eLightsStateToUse = MC_serverBD_2.sArtificialLightsServerData.eBaseState
	
	IF DOES_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(g_FMMC_STRUCT.iArtificialLightControlTeam)
	AND g_FMMC_STRUCT.sFMMCEndConditions[g_FMMC_STRUCT.iArtificialLightControlTeam].iArtificalLightTimer[GET_TEAM_CURRENT_RULE(g_FMMC_STRUCT.iArtificialLightControlTeam)] = FMMC_ARTIFICIAL_LIGHTS_FORCE_ON_AT_RULE_START
		IF ARE_LIGHTS_TURNED_OFF()
			PRINTLN("[ArtificialLights] PROCESS_ARTIFICIAL_LIGHTS_CLIENT | Turning on lights now due to FMMC_ARTIFICIAL_LIGHTS_FORCE_ON_AT_RULE_START")
			TURN_ON_ARTIFICIAL_LIGHTS_LOCALLY()
		ENDIF
	ENDIF
	
	IF iMaxEMPDuration > -1
		IF HAS_NET_TIMER_EXPIRED(stEMPTimeActive, iMaxEMPDuration)
			PRINTLN("[ArtificialLights] PROCESS_ARTIFICIAL_LIGHTS_CLIENT | iMaxEMPDuration timer has expired!")
			BROADCAST_FMMC_SET_ARTIFICIAL_LIGHTS(eARTIFICIAL_LIGHTS_STATE__DEFAULT, TRUE, NULL)
			iMaxEMPDuration = -1
		ENDIF
	ENDIF
	
	IF eLocalArtificialLightsState != eARTIFICIAL_LIGHTS_STATE__DEFAULT
		// Use the local overriden light state
		eLightsStateToUse = eLocalArtificialLightsState
	ENDIF
	
	IF SHOULD_EMP_BE_IGNORED_DUE_TO_CURRENT_INTERIOR()
		IF eLightsStateToUse != eARTIFICIAL_LIGHTS_STATE__ON
			// The player isn't currently in the EMP'd interior! Keep the lights on until they enter it
			eLightsStateToUse = eARTIFICIAL_LIGHTS_STATE__ON
		ENDIF
		
		SET_BIT(iLocalBoolCheck34, LBOOL34_BLOCK_EMP_SOUNDS_DUE_TO_SPECIFIED_INTERIOR)
	ENDIF
	
	IF NOT bIsAnySpectator	
		IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_TURNED_ON_LIGHTS_OFF_TC_MODIFIER)
			SET_BIT(MC_PlayerBD[iLocalPart].iClientBitset3, PBBOOL3_TURNED_ON_LIGHTS_OFF_TC_MODIFIER)
		ELSE
			CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitset3, PBBOOL3_TURNED_ON_LIGHTS_OFF_TC_MODIFIER)
		ENDIF
		MC_PlayerBD[iLocalPart].eLightsStateToUse = eLightsStateToUse
	ELSE	
		eLightsStateToUse = MC_PlayerBD[iPartToUse].eLightsStateToUse
	ENDIF
	
	// Maintain lights going on/off.
	SWITCH eLightsStateToUse
		CASE eARTIFICIAL_LIGHTS_STATE__ON
		CASE eARTIFICIAL_LIGHTS_STATE__WAITING_TO_TURN_OFF
			PROCESS_TURNING_LIGHTS_BACK_ON()
		BREAK
		
		CASE eARTIFICIAL_LIGHTS_STATE__OFF
			PROCESS_TURNING_LIGHTS_OFF()
		BREAK
	ENDSWITCH
	
	PROCESS_ARTIFICIAL_LIGHTS_SOUND_EFFECTS()
	
	PROCESS_ARTIFICIAL_LIGHTS_AMBIENT_ZONE_STATES()
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: General Interiors  ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Handles loading/pinning interiors.  ------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC MAINTAIN_DISABLED_APARTMENT_ACCESS_FOR_COPS()
	IF IS_CURRENT_OBJECTIVE_LOSE_WANTED()
		IF NOT g_bDisablePropertyAccessForLoseCopMissionObj
			g_bDisablePropertyAccessForLoseCopMissionObj = TRUE
			PRINTLN("g_bDisablePropertyAccessForLoseCopMissionObj set to true - 1")
		ENDIF
	ELIF IS_CURRENT_OBJECTIVE_LOSE_GANG_CHASE()
		IF NOT g_bDisablePropertyAccessForLoseCopMissionObj
			g_bDisablePropertyAccessForLoseCopMissionObj = TRUE
			PRINTLN("g_bDisablePropertyAccessForLoseCopMissionObj set to true - 2")
		ENDIF
	ELIF IS_CURRENT_OBJECTIVE_REPAIR_VEHICLE()
		IF NOT g_bDisablePropertyAccessForLoseCopMissionObj
			g_bDisablePropertyAccessForLoseCopMissionObj = TRUE
			PRINTLN("g_bDisablePropertyAccessForLoseCopMissionObj set to true - 3")
		ENDIF
	ELSE
		IF g_bDisablePropertyAccessForLoseCopMissionObj
			g_bDisablePropertyAccessForLoseCopMissionObj = FALSE
			PRINTLN("g_bDisablePropertyAccessForLoseCopMissionObj set to FALSE - 1")
		ENDIF
	ENDIF
ENDPROC

PROC DETONATE_INTERIOR_DESTRUCTION(INT iDestructionIndex)

	PRINTLN("[InteriorDestruction][ID ", iDestructionIndex, "] DETONATE_INTERIOR_DESTRUCTION | Detonating interior ", NATIVE_TO_INT(sInteriorDestructionVars[iDestructionIndex].intInteriorToDestroy))
	
	RESET_NET_TIMER(sInteriorDestructionVars[iDestructionIndex].stInteriorDestructionTimer)
	
	// Show the explosion at the interior entrance
	FLOAT fDistanceToEntrance = VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), g_FMMC_STRUCT.sInteriorDestructionData[iDestructionIndex].vInteriorEntrance)
	FLOAT fBaseShakeSize = 1.0
	FLOAT fShakeSize = (POW(fBaseShakeSize, 2.0) / fDistanceToEntrance)
	
	SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE", fShakeSize)
	
	// Create door explosion PTFX
	IF HAS_NAMED_PTFX_ASSET_LOADED("scr_tn_tr")
		USE_PARTICLE_FX_ASSET("scr_tn_tr")
		START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_tn_tr_door_explosion", g_FMMC_STRUCT.sInteriorDestructionData[iDestructionIndex].vInteriorEntrance, (<<0.0, 0.0, g_FMMC_STRUCT.sInteriorDestructionData[iDestructionIndex].fInteriorEntranceHeading>>), 1.0, DEFAULT, DEFAULT, DEFAULT)
		PLAY_SOUND_FROM_COORD(-1, "External_Explosion", g_FMMC_STRUCT.sInteriorDestructionData[iDestructionIndex].vInteriorEntrance, "Methamphetamine_Job_Sounds")
	ENDIF

	// Create smoke PTFX (Temporarily removed)
	IF HAS_NAMED_PTFX_ASSET_LOADED("scr_tn_tr")
		USE_PARTICLE_FX_ASSET("scr_tn_tr")
		START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_tn_tr_door_smoke", g_FMMC_STRUCT.sInteriorDestructionData[iDestructionIndex].vInteriorEntrance, (<<0.0, 0.0, g_FMMC_STRUCT.sInteriorDestructionData[iDestructionIndex].fInteriorEntranceHeading>>), 1.0)
	ENDIF
	
	IF LocalPlayerCurrentInterior = sInteriorDestructionVars[iDestructionIndex].intInteriorToDestroy
	AND NOT (IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT())
		// Handle what happens if the player is still in the interior
		PRINTLN("[InteriorDestruction][ID ", iDestructionIndex, "] DETONATE_INTERIOR_DESTRUCTION | Player was still in the interior as it exploded!")
		ADD_EXPLOSION(g_FMMC_STRUCT.sInteriorDestructionData[iDestructionIndex].vDestructionOrigin, EXP_TAG_HI_OCTANE, 1.0)
		PLAY_SOUND_FROM_COORD(-1, "Internal_Explosion", g_FMMC_STRUCT.sInteriorDestructionData[iDestructionIndex].vDestructionOrigin, "Methamphetamine_Job_Sounds")

		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
			SET_ENTITY_HEALTH(LocalPlayerPed, 0)
		ENDIF
		
		SHAKE_GAMEPLAY_CAM("LARGE_EXPLOSION_SHAKE", 1.0)
	ENDIF
	
ENDPROC

PROC INITIALISE_INTERIOR_DESTRUCTION(INT iDestructionIndex)
	PRINTLN("[InteriorDestruction][ID ", iDestructionIndex, "] INITIALISE_INTERIOR_DESTRUCTION | Initialising interior destruction!")
	SET_BIT(iLocalBoolCheck34, LBOOL34_USING_INTERIOR_DESTRUCTION)
	
	REINIT_NET_TIMER(sInteriorDestructionVars[iDestructionIndex].stInteriorDestructionTimer)
	
	sInteriorDestructionVars[iDestructionIndex].intInteriorToDestroy = GET_INTERIOR_AT_COORDS(g_FMMC_STRUCT.sInteriorDestructionData[iDestructionIndex].vDestructionOrigin)
	PRINTLN("[InteriorDestruction][ID ", iDestructionIndex, "] INITIALISE_INTERIOR_DESTRUCTION | Grabbed interior at ", g_FMMC_STRUCT.sInteriorDestructionData[iDestructionIndex].vDestructionOrigin, ": ", NATIVE_TO_INT(sInteriorDestructionVars[iDestructionIndex].intInteriorToDestroy), " | Player's current interior: ", NATIVE_TO_INT(LocalPlayerCurrentInterior))
ENDPROC

FUNC BOOL SHOULD_DISPLAY_INTERIOR_DESTRUCTION_TIMER(INT iDestructionIndex)
	
	IF LocalPlayerCurrentInterior = sInteriorDestructionVars[iDestructionIndex].intInteriorToDestroy
		PRINTLN("[InteriorDestruction][ID ", iDestructionIndex, "] SHOULD_DISPLAY_INTERIOR_DESTRUCTION_TIMER | Player is in the relevant interior! || sInteriorDestructionVars[", iDestructionIndex, "].intInteriorToDestroy: ", NATIVE_TO_INT(sInteriorDestructionVars[iDestructionIndex].intInteriorToDestroy))
		RETURN TRUE
	ELSE
		PRINTLN("[InteriorDestruction][ID ", iDestructionIndex, "] SHOULD_DISPLAY_INTERIOR_DESTRUCTION_TIMER | Player isn't in the relevant interior! || sInteriorDestructionVars[", iDestructionIndex, "].intInteriorToDestroy: ", NATIVE_TO_INT(sInteriorDestructionVars[iDestructionIndex].intInteriorToDestroy), " / LocalPlayerCurrentInterior: ", NATIVE_TO_INT(LocalPlayerCurrentInterior))
	ENDIF
	
	FLOAT fDistanceToEntrance = VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), g_FMMC_STRUCT.sInteriorDestructionData[iDestructionIndex].vInteriorEntrance)
	IF fDistanceToEntrance < POW(cfInteriorDestruction_TimerDistance, 2.0)
		PRINTLN("[InteriorDestruction][ID ", iDestructionIndex, "] SHOULD_DISPLAY_INTERIOR_DESTRUCTION_TIMER | Player is close to the entrance!")
		RETURN TRUE
	ELSE
		PRINTLN("[InteriorDestruction][ID ", iDestructionIndex, "] SHOULD_DISPLAY_INTERIOR_DESTRUCTION_TIMER | Player is far away from the entrance!")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DISPLAY_INTERIOR_DESTRUCTION_ESCAPE_OBJECTIVE_TEXT(INT iDestructionIndex)
	
	IF g_FMMC_STRUCT.sInteriorDestructionData[iDestructionIndex].iInteriorDestruction_EscapeObjectiveTextIndex = -1
		RETURN FALSE
	ENDIF
	
	IF LocalPlayerCurrentInterior = sInteriorDestructionVars[iDestructionIndex].intInteriorToDestroy
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_INTERIOR_DESTRUCTION(INT iDestructionIndex)
	
	IF NOT HAS_NET_TIMER_STARTED(sInteriorDestructionVars[iDestructionIndex].stInteriorDestructionTimer)
		EXIT
	ENDIF
	
	IF RUN_TIMER(sInteriorDestructionVars[iDestructionIndex].stInteriorDestructionTimer, g_FMMC_STRUCT.sInteriorDestructionData[iDestructionIndex].iInteriorDestructionTimeLimit)
		DETONATE_INTERIOR_DESTRUCTION(iDestructionIndex)
		
	ELSE
		// Load PTFX ready for after the detonation
		REQUEST_NAMED_PTFX_ASSET("scr_tn_tr")
		REQUEST_NAMED_PTFX_ASSET("scr_vw_finale")

		// Display the timer
		IF SHOULD_DISPLAY_INTERIOR_DESTRUCTION_TIMER(iDestructionIndex)
			
			HUD_COLOURS eTimerColour = HUD_COLOUR_WHITE
			
			IF LocalPlayerCurrentInterior = sInteriorDestructionVars[iDestructionIndex].intInteriorToDestroy
				eTimerColour = HUD_COLOUR_RED
			ENDIF
			
			INT iTimeRemaining = GET_INTERIOR_DESTRUCTION_TIME_REMAINING(iDestructionIndex)
			INT iSecondsRemaining = iTimeRemaining / 1000
			DRAW_GENERIC_TIMER(iTimeRemaining, "INT_DES_TMR", 0, DEFAULT, DEFAULT, DEFAULT, HUDORDER_TOP, FALSE, eTimerColour, DEFAULT, DEFAULT, DEFAULT, eTimerColour)
			
			IF sInteriorDestructionVars[iDestructionIndex].iCachedInteriorDestructionTimeRemaining != iSecondsRemaining
				STRING sCountdownSoundToPlay = ""
				
				IF iSecondsRemaining <= 0
					sCountdownSoundToPlay = "Countdown0"
				ELIF iSecondsRemaining <= 5
					sCountdownSoundToPlay = "Countdown5to1"
				ELIF iSecondsRemaining <= 30
					sCountdownSoundToPlay = "Countdown30to5"
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sCountdownSoundToPlay)
					PRINTLN("[InteriorDestruction][ID ", iDestructionIndex, "] PROCESS_INTERIOR_DESTRUCTION | Playing countdown sound ", sCountdownSoundToPlay, " || Seconds remaining: ", iSecondsRemaining)
					PLAY_SOUND_FRONTEND(-1, sCountdownSoundToPlay, "Methamphetamine_Job_Sounds")
				ENDIF
				
				sInteriorDestructionVars[iDestructionIndex].iCachedInteriorDestructionTimeRemaining = iSecondsRemaining
			ENDIF
		ENDIF
		
		IF SHOULD_DISPLAY_INTERIOR_DESTRUCTION_ESCAPE_OBJECTIVE_TEXT(iDestructionIndex)
			OVERRIDE_OBJECTIVE_TEXT_WITH_CUSTOM_STRING_THIS_FRAME(g_FMMC_STRUCT.sInteriorDestructionData[iDestructionIndex].iInteriorDestruction_EscapeObjectiveTextIndex)
		ENDIF
	ENDIF
	
	IF GET_INTERIOR_DESTRUCTION_TIME_REMAINING(iDestructionIndex) <= ciInteriorDestruction_BlockPortalTime
		IF g_FMMC_STRUCT.sInteriorDestructionData[iDestructionIndex].iWarpPortalToDisableStartPos > -1
		AND NOT IS_BIT_SET(iInteriorDestruction_BlockWarpPortalStartPosBS, g_FMMC_STRUCT.sInteriorDestructionData[iDestructionIndex].iWarpPortalToDisableStartPos)
			SET_BIT(iInteriorDestruction_BlockWarpPortalStartPosBS, g_FMMC_STRUCT.sInteriorDestructionData[iDestructionIndex].iWarpPortalToDisableStartPos)
			PRINTLN("[InteriorDestruction][ID ", iDestructionIndex, "] PROCESS_INTERIOR_DESTRUCTION | Blocking warp portal ", g_FMMC_STRUCT.sInteriorDestructionData[iDestructionIndex].iWarpPortalToDisableStartPos, "'s start pos")
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bInteriorDestructionDebug
		DRAW_DEBUG_SPHERE(g_FMMC_STRUCT.sInteriorDestructionData[iDestructionIndex].vDestructionOrigin, 1.0, 255, 0, 0, 100)
		DRAW_DEBUG_SPHERE(g_FMMC_STRUCT.sInteriorDestructionData[iDestructionIndex].vInteriorEntrance, 1.0, 0, 255, 0, 100)
	ENDIF
	#ENDIF
ENDPROC

PROC PROCESS_CASINO_APARTMENT()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_APARTMENT)
		EXIT
	ENDIF
	
	IF interiorCasinoApartmentIndex = NULL
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.sCasinoApartment.iCasinoPartyRadio != -1
		INTERIOR_INSTANCE_INDEX iInteriorIndex = GET_INTERIOR_FROM_ENTITY(PlayerPedToUse)
		IF iInteriorIndex = interiorCasinoApartmentIndex
			IF IS_AUDIO_SCENE_ACTIVE("MP_POSITIONED_RADIO_MUTE_SCENE")
				SET_AUDIO_SCENE_VARIABLE("MP_POSITIONED_RADIO_MUTE_SCENE", "apply", 0)
			ENDIF
			SET_BIT(iLocalBoolCheck32, LBOOL32_APARTMENT_RADIO_MUTE)
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_APARTMENT_RADIO_MUTE)
				CLEAR_BIT(iLocalBoolCheck32, LBOOL32_APARTMENT_RADIO_MUTE)
				IF IS_AUDIO_SCENE_ACTIVE("MP_POSITIONED_RADIO_MUTE_SCENE")
					SET_AUDIO_SCENE_VARIABLE("MP_POSITIONED_RADIO_MUTE_SCENE", "apply", 1.0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CARGO_PLANE_INTERIOR_MINIMAP()
	
	IF NOT DOES_ENTITY_EXIST(viCargoPlane)
	OR NOT IS_ENTITY_ALIVE(viCargoPlane)
		EXIT
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(viCargoPlane)
	
		VECTOR vPlanePos = GET_ENTITY_COORDS(viCargoPlane)
		
		SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("V_FakeCargoPlaneDive"), vPlanePos.x, vPlanePos.y, FLOOR(GET_ENTITY_HEADING(viCargoPlane)), 0)
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
		
		DONT_ZOOM_MINIMAP_WHEN_RUNNING_THIS_FRAME()
		SET_RADAR_ZOOM_PRECISE(1)
		
		SET_BIT(iLocalBoolCheck35, LBOOL35_MINIMAP_ZOOMED_IN)
	
	ENDIF
	
ENDPROC

PROC PROCESS_FORCED_MINIMAP_INTERIOR()
	
	INT iTeam = GET_LOCAL_PLAYER_TEAM(TRUE)
	
	IF NOT IS_TEAM_INDEX_VALID(iTeam)
		EXIT
	ENDIF
	
	INT iRule = GET_TEAM_CURRENT_RULE(iTeam)
	
	IF NOT IS_RULE_INDEX_VALID(iRule)
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iForcedMinimapInterior[iRule] = ciFMMC_FORCED_MINIMAP_INTERIOR_OFF
		IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_MINIMAP_ZOOMED_IN)
			CLEAR_BIT(iLocalBoolCheck35, LBOOL35_MINIMAP_ZOOMED_IN)
			SET_RADAR_ZOOM_PRECISE(0)
		ENDIF
		EXIT
	ENDIF
	
	SWITCH g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iForcedMinimapInterior[iRule]
		CASE ciFMMC_FORCED_MINIMAP_INTERIOR_MANSION_SEARCH
			SET_RADAR_AS_INTERIOR_THIS_FRAME(ciMansionSearchHash, -1540, 150)
		BREAK
		CASE ciFMMC_FORCED_MINIMAP_INTERIOR_CARGO_PLANE
			PROCESS_CARGO_PLANE_INTERIOR_MINIMAP()
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC VECTOR GET_MUSIC_LOCKER_LIGHT_PROP_OFFSET(INT iLight)
	
	VECTOR vMusicLocker = vMusicLockerLocation
	VECTOR vReturn
	
	SWITCH iLight
		CASE 0 vReturn = <<3.7219, -1.0028, 5.721>>		BREAK
		CASE 1 vReturn = <<5.8927, 2.1514, 5.4099>>		BREAK
		CASE 2 vReturn = <<8.0635, 3.4921, 5.721>>		BREAK
		CASE 3 vReturn = <<10.2343, -1.0028, 5.721>>	BREAK
		CASE 4 vReturn = <<5.8927, -3.7085, 5.721>>		BREAK
		CASE 5 vReturn = <<12.4051, 3.4921, 5.721>>		BREAK
	ENDSWITCH
	
	RETURN GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vMusicLocker, 0.0, vReturn)
ENDFUNC

PROC CREATE_FMMC_MUSIC_LOCKER_LIGHT_PROPS(INTERIOR_INSTANCE_INDEX iInteriorIndex)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MUSIC_LOCKER_ENABLE_DJ_SET)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MUSIC_LOCKER_DISABLE_SPOTLIGHTS)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_MusicLockerLightsCreated)
		EXIT
	ENDIF
	
	REQUEST_MODEL(mnMusicLockerLight)
	IF NOT HAS_MODEL_LOADED(mnMusicLockerLight)
		PRINTLN("CREATE_FMMC_MUSIC_LOCKER_LIGHT_PROPS - Light model not loaded yet")
		EXIT
	ENDIF
	
	IF NOT IS_INTERIOR_READY(iInteriorIndex)
		PRINTLN("CREATE_FMMC_MUSIC_LOCKER_LIGHT_PROPS - Interior not ready yet")
		EXIT
	ENDIF
	
	INT iMusicLockerLight
	FOR iMusicLockerLight = 0 TO ciMAX_MUSIC_LOCKER_LIGHTS-1
		VECTOR vPos = GET_MUSIC_LOCKER_LIGHT_PROP_OFFSET(iMusicLockerLight)
		oiMusicLockerLights[iMusicLockerLight] = CREATE_OBJECT_NO_OFFSET(mnMusicLockerLight, vPos, FALSE, FALSE)
		FORCE_ROOM_FOR_ENTITY(oiMusicLockerLights[iMusicLockerLight], iInteriorIndex, ciMusicLockerRoomKey)
		SET_PROP_LIGHT_COLOR(oiMusicLockerLights[iMusicLockerLight], TRUE, 255, 150, 225)
		PRINTLN("CREATE_FMMC_MUSIC_LOCKER_LIGHT_PROPS - Created light model ", iMusicLockerLight)
	ENDFOR
	
	SET_BIT(iLocalBoolCheck29, LBOOL29_MusicLockerLightsCreated)
	
	SET_MODEL_AS_NO_LONGER_NEEDED(mnMusicLockerLight)
	
ENDPROC

PROC GET_FIB_OFFICE_PROP_DETAILS(FIB_OFFICE_PROP_STRUCT& sDetails, INT iIndex)
	SWITCH iIndex
		CASE 0
			sDetails.vPosition = <<143.155,-740.716,241.129>>
			sDetails.vRotation = <<0,0,70>>
		BREAK
		CASE 1
			sDetails.vPosition = <<138.247,-743.336,241.129>>
			sDetails.vRotation = <<0,0,160>>
		BREAK
		CASE 2
			sDetails.vPosition = <<131.583,-740.912,241.129>>
			sDetails.vRotation = <<0,0,160>>
		BREAK
	ENDSWITCH
ENDPROC

PROC PROCESS_INTERIORS_EVERY_FRAME()
	INT iTeam = MC_PlayerBD[iPartToUse].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		INTERIOR_INSTANCE_INDEX playerInterior = LocalPlayerCurrentInterior
		
		VECTOR vPos
		INT interiorNameHash = -1
		INT interiorGroupId = -1
		IF IS_VALID_INTERIOR(playerInterior)
			GET_INTERIOR_LOCATION_AND_NAMEHASH(playerInterior, vPos, interiorNameHash)
			interiorGroupId = GET_INTERIOR_GROUP_ID(playerInterior)
		ELSE
			playerInterior = NULL
		ENDIF
		
		PROCESS_EVERY_FRAME_INTERIOR_AUDIO(playerInterior, interiorNameHash, interiorGroupId)
		
		IF IS_BIT_SET(iLocalBoolCheck34, LBOOL34_USING_INTERIOR_DESTRUCTION)
			INT iDestruction = 0
			FOR iDestruction = 0 TO ciMAX_INTERIOR_DESTRUCTIONS - 1
				PROCESS_INTERIOR_DESTRUCTION(iDestruction)
			ENDFOR
		ENDIF

		// HEISTS 2 - SUBMARINE
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SUB)
		AND playerInterior != NULL
		AND interiorNameHash = HASH("xm_x17dlc_int_sub") // GET_INTERIOR_AT_COORDS(<<512.7328, 4881.1152, -63.5867>>)
		
			// Use room key to narrow it down
			IF iLocalPlayerCurrentRoomKey = HASH("Rm_Escape")
				SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableEasyLadderConditions, TRUE)
				CDEBUG3LN(DEBUG_NET_MISSION,"[RCC MISSION] - PROCESS_INTERIORS_EVERY_FRAME - Player ped reset flag set this frame, PCF_DisableEasyLadderConditions")
			ENDIF

		ENDIF
		
		// HEISTS 2 - HANGAR
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_HANGAR)
		
			IF NOT DOES_ENTITY_EXIST(oiHangarWayfinding)
			
				INTERIOR_DATA_STRUCT structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_HANGAR)			
				INTERIOR_INSTANCE_INDEX iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
				IF NOT IS_VALID_INTERIOR(iInteriorIndex)
					ASSERTLN("[RCC MISSION] - PROCESS_INTERIORS_EVERY_FRAME - iInteriorIndex is INVALID - cannot create wayfinding prop")
				ELSE
					VECTOR vHangarCoords
					INT vHangarHash
					GET_INTERIOR_LOCATION_AND_NAMEHASH(iInteriorIndex, vHangarCoords, vHangarHash)

					IF NOT IS_VECTOR_ZERO(vHangarCoords)					
						MODEL_NAMES wayfindingProp = INT_TO_ENUM(MODEL_NAMES, HASH("xm_Prop_xm17_Wayfinding"))
						REQUEST_MODEL(wayfindingProp)
						IF HAS_MODEL_LOADED(wayfindingProp)
						AND IS_INTERIOR_READY(iInteriorIndex)
							oiHangarWayfinding = CREATE_OBJECT_NO_OFFSET(wayfindingProp, vHangarCoords, FALSE, FALSE)
							RETAIN_ENTITY_IN_INTERIOR(oiHangarWayfinding, iInteriorIndex)
							FORCE_ROOM_FOR_ENTITY(oiHangarWayfinding, iInteriorIndex, HASH("GtaMloRoom001"))
							SET_MODEL_AS_NO_LONGER_NEEDED(wayfindingProp)
							PRINTLN("[RCC MISSION] - PROCESS_INTERIORS_EVERY_FRAME - Hangar - Created wayfinding prop @ coords: ", vHangarCoords)
						ENDIF					
					ENDIF
				ENDIF
			ENDIF		
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetTwentyFour, ciOptionsBS24_EnableCasino_InteriorScripts)
				MAINTAIN_PRIZE_VEHICLE_MISSION()
			ENDIF
			
			#IF IS_DEBUG_BUILD
			g_PlayerBlipsData.bDebugBlipOutput = TRUE
			#ENDIF
			PRINTLN("interiorNameHash: ", interiorNameHash, " Casino hash: ", HASH("vw_dlc_casino_main"))
			IF IS_VALID_INTERIOR(playerInterior)
			AND interiorNameHash = HASH("vw_dlc_casino_main")
				IF NOT g_bInInteriorThatRequiresInstantFadeout
					PRINTLN("[RCC MISSION] - PROCESS_INTERIORS_EVERY_FRAME - g_bInInteriorThatRequiresInstantFadeout = TRUE")
					g_bInInteriorThatRequiresInstantFadeout = TRUE
				ENDIF
			ELSE
				IF g_bInInteriorThatRequiresInstantFadeout
					PRINTLN("[RCC MISSION] - PROCESS_INTERIORS_EVERY_FRAME - g_bInInteriorThatRequiresInstantFadeout = FALSE")
					g_bInInteriorThatRequiresInstantFadeout = FALSE
				ENDIF
			ENDIF
		ENDIF
				
		IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_LOCK_CASINO_DOORS)
		AND (GET_FRAME_COUNT() % 10) = 0
			IF IS_ENTITY_ALIVE(LocalPlayerPed)
				VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)			
				IF VDIST2(vPlayerPos, <<925.04, 46.48, 80.0960>>) < 100.0
					ADD_CASINO_DOOR_TO_SYSTEM_AND_LOCK(CED_LEFT_SIDE_LEFT_DOOR, TRUE)
					ADD_CASINO_DOOR_TO_SYSTEM_AND_LOCK(CED_LEFT_SIDE_RIGHT_DOOR, TRUE)
					ADD_CASINO_DOOR_TO_SYSTEM_AND_LOCK(CED_CENTRAL_LEFT_DOOR, TRUE)
					ADD_CASINO_DOOR_TO_SYSTEM_AND_LOCK(CED_CENTRAL_RIGHT_DOOR, TRUE)
					ADD_CASINO_DOOR_TO_SYSTEM_AND_LOCK(CED_RIGHT_SIDE_LEFT_DOOR, TRUE)
					ADD_CASINO_DOOR_TO_SYSTEM_AND_LOCK(CED_RIGHT_SIDE_RIGHT_DOOR, TRUE)				
					SET_BIT(iLocalBoolCheck31, LBOOL31_LOCK_CASINO_DOORS)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_ENABLE_DESTROYED_CASINO_IPL_DOORS)
			PRINTLN("[RCC MISSION] PROCESS_INTERIORS_EVERY_FRAME - ci_CSBS2_UseDamagedCasinoDoorsIPL is SET, calling TOGGLE_HIDE_CASINO_FRONT_DOOR_IPL(TRUE)")
			TOGGLE_HIDE_CASINO_FRONT_DOOR_IPL(TRUE)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_CUSTOM_MISSION_MOC)
			PROCESS_MISSION_MOC_INTERIOR()			
		ENDIF
		
		// FIB Office
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FIB_OFFICE)
			INT i
			FIB_OFFICE_PROP_STRUCT sPropDetails
			FOR i = 0 TO ciFIB_OFFICE_NUM_PROPS - 1
				IF NOT DOES_ENTITY_EXIST(oiFIBOfficeProps[i])
					REQUEST_MODEL(PROP_CONST_FENCE02B)
					IF HAS_MODEL_LOADED(PROP_CONST_FENCE02B)
						GET_FIB_OFFICE_PROP_DETAILS(sPropDetails, i)
						oiFIBOfficeProps[i] = CREATE_OBJECT_NO_OFFSET(PROP_CONST_FENCE02B, sPropDetails.vPosition, FALSE, FALSE)
						SET_ENTITY_ROTATION(oiFIBOfficeProps[i], sPropDetails.vRotation)
						FREEZE_ENTITY_POSITION(oiFIBOfficeProps[i], TRUE)
						SET_ENTITY_INVINCIBLE(oiFIBOfficeProps[i], TRUE)
						PRINTLN("[RCC MISSION] - PROCESS_INTERIORS_EVERY_FRAME - FIB Office - Created fence prop @ coords: ", sPropDetails.vPosition)
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
	ENDIF
		
	PROCESS_CASINO_APARTMENT()
	
	PROCESS_FORCED_MINIMAP_INTERIOR()
	
	CREATE_FMMC_MUSIC_LOCKER_LIGHT_PROPS(iInteriorIndexMusicLocker)
ENDPROC

FUNC VECTOR GET_ISLAND_AIR_DEFENCE_TURRET_COORDS(INT iTurretIndex)
	SWITCH iTurretIndex
		CASE 0		RETURN <<4907.4, -5833.0, 28.2>>
		CASE 1		RETURN <<5105.3, -5522.6, 54.6>>
		CASE 2		RETURN <<5588.4312, -5221.2114, 13.3218>>
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC BOOL SHOULD_ISLAND_AIR_DEFENCE_TURRETS_PLAY_DISABLED_SOUND()
	RETURN FMMC_IS_LONG_BIT_SET(gMC_PlayerBD_VARS.iPrerequisiteBS, gMC_PlayerBD_VARS.iAbilityPrerequisite_ReconDrone)
ENDFUNC

PROC PROCESS_ISLAND_IPL_AIR_DEFENCE_TURRET(INT iTurretIndex)
	
	IF g_bMissionEnding
		IF iIslandAirDefenceTurretAudioID[iTurretIndex] != -1
			PRINTLN("[IPLS][IslandIPL] PROCESS_ISLAND_IPL_AIR_DEFENCE_TURRET | Turret ", iTurretIndex, " | Stopping air defence turret sound because the mission is ending")
			STOP_SOUND(iIslandAirDefenceTurretAudioID[iTurretIndex])
			RELEASE_SOUND_ID(iIslandAirDefenceTurretAudioID[iTurretIndex])
			iIslandAirDefenceTurretAudioID[iTurretIndex] = -1
		ENDIF
		
		EXIT
	ENDIF
		
	VECTOR vTurretCoords = GET_ISLAND_AIR_DEFENCE_TURRET_COORDS(iTurretIndex)
	
	IF iIslandAirDefenceTurretAudioID[iTurretIndex] = -1
		iIslandAirDefenceTurretAudioID[iTurretIndex] = GET_SOUND_ID()
		
		IF SHOULD_ISLAND_AIR_DEFENCE_TURRETS_PLAY_DISABLED_SOUND()
			PRINTLN("[IPLS][IslandIPL] PROCESS_ISLAND_IPL_AIR_DEFENCE_TURRET | Turret ", iTurretIndex, " | Starting air defence turret sound now (Disabled)")
			PLAY_SOUND_FROM_COORD(iIslandAirDefenceTurretAudioID[iTurretIndex], "Disabled", vTurretCoords, "DLC_H4_Island_Defences_Soundset", DEFAULT, DEFAULT, TRUE)
			SET_BIT(iIslandAirDefenceTurretPlayingDisabledSoundBS, iTurretIndex)
		ELSE
			PRINTLN("[IPLS][IslandIPL] PROCESS_ISLAND_IPL_AIR_DEFENCE_TURRET | Turret ", iTurretIndex, " | Starting air defence turret sound now (Active)")
			PLAY_SOUND_FROM_COORD(iIslandAirDefenceTurretAudioID[iTurretIndex], "Active", vTurretCoords, "DLC_H4_Island_Defences_Soundset", DEFAULT, DEFAULT, TRUE)
		ENDIF
	ELSE
		IF SHOULD_ISLAND_AIR_DEFENCE_TURRETS_PLAY_DISABLED_SOUND()
		AND NOT IS_BIT_SET(iIslandAirDefenceTurretPlayingDisabledSoundBS, iTurretIndex)
			PRINTLN("[IPLS][IslandIPL] PROCESS_ISLAND_IPL_AIR_DEFENCE_TURRET | Turret ", iTurretIndex, " | Stopping air defence turret sound now to swap to playing inactive sound instead")
			STOP_SOUND(iIslandAirDefenceTurretAudioID[iTurretIndex])
			RELEASE_SOUND_ID(iIslandAirDefenceTurretAudioID[iTurretIndex])
			iIslandAirDefenceTurretAudioID[iTurretIndex] = -1
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_ISLAND_IPL_EVERY_FRAME()
	IF ARE_LIGHTS_TURNED_ON()
		IF IS_BIT_SET(iLocalBoolCheck34, LBOOL34_ISLAND_LIGHTS_HAVE_TURNED_OFF)
			PRINTLN("[IPLS][IslandIPL] PROCESS_ISLAND_IPL_EVERY_FRAME | Lights are back on now, so clearing LBOOL34_ISLAND_LIGHTS_HAVE_TURNED_OFF and performing other lights-on processes")
			CLEAR_BIT(iLocalBoolCheck34, LBOOL34_ISLAND_LIGHTS_HAVE_TURNED_OFF)
			
			// Main Gate
			PLAY_SOUND_FROM_COORD(-1, "PowerUp", <<4981.333496, -5709.093262, 23.831818>>, "DLC_H4_scripted_island_power_sounds")
			
			// Power Station
			PLAY_SOUND_FROM_COORD(-1, "PowerUp", <<4481.995117, -4589.165527, 6.341058>>, "DLC_H4_scripted_island_power_sounds")
		ENDIF
		
	ELSE
		IF NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_ISLAND_LIGHTS_HAVE_TURNED_OFF)
			SET_BIT(iLocalBoolCheck34, LBOOL34_ISLAND_LIGHTS_HAVE_TURNED_OFF)
			PRINTLN("[IPLS][IslandIPL] PROCESS_ISLAND_IPL_EVERY_FRAME | Setting LBOOL34_ISLAND_LIGHTS_HAVE_TURNED_OFF")
		ENDIF
	ENDIF
		
	// Air Defence Turrets
	PROCESS_ISLAND_IPL_AIR_DEFENCE_TURRET(iIslandAirDefenceTurretStaggeredIndex)
	iIslandAirDefenceTurretStaggeredIndex++
	IF iIslandAirDefenceTurretStaggeredIndex >= ciIslandAirDefenceTurrets
		iIslandAirDefenceTurretStaggeredIndex = 0
	ENDIF
ENDPROC

PROC PROCESS_IPLS_EVERY_FRAME()
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_ISLAND)
		PROCESS_ISLAND_IPL_EVERY_FRAME()
	ENDIF
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Alarms and World Speakers  ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Handles Alarms and World Speakers.  ------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC OBJECT_INDEX GET_CLOSEST_SPEAKER_OF_SPECIFIED_MODEL(MODEL_NAMES eModel = DUMMY_MODEL_FOR_SCRIPT)	
	RETURN GET_CLOSEST_OBJECT_OF_TYPE(vLocalPlayerPosition, cfFindSpeakerDistance, eModel, FALSE)
ENDFUNC

FUNC OBJECT_INDEX GET_CLOSEST_CAYO_PERICO_SPEAKER(INT iSpeakerTypeToFind = 0)
	SWITCH iSpeakerTypeToFind
		CASE 0		RETURN GET_CLOSEST_SPEAKER_OF_SPECIFIED_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_out_door_speaker")))
		CASE 1		RETURN GET_CLOSEST_SPEAKER_OF_SPECIFIED_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Isl_Speaker_01a")))
	ENDSWITCH
	
	RETURN NULL
ENDFUNC

PROC PROCESS_ISLAND_ALARMS()

	IF g_bMissionEnding	
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			CLEANUP_WORLD_SOUND(1)
			CLEANUP_WORLD_SOUND(2)
			CLEANUP_WORLD_SOUND(3)
		ENDIF
		
		EXIT
	ENDIF

	SET_BIT(iWorldAlarmTypeBitset, ciWORLD_ALARM_TYPE_CUTSCENE_ISLAND_HEIST_ONE_SHOT)
	SET_BIT(iWorldAlarmTypeBitset, ciWORLD_ALARM_TYPE_CUTSCENE_ISLAND_HEIST_GATE)
	
	IF NOT DOES_ENTITY_EXIST(oiCachedCayoPericoSpeakerToUse)
	OR VDIST2(GET_ENTITY_COORDS(LocalPlayerPed, FALSE), vWorldAlarmPos_1) >= POW(cfFindSpeakerDistance * 0.5, 2.0) // Quite far from cached speaker now - find a new one if possible
		INT iFrameCount = GET_FRAME_COUNT()
		
		IF iFrameCount % 5 = 0
			oiCachedCayoPericoSpeakerToUse = GET_CLOSEST_CAYO_PERICO_SPEAKER(0)
		ELIF iFrameCount % 8 = 0
			oiCachedCayoPericoSpeakerToUse = GET_CLOSEST_CAYO_PERICO_SPEAKER(1)
		ENDIF
		
		// Update our alarm pos to the newly-selected speaker object if there is one
		IF DOES_ENTITY_EXIST(oiCachedCayoPericoSpeakerToUse)	
			vWorldAlarmPos_1 = GET_ENTITY_COORDS(oiCachedCayoPericoSpeakerToUse, FALSE)
		ENDIF
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_H4_Island_Finale_Stealth_Scene")
		PRINTLN("[ALARM] - PROCESS_ISLAND_ALARMS - STOP_AUDIO_SCENE: DLC_H4_Island_Finale_Stealth_Scene")
		STOP_AUDIO_SCENE("DLC_H4_Island_Finale_Stealth_Scene")
	ENDIF
	
	IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_H4_Island_Finale_Action_Scene")
		START_AUDIO_SCENE("DLC_H4_Island_Finale_Action_Scene")
		PRINTLN("[ALARM] - PROCESS_ISLAND_ALARMS - START_AUDIO_SCENE: DLC_H4_Island_Finale_Action_Scene")
	ENDIF
	
	
	IF NOT IS_VECTOR_ZERO(vWorldAlarmPos_1)
		IF IS_ANY_POSITIONAL_SPEECH_PLAYING()
			IF (iWorldSoundID_1 != -1 AND NOT HAS_SOUND_FINISHED(iWorldSoundID_1))				
				IF NOT HAS_SOUND_FINISHED(iWorldSoundID_1)
					PRINTLN("[ALARM] - PROCESS_WORLD_ALARMS - PROCESS_ISLAND_ALARMS - Stopping Alarm_Oneshot")
					STOP_SOUND(iWorldSoundID_1)
				ENDIF
				iWorldSoundID_1 = -1				
			ENDIF
		ELSE
			IF iWorldSoundID_1 = -1
			OR HAS_SOUND_FINISHED(iWorldSoundID_1)
				PRINTLN("[ALARM] - PROCESS_WORLD_ALARMS - PROCESS_ISLAND_ALARMS - Playing Alarm_Oneshot from vWorldAlarmPos_1: ", vWorldAlarmPos_1)
				
				IF iWorldSoundID_1 = -1
					iWorldSoundID_1 = GET_SOUND_ID()
				ENDIF
				PLAY_SOUND_FROM_COORD(iWorldSoundID_1, "Alarm_Oneshot" , vWorldAlarmPos_1, "DLC_H4_Island_Alarms_Sounds")
			ENDIF
		ENDIF
	ENDIF
	
	// Vault Interior Gate Alarm
	VECTOR vCoord = <<5008.1, -5754, 17.6>>		
	IF iWorldSoundID_2 = -1
	OR HAS_SOUND_FINISHED(iWorldSoundID_2)
		IF iWorldSoundID_2 = -1
			iWorldSoundID_2 = GET_SOUND_ID()
		ENDIF
		PRINTLN("[ALARM] - PROCESS_WORLD_ALARMS - PROCESS_ISLAND_ALARMS - (vault) Playing Alarm_Interior_Gate_Loop from vCoord: ", vCoord)
		PLAY_SOUND_FROM_COORD(iWorldSoundID_2, "Alarm_Interior_Gate_Loop", vCoord, "DLC_H4_Island_Alarms_Sounds")
	ENDIF
	
	// Tripped alarm.
	IF NOT IS_VECTOR_ZERO(vWorldAlarmPos_2)
		IF iWorldSoundID_3 = -1
		OR HAS_SOUND_FINISHED(iWorldSoundID_3)
			IF iWorldSoundID_3 = -1
				iWorldSoundID_3 = GET_SOUND_ID()
			ENDIF
			PRINTLN("[ALARM] - PROCESS_WORLD_ALARMS - PROCESS_ISLAND_ALARMS - (tripped alarm) Playing Alarm_Interior_Gate_Loop from vWorldAlarmPos_2: ", vWorldAlarmPos_2)
			PLAY_SOUND_FROM_COORD(iWorldSoundID_3, "Alarm_Interior_Gate_Loop", vWorldAlarmPos_2, "DLC_H4_Island_Alarms_Sounds")
		ENDIF
	ENDIF	
ENDPROC

PROC PROCESS_PRISON_ALARMS()

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciTURN_OFF_PRISON_YARD_AMB)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciTRIGGER_ALARMS_ON_WANTED)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck4, LBOOL4_TRIGGER_ALARMS)
		EXIT
	ENDIF
	
	IF IS_ALARM_PLAYING("PRISON_ALARMS")
		EXIT
	ENDIF
	
	IF NOT PREPARE_ALARM("PRISON_ALARMS")
		EXIT
	ENDIF
	
	PRINTLN("[ALARM] PROCESS_PRISON_ALARMS - Starting Prison Alarm.")
	
	START_ALARM("PRISON_ALARMS", FALSE)
	SET_AMBIENT_ZONE_STATE("AZ_COUNTRYSIDE_PRISON_01_ANNOUNCER_GENERAL", FALSE, TRUE)
	SET_AMBIENT_ZONE_STATE("AZ_COUNTRYSIDE_PRISON_01_ANNOUNCER_ALARM", TRUE, TRUE)
	
ENDPROC

PROC PROCESS_UNION_DEPOSITORY_ALARMS()

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_UNION_DEPOSITORY)
		EXIT
	ENDIF
	
	IF IS_ALARM_PLAYING("BIG_SCORE_HEIST_VAULT_ALARMS")
		EXIT
	ENDIF
	
	IF NOT PREPARE_ALARM("BIG_SCORE_HEIST_VAULT_ALARMS")
		EXIT
	ENDIF
	
	IF MC_playerBD[iPartToUse].iWanted > 0
	OR MC_playerBD[iPartToUse].iFakeWanted > 0
	OR HAS_ANY_TEAM_TRIGGERED_AGGRO(ciMISSION_AGGRO_INDEX_DEFAULT)
		PRINTLN("[ALARM] PROCESS_UNION_DEPOSITORY_ALARMS - Starting Union Depository Alarm || iWanted: ", MC_playerBD[iPartToUse].iWanted, " / iFakeWanted: ", MC_playerBD[iPartToUse].iFakeWanted)
		START_ALARM("BIG_SCORE_HEIST_VAULT_ALARMS", FALSE)
	ENDIF
	
ENDPROC

PROC PROCESS_WORLD_ALARMS()
	
	PROCESS_PRISON_ALARMS()
	
	PROCESS_UNION_DEPOSITORY_ALARMS()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_TriggerMissionAlarmOnAggro)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_TriggerMissionAlarmAtStart)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_TriggerMissionAlarmAtStart)
		IF NOT HAS_ANY_TEAM_TRIGGERED_AGGRO(g_FMMC_STRUCT.iAggroIndexBS_iStartMissionWithAggro)
			EXIT
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_WorldAlarmAssetIsland)
		PRINTLN("[ALARM] - PROCESS_WORLD_ALARMS - PROCESS_ISLAND_ALARMS - Processing Alarms.")
		PROCESS_ISLAND_ALARMS()
	ENDIF
	
ENDPROC

///PURPOSE: Adds areas where the water is calmed, so the waves don't clip through IPL things on water (like the yacht / carrier).
///    Creator-placed water calming quad zones are handled elsewhere (in the function CREATE_ZONES() in the 
///    FM_Mission_Controller_Zones header).
PROC PROCESS_WATER_CALMING_QUADS_FOR_IPLS()
	
	INT iQuad = -1
	
	IF g_FMMC_STRUCT.iIPLOptions > 0
		
		/*IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,IPL_CARRIER)
		AND NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_WATER_CALMING_QUAD_CARRIER_CREATED)
			
			iQuad = GET_FREE_WATER_CALMING_QUAD()
			
			IF iQuad > -1
			AND iQuad < FMMC_MAX_WATER_CALMING_QUADS
			AND iWaterCalmingQuad[iQuad] = -1
				iWaterCalmingQuad[iQuad] = ADD_EXTRA_CALMING_QUAD(3000,-4837,3120,-4508,0.02)
				PRINTLN("[RCC MISSION] PROCESS_WATER_CALMING_QUADS - Adding calming quad to Carrier, calming quad index ",iQuad)
			ELSE
				CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] PROCESS_WATER_CALMING_QUADS, Carrier - Trying to create too many water dampening zones! There is a maximum of 8.")
				PRINTLN("[RCC MISSION] PROCESS_WATER_CALMING_QUADS, Carrier - Trying to create too many water dampening zones! There is a maximum of 8.")
			ENDIF
			
			SET_BIT(iLocalBoolCheck7, LBOOL7_WATER_CALMING_QUAD_CARRIER_CREATED)
		ENDIF*/
		
		IF ( IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,IPL_YACHT_CHUMASH)
			 OR IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,IPL_YACHT_NEAR_PIER) 
			 OR IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,IPL_YACHT_PALETO))
		AND NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_WATER_CALMING_QUAD_YACHT_CREATED)
			
			iQuad = GET_FREE_WATER_CALMING_QUAD()
			
			IF iQuad > -1
			AND iQuad < FMMC_MAX_WATER_CALMING_QUADS
			AND iWaterCalmingQuad[iQuad] = -1
				iWaterCalmingQuad[iQuad] = ADD_EXTRA_CALMING_QUAD(-2125,-1049,-2013,1002,0.02)
				PRINTLN("[RCC MISSION] PROCESS_WATER_CALMING_QUADS - Adding calming quad to Yacht, calming quad index ",iQuad)
			ELSE
				CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] PROCESS_WATER_CALMING_QUADS, Yacht - Trying to create too many water dampening zones! There is a maximum of 8.")
				PRINTLN("[RCC MISSION] PROCESS_WATER_CALMING_QUADS, Yacht - Trying to create too many water dampening zones! There is a maximum of 8.")
			ENDIF
			
			SET_BIT(iLocalBoolCheck7, LBOOL7_WATER_CALMING_QUAD_YACHT_CREATED)
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROCESS_DISPLACED_INTERIORS()
	INT iInteriorHash = -1
	VECTOR vInteriorPos
	VECTOR vDisplacePos
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_WEED_FARM)
		iInteriorHash = HASH("bkr_biker_dlc_int_ware02")
		vInteriorPos = <<1049.6, -3196.6, -38.5>>
		PROCESS_INTERIOR_MINIMAP(iInteriorHash, vInteriorPos, 10, -5)
		PROCESS_INTERIOR_DISPLACEMENT(iInteriorHash, vInteriorPos, <<996.97, -2546.14, -38.5>>)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_ALT_WEED_FARM)
		iInteriorHash = HASH("sf_dlc_warehouse_sec")
		vInteriorPos = <<2920.0, 4470.0, -100.0>>
		PROCESS_INTERIOR_MINIMAP(iInteriorHash, vInteriorPos)
		//PROCESS_INTERIOR_DISPLACEMENT(iInteriorHash, vInteriorPos, <<996.97, -2546.14, -38.5>>)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_COCAINE_FACTORY)
		iInteriorHash = HASH("bkr_biker_dlc_int_ware03")
		vInteriorPos = <<1093.6, -3196.6, -38.5>>
		PROCESS_INTERIOR_MINIMAP(iInteriorHash, vInteriorPos)
		PROCESS_INTERIOR_DISPLACEMENT(iInteriorHash, vInteriorPos, <<-1211.71, -1070.31, -38.5>>)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_METH_FACTORY)
		iInteriorHash = HASH("bkr_biker_dlc_int_ware01")
		vInteriorPos = <<1009.5, -3196.6, -38.5>>
		PROCESS_INTERIOR_MINIMAP(iInteriorHash, vInteriorPos)
		PROCESS_INTERIOR_DISPLACEMENT(iInteriorHash, vInteriorPos, <<359.85, 357.91, -38.5>>)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SPECIAL_CARGO_WAREHOUSE_L)
		iInteriorHash = HASH("ex_int_warehouse_l_dlc")
		vInteriorPos = <<1010.0083, -3100.0000, -39.9999>>
		PROCESS_INTERIOR_MINIMAP(iInteriorHash, vInteriorPos)
		PROCESS_INTERIOR_DISPLACEMENT(iInteriorHash, vInteriorPos, <<1186.61, -1394.78, -39.9>>)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_METH_FACTORY_MURRIETA)
		iInteriorHash = HASH("tr_Tuner_MethLab_1")
		vInteriorPos = <<1210.0, 1857.0, -50.0>>
		PROCESS_INTERIOR_MINIMAP(HASH("bkr_biker_dlc_int_ware01"), vInteriorPos, DEFAULT, DEFAULT, TRUE)
		PROCESS_INTERIOR_DISPLACEMENT(iInteriorHash, vInteriorPos, vInteriorPos)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_METH_FACTORY_EAST_VINEWOOD)
		iInteriorHash = HASH("tr_Tuner_MethLab_2")
		vInteriorPos = <<1569.0, -2130.0, -50.0>>
		PROCESS_INTERIOR_MINIMAP(HASH("bkr_biker_dlc_int_ware01"), vInteriorPos, DEFAULT, DEFAULT, TRUE)
		PROCESS_INTERIOR_DISPLACEMENT(iInteriorHash, vInteriorPos, vInteriorPos)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_METH_FACTORY_SENORA_DESERT_1)
		iInteriorHash = HASH("tr_Tuner_MethLab_3")
		vInteriorPos = <<839.0, 2176.0, -50.0>>
		PROCESS_INTERIOR_MINIMAP(HASH("bkr_biker_dlc_int_ware01"), vInteriorPos, DEFAULT, DEFAULT, TRUE)
		PROCESS_INTERIOR_DISPLACEMENT(iInteriorHash, vInteriorPos, vInteriorPos)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_METH_FACTORY_SENORA_DESERT_2)
		iInteriorHash = HASH("tr_Tuner_MethLab_4")
		vInteriorPos = <<982.0, -143.0, -50.0>>
		PROCESS_INTERIOR_MINIMAP(HASH("bkr_biker_dlc_int_ware01"), vInteriorPos, DEFAULT, DEFAULT, TRUE)
		PROCESS_INTERIOR_DISPLACEMENT(iInteriorHash, vInteriorPos, vInteriorPos)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_IAA)
		vInteriorPos = <<2047.0, 2942.0, -61.9>>
		iInteriorHash = HASH("xm_x17dlc_int_facility")
		PROCESS_INTERIOR_MINIMAP(iInteriorHash, vInteriorPos, DEFAULT, DEFAULT, DEFAULT, TRUE, TRUE)
		PROCESS_INTERIOR_DISPLACEMENT(iInteriorHash, vInteriorPos, GET_IAA_BASE_LOCATION())
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_BUNKER)
		vInteriorPos = <<938.307678, -3196.111572, -100.000000>>
		iInteriorHash = HASH("gr_grdlc_int_02")
		PROCESS_INTERIOR_MINIMAP(iInteriorHash, vInteriorPos, DEFAULT, DEFAULT, DEFAULT, TRUE, TRUE)
		PROCESS_INTERIOR_DISPLACEMENT(iInteriorHash, vInteriorPos, GET_BUNKER_LOCATION())
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_CUSTOM_MISSION_MOC)
		vInteriorPos = <<1103.562378, -3000.0, -40.0>>
		iInteriorHash = HASH("gr_grdlc_int_01")
		PROCESS_INTERIOR_MINIMAP(iInteriorHash, vInteriorPos, DEFAULT, DEFAULT, TRUE)
		PROCESS_INTERIOR_DISPLACEMENT(iInteriorHash, vInteriorPos, GET_MOC_INTERIOR_LOCATION())
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_APARTMENT)
		vInteriorPos = <<976.636414, 70.294762, 115.164131>>
		IF GET_INTERIOR_FROM_ENTITY(PlayerPedToUse) = GET_INTERIOR_AT_COORDS(vInteriorPos)
			SET_GLOBAL_FLAG_BIT(eGLOBALFLAGBITSET_USING_CASINO_MISSION_INTERIOR)
		ELSE
			CLEAR_GLOBAL_FLAG_BIT(eGLOBALFLAGBITSET_USING_CASINO_MISSION_INTERIOR)
		ENDIF
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_RECORDING_STUDIO)
		vInteriorPos = <<-1010, -70, -99.4>>
		iInteriorHash = HASH("sf_dlc_studio_sec")
		vDisplacePos = <<-833.41, -228.85, 0.0>>
		PROCESS_INTERIOR_MINIMAP(iInteriorHash, vInteriorPos, DEFAULT, DEFAULT, TRUE)
		PROCESS_INTERIOR_DISPLACEMENT(iInteriorHash, vInteriorPos, vDisplacePos)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_CUSTOM_HIGH_END_APARTMENT)
		iInteriorHash = HASH("apa_v_mp_h_04")
		vInteriorPos = <<-773.2258, 322.8252, 194.8862>>
		vDisplacePos = <<-1243.8, -850.5, 0.0>>
		PROCESS_INTERIOR_DISPLACEMENT(iInteriorHash, vInteriorPos, vDisplacePos)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_ARENA_WAR_GARAGE_LEVEL_1)
		iInteriorHash = HASH("xs_x18_int_mod2")
		vInteriorPos = <<170.0, 5190.0, 10.0>>
		vDisplacePos = <<857.0, -2395.0, 0.0>>
		PROCESS_INTERIOR_DISPLACEMENT(iInteriorHash, vInteriorPos, vDisplacePos)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR_MEDIUM_GARAGE)
		iInteriorHash = HASH("v_garagem")
		vInteriorPos = <<199.9715, -999.6678, -100.0000>>
		vDisplacePos = <<152.7, -114.5, 0.0>>
		PROCESS_INTERIOR_MINIMAP(iInteriorHash, vInteriorPos, DEFAULT, DEFAULT, TRUE)
		PROCESS_INTERIOR_DISPLACEMENT(iInteriorHash, vInteriorPos, vDisplacePos)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MOTEL)
		iInteriorHash = HASH("v_motel_mp")
		vInteriorPos = <<152.3, -1004.4, -97.8>>
		vDisplacePos = <<546.9, -1769.3, 0.0>>
		PROCESS_INTERIOR_MINIMAP(iInteriorHash, vInteriorPos, DEFAULT, DEFAULT, TRUE)
		PROCESS_INTERIOR_DISPLACEMENT(iInteriorHash, vInteriorPos, vDisplacePos)
	ENDIF
	IF IS_MISSION_BASEMENT_INTERIOR_ENABLED()
		iInteriorHash = HASH("reh_dlc_int_04_sum2")
		vInteriorPos = <<850.0, -3000.0, -50.0>>
		//vDisplacePos = <<546.9, -1769.3, 0.0>>
		PROCESS_INTERIOR_MINIMAP(iInteriorHash, vInteriorPos, DEFAULT, DEFAULT, TRUE, TRUE, DEFAULT, TRUE)
		//PROCESS_INTERIOR_DISPLACEMENT(iInteriorHash, vInteriorPos, vDisplacePos)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_PHARMA_OFFICE)
		iInteriorHash = HASH("xm3_DLC_INT_04_xm3")
		vInteriorPos = <<495, -2560, -50.0>>
		//vDisplacePos = <<546.9, -1769.3, 0.0>>
		PROCESS_INTERIOR_MINIMAP(iInteriorHash, vInteriorPos)
		//PROCESS_INTERIOR_DISPLACEMENT(iInteriorHash, vInteriorPos, vDisplacePos)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_AUTOCARE_WAREHOUSE)
		iInteriorHash = HASH("xm3_DLC_INT_03_xm3")
		vInteriorPos = <<584, -2605, -50.0>>
		//vDisplacePos = <<546.9, -1769.3, 0.0>>
		PROCESS_INTERIOR_MINIMAP(iInteriorHash, vInteriorPos)
		//PROCESS_INTERIOR_DISPLACEMENT(iInteriorHash, vInteriorPos, vDisplacePos)
	ENDIF
ENDPROC

PROC PROCESS_IPL_MAP_DISPLAY()
	
	//IPL Maps:	
	IF g_FMMC_STRUCT.iIPLOptions > 0
	AND IS_ENTITY_ALIVE(PlayerPedToUse)
		VECTOR vPlayer = GET_ENTITY_COORDS(PlayerPedToUse)
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,IPL_CARRIER)
			PROCESS_IPL_CARRIER_MAP(vPlayer)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,IPL_YACHT_CHUMASH)
		OR IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,IPL_YACHT_NEAR_PIER)
		OR IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,IPL_YACHT_PALETO)
			PROCESS_IPL_YACHT_NEAR_PIER_MAP(vPlayer)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_HANGAR)
		IF LocalPlayerCurrentInterior = GET_INTERIOR_AT_COORDS(vHangarInteriorCoords) // If in the hangar 
			PROCESS_HANGAR_MINIMAP()
			IF NOT bIsDisplacedInteriorSet
				bIsDisplacedInteriorSet = TRUE	
				SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(638945734, <<-1174.49,-3457.17,-50>>)
			ENDIF
		ELSE
			IF bIsDisplacedInteriorSet
				bIsDisplacedInteriorSet = FALSE
				CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA()
			ENDIF
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SERVER_FARM)
		IF LocalPlayerCurrentInterior = GET_INTERIOR_AT_COORDS(<< 2168.09, 2920.89, -85.8005 >>) // If in the Server Farm
			IF NOT bIsDisplacedInteriorSet
				bIsDisplacedInteriorSet = TRUE
				SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(1979383629,<< 2483.0, -404.2, -85.8005 >>)
			ENDIF
			PROCESS_SERVER_FARM_MINIMAP()
		ELSE 
			IF bIsDisplacedInteriorSet
				bIsDisplacedInteriorSet = FALSE
				CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA()
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_ARENA)
		IF LocalPlayerCurrentInterior = g_ArenaInterior
		OR LocalPlayerCurrentInterior = iArenaInterior_VIPLoungeIndex
		
			IF NOT bIsDisplacedInteriorSet
				bIsDisplacedInteriorSet = TRUE
				SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(NATIVE_TO_INT(g_ArenaInterior),<<ARENA_X, ARENA_Y, ARENA_Z>>)
			ENDIF
			PROCESS_DISPLACEMENT_ARENA_INTERIOR_MC()
		ELSE
			IF bIsDisplacedInteriorSet
				bIsDisplacedInteriorSet = FALSE
				CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA()
			ENDIF
		ENDIF
	ENDIF
	
	IF (IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_01)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_02)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_03)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_TUNNEL)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_LOOP)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_ENTRANCE)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_BASE))
		IF IS_VALID_INTERIOR(LocalPlayerCurrentInterior)
		AND (LocalPlayerCurrentInterior = GET_INTERIOR_AT_COORDS(<<244.252, 6163.91, -161.423>>)// Silo Lab  -635400705 
		 OR LocalPlayerCurrentInterior = GET_INTERIOR_AT_COORDS(<<446.172, 5922.13, -157.216>>) // Silo -222705970
		 OR LocalPlayerCurrentInterior = GET_INTERIOR_AT_COORDS(<<252.062, 5972.12, -159.102>>) // Silo 1704012289 
		 OR LocalPlayerCurrentInterior = GET_INTERIOR_AT_COORDS(<<550.948, 5939.26, -157.216>>)  // Silo 1914093948
		 OR LocalPlayerCurrentInterior = GET_INTERIOR_AT_COORDS(<<361.0, 6306.0, -159.0>>)) // Silo 01
			PROCESS_SILO_MINIMAP(vLocalPlayerCurrentInteriorPos, iLocalPlayerCurrentInteriorHash)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FOUNDRY)
	AND LocalPlayerCurrentInterior = GET_INTERIOR_AT_COORDS(<<1087.14, -1986.68, 31.2089>>) // If in the foundry
		PROCESS_FOUNDRY_MINIMAP()
		
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FOUNDRY)
		SET_RADAR_ZOOM_PRECISE(0)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SUB)
	AND LocalPlayerCurrentInterior = GET_INTERIOR_AT_COORDS(<< 512.769, 4851.98, -62.9025 >>) //If in the submarine
		PROCESS_SUBMARINE_MINIMAP()
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_IMPORT_WAREHOUSE)
	AND LocalPlayerCurrentInterior = GET_INTERIOR_AT_COORDS(<<974.9203, -3000.0647, -40.6470>>) //If in the docks import warehouse
		PROCESS_IMPORT_WAREHOUSE()
		
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_WAREHOUSE_UNDRGRND_FACILITY)
	AND LocalPlayerCurrentInterior = GET_INTERIOR_AT_COORDS(<<969.5376, -3000.4111, -48.6470>>) //If in the docks import warehouse
		PROCESS_WAREHOUSE_UNDRGRND_FACILITY()
	ENDIF
	
	PROCESS_DISPLACED_INTERIORS()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_CAR_PARK)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_APARTMENT)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_GARAGE)
		PROCESS_CASINO_BLIPS()
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_MAIN)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_TUNNEL)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_BACK_AREA)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_HOTEL_FLOOR)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_LOADING_BAY)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_VAULT)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_UTILITY_LIFT)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_LIFT_SHAFT)
		PROCESS_CASINO_HEIST_MINIMAP()
		PROCESS_CASINO_ROOF_MINIMAP()
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MUSIC_LOCKER)
		PROCESS_MUSIC_LOCKER_MINIMAP()
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FIB_OFFICE)
		PROCESS_FIB_OFFICE_MINIMAP()
	ENDIF
		
ENDPROC

PROC PROCESS_RADAR_ZOOM()
	
	IF GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) != NULL
		IF IS_BIT_SET(iLocalBoolCheck34, LBOOL34_MAP_ZOOM_CHANGED)
			SET_RADAR_ZOOM_PRECISE(0)
			CLEAR_BIT(iLocalBoolCheck34, LBOOL34_MAP_ZOOM_CHANGED)
		ENDIF
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.fRadarZoomAmount != 0
		SET_RADAR_ZOOM_PRECISE(g_FMMC_STRUCT.fRadarZoomAmount)
		SET_BIT(iLocalBoolCheck34, LBOOL34_MAP_ZOOM_CHANGED)
	ENDIF
	
ENDPROC

PROC PROCESS_MAP_AND_RADAR()
	
	PROCESS_IPL_MAP_DISPLAY()
	
	PROCESS_WATER_CALMING_QUADS_FOR_IPLS()
	
	PROCESS_RADAR_ZOOM()
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE_HIDE_RADAR)
		
		IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_HIDE_RADAR)
			DISPLAY_RADAR(FALSE)
			SET_BIT(iLocalBoolCheck6, LBOOL6_HIDE_RADAR)
		ENDIF
		
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_HIDE_RADAR)
			DISPLAY_RADAR(TRUE)
			CLEAR_BIT(iLocalBoolCheck6, LBOOL6_HIDE_RADAR)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck28, LBBOOL28_HIDE_RADAR_THIS_FRAME)
		IF iRadarHiddenFrame >= GET_FRAME_COUNT()
			DISPLAY_RADAR(FALSE)
		ELSE
			CLEAR_BIT(iLocalBoolCheck28, LBBOOL28_HIDE_RADAR_THIS_FRAME)
			DISPLAY_RADAR(TRUE)
		ENDIF
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: IPL Functionality  ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Handles various IPL functionality.  ------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC BOOL HAVE_ALL_PLAYERS_LOADED_FMMC_YACHT()
	
	INT iNumberOfPlayers = (MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]) 
	IF iNumberOfPlayers = 0
		PRINTLN("HAVE_ALL_PLAYERS_LOADED_FMMC_YACHT - iNumberOfPlayers is 0, need to wait")
		RETURN FALSE
	ENDIF	
	
	INT iPlayersReady = 0
	INT iPart = -1
	WHILE DO_PARTICIPANT_LOOP(iPart)
		IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet4, PBBOOL4_FMMC_YACHTS_READY)
			iPlayersReady++
			PRINTLN("HAVE_ALL_PLAYERS_LOADED_FMMC_YACHT - Participant ", iPart, " says they're ready")
			PRINTLN("HAVE_ALL_PLAYERS_LOADED_FMMC_YACHT - ", iPlayersReady, " out of ", iNumberOfPlayers, " participants are ready to go")
		ELSE
			PRINTLN("HAVE_ALL_PLAYERS_LOADED_FMMC_YACHT - Participant ", iPart, " says they're NOT ready")
		ENDIF
	ENDWHILE
	
	PRINTLN("HAVE_ALL_PLAYERS_LOADED_FMMC_YACHT - iPlayersReady: ", iPlayersReady, " / iNumberOfPlayers: ", iNumberOfPlayers)
	
	RETURN iPlayersReady >= iNumberOfPlayers
ENDFUNC

PROC UPDATE_FMMC_YACHT_WARPING(INT &iScene, SceneYachtFunc customYachtCutscene)
	
	INT iYachtIndex = 0
	
	INT iPlayer = NATIVE_TO_INT(LocalPlayer)
	INT iYachtID
	STRING AnimDictName = "ANIM@MP_YACHT@YACHT_TRANS@"
	STRING outboundAnimName = "OUTBOUND_CAM", inboundAnimName = "INBOUND_CAM"
	STRING outboundSoundName = "Leave_R_L", inboundSoundName = "Arrive_R_L"
	VECTOR outboundSceneOffset = <<-0.030, 57.720, -0.860>>
	VECTOR inboundSceneOffset = outboundSceneOffset
	
	DRAW_DEBUG_TEXT_2D("UPDATE_FMMC_YACHT_WARPING", <<0.05, 0.05, 0.5>>)
	
	TEXT_LABEL_63 tlDebugText
	tlDebugText = "iWarpState: "
	tlDebugText += GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState
	DRAW_DEBUG_TEXT_2D(tlDebugText,  <<0.05, 0.11, 0.5>>)
	
	
	CONST_INT iWarp_0	0		// wait for a warp to start
	CONST_INT iWarp_1	1		// load ourbound audio scene
	CONST_INT iWarp_2	2		// play establishing shot
	CONST_INT iWarp_3	3		// play outbound animated camera
	CONST_INT iWarp_4	4		// 
	CONST_INT iWarp_5	5		// 
	CONST_INT iWarp_6	6		// switch off player control and do fade
	CONST_INT iWarp_7	7		// wait for screen to fade out
	CONST_INT iWarp_8	8		// wait for server to assign new location
	CONST_INT iWarp_9	9		// do warp
	CONST_INT iWarp_10	10		// fade out for warp to beach	
	CONST_INT iWarp_96	96		// play inbound animated camera
	CONST_INT iWarp_97	97		// 
	CONST_INT iWarp_98	98		// 
	CONST_INT iWarp_99	99		// restore gameplay / cleanup
	CONST_INT iWarp_100	100		// make sure owner has finished.
	
	// wait for a warp to start
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_0)	
		// store the owner id, only if it is valid.
		iYachtID = GET_FMMC_YACHT_ID(iYachtIndex)

		PRINTLN("[MYACHT] - yacht owner has started yacht warp, Yacht ID is ", iYachtID)
		
		g_SpawnData.iYachtToWarpFrom = iYachtID
		g_SpawnData.bHasAccessToYachtWarp = TRUE
		
		PRINTLN("[MYACHT] - bHasAccessToYachtWarp = ", g_SpawnData.bHasAccessToYachtWarp, ", owner = ", NATIVE_TO_INT(g_SpawnData.WarpOwnerID))
		
		Private_Get_NET_YACHT_SCENE(iYachtID, sMoveScene
			#IF IS_DEBUG_BUILD
			, &GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS
			, &GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING
			#ENDIF
			)
		GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_1														
	ENDIF
	
	// load ourbound audio scene
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_1)
	
		UpdateStoreLastVehicleOnYacht()
		
		IF NOT DOES_ANIM_DICT_EXIST(AnimDictName)
			PRINTLN("[MYACHT] - bypass establishing shot, owner = ", NATIVE_TO_INT(g_SpawnData.WarpOwnerID))
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_5
		ELSE
			REQUEST_ANIM_DICT(AnimDictName)
			CDEBUG1LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - waiting for outbound stream \"", outboundSoundName, "\"")
			IF LOAD_STREAM(outboundSoundName,"DLC_Apartment_Yacht_Streams_Soundset")
			AND REQUEST_SCRIPT_AUDIO_BANK("DLC_APARTMENT/APT_Yacht_01")
				PRINTLN("[MYACHT] - finished loading stream ", outboundSoundName, ", owner = ", NATIVE_TO_INT(g_SpawnData.WarpOwnerID))
				iYachtCutStage = 0
				GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_2
			ENDIF
		ENDIF
	ENDIF
	
	// play establishing shot
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_2)
	
		UpdateStoreLastVehicleOnYacht()
		
		REQUEST_ANIM_DICT(AnimDictName)
		
		INT iPrevCutStage = iYachtCutStage
		IF CALL customYachtCutscene(iYachtCutStage, sMoveScene, cYachtCam1, cYachtCam2, FALSE)
			IF HAS_ANIM_DICT_LOADED(AnimDictName)
				PRINTLN("[MYACHT] - finished playing establishing shot, owner = ", NATIVE_TO_INT(g_SpawnData.WarpOwnerID))
				IF (g_SpawnData.bHasAccessToYachtWarp)
					GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_3
				ELSE
					g_SpawnData.iYachtToWarpTo = g_SpawnData.iYachtToWarpFrom
					GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_10
					NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
					PRINTLN("[MYACHT] - dont have access, so will resapwn on beacht. owener = ", NATIVE_TO_INT(g_SpawnData.WarpOwnerID))
				ENDIF
			ENDIF
		ENDIF
		
		IF iPrevCutStage = 0
			iYachtID = g_SpawnData.iYachtToWarpFrom
			PLAY_STREAM_FROM_POSITION(GET_COORDS_OF_PRIVATE_YACHT(iYachtID))
			START_AUDIO_SCENE("DLC_APT_Yacht_Leave_Scene")
			PLAY_SOUND_FROM_COORD(-1, "Leave_Horn", GET_COORDS_OF_PRIVATE_YACHT(iYachtID), "DLC_Apartment_Yacht_Streams_Soundset")
		ENDIF
	ENDIF
	
	// play outbound animated camera
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_3)
		
		UpdateStoreLastVehicleOnYacht()
		
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpFrom)
		
		NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
		START_MP_CUTSCENE(TRUE)
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
		iYachtID = g_SpawnData.iYachtToWarpFrom
		VECTOR vOutboundSceneCoord = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, outboundSceneOffset)
		iScene = CREATE_SYNCHRONIZED_SCENE(
				vOutboundSceneCoord,
				<<0,0,GET_HEADING_OF_PRIVATE_YACHT(iYachtID)>>)
		SET_SYNCHRONIZED_SCENE_PHASE(iScene, 0.0)
		cYachtCam1 = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		PLAY_SYNCHRONIZED_CAM_ANIM(cYachtCam1, iScene, outboundAnimName, AnimDictName)
		
		PRINTLN("[MYACHT] - start outbound synch scene ", iScene, " camera \"", outboundAnimName, "\", \"", AnimDictName, "\", ", vOutboundSceneCoord)
		GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_4
	ENDIF
	
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_4)
		
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpFrom)
	
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
			IF GET_SYNCHRONIZED_SCENE_PHASE(iScene) <= 0.85
				//
				#IF IS_DEBUG_BUILD
				IF g_bEditSynchSceneOffsets
					iYachtID = g_SpawnData.iYachtToWarpFrom
					SET_SYNCHRONIZED_SCENE_PHASE(iScene, g_vSynchScenePhase)
					SET_SYNCHRONIZED_SCENE_ORIGIN(iScene,
							GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, outboundSceneOffset+g_vSynchSceneCoordOffset),
							<<0,0,GET_HEADING_OF_PRIVATE_YACHT(iYachtID)>>+g_vSynchSceneRotOffset)
					DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, outboundSceneOffset+g_vSynchSceneCoordOffset), 0.125)
				ENDIF
				#ENDIF
			ELSE
				GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_5
			ENDIF
		ELSE
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_5
		ENDIF
	ENDIF
	
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_5)
	
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpFrom)
		
		DO_SCREEN_FADE_OUT(1000)
		PRINTLN("[MYACHT] - finished synch scene ", iScene, " camera, fade out")
		GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_6
	ENDIF
	
	// switch off player control and do fade
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_6)
		
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpFrom)
	
		g_SpawnData.YachtWarpTimer = GET_NETWORK_TIME_ACCURATE()
		NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
		IF NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(500)
		ENDIF
		g_SpawnData.iYachtToWarpTo = g_SpawnData.iYachtToWarpFrom // so it has a valid number set
		GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_7
	ENDIF

	// wait for screen to fade out
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_7)
		
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpFrom)
		
		IF IS_SCREEN_FADED_OUT()		
		
			g_SpawnData.YachtWarpTimer = GET_NETWORK_TIME_ACCURATE()
			PRINTLN("[MYACHT] Moving yacht now!")
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
				DETACH_SYNCHRONIZED_SCENE(iScene)
			ENDIF
			iScene = -1
			
			IF DOES_CAM_EXIST(cYachtCam1)
				DESTROY_CAM(cYachtCam1)
			ENDIF
			IF DOES_CAM_EXIST(cYachtCam2)
				DESTROY_CAM(cYachtCam2)
			ENDIF
			
			// make sure we are clear of any tasks
			STOP_STREAM()
			STOP_AUDIO_SCENE("DLC_APT_Yacht_Leave_Scene")
			CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
			
			IF NETWORK_IS_IN_MP_CUTSCENE()
				NETWORK_SET_IN_MP_CUTSCENE(FALSE)
				PRINTLN("[MYACHT] - not longer setting as network cutscene, so yacht vehicles can get created..")
			ENDIF
			
			LOAD_STREAM(inboundSoundName,"DLC_Apartment_Yacht_Streams_Soundset")			
			
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_8
		ELSE
			IF NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(500)
			ELSE
				CDEBUG1LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - doing fade out.")
			ENDIF		
		ENDIF
	ENDIF
	
	// wait for server to assign new location
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_8)
		SET_FMMC_YACHT_STATE(0, FMMC_YACHT_SPAWN_STATE__INIT)
		DELETE_AND_CLEANUP_ASSETS_FOR_MISSION_CREATOR_YACHT(sMissionYachtData[iYachtIndex])
		
		IF bIsLocalPlayerHost
			SET_BIT(MC_serverBD.iServerBitSet7, SBBOOL7_YACHT_MOVED_TO_DESTINATION)
		ENDIF
		
		GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_9
	ENDIF
	
	// do warp
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_9)
	
		DoLastVehicleYachtWarp()
		
		IF sMissionYachtVars[0].eMissionYachtSpawnState != FMMC_YACHT_SPAWN_STATE__COMPLETE
			PRINTLN("[MYACHT] Waiting for yacht to load!")
			EXIT
		ENDIF
		
		IF NOT HAVE_ALL_PLAYERS_LOADED_FMMC_YACHT()
			PRINTLN("[MYACHT] Waiting for other players")
			EXIT
		ENDIF
		
		VECTOR vCoords = <<1, 1, 1>>
		FLOAT fHeading = 11.9
		
		GetSpawnForYachtExterior(g_SpawnData.iYachtToWarpTo, vCoords, fHeading)
		//vCoords = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(g_SpawnData.iYachtToWarpTo, g_PrivateYachtSpawnLocationOffest[i].vPlayerLoc)
		
		IF NET_WARP_TO_COORD(vCoords, fHeading, FALSE, FALSE)
			PRINTLN("[MYACHT] We've warped to the coords - moving on. || warped to ", vCoords)
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_96
		ELSE
			PRINTLN("[MYACHT] Waitinf ofr warp - warping to ", vCoords)
		ENDIF
	ENDIF
	
	// wait for scereen to fade out
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_10)
		IF IS_SCREEN_FADED_OUT()
			PRINTLN("[MYACHT] - screen is faded out, do warp. ")	
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_9	
		ELSE
			IF NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(500)
			ELSE
				PRINTLN("[MYACHT] - screen is fadeing out... ")
			ENDIF
		ENDIF
	ENDIF
	
	// play inbound animated camera
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_96)
		
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpTo)
	
		IF NOT DOES_ANIM_DICT_EXIST(AnimDictName)
			PRINTLN("[MYACHT] - bypass synch scene camera")
			
			IF NOT IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(1000)
			ENDIF
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_98
		ELSE
			IF NOT LOAD_STREAM(inboundSoundName,"DLC_Apartment_Yacht_Streams_Soundset")
			//OR (MPGlobalsPrivateYacht.bUseYachtObjectsOptimisation AND NOT IS_PRIVATE_YACHT_FULLY_LOADED(g_SpawnData.iYachtToWarpTo))
				CDEBUG1LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - waiting for inbound stream \"", inboundSoundName, "\" to load")
			ELSE
				iYachtID = g_SpawnData.iYachtToWarpTo
				VECTOR vInboundSceneCoord = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, inboundSceneOffset)
				iScene = CREATE_SYNCHRONIZED_SCENE(
						vInboundSceneCoord, <<0,0,GET_HEADING_OF_PRIVATE_YACHT(iYachtID)>>)
				SET_SYNCHRONIZED_SCENE_PHASE(iScene, 0.0)
				cYachtCam1 = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				PLAY_SYNCHRONIZED_CAM_ANIM(cYachtCam1, iScene, inboundAnimName, AnimDictName)
				PRINTLN("[MYACHT] - start inbound synch scene ", iScene, " camera \"", inboundAnimName, "\", \"", AnimDictName, "\", ", vInboundSceneCoord)
				
				
				PLAY_STREAM_FROM_POSITION(GET_COORDS_OF_PRIVATE_YACHT(iYachtID))
				START_AUDIO_SCENE("DLC_APT_Yacht_Arrive_Scene")
				PLAY_SOUND_FROM_COORD(-1, "Arrive_Horn", GET_COORDS_OF_PRIVATE_YACHT(iYachtID), "DLC_Apartment_Yacht_Streams_Soundset")
				
				IF NOT IS_SCREEN_FADED_IN()
				AND NOT IS_SCREEN_FADING_IN()
					DO_SCREEN_FADE_IN(1000)
				ENDIF
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_97
			ENDIF
		ENDIF
	ENDIF
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_97)
	
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpTo)
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
			IF GET_SYNCHRONIZED_SCENE_PHASE(iScene) <= 0.95
				//
				#IF IS_DEBUG_BUILD
				IF g_bEditSynchSceneOffsets
					iYachtID = g_SpawnData.iYachtToWarpTo
					SET_SYNCHRONIZED_SCENE_PHASE(iScene, g_vSynchScenePhase)
					SET_SYNCHRONIZED_SCENE_ORIGIN(iScene,
							GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, inboundSceneOffset+g_vSynchSceneCoordOffset),
							<<0,0,GET_HEADING_OF_PRIVATE_YACHT(iYachtID)>>+g_vSynchSceneRotOffset)
					DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, inboundSceneOffset+g_vSynchSceneCoordOffset), 0.125)
				ENDIF
				#ENDIF
				
				IF DOES_CAM_EXIST(cYachtCam1)
					IF IS_CAM_RENDERING(cYachtCam1)
						CDEBUG3LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - hCam0 rendering")
					ELSE
						CERRORLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - hCam0 not rendering")
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - hCam0 doesn't exist")
				ENDIF
				
			ELSE
				PRINTLN("[MYACHT] - finished inbound synch scene ", iScene, " - phase > 0.95")
				GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_98
			ENDIF
		ELSE
			PRINTLN("[MYACHT] - finished inbound synch scene ", iScene, " - not running")
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_98
		ENDIF
	ENDIF
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_98)
		
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpTo)
		
		PRINTLN("[MYACHT] - finished synch scene ", iScene, " camera, clean up")
		IF DOES_CAM_EXIST(cYachtCam1)
			DESTROY_CAM(cYachtCam1)
		ENDIF
		IF DOES_CAM_EXIST(cYachtCam2)
			DESTROY_CAM(cYachtCam2)
		ENDIF
		IF DOES_ANIM_DICT_EXIST(AnimDictName)
			REMOVE_ANIM_DICT(AnimDictName)
		ENDIF
		
		STOP_STREAM()
		STOP_AUDIO_SCENE("DLC_APT_Yacht_Arrive_Scene")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_APARTMENT/APT_Yacht_01")
		
		CLEANUP_MP_CUTSCENE()
	
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_PLAYER_CONTROL(LocalPlayer,TRUE)
		SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE)
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		
		CLEANUP_MP_CUTSCENE()
		
		
		
		GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_99
	ENDIF
	
	// restore gameplay / cleanup
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_99)
		NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
		
		GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_100
		PRINTLN("[MYACHT] - going to wait for owner cleanup confirmation. ")
		
		GlobalplayerBD[iPlayer].PrivateYachtDetails.bWarpActive = FALSE
	ENDIF
	
	// make sure owner has finished.
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_100)
	
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_YACHT_CUTSCENE_RUNNING)
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_0
			PRINTLN("[MYACHT] Going back to iWarp_0")
		ENDIF

		IF bIsLocalPlayerHost
			CLEAR_BIT(MC_serverBD.iServerBitSet7, SBBOOL7_YACHT_CUTSCENE_RUNNING)
		ENDIF
	ENDIF
	
	IF GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState > iWarp_0
		DISABLE_DPADDOWN_THIS_FRAME()
		
		DISPLAY_AMMO_THIS_FRAME(FALSE)			
		HUD_FORCE_WEAPON_WHEEL(FALSE)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
		SET_CLEAR_ON_CALL_HUD_THIS_FRAME()

		DISABLE_SELECTOR_THIS_FRAME()
		
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
		
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
		
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		SET_CLEAR_ON_CALL_HUD_THIS_FRAME(TRUE)
		
		SET_IDLE_KICK_DISABLED_THIS_FRAME()
	ENDIF
ENDPROC

FUNC BOOL PLAY_YACHT_SCENE(INT &iCutStage, STRUCT_NET_YACHT_SCENE &scene, CAMERA_INDEX& hCam0, CAMERA_INDEX& hCam1, BOOL bCleanupAtEnd)

	SWITCH iCutStage
		CASE 0
			START_MP_CUTSCENE(FALSE) // should still see ambient vehicle in wide shot?
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
			IF IS_SKYSWOOP_AT_GROUND()
				NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
			ENDIF
			
			IF DOES_CAM_EXIST(hCam1)
				DESTROY_CAM(hCam1)
			ENDIF
			IF DOES_CAM_EXIST(hCam0)
				DESTROY_CAM(hCam0)
			ENDIF
			
			SceneTool_ExecutePan(scene.mPans[NET_YACHT_SCENE_PAN_establishing], hCam0, hCam1)
			scene.iTimer = GET_GAME_TIMER()
			
			PRINTLN("[MYACHT] PLAY_YACHT_SCENE - start camera pan.")	
			iCutStage = 1
		BREAK
		
		CASE 1
			IF GET_GAME_TIMER() > (scene.iTimer + ROUND(scene.mPans[NET_YACHT_SCENE_PAN_establishing].fDuration * 1000.0))
				PRINTLN("[MYACHT] PLAY_YACHT_SCENE - start camera hold.")	
				iCutStage = 2
			ENDIF
		BREAK
		
		CASE 2
			IF GET_GAME_TIMER() > (scene.iTimer + ROUND(scene.fExitDelay * 1000.0))
				PRINTLN("[MYACHT] PLAY_YACHT_SCENE - finished scene, cleanup.")	
				iCutStage = 3
			ENDIF
		BREAK
		
		CASE 3
			IF bCleanupAtEnd
				CLEANUP_MP_CUTSCENE()
			
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				SET_PLAYER_CONTROL(LocalPlayer,TRUE)
				SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				IF DOES_CAM_EXIST(hCam1)
					DESTROY_CAM(hCam1)
				ENDIF
				IF DOES_CAM_EXIST(hCam0)
					DESTROY_CAM(hCam0)
				ENDIF
			ENDIF
			
			PRINTLN("[MYACHT] PLAY_YACHT_SCENE - cleanup camera data, bCleanupAtEnd:", GET_STRING_FROM_BOOL(bCleanupAtEnd))	
			iCutStage = 0
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC PROCESS_YACHT_FOR_CUTSCENE()
	IF IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_YACHT_CUTSCENE_RUNNING)
	OR GlobalplayerBD[iPartToUse].PrivateYachtDetails.iWarpState > 0
		UPDATE_FMMC_YACHT_WARPING(iYachtScene, &PLAY_YACHT_SCENE)
	ELSE
		#IF IS_DEBUG_BUILD
		IF bMissionYachtDebug
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD4)
			AND IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5)
				BROADCAST_FMMC_MOVE_FMMC_YACHT(iLocalPart)
			ENDIF
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Artificial Lights / Pre-game EMP --------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Contains some functionality related to artificial lights.  -------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC PROCESS_PRE_GAME_EMP_AFTER_INTRO()
	//If player joins after the intro then it needs to do this. 
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciPRE_GAME_EMP)
			TURN_LIGHTS_OFF_NOW()
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: IAA Gun Cameras  ------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Handles enabling Technical's gun camera functionality around the IAA building.  ----------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC PROCESS_IAA_ENABLED_GUN_CAMERAS()
	INT iTeam = MC_playerBD[iLocalPart].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_IAA_GUN_CAMERAS_ENABLED)
			g_eIAAGunCamerasEnabled = IAA_ALLOW_GUN_CAM
		ELSE
			g_eIAAGunCamerasEnabled = IAA_FULL_ACTIVITY_KICK
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Time of Day  ----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Handles time of day.  --------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_BLOCK_TOD_EVERY_FRAME()
	
	IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_BLOCK_TOD_EVERY_FRAME)
		PRINTLN("SHOULD_BLOCK_TOD_EVERY_FRAME - LBOOL7_BLOCK_TOD_EVERY_FRAME")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_USING_TOD_BASED_ON_RULE_TIME)
		PRINTLN("SHOULD_BLOCK_TOD_EVERY_FRAME - LBOOL35_USING_TOD_BASED_ON_RULE_TIME")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_USING_TOD_BASED_ON_RULE)
		PRINTLN("SHOULD_BLOCK_TOD_EVERY_FRAME - LBOOL35_USING_TOD_BASED_ON_RULE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_MISSION_TIME_OF_DAY_EVERY_FRAME()
	IF MC_serverBD.iTimeOfDay > -1
		IF IS_CORONA_READY_TO_START_WITH_JOB()
		OR DID_I_JOIN_MISSION_AS_SPECTATOR()
		OR IS_FAKE_MULTIPLAYER_MODE_SET()
		OR bIsSCTV
			IF NOT SHOULD_BLOCK_TOD_EVERY_FRAME()
			AND IS_BIT_SET(MC_serverbd.iServerBitSet6, SBBOOL6_UPDATED_WEATHER_AND_TIME_FROM_CORONA)
								
				INT iTeam = MC_playerBD[iPartToUse].iteam
				INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
				
				IF iRule < FMMC_MAX_RULES
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_ENABLE_SPECIFIC_TOD)
						IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciPROGRESS_TIME_OF_DAY) AND NOT IS_BIT_SET(iLocalBoolCheck9,LBOOL9_TIME_SET))
						OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciPROGRESS_TIME_OF_DAY)
							
							IF MC_ServerBD_1.sMissionContinuityVars.iTimeHour != -1
							AND MC_ServerBD_1.sMissionContinuityVars.iTimeMinute != -1
								PRINTLN("PROCESS_MISSION_TIME_OF_DAY_EVERY_FRAME - Using continuity time ",MC_ServerBD_1.sMissionContinuityVars.iTimeHour,":", MC_ServerBD_1.sMissionContinuityVars.iTimeMinute)
								NETWORK_OVERRIDE_CLOCK_TIME(MC_ServerBD_1.sMissionContinuityVars.iTimeHour, MC_ServerBD_1.sMissionContinuityVars.iTimeMinute, 0)
							ELSE
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciFORCE_SPECIFIC_TOD)
									SET_TIME_OF_DAY(MC_serverBD.iTimeOfDay)
								ELSE
									NETWORK_OVERRIDE_CLOCK_TIME(g_FMMC_STRUCT.iTODOverrideHours, g_FMMC_STRUCT.iTODOverrideMinutes, 0)
								ENDIF
							ENDIF
							
							IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_TIME_SET)
								SET_BIT(iLocalBoolCheck9, LBOOL9_TIME_SET)
								PRINTLN("PROCESS_MISSION_TIME_OF_DAY_EVERY_FRAME - Setting LBOOL9_TIME_SET")
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_ProgressTimeToPoint)
						AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciPROGRESS_TIME_OF_DAY)
						AND IS_BIT_SET(iLocalBoolCheck9,LBOOL9_TIME_SET)
						AND MC_serverBD.iProgressTime_Hours != -1
							IF MC_serverBD.iProgressTime_Minutes > 0
								IF GET_CLOCK_HOURS() = MC_serverBD.iProgressTime_Hours
								AND GET_CLOCK_MINUTES() = MC_serverBD.iProgressTime_Minutes
									NETWORK_OVERRIDE_CLOCK_TIME(MC_serverBD.iProgressTime_Hours, MC_serverBD.iProgressTime_Minutes, 0)
								ENDIF
							ELSE
								IF GET_CLOCK_HOURS() = MC_serverBD.iProgressTime_Hours
									NETWORK_OVERRIDE_CLOCK_TIME(MC_serverBD.iProgressTime_Hours, 0, 0)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_WEATHER_SET)
				IF MC_serverBD.iWeather > -1
				AND IS_BIT_SET(MC_serverbd.iServerBitSet6, SBBOOL6_UPDATED_WEATHER_AND_TIME_FROM_CORONA)
					SET_WEATHER_FOR_FMMC_MISSION(MC_serverBD.iWeather)
					SET_BIT(iLocalBoolCheck2, LBOOL2_WEATHER_SET)
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_WeatherTracking)
					AND g_TransitionSessionNonResetVars.sMissionContinuityVars.iWeather = -1
						g_TransitionSessionNonResetVars.sMissionContinuityVars.iWeather = MC_ServerBD_1.sMissionContinuityVars.iWeather
						PRINTLN("[CONTINUITY] - PROCESS_MISSION_TIME_OF_DAY_EVERY_FRAME - g_TransitionSessionNonResetVars.sMissionContinuityVars.iWeather set to ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iWeather)
					ENDIF
				ENDIF
			ENDIF		
		ENDIF
	ENDIF
ENDPROC

PROC SET_START_TIME_BASED_ON_RULE()
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_ENABLE_IS_START_TIME)
			iTargetHour = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODStartHour[iRule]
			IF iRuleHour != iTargetHour
				iRuleHour = iTargetHour
			ENDIF
			
			NETWORK_OVERRIDE_CLOCK_TIME(iRuleHour,  0, 0)
			PRINTLN("[JT][TIME] Starting time is: ", iRuleHour)
		ENDIF				
	ENDIF
ENDPROC

///PURPOSE:
///    Adjusts the time to creator specified values
PROC SET_TIME_BASED_ON_RULE()
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSixteen[iRule], ciBS_RULE16_TOD_UPDATE_TIME_OVER_THE_COURSE_OF_RULE)
			EXIT
		ENDIF
		
		INT iSpeed = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODSpeed[iRule]
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_ENABLE_SPECIFIC_TOD)
		AND NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
			SET_BIT(iLocalBoolCheck35, LBOOL35_USING_TOD_BASED_ON_RULE)
			IF iRuleHour != iTargetHour
				SWITCH iSpeed
					CASE 0
						IF iRuleMinute != 59
							iRuleMinute++
						ELIF iRuleMinute >= 59
							IF iRuleHour >= 23 AND iRuleMinute >= 59
								iRuleHour = 0
								iRuleMinute = 0
							ELSE
								iRuleHour++
								iRuleMinute = 0
							ENDIF
						ENDIF	
					BREAK
					CASE 1
						IF iRuleMinute != 57
							iRuleMinute+=3
						ELIF iRuleMinute >= 57
							IF iRuleHour >= 23 AND iRuleMinute >= 57
								iRuleHour = 0
								iRuleMinute = 0
							ELSE
								iRuleHour++
								iRuleMinute = 0
							ENDIF
						ENDIF
					BREAK
					CASE 2
						IF iRuleMinute != 55
							iRuleMinute+=5
						ELIF iRuleMinute >= 55
							IF iRuleHour >= 23 AND iRuleMinute >= 55
								iRuleHour = 0
								iRuleMinute = 0
							ELSE
								iRuleHour++
								iRuleMinute = 0
							ENDIF
						ENDIF
					BREAK
					CASE 3
						IF iRuleMinute != 56
							iRuleMinute+=8
						ELIF iRuleMinute >= 56
							IF iRuleHour >= 23 AND iRuleMinute >= 56
								iRuleHour = 0
								iRuleMinute = 0
							ELSE
								iRuleHour++
								iRuleMinute = 0
							ENDIF
						ENDIF
					BREAK
					CASE 4
						RESET_ADAPTATION(10)
						iRuleHour = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODHour[iRule]
						iRuleMinute = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODMinute[iRule]
						
					BREAK
				ENDSWITCH
			ELIF iRuleHour = iTargetHour
			AND (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_ENABLE_CHANGE_TOD_ON_RULE)
			OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_ALLOW_TOD_CHANGE))
					
				iTargetHour = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODHour[iRule]
				PRINTLN("[JT TIMECYCLE] Setting the target hour to: ", iTargetHour)
				
			ELIF iTargetHour = -1
				iTargetHour = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODHour[iRule]
			ENDIF
			
			NETWORK_OVERRIDE_CLOCK_TIME(iRuleHour, iRuleMinute, 0)
			
			PRINTLN("[JT][TIME] Current time for team ", iTeam, " is: ", iRuleHour, ":", iRuleMinute," target hour is: ", iTargetHour)
		ELIF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciENABLE_SUDDEN_DEATH_TIME_OF_DAY_CHANGE)
			IF iRuleHour != iSuddenDeathHour
				IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_SD_TIMECYCLE_SET)
					CLEAR_BIT(iLocalBoolCheck18, LBOOL18_SD_TIMECYCLE_SET)
				ENDIF

				PRINTLN("[JT ANIMPOST] NIGHT Time is 0")
				IF NOT ANIMPOSTFX_IS_RUNNING("LostTimeDay")
					ANIMPOSTFX_PLAY("LostTimeDay", iDayAnimFXDura, FALSE)									
					iRuleHour = iSuddenDeathHour
					PRINTLN("[JT ANIMPOST] JUDGEMENT DAY Started playing animFX for 2 seconds")
				ENDIF
			ENDIF
			
			IF iRuleHour = iSuddenDeathHour
				//Timecycle
				IF NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_SD_TIMECYCLE_SET)
					CLEAR_TIMECYCLE_MODIFIER()
					SET_TIMECYCLE_MODIFIER("mp_lad_judgment")
					
					SET_BIT(iLocalBoolCheck18, LBOOL18_SD_TIMECYCLE_SET)
					PRINTLN("[JT TIMECYCLE] SUDDEN DEATH TIMECYCLE ACTIVE!")
				ENDIF
				
				//Particles
				IF bLocalPlayerPedOK
					IF NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_TLAD_JUDGEMENT_DAY_PTFX_ACTIVE)
						USE_PARTICLE_FX_ASSET("scr_bike_adversary")
						START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_adversary_judgement_ash", localPlayerPed, <<0,0,0>>, <<0,0,0>>)
						USE_PARTICLE_FX_ASSET("scr_bike_adversary")
						TLADptfxLensDirt = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_adversary_judgement_lens_dirt", localPlayerPed, <<0,0,0>>, <<0,0,0>>)
						SET_PARTICLE_FX_LOOPED_EVOLUTION(TLADptfxLensDirt,"level", 1.0, TRUE)
						PRINTLN("[JT PFX] PARTICLES ON SHOW")
						SET_BIT(iLocalBoolCheck18, LBOOL18_TLAD_JUDGEMENT_DAY_PTFX_ACTIVE)
					ENDIF
				ELSE
					CLEAR_BIT(iLocalBoolCheck18, LBOOL18_TLAD_JUDGEMENT_DAY_PTFX_ACTIVE)
				ENDIF
			ENDIF
			
			NETWORK_OVERRIDE_CLOCK_TIME(iRuleHour,  0, 0)
			
			PRINTLN("[JT][TIME] SUDDEN DEATH Current time for team ", iTeam, " is: ", iRuleHour, " target hour is: ", iTargetHour)
		ELSE
			CLEAR_BIT(iLocalBoolCheck35, LBOOL35_USING_TOD_BASED_ON_RULE)
		ENDIF
	ENDIF
ENDPROC

PROC SET_TIME_PROGRESSION_BASED_ON_RULE_TIME()
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	IF iRule < FMMC_MAX_RULES
		INT iMaxRuleTime = GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iRule]) / 1000
		
		IF  IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_ENABLE_SPECIFIC_TOD)
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSixteen[iRule], ciBS_RULE16_TOD_UPDATE_TIME_OVER_THE_COURSE_OF_RULE)
		AND iMaxRuleTime > 0
		
			INT iMinute, iSec, iMil, iCurrentTimeTotalAmount
			GET_TIME_AS_MINUTES_SECONDS_MILLISECONDS(GET_GAME_TIMER() - i_clock_start, iMinute, iSec, iMil)
			iCurrentTimeTotalAmount = iMil + (iSec * 1000) + (iMinute * 60000)
			
			//On Rule Start
			IF IS_BIT_SET(iLocalBoolCheck6, (LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam))
				CLEAR_TIMECYCLE_MODIFIER()
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_ENABLE_IS_START_TIME)
					iRuleHour = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODStartHour[iRule]
				ELSE
					iRuleHour = GET_CLOCK_HOURS()
					IF iTargetHour != 0
						iRuleHour = iTargetHour
					ENDIF
				ENDIF
				iRuleMinute = 0
				iTimeUpdateFinishTime = iCurrentTimeTotalAmount + (iMaxRuleTime * 1000)
				iTargetHour = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODHour[iRule]
				
				INT iHoursToUpdate = ABSI(iTargetHour - iRuleHour)
				
				IF iTargetHour <= iRuleHour
					iHoursToUpdate += 24
				ENDIF
				
				iUpdateRateHour   = (iTimeUpdateFinishTime - iCurrentTimeTotalAmount) / (iHoursToUpdate)
				iUpdateRateMinute = iUpdateRateHour / 60
				
				iNextTimeUpdateHour    = iCurrentTimeTotalAmount + iUpdateRateHour
				iNextTimeUpdateMinute  = iCurrentTimeTotalAmount + iUpdateRateMinute
			ENDIF
			
			//Update Hours
			IF iNextTimeUpdateHour <= iCurrentTimeTotalAmount
			AND iRuleHour < iTargetHour
				iNextTimeUpdateHour    = iCurrentTimeTotalAmount + iUpdateRateHour
				iNextTimeUpdateMinute  = iCurrentTimeTotalAmount + iUpdateRateMinute
				iRuleHour++
				iRuleMinute = 0
			ENDIF
			
			//Update Minutes
			IF iNextTimeUpdateMinute <= iCurrentTimeTotalAmount
			AND iRuleHour < iTargetHour
				iNextTimeUpdateMinute = iCurrentTimeTotalAmount + iUpdateRateMinute
				iRuleMinute++
				IF iRuleMinute = 60
					iRuleMinute = 0
				ENDIF
			ENDIF
			
			//Just in case something goes wrong set the hour manually
			IF iCurrentTimeTotalAmount >= iTimeUpdateFinishTime
				iRuleHour = iTargetHour
				iRuleMinute = 0
			ENDIF

			IF iRuleHour < 25 AND iRuleHour > -1
			AND iRuleMinute < 60 AND iRuleMinute > -1
				NETWORK_OVERRIDE_CLOCK_TIME(iRuleHour, iRuleMinute, 0)
				SET_BIT(iLocalBoolCheck35, LBOOL35_USING_TOD_BASED_ON_RULE_TIME)
				PRINTLN("[TIME] - SET_TIME_PROGRESSION_BASED_ON_RULE - Current time for team ", iTeam, " is: ", iRuleHour, ":", iRuleMinute," target hour is: ", iTargetHour)
			ENDIF
		ELSE
			CLEAR_BIT(iLocalBoolCheck35, LBOOL35_USING_TOD_BASED_ON_RULE_TIME)
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_WEATHER_AS_STRING(INT iWeather)
	SWITCH iWeather
		CASE ciFMMC_WEATHER_OPTION_CURRENT				RETURN "CURRENT"
		CASE ciFMMC_WEATHER_OPTION_SUNNY				RETURN "EXTRASUNNY"
		CASE ciFMMC_WEATHER_OPTION_RAINING				RETURN "RAIN"
		CASE ciFMMC_WEATHER_OPTION_SNOW					RETURN "SNOW"
		CASE ciFMMC_WEATHER_OPTION_SMOG					RETURN "SMOG"
		CASE ciFMMC_WEATHER_OPTION_HALLOWEEN			RETURN "HALLOWEEN"
		CASE ciFMMC_WEATHER_OPTION_HALLOWEEN_NO_RAIN 	RETURN "HALLOWEEN NO RAIN"
		CASE ciFMMC_WEATHER_OPTION_CLEAR 				RETURN "CLEAR"
		CASE ciFMMC_WEATHER_OPTION_CLOUDS 				RETURN "CLOUDS"
		CASE ciFMMC_WEATHER_OPTION_OVERCAST				RETURN "OVERCAST"
		CASE ciFMMC_WEATHER_OPTION_THUNDER 				RETURN "THUNDER"
		CASE ciFMMC_WEATHER_OPTION_FOGGY 				RETURN "FOGGY"
		CASE ciFMMC_WEATHER_OPTION_RAIN_THUNDER 		RETURN "RAIN THUNDER"
	ENDSWITCH
	
	RETURN "UNKNOWN"
ENDFUNC
#ENDIF

FUNC STRING GET_WEATHER_NAME_FROM_INT(INT iWeather)
	SWITCH iWeather
		CASE ciFMMC_WEATHER_OPTION_CURRENT				RETURN "CURRENT"
		CASE ciFMMC_WEATHER_OPTION_SUNNY				RETURN "EXTRASUNNY"
		CASE ciFMMC_WEATHER_OPTION_RAINING				RETURN "RAIN"
		CASE ciFMMC_WEATHER_OPTION_SNOW					RETURN "SNOW"
		CASE ciFMMC_WEATHER_OPTION_SMOG					RETURN "SMOG"
		CASE ciFMMC_WEATHER_OPTION_HALLOWEEN			RETURN "HALLOWEEN"
		CASE ciFMMC_WEATHER_OPTION_HALLOWEEN_NO_RAIN	RETURN "HALLOWEEN"
		CASE ciFMMC_WEATHER_OPTION_CLEAR				RETURN "CLEAR"
		CASE ciFMMC_WEATHER_OPTION_CLOUDS				RETURN "CLOUDS"
		CASE ciFMMC_WEATHER_OPTION_OVERCAST				RETURN "OVERCAST"
		CASE ciFMMC_WEATHER_OPTION_THUNDER				RETURN "THUNDER"
		CASE ciFMMC_WEATHER_OPTION_FOGGY				RETURN "FOGGY"
		CASE ciFMMC_WEATHER_OPTION_RAIN_THUNDER			RETURN "THUNDER"
	ENDSWITCH
	
	RETURN "UNKNOWN"
ENDFUNC

///PURPOSE:
///    Adjusts the weather to creator specified values
PROC SET_WEATHER_BASED_ON_RULE()
	IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iSpecificTODWeather[GET_TEAM_CURRENT_RULE(MC_playerBD[iPartToUse].iTeam)] = ciFMMC_WEATHER_OPTION_CURRENT
		EXIT
	ENDIF
	
	INT iTeam     = MC_playerBD[iPartToUse].iTeam
	INT iWeather  = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODWeather[GET_TEAM_CURRENT_RULE(iTeam)]
	FLOAT fTransitionTimeInSeconds = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fSpecificTODWeatherTransitionTime[GET_TEAM_CURRENT_RULE(iTeam)]
	INT iClearOverrideDelay = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODWeatherClearOverrideDelay[GET_TEAM_CURRENT_RULE(iTeam)]
	
	//Modify the weather
	IF DOES_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(iTeam)
		IF fTransitionTimeInSeconds = 0.0
		OR iClearOverrideDelay = 0
			CLEAR_OVERRIDE_WEATHER()
		ELSE
			iWeatherTransitionStartTime = GET_CLOUD_TIME_AS_INT()
		ENDIF
		
		SET_WEATHER_FOR_FMMC_MISSION(iWeather, FALSE, fTransitionTimeInSeconds)
		
		PRINTLN("[WEATHER][TEAM ", iTeam, "][RULE ", GET_TEAM_CURRENT_RULE(iTeam), "] Set weather to ", DEBUG_GET_WEATHER_AS_STRING(iWeather))
	ENDIF
	
	//Required for a smooth blend from the global override
	IF iWeatherTransitionStartTime > 0
	AND GET_CLOUD_TIME_AS_INT() - iWeatherTransitionStartTime >= iClearOverrideDelay
		CLEAR_OVERRIDE_WEATHER()
		SET_WEATHER_FOR_FMMC_MISSION(iWeather, FALSE)
		iWeatherTransitionStartTime = 0
	ENDIF

ENDPROC

PROC PROCESS_SERVER_TIME_OF_DAY()

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_ProgressTimeToPoint)
	OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciPROGRESS_TIME_OF_DAY)
		EXIT
	ENDIF
	
	INT iTeam
	FOR iTeam = 0 TO MC_serverBD.iNumActiveTeams-1
	
		INT iRule = GET_TEAM_CURRENT_RULE(iTeam)
		
		IF NOT IS_RULE_INDEX_VALID(iRule)
			RELOOP
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_UPDATE_PROGRESS_TO_POINT_TIME)
			RELOOP
		ENDIF
		
		IF DOES_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(iTeam)
			MC_serverBD.iProgressTime_Hours = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODHour[iRule]
			MC_serverBD.iProgressTime_Minutes = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODMinute[iRule]
			PRINTLN("PROCESS_SERVER_TIME_OF_DAY - Updating time to progress to ", MC_serverBD.iProgressTime_Hours, ":", MC_serverBD.iProgressTime_Minutes)
		ENDIF
		
	ENDFOR
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Orbital Cannon  -------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Handles Orbital Cannon functionality.  --------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC MAINTAIN_MISSION_ORBITAL_CANNON_FOR_CREATOR()	
	IF NOT bHasLaunchedFakeMissionOrbitalCannon
		IF g_bMissionPlacedOrbitalCannon	 
			PRINTLN("[AM_MP_ORBITAL_CANNON] - MAINTAIN_MISSION_ORBITAL_CANNON - Requesting")
			
			REQUEST_SCRIPT("AM_MP_ORBITAL_CANNON")
			
			IF HAS_SCRIPT_LOADED("AM_MP_ORBITAL_CANNON")
				PRINTLN("[AM_MP_ORBITAL_CANNON] - MAINTAIN_MISSION_ORBITAL_CANNON - Launching")
				
				START_NEW_SCRIPT("AM_MP_ORBITAL_CANNON", DEFAULT_STACK_SIZE)
				SET_SCRIPT_AS_NO_LONGER_NEEDED("AM_MP_ORBITAL_CANNON")
				bHasLaunchedFakeMissionOrbitalCannon = TRUE
			ELSE
				PRINTLN("[AM_MP_ORBITAL_CANNON] - MAINTAIN_MISSION_ORBITAL_CANNON - Script not loaded")
			ENDIF
		ENDIF
	ELSE
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("AM_MP_ORBITAL_CANNON")) = 0
		AND bHasLaunchedFakeMissionOrbitalCannon
		AND (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(LocalPlayerPed), <<258.5167, 6123.9121, -160.4223>>) > 30 AND NOT g_bMissionPlacedOrbitalCannon)		
			bHasLaunchedFakeMissionOrbitalCannon = FALSE
			
			PRINTLN("[AM_MP_ORBITAL_CANNON] - MAINTAIN_MISSION_ORBITAL_CANNON - Resetting")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_MISSION_ORBITAL_CANNON()
	
	IF HAS_TEAM_FAILED(MC_PlayerBD[iLocalPart].iTeam)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_FINISHED)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
		PRINTLN("[PROCESS_MISSION_ORBITAL_CANNON] - Setting g_bMissionPlacedOrbitalCannon to FALSE.")
		g_bMissionPlacedOrbitalCannon = FALSE
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Shops  ----------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Handles shop functionality.  -------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_FIRST_VEHICLE_FROM_OTHER_PLAYERS_NEEDED_TO_REPAIR()
	
	INT iPart = -1
	DO_PARTICIPANT_LOOP_FLAGS eFlags = GET_PARTICIPANT_LOOP_FLAGS_TO_CHECK_SINGLE_TEAM(MC_PlayerBD[iPartToUse].iTeam)
	WHILE DO_PARTICIPANT_LOOP(iPart, eFlags | DPLF_EXCLUDE_LOCAL_PART | DPLF_CHECK_PED_ALIVE | DPLF_FILL_PLAYER_IDS)
		
		IF MC_playerBD_1[iPart].iVehicleOwnerNeededToRepair > -1
			IF IS_PED_IN_ANY_VEHICLE(piParticipantLoop_PedIndex)
				PRINTLN("[RCC MISSION][ModShopProg] - iPart: ", iPart, " Needs to repair iVehicle: ", MC_playerBD_1[iPart].iVehicleOwnerNeededToRepair)
				RETURN MC_playerBD_1[iPart].iVehicleOwnerNeededToRepair
			ENDIF
		ENDIF
		
	ENDWHILE
	
	RETURN -1
	
ENDFUNC

PROC PROCESS_SHOPS_FOR_INSTANCED_CONTENT()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	IF iTeam > -1 AND iTeam < FMMC_MAX_TEAMS
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		IF iRule < FMMC_MAX_RULES
			
			INT iMissionEntityID = -1
			INT iClientStage 
			iClientStage = GET_MC_CLIENT_MISSION_STAGE(iLocalPart)
			IF iClientStage = CLIENT_MISSION_STAGE_DELIVER_VEH
				PRINTLN("[RCC MISSION][ModShopProg] - CLIENT_MISSION_STAGE_DELIVER_VEH")
				
				IF bLocalPlayerOK
					PRINTLN("[RCC MISSION][ModShopProg] - Player Ok.")
					
					IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)	
						FLOAT fPercentage
						VEHICLE_INDEX viTempVeh
						viTempVeh = GET_VEHICLE_PED_IS_IN(localPlayerPed)							
						iMissionEntityID = IS_ENTITY_A_MISSION_CREATOR_ENTITY(viTempVeh)
							
						PRINTLN("[RCC MISSION][ModShopProg] - iMissionEntityID = ", iMissionEntityID)
													
						IF iMissionEntityID != -1								
							fPercentage = MC_GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(viTempVeh, iMissionEntityID, GET_TOTAL_STARTING_PLAYERS())
						ELSE
							fPercentage = GET_VEHICLE_HEALTH_PERCENTAGE(viTempVeh)
						ENDIF
						
						PRINTLN("[MMacK][ModShopProg] Limit is : ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinVehHealth[iRule])
						PRINTLN("[MMacK][ModShopProg] Car Is : ", fPercentage)
						
						IF fPercentage > g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinVehHealth[iRule]
							PRINTLN("[MMacK][ModShopProg] Car Is over Health Limit")
							iMissionEntityID = -1
						ELSE
							PRINTLN("[MMacK][ModShopProg] Car Is under Health Limit")
						ENDIF
					ENDIF
				ENDIF
			
		
				IF MC_playerBD_1[iLocalPart].iVehicleOwnerNeededToRepair != iMissionEntityID
					MC_playerBD_1[iLocalPart].iVehicleOwnerNeededToRepair = iMissionEntityID
					PRINTLN("[LM][[ModShopProg] - new value for iVehicleOwnerNeededToRepair: ", MC_playerBD_1[iLocalPart].iVehicleOwnerNeededToRepair)
				ENDIF
				
				IF iMissionEntityID = -1
					INT iTempV = GET_FIRST_VEHICLE_FROM_OTHER_PLAYERS_NEEDED_TO_REPAIR()
					IF MC_playerBD_1[iLocalPart].iVehicleNeededToRepair != iTempV
						MC_playerBD_1[iLocalPart].iVehicleNeededToRepair = iTempV
						PRINTLN("[LM][[ModShopProg] - new value for iVehicleNeededToRepair: ", 	MC_playerBD_1[iLocalPart].iVehicleNeededToRepair)
					ENDIF					
				ENDIF
			ELSE
				MC_playerBD_1[iLocalPart].iVehicleNeededToRepair = -1
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_ALL_SHOPS_LOCKED)
				IF IS_CURRENT_OBJECTIVE_REPAIR_VEHICLE()
					IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_MODSHOPS_AVAILABLE_FOR_REPAIR)
						SET_ALL_SHOPS_OF_TYPE_TEMPORARILY_UNAVAILABLE(SHOP_TYPE_CARMOD, FALSE)
						HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_CARMOD, FALSE)
						SET_BIT(iLocalBoolCheck29, LBOOL29_MODSHOPS_AVAILABLE_FOR_REPAIR)
						SET_ALLOW_MOD_SHOP_DURING_CONTENT_MISSION(TRUE)
						PRINTLN("PROCESS_SHOPS_FOR_INSTANCED_CONTENT - Unblocking")
					ENDIF
				ELSE
					IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_MODSHOPS_AVAILABLE_FOR_REPAIR)
						HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_CARMOD, TRUE)
						CLEAR_BIT(iLocalBoolCheck29, LBOOL29_MODSHOPS_AVAILABLE_FOR_REPAIR)
						SET_ALLOW_MOD_SHOP_DURING_CONTENT_MISSION(FALSE)
						PRINTLN("PROCESS_SHOPS_FOR_INSTANCED_CONTENT - Blocking again")
					ENDIF
				ENDIF
			ENDIF
			
			PRINTLN("[RCC MISSION][ModShopProg] - iVehicleNeededToRepair: ", MC_playerBD_1[iLocalPart].iVehicleNeededToRepair)
			
			// Might want to expand this so that clients can remotely chck that other clients need to respray or change license plate...			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_BLOCK_PROGRESSION_BF_MODSHOP_REPAIR)
				IF (NOT IS_BIT_SET(g_iBS1_Mission, ciBS1_Mission_PurchasedVehicleRespray) AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_BLOCK_PROGRESSION_BF_MODSHOP_RESPRAY))
				OR (NOT IS_BIT_SET(g_iBS1_Mission, ciBS1_Mission_PurchasedVehiclePlateChange) AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_BLOCK_PROGRESSION_BF_MODSHOP_LICENSE_CHNG))
					PRINTLN("[RCC MISSION][ModShopProg] - Blocking the objective. Waiting to modify the vehicle. g_iBS1_Mission: ", g_iBS1_Mission)
					BLOCK_OBJECTIVE_THIS_FRAME()
				ENDIF				
			ENDIF
		ENDIF
	ENDIF
	
	MAINTAIN_DISABLED_APARTMENT_ACCESS_FOR_COPS()
ENDPROC

PROC PROCESS_NAVMESH_UNLOADING_WHEN_UNSAFE()
	
	IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_NAVMESH_REGION_REQUESTED)
		IF NOT IS_IT_SAFE_TO_LOAD_NAVMESH()
			
			PRINTLN("[LOAD_COLLISION_FLAG] PROCESS_NAVMESH_UNLOADING_WHEN_UNSAFE - IS_IT_SAFE_TO_LOAD_NAVMESH returning FALSE, remove navmesh")
			REMOVE_NAVMESH_REQUIRED_REGIONS()
			
			#IF IS_DEBUG_BUILD
			INT iPedsWithLoadedNavmesh
			#ENDIF
			
			IF iPedNavmeshLoadedBS[0] != 0
			OR iPedNavmeshLoadedBS[1] != 0
			OR iPedNavmeshLoadedBS[2] != 0
				INT iped
				
				FOR iped = 0 TO (MC_serverBD.iNumPedCreated - 1)
					IF FMMC_IS_LONG_BIT_SET(iPedNavmeshLoadedBS, iPed)
						PRINTLN("[LOAD_COLLISION_FLAG] PROCESS_NAVMESH_UNLOADING_WHEN_UNSAFE - Clearing iPedNavmeshLoadedBS & iPedNavmeshCheckedBS for ped ",iped)
						FMMC_CLEAR_LONG_BIT(iPedNavmeshCheckedBS, iPed)
						FMMC_CLEAR_LONG_BIT(iPedNavmeshLoadedBS, iPed)
						
						#IF IS_DEBUG_BUILD
						iPedsWithLoadedNavmesh++
						#ENDIF
					ENDIF
				ENDFOR
			ENDIF
			
			IF iGangChasePedNavmeshLoadedBS != 0
				INT iGangChaseUnit
				
				FOR iGangChaseUnit = 0 TO ciMAX_GANG_CHASE_VEHICLES - 1
					IF IS_BIT_SET(iGangChasePedNavmeshLoadedBS, iGangChaseUnit)
						PRINTLN("[LOAD_COLLISION_FLAG][GANG_CHASE] PROCESS_NAVMESH_UNLOADING_WHEN_UNSAFE - Clearing iGangChasePedNavmeshLoadedBS & iGangChasePedNavmeshCheckedBS for backup unit ", iGangChaseUnit)
						CLEAR_BIT(iGangChasePedNavmeshCheckedBS, iGangChaseUnit)
						CLEAR_BIT(iGangChasePedNavmeshLoadedBS, iGangChaseUnit)
						
						#IF IS_DEBUG_BUILD
						iPedsWithLoadedNavmesh++
						#ENDIF
					ENDIF
				ENDFOR
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF iPedsWithLoadedNavmesh != 1
				PRINTLN("[LOAD_COLLISION_FLAG] PROCESS_NAVMESH_UNLOADING_WHEN_UNSAFE - Found ",iPedsWithLoadedNavmesh," peds with loaded navmesh, should only be one!")
				CASSERTLN(DEBUG_CONTROLLER, "[LOAD_COLLISION_FLAG] PROCESS_NAVMESH_UNLOADING_WHEN_UNSAFE - Found ",iPedsWithLoadedNavmesh," peds with loaded navmesh, should only be one!")
			ELSE
				PRINTLN("[LOAD_COLLISION_FLAG] PROCESS_NAVMESH_UNLOADING_WHEN_UNSAFE - Only found 1 ped with loaded navmesh, as expected :)")
			ENDIF
			#ENDIF
			
			CLEAR_BIT(iLocalBoolCheck8, LBOOL8_NAVMESH_REGION_REQUESTED)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_CONSTRUCTION_SITE_HIGH_RISE_ROOFTOP_SHADOW_TIMECYCLE_MOD()
	
	IF NOT SHOULD_CONSTRUCTION_SITE_HIGH_RISE_ROOFTOP_SHADOW_TIMECYCLE_MOD_BE_USED()
		EXIT
	ENDIF
	
	VECTOR vRooftopPosition = <<-151, -957, 269>>
	VECTOR vLocalPlayerPed = GET_ENTITY_COORDS(localPlayerPed, FALSE)
	VECTOR vCutsceneCam 
	IF DOES_CAM_EXIST(cutscenecam)
		vCutsceneCam = GET_CAM_COORD(cutscenecam)
	ENDIF
	
	IF VDIST2(vRooftopPosition, vLocalPlayerPed) <= POW(40.0, 2.0)
	OR VDIST2(vRooftopPosition, vCutsceneCam) <= POW(40.0, 2.0)
		IF NOT IS_BIT_SET(iLocalBoolCheck35, LBOOL35_APPLIED_ROOFTOP_TIMECYCLE_MOD)
			PRINTLN("PROCESS_CONSTRUCTION_SITE_HIGH_RISE_ROOFTOP_SHADOW_TIMECYCLE_MOD - Applying MP_Sum2_ULP_Rooftop_Cascade")
			SET_TIMECYCLE_MODIFIER("MP_Sum2_ULP_Rooftop_Cascade")
			SET_TIMECYCLE_MODIFIER_STRENGTH(1.0)
			SET_BIT(iLocalBoolCheck35, LBOOL35_APPLIED_ROOFTOP_TIMECYCLE_MOD)
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_APPLIED_ROOFTOP_TIMECYCLE_MOD)
			PRINTLN("PROCESS_CONSTRUCTION_SITE_HIGH_RISE_ROOFTOP_SHADOW_TIMECYCLE_MOD - Clearing MP_Sum2_ULP_Rooftop_Cascade")
			CLEAR_TIMECYCLE_MODIFIER()
			CLEAR_BIT(iLocalBoolCheck35, LBOOL35_APPLIED_ROOFTOP_TIMECYCLE_MOD)
		ENDIF
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Warp Portals 
// ##### Description: Functions relating to the Warp Portal System. A way of setting up places that are configurable in the creator via global data, and will warp players from one place to another.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL MC_HAS_ENTITY_WARP_PORTAL_IS_ATTACHED_TO_SETTLED(INT iEntityType, INT iEntityID)

	IF iEntityID != -1
		SWITCH iEntityType
			CASE ciENTITY_TYPE_PED
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iEntityID])
					PED_INDEX pedIndex
					pedIndex = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iEntityID])
					
					IF NOT IS_PED_INJURED(pedIndex)
						IF GET_ENTITY_SPEED(pedIndex) > 0.0
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE ciENTITY_TYPE_VEHICLE
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID])
					VEHICLE_INDEX vehIndex
					vehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID])
					
					IF IS_VEHICLE_DRIVEABLE(vehIndex)
					AND IS_ENTITY_ALIVE(vehIndex)
						IF GET_ENTITY_SPEED(vehIndex) > 0.1 // Just incase we're sliding very slowly on a hill in a car
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL MC_HAS_ENTITY_WARP_PORTAL_IS_ATTACHED_TO_SETTLED_START(INT iPortal)
	RETURN MC_HAS_ENTITY_WARP_PORTAL_IS_ATTACHED_TO_SETTLED(g_FMMC_STRUCT.sWarpPortals[iPortal].iAttachToEntityType, g_FMMC_STRUCT.sWarpPortals[iPortal].iAttachToEntityID)
ENDFUNC

FUNC BOOL MC_HAS_ENTITY_WARP_PORTAL_IS_ATTACHED_TO_SETTLED_END(INT iPortal)
	INT iLinkedPortal = g_FMMC_STRUCT.sWarpPortals[iPortal].iLinkedPortal
	IF iLinkedPortal = -1
		RETURN FALSE
	ENDIF
	RETURN MC_HAS_ENTITY_WARP_PORTAL_IS_ATTACHED_TO_SETTLED(g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].iAttachToEntityType, g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].iAttachToEntityID)
ENDFUNC

FUNC VECTOR MC_GET_WARP_PORTAL_START_POSITION(INT iPortal)
	RETURN MC_GET_POSITION_OFFSET_RELATIVE_TO_ENTITY(g_FMMC_STRUCT.sWarpPortals[iPortal].vStartCoord, g_FMMC_STRUCT.sWarpPortals[iPortal].iAttachToEntityType, g_FMMC_STRUCT.sWarpPortals[iPortal].iAttachToEntityID, IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_ForcePortalToFloor))
ENDFUNC

FUNC VECTOR MC_GET_WARP_PORTAL_START_FALLBACK_POSITION(INT iPortal, INT iPosition)
	RETURN MC_GET_POSITION_OFFSET_RELATIVE_TO_ENTITY(g_FMMC_STRUCT.sWarpPortals[iPortal].vExtraStartCoord[iPosition], g_FMMC_STRUCT.sWarpPortals[iPortal].iAttachToEntityType, g_FMMC_STRUCT.sWarpPortals[iPortal].iAttachToEntityID, IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_ForcePortalToFloor))
ENDFUNC

FUNC VECTOR MC_GET_WARP_PORTAL_END_POSITION(INT iPortal)
	INT iLinkedPortal = g_FMMC_STRUCT.sWarpPortals[iPortal].iLinkedPortal
	IF iLinkedPortal = -1
		RETURN <<0.0, 0.0, 0.0>>
	ENDIF
	RETURN MC_GET_POSITION_OFFSET_RELATIVE_TO_ENTITY(g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].vStartCoord, g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].iAttachToEntityType, g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].iAttachToEntityID, IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].iWarpPortalBS, ciWARP_PORTAL_ForcePortalToFloor))
ENDFUNC

FUNC VECTOR MC_GET_WARP_PORTAL_END_FALLBACK_POSITION(INT iPortal, INT iPosition)
	INT iLinkedPortal = g_FMMC_STRUCT.sWarpPortals[iPortal].iLinkedPortal
	IF iLinkedPortal = -1
		RETURN <<0.0, 0.0, 0.0>>
	ENDIF
	RETURN MC_GET_POSITION_OFFSET_RELATIVE_TO_ENTITY(g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].vExtraStartCoord[iPosition], g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].iAttachToEntityType, g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].iAttachToEntityID, IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].iWarpPortalBS, ciWARP_PORTAL_ForcePortalToFloor))
ENDFUNC

FUNC FLOAT MC_GET_WARP_PORTAL_START_HEADING(INT iPortal)
	RETURN MC_GET_HEADING_OFFSET_RELATIVE_TO_ENTITY(g_FMMC_STRUCT.sWarpPortals[iPortal].fStartHead, g_FMMC_STRUCT.sWarpPortals[iPortal].iAttachToEntityType, g_FMMC_STRUCT.sWarpPortals[iPortal].iAttachToEntityID)
ENDFUNC

FUNC FLOAT MC_GET_WARP_PORTAL_START_FALLBACK_HEADING(INT iPortal, INT iPosition)
	RETURN MC_GET_HEADING_OFFSET_RELATIVE_TO_ENTITY(g_FMMC_STRUCT.sWarpPortals[iPortal].fExtraStartHead[iPosition], g_FMMC_STRUCT.sWarpPortals[iPortal].iAttachToEntityType, g_FMMC_STRUCT.sWarpPortals[iPortal].iAttachToEntityID)
ENDFUNC

FUNC FLOAT MC_GET_WARP_PORTAL_END_HEADING(INT iPortal)
	INT iLinkedPortal = g_FMMC_STRUCT.sWarpPortals[iPortal].iLinkedPortal
	IF iLinkedPortal = -1		
		RETURN 0.0
	ENDIF
	RETURN MC_GET_HEADING_OFFSET_RELATIVE_TO_ENTITY(g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].fStartHead, g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].iAttachToEntityType, g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].iAttachToEntityID)
ENDFUNC

FUNC FLOAT MC_GET_WARP_PORTAL_END_FALLBACK_HEADING(INT iPortal, INT iPosition)
	INT iLinkedPortal = g_FMMC_STRUCT.sWarpPortals[iPortal].iLinkedPortal
	IF iLinkedPortal = -1		
		RETURN 0.0
	ENDIF
	RETURN MC_GET_HEADING_OFFSET_RELATIVE_TO_ENTITY(g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].fExtraStartHead[iPosition], g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].iAttachToEntityType, g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].iAttachToEntityID)
ENDFUNC

FUNC BOOL IS_THIS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_A_SYNC_SCENE(INT i)
	SWITCH i	
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_L
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_R
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_L_CHARGE		
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_ELEVATOR_BUTTON_PRESS_RIGHT
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_GARAGE_BOTH_HANDS
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_GARAGE_LEFT_HAND
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_GARAGE_RIGHT_HAND
			RETURN TRUE
		BREAK		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_ENTRY_SOUND_NAME_FROM_WARP_PORTAL_INT(INT i)
	STRING tlReturn
	
	SWITCH i
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_HINGE_L_ACTION
			tlReturn = "Barge_Door_Metal"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_L_CHARGE
			tlReturn = "Barge_Door_Metal"
		BREAK		
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_GARAGE_BOTH_HANDS
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_GARAGE_LEFT_HAND
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_GARAGE_RIGHT_HAND
			tlReturn = "Garage_Door_Open"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_ELEVATOR_BUTTON_PRESS_LEFT
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_ELEVATOR_BUTTON_PRESS_RIGHT
			tlReturn = "OPENING"
		BREAK
	ENDSWITCH
	
	RETURN tlReturn
ENDFUNC

FUNC STRING GET_ENTRY_SOUND_SET_FROM_WARP_PORTAL_INT(INT i)
	STRING tlReturn
	
	SWITCH i
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_HINGE_L_ACTION
			tlReturn = "dlc_h4_Prep_FC_Sounds"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_L_CHARGE
			tlReturn = "dlc_h4_Prep_FC_Sounds"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_GARAGE_BOTH_HANDS
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_GARAGE_LEFT_HAND
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_GARAGE_RIGHT_HAND
			tlReturn = "GTAO_Script_Doors_Faded_Screen_Sounds"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_ELEVATOR_BUTTON_PRESS_LEFT
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_ELEVATOR_BUTTON_PRESS_RIGHT
			tlReturn = "MP_PROPERTIES_ELEVATOR_DOORS"
		BREAK
	ENDSWITCH
	
	RETURN tlReturn
ENDFUNC

FUNC STRING GET_ENTRY_SOUND_NAME_END_FROM_WARP_PORTAL_INT(INT i)
	STRING tlReturn
	
	SWITCH i
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_ELEVATOR_BUTTON_PRESS_LEFT
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_ELEVATOR_BUTTON_PRESS_RIGHT
			tlReturn = "FAKE_ARRIVE"
		BREAK
	ENDSWITCH
	
	RETURN tlReturn
ENDFUNC

FUNC STRING GET_ENTRY_SOUND_SET_END_FROM_WARP_PORTAL_INT(INT i)
	STRING tlReturn
	
	SWITCH i
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_ELEVATOR_BUTTON_PRESS_LEFT
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_ELEVATOR_BUTTON_PRESS_RIGHT
			tlReturn = "MP_PROPERTIES_ELEVATOR_DOORS"
		BREAK
	ENDSWITCH
	
	RETURN tlReturn
ENDFUNC

FUNC FLOAT GET_ENTRY_SOUND_START_PHASE_FROM_WARP_PORTAL_INT(INT i)
	FLOAT fReturn = 0.0
	
	SWITCH i
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_GARAGE_BOTH_HANDS
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_GARAGE_LEFT_HAND
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_GARAGE_RIGHT_HAND
			fReturn = 0.5
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_ELEVATOR_BUTTON_PRESS_LEFT
			fReturn = 0.28
		BREAK		
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_ELEVATOR_BUTTON_PRESS_RIGHT
			fReturn = 0.8
		BREAK	
	ENDSWITCH
	
	RETURN fReturn
ENDFUNC

FUNC FLOAT GET_ENTRY_PHASE_START_FROM_WARP_PORTAL_INT(INT i)
	FLOAT fReturn = 0.0
	
	SWITCH i
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_ELEVATOR_BUTTON_PRESS_LEFT
			fReturn = 0.14
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_ELEVATOR_BUTTON_PRESS_RIGHT
			fReturn = 0.621
		BREAK
	ENDSWITCH
	
	RETURN fReturn
ENDFUNC

FUNC FLOAT GET_ENTRY_PHASE_END_FROM_WARP_PORTAL_INT(INT i)
	FLOAT fReturn = 1.0
	
	SWITCH i
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_L
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_R
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_L_CHARGE
			fReturn = 1.0
		BREAK		
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_ELEVATOR_BUTTON_PRESS_LEFT
			fReturn = 0.34
		BREAK		
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_ELEVATOR_BUTTON_PRESS_RIGHT
			fReturn = 0.9
		BREAK		
	ENDSWITCH
	
	RETURN fReturn
ENDFUNC

FUNC STRING GET_ENTRY_ANIM_DICT_FROM_WARP_PORTAL_INT(INT i, BOOL bAlternate)
	STRING tlReturn
	
	SWITCH i
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_HINGE_L
			tlReturn = "anim@apt_trans@hinge_l"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_HINGE_R
			tlReturn = "anim@apt_trans@hinge_r"
		BREAK		
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_GARAGE
			tlReturn = "anim@apt_trans@garage"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_HINGE_L_ACTION
			tlReturn = "anim@apt_trans@hinge_l_action"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_VEHICLE
			tlReturn = "Vehicle"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_L
			tlReturn = "anim@door_trans@hinge_l@"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_R
			tlReturn = "anim@door_trans@hinge_r@"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_L_CHARGE
			tlReturn = "anim@door_trans@hinge_l@"
		BREAK		
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_ELEVATOR_BUTTON_PRESS_LEFT
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_ELEVATOR_BUTTON_PRESS_RIGHT
			tlReturn = "anim@apt_trans@elevator"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_GARAGE_BOTH_HANDS
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_GARAGE_LEFT_HAND
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_GARAGE_RIGHT_HAND		
			IF bAlternate
				tlReturn = "ANIM@SCRIPTED@ULP_MISSIONS@GARAGEOPEN@HEELED@"
			ELSE
				tlReturn = "ANIM@SCRIPTED@ULP_MISSIONS@GARAGEOPEN@MALE@"
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN tlReturn
ENDFUNC

FUNC STRING GET_ENTRY_ANIM_CLIP_FROM_WARP_PORTAL_INT(INT i)
	STRING tlReturn
	
	SWITCH i
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_HINGE_L
			tlReturn = "ext_player"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_HINGE_R
			tlReturn = "ext_player"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_GARAGE
			tlReturn = "gar_open_1_left"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_HINGE_L_ACTION
			tlReturn = "player_exit"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_VEHICLE
			tlReturn = "Vehicle"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_L
			tlReturn = "walk_player1"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_R
			tlReturn = "walk_player1"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_L_CHARGE
			tlReturn = "charge_player1"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_ELEVATOR_BUTTON_PRESS_RIGHT
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_ELEVATOR_BUTTON_PRESS_LEFT
			tlReturn = "elev_1"
		BREAK		
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_GARAGE_BOTH_HANDS
			tlReturn = "KNEEL_DOWN_01"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_GARAGE_LEFT_HAND
			tlReturn = "KNEEL_DOWN_02"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_GARAGE_RIGHT_HAND
			tlReturn = "KNEEL_DOWN_03"
		BREAK
	ENDSWITCH
	
	RETURN tlReturn
ENDFUNC

FUNC STRING GET_ENTRY_ANIM_CLIP_CAMERA_FROM_WARP_PORTAL_INT(INT i, BOOL bForceLeft)
	STRING tlReturn
	
	SWITCH i		
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_L				
			IF bForceLeft
				tlReturn = "WALK_CAM_LEFT"
			ELSE
				tlReturn = "WALK_CAM_RIGHT"
			ENDIF			
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_R
			IF bForceLeft
				tlReturn = "WALK_CAM_LEFT"
			ELSE
				tlReturn = "WALK_CAM_RIGHT"
			ENDIF			
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_L_CHARGE
			IF bForceLeft
				tlReturn = "CHARGE_CAM_LEFT"
			ELSE
				tlReturn = "CHARGE_CAM_RIGHT"
			ENDIF	
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_ELEVATOR_BUTTON_PRESS_RIGHT
			tlReturn = "elev_1_cam"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_GARAGE_BOTH_HANDS
			tlReturn = "KNEEL_DOWN_01_CAM"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_GARAGE_LEFT_HAND
			tlReturn = "KNEEL_DOWN_02_CAM"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_GARAGE_RIGHT_HAND
			tlReturn = "KNEEL_DOWN_03_CAM"
		BREAK
	ENDSWITCH
	
	RETURN tlReturn
ENDFUNC

FUNC BOOL SHOULD_WARP_PORTAL_DISPLAY_AS_DISABLED(INT iPortal)
	
	IF HAS_NET_TIMER_STARTED(sWarpPortal.tdWarpPortalCooldown)
		IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(sWarpPortal.tdWarpPortalCooldown, 2000)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sWarpPortal.iJustUsedPortalStartBS, iPortal)
		RETURN TRUE
	ENDIF
			
	RETURN FALSE
ENDFUNC

PROC PROCESS_WARP_PORTAL_DISPLAY(INT iPortal, BOOL bActive, VECTOR vStartPassedIn)
	
	IF IS_VECTOR_ZERO(vStartPassedIn)
		EXIT
	ENDIF
		
	FLOAT fStartRange = GET_WARP_PORTAL_START_RANGE(iPortal)
	VECTOR vRangeStart	
	IF g_FMMC_STRUCT.sWarpPortals[iPortal].fVisualRangeOverride = -2.0
		fStartRange = 0.75 // freemode size, see: DRAW_LOCATE_FOR_THIS_SIMPLE_INTERIOR_ENTRANCE
		vRangeStart = <<fStartRange, fStartRange, fStartRange>>	
	ELIF g_FMMC_STRUCT.sWarpPortals[iPortal].fVisualRangeOverride > -1.0
		fStartRange = g_FMMC_STRUCT.sWarpPortals[iPortal].fVisualRangeOverride
		vRangeStart = <<fStartRange, fStartRange, fStartRange>>	
	ELSE
		vRangeStart = <<fStartRange*1.9, fStartRange*1.9, fStartRange*0.8>>	
	ENDIF
		
	VECTOR vStart = <<vStartPassedIn.x, vStartPassedIn.y, vStartPassedIn.z-0.15>>
	INT iBlipCol = GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT.sWarpPortals[iPortal].iColourType)
	INT iR, iG, iB, iA	
	BOOL bDisplayAsDisabled
	HUD_COLOURS colPortal
			
	IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_BlipLocation)
		IF NOT DOES_BLIP_EXIST(sWarpPortal.biStartWarpPortals[iPortal])
		AND bActive
			sWarpPortal.biStartWarpPortals[iPortal] = ADD_BLIP_FOR_COORD(vStart)
			SET_BLIP_COLOUR(sWarpPortal.biStartWarpPortals[iPortal], iBlipCol)
		ENDIF
	ENDIF
	
	bDisplayAsDisabled = SHOULD_WARP_PORTAL_DISPLAY_AS_DISABLED(iPortal)
	
	IF bDisplayAsDisabled
		colPortal = HUD_COLOUR_RED
		GET_HUD_COLOUR(colPortal, iR, iG, iB, iA)
	ELSE
		IF ENUM_TO_INT(sWarpPortal.colPortalCached[iPortal]) = -1
			sWarpPortal.colPortalCached[iPortal] = GET_HUD_COLOUR_FROM_CREATOR_BLIP_COLOUR(g_FMMC_STRUCT.sWarpPortals[iPortal].iColourType)
		ENDIF
		colPortal = sWarpPortal.colPortalCached[iPortal]
		GET_HUD_COLOUR(colPortal, iR, iG, iB, iA)
	ENDIF
	
	iA = 140
		
	IF bActive	
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_HideStartPortalMarker)		
		AND fStartRange > 0.0
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_PortalPTFX)
				DRAW_MARKER(MARKER_CYLINDER, vStart, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, vRangeStart, iR, iG, iB, iA)
				//DRAW_MARKER(MARKER_CYLINDER, vStart, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, vRangeSmall, iR, iG, iB, 40)
			ELSE
				// Make it look like a portal (WIP)
				DRAW_MARKER(MARKER_CYLINDER, vStart, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, vRangeStart, iR, iG, iB, iA)
				DRAW_MARKER(MARKER_CYLINDER, vStart+<<0.0,0.0,2.0*fStartRange>>, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, vRangeStart, iR, iG, iB, iA)
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(sWarpPortal.biStartWarpPortals[iPortal])
			REMOVE_BLIP(sWarpPortal.biStartWarpPortals[iPortal])
		ENDIF
	ENDIF
	
ENDPROC
 
PROC SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(WARP_PORTAL_ANIM_STATE eWarpPortalAnimStateNew)
	PRINTLN("[LM][wPortals][wPortal ", sWarpPortal.iWarpPortalEntered, "] - SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE - Setting Anim State New: ", ENUM_TO_INT(eWarpPortalAnimStateNew), " From Old: ", ENUM_TO_INT(sWarpPortal.eWarpPortalAnimState))
	sWarpPortal.eWarpPortalAnimState = eWarpPortalAnimStateNew	
ENDPROC

// Preload all possible vehicles so that transforming is quicker.
PROC PROCESS_PRELOADING_VEHICLE_MODEL_FOR_WARP_PORTAL(INT iPortal, BOOL bLoad)
	IF sWarpPortal.mnModelsPreloaded[iPortal] = DUMMY_MODEL_FOR_SCRIPT
		EXIT
	ENDIF
	IF NOT IS_MODEL_VALID(sWarpPortal.mnModelsPreloaded[iPortal])
		EXIT
	ENDIF
	IF bLoad
		REQUEST_MODEL(sWarpPortal.mnModelsPreloaded[iPortal])
	ELSE
		SET_MODEL_AS_NO_LONGER_NEEDED(sWarpPortal.mnModelsPreloaded[iPortal])
	ENDIF
ENDPROC

PROC INITIALISE_KEEP_MOMENTUM_FROM_WARP_PORTAL_DATA(INT iPortal)
	IF NOT IS_WARP_PORTAL_USING_TRANSFORM(iPortal)	
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS2, ciWARP_PORTAL_2_MaintainMomentumAfterWarp)
		EXIT
	ENDIF
		
	IF sWarpPortal.bKeepVehicle
	AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
	AND GET_SEAT_PED_IS_IN(localPlayerPed) != VS_DRIVER
		EXIT
	ENDIF
	
	IF IS_WARP_PORTAL_USING_TRANSFORM(iPortal)	
		sWarpPortal.sVehicleSwap = g_FMMC_STRUCT.sWarpPortals[iPortal].sVehicleSwap
	ENDIF
	
	IF GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_SKY_DIVE) = PERFORMING_TASK
	OR GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_PARACHUTE) = PERFORMING_TASK
		SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_WasSkyDivingKeepMomentum)
		PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - INITIALISE_VEHICLE_SWAP_FROM_WARP_PORTAL_DATA - Setting ciWarpPortalBS_WasSkyDivingKeepMomentum")	
	ENDIF
	
	IF IS_PED_WALKING(LocalPlayerPed)
		sWarpPortal.PMS_Cached = MS_ON_FOOT_WALK
		PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - INITIALISE_VEHICLE_SWAP_FROM_WARP_PORTAL_DATA - Setting MS_ON_FOOT_WALK")	
	ELIF IS_PED_RUNNING(LocalPlayerPed)
		sWarpPortal.PMS_Cached = MS_ON_FOOT_RUN
		PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - INITIALISE_VEHICLE_SWAP_FROM_WARP_PORTAL_DATA - Setting MS_ON_FOOT_RUN")	
	ELIF IS_PED_SPRINTING(LocalPlayerPed)
		sWarpPortal.PMS_Cached = MS_ON_FOOT_SPRINT
		PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - INITIALISE_VEHICLE_SWAP_FROM_WARP_PORTAL_DATA - Setting MS_ON_FOOT_SPRINT")	
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		VEHICLE_INDEX viVeh = GET_VEHICLE_PED_IS_IN(localPlayerPed)
		sWarpPortal.vPosition = GET_ENTITY_COORDS(viVeh)
		sWarpPortal.fHeading = GET_ENTITY_HEADING(viVeh)
		sWarpPortal.vRotation = GET_ENTITY_ROTATION(viVeh)
		sWarpPortal.vTorque = GET_ENTITY_ROTATION_VELOCITY(viVeh)
		sWarpPortal.vVelocity = GET_ENTITY_VELOCITY(viVeh)		
		PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - INITIALISE_VEHICLE_SWAP_FROM_WARP_PORTAL_DATA - Caching vVelocity in veh: ", sWarpPortal.vVelocity)
	ELSE	
		sWarpPortal.vPosition = GET_ENTITY_COORDS(localPlayerPed)
		sWarpPortal.fHeading = GET_ENTITY_HEADING(localPlayerPed)
		sWarpPortal.vRotation = GET_ENTITY_ROTATION(localPlayerPed)
		sWarpPortal.vTorque = GET_ENTITY_ROTATION_VELOCITY(localPlayerPed)
		sWarpPortal.vVelocity = GET_ENTITY_VELOCITY(localPlayerPed)		
		PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - INITIALISE_VEHICLE_SWAP_FROM_WARP_PORTAL_DATA - Caching vVelocity not in veh: ", sWarpPortal.vVelocity)
	ENDIF
ENDPROC

PROC START_VEHICLE_SWAP_FROM_WARP_PORTAL_DATA(INT iPortal)
	IF NOT IS_WARP_PORTAL_USING_TRANSFORM(iPortal)
		EXIT
	ENDIF	
	IF IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_TriggeredVehicleTransform)
		EXIT
	ENDIF
		
	SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_TriggeredVehicleTransform)		
	PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - START_VEHICLE_SWAP_FROM_WARP_PORTAL_DATA - Setting ciWarpPortalBS_TriggeredVehicleTransform")
ENDPROC

PROC WARP_PORTAL_APPLY_PREVIOUS_MOMENTUM(INT iPortal)	
		
	IF NOT IS_WARP_PORTAL_USING_TRANSFORM(iPortal)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS2, ciWARP_PORTAL_2_MaintainMomentumAfterWarp)
		EXIT
	ENDIF
	
	IF sWarpPortal.bKeepVehicle
	AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
	AND GET_SEAT_PED_IS_IN(localPlayerPed) != VS_DRIVER
		EXIT
	ENDIF
		
	IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(localPlayerPed)
		MODEL_NAMES mnVeh = GET_ENTITY_MODEL(vehIndex)
		
		IF NOT DOES_ENTITY_EXIST(vehIndex)
		OR NOT IS_ENTITY_ALIVE(vehIndex)		
			EXIT
		ENDIF
		
		BOOL bPlane = IS_THIS_MODEL_A_PLANE(mnVeh)

		IF bPlane
			//Planes need a "slight" boost
			SET_VEHICLE_FORWARD_SPEED(vehIndex, 30)		
		ELSE
			SET_VEHICLE_FORWARD_SPEED(vehIndex, 1)		
		ENDIF
		
		PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - WARP_PORTAL_APPLY_PREVIOUS_MOMENTUM - FORCE Velocity: ", sWarpPortal.vVelocity)
		
		SET_PED_RESET_FLAG(localPlayerPed, PRF_PreventGoingIntoStillInVehicleState, TRUE)
		
		#IF IS_DEBUG_BUILD
			VECTOR vOldVel = GET_ENTITY_VELOCITY(vehIndex)
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - WARP_PORTAL_APPLY_PREVIOUS_MOMENTUM - Pre Velocity Change: ", vOldVel)
		#ENDIF
		
		sWarpPortal.vVelocity = ROTATE_VECTOR_ABOUT_Z(sWarpPortal.vVelocity, GET_ENTITY_HEADING(vehIndex) - sWarpPortal.vRotation.z)
		
		SET_ENTITY_VELOCITY(vehIndex, sWarpPortal.vVelocity)
		#IF IS_DEBUG_BUILD
			VECTOR vNewVel = GET_ENTITY_VELOCITY(vehIndex)
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - WARP_PORTAL_APPLY_PREVIOUS_MOMENTUM - Post Velocity Change: ", vNewVel)
		#ENDIF		
		
		IF NOT IS_THIS_MODEL_A_PLANE(mnVeh) //Rotational force from air vehicles seem to be very severe
		AND NOT IS_THIS_MODEL_A_HELI(mnVeh)
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - WARP_PORTAL_APPLY_PREVIOUS_MOMENTUM - FORCE Pre Torque: ", sWarpPortal.vTorque)
			CONSTRAIN_TRANSFORMATION_TORQUE(sWarpPortal.vTorque)
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - WARP_PORTAL_APPLY_PREVIOUS_MOMENTUM - FORCE Post Torque: ", sWarpPortal.vTorque)
			APPLY_FORCE_TO_ENTITY(vehIndex, APPLY_TYPE_ANGULAR_IMPULSE, sWarpPortal.vTorque, <<0,0,0>>, 0 , TRUE, TRUE, TRUE)
		ENDIF
	ELSE
		CLEAN_UP_PLAYER_PED_PARACHUTE()
		SET_PED_RESET_FLAG(LocalPlayerPed, PRF_SuppressInAirEvent, TRUE)
		CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
		SET_PED_USING_ACTION_MODE(LocalPlayerPed, TRUE)
		SET_ENTITY_ROTATION(LocalPlayerPed, sWarpPortal.vRotation)				
		ACTIVATE_PHYSICS(LocalPlayerPed)
		
		SET_ENTITY_VELOCITY(LocalPlayerPed, sWarpPortal.vVelocity)
		
		PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - WARP_PORTAL_APPLY_PREVIOUS_MOMENTUM - Setting sWarpPortal.vVelocity: ", sWarpPortal.vVelocity)
		
		IF IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_WasSkyDivingKeepMomentum)		
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - WARP_PORTAL_APPLY_PREVIOUS_MOMENTUM - Sky Dive")
			TASK_SKY_DIVE(LocalPlayerPed, TRUE)
		ELSE
			IF sWarpPortal.vVelocity.z > 1.0
				SET_PED_TO_RAGDOLL(LocalPlayerPed, 0, 1000, TASK_NM_BALANCE)
			ELSE
				FORCE_PED_MOTION_STATE(LocalPlayerPed, sWarpPortal.PMS_Cached)
			ENDIF		
		ENDIF
					
		FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
	ENDIF
	
ENDPROC

FUNC BOOL HAS_WARP_PORTAL_SYNC_STAGE_FINISHED(INT iPortal)
	
	IF g_FMMC_STRUCT.sWarpPortals[iPortal].sVehicleSwap.mnVehicleModelSwap != DUMMY_MODEL_FOR_SCRIPT
	AND sVehicleSwap.eVehicleSwapState != eVehicleSwapState_IDLE
		PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_WARP_PORTAL_SYNC_STAGE_FINISHED - Waiting for VehSwap.")
		RETURN FALSE		
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_DoNoFadeInScreenAfterUse)
		PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_WARP_PORTAL_SYNC_STAGE_FINISHED - Set to ignore Fadein.")
		RETURN TRUE
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS2, ciWARP_PORTAL_2_WhiteOutBeforeWarp)
			IF IS_RECTANGLE_FADED_IN()
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_WARP_PORTAL_SYNC_STAGE_FINISHED - Undoing Whiteout ...")
				SET_RECTANGLE_COLOUR(TRUE)
				ACTIVATE_RECTANGLE_FADE_OUT(TRUE)
				ACTIVATE_RECTANGLE_FADE_IN(FALSE)
				SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_PlayedWhiteout)
				RETURN FALSE
			ENDIF
			IF IS_RECTANGLE_FADING_OUT()
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF IS_SCREEN_FADED_OUT()
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_WARP_PORTAL_SYNC_STAGE_FINISHED - Fading Screen Back In.")
			DO_SCREEN_FADE_IN(1000)
		ENDIF				
		IF IS_SCREEN_FADED_IN()
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_WARP_PORTAL_SYNC_STAGE_FINISHED - Screen Faded Back In.")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_MOVE_WARP_PORTAL_VEHICLE_TOWARD_DESTINATION(INT iPortal)
	
	IF NOT DOES_ENTITY_EXIST(sWarpPortal.vehEntryAnimClone)
	OR NOT IS_VEHICLE_DRIVEABLE(sWarpPortal.vehEntryAnimClone)
		RETURN FALSE
	ENDIF
	
	FLOAT fAmountToMove = 0.0035
	FLOAT fModifier = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sWarpPortal.tdCamAnimTimer)) / 500.0
	fModifier += 1.0
	fAmountToMove *= fModifier	
	IF fAmountToMove > 0.125
		fAmountToMove  = 0.125
	ENDIF
	
	PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - PROCESS_MOVE_WARP_PORTAL_VEHICLE_TOWARD_DESTINATION - fAmountToMove: ", fAmountToMove, " fModifier: ", fModifier, " Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sWarpPortal.tdCamAnimTimer))
		
	VECTOR vCoord = GET_ENTITY_COORDS(sWarpPortal.vehEntryAnimClone)
	FLOAT fHead = GET_ENTITY_HEADING(sWarpPortal.vehEntryAnimClone)
	VECTOR vNewCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoord, fHead, <<0.0, fAmountToMove, 0.0>>)
	FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord, GET_ENTITY_COORDS(sWarpPortal.vehEntryAnimClone), FALSE)
	
	PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - PROCESS_MOVE_WARP_PORTAL_VEHICLE_TOWARD_DESTINATION - vCoord: ", vCoord, " fHead: ", fHead, " vNewCoord: ", vNewCoord, " fDist: ", fDist)
		
	SET_ENTITY_COORDS_NO_OFFSET(sWarpPortal.vehEntryAnimClone, vNewCoord, FALSE, FALSE, TRUE)
	FREEZE_ENTITY_POSITION(sWarpPortal.vehEntryAnimClone, TRUE)
	
	IF fDist >= 15.0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_WARP_PORTAL_CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS(INT iPortal, VEHICLE_INDEX &vehToCreate, VEHICLE_INDEX vehToClone, VEHICLE_INDEX &vehTrailerToCreate)
	
	IF DOES_ENTITY_EXIST(vehToCreate)
		
		IF NOT CUTSCENE_HELP_VEH_PASSENGERS_CLONE(vehToClone, vehToCreate, sWarpPortal.pedEntryAnimClones, FALSE, TRUE, FALSE)
			PRINTLN("[LM][CUTSCENE_HELP][wPortals][wPortal ", iPortal, "] - PROCESS_WARP_PORTAL_CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - Passengers not created yet.")
			RETURN FALSE
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehToClone)
			IF IS_ENTITY_ALIVE(vehToClone)
				VEHICLE_INDEX vehTrailer
				GET_VEHICLE_TRAILER_VEHICLE(vehToClone, vehTrailer)
				IF DOES_ENTITY_EXIST(vehTrailer)
					IF NOT CUTSCENE_HELP_CLONE_VEH_TRAILER(vehToClone, vehTrailerToCreate)
						PRINTLN("[LM][CUTSCENE_HELP][wPortals][wPortal ", iPortal, "] - PROCESS_WARP_PORTAL_CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - Trailer not created yet")
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		RETURN TRUE
	ELSE
		IF CREATE_VEHICLE_CLONE(vehToCreate, vehToClone, g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord, g_FMMC_STRUCT.sWarpPortals[iPortal].fEntryAnimHead, FALSE, FALSE)
			PRINTLN("[LM][CUTSCENE_HELP][wPortals][wPortal ", iPortal, "] -  PROCESS_WARP_PORTAL_CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - Delivery vehicle clone created succesfully")
			
			IF DOES_ENTITY_EXIST(vehToCreate)
				PRINTLN("[LM][CUTSCENE_HELP][wPortals][wPortal ", iPortal, "] -  PROCESS_WARP_PORTAL_CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - Vehicle exists.")
				//Clone vehicle mods.
				VEHICLE_SETUP_STRUCT_MP vehicleSetup
				GET_VEHICLE_SETUP_MP(vehToClone, vehicleSetup)
				SET_VEHICLE_SETUP_MP(vehToCreate, vehicleSetup)
				//clone extras
				VEHICLE_COPY_EXTRAS(vehToClone, vehToCreate)
				
				SET_ENTITY_COLLISION(vehToCreate, TRUE)
				SET_ENTITY_VISIBLE(vehToCreate, FALSE)
				SET_ENTITY_INVINCIBLE(vehToCreate, TRUE)
				SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehToCreate, FALSE)
				FREEZE_ENTITY_POSITION(vehToCreate, FALSE)
				
				MODEL_NAMES eModel = GET_ENTITY_MODEL(vehToCreate)

				IF NOT IS_THIS_MODEL_A_BIKE(eModel)
				AND NOT IS_THIS_MODEL_A_BICYCLE(eModel)
				AND NOT IS_THIS_MODEL_A_BOAT(eModel)
				AND NOT IS_THIS_MODEL_A_SEA_VEHICLE(eModel)
				AND NOT (eModel = OPPRESSOR)
					SET_VEHICLE_CAN_BREAK(vehToCreate, FALSE)
					PRINTLN("[LM][CUTSCENE_HELP][wPortals][wPortal ", iPortal, "] - PROCESS_WARP_PORTAL_CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - Preventing vehicle from being able to break apart.")
				ELSE
					PRINTLN("[LM][CUTSCENE_HELP][wPortals][wPortal ", iPortal, "] - PROCESS_WARP_PORTAL_CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - Vehicle type cannot be set as breakable.")
				ENDIF
				
				//LIGHTS
				INT iOn, iFullBeam
				GET_VEHICLE_LIGHTS_STATE(vehToClone, iOn, iFullBeam)
				PRINTLN("[LM][CUTSCENE_HELP][wPortals][wPortal ", iPortal, "] - PROCESS_WARP_PORTAL_CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - On: ", iOn, " FullBeam: ", iFullBeam)
				IF iOn <> 0
					SET_VEHICLE_LIGHTS(vehToCreate, FORCE_VEHICLE_LIGHTS_ON)
				ENDIF
				
				IF iFullBeam <> 0
					SET_VEHICLE_FULLBEAM(vehToCreate, TRUE)
				ENDIF
				
				SET_VEHICLE_ENGINE_ON(vehToCreate, TRUE, TRUE)
								
				/*INT iWheel
				FOR iWheel = SC_WHEEL_CAR_FRONT_LEFT TO SC_WHEEL_BIKE_REAR
					IF IS_VEHICLE_TYRE_BURST(vehCloneStruct.vehicle, SC_WHEEL_CAR_FRONT_LEFT)
						SET_VEHICLE_TYRE_FIXED(vehCloneStruct.vehicle, INT_TO_ENUM(SC_WHEEL_LIST, iWheel))
					ENDIF
				ENDFOR*/
				
				/*INT iWheel
				FOR iWheel = SC_WHEEL_CAR_FRONT_LEFT TO SC_WHEEL_BIKE_REAR
					IF IS_VEHICLE_TYRE_BURST(vehCloneStruct.vehicle, SC_WHEEL_CAR_FRONT_LEFT)
						SET_VEHICLE_TYRE_FIXED(vehCloneStruct.vehicle, INT_TO_ENUM(SC_WHEEL_LIST, iWheel))
					ENDIF
				ENDFOR*/
			ENDIF
		ELSE
			PRINTLN("[CUTSCENE_HELP][wPortals][wPortal ", iPortal, "] - PROCESS_WARP_PORTAL_CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - Unable to create Player vehicle clone.")
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC PROCESS_WARP_PORTAL_SAFETY_TIMER()
	IF NOT HAS_NET_TIMER_STARTED(sWarpPortal.tdCamAnimTimerSafety)
		PRINTLN("[LM][wPortals] - PROCESS_WARP_PORTAL_SAFETY_TIMER - Starting Safety Timer.")
		START_NET_TIMER(sWarpPortal.tdCamAnimTimerSafety)
	ENDIF
	IF HAS_NET_TIMER_EXPIRED_READ_ONLY(sWarpPortal.tdCamAnimTimerSafety, 8000)
		PRINTLN("[LM][wPortals] - PROCESS_WARP_PORTAL_SAFETY_TIMER - Safety Timer ran out.")
		SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Cleanup)
	ENDIF
ENDPROC

PROC PROCESS_WARP_PORTAL_HIDE_OTHER_PLAYERS(BOOL bHide)
	INT iPart = -1
	DO_PARTICIPANT_LOOP_FLAGS eFlags = DPLF_CHECK_PED_ALIVE | DPLF_EXCLUDE_LOCAL_PART | DPLF_CHECK_PLAYER_OK | DPLF_FILL_PLAYER_IDS
	WHILE DO_PARTICIPANT_LOOP(iPart, eFlags)
		IF iPart = iLocalPart
			RELOOP
		ENDIF
		
		IF bHide
			PRINTLN("[LM][wPortals] - PROCESS_WARP_PORTAL_HIDE_OTHER_PLAYERS - iPart: ", iPart, " Setting invisible locally during scene. (", GET_PLAYER_NAME(piParticipantLoop_PlayerIndex), ")")
			SET_PLAYER_INVISIBLE_LOCALLY(piParticipantLoop_PlayerIndex)
			SET_ENTITY_LOCALLY_INVISIBLE(piParticipantLoop_PedIndex)
		ELSE			
			PRINTLN("[LM][wPortals] - PROCESS_WARP_PORTAL_HIDE_OTHER_PLAYERS - iPart: ", iPart, " Setting visible locally, cleaning up. (", GET_PLAYER_NAME(piParticipantLoop_PlayerIndex), ")")
			SET_PLAYER_VISIBLE_LOCALLY(piParticipantLoop_PlayerIndex)
			SET_ENTITY_LOCALLY_VISIBLE(piParticipantLoop_PedIndex)
		ENDIF
	ENDWHILE	
ENDPROC

PROC PROCESS_WARP_PORTAL_HIDE_ENTITIES_IN_AREA(VECTOR vCentre, FLOAT fRadius)
	IF IS_VECTOR_ZERO(vCentre)
		EXIT
	ENDIF
	
	VECTOR vCurrentPos
	VEHICLE_INDEX currentVeh	
	VEHICLE_INDEX vehPedIsIn
	
	IF NOT IS_PED_INJURED(localPlayerPed)
	AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		vehPedIsIn = GET_VEHICLE_PED_IS_IN(localPlayerPed)
	ENDIF
	
	INT iNumVehicles
	INT i
	
	iNumVehicles = GET_ALL_VEHICLES(g_PoolVehicles)

	FOR i = 0 TO iNumVehicles-1
		
		currentVeh = g_PoolVehicles[i]
		
		IF currentVeh = vehPedIsIn
			RELOOP
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(currentVeh)
			RELOOP
		ENDIF
		
		IF IS_ENTITY_A_MISSION_ENTITY(currentVeh)
			RELOOP
		ENDIF
		
		vCurrentPos = GET_ENTITY_COORDS(currentVeh, FALSE)
		
		IF GET_DISTANCE_BETWEEN_COORDS(vCurrentPos, vCentre) > fRadius
			RELOOP
		ENDIF	
		
		SET_ENTITY_LOCALLY_INVISIBLE(currentVeh)
		
	ENDFOR
	
	FOR i = 0 TO FMMC_MAX_VEHICLES-1

		IF currentVeh = vehPedIsIn
			RELOOP
		ENDIF
		
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
			RELOOP
		ENDIF
		
		currentVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
		
		IF NOT DOES_ENTITY_EXIST(currentVeh)
			RELOOP
		ENDIF
		
		vCurrentPos = GET_ENTITY_COORDS(currentVeh, FALSE)
		
		IF GET_DISTANCE_BETWEEN_COORDS(vCurrentPos, vCentre) > fRadius
			RELOOP
		ENDIF
		
		SET_ENTITY_LOCALLY_INVISIBLE(currentVeh)
		
	ENDFOR	
		
	PROCESS_HIDE_PERSONAL_VEHICLES_LOCALLY_FOR_PLAYER()
ENDPROC

FUNC BOOL HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED(INT iPortal)
	STRING sAnimDict = GET_ENTRY_ANIM_DICT_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType, IS_PED_WEARING_HIGH_HEELS(localPlayerPed))
	STRING sAnimName = GET_ENTRY_ANIM_CLIP_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType)
	STRING sCamAnimName = GET_ENTRY_ANIM_CLIP_CAMERA_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType, IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_CamForceLeftAnim))
	STRING sSoundName = GET_ENTRY_SOUND_NAME_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType)
	STRING sSoundSet = GET_ENTRY_SOUND_SET_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType)	
	FLOAT fSoundStartPhase = GET_ENTRY_SOUND_START_PHASE_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType)	
	FLOAT fCustomPhaseStart = GET_ENTRY_PHASE_START_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType)
	FLOAT fCustomPhaseEnd = GET_ENTRY_PHASE_END_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType)
	FLOAT fAnimHead = g_FMMC_STRUCT.sWarpPortals[iPortal].fEntryAnimHead
	INT iThisLocalSceneID = -1
	BOOL bProgress
	FLOAT fCurrentPhase
	FLOAT fPhaseEnd	
					
	SWITCH sWarpPortal.eWarpPortalAnimState
		CASE eWarpPortalAnimState_Init						
			IF IS_THIS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_A_SYNC_SCENE(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType)
				SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Init_SyncScene)
			ELIF NOT IS_STRING_NULL_OR_EMPTY(sAnimDict)
			AND ARE_STRINGS_EQUAL(sAnimDict, "Vehicle")
				SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Init_Vehicle)
			ELIF NOT IS_STRING_NULL_OR_EMPTY(sAnimDict)
				SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Init_Anim)
			ELSE
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - No Anim has been selected. Returning TRUE straight away.")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE eWarpPortalAnimState_Init_Anim				
			PROCESS_WARP_PORTAL_SAFETY_TIMER()
						
			REQUEST_ANIM_DICT(sAnimDict)
			IF NOT HAS_ANIM_DICT_LOADED(sAnimDict)
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Waiting for the Anim Dict to be loaded: ", sAnimDict)
				RETURN FALSE
			ENDIF
			
			IF CUTSCENE_HELP_CLONE_PLAYER(sWarpPortal.pedEntryAnimClone, LocalPlayer, FALSE, TRUE)
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Clone has been created (1).")				
				SET_ENTITY_VISIBLE(sWarpPortal.pedEntryAnimClone, TRUE)
				SET_ENTITY_ALWAYS_PRERENDER(sWarpPortal.pedEntryAnimClone, TRUE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(sWarpPortal.pedEntryAnimClone)
			AND NOT IS_PED_INJURED(sWarpPortal.pedEntryAnimClone)
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Clone has been created (2).")
			ELSE
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Waiting for Clone to be created")
				RETURN FALSE
			ENDIF
			
			IF NOT DOES_CAM_EXIST(sWarpPortal.camWarpPortal)
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Creating Camera for Entry Anim at Coord: ", g_FMMC_STRUCT.sWarpPortals[iPortal].vCamAnimCoord, " With Rotation: ", g_FMMC_STRUCT.sWarpPortals[iPortal].vCamAnimRot)
								
				CAMERA_TYPE eCamType
			
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_ForceAnimatedCamera)
					eCamType = CAMTYPE_SCRIPTED					
				ELSE
					IF NOT IS_STRING_NULL_OR_EMPTY(sCamAnimName)
						eCamType = CAMTYPE_ANIMATED
					ELSE
						#IF IS_DEBUG_BUILD
						ASSERTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - There is no Camera Animation associated with this kind of Entry/Exit. Falling back to Creator Placed Camera.")	
						ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_WARP_PORTAL_MISSING_CAM_ANIM, ENTITY_RUNTIME_ERROR_TYPE_WARNING_WARP_PORTAL, "Warp Portal does not have a Camera Anim for this Exit/Enter", iPortal)
						#ENDIF
					ENDIF
				ENDIF
				
				sWarpPortal.camWarpPortal = CREATE_CAMERA_WITH_PARAMS(eCamType, g_FMMC_STRUCT.sWarpPortals[iPortal].vCamAnimCoord, g_FMMC_STRUCT.sWarpPortals[iPortal].vCamAnimRot, g_FMMC_STRUCT.sWarpPortals[iPortal].fCamAnimFOV)
			ELSE
				#IF IS_DEBUG_BUILD
					VECTOR vLoc
					vLoc = GET_ENTITY_COORDS(sWarpPortal.pedEntryAnimClone)
				#ENDIF
				IF NOT IS_ENTITY_AT_COORD(sWarpPortal.pedEntryAnimClone, g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord, (<<0.8, 0.8, 2.0>>), FALSE, TRUE)	
				OR NOT HAS_PED_HEAD_BLEND_FINISHED(sWarpPortal.pedEntryAnimClone)
					PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Waiting for CLONE ped to be at AnimCoord: ", g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord, " Current Loc: ", vLoc)
					
					SET_ENTITY_COORDS(sWarpPortal.pedEntryAnimClone, g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord, FALSE, FALSE, FALSE, FALSE)
					SET_ENTITY_HEADING(sWarpPortal.pedEntryAnimClone, fAnimHead)
					RETURN FALSE
				ELSE
					PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - CLONE ped is now at the AnimCoord: ", g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord, " Current Loc: ", vLoc)
				ENDIF
				
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Setting camera as active.")
				SET_CAM_ACTIVE(sWarpPortal.camWarpPortal, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_ForceAnimatedCamera)
					PLAY_CAM_ANIM(sWarpPortal.camWarpPortal, sCamAnimName, sAnimDict, g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord, <<0.0, 0.0, fAnimHead>>) // Coord and Heading are Origin of the "scene".
				ENDIF
				
				IF NOT IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_FadedPlayerOut)
					PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Calling NETWORK_FADE_OUT_ENTITY on local player.")
					SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_FadedPlayerOut)
					SET_ENTITY_VISIBLE(LocalPlayerPed, FALSE)
					SET_ENTITY_ALPHA(LocalPlayerPed, 0, FALSE)
				ENDIF
			ENDIF
			
			IF DOES_CAM_EXIST(sWarpPortal.camWarpPortal)
			AND IS_CAM_ACTIVE(sWarpPortal.camWarpPortal)
			AND IS_CAM_RENDERING(sWarpPortal.camWarpPortal)
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Starting Anim Timer.")
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - sAnimName: ", sAnimName, " - sAnimDict: ", sAnimDict, " - sSoundName: ", sSoundName, " - sSoundSet: ", sSoundSet, " fSoundStartPhase: ", fSoundStartPhase)
				
				SET_ENTITY_COLLISION(sWarpPortal.pedEntryAnimClone, TRUE)
				FREEZE_ENTITY_POSITION(sWarpPortal.pedEntryAnimClone, FALSE)
				
				ANIM_DATA sAnimDataNone
				ANIM_DATA sAnimDataBlend
				INT iAnimFlags
				sAnimDataBlend.type = APT_SINGLE_ANIM
				sAnimDataBlend.anim0 = sAnimName
				sAnimDataBlend.dictionary0 = sAnimDict
				sAnimDataBlend.phase0 = fCustomPhaseStart
				sAnimDataBlend.rate0 = 1.0
				iAnimFlags = 0
				iAnimFlags += ENUM_TO_INT(AF_DEFAULT)
				iAnimFlags += ENUM_TO_INT(AF_OVERRIDE_PHYSICS)
				iAnimFlags += ENUM_TO_INT(AF_HOLD_LAST_FRAME)
				sAnimDataBlend.flags = INT_TO_ENUM(ANIMATION_FLAGS, iAnimFlags)	
				TASK_SCRIPTED_ANIMATION(sWarpPortal.pedEntryAnimClone, sAnimDataBlend, sAnimDataNone, sAnimDataNone, 0.15, 0.15)			
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(sWarpPortal.pedEntryAnimClone)
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sSoundName)
				AND NOT IS_STRING_NULL_OR_EMPTY(sSoundSet)
				AND fSoundStartPhase = 0.0
					PLAY_SOUND_FRONTEND(-1, sSoundName, sSoundSet)
					SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_PlayedSFX)
				ENDIF
				
				RESET_NET_TIMER(sWarpPortal.tdCamAnimTimer)
				SHAKE_CAM(sWarpPortal.camWarpPortal, "HAND_SHAKE", 0.35)				
				SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Run_Anim)			
			ENDIF	
		BREAK
		
		CASE eWarpPortalAnimState_Run_Anim
			IF NOT IS_PED_INJURED(sWarpPortal.pedEntryAnimClone)
				IF IS_ENTITY_PLAYING_ANIM(sWarpPortal.pedEntryAnimClone, sAnimDict, sAnimName)
				AND GET_ENTITY_ANIM_CURRENT_TIME(sWarpPortal.pedEntryAnimClone, sAnimDict, sAnimName) > 0.05
					IF NOT HAS_NET_TIMER_STARTED(sWarpPortal.tdCamAnimTimer)
						START_NET_TIMER(sWarpPortal.tdCamAnimTimer)
						DO_SCREEN_FADE_IN(300)
						SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationStarted)
						PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Starting the Camera Script Timer")
					ENDIF
				ELSE
					PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Waiting for Anim to start running.")
				ENDIF
			ELSE
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Ped dead/doesn't exist, emergancy cleanup.")
				SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Cleanup)
				RETURN FALSE
			ENDIF
			
			IF IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationStarted)
				
				IF IS_ENTITY_PLAYING_ANIM(sWarpPortal.pedEntryAnimClone, sAnimDict, sAnimName)
					fCurrentPhase = GET_ENTITY_ANIM_CURRENT_TIME(sWarpPortal.pedEntryAnimClone, sAnimDict, sAnimName)
				ELSE
					fCurrentPhase = fPhaseEnd
				ENDIF
				fPhaseEnd = fCustomPhaseEnd

				IF fCurrentPhase * (GET_ANIM_DURATION(sAnimDict, sAnimName)*1000) >= ((GET_ANIM_DURATION(sAnimDict, sAnimName)*1000) - 400)
					bProgress = TRUE
					PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Finishing slightly early for fadeout.")
				ENDIF
				IF fCurrentPhase >= fPhaseEnd
					bProgress = TRUE
					PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Anim has finished.")
				ENDIF
					
				IF NOT IS_STRING_NULL_OR_EMPTY(sSoundName)
				AND NOT IS_STRING_NULL_OR_EMPTY(sSoundSet)
				AND fCurrentPhase >= fSoundStartPhase
				AND NOT IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_PlayedSFX)
					PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Playing Entry SFX. sSoundName: ", sSoundName, " sSoundSet: ", sSoundSet, " fSoundStartPhase: ", fSoundStartPhase)
					PLAY_SOUND_FRONTEND(-1, sSoundName, sSoundSet)
					SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_PlayedSFX)
				ENDIF
				
				IF HAS_NET_TIMER_EXPIRED_READ_ONLY(sWarpPortal.tdCamAnimTimer, 5000)
				OR NOT IS_ENTITY_ALIVE(sWarpPortal.pedEntryAnimClone)
				OR bProgress
					IF IS_SCREEN_FADED_IN()
					AND NOT IS_SCREEN_FADING_OUT()
						PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Anim Timer Expired. Fading out the screen so we can clean up.")
						DO_SCREEN_FADE_OUT(500)
						SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationFinished)
					ELSE
						PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Waiting for screen fadeout.")
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationFinished)
				AND IS_SCREEN_FADED_OUT()
					PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Anim Timer Expired and Fadeout done. Sending to Cleanup.")
					SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Cleanup)
				ENDIF			
			ELSE
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Waiting for Camera Script Timer to start.")
			ENDIF
		BREAK
		
		CASE eWarpPortalAnimState_Init_Vehicle
			PROCESS_WARP_PORTAL_SAFETY_TIMER()
			
			IF NOT DOES_ENTITY_EXIST(sWarpPortal.vehEntryAnimOriginal)
				sWarpPortal.vehEntryAnimOriginal = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			ENDIF
			
			// Clone Vehicle
			IF NOT IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationVehCreated)
				IF PROCESS_WARP_PORTAL_CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS(iPortal, sWarpPortal.vehEntryAnimClone, sWarpPortal.vehEntryAnimOriginal, sWarpPortal.vehEntryAnimCloneTrailer)
					FREEZE_ENTITY_POSITION(sWarpPortal.vehEntryAnimClone, TRUE)
					IF DOES_ENTITY_EXIST(sWarpPortal.vehEntryAnimCloneTrailer)
					AND NOT IS_ENTITY_DEAD(sWarpPortal.vehEntryAnimCloneTrailer)
						ATTACH_VEHICLE_TO_TRAILER(sWarpPortal.vehEntryAnimClone, sWarpPortal.vehEntryAnimCloneTrailer)
					ENDIF
					
					SET_ENTITY_ALPHA(sWarpPortal.vehEntryAnimOriginal, 0, false)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(sWarpPortal.vehEntryAnimOriginal)
						FREEZE_ENTITY_POSITION(sWarpPortal.vehEntryAnimOriginal, TRUE)
						SET_ENTITY_COLLISION(sWarpPortal.vehEntryAnimOriginal, FALSE)
						SET_ENTITY_VISIBLE(sWarpPortal.vehEntryAnimOriginal, FALSE)
					ENDIF
					
					SET_VEHICLE_ON_GROUND_PROPERLY(sWarpPortal.vehEntryAnimClone)					
					
					#IF IS_DEBUG_BUILD 
					MODEL_NAMES vehModel
					VECTOR vCoordToCreate
					FLOAT fHeadToCreate	
					vCoordToCreate = g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord
					fHeadToCreate = fAnimHead
					vehModel = GET_ENTITY_MODEL(sWarpPortal.vehEntryAnimOriginal) #ENDIF
					PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Creating Clone Vehicle at: ", vCoordToCreate, " Heading: ", fHeadToCreate, " Model: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(vehModel))
					
					SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationVehCreated)
				ENDIF
				RETURN FALSE
			ENDIF
							
			IF NOT DOES_CAM_EXIST(sWarpPortal.camWarpPortal)
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Creating Camera for Entry Anim at Coord: ", g_FMMC_STRUCT.sWarpPortals[iPortal].vCamAnimCoord, " With Rotation: ", g_FMMC_STRUCT.sWarpPortals[iPortal].vCamAnimRot)
				sWarpPortal.camWarpPortal = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, g_FMMC_STRUCT.sWarpPortals[iPortal].vCamAnimCoord, g_FMMC_STRUCT.sWarpPortals[iPortal].vCamAnimRot, g_FMMC_STRUCT.sWarpPortals[iPortal].fCamAnimFOV)
			ELSE
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Setting camera as active.")
				SET_CAM_ACTIVE(sWarpPortal.camWarpPortal, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				IF NOT IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_FadedPlayerOut)
					PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Calling NETWORK_FADE_OUT_ENTITY on local player.")
					SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_FadedPlayerOut)
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
					SET_ENTITY_ALPHA(PLAYER_PED_ID(), 0, FALSE)
				ENDIF
			ENDIF
			
			IF DOES_CAM_EXIST(sWarpPortal.camWarpPortal)
			AND IS_CAM_ACTIVE(sWarpPortal.camWarpPortal)
			AND IS_CAM_RENDERING(sWarpPortal.camWarpPortal)
			AND IS_ENTITY_ALIVE(sWarpPortal.vehEntryAnimClone)
			AND IS_VEHICLE_ON_ALL_WHEELS(sWarpPortal.vehEntryAnimClone)
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Starting Anim Timer.")
				
				SET_ENTITY_COLLISION(sWarpPortal.vehEntryAnimClone, FALSE)
				FREEZE_ENTITY_POSITION(sWarpPortal.vehEntryAnimClone, TRUE)
				RESET_ENTITY_ALPHA(sWarpPortal.vehEntryAnimClone)
				SET_ENTITY_VISIBLE(sWarpPortal.vehEntryAnimClone, TRUE)
				
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Starting Anim Timer.")
				
				RESET_NET_TIMER(sWarpPortal.tdCamAnimTimer)
				SHAKE_CAM(sWarpPortal.camWarpPortal, "HAND_SHAKE", 0.35)
				SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Run_Vehicle)
			ENDIF
		BREAK
		
		CASE eWarpPortalAnimState_Run_Vehicle
			IF DOES_ENTITY_EXIST(sWarpPortal.vehEntryAnimClone)
				IF NOT HAS_NET_TIMER_STARTED(sWarpPortal.tdCamAnimTimer)
					START_NET_TIMER(sWarpPortal.tdCamAnimTimer)
					DO_SCREEN_FADE_IN(300)
					SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationStarted)
					PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Starting the Camera Script Timer")
				ENDIF
			ELSE
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Veh dead/doesn't exist, emergancy cleanup.")
				SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Cleanup)
				RETURN FALSE
			ENDIF
			
			IF IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationStarted)
				IF HAS_NET_TIMER_EXPIRED_READ_ONLY(sWarpPortal.tdCamAnimTimer, 8000)
				OR (PROCESS_MOVE_WARP_PORTAL_VEHICLE_TOWARD_DESTINATION(iPortal) AND HAS_NET_TIMER_EXPIRED_READ_ONLY(sWarpPortal.tdCamAnimTimer, 3000))
					IF IS_SCREEN_FADED_IN()
					AND NOT IS_SCREEN_FADING_OUT()
						PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Anim Timer Expired. Fading out the screen so we can clean up.")
						DO_SCREEN_FADE_OUT(500)
						SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationFinished)
					ELSE
						PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Waiting for screen fadeout.")
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationFinished)
				AND IS_SCREEN_FADED_OUT()
					PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Anim Timer Expired and Fadeout done. Sending to Cleanup.")
					SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Cleanup)
				ENDIF			
			ELSE
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Waiting for Camera Script Timer to start.")
			ENDIF
		BREAK
		
		CASE eWarpPortalAnimState_Init_SyncScene
			PROCESS_WARP_PORTAL_SAFETY_TIMER()
						
			REQUEST_ANIM_DICT(sAnimDict)
			IF NOT HAS_ANIM_DICT_LOADED(sAnimDict)
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Waiting for the Anim Dict to be loaded: ", sAnimDict)
				RETURN FALSE
			ENDIF
			
			sWarpPortal.pedEntryAnimClone = LocalPlayerPed
			
			IF DOES_ENTITY_EXIST(sWarpPortal.pedEntryAnimClone)
			AND NOT IS_PED_INJURED(sWarpPortal.pedEntryAnimClone)
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Player Ped is ok.")
			ELSE
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Waiting for Player Ped.")
				RETURN FALSE
			ENDIF
			
			IF NOT HAS_PED_HEAD_BLEND_FINISHED(sWarpPortal.pedEntryAnimClone)
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Waiting for Ped Head Blend to finish.")
				RETURN FALSE	
			ENDIF
			
			VECTOR vCoords
			VECTOR vRot
			vCoords = g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord
			vRot = <<0.0, 0.0, fAnimHead>>
				
			IK_CONTROL_FLAGS ikFlags
			ikFlags = AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK | AIK_DISABLE_HEAD_IK | AIK_DISABLE_TORSO_IK | AIK_DISABLE_TORSO_REACT_IK
			
			SYNCED_SCENE_PLAYBACK_FLAGS sceneFlags
			sceneFlags = SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT
			
			RAGDOLL_BLOCKING_FLAGS rbfPed
			rbfPed = RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_FIRE | RBF_ELECTROCUTION | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_IMPACT_OBJECT | RBF_MELEE | RBF_RUBBER_BULLET | RBF_FALLING | RBF_WATER_JET | RBF_DROWNING |RBF_ALLOW_BLOCK_DEAD_PED | RBF_PLAYER_BUMP | RBF_PLAYER_RAGDOLL_BUMP | RBF_PED_RAGDOLL_BUMP | RBF_VEHICLE_GRAB			
			
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Starting Sync Scene with vCoord: ", vCoords, " vRot:, ", vRot)
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - sAnimName: ", sAnimName, " - sAnimDict: ", sAnimDict, " - sSoundName: ", sSoundName, " - sSoundSet: ", sSoundSet, " fSoundStartPhase: ", fSoundStartPhase, "sCamAnimName: ", sCamAnimName)
							
			FORCE_PED_AI_AND_ANIMATION_UPDATE(sWarpPortal.pedEntryAnimClone)
			
			sWarpPortal.iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, TRUE, FALSE, fCustomPhaseEnd, fCustomPhaseStart)
			
			NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(sWarpPortal.pedEntryAnimClone, sWarpPortal.iSyncScene, sAnimDict, sAnimName, SLOW_BLEND_IN, INSTANT_BLEND_OUT, sceneFlags, rbfPed, DEFAULT, ikFlags)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sCamAnimName)
				NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(sWarpPortal.iSyncScene, sAnimDict, sCamAnimName)			
				NETWORK_FORCE_LOCAL_USE_OF_SYNCED_SCENE_CAMERA(sWarpPortal.iSyncScene)
			ENDIF
			
			NETWORK_START_SYNCHRONISED_SCENE(sWarpPortal.iSyncScene)	
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sSoundName)
			AND NOT IS_STRING_NULL_OR_EMPTY(sSoundSet)
			AND fSoundStartPhase = 0.0
				PLAY_SOUND_FRONTEND(-1, sSoundName, sSoundSet)
				SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_PlayedSFX)
			ENDIF
			
			IF NOT IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_FadedPlayerOut)
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Calling SET_ENTITY_VISIBLE false on local player.")
				SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_FadedPlayerOut)
				SET_ENTITY_VISIBLE(LocalPlayerPed, FALSE)
			ENDIF
			
			TOGGLE_PAUSED_RENDERPHASES(TRUE)
						
			SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Run_SyncScene)			
		BREAK
		
		CASE eWarpPortalAnimState_Run_SyncScene
					
			IF sWarpPortal.iSyncScene != -1
				iThisLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sWarpPortal.iSyncScene)	
			ELSE
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Sync Scene Invalid. Going straight to cleanup.")
				SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Cleanup)
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iThisLocalSceneID)	
				SET_PLAYER_VISIBLE_LOCALLY(LocalPlayer)
				SET_ENTITY_LOCALLY_VISIBLE(LocalPlayerPed)
			ENDIF
			
			IF NOT IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationStarted)
			AND IS_SYNCHRONIZED_SCENE_RUNNING(iThisLocalSceneID)
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Running Sync Scene...")
				SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationStarted)
				DO_SCREEN_FADE_IN(300)
				
			ELIF IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationStarted)
				
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - GET_SYNCHRONIZED_SCENE_PHASE: ", GET_SYNCHRONIZED_SCENE_PHASE(iThisLocalSceneID), " Duration Played: ", GET_SYNCHRONIZED_SCENE_PHASE(iThisLocalSceneID) * (GET_ANIM_DURATION(sAnimDict, sAnimName)*1000), " Duration Max: ", (GET_ANIM_DURATION(sAnimDict, sAnimName)*1000))
				
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iThisLocalSceneID)
					PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Sync Scene not running anymore. Instant fade out.")
					DO_SCREEN_FADE_OUT(0)
					SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationFinished)
				ENDIF
				
				IF NOT IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationFinished)	
				
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iThisLocalSceneID)
						fCurrentPhase = GET_SYNCHRONIZED_SCENE_PHASE(iThisLocalSceneID)
					ELSE
						fCurrentPhase = fCustomPhaseEnd
					ENDIF
					fPhaseEnd = fCustomPhaseEnd
					
					IF NOT IS_STRING_NULL_OR_EMPTY(sSoundName)
					AND NOT IS_STRING_NULL_OR_EMPTY(sSoundSet)
					AND fCurrentPhase >= fSoundStartPhase
					AND NOT IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_PlayedSFX)
						PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Playing Entry SFX. sSoundName: ", sSoundName, " sSoundSet: ", sSoundSet, " fSoundStartPhase: ", fSoundStartPhase)
						PLAY_SOUND_FRONTEND(-1, sSoundName, sSoundSet)
						SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_PlayedSFX)
					ENDIF
					
					IF NOT IS_STRING_NULL_OR_EMPTY(sCamAnimName)
						IF fCurrentPhase * (GET_ANIM_DURATION(sAnimDict, sCamAnimName)*1000) >= ((GET_ANIM_DURATION(sAnimDict, sCamAnimName)*1000) - 400)				
							PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Animation Finished for early fadeout, sCamAnimName: ", sCamAnimName)
							bProgress = TRUE
						ENDIF
					ENDIF					
					IF fCurrentPhase * (GET_ANIM_DURATION(sAnimDict, sAnimName)*1000) >= ((GET_ANIM_DURATION(sAnimDict, sAnimName)*1000) - 400)
						PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Animation Finished for early fadeout, sAnimName: ", sAnimName)
						bProgress = TRUE
					ENDIF					
					IF fCurrentPhase >= fPhaseEnd
						bProgress = TRUE
						PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Anim has finished.")
					ENDIF

					IF bProgress
						IF IS_SCREEN_FADED_IN()
						AND NOT IS_SCREEN_FADING_OUT()
							PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Sync Scene Timer Expired or hit Breakout Event. Fading out the screen so we can clean up.")
							DO_SCREEN_FADE_OUT(300)
							SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationFinished)
						ELSE
							PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Waiting for screen fadeout.")
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationFinished)
				AND IS_SCREEN_FADED_OUT()
				AND NOT IS_SCREEN_FADING_IN()
					PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Finished Sync Scene!!!")
					SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Cleanup)
				ENDIF
			ENDIF
		BREAK
		
		CASE eWarpPortalAnimState_Cleanup
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Cleaning Up.")
			INT i 
			
			IF sWarpPortal.iSyncScene != -1
				iThisLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sWarpPortal.iSyncScene)		
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iThisLocalSceneID)
					NETWORK_STOP_SYNCHRONISED_SCENE(sWarpPortal.iSyncScene)
					PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED - Stopping Sync Scene.")
				ENDIF
			ENDIF
			sWarpPortal.iSyncScene = -1
			
			DESTROY_CAM(sWarpPortal.camWarpPortal)

			IF sWarpPortal.pedEntryAnimClone != LocalPlayerPed
				DELETE_PED(sWarpPortal.pedEntryAnimClone)
			ELSE			
				sWarpPortal.pedEntryAnimClone = NULL
			ENDIF
			
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			SET_ENTITY_ALPHA(LocalPlayerPed, 255, FALSE)
			SET_ENTITY_VISIBLE(LocalPlayerPed, TRUE)
			FOR i = 0 TO ci_WARP_PORTAL_PED_CLONES_MAX-1
				IF DOES_ENTITY_EXIST(sWarpPortal.pedEntryAnimClones[i])
					DELETE_PED(sWarpPortal.pedEntryAnimClones[i])
				ENDIF
			ENDFOR
			IF DOES_ENTITY_EXIST(sWarpPortal.vehEntryAnimClone)
				DELETE_VEHICLE(sWarpPortal.vehEntryAnimClone)
			ENDIF
			IF DOES_ENTITY_EXIST(sWarpPortal.vehEntryAnimOriginal)
				SET_ENTITY_VISIBLE(sWarpPortal.vehEntryAnimOriginal, TRUE)
				RESET_ENTITY_ALPHA(sWarpPortal.vehEntryAnimOriginal)
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(sWarpPortal.vehEntryAnimOriginal)
					FREEZE_ENTITY_POSITION(sWarpPortal.vehEntryAnimOriginal, FALSE)
					SET_ENTITY_COLLISION(sWarpPortal.vehEntryAnimOriginal, TRUE)
					SET_ENTITY_VISIBLE(sWarpPortal.vehEntryAnimOriginal, TRUE)
				ENDIF
			ENDIF
			RESET_NET_TIMER(sWarpPortal.tdCamAnimTimerSafety)
			RESET_NET_TIMER(sWarpPortal.tdCamAnimTimer)
			CLEAR_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_FadedPlayerOut)
			CLEAR_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationStarted)
			CLEAR_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationFinished)
			CLEAR_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationVehCreated)
			CLEAR_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_PlayedSFX)
			SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Init)
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_WARP_PORTAL_MOVE_PLAYERS_FORWARD_AFTER_WARP(INT iPortal, VECTOR vOrigin, FLOAT fHeading) 

	INT iLinkedPortal = g_FMMC_STRUCT.sWarpPortals[iPortal].iLinkedPortal
	IF iLinkedPortal = -1	
		RETURN TRUE
	ENDIF
	
	FLOAT fMoveForwardXMeters = 0.0	
	FLOAT fMoveForwardSpeed = 2.0
	
	PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - PROCESS_WARP_PORTAL_MOVE_PLAYERS_FORWARD_AFTER_WARP - Processing")
	
	IF g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].fMoveForwardXMeters > 0.0		
		fMoveForwardXMeters = g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].fMoveForwardXMeters
		PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - PROCESS_WARP_PORTAL_MOVE_PLAYERS_FORWARD_AFTER_WARP - fMoveForwardXMeters: ", fMoveForwardXMeters)
	ENDIF
	
	BOOL bHasControl
	
	IF fMoveForwardXMeters > 0.0
	
		VEHICLE_INDEX vehPlayer			
		VECTOR vPlayerPos = GET_ENTITY_COORDS(localPlayerPed)
				
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
			vehPlayer = GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
				IF GET_SEAT_PED_IS_IN(LocalPlayerPed, TRUE) = VS_DRIVER
					bHasControl = TRUE
				ENDIF
				IF IS_VECTOR_ZERO(vStartDriveToPosition)
					FREEZE_ENTITY_POSITION(vehPlayer, FALSE)
					SET_VEHICLE_ENGINE_ON(vehPlayer, TRUE, TRUE, FALSE)
					SET_VEHICLE_LIMIT_SPEED_WHEN_PLAYER_INACTIVE(vehPlayer, FALSE)
					SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE(vehPlayer, FALSE)					
					vStartDriveToPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fHeading, <<0.0, fMoveForwardXMeters, 0.0>>)						
					GET_GROUND_Z_FOR_3D_COORD(vStartDriveToPosition, vStartDriveToPosition.z)					
				ENDIF
			ENDIF
		ELSE
			IF IS_VECTOR_ZERO(vStartDriveToPosition)				
				vStartDriveToPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fHeading, <<0.0, fMoveForwardXMeters, 0.0>>)
				GET_GROUND_Z_FOR_3D_COORD(vStartDriveToPosition, vStartDriveToPosition.z)
			ENDIF
		ENDIF
		
		//DRAW_DEBUG_SPHERE(vStartDriveToPosition, 1.0, 255, 255, 255)
		
		FLOAT fDistanceTarget = POW(1.5, 2.0)
		FLOAT fDistanceCurrent = VDIST2(vStartDriveToPosition, vPlayerPos)
		
		PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - PROCESS_WARP_PORTAL_MOVE_PLAYERS_FORWARD_AFTER_WARP - Going to: ", vStartDriveToPosition, "out of fDistanceTarget: ", fDistanceTarget, " fDistanceCurrent: ", fDistanceCurrent, " fCachedDriveToPositionDistance: ", fCachedDriveToPositionDistance)
		
		IF fCachedDriveToPositionDistance > fDistanceCurrent			
		OR fCachedDriveToPositionDistance = 0
			fCachedDriveToPositionDistance = fDistanceCurrent			
		ENDIF
		
		IF fDistanceCurrent <= fDistanceTarget
		OR fDistanceCurrent > fCachedDriveToPositionDistance
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - PROCESS_WARP_PORTAL_MOVE_PLAYERS_FORWARD_AFTER_WARP - At Destination vStartDriveToPosition: ", vStartDriveToPosition)
			IF IS_PED_PERFORMING_TASK(LocalPlayerPed, SCRIPT_TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED)		
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - PROCESS_WARP_PORTAL_MOVE_PLAYERS_FORWARD_AFTER_WARP - Clearing Task")
				CLEAR_PED_TASKS(LocalPlayerPed)
			ENDIF
					
			IF IS_VEHICLE_DRIVEABLE(vehPlayer)
				IF GET_SEAT_PED_IS_IN(localPlayerPed) = VS_DRIVER
				AND NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
					PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - PROCESS_WARP_PORTAL_MOVE_PLAYERS_FORWARD_AFTER_WARP - Forcing vehicle brakes.")
					SET_VEHICLE_BRAKE(vehPlayer, 1.0)					
				ENDIF
			ENDIF
			
			RETURN TRUE
		
		ELIF NOT IS_PED_PERFORMING_TASK(LocalPlayerPed, SCRIPT_TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED)
			
			IF bHasControl
				FREEZE_ENTITY_POSITION(LocalPlayerPed, FALSE)
				
				IF NOT IS_VECTOR_ZERO(vStartDriveToPosition)				
					PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - PROCESS_WARP_PORTAL_MOVE_PLAYERS_FORWARD_AFTER_WARP - Giving task, TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED, vStartDriveToPosition: ", vStartDriveToPosition)
					DRIVINGMODE dmFlags
					TASK_GO_TO_COORD_ANY_MEANS_FLAGS tgcamFlags								
					tgcamFlags = TGCAM_IGNORE_VEHICLE_HEALTH
					tgcamFlags = tgcamFlags | TGCAM_REMAIN_IN_VEHICLE_AT_DESTINATION
					tgcamFlags = tgcamFlags | TGCAM_NEVER_ABANDON_VEHICLE_IF_MOVING
					tgcamFlags = tgcamFlags | TGCAM_NEVER_ABANDON_VEHICLE
					dmFlags = dmFlags | DF_DriveIntoOncomingTraffic
					dmFlags = dmFlags | DRIVINGMODE_PLOUGHTHROUGH
					dmFlags = dmFlags | DF_ForceStraightLine
					TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED(LocalPlayerPed, vStartDriveToPosition, PEDMOVEBLENDRATIO_RUN, vehPlayer, FALSE, dmFlags, DEFAULT, 0, 20.0, tgcamFlags, fMoveForwardSpeed, 0.5)
					SET_DRIVE_TASK_CRUISE_SPEED(LocalPlayerPed, fMoveForwardSpeed)
					SET_DRIVE_TASK_MAX_CRUISE_SPEED(LocalPlayerPed, fMoveForwardSpeed)					
					SET_VEHICLE_MAX_SPEED(vehPlayer, fMoveForwardSpeed)
				ENDIF
				
			ENDIF
		ELSE
			IF bHasControl
				SET_DRIVE_TASK_CRUISE_SPEED(LocalPlayerPed, fMoveForwardSpeed)
				SET_DRIVE_TASK_MAX_CRUISE_SPEED(LocalPlayerPed, fMoveForwardSpeed)
				SET_VEHICLE_MAX_SPEED(vehPlayer, fMoveForwardSpeed)
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - PROCESS_WARP_PORTAL_MOVE_PLAYERS_FORWARD_AFTER_WARP - Calling SET_DRIVE_TASK_CRUISE_SPEED with 6.0")
			ENDIF
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC 

PROC SET_INSTANCED_PORTAL_FLY_CAMERA_CLIENT_STATE(WARP_PORTAL_FLY_CAM_STATE eWarpPortalFlyCamStateNew)
	PRINTLN("[LM][wPortals][wPortal ", sWarpPortal.iWarpPortalEntered, "] - SET_INSTANCED_PORTAL_FLY_CAMERA_CLIENT_STATE - Setting Fly Cam State New: ", ENUM_TO_INT(eWarpPortalFlyCamStateNew), " From Old: ", ENUM_TO_INT(sWarpPortal.eWarpPortalFlyCamState))
	sWarpPortal.eWarpPortalFlyCamState = eWarpPortalFlyCamStateNew	
ENDPROC

FUNC VECTOR GET_CAMERA_FLY_DOWN_AFTER_WARP_PORTAL_OFFSET_POSITION(INT iPortal, BOOL bPullOut)
	
	UNUSED_PARAMETER(iPortal) // Will be tied to creator functionality. url:bugstar:7424150 - Data Leak 5 - Don't Fuck With Dre - Can please we have these camera changes made for the trip skip section of the mission? [Creator support]
	
	IF bPullOut
		RETURN <<-0.0687, -29.8615, 11.2027>>
	ELSE
		RETURN <<-3.7764, -30.7116, 6.4688>>
	ENDIF
	
	RETURN <<0.0, 0.0, 0.0>>
	
ENDFUNC

FUNC VECTOR GET_CAMERA_FLY_DOWN_AFTER_WARP_PORTAL_POINT_AT_OFFSET_POSITION(INT iPortal, BOOL bPullOut)
		
	UNUSED_PARAMETER(iPortal) // Will be tied to creator functionality. url:bugstar:7424150 - Data Leak 5 - Don't Fuck With Dre - Can please we have these camera changes made for the trip skip section of the mission? [Creator support]
	
	IF bPullOut
		RETURN <<-0.0842, -26.8629, 11.1095>>
	ELSE
		RETURN <<-2.6612, -27.9286, 6.3649>>
	ENDIF
	
	RETURN <<0.0, 0.0, 0.0>>
	
ENDFUNC

FUNC FLOAT GET_CAMERA_FLY_DOWN_AFTER_WARP_PORTAL_FOV(INT iPortal, BOOL bPullOut)
	
	UNUSED_PARAMETER(iPortal) // Will be tied to creator functionality. url:bugstar:7424150 - Data Leak 5 - Don't Fuck With Dre - Can please we have these camera changes made for the trip skip section of the mission? [Creator support]
	
	IF bPullOut	
		RETURN 50.0
	ELSE
		RETURN 50.0
	ENDIF
	
	RETURN 50.0
	
ENDFUNC

FUNC BOOL HAS_FLY_CAMERA_FOR_WARP_PORTAL_FINISHED(INT iPortal, VECTOR vStart, VECTOR vEnd)

	UNUSED_PARAMETER(iPortal)
	UNUSED_PARAMETER(vStart)
	UNUSED_PARAMETER(vEnd)
	
	IF HAS_NET_TIMER_STARTED(sWarpPortal.tdCamAnimTimerSafety)
	AND HAS_NET_TIMER_EXPIRED_READ_ONLY(sWarpPortal.tdCamAnimTimerSafety, 20000)		
		SET_INSTANCED_PORTAL_FLY_CAMERA_CLIENT_STATE(eWarpPortalFlyCamState_Cleanup)
	ENDIF
	
	SWITCH sWarpPortal.eWarpPortalFlyCamState
		CASE eWarpPortalFlyCamState_Init
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_USING_WARP_PORTAL_FLY_CAM)
			REINIT_NET_TIMER(sWarpPortal.tdCamAnimTimerSafety)
			REINIT_NET_TIMER(sWarpPortal.tdCamAnimTimer)
			
			VECTOR vCamPosOffset
			VECTOR vCamPointAtPosOffset	
			FLOAT fCamFov
			
			vCamPosOffset = GET_CAMERA_FLY_DOWN_AFTER_WARP_PORTAL_OFFSET_POSITION(iPortal, TRUE)
			vCamPointAtPosOffset = GET_CAMERA_FLY_DOWN_AFTER_WARP_PORTAL_POINT_AT_OFFSET_POSITION(iPortal, TRUE)
			fCamFov = GET_CAMERA_FLY_DOWN_AFTER_WARP_PORTAL_FOV(iPortal, TRUE)
			
			VECTOR vCamPos
			vCamPos = GET_ENTITY_COORDS(localPlayerPed)
			vCamPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCamPos, GET_ENTITY_HEADING(LocalPlayerPed), vCamPosOffset)
			
			sWarpPortal.camWarpPortal = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamPos, <<0.0, 0.0, 0.0>>, fCamFov)
			
			SET_CAM_ACTIVE(sWarpPortal.camWarpPortal, TRUE)
			
			POINT_CAM_AT_ENTITY(sWarpPortal.camWarpPortal, LocalPlayerPed, vCamPointAtPosOffset)
			
			RENDER_SCRIPT_CAMS(TRUE, TRUE, 4000, DEFAULT, TRUE)
			
			SET_CAM_FOV(sWarpPortal.camWarpPortal, fCamFov)
			
			VECTOR vMoveToPosition
			vMoveToPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(LocalPlayerPed, FALSE), GET_ENTITY_HEADING(LocalPlayerPed), <<0.0, 100.0, 0.0>>)
								
			VECTOR vTemp
			FLOAT fTemp
			INT iNode, iLanes	
			FOR iNode = 0 TO 30
				IF GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vMoveToPosition, iNode, vTemp, fTemp, iLanes, DEFAULT, 0.0, 99999.0)
					vMoveToPosition = vTemp
					BREAKLOOP
				ENDIF
			ENDFOR
			
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				VEHICLE_INDEX vehPlayer
				vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
				IF IS_VEHICLE_DRIVEABLE(vehPlayer)
				AND NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
					SET_VEHICLE_LIMIT_SPEED_WHEN_PLAYER_INACTIVE(vehPlayer, FALSE)
					SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE(vehPlayer, FALSE)
					
					DRIVINGMODE dmFlags
					TASK_GO_TO_COORD_ANY_MEANS_FLAGS tgcamFlags
					tgcamFlags = TGCAM_IGNORE_VEHICLE_HEALTH | TGCAM_CONSIDER_ALL_NEARBY_VEHICLES | TGCAM_PROPER_IS_DRIVEABLE_CHECK
					dmFlags = dmFlags | DF_SteerAroundPeds | DF_SteerAroundStationaryCars | DF_SteerAroundObjects
					tgcamFlags = tgcamFlags | TGCAM_REMAIN_IN_VEHICLE_AT_DESTINATION
					tgcamFlags = tgcamFlags | TGCAM_NEVER_ABANDON_VEHICLE_IF_MOVING
					tgcamFlags = tgcamFlags | TGCAM_NEVER_ABANDON_VEHICLE
					dmFlags = dmFlags | DRIVINGMODE_AVOIDCARS
					
					TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED(LocalPlayerPed, vMoveToPosition, PEDMOVEBLENDRATIO_RUN, vehPlayer, TRUE, dmFlags, DEFAULT, 0, 50.0, tgcamFlags, 15.0, 2.0)
				ENDIF
			ELSE
				TASK_GO_TO_COORD_ANY_MEANS(LocalPlayerPed, vMoveToPosition, PEDMOVEBLENDRATIO_RUN, NULL)
			ENDIF
			
			SET_INSTANCED_PORTAL_FLY_CAMERA_CLIENT_STATE(eWarpPortalFlyCamState_Process)
		BREAK
		CASE eWarpPortalFlyCamState_Process	
			IF HAS_NET_TIMER_EXPIRED_READ_ONLY(sWarpPortal.tdCamAnimTimer, 2000)
				SET_INSTANCED_PORTAL_FLY_CAMERA_CLIENT_STATE(eWarpPortalFlyCamState_Fade)
			ENDIF
		BREAK
		CASE eWarpPortalFlyCamState_Fade
			IF NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(1000)
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()			
				SET_INSTANCED_PORTAL_FLY_CAMERA_CLIENT_STATE(eWarpPortalFlyCamState_Sync)			
			ENDIF
		BREAK
		CASE eWarpPortalFlyCamState_Sync		
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_FINISHED_WARP_PORTAL_FLY_CAM)
			
			INT iPart
			iPart = -1
			DO_PARTICIPANT_LOOP_FLAGS eFlags
			eFlags = DPLF_CHECK_PED_ALIVE | DPLF_EXCLUDE_LOCAL_PART | DPLF_CHECK_PLAYER_OK
			
			WHILE DO_PARTICIPANT_LOOP(iPart, eFlags)
				IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet4, PBBOOL4_USING_WARP_PORTAL_FLY_CAM)
				AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet4, PBBOOL4_FINISHED_WARP_PORTAL_FLY_CAM)
					RETURN FALSE
				ENDIF
			ENDWHILE
			
			SET_INSTANCED_PORTAL_FLY_CAMERA_CLIENT_STATE(eWarpPortalFlyCamState_Cleanup)
		BREAK
		CASE eWarpPortalFlyCamState_Cleanup			
			DESTROY_CAM(sWarpPortal.camWarpPortal)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			RESET_NET_TIMER(sWarpPortal.tdCamAnimTimerSafety)
			RESET_NET_TIMER(sWarpPortal.tdCamAnimTimer)
			
			IF IS_PED_PERFORMING_TASK(LocalPlayerPed, SCRIPT_TASK_GO_TO_COORD_ANY_MEANS)
			OR IS_PED_PERFORMING_TASK(LocalPlayerPed, SCRIPT_TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS)
			OR IS_PED_PERFORMING_TASK(LocalPlayerPed, SCRIPT_TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED)
				CLEAR_PED_TASKS(LocalPlayerPed)
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				VEHICLE_INDEX vehPlayer
				vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)				
				IF IS_VEHICLE_DRIVEABLE(vehPlayer)
				AND NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
					SET_VEHICLE_LIMIT_SPEED_WHEN_PLAYER_INACTIVE(vehPlayer, TRUE)
					SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE(vehPlayer, TRUE)
				ENDIF
			ENDIF
			
			CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_USING_WARP_PORTAL_FLY_CAM)
			CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_FINISHED_WARP_PORTAL_FLY_CAM)
			
			SET_INSTANCED_PORTAL_FLY_CAMERA_CLIENT_STATE(eWarpPortalFlyCamState_Init)
			RETURN TRUE
		BREAK		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_FLY_CAMERA_BACK_IN_FOR_WARP_PORTAL_FINISHED(INT iPortal, VECTOR vOrigin, FLOAT fHeading)

	UNUSED_PARAMETER(iPortal)
	
	IF HAS_NET_TIMER_STARTED(sWarpPortal.tdCamAnimTimerSafety)
	AND HAS_NET_TIMER_EXPIRED_READ_ONLY(sWarpPortal.tdCamAnimTimerSafety, 10000)		
		SET_INSTANCED_PORTAL_FLY_CAMERA_CLIENT_STATE(eWarpPortalFlyCamState_Cleanup)
	ENDIF
	
	// Hold until driver has warped us.
	IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_USING_WARP_PORTAL_FLY_CAM_BACK_IN)
		IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
			VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
			
			IF IS_VEHICLE_DRIVEABLE(vehPlayer)
				PED_INDEX pedIndex = GET_PED_IN_VEHICLE_SEAT(vehPlayer, VS_DRIVER)
				
				IF pedIndex != localPlayerPed
					PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedIndex)
					IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)				
						PARTICIPANT_INDEX piPart = NETWORK_GET_PARTICIPANT_INDEX(piPlayer)
						INT iPart = NATIVE_TO_INT(piPart)
						IF iPart != -1
							IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet4, PBBOOL4_USING_WARP_PORTAL_FLY_CAM_BACK_IN)
							AND MC_PlayerBD[iPart].iWarpPortalEntered != -1
								PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_FLY_CAMERA_BACK_IN_FOR_WARP_PORTAL_FINISHED - Waiting for Driver to start their Fly Cam.")
								RETURN FALSE
							ELSE
								PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_FLY_CAMERA_BACK_IN_FOR_WARP_PORTAL_FINISHED - Fly cam has either started or finished their Fly Cam.")
							ENDIF
						ELSE
							PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_FLY_CAMERA_BACK_IN_FOR_WARP_PORTAL_FINISHED - Driver does not exist (iPart is -1) Skipping Fly Cam")
							RETURN TRUE
						ENDIF
					ELSE
						PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_FLY_CAMERA_BACK_IN_FOR_WARP_PORTAL_FINISHED - Driver is not an active player Skipping Fly Cam")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	SWITCH sWarpPortal.eWarpPortalFlyCamState
		CASE eWarpPortalFlyCamState_Init
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_USING_WARP_PORTAL_FLY_CAM_BACK_IN)
			REINIT_NET_TIMER(sWarpPortal.tdCamAnimTimerSafety)
			REINIT_NET_TIMER(sWarpPortal.tdCamAnimTimer)
			
			VECTOR vCamPosOffset
			VECTOR vCamPointAtPosOffset	
			FLOAT fCamFov
			
			vCamPosOffset = GET_CAMERA_FLY_DOWN_AFTER_WARP_PORTAL_OFFSET_POSITION(iPortal, FALSE)
			vCamPointAtPosOffset = GET_CAMERA_FLY_DOWN_AFTER_WARP_PORTAL_POINT_AT_OFFSET_POSITION(iPortal, FALSE)
			fCamFov = GET_CAMERA_FLY_DOWN_AFTER_WARP_PORTAL_FOV(iPortal, FALSE)
			
			VECTOR vCamPos
			vCamPos = GET_ENTITY_COORDS(localPlayerPed)
			vCamPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCamPos, GET_ENTITY_HEADING(LocalPlayerPed), vCamPosOffset)
			
			sWarpPortal.camWarpPortal = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamPos, <<0.0, 0.0, 0.0>>, fCamFov)
			
			SET_CAM_ACTIVE(sWarpPortal.camWarpPortal, TRUE)
			
			POINT_CAM_AT_ENTITY(sWarpPortal.camWarpPortal, LocalPlayerPed, vCamPointAtPosOffset)			
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE, 3000, DEFAULT, TRUE)
			
			SET_CAM_FOV(sWarpPortal.camWarpPortal, fCamFov)
			
			vStartDriveToPosition = <<0.0, 0.0, 0.0>>
			
			PROCESS_WARP_PORTAL_MOVE_PLAYERS_FORWARD_AFTER_WARP(iPortal, vOrigin, fHeading)
			
			SET_INSTANCED_PORTAL_FLY_CAMERA_CLIENT_STATE(eWarpPortalFlyCamState_Fade)
		BREAK
		
		CASE eWarpPortalFlyCamState_Fade	
			
			PROCESS_WARP_PORTAL_MOVE_PLAYERS_FORWARD_AFTER_WARP(iPortal, vOrigin, fHeading)
			
			RENDER_SCRIPT_CAMS(FALSE, TRUE, 4000, DEFAULT, TRUE)
			
			IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(sWarpPortal.tdCamAnimTimer, 1000)
				RETURN FALSE
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(1000)
			ENDIF
			
			SET_INSTANCED_PORTAL_FLY_CAMERA_CLIENT_STATE(eWarpPortalFlyCamState_Process)	
		BREAK
		
		CASE eWarpPortalFlyCamState_Process
			IF PROCESS_WARP_PORTAL_MOVE_PLAYERS_FORWARD_AFTER_WARP(iPortal, vOrigin, fHeading)
				IF HAS_NET_TIMER_EXPIRED_READ_ONLY(sWarpPortal.tdCamAnimTimer, 5000)
					SET_INSTANCED_PORTAL_FLY_CAMERA_CLIENT_STATE(eWarpPortalFlyCamState_Cleanup)
				ENDIF
			ENDIF
		BREAK
		
		// not used ---		
		CASE eWarpPortalFlyCamState_Sync					
			SET_INSTANCED_PORTAL_FLY_CAMERA_CLIENT_STATE(eWarpPortalFlyCamState_Cleanup)
		BREAK
		// not used ---
		
		CASE eWarpPortalFlyCamState_Cleanup			
			DESTROY_CAM(sWarpPortal.camWarpPortal)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			RESET_NET_TIMER(sWarpPortal.tdCamAnimTimerSafety)
			RESET_NET_TIMER(sWarpPortal.tdCamAnimTimer)						
			SET_INSTANCED_PORTAL_FLY_CAMERA_CLIENT_STATE(eWarpPortalFlyCamState_Init)
			RETURN TRUE
		BREAK		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_POST_WARP_INSTANCED_CONTENT_PORTAL_FUNCTIONALITY(INT iPortal)

	IF NOT IS_CONTROL_ACTION_FROM_WARP_PORTAL_TRIGGER_OPTION_SET(iPortal)
		SET_BIT(sWarpPortal.iJustUsedPortalStartBS, iPortal)
		PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - PROCESS_POST_WARP_INSTANCED_CONTENT_PORTAL_FUNCTIONALITY - Warping Stage - Post Warp - Setting that we just used START Portal")			
	ENDIF
	
	INT iCompanionIndex = 0
	SET_BIT(iDialogueHasUsedPortalStartBS, iPortal)
	PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - PROCESS_POST_WARP_INSTANCED_CONTENT_PORTAL_FUNCTIONALITY - Warping Stage - Post Warp - Setting iDialogueHasUsedPortalStartBS")
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_BringCompanions)
		FOR iCompanionIndex = 0 TO FMMC_MAX_COMPANIONS-1
			IF MC_playerBD_1[iLocalPart].sPedCompanionTaskData[iCompanionIndex].iPed = -1
			OR MC_playerBD_1[iLocalPart].sPedCompanionTaskData[iCompanionIndex].targetEntityFriend != LocalPlayer
				RELOOP
			ENDIF
			SET_COMPANION_WARP_LOCATION(iCompanionIndex, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sWarpPortal.vTargetLocation, sWarpPortal.fTargetHead, <<1.0*(iCompanionIndex+1), 0.0, 0.0>>), sWarpPortal.fTargetHead)
		ENDFOR
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS2, ciWARP_PORTAL_2_TriggerLightsOffLocally)
		PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - PROCESS_POST_WARP_INSTANCED_CONTENT_PORTAL_FUNCTIONALITY - Warping Stage - Post Warp - Turning off Lights.")
		TURN_OFF_ARTIFICIAL_LIGHTS_LOCALLY(FALSE)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS2, ciWARP_PORTAL_2_TriggerLightsOnLocally)
		PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - PROCESS_POST_WARP_INSTANCED_CONTENT_PORTAL_FUNCTIONALITY - Warping Stage - Post Warp - Turning on Lights.")	
		TURN_ON_ARTIFICIAL_LIGHTS_LOCALLY(FALSE)	
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS2, ciWARP_PORTAL_2_ForceFlashlightInHand)
		IF NOT IS_PED_HOLDING_THIS_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_FLASHLIGHT)
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - PROCESS_POST_WARP_INSTANCED_CONTENT_PORTAL_FUNCTIONALITY - Warping Stage - Post Warp - Equpping player with flashlight.")
			GIVE_WEAPON_TO_PED(LocalPlayerPed, WEAPONTYPE_DLC_FLASHLIGHT, 1, TRUE, TRUE)
		ENDIF
	ENDIF
		
	WARP_PORTAL_APPLY_PREVIOUS_MOMENTUM(iPortal)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS2, ciWARP_PORTAL_2_TriggerSkydivingTask)	
		PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - PROCESS_POST_WARP_INSTANCED_CONTENT_PORTAL_FUNCTIONALITY - Warping Stage - Post Warp - Triggering Skydiving Tasks.")
		TASK_SKY_DIVE(localPlayerPed, TRUE)
	ENDIF
	
	START_VEHICLE_SWAP_FROM_WARP_PORTAL_DATA(iPortal)
	
ENDPROC

FUNC BOOL WARP_PLAYER_USING_INSTANCED_CONTENT_PORTAL(INT iPortal, VECTOR vStart, VECTOR vEnd)
		
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
	
	IF IS_VECTOR_ZERO(sWarpPortal.vTargetLocation)
	
		VECTOR vTargetLocation, vTargetLocationOriginal
		FLOAT fTargetHead, fHeadOriginal
		vTargetLocation = vEnd	
		fTargetHead = MC_GET_WARP_PORTAL_END_HEADING(iPortal)
		sWarpPortal.bKeepVehicle = IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_KeepVehicle) OR g_FMMC_STRUCT.sWarpPortals[iPortal].sVehicleSwap.mnVehicleModelSwap != DUMMY_MODEL_FOR_SCRIPT
			
		BOOL bSuccess
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_UseParticpantNumberAsFallbackPos)		
			INT i		
			VECTOR vTargetLocationMin = vTargetLocation
			VECTOR vTargetLocationMax = vTargetLocation
			vTargetLocationMin.x -= 0.15
			vTargetLocationMax.x += 0.15	
			vTargetLocationMin.y -= 0.15
			vTargetLocationMax.y += 0.15	
			vTargetLocationMax.z += 1.0
			
			vTargetLocationOriginal = vTargetLocation
			fHeadOriginal = fTargetHead
			
			IF IS_AREA_OCCUPIED(vTargetLocationMin, vTargetLocationMax, FALSE, TRUE, TRUE, FALSE, FALSE)
				FOR i = 0 TO FMMC_MAX_WARP_PORTALS_EXTRA_POSITIONS-1			
					IF NOT IS_VECTOR_ZERO(MC_GET_WARP_PORTAL_END_FALLBACK_POSITION(iPortal, i))
						vTargetLocation = MC_GET_WARP_PORTAL_END_FALLBACK_POSITION(iPortal, i)
						fTargetHead = MC_GET_WARP_PORTAL_END_FALLBACK_HEADING(iPortal, i)
					ENDIF
					
					vTargetLocationMin = vTargetLocation
					vTargetLocationMax = vTargetLocation
					vTargetLocationMin.x -= 0.15
					vTargetLocationMax.x += 0.15
					vTargetLocationMin.y -= 0.15
					vTargetLocationMax.y += 0.15
					vTargetLocationMax.z += 1.0
					
					IF NOT IS_AREA_OCCUPIED(vTargetLocationMin, vTargetLocationMax, FALSE, TRUE, TRUE, FALSE, FALSE)				
						bSuccess = TRUE
						BREAKLOOP
					ENDIF
				ENDFOR
			ELSE
				bSuccess = TRUE
			ENDIF
		ELSE
			INT iExtraPortalIndex = GET_ORDERED_PARTICIPANT_NUMBER(iLocalPart)
			IF iExtraPortalIndex > -1
			AND iExtraPortalIndex < FMMC_MAX_WARP_PORTALS_EXTRA_POSITIONS
				IF NOT IS_VECTOR_ZERO(MC_GET_WARP_PORTAL_END_FALLBACK_POSITION(iPortal, iExtraPortalIndex))
					vTargetLocation = MC_GET_WARP_PORTAL_END_FALLBACK_POSITION(iPortal, iExtraPortalIndex)
					fTargetHead = MC_GET_WARP_PORTAL_END_FALLBACK_HEADING(iPortal, iExtraPortalIndex)
				ENDIF
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - WARP_PLAYER_USING_INSTANCED_CONTENT_PORTAL - Warping Stage - Using Fallback Pos: ", iExtraPortalIndex, " - vTargetLocation: ", vTargetLocation)
			ELSE
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - WARP_PLAYER_USING_INSTANCED_CONTENT_PORTAL - Warping Stage - Using main position as Fallback is not suitable... iExtraPortalIndex: ", iExtraPortalIndex)
			ENDIF
			
			bSuccess = TRUE
		ENDIF
		
		IF NOT bSuccess
			sWarpPortal.vTargetLocation = vTargetLocationOriginal
			sWarpPortal.fTargetHead = fHeadOriginal
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - WARP_PLAYER_USING_INSTANCED_CONTENT_PORTAL - Warping Stage - Had to use a the Main Position as all fallbacks are taken - vTargetLocation: ", vTargetLocation)
		ELSE
			sWarpPortal.vTargetLocation = vTargetLocation
			sWarpPortal.fTargetHead = fTargetHead
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - WARP_PLAYER_USING_INSTANCED_CONTENT_PORTAL - Warping Stage - vTargetLocation: ", vTargetLocation)
		ENDIF
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(sWarpPortal.vTargetLocation)
		
		BOOL bQuickWarp	= TRUE		
		INTERIOR_INSTANCE_INDEX interiorPlayer = GET_INTERIOR_AT_COORDS(vStart)
		INTERIOR_INSTANCE_INDEX interiorTarget = GET_INTERIOR_AT_COORDS(sWarpPortal.vTargetLocation)				
		IF interiorTarget != NULL		
		AND interiorPlayer != interiorTarget
			bQuickWarp = FALSE
		ENDIF
		
		IF VDIST2(vStart, sWarpPortal.vTargetLocation) > POW(180, 2.0)
			bQuickWarp = FALSE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS2, ciWARP_PORTAL_2_ForceFullTeleport)
			bQuickWarp = FALSE
		ENDIF
			
		BOOL bSkipWarp
		IF sWarpPortal.bKeepVehicle
		AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		AND GET_SEAT_PED_IS_IN(localPlayerPed) != VS_DRIVER
			bSkipWarp = TRUE
		ENDIF
		
		IF bSkipWarp
		OR NET_WARP_TO_COORD(sWarpPortal.vTargetLocation, sWarpPortal.fTargetHead, sWarpPortal.bKeepVehicle, FALSE, FALSE, FALSE, TRUE, bQuickWarp, TRUE)
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - WARP_PLAYER_USING_INSTANCED_CONTENT_PORTAL - Warping Stage - Warp Successful.")
			
			PROCESS_POST_WARP_INSTANCED_CONTENT_PORTAL_FUNCTIONALITY(iPortal)
			
			sWarpPortal.bKeepVehicle = FALSE
			
			RETURN TRUE
		ELSE
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - WARP_PLAYER_USING_INSTANCED_CONTENT_PORTAL - Warping Stage - bQuickWarp: ", bQuickWarp, " Waiting for Warp to complete...")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PRELOAD_WARP_PORTAL_FINISHED(INT iPortal)
	BOOL bFinishedFade, bFinishedPostFx, bFinishedWhiteout

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_PostFXBeforeFade) 
		bFinishedPostFx = TRUE
		PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_PRELOAD_WARP_PORTAL_FINISHED - Preload Stage - No Flash...")
	ENDIF
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_FadeOutBeforeWarp)
		bFinishedFade = TRUE
		PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_PRELOAD_WARP_PORTAL_FINISHED - Preload Stage - No Fade...")
	ENDIF
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS2, ciWARP_PORTAL_2_WhiteOutBeforeWarp)
		bFinishedWhiteout = TRUE
		PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_PRELOAD_WARP_PORTAL_FINISHED - Preload Stage - No Whiteout...")
	ENDIF	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_PostFXBeforeFade)
		IF NOT ANIMPOSTFX_IS_RUNNING("InchPurple")
		AND NOT HAS_NET_TIMER_STARTED(sWarpPortal.tdWarpFlashTimer)
			ANIMPOSTFX_PLAY("InchPurple", 0, TRUE)
			REINIT_NET_TIMER(sWarpPortal.tdWarpFlashTimer)
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_PRELOAD_WARP_PORTAL_FINISHED - Preload Stage - Start Flash...")
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(sWarpPortal.tdWarpFlashTimer)
		AND HAS_NET_TIMER_EXPIRED_READ_ONLY(sWarpPortal.tdWarpFlashTimer, 2000)
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_PRELOAD_WARP_PORTAL_FINISHED - Preload Stage - Flash Finished...")
			bFinishedPostFx = TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_FadeOutBeforeWarp)
	AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_PostFXBeforeFade) OR bFinishedPostFx)
	AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS2, ciWARP_PORTAL_2_WhiteOutBeforeWarp) OR bFinishedWhiteout)
		IF IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_OUT(1000)
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_PRELOAD_WARP_PORTAL_FINISHED - Preload Stage - Start Fade...")
		ENDIF
		
		IF IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
			bFinishedFade = TRUE
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_PRELOAD_WARP_PORTAL_FINISHED - Preload Stage - Fade Finished...")
		ENDIF		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS2, ciWARP_PORTAL_2_WhiteOutBeforeWarp)
	AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_PostFXBeforeFade) OR bFinishedPostFx)
	AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_FadeOutBeforeWarp) OR bFinishedFade)
		IF IS_RECTANGLE_FADED_IN()
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_PRELOAD_WARP_PORTAL_FINISHED - Preload Stage - Whiteout Finished...")
			bFinishedWhiteout = TRUE			
		ELSE
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_PRELOAD_WARP_PORTAL_FINISHED - Preload Stage - Whiteout Started...")			
			SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_PlayedWhiteout)
			SET_RECTANGLE_COLOUR(TRUE)
			ACTIVATE_RECTANGLE_FADE_OUT(FALSE)
			ACTIVATE_RECTANGLE_FADE_IN(TRUE)
		ENDIF
	ENDIF
	
	STRING sAnimDict = GET_ENTRY_ANIM_DICT_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType, IS_PED_WEARING_HIGH_HEELS(localPlayerPed))
	IF NOT IS_STRING_NULL_OR_EMPTY(sAnimDict)
	AND ARE_STRINGS_EQUAL(sAnimDict, "Vehicle")
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_PRELOAD_WARP_PORTAL_FINISHED - Preload Stage - Using a Vehicle in Anim - Make sure it's safe.")
			VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF IS_VEHICLE_DRIVEABLE(tempVeh)
				INT iPart = 0
				FOR iPart = 0 TO NUM_NETWORK_PLAYERS-1
					PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iPart)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
						PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
						PED_INDEX pedPlayer = GET_PLAYER_PED(piPlayer)
						
						IF NOT IS_PED_INJURED(pedPlayer)
							IF IS_PED_IN_ANY_VEHICLE(pedPlayer, TRUE)
								IF GET_VEHICLE_PED_IS_ENTERING(pedPlayer) = tempVeh								
									PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_PRELOAD_WARP_PORTAL_FINISHED - Preload Stage - we are waiting for all players to have finished entering the vehicle - Returning False.")
									RETURN FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
	
	IF (bFinishedFade AND bFinishedPostFx AND bFinishedWhiteout)
		PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_PRELOAD_WARP_PORTAL_FINISHED - Preload Stage - Preload Finished Returning True...")
		IF ANIMPOSTFX_IS_RUNNING("InchPurple")
			ANIMPOSTFX_STOP("InchPurple")
		ENDIF
		RESET_NET_TIMER(sWarpPortal.tdWarpFlashTimer)
		RETURN TRUE
	ENDIF
	
	PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - HAS_PRELOAD_WARP_PORTAL_FINISHED - Preload Stage - Preloading - Returning False...")
	
	RETURN FALSE	
ENDFUNC	

FUNC BOOL DOES_WARP_PORTAL_IGNORE_RANGE_TRIGGER(INT iPortal)

	IF g_FMMC_STRUCT.sWarpPortals[iPortal].iAutoTriggerAfterTime > 0		
	AND NOT IS_BIT_SET(sWarpPortal.iWarpPortalBitset_AutoWarpedFromStart, iPortal)
	AND MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_ServerBD_4.iAutoTriggerAfterTimeStampStart[iPortal]) 
	AND MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(MC_ServerBD_4.iAutoTriggerAfterTimeStampStart[iPortal], g_FMMC_STRUCT.sWarpPortals[iPortal].iAutoTriggerAfterTime*1000)
		RETURN TRUE			
	ENDIF
	
	RETURN FALSE
	
ENDFUNC	

PROC PROCESS_ACTIVATING_AUTO_WARP_TIMER(INT iPortal)
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.sWarpPortals[iPortal].iAutoTriggerAfterTime > 0		
		MC_START_NET_TIMER_WITH_TIMESTAMP(MC_ServerBD_4.iAutoTriggerAfterTimeStampStart[iPortal])
	ENDIF
		
ENDPROC

FUNC BOOL IS_PLAYER_IN_RANGE_OF_PORTAL(INT iPortal, VECTOR vStart)

	PED_INDEX pedPlayer = PLAYER_PED_ID()
	IF IS_PED_INJURED(pedPlayer)
		RETURN FALSE
	ENDIF
		
	IF HAS_NET_TIMER_STARTED(sWarpPortal.tdWarpPortalCooldown)
		IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(sWarpPortal.tdWarpPortalCooldown, 2000)
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Special forced conditions.
	IF DOES_WARP_PORTAL_IGNORE_RANGE_TRIGGER(iPortal)
		RETURN TRUE
	ENDIF
	
	VECTOR vPlayerPos = GET_ENTITY_COORDS(pedPlayer)
	FLOAT fDistZStart = (vPlayerPos.z - vStart.z)
	BOOL bHeightPassStart = (fDistZStart < 2.0 AND fDistZStart > -0.75)
	vPlayerPos.z = 0.0
	vStart.z = 0.0
	
	/*PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - IS_PLAYER_IN_RANGE_OF_PORTAL - vPlayerPos: " , vPlayerPos)
	PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - IS_PLAYER_IN_RANGE_OF_PORTAL - VDIST2(vPlayerPos, vStart): " , VDIST2(vPlayerPos, vStart), " POW(GET_WARP_PORTAL_START_RANGE(iPortal), 2.0): ", POW(GET_WARP_PORTAL_START_RANGE(iPortal), 2.0))	
	PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - IS_PLAYER_IN_RANGE_OF_PORTAL - fDistZStart: " , fDistZStart) */
	
	INT iRule = GET_TEAM_CURRENT_RULE(MC_PlayerBD[iLocalPart].iTeam)
	
	IF (bHeightPassStart 
	AND VDIST2(vPlayerPos, vStart) < POW(GET_WARP_PORTAL_START_RANGE(iPortal), 2.0))
		
		IF iPortal = sWarpPortal.iWarpPortalLastWarpedTo
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - IS_PLAYER_IN_RANGE_OF_PORTAL - (start) Blocked by sWarpPortal.iWarpPortalLastWarpedTo. Returning False.")
			RETURN FALSE
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck34, LBOOL34_USING_INTERIOR_DESTRUCTION)
			IF IS_BIT_SET(iInteriorDestruction_BlockWarpPortalStartPosBS, iPortal)
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - IS_PLAYER_IN_RANGE_OF_PORTAL - (start) Blocked by iInteriorDestruction_BlockWarpPortalStartPosBS. Returning False.")
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF iRule > -1
		AND iRule <= FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_RequiresRuleMidpoint)
			AND NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_PlayerBD[iLocalPart].iTeam], iRule)
				PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - IS_PLAYER_IN_RANGE_OF_PORTAL - (start) ciWARP_PORTAL_RequiresRuleMidpoint is set, and we are not at midpoint. Returning False.")
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_WaitForDialogueToFinish)
		AND IS_CONVERSATION_STATUS_FREE()
		AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - IS_PLAYER_IN_RANGE_OF_PORTAL - (start) We are still listening to dialogue. Returning False.")
			RETURN FALSE
		ENDIF
		
		IF IS_LOCAL_PLAYER_PED_GETTING_IN_OR_OUT_OF_ANY_VEHICLE()
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - IS_PLAYER_IN_RANGE_OF_PORTAL - (start) We are getting in or out of a Vehicle, Returning False.")
			RETURN FALSE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_DisableOnFootTrigger)
		AND NOT IS_PED_IN_ANY_VEHICLE(pedPlayer, TRUE)
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - IS_PLAYER_IN_RANGE_OF_PORTAL - (start) We are not inside a Vehicle, and On Foot triggering is disabled. Returning False.")
			RETURN FALSE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_DisableInVehicleTrigger)
		AND IS_PED_IN_ANY_VEHICLE(pedPlayer, FALSE)
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - IS_PLAYER_IN_RANGE_OF_PORTAL - (start) We are inside a Vehicle, and In Vehicle triggering is disabled. Returning False.")
			RETURN FALSE
		ENDIF
		
		IF NOT MC_HAS_ENTITY_WARP_PORTAL_IS_ATTACHED_TO_SETTLED_START(iPortal)
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - IS_PLAYER_IN_RANGE_OF_PORTAL - (start) Not Settled yet - MC_HAS_ENTITY_WARP_PORTAL_IS_ATTACHED_TO_SETTLED_START Returning False.")
			RETURN FALSE
		ENDIF
		
		IF IS_BIT_SET(sWarpPortal.iJustUsedPortalStartBS, iPortal)
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - IS_PLAYER_IN_RANGE_OF_PORTAL - (start) We have just used this portal. Returning False.")
			RETURN FALSE	
		ELSE
			PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - IS_PLAYER_IN_RANGE_OF_PORTAL - (start) In Range. Returning True.")			
			RETURN TRUE
		ENDIF
	ELSE
		IF iPortal = sWarpPortal.iWarpPortalLastWarpedTo
			sWarpPortal.iWarpPortalLastWarpedTo = -1
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PORTAL_DISABLED_BY_SPECIAL_MEANS_START(INT iPortal)
	IF IS_BIT_SET(iInteriorDestruction_BlockWarpPortalStartPosBS, iPortal)
		RETURN TRUE
	ENDIF
	IF NOT MC_HAS_ENTITY_WARP_PORTAL_IS_ATTACHED_TO_SETTLED_START(iPortal)
		RETURN TRUE
	ENDIF

	INT iRule = GET_TEAM_CURRENT_RULE(MC_PlayerBD[iLocalPart].iTeam)
	IF iRule > -1
	AND iRule <= FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_RequiresRuleMidpoint)
		AND NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_PlayerBD[iLocalPart].iTeam], iRule)			
			RETURN TRUE
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PORTAL_ACTIVE_AND_AVAILABLE(INT iPortal, INT iTeam, INT iRule, BOOL bObjectiveBlocked, VECTOR vStart, VECTOR vEnd #IF IS_DEBUG_BUILD, BOOL bPrints #ENDIF)
	
	#IF IS_DEBUG_BUILD IF bPrints PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - IS_PORTAL_ACTIVE_AND_AVAILABLE - at vStart: ", vStart, " and vEnd: ", vEnd) ENDIF #ENDIF
			
	IF bObjectiveBlocked
	AND IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_DeactivateIfObjectiveBlocked)
		#IF IS_DEBUG_BUILD IF bPrints PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - IS_PORTAL_ACTIVE_AND_AVAILABLE - Objective Blocked. Returning False.") ENDIF #ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_VECTOR_ZERO(vStart)
	OR IS_VECTOR_ZERO(vEnd)
		#IF IS_DEBUG_BUILD IF bPrints PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - IS_PORTAL_ACTIVE_AND_AVAILABLE - Null Cordinates Returning False.") ENDIF #ENDIF
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT.sWarpPortals[iPortal].iActiveOnRule[iTeam] = -1
	AND g_FMMC_STRUCT.sWarpPortals[iPortal].iEndOnRule[iTeam] = -1
		#IF IS_DEBUG_BUILD IF bPrints PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - IS_PORTAL_ACTIVE_AND_AVAILABLE - Any Rule, Returning True.") ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	
	IF (iRule >= g_FMMC_STRUCT.sWarpPortals[iPortal].iActiveOnRule[iTeam] OR g_FMMC_STRUCT.sWarpPortals[iPortal].iActiveOnRule[iTeam] = -1)
	AND (iRule < g_FMMC_STRUCT.sWarpPortals[iPortal].iEndOnRule[iTeam] OR g_FMMC_STRUCT.sWarpPortals[iPortal].iEndOnRule[iTeam] = -1)		
		#IF IS_DEBUG_BUILD IF bPrints PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - IS_PORTAL_ACTIVE_AND_AVAILABLE - Within rule Params.") ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD IF bPrints PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - IS_PORTAL_ACTIVE_AND_AVAILABLE - Not Within rule Params. iRule: ", iRule, " iActiveRule: ", g_FMMC_STRUCT.sWarpPortals[iPortal].iActiveOnRule[iTeam], " iEndRule: ", g_FMMC_STRUCT.sWarpPortals[iPortal].iEndOnRule[iTeam]) ENDIF #ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_INSTANCED_PORTAL_CLIENT_STATE(WARP_PORTAL_STATE eWarpPortalStateNew)
	PRINTLN("[LM][wPortals][wPortal ", sWarpPortal.iWarpPortalEntered, "] - SET_INSTANCED_PORTAL_CLIENT_STATE - Setting Instanced Portal State New: ", ENUM_TO_INT(eWarpPortalStateNew), " From Old: ", ENUM_TO_INT(sWarpPortal.eWarpPortalState))
	sWarpPortal.eWarpPortalState = eWarpPortalStateNew
ENDPROC

// Tells the system that an input trigger has been used
FUNC BOOL HAS_PLAYER_TRIED_TO_ACTIVATE_ANY_WARP_PORTAL()
	
	INT i = 0
	FOR i = 0 TO ciWARP_PORTAL_TRIGGERS_MAX-1		
		IF i = ciWARP_PORTAL_TRIGGERS_NONE
			RELOOP
		ENDIF
		IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, GET_CONTROL_ACTION_FROM_TRIGGER_OPTION(i))
			RESET_NET_TIMER(sWarpPortal.tdWarpPortalActivation)
			START_NET_TIMER(sWarpPortal.tdWarpPortalActivation)
			sWarpPortal.eTriggerTypePressed = GET_CONTROL_ACTION_FROM_TRIGGER_OPTION(i)
			RETURN TRUE
		ENDIF
	ENDFOR	
	
	IF HAS_NET_TIMER_STARTED(sWarpPortal.tdWarpPortalActivation)
		IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(sWarpPortal.tdWarpPortalActivation, 300)
			RETURN TRUE
		ENDIF
	ENDIF
	
	sWarpPortal.eTriggerTypePressed = MAX_INPUTS
	
	RETURN FALSE
ENDFUNC

// Checks to see if a portal has to be triggered and can be triggered at that moment
FUNC BOOL CAN_INSTANCED_CONTENT_PORTALS_TRIGGER_BE_USED(INT iPortal)
	
	IF IS_LOCAL_PLAYER_PED_GETTING_IN_OR_OUT_OF_ANY_VEHICLE()
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_DisableOnFootTrigger)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_DisableInVehicleTrigger)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, FALSE)
			VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(localPlayerPed)
			IF IS_VEHICLE_DRIVEABLE(vehIndex)
				IF GET_PED_IN_VEHICLE_SEAT(vehIndex, VS_DRIVER) != LocalPlayerPed				
					RETURN FALSE
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
				
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_DisableOnFootTrigger)
	AND IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_DisableInVehicleTrigger)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)		
			RETURN FALSE
		ENDIF
	ENDIF
		
	IF IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL)
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SELECT_WEAPON)
		RETURN FALSE
	ENDIF	
		
	IF IS_PAUSE_MENU_ACTIVE()
		RETURN FALSE
	ENDIF
	
	IF IS_WEB_BROWSER_OPEN()
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		RETURN FALSE
	ENDIF
	
	IF IS_PI_MENU_OPEN()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC TEXT_LABEL_15 GET_HELP_TEXT_TO_USE_FOR_WARP_PORTAL_ENTRY(INT iPortal)
	TEXT_LABEL_15 tl15
	
	IF IS_CONTROL_ACTION_FROM_WARP_PORTAL_TRIGGER_OPTION_SET(iPortal)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS2, ciWARP_PORTAL_2_HideTriggerHelpText)
			IF g_FMMC_STRUCT.sWarpPortals[iPortal].iCustomPromptLabel[FMMC_WARP_PORTAL_CUSTOM_PROMPT_LABEL_TRIGGER_START] != -1
				tl15 = GET_CUSTOM_STRING_LIST_TEXT_LABEL(g_FMMC_STRUCT.sWarpPortals[iPortal].iCustomPromptLabel[FMMC_WARP_PORTAL_CUSTOM_PROMPT_LABEL_TRIGGER_START])
			ELSE
				tl15 = GET_HELP_TEXT_FOR_WARP_PORTAL_TRIGGER_TYPE_START(iPortal)
			ENDIF
		ELSE
			tl15 = ""
		ENDIF
	ENDIF
			
	RETURN tl15
ENDFUNC

PROC CLEAR_WARP_PORTAL_HELP_TEXT(INT iPortal)
	TEXT_LABEL_15 tl15
	
	// Clear Start
	IF IS_BIT_SET(sWarpPortal.iHelpTextPortalBS, iPortal)
		tl15 = GET_HELP_TEXT_TO_USE_FOR_WARP_PORTAL_ENTRY(iPortal)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tl15)	
			CLEAR_HELP(TRUE)
			CLEAR_BIT(sWarpPortal.iHelpTextPortalBS, iPortal)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_WARP_PORTAL_FADEOUT_DUE_TO_REMOTE_PLAYER()

	INT iWarpPortalPart = -1	
	IF HAS_NET_TIMER_STARTED(sWarpPortal.tdRemoteFadeSafetyTimer)
	AND HAS_NET_TIMER_EXPIRED_READ_ONLY(sWarpPortal.tdRemoteFadeSafetyTimer, 10000)
		IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_FADE_OUT_DUE_TO_WARP_PORTAL_USED_BY_REMOTE_PLAYER)
			PRINTLN("[LM][wPortals] - PROCESS_WARP_PORTAL_FADEOUT_DUE_TO_REMOTE_PLAYER - tdRemoteFadeSafetyTimer timer hit, fading screen back in!")
			DO_SCREEN_FADE_IN(500)
			CLEAR_BIT(iLocalBoolCheck29, LBOOL29_FADE_OUT_DUE_TO_WARP_PORTAL_USED_BY_REMOTE_PLAYER)
		ENDIF
		RESET_NET_TIMER(sWarpPortal.tdRemoteFadeSafetyTimer)
		RESET_NET_TIMER(sWarpPortal.tdRemoteFadeLoadingBufferPeriod)
		CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet2, iABI2_BLOCK_SPECTATOR_TARGET_COPY_FADING_IN)
		EXIT
	ENDIF
	
	PED_INDEX pedDriver
	PLAYER_INDEX niPlayer
	
	IF bIsAnySpectator		
		iWarpPortalPart = iPartToUse		
	ELSE		
		IF IS_PED_IN_ANY_VEHICLE(localPlayerPed, TRUE)
		AND GET_SEAT_PED_IS_IN(localPlayerPed) != VS_DRIVER
			VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(localPlayerPed, TRUE)
			pedDriver = GET_PED_IN_VEHICLE_SEAT(vehIndex)		
			IF NOT IS_PED_INJURED(pedDriver)			
				niPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedDriver)			
				IF niPlayer != INVALID_PLAYER_INDEX()
				AND NETWORK_IS_PLAYER_ACTIVE(niPlayer)
				AND NETWORK_IS_PLAYER_A_PARTICIPANT(niPlayer)
				AND NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(niPlayer))						
					iWarpPortalPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(niPlayer))					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iWarpPortalPart = -1
	AND NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_FADE_OUT_DUE_TO_WARP_PORTAL_USED_BY_REMOTE_PLAYER)	
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_FADE_OUT_DUE_TO_WARP_PORTAL_USED_BY_REMOTE_PLAYER)	
	AND iWarpPortalPart > -1
	AND MC_PlayerBD[iWarpPortalPart].iWarpPortalEntered != -1	
		IF NOT IS_SCREEN_FADED_OUT()
			PRINTLN("[LM][wPortals][wPortal ", MC_PlayerBD[iWarpPortalPart].iWarpPortalEntered, "] - PROCESS_WARP_PORTAL_FADEOUT_DUE_TO_REMOTE_PLAYER - Fading the screen out. iWarpPortalPart: ", iWarpPortalPart, " MC_PlayerBD[iWarpPortalPart].iWarpPortalEntered: ", MC_PlayerBD[iWarpPortalPart].iWarpPortalEntered)
			DO_SCREEN_FADE_OUT(125)
			SET_BIT(iLocalBoolCheck29, LBOOL29_FADE_OUT_DUE_TO_WARP_PORTAL_USED_BY_REMOTE_PLAYER)
			SET_BIT(MPGlobalsAmbience.iAmbBitSet2, iABI2_BLOCK_SPECTATOR_TARGET_COPY_FADING_IN)
			REINIT_NET_TIMER(sWarpPortal.tdRemoteFadeSafetyTimer)
			REINIT_NET_TIMER(sWarpPortal.tdRemoteFadeLoadingBufferPeriod)
		ENDIF
	ELSE
		IF iWarpPortalPart = -1
		OR MC_PlayerBD[iWarpPortalPart].iWarpPortalEntered = -1	
			IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_FADE_OUT_DUE_TO_WARP_PORTAL_USED_BY_REMOTE_PLAYER)			
			AND HAS_NET_TIMER_EXPIRED_READ_ONLY(sWarpPortal.tdRemoteFadeLoadingBufferPeriod, 5000)
				#IF IS_DEBUG_BUILD
					IF iWarpPortalPart > -1
						PRINTLN("[LM][wPortals][wPortal ", MC_PlayerBD[iWarpPortalPart].iWarpPortalEntered, "] - PROCESS_WARP_PORTAL_FADEOUT_DUE_TO_REMOTE_PLAYER - Fading the screen back in. iWarpPortalPart: ", iWarpPortalPart, " MC_PlayerBD[iWarpPortalPart].iWarpPortalEntered: ", MC_PlayerBD[iWarpPortalPart].iWarpPortalEntered)
					ELSE
						PRINTLN("[LM][wPortals][wPortal ", MC_PlayerBD[iWarpPortalPart].iWarpPortalEntered, "] - PROCESS_WARP_PORTAL_FADEOUT_DUE_TO_REMOTE_PLAYER - Fading the screen back in. iWarpPortalPart no longer valid (-1)")
					ENDIF
				#ENDIF
				DO_SCREEN_FADE_IN(500)
				CLEAR_BIT(iLocalBoolCheck29, LBOOL29_FADE_OUT_DUE_TO_WARP_PORTAL_USED_BY_REMOTE_PLAYER)
				RESET_NET_TIMER(sWarpPortal.tdRemoteFadeLoadingBufferPeriod)
				RESET_NET_TIMER(sWarpPortal.tdRemoteFadeSafetyTimer)
				CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet2, iABI2_BLOCK_SPECTATOR_TARGET_COPY_FADING_IN)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_INSTANCED_CONTENT_PORTALS(BOOL bObjectiveBlocked = FALSE)

	IF sWarpPortal.iMaxNumberOfPortals = 0
		EXIT
	ENDIF
	
	PROCESS_WARP_PORTAL_FADEOUT_DUE_TO_REMOTE_PLAYER()
	
	IF g_bMissionEnding
	OR NOT g_bMissionClientGameStateRunning
	OR NOT IS_INSTANCED_CONTENT_COUNTDOWN_FINISHED()
	OR bIsAnySpectator
		CLEAN_UP_INSTANCED_CONTENT_PORTAL_WHITEOUT()
		EXIT
	ENDIF
	
	INT iTeam, iRule, iPortal
	iRule = GET_CURRENT_MC_TEAM_AND_RULE_FROM_GLOBAL_DATA(iTeam)
	IF iRule < FMMC_MAX_RULES
		
		BOOL bActivation = HAS_PLAYER_TRIED_TO_ACTIVATE_ANY_WARP_PORTAL()
		
		IF sWarpPortal.iWarpPortalEntered > -1 
			iPortal = sWarpPortal.iWarpPortalEntered
		ELSE
			iPortal = sWarpPortal.iPortalStaggeredLoop 
		ENDIF
		
		VECTOR vStart = MC_GET_WARP_PORTAL_START_POSITION(iPortal)
		VECTOR vEnd = MC_GET_WARP_PORTAL_END_POSITION(iPortal)

		SWITCH sWarpPortal.eWarpPortalState
			CASE eWarpPortalState_Idle
				
				IF IS_PORTAL_ACTIVE_AND_AVAILABLE(iPortal, iTeam, iRule, bObjectiveBlocked, vStart, vEnd #IF IS_DEBUG_BUILD , TRUE #ENDIF)
					
					PROCESS_PRELOADING_VEHICLE_MODEL_FOR_WARP_PORTAL(iPortal, TRUE)
					
					PROCESS_ACTIVATING_AUTO_WARP_TIMER(iPortal)
					
					IF IS_PLAYER_IN_RANGE_OF_PORTAL(iPortal, vStart)					
					
						IF HAVE_CONTROLS_CHANGED(PLAYER_CONTROL)
							CLEAR_WARP_PORTAL_HELP_TEXT(iPortal)
						ENDIF						
						
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
						
						IF IS_CONTROL_ACTION_FROM_WARP_PORTAL_TRIGGER_OPTION_SET(iPortal)
						
							IF NOT IS_CONTROL_ACTION_FROM_WARP_PORTAL_TRIGGER_OPTION_TRIGGERED(sWarpPortal, iPortal)
								bActivation = FALSE
							ENDIF
							
							IF CAN_INSTANCED_CONTENT_PORTALS_TRIGGER_BE_USED(iPortal)								
								TEXT_LABEL_15 tl15
								tl15 = GET_HELP_TEXT_TO_USE_FOR_WARP_PORTAL_ENTRY(iPortal)
								
								IF NOT IS_STRING_NULL_OR_EMPTY(tl15)						
								AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED() 
									PRINT_HELP_FOREVER(tl15)
									SET_BIT(sWarpPortal.iHelpTextPortalBS, iPortal)
									PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - PROCESS_INSTANCED_CONTENT_PORTALS - Printing Help Text")
								ENDIF
							ELSE
								CLEAR_WARP_PORTAL_HELP_TEXT(iPortal)
								bActivation = FALSE
							ENDIF
						ELSE
							bActivation = TRUE
						ENDIF
						
						IF DOES_WARP_PORTAL_IGNORE_RANGE_TRIGGER(iPortal)
							PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - PROCESS_INSTANCED_CONTENT_PORTALS - Auto activating warp portal (DOES_WARP_PORTAL_IGNORE_RANGE_TRIGGER)")
							SET_BIT(sWarpPortal.iWarpPortalBitset_AutoWarpedFromStart, iPortal)
							bActivation = TRUE
						ENDIF
						
						IF bActivation
							PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - PROCESS_INSTANCED_CONTENT_PORTALS - Assigning sWarpPortal.iWarpPortalEntered")
							sWarpPortal.iWarpPortalEntered = iPortal							
							sWarpPortal.iWarpPortalLastWarpedTo = g_FMMC_STRUCT.sWarpPortals[iPortal].iLinkedPortal
							INITIALISE_KEEP_MOMENTUM_FROM_WARP_PORTAL_DATA(iPortal)
							IF NOT IS_WARP_PORTAL_USING_TRANSFORM(iPortal)	
							AND NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS2, ciWARP_PORTAL_2_MaintainMomentumAfterWarp)
								NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							ENDIF
							CLEAR_WARP_PORTAL_HELP_TEXT(iPortal)							
							SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_Init)
						ENDIF
					ELSE
						CLEAR_WARP_PORTAL_HELP_TEXT(iPortal)												
						#IF IS_DEBUG_BUILD
						IF IS_BIT_SET(sWarpPortal.iJustUsedPortalStartBS, iPortal)
							PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - PROCESS_INSTANCED_CONTENT_PORTALS - Clearing iJustUsedPortalStartBS. (a)")
						ENDIF
						#ENDIF
						CLEAR_BIT(sWarpPortal.iJustUsedPortalStartBS, iPortal)
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					IF IS_BIT_SET(sWarpPortal.iJustUsedPortalStartBS, iPortal)
						PRINTLN("[LM][wPortals][wPortal ", iPortal, "] - PROCESS_INSTANCED_CONTENT_PORTALS - Clearing iJustUsedPortalStartBS. (b)")
					ENDIF
					#ENDIF
					CLEAR_BIT(sWarpPortal.iJustUsedPortalStartBS, iPortal)
					
					PROCESS_PRELOADING_VEHICLE_MODEL_FOR_WARP_PORTAL(iPortal, FALSE)
				ENDIF				
			BREAK
			
			CASE eWarpPortalState_Init
				sWarpPortal.vTargetLocation = <<0.0, 0.0, 0.0>>
				sWarpPortal.fTargetHead = 0.0				
				IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_FadeOutBeforeWarp)
				OR IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_PostFXBeforeFade)
				OR IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS2, ciWARP_PORTAL_2_WhiteOutBeforeWarp)
				OR NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord)
					SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_Preload)
				ELIF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS2, ciWARP_PORTAL_2_TripSkipStyleCam)
					SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_FlyCamera)
				ELSE
					SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_Warping)
				ENDIF
			BREAK
			
			CASE eWarpPortalState_Preload				
				IF HAS_PRELOAD_WARP_PORTAL_FINISHED(iPortal)
					IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS2, ciWARP_PORTAL_2_TripSkipStyleCam)			
						SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_FlyCamera)
					ELIF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord)
						SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_Cutscene)
					ELSE
						SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_Warping)
					ENDIF					
				ENDIF
			BREAK
			
			CASE eWarpPortalState_Cutscene										
				IF HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED(iPortal)
					SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_Warping)
				ENDIF
				IF IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationStarted)
					PROCESS_WARP_PORTAL_HIDE_OTHER_PLAYERS(TRUE)
					PROCESS_WARP_PORTAL_HIDE_ENTITIES_IN_AREA(vStart, 15.0)
				ENDIF
			BREAK
			
			CASE eWarpPortalState_FlyCamera
				IF HAS_FLY_CAMERA_FOR_WARP_PORTAL_FINISHED(iPortal, vStart, vEnd)
					SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_Warping)
				ENDIF
			BREAK
			
			CASE eWarpPortalState_Warping
				IF WARP_PLAYER_USING_INSTANCED_CONTENT_PORTAL(iPortal, vStart, vEnd)
					PRINTLN("[wPortals][wPortal ", iPortal, "] - PROCESS_INSTANCED_CONTENT_PORTALS - Setting LBOOL_RESET1_FULL_ZONES_AND_BOUNDS_LOOP_REQUIRED")
					SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_FULL_ZONES_AND_BOUNDS_LOOP_REQUIRED)
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS2, ciWARP_PORTAL_2_TripSkipStyleCam)					
						SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_FlyCamera_BackIn)
					ELSE
						SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_Sync)
					ENDIF
				ENDIF
			BREAK
			
			CASE eWarpPortalState_FlyCamera_BackIn
				IF HAS_FLY_CAMERA_BACK_IN_FOR_WARP_PORTAL_FINISHED(iPortal, sWarpPortal.vTargetLocation, sWarpPortal.fTargetHead)
					SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_Sync)
				ENDIF
			BREAK
			
			CASE eWarpPortalState_Sync
				IF HAS_WARP_PORTAL_SYNC_STAGE_FINISHED(iPortal)
					STRING sSoundNameEnd
					STRING sSoundSetEnd 
					sSoundNameEnd = GET_ENTRY_SOUND_NAME_END_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType)
					sSoundSetEnd = GET_ENTRY_SOUND_SET_END_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType)
					
					IF NOT IS_STRING_NULL_OR_EMPTY(sSoundNameEnd)
					AND NOT IS_STRING_NULL_OR_EMPTY(sSoundSetEnd)
						PLAY_SOUND_FRONTEND(-1, sSoundNameEnd, sSoundSetEnd)
					ENDIF
					
					NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
					SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_Cleanup)
				ENDIF
			BREAK			
			
			CASE eWarpPortalState_Cleanup
				RESET_NET_TIMER(sWarpPortal.tdWarpPortalCooldown)
				START_NET_TIMER(sWarpPortal.tdWarpPortalCooldown)				 
				sWarpPortal.iWarpPortalEntered = -1
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_USING_WARP_PORTAL_FLY_CAM_BACK_IN)
				sWarpPortal.PMS_Cached = MS_ON_FOOT_IDLE
				CLEAR_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_WasSkyDivingKeepMomentum)
				CLEAN_UP_INSTANCED_CONTENT_PORTAL_WHITEOUT()
				SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_Idle)
			BREAK
		ENDSWITCH
		
		IF sWarpPortal.eWarpPortalState != eWarpPortalState_Cutscene
		AND sWarpPortal.eWarpPortalState != eWarpPortalState_Warping
			BOOL bActive
			INT iPortalEveryFrame
			FOR iPortalEveryFrame = 0 TO sWarpPortal.iMaxNumberOfPortals-1
				bActive = FALSE
				
				vStart = MC_GET_WARP_PORTAL_START_POSITION(iPortalEveryFrame)
				vEnd = MC_GET_WARP_PORTAL_END_POSITION(iPortalEveryFrame)
				
				IF IS_PORTAL_ACTIVE_AND_AVAILABLE(iPortalEveryFrame, iTeam, iRule, bObjectiveBlocked, vStart, vEnd #IF IS_DEBUG_BUILD , FALSE #ENDIF)
					bActive = TRUE
				ENDIF
				
				IF IS_PORTAL_DISABLED_BY_SPECIAL_MEANS_START(iPortalEveryFrame)
					PRINTLN("[LM][wPortals][wPortal ", iPortalEveryFrame, "] - PROCESS_INSTANCED_CONTENT_PORTALS - IS_PORTAL_DISABLED_BY_SPECIAL_MEANS_START = TRUE")
					bActive = FALSE
				ENDIF
				
				IF iPortalEveryFrame = sWarpPortal.iWarpPortalLastWarpedTo
					PRINTLN("[LM][wPortals][wPortal ", iPortalEveryFrame, "] - PROCESS_INSTANCED_CONTENT_PORTALS - This portal is equal to sWarpPortal.iWarpPortalLastEntered, we haven't left it's area yet.")
					bActive = FALSE
				ENDIF
				
				PROCESS_WARP_PORTAL_DISPLAY(iPortalEveryFrame, bActive, vStart)
			ENDFOR
		ENDIF
		
		IF sWarpPortal.iWarpPortalEntered = -1 
			sWarpPortal.iPortalStaggeredLoop++
			IF sWarpPortal.iPortalStaggeredLoop >= sWarpPortal.iMaxNumberOfPortals
				sWarpPortal.iPortalStaggeredLoop = 0
			ENDIF
		ENDIF
	ENDIF
	
	IF sWarpPortal.iWarpPortalEntered != MC_PlayerBD[iLocalPart].iWarpPortalEntered
		PRINTLN("[LM][wPortals] - PROCESS_INSTANCED_CONTENT_PORTALS - Updating iWarpPortalEntered to: ", sWarpPortal.iWarpPortalEntered)
		MC_PlayerBD[iLocalPart].iWarpPortalEntered = sWarpPortal.iWarpPortalEntered
	ENDIF
		
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Interiors
// ##### Description: Contains Wrappers and the main process functions for loading and setting up the Interiors.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC ADD_INTERIOR_TO_CHECK(INT &iInterior, INTERIOR_INSTANCE_INDEX &iInteriorToCheck[], INTERIOR_INSTANCE_INDEX iInteriorToAdd)
	IF (iInterior + 1) < ci_MAX_INTERIORS_TO_LOAD
		PRINTLN("[LM][SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER][ADD_INTERIOR_TO_CHECK] iInterior: ", iInterior, " Is being assigned to.")
		iInteriorToCheck[iInterior] = iInteriorToAdd
		iInterior++
	ELSE
		PRINTLN("[LM][SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER][ADD_INTERIOR_TO_CHECK] iInterior: ", iInterior, " We are at max interiors we can check. This is concerning.")
	ENDIF
ENDPROC

FUNC BOOL ARE_ADDED_INTERIORS_READY(INTERIOR_INSTANCE_INDEX &iInteriorToCheck[], INT iMaxToCheck)
	
	INT iInterior
	FOR iInterior = 0 TO iMaxToCheck
		IF IS_VALID_INTERIOR(iInteriorToCheck[iInterior])
			IF NOT IS_INTERIOR_READY(iInteriorToCheck[iInterior])
				PRINTLN("[LM][SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER][ARE_ADDED_INTERIORS_READY] iInterior: ", iInterior, " Is NOT READY")
			
				IF NOT HAS_NET_TIMER_STARTED(tdInitialIPLSetupSafetyTimeout)
					PRINTLN("[LM][SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER][ARE_ADDED_INTERIORS_READY] Starting Safety Timer.")
					START_NET_TIMER(tdInitialIPLSetupSafetyTimeout)
				ENDIF
				
				IF HAS_NET_TIMER_STARTED(tdInitialIPLSetupSafetyTimeout)
				AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdInitialIPLSetupSafetyTimeout, ci_IPL_INTERIOR_LOAD_SAFETY_TIMEOUT)
					
					#IF IS_DEBUG_BUILD
					PRINTLN("[LM][SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER][ARE_ADDED_INTERIORS_READY] BAILING - SAFETY TIMER HAS EXPIRED, SOMETHING WENT WRONG LOADING INTERIORS!!")
					ASSERTLN("[LM][SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER][ARE_ADDED_INTERIORS_READY] BAILING - SAFETY TIMER HAS EXPIRED, SOMETHING WENT WRONG LOADING INTERIORS!!")
					ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_INTERIORS_LOAD_FAILSAFE_HIT, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "Interiors Safety Timer was hit. An interior was stuck getting ready.")
					#ENDIF
					
					RETURN TRUE
				ELSE
					RETURN FALSE
				ENDIF				
			ENDIF
		ENDIF
	ENDFOR
	
	RESET_NET_TIMER(tdInitialIPLSetupSafetyTimeout)	
	
	PRINTLN("[LM][SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER][ARE_ADDED_INTERIORS_READY] Returning True.")
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_THERE_ANY_ADDED_INTERIORS_TO_CHECK(INT iInteriorsToCheck)
	IF iInteriorsToCheck > 0
		PRINTLN("[LM][SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER][ARE_THERE_ANY_ADDED_INTERIORS_TO_CHECK] There are valid interiors that we should check are ready.")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[LM][SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER][ARE_THERE_ANY_ADDED_INTERIORS_TO_CHECK] There are 'NO' valid interiors that we should check are ready.")
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING GET_IPL_TYPE_NAME(INT iIPLType)
	
	SWITCH iIPLType
		CASE ciFMMC_IPLTYPE_Island	RETURN "Island"
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC
#ENDIF

PROC SET_IPL_FROM_CONTINUITY(INT iIPLType, INT iConditionalIPL)
	
	IF g_FMMC_STRUCT.sIPLOptions.iIPLLinkedContinuityEntityType[iIPLType][iConditionalIPL] = -1
	OR g_FMMC_STRUCT.sIPLOptions.iIPLLinkedContinuityEntityID[iIPLType][iConditionalIPL] = -1
		EXIT
	ENDIF
	
	PRINTLN("SET_IPL_FROM_CONTINUITY - iIPLType: ", iIPLType, " iConditionalIPL: ", iConditionalIPL)
	
	IF g_FMMC_STRUCT.sIPLOptions.iIPLLinkedContinuityEntityType[iIPLType][iConditionalIPL] = ciCONTINUITY_ENTITY_TYPE_LOCATION
	AND IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_LocationTracking)
	AND NOT IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iLocationTrackingBitset, g_FMMC_STRUCT.sIPLOptions.iIPLLinkedContinuityEntityID[iIPLType][iConditionalIPL])
		PRINTLN("[InitIPL][CONTINUITY] - SET_IPL_FROM_CONTINUITY - not enabling ",GET_IPL_TYPE_NAME(iIPLType)," IPL ",iConditionalIPL," due to location continuity")
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.sIPLOptions.iIPLLinkedContinuityEntityType[iIPLType][iConditionalIPL] = ciCONTINUITY_ENTITY_TYPE_OBJECT
	AND IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ObjectTracking)
	AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iObjectTrackingBitset, g_FMMC_STRUCT.sIPLOptions.iIPLLinkedContinuityEntityID[iIPLType][iConditionalIPL])
		PRINTLN("[InitIPL][CONTINUITY] - SET_IPL_FROM_CONTINUITY - not enabling ",GET_IPL_TYPE_NAME(iIPLType)," IPL ",iConditionalIPL," due to object continuity")
		EXIT
	ENDIF
	
	SWITCH iIPLType
		CASE ciFMMC_IPLTYPE_Island
			SET_BIT(g_FMMC_STRUCT.sIPLOptions.iIslandIPL, g_FMMC_STRUCT.sIPLOptions.iIPLLinkedIPLIndex[iIPLType][iConditionalIPL])
			PRINTLN("[InitIPL][CONTINUITY] - SET_IPL_FROM_CONTINUITY - Setting g_FMMC_STRUCT.sIPLOptions.iIslandIPL bit ", g_FMMC_STRUCT.sIPLOptions.iIPLLinkedIPLIndex[iIPLType][iConditionalIPL])
		BREAK
	ENDSWITCH
ENDPROC

PROC CHECK_FOR_IPL_CONTINUITY()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_LocationTracking)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ObjectTracking)
		PRINTLN("[InitIPL][CONTINUITY] - CHECK_FOR_IPL_CONTINUITY - No relevant continuity options are set - exiting")
		EXIT
	ENDIF
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		PRINTLN("[InitIPL][CONTINUITY] - CHECK_FOR_IPL_CONTINUITY - Running in fake multiplayer - exiting")
		EXIT
	ENDIF
	
	INT iIPLType, iConditionalIPL
	FOR iIPLType = 0 TO ciFMMC_IPLTYPE_Max-1
		FOR iConditionalIPL = 0 TO ciFMMC_IPLMaxConditional-1
			SET_IPL_FROM_CONTINUITY(iIPLType, iConditionalIPL)
		ENDFOR
	ENDFOR
	
ENDPROC

FUNC BOOL INITIALIZE_SPECIAL_ASSETS_FOR_EXTERNAL_CUTSCENE_SEQUENCE_BUNKER(INT iCutscene)
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		RETURN TRUE
	ENDIF
	
	// Only do below if in creator.
	
	SIMPLE_INTERIORS simpInterior
	
	SWITCH g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].eExternalCutsceneSequence
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_1
			simpInterior = SIMPLE_INTERIOR_BUNKER_1
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_2
			simpInterior = SIMPLE_INTERIOR_BUNKER_2
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_3
			simpInterior = SIMPLE_INTERIOR_BUNKER_3
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_4
			simpInterior = SIMPLE_INTERIOR_BUNKER_4
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_5
			simpInterior = SIMPLE_INTERIOR_BUNKER_5
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_6
			simpInterior = SIMPLE_INTERIOR_BUNKER_6
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_7
			simpInterior = SIMPLE_INTERIOR_BUNKER_7
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_9
			simpInterior = SIMPLE_INTERIOR_BUNKER_9
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_10
			simpInterior = SIMPLE_INTERIOR_BUNKER_10
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_11
			simpInterior = SIMPLE_INTERIOR_BUNKER_11
		BREAK
		CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_12
			simpInterior = SIMPLE_INTERIOR_BUNKER_12
		BREAK
	ENDSWITCH
	
	IF simpInterior != simpInteriorCache
		simpInteriorCache = simpInterior
		PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Bunker] - INITIALIZE_SPECIAL_ASSETS_FOR_EXTERNAL_CUTSCENE_SEQUENCE_BUNKER Caching simpInteriorCache to: ", ENUM_TO_INT(simpInteriorCache))
	ENDIF
	
	//IF HAVE_BUNKER_CUSTOM_CUTSCENE_ASSETS_LOADED(simpInterior, entryAnimExtCut)
	IF BUNKER_CUTSCENE_CREATE_ENTRY_DOOR(simpInterior, oibunkerEntryDoor, oiBunkerEntryFrame)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL INITIALIZE_SPECIAL_ASSETS_FOR_EXTERNAL_CUTSCENE_SEQUENCE()
					
	INT iCutscene = 0
	FOR iCutscene = 0 TO _POST_CASINO_HEIST__MAX_SCRIPTED_CUTSCENES-1
	
		BOOL bReloopCutscene = TRUE
		INT iTeam, iPlayerRule
		FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
			IF NOT bReloopCutscene
				BREAKLOOP
			ENDIF
			
			FOR iPlayerRule = 0 TO FMMC_MAX_RULES-1
				IF MC_serverBD.iPlayerRuleLimit[iPlayerRule][iteam] = iCutscene // a valid cutscene.
					bReloopCutscene = FALSE
					BREAKLOOP
				ENDIF
			ENDFOR
		ENDFOR
		
		IF bReloopCutscene
			PRINTLN("INITIALIZE_SPECIAL_ASSETS_FOR_EXTERNAL_CUTSCENE_SEQUENCE Cutscene is not selected/valid on any rule. Skip this one. iCutscene: ", iCutscene)
			RELOOP
		ENDIF
		
		PRINTLN("INITIALIZE_SPECIAL_ASSETS_FOR_EXTERNAL_CUTSCENE_SEQUENCE Initializing special assets for iCutscene: ", iCutscene)
		
		SWITCH g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].eExternalCutsceneSequence
			CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_1
			CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_2
			CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_3
			CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_4
			CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_5
			CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_6
			CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_7
			CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_9
			CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_10
			CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_11
			CASE EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_BUNKER_ENTRY_12
				IF NOT INITIALIZE_SPECIAL_ASSETS_FOR_EXTERNAL_CUTSCENE_SEQUENCE_BUNKER(iCutscene)
					PRINTLN("INITIALIZE_SPECIAL_ASSETS_FOR_EXTERNAL_CUTSCENE_SEQUENCE Assets not ready for iCutscene: ", iCutscene)
					RETURN FALSE
				ENDIF
			BREAK
		ENDSWITCH
	ENDFOR
	
	PRINTLN("INITIALIZE_SPECIAL_ASSETS_FOR_EXTERNAL_CUTSCENE_SEQUENCE Assets are all ready now or none are necessary to load.")
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_FIXER_AGENCY_SIGN_IPLS()
	
	PLAYER_INDEX piLobbyLeader = GET_FMMC_LOBBY_LEADER()
	
	IF piLobbyLeader = INVALID_PLAYER_INDEX()
		EXIT
	ENDIF
	
	IF NOT NETWORK_IS_PLAYER_ACTIVE(piLobbyLeader)
		EXIT
	ENDIF
	
	SWITCH GET_PLAYERS_OWNED_FIXER_HQ(piLobbyLeader)
		CASE FIXER_HQ_HAWICK
			REQUEST_IPL("sf_plaque_hw1_08")
			REMOVE_IPL("sf_plaque_bh1_05")
			REMOVE_IPL("sf_plaque_kt1_08")
			REMOVE_IPL("sf_plaque_kt1_05")
			PRINTLN("PROCESS_FIXER_AGENCY_SIGN_IPLS - Setting fixer IPL based on lobby leader to: FIXER_HQ_HAWICK")
		BREAK
		CASE FIXER_HQ_ROCKFORD
			REQUEST_IPL("sf_plaque_bh1_05")
			REMOVE_IPL("sf_plaque_hw1_08")
			REMOVE_IPL("sf_plaque_kt1_08")
			REMOVE_IPL("sf_plaque_kt1_05")
			PRINTLN("PROCESS_FIXER_AGENCY_SIGN_IPLS - Setting fixer IPL based on lobby leader to: FIXER_HQ_ROCKFORD")
		BREAK
		CASE FIXER_HQ_SEOUL
			REQUEST_IPL("sf_plaque_kt1_08")
			REMOVE_IPL("sf_plaque_hw1_08")
			REMOVE_IPL("sf_plaque_bh1_05")
			REMOVE_IPL("sf_plaque_kt1_05")
			PRINTLN("PROCESS_FIXER_AGENCY_SIGN_IPLS - Setting fixer IPL based on lobby leader to: FIXER_HQ_SEOUL")
		BREAK
		CASE FIXER_HQ_VESPUCCI
			REQUEST_IPL("sf_plaque_kt1_05")
			REMOVE_IPL("sf_plaque_hw1_08")
			REMOVE_IPL("sf_plaque_bh1_05")
			REMOVE_IPL("sf_plaque_kt1_08")
			PRINTLN("PROCESS_FIXER_AGENCY_SIGN_IPLS - Setting fixer IPL based on lobby leader to: FIXER_HQ_VESPUCCI")
		BREAK
	ENDSWITCH
	
ENDPROC

//Deal with the swapping of the IPLs at the start of the mission controller
PROC SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER()
		
	//If a load scene is active get out	
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		PRINTLN("[MC_Interiors][ARENA] SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER load scene active")
		EXIT
	ENDIF
	
	BOOL bCheckPreviousMission = FALSE
	
	IF (HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET() OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED())
	AND (g_TransitionSessionNonResetVars.iFMMCiInteriorBS2 != 0 OR g_TransitionSessionNonResetVars.iIPLOptions != 0)
		bCheckPreviousMission = TRUE
		PRINTLN("[MC_Interiors] - SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER - using previous mission interiors  - ", g_TransitionSessionNonResetVars.iFMMCiInteriorBS2)
		PRINTLN("[MC_Interiors] - SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER - using previous mission IPLs  - ", g_TransitionSessionNonResetVars.iIPLOptions)
	ENDIF
	
	INTERIOR_INSTANCE_INDEX iInteriorToCheck[ci_MAX_INTERIORS_TO_LOAD]
	INT iInteriorsToCheck = 0
		
	//Needs longer to load and a special update...
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_YACHT_AQUARIS)
			YACHT_DATA YachtData    
			YachtData.iLocation = 34            // Paleto Bay
			YachtData.Appearance.iOption = 2    // The Aquaris
			YachtData.Appearance.iTint = 12     // Vintage
			YachtData.Appearance.iLighting = 5 // Blue Vivacious
			YachtData.Appearance.iRailing = 1   // Gold Fittings
			YachtData.Appearance.iFlag = -1     // no flag 
			//FILL_YACHT_MODEL_TRANSLATION		<<-777.487,6566.91,5.42995>>
			IF NOT IS_BIT_SET(iLocalBoolCheck19, LBOOL19_YACHT_AQUARIS_ASSETS_LOADED)
				REQUEST_ASSETS_FOR_MISSION_CREATOR_YACHT(YachtData)
				
				SET_BIT(iLocalBoolCheck19, LBOOL19_YACHT_AQUARIS_ASSETS_REQUESTED)
				
				IF HAVE_ASSETS_LOADED_FOR_MISSION_CREATOR_YACHT(YachtData)
		        	CREATE_YACHT_FOR_MISSION_CREATOR(YachtData)
					
					SET_BIT(iLocalBoolCheck19, LBOOL19_YACHT_AQUARIS_ASSETS_LOADED)
				ENDIF
			ELSE
				BOOL bRuleDelayed
				
				INT iTeam = MC_playerBD[iPartToUse].iteam			
				INT iRule = 0
				FOR iRule = 0 TO FMMC_MAX_RULES-1
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_SHOW_YACHT_BLIP_THIS_RULE)
						bRuleDelayed = TRUE
						PRINTLN("[LM][UPDATE_YACHT_FOR_MISSION_CREATOR] - Not creating Yacht Blip on INIT, instead we will do it on Rule: ", iRule)
						BREAKLOOP
					ENDIF				
				ENDFOR
				
				IF NOT bRuleDelayed
					PRINTLN("[LM][UPDATE_YACHT_FOR_MISSION_CREATOR] - Creating Yacht on INIT")
					UPDATE_YACHT_FOR_MISSION_CREATOR(YachtData)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF MISSION_CONTAINS_A_PLACED_YACHT()
		
		INT iYachtIndex
		BOOL bAllYachtsFullySpawned = TRUE
		
		FOR iYachtIndex = 0 TO ciYACHT_MAX_YACHTS - 1
			IF g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_ModelIndex = ciYACHT_MODEL__OFF
				RELOOP
			ELSE
				PROCESS_FMMC_YACHT(iYachtIndex)
				
				IF sMissionYachtVars[iYachtIndex].eMissionYachtSpawnState != FMMC_YACHT_SPAWN_STATE__COMPLETE
					bAllYachtsFullySpawned = FALSE
					PRINTLN("[MYACHT][UPDATE_YACHT_FOR_MISSION_CREATOR] - Mission Yacht ", iYachtIndex, " isn't ready!")
				ENDIF
			ENDIF
		ENDFOR
		
		IF bAllYachtsFullySpawned
			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_FMMC_YACHTS_READY)
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_FMMC_YACHTS_READY)
				PRINTLN("[MYACHT][UPDATE_YACHT_FOR_MISSION_CREATOR] - Setting PBBOOL4_FMMC_YACHTS_READY!")
			ENDIF
		ELSE
			IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_FMMC_YACHTS_READY)
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_FMMC_YACHTS_READY)
				PRINTLN("[MYACHT][UPDATE_YACHT_FOR_MISSION_CREATOR] - Clearing PBBOOL4_FMMC_YACHTS_READY!")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_BLOCKING_OF_CORONA_RED_FLASHING_LIGHTS)
		CREATE_MODEL_HIDE(<< 244, -1362.47, 25.18 >>, 50.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_med_cor_AlarmLight")), TRUE)
		PRINTLN("[MC_Interiors] - LBOOL25_PERFORMED_MODEL_SWAP_CORONA_LIGHT Hiding red flashing lights. lm 4 INIT")	
	ENDIF
	
	//Already done, get out
	IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_INITIAL_IPL_SET_UP_COMPLETE)
		IF NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_PROPS_HAVE_BEEN_HIDDEN)
			INITIALISE_WORLD_PROPS()
			PRINTLN("[KH] HIDING PROPS - Setting LBOOL17_PROPS_HAVE_BEEN_HIDDEN")
			SET_BIT(iLocalBoolCheck17, LBOOL17_PROPS_HAVE_BEEN_HIDDEN)
		ENDIF
		
		PRINTLN("[KH] Exiting SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER early due to LBOOL8_INITIAL_IPL_SET_UP_COMPLETE")
		EXIT
	ENDIF
	
	CHECK_FOR_IPL_CONTINUITY()
	
	IF NOT IS_PLAYER_ON_A_PLAYLIST(LocalPlayer)
		PRINTLN("[MC_Interiors] SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER reset bitset")
		IF g_TransitionSessionNonResetVars.iFMMC_IPL_Bitset != 0
	
			IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_CARRIER)
			AND IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMC_IPL_Bitset, ciFMMC_IPL_Carrier)
				CLEAR_BIT(g_TransitionSessionNonResetVars.iFMMC_IPL_Bitset, ciFMMC_IPL_Carrier)
			ENDIF
						
			IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_YACHT_NEAR_PIER)
			AND IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMC_IPL_Bitset, ciFMMC_IPL_YachtNearPier)
				CLEAR_BIT(g_TransitionSessionNonResetVars.iFMMC_IPL_Bitset, ciFMMC_IPL_YachtNearPier)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_YACHT_CHUMASH)
			AND IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMC_IPL_Bitset, ciFMMC_IPL_YachtChumash)
				CLEAR_BIT(g_TransitionSessionNonResetVars.iFMMC_IPL_Bitset, ciFMMC_IPL_YachtChumash)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_OPEN_GRAVE)
			AND IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMC_IPL_Bitset, ciFMMC_IPL_OpenGrave)
				CLEAR_BIT(g_TransitionSessionNonResetVars.iFMMC_IPL_Bitset, ciFMMC_IPL_OpenGrave)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_YACHT_PALETO)
			AND IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMC_IPL_Bitset, ciFMMC_IPL_YachtPaletoBay)
				CLEAR_BIT(g_TransitionSessionNonResetVars.iFMMC_IPL_Bitset, ciFMMC_IPL_YachtPaletoBay)
			ENDIF
		ENDIF
		
		RESET_MISSION_CONTROLLER_IPLS()
	ENDIF
	
	//Good to swap all as needed 
	PRINTLN("[MC_Interiors] SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER called")
	PRINTLN("[MC_Interiors] SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER - Setting up IPLs")
		

	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_CARRIER)						
		REQUEST_IPL("hei_carrier")
		
		REQUEST_IPL("hei_Carrier_int1")
		REQUEST_IPL("hei_Carrier_int2")
		REQUEST_IPL("hei_Carrier_int3")
		REQUEST_IPL("hei_Carrier_int4")
		REQUEST_IPL("hei_Carrier_int5")
		REQUEST_IPL("hei_Carrier_int6")
		//REMOVE_IPL("hei_carrier_DistantLights") This IPL seems to have been removed from game.
		REQUEST_IPL("hei_carrier_LODLights")
		
		SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT("AZL_HEI_AIRCRAFT_CARRIER",TRUE,TRUE)
		PRINTLN("[MC_Interiors] pre game - adding carrier ipl")
		SET_BIT(g_TransitionSessionNonResetVars.iFMMC_IPL_Bitset, ciFMMC_IPL_Carrier)
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_YACHT_NEAR_PIER)						
		REQUEST_IPL("smboat")
		PRINTLN("[MC_Interiors] pre game - adding yacht near pier ipl")
		SET_BIT(g_TransitionSessionNonResetVars.iFMMC_IPL_Bitset, ciFMMC_IPL_YachtNearPier)
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_YACHT_CHUMASH)
		REQUEST_IPL("hei_yacht_heist")
		REQUEST_IPL("hei_yacht_heist_enginrm")
		REQUEST_IPL("hei_yacht_heist_Lounge")
		REQUEST_IPL("hei_yacht_heist_Bridge")
		REQUEST_IPL("hei_yacht_heist_Bar")
		REQUEST_IPL("hei_yacht_heist_Bedrm")
		SET_BIT(g_TransitionSessionNonResetVars.iFMMC_IPL_Bitset, ciFMMC_IPL_YachtChumash)
		SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT("AZL_DLC_HEI_MILO_YACHT_ZONES",TRUE,TRUE)
		PRINTLN("[MC_Interiors] pre game - adding yacht near chumash ipl")
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_YACHT_PALETO)
		REQUEST_IPL("gr_Heist_Yacht2")
		REQUEST_IPL("gr_Heist_Yacht2_Bridge")
		REQUEST_IPL("gr_Heist_Yacht2_Bar")
		REQUEST_IPL("gr_Heist_Yacht2_Bedrm")
		REQUEST_IPL("gr_Heist_Yacht2_Lounge")
		REQUEST_IPL("gr_Heist_Yacht2_enginrm")
		SET_BIT(g_TransitionSessionNonResetVars.iFMMC_IPL_Bitset, ciFMMC_IPL_YachtPaletoBay)
		PRINTLN("[MC_Interiors] pre game - adding paleto bay ipl")
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF

		
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_OPEN_GRAVE)						
		REMOVE_IPL("lr_cs6_08_grave_closed")
		REQUEST_IPL("lr_cs6_08_grave_open")
		ADD_NAVMESH_BLOCKING_OBJECT(<<-282.887, 2834.759, 53.769>>, <<2.1,3.4,1.1>>, 149.920)
		PRINTLN("[MC_Interiors] pre game - adding open grave ipl")
		SET_BIT(g_TransitionSessionNonResetVars.iFMMC_IPL_Bitset, ciFMMC_IPL_OpenGrave)
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_FORT_ZANCUDO_GATES)
		REMOVE_IPL("cs3_07_mpgates")
		PRINTLN("[MC_Interiors] pre game - removing ford zancudo gates ipl")
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ELSE
		REQUEST_IPL("cs3_07_mpgates")
		PRINTLN("[MC_Interiors] pre game - adding ford zancudo gates ipl")
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_FRANKLINS_HOUSE)
		INTERIOR_INSTANCE_INDEX interiorFranklinID = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<7.0256, 537.3075, 175.0281>>, "v_franklinshouse")
		IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(interiorFranklinID, "franklin_settled")
			ACTIVATE_INTERIOR_ENTITY_SET(interiorFranklinID, "franklin_settled")
		ENDIF
		IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(interiorFranklinID, "bong_and_wine")
			ACTIVATE_INTERIOR_ENTITY_SET(interiorFranklinID, "bong_and_wine")
		ENDIF
		IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(interiorFranklinID, "unlocked")
			ACTIVATE_INTERIOR_ENTITY_SET(interiorFranklinID, "unlocked")
		ENDIF
		SET_DOOR_STATE(DOORNAME_F_HOUSE_VH_F, DOORSTATE_UNLOCKED)
		REFRESH_INTERIOR(interiorFranklinID)
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_MICHAELS_HOUSE)
		INTERIOR_INSTANCE_INDEX interiorMichaelID = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-811.2679, 179.3344, 75.7408>>, "v_Michael")
		SET_DOOR_STATE(DOORNAME_M_MANSION_F_L, DOORSTATE_UNLOCKED)
		SET_DOOR_STATE(DOORNAME_M_MANSION_F_R, DOORSTATE_UNLOCKED)
		SET_DOOR_STATE(DOORNAME_M_MANSION_G1, DOORSTATE_UNLOCKED)
		SET_DOOR_STATE(DOORNAME_M_MANSION_R_L1, DOORSTATE_UNLOCKED)
		SET_DOOR_STATE(DOORNAME_M_MANSION_R_R1, DOORSTATE_UNLOCKED)
		SET_DOOR_STATE(DOORNAME_M_MANSION_R_L2, DOORSTATE_UNLOCKED)
		SET_DOOR_STATE(DOORNAME_M_MANSION_R_R2, DOORSTATE_UNLOCKED)
		SET_DOOR_STATE(DOORNAME_M_MANSION_GA_SM, DOORSTATE_UNLOCKED)
		REFRESH_INTERIOR(interiorMichaelID)
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_TEQUI_LA_LA)
		INTERIOR_INSTANCE_INDEX interiorRockID = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -556.5089, 286.3181, 81.1763 >>, "v_rockclub")
		SET_INTERIOR_CAPPED(INTERIOR_V_ROCKCLUB, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_ROCKCLUB, FALSE)
		REFRESH_INTERIOR(interiorRockID)
		SET_DOOR_STATE(DOORNAME_TEQUILA_CLUB_DOOR_F, DOORSTATE_UNLOCKED)
		SET_DOOR_STATE(DOORNAME_TEQUILA_CLUB_DOOR_R, DOORSTATE_UNLOCKED)
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_STRIP_CLUB)
		INTERIOR_INSTANCE_INDEX interiorStripID = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<119.6, -1286.6, 29.3>>, "v_strip3")
		REFRESH_INTERIOR(interiorStripID)
		SET_DOOR_STATE(DOORNAME_STRIPCLUB_F, DOORSTATE_UNLOCKED)
		SET_DOOR_STATE(DOORNAME_STRIPCLUB_R, DOORSTATE_UNLOCKED)
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_CHICKEN_FACTORY)
		REMOVE_IPL("cs1_02_cf_offmission")
		REQUEST_IPL("CS1_02_cf_onmission1")
		REQUEST_IPL("CS1_02_cf_onmission2")
		REQUEST_IPL("CS1_02_cf_onmission3")
		REQUEST_IPL("CS1_02_cf_onmission4")
		
		PRINTLN("[MC_Interiors] pre game - adding chicken factory ipl")
		SET_BIT(g_TransitionSessionNonResetVars.iFMMC_IPL_Bitset, ciFMMC_IPL_ChickenFactory)
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_SILO)
		REQUEST_IPL("xm_siloentranceclosed_x17")
		REQUEST_IPL("xm_hatches_terrain")
		REQUEST_IPL("xm_hatch_closed")
		PRINTLN("[MC_Interiors] pre game - adding xm_siloentranceclosed_x17, xm_hatches_terrain, ")
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FARMHOUSE)	
		REMOVE_IPL("farmint_cap")
		REQUEST_IPL("farmint")
		SET_INTERIOR_DISABLED(INTERIOR_V_FARMHOUSE, FALSE) 
		SET_INTERIOR_CAPPED(INTERIOR_V_FARMHOUSE, FALSE)
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_FARMHOUSE, FALSE)
		PRINTLN("[MC_Interiors] pre game - adding farmhouse")
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_ABATTOIR)	
		SET_INTERIOR_DISABLED(INTERIOR_V_ABATTOIR, FALSE) 
		SET_INTERIOR_CAPPED(INTERIOR_V_ABATTOIR, FALSE)
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_ABATTOIR, FALSE)
		PRINTLN("[MC_Interiors] pre game - adding ABATTOIR ")
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_CORONER)	
		REMOVE_IPL("Coroner_Int_off")
		REQUEST_IPL("Coroner_Int_on")
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_CORONER_DARK)	
			ADD_TCMODIFIER_OVERRIDE("morgue_dark","morgue_dark_ovr")
			PRINTLN("[JS] Applying morgue_dark_ovr init")
		ENDIF
		SET_UP_INTERIOR(INTERIOR_V_CORONER)
		PRINTLN("[MC_Interiors] pre game - adding Coroner ")
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_RECYCLEPLANT)	
		SET_INTERIOR_DISABLED(INTERIOR_V_RECYCLE, FALSE)
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_RECYCLE, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_RECYCLE, FALSE)
		PRINTLN("[MC_Interiors] pre game - adding Recycleplant ")
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_UNION_DEPOSITORY)
		
		SET_BUILDING_STATE(BUILDINGNAME_ES_BANK_CAR_PARK_SHUTTERS, BUILDINGSTATE_DESTROYED) 
		SET_BUILDING_STATE(BUILDINGNAME_IPL_BIG_SCORE_BANK, BUILDINGSTATE_DESTROYED)
		
		// Union Depository Carpark
		SET_INTERIOR_CAPPED(INTERIOR_DT1_03_CARPARK, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_DT1_03_CARPARK, FALSE)
		
		INTERIOR_INSTANCE_INDEX iCarParkInteriorIndex		
		INTERIOR_DATA_STRUCT structCarparkInteriorData	
		structCarparkInteriorData = GET_INTERIOR_DATA(INTERIOR_DT1_03_CARPARK)			
		iCarParkInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structCarparkInteriorData.vPos, structCarparkInteriorData.sInteriorName)
		IF IS_VALID_INTERIOR(iCarParkInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iCarParkInteriorIndex)
		ENDIF
		
		// Union Depository Interior
		SET_INTERIOR_CAPPED(INTERIOR_V_UNION_DEPOSITORY, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_UNION_DEPOSITORY, FALSE)
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_UNION_DEPOSITORY, FALSE)
		
		INTERIOR_INSTANCE_INDEX iInteriorIndex
		INTERIOR_DATA_STRUCT structInteriorData
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_UNION_DEPOSITORY)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF IS_VALID_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_REMOVE_IAA_FACILITY_SATELITE_ENTRY_DOOR)
		REMOVE_IPL("xm_bunkerentrance_door")		
		PRINTLN("[MC_Interiors] pre game - removing IAA FACILITY DOOR")
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SUB)
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SUB)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		
		SET_INTERIOR_CAPPED(INTERIOR_V_SUB, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_SUB, FALSE)	
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_SUB, FALSE)

		IF IS_VALID_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_IAA)
		PRINTLN("[MC_Interiors] Setting up IAA")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_IAA)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex != NULL
			PRINTLN("[MC_Interiors] iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_IAA, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_IAA, FALSE)
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_IAA, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
			
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "IAA_Conf2_Chairs")
				ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "IAA_Conf2_Chairs")
				SET_INTERIOR_ENTITY_SET_TINT_INDEX(iInteriorIndex, "IAA_Conf2_Chairs", 0)
				PRINTLN("ACTIVATE_INTERIOR_ENTITY_SET - IAA_Conf2_Chairs")
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_IAA_COVER_POINTS)
				IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "IAA_CoverPoints_01")
					ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "IAA_CoverPoints_01")
					PRINTLN("ACTIVATE_INTERIOR_ENTITY_SET - IAA_CoverPoints_01")
				ENDIF
			ENDIF
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_LIFEINVADER)
		REMOVE_IPL("facelobbyfake")
		REQUEST_IPL("facelobby")
	
		PRINTLN("[MC_Interiors] Setting up Life Invader")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_LIFEINVADER)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] Life Invader iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_LIFEINVADER, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_LIFEINVADER, FALSE)
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_LIFEINVADER, FALSE)

		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
					
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FOUNDRY)
		PRINTLN("[MC_Interiors] Setting up Foundry")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_FOUNDRY)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] Foundry iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_FOUNDRY, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_FOUNDRY, FALSE)	
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_FOUNDRY, FALSE)

		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_01)
		PRINTLN("[MC_Interiors] Setting up Silo 1")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SILO_1)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] Silo 1 iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_SILO_1, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_SILO_1, FALSE)	
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_SILO_1, FALSE)

		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_02)
		PRINTLN("[MC_Interiors] Setting up Silo 2")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SILO_2)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] Silo 2 iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_SILO_2, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_SILO_2, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_SILO_2, FALSE)

		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_03)
		PRINTLN("[MC_Interiors] Setting up Silo 3 - Lab")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SILO_3)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] Silo 3 iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_SILO_3, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_SILO_3, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_SILO_3, FALSE)

		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
			
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SERVER_FARM)
		PRINTLN("[MC_Interiors] Setting up Server Farm")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SERVER_FARM)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] Server Farm iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_SERVER_FARM, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_SERVER_FARM, FALSE)
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_SERVER_FARM, FALSE)

		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_TUNNEL)
		PRINTLN("[MC_Interiors] Setting up Silo Tunnel")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SILO_TUNNEL)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] Silo Tunnel iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_SILO_TUNNEL, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_SILO_TUNNEL, FALSE)
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_SILO_TUNNEL, FALSE)

		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_LOOP)
		PRINTLN("[MC_Interiors] Setting up Silo Loop")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SILO_LOOP)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] Silo Loop iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_SILO_LOOP, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_SILO_LOOP, FALSE)
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_SILO_LOOP, FALSE)

		IF IS_VALID_INTERIOR(iInteriorIndex)
			IF LocalPlayerCurrentInterior != iInteriorIndex
				REFRESH_INTERIOR(iInteriorIndex)
			ENDIF
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_ENTRANCE)
		PRINTLN("[MC_Interiors] Setting up Silo Entrance")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SILO_ENTRANCE)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] Silo Entrance iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_SILO_ENTRANCE, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_SILO_ENTRANCE, FALSE)
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_SILO_ENTRANCE, FALSE)

		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_BASE)
		PRINTLN("[MC_Interiors] Setting up Silo Base")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SILO_BASE)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] Silo Base iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_SILO_BASE, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_SILO_BASE, FALSE)
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_SILO_BASE, FALSE)

		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_OSPREY)
		PRINTLN("[MC_Interiors] Setting up Osprey")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_OSPREY)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] Osprey iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_OSPREY, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_OSPREY, FALSE)
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OSPREY, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_ENTRANCE)
		SET_UP_INTERIOR(INTERIOR_V_TUNNEL_ENTRY)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_STRAIGHT)
		SET_UP_INTERIOR(INTERIOR_V_TUNNEL_STRAIGHT_0)
		SET_UP_INTERIOR(INTERIOR_V_TUNNEL_STRAIGHT_1)
		SET_UP_INTERIOR(INTERIOR_V_TUNNEL_STRAIGHT_2)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_FLAT_SLOPE)
		SET_UP_INTERIOR(INTERIOR_V_TUNNEL_SLOPE_FLAT_0)
		SET_UP_INTERIOR(INTERIOR_V_TUNNEL_SLOPE_FLAT_1)
		SET_UP_INTERIOR(INTERIOR_V_TUNNEL_SLOPE_FLAT_2)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_FLAT_SLOPE_2)
		SET_UP_INTERIOR(INTERIOR_V_TUNNEL_FLAT_SLOPE_0)
		SET_UP_INTERIOR(INTERIOR_V_TUNNEL_FLAT_SLOPE_1)
		SET_UP_INTERIOR(INTERIOR_V_TUNNEL_FLAT_SLOPE_2)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_30D_R)
		SET_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_RIGHT_0)
		SET_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_RIGHT_1)
		SET_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_RIGHT_2)
		SET_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_RIGHT_3)
		SET_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_RIGHT_4)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_30D_L)
		SET_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_LEFT_0)
		SET_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_LEFT_1)
		SET_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_LEFT_2)
		SET_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_LEFT_3)
		SET_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_LEFT_4)
		SET_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_LEFT_5)
	ENDIF
	
	//New interiors for Heists 2//
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_HANGAR)
		PRINTLN("[MC_Interiors] Setting up Hangar")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_HANGAR)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_HANGAR iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_UP_FMMC_HANGAR_INTERIOR_EXTRAS(iInteriorIndex)
		
		SET_INTERIOR_CAPPED(INTERIOR_V_HANGAR, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_HANGAR, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_HANGAR, FALSE)

		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_IMPORT_WAREHOUSE)
		PRINTLN("[MC_Interiors] Setting up INTERIOR_IMPORT_WAREHOUSE")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_IMPORT_WAREHOUSE)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_IMPORT_WAREHOUSE iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_UP_FMMC_IMPORT_WAREHOUSE_INTERIOR_EXTRAS(iInteriorIndex)
				
		SET_INTERIOR_CAPPED(INTERIOR_V_IMPORT_WAREHOUSE, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_IMPORT_WAREHOUSE, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_IMPORT_WAREHOUSE, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_WAREHOUSE_UNDRGRND_FACILITY)
		PRINTLN("[MC_Interiors] Setting up INTERIOR_WAREHOUSE_UNDRGRND_FACILITY")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_WAREHOUSE_UNDRGRND_FACILITY)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_WAREHOUSE_UNDRGRND_FACILITY iInteriorIndex = NULL Set up failed")
		ENDIF
					
		IF IS_VALID_INTERIOR(iInteriorIndex)
			INT iStyle = 0
			FOR iStyle = 0 TO ENUM_TO_INT(VEHICLE_GARAGE_SET_COUNT)-1
				IF INT_TO_ENUM(VEHICLE_GARAGE_ENTITY_SET_ID, iStyle) != VEHICLE_GARAGE_SET_CAR_ELEVATOR
				AND INT_TO_ENUM(VEHICLE_GARAGE_ENTITY_SET_ID, iStyle) != VEHICLE_GARAGE_SET_BASIC
					IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, GET_VEHICLE_GARAGE_ENTITY_SET(INT_TO_ENUM(VEHICLE_GARAGE_ENTITY_SET_ID, iStyle)))
						DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, GET_VEHICLE_GARAGE_ENTITY_SET(INT_TO_ENUM(VEHICLE_GARAGE_ENTITY_SET_ID, iStyle)))
						PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - ", GET_VEHICLE_GARAGE_ENTITY_SET(INT_TO_ENUM(VEHICLE_GARAGE_ENTITY_SET_ID, iStyle)))
					ENDIF
				ENDIF
			ENDFOR
		ENDIF			
		
		SET_INTERIOR_CAPPED(INTERIOR_V_WAREHOUSE_UNDRGRND_FACILITY, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_WAREHOUSE_UNDRGRND_FACILITY, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_WAREHOUSE_UNDRGRND_FACILITY, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SPECIAL_CARGO_WAREHOUSE_S)
		PRINTLN("[MC_Interiors] Setting up INTERIOR_SPECIAL_CARGO_WAREHOUSE_S")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_S)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_S iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_S, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_S, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_S, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SPECIAL_CARGO_WAREHOUSE_M)
		PRINTLN("[MC_Interiors] Setting up INTERIOR_SPECIAL_CARGO_WAREHOUSE_M")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_M)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_M iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_M, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_M, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_M, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SPECIAL_CARGO_WAREHOUSE_L)
		PRINTLN("[MC_Interiors] Setting up INTERIOR_SPECIAL_CARGO_WAREHOUSE_L")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_L)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_L iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_L, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_L, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_L, FALSE)

		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_STILT_APARTMENT)
		PRINTLN("[MC_Interiors] Setting up INTERIOR_STILT_APARTMENT")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_STILT_APARTMENT)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_STILT_APARTMENT iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_STILT_APARTMENT, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_STILT_APARTMENT, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_STILT_APARTMENT, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_HIGH_END_APARTMENT)
		PRINTLN("[MC_Interiors] Setting up INTERIOR_HIGH_END_APARTMENT")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_HIGH_END_APARTMENT)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_HIGH_END_APARTMENT iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_HIGH_END_APARTMENT, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_HIGH_END_APARTMENT, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_HIGH_END_APARTMENT, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_MEDIUM_END_APARTMENT)
		PRINTLN("[MC_Interiors] Setting up INTERIOR_MEDIUM_END_APARTMENT")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_MEDIUM_END_APARTMENT)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_MEDIUM_END_APARTMENT iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_MEDIUM_END_APARTMENT, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_MEDIUM_END_APARTMENT, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_MEDIUM_END_APARTMENT, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_LOW_END_APARTMENT)
		PRINTLN("[MC_Interiors] Setting up INTERIOR_LOW_END_APARTMENT")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_LOW_END_APARTMENT)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_LOW_END_APARTMENT iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_LOW_END_APARTMENT, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_LOW_END_APARTMENT, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_LOW_END_APARTMENT, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FIB_OFFICE)
		PRINTLN("[MC_Interiors] Setting up INTERIOR_FIB_OFFICE")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_FIB_OFFICE)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_FIB_OFFICE iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_FIB_OFFICE, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_FIB_OFFICE, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_FIB_OFFICE, FALSE)
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FIB_OFFICE_2)
		PRINTLN("[MC_Interiors] Setting up INTERIOR_FIB_OFFICE_2")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_FIB_OFFICE_2)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_FIB_OFFICE_2 iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_FIB_OFFICE_2, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_FIB_OFFICE_2, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_FIB_OFFICE_2, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR_FIB_LOBBY)
		
		IF NOT IS_IPL_ACTIVE("fiblobby")
			REQUEST_IPL("fiblobby")
			REMOVE_IPL("fiblobbyfake")
		ENDIF
	
		PRINTLN("[MC_Interiors] Setting up INTERIOR_FIB_LOBBY")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_FIB_LOBBY)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_FIB_LOBBY iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_FIB_LOBBY, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_FIB_LOBBY, FALSE)
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_FIB_LOBBY, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR_HIGH_END_GARAGE)
		PRINTLN("[MC_Interiors] Setting up INTERIOR_HIGH_END_GARAGE")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_HIGH_END_GARAGE)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_HIGH_END_GARAGE iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_HIGH_END_GARAGE, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_HIGH_END_GARAGE, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_HIGH_END_GARAGE, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR_MEDIUM_GARAGE)
		PRINTLN("[MC_Interiors] Setting up INTERIOR_MEDIUM_GARAGE")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_GARAGEM)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_GARAGEM iInteriorIndex = NULL Set up failed")
		ENDIF
			
		BLOCK_PROPERTY_BOUNDS_CHECK_LAUNCH(TRUE)
		
		SET_INTERIOR_CAPPED(INTERIOR_V_GARAGEM, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEM, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_GARAGEM, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			PRINTLN("[MC_Interiors] Setting up INTERIOR_MEDIUM_GARAGE")			
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF	
		
		ADD_INTERIOR_TO_CHECK(iInteriorsToCheck, iInteriorToCheck, iInteriorIndex) // LARGE INTERIORS NEED THIS!
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR_SMALL_GARAGE)
		PRINTLN("[MC_Interiors] Setting up INTERIOR_SMALL_GARAGE")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_GARAGES)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_GARAGES iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_GARAGES, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_GARAGES, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_GARAGES, FALSE)

		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	////
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_BUNKER)
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_BUNKER)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		
		REFRESH_INTERIOR(iInteriorIndex)
		SET_INTERIOR_CAPPED(INTERIOR_V_BUNKER, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_BUNKER, FALSE)
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_BUNKER, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		PRINTLN("[MC_Interiors] pre game - adding Bunker ")
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STANDARD_SET)
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Standard_Bunker_Set")
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "upgrade_bunker_set")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "upgrade_bunker_set")
				ENDIF
				ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Standard_Bunker_Set")
				PRINTLN("ACTIVATE_INTERIOR_ENTITY_SET - \"Standard_Bunker_Set\"")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_UPGRADE_SET)
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Upgrade_Bunker_Set")
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Standard_Bunker_Set")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Standard_Bunker_Set")
				ENDIF
				ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Upgrade_Bunker_Set")
				PRINTLN("ACTIVATE_INTERIOR_ENTITY_SET - \"Upgrade_Bunker_Set\"")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STYLE_A)
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Bunker_Style_A")
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "bunker_style_b")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "bunker_style_b")
				ENDIF
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "bunker_style_c")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "bunker_style_c")
				ENDIF
				ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Bunker_Style_A")
				PRINTLN("ACTIVATE_INTERIOR_ENTITY_SET - \"Bunker_Style_A\"")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STYLE_B)
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Bunker_Style_B")
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "bunker_style_a")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "bunker_style_a")
				ENDIF
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "bunker_style_c")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "bunker_style_c")
				ENDIF
				ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Bunker_Style_B")
				PRINTLN("ACTIVATE_INTERIOR_ENTITY_SET - \"Bunker_Style_B\"")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STYLE_C)
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Bunker_Style_C")
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "bunker_style_a")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "bunker_style_a")
				ENDIF
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "bunker_style_b")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "bunker_style_b")
				ENDIF
				ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Bunker_Style_C")
				PRINTLN("ACTIVATE_INTERIOR_ENTITY_SET - \"Bunker_Style_C\"")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_OFFICE_UPGRADE_SET)
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Office_Upgrade_Set")
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Office_blocker_set")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Office_blocker_set")
				ENDIF
				ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Office_Upgrade_Set")
				PRINTLN("ACTIVATE_INTERIOR_ENTITY_SET - \"Office_Upgrade_Set\"")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_OFFICE_BLOCKER_SET)
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Office_Blocker_Set")
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Office_Upgrade_Set")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Office_Upgrade_Set")
				ENDIF
				ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Office_Blocker_Set")
				PRINTLN("ACTIVATE_INTERIOR_ENTITY_SET - \"Office_Blocker_Set\"")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_GUN_RANGE_BLOCKER_SET)
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Gun_Range_Blocker_Set")
				ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Gun_Range_Blocker_Set")
				PRINTLN("ACTIVATE_INTERIOR_ENTITY_SET - \"Gun_Range_Blocker_Set\"")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_SECURITY_UPGRADE)
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Security_upgrade")
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "standard_security_set")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "standard_security_set")
				ENDIF
				ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Security_upgrade")
				PRINTLN("ACTIVATE_INTERIOR_ENTITY_SET - \"Security_upgrade\"")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STANDARD_SECURITY_SET)
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Standard_Security_Set")
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Security_upgrade")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Security_upgrade")
				ENDIF
				ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Standard_Security_Set")
				PRINTLN("ACTIVATE_INTERIOR_ENTITY_SET - \"Standard_Security_Set\"")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_GUN_LOCKER_UPGRADE)
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Gun_locker_upgrade")
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Gun_wall_blocker")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Gun_wall_blocker")
				ENDIF
				ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Gun_locker_upgrade")
				PRINTLN("ACTIVATE_INTERIOR_ENTITY_SET - \"Gun_locker_upgrade\"")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_GUN_WALL_BLOCKER)
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Gun_wall_blocker")
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Gun_locker_upgrade")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Gun_locker_upgrade")
				ENDIF
				ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Gun_wall_blocker")
				PRINTLN("ACTIVATE_INTERIOR_ENTITY_SET - \"Gun_wall_blocker\"")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciBUNKER_STANDARD_SET_MORE)
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Standard_Bunker_Set_More")
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Upgrade_Bunker_Set_More")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Upgrade_Bunker_Set_More")
				ENDIF
				ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Standard_Bunker_Set_More")
				PRINTLN("ACTIVATE_INTERIOR_ENTITY_SET - \"Standard_Bunker_Set_More\"")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciBUNKER_UPGRADE_SET_MORE)
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Upgrade_Bunker_Set_More")
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Standard_Bunker_Set_More")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Standard_Bunker_Set_More")
				ENDIF
				ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Upgrade_Bunker_Set_More")
				PRINTLN("ACTIVATE_INTERIOR_ENTITY_SET - \"Upgrade_Bunker_Set_More\"")
			ENDIF
		ENDIF
		
		//Audio Scene
		IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_GR_Bunker_Interior")
			START_AUDIO_SCENE("DLC_GR_Bunker_Interior")
			PRINTLN("[MC_Interiors] pre game - START_AUDIO_SCENE(\"DLC_GR_Bunker_Interior\"")
		ENDIF
	ENDIF
	
	BOOL bWaitForArenaTimer
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_ARENA)
		IF LOAD_UGC_ARENA(g_ArenaInterior, iArenaInterior_VIPLoungeIndex)
			IF DOES_ARENA_SET_UP_NEED_EXTRA_TIME()
				IF NOT HAS_NET_TIMER_STARTED(tdExtraArenaTimer)
					REINIT_NET_TIMER(tdExtraArenaTimer)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(tdExtraArenaTimer, ciExtraArenaTime)
						bWaitForArenaTimer = TRUE
						PRINTLN("[MC_Interiors][ARENA] SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER - Arena set up wait is over.")
					ENDIF
				ENDIF
			ELSE
				bWaitForArenaTimer = TRUE
				PRINTLN("[MC_Interiors][ARENA] SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER - Arena set up does not need to wait.")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_WEED_FARM)
		PRINTLN("[MC_Interiors][MISSIONFACTORY] Setting up INTERIOR2_WEED_FARM")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_WEED_FARM)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_WEED_FARM iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_WEED_FARM, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_WEED_FARM, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_WEED_FARM, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
			FMMC_SET_LONG_BIT(iInteriorInitializingExtrasBS, INTERIOR2_WEED_FARM)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_ALT_WEED_FARM)
		PRINTLN("[MC_Interiors][MISSIONFACTORY] Setting up INTERIOR3_ALT_WEED_FARM")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_ALT_WEED_FARM)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_ALT_WEED_FARM iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_ALT_WEED_FARM, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_ALT_WEED_FARM, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_ALT_WEED_FARM, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
			FMMC_SET_LONG_BIT(iInteriorInitializingExtrasBS, INTERIOR3_ALT_WEED_FARM)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_COCAINE_FACTORY)
		PRINTLN("[MC_Interiors][MISSIONFACTORY] Setting up INTERIOR2_COCAINE_FACTORY")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_COCAINE_FACTORY)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_COCAINE_FACTORY iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_COCAINE_FACTORY, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_COCAINE_FACTORY, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_COCAINE_FACTORY, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
			FMMC_SET_LONG_BIT(iInteriorInitializingExtrasBS, INTERIOR2_COCAINE_FACTORY)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_METH_FACTORY)
		PRINTLN("[MC_Interiors][MISSIONFACTORY] Setting up INTERIOR2_METH_FACTORY")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_METH_FACTORY)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_METH_FACTORY iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_METH_FACTORY, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_METH_FACTORY, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_METH_FACTORY, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
			FMMC_SET_LONG_BIT(iInteriorInitializingExtrasBS, INTERIOR2_METH_FACTORY)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO)
	AND (bCheckPreviousMission = FALSE OR NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCiInteriorBS2, INTERIOR2_CASINO))
		PRINTLN("[MC_Interiors] Setting up INTERIOR2_CASINO")
			
		SET_BIT(g_iBS1_Mission, ciBS1_Mission_PreventSimpleInteriorTerminateOnDeathAndIPLCleanup)
			
		IF NOT IS_BIT_SET(iInteriorInitializedBS, iInteriorsToCheck)
			REQUEST_IPL("vw_casino_main")
		ENDIF
		
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_CASINO iInteriorIndex = NULL Set up failed")
		ENDIF
		
		IF NOT IS_BIT_SET(iInteriorInitializedBS, iInteriorsToCheck)
			SET_INTERIOR_CAPPED(INTERIOR_V_CASINO, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_CASINO, FALSE)
			SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_CASINO, FALSE)
			
			IF IS_VALID_INTERIOR(iInteriorIndex)
				REFRESH_INTERIOR(iInteriorIndex)
				PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
				PRINTLN("[MC_Interiors] INTERIOR_V_CASINO iInteriorIndex PIN_INTERIOR_IN_MEMORY")
			ENDIF
			SET_BIT(iInteriorInitializedBS, iInteriorsToCheck)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
		
		ADD_INTERIOR_TO_CHECK(iInteriorsToCheck, iInteriorToCheck, iInteriorIndex) // LARGE INTERIORS NEED THIS!
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_CAR_PARK)
	AND (bCheckPreviousMission = FALSE OR NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCiInteriorBS2, INTERIOR2_CASINO_CAR_PARK))
		PRINTLN("[MC_Interiors] Setting up INTERIOR2_CASINO_CAR_PARK")
			
		SET_BIT(g_iBS1_Mission, ciBS1_Mission_PreventSimpleInteriorTerminateOnDeathAndIPLCleanup)
			
		IF NOT IS_BIT_SET(iInteriorInitializedBS, iInteriorsToCheck)
			REQUEST_IPL("vw_casino_carpark")
		ENDIF
		
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_CAR_PARK)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_CAR_PARK iInteriorIndex = NULL Set up failed")
		ENDIF
		
		IF NOT IS_BIT_SET(iInteriorInitializedBS, iInteriorsToCheck)
			SET_INTERIOR_CAPPED(INTERIOR_V_CASINO_CAR_PARK, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_CASINO_CAR_PARK, FALSE)
			SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_CASINO_CAR_PARK, FALSE)
			
			IF IS_VALID_INTERIOR(iInteriorIndex)
				REFRESH_INTERIOR(iInteriorIndex)
				PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
				PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_CAR_PARK iInteriorIndex PIN_INTERIOR_IN_MEMORY")
			ENDIF
			SET_BIT(iInteriorInitializedBS, iInteriorsToCheck)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
		
		ADD_INTERIOR_TO_CHECK(iInteriorsToCheck, iInteriorToCheck, iInteriorIndex) // LARGE INTERIORS NEED THIS!
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_APARTMENT)
	AND (bCheckPreviousMission = FALSE OR NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCiInteriorBS2, INTERIOR2_CASINO_APARTMENT))
		PRINTLN("[MC_Interiors] Setting up INTERIOR2_CASINO_APARTMENT")
			
		SET_BIT(g_iBS1_Mission, ciBS1_Mission_PreventSimpleInteriorTerminateOnDeathAndIPLCleanup)
			
		IF NOT IS_BIT_SET(iInteriorInitializedBS, iInteriorsToCheck)
			REQUEST_IPL("vw_casino_penthouse")
		ENDIF
		
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_APARTMENT)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_APARTMENT iInteriorIndex = NULL Set up failed")
		ENDIF
		
		interiorCasinoApartmentIndex = iInteriorIndex
		INIT_CASINO_APARTMENT_CREATOR_SETUP(interiorCasinoApartmentIndex)
		
		IF NOT IS_BIT_SET(iInteriorInitializedBS, iInteriorsToCheck)
			SET_INTERIOR_CAPPED(INTERIOR_V_CASINO_APARTMENT, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_CASINO_APARTMENT, FALSE)
			SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_CASINO_APARTMENT, FALSE)
			
			IF IS_VALID_INTERIOR(iInteriorIndex)
				REFRESH_INTERIOR(iInteriorIndex)
				PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
				PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_APARTMENT iInteriorIndex PIN_INTERIOR_IN_MEMORY")
			ENDIF
			SET_BIT(iInteriorInitializedBS, iInteriorsToCheck)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
		
		ADD_INTERIOR_TO_CHECK(iInteriorsToCheck, iInteriorToCheck, iInteriorIndex) // LARGE INTERIORS NEED THIS!
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_GARAGE)
	AND (bCheckPreviousMission = FALSE OR NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCiInteriorBS2, INTERIOR2_CASINO_GARAGE))
		PRINTLN("[MC_Interiors] Setting up INTERIOR2_CASINO_GARAGE")
			
		SET_BIT(g_iBS1_Mission, ciBS1_Mission_PreventSimpleInteriorTerminateOnDeathAndIPLCleanup)
			
		IF NOT IS_BIT_SET(iInteriorInitializedBS, iInteriorsToCheck)
			REQUEST_IPL("vw_casino_garage")
		ENDIF
		
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_GARAGE)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_GARAGE iInteriorIndex = NULL Set up failed")
		ENDIF
		
		IF NOT IS_BIT_SET(iInteriorInitializedBS, iInteriorsToCheck)
			SET_INTERIOR_CAPPED(INTERIOR_V_CASINO_GARAGE, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_CASINO_GARAGE, FALSE)
			SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_CASINO_GARAGE, FALSE)
			
			IF IS_VALID_INTERIOR(iInteriorIndex)
				REFRESH_INTERIOR(iInteriorIndex)
				PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
				PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_GARAGE iInteriorIndex PIN_INTERIOR_IN_MEMORY")
			ENDIF
			SET_BIT(iInteriorInitializedBS, iInteriorsToCheck)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
		
		ADD_INTERIOR_TO_CHECK(iInteriorsToCheck, iInteriorToCheck, iInteriorIndex) // LARGE INTERIORS NEED THIS!
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_MAIN)
	AND (bCheckPreviousMission = FALSE OR NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCiInteriorBS2, INTERIOR2_CASINO_MAIN))
		PRINTLN("[MC_Interiors] Setting up INTERIOR2_CASINO_MAIN")
			
		SET_BIT(g_iBS1_Mission, ciBS1_Mission_PreventSimpleInteriorTerminateOnDeathAndIPLCleanup)
		
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_MAIN)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_MAIN iInteriorIndex = NULL Set up failed")
		ENDIF
		
		IF NOT IS_BIT_SET(iInteriorInitializedBS, iInteriorsToCheck)
			SET_INTERIOR_CAPPED(INTERIOR_V_CASINO_MAIN, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_CASINO_MAIN, FALSE)
			SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_CASINO_MAIN, FALSE)
			
			IF IS_VALID_INTERIOR(iInteriorIndex)
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_CASINO_BAR_CHAIRS)
					TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "Set_Casino_Bar_Chairs")
				ELSE
					TURN_OFF_INTERIOR_ENTITY_SET(iInteriorIndex, "Set_Casino_Bar_Chairs")
				ENDIF
				
				REFRESH_INTERIOR(iInteriorIndex)
				PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
				PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_MAIN iInteriorIndex PIN_INTERIOR_IN_MEMORY")
			ENDIF
			SET_BIT(iInteriorInitializedBS, iInteriorsToCheck)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
		
		ADD_INTERIOR_TO_CHECK(iInteriorsToCheck, iInteriorToCheck, iInteriorIndex) // LARGE INTERIORS NEED THIS!
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_ARCADE)
	AND (bCheckPreviousMission = FALSE OR NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCiInteriorBS2, INTERIOR2_CASINO_ARCADE))
		PRINTLN("[MC_Interiors] Setting up INTERIOR2_CASINO_ARCADE")
			
		SET_BIT(g_iBS1_Mission, ciBS1_Mission_PreventSimpleInteriorTerminateOnDeathAndIPLCleanup)
		
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_ARCADE)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_ARCADE iInteriorIndex = NULL Set up failed")
		ENDIF
		
		IF NOT IS_BIT_SET(iInteriorInitializedBS, iInteriorsToCheck)
			SET_INTERIOR_CAPPED(INTERIOR_V_CASINO_ARCADE, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_CASINO_ARCADE, FALSE)
			SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_CASINO_ARCADE, FALSE)
			
			IF IS_VALID_INTERIOR(iInteriorIndex)
				REFRESH_INTERIOR(iInteriorIndex)
				PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
				PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_ARCADE iInteriorIndex PIN_INTERIOR_IN_MEMORY")
			ENDIF
			SET_BIT(iInteriorInitializedBS, iInteriorsToCheck)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
		
		ADD_INTERIOR_TO_CHECK(iInteriorsToCheck, iInteriorToCheck, iInteriorIndex) // LARGE INTERIORS NEED THIS!
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_HEIST_PLANNING)
	AND (bCheckPreviousMission = FALSE OR NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCiInteriorBS2, INTERIOR2_CASINO_HEIST_PLANNING))
		PRINTLN("[MC_Interiors] Setting up INTERIOR2_CASINO_HEIST_PLANNING")
			
		SET_BIT(g_iBS1_Mission, ciBS1_Mission_PreventSimpleInteriorTerminateOnDeathAndIPLCleanup)
		
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_HEIST_PLANNING)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_HEIST_PLANNING iInteriorIndex = NULL Set up failed")
		ENDIF
		
		IF NOT IS_BIT_SET(iInteriorInitializedBS, iInteriorsToCheck)
			SET_INTERIOR_CAPPED(INTERIOR_V_CASINO_HEIST_PLANNING, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_CASINO_HEIST_PLANNING, FALSE)
			SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_CASINO_HEIST_PLANNING, FALSE)
			
			IF IS_VALID_INTERIOR(iInteriorIndex)
				REFRESH_INTERIOR(iInteriorIndex)
				PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
				PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_HEIST_PLANNING iInteriorIndex PIN_INTERIOR_IN_MEMORY")
			ENDIF
			SET_BIT(iInteriorInitializedBS, iInteriorsToCheck)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
		
		ADD_INTERIOR_TO_CHECK(iInteriorsToCheck, iInteriorToCheck, iInteriorIndex) // LARGE INTERIORS NEED THIS!
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_TUNNEL)
	AND (bCheckPreviousMission = FALSE OR NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCiInteriorBS2, INTERIOR2_CASINO_TUNNEL))
		PRINTLN("[MC_Interiors] Setting up INTERIOR2_CASINO_TUNNEL")
			
		SET_BIT(g_iBS1_Mission, ciBS1_Mission_PreventSimpleInteriorTerminateOnDeathAndIPLCleanup)
		
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_TUNNEL)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_TUNNEL iInteriorIndex = NULL Set up failed")
		ENDIF
		
		IF NOT IS_BIT_SET(iInteriorInitializedBS, iInteriorsToCheck)
			SET_INTERIOR_CAPPED(INTERIOR_V_CASINO_TUNNEL, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_CASINO_TUNNEL, FALSE)
			SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_CASINO_TUNNEL, FALSE)
			
			IF IS_VALID_INTERIOR(iInteriorIndex)

				IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "set_tunnel_collapse")
					PRINTLN("[MC_Interiors] set_tunnel_collapse is not yet active, so ACTIVATING NOW")
					ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "set_tunnel_collapse")
					REFRESH_INTERIOR(iInteriorIndex)
				ENDIF

				PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
				PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_TUNNEL iInteriorIndex PIN_INTERIOR_IN_MEMORY")
			ENDIF
			SET_BIT(iInteriorInitializedBS, iInteriorsToCheck)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
		
		ADD_INTERIOR_TO_CHECK(iInteriorsToCheck, iInteriorToCheck, iInteriorIndex) // LARGE INTERIORS NEED THIS!
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_BACK_AREA)
	AND (bCheckPreviousMission = FALSE OR NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCiInteriorBS2, INTERIOR2_CASINO_BACK_AREA))
		PRINTLN("[MC_Interiors] Setting up INTERIOR2_CASINO_BACK_AREA")
			
		SET_BIT(g_iBS1_Mission, ciBS1_Mission_PreventSimpleInteriorTerminateOnDeathAndIPLCleanup)
		
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_BACK_AREA)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_BACK_AREA iInteriorIndex = NULL Set up failed")
		ENDIF
		
		IF NOT IS_BIT_SET(iInteriorInitializedBS, iInteriorsToCheck)
			SET_INTERIOR_CAPPED(INTERIOR_V_CASINO_BACK_AREA, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_CASINO_BACK_AREA, FALSE)
			SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_CASINO_BACK_AREA, FALSE)
			
			IF IS_VALID_INTERIOR(iInteriorIndex)
				IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_USE_LAUNDRY_DAMAGE)
					IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Casino_Back_Laundry_Damage")
						PRINTLN("[MC_Interiors][ML] Casino_Back_Laundry_Damage is not yet active, so ACTIVATING NOW")
						ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Casino_Back_Laundry_Damage")						
					ENDIF		
				ENDIF			
				REFRESH_INTERIOR(iInteriorIndex)
				PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
				PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_BACK_AREA iInteriorIndex PIN_INTERIOR_IN_MEMORY")
			ENDIF
			SET_BIT(iInteriorInitializedBS, iInteriorsToCheck)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
		
		ADD_INTERIOR_TO_CHECK(iInteriorsToCheck, iInteriorToCheck, iInteriorIndex) // LARGE INTERIORS NEED THIS!
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_HOTEL_FLOOR)
	AND (bCheckPreviousMission = FALSE OR NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCiInteriorBS2, INTERIOR2_CASINO_HOTEL_FLOOR))
		PRINTLN("[MC_Interiors] Setting up INTERIOR2_CASINO_HOTEL_FLOOR")
			
		SET_BIT(g_iBS1_Mission, ciBS1_Mission_PreventSimpleInteriorTerminateOnDeathAndIPLCleanup)
		
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_HOTEL_FLOOR)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_HOTEL_FLOOR iInteriorIndex = NULL Set up failed")
		ENDIF
		
		IF NOT IS_BIT_SET(iInteriorInitializedBS, iInteriorsToCheck)
			SET_INTERIOR_CAPPED(INTERIOR_V_CASINO_HOTEL_FLOOR, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_CASINO_HOTEL_FLOOR, FALSE)
			SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_CASINO_HOTEL_FLOOR, FALSE)
			
			IF IS_VALID_INTERIOR(iInteriorIndex)
				REFRESH_INTERIOR(iInteriorIndex)
				PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
				PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_HOTEL_FLOOR iInteriorIndex PIN_INTERIOR_IN_MEMORY")
			ENDIF
			SET_BIT(iInteriorInitializedBS, iInteriorsToCheck)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
		
		ADD_INTERIOR_TO_CHECK(iInteriorsToCheck, iInteriorToCheck, iInteriorIndex) // LARGE INTERIORS NEED THIS!
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_LOADING_BAY)
	AND (bCheckPreviousMission = FALSE OR NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCiInteriorBS2, INTERIOR2_CASINO_LOADING_BAY))
		PRINTLN("[MC_Interiors] Setting up INTERIOR2_CASINO_LOADING_BAY")
			
		SET_BIT(g_iBS1_Mission, ciBS1_Mission_PreventSimpleInteriorTerminateOnDeathAndIPLCleanup)
		
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_LOADING_BAY)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_LOADING_BAY iInteriorIndex = NULL Set up failed")
		ENDIF
		
		IF NOT IS_BIT_SET(iInteriorInitializedBS, iInteriorsToCheck)
			SET_INTERIOR_CAPPED(INTERIOR_V_CASINO_LOADING_BAY, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_CASINO_LOADING_BAY, FALSE)
			SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_CASINO_LOADING_BAY, FALSE)
			
			IF IS_VALID_INTERIOR(iInteriorIndex)
				REFRESH_INTERIOR(iInteriorIndex)
				PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
				PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_LOADING_BAY iInteriorIndex PIN_INTERIOR_IN_MEMORY")
			ENDIF
			SET_BIT(iInteriorInitializedBS, iInteriorsToCheck)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
		
		ADD_INTERIOR_TO_CHECK(iInteriorsToCheck, iInteriorToCheck, iInteriorIndex) // LARGE INTERIORS NEED THIS!
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_VAULT)
	AND (bCheckPreviousMission = FALSE OR NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCiInteriorBS2, INTERIOR2_CASINO_VAULT))
		PRINTLN("[MC_Interiors] Setting up INTERIOR2_CASINO_VAULT")
			
		SET_BIT(g_iBS1_Mission, ciBS1_Mission_PreventSimpleInteriorTerminateOnDeathAndIPLCleanup)
		
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_VAULT)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_VAULT iInteriorIndex = NULL Set up failed")
		ENDIF
		
		IF NOT IS_BIT_SET(iInteriorInitializedBS, iInteriorsToCheck)
			SET_INTERIOR_CAPPED(INTERIOR_V_CASINO_VAULT, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_CASINO_VAULT, FALSE)
			SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_CASINO_VAULT, FALSE)
			
			IF IS_VALID_INTERIOR(iInteriorIndex)				
				INIT_CASINO_VAULT_DOOR(iInteriorIndex)
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_CASINO_VAULT_DRESSING)
					TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "Set_Vault_Dressing")
				ELSE
					TURN_OFF_INTERIOR_ENTITY_SET(iInteriorIndex, "Set_Vault_Dressing")
				ENDIF
				
				REFRESH_INTERIOR(iInteriorIndex)
				PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
				PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_VAULT iInteriorIndex PIN_INTERIOR_IN_MEMORY")
			ENDIF
			SET_BIT(iInteriorInitializedBS, iInteriorsToCheck)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
		
		ADD_INTERIOR_TO_CHECK(iInteriorsToCheck, iInteriorToCheck, iInteriorIndex) // LARGE INTERIORS NEED THIS!
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_UTILITY_LIFT)
	AND (bCheckPreviousMission = FALSE OR NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCiInteriorBS2, INTERIOR2_CASINO_UTILITY_LIFT))
		PRINTLN("[MC_Interiors] Setting up INTERIOR2_CASINO_UTILITY_LIFT")
			
		SET_BIT(g_iBS1_Mission, ciBS1_Mission_PreventSimpleInteriorTerminateOnDeathAndIPLCleanup)
		
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_UTILITY_LIFT)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_UTILITY_LIFT iInteriorIndex = NULL Set up failed")
		ENDIF
		
		IF NOT IS_BIT_SET(iInteriorInitializedBS, iInteriorsToCheck)
			SET_INTERIOR_CAPPED(INTERIOR_V_CASINO_UTILITY_LIFT, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_CASINO_UTILITY_LIFT, FALSE)
			SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_CASINO_UTILITY_LIFT, FALSE)
			
			IF IS_VALID_INTERIOR(iInteriorIndex)
				REFRESH_INTERIOR(iInteriorIndex)
				PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
				PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_UTILITY_LIFT iInteriorIndex PIN_INTERIOR_IN_MEMORY")
			ENDIF
			SET_BIT(iInteriorInitializedBS, iInteriorsToCheck)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
		
		ADD_INTERIOR_TO_CHECK(iInteriorsToCheck, iInteriorToCheck, iInteriorIndex) // LARGE INTERIORS NEED THIS!
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_LIFT_SHAFT)
	AND (bCheckPreviousMission = FALSE OR NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCiInteriorBS2, INTERIOR2_CASINO_LIFT_SHAFT))
		PRINTLN("[MC_Interiors] Setting up INTERIOR2_CASINO_LIFT_SHAFT")
			
		SET_BIT(g_iBS1_Mission, ciBS1_Mission_PreventSimpleInteriorTerminateOnDeathAndIPLCleanup)
		
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_LIFT_SHAFT)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_LIFT_SHAFT iInteriorIndex = NULL Set up failed")
		ENDIF
		
		IF NOT IS_BIT_SET(iInteriorInitializedBS, iInteriorsToCheck)
			SET_INTERIOR_CAPPED(INTERIOR_V_CASINO_LIFT_SHAFT, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_CASINO_LIFT_SHAFT, FALSE)
			SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_CASINO_LIFT_SHAFT, FALSE)
			
			IF IS_VALID_INTERIOR(iInteriorIndex)
				REFRESH_INTERIOR(iInteriorIndex)
				PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
				PRINTLN("[MC_Interiors] INTERIOR_V_CASINO_LIFT_SHAFT iInteriorIndex PIN_INTERIOR_IN_MEMORY")
			ENDIF
			SET_BIT(iInteriorInitializedBS, iInteriorsToCheck)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
		
		ADD_INTERIOR_TO_CHECK(iInteriorsToCheck, iInteriorToCheck, iInteriorIndex) // LARGE INTERIORS NEED THIS!
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_METH_FACTORY_MURRIETA)
		PRINTLN("[MC_Interiors][MISSIONFACTORY] Setting up INTERIOR3_METH_FACTORY_MURRIETA")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_METH_FACTORY_MURRIETA)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_METH_FACTORY_MURRIETA iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_METH_FACTORY_MURRIETA, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_METH_FACTORY_MURRIETA, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_METH_FACTORY_MURRIETA, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			IF NOT FMMC_IS_LONG_BIT_SET(iInteriorInitializingExtrasBS, INTERIOR3_METH_FACTORY_MURRIETA)
				IF g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_MURRIETA] != -1
					TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "tintable_walls")
					SET_INTERIOR_ENTITY_SET_TINT_INDEX(iInteriorIndex, "tintable_walls", g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_MURRIETA])
				ELSE
					TURN_OFF_INTERIOR_ENTITY_SET(iInteriorIndex, "tintable_walls")
				ENDIF
			
				REFRESH_INTERIOR(iInteriorIndex)
				FMMC_SET_LONG_BIT(iInteriorInitializingExtrasBS, INTERIOR3_METH_FACTORY_MURRIETA)
			ENDIF
			
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)		
		
		ADD_INTERIOR_TO_CHECK(iInteriorsToCheck, iInteriorToCheck, iInteriorIndex) // LARGE INTERIORS NEED THIS!
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_METH_FACTORY_EAST_VINEWOOD)
		PRINTLN("[MC_Interiors][MISSIONFACTORY] Setting up INTERIOR3_METH_FACTORY_EAST_VINEWOOD")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_METH_FACTORY_EAST_VINEWOOD)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_METH_FACTORY_EAST_VINEWOOD iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_METH_FACTORY_EAST_VINEWOOD, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_METH_FACTORY_EAST_VINEWOOD, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_METH_FACTORY_EAST_VINEWOOD, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
		
			IF NOT FMMC_IS_LONG_BIT_SET(iInteriorInitializingExtrasBS, INTERIOR3_METH_FACTORY_EAST_VINEWOOD)
				IF g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_EAST_VINEWOOD] != -1
					TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "tintable_walls")
					SET_INTERIOR_ENTITY_SET_TINT_INDEX(iInteriorIndex, "tintable_walls", g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_EAST_VINEWOOD])
				ELSE
					TURN_OFF_INTERIOR_ENTITY_SET(iInteriorIndex, "tintable_walls")
				ENDIF
			
				REFRESH_INTERIOR(iInteriorIndex)
				FMMC_SET_LONG_BIT(iInteriorInitializingExtrasBS, INTERIOR3_METH_FACTORY_EAST_VINEWOOD)
			ENDIF
			
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
		
		ADD_INTERIOR_TO_CHECK(iInteriorsToCheck, iInteriorToCheck, iInteriorIndex) // LARGE INTERIORS NEED THIS!
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_METH_FACTORY_SENORA_DESERT_1)
		PRINTLN("[MC_Interiors][MISSIONFACTORY] Setting up INTERIOR3_METH_FACTORY_SENORA_DESERT_1")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_METH_FACTORY_SENORA_DESERT_1)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_METH_FACTORY_SENORA_DESERT_1 iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_METH_FACTORY_SENORA_DESERT_1, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_METH_FACTORY_SENORA_DESERT_1, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_METH_FACTORY_SENORA_DESERT_1, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
		
			IF NOT FMMC_IS_LONG_BIT_SET(iInteriorInitializingExtrasBS, INTERIOR3_METH_FACTORY_SENORA_DESERT_1)
				IF g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_SENORA_DESERT_1] != -1
					TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "tintable_walls")
					SET_INTERIOR_ENTITY_SET_TINT_INDEX(iInteriorIndex, "tintable_walls", g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_SENORA_DESERT_1])
				ELSE
					TURN_OFF_INTERIOR_ENTITY_SET(iInteriorIndex, "tintable_walls")
				ENDIF
			
				REFRESH_INTERIOR(iInteriorIndex)
				FMMC_SET_LONG_BIT(iInteriorInitializingExtrasBS, INTERIOR3_METH_FACTORY_SENORA_DESERT_1)
			ENDIF
			
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
		
		ADD_INTERIOR_TO_CHECK(iInteriorsToCheck, iInteriorToCheck, iInteriorIndex) // LARGE INTERIORS NEED THIS!
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_METH_FACTORY_SENORA_DESERT_2)
		PRINTLN("[MC_Interiors][MISSIONFACTORY] Setting up INTERIOR3_METH_FACTORY_SENORA_DESERT_2")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_METH_FACTORY_SENORA_DESERT_2)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_METH_FACTORY_SENORA_DESERT_2 iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_METH_FACTORY_SENORA_DESERT_2, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_METH_FACTORY_SENORA_DESERT_2, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_METH_FACTORY_SENORA_DESERT_2, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			
			IF NOT FMMC_IS_LONG_BIT_SET(iInteriorInitializingExtrasBS, INTERIOR3_METH_FACTORY_SENORA_DESERT_2)
				IF g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_SENORA_DESERT_2] != -1
					TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "tintable_walls")
					SET_INTERIOR_ENTITY_SET_TINT_INDEX(iInteriorIndex, "tintable_walls", g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_SENORA_DESERT_2])
				ELSE
					TURN_OFF_INTERIOR_ENTITY_SET(iInteriorIndex, "tintable_walls")
				ENDIF
			
				REFRESH_INTERIOR(iInteriorIndex)
				FMMC_SET_LONG_BIT(iInteriorInitializingExtrasBS, INTERIOR3_METH_FACTORY_SENORA_DESERT_2)
			ENDIF
			
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
		
		ADD_INTERIOR_TO_CHECK(iInteriorsToCheck, iInteriorToCheck, iInteriorIndex) // LARGE INTERIORS NEED THIS!
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_TUNER_CAR_MEET)
		PRINTLN("[MC_Interiors] Setting up INTERIOR3_TUNER_CAR_MEET")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_TUNER_CAR_MEET)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_TUNER_CAR_MEET iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_TUNER_CAR_MEET, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_TUNER_CAR_MEET, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_TUNER_CAR_MEET, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_TUNER_MOD_GARAGE)
		PRINTLN("[MC_Interiors] Setting up INTERIOR3_TUNER_MOD_GARAGE")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_TUNER_MOD_GARAGE)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_TUNER_MOD_GARAGE iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_TUNER_MOD_GARAGE, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_TUNER_MOD_GARAGE, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_TUNER_MOD_GARAGE, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
			
			TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "entity_set_bedroom")
			TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "entity_set_bedroom_empty")
			TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "entity_set_box_clutter")
			TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "entity_set_car_lift_cutscene")
			TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "entity_set_car_lift_default")
			TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "entity_set_car_lift_purchase")
			TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "entity_set_chalkboard")
			TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "entity_set_cut_seats")
			TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "entity_set_style_1")
			TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "entity_set_table")
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_CASINO_AIR_CONDITIONING_UNIT)
		IF NOT IS_IPL_ACTIVE("ch_dlc_casino_aircon_broken")
			REQUEST_IPL("ch_dlc_casino_aircon_broken")	
		ENDIF
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)			
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_REMOVE_CASINO_EXTERIOR_CAMERAS)
		IF IS_IPL_ACTIVE("ch_h3_casino_cameras")
			REMOVE_IPL("ch_h3_casino_cameras")
			PRINTLN("[MC_Interiors] pre game - Removing ch_h3_casino_cameras")
			BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_BUNKER_DOORS)
		REQUEST_OR_REMOVE_BUNKER_DOOR_IPLS(TRUE)
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)			
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_DEVIN_HANGAR_DOOR)
	
		PRINTLN("[MC_Interiors] Setting up INTERIOR_V_DEVIN_HANGAR")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_DEVIN_HANGAR)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_DEVIN_HANGAR iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_DEVIN_HANGAR, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_DEVIN_HANGAR, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_DEVIN_HANGAR, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		IF NOT IS_IPL_ACTIVE("sf_dlc_fixer_hanger_door")
			REQUEST_IPL("sf_dlc_fixer_hanger_door")
			CREATE_MODEL_HIDE(<<-1011.643, -2983.132, 25.444>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("ap1_01_b_shadowonly")), TRUE)
			PRINTLN("SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER - Requesting sf_dlc_fixer_hanger_door")
		ENDIF
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_LOST_MC_CLUB_HOUSE)
		IF NOT IS_IPL_ACTIVE("bkr_bi_hw1_13_int")
			REQUEST_IPL("bkr_bi_hw1_13_int")
			REMOVE_IPL("hei_bi_hw1_13_door")
		ENDIF
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)			
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_ISLAND)
		IF FMMC_LOAD_ISLAND_IPLS()
			BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
		ELSE
			PRINTLN("[MC_Interiors] SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER - Waiting on island")
			EXIT
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_CUSTOM_MISSION_MOC)
		IF NOT LOAD_CUSTOM_MISSION_MOC(iInteriorInitializedBS, iInteriorsToCheck, sMissionMOCProps, TRUE)
			PRINTLN("[MC_Interiors] SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER - Waiting on Custom MOC")
			EXIT
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_NIGHTCLUB_BASEMENT)
		PRINTLN("[MC_Interiors] Setting up INTERIOR3_NIGHTCLUB_BASEMENT")
			
		SET_BIT(g_iBS1_Mission, ciBS1_Mission_PreventSimpleInteriorTerminateOnDeathAndIPLCleanup)
		
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_NIGHTCLUB_BASEMENT)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR3_NIGHTCLUB_BASEMENT iInteriorIndex = NULL Set up failed")
		ENDIF
		
		IF NOT IS_BIT_SET(iInteriorInitializedBS, iInteriorsToCheck)
			SET_INTERIOR_CAPPED(INTERIOR_V_NIGHTCLUB_BASEMENT, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_NIGHTCLUB_BASEMENT, FALSE)
			SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_NIGHTCLUB_BASEMENT, FALSE)
			
			IF IS_VALID_INTERIOR(iInteriorIndex)
				REFRESH_INTERIOR(iInteriorIndex)
				PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
				PRINTLN("[MC_Interiors] INTERIOR_V_NIGHTCLUB_BASEMENT iInteriorIndex PIN_INTERIOR_IN_MEMORY")
			ENDIF
			SET_BIT(iInteriorInitializedBS, iInteriorsToCheck)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
		
		ADD_INTERIOR_TO_CHECK(iInteriorsToCheck, iInteriorToCheck, iInteriorIndex) // LARGE INTERIORS NEED THIS!
	ENDIF
	
	IF SHOULD_CONTROLLER_WAIT_FOR_NIGHTCLUB_TO_LOAD()
		IF NOT FMMC_LOAD_NIGHTCLUB_INTERIOR_AND_IPLS()
			PRINTLN("[MC_Interiors] SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER - Waiting on Nightclub")
			EXIT
		ENDIF
	ENDIF
		
	IF NOT INITIALIZE_SPECIAL_ASSETS_FOR_EXTERNAL_CUTSCENE_SEQUENCE()	
		PRINTLN("[MC_Interiors] SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER - Waiting on INITIALIZE_SPECIAL_ASSETS_FOR_EXTERNAL_CUTSCENE_SEQUENCE .... ")
		EXIT
	ENDIF	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_RECORDING_STUDIO)
		PRINTLN("[MC_Interiors] Setting up INTERIOR3_RECORDING_STUDIO")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_RECORDING_STUDIO)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_RECORDING_STUDIO iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_RECORDING_STUDIO, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_RECORDING_STUDIO, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_RECORDING_STUDIO, FALSE)
		
		DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Entity_Set_Default")
		ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Entity_Set_Fire")
		DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "entity_set_fix_stu_ext_p3a1")
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_RECORDING_STUDIO_ST_SET)
			ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "entity_set_fix_trip1_int_p2")
		ELSE
			ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "entity_set_fix_stu_ext_p1")
		ENDIF
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MUSIC_LOCKER)
		PRINTLN("[MC_Interiors] Setting up INTERIOR3_MUSIC_LOCKER")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_MUSIC_LOCKER)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_MUSIC_LOCKER iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_MUSIC_LOCKER, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_MUSIC_LOCKER, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_MUSIC_LOCKER, FALSE)
		
		INIT_MUSIC_LOCKER_CREATOR_SETUP(iInteriorIndex)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
			iInteriorIndexMusicLocker = iInteriorIndex
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_OFFICE_AUTOSHOP)
		PRINTLN("[MC_Interiors] Setting up INTERIOR3_OFFICE_AUTOSHOP")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_OFFICE_AUTOSHOP)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_OFFICE_AUTOSHOP iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_OFFICE_AUTOSHOP, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_AUTOSHOP, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_AUTOSHOP, FALSE)
				
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_HIGH_END_GARAGE_NEW)
		PRINTLN("[MC_Interiors] Setting up INTERIOR3_HIGH_END_GARAGE_NEW")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_GARAGEL)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_GARAGEL iInteriorIndex = NULL Set up failed")
		ENDIF
				
		BLOCK_PROPERTY_BOUNDS_CHECK_LAUNCH(TRUE)		
		
		SET_INTERIOR_CAPPED(INTERIOR_V_GARAGEL, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEL, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_GARAGEL, FALSE)
				
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MOTEL)
		PRINTLN("[MC_Interiors] Setting up INTERIOR3_MOTEL")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_MOTEL)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_MOTEL iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_MOTEL, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_MOTEL, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_MOTEL, FALSE)
				
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_PROPERTY_BOUNDS_CHECK_LAUNCH(TRUE)
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_ARENA_WAR_GARAGE_LEVEL_1)	
	
		IF NOT IS_IPL_ACTIVE("xs_arena_interior_mod_2")
			REQUEST_IPL("xs_arena_interior_mod_2")
			PRINTLN("SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER - Requesting xs_arena_interior_mod_2")
		ENDIF
	
		PRINTLN("[MC_Interiors] Setting up INTERIOR3_ARENA_WAR_GARAGE_LEVEL_1")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_ARENA_WAR_GARAGE_LEVEL_1)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_ARENA_WAR_GARAGE_LEVEL_1 iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_ARENA_WAR_GARAGE_LEVEL_1, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_ARENA_WAR_GARAGE_LEVEL_1, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_ARENA_WAR_GARAGE_LEVEL_1, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "set_int_mod2_b_tint")
			SET_INTERIOR_ENTITY_SET_TINT_INDEX(iInteriorIndex, "set_int_mod2_b_tint", 8)

			TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "set_int_mod2_b1")
			SET_INTERIOR_ENTITY_SET_TINT_INDEX(iInteriorIndex, "set_int_mod2_b1", 8)

			TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "set_mod2_style_01")
			
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
	
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_CUSTOM_HIGH_END_APARTMENT)	
		
		BLOCK_PROPERTY_BOUNDS_CHECK_LAUNCH(TRUE)
		
		IF NOT IS_IPL_ACTIVE("apa_v_mp_h_04_b")
			REQUEST_IPL("apa_v_mp_h_04_b")
			PRINTLN("SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER - Requesting apa_v_mp_h_04_b")
		ENDIF
	
		PRINTLN("[RCC MISSION] Setting up INTERIOR3_CUSTOM_HIGH_END_APARTMENT")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CUSTOM_B_4)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[RCC MISSION] INTERIOR_V_CUSTOM_B_4 iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_CUSTOM_B_4, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_CUSTOM_B_4, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_CUSTOM_B_4, FALSE)
	
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_MISSION_BASEMENT_INTERIOR_ENABLED()
		
		PRINTLN("[MC_Interiors] Setting up INTERIOR_V_MISSION_BASEMENT")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_MISSION_BASEMENT)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_MISSION_BASEMENT iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_MISSION_BASEMENT, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_MISSION_BASEMENT, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_MISSION_BASEMENT, FALSE)
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			
			TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, GET_INTERIOR_SET_FOR_MISSION_BASEMENT(g_FMMC_STRUCT.iMissionBasement))
			
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
	
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_PHARMA_OFFICE)
		PRINTLN("[MC_Interiors] Setting up INTERIOR3_PHARMA_OFFICE")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_PHARMA_OFFICE)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_PHARMA_OFFICE iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_PHARMA_OFFICE, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_PHARMA_OFFICE, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_PHARMA_OFFICE, FALSE)
				
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_AUTOCARE_WAREHOUSE)
		PRINTLN("[MC_Interiors] Setting up INTERIOR3_AUTOCARE_WAREHOUSE")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_AUTOCARE_WAREHOUSE)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[MC_Interiors] INTERIOR_V_AUTOCARE_WAREHOUSE iInteriorIndex = NULL Set up failed")
		ENDIF
		
		SET_INTERIOR_CAPPED(INTERIOR_V_AUTOCARE_WAREHOUSE, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_AUTOCARE_WAREHOUSE, FALSE)		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_AUTOCARE_WAREHOUSE, FALSE)
				
		IF IS_VALID_INTERIOR(iInteriorIndex)
			REFRESH_INTERIOR(iInteriorIndex)
			PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
		ENDIF
		
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	ENDIF
		
	// special case
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_ARENA)
		IF (IS_VALID_INTERIOR(g_ArenaInterior) AND IS_INTERIOR_READY(g_ArenaInterior) AND bWaitForArenaTimer)
			//Set we're done to not come in here again
			g_bMissionIPLsRequested = TRUE
			SET_BIT(iLocalBoolCheck8, LBOOL8_INITIAL_IPL_SET_UP_COMPLETE)
		ENDIF
	ELSE
		// Regular way
		IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_INITIAL_IPL_SET_UP_COMPLETE)
			IF ARE_THERE_ANY_ADDED_INTERIORS_TO_CHECK(iInteriorsToCheck)
				IF ARE_ADDED_INTERIORS_READY(iInteriorToCheck, iInteriorsToCheck)
					IF NOT HAS_NET_TIMER_STARTED(tdInitialIPLSetupTimer)
						PRINTLN("[MC_Interiors][LoadInteriorsAndCheck] - SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER - Start Timer")
						START_NET_TIMER(tdInitialIPLSetupTimer)
					ENDIF
					IF HAS_NET_TIMER_STARTED(tdInitialIPLSetupTimer)
					AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdInitialIPLSetupTimer, 3000)
						PRINTLN("[MC_Interiors][LoadInteriorsAndCheck] - SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER - Interior returning that it is ready.")
						g_bMissionIPLsRequested = TRUE
						SET_BIT(iLocalBoolCheck8, LBOOL8_INITIAL_IPL_SET_UP_COMPLETE)
						RESET_NET_TIMER(tdInitialIPLSetupTimer)
					ELSE
						PRINTLN("[MC_Interiors][LoadInteriorsAndCheck] - SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER - Waiting for Delay/Timer to expire.")
					ENDIF
				ELSE
					PRINTLN("[MC_Interiors][LoadInteriorsAndCheck] - SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER - Waiting for interior to be ready.")
				ENDIF
			ELSE
				PRINTLN("[MC_Interiors][LoadInteriorsAndCheck] - SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER - Don't need to check if interior is ready.")
				g_bMissionIPLsRequested = TRUE
				SET_BIT(iLocalBoolCheck8, LBOOL8_INITIAL_IPL_SET_UP_COMPLETE)				
			ENDIF
		ENDIF
	ENDIF
	
	PROCESS_FIXER_AGENCY_SIGN_IPLS()
	
	IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_INITIAL_IPL_SET_UP_COMPLETE)
		PRINTLN("[MC_Interiors][ARENA] SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER complete")
		IF g_TransitionSessionNonResetVars.iFMMCiInteriorBS2 != 0
			g_TransitionSessionNonResetVars.iFMMCiInteriorBS2 = 0
			PRINTLN("[MC_Interiors] SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER - Reset g_TransitionSessionNonResetVars.iFMMCiInteriorBS2")
		ENDIF
		IF g_TransitionSessionNonResetVars.iIPLOptions != 0
			g_TransitionSessionNonResetVars.iIPLOptions = 0
			PRINTLN("[MC_Interiors] SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER - Reset g_TransitionSessionNonResetVars.iIPLOptions")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CUSTOM_AUDIO_MIXING_SCENES()

	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule > -1
	AND iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_FIXER_PARTY_PROMOTER_PENTHOUSE_AUDIO_SCENE)
			IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_Fixer_Casino_Penthouse_Shootout_Scene")
				PRINTLN("PROCESS_CUSTOM_AUDIO_MIXING_SCENES - Starting DLC_Fixer_Casino_Penthouse_Shootout_Scene")
				START_AUDIO_SCENE("DLC_Fixer_Casino_Penthouse_Shootout_Scene")
			ENDIF
		ELSE
			IF IS_AUDIO_SCENE_ACTIVE("DLC_Fixer_Casino_Penthouse_Shootout_Scene")
				PRINTLN("PROCESS_CUSTOM_AUDIO_MIXING_SCENES - Stopping DLC_Fixer_Casino_Penthouse_Shootout_Scene")
				STOP_AUDIO_SCENE("DLC_Fixer_Casino_Penthouse_Shootout_Scene")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_CUSTOM_AUDIO_DUCKING_SCENE)
		AND (IS_SCRIPTED_CONVERSATION_ONGOING() OR NOT IS_CONVERSATION_STATUS_FREE())
			IF NOT IS_AUDIO_SCENE_ACTIVE("GTAO_POSITIONED_RADIO_DUCKING_SCENE")
				PRINTLN("PROCESS_CUSTOM_AUDIO_MIXING_SCENES - Starting GTAO_POSITIONED_RADIO_DUCKING_SCENE")
				START_AUDIO_SCENE("GTAO_POSITIONED_RADIO_DUCKING_SCENE")
			ENDIF
		ELSE
			IF IS_AUDIO_SCENE_ACTIVE("GTAO_POSITIONED_RADIO_DUCKING_SCENE")
				PRINTLN("PROCESS_CUSTOM_AUDIO_MIXING_SCENES - Stopping GTAO_POSITIONED_RADIO_DUCKING_SCENE")
				STOP_AUDIO_SCENE("GTAO_POSITIONED_RADIO_DUCKING_SCENE")
			ENDIF
		ENDIF
	ENDIF

ENDPROC
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: World Markers
// ##### Description: Small functions that don't fit into a section
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_WORLD_MARKER(FMMC_MARKER_STRUCT &sPlacedMarker, INT iIndex)
			
	VECTOR vPos = sPlacedMarker.vPosition
	
	IF NOT IS_VECTOR_ZERO(vPlacedWorldMarkerForInteractable[iIndex])
		vPos = vPlacedWorldMarkerForInteractable[iIndex]
	ENDIF
	
	IF IS_VECTOR_ZERO(vPos)
		EXIT
	ENDIF
	
	PROCESS_PLACED_MARKER(sPlacedMarker, vPos, FALSE)	
	
ENDPROC

PROC PROCESS_WORLD_MARKERS()
		
	INT iIndex = 0
	FOR iIndex = 0 TO FMMC_MAX_PLACED_MARKERS-1
		PROCESS_WORLD_MARKER(g_FMMC_STRUCT_ENTITIES.sPlacedMarker[iIndex], iIndex)
	ENDFOR
	
ENDPROC
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: World Init
// ##### Description: Small functions that don't fit into a section
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC CREATE_CASINO_SCENARIO_BLOCKING_AREA()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_AllowCasinoValetScenario)
		EXIT
	ENDIF
	IF sbiCasinoValet = NULL
		sbiCasinoValet = ADD_SCENARIO_BLOCKING_AREA_FROM_POSITION_AND_RADIUS(vValetPos, 1.0)
		PRINTLN("CREATE_CASINO_SCENARIO_BLOCKING_AREA - Created scenario blocking area for casino valet")
	ENDIF
ENDPROC

PROC INIT_WORLD()

	//Setting the time of day if it is active on the starting rule
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetSeven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE7_ENABLE_IS_START_TIME)						
			SET_START_TIME_BASED_ON_RULE()
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_ISLAND)
		g_bMissionLoadingIsland = TRUE
		PRINTLN("INIT_WORLD - Setting g_bMissionLoadingIsland")
	ENDIF
	
	CREATE_CASINO_SCENARIO_BLOCKING_AREA()
ENDPROC

PROC PROCESS_CLIENT_WORLD_EVERY_FRAME()

	PROCESS_INSTANCED_CONTENT_PORTALS(IS_OBJECTIVE_BLOCKED())
	
	PROCESS_PRE_GAME_EMP_AFTER_INTRO()
	
	SET_TIME_BASED_ON_RULE()
	
	SET_TIME_PROGRESSION_BASED_ON_RULE_TIME()
	
	SET_WEATHER_BASED_ON_RULE()
	
	PROCESS_NAVMESH_UNLOADING_WHEN_UNSAFE()
	
	PROCESS_WORLD_MARKERS()
	
	PROCESS_ARTIFICIAL_LIGHTS_CLIENT()
	
	PROCESS_CUSTOM_AUDIO_MIXING_SCENES()
	
	PROCESS_CONSTRUCTION_SITE_HIGH_RISE_ROOFTOP_SHADOW_TIMECYCLE_MOD()
	
ENDPROC

PROC CLEAR_SERVER_PED_CLONING_DATA_WHEN_NO_LONGER_NEEDED()
	IF MC_serverBD_4.sPedCloningData.iNumOfEntsToClone != 0
		
		PRINTLN("[PedClone] CLEAR_SERVER_PED_CLONING_DATA_WHEN_NO_LONGER_NEEDED - sPedCloningData data in use. Size = ", MC_serverBD_4.sPedCloningData.iNumOfEntsToClone, ". Checking if it's ready to be cleared.")
		
		INT i
		REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() i
			IF IS_BIT_SET(MC_playerBD[i].iClientBitSet5, PBBOOL5_APPLY_SERVER_CACHED_CLONE_HEAD_BLEND)
				EXIT //We are still using the data
			ENDIF
		ENDREPEAT
		
		
		//Clear ped cloning data when it's no longer needed
		i = 0
		REPEAT MC_serverBD_4.sPedCloningData.iNumOfEntsToClone i
			MC_serverBD_4.sPedCloningData.playerSource[i] = INVALID_PLAYER_INDEX()
			MC_serverBD_4.sPedCloningData.niTarget[i] = NULL
		ENDREPEAT
	
		MC_serverBD_4.sPedCloningData.iNumOfEntsToClone = 0
		
		PRINTLN("[PedClone] CLEAR_SERVER_PED_CLONING_DATA_WHEN_NO_LONGER_NEEDED - Clearing sPedCloningData data struct.")
	ENDIF
ENDPROC

PROC PROCESS_SERVER_WORLD_EVERY_FRAME()
	PROCESS_ARTIFICIAL_LIGHTS_SERVER(g_FMMC_STRUCT.iArtificialLightControlTeam, GET_TEAM_CURRENT_RULE(g_FMMC_STRUCT.iArtificialLightControlTeam))
	
	PROCESS_SERVER_TIME_OF_DAY()
	
	CLEAR_SERVER_PED_CLONING_DATA_WHEN_NO_LONGER_NEEDED()
ENDPROC

