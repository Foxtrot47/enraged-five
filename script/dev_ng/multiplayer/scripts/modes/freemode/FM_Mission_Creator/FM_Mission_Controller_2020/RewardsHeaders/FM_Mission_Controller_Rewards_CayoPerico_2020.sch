// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Rewards - Cayo Perico -----------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script Name: FM_Mission_Controller_2020.sc ------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Money, RP, Rewards, Stats & Telemetry for the Cayo Perico Heist.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_EventBroadcasting_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Island Heist Anti Cheat
// ##### Description: Functions for maintaining, incrementing and decrementing island heist anti-cheat chances.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC INCREMENT_CURRENT_ISLAND_HEIST_CHEATER_BITSET()

	IF NOT IS_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS_PASSED(PSSPS_ISLAND_HEIST_FINALE_CHANCE_1, DEFAULT)
		SET_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS(PSSPS_ISLAND_HEIST_FINALE_CHANCE_1, DEFAULT)
		g_bIslandHeistCheaterIncremented = TRUE
		PRINTLN("[RCC MISSION][ISLAND_HEIST_ANTICHEAT] PSSPS_ISLAND_HEIST_FINALE_CHANCE_1 SET")
	ELIF NOT IS_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS_PASSED(PSSPS_ISLAND_HEIST_FINALE_CHANCE_2, DEFAULT)
		SET_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS(PSSPS_ISLAND_HEIST_FINALE_CHANCE_2, DEFAULT)
		g_bCasinoHeistCheaterIncremented = TRUE
		PRINTLN("[RCC MISSION][ISLAND_HEIST_ANTICHEAT] PSSPS_ISLAND_HEIST_FINALE_CHANCE_2 SET")
	ELIF NOT IS_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS_PASSED(PSSPS_ISLAND_HEIST_FINALE_CHANCE_3, DEFAULT)
		SET_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS(PSSPS_ISLAND_HEIST_FINALE_CHANCE_3, DEFAULT)
		g_bIslandHeistCheaterIncremented = TRUE
		PRINTLN("[RCC MISSION][ISLAND_HEIST_ANTICHEAT] PSSPS_ISLAND_HEIST_FINALE_CHANCE_3 SET")
	ENDIF
	
ENDPROC

PROC DECREMENT_CURRENT_ISLAND_HEIST_CHEATER_BITSET()

	IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
		IF IS_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS_PASSED(PSSPS_ISLAND_HEIST_FINALE_CHANCE_3, DEFAULT)
			CLEAR_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS(PSSPS_ISLAND_HEIST_FINALE_CHANCE_3, DEFAULT)
			PRINTLN("[RCC MISSION][ISLAND_HEIST_ANTICHEAT] PSSPS_ISLAND_HEIST_FINALE_CHANCE_3 CLEAR")
		ELIF IS_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS_PASSED(PSSPS_ISLAND_HEIST_FINALE_CHANCE_2, DEFAULT)
			CLEAR_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS(PSSPS_ISLAND_HEIST_FINALE_CHANCE_2, DEFAULT)
			PRINTLN("[RCC MISSION][ISLAND_HEIST_ANTICHEAT] PSSPS_ISLAND_HEIST_FINALE_CHANCE_2 CLEAR")
		ELIF IS_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS_PASSED(PSSPS_ISLAND_HEIST_FINALE_CHANCE_1, DEFAULT)
			CLEAR_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS(PSSPS_ISLAND_HEIST_FINALE_CHANCE_1, DEFAULT)
			PRINTLN("[RCC MISSION][ISLAND_HEIST_ANTICHEAT] PSSPS_ISLAND_HEIST_FINALE_CHANCE_1 CLEAR")
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_ISLAND_HEIST_LEADER_ANTI_CHEAT()

	INT iTeam
	IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PROCESSED_END_OF_MISSION_AWARDS)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(PARTICIPANT_ID())
		AND GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
			IF IS_THIS_MISSION_HEIST_ISLAND_FINAL_STAGE(g_FMMC_STRUCT.iRootContentIDHash)
				FOR iTeam = 0 TO (MC_ServerBD.iNumActiveTeams-1)
					IF NOT IS_BIT_SET(iLocalBoolCheck35, LBOOL35_ISLAND_HEIST_LEADER_CHEAT_CHECK_DONE)
						IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] >= (MC_serverBD.iMaxObjectives[ iTeam ]-1)
						AND MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
							IF SHOULD_END_OF_MISSION_HEIST_LOGIC_BE_PROCESSED_BECAUSE_OF_STRAND_MISSION()
								INCREMENT_CURRENT_ISLAND_HEIST_CHEATER_BITSET()
							ENDIF
							SET_BIT(iLocalBoolCheck35, LBOOL35_ISLAND_HEIST_LEADER_CHEAT_CHECK_DONE)
							PRINTLN("[RCC MISSION][ISLAND_HEIST_ANTICHEAT] MAINTAIN_ISLAND_HEIST_LEADER_ANTI_CHEAT CALLED because team on final objective: ",iTeam)
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cash Rewards
// ##### Description: Functions for calculating cash rewards
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT CALCULATE_ISLAND_HEIST_CASH_REWARD(BOOL bPass, INT iTotalTake, INT iTotalMissionTime)
	
	IF NOT bPass
		PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD - FAILED: No Cash Reward")
		RETURN 0
	ENDIF
	
	missionEndShardData.bRunOnReturnToFreemode = TRUE
	missionEndShardData.eType = MISSION_END_SHARD_TYPE_ISLAND_HEIST
	PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD missionEndShardData.eType = ",missionEndShardData.eType)
	missionEndShardData.bPass = bPass
	missionEndShardData.iTime = iTotalMissionTime
	missionEndShardData.iApproachID = ENUM_TO_INT(g_sHeistIslandConfig.eApproachVehicle)
	
	//Primary Target
	HEIST_ISLAND_PRIMARY_TARGETS eTarget = GET_PLAYER_HEIST_ISLAND_PRIMARY_TARGET(GB_GET_LOCAL_PLAYER_GANG_BOSS())
	missionEndShardData.iTargetID = GET_ISLAND_HEIST_REWARD_OVERVIEW_TARGET(eTarget)
	PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD - Primary Target: ", HEIST_ISLAND_PRIMARY_TARGETS_TO_STRING(eTarget))
	
	INT iPrimaryTargetValue = GET_HEIST_ISLAND_PRIMARY_TARGET_VALUE(eTarget)
	PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD - Primary Target Value: $", iPrimaryTargetValue)
	
	FLOAT fDifficultyMod = GET_HEIST_ISLAND_PRIMARY_TARGET_VALUE_DIFFICULTY_MODIFIER(g_FMMC_STRUCT.iDifficulity)
	PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD - Primary Target Difficulty Mod: ", fDifficultyMod)
	
	iPrimaryTargetValue = ROUND(iPrimaryTargetValue * fDifficultyMod)
	missionEndShardData.iTake[MISSION_END_SHARD_IH_MAIN_TAKE] = iPrimaryTargetValue
	PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD - Scaled Primary Target Value: ", iPrimaryTargetValue)
	
	//Side Loot Value
	INT iMaxSideLootValue = GET_PLAYER_ADDITIONAL_LOOT_TOTAL_VALUE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
	PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD - Max Side Loot Value: $", iMaxSideLootValue)
	
	//Wall Safe Value
	INT iMaxWallSafeValue = g_sMPTunables.iH4LOOT_MAX_SAFE_CASH_VALUE
	PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD - Max Wall Safe Value: $", iMaxWallSafeValue)
		
	missionEndShardData.iTake[MISSION_END_SHARD_IH_ADD_TAKE] = iTotalTake
	
	//Total Take
	iTotalTake += iPrimaryTargetValue
	INT iMaxTake = iPrimaryTargetValue + iMaxSideLootValue + iMaxWallSafeValue
	PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD - Max Take: $", iMaxTake)
	
	IF iTotalTake > iMaxTake
		PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD - Total Take Exceeds Max Take, capping to: $", iMaxTake)
		iTotalTake = iMaxTake
	ENDIF
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMaximumTake = iMaxTake
	
	//Deductions
	FLOAT fDeductionsMod = 1.0
	
	IF GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER() >= g_sMPTunables.iIH_DEDUCTION_FENCING_FEE_PLAYTHROUGH
		fDeductionsMod += g_sMPTunables.fIH_DEDUCTION_FENCING_FEE
		PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD - Playthroughs (", GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER(), ") exceeds ", 
					g_sMPTunables.iIH_DEDUCTION_FENCING_FEE_PLAYTHROUGH, ". Adding ", g_sMPTunables.fIH_DEDUCTION_FENCING_FEE, " to Deductions Mod: ", fDeductionsMod)
		missionEndShardData.iMissionFee = iTotalTake - ROUND(iTotalTake * (1.0 + g_sMPTunables.fIH_DEDUCTION_FENCING_FEE))
	ELSE
		missionEndShardData.iMissionFee = 0
	ENDIF
	
	fDeductionsMod += g_sMPTunables.fIH_DEDUCTION_PAVEL_CUT
	PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD - Adding Pavel cut deduction ", g_sMPTunables.fIH_DEDUCTION_PAVEL_CUT, " to Deductions Mod: ", fDeductionsMod)
	missionEndShardData.npcTake[0].iTake = iTotalTake - ROUND(iTotalTake * (1.0 + g_sMPTunables.fIH_DEDUCTION_PAVEL_CUT))
	
	PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD - Total deductions mod: ", fDeductionsMod)
	
	iTotalTake = ROUND(iTotalTake * fDeductionsMod)
	PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD - Total take after deductions: $", iTotalTake)
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iActualTake = iTotalTake
	
	//Player Cut
	INT iCut = g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent
	INT iPlayer = 0
	INT iExtraCut = 0
	INT iGangBoss = NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())
	
	//Check for leaving players
	IF iGangBoss > -1
		FOR iPlayer = 0 TO MAX_NHPG_PLAYERS - 1
			
			IF GlobalPlayerBD_NetHeistPlanningGeneric[iGangBoss].iRewardPercentages[iPlayer] <= 0
				RELOOP
			ENDIF
			
			PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(GlobalPlayerBD_NetHeistPlanningGeneric[iGangBoss].iPlayerOrder[iPlayer])
			
			IF playerID = INVALID_PLAYER_INDEX()
				RELOOP
			ENDIF
			
			IF NETWORK_IS_PLAYER_ACTIVE(playerID)
				RELOOP
			ENDIF
			
			PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD - Player ", iPlayer, " quit. Adding their cut to the leader's: ", GlobalPlayerBD_NetHeistPlanningGeneric[iGangBoss].iRewardPercentages[iPlayer])
			
			iExtraCut += GlobalPlayerBD_NetHeistPlanningGeneric[iGangBoss].iRewardPercentages[iPlayer]
			
		ENDFOR
	ENDIF
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer
	AND iExtraCut > 0
		iCut += iExtraCut
	ENDIF
	
	FLOAT fCut = TO_FLOAT(iCut)
	PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD - My Cut : ", fCut, "%")
	INT iLocalPlayerCut = ROUND(iTotalTake * (fCut / 100))
	PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD - My Cut: $", iLocalPlayerCut)
	
	IF iGangBoss = -1
	OR NOT NETWORK_IS_PLAYER_ACTIVE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
		PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD - GANG BOSS NOT ACTIVE! ZEROING CUT")
		iLocalPlayerCut = 0
		iTotalTake = 0
	ENDIF
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyCutPercentage = iCut
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake = iLocalPlayerCut
	
	IF iGangBoss > -1
		
		FOR iPlayer = 0 TO MAX_NHPG_PLAYERS - 1
			missionEndShardData.playerCut[iPlayer].bLeader 					= FALSE
			missionEndShardData.playerCut[iPlayer].playerID 				= INVALID_PLAYER_INDEX()
			missionEndShardData.playerCut[iPlayer].playerName				= ""
			missionEndShardData.playerCut[iPlayer].iCut						= 0
		ENDFOR
		
		FOR iPlayer = 0 TO MAX_NHPG_PLAYERS - 1
			
			PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(GlobalPlayerBD_NetHeistPlanningGeneric[iGangBoss].iPlayerOrder[iPlayer])
			
			IF playerID = INVALID_PLAYER_INDEX()
				RELOOP
			ENDIF
			
			IF NOT NETWORK_IS_PLAYER_ACTIVE(playerID)
				RELOOP
			ENDIF
			
			missionEndShardData.playerCut[iPlayer].bLeader = (iGangBoss = GlobalPlayerBD_NetHeistPlanningGeneric[iGangBoss].iPlayerOrder[iPlayer])
			missionEndShardData.playerCut[iPlayer].playerID = INT_TO_PLAYERINDEX(GlobalPlayerBD_NetHeistPlanningGeneric[iGangBoss].iPlayerOrder[iPlayer])
			IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(GlobalPlayerBD_NetHeistPlanningGeneric[iGangBoss].iPlayerOrder[iPlayer]), FALSE)
				missionEndShardData.playerCut[iPlayer].playerName = GET_PLAYER_NAME(INT_TO_PLAYERINDEX(GlobalPlayerBD_NetHeistPlanningGeneric[iGangBoss].iPlayerOrder[iPlayer]))
			ENDIF
			
			FLOAT fPlayerCut = TO_FLOAT(GlobalPlayerBD_NetHeistPlanningGeneric[iGangBoss].iRewardPercentages[iPlayer])
			IF playerID = LocalPlayer
				fPlayerCut = fCut
			ELIF missionEndShardData.playerCut[iPlayer].bLeader
			AND iExtraCut > 0
				fPlayerCut += iExtraCut
			ENDIF
			missionEndShardData.playerCut[iPlayer].iCut = ROUND(iTotalTake * (fPlayerCut / 100))
			
			PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD - Player ", iPlayer, " Cut: $", 		missionEndShardData.playerCut[iPlayer].iCut)
			PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD - Player ", iPlayer, " playerName: ", 	missionEndShardData.playerCut[iPlayer].playerName)
			PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD - Player ", iPlayer, " playerID: ", 	NATIVE_TO_INT(missionEndShardData.playerCut[iPlayer].playerID))
			PRINTLN("[Rewards_Cash] - CALCULATE_ISLAND_HEIST_CASH_REWARD - Player ", iPlayer, " bLeader: ", 	BOOL_TO_STRING(missionEndShardData.playerCut[iPlayer].bLeader))
			
		ENDFOR
	ENDIF
	
	RETURN iLocalPlayerCut
	
ENDFUNC

PROC PROCESS_GIVING_ISLAND_HEIST_CASH_REWARD(INT iCachedReward)
	
	INT iScriptTransactionIndex
	
	IF USE_SERVER_TRANSACTIONS()
		TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_ISLAND_HEIST_FINALE, iCachedReward, iScriptTransactionIndex, DEFAULT, DEFAULT)
		g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
		g_cashTransactionData[iScriptTransactionIndex].eEssentialData.iItemID = iCachedReward
		g_cashTransactionData[iScriptTransactionIndex].eEssentialData.iCost = iCachedReward
	ELSE
		NETWORK_EARN_ISLAND_HEIST(iCachedReward, iCachedReward, 0, 0, 0, 0)
	ENDIF	
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: RP Rewards
// ##### Description: Functions for calculating cash rewards
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC FLOAT CALCULATE_ISLAND_HEIST_RP(BOOL bPass, INT iMissionEndTime, INT iMedal)

	PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - ##################################################")
	PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - Time: ", FLOOR(TO_FLOAT(iMissionEndTime - 500 - (iMissionEndTime - 500) % 1000) / 60000.0))
	PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - Role: ", GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer)
	PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - Medal: ", iMedal)
	PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - Difficulty: ", g_FMMC_STRUCT.iDifficulity)
	PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - Pass: ", bPass)
	PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - ##################################################")
	
	IF NOT bPass
	AND IS_THIS_A_QUICK_RESTART_JOB() 
		PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - QUICK RESTART - NO RP") 
		RETURN 0.0
	ENDIF
	
	FLOAT fMedalMulti, fRPGained, fStage, fRole, fTimeScale, fFailDivider

	FLOAT fBaseMulti = g_sMPTunables.fIH_RP_Base_Multiplier              
	PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - fBaseMulti : ",fBaseMulti)
	
	FLOAT fMissionRank = TO_FLOAT(g_FMMC_STRUCT.iRank) / 2
	IF fMissionRank > g_sMPTunables.iIH_RP_Rank_Cap                      
		fMissionRank = TO_FLOAT(g_sMPTunables.iIH_RP_Rank_Cap)
	ENDIF
	fMissionRank += g_sMPTunables.iIH_RP_Basic_Value 
	PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - fMissionRank : ",fMissionRank)
	
	IF bPass
		SWITCH iMedal
			CASE ci_MEDALRANK_PLATINUM
				fMedalMulti = g_sMPTunables.fRP_IH_Platinum_Medal_RP_Multiplier	
			BREAK
			CASE ci_MEDALRANK_GOLD
				fMedalMulti = g_sMPTunables.fRP_IH_Gold_Medal_RP_Multiplier	
			BREAK
			CASE ci_MEDALRANK_SILVER
				fMedalMulti = g_sMPTunables.fRP_IH_Silver_Medal_RP_Multiplier	
			BREAK
			CASE ci_MEDALRANK_BRONZE
				fMedalMulti = g_sMPTunables.fRP_IH_Bronze_Medal_RP_Multiplier	
			BREAK
			DEFAULT
				fMedalMulti = 1.0
			BREAK
		ENDSWITCH
	ELSE
		fMedalMulti = 1.0
	ENDIF
	PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - medal bonus: ", fMedalMulti) 
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer
		fRole = g_sMPTunables.fIH_RP_Role_Boss_Bonus
	ELSE
		fRole = g_sMPTunables.fIH_RP_Role_Goon_Bonus  
	ENDIF
	PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - Role Bonus: ", fRole) 
	
	fStage = g_sMPTunables.fIH_RP_Stage_Finale_Bonus           
	PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - Stage Bonus: ", fStage) 
	
	IF iMissionEndTime <= (g_sMPTunables.iIH_fail_RP_time_period_1 * 60000)
		fTimeScale = g_sMPTunables.fIH_RP_Time_Period_1_Percentage     
	ELIF iMissionEndTime <= (g_sMPTunables.iIH_fail_RP_time_period_2 * 60000)
		fTimeScale = g_sMPTunables.fIH_RP_Time_Period_2_Percentage     
		fFailDivider =g_sMPTunables.fIH_Fail_RP_Time_Period_2_Divider
	ELIF iMissionEndTime <= (g_sMPTunables.iIH_fail_RP_time_period_3 * 60000)
		fTimeScale = g_sMPTunables.fIH_RP_Time_Period_3_Percentage     
		fFailDivider =g_sMPTunables.fIH_Fail_RP_Time_Period_3_Divider
	ELIF iMissionEndTime <= (g_sMPTunables.iIH_fail_RP_time_period_4 * 60000)
		fTimeScale = g_sMPTunables.fIH_RP_Time_Period_4_Percentage     
		fFailDivider =g_sMPTunables.fIH_Fail_RP_Time_Period_4_Divider
	ELIF iMissionEndTime <= (g_sMPTunables.iIH_fail_RP_time_period_5 * 60000)
		fTimeScale = g_sMPTunables.fIH_RP_Time_Period_5_Percentage     
		fFailDivider =g_sMPTunables.fIH_Fail_RP_Time_Period_5_Divider
	ELIF iMissionEndTime <= (g_sMPTunables.iIH_fail_RP_time_period_6 * 60000)
		fTimeScale = g_sMPTunables.fIH_RP_Time_Period_6_Percentage     
		fFailDivider =g_sMPTunables.fIH_Fail_RP_Time_Period_6_Divider
	ELIF iMissionEndTime <= (g_sMPTunables.iIH_fail_RP_time_period_7 * 60000)
		fTimeScale = g_sMPTunables.fIH_RP_Time_Period_7_Percentage     
		fFailDivider =g_sMPTunables.fIH_Fail_RP_Time_Period_7_Divider	
	ELIF iMissionEndTime <= (g_sMPTunables.iIH_fail_RP_time_period_8 * 60000)
		fTimeScale = g_sMPTunables.fIH_RP_Time_Period_8_Percentage     
		fFailDivider =g_sMPTunables.fIH_Fail_RP_Time_Period_8_Divider
	ELIF iMissionEndTime <= (g_sMPTunables.iIH_fail_RP_time_period_9 * 60000)
		fTimeScale = g_sMPTunables.fIH_RP_Time_Period_9_Percentage     
		fFailDivider =g_sMPTunables.fIH_Fail_RP_Time_Period_9_Divider	
	ELSE
		fTimeScale = g_sMPTunables.fIH_RP_Time_Period_10_Percentage     
		fFailDivider = g_sMPTunables.fIH_Fail_RP_Time_Period_10_Divider
	ENDIF
	PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - Time Bonus: time = ", iMissionEndTime, " scale = ", fTimeScale, " divider = ", fFailDivider) 
	
	fRPGained = fBaseMulti * fMissionRank * fRole * fTimeScale * (fStage + fMedalMulti)
	PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - RP Calculation: ", fRPGained) 
	
	SWITCH(g_FMMC_STRUCT.iDifficulity)
		CASE DIFF_NORMAL
			fRPGained *= g_sMPTunables.fIH_difficulty_normal
			PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - Normal Difficulty Scaler: ", g_sMPTunables.fIH_difficulty_normal) 
		BREAK
		CASE DIFF_HARD
			fRPGained *= g_sMPTunables.fIH_difficulty_hard
			PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - Hard Difficulty Scaler: ", g_sMPTunables.fIH_difficulty_hard) 
		BREAK
	ENDSWITCH
	PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - After difficulty scaling: ", fRPGained) 

	IF NOT bPass
		IF fFailDivider = 0
			PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - fFailDivider = 0") 
			fRPGained = 0
		ELSE
			fRPGained /= fFailDivider
			PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - After Fail Divider: ", fRPGained) 
		ENDIF
		
		IF fRPGained < TO_FLOAT(g_sMPTunables.iIH_Fail_RP_Minimum)
			fRPGained = TO_FLOAT(g_sMPTunables.iIH_Fail_RP_Minimum)
			PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - RP set to min: ", fRPGained) 
		ENDIF
	ENDIF
	
	IF fRPGained > TO_FLOAT(g_sMPTunables.iIH_JOB_RP_CAP)
		fRPGained = TO_FLOAT(g_sMPTunables.iIH_JOB_RP_CAP)
		PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - Capping to max: ", fRPGained) 
	ENDIF
	
	PRINTLN("[Rewards_RP] - CALCULATE_ISLAND_HEIST_RP - Final RP : ", fRPGained) 
	
	RETURN fRPGained
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Elite Challenges
// ##### Description: Functions for checkign if players have compelted Elite Challenges and giving those rewards
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_ISLAND_HEIST_ELITE_CHALLENGE_CASH()
	
	INT iCash = g_sMPTunables.iIH_ELITE_CHALLENGE_REWARD
	PRINTLN("[Rewards_Elite] - GET_ISLAND_HEIST_ELITE_CHALLENGE_CASH - Base Reward $", g_sMPTunables.iIH_ELITE_CHALLENGE_REWARD)
	
	IF g_FMMC_STRUCT.iDifficulity = DIFF_HARD
	AND g_sMPTunables.fIH_ELITE_CHALLENGE_HARD_MOD > 0.0
		iCash = ROUND(iCash * g_sMPTunables.fIH_ELITE_CHALLENGE_HARD_MOD)
		PRINTLN("[Rewards_Elite] - GET_ISLAND_HEIST_ELITE_CHALLENGE_CASH - Hard Mod: ", g_sMPTunables.fIH_ELITE_CHALLENGE_HARD_MOD, " New Reward: $", iCash)
	ENDIF
	
	RETURN iCash
	
ENDFUNC

PROC MARK_ISLAND_HEIST_ELITE_CHALLENGE_AS_COMPLETE()

	missionEndShardData.iEliteChallenge = ISLAND_HEIST_END_ELITE_COMPLETE
	//Tracked as an award
	PRINTLN("[Rewards_Elite] - MARK_ISLAND_HEIST_ELITE_CHALLENGE_AS_COMPLETE - No Stats setup for this content.")

ENDPROC

PROC GIVE_PLAYER_ISLAND_HEIST_ELITE_CHALLENGE_REWARD(INT iEliteChallengeReward)
	
	missionEndShardData.iEliteChallengeCash = iEliteChallengeReward

	INT iScriptTransactionIndex
	IF USE_SERVER_TRANSACTIONS()
		TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_ISLAND_HEIST_ELITE_CHALLENGE, iEliteChallengeReward, iScriptTransactionIndex, FALSE, TRUE)
		g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
		g_cashTransactionData[iScriptTransactionIndex].eEssentialData.iExtraItemID = iEliteChallengeReward
		g_cashTransactionData[iScriptTransactionIndex].cashInfo.iItemHash = HASH("ELITE_ISLAND_HEIST")
	ELSE
		NETWORK_EARN_ISLAND_HEIST(iEliteChallengeReward, 0, iEliteChallengeReward, HASH("ELITE_ISLAND_HEIST"), 0, 0)
	ENDIF

ENDPROC

FUNC BOOL HAVE_ISLAND_HEIST_ELITE_CHALLENGE_CONDITIONS_BEEN_MET(INT &iNumEliteComponents)
	
	UNUSED_PARAMETER(iNumEliteComponents) //Not needed for the current challenges
	BOOL bGiveReward = TRUE
	
	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iGenericTrackingBitset, ciContinuityGenericTrackingBS_DroppedToDirect)
		IF g_sHeistIslandConfig.eInfiltrationPoint = HIIP_AIRSTRIP
		AND NOT IS_BIT_SET(g_sHeistIslandConfig.iAbilitiesBitset, HEIST_ISLAND_CONFIG_ABILITIES_BITSET__WEAPON_STASH)
			//Started in Direct - Challenge Valid
		ELSE
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentDroppedToDirect = TRUE
			bGiveReward = FALSE
			PRINTLN("[Rewards_Elite] - HAVE_ISLAND_HEIST_ELITE_CHALLENGE_CONDITIONS_BEEN_MET - Dropped to the direct route, challenge void.")
		ENDIF
	ENDIF
	
	RETURN bGiveReward
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Awards and Challenges
// ##### Description: Functions for giving awards/achievements
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_ISLAND_HEIST_AWARD_DISABLED(MP_BOOL_AWARD eAward)

	SWITCH eAward
		CASE MP_AWARD_PROFESSIONAL			RETURN g_sMPTunables.bDisableIHAward_Professional
		CASE MP_AWARD_ELITE_THIEF			RETURN g_sMPTunables.bDisableIHAward_EliteThief
		CASE MP_AWARD_THE_ISLAND_HEIST		RETURN g_sMPTunables.bDisableIHAward_TheIslandHeist
		CASE MP_AWARD_GOING_ALONE			RETURN g_sMPTunables.bDisableIHAward_GoingAlone
		CASE MP_AWARD_TEAM_WORK				RETURN g_sMPTunables.bDisableIHAward_TeamWork
		CASE MP_AWARD_CAT_BURGLAR			RETURN g_sMPTunables.bDisableIHAward_CatBurglar
		CASE MP_AWARD_PRO_THIEF				RETURN g_sMPTunables.bDisableIHAward_ProThief
		CASE MP_AWARD_MIXING_UP				RETURN g_sMPTunables.bDisableIHAward_MixingItUp
		CASE MP_AWARD_GOLDEN_GUN			RETURN g_sMPTunables.bDisableIHAward_GoForGold
		CASE MP_AWARD_ONE_OF_THEM			RETURN g_sMPTunables.bDisableIHAward_OneOfThem
	ENDSWITCH

	ASSERTLN("[Rewards] - IS_ISLAND_HEIST_AWARD_DISABLED - No tunable for award: ", eAward)
	PRINTLN("[Rewards] - IS_ISLAND_HEIST_AWARD_DISABLED - No tunable for award: ", eAward)
	
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_ISLAND_HEIST_AWARD_PROFESSIONAL()
	
	//Complete the Island Heist on Hard and without losing a life
	
	IF IS_ISLAND_HEIST_AWARD_DISABLED(MP_AWARD_PROFESSIONAL)
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_PROFESSIONAL - Award Disabled.")
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.iDifficulity != DIFF_HARD
		EXIT
	ENDIF
	
	IF GET_TEAM_DEATHS(0) > 0
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_PROFESSIONAL - Invalid - has lost a life.")
		EXIT
	ENDIF
	
	IF g_TransitionSessionNonResetVars.bHasQuickRestartedDuringStrandMission
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_PROFESSIONAL - Invalid - has restarted.")
		EXIT
	ENDIF
	
	PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_PROFESSIONAL - Completed.")
	
	PROCESS_GIVING_FMMC_CHARACTER_AWARD(MP_AWARD_PROFESSIONAL)
	
ENDPROC

PROC PROCESS_ISLAND_HEIST_AWARD_ELITE_THIEF()
	
	//Complete the elite challenges for the Island Heist
	
	IF IS_ISLAND_HEIST_AWARD_DISABLED(MP_AWARD_ELITE_THIEF)
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_ELITE_THIEF - Award Disabled.")
		EXIT
	ENDIF
	
	IF !g_TransitionSessionNonResetVars.sGlobalCelebrationData.bEliteChallengeComplete
		EXIT
	ENDIF
	
	PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_ELITE_THIEF - Completed.")
	
	PROCESS_GIVING_FMMC_CHARACTER_AWARD(MP_AWARD_ELITE_THIEF)

ENDPROC

PROC PROCESS_ISLAND_HEIST_AWARD_GO_FOR_GOLD()
	
	//Steal El Rubio’s prized weapon during The Island Heist
	
	IF IS_ISLAND_HEIST_AWARD_DISABLED(MP_AWARD_GOLDEN_GUN)
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_GO_FOR_GOLD - Award Disabled.")
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_MiscActionsBS], ciLocalContinuityTelemetry_miscActions_GoldenGunCollected)
		EXIT
	ENDIF
	
	PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_GO_FOR_GOLD - Completed.")
	
	PROCESS_GIVING_FMMC_CHARACTER_AWARD(MP_AWARD_GOLDEN_GUN)
	
ENDPROC

PROC PROCESS_ISLAND_HEIST_AWARD_ONE_OF_THEM()
	
	//Enter the compound using a stolen disguise during The Island Heist.
	
	IF IS_ISLAND_HEIST_AWARD_DISABLED(MP_AWARD_ONE_OF_THEM)
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_ONE_OF_THEM - Award Disabled.")
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.eContinuityContentSpecificType != FMMC_MISSION_CONTINUITY_CONTENT_SPECIFIC_TYPE_ISLAND_HEIST
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_VEHICLE_FRONT_GATE)
		EXIT
	ENDIF
	
	PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_ONE_OF_THEM - Completed.")
	
	PROCESS_GIVING_FMMC_CHARACTER_AWARD(MP_AWARD_ONE_OF_THEM)
	
ENDPROC

PROC PROCESS_ISLAND_HEIST_AWARD_THE_ISLAND_HEIST()
	
	//Complete The Island Heist for the first time
	
	IF IS_ISLAND_HEIST_AWARD_DISABLED(MP_AWARD_THE_ISLAND_HEIST)
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_THE_ISLAND_HEIST - Award Disabled.")
		EXIT
	ENDIF
	
	PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_THE_ISLAND_HEIST - Completed.")
	
	PROCESS_GIVING_FMMC_CHARACTER_AWARD(MP_AWARD_THE_ISLAND_HEIST)
	
ENDPROC

PROC PROCESS_ISLAND_HEIST_AWARD_GOING_ALONE()
	
	//Complete The Island Heist with one player
	
	IF IS_ISLAND_HEIST_AWARD_DISABLED(MP_AWARD_GOING_ALONE)
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_GOING_ALONE - Award Disabled.")
		EXIT
	ENDIF
	
	IF MC_serverBD.iNumStartingPlayers[0] != 1
		EXIT
	ENDIF
	
	PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_GOING_ALONE - Completed.")
	
	PROCESS_GIVING_FMMC_CHARACTER_AWARD(MP_AWARD_GOING_ALONE)
	
ENDPROC

PROC PROCESS_ISLAND_HEIST_AWARD_TEAM_WORK()
	
	//Complete The Island Heist with four players
	
	IF IS_ISLAND_HEIST_AWARD_DISABLED(MP_AWARD_TEAM_WORK)
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_TEAM_WORK - Award Disabled.")
		EXIT
	ENDIF
	
	IF MC_serverBD.iNumStartingPlayers[0] != 4
		EXIT
	ENDIF
	
	PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_TEAM_WORK - Completed.")
	
	PROCESS_GIVING_FMMC_CHARACTER_AWARD(MP_AWARD_TEAM_WORK)
	
ENDPROC

PROC PROCESS_ISLAND_HEIST_AWARD_CAT_BURGLAR()
	
	//Complete The Island Heist without ever alerting the guards
	
	IF IS_ISLAND_HEIST_AWARD_DISABLED(MP_AWARD_CAT_BURGLAR)
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_CAT_BURGLAR - Award Disabled.")
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iGenericTrackingBitset, ciContinuityGenericTrackingBS_DroppedToDirect)
		EXIT
	ENDIF
	
	PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_CAT_BURGLAR - Completed.")
	
	PROCESS_GIVING_FMMC_CHARACTER_AWARD(MP_AWARD_CAT_BURGLAR)
	
ENDPROC

PROC PROCESS_ISLAND_HEIST_AWARD_MIXING_IT_UP()
	
	//Approach The Island Heist using all of the different vehicles
	
	SWITCH g_sHeistIslandConfig.eApproachVehicle
		CASE HIAV_SUBMARINE
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CR_SUBMARINE, GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_SUBMARINE) + 1)
			PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_MIXING_IT_UP - Incremented Submarine Attempts to: ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_SUBMARINE))
		BREAK
		CASE HIAV_STRATEGIC_BOMBER
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CR_STRATEGIC_BOMBER, GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_STRATEGIC_BOMBER) + 1)
			PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_MIXING_IT_UP - Incremented Strategic Bomber Attempts to: ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_STRATEGIC_BOMBER))
		BREAK
		CASE HIAV_SMUGGLER_PLANE
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CR_SMUGGLER_PLANE, GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_SMUGGLER_PLANE) + 1)
			PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_MIXING_IT_UP - Incremented Smuggler Plane Attempts to: ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_SMUGGLER_PLANE))
		BREAK
		CASE HIAV_STEALTH_HELICOPTER
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CR_STEALTH_HELI, GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_STEALTH_HELI) + 1)
			PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_MIXING_IT_UP - Incremented Stealth Heli Attempts to: ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_STEALTH_HELI))
		BREAK
		CASE HIAV_PATROL_BOAT
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CR_PATROL_BOAT, GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_PATROL_BOAT) + 1)
			PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_MIXING_IT_UP - Incremented Patrol Boat Attempts to: ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_PATROL_BOAT))
		BREAK
		CASE HIAV_SMUGGLER_BOAT
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CR_SMUGGLER_BOAT, GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_SMUGGLER_BOAT) + 1)
			PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_MIXING_IT_UP - Incremented Smuggler Boat Attempts to: ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_SMUGGLER_BOAT))
		BREAK
	ENDSWITCH
	
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_SUBMARINE) < 1
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_MIXING_IT_UP - Still need to use Sub")
		EXIT
	ENDIF

	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_STRATEGIC_BOMBER) < 1
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_MIXING_IT_UP - Still need to use Bomber")
		EXIT
	ENDIF
	
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_SMUGGLER_PLANE) < 1
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_MIXING_IT_UP - Still need to use Plane")
		EXIT
	ENDIF
	
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_STEALTH_HELI) < 1
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_MIXING_IT_UP - Still need to use Heli")
		EXIT
	ENDIF
	
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_PATROL_BOAT) < 1
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_MIXING_IT_UP - Still need to use Patrol boat")
		EXIT
	ENDIF
	
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_SMUGGLER_BOAT) < 1
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_MIXING_IT_UP - Still need to use Smuggler boat")
		EXIT
	ENDIF
		
	IF IS_ISLAND_HEIST_AWARD_DISABLED(MP_AWARD_MIXING_UP)
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_MIXING_IT_UP - Award Disabled.")
		EXIT
	ENDIF
	
	PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_MIXING_IT_UP - Completed.")
	
	PROCESS_GIVING_FMMC_CHARACTER_AWARD(MP_AWARD_MIXING_UP)
	
ENDPROC

PROC PROCESS_ISLAND_HEIST_AWARD_PRO_THIEF()
	
	//Steal all variations of the main target from the basement during The Island Heist
	
	SWITCH g_sHeistIslandConfig.ePrimaryTarget
		CASE HIPT_TEQUILA
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CR_TEQUILA, GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_TEQUILA) + 1)
			PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_PRO_THIEF - Incremented Tequila thefts to: ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_TEQUILA))
		BREAK
		
		CASE HIPT_PEARL_NECKLACE
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CR_PEARL_NECKLACE, GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_PEARL_NECKLACE) + 1)
			PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_PRO_THIEF - Incremented Necklace thefts to: ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_PEARL_NECKLACE))
		BREAK
		
		CASE HIPT_BEARER_BONDS
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CR_BEARER_BONDS, GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_BEARER_BONDS) + 1)
			PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_PRO_THIEF - Incremented Bonds thefts to: ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_BEARER_BONDS))
		BREAK
		
		CASE HIPT_PINK_DIAMOND
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CR_PINK_DIAMOND, GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_PINK_DIAMOND) + 1)
			PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_PRO_THIEF - Incremented Diamond thefts to: ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_PINK_DIAMOND))
		BREAK
		
		CASE HIPT_MADRAZO_FILES
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CR_MADRAZO_FILES, GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_MADRAZO_FILES) + 1)
			PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_PRO_THIEF - Incremented Files thefts to: ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_MADRAZO_FILES))
		BREAK
		
		CASE HIPT_SAPPHIRE_PANTHER_STATUE
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CR_SAPHIREPANSTAT, GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_SAPHIREPANSTAT) + 1)
			PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_PRO_THIEF - Incremented Panther thefts to: ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_SAPHIREPANSTAT))
		BREAK
	ENDSWITCH
	
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_TEQUILA) < 1
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_PRO_THIEF - Still need to steal Tequila")
		EXIT
	ENDIF
	
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_PEARL_NECKLACE) < 1
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_PRO_THIEF - Still need to steal Necklace")
		EXIT
	ENDIF
	
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_BEARER_BONDS) < 1
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_PRO_THIEF - Still need to steal Bonds")
		EXIT
	ENDIF
	
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_PINK_DIAMOND) < 1
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_PRO_THIEF - Still need to steal Diamond")
		EXIT
	ENDIF
	
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_MADRAZO_FILES) < 1
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_PRO_THIEF - Still need to steal Files")
		EXIT
	ENDIF
		
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_SAPHIREPANSTAT) < 1
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_PRO_THIEF - Still need to steal Panther")
		EXIT
	ENDIF	
		
	IF IS_ISLAND_HEIST_AWARD_DISABLED(MP_AWARD_PRO_THIEF)
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_PRO_THIEF - Award Disabled.")
		EXIT
	ENDIF
	
	PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_PRO_THIEF - Completed.")
	
	PROCESS_GIVING_FMMC_CHARACTER_AWARD(MP_AWARD_PRO_THIEF)
	
ENDPROC

PROC PROCESS_ISLAND_HEIST_AWARD_FILL_YOUR_BAGS()
	
	//Steal a total of $X in additional loot during The Island Heist.
	
	IF g_sMPTunables.bDisableIHAward_FillYourBags
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_FILL_YOUR_BAGS - Award Disabled.")
		EXIT
	ENDIF
	
	IF MC_playerBD[iLocalPart].sLootBag.iCashGrabbed <= 0
		EXIT
	ENDIF
	
	INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FILL_YOUR_BAGS, MC_playerBD[iLocalPart].sLootBag.iCashGrabbed)
	
	PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_AWARD_FILL_YOUR_BAGS - Incremented to: ", GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FILL_YOUR_BAGS))
	
ENDPROC

PROC PROCESS_ISLAND_HEIST_AWARDS()
	
	PROCESS_ISLAND_HEIST_AWARD_ELITE_THIEF()
	PROCESS_ISLAND_HEIST_AWARD_PROFESSIONAL()
	PROCESS_ISLAND_HEIST_AWARD_GO_FOR_GOLD()
	PROCESS_ISLAND_HEIST_AWARD_ONE_OF_THEM()
	PROCESS_ISLAND_HEIST_AWARD_THE_ISLAND_HEIST()
	PROCESS_ISLAND_HEIST_AWARD_GOING_ALONE()
	PROCESS_ISLAND_HEIST_AWARD_TEAM_WORK()
	PROCESS_ISLAND_HEIST_AWARD_CAT_BURGLAR()
	PROCESS_ISLAND_HEIST_AWARD_MIXING_IT_UP()
	PROCESS_ISLAND_HEIST_AWARD_PRO_THIEF()
	PROCESS_ISLAND_HEIST_AWARD_FILL_YOUR_BAGS()
	
ENDPROC

PROC PROCESS_ISLAND_HEIST_COMPLETION_VETIR_UNLOCK()

	IF g_FMMC_STRUCT.eContinuityContentSpecificType != FMMC_MISSION_CONTINUITY_CONTENT_SPECIFIC_TYPE_ISLAND_HEIST
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_VEHICLE_FRONT_GATE)
		EXIT
	ENDIF
	
	IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_COMPLETE_H4_F_USING_VETIR)
		EXIT
	ENDIF
	
	PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_COMPLETION_VETIR_UNLOCK - Player used the Vetir, unlock trade price for Vetir")
	SET_MP_BOOL_CHARACTER_STAT(MP_STAT_COMPLETE_H4_F_USING_VETIR, TRUE)
	
ENDPROC

PROC PROCESS_ISLAND_HEIST_COMPLETION_SCOPING()

	IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.eContinuityContentSpecificType != FMMC_MISSION_CONTINUITY_CONTENT_SPECIFIC_TYPE_ISLAND_HEIST
		EXIT
	ENDIF

	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_EXPLOSIVES_FRONT_GATE)
		SET_PLAYER_HAS_SCOPED_HEIST_ISLAND_COMPOUND_ENTRANCE(HICE_MAIN_GATE, TRUE)
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_COMPLETION_SCOPING - Setting Entrance as scoped for next play - Explosives Front Gate.")
	ENDIF
		
	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_KEYCARD_SOUTH)
		SET_PLAYER_HAS_SCOPED_HEIST_ISLAND_COMPOUND_ENTRANCE(HICE_SOUTH_SIDE_GATE, TRUE)
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_COMPLETION_SCOPING - Setting Entrance as scoped for next play - Keycard West.")
	ENDIF
		
	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_KEYCARD_NORTH)
		SET_PLAYER_HAS_SCOPED_HEIST_ISLAND_COMPOUND_ENTRANCE(HICE_NORTH_SIDE_GATE, TRUE)
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_COMPLETION_SCOPING - Setting Entrance as scoped for next play - Keycard East.")
	ENDIF
		
	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_RAPPEL_SOUTH)
		SET_PLAYER_HAS_SCOPED_HEIST_ISLAND_COMPOUND_ENTRANCE(HICE_SOUTH_WALL, TRUE)
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_COMPLETION_SCOPING - Setting Entrance as scoped for next play - Rappel West.")
	ENDIF
		
	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_RAPPEL_NORTH)
		SET_PLAYER_HAS_SCOPED_HEIST_ISLAND_COMPOUND_ENTRANCE(HICE_NORTH_WALL, TRUE)
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_COMPLETION_SCOPING - Setting Entrance as scoped for next play - Rappel East.")
	ENDIF
		
	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_VEHICLE_FRONT_GATE)
		SET_PLAYER_HAS_SCOPED_HEIST_ISLAND_COMPOUND_ENTRANCE(HICE_MAIN_GATE, TRUE)
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_COMPLETION_SCOPING - Setting Entrance as scoped for next play - Front Gate.")
	ENDIF
		
	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_SEWER_GRATE)
		SET_PLAYER_HAS_SCOPED_HEIST_ISLAND_COMPOUND_ENTRANCE(HICE_TUNNEL, TRUE)
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_COMPLETION_SCOPING - Setting Entrance as scoped for next play - Sewer Grate.")
	ENDIF
	
ENDPROC

PROC COMPLETE_ISLAND_HEIST_ITEM_REWARD(HEIST_ISLAND_EVENT_AWARDS eHeistAward)
	
	IF NOT HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		EXIT
	ENDIF

	IF IS_ISLAND_HEIST_EVENT_ALLOWED(eHeistAward)
		IF NOT DOES_LOCAL_PLAYER_OWN_ISLAND_HEIST_EVENT_ITEM(eHeistAward)
			GIVE_LOCAL_PLAYER_ISLAND_HEIST_EVENT_ITEM_REWARD(eHeistAward)
		ELSE
			PRINTLN("[Rewards_Unlocks] COMPLETE_ISLAND_HEIST_ITEM_REWARD - Local player already has island heist reward")
		ENDIF
	ELSE
		PRINTLN("[Rewards_Unlocks] COMPLETE_ISLAND_HEIST_ITEM_REWARD - Not allowed this island heist reward item")
	ENDIF
	
ENDPROC

PROC PROCESS_ISLAND_HEIST_COMPLETION()
	
	PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_COMPLETION - Heist Island Final Stage Passed Leader: ", GET_STRING_FROM_BOOL(GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()))
	SET_COMPLETED_ISLAND_HEIST(GB_GET_LOCAL_PLAYER_GANG_BOSS())
		
	PROCESS_ISLAND_HEIST_COMPLETION_VETIR_UNLOCK()
	
	PROCESS_ISLAND_HEIST_COMPLETION_SCOPING()
	
	COMPLETE_ISLAND_HEIST_ITEM_REWARD(STEAL_PRIMARY_TARGET_AND_COMPLETE_THE_FINALE)
	
	IF IS_SUM22_EVENT_REWARD_ACTIVE(SUM22_EVENT_REWARD_COMPLETE_CAYO_PERICO_FINALE)
	AND NOT DOES_LOCAL_PLAYER_OWN_SUM22_EVENT_REWARD(SUM22_EVENT_REWARD_COMPLETE_CAYO_PERICO_FINALE)
		GIVE_LOCAL_PLAYER_SUM22_EVENT_REWARD(SUM22_EVENT_REWARD_COMPLETE_CAYO_PERICO_FINALE)
	ENDIF
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != LocalPlayer
		SET_H4_QUICKMATCH_COOLDOWN_TIMER()
		PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_COMPLETION -  Setting SET_H4_QUICKMATCH_COOLDOWN_TIMER")
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		END_ISLAND_HEIST_UDS_ACTIVITY_TASK(ISLAND_HEIST_UDS_ACTIVITY_TASK_HEIST, UDS_ACTIVITY_END_REASON_COMPLETED)
		END_ISLAND_HEIST_UDS_ACTIVITY(UDS_ACTIVITY_END_REASON_COMPLETED)
	ENDIF
	#ENDIF
	
ENDPROC

PROC PROCESS_ISLAND_HEIST_UNLOCK_GOLDEN_GUN()
	
	IF NOT HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		EXIT
	ENDIF
	
	PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_UNLOCK_GOLDEN_GUN - Collected")
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		UNLOCK_GOLDEN_GADGET_PISTOL_FOR_FREEMODE()
	ENDIF
	
ENDPROC

PROC PROCESS_ISLAND_HEIST_UNLOCK_SPAS12()

	IF NOT HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		EXIT
	ENDIF
	
	PRINTLN("[Rewards] - PROCESS_ISLAND_HEIST_UNLOCK_SPAS12 - Collected")
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		UNLOCK_SPAS_12_FOR_FREEMODE(TRUE)
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Telemetry
// ##### Description: Functions for tracking and setting telemetry
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_ISLAND_HEIST_TELEMETRY_COMPOUND_ENTRANCE_USED()

	IF g_FMMC_STRUCT.eContinuityContentSpecificType != FMMC_MISSION_CONTINUITY_CONTENT_SPECIFIC_TYPE_ISLAND_HEIST
		RETURN -1
	ENDIF

	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_EXPLOSIVES_FRONT_GATE)
		RETURN ciIslandHeistTelemetry_CompoundEntrances_EXPLOSIVES_FRONT_GATE
	ENDIF
		
	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_KEYCARD_SOUTH)
		RETURN ciIslandHeistTelemetry_CompoundEntrances_KEYCARD_SOUTH
	ENDIF
		
	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_KEYCARD_NORTH)
		RETURN ciIslandHeistTelemetry_CompoundEntrances_KEYCARD_NORTH
	ENDIF
		
	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_RAPPEL_SOUTH)
		RETURN ciIslandHeistTelemetry_CompoundEntrances_RAPPEL_SOUTH
	ENDIF
		
	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_RAPPEL_NORTH)
		RETURN ciIslandHeistTelemetry_CompoundEntrances_RAPPEL_NORTH
	ENDIF
		
	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_VEHICLE_FRONT_GATE)
		RETURN ciIslandHeistTelemetry_CompoundEntrances_VEHICLE_FRONT_GATE
	ENDIF
		
	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_SEWER_GRATE)
		RETURN ciIslandHeistTelemetry_CompoundEntrances_SEWER_GRATE
	ENDIF

	RETURN -1
	
ENDFUNC

FUNC INT GET_ISLAND_HEIST_TELEMETRY_COMPOUND_EXIT_USED()

	IF g_FMMC_STRUCT.eContinuityContentSpecificType != FMMC_MISSION_CONTINUITY_CONTENT_SPECIFIC_TYPE_ISLAND_HEIST
		RETURN -1
	ENDIF

	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_EXIT_FRONT_GATE)
		RETURN ciIslandHeistTelemetry_CompoundExits_FRONT_GATE
	ENDIF
		
	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_EXIT_KEYCARD_SOUTH)
		RETURN ciIslandHeistTelemetry_CompoundExits_KEYCARD_SOUTH
	ENDIF
		
	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_EXIT_KEYCARD_NORTH)
		RETURN ciIslandHeistTelemetry_CompoundExits_KEYCARD_NORTH
	ENDIF
		
	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_EXIT_RAPPEL_SOUTH)
		RETURN ciIslandHeistTelemetry_CompoundExits_RAPPEL_SOUTH
	ENDIF
		
	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_EXIT_RAPPEL_NORTH)
		RETURN ciIslandHeistTelemetry_CompoundExits_RAPPEL_NORTH
	ENDIF

	RETURN -1
	
ENDFUNC

FUNC INT GET_ISLAND_HEIST_TELEMETRY_INDEX_FROM_ABILITY_TYPE(FMMC_PLAYER_ABILITY_TYPES eType)
	
	SWITCH eType
		CASE PA_AIR_STRIKE
		RETURN ciContinuityTelemetry_SupportCrewBS_AirStrikeUsed
		CASE PA_HELI_BACKUP
		RETURN ciContinuityTelemetry_SupportCrewBS_HeliUsed
		CASE PA_HEAVY_LOADOUT
		RETURN ciContinuityTelemetry_SupportCrewBS_WeaponDropUsed
		CASE PA_RECON_DRONE
		RETURN ciContinuityTelemetry_SupportCrewBS_DroneUsed
		CASE PA_SUPPORT_SNIPER		
		RETURN ciContinuityTelemetry_SupportCrewBS_SniperUsed
	ENDSWITCH

	RETURN -1
	
ENDFUNC

PROC SET_ISLAND_HEIST_TELEMETRY_SUPPORT_CREW_USED(INT iSupportCrewUsed)
	
	IF iSupportCrewUsed = -1
		EXIT
	ENDIF
	
	IF NOT HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		EXIT
	ENDIF
	
	PRINTLN("[TEL] - SET_ISLAND_HEIST_TELEMETRY_SUPPORT_CREW_USED - Support Crew Used - ", iSupportCrewUsed)
	
	IF bIsLocalPlayerHost
		SET_BIT(MC_serverBD_1.sMissionContinuityVars.iTelemetryInts[ciContinuityTelemetryInts_IslandHeist_SupportCrewBS], iSupportCrewUsed)
	ELSE
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_IslandHeistTelemetry_SupportCrewUsed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iSupportCrewUsed, DEFAULT)
	ENDIF
	
ENDPROC

FUNC INT GET_ISLAND_HEIST_SECONDARY_LOOT_INDEX_FROM_INTERACTABLE(INT iInteractable)

	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Group_1)
		
		IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_1)
			RETURN 0
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_2)
			RETURN 1
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_3)
			RETURN 2
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_4)
			RETURN 3
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_5)
			RETURN 4
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_6)
			RETURN 5
		ENDIF
	
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Group_2)
		
		IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_1)
			RETURN 6
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_2)
			RETURN 7
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_3)
			RETURN 8
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_4)
			RETURN 9
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_5)
			RETURN 10
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_6)
			RETURN 11
		ENDIF
		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Group_3)
		
		IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_1)
			RETURN 12
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_2)
			RETURN 13
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_3)
			RETURN 14
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_4)
			RETURN 15
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_5)
			RETURN 16
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_6)
			RETURN 17
		ENDIF
		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Group_4)
		
		IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_1)
			RETURN 18
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_2)
			RETURN 19
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_3)
			RETURN 20
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_4)
			RETURN 21
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_5)
			RETURN 22
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_6)
			RETURN 23
		ENDIF
		
	ENDIF
	
	RETURN -1
	
ENDFUNC

PROC SET_ISLAND_HEIST_SECONDARY_LOOT_GRABBED(INT iLootInteractableIndex)
	
	IF iLootInteractableIndex = -1
		EXIT
	ENDIF
	
	IF NOT HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		EXIT
	ENDIF
	
	INT iLootIndex = GET_ISLAND_HEIST_SECONDARY_LOOT_INDEX_FROM_INTERACTABLE(iLootInteractableIndex)
		
	IF iLootIndex = -1
		EXIT
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iLootInteractableIndex].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_AsSecondaryLoot_Compound)
		iLootIndex += 24 //To match loot spreadsheet
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iTelemetryInts[ciContinuityTelemetryInts_IslandHeist_SecLootBS], iLootIndex)
		EXIT
	ENDIF
	
	PRINTLN("[TEL] - SET_ISLAND_HEIST_SECONDARY_LOOT_GRABBED - Loot Grabbed from index - ", iLootIndex)
	
	IF bIsLocalPlayerHost
		SET_BIT(MC_serverBD_1.sMissionContinuityVars.iTelemetryInts[ciContinuityTelemetryInts_IslandHeist_SecLootBS], iLootIndex)
	ELSE
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_IslandHeistTelemetry_SecLootGrabbed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iLootIndex, DEFAULT)
	ENDIF
	
ENDPROC

PROC SET_ISLAND_HEIST_INTEREST_ITEM_USED(INT iInterestItemUsed)
	
	IF iInterestItemUsed = -1
		EXIT
	ENDIF
	
	IF NOT HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		EXIT
	ENDIF
	
	PRINTLN("[TEL] - SET_ISLAND_HEIST_INTEREST_ITEM_USED - Interest Item Used - ", iInterestItemUsed)
	
	IF bIsLocalPlayerHost
		SET_BIT(MC_serverBD_1.sMissionContinuityVars.iTelemetryInts[ciContinuityTelemetryInts_IslandHeist_InterestItemBS], iInterestItemUsed)
	ELSE
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_IslandHeistTelemetry_InterestItemUsed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iInterestItemUsed, DEFAULT)
	ENDIF
	
ENDPROC

PROC SET_ISLAND_HEIST_ESCAPE_USED(INT iEscapeUsed)
	
	IF iEscapeUsed = -1
		EXIT
	ENDIF
	
	IF NOT HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		EXIT
	ENDIF
	
	IF sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_EscActual] != ciLocalContinuityTelemetry_EscActual_None
		EXIT
	ENDIF
	
	PRINTLN("[TEL] - SET_ISLAND_HEIST_ESCAPE_USED - Escape Used - ", iEscapeUsed)
	
	sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_EscActual] = iEscapeUsed
	
ENDPROC

PROC SET_ISLAND_HEIST_PRIMARY_TARGET_ENTRANCE_USED(INT iPrimaryTargetEntrance)
	
	IF iPrimaryTargetEntrance = -1
		EXIT
	ENDIF
	
	IF NOT HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		EXIT
	ENDIF
	
	PRINTLN("[TEL] - SET_ISLAND_HEIST_PRIMARY_TARGET_ENTRANCE_USED - Primary Target Entrance Used - ", iPrimaryTargetEntrance)
	
	sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_PrimTarEntrance] = iPrimaryTargetEntrance
	
ENDPROC

PROC SET_ISLAND_HEIST_MISC_ACTION_USED(INT iMiscAction)
	
	IF iMiscAction = -1
		EXIT
	ENDIF
	
	IF NOT HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		EXIT
	ENDIF
	
	PRINTLN("[TEL] - SET_ISLAND_HEIST_MISC_ACTION_USED - Misc Action Used - ", iMiscAction)
	
	SET_BIT(sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_MiscActionsBS], iMiscAction)
	
ENDPROC

PROC PROCESS_ISLAND_HEIST_CONTINUITY_TYPE_TELEMETRY(INT iContentContinuityType)

	IF g_FMMC_STRUCT.eContinuityContentSpecificType != FMMC_MISSION_CONTINUITY_CONTENT_SPECIFIC_TYPE_ISLAND_HEIST
		EXIT
	ENDIF
	
	SWITCH iContentContinuityType
		
		CASE ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_KEYCARD_NORTH
		CASE ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_EXIT_KEYCARD_NORTH
		CASE ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_KEYCARD_SOUTH
		CASE ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_EXIT_KEYCARD_SOUTH
			SET_ISLAND_HEIST_INTEREST_ITEM_USED(ciContinuityTelemetry_InterestItemBS_SideGateCodesUsed)
		BREAK
		
		CASE ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_RAPPEL_NORTH
		CASE ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_EXIT_RAPPEL_NORTH
		CASE ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_RAPPEL_SOUTH
		CASE ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_EXIT_RAPPEL_SOUTH
			SET_ISLAND_HEIST_INTEREST_ITEM_USED(ciContinuityTelemetry_InterestItemBS_RappelUsed)
		BREAK
		
		CASE ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_VEHICLE_FRONT_GATE
			SET_ISLAND_HEIST_INTEREST_ITEM_USED(ciContinuityTelemetry_InterestItemBS_TrojanHorseUsed)
		BREAK
		
		CASE ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_SECRET_ENTRANCE_NORTH
			SET_ISLAND_HEIST_INTEREST_ITEM_USED(ciContinuityTelemetry_InterestItemBS_JailKeysUsed)
			SET_ISLAND_HEIST_PRIMARY_TARGET_ENTRANCE_USED(ciLocalContinuityTelemetry_PrimTarEntrance_SecretEntranceNorth)
		BREAK
		
		CASE ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_SECRET_ENTRANCE_SOUTH
			SET_ISLAND_HEIST_PRIMARY_TARGET_ENTRANCE_USED(ciLocalContinuityTelemetry_PrimTarEntrance_SecretEntranceSouth)
			SET_ISLAND_HEIST_INTEREST_ITEM_USED(ciContinuityTelemetry_InterestItemBS_JailKeysUsed)
		BREAK
		
		CASE ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_OFFICE_ELEVATOR_HACK
			SET_ISLAND_HEIST_PRIMARY_TARGET_ENTRANCE_USED(ciLocalContinuityTelemetry_PrimTarEntrance_OfficeElevator)
		BREAK
		
		CASE ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_CONTROL_TOWER_HACK
			SET_ISLAND_HEIST_MISC_ACTION_USED(ciLocalContinuityTelemetry_miscActions_RadarTowerDisabled)
		BREAK
		
		CASE ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_OFFICE_WALL_SAFE
			SET_ISLAND_HEIST_MISC_ACTION_USED(ciLocalContinuityTelemetry_miscActions_WallSafeLooted)
		BREAK
		
		CASE ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ESCAPE_SUBMARINE
			SET_ISLAND_HEIST_ESCAPE_USED(ciLocalContinuityTelemetry_EscActual_Submarine)
		BREAK
		
		CASE ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ESCAPE_LEAVE_ISLAND
			SET_ISLAND_HEIST_ESCAPE_USED(ciLocalContinuityTelemetry_EscActual_LeaveIsland)
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC PROCESS_ISLAND_HEIST_CASH_GRAB_TELEMETRY(INT iLootType, INT iBlocksGrabbed, INT iValueGrabbed, INT iBlockWeight)

	PRINTLN("[TEL] - PROCESS_ISLAND_HEIST_CASH_GRAB_TELEMETRY - Loot Type - ", iLootType, ", Blocks Grabbed - ", iBlocksGrabbed, ", Value Grabbed, ", iValueGrabbed, ", Block Weight, ", iBlockWeight)
	
	SWITCH iLootType
		CASE ciLOOT_TYPE_CASH
			sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarBlocks_Cash] += iBlocksGrabbed
			sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarHeld_Cash] += iValueGrabbed
			sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarWeight_Cash] = iBlockWeight
		BREAK
		CASE ciLOOT_TYPE_COKE
			sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarBlocks_Coke] += iBlocksGrabbed
			sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarHeld_Coke] += iValueGrabbed
			sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarWeight_Coke] = iBlockWeight
		BREAK
		CASE ciLOOT_TYPE_WEED
			sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarBlocks_Weed] += iBlocksGrabbed
			sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarHeld_Weed] += iValueGrabbed
			sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarWeight_Weed] = iBlockWeight
		BREAK
		CASE ciLOOT_TYPE_GOLD
			sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarBlocks_Gold] += iBlocksGrabbed
			sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarHeld_Gold] += iValueGrabbed
			sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarWeight_Gold] = iBlockWeight
		BREAK
		CASE ciLOOT_TYPE_PAINTING
			sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarBlocks_Paint] += iBlocksGrabbed
			sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarHeld_Paint] += iValueGrabbed
			sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarWeight_Paint] = iBlockWeight
		BREAK
	ENDSWITCH

ENDPROC

FUNC FLOAT GET_ISLAND_HEIST_TELEMETRY_END_BLOCKS_HELD(INT iLootType, INT iWeight)

	RETURN MC_playerBD[iLocalPart].sLootBag.fCurrentCapacity[iLootType] / TO_FLOAT(iWeight)
	
ENDFUNC

FUNC INT GET_ISLAND_HEIST_TELEMETRY_VALUE_PER_BLOCK(INT iValueGrabbed, INT iBlocksGrabbed, FLOAT fBlocksHeld)

	FLOAT fBlockValue = iValueGrabbed / TO_FLOAT(iBlocksGrabbed)
	RETURN ROUND(fBlocksHeld * fBlockValue)
	
ENDFUNC

PROC FILL_ISLAND_HEIST_LOOT_TELEMETRY_INDEXES(INT iLootType, INT &iSecTarBlocks, INT &iSecTarWeight, INT &iSecTarHeld, INT &iSecTarEndBlocks)
	
	SWITCH iLootType
		CASE ciLOOT_TYPE_CASH
			iSecTarBlocks = ciLocalContinuityTelemetryInts_IslandHeist_SecTarBlocks_Cash
			iSecTarWeight = ciLocalContinuityTelemetryInts_IslandHeist_SecTarWeight_Cash
			iSecTarHeld = ciLocalContinuityTelemetryInts_IslandHeist_SecTarHeld_Cash
			iSecTarEndBlocks = ciLocalContinuityTelemetryInts_IslandHeist_SecTarEndBlocks_Cash
		BREAK
		CASE ciLOOT_TYPE_COKE
			iSecTarBlocks = ciLocalContinuityTelemetryInts_IslandHeist_SecTarBlocks_Coke
			iSecTarWeight = ciLocalContinuityTelemetryInts_IslandHeist_SecTarWeight_Coke
			iSecTarHeld = ciLocalContinuityTelemetryInts_IslandHeist_SecTarHeld_Coke
			iSecTarEndBlocks = ciLocalContinuityTelemetryInts_IslandHeist_SecTarEndBlocks_Coke
		BREAK
		CASE ciLOOT_TYPE_WEED
			iSecTarBlocks = ciLocalContinuityTelemetryInts_IslandHeist_SecTarBlocks_Weed
			iSecTarWeight = ciLocalContinuityTelemetryInts_IslandHeist_SecTarWeight_Weed
			iSecTarHeld = ciLocalContinuityTelemetryInts_IslandHeist_SecTarHeld_Weed
			iSecTarEndBlocks = ciLocalContinuityTelemetryInts_IslandHeist_SecTarEndBlocks_Weed
		BREAK
		CASE ciLOOT_TYPE_GOLD
			iSecTarBlocks = ciLocalContinuityTelemetryInts_IslandHeist_SecTarBlocks_Gold
			iSecTarWeight = ciLocalContinuityTelemetryInts_IslandHeist_SecTarWeight_Gold
			iSecTarHeld = ciLocalContinuityTelemetryInts_IslandHeist_SecTarHeld_Gold
			iSecTarEndBlocks = ciLocalContinuityTelemetryInts_IslandHeist_SecTarEndBlocks_Gold
		BREAK
		CASE ciLOOT_TYPE_PAINTING
			iSecTarBlocks = ciLocalContinuityTelemetryInts_IslandHeist_SecTarBlocks_Paint
			iSecTarWeight = ciLocalContinuityTelemetryInts_IslandHeist_SecTarWeight_Paint
			iSecTarHeld = ciLocalContinuityTelemetryInts_IslandHeist_SecTarHeld_Paint
			iSecTarEndBlocks = ciLocalContinuityTelemetryInts_IslandHeist_SecTarEndBlocks_Paint
		BREAK
		DEFAULT
			PRINTLN("[TEL] - PROCESS_ISLAND_HEIST_CASH_GRAB_TELEMETRY - Invalid Loot Type - ", iLootType)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_ISLAND_HEIST_END_LOOT_TELEMETRY()

	INT iSecTarBlocks, iSecTarWeight, iSecTarHeld, iSecTarEndBlocks
	INT i
	FOR i = 0 TO ciLOOT_TYPE_MAX - 1
	
		FILL_ISLAND_HEIST_LOOT_TELEMETRY_INDEXES(i, iSecTarBlocks, iSecTarWeight, iSecTarHeld, iSecTarEndBlocks)
	
		IF sMissionLocalContinuityVars.iLocalTelemetryInts[iSecTarBlocks] > 1
		AND sMissionLocalContinuityVars.iLocalTelemetryInts[iSecTarWeight] > 0
			
			FLOAT fBlocksHeld = GET_ISLAND_HEIST_TELEMETRY_END_BLOCKS_HELD(i, sMissionLocalContinuityVars.iLocalTelemetryInts[iSecTarWeight])
			
			INT iEndValue = GET_ISLAND_HEIST_TELEMETRY_VALUE_PER_BLOCK(sMissionLocalContinuityVars.iLocalTelemetryInts[iSecTarHeld], sMissionLocalContinuityVars.iLocalTelemetryInts[iSecTarBlocks], fBlocksHeld)
						
			sMissionLocalContinuityVars.iLocalTelemetryInts[iSecTarEndBlocks] = FLOOR(fBlocksHeld)
			sMissionLocalContinuityVars.iLocalTelemetryInts[iSecTarHeld] = iEndValue
			
		ENDIF
		
	ENDFOR
	
ENDPROC

PROC PROCESS_FAIL_REASON_ISLAND_HEIST_TELEMETRY(eFailMissionEnum eFailReason)
	
	g_sIsland_Heist_Finale_Telemetry_data.endingReason = ENUM_TO_INT(eFailReason)
	PRINTLN("[TEL] - PROCESS_FAIL_REASON_ISLAND_HEIST_TELEMETRY - endingReason - ", g_sIsland_Heist_Finale_Telemetry_data.endingReason)
	
ENDPROC

PROC PROCESS_START_OF_MISSION_ISLAND_HEIST_TELEMETRY()

	sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_PrimTarEntrance] = -1
	
	g_sIsland_Heist_Finale_Telemetry_data.missionId = g_FMMC_STRUCT.iRootContentIDHash
	PRINTLN("[TEL] - PROCESS_START_OF_MISSION_ISLAND_HEIST_TELEMETRY - missionId ", g_sIsland_Heist_Finale_Telemetry_data.missionId)
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
		g_sIsland_Heist_Finale_Telemetry_data.bossId1 = GB_GET_GANG_BOSS_UUID_INT_A(GB_GET_LOCAL_PLAYER_GANG_BOSS())
		g_sIsland_Heist_Finale_Telemetry_data.bossId2 = GB_GET_GANG_BOSS_UUID_INT_B(GB_GET_LOCAL_PLAYER_GANG_BOSS())
		PRINTLN("[TEL] - PROCESS_START_OF_MISSION_ISLAND_HEIST_TELEMETRY - bossId1 ", g_sIsland_Heist_Finale_Telemetry_data.bossId1)
		PRINTLN("[TEL] - PROCESS_START_OF_MISSION_ISLAND_HEIST_TELEMETRY - bossId2 ", g_sIsland_Heist_Finale_Telemetry_data.bossId2)
	ENDIF
	
	g_sIsland_Heist_Finale_Telemetry_data.playerRole = GET_PLAYER_ROLE_FOR_TELEMETRY()
	PRINTLN("[TEL] - PROCESS_START_OF_MISSION_ISLAND_HEIST_TELEMETRY - playerRole ", g_sIsland_Heist_Finale_Telemetry_data.playerRole)

	g_sIsland_Heist_Finale_Telemetry_data.bosstype = iBossType
	PRINTLN("[TEL] - PROCESS_START_OF_MISSION_ISLAND_HEIST_TELEMETRY - bosstype ", g_sIsland_Heist_Finale_Telemetry_data.bosstype)

ENDPROC

PROC PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY(BOOL bPassed, INT iTotalMissionTime)
	
	g_sIsland_Heist_Finale_Telemetry_data.timeTakenToComplete = iTotalMissionTime
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - timeTakenToComplete ", g_sIsland_Heist_Finale_Telemetry_data.timeTakenToComplete)
	
	g_sIsland_Heist_Finale_Telemetry_data.deaths = MC_Playerbd[iLocalPart].iNumPlayerDeaths
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - deaths ", g_sIsland_Heist_Finale_Telemetry_data.deaths)
	
	g_sIsland_Heist_Finale_Telemetry_data.targetsKilled = MC_Playerbd[iLocalPart].iNumPedKills + MC_Playerbd[iLocalPart].iAmbientCopsKilled
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - targetsKilled ", g_sIsland_Heist_Finale_Telemetry_data.targetsKilled)
	
	g_sIsland_Heist_Finale_Telemetry_data.innocentsKilled = MC_playerBD[iLocalPart].iCivilianKills
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - innocentsKilled ", g_sIsland_Heist_Finale_Telemetry_data.innocentsKilled)
	
	g_sIsland_Heist_Finale_Telemetry_data.weather = MC_serverBD.iWeather
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - weather ", g_sIsland_Heist_Finale_Telemetry_data.weather)
	
	g_sIsland_Heist_Finale_Telemetry_data.supCrewActual = MC_serverBD_1.sMissionContinuityVars.iTelemetryInts[ciContinuityTelemetryInts_IslandHeist_SupportCrewBS]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - supCrewActual ", g_sIsland_Heist_Finale_Telemetry_data.supCrewActual)
	
	g_sIsland_Heist_Finale_Telemetry_data.miscActions = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_MiscActionsBS]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - miscActions ", g_sIsland_Heist_Finale_Telemetry_data.miscActions)
	
	g_sIsland_Heist_Finale_Telemetry_data.primTarEntrance = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_PrimTarEntrance]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - primTarEntrance ", g_sIsland_Heist_Finale_Telemetry_data.primTarEntrance)
	
	g_sIsland_Heist_Finale_Telemetry_data.islEscActual = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_EscActual]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - islEscActual ", g_sIsland_Heist_Finale_Telemetry_data.islEscActual)
	
	g_sIsland_Heist_Finale_Telemetry_data.compEntrance = GET_ISLAND_HEIST_TELEMETRY_COMPOUND_ENTRANCE_USED()
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - compEntrance ", g_sIsland_Heist_Finale_Telemetry_data.compEntrance)
	
	g_sIsland_Heist_Finale_Telemetry_data.compExit = GET_ISLAND_HEIST_TELEMETRY_COMPOUND_EXIT_USED()
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - compExit ", g_sIsland_Heist_Finale_Telemetry_data.compExit)
	
	g_sIsland_Heist_Finale_Telemetry_data.interestItemUsed = MC_serverBD_1.sMissionContinuityVars.iTelemetryInts[ciContinuityTelemetryInts_IslandHeist_InterestItemBS]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - interestItemUsed ", g_sIsland_Heist_Finale_Telemetry_data.interestItemUsed)
	
	g_sIsland_Heist_Finale_Telemetry_data.failedStealth = HAS_ANY_TEAM_TRIGGERED_AGGRO(ciMISSION_AGGRO_INDEX_ALL)
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - failedStealth ", g_sIsland_Heist_Finale_Telemetry_data.failedStealth)
	
	g_sIsland_Heist_Finale_Telemetry_data.specCam = IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_EARLY_END_SPECTATOR)
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - specCam ", g_sIsland_Heist_Finale_Telemetry_data.specCam)
	
	g_sIsland_Heist_Finale_Telemetry_data.moneyEarned = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iActualTake
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - moneyEarned ", g_sIsland_Heist_Finale_Telemetry_data.moneyEarned)
	
	g_sIsland_Heist_Finale_Telemetry_data.secLoot = MC_serverBD_1.sMissionContinuityVars.iTelemetryInts[ciContinuityTelemetryInts_IslandHeist_SecLootBS]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - secLoot ", g_sIsland_Heist_Finale_Telemetry_data.secLoot)
	
	g_sIsland_Heist_Finale_Telemetry_data.bagCapacity = BAG_CAPACITY__GET_MAX_CAPACITY()
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - bagCapacity ", g_sIsland_Heist_Finale_Telemetry_data.bagCapacity)
	
	g_sIsland_Heist_Finale_Telemetry_data.playCount = GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER()
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - playCount ", g_sIsland_Heist_Finale_Telemetry_data.playCount)
	
	PROCESS_ISLAND_HEIST_END_LOOT_TELEMETRY()
	
	g_sIsland_Heist_Finale_Telemetry_data.CashBlocksStolen = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarBlocks_Cash]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - CashBlocksStolen ", g_sIsland_Heist_Finale_Telemetry_data.CashBlocksStolen)
	
	g_sIsland_Heist_Finale_Telemetry_data.CashValueFinal = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarHeld_Cash]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - CashValueFinal ", g_sIsland_Heist_Finale_Telemetry_data.CashValueFinal)
	
	g_sIsland_Heist_Finale_Telemetry_data.CashBlocksFinal = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarEndBlocks_Cash]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - CashBlocksFinal ", g_sIsland_Heist_Finale_Telemetry_data.CashBlocksFinal)
	
	g_sIsland_Heist_Finale_Telemetry_data.CashBlocksSpace = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarWeight_Cash]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - CashBlocksSpace ", g_sIsland_Heist_Finale_Telemetry_data.CashBlocksSpace)
	
	g_sIsland_Heist_Finale_Telemetry_data.CocaBlocksStolen = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarBlocks_Coke]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - CocaBlocksStolen ", g_sIsland_Heist_Finale_Telemetry_data.CocaBlocksStolen)
	
	g_sIsland_Heist_Finale_Telemetry_data.CocaValueFinal = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarHeld_Coke]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - CocaValueFinal ", g_sIsland_Heist_Finale_Telemetry_data.CocaValueFinal)
	
	g_sIsland_Heist_Finale_Telemetry_data.CocaBlocksFinal = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarEndBlocks_Coke]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - CocaBlocksFinal ", g_sIsland_Heist_Finale_Telemetry_data.CocaBlocksFinal)
	
	g_sIsland_Heist_Finale_Telemetry_data.CocaBlocksSpace = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarWeight_Coke]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - CocaBlocksSpace ", g_sIsland_Heist_Finale_Telemetry_data.CocaBlocksSpace)
	
	g_sIsland_Heist_Finale_Telemetry_data.WeedBlocksStolen = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarBlocks_Weed]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - WeedBlocksStolen ", g_sIsland_Heist_Finale_Telemetry_data.WeedBlocksStolen)
	
	g_sIsland_Heist_Finale_Telemetry_data.WeedValueFinal = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarHeld_Weed]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - WeedValueFinal ", g_sIsland_Heist_Finale_Telemetry_data.WeedValueFinal)
	
	g_sIsland_Heist_Finale_Telemetry_data.WeedBlocksFinal = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarEndBlocks_Weed]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - WeedBlocksFinal ", g_sIsland_Heist_Finale_Telemetry_data.WeedBlocksFinal)
	
	g_sIsland_Heist_Finale_Telemetry_data.WeedBlocksSpace = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarWeight_Weed]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - WeedBlocksSpace ", g_sIsland_Heist_Finale_Telemetry_data.WeedBlocksSpace)
	
	g_sIsland_Heist_Finale_Telemetry_data.GoldBlocksStolen = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarBlocks_Gold]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - GoldBlocksStolen ", g_sIsland_Heist_Finale_Telemetry_data.GoldBlocksStolen)
	
	g_sIsland_Heist_Finale_Telemetry_data.GoldValueFinal = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarHeld_Gold]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - GoldValueFinal ", g_sIsland_Heist_Finale_Telemetry_data.GoldValueFinal)
	
	g_sIsland_Heist_Finale_Telemetry_data.GoldBlocksFinal = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarEndBlocks_Gold]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - GoldBlocksFinal ", g_sIsland_Heist_Finale_Telemetry_data.GoldBlocksFinal)
	
	g_sIsland_Heist_Finale_Telemetry_data.GoldBlocksSpace = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarWeight_Gold]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - GoldBlocksSpace ", g_sIsland_Heist_Finale_Telemetry_data.GoldBlocksSpace)
	
	g_sIsland_Heist_Finale_Telemetry_data.PaintBlocksStolen = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarBlocks_Paint]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - PaintBlocksStolen ", g_sIsland_Heist_Finale_Telemetry_data.PaintBlocksStolen)
	
	g_sIsland_Heist_Finale_Telemetry_data.PaintValueFinal = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarHeld_Paint]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - PaintValueFinal ", g_sIsland_Heist_Finale_Telemetry_data.PaintValueFinal)
	
	g_sIsland_Heist_Finale_Telemetry_data.PaintBlocksFinal = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarEndBlocks_Paint]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - PaintBlocksFinal ", g_sIsland_Heist_Finale_Telemetry_data.PaintBlocksFinal)
	
	g_sIsland_Heist_Finale_Telemetry_data.PaintBlocksSpace = sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_SecTarWeight_Paint]
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - PaintBlocksSpace ", g_sIsland_Heist_Finale_Telemetry_data.PaintBlocksSpace)

	IF bPassed
		g_sIsland_Heist_Finale_Telemetry_data.checkpoint = 0
		PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - checkpoint ", g_sIsland_Heist_Finale_Telemetry_data.checkpoint)
		
		g_sIsland_Heist_Finale_Telemetry_data.endingReason = 0
		PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - endingReason ", g_sIsland_Heist_Finale_Telemetry_data.endingReason)
	ELSE
		g_sIsland_Heist_Finale_Telemetry_data.checkpoint = MC_serverBD_1.sMissionContinuityVars.iStrandMissionsComplete
		PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY - checkpoint ", g_sIsland_Heist_Finale_Telemetry_data.checkpoint)
	ENDIF
		
ENDPROC
