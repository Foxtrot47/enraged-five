// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Celebrations ------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script Name: FM_Mission_Controller_2020.sc ------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Runs the end of mission celebration screens.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Ending_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Shared Functionality
// ##### Description: Functions used by both the internal and external celebrartion screens.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_MISSION_USE_EXTERNAL_CELEBRATION_SCREEN()

	RETURN g_FMMC_STRUCT.iCelebrationType = FMMC_CELEBRATION_TYPE_ISLAND_HEIST

ENDFUNC

FUNC BOOL DOES_MISSION_HAVE_AN_END_WINNER_SCENE()

	RETURN g_FMMC_STRUCT.iCelebrationType = FMMC_CELEBRATION_TYPE_ISLAND_HEIST

ENDFUNC

FUNC BOOL SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_SkipCelebrationPreloadFX)
		PRINTLN("[Celebrations] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - returning FALSE (Set to skip)")
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_IS_A_STRAND_MISSION()
		PRINTLN("[Celebrations] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - IS_THIS_IS_A_STRAND_MISSION = TRUE.")
		IF NOT IS_THIS_THE_LAST_IN_A_SERIES_OF_STRAND_MISSIONS()
			PRINTLN("[Celebrations] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - IS_THIS_THE_LAST_IN_A_SERIES_OF_STRAND_MISSIONS = FALSE.")
			IF IS_CUTSCENE_ACTIVE()
			AND MC_serverBD_4.iCurrentHighestPriority[ MC_playerBD[iPartToUse].iTeam ] >= MC_serverBD.iMaxObjectives[ MC_playerBD[iPartToUse].iTeam ]
				PRINTLN("[Celebrations] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - IS_CUTSCENE_ACTIVE = TRUE.")
				PRINTLN("[Celebrations] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - returning FALSE")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_serverBD.iNextMission < FMMC_MAX_STRAND_MISSIONS
	AND MC_serverBD.iNextMission >= 0
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23NextContentID[MC_serverBD.iNextMission])
		//Strand mission is going to be initialised soon
		IF g_FMMC_STRUCT.eBranchingTransitionType[MC_serverBD.iNextMission] != FMMC_BRANCHING_MISSION_TRANSITION_TYPE_MOCAP
			PRINTLN("[Celebrations] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - returning FALSE (Airlock or Fade Out strand transition")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF MC_ServerBD.iEndCutscene > 0
	AND IS_THIS_IS_A_STRAND_MISSION()	
		PRINTLN("[Celebrations] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - MC_ServerBD.iEndCutscene is set to: ", MC_ServerBD.iEndCutscene)
		RETURN FALSE
	ENDIF
				
	IF IS_CUTSCENE_PLAYING()
		PRINTLN("[Celebrations] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - IS_CUTSCENE_PLAYING() = TRUE - returning FALSE")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
		PRINTLN("[Celebrations] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - ciCELEBRATION_BIT_SET_TriggerEndCelebration is set - returning FALSE")
		RETURN FALSE
	ENDIF
	
	IF g_bCelebrationScreenIsActive
		PRINTLN("[Celebrations] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - g_bCelebrationScreenIsActive = TRUE - returning FALSE")
		RETURN FALSE
	ENDIF
	
	IF (SHOULD_MISSION_USE_EXTERNAL_CELEBRATION_SCREEN() AND HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iTeam))
		PRINTLN("[Celebrations] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - Using External Celebration - returning FALSE")
		RETURN FALSE
	ENDIF
	
	PRINTLN("[Celebrations] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - returning TRUE")
	RETURN TRUE
	
ENDFUNC

PROC MAINTAIN_CELEB_PRE_LOAD_FX(BOOL &bShownPreloadFX, SCRIPT_TIMER timerCelebPreLoadPostFxBeenPlaying)
	
	IF !g_bEndOfMissionCleanUpHUD
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PLAYED_CELEB_PRE_LOAD_FOR_HUD_CLEAR)
		EXIT
	ENDIF
	
	SET_BIT(iLocalBoolCheck10, LBOOL10_PLAYED_CELEB_PRE_LOAD_FOR_HUD_CLEAR)
	
	IF SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED()
	AND bLocalPlayerOK	
		PLAY_MP_CELEB_PRELOAD_POST_FX(bShownPreloadFX,timerCelebPreLoadPostFxBeenPlaying)
	ENDIF

ENDPROC

FUNC BOOL HAS_CELEBRATION_SCREEN_SKIP_FINISHED()
	
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		PRINTLN("[Celebration] - HAS_CELEBRATION_SCREEN_SKIP_FINISHED - Strand Mission - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		PRINTLN("[Celebration] - HAS_CELEBRATION_SCREEN_SKIP_FINISHED - Test Mode - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_SCREEN_FADED_IN()
		IF SET_SKYSWOOP_UP(TRUE, TRUE, FALSE, DEFAULT, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
			PRINTLN("[Celebration] - HAS_CELEBRATION_SCREEN_SKIP_FINISHED - Screen Faded In and Swoop Up - TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_SCREEN_FADED_IN()
		IF IS_SKYSWOOP_IN_SKY()
			PRINTLN("[Celebration] - HAS_CELEBRATION_SCREEN_SKIP_FINISHED - Screen Faded In and Swoop In Sky - TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN(BOOL bPlayResultPostFx = TRUE, BOOL bDoRespawningDuringFailCheck = TRUE)

	IF IS_SCREEN_FADING_OUT()
		PRINTLN("[Celebration] - DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN - IS_SCREEN_FADING_OUT - Exit.")
		EXIT
	ENDIF
	
	IF bDoRespawningDuringFailCheck
		IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)
			PRINTLN("[Celebration] - DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN - LBOOL10_RESPAWNING_DURING_FAIL - Exit.")
			EXIT
		ENDIF
	ENDIF
	
	IF MC_serverBD.iNextMission < FMMC_MAX_STRAND_MISSIONS
	AND MC_serverBD.iNextMission >= 0
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23NextContentID[MC_serverBD.iNextMission])
		//Strand mission is going to be initialised soon
		IF g_FMMC_STRUCT.eBranchingTransitionType[MC_serverBD.iNextMission] != FMMC_BRANCHING_MISSION_TRANSITION_TYPE_MOCAP
			PRINTLN("[Celebration] - DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN - Airlock or Fade Out strand transition - Exit.")
			EXIT
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggeredRenderPhasePause)
		EXIT
	ENDIF
	
	PRINTLN("[Celebration] - DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN - Doing Renderphase Pause!")
	
	SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(TRUE)
	
	IF bPlayResultPostFx
		IF SHOULD_POSTFX_BE_WINNER_VERSION()
			PLAY_CELEB_WIN_POST_FX()
		ELSE
			PLAY_CELEB_LOSE_POST_FX()
		ENDIF
	ENDIF
	
	START_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
	
	TURN_ON_ARTIFICIAL_LIGHTS_LOCALLY(FALSE)

	SET_NIGHTVISION(FALSE)
	
	THEFEED_PAUSE()
	
	SET_SKYFREEZE_STAGE(SKYFREEZE_FROZEN)
	
	TOGGLE_PAUSED_RENDERPHASES(FALSE)
	
	SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggeredRenderPhasePause)
							
ENDPROC

PROC TRIGGER_END_HEIST_WINNER_CELEBRATION()
	
	PRINTLN("[Celebration] - TRIGGER_END_HEIST_WINNER_CELEBRATION - Triggering Celebration for Heist")
		
	PLAY_HEIST_PASS_FX("HeistCelebPass")
	TOGGLE_RENDERPHASES(FALSE, TRUE)
	
	TOGGLE_PLAYER_DAMAGE_OVERLAY(FALSE)
	REMOVE_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_NIGHTVISION)
	
	IF GET_REQUESTINGNIGHTVISION()
	OR GET_USINGNIGHTVISION()
		ENABLE_NIGHTVISION(VISUALAID_OFF)
		DISABLE_NIGHT_VISION_CONTROLLER(TRUE)
	ENDIF
	
	SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggeredCelebrationStreamingRequests)
	SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene)
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bGangOpsTransition = TRUE
	
	g_bCelebrationScreenIsActive = TRUE	
	
ENDPROC

PROC START_CELEBRATION_SCREEN()
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		EXIT
	ENDIF
	
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		EXIT
	ENDIF

	IF NOT IS_CUTSCENE_PLAYING()
		EXIT
	ENDIF
			
	CELEBRATION_SCREEN_TYPE eCelebType = CELEBRATION_STANDARD
	IF SHOULD_MISSION_USE_EXTERNAL_CELEBRATION_SCREEN()
		eCelebType = CELEBRATION_HEIST
	ENDIF
	
	PRINTLN("[Celebration] - START_CELEBRATION_SCREEN - Type: ", ENUM_TO_INT(eCelebType))
	
	REQUEST_CELEBRATION_SCREEN(sCelebrationData, eCelebType)
		
	SWITCH eCelebType
		
		CASE CELEBRATION_HEIST
			IF GET_CUTSCENE_END_TIME() - GET_CUTSCENE_TIME() < 450
				IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene)
					TRIGGER_END_HEIST_WINNER_CELEBRATION()
				ENDIF
			ENDIF
		BREAK
		
		CASE CELEBRATION_STANDARD
		FALLTHRU //Use Default
		
		DEFAULT
			IF GET_CUTSCENE_END_TIME() - GET_CUTSCENE_TIME() < 450
				SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bStartedCelebrationBeforeCutsceneEnded)
				IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
					DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN()
					SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
					PRINTLN("[Celebration] - ciCELEBRATION_BIT_SET_TriggerEndCelebration - call 1.")
				ENDIF
			ENDIF
		BREAK	
		
	ENDSWITCH
	
ENDPROC

FUNC BOOL SHOULD_PLAYER_BE_ADDED_TO_POST_SCENE_HASH_LIST(INT iPart)
	
	IF MC_playerBD[iPartToUse].iTeam = MC_playerBD[iPart].iTeam
		//Same Team
		RETURN TRUE
	ENDIF
	
	IF GET_POST_MISSION_CUTSCENE_ID_FROM_MISSION_DATA(MC_playerBD[iPartToUse].iTeam) = GET_POST_MISSION_CUTSCENE_ID_FROM_MISSION_DATA(MC_playerBD[iPart].iTeam)
		//Same Scene
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC RESET_CELEBRATION_MEDAL_DATA()
	INT i
	TEXT_LABEL_63 tl63Null
	FOR i = 0 TO ci_MEDALRANK_MAX - 1
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.strLeaderBoardPlayerName[i] = tl63Null
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.eRatingColour[i] = HUD_COLOUR_WHITE
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.strRatingColour[i] = tl63Null
	ENDFOR
ENDPROC

PROC COPY_CELEBRATION_DATA_TO_GLOBALS()
		
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - Network game not in progress - Early Exit.")
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_COMPLETED_POPULATION_OF_CELEB_GLOBALS)
		//Already copied
		EXIT
	ENDIF
	
	PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - Strand: ", BOOL_TO_STRING(IS_A_STRAND_MISSION_BEING_INITIALISED()), " Quick Restart: ", BOOL_TO_STRING(IS_THIS_A_QUICK_RESTART_JOB() ))
	
	IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer)
		//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].bJoinedMissionAsSpectator = TRUE
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bJoinedMissionAsSpectator = TRUE
		PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - bJoinedMissionAsSpectator = TRUE")
	ENDIF
	
	INT iPart = 0
	PARTICIPANT_INDEX partId
	PLAYER_INDEX playerId
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		INT iHashCount
		
		RESET_CELEBRATION_MEDAL_DATA()
		   
		FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
		
			partId = INT_TO_NATIVE(PARTICIPANT_INDEX, iPart)
			IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(partId)
				RELOOP
			ENDIF
			
			playerId = NETWORK_GET_PLAYER_INDEX(partId)
			IF NOT IS_NET_PLAYER_OK(playerId, FALSE)
				RELOOP
			ENDIF
			
			IF IS_PLAYER_SCTV(playerId)
			OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerId)
				RELOOP
			ENDIF
			
			INT iRank = GET_MEDAL_RANKING_FOR_PARTICIPANT(iPart)
			IF iRank >= ci_MEDALRANK_BAD
				RELOOP
			ENDIF
			
			PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - Participant: ", iPart, " is medal rank: ", iRank)
			
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.strLeaderBoardPlayerName[iRank] = MC_serverBD_2.tParticipantNames[iPart]
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.eRatingColour[iRank] = GET_MEDAL_RANKING_HUD_COLOUR(iPart)
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.strRatingColour[iRank] = GET_MEDAL_RANKING_STRING_FROM_COLOUR(g_TransitionSessionNonResetVars.sGlobalCelebrationData.eRatingColour[iRank])
		
			IF SHOULD_PLAYER_BE_ADDED_TO_POST_SCENE_HASH_LIST(iPart)
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash[iHashCount] = NETWORK_HASH_FROM_PLAYER_HANDLE(playerId)
				PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - Participant: ", iPart, " name hash (",iHashCount,"): ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash[iHashCount])
				iHashCount++
			ENDIF
			
		ENDFOR
	ENDIF
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = GET_POST_MISSION_CUTSCENE_ID_FROM_MISSION_DATA(MC_playerBD[iPartToUse].iTeam)
		
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iWinnerSceneType = g_FMMC_STRUCT.iCelebrationType
		
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_POST_MISSION_SPAWN_ON_FAIL)
	AND NOT HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iTeam)
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos = << 0.0, 0.0, 0.0 >> 
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.fPostMissionPosSearchRadius = 0.0
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPointOfInterest = << 0.0, 0.0, 0.0 >> 
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPostMissionPosIgnoreExclusionZones = FALSE
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.bForcePostMissionWarpCreatorPosition = FALSE
		PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - resetting vPostMissionPos. I failed the mission.")
	ENDIF
		
	IF SHOULD_MISSION_USE_EXTERNAL_CELEBRATION_SCREEN()
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHeistType = ciCELEBRATION_HEIST_TYPE__FINALE
	ELSE
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHeistType = ciCELEBRATION_HEIST_TYPE__NONE
	ENDIF

	IF NOT bIsSCTV
	AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].bJoinedMissionAsSpectator
		INT iCurrentRP = GET_PLAYER_XP(PlayerToUse) - sCelebrationStats.iLocalPlayerXP
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRpGained = GET_MAX_XP_GAIN_FOR_CELEBRATION_SCREEN(sCelebrationStats.iLocalPlayerXP)		
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRpStarted = iCurrentRP
	 	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevel = GET_FM_RANK_FROM_XP_VALUE(iCurrentRP)
	 	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevelStartPoints = GET_XP_NEEDED_FOR_FM_RANK(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevel)
	 	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNextLevel = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevel + 1
	 	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevelEndPoints = GET_XP_NEEDED_FOR_FM_RANK(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNextLevel)
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake = iCashGainedForCelebration
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumJobPoints = sCelebrationStats.iLocalPlayerJobPoints
	ENDIF
		
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission = HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iTeam)
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bIAmHeistLeader = (IS_THIS_A_GANG_MISSION() AND AM_I_FMMC_LOBBY_LEADER())
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReason = sCelebrationStats.strFailReason
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReasonLiteral = sCelebrationStats.strFailLiteral
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReasonLiteral2 = sCelebrationStats.strFailLiteral2
		
	IF IS_THIS_IS_A_STRAND_MISSION()
	AND NOT IS_STRING_NULL_OR_EMPTY(g_sTransitionSessionData.sStrandMissionData.tl63FirstMissionName)
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.tl63_missionName = g_sTransitionSessionData.sStrandMissionData.tl63FirstMissionName
	ELSE
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.tl63_missionName = g_FMMC_STRUCT.tl63MissionName
	ENDIF
		
	PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - iPostMissionSceneId: ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId)
	PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - iWinnerSceneType: ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iWinnerSceneType)
	PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - iHeistType: ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHeistType)
	PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - iRpGained: ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRpGained)
	PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - iRpStarted: ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRpStarted)
	PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - iCurrentLevelStartPoints: ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevelStartPoints)
	PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - iCurrentLevelEndPoints: ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevelEndPoints)
	PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - iCurrentLevel: ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevel)
	PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - iNextLevel: ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNextLevel)
	PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - iMyTake: ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake)
	PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - iNumJobPoints: ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumJobPoints)
	PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - bPassedMission: ", BOOL_TO_STRING(g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission))
	PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - bIAmHeistLeader: ", BOOL_TO_STRING(g_TransitionSessionNonResetVars.sGlobalCelebrationData.bIAmHeistLeader))
	PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - strFailReason: ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReason)
	PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - strFailReasonLiteral: ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReasonLiteral)
	PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - strFailReasonLiteral2: ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReasonLiteral2)
	PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - tl63_missionName: ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.tl63_missionName)	
		
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bFirstUpdate = FALSE
	SET_CELEBRATION_GLOBALS_FIRST_UPDATE(FALSE)
		
	//Send events to populate data on player's spectators
	FOR iPart = 0 TO NUM_NETWORK_PLAYERS - 1
		
		partId = INT_TO_NATIVE(PARTICIPANT_INDEX, iPart)
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(partId)
			RELOOP
		ENDIF
		
		playerId = NETWORK_GET_PLAYER_INDEX(partId)
		IF NOT IS_NET_PLAYER_OK(playerId, FALSE)
			RELOOP
		ENDIF
		
		IF GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].playerSCTVTarget = LocalPlayer
			PRINTLN("[Celebration] - COPY_CELEBRATION_DATA_TO_GLOBALS - Sending globals to part: ", iPart)
			BROADCAST_SYNC_SPECTATOR_CELEBRATION_GLOBALS_1(SPECIFIC_PLAYER(playerId))
			BROADCAST_SYNC_SPECTATOR_CELEBRATION_GLOBALS_2(SPECIFIC_PLAYER(playerId))
		ENDIF
		
	ENDFOR
		
	SET_BIT(MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_COMPLETED_POPULATION_OF_CELEB_GLOBALS)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Internal Celebration Screen
// ##### Description: Functions for running the celebration screen from within the Mission Controller.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC FREEZE_SCREEN_FOR_PLACED_CAMERA()
	
	BOOL bMoveOntoNextStage
	
	SWITCH iFreezeCamForPlaceDcamStage
		
		CASE 0
			IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_NOT_SAFE_FOR_ALTERNATE_CAM)
				PRINTLN("[Celebrations_Internal] - FREEZE_SCREEN_FOR_PLACED_CAMERA - LBOOL13_NOT_SAFE_FOR_ALTERNATE_CAM.")
				bMoveOntoNextStage = TRUE
			ENDIF
			IF DOES_CAM_EXIST(camEndScreen)
				PRINTLN("[Celebrations_Internal] - FREEZE_SCREEN_FOR_PLACED_CAMERA - camEndScreen exists.")
				IF bCreatedCustomCelebrationCam
					PRINTLN("[Celebrations_Internal] - FREEZE_SCREEN_FOR_PLACED_CAMERA - bCreatedCustomCelebrationCam = TRUE.")
					IF IS_CAM_ACTIVE(camEndScreen)
						PRINTLN("[Celebrations_Internal] - FREEZE_SCREEN_FOR_PLACED_CAMERA - camEndScreen is active.")
						IF IS_CAM_RENDERING(camEndScreen)
							PRINTLN("[Celebrations_Internal] - FREEZE_SCREEN_FOR_PLACED_CAMERA - is rendering.")
							bMoveOntoNextStage = TRUE
						ELSE
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							PRINTLN("[Celebrations_Internal] - FREEZE_SCREEN_FOR_PLACED_CAMERA - RENDER_SCRIPT_CAMS")
						ENDIF
					ELSE
						SET_CAM_ACTIVE(camEndScreen, TRUE)													
						PRINTLN("[Celebrations_Internal] - FREEZE_SCREEN_FOR_PLACED_CAMERA - SET_CAM_ACTIVE")
					ENDIF
				ENDIF
			ENDIF
			IF bMoveOntoNextStage
				PRINTLN("[Celebrations_Internal] - FREEZE_SCREEN_FOR_PLACED_CAMERA - bMoveOntoNextStage = TRUE.")
				IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)
					iFreezeCamForPlaceDcamStage++
				ELSE
					iFreezeCamForPlaceDcamStage = 2
				ENDIF
				PRINTLN("[Celebrations_Internal] - FREEZE_SCREEN_FOR_PLACED_CAMERA - set iFreezeCamForPlaceDcamStage = ", iFreezeCamForPlaceDcamStage)
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_SCREEN_FADED_IN()
				IF NOT IS_PLAYER_RESPAWNING(LocalPlayer)
					IF NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(500) 
					ENDIF
				ELSE
					PRINTLN("[Celebrations_Internal] - FREEZE_SCREEN_FOR_PLACED_CAMERA - We are still respawning! Do not fade in!")
				ENDIF
			ELSE
				iFreezeCamForPlaceDcamStage++
				PRINTLN("[Celebrations_Internal] - FREEZE_SCREEN_FOR_PLACED_CAMERA - set iFreezeCamForPlaceDcamStage = ", iFreezeCamForPlaceDcamStage)
			ENDIF
		BREAK
		
		CASE 2
			DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN(TRUE, FALSE)			
			SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_FrozenRenderPhasesForFail)
			PRINTLN("[Celebrations_Internal] - FREEZE_SCREEN_FOR_PLACED_CAMERA - frozen screen on cut to placed camera. Set bit ciCELEBRATION_BIT_SET_FrozenRenderPhasesForFail.")
			iFreezeCamForPlaceDcamStage++
			PRINTLN("[Celebrations_Internal] - FREEZE_SCREEN_FOR_PLACED_CAMERA - set iFreezeCamForPlaceDcamStage = ", iFreezeCamForPlaceDcamStage)
		BREAK
		
		CASE 3
			// Done. 
		BREAK
		
	ENDSWITCH
	
ENDPROC

FUNC BOOL SHOULD_RESURRECT_PLAYER_AT_END()

	IF IS_THIS_A_VERSUS_MISSION()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_CELEBRATION_SCREEN_FINISHED()
	
	#IF IS_DEBUG_BUILD
	IF g_SkipCelebAndLbd
		SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationSummaryComplete)
		SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationUnfrozeSkycam)
		SET_BIT(iLocalBoolCheck3,LBOOL3_SHOWN_RESULTS)
		SET_BIT(iLocalBoolCheck2,LBOOL2_LB_CAM_READY)
		PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - SKIPPING DUE TO g_SkipCelebAndLbd")
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(iTimeStampForFirstRenderphasePause)
	OR NOT MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(iTimeStampForFirstRenderphasePause, 500)		
		
		IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_TRIGGERED_CELEBRATION_SCREEN_RENDERPHASE_PAUSE)
			DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN()
			SET_BIT(iLocalBoolCheck33, LBOOL33_TRIGGERED_CELEBRATION_SCREEN_RENDERPHASE_PAUSE)
		ENDIF
		
		// A timer is used here because if additional renderphase pauses are called a few frames after the first then for some reason the camera pos/rotation can update if the player is moved with SET_ENTITY_COORDS. Situations like resurrection and PMC early placement can cause this bug.
		PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - returning false. Waiting for Renderphase Pause Timer: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(iTimeStampForFirstRenderphasePause), ", / 500.")
		MC_START_NET_TIMER_WITH_TIMESTAMP(iTimeStampForFirstRenderphasePause) 
		
		RETURN FALSE
	ENDIF
	
	IF bLocalPlayerPedOK
	AND NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_ALPHA_RESET_DONE)
		IF GET_ENTITY_ALPHA(localPlayerPed) < 255
		OR NOT IS_ENTITY_VISIBLE(LocalPlayerPed)		
			PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - Clearing SPAWN_PROTECTION_ALPHA. Current alpha is: ", GET_ENTITY_ALPHA(localPlayerPed))
			
			IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
				VEHICLE_INDEX vehPlayerIsIn = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)	
				IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(vehPlayerIsIn)
					IF NETWORK_DOES_NETWORK_ID_EXIST(VEH_TO_NET(vehPlayerIsIn))
						SET_NETWORK_VEHICLE_RESPOT_TIMER(VEH_TO_NET(vehPlayerIsIn), 0, FALSE, FALSE)
						NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(0)
						PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - Clearing Spawn Protection Flash.")
					ENDIF
				ENDIF
			ENDIF
			
			BROADCAST_FMMC_SPAWN_PROTECTION_ALPHA(FALSE, iLocalPart, GET_FRAME_COUNT())
			RESET_ENTITY_ALPHA(LocalPlayerPed)
			
			IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				AND DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
					RESET_ENTITY_ALPHA(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
				ENDIF
			ENDIF
			iAlphaResetAttempts++
			PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - iAlphaResetAttempts = ", iAlphaResetAttempts)
			IF iAlphaResetAttempts >= 10
				SET_BIT(iLocalBoolCheck28, LBOOL28_ALPHA_RESET_DONE)
				PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - iAlphaResetAttempts >= 10! Setting LBOOL28_ALPHA_RESET_DONE")
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_ALPHA_RESET_DONE)
				SET_BIT(iLocalBoolCheck28, LBOOL28_ALPHA_RESET_DONE)
				PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - setting LBOOL28_ALPHA_RESET_DONE Entity is visible.")
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_ALPHA_RESET_DONE)
			SET_BIT(iLocalBoolCheck28, LBOOL28_ALPHA_RESET_DONE)
			PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - setting LBOOL28_ALPHA_RESET_DONE Don't Care.")
		ENDIF
	ENDIF
	
	IF IS_THIS_A_VERSUS_MISSION()
	AND NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_WINNER_LOSER_SHARD_RENDERPHASE_INITIAL_PAUSE)
	AND NOT IS_PED_GETTING_UP(LocalPlayerPed)
	AND IS_BIT_SET(iLocalBoolCheck28, LBOOL28_ALPHA_RESET_DONE)
	AND NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationSummaryComplete)
		// Twice due to a weird Graphics code bug.
		TOGGLE_PAUSED_RENDERPHASES(FALSE)
		TOGGLE_PAUSED_RENDERPHASES(FALSE)
		SET_BIT(iLocalBoolCheck27, LBOOL27_WINNER_LOSER_SHARD_RENDERPHASE_INITIAL_PAUSE)
		PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - Renderphase Paused. 2")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_RESET_TO_DEFAULT_OUTFITS_ON_MISSION_END)
		IF GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
		AND NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_CELEBRATION_SCREEN_OUTFIT_CHANGE)
			TOGGLE_RENDERPHASES(FALSE, TRUE)
			PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - Renderphase Paused. 3")
			IF NOT IS_XBOX_PLATFORM() //Added for url:bugstar:4521637 - Render phases are fucked on xbox one.
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF

	IF NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
		// For modes which change an outfit mid-game.
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_RESET_TO_DEFAULT_OUTFITS_ON_MISSION_END)
			IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_CELEBRATION_SCREEN_OUTFIT_CHANGE)
				PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - Changing outfit back to default.")
				
				sApplyOutfitData.pedID 		= LocalPlayerPed
				sApplyOutfitData.eOutfit 	= INT_TO_ENUM(MP_OUTFIT_ENUM, GET_LOCAL_DEFAULT_OUTFIT(MC_playerBD[iPartToUse].iteam))
				sApplyOutfitData.eMask   	= INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, FMMC_GET_LOCAL_DEFAULT_MASK(MC_playerBD[iPartToUse].iteam))
			
				IF SET_PED_MP_OUTFIT(sApplyOutfitData)
				AND bLocalPlayerPedOK
				AND NOT IS_PED_DEAD_OR_DYING(localPlayerPed)
					PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - Outfit changed back to the default. We can now progress.")
					SET_BIT(iLocalBoolCheck26, LBOOL26_CELEBRATION_SCREEN_OUTFIT_CHANGE)
					BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
				ELSE
					PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - Returning False. Our outfit is still wrong.")
					RETURN FALSE
				ENDIF		
			ENDIF
		ENDIF	
	ENDIF
		
	SET_DO_MISSION_OUTRO_ANIM()
	POPULATE_RESULTS()
	COPY_CELEBRATION_DATA_TO_GLOBALS()
	
	IF NOT bIsSCTV
	AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer)
	
		// Copy from the Celeb version if we are not doing a heist celeb.
		IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = -1  	// Not set yet.
		AND g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId != -1	// Valid.
		AND g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission	
			PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - SHOULD_DO_POST_MISSION_SCENE_CELEBRATION = TRUE, saving out post mission scene data.")
			
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId
			
			PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId)
			
			// Get the players to be in the scene.
			INT iPart
			REPEAT ciPMC_PLAYER_MAX iPart
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPlayerNameHash[iPart] = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash[iPart]
			ENDREPEAT
		ENDIF
		
	ENDIF
		
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId != -1
		// Move the player to the scene coords as soon as we've paused the screen.
		IF NETWORK_IS_PLAYER_ACTIVE(LocalPlayer)
			IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_PlacedPlayerForLowriderPostScene)
				IF NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS() // sCelebrationData.eCurrentStage >= CELEBRATION_STAGE_PLAYING
					IF DOES_ENTITY_EXIST(LocalPlayerPed)
						VECTOR vSceneCoords
						vSceneCoords = GET_SYNCED_INTERACTION_SCENE_COORDS(g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId)
						CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
						SET_ENTITY_COORDS(LocalPlayerPed, vSceneCoords, FALSE)
						FREEZE_ENTITY_POSITION(LocalPlayerPed, TRUE)
						IF DOES_ENTITY_EXIST(LocalPlayerPed)
							Clear_All_Generated_MP_Headshots(FALSE)
							SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
							PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - reset clothes for freemode and cleared generated headshots.")
						ENDIF
						SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_PlacedPlayerForLowriderPostScene)
						PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] Post mission scene, renderpahses are paused, put player at GET_SYNCED_INTERACTION_SCENE_COORDS(", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId, ") = ", vSceneCoords)
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - Can't do PMC as renderphases are not paused.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bIsSCTV
	OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer)
		IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId != -1 
		OR g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId != -1
			PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - There's a valid iPostMissionSceneId so we need to skip the NJVS.")
			SET_TRANSITION_SESSIONS_SKIP_LB_AND_NJVS()
		ENDIF
		IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_OVER)
			SET_SKYSWOOP_UP()
			PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - IS_PLAYER_SCTV OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR Calling SET_SKYSWOOP_UP")
		ENDIF
	ENDIF
	
	// Fix for 2684556
	SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed, TRUE)
	
	// No pausing the celebration screen.
	IF IS_PAUSE_MENU_ACTIVE()
		SET_FRONTEND_ACTIVE(FALSE)
		PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - called SET_FRONTEND_ACTIVE(FALSE) to remove pause menu for celebration screen.")
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
	
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationSummaryComplete)
		SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationUnfrozeSkycam)
		SET_BIT(iLocalBoolCheck3,LBOOL3_SHOWN_RESULTS)
		PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - Returning true IS_A_STRAND_MISSION_BEING_INITIALISED.")
		RETURN TRUE
	ENDIF
	
	// If this causes any issues let me know please [LM] we may have to remove it.
	// url:bugstar:5400867
	IF IS_PLAYER_TELEPORT_ACTIVE()
	AND (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobals.g_NetWarpTime) < 10000)
		PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - IS_PLAYER_TELEPORT_ACTIVE = TRUE AND Time Spent Teleporting: ", (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobals.g_NetWarpTime)), " Returning FALSE to stall so we can finish before cam freeze.")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationSummaryComplete)
	
		CELEBRATION_SCREEN_TYPE eCelebType
		
		eCelebType = CELEBRATION_STANDARD
		IF g_FMMC_STRUCT.iCelebrationType = FMMC_CELEBRATION_TYPE_ISLAND_HEIST
			eCelebType = CELEBRATION_HEIST
		ENDIF
		
		PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - USING CELEBRATION SCREEN: ", ENUM_TO_INT(eCelebType))
		
		IF NOT g_bCelebrationScreenIsActive

			BOOL bTimeToStartScreen = FALSE
			
			IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PLACED_REVERSE_ANGLE_CAMERA)
				bCreatedCustomCelebrationCam = FALSE
			ENDIF

			//Do a deathmatch-style cam.
			IF NOT IS_PLAYER_SPECTATING(LocalPlayer)
			AND NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_PLAYER_IS_RESPAWNING)
				IF IS_THIS_A_VERSUS_MISSION()
				AND HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iteam)
					
					IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)
						IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PLACED_REVERSE_ANGLE_CAMERA)
							bTimeToStartScreen = TRUE
						ENDIF
					ELSE
						IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							IF ACTIVATE_CINEMATIC_CAM_FOR_IN_VEHICLE_CELEBRATION_SCREEN() // 2229861
								PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - ACTIVATE_CINEMATIC_CAM_FOR_IN_VEHICLE_CELEBRATION_SCREEN = TRUE 1")
								bTimeToStartScreen = TRUE
							ELSE
								PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - ACTIVATE_CINEMATIC_CAM_FOR_IN_VEHICLE_CELEBRATION_SCREEN = FALSE 1")
							ENDIF
						ELSE
							IF PLACE_CAMERA_FOR_CELEBRATION_SCREEN(sCelebrationData, camEndScreen, bCreatedCustomCelebrationCam)
								bTimeToStartScreen = TRUE
							ELSE
								PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - PLACE_CAMERA_FOR_CELEBRATION_SCREEN ")
							ENDIF
						ENDIF
							
					ENDIF
					
				ELIF IS_THIS_A_VERSUS_MISSION()
				AND NOT HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iteam)
					IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)
						IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PLACED_REVERSE_ANGLE_CAMERA)
							bTimeToStartScreen = TRUE
						ENDIF
					ELSE
						IF bLocalPlayerOK
							IF NOT bIsAnySpectator
								TOGGLE_RENDERPHASES(TRUE)
								PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - TOGGLE_RENDERPHASES TRUE")
								PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - SET_ENTITY_VISIBLE TRUE")
								IF PLACE_CAMERA_FOR_CELEBRATION_SCREEN(sCelebrationData, camEndScreen, bCreatedCustomCelebrationCam)
									bTimeToStartScreen = TRUE
								ELSE
									PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - PLACE_CAMERA_FOR_CELEBRATION_SCREEN ")
								ENDIF
							ELSE
								PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - PLAYER IS SPECTATING - skipping PLACE_CAMERA_FOR_CELEBRATION_SCREEN. Setting bTimeToStartScreen = TRUE.")
								TOGGLE_RENDERPHASES(FALSE)
								bTimeToStartScreen = TRUE
							ENDIF
						ELSE
							bTimeToStartScreen = TRUE
							PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - Setting bTimeToStartScreen = TRUE.")
						ENDIF					
					ENDIF					
				ELSE
					bTimeToStartScreen = TRUE
				ENDIF
			ELSE
				bTimeToStartScreen = TRUE
			ENDIF
			
			IF bTimeToStartScreen
				
				PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - bTimeToStartScreen: is TRUE")
							
				POPULATE_RESULTS()
				
				IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bStartedCelebrationBeforeCutsceneEnded)
					INITIALISE_CELEBRATION_SCREEN_DATA(sCelebrationData)
				ENDIF
				
				REQUEST_CELEBRATION_SCREEN(sCelebrationData, eCelebType)
				
				SET_CELEBRATION_SCREEN_AS_ACTIVE(TRUE)
				KILL_UI_FOR_CELEBRATION_SCREEN()
				HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
				PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - 4")
				
				// This might cause issues where we see under the world but we need this here so players are not stuck in a Black Screen.
				// Normally happens because we're sent to spectator and before we get a target we end the mission.
				IF IS_SCREEN_FADED_OUT()				
					PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - Emergancy Screen Fade In. Celebration Screen should start.")
					DO_SCREEN_FADE_IN_FOR_TRANSITION(250)
				ENDIF
				
				TOGGLE_PLAYER_DAMAGE_OVERLAY(FALSE)
				
				IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bStartedCelebrationBeforeCutsceneEnded)
					IF bCreatedCustomCelebrationCam
						IF DOES_CAM_EXIST(camEndScreen)
							SET_CAM_ACTIVE(camEndScreen, TRUE)
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
						ENDIF						
						PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - PLACE_CAMERA_FOR_CELEBRATION_SCREEN RENDER_SCRIPT_CAMS(TRUE, FALSE) ")
					ENDIF
					
					IF NOT ANIMPOSTFX_IS_RUNNING("MP_Celeb_Win")
					AND NOT ANIMPOSTFX_IS_RUNNING("MP_Celeb_Lose")
																			
						IF SHOULD_POSTFX_BE_WINNER_VERSION()
							PLAY_CELEB_WIN_POST_FX()
						ELSE
							PLAY_CELEB_LOSE_POST_FX()
							IF GET_REQUESTINGNIGHTVISION()
							OR GET_USINGNIGHTVISION()
								ENABLE_NIGHTVISION(VISUALAID_OFF)
								DISABLE_NIGHT_VISION_CONTROLLER(TRUE) // Added under instruction of Alwyn for B*2196822
							ENDIF
						ENDIF
					
					ENDIF
					
					TOGGLE_PAUSED_RENDERPHASES(FALSE)
					
					RESET_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
					START_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
					
					IF IS_THIS_ROCKSTAR_MISSION_NEW_HALLOWEEN_ADVERSARY_2022(g_FMMC_STRUCT.iAdversaryModeType)
						IF SHOULD_POSTFX_BE_WINNER_VERSION()
							PLAY_SOUND_FRONTEND(-1, "Round_Win", "Halloween_Adversary_Sounds")
						ELSE
							PLAY_SOUND_FRONTEND(-1, "Round_Lost", "Halloween_Adversary_Sounds")
						ENDIF
					ELSE 
						PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS", FALSE)
					ENDIF
					
					PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - STARTING CELEBRATION TIMER")
				ENDIF
			ELSE
				PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - bTimeToStartScreen: is FALSE")
			ENDIF

		ELSE
			IF HAS_CELEBRATION_SUMMARY_FINISHED()
				RESET_CELEBRATION_SCREEN_STATE(sCelebrationData)
				
				RESET_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
				START_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
				
				CLEAR_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationUnfrozeSkycam)
				SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationSummaryComplete)
				
				PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - HAS_CELEBRATION_SUMMARY_FINISHED returning true... Setting ciCELEBRATION_BIT_SET_bCelebrationSummaryComplete")
			ELSE
				PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - HAS_CELEBRATION_SUMMARY_FINISHED returning false...")
			ENDIF
			
		ENDIF
	
	ELSE
		
		HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
				
		IF g_bCelebrationScreenIsActive
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_RESET_TO_DEFAULT_OUTFITS_ON_MISSION_END)
				IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_CELEBRATION_SCREEN_OUTFIT_CHANGE_BROADCAST)
					PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - CALLING REFRESH...")
					SET_BIT(iLocalBoolCheck26, LBOOL26_CELEBRATION_SCREEN_OUTFIT_CHANGE_BROADCAST)
					BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
				ENDIF
			ENDIF
		
			BOOL bSafeToContinue
			PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - 0")
			//Only display the winner screen if the player passed the mission.
			IF g_bVSMission
				IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
				AND IS_CELEBRATION_FOR_NON_MATCH_OR_LAST_ROUND_OF_MATCH()
					IF HAS_CELEBRATION_WINNER_FINISHED(TRUE)
						bSafeToContinue = TRUE
						PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - 1")
					ELSE
						PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - 2")
					ENDIF
				ELSE
					bSafeToContinue = TRUE
					PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED -  bSafeToContinue = TRUE 2")
				ENDIF
			ELSE
				//In case the transition from the previous screen is tight: keep drawing to prevent pops.
				IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
					DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData, FALSE)
				ENDIF
				bSafeToContinue = TRUE
				PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - bSafeToContinue = TRUE 3")
			ENDIF
			PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - 2 bSafeToContinue ", BOOL_TO_INT(bSafeToContinue))
			//Need to wait for the player to play their gesture before starting the swoop cam (just ignore if we skipped the winner screen).
			IF bSafeToContinue	

				TOGGLE_PLAYER_DAMAGE_OVERLAY(TRUE)
				
				PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - HAS_CELEBRATION_SCREEN_FINISHED - 3")
				
				IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
					STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
				ENDIF
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_OUT_TRANSITION_ONESHOT_SCENE")
					START_AUDIO_SCENE("MP_CELEB_SCREEN_OUT_TRANSITION_ONESHOT_SCENE")
				ENDIF
					
				IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId != -1
				OR SET_SKYSWOOP_UP(FALSE, TRUE, FALSE, DEFAULT, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, FALSE, FALSE, TRUE) //This skyswoop needs to keep the screen frozen until the first jump.
					
					IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId != -1
						IF NOT IS_SCREEN_FADED_OUT()
							IF NOT IS_SCREEN_FADING_OUT()
								PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - Doing fadeout because iPostMissionSceneId: ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId)
								DO_SCREEN_FADE_OUT(1000)
								PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SUMMARY_FINISHED - Calling SET_TRANSITION_SESSIONS_SKIP_LB_AND_NJVS - Post mission scene") 	
								SET_TRANSITION_SESSIONS_SKIP_LB_AND_NJVS()
							ENDIF
							
							PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - Waiting for fadeout because iPostMissionSceneId: ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId)
							RETURN FALSE
							
						ENDIF
					ELSE
						IF IS_SCREEN_FADED_OUT()
							PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - Emergancy Screen Fade In. Celebration Screen should start.")
							DO_SCREEN_FADE_IN_FOR_TRANSITION(250)
						ENDIF
					ENDIF
					
					PROCESS_CACHING_LOCAL_PLAYER_STATE() //Ped has always been re-cached here - not clear what would be altering it but leaving it in

					ANIMPOSTFX_STOP("MP_Celeb_Win")
					ANIMPOSTFX_STOP("MP_Celeb_Lose")
					
					REMOVE_MP_HEIST_GEAR(LocalPlayerPed, HEIST_GEAR_NIGHTVISION) // Remove night vision for celebration screen.
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_SCENE")
						START_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
					ENDIF
					
					IF SHOULD_DO_TRANSITION_MENU_AUDIO()
						//1665147 - Start the pause menu music as soon as the celebration screen ends.
						RUN_TRANSITION_MENU_AUDIO(TRUE)
					ENDIF
					
					CLEAR_ALL_BIG_MESSAGES()
					
					IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
						PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - CLEANUP_CELEBRATION_SCREEN call 2.")
						STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "WINNER")
						STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "SUMMARY")
						STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "EARLYDEATH")
						CLEANUP_CELEBRATION_SCREEN(sCelebrationData, TRUE, "WINNER")
						CLEANUP_CELEBRATION_SCREEN(sCelebrationData, TRUE, "SUMMARY")
						CLEANUP_CELEBRATION_SCREEN(sCelebrationData, TRUE, "EARLYDEATH")
					ENDIF
					
					SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
					REINIT_NET_TIMER(tdresultstimer)
					SET_BIT(iLocalBoolCheck3,LBOOL3_SHOWN_RESULTS)
					
					RETURN TRUE
				ENDIF
					
				PROCESS_CACHING_LOCAL_PLAYER_STATE() //Ped has always been re-cached here - not clear what would be altering it but leaving it in
				
			ENDIF
			
			// Stop post fx on first switch cut.
			IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_StoppedAllPostFxForSwitch)
				IF IS_PLAYER_SWITCH_IN_PROGRESS()
					IF (GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_ASCENT)
						IF GET_REQUESTINGNIGHTVISION()
						OR GET_USINGNIGHTVISION()
							ENABLE_NIGHTVISION(VISUALAID_OFF)
							DISABLE_NIGHT_VISION_CONTROLLER(TRUE)
						ENDIF
						CLEAR_ALL_CELEBRATION_POST_FX()
						PRINTLN("[Celebrations_Internal] - HAS_CELEBRATION_SCREEN_FINISHED - called ANIMPOSTFX_STOP_ALL, switch state is in SWITCH_STATE_JUMPCUT_ASCENT.")
						SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_StoppedAllPostFxForSwitch)
					ENDIF
				ENDIF
			ENDIF
			
			//Make sure the screen is unfrozen when the sky cam starts jumping up.
			IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationUnfrozeSkycam)
				IF IS_SWITCH_TO_MULTI_FIRSTPART_FINISHED()
					SET_SKYFREEZE_CLEAR(TRUE)
					SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationUnfrozeSkycam)
				ENDIF
			ENDIF
			
		ELSE

			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_INTERNAL_CELEBRATION_SCREEN()
	
	IF IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
		EXIT
	ENDIF
	
	BOOL bPreLoadEffectBeenPlayingLongEnough = TRUE
	BOOL bSortedCamera = TRUE
	
	// If we have started the celebration pre load effect, make sure it's had time to play properly.
	IF HAS_NET_TIMER_STARTED(sCelebrationData.timerCelebPreLoadPostFxBeenPlaying)		
		IF NOT HAS_NET_TIMER_EXPIRED(sCelebrationData.timerCelebPreLoadPostFxBeenPlaying, ciMissionEndEffectDelay)
			bPreLoadEffectBeenPlayingLongEnough = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
		IF NOT bIsSCTV
		AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer)
			IF IS_PED_DEAD_OR_DYING(LocalPlayerPed)
			OR !bLocalPlayerPedOK
				IF SHOULD_RESURRECT_PLAYER_AT_END()
					IF NOT IS_SCREEN_FADING_OUT()
						DO_SCREEN_FADE_OUT(150)
					ENDIF
					
					IF IS_SCREEN_FADED_OUT()
						PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - Resurrecting Player.")
						FMMC_NETWORK_RESURRECT_LOCAL_PLAYER(GET_ENTITY_COORDS(LocalPlayerPed), GET_ENTITY_HEADING(LocalPlayerPed), 0, FALSE, TRUE)
						CLEAR_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggeredRenderPhasePause)
					ENDIF
					
					VECTOR vNewCoord = GET_ENTITY_COORDS(LocalPlayerPed, FALSE)
					IF GET_GROUND_Z_FOR_3D_COORD(vNewCoord, vNewCoord.z)
						SET_ENTITY_COORDS(LocalPlayerPed, vNewCoord)
						PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - Setting on ground..")
					ENDIF
					
					g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockSpawnFadeInForMissionFail = TRUE
					SET_BIT(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)
					CLEANUP_WHITEOUT_RESPAWNING_FADING()
					CLEANUP_BLOCK_WASTED_KILLSTRIP_SCREEN()
					
					PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - Setting LBOOL10_RESPAWNING_DURING_FAIL.")
				ENDIF
			ELSE
				IF (IS_PLAYER_RESPAWNING(LocalPlayer) OR bIsAnySpectator)
				AND NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)
					SET_BIT(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)
					CLEANUP_WHITEOUT_RESPAWNING_FADING()
					CLEANUP_BLOCK_WASTED_KILLSTRIP_SCREEN()
					FORCE_RESPAWN(TRUE)
					FORCE_PlAYER_BACK_INTO_PLAYING_STATE(TRUE, TRUE)
					SET_MANUAL_RESPAWN_STATE(MRS_NULL)
					RESET_GAME_STATE_ON_DEATH()
					CLEAR_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggeredRenderPhasePause)
					PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - Force Respawning Player.")
				ENDIF
				
				SET_PED_CONFIG_FLAG(localPlayerPed, PCF_GetOutUndriveableVehicle, FALSE)
				SET_PED_CONFIG_FLAG(localPlayerPed, PCF_GetOutBurningVehicle, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PLACED_REVERSE_ANGLE_CAMERA)
		IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)		
			PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - LBOOL10_RESPAWNING_DURING_FAIL = TRUE")
			IF bLocalPlayerOK
				PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - IS_NET_PLAYER_OK = TRUE.")
				IF NOT IS_PLAYER_RESPAWNING(LocalPlayer)
					PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - IS_PLAYER_RESPAWNING = FALSE.")
					IF g_KillCamStruct.eCurrentStage = KILL_CAM_STAGE_OFF
						PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - g_KillCamStruct.eCurrentStage = KILL_CAM_STAGE_OFF.")
						IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
							PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - IS_ANY_BIG_MESSAGE_BEING_DISPLAYED = FALSE.")
							PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - IS_GAMEPLAY_CAM_RENDERING = TRUE.")
							IF PLACE_CAMERA_FOR_CELEBRATION_SCREEN(sCelebrationData, camEndScreen, bCreatedCustomCelebrationCam)
								PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - PLACE_CAMERA_FOR_CELEBRATION_SCREEN = TRUE.")
								IF bCreatedCustomCelebrationCam
									PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - bCreatedCustomCelebrationCam = TRUE.")
									IF DOES_CAM_EXIST(camEndScreen)
										PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - DOES_CAM_EXIST(camEndScreen) = TRUE.")
										IF NOT IS_CAM_RENDERING(camEndScreen)											
											IF NOT IS_BIT_SET(iLocalBoolCheck35, LBOOL35_FORCE_ANIM_UPDATE_FOR_CELEB_SCREEN)
												PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - Calling FORCE_PED_AI_AND_ANIMATION_UPDATE.")
												SET_BIT(iLocalBoolCheck35, LBOOL35_FORCE_ANIM_UPDATE_FOR_CELEB_SCREEN)
												FORCE_PED_AI_AND_ANIMATION_UPDATE(localPlayerPed)
											ELSE
												IF HAS_PED_HEAD_BLEND_FINISHED(LocalPlayerPed)
													PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - IS_CAM_RENDERING(camEndScreen) = FALSE.")
													IF g_bCelebrationScreenIsActive
														PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - Killing the early death screen to make way for the SUMMARY!")
														STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "EARLYDEATH")
														CLEANUP_CELEBRATION_SCREEN(sCelebrationData, TRUE, "EARLYDEATH")
														INITIALISE_CELEBRATION_SCREEN_DATA(sCelebrationData)
													ENDIF
													SET_CAM_ACTIVE(camEndScreen, TRUE)													
													RENDER_SCRIPT_CAMS(TRUE, FALSE)
													PRINTLN("[RCC MISSION] - [SAC] - rendered cam camEndScreen.")
												ELSE
													PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - Waiting for HAS_PED_HEAD_BLEND_FINISHED")
												ENDIF
											ENDIF											
										ENDIF
									ENDIF
								ELSE
									PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - bCreatedCustomCelebrationCam = FALSE, set bit LBOOL13_NOT_SAFE_FOR_ALTERNATE_CAM.")
									SET_BIT(iLocalBoolCheck13, LBOOL13_NOT_SAFE_FOR_ALTERNATE_CAM)
								ENDIF
								
								PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - set bit LBOOL10_PLACED_REVERSE_ANGLE_CAMERA.")
								SET_BIT(iLocalBoolCheck10, LBOOL10_PLACED_REVERSE_ANGLE_CAMERA)
								
								IF HAS_NET_TIMER_STARTED(sCelebrationData.sCelebrationTransitionTimer)
									RESET_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
									START_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
									PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - sCelebrationTransitionTimer had already started, reinitializing it.")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	FREEZE_SCREEN_FOR_PLACED_CAMERA()
	
	IF NOT bIsSCTV
	AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer)
		IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)
			IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PLACED_REVERSE_ANGLE_CAMERA)
				PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - LBOOL10_PLACED_REVERSE_ANGLE_CAMERA bit not set, setting bSortedCamera = FALSE.")
				bSortedCamera = FALSE
			ENDIF
			IF iFreezeCamForPlaceDcamStage < 3
				PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - iFreezeCamForPlaceDcamStage = ", iFreezeCamForPlaceDcamStage, ", setting bSortedCamera = FALSE.")
				bSortedCamera = FALSE
			ENDIF
		ENDIF
	ENDIF
		
	// Run the main celebration logic.
	IF (bPreLoadEffectBeenPlayingLongEnough	AND bSortedCamera)
	#IF IS_DEBUG_BUILD
	OR g_SkipCelebAndLbd
	#ENDIF
		IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
		AND NOT (SHOULD_MISSION_USE_EXTERNAL_CELEBRATION_SCREEN() AND HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iTeam))
			IF HAS_CELEBRATION_SCREEN_FINISHED()
				PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - HAS_CELEBRATION_SCREEN_FINISHED = TRUE Setting ciCELEBRATION_BIT_SET_EndCelebrationFinished")
				SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_EndCelebrationFinished)
			ELSE
				PRINTLN("[Celebrations_Internal] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - HAS_CELEBRATION_SCREEN_FINISHED = FALSE")
			ENDIF
		ENDIF
	
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: External Celebration Screen
// ##### Description: Functions for running the celebration screen from Celebrations.sc. Used for heists mainly.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_HEIST_CELEBRATION_PLAYER_VARIATIONS_STATE(HEIST_CELEBRATION_PLAYER_VARIATIONS_STATE eNewState)
	IF eHeistCelebrationPlayerVariationsState = eNewState 
		EXIT
	ENDIF
	PRINTLN("[Celebrations_External] - SET_HEIST_CELEBRATION_PLAYER_VARIATIONS_STATE - From: ", ENUM_TO_INT(eHeistCelebrationPlayerVariationsState), " to: ", ENUM_TO_INT(eNewState))
	eHeistCelebrationPlayerVariationsState = eNewState 
ENDPROC

FUNC BOOL SET_PLAYER_VARIATIONS_FOR_HEIST_CELEBRATION()

	INT i
	
	BOOL bAllStreamingRequestsComplete = TRUE
	
	SWITCH eHeistCelebrationPlayerVariationsState
		
		CASE HCPV_SetPedVariations
			
			IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggeredCelebrationStreamingRequests)
				SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
				SET_HEIST_CELEBRATION_PLAYER_VARIATIONS_STATE(HCPV_WaitForStreamingRequests)
			ENDIF
			
		BREAK
		
		CASE HCPV_WaitForStreamingRequests
			
			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_COMPLETED_CELEBRATION_STREAMING_REQUESTS)
				IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(LocalPlayerPed)
					SET_BIT(MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_COMPLETED_CELEBRATION_STREAMING_REQUESTS)
					SET_HEIST_CELEBRATION_PLAYER_VARIATIONS_STATE(HCPV_WaitForOtherPlayers)
				ENDIF
			ENDIF
			
		BREAK
		
		CASE HCPV_WaitForOtherPlayers
			
			FOR i = 0 TO NUM_NETWORK_PLAYERS - 1
				IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					RELOOP
				ENDIF
				
				IF IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
					RELOOP
				ENDIF
				
				IF NOT IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_COMPLETED_CELEBRATION_STREAMING_REQUESTS)
					bAllStreamingRequestsComplete = FALSE
					PRINTLN("[Celebrations_External] - SET_HEIST_CELEBRATION_PLAYER_VARIATIONS_STATE - part ", i, " not finished streaming requests.")
					BREAKLOOP
				ENDIF
			ENDFOR
			
			IF bAllStreamingRequestsComplete
				SET_HEIST_CELEBRATION_PLAYER_VARIATIONS_STATE(HCPV_WaitForVariationUpdates)
			ENDIf
			
		BREAK
		
		CASE HCPV_WaitForVariationUpdates
		
			IF NOT HAS_NET_TIMER_STARTED(timerCelebReceivedAllPlayerVariationUpdates)
				REINIT_NET_TIMER(timerCelebReceivedAllPlayerVariationUpdates)
				PRINTLN("[Celebrations_External] - SET_HEIST_CELEBRATION_PLAYER_VARIATIONS_STATE - starting timer.")
			ELIF HAS_NET_TIMER_EXPIRED(timerCelebReceivedAllPlayerVariationUpdates, 3000)
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.bStreamingRequestsComplete = TRUE
				SET_HEIST_CELEBRATION_PLAYER_VARIATIONS_STATE(HCPV_Finished)
			ENDIF
		
		BREAK
		
		CASE HCPV_Finished
			
			RETURN TRUE
			
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_SCTV_PLAYER_POPULATED_CELEBRATION_GLOBALS()
	
	IF NOT bIsSCTV
	AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].bJoinedMissionAsSpectator
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck9, LBOOL9_COPIED_CELEBRATION_DATA_EVENT_1)
	AND IS_BIT_SET(iLocalBoolCheck9, LBOOL9_COPIED_CELEBRATION_DATA_EVENT_2)
		RETURN TRUE
	ENDIF
		
	IF NOT HAS_NET_TIMER_STARTED(sCelebrationData.SpecFillGlobalsTimeout)
		REINIT_NET_TIMER(sCelebrationData.SpecFillGlobalsTimeout)
	ELIF HAS_NET_TIMER_EXPIRED(sCelebrationData.SpecFillGlobalsTimeout, 60000)
		RETURN TRUE
	ENDIF

	PRINTLN("[Celebrations_External] - SET_HEIST_CELEBRATION_PLAYER_VARIATIONS_STATE - waiting for sctv globals to sync.")
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL ARE_ALL_PLAYERS_READY_FOR_EXTERNAL_CELEBRATION()
	
	INT iPart
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
		PARTICIPANT_INDEX temppart = INT_TO_PARTICIPANTINDEX(iPart)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(temppart)
			PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(temppart)
			IF IS_NET_PLAYER_OK(tempPlayer, FALSE)
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitset, PBBOOL_PLAYER_OUT_OF_LIVES)
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitset, PBBOOL_PLAYER_FAIL)
				IF IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_PLAYING_MOCAP_CUTSCENE )
					PRINTLN("[Celebrations_External] - ARE_ALL_PLAYERS_READY_FOR_EXTERNAL_CELEBRATION - FALSE for Part: ", iPart, " Not finished Mocap.")
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_COMPLETED_POPULATION_OF_CELEB_GLOBALS)
					PRINTLN("[Celebrations_External] - ARE_ALL_PLAYERS_READY_FOR_EXTERNAL_CELEBRATION - FALSE for Part: ", iPart, " Not populated celebration globals.")
				RETURN FALSE
			ENDIF
			
			IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_ALL_PARTICIPANTS_LAUNCHED_CELEB_SCRIPT)
					PRINTLN("[Celebrations_External] - ARE_ALL_PLAYERS_READY_FOR_EXTERNAL_CELEBRATION - FALSE for Part: ", iPart, " Not launched celebration script.")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

PROC MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN()
	
	IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerHeistWinnerCutscene)
		EXIT
	ENDIF
	
	SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(TRUE)
	SET_PLAYER_VARIATIONS_FOR_HEIST_CELEBRATION()
	
	IF NOT HAS_SCTV_PLAYER_POPULATED_CELEBRATION_GLOBALS()
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggeredHeistWinnerCutscene)
	
		IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_LB_CAM_READY)
		AND IS_BIT_SET(iLocalBoolCheck3, LBOOL3_SHOWN_RESULTS)
			//All done
			EXIT
		ENDIF
		
		IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bCloningComplete
			PRINTLN("[Celebrations_External] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - Cloning Complete - FALSE.")
			EXIT
		ENDIF

		IF NOT ARE_ALL_PLAYERS_READY_FOR_EXTERNAL_CELEBRATION()
			PRINTLN("[Celebrations_External] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - ARE_ALL_PLAYERS_READY_FOR_EXTERNAL_CELEBRATION - FALSE.")
			EXIT
		ENDIF
			
		PRINTLN("[Celebrations_External] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - Setting bits to allow Mission Controller to end.")
		
		SET_BIT(iLocalBoolCheck2, LBOOL2_LB_CAM_READY)
		SET_BIT(iLocalBoolCheck3, LBOOL3_SHOWN_RESULTS)
		
		EXIT
		
	ENDIF
	
	//Celebration Screen not triggered yet
	
	IF HAS_NET_TIMER_STARTED(sCelebrationData.timerCelebPreLoadPostFxBeenPlaying)
	AND NOT HAS_NET_TIMER_EXPIRED(sCelebrationData.timerCelebPreLoadPostFxBeenPlaying, ciMissionEndEffectDelay)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
		EXIT
	ENDIF
	
	POPULATE_RESULTS() 
	COPY_CELEBRATION_DATA_TO_GLOBALS()	
	SAVE_EOM_VEHCILE()
	
	REQUEST_SCRIPT("Celebrations")
	IF NOT HAS_SCRIPT_LOADED("Celebrations")
		PRINTLN("[Celebrations_External] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - Loading Celebrations.sc")
		EXIT
	ENDIF
	
	IF DOES_MISSION_HAVE_AN_END_WINNER_SCENE()
		PRINTLN("[Celebrations_External] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - Mission has an end scene.")
		SET_DOING_HEIST_END_WINNER_SCENE(TRUE)
	ELSE
		PRINTLN("[Celebrations_External] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - Mission has no end scene.")
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bCloningComplete = TRUE
	ENDIF
	
	PRINTLN("[Celebrations_External] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - Launching Celebrations Script.")
	
	SET_DOING_HEIST_CELEBRATION_SCREEN(TRUE)
	
	SET_TRANSITION_SESSIONS_SKIP_LB_AND_NJVS()
	
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bDoingCelebrationTransition = TRUE
	
	CELEBRATION_SCRIPT_DATA sLaunchData
							
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
		sLaunchData.leaderHandle = GET_GAMER_HANDLE_PLAYER(GB_GET_LOCAL_PLAYER_GANG_BOSS())
	ENDIF
	
	START_NEW_SCRIPT_WITH_ARGS("Celebrations", sLaunchData.leaderHandle, SIZE_OF(sLaunchData), MULTIPLAYER_MISSION_STACK_SIZE)
	SET_SCRIPT_AS_NO_LONGER_NEEDED("Celebrations")
	
	SET_BIT(MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_ALL_PARTICIPANTS_LAUNCHED_CELEB_SCRIPT)
	SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggeredHeistwinnerCutscene)

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Main Processing
// ##### Description: Every frame processing of Celebration Screens
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
		
PROC PROCESS_CLEEBRATION_SCREENS_EVERY_FRAME()
	
	MAINTAIN_CELEB_PRE_LOAD_FX(sCelebrationData.bShownPreloadFX, sCelebrationData.timerCelebPreLoadPostFxBeenPlaying)
	
	MAINTAIN_INTERNAL_CELEBRATION_SCREEN()
	
	MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN()
	
ENDPROC
