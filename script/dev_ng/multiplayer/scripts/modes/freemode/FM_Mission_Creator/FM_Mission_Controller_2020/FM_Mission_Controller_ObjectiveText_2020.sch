// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Objective Text ----------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### Processes displaying objective text.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020 -------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Include_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Objective Text Delays
// ##### Description: Functionality to check if a specific objective type's text is ready to update.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_OBJECTIVE_TEXT_DELAY_FOR_TYPE(INT iDelayType)

	SWITCH iDelayType
		CASE ciOBJECTIVE_TEXT_DELAY_VEHICLE
		CASE ciOBJECTIVE_TEXT_DELAY_PED
		CASE ciOBJECTIVE_TEXT_DELAY_OBJECT
		CASE ciOBJECTIVE_TEXT_DELAY_OVERRIDE
			RETURN OBJECTIVE_TEXT_DELAY
			
		CASE ciOBJECTIVE_TEXT_DELAY_WANTED
		CASE ciOBJECTIVE_TEXT_DELAY_PLAYER
			RETURN OBJECTIVE_TEXT_DELAY_LONG
			
		CASE ciOBJECTIVE_TEXT_DELAY_LOCATION
			RETURN OBJECTIVE_TEXT_DELAY_VERY_LONG
	ENDSWITCH
	   
	PRINTLN("[ObjectiveText_Delay] GET_OBJECTIVE_TEXT_DELAY_FOR_TYPE - Invalid delay type: ", iDelayType)
	ASSERTLN("[ObjectiveText_Delay] GET_OBJECTIVE_TEXT_DELAY_FOR_TYPE - Invalid delay type: ", iDelayType)
	RETURN OBJECTIVE_TEXT_DELAY
	
ENDFUNC

PROC START_OBJECTIVE_TEXT_DELAY(INT iDelayType)
	PRINTLN("[ObjectiveText_Delay] START_OBJECTIVE_TEXT_DELAY - Delay type: ", GET_OBJECTIVE_TEXT_DELAY_TYPE_STRING(iDelayType), " update requested, starting/restarting timer.")
	REINIT_NET_TIMER(tdObjectiveTextActiveDelayTimers[iDelayType])
	SET_BIT(iObjectiveTextActiveDelayBitset, iDelayType)
	CLEAR_BIT(iObjectiveTextRequestDelayBitset, iDelayType)
ENDPROC

PROC PROCESS_COMPLETED_OBJECTIVE_TEXT_DELAY_TIMERS()
	
	INT i
	
	#IF IS_DEBUG_BUILD
	IF iObjectiveTextRequestDelayBitset != 0
		PRINTLN("[ObjectiveText_Delay] PROCESS_COMPLETED_OBJECTIVE_TEXT_DELAY_TIMERS - Clearing request queue")
	ENDIF
	#ENDIF
	
	FOR i = 0 TO ciOBJECTIVE_TEXT_DELAY_MAX - 1
		IF iObjectiveTextRequestDelayBitset = 0
			BREAKLOOP
		ENDIF
		
		IF NOT IS_BIT_SET(iObjectiveTextRequestDelayBitset, i)
			RELOOP
		ENDIF
		
		START_OBJECTIVE_TEXT_DELAY(i)
	ENDFOR

	FOR i = 0 TO ciOBJECTIVE_TEXT_DELAY_MAX - 1
		IF iObjectiveTextActiveDelayBitset = 0
			BREAKLOOP
		ENDIF
	
		IF NOT IS_BIT_SET(iObjectiveTextActiveDelayBitset, i)
			RELOOP
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED_AND_EXPIRED(tdObjectiveTextActiveDelayTimers[i], GET_OBJECTIVE_TEXT_DELAY_FOR_TYPE(i))
			RELOOP
		ENDIF
		
		RESET_NET_TIMER(tdObjectiveTextActiveDelayTimers[i])
		CLEAR_BIT(iObjectiveTextActiveDelayBitset, i)
		PRINTLN("[ObjectiveText_Delay] PROCESS_COMPLETED_OBJECTIVE_TEXT_DELAY_TIMERS - Delay type: ", GET_OBJECTIVE_TEXT_DELAY_TYPE_STRING(i), " cleared.")
	ENDFOR
	
ENDPROC

FUNC BOOL IS_OBJECTIVE_TEXT_READY_TO_UPDATE(INT iDelayType)
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iObjectiveTextRequestDelayBitset, iDelayType)
		START_OBJECTIVE_TEXT_DELAY(iDelayType)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iObjectiveTextActiveDelayBitset, iDelayType)
		IF HAS_NET_TIMER_STARTED_AND_EXPIRED(tdObjectiveTextActiveDelayTimers[iDelayType], GET_OBJECTIVE_TEXT_DELAY_FOR_TYPE(iDelayType))
			PRINTLN("[ObjectiveText_Delay] IS_OBJECTIVE_TEXT_READY_TO_UPDATE - Delay type: ", GET_OBJECTIVE_TEXT_DELAY_TYPE_STRING(iDelayType), " is ready to update.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

