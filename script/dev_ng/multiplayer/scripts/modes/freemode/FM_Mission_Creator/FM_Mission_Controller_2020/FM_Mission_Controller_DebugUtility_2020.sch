// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Debug Utility -------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: The aim of this header is to temporarily contain (or retroactively contain) debug functions that the main process and entity loops call with parameters.
// ##### in the future PROCESS functions should be declared and called from the DebugProcessing header.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD

USING "FM_Mission_Controller_DebugProfiler_2020.sch"

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Command Lines ------------------------------------------------------------------
// ##### Description: Caching commandlines ------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

//[FMMC2020] - Remove no longer needed ones and move to a debug bitset
PROC CACHE_COMMANDLINES()
	PRINTLN("CACHE_COMMANDLINES - Caching all command lines")
	
	bInVehicleDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_InVehicleDebug")
	bPlacedMissionVehiclesDebugHealth = GET_COMMANDLINE_PARAM_EXISTS("sc_MissionPlacedVehicleHealth")
	bobjectiveVehicleOnCargobobDebug = GET_COMMANDLINE_PARAM_EXISTS("objectiveVehicleOnCargobobDebug")
	bAllowAllMissionsWithTwoPlayers = DEBUG_IS_ALLOW_ALL_MISSION_WITH_TWO_PLAYERS_ENABLED()
	bTeamColourDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_TeamColourDebug")
	bFMMCOverrideMultiRuleTimer = GET_COMMANDLINE_PARAM_EXISTS("sc_FMMCOverrideMultiRuleTimer")
	bJS3226522 = GET_COMMANDLINE_PARAM_EXISTS("JS3226522")
	bAllowGangOpsWithoutBoss = GET_COMMANDLINE_PARAM_EXISTS("sc_AllowGangOpsWithoutBoss")
	bHandcuff = GET_COMMANDLINE_PARAM_EXISTS("sc_handcuff")
	bMCScriptAIAdditionalPrints = TRUE // GET_COMMANDLINE_PARAM_EXISTS("sc_MCScriptAIAdditionalPrints")
	bMCScriptAISpammyPrints = GET_COMMANDLINE_PARAM_EXISTS("sc_MCScriptAISpammyPrints")
	bExpProjDebug = GET_COMMANDLINE_PARAM_EXISTS("ExpProjDebug")
	bObjectGotoDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_ObjectGotoDebug")
	bMultiPersonVehiclePrints = GET_COMMANDLINE_PARAM_EXISTS("sc_MultiPersonVehiclePrints")
	bSkipSVMIntro = GET_COMMANDLINE_PARAM_EXISTS("sc_SkipSVMIntro")
	bAllowTripSkipNoPlay = GET_COMMANDLINE_PARAM_EXISTS("sc_AllowTripSkipNoPlay")
	bIdleKickPrints = GET_COMMANDLINE_PARAM_EXISTS("sc_g_B_IdleKickPrints")
	bDebugPrintCustomText = GET_COMMANDLINE_PARAM_EXISTS("sc_DebugPrintCustomText")
	bOverrideCCTVRotDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_OverrideCCTVRotDebug")
	bsc3378817 = GET_COMMANDLINE_PARAM_EXISTS("sc3378817")
	bHeistHashedMac = GET_COMMANDLINE_PARAM_EXISTS("sc_HeistHashedMac")
	bAutoForceRestartNoCorona = GET_COMMANDLINE_PARAM_EXISTS("sc_AutoForceRestartNoCorona")
	bPhoneEMPDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_phoneEMPDebug")
	bMissionYachtDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_missionYachtDebug")
	bHealthDrainZoneDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_healthDrainZoneDebug")
	bObjectTaserDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_objectTaserDebug")
	bZoneTimerDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_zoneTimerDebug")
	bInteractWithDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_interactWithDebug")
	bCCTVDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_CCTVDebug")
	bDoorDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_DoorDebug")
	bDirectionalDoorDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_DirectionalDoorDebug")
	bDynoPropSpam = GET_COMMANDLINE_PARAM_EXISTS("sc_DynoPropSpam")
	bObjDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_ObjDebug")
	bHumaneSyncLockDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_HumaneSyncLockDebug")
	bZoneDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_zoneDebug")
	bDontFrameStaggeredDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_MCNoFrameStaggeredDebug")
	bPrintExtraObjectiveCompletion = GET_COMMANDLINE_PARAM_EXISTS("sc_PrintExtraObjectiveCompletion")
	bBoundsDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_boundsDebug")
	bAddToGroupPrints = GET_COMMANDLINE_PARAM_EXISTS("sc_PrintExtraObjectiveCompletion")
	bInteractableDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_interactableDebug")
	bWorldPropDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_WorldPropDebug")
	bUnderwaterTunnelWeldDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_UnderwaterWeldingDebug")
	bMissionControllerProfilerEnabled = GET_COMMANDLINE_PARAM_EXISTS("sc_EnableMissionControllerProfiler")
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_MissionControllerProfilerResetInterval")
		ciFMMC_PROFILER_RESET_INTERVAL = GET_COMMANDLINE_PARAM_INT("sc_MissionControllerProfilerResetInterval")
	ENDIF
	bMissionControllerProfilerPeakPrints = GET_COMMANDLINE_PARAM_EXISTS("sc_MissionControllerProfilerPeakPrints")
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_AutoShowMissionControllerProfiler")
		bShowProfiler = TRUE
	ENDIF
	bBlockBranchingOnAggro = GET_COMMANDLINE_PARAM_EXISTS("sc_BlockBranchingOnAggro")
	bBlockBranchingOnAggroKeepStealthDialogue = GET_COMMANDLINE_PARAM_EXISTS("sc_BlockBranchingOnAggroKeepStealthDialogue")
	bAllFMMCPrereqsSet = GET_COMMANDLINE_PARAM_EXISTS("sc_AllFMMCPrereqsSet")
	bDialogueTriggerDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_DialogueTriggerDebug")
	bPantherDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_PantherDebug")
	bEnterSafeCombinationDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_EnterSafeCombinationDebug")
	bGlassCuttingMinigameDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_GlassCuttingMinigameDebug")
	bInterrogationMinigameDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_InterrogationMinigameDebug")
	bObjectLastSafeDropPointDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_ObjectLastSafeDropPointDebug")
	bScriptedFlammableEntityDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_ScriptedFlammableEntityDebug")
	bInteriorDestructionDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_InteriorDestructionDebug")
	bEntityCheckpointContinuityDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_EntityCheckpointContinuityDebug")
	bBlockAllDialogue = GET_COMMANDLINE_PARAM_EXISTS("sc_BlockAllDialogue")
	bDebugForceCutsceneMinimumShotsProcessing = GET_COMMANDLINE_PARAM_EXISTS("sc_DebugForceCutsceneMinimumShotsProcessing")
	bDrivebyDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_FMMCDrivebyDebug")
	bScriptedAnimConversationDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_ScriptedAnimConversationDebug")
	bEntityInteriorDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_EntityInteriorDebug")
	
	bDebugAlwaysAllowMocapSkipping = GET_COMMANDLINE_PARAM_EXISTS("sc_MCAlwaysAllowMocapSkipping")
	
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_MissionControllerXMLOverride")
		stXMLOverridePath = GET_COMMANDLINE_PARAM("sc_MissionControllerXMLOverride")
	ENDIF
	
	IF !g_bForceMissionControllerMaxPlayerCount
		g_bForceMissionControllerMaxPlayerCount = GET_COMMANDLINE_PARAM_EXISTS("sc_ForceMissionControllerMaxPlayerCount")
	ENDIF
	
ENDPROC
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: XML Overrides
// ##### Description: Debug functions for saving and loading configs to and from XML
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC DUMP_XML_OVERRIDES(FMMC_XML_DUMP_FLAGS eFlags = XMLDUMP_ALL)
	
	PRINTLN("[XML_OVERRIDE] DUMP_XML_OVERRIDES - Dumping XML Override below - Flags = ", ENUM_TO_INT(eFlags))
	PRINTLN("<?xml version='1.0'?>")
	PRINTLN("<FMMCConfig>")
	
	//Continuity
	IF eFlags = XMLDUMP_ALL
	OR (ENUM_TO_INT(eFlags) & ENUM_TO_INT(XMLDUMP_CONTINUITY)) != 0
		PRINTLN("		<Continuity>")
		PRINTLN("			<ServerContinuity>")
		PRINTLN("				<iPedDeathBitset 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iPedDeathBitset[0], "\" 1=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iPedDeathBitset[1], "\" 2=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iPedDeathBitset[2], "\"/>")
		PRINTLN("				<iPedSpecialBitset 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iPedSpecialBitset[0], "\" 1=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iPedSpecialBitset[1], "\" 2=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iPedSpecialBitset[2], "\"/>")
		PRINTLN("				<iObjectDestroyedBitset 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iObjectDestroyedBitset[0], "\" 1=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iObjectDestroyedBitset[1], "\"/>")
		PRINTLN("				<iInteractablesCompleteBitset 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iInteractablesCompleteBitset[0], "\" 1=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iInteractablesCompleteBitset[1], "\" 2=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iInteractablesCompleteBitset[2], "\"/>")
		PRINTLN("				<iGenericTrackingBitset 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iGenericTrackingBitset, "\"/>")
		PRINTLN("				<iDoorsUseAltConfigBitset 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iDoorsUseAltConfigBitset[0], "\" 1=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iDoorsUseAltConfigBitset[1], "\"/>")
		PRINTLN("				<iLocationTrackingBitset 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iLocationTrackingBitset, "\"/>")
		PRINTLN("				<iVehicleDestroyedTrackingBitset 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iVehicleDestroyedTrackingBitset, "\"/>")
		PRINTLN("				<iDynoPropDestroyedTrackingBitset 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iDynoPropDestroyedTrackingBitset, "\"/>")
		PRINTLN("				<iObjectTrackingBitset 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iObjectTrackingBitset[0], "\" 1=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iObjectTrackingBitset[1], "\"/>")
		PRINTLN("				<iWeather 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iWeather, "\"/>")
		PRINTLN("				<iTimeHour 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTimeHour, "\"/>")
		PRINTLN("				<iTimeMinute 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTimeMinute, "\"/>")
		PRINTLN("				<iCurrentHeistCompletionStat 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iCurrentHeistCompletionStat, "\"/>")
		PRINTLN("				<iTotalMissionTime 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTotalMissionTime, "\"/>")
		PRINTLN("				<iStrandMissionsComplete 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iStrandMissionsComplete, "\"/>")
		PRINTLN("				<iTeamKills 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTeamKills[0], "\" 1=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTeamKills[1], "\" 2=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTeamKills[2], "\" 3=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTeamKills[3], "\"/>")
		PRINTLN("				<iTeamDeaths 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTeamDeaths[0], "\" 1=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTeamDeaths[1], "\" 2=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTeamDeaths[2], "\" 3=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTeamDeaths[3], "\"/>")
		PRINTLN("				<iVariableTeamLives 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iVariableTeamLives[0], "\" 1=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iVariableTeamLives[1], "\" 2=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iVariableTeamLives[2], "\" 3=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iVariableTeamLives[3], "\"/>")
		PRINTLN("				<iTeamHeadshots 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTeamHeadshots[0], "\" 1=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTeamHeadshots[1], "\" 2=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTeamHeadshots[2], "\" 3=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTeamHeadshots[3], "\"/>")
		PRINTLN("				<iHackFails 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iHackFails, "\"/>")
		PRINTLN("				<iGrabbedCashTotalDropped 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iGrabbedCashTotalDropped, "\"/>")
		PRINTLN("				<iGrabbedCashTotalTake 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iGrabbedCashTotalTake, "\"/>")
		PRINTLN("				<iContinuitySeed 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iContinuitySeed, "\"/>")
		PRINTLN("				<iContentSpecificContinuityBitset 0=\"", g_TransitionSessionNonResetVars.sMissionContinuityVars.iContentSpecificContinuityBitset, "\"/>")
		PRINTLN("			</ServerContinuity>")
		//Add Local Continuity here if needed
		PRINTLN("		</Continuity>")
	ENDIF
	
	//AltVars
	IF eFlags = XMLDUMP_ALL
	OR (ENUM_TO_INT(eFlags) & ENUM_TO_INT(XMLDUMP_ALTVARS)) != 0
		PRINTLN("		<AltVars>")
		PRINTLN("			<iAltVarsBS 0=\"", MC_ServerBD_4.iAltVarsBS[0], "\" 1=\"", MC_ServerBD_4.iAltVarsBS[1], "\" 2=\"", MC_ServerBD_4.iAltVarsBS[2], "\"/>")
		PRINTLN("		</AltVars>")
	ENDIF
	
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		IF eFlags = XMLDUMP_ALL
		OR (ENUM_TO_INT(eFlags) & ENUM_TO_INT(XMLDUMP_ALTVARS)) != 0 //Include in altvar dump
		OR (ENUM_TO_INT(eFlags) & ENUM_TO_INT(XMLDUMP_ISLANDHEIST)) != 0
			PRINTLN("		<IslandHeist>")
			PRINTLN("				<iCashIslandLocationsBitSet 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iCashIslandLocationsBitSet, "\"/>")
			PRINTLN("				<iWeedIslandLocationsBitSet 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iWeedIslandLocationsBitSet, "\"/>")
			PRINTLN("				<iCokeIslandLocationsBitset 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iCokeIslandLocationsBitset, "\"/>")
			PRINTLN("				<iGoldIslandLocationsBitSet 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iGoldIslandLocationsBitSet, "\"/>")
			PRINTLN("				<iCashIslandLocationsBitSetScoped 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iCashIslandLocationsBitSetScoped, "\"/>")
			PRINTLN("				<iWeedIslandLocationsBitSetScoped 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iWeedIslandLocationsBitSetScoped, "\"/>")
			PRINTLN("				<iCokeIslandLocationsBitsetScoped 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iCokeIslandLocationsBitsetScoped, "\"/>")
			PRINTLN("				<iGoldIslandLocationsBitSetScoped 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iGoldIslandLocationsBitSetScoped, "\"/>")
			PRINTLN("				<iCashCompoundLocationsBitSet 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iCashCompoundLocationsBitSet, "\"/>")
			PRINTLN("				<iWeedCompoundLocationsBitSet 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iWeedCompoundLocationsBitSet, "\"/>")
			PRINTLN("				<iCokeCompoundLocationsBitset 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iCokeCompoundLocationsBitset, "\"/>")
			PRINTLN("				<iGoldCompoundLocationsBitSet 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iGoldCompoundLocationsBitSet, "\"/>")
			PRINTLN("				<iCashCompoundLocationsBitSetScoped 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iCashCompoundLocationsBitSetScoped, "\"/>")
			PRINTLN("				<iWeedCompoundLocationsBitSetScoped 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iWeedCompoundLocationsBitSetScoped, "\"/>")
			PRINTLN("				<iCokeCompoundLocationsBitsetScoped 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iCokeCompoundLocationsBitsetScoped, "\"/>")
			PRINTLN("				<iGoldCompoundLocationsBitSetScoped 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iGoldCompoundLocationsBitSetScoped, "\"/>")
			PRINTLN("				<iPaintingsLocationsBitSet 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iPaintingsLocationsBitSet, "\"/>")
			PRINTLN("				<iPaintingsLocationsBitSetScoped 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iPaintingsLocationsBitSetScoped, "\"/>")
			PRINTLN("				<iCashValue 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iCashValue, "\"/>")
			PRINTLN("				<iWeedValue 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iWeedValue, "\"/>")
			PRINTLN("				<iCokeValue 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iCokeValue, "\"/>")
			PRINTLN("				<iGoldValue 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iGoldValue, "\"/>")
			PRINTLN("				<iPaintingsValue 0=\"", g_sHeistIslandConfig.sAdditionalLootData.iPaintingsValue, "\"/>")
			PRINTLN("		</IslandHeist>")
		ENDIF
	ENDIF
	
	PRINTLN("</FMMCConfig>")
	
	PRINTLN("[XML_OVERRIDE] DUMP_XML_OVERRIDES - End of XML Override Dump")
	
ENDPROC

PROC LOAD_XML_OVERRIDES()

	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		//Don't do this in the creator!
		EXIT
	ENDIF

	IF IS_STRING_NULL_OR_EMPTY(stXMLOverridePath)
		EXIT
	ENDIF
	
	IF NOT LOAD_XML_FILE(stXMLOverridePath)
		PRINTLN("[XML_OVERRIDE] LOAD_XML_OVERRIDES - XML file broken or missing")
		EXIT
	ENDIF
	
	INT iNodeCount = GET_NUMBER_OF_XML_NODES()
	
	IF iNodeCount <= 0
		PRINTLN("[XML_OVERRIDE] LOAD_XML_OVERRIDES - XML file lacks nodes.")
		EXIT
	ENDIF
		
	INT iNode, iNodeNameHash, i
	STRING stNodeName
	
	FMMC_MISSION_CONTINUITY_VARS sEmptyMissionContinuityVars
	FMMC_MISSION_LOCAL_CONTINUITY_VARS sEmptyMissionLocalContinuityVars

	FOR iNode = 0 TO iNodeCount - 1

		stNodeName = GET_XML_NODE_NAME()
		iNodeNameHash = GET_HASH_KEY(stNodeName)
			
		IF !bXMLOverrideActive
			
			IF iNodeNameHash = HASH("FMMCConfig")
				bXMLOverrideActive = TRUE
				PRINTLN("[CONTINUITY][XML_OVERRIDE] LOAD_XML_OVERRIDES - XML Overrides active.")
			ENDIF
			
		ELSE
		
			SWITCH iNodeNameHash
			
				CASE HASH("Continuity")
					IF NOT (HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET() OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED())
					AND NOT (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
						bXMLOverride_Continuity = TRUE
						PRINTLN("[CONTINUITY][XML_OVERRIDE] LOAD_XML_OVERRIDES - Continuity being overriden.")
					ENDIF
				BREAK
				
				CASE HASH("AltVars")
					IF g_FMMC_STRUCT.sPoolVars.eConfig != FMMC_ALT_VAR_CONFIG_NONE
						bXMLOverride_AltVars = TRUE
						PRINTLN("[AltVarsSystem][XML_OVERRIDE] LOAD_XML_OVERRIDES - Alt Vars being overriden.")
					ENDIF
				BREAK
				
				CASE HASH("IslandHeist")
					IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
						bXMLOverride_IslandHeist = TRUE
						PRINTLN("[AltVarsSystem][XML_OVERRIDE] LOAD_XML_OVERRIDES - Island Heist specifics being overriden.")
					ENDIF
				BREAK
				
			ENDSWITCH
			
			//### Continuity ###
			IF bXMLOverride_Continuity
			
				SWITCH iNodeNameHash
				
					///Server Continuity
					CASE HASH("ServerContinuity")
						g_TransitionSessionNonResetVars.sMissionContinuityVars = sEmptyMissionContinuityVars
						PRINTLN("[CONTINUITY][XML_OVERRIDE] LOAD_XML_OVERRIDES - Server Continuity being overriden.")
					BREAK
					
					CASE HASH("iPedDeathBitset")
						FOR i = 0 TO  ciFMMC_PED_BITSET_SIZE - 1
							g_TransitionSessionNonResetVars.sMissionContinuityVars.iPedDeathBitset[i] = GET_INT_FROM_XML_NODE_ATTRIBUTE(i)
						ENDFOR
					BREAK
					
					CASE HASH("iPedSpecialBitset")
						FOR i = 0 TO  ciFMMC_PED_BITSET_SIZE - 1
							g_TransitionSessionNonResetVars.sMissionContinuityVars.iPedSpecialBitset[i] = GET_INT_FROM_XML_NODE_ATTRIBUTE(i)
						ENDFOR
					BREAK
					
					CASE HASH("iObjectDestroyedBitset")
						FOR i = 0 TO  FMMC_MAX_OBJ_CONTINUITY_BITSET - 1
							g_TransitionSessionNonResetVars.sMissionContinuityVars.iObjectDestroyedBitset[i] = GET_INT_FROM_XML_NODE_ATTRIBUTE(i)
						ENDFOR
					BREAK
					
					CASE HASH("iInteractablesCompleteBitset")
						FOR i = 0 TO  FMMC_MAX_INTERACTABLE_CONTINUITY_BITSET - 1
							g_TransitionSessionNonResetVars.sMissionContinuityVars.iInteractablesCompleteBitset[i] = GET_INT_FROM_XML_NODE_ATTRIBUTE(i)
						ENDFOR
					BREAK
					
					CASE HASH("iGenericTrackingBitset")
						g_TransitionSessionNonResetVars.sMissionContinuityVars.iGenericTrackingBitset = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iDoorsUseAltConfigBitset")
						FOR i = 0 TO  FMMC_MAX_DOOR_CONTINUITY_BITSET - 1
							g_TransitionSessionNonResetVars.sMissionContinuityVars.iDoorsUseAltConfigBitset[i] = GET_INT_FROM_XML_NODE_ATTRIBUTE(i)
						ENDFOR
					BREAK
					
					CASE HASH("iLocationTrackingBitset")
						g_TransitionSessionNonResetVars.sMissionContinuityVars.iLocationTrackingBitset = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iVehicleDestroyedTrackingBitset")
						g_TransitionSessionNonResetVars.sMissionContinuityVars.iVehicleDestroyedTrackingBitset = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iDynoPropDestroyedTrackingBitset")
						g_TransitionSessionNonResetVars.sMissionContinuityVars.iDynoPropDestroyedTrackingBitset = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iObjectTrackingBitset")
						FOR i = 0 TO  FMMC_MAX_DOOR_CONTINUITY_BITSET - 1
							g_TransitionSessionNonResetVars.sMissionContinuityVars.iObjectTrackingBitset[i] = GET_INT_FROM_XML_NODE_ATTRIBUTE(i)
						ENDFOR
					BREAK
					
					CASE HASH("iWeather")
						g_TransitionSessionNonResetVars.sMissionContinuityVars.iWeather = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iTimeHour")
						g_TransitionSessionNonResetVars.sMissionContinuityVars.iTimeHour = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iTimeMinute")
						g_TransitionSessionNonResetVars.sMissionContinuityVars.iTimeMinute = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iCurrentHeistCompletionStat")
						g_TransitionSessionNonResetVars.sMissionContinuityVars.iCurrentHeistCompletionStat = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
	
					CASE HASH("iTotalMissionTime")
						g_TransitionSessionNonResetVars.sMissionContinuityVars.iTotalMissionTime = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iStrandMissionsComplete")
						g_TransitionSessionNonResetVars.sMissionContinuityVars.iStrandMissionsComplete = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iTeamKills")
						FOR i = 0 TO  FMMC_MAX_TEAMS - 1
							g_TransitionSessionNonResetVars.sMissionContinuityVars.iTeamKills[i] = GET_INT_FROM_XML_NODE_ATTRIBUTE(i)
						ENDFOR
					BREAK
					
					CASE HASH("iTeamDeaths")
						FOR i = 0 TO  FMMC_MAX_TEAMS - 1
							g_TransitionSessionNonResetVars.sMissionContinuityVars.iTeamDeaths[i] = GET_INT_FROM_XML_NODE_ATTRIBUTE(i)
						ENDFOR
					BREAK
					
					CASE HASH("iVariableTeamLives")
						FOR i = 0 TO  FMMC_MAX_TEAMS - 1
							g_TransitionSessionNonResetVars.sMissionContinuityVars.iVariableTeamLives[i] = GET_INT_FROM_XML_NODE_ATTRIBUTE(i)
						ENDFOR
					BREAK
					
					CASE HASH("iTeamHeadshots")
						FOR i = 0 TO  FMMC_MAX_TEAMS - 1
							g_TransitionSessionNonResetVars.sMissionContinuityVars.iTeamHeadshots[i] = GET_INT_FROM_XML_NODE_ATTRIBUTE(i)
						ENDFOR
					BREAK
					
					CASE HASH("iHackFails")
						g_TransitionSessionNonResetVars.sMissionContinuityVars.iHackFails = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iGrabbedCashTotalDropped")
						g_TransitionSessionNonResetVars.sMissionContinuityVars.iGrabbedCashTotalDropped = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iGrabbedCashTotalTake")
						g_TransitionSessionNonResetVars.sMissionContinuityVars.iGrabbedCashTotalTake = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iContinuitySeed")
						g_TransitionSessionNonResetVars.sMissionContinuityVars.iContinuitySeed = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iContentSpecificContinuityBitset")
						g_TransitionSessionNonResetVars.sMissionContinuityVars.iContentSpecificContinuityBitset = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					///Local Continuity
					CASE HASH("LocalContinuity")
						g_TransitionSessionNonResetVars.sMissionLocalContinuityVars = sEmptyMissionLocalContinuityVars
						PRINTLN("[CONTINUITY][XML_OVERRIDE] LOAD_XML_OVERRIDES - Local Continuity being overriden.")
					BREAK
					
					//Add Local Continuity laoding here if we have a use for it
					//Note: This will set the same data for all players so will only be 
					//		useful in single player tests or very specific circumstances.
					
				ENDSWITCH
			
			ENDIF
		
			//### Alt Vars ###
			IF bXMLOverride_AltVars
			
				SWITCH iNodeNameHash
				
					CASE HASH("iAltVarsBS")
						FOR i = 0 TO ciFMMC_ALT_VAR_MAX_BS - 1
							MC_ServerBD_4.iAltVarsBS[i] = GET_INT_FROM_XML_NODE_ATTRIBUTE(i)
						ENDFOR
					BREAK
					
				ENDSWITCH
			
			ENDIF
			
			//# Island Heist Specific Config #
			IF bXMLOverride_IslandHeist
				SWITCH iNodeNameHash

					CASE HASH("iCashIslandLocationsBitSet")
						g_sHeistIslandConfig.sAdditionalLootData.iCashIslandLocationsBitSet = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iWeedIslandLocationsBitSet")
						g_sHeistIslandConfig.sAdditionalLootData.iWeedIslandLocationsBitSet = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iCokeIslandLocationsBitset")
						g_sHeistIslandConfig.sAdditionalLootData.iCokeIslandLocationsBitset = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iGoldIslandLocationsBitSet")
						g_sHeistIslandConfig.sAdditionalLootData.iGoldIslandLocationsBitSet = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iCashIslandLocationsBitSetScoped")
						g_sHeistIslandConfig.sAdditionalLootData.iCashIslandLocationsBitSetScoped = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iWeedIslandLocationsBitSetScoped")
						g_sHeistIslandConfig.sAdditionalLootData.iWeedIslandLocationsBitSetScoped = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iCokeIslandLocationsBitsetScoped")
						g_sHeistIslandConfig.sAdditionalLootData.iCokeIslandLocationsBitsetScoped = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iGoldIslandLocationsBitSetScoped")
						g_sHeistIslandConfig.sAdditionalLootData.iGoldIslandLocationsBitSetScoped = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iCashCompoundLocationsBitSet")
						g_sHeistIslandConfig.sAdditionalLootData.iCashCompoundLocationsBitSet = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iWeedCompoundLocationsBitSet")
						g_sHeistIslandConfig.sAdditionalLootData.iWeedCompoundLocationsBitSet = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iCokeCompoundLocationsBitset")
						g_sHeistIslandConfig.sAdditionalLootData.iCokeCompoundLocationsBitset = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iGoldCompoundLocationsBitSet")
						g_sHeistIslandConfig.sAdditionalLootData.iGoldCompoundLocationsBitSet = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iCashCompoundLocationsBitSetScoped")
						g_sHeistIslandConfig.sAdditionalLootData.iCashCompoundLocationsBitSetScoped = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iWeedCompoundLocationsBitSetScoped")
						g_sHeistIslandConfig.sAdditionalLootData.iWeedCompoundLocationsBitSetScoped = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iCokeCompoundLocationsBitsetScoped")
						g_sHeistIslandConfig.sAdditionalLootData.iCokeCompoundLocationsBitsetScoped = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iGoldCompoundLocationsBitSetScoped")
						g_sHeistIslandConfig.sAdditionalLootData.iGoldCompoundLocationsBitSetScoped = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK

					CASE HASH("iPaintingsLocationsBitSet")
						g_sHeistIslandConfig.sAdditionalLootData.iPaintingsLocationsBitSet = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iPaintingsLocationsBitSetScoped")
						g_sHeistIslandConfig.sAdditionalLootData.iPaintingsLocationsBitSetScoped = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK

					CASE HASH("iCashValue")
						g_sHeistIslandConfig.sAdditionalLootData.iCashValue = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iWeedValue")
						g_sHeistIslandConfig.sAdditionalLootData.iWeedValue = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iCokeValue")
						g_sHeistIslandConfig.sAdditionalLootData.iCokeValue = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK

					CASE HASH("iGoldValue")
						g_sHeistIslandConfig.sAdditionalLootData.iGoldValue = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
					CASE HASH("iPaintingsValue")
						g_sHeistIslandConfig.sAdditionalLootData.iPaintingsValue = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
					BREAK
					
				ENDSWITCH
			ENDIF
		
		ENDIF
		
		GET_NEXT_XML_NODE()	
		
	ENDFOR
	 
	DELETE_XML_FILE()
	
ENDPROC	

PROC PROCESS_XML_OVERRIDE_TEXT()

	IF !bXMLOverrideActive
		EXIT
	ENDIF

	SET_TEXT_SCALE(0.6800, 0.6800)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	SET_TEXT_CENTRE(TRUE)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)
	SET_TEXT_EDGE(0, 0, 0, 0, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.0650, "STRING", "USED XML OVERRIDES!!")
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Bug Entered Log Dump
// ##### Description: Functions for outputting logs on bug entry
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC ON_BUG_ENTERED_DEBUG_INIT()
	REINIT_NET_TIMER(tdExtraScriptDebugOnBugEntryTimer)
	bDebugPrintCustomText = TRUE
ENDPROC

PROC ON_BUG_ENTERED_DEBUG_CLEANUP()
	RESET_NET_TIMER(tdExtraScriptDebugOnBugEntryTimer)
	bDebugPrintCustomText = GET_COMMANDLINE_PARAM_EXISTS("sc_DebugPrintCustomText")
ENDPROC

PROC PROCESS_LOGGING_ON_BUG_ENTRY()

	IF HAS_NET_TIMER_STARTED_AND_EXPIRED(tdExtraScriptDebugOnBugEntryTimer, ciEXTRA_SCRIPT_DEBUG_ON_BUG_ENTRY_TIME)
		ON_BUG_ENTERED_DEBUG_CLEANUP()
	ENDIF

	IF NOT IS_DEBUG_KEY_JUST_RELEASED(KEY_SPACE, KEYBOARD_MODIFIER_NONE, "FMMC Bug Entered Log Dump")
		EXIT
	ENDIF
	   
	PRINTLN("PROCESS_LOGGING_ON_BUG_ENTRY - FMMC BUG ENTRY SPEW")
	    
	ON_BUG_ENTERED_DEBUG_INIT()
	
	DUMP_XML_OVERRIDES()
	
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Audio --------------------------------------------------------------------------
// ##### Description: Debug functions for audio related things ----------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

FUNC STRING GET_MUSIC_STATE_NAME(INT istate)
	SWITCH istate
		CASE MUSIC_SILENT
			RETURN "MUSIC_SILENT"
		CASE MUSIC_SUSPENSE
			RETURN "MUSIC_SUSPENSE"
		CASE MUSIC_ACTION
			RETURN "MUSIC_ACTION"
		CASE MUSIC_VEHICLE_CHASE
			RETURN "MUSIC_VEHICLE_CHASE"
		CASE MUSIC_AIRBORNE
			RETURN "MUSIC_AIRBORNE"
		CASE MUSIC_COCK_SONG
			RETURN "MUSIC_COCK_SONG"
		CASE MUSIC_WAVERY
			RETURN "MUSIC_WAVERY"
		CASE MUSIC_SEA_RACE
			RETURN "MUSIC_SEA_RACE"
		CASE MUSIC_STOP
			RETURN "MUSIC_STOP"
		CASE MUSIC_FAIL
			RETURN "MUSIC_FAIL"
		CASE MUSIC_CTF_ENDING
			RETURN "MUSIC_CTF_ENDING"
		CASE MUSIC_MID_COUNTDOWN
			RETURN "MUSIC_MID_COUNTDOWN"
		CASE MUSIC_MOOD_NONE
			RETURN "MUSIC_MOOD_NONE"
		CASE MUSIC_DRIVING
			RETURN "MUSIC_DRIVING"
		CASE MUSIC_MED_INTENSITY
			RETURN "MUSIC_MED_INTENSITY"
		CASE MUSIC_SUSPENSE_LOW
			RETURN "MUSIC_SUSPENSE_LOW"
		CASE MUSIC_SUSPENSE_HIGH
			RETURN "MUSIC_SUSPENSE_HIGH"
		CASE MUSIC_SAFE_ROOM
			RETURN "MUSIC_SAFE_ROOM"
		CASE MUSIC_SAFE_ROOM_AGGRO
			RETURN "MUSIC_SAFE_ROOM_AGGRO"
		CASE MUSIC_VEHICLE_CHASE_ALT
			RETURN "MUSIC_VEHICLE_CHASE_ALT"
	ENDSWITCH
	
	RETURN "INVALID_MUSIC"
		
ENDFUNC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Cutscenes ----------------------------------------------------------------------
// ##### Description: Debug functions for cutscenes related things ------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

FUNC STRING GET_SCRIPTED_CUTSCENE_END_WARP_STATE_NAME(SCRIPTED_CUTSCENE_END_WARP_LOCAL_STATE eState)
	SWITCH eState
		CASE SCUT_EWLS_INIT 				RETURN "SCUT_EWLS_INIT"
		CASE SCUT_EWLS_WARP 				RETURN "SCUT_EWLS_WARP"
		CASE SCUT_EWLS_WAIT_FOR_VEHICLE 	RETURN "SCUT_EWLS_WAIT_FOR_VEHICLE"
		CASE SCUT_EWLS_WAIT_FOR_INTERIOR	RETURN "SCUT_EWLS_WAIT_FOR_INTERIOR"
		CASE SCUT_EWLS_END 					RETURN "SCUT_EWLS_END"
	ENDSWITCH
	RETURN "INVALID"
ENDFUNC

FUNC STRING GET_SCRIPTED_CUTSCENE_LIFT_DEBUG_NAME(eCutsceneLift eLiftType)
	
	SWITCH eLiftType
		CASE eCSLift_None									RETURN "eCSLift_None"
		CASE eCSLift_ServerFarm								RETURN "eCSLift_ServerFarm"
		CASE eCSLift_IAABase								RETURN "eCSLift_IAABase"
		CASE eCSLift_ConstructionSkyscraper_SouthA_Bottom	RETURN "eCSLift_ConstructionSkyscraper_SouthA_Bottom"
		CASE eCSLift_ConstructionSkyscraper_SouthA_Mid		RETURN "eCSLift_ConstructionSkyscraper_SouthA_Mid"
		CASE eCSLift_ConstructionSkyscraper_SouthB_Bottom	RETURN "eCSLift_ConstructionSkyscraper_SouthB_Bottom"
		CASE eCSLift_ConstructionSkyscraper_SouthB_Mid 		RETURN "eCSLift_ConstructionSkyscraper_SouthB_Mid "
		CASE eCSLift_ConstructionSkyscraper_NorthA_Bottom	RETURN "eCSLift_ConstructionSkyscraper_NorthA_Bottom"
		CASE eCSLift_ConstructionSkyscraper_NorthA_Mid		RETURN "eCSLift_ConstructionSkyscraper_NorthA_Mid"
		CASE eCSLift_ConstructionSkyscraper_NorthA_Top		RETURN "eCSLift_ConstructionSkyscraper_NorthA_Top"
		CASE eCSLift_ConstructionSkyscraper_NorthB_Bottom	RETURN "eCSLift_ConstructionSkyscraper_NorthB_Bottom"		
		CASE eCSLift_ConstructionSkyscraper_NorthB_Mid		RETURN "eCSLift_ConstructionSkyscraper_NorthB_Mid"
		CASE eCSLift_ConstructionSkyscraper_NorthB_Top		RETURN "eCSLift_ConstructionSkyscraper_NorthB_Top"
	ENDSWITCH
	
	RETURN "Unknown"
	
ENDFUNC

FUNC STRING GET_SCRIPTED_CUTSCENE_LIFT_STATE_STRING(eLiftMovementState eState)
	SWITCH eState
		CASE eLMS_WaitForStart	RETURN "eLMS_WaitForStart"
		CASE eLMS_AttachClones	RETURN "eLMS_AttachClones"
		CASE eLMS_CloseDoors	RETURN "eLMS_CloseDoors"
		CASE eLMS_MoveLiftUp	RETURN "eLMS_MoveLiftUp"
		CASE eLMS_MoveLiftDown	RETURN "eLMS_MoveLiftDown"
		CASE eLMS_OpenDoors		RETURN "eLMS_OpenDoors"
		CASE eLMS_Complete		RETURN "eLMS_Complete"
	ENDSWITCH
	RETURN "Unknown"
ENDFUNC

FUNC STRING GET_FMMC_CUTSCENE_TYPE_NAME(FMMC_CUTSCENE_TYPE cutType)
	SWITCH cutType
		CASE FMMCCUT_MOCAP			RETURN "FMMCCUT_MOCAP"
		CASE FMMCCUT_ENDMOCAP		RETURN "FMMCCUT_ENDMOCAP"
		CASE FMMCCUT_SCRIPTED		RETURN "FMMCCUT_SCRIPTED"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_MOCAP_STREAMING_STAGE_DEBUG_NAME(MOCAP_STREAMING_STAGE eState)
	SWITCH eState
		CASE MOCAP_STREAMING_STAGE_INIT							RETURN "MOCAP_STREAMING_STAGE_INIT" 
		CASE MOCAP_STREAMING_STAGE_REGISTER_ENTITIES			RETURN "MOCAP_STREAMING_STAGE_REGISTER_ENTITIES" 
		CASE MOCAP_STREAMING_STAGE_REQUEST_MODELS				RETURN "MOCAP_STREAMING_STAGE_REQUEST_MODELS" 
		CASE MOCAP_STREAMING_STAGE_ENDING						RETURN "MOCAP_STREAMING_STAGE_ENDING" 
	ENDSWITCH
	RETURN "?????"
ENDFUNC

FUNC STRING GET_SCRIPTED_CUTSCENE_STATE_PROGRESS_NAME(eFMMC_SCRIPTED_CUTSCENE_PROGRESS eState)
	SWITCH eState
		CASE	SCRIPTEDCUTPROG_INIT					RETURN	"SCRIPTEDCUTPROG_INIT (0)"
		CASE	SCRIPTEDCUTPROG_WAIT_PLAYER_SELECTION	RETURN	"SCRIPTEDCUTPROG_WAIT_PLAYER_SELECTION (1)"
		CASE	SCRIPTEDCUTPROG_FADEOUT					RETURN	"SCRIPTEDCUTPROG_FADEOUT (2)"
		CASE	SCRIPTEDCUTPROG_FADEOUT_AND_WARP		RETURN	"SCRIPTEDCUTPROG_FADEOUT_AND_WARP (2)"
		CASE	SCRIPTEDCUTPROG_WARP_PLAYERS			RETURN	"SCRIPTEDCUTPROG_WARP_PLAYERS (3)"
		CASE	SCRIPTEDCUTPROG_WARPINTRO				RETURN	"SCRIPTEDCUTPROG_WARPINTRO (4)"
		CASE	SCRIPTEDCUTPROG_PROCESS_SHOTS			RETURN	"SCRIPTEDCUTPROG_PROCESS_SHOTS (5)"
		CASE	SCRIPTEDCUTPROG_WARPEND_SHORT			RETURN	"SCRIPTEDCUTPROG_WARPEND_SHORT (6)"
		CASE	SCRIPTEDCUTPROG_WARPEND_LONG			RETURN	"SCRIPTEDCUTPROG_WARPEND_LONG (7)"
		CASE	SCRIPTEDCUTPROG_ENDING_PROCEDURES		RETURN	"SCRIPTEDCUTPROG_ENDING_PROCEDURES (8)"
		CASE	SCRIPTEDCUTPROG_CLEANUP					RETURN	"SCRIPTEDCUTPROG_CLEANUP (9)"
	ENDSWITCH
	
	RETURN "invalid"
ENDFUNC

FUNC STRING GET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS_NAME(eFMMC_MOCAP_PROGRESS eState)
	SWITCH eState
		CASE	MOCAPPROG_INIT						RETURN	"MOCAPPROG_INIT (0)"
		CASE	MOCAPPROG_WAIT_PLAYER_SELECTION		RETURN	"MOCAPPROG_WAIT_PLAYER_SELECTION (1)"
		CASE	MOCAPPROG_FADEOUT					RETURN	"MOCAPPROG_FADEOUT (2)"
		CASE	MOCAPPROG_INTRO						RETURN	"MOCAPPROG_INTRO (3)"
		CASE	MOCAPPROG_APARTMENT					RETURN	"MOCAPPROG_APARTMENT (4)"
		CASE	MOCAPPROG_PROCESS_SHOTS				RETURN	"MOCAPPROG_PROCESS_SHOTS (5)"
		CASE	MOCAPPROG_WARPEND					RETURN	"MOCAPPROG_WARPEND (6)"
		CASE 	MOCAPPROG_ENDING_PROCEDURES			RETURN	"MOCAPPROG_ENDING_PROCEDURES (7)"
		CASE	MOCAPPROG_CLEANUP					RETURN	"MOCAPPROG_CLEANUP (8)"	
	ENDSWITCH
	
	RETURN "invalid"
ENDFUNC

FUNC STRING GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS_NAME(eFMMC_MOCAP_RUNNING_PROGRESS eState)
	SWITCH eState
		CASE	MOCAPRUNNING_STAGE_0			RETURN	"MOCAPRUNNING_STAGE_0"
		CASE	MOCAPRUNNING_STAGE_1			RETURN	"MOCAPRUNNING_STAGE_1"
		CASE	MOCAPRUNNING_STAGE_2			RETURN	"MOCAPRUNNING_STAGE_2"
		CASE	MOCAPRUNNING_STAGE_3			RETURN	"MOCAPRUNNING_STAGE_3"
		CASE	MOCAPRUNNING_STAGE_4			RETURN	"MOCAPRUNNING_STAGE_4"
	ENDSWITCH
	
	RETURN "invalid"
ENDFUNC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Dialogue Trigger ---------------------------------------------------------------
// ##### Description: Debug functions for dialogue trigger related things -----------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC DEBUG_AUDIO_DIALOGUE_TRIGGERS_READOUT(INT iDialogueTriggerProgress)
	PRINTLN("[Dialogue Trigger][CDT] -----------------------------------------------------------------------------------------------")
	PRINTLN("[Dialogue Trigger][CDT] iDialogueProgress: 		", iDialogueTriggerProgress)
	PRINTLN("[Dialogue Trigger][CDT] tlBlock: 					", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].tlBlock)
	PRINTLN("[Dialogue Trigger][CDT] tlRoot: 					", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].tlRoot)
	PRINTLN("[Dialogue Trigger][CDT] tlDialogueTriggerAdditionalHelpText: 	", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].tlDialogueTriggerAdditionalHelpText)
	PRINTLN("[Dialogue Trigger][CDT] iTimeDelay: 				", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iTimeDelay)
	PRINTLN("[Dialogue Trigger][CDT] iRule: 					", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iRule)
	PRINTLN("[Dialogue Trigger][CDT] iTeam: 					", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iTeam)
	PRINTLN("[Dialogue Trigger][CDT] iVehicleTrigger: 			", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iVehicleTrigger)
	PRINTLN("[Dialogue Trigger][CDT] iPrerequisite: 			", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iPrerequisite)
	PRINTLN("[Dialogue Trigger][CDT] iPedTrigger: 				", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iPedTrigger)
	PRINTLN("[Dialogue Trigger][CDT] Played: 					", IS_BIT_SET(iLocalDialoguePlayedBS[iDialogueTriggerProgress/32], iDialogueTriggerProgress%32))
	
	PRINTLN("[Dialogue Trigger][CDT] ")
	PRINTLN("[Dialogue Trigger][CDT] Other Unlikely Causes -----------")
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iTeam >= 0
		PRINTLN("[Dialogue Trigger][CDT] iTeam Rule Limit:			", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iTeamRuleLimit[g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iTeam])
	ENDIF
	PRINTLN("[Dialogue Trigger][CDT] Trigger Later: 			", IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iDialogueBitset,iBS_DialogueTriggerLater))
	PRINTLN("[Dialogue Trigger][CDT] First Person Cam			", IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iDialogueBitset2,iBS_Dialogue2PlayOnlyInFirstPerson))
	PRINTLN("[Dialogue Trigger][CDT] Block Text if Wanted		", IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iDialogueBitset2,iBS_Dialogue2BlockIfWanted))
	PRINTLN("[Dialogue Trigger][CDT] Wanted Loss				", IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iDialogueBitset,iBS_DialogueTriggerOnWantedGain))
	PRINTLN("[Dialogue Trigger][CDT] Wanted Gain 				", IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iDialogueBitset,iBS_DialogueTriggerOnWantedLoss))
	
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Doors --------------------------------------------------------------------------
// ##### Description: Debug functions for door related things -----------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC PROCESS_DOOR_DEBUG(INT iDoor, FMMC_DOOR_CONFIGURATION& sDoorConfig, INT iConfigIndex)
	IF NOT bDoorDebug
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tlDebugText3D = "Door "
	tlDebugText3D += iDoor
	
	IF HAS_DOOR_BEEN_INITIALISED(iDoor)
		tlDebugText3D += " | LockType: "
		tlDebugText3D += sDoorConfig.iBaseLockType
		
		IF iConfigIndex > 0
			tlDebugText3D += " | Config "
			tlDebugText3D += iConfigIndex
		ENDIF
		
		IF IS_BIT_SET(iDoorExternallyLockedBS, iDoor)
			tlDebugText3D += " | Ex. Locked"
		ENDIF
		IF IS_BIT_SET(iDoorExternallyUnlockedBS, iDoor)
			tlDebugText3D += " | Ex. Unlocked"
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD_1[iLocalPart].iDoorAnimDynopropInitialisedBS, iDoor)
			tlDebugText3D += " | DynoReady"
		ENDIF
		IF IS_BIT_SET(iDoorAnimDynopropSwappedInBS, iDoor)
			tlDebugText3D += " | DynoSwapped"
		ENDIF
	ELSE
		tlDebugText3D += " | UNINITIALISED"
	ENDIF
	
	DRAW_DEBUG_TEXT(tlDebugText3D, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, 100, 0, 0)
	
	IF IS_BIT_SET(MC_playerBD_1[iLocalPart].iDoorAnimDynopropInitialisedBS, iDoor)
		IF DOES_ENTITY_EXIST(oiCachedDoorObjects[iDoor])
		OR oiCachedDoorObjects[iDoor] != null
			tlDebugText3D = "oiCachedDoorObjects["
			tlDebugText3D += iDoor
			tlDebugText3D += "]="
			tlDebugText3D += NATIVE_TO_INT(oiCachedDoorObjects[iDoor])
			DRAW_DEBUG_TEXT(tlDebugText3D, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos + <<0, 0, 0.35>>, 100, 255, 255)
		ENDIF
		
		IF DOES_ENTITY_EXIST(GET_DOOR_ANIMATION_DYNOPROP(iDoor))
			tlDebugText3D = "GET_DOOR_ANIMATION_DYNOPROP["
			tlDebugText3D += iDoor
			tlDebugText3D += "]="
			tlDebugText3D += NATIVE_TO_INT(GET_DOOR_ANIMATION_DYNOPROP(iDoor))
			DRAW_DEBUG_TEXT(tlDebugText3D, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos + <<0, 0, 0.4>>, 100, 255, 255)
		ENDIF
	ENDIF
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Ending -------------------------------------------------------------------------
// ##### Description: Debug functions for ending related things ---------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE()

	PRINTLN("START PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE -------------------------------- ")
	
	INT i
	
	PRINTLN("[PLAYER_LOOP] - PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE")
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF (g_MissionControllerserverBD_LB.sleaderboard[i].playerID <> INVALID_PLAYER_INDEX())
			PRINTLN("[LBD_NAMES] i = ", i, "  Name = ", GET_PLAYER_NAME(g_MissionControllerserverBD_LB.sleaderboard[i].playerID))
		ENDIF
		IF (g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant <> -1)
			PRINTLN("[LBD_NAMES] iParticipant = ", g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant)
			PRINTLN("[LBD_NAMES] serverBDpassed i = ", i, "  tl63_ParticipantNames = ", MC_ServerBD_2.tParticipantNames[g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant])
		ENDIF
	ENDREPEAT
	
	INT iPlayerCount = (MC_serverBD.iNumberOfLBPlayers[0] + MC_serverBD.iNumberOfLBPlayers[1] + MC_serverBD.iNumberOfLBPlayers[2] +MC_serverBD.iNumberOfLBPlayers[3])
	INT iNumLBDTeams = MC_serverBD.iNumActiveTeams
	
	PRINTLN("PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, iPlayerCount = ",iPlayerCount)
	PRINTLN("PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, iNumLBDTeams = ",iNumLBDTeams)
	PRINTLN("PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, MC_serverBD.iWinningTeam  = ",MC_serverBD.iWinningTeam)
	PRINTLN("PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, MC_serverBD.iSecondTeam   = ",MC_serverBD.iSecondTeam)
	PRINTLN("PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, MC_serverBD.iThirdTeam    = ",MC_serverBD.iThirdTeam)
	PRINTLN("PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, MC_serverBD.iLosingTeam   = ",MC_serverBD.iLosingTeam)
	
	
	IF NOT IS_SAFE_TO_DRAW_ON_SCREEN()
		PRINTLN("[2056544] PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, NOT IS_SAFE_TO_DRAW_ON_SCREEN() ")
	ENDIF
	
	IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
		PRINTLN("[2056544] PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, IS_SCRIPT_HUD_DISPLAYING ")
	ENDIF
	
	IF g_b_PopulateReady = FALSE
		PRINTLN("[2056544] PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, bCheckReady ")
	ENDIF

	IF IS_WARNING_MESSAGE_ACTIVE()
	
		PRINTLN("[2056544] PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, IS_WARNING_MESSAGE_ACTIVE() ")
	ENDIF
	
	//If we should get out because the NJVS is being skipped
	IF SHOULD_TRANSITION_SESSIONS_SKIP_LB_AND_NJVS()
		PRINTLN("[2056544] PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, SHOULD_TRANSITION_SESSIONS_SKIP_LB_AND_NJVS ")
	ENDIF

ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Events -------------------------------------------------------------------------
// ##### Description: Debug functions for event related things ----------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC PRINT_DAMAGED_EVENT_DATA(STRUCT_ENTITY_DAMAGE_EVENT &sEntityID)

	PRINTLN("[RCC MISSION]  - Mission Controller - event vars - sEntityID.VictimIndex = ",NATIVE_TO_INT(sEntityID.VictimIndex))
	PRINTLN("[RCC MISSION]  - Mission Controller - event vars - sEntityID.DamagerIndex = ",NATIVE_TO_INT(sEntityID.DamagerIndex))
	PRINTLN("[RCC MISSION]  - Mission Controller - event vars - sEntityID.Damage = ",sEntityID.Damage)
	PRINTLN("[RCC MISSION] - Mission Controller - event vars - sEntityID.VictimDestroyed = ")
	IF sEntityID.VictimDestroyed
		PRINTLN("[RCC MISSION] TRUE")
	ELSE
		PRINTLN("[RCC MISSION] FALSE")
	ENDIF
	PRINTLN("[RCC MISSION] - Mission Controller - event vars - sEntityID.WeaponUsed = ",sEntityID.WeaponUsed)
	PRINTLN("[RCC MISSION] - Mission Controller - event vars - sEntityID.VictimSpeed = ",sEntityID.VictimSpeed)
	PRINTLN("[RCC MISSION] - Mission Controller - event vars - sEntityID.DamagerSpeed = ",sEntityID.DamagerSpeed)
	
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Init --------------------------------------------------------------------------
// ##### Description: Debug functions for init related things -----------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

FUNC STRING GET_RESTART_VEH_STAGE_STRING(SPAWN_IN_PLACED_VEH_STAGE eStage)
	SWITCH(eStage)
		CASE eRESTARTVEH_INIT			RETURN	"eRESTARTVEH_INIT"
		CASE eRESTARTVEH_ENTER_VEH		RETURN	"eRESTARTVEH_ENTER_VEH"
		CASE eRESTARTVEH_FINISHED		RETURN	"eRESTARTVEH_FINISHED"
	ENDSWITCH
	RETURN "INVALID"
ENDFUNC

PROC PRINT_ALL_PARTICPANTS_ON_TEAM(INT iTeam)
		
	INT i
	PARTICIPANT_INDEX partId
	PLAYER_INDEX playerId
	INT iPlayerId
	TEXT_LABEL_63 tl63Name
	INT iPlayerCount
	
	PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - Dumping Team ", iTeam, " Info.")
	PRINTLN("[PLAYER_LOOP] - PRINT_ALL_PARTICPANTS_ON_TEAM")
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, i))
			
			partId = INT_TO_NATIVE(PARTICIPANT_INDEX, i)
			playerId = NETWORK_GET_PLAYER_INDEX(partId)
			iPlayerId = NATIVE_TO_INT(playerId)
			tl63Name = GET_PLAYER_NAME(playerId)
			
			IF (GET_PLAYER_TEAM(playerId) = iTeam)
				PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - Player Name = ", tl63Name, ", Participant ID = ", i, ", player ID = ", iPlayerId)
				PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - Player frame: ", g_debugPlayersFrameCount[NATIVE_TO_INT(playerId)])
				VECTOR vtempPos =  GET_PLAYER_COORDS(playerId)
				PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - Player coords ", vtempPos)
				iPlayerCount++
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	
	PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - total num players on team ", iTeam, " = ", iPlayerCount)
	
	IF (iPlayerCount = 0)
		PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - Team ", iTeam, " Is Empty")
	ENDIF
	
ENDPROC

PROC PROCESS_PAUSED_TIMERS()
	
	BOOL bChange
	IF bWdPauseGameTimerRag
		bWdPauseGameTimerRag = FALSE
		bChange = TRUE
	ENDIF
	
	IF bChange
		IF NOT bWdPauseGameTimer
			bWdPauseGameTimer = TRUE				
		ELIF bWdPauseGameTimer
			bWdPauseGameTimer = FALSE				
		ENDIF
		BROADCAST_FMMC_PAUSE_ALL_TIMERS(bWdPauseGameTimer)
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_9, KEYBOARD_MODIFIER_ALT, "Pause Mission Timers")
		IF NOT bWdPauseGameTimer
			bWdPauseGameTimer = TRUE
		ELIF bWdPauseGameTimer
			bWdPauseGameTimer = FALSE
		ENDIF			
		BROADCAST_FMMC_PAUSE_ALL_TIMERS(bWdPauseGameTimer)
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_0, KEYBOARD_MODIFIER_ALT, "Increment Timers")		
		BROADCAST_FMMC_EDIT_ALL_TIMERS(TRUE)
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_8, KEYBOARD_MODIFIER_ALT, "Decrement Timers")		
		BROADCAST_FMMC_EDIT_ALL_TIMERS(FALSE)
	ENDIF
	
	IF bWdPauseGameTimer
		FLOAT fTextSize = 0.55
		TEXT_LABEL_63 sText
		SET_TEXT_SCALE(fTextSize, fTextSize)
		SET_TEXT_COLOUR(75, 75, 170, 200)
		sText = "### Mission Timers Paused ###"
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.4, 0.15, "STRING", sText)
	ENDIF
	
	IF NOT bIsLocalPlayerHost
		bWdDecrementTimer = FALSE
		bWdIncrementTimer = FALSE
		EXIT
	ENDIF
	
	IF bWdIncrementTimer
		INT iTeamDebug
		FOR iTeamDebug = 0 TO FMMC_MAX_TEAMS-1
			MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug].Timer, 30000)
			MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug].Timer, 30000)
		ENDFOR
		MC_serverBD.tdMissionLengthTimer.Timer = GET_TIME_OFFSET(MC_serverBD.tdMissionLengthTimer.Timer, 30000)
		bWdIncrementTimer = FALSE
		
		PRINTLN("[LM][PROCESS_PAUSED_TIMERS] - Incrementing by 30000")
	ENDIF
	
	IF bWdDecrementTimer
		INT iTeamDebug
		FOR iTeamDebug = 0 TO FMMC_MAX_TEAMS-1
			MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug].Timer, -30000)
			MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug].Timer, -30000)
		ENDFOR
		MC_serverBD.tdMissionLengthTimer.Timer = GET_TIME_OFFSET(MC_serverBD.tdMissionLengthTimer.Timer, -30000)
		bWdDecrementTimer = FALSE
		
		PRINTLN("[LM][PROCESS_PAUSED_TIMERS] - Decrementing by 30000")
	ENDIF
	
	IF NOT bWdPauseGameTimer
		IF bWdPauseGameTimerStarted
			// Apply Cached Time.
			PRINTLN("[LM][PROCESS_PAUSED_TIMERS] - Unpausing Mission Timers.")
			bWdPauseGameTimerStarted = FALSE
			INT iTeamDebug
			FOR iTeamDebug = 0 TO FMMC_MAX_TEAMS-1
				MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug].Timer, -(MC_serverBD_3.iMultiObjectiveTimeLimitDebugCache[iTeamDebug]-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug])))
				MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug].Timer, -(MC_serverBD_3.iRuleObjectiveTimeLimitDebugCache[iTeamDebug]-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug])))
			ENDFOR
			MC_serverBD.tdMissionLengthTimer.Timer = GET_TIME_OFFSET(MC_serverBD.tdMissionLengthTimer.Timer, -(MC_serverBD_3.iMissionLengthTimerDebugCache-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer)))
		ENDIF
	ELIF bWdPauseGameTimer				
		IF NOT bWdPauseGameTimerStarted
			PRINTLN("[LM][PROCESS_PAUSED_TIMERS] - Pausing Mission Timers.")
			// Cache Time Started
			bWdPauseGameTimerStarted = TRUE
			INT iTeamDebug
			FOR iTeamDebug = 0 TO FMMC_MAX_TEAMS-1
				MC_serverBD_3.iMultiObjectiveTimeLimitDebugCache[iTeamDebug] = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug])
				MC_serverBD_3.iRuleObjectiveTimeLimitDebugCache[iTeamDebug] = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug])
			ENDFOR
			MC_serverBD_3.iMissionLengthTimerDebugCache = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer)
		ENDIF
		
		IF (GET_FRAME_COUNT() % 20) = 0
			INT iTeamDebug
			FOR iTeamDebug = 0 TO FMMC_MAX_TEAMS-1
				MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug].Timer, -(MC_serverBD_3.iMultiObjectiveTimeLimitDebugCache[iTeamDebug]-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug])))
				MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug].Timer, -(MC_serverBD_3.iRuleObjectiveTimeLimitDebugCache[iTeamDebug]-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug])))
			ENDFOR
			MC_serverBD.tdMissionLengthTimer.Timer = GET_TIME_OFFSET(MC_serverBD.tdMissionLengthTimer.Timer, -(MC_serverBD_3.iMissionLengthTimerDebugCache-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer)))
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Dumps data for the logs
/// PARAMS:
///    bStarting - Used to change some of the logs to reflect that this is called at the start
///    
///    //fmmc2020 rename and make neater as it is actually important
PROC MAINTAIN_EOM_DATA_DUMP( BOOL bStarting = FALSE )
	
	IF g_bMissionEnding
	OR bStarting
		
		IF (NOT bStarting AND NOT bDumpedF9Info)
		OR (bStarting AND NOT bDumpedPreGameF9Info)
			
			PRINTLN("*********************** [RCC MISSION] - [PLAYER DATA DUMP] - MISSION NAME: ",g_FMMC_STRUCT.tl63DebugMissionName)
			PRINTLN("*********************** [RCC MISSION] - [PLAYER DATA DUMP] - DUMP START ***********************")
			PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP]")
			
			IF bStarting
				PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - Mission start")
			ELSE
				PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - Mission end")
			ENDIF
			
			INT i
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(PARTICIPANT_ID())
				PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - local player's name: ", GET_PLAYER_NAME(LocalPlayer))
				PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - local player's player index: ", NATIVE_TO_INT(LocalPlayer))
				PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - local player's participant index: ", iLocalPart)
				PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - local player's team: ", GET_PLAYER_TEAM(LocalPlayer))
				VECTOR vTempPos = GET_PLAYER_COORDS(LocalPlayer)
				PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - local player's coords: ", vTempPos)
				PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - local player's spectator status: SCTV ",bIsSCTV," / joined as spectator ",DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer))
			ENDIF
			
			PARTICIPANT_INDEX hostPartId = NETWORK_GET_HOST_OF_THIS_SCRIPT()
			INT iHostPlayer
			INT iHostTeam
			TEXT_LABEL_63 tl63_hostName
			PLAYER_INDEX hostPlayerId
			INT iHostPart 
			
			IF hostPartID != INVALID_PARTICIPANT_INDEX()
				IF NETWORK_IS_PARTICIPANT_ACTIVE(hostPartId)
					iHostPart = NATIVE_TO_INT(hostPartId)
					hostPlayerId = NETWORK_GET_PLAYER_INDEX(hostPartId)
					iHostPlayer = NATIVE_TO_INT(hostPlayerId)
					tl63_hostName = GET_PLAYER_NAME(hostPlayerId)
					iHostTeam = GET_PLAYER_TEAM(hostPlayerId)
					
					PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - host name: ", tl63_hostName)
					PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - host player index: ", iHostPlayer)
					PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - host participant index: ", iHostPart)
					PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - host team: ", iHostTeam)
					PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - host frame: ", g_debugPlayersFrameCount[NATIVE_TO_INT(hostPlayerId)])
					PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - GET_POSIX_TIME: ", g_debugPlayersFrameCount[NATIVE_TO_INT(hostPlayerId)])
					VECTOR vTempPos = GET_PLAYER_COORDS(hostPlayerId)
					PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - host coords ", vTempPos)
					
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP] - Host participant is invalid currently. They have probably just left the game.")
			ENDIF
			
			REPEAT FMMC_MAX_TEAMS i
				PRINT_ALL_PARTICPANTS_ON_TEAM(i)
			ENDREPEAT
			
			IF NOT bStarting
				bDumpedF9Info = TRUE
			ELSE
				bDumpedPreGameF9Info = TRUE
			ENDIF
			
			PRINTLN("[RCC MISSION] - [PLAYER DATA DUMP]")
			PRINTLN("*********************** [RCC MISSION] - [PLAYER DATA DUMP] - DUMP FINISH ***********************")
			
		ENDIF
		
	ENDIF
	
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Main -------------------------------------------------------------------------
// ##### Description: Debug functions for main related things -----------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

FUNC BOOL HAS_QUICK_RESTART_DEBUG_BEEN_ACTIVATED()
	//CRTL + SHIFT + Z
	RETURN g_debugDisplayState = DEBUG_DISP_REPLAY
ENDFUNC

FUNC STRING GET_GAME_STATE_STRING( INT iGameState )
	STRING toReturn = ""
	
	SWITCH iGameState
		CASE GAME_STATE_INI
			toReturn = "GAME_STATE_INI"
		BREAK
		
		CASE GAME_STATE_CAMERA_BLEND
			toReturn = "GAME_STATE_CAMERA_BLEND"
		BREAK
		
		CASE GAME_STATE_END
			toReturn = "GAME_STATE_END"
		BREAK
		
		CASE GAME_STATE_FORCED_TRIP_SKIP
			toReturn = "GAME_STATE_FORCED_TRIP_SKIP"
		BREAK
		
		CASE GAME_STATE_INTRO_CUTSCENE
			toReturn = "GAME_STATE_INTRO_CUTSCENE"
		BREAK
		
		CASE GAME_STATE_LEAVE
			toReturn = "GAME_STATE_LEAVE"
		BREAK
		
		CASE GAME_STATE_MISSION_OVER
			toReturn = "GAME_STATE_MISSION_OVER"
		BREAK
		
		CASE GAME_STATE_RUNNING
			toReturn = "GAME_STATE_RUNNING"
		BREAK
		
		CASE GAME_STATE_SOLO_PLAY
			toReturn = "GAME_STATE_SOLO_PLAY"
		BREAK
		
		CASE GAME_STATE_TEAM_PLAY
			toReturn = "GAME_STATE_TEAM_PLAY"
		BREAK
		
		DEFAULT
			toReturn = "INVALID_GAME_STATE"
		BREAK
	ENDSWITCH
	
	RETURN toReturn
ENDFUNC


// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: ObjectMinigames ----------------------------------------------------------------
// ##### Description: Debug functions for minigame related things -------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC UPDATE_THERMITE_DEBUG(OBJECT_INDEX objMinigame)
	IF bDebugIsThermiteDebuggingEnabled
		//Allow movement of the minigame object.
		VECTOR vObjPos = GET_ENTITY_COORDS(objMinigame)
		BOOL bChangedPos = FALSE
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD8)
			vObjPos.y += 0.01
			bChangedPos = TRUE
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD2)
			vObjPos.y -= 0.01
			bChangedPos = TRUE
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD4)
			vObjPos.x -= 0.01
			bChangedPos = TRUE
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD6)
			vObjPos.x += 0.01
			bChangedPos = TRUE
		ENDIF
		
		IF bChangedPos
			SET_ENTITY_COORDS_NO_OFFSET(objMinigame, vObjPos)
			PRINTLN("New thermite pos: ", vObjPos)
		ENDIF
		
		DRAW_DEBUG_CROSS(vObjPos, 0.5, 255, 0, 0, 255)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DEBUG_LAUNCH_PHONE_HACK_MINIGAME()

	IF bLocalPlayerPedOk
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				PRINTLN("[RCC MISSION] SHOULD_DEBUG_LAUNCH_PHONE_HACK_MINIGAME - I have pressed P, part ",iLocalPart)
				bDebugCircuitHack = TRUE
			ENDIF
			IF bDebugCircuitHack
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC STRING GET_IW_STATE_NAME(INTERACT_WITH_STATE eState)
	SWITCH eState
		CASE IW_STATE_IDLE									RETURN "IW_STATE_IDLE"
		CASE IW_STATE_INIT									RETURN "IW_STATE_INIT"
		CASE IW_STATE_WALKING								RETURN "IW_STATE_WALKING"
		CASE IW_STATE_TURNING								RETURN "IW_STATE_TURNING"
		CASE IW_STATE_PREPARING_ANIMATION					RETURN "IW_STATE_PREPARING_ANIMATION"
		CASE IW_STATE_STARTING_ANIMATION					RETURN "IW_STATE_STARTING_ANIMATION"
		CASE IW_STATE_PERFORMING_ENTER_ANIMATION			RETURN "IW_STATE_PERFORMING_ENTER_ANIMATION"
		CASE IW_STATE_PERFORMING_LOOPING_ANIMATION			RETURN "IW_STATE_PERFORMING_LOOPING_ANIMATION"
		CASE IW_STATE_PERFORMING_EXIT_ANIMATION				RETURN "IW_STATE_PERFORMING_EXIT_ANIMATION"
		CASE IW_STATE_COMPLETED								RETURN "IW_STATE_COMPLETED"
		CASE IW_STATE_CLEANUP_WHEN_READY					RETURN "IW_STATE_CLEANUP_WHEN_READY"
		
		CASE IW_STATE_CUSTOM__DOWNLOADING					RETURN "IW_STATE_CUSTOM__DOWNLOADING"
		CASE IW_STATE_CUSTOM__PLANT_VAULT_DOOR_EXPLOSIVES	RETURN "IW_STATE_CUSTOM__PLANT_VAULT_DOOR_EXPLOSIVES"
		CASE IW_STATE_CUSTOM__YACHT_CUTSCENE				RETURN "IW_STATE_CUSTOM__YACHT_CUTSCENE"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_IW_ANIM_STAGE_NAME(INTERACT_WITH_ANIM_STAGE eAnimStage)
	SWITCH eAnimStage
		CASE IW_ANIM_STAGE__ENTER		RETURN "IW_ANIM_STAGE__ENTER"
		CASE IW_ANIM_STAGE__LOOPING		RETURN "IW_ANIM_STAGE__LOOPING"
		CASE IW_ANIM_STAGE__EXIT		RETURN "IW_ANIM_STAGE__EXIT"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC INTERACT_WITH_DEBUG(INTERACT_WITH_PARAMS& sInteractWithParams)
	
	TEXT_LABEL_63 tlDebugText
	
	IF NOT bInteractWithDebug
		EXIT
	ENDIF
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	IF sInteractWithVars.eInteractWith_CurrentState != IW_STATE_IDLE
		DRAW_DEBUG_TEXT_2D("Interact-With", <<0.1, 0.3, 0.5>>)
		
		tlDebugText = "State: "
		tlDebugText += GET_IW_STATE_NAME(sInteractWithVars.eInteractWith_CurrentState)
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.1, 0.335, 0.5>>)
		
		tlDebugText = "Anim Stage: "
		tlDebugText += GET_IW_ANIM_STAGE_NAME(sInteractWithVars.eInteractWith_CurrentAnimStage)
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.1, 0.35, 0.5>>)
		
		tlDebugText = "Anim Set: "
		tlDebugText += sInteractWithParams.iInteractWith_AltAnimSet
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.1, 0.465, 0.5>>)
		
		tlDebugText = "SubAnim: "
		tlDebugText += sInteractWithVars.iInteractWith_SubAnimPreset
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.1, 0.485, 0.5>>)
		
		IF sInteractWithVars.iInteractWith_NextSubAnimPreset > -1
			tlDebugText = "Queued: "
			tlDebugText += sInteractWithVars.iInteractWith_NextSubAnimPreset
			DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.3, 0.485, 0.5>>)
		ENDIF
		
		tlDebugText = "Phase: "
		tlDebugText += FLOAT_TO_STRING(GET_SYNC_SCENE_PHASE(MC_playerBD_1[iLocalPart].iInteractWithSyncedScene))
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.1, 0.5, 0.5>>, 255, 255, 255)
		
		IF IS_BIT_SET(sInteractWithParams.iInteractWithParamsBS, ciINTERACT_WITH_PARAMS_BS__IsObjectiveMinigame)
			tlDebugText = "iObjHacking: "
			tlDebugText += MC_playerBD[iLocalPart].iObjHacking
			DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.1, 0.55, 0.5>>)
			
		ELIF IS_BIT_SET(sInteractWithParams.iInteractWithParamsBS, ciINTERACT_WITH_PARAMS_BS__IsInteractable)
			tlDebugText = "sInteractWithParams.iInteractableIndex: "
			tlDebugText += sInteractWithParams.iInteractableIndex
			DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.1, 0.55, 0.5>>)
		ENDIF
	ENDIF
	
	TEXT_LABEL_63 tlEntityDebug
	
	IF IS_BIT_SET(sInteractWithParams.iInteractWithParamsBS, ciINTERACT_WITH_PARAMS_BS__IsObjectiveMinigame)
		tlEntityDebug = "Interact-With (Obj "
		tlEntityDebug += sInteractWithParams.iObjectiveObjectIndex
		tlEntityDebug += ")"
	ELSE
		tlEntityDebug = "Interact-With (Int "
		tlEntityDebug += GET_FOREGROUND_INTERACTABLE_INDEX()
		tlEntityDebug += ")"
	ENDIF
	DRAW_DEBUG_TEXT_ABOVE_ENTITY(sInteractWithParams.oiInteractWithEntity, tlEntityDebug, 0.1, 255, 255, 255)
	
	INT iSpawnedProp
	FOR iSpawnedProp = 0 TO MC_PlayerBD_1[iLocalPart].iInteractWith_NumSpawnedProps - 1
		ENTITY_INDEX eiSP = GET_INTERACT_WITH_SPAWNED_PROP(iSpawnedProp)
		
		IF DOES_ENTITY_EXIST(eiSP)
			tlEntityDebug = "SP"
			tlEntityDebug += iSpawnedProp
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(eiSP, tlEntityDebug, 0.1, 200, 0, 0, 155)
		ENDIF
	ENDFOR
	
	INT iPersistentProp
	FOR iPersistentProp = 0 TO MC_PlayerBD_1[iLocalPart].iInteractWith_NumPersistentProps - 1
		ENTITY_INDEX eiSP = GET_INTERACT_WITH_PERSISTENT_PROP(iPersistentProp)
		
		IF DOES_ENTITY_EXIST(eiSP)
			tlEntityDebug = "PP"
			tlEntityDebug += iPersistentProp
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(eiSP, tlEntityDebug, 0.1, 0, 0, 255, 155)
		ENDIF
	ENDFOR
	
	IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
		DRAW_DEBUG_LINE(GET_ENTITY_COORDS(sInteractWithParams.oiInteractWithEntity), sInteractWithVars.vInteractWith_PlayerStartPos, 255, 0, 0)
		DRAW_DEBUG_SPHERE(sInteractWithVars.vInteractWith_PlayerStartPos, 1.0, 255, 0, 0, 100)
		
		DRAW_DEBUG_LINE(GET_ENTITY_COORDS(LocalPlayerPed), sInteractWithVars.vInteractWith_PlayerStartPos, 255, 0, 0)
		
		IF IS_SYNC_SCENE_RUNNING(MC_playerBD_1[iLocalPart].iInteractWithSyncedScene)
			FLOAT fPhase = GET_SYNC_SCENE_PHASE(MC_playerBD_1[iLocalPart].iInteractWithSyncedScene)
			
			tlDebugText = "fPhase: "
			tlDebugText += FLOAT_TO_STRING(fPhase)
			DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.1, 0.4, 0.5>>)
		ENDIF
	ENDIF

ENDPROC

PROC HACKING_J_SKIP()
	ENABLE_INTERACTION_MENU()
	CLEANUP_MENU_ASSETS()
	FORCE_QUIT_PASS_HACKING_MINIGAME(sHackingData)
	
	INT i
	IF MC_serverBD.iNumObjCreated > 0	
	AND MC_serverBD.iNumObjCreated <= FMMC_MAX_NUM_OBJECTS
		FOR i = 0 TO (MC_serverBD.iNumObjCreated -1)
			MC_playerBD[iPartToUse].iObjHacked = i
		ENDFOR
	ENDIF
	CLEAR_OBJECT_HACKING()
	IF bLocalPlayerPedOk
		SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
		NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
	ENDIF
	CLEAR_PED_TASKS(LocalPlayerPed)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	TASK_CLEAR_LOOK_AT(LocalPlayerPed)
	RENDER_SCRIPT_CAMS(FALSE, TRUE)
	RELEASE_SCRIPT_AUDIO_BANK()
	PRINTLN("RELEASE_SCRIPT_AUDIO_BANK - Called from fm_mission_controller_debug.sch 1")
	iHackLimitTimer = 0
	iHackProgress = 0
	PRINTLN("[RCC MISSION] [AW_MISSION] - [RCC MISSION] HACKING_J_SKIP")
ENDPROC

FUNC STRING GET_ENTER_SAFE_COMBINATION_STAGE_NAME(ENTER_SAFE_COMBINATION_STAGE eStage)
	SWITCH eStage
		CASE ENTER_SAFE_COMBINATION_STAGE__WAITING_TO_START 			RETURN "ENTER_SAFE_COMBINATION_STAGE__WAITING_TO_START"
		CASE ENTER_SAFE_COMBINATION_STAGE__INIT 						RETURN "ENTER_SAFE_COMBINATION_STAGE__INIT"
		CASE ENTER_SAFE_COMBINATION_STAGE__STARTING 					RETURN "ENTER_SAFE_COMBINATION_STAGE__STARTING"
		CASE ENTER_SAFE_COMBINATION_STAGE__RUNNING 						RETURN "ENTER_SAFE_COMBINATION_STAGE__RUNNING"
		CASE ENTER_SAFE_COMBINATION_STAGE__OPEN_SAFE 					RETURN "ENTER_SAFE_COMBINATION_STAGE__OPEN_SAFE"
		CASE ENTER_SAFE_COMBINATION_STAGE__FAIL 						RETURN "ENTER_SAFE_COMBINATION_STAGE__FAIL"
		CASE ENTER_SAFE_COMBINATION_STAGE__EXIT 						RETURN "ENTER_SAFE_COMBINATION_STAGE__EXIT"
		CASE ENTER_SAFE_COMBINATION_STAGE__CLEANUP 						RETURN "ENTER_SAFE_COMBINATION_STAGE__CLEANUP"
		CASE ENTER_SAFE_COMBINATION_STAGE__CLEANED_UP					RETURN "ENTER_SAFE_COMBINATION_STAGE__CLEANED_UP"
  	ENDSWITCH  	
RETURN ""
ENDFUNC


// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Peds --------------------------------------------------------------------------
// ##### Description: Debug functions for ped related things -----------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

FUNC STRING GET_PED_SPOOK_REASON_FOR_DEBUG(INT iReason)
	
	SWITCH iReason
		CASE ciSPOOK_NONE										RETURN "NONE"	
		CASE ciSPOOK_PLAYER_WITH_WEAPON							RETURN "PLAYER_WITH_WEAPON"
		CASE ciSPOOK_PLAYER_BLOCKED_VEHICLE						RETURN "PLAYER_BLOCKED_VEHICLE"
		CASE ciSPOOK_PLAYER_CAUSED_EXPLOSION					RETURN "PLAYER_CAUSED_EXPLOSION"
		CASE ciSPOOK_PLAYER_POTENTIAL_RUN_OVER					RETURN "PLAYER_POTENTIAL_RUN_OVER"
		CASE ciSPOOK_PLAYER_AIMED_AT							RETURN "PLAYER_AIMED_AT"
		CASE ciSPOOK_SHOTS_FIRED								RETURN "SHOTS_FIRED"
		CASE ciSPOOK_DAMAGED									RETURN "DAMAGED"
		CASE ciSPOOK_PLAYER_TOUCHING_ME							RETURN "PLAYER_TOUCHING_ME"
		CASE ciSPOOK_PLAYER_CAR_CRASH							RETURN "PLAYER_CAR_CRASH"
		CASE ciSPOOK_PLAYER_TOUCHING_MY_VEHICLE					RETURN "TOUCHING_MY_VEHICLE"
		CASE ciSPOOK_PLAYER_HEARD								RETURN "PLAYER_HEARD"
		CASE ciSPOOK_SEEN_BODY									RETURN "SEEN_BODY"
		CASE ciSPOOK_SEEN_PLAYER_NOT_IN_SECONDARY_ANIM			RETURN "SEEN_PLAYER_NOT_IN_SECONDARY_ANIM"
		CASE ciSPOOK_VEHICLE_ATTACHED							RETURN "VEHICLE_ATTACHED"
		CASE ciSPOOK_PLAYER_GENERIC								RETURN "PLAYER_GENERIC"
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC
PROC SET_PED_IS_SPOOKED_DEBUG(INT iPed, BOOL bAlertedFromAll = FALSE, INT iReason = 0)
	IF bAlertedFromAll
		FMMC_SET_LONG_BIT(iPedBSDebugWindow_SpookedFromAll, iPed)
	ENDIF
	IF iPedSpookOrderDebugWindow[iPed] = 0
		iPedSpookOrderDebugWindow[iPed] = (iPedsSpookedDebugWindow+1)
		iPedsSpookedDebugWindow++
		iPedSpookReasonDebugWindow[iPed] = iReason
	ENDIF
ENDPROC
PROC CLEAR_PED_IS_SPOOKED_DEBUG(INT iPed)
	FMMC_CLEAR_LONG_BIT(iPedBSDebugWindow_SpookedFromAll, iPed)
	iPedSpookOrderDebugWindow[iPed] = 0	
	iPedSpookReasonDebugWindow[iPed] = 0
ENDPROC
PROC SET_PED_IS_AGGROD_DEBUG(INT iPed, BOOL bAlertedFromAll = FALSE)
	IF bAlertedFromAll
		FMMC_SET_LONG_BIT(iPedBSDebugWindow_AggroedFromAll, iPed)
	ENDIF
	IF iPedAggroOrderDebugWindow[iPed] = 0
		iPedAggroOrderDebugWindow[iPed] = (iPedsAggroedDebugWindow+1)
		iPedsAggroedDebugWindow++		
	ENDIF
ENDPROC
PROC CLEAR_PED_IS_AGGROD_DEBUG(INT iPed)	
	FMMC_CLEAR_LONG_BIT(iPedBSDebugWindow_AggroedFromAll, iPed)
	iPedAggroOrderDebugWindow[iPed] = 0	
ENDPROC


FUNC STRING GET_PED_SYNC_SCENE_STATE_NAME_PRINT(PED_SYNC_SCENE_STATE ePedSyncSceneState)
	SWITCH ePedSyncSceneState
		CASE	PED_SYNC_SCENE_STATE_INIT					RETURN "PED_SYNC_SCENE_STATE_INIT"
		CASE	PED_SYNC_SCENE_STATE_PROCESS_START			RETURN "PED_SYNC_SCENE_STATE_PROCESS_START"		
		CASE	PED_SYNC_SCENE_STATE_PROCESS_PHASE_2		RETURN "PED_SYNC_SCENE_STATE_PROCESS_PHASE_2"	
		CASE	PED_SYNC_SCENE_STATE_PROCESS_PHASE_3		RETURN "PED_SYNC_SCENE_STATE_PROCESS_PHASE_3"	
		CASE	PED_SYNC_SCENE_STATE_PROCESS_PHASE_4		RETURN "PED_SYNC_SCENE_STATE_PROCESS_PHASE_4"	
		CASE	PED_SYNC_SCENE_STATE_PROCESS_PHASE_5		RETURN "PED_SYNC_SCENE_STATE_PROCESS_PHASE_5"	
		CASE	PED_SYNC_SCENE_STATE_PROCESS_PHASE_6		RETURN "PED_SYNC_SCENE_STATE_PROCESS_PHASE_6"	
		CASE	PED_SYNC_SCENE_STATE_PROCESS_PHASE_7		RETURN "PED_SYNC_SCENE_STATE_PROCESS_PHASE_7"	
		CASE	PED_SYNC_SCENE_STATE_PROCESS_PHASE_8		RETURN "PED_SYNC_SCENE_STATE_PROCESS_PHASE_8"	
		CASE	PED_SYNC_SCENE_STATE_PROCESS_PHASE_9		RETURN "PED_SYNC_SCENE_STATE_PROCESS_PHASE_9"	
		CASE	PED_SYNC_SCENE_STATE_PROCESS_PHASE_10		RETURN "PED_SYNC_SCENE_STATE_PROCESS_PHASE_10"	
		CASE	PED_SYNC_SCENE_STATE_PROCESS_PHASE_11		RETURN "PED_SYNC_SCENE_STATE_PROCESS_PHASE_11"	
		CASE	PED_SYNC_SCENE_STATE_PROCESS_PHASE_12		RETURN "PED_SYNC_SCENE_STATE_PROCESS_PHASE_12"	
		CASE	PED_SYNC_SCENE_STATE_PROCESS_PHASE_13		RETURN "PED_SYNC_SCENE_STATE_PROCESS_PHASE_13"	
		CASE	PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT		RETURN "PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT"	
		CASE	PED_SYNC_SCENE_STATE_PROCESS_END			RETURN "PED_SYNC_SCENE_STATE_PROCESS_END"
		CASE	PED_SYNC_SCENE_STATE_CLEANUP				RETURN "PED_SYNC_SCENE_STATE_CLEANUP"			
	ENDSWITCH
	
	RETURN "Invalid"
ENDFUNC

FUNC STRING GET_DBG_NAME_PED_SYNC_SCENE_ANIM(INT iAnimation)
	SWITCH iAnimation
		CASE ciPED_IDLE_ANIM__CASINO_VINCENT_INTRO							RETURN "VINCENT_INTRO"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1						RETURN "FIGHT_BREAKOUT"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1_CROUPIER				RETURN "FIGHT_BREAKOUT"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1_PEDESTRIAN			RETURN "FIGHT_BREAKOUT"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD					RETURN "FIGHT_SECURITY_GUARD"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD_REDNECK_1			RETURN "FIGHT_SECURITY_GUARD"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD_REDNECK_2			RETURN "FIGHT_SECURITY_GUARD"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CONVERSATION						RETURN "FIGHT_CONVERSATION"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CONVERSATION_SECOND				RETURN "FIGHT_CONVERSATION"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_HEADLOCK							RETURN "FIGHT_HEADLOCK"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_HEADLOCK_VICTIM					RETURN "FIGHT_HEADLOCK"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_ARGUE							RETURN "FIGHT_ARGUE"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_ARGUE_VICTIM						RETURN "FIGHT_ARGUE"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BAR_THROW						RETURN "FIGHT_BAR_THROW"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BAR_THROW_PEDESTRIAN				RETURN "FIGHT_BAR_THROW"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SLOT_SLAM						RETURN "FIGHT_SLOT_SLAM"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SLOT_SLAM_PEDESTRIAN				RETURN "FIGHT_SLOT_SLAM"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_ATTACKER_1			RETURN "FIGHT_CHIP_STEAL"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_ATTACKER_2			RETURN "FIGHT_CHIP_STEAL"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_CROUPIER				RETURN "FIGHT_CHIP_STEAL"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_PEDESTRIAN_1			RETURN "FIGHT_CHIP_STEAL"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_PEDESTRIAN_2			RETURN "FIGHT_CHIP_STEAL"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CASHIER							RETURN "FIGHT_CASHIER"
		CASE ciPED_IDLE_ANIM__CASINO_SLOTMACHINE_ATTACK						RETURN "SLOTMACHINE_ATTACK"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SHOP_THROW						RETURN "FIGHT_SHOP_THROW"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SHOP_THROW_VICTIM				RETURN "FIGHT_SHOP_THROW"
		CASE ciPED_IDLE_ANIM__CASINO_ROULETTE_REACT_1						RETURN "ROULETTE_REACT_1"
		CASE ciPED_IDLE_ANIM__CASINO_ROULETTE_REACT_2						RETURN "ROULETTE_REACT_2"
		CASE ciPED_IDLE_ANIM__CASINO_GET_IN_CAR_TAO							RETURN "GET_IN_CAR"
		CASE ciPED_IDLE_ANIM__CASINO_GET_IN_CAR_TRANS						RETURN "GET_IN_CAR"
		CASE ciPED_IDLE_ANIM__CASINO_GET_IN_CAR_AGATHA						RETURN "GET_IN_CAR"
		CASE ciPED_IDLE_ANIM__WINDOW_SHOP_IDLE								RETURN "BROWSE_A"
		CASE ciPED_IDLE_ANIM__CAR_PARK_ATTENDANT_IDLE						RETURN "BASE"
		CASE ciPED_IDLE_ANIM__HOSTAGE_SITUATION_LEADER						RETURN "HOSTAGE_SITUATION_LEADER"
		CASE ciPED_IDLE_ANIM__HOSTAGE_SITUATION_BAR_LADY_CAPTOR				RETURN "HOSTAGE_SITUATION_BAR_LADY_CAPTOR"
		CASE ciPED_IDLE_ANIM__HOSTAGE_SITUATION_BAR_LADY_VICTIM				RETURN "HOSTAGE_SITUATION_BAR_LADY_VICTIM"
		CASE ciPED_IDLE_ANIM__HOSTAGE_SITUATION_CAPTAIN_CAPTOR				RETURN "HOSTAGE_SITUATION_CAPTAIN_CAPTOR"
		CASE ciPED_IDLE_ANIM__HOSTAGE_SITUATION_CAPTAIN_VICTIM				RETURN "HOSTAGE_SITUATION_CAPTAIN_VICTIM"
		CASE ciPED_IDLE_ANIM__PANTHER_IN_CAGE								RETURN "ciPED_IDLE_ANIM__PANTHER_IN_CAGE"
		CASE ciPED_IDLE_ANIM__ENGLISH_DAVE_IN_A_CAGE_MEDITATING				RETURN "ciPED_IDLE_ANIM__ENGLISH_DAVE_IN_A_CAGE_MEDITATING"
		CASE ciPED_IDLE_ANIM__ENGLISH_DAVE_IN_A_CAGE_PACING					RETURN "ciPED_IDLE_ANIM__ENGLISH_DAVE_IN_A_CAGE_PACING"
		CASE ciPED_IDLE_ANIM__ENGLISH_DAVE_IN_A_CAGE_PANTHER_SCARY			RETURN "ciPED_IDLE_ANIM__ENGLISH_DAVE_IN_A_CAGE_PANTHER_SCARY"
		CASE ciPED_IDLE_ANIM__SITTING_IN_CHAIR_RELAXED						RETURN "ciPED_IDLE_ANIM__SITTING_IN_CHAIR_RELAXED"
		CASE ciPED_IDLE_ANIM__SITTING_IN_CHAIR_FRUSTRATED					RETURN "ciPED_IDLE_ANIM__SITTING_IN_CHAIR_FRUSTRATED"
		CASE ciPED_IDLE_ANIM__AVI_NAS_UPLOAD_SEQUENCE						RETURN "ciPED_IDLE_ANIM__AVI_NAS_UPLOAD_SEQUENCE"
		CASE ciPED_IDLE_ANIM__RUN_AND_SHUT_DOOR_PARTY						RETURN "ciPED_IDLE_ANIM__RUN_AND_SHUT_DOOR_PARTY"
		CASE ciPED_IDLE_ANIM__ELEVATOR_PARTY								RETURN "ciPED_IDLE_ANIM__ELEVATOR_PARTY"
		CASE ciPED_IDLE_ANIM__CHOPPER_CRAWL									RETURN "ciPED_IDLE_ANIM__CHOPPER_CRAWL"
		CASE ciPED_IDLE_ANIM__GOLF_CLUB_INTERROGATION_GOLFER				RETURN "ciPED_IDLE_ANIM__GOLF_CLUB_INTERROGATION_GOLFER"
		CASE ciPED_IDLE_ANIM__GOLF_CLUB_INTERROGATION_FRANKLIN				RETURN "ciPED_IDLE_ANIM__GOLF_CLUB_INTERROGATION_FRANKLIN"
		CASE ciPED_IDLE_ANIM__PARTY_PROMOTER_DJ								RETURN "ciPED_IDLE_ANIM__PARTY_PROMOTER_DJ"
		CASE ciPED_IDLE_ANIM__STU_DRE_INSTRUCTIONS							RETURN "ciPED_IDLE_ANIM__STU_DRE_INSTRUCTIONS"
		CASE ciPED_IDLE_ANIM__INJURED_AGENT									RETURN "ciPED_IDLE_ANIM__INJURED_AGENT"
		CASE ciPED_IDLE_ANIM__JUGGERNAUT_POWERED_DOWN						RETURN "ciPED_IDLE_ANIM__JUGGERNAUT_POWERED_DOWN"
	ENDSWITCH
	
	RETURN "INVALID_POPULATE GET_DBG_NAME_PED_SYNC_SCENE_ANIM"
ENDFUNC

FUNC STRING GET_SPOOK_AT_COORD_AND_RADIUS_DEBUG_NAME(INT iSpookID)
	SWITCH iSpookID
		CASE ciSpookCoordID_GENERIC						RETURN "GENERIC"
		CASE ciSpookCoordID_THERMITE_PLACED				RETURN "THERMITE_PLACED"
		CASE ciSpookCoordID_OBJECT_INTERACT_WITH		RETURN "OBJECT_INTERACT_WITH"
		CASE ciSpookCoordID_OBJECT_MULTI_SOLUTION_LOCK	RETURN "OBJECT_MULTI_SOLUTION_LOCK"
	ENDSWITCH

	RETURN "INVALID"
ENDFUNC

/// PURPOSE: Get the text name for the state
FUNC TEXT_LABEL_15 GET_PED_STATE_NAME(INT iPedState)
	TEXT_LABEL_15 l_sDebug
	
	SWITCH iPedState

		CASE ciTASK_CHOOSE_NEW_TASK
			l_sDebug = "Idle"
		BREAK
		CASE ciTASK_GOTO_COORDS
			l_sDebug = "GoCrd"
		BREAK
		CASE ciTASK_DEFEND_AREA
			l_sDebug = "DefAr"
		BREAK
		CASE ciTASK_DEFEND_AREA_IN_VEHICLE
			l_sDebug = "DefArVeh"
		BREAK
		CASE ciTASK_WANDER
			l_sDebug = "Wndr"
		BREAK
		CASE ciTASK_GOTO_ENTITY
			l_sDebug = "GoEnt"
		BREAK

		CASE ciTASK_DEFEND_ENTITY
			l_sDebug = "DefEnt"
		BREAK
		CASE ciTASK_ATTACK_ENTITY
			l_sDebug = "AttEnt"
		BREAK
		CASE ciTASK_AIM_AT_ENTITY
			l_sDebug = "AimEnt"
		BREAK
		CASE ciTASK_TAKE_COVER
			l_sDebug = "Cvr"
		BREAK
		CASE ciTASK_GOTO_PLAYER
			l_sDebug = "GoPlr"
		BREAK

		CASE ciTASK_DEFEND_PLAYER
			l_sDebug = "DefPlr"
		BREAK
		CASE ciTASK_ATTACK_PLAYER
			l_sDebug = "AttPlr"
		BREAK
		CASE ciTASK_GENERAL_COMBAT
			l_sDebug = "Cmbt"
		BREAK
		CASE ciTASK_THROW_PROJECTILES
			l_sDebug = "Throw"
		BREAK
		CASE ciTASK_FLEE
			l_sDebug = "Flee"
		BREAK
		CASE ciTASK_FOUND_BODY_RESPONSE
			l_sDebug = "Body"
		BREAK
		CASE ciTASK_HUNT_FOR_PLAYER
			l_sDebug = "Hunt"
		BREAK
		CASE ciTASK_ABORT_GOTO_AND_FLEE
			l_sDebug = "Abrt"
		BREAK
		
		CASE ciTASK_DRIVE_TRAIN
			l_sDebug = "Drive Train"
		BREAK
		CASE ciTASK_COMBAT_TRAIN
			l_sDebug = "Combat Train"
		BREAK
		
		CASE ciTASK_COMPANION
			l_sDebug = "Companion"
		BREAK
		CASE ciTASK_CRASH_VEHICLE
			l_sDebug = "Crash Vehicle"
		BREAK
		
		DEFAULT
			l_sDebug = "Undef"
		BREAK
		
	ENDSWITCH
	
	RETURN l_sDebug
ENDFUNC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Players --------------------------------------------------------------------------
// ##### Description: Debug functions for player related things -----------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC STOP_MISSION_FAILING_AS_TEAM_SWAPS(INT iOldTeam, INT iNewTeam)
	
	INT iPlayers[FMMC_MAX_TEAMS] // iNumberOfPlayingPlayers is staggered and unreliable
	
	INT iPart
	
	PRINTLN("[PLAYER_LOOP] - STOP_MISSION_FAILING_AS_TEAM_SWAPS")
	FOR iPart = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
			IF ((NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_ANY_SPECTATOR))
				OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_HEIST_SPECTATOR))
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
				IF IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
					IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
						iPlayers[MC_playerBD[iPart].iteam]++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF iPlayers[iOldTeam] <= 1 // If the player leaving was the only person on this team
		CLEAR_BIT(iLocalBoolCheck, TEMP_TEAM_0_ACTIVE + iOldTeam)
		CLEAR_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_TEAM_0_WAS_ACTIVE + iOldTeam)
		CLEAR_BIT(MC_serverBD.iServerBitSet, SBBOOL_TEAM_0_ACTIVE + iOldTeam)
	ENDIF
	
	IF iPlayers[iNewTeam] = 0
		SET_BIT(iLocalBoolCheck, TEMP_TEAM_0_ACTIVE + iNewTeam)
		SET_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_TEAM_0_WAS_ACTIVE + iNewTeam)
		SET_BIT(MC_serverBD.iServerBitSet, SBBOOL_TEAM_0_ACTIVE + iNewTeam)
		CLEAR_BIT(MC_serverBD.iServerBitSet, SSBOOL_TEAM0_FINISHED + iNewTeam)
	ENDIF
	
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Rules -------------------------------------------------------------------------
// ##### Description: Debug functions for rule related things -----------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------
PROC PRINT_CLIENT_GAME_STATE(INT iLocalState)
	SWITCH iLocalState
		CASE GAME_STATE_INI 	 			PRINTLN("[RCC MISSION] GAME_STATE_INI  ")		 		BREAK
		CASE GAME_STATE_INTRO_CUTSCENE		PRINTLN("[RCC MISSION] GAME_STATE_INTRO_CUTSCENE ")		BREAK
		CASE GAME_STATE_FORCED_TRIP_SKIP	PRINTLN("[RCC MISSION] GAME_STATE_FORCED_TRIP_SKIP ")	BREAK	
		CASE GAME_STATE_RUNNING				PRINTLN("[RCC MISSION] GAME_STATE_RUNNING  ")			BREAK	
		CASE GAME_STATE_MISSION_OVER		PRINTLN("[RCC MISSION] GAME_STATE_MISSION_OVER  ")		BREAK	
		CASE GAME_STATE_LEAVE				PRINTLN("[RCC MISSION] GAME_STATE_LEAVE  ")				BREAK
		CASE GAME_STATE_END					PRINTLN("[RCC MISSION] GAME_STATE_END  ")				BREAK
	ENDSWITCH
ENDPROC

DEBUGONLY FUNC STRING GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(INT iMissionStage)
	SWITCH iMissionStage
		CASE CLIENT_MISSION_STAGE_NONE						RETURN "CLIENT_MISSION_STAGE_NONE"
		CASE CLIENT_MISSION_STAGE_SETUP						RETURN "CLIENT_MISSION_STAGE_SETUP"
		CASE CLIENT_MISSION_STAGE_KILL_PED					RETURN "CLIENT_MISSION_STAGE_KILL_PED"
		CASE CLIENT_MISSION_STAGE_DAMAGE_PED				RETURN "CLIENT_MISSION_STAGE_DAMAGE_PED"
		CASE CLIENT_MISSION_STAGE_DAMAGE_VEH				RETURN "CLIENT_MISSION_STAGE_DAMAGE_VEH"
		CASE CLIENT_MISSION_STAGE_DAMAGE_OBJ				RETURN "CLIENT_MISSION_STAGE_DAMAGE_OBJ"
		CASE CLIENT_MISSION_STAGE_PROTECT_PED				RETURN "CLIENT_MISSION_STAGE_PROTECT_PED"
		CASE CLIENT_MISSION_STAGE_COLLECT_PED				RETURN "CLIENT_MISSION_STAGE_COLLECT_PED"
		CASE CLIENT_MISSION_STAGE_DELIVER_PED				RETURN "CLIENT_MISSION_STAGE_DELIVER_PED"
		CASE CLIENT_MISSION_STAGE_KILL_VEH					RETURN "CLIENT_MISSION_STAGE_KILL_VEH"
		CASE CLIENT_MISSION_STAGE_PROTECT_VEH				RETURN "CLIENT_MISSION_STAGE_PROTECT_VEH"
		CASE CLIENT_MISSION_STAGE_COLLECT_VEH				RETURN "CLIENT_MISSION_STAGE_COLLECT_VEH"
		CASE CLIENT_MISSION_STAGE_DELIVER_VEH				RETURN "CLIENT_MISSION_STAGE_DELIVER_VEH"
		CASE CLIENT_MISSION_STAGE_KILL_OBJ					RETURN "CLIENT_MISSION_STAGE_KILL_OBJ"
		CASE CLIENT_MISSION_STAGE_PROTECT_OBJ				RETURN "CLIENT_MISSION_STAGE_PROTECT_OBJ"
		CASE CLIENT_MISSION_STAGE_COLLECT_OBJ				RETURN "CLIENT_MISSION_STAGE_COLLECT_OBJ"
		CASE CLIENT_MISSION_STAGE_DELIVER_OBJ				RETURN "CLIENT_MISSION_STAGE_DELIVER_OBJ"
		CASE CLIENT_MISSION_STAGE_GOTO_LOC					RETURN "CLIENT_MISSION_STAGE_GOTO_LOC"
		CASE CLIENT_MISSION_STAGE_CAPTURE_AREA  			RETURN "CLIENT_MISSION_STAGE_CAPTURE_AREA"
		CASE CLIENT_MISSION_STAGE_CAPTURE_PED   			RETURN "CLIENT_MISSION_STAGE_CAPTURE_PED"
		CASE CLIENT_MISSION_STAGE_CAPTURE_VEH   			RETURN "CLIENT_MISSION_STAGE_CAPTURE_VEH"
		CASE CLIENT_MISSION_STAGE_CAPTURE_OBJ   			RETURN "CLIENT_MISSION_STAGE_CAPTURE_OBJ"
		CASE CLIENT_MISSION_STAGE_GOTO_PED					RETURN "CLIENT_MISSION_STAGE_GOTO_PED"
		CASE CLIENT_MISSION_STAGE_GOTO_VEH					RETURN "CLIENT_MISSION_STAGE_GOTO_VEH"
		CASE CLIENT_MISSION_STAGE_GOTO_OBJ					RETURN "CLIENT_MISSION_STAGE_GOTO_OBJ"
		CASE CLIENT_MISSION_STAGE_KILL_PLAYERS  			RETURN "CLIENT_MISSION_STAGE_KILL_PLAYERS"
		CASE CLIENT_MISSION_STAGE_KILL_TEAM0   	 		 	RETURN "CLIENT_MISSION_STAGE_KILL_TEAM0"
		CASE CLIENT_MISSION_STAGE_KILL_TEAM1				RETURN "CLIENT_MISSION_STAGE_KILL_TEAM1"
		CASE CLIENT_MISSION_STAGE_KILL_TEAM2				RETURN "CLIENT_MISSION_STAGE_KILL_TEAM2"
		CASE CLIENT_MISSION_STAGE_KILL_TEAM3				RETURN "CLIENT_MISSION_STAGE_KILL_TEAM3"
		CASE CLIENT_MISSION_STAGE_PHOTO_LOC					RETURN "CLIENT_MISSION_STAGE_PHOTO_LOC"
		CASE CLIENT_MISSION_STAGE_PHOTO_PED   				RETURN "CLIENT_MISSION_STAGE_PHOTO_PED"
		CASE CLIENT_MISSION_STAGE_PHOTO_VEH    				RETURN "CLIENT_MISSION_STAGE_PHOTO_VEH"
		CASE CLIENT_MISSION_STAGE_PHOTO_OBJ    				RETURN "CLIENT_MISSION_STAGE_PHOTO_OBJ"
		CASE CLIENT_MISSION_STAGE_HACK_COMP    	 			RETURN "CLIENT_MISSION_STAGE_HACK_COMP"
		CASE CLIENT_MISSION_STAGE_INTERACT_WITH				RETURN "CLIENT_MISSION_STAGE_INTERACT_WITH"
		CASE CLIENT_MISSION_STAGE_GET_MASK       			RETURN "CLIENT_MISSION_STAGE_GET_MASK"
		CASE CLIENT_MISSION_STAGE_LOOT_THRESHOLD      		RETURN "CLIENT_MISSION_STAGE_LOOT_THRESHOLD"
		CASE CLIENT_MISSION_STAGE_HOLDING_RULE      		RETURN "CLIENT_MISSION_STAGE_HOLDING_RULE"
		CASE CLIENT_MISSION_STAGE_SCRIPTED_CUTSCENE 		RETURN "CLIENT_MISSION_STAGE_SCRIPTED_CUTSCENE"
		CASE CLIENT_MISSION_STAGE_LEAVE_LOC     			RETURN "CLIENT_MISSION_STAGE_LEAVE_LOC"
		CASE CLIENT_MISSION_STAGE_PHONE_BULLSHARK 		 	RETURN "CLIENT_MISSION_STAGE_PHONE_BULLSHARK"
		CASE CLIENT_MISSION_STAGE_PHONE_AIRSTRIKE 		 	RETURN "CLIENT_MISSION_STAGE_PHONE_AIRSTRIKE"
		CASE CLIENT_MISSION_STAGE_HACK_DRILL      		 	RETURN "CLIENT_MISSION_STAGE_HACK_DRILL"
		CASE CLIENT_MISSION_STAGE_GOTO_ANY_PLAYER			RETURN "CLIENT_MISSION_STAGE_GOTO_ANY_PLAYER"
		CASE CLIENT_MISSION_STAGE_GOTO_TEAM0				RETURN "CLIENT_MISSION_STAGE_GOTO_TEAM0"
		CASE CLIENT_MISSION_STAGE_GOTO_TEAM1				RETURN "CLIENT_MISSION_STAGE_GOTO_TEAM1"
		CASE CLIENT_MISSION_STAGE_GOTO_TEAM2				RETURN "CLIENT_MISSION_STAGE_GOTO_TEAM2"
		CASE CLIENT_MISSION_STAGE_GOTO_TEAM3				RETURN "CLIENT_MISSION_STAGE_GOTO_TEAM3"
		CASE CLIENT_MISSION_STAGE_HACKING				 	RETURN "CLIENT_MISSION_STAGE_HACKING"
		CASE CLIENT_MISSION_STAGE_RESULTS				 	RETURN "CLIENT_MISSION_STAGE_RESULTS"
		CASE CLIENT_MISSION_STAGE_TERMINATE_DELAY 			RETURN "CLIENT_MISSION_STAGE_TERMINATE_DELAY"
		CASE CLIENT_MISSION_STAGE_HACK_FINGERPRINT_KEYPAD 	RETURN "CLIENT_MISSION_STAGE_HACK_FINGERPRINT_KEYPAD"
		CASE CLIENT_MISSION_STAGE_GLASS_CUTTING 			RETURN "CLIENT_MISSION_STAGE_GLASS_CUTTING"
		CASE CLIENT_MISSION_STAGE_UNDERWATER_WELDING		RETURN "CLIENT_MISSION_STAGE_UNDERWATER_WELDING"
		CASE CLIENT_MISSION_STAGE_ENTER_SAFE_COMBINATION	RETURN "CLIENT_MISSION_STAGE_ENTER_SAFE_COMBINATION"		
		CASE CLIENT_MISSION_STAGE_INTERROGATION 			RETURN "CLIENT_MISSION_STAGE_INTERROGATION"
		CASE CLIENT_MISSION_STAGE_POINTS_THRESHOLD	 		RETURN "CLIENT_MISSION_STAGE_POINTS_THRESHOLD"
		CASE CLIENT_MISSION_STAGE_PED_GOTO_LOCATION	 		RETURN "CLIENT_MISSION_STAGE_PED_GOTO_LOCATION"
		CASE CLIENT_MISSION_STAGE_BEAMHACK_VEH	 			RETURN "CLIENT_MISSION_STAGE_BEAMHACK_VEH"
		CASE CLIENT_MISSION_STAGE_INSERT_FUSE	 			RETURN "CLIENT_MISSION_STAGE_INSERT_FUSE"
		CASE CLIENT_MISSION_STAGE_END				 		RETURN "CLIENT_MISSION_STAGE_END"
	ENDSWITCH
	
	RETURN "INVALID CLIENT MISSION STAGE"
ENDFUNC

PROC PRINT_SERVER_GAME_STATE(INT iLocalServerState)
	SWITCH iLocalServerState
		CASE GAME_STATE_INI 	 	PRINTLN("[RCC MISSION] GAME_STATE_INI  ")		 	BREAK
		CASE GAME_STATE_RUNNING		PRINTLN("[RCC MISSION] GAME_STATE_RUNNING  ")		BREAK
		CASE GAME_STATE_LEAVE		PRINTLN("[RCC MISSION] GAME_STATE_LEAVE  ")			BREAK
		CASE GAME_STATE_END			PRINTLN("[RCC MISSION] GAME_STATE_END  ")			BREAK
	ENDSWITCH
ENDPROC

PROC BROADCAST_DEBUG_OBJECTIVE_SKIP_FOR_TEAM(INT iTeam, INT iObjective, INT iNewTeam = -1)
	
	PRINTLN("BROADCAST_DEBUG_OBJECTIVE_SKIP_FOR_TEAM - called... iteam: ",iTeam, " skip to objective: ", iObjective)
	
	SCRIPT_EVENT_DATA_DEBUG_OBJECTIVE_SKIP_FOR_TEAM Event
	
	Event.Details.Type = SCRIPT_EVENT_DEBUG_OBJECTIVE_SKIP_FOR_TEAM
	Event.Details.FromPlayerIndex = LocalPlayer
	Event.iTeam = iTeam
	Event.iSkipObjective = iObjective
	Event.iNewTeam = iNewTeam
	
	
	INT iPlayerFlags = ALL_PLAYERS(TRUE) // any player could be the host 
	IF NOT (iPlayerFlags = 0)
		
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_DEBUG_OBJECTIVE_SKIP_FOR_TEAM - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF	

ENDPROC

FUNC BOOL HAS_ANY_PLAYER_DEBUG_SKIPPED_FOR_CUTSCENE()

	IF IS_DEBUG_KEY_PRESSED(KEY_J, KEYBOARD_MODIFIER_NONE, "SkipCut")
		RETURN TRUE
	ENDIF
	
	INT iPart = -1
	WHILE DO_PARTICIPANT_LOOP(iPart, DPLF_FILL_PLAYER_IDS)
	
		IF IS_BIT_SET(MC_serverBD.ijSkipBitset, iPart)			
		OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_PRESSED_J)		
			RETURN TRUE
		ENDIF
		
	ENDWHILE
	
	RETURN FALSE	
ENDFUNC

PROC MAINTAIN_SERVER_J_SKIP(INT iparticipant)
	
	INT i
	PARTICIPANT_INDEX temppart
	PLAYER_INDEX tempPlayer
	PED_INDEX tempPed 

	IF IS_BIT_SET(MC_playerBD[iparticipant].iClientBitSet, PBBOOL_PRESSED_J)
		IF NOT IS_BIT_SET(MC_serverBD.ijSkipBitset,iparticipant)
			INT iclientstage = GET_MC_CLIENT_MISSION_STAGE(iparticipant)
			temppart = INT_TO_PARTICIPANTINDEX(iparticipant)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(temppart)
				tempPlayer = NETWORK_GET_PLAYER_INDEX(temppart)
			ENDIF
			
			IF IS_NET_PLAYER_OK(tempPlayer)
				tempPed = GET_PLAYER_PED(tempPlayer)
				SWITCH iclientstage
				
					CASE CLIENT_MISSION_STAGE_CAPTURE_AREA
						FOR i = 0 TO (FMMC_MAX_GO_TO_LOCATIONS-1)
							IF MC_serverBD_4.iGotoLocationDataPriority[i][MC_playerBD[iparticipant].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iparticipant].iteam]							
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(tempPed, GET_LOCATION_VECTOR(i)) < 10
									IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iAreaTimer[i][MC_playerBD[iparticipant].iteam])
										INT iMod
										iMod = -1*(MC_serverBD.iGotoLocationDataTakeoverTime[MC_playerBD[iparticipant].iteam][MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iparticipant].iteam]])
										MC_serverBD.iAreaTimer[i][MC_playerBD[iparticipant].iteam] += iMod
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
					BREAK

					CASE CLIENT_MISSION_STAGE_CAPTURE_PED
					
						FOR i = 0 TO (FMMC_MAX_PEDS-1)
							IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
								IF MC_serverBD_4.iPedPriority[i][MC_playerBD[iparticipant].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iparticipant].iteam]
									IF GET_DISTANCE_BETWEEN_ENTITIES(tempPed, NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])) < 10
										IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlPedTimer[i])
											INT iMod
											iMod = -1*(2*MC_serverBD.iPedTakeoverTime[i][MC_playerBD[iparticipant].iteam])
											MC_serverBD_1.iControlPedTimer[i] += iMod
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						
					BREAK

					
					CASE CLIENT_MISSION_STAGE_CAPTURE_VEH
					
						FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
							IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
								IF MC_serverBD_4.iVehPriority[i][MC_playerBD[iparticipant].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iparticipant].iteam]
									IF GET_DISTANCE_BETWEEN_ENTITIES(tempPed, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])) < 10
										IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[i])
											INT iMod 
											iMod = -1*(2*MC_serverBD.iVehTakeoverTime[i][MC_playerBD[iparticipant].iteam])
											MC_serverBD_1.iControlVehTimer[i] += iMod											
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						
					BREAK

					CASE CLIENT_MISSION_STAGE_CAPTURE_OBJ
					
						FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
							IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(i))
								IF NOT IS_ENTITY_DEAD(NET_TO_OBJ(GET_OBJECT_NET_ID(i)))
									IF MC_serverBD_4.iObjPriority[i][MC_playerBD[iparticipant].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iparticipant].iteam]
										IF GET_DISTANCE_BETWEEN_ENTITIES(tempPed, NET_TO_OBJ(GET_OBJECT_NET_ID(i))) < 10
											IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlObjTimer[i])												
												INT iMod 
												iMod = -1*(2*MC_serverBD.iObjTakeoverTime[i][MC_playerBD[iparticipant].iteam])
												MC_serverBD_1.iControlObjTimer[i] += iMod		
											ENDIF
										ENDIF
									ENDIF 
								ENDIF
							ENDIF
						ENDFOR			

					BREAK
					
					CASE CLIENT_MISSION_STAGE_PROTECT_VEH
					CASE CLIENT_MISSION_STAGE_PROTECT_PED
					CASE CLIENT_MISSION_STAGE_PROTECT_OBJ
					
						IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iparticipant].iteam] < FMMC_MAX_RULES
							IF IS_OBJECTIVE_TIMER_RUNNING(MC_playerBD[iparticipant].iteam)
								MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iparticipant].iteam].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iparticipant].iteam].Timer,(-1* GET_OBJECTIVE_TIME_LIMIT_FOR_RULE(MC_playerBD[iParticipant].iteam, GET_TEAM_CURRENT_RULE(MC_playerBD[iparticipant].iteam))))
							ENDIF
						ENDIF
							
					BREAK
					
					CASE CLIENT_MISSION_STAGE_POINTS_THRESHOLD
						//Add required points
						IF MC_serverBD.iPlayerRuleLimit[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iPartToUse].iteam]][MC_playerBD[iparticipant].iTeam] > 0	
							INT iCurrentScore 
							iCurrentScore = MC_serverBD.iTeamScore[MC_playerBD[iparticipant].iTeam]
							
							INT iRequiredScore
							iRequiredScore = MC_serverBD.iPlayerRuleLimit[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iPartToUse].iteam]][MC_playerBD[iparticipant].iTeam]
							
							REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_BY(iRequiredScore - iCurrentScore)
							PRINTLN("[MAINTAIN_SERVER_J_SKIP] CLIENT_MISSION_STAGE_POINTS_THRESHOLD - incrementing player points by: ", iRequiredScore - iCurrentScore)
						ENDIF						
					BREAK				

				ENDSWITCH	
			ENDIF
			SET_BIT(MC_serverBD.ijSkipBitset,iparticipant)
		ENDIF
	ELSE
		CLEAR_BIT(MC_serverBD.ijSkipBitset,iparticipant)
	ENDIF

ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Zones --------------------------------------------------------------------------
// ##### Description: Debug functions for zone related things -----------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC DRAW_ZONE_TIMER_DEBUG(INT iZone)
	
	TEXT_LABEL_63 tlDebugText3D = "Zone Timer"
	
	IF HAS_ZONE_TIMER_COMPLETED(iZone)
		tlDebugText3D += " | Completed"
		
	ELIF IS_ZONE_TIMER_RUNNING(iZone)
		tlDebugText3D += " | Time: "
		tlDebugText3D += g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneTimer_EnableTime - GET_ZONE_TIMER_ELAPSED_TIME(iZone)
	
	ELSE
		tlDebugText3D += " | Not Started"
	ENDIF
	
	VECTOR vCoords = GET_ZONE_RUNTIME_CENTRE(iZone)
	vCoords -= <<0.0, 0.0, 0.5>>
	DRAW_DEBUG_TEXT(tlDebugText3D, vCoords)
ENDPROC

PROC DRAW_ZONE_DEBUG(INT iZone)
	
	IF NOT bZoneDebug
		EXIT
	ENDIF
	
	BOOL bDrawThisZone = DOES_ZONE_EXIST(iZone) OR HAS_NET_TIMER_STARTED(stZoneCreationDelayTimers[iZone])
	
	IF GET_FMMC_ZONE_TYPE(iZone) = ciFMMC_ZONE_TYPE__BOUNDS
		IF NOT IS_THIS_ZONE_SET_AS_BOUNDS_ON_CURRENT_RULE(iZone, 0)
		AND NOT IS_THIS_ZONE_SET_AS_BOUNDS_ON_CURRENT_RULE(iZone, 1)
		AND NOT IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD6)
			bDrawThisZone = FALSE
		ENDIF
	ENDIF
	
	IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD4)
		// Force to draw
		bDrawThisZone = TRUE
	ENDIF
	
	IF bDrawThisZone
		TEXT_LABEL_63 tlDebugText3D = "Z"
		tlDebugText3D += iZone
		
		INT iR = 255, iG = 255, iB = 255, iA = 180
		
		tlDebugText3D += " / Type: "
		tlDebugText3D += GET_FMMC_ZONE_TYPE(iZone)
		
		IF IS_ZONE_TRIGGERING(iZone)
			iR = 0
			iG = 0
			iB = 0
			iA = 255
			tlDebugText3D += " (T) "
		
		ELIF IS_ZONE_TRIGGER_BLOCKED(iZone)
			iR = 0
			iG = 0
			iB = 0
			iA = 255
			tlDebugText3D += " (BLOCKED) "
			
		ELIF HAS_NET_TIMER_STARTED(stZoneCreationDelayTimers[iZone])
			iR = 0
			iG = 0
			iB = 255
			iA = 255
			tlDebugText3D += " / Delayed"
			
		ENDIF
		
		IF iCurrentSpawnAreaZone = iZone
			tlDebugText3D += " (SPAWN AREA) "
			
			IF USING_SPAWN_AREA_ZONE_RESPAWN_HEADING()
				SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[iCurrentSpawnAreaZone].iZoneSpawnFacingType
					CASE ciFMMC_ZONE_SPAWN_FACING__DEFAULT
						tlDebugText3D += " (DEFAULT HEADING) "
					BREAK
					
					CASE ciFMMC_ZONE_SPAWN_FACING__RULE_ENTITY
						tlDebugText3D += " (FACING OBJECTIVE) "
					BREAK
					
					CASE ciFMMC_ZONE_SPAWN_FACING__CHOSEN_ENTITY
						tlDebugText3D += " (FACING ENTITY "
						tlDebugText3D += g_FMMC_STRUCT_ENTITIES.sPlacedZones[iCurrentSpawnAreaZone].iSpawnFacingEntityType
						tlDebugText3D += "/"
						tlDebugText3D += g_FMMC_STRUCT_ENTITIES.sPlacedZones[iCurrentSpawnAreaZone].iSpawnFacingEntityIndex
						tlDebugText3D += ") "
					BREAK
					
					CASE ciFMMC_ZONE_SPAWN_FACING__CHOSEN_POSITION
						tlDebugText3D += " (FACING POS "
						tlDebugText3D += FLOAT_TO_STRING(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iCurrentSpawnAreaZone].vZoneSpawnFacingPosition.x)
						tlDebugText3D += ","
						tlDebugText3D += FLOAT_TO_STRING(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iCurrentSpawnAreaZone].vZoneSpawnFacingPosition.y)
						tlDebugText3D += ","
						tlDebugText3D += FLOAT_TO_STRING(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iCurrentSpawnAreaZone].vZoneSpawnFacingPosition.z)
						tlDebugText3D += ") "
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
		
		IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
			DRAW_FMMC_ZONE(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone], GET_ZONE_RUNTIME_POSITION(iZone, 0), GET_ZONE_RUNTIME_POSITION(iZone, 1), GET_ZONE_RUNTIME_RADIUS(iZone), iA * 0.2, DEFAULT, TRUE)
			DRAW_DEBUG_TEXT(tlDebugText3D, GET_ZONE_RUNTIME_CENTRE(iZone) + <<0,0,-2>>, iR, iG, iB, iA)
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_REQUIRE_ALL_PLAYERS)
			OR IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD6)
				tlDebugText3D = "Players: "
				tlDebugText3D += iZoneTriggeringPlayerCount[iZone]
				DRAW_DEBUG_TEXT(tlDebugText3D, GET_ZONE_RUNTIME_CENTRE(iZone) + <<0,0,2.4>>, iR, 0, 0, 255)
			ENDIF
			
			IF bZoneTimerDebug
			AND DOES_ZONE_USE_A_ZONE_TIMER(iZone)
				DRAW_ZONE_TIMER_DEBUG(iZone)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HEALTH_DRAIN_ZONE_DEBUG(INT iZone)

	TEXT_LABEL_63 tlDebugText
	
	IF IS_ZONE_TRIGGERING(iZone)
		tlDebugText = "fHealthDrainZoneDamageBuildup = "
		tlDebugText += FLOAT_TO_STRING(fHealthDrainZoneDamageBuildup)
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.3, 0.7, 0.0>>)
	ENDIF
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Bounds -------------------------------------------------------------------------
// ##### Description: Debug functions for Rule Bounds -------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------
PROC DRAW_BOUNDS_DEBUG(INT iBoundsIndex, RULE_BOUNDS_STRUCT& sBounds)
	
	IF NOT bBoundsDebug
		EXIT
	ENDIF
	
	IF NOT IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD6)
		EXIT
	ENDIF
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	IF sBounds.iRuleBoundsType = ciRULE_BOUNDS_TYPE__OFF
	AND IS_LONG_BITSET_EMPTY(sBounds.iZonesToUseBS)
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tlDebugTxt = ""
	FLOAT fSeparation = 0.22
	FLOAT fStartingX = 0.05 + (TO_FLOAT(iBoundsIndex) * fSeparation)
	FLOAT fCurrentY = 0.1
	
	fCurrentY += 0.05
	tlDebugTxt = "RuleBounds "
	tlDebugTxt += iBoundsIndex
	DRAW_DEBUG_TEXT_2D(tlDebugTxt, (<<fStartingX, fCurrentY, 0.0>>))
	
	IF IS_LOCAL_PLAYER_OUT_OF_BOUNDS(iBoundsIndex)
		fCurrentY += 0.04
		tlDebugTxt = "OUT OF BOUNDS"
		DRAW_DEBUG_TEXT_2D(tlDebugTxt, (<<fStartingX, fCurrentY, 0.0>>), 255, 0, 0)
	ENDIF
	
	IF IS_BIT_SET(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsBS, ciRULE_BOUNDS_BS__BLOCK_BOUNDS_EVERY_FRAME_PROCESSING)
		fCurrentY += 0.04
		tlDebugTxt = "EVERY FRAME PROCESSING IS BLOCKED"
		DRAW_DEBUG_TEXT_2D(tlDebugTxt, (<<fStartingX, fCurrentY, 0.0>>), 255, 0, 255)
	ENDIF
	
	IF IS_BIT_SET(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsBS, ciRULE_BOUNDS_BS__WAIT_FOR_NEXT_COMPLETED_ZONE_STAGGERED_LOOP)
		IF IS_BIT_SET(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsBS, ciRULE_BOUNDS_BS__WAITING_FOR_ZONE_STAGGERED_LOOP_LOOP_POINT)
			fCurrentY += 0.05
			tlDebugTxt = "Waiting to reach staggered Zone loop point ("
			tlDebugTxt += sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFullZoneStaggeredLoop_LoopPoint
			tlDebugTxt += ")"
			DRAW_DEBUG_TEXT_2D(tlDebugTxt, (<<fStartingX, fCurrentY, 0.0>>), 255, 255)
		ELSE
			fCurrentY += 0.05
			tlDebugTxt = "Waiting for end of staggered loop"
			DRAW_DEBUG_TEXT_2D(tlDebugTxt, (<<fStartingX, fCurrentY, 0.0>>), 255, 255)
		ENDIF
		
		EXIT
	ENDIF
	
	fCurrentY += 0.075
	tlDebugTxt = "Type: "
	tlDebugTxt += sBounds.iRuleBoundsType
	DRAW_DEBUG_TEXT_2D(tlDebugTxt, (<<fStartingX, fCurrentY, 0.0>>))
	
	fCurrentY += 0.05
	tlDebugTxt = "Active Zones: "
	tlDebugTxt += sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsActiveZoneCount
	DRAW_DEBUG_TEXT_2D(tlDebugTxt, (<<fStartingX, fCurrentY, 0.0>>))
	
	fCurrentY += 0.035
	tlDebugTxt = "Triggered Zones: "
	tlDebugTxt += sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsCurrentTriggeredZoneCount
	DRAW_DEBUG_TEXT_2D(tlDebugTxt, (<<fStartingX, fCurrentY, 0.0>>))
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Vehicle ------------------------------------------------------------------------
// ##### Description: Debug functions for vehicle related things --------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

FUNC INT GET_VEHICLE_INT_FROM_INDEX(VEHICLE_INDEX vehToCheck)
	PRINTLN("[LM][GET_VEHICLE_INT_FROM_INDEX] - starting check")
	INT i = 0
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
		AND NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]) = vehToCheck
			PRINTLN("[LM][GET_VEHICLE_INT_FROM_INDEX] - Returning i: ", i)
			RETURN i
		ENDIF
	ENDREPEAT
	PRINTLN("[LM][GET_VEHICLE_INT_FROM_INDEX] - Returning -1 ... Failed? ")
	RETURN -1
ENDFUNC

PROC DEBUG_PRINT_PLACED_VEHICLE_HEALTH_INFO(INT iVeh)
	IF bPlacedMissionVehiclesDebugHealth
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
		VEHICLE_INDEX tempV = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
		IF DOES_ENTITY_EXIST(tempV)
		AND IS_ENTITY_ALIVE(tempV)
			PRINTLN("[LM][DEBUG_PRINT_PLACED_VEHICLE_HEALTH_INFO]   iVeh: ", iVeh,
			"     SCRIPT_AUTOMOBILE_", NETWORK_ENTITY_GET_OBJECT_ID(tempV),
			"     Model: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(tempV)),
			"     Body Health: ", GET_VEHICLE_BODY_HEALTH(tempV),
			"     Body Health [Creator]: ", MC_GET_VEHICLE_MAX_BODY_HEALTH(iveh, GET_TOTAL_STARTING_PLAYERS()),
			"     UI Health %: ", MC_GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(tempV, iVeh, GET_TOTAL_STARTING_PLAYERS(), GET_ENTITY_MODEL(tempV) = INT_TO_ENUM(MODEL_NAMES, HASH("TRAILERLARGE")) OR GET_ENTITY_MODEL(tempV) = INT_TO_ENUM(MODEL_NAMES, HASH("TRAILERSMALL2"))))
			
			PRINTLN("[LM][DEBUG_PRINT_PLACED_VEHICLE_HEALTH_INFO]   iVeh: ", iVeh,
			"     (Continued) - Engine Health: ", GET_VEHICLE_ENGINE_HEALTH(tempV),
			"     Engine Health [Creator]: ", MC_GET_FMMC_VEHICLE_MAX_ENGINE_HEALTH(iVeh, GET_TOTAL_STARTING_PLAYERS()),
			"     Petrol Tank: ", GET_VEHICLE_PETROL_TANK_HEALTH(tempV),
			"     Petrol Tank [Creator]: ", MC_GET_FMMC_VEHICLE_MAX_PETROL_TANK_HEALTH(iveh, GET_TOTAL_STARTING_PLAYERS()))
			
			IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(tempV))
				PRINTLN("[LM][DEBUG_PRINT_PLACED_VEHICLE_HEALTH_INFO]   iVeh: ", iVeh,
				"     (Continued HELI) - Rotor Tail Health: ", GET_HELI_TAIL_ROTOR_HEALTH(tempV),
				"     Rotor Tail Health [Creator]: ", MC_GET_FMMC_HELI_MAX_TAIL_ROTOR_HEALTH(iVeh, GET_TOTAL_STARTING_PLAYERS()),
				"     Heli Rotor Main: ", GET_HELI_MAIN_ROTOR_HEALTH(tempV),
				"     Heli Rotor Main [Creator]: ", MC_GET_FMMC_HELI_MAX_MAIN_ROTOR_HEALTH(iveh, GET_TOTAL_STARTING_PLAYERS()))
			ENDIF
			
			PRINTLN("[LM][DEBUG_PRINT_PLACED_VEHICLE_HEALTH_INFO]   (Continued) Using ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR Override: ", IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR))
		ELSE
			PRINTLN("[LM][DEBUG_PRINT_PLACED_VEHICLE_HEALTH_INFO] iVeh: ", iVeh, " Does not exist or is dead")
		ENDIF
	ENDIF
ENDPROC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Process Debug main functions	 --------------------------------------------------
// ##### Description: Main debug function calls -------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC DEBUG_SET_LOCAL_PLAYER_LAST_SPAWNED_COORD_AND_HEADING(VECTOR vCoord, FLOAT fHead)
	vDebugSpawnLastSpawnLocation = vCoord
	fDebugSpawnLastSpawnHeading = fHead
ENDPROC
PROC DEBUG_SET_LOCAL_PLAYER_LAST_DEATH_COORD_AND_HEADING(VECTOR vCoord, FLOAT fHead)
	vDebugSpawnLastDeathLocation = vCoord
	fDebugSpawnLastDeathHeading = fHead
ENDPROC

PROC DEBUG_RESET_LOCAL_PLAYER_USING_SPAWN_POINTS()
	INT i = 0	
	FOR i = 0 TO FMMC_MAX_TEAM_SPAWN_POINT_BITSET-1
		iDebugPreviousSpawnPointsActiveBS[i] = iDebugSpawnPointsActiveBS[i]
		iDebugSpawnPointsActiveBS[i] = 0
	ENDFOR
ENDPROC

PROC DEBUG_SET_LOCAL_PLAYER_USING_SPAWN_POINT(INT iSpawnPoint)
	FMMC_SET_LONG_BIT(iDebugSpawnPointsActiveBS, iSpawnPoint)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Misc ------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC STRING GET_ENTITY_TYPE_PRINT_TAG(INT iEntityType)
	SWITCH iEntityType
		CASE CREATION_TYPE_PEDS
			RETURN "Ped"
		BREAK
		
		CASE CREATION_TYPE_VEHICLES
			RETURN "Vehicle"
		BREAK
		
		CASE CREATION_TYPE_OBJECTS		
			RETURN "Object"
		BREAK
		
		CASE CREATION_TYPE_GOTO_LOC
			RETURN "Location"
		BREAK
		
		CASE CREATION_TYPE_INTERACTABLE	
			RETURN "Interactable"
		BREAK
		
		CASE CREATION_TYPE_END_CUTSCENE	
			RETURN "EndCutscene"
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC BOOL HAS_RAG_PLACEMENT_VECTOR_CHANGED()
	RETURN NOT ARE_VECTORS_EQUAL(vRagPlacementVector, vRagPlacementVector_Prev)
ENDFUNC

FUNC BOOL HAS_RAG_ROTATION_VECTOR_CHANGED()
	RETURN NOT ARE_VECTORS_EQUAL(vRagRotationVector, vRagRotationVector_Prev)
ENDFUNC

FUNC BOOL HAS_RAG_PLACEMENT_OR_ROTATION_VECTOR_CHANGED()
	RETURN (HAS_RAG_PLACEMENT_VECTOR_CHANGED() OR HAS_RAG_ROTATION_VECTOR_CHANGED())
ENDFUNC
#ENDIF
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: MissionVariation Debug Functions --------------------------------------------------------------------------------------------------------------------------------
// ##### Description: All of the Debug functions called by the mission variations system.																		------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

