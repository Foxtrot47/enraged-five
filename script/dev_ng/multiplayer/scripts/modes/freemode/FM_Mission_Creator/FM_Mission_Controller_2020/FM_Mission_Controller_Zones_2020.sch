// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Zones -------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: All logic relating to zones                                                                                                                                                                                                                                                             
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020                                                                                       
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Include_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: GENERAL HELPER FUNCTIONS ----------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions to be used throughout the Zones system -----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL DOES_ZONE_REQUIRE_EVERY_FRAME_TRIGGER_CHECKS(INT iZone)

	SWITCH GET_FMMC_ZONE_TYPE(iZone)
		CASE ciFMMC_ZONE_TYPE__ANTI_ZONE
		CASE ciFMMC_ZONE_TYPE__METAL_DETECTOR
		CASE ciFMMC_ZONE_TYPE__BLIP_ZONE
		CASE ciFMMC_ZONE_TYPE__RACE_PIT_ZONE
		CASE ciFMMC_ZONE_TYPE__CLEAR_ENTITY
		CASE ciFMMC_ZONE_TYPE__LIVES_DEPLETION_ZONE
		CASE ciFMMC_ZONE_TYPE__RULE_ENTITY_PREREQ_ZONE
		CASE ciFMMC_ZONE_TYPE__GIVE_AND_MAINTAIN_WANTED_LEVEL
			RETURN TRUE
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__OBJECTIVE_TEXT_OVERRIDE
			RETURN IS_SCREEN_FADING_IN()
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__AIR_DEFENCE
			// If this is an Air Defence Zone with a very low time limit, it's probably quite urgent and the player entering should be detected ASAP
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue < 5000
		BREAK
	ENDSWITCH
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_USE_AS_SPAWN_AREA)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ZONE_TYPE_STATIC(INT iZone)
	SWITCH GET_FMMC_ZONE_TYPE(iZone)
		CASE ciFMMC_ZONE_TYPE__CLEAR_PEDS
		CASE ciFMMC_ZONE_TYPE__CLEAR_VEHICLES
		CASE ciFMMC_ZONE_TYPE__CLEAR_ALL
		CASE ciFMMC_ZONE_TYPE__CLEAR_ALL_AND_PV
		CASE ciFMMC_ZONE_TYPE__CLEAR_COVER
		CASE ciFMMC_ZONE_TYPE__NO_PLAYER_SPAWNING
		CASE ciFMMC_ZONE_TYPE__GPS
		CASE ciFMMC_ZONE_TYPE__DISPATCH_SPAWN_BLOCK
		CASE ciFMMC_ZONE_TYPE__ACTIVATE_ROADS
		CASE ciFMMC_ZONE_TYPE__WATER_DAMPENING
		CASE ciFMMC_ZONE_TYPE__SPAWN_PROTECTION
		CASE ciFMMC_ZONE_TYPE__NAV_BLOCKING
		CASE ciFMMC_ZONE_TYPE__AIR_DEFENCE
		CASE ciFMMC_ZONE_TYPE__BLOCK_ROAD_NODES
		CASE ciFMMC_ZONE_TYPE__CLEAR_ENTITY
		CASE ciFMMC_ZONE_TYPE__BLOCK_LADDER_CLIMBING
		CASE ciFMMC_ZONE_TYPE__ENABLE_ROAD_NODES
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_ZONE_REQUIRE_CONTINUOUS_PROCESSING(INT iZone)

	IF DOES_ZONE_REQUIRE_EVERY_FRAME_TRIGGER_CHECKS(iZone)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_DRAW_MARKER)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_AREA_BLIP_ZONE)
		RETURN TRUE
	ENDIF
	
	SWITCH GET_FMMC_ZONE_TYPE(iZone)
		CASE ciFMMC_ZONE_TYPE__CLEAR_PEDS
		CASE ciFMMC_ZONE_TYPE__CLEAR_VEHICLES
		CASE ciFMMC_ZONE_TYPE__CLEAR_ALL
		CASE ciFMMC_ZONE_TYPE__CLEAR_ALL_AND_PV
		CASE ciFMMC_ZONE_TYPE__CLEAR_COVER
		CASE ciFMMC_ZONE_TYPE__NO_PLAYER_SPAWNING
		CASE ciFMMC_ZONE_TYPE__GPS
		CASE ciFMMC_ZONE_TYPE__DISPATCH_SPAWN_BLOCK
		CASE ciFMMC_ZONE_TYPE__ACTIVATE_ROADS
		CASE ciFMMC_ZONE_TYPE__NAV_BLOCKING
			RETURN FALSE
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__WATER_DAMPENING
			IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue2 = 1.0
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_ZONE_BE_REMOVED_DUE_TO_AGGRO(INT iZone)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_REMOVE_ZONE_ON_AGGRO)
		IF HAS_ANY_TEAM_TRIGGERED_AGGRO(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAggroIndexBS_Entity_Zone)
			PRINTLN("[Zones][Zone ", iZone, "] SHOULD_ZONE_BE_REMOVED_DUE_TO_AGGRO | Returning TRUE for zone ", iZone)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Modify the size of ocean waves in a FMMC_ZONE_WATER_DAMPENING
PROC SELECT_NEXT_WAVE_DAMPING_SCALER()
	
	INT iZone = -1
	INT iZonesToConsiderBS
	
	INT i
	FLOAT fSmallestLength = 99999
	VECTOR vLengthTemp
	FLOAT fTempLength
	INT iTempDampingZoneBitset = iWaveDampingZoneBitset
	
	FOR i = 0 TO FMMC_MAX_NUM_ZONES - 1
		IF IS_BIT_SET(iTempDampingZoneBitset, i)
			//pick ones of smallest length
			//from those, pick one w closest centrepoint
			
			vLengthTemp = g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1] - g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0]
			vLengthTemp.z = 0
			
			fTempLength = VMAG(vLengthTemp)
			
			IF fTempLength < (fSmallestLength * 0.9)
				fSmallestLength = fTempLength
				iZonesToConsiderBS = 0
				SET_BIT(iZonesToConsiderBS, i)
				iZone = i
			ELIF (fTempLength >= (fSmallestLength * 0.9))
			AND (fTempLength <= (fSmallestLength * 1.1))
				SET_BIT(iZonesToConsiderBS, i)
				iZone = -1
			ENDIF
			
			CLEAR_BIT(iTempDampingZoneBitset, i)
		ENDIF
		
		IF iTempDampingZoneBitset = 0
			i = FMMC_MAX_NUM_ZONES //Break out, we've checked all the zones we need to!
		ENDIF
	ENDFOR
	
	IF iZone = -1
	AND iZonesToConsiderBS != 0
		//Have more than one zone to consider, find the one with the closest centrepoint:
		
		VECTOR vPlayer = GET_ENTITY_COORDS(PlayerPedToUse)
		
		fSmallestLength = 999999999
		
		FOR i = 0 TO FMMC_MAX_NUM_ZONES - 1
			IF IS_BIT_SET(iZonesToConsiderBS, i)
				
				fTempLength = VDIST2(vPlayer, ((g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0] + g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1]) * 0.5))
				
				IF fTempLength < fSmallestLength
					fSmallestLength = fTempLength
					iZone = i
				ENDIF
				
				CLEAR_BIT(iZonesToConsiderBS, i)
			ENDIF
			
			IF iZonesToConsiderBS = 0
				i = FMMC_MAX_NUM_ZONES //Break out, we've checked all the zones we need to!
			ENDIF
		ENDFOR
		
	ENDIF
	
	PRINTLN("[RCC MISSION] SELECT_NEXT_WAVE_DAMPING_SCALER - next zone found is ", iZone)
	
	IF iZone != -1
		IF GET_DEEP_OCEAN_SCALER() != g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue2
			PRINTLN("[RCC MISSION] SELECT_NEXT_WAVE_DAMPING_SCALER - calling SET_DEEP_OCEAN_SCALER for ", iZone," with value ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue2)
			SET_DEEP_OCEAN_SCALER(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue2)
		ENDIF
	ELSE
		IF GET_DEEP_OCEAN_SCALER() != 1.0
			PRINTLN("[RCC MISSION] SELECT_NEXT_WAVE_DAMPING_SCALER - Calling RESET_DEEP_OCEAN_SCALER")
			RESET_DEEP_OCEAN_SCALER()
		ENDIF
	ENDIF
	
ENDPROC

FUNC VECTOR GET_ZONE_COORDS(INT iZone, INT iPositionIndex)

	VECTOR vPosition = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[iPositionIndex]
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__ANGLED_AREA
	OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
		SWITCH iPositionIndex
			CASE 0
				vPosition.z -= 100.0
			BREAK
			CASE 1
				vPosition.z += 500.0
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN vPosition
ENDFUNC

FUNC BOOL SHOULD_ZONE_BE_BLIPPED_ON_CREATION(INT iZone)
	SWITCH GET_FMMC_ZONE_TYPE(iZone)
		CASE ciFMMC_ZONE_TYPE__AIR_DEFENCE
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: ZONE TRIGGERING -------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to the triggering of zones - checking what triggers them and whether they should currently be triggered etc. ----------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL CAN_THIS_ZONE_TYPE_BE_TRIGGERED_BY_SPECTATORS(INT iType)
	SWITCH iType
		CASE ciFMMC_ZONE_TYPE__ALTIMETER_SYSTEM_ZONE
		CASE ciFMMC_ZONE_TYPE__BOUNDS
		CASE ciFMMC_ZONE_TYPE__ANTI_ZONE
		CASE ciFMMC_ZONE_TYPE__OBJECTIVE_TEXT_OVERRIDE
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ZONE_CONNECTED_ANTI_ZONE_CLEAR(INT iZone)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iConnectedAntiZone = ciFMMC_CONNECTED_ANTI_ZONE__NONE
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iConnectedAntiZone = ciFMMC_CONNECTED_ANTI_ZONE__ANY
		// If it's set to care about ANY Anti-Zones, being within any Anti-Zone is enough to prevent this one from triggering
		RETURN (NOT IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__ANTI_ZONE)) OR (GET_TRIGGERING_ZONE_INDEX(ciFMMC_ZONE_TYPE__ANTI_ZONE) = iZone)
	ENDIF
	
	// Below this point, the connected Anti-Zone is a particular Zone
	
	IF NOT DOES_ZONE_EXIST(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iConnectedAntiZone)
		RETURN TRUE
	ENDIF
	
	RETURN IS_BIT_SET(iAntiZone_ConnectedZonesCanTriggerBS, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iConnectedAntiZone)
ENDFUNC

PROC SET_ANTI_ZONE_VALID_TO_TRIGGER_CONNECTED_ZONES(INT iAntiZone)
	IF NOT IS_BIT_SET(iAntiZone_ConnectedZonesCanTriggerBS, iAntiZone)
		SET_BIT(iAntiZone_ConnectedZonesCanTriggerBS, iAntiZone)
		PRINTLN("[Zones][Zone ", iAntiZone, "][AntiZones] Anti-Zone ", iAntiZone, " is giving the all-clear!")
	ENDIF
ENDPROC
PROC SET_ANTI_ZONE_NOT_VALID_TO_TRIGGER_CONNECTED_ZONES(INT iAntiZone)
	IF IS_BIT_SET(iAntiZone_ConnectedZonesCanTriggerBS, iAntiZone)
		CLEAR_BIT(iAntiZone_ConnectedZonesCanTriggerBS, iAntiZone)
		PRINTLN("[Zones][Zone ", iAntiZone, "][AntiZones] Anti-Zone ", iAntiZone, " is not clear!")
	ENDIF
ENDPROC

FUNC BOOL IS_ZONE_TRIGGERABLE(INT iZone)

	IF NOT DOES_ZONE_EXIST(iZone)
		//PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "] IS_ZONE_TRIGGERABLE | Zone ", iZone, " doesn't exist right now")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_ENABLE_BASED_ON_ZONE_TIMER)
		IF HAS_ZONE_TIMER_COMPLETED(iZone)
			//PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "][ZoneTimer_SPAM] IS_ZONE_TRIGGERABLE | Zone ", iZone, "'s zone timer has been completed")
		ELSE
			//PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "][ZoneTimer_SPAM] IS_ZONE_TRIGGERABLE | Disabling zone ", iZone, " this frame due to ciFMMC_ZONEBS_ENABLE_BASED_ON_ZONE_TIMER")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF iSpectatorTarget > -1
	OR IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
	OR bIsAnySpectator
		IF NOT CAN_THIS_ZONE_TYPE_BE_TRIGGERED_BY_SPECTATORS(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType)
			//PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "] IS_ZONE_TRIGGERABLE | Zone ", iZone, " can't be triggered by me because I'm a spectator")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_ONLY_TRIGGER_ZONE_REMOTELY)
		IF NOT IS_BIT_SET(iZoneBS_TriggeredByLinkedZone, iZone)
			//PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "] IS_ZONE_TRIGGERABLE | Zone ", iZone, " can only be triggered remotely, and that isn't currently happening")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iConnectedAntiZone != ciFMMC_CONNECTED_ANTI_ZONE__NONE
		IF NOT IS_ZONE_CONNECTED_ANTI_ZONE_CLEAR(iZone)
			PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "] IS_ZONE_TRIGGERABLE | Zone ", iZone, " hasn't been given the all-clear by connected Anti-Zone(s), Zone ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iConnectedAntiZone)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iActivateOnPreReq != ciPREREQ_None
		IF NOT IS_PREREQUISITE_COMPLETED(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iActivateOnPreReq)
			PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "] IS_ZONE_TRIGGERABLE | Zone ", iZone, " cannot trigger yet because the player needs to complete PreReq ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iActivateOnPreReq)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iDeactivateOnPreReq != ciPREREQ_None
		IF IS_PREREQUISITE_COMPLETED(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iDeactivateOnPreReq)
			PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "] IS_ZONE_TRIGGERABLE | Zone ", iZone, " cannot trigger right now because of PreReq ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iDeactivateOnPreReq)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_DISABLE_IF_LINKED_ENTITY_NOT_CARRIED)
	AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkType = ciENTITY_TYPE_OBJECT
		OBJECT_INDEX oiObject = GET_FMMC_ENTITY_OBJECT(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkIndex)
		BOOL bObjectIsCarried = FALSE
		
		IF DOES_ENTITY_EXIST(oiObject)
			IF IS_ENTITY_ATTACHED_TO_ANY_PED(oiObject)
				ENTITY_INDEX eiCarrier = GET_ENTITY_ATTACHED_TO(oiObject)
				
				IF DOES_ENTITY_EXIST(eiCarrier)
					PED_INDEX piCarrier = GET_PED_INDEX_FROM_ENTITY_INDEX(eiCarrier)
					
					IF IS_PED_A_PLAYER(piCarrier)
						// The object is currently carried by a player!
						bObjectIsCarried = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bObjectIsCarried
			PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "] IS_ZONE_TRIGGERABLE | Zone ", iZone, " cannot trigger - linked object isn't carried")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_bMissionEnding
		//PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "] IS_ZONE_TRIGGERABLE | Zone ", iZone, " cannot trigger right now because the mission is ending")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_THIS_ZONE_BE_TRIGGERED(INT iZone)
	
	BOOL bExtraDebug = FALSE
	#IF IS_DEBUG_BUILD
	IF bZoneDebug
		bExtraDebug = IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
	ENDIF
	#ENDIF
	
	IF NOT IS_ZONE_TRIGGERABLE(iZone)
		IF bExtraDebug	PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "] SHOULD_THIS_ZONE_BE_TRIGGERED | Zone ", iZone, " is currently not triggerable")	ENDIF
		RETURN FALSE
	ENDIF
	
	IF DOES_THIS_ZONE_REQUIRE_PLAYER_TO_BE_ALIVE_TO_TRIGGER(iZone)
		IF IS_ENTITY_DEAD(playerPedToUse)
			IF bExtraDebug	PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "] SHOULD_THIS_ZONE_BE_TRIGGERED | Zone ", iZone, " returning FALSE early due to playerPedToUse being dead and this Zone needing the player alive")	ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iZoneBS_TriggeredByLinkedZone, iZone)
		IF bExtraDebug	PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "] SHOULD_THIS_ZONE_BE_TRIGGERED | Zone ", iZone, " is currently triggered remotely")	ENDIF
		RETURN TRUE
	ENDIF
	
	IF GET_FMMC_ZONE_TYPE(iZone) = ciFMMC_ZONE_TYPE__RULE_ENTITY_PREREQ_ZONE
		IF IS_BIT_SET(MC_serverBD.iEntitiesInPreReqZoneBS, iZone)
			IF bExtraDebug	PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "] SHOULD_THIS_ZONE_BE_TRIGGERED | Zone ", iZone, " is currently triggered by entities")	ENDIF
			RETURN TRUE
		ELSE
			IF bExtraDebug	PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "] SHOULD_THIS_ZONE_BE_TRIGGERED | Zone ", iZone, " is not currently triggered by entities")	ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_THIS_ZONE_TYPE_ALWAYS_TRIGGERED_WHEN_TRIGGERABLE(GET_FMMC_ZONE_TYPE(iZone))
		IF bExtraDebug	PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "] SHOULD_THIS_ZONE_BE_TRIGGERED | Zone ", iZone, " is always triggered")	ENDIF
		RETURN TRUE
	ENDIF
	
	// SEE ALSO: IS_ZONE_TRIGGER_BLOCKED for last minute blockers that allow iZoneIsTriggeredBS to be set but not perform the Zone triggered functionality //
	
	IF IS_THIS_ZONE_TRIGGERED_WHEN_LOCAL_PLAYER_IS_INSIDE(iZone)	
		IF IS_ENTITY_IN_FMMC_ZONE(playerPedToUse, iZone, DEFAULT, FALSE)
			IF bExtraDebug	PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "] SHOULD_THIS_ZONE_BE_TRIGGERED | Zone ", iZone, " is currently triggered by the player")	ENDIF
			RETURN TRUE
		ELSE
			IF bExtraDebug	PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "] SHOULD_THIS_ZONE_BE_TRIGGERED | Zone ", iZone, " doesn't have the local player inside it")	ENDIF
		ENDIF
	
	ELIF IS_THIS_ZONE_TRIGGERED_WHEN_LOCAL_PLAYER_IS_OUTSIDE(iZone)
		IF NOT IS_ENTITY_IN_FMMC_ZONE(playerPedToUse, iZone, DEFAULT, FALSE)
			IF bExtraDebug	PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "] SHOULD_THIS_ZONE_BE_TRIGGERED | Zone ", iZone, " is currently triggered by the player being outside of it")	ENDIF
			RETURN TRUE
		ELSE
			IF bExtraDebug	PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "] SHOULD_THIS_ZONE_BE_TRIGGERED | Zone ", iZone, " doesn't have the local player inside it, and is configured to trigger when the player is outside only")	ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// This is where to put functionality that should only happen once, at the moment a zone starts triggering (usually the moment the player enters it)
PROC PROCESS_ZONE_ENTER(INT iZone)

	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType
		CASE ciFMMC_ZONE_TYPE__BLOCK_RUNNING
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "][Zone ", iZone, "] PROCESS_ZONE_ENTER | Player entered Blocked Running zone - Setting fBlockedRunningZoneSpeed to ", fBlockedRunningZoneSpeed)
			fBlockedRunningZoneSpeed = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue
			fMaxRunningZoneSpeed     = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue2
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__BLOCK_WEAPONS
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_ENTER | Player entered Blocked weapons zone - Will be Blocking weapon wheel and setting player to unarmed")
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__SET_AIR_DRAG_MULTIPLIER
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_ENTER | Player has entered Air Drag Multiplier zone - Setting air drag multiplier to ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue)
			SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(LocalPlayer, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue)
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__BLOCK_VTOL_TOGGLE
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_ENTER | Player has entered Block VTOL zone")
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__BLOCK_ELECTROCUTION
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_ENTER | Setting PCF_DontActivateRagdollFromElectrocution to TRUE.")
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DontActivateRagdollFromElectrocution, TRUE)
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__BLOCK_LADDER_CLIMBING
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_ENTER | Setting PCF_DisableLadderClimbing to TRUE.")
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableLadderClimbing, TRUE)
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__BLOCK_DISPATCH
			IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_DISPATCHOFF) 
				SET_BIT(iLocalBoolCheck31, LBOOL30_REAPPLY_DISPATCH_SERVICES_ON_ZONE_EXIT)
			ENDIF
			SET_DISPATCH_SERVICES(FALSE, TRUE)
			SET_BIT(iLocalBoolCheck31, LBOOL30_DISPATCH_BLOCKED_BY_ZONE)
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_ENTER |  Setting dispatch block.")
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__BLIP_ZONE
			//Is the hide PV blip option enabled by the user?			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_HIDE_PV_BLIP)
				SET_BIT(MPGlobalsAmbience.iNGAmbBitSet,  iNGABI_PLAYER_IN_HIDE_PV_BLIP_AREA)
				PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_ENTER |  Setting bit iNGABI_PLAYER_IN_HIDE_PV_BLIP_AREA in MPGlobalsAmbience.iNGAmbBitSet.")
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__CLEAR_ENTITY
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_ENTER | Player entered CLEAR_ENTITY zone.")
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__PREREQ_ZONE
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_ENTER | Player entered Prereq Zone! Completing prereq ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneValue3, " now!")
			SET_PREREQUISITE_COMPLETED(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneValue3, NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_SET_PREREQ_ONLY_FOR_LOCAL_PLAYER))
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__RULE_ENTITY_PREREQ_ZONE
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_ENTER | Rule Entity Prereq Zone Triggered! Completing prereq ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneValue3, " now!")
			SET_PREREQUISITE_COMPLETED(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneValue3)
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__CHECK_AGGRO_PREREQS
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_ENTER | Player entered Prereq Aggro Index Checking Zone!")
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_HostCheckAggroPrereqs)
		BREAK		
		
	ENDSWITCH
ENDPROC

//Zones that should process their PROCESS_ZONE_EXIT functionality when left, even if the player is in another of the same type
FUNC BOOL SHOULD_ZONE_TYPE_PROCESS_EXIT_FOR_EACH_ZONE(INT iType)
	IF iType = ciFMMC_ZONE_TYPE__RULE_ENTITY_PREREQ_ZONE
	OR iType = ciFMMC_ZONE_TYPE__PREREQ_ZONE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

// This is where to put functionality that should only happen once, at the moment a zone stops triggering (usually the moment the player exits it)
PROC PROCESS_ZONE_EXIT(INT iZone)

	IF IS_ZONE_TYPE_TRIGGERING(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType)
	AND NOT SHOULD_ZONE_TYPE_PROCESS_EXIT_FOR_EACH_ZONE(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType)
		// We're still in another
		PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_EXIT | We're still in another Zone of type ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType, " so not processing the Zone exit just yet")
		EXIT
	ENDIF
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType
		CASE ciFMMC_ZONE_TYPE__BLOCK_RUNNING
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_EXIT | Player has left Blocked Running zone - Re-allowing run action")
			fBlockedRunningZoneSpeed = 0.0
			fMaxRunningZoneSpeed     = 0.0
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__BLOCK_WEAPONS
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_EXIT | Player has left Blocked Weapons zone - Re-allowing weapons")
			IF wtWeaponBlockZonePreviousWeapon != WEAPONTYPE_INVALID
				BOOL bForceInHand
				bForceInHand = (NOT IS_ENTITY_IN_WATER(LocalPlayerPed) AND NOT IS_PED_FALLING(LocalPlayerPed))
				SET_CURRENT_PED_WEAPON(LocalPlayerPed, wtWeaponBlockZonePreviousWeapon, bForceInHand)
				wtWeaponBlockZonePreviousWeapon = WEAPONTYPE_INVALID
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__AUTO_PARACHUTE
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_EXIT | Player has left auto parachute zone - Clearing LBOOL19_AUTO_PARACHUTE_ZONE_ACTIVATED now")
			CLEAR_BIT(iLocalBoolCheck19, LBOOL19_AUTO_PARACHUTE_ZONE_ACTIVATED)
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__SET_AIR_DRAG_MULTIPLIER
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_EXIT | Player has left Air Drag Multiplier zone - Re-setting air drag multiplier")
			SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(LocalPlayer, 1.0)
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__BLOCK_VTOL_TOGGLE
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_EXIT | Player has left Block VTOL zone")
			
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				VEHICLE_INDEX viMyVeh
				viMyVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(viMyVeh)
					IF DOES_THIS_MODEL_HAVE_VERTICAL_FLIGHT_MODE(GET_ENTITY_MODEL(viMyVeh))
						SET_DISABLE_VERTICAL_FLIGHT_MODE_TRANSITION(viMyVeh, FALSE)
					ENDIF
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_EXIT | Calling: SET_DISABLE_VERTICAL_FLIGHT_MODE_TRANSITION False.")
				ENDIF
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__BLOCK_ELECTROCUTION
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_EXIT | Calling: PCF_DontActivateRagdollFromElectrocution FALSE.")
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DontActivateRagdollFromElectrocution, FALSE)
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__BLOCK_LADDER_CLIMBING
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_EXIT | Calling: PCF_DisableLadderClimbing FALSE.")
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableLadderClimbing, FALSE)
		BREAK 
		
		CASE ciFMMC_ZONE_TYPE__AIR_DEFENCE
			REMOVE_NAMED_PTFX_ASSET("scr_apartment_mp")
			CLEAR_BIT(iLocalBoolCheck31, LBOOL31_USED_AIR_DEFENCE_PTFX)
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_EXIT | Removing scr_apartment_mp")
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__BLOCK_DISPATCH
			CLEAR_BIT(iLocalBoolCheck31, LBOOL30_DISPATCH_BLOCKED_BY_ZONE)
				
			BOOL bHelisOff
			bHelisOff = FALSE
			IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
				bHelisOff = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]], ciBS_RULE2_DISABLE_HELI_RESPONSE)
			ENDIF
			
			SET_DISPATCH_SERVICES(IS_BIT_SET(iLocalBoolCheck31, LBOOL30_REAPPLY_DISPATCH_SERVICES_ON_ZONE_EXIT), bHelisOff)
			
			CLEAR_BIT(iLocalBoolCheck31, LBOOL30_REAPPLY_DISPATCH_SERVICES_ON_ZONE_EXIT)
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_EXIT |  Clearing dispatch block.")
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__BLIP_ZONE
			//Is the hide PV blip option enabled by the user?
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_HIDE_PV_BLIP)				
				CLEAR_BIT(MPGlobalsAmbience.iNGAmbBitSet,  iNGABI_PLAYER_IN_HIDE_PV_BLIP_AREA)
				PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_EXIT |  Clearing bit iNGABI_PLAYER_IN_HIDE_PV_BLIP_AREA in MPGlobalsAmbience.iNGAmbBitSet.")
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__PREREQ_ZONE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_CLEAR_PREREQ_ON_EXIT)
				PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_EXIT | Player exited Prereq Zone! Clearing prereq ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneValue3, " now")
				RESET_PREREQUISITE_COMPLETED(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneValue3, NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_SET_PREREQ_ONLY_FOR_LOCAL_PLAYER))
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__RULE_ENTITY_PREREQ_ZONE
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_EXIT | Rule Entity Prereq Zone No Longer Triggering! Clearing prereq ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneValue3, " now!")
			RESET_PREREQUISITE_COMPLETED(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneValue3)
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_TRIGGERING_ZONE_OF_TYPE(INT iZone, INT iZoneType = ciFMMC_ZONE_TYPE__MAX)
	IF iCurrentTriggeringZone[iZoneType] = -1
	OR iCurrentTriggeringZone[iZoneType] > iZone //Allow the lowest numbered zone priority to reduce issues cause by zones not triggering for a frame when going from a higher to lower index
		iCurrentTriggeringZone[iZoneType] = iZone
		PRINTLN("[Zones][Zone ", iZone, "] Setting iCurrentTriggeringZone[", iZoneType, "] to ", iCurrentTriggeringZone[iZoneType])
		
		SWITCH GET_FMMC_ZONE_TYPE(iZone)
			CASE ciFMMC_ZONE_TYPE__OBJECTIVE_TEXT_OVERRIDE
				REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "SET_TRIGGERING_ZONE_OF_TYPE - ciFMMC_ZONE_TYPE__OBJECTIVE_TEXT_OVERRIDE" #ENDIF )
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC
PROC CLEAR_TRIGGERING_ZONE_OF_TYPE(INT iZone, INT iZoneType = ciFMMC_ZONE_TYPE__MAX)
	IF iCurrentTriggeringZone[iZoneType] = iZone
		iCurrentTriggeringZone[iZoneType] = -1
		PRINTLN("[Zones][Zone ", iZone, "] Clearing iCurrentTriggeringZone[", iZoneType, "] because Zone ", iZone, " is no longer triggering")
		
		SWITCH GET_FMMC_ZONE_TYPE(iZone)
			CASE ciFMMC_ZONE_TYPE__OBJECTIVE_TEXT_OVERRIDE
				REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "CLEAR_TRIGGERING_ZONE_OF_TYPE - ciFMMC_ZONE_TYPE__OBJECTIVE_TEXT_OVERRIDE" #ENDIF )
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC SET_ZONE_IS_TRIGGERING(INT iZone)

	SET_TRIGGERING_ZONE_OF_TYPE(iZone, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType)
	
	IF NOT IS_BIT_SET(iZoneIsTriggeredBS, iZone)
		SET_BIT(iZoneIsTriggeredBS, iZone)
		PROCESS_ZONE_ENTER(iZone)
		PRINTLN("[Zones][Zone ", iZone, "] SET_ZONE_IS_TRIGGERING | Zone ", iZone, " is triggering now")
	ENDIF
ENDPROC

PROC SET_ZONE_IS_NOT_TRIGGERING(INT iZone)

	CLEAR_TRIGGERING_ZONE_OF_TYPE(iZone, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType)
	
	IF IS_BIT_SET(iZoneIsTriggeredBS, iZone)
		CLEAR_BIT(iZoneIsTriggeredBS, iZone)
		PROCESS_ZONE_EXIT(iZone)
		PRINTLN("[Zones][Zone ", iZone, "] SET_ZONE_IS_NOT_TRIGGERING | Zone ", iZone, " has stopped triggering")
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: FIRE ROCKET ZONES -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to Rockets firing in Fire Rocket Zone zones ---------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_RESTRICTION_ROCKET_POS_VALID_FOR_ZONE(INT iPos, INT iZone)
	
	INT iLinkedZoneBS = g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketZoneBS[iPos]
	
	IF iLinkedZoneBS != 0
	AND NOT IS_BIT_SET(iLinkedZoneBS, iZone)
		PRINTLN("[RCC MISSION][ZoneRockets] IS_RESTRICTION_ROCKET_POS_VALID - Returning FALSE for position ", iPos," not linked to zone ", iZone)
		RETURN FALSE
	ENDIF
	
	INT iLinkedObj = g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketLinkedObj[iPos]
	
	IF iLinkedObj != -1
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iLinkedObj))
		OR IS_ENTITY_DEAD(NET_TO_OBJ(GET_OBJECT_NET_ID(iLinkedObj)))
			PRINTLN("[RCC MISSION][ZoneRockets] IS_RESTRICTION_ROCKET_POS_VALID - Returning FALSE for position ", iPos," as linked obj ", iLinkedObj," is non-existent or dead")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC VECTOR GET_CLOSEST_ROCKET_POSITION_FOR_ZONE(INT iZone, INT &iRocketPos)
	
	VECTOR vRocketPosition
	VECTOR vPlayer = GET_PLAYER_COORDS(LocalPlayer)
	FLOAT fClosestDist2 = 999999999
	INT iPosLoop
	
	FOR iPosLoop = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfRocketPositions - 1
		IF IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.vFMMCZoneRocketPositions[iPosLoop])
			RELOOP
		ENDIF
		
		IF NOT IS_RESTRICTION_ROCKET_POS_VALID_FOR_ZONE(iPosLoop, iZone)
			RELOOP
		ENDIF
		
		FLOAT fDist2 = VDIST2(vPlayer, g_FMMC_STRUCT_ENTITIES.vFMMCZoneRocketPositions[iPosLoop])
		
		IF fDist2 < fClosestDist2
			fClosestDist2 = fDist2
			vRocketPosition = g_FMMC_STRUCT_ENTITIES.vFMMCZoneRocketPositions[iPosLoop]
			iRocketPos = iPosLoop
		ENDIF
		
	ENDFOR
	
	RETURN vRocketPosition
	
ENDFUNC

FUNC FLOAT GET_ROCKET_AIMING_INACCURACY_MAX_DISP(FLOAT fAccuracy)
	
	RETURN LERP_FLOAT(30.0, 0.0, (fAccuracy/100.0))
	
ENDFUNC

FUNC FLOAT GET_ROCKET_AIMING_INACCURACY_MIN_DISP(FLOAT fAccuracy)
	
	IF fAccuracy >= 10.0
		RETURN 0.0
	ENDIF
	
	RETURN LERP_FLOAT(10.0, 0.0, (fAccuracy/10.0))
	
ENDFUNC

PROC GET_ROCKET_AIMING_INACURRACY_MODIFIERS(FLOAT fAccuracy, FLOAT &fXMod, FLOAT &fYMod, FLOAT &fZMod)
	
	FLOAT fMaxDisp = GET_ROCKET_AIMING_INACCURACY_MAX_DISP(fAccuracy) // The furthest possible this could be put away from the original position
	FLOAT fMinDisp = GET_ROCKET_AIMING_INACCURACY_MIN_DISP(fAccuracy) // The minimum we need to move this away from the original position, only used at very low accuracies to make sure it doesn't hit
	
	PRINTLN("[RCC MISSION] GET_ROCKET_AIMING_INACURRACY_MODIFIERS - Called with fAccuracy ", fAccuracy,"; fMaxDisp = ", fMaxDisp,", fMinDisp = ", fMinDisp)
	
	SET_RANDOM_SEED(NATIVE_TO_INT(GET_NETWORK_TIME()))
	
	FLOAT fXRan = GET_RANDOM_FLOAT_IN_RANGE(0, 1) // A random number between 0 & 1, to pick a float between fMinDisp and fMaxDisp
	BOOL bXNve = GET_RANDOM_BOOL() // Whether this should be a positive or negative displacement from the original X position
	
	fXMod = LERP_FLOAT(fMinDisp, fMaxDisp, fXRan) * PICK_FLOAT(bXNve, -1, 1)
	PRINTLN("[RCC MISSION] GET_ROCKET_AIMING_INACURRACY_MODIFIERS - X: fXRan ", fXRan," bXNve ", bXNve," -> fXMod ", fXMod)
	
	FLOAT fYRan = GET_RANDOM_FLOAT_IN_RANGE(0, 1)
	BOOL bYNve = GET_RANDOM_BOOL()
	
	fYMod = LERP_FLOAT(fMinDisp, fMaxDisp, fYRan) * PICK_FLOAT(bYNve, -1, 1)
	PRINTLN("[RCC MISSION] GET_ROCKET_AIMING_INACURRACY_MODIFIERS - Y: fYRan ", fYRan," bYNve ", bYNve," -> fYMod ", fYMod)
	
	FLOAT fZRan = GET_RANDOM_FLOAT_IN_RANGE(0, 1)
	BOOL bZNve = GET_RANDOM_BOOL()
	
	fZMod = LERP_FLOAT(fMinDisp, fMaxDisp, fZRan) * PICK_FLOAT(bZNve, -1, 1)
	PRINTLN("[RCC MISSION] GET_ROCKET_AIMING_INACURRACY_MODIFIERS - Z: fZRan ", fZRan," bZNve ", bZNve," -> fZMod ", fZMod)
	
ENDPROC

//Assuming the player keeps on moving this velocity, where do I need to aim to hit them?
FUNC VECTOR GET_VECTOR_TO_AIM_AT(VECTOR vPlayerPos, VECTOR vPlayerVelocity, VECTOR vRocketPos, FLOAT fRocketSpeed, FLOAT fAccuracy)
	
	FLOAT t // Time taken for the rocket to hit
	
	VECTOR vDisp = vPlayerPos - vRocketPos
	
	FLOAT fVX = DOT_PRODUCT(vPlayerVelocity, vDisp)
	
	FLOAT fRoot = (POW(fVX,2) + (4 * VMAG2(vDisp) * (POW(fRocketSpeed,2) - VMAG2(vPlayerVelocity))))
	
	IF fRoot >= 0
		// Solve the quadratic equation for t
		t = ((fVX + SQRT(fRoot)) / (2 * (POW(fRocketSpeed,2) - VMAG2(vPlayerVelocity))))
	ELSE
		//Shoot at an arbitrary distance in front (square root of a negative number isn't something we want to work with in our real-space game world)
		t = 1
	ENDIF
	
	// Work out the values of our aim at vector:
	VECTOR vAim = vPlayerPos + (vPlayerVelocity * t)
	PRINTLN("[RCC MISSION] GET_VECTOR_TO_AIM_AT - Perfect accuracy vAim calculated as ",vAim)
	
	IF fAccuracy <= 99.9
		FLOAT fXMod, fYMod, fZMod
		GET_ROCKET_AIMING_INACURRACY_MODIFIERS(fAccuracy, fXMod, fYMod, fZMod)
		
		vAim.x += fXMod
		vAim.y += fYMod
		vAim.z += fZMod
		
		PRINTLN("[RCC MISSION] GET_VECTOR_TO_AIM_AT - Imperfect fAccuracy ", fAccuracy," -> new inaccuracte vAim ",vAim)
	ENDIF
	
	RETURN vAim
	
ENDFUNC

FUNC BOOL IS_PLAYER_IN_CORRECT_VEHICLE_TYPE_FOR_AIR_ZONE(INT iZone)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_AirRestrictionRocketsFireOnAllPlayers)
		// This is an air restriction zone, only fire on air vehicles
		IF IS_PED_IN_ANY_HELI(LocalPlayerPed)
		OR IS_PED_IN_ANY_PLANE(LocalPlayerPed)
			IF IS_PLAYER_IN_VEH_SEAT(LocalPlayer, VS_DRIVER)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		// Fire at everything!
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			// Only fire at the driver of the vehicle, don't want loads of rockets all firing on the same place
			IF IS_PLAYER_IN_VEH_SEAT(LocalPlayer, VS_DRIVER)
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: ZONE TIMERS -----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to zone timers --------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_ZONE_TIMER_AS_COMPLETED(INT iZone)
	IF NOT IS_BIT_SET(iZoneTimersCompletedBS, iZone)
		SET_BIT(iZoneTimersCompletedBS, iZone)
		RESET_NET_TIMER(stZoneTimers[iZone])
		PRINTLN("[ZoneTimer] Setting zone ", iZone, " as completed!")
	ENDIF
ENDPROC

PROC SET_ZONE_TIMER_AS_HIDDEN(INT iZone)
	IF NOT IS_BIT_SET(iZoneTimersHiddenBS, iZone)
		SET_BIT(iZoneTimersHiddenBS, iZone)
		PRINTLN("[ZoneTimer] Setting zone ", iZone, " as hidden!")
	ENDIF
ENDPROC

PROC DISPLAY_ZONE_TIMER_HUD(INT iZone)
	
	TEXT_LABEL_15 tl15 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].tlZoneTimer_TimerName
	HUD_COLOURS eTitleColour = HUD_COLOUR_WHITE
	
	IF GET_ZONE_TIMER_TIME_REMAINING(iZone) < 5000
		eTitleColour = HUD_COLOUR_RED
	ENDIF
	
	DRAW_GENERIC_TIMER(GET_ZONE_TIMER_TIME_REMAINING(iZone), tl15, DEFAULT, TIMER_STYLE_DONTUSEMILLISECONDS, -1,  DEFAULT, HUDORDER_TOP, DEFAULT, eTitleColour, DEFAULT, DEFAULT, DEFAULT, eTitleColour)
ENDPROC

FUNC BOOL SHOULD_ZONE_TIMER_START()

	IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME()
		INT iTeam
		FOR iTeam = 0 TO g_FMMC_STRUCT.iNumberOfTeams - 1
			IF NOT DOES_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(iTeam)
				RELOOP
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[GET_TEAM_CURRENT_RULE(iTeam)], ciBS_RULE13_START_ALL_ZONE_TIMERS)
				PRINTLN("[ZoneTimer] SHOULD_ZONE_TIMER_START | Returning TRUE due to ciBS_RULE13_START_ALL_ZONE_TIMERS coming from Team ", iTeam)
				RETURN TRUE
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ZONE_TIMER_STOP(INT iZone)
	
	IF NOT IS_ZONE_TIMER_RUNNING(iZone)
		RETURN FALSE
	ENDIF
	
	IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME()
		INT iTeam
		FOR iTeam = 0 TO g_FMMC_STRUCT.iNumberOfTeams - 1
			IF NOT DOES_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(iTeam)
				RELOOP
			ENDIF
		
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[GET_TEAM_CURRENT_RULE(iTeam)], ciBS_RULE13_STOP_ALL_ZONE_TIMERS)
				PRINTLN("[ZoneTimer] SHOULD_ZONE_TIMER_STOP | Returning TRUE due to ciBS_RULE13_STOP_ALL_ZONE_TIMERS coming from Team ", iTeam)
				RETURN TRUE
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ZONE_TIMER_BE_HIDDEN(INT iZone)
	UNUSED_PARAMETER(iZone)
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ZONE_TIMER_SKIP(INT iZone)
	UNUSED_PARAMETER(iZone)
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DISPLAY_ZONE_TIMER(INT iZone)

	IF IS_BIT_SET(iZoneTimersHiddenBS, iZone)
		RETURN FALSE
	ENDIF
	
	IF GET_ZONE_TIMER_TIME_REMAINING(iZone) <= 0
		RETURN FALSE
	ENDIF

	RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_DISPLAY_ZONE_TIMER)
ENDFUNC

PROC PROCESS_ZONE_TIMER_EVERY_FRAME_CLIENT(INT iZone)
	IF IS_ZONE_TIMER_RUNNING(iZone)
		IF SHOULD_DISPLAY_ZONE_TIMER(iZone)
			DISPLAY_ZONE_TIMER_HUD(iZone)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_ZONE_TIMER_SERVER(INT iZone)
	IF NOT IS_ZONE_TIMER_RUNNING(iZone)
		IF SHOULD_ZONE_TIMER_START()
			BROADCAST_FMMC_ZONE_TIMER_EVENT(iZone, TRUE, FALSE)
			PRINTLN("[ZoneTimer] Starting zone timer for Zone ", iZone)
		ENDIF
	ELSE
		IF HAS_ZONE_TIMER_EXPIRED(iZone)
			IF NOT IS_BIT_SET(iZoneTimersCompletedBS, iZone)
				BROADCAST_FMMC_ZONE_TIMER_EVENT(iZone, FALSE, FALSE, FALSE, FALSE, TRUE)
			ENDIF
			
		ELIF SHOULD_ZONE_TIMER_BE_HIDDEN(iZone)
			IF NOT IS_BIT_SET(iZoneTimersHiddenBS, iZone)
				BROADCAST_FMMC_ZONE_TIMER_EVENT(iZone, FALSE, FALSE, FALSE, TRUE, FALSE)
			ENDIF
			
		ELIF SHOULD_ZONE_TIMER_STOP(iZone)
			PRINTLN("[ZoneTimer] Stopping zone timer for Zone ", iZone)
			BROADCAST_FMMC_ZONE_TIMER_EVENT(iZone, FALSE, TRUE)
		ENDIF
	ENDIF
	
	IF SHOULD_ZONE_TIMER_SKIP(iZone)
		BROADCAST_FMMC_ZONE_TIMER_EVENT(iZone, FALSE, FALSE, TRUE)
		PRINTLN("[ZoneTimer] Skipping zone timer for Zone ", iZone)
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: METAL DETECTOR ZONES --------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Metal Detector specific functions --------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_METAL_DETECTOR_FUNCTIONAL(INT iZone)

	IF IS_BIT_SET(iMetalDetectorZoneDisabledBitset, iZone)
		PRINTLN("[MetalDetector] IS_METAL_DETECTOR_FUNCTIONAL - Returning FALSE due to iMetalDetectorZoneDisabledBitset")
		RETURN FALSE
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_METAL_DETECTOR_AFFECTED_BY_EMP)
	AND IS_ANY_EMP_CURRENTLY_ACTIVE()
		PRINTLN("[MetalDetector] IS_METAL_DETECTOR_FUNCTIONAL - Returning FALSE due to IS_ANY_EMP_CURRENTLY_ACTIVE")
		RETURN FALSE
	ENDIF
		
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLAYER_HOLDING_METAL_DETECTOR_EXTREME_WEAPON()
	IF HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_MICROSMG)
	OR HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_DBSHOTGUN)
	OR HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_MACHINEPISTOL)
	OR HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_AUTOSHOTGUN)
	OR HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_COMPACTRIFLE)
	OR HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_SAWNOFFSHOTGUN)
	OR HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_CARBINERIFLE)
	OR HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_ASSAULTSHOTGUN)
	OR HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_SMG_MK2)
	OR HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_BULLPUPRIFLE_MK2)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PLAY_METAL_DETECTOR_SOUND(VECTOR vDetectionPos, BOOL bExtremeWeapon)
	IF bExtremeWeapon
		PLAY_SOUND_FROM_COORD(-1, "Metal_Detector_Big_Guns", vDetectionPos, "dlc_ch_heist_finale_security_alarms_sounds")
	ELSE
		PLAY_SOUND_FROM_COORD(-1, "Metal_Detector_Small_Guns", vDetectionPos, "dlc_ch_heist_finale_security_alarms_sounds")
	ENDIF
ENDPROC

PROC SET_METAL_DETECTOR_AS_ALERTED(INT iZone, VECTOR vDetectionPos, BOOL bExtremeWeapon)
	UNUSED_PARAMETER(vDetectionPos) //Will later be used to get props/lights etc
	
	PLAY_METAL_DETECTOR_SOUND(vDetectionPos, bExtremeWeapon)
	
	IF NOT IS_BIT_SET(iMetalDetectorZoneHasBeenAlertedBS, iZone)
		PRINTLN("[MetalDetector] SET_METAL_DETECTOR_AS_ALERTED || iZone: ", iZone, " | vDetectionPos: ", vDetectionPos)
		SET_BIT(iMetalDetectorZoneHasBeenAlertedBS, iZone)
		
		IF bExtremeWeapon
			SET_BIT(iLocalBoolCheck25, LBOOL25_A_PLAYER_HAS_SUPER_ALERTED_METAL_DETECTOR)
			PRINTLN("[MetalDetector] Setting LBOOL25_A_PLAYER_HAS_SUPER_ALERTED_METAL_DETECTOR")
		ENDIF
		
		INT iPed
		FOR iPed = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - 1
			NETWORK_INDEX niPed = MC_serverBD_1.sFMMC_SBD.niPed[iPed]
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niPed)
			OR NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niPed)
				RELOOP
			ENDIF
			
			PED_INDEX piPed = NET_TO_PED(niPed)
			IF NOT IS_ENTITY_ALIVE(piPed)
				RELOOP
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedAggroZone != iZone
			AND VDIST2(GET_ENTITY_COORDS(piPed), vDetectionPos) > POW(25.0, 2.0)
				PRINTLN("[MetalDetector] Ped ", iPed, " isn't bothered about zone ", iZone)
				RELOOP
			ENDIF
			
			fPedHeadingBeforeMetalDetector[iPed] = GET_ENTITY_HEADING(piPed)
			REINIT_NET_TIMER(stPedMetalDetectorInvestigationTimer[iPed])
			
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niPed)
				TASK_LOOK_AT_COORD(piPed, vDetectionPos, 3000, SLF_USE_TORSO | SLF_WHILE_NOT_IN_FOV | SLF_FAST_TURN_RATE, SLF_LOOKAT_VERY_HIGH)
				TASK_TURN_PED_TO_FACE_COORD(piPed, vDetectionPos, 2000)
				PRINTLN("[MetalDetector] Tasking ped ", iPed, " to look at coords ", vDetectionPos)
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: HEALTH DRAIN ----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to Health Drain zones  ------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL CAN_PLAYER_COUGH()
	
	IF HAS_NET_TIMER_STARTED(stHealthDrainCoughCooldownTimer)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_JUMPING(LocalPlayerPed)
	OR IS_PED_SPRINTING(LocalPlayerPed)
	OR IS_PED_RAGDOLL(LocalPlayerPed)
	OR IS_PED_CLIMBING(LocalPlayerPed)
	OR IS_PED_DEAD_OR_DYING(LocalPlayerPed)
	OR IS_PED_GESTURING(LocalPlayerPed)
	OR IS_PED_GOING_INTO_COVER(LocalPlayerPed)
	OR IS_PED_VAULTING(LocalPlayerPed)
	OR IS_PED_IN_COVER(LocalPlayerPed)
	OR GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IsAimingGun)
	OR MC_playerBD[iLocalPart].iObjHacking > -1
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC INT GET_HEALTH_DRAIN_ZONE_COUGH_COOLDOWN_LENGTH()
	RETURN ciHealthDrainCoughCooldownLength
ENDFUNC

FUNC FLOAT GET_HEALTH_DRAIN_ZONE_COUGH_CHANCE()
	RETURN cfHealthDrainCoughChance
ENDFUNC

FUNC STRING GET_HEALTH_DRAIN_ZONE_COUGH_SOUND_EFFECT()

	SWITCH GET_PARTICIPANT_NUMBER_IN_TEAM()
		CASE 0
			IF IS_PLAYER_FEMALE()
				RETURN "Female_01"
			ELSE
				RETURN "Male_01"
			ENDIF
		BREAK
		CASE 1
			IF IS_PLAYER_FEMALE()
				RETURN "Female_02"
			ELSE
				RETURN "Male_02"
			ENDIF
		BREAK
		CASE 2
			IF IS_PLAYER_FEMALE()
				RETURN "Female_03"
			ELSE
				RETURN "Male_03"
			ENDIF
		BREAK
		CASE 3
			IF IS_PLAYER_FEMALE()
				RETURN "Female_04"
			ELSE
				RETURN "Male_04"
			ENDIF
		BREAK
	ENDSWITCH

	IF IS_PLAYER_FEMALE()
		RETURN "Female_01"
	ELSE
		RETURN "Male_01"
	ENDIF
ENDFUNC

PROC PROCESS_HEALTH_DRAIN_ZONE_COUGHING()
	
	ANIM_DATA sAnimData, sAnimDataNull1, sAnimDataNull2
	sAnimData.type = APT_SINGLE_ANIM
	sAnimData.phase0 = 0.0
	sAnimData.rate0 = 1.0
	sAnimData.filter = GET_HASH_KEY("BONEMASK_UPPERONLY")
	sAnimData.dictionary0 = "anim@fidgets@coughs"
	sAnimData.flags = (AF_UPPERBODY | AF_SECONDARY | AF_ADDITIVE)
	STRING sFacialAnimName
	
	REQUEST_SCRIPT_AUDIO_BANK("DLC_HEIST3/CASINO_HEIST_FINALE_GENERAL_01")
	
	REQUEST_ANIM_DICT("anim@fidgets@coughs")
	REQUEST_ANIM_SET("anim@fidgets@coughs")
	
	IF NOT HAS_ANIM_DICT_LOADED("anim@fidgets@coughs")
		PRINTLN("[HealthDrainZone] PROCESS_HEALTH_DRAIN_ZONE_COUGHING | Waiting for anims to load")
		EXIT
	ENDIF
	
	IF CAN_PLAYER_COUGH()
	
		IF GET_RANDOM_FLOAT_IN_RANGE(0.0, 1.0) < GET_HEALTH_DRAIN_ZONE_COUGH_CHANCE()
		OR NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_DONE_FIRST_COUGH)
			iHealthDrainCoughAnim = GET_RANDOM_INT_IN_RANGE(0, 3)
			
			SWITCH iHealthDrainCoughAnim
				CASE 0	
					sAnimData.anim0 = "COUGH_A"
					sFacialAnimName = "COUGH_A_FACIAL"
				BREAK
				CASE 1
					sAnimData.anim0 = "COUGH_B"
					sFacialAnimName = "COUGH_B_FACIAL"
				BREAK
				CASE 2
					sAnimData.anim0 = "COUGH_C"
					sFacialAnimName = "COUGH_C_FACIAL"
				BREAK
			ENDSWITCH
			
			TASK_SCRIPTED_ANIMATION(LocalPlayerPed, sAnimData, sAnimDataNull1, sAnimDataNull2)
			PLAY_FACIAL_ANIM(LocalPlayerPed, sFacialAnimName, sAnimData.dictionary0)
			REINIT_NET_TIMER(stHealthDrainCoughCooldownTimer)
			SET_BIT(iLocalBoolCheck32, LBOOL32_DONE_FIRST_COUGH)
			CLEAR_BIT(iLocalBoolCheck33, LBOOL33_PLAYED_COUGH_SOUND_EFFECT)
		ENDIF
	ENDIF
	
	// Coughing sounds are handled by starting a timer when the cough anim starts and then we process the timer here:
	IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_PLAYED_COUGH_SOUND_EFFECT)
		IF HAS_NET_TIMER_STARTED(stHealthDrainCoughCooldownTimer)
			INT iCoughTime
			SWITCH iHealthDrainCoughAnim
				CASE 0	
					iCoughTime = 750
				BREAK
				CASE 1	
					iCoughTime = 275
				BREAK
				CASE 2	
					iCoughTime = 335
				BREAK
			ENDSWITCH
			
			IF HAS_NET_TIMER_EXPIRED(stHealthDrainCoughCooldownTimer, iCoughTime)
				PRINTLN("[HealthDrainZone] Playing cough sound effect: ", GET_HEALTH_DRAIN_ZONE_COUGH_SOUND_EFFECT())
				PLAY_SOUND_FROM_ENTITY(-1, GET_HEALTH_DRAIN_ZONE_COUGH_SOUND_EFFECT(), LocalPlayerPed, "dlc_ch_heist_finale_poison_gas_coughs_sounds", TRUE, 500)
				SET_BIT(iLocalBoolCheck33, LBOOL33_PLAYED_COUGH_SOUND_EFFECT)
			ENDIF
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(stHealthDrainCoughCooldownTimer)
	AND HAS_NET_TIMER_EXPIRED(stHealthDrainCoughCooldownTimer, GET_HEALTH_DRAIN_ZONE_COUGH_COOLDOWN_LENGTH())
		RESET_NET_TIMER(stHealthDrainCoughCooldownTimer)
	ENDIF
ENDPROC

PROC PROCESS_HEALTH_DRAIN_ZONE_DAMAGE_EFFECTS(INT iZone)
	APPLY_OR_REMOVE_POISON_GAS_SCREEN_EFFECT(IS_ZONE_TRIGGERING(iZone))
ENDPROC

PROC PROCESS_HEALTH_DRAIN_ZONE_EVERY_FRAME(INT iZone)
	
	IF IS_ZONE_TRIGGERING(iZone)
		//fHealthDrainZoneDamageBuildup += GET_FRAME_TIME() * g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue
		fHealthDrainZoneDamageBuildup += GET_FRAME_TIME() * fHealthDrainZoneTickSpeed
		
		IF fHealthDrainZoneDamageBuildup >= 1.0
		
			INT iDamage = CEIL(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue / fHealthDrainZoneTickSpeed)
			INT iNewHealth = GET_ENTITY_HEALTH(LocalPlayerPed) - iDamage
			
			IF IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
				// In test mode this lets you test the damage but will never kill you
				iNewHealth = CLAMP_INT(iNewHealth, 110, iNewHealth)
			ENDIF
			
			IF iNewHealth < 0
				iNewHealth = 0
			ENDIF
			
			IF NOT GET_PLAYER_INVINCIBLE(LocalPlayer)
			AND NOT GET_PLAYER_DEBUG_INVINCIBLE(LocalPlayer)
			AND NOT HAS_LOCAL_PLAYER_FINISHED_OR_REACHED_END()
				SET_ENTITY_HEALTH(LocalPlayerPed, iNewHealth)
				PRINTLN("[HealthDrainZone] PROCESS_HEALTH_DRAIN_ZONE_EVERY_FRAME - Damaging the player for ", iDamage, " damage! HP is now ", iNewHealth)
			ENDIF
			
			fHealthDrainZoneDamageBuildup -= 1.0
			
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_COUGH_IN_HEALTH_DRAIN_ZONE)
			PROCESS_HEALTH_DRAIN_ZONE_COUGHING()
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_USE_HEALTH_DRAIN_ZONE_SCREEN_FX)
			APPLY_OR_REMOVE_POISON_GAS_SCREEN_EFFECT(TRUE)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bHealthDrainZoneDebug
		PROCESS_HEALTH_DRAIN_ZONE_DEBUG(iZone)
	ENDIF
	#ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: ZONE REMOVAL ----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to Zone removal -------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_ZONE_BE_REMOVED_DUE_TO_ENTITY_LINK(INT i)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkType = -1
	OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex = -1
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iLocalZoneRemovedBitset, i)
		RETURN FALSE
	ENDIF
	
	BOOL bRemoveWhenNotExist = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneBS, ciFMMC_ZONEBS_REMOVE_ZONE_WHEN_NO_ENTITY_LINK)
	BOOL bRemoveWhenNotPriority = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneBS, ciFMMC_ZONEBS_REMOVE_ZONE_WHEN_ENTITY_LINK_NOT_PRIORITY)
	INT iTeam = MC_PlayerBD[iPartToUse].iTeam
	
	IF bRemoveWhenNotExist
	OR bRemoveWhenNotPriority
		IF iTeam > -1
		AND iTeam < FMMC_MAX_TEAMS
			INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
			
			IF iRule < FMMC_MAX_RULES
				SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkType
					CASE ciENTITY_TYPE_PED
						CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] iZone ", i, " Ped Link: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex, " Ped Priority: ", MC_ServerBD_4.iPedPriority[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex][iTeam])
						
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex])
							PED_INDEX pedIndex
							pedIndex = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex])
							IF (IS_PED_INJURED(pedIndex) AND bRemoveWhenNotExist)
							OR ((MC_ServerBD_4.iPedPriority[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex][iTeam] = 99999 OR MC_ServerBD_4.iPedPriority[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex][iTeam] < iRule) AND bRemoveWhenNotPriority)
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] iZone ", i, " Ped Link Exists... Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex)
								RETURN TRUE
							ENDIF
						ELSE
							IF bRemoveWhenNotExist
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] iZone ", i, " Ped Link Does not Exist... Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex)
								RETURN TRUE
							ENDIF
						ENDIF
					BREAK
					CASE ciENTITY_TYPE_VEHICLE
						CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] iZone ", i, " Vehicle Link: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex, " Veh Priority: ", MC_ServerBD_4.iVehPriority[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex][iTeam])
					
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex])
							VEHICLE_INDEX vehIndex
							vehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex])
							IF (NOT IS_VEHICLE_DRIVEABLE(vehIndex) AND bRemoveWhenNotExist)
							OR ((MC_ServerBD_4.iVehPriority[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex][iTeam] = 99999 OR MC_ServerBD_4.iVehPriority[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex][iTeam] < iRule) AND bRemoveWhenNotPriority)
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] iZone ", i, " Vehicle Link Exists but it's destroyed... Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex)
								RETURN TRUE
							ENDIF
						ELSE
							IF bRemoveWhenNotExist
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] iZone ", i, " Vehicle Link Does not Exist... Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex)
								RETURN TRUE
							ENDIF
						ENDIF
					BREAK
					CASE ciENTITY_TYPE_OBJECT						
						CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] iZone ", i, " Object Link: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex, " Obj Priority: ",  MC_ServerBD_4.iObjPriority[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex][iTeam])
						
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_OBJECT_NET_ID(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex))
							OBJECT_INDEX ObjIndex
							ObjIndex = NET_TO_OBJ(GET_OBJECT_NET_ID(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex))
							IF ((IS_ENTITY_ATTACHED_TO_ANY_PED(ObjIndex) OR IS_ENTITY_ATTACHED(ObjIndex) OR MC_ServerBD.iObjCarrier[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex] > -1) AND bRemoveWhenNotExist)
							OR ((MC_ServerBD_4.iObjPriority[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex][iTeam] = 99999 OR MC_ServerBD_4.iObjPriority[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex][iTeam] < iRule) AND bRemoveWhenNotPriority)
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] iZone ", i, " Object Link Exists and is attached/carried... Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex)
								RETURN TRUE
							ENDIF
						ELSE
							IF bRemoveWhenNotExist
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] iZone ", i, " Object Link Does not Exists... Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex)
								RETURN TRUE
							ENDIF
						ENDIF
					BREAK
					CASE ciENTITY_TYPE_GOTO
						
					BREAK
					CASE ciENTITY_TYPE_KILL_PLAYER
					
					BREAK
					CASE ciENTITY_TYPE_LAST_PLAYER
					
					BREAK
					CASE ciENTITY_TYPE_ZONES
					
					BREAK
					CASE ciENTITY_TYPE_TRAIN
						IF bRemoveWhenNotExist
							RETURN NOT DOES_ENTITY_EXIST(GET_FMMC_ENTITY(ciENTITY_TYPE_TRAIN, g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex))
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC REMOVE_ZONE(INT iZone)
	
	VECTOR vZoneCoords_0 = GET_ZONE_COORDS(iZone, 0)
	VECTOR vZoneCoords_1 = GET_ZONE_COORDS(iZone, 1)
	
	PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] REMOVE_ZONE | Removing Zone ", iZone)
	
	// Generic Zone Removal
	SET_ZONE_IS_NOT_TRIGGERING(iZone)
	RESET_NET_TIMER(td_ZoneRadiusOverrideTimer[iZone])
	PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] REMOVE_ZONE | Setting iLocalZoneRemovedBitset for iZone; ", iZone)
	SET_BIT(iLocalZoneRemovedBitset, iZone)
	
	iCurrentNumActiveZones[GET_FMMC_ZONE_TYPE(iZone)]--
	PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] REMOVE_ZONE | iCurrentNumActiveZones[", GET_FMMC_ZONE_TYPE(iZone), "] is now ", iCurrentNumActiveZones[GET_FMMC_ZONE_TYPE(iZone)])
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_TRIGGER_ZONE_REMOTELY)
	AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkType = ciENTITY_TYPE_ZONES
	AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkIndex > -1
		IF IS_BIT_SET(iZoneBS_TriggeredByLinkedZone, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkIndex)
			PRINTLN("[RCC MISSION] REMOVE_ZONE | (REMOVED) iZone: ", iZone, " Zone Entity Link Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkIndex, " Is now being de-activated remotely ..")
			CLEAR_BIT(iZoneBS_TriggeredByLinkedZone, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkIndex)
		ENDIF
	ENDIF
	
	IF iPopMultiArea[iZone] != -1
		IF DOES_POP_MULTIPLIER_AREA_EXIST(iPopMultiArea[iZone])
			REMOVE_POP_MULTIPLIER_AREA(iPopMultiArea[iZone], TRUE)
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] REMOVE_ZONE | iPopMultiArea cleaned up: ", iZone," native area id: ", iPopMultiArea[iZone])
			iPopMultiArea[iZone] = -1
		ENDIF
	ENDIF
	
	IF bipopScenarioArea[iZone] != NULL
		REMOVE_SCENARIO_BLOCKING_AREA(bipopScenarioArea[iZone])
		PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] REMOVE_ZONE | bipopScenarioArea cleaned up: ", iZone," native area id: ",NATIVE_TO_INT(bipopScenarioArea[iZone]))
		bipopScenarioArea[iZone] = NULL
	ENDIF
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType
		CASE ciFMMC_ZONE_TYPE__CLEAR_VEHICLES
		CASE ciFMMC_ZONE_TYPE__CLEAR_ALL
		CASE ciFMMC_ZONE_TYPE__ACTIVATE_ROADS
			SET_VEHICLE_GENERATORS_AND_ROADS_INACTIVE_FOR_ZONE(iZone, FALSE, iBitsetVehicleGeneratorsActiveArea, iBitsetSetRoadsArea)
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__CLEAR_ALL_AND_PV
			CLEAR_PERSONAL_VEHICLE_NO_SPAWN_ZONE()
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__BLOCK_WANTED_LEVEL
			IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE)
			OR IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_AND_CLEAR_WANTED_LEVEL_ZONE)
			
				INT iMaxWanted
				IF MC_serverBD.iPolice > 1
				AND GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice) > 0
					iMaxWanted = GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice)
				ELSE
					iMaxWanted = 5
				ENDIF
				
				PRINTLN("[RCC MISSION] REMOVE_ZONE | Block wanted level zone has been removed, setting max wanted level back to ", iMaxWanted)
				SET_MAX_WANTED_LEVEL(iMaxWanted)
				
				CLEAR_BIT(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE)
				CLEAR_BIT(iLocalBoolCheck13, LBOOL13_IN_BLOCK_AND_CLEAR_WANTED_LEVEL_ZONE)
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__CLEAR_COVER
			REMOVE_SPECIFIC_COVER_BLOCKING_AREAS(vZoneCoords_0, vZoneCoords_1, FALSE, FALSE, TRUE, TRUE)
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__NO_PLAYER_SPAWNING
			//bug 2123637 - need a way of removing mission spawn occlusion boxes
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__WATER_DAMPENING
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] REMOVE_ZONE - Clearing water dampening zone ", iZone)
			
			IF iZoneWaterCalmingQuadID[iZone] != -1
				IF iWaterCalmingQuad[iZoneWaterCalmingQuadID[iZone]] != -1
					REMOVE_EXTRA_CALMING_QUAD(iWaterCalmingQuad[iZoneWaterCalmingQuadID[iZone]])
					iWaterCalmingQuad[iZoneWaterCalmingQuadID[iZone]] = -1
				ENDIF
				iZoneWaterCalmingQuadID[iZone] = -1
			ENDIF
			
			DISABLE_IN_WATER_PTFX(FALSE)
			
			IF IS_BIT_SET(iWaveDampingZoneBitset, iZone)
				CLEAR_BIT(iWaveDampingZoneBitset, iZone)
				
				IF iWaveDampingZoneBitset != 0
					IF GET_DEEP_OCEAN_SCALER() = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue2
						SELECT_NEXT_WAVE_DAMPING_SCALER()
					ENDIF
				ELSE
					IF GET_DEEP_OCEAN_SCALER() != 1.0
						PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] REMOVE_ZONE - Calling RESET_DEEP_OCEAN_SCALER")
						RESET_DEEP_OCEAN_SCALER()
					ENDIF
				ENDIF
				
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__GPS
			IF iGPSDisabledZones[iZone] != -1
				PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] REMOVE_ZONE | Clearing GPS disabled zone at index ", iGPSDisabledZones[iZone])
				CLEAR_GPS_DISABLED_ZONE_AT_INDEX(iGPSDisabledZones[iZone])
				iGPSDisabledZones[iZone] = -1
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__DISPATCH_SPAWN_BLOCK
			IF iDispatchBlockZone[iZone] != -1
				PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] REMOVE_ZONE clearing Dispatch spawn disabled zone at index ", iDispatchBlockZone[iZone])
				REMOVE_DISPATCH_SPAWN_BLOCKING_AREA(iDispatchBlockZone[iZone])
				iDispatchBlockZone[iZone] = -1
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__EXPLOSION_AREA
			
			BOOL bOtherZoneActive
			INT iLoop
			
			FOR iLoop = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfZones - 1
				IF iLoop != iZone
				AND DOES_ZONE_EXIST(iZone)
				AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[iLoop].iType = ciFMMC_ZONE_TYPE__EXPLOSION_AREA
					bOtherZoneActive = TRUE
				ENDIF
			ENDFOR
			
			IF NOT bOtherZoneActive
				CLEAR_BIT(iLocalBoolCheck3, LBOOL3_EXPLOSIVE_ZONE_ACTIVE)
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__REFILL_SPECIAL_ABILITY
			CLEAR_BIT(iLocalBoolCheck24, LBOOL24_SPECIAL_ABILITY_REFILLED)
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] Removing Refill Special Ability zone ", iZone)
		BREAK		

		CASE ciFMMC_ZONE_TYPE__SPAWN_PROTECTION
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] REMOVE_ZONE Removing ciFMMC_ZONE_TYPE__SPAWN_PROTECTION zone ", iZone)
			IF iFMMCAirDefenceZoneArea[iZone] != -1
				IF DOES_AIR_DEFENCE_SPHERE_EXIST(iFMMCAirDefenceZoneArea[iZone])
					PRINTLN("[LM][REMOVE_ZONE] - Removing air defence sphere.")
					REMOVE_AIR_DEFENCE_SPHERE(iFMMCAirDefenceZoneArea[iZone])
				ENDIF
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__AIR_DEFENCE
			IF iFMMCAirDefenceZoneArea[iZone] != -1
				IF DOES_AIR_DEFENCE_SPHERE_EXIST(iFMMCAirDefenceZoneArea[iZone])
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] REMOVE_ZONE | Removing air defence area")
					REMOVE_AIR_DEFENCE_SPHERE(iFMMCAirDefenceZoneArea[iZone])
					iFMMCAirDefenceZoneArea[iZone] = -1
				ENDIF
			ENDIF
			
			CLEAR_BIT(iAirDefenceZone_UsingTimersBS, iZone)
		BREAK		

		CASE ciFMMC_ZONE_TYPE__NAV_BLOCKING
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] REMOVE_ZONE | ciFMMC_ZONE_TYPE__NAV_BLOCKING for iZone: ", iZone)
			REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlockerZone[iZone])
		BREAK
								
		CASE ciFMMC_ZONE_TYPE__BLOCK_ROAD_NODES			
			SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType
				CASE ciFMMC_ZONE_SHAPE__ANGLED_AREA
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] REMOVE_ZONE | ciFMMC_ZONE_TYPE__BLOCK_ROAD_NODES for iZone: ", iZone)
					SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1], GET_PLACED_ZONE_RADIUS(iZone), FALSE)
				BREAK
				
				CASE ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] REMOVE_ZONE | ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX for iZone: ", iZone)
					SET_ROADS_BACK_TO_ORIGINAL(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1], FALSE)
				BREAK
			ENDSWITCH
		BREAK
		CASE ciFMMC_ZONE_TYPE__ENABLE_ROAD_NODES
			SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType
				CASE ciFMMC_ZONE_SHAPE__ANGLED_AREA
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] REMOVE_ZONE | ciFMMC_ZONE_TYPE__ENABLE_ROAD_NODES for iZone: ", iZone)
					SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1], GET_PLACED_ZONE_RADIUS(iZone), FALSE)
				BREAK
				
				CASE ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] REMOVE_ZONE | ciFMMC_ZONE_TYPE__ENABLE_ROAD_NODES for iZone: ", iZone)
					SET_ROADS_BACK_TO_ORIGINAL(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1], FALSE)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__DROPOFF_ZONE
			INT iDropoffZoneTeam
			FOR iDropoffZoneTeam = 0 TO FMMC_MAX_TEAMS - 1
				// Clear iCurrentDropOffZone if it refers to this Zone
				IF iCurrentDropOffZone[iDropoffZoneTeam] = iZone
					iCurrentDropOffZone[iDropoffZoneTeam] = -1
					vCurrentDropOffZoneCachedCentre[iDropoffZoneTeam] = <<0,0,0>>
					PRINTLN("[LM][Zones][Zone ", iZone, "] REMOVE_ZONE | Clearing iCurrentDropOffZone[", iDropoffZoneTeam, "]")
				ENDIF
			ENDFOR
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__PETROL_POOL
			PRINTLN("[Zones][Zone ", iZone, "] REMOVE_ZONE | ciFMMC_ZONE_TYPE__PETROL_POOL | Removing Petrol Pool Zone now!")
			IF IS_DECAL_ALIVE(diPetrolDecals[iZone])
				REMOVE_DECAL(diPetrolDecals[iZone])
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_ZONE_PAST_VALID_RULE(INT iZone)
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] >= FMMC_MAX_RULES
		RETURN TRUE
	ENDIF
	
	RETURN MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneEndPriority[MC_playerBD[iPartToUse].iTeam]
ENDFUNC

PROC CACHE_ZONE_SHOULD_CLEANUP_THIS_FRAME(INT i)
	SET_BIT(iZoneShouldCleanupThisFrameBS, i)
ENDPROC

PROC CACHE_ZONE_SHOULD_NOT_CLEANUP_THIS_FRAME(INT i)
	SET_BIT(iZoneShouldNotCleanupThisFrameBS, i)
ENDPROC

FUNC BOOL SHOULD_ZONE_BE_REMOVED(INT iZone)
	
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iZoneShouldCleanupThisFrameBS, iZone)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iZoneShouldNotCleanupThisFrameBS, iZone)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_ZONE_BE_REMOVED_DUE_TO_ENTITY_LINK(iZone)
		PRINTLN("[Zones][Zone ", iZone, "] SHOULD_ZONE_BE_REMOVED - Zone ", iZone, " should be removed due to SHOULD_ZONE_BE_REMOVED_DUE_TO_ENTITY_LINK")
		CACHE_ZONE_SHOULD_CLEANUP_THIS_FRAME(iZone)
		RETURN TRUE
	ENDIF
	
	IF SHOULD_ZONE_BE_REMOVED_DUE_TO_AGGRO(iZone)
		PRINTLN("[Zones][Zone ", iZone, "] SHOULD_ZONE_BE_REMOVED - Zone ", iZone, " should be removed due to SHOULD_ZONE_BE_REMOVED_DUE_TO_AGGRO")
		CACHE_ZONE_SHOULD_CLEANUP_THIS_FRAME(iZone)
		RETURN TRUE
	ENDIF
	
	IF NOT IS_ZONE_A_BOUNDS_ZONE(iZone) // Bounds Zones don't use these priority options
		IF IS_ZONE_PAST_VALID_RULE(iZone)
			PRINTLN("[Zones][Zone ", iZone, "] SHOULD_ZONE_BE_REMOVED - Zone ", iZone, " should be removed due to the rule we're on")
			CACHE_ZONE_SHOULD_CLEANUP_THIS_FRAME(iZone)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.sPlayerAbilities.sVehicleRepair.iRepairZoneIndex = iZone
		IF IS_VECTOR_ZERO(sLocalPlayerAbilities.sVehicleRepairData.vVehicleRepairCoords)
			PRINTLN("[Zones][Zone ", iZone, "] SHOULD_ZONE_BE_REMOVED - Zone ", iZone, " is set to be the Repair Zone, but the Vehicle Repair ability isn't currently in use")
			CACHE_ZONE_SHOULD_CLEANUP_THIS_FRAME(iZone)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iZone, eSGET_Zone)
		PRINTLN("[Zones][Zone ", iZone, "] SHOULD_ZONE_BE_REMOVED - Zone ", iZone, " doesn't have a suitable spawn group")
		CACHE_ZONE_SHOULD_CLEANUP_THIS_FRAME(iZone)
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].eSpawnConditionFlag[0] != SPAWN_CONDITION_FLAG_NOT_SET
		IF MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].eSpawnConditionFlag, FALSE, FALSE, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAggroIndexBS_Entity_Zone, MC_PlayerBD[iPartToUse].iTeam)
			PRINTLN("[Zones][Zone ", iZone, "] SHOULD_ZONE_BE_REMOVED | Zone ", iZone, " Is being removed due to Spawn Blocking Flag MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")
			CACHE_ZONE_SHOULD_CLEANUP_THIS_FRAME(iZone)
			RETURN TRUE
		ENDIF
	ENDIF
	
	CACHE_ZONE_SHOULD_NOT_CLEANUP_THIS_FRAME(iZone)
	
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: ZONE CREATION ---------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to Zone Creation ------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC CLEAR_ZONE_AREA(INT iZone)
	
	VECTOR vClearAreaCoord = GET_ZONE_RUNTIME_CENTRE(iZone)
	FLOAT fClearRadius = GET_PLACED_ZONE_RADIUS(iZone)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
	OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__ANGLED_AREA
		IF GET_DISTANCE_BETWEEN_COORDS(vClearAreaCoord, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1], FALSE) > fClearRadius
			fClearRadius = GET_DISTANCE_BETWEEN_COORDS(vClearAreaCoord, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1], FALSE)
		ELSE
			fClearRadius = GET_DISTANCE_BETWEEN_COORDS(vClearAreaCoord, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], FALSE)
		ENDIF
	ENDIF
	
	GET_GROUND_Z_FOR_3D_COORD(vClearAreaCoord, vClearAreaCoord.z)
	CLEAR_AREA_LEAVE_VEHICLE_HEALTH(vClearAreaCoord, fClearRadius, TRUE)
	PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CLEAR_ZONE_AREA | vClearAreaCoord: ", vClearAreaCoord, " | fClearRadius: ", fClearRadius, " | Zone: ", iZone)
ENDPROC

PROC SANITISE_AXIS_ALIGNED_ZONE_COORDS(VECTOR& vZoneCoords_0, VECTOR& vZoneCoords_1)
	VECTOR vTemp0 = vZoneCoords_0
	VECTOR vTemp1 = vZoneCoords_1
	
	IF vZoneCoords_0.x > vZoneCoords_1.x
		vZoneCoords_0.x = vTemp1.x
		vZoneCoords_1.x = vTemp0.x
		PRINTLN("[Zones] SANITISE_AXIS_ALIGNED_ZONE_COORDS | Swapping X coords")
	ENDIF
	
	IF vZoneCoords_0.y > vZoneCoords_1.y
		vZoneCoords_0.y = vTemp1.y
		vZoneCoords_1.y = vTemp0.y
		PRINTLN("[Zones] SANITISE_AXIS_ALIGNED_ZONE_COORDS | Swapping Y coords")
	ENDIF
	
	IF vZoneCoords_0.z > vZoneCoords_1.z
		vZoneCoords_0.z = vTemp1.z
		vZoneCoords_1.z = vTemp0.z
		PRINTLN("[Zones] SANITISE_AXIS_ALIGNED_ZONE_COORDS | Swapping Z coords")
	ENDIF
ENDPROC

PROC CREATE_THIS_ZONE(INT iZone)

	PRINTLN("[Zones][Zone ", iZone, "] CREATE_THIS_ZONE - Creating Zone this frame iZone: ", iZone)
	CLEAR_BIT(iZoneShouldRespawnNowBS, iZone)
	
	INT iZoneTeam
	VECTOR vZoneCoords_0 = GET_ZONE_COORDS(iZone, 0)
	VECTOR vZoneCoords_1 = GET_ZONE_COORDS(iZone, 1)
	
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_DONT_CLEAR_AREA)
	AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		CLEAR_ZONE_AREA(iZone)
	ENDIF
	
	IF GET_ZONE_SHAPE(iZone) = ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
		SANITISE_AXIS_ALIGNED_ZONE_COORDS(vZoneCoords_0, vZoneCoords_1)
	ENDIF
	
	IF SHOULD_ZONE_BE_BLIPPED_ON_CREATION(iZone)
		SET_ZONE_AS_SAFE_TO_BLIP(iZone)
	ELSE
		SET_ZONE_AS_NOT_SAFE_TO_BLIP(iZone)
	ENDIF
	
	iCurrentNumActiveZones[GET_FMMC_ZONE_TYPE(iZone)]++
	PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE | iCurrentNumActiveZones[", GET_FMMC_ZONE_TYPE(iZone), "] is now ", iCurrentNumActiveZones[GET_FMMC_ZONE_TYPE(iZone)])
	
	SWITCH GET_FMMC_ZONE_TYPE(iZone)
		
		CASE ciFMMC_ZONE_TYPE__CLEAR_PEDS
			IF iPopMultiArea[iZone] = -1
				iPopMultiArea[iZone] = ADD_POP_MULTIPLIER_AREA(vZoneCoords_0, vZoneCoords_1, 0, MC_serverBD.fVehicleDensity, TRUE, FALSE)
			ENDIF
			
			IF NOT DOES_SCENARIO_BLOCKING_AREA_EXISTS(vZoneCoords_0, vZoneCoords_1)
				IF bipopScenarioArea[iZone] = NULL
					bipopScenarioArea[iZone] = ADD_SCENARIO_BLOCKING_AREA(vZoneCoords_0, vZoneCoords_1, FALSE,TRUE,TRUE, FALSE)
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - ADD_SCENARIO_BLOCKING_AREA - ciFMMC_ZONE_TYPE__CLEAR_PEDS - failed - already exists")
			ENDIF
			
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - turning off peds in area: ", iZone, " pos 0: ", vZoneCoords_0," pos 1: ", vZoneCoords_1)  
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - area been given id: ", iPopMultiArea[iZone]) 							
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__CLEAR_VEHICLES
			IF iPopMultiArea[iZone] = -1
				iPopMultiArea[iZone] = ADD_POP_MULTIPLIER_AREA(vZoneCoords_0, vZoneCoords_1, MC_serverBD.fPedDensity, 0, TRUE, FALSE)
			ENDIF
			
			IF NOT DOES_SCENARIO_BLOCKING_AREA_EXISTS(vZoneCoords_0, vZoneCoords_1)
				IF bipopScenarioArea[iZone] = NULL
					bipopScenarioArea[iZone] = ADD_SCENARIO_BLOCKING_AREA(vZoneCoords_0, vZoneCoords_1, FALSE, TRUE, FALSE, TRUE)
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - ADD_SCENARIO_BLOCKING_AREA - ciFMMC_ZONE_TYPE__CLEAR_VEHICLES - failed - already exists")
			ENDIF
			
			SET_VEHICLE_GENERATORS_AND_ROADS_INACTIVE_FOR_ZONE(iZone, TRUE, iBitsetVehicleGeneratorsActiveArea, iBitsetSetRoadsArea)
			
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - turning off vehs in area: ", iZone, " pos 0: ", vZoneCoords_0, " pos 1: ", vZoneCoords_1)
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - area been given id: ", iPopMultiArea[iZone])
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__CLEAR_ALL // Peds + Vehicles
			IF iPopMultiArea[iZone] = -1
				iPopMultiArea[iZone] = ADD_POP_MULTIPLIER_AREA(vZoneCoords_0, vZoneCoords_1, 0, 0, TRUE, FALSE)
			ENDIF
			IF NOT DOES_SCENARIO_BLOCKING_AREA_EXISTS(vZoneCoords_0, vZoneCoords_1)
				IF bipopScenarioArea[iZone] = NULL
					bipopScenarioArea[iZone] = ADD_SCENARIO_BLOCKING_AREA(vZoneCoords_0, vZoneCoords_1)
				ENDIF									
			ELSE
				PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - ADD_SCENARIO_BLOCKING_AREA - ciFMMC_ZONE_TYPE__CLEAR_ALL - failed - already exists")
			ENDIF
			
			SET_VEHICLE_GENERATORS_AND_ROADS_INACTIVE_FOR_ZONE(iZone, TRUE, iBitsetVehicleGeneratorsActiveArea, iBitsetSetRoadsArea)
			
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - turning off vehs and peds in area: ", iZone, " pos 0: ", vZoneCoords_0, " pos 1: ", vZoneCoords_1)
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - area been given id: ", iPopMultiArea[iZone])
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__CLEAR_ALL_AND_PV // Player Vehicle
			SET_PERSONAL_VEHICLE_NO_SPAWN_BOX(vZoneCoords_0, vZoneCoords_1)
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__CLEAR_COVER
			ADD_COVER_BLOCKING_AREA(vZoneCoords_0, vZoneCoords_1, FALSE, FALSE, TRUE, TRUE)
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - turning off cover in area: ", iZone, " pos 0: ", vZoneCoords_0," pos 1: ", vZoneCoords_1)
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__NO_PLAYER_SPAWNING
			IF iSpawnOcclusionIndex[iZone] = -1
				SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType
					CASE ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
						iSpawnOcclusionIndex[iZone] = ADD_MISSION_SPAWN_OCCLUSION_BOX(vZoneCoords_0, vZoneCoords_1)
						PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - added axis-aligned no_player_spawning zone ", iZone, " between ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], " and ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1])
					BREAK
					CASE ciFMMC_ZONE_SHAPE__SPHERE
						iSpawnOcclusionIndex[iZone] = ADD_MISSION_SPAWN_OCCLUSION_SPHERE(vZoneCoords_0, GET_PLACED_ZONE_RADIUS(iZone))
						PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - added spherical no_player_spawning zone ", iZone, " centered at ", vZoneCoords_0, " w radius ", GET_PLACED_ZONE_RADIUS(iZone))
					BREAK
					CASE ciFMMC_ZONE_SHAPE__ANGLED_AREA
						iSpawnOcclusionIndex[iZone] = ADD_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(vZoneCoords_0, vZoneCoords_1, GET_PLACED_ZONE_RADIUS(iZone))
						PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - added angled area no_player_spawning zone ", iZone, ", coords 1 ", vZoneCoords_0, " coords 2 ", vZoneCoords_1, " width ", GET_PLACED_ZONE_RADIUS(iZone))
					BREAK
				ENDSWITCH
			ENDIF
		BREAK

		CASE ciFMMC_ZONE_TYPE__WATER_DAMPENING
			IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue != 1.0
				IF iZoneWaterCalmingQuadID[iZone] = -1
					INT iQuad
					iQuad = GET_FREE_WATER_CALMING_QUAD()

					IF iQuad > -1
					AND iQuad < FMMC_MAX_WATER_CALMING_QUADS
					AND iWaterCalmingQuad[iQuad] = -1
						iWaterCalmingQuad[iQuad] = ADD_EXTRA_CALMING_QUAD(vZoneCoords_0.x, vZoneCoords_0.y, vZoneCoords_1.x, vZoneCoords_1.y, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue)
						iZoneWaterCalmingQuadID[iZone] = iQuad
						PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - Added water calming quad ", iZone, " as iQuad ", iQuad)
					ELSE
						#IF IS_DEBUG_BUILD
						CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CREATE_THIS_ZONE - Trying to create too many water dampening zones/quads! There is a maximum of 8.")
						PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - Trying to create too many water dampening zones/quads! There is a maximum of 8.")
						ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_TOO_MANY_WATER_DAMP_ZONES, ENTITY_RUNTIME_ERROR_TYPE_WARNING_ZONE, "Too many Water Dampening zones already!", iZone)
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__GPS
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - Trying to add GPS disabled zone.")
			
			IF iGPSDisabledZones[iZone] = -1
				INT iGPSZone
				iGPSZone = GET_NEXT_GPS_DISABLED_ZONE_INDEX()
				IF iGPSZone != -1
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - GPS_DISABLED_ZONE SET_GPS_DISABLED_ZONE_AT_INDEX called")
					SET_GPS_DISABLED_ZONE_AT_INDEX(vZoneCoords_0, vZoneCoords_1, iGPSZone)
					iGPSDisabledZones[iZone] = iGPSZone								
				ELSE
					SET_BIT(iLocalZoneFailedCreationBitset, iZone)
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - GPS_DISABLED_ZONE No free GPS disabled zone slots for zone ", iZone)
					CASSERTLN(DEBUG_CONTROLLER,"CREATE_THIS_ZONE - GPS_DISABLED_ZONE No GPS disabled zones available.")
				ENDIF
				
				PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - GPS_DISABLED_ZONE GET_NEXT_GPS_DISABLED_ZONE_INDEX index: ", iZone)
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__DISPATCH_SPAWN_BLOCK
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - Trying to add Block Dispatch zone.")
			IF iDispatchBlockZone[iZone] = -1
				SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType
					CASE ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
						VECTOR v1, v2
						FLOAT fWidth
						fWidth = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1].x - g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0].x
						
						v1 = vZoneCoords_0 + <<fWidth/2, 0, 0>>
						v2 = vZoneCoords_1 - <<fWidth/2, 0, 0>>
						
						PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - Trying to add Dispatch axis-aligned disabled zone. vPos[0] = ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], " vPos[1] = ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1],"; converted: v1 ",v1," v2 ",v2," fWidth ", fWidth)
						iDispatchBlockZone[iZone] = ADD_DISPATCH_SPAWN_ANGLED_BLOCKING_AREA(v1, v2, fWidth)
					BREAK
					CASE ciFMMC_ZONE_SHAPE__SPHERE
						PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - Adding Block Dispatch Spawn Sphere")
						iDispatchBlockZone[iZone] = ADD_DISPATCH_SPAWN_SPHERE_BLOCKING_AREA(vZoneCoords_0, GET_PLACED_ZONE_RADIUS(iZone))
					BREAK
					CASE ciFMMC_ZONE_SHAPE__ANGLED_AREA
						PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - Adding Block Dispatch Spawn Area")
						iDispatchBlockZone[iZone] = ADD_DISPATCH_SPAWN_ANGLED_BLOCKING_AREA(vZoneCoords_0, vZoneCoords_1, GET_PLACED_ZONE_RADIUS(iZone))
					BREAK
				ENDSWITCH
			ENDIF
			
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - area been given id: ", iDispatchBlockZone[iZone])
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__EXPLOSION_AREA
			SET_BIT(iLocalBoolCheck3, LBOOL3_EXPLOSIVE_ZONE_ACTIVE)
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__SPAWN_PROTECTION
			IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)
			AND NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
				PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - ciFMMC_ZONE_TYPE__SPAWN_PROTECTION: ", iZone)
				iZoneTeam = ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue)
				
				IF iZoneTeam > -1
				AND iZoneTeam < FMMC_MAX_TEAMS
					IF MC_serverBD.iNumStartingPlayers[iZoneTeam] > 0
						IF iFMMCAirDefenceZoneArea[iZone] = -1
							IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__SPHERE
								iFMMCAirDefenceZoneArea[iZone] = CREATE_AIR_DEFENCE_SPHERE(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], GET_PLACED_ZONE_RADIUS(iZone), g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], WEAPONTYPE_INVALID)
								
								IF ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue) = MC_PlayerBD[iLocalPart].iTeam
									SET_PLAYER_TARGETTABLE_FOR_AIR_DEFENCE_SPHERE(NATIVE_TO_INT(LocalPlayer), iFMMCAirDefenceZoneArea[iZone], FALSE)
								ELSE
									SET_PLAYER_TARGETTABLE_FOR_AIR_DEFENCE_SPHERE(NATIVE_TO_INT(LocalPlayer), iFMMCAirDefenceZoneArea[iZone], TRUE)
								ENDIF
							ELSE
								VECTOR vMidPoint
								vMidPoint = (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0] + g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1])
								vMidPoint = <<vMidPoint.x / 2, vMidPoint.y / 2, vMidPoint.z / 2>>
								iFMMCAirDefenceZoneArea[iZone] = CREATE_AIR_DEFENCE_ANGLED_AREA(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1], GET_PLACED_ZONE_RADIUS(iZone), vMidPoint, WEAPONTYPE_INVALID)
								
								IF ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue) = MC_PlayerBD[iLocalPart].iTeam
									SET_PLAYER_TARGETTABLE_FOR_AIR_DEFENCE_SPHERE(NATIVE_TO_INT(LocalPlayer), iFMMCAirDefenceZoneArea[iZone], FALSE)
								ELSE
									SET_PLAYER_TARGETTABLE_FOR_AIR_DEFENCE_SPHERE(NATIVE_TO_INT(LocalPlayer), iFMMCAirDefenceZoneArea[iZone], TRUE)
								ENDIF
							ENDIF
							
							// So we don't play the sound of entering the zone...
							IF IS_ZONE_TRIGGERING(iZone)
								SET_BIT(iSpawnProtectionZoneDefenceBS, LBOOLZONE_PLAYER_JUST_LEFT_DEFENSE_SPHERE + iZone)
							ENDIF
						ENDIF
						PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - Added Spawn Protection zone number: ", iZone)
					ELSE
						PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - iTeam: ", iZoneTeam, " is not active, therefore we will not create the Zone.")
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - iTeam: ", iZoneTeam, " Bad value assigned in the creator: g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue, " i = ", iZone)
				ENDIF
			ELSE
				SET_BIT(iZonesWaitingForCreationBS, iZone)
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__ACTIVATE_ROADS
			SET_ROADS_IN_AREA(vZoneCoords_0, vZoneCoords_1, TRUE, FALSE)
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - Setting roads in area to true: ", vZoneCoords_0, " - ", vZoneCoords_1)
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__NAV_BLOCKING
			VECTOR vZoneMiddle, vZoneSize
			FLOAT fZTemp
			FLOAT fHeading
			vZoneMiddle = GET_ZONE_RUNTIME_CENTRE(iZone)
			
			fHeading = GET_HEADING_BETWEEN_VECTORS(vZoneCoords_1, vZoneCoords_0)
			fHeading += 90.0
			
			vZoneSize = vZoneCoords_1 - vZoneCoords_0						
			fZTemp = ABSF(vZoneSize.z)
			
			vZoneSize.z = 0   // Flatten for VMAG
			vZoneSize.x = VMAG(vZoneSize)
			vZoneSize.y = GET_PLACED_ZONE_RADIUS(iZone)
			vZoneSize.z = fZTemp
			
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - ciFMMC_ZONE_TYPE__NAV_BLOCKING | vZoneMiddle: ",vZoneMiddle,", Size: ", vZoneSize, " fHeading: ", fHeading)
			iNavBlockerZone[iZone] = ADD_NAVMESH_BLOCKING_OBJECT(vZoneMiddle, vZoneSize, DEG_TO_RAD(fHeading), DEFAULT, BLOCKING_OBJECT_ALLPATHS)						
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__AIR_DEFENCE
			IF iFMMCAirDefenceZoneArea[iZone] = -1
			
				IF IS_THIS_ZONE_TRIGGERED_WHEN_LOCAL_PLAYER_IS_INSIDE(iZone)
				AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_AIR_DEFENCE_IGNORE_PROJECTILES)
					SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType
						CASE ciFMMC_ZONE_SHAPE__SPHERE
							iFMMCAirDefenceZoneArea[iZone] = CREATE_AIR_DEFENCE_SPHERE(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], GET_PLACED_ZONE_RADIUS(iZone), g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0])
						BREAK
						
						CASE ciFMMC_ZONE_SHAPE__ANGLED_AREA
							iFMMCAirDefenceZoneArea[iZone] = CREATE_AIR_DEFENCE_ANGLED_AREA(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1], GET_PLACED_ZONE_RADIUS(iZone), GET_ZONE_RUNTIME_CENTRE(iZone))
						BREAK
						
						DEFAULT
							PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - ciFMMC_ZONE_TYPE__AIR_DEFENCE | The shape of this Zone doesn't support the native Air Defence functionality.")
						BREAK
					ENDSWITCH
					
					IF DOES_AIR_DEFENCE_SPHERE_EXIST(iFMMCAirDefenceZoneArea[iZone])
						SET_PLAYER_TARGETTABLE_FOR_AIR_DEFENCE_SPHERE(NATIVE_TO_INT(LocalPlayer), iFMMCAirDefenceZoneArea[iZone], FALSE)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__BLOCK_ROAD_NODES
			SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType
				CASE ciFMMC_ZONE_SHAPE__ANGLED_AREA
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - ciFMMC_ZONE_TYPE__BLOCK_ROAD_NODES | Calling SET_ROADS_IN_ANGLED_AREA with vZoneCoords_0: ",vZoneCoords_0,", vZoneCoords_1: ", vZoneCoords_1, " fRadius: ",  GET_PLACED_ZONE_RADIUS(iZone))
					SET_ROADS_IN_ANGLED_AREA(vZoneCoords_0, vZoneCoords_1, GET_PLACED_ZONE_RADIUS(iZone), FALSE, FALSE, FALSE)
				BREAK
				
				CASE ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - ciFMMC_ZONE_TYPE__BLOCK_ROAD_NODES | Calling SET_ROADS_IN_AREA with vZoneCoords_0: ",vZoneCoords_0,", vZoneCoords_1: ", vZoneCoords_1)
					SET_ROADS_IN_AREA(vZoneCoords_0, vZoneCoords_1, FALSE, FALSE)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__ENABLE_ROAD_NODES
			SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType
				CASE ciFMMC_ZONE_SHAPE__ANGLED_AREA
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - ciFMMC_ZONE_TYPE__ENABLE_ROAD_NODES | Calling SET_ROADS_IN_ANGLED_AREA with vZoneCoords_0: ",vZoneCoords_0,", vZoneCoords_1: ", vZoneCoords_1, " fRadius: ",  GET_PLACED_ZONE_RADIUS(iZone))
					SET_ROADS_IN_ANGLED_AREA(vZoneCoords_0, vZoneCoords_1, GET_PLACED_ZONE_RADIUS(iZone), FALSE, TRUE, FALSE)
				BREAK
				
				CASE ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - ciFMMC_ZONE_TYPE__ENABLE_ROAD_NODES | Calling SET_ROADS_IN_AREA with vZoneCoords_0: ",vZoneCoords_0,", vZoneCoords_1: ", vZoneCoords_1)
					SET_ROADS_IN_AREA(vZoneCoords_0, vZoneCoords_1, TRUE, FALSE)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__BOUNDS
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "][Bounds] CREATE_THIS_ZONE - ciFMMC_ZONE_TYPE__BOUNDS | Creating Bounds Zone now!")
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__PETROL_POOL
			PRINTLN("[Zones][Zone ", iZone, "] CREATE_THIS_ZONE - ciFMMC_ZONE_TYPE__PETROL_POOL | Creating Petrol Pool Zone now!")
			diPetrolDecals[iZone] = ADD_PETROL_DECAL(vZoneCoords_0, GET_PLACED_ZONE_RADIUS(iZone), GET_PLACED_ZONE_RADIUS(iZone), 1.0)
		BREAK
		
	ENDSWITCH
	
	IF IS_BIT_SET(iZonesWaitingForCreationBS, iZone)
	AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType = ciFMMC_ZONE_TYPE__SPAWN_PROTECTION
		RENDER_SPAWN_PROTECTION_SPHERES_EARLY()
		PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] CREATE_THIS_ZONE - ciFMMC_ZONE_TYPE__SPAWN_PROTECTION Delaying creation until everyone is ready for data reasons | iZone: ", iZone)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalZoneFailedCreationBitset, iZone)
		SET_BIT(iZoneCreatedBS, iZone)
	ELSE
		PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] iLocalZoneFailedCreationBitset is SET. This means there is no free slot until a zone is first deleted. Not setting iZoneCreatedBS so that we can loop in here again")
	ENDIF
ENDPROC

PROC CACHE_ZONE_RESPAWN_BLOCKED(INT i)
	IF NOT IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iMiscBS, ciRandomSpawnGroupBS_HasBeenSet)
		EXIT
	ENDIF	
	SET_BIT(iZoneRespawnIsBlockedBS, i)
	PRINTLN("[RCC MISSION][Zones][Zone ", i, "] SHOULD_CREATE_ZONE - CACHE_ZONE_RESPAWN_BLOCKED - Caching that the Zone has been blocked from respawning.")
ENDPROC

PROC CACHE_ZONE_SHOULD_NOT_RESPAWN_THIS_FRAME(INT i)
	SET_BIT(iZoneRespawnIsBlockedThisFrameBS, i)
ENDPROC

PROC CACHE_ZONE_SHOULD_RESPAWN_THIS_FRAME(INT i)
	SET_BIT(iZoneShouldRespawnNowBS, i)
ENDPROC

PROC PROCESS_ZONE_RESIZING(INT iZone)
	
	IF fZoneRadiusCached[iZone] = 0
		PRINTLN("[Zones][Zone ", iZone, "] PROCESS_ZONE_RESIZING - Caching radius for the first time: ", GET_PLACED_ZONE_RADIUS(iZone))
		fZoneRadiusCached[iZone] = GET_PLACED_ZONE_RADIUS(iZone)
	ENDIF

	IF GET_PLACED_ZONE_RADIUS(iZone) < fZoneRadiusCached[iZone]
		PRINTLN("[Zones][Zone ", iZone, "] PROCESS_ZONE_RESIZING - Resizing Zone: ", GET_PLACED_ZONE_RADIUS(iZone))
		REMOVE_ZONE(iZone)
		SET_BIT(iZoneNeedsResizingBS, iZone)
		
		//We need to clear this bit so the zone can be recreated
		CLEAR_BIT(iLocalZoneRemovedBitset, iZone)
		
		fZoneRadiusCached[iZone] = GET_PLACED_ZONE_RADIUS(iZone)
	ENDIF

ENDPROC

PROC PROCESS_ZONE_MID_MISSION_REMOVAL(INT iZone)
	IF DOES_ZONE_EXIST(iZone)
		IF SHOULD_ZONE_BE_REMOVED(iZone)
			REMOVE_ZONE(iZone)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_ZONE_WAIT_FOR_GAME_STATE_RUNNING(INT iType)
	SWITCH iType
		CASE ciFMMC_ZONE_TYPE__ENABLE_ROAD_NODES
			IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH 
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CREATE_ZONE(INT iZone, INT iTeam, BOOL bSkipSpawningFlagCheck = FALSE, BOOL bSkipSpeedCheck = FALSE)

	IF IS_BIT_SET(iZoneRespawnIsBlockedBS, iZone)
		RETURN FALSE
	ENDIF	
	IF IS_BIT_SET(iZoneRespawnIsBlockedThisFrameBS, iZone)
		RETURN FALSE
	ENDIF	
	IF IS_BIT_SET(iZoneShouldRespawnNowBS, iZone)
		PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] SHOULD_CREATE_ZONE | iZone: ", iZone, " | iTeam: ", iTeam, " | Returning TRUE due to iZoneShouldRespawnNowBS")
		RETURN TRUE
	ENDIF
	
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	INT iZoneStartPriority 	= g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneStartPriority[iTeam] // '-2' is Off. '-1' is always On. >= 0 is a specific rule.
	
	IF MC_SHOULD_WE_START_FROM_CHECKPOINT()	
		INT iCheckPoint = MC_GET_MISSION_STARTING_CHECKPOINT()
		IF iRule < g_FMMC_Struct.iRestartRule[iTeam][iCheckpoint]
			iRule = g_FMMC_Struct.iRestartRule[iTeam][iCheckpoint]
		ENDIF
	ENDIF
	
	PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] SHOULD_CREATE_ZONE | iZone: ", iZone, " | iTeam: ", iTeam, " | iZoneStartPriority: ", iZoneStartPriority)
	
	IF iTeam < 0
	OR iTeam >= FMMC_MAX_TEAMS
	OR IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0])
		CACHE_ZONE_RESPAWN_BLOCKED(iZone)
		RETURN FALSE
	ENDIF
	
	IF NOT bSkipSpeedCheck
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_ATTACH_ZONE_TO_LINKED_ENTITY)
	AND IS_ZONE_TYPE_STATIC(iZone)
		ENTITY_INDEX eiEntity = GET_ZONE_LINKED_ENTITY(iZone)
		IF DOES_ENTITY_EXIST(eiEntity)
			FLOAT fSpeed = FMMC_GET_ENTITY_CURRENT_VELOCITY_MAGNITUDE(eiEntity)
			IF fSpeed > 0.5 // Tolerance for physics/net inaccuracies.
				PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] SHOULD_CREATE_ZONE | Zone ", iZone, " fSpeed: ", fSpeed)
				CACHE_ZONE_SHOULD_NOT_RESPAWN_THIS_FRAME(iZone)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalZoneRemovedBitset, iZone)
		PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] SHOULD_CREATE_ZONE | Zone ", iZone, " has already been removed")
		CACHE_ZONE_SHOULD_NOT_RESPAWN_THIS_FRAME(iZone)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_ZONE_BE_REMOVED(iZone)
		PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] SHOULD_CREATE_ZONE | Returning FALSE because Zone ", iZone, " should be removed")
		CACHE_ZONE_SHOULD_NOT_RESPAWN_THIS_FRAME(iZone)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_ZONE_A_BOUNDS_ZONE(iZone) // Bounds Zones don't use these priority options
		IF iZoneStartPriority = -2 // zone data not set
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] SHOULD_CREATE_ZONE | Return FALSE, iZoneStartPriority is -2 (no data set) for ", iZone)
			CACHE_ZONE_SHOULD_NOT_RESPAWN_THIS_FRAME(iZone)
			RETURN FALSE
		ENDIF
		
		IF iRule < iZoneStartPriority
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] SHOULD_CREATE_ZONE | Return FALSE, haven't reached the starting rule for Zone ", iZone)
			CACHE_ZONE_SHOULD_NOT_RESPAWN_THIS_FRAME(iZone)
			RETURN FALSE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_ACTIVATE_ON_MIDPOINT)
		AND iRule = iZoneStartPriority // Only worry about the midpoint on the Zone's start rule, not any following rules
			IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iZoneStartPriority)
				PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] SHOULD_CREATE_ZONE | Zone ", iZone, " is waiting for the rule's midpoint")
				SET_BIT(iZonesWaitingForCreationBS, iZone)
				CACHE_ZONE_SHOULD_NOT_RESPAWN_THIS_FRAME(iZone)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iZone, eSGET_Zone)
		PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] SHOULD_CREATE_ZONE | Zone ", iZone, " doesn't have a suitable spawn group")
		CACHE_ZONE_RESPAWN_BLOCKED(iZone)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].eSpawnConditionFlag[0] != SPAWN_CONDITION_FLAG_NOT_SET
		IF NOT bSkipSpawningFlagCheck
		AND MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].eSpawnConditionFlag, FALSE, FALSE, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAggroIndexBS_Entity_Zone, MC_PlayerBD[iPartToUse].iTeam)
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] SHOULD_CREATE_ZONE | Zone ", iZone, " Is being blocked due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")
			CACHE_ZONE_SHOULD_NOT_RESPAWN_THIS_FRAME(iZone)
			RETURN FALSE
		ENDIF
	ENDIF
		
	IF SHOULD_ZONE_WAIT_FOR_GAME_STATE_RUNNING(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType)
		PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] SHOULD_CREATE_ZONE | Zone ", iZone, " Waiting for game state running.")
		CACHE_ZONE_SHOULD_NOT_RESPAWN_THIS_FRAME(iZone)
		RETURN FALSE
	ENDIF
	
	
	CACHE_ZONE_SHOULD_RESPAWN_THIS_FRAME(iZone)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_CREATE_ZONE_STAGGERED(INT iZone)
	
	IF MC_ARE_SPAWN_CONDITION_FLAGS_MET_DURING_GAMEPLAY(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].eSpawnConditionFlag)
	AND MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].eSpawnConditionFlag, FALSE, FALSE, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAggroIndexBS_Entity_Zone, MC_PlayerBD[iPartToUse].iTeam)					
		PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] SHOULD_CREATE_ZONE_STAGGERED | Should Create Zone = False. Spawning Flags are Blocking due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_ATTACH_ZONE_TO_LINKED_ENTITY)
	AND IS_ZONE_TYPE_STATIC(iZone)
		ENTITY_INDEX eiEntity = GET_ZONE_LINKED_ENTITY(iZone)
		IF DOES_ENTITY_EXIST(eiEntity)
			FLOAT fSpeed = FMMC_GET_ENTITY_CURRENT_VELOCITY_MAGNITUDE(eiEntity)
			IF fSpeed > 0.5 // Tolerance for physics/net inaccuracies.
				PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] SHOULD_CREATE_ZONE_STAGGERED | Should Create Zone = False. Linked Entity is still moving.")
				RETURN FALSE			
			ENDIF				
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CHECK_SHOULD_CREATE_ZONE_STAGGERED(INT iZone)
	IF MC_ARE_SPAWN_CONDITION_FLAGS_MET_DURING_GAMEPLAY(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].eSpawnConditionFlag)
		RETURN TRUE
	ENDIF
	
	IF IS_ZONE_TYPE_STATIC(iZone)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_ATTACH_ZONE_TO_LINKED_ENTITY)
			RETURN TRUE
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC

PROC HANDLE_ZONE_CREATION_DELAY_TIMER(INT iZone, BOOL& bShouldCreateZone)
	
	IF NOT DOES_ZONE_USE_A_CREATION_DELAY_TIMER(iZone)
		EXIT
	ENDIF
	
	IF bShouldCreateZone
		IF NOT HAS_NET_TIMER_STARTED(stZoneCreationDelayTimers[iZone])
			REINIT_NET_TIMER(stZoneCreationDelayTimers[iZone])
			bShouldCreateZone = FALSE
			SET_BIT(iZonesWaitingForCreationBS, iZone)
			PRINTLN("[Zones][Zone ", iZone, "] HANDLE_ZONE_CREATION_DELAY_TIMER |Starting Zone delay timer for zone ", iZone, "! | Delay length: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneDelay)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(stZoneCreationDelayTimers[iZone], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneDelay)
				// Timer complete!
				RESET_NET_TIMER(stZoneCreationDelayTimers[iZone])
				PRINTLN("[Zones][Zone ", iZone, "] HANDLE_ZONE_CREATION_DELAY_TIMER | Zone ", iZone, " creation delay timer just finished!")
			ELSE
				bShouldCreateZone = FALSE
				SET_BIT(iZonesWaitingForCreationBS, iZone)
				PRINTLN("[Zones][Zone ", iZone, "] HANDLE_ZONE_CREATION_DELAY_TIMER | Zone ", iZone, " is waiting for its creation delay timer")
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(stZoneCreationDelayTimers[iZone])
			RESET_NET_TIMER(stZoneCreationDelayTimers[iZone])
			PRINTLN("[Zones][Zone ", iZone, "] HANDLE_ZONE_CREATION_DELAY_TIMER | Zone ", iZone, " | Resetting stZoneCreationDelayTimers[", iZone, "] because this Zone is no longer valid for creation")
		ENDIF
	ENDIF
ENDPROC

PROC CREATE_ZONES(INT iLocalTeam)

	PRINTLN("[Zones] CREATE_ZONES | iLocalTeam: ", iLocalTeam)
	
	INT iZone
	FOR iZone = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfZones - 1
		
		BOOL bShouldCreateZone = FALSE
		
		IF DOES_ZONE_EXIST(iZone)
			RELOOP
		ENDIF
		
		CLEAR_BIT(iZonesWaitingForCreationBS, iZone)
		CLEAR_BIT(iLocalZoneFailedCreationBitset, iZone)
		CLEAR_BIT(iZoneNeedsResizingBS, iZone)
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_APPLY_TO_ALL_TEAMS) // the zone bit for all teams is set
			INT iTeam
			FOR iTeam = 0 TO g_FMMC_STRUCT.iNumberOfTeams - 1
				IF SHOULD_CREATE_ZONE(iZone, iTeam, FALSE)
					bShouldCreateZone = TRUE
					BREAKLOOP
				ENDIF
			ENDFOR
		ELSE
			bShouldCreateZone = SHOULD_CREATE_ZONE(iZone, iLocalTeam, FALSE)
		ENDIF
		
		HANDLE_ZONE_CREATION_DELAY_TIMER(iZone, bShouldCreateZone)
	
		IF bShouldCreateZone
			CREATE_THIS_ZONE(iZone)
		ENDIF
	ENDFOR
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: PROCESS ZONES ---------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing logic which needs to run regularly on active zones ----------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// This doesn't work at the moment, starts as set to 0.
PROC FIND_AND_STORE_NO_WEAPONS_ZONE()
	INT i
	IF iStoredNoWeaponZone = -1
		FOR i = 0 TO FMMC_MAX_NUM_ZONES - 1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iType = ciFMMC_ZONE_TYPE__BLOCK_WEAPONS
				PRINTLN("[RCC MISSION][EXPLODE]FIND_NO_WEAPONS_ZONE iStoredNoWeaponZone = ", i)
				iStoredNoWeaponZone = i
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC KEEP_OBJECT_IN_ZONE(OBJECT_INDEX Obj, INT iZone)
	VECTOR vObjCoords = GET_ENTITY_COORDS(Obj)
	PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - vObjCoords = ", vObjCoords)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__ANGLED_AREA
		PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - Angled area")
		IF NOT IS_ENTITY_IN_ANGLED_AREA(Obj, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1], GET_PLACED_ZONE_RADIUS(iZone))
			vObjCoords = GET_CLOSEST_POINT_AT_EDGE_OF_BOUNDS(vObjCoords, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1], GET_PLACED_ZONE_RADIUS(iZone))
			PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - vObjCoords at edge: ", vObjCoords)
			FLOAT fZ
			IF GET_GROUND_Z_FOR_3D_COORD(vObjCoords, fZ)
				vObjCoords.z = fZ
				vObjCoords.z = CHECK_Z_FOR_OVERHANG(vObjCoords)
				PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - GET_GROUND_Z_FOR_3D_COORD returned true")
			ELSE
				PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - GET_GROUND_Z_FOR_3D_COORD returned false")
				vObjCoords = (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0] + g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1]) * 0.5
				IF GET_GROUND_Z_FOR_3D_COORD(vObjCoords, fZ)
					vObjCoords.z = fZ
					PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - BACKUP PLACING AT CENTER -  FOUND A VALID GROUND Z = ", fZ)
				ELSE
					PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - BACKUP PLACING AT CENTER -  NO VALID Z FOUND, LIKELY SOME STRANGE PLACEMENT")
				ENDIF
				PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - BACKUP PLACING AT CENTER - new coords: ", vObjCoords)
			ENDIF
			PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - moving obj to new coords: ", vObjCoords)
			SET_ENTITY_COORDS(Obj, vObjCoords)
		ENDIF
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__SPHERE
		PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - Sphere area")
		IF VDIST2(vObjCoords, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0]) > POW(GET_PLACED_ZONE_RADIUS(iZone), 2)
			VECTOR vDir = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0] - vObjCoords
			vDir = NORMALISE_VECTOR(vDir)
			vObjCoords = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0] + (vDir * GET_PLACED_ZONE_RADIUS(iZone))
			GET_GROUND_Z_FOR_3D_COORD(vObjCoords, vObjCoords.Z)
			PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - moving obj to new coords: ", vObjCoords)
			SET_ENTITY_COORDS(Obj, vObjCoords)
		ENDIF
	ENDIF
ENDPROC

PROC KEEP_OBJECT_OUT_OF_ZONE(OBJECT_INDEX Obj, INT iZone, INT iObj)
	VECTOR vObjCoords = GET_ENTITY_COORDS(Obj)
	PRINTLN("[JS] - KEEP_OBJECT_OUT_OF_ZONE - vObjCoords = ", vObjCoords)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__ANGLED_AREA
		PRINTLN("[JS] - KEEP_OBJECT_OUT_OF_ZONE - Angled area")
		IF IS_ENTITY_IN_ANGLED_AREA(Obj, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1], GET_PLACED_ZONE_RADIUS(iZone))
			vObjCoords = GET_CLOSEST_POINT_AT_EDGE_OF_BOUNDS(vObjCoords, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1], GET_PLACED_ZONE_RADIUS(iZone) + 1.0)
			PRINTLN("[JS] - KEEP_OBJECT_OUT_OF_ZONE - vObjCoords at edge: ", vObjCoords)
			FLOAT fZ
			IF GET_GROUND_Z_FOR_3D_COORD(vObjCoords, fZ)
				vObjCoords.z = fZ
				vObjCoords.z = CHECK_Z_FOR_OVERHANG(vObjCoords)
				PRINTLN("[JS] - KEEP_OBJECT_OUT_OF_ZONE - GET_GROUND_Z_FOR_3D_COORD returned true")
			ELSE
				PRINTLN("[JS] - KEEP_OBJECT_OUT_OF_ZONE - GET_GROUND_Z_FOR_3D_COORD returned false putting back at spawn")
				vObjCoords = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vPos
			ENDIF
			PRINTLN("[JS] - KEEP_OBJECT_OUT_OF_ZONE - moving obj to new coords: ", vObjCoords)
			SET_ENTITY_COORDS(Obj, vObjCoords)
		ENDIF
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__SPHERE
		PRINTLN("[JS] - KEEP_OBJECT_OUT_OF_ZONE - Sphere area")
		IF VDIST2(vObjCoords, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0]) < POW(GET_PLACED_ZONE_RADIUS(iZone), 2)
			VECTOR vDir = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0] - vObjCoords
			vDir = NORMALISE_VECTOR(vDir)
			vObjCoords = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0] + (vDir * GET_PLACED_ZONE_RADIUS(iZone) + 1.0)
			GET_GROUND_Z_FOR_3D_COORD(vObjCoords + 1.0, vObjCoords.Z)
			PRINTLN("[JS] - KEEP_OBJECT_OUT_OF_ZONE - moving obj to new coords: ", vObjCoords)
			SET_ENTITY_COORDS(Obj, vObjCoords)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_ZONE_RADIUS_CHANGES(INT iZone)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fGrowthPercent = 0.0
		EXIT
	ENDIF

	FLOAT fGrowthPercent = (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fGrowthPercent*0.01)
	BOOL bReady
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_START_ACTIVATION_ON_PLAYER_ENTRY)
	OR IS_BIT_SET(iZoneBS_RadiusChangeTriggered, iZone)
		bReady = TRUE
	ELSE
		IF IS_ZONE_TRIGGERING(iZone)
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedContentEventType_ActivateZoneRadiusChange, DEFAULT, DEFAULT, DEFAULt, DEFAULT, DEFAULT, iZone)
			SET_BIT(iZoneBS_RadiusChangeTriggered, iZone)
		ENDIF
	ENDIF
	
	// Init for lerping.
	IF NOT HAS_NET_TIMER_STARTED(td_ZoneRadiusOverrideTimer[iZone])
	AND bReady 
		START_NET_TIMER(td_ZoneRadiusOverrideTimer[iZone])
		fZoneRadiusOverride[iZone] = GET_PLACED_ZONE_RADIUS(iZone)
	ENDIF
	
	// Lerpy time.
	IF HAS_NET_TIMER_EXPIRED_READ_ONLY(td_ZoneRadiusOverrideTimer[iZone], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iRadiusSecondaryActivationTimer*1000)
	AND HAS_NET_TIMER_STARTED(td_ZoneRadiusOverrideTimer[iZone])
		// Radius and Width.
		FLOAT fDiff = (ABSF(GET_PLACED_ZONE_RADIUS(iZone) - (GET_PLACED_ZONE_RADIUS(iZone)*fGrowthPercent)) * 1000)						
		FLOAT fDiffThisFrame = (fDiff / (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iRadiusSecondaryInterpTime * 1000)) * GET_FRAME_TIME()			
		IF (GET_PLACED_ZONE_RADIUS(iZone)*fGrowthPercent) > GET_PLACED_ZONE_RADIUS(iZone)
			IF fZoneRadiusOverride[iZone] < (GET_PLACED_ZONE_RADIUS(iZone)*fGrowthPercent)
				fZoneRadiusOverride[iZone] += fDiffThisFrame					
				IF fZoneRadiusOverride[iZone] > (GET_PLACED_ZONE_RADIUS(iZone)*fGrowthPercent)
					fZoneRadiusOverride[iZone] = (GET_PLACED_ZONE_RADIUS(iZone)*fGrowthPercent)
				ENDIF
			ELSE
				fZoneRadiusOverride[iZone] = (GET_PLACED_ZONE_RADIUS(iZone)*fGrowthPercent)
			ENDIF
		ELSE
			IF fZoneRadiusOverride[iZone] > (GET_PLACED_ZONE_RADIUS(iZone)*fGrowthPercent)
				fZoneRadiusOverride[iZone] -= fDiffThisFrame
				IF fZoneRadiusOverride[iZone] < (GET_PLACED_ZONE_RADIUS(iZone)*fGrowthPercent)
					fZoneRadiusOverride[iZone] = (GET_PLACED_ZONE_RADIUS(iZone)*fGrowthPercent)
				ENDIF
			ELSE
				fZoneRadiusOverride[iZone] = (GET_PLACED_ZONE_RADIUS(iZone)*fGrowthPercent)
			ENDIF
		ENDIF
		
		// Areas
		FLOAT fDiffGrow = (ABSF(1.0 - (1.0*fGrowthPercent)) * 1000)
		FLOAT fDiffGrowThisFrame = (fDiffGrow / (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iRadiusSecondaryInterpTime * 1000)) * GET_FRAME_TIME()	
		IF fGrowthPercent > 1.0
			IF (fZoneSizeMultiplier[iZone] + fDiffGrowThisFrame) < fGrowthPercent
				fZoneSizeMultiplier[iZone] += fDiffGrowThisFrame
				IF fZoneSizeMultiplier[iZone] > fGrowthPercent
					fZoneSizeMultiplier[iZone] = fGrowthPercent
				ENDIF
			ELSE
				fZoneSizeMultiplier[iZone] = fGrowthPercent
			ENDIF
		ELSE
			IF (fZoneSizeMultiplier[iZone] + fDiffGrowThisFrame) > fGrowthPercent
				fZoneSizeMultiplier[iZone] -= fDiffGrowThisFrame
				IF fZoneSizeMultiplier[iZone] < fGrowthPercent
					fZoneSizeMultiplier[iZone] = fGrowthPercent
				ENDIF
			ELSE
				fZoneSizeMultiplier[iZone] = fGrowthPercent
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_ZONE_SMOOTH_ATTACHMENT_OFFSET(INT iZone)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkType = ciENTITY_TYPE_NONE
	OR NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_ATTACH_TO_LINKED_ENTITY_WITH_OFFSET)
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iSmoothZoneAttachmentOffsetSlot = -1
		EXIT
	ENDIF
	
	ENTITY_INDEX eiLinkedEntity = GET_ZONE_LINKED_ENTITY(iZone)
	INT iAttachmentOffsetSlot = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iSmoothZoneAttachmentOffsetSlot
	
	IF NOT DOES_ENTITY_EXIST(eiLinkedEntity)
		EXIT
	ENDIF
	
	VECTOR vEntityCoords = GET_ENTITY_COORDS(eiLinkedEntity, FALSE)
	VECTOR vTargetPosThisFrame = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiLinkedEntity, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vZoneAttachmentOffset)
	
	IF IS_VECTOR_ZERO(vSmoothAttachedZonePos[iAttachmentOffsetSlot])
		vSmoothAttachedZonePos[iAttachmentOffsetSlot] = vTargetPosThisFrame
		fSmoothAttachedZoneDistance[iAttachmentOffsetSlot] = VDIST(vEntityCoords, vTargetPosThisFrame)
		PRINTLN("[Zones][Zone ", iZone, "][ZoneAttachment] PROCESS_ZONE_ATTACHMENT_OFFSET | Initialising offset now! Offset: ", vSmoothAttachedZonePos[iAttachmentOffsetSlot], " / Distance: ", fSmoothAttachedZoneDistance[iAttachmentOffsetSlot])
		
	ELSE
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_POINT_FROM_LINKED_ENTITY_TO_ALT_LINKED_ENTITY)
			VECTOR vAltEntityCoords = GET_COORDS_FROM_LINKED_ENTITY_TYPE(GET_LINKED_ENTITY_TYPE_FROM_ENTITY_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAltEntityLinkType), g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAltEntityLinkIndex)
			vTargetPosThisFrame = LERP_VECTOR(vEntityCoords, vAltEntityCoords, 0.5)
			
			#IF IS_DEBUG_BUILD
			IF bZoneDebug
				DRAW_DEBUG_LINE(vEntityCoords, vAltEntityCoords, 0, 0, 255)
			ENDIF
			#ENDIF
		ENDIF
	
		// Smoothly move the position to where it's meant to be
		vSmoothAttachedZonePos[iAttachmentOffsetSlot] = LERP_VECTOR(vSmoothAttachedZonePos[iAttachmentOffsetSlot], vTargetPosThisFrame, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fSmoothZoneAttachmentSpeed * GET_FRAME_TIME())
		
		// Clamp the distance that the Zone can be from its attachment entity so that the distance is always the same, it's just the offset that smoothly follows
		VECTOR vDirection = NORMALISE_VECTOR(vSmoothAttachedZonePos[iAttachmentOffsetSlot] - vEntityCoords)
		vSmoothAttachedZonePos[iAttachmentOffsetSlot] = vEntityCoords + (vDirection * fSmoothAttachedZoneDistance[iAttachmentOffsetSlot])
		
		#IF IS_DEBUG_BUILD
		IF bZoneDebug
			DRAW_DEBUG_LINE(vSmoothAttachedZonePos[iAttachmentOffsetSlot], vTargetPosThisFrame, 255, 255, 255)
			DRAW_DEBUG_SPHERE(vSmoothAttachedZonePos[iAttachmentOffsetSlot], 0.5, 255, 0, 0, 155)
			DRAW_DEBUG_SPHERE(vTargetPosThisFrame, 0.25, 0, 255, 0, 100)
			
			TEXT_LABEL_63 tlVisualDebug = "Zone slot: "
			tlVisualDebug += iAttachmentOffsetSlot
			DRAW_DEBUG_TEXT(tlVisualDebug, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], 255, 255, 255, 255)
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DRAW_BOUNDS_ZONE(INT iZone)
	
	UNUSED_PARAMETER(iZone)

	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iRuleBitsetEight[GET_LOCAL_PLAYER_CURRENT_RULE()], ciBS_RULE8_ONLY_SHOW_BOUNDS_WHEN_OOB)
	AND NOT IS_LOCAL_PLAYER_OUT_OF_BOUNDS()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DRAW_ZONE_BLIP(INT iZone)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_AREA_BLIP_ZONE)
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ZONE_EXIST(iZone)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkIndex > -1
		IF NOT DOES_ENTITY_EXIST(GET_ZONE_LINKED_ENTITY(iZone))
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF GET_FMMC_ZONE_TYPE(iZone) = ciFMMC_ZONE_TYPE__BOUNDS
		IF NOT SHOULD_DRAW_BOUNDS_ZONE(iZone)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fBlipRange != 0.0
	
		IF IS_VALID_INTERIOR(GET_INTERIOR_AT_COORDS(GET_ZONE_RUNTIME_CENTRE(iZone)))
		AND LocalPlayerCurrentInterior != GET_INTERIOR_AT_COORDS(GET_ZONE_RUNTIME_CENTRE(iZone))
			//Distance check is in an interior
			RETURN FALSE
		ENDIF
	
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_USE_3D_RANGE_CHECK_FOR_BLIP)
			IF VDIST(GET_ZONE_RUNTIME_CENTRE(iZone), vLocalPlayerPosition) > POW(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fBlipRange, 2)
				RETURN FALSE
			ENDIF
		ELSE
			IF VDIST2_2D(GET_ZONE_RUNTIME_CENTRE(iZone), vLocalPlayerPosition) > POW(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fBlipRange, 2)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_ONLY_BLIP_IF_TRIGGERING)
		IF NOT IS_ZONE_TRIGGERING(iZone)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN IS_ZONE_SAFE_TO_BLIP(iZone)
ENDFUNC

FUNC BOOL SHOULD_DRAW_ZONE_MARKER(INT iZone)
	
	#IF IS_DEBUG_BUILD
		IF FMMC_IS_LONG_BIT_SET(sContentOverviewDebug.iEntityStateDrawZoneDebugBS, iZone)
			RETURN TRUE	
		ENDIF
	#ENDIF
	
	IF NOT DOES_ZONE_EXIST(iZone)
		RETURN FALSE
	ENDIF
	
	IF GET_FMMC_ZONE_TYPE(iZone) = ciFMMC_ZONE_TYPE__BOUNDS
		IF NOT SHOULD_DRAW_BOUNDS_ZONE(iZone)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkIndex > -1
		IF NOT DOES_ENTITY_EXIST(GET_ZONE_LINKED_ENTITY(iZone))
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_DRAW_MARKER)
ENDFUNC

PROC SET_UP_ZONE_BLIP(BLIP_INDEX &biZoneBlip, INT iZone)
	SET_BLIP_COLOUR(biZoneBlip, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iBlipColor)
	SET_BLIP_ALPHA(biZoneBlip, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iBlipAlpha)
	
	IF DOES_ZONE_SHAPE_SUPPORT_LONG_RANGE_BLIPS(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_MAKE_BLIP_LONG_RANGE)
			SET_BLIP_AS_SHORT_RANGE(biZoneBlip, TRUE)
		ELSE
			SET_BLIP_MARKER_LONG_DISTANCE(biZoneBlip, TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC SET_ZONE_BLIP_SCALE_FLOATS(FLOAT &fScaleX, FLOAT &fScaleY, INT iZone)
	VECTOR vPos1 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0]
	VECTOR vPos2 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1]
	
	IF vPos1.x < vPos2.x
		fScaleX = vPos1.x - vPos2.x
	ELIF vPos1.x > vPos2.x
		fScaleX = vPos2.x - vPos1.x
	ENDIF
	IF vPos1.y < vPos2.y
		fScaleY = vPos1.y - vPos2.y
	ELIF vPos1.y > vPos2.y
		fScaleY = vPos2.y - vPos1.y
	ENDIF
ENDPROC

PROC PROCESS_CUSTOM_ZONE_BLIP_FUNCTIONALITY(BLIP_INDEX biZoneBlip, INT iZone)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iBlipFlashing_ShowEvery = 0
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(tdZoneBlipFlashTimer[iZone])
		START_NET_TIMER(tdZoneBlipFlashTimer[iZone])
	ENDIF
	
	INT iDurationToUse
	IF IS_BIT_SET(iZoneBlipHiddenBS, iZone)
		iDurationToUse = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iBlipFlashing_ShowEvery
	ELSE
		iDurationToUse = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iBlipFlashing_ShowDuration
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdZoneBlipFlashTimer[iZone], (iDurationToUse * 1000))
		IF GET_BLIP_ALPHA(biZoneBlip) > 0
			SET_BIT(iZoneBlipFadingBS, iZone)
		ELSE
			SET_BLIP_ALPHA(biZoneBlip, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iBlipAlpha)
			CLEAR_BIT(iZoneBlipHiddenBS, iZone)
		ENDIF
		REINIT_NET_TIMER(tdZoneBlipFlashTimer[iZone])
	ENDIF
	
	//Handle the blip fading
	IF IS_BIT_SET(iZoneBlipFadingBS, iZone)
		fCurrentFadeTime[iZone] += GET_FRAME_TIME()
		FLOAT fFadeTime = CLAMP((fCurrentFadeTime[iZone] / g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iBlipFlashing_FadeTime), 0.0, 1.0)
		SET_BLIP_ALPHA(biZoneBlip, FLOOR(LERP_FLOAT(TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iBlipAlpha), 0.0, fFadeTime)))
		IF GET_BLIP_ALPHA(biZoneBlip) = 0
			CLEAR_BIT(iZoneBlipFadingBS, iZone)
			SET_BIT(iZoneBlipHiddenBS, iZone)
			fCurrentFadeTime[iZone] = 0
			SET_NEW_ZONE_OFFSET_REQUIRED(iZone)
		ENDIF
	ENDIF

ENDPROC

PROC CREATE_ZONE_BLIP(BLIP_INDEX &biZoneBlip, INT iZone)
	
	IF DOES_BLIP_EXIST(biZoneBlip)
		EXIT
	ENDIF
	
	IF GET_FMMC_ZONE_TYPE(iZone) = ciFMMC_ZONE_TYPE__RACE_PIT_ZONE
		biZoneBlip = ADD_BLIP_FOR_COORD(GET_ZONE_RUNTIME_CENTRE(iZone))
		SET_BLIP_SPRITE(biZoneBlip, RADAR_TRACE_PICKUP_REPAIR)
		SET_BLIP_SCALE(biZoneBlip, 1.6)
		SET_BLIP_NAME_FROM_TEXT_FILE(biZoneBlip, "VEH_REP_BLIP")
		PRINTLN("[Blips][Zones][Zone ", iZone,"] CREATE_ZONE_BLIP - Creating Race Pit Blip")
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__SPHERE 
	OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__CYLINDER
		
		biZoneBlip = ADD_BLIP_FOR_RADIUS(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], GET_PLACED_ZONE_RADIUS(iZone))
		PRINTLN("[Blips][Zones][Zone ", iZone,"] CREATE_ZONE_BLIP - Creating sphere/cylinder radius blip")
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__ANGLED_AREA
		
		biZoneBlip = ADD_BLIP_FOR_AREA_FROM_EDGES(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1], GET_PLACED_ZONE_RADIUS(iZone))
		PRINTLN("[Blips][Zones][Zone ", iZone,"] CREATE_ZONE_BLIP - Creating angled area blip")
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
		
		FLOAT fScaleX, fScaleY
		
		SET_ZONE_BLIP_SCALE_FLOATS(fScaleX, fScaleY, iZone)
		
		biZoneBlip = ADD_BLIP_FOR_AREA(GET_ZONE_RUNTIME_CENTRE(iZone), ABSF(fScaleX), ABSF(fScaleY))
		SET_BLIP_ROTATION(biZoneBlip, 0)
		PRINTLN("[Blips][Zones][Zone ", iZone,"] CREATE_ZONE_BLIP - Creating axis aligned area blip")
		
	ENDIF
	
	SET_UP_ZONE_BLIP(biZoneBlip, iZone)
	
	
ENDPROC

PROC PROCESS_ZONE_BLIP(BLIP_INDEX &biZoneBlip, INT iZone)
	
	IF NOT SHOULD_DRAW_ZONE_BLIP(iZone)
		IF DOES_BLIP_EXIST(biZoneBlip)					
			REMOVE_BLIP(biZoneBlip)
		ENDIF
		
		EXIT
	ENDIF
	
	IF NOT DOES_BLIP_EXIST(biZoneBlip)
		CREATE_ZONE_BLIP(biZoneBlip, iZone)
		EXIT
	ENDIF
	
	PROCESS_CUSTOM_ZONE_BLIP_FUNCTIONALITY(biZoneBlip, iZone)
	
	IF GET_FMMC_ZONE_TYPE(iZone) = ciFMMC_ZONE_TYPE__RACE_PIT_ZONE
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__SPHERE 
	OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__CYLINDER
		
		SET_BLIP_COORDS(biZoneBlip, GET_ZONE_RUNTIME_CENTRE(iZone))
		SET_BLIP_SCALE(biZoneBlip, GET_PLACED_ZONE_RADIUS(iZone))
	
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__ANGLED_AREA
		VECTOR vPos1 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0]
		VECTOR vPos2 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1]
		
		SET_BLIP_COORDS(biZoneBlip, GET_ZONE_RUNTIME_CENTRE(iZone))
		SET_BLIP_SCALE_2D(biZoneBlip, GET_PLACED_ZONE_RADIUS(iZone), GET_DISTANCE_BETWEEN_COORDS(vPos2, vPos1, FALSE))
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
		
		FLOAT fScaleX, fScaleY
		
		SET_ZONE_BLIP_SCALE_FLOATS(fScaleX, fScaleY, iZone)
		
		SET_BLIP_COORDS(biZoneBlip, GET_ZONE_RUNTIME_CENTRE(iZone))
		SET_BLIP_SCALE_2D(biZoneBlip, ABSF(fScaleX), ABSF(fScaleY))
		PRINTLN("JT DEBUG - sX ",ABSF(fScaleX), " sY ", ABSF(fScaleY)," c ", VECTOR_TO_STRING( GET_ZONE_RUNTIME_CENTRE(iZone)))
		
	ENDIF
	
ENDPROC

PROC PROCESS_ZONE_MARKER(INT iZone)
	
	IF NOT SHOULD_DRAW_ZONE_MARKER(iZone)
		EXIT
	ENDIF
	
	HUD_COLOURS eHudColour = MC_GET_HUD_COLOUR_FROM_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iBlipColor)	
	FLOAT fAlpha = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iBlipAlpha * 0.35
	
	#IF IS_DEBUG_BUILD
		IF FMMC_IS_LONG_BIT_SET(sContentOverviewDebug.iEntityStateDrawZoneDebugBS, iZone)
			eHudColour = HUD_COLOUR_WHITE
			fAlpha = 150
		ENDIF
	#ENDIF
	
	DRAW_FMMC_ZONE(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone], GET_ZONE_RUNTIME_POSITION(iZone, 0), GET_ZONE_RUNTIME_POSITION(iZone, 1), GET_ZONE_RUNTIME_RADIUS(iZone), fAlpha, eHudColour #IF IS_DEBUG_BUILD, FMMC_IS_LONG_BIT_SET(sContentOverviewDebug.iEntityStateDrawZoneDebugBS, iZone) #ENDIF )
	
	IF GET_FMMC_ZONE_TYPE(iZone) = ciFMMC_ZONE_TYPE__RACE_PIT_ZONE
		INT iR, iG, iB, iA
		// Marker
		IF iCurrentRacePitZone = -1
			GET_HUD_COLOUR(HUD_COLOUR_BLUELIGHT, iR, iG, iB, iA)
			DRAW_MARKER(MARKER_PIT_LANE, GET_ZONE_RUNTIME_CENTRE(iZone) + <<0,0,cfGroundedZoneUndergroundOffset>>, <<0,0,0>>, <<0,0,0>>, <<2.0, 2.0, 2.0>>, iR, iG, iB, 225, DEFAULT, TRUE)
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_ZONE_SPAWN_AREA_COORDS_TO_FACE(INT iZone)
	
	VECTOR vCoordsToFace = <<0,0,0>>
	ENTITY_INDEX eiEntityToFace = NULL
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneSpawnFacingType
		CASE ciFMMC_ZONE_SPAWN_FACING__DEFAULT
			RETURN <<0,0,0>>
		BREAK
		
		CASE ciFMMC_ZONE_SPAWN_FACING__RULE_ENTITY
			vCoordsToFace = GET_COORDS_OF_CLOSEST_OBJECTIVE()
		BREAK
		
		CASE ciFMMC_ZONE_SPAWN_FACING__CHOSEN_ENTITY
			eiEntityToFace = GET_FMMC_ENTITY(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iSpawnFacingEntityType, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iSpawnFacingEntityIndex)
		BREAK
		
		CASE ciFMMC_ZONE_SPAWN_FACING__CHOSEN_POSITION
			vCoordsToFace = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vZoneSpawnFacingPosition
		BREAK
	ENDSWITCH
	
	IF DOES_ENTITY_EXIST(eiEntityToFace)
		vCoordsToFace = GET_ENTITY_COORDS(eiEntityToFace)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[Zones_SPAM][ZoneSpawnAreas_SPAM] GET_ZONE_SPAWN_AREA_COORDS_TO_FACE - Returning ", vCoordsToFace)
		
		IF bZoneDebug
			DRAW_DEBUG_SPHERE(vCoordsToFace, 1.0, 255, 255, 255, ROUND(GET_SINE_ALPHA(0.0, 5.0) * 255))
		ENDIF
	#ENDIF
	
	RETURN vCoordsToFace
ENDFUNC

FUNC BOOL SHOULD_REFRESH_ZONE_SPAWN_AREA_COORDS()
	
	IF iCurrentSpawnAreaZone = -1
		RETURN FALSE
	ENDIF
	
	IF VDIST2(GET_ZONE_RUNTIME_POSITION(iCurrentSpawnAreaZone, 0), g_SpawnData.MissionSpawnDetails.SpawnArea[0].vCoords1) > POW(cfZoneSpawnArea_RefreshSpawnAreaDistance, 2.0)
		PRINTLN("[MC_CustomSpawning_SPAM][ZoneSpawnAreas_SPAM][Zones_SPAM][Zone_SPAM ", iCurrentSpawnAreaZone, "] SHOULD_REFRESH_ZONE_SPAWN_AREA_COORDS | Refreshing due to distance from last Spawn Area!")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CACHE_ZONE_SPAWN_AREA_COORDS_TO_FACE()

	IF NOT bLocalPlayerPedOk
		RETURN TRUE
	ENDIF
	IF DOES_LOCAL_PLAYER_HAVE_NEW_PRIORITY_THIS_FRAME()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CACHE_ZONE_SPAWN_AREA_COORDS_TO_FACE(INT iZone)
	
	VECTOR vCoordsToFace = GET_ZONE_SPAWN_AREA_COORDS_TO_FACE(iZone)
	
	IF NOT IS_VECTOR_ZERO(vCoordsToFace)
		IF NOT ARE_VECTORS_EQUAL(g_SpawnData.MissionSpawnDetails.vFacing, vCoordsToFace)
			PRINTLN("[MC_CustomSpawning][ZoneSpawnAreas_SPAM][Zones_SPAM][Zone_SPAM ", iZone, "] CACHE_ZONE_SPAWN_AREA_COORDS_TO_FACE | vZoneSpawnArea_FaceCoordsOnRespawn to ", vCoordsToFace)
			vZoneSpawnArea_FaceCoordsOnRespawn = vCoordsToFace
		ENDIF
		
		SET_BIT(iLocalBoolCheck31, LBOOL31_USING_SPAWN_AREA_ZONE_FOR_RESPAWN_HEADING)
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_USING_SPAWN_AREA_ZONE_FOR_RESPAWN_HEADING)
			PRINTLN("[MC_CustomSpawning][ZoneSpawnAreas][Zones][Zone ", iZone, "] CACHE_ZONE_SPAWN_AREA_COORDS_TO_FACE | Clearing g_SpawnData.MissionSpawnDetails.bFacePoint and vZoneSpawnArea_FaceCoordsOnRespawn")
			g_SpawnData.MissionSpawnDetails.bFacePoint = FALSE
			vZoneSpawnArea_FaceCoordsOnRespawn = <<0,0,0>>
			CLEAR_BIT(iLocalBoolCheck31, LBOOL31_USING_SPAWN_AREA_ZONE_FOR_RESPAWN_HEADING)
		ENDIF
	ENDIF
ENDPROC

PROC SET_ZONE_AS_SPAWN_AREA(INT iZone)
	
	IF iCurrentSpawnAreaZone != iZone
		PRINTLN("[MC_CustomSpawning][ZoneSpawnAreas][Zones][Zone ", iZone, "] SET_ZONE_AS_SPAWN_AREA | Setting Zone ", iZone, " as the new spawn area!")
		iCurrentSpawnAreaZone = iZone
		REINIT_NET_TIMER(stZoneSpawnArea_CooldownTimer)
	ELSE
		PRINTLN("[MC_CustomSpawning_SPAM][ZoneSpawnAreas_SPAM][Zones_SPAM][Zone_SPAM ", iZone, "] SET_ZONE_AS_SPAWN_AREA | Refreshing Spawn Area coords for the current Spawn Area Zone (Zone ", iZone, ")!")
	ENDIF
	
	SWITCH GET_ZONE_SHAPE(iZone)
		CASE ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
			SET_MISSION_SPAWN_BOX(GET_ZONE_RUNTIME_POSITION(iZone, 0), GET_ZONE_RUNTIME_POSITION(iZone, 1))
		BREAK
		
		CASE ciFMMC_ZONE_SHAPE__ANGLED_AREA
			SET_MISSION_SPAWN_ANGLED_AREA(GET_ZONE_RUNTIME_POSITION(iZone, 0), GET_ZONE_RUNTIME_POSITION(iZone, 1), GET_ZONE_RUNTIME_WIDTH(iZone), DEFAULT, TRUE)
		BREAK
		
		CASE ciFMMC_ZONE_SHAPE__SPHERE
		CASE ciFMMC_ZONE_SHAPE__CYLINDER
			SET_MISSION_SPAWN_SPHERE(GET_ZONE_RUNTIME_POSITION(iZone, 0), GET_ZONE_RUNTIME_RADIUS(iZone), DEFAULT, TRUE, TRUE)
		BREAK
	ENDSWITCH
	
	CACHE_ZONE_SPAWN_AREA_COORDS_TO_FACE(iZone)
	
ENDPROC

PROC CLEAR_CURRENT_ZONE_SPAWN_AREA()

	IF iCurrentSpawnAreaZone = -1
		EXIT
	ENDIF
	
	INT iZone = iCurrentSpawnAreaZone
	PRINTLN("[MC_CustomSpawning][ZoneSpawnAreas][Zones][Zone ", iZone, "] CLEAR_ZONE_AS_SPAWN_AREA | Clearing spawn area from Zone ", iZone)
	
	CLEAR_SPAWN_AREA(TRUE, FALSE, SHOULD_USE_CUSTOM_SPAWN_POINTS())
	
	iCurrentSpawnAreaZone = -1
	REINIT_NET_TIMER(stZoneSpawnArea_CooldownTimer)
	
	CLEAR_BIT(iLocalBoolCheck31, LBOOL31_USING_SPAWN_AREA_ZONE_FOR_RESPAWN_HEADING)
	vZoneSpawnArea_FaceCoordsOnRespawn = <<0,0,0>>
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Zone Every Frame Processing
// ##### Description: Client and Server Zone Every Frame Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_REFRESH_ZONES_FOR_TEAM(INT iTeam)
	
	IF DOES_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(iTeam)
		PRINTLN("[Zones] SHOULD_REFRESH_ZONES_FOR_TEAM | Refreshing because Team ", iTeam, " has reached a new rule")
		RETURN TRUE
	ENDIF
	
	IF iZonesWaitingForCreationBS != 0
		PRINTLN("[Zones] SHOULD_REFRESH_ZONES_FOR_TEAM | Refreshing because iZonesWaitingForCreationBS is ", iZonesWaitingForCreationBS)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck34, LBOOL34_SPAWN_GROUPS_CHANGED_REVALIDATE_ZONES)
		PRINTLN("[Zones] SHOULD_REFRESH_ZONES_FOR_TEAM | Refreshing because Spawn Groups have changed")
		RETURN TRUE
	ENDIF
	
	IF iZoneNeedsResizingBS != 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_ZONE_MID_MISSION_CREATION()
	
	IF SHOULD_REFRESH_ZONES_FOR_TEAM(MC_playerBD[iPartToUse].iTeam)
		CREATE_ZONES(MC_playerBD[iPartToUse].iTeam)
	ENDIF
	
	CLEAR_BIT(iLocalBoolCheck34, LBOOL34_SPAWN_GROUPS_CHANGED_REVALIDATE_ZONES)
	
	IF iRefreshZonesForThisTeam > -1
		PRINTLN("[RCC MISSION] PROCESS_ZONES - Calling CREATE_ZONES for team ", iRefreshZonesForThisTeam, " due to iRefreshZonesForThisTeam")
		CREATE_ZONES(iRefreshZonesForThisTeam)
		iRefreshZonesForThisTeam = -1
	ENDIF
ENDPROC

PROC PROCESS_ZONE_PRE_EVERY_FRAME()
	PROCESS_ZONE_MID_MISSION_CREATION()
	
	// Zone Spawn Areas
	iSpawnAreaZoneThisFrame = -1
	fCurrentClosestSpawnAreaZoneDistance = -1.0
	INT iZone
	FOR iZone = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfZones-1
		INT iPositionIndex = 0
		FOR iPositionIndex = 0 TO ciFMMC_ZONE_MAX_POSITIONS-1
			vCachedZonePosition[iZone][iPositionIndex] = EMPTY_VEC()
		ENDFOR
	ENDFOR
ENDPROC

FUNC BOOL CAN_SET_CURRENT_ZONE_SPAWN_AREA()
	IF NOT bLocalPlayerPedOk
		PRINTLN("[Zones_SPAM][ZoneSpawnAreas_SPAM] CAN_SET_CURRENT_ZONE_SPAWN_AREA | Returning FALSE due to bLocalPlayerPedOk")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
		IF GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iRespawnState != RESPAWN_STATE_PLAYING
			PRINTLN("[Zones_SPAM][ZoneSpawnAreas_SPAM] CAN_SET_CURRENT_ZONE_SPAWN_AREA | Returning FALSE due to iRespawnState being ", GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iRespawnState)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(stZoneSpawnArea_CooldownTimer)
		IF NOT HAS_NET_TIMER_EXPIRED(stZoneSpawnArea_CooldownTimer, ciZoneSpawnArea_CooldownLength)
			PRINTLN("[Zones_SPAM][ZoneSpawnAreas_SPAM] CAN_SET_CURRENT_ZONE_SPAWN_AREA | Returning FALSE due to stZoneSpawnArea_CooldownTimer")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_ZONE_SPAWN_AREAS_POST_EVERY_FRAME()
	IF CAN_SET_CURRENT_ZONE_SPAWN_AREA()
		IF iSpawnAreaZoneThisFrame = -1
			IF iCurrentSpawnAreaZone != -1
				// We've currently got a Zone acting as the Spawn Area, but we're not in any Zones that act as spawn areas right now. Clear the spawn area
				CLEAR_CURRENT_ZONE_SPAWN_AREA()
			ENDIF
		ELSE
			IF iSpawnAreaZoneThisFrame != iCurrentSpawnAreaZone
				// We've entered this Zone but haven't moved the Spawn Area here yet - do that now!
				SET_ZONE_AS_SPAWN_AREA(iSpawnAreaZoneThisFrame)
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF bZoneDebug
			DRAW_DEBUG_TEXT_2D("CAN_SET_CURRENT_ZONE_SPAWN_AREA is FALSE", (<<0.15, 0.15, 0.5>>), 255, 255, 255, 255)
		ENDIF
		#ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bZoneDebug
			TEXT_LABEL_63 tlVisualDebug = "iSpawnAreaZoneThisFrame: "
			tlVisualDebug += iSpawnAreaZoneThisFrame
			tlVisualDebug += " | iCurrentSpawnAreaZone: "
			tlVisualDebug += iCurrentSpawnAreaZone
			DRAW_DEBUG_TEXT_2D(tlVisualDebug, (<<0.35, 0.885, 0.5>>), 255, 255, 255, 255)
		ENDIF
	#ENDIF
	
	IF iCurrentSpawnAreaZone > -1
	
		IF SHOULD_REFRESH_ZONE_SPAWN_AREA_COORDS()
			SET_ZONE_AS_SPAWN_AREA(iCurrentSpawnAreaZone)
		ELIF SHOULD_CACHE_ZONE_SPAWN_AREA_COORDS_TO_FACE()
			CACHE_ZONE_SPAWN_AREA_COORDS_TO_FACE(iCurrentSpawnAreaZone)
		ENDIF
		
		IF USING_SPAWN_AREA_ZONE_RESPAWN_HEADING()
			PRINTLN("[RCC MISSION][Zones] PROCESS_ZONE_POST_EVERY_FRAME - Calling SET_PLAYER_WILL_SPAWN_FACING_COORDS with ", vZoneSpawnArea_FaceCoordsOnRespawn)
			SET_PLAYER_WILL_SPAWN_FACING_COORDS(vZoneSpawnArea_FaceCoordsOnRespawn, TRUE, FALSE)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bZoneDebug
			DRAW_DEBUG_LINE(GET_ENTITY_COORDS(LocalPlayerPed), g_SpawnData.MissionSpawnDetails.vFacing, 0, 255, 0, 255)
			DRAW_DEBUG_LINE(GET_ZONE_RUNTIME_CENTRE(iCurrentSpawnAreaZone), g_SpawnData.MissionSpawnDetails.vFacing, 0, 255, 0, 255)
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC PROCESS_ZONE_POST_EVERY_FRAME()
	
	iZoneStaggeredTeamToCheck++
	IF iZoneStaggeredTeamToCheck >= g_FMMC_STRUCT.iNumberOfTeams - 1
		iZoneStaggeredTeamToCheck = 0
	ENDIF
	
	IF IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__BLOCK_JUMPING_AND_CLIMBING)
		PRINTLN("[RCC MISSION] PROCESS_ZONE_POST_EVERY_FRAME - DISABLING JUMP")
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	ENDIF
	
	IF NOT IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__HEALTH_DRAIN)
	AND NOT IS_BIT_SET(iPoisonGasBS, ciPoisonGasActivatedByScript)
		APPLY_OR_REMOVE_POISON_GAS_SCREEN_EFFECT(FALSE)
	ENDIF
	
	IF IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__BLOCK_CLIMBING)
		PRINTLN("[RCC MISSION] PROCESS_ZONE_POST_EVERY_FRAME - DISABLING CLIMBING")
		SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisablePlayerVaulting, TRUE)
		SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisablePlayerAutoVaulting, TRUE)
	ENDIF
	
	IF IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__BLOCK_RUNNING)
		IF fMaxRunningZoneSpeed < 0.1
			//Block Running
			PRINTLN("[RCC MISSION] PROCESS_ZONE_POST_EVERY_FRAME - DISABLING RUN fBlockedRunningZoneSpeed: ", fBlockedRunningZoneSpeed)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
			IF fBlockedRunningZoneSpeed < 0.1
				SET_PED_MAX_MOVE_BLEND_RATIO(LocalPlayerPed, PEDMOVEBLENDRATIO_WALK)
			ELSE
				SET_PED_MAX_MOVE_BLEND_RATIO(LocalPlayerPed, fBlockedRunningZoneSpeed)			
				SET_PED_MOVE_RATE_OVERRIDE(LocalPlayerPed, fBlockedRunningZoneSpeed)
			ENDIF
		ELSE
			//Modify Player's Speed
			PRINTLN("[RCC MISSION] PROCESS_ZONE_POST_EVERY_FRAME - MOVE SPEED OVERRIDE - Overriding max player speed to ", fMaxRunningZoneSpeed)
			SET_PED_MAX_MOVE_BLEND_RATIO(LocalPlayerPed, fMaxRunningZoneSpeed)
		ENDIF
	ENDIF
	
	IF IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__BLOCK_WEAPONS)
		IF wtWeaponBlockZonePreviousWeapon = WEAPONTYPE_INVALID
			GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtWeaponBlockZonePreviousWeapon)
		ELSE
			PRINTLN("[RCC MISSION] PROCESS_ZONE_POST_EVERY_FRAME - DISABLING WEAPON WHEEL AND EQUIPPING UNARMED")
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
			SET_CURRENT_PED_WEAPON(LocalPlayerPed,WEAPONTYPE_UNARMED,TRUE)
		ENDIF
	ENDIF
	
	IF IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__PLAYER_CANT_EXIT_VEHICLE)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			DISABLE_VEHICLE_EXIT_THIS_FRAME()
		ENDIF
	ENDIF
	
	IF IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__PLAYER_CANT_DRIVE_VEHICLE)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			DISABLE_VEHICLE_MOVEMENT_CONTROLS_THIS_FRAME()
		ENDIF
	ENDIF
	
	IF IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__BLOCK_VTOL_TOGGLE)
		PRINTLN("[RCC MISSION] PROCESS_ZONE_POST_EVERY_FRAME - BLOCK VTOL TOGGLE")
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			VEHICLE_INDEX viMyVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			IF DOES_THIS_MODEL_HAVE_VERTICAL_FLIGHT_MODE(GET_ENTITY_MODEL(viMyVeh))
				IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
					SET_DISABLE_VERTICAL_FLIGHT_MODE_TRANSITION(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__FIRE_ROCKETS_AT_PLAYER)
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_ZONE_ROCKETS_REQUIRED)
			PRINTLN("[RCC MISSION] PROCESS_ZONE_POST_EVERY_FRAME - No longer in any Fire Rocket Zone, clearing PBBOOL_ZONE_ROCKETS_REQUIRED & resetting rocket timer")
			RESET_NET_TIMER(tdRocketTimer)
			CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_ZONE_ROCKETS_REQUIRED)
		ENDIF
	ENDIF
	
	IF IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__PLAYER_VEHICLE_MAX_SPEED)
	AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
		INT iMaxSpeedZone = GET_TRIGGERING_ZONE_INDEX(ciFMMC_ZONE_TYPE__PLAYER_VEHICLE_MAX_SPEED)
		
		IF iMaxSpeedZone > -1
		AND NOT DOES_ENTITY_EXIST(viCachedMaxSpeedZoneVehicle)
			viCachedMaxSpeedZoneVehicle = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)
			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(viCachedMaxSpeedZoneVehicle)
				IF IS_ENTITY_ALIVE(viCachedMaxSpeedZoneVehicle)
					PRINTLN("[RCC MISSION][Zones] PROCESS_ZONE_POST_EVERY_FRAME | Setting player vehicle max speed to ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iMaxSpeedZone].fZoneValue)
					SET_VEHICLE_MAX_SPEED(viCachedMaxSpeedZoneVehicle, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iMaxSpeedZone].fZoneValue)
				ENDIF
			ENDIF
		ENDIF
	ELSE	
		IF DOES_ENTITY_EXIST(viCachedMaxSpeedZoneVehicle)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(viCachedMaxSpeedZoneVehicle)
				IF IS_ENTITY_ALIVE(viCachedMaxSpeedZoneVehicle)
					PRINTLN("[RCC MISSION][Zones] PROCESS_ZONE_POST_EVERY_FRAME | Clearing player vehicle max speed override")
					SET_VEHICLE_MAX_SPEED(viCachedMaxSpeedZoneVehicle, -1.0)
				ENDIF
				
				viCachedMaxSpeedZoneVehicle = NULL
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__AIR_DEFENCE)
		IF HAS_NET_TIMER_STARTED(stAirDefenceZone_TimeSpentInside)
			RESET_NET_TIMER(stAirDefenceZone_TimeSpentInside)
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(stAirDefenceZone_WarningShotTimer)
			RESET_NET_TIMER(stAirDefenceZone_WarningShotTimer)
		ENDIF
	ENDIF
	
	IF DOES_A_ZONE_OF_THIS_TYPE_EXIST(ciFMMC_ZONE_TYPE__EXCLUSIVE_CUSTOM_SPAWN_POINTS)
		IF IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__EXCLUSIVE_CUSTOM_SPAWN_POINTS)
			IF IS_BIT_SET(iLocalBoolCheck34, LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES)
				PRINTLN("[RCC MISSION][Zones] PROCESS_ZONE_POST_EVERY_FRAME - Inside a ciFMMC_ZONE_TYPE__EXCLUSIVE_CUSTOM_SPAWN_POINTS type zone - Clearing LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES")
				CLEAR_BIT(iLocalBoolCheck34, LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES)
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES)
				PRINTLN("[RCC MISSION][Zones] PROCESS_ZONE_POST_EVERY_FRAME - Not inside ciFMMC_ZONE_TYPE__EXCLUSIVE_CUSTOM_SPAWN_POINTS type zone - Setting LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES")
				SET_BIT(iLocalBoolCheck34, LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck34, LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES)
			PRINTLN("[RCC MISSION][Zones] PROCESS_ZONE_POST_EVERY_FRAME - There are no ciFMMC_ZONE_TYPE__EXCLUSIVE_CUSTOM_SPAWN_POINTS Zones - Clearing LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES")
			CLEAR_BIT(iLocalBoolCheck34, LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES)
		ENDIF
	ENDIF
	
	IF IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__CHANGE_WALK_STYLE)
		IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_APPLIED_CUSTOM_ZONE_WALK_STYLE)
			INT iZoneToUse = GET_TRIGGERING_ZONE_INDEX(ciFMMC_ZONE_TYPE__CHANGE_WALK_STYLE)
			IF APPLY_WALK_STYLE_CLIPSET(LocalPlayerPed, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneToUse].iZoneValue3)
				PRINTLN("[RCC MISSION][Zones][Zone ", iZoneToUse, "][WalkStyles] PROCESS_ZONE_POST_EVERY_FRAME - Applied custom walk style ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneToUse].iZoneValue3)
				SET_BIT(iLocalBoolCheck31, LBOOL31_APPLIED_CUSTOM_ZONE_WALK_STYLE)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_APPLIED_CUSTOM_ZONE_WALK_STYLE)
			PRINTLN("[RCC MISSION][Zones][WalkStyles] PROCESS_ZONE_POST_EVERY_FRAME - Clearing custom walk style")
			APPLY_WALK_STYLE_CLIPSET(LocalPlayerPed, ciFMMC_WALK_STYLE__DEFAULT)
			UNLOAD_ALL_WALK_STYLE_CLIPSETS()
			
			CLEAR_BIT(iLocalBoolCheck31, LBOOL31_APPLIED_CUSTOM_ZONE_WALK_STYLE)
		ENDIF
	ENDIF
	
	// Zone Spawn Areas
	PROCESS_ZONE_SPAWN_AREAS_POST_EVERY_FRAME()
	
ENDPROC

PROC PROCESS_SPAWN_PROTECTION_ZONE_EVERY_FRAME_CLIENT(INT iZone)

	IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
	AND NOT g_bCelebrationScreenIsActive
		IF IS_ZONE_TRIGGERING(iZone)
			PRINTLN("[LM][Zones][Zone ", iZone, "] PROCESS_ZONES | Inside of an Air Defense Zone. Blocking Inputs (1)")
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)	
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK, TRUE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM, TRUE)
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MELEE_LEFT)
			
			IF bLocalPlayerPedOK
				SET_PED_RESET_FLAG(LocalPlayerPed, PRF_BlockWeaponFire, TRUE)
			ENDIF
			
			SET_PED_CONFIG_FLAG(localPlayerPed, PCF_DontActivateRagdollFromExplosions, TRUE)
			SET_PED_CONFIG_FLAG(localPlayerPed, PCF_DontActivateRagdollFromFire, TRUE)
			SET_PED_CONFIG_FLAG(localPlayerPed, PCF_DontActivateRagdollFromElectrocution, TRUE)
			
			WEAPON_TYPE wt = WEAPONTYPE_INVALID
			GET_CURRENT_PED_WEAPON(LocalPlayerPed, wt)
		
			IF wt != WEAPONTYPE_UNARMED
				DISABLE_PLAYER_THROW_GRENADE_WHILE_USING_GUN()
				DISABLE_PLAYER_FIRING(localPlayer, TRUE)										
			ENDIF
			
			IF wt = WEAPONTYPE_GRENADE
				SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
			ENDIF
			
			DISABLE_VEHICLE_MINES(TRUE)
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_AD_INVINCIBILITY)
			AND (ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue) = MC_PlayerBD[iLocalPart].iTeam OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_APPLY_TO_ALL_TEAMS))
				IF bLocalPlayerPedOK
					IF NOT IS_ENTITY_A_GHOST(localPlayerPed)
						MC_SET_LOCAL_PLAYER_AS_GHOST(TRUE)
					ENDIF
					
					IF NOT HAS_NET_TIMER_STARTED(tdDefenseSphereAlphaTimer)
					OR (HAS_NET_TIMER_STARTED(tdDefenseSphereAlphaTimer) AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdDefenseSphereAlphaTimer, (g_FMMC_STRUCT.iSpawnProtectionDuration*100)))
						RESET_NET_TIMER(tdDefenseSphereAlphaTimer)
						START_NET_TIMER(tdDefenseSphereAlphaTimer)
						IF NOT IS_PED_FALLING(localPlayerPed)
							BROADCAST_FMMC_SPAWN_PROTECTION_ALPHA(TRUE, iLocalPart, GET_FRAME_COUNT())
						ENDIF
					ENDIF
					
					IF GET_ENTITY_HEALTH(localPlayerPed) > 0
						SET_ENTITY_INVINCIBLE(localPlayerPed, TRUE)
					ENDIF
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						IF IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
							SET_ENTITY_INVINCIBLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				RESET_NET_TIMER(tdDefenseSphereInvulnerabilityTimer)
				START_NET_TIMER(tdDefenseSphereInvulnerabilityTimer)
			ENDIF
			
			IF NOT IS_BIT_SET(iSpawnProtectionZoneDefenceBS, LBOOLZONE_PLAYER_JUST_LEFT_DEFENSE_SPHERE + iZone)
				PRINTLN("[LM][Zones][Zone ", iZone, "] PROCESS_ZONES | Inside of an Air Defense Zone. Setting Bit")
				// So we can process a period of temporary invincibility when we leave.
				SET_BIT(iSpawnProtectionZoneDefenceBS, LBOOLZONE_PLAYER_JUST_LEFT_DEFENSE_SPHERE + iZone)
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_LAND_GRAB_SOUNDS)
					IF bLocalPlayerPedOK
						PLAY_SOUND_FRONTEND(-1, "Weapon_Disabled", "DLC_SR_LG_Player_Sounds")
					ENDIF
				ENDIF
			ENDIF								
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_AD_INVINCIBILITY)
			AND(ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue) = MC_PlayerBD[iLocalPart].iTeam OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_APPLY_TO_ALL_TEAMS))
				IF IS_BIT_SET(iSpawnProtectionZoneDefenceBS, LBOOLZONE_PLAYER_JUST_LEFT_DEFENSE_SPHERE + iZone)

					PRINTLN("[LM][Zones][Zone ", iZone, "] PROCESS_ZONES | Starting Ghost and Invincibility.")
					
					RESET_NET_TIMER(tdDefenseSphereInvulnerabilityTimer)
					START_NET_TIMER(tdDefenseSphereInvulnerabilityTimer)
					
					IF bLocalPlayerPedOK
						IF NOT IS_ENTITY_A_GHOST(localPlayerPed)
							IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
							AND NOT IS_PED_FALLING(localPlayerPed)
								MC_SET_LOCAL_PLAYER_AS_GHOST(TRUE)
								BROADCAST_FMMC_SPAWN_PROTECTION_ALPHA(TRUE, iLocalPart, GET_FRAME_COUNT())
								DISABLE_VEHICLE_MINES(FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						IF IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
							SET_ENTITY_INVINCIBLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iSpawnProtectionZoneDefenceBS, LBOOLZONE_PLAYER_JUST_LEFT_DEFENSE_SPHERE + iZone)
				CLEAR_BIT(iSpawnProtectionZoneDefenceBS, LBOOLZONE_PLAYER_JUST_LEFT_DEFENSE_SPHERE + iZone)
				PRINTLN("[LM][Zones][Zone ", iZone, "] PROCESS_ZONES | Now Outside of an Air Defense Zone. Clearing Bit")		
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_LAND_GRAB_SOUNDS)
					IF bLocalPlayerPedOK
						PLAY_SOUND_FRONTEND(-1, "Weapon_Enabled", "DLC_SR_LG_Player_Sounds")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT g_bCelebrationScreenIsActive
			IF bLocalPlayerPedOk
			AND NOT bIsAnySpectator
				SET_ENTITY_INVINCIBLE(localPlayerPed, FALSE)
				MC_SET_LOCAL_PLAYER_AS_GHOST(FALSE)
				
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					IF IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
						SET_ENTITY_INVINCIBLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), FALSE)
					ENDIF
				ENDIF
			ENDIF
			IF iFMMCAirDefenceZoneArea[iZone] != -1
				IF DOES_AIR_DEFENCE_SPHERE_EXIST(iFMMCAirDefenceZoneArea[iZone])
					PRINTLN("[LM][Zones][Zone ", iZone, "] PROCESS_ZONES | Removing air defence sphere.")
					REMOVE_AIR_DEFENCE_SPHERE(iFMMCAirDefenceZoneArea[iZone])
				ENDIF
			ENDIF
			
			// We only need to call this once.
			IF NOT IS_BIT_SET(iLocalboolCheck25, LBOOL25_SUDDEN_DEATH_SPAWN_PROTECTION_DISABLED)
			AND NOT IS_PED_FALLING(localPlayerPed)
				BROADCAST_FMMC_SPAWN_PROTECTION_ALPHA(FALSE, iLocalPart, GET_FRAME_COUNT())
				SET_BIT(iLocalboolCheck25, LBOOL25_SUDDEN_DEATH_SPAWN_PROTECTION_DISABLED)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FIRE_ROCKETS_ZONE_EVERY_FRAME_CLIENT(INT iZone)
	
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_SOMEONE_NEEDS_ZONE_ROCKETS)
		IF NOT HAS_WEAPON_ASSET_LOADED(GET_FIRE_ROCKETS_ZONE_WEAPON_TYPE(iZone))
			REQUEST_WEAPON_ASSET(GET_FIRE_ROCKETS_ZONE_WEAPON_TYPE(iZone))
			PRINTLN("[ZoneRockets] PROCESS_FIRE_ROCKETS_ZONE_EVERY_FRAME_CLIENT | Requesting RPG asset!")
			EXIT
		ENDIF
	ELSE
		IF HAS_WEAPON_ASSET_LOADED(GET_FIRE_ROCKETS_ZONE_WEAPON_TYPE(iZone))
			REMOVE_WEAPON_ASSET(GET_FIRE_ROCKETS_ZONE_WEAPON_TYPE(iZone))
			PRINTLN("[ZoneRockets] PROCESS_FIRE_ROCKETS_ZONE_EVERY_FRAME_CLIENT | Releasing RPG asset")
		ENDIF
	ENDIF
				
	IF NOT IS_ZONE_TRIGGERING(iZone)
	OR NOT IS_PLAYER_IN_CORRECT_VEHICLE_TYPE_FOR_AIR_ZONE(iZone)
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(tdRocketTimer)
		PRINTLN("[RCC MISSION][ZoneRockets] PROCESS_ZONE_STAGGERED_FIRE_ROCKETS_CLIENT | Zone ", iZone, " Starting timer")
		REINIT_NET_TIMER(tdRocketTimer)
	ENDIF
	
	IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_ZONE_ROCKETS_REQUIRED)
		PRINTLN("[RCC MISSION][ZoneRockets] PROCESS_ZONE_STAGGERED_FIRE_ROCKETS_CLIENT | Zone ", iZone, ", I need restriction rockets")
		SET_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_ZONE_ROCKETS_REQUIRED)
		BROADCAST_FMMC_REQUEST_RESTRICTION_ROCKETS(TRUE)
		EXIT
	ENDIF
	
	IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdRocketTimer) < ciRocketTimer_Cooldown
		PRINTLN("[RCC MISSION][ZoneRockets] PROCESS_ZONE_STAGGERED_FIRE_ROCKETS_CLIENT | Zone ", iZone, " Waiting for tdRocketTimer")
		EXIT
	ENDIF
	
	INT iRocketPos
	VECTOR vRocketFireFromThisLocation = GET_CLOSEST_ROCKET_POSITION_FOR_ZONE(iZone, iRocketPos)
	
	IF IS_VECTOR_ZERO(vRocketFireFromThisLocation)
		PRINTLN("[RCC MISSION][ZoneRockets] PROCESS_ZONE_STAGGERED_FIRE_ROCKETS_CLIENT | Zone ", iZone, " Unable to find a Rocket location!")
		EXIT
	ENDIF
	
	ENTITY_INDEX TargetEntity = LocalPlayerPed
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		TargetEntity = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
	ENDIF
	
	FLOAT fAccuracy
	BOOL bHoming
	INT iCustomAccuracyVeh = g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketCustomAccuracyOnlyInThisVeh[iRocketPos]
	ENTITY_INDEX eiCustomAccuracyVeh
	
	IF iCustomAccuracyVeh > -1
	AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iCustomAccuracyVeh])
		eiCustomAccuracyVeh = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[iCustomAccuracyVeh])
	ENDIF
	
	IF iCustomAccuracyVeh = -1
	OR TargetEntity = eiCustomAccuracyVeh
		fAccuracy = g_FMMC_STRUCT_ENTITIES.fFMMCZoneRocketAccuracy[iRocketPos]
		bHoming = NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketNotHomingBS, iRocketPos)
		PRINTLN("[RCC MISSION][ZoneRockets] PROCESS_ZONE_STAGGERED_FIRE_ROCKETS_CLIENT | Zone ", iZone, ", Fire Rocket Zone - Loaded globals accuracy ", fAccuracy, " / bHoming ", bHoming)
	ELSE
		fAccuracy = 100.0
		bHoming = TRUE
		PRINTLN("[RCC MISSION][ZoneRockets] PROCESS_ZONE_STAGGERED_FIRE_ROCKETS_CLIENT | Zone ", iZone, ", Fire Rocket Zone - Using perfect accuracy and homing!")
	ENDIF
	
	FLOAT fRocketSpeed = 62.80637891
	VECTOR vAimAtLocation = GET_VECTOR_TO_AIM_AT(GET_ENTITY_COORDS(TargetEntity), GET_ENTITY_VELOCITY(TargetEntity), vRocketFireFromThisLocation, fRocketSpeed, fAccuracy)
	
	ENTITY_INDEX IgnoreEntity
	ENTITY_INDEX HomingEntity
	
	IF g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketLinkedObj[iRocketPos] != -1
		// No need to check if it exists or is alive, it must be for this rocket position to have been chosen
		PRINTLN("[RCC MISSION][ZoneRockets] PROCESS_ZONE_STAGGERED_FIRE_ROCKETS_CLIENT | Zone ", iZone, ", Fire Rocket Zone - Rocket pos ", iRocketPos," should ignore linked obj ", g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketLinkedObj[iRocketPos])
		IgnoreEntity = NET_TO_OBJ(GET_OBJECT_NET_ID(g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketLinkedObj[iRocketPos]))
	ENDIF
	
	IF bHoming
		PRINTLN("[RCC MISSION][ZoneRockets] PROCESS_ZONE_STAGGERED_FIRE_ROCKETS_CLIENT | Zone ", iZone, ", Fire Rocket Zone - This rocket pos ", iRocketPos, " is set to home in, use player / player veh")
		HomingEntity = TargetEntity
	ENDIF
	
	IF IS_BIT_SET(iZoneRocketClearToFireBS, iZone)
		PRINTLN("[RCC MISSION][ZoneRockets] PROCESS_ZONE_STAGGERED_FIRE_ROCKETS_CLIENT | Zone ", iZone, ", Fire Rocket Zone - Firing rocket from ", vRocketFireFromThisLocation, " to ", vAimAtLocation)
		SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY(vRocketFireFromThisLocation, vAimAtLocation, 99, fAccuracy > 99.9, GET_FIRE_ROCKETS_ZONE_WEAPON_TYPE(iZone), NULL, TRUE, TRUE, DEFAULT, IgnoreEntity, HomingEntity)
		REINIT_NET_TIMER(tdRocketTimer)
		CLEAR_BIT(iZoneRocketClearToFireBS, iZone)
		
	ELSE	
		PRINTLN("[RCC MISSION][ZoneRockets] PROCESS_ZONE_STAGGERED_FIRE_ROCKETS_CLIENT | Zone ", iZone, ", Fire Rocket Zone - Calling CLEAR_AREA_OF_PROJECTILES at ", vRocketFireFromThisLocation)
		CLEAR_AREA_OF_PROJECTILES(vRocketFireFromThisLocation, 10.0, TRUE)
		SET_BIT(iZoneRocketClearToFireBS, iZone)
		
	ENDIF
ENDPROC

FUNC BOOL SHOULD_AIR_DEFENCE_ZONE_TARGET_PLAYER(INT iZone, BOOL bIgnorePosition = FALSE)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue2 > 0
	AND NOT bIgnorePosition
		#IF IS_DEBUG_BUILD
		IF bZoneDebug
		AND IS_ZONE_TRIGGERING(iZone)
			TEXT_LABEL_63 tlVisualDebug = "Altitude: "
			tlVisualDebug += FLOAT_TO_STRING(GET_ENTITY_HEIGHT_ABOVE_GROUND(LocalPlayerPed))
			tlVisualDebug += " / "
			tlVisualDebug += FLOAT_TO_STRING(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue2)
			DRAW_DEBUG_TEXT_2D(tlVisualDebug, (<<0.35, 0.5, 0.5>>), 255, 255, 255, 255)
		ENDIF
		#ENDIF
		
		IF GET_ENTITY_HEIGHT_ABOVE_GROUND(LocalPlayerPed) < g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue2
			PRINTLN("[Zones][Zone ", iZone, "] SHOULD_AIR_DEFENCE_ZONE_TARGET_PLAYER | Player isn't high up enough! ", GET_ENTITY_HEIGHT_ABOVE_GROUND(LocalPlayerPed), " / ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue2)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_PED_IN_FLYING_VEHICLE(PlayerPedToUse)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_AIR_DEFENCE_TARGET_PARACHUTING_PLAYERS)
		IF IS_PED_IN_PARACHUTE_FREE_FALL(PlayerPedToUse)
			RETURN TRUE
		ENDIF
		
		IF GET_PED_PARACHUTE_STATE(PlayerPedToUse) != PPS_INVALID
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//This function handles warning shots, and keeping the air defence sphere disabled until after the warning shots are done
PROC PROCESS_AIR_DEFENCE_ZONE_EVERY_FRAME(INT iZone)
	
	CLEAR_BIT(iAirDefenceZone_UsingTimersBS, iZone)
	
	IF !bPedToUseOk
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_AIR_DEFENCE_DISPLAY_WARNING_SHARD)
		IF SHOULD_AIR_DEFENCE_ZONE_TARGET_PLAYER(iZone, TRUE)
		AND IS_ZONE_TRIGGERABLE(iZone)
			IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_AIR_DEFENCE_SHOWN_SHARD)
				PRINTLN("[Zones][Zone ", iZone, "] PROCESS_AIR_DEFENCE_ZONE_EVERY_FRAME | Showing warning shard!")
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "FMMC_AD_WSHRD", "FMMC_AD_SHRD", DEFAULT, DEFAULT, HUD_COLOUR_RED)
				SET_BIT(iLocalBoolCheck32, LBOOL32_AIR_DEFENCE_SHOWN_SHARD)
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_AIR_DEFENCE_SHOWN_SHARD)
				PRINTLN("[Zones][Zone ", iZone, "] PROCESS_AIR_DEFENCE_ZONE_EVERY_FRAME | Clearing LBOOL32_AIR_DEFENCE_SHOWN_SHARD!")
				CLEAR_BIT(iLocalBoolCheck32, LBOOL32_AIR_DEFENCE_SHOWN_SHARD)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT SHOULD_AIR_DEFENCE_ZONE_TARGET_PLAYER(iZone)
		// If they're not in an air vehicle or shouldn't be targetted for any other reason, reset the timers and don't process anything else until they're in an air vehicle
		
		IF iAirDefenceZone_UsingTimersBS = 0 // Only reset the timers if ALL air defence zones want to
			IF HAS_NET_TIMER_STARTED(stAirDefenceZone_TimeSpentInside)
			OR HAS_NET_TIMER_STARTED(stAirDefenceZone_WarningShotTimer)
				RESET_NET_TIMER(stAirDefenceZone_TimeSpentInside)
				RESET_NET_TIMER(stAirDefenceZone_WarningShotTimer)
				PRINTLN("[Zones][Zone ", iZone, "] PROCESS_AIR_DEFENCE_ZONE_EVERY_FRAME | Resetting stAirDefenceZone_TimeSpentInside and stAirDefenceZone_WarningShotTimer")
			ENDIF
		ENDIF
		
		IF DOES_AIR_DEFENCE_SPHERE_EXIST(iFMMCAirDefenceZoneArea[iZone])
			IF NOT IS_ZONE_TRIGGERING(iZone)
			AND IS_ZONE_TRIGGERABLE(iZone)
				// In this case, we actually DO want to turn on the air defence sphere, purely for the anti-rocket/sniper rifle benefits.
				SET_PLAYER_TARGETTABLE_FOR_AIR_DEFENCE_SPHERE(NATIVE_TO_INT(LocalPlayer), iFMMCAirDefenceZoneArea[iZone], TRUE)
			
			ELSE
				// The player is meant to be safe from the air defence sphere right now despite being inside the Zone.
				SET_PLAYER_TARGETTABLE_FOR_AIR_DEFENCE_SPHERE(NATIVE_TO_INT(LocalPlayer), iFMMCAirDefenceZoneArea[iZone], FALSE)
				
			ENDIF
		ENDIF
		
		SET_ZONE_AS_NOT_SAFE_TO_BLIP(iZone)
		EXIT
		
	ELSE
		SET_ZONE_AS_SAFE_TO_BLIP(iZone)
		
	ENDIF
	
	IF NOT IS_ZONE_TRIGGERING(iZone)
		IF DOES_AIR_DEFENCE_SPHERE_EXIST(iFMMCAirDefenceZoneArea[iZone])
			// Disable the air defence zone controlled by this particular Zone
			SET_PLAYER_TARGETTABLE_FOR_AIR_DEFENCE_SPHERE(NATIVE_TO_INT(LocalPlayer), iFMMCAirDefenceZoneArea[iZone], FALSE)
		ENDIF
		
		EXIT
	ENDIF
	
	SET_BIT(iAirDefenceZone_UsingTimersBS, iZone)
	
	IF IS_PED_IN_FLYING_VEHICLE(PlayerPedToUse)
	AND NOT IS_PLAYER_IN_VEH_SEAT(LocalPlayer, VS_DRIVER)
		// Block passengers from firing because being a passenger in a flying vehicle is a bit of an air-defence loophole as far as projectile blocking is concerned
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue <= 1
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			PRINTLN("[Zones][Zone ", iZone, "] PROCESS_AIR_DEFENCE_ZONE_EVERY_FRAME | Exploding local player vehicle instantly due to its Time Allowed Inside being 0")
			NETWORK_EXPLODE_VEHICLE(GET_VEHICLE_PED_IS_USING(LocalPlayerPed))
		ELSE
			PRINTLN("[Zones][Zone ", iZone, "] PROCESS_AIR_DEFENCE_ZONE_EVERY_FRAME | Exploding local player instantly due to its Time Allowed Inside being 0")
			BROADCAST_FMMC_AIR_DEFENCE_SHOT(iZone, GET_ENTITY_COORDS(LocalPlayerPed))
			IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
				SET_ENTITY_HEALTH(LocalPlayerPed, 0)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(stAirDefenceZone_TimeSpentInside)
		REINIT_NET_TIMER(stAirDefenceZone_TimeSpentInside)
		PRINTLN("[Zones][Zone ", iZone, "] PROCESS_AIR_DEFENCE_ZONE_EVERY_FRAME | Starting stAirDefenceZone_TimeSpentInside")
	ELSE
		// Keep the air defence sphere non-lethal until the player has been triggering the Zone for longer than the amount of time set in fZoneValue
		IF HAS_NET_TIMER_EXPIRED(stAirDefenceZone_TimeSpentInside, ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue))
		
			IF DOES_AIR_DEFENCE_SPHERE_EXIST(iFMMCAirDefenceZoneArea[iZone])
				SET_PLAYER_TARGETTABLE_FOR_AIR_DEFENCE_SPHERE(NATIVE_TO_INT(LocalPlayer), iFMMCAirDefenceZoneArea[iZone], TRUE)
				PRINTLN("[Zones][Zone ", iZone, "] PROCESS_AIR_DEFENCE_ZONE_EVERY_FRAME | Enabling air defence sphere")
			ENDIF
			
			IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				PRINTLN("[Zones][Zone ", iZone, "] PROCESS_AIR_DEFENCE_ZONE_EVERY_FRAME | Exploding local player now!")
				BROADCAST_FMMC_AIR_DEFENCE_SHOT(iZone, GET_ENTITY_COORDS(LocalPlayerPed))
				
				IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
					SET_ENTITY_HEALTH(LocalPlayerPed, 0)
				ENDIF
			ELSE
				IF NOT DOES_AIR_DEFENCE_SPHERE_EXIST(iFMMCAirDefenceZoneArea[iZone])
					PRINTLN("[Zones][Zone ", iZone, "] PROCESS_AIR_DEFENCE_ZONE_EVERY_FRAME | Exploding local player's vehicle due to time running out and not using the native air defence system")
					BROADCAST_FMMC_AIR_DEFENCE_SHOT(iZone, GET_ENTITY_COORDS(LocalPlayerPed))
					NETWORK_EXPLODE_VEHICLE(GET_VEHICLE_PED_IS_USING(LocalPlayerPed))
				ENDIF
			ENDIF
		ELSE
			IF DOES_AIR_DEFENCE_SPHERE_EXIST(iFMMCAirDefenceZoneArea[iZone])
				SET_PLAYER_TARGETTABLE_FOR_AIR_DEFENCE_SPHERE(NATIVE_TO_INT(LocalPlayer), iFMMCAirDefenceZoneArea[iZone], FALSE)
				PRINTLN("[Zones][Zone ", iZone, "] PROCESS_AIR_DEFENCE_ZONE_EVERY_FRAME | Turning off air defence sphere for Zone ", iZone)
			ENDIF
		ENDIF
	ENDIF
	
	REQUEST_NAMED_PTFX_ASSET("scr_apartment_mp")
	IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_USED_AIR_DEFENCE_PTFX)
		SET_BIT(iLocalBoolCheck31, LBOOL31_USED_AIR_DEFENCE_PTFX)
	ENDIF
	
	IF HAS_NAMED_PTFX_ASSET_LOADED("scr_apartment_mp")
		IF NOT HAS_NET_TIMER_STARTED(stAirDefenceZone_WarningShotTimer)
			REINIT_NET_TIMER(stAirDefenceZone_WarningShotTimer)
			PRINTLN("[Zones][Zone ", iZone, "] PROCESS_AIR_DEFENCE_ZONE_EVERY_FRAME | Starting stAirDefenceZone_WarningShotTimer")
			EXIT
		ENDIF
		
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(stAirDefenceZone_WarningShotTimer, ciAirDefenceZone_WarningShotTimer_Length)
			PRINTLN("[Zones][Zone ", iZone, "] PROCESS_AIR_DEFENCE_ZONE_EVERY_FRAME | Firing warning shot!")
			
			INT iRandomX = GET_RANDOM_INT_IN_RANGE(-20, 20)
			INT iRandomY = GET_RANDOM_INT_IN_RANGE(20, 40) //always in front
			INT iRandomZ = GET_RANDOM_INT_IN_RANGE(-10, 20)
			VECTOR vWarningShotPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(LocalPlayerPed, <<iRandomX, iRandomY, iRandomZ>>)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sMissionYachtInfo[ciYACHT_LOBBY_HOST_YACHT_INDEX].iMYacht_Bitset, ciYACHT_BITSET__MATCH_LOBBY_HOST_YACHT)
			AND DOES_AIR_DEFENCE_SPHERE_EXIST(iFMMCAirDefenceZoneArea[iZone])
				IF IS_FAKE_MULTIPLAYER_MODE_SET()
					FIRE_AIR_DEFENCE_SPHERE_WEAPON_AT_POSITION(iFMMCAirDefenceZoneArea[iZone], vWarningShotPos) // Creates a local air defence explosion in test mode
				ELSE
					BROADCAST_PRIVATE_YACHT_AIRDEF_EXPLOSION(vWarningShotPos, vWarningShotPos, iFMMCAirDefenceZoneArea[iZone], GET_FMMC_YACHT_ID(ciYACHT_LOBBY_HOST_YACHT_INDEX), TRUE) // Broadcasts it out so all players create local explosions at the same time
				ENDIF
			ELSE
				// We're not using a Yacht, use our basic method instead
				BROADCAST_FMMC_AIR_DEFENCE_SHOT(iZone, vWarningShotPos)
			ENDIF
			
			RESET_NET_TIMER(stAirDefenceZone_WarningShotTimer)
		ENDIF
	ELSE
		PRINTLN("[Zones][Zone ", iZone, "] PROCESS_AIR_DEFENCE_ZONE_EVERY_FRAME | Waiting for scr_apartment_mp")
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_PIT_STOP_ZONE_EVERY_FRAME(INT iZone)
	
	VEHICLE_INDEX viVehToUse
	IF IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
		viVehToUse = GET_VEHICLE_PED_IS_IN(PlayerPedToUse)
	ENDIF
	
	IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(iRacePitZoneCooldown)
		// Waiting for cooldown
		IF MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(iRacePitZoneCooldown, ciRacePitZoneCooldownLength)
			PRINTLN("[RC][PlayerAbilities][RepairAbility] PROCESS_VEHICLE_PIT_STOP_ZONE_EVERY_FRAME - Repair cooldown is up!")
			MC_STOP_NET_TIMER_WITH_TIMESTAMP(iRacePitZoneCooldown)
			
			IF iRacePitZoneSoundID > -1
				IF DOES_ENTITY_EXIST(viVehToUse)
					PLAY_SOUND_FROM_ENTITY(-1, "Pit_Stop_Complete", viVehToUse, "DLC_H3_Circuit_Racing_Sounds")
				ENDIF
				
				STOP_SOUND(iRacePitZoneSoundID)
				iRacePitZoneSoundID = -1
			ENDIF
			
		ELSE
			PRINTLN("[RC][PlayerAbilities_SPAM][RepairAbility_SPAM] PROCESS_VEHICLE_PIT_STOP_ZONE_EVERY_FRAME - Waiting for Repair cooldown!")
			
		ENDIF
		
		EXIT
	ENDIF
	
	IF NOT IS_ZONE_TRIGGERING(iZone)
		IF iCurrentRacePitZone = iZone
			iCurrentRacePitZone = -1
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_HEIST3/Circuit_Racing")
			PRINTLN("[RC][PlayerAbilities] PROCESS_VEHICLE_PIT_STOP_ZONE_EVERY_FRAME - Clearing iCurrentRacePitZone and releasing Audio Bank")
		ENDIF
		
		IF IS_BIT_SET(iRacePitZoneInUseBS, iZone)
			IF IS_ENTITY_A_GHOST(LocalPlayerPed)
				SET_LOCAL_PLAYER_AS_GHOST(FALSE)
				PRINTLN("[PlayerAbilities] PROCESS_VEHICLE_PIT_STOP_ZONE_EVERY_FRAME - Clearing Ghost")
			ENDIF
			
			RESET_NET_TIMER(stRacePitZoneTimer)
			
			SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(LocalPlayer, 1.0)
			CLEAR_BIT(iRacePitZoneInUseBS, iZone)
		ENDIF
		
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(viVehToUse)
	OR NOT NETWORK_HAS_CONTROL_OF_ENTITY(viVehToUse)
	OR IS_VEHICLE_FUCKED(viVehToUse)
		EXIT
	ENDIF
	
	// From this point forward, we're inside the Zone in a vehicle, and almost ready to be repaired
		
	BOOL bVehicleNeedsRepairing = GET_VEHICLE_BODY_HEALTH(viVehTouse) < 1000

	IF iCurrentRacePitZone = -1
		iCurrentRacePitZone = iZone
		PRINTLN("[RC][PlayerAbilities][RepairAbility] PROCESS_VEHICLE_PIT_STOP_ZONE_EVERY_FRAME - Setting iCurrentRacePitZone to ", iCurrentRacePitZone)
	ENDIF
	
	IF REQUEST_SCRIPT_AUDIO_BANK("DLC_HEIST3/Circuit_Racing")
		OPEN_WHEEL_UI_INFO sDummyStruct
		IF PERFORM_RACE_PIT_STOP(viVehTouse, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue, stRacePitZoneTimer, !bVehicleNeedsRepairing, iZone,	sDummyStruct)
			PRINTLN("[RC][PlayerAbilities][RepairAbility] PROCESS_VEHICLE_PIT_STOP_ZONE_EVERY_FRAME - Repaired player's vehicle! Starting cooldown now")
			MC_REINIT_NET_TIMER_WITH_TIMESTAMP(iRacePitZoneCooldown)
			ANIMPOSTFX_PLAY("CarPitstopHealth", 0, FALSE)
			
			IF iRacePitZoneSoundID = -1
				iRacePitZoneSoundID = GET_SOUND_ID()
				PLAY_SOUND_FROM_ENTITY(iRacePitZoneSoundID, "Pit_Stop_Loop", viVehTouse, "DLC_H3_Circuit_Racing_Sounds")
			ENDIF
		ELSE
			PRINTLN("[RC][PlayerAbilities][RepairAbility] PROCESS_VEHICLE_PIT_STOP_ZONE_EVERY_FRAME - Waiting for RACE_PIT_STOP!")
			
		ENDIF
		
	ELSE
		PRINTLN("[RC][PlayerAbilities][RepairAbility] PROCESS_VEHICLE_PIT_STOP_ZONE_EVERY_FRAME - Waiting to load audio!")
		
	ENDIF
	
	IF NOT IS_ENTITY_A_GHOST(LocalPlayerPed)
		IF bVehicleNeedsRepairing
			SET_LOCAL_PLAYER_AS_GHOST(TRUE, TRUE)
			PRINTLN("[RC][PlayerAbilities][RepairAbility] PROCESS_VEHICLE_PIT_STOP_ZONE_EVERY_FRAME - Setting local player to be a ghost!")
		ENDIF
		
	ELSE
		IF IS_BIT_SET(iRacePitZoneInUseBS, iZone)
		AND NOT bVehicleNeedsRepairing
			IF IS_ENTITY_A_GHOST(LocalPlayerPed)
				SET_LOCAL_PLAYER_AS_GHOST(FALSE)
				PRINTLN("[RC][PlayerAbilities][RepairAbility] PROCESS_VEHICLE_PIT_STOP_ZONE_EVERY_FRAME - Local player doesn't need repairing - clearing ghost status")
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iRacePitZoneInUseBS, iZone)
		PRINT_HELP("RCE_PIT_STOP") //Remain stationary while in the pit area to regain vehicle health
		SET_BIT(iRacePitZoneInUseBS, iZone)	
	ENDIF
	
ENDPROC

PROC PROCESS_LIVES_DEPLETION_ZONE_EVERY_FRAME(INT iZone)
	
	INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneValue3
	
	//Check how many players from the relevant team are triggering this zone
	//Or if we can't take off anymore lives
	IF GET_NUM_PLAYERS_IN_TEAM_TRIGGERING_ZONE(iZone, iTeam) > 0
	OR GET_TEAM_DEATHS(iTeam) >= GET_FMMC_MISSION_LIVES_FOR_TEAM(iTeam)
		RESET_NET_TIMER(stLivesDepletionTimer)
		RESET_NET_TIMER(stLivesDepletionGraceTimer)
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(stLivesDepletionGraceTimer)
		PRINTLN("[RC] PROCESS_LIVES_DEPLETION_ZONE_EVERY_FRAME - Starting grace timer")
		START_NET_TIMER(stLivesDepletionGraceTimer)
	ENDIF
	
	IF NOT HAS_NET_TIMER_EXPIRED(stLivesDepletionGraceTimer, ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue * 1000))
		EXIT
	ENDIF
	
	//The grace timer has expired and now we need to start depleting lives
	IF NOT HAS_NET_TIMER_STARTED(stLivesDepletionTimer)
		PRINTLN("[RC] PROCESS_LIVES_DEPLETION_ZONE_EVERY_FRAME - Starting lives depletion timer")
		START_NET_TIMER(stLivesDepletionTimer)
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(stLivesDepletionTimer, ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue2 * 1000))
		PRINTLN("[RC] PROCESS_LIVES_DEPLETION_ZONE_EVERY_FRAME - Lives depletion timer expired, reset and take off life")
		RESET_NET_TIMER(stLivesDepletionTimer)

		IF bIsLocalPlayerHost
			//Take off a life from here
			SET_TEAM_DEATHS(iTeam, GET_TEAM_DEATHS(iTeam) + 1)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SPAWN_AREA_ZONE_FUNCTIONALITY(INT iZone)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_USE_AS_SPAWN_AREA)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_ONLY_USE_AS_SPAWN_AREA_WHEN_TRIGGERED)
		IF IS_ZONE_TRIGGERING(iZone)
			iSpawnAreaZoneThisFrame = iZone
		ENDIF
		
	ELSE
	
		IF iSpawnAreaZoneThisFrame > -1
			IF IS_ZONE_TRIGGERING(iSpawnAreaZoneThisFrame)
			AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iSpawnAreaZoneThisFrame].iZoneBS2, ciFMMC_ZONEBS2_ONLY_USE_AS_SPAWN_AREA_WHEN_TRIGGERED)
				// We've already got a definitive triggering spawn area to use, don't check this distance-based one
				EXIT
			ENDIF
		ENDIF
		
		FLOAT fDistance = VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), GET_ZONE_RUNTIME_CENTRE(iZone))
		
		IF fDistance < fCurrentClosestSpawnAreaZoneDistance
		OR fCurrentClosestSpawnAreaZoneDistance < 0
		OR IS_ZONE_TRIGGERING(iZone)
			iSpawnAreaZoneThisFrame = iZone
			fCurrentClosestSpawnAreaZoneDistance = fDistance
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_ZONE_EVERY_FRAME_CLIENT(INT iZone)
	
	#IF IS_DEBUG_BUILD
	DRAW_ZONE_DEBUG(iZone)
	#ENDIF
	
	PROCESS_ZONE_MID_MISSION_REMOVAL(iZone)
	
	PROCESS_ZONE_RESIZING(iZone)
	
	IF NOT DOES_ZONE_REQUIRE_CONTINUOUS_PROCESSING(iZone)
		EXIT
	ENDIF
	
	IF DOES_ZONE_REQUIRE_EVERY_FRAME_TRIGGER_CHECKS(iZone)
		IF SHOULD_THIS_ZONE_BE_TRIGGERED(iZone)
			SET_ZONE_IS_TRIGGERING(iZone)
		ELSE
			SET_ZONE_IS_NOT_TRIGGERING(iZone)
		ENDIF
	ENDIF
	
	PROCESS_ZONE_TIMER_EVERY_FRAME_CLIENT(iZone)
	
	PROCESS_ZONE_BLIP(biZoneBlips[iZone], iZone)
	PROCESS_ZONE_MARKER(iZone)
	
	// Exit here if the Zone doesn't exist. The functions above are designed to be called regardless
	IF NOT DOES_ZONE_EXIST(iZone)
		EXIT
	ENDIF
	
	PROCESS_SPAWN_AREA_ZONE_FUNCTIONALITY(iZone)
	
	PROCESS_ZONE_RADIUS_CHANGES(iZone)
	
	PROCESS_ZONE_SMOOTH_ATTACHMENT_OFFSET(iZone)
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType
	
		CASE ciFMMC_ZONE_TYPE__SPAWN_PROTECTION
			PROCESS_SPAWN_PROTECTION_ZONE_EVERY_FRAME_CLIENT(iZone)
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__FIRE_ROCKETS_AT_PLAYER
			PROCESS_FIRE_ROCKETS_ZONE_EVERY_FRAME_CLIENT(iZone)
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__BLOCK_PARACHUTE
			IF IS_ZONE_TRIGGERING(iZone)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
				SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableParachuting, TRUE)
				PRINTLN("[Zones][Zone ", iZone, "] PROCESS_ZONE_EVERY_FRAME_CLIENT | ciFMMC_ZONE_TYPE__BLOCK_PARACHUTE - Disabling INPUT_PARACHUTE_DEPLOY")
				
				IF GET_PED_PARACHUTE_STATE(LocalPlayerPed) = PPS_PARACHUTING
					TASK_SKY_DIVE(LocalPlayerPed, TRUE)
					PRINTLN("[Zones][Zone ", iZone, "] PROCESS_ZONE_EVERY_FRAME_CLIENT | ciFMMC_ZONE_TYPE__BLOCK_PARACHUTE - Using TASK_SKY_DIVE!")
				ENDIF
				
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__REMOVE_AND_BLOCK_PARACHUTE
			IF IS_ZONE_TRIGGERING(iZone)				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
				SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableParachuting, TRUE)
				PRINTLN("[Zones][Zone ", iZone, "] PROCESS_ZONE_EVERY_FRAME_CLIENT | ciFMMC_ZONE_TYPE__REMOVE_AND_BLOCK_PARACHUTE - Disabling INPUT_PARACHUTE_DEPLOY")
				
				IF GET_PED_PARACHUTE_STATE(LocalPlayerPed) = PPS_PARACHUTING
					TASK_SKY_DIVE(LocalPlayerPed, TRUE)
					PRINTLN("[Zones][Zone ", iZone, "] PROCESS_ZONE_EVERY_FRAME_CLIENT | ciFMMC_ZONE_TYPE__REMOVE_AND_BLOCK_PARACHUTE - Using TASK_SKY_DIVE!")
				ENDIF
				CLEAN_UP_PLAYER_PED_PARACHUTE()
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__KILL_ZONE
			IF IS_ZONE_TRIGGERING(iZone)
				IF IS_SCREEN_FADED_IN()
				AND bLocalPlayerPedOK
				AND NOT HAS_LOCAL_PLAYER_FINISHED_OR_REACHED_END()
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
						NETWORK_EXPLODE_VEHICLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), TRUE)
						PRINTLN("[Zones][Zone ", iZone, "] PROCESS_ZONE_EVERY_FRAME_CLIENT | ciFMMC_ZONE_TYPE__KILL_ZONE - Exploding player's vehicle")
					ENDIF
					
					IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
						SET_ENTITY_HEALTH(LocalPlayerPed, 0)
					ENDIF
					
					PRINTLN("[Zones][Zone ", iZone, "] PROCESS_ZONE_EVERY_FRAME_CLIENT | ciFMMC_ZONE_TYPE__KILL_ZONE - Killing player")
				ELSE
					PRINTLN("[Zones][Zone ", iZone, "] PROCESS_ZONE_EVERY_FRAME_CLIENT | ciFMMC_ZONE_TYPE__KILL_ZONE - Not killing player even though they're in the zone")
				ENDIF
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__BLOCK_PLAYER_CONTROL
			IF IS_ZONE_TRIGGERING(iZone)
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_BlockPlayerControl_InputContext)						
					PRINTLN("[RCC MISSION] PROCESS_ZONES ciFMMC_ZONE_TYPE__BLOCK_PLAYER_CONTROL Calling: DISABLE_CONTROL_ACTION > INPUT_CONTEXT")
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT, TRUE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT, TRUE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_TRANSFORM, TRUE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF, TRUE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_BOMB_BAY, TRUE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_COUNTER, TRUE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CELLPHONE_RIGHT, TRUE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SHUFFLE, TRUE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GRAPPLING_HOOK, TRUE)
					
					// Special actions so we don't get trapped in these states.
					IF bLocalPlayerPedOK
						IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
							VEHICLE_INDEX vehIndex
							vehIndex = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
							
							IF GET_ENTITY_MODEL(vehIndex) = STROMBERG
							AND IS_VEHICLE_IN_SUBMARINE_MODE(vehIndex)
								PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_EVERY_FRAME_CLIENT | ciFMMC_ZONE_TYPE__BLOCK_PLAYER_CONTROL Calling: TRANSFORM_TO_CAR")
								TRANSFORM_TO_CAR(vehIndex)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__GOAL_AREA
			//All in BMBFB_PROCESS_BALL
			
		CASE ciFMMC_ZONE_TYPE__METAL_DETECTOR
			PRINTLN("[RCC MISSION][MetalDetector][Zones][Zone ", iZone, "] PROCESS_ZONE_EVERY_FRAME_CLIENT | ciFMMC_ZONE_TYPE__METAL_DETECTOR - Hitting Metal Detector Zone checks for zone ", iZone)
			
			IF IS_ZONE_TRIGGERING(iZone)
			AND IS_METAL_DETECTOR_FUNCTIONAL(iZone)
				PRINTLN("[RCC MISSION][MetalDetector][Zones][Zone ", iZone, "] PROCESS_ZONE_EVERY_FRAME_CLIENT | ciFMMC_ZONE_TYPE__METAL_DETECTOR - Player in Metal Detector zone ", iZone)
				
				IF NOT IS_BIT_SET(iMetalDetectorZoneHasBeenAlertedBS, iZone)
					BROADCAST_FMMC_METAL_DETECTOR_ALERTED(iZone, iLocalPart, GET_ENTITY_COORDS(LocalPlayerPed), IS_PLAYER_HOLDING_METAL_DETECTOR_EXTREME_WEAPON())
					SET_METAL_DETECTOR_AS_ALERTED(iZone, GET_ENTITY_COORDS(LocalPlayerPed), IS_PLAYER_HOLDING_METAL_DETECTOR_EXTREME_WEAPON())
					
					PRINTLN("[RCC MISSION][MetalDetector][Zones][Zone ", iZone, "] PROCESS_ZONE_EVERY_FRAME_CLIENT | ciFMMC_ZONE_TYPE__METAL_DETECTOR - SET_METAL_DETECTOR_AS_ALERTED for zone: ", iZone)
				ENDIF
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__STUNT_SCORE
			
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__HEALTH_DRAIN
			PROCESS_HEALTH_DRAIN_ZONE_EVERY_FRAME(iZone)
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__EXPLODE_PLAYER
			IF IS_ZONE_TRIGGERING(iZone)
			AND bLocalPlayerPedOK
				PROCESS_PLAYER_EXPLODE_ZONE(tdExplodePlayerZoneTimer[iZone], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue)
				PROCESS_PLAYER_EXPLODE_ZONE_EVERY_FRAME_HUD(tdExplodePlayerZoneTimer[iZone], iZone)
			ELSE
				IF HAS_NET_TIMER_STARTED(tdExplodePlayerZoneTimer[iZone])
					CLEAR_ALL_BIG_MESSAGES()
					RESET_NET_TIMER(tdExplodePlayerZoneTimer[iZone])
				ENDIF
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__AIR_DEFENCE
			PROCESS_AIR_DEFENCE_ZONE_EVERY_FRAME(iZone)
		BREAK

		CASE ciFMMC_ZONE_TYPE__ALTIMETER_SYSTEM_ZONE
			IF NOT IS_ZONE_TRIGGERING(iZone)
				PRINTLN("[LM][AltitudeSystem][Zones][Zone ", iZone, "] We are not inside the Altimeter Zone, Index: ", iZone, " (ciFMMC_ZONE_TYPE__ALTIMETER_SYSTEM_ZONE)")
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__SPOOK_PED_IN_RADIUS
			IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdRadiusSpookZoneTimer[iZone], 5000)
			OR NOT HAS_NET_TIMER_STARTED(tdRadiusSpookZoneTimer[iZone])
				IF IS_ZONE_TRIGGERING(iZone)
					PRINTLN("[LM][SpookAggro][Zones][Zone ", iZone, "] Inside ciFMMC_ZONE_TYPE__SPOOK_PED_IN_RADIUS Zone: ", iZone, " calling spookPedsAtCoordInRadius")
					CALL spookPedsAtCoordInRadius(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], GET_PLACED_ZONE_RADIUS(iZone), ciSpookCoordID_GENERIC)
					RESET_NET_TIMER(tdRadiusSpookZoneTimer[iZone])
					START_NET_TIMER(tdRadiusSpookZoneTimer[iZone])					
				ENDIF					
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__ANTI_ZONE
			IF IS_ZONE_TRIGGERING(iZone)
				SET_ANTI_ZONE_NOT_VALID_TO_TRIGGER_CONNECTED_ZONES(iZone)
			ELSE
				SET_ANTI_ZONE_VALID_TO_TRIGGER_CONNECTED_ZONES(iZone)
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__PED_PLAYER_LEAVE_VEHICLE
			IF IS_ZONE_TRIGGERING(iZone)
				IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
					IF NOT IS_PED_PERFORMING_SCRIPT_TASK(localPlayerPed, SCRIPT_TASK_LEAVE_VEHICLE)
						
						PRINTLN("[LM][Zones][Zone ", iZone, "] Inside ciFMMC_ZONE_TYPE__PED_PLAYER_LEAVE_VEHICLE - Tasking local player ped to leave.")
						
						VEHICLE_INDEX vehPedIsIn
						vehPedIsIn = GET_VEHICLE_PED_IS_IN(localPlayerPed)
						ENTER_EXIT_VEHICLE_FLAGS eEFlags
						
						eEFlags = eEFlags | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP
						
						IF IS_ENTITY_IN_AIR(vehPedIsIn)
							eEFlags = eEFlags | ECF_JUMP_OUT
						ENDIF
						
						TASK_LEAVE_ANY_VEHICLE(localPlayerPed, 0, eEFlags)
						
					ENDIF
				ENDIF
			ENDIF
		BREAK 
		
		CASE ciFMMC_ZONE_TYPE__PED_PLAYER_PREVENT_LEAVE_VEHICLE
			IF IS_ZONE_TRIGGERING(iZone)
				IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)				
					PRINTLN("[LM][Zones][Zone ", iZone, "] Inside ciFMMC_ZONE_TYPE__PED_PLAYER_PREVENT_LEAVE_VEHICLE - Blocking Leave Vehicle Inputs.")
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)			
				ENDIF
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__DROPOFF_ZONE
			IF g_FMMC_STRUCT.sFMMCEndConditions[iZoneStaggeredTeamToCheck].iDropOffType[GET_TEAM_CURRENT_RULE(iZoneStaggeredTeamToCheck)] = ciFMMC_DROP_OFF_TYPE_PLACED_ZONE
				IF IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iZoneStaggeredTeamToCheck].iDropOffZonesBS[GET_TEAM_CURRENT_RULE(iZoneStaggeredTeamToCheck)], iZone)
					// This Zone is set to be used as a dropoff on the current rule!
					IF iCurrentDropOffZone[iZoneStaggeredTeamToCheck] != iZone
						iCurrentDropOffZone[iZoneStaggeredTeamToCheck] = iZone
						vCurrentDropOffZoneCachedCentre[iZoneStaggeredTeamToCheck] = GET_ZONE_RUNTIME_CENTRE(iCurrentDropOffZone[iZoneStaggeredTeamToCheck])
						PRINTLN("[LM][Zones][Zone ", iZone, "] Setting iCurrentDropOffZone[", iZoneStaggeredTeamToCheck, "] to ", iCurrentDropOffZone[iZoneStaggeredTeamToCheck])
					ENDIF
				ELSE
					// This Zone is NOT set to be used as a dropoff on the current rule!
					IF iCurrentDropOffZone[iZoneStaggeredTeamToCheck] = iZone
						iCurrentDropOffZone[iZoneStaggeredTeamToCheck] = -1
						vCurrentDropOffZoneCachedCentre[iZoneStaggeredTeamToCheck] = <<0,0,0>>
						PRINTLN("[LM][Zones][Zone ", iZone, "] Clearing iCurrentDropOffZone[", iZoneStaggeredTeamToCheck, "]")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__RACE_PIT_ZONE
			PROCESS_VEHICLE_PIT_STOP_ZONE_EVERY_FRAME(iZone)
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__LIVES_DEPLETION_ZONE
			PROCESS_LIVES_DEPLETION_ZONE_EVERY_FRAME(iZone)
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__GIVE_AND_MAINTAIN_WANTED_LEVEL
			IF IS_ZONE_TRIGGERING(iZone)
				INT iWanted
				iWanted = ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue)
				PRINTLN("[WANTED][Zones][Zone ", iZone, "] PROCESS_ZONE_EVERY_FRAME_CLIENT | ciFMMC_ZONE_TYPE__GIVE_AND_MAINTAIN_WANTED_LEVEL - Forcing a wanted level of: ", iWanted)			
				SET_MAX_WANTED_LEVEL(iWanted)
				SET_PLAYER_WANTED_LEVEL(LocalPlayer, iWanted)
				SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
				REPORT_POLICE_SPOTTED_PLAYER(LocalPlayer)
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_ZONE_EVERY_FRAME_SERVER(INT iZone)
	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	
	PROCESS_ZONE_TIMER_SERVER(iZone)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Zone Staggered Processing
// ##### Description: Client and Server Zone Staggered Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_BOUNDS_ZONE_PRE_STAGGERED_CLIENT()
	INT iBoundsIndex
	FOR iBoundsIndex = 0 TO ciMAX_RULE_BOUNDS_PER_RULE - 1
		
		sRuleBoundsRuntimeVars[iBoundsIndex].vRuleBoundsPendingCentrePos = <<0,0,0>>
		sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFirstActiveZone_Temp = -1
		sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsPendingActiveZoneCount = 0
		
		sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsPendingTriggeredZoneCount = 0
	ENDFOR
ENDPROC

PROC PROCESS_BOUNDS_ZONE_STAGGERED_CLIENT(INT iZone)

	INT iBoundsIndex
	FOR iBoundsIndex = 0 TO ciMAX_RULE_BOUNDS_PER_RULE - 1
		
		IF NOT SHOULD_RULE_BOUNDS_WAIT_FOR_FULL_STAGGERED_LOOP(iBoundsIndex)
			IF IS_BIT_SET(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsBS, ciRULE_BOUNDS_BS__WAITING_FOR_ZONE_STAGGERED_LOOP_LOOP_POINT)
			AND sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFullZoneStaggeredLoop_LoopPoint = iZone
				// These bounds were waiting for the staggered loop to reach this point, so now we can clear the bit and allow the Bounds to be processed again after the end of the full Staggered Loop.
				CLEAR_BIT(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsBS, ciRULE_BOUNDS_BS__WAITING_FOR_ZONE_STAGGERED_LOOP_LOOP_POINT)
				sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFullZoneStaggeredLoop_LoopPoint = -1
				PRINTLN("[Zones][Zone ", iZone, "][Bounds][RB ", iBoundsIndex, "] PROCESS_BOUNDS_ZONE_STAGGERED_CLIENT | Reached the loop point! Once this staggered loop ends, we'll be able to process these Bounds again.")
			ENDIF
		ELSE
			PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "][Bounds_SPAM][RuleBounds_SPAM ", iBoundsIndex, "] PROCESS_BOUNDS_ZONE_STAGGERED_CLIENT | SHOULD_RULE_BOUNDS_WAIT_FOR_FULL_STAGGERED_LOOP is still returning TRUE")
		ENDIF
		
		IF NOT IS_THIS_ZONE_SET_AS_BOUNDS_ON_CURRENT_RULE(iZone, iBoundsIndex)
			// These bounds don't care about this zone
			RELOOP
		ENDIF
		
		IF sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFirstActiveZone_Temp = -1
			sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFirstActiveZone_Temp = iZone
		ENDIF
		
		sRuleBoundsRuntimeVars[iBoundsIndex].vRuleBoundsPendingCentrePos += GET_ZONE_RUNTIME_CENTRE(iZone)
		sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsPendingActiveZoneCount++
		sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsLatestBoundsZoneProcessed = iZone
		PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "][Bounds_SPAM][RuleBounds_SPAM ", iBoundsIndex, "] PROCESS_BOUNDS_ZONE_STAGGERED_CLIENT | Bounds Zone ", iZone, " is active! Triggered: ", IS_ZONE_TRIGGERING(iZone))

		IF IS_ZONE_TRIGGERING(iZone)
			sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsPendingTriggeredZoneCount++
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_BOUNDS_ZONE_POST_STAGGERED_CLIENT()
	INT iBoundsIndex
	FOR iBoundsIndex = 0 TO ciMAX_RULE_BOUNDS_PER_RULE - 1
		
		IF sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFirstActiveZone != sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFirstActiveZone_Temp
			PRINTLN("[Zones][Bounds][RB ", iBoundsIndex, "] PROCESS_BOUNDS_ZONE_POST_STAGGERED_CLIENT | Updating sRuleBoundsRuntimeVars[", iBoundsIndex, "].iRuleBoundsFirstActiveZone from ", sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFirstActiveZone, " to ", sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFirstActiveZone_Temp)
			sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFirstActiveZone = sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFirstActiveZone_Temp
		ENDIF
		
		IF NOT SHOULD_RULE_BOUNDS_WAIT_FOR_FULL_STAGGERED_LOOP(iBoundsIndex)
			IF IS_BIT_SET(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsBS, ciRULE_BOUNDS_BS__WAIT_FOR_NEXT_COMPLETED_ZONE_STAGGERED_LOOP)
				IF IS_BIT_SET(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsBS, ciRULE_BOUNDS_BS__WAITING_FOR_ZONE_STAGGERED_LOOP_LOOP_POINT)
				AND sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFullZoneStaggeredLoop_LoopPoint > -1
					PRINTLN("[Zones][Bounds][RB ", iBoundsIndex, "] PROCESS_BOUNDS_ZONE_POST_STAGGERED_CLIENT | Full Zone staggered loop complete, but we haven't had a chance to process all of the Zones we needed to yet, because our loop point is ", sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFullZoneStaggeredLoop_LoopPoint, " and we haven't reached it yet, so we'll have to wait for the end of the next staggered loop.")
				ELSE
					CLEAR_BIT(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsBS, ciRULE_BOUNDS_BS__WAIT_FOR_NEXT_COMPLETED_ZONE_STAGGERED_LOOP)
					sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFullZoneStaggeredLoop_LoopPoint = -1
					PRINTLN("[Zones][Bounds][RB ", iBoundsIndex, "] PROCESS_BOUNDS_ZONE_POST_STAGGERED_CLIENT | Full Zone staggered loop complete! Bounds ", iBoundsIndex, " can now be processed again")
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[Zones][Bounds_SPAM][RuleBounds_SPAM ", iBoundsIndex, "] PROCESS_BOUNDS_ZONE_POST_STAGGERED_CLIENT | Still waiting for SHOULD_RULE_BOUNDS_WAIT_FOR_FULL_STAGGERED_LOOP to be FALSE")
		ENDIF
		
		IF sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsActiveZoneCount != sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsPendingActiveZoneCount
			sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsActiveZoneCount = sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsPendingActiveZoneCount
			PRINTLN("[Zones][Bounds][RB ", iBoundsIndex, "] PROCESS_BOUNDS_ZONE_POST_STAGGERED_CLIENT | Setting iRuleBoundsActiveZoneCount to ", sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsActiveZoneCount)
		ENDIF
		
		IF sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsCurrentTriggeredZoneCount != sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsPendingTriggeredZoneCount
			sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsCurrentTriggeredZoneCount = sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsPendingTriggeredZoneCount
			PRINTLN("[Zones][Bounds][RB ", iBoundsIndex, "] PROCESS_BOUNDS_ZONE_POST_STAGGERED_CLIENT | Setting iRuleBoundsCurrentTriggeredZoneCount to ", sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsCurrentTriggeredZoneCount)
		ENDIF
		
		IF sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsActiveZoneCount > 0
			sRuleBoundsRuntimeVars[iBoundsIndex].vRuleBoundsCentrePos.x = (sRuleBoundsRuntimeVars[iBoundsIndex].vRuleBoundsPendingCentrePos.x / sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsActiveZoneCount)
			sRuleBoundsRuntimeVars[iBoundsIndex].vRuleBoundsCentrePos.y = (sRuleBoundsRuntimeVars[iBoundsIndex].vRuleBoundsPendingCentrePos.y / sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsActiveZoneCount)
			sRuleBoundsRuntimeVars[iBoundsIndex].vRuleBoundsCentrePos.z = (sRuleBoundsRuntimeVars[iBoundsIndex].vRuleBoundsPendingCentrePos.z / sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsActiveZoneCount)
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_ZONE_PRE_STAGGERED_CLIENT()
	PROCESS_BOUNDS_ZONE_PRE_STAGGERED_CLIENT()
ENDPROC

PROC PROCESS_ZONE_POST_STAGGERED_CLIENT()
	
	// Update the playerBD version of our triggered variable
	MC_playerBD[iLocalPart].iZoneIsTriggeredBS_Networked = iZoneIsTriggeredBS
	
	PROCESS_BOUNDS_ZONE_POST_STAGGERED_CLIENT()
	
	IF (IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE) OR IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_AND_CLEAR_WANTED_LEVEL_ZONE))
	AND NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE_TEMP)
		
		INT iMaxWanted = 5
		
		IF MC_serverBD.iPolice > 1
		AND GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice) > 0
			iMaxWanted = GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice)
		ENDIF
		
		PRINTLN("[RCC MISSION] PROCESS_ZONE_POST_STAGGERED_CLIENT - No longer in a block wanted level zone, setting max wanted level back to ", iMaxWanted)
		SET_MAX_WANTED_LEVEL(iMaxWanted)
		
		CLEAR_BIT(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE)
		CLEAR_BIT(iLocalBoolCheck13, LBOOL13_IN_BLOCK_AND_CLEAR_WANTED_LEVEL_ZONE)
	ENDIF
	
	CLEAR_BIT(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE_TEMP)
	
	SET_BIT(iEntityFirstStaggeredLoopComplete, ci_ZONES_FIRST_STAGGERED_LOOP_COMPLETE)
ENDPROC

PROC PROCESS_ZONE_STAGGERED_CLIENT(INT iZone)
	
	IF NOT DOES_ZONE_REQUIRE_CONTINUOUS_PROCESSING(iZone)
		EXIT
	ENDIF
	
	INT iObj
	OBJECT_INDEX tempObj
	
	IF NOT DOES_ZONE_REQUIRE_EVERY_FRAME_TRIGGER_CHECKS(iZone)
		IF SHOULD_THIS_ZONE_BE_TRIGGERED(iZone)
			SET_ZONE_IS_TRIGGERING(iZone)
		ELSE
			SET_ZONE_IS_NOT_TRIGGERING(iZone)
		ENDIF
	ENDIF
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType
		
		CASE ciFMMC_ZONE_TYPE__MENU_CLEAR
			IF IS_ZONE_TRIGGERING(iZone)
				IF NOT IS_BIT_SET(iLocalGearZoneNoneSet, iZone)
					SET_BIT(iLocalGearZoneNoneSet, iZone)
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT, Zone ", iZone, ", ciFMMC_ZONE_TYPE__MENU_CLEAR set ")
					SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_MAKE_PI_GEAR_START_AT_NONE)
				ENDIF
			ENDIF
		BREAK
				
		CASE ciFMMC_ZONE_TYPE__FORCE_OBJECT
			SET_FORCE_OBJECT_THIS_FRAME(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], GET_PLACED_ZONE_RADIUS(iZone))
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT, Zone ", iZone, ", ciFMMC_ZONE_TYPE__FORCE_OBJECT set ")
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__WATER_DAMPENING
			
			IF IS_ZONE_TRIGGERING(iZone)
				IF NOT IS_BIT_SET(iWaveDampingZoneBitset, iZone)
					IF GET_DEEP_OCEAN_SCALER() != g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue2
						PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT, Zone ", iZone, ", Wave Amounts - Calling SET_DEEP_OCEAN_SCALER w value ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue2)
						SET_DEEP_OCEAN_SCALER(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue2)
					ENDIF
					DISABLE_IN_WATER_PTFX(TRUE)
					SET_BIT(iWaveDampingZoneBitset, iZone)
				ENDIF
			ELSE
				IF IS_BIT_SET(iWaveDampingZoneBitset, iZone)
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT, Zone ", iZone, ", Wave Amounts - No longer in this zone")
					CLEAR_BIT(iWaveDampingZoneBitset, iZone)
					
					IF iWaveDampingZoneBitset = 0
						DISABLE_IN_WATER_PTFX(FALSE)
					ENDIF
					
					IF iWaveDampingZoneBitset != 0
						IF GET_DEEP_OCEAN_SCALER() = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue2 //If this one is the zone running right now
							SELECT_NEXT_WAVE_DAMPING_SCALER()
						ENDIF
					ELSE
						IF GET_DEEP_OCEAN_SCALER() != 1.0
							PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT, Zone ", iZone, ", Wave Amounts - Calling RESET_DEEP_OCEAN_SCALER")
							RESET_DEEP_OCEAN_SCALER()
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__BLOCK_WANTED_LEVEL
			IF IS_ZONE_TRIGGERING(iZone)
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_BLOCK_WNTD_CLEARS_WNTD)
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT - Clear Existing Wanted Level is set. for iZone: ", iZone)
					IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_AND_CLEAR_WANTED_LEVEL_ZONE)
						
						IF GET_PLAYER_WANTED_LEVEL(LocalPlayer) != 0
							g_bMissionRemovedWanted = TRUE // Don't want to get XP for losing a wanted level here, the player didn't actually do anything
						ENDIF
						
						SET_PLAYER_WANTED_LEVEL(LocalPlayer, 0)
						SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
						MC_playerBD[iLocalPart].iWanted = 0
						
						SET_FAKE_WANTED_LEVEL(0)
						MC_playerBD[iLocalPart].iFakeWanted = 0
						
						PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT - Entered a block wanted level zone, clear wanted level and set max wanted level to 0 - current max = ", gET_MAX_WANTED_LEVEL())
						SET_MAX_WANTED_LEVEL(0)
						
						IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_SCRIPT_FAKE_WANTED)
							SET_DISPATCH_SERVICES(TRUE, FALSE) // Make sure these are ready to go once players leave the area
						ENDIF
						
						SET_BIT(iLocalBoolCheck13, LBOOL13_IN_BLOCK_AND_CLEAR_WANTED_LEVEL_ZONE)
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT - Clear Existing Wanted Level is NOT set. for iZone: ", iZone)
					IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE)
						PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT - Entered a block wanted level zone, set max wanted level to player current wanted (", gET_PLAYER_WANTED_LEVEL(LocalPlayer),") - current max = ", gET_MAX_WANTED_LEVEL())
						SET_MAX_WANTED_LEVEL(GET_PLAYER_WANTED_LEVEL(LocalPlayer))
						SET_BIT(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE)
					ENDIF
				ENDIF
				
				SET_BIT(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE_TEMP)
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__BLIP_PLAYER
			IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_BLIP_FORCED_ON_BY_ZONE)
			AND MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
				IF IS_ZONE_TRIGGERING(iZone)
					SET_BIT(iLocalBoolCheck23, LBOOL23_BLIP_FORCED_ON_BY_ZONE)
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "GUARD_BP")
					REFRESH_PLAYER_BLIPS()
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT Player in blip player zone")
				ENDIF
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__OBJECT_RESPAWN
			ASSERTLN("[FMMC2020] ciFMMC_ZONE_TYPE__OBJECT_RESPAWN needs to be optimised!")
			FOR iObj = 0 TO MC_serverBD.iNumObjCreated - 1
				IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iObj))
				AND MC_serverBD.iObjCarrier[iObj] = -1
					tempObj = NET_TO_OBJ(GET_OBJECT_NET_ID(iObj))
					
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
						IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
							PRINTLN("[JS] PROCESS_ZONE_STAGGERED_CLIENT - KEEP_OBJECT_IN_ZONE - called Obj - ", iObj, " Zone - ", iZone)
							KEEP_OBJECT_IN_ZONE(tempObj, iZone)
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__OBJECT_RESPAWN_WITHIN
			ASSERTLN("[FMMC2020] ciFMMC_ZONE_TYPE__OBJECT_RESPAWN_WITHIN needs to be optimised!")
			FOR iObj = 0 TO MC_serverBD.iNumObjCreated - 1
				IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iObj))
				AND MC_serverBD.iObjCarrier[iObj] = -1
					tempObj = NET_TO_OBJ(GET_OBJECT_NET_ID(iObj))
					
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
						IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
							PRINTLN("[JS] PROCESS_ZONE_STAGGERED_CLIENT - KEEP_OBJECT_OUT_OF_ZONE - called Obj - ", iObj, " Zone - ", iZone)
							KEEP_OBJECT_OUT_OF_ZONE(tempObj, iZone, iObj)
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__AUTO_PARACHUTE
		
			IF IS_BIT_SET(iLocalBoolCheck19, LBOOL19_AUTO_PARACHUTE_ZONE_ACTIVATED)
				IF NOT IS_ENTITY_IN_AIR(LocalPlayerPed)
					CLEAR_BIT(iLocalBoolCheck19, LBOOL19_AUTO_PARACHUTE_ZONE_ACTIVATED)
					PRINTLN("[RCC MISSION][AutoParachute] PROCESS_ZONE_STAGGERED_CLIENT | Clearing LBOOL19_AUTO_PARACHUTE_ZONE_ACTIVATED")
				ENDIF
			ELSE					
				IF IS_ZONE_TRIGGERING(iZone)
					PRINTLN("[RCC MISSION][AutoParachute_Spam] PROCESS_ZONE_STAGGERED_CLIENT PARACHUTE ZONE BEING PROCESSD")
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue = cfFMMC_PARACHUTEZONE_VehiclesOnly
					OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue = cfFMMC_PARACHUTEZONE_Both
						IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							VEHICLE_INDEX vehIndex
							vehIndex = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
							IF IS_VEHICLE_DRIVEABLE(vehIndex)
								IF GET_VEHICLE_HAS_PARACHUTE(vehIndex)
									IF GET_VEHICLE_CAN_DEPLOY_PARACHUTE(vehIndex)
										VEHICLE_START_PARACHUTING(vehIndex)
										PRINTLN("[RCC MISSION][AutoParachute] PROCESS_ZONE_STAGGERED_CLIENT PARACHUTE ACTIVATE")
										SET_BIT(iLocalBoolCheck19, LBOOL19_AUTO_PARACHUTE_ZONE_ACTIVATED)
									ENDIF
								ENDIF
								IF GET_ENTITY_MODEL(vehIndex) = OPPRESSOR
									SET_GLIDER_ACTIVE(vehIndex, TRUE)
									PRINTLN("[RCC MISSION][AutoParachute] PROCESS_ZONE_STAGGERED_CLIENT OPPRESSOR GLIDER ACTIVATE")
									SET_BIT(iLocalBoolCheck19, LBOOL19_AUTO_PARACHUTE_ZONE_ACTIVATED)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue = cfFMMC_PARACHUTEZONE_PedsOnly
					OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue = cfFMMC_PARACHUTEZONE_Both
						
						IF IS_PED_IN_PARACHUTE_FREE_FALL(LocalPlayerPed)
							// Release parachute
							FORCE_PED_TO_OPEN_PARACHUTE(LocalPlayerPed)
							PRINTLN("[RCC MISSION][AutoParachute] PROCESS_ZONE_STAGGERED_CLIENT | Forcing use of parachute now!")
							SET_BIT(iLocalBoolCheck19, LBOOL19_AUTO_PARACHUTE_ZONE_ACTIVATED)
						ENDIF
					ENDIF
				ENDIF
			ENDIF

		BREAK
		
		CASE ciFMMC_ZONE_TYPE__ALERT_PEDS
			
			PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT ALERT PED - Hitting Alert Peds Zone")
			IF IS_ZONE_TRIGGERING(iZone)
				PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT ALERT PED - Player in Alert Ped zone")
				IF NOT IS_BIT_SET(iAlertPedsZoneBitSet, iZone)
				AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					SET_BIT(iAlertPedsZoneBitSet, iZone)
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT ALERT PED - Setting bit for zone: ", iZone)
				ENDIF
			ELSE
				IF IS_BIT_SET(iAlertPedsZoneBitSet, iZone)
					CLEAR_BIT(iAlertPedsZoneBitSet, iZone)
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT ALERT PED - Clearing bit for zone: ", iZone)
				ENDIF
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__PLAYER_TRIGGERED_EXPLOSION_DANGER_AREA
			IF IS_ZONE_TRIGGERING(iZone)
				IF NOT IS_BIT_SET(MC_playerBD_1[iLocalPart].iExplosionDangerZoneInsideBS, iZone)
					SET_BIT(MC_playerBD_1[iLocalPart].iExplosionDangerZoneInsideBS, iZone)
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] ciFMMC_ZONE_TYPE__PLAYER_TRIGGERED_EXPLOSION_DANGER_AREA - Setting iExplosionDangerZoneInsideBS for zone: ", iZone)
				ENDIF
			ELSE
				IF IS_BIT_SET(MC_playerBD_1[iLocalPart].iExplosionDangerZoneInsideBS, iZone)
					CLEAR_BIT(MC_playerBD_1[iLocalPart].iExplosionDangerZoneInsideBS, iZone)
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] ciFMMC_ZONE_TYPE__PLAYER_TRIGGERED_EXPLOSION_DANGER_AREA - Clearing iExplosionDangerZoneInsideBS for zone: ", iZone)
				ENDIF
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__REFILL_SPECIAL_ABILITY
			IF IS_ZONE_TRIGGERING(iZone)
				IF NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_SPECIAL_ABILITY_REFILLED)
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						VEHICLE_INDEX viPedVeh
						viPedVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
						IF GET_ENTITY_MODEL(viPedVeh) = OPPRESSOR
							SET_ROCKET_BOOST_FILL(viPedVeh, 1.0)
							SET_BIT(iLocalBoolCheck24, LBOOL24_SPECIAL_ABILITY_REFILLED)
							PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT ciFMMC_ZONE_TYPE__REFILL_SPECIAL_ABILITY - Setting bit for zone: ", iZone)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_SPECIAL_ABILITY_REFILLED)
					CLEAR_BIT(iLocalBoolCheck24, LBOOL24_SPECIAL_ABILITY_REFILLED)
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT ciFMMC_ZONE_TYPE__REFILL_SPECIAL_ABILITY - Clearing bit for zone: ", iZone)
				ENDIF
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__RETURN_OBJECT
			ASSERTLN("[FMMC2020] ciFMMC_ZONE_TYPE__RETURN_OBJECT needs to be optimised!")
			FOR iObj = 0 TO MC_serverBD.iNumObjCreated - 1
				IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iObj))
				AND MC_serverBD.iObjCarrier[iObj] = -1
					tempObj = NET_TO_OBJ(GET_OBJECT_NET_ID(iObj))
					
					IF DOES_ENTITY_EXIST(tempObj)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
						AND NOT IS_ENTITY_ATTACHED(tempObj)
						AND IS_ENTITY_IN_FMMC_ZONE(tempObj, iZone, TRUE)
							PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT - Object ", iObj, " is in zone: ", iZone, " Moving back to spawn pos.")
							SET_ENTITY_COORDS(tempObj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos)
							SET_ENTITY_ROTATION(tempObj, <<0.0, 0.0, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].fHead>>)
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__HIDE_OBJECTS_SEARCH_AREA
			
			ASSERTLN("[FMMC2020] ciFMMC_ZONE_TYPE__HIDE_OBJECTS_SEARCH_AREA needs to be completely refactored!")
			
			IF IS_ZONE_TRIGGERING(iZone)
				INT iObject
				FOR iObject = 0 TO MC_serverBD.iNumObjCreated - 1
					IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iObject))
						tempObj = NET_TO_OBJ(GET_OBJECT_NET_ID(iObject))
						
						IF DOES_ENTITY_EXIST(tempObj)
							IF NOT IS_ENTITY_ATTACHED(tempObj)
							AND IS_ENTITY_IN_FMMC_ZONE(tempObj, iZone, TRUE)
								IF NOT IS_BIT_SET(iObjectHiddenBS[iZone], iObject)
									PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT - Object ", iObject, " is in zone: ", iZone, " Hiding object")
									SET_ENTITY_ALPHA(tempObj, 0, FALSE)
									SET_BIT(iObjectHiddenBS[iZone], iObject)
								ENDIF
								IF DOES_BLIP_EXIST(biObjBlip[iObject])
								AND GET_BLIP_ALPHA(biObjBlip[iObject]) > 0
									iObjectBlipAlpha[iObject] = GET_BLIP_ALPHA(biObjBlip[iObject])
									SET_BLIP_ALPHA(biObjBlip[iObject], 0)
								ENDIF
							ELSE
								IF IS_BIT_SET(iObjectHiddenBS[iZone], iObject)
									PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT - Object ", iObject, " is in zone: ", iZone, " (1) Clearing Hidden Flag for object")
									RESET_ENTITY_ALPHA(tempObj)
									IF DOES_BLIP_EXIST(biObjBlip[iObject])
										SET_BLIP_ALPHA(biObjBlip[iObject], iObjectBlipAlpha[iObject])
									ENDIF
									CLEAR_BIT(iObjectHiddenBS[iZone], iObject)
								ENDIF
							ENDIF
						ELSE
							IF IS_BIT_SET(iObjectHiddenBS[iZone], iObject)
								PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT - Object ", iObject, " is in zone: ", iZone, " (2) Clearing Hidden Flag for object")
								CLEAR_BIT(iObjectHiddenBS[iZone], iObject)
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(iObjectHiddenBS[iZone], iObject)
							PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT - Object ", iObject, " is in zone: ", iZone, " (3) Clearing Hidden Flag for object")
							CLEAR_BIT(iObjectHiddenBS[iZone], iObject)
						ENDIF
					ENDIF
				ENDFOR
			ELSE
				IF iObjectHiddenBS[iZone] != 0
					FOR iObj = 0 TO MC_serverBD.iNumObjCreated - 1
						IF IS_BIT_SET(iObjectHiddenBS[iZone], iObj)
							IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iObj))
								tempObj = NET_TO_OBJ(GET_OBJECT_NET_ID(iObj))
								
								IF DOES_ENTITY_EXIST(tempObj)
									IF NOT IS_ENTITY_ATTACHED(tempObj)
									AND IS_ENTITY_IN_FMMC_ZONE(tempObj, iZone, TRUE)
										IF IS_BIT_SET(iObjectHiddenBS[iZone], iObj)
											PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT - Object ", iObj, " is in zone: ", iZone, " (2) un-hiding object")
											RESET_ENTITY_ALPHA(tempObj)
											IF DOES_BLIP_EXIST(biObjBlip[iObj])
												SET_BLIP_ALPHA(biObjBlip[iObj], iObjectBlipAlpha[iObj])
											ENDIF
											CLEAR_BIT(iObjectHiddenBS[iZone], iObj)											
										ENDIF
									ENDIF
								ELSE
									IF IS_BIT_SET(iObjectHiddenBS[iZone], iObj)
										PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT - Object ", iObj, " is in zone: ", iZone, " (5) Clearing Hidden Flag for object")
										CLEAR_BIT(iObjectHiddenBS[iZone], iObj)
									ENDIF
								ENDIF
							ELSE
								IF IS_BIT_SET(iObjectHiddenBS[iZone], iObj)
									PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT - Object ", iObj, " is in zone: ", iZone, " (6) Clearing Hidden Flag for object")
									CLEAR_BIT(iObjectHiddenBS[iZone], iObj)
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		BREAK

		CASE ciFMMC_ZONE_TYPE__AGGRO_PEDS_IN_THIS_ZONE
			IF IS_ZONE_TRIGGERING(iZone)
				PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT - LocalPlayer Activated ciFMMC_ZONE_TYPE__AGGRO_PEDS_IN_THIS_ZONE iZone: ", iZone)
				INT iEnemyPed
				FOR iEnemyPed = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1
					IF NOT FMMC_IS_LONG_BIT_SET(iPedZoneAggroed, iEnemyPed)
					AND NOT FMMC_IS_LONG_BIT_SET(iPedAggroedWithScriptTrigger, iEnemyPed)
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iEnemyPed])
							PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_STAGGERED_CLIENT - iEnemyPed: ", iEnemyPed, " Exists.")
							
							PED_INDEX pedIndex
							pedIndex = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iEnemyPed])
							IF NOT IS_PED_INJURED(pedIndex)
								IF IS_ENTITY_IN_FMMC_ZONE(pedIndex, iZone, FALSE, TRUE)
									PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "][SpookAggro] PROCESS_ZONE_STAGGERED_CLIENT - iEnemyPed: ", iEnemyPed, " Inside Zone. Aggroing!")									
									FMMC_SET_LONG_BIT(iPedZoneAggroed, iEnemyPed)
									FMMC_SET_LONG_BIT(iPedAggroedWithScriptTrigger, iEnemyPed)
									BROADCAST_FMMC_AGGROED_SCRIPT_FORCED_PED(iEnemyPed)
									IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEnemyPed].iPedBitsetFour ,ciPed_BSFour_StayInTasks_On_Aggro)
										FMMC_SET_LONG_BIT(iPedLocalCancelTasksBitset, iEnemyPed)
										BROADCAST_FMMC_PED_CANCEL_TASKS(iEnemyPed)
									ENDIF
									BROADCAST_FMMC_PED_AGGROED(iEnemyPed, DEFAULT, TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__BOUNDS
			PROCESS_BOUNDS_ZONE_STAGGERED_CLIENT(iZone)
		BREAK
		
		CASE ciFMMC_ZONE_TYPE__SOUND_ZONE
			IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iSoundType != ciFMMC_ZONE_SOUND_OFF
			AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneResetTimerLength != 0
			AND IS_ZONE_TRIGGERING(iZone)
				IF NOT HAS_NET_TIMER_STARTED(stZoneResetTimer)
					REINIT_NET_TIMER(stZoneResetTimer)
					INT iRandom
					iRandom = GET_RANDOM_INT_IN_RANGE(1, 1001)
					IF iRandom <= g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneChance*10
						VECTOR vPlayerPos
						vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
						VECTOR vSoundCoord
						FLOAT fRange
						fRange = 60
						vSoundCoord = GET_RANDOM_POINT_IN_DISC(vPlayerPos, fRange, 2.0)
						BROADCAST_FMMC_SOUND_ZONE_SOUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iSoundType, vSoundCoord, ROUND(fRange))
					ENDIF
				ELSE
					IF HAS_NET_TIMER_EXPIRED_READ_ONLY(stZoneResetTimer, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneResetTimerLength)
						RESET_NET_TIMER(stZoneResetTimer)
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_AGGRO_WHEN_ENTERED)
		IF NOT HAS_LOCAL_PLAYER_TEAM_TRIGGERED_AGGRO(FALSE, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAggroIndexBS_Entity_Zone)
			IF IS_ZONE_TRIGGERING(iZone)
				IF GET_FRAME_COUNT() % 3 = 0
					BROADCAST_FMMC_TRIGGER_AGGRO_FOR_TEAM(GET_LOCAL_PLAYER_TEAM(FALSE), g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAggroIndexBS_Entity_Zone)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	// Generic "In Zone" Functionality.
	IF DOES_ZONE_EXIST(iZone)
		IF IS_ZONE_TRIGGERING(iZone)
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_TRIGGER_ZONE_REMOTELY)
			AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkType = ciENTITY_TYPE_ZONES
			AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkIndex > -1
				IF NOT IS_BIT_SET(iZoneBS_TriggeredByLinkedZone, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkIndex)
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_EVERY_FRAME_STAGGERED | Zone Entity Link Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkIndex, " Is now being activated remotely FROM iZone: ", iZone)
					SET_BIT(iZoneBS_TriggeredByLinkedZone, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkIndex)
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_TRIGGER_ZONE_REMOTELY)
			AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkType = ciENTITY_TYPE_ZONES
			AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkIndex > -1
				IF IS_BIT_SET(iZoneBS_TriggeredByLinkedZone, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkIndex)
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_EVERY_FRAME_STAGGERED | Zone Entity Link Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkIndex, " Is now being de-activated remotely FROM iZone: ", iZone)
					CLEAR_BIT(iZoneBS_TriggeredByLinkedZone, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkIndex)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF CHECK_SHOULD_CREATE_ZONE_STAGGERED(iZone)
			IF SHOULD_CREATE_ZONE_STAGGERED(iZone)
				IF SHOULD_CREATE_ZONE(iZone, MC_PlayerBD[iLocalPart].iTeam, TRUE, TRUE)
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] PROCESS_ZONE_EVERY_FRAME_STAGGERED | Should Create Zone = True. Setting iZonesWaitingForCreationBS.")
					SET_BIT(iZonesWaitingForCreationBS, iZone)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_ZONE_STAGGERED_SERVER(INT iZone)
	
	UNUSED_PARAMETER(iZone)
	
	IF !bIsLocalPlayerHost
		EXIT
	ENDIF

ENDPROC
