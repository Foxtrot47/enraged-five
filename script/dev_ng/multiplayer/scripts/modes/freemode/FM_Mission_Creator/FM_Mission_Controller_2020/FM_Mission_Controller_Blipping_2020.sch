// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Blipping ----------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Contains functions for creating and processing generic blip functionality
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Assets_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: GENERAL BLIPS ---------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to xxxxxxxxxxxxxxxx ---------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC HUD_COLOURS GET_MISSING_HUD_COLOUR_FROM_BLIP_COLOUR(INT iBlipColour)
	SWITCH iBlipColour
		CASE BLIP_COLOUR_RED 			RETURN HUD_COLOUR_RED
		CASE BLIP_COLOUR_BLUEDARK 		RETURN HUD_COLOUR_BLUEDARK
		CASE BLIP_COLOUR_BLUE 			RETURN HUD_COLOUR_BLUE
		CASE BLIP_COLOUR_GREEN 			RETURN HUD_COLOUR_GREEN
		CASE BLIP_COLOUR_YELLOW 		RETURN HUD_COLOUR_YELLOW
		CASE BLIP_COLOUR_WHITE 			RETURN HUD_COLOUR_WHITE
		CASE BLIP_COLOUR_SPECIAL_BLACK 	RETURN HUD_COLOUR_BLACK
		CASE BLIP_COLOUR_PURPLE 		RETURN HUD_COLOUR_PURPLE
	ENDSWITCH
	
	RETURN HUD_COLOUR_WHITE
ENDFUNC

FUNC HUD_COLOURS MC_GET_HUD_COLOUR_FROM_BLIP_COLOUR(INT iBlipColour)
	HUD_COLOURS retColour = GET_HUD_COLOUR_FROM_BLIP_COLOUR(iBlipColour)
	IF ENUM_TO_INT(retColour) = -1
		retColour = GET_MISSING_HUD_COLOUR_FROM_BLIP_COLOUR(iBlipColour)
	ENDIF
	RETURN retColour
ENDFUNC 

FUNC VECTOR GET_DUMMY_OVERRIDE_CENTER(INT iDummyBlip)
	VECTOR vcenter = <<0,0,0>>
	INT iteam = MC_playerBD[iPartToUse].iteam
	INT iveh = -1
	IF getVehicleBeingDroppedOff != NULL	
		iveh = CALL getVehicleBeingDroppedOff(iteam, iPartToUse)
	ENDIF
	IF iveh != -1
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vDropOffOverride)
			vcenter = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vDropOffOverride
		ELSE
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].vPos)
				vcenter = g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].vPos
			ENDIF
		ENDIF
	ENDIF
	PRINTLN("[QGPS][JR] - DUMMY OVERRIDE CENTER - RETURNING ", vcenter)
	RETURN vcenter
ENDFUNC

PROC SET_BLIP_TEAM_SECONDARY_COLOUR(BLIP_INDEX tempBlip,BOOL bSet,INT iteam)
	
	HUD_COLOURS teamcolour
	INT R,G,B,A
	
	IF bSet
		
		teamcolour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iteam, PlayerToUse)
		
		GET_HUD_COLOUR(teamcolour,R,G,B,A)
		APPLY_SECONDARY_COLOUR_TO_BLIP(tempBlip,R,G,B)
	ELSE
		CLEAR_SECONDARY_COLOUR_FROM_BLIP(tempBlip)
	ENDIF

ENDPROC

PROC REMOVE_ANY_EXISTING_BLIP_FROM_ENTITY(ENTITY_INDEX EntityID)
	IF DOES_ENTITY_EXIST(EntityID)
		BLIP_INDEX TempBlip = GET_BLIP_FROM_ENTITY(EntityID)
		IF DOES_BLIP_EXIST(TempBlip)
			PRINTLN("[RCC MISSION] Removing any existing blip from entity ", NATIVE_TO_INT(EntityID), ". Blip was found and removed.")
			REMOVE_BLIP(TempBlip)
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_MODSHOP_BLIP_SETTINGS()
	IF DOES_BLIP_EXIST(biModshopBlip)
		CLEAR_ALL_BLIP_ROUTES()
		SET_BLIP_COLOUR(biModshopBlip, BLIP_COLOUR_DEFAULT)
		SET_BLIP_AS_SHORT_RANGE(biModshopBlip, TRUE)
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Generic Blip Checks
// ##### Description: Functionality checks for all blip types. Range, height difference etc
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_PLAYER_WITHIN_ENTITY_BLIP_RANGE(ENTITY_INDEX eiEntityToCheck, FMMC_BLIP_INFO_STRUCT &sBlipStruct, BOOL bIgnoreZ = FALSE)
	
	IF sBlipStruct.fBlipRange > 0.0
	
		IF IS_ENTITY_ALIVE(eiEntityToCheck)
		AND IS_ENTITY_ALIVE(PlayerPedToUse)
			
			VECTOR vPlayerPos = GET_ENTITY_COORDS(PlayerPedToUse)
			VECTOR vEntityPos = GET_ENTITY_COORDS(eiEntityToCheck)
			
			IF bIgnoreZ
				vPlayerPos.z = vEntityPos.z
			ENDIF
			
			FLOAT fDist = VDIST2(vPlayerPos, vEntityPos)
			IF IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Blip_Out_Of_Range)
				IF fDist <= POW(sBlipStruct.fBlipRange, 2)
					PRINTLN("[Blip] IS_PLAYER_WITHIN_ENTITY_BLIP_RANGE - Entity is in blip range when set to blip out of range: ", sBlipStruct.fBlipRange, " fDist: ", fDist, " Range Pow: ", POW(sBlipStruct.fBlipRange, 2))
					RETURN FALSE
				ENDIF
			ELSE
				IF fDist > POW(sBlipStruct.fBlipRange, 2)
					PRINTLN("[Blip] IS_PLAYER_WITHIN_ENTITY_BLIP_RANGE - Entity is out of blip range: ", sBlipStruct.fBlipRange, " fDist: ", fDist, " Range Pow: ", POW(sBlipStruct.fBlipRange, 2))
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLAYER_WITHIN_COORD_BLIP_RANGE(VECTOR vCoord, FMMC_BLIP_INFO_STRUCT &sBlipStruct, BOOL bIgnoreZ = FALSE)
	
	IF sBlipStruct.fBlipRange > 0.0
		
		IF NOT IS_VECTOR_ZERO(vCoord)
			
			VECTOR vPlayerPos = GET_ENTITY_COORDS(PlayerPedToUse)
			
			IF bIgnoreZ
				vPlayerPos.z = vCoord.z
			ENDIF
			
			FLOAT fDist = VDIST2(vPlayerPos, vCoord)
			IF IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Blip_Out_Of_Range)
				IF fDist <= POW(sBlipStruct.fBlipRange, 2)
					PRINTLN("[Blip] IS_PLAYER_WITHIN_COORD_BLIP_RANGE - Coord is in blip range when set to blip out of range ", fDist ," <= ", POW(sBlipStruct.fBlipRange, 2))
					RETURN FALSE
				ENDIF
			ELSE
				IF fDist > POW(sBlipStruct.fBlipRange, 2)
					PRINTLN("[Blip] IS_PLAYER_WITHIN_COORD_BLIP_RANGE - Coord is out of blip range ", fDist ," <= ", POW(sBlipStruct.fBlipRange, 2))
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_ENTITY_WITHIN_HEIGHT_DIFFERENCE_RANGE_FOR_BLIP(ENTITY_INDEX eiEntityToCheck, FMMC_BLIP_INFO_STRUCT &sBlipStruct)
	
	IF sBlipStruct.fBlipHeightDifference > 0.0
	
		IF IS_ENTITY_ALIVE(eiEntityToCheck)
		AND IS_ENTITY_ALIVE(PlayerPedToUse)
		
			VECTOR vPlayerPos = GET_ENTITY_COORDS(PlayerPedToUse)
			VECTOR vEntPos = GET_ENTITY_COORDS(eiEntityToCheck)
			
			IF NOT IS_VECTOR_ZERO(vPlayerPos)
			AND NOT IS_VECTOR_ZERO(vEntPos)
				FLOAT fZDifference = ABSF(vPlayerPos.z - vEntPos.z)
				IF IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Blip_Out_Of_Range)
					IF fZDifference <= sBlipStruct.fBlipHeightDifference
						PRINTLN("[Blip] IS_ENTITY_WITHIN_HEIGHT_DIFFERENCE_RANGE_FOR_BLIP - Entity is in blip height range, when set to blip out of range")
						RETURN FALSE
					ENDIF
				ELSE
					IF fZDifference > sBlipStruct.fBlipHeightDifference
						PRINTLN("[Blip] IS_ENTITY_WITHIN_HEIGHT_DIFFERENCE_RANGE_FOR_BLIP - Entity is out of blip height range")
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_COORD_WITHIN_HEIGHT_DIFFERENCE_RANGE_FOR_BLIP(VECTOR vCoord, FMMC_BLIP_INFO_STRUCT &sBlipStruct)
	
	IF sBlipStruct.fBlipHeightDifference > 0.0
		
		IF NOT IS_VECTOR_ZERO(vCoord)
		
			IF bPedToUseOk
				VECTOR vPlayerPos = GET_ENTITY_COORDS(PlayerPedToUse)
				
				IF NOT IS_VECTOR_ZERO(vPlayerPos)
					FLOAT fZDifference = ABSF(vPlayerPos.z - vCoord.z)
					IF IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Blip_Out_Of_Range)
						IF fZDifference <= sBlipStruct.fBlipHeightDifference
							PRINTLN("[Blip] IS_COORD_WITHIN_HEIGHT_DIFFERENCE_RANGE_FOR_BLIP - Coord is in blip height range, when set to blip out of range ", fZDifference ," <= ", sBlipStruct.fBlipHeightDifference)
							RETURN FALSE
						ENDIF
					ELSE
						IF fZDifference > sBlipStruct.fBlipHeightDifference
							PRINTLN("[Blip] IS_COORD_WITHIN_HEIGHT_DIFFERENCE_RANGE_FOR_BLIP - Coord is out of blip height range ", fZDifference ," <= ", sBlipStruct.fBlipHeightDifference)
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL WAS_PLAYER_PREVIOUSLY_IN_RANGE_OF_BLIP(BLIP_RUNTIME_VARS_STRUCT &sBlipRuntimeVars)
	RETURN IS_BIT_SET(sBlipRuntimeVars.iBlipBS, ciBLIP_RUNTIME_Player_Previously_In_Blip_Range)
ENDFUNC

PROC PROCESS_BLIP_PLAYER_IN_RANGE(FMMC_BLIP_INFO_STRUCT &sBlipStruct, BLIP_RUNTIME_VARS_STRUCT &sBlipRuntimeVars)

	IF NOT IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Dont_Unblip_When_Out_Of_Range)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(sBlipRuntimeVars.iBlipBS, ciBLIP_RUNTIME_Player_Previously_In_Blip_Range)
		SET_BIT(sBlipRuntimeVars.iBlipBS, ciBLIP_RUNTIME_Player_Previously_In_Blip_Range)
	ENDIF
	
ENDPROC

FUNC BOOL IS_BLIP_SET_AS_HIDDEN(FMMC_BLIP_INFO_STRUCT &sBlipStruct)
	RETURN IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Hide_Blip)
ENDFUNC

FUNC BOOL IS_BLIP_SET_AS_GPS_ONLY(FMMC_BLIP_INFO_STRUCT &sBlipStruct)
	RETURN IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Blip_As_GPS_Only)
ENDFUNC

FUNC BOOL SHOULD_HINT_CAM_WORK_FOR_OBJECTIVE_ENTITY_WITHOUT_BLIP(FMMC_BLIP_INFO_STRUCT &sBlipStruct)
	RETURN IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Allow_Objective_Hint_Cam_Without_Blip)
ENDFUNC

FUNC BOOL HAS_SHOW_BLIP_PREREQ_BEEN_MET(FMMC_BLIP_INFO_STRUCT &sBlipStruct)
	
	IF sBlipStruct.iShowBlipPreReqRequired != ciPREREQ_None
		IF NOT IS_PREREQUISITE_COMPLETED(sBlipStruct.iShowBlipPreReqRequired)
			PRINTLN("[RCC MISSION] HAS_SHOW_BLIP_PREREQ_BEEN_MET - Player hasn't completed required prerequisite ", sBlipStruct.iShowBlipPreReqRequired)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_REMOVE_BLIP_PREREQ_BEEN_MET(FMMC_BLIP_INFO_STRUCT &sBlipStruct)
	
	IF sBlipStruct.iRemoveBlipPreReqRequired != ciPREREQ_None
		IF IS_PREREQUISITE_COMPLETED(sBlipStruct.iRemoveBlipPreReqRequired)
			PRINTLN("[RCC MISSION] HAS_REMOVE_BLIP_PREREQ_BEEN_MET - Player hasn't completed required prerequisite ", sBlipStruct.iRemoveBlipPreReqRequired)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ZONE_TRIGGERING_FOR_BLIP(INT iBlipBitSetToCheck)
	
	IF (iZoneIsTriggeredBS & iBlipBitSetToCheck) != 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DOES_BLIP_MEET_ZONE_REQUIREMENT(FMMC_BLIP_INFO_STRUCT &sBlipStruct)
	
	IF sBlipStruct.iBlipShowZonesBS = 0
	AND sBlipStruct.iBlipHideZonesBS = 0
		RETURN TRUE
	ENDIF
	
	IF sBlipStruct.iBlipShowZonesBS != 0
		IF NOT IS_ZONE_TRIGGERING_FOR_BLIP(sBlipStruct.iBlipShowZonesBS)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF sBlipStruct.iBlipHideZonesBS != 0
		IF IS_ZONE_TRIGGERING_FOR_BLIP(sBlipStruct.iBlipHideZonesBS)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_BLIP_ENTITY_IN_SAME_INTERIOR(FMMC_BLIP_INFO_STRUCT &sBlipStruct, ENTITY_INDEX eiEntity)
	
	IF NOT IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Blip_Only_In_Same_Interior)
		RETURN TRUE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(eiEntity)
		RETURN TRUE
	ENDIF
	
	IF LocalPlayerCurrentInterior != GET_INTERIOR_FROM_ENTITY(eiEntity)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_BLIP_COORD_IN_SAME_INTERIOR(FMMC_BLIP_INFO_STRUCT &sBlipStruct, VECTOR vCoord)
	IF NOT IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Blip_Only_In_Same_Interior)
		RETURN TRUE
	ENDIF
	
	IF IS_VECTOR_ZERO(vCoord)
		RETURN TRUE
	ENDIF
	
	IF GET_INTERIOR_AT_COORDS(vLocalPlayerPosition) != GET_INTERIOR_AT_COORDS(vCoord)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_ENTITY_HAVE_BLIP_OVERRIDES(FMMC_BLIP_INFO_STRUCT &sBlipStruct)
	INT iTeam = GET_LOCAL_PLAYER_TEAM(TRUE)
	
	IF NOT IS_TEAM_INDEX_VALID(iTeam)
		RETURN FALSE
	ENDIF
	
	IF sBlipStruct.sTeamBlip[iTeam].iBlipOverrideBitSet != 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC FIND_FIRST_AND_LAST_SET_BITS_FOR_BLIP_OVERRIDE(INT iOverrideBS, INT &iFirstSet, INT &iLastSet)
	INT iBitToCheck
	INT iFirst = -1
	INT iLast = -1
	FOR iBitToCheck = 0 TO FMMC_MAX_RULES-1
		IF IS_BIT_SET(iOverrideBS, iBitToCheck)
			IF iFirst = -1
				iFirst = iBitToCheck
			ENDIF
			iLast = iBitToCheck
		ENDIF
		
		CLEAR_BIT(iOverrideBS, iBitToCheck)
		IF iOverrideBS = 0
			BREAKLOOP
		ENDIF
	ENDFOR
	
	iFirstSet = iFirst
	iLastSet = iLast
	 
	PRINTLN("FIND_FIRST_AND_LAST_SET_BITS_FOR_BLIP_OVERRIDE - iFirstSet: ", iFirstSet, " iLastSet: ", iLastSet)
ENDPROC

FUNC BOOL SHOULD_ENTITY_BE_BLIPPED_THROUGH_OVERRIDES(FMMC_BLIP_INFO_STRUCT &sBlipStruct)
	
	INT iTeam = GET_LOCAL_PLAYER_TEAM(TRUE)
	
	IF NOT IS_TEAM_INDEX_VALID(iTeam)
		RETURN FALSE
	ENDIF
	
	IF sBlipStruct.sTeamBlip[iTeam].iBlipOverrideBitSet = 0
		RETURN FALSE
	ENDIF
		
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF NOT IS_RULE_INDEX_VALID(iRule)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(sBlipStruct.sTeamBlip[iTeam].iBlipOverrideBitSet, iRule)
		RETURN FALSE
	ENDIF
	
	INT iFirstOverrideRule = -1
	INT iLastOverrideRule = -1
	
	IF IS_BIT_SET(sBlipStruct.sTeamBlip[iTeam].iTeamBlipBitset, ciTEAM_BLIP_INFO_Start_On_MidPoint)
	OR IS_BIT_SET(sBlipStruct.sTeamBlip[iTeam].iTeamBlipBitset, ciTEAM_BLIP_INFO_End_On_MidPoint)
		FIND_FIRST_AND_LAST_SET_BITS_FOR_BLIP_OVERRIDE(sBlipStruct.sTeamBlip[iTeam].iBlipOverrideBitSet, iFirstOverrideRule, iLastOverrideRule)
	ENDIF
		
	IF IS_BIT_SET(sBlipStruct.sTeamBlip[iTeam].iTeamBlipBitset, ciTEAM_BLIP_INFO_Start_On_MidPoint)
	AND iFirstOverrideRule != -1
	AND NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iFirstOverrideRule)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sBlipStruct.sTeamBlip[iTeam].iTeamBlipBitset, ciTEAM_BLIP_INFO_End_On_MidPoint)
	AND iLastOverrideRule != -1
	AND IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iLastOverrideRule)
		RETURN FALSE
	ENDIF
		
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Overrides all other blip script checks and allows the entity to be blipped.
///    
FUNC BOOL SHOULD_ENTITY_BLIP_BE_FORCED_ON(FMMC_BLIP_INFO_STRUCT &sBlipStruct, ENTITY_INDEX eiEntity = NULL)
	
	IF IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Always_Blip_On_Pause_Map)
		
		IF IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Blip_On_Pause_Map_Check_Interior)
		AND DOES_ENTITY_EXIST(eiEntity)
			IF NOT IS_ENTITY_IN_SAME_INTERIOR_AS_LOCAL_PLAYER(eiEntity)
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF IS_PAUSE_MENU_ACTIVE()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ENTITY_BLIP_BE_FORCED_OFF(FMMC_BLIP_INFO_STRUCT &sBlipStruct)
	
	UNUSED_PARAMETER(sBlipStruct)
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Generic check for all entity types looking only at the blip struct to see if it should be blipped or not.
/// PARAMS:
///    sBlipStruct - Blip data for given entity
/// RETURNS:
///    
FUNC BOOL SHOULD_ENTITY_BE_BLIPPED(FMMC_BLIP_INFO_STRUCT &sBlipStruct, BLIP_RUNTIME_VARS_STRUCT &sBlipRuntimeVars, ENTITY_INDEX eiBlipEntity, BOOL bCheckRangesForCreation = TRUE, BOOL bIgnoreOverrides = FALSE)
		
	IF NOT HAS_SHOW_BLIP_PREREQ_BEEN_MET(sBlipStruct)
		RETURN FALSE
	ENDIF
	
	IF HAS_REMOVE_BLIP_PREREQ_BEEN_MET(sBlipStruct)
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_BLIP_MEET_ZONE_REQUIREMENT(sBlipStruct)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BLIP_ENTITY_IN_SAME_INTERIOR(sBlipStruct, eiBlipEntity)
		RETURN FALSE
	ENDIF
	
	IF bCheckRangesForCreation
	AND NOT WAS_PLAYER_PREVIOUSLY_IN_RANGE_OF_BLIP(sBlipRuntimeVars)
	
		IF NOT IS_PLAYER_WITHIN_ENTITY_BLIP_RANGE(eiBlipEntity, sBlipStruct)
			RETURN FALSE
		ENDIF
		
		IF NOT IS_ENTITY_WITHIN_HEIGHT_DIFFERENCE_RANGE_FOR_BLIP(eiBlipEntity, sBlipStruct)
			RETURN FALSE
		ENDIF
				
	ENDIF
		
	IF DOES_ENTITY_HAVE_BLIP_OVERRIDES(sBlipStruct)
	AND NOT bIgnoreOverrides
		RETURN SHOULD_ENTITY_BE_BLIPPED_THROUGH_OVERRIDES(sBlipStruct)
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL IS_BLIP_SPRITE_OVERRIDE_SET(FMMC_BLIP_INFO_STRUCT &sBlipStruct)
	RETURN sBlipStruct.iBlipSpriteOverride > 0
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Blip Sprites --------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to blip sprites ---------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//FMMC_2020 REFACTOR - Make one blip sprite function
FUNC BLIP_SPRITE GET_BLIP_SPRITE_FOR_LOCATE(INT iBlipSprite, BOOL bNewData)
	
	IF bNewData
		RETURN INT_TO_ENUM(BLIP_SPRITE, iBlipSprite)
	ENDIF
	
	SWITCH iBlipSprite
		CASE LOCATE_BLIP_VOLTIC 		RETURN RADAR_TRACE_EX_VECH_6
		CASE LOCATE_BLIP_RUINER			RETURN RADAR_TRACE_EX_VECH_3
		CASE LOCATE_BLIP_DUNE4			RETURN RADAR_TRACE_EX_VECH_4
		CASE LOCATE_BLIP_PHANTOM		RETURN RADAR_TRACE_EX_VECH_1
		CASE LOCATE_BLIP_TECHNICAL		RETURN RADAR_TRACE_EX_VECH_7
		CASE LOCATE_BLIP_BOXVILLE		RETURN RADAR_TRACE_EX_VECH_2
		CASE LOCATE_BLIP_WASTELANDER	RETURN RADAR_TRACE_EX_VECH_5
		CASE LOCATE_BLIP_BLAZER			RETURN RADAR_TRACE_QUAD
		CASE LOCATE_BLIP_NUM_1			RETURN RADAR_TRACE_CAPTURE_1
		CASE LOCATE_BLIP_NUM_2			RETURN RADAR_TRACE_CAPTURE_2
		CASE LOCATE_BLIP_NUM_3			RETURN RADAR_TRACE_CAPTURE_3
		CASE LOCATE_BLIP_NUM_4			RETURN RADAR_TRACE_CAPTURE_4
		CASE LOCATE_BLIP_NUM_5			RETURN RADAR_TRACE_CAPTURE_5
		CASE LOCATE_BLIP_TAR_A			RETURN RADAR_TRACE_TARGET_A
		CASE LOCATE_BLIP_TAR_B			RETURN RADAR_TRACE_TARGET_B
		CASE LOCATE_BLIP_TAR_C			RETURN RADAR_TRACE_TARGET_C
		CASE LOCATE_BLIP_TAR_D			RETURN RADAR_TRACE_TARGET_D
		CASE LOCATE_BLIP_TAR_E			RETURN RADAR_TRACE_TARGET_E
		CASE LOCATE_BLIP_TAR_F			RETURN RADAR_TRACE_TARGET_F
		CASE LOCATE_BLIP_TAR_G			RETURN RADAR_TRACE_TARGET_G
		CASE LOCATE_BLIP_TAR_H			RETURN RADAR_TRACE_TARGET_H
		CASE LOCATE_BLIP_RAPPEL			RETURN RADAR_TRACE_RAPPEL
	ENDSWITCH
	RETURN RADAR_TRACE_OBJECTIVE
ENDFUNC
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Blip Creation
// ##### Description: Creation and initial set up for an entity blip
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_DEFAULT_BLIP_PRIORITY(FMMC_BLIP_TYPE eBlipType, BLIP_INDEX &biBlip)
	SWITCH eBlipType
		CASE FMMC_BLIP_OBJECT
		CASE FMMC_BLIP_VEHICLE
		CASE FMMC_BLIP_PED
		CASE FMMC_BLIP_LOCATION
		CASE FMMC_BLIP_TRAIN
			SET_BLIP_PRIORITY(biBlip, BLIPPRIORITY_HIGHEST)
		BREAK
		
		CASE FMMC_BLIP_INTERACTABLE
			SET_BLIP_PRIORITY(biBlip, BLIPPRIORITY_HIGH)
		BREAK
	ENDSWITCH
ENDPROC

FUNC INT GET_DEFAULT_ENTITY_BLIP_COLOUR(FMMC_BLIP_TYPE eBlipType)
	SWITCH eBlipType
		CASE FMMC_BLIP_OBJECT 		RETURN FMMC_BLIP_COLOUR_GREEN
		CASE FMMC_BLIP_INTERACTABLE RETURN FMMC_BLIP_COLOUR_GREEN
		CASE FMMC_BLIP_VEHICLE		RETURN FMMC_BLIP_COLOUR_DARK_BLUE
		CASE FMMC_BLIP_TRAIN		RETURN FMMC_BLIP_COLOUR_DARK_BLUE
		CASE FMMC_BLIP_PED			RETURN FMMC_BLIP_COLOUR_RED
		CASE FMMC_BLIP_LOCATION		RETURN FMMC_BLIP_COLOUR_YELLOW
		CASE FMMC_BLIP_DUMMY		RETURN FMMC_BLIP_COLOUR_YELLOW
	ENDSWITCH
	
	RETURN FMMC_BLIP_COLOUR_DEFAULT
ENDFUNC

FUNC FLOAT GET_DEFAULT_ENTITY_BLIP_SCALE(FMMC_BLIP_TYPE eBlipType)
	SWITCH eBlipType
		CASE FMMC_BLIP_PICKUP
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciUSE_SMALL_WEP_BLIPS)
				RETURN BLIP_SIZE_NETWORK_PICKUP
			ELSE
				RETURN BLIP_SIZE_NETWORK_PICKUP_LARGE
			ENDIF
		BREAK
		CASE FMMC_BLIP_OBJECT
			RETURN FMMC_BLIP_SCALE_OBJECT
		BREAK
		CASE FMMC_BLIP_INTERACTABLE
			RETURN FMMC_BLIP_SCALE_OBJECT
		BREAK
		CASE FMMC_BLIP_PED
			RETURN BLIP_SIZE_NETWORK_PED
		BREAK
	ENDSWITCH
	
	RETURN 1.0
ENDFUNC

PROC SET_BLIP_CUSTOM_NAME(BLIP_INDEX& biBlip, TEXT_LABEL_63 tl63CustomName)
	IF NOT IS_STRING_NULL_OR_EMPTY(tl63CustomName)
		PRINTLN("[RCC MISSION] SET_BLIP_CUSTOM_NAME | Setting custom blip name to ", tl63CustomName)
		
		BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MCUSTBLIP")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl63CustomName)
		END_TEXT_COMMAND_SET_BLIP_NAME(biBlip)
	ENDIF
ENDPROC

PROC SET_BLIP_NAME_FROM_STRING_LIST(FMMC_BLIP_INFO_STRUCT &sBlipStruct, BLIP_INDEX& biBlip)
	IF sBlipStruct.iBlipNameIndex > -1
		SET_BLIP_CUSTOM_NAME(biBlip, GET_CUSTOM_STRING_LIST_TEXT_LABEL(sBlipStruct.iBlipNameIndex))
	ENDIF
ENDPROC

PROC INITIALISE_CACHED_BLIP_NAME_INDEXES()
	CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][CACHE] Initialising cached blip name indexes to starting values.")

	INT index
	REPEAT FMMC_MAX_PEDS index
		iPedBlipCachedNamePriority[index] = -1
	ENDREPEAT
	REPEAT FMMC_MAX_VEHICLES index
		iVehBlipCachedNamePriority[index] = -1
	ENDREPEAT
	REPEAT FMMC_MAX_NUM_OBJECTS index
		iObjBlipCachedNamePriority[index] = -1
	ENDREPEAT
ENDPROC

FUNC TEXT_LABEL_23 GET_BLIP_LITERAL_STRING_NAME(INT iTeam, INT iPriority, INT &iCachedPriority)
	TEXT_LABEL_23 tl23LiteralStringName = ""

	//First check the passed priority for a name.
	IF iPriority < FMMC_MAX_RULES
	AND iPriority != -1
		IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl23ObjBlip[iPriority])
		AND NOT g_FMMC_STRUCT.bPlayerLangNotEqualCreated
			//Set the current priority name.
			tl23LiteralStringName = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl23ObjBlip[iPriority]
			
			//Cache the priority if it doesn't already match.
			IF iCachedPriority != iPriority
				CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][CACHE] Caching blip name priority for team ", iTeam, " at priority ", iPriority, ".")
				CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][CACHE] Caching blip priority as ", iPriority, ".")
			
				iCachedPriority = iPriority
			ENDIF
		ENDIF
	ENDIF
	
	//If we haven't got a name yet check to see if we have a cached priority we can use.
	//This means entities will retain their blip names across rules.
	IF IS_STRING_NULL_OR_EMPTY(tl23LiteralStringName)
		IF iCachedPriority != -1 AND iCachedPriority < FMMC_MAX_RULES
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl23ObjBlip[iCachedPriority])
			AND NOT g_FMMC_STRUCT.bPlayerLangNotEqualCreated
				//Set the cached blip name.
				tl23LiteralStringName = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl23ObjBlip[iCachedPriority]
			ENDIF
		ENDIF
	ENDIF
	
	RETURN tl23LiteralStringName
ENDFUNC

FUNC BOOL SET_BLIP_CUSTOM_NAME_FROM_RULE(BLIP_INDEX &biBlip,INT iTeam, INT iPriority, INT &iCachedPriority)

	TEXT_LABEL_63 tl63LiteralStringName = GET_BLIP_LITERAL_STRING_NAME(iTeam, iPriority, iCachedPriority)
	PRINTLN("SET_BLIP_CUSTOM_NAME_FROM_RULE - iTeam: ", iTeam, " iPriority: ", iPriority, " iCachedPriority: ", iCachedPriority)
	IF NOT IS_STRING_NULL_OR_EMPTY(tl63LiteralStringName)
		SET_BLIP_CUSTOM_NAME(biBlip, tl63LiteralStringName)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_BLIP_NAME_FROM_CREATOR(BLIP_INDEX &biBlip)
	
	BLIP_SPRITE bsBlipSprite = GET_BLIP_SPRITE(biBlip)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(GET_FMMC_BLIP_NAME_FROM_RADAR_TRACE(bsBlipSprite))
		TEXT_LABEL_15 tl15Blipname = GET_BLIP_NAME_FROM_RADAR_TRACE_CREATOR(bsBlipSprite)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(tl15Blipname)
			SET_BLIP_NAME_FROM_TEXT_FILE(biBlip, tl15Blipname)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SET_CUSTOM_BLIP_NAME(FMMC_BLIP_TYPE &eBlipType, BLIP_INDEX &biBlip, INT iTeam, INT iEntityIndex)
	INT iPriority = -1
	INT iCachedPriority = -1
	SWITCH eBlipType
		CASE FMMC_BLIP_OBJECT
			iPriority = MC_serverBD_4.iObjPriority[iEntityIndex][iTeam]
			iCachedPriority = iObjBlipCachedNamePriority[iEntityIndex]
		BREAK
		CASE FMMC_BLIP_VEHICLE
			iPriority = MC_serverBD_4.iVehPriority[iEntityIndex][iTeam]
			iCachedPriority = iVehBlipCachedNamePriority[iEntityIndex]
		BREAK
		CASE FMMC_BLIP_PED
			iPriority = MC_serverBD_4.iPedPriority[iEntityIndex][iTeam]
			iCachedPriority = iPedBlipCachedNamePriority[iEntityIndex]
		BREAK
		CASE FMMC_BLIP_LOCATION
			iPriority = MC_serverBD_4.iGotoLocationDataPriority[iEntityIndex][iTeam]
			iCachedPriority = -1 //No cached name for locations
		BREAK
	ENDSWITCH
	RETURN SET_BLIP_CUSTOM_NAME_FROM_RULE(biBlip, iTeam, iPriority, iCachedPriority)
ENDFUNC

PROC SET_NAME_FOR_BLIP(BLIP_INDEX &biBlip, FMMC_BLIP_INFO_STRUCT &sBlipStruct, FMMC_BLIP_TYPE eBlipType, INT iEntityIndex, INT iTeam)
	IF NOT IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Use_Default_Blip_Name)
		IF IS_CUSTOM_STRING_LIST_STRING_VALID(sBlipStruct.iBlipNameIndex)
			SET_BLIP_NAME_FROM_STRING_LIST(sBlipStruct, biBlip)
		ELSE
			IF NOT SET_CUSTOM_BLIP_NAME(eBlipType, biBlip, iTeam, iEntityIndex)
				SET_BLIP_NAME_FROM_CREATOR(biBlip)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_BLIP_HEIGHT_INDICATOR(BLIP_INDEX &biBlip, FMMC_BLIP_INFO_STRUCT &sBlipStruct)
	SHOW_HEIGHT_ON_BLIP(biBlip, IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Show_Height_Indicator))
	PRINTLN("SET_BLIP_HEIGHT_INDICATOR - Show height on blip: ", BOOL_TO_STRING(IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Show_Height_Indicator)))
	IF IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Low_Blip_Height_Threshold)
		SET_BLIP_SHORT_HEIGHT_THRESHOLD(biBlip, TRUE)
	ELIF IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Extended_Height_Threshold)
		SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(biBlip, TRUE)
	ENDIF
ENDPROC

PROC PROCESS_BLIP_PLAY_GPS_SOUND_ON_CREATION(BLIP_INDEX &biBlip, FMMC_BLIP_INFO_STRUCT &sBlipStruct)

	IF NOT DOES_BLIP_EXIST(biBlip)
		EXIT
	ENDIF

	IF NOT IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Play_GPS_Sound_On_Creation)
		EXIT
	ENDIF
		
	PRINTLN("[Blips] PROCESS_BLIP_PLAY_GPS_SOUND_ON_CREATION - Playing GPS sound effect")
	PLAY_SOUND_FRONTEND(-1, "GPS_Set", "DLC_Security_Investigation_The_Yacht_Sounds")
	
ENDPROC

PROC PROCESS_BLIP_FLASH_ON_FIRST_CREATION(BLIP_INDEX &biBlip, FMMC_BLIP_INFO_STRUCT &sBlipStruct, BLIP_RUNTIME_VARS_STRUCT &sBlipRuntimeVars)
	
	IF NOT DOES_BLIP_EXIST(biBlip)
	OR NOT IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Flash_On_First_Creation)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(sBlipRuntimeVars.iBlipBS, ciBLIP_RUNTIME_Blip_Flashed_On_First_Creation)
		SET_BIT(sBlipRuntimeVars.iBlipBS, ciBLIP_RUNTIME_Blip_Flashed_On_First_Creation)
		SET_BLIP_FLASH_TIMER(biBlip, ciBLIP_FLASH_TIME_DEFAULT)
	ENDIF
	
ENDPROC

FUNC INT GET_CORRECT_BLIP_COLOUR(FMMC_BLIP_INFO_STRUCT &sBlipStruct, INT iAggroBS)
	INT iBlipColour
	
	IF sBlipStruct.iBlipColour != FMMC_BLIP_COLOUR_DEFAULT
		iBlipColour = sBlipStruct.iBlipColour
		PRINTLN("[Blips] GET_CORRECT_BLIP_COLOUR - Using custom colour from creator.")
	ENDIF
	
	IF (sBlipStruct.iBlipColourAggro != FMMC_BLIP_COLOUR_DEFAULT
	AND HAS_TEAM_TRIGGERED_AGGRO(MC_PlayerBD[iPartToUse].iTeam, iAggroBS))
		iBlipColour = sBlipStruct.iBlipColourAggro
		PRINTLN("[Blips] GET_CORRECT_BLIP_COLOUR - Using aggro colour from creator.")
	ENDIF
	
	IF GET_BLIP_SPRITE_FROM_BLIP_SPRITE_OVERRIDE(sBlipStruct.iBlipSpriteOverride, TRUE) = RADAR_TRACE_POLICE
		iBlipColour = FMMC_BLIP_COLOUR_WHITE
		PRINTLN("[Blips] GET_CORRECT_BLIP_COLOUR - Using white for the police blip.")
	ENDIF
	
	RETURN iBlipColour
ENDFUNC

PROC SET_COLOUR_FOR_ENTITY_BLIP(BLIP_INDEX &biBlip, FMMC_BLIP_INFO_STRUCT &sBlipStruct, FMMC_BLIP_TYPE eBlipType, INT iAggroBS)
	INT iBlipColour = GET_DEFAULT_ENTITY_BLIP_COLOUR(eBlipType)
	
	IF GET_CORRECT_BLIP_COLOUR(sBlipStruct, iAggroBS) != FMMC_BLIP_COLOUR_DEFAULT
		iBlipColour = GET_CORRECT_BLIP_COLOUR(sBlipStruct, iAggroBS)
	ENDIF
		
	SET_BLIP_COLOUR(biBlip, GET_BLIP_COLOUR_FROM_CREATOR(iBlipColour))
ENDPROC

PROC SET_UP_BLIP_OUTLINE(BLIP_INDEX &biBlip, FMMC_BLIP_INFO_STRUCT &sBlipStruct)
	
	IF sBlipStruct.iBlipSecondaryColour = FMMC_BLIP_COLOUR_DEFAULT
		EXIT
	ENDIF
	
	INT R, G, B, A
	
	HUD_COLOURS hcSecondaryColour = GET_HUD_COLOUR_FROM_CREATOR_BLIP_COLOUR(sBlipStruct.iBlipSecondaryColour)
	GET_HUD_COLOUR(hcSecondaryColour,R,G,B,A)
	
	APPLY_SECONDARY_COLOUR_TO_BLIP(biBlip,R,G,B)
	
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING GET_BLIP_TYPE_FROM_ENUM(FMMC_BLIP_TYPE eBlipType)
	SWITCH eBlipType
		CASE FMMC_BLIP_OBJECT 		RETURN "Object"
		CASE FMMC_BLIP_INTERACTABLE RETURN "Interactable"
		CASE FMMC_BLIP_PED 			RETURN "Ped"
		CASE FMMC_BLIP_VEHICLE 		RETURN "Vehicle"
		CASE FMMC_BLIP_TRAIN 		RETURN "Train"
		CASE FMMC_BLIP_DYNO_PROP 	RETURN "Dyno Prop"
		CASE FMMC_BLIP_LOCATION 	RETURN "Location"
		CASE FMMC_BLIP_PICKUP 		RETURN "Pickup"
		CASE FMMC_BLIP_DUMMY		RETURN "Dummy Blip"
	ENDSWITCH
	
	RETURN "Invalid"
ENDFUNC

PROC PRINT_BLIP_INFO_DATA(FMMC_BLIP_INFO_STRUCT &sBlipStruct, INT iTeam)

	PRINTLN("[RCC MISSION][Blip][Create] iBlipBitset: ", sBlipStruct.iBlipBitset)
	PRINTLN("[RCC MISSION][Blip][Create] iBlipStyle: ", sBlipStruct.iBlipStyle)
	PRINTLN("[RCC MISSION][Blip][Create] iBlipColour: ", sBlipStruct.iBlipColour)
	PRINTLN("[RCC MISSION][Blip][Create] iBlipColourAggro: ", sBlipStruct.iBlipColourAggro)
	PRINTLN("[RCC MISSION][Blip][Create] iBlipAlpha: ", sBlipStruct.iBlipAlpha)
	PRINTLN("[RCC MISSION][Blip][Create] fBlipScale: ", sBlipStruct.fBlipScale)
	PRINTLN("[RCC MISSION][Blip][Create] iBlipSpriteOverride: ", sBlipStruct.iBlipSpriteOverride)
	PRINTLN("[RCC MISSION][Blip][Create] fBlipRange: ", sBlipStruct.fBlipRange)
	PRINTLN("[RCC MISSION][Blip][Create] fBlipHeightDifference: ", sBlipStruct.fBlipHeightDifference)
	PRINTLN("[RCC MISSION][Blip][Create] iBlipPriority: ", sBlipStruct.iBlipPriority)
	PRINTLN("[RCC MISSION][Blip][Create] Team: ", iTeam)
	PRINTLN("[RCC MISSION][Blip][Create] iTeamBlipBitset: ", sBlipStruct.sTeamBlip[iTeam].iTeamBlipBitset)
	PRINTLN("[RCC MISSION][Blip][Create] iBlipOverrideBitSet: ", sBlipStruct.sTeamBlip[iTeam].iBlipOverrideBitSet)
	TEXT_LABEL_63 tlBlipname = GET_CUSTOM_STRING_LIST_TEXT_LABEL(sBlipStruct.iBlipNameIndex)
	PRINTLN("[RCC MISSION][Blip][Create] iBlipNameIndex: ", sBlipStruct.iBlipNameIndex, " - ", tlBlipname)
	PRINTLN("[RCC MISSION][Blip][Create] iShowBlipPreReqRequired: ", sBlipStruct.iShowBlipPreReqRequired)
	PRINTLN("[RCC MISSION][Blip][Create] iRemoveBlipPreReqRequired: ", sBlipStruct.iRemoveBlipPreReqRequired)
	PRINTLN("[RCC MISSION][Blip][Create] iBlipShowZonesBS: ", sBlipStruct.iBlipShowZonesBS)
	PRINTLN("[RCC MISSION][Blip][Create] iBlipHideZonesBS: ", sBlipStruct.iBlipHideZonesBS)
	
	IF IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Alpha_Based_On_Range)
		PRINTLN("[RCC MISSION][Blip][Create] iMinAlpha: ", sBlipStruct.sBlipAlphaRange.iMinAlpha)
		PRINTLN("[RCC MISSION][Blip][Create] fMinDistance: ", sBlipStruct.sBlipAlphaRange.fMinDistance)
		PRINTLN("[RCC MISSION][Blip][Create] fMaxDistance: ", sBlipStruct.sBlipAlphaRange.fMaxDistance)
	ENDIF
	
ENDPROC
#ENDIF

PROC CREATE_BLIP_WITH_CREATOR_SETTINGS(FMMC_BLIP_TYPE eBlipType, BLIP_INDEX &biBlip, FMMC_BLIP_INFO_STRUCT &sBlipStruct, BLIP_RUNTIME_VARS_STRUCT &sBlipRuntimeVars, ENTITY_INDEX entToBlip, INT iEntityIndex, VECTOR vBlipCoord, INT iAggroBS)

	INT iTeam = MC_playerBD[iPartToUse].iTeam
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[RCC MISSION][Blip][Create] CREATE_BLIP_WITH_CREATOR_SETTINGS - Adding blip for ", GET_BLIP_TYPE_FROM_ENUM(eBlipType), " ", iEntityIndex)
	PRINT_BLIP_INFO_DATA(sBlipStruct, iTeam)
	#ENDIF
	
	IF DOES_ENTITY_EXIST(entToBlip)
		IF eBlipType = FMMC_BLIP_PED
			CLEANUP_AI_PED_BLIP(biHostilePedBlip[iEntityIndex])
		ENDIF
		REMOVE_ANY_EXISTING_BLIP_FROM_ENTITY(entToBlip)
	ENDIF
	
	//Adding blip
	IF NOT DOES_BLIP_EXIST(biBlip)
		PRINTLN("[RCC MISSION][Blip][Create] - CREATE_BLIP_WITH_CREATOR_SETTINGS - Creating new blip")
		IF DOES_ENTITY_EXIST(entToBlip)
			biBlip = ADD_BLIP_FOR_ENTITY(entToBlip)
		ELIF NOT IS_VECTOR_ZERO(vBlipCoord)
			biBlip = ADD_BLIP_FOR_COORD(vBlipCoord)
		ENDIF
	ENDIF
		
	IF DOES_BLIP_EXIST(biBlip)
	
		//Custom Blip Sprite
		IF IS_BLIP_SPRITE_OVERRIDE_SET(sBlipStruct)
			SET_BLIP_SPRITE(biBlip, GET_BLIP_SPRITE_FROM_BLIP_SPRITE_OVERRIDE(sBlipStruct.iBlipSpriteOverride, TRUE))
			SET_BLIP_OVERRIDE_SPRITE_USE_CUSTOM_NAME(sBlipStruct.iBlipSpriteOverride, biBlip, TRUE)
		ENDIF
		
		//Blip Colour
		SET_COLOUR_FOR_ENTITY_BLIP(biBlip, sBlipStruct, eBlipType, iAggroBS)		
		
		//Blip Alpha
		SET_BLIP_ALPHA(biBlip, sBlipStruct.iBlipAlpha) 
		
		//Blip Scale
		FLOAT fBlipScale = GET_DEFAULT_ENTITY_BLIP_SCALE(eBlipType)
		IF sBlipStruct.fBlipScale != 1.0
			fBlipScale = sBlipStruct.fBlipScale
		ENDIF
		
		SET_BLIP_SCALE(biBlip, fBlipScale)
		
		//Blip Priority
		SET_DEFAULT_BLIP_PRIORITY(eBlipType, biBlip)
		IF sBlipStruct.iBlipPriority != 0
			SET_BLIP_PRIORITY(biBlip, INT_TO_ENUM(BLIP_PRIORITY, sBlipStruct.iBlipPriority))
		ENDIF
		
		//Height Indicator
		SET_BLIP_HEIGHT_INDICATOR(biBlip, sBlipStruct)
		
		//Arena
		IF CONTENT_IS_USING_ARENA()
			SET_BLIP_DISPLAY(biBlip, DISPLAY_RADAR_ONLY)
		ENDIF
		
		//Blipping as just a GPS route
		IF IS_BLIP_SET_AS_GPS_ONLY(sBlipStruct)
		
			PRINTLN("[RCC MISSION][Blip][Create] - Setting blip to be used as a GPS route only (Setting scale to 0 etc)")
			SET_BLIP_SCALE(biBlip, 0.0)
			SET_BLIP_HIDDEN_ON_LEGEND(biBlip, TRUE)
			
			SET_BLIP_ROUTE(biBlip, TRUE)
			INT iRouteColour = GET_DEFAULT_ENTITY_BLIP_COLOUR(eBlipType)
			IF sBlipStruct.iBlipColour != BLIP_COLOUR_DEFAULT
				iRouteColour = GET_BLIP_COLOUR_FROM_CREATOR(sBlipStruct.iBlipColour)
			ENDIF
			IF (sBlipStruct.iBlipColourAggro != BLIP_COLOUR_DEFAULT AND HAS_TEAM_TRIGGERED_AGGRO(iTeam, iAggroBS))
				iRouteColour = GET_BLIP_COLOUR_FROM_CREATOR(sBlipStruct.iBlipColourAggro)
			ENDIF		
	
			SET_BLIP_ROUTE_COLOUR(biBlip, iRouteColour)
			
		ENDIF
		
		//Play GPS Sound on Creation
		PROCESS_BLIP_PLAY_GPS_SOUND_ON_CREATION(biBlip, sBlipStruct)
		
		//Flash on First Creation
		PROCESS_BLIP_FLASH_ON_FIRST_CREATION(biBlip, sBlipStruct, sBlipRuntimeVars)
		
		//Player In Range
		IF eBlipType != FMMC_BLIP_DUMMY
			PROCESS_BLIP_PLAYER_IN_RANGE(sBlipStruct, sBlipRuntimeVars)
		ENDIF
		
		//Custom Name
		SET_NAME_FOR_BLIP(biBlip, sBlipStruct, eBlipType, iEntityIndex, iTeam)
		
		SET_UP_BLIP_OUTLINE(biBlip, sBlipStruct)
		
		IF IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Hide_Blip_On_Pause_Legend)
			SET_BLIP_HIDDEN_ON_LEGEND(biBlip, TRUE)
		ENDIF
		
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: OBJECT BLIPS ---------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to object blips	 ---------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_OBJECT_BLIP_COLOUR(INT iObjIndex)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].sObjBlipStruct.iBlipColour != BLIP_COLOUR_DEFAULT
		EXIT
	ENDIF
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iBlipColour = -1
	INT iBlipAlpha = -1

	IF GET_CORRECT_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].sObjBlipStruct, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].iAggroIndexBS_Entity_Obj) != BLIP_COLOUR_DEFAULT
		iBlipColour = GET_BLIP_COLOUR_FROM_CREATOR(GET_CORRECT_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].sObjBlipStruct, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].iAggroIndexBS_Entity_Obj))
	ENDIF
			
	IF MC_serverBD_4.iObjRule[iObjIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY		
		iBlipColour = BLIP_COLOUR_GREEN
	ENDIF
	
	IF (MC_serverBD_4.iObjRule[iObjIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_KILL OR MC_serverBD_4.iObjRule[iObjIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_DAMAGE)
	OR DOES_ENTITY_HAVE_BLIP_OVERRIDES(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].sObjBlipStruct)
		iBlipColour = BLIP_COLOUR_RED
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].sObjElectronicData.sCCTVData.iCCTVBS, ciCCTV_BS_OverrideBlipColour)
		IF DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].sObjElectronicData.sCCTVData)
			iBlipColour = BLIP_COLOUR_WHITE
		ELSE
			iBlipColour = BLIP_COLOUR_RED
		ENDIF
	ENDIF
	
	
	IF NOT IS_LONG_BITSET_EMPTY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].iPreReqRequiredForMinigame)
	AND NOT ARE_ANY_PREREQUSITES_IN_LONG_BITSET_COMPLETED(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].iPreReqRequiredForMinigame)
		iBlipColour = BLIP_COLOUR_WHITE
		iBlipAlpha = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].sObjBlipStruct.iBlipAlpha / 2
	ENDIF
	
	IF DOES_BLIP_EXIST(biObjBlip[iObjIndex])
		IF iBlipColour != -1
			SET_BLIP_COLOUR(biObjBlip[iObjIndex], iBlipColour)
		ENDIF
		IF iBlipAlpha != -1
			SET_BLIP_ALPHA(biObjBlip[iObjIndex], iBlipAlpha)
		ENDIF
	ENDIF
	
ENDPROC

PROC SET_CUSTOM_OBJECT_BLIP_SPRITE(INT iObjIndex)
	IF IS_BLIP_SPRITE_OVERRIDE_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].sObjBlipStruct)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].iObjectBitSetTwo, cibsOBJ2_TransformJuggernaut)
		SET_BLIP_SPRITE(biObjBlip[iObjIndex], RADAR_TRACE_JUGG)
	ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].iObjectBitSetTwo, cibsOBJ2_TransformBeast)
		SET_BLIP_SPRITE(biObjBlip[iObjIndex], RADAR_TRACE_BEAST)
	ENDIF
	
ENDPROC

PROC SET_PACKAGE_BLIP_TEAM_SECONDARY_COLOUR( BLIP_INDEX tempBlip, BOOL bSet, INT iteam )
	
	HUD_COLOURS teamcolour
	INT R,G,B,A
	 
	IF bSet
		teamcolour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iteam, PlayerToUse)
		
		GET_HUD_COLOUR( teamcolour, R, G, B, A )
		SET_SECONDARY_BLIP_COLOUR( tempBlip, R, G, B )
	ENDIF
	
	SHOW_OUTLINE_INDICATOR_ON_BLIP( tempBlip, bSet )

ENDPROC

PROC REFRESH_OBJECT_BLIP_COLOURING(FMMC_OBJECT_STATE &sObjState)
	IF NOT DOES_BLIP_EXIST(biObjBlip[sObjState.iIndex])
		EXIT
	ENDIF
	SET_COLOUR_FOR_ENTITY_BLIP(biObjBlip[sObjState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].sObjBlipStruct, FMMC_BLIP_OBJECT, g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iAggroIndexBS_Entity_Obj)
	SET_BLIP_ALPHA(biObjBlip[sObjState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].sObjBlipStruct.iBlipAlpha) 
	SET_OBJECT_BLIP_COLOUR(sObjState.iIndex)
ENDPROC

PROC SET_UP_OBJECT_BLIP(INT iObjIndex)
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iPriority = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iPriority < FMMC_MAX_RULES
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].sObjBlipStruct.iBlipStyle != FMMC_OBJECTBLIP_CTF
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].iObjectBitSetTwo, cibsOBJ2_BlipUseCustomHud)
		AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63ObjectiveEntityHUDMessage[iPriority])
			TEXT_LABEL_23 tl23LiteralStringName = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63ObjectiveEntityHUDMessage[iPriority]
			
			BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MCUSTBLIP")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl23LiteralStringName)
			END_TEXT_COMMAND_SET_BLIP_NAME(biObjBlip[iObjIndex])
		ENDIF
	ENDIF
	
	IF GET_TEAM_HOLDING_PACKAGE(iObjIndex) > -1
		SET_PACKAGE_BLIP_TEAM_SECONDARY_COLOUR(biObjBlip[iObjIndex], TRUE, GET_TEAM_HOLDING_PACKAGE(iObjIndex))
	ENDIF
	
	SET_OBJECT_BLIP_COLOUR(iObjIndex)
	SET_CUSTOM_OBJECT_BLIP_SPRITE(iObjIndex)
	
ENDPROC

PROC CREATE_OBJECT_BLIP(FMMC_OBJECT_STATE &sObjState)
	
	IF MC_serverBD.iObjCarrier[sObjState.iIndex] > -1
	AND MC_serverBD.iObjCarrier[sObjState.iIndex] = iPartToUse
		EXIT
	ENDIF
		
	CREATE_BLIP_WITH_CREATOR_SETTINGS(
		FMMC_BLIP_OBJECT, 
		biObjBlip[sObjState.iIndex], 
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].sObjBlipStruct, 
		sMissionObjectsLocalVars[sObjState.iIndex].sBlipRuntimeVars, 
		sObjState.objIndex, 
		sObjState.iIndex, 
		EMPTY_VEC(), 
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[sObjState.iIndex].iAggroIndexBS_Entity_Obj
	)
	
	SET_UP_OBJECT_BLIP(sObjState.iIndex)

ENDPROC

FUNC INT GET_HACK_OBJ_INITIAL_BLIP_COLOUR()
	INT iCol = BLIP_COLOUR_GREEN	
	RETURN iCol
ENDFUNC

// Every time a new blip is added and set to flash, resync all the currently flashing blips to ensure they flash in unison. 
PROC RESYNC_FLASHING_OBJECT_BLIPS()
	INT i
	REPEAT FMMC_MAX_NUM_OBJECTS i
		IF DOES_BLIP_EXIST(biObjBlip[i])
			IF IS_BLIP_FLASHING(biObjBlip[i])
				SET_BLIP_FLASHES(biObjBlip[i], TRUE)
			ENDIF
		ENDIF
	ENDREPEAT
	NET_PRINT("RESYNC_FLASHING_OBJECT_BLIPS() called. ") NET_NL()
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Interactable
// ##### Description: Functions for Interactable blips
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_UP_INTERACTABLE_BLIP(INT iInteractable)
	UNUSED_PARAMETER(iInteractable)
ENDPROC

PROC CREATE_INTERACTABLE_BLIP(ENTITY_INDEX entToblip, INT iInteractable)
	
	PRINTLN("[Interactables][iInteractable ", iInteractable, "] CREATE_INTERACTABLE_BLIP | Creating blip!")
	
	CREATE_BLIP_WITH_CREATOR_SETTINGS(
		FMMC_BLIP_INTERACTABLE, 
		biInteractableBlip[iInteractable], 
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct, 
		sMissionInteractablesLocalVars[iInteractable].sBlipRuntimeVars, 
		entToblip, 
		iInteractable, 
		EMPTY_VEC(), 
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAggroIndexBS_Entity_Interactable
	)
	
	SET_UP_INTERACTABLE_BLIP(iInteractable)

ENDPROC

PROC REMOVE_INTERACTABLE_BLIP(INT iInteractable)
	IF DOES_BLIP_EXIST(biInteractableBlip[iInteractable])
		PRINTLN("[Interactables][iInteractable ", iInteractable, "] REMOVE_INTERACTABLE_BLIP | Removing blip!")
		REMOVE_BLIP(biInteractableBlip[iInteractable])
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Dyno Props
// ##### Description: Functions for dyno prop blips
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_DYNOPROP_BE_BLIPPED(FMMC_DYNOPROP_STATE& sDynopropState)
	
	IF NOT sDynopropState.bDynopropExists
		#IF IS_DEBUG_BUILD
		IF bDynoPropSpam
			PRINTLN("[Dynoprops_SPAM][SPAM_Dynoprop ", sDynopropState.iIndex, "] SHOULD_DYNOPROP_BE_BLIPPED - iDynoProp: ", sDynopropState.iIndex, " Doesn't exist")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT sDynopropState.bDynopropAlive
		#IF IS_DEBUG_BUILD
		IF bDynoPropSpam
			PRINTLN("[Dynoprops_SPAM][SPAM_Dynoprop ", sDynopropState.iIndex, "] SHOULD_DYNOPROP_BE_BLIPPED - DynoProp: ", sDynopropState.iIndex, " Is dead")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_OBJECT_BROKEN_AND_VISIBLE(sDynopropState.objIndex, TRUE)
		#IF IS_DEBUG_BUILD
		IF bDynoPropSpam
			PRINTLN("[Dynoprops_SPAM][SPAM_Dynoprop ", sDynopropState.iIndex, "] SHOULD_DYNOPROP_BE_BLIPPED - Dynoprop: ", sDynopropState.iIndex, " Is broken (networked)")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF SHOULD_ENTITY_BLIP_BE_FORCED_OFF(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].sDynoPropBlipStruct)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_ENTITY_BLIP_BE_FORCED_ON(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].sDynoPropBlipStruct, sDynopropState.objIndex)
		RETURN TRUE
	ENDIF
	
	IF IS_BLIP_SET_AS_HIDDEN(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].sDynoPropBlipStruct)
		#IF IS_DEBUG_BUILD
		IF bDynoPropSpam
			PRINTLN("[Dynoprops_SPAM][SPAM_Dynoprop ", sDynopropState.iIndex, "] SHOULD_DYNOPROP_BE_BLIPPED - DynoProp: ", sDynopropState.iIndex, " Set to be hidden")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT SHOULD_ENTITY_BE_BLIPPED(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].sDynoPropBlipStruct, sMissionDynoPropsLocalVars[sDynopropState.iIndex].sBlipRuntimeVars, sDynopropState.objIndex)
		#IF IS_DEBUG_BUILD
		IF bDynoPropSpam
			PRINTLN("[Dynoprops_SPAM][SPAM_Dynoprop ", sDynopropState.iIndex, "] SHOULD_DYNOPROP_BE_BLIPPED - DynoProp: ", sDynopropState.iIndex, " Shouldn't be blipped according to blip struct")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].mn)
		IF HAS_ANY_TEAM_TRIGGERED_AGGRO(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].iAggroIndexBS_Entity_DynoProp)
			#IF IS_DEBUG_BUILD
			IF bDynoPropSpam
				PRINTLN("[Dynoprops_SPAM][SPAM_Dynoprop ", sDynopropState.iIndex, "] SHOULD_DYNOPROP_BE_BLIPPED - Dynoprop: ", sDynopropState.iIndex, " Is a CCTV camera and a player has already been spotted - removing blip now")
			ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_CASINO_HEIST_MISSION_CONTROLLER_BLIPS_HIDDEN)
		PRINTLN("[Dynoprops_SPAM][SPAM_Dynoprop ", sDynopropState.iIndex, "] SHOULD_DYNOPROP_BE_BLIPPED - DynoProp: ",sDynopropState.iIndex, " - False - LBOOL32_CASINO_HEIST_MISSION_CONTROLLER_BLIPS_HIDDEN is set.")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Peds
// ##### Description: Functions for ped blips
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_PED_HAVE_BLIP(INT iPed)
	RETURN FMMC_IS_LONG_BIT_SET(iPedLocalShouldHaveBlip, iPed)
ENDFUNC		

/// PURPOSE:
///    Check to see if we should force this ped blip to be visible because they are the current objective
FUNC BOOL SHOULD_PED_BLIP_DISPLAY_FOR_OBJECTIVE(INT iPedIndex)
	
	IF IS_PED_INVISIBLE(iPedIndex)
		IF NOT GET_USINGSEETHROUGH()
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF MC_ServerBD_4.iPedPriority[iPedIndex][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
		IF MC_serverBD_4.iPedRule[iPedIndex][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
		OR MC_serverBD_4.iPedRule[iPedIndex][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_PROTECT
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PED_IGNORE_VEHICLE_BLIP(INT iPedIndex)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iPedBitsetFifteen, ciPED_BSFifteen_IgnoreVehicleBlip)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BLIP_SPRITE GET_CORRECT_SPRITE_FOR_PED_BLIP(BLIP_INDEX &biBlip, INT iPedIndex, PED_INDEX piPed)
	
	BLIP_SPRITE BlipSprite
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iPedBitsetFour, ciPED_BSFour_SolidBlip)
		BlipSprite = GET_STANDARD_BLIP_ENUM_ID()
		PRINTLN("[Blips][Ped] SET_CORRECT_SPRITE_FOR_PED_BLIP - Using GET_STANDARD_BLIP_ENUM_ID")
	ELSE
		IF IS_PED_IN_ANY_VEHICLE(piPed)
			IF SHOULD_PED_BLIP_DISPLAY_FOR_OBJECTIVE(iPedIndex)
			OR SHOULD_PED_IGNORE_VEHICLE_BLIP(iPedIndex)
				BlipSprite = RADAR_TRACE_AI
				PRINTLN("[Blips][Ped] SET_CORRECT_SPRITE_FOR_PED_BLIP - Using RADAR_TRACE_AI in vehicle")
			ELSE
				BlipSprite = GET_CORRECT_BLIP_SPRITE_FOR_PED_IN_VEHICLE(piPed)
				PRINTLN("[Blips][Ped] SET_CORRECT_SPRITE_FOR_PED_BLIP - Using GET_CORRECT_BLIP_SPRITE_FOR_PED_IN_VEHICLE")
			ENDIF
		ELSE	
			BlipSprite = RADAR_TRACE_AI
			PRINTLN("[Blips][Ped] SET_CORRECT_SPRITE_FOR_PED_BLIP - Using RADAR_TRACE_AI on foot")
		ENDIF
		
	ENDIF
	
	IF IS_BLIP_SPRITE_OVERRIDE_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].sPedBlipStruct)
		BlipSprite = GET_BLIP_SPRITE_FROM_BLIP_SPRITE_OVERRIDE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].sPedBlipStruct.iBlipSpriteOverride, TRUE)
		PRINTLN("[Blips][Ped] SET_CORRECT_SPRITE_FOR_PED_BLIP - Using blip sprite override")
	ENDIF
	
	INT iBlipColour 
	IF DOES_BLIP_EXIST(biBlip)
		iBlipColour = GET_BLIP_COLOUR(biBlip)
	ENDIF
	
	IF (BlipSprite = RADAR_TRACE_ENEMY_HELI_SPIN)
	AND NOT ((iBlipColour = BLIP_COLOUR_HUDCOLOUR_RED) OR (iBlipColour = BLIP_COLOUR_RED))
		BlipSprite = RADAR_TRACE_PLAYER_HELI
		PRINTLN("[Blips][Ped] SET_CORRECT_SPRITE_FOR_PED_BLIP - Using player heli")
	ENDIF	
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iPedBitsetEight, ciPed_BSEight_ForceBoatBlipSprite)
		BlipSprite = RADAR_TRACE_PLAYER_BOAT
		PRINTLN("[Blips][Ped] SET_CORRECT_SPRITE_FOR_PED_BLIP - Using boat")
	ENDIF
	
	RETURN BlipSprite
	
ENDFUNC

PROC SET_CORRECT_SPRITE_FOR_PED_BLIP(BLIP_INDEX &biBlip, INT iPedIndex, PED_INDEX piPed)
	
	IF IS_BLIP_SPRITE_OVERRIDE_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].sPedBlipStruct)
		EXIT
	ENDIF
	
	BLIP_SPRITE BlipSprite = GET_CORRECT_SPRITE_FOR_PED_BLIP(biBlip, iPedIndex, piPed)
	
	IF BlipSprite != RADAR_TRACE_INVALID
		SET_BLIP_SPRITE(biBlip, BlipSprite)
		FLOAT fBlipScale
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iPedBitsetFifteen, ciPED_BSFifteen_UseCreatorBlipScale)
			fBlipScale = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].sPedBlipStruct.fBlipScale
			PRINTLN("[Blips][Ped] SET_CORRECT_SPRITE_FOR_PED_BLIP - Using creator set size for blip sprite")
		ELSE
			fBlipScale = GET_BASE_SCALE_FOR_BLIP_SPRITE(BlipSprite, BLIP_SIZE_NETWORK_PED)
			PRINTLN("[Blips][Ped] SET_CORRECT_SPRITE_FOR_PED_BLIP - Using default size for blip sprite")
		ENDIF
		SET_BLIP_SCALE(biBlip, fBlipScale)
		
		SET_BLIP_HEIGHT_INDICATOR(biBlip, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].sPedBlipStruct)
	ENDIF
ENDPROC

FUNC INT GET_NEW_RELATIONSHIP_GROUP_BLIP_COLOUR(INT iPed)	
	IF FMMC_IS_LONG_BIT_SET(iPedUseRelGroupChangedToFriendly, iPed)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iBlipColourRelGroupFriendly > -1
			RETURN GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iBlipColourRelGroupFriendly)
		ENDIF
	ELIF FMMC_IS_LONG_BIT_SET(iPedUseRelGroupChangedToNeutral, iPed)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iBlipColourRelGroupNeutral > -1
			RETURN GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iBlipColourRelGroupNeutral)
		ENDIF
	ELIF FMMC_IS_LONG_BIT_SET(iPedUseRelGroupChangedToHostile, iPed)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iBlipColourRelGroupHostile > -1
			RETURN GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iBlipColourRelGroupHostile)
		ENDIF
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC BOOL SHOULD_OBJ_BLOCKER_AFFECT_BLIP(INT iPedIndex)
	IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iPedBitsetTwo, ciPed_BSTwo_BlipOnFollow )
		RETURN FALSE
	ENDIF

	RETURN IS_OBJECTIVE_BLOCKED()
ENDFUNC

PROC SET_CORRECT_COLOUR_FOR_PED_BLIP(BLIP_INDEX &biBlip, INT iPedIndex)
	INT iBlipColour = BLIP_COLOUR_RED
	
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iPedBitsetTwo, ciPed_BSTwo_BlipOnFollow)
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iPriority[iTeam] <= iRule
	AND MC_serverBD_4.iPedPriority[iPedIndex][iTeam] < FMMC_MAX_RULES
		iBlipColour = BLIP_COLOUR_BLUE
		PRINTLN("[Blips][Ped] SET_CORRECT_COLOUR_FOR_PED_BLIP - Ped ", iPedIndex, " Blip on follow, setting blip colour to blue ", iBlipColour)
	ENDIF
	
	IF MC_serverBD_4.iPedPriority[iPedIndex][iTeam] <= iRule
	AND MC_serverBD_4.iPedPriority[iPedIndex][iTeam] < FMMC_MAX_RULES
		IF NOT SHOULD_OBJ_BLOCKER_AFFECT_BLIP(iPedIndex)
		AND MC_serverBD_4.iPedRule[iPedIndex][iTeam] != FMMC_OBJECTIVE_LOGIC_KILL
		AND MC_serverBD_4.iPedRule[iPedIndex][iTeam] != FMMC_OBJECTIVE_LOGIC_DAMAGE
			IF (MC_serverBD_4.iPedRule[iPedIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAtYourHolding[iTeam], iPedIndex))
			OR MC_serverBD_4.iPedRule[iPedIndex][iTeam] != FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
				iBlipColour = BLIP_COLOUR_BLUE
				PRINTLN("[Blips][Ped] SET_CORRECT_COLOUR_FOR_PED_BLIP - Ped ", iPedIndex, " Blipped not on a kill rule, setting blip colour to blue ", iBlipColour)
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_serverBD_4.iPedRule[iPedIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iTeam[iTeam] = ciPED_RELATION_SHIP_LIKE			
		iBlipColour = BLIP_COLOUR_BLUE
		PRINTLN("[Blips][Ped] SET_CORRECT_COLOUR_FOR_PED_BLIP - Ped ", iPedIndex, " Blipped on a Leave Entity rule and is friendly, setting blip colour to blue ", iBlipColour)
	ENDIF
	
	IF GET_NEW_RELATIONSHIP_GROUP_BLIP_COLOUR(iPedIndex) > -1
		iBlipColour = GET_NEW_RELATIONSHIP_GROUP_BLIP_COLOUR(iPedIndex)
		PRINTLN("[Blips][Ped] SET_CORRECT_COLOUR_FOR_PED_BLIP - Ped ", iPedIndex, " using new Relationship Group Blip Colour: ", iBlipColour)
	ENDIF
	
	IF GET_CORRECT_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].sPedBlipStruct, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iAggroIndexBS_Entity_Ped) != BLIP_COLOUR_DEFAULT
		iBlipColour = GET_BLIP_COLOUR_FROM_CREATOR(GET_CORRECT_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].sPedBlipStruct, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iAggroIndexBS_Entity_Ped))
	ENDIF
	
	IF iBlipColour != -1
		SET_BLIP_COLOUR(biBlip, iBlipColour)
	ENDIF
	
ENDPROC

FUNC BOOL FMMC_IS_PED_LEADER_OF_VEHICLE(PED_INDEX PedID, VEHICLE_INDEX VehicleID, BOOL bConsiderPlayerPedsOnly = FALSE)
	
	INT iNoOfSeats
	INT iSeat
	VEHICLE_SEAT eSeat
	iNoOfSeats = GET_VEHICLE_MODEL_NUMBER_OF_SEATS(GET_ENTITY_MODEL(VehicleID))
	PED_INDEX PassengerPedID
	
	FOR iSeat = -1 TO iNoOfSeats-1
		eSeat = INT_TO_ENUM(VEHICLE_SEAT, iSeat)
		// the first non free seat is the 'leader'
		IF NOT IS_VEHICLE_SEAT_FREE(VehicleID, eSeat)
			PassengerPedID = GET_PED_IN_VEHICLE_SEAT(VehicleID, eSeat) 		
			IF IS_PED_A_PLAYER(PassengerPedID)
			OR NOT bConsiderPlayerPedsOnly	
				IF PassengerPedID = PedID
					RETURN TRUE
				ELSE
					IF IS_ENTITY_ALIVE(PassengerPedID)
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_HIDE_PED_BLIP_ON_CREATION(BLIP_INDEX &biBlip, INT iPedIndex, PED_INDEX piPed)
	
	IF IS_PED_IN_ANY_VEHICLE(piPed)
	AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
	AND GET_VEHICLE_PED_IS_IN(piPed) = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		PRINTLN("[Blips] - SHOULD_HIDE_PED_BLIP_ON_CREATION - Hiding blip as ped is in vehicle with local player")
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_VEHICLE_THAT_HAS_A_BLIP(piPed)
		PRINTLN("[Blips] - SHOULD_HIDE_PED_BLIP_ON_CREATION - Hiding blip as ped is in vehicle that has a blip")
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(piPed)
	AND NOT FMMC_IS_PED_LEADER_OF_VEHICLE(piPed, GET_VEHICLE_PED_IS_IN(piPed))
	AND SHOULD_BLIP_ONLY_SHOW_LEADERS_WHEN_IN_VEHICLE(biBlip)
		IF SHOULD_PED_BLIP_DISPLAY_FOR_OBJECTIVE(iPedIndex)
			PRINTLN("[Blips] - SHOULD_HIDE_PED_BLIP_ON_CREATION - Not hiding blip but setting it to flash")
			SET_BLIP_FLASHES(biBlip, TRUE)
		ELSE
			PRINTLN("[Blips] - SHOULD_HIDE_PED_BLIP_ON_CREATION - Hiding blip as ped is not an objective")
			RETURN TRUE
		ENDIF
	ENDIF

	
	RETURN FALSE
ENDFUNC

PROC SET_PED_BLIP_ALPHA(BLIP_INDEX &biBlip, INT iPedIndex, PED_INDEX piPed)
	
	IF SHOULD_HIDE_PED_BLIP_ON_CREATION(biBlip, iPedIndex, piPed)
		SET_BLIP_ALPHA(biBlip, 0)
	ENDIF
	
ENDPROC

PROC SET_PED_BLIP_CUSTOM_NAME(BLIP_INDEX &biBlip, INT iPedIndex)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iPedBitsetTwelve, ciPed_BSTwelve_UseFailNameForBlip)
		INT iName = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iCustomPedName
		IF iName > -1
			PRINTLN("[Blips] SET_PED_BLIP_CUSTOM_NAME - [RCC MISSION][CACHE] iPedIndex: ", iPedIndex, " iName: ", iName, " ActualString: ", g_FMMC_STRUCT_ENTITIES.tlCustomPedName[iName], ".")
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[iName])
				BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MCUSTBLIP")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[iName])
				END_TEXT_COMMAND_SET_BLIP_NAME(biBlip)
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	SET_NAME_FOR_BLIP(biBlip, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].sPedBlipStruct, FMMC_BLIP_PED, iPedIndex, GET_LOCAL_PLAYER_TEAM(TRUE))
	
ENDPROC

FUNC BOOL SHOULD_DISPLAY_CONE_ON_THIS_PED(INT iPedIndex)
	
	BOOL bCone
	BOOL bAggroPed
	
	bCone = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iPedBitsetThree, ciPed_BSThree_ShowVisionCone)
	
	IF bCone
		
		bAggroPed = SHOULD_AGRO_FAIL_FOR_TEAM(iPedIndex, MC_playerBD[iPartToUse].iTeam)
		
		IF (bAggroPed AND HAS_TEAM_TRIGGERED_AGGRO(MC_playerBD[iPartToUse].iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iAggroIndexBS_Entity_Ped) AND FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAggroedBitset, iPedIndex))
		OR ((NOT bAggroPed) AND FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedBitset, iPedIndex))
			bCone = FALSE
		ENDIF
	ENDIF
	
	RETURN bCone
	
ENDFUNC

///PURPOSE: This function will change whether the blip's vision cone is displayed. 
PROC ENABLE_BLIP_VISION_CONE(BLIP_INDEX &biBlip, BOOL bEnable)
	IF DOES_BLIP_EXIST(biBlip)
		SET_BLIP_SHOW_CONE(biBlip, bEnable )
	ENDIF
ENDPROC

PROC SET_UP_PED_BLIP(BLIP_INDEX &biBlip, INT iPedIndex, PED_INDEX piPed)
	
	IF NOT DOES_ENTITY_EXIST(piPed)
		EXIT
	ENDIF
	
	SET_CORRECT_SPRITE_FOR_PED_BLIP(biBlip, iPedIndex, piPed)
	
	IF SHOULD_BLIP_BE_MANUALLY_ROTATED(biPedBlip[iPedIndex])
		SET_BLIP_ROTATION(biBlip, ROUND(GET_ENTITY_HEADING_FROM_EULERS(piPed)))
	ENDIF
	
	SET_CORRECT_COLOUR_FOR_PED_BLIP(biBlip, iPedIndex)
	
	SET_PED_BLIP_ALPHA(biBlip, iPedIndex, piPed)
	
	SET_PED_BLIP_CUSTOM_NAME(biBlip, iPedIndex)
	
	ENABLE_BLIP_VISION_CONE(biPedBlip[iPedIndex], SHOULD_DISPLAY_CONE_ON_THIS_PED(iPedIndex))
	
ENDPROC

PROC CREATE_PED_BLIP_WITH_CREATOR_SETTINGS(INT iPedIndex, PED_INDEX piPed)
	
	IF IS_PED_A_COP_MC(iPedIndex)
	AND GET_PLAYER_WANTED_LEVEL(LocalPlayer) > 0
		PRINTLN("[Blip][Peds][Ped ", iPedIndex, "] CREATE_PED_BLIP_WITH_CREATOR_SETTINGS - This ped is a cop and player has wanted level, leaving it to code.")
		EXIT
	ENDIF
	
	CLEANUP_AI_PED_BLIP(biHostilePedBlip[iPedIndex])
	
	FMMC_CLEAR_LONG_BIT(iPedSASBlipColour, iPedIndex)
	
	FMMC_CLEAR_LONG_BIT(iPedSASBlipCone, iPedIndex)
	
	CREATE_BLIP_WITH_CREATOR_SETTINGS(
		FMMC_BLIP_PED, 
		biPedBlip[iPedIndex], 
		g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].sPedBlipStruct, 
		sMissionPedsLocalVars[iPedIndex].sBlipRuntimeVars, 
		piPed, 
		iPedIndex, 
		EMPTY_VEC(), 
		g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iAggroIndexBS_Entity_Ped
	)

	SET_UP_PED_BLIP(biPedBlip[iPedIndex], iPedIndex, piPed)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicles
// ##### Description: Functions for vehicle blips
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(INT iVeh)
	RETURN IS_BIT_SET(iShouldBlipVehBitSet, iVeh)
ENDFUNC

FUNC BOOL IS_VEHICLE_BLIP_A_CUSTOM_BLIP(INT iVeh)
	RETURN IS_BIT_SET(iVehicleCustomBlip, iVeh)
ENDFUNC
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cleanup
// ##### Description: Functions for cleaning up blips
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC REMOVE_OBJECT_BLIP(INT iObj)
	IF DOES_BLIP_EXIST(biObjBlip[iObj])
		REMOVE_CCTV_VISION_CONE(biObjBlip[iobj], iObj, FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT)
		PRINTLN("[RCC MISSION] Removing blip for object ", iObj)
		REMOVE_BLIP(biObjBlip[iobj])
	ENDIF
ENDPROC	

PROC REMOVE_DYNOPROP_BLIP(INT iDynoProp)
	IF DOES_BLIP_EXIST(biDynoPropBlips[iDynoProp])
		REMOVE_CCTV_VISION_CONE(biDynoPropBlips[iDynoProp], iDynoProp, FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP)
		REMOVE_BLIP(biDynoPropBlips[iDynoProp])
		PRINTLN("[Dynoprops] REMOVE_DYNOPROP_BLIP | Removing blip for dynoprop ", iDynoProp)
	ENDIF
ENDPROC

PROC REMOVE_TRAIN_BLIP(INT iTrain)
	IF DOES_BLIP_EXIST(biTrainBlip[iTrain])
		PRINTLN("[Trains][Train ", iTrain, "] REMOVE_TRAIN_BLIP Removing blip")
		REMOVE_BLIP(biTrainBlip[iTrain])
	ENDIF
ENDPROC	

// Custom procedure for the removal of a vehicle blip.
PROC REMOVE_VEHICLE_BLIP( INT iVeh )
	IF IS_BIT_SET(iVehBlipFlashingBitset, iVeh)
		CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Clearing flashing vehicle blip for vehicle ", iVeh, ".")
		CLEAR_BIT(iVehBlipFlashingBitset, iVeh)
	ENDIF

	IF DOES_BLIP_EXIST(biVehBlip[iVeh])
		// url:bugstar:2192742
		IF DOES_BLIP_HAVE_GPS_ROUTE(biVehBlip[iVeh])
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_IgnoreNoGPSFlag)
			SET_IGNORE_NO_GPS_FLAG(FALSE)
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Calling SET_IGNORE_NO_GPS_FLAG FALSE for vehicle ", iVeh, ".")
		ENDIF
		
		CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH]  Removing vehicle blip for vehicle ", iVeh, ".")
		REMOVE_BLIP(biVehBlip[iVeh])
	ENDIF
ENDPROC

///PURPOSE: This function removes the blip for get & deliver
PROC REMOVE_DELIVERY_BLIP()
	IF DOES_BLIP_EXIST(DeliveryBlip)
		REMOVE_BLIP(DeliveryBlip)
		CLEAR_BIT( iLocalBoolCheck5, LBOOL5_GPS_DELIVER_ROUTE_SET )
		
		IF IS_BIT_SET( iLocalBoolCheck9, LBOOL9_SET_IGNORE_NO_GPS_FLAG )
			SET_IGNORE_NO_GPS_FLAG(FALSE)
			PRINTLN("[RCC MISSION] REMOVE_DELIVERY_BLIP called SET_IGNORE_NO_GPS_FLAG FALSE")
			CLEAR_BIT( iLocalBoolCheck9, LBOOL9_SET_IGNORE_NO_GPS_FLAG )
		ENDIF
	ENDIF
	iDeliveryBlip_veh = -1
ENDPROC

PROC CLEANUP_OBJECTIVE_BLIPS()
	
	INT i
	
	FOR i = 0 TO (FMMC_MAX_PEDS-1)
		IF DOES_BLIP_EXIST(biPedBlip[i])
			IF NOT SHOULD_PED_HAVE_BLIP(i)
				REMOVE_BLIP(biPedBlip[i])
				PRINTLN("[RCC MISSION] CLEANUP_OBJECTIVE_BLIPS removing ped: ",i)
			ENDIF
		ENDIF
	ENDFOR
	
	FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
		IF DOES_BLIP_EXIST(biVehBlip[i])
		AND NOT SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(i)
			REMOVE_VEHICLE_BLIP(i)
			PRINTLN("[RCC MISSION] CLEANUP_OBJECTIVE_BLIPS removing veh: ",i) 
		ENDIF
	ENDFOR
	
	FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
		REMOVE_OBJECT_BLIP(i)
	ENDFOR
	
	FOR i = 0 TO (FMMC_MAX_NUM_DYNOPROPS-1)
		REMOVE_DYNOPROP_BLIP(i)
	ENDFOR
	CLEAR_FAKE_CONE_ARRAY()
	iCCTVConeBitSet = 0
	
	FOR i = 0 TO (FMMC_MAX_GO_TO_LOCATIONS -1)
		IF DOES_BLIP_EXIST(LocBlip[i])
			REMOVE_BLIP(LocBlip[i])
		ENDIF
	ENDFOR
	
	FOR i = 0 TO FMMC_MAX_TRAINS - 1
		IF DOES_BLIP_EXIST(biTrainBlip[i])
			REMOVE_BLIP(biTrainBlip[i])
		ENDIF
	ENDFOR
	
	IF DOES_BLIP_EXIST(DeliveryBlip)
		REMOVE_DELIVERY_BLIP()
	ENDIF

ENDPROC

PROC CLEANUP_ALL_BLIPS()

	PRINTLN("[Blips] CLEANUP_ALL_BLIPS called") 

	INT i
	
	CLEANUP_ALL_AI_PED_BLIPS(biHostilePedBlip)
	
	FOR i = 0 TO (FMMC_MAX_PEDS-1)
		IF IS_LONG_BIT_SET(iPedTagCreated, i) //Used in a shared external file
	        REMOVE_PED_OVERHEAD_ICONS(iPedTag[i], i, iPedTagCreated, iPedTagInitalised)
	    ENDIF
		IF DOES_BLIP_EXIST(biPedBlip[i])
			REMOVE_BLIP(biPedBlip[i])
		ENDIF
	ENDFOR

	FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
		IF DOES_BLIP_EXIST(biVehBlip[i])
			REMOVE_VEHICLE_BLIP(i)
		ENDIF
	ENDFOR
	FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
		REMOVE_OBJECT_BLIP(i)
	ENDFOR
	FOR i = 0 TO (FMMC_MAX_GO_TO_LOCATIONS -1)
		IF DOES_BLIP_EXIST(LocBlip[i])
			REMOVE_BLIP(LocBlip[i])
		ENDIF
		IF DOES_BLIP_EXIST(LocBlip1[i])
			REMOVE_BLIP(LocBlip1[i])
		ENDIF
	ENDFOR
	
	// Remove pickup blips
	FOR i = 0 TO (FMMC_MAX_WEAPONS-1)
		IF DOES_BLIP_EXIST(biPickup[i])
			REMOVE_BLIP(biPickup[i])
		ENDIF
	ENDFOR
	
	FOR i = 0 TO FMMC_MAX_TRAINS - 1
		IF DOES_BLIP_EXIST(biTrainBlip[i])
			REMOVE_BLIP(biTrainBlip[i])
		ENDIF
	ENDFOR
	
	IF DOES_BLIP_EXIST(DeliveryBlip)
		REMOVE_BLIP(DeliveryBlip)
	ENDIF
	
ENDPROC

		
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Shared Processing
// ##### Description: Functions for shared blip processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_BLIP_HEIGHT_INDICATOR(BLIP_INDEX &biBlip, FMMC_BLIP_INFO_STRUCT &sBlipStruct, VECTOR vCoord)
	
	IF NOT IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Show_Height_Indicator)
		EXIT
	ENDIF
	
	IF sBlipStruct.fBlipShowHeightIndicatorRange = 0.0
		EXIT
	ENDIF
	
	IF NOT bLocalPlayerPedOK
		EXIT
	ENDIF
	
	IF VDIST2_2D(vLocalPlayerPosition, vCoord) < POW(sBlipStruct.fBlipShowHeightIndicatorRange, 2)
		SHOW_HEIGHT_ON_BLIP(biBlip, TRUE)
	ELSE
		SHOW_HEIGHT_ON_BLIP(biBlip, FALSE)
	ENDIF
	
ENDPROC

PROC PROCESS_BLIP_AGGRO_COLOUR(BLIP_INDEX &biBlip, FMMC_BLIP_INFO_STRUCT &sBlipStruct, INT iAggroBS)

	IF GET_BLIP_COLOUR_FROM_CREATOR(sBlipStruct.iBlipColourAggro) = FMMC_BLIP_COLOUR_DEFAULT
		EXIT
	ENDIF
	
	IF NOT HAS_TEAM_TRIGGERED_AGGRO(MC_PlayerBD[iPartToUse].iTeam, iAggroBS)
		EXIT
	ENDIF	
	
	IF GET_BLIP_COLOUR(biBlip) != GET_BLIP_COLOUR_FROM_CREATOR(sBlipStruct.iBlipColourAggro)
		SET_BLIP_COLOUR(biBlip, GET_BLIP_COLOUR_FROM_CREATOR(sBlipStruct.iBlipColourAggro))
	ENDIF
	
ENDPROC

PROC PROCESS_BLIP_ALPHA_BASED_ON_RANGE_FROM_PLAYER(BLIP_INDEX &biBlip, FMMC_BLIP_INFO_STRUCT &sBlipStruct)
	IF NOT DOES_BLIP_EXIST(biBlip)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(sBlipStruct.iBlipBitset, ciBLIP_INFO_Alpha_Based_On_Range)
		EXIT
	ENDIF
		
	IF IS_PAUSE_MENU_ACTIVE()
		SET_BLIP_ALPHA(biBlip, 255)
	ELSE
		SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER(biBlip, sBlipStruct.sBlipAlphaRange.iMinAlpha, sBlipStruct.sBlipAlphaRange.fMaxDistance, sBlipStruct.sBlipAlphaRange.fMinDistance)
	ENDIF
	
ENDPROC

PROC PROCESS_ENTITY_BLIP(BLIP_INDEX &biBlip, FMMC_BLIP_INFO_STRUCT &sBlipStruct, VECTOR vCoord, INT iAggroBS)
	
	IF NOT DOES_BLIP_EXIST(biBlip)
		EXIT
	ENDIF
	
	PROCESS_BLIP_HEIGHT_INDICATOR(biBlip, sBlipStruct, vCoord)
	
	PROCESS_BLIP_AGGRO_COLOUR(biBlip, sBlipStruct, iAggroBS)
	
	PROCESS_BLIP_ALPHA_BASED_ON_RANGE_FROM_PLAYER(biBlip, sBlipStruct)
		
ENDPROC


				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Dummy Blips [Dummy Blip]
// ##### Description: All functions for dummy blips
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_DUMMY_BLIP_WITHIN_SPECIFIED_RULES(INT iDummyBlip)
	
	IF DOES_ENTITY_HAVE_BLIP_OVERRIDES(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData)
		IF SHOULD_ENTITY_BE_BLIPPED_THROUGH_OVERRIDES(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData)
			RETURN TRUE
		ENDIF
	ENDIF

	IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iRule = ciDUMMY_BLIP_RULE_ANY
		RETURN TRUE
	ELIF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iRule = MC_ServerBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iTeam]
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL DOES_DUMMY_BLIP_LINKED_ENTITY_EXIST(INT iDummyBlip)
	IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityType > -1
	AND g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityIndex > -1		
		
		VEHICLE_INDEX vehIndex
		
		SWITCH g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityType
			CASE ciENTITY_TYPE_PED
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityIndex])
					PED_INDEX pedIndex
					pedIndex = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityIndex])
					IF NOT IS_PED_INJURED(pedIndex)
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			CASE ciENTITY_TYPE_VEHICLE
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityIndex])
					vehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityIndex])
					IF IS_VEHICLE_DRIVEABLE(vehIndex)
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			CASE ciENTITY_TYPE_OBJECT	
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_OBJECT_NET_ID(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityIndex))
					RETURN TRUE
				ENDIF
			BREAK
			CASE ciENTITY_TYPE_TRAIN
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niTrain[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityIndex])
					vehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niTrain[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityIndex])
					IF IS_ENTITY_ALIVE(vehIndex)
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK

		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC SET_DUMMY_BLIP_LOCATION_AND_ENTITY(INT iDummyBlip, VECTOR &vBlip, INT& iEntity)
	
	IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iType = ciFMMC_DROP_OFF_TYPE_CYLINDER
	OR g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iType = ciFMMC_DROP_OFF_TYPE_AREA
    	vBlip = g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].vPos
	ELIF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iType = ciFMMC_DROP_OFF_TYPE_PED
		IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityToUse > -1
			NETWORK_INDEX niPed = MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityToUse]
			IF NETWORK_DOES_NETWORK_ID_EXIST(niPed)
			AND IS_ENTITY_ALIVE(NET_TO_PED(niPed))
				iEntity = g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityToUse
			ENDIF
		ENDIF
	ELSE
		IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityToUse > -1
			NETWORK_INDEX niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityToUse]
			IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
			AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(niVeh))
				iEntity = g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityToUse
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_DISPLAY_DUMMY_BLIP_FOR_TEAM(INT iDummyBlip, INT iTeam)
	
	IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iTeam = -1
		PRINTLN("[RCC MISSION][Dummy Blip] SHOULD_DISPLAY_DUMMY_BLIP_FOR_TEAM - Dummy blip doesn't have a team set. One is required.")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet,ciDUMMY_BLIP_SHOW_FOR_TEAM1 + iTeam)
		PRINTLN("[RCC MISSION][Dummy Blip] SHOULD_DISPLAY_DUMMY_BLIP_FOR_TEAM - Dummy blip not set to be shown for team ", iTeam)
		RETURN FALSE
	ENDIF

	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_DUMMY_BLIP_EXIST(INT iDummyBlip, VECTOR vBlip, INT iEntity)
	
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iDummyBlip, eSGET_Dummyblip)
		PRINTLN("[RCC MISSION][Dummy Blip] SHOULD_DUMMY_BLIP_EXIST - iDummyBlip ", iDummyBlip, " Does not have a suitable spawn group - Returning False")
		RETURN FALSE
	ENDIF
	
	IF SHOULD_ENTITY_BLIP_BE_FORCED_OFF(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_ENTITY_BLIP_BE_FORCED_ON(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData)
		RETURN TRUE
	ENDIF
	
	IF IS_BLIP_SET_AS_HIDDEN(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData)
		RETURN FALSE
	ENDIF
		
	IF NOT SHOULD_ENTITY_BE_BLIPPED(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData, sMissionDummyBlipsLocalVars[iDummyBlip].sBlipRuntimeVars, NULL, FALSE)
		RETURN FALSE
	ENDIF
	
	IF NOT SHOULD_DISPLAY_DUMMY_BLIP_FOR_TEAM(iDummyBlip, MC_playerBD[iPartToUse].iTeam)
		PRINTLN("[RCC MISSION][Dummy Blip] SHOULD_DUMMY_BLIP_EXIST - NO - Team not valid for this dummy blip")
		RETURN FALSE
	ENDIF
	
	IF IS_VECTOR_ZERO(vBlip)
	AND iEntity = -1
		PRINTLN("[RCC MISSION][Dummy Blip] SHOULD_DUMMY_BLIP_EXIST - NO - Blip creation point is invalid")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_DUMMY_BLIP_WITHIN_SPECIFIED_RULES(iDummyBlip)
		PRINTLN("[RCC MISSION][Dummy Blip] SHOULD_DUMMY_BLIP_EXIST - NO - Current rule isn't valid for this dummy blip")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iDummyBlipBitset[iDummyBlip], ciDBBS_Remove_On_Hide)
		PRINTLN("[RCC MISSION][Dummy Blip] SHOULD_DUMMY_BLIP_EXIST - NO - ciDBBS_Remove_On_Hide is set")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_DUMMY_BLIP_HIDE_FOR_FLIGHT(INT iDummyBlip)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_HIDE_FLYING)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		VEHICLE_INDEX viVeh = GET_VEHICLE_PED_IS_IN(localPlayerPed, FALSE)
		
		IF DOES_ENTITY_EXIST(viVeh)
		AND NOT IS_VEHICLE_FUCKED_MP(viVeh)
			MODEL_NAMES mnVeh = GET_ENTITY_MODEL(viVeh)
			
			IF IS_THIS_MODEL_A_PLANE(mnVeh)
			OR IS_THIS_MODEL_A_HELI(mnVeh)
				IF IS_ENTITY_IN_AIR(viVeh)
				AND GET_PLANE_ALTITUDE(viVeh) > ciDUMMY_BLIP_HIDE_FLYING_ALTITUDE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DUMMY_BLIP_BE_HIDDEN(INT iDummyBlip)
	
	VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed, FALSE)
	VECTOR vDummyBlipPos = GET_BLIP_COORDS(DummyBlip[iDummyBlip])
	
	IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].fHideBlipRange != 0.0
		IF VDIST2_2D(vDummyBlipPos, vPlayerPos) < POW(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].fHideBlipRange, 2)
			PRINTLN("[RCC MISSION][Dummy Blip ",iDummyBlip,"] SHOULD_DUMMY_BLIP_BE_HIDDEN - in hide blip range")
			RETURN TRUE
		ENDIF		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_HIDE_BLIP_AFTER_OUTFIT_SWAP)
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_CHANGED_INTO_DISGUISE)
			PRINTLN("[RCC MISSION][Dummy Blip ",iDummyBlip,"] SHOULD_DUMMY_BLIP_BE_HIDDEN - Outfit swapped")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_HIDE_IN_GUN_TURRET)
	AND g_iInteriorTurretSeat > -1
		SET_BIT(iLocalBoolCheck27, LBOOL27_HAVE_HIDDEN_DUMMY_BLIPS)
		PRINTLN("[RCC MISSION][Dummy Blip ",iDummyBlip,"] SHOULD_DUMMY_BLIP_BE_HIDDEN - Player is in turret")
		RETURN TRUE
	ENDIF
	
	IF DOES_DUMMY_BLIP_LINKED_ENTITY_EXIST(iDummyBlip)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitset, ciDUMMY_BLIP_HIDE_WHEN_ENTITY_LINK_EXISTS)
			PRINTLN("[RCC MISSION][Dummy Blip ",iDummyBlip,"] SHOULD_DUMMY_BLIP_BE_HIDDEN - Linked entity exists")
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitset, ciDUMMY_BLIP_HIDE_WHEN_ENTITY_LINK_NOT_EXISTS)				
			PRINTLN("[RCC MISSION][Dummy Blip ",iDummyBlip,"] SHOULD_DUMMY_BLIP_BE_HIDDEN - Linked entity doesn't exist")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF bPedToUseOk
		IF LocalPlayerCurrentInterior != NULL
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitset, ciDUMMY_BLIP_HIDE_WHEN_IN_INTERIOR)			
				PRINTLN("[RCC MISSION][Dummy Blip ",iDummyBlip,"] SHOULD_DUMMY_BLIP_BE_HIDDEN - Player is in an interior")
				RETURN TRUE
			ENDIF
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitset, ciDUMMY_BLIP_HIDE_WHEN_NOT_IN_INTERIOR)
				PRINTLN("[RCC MISSION][Dummy Blip ",iDummyBlip,"] SHOULD_DUMMY_BLIP_BE_HIDDEN - Player isn't in an interior")
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_HIDE_IN_VEHICLE)
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				PRINTLN("[RCC MISSION][Dummy Blip ",iDummyBlip,"] SHOULD_DUMMY_BLIP_BE_HIDDEN - Player is in a vehicle")
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iVehicleRequired != -1
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iVehicleRequired])
				VEHICLE_INDEX viVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iVehicleRequired])
				IF NOT DOES_ENTITY_EXIST(viVeh)
				OR NOT IS_PED_IN_VEHICLE(LocalPlayerPed, viVeh)
					PRINTLN("[RCC MISSION][Dummy Blip ",iDummyBlip,"] SHOULD_DUMMY_BLIP_BE_HIDDEN - Player is not in the specified vehicle")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_HIDE_OPEN_SEA)
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				VEHICLE_INDEX viVeh = GET_VEHICLE_PED_IS_IN(localPlayerPed, FALSE)
				IF IS_VEHICLE_IN_WATER(viVeh)
				AND IS_ENTITY_IN_OPEN_WATER(viVeh)
					PRINTLN("[RCC MISSION][Dummy Blip ",iDummyBlip,"] SHOULD_DUMMY_BLIP_BE_HIDDEN - Player is in a vehicle on deep water")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF SHOULD_DUMMY_BLIP_HIDE_FOR_FLIGHT(iDummyBlip)
			PRINTLN("[RCC MISSION][Dummy Blip ",iDummyBlip,"] SHOULD_DUMMY_BLIP_BE_HIDDEN - Player is in a vehicle on deep water")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DUMMY_BLIP_BE_VISIBLE(INT iDummyBlip, BLIP_RUNTIME_VARS_STRUCT &sBlipRuntimeVars)

	VECTOR vDummyBlipPos = GET_BLIP_COORDS(DummyBlip[iDummyBlip])
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitset, ciDUMMY_BLIP_DISPLAY_ONLY_WHEN_OOB)
		IF IS_LOCAL_PLAYER_INSIDE_BOUNDS(ciRULE_BOUNDS_INDEX__ANY)
			PRINTLN("[RCC MISSION][Dummy Blip ",iDummyBlip,"] SHOULD_DUMMY_BLIP_BE_VISIBLE - NO - Not out of bounds")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT WAS_PLAYER_PREVIOUSLY_IN_RANGE_OF_BLIP(sBlipRuntimeVars)
	
		IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData.fBlipRange != 0.0
		AND g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData.fBlipHeightDifference != 0.0
		
			IF NOT IS_PLAYER_WITHIN_COORD_BLIP_RANGE(vDummyBlipPos, g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData, TRUE)
				PRINTLN("[RCC MISSION][Dummy Blip ",iDummyBlip,"] SHOULD_DUMMY_BLIP_BE_VISIBLE - NO - Not in blip range. Checking both.")
				RETURN FALSE
			ENDIF
			IF NOT IS_COORD_WITHIN_HEIGHT_DIFFERENCE_RANGE_FOR_BLIP(vDummyBlipPos, g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData)
				PRINTLN("[RCC MISSION][Dummy Blip ",iDummyBlip,"] SHOULD_DUMMY_BLIP_BE_VISIBLE - NO - Not in height range. Checking both.")
				RETURN FALSE
			ENDIF
			
		ELIF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData.fBlipRange != 0.0
		
			BOOL bIgnoreZ = NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_3D_RANGE_CHECK)
			IF NOT IS_PLAYER_WITHIN_COORD_BLIP_RANGE(vDummyBlipPos, g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData, bIgnoreZ)
				PRINTLN("[RCC MISSION][Dummy Blip ",iDummyBlip,"] SHOULD_DUMMY_BLIP_BE_VISIBLE - NO - Not in blip range")
				RETURN FALSE
			ENDIF
			
		ELIF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData.fBlipHeightDifference != 0.0
		
			IF NOT IS_COORD_WITHIN_HEIGHT_DIFFERENCE_RANGE_FOR_BLIP(vDummyBlipPos, g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData)
				PRINTLN("[RCC MISSION][Dummy Blip ",iDummyBlip,"] SHOULD_DUMMY_BLIP_BE_VISIBLE - NO - Not in height range")
				RETURN FALSE
			ENDIF
			
		ENDIF
		
		PROCESS_BLIP_PLAYER_IN_RANGE(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData, sBlipRuntimeVars)
		
	ENDIF
	
	IF NOT IS_BLIP_COORD_IN_SAME_INTERIOR(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData, vDummyBlipPos)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_CASINO_HEIST_MISSION_CONTROLLER_BLIPS_HIDDEN)
		PRINTLN("[RCC MISSION][Dummy Blip ",iDummyBlip,"] SHOULD_DUMMY_BLIP_BE_VISIBLE - NO - Blips hidden by LBOOL32_CASINO_HEIST_MISSION_CONTROLLER_BLIPS_HIDDEN")
		RETURN FALSE
	ENDIF
	
	IF SHOULD_DUMMY_BLIP_BE_HIDDEN(iDummyBlip)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_REMOVE_ON_HIDE)
			SET_BIT(iDummyBlipBitset[iDummyBlip], ciDBBS_Remove_On_Hide)
			PRINTLN("[RCC MISSION][Dummy Blip ",iDummyBlip,"] SHOULD_DUMMY_BLIP_BE_VISIBLE - Setting ciDBBS_Remove_On_Hide")
		ENDIF
		PRINTLN("[RCC MISSION][Dummy Blip ",iDummyBlip,"] SHOULD_DUMMY_BLIP_BE_VISIBLE - NO - Blip hidden by SHOULD_DUMMY_BLIP_BE_HIDDEN")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_HIDE_DUMMY_BLIPS_ON_MAP_WHILE_IN_INTERIOR)
		CLEAR_BIT(iLocalBoolCheck35, LBOOL35_HIDE_DUMMY_BLIPS_ON_MAP_WHILE_IN_INTERIOR) //Clear this when we check it as it'll be set again if we need it
		PRINTLN("[RCC MISSION][Dummy Blip ",iDummyBlip,"] SHOULD_DUMMY_BLIP_BE_VISIBLE - NO - Blips hidden by LBOOL35_HIDE_DUMMY_BLIPS_ON_MAP_WHILE_IN_INTERIOR")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_PROCESS_DUMMY_BLIP_THIS_FRAME(INT iDummyBlip)
	
	IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_NEED_TO_CHECK_IN_VEH_FOR_DUMMY_BLIPS)
	AND bPedToUseOk
		
		BOOL bInVeh = IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
		
		IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_WAS_IN_VEH_FOR_DUMMY_BLIPS)
			IF NOT bInVeh
				CLEAR_BIT(iLocalBoolCheck11, LBOOL11_WAS_IN_VEH_FOR_DUMMY_BLIPS)
				RETURN TRUE
			ENDIF
		ELSE
			IF bInVeh
				SET_BIT(iLocalBoolCheck11, LBOOL11_WAS_IN_VEH_FOR_DUMMY_BLIPS)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF g_iInteriorTurretSeat > -1
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_IS_USING_SCRIPTED_GUN_CAM)
	OR IS_BIT_SET(iLocalBoolCheck27, LBOOL27_HAVE_HIDDEN_DUMMY_BLIPS)
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData.iShowBlipPreReqRequired != ciPREREQ_None
	AND NOT DOES_BLIP_EXIST(DummyBlip[iDummyBlip])
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData.iBlipShowZonesBS != 0
	OR g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData.iBlipHideZonesBS != 0
		RETURN TRUE
	ENDIF
	
	IF MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iDummyBlip, eSGET_Dummyblip) AND NOT DOES_BLIP_EXIST(DummyBlip[iDummyBlip])
	OR NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iDummyBlip, eSGET_Dummyblip) AND DOES_BLIP_EXIST(DummyBlip[iDummyBlip])
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DUMMY_BLIP_BE_CREATED(INT iDummyBlip)
	
	IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iRule >= 0
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_MIDPOINT)
	AND NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iTeam], g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iRule)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC ADD_ROUTE_TO_DUMMY_BLIP(INT iDummyBlip)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_GPS_ROUTE)
		EXIT
	ENDIF
	IF DOES_BLIP_HAVE_GPS_ROUTE(DummyBlip[iDummyBlip])
		EXIT
	ENDIF
	SET_BLIP_ROUTE(DummyBlip[iDummyBlip], TRUE)
	INT iBlipColour = GET_DEFAULT_ENTITY_BLIP_COLOUR(FMMC_BLIP_DUMMY)
	IF GET_CORRECT_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData, g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iAggroIndexBS_Entity_DummyBlip) != BLIP_COLOUR_DEFAULT
		iBlipColour = GET_BLIP_COLOUR_FROM_CREATOR(GET_CORRECT_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData, g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iAggroIndexBS_Entity_DummyBlip))
	ENDIF
	
	SET_BLIP_ROUTE_COLOUR(DummyBlip[iDummyBlip], iBlipColour)
ENDPROC

PROC REMOVE_ROUTE_FROM_DUMMY_BLIP(INT iDummyBlip)
	IF NOT DOES_BLIP_HAVE_GPS_ROUTE(DummyBlip[iDummyBlip])
		EXIT
	ENDIF
	
	PRINTLN("[Dummy Blip ",iDummyBlip,"] REMOVE_ROUTE_FROM_DUMMY_BLIP - Removing GPS route")
	SET_BLIP_ROUTE(DummyBlip[iDummyBlip], FALSE)
ENDPROC

FUNC BOOL CREATE_DUMMY_BLIP(INT iDummyBlip, VECTOR vBlipCoord, INT iEntity)
	
	IF NOT DOES_BLIP_EXIST(DummyBlip[iDummyBlip])
		ENTITY_INDEX eiEntity
		IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iType = ciFMMC_DROP_OFF_TYPE_PED
			eiEntity = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iEntity])
		ELIF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iType = ciFMMC_DROP_OFF_TYPE_VEHICLE
			eiEntity = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iEntity])
		ENDIF
		
		CREATE_BLIP_WITH_CREATOR_SETTINGS(
			FMMC_BLIP_DUMMY, 
			DummyBlip[iDummyBlip], 
			g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData, 
			sMissionDummyBlipsLocalVars[iDummyBlip].sBlipRuntimeVars, 
			eiEntity, 
			iDummyBlip, 
			vBlipCoord, 
			g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iAggroIndexBS_Entity_DummyBlip
		)
		
		ADD_ROUTE_TO_DUMMY_BLIP(iDummyBlip)
				
		IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].tlDBName)
			BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MCUSTBLIP")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].tlDBName)
			END_TEXT_COMMAND_SET_BLIP_NAME(DummyBlip[iDummyBlip])
		ENDIF
		
		RETURN DOES_BLIP_EXIST(DummyBlip[iDummyBlip])
		
	ENDIF
	
	RETURN TRUE
	
	
ENDFUNC

PROC PROCESS_DUMMY_BLIP(INT iDummyBlip)
	
	IF NOT SHOULD_PROCESS_DUMMY_BLIP_THIS_FRAME(iDummyBlip)
	 	EXIT
	ENDIF
	
	INT iEntity = -1
	VECTOR vBlip
	
	CLEAR_BIT(iLocalBoolCheck11, LBOOL11_NEED_TO_CHECK_IN_VEH_FOR_DUMMY_BLIPS_TEMP)
		
	SET_DUMMY_BLIP_LOCATION_AND_ENTITY(iDummyBlip, vBlip, iEntity)
	
	IF SHOULD_DUMMY_BLIP_EXIST(iDummyBlip, vBlip, iEntity)	
		IF SHOULD_DUMMY_BLIP_BE_CREATED(iDummyBlip)
			CREATE_DUMMY_BLIP(iDummyBlip, vBlip, iEntity)
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(DummyBlip[iDummyBlip])
			PRINTLN("[RCC MISSION][PROCESS_STAGGERED_DUMMY_BLIPS][Dummy Blip] Removing dummy blip at index ", iDummyBlip, ".")
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_REMOVE_FOR_ALL_PLAYERS)
				BROADCAST_FMMC_REMOVE_DUMMY_BLIP(iDummyBlip)
			ENDIF
			REMOVE_BLIP(DummyBlip[iDummyBlip])
		ENDIF
	ENDIF
		
ENDPROC

PROC PROCESS_DUMMY_BLIP_PRE_EVERY_FRAME()
	iDummyBlipCurrentOverride = -1
	CLEAR_BIT(iLocalBoolCheck26, LBOOL26_DUMMY_BLIP_OVERRIDE)
ENDPROC

FUNC BOOL IS_PLAYER_IN_DUMMY_BLIP_CLEANUP_RANGE(INT iDummyBlip)
	IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iGPSRange = 0
		RETURN FALSE
	ENDIF
	
	IF VDIST2(vLocalPlayerPosition, GET_BLIP_COORDS(DummyBlip[iDummyBlip])) <= POW(TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iGPSRange), 2.0)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_DUMMY_BLIP_GPS(INT iDummyBlip)
	
	IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iGPSRange != 0
	AND IS_PLAYER_IN_DUMMY_BLIP_CLEANUP_RANGE(iDummyBlip)
		REMOVE_ROUTE_FROM_DUMMY_BLIP(iDummyBlip)
	ELSE
		ADD_ROUTE_TO_DUMMY_BLIP(iDummyBlip)
	ENDIF
	
ENDPROC

PROC PROCESS_EVERY_FRAME_DUMMY_BLIPS(INT iDummyBlip)
	
	PROCESS_DUMMY_BLIP(iDummyBlip)
	
	IF DOES_BLIP_EXIST(DummyBlip[iDummyBlip])
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_OVERRIDE_QUICK_GPS)
			SET_BIT(iLocalBoolCheck26, LBOOL26_DUMMY_BLIP_OVERRIDE)
			iDummyBlipCurrentOverride = iDummyBlip
		ENDIF
		
		VECTOR vBlip
		INT iEntity
		
		SET_DUMMY_BLIP_LOCATION_AND_ENTITY(iDummyBlip, vBlip, iEntity)
	
		IF SHOULD_DUMMY_BLIP_BE_VISIBLE(iDummyBlip, sMissionDummyBlipsLocalVars[iDummyBlip].sBlipRuntimeVars)
		AND SHOULD_DUMMY_BLIP_EXIST(iDummyBlip, vBlip, iEntity)
			
			SET_BLIP_DISPLAY(DummyBlip[iDummyBlip], DISPLAY_BLIP)
			
			PROCESS_DUMMY_BLIP_GPS(iDummyBlip)
			
		ELSE
			IF IS_BIT_SET(iDummyBlipBitset[iDummyBlip], ciDBBS_Remove_On_Hide)
				REMOVE_BLIP(DummyBlip[iDummyBlip])
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_REMOVE_FOR_ALL_PLAYERS)
					BROADCAST_FMMC_REMOVE_DUMMY_BLIP(iDummyBlip)
				ENDIF
				PRINTLN("PROCESS_EVERY_FRAME_DUMMY_BLIPS - Removing Dummy Blip as set to remove on hide")
				EXIT
			ELSE
				SET_BLIP_DISPLAY(DummyBlip[iDummyBlip], DISPLAY_NOTHING)
				IF DOES_BLIP_HAVE_GPS_ROUTE(DummyBlip[iDummyBlip])
					SET_BLIP_ROUTE(DummyBlip[iDummyBlip], FALSE)
				ENDIF
			ENDIF
		ENDIF	
		
		PROCESS_ENTITY_BLIP(DummyBlip[iDummyBlip], g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData, GET_BLIP_COORDS(DummyBlip[iDummyBlip]), g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iAggroIndexBS_Entity_DummyBlip)
		
	ENDIF
	
ENDPROC

PROC PROCESS_DUMMY_BLIP_POST_EVERY_FRAME()
	IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_NEED_TO_CHECK_IN_VEH_FOR_DUMMY_BLIPS_TEMP)
		SET_BIT(iLocalBoolCheck10, LBOOL10_NEED_TO_CHECK_IN_VEH_FOR_DUMMY_BLIPS)
	ELSE
		CLEAR_BIT(iLocalBoolCheck10, LBOOL10_NEED_TO_CHECK_IN_VEH_FOR_DUMMY_BLIPS)
	ENDIF
ENDPROC



// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Player Blips
// ##### Description: Functions that interact with the freemode systems to control player blips
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_HIDE_ALL_PLAYER_BLIPS(BOOL bHide)
	g_PlayerBlipsData.bHideAllPlayerBlips = bHide
	PRINTLN("SET_HIDE_ALL_PLAYER_BLIPS - g_PlayerBlipsData.bHideAllPlayerBlips set to ", BOOL_TO_STRING(bHide))
ENDPROC
