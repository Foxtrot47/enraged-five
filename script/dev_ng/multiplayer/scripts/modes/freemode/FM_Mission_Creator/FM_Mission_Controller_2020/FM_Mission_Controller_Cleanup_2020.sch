
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Cleanup -----------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description:
// ##### Script cleanup function and associated helper functions
// ##### Called to cleanup and terminate the script.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Assets_2020.sch"


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Gang Chase
// ##### Description: Cleanup functions for Gang Chase units.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
				
PROC CLEANUP_COLLISION_ON_GANG_CHASE_PED(INT iGangChaseUnit, PED_INDEX piPed)
	
	IF IS_BIT_SET(iGangChasePedCollisionBitset, iGangChaseUnit)
	
		iCollisionStreamingLimit--
		CLEAR_BIT(iGangChasePedCollisionBitset, iGangChaseUnit)
		
		IF iCollisionStreamingLimit < 0
			ASSERTLN("[GANG_CHASE] CLEANUP_COLLISION_ON_GANG_CHASE_PED - Unit: ", iGangChaseUnit, " Collision Streaming Requests < 0!")
			iCollisionStreamingLimit = 0
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(iGangChasePedNavmeshCheckedBS, iGangChaseUnit)
		
		CLEAR_BIT(iGangChasePedNavmeshCheckedBS, iGangChaseUnit)
		
		IF IS_BIT_SET(iGangChasePedNavmeshLoadedBS, iGangChaseUnit)
			
			CLEAR_BIT(iGangChasePedNavmeshLoadedBS, iGangChaseUnit)
			
			IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_NAVMESH_REGION_REQUESTED)
				PRINTLN("[GANG_CHASE] CLEANUP_COLLISION_ON_GANG_CHASE_PED - Unit: ", iGangChaseUnit, " Clearing Navmesh Region Requested")
				REMOVE_NAVMESH_REQUIRED_REGIONS()
				CLEAR_BIT(iLocalBoolCheck8, LBOOL8_NAVMESH_REGION_REQUESTED)
			ELSE
				ASSERTLN("[GANG_CHASE] CLEANUP_COLLISION_ON_GANG_CHASE_PED - Unit: ", iGangChaseUnit, " No Navmesh Region Requested!")
			ENDIF
			
		ENDIF
		
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(piPed)
		EXIT
	ENDIF
	
	IF IS_ENTITY_DEAD(piPed)
		EXIT
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(piPed)
		EXIT
	ENDIF
	
	SET_ENTITY_LOAD_COLLISION_FLAG(piPed, FALSE)
	
ENDPROC

PROC CLEANUP_GANG_CHASE_UNIT(INT iGangChaseUnit)
	
	PRINTLN("[GANG_CHASE] CLEANUP_GANG_CHASE_UNIT - Unit: ", iGangChaseUnit)
	
	INT iPed
	FOR iPed = 0 TO ciMAX_GANG_CHASE_PEDS_PER_VEHICLE - 1
		
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
			//Collision is only loaded on Ped 0
			IF iPed = 0
				CLEANUP_COLLISION_ON_GANG_CHASE_PED(iGangChaseUnit, NULL)
			ENDIF
			
			RELOOP
		ENDIF
		
		//Collision is only loaded on Ped 0
		IF iPed = 0
			CLEANUP_COLLISION_ON_GANG_CHASE_PED(iGangChaseUnit, NET_TO_PED(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[0]))
		ENDIF
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
			DELETE_NET_ID(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
		ELSE
			CLEANUP_NET_ID(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
		ENDIF
		
	ENDFOR
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sGangChase[iGangChaseUnit].niVeh)
		EXIT
	ENDIF
	
	IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sGangChase[iGangChaseUnit].niVeh)
		DELETE_NET_ID(MC_serverBD_1.sGangChase[iGangChaseUnit].niVeh)
	ELSE
		CLEANUP_NET_ID(MC_serverBD_1.sGangChase[iGangChaseUnit].niVeh)
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Model Swaps
// ##### Description: Cleanup functions for Model Swaps.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC CLEANUP_MODEL_SWAPS()
	INT iModelSwap
	
	FOR iModelSwap = 0 TO ciMAX_MODEL_SWAPS - 1
		IF DOES_ENTITY_EXIST(sActiveModelSwaps[iModelSwap].oiModelSwapProp_SwapOut)
			PRINTLN("[ModelSwaps][ModelSwap ", iModelSwap, "] CLEANUP_MODEL_SWAPS | Cleaning up oiModelSwapProp_SwapOut")
			DELETE_OBJECT(sActiveModelSwaps[iModelSwap].oiModelSwapProp_SwapOut)
		ENDIF
		
		IF DOES_ENTITY_EXIST(sActiveModelSwaps[iModelSwap].oiModelSwapProp_SwapIn)
			PRINTLN("[ModelSwaps][ModelSwap ", iModelSwap, "] CLEANUP_MODEL_SWAPS | Cleaning up oiModelSwapProp_SwapIn")
			DELETE_OBJECT(sActiveModelSwaps[iModelSwap].oiModelSwapProp_SwapIn)
		ENDIF
	ENDFOR
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cached Vehicles
// ##### Description: Cleanup functions for cached vehicles.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC CLEANUP_PLAYER_CACHED_VEHICLES()
	INT iPlayerIndex
	FOR iPlayerIndex = 0 TO GET_MAX_PLAYERS() - 1
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD_1[iPlayerIndex].sCachedVehicleData.niCachedVehicle)
		AND IS_ENTITY_ALIVE(NET_TO_ENT(MC_playerBD_1[iPlayerIndex].sCachedVehicleData.niCachedVehicle))
		AND IS_VEHICLE_EMPTY(NET_TO_VEH(MC_playerBD_1[iPlayerIndex].sCachedVehicleData.niCachedVehicle))
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_playerBD_1[iPlayerIndex].sCachedVehicleData.niCachedVehicle)
				DELETE_NET_ID(MC_playerBD_1[iPlayerIndex].sCachedVehicleData.niCachedVehicle)
			ELSE
				CLEANUP_NET_ID(MC_playerBD_1[iPlayerIndex].sCachedVehicleData.niCachedVehicle)
			ENDIF
			
			PRINTLN("[MC_SCRIPT_CLEANUP] CLEANUP_PLAYER_CACHED_VEHICLES | Deleting niCachedVehicle, iPart = ", iPlayerIndex)
		ENDIF
		
		IF MC_playerBD_1[iPlayerIndex].iNumOfCachedVehiclesForCleanup > 0
			INT i
			FOR i = 0 TO MAX_NUM_OF_CACHED_VEHICLES_FOR_CLEANUP - 1
				IF MC_playerBD_1[iPlayerIndex].niCachedVehiclesForCleanup[i] != NULL
				AND NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD_1[iPlayerIndex].niCachedVehiclesForCleanup[i])
				AND IS_ENTITY_ALIVE(NET_TO_ENT(MC_playerBD_1[iPlayerIndex].niCachedVehiclesForCleanup[i]))
				AND IS_VEHICLE_EMPTY(NET_TO_VEH(MC_playerBD_1[iPlayerIndex].niCachedVehiclesForCleanup[i]))
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_playerBD_1[iPlayerIndex].niCachedVehiclesForCleanup[i])
						DELETE_NET_ID(MC_playerBD_1[iPlayerIndex].niCachedVehiclesForCleanup[i])
					ELSE
						CLEANUP_NET_ID(MC_playerBD_1[iPlayerIndex].niCachedVehiclesForCleanup[i])
					ENDIF
					MC_playerBD_1[iPlayerIndex].iNumOfCachedVehiclesForCleanup--
					PRINTLN("[MC_SCRIPT_CLEANUP] CLEANUP_PLAYER_CACHED_VEHICLES | Deleting niCachedVehiclesForCleanup[", i, "], iPart = ", iPlayerIndex)
				ENDIF
			ENDFOR
		ENDIF
	ENDFOR
ENDPROC

PROC CLEANUP_HALLOWEEN_ADVERSARY_2022_SOUNDS()
	INT i
	FOR i = 0 TO ciHALLOWEEN_ADVERSARY_MAX_PLAYERS_PER_TEAM - 1
		IF iEnemyCloseSoundLoop[i] > 0
			STOP_SOUND(iEnemyCloseSoundLoop[i])
			RELEASE_SOUND_ID(iEnemyCloseSoundLoop[i])
			iEnemyCloseSoundLoop[i] = 0
		ENDIF
	ENDFOR
	
	IF iHunterThermalVisionSoundLoop > -1
		STOP_SOUND(iHunterThermalVisionSoundLoop)
		RELEASE_SOUND_ID(iHunterThermalVisionSoundLoop)
		iHunterThermalVisionSoundLoop = -1
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Warp Portals
// ##### Description: Cleanup functions for Warp Portals
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC CLEAN_UP_INSTANCED_CONTENT_PORTAL_WHITEOUT()
	IF NOT IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_PlayedWhiteout)
		EXIT
	ENDIF
	
	CLEAR_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_PlayedWhiteout)
	SET_RECTANGLE_COLOUR(TRUE)
	ACTIVATE_RECTANGLE_FADE_OUT(TRUE)
	ACTIVATE_RECTANGLE_FADE_IN(FALSE)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cutscene Bink and RenderTarget
// ##### Description: Cleanup functions for Cutscenes Bink and RenderTarget
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC CLEANUP_CUTSCENE_BINK_AND_RENDERTARGET()
	IF bmi_Cutscene != null
		RELEASE_BINK_MOVIE(bmi_Cutscene)
	ENDIF
	IF NOT IS_STRING_NULL_OR_EMPTY(sCutsceneRenderTarget)
		RELEASE_NAMED_RENDERTARGET(sCutsceneRenderTarget)
	ENDIF
ENDPROC
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cached Cutscene Peds
// ##### Description: Cleanup functions Cached Cutscene Peds.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC CLEANUP_CACHED_ORIGINAL_PLAYER_PEDS()
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
	OR IS_CORONA_INITIALISING_A_QUICK_RESTART()
	OR IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
		EXIT
	ENDIF
	INT iPlayer, iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		FOR iPlayer = 0 TO (FMMC_MAX_CUTSCENE_PLAYERS-1)
			IF DOES_ENTITY_EXIST(MocapCachedIntroCutscenePlayerPedClone[iTeam][iPlayer])
				PRINTLN("[Player Model Swap] - MC_SCRIPT_CLEANUP - CLEANUP_CACHED_ORIGINAL_PLAYER_PEDS - Deleting Team: ", iTeam, " Ped Clone: ", iPlayer, " (MocapCachedIntroCutscenePlayerPedClone)")
				DELETE_PED(MocapCachedIntroCutscenePlayerPedClone[iTeam][iPlayer])
			ENDIF
			MocapCachedIntroCutscenePlayer[iTeam][iplayer] = INVALID_PLAYER_INDEX()
			
			IF DOES_ENTITY_EXIST(g_sTransitionSessionData.piMocapOriginalPedClones[iTeam][iPlayer])
				PRINTLN("[Player Model Swap] - MC_SCRIPT_CLEANUP - CLEANUP_CACHED_ORIGINAL_PLAYER_PEDS - Deleting Team: ", iTeam, " Ped Clone: ", iPlayer, " (piMocapOriginalPedClones)")
				DELETE_PED(g_sTransitionSessionData.piMocapOriginalPedClones[iTeam][iPlayer])
			ENDIF
			g_sTransitionSessionData.piMocapOriginalPlayer[iTeam][iPlayer] = INVALID_PLAYER_INDEX()
		ENDFOR
	ENDFOR
ENDPROC
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: FMMC2020
// ##### Description: TO SORT
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
PROC CLEANUP_BURNING_VEHICLES()
	
	INT iVeh
	FOR iVeh = 0 TO FMMC_MAX_VEHICLES - 1
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(MC_serverBD_4.sBurningVehicle.sVehicle[iVeh].ptfxVehicleFireFX_Front)
			REMOVE_PARTICLE_FX(MC_serverBD_4.sBurningVehicle.sVehicle[iVeh].ptfxVehicleFireFX_Front)
		ENDIF
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(MC_serverBD_4.sBurningVehicle.sVehicle[iVeh].ptfxVehicleFireFX_Back)
			REMOVE_PARTICLE_FX(MC_serverBD_4.sBurningVehicle.sVehicle[iVeh].ptfxVehicleFireFX_Back)
		ENDIF
		
		IF IS_DECAL_ALIVE(di_RiotVanDecals[iVeh])
			REMOVE_DECAL(di_RiotVanDecals[iVeh])
		ENDIF
		
	ENDFOR
	
ENDPROC

PROC CLEAN_QUICK_GPS(BOOL bPrimaryGPS = TRUE, BOOL bSecondaryGPS = FALSE)
	
	//Note: All Quick GPS options in the PI Menu use this bit: QGPSU_HELI_PICKUP.
	
	IF bPrimaryGPS
		IF NOT IS_VECTOR_ZERO(MPGlobalsAmbience.vQuickGPS)
		AND HAS_QUICK_GPS_BEEN_USED(QGPSU_HELI_PICKUP) 
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[CLEAN_QUICK_GPS] - Clearing Way Point.")
			SET_WAYPOINT_OFF()
		ENDIF
	
		MPGlobalsAmbience.vQuickGPS = <<0,0,0>>
	ENDIF
	
	IF bSecondaryGPS
		IF NOT IS_VECTOR_ZERO(MPGlobalsAmbience.vQuickGPS2)
		AND HAS_QUICK_GPS_BEEN_USED(QGPSU_HELI_PICKUP)
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[CLEAN_QUICK_GPS] - Clearing Way Point.")
			SET_WAYPOINT_OFF()
		ENDIF
		
		MPGlobalsAmbience.vQuickGPS2 = <<0,0,0>>
		MPGlobalsAmbience.tlQuickGPS_Offrule = ""
	ENDIF
	
	MPGlobalsAmbience.bIsGPSDropOff = FALSE
	MPGlobalsAmbience.iQuickGPSTeam = -1
	MPGlobalsAmbience.iQuickGPSPriority = -1	
	CLEAR_QUICK_GPS_BEEN_USED(QGPSU_HELI_PICKUP)
	
ENDPROC

FUNC BOOL SHOULD_DOOR_BE_LOCKED_ON_CLEANUP(INT iDoor)
	UNUSED_PARAMETER(iDoor)
	RETURN TRUE
ENDFUNC

PROC CLEANUP_DOOR(INT iDoor)
	PRINTLN("[Doors][Door ", iDoor, "] CLEANUP_DOORS | Cleaning up door! Hash: ", GET_DOOR_HASH(iDoor), " || SHOULD_DOOR_BE_LOCKED_ON_CLEANUP: ", SHOULD_DOOR_BE_LOCKED_ON_CLEANUP(iDoor))
		
	IF NOT IS_DOOR_REGISTERED(iDoor)
		PRINTLN("[Doors][Door ", iDoor, "] CLEANUP_DOORS | Door isn't registered")
		EXIT
	ENDIF
	
	IF DOES_ENTITY_EXIST(oiCachedDoorObjects[iDoor])
		SET_OBJECT_AS_NO_LONGER_NEEDED(oiCachedDoorObjects[iDoor])
	ENDIF
	
	IF HAS_CONTROL_OF_DOOR(iDoor)
		DOOR_SYSTEM_SET_HOLD_OPEN(GET_DOOR_HASH(iDoor), FALSE)
		DOOR_SYSTEM_SET_SPRING_REMOVED(GET_DOOR_HASH(iDoor), FALSE, IS_DOOR_NETWORKED(iDoor))
		
		DOOR_SYSTEM_SET_OPEN_RATIO(GET_DOOR_HASH(iDoor), 0, IS_DOOR_NETWORKED(iDoor), TRUE)
		DOOR_SYSTEM_SET_DOOR_STATE(GET_DOOR_HASH(iDoor), DOORSTATE_LOCKED, IS_DOOR_NETWORKED(iDoor), FALSE)
	ENDIF
	
	IF IS_BIT_SET(iDoorModelSwappedBS, iDoor)
		PRINTLN("[Doors][Door ", iDoor, "] CLEANUP_DOORS | Removing model swap!")
		REMOVE_MODEL_SWAP(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, 1.0, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel, GET_DOOR_MODEL(iDoor))
	ENDIF
	
	REMOVE_DOOR_FROM_SYSTEM(GET_DOOR_HASH(iDoor), SHOULD_DOOR_BE_LOCKED_ON_CLEANUP(iDoor))
	CLEAR_BIT(iDoorHasBeenInitialisedBS, iDoor)
	
ENDPROC

PROC CLEANUP_DOORS()
	INT iDoor
	FOR iDoor = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDoors - 1		
		CLEANUP_DOOR(iDoor)
	ENDFOR
ENDPROC

PROC CLEANUP_COLLISION_ON_PED(PED_INDEX tempPed,INT iped)

	IF tempPed != NULL
	AND DOES_ENTITY_EXIST(tempPed)
		IF NOT IS_ENTITY_DEAD(tempPed)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
				SET_ENTITY_LOAD_COLLISION_FLAG(tempPed, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(iPedCollisionBitset, iPed)
		IF iCollisionStreamingLimit > 0
			iCollisionStreamingLimit--
		ELSE
			PRINTLN("[CLEANUP_COLLISION_ON_PED] CLEANUP_COLLISION_ON_PED - iCollisionStreamingLimit somehow dropping to below 0, somebody messed up!")
			SCRIPT_ASSERT("[CLEANUP_COLLISION_ON_PED] - iCollisionStreamingLimit somehow dropping to below 0, somebody messed up!")
		ENDIF
		
		FMMC_CLEAR_LONG_BIT(iPedCollisionBitset, iPed)
		PRINTLN("[CLEANUP_COLLISION_ON_PED] Clearing iPedCollisionBitset for ped: ",iped, " current requests: ",iCollisionStreamingLimit)
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(iPedNavmeshCheckedBS, iPed)
		IF FMMC_IS_LONG_BIT_SET(iPedNavmeshLoadedBS, iPed)
			IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_NAVMESH_REGION_REQUESTED)
				PRINTLN("[CLEANUP_COLLISION_ON_PED] CLEANUP_COLLISION_ON_PED - Calling REMOVE_NAVMESH_REQUIRED_REGIONS for ped: ",iped,".")
				REMOVE_NAVMESH_REQUIRED_REGIONS()
				CLEAR_BIT(iLocalBoolCheck8, LBOOL8_NAVMESH_REGION_REQUESTED)
			ELSE
				PRINTLN("[CLEANUP_COLLISION_ON_PED] CLEANUP_COLLISION_ON_PED - iPedNavmeshLoadedBS set, but no navmesh region requested")
				SCRIPT_ASSERT("[CLEANUP_COLLISION_ON_PED] - iPedNavmeshLoadedBS set, but no navmesh region requested!")
			ENDIF
			
			FMMC_CLEAR_LONG_BIT(iPedNavmeshLoadedBS, iPed)
			PRINTLN("[CLEANUP_COLLISION_ON_PED] CLEANUP_COLLISION_ON_PED - Clearing iPedNavmeshLoadedBS for ped: ",iped,".")
		ENDIF
		
		FMMC_CLEAR_LONG_BIT(iPedNavmeshCheckedBS, iPed)
		PRINTLN("[CLEANUP_COLLISION_ON_PED] - Clearing iPedNavmeshCheckedBS for ped: ",iped,".")
	ENDIF
	
ENDPROC

PROC CLEAN_UP_PHYSICAL_BEAM_HACK_ANIM()
	bhaBeamHackAnimState = BHA_STATE_NONE
	STOP_SYNC_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
	CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
	TASK_PLAY_ANIM(LocalPlayerPed, "anim@GangOps@Facility@Servers@", "HOTWIRE_OUTRO", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_HIDE_WEAPON)
	PRINTLN("[BeamHackAnim] Stopping sync scene and playing HOTWIRE_OUTRO because our iObjHacking is -1 but we were still not in BHA_STATE_NONE")
	
	IF IS_SKYSWOOP_AT_GROUND()
		IF NETWORK_IS_GAME_IN_PROGRESS()
			NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
			ENABLE_INTERACTION_MENU() 
			PRINTLN("[BeamHackAnim] Giving player control back!")
		ELSE
			SET_PLAYER_CONTROL(LocalPlayer, TRUE)
			PRINTLN("[BeamHackAnim] Giving player control back!")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_WEAPON_HAS_BEEN_HIDDEN_BY_INTERACTION)
		IF sInteractWithVars.wtInteractWith_StoredWeapon != WEAPONTYPE_INVALID
			SET_CURRENT_PED_WEAPON(LocalPlayerPed, sInteractWithVars.wtInteractWith_StoredWeapon, TRUE)
		ENDIF
		
		CLEAR_BIT(iLocalBoolCheck27, LBOOL27_WEAPON_HAS_BEEN_HIDDEN_BY_INTERACTION)
		PRINTLN("[BeamHackAnim] Putting player's weapon back in their hand")
	ENDIF
ENDPROC

//[FMMC2020] Rename
PROC DELETE_LAPTOP_OBJECT()
	OBJECT_INDEX tempObj
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD[iPartToUse].netMiniGameObj2)
		tempObj = NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2)
		DELETE_OBJECT(tempObj)
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
	ENDIF
ENDPROC

//[FMMC2020] Rename
PROC DELETE_FAKE_PHONE_OBJECT()
	OBJECT_INDEX tempObj
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD[iPartToUse].netMiniGameObj2)
		tempObj = NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2)
		DELETE_OBJECT(tempObj)
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
	Else
		tempObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(LocalPlayerPed), 10, Prop_Phone_ING_03, false, false, false)
		IF DOES_ENTITY_EXIST(tempObj)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
				NETWORK_INDEX niPhone
				niPhone = OBJ_TO_NET(tempObj)
				DELETE_NET_ID(niPhone)
				MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
			ELSE
				NETWORK_REQUEST_CONTROL_OF_ENTITY(tempObj)
			endif
		endif
	endif
ENDPROC

PROC DELETE_HACKING_KEYCARD_OBJECT()
	OBJECT_INDEX tempObj
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD[iPartToUse].netMiniGameObj)
		tempObj = NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj)
		DELETE_OBJECT(tempObj)
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
	ENDIF
ENDPROC

PROC DELETE_KEYCARD_OBJECT()
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD[iPartToUse].netMiniGameObj)
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_playerBD[iPartToUse].netMiniGameObj)
			PRINTLN("[RCC MISSION] DELETE_KEYCARD_OBJECT: deleted keycard object.")
			DELETE_NET_ID(MC_playerBD[iPartToUse].netMiniGameObj)
		ELSE
			PRINTLN("[RCC MISSION] DELETE_KEYCARD_OBJECT: set keycard as no longer needed.")
			CLEANUP_NET_ID(MC_playerBD[iPartToUse].netMiniGameObj)
		ENDIF
		
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
	ENDIF
ENDPROC

PROC CLEAN_UP_MINIGAME_OBJECTS()
	DELETE_FAKE_PHONE_OBJECT()
	DELETE_KEYCARD_OBJECT()
	DELETE_LAPTOP_OBJECT()
ENDPROC

PROC CLEAN_UP_PHYSICAL_FINGERPRINT_KEYPAD_HACK_ANIM()
	PRINTLN("[FingerprintKeypadHackAnim] CLEAN_UP_PHYSICAL_FINGERPRINT_KEYPAD_HACK_ANIM()")
	
	fkhaFingerprintKeypadHackAnimState = FKHA_STATE_NONE
	iFingerprintKeypadForCurrentAnim = -1
	STOP_SYNC_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
	CLEAN_UP_MINIGAME_OBJECTS()
	
	IF IS_SKYSWOOP_AT_GROUND()
		IF NETWORK_IS_GAME_IN_PROGRESS()
			NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
			ENABLE_INTERACTION_MENU() 
			PRINTLN("[FingerprintKeypadHackAnim] Giving player control back!")
		ELSE
			SET_PLAYER_CONTROL(LocalPlayer, TRUE)
			PRINTLN("[FingerprintKeypadHackAnim] Giving player control back!")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_WEAPON_HAS_BEEN_HIDDEN_BY_INTERACTION)
		IF sInteractWithVars.wtInteractWith_StoredWeapon != WEAPONTYPE_INVALID
			SET_CURRENT_PED_WEAPON(LocalPlayerPed, sInteractWithVars.wtInteractWith_StoredWeapon, TRUE)
		ENDIF
		
		CLEAR_BIT(iLocalBoolCheck27, LBOOL27_WEAPON_HAS_BEEN_HIDDEN_BY_INTERACTION)
		PRINTLN("[FingerprintKeypadHackAnim] Putting player's weapon back in their hand")
	ENDIF
	
ENDPROC

PROC HANDLE_FINGERPRINT_KEYPAD_EXIT_HIDING_PROPS()
	
	//Hacking device
	IF IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj))
		IF HAS_ANIM_EVENT_FIRED(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj), HASH("DELETE"))
			SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj), FALSE)
		ENDIF
	ENDIF

	//Fake phone
	IF IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2))
		IF HAS_ANIM_EVENT_FIRED(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), HASH("DELETE"))
			SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), FALSE)
		ENDIF
	ENDIF

ENDPROC

PROC RESET_ROUNDS_MISSION_CELEBRATION_GLOBALS()

	#IF IS_DEBUG_BUILD
	PRINTLN("[NETCELEBRATION] [MSROUND] - RESET_ROUNDS_MISSION_CELEBRATION_GLOBALS")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartRp				= 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRpGained				= 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartLevel			= 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iNextLvl				= 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iEndLvl				= 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRPToReachStartLvl		= 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRptoReachNextLvl		= 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRptoReachEndLvl		= 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iJobPoints				= 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iCash					= 0
ENDPROC

FUNC BOOL SHOULD_PARTICIPANT_KEEP_TASK_AFTER_CUTSCENE(INT iPart)
	
	IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet2, PBBOOL2_TASK_PLAYER_CUTANIM_INTRO)
		PRINTLN("CLEANUP_SCRIPTED_CUTSCENE - SHOULD_PARTICIPANT_KEEP_TASK_AFTER_CUTSCENE - RETURNING TRUE - BIT SET")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_KEEP_TASKS_AFTER_CUTSCENE)
		PRINTLN("CLEANUP_SCRIPTED_CUTSCENE - SHOULD_PARTICIPANT_KEEP_TASK_AFTER_CUTSCENE - RETURNING TRUE - LBOOL29_KEEP_TASKS_AFTER_CUTSCENE IS SET")
		RETURN TRUE
	ENDIF
	
	INT iTeam = MC_PlayerBD[iPart].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam] + 1
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_POST_CUTSCENE_COVER)
			PRINTLN("CLEANUP_SCRIPTED_CUTSCENE - SHOULD_PARTICIPANT_KEEP_TASK_AFTER_CUTSCENE - Ped Should Keep Task IAA - SERVER FARM - RETURNING TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Clear up the cutscene
PROC CLEANUP_SCRIPTED_CUTSCENE(BOOL bFinalCleanUp = FALSE, BOOL bInterpBackToGameplayCam = FALSE, BOOL bClearFade = TRUE, BOOL bSkipRenderScriptCams = FALSE, INT iInterpToGameplayTime = DEFAULT_INTERP_TO_FROM_GAME)
	
	//only clear it up if it's not all ready been!
	IF IS_BIT_SET(iLocalBoolCheck4, LBOOL4_SCRIPTED_CUT_NEEDS_CLEANED_UP)
		PRINTLN("[RCC MISSION] CLEANUP_SCRIPTED_CUTSCENE called")
		
		CLEANUP_CCTV_ASSETS()
		
		STOP_SOUND(iSoundIDCamBackground)
		STOP_SOUND(iSoundPan)
		STOP_SOUND(iSoundZoom)
		STOP_SOUND(iCamSound)
		CLEAR_BIT(iCamSoundBitSet,0)
		CLEAR_BIT(iCamSoundBitSet,1)
		CLEAR_BIT(iCamSoundBitSet,2)
		CLEAR_BIT(iCamSoundBitSet,3)
		
		IF IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_BANK_CCTV_SCENE")
			STOP_AUDIO_SCENE("DLC_HEIST_BANK_CCTV_SCENE")
		ENDIF
		
		HIDE_SPECTATOR_HUD(FALSE)
		HIDE_SPECTATOR_BUTTONS(FALSE)
		
		INT iTeam = GET_LOCAL_PLAYER_TEAM(TRUE)
		IF IS_RULE_INDEX_VALID(GET_TEAM_CURRENT_RULE(iTeam))
		AND NOT SHOULD_FILTERS_BE_PROCESSED(iTeam, GET_TEAM_CURRENT_RULE(iTeam))
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[GET_TEAM_CURRENT_RULE(iTeam)].iFilterFadeOutDuration = 0
		AND NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_TURNED_ON_LIGHTS_OFF_TC_MODIFIER)
			CLEAR_TIMECYCLE_MODIFIER()
		ENDIF
		ENABLE_INTERACTION_MENU()
		
		CLEANUP_MP_CUTSCENE()
		
		IF !bSkipRenderScriptCams
			RENDER_SCRIPT_CAMS(FALSE, bInterpBackToGameplayCam, iInterpToGameplayTime, DEFAULT, TRUE)
		ENDIF
		
		IF DOES_CAM_EXIST(cutscenecam)
			DESTROY_CAM(cutscenecam)
		ENDIF
		
		IF NOT SHOULD_PARTICIPANT_KEEP_TASK_AFTER_CUTSCENE(iLocalPart)
		AND bLocalPlayerPedOK
			PRINTLN("[RCC MISSION] CLEANUP_SCRIPTED_CUTSCENE, CLEARING LOCAL PLAYER PED TASK.")
			CLEAR_PED_TASKS(LocalPlayerPed)
		ENDIF
		
		IF bLocalPlayerPedOK
			IF IS_ENTITY_ATTACHED(LocalPlayerPed)
			AND NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				PRINTLN("[RCC MISSION] CLEANUP_SCRIPTED_CUTSCENE, DETACH_ENTITY")
				DETACH_ENTITY(LocalPlayerPed)
			ENDIF
		ENDIF
		
		IF NOT bFinalCleanUp
		AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
			NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
		ENDIF
		
		IF bLocalPlayerPedOK
			PED_PARACHUTE_STATE eCurrentParachuteState = GET_PED_PARACHUTE_STATE(LocalPlayerPed)
			IF eCurrentParachuteState != PPS_PARACHUTING 
			AND eCurrentParachuteState != PPS_DEPLOYING
			AND NOT IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
				WEAPON_TYPE weapcur
				GET_CURRENT_PED_WEAPON(LocalPlayerPed, weapcur)
				
				IF weapcur != WEAPONTYPE_INVALID
					SET_CURRENT_PED_WEAPON(LocalPlayerPed, weapcur , TRUE)
				ENDIF
				
				SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE)
			ENDIF
		ENDIF
		
		IF bClearFade
			IF (IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT())
			AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
		
		CLEAR_BIT(iLocalBoolCheck7,LBOOL7_CUTSCENE_PHONE_SAFETY_TRIGGERED)
		CLEAR_BIT(iLocalBoolCheck29, LBOOL29_KEEP_TASKS_AFTER_CUTSCENE)
		RESET_NET_TIMER(tdPhoneCutSafetyTimer)
		
		UNLOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS(GET_SCRIPTED_CUTSCENE_LIFT_COORDS(eCSLift))
		eLiftState = eLMS_WaitForStart
		
		NETWORK_SET_VOICE_ACTIVE(true)
		
		iCamShot = 0
		iScriptedCutsceneTeam = -1
		g_iFMMCScriptedCutscenePlaying= -1
		bWarpPlayerAfterScriptCameaSetup = FALSE
		CLEAR_BIT(iLocalBoolCheck4, LBOOL4_SCRIPTED_CUT_NEEDS_CLEANED_UP)
		g_FMMC_STRUCT.bSecurityCamActive = FALSE
		CLEAR_IM_WATCHING_A_MOCAP()
	ENDIF
ENDPROC

PROC CLEANUP_PLAYER_VISUAL_AIDS()

	IF IS_SOUND_ID_VALID(iThermalSoundID)
	AND NOT HAS_SOUND_FINISHED(iThermalSoundID)
		STOP_SOUND(iThermalSoundID)
		RELEASE_SOUND_ID(iThermalSoundID)
	ENDIF
	
	IF IS_SOUND_ID_VALID(iNightVisSoundID)
	AND NOT HAS_SOUND_FINISHED(iNightVisSoundID)
		STOP_SOUND(iNightVisSoundID)
		RELEASE_SOUND_ID(iNightVisSoundID)
	ENDIF
	
	IF IS_PIM_NIGHTVISION_BUTTON_DISABLED()
		DISABLE_PIM_NIGHTVISION_BUTTON(FALSE)
	ENDIF
	CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_MASKS_AND_GEAR)

	SET_PLAYER_VISUAL_AID(VISUALAID_OFF, FALSE)
	SEETHROUGH_RESET()
	CLEAR_BIT(iLocalBoolCheck14, LBOOL14_THERMALVISION_INITIALISED)

	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		MANAGE_VISUALAID_CONTENT_STRUCT s
		MANAGE_VISUALAID_CONTENT (s)
	ENDIF
ENDPROC

PROC CLEAR_LAST_ALIVE_SOUND_EFFECT()
	IF iSoundIDLastAlive > -1
		PRINTLN("[LM][CLEAR_LAST_ALIVE_SOUND_EFFECT] - iSoundIDLastAlive: ", iSoundIDLastAlive)
		STOP_SOUND(iSoundIDLastAlive)
		RELEASE_SOUND_ID(iSoundIDLastAlive)		
		PRINTLN("[LM][CLEAR_LAST_ALIVE_SOUND_EFFECT] - iSoundIDLastAlive: ", iSoundIDLastAlive, " Stopped and released")
		iSoundIDLastAlive = -1
	ENDIF
ENDPROC


PROC CLEANUP_PHOTO_TRACKED_POINT(INT &iTrackedPointID)

	IF ( iTrackedPointID != -1  )
		DESTROY_TRACKED_POINT(iTrackedPointID)
		PRINTLN("Destroyed tracked point with id: ", iTrackedPointID, ".")
		iTrackedPointID = -1
	ENDIF

ENDPROC

PROC RESET_PHOTO_DATA(INT iveh = -1)
	IF iveh = -1
		CLEANUP_PHOTO_TRACKED_POINT(iPhotoTrackedPoint)
	ELSE
		CLEANUP_PHOTO_TRACKED_POINT(iVehPhotoTrackedPoint[iveh])
		CLEANUP_PHOTO_TRACKED_POINT(iVehPhotoTrackedPoint2[iveh])
	ENDIF
ENDPROC

PROC CLEANUP_DYNAMIC_FLARE(INT i)	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlareIndex[i])
		STOP_PARTICLE_FX_LOOPED(ptfxFlareIndex[i])
	ENDIF
	flare_data[i].iFlareState = FLARE_STATE_INIT
	flare_data[i].fNextRed = 0
	flare_data[i].FNextGreen = 0
	flare_data[i].fNextBlue = 0 
	flare_data[i].fRed = 0.0
	flare_data[i].fGreen = 0.0
	flare_data[i].fBlue = 0.0
	flare_data[i].iPTFXval = -1
	
	corona_flare_data[i].iFlareState = FLARE_STATE_INIT
	corona_flare_data[i].fNextRed = 0
	corona_flare_data[i].FNextGreen = 0
	corona_flare_data[i].fNextBlue = 0 
	corona_flare_data[i].fRed = 0.0
	corona_flare_data[i].fGreen = 0.0
	corona_flare_data[i].fBlue = 0.0
	corona_flare_data[i].iPTFXval = -1
	PRINTLN("[RCC MISSION][AW SMOKE] CLEANUP_DYNAMIC_FLARE |   ", i)
	PRINTLN("[RCC MISSION][AW SMOKE] CLEANUP_DYNAMIC_FLARE (CORONA) |   ", i)
	
	iNumCreatedDynamicFlarePTFX--

ENDPROC

PROC CLEANUP_DYNAMIC_FLARES()
	INT i	
	FOR i = 0 TO ciTOTAL_PTFX -1
		CLEANUP_DYNAMIC_FLARE(i)
	ENDFOR
	
	FOR i = 0 TO ((GET_FMMC_MAX_NUM_PROPS() / 32) + 1)-1
		iCreatedFlaresBitset[i] = 0
	ENDFOR
	
	iNumCreatedDynamicFlarePTFX = 0
	iNumCreatedFlarePTFX = 0
ENDPROC

PROC CLEANUP_VEHICLE_CARGO(INT iVeh)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iVeh])
		CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iVeh])
		PRINTLN("[RCC MISSION] CLEANUP_VEHICLE_CARGO - Veh ",iVeh,"'s cargo getting cleaned up (MC_serverBD_1.sFMMC_SBD.niVehicleCrate[", iVeh, "]).")
	ENDIF
ENDPROC

PROC DELETE_VEHICLE_CARGO(INT iVeh)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iVeh])
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iVeh])
			DELETE_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iVeh])
			PRINTLN("[RCC MISSION] DELETE_VEHICLE_CARGO - Veh ",iVeh,"'s cargo getting deleted (MC_serverBD_1.sFMMC_SBD.niVehicleCrate[", iVeh, "]).")
		ELSE
			CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iVeh])
			PRINTLN("[RCC MISSION] DELETE_VEHICLE_CARGO - Veh ",iVeh,"'s cargo getting cleaned up as we don't have control (MC_serverBD_1.sFMMC_SBD.niVehicleCrate[", iVeh, "]).")
		ENDIF
	ENDIF
ENDPROC

PROC DELETE_FMMC_VEHICLE(INT iVeh, BOOL bFinalCleanup = TRUE)
	
	INT iTeam
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		CLEAR_BIT(MC_serverBD.iVehteamFailBitset[iveh],iteam)
		CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iteam], iveh)
	ENDFOR
	
	IF NOT SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRespawnOnRule, -1)
		IF bFinalCleanup
			SET_VEHICLE_RESPAWNS(iVeh, 0)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] DELETE_FMMC_VEHICLE - Setting respawns to 0")
		ENDIF
	ENDIF
	
	START_ENTITY_PTFX_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sEntitySpawnPTFX, GET_ENTITY_COORDS(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]), FALSE), GET_ENTITY_ROTATION(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])))
	
	DELETE_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
	PRINTLN("[Vehicles][Vehicle ", iVeh, "] DELETE_FMMC_VEHICLE - (Deleting) Objective: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupObjective, " Team: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupTeam)
	
	DELETE_VEHICLE_CARGO(iveh)
	
	IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRespawnOnRule, -1)
	AND NOT IS_BIT_SET(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, iVeh)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] DELETE_FMMC_VEHICLE - Setting iServerBS_PassRuleVehAfterRespawn")
		SET_BIT(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, iVeh)
	ENDIF
	
	CLEAR_BIT(MC_serverBD.iVehCleanup_NeedOwnershipBS, iveh)
	
ENDPROC

FUNC BOOL GRAB_CONTROL_OF_ENTIRE_TRAIN(VEHICLE_INDEX viTrain #IF IS_DEBUG_BUILD , INT iTrain #ENDIF)
	
	IF NOT IS_ENTITY_ALIVE(viTrain)
		RETURN FALSE
	ENDIF
	
	INT iCarriage
	FOR iCarriage = 0 TO FMMC_MAX_NUM_OF_TRAIN_CARRIAGES - 1
		VEHICLE_INDEX viCarriage = GET_TRAIN_CARRIAGE(viTrain, iCarriage)
		
		IF DOES_ENTITY_EXIST(viCarriage)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(viCarriage)
				
			ELSE
				PRINTLN("[Trains][Train ", iTrain, "] GRAB_CONTROL_OF_ENTIRE_TRAIN - Grabbing control of carriage ", iCarriage)
				NETWORK_REQUEST_CONTROL_OF_ENTITY(viCarriage)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

PROC DELETE_FMMC_TRAIN(INT iTrain)
	
	VEHICLE_INDEX viTrain = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niTrain[iTrain])
	
	IF NOT GRAB_CONTROL_OF_ENTIRE_TRAIN(viTrain #IF IS_DEBUG_BUILD , iTrain #ENDIF )
		EXIT
	ENDIF
	
//	INT iTeam
//	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
//		CLEAR_BIT(MC_serverBD.iTrainTeamFailBitset[iTrain], iTeam)
//		CLEAR_BIT(MC_serverBD.iMissionCriticalTrain[iTeam], iTrain)
//	ENDFOR

	DELETE_MISSION_TRAIN(viTrain)
	PRINTLN("[Trains][Train ", iTrain, "] DELETE_FMMC_TRAIN - Train getting cleaned up early - delete")
	
	//Delete Cargo here if required
	
	CLEAR_BIT(MC_serverBD.iTrainCleanup_NeedOwnershipBS, iTrain)
	
ENDPROC

FUNC BOOL IS_FORCED_WEAPON_STRAND_BEING_INITIALISED()
	RETURN (IS_JOB_FORCED_WEAPON_ONLY() OR IS_JOB_FORCED_WEAPON_PLUS_PICKUPS()) AND IS_A_STRAND_MISSION_BEING_INITIALISED()
ENDFUNC

PROC CLEANUP_MISSION_FLARE_PTFX()
	INT iPTFX

	FOR iPTFX = 0 TO (ciTOTAL_PTFX-1)
		IF ptfxFlareIndex[iPTFX] != NULL
			IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxFlareIndex[ iPTFX ] )
				STOP_PARTICLE_FX_LOOPED(ptfxFlareIndex[iPTFX])
			ENDIF
			PRINTLN("[RCC MISSION] cleaning up PTFX for end of mission: ",iPTFX)
			ptfxFlareIndex[iPTFX] = NULL
		ENDIF
	ENDFOR
ENDPROC

PROC CLEAR_WATER_CALMING_QUADS()
	
	INT iQuad
	
	FOR iQuad = 0 TO (FMMC_MAX_WATER_CALMING_QUADS - 1)
		IF iWaterCalmingQuad[iQuad] != -1
			REMOVE_EXTRA_CALMING_QUAD(iWaterCalmingQuad[iQuad])
			iWaterCalmingQuad[iQuad] = -1
		ENDIF
	ENDFOR
	
ENDPROC

//PURPOSE: Clears the render targets to their blank start states
PROC CLEANUP_RENDERTARGETS()
	PRINTLN("RC MISSION Cleanup_RenderTargets() called")
	IF IS_NAMED_RENDERTARGET_REGISTERED("tvscreen")
		RELEASE_NAMED_RENDERTARGET("tvscreen")
		PRINTLN("RC MISSION Cleanup_RenderTargets() named render target tvscreen found & released")
	ENDIF
ENDPROC

PROC CLEANUP_NAMED_RENDERTARGET(STRING sRenderTarget)
	IF IS_NAMED_RENDERTARGET_REGISTERED(sRenderTarget)
		RELEASE_NAMED_RENDERTARGET(sRenderTarget)
		PRINTLN("[RCC MISSION] CLEANUP_NAMED_RENDERTARGET | Releasing ", sRenderTarget)
	ENDIF
ENDPROC

PROC CLEAN_UP_SPAWN_VEHICLE_BLIPS()
	CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Cleaning up spawn vehicle blips.")

	// Remove spawn vehicle blips.
	IF DOES_BLIP_EXIST(biSpawnVehicle)
		REMOVE_BLIP(biSpawnVehicle)
		CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Spawn vehicle blip deleted.")
	ENDIF
	
	// Dereference our vehicle index.
	vehSpawnVehicle = NULL
	CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Spawn vehicle index dereferenced.")
	
	// Reset the flag to maintain the blip.
	bManageSpawnVehicleBlip = FALSE
	CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Spawn vehicle management flag cleared.")
ENDPROC

PROC REMOVE_PLAYER_CUSTOM_BLIP_SPRITE()

	INT iParticipant

	
	PRINTLN("[PLAYER_LOOP] - REMOVE_PLAYER_CUSTOM_BLIP_SPRITE")
	FOR iParticipant = 0 TO MAX_NUM_MC_PLAYERS -1
	
		IF iParticipant < MAX_NUM_MC_PLAYERS
			PARTICIPANT_INDEX currentParticipant = INT_TO_PARTICIPANTINDEX(iParticipant)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(currentParticipant)
				PLAYER_INDEX currentPlayer = NETWORK_GET_PLAYER_INDEX(currentParticipant)
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iParticipant].iteam].iTeamBitset4, ciBS4_TEAM_1_UsingNewBlipSpriteData+MC_playerBD[iParticipant].iteam)
					SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(currentPlayer, INT_TO_ENUM(BLIP_SPRITE, g_FMMC_STRUCT.iTeamBlipType[MC_playerBD[iParticipant].iteam]), FALSE)
					FORCE_BLIP_PLAYER(localPlayer, FALSE, FALSE)
					PRINTLN("CLEARING CUSTOM BLIP SPRITE FOR PLAYER - ", g_FMMC_STRUCT.iTeamBlipType[MC_playerBD[iParticipant].iteam], " | iParticipant: ", iParticipant, " | iTeam: ", MC_playerBD[iParticipant].iteam)
				ELSE				
					IF g_FMMC_STRUCT.iTeamBlipType[MC_playerBD[iParticipant].iteam] = TEAM_BLIP_RED_SKULL					
						SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(currentPlayer, RADAR_TRACE_BOUNTY_HIT, FALSE)
						FORCE_BLIP_PLAYER(localPlayer, FALSE, FALSE)
						PRINTLN("CLEARING CUSTOM BLIP SPRITE FOR PLAYER - SKULL | iParticipant: ", iParticipant, " | iTeam: ", MC_playerBD[iParticipant].iteam)
					ELIF g_FMMC_STRUCT.iTeamBlipType[MC_playerBD[iParticipant].iteam] = TEAM_BLIP_HOLDING_PACKAGE	
						SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(currentPlayer, RADAR_TRACE_RACEFLAG, FALSE)
						FORCE_BLIP_PLAYER(localPlayer, FALSE, FALSE)
						PRINTLN("CLEARING CUSTOM BLIP SPRITE FOR PLAYER - PACKAGE | iParticipant: ", iParticipant, " | iTeam: ", MC_playerBD[iParticipant].iteam)
					ELIF g_FMMC_STRUCT.iTeamBlipType[MC_playerBD[iParticipant].iteam] = TEAM_BLIP_BEAST	
						SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(currentPlayer, RADAR_TRACE_BEAST, FALSE)
						FORCE_BLIP_PLAYER(localPlayer, FALSE, FALSE)
						PRINTLN("CLEARING CUSTOM BLIP SPRITE FOR PLAYER - BEAST | iParticipant: ", iParticipant, " | iTeam: ", MC_playerBD[iParticipant].iteam)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC CLEANUP_ALTERED_RADIO_FOR_MC()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_KEEP_RADIO_STATION_ON_RESPAWN)
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Cleaning up altered radio.")		
		SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(FALSE)
		SET_MOBILE_PHONE_RADIO_STATE(FALSE)
		SET_AUDIO_FLAG("MobileRadioInGame", FALSE)
		SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)		
		SET_RADIO_TO_STATION_INDEX(255)		
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			IF DOES_PLAYER_VEH_HAVE_RADIO()
				SET_VEH_RADIO_STATION(GET_VEHICLE_PED_IS_IN(localPlayerPed), GET_RADIO_STATION_NAME(255))
			ENDIF
		ENDIF
	ENDIF
	
	LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", "FIXER_AP_LOWRIDERS_MIX")
ENDPROC

PROC CLEANUP_PLACED_PTFX_ASSOCIATED_SOUND_FX(INT iPTFX)
	TEXT_LABEL_63 tl63_EffectName = g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl63EffectName
	TEXT_LABEL_63 tl63_SoundBank
	TEXT_LABEL_63 tl63_SoundName
	TEXT_LABEL_63 tl63_SoundSet
	
	BUILD_PLACED_PTFX_ASSOCIATED_SOUND_FX(tl63_EffectName, tl63_SoundBank, tl63_SoundName, tl63_SoundSet)
	
	IF sPlacedPtfxLocal[iPTFX].iPTFXSoundID > -1
		STOP_SOUND(sPlacedPtfxLocal[iPTFX].iPTFXSoundID)
		RELEASE_SOUND_ID(sPlacedPtfxLocal[iPTFX].iPTFXSoundID)
		sPlacedPtfxLocal[iPTFX].iPTFXSoundID = -1
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(tl63_SoundBank)
	AND NOT ARE_STRINGS_EQUAL(tl63_SoundBank, "DLC_HEIST3/CASINO_HEIST_FINALE_GENERAL_01")	
		RELEASE_NAMED_SCRIPT_AUDIO_BANK(tl63_SoundBank)
	ENDIF
ENDPROC

PROC CLEANUP_PLACED_PTFX()
	INT iPTFX
	FOR iPTFX = 0 TO FMMC_MAX_PLACED_PTFX-1	
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sPlacedPtfx[iPTFX].vStartCoord)
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sPlacedPtfx[iPTFX].tl63AssetName)
				PRINTLN("[LM][Placed_PTFX] MC_SCRIPT_CLEANUP - CLEANUP_PLACED_PTFX - iPTFX: ", iPTFX, " removing named asset: ", g_FMMC_STRUCT.sPlacedPtfx[iPTFX].tl63AssetName)
				REMOVE_NAMED_PTFX_ASSET(g_FMMC_STRUCT.sPlacedPtfx[iPTFX].tl63AssetName)
			ENDIF
			
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sPlacedPtfxLocal[iPTFX].ptfxID)
				PRINTLN("[LM][Placed_PTFX] MC_SCRIPT_CLEANUP - CLEANUP_PLACED_PTFX - iPTFX: ", iPTFX, " Stopping PTFX: ", g_FMMC_STRUCT.sPlacedPtfx[iPTFX].tl63EffectName)
				STOP_PARTICLE_FX_LOOPED(sPlacedPtfxLocal[iPTFX].ptfxID)
			ENDIF
			
			//If option is on RESET_PARTICLE_FX_OVERRIDE
		ENDIF
		CLEANUP_PLACED_PTFX_ASSOCIATED_SOUND_FX(iPTFX)
	ENDFOR
ENDPROC

PROC CLEANUP_EXTRA_ENTITY_PTFX()
	INT iDynopropLoop	
	FOR iDynopropLoop = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps-1
		RELEASE_EXTRA_PTFX_FOR_ENTITY_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynopropLoop].mn)
	ENDFOR
	
	INT iObjectLoop	
	FOR iObjectLoop = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects-1
		RELEASE_EXTRA_PTFX_FOR_ENTITY_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectLoop].mn)
	ENDFOR
	
	INT iInteractableLoop	
	FOR iInteractableLoop = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables-1
		RELEASE_EXTRA_PTFX_FOR_ENTITY_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractableLoop].mnInteractable_Model)
	ENDFOR
ENDPROC

PROC CLEANUP_INTERACTABLE_INTERACTIONS()
	INT iInteractable
	FOR iInteractable = 0 TO FMMC_MAX_NUM_INTERACTABLES - 1
		PRINTLN("[Interactables] CLEANUP_INTERACTABLE_INTERACTIONS | Cleaning up Interactable ", iInteractable, "'s interaction. iInteractable_InteractionType: ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_InteractionType)
	
		SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_InteractionType
			CASE ciInteractableInteraction_VoltageMinigame
				VOLTAGE_CLEANUP(sVoltageHack[GET_HACKING_STRUCT_INDEX_TO_USE(iInteractable, ciENTITY_TYPE_INTERACTABLES)], sVoltageGameplay)
			BREAK
			
			CASE ciInteractableInteraction_FingerPrintMinigame
				CLEAN_UP_FINGERPRINT_CLONE(sFingerprintCloneGameplay, sFingerprintClone[GET_HACKING_STRUCT_INDEX_TO_USE(iInteractable, ciENTITY_TYPE_INTERACTABLES)], TRUE, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
			BREAK
		ENDSWITCH
		
		MC_UNLOAD_INTERACTABLE_EXTRA_ASSETS(iInteractable)
	ENDFOR
ENDPROC

PROC CLEAR_ALL_LARGE_PICKUP_COPIES()
	INT i
	
	FOR i = 0 TO FMMC_MAX_WEAPONS - 1
		IF DOES_ENTITY_EXIST(oiLargePowerups[i])
			DELETE_OBJECT(oiLargePowerups[i])
		ENDIF
	ENDFOR
ENDPROC

PROC DELETE_INTERACT_WITH_SPAWNED_PROP(INT iSpawnedProp)
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD_1[iLocalPart].niInteractWith_CurrentSpawnedProps[iSpawnedProp])
		DELETE_NET_ID(MC_playerBD_1[iLocalPart].niInteractWith_CurrentSpawnedProps[iSpawnedProp])
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
		PRINTLN("[InteractWith_SpawnedProps] Deleting the spawned Interact-With prop at index ", iSpawnedProp)
		MC_PlayerBD_1[iLocalPart].iInteractWith_NumSpawnedProps--
	ENDIF
ENDPROC

PROC CLEANUP_HEALTH_DRAIN_ZONE(BOOL bFinalCleanup = TRUE)
	IF bFinalCleanup
		REMOVE_ANIM_DICT("anim@fidgets@coughs")
		REMOVE_ANIM_SET("anim@fidgets@coughs")
	ENDIF
	
	fHealthDrainZoneDamageBuildup = 0.0
ENDPROC

PROC CLEANUP_ZONES()

	INT iZone
	FOR iZone = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfZones - 1
		SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType
			CASE ciFMMC_ZONE_TYPE__FIRE_ROCKETS_AT_PLAYER
				IF HAS_WEAPON_ASSET_LOADED(GET_FIRE_ROCKETS_ZONE_WEAPON_TYPE(iZone))
					REMOVE_WEAPON_ASSET(GET_FIRE_ROCKETS_ZONE_WEAPON_TYPE(iZone))
					PRINTLN("[Zones][Zone ", iZone, "][ZoneRockets] Releasing Fire Rocket Zone asset!")
				ENDIF
			BREAK
			
			CASE ciFMMC_ZONE_TYPE__SET_AIR_DRAG_MULTIPLIER
				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(LocalPlayer, 1.0)
			BREAK
			
			CASE ciFMMC_ZONE_TYPE__HEALTH_DRAIN
				CLEANUP_HEALTH_DRAIN_ZONE(TRUE)
			BREAK
			
			CASE ciFMMC_ZONE_TYPE__RACE_PIT_ZONE
				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(LocalPlayer, 1.0)
				SET_LOCAL_PLAYER_AS_GHOST(FALSE)
				
				IF iRacePitZoneSoundID > -1
					STOP_SOUND(iRacePitZoneSoundID)
					iRacePitZoneSoundID = -1
				ENDIF
			BREAK
			
			CASE ciFMMC_ZONE_TYPE__PETROL_POOL
				IF IS_DECAL_ALIVE(diPetrolDecals[iZone])
					REMOVE_DECAL(diPetrolDecals[iZone])
				ENDIF
			BREAK
			
		ENDSWITCH
	ENDFOR
ENDPROC

PROC CLEANUP_BOUNDS()

	INT iBoundsIndex
	FOR iBoundsIndex = 0 TO ciMAX_RULE_BOUNDS_PER_RULE - 1
		RESET_RULE_BOUNDS_OUT_OF_BOUNDS_VARS(iBoundsIndex)
	ENDFOR
	
ENDPROC

PROC CLEANUP_INTERACT_WITH_PERSISTENT_PROPS()
	INT iIWPropPart = -1
	INT iIWProp = 0
	
	WHILE DO_PARTICIPANT_LOOP(iIWPropPart, DPLF_IGNORE_ACTIVE_CHECKS)
		FOR iIWProp = 0 TO ciINTERACT_WITH__MAX_PERSISTENT_PROPS - 1
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_PlayerBD_1[iIWPropPart].niInteractWith_PersistentProps[iIWProp])
			AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_PlayerBD_1[iIWPropPart].niInteractWith_PersistentProps[iIWProp])
				DELETE_NET_ID(MC_PlayerBD_1[iIWPropPart].niInteractWith_PersistentProps[iIWProp])
				PRINTLN("[InteractWith_PersistentProp] Deleting MC_PlayerBD_1[", iIWPropPart, "].niInteractWith_PersistentProps[", iIWProp, "]")
			ENDIF
		ENDFOR
	ENDWHILE
ENDPROC

PROC RELEASE_RESTRICTED_PICKUP_MODELS()
	IF NETWORK_IS_PLAYER_ACTIVE(LocalPlayer)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP")), TRUE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_BMD")), TRUE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_SHARK")), TRUE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MK_THERMAL")), TRUE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_WEED")), TRUE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MK_S_TIME")), TRUE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME")), TRUE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_HIDDEN")), TRUE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MK_RANDOM")), TRUE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_P")), TRUE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_BMD_P")), TRUE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_SHARK_P")), TRUE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_WEED_P")), TRUE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_P")), TRUE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_HIDDEN_P")), TRUE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_P")), TRUE)
		//SET_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_OF_TYPE(LocalPlayer, PICKUP_CUSTOM_SCRIPT, TRUE)
	ENDIF
ENDPROC

PROC UNLOAD_MISSION_SPECIFIC_MODELS_AND_ENTITIES()
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - UNLOAD_MISSION_SPECIFIC_MODELS_AND_ENTITIES ################################")
	INT i
	SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("w_lr_rpg_rocket")))
	IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VEHICLE_ROCKET_ASSET_REQUEST)
		REMOVE_WEAPON_ASSET(g_wtPickupRocketType)
	ENDIF
	
	RELEASE_RESTRICTED_PICKUP_MODELS()
	
	INT iObj
	INTERACT_WITH_PARAMS sCleanupParams
	
	IF sInteractWithVars.eInteractWith_CurrentState != IW_STATE_IDLE
		IF MC_playerBD_1[iLocalPart].iInteractWithSyncedScene != -1
			PRINTLN("[InteractWith] UNLOAD_MISSION_SPECIFIC_MODELS_AND_ENTITIES | Stopping iInteractWithSyncedScene")
			STOP_SYNC_SCENE(MC_playerBD_1[iLocalPart].iInteractWithSyncedScene, TRUE)
		ENDIF
	ENDIF
	
	FOR iObj = 0 TO FMMC_MAX_NUM_OBJECTS - 1
	
		SET_BIT(sCleanupParams.iInteractWithParamsBS, ciINTERACT_WITH_PARAMS_BS__IsObjectiveMinigame)
		sCleanupParams.iObjectiveObjectIndex = iObj
		sCleanupParams.iInteractWith_AnimPreset = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
		CACHE_ANIM_INFO_FOR_INTERACT_WITH(sCleanupParams)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sInteractWithVars.sIWAnims.sIW_AnimDict)
			REMOVE_ANIM_DICT(sInteractWithVars.sIWAnims.sIW_AnimDict)
		ENDIF
		IF NOT IS_STRING_NULL_OR_EMPTY(GET_INTERACT_WITH_PLAYER_FAILSAFE_ANIM_DICT(sCleanupParams))
			REMOVE_ANIM_DICT(GET_INTERACT_WITH_PLAYER_FAILSAFE_ANIM_DICT(sCleanupParams))
		ENDIF
		
		INT iSpawnedProp
		FOR iSpawnedProp = 0 TO (GET_CURRENT_INTERACT_WITH_NUM_PROPS_TO_SPAWN(sCleanupParams) - 1)
			IF GET_MODEL_NAME_FOR_CURRENT_INTERACT_WITH_SPAWNED_PROP(sCleanupParams, iSpawnedProp) != DUMMY_MODEL_FOR_SCRIPT
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_MODEL_NAME_FOR_CURRENT_INTERACT_WITH_SPAWNED_PROP(sCleanupParams, iSpawnedProp))
				PRINTLN("[InteractWith_SpawnedProps] UNLOAD_MISSION_SPECIFIC_MODELS_AND_ENTITIES | Setting prop model for iObj ", iObj, " as no longer needed")
			ENDIF
		ENDFOR
	ENDFOR
	
	FOR i = 0 TO ciINTERACT_WITH__MAX_SPAWNED_PROPS - 1
		DELETE_INTERACT_WITH_SPAWNED_PROP(i)
	ENDFOR
	
	CLEANUP_INTERACT_WITH_PERSISTENT_PROPS()
	
	CLEAR_ALL_LARGE_PICKUP_COPIES()
	
	IF g_FMMC_STRUCT.sPedAmmoDrop.iAmmoPickupType != FMMC_PED_AMMO_DROP_TYPE_INVALID
		SET_MODEL_AS_NO_LONGER_NEEDED(mnPedAmmoDropModel)
		PRINTLN("UNLOAD_MISSION_SPECIFIC_MODELS_AND_ENTITIES - Setting ped ammo drop model as no longer needed")
	ENDIF
	
	CLEANUP_SPECIFIC_CUTSCENE_REFERENCES()
				
ENDPROC

PROC CLEANUP_MUSIC_LOCKER_LIGHTS()
	INT iMusicLockerLight
	FOR iMusicLockerLight = 0 TO ciMAX_MUSIC_LOCKER_LIGHTS-1
		IF DOES_ENTITY_EXIST(oiMusicLockerLights[iMusicLockerLight])
			DELETE_OBJECT(oiMusicLockerLights[iMusicLockerLight])
		ENDIF
	ENDFOR
ENDPROC

PROC UNLOAD_MISSION_SPECIFIC_INTERIORS_IPLS()
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - UNLOAD_MISSION_SPECIFIC_INTERIORS_IPLS ################################")
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
	ENDIF
	
	IF IPL_GROUP_SWAP_IS_ACTIVE()
		IPL_GROUP_SWAP_CANCEL()
	ENDIF
	
	CLEANUP_SPECIFIC_CUTSCENE_FUNCTIONALITY(sMocapCutscene)
	
	CLEANUP_CASINO_APARTMENT_SETUP(interiorCasinoApartmentIndex)
	
	CLEANUP_MUSIC_LOCKER_SETUP(iInteriorIndexMusicLocker)
	CLEANUP_MUSIC_LOCKER_LIGHTS()
	
	CLEANUP_INTERIOR_COLLISION_FIXES(sInteriorFixStruct)
	
	IF DOES_ENTITY_EXIST(oiBunkerEntryFrame)
		DELETE_OBJECT(oiBunkerEntryFrame)
	ENDIF
	
	IF DOES_ENTITY_EXIST(oibunkerEntryDoor)
		DELETE_OBJECT(oibunkerEntryDoor)
	ENDIF
	
	IF eExternalScriptedCutsceneSequenceState != EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_IDLE
		PRINTLN("[ExternalCutscene][MC_SimpleCutscene][Bunker] - EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP Starting Load Cutscene for in Veh.")
		CLEANUP_BUNKER_CUSTOM_CUTSCENE(simpInteriorCache, entryAnimExtCut)
	ENDIF
	
	IF eExternalScriptedCutsceneSequenceState != EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_IDLE
		SIMPLE_CUTSCENE_CLEAR_ALL_DATA(g_SimpleInteriorData.sCustomCutsceneExt)
		CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iBS, BS_SIMPLE_CUTSCENE_GLOBAL_PLAYER_BD_PLAYING)
		GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iCutsceneHash = 0
	ENDIF
		
	IF NOT (IS_CORONA_INITIALISING_A_QUICK_RESTART()
	AND NOT IS_THIS_A_ROUNDS_RESTART())
	OR IS_FORCE_ABANDON_CURRENT_MP_MISSION_SET()
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Removing IPLs as not a quick restart")
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_FORT_ZANCUDO_GATES)
			REQUEST_IPL("cs3_07_mpgates")
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - adding ford zancudo gates ipl")
		ELSE
			REMOVE_IPL("cs3_07_mpgates")
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - removing ford zancudo gates ipl")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FARMHOUSE)	
			SET_INTERIOR_CAPPED(INTERIOR_V_FARMHOUSE, TRUE)
			REMOVE_IPL("farmint")
			REQUEST_IPL("farmint_cap")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_ABATTOIR)	
			SET_INTERIOR_CAPPED(INTERIOR_V_ABATTOIR, TRUE)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_CORONER)	
			CLEAN_UP_INTERIOR(INTERIOR_V_CORONER)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_RECYCLEPLANT)	
			SET_INTERIOR_CAPPED(INTERIOR_V_RECYCLE, TRUE)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_UNION_DEPOSITORY)
			CLEAN_UP_INTERIOR(INTERIOR_V_UNION_DEPOSITORY)
			CLEAN_UP_INTERIOR(INTERIOR_DT1_03_CARPARK)
		ENDIF	
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_BUNKER)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_BUNKER)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)

			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STANDARD_SET)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Standard_Bunker_Set")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Standard_Bunker_Set")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Standard_Bunker_Set\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_UPGRADE_SET)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Upgrade_Bunker_Set")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Upgrade_Bunker_Set")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Upgrade_Bunker_Set\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STYLE_A)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Bunker_Style_A")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Bunker_Style_A")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Bunker_Style_A\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STYLE_B)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Bunker_Style_B")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Bunker_Style_B")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Bunker_Style_B\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STYLE_C)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Bunker_Style_C")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Bunker_Style_C")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Bunker_Style_C\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_OFFICE_UPGRADE_SET)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Office_Upgrade_Set")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Office_Upgrade_Set")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Office_Upgrade_Set\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_OFFICE_BLOCKER_SET)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Office_Blocker_Set")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Office_Blocker_Set")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Office_Blocker_Set\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_GUN_RANGE_BLOCKER_SET)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Gun_Range_Blocker_Set")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Gun_Range_Blocker_Set")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Gun_Range_Blocker_Set\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_GUN_LOCKER_UPGRADE)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Gun_locker_upgrade")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Gun_locker_upgrade")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Gun_locker_upgrade\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STANDARD_SECURITY_SET)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Standard_Security_Set")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Standard_Security_Set")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Standard_Security_Set\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_SECURITY_UPGRADE)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Security_upgrade")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Security_upgrade")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Security_upgrade\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_GUN_WALL_BLOCKER)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Gun_wall_blocker")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Gun_wall_blocker")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Gun_wall_blocker\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciBUNKER_STANDARD_SET_MORE)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Standard_Bunker_Set_More")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Standard_Bunker_Set_More")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Standard_Bunker_Set_More\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciBUNKER_UPGRADE_SET_MORE)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Upgrade_Bunker_Set_More")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Upgrade_Bunker_Set_More")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Upgrade_Bunker_Set_More\"")
				ENDIF
			ENDIF
			
			SET_INTERIOR_DISABLED(INTERIOR_V_BUNKER, TRUE)
			UNPIN_INTERIOR(iInteriorIndex)
			
			//Audio Scene
			IF IS_AUDIO_SCENE_ACTIVE("DLC_GR_Bunker_Interior")
				STOP_AUDIO_SCENE("DLC_GR_Bunker_Interior")
				PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - STOP_AUDIO_SCENE(\"DLC_GR_Bunker_Interior\"")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SUB)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SUB)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SUB, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_IAA)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_IAA)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_IAA, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_IAA_COVER_POINTS)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "IAA_CoverPoints_01")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "IAA_CoverPoints_01")
					PRINTLN("ACTIVATE_INTERIOR_ENTITY_SET - IAA_CoverPoints_01")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_LIFEINVADER)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_LIFEINVADER)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_LIFEINVADER, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
			
			REMOVE_IPL("facelobby")
			REQUEST_IPL("facelobbyfake")
			
		ENDIF
				
		IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_SILO)
			REMOVE_IPL("xm_siloentranceclosed_x17")
			REMOVE_IPL("xm_hatches_terrain")
			REMOVE_IPL("xm_hatch_closed")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FOUNDRY)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_FOUNDRY)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_CAPPED(INTERIOR_V_FOUNDRY, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_01)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SILO_1)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SILO_1, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_02)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SILO_2)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SILO_2, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_03)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SILO_3)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SILO_3, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SERVER_FARM)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SERVER_FARM)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SERVER_FARM, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_TUNNEL)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SILO_TUNNEL)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SILO_TUNNEL, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_LOOP)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SILO_LOOP)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SILO_LOOP, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_ENTRANCE)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SILO_ENTRANCE)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SILO_ENTRANCE, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_BASE)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SILO_BASE)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SILO_BASE, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_OSPREY)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_OSPREY)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_OSPREY, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_HANGAR)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_HANGAR)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_HANGAR, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_IMPORT_WAREHOUSE)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_IMPORT_WAREHOUSE)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_IMPORT_WAREHOUSE, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_WAREHOUSE_UNDRGRND_FACILITY)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_WAREHOUSE_UNDRGRND_FACILITY)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_WAREHOUSE_UNDRGRND_FACILITY, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SPECIAL_CARGO_WAREHOUSE_S)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_S)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_S, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SPECIAL_CARGO_WAREHOUSE_M)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_M)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_M, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SPECIAL_CARGO_WAREHOUSE_L)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_L)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_L, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_STILT_APARTMENT)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_STILT_APARTMENT)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_STILT_APARTMENT, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_HIGH_END_APARTMENT)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_HIGH_END_APARTMENT)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_HIGH_END_APARTMENT, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_MEDIUM_END_APARTMENT)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_MEDIUM_END_APARTMENT)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_MEDIUM_END_APARTMENT, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_LOW_END_APARTMENT)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_LOW_END_APARTMENT)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_LOW_END_APARTMENT, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FIB_OFFICE)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_FIB_OFFICE)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_FIB_OFFICE, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
			
			INT i
			FOR i = 0 TO ciFIB_OFFICE_NUM_PROPS - 1
				IF DOES_ENTITY_EXIST(oiFIBOfficeProps[i])
					DELETE_OBJECT(oiFIBOfficeProps[i])
				ENDIF
			ENDFOR
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FIB_OFFICE_2)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_FIB_OFFICE_2)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_FIB_OFFICE_2, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR_FIB_LOBBY)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_FIB_LOBBY)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_FIB_LOBBY, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
			
			IF IS_IPL_ACTIVE("fiblobby")
				REMOVE_IPL("fiblobby")
				REQUEST_IPL("fiblobbyfake")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR_HIGH_END_GARAGE)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_HIGH_END_GARAGE)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_HIGH_END_GARAGE, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR_MEDIUM_GARAGE)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_GARAGEM)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			BLOCK_PROPERTY_BOUNDS_CHECK_LAUNCH(FALSE)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEM, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR_SMALL_GARAGE)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_GARAGES)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_GARAGES, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_ENTRANCE)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_ENTRY)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_STRAIGHT)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_STRAIGHT_0)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_STRAIGHT_1)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_STRAIGHT_2)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_FLAT_SLOPE)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_SLOPE_FLAT_0)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_SLOPE_FLAT_1)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_SLOPE_FLAT_2)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_FLAT_SLOPE_2)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_FLAT_SLOPE_0)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_FLAT_SLOPE_1)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_FLAT_SLOPE_2)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_30D_R)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_RIGHT_0)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_RIGHT_1)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_RIGHT_2)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_RIGHT_3)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_RIGHT_4)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_30D_L)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_LEFT_0)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_LEFT_1)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_LEFT_2)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_LEFT_3)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_LEFT_4)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_LEFT_5)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_WEED_FARM)
			CLEAN_UP_INTERIOR(INTERIOR_V_WEED_FARM)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_ALT_WEED_FARM)
			CLEAN_UP_INTERIOR(INTERIOR_V_ALT_WEED_FARM)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_COCAINE_FACTORY)
			CLEAN_UP_INTERIOR(INTERIOR_V_COCAINE_FACTORY)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_METH_FACTORY)
			CLEAN_UP_INTERIOR(INTERIOR_V_METH_FACTORY)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_CUSTOM_MISSION_MOC)
			CLEAN_UP_INTERIOR(INTERIOR_V_CUSTOM_MISSION_MOC)
			DELETE_MISSION_MOC_PROPS(sMissionMOCProps)
			CLEANUP_NAMED_RENDERTARGET(GET_RENDER_TARGET(AT_COMMAND_CENTER_TV_01))
			CLEANUP_NAMED_RENDERTARGET(GET_RENDER_TARGET(AT_TRAILER_MONITOR_01))
			CLEANUP_NAMED_RENDERTARGET(GET_RENDER_TARGET(AT_TRAILER_MONITOR_02))
			CLEANUP_NAMED_RENDERTARGET(GET_RENDER_TARGET(AT_TRAILER_MONITOR_03))
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_METH_FACTORY_MURRIETA)
			CLEAN_UP_INTERIOR(INTERIOR_V_METH_FACTORY_MURRIETA)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_METH_FACTORY_EAST_VINEWOOD)
			CLEAN_UP_INTERIOR(INTERIOR_V_METH_FACTORY_EAST_VINEWOOD)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_METH_FACTORY_SENORA_DESERT_1)
			CLEAN_UP_INTERIOR(INTERIOR_V_METH_FACTORY_SENORA_DESERT_1)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_METH_FACTORY_SENORA_DESERT_2)
			CLEAN_UP_INTERIOR(INTERIOR_V_METH_FACTORY_SENORA_DESERT_2)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_TUNER_CAR_MEET)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNER_CAR_MEET)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_TUNER_MOD_GARAGE)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNER_MOD_GARAGE)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_RECORDING_STUDIO)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_RECORDING_STUDIO)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Entity_Set_Fire")
			DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "entity_set_fix_stu_ext_p1")
			DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "entity_set_fix_trip1_int_p2")
			CLEAN_UP_INTERIOR(INTERIOR_V_RECORDING_STUDIO)
			
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MUSIC_LOCKER)
			CLEAN_UP_INTERIOR(INTERIOR_V_MUSIC_LOCKER)
			
			SET_STATIC_EMITTER_ENABLED("SE_h4_dlc_int_02_h4_Bogs", FALSE)
			SET_STATIC_EMITTER_ENABLED("SE_h4_dlc_int_02_h4_Entrance_Doorway", FALSE)
			SET_STATIC_EMITTER_ENABLED("SE_h4_dlc_int_02_h4_lobby", FALSE)
			SET_STATIC_EMITTER_ENABLED("SE_h4_dlc_int_02_h4_main_bar", FALSE)
			SET_STATIC_EMITTER_ENABLED("SE_h4_dlc_int_02_h4_main_front_01", FALSE)
			SET_STATIC_EMITTER_ENABLED("SE_h4_dlc_int_02_h4_main_front_02", FALSE)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_DEVIN_HANGAR_DOOR)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_DEVIN_HANGAR)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
				PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - unpinning hangar interior")
			ENDIF
			REMOVE_IPL("sf_dlc_fixer_hanger_door")
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Removing sf_dlc_fixer_hanger_door")
			REMOVE_MODEL_HIDE(<<-1011.643,-2983.132,25.444>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("ap1_01_b_shadowonly")))
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_OFFICE_AUTOSHOP)
			CLEAN_UP_INTERIOR(INTERIOR_V_OFFICE_AUTOSHOP)
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_HIGH_END_GARAGE_NEW)
			CLEAN_UP_INTERIOR(INTERIOR_V_GARAGEL)
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MOTEL)
			CLEAN_UP_INTERIOR(INTERIOR_V_MOTEL)
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_ARENA_WAR_GARAGE_LEVEL_1)
			CLEAN_UP_INTERIOR(INTERIOR_V_ARENA_WAR_GARAGE_LEVEL_1)	
			IF IS_IPL_ACTIVE("xs_arena_interior_mod_2")
				REMOVE_IPL("xs_arena_interior_mod_2")
				PRINTLN("MC_SCRIPT_CLEANUP - Removing xs_arena_interior_mod_2")
			ENDIF
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_CUSTOM_HIGH_END_APARTMENT)
			BLOCK_PROPERTY_BOUNDS_CHECK_LAUNCH(FALSE)
			CLEAN_UP_INTERIOR(INTERIOR_V_CUSTOM_B_4)	
			IF IS_IPL_ACTIVE("apa_v_mp_h_04_b")
				REMOVE_IPL("apa_v_mp_h_04_b")
				PRINTLN("MC_SCRIPT_CLEANUP - Removing apa_v_mp_h_04_b")
			ENDIF
		ENDIF
		IF IS_MISSION_BASEMENT_INTERIOR_ENABLED()
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_RECORDING_STUDIO)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			TURN_OFF_INTERIOR_ENTITY_SET(iInteriorIndex, GET_INTERIOR_SET_FOR_MISSION_BASEMENT(g_FMMC_STRUCT.iMissionBasement))
			
			CLEAN_UP_INTERIOR(INTERIOR_V_MISSION_BASEMENT)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_PHARMA_OFFICE)
			CLEAN_UP_INTERIOR(INTERIOR_V_PHARMA_OFFICE)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_AUTOCARE_WAREHOUSE)
			CLEAN_UP_INTERIOR(INTERIOR_V_AUTOCARE_WAREHOUSE)
		ENDIF
	
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - CLEANUP_CASINO_INTERIORS")
			g_TransitionSessionNonResetVars.iFMMCiInteriorBS2 = 0
			g_TransitionSessionNonResetVars.iIPLOptions = 0
			CLEANUP_STRAND_INTERIORS()
		ELSE
			g_TransitionSessionNonResetVars.iFMMCiInteriorBS2 = g_FMMC_STRUCT.iInteriorBS2
			g_TransitionSessionNonResetVars.iIPLOptions = g_FMMC_STRUCT.iIPLOptions
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - iFMMCiInteriorBS2 = ", g_TransitionSessionNonResetVars.iFMMCiInteriorBS2)
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - iIPLOptions = ", g_TransitionSessionNonResetVars.iIPLOptions)
		ENDIF
		
		g_bMissionIPLsRequested = FALSE
		
	#IF IS_DEBUG_BUILD
	ELSE
		IF IS_CORONA_INITIALISING_A_QUICK_RESTART()
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Keeping IPLs as this is a quick restart")
		ENDIF
		IF IS_THIS_A_ROUNDS_RESTART()
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Keeping IPLs as we're going into another round")
		ENDIF
	#ENDIF
	ENDIF
	
	REQUEST_IPL("cs1_02_cf_offmission")
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		REMOVE_IPL("cs1_02_cf_onmission1")
		REMOVE_IPL("cs1_02_cf_onmission2")
		REMOVE_IPL("cs1_02_cf_onmission3")
		REMOVE_IPL("cs1_02_cf_onmission4")
	ENDIF
	
	IF NOT IS_IPL_ACTIVE("chemgrill_grp1")
		REQUEST_IPL("chemgrill_grp1")
	ENDIF
	
	IF IS_IPL_ACTIVE("sf_heli_crater")
		REMOVE_IPL("sf_heli_crater")
	ENDIF
	
	CLEAR_WATER_CALMING_QUADS()
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF IS_BIT_SET(iLocalBoolCheck19, LBOOL19_YACHT_AQUARIS_ASSETS_REQUESTED)
			YACHT_DATA YachtData    
			YachtData.iLocation = 34            // Paleto Bay
			YachtData.Appearance.iOption = 2    // The Aquaris
			YachtData.Appearance.iTint = 12     // Vintage
			YachtData.Appearance.iLighting = 5 // Blue Vivacious
			YachtData.Appearance.iRailing = 1   // Gold Fittings
			YachtData.Appearance.iFlag = -1     // no flag 
			
			DELETE_AND_CLEANUP_ASSETS_FOR_MISSION_CREATOR_YACHT(YachtData)
			
			CLEAR_BIT(iLocalBoolCheck19, LBOOL19_YACHT_AQUARIS_ASSETS_REQUESTED)
			CLEAR_BIT(iLocalBoolCheck19, LBOOL19_YACHT_AQUARIS_ASSETS_LOADED)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_BIG_MAP_WAS_ZOOMED_IN_FOR_CARRIER)
		SET_RADAR_ZOOM_PRECISE(0)
		CLEAR_BIT(iLocalBoolCheck11, LBOOL11_BIG_MAP_WAS_ZOOMED_IN_FOR_CARRIER)
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_ROOF)
		SET_RADAR_ZOOM_PRECISE(0)
		CLEAR_BIT(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_ROOF)
	ENDIF
	
	CLEAR_ALL_TCMODIFIER_OVERRIDES("lab_none_exit")
	CLEAR_ALL_TCMODIFIER_OVERRIDES("lab_none_dark")
	CLEAR_ALL_TCMODIFIER_OVERRIDES("morgue_dark")
	CLEAR_TIMECYCLE_MODIFIER()
	IF DOES_ENTITY_EXIST(vehPrizedVehicle)
		DELETE_VEHICLE(vehPrizedVehicle)
	ENDIF
	
	CLEANUP_MISSION_FACTORIES(sMissionFactories)
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		INT iYachtIndex
		FOR iYachtIndex = 0 TO ciYACHT_MAX_YACHTS - 1
			DELETE_AND_CLEANUP_ASSETS_FOR_MISSION_CREATOR_YACHT(sMissionYachtData[iYachtIndex], FALSE)
			CLEANUP_FMMC_YACHT(iYachtIndex)
		ENDFOR
		
		MPGlobalsPrivateYacht.bFMMC_RemoveAllYachtIPLS = TRUE
	ENDIF
	CLEAR_LOCAL_PLAYER_BD_DESIRED_YACHT_LOCATION()
	CLEAR_LOCAL_PLAYER_BD_DESIRED_YACHT_ID()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_CASINO_AIR_CONDITIONING_UNIT)
		PRINTLN("[RCC MISSION] REQUEST_IPL - ch_dlc_casino_aircon_broken")
		IF NOT IS_IPL_ACTIVE("ch_dlc_casino_aircon_broken")
			REQUEST_IPL("ch_dlc_casino_aircon_broken")	
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_BUNKER_DOORS)
		REQUEST_OR_REMOVE_BUNKER_DOOR_IPLS(FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_LOST_MC_CLUB_HOUSE)
		IF IS_IPL_ACTIVE("bkr_bi_hw1_13_int")
			REMOVE_IPL("bkr_bi_hw1_13_int")
			REQUEST_IPL("hei_bi_hw1_13_door")
		ENDIF
	ENDIF
	
	IF eExternalScriptedCutsceneSequenceState != EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_IDLE		
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Emergancy Cleanup for Bunker Scene. We bailed during it.")
		CLEANUP_BUNKER_CUSTOM_CUTSCENE(simpInteriorCache, entryAnimExtCut)
	ENDIF
	
	CLEAR_GLOBAL_FLAG_BIT(eGLOBALFLAGBITSET_USING_CASINO_MISSION_INTERIOR)
	
ENDPROC

//[FMMC2020] Check out this
PROC RESET_ALL_HACKING_MINIGAMES(BOOL bSkipControl = FALSE)
	RESET_SAFE_CRACK(SafeCrackData, TRUE)
	CLEAR_SAFE_CRACK_SEQUENCES(SafeCrackData)
	REMOVE_ANIM_DICT("mp_common_miss")
	RESET_NET_TIMER(tdHackTimer)
	ENABLE_INTERACTION_MENU()
	IF NOT g_b_OnLeaderboard
		CLEANUP_MENU_ASSETS()
	ENDIF
	FORCE_QUIT_PASS_HACKING_MINIGAME(sHackingData, TRUE)
	IF NOT bSkipControl
		IF bLocalPlayerPedOk
			NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
			PRINTLN("[RCC MISSION] RESET_ALL_HACKING_MINIGAMES() - Returning player control.")
			CLEAR_PED_TASKS(LocalPlayerPed)
		ENDIF
	ENDIF
	
	PRINTLN("[RCC MISSION] SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)")
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	
	IF MC_playerBD[iLocalPart].iObjHacking != -1
		CLEAR_BIT(MC_playerBD[iLocalPart].iHackFailBitset, MC_playerBD[iLocalPart].iObjHacking)
		PRINTLN("[RCC MISSION] Resetting iObjHacking, part: ", iPartToUse, " for object: ", MC_playerBD[iLocalPart].iObjHacking)
		INT iHackStructToUse
		FOR iHackStructToUse = 0 TO (ciMAX_HACKING_STRUCTS_MAX - 1)
			IF iHackStructToUse < ciMAX_HACKING_STRUCTS_DEFAULT
				CLEAN_UP_BEAM_HACK_MINIGAME(sBeamHack[iHackStructToUse])
				CLEAN_UP_HOTWIRE_MINIGAME(sHotwire[iHackStructToUse])
				CLEAN_UP_ORDER_UNLOCK(sOrderUnlockGameplay, sOrderUnlock[iHackStructToUse], DEFAULT, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
			ENDIF
			
			IF iHackStructToUse < ciMAX_HACKING_STRUCTS_FINGERPRINT
				CLEAN_UP_FINGERPRINT_CLONE(sFingerprintCloneGameplay, sFingerprintClone[iHackStructToUse], DEFAULT, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
			ENDIF
			
			IF iHackStructToUse < ciMAX_HACKING_STRUCTS_VOLTAGE
				VOLTAGE_CLEANUP(sVoltageHack[iHackStructToUse], sVoltageGameplay)
			ENDIF
		ENDFOR
	ENDIF
	
	CLEAR_OBJECT_HACKING()
ENDPROC

PROC CLEAN_UP_NEW_HACKING_GAME_SOUNDS()
	
	PRINTLN("[HACKING] CLEAN_UP_NEW_HACKING_GAME_SOUNDS()")
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,0)
		IF iNewMiniGameSoundID[0] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[0])
				STOP_SOUND(iNewMiniGameSoundID[0])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[0])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[0]")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,1)
		IF iNewMiniGameSoundID[1] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[1])
				STOP_SOUND(iNewMiniGameSoundID[1])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[1])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[1]")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,2)
		IF iNewMiniGameSoundID[2] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[2])
				STOP_SOUND(iNewMiniGameSoundID[2])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[2])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[2]")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,3)
		IF iNewMiniGameSoundID[3] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[3])
				STOP_SOUND(iNewMiniGameSoundID[3])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[3])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[3]")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,4)
		IF iNewMiniGameSoundID[4] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[4])
				STOP_SOUND(iNewMiniGameSoundID[4])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[4])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[4]")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,5)
		IF iNewMiniGameSoundID[5] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[5])
				STOP_SOUND(iNewMiniGameSoundID[5])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[5])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[5]")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,6)
		IF iNewMiniGameSoundID[6] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[6])
				STOP_SOUND(iNewMiniGameSoundID[6])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[6])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[6]")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,7)
		IF iNewMiniGameSoundID[7] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[7])
				STOP_SOUND(iNewMiniGameSoundID[7])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[7])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[7]")
			ENDIF
		ENDIF
	ENDIF

	
ENDPROC

PROC RUN_HACKING_CLEANUP(BOOL bBlockAudioRelease = FALSE)
	SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
	IF MC_playerBD[iPartToUse].iObjHacking !=-1
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
			CLEAR_BIT(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
		ENDIF
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iBeginHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
			CLEAR_BIT(MC_playerBD[iPartToUse].iBeginHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
		ENDIF
	ENDIF
	
	CLEAR_OBJECT_HACKING()
	CLEANUP_MENU_ASSETS()
	FORCE_QUIT_FAIL_HACKING_MINIGAME(sHackingData)
	RESET_HACKING_DATA(sHackingData, bBlockAudioRelease)
	IF bLocalPlayerPedOK
		CLEAR_PED_TASKS(LocalPlayerPed)
		TASK_CLEAR_LOOK_AT(LocalPlayerPed)
	ENDIF
	NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
	ENABLE_INTERACTION_MENU()
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Force fail and clean up hacking minigame due to player death.  ",iPartToUse)
	iHackLimitTimer = 0
	iHackProgress = 0
ENDPROC

PROC DELETE_MONEY_OBJECT()
	OBJECT_INDEX tempObj
	IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[1])
		tempObj = NET_TO_OBJ(niMinigameObjects[1])
		DELETE_OBJECT(tempObj)
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
	ENDIF
ENDPROC

PROC DELETE_BAG_OBJECT()
	OBJECT_INDEX tempObj
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[0])
		tempObj = NET_TO_OBJ(niMinigameObjects[0])
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niMinigameObjects[0])
			PRINTLN("[AW_MISSION] - [RCC MISSION] DELETE_BAG_OBJECT: deleted bag object.")
		
			DELETE_OBJECT(tempObj)
		ELSE
			PRINTLN("[AW_MISSION] - [RCC MISSION] DELETE_BAG_OBJECT: set bag as no longer needed.")
		
			CLEANUP_NET_ID(niMinigameObjects[0])
		ENDIF
		
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
	ENDIF
ENDPROC

PROC PROCESS_CASH_GRAB_FAIL_CLEANUP(INT iObj, OBJECT_INDEX oiCashGrab, STRING sCashExitAnimName, BOOL bForceCleanup = FALSE)
	IF MC_playerBD[iLocalPart].iObjHacking = iObj
	OR bForceCleanup
		IF (MC_playerBD[iLocalPart].iObjHacking = iObj AND IS_BIT_SET(MC_playerBD[iLocalPart].iHackFailBitset, MC_playerBD[iLocalPart].iObjHacking))
		OR bForceCleanup
							
			DELETE_BAG_OBJECT()
			DELETE_MONEY_OBJECT()
			
			IF DOES_CAM_EXIST(sCashGrabData.cameraIndex)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			ENDIF
			
			//start trolley synchronised scene to have it perform the same anim it would perform if you exited safely
			IF DOES_ENTITY_EXIST(oiCashGrab)
				NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(oiCashGrab, sCashGrabData.iTrolleySceneID,
														 sCashGrabData.sBagAnimDict, sCashExitAnimName,
														 INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiCashGrab)
			ENDIF
			NETWORK_START_SYNCHRONISED_SCENE(sCashGrabData.iTrolleySceneID)
		
			CLEANUP_CASH_GRAB_SCRIPTED_CAMERA(sCashGrabData)
			STOP_AUDIO_SCENE("DLC_HEIST_MINIGAME_PAC_CASH_GRAB_SCENE")
			
			IF bLocalPlayerPedOK
				CLEAR_PED_TASKS(LocalPlayerPed)
				TASK_CLEAR_LOOK_AT(LocalPlayerPed)
			ENDIF
			
			NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)

			SET_CURRENT_PED_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame, TRUE)
			APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)

			REINIT_NET_TIMER(tdHackTimer)

			ENABLE_INTERACTION_MENU()
			RESET_CASH_GRAB_MINIGAME_DATA(sCashGrabData)
			FORCE_QUIT_FAIL_CASH_GRAB_MINIGAME_KEEP_DATA(sCashGrabData)
			
			IF MC_playerBD[iLocalPart].iObjHacking = iObj
				CLEAR_BIT(MC_playerBD[iLocalPart].iHackFailBitset, MC_playerBD[iLocalPart].iObjHacking)
			ENDIF
			
			CLEAR_OBJECT_HACKING()
			
			IF NOT bForceCleanup
				PRINTLN("[CashGrab] PROCESS_CASH_GRAB_FAIL_CLEANUP - Local player has failed cash grab minigame due to being smacked, part: ", iLocalPart, ". Running fail cleanup 3.")
			ELSE
				PRINTLN("[CashGrab] PROCESS_CASH_GRAB_FAIL_CLEANUP - Forced cleanup, likely a player death.")
			ENDIF

			iHackProgress 	= 0
			iHackLimitTimer = 0
			
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_OBJECT_INVENTORIES()
	
	iInventoryObjectIndex = -1
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_HAS_OBJECT_INVENTORY)
		PRINTLN("CLEANUP_OBJECT_INVENTORIES - Clearing the player's Inventory as they are no longer holding Obj!")
		REMOVE_ALL_PLAYERS_WEAPONS()
		GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_STARTING, WEAPONINHAND_LASTWEAPON_BOTH)
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_HAS_OBJECT_INVENTORY)
	ENDIF
	
ENDPROC

PROC CLEANUP_CACHED_VEHICLE_FLAGS()
	CLEAR_BIT(iLocalBoolCheck27, LBOOL27_PLAYER_WEAPON_DAMAGE_MODIFIER_APPLIED)
	CLEAR_BIT(iLocalBoolCheck27, LBOOL27_CACHED_VEHICLE_RANGE_CHECK_SKIP_BLIP_COLOUR)
	CLEAR_BIT(iLocalBoolCheck27, LBOOL27_PLAYER_IN_RANGE_OF_THE_CACHED_VEHICLE)
	CLEAR_BIT(iLocalBoolCheck27, LBOOL27_CAN_PROCESS_CACHED_VEH_HEALING_EVENTS)	
ENDPROC

PROC CLEANUP_MULTI_VEHICLE_SEAT_SWAP(BOOL bDirtyCleanup = FALSE)
	PRINTLN("[LM][MULTI_VEH_RESPAWN][CLEANUP_MULTI_VEHICLE_SEAT_SWAP][SWAP_SEAT] - CLEANUP_MULTI_VEHICLE_SEAT_SWAP called")
	iCachedNewVehSeatPref = -3
	CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_INITIATED_VEHICLE_SEAT_SWAP)
	CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE)	
	CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_FINAL)
	CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_DONE)
	CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_GAVE_CONSENT)
	
	RESET_NET_TIMER(tdVehicleSeatSwapConsentTimer)
	RESET_NET_TIMER(tdVehicleSeatSwapSafetyTimer)
	
	// Fixes assert.
	IF NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		SET_ENTITY_COLLISION(localPlayerPed, TRUE)
	ENDIF
	
	SET_ENTITY_INVINCIBLE(localPlayerPed, FALSE)
	SET_ENTITY_VISIBLE(localPlayerPed, TRUE)
	
	RESET_NET_TIMER(tdVehicleSeatSwapCooldownTimer)
	START_NET_TIMER(tdVehicleSeatSwapCooldownTimer)
	
	CLEAR_HELP()
	
	g_bMissionSeatSwapping = FALSE
	
	// These values HAVE to default if we perform a dirty cleanup. This will sadly cause the vehicle swapping order to return to normal. May have to put this in an event to sync people up with the same values.
	IF bDirtyCleanup
		PRINTLN("[LM][MULTI_VEH_RESPAWN][CLEANUP_MULTI_VEHICLE_SEAT_SWAP][SWAP_SEAT] - PERFORMED DIRTY CLEANUP, RESETTING A BUNCH OF VALUES!")
		iCachedPartOnTeamVehicleRespawn = -1
	ENDIF
ENDPROC

PROC RESET_VEHICLE_RELATED_CONFIGS()	
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - RESET_VEHICLE_RELATED_CONFIGS ################################")

	//Cleanup Don't Fall Off Bike
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciBIKES_DONT_WIPE_OUT) 
	AND NOT IS_PED_INJURED(LocalPlayerPed)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
			IF IS_PED_ON_ANY_BIKE(LocalPlayerPed)
				SET_BIKE_EASY_TO_LAND(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), FALSE)
				PRINTLN("MC - CLEARING EASY TO LAND")
			ENDIF
		ENDIF
	ENDIF
	
	SET_VEHICLE_SLIPSTREAMING_SHOULD_TIME_OUT(TRUE)
	
	IF DOES_ENTITY_EXIST(LocalPlayerPed)
		IF bLocalPlayerPedOK
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableAutoEquipHelmetsInBikes, GET_MP_INT_CHARACTER_STAT(MP_STAT_AUTO_EQUIP_HELMET)=0)
			CPRINTLN(DEBUG_AMBIENT, "[RCC MC_SCRIPT_CLEANUP] default PCF_DisableAutoEquipHelmetsInBikes=",GET_STRING_FROM_BOOL(GET_MP_INT_CHARACTER_STAT(MP_STAT_AUTO_EQUIP_HELMET)=0))
		
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableAutoEquipHelmetsInAicraft, GET_MP_INT_CHARACTER_STAT(MP_STAT_AUTO_EQUIP_HELMET)=0)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset3, ciBS3_TEAM_PLAYER_PREVENT_FALLS_OUT_WHEN_KILLED)
				IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SAVING_FALLOUT_FLAG)
					SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_FallsOutOfVehicleWhenKilled, TRUE)
					CLEAR_BIT(iLocalBoolCheck29, LBOOL29_SAVING_FALLOUT_FLAG)
					SET_BIT(iLocalBoolCheck29, LBOOL29_SAVING_FALLOUT_FLAG)
				ELSE
					SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_FallsOutOfVehicleWhenKilled, FALSE)
					CLEAR_BIT(iLocalBoolCheck29, LBOOL29_SAVING_FALLOUT_FLAG)
				ENDIF
			ENDIF
						
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_BlockAutoSwapOnWeaponPickups, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(LocalPlayerPed)
		IF bLocalPlayerPedOK
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_GetOutUndriveableVehicle, TRUE)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_GetOutBurningVehicle, TRUE)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_RunFromFiresAndExplosions, TRUE)
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableAutoForceOutWhenBlowingUpCar, FALSE)
	
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PhoneDisableTextingAnimations, TRUE)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PhoneDisableTalkingAnimations, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(LocalPlayerPed)
		IF bLocalPlayerPedOK
			SET_PED_CONFIG_FLAG(LocalPlayerPed,PCF_PlayersDontDragMeOutOfCar, FALSE)
		ENDIF
	ENDIF
		
	IF NOT IS_PED_DEAD_OR_DYING(LocalPlayerPed)
	AND bLocalPlayerPedOK
	AND DOES_ENTITY_EXIST(localPlayerPed)
		SET_PED_CONFIG_FLAG(localPlayerPed, PCF_GetOutUndriveableVehicle, TRUE)
		SET_PED_CONFIG_FLAG(localPlayerPed, PCF_GetOutBurningVehicle, TRUE)
	ENDIF
	
	IF (g_FMMC_STRUCT.iTeamVehicleModCountermeasure_AmmoPerLife[MC_playerBD[iLocalPart].iTeam] > 0) OR (g_FMMC_STRUCT.iTeamVehicleModCountermeasure_Cooldown[MC_playerBD[iLocalPart].iTeam] > 0)
		RESET_COUNTERMEASURE_OVERRIDES()
	ENDIF	
			
	IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_VEHICLE_WEAPONS_HOMING_DISABLED)
		SET_PLAYER_HOMING_DISABLED_FOR_ALL_VEHICLE_WEAPONS(localPlayer, FALSE)
		CLEAR_BIT(iLocalBoolCheck25, LBOOL25_VEHICLE_WEAPONS_HOMING_DISABLED)
		
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP -  Clearing Blocked Homing Missiles.")
	ENDIF	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset3, ciBS3_PREFER_FRONT_SEATS)
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - ciBS3_PREFER_FRONT_SEATS is set. Resetting: PCF_PlayerPreferFrontSeatMP")
		
		IF DOES_ENTITY_EXIST(LocalPlayerPed)
			IF  bLocalPlayerPedOK
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PlayerPreferFrontSeatMP, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF PERFORMING_VEHICLE_DRIVEBY_SEQUENCE()
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_ForcedAim, FALSE)
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Cleaning up Vehicle Driveby Sequence")
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_VEHICLE_SWAP_RETRACTABLE_WHEEL_BUTTON)
		SET_VEHICLE_USE_BOOST_BUTTON_FOR_WHEEL_RETRACT(FALSE)
	ENDIF	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_VEHICLE_TARGETTING_RETICULE)
		USE_VEHICLE_TARGETING_RETICULE(FALSE)
	ENDIF
	CLEAR_VALID_VEHICLE_HIT_HASHES()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_DisableCollisionBetweenCarParachutesAndCars)
		SET_DISABLE_COLLISIONS_BETWEEN_CARS_AND_CAR_PARACHUTE(FALSE)
	ENDIF
	
	CLEANUP_ALTERED_RADIO_FOR_MC()
ENDPROC


PROC CLEAN_UP_BEAST_MODE(BOOL bFinalCleanup = FALSE, BOOL bDoHealthCleanup = TRUE, BOOL bResetClothing = FALSE)
	//Reset flags
	IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_IN_BEAST_MODE)
		CLEAR_BIT(iLocalBoolCheck14, LBOOL14_IN_BEAST_MODE)
	ENDIF
	IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_BEAST_STEALTH_TOGGLE)
		CLEAR_BIT(iLocalBoolCheck14, LBOOL14_BEAST_STEALTH_TOGGLE)
	ENDIF
	IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_BEAST_IS_STEALTHED)
		CLEAR_BIT(iLocalBoolCheck14, LBOOL14_BEAST_IS_STEALTHED)
	ENDIF	
	IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_IN_BEAST_COSTUME)
		CLEAR_BIT(iLocalBoolCheck14, LBOOL14_IN_BEAST_COSTUME)
	ENDIF
	IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_OTHER_TEAM_IN_BEAST_MODE)
		CLEAR_BIT(iLocalBoolCheck14, LBOOL14_OTHER_TEAM_IN_BEAST_MODE)
	ENDIF
	bFMMCHealthBoosted = FALSE
	MC_playerBD[iLocalPart].iBeastAlpha = 255
	IF NOT IS_BIT_SET(mc_playerBD[iLocalPart].iClientBitSet3,PBBOOL3_SPAWN_PROTECTION_ON)
	AND NOT IS_PLAYER_IN_TEMP_PASSIVE_MODE(LocalPlayer)
		SET_ENTITY_ALPHA(LocalPlayerPed, MC_playerBD[iLocalPart].iBeastAlpha , FALSE)
	ENDIF
	SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(localPlayer, 1.0)
	SET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(LocalPlayer, 0.5)
	SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(LocalPlayer, 1.0)					
	PRINTLN("[JS] [BEASTMODE] Cleaning up melee modifier")
	SET_PED_SUFFERS_CRITICAL_HITS(LocalPlayerPed, TRUE)
	SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IgnoreMeleeFistWeaponDamageMult, FALSE)
	SET_PLAYER_MAX_EXPLOSIVE_DAMAGE(LocalPlayer, -1)
	SET_DISABLE_HIGH_FALL_DEATH(LocalPlayerPed, FALSE)
	SET_PLAYER_FALL_DISTANCE_TO_TRIGGER_RAGDOLL_OVERRIDE(LocalPlayer, -1.0)
	
	IF bDoHealthCleanup
		PRINTLN("[JS] [BEASTMODE] Cleaning up health")
		IF iMaxHealthBeforebigfoot > 0
			SET_PED_MAX_HEALTH(LocalPlayerPed, iMaxHealthBeforebigfoot)
			IF bLocalPlayerPedOK
			AND bLocalPlayerOK
				PRINTLN("[JS] [BEASTMODE] Setting health back to normal (", iMaxHealthBeforebigfoot,")")
				SET_ENTITY_HEALTH(LocalPlayerPed, iMaxHealthBeforebigfoot)
			ENDIF
			iMaxHealthBeforebigfoot = 0
		ENDIF
	ENDIF
	
	IF bFinalCleanup
		PRINTLN("[JS] [BEASTMODE] Doing final cleanup")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_APARTMENT/APT_BEAST")
		IF IS_AUDIO_SCENE_ACTIVE("GTAO_BvS_Gameplay_Scene")
			STOP_AUDIO_SCENE("GTAO_BvS_Gameplay_Scene")
		ENDIF
		SET_AMBIENT_ZONE_STATE("AZ_COUNTRY_SAWMILL",TRUE,TRUE)
		SET_AMBIENT_ZONE_STATE("AZ_SAWMILL_CONVEYOR_01",TRUE,TRUE)
		SET_STATIC_EMITTER_ENABLED("SE_COUNTRY_SAWMILL_MAIN_BUILDING", TRUE)
	ENDIF
	
	IF iBeastSprintSoundID > -1
		IF NOT HAS_SOUND_FINISHED(iBeastSprintSoundID)
			STOP_SOUND(iBeastSprintSoundID)
			SCRIPT_RELEASE_SOUND_ID(iBeastSprintSoundID)
		ENDIF
	ENDIF
	
	CLEAR_10_SECOND_COUNTDOWN_BEEPS()
	
	IF bResetClothing OR IS_BIT_SET(iLocalBoolCheck17, LBOOL17_CLEANUP_BEAST_MODE_ON_RESPAWN)
		IF IS_CORONA_READY_TO_START_WITH_JOB()
		OR IS_FAKE_MULTIPLAYER_MODE_SET()
			MP_OUTFITS_APPLY_DATA   sApplyData
			sApplyData.pedID		= LocalPlayerPed
			sApplyData.eApplyStage 	= AOS_SET
			sApplyData.eOutfit		= INT_TO_ENUM(MP_OUTFIT_ENUM, GET_LOCAL_DEFAULT_OUTFIT(MC_playerBD[iPartToUse].iTeam))
			IF SET_PED_MP_OUTFIT(sApplyData, FALSE, FALSE, FALSE, FALSE)
				PRINTLN("[JS][BEASTMODE] Outfit not reset")
				SET_BIT(iLocalBoolCheck17, LBOOL17_CLEANUP_BEAST_MODE_ON_RESPAWN)
			ELSE
				PRINTLN("[JS][BEASTMODE] Reset player outfit")
				CLEAR_BIT(iLocalBoolCheck17, LBOOL17_CLEANUP_BEAST_MODE_ON_RESPAWN)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


PROC RESET_PLAYER_HEALTH()
	IF iMaxHealthBeforebigfoot != 0
		IF bLocalPlayerOK	
			PRINTLN("[JS] [BEASTMODE] Decreasing health from ", GET_PED_MAX_HEALTH(LocalPlayerPed)," to ", iMaxHealthBeforebigfoot)
			SET_PED_MAX_HEALTH(LocalPlayerPed, iMaxHealthBeforebigfoot)
			SET_ENTITY_HEALTH(LocalPlayerPed, iMaxHealthBeforebigfoot)
			SET_PED_SUFFERS_CRITICAL_HITS(LocalPlayerPed, TRUE)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IgnoreMeleeFistWeaponDamageMult, FALSE)
			SET_PLAYER_MAX_EXPLOSIVE_DAMAGE(LocalPlayer, -1)
			iMaxHealthBeforebigfoot = 0
			bFMMCHealthBoosted = FALSE
		ENDIF
	ENDIF
ENDPROC


PROC RESET_PLAYER_RELATED_CONFIGS()
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - RESET_PLAYER_RELATED_CONFIGS ################################")
	IF NOT IS_PED_INJURED(LocalPlayerPed)
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset3, ciBS3_TEAM_DISABLE_HELMET_ARMOUR_FROM_OUTFIT)
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - ciBS3_TEAM_DISABLE_HELMET_ARMOUR_FROM_OUTFIT is set. Resetting: PCF_DisableHelmetArmor")
			
			IF DOES_ENTITY_EXIST(LocalPlayerPed)
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableHelmetArmor, FALSE)
			ENDIF
		ENDIF		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_SPRINTING_IN_INTERIORS)
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IgnoreInteriorCheckForSprinting, FALSE)")
			IF DOES_ENTITY_EXIST(localPlayerPed)
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IgnoreInteriorCheckForSprinting, FALSE)
			ENDIF		
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(tdTeamSwapPassiveModeTimer)
	AND g_MultiplayerSettings.g_bTempPassiveModeSetting
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - g_bTempPassiveModeSetting was set. Cleaning up passive mode.")
		CLEANUP_TEMP_PASSIVE_MODE()
		TURN_OFF_PASSIVE_MODE_OPTION(FALSE)		
		SET_ENTITY_ALPHA(LocalPlayerPed, 255, FALSE)
		IF NOT IS_PED_INJURED(LocalPlayerPed)
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_BlockWeaponFire, FALSE)
		ENDIF
	ENDIF
	IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_IN_BEAST_MODE)
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - cleanup beast.")
		CLEAN_UP_BEAST_MODE(TRUE, DEFAULT, TRUE)
	ENDIF	
	
	IF NOT IS_PED_INJURED(LocalPlayerPed)
		IF HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_MINIGUN)
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - SET_PED_INFINITE_AMMO = false.")
			SET_PED_INFINITE_AMMO(LocalPlayerPed, FALSE, WEAPONTYPE_MINIGUN)
		ENDIF	
		RESET_PLAYER_HEALTH()
		IF DOES_ENTITY_EXIST(GET_PLAYER_PED(LocalPlayer))
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - resetting SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER")
			SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(LocalPlayer, 1.0)
		ENDIF
		
		IF DOES_ENTITY_EXIST(GET_PLAYER_PED(LocalPlayer))
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - resetting SET_PLAYER_WEAPON_DEFENSE_MODIFIER")
			SET_PLAYER_WEAPON_DEFENSE_MODIFIER( LocalPlayer, 1.0 ) // Reset any defense modifiers
		ENDIF
				
		IF bLocalPlayerPedOK
			IF( bWasKnockOffSettingChangedForTrash )
				PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - resetting KNOCKOFFVEHICLE_DEFAULT")
				SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE( LocalPlayerPed, KNOCKOFFVEHICLE_DEFAULT )
			ENDIF
			CLEAR_PED_PROP(LocalPlayerPed,ANCHOR_RIGHT_HAND)
			CLEAR_PED_PROP(LocalPlayerPed,ANCHOR_LEFT_HAND)
			PRINTLN("[RCC MISSION] - Cleanup - Set PCF_PreventAutoShuffleToDriversSeat, false")
			SET_PED_CONFIG_FLAG(LocalPlayerPed,PCF_PreventAutoShuffleToDriversSeat,FALSE)
		ENDIF
		
		PRINTLN("[RCC MISSION] - Cleanup - Set PCF_DisableLadderClimbing, false")
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableLadderClimbing, FALSE)
		
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_GetOutUndriveableVehicle, TRUE)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_FallsOutOfVehicleWhenKilled, TRUE)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_GetOutBurningVehicle, TRUE)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_RunFromFiresAndExplosions, TRUE)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_BlockDispatchedHelicoptersFromLanding, FALSE)
		
		SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed, TRUE)
	ENDIF
	
	IF LocalPlayerPed != NULL
		IF NETWORK_GET_ENTITY_IS_NETWORKED(LocalPlayerPed)
			IF NETWORK_IS_PLAYER_CONCEALED(LocalPlayer)
			AND NOT bIsSCTV
				PRINTLN("[RCC MISSION] - Cleanup - Set NETWORK_CONCEAL_PLAYER, false")
				NETWORK_CONCEAL_PLAYER(LocalPlayer, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_VISIBLE(LocalPlayerPed)
		IF NOT IS_PLAYER_SPECTATING(LocalPlayer)
		AND NOT bIsSCTV
			SET_ENTITY_VISIBLE(LocalPlayerPed, TRUE)
		ENDIF 
	ENDIF
	
ENDPROC

PROC CLEANUP_WHITEOUT_RESPAWNING_FADING()
	IF g_b_RespawnUseWhiteOutIn
		SET_RECTANGLE_COLOUR(TRUE)
		ACTIVATE_RECTANGLE_FADE_OUT(TRUE) 
		ACTIVATE_RECTANGLE_FADE_IN(FALSE) 
		g_b_RespawnUseWhiteOutIn = FALSE
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - g_b_RespawnUseWhiteOutIn = FALSE")
	ENDIF
ENDPROC

PROC CLEANUP_BLOCK_WASTED_KILLSTRIP_SCREEN()
	SET_BIG_MESSAGE_SUPPRESS_WASTED(FALSE)
	g_b_SkipKillStrip = FALSE	
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - g_b_SkipKillStrip = FALSE")
ENDPROC

PROC RESET_MISSION_SPECIFIC_GLOBALS(BOOL bFinalCleanup, BOOL bStrand)
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - RESET_MISSION_SPECIFIC_GLOBALS ################################")
	UNUSED_PARAMETER(bFinalCleanup)
	INT i
	g_iMissionMiscBitset1 = 0
	g_bMissionInstantRespawn = FALSE
	g_bAppliedMissionOutfit = FALSE
	g_bBlockInfiniteRangeCheck = FALSE
	g_bMissionPlacedOrbitalCannon = FALSE
	g_iMissionObjectHasSpawnedBitset = 0
	g_bMissionHasPlacedAvengerVehicle = FALSE
	g_bLeavingVehInteriorForMCEnd = FALSE
	g_bMissionHideRespawnBar = FALSE 
	g_bMissionHideRespawnBarForRestart = FALSE 
	g_bBlockSeatShufflePlacedVeh = FALSE
	g_bInMissionControllerCutscene = FALSE
	g_SpawnData.VehicleWeapon = WEAPONTYPE_INVALID
	g_mnOfLastVehicleWeWasIn = DUMMY_MODEL_FOR_SCRIPT
	g_iFMMCCurrentStaticCam = -1	
	g_bMCForceThroughRestartState = FALSE	
	g_bMissionSeatSwapping = FALSE
	g_bAbortPropertyMenus = FALSE
	MPGlobalsAmbience.bEnablePassengerBombing = FALSE
	g_SpawnData.bCargobobUseBehaviour = FALSE
	g_SpawnData.bRememberVehicleWeaponOnSpawn = FALSE	
	iTotalObjectsToDefend = -1	
	g_b_UseBottomRightTitleColour = FALSE
	iStoreLastTeam0Score = 0
	iStoreLastTeam1Score = 0
	bShownScoreShardInSuddenDeath = FALSE
	bsHasObjExploded = - 1
	iExplodeCountdownSound = - 1
	iExplodeLocalTimer = - 1
	iStoredNoWeaponZone = - 1
	iSniperBallObj = - 1
	oiSniperBallPackage = NULL	
	DISABLE_VEHICLE_MINES(FALSE)
	bFMMCHealthBoosted = FALSE	
	iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
	iHackPercentage = 0
	tl15HackComplete = ""
	iTrackifyProgress = 0
	g_VehicleRocketInfo.bIsCollected = FALSE
	g_VehicleBoostInfo.bCollected = FALSE
	g_VehicleSpikeInfo.bIsCollected = FALSE
	g_VehicleGunInfo.bCollected = FALSE
	g_vehStandbyVehicle = NULL
	//clean up for - 2646657
	g_bNoDeathTickers = FALSE
	g_bTeamColoursOverridden = FALSE
	g_ForceRedWastedKillerShard = FALSE	
	g_bRunGroupYachtExit = FALSE
	g_bRunningGroupYachtExit = FALSE	
	g_bMissionLoadingIsland = FALSE
	
	CLEANUP_WHITEOUT_RESPAWNING_FADING()
	CLEANUP_BLOCK_WASTED_KILLSTRIP_SCREEN()
	
	IF NOT bStrand
	AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
		g_bMissionReadyForIslandWater = FALSE
		PRINTLN("MC_SCRIPT_CLEANUP - g_bMissionReadyForIslandWater cleaned up!")
	ELSE
		PRINTLN("MC_SCRIPT_CLEANUP - g_bMissionReadyForIslandWater Not cleaning this up, we are Quick Restarting or Strand Transitioning.")
	ENDIF
		
	IF bFinalCleanup
	AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
		FOR i = 0 TO g_FMMC_STRUCT.iNumberOfTeams-1
			g_iMissionLobbyStartingInventoryIndex[i] = 0 // Not too important as it requires a bitset to be used. Up to Freemode to ensure this is assigned to properly.
		ENDFOR
	ENDIF
	
	IF NOT bStrand
		g_bBlockScubaGear = FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD 
	g_iDialogueProgressLastPlayed = -1 
	#ENDIF
	
	SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(FALSE)
		
	// Cleanup assists
	g_MissionControllerserverBD_LB.sleaderboard[iPartToUse].iAssists += g_iMyAssists
	g_iMyAssists = 0
	
	//For altering vehicle objective text based on the player who is on the vehicles colouring.
	iVehCapTeam =  -1	
	bsCheckPointCreated = 0
	
	//Clean up passed through globals
	MC_PlayerBD_VARS_STRUCT sEmptyPlayerBDVars
	MC_serverBD_VARS_STRUCT sEmptyServerBDVars
	MC_LocalVariables_VARS_STRUCT sEmptyLocalVars
	gMC_PlayerBD_VARS = sEmptyPlayerBDVars
	gBG_MC_serverBD_VARS = sEmptyServerBDVars
	gMC_LocalVariables_VARS = sEmptyLocalVars
	
	#IF IS_DEBUG_BUILD
	g_debugDisplayState = DEBUG_DISP_OFF
	#ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciALLOW_MISSION_AIRSTRIKES)
		g_vAirstrikeCoords = <<0,0,0>>
	ENDIF
	
	FOR i = 0 TO FMMC_MAX_NUM_OBJECTS - 1
		gBG_MC_serverBD_VARS.oiObjects[i] = NULL
	ENDFOR
	FOR i = 0 TO FMMC_MAX_VEHICLES - 1
		gBG_MC_serverBD_VARS.viVehicles[i] = NULL
	ENDFOR
	FOR i = 0 TO FMMC_MAX_PEDS - 1
		gBG_MC_serverBD_VARS.piPeds[i] = NULL
	ENDFOR
	
	CLEAR_CACHED_AVAILABLE_VEHICLE_COUNTS()
	
	CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iMissionModeBit, ciMISSION_USING_ORBITAL_CANNON)

	CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].playerBlipData.iFlags, PBBD_FADE_HIDE)
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER) 
			SET_EMPTY_ARMORY_TRUCK_TRAILER(FALSE)
			PRINTLN("[RCC MISSION] - MC_SCRIPT_CLEANUP - Setting BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER")
		ENDIF
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_LEAVE_ARMORY_TRUCK_TRAILER_ON_DELIVER)
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_LEAVE_ARMORY_TRUCK_TRAILER_ON_DELIVER)
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD
		bDrawMenu = FALSE
	#ENDIF
	IF NOT IS_THIS_A_ROUNDS_MISSION()
		PRINTLN("[MMacK][RoundsTimeBonus] MC_SCRIPT_CLEANUP g_iRoundsCombinedTime cleaned up!")
		g_iRoundsCombinedTime = 0
		g_iCachedMatchBonus = 0
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciALLOW_MOC_ENTRY_THIS_MISSION)
		SET_ALLOW_INTERIOR_SCRIPT_FOR_MISSION(FALSE)		
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_LEAVE_ARMORY_TRUCK_TRAILER_ON_DELIVER)
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_LEAVE_ARMORY_TRUCK_TRAILER_ON_DELIVER) 
			CDEBUG1LN(DEBUG_SPAWNING, "MC_SCRIPT_CLEANUP Clearing BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_LEAVE_ARMORY_TRUCK_TRAILER_ON_DELIVER ENDING")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen,ciENABLE_AVENGER_INTERIOR_ENTRY)		
		SET_ALLOW_INTERIOR_SCRIPT_FOR_MISSION(FALSE)
	ENDIF
	
	CLEAR_BIT(g_FMMC_STRUCT.bsRaceMission, ciDidSpeedBoost)
	REENABLE_AMBIENT_NIGHT_VISION()
	CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_TOGGLING_NIGHTVISION_FILTER)
	CLEAR_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
		SET_ON_TEAM_RACE_RACE_GLOBAL(FALSE)
	ENDIF
	
	// cleanup our saved drivers and partners
	PRINTLN("[LM][MULTI_VEH_RESPAWN][MC_SCRIPT_CLEANUP] - Cleaning up the variables for the next mission/round.")
	INT iii = 0
	FOR iii = 0 TO MAX_VEHICLE_PARTNERS-1
		SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iii, -1)
	ENDFOR
	SET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER(0)
	SET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE(-3)
	CLEAR_SPECIFIC_VEHICLE_TO_RESPAWN_IN()	
	CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()

	FOR i = 0 TO 3
		g_SpawnData.vCargobobSpawnCoord[i] = <<-1,-1,-1>>
		g_SpawnData.fCargobobSpawnHeading[i] = -1
	ENDFOR	
	
	g_SpawnData.iClosestCBPlayerIndex = -1
	g_SpawnData.fClosestCBPlayerDist = -1	
	MPGlobalsHud.b_HideCrossTheLineUI = FALSE
	MPGlobalsAmbience.bWantedStarsHidden = FALSE
	
	ACTIVATE_KILL_TICKERS( TRUE )
	g_bNoDeathTickers = FALSE
	g_bTriggerCrossTheLineAudio = FALSE
	
	SET_USE_RACE_RESPOT_AFTER_RESPAWNS(FALSE)	
	CLEANUP_DYNAMIC_FLARES()	
	RESET_GLOBAL_SPECTATOR_STRUCT()	
	CLEAR_IDLE_KICK_TIME_OVERRIDDEN()	
	RESET_WARP_TO_SPAWN_LOCATION_GLOBALS() 
	RESET_NET_WARP_TO_COORD_GLOBALS() //kill any active warps
	CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_TEXT)	
	SET_SPECTATOR_CAN_QUIT(TRUE)
	
	g_bEndOfMissionCleanUpHUD = FALSE
	g_FMMC_STRUCT.bRemoveUnwantedCamPhoneOptions = FALSE	
	//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].g_bIsHacking = FALSE
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bIsHacking)
	
	CLEAR_IM_WATCHING_A_MOCAP()
	
	//Hacking Scaleform
	sfHacking = NULL
	
	g_bDelayUnlockScreenAfterHeists = TRUE // added by Kevin as Imran wanted unlocks delayed by 10 secs bug 2172240 6/1/2014
	MPGlobalsHud_TitleUpdate.unlock_delay_heists_ticker_main_timer = 0
	
	//Render target clean up.
	MC_serverBD_1.iPlayerForHackingMG = -1
	CLEANUP_RENDERTARGETS()
	IF ARE_EMERGENCY_CALLS_SUPPRESSED()
		RELEASE_SUPPRESSED_EMERGENCY_CALLS()
	ENDIF	
	
	g_FinalRoundsLbd = FALSE
	g_b_PopulateReady = FALSE
	
	SET_ON_JOB_INTRO(FALSE)	
	SET_PLAYER_WILL_SPAWN_USING_SAME_NODE_TYPE_AS_TARGET(FALSE)	
	IF g_MultiplayerSettings.g_bTempPassiveModeSetting
		CLEANUP_TEMP_PASSIVE_MODE()
		TURN_OFF_PASSIVE_MODE_OPTION(FALSE)
	ENDIF	
	Clear_Any_Objective_Text()	
	CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
	
	INT iCheckpoint
	FOR iCheckpoint = 0 TO FMMC_MAX_RESTART_CHECKPOINTS - 1
		IF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_1 + iCheckpoint)
			FMMC_CHECKPOINTS_SET_RETRY_START_POINT(iCheckpoint)
		ENDIF
	ENDFOR
	
	IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciOPTIONS4_MAINTAIN_SPAWN_GROUPS_ON_RESTART) AND IS_CORONA_INITIALISING_A_QUICK_RESTART())
		PRINTLN("[RCC MISSION][ran][SpawnGroups] MC_SCRIPT_CLEANUP - Copying rsgSpawnSeed data into g_TransitionSessionNonResetVars, as this is a quick restart / strand,  rsgFMMCSpawnSeedCachedMissionStart.iEntitySpawnSeedBS: ",  MC_serverBD_4.rsgSpawnSeedCachedMissionStart.iEntitySpawnSeedBS)
		g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed = MC_serverBD_4.rsgSpawnSeedCachedMissionStart
		FOR i = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1 
			PRINTLN("[RCC MISSION][ran][SpawnGroups] MC_SCRIPT_CLEANUP - Copying rsgSpawnSeed data into g_TransitionSessionNonResetVars, as this is a quick restart / strand,  rsgFMMCSpawnSeedCachedMissionStart.iEntitySpawnSeed_SubGroupBS[",i,"] = ", MC_serverBD_4.rsgSpawnSeedCachedMissionStart.iEntitySpawnSeed_SubGroupBS[i])
		ENDFOR
		
	ELIF (bStrand AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_MaintainSpawnGroupOverStrand_SaveOnly))			
		PRINTLN("[RCC MISSION][ran][SpawnGroups] MC_SCRIPT_CLEANUP - Copying rsgSpawnSeed data into g_TransitionSessionNonResetVars, as this is a quick restart / strand,  MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS: ",  MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS)
		g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed = MC_serverBD_4.rsgSpawnSeed
		FOR i = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1 
			PRINTLN("[RCC MISSION][ran][SpawnGroups] MC_SCRIPT_CLEANUP - Copying rsgSpawnSeed data into g_TransitionSessionNonResetVars, as this is a quick restart / strand,  MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[",i,"] = ", MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[i])
		ENDFOR
		
	ELSE
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_MaintainSpawnGroupOverStrand_DontClearSaved)
			PRINTLN("[RCC MISSION][ran][SpawnGroups] MC_SCRIPT_CLEANUP - CLEARING g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed: ", g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed.iEntitySpawnSeedBS, " as ciOptionsBS25_MaintainSpawnGroupOverStrand_DontClearSaved is NOT set.")
			CLEAR_SPAWN_GROUP_STRUCT(g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed)
		ELSE
			PRINTLN("[RCC MISSION][ran][SpawnGroups] MC_SCRIPT_CLEANUP - NOT CLEARING g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed: ", g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed.iEntitySpawnSeedBS, " as ciOptionsBS25_MaintainSpawnGroupOverStrand_DontClearSaved is set.")
		ENDIF
	ENDIF
		
	INT iSpawnGroup	
	IF (IS_THIS_A_ROUNDS_MISSION() AND NOT SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD())
		g_TransitionSessionNonResetVars.iSpawnGroupLastBitset = MC_serverBD_4.rsgSpawnSeedCachedMissionStart.iEntitySpawnSeedBS
		PRINTLN("MC_SCRIPT_CLEANUP - COPY iSpawnGroupLastBitset = ", g_TransitionSessionNonResetVars.iSpawnGroupLastBitset)
		REPEAT ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS iSpawnGroup
			g_TransitionSessionNonResetVars.iSpawnSubGroupLastBitset[iSpawnGroup] = MC_serverBD_4.rsgSpawnSeedCachedMissionStart.iEntitySpawnSeed_SubGroupBS[iSpawnGroup]
			PRINTLN("MC_SCRIPT_CLEANUP - COPY iSpawnSubGroupLastBitset[", iSpawnGroup, "] = ", g_TransitionSessionNonResetVars.iSpawnSubGroupLastBitset[iSpawnGroup])
		ENDREPEAT
	ELSE
		g_TransitionSessionNonResetVars.iSpawnGroupLastBitset = 0
		PRINTLN("MC_SCRIPT_CLEANUP - CLEAR iSpawnGroupLastBitset = 0")
		REPEAT ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS iSpawnGroup
			g_TransitionSessionNonResetVars.iSpawnSubGroupLastBitset[iSpawnGroup] = 0
			PRINTLN("MC_SCRIPT_CLEANUP - CLEAR iSpawnSubGroupLastBitset[", iSpawnGroup, "] = 0")
		ENDREPEAT
	ENDIF
				
	IF IS_CORONA_INITIALISING_A_QUICK_RESTART()
		INT iTeam = MC_playerBD[iPartToUse].iteam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]		
		IF iRule < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_ENABLE_IS_START_TIME)			
				SET_TIME_OF_DAY_FOR_QUICK_RESTART_FROM_RULE(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODStartHour[iRule])
			ELSE
				SET_TIME_OF_DAY_FOR_QUICK_RESTART_FROM_RULE(-1)
			ENDIF
		ENDIF
	ELSE
		SET_TIME_OF_DAY_FOR_QUICK_RESTART_FROM_RULE(-1)
	ENDIF
	
	IF IS_BIT_SET(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_PRES_EXTRACTION_APP)
		PRINTLN("[RCC MISSION][EXTRACTION] Turning off Extraction App!")
		CLEAR_BIT(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_PRES_EXTRACTION_APP)
	ENDIF
	
	IF IS_BIT_SET(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SECUROSERV_HACK_APP)
	OR IS_BIT_SET(BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPDUMMYAPP0)
		PRINTLN("[RCC MISSION][SECUROHACK] Turning off SecuroServ Hack App!")
		CLEAR_BIT(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SECUROSERV_HACK_APP)
		CLEAR_BIT(BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPDUMMYAPP0)
		CLEAR_BIT(BitSet_CellphoneTU, g_BSTU_SET_CELLPHONE_CONTROLLER_LAUNCH)
	ENDIF
	
	IF g_bDisablePropertyAccessForLoseCopMissionObj
		g_bDisablePropertyAccessForLoseCopMissionObj = FALSE
		PRINTLN("g_bDisablePropertyAccessForLoseCopMissionObj set to FALSE - 2")
	ENDIF
	g_bReducedApartmentCreationTurnedOn = FALSE
	PRINTLN("g_bReducedApartmentCreationTurnedOn set to: ",g_bReducedApartmentCreationTurnedOn, " reason: ",999)
	PRINTLN("Script has disabled Trackify ")
	ENABLE_MULTIPLAYER_TRACKIFY_APP(FALSE)
	iTrackifyProgress = 0
	
	IF NOT IS_PED_INJURED(LocalPlayerPed)
		CLEAR_ALL_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed)
	ENDIF
	
	g_sTransitionSessionData.bMissionCutsceneInProgress = FALSE
	PRINTLN("[RCC MISSION][AMEC] Setting bMissionCutsceneInProgress to FALSE- 2.")
	
	SET_SHOP_IGNORES_KICKING_OFF_CHECKS(CARMOD_SHOP_01_AP, FALSE)
	SET_SHOP_IGNORES_KICKING_OFF_CHECKS(CARMOD_SHOP_05_ID2, FALSE)
	SET_SHOP_IGNORES_KICKING_OFF_CHECKS(CARMOD_SHOP_06_BT1, FALSE)
	SET_SHOP_IGNORES_KICKING_OFF_CHECKS(CARMOD_SHOP_07_CS1, FALSE)
	SET_SHOP_IGNORES_KICKING_OFF_CHECKS(CARMOD_SHOP_08_CS6, FALSE)
	SET_SHOP_IGNORES_KICKING_OFF_CHECKS(CARMOD_SHOP_SUPERMOD, FALSE) 
	
	g_bAllowClothesShopInMission = FALSE
	g_bMissionEnding = FALSE
	CLEAR_MP_DECORATOR_BIT(LocalPlayer, MP_DECORATOR_BS_MISSION_ENDING)
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - MP_DECORATOR_BS_MISSION_ENDING - CLEARED")
	g_bVSMission = FALSE
	g_bOnCoopMission = FALSE
	g_b_OnRaceIntro = FALSE
	g_bDeactivateRestrictedAreas = FALSE
	g_bMissionOver = FALSE	
	g_i_IgnoreSuicideTicker_PlayerBS = 0	
	g_bCleanupMultiplayerMissionInteriors = true 
	//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].g_bInMissionCreatorApartment = FALSE
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorApartment)
	CLEAR_BIT(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].iXPBitset, ciGLOBAL_XP_BIT_ON_JOB)	
	CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM0_TARGET)
	CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM1_TARGET)
	CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM2_TARGET)
	CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM3_TARGET)	
	Delete_MP_Objective_Text()
	BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(FALSE)
	g_FMMC_STRUCT.g_b_QuitTest = FALSE
	IF NOT IS_CORONA_INITIALISING_A_QUICK_RESTART() // We don't want the IPLs to clean up if we're just coming back in
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Not Quick Restarting, Blocking Building Controller FALSE")
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(FALSE)
	ENDIF
	
	MPGlobalsInteractions.ePickupState = INT_TO_ENUM(eSpecialPickupState, 0)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_NON_HEIST_REBREATHERS)
		IF (NOT IS_ENTITY_DEAD(LocalPlayerPed))
		AND IS_MP_HEIST_GEAR_EQUIPPED(LocalPlayerPed, HEIST_GEAR_REBREATHER)
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Have an equipped rebreather on since we were using them during mission. Removing for going back into freemode.")
			REMOVE_MP_HEIST_GEAR(LocalPlayerPed, HEIST_GEAR_REBREATHER)
		ENDIF
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			IF sMissionLocalContinuityVars.iNumRebreathersFromFreemode > 0
				SET_MP_INT_CHARACTER_STAT(MP_STAT_BREATHING_APPAR_BOUGHT, sMissionLocalContinuityVars.iNumRebreathersFromFreemode)
				PRINTLN("[RCC MISSION] PROCESS_MISSION_END_STAGE - Resetting number of rebreathers to: ", sMissionLocalContinuityVars.iNumRebreathersFromFreemode)
			ENDIF
		ENDIF
		g_bResetRebreatherAir = FALSE
	ENDIF
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		IF (NOT IS_ENTITY_DEAD(LocalPlayerPed))
		AND IS_MP_HEIST_GEAR_EQUIPPED(LocalPlayerPed, MC_playerBD_1[iLocalPart].eHeistGearBagType)
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - The player has an equipped duffel bag on since we were using them during mission. Removing for going back into freemode.")
			REMOVE_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
		ENDIF
	ENDIF
	
	PREVENT_GANG_OUTFIT_CHANGING(FALSE)	
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF NOT IS_FORCED_WEAPON_STRAND_BEING_INITIALISED()
			CONFIGURE_CUSTOM_WEAPON_LOADOUT()
			PRINTLN("MC_SCRIPT_CLEANUP - CONFIGURE_CUSTOM_WEAPON_LOADOUT()")
		ENDIF
	ENDIF
			
	IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_SCUBA_GEAR_ON)
		SET_PED_ABLE_TO_DROWN(LocalPlayerPed)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRESPAWN_WITH_PARACHUTE_EQUIPPED)
		MPGlobalsAmbience.bDisableTakeOffChute = FALSE
	ENDIF	
	DISABLE_SPECTATOR_FILTER_OPTION(FALSE)
	
	g_iMissionPlayerLeftBS = 0
	g_iCurrentSpectatorTrapSpawned = 0
	g_bMissionClientGameStateRunning = FALSE
	g_bPlayerFullySeated = FALSE
	
	g_iLastDamagerPlayer = -1
	g_iLastDamagerTimeStamp = -1
	g_iLastPlayerIDamaged = -1
	g_iLastPlayerIDamagedTimeStamp = -1
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].iLastDamagerPlayer = -1
	ENDIF
	
	SET_MC_PLAYER_MINI_ROUND_RESTART(FALSE)
	g_iFailedTrapCamSwitchBlackList = 0
	g_bMission321Done = FALSE
	g_bIntroJobShardPlayed = FALSE
	g_fTimeSpentInSpectatorMode = 0
	g_fTimeSpentInSpectatorPowerup = 0
	g_iKillsInSpectatorMode = 0
	g_iKillsInSpectatorPowerup = 0
	g_iSpecialSpectatorTelemetryBS = 0
	RESET_NET_TIMER(g_tdTelemetryDelay)
	RESET_NET_TIMER(g_tdTelemetryDelay2)
	SET_TIME_LEFT_FOR_INSTANCED_CONTENT_IN_MILISECONDS(-1)
	g_iBS1_Mission = 0
	g_iBSPlayerLeftDoSomething = 0
	g_bUsingTurretHeliCam = FALSE
	g_bInInteriorThatRequiresInstantFadeout = FALSE
	g_bMissionDoNotHangUpFromDialogueHandler = FALSE
	CLEAR_BIT(BitSet_CellphoneDisplay, g_BS_DISABLE_INCOMING_OR_OUTGOING_CALL_HANGUP)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_EnableWeaponsInFreemodeShops)
		ALLOW_WEAPONS_IN_SHOPS(FALSE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_DisablePlayerPropertiesInMission)
		INT iMaxProperties = ENUM_TO_INT(PPAG_ARENA_GARAGE)
		INT iProperty
		FOR iProperty = 0 TO iMaxProperties
			PRINTLN("[RCC MISSION] SET_PLAYER_BLOCK_FROM_PROPERTY_FLAG with iProperty: ", iProperty)
			CLEAR_PLAYER_BLOCK_FROM_PROPERTY_FLAG(INT_TO_ENUM(PREVENT_PROPERTY_ACCESS_FLAGS, iProperty))
		ENDFOR
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableCasino_InteriorScripts)
		SET_ALLOW_INTERIOR_SCRIPT_FOR_MISSION(FALSE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableDroneDeployment)
		PRINTLN("[LM][RCC MISSION] MC_SCRIPT_CLEANUP, ciOptionsBS24_EnableDroneDeployment enabled. Disabling Drone Deployment for mission.") 
		SET_ENABLE_DRONE_USE_FROM_PI_MENU_IN_MISSION(FALSE)
	ENDIF
	CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_MASKS_AND_GEAR)	
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		g_bSlowingArmour = FALSE
	ENDIF
	
	g_bAutoMaskEquip = FALSE
	g_bAllowKillYourselfOnMission = FALSE
		
	IF iPersonalVehicleSlotCache != -1
		PRINTLN("MC_SCRIPT_CLEANUP - Resetting personal vehicle to iPersonalVehicleSlotCache: ", iPersonalVehicleSlotCache) 
		SET_LAST_USED_VEHICLE_SLOT(iPersonalVehicleSlotCache)
		
		IF IS_CORONA_INITIALISING_A_QUICK_RESTART()
		AND IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_IN_PERSONAL_VEHICLE)
			PRINTLN("MC_SCRIPT_CLEANUP - started in iPersonalVehicleSlotCache: ", iPersonalVehicleSlotCache)
			IF IS_BIT_SET(g_MpSavedVehicles[iPersonalVehicleSlotCache].iVehicleBS, MP_SAVED_VEHICLE_DESTROYED)
				PRINTLN("MC_SCRIPT_CLEANUP - clearing MP_SAVED_VEHICLE_DESTROYED for iPersonalVehicleSlotCache: ", iPersonalVehicleSlotCache)
				CLEAR_BIT(g_MpSavedVehicles[iPersonalVehicleSlotCache].iVehicleBS, MP_SAVED_VEHICLE_DESTROYED)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
		DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION_STARTUP(FALSE)
		DISABLE_PERSONAL_VEHICLE_AMBIENT_CREATION_DURING_MISSION(FALSE)
	ENDIF
	
	SET_ALLOW_MOD_SHOP_DURING_CONTENT_MISSION(FALSE)
	
	CLEAR_BIT(MPGlobalsAmbience.iNGAmbBitSet,  iNGABI_PLAYER_IN_HIDE_PV_BLIP_AREA)
	CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet2, iABI2_BLOCK_SPECTATOR_TARGET_COPY_FADING_IN)
ENDPROC

PROC CLEAR_WORLD_PROP_MODEL_SWAP(INT iWorldProp)

	IF NOT IS_CUSTOM_STRING_LIST_STRING_VALID(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].iModelSwapStringIndex)
		EXIT
	ENDIF
	
	MODEL_NAMES mnWorldPropModelToSwapTo = GET_WORLD_PROP_MODEL_SWAP_MODEL_NAME(iWorldProp)
	
	IF IS_MODEL_VALID(mnWorldPropModelToSwapTo)
		#IF IS_DEBUG_BUILD
		PRINTLN("[WORLDPROPS][WorldPropModelSwap][WorldProp ", iWorldProp, "] CLEAR_WORLD_PROP_MODEL_SWAP | Removing model swap from ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].mn), " / ", mnWorldPropModelToSwapTo)
		#ENDIF
		
		REMOVE_MODEL_SWAP(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].vPos, 2.5, g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].mn, mnWorldPropModelToSwapTo, FALSE)
	ELSE
		#IF IS_DEBUG_BUILD
		PRINTLN("[WORLDPROPS][WorldPropModelSwap][WorldProp ", iWorldProp, "] CLEAR_WORLD_PROP_MODEL_SWAP | Model was invalid!")
		#ENDIF
	ENDIF
ENDPROC

PROC CLEAN_UP_WORLD_PROP_OBJECT_REFERENCE(INT iWorldProp, BOOL bPrintDebug = TRUE)

	IF NOT DOES_ENTITY_EXIST(sRunTimeWorldPropData.oiWorldProps[iWorldProp])
		IF bPrintDebug PRINTLN("[WorldProps_SPAM][WorldProp_SPAM ", iWorldProp, "] CLEAN_UP_WORLD_PROP_OBJECT_REFERENCE - We don't have an object reference for world prop ", iWorldProp) ENDIF
		EXIT
	ENDIF
	
	IF IS_LONG_BIT_SET(sRunTimeWorldPropData.iWorldPropFrozenBitSet, iWorldProp)
		FREEZE_ENTITY_POSITION(sRunTimeWorldPropData.oiWorldProps[iWorldProp], FALSE)
		CLEAR_LONG_BIT(sRunTimeWorldPropData.iWorldPropFrozenBitSet, iWorldProp)
		IF bPrintDebug PRINTLN("[WorldProps][WorldProp ", iWorldProp, "] CLEAN_UP_WORLD_PROP_OBJECT_REFERENCE - Unfreezing world prop ", iWorldProp) ENDIF
	ENDIF
	
	IF IS_LONG_BIT_SET(sRunTimeWorldPropData.iWorldPropInvincibleBitSet, iWorldProp)
		SET_ENTITY_INVINCIBLE(sRunTimeWorldPropData.oiWorldProps[iWorldProp], FALSE)
		
		IF GET_IS_ENTITY_A_FRAG(sRunTimeWorldPropData.oiWorldProps[iWorldProp])
			PRINTLN("[RCC MISSION][WorldProps][WorldProp ", iWorldProp, "] CLEAN_UP_WORLD_PROP_OBJECT_REFERENCE - Re-enabling frag damage on world prop ", iWorldProp, " at position: ", g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].vPos)
			SET_DISABLE_FRAG_DAMAGE(sRunTimeWorldPropData.oiWorldProps[iWorldProp], FALSE)
		ENDIF
		
		CLEAR_LONG_BIT(sRunTimeWorldPropData.iWorldPropInvincibleBitSet, iWorldProp)
		IF bPrintDebug PRINTLN("[WorldProps][WorldProp ", iWorldProp, "] CLEAN_UP_WORLD_PROP_OBJECT_REFERENCE - Disabling invincibility for world prop ", iWorldProp) ENDIF
	ENDIF
		
	IF bPrintDebug PRINTLN("[WorldProps][WorldProp ", iWorldProp, "] CLEAN_UP_WORLD_PROP_OBJECT_REFERENCE - Cleaning up object reference for world prop ", iWorldProp) ENDIF
	SET_OBJECT_AS_NO_LONGER_NEEDED(sRunTimeWorldPropData.oiWorldProps[iWorldProp])
	
ENDPROC

PROC CLEANUP_WORLD_PROPS()
	
	INT iWorldProp
	PRINTLN("[RCC MISSION][WORLDPROPS][WorldProp ", iWorldProp, "] CLEANUP_WORLD_PROPS - Cleaning up world props now!")
	DEBUG_PRINTCALLSTACK()
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps iWorldProp
		IF g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].mn = DUMMY_MODEL_FOR_SCRIPT
			RELOOP
		ENDIF
		
		IF IS_LONG_BIT_SET(sRunTimeWorldPropData.iWorldPropHiddenForCutsceneBitSet, iWorldProp)
		AND IS_A_STRAND_MISSION_BEING_INITIALISED()
			PRINTLN("[RCC MISSION][WORLDPROPS][WorldProp ", iWorldProp, "] CLEANUP_WORLD_PROPS - World Prop ", iWorldProp, " flagged for strand pass-over. Not deleting.")
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].iBitset, ciFMMC_WorldPropHidden)
			REMOVE_MODEL_HIDE(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].vPos, GET_WORLD_PROP_SEARCH_RADIUS(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp]), g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].mn, TRUE) 
			PRINTLN("[RCC MISSION][WORLDPROPS][WorldProp ", iWorldProp, "] CLEANUP_WORLD_PROPS - UN-Hidden World Prop ", iWorldProp, " at position: ", g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].vPos)
		ENDIF
		
		CLEAN_UP_WORLD_PROP_OBJECT_REFERENCE(iWorldProp, TRUE)
		
		CLEAR_WORLD_PROP_MODEL_SWAP(iWorldProp)
		
	ENDREPEAT
ENDPROC

PROC CLEANUP_PHONE_BOOK()
	INT i
	FOR i = 0 TO FMMC_MAX_DIALOGUES-1
		IF g_FMMC_STRUCT.sDialogueTriggers[i].iCallCharacter = -1
		OR g_FMMC_STRUCT.sDialogueTriggers[i].iCallCharacter >= 32
			PRINTLN("[RCC MISSION] CLEANUP_PHONE_BOOK - Skipping Dialogue Trigger ", i, " because g_FMMC_STRUCT.sDialogueTriggers[", i, "].iCallCharacter is ", g_FMMC_STRUCT.sDialogueTriggers[i].iCallCharacter)
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(iCharacterAddedToPhoneBook, g_FMMC_STRUCT.sDialogueTriggers[i].iCallCharacter)
			REMOVE_CONTACT_FROM_INDIVIDUAL_PHONEBOOK(GET_INCOMING_CONTACT_CHARACTER_FROM_CREATOR(g_FMMC_STRUCT.sDialogueTriggers[i].iCallCharacter), MULTIPLAYER_BOOK)
			CLEAR_BIT(iCharacterAddedToPhoneBook, g_FMMC_STRUCT.sDialogueTriggers[i].iCallCharacter)
			PRINTLN("[RCC MISSION] CLEANUP_PHONE_BOOK - Removing temporary contact from phone book for Dialogue Trigger ", i)
		ENDIF
	ENDFOR
ENDPROC

PROC CLEANUP_RAPPEL_ROPE()
	IF DOES_ROPE_EXIST(sWallRappel.RappelRopeID)
		DELETE_ROPE(sWallRappel.RappelRopeID)
	ENDIF
ENDPROC

PROC CLEANUP_CCTV_CONES()		
	INT iObj
	FOR iObj = 0 TO FMMC_MAX_NUM_OBJECTS-1
		REMOVE_CCTV_VISION_CONE(biObjBlip[iobj], iObj, FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT)
		REMOVE_CCTV_VISION_CONE(biDynoPropBlips[iobj], iObj, FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP)
	ENDFOR		
ENDPROC

FUNC BOOL CLEANUP_ENTER_SAFE_COMBINATION_MINIGAME(BOOL bIsEndOfMissionCleanup, OBJECT_INDEX oiSafeObj = NULL)
	
	IF bIsEndOfMissionCleanup
	
		PRINTLN("[SafeCombination] CLEANUP_ENTER_SAFE_COMBINATION_MINIGAME | Cleaning up minigame! (End of Mission)")
		
		IF IS_NAMED_RENDERTARGET_REGISTERED("safe_01a")
		OR sEnterSafeCombinationData.iSafeRenderTargetID > -1
			IF RELEASE_NAMED_RENDERTARGET("safe_01a")
				sEnterSafeCombinationData.iSafeRenderTargetID = -1
			ELSE
				PRINTLN("[SafeCombination] CLEANUP_ENTER_SAFE_COMBINATION_MINIGAME | Needs more time to release render target!")
				RETURN FALSE
			ENDIF
		ENDIF
		
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sEnterSafeCombinationData.siEnterSafeCombinationScaleformIndex)
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_HEI4/DLCHEI4_GENERIC_01")
		
	ELSE
	
		PRINTLN("[SafeCombination] CLEANUP_ENTER_SAFE_COMBINATION_MINIGAME | Cleaning up minigame! (Player done with it)")
		CLEAR_OBJECT_HACKING()
		
		CLEAR_HELP()
		
		IF DOES_ENTITY_EXIST(oiSafeObj)
			FREEZE_ENTITY_POSITION(oiSafeObj, TRUE)
		ENDIF
		
		DISPLAY_RADAR(TRUE)
		DISPLAY_HUD(TRUE)
		SET_WIDESCREEN_BORDERS(FALSE, 0)
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_ISLAND)
			// We loaded this audio bank during the minigame as opposed to it being loaded by default at the start of the mission - clean it up now
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_HEI4/DLCHEI4_GENERIC_01")
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC STOP_GLASS_CUTTING_LOOPING_HEAT_SOUND()
	IF sGlassCuttingData.iGlassCutting_HeatLoopSoundID > -1
		STOP_SOUND(sGlassCuttingData.iGlassCutting_HeatLoopSoundID)
		RELEASE_SOUND_ID(sGlassCuttingData.iGlassCutting_HeatLoopSoundID)
		sGlassCuttingData.iGlassCutting_HeatLoopSoundID = -1
	ENDIF
ENDPROC

PROC STOP_GLASS_CUTTING_LOOPING_CUT_SOUND()
	IF sGlassCuttingData.iGlassCutting_CuttingLoopSoundID > -1
		STOP_SOUND(sGlassCuttingData.iGlassCutting_CuttingLoopSoundID)
		RELEASE_SOUND_ID(sGlassCuttingData.iGlassCutting_CuttingLoopSoundID)
		sGlassCuttingData.iGlassCutting_CuttingLoopSoundID = -1
	ENDIF
ENDPROC

PROC CLEANUP_GLASS_CUTTING_MINIGAME_PTFX(BOOL bRemovePTFXAndAsset)

	IF bRemovePTFXAndAsset
		REMOVE_NAMED_PTFX_ASSET("scr_ih_fin")
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sGlassCuttingData.ptCutPTFX)
			PRINTLN("[GlassCutting] CLEANUP_GLASS_CUTTING_MINIGAME_PTFX | Stopping and cleaning up ptCutPTFX")
			STOP_PARTICLE_FX_LOOPED(sGlassCuttingData.ptCutPTFX)
		ENDIF
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sGlassCuttingData.ptOverheatPTFX)
			PRINTLN("[GlassCutting] CLEANUP_GLASS_CUTTING_MINIGAME_PTFX | Stopping and cleaning up ptOverheatPTFX")
			STOP_PARTICLE_FX_LOOPED(sGlassCuttingData.ptOverheatPTFX)
		ENDIF
	ELSE
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sGlassCuttingData.ptCutPTFX)
			PRINTLN("[GlassCutting] CLEANUP_GLASS_CUTTING_MINIGAME_PTFX | Putting ptCutPTFX into low power mode")
			SET_PARTICLE_FX_LOOPED_EVOLUTION(sGlassCuttingData.ptCutPTFX, "power", 0.0)
		ENDIF
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sGlassCuttingData.ptOverheatPTFX)
			PRINTLN("[GlassCutting] CLEANUP_GLASS_CUTTING_MINIGAME_PTFX | Setting ptOverheatPTFX to a low heat")
			SET_PARTICLE_FX_LOOPED_EVOLUTION(sGlassCuttingData.ptCutPTFX, "heat", 0.0)
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_GLASS_CUTTING_MINIGAME_ASSETS(BOOL bFinalCleanup)

	UNUSED_PARAMETER(bFinalCleanup)
	
	STOP_GLASS_CUTTING_LOOPING_CUT_SOUND()
	STOP_GLASS_CUTTING_LOOPING_HEAT_SOUND()
	
	CLEANUP_GLASS_CUTTING_MINIGAME_PTFX(TRUE)
	
	IF IS_BIT_SET(sGlassCuttingData.iBitset, ciGLASS_CUTTING_BITSET__STARTED_TIMECYCLE_MOD)
		PRINTLN("[GlassCutting] CLEANUP_GLASS_CUTTING_MINIGAME_ASSETS | Clearing timecycle mod!")
		CLEAR_TIMECYCLE_MODIFIER()
		CLEAR_BIT(sGlassCuttingData.iBitset, ciGLASS_CUTTING_BITSET__STARTED_TIMECYCLE_MOD)
	ENDIF
ENDPROC

PROC RESET_TEAM_HEALTH_REGEN()
	SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(LocalPlayer, 1.0)
	SET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(LocalPlayer, 0.5)
ENDPROC

PROC RESET_TEAM_DAMAGE_MOD()
	IF NOT IS_PED_WEARING_HEIST_ARMOUR(LocalPlayerPed)
		SET_PLAYER_WEAPON_DEFENSE_MODIFIER(LocalPlayer, FREEMODE_PLAYER_WEAPON_DEFENSE)
		SET_PLAYER_MELEE_WEAPON_DEFENSE_MODIFIER(LocalPlayer, FREEMODE_PLAYER_MELEE_WEAPON_DEFENSE)
	ENDIF
ENDPROC

PROC CLEAR_FOOTSTEP_SWEETENERS_FOR_ALL_PLAYER_PEDS()
	INT i
	PRINTLN("[PLAYER_LOOP] - CLEAR_FOOTSTEP_SWEETENERS_FOR_ALL_PLAYER_PEDS")
	FOR i = 0 TO NUM_NETWORK_PLAYERS - 1
		PED_INDEX ped = GET_PLAYER_PED(INT_TO_PLAYERINDEX(i))
		IF(IS_ENTITY_ALIVE(ped))
			USE_FOOTSTEP_SCRIPT_SWEETENERS(ped, FALSE, 0)
		ENDIF
	ENDFOR
ENDPROC

PROC REMOVE_CASINO_SCENARIO_BLOCKING_AREA()
	IF sbiCasinoValet != NULL
		REMOVE_SCENARIO_BLOCKING_AREA(sbiCasinoValet)
		PRINTLN("REMOVE_CASINO_SCENARIO_BLOCKING_AREA - Removed scenario blocking area for casino valet")
	ENDIF
ENDPROC

PROC CLEANUP_PLACED_TRAINS(FMMC_MISSION_SERVER_DATA_STRUCT &sSDSpassed)

	INT i
	FOR i = 0 TO FMMC_MAX_TRAINS - 1
		IF NETWORK_DOES_NETWORK_ID_EXIST(sSDSpassed.niTrain[i])
			PRINTLN("[Trains][Train ", i, "] CLEANUP_PLACED_TRAINS - cleaning up Train ", i)
			
			VEHICLE_INDEX viTempTrain = NET_TO_VEH(sSDSpassed.niTrain[i])
			
			IF GRAB_CONTROL_OF_ENTIRE_TRAIN(viTempTrain #IF IS_DEBUG_BUILD , i #ENDIF )
				DELETE_FMMC_TRAIN(i)
			ELSE
				SET_MISSION_TRAIN_AS_NO_LONGER_NEEDED(viTempTrain)
			ENDIF
		ENDIF
	ENDFOR
	DELETE_ALL_TRAINS() //Non-mission only - cleans up those set as no longer needed.
	
ENDPROC


PROC CLEANUP_PLACED_ENTITIES(FMMC_MISSION_SERVER_DATA_STRUCT &sSDSpassed)

	INT i

	// Peds
	NET_PRINT("CLEANUP_PLACED_ENTITIES - cleaning up peds - called by ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()		
	FOR i = 0 TO (FMMC_MAX_PEDS-1)	
		IF NETWORK_DOES_NETWORK_ID_EXIST(sSDSpassed.niPed[i])
			NET_PRINT("CLEANUP_PLACED_ENTITIES - cleaning up ped: ") NET_PRINT_INT( i ) NET_NL()
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSDSpassed.niPed[i])
				DELETE_NET_ID(sSDSpassed.niPed[i])
			ELSE
				CLEANUP_NET_ID(sSDSpassed.niPed[i])
			ENDIF
		ENDIF
	ENDFOR
	
	// Vehicles
	NET_PRINT("CLEANUP_PLACED_ENTITIES - cleaning up vehicles - called by ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()		
	FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
		IF NETWORK_DOES_NETWORK_ID_EXIST(sSDSpassed.niVehicle[i])
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSDSpassed.niVehicle[i])
				BOOL bWasAttatched = FALSE
				NET_PRINT("CLEANUP_PLACED_ENTITIES - cleaning up vehicle: ") NET_PRINT_INT( i ) NET_NL()
				VEHICLE_INDEX tempVeh = NET_TO_VEH(sSDSpassed.niVehicle[i])
				IF IS_VEHICLE_MODEL_A_CARGOBOB(GET_ENTITY_MODEL(tempVeh))
					IF IS_VEHICLE_DRIVEABLE(tempVeh)
						IF GET_VEHICLE_ATTACHED_TO_CARGOBOB(tempVeh) != NULL
							SET_CARGOBOB_FORCE_DONT_DETACH_VEHICLE(tempVeh, FALSE)
							DETACH_VEHICLE_FROM_CARGOBOB(tempVeh, tempVeh) //detach anything from this cargobob
							bWasAttatched = TRUE
						ENDIF
						SET_CARGOBOB_PICKUP_MAGNET_ACTIVE(tempVeh, FALSE)
						NET_PRINT("CLEANUP_PLACED_ENTITIES - detaching vehicles from cargobob: ") NET_PRINT_INT( i ) NET_NL()
					ENDIF
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(tempVeh)
					ENTITY_INDEX attachedEnt = GET_ENTITY_ATTACHED_TO(tempVeh)
					IF DOES_ENTITY_EXIST(attachedEnt)
					AND IS_VEHICLE_MODEL_A_CARGOBOB(GET_ENTITY_MODEL(attachedEnt))
						VEHICLE_INDEX attachedVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(attachedEnt)
						SET_CARGOBOB_FORCE_DONT_DETACH_VEHICLE(attachedVeh, FALSE)
						DETACH_VEHICLE_FROM_CARGOBOB(attachedVeh, tempVeh) //detach anything from this cargobob
						bWasAttatched = TRUE
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_EMPTY(NET_TO_VEH(sSDSpassed.niVehicle[i]))
				AND NOT bWasAttatched
					DELETE_NET_ID(sSDSpassed.niVehicle[i])
				ELSE
					CLEANUP_NET_ID(sSDSpassed.niVehicle[i])
				ENDIF
			ELSE
				CLEANUP_NET_ID(sSDSpassed.niVehicle[i])
			ENDIF
		ENDIF
	ENDFOR
	
	// Networked Objects (Objects, Dynoprops and Interactables)
	NET_PRINT("CLEANUP_PLACED_ENTITIES - cleaning up networked objects - called by ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()		
	FOR i = 0 TO FMMC_MAX_NUM_NETWORKED_OBJ_POOL - 1
		IF NETWORK_DOES_NETWORK_ID_EXIST(sSDSpassed.niNetworkedObject[i])
			NET_PRINT("CLEANUP_PLACED_ENTITIES - cleaning up networked object: ") NET_PRINT_INT( i ) NET_NL()
			
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSDSpassed.niNetworkedObject[i])
				DELETE_NET_ID(sSDSpassed.niNetworkedObject[i])
			ELSE
				CLEANUP_NET_ID(sSDSpassed.niNetworkedObject[i])
			ENDIF
		ENDIF
	ENDFOR
	
	// Trains
	NET_PRINT("CLEANUP_PLACED_ENTITIES - cleaning up trains - called by ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()		
	CLEANUP_PLACED_TRAINS(sSDSpassed)
	
	// Props
	INT iProp
	FOR iProp = 0 TO (GET_FMMC_MAX_NUM_PROPS()-1)
		IF DOES_ENTITY_EXIST(oiProps[iProp])
			DELETE_OBJECT(oiProps[iProp])
		ENDIF
	ENDFOR
	
	FOR iProp = 0 TO (FMMC_MAX_NUM_PROP_CHILDREN-1)
		IF DOES_ENTITY_EXIST(oiPropsChildren[iProp])
		   DELETE_OBJECT(oiPropsChildren[iProp])
		ENDIF
	ENDFOR 
		
	// This could be dangerous. Might be necessary due to this bug: url:bugstar:5632342
	CLEAR_AREA(<<0.0, 0.0, 0.0>>, 99999.9, TRUE)
	
ENDPROC

PROC CLEANUP_MISSION_ENTITIES(BOOL bStrand)
	
	BOOL bSkipEntityCleanup = FALSE
		
	IF bStrand
	AND NOT IS_MOCAP_CUTSCENE_A_PLACEHOLDER(FMMCCUT_ENDMOCAP #IF IS_DEBUG_BUILD , sMocapCutscene #ENDIF )
		PRINTLN("CLEANUP_MISSION_ENTITIES | Exiting due to bFinalCleanup")
		bSkipEntityCleanup = TRUE
	ENDIF
		
	IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_END_CUTSCENE_STARTED)
	OR IS_BIT_SET(iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_OVER) // url:bugstar:5830986 || (Cutscene Entities were being cleaned up by remote clients finishing it early...)
		PRINTLN("CLEANUP_MISSION_ENTITIES | Exiting due to LBOOL31_END_CUTSCENE_STARTED or LBOOL2_ACTIVATE_MOCAP_CUTSCENE_OVER")
		bSkipEntityCleanup = TRUE
	ENDIF
	
	IF bSkipEntityCleanup
		IF IS_FAKE_MULTIPLAYER_MODE_SET()
			 // Creator needs an extra train cleanup here to ensure the trains get cleaned up since we can't rely on NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS
			CLEANUP_PLACED_TRAINS(MC_serverBD_1.sFMMC_SBD)
		ENDIF
		
		// Entity cleanup is being skipped for one of the reasons above!
		EXIT
	ENDIF
	
	// Clean up networked placed entities
	CLEANUP_PLACED_ENTITIES(MC_serverBD_1.sFMMC_SBD)

	// cleanup respawn vehicle
	IF NETWORK_DOES_NETWORK_ID_EXIST(netRespawnVehicle)
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(netRespawnVehicle)
			DELETE_NET_ID(netRespawnVehicle)
		ELSE
			CLEANUP_NET_ID(netRespawnVehicle)
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_COVER_POINTS()	
	INT i = 0
	FOR i = 0 TO (FMMC_MAX_NUM_COVER-1)
		IF DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos)
			PRINTLN("CLEANUP_COVER_POINTS | Cleaning up Cover Point: ", i)
			REMOVE_COVER_POINT(cpPlacedCoverPoints[i])
		ENDIF
	ENDFOR	
ENDPROC

FUNC BOOL DOES_CONTENT_HAVE_UDS_FUNCTIONALITY()
	
//	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
//		RETURN TRUE
//	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_CUSTOM_PED_MODEL_CLEANUP()
	
	IF NOT IS_USING_CUSTOM_PLAYER_MODEL()
		EXIT
	ENDIF
	
	CLEAR_CUSTOM_PLAYER_MODEL()
	LocalPlayerPed = PLAYER_PED_ID()
	LocalPlayer = PLAYER_ID()		
	SET_CUSTOM_MP_HUD_COLOR(-1)
	
	g_bUseFranklinThemeInMP = FALSE
	PRINTLN("PROCESS_CUSTOM_PED_MODEL_CLEANUP - Setting g_bUseFranklinThemeInMP to FALSE")
	g_bUseLamarThemeInMP = FALSE
	PRINTLN("PROCESS_CUSTOM_PED_MODEL_CLEANUP - Setting g_bUseLamarThemeInMP to FALSE")
ENDPROC

/// PURPOSE:
///    Performs final cleanup and terminates the Mission Controller Script
/// PARAMS:
///    bFinalCleanUp - TRUE when cleaning up for all players, FALSE when called from a quitting/bailing player.
PROC MC_SCRIPT_CLEANUP(BOOL bFinalCleanUp = TRUE)
	
	INT i
	INT iPart = -1
	BOOL bStrand = IS_A_STRAND_MISSION_BEING_INITIALISED()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		iPart = PARTICIPANT_ID_TO_INT() //FMMC2020 - Look into replacing with iLocalPart when refactoring
	ENDIF		
	IF iPart = -1
	AND (iPartToUse != -1)
		iPart = iPartToUse
	ENDIF
	
	PRINTLN( "[RCC MISSION] **************MC_SCRIPT_CLEANUP*************** " )
	PRINTLN( "[RCC MISSION] MC_SCRIPT_CLEANUP - With iPart = ",iPart )
	PRINTLN( "[RCC MISSION] MC_SCRIPT_CLEANUP - bFinalCleanUp: ",bFinalCleanUp )
	DEBUG_PRINTCALLSTACK()
	
	#IF IS_DEBUG_BUILD
	PRINT_ALL_CURRENT_MISSION_STATE_RUNTIME_ERROR()	
	#ENDIF
	
	//Re-cache Player State
	PROCESS_CACHING_NETWORK_HOST()
	PROCESS_CACHING_LOCAL_PLAYER_STATE()
	
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - STARTING ################################")
	
	// FUNCTIONALISING THIS IS A WIP AT THE MOMENT - PLEASE TRY TO ADD TO, OR ADD NEW RELEVANT FUNCTIONS. THANK YOU.
	
	PROCESS_CUSTOM_PED_MODEL_CLEANUP()
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF DOES_CONTENT_HAVE_UDS_FUNCTIONALITY()
		END_SERIES_UDS_ACTIVITY(g_FMMC_STRUCT.iRootContentIDHash)
	ENDIF
	#ENDIF
	
	// Handles Screen Blur and other Post FX.
	UNLOAD_MISSION_SPECIFIC_POSTFX()
	
	CLEANUP_PLAYER_VISUAL_AIDS()
	
	TURN_OFF_ACTIVE_FILTERS()
	
	// Handles the cleaning up of PTFX, Particles, Effects, etc.
	UNLOAD_MISSION_SPECIFIC_PTFX()
	
	// Removes all Zones and cleans up assets
	CLEANUP_ZONES()
	
	// Cleans up all Bounds stuff (e.g blips and sounds)
	CLEANUP_BOUNDS()
	
	// Cleans up extra Interactable assets (e.g PTFX or sounds)
	CLEANUP_INTERACTABLE_INTERACTIONS()
	
	CLEANUP_ALL_TRAP_PTFX(MC_serverBD_2.sTrapInfo_Host)
	
	CLEANUP_PLACED_PTFX()
	
	CLEANUP_EXTRA_ENTITY_PTFX()
	
	CLEANUP_MODEL_SWAPS()
	
	CLEANUP_PLAYER_CACHED_VEHICLES()
	
	CLEANUP_HALLOWEEN_ADVERSARY_2022_SOUNDS()
	
	CLEANUP_CACHED_ORIGINAL_PLAYER_PEDS()
	
	CLEANUP_CUTSCENE_BINK_AND_RENDERTARGET()	
	
	CLEANUP_COVER_POINTS()
	
	REMOVE_ENTITY_PTFX_ASSETS()
	
	// Audio Scenes, Sound Banks, and Sound IDs are all destroyed/released here.
	UNLOAD_MISSION_SPECIFIC_AUDIO(bFinalCleanUp)
	
	CLEAR_LAST_ALIVE_SOUND_EFFECT()
	CLEANUP_MISSION_FLARE_PTFX()
		
	// Scaleform Movies, UI textures, all unloaded here.
	UNLOAD_MISSION_SPECIFIC_SCALEFORM()
	
	// Interiors and IPLS are unpinned/released in here.	
	UNLOAD_MISSION_SPECIFIC_INTERIORS_IPLS()
	
	// Special Models, Pickups or objects etc are all unloaded here.
	UNLOAD_MISSION_SPECIFIC_MODELS_AND_ENTITIES()
	
	// Any Anims that were specially loaded in are unloaded here.
	UNLOAD_MISSION_SPECIFIC_ANIMS()
	
	// Config Flags and other settings that were applied to vehicles during the mission are cleaned up
	RESET_VEHICLE_RELATED_CONFIGS()
	
	// Config Flags and other setting that were applied to players during the mission are cleaned up
	RESET_PLAYER_RELATED_CONFIGS()
	
	// General or External globals are reset here.
	RESET_MISSION_SPECIFIC_GLOBALS(bFinalCleanUp, bStrand)
	
	CLEAN_UP_INSTANCED_CONTENT_PORTAL_WHITEOUT()
	
	//Cleanup Test Mode Vehicle Weapons
	#IF IS_DEBUG_BUILD
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		CLEANUP_VEHICLE_WEAPONS(sVehicleWeaponsData)
	ENDIF
	#ENDIF
	
	CLEANUP_PHONE_BOOK()
		
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Resetting DISABLE_CUSTOM_VEHICLE_NODES_FOR_ALL_PROPERTIES to false.")
	DISABLE_CUSTOM_VEHICLE_NODES_FOR_ALL_PROPERTIES(FALSE)
	CLEAR_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_GEN)	
	Make_Ped_Sober(localPlayerPed)
	Quit_Drunk_Camera_Immediately()		
	PRINTLN("Calling CLEANUP_MODE_LEAVERS_ON_OPTION for Mission")
	CLEANUP_MODE_LEAVERS_ON_OPTION()
	CLEANUP_BLIMP(sBlimpSign)
	DO_BLIMP_SIGNS(sBlimpSign, TRUE)	
	RESET_WEAPON_DAMAGE_MODIFIERS()
	RESET_WEAPON_AREA_OF_EFFECT()
	RESET_TEAM_HEALTH_REGEN()
	CLEANUP_RAPPEL_ROPE()
	CLEANUP_CCTV_CONES()
	CLEANUP_ENTER_SAFE_COMBINATION_MINIGAME(TRUE)
	CLEANUP_GLASS_CUTTING_MINIGAME_ASSETS(TRUE)
	CLEAN_UP_ALL_SHARED_RENDERTARGETS()
	CLEANUP_ALL_ENTITY_LOOPING_AUDIOS()
	
	SET_LOCK_ADAPTIVE_DOF_DISTANCE(FALSE)	
	
	SET_VEHICLE_COMBAT_MODE(FALSE)
	SET_IN_STUNT_MODE(FALSE)
	ALLOW_PLAYER_HEIGHTS(FALSE)

	IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_STROMBERG_FORCE_GPS_ACTIVE)
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Setting Force show GPS back to false")
		SET_FORCE_SHOW_GPS(FALSE)
	ENDIF	
		
	IF fLightIntesityScale >= 0
		SET_LIGHT_OVERRIDE_MAX_INTENSITY_SCALE(fLightIntesityScale)
	ENDIF
	
	IF HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_GRENADE)
		PRINTLN("[RCC MISSION] TURNING OFF INFINITE AMMO")
		SET_PED_INFINITE_AMMO(LocalPlayerPed, FALSE, WEAPONTYPE_GRENADE)
	ENDIF
		
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		TOGGLE_PAUSED_RENDERPHASES(TRUE)
		TOGGLE_PAUSED_RENDERPHASES(TRUE)
	ENDIF
		
	IF GET_IDLE_KICK_TIME_OVERRIDE_MS() != -1
		CLEAR_IDLE_KICK_TIME_OVERRIDDEN()
	ENDIF	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TEAM_SCORE_ON_BLIPS)
		INT iPlayBlip
		PRINTLN("[PLAYER_LOOP] - MC_SCRIPT_CLEANUP 4")
		FOR iPlayBlip = 0 TO (MAX_NUM_MC_PLAYERS - 1)
			SET_CUSTOM_BLIP_NUMBER_FOR_PLAYER(INT_TO_PLAYERINDEX(i),0, FALSE)
		ENDFOR
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_SET_STUN_GUN_AS_LIMITED)
		SET_PED_STUN_GUN_FINITE_AMMO(LocalPlayerPed, FALSE)
		PRINTLN("[StunGun] Resetting SET_PED_STUN_GUN_FINITE_AMMO")
	ENDIF
	
	IF DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE(g_BossSpecData.specCamData)
		IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
			CPRINTLN(DEBUG_SPECTATOR, "MC_SCRIPT_CLEANUP - DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE() = TRUE")
						
			TOGGLE_RENDERPHASES( FALSE )
			DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, DSCF_BAIL_FOR_TRANSITION)
			NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
		ENDIF
	ENDIF
	CLEAR_DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE(g_BossSpecData.specCamData)		
	
	IF bLocalPlayerPedOK
		SET_PED_DIES_INSTANTLY_IN_WATER( LocalPlayerPed, FALSE )
	ENDIF
	
	SET_CREATE_RANDOM_COPS( TRUE )
	SET_PED_MODEL_IS_SUPPRESSED(S_M_M_Security_01, FALSE)
	SET_PED_MODEL_IS_SUPPRESSED(S_M_M_PrisGuard_01, FALSE)
	
	IF HAS_NET_TIMER_STARTED(tdRaceMissionTime)
		RESET_NET_TIMER(tdRaceMissionTime)
	ENDIF
	
	IF SHOULD_CLEAR_QUEUED_JOIN_REQUESTS_BASED_ON_EOJ_VOTE_STATUS()
		NETWORK_REMOVE_ALL_QUEUED_JOIN_REQUESTS()
		PRINTLN("[RCC MISSION] NETWORK_REMOVE_ALL_QUEUED_JOIN_REQUESTS called")
	ENDIF
	
	CLEAR_ENTITY_LAST_DAMAGE_ENTITY(localPlayerPed)
	PRINTLN("[RCC MISSION][ASSISTS][LM] Clearing the last damage event on localPlayerPed due to round end.")
		
	//Reset lowered heatmaps for friendly players
	INT iParticipant	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_DAMAGE_TRACKER_ACTIVE_ON_PLAYER(LocalPlayer)
			ACTIVATE_DAMAGE_TRACKER_ON_PLAYER(LocalPlayer,FALSE)
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(LocalPlayerPed)
			PRINTLN("[RCC MISSION][ASSISTS]Clearing Damage tracker on Local Player")
		ELSE
			PRINTLN("[RCC MISSION][ASSISTS]Damage tracker isn't active on Local Player")
		ENDIF
		
		IF IS_DAMAGE_TRACKER_ACTIVE_ON_PLAYER(LocalPlayer)
			ACTIVATE_DAMAGE_TRACKER_ON_PLAYER(LocalPlayer,TRUE)
			PRINTLN("[RCC MISSION][ASSISTS] Reactivating damage tracker on local player")
		ENDIF
		
		PRINTLN("[PLAYER_LOOP] - MC_SCRIPT_CLEANUP 2")
		FOR iParticipant = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() -1
			IF iParticipant < MAX_NUM_MC_PLAYERS
				PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(iParticipant)
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
					PLAYER_INDEX 	playerIndex = NETWORK_GET_PLAYER_INDEX(participantIndex)
					PED_INDEX		playerPedIndex = GET_PLAYER_PED( playerIndex )
					
					IF NOT IS_PED_INJURED(playerPedIndex)
						DISABLE_PED_HEATSCALE_OVERRIDE(playerPedIndex)
						
						PRINTLN("[ThermalVision] DISABLE_PED_HEATSCALE_OVERRIDE ", iParticipant)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
		REMOVE_PLAYER_CUSTOM_BLIP_SPRITE()
	ENDIF
		
	IF IS_PLAYSTATION_PLATFORM()
		CLEAR_CONTROL_LIGHT_EFFECT(PLAYER_CONTROL)
	ENDIF
	
	//Check if it needs updating
	IF DOES_BLIP_EXIST(DestinationBlip)	
		REMOVE_BLIP(DestinationBlip)
		PRINTLN( "MC_SCRIPT_CLEANUP [RCC MISSION] - VIP MARKER REMOVED  ")
	ENDIF	
	INT iplayerloop
	PRINTLN("[PLAYER_LOOP] - MC_SCRIPT_CLEANUP 5")
	FOR iplayerloop = 0 TO (NUM_NETWORK_PLAYERS-1)
		//turn off flashing player blips if they are still flashing
		PLAYER_INDEX tempPlayer = INT_TO_PLAYERINDEX(iplayerloop)
		FLASH_BLIP_FOR_PLAYER(tempPlayer, FALSE)

		//turn on any hidden health bars
		IF IS_BIT_SET(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iHideHealthBarsBitset[0], iplayerloop)
			CLEAR_BIT(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iHideHealthBarsBitset[0], iplayerloop)
		ENDIF
	ENDFOR	
	INT iloc	
	FOR i = 0 TO  i_NUMBER_OF_GRADIENT_BLIPS -1
		FOR iloc = 0 TO FMMC_MAX_GO_TO_LOCATIONS - 1
			IF DOES_BLIP_EXIST(LineGradientBlips[ i ][ iloc ])
				REMOVE_BLIP(LineGradientBlips[ i ][ iloc ])
			ENDIF
		ENDFOR
	ENDFOR
	IF DOES_BLIP_EXIST(biRegenBlip)
		REMOVE_BLIP(biRegenBlip)
		PRINTLN("[JS][TEAMREGEN] - MC_SCRIPT_CLEANUP removing blip")
	ENDIF
		
	//Reset Pickup Generation Range
	SET_PICKUP_GENERATION_RANGE_MULTIPLIER(1.0)

	CLEAR_GPS_MULTI_ROUTE()
	
	SET_GPS_MULTI_ROUTE_RENDER(FALSE)
	
	SET_IGNORE_NO_GPS_FLAG(FALSE)
		
	IF MP_FORCE_TERMINATE_INTERNET_ACTIVE()
		MP_FORCE_TERMINATE_INTERNET_CLEAR()
		PRINTLN("[RCC MISSION] MP_FORCE_TERMINATE_INTERNET_ACTIVE - MP_FORCE_TERMINATE_INTERNET_CLEAR() called by MC_SCRIPT_CLEANUP")
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_NAVMESH_REQUIRED_REGION_IN_USE()
			REMOVE_NAVMESH_REQUIRED_REGIONS()
		ENDIF		
	ENDIF		
	
	IF NOT g_bCelebrationScreenIsActive // So we won't unpause the renderphases while the celebration is active. Need the screen to be frozen for them. 
		IF IS_BIT_SET( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )
			TOGGLE_RENDERPHASES( TRUE )
			CLEAR_BIT( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )
			PRINTLN("[RCC MISSION] - LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF set false- 1")
		ENDIF
	ENDIF
	
	INT iBargeProp = 0
	REPEAT 2 iBargeProp
		IF DOES_ENTITY_EXIST(g_FMMC_STRUCT_ENTITIES.oiBargeCollisionProps[iBargeProp])
			DELETE_OBJECT(g_FMMC_STRUCT_ENTITIES.oiBargeCollisionProps[iBargeProp])
		ENDIF
	ENDREPEAT

	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		MAINTAIN_ALL_ENTITIES_WHEN_ON_STRAND_CUTSCENE(TRUE)
		CLEAR_ALL_ENTITY_CHECKPOINT_CONTINUITY_VARS()
	ENDIF
	
	IF (iPart != -1)
	AND (NOT IS_BIT_SET(MC_playerBD[ iPart ].iClientBitSet,PBBOOL_START_SPECTATOR))
		IF NOT bIsSCTV
			//-- DOn't remove weapons if there's another mission coming
			IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
				DEAL_WITH_DLC_WEAPONS(iDlcWeaponBitSet, FALSE)
			ELSE
				PRINTLN("[RCC MISSION] [dsw] Cleanup not removing DLC weapons as strand mission or a test mission")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_ALL_SHOPS_LOCKED)
		SET_ALL_SHOP_LOCATES_ARE_BLOCKED(FALSE)
		SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
		HIDE_ALL_SHOP_BLIPS(FALSE)
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - SET_ALL_SHOP_LOCATES_ARE_BLOCKED updated to FALSE ")
		CLEAR_BIT(iLocalBoolCheck10, LBOOL10_ALL_SHOPS_LOCKED)
	ENDIF
		
	g_bEnableHackingApp = FALSE
	g_HackingAppHasBeenLaunched = FALSE	
	g_FMMC_STRUCT.bDisablePhoneInstructions = FALSE
	g_FMMC_STRUCT.bRememberToCleanUpHacking = FALSE
	IF NETWORK_IS_GAME_IN_PROGRESS()
		CLEAR_FOOTSTEP_SWEETENERS_FOR_ALL_PLAYER_PEDS()
	ENDIF
	
	iVehDamageTrackerBS = 0
	
	IF HAS_SCALEFORM_MOVIE_LOADED(uiGoStopDisplay)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(uiGoStopDisplay)
	ENDIF
	
	IF NOT IS_THIS_A_ROUNDS_MISSION()
	OR SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
	OR NOT bFinalCleanUp
		SET_RANDOM_TRAINS(TRUE)
		PRINTLN("[Trains] MC_SCRIPT_CLEANUP - Re-enabling SET_RANDOM_TRAINS and SET_RANDOM_BOATS_MP")
		SET_RANDOM_BOATS_MP(TRUE)
	ENDIF
	
	g_iApartmentDropoffObjectsBitset = 0
	
	g_bFailedMission = FALSE
	
	IF IS_PERSONAL_VEHICLE_FUNCTIONALITY_DISABLED_IN_MISSIONS()
		DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION( FALSE ) 
	ENDIF
	
	SET_DO_VISIBILITY_CHECKS_FOR_SPAWNING(TRUE)
	
	RESET_CARMOD_MENU_OPTION_FOR_CREATOR_MISSION()
	
	IF DOES_ENTITY_EXIST(e_FakePlayerPhone)
		DELETE_OBJECT(e_FakePlayerPhone)
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Deleted Fake Hacking Phone object.")
		
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPCircuitHack2")
			SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPCircuitHack2")
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Unloaded Hacking Render to Phone Sprite Dict.")
		ENDIF
	ENDIF
	
	CLEAR_RESTRICTED_AREA_BLOCKING()
	
	// Return weapons at the end
    IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		
		IF IS_JOB_FORCED_WEAPON_ONLY()		// Return weapons if we've been in a deathmatch
	    OR IS_JOB_FORCED_WEAPON_PLUS_PICKUPS()
		OR ((iPart != -1) AND SHOULD_CURRENT_PLAYER_HAVE_WEAPONS_REMOVED( MC_playerBD[ iPart ].iteam ))	// Or if the mission requested the weapons to be removed
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_RemoveMissionRestrictedWeapons)
			IF NOT IS_FORCED_WEAPON_STRAND_BEING_INITIALISED()
				PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP calling GIVE_MP_REWARD_WEAPON_IN_FREEMODE")
				GIVE_MP_REWARD_WEAPON_IN_FREEMODE( TRUE, FALSE, WEAPONINHAND_FIRSTSPAWN_HOLSTERED, FALSE, FALSE, FALSE, SHOULD_GIVE_WEAPONS_BACK_THROUGH_GIVE_MP_REWARD_WEAPON(MC_playerBD[iPart].iteam))
		        IF wtWeaponToRemove <> WEAPONTYPE_INVALID
		            IF HAS_PED_GOT_WEAPON( LocalPlayerPed, wtWeaponToRemove )
		                REMOVE_WEAPON_FROM_PED( LocalPlayerPed, wtWeaponToRemove )
		                PRINTLN("[RCC MISSION] - REMOVING WEAPON LOANED AT START")
		            ENDIF
		    	ENDIF
			ENDIF
		ENDIF
		
		IF ((iPart != -1) AND DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY(MC_playerBD[iPart].iTeam) )
		OR IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSet, ciREMOVE_WEAPONS )	// Or if the mission requested the weapons to be removed
		OR IS_BIT_SET( g_FMMC_STRUCT.iWepRestBS, REST_TEAM_1 ) // Or if we removed a particular team's weapons
		OR IS_BIT_SET( g_FMMC_STRUCT.iWepRestBS, REST_TEAM_2 )
		OR IS_BIT_SET( g_FMMC_STRUCT.iWepRestBS, REST_TEAM_3 )
		OR IS_BIT_SET( g_FMMC_STRUCT.iWepRestBS, REST_TEAM_4 )
			
			IF (g_bMissionInventory OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_OVERRIDE_AMMUNATION_FORCE_CLOSE))
			AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			OR NOT bFinalCleanUp
				PRINTLN("[RCC MISSION] DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY OR ciREMOVE_WEAPONS set - Resetting weapons...")
				IF NOT HAS_PLAYER_ONLY_HAD_RESTRICTED_WEAPONS_REMOVED(MC_playerBD[iPart].iteam)
					REMOVE_ALL_WEAPONS()
				ENDIF
				RESET_PLAYERS_WEAPONS_OBTAINED()
				IF GET_PED_ARMOUR(LocalPlayerPed) > 0
					SET_PED_ARMOUR(LocalPlayerPed, 0)
				ENDIF
				g_bMissionInventory = FALSE
				IF SHOULD_GIVE_WEAPONS_BACK_THROUGH_GIVE_MP_REWARD_WEAPON(MC_playerBD[iPart].iteam)
					g_GIVE_PLAYER_ALL_UNLOCKED_AND_EQUIPED_WEAPONS_FM_CHECK = TRUE
					PRINTLN("[RCC MISSION] Setting g_GIVE_PLAYER_ALL_UNLOCKED_AND_EQUIPED_WEAPONS_FM_CHECK to TRUE")
				ELSE
					g_GIVE_PLAYER_ALL_UNLOCKED_AND_EQUIPED_WEAPONS_FM_CHECK = FALSE
					PRINTLN("[RCC MISSION] Setting g_GIVE_PLAYER_ALL_UNLOCKED_AND_EQUIPED_WEAPONS_FM_CHECK to FALSE")
				ENDIF
			ENDIF
			
		ENDIF
		
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		OR NOT bFinalCleanUp
			g_bMissionInventory = FALSE
		ENDIF
		
		//Restore Parachute Tints
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		AND NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_SET_UP_STRAND_DETAILS_AT_END_OF_MISSION)
			EQUIP_STORED_MP_PARACHUTE(LocalPlayer)
		ENDIF		
	ENDIF
	
	//If the cut scene has
	IF bFinalCleanUp
		CLEANUP_SCRIPTED_CUTSCENE(bFinalCleanUp)		
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
		SET_NETWORK_CUTSCENE_ENTITIES(FALSE)
	ENDIF
	
	NETWORK_SET_TASK_CUTSCENE_INSCOPE_MULTIPLER(1.0)
	
	SET_ENABLE_VEHICLE_SLIPSTREAMING(FALSE)
	
	SET_HIDOF_OVERRIDE(FALSE,FALSE,0,0,0,0)
	
	SET_DISABLE_RANK_UP_MESSAGE(FALSE)
	g_bAllowJobReplay = FALSE
	g_MaskObjectiveActive = FALSE
	
	SET_USE_DLC_DIALOGUE(FALSE)
	
	IF DOES_PLAYER_HAVE_A_BAG( LocalPlayerPed )
		REMOVE_PLAYER_BAG()
	ENDIF
	
	//Clear the prop spawn bitsets
	INT iTeamLoop
	FOR iTeamLoop = 0 TO 3
		LocalRandomSpawnBitset[iTeamLoop] = 0
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND bIsLocalPlayerHost
			MC_ServerBD.iSpawnPointsUsed[iTeamLoop] = 0
		ENDIF
		IF IS_BIT_SET(iTeamPropSpawns, iTeamLoop)
			CLEAR_BIT(iTeamPropSpawns, iTeamLoop)
		ENDIF
	ENDFOR
	
	//Reseting Melee Damage to Normal
	IF bTakedownDamageModifier
		IF NETWORK_IS_GAME_IN_PROGRESS()
			INT iDamageModLoop
			PRINTLN("[PLAYER_LOOP] - MC_SCRIPT_CLEANUP")
			FOR iDamageModLoop = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() -1
				PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iDamageModLoop)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
					SET_PLAYER_WEAPON_TAKEDOWN_DEFENSE_MODIFIER(tempPlayer, 1.0)
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
	
	// Spectator cam
    IF NOT IS_THIS_SPECTATOR_CAM_OFF(g_BossSpecData.specCamData)
		FORCE_CLEANUP_SPECTATOR_CAM(g_BossSpecData, TRUE)
    ENDIF
	
	CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_ALLOW_SPECTATING_RIVAL_TEAMS)
	MPSpecGlobals.iPreferredSpectatorPlayerID = -1
	
	CLEANUP_END_JOB_VOTING(nextJobStruct)
	
	SET_PED_USING_ACTION_MODE(LocalPlayerPed, FALSE)
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		SET_ENTITY_PROOFS(LocalPlayerPed,FALSE,FALSE,FALSE,FALSE,FALSE)
	ENDIF
	
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
		
	SET_STORE_ENABLED(TRUE)
	
	CLEAR_TIMECYCLE_MODIFIER()
	
	OVERRIDE_INTERIOR_SMOKE_END()
	
	UNLOAD_ALL_COMMON_LEADERBOARD_ELEMENTS(LBPlacement)
	
	SHOW_ALL_PLAYER_BLIPS(FALSE)
	SET_ALL_PLAYER_BLIPS_LONG_RANGE(FALSE)
	
	HIDE_SPECTATOR_HUD(FALSE)
	
	IF (iPart != -1)
		SET_BIT(MC_PlayerBD[iPart].iClientBitSet,PBBOOL_TERMINATED_SCRIPT)
	ENDIF
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		SET_AUTO_GIVE_PARACHUTE_WHEN_ENTER_PLANE(LocalPlayer,FALSE)
	ENDIF
	
	CLEAR_ALL_SPECTATOR_TARGET_LISTS(g_BossSpecData.specHUDData)	
		
	//Clear Radio Station Over Score (Final Round Only)
	IF NOT (IS_THIS_A_ROUNDS_MISSION() AND NOT SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD())
		g_SpawnData.bRadioStationOverScore = FALSE
		PRINTLN("MC_SCRIPT_CLEANUP - g_SpawnData.bRadioStationOverScore = FALSE")
	ENDIF
		
	FOR i = 0 TO (FMMC_MAX_TEAMS-1)
		NETWORK_OVERRIDE_TEAM_RESTRICTIONS(i, FALSE)
		PRINTLN("[MMacK][VoiceChat] Setting All Teams NETWORK_OVERRIDE_TEAM_RESTRICTIONS to FALSE ... Team = ", i)
	ENDFOR
	MC_playerBD[iLocalPart].iAdditionalPlayerLives = 0
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PRINTLN("[PLAYER_LOOP] - MC_SCRIPT_CLEANUP 3")
		FOR i = 0 TO (MAX_NUM_MC_PLAYERS - 1)
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				PLAYER_INDEX playerIndex = NETWORK_GET_PLAYER_INDEX(tempPart)
				
				NETWORK_OVERRIDE_SEND_RESTRICTIONS(playerIndex, FALSE)
				PRINTLN("[VoiceChat] Setting All Players NETWORK_OVERRIDE_SEND_RESTRICTIONS to FALSE... Name = ", GET_PLAYER_NAME(playerIndex))
				NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS(playerIndex, FALSE)
				PRINTLN("[VoiceChat] Setting All Players NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS to FALSE... Name = ", GET_PLAYER_NAME(playerIndex))
			ENDIF
		ENDFOR
	ENDIF
	
	NETWORK_OVERRIDE_SEND_RESTRICTIONS_ALL(FALSE)
	NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS_ALL(FALSE)
	
	NETWORK_SET_OVERRIDE_SPECTATOR_MODE(FALSE)
	
	g_iOverheadNamesState = -1
	
	g_bMissionReadyForAM_VEHICLE_SPAWN = FALSE
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== Mission === g_bMissionReadyForAM_VEHICLE_SPAWN = FALSE")
	#ENDIF
	
	SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(DUMMY_MODEL_FOR_SCRIPT,-1)
	
	CLEAR_A_INVENTORY_BOX_CONTENT(ci_Ped_Inventory)
	CLEAR_A_INVENTORY_BOX_CONTENT(ci_veh_Inventory)
	CLEAR_A_INVENTORY_BOX_CONTENT(ci_obj_Inventory)
	CLEAR_INVENTORY_BOX_CONTENT()
	
	CLEANUP_ALL_AI_PED_BLIPS(biHostilePedBlip)

	CLEANUP_SOCIAL_CLUB_LEADERBOARD(scLB_control)
	CLEAR_RANK_REDICTION_DETAILS() 
	
	SET_ARTIFICIAL_VEHICLE_LIGHTS_STATE(TRUE)
	SET_ARTIFICIAL_LIGHTS_STATE(FALSE)
	CLEAR_TIMECYCLE_MODIFIER()
	RELEASE_NAMED_SCRIPT_AUDIO_BANK(GET_ARTIFICIAL_LIGHTS_AUDIOBANK__LIGHTS_OFF())
	RELEASE_NAMED_SCRIPT_AUDIO_BANK(GET_ARTIFICIAL_LIGHTS_AUDIOBANK__LIGHTS_BACK_ON())
	g_bFMMCLightsTurnedOff = FALSE

	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF GET_JOINING_GAMEMODE() = GAMEMODE_CREATOR
			IF DOES_ENTITY_EXIST(LocalPlayerPed)
				PRINTLN("[RCC MISSION][JJT] MC_SCRIPT_CLEANUP - Heading into the creator, calling SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE")
				Clear_All_Generated_MP_Headshots(FALSE)
				IF NOT GB_OUTFITS_IS_PLAYER_WEARING_GANG_OUTFIT(LocalPlayer)
					SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//if garage_model_hide_set
	if enum_to_int(garage_door_model) != 0
		remove_model_hide(<<-29.32, -1086.63, 26.95>>, 1.0, garage_door_model)
	endif 
	
	IF DOES_CAM_EXIST(camEndScreen)
		DESTROY_CAM(camEndScreen)
	ENDIF
	
	if does_cam_exist(cutscenecam)
		destroy_cam(cutscenecam)
	endif 
	
	NETWORK_SET_VOICE_ACTIVE(true)
	
	PRINTLN("[dsw] [RCC MISSION] Deleteing mission text messages")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("MIS_CUST_TXT2")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("MIS_CUST_TXT3")
	PURGE_TEXT_MESSAGES_FOR_MULTIPLAYER()
	
	// Door Cleanup
	CLEANUP_DOORS()
	
	IF IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCMissionBS, iFMMCMissionBS__MISSION_CONTROLLER_IS_CONTROLLING_DOORS)
		CLEAR_BIT(g_TransitionSessionNonResetVars.iFMMCMissionBS, iFMMCMissionBS__MISSION_CONTROLLER_IS_CONTROLLING_DOORS)
		PRINTLN("[RCC MISSION][Doors] Clearing iFMMCMissionBS__MISSION_CONTROLLER_IS_CONTROLLING_DOORS")
	ENDIF
	
	CLEAR_ALL_MISSION_CONTROL_NET_DOOR_OVERRIDE() //clears all conor's networked doors.
	
	CLEANUP_WORLD_PROPS()	
	
	FOR i = 0 TO (FMMC_MAX_PEDS-1)
		IF IS_LONG_BIT_SET(iPedTagCreated, i) 
	        REMOVE_PED_OVERHEAD_ICONS(iPedTag[i], i, iPedTagCreated, iPedTagInitalised)
	    ENDIF
		IF DOES_BLIP_EXIST(biPedBlip[i])
			REMOVE_BLIP(biPedBlip[i])
		ENDIF
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[i])
			CLEANUP_COLLISION_ON_PED(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i]), i)
		ELSE
			CLEANUP_COLLISION_ON_PED(NULL, i)
		ENDIF
	ENDFOR
	
	FOR i = 0 TO FMMC_MAX_PEDS - 1
		IF DOES_BLIP_EXIST(biCaptureRangeBlip[i])
			REMOVE_BLIP(biCaptureRangeBlip[i])
		ENDIF
	ENDFOR

	FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
		IF DOES_BLIP_EXIST(biVehBlip[i])
			REMOVE_VEHICLE_BLIP(i)
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP removing veh: ",i)
		ENDIF
		
		IF DOES_BLIP_EXIST(biVehCaptureRangeBlip[i])
			REMOVE_BLIP(biVehCaptureRangeBlip[i])
		ENDIF
	ENDFOR
	
	FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
		REMOVE_OBJECT_BLIP(i)
		IF DOES_BLIP_EXIST(biObjCaptureRangeBlip[i])
			REMOVE_BLIP(biObjCaptureRangeBlip[i])
		ENDIF
	ENDFOR
	CLEAR_FAKE_CONE_ARRAY()
	
	// Remove pickup blips
	FOR i = 0 TO (FMMC_MAX_WEAPONS-1)
		IF DOES_BLIP_EXIST(biPickup[i])
			REMOVE_BLIP(biPickup[i])
		ENDIF
		IF DOES_PICKUP_EXIST(pipickup[i])
			REMOVE_PICKUP(pipickup[i])
		ENDIF
	ENDFOR
	
	CLEAN_UP_SPAWN_VEHICLE_BLIPS()
	
	IF bLocalPlayerPedOK
		IF bStopDurationBlockSeatShuffle
			IF GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat)
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat, FALSE)
				
				bStopDurationBlockSeatShuffle = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF bLocalPlayerOK
		SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableVoiceDrivenMouthMovement , FALSE)
		
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			CLEAR_PED_TASKS(LocalPlayerPed)
			RESET_PED_MOVEMENT_CLIPSET(LocalPlayerPed)
			RESET_PED_WEAPON_MOVEMENT_CLIPSET(LocalPlayerPed)
			CLEAR_PED_FALL_UPPER_BODY_CLIPSET_OVERRIDE(LocalPlayerPed)
			//Don't remove the players wanted level in a strand mission
			g_bMissionRemovedWanted = TRUE
			SET_PLAYER_WANTED_LEVEL(LocalPlayer,0)
			SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
			PRINTLN("MC_SCRIPT_CLEANUP - Cleaning up player's wanted level now")			
		ENDIF
	ENDIF
	
	IF bFinalCleanUp
		
		PRINTLN("MC_SCRIPT_CLEANUP - bFinalCleanup is TRUE! bStrand = ",bStrand,", MC_serverBD.iEndCutscene = ",MC_serverBD.iEndCutscene)
		
		IF bLocalPlayerOK
		AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		AND NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_SET_UP_STRAND_DETAILS_AT_END_OF_MISSION)
			IF NOT IS_PED_RAGDOLL(LocalPlayerPed)
			OR IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_USING_PV)
			CLEANUP_MP_SAVED_VEHICLE(TRUE)
		ENDIF
		
		IF IS_NET_PLAYER_OK(LocalPlayer,FALSE)
		AND (NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_END_CUTSCENE_STARTED) AND NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_OVER))
			IF (NOT bStrand AND NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE ))
			OR (IS_MOCAP_CUTSCENE_A_PLACEHOLDER(FMMCCUT_ENDMOCAP #IF IS_DEBUG_BUILD , sMocapCutscene #ENDIF ))
				VEHICLE_INDEX LastVeh = GET_LAST_DRIVEN_VEHICLE()
				
				IF DOES_ENTITY_EXIST(LastVeh) 					
					PRINTLN("MC_SCRIPT_CLEANUP - LastVeh = ", NETWORK_ENTITY_GET_OBJECT_ID(LastVeh))
					
					IF NETWORK_HAS_CONTROL_OF_ENTITY(LastVeh)
						PRINTLN("MC_SCRIPT_CLEANUP - LastVeh - We Have Control.")
						
						IF CAN_REGISTER_MISSION_VEHICLES(1)
							PRINTLN("MC_SCRIPT_CLEANUP - LastVeh - Can Register Additional Vehicles")
							IF NOT IS_VEHICLE_A_PERSONAL_VEHICLE(LastVeh) // let neil's pv system manage themselves.
							OR (NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(LastVeh) AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(LastVeh))
								PRINTLN("MC_SCRIPT_CLEANUP - LastVeh - Registered with the Mission Controller")
								IF IS_VEHICLE_DRIVEABLE(LastVeh)
									IF IS_ENTITY_ATTACHED(LastVeh)
										DETACH_ENTITY(LastVeh) //for the magnet stuff
									ENDIF
								ENDIF
								
								IF NOT NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(LastVeh) 
									PRINTLN("[MC_SCRIPT_CLEANUP][RH] Network has control of LastVeh but it is not registered with the Mission Controller. Registering now")
									SET_ENTITY_AS_MISSION_ENTITY(LastVeh, bIsLocalPlayerHost, TRUE) 
								ENDIF
								
								IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(LastVeh)
									PRINTLN("MC_SCRIPT_CLEANUP - LastVeh - Deleting Vehicle")
								
									IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
										CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
									ENDIF
																		
									IF IS_VEHICLE_A_CARGOBOB(LastVeh)
										ENTITY_INDEX vehcargo = GET_VEHICLE_ATTACHED_TO_CARGOBOB(LastVeh)
										IF DOES_ENTITY_EXIST(vehcargo)
											IF IS_ENTITY_A_VEHICLE(vehCargo)
												VEHICLE_INDEX vehCargoIndex = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(vehCargo)
												DETACH_VEHICLE_FROM_ANY_CARGOBOB(vehCargoIndex)
											ENDIF
										ENDIF
									ENDIF
									
									DELETE_VEHICLE(LastVeh)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						PRINTLN("MC_SCRIPT_CLEANUP - LastVeh - We Do Not Have Control.")
						
						IF NOT IS_VEHICLE_A_PERSONAL_VEHICLE(LastVeh) // let neil's pv system manage themselves.
						AND (NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(LastVeh) AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(LastVeh))
							PRINTLN("MC_SCRIPT_CLEANUP - LastVeh - Registered with the Mission Controller")
							
							IF NETWORK_IS_GAME_IN_PROGRESS()
								PRINTLN("MC_SCRIPT_CLEANUP - LastVeh - Can Reigster Additional Vehicles")
								
								IF bIsLocalPlayerHost
									IF NOT NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(LastVeh)
										PRINTLN("[MC_SCRIPT_CLEANUP][RH] Network does not control of LastVeh but it is not registered with the Mission Controller. Registering now")
										SET_ENTITY_AS_MISSION_ENTITY(LastVeh, TRUE, TRUE)
									ENDIF
								ENDIF
								
								// If these fail then whatever made this vehicle should be the thing that cleans it up.
								IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(LastVeh)
								AND IS_ENTITY_A_MISSION_ENTITY(LastVeh)								
									PRINTLN("MC_SCRIPT_CLEANUP - LastVeh - Setting as No Longer Needed")
									SET_VEHICLE_AS_NO_LONGER_NEEDED(LastVeh)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					PRINTLN("MC_SCRIPT_CLEANUP - LastVeh = NULL")
				ENDIF
				CLEAR_LAST_DRIVEN_VEHICLE()
			ENDIF
		ENDIF
		
		FOR i = 0 TO ciMAX_GANG_CHASE_VEHICLES - 1
			CLEANUP_GANG_CHASE_UNIT(i)
		ENDFOR
		
		CLEANUP_MISSION_ENTITIES(bStrand)		

		IF NOT bStrand
		OR IS_MOCAP_CUTSCENE_A_PLACEHOLDER(FMMCCUT_ENDMOCAP #IF IS_DEBUG_BUILD , sMocapCutscene #ENDIF )
			FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[i])
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[i])
						DELETE_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[i])
					ELSE
						CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[i])
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_END_CUTSCENE_STARTED) AND NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_OVER)
			FOR i = 0 to (ciTOTAL_NUMBER_OF_MC_CUT_VEHCILES-1)
				IF DOES_ENTITY_EXIST(cutscene_vehicle[i])
					IF NETWORK_HAS_CONTROL_OF_ENTITY(cutscene_vehicle[i])
						IF CAN_REGISTER_MISSION_VEHICLES(1)
						AND ((NOT bStrand) OR (MC_serverBD.iEndCutscene = ciMISSION_CUTSCENE_PLACE_HOLDER))
							SET_ENTITY_AS_MISSION_ENTITY(cutscene_vehicle[i], FALSE, TRUE)
							DELETE_VEHICLE(cutscene_vehicle[i])
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
		FOR i = 0 TO (FMMC_MAX_NUM_DYNOPROPS-1)
			
			REMOVE_DYNOPROP_BLIP(i)
			
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptDynopropPTFX[i])
				STOP_PARTICLE_FX_LOOPED(ptDynopropPTFX[i])
			ENDIF
			
			IF NETWORK_DOES_NETWORK_ID_EXIST(GET_DYNOPROP_NET_ID(i))
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GET_DYNOPROP_NET_ID(i))
					DELETE_NET_ID(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_DYNOPROP_NET_ID_INDEX(i)])
				ELSE
					CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_DYNOPROP_NET_ID_INDEX(i)])
				ENDIF
			ENDIF
		ENDFOR
		
		REPEAT COUNT_OF(niMinigameObjects) i
			IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[i])
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niMinigameObjects[i])
					DELETE_NET_ID(niMinigameObjects[i])
				ELSE
					CLEANUP_NET_ID(niMinigameObjects[i])
				ENDIF
			ENDIF
		ENDREPEAT
	
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		AND NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_END_CUTSCENE_STARTED) AND NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_OVER)
			IF bIsLocalPlayerHost
				CLEAR_AREA_LEAVE_VEHICLE_HEALTH(GET_ENTITY_COORDS(LocalPlayerPed,FALSE),5000,TRUE, DEFAULT, DEFAULT, TRUE)
			ENDIF
			CLEAR_AREA_OF_OBJECTS(GET_ENTITY_COORDS(LocalPlayerPed,FALSE),5000,CLEAROBJ_FLAG_FORCE) 
			PRINTLN("[RCC MISSION] CLEAR_AREA_OF_OBJECTS called with force flag ")
		ENDIF
		
		// Clear Continuity vars if appropriate
		IF NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
			CLEAR_ALL_ENTITY_CHECKPOINT_CONTINUITY_VARS()
		ENDIF
		
	ENDIF
	
	HEIST_ISLAND_BACKUP_HELI__TERMINATE()
	
	IF NETWORK_IS_SIGNED_ONLINE() 
		MC_CLEAR_PED_AND_TRAFFIC_DENSITIES(iPopulationHandle, bVehicleGenerator)
		iPopulationHandle = -1
		
		IF bIsLocalPlayerHost
			IF MC_serverBD.fPedDensity = 0.0 
			AND MC_serverBD.fVehicleDensity = 0.0
				SERVER_CLEAR_BLOCKING_ALL_SCENARIOS(MC_serverBD_3.scenBlockServer)
			ENDIF
			
			IF NETWORK_ENTITY_AREA_DOES_EXIST(MC_serverBD.iSpawnArea)
				NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
			ENDIF
			MC_serverBD.iSpawnArea = -1
		ENDIF
	ENDIF
	
	FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfZones-1)
		IF iPopMultiArea[i] != -1
			IF DOES_POP_MULTIPLIER_AREA_EXIST(iPopMultiArea[i])
				REMOVE_POP_MULTIPLIER_AREA(iPopMultiArea[i], TRUE)
				PRINTLN("[RCC MISSION] iPopMultiArea cleaned up: ",i," native area id: ",iPopMultiArea[i])
				iPopulationHandle = -1
			ENDIF
		ENDIF
		IF bipopScenarioArea[i] != NULL
			REMOVE_SCENARIO_BLOCKING_AREA(bipopScenarioArea[i])
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP bipopScenarioArea cleaned up: ",i," native area id: ",NATIVE_TO_INT(bipopScenarioArea[i]))
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iType = ciFMMC_ZONE_TYPE__CLEAR_ALL
		OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iType = ciFMMC_ZONE_TYPE__CLEAR_VEHICLES
		OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iType = ciFMMC_ZONE_TYPE__ACTIVATE_ROADS
			SET_VEHICLE_GENERATORS_AND_ROADS_INACTIVE_FOR_ZONE(i, FALSE, iBitsetVehicleGeneratorsActiveArea, iBitsetSetRoadsArea, TRUE, TRUE)
		ENDIF
		
		IF iGPSDisabledZones[i] != -1
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP clearing GPS disabled zone at index ", iGPSDisabledZones[i])
			CLEAR_GPS_DISABLED_ZONE_AT_INDEX(iGPSDisabledZones[i])
			iGPSDisabledZones[i] = -1
		ENDIF
		IF iDispatchBlockZone[i] != -1
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP removing dispatch block zone at index: ", iDispatchBlockZone[i])
			REMOVE_DISPATCH_SPAWN_BLOCKING_AREA(iDispatchBlockZone[i])
		ENDIF
		IF iSpawnOcclusionIndex[i] != -1
			CLEAR_MISSION_SPAWN_OCCLUSION_AREA(iSpawnOcclusionIndex[i])
		ENDIF
		IF iFMMCAirDefenceZoneArea[i] != -1
			REMOVE_AIR_DEFENCE_SPHERE(iFMMCAirDefenceZoneArea[i])
		ENDIF
		IF iNavBlockerZone[i] != -1
			IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavBlockerZone[i])
				REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlockerZone[i])
			ENDIF
		ENDIF
	ENDFOR
	
	REMOVE_ALL_COVER_BLOCKING_AREAS()
	
	FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
			AND IS_MODEL_VALID(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
				PRINTLN("[RCC MISSION] suppressing vehicle model FALSE: ",ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn))
				SET_VEHICLE_MODEL_IS_SUPPRESSED(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn,FALSE)				
				VALIDATE_NUMBER_MODELS_SUPRESSED(-1)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = Technical
					PRINTLN("[RCC MISSION] suppressing vehicle model FALSE: ",ENUM_TO_INT(REBEL))
					SET_VEHICLE_MODEL_IS_SUPPRESSED(Rebel,FALSE)
					VALIDATE_NUMBER_MODELS_SUPRESSED(-1)
				ENDIF
				CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetTwo, ciFMMC_VEHICLE2_TriggerAudioOnVehicle) 
			ENDIF
		ENDIF
	ENDFOR
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		IF g_FMMC_STRUCT.mnVehicleModel[i] != DUMMY_MODEL_FOR_SCRIPT
		AND IS_MODEL_VALID(g_FMMC_STRUCT.mnVehicleModel[i])
			PRINTLN("[RCC MISSION] suppressing vehicle model FALSE: ",ENUM_TO_INT(g_FMMC_STRUCT.mnVehicleModel[i]))
			SET_VEHICLE_MODEL_IS_SUPPRESSED(g_FMMC_STRUCT.mnVehicleModel[i], FALSE)
			VALIDATE_NUMBER_MODELS_SUPRESSED(-1)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_KeepRandomNextObjectiveSeed)
			g_TransitionSessionNonResetVars.iRandomNextObjectiveSeed[i] = MC_serverBD_1.iRandomNextObjectiveSeed[i]
		ENDIF
	ENDFOR
	
	FOR i = 0 TO (FMMC_MAX_NUM_STUNTJUMPS - 1)
		IF IS_BIT_SET(iStuntJumpBitSet, i)
			DELETE_STUNT_JUMP(sjsID[i])
			CLEAR_BIT(iStuntJumpBitSet, i)
		ENDIF
	ENDFOR
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF NETWORK_IS_SIGNED_ONLINE() 
		
			NET_NL()NET_PRINT("[RCC MISSION] [BCCHEAT] MC_SCRIPT_CLEANUP  ")NET_PRINT(" g_FMMC_STRUCT.iMissionType = ")NET_PRINT_INT(g_FMMC_STRUCT.iMissionType)
			NET_PRINT("[RCC MISSION]  g_FMMC_STRUCT.iMissionSubType = ")NET_PRINT_INT(g_FMMC_STRUCT.iMissionSubType)

			NET_NL()NET_PRINT("[RCC MISSION] [BCCHEAT] MPPLY_MC_CHEAT_END Being Incremented in MC_SCRIPT_CLEANUP. ")
			INT IncrementBy = INCREMENT_ENDING_ACTIVITY_CHEAT_STAT_BY()
			INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_MC_OVER)
			INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_END, IncrementBy)
			IF IncrementBy > 0
				INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_ACTIVITY_ENDED, 1)
			ENDIF
		ENDIF
	ENDIF
	
	REMOVE_CASINO_SCENARIO_BLOCKING_AREA()
	
	g_bFM_ON_TEAM_MISSION = FALSE
	g_i_ActualLeaderboardPlayerSelected = 0
	SET_BIG_MESSAGE_ENABLED_WHEN_SPECTATING(FALSE)
	
	SET_PLAYER_LOCKON(LocalPlayer, TRUE)
	
	SET_EVERYONE_IGNORE_PLAYER(LocalPlayer,FALSE)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		CLEAR_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(LocalPlayer)].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_ON_SECOND_PART_OF_STRAND)
		//Force the player to respawn
		IF g_sFMMCEOM.iVoteStatus =  ciFMMC_EOM_VOTE_STATUS_ROUNDS_MISSION
		OR IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
			IF !bLocalPlayerPedOK	
			OR !bLocalPlayerOK
			OR IS_PLAYER_RESPAWNING(LocalPlayer)
				IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
				AND LocalPlayerCurrentInterior = NULL
					SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AUTOMATIC)
					PRINTLN("[TS] [MSROUND] - RESET_FMMC_MISSION_VARIABLES - SPAWN_LOCATION_AUTOMATIC - SET_PLAYER_NEXT_RESPAWN_LOCATION - FORCE_RESPAWN(TRUE)")
				ELSE
	  				SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AT_CURRENT_POSITION)
					PRINTLN("[TS] [MSROUND] - RESET_FMMC_MISSION_VARIABLES - SPAWN_LOCATION_AT_CURRENT_POSITION - SET_PLAYER_NEXT_RESPAWN_LOCATION - FORCE_RESPAWN(TRUE)")
				ENDIF
				FORCE_RESPAWN(TRUE)
			#IF IS_DEBUG_BUILD
			ELSE 
				PRINTLN("[TS] [MSROUND] - RESET_FMMC_MISSION_VARIABLES -  IS_PED_INJURED(LocalPlayerPed) = FALSE")
				#ENDIF
			ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		IF IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
		AND g_sFMMCEOM.iVoteStatus != ciFMMC_EOM_VOTE_STATUS_ROUNDS_MISSION
		AND g_sFMMCEOM.iVoteStatus != ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART
			PRINTLN("[TS] [MSROUND] - RESET_FMMC_MISSION_VARIABLES - SPAWN_LOCATION_AT_CURRENT_POSITION - SET_PLAYER_NEXT_RESPAWN_LOCATION - FORCE_RESPAWN(TRUE)")
		ENDIF
	#ENDIF
	ENDIF	
	
	BOOL bJoinSpectator = DID_I_JOIN_MISSION_AS_SPECTATOR()//url:bugstar:4744458
	
	RESET_FMMC_MISSION_VARIABLES(TRUE,FALSE,iMissionResult)	
	
	//If the player is dead and on a playlist then force them to respawn
	IF NETWORK_IS_GAME_IN_PROGRESS()
		
		IF NOT bStrand
			PRINTLN("[TS] [MSRAND] - NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)")
			NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
		ENDIF
		
		IF IS_PLAYER_ON_A_PLAYLIST_INT(NATIVE_TO_INT(LocalPlayer))
			IF NOT bLocalPlayerOK	
			OR IS_PLAYER_RESPAWNING(LocalPlayer)
		  		SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AT_CURRENT_POSITION)
				FORCE_RESPAWN(TRUE)
			ENDIF
		ENDIF	
		//Force the player to respawn
		IF g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART
		OR g_sFMMCEOM.iVoteStatus =  ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART_SWAP
			IF !bLocalPlayerPedOK	
			OR !bLocalPlayerOK
			OR IS_PLAYER_RESPAWNING(LocalPlayer)
		  		SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AT_CURRENT_POSITION)
				FORCE_RESPAWN(TRUE)
			ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[TS] [MSROUND] - RESET_FMMC_MISSION_VARIABLES - SPAWN_LOCATION_AT_CURRENT_POSITION - SET_PLAYER_NEXT_RESPAWN_LOCATION - FORCE_RESPAWN(TRUE)")
	#ENDIF
	ENDIF	
	
	CLEANUP_POLICE()
	
	SET_FAKE_WANTED_LEVEL(0)
	
	SET_BLOCK_WANTED_FLASH(FALSE)
	FORCE_OFF_WANTED_STAR_FLASH(FALSE)
	SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_ForceBlipSecurityPedsIfPlayerIsWanted, FALSE)
	
	ALLOW_EVASION_HUD_IF_DISABLING_HIDDEN_EVASION_THIS_FRAME(LocalPlayer, -1.0)	//Reset Cop Report Delay
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciTRIGGER_ALARMS_ON_WANTED)
		IF IS_BIT_SET(iLocalBoolCheck4, LBOOL4_TRIGGER_ALARMS)
			CLEAR_BIT(iLocalBoolCheck4, LBOOL4_TRIGGER_ALARMS)
			STOP_ALARM("PRISON_ALARMS", TRUE)
			SET_AMBIENT_ZONE_STATE("AZ_COUNTRYSIDE_PRISON_01_ANNOUNCER_GENERAL", TRUE, TRUE)
			SET_AMBIENT_ZONE_STATE("AZ_COUNTRYSIDE_PRISON_01_ANNOUNCER_ALARM", FALSE, TRUE)
		ENDIF
		
	ENDIF
	
	IF IS_ALARM_PLAYING("BIG_SCORE_HEIST_VAULT_ALARMS")
		STOP_ALARM("BIG_SCORE_HEIST_VAULT_ALARMS", TRUE)
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP | Cleaning up BIG_SCORE_HEIST_VAULT_ALARMS")
	ENDIF
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
	AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
	AND NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
		SET_TIME_OF_DAY(TIME_OFF, TRUE)
	ENDIF
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		CLEAR_OVERRIDE_WEATHER()
	ENDIF
		
	RESET_GAME_STATE_ON_DEATH() 

	// Player	
	IF NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
		PRINTLN("[RCC MISSION] Cleaning up team and friendly fire")
		
		FOR i = 0 TO (FMMC_MAX_TEAMS-1)
			IF NETWORK_IS_GAME_IN_PROGRESS()
	        	SET_PED_CAN_BE_TARGETTED_BY_TEAM(LocalPlayerPed, i, TRUE)
			ENDIF
			g_sMission_TeamName[i] = NULL_STRING()
	    ENDFOR
		
		// Returns FALSE by the cleanup stage
		// Cleanup team targetting
		PRINTLN("[RCC MISSION] NETWORK_SET_FRIENDLY_FIRE_OPTION(TRUE)")
		SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()	
		
		IF NOT bIsSCTV
			PRINTLN("[RCC MISSION] SET_PLAYER_TEAM(LocalPlayer, -1)")
			SET_PLAYER_TEAM(LocalPlayer, -1)
		ENDIF
		
		REFRESH_ALL_OVERHEAD_DISPLAY()
		g_bDoMCTeamCleanup = FALSE
	ELSE
		g_bDoMCTeamCleanup = TRUE
		PRINTLN("[RCC MISSION] Not Cleaning up team and friendly fire due to strand")
	ENDIF
	
	SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_WillFlyThroughWindscreen, TRUE) 
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
			NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
	ENDIF
	
	
	RESET_SAFE_CRACK( SafeCrackData, FALSE )
	CLEAR_SAFE_CRACK_SEQUENCES(SafeCrackData)
	
	// Emergency services
	SET_MAX_WANTED_LEVEL(5)
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	RESET_WANTED_LEVEL_DIFFICULTY(LocalPlayer)
	
	IF NOT (IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING())
		PRINTLN("[RCC MISSION] Turning dispatch services back on.")
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)			
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)	
		SET_DISPATCH_SERVICES(TRUE, FALSE, TRUE)
		g_bDoMCDispatchCleanup = FALSE
	ELSE
		PRINTLN("[RCC MISSION] Blocking dispatch for casino airlock")
		SET_DISPATCH_SERVICES(FALSE, TRUE, TRUE)
		g_bDoMCDispatchCleanup = TRUE
	ENDIF
	
	// HUD
	IF NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
		SET_WIDESCREEN_BORDERS(FALSE, -1)
		DISPLAY_RADAR(TRUE)
		DISPLAY_HUD(TRUE)
		UNLOCK_MINIMAP_ANGLE()
	ENDIF
	
	SET_PED_COMBAT_ATTRIBUTES(LocalPlayerPed, CA_IGNORED_BY_OTHER_PEDS_WHEN_WANTED, FALSE)
	
	IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_SET_TIME_TO_LOSE_COPS_CLEAR_MODE)
		RESET_WANTED_LEVEL_HIDDEN_ESCAPE_TIME(LocalPlayer)
		CLEAR_BIT(iLocalBoolCheck33, LBOOL33_SET_TIME_TO_LOSE_COPS_CLEAR_MODE)
	ENDIF
	
	SET_KILLSTRIP_AUTO_RESPAWN(FALSE)
	
	CLEAR_CELEBRATION_MISC_POST_FX()
	CLEAR_CELEBRATION_FAIL_POST_FX()
	IF NOT DOES_SCRIPT_EXIST("Celebrations")
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Celebrations")) = 0
			CLEAR_CELEBRATION_PASS_POST_FX()
			SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(FALSE)
		ENDIF
	ENDIF
	
	// (414073)
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
		Enable_MP_Comms()
	ENDIF
	ENABLE_SELECTOR()
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()	
		CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT,TRUE)
		CLEAR_ADDITIONAL_TEXT(MINIGAME_TEXT_SLOT,TRUE)
		CLEAR_ADDITIONAL_TEXT(DLC_MISSION_DIALOGUE_TEXT_SLOT,TRUE)
		CLEAR_ADDITIONAL_TEXT(DLC_MISSION_DIALOGUE_TEXT_SLOT2,TRUE)
	ENDIF
		
	DISABLE_SCRIPT_HUD(HUDPART_RANKBAR, FALSE)
	ENABLE_ALL_MP_HUD()
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
	AND NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene) // If we have trigged a stand alone celebration, we don't want to cleanup here, the stand alone celebration script will handle that.
		PRINTLN("[NETCELEBRATION] - [RCC MISSION] - CLEANUP_CELEBRATION_SCREEN call 0.")
		CLEANUP_CELEBRATION_SCREEN(sCelebrationData, TRUE, "SUMMARY", bJoinSpectator)
		RESET_WEATHER_AND_TIME_AFTER_CELEBRATION_WINNER() //In case the script terminated before the winner screen cleaned up.
		SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
		SET_CHAT_ENABLED_FOR_CELEBRATION_SCREEN(FALSE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bFinalCleanUp
			NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION]     MC_SCRIPT_CLEANUP  bFinalCleanUp = TRUE   ", tl31ScriptName) NET_NL()
		ELSE
			NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION]     MC_SCRIPT_CLEANUP  bFinalCleanUp = FALSE   ", tl31ScriptName) NET_NL()
		ENDIF

	#ENDIF	
	
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_HIDE_RADAR)
		DISPLAY_RADAR(TRUE)
		CLEAR_BIT(iLocalBoolCheck6, LBOOL6_HIDE_RADAR)
	ENDIF
		
	IF interiorWarp[g_FMMC_STRUCT.iBuildingToWarpTo] != NULL
		UNPIN_INTERIOR(interiorWarp[g_FMMC_STRUCT.iBuildingToWarpTo])
		interiorWarp[g_FMMC_STRUCT.iBuildingToWarpTo] = NULL

		IF DOES_ENTITY_EXIST(objDoorBlocker)
			DELETE_OBJECT(objDoorBlocker)
		ENDIF
	ENDIF
	
	CLEAR_GPS_FLAGS()
	
	//Clear the Quick GPS
	CLEAN_QUICK_GPS()
	//Force Clean all waypoints
	SET_WAYPOINT_OFF()
	//Clear Hint Cam
	KILL_CHASE_HINT_CAM(sHintCam)
	
	IF bIsDisplacedInteriorSet
		CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA()
		bIsDisplacedInteriorSet = FALSE
	ENDIF

	ENABLE_SPECTATOR_FADES()
	
	IF bIsSCTV
		CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCTV_IN_MOCAP)
		PRINTLN("[RCC MISSION] GLOBAL_SPEC_BS_SCTV_IN_MOCAP CLEARED")
	ENDIF
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockSpawnFadeInForMissionFail = FALSE
	PRINTLN("[RCC MISSION] - [SAC] - [spawning] - setting bBlockSpawnFadeInForMissionFail = FALSE.")
	
	RESET_PHOTO_DATA()
	CLEANUP_PHOTO_TRACKED_POINT(iObjPhotoTrackedPoint)
	
	SET_PLAYER_BLOCKED_FROM_EXITING_VEHICLE_IN_MISSION_FLAG(FALSE)
	
	ENABLE_MULTIPLAYER_TRACKIFY_APP(FALSE)
	
	IF bFinalCleanUp
		g_sTransitionSessionData.bJustDoneMission = TRUE
		PRINTLN("[RCC MISSION] [dsw] Mission controller cleanup set bJustDoneMission")
	ENDIF
	
	SET_PED_DROPS_WEAPONS_WHEN_DEAD(LocalPlayerPed, TRUE)
	
	//reset wasted text to original after sudden death penned in
	CHANGE_WASTED_SHARD_TEXT(FALSE)
	SET_BIG_MESSAGE_SUPPRESS_WASTED(FALSE)
		
	IF IS_BIT_SET(iLocalBoolCheck19, LBOOL19_RACE_SPLIT_TIMES_STARTED)
		SCRIPT_RACE_SHUTDOWN()
	ENDIF
	
	g_bFinishedExitingOfSMPLinteriorOnHeist = FALSE
	PRINTLN("[RCC MISSION] Cleaning up g_bFinishedExitingOfSMPLinteriorOnHeist")
	
	SET_SPEED_BOOST_EFFECT_DISABLED(FALSE)	
		
	INT iObj
	FOR iObj = 0 TO FMMC_MAX_NUM_OBJECTS - 1
		SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectMinigame
			CASE ciFMMC_OBJECT_MINIGAME_TYPE__BEAM_HACK
			CASE ciFMMC_OBJECT_MINIGAME_TYPE__BEAMHACK_VEHICLE
				CLEAN_UP_BEAM_HACK_MINIGAME(sBeamHack[GET_HACKING_STRUCT_INDEX_TO_USE(iObj, ciENTITY_TYPE_OBJECT)], TRUE)
			BREAK
			CASE ciFMMC_OBJECT_MINIGAME_TYPE__HOTWIRE
				CLEAN_UP_HOTWIRE_MINIGAME(sHotwire[GET_HACKING_STRUCT_INDEX_TO_USE(iObj, ciENTITY_TYPE_OBJECT)], TRUE)
			BREAK
			CASE ciFMMC_OBJECT_MINIGAME_TYPE__ORDER_UNLOCK
				CLEAN_UP_ORDER_UNLOCK(sOrderUnlockGameplay, sOrderUnlock[GET_HACKING_STRUCT_INDEX_TO_USE(iObj, ciENTITY_TYPE_OBJECT)], TRUE, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
			BREAK
			CASE ciFMMC_OBJECT_MINIGAME_TYPE__FINGERPRINT_CLONE
				CLEAN_UP_FINGERPRINT_CLONE(sFingerprintCloneGameplay, sFingerprintClone[GET_HACKING_STRUCT_INDEX_TO_USE(iObj, ciENTITY_TYPE_OBJECT)], TRUE, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
			BREAK
		ENDSWITCH
	ENDFOR
	
	IF bLocalPlayerPedOK
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableVehicleCombat, FALSE)
		SET_PED_CAN_SWITCH_WEAPON(LocalPlayerPed, TRUE)
	ENDIF
	
	SET_MISSION_CONTROLLER_RACE_TYPE(-1)
	
	SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableShallowWaterBikeJumpOutThisFrame, FALSE)
	
	IF bShouldProcessCokeGrabHud
		REMOVE_PLAYER_BAG()
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDISABLE_AMBIENT_VFX)
		DISABLE_REGION_VFX(FALSE)
	ENDIF
	
	IF IS_NET_PLAYER_OK(LocalPlayer)
		SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(LocalPlayer, 1.0)
	ENDIF
	
	IF IS_THIS_TRANSITION_SESSION_A_PLAYLIST()
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciDISGUISE_TEAM_SELECTION)
		PRINTLN("[KH][RCC MISSION] MC_SCRIPT_CLEANUP - Setting player back into freemode outfit as we're on a playlist and ciDISGUISE_TEAM_SELECTION is set")
		SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
	ENDIF

	g_bLastMissionWVM = FALSE
		
	SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
	
	MPGlobalsAmbience.bIgnoreInvalidToSpectateBail = FALSE
	
	SET_LOCAL_PLAYER_ARROW_TO_CURRENT_HUD_COLOUR(FALSE)
	
	g_iFMMCScriptedCutscenePlaying = -1
	
	g_bReBlockSeatSwapping = FALSE
	
	g_EnableKersHelp = FALSE
	
	g_piFMMCLobbyHostPlayer = INVALID_PLAYER_INDEX()
	
	IF DOES_RELATIONSHIP_GROUP_EXIST(relPlayerAbilityPed)
		REMOVE_RELATIONSHIP_GROUP(relPlayerAbilityPed)
	ENDIF
	
	RESET_DISPATCH_SPAWN_LOCATION()
	
	IF bLocalPlayerOK
		SET_PLAYER_PARACHUTE_SMOKE_TRAIL_COLOR(LocalPlayer, iCachedParachuteSmokeRValue, iCachedParachuteSmokeGValue, iCachedParachuteSmokeBValue)
	ENDIF
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer
		IF IS_CREATOR_AIRCRAFT_ENTRY_BLOCKED(LocalPlayer)
			BLOCK_CREATOR_AIRCRAFT_INTERIOR_ENTRY(FALSE)
		ENDIF
	ENDIF
	
	IF IS_SIMPLE_INTERIOR_EXIT_BLOCKED()
		BLOCK_SIMPLE_INTERIOR_EXIT(FALSE)
	ENDIF
	
	SET_ARMORY_AIRCRAFT_AUTOPILOT_ACTIVE(FALSE)
	
	IF DOES_ENTITY_EXIST(oiHangarWayfinding)
		DELETE_OBJECT(oiHangarWayfinding)
	ENDIF
	
	SET_VEHICLE_DETONATION_MODE(FALSE)
	SET_VEHICLE_SHUNT_ON_STICK(FALSE)

	PLAYSTATS_STOP_TRACKING_STUNTS()
	
	g_bBlockHintCamUsageInMPMission = FALSE
	
	TOGGLE_HIDE_CASINO_FRONT_DOOR_IPL(FALSE)
	
	CLEANUP_BURNING_VEHICLES()
	
	PROCESS_RESET_AIRLOCK_HUD_VARS()
	g_iAirlockHudDrawnFrame = GET_FRAME_COUNT() //Stop it running again this frame
	g_TransitionSessionNonResetVars.iMissionControllerTerminateTime = (GET_GAME_TIMER() / 1000)
	
	CLEAR_LONG_BITSET(MPGlobalsAmbience.iPlayerAbilitiesBitset)
	
	BLOCK_INTERNET_APP(FALSE)
	
	SET_PED_CAN_LOSE_PROPS_ON_DAMAGE(LocalPlayerPed, TRUE)
	
	SET_PLAYER_IS_IN_ANIMAL_FORM(FALSE)
	
	#IF IS_DEBUG_BUILD
	PASS_OVER_PREVIOUS_DEBUG_WINDOW_INFORMATION(ciCONTENT_OVERVIEW_DEBUG_WINDOW_SCRIPT__MC, sContentOverviewDebug.eDebugWindow, sContentOverviewDebug.eDebugWindowPrevious)	
	#ENDIF
	
	TERMINATE_THIS_MULTIPLAYER_THREAD(MC_serverBD.TerminationTimer)

ENDPROC
