// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Wanted --------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description:
// ##### Triggering, processing and removing wanted stars from the local player
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
USING "FM_Mission_Controller_Include_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Helpers
// ##### Description: Helper functions used by various wanted sub-systems
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_PREVIOUS_RULE_BLOCK_WANTED_CHANGE(INT iTeam, INT iRule)
	
	IF MC_serverBD_4.iPreviousHighestPriority[iTeam] >= FMMC_MAX_RULES
	OR MC_serverBD_4.iPreviousHighestPriority[iTeam] < 0
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_WANTED_ON_FAIL)
		IF DID_TEAM_PASS_OBJECTIVE(iTeam, MC_serverBD_4.iPreviousHighestPriority[iTeam])
			RETURN TRUE
		ENDIF
		PRINTLN("[WANTED] SHOULD_PREVIOUS_RULE_BLOCK_WANTED_CHANGE - Team ", iTeam, " failed previous objective (", MC_serverBD_4.iPreviousHighestPriority[iTeam], ") - allow wanted") 
	ENDIF
	                
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_AGGRO_BLOCK_WANTED_WANTED_CHANGE(INT iTeam, INT iRule)

	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_CHECK_TEAMS_FOR_AGGRO_WANTED)
	AND HAS_ANY_FRIENDLY_TEAM_TRIGGERED_AGGRO(iTeam, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAggroIndexBS_CheckTeamsForAggroWanted[iRule])
		PRINTLN("[WANTED] SHOULD_AGGRO_BLOCK_WANTED_WANTED_CHANGE - A friendly team to team ", iTeam, " has aggro - allow wanted") 
		RETURN FALSE
	ENDIF
	
	IF HAS_TEAM_TRIGGERED_AGGRO(iTeam, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAggroIndexBS_CheckTeamsForAggroWanted[iRule])
		PRINTLN("[WANTED] SHOULD_AGGRO_BLOCK_WANTED_WANTED_CHANGE - Team ", iTeam, " has aggro - allow wanted") 
		RETURN FALSE
	ENDIF
		
	RETURN TRUE

ENDFUNC

FUNC BOOL IS_PLAYER_IN_WANTED_BLOCKING_ZONE()
	
	IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE)
	OR IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_AND_CLEAR_WANTED_LEVEL_ZONE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_WANTED_BLOCKED_BY_OUTFIT(INT iTeam, INT iRule, INT iBounds)
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleBounds[iRule][iBounds].iIgnoreWantedOutfit = -1
		RETURN FALSE
	ENDIF
	
	IF MC_playerBD[iLocalPart].iOutfit != g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleBounds[iRule][iBounds].iIgnoreWantedOutfit
		RETURN FALSE
	ENDIF
	
	PRINTLN("[WANTED] IS_WANTED_BLOCKED_BY_OUTFIT - Player's current outfit blocks wanted") 
	
	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_CURRENT_VEHICLE_BLOCK_WANTED(INT iTeam, INT iRule, INT iBounds)
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleBounds[iRule][iBounds].MNIgnoreWantedVeh = DUMMY_MODEL_FOR_SCRIPT
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		RETURN FALSE
	ENDIF
	
	VEHICLE_INDEX viTempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
	
	IF NOT IS_VEHICLE_DRIVEABLE(viTempVeh)
		RETURN FALSE
	ENDIF
	
	IF GET_ENTITY_MODEL(viTempVeh) != g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleBounds[iRule][iBounds].MNIgnoreWantedVeh
		RETURN FALSE
	ENDIF
	
	PRINTLN("[WANTED] SHOULD_CURRENT_VEHICLE_BLOCK_WANTED - Player's current vehicle blocks wanted") 
	
	RETURN TRUE

ENDFUNC

FUNC INT GET_EASY_WANTED_TIME(INT iTeam, INT iRule)

	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTimeToLoseCops[iRule] >= 0
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_TIME_TO_LOSE_COPS_CLEAR_WANTED_MODE)
		RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTimeToLoseCops[iRule]
	ENDIF
	
	IF MC_serverBD.iDifficulty = DIFF_EASY
		RETURN ciFMMC_EASY_WANTED_TIME_EASY 
	ELIF MC_serverBD.iDifficulty = DIFF_HARD
		RETURN ciFMMC_EASY_WANTED_TIME_HARD
	ENDIF
	
	RETURN ciFMMC_EASY_WANTED_TIME_DEFAULT

ENDFUNC

FUNC BOOL SHOULD_WANTED_DELAY_BE_SKIPPED(INT iTeam, INT iRule)
	
	IF IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__SKIP_WANTED_DELAY)
		PRINTLN("[WANTED] SHOULD_WANTED_DELAY_BE_SKIPPED - Skipping wanted delay due to zone.")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_SKIP_WANTED_DELAY_IF_WANTED)
	AND GET_PLAYER_WANTED_LEVEL(LocalPlayer) > 0
		PRINTLN("[WANTED] SHOULD_WANTED_DELAY_BE_SKIPPED - Skipping wanted delay due to being wanted already.")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC
		
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Bounds Wanted
// ##### Description: Processing of Wanted triggered by bounds
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_WANTED_BOUNDS_BITSET(INT iBounds, INT iTeam)
	
	IF iBounds = ciRULE_BOUNDS_INDEX__SECONDARY
		RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedLevelBounds2
	ENDIF
	
	RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedLevelBounds
	
ENDFUNC

PROC PROCESS_WANTED_BOUNDS(INT iTeam, INT iRule)
	
	IF !bLocalPlayerOK
		EXIT
	ENDIF
	
	INT iBounds
	FOR iBounds = 0 TO ciMAX_RULE_BOUNDS_PER_RULE - 1
		
		IF NOT IS_LOCAL_PLAYER_INSIDE_BOUNDS(iBounds)
			CLEAR_BIT(iLocalBoolCheck3, LBOOL3_SAFE_FROM_WANTED_BOUNDS1 + iBounds)
			CLEAR_BIT(iLocalBoolCheck3, LBOOL3_WANTED_BOUNDS1_TOGGLE + iBounds)
		ENDIF
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleBounds[iRule][iBounds].iWantedToGive = -1
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_WANTED_BOUNDS1_TOGGLE + iBounds)
			
			IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_IN_VALID_VEHICLE_FOR_WANTED_BOUNDS1 + iBounds)
			AND NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				CLEAR_BIT(iLocalBoolCheck3, LBOOL3_WANTED_BOUNDS1_TOGGLE + iBounds)
			ENDIF
			
			RELOOP
			
		ENDIF
		
		SET_BIT(iLocalBoolCheck3, LBOOL3_WANTED_BOUNDS1_TOGGLE + iBounds)
			
		IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_IN_VALID_VEHICLE_FOR_WANTED_BOUNDS1 + iBounds)
			CLEAR_BIT(iLocalBoolCheck11, LBOOL11_IN_VALID_VEHICLE_FOR_WANTED_BOUNDS1 + iBounds)
			CLEAR_BIT(iLocalBoolCheck3, LBOOL3_SAFE_FROM_WANTED_BOUNDS1 + iBounds)
		ENDIF
		
		IF IS_WANTED_BLOCKED_BY_OUTFIT(iTeam, iRule, iBounds)
			SET_BIT(iLocalBoolCheck3, LBOOL3_SAFE_FROM_WANTED_BOUNDS1 + iBounds)
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck3, LBOOL3_SAFE_FROM_WANTED_BOUNDS1 + iBounds)
		AND SHOULD_CURRENT_VEHICLE_BLOCK_WANTED(iTeam, iRule, iBounds)
			SET_BIT(iLocalBoolCheck11, LBOOL11_IN_VALID_VEHICLE_FOR_WANTED_BOUNDS1 + iBounds)
			SET_BIT(iLocalBoolCheck3, LBOOL3_SAFE_FROM_WANTED_BOUNDS1 + iBounds)
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_SAFE_FROM_WANTED_BOUNDS1 + iBounds)
			EXIT
		ENDIF
		
		IF IS_BIT_SET(GET_WANTED_BOUNDS_BITSET(iBounds, iTeam), iRule)
			SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(LocalPlayer)
			FORCE_OFF_WANTED_STAR_FLASH(TRUE)
			REPORT_POLICE_SPOTTED_PLAYER(LocalPlayer)
			SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_FORCE_NO_WANTED_FLASH)
		ENDIF
		
		INT iWantedToGive = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleBounds[iRule][iBounds].iWantedToGive
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_INCREASE_WANTED_LEVEL_ON_THIS_RULE)
			IF MC_serverBD.iDifficulty = DIFF_EASY
				iWantedToGive--
			ELIF MC_serverBD.iDifficulty = DIFF_HARD
				iWantedToGive++
			ENDIF
		ENDIF
		
		IF GET_PLAYER_WANTED_LEVEL(LocalPlayer) < iWantedToGive
			SET_PLAYER_WANTED_LEVEL(LocalPlayer, iWantedToGive)
			SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
			PRINTLN("[WANTED] PROCESS_WANTED_BOUNDS - Giving player ", iWantedToGive, " wanted stars due to bounds: ", iBounds) 
		ENDIF
	
	ENDFOR
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Decoy
// ##### Description: Processing for forcing wanted levels when using cop decoys
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_PLAYER_COP_DECOY()
	
	IF IS_BIT_SET(iCopDecoyBS, ci_COP_DECOY_FORCE_5_STAR_ENDED)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iCopDecoyBS, ci_COP_DECOY_ENTERED_AREA)
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(tdCopDecoy5StarForceTimer)
		IF GET_PLAYER_WANTED_LEVEL(LocalPlayer) != FMMC_MAX_WANTED_STARS
			SET_PLAYER_WANTED_LEVEL(LocalPlayer, FMMC_MAX_WANTED_STARS)
			SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
		ENDIF
		SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(LocalPlayer)
		REINIT_NET_TIMER(tdCopDecoy5StarForceTimer)
		PRINTLN("[WANTED][CopDecoy][PlayerAbilities][DistractionAbility] PROCESS_PLAYER_COP_DECOY - Player entered decoy area! Forcing wanted to 5* for 30 seconds.")
	ELIF NOT HAS_NET_TIMER_EXPIRED(tdCopDecoy5StarForceTimer, 30000)
		SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(LocalPlayer)
	ELSE
		PRINTLN("[WANTED][CopDecoy][PlayerAbilities][DistractionAbility] PROCESS_PLAYER_COP_DECOY - Force 5* wanted 30 second timer ended.")
		SET_BIT(iCopDecoyBS, ci_COP_DECOY_FORCE_5_STAR_ENDED)
	ENDIF
	
ENDPROC
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Treat as Wanted
// ##### Description: Processing to mark the local player as still being wanted while dead for lose wanted objectives.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_CLIENT_TREAT_AS_WANTED()
	
	IF IS_PLAYER_DEAD(LocalPlayer)
		
		IF GET_PLAYER_WANTED_LEVEL(LocalPlayer) > 0
			
			#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_TREAT_AS_WANTED)
				PRINTLN("[WANTED] PROCESS_CLIENT_TREAT_AS_WANTED - Local player is dead, setting bit")
			ENDIF
			#ENDIF
			
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_TREAT_AS_WANTED)
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(stWantedDeathCooldownTimer)
			RESET_NET_TIMER(stWantedDeathCooldownTimer)
		ENDIF
		
	ELIF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_TREAT_AS_WANTED)
	
		IF NOT HAS_NET_TIMER_STARTED(stWantedDeathCooldownTimer)
			REINIT_NET_TIMER(stWantedDeathCooldownTimer)
			PRINTLN("[WANTED] PROCESS_CLIENT_TREAT_AS_WANTED - No longer dead, starting timer")
		ELIF HAS_NET_TIMER_EXPIRED(stWantedDeathCooldownTimer, ciWantedDeathCooldownTimer_Length)
			PRINTLN("[WANTED] PROCESS_CLIENT_TREAT_AS_WANTED - Timer expired, clearing bit")
			CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_TREAT_AS_WANTED)
		ENDIF
		
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: "Real" Wanted
// ##### Description: Processing of "real" wanted levels (the stars shown represent the true code wanted level)
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_AGGRO_BLOCK_REAL_WANTED_WANTED_CHANGE(INT iTeam, INT iRule)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedOnAggroBitset, iRule)
		RETURN FALSE
	ENDIF
	
	RETURN SHOULD_AGGRO_BLOCK_WANTED_WANTED_CHANGE(iTeam, iRule)
	
ENDFUNC

FUNC BOOL SHOULD_WANTED_LEVEL_BE_CHANGED_NOW(INT iTeam, INT iRule)
	
	IF SHOULD_PREVIOUS_RULE_BLOCK_WANTED_CHANGE(iTeam, iRule)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_AGGRO_BLOCK_REAL_WANTED_WANTED_CHANGE(iTeam, iRule)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedAtMidBitset, iRule)
		IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iRule)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_CLEAR_WANTED_FOR_CONE_REPONSE_BLOCKING(INT iTeam, INT iRule)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_CLEAR_WANTED_FOR_CONE_REPONSE_BLOCKING)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE)
		EXIT
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(LocalPlayer) > 0
		PRINTLN("[WANTED] PROCESS_CLEAR_WANTED_FOR_CONE_REPONSE_BLOCKING - Setting wanted level to 0")
		SET_PLAYER_WANTED_LEVEL(LocalPlayer, 0)
		SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
	ENDIF
	
ENDPROC

PROC PROCESS_DISABLE_WANTED_HELI_RESPONSE(INT iTeam, INT iRule)

	BOOL bBlockHelis = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_DISABLE_HELI_RESPONSE)
	
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, !bBlockHelis)
	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_HELICOPTER, bBlockHelis)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_HELICOPTER, !bBlockHelis)
	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_SWAT_HELICOPTER, bBlockHelis)
	
ENDPROC

PROC DISABLE_LOCAL_PLAYER_WANTED_LEVEL(INT iRule)
	PRINTLN("[WANTED] DISABLE_LOCAL_PLAYER_WANTED_LEVEL - Called")
	iLocalWantedHighestPriority = iRule
	g_bMissionRemovedWanted = TRUE
	IF bLocalPlayerOK
		CLIENT_SET_POLICE(POLICE_OFF)
		SET_PLAYER_WANTED_LEVEL(LocalPlayer, 0)
		SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
	ENDIF
	g_bDeactivateRestrictedAreas = TRUE
	RESET_NET_TIMER(tdWantedDelayTimer)
	iNewWantedThisFrame = 0
ENDPROC

FUNC INT CLAMP_WANTED_LEVEL(INT iWanted)
	
	iWanted = CLAMP_INT(iWanted, 0, FMMC_MAX_WANTED_STARS)

	IF MC_serverBD.iPolice > POLICE_OFF
		IF iWanted > GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice)
			iWanted = GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice)
		ENDIF
	ENDIF
	
	RETURN iWanted
	
ENDFUNC

FUNC INT GET_NEW_WANTED_LEVEL(INT iTeam, INT iRule)
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedChange[iRule] <= FMMC_WANTED_RULE_SET_5	
	
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedChange[iRule] = FMMC_WANTED_RULE_SET_0
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedForce[iRule] = 0
			RETURN 0
		ENDIF
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedForce[iRule] > 0
			RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedForce[iRule] 
		ENDIF
		
		IF MC_playerBD[iPartToUse].iWanted >= GET_WANTED_CHANGE_FROM_MENU_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedChange[iRule])
			RETURN MC_playerBD[iPartToUse].iWanted
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_INCREASE_WANTED_LEVEL_ON_THIS_RULE)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_REDUCE_DIFFICULTY_ON_RETRY)
			RETURN GET_WANTED_CHANGE_FROM_MENU_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedChange[iRule])
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_REDUCE_DIFFICULTY_ON_RETRY)
		AND IS_THIS_A_QUICK_RESTART_JOB()	
			INT iNewWanted = GET_WANTED_CHANGE_FROM_MENU_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedChange[iRule])
			
			IF MC_serverBD.iDifficulty = DIFF_EASY
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_INCREASE_WANTED_LEVEL_ON_THIS_RULE)
					iNewWanted -= 2
				ELSE
					iNewWanted -= 1
				ENDIF
			ELIF MC_serverBD.iDifficulty = DIFF_HARD
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_INCREASE_WANTED_LEVEL_ON_THIS_RULE)
					iNewWanted -= 1
				ENDIF
			ELSE
				iNewWanted -= 1
			ENDIF
			
			IF iNewWanted <= 0
				RETURN 1
			ENDIF
			
			RETURN iNewWanted
			
		ENDIF
		
		IF MC_serverBD.iDifficulty = DIFF_EASY
			RETURN GET_WANTED_CHANGE_FROM_MENU_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedChange[iRule]) - 1
		ELIF MC_serverBD.iDifficulty = DIFF_HARD
			RETURN GET_WANTED_CHANGE_FROM_MENU_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedChange[iRule]) + 1
		ELSE
			RETURN GET_WANTED_CHANGE_FROM_MENU_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedChange[iRule])
		ENDIF
		
	ELSE 
	
		IF GET_WANTED_CHANGE_FROM_MENU_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedChange[iRule]) >= 0
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_INCREASE_WANTED_LEVEL_ON_THIS_RULE)
				IF MC_serverBD.iDifficulty = DIFF_EASY
					IF MC_playerBD[iPartToUse].iWanted >= GET_WANTED_CHANGE_FROM_MENU_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedChange[iRule]) -1
						RETURN MC_playerBD[iPartToUse].iWanted - 1
					ELSE
						RETURN MC_playerBD[iPartToUse].iWanted + GET_WANTED_CHANGE_FROM_MENU_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedChange[iRule]) - 1
					ENDIF
				ELIF MC_serverBD.iDifficulty = DIFF_HARD
					IF MC_playerBD[iPartToUse].iWanted >= GET_WANTED_CHANGE_FROM_MENU_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedChange[iRule]) +1
						RETURN MC_playerBD[iPartToUse].iWanted - 1
					ELSE
						RETURN MC_playerBD[iPartToUse].iWanted + GET_WANTED_CHANGE_FROM_MENU_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedChange[iRule]) + 1
					ENDIF
				ELSE
					IF MC_playerBD[iPartToUse].iWanted >= GET_WANTED_CHANGE_FROM_MENU_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedChange[iRule])
						RETURN MC_playerBD[iPartToUse].iWanted
					ELSE
						RETURN MC_playerBD[iPartToUse].iWanted + GET_WANTED_CHANGE_FROM_MENU_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedChange[iRule])
					ENDIF
				ENDIF
			ELSE
				IF MC_playerBD[iPartToUse].iWanted >= GET_WANTED_CHANGE_FROM_MENU_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedChange[iRule])
					RETURN MC_playerBD[iPartToUse].iWanted
				ELSE
					RETURN MC_playerBD[iPartToUse].iWanted + GET_WANTED_CHANGE_FROM_MENU_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedChange[iRule])
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_INCREASE_WANTED_LEVEL_ON_THIS_RULE)
				RETURN MC_playerBD[iPartToUse].iWanted + GET_WANTED_CHANGE_FROM_MENU_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedChange[iRule])
			ENDIF
		ENDIF

	ENDIF
	
	RETURN 0
	
ENDFUNC

FUNC BOOL HAS_REAL_WANTED_DELAY_EXPIRED(INT iTeam, INT iRule)

	IF SHOULD_WANTED_DELAY_BE_SKIPPED(iTeam, iRule)
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedDelay[iRule] = 0
		RETURN TRUE
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(tdWantedDelayTimer)
		PRINTLN("[WANTED] HAS_REAL_WANTED_DELAY_EXPIRED - Starting wanted delay")
		REINIT_NET_TIMER(tdWantedDelayTimer)
	ENDIF
				
	IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdWantedDelayTimer) <= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedDelay[iRule]
		SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_SHOW_WANTED_DELAY_HUD_REAL_WANTED_TIMER)
		RETURN FALSE
	ENDIF
	
	RESET_NET_TIMER(tdWantedDelayTimer)
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_REAL_WANTED(INT iTeam, INT iRule)
	
	IF bIsAnySpectator
		EXIT
	ENDIF
	
	IF iLocalWantedHighestPriority = iRule
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED)
		EXIT
	ENDIF
				
	PROCESS_CLEAR_WANTED_FOR_CONE_REPONSE_BLOCKING(iTeam, iRule)
	
	PROCESS_DISABLE_WANTED_HELI_RESPONSE(iTeam, iRule)
	
	IF NOT SHOULD_WANTED_LEVEL_BE_CHANGED_NOW(iTeam, iRule)
		IF HAS_NET_TIMER_STARTED(tdWantedDelayTimer)
			RESET_NET_TIMER(tdWantedDelayTimer)
		ENDIF
		
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSurpressWantedBitset, iRule)
		
		IF MC_playerBD[iPartToUse].iWanted = 0
			DISABLE_LOCAL_PLAYER_WANTED_LEVEL(iRule)
			EXIT
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(tdWantedDelayTimer)
			REINIT_NET_TIMER(tdWantedDelayTimer)
		ENDIF
		
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdWantedDelayTimer) > g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedDelay[iRule]
			DISABLE_LOCAL_PLAYER_WANTED_LEVEL(iRule)
		ELSE
			SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_SHOW_WANTED_DELAY_HUD_REAL_WANTED_TIMER)
		ENDIF
		
		EXIT
		
	ENDIF
		
	IF NOT HAS_REAL_WANTED_DELAY_EXPIRED(iTeam, iRule)
		EXIT
	ENDIF
	
	iLocalWantedHighestPriority = iRule
	
	g_bDeactivateRestrictedAreas = (MC_serverBD.iPolice = POLICE_OFF) //turning off cops setting military base off
				
	IF MC_serverBD.iPolice > POLICE_OFF
		IF GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice) > 0
			CLIENT_SET_POLICE(POLICE_ON)
			SET_MAX_WANTED_LEVEL(GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice))
		ENDIF
	ELSE
		CLIENT_SET_POLICE(MC_serverBD.iPolice)
	ENDIF
	
	INT iNewWanted = CLAMP_WANTED_LEVEL(GET_NEW_WANTED_LEVEL(iTeam, iRule))
	
	PRINTLN("[WANTED] PROCESS_REAL_WANTED - New Wanted Level: ", iNewWanted)
		
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedChange[iRule] != FMMC_WANTED_RULE_IGNORE
	OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedForce[iRule] != FMMC_WANTED_RULE_IGNORE
		
		IF iNewWanted = 0
			g_bMissionRemovedWanted = TRUE
		ENDIF
		
		iNewWantedThisFrame = iNewWanted
		
		IF !g_bMissionEnding 
		AND iNewWanted > 0
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciWANTED_CLEARED_ON_DEATH)
			AND !bLocalPlayerPedOK
				IF iWantedBeforeDeath != iNewWanted
					iWantedBeforeDeath = iNewWanted
				ENDIF
				PRINTLN("[WANTED] PROCESS_REAL_WANTED - Setting new wanted to be applied after death: ", iNewWanted)
				SET_BIT(iLocalBoolCheck, LBOOL_SETTING_AFTER_DEATH_WANTED)
			ENDIF
			
			PRINTLN("[WANTED] PROCESS_REAL_WANTED - Applying New Wanted Level: ", iNewWanted)
			
			SET_PLAYER_WANTED_LEVEL(LocalPlayer, iNewWanted)
			SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)		
			iPreviousWanted = -1 //Clear so we count this change from fake to real as a wanted change when swapping to and from the same number of stars
			
			IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_SCRIPT_FAKE_WANTED)
			AND MC_playerBD[iPartToUse].iFakeWanted = 0
				PRINTLN("[WANTED] PROCESS_REAL_WANTED - Clearing fake wanted")
				CLEAR_BIT(iLocalBoolCheck5, LBOOL5_SCRIPT_FAKE_WANTED)
				SET_DISPATCH_SERVICES(TRUE, IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_DISABLE_HELI_RESPONSE))
			ELSE
				PRINTLN("[WANTED] PROCESS_REAL_WANTED - Not clearing fake wanted, LBOOL5_SCRIPT_FAKE_WANTED = ", BOOL_TO_STRING(IS_BIT_SET(iLocalBoolCheck5, LBOOL5_SCRIPT_FAKE_WANTED)), " iFakeWanted = ", MC_playerBD[iPartToUse].iFakeWanted)
			ENDIF
		ENDIF
	ENDIF
				
	IF iNewWanted > 0
		PRINTLN("[WANTED] PROCESS_REAL_WANTED - Triggering alarms")
		SET_BIT(iLocalBoolCheck4, LBOOL4_TRIGGER_ALARMS)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_FORCE_SEARCH_NO_AMBIENT_COPS)
		PRINTLN("[WANTED] PROCESS_REAL_WANTED - Forcing search no ambient cops.")
		FORCE_START_HIDDEN_EVASION(LocalPlayer)
		SET_DISPATCH_SERVICES(FALSE, TRUE)
		SET_BIT(iLocalBoolCheck26, LBOOL26_DISABLED_LAW_DISPATCH_FOR_FORCED_SEARCH)
	ELIF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_DISABLED_LAW_DISPATCH_FOR_FORCED_SEARCH)
		PRINTLN("[WANTED] PROCESS_REAL_WANTED - Forced search with dispatch.")
		SET_DISPATCH_SERVICES(TRUE, IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_DISABLE_HELI_RESPONSE))
		CLEAR_BIT(iLocalBoolCheck26, LBOOL26_DISABLED_LAW_DISPATCH_FOR_FORCED_SEARCH)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_TIME_TO_LOSE_COPS_CLEAR_WANTED_MODE)
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTimeToLoseCops[iRule] >= 0
			PRINTLN("[WANTED] PROCESS_REAL_WANTED - Setting time to lose cops for all wanted levels: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTimeToLoseCops[iRule])
			INT i
			FOR i = 1 TO FMMC_MAX_WANTED_STARS
				SET_WANTED_LEVEL_HIDDEN_ESCAPE_TIME(LocalPlayer, i, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTimeToLoseCops[iRule])
			ENDFOR
			SET_BIT(iLocalBoolCheck33, LBOOL33_SET_TIME_TO_LOSE_COPS_CLEAR_MODE)
		ENDIF
	ELIF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_SET_TIME_TO_LOSE_COPS_CLEAR_MODE)
		PRINTLN("[WANTED] PROCESS_REAL_WANTED - Resetting time to lose cops for all wanted levels.")
		RESET_WANTED_LEVEL_HIDDEN_ESCAPE_TIME(LocalPlayer)
		CLEAR_BIT(iLocalBoolCheck33, LBOOL33_SET_TIME_TO_LOSE_COPS_CLEAR_MODE)
	ENDIF
		 
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: "Fake" Wanted
// ##### Description: Processing of "fake" wanted levels (the stars shown are set by script, the true code wanted level could be something different including being off)
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Determines if fake wanted should be handled by script or code. 
FUNC BOOL SHOULD_APPLY_NATIVE_FAKE_WANTED()
	IF ((MC_playerBD[iPartToUse].iWanted > 0) OR (iNewWantedThisFrame > 0)) //Player has a wanted level or is about to be given one
	AND NOT (iNewWantedThisFrame = 0) //Player's wanted level is not just about to be cleared
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Uses the SET_FAKE_WANTED_LEVEL native to display fake wanted stars. This will not work if the player has an existing real wanted level as that overrides the stars.
/// PARAMS:
PROC APPLY_NATIVE_FAKE_WANTED(INT iTeam, INT iRule, INT iFakeWanted)

	PRINTLN("[WANTED] APPLY_NATIVE_FAKE_WANTED - Fake wanted set to: ", iFakeWanted)
	
	SET_FAKE_WANTED_LEVEL(iFakeWanted)
	
	SET_DISPATCH_SERVICES(TRUE, IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_DISABLE_HELI_RESPONSE))
	CLEAR_BIT(iLocalBoolCheck5, LBOOL5_SCRIPT_FAKE_WANTED)
	
	iPreviousWanted = -1 //Clear so we count this change from fake to real as a wanted change when swapping to and from the same number of stars
	
ENDPROC

/// PURPOSE:
///    Applies a real wanted level to the player but blocks all dispatch to show a number of wanted stars without the player being chased. 
///    Unlike APPLY_NATIVE_FAKE_WANTED, existing ambient cops will continue to attack the player but no more will spawn.
PROC APPLY_SCRIPT_FAKE_WANTED(INT iTeam, INT iRule, INT iFakeWanted)
	
	PRINTLN("[WANTED] APPLY_SCRIPT_FAKE_WANTED - Fake wanted set to 0, real wanted set to: ", iFakeWanted)
		
	SET_FAKE_WANTED_LEVEL(0)
	
	IF GET_MAX_WANTED_LEVEL() < iFakeWanted
		SET_MAX_WANTED_LEVEL(iFakeWanted)
	ENDIF
	
	SET_PLAYER_WANTED_LEVEL_NO_DROP(LocalPlayer, iFakeWanted)
	SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
	
	SET_DISPATCH_SERVICES(FALSE, IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_DISABLE_HELI_RESPONSE))
	SET_BIT(iLocalBoolCheck5, LBOOL5_SCRIPT_FAKE_WANTED)
	
	iPreviousWanted = -1 //Clear so we count this change from fake to real as a wanted change when swapping to and from the same number of stars
	
ENDPROC

FUNC BOOL SHOULD_AGGRO_BLOCK_FAKE_WANTED_WANTED_CHANGE(INT iTeam, INT iRule)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iFakeWantedOnAggroBitset, iRule)
		RETURN FALSE
	ENDIF
	
	RETURN SHOULD_AGGRO_BLOCK_WANTED_WANTED_CHANGE(iTeam, iRule)
	
ENDFUNC

FUNC BOOL SHOULD_FAKE_WANTED_LEVEL_BE_CHANGED_NOW(INT iTeam, INT iRule)
	
	IF SHOULD_AGGRO_BLOCK_FAKE_WANTED_WANTED_CHANGE(iTeam, iRule)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iFakeWantedAtMidBitset, iRule)
		IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iRule)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL HAS_FAKE_WANTED_DELAY_EXPIRED(INT iTeam, INT iRule)

	IF SHOULD_WANTED_DELAY_BE_SKIPPED(iTeam, iRule)
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedDelay[iRule] = 0
		RETURN TRUE
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(tdFakeWantedDelayTimer)
		PRINTLN("[WANTED] HAS_FAKE_WANTED_DELAY_EXPIRED - Starting wanted delay")
		REINIT_NET_TIMER(tdFakeWantedDelayTimer)
	ENDIF
				
	IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdFakeWantedDelayTimer) <= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedDelay[iRule]
		SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_SHOW_WANTED_DELAY_HUD_FAKE_WANTED_TIMER)
		RETURN FALSE
	ENDIF
	
	RESET_NET_TIMER(tdFakeWantedDelayTimer)
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_FAKE_WANTED(INT iTeam, INT iRule)

	IF bIsAnySpectator
		EXIT
	ENDIF
	
	IF iLocalFakeWantedHighestPriority = iRule
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED)
		EXIT
	ENDIF
				
	IF NOT SHOULD_FAKE_WANTED_LEVEL_BE_CHANGED_NOW(iTeam, iRule)
		IF HAS_NET_TIMER_STARTED(tdFakeWantedDelayTimer)
			RESET_NET_TIMER(tdFakeWantedDelayTimer)
		ENDIF
		EXIT
	ENDIF
			
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iFakeWantedLevel[iRule] <= 0
		IF MC_playerBD[iPartToUse].iFakeWanted = 0	
			iLocalFakeWantedHighestPriority = iRule
			EXIT
		ENDIF
	ENDIF
		
	IF NOT HAS_FAKE_WANTED_DELAY_EXPIRED(iTeam, iRule)
		EXIT
	ENDIF
	
	iLocalFakeWantedHighestPriority = iRule
							
	INT iFakeWanted = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iFakeWantedLevel[iRule]
	PRINTLN("[WANTED] PROCESS_FAKE_WANTED - New Wanted Level: ", iFakeWanted)
							
	IF iFakeWanted > 0
		PRINTLN("[WANTED] PROCESS_FAKE_WANTED - Triggering alarms")
		SET_BIT(iLocalBoolCheck4, LBOOL4_TRIGGER_ALARMS)
	ENDIF
							
	MC_playerBD[iPartToUse].iFakeWanted = iFakeWanted
	
	IF SHOULD_APPLY_NATIVE_FAKE_WANTED()
		APPLY_NATIVE_FAKE_WANTED(iTeam, iRule, iFakeWanted)
	ELSE
		APPLY_SCRIPT_FAKE_WANTED(iTeam, iRule, iFakeWanted)
	ENDIF
	
	CLEAR_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_CHECK_REAL_TO_FAKE_WANTED_SWAP)
	SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_BLOCK_REAL_TO_FAKE_WANTED_SWAP)

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Max Wanted
// ##### Description: Fucntions for maintaining the maximum wanted level the player can currently get
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MAX_WANTED_LEVEL(INT iTeam, INT iRule)

	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMaxWanted[iRule] = ciMISSION_MAX_WANTED_OFF
		EXIT
	ENDIF
	
	IF iPreviousMaxDisplayedWanted != -1
		EXIT
	ENDIF
			
	//Previously this was set to MC_serverBD.iPolice then immediately clobbered - if this needs to work from a lobby setting in future it can be added here.
	INT iMaxWanted = GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMaxWanted[iRule])
	
	#IF IS_DEBUG_BUILD
	IF iPreviousMaxWanted != iMaxWanted
		PRINTLN("[WANTED] PROCESS_MAX_WANTED_LEVEL - Setting max wanted level to ", iMaxWanted)
		iPreviousMaxWanted = iMaxWanted
	ENDIF
	#ENDIF
	SET_MAX_WANTED_LEVEL(iMaxWanted)
	
	IF GET_PLAYER_WANTED_LEVEL(LocalPlayer) > iMaxWanted
		SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
		PRINTLN("[WANTED] PROCESS_MAX_WANTED_LEVEL - New max wanted level is less than current wanted level! Setting wanted to ", iMaxWanted, " now")
	ENDIF

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Main Wanted Processing
// ##### Description: Main processing functions for wanted changes
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_FAKE_DISPLAYED_WANTED_LEVELS(INT iTeam, INT iRule)
	INT iCurrentWanted = GET_PLAYER_WANTED_LEVEL(LocalPlayer)
	IF iPreviousRealDisplayedWanted != -1
		iCurrentWanted = iPreviousRealDisplayedWanted
	ENDIF
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedLevelDisplayBitset[iRule] != 0
		INT iDisplayedLevel = GET_DISPLAYED_WANTED_LEVEL(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedLevelDisplayBitset[iRule], iCurrentWanted)
		IF iDisplayedLevel <= iCurrentWanted
			iDisplayedLevel = -1
		ENDIF
		IF iFakeDisplayedWanted != iDisplayedLevel
			IF iDisplayedLevel = -1
				SET_FAKE_WANTED_LEVEL(0)
				iFakeDisplayedWanted = -1
				PRINTLN("[WANTED] PROCESS_FAKE_DISPLAYED_WANTED_LEVELS - clearing fake displayed wanted as Displayed Level: ", iDisplayedLevel, " <= Current Wanted: ", iCurrentWanted)
			ELSE
				SET_FAKE_WANTED_LEVEL(iDisplayedLevel)
				iFakeDisplayedWanted = iDisplayedLevel
				PRINTLN("[WANTED] PROCESS_FAKE_DISPLAYED_WANTED_LEVELS - Displaying wanted level: ", iCurrentWanted, " as ", iDisplayedLevel, " stars")
			ENDIF
		ENDIF
	ELIF iFakeDisplayedWanted != -1
		SET_FAKE_WANTED_LEVEL(0)
		iFakeDisplayedWanted = -1
		PRINTLN("[WANTED] PROCESS_FAKE_DISPLAYED_WANTED_LEVELS - clearing fake displayed wanted")
	ENDIF

ENDPROC

PROC PROCESS_FAKE_WANTED_DISPLAY_FLASHING_STAR_SPOOFING(INT iTeam, INT iRule)
	
	IF iFakeDisplayedWanted = -1
		EXIT
	ENDIF
	
	SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_BLOCK_REAL_TO_FAKE_WANTED_SWAP)
	
	IF ARE_PLAYER_FLASHING_STARS_ABOUT_TO_DROP(LocalPlayer)
	OR ARE_PLAYER_STARS_GREYED_OUT(LocalPlayer)
		
		IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_SCRIPT_FAKE_WANTED)
			iPreviousRealDisplayedWanted = GET_PLAYER_WANTED_LEVEL(LocalPlayer)
			iPreviousMaxDisplayedWanted = GET_MAX_WANTED_LEVEL()
			INT iTimeToEscape = GET_WANTED_LEVEL_TIME_TO_ESCAPE()
			
			PRINTLN("[WANTED] PROCESS_FAKE_WANTED_DISPLAY_FLASHING_STAR_SPOOFING - Swapping to real wanted for flashing stars (real before: ", iPreviousRealDisplayedWanted, " new: ", iFakeDisplayedWanted, " previous max: ", iPreviousMaxDisplayedWanted, ")")
			APPLY_SCRIPT_FAKE_WANTED(iTeam, iRule, iFakeDisplayedWanted)
			
			SET_WANTED_LEVEL_HIDDEN_ESCAPE_TIME(LocalPlayer, iFakeDisplayedWanted, iTimeToEscape)
			
			ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
			BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_AUTOMOBILE, FALSE)
			
			IF iPreviousRealDisplayedWanted >= 3
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_DISABLE_HELI_RESPONSE)
				PRINTLN("[WANTED] PROCESS_FAKE_WANTED_DISPLAY_FLASHING_STAR_SPOOFING - Over 3 Stars, keep spawning helis.")
				ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
				BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_HELICOPTER, FALSE)
			ENDIF
		ENDIF
			
		EXIT
		
	ENDIF
			
	IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_SCRIPT_FAKE_WANTED)
		
		PRINTLN("[WANTED] PROCESS_FAKE_WANTED_DISPLAY_FLASHING_STAR_SPOOFING - Removing spoofed wanted")
	
		IF GET_PLAYER_WANTED_LEVEL(LocalPlayer) > 0
			PRINTLN("[WANTED] PROCESS_FAKE_WANTED_DISPLAY_FLASHING_STAR_SPOOFING - Swapping back to fake wanted (spoofed before: ", iFakeDisplayedWanted, " new: ", iPreviousRealDisplayedWanted, " previous max: ", iPreviousMaxDisplayedWanted, ")")
			SET_MAX_WANTED_LEVEL(iPreviousMaxDisplayedWanted)
			SET_PLAYER_WANTED_LEVEL(LocalPlayer, iPreviousRealDisplayedWanted)
			SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
		ENDIF
		
		APPLY_NATIVE_FAKE_WANTED(iTeam, iRule, iFakeDisplayedWanted)
		iPreviousRealDisplayedWanted = -1
		iPreviousMaxDisplayedWanted = -1
		PROCESS_FAKE_DISPLAYED_WANTED_LEVELS(iTeam, iRule)
		RESET_WANTED_LEVEL_HIDDEN_ESCAPE_TIME(LocalPlayer)
	ENDIF
	
ENDPROC

PROC PROCESS_WANTED_CHANGED_THIS_FRAME(INT iTeam, INT iRule)
	
	INT iCurrentWanted = GET_PLAYER_WANTED_LEVEL(LocalPlayer)
	
	IF iPreviousRealDisplayedWanted != -1
	AND iCurrentWanted != 0
		iCurrentWanted = iPreviousRealDisplayedWanted
	ENDIF
	
	IF iPreviousWanted = iCurrentWanted
		EXIT
	ENDIF
	
	IF !bLocalPlayerOk
	OR IS_BIT_SET(iLocalBoolCheck, LBOOL_SETTING_AFTER_DEATH_WANTED)
		//Wait until the player is alive to process wanted changes
		EXIT
	ENDIF
	
	IF iCurrentWanted != MC_playerBD[iPartToUse].iWanted
		IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_SCRIPT_FAKE_WANTED)
			IF iCurrentWanted != MC_playerBD[iPartToUse].iFakeWanted
				SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_CHECK_REAL_TO_FAKE_WANTED_SWAP)
				MC_playerBD[iPartToUse].iWanted = iCurrentWanted
				PRINTLN("[WANTED] PROCESS_WANTED_CHANGED_THIS_FRAME - Set LBOOL_RESET1_CHECK_REAL_TO_FAKE_WANTED_SWAP (Fake Wanted)")
			ELSE
				MC_playerBD[iPartToUse].iWanted = 0
				PRINTLN("[WANTED] PROCESS_WANTED_CHANGED_THIS_FRAME - Set playerBD wanted level to zero")
			ENDIF
		ELSE
			IF MC_playerBD[iPartToUse].iWanted = 0
				SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_CHECK_REAL_TO_FAKE_WANTED_SWAP)
				PRINTLN("[WANTED] PROCESS_WANTED_CHANGED_THIS_FRAME - Set LBOOL_RESET1_CHECK_REAL_TO_FAKE_WANTED_SWAP (Real Wanted)")
			ENDIF
			MC_playerBD[iPartToUse].iWanted = iCurrentWanted
		ENDIF
	ENDIF

	IF MC_playerBD[iPartToUse].iWanted > 0
		IF iPreviousWanted < MC_playerBD[iPartToUse].iWanted
			MC_playerBD[iPartToUse].iNumWantedStars = MC_playerBD[iPartToUse].iNumWantedStars + (MC_playerBD[iPartToUse].iWanted - iPreviousWanted)
		ENDIF
		IF NOT HAS_NET_TIMER_STARTED(tdWantedTimer)
			START_NET_TIMER(tdWantedTimer)
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED)
		AND HAS_NET_TIMER_STARTED(tdWantedTimer)
			MC_playerBD[iPartToUse].iWantedTime = MC_playerBD[iPartToUse].iWantedTime + GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdWantedTimer)
			MC_playerBD[iPartToUse].iNumWantedLose++
			RESET_NET_TIMER(tdWantedTimer)
		ENDIF
	ENDIF
	
	PROCESS_FAKE_DISPLAYED_WANTED_LEVELS(iTeam, iRule)
	
	PRINTLN("[WANTED] PROCESS_WANTED_CHANGED_THIS_FRAME - Wanted level changed from ", iPreviousWanted, " to ", iCurrentWanted)
	
	iPreviousWanted = iCurrentWanted
	
ENDPROC

PROC PROCESS_SPECTATOR_WANTED(INT iTeam, INT iRule)
	
	IF !bIsAnySpectator
		EXIT
	ENDIF
	
	SET_BLOCK_WANTED_FLASH(IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_FLASHING_WANTED_STARS))
	
ENDPROC

PROC PROCESS_EVERY_FRAME_WANTED_FUNCTIONALITY(INT iTeam, INT iRule)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_HIDE_WANTED_STARS)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
		MPGlobalsAmbience.bWantedStarsHidden = TRUE
	ELSE
		MPGlobalsAmbience.bWantedStarsHidden = FALSE
	ENDIF
	
	IF bLocalPlayerPedOk
		IF MC_playerBD[iPartToUse].iWanted > 0
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableSpikeStripRoadBlocks, TRUE)
		ENDIF

		IF NOT IS_BIT_SET(iLocalBoolCheck, LBOOL_SETTING_AFTER_DEATH_WANTED)
		AND iPreviousMaxDisplayedWanted = -1
			IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_SCRIPT_FAKE_WANTED)
			OR IS_BIT_SET(iLocalBoolCheck26, LBOOL26_DISABLED_LAW_DISPATCH_FOR_FORCED_SEARCH)
				IF iWantedBeforeDeath != MC_playerBD[iPartToUse].iWanted
					iWantedBeforeDeath = MC_playerBD[iPartToUse].iWanted
					PRINTLN("[WANTED] PROCESS_EVERY_FRAME_WANTED_FUNCTIONALITY - Wanted Before Death updated to real wanted level: ", iWantedBeforeDeath)
				ENDIF
			ELSE
				IF iWantedBeforeDeath != MC_playerBD[iPartToUse].iFakeWanted
					iWantedBeforeDeath = MC_playerBD[iPartToUse].iFakeWanted
					PRINTLN("[WANTED] PROCESS_EVERY_FRAME_WANTED_FUNCTIONALITY - Wanted Before Death updated to fake wanted level: ", iWantedBeforeDeath)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bLocalPlayerOk
		//Holding up the fake wanted levels when we need to represent them using a real level:
		IF MC_playerBD[iPartToUse].iFakeWanted > 0
		AND IS_BIT_SET(iLocalBoolCheck5, LBOOL5_SCRIPT_FAKE_WANTED)
			REPORT_POLICE_SPOTTED_PLAYER(LocalPlayer)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_FORCE_NO_WANTED_FLASH)
		FORCE_OFF_WANTED_STAR_FLASH(FALSE)
	ENDIF
	
	PROCESS_FAKE_WANTED_DISPLAY_FLASHING_STAR_SPOOFING(iTeam, iRule)

ENDPROC

PROC PROCESS_NEW_OBJECTIVE_WANTED_FUNCTIONALITY(INT iTeam, INT iRule)

	IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		EXIT
	ENDIF
	
	IF bLocalPlayerPedOk
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_BLOCK_POLICE_HELICOPTERS_LANDING)
			IF NOT GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_BlockDispatchedHelicoptersFromLanding)
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_BlockDispatchedHelicoptersFromLanding, TRUE)
				PRINTLN("[WANTED] PROCESS_NEW_OBJECTIVE_WANTED_PROCESSING - Setting PCF_BlockDispatchedHelicoptersFromLanding")
			ENDIF
		ELIF GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_BlockDispatchedHelicoptersFromLanding)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_BlockDispatchedHelicoptersFromLanding, FALSE)
			PRINTLN("[WANTED] PROCESS_NEW_OBJECTIVE_WANTED_PROCESSING - Clearing PCF_BlockDispatchedHelicoptersFromLanding")
		ENDIF
	ENDIF
	
	PROCESS_FAKE_DISPLAYED_WANTED_LEVELS(iTeam, iRule)
	
ENDPROC

PROC PROCESS_LOSE_WANTED_DIFFICULTY(INT iTeam, INT iRule)
	
	IF !bLocalPlayerOK
		EXIT
	ENDIF
	
	IF iWantedBeforeDeath <= ciFMMC_EASY_WANTED_THRESHOLD
		IF HAS_NET_TIMER_STARTED(tdWantedDifficultyTimer)
			RESET_NET_TIMER(tdWantedDifficultyTimer)
			RESET_WANTED_LEVEL_DIFFICULTY(LocalPlayer)
			CLEAR_BIT(iLocalBoolCheck, LBOOL_MADE_WANTED_EASY)
		ENDIF
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck, LBOOL_MADE_WANTED_EASY)
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(tdWantedDifficultyTimer)
		PRINTLN("[WANTED] PROCESS_LOSE_WANTED_DIFFICULTY - Starting easy wanted delay timer, time: ", GET_EASY_WANTED_TIME(iTeam, iRule))
		REINIT_NET_TIMER(tdWantedDifficultyTimer)
	ENDIF

	IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdWantedDifficultyTimer) > GET_EASY_WANTED_TIME(iTeam, iRule)
		PRINTLN("[WANTED] PROCESS_LOSE_WANTED_DIFFICULTY - Setting easy wanted.")
		SET_WANTED_LEVEL_DIFFICULTY(LocalPlayer, 0.0)
		SET_BIT(iLocalBoolCheck, LBOOL_MADE_WANTED_EASY)
	ENDIF
	
ENDPROC

PROC PROCESS_CANT_LOSE_WANTED(INT iTeam, INT iRule)

	IF bIsAnySpectator
		EXIT
	ENDIF
				
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_CANT_LOSE_WANTED)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_FLASHING_WANTED_STARS)
		EXIT
	ENDIF
	
	SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(LocalPlayer)
				
	IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE)
	OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_FORCE_SEARCH_NO_AMBIENT_COPS)
	OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_ALLOW_SEARCHING_WHEN_CANT_LOSE_WANTED)
		ALLOW_EVASION_HUD_IF_DISABLING_HIDDEN_EVASION_THIS_FRAME(LocalPlayer, 1)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_FLASHING_WANTED_STARS)
		ALLOW_EVASION_HUD_IF_DISABLING_HIDDEN_EVASION_THIS_FRAME(LocalPlayer, g_FMMC_STRUCT.fCopDelayedReportTime)
		
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		OR iNewWantedThisFrame > 0
			SET_BLOCK_WANTED_FLASH(TRUE)
			FORCE_START_HIDDEN_EVASION(LocalPlayer)
		ELSE
			SET_BLOCK_WANTED_FLASH(FALSE)
		ENDIF
	ELSE
		FORCE_OFF_WANTED_STAR_FLASH(TRUE)
		SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_FORCE_NO_WANTED_FLASH)
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(LocalPlayer) > 0
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_CANT_LOSE_WANTED)
		REPORT_POLICE_SPOTTED_PLAYER(LocalPlayer)
	ENDIF

ENDPROC

PROC PROCESS_WANTED_OBJECTIVE_BLOCKER(INT iTeam, INT iRule)
	
	IF bIsAnySpectator
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedLevelReq[iRule] = ciFMMC_WANTED_REQUIREMENT_OFF
		EXIT
	ENDIF

	INT iWanted = GET_PLAYER_WANTED_LEVEL(LocalPlayer)
	
	IF iWanted = 0
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedLevelReq[iRule] > ciFMMC_WANTED_REQUIREMENT_ANY
		IF GET_PLAYER_WANTED_LEVEL(LocalPlayer) < g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedLevelReq[iRule] + 1
			EXIT
		ENDIF
	ENDIF
	
	BLOCK_OBJECTIVE_THIS_FRAME()
	
ENDPROC

/// PURPOSE:
///     Processes swapping from a real wanted level to a fake one - this disables the spawning of additional cops but allows the leftover cops to continue attacking the player.
PROC PROCESS_REAL_TO_FAKE_WANTED_SWAP(INT iTeam, INT iRule)
	
	IF bIsAnySpectator
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_BLOCK_REAL_TO_FAKE_WANTED_SWAP)
		EXIT
	ENDIF
						
	IF iNewWantedThisFrame = -1
	AND NOT IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_CHECK_REAL_TO_FAKE_WANTED_SWAP)
		EXIT
	ENDIF
	
	INT iFakeWanted = MC_playerBD[iPartToUse].iFakeWanted
	
	IF iFakeWanted = 0
		EXIT
	ENDIF
	
	PRINTLN("[WANTED] PROCESS_REAL_TO_FAKE_WANTED_SWAP - Swapping Fake/Real Wanted levels this frame.")
					
	IF SHOULD_APPLY_NATIVE_FAKE_WANTED()
		APPLY_NATIVE_FAKE_WANTED(iTeam, iRule, iFakeWanted)
	ELSE
		APPLY_SCRIPT_FAKE_WANTED(iTeam, iRule, iFakeWanted)
	ENDIF
			
ENDPROC

PROC PROCESS_LOCAL_PLAYER_WANTED()

	IF IS_PLAYER_IN_WANTED_BLOCKING_ZONE()
		EXIT
	ENDIF
		
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]	

	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	PROCESS_MAX_WANTED_LEVEL(iTeam, iRule)
	
	PROCESS_WANTED_CHANGED_THIS_FRAME(iTeam, iRule)
	
	PROCESS_WANTED_BOUNDS(iTeam, iRule)
	
	PROCESS_LOSE_WANTED_DIFFICULTY(iTeam, iRule)
	
	PROCESS_CANT_LOSE_WANTED(iTeam, iRule)
	
	PROCESS_WANTED_OBJECTIVE_BLOCKER(iTeam, iRule)
	
	PROCESS_REAL_WANTED(iTeam, iRule)
	
	PROCESS_FAKE_WANTED(iTeam, iRule)
	
	PROCESS_EVERY_FRAME_WANTED_FUNCTIONALITY(iTeam, iRule)
	
	PROCESS_REAL_TO_FAKE_WANTED_SWAP(iTeam, iRule)
	
	PROCESS_SPECTATOR_WANTED(iTeam, iRule)
	
	PROCESS_NEW_OBJECTIVE_WANTED_FUNCTIONALITY(iTeam, iRule)

ENDPROC
