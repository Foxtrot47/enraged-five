// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Rewards - Tuner Contracts -----------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script Name: FM_Mission_Controller_2020.sc ------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Money, RP, Rewards, Stats & Telemetry for the Tuner Contracts and Fixer Data Leaks.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_EventBroadcasting_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cash Rewards
// ##### Description: Functions for calculating cash rewards
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT CALCULATE_TUNER_ROBBERY_REWARD(BOOL bPass)

	IF NOT bPass
		PRINTLN("[Rewards_Cash] - CALCULATE_TUNER_ROBBERY_REWARD - FAILED: No Cash Reward")
		RETURN 0
	ENDIF
	
	TUNER_ROBBERIES eRobbery = GET_TUNER_ROBBERY_FINALE_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash)
	IF eRobbery = TR_NONE
		PRINTLN("[Rewards_Cash] - CALCULATE_TUNER_ROBBERY_REWARD - FAILED: Not on a tuner robbery.")
		RETURN 0
	ENDIF
	
	INT iPart = -1
	BOOL bLeader = (GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer)
	INT iLeaderReward = GET_TUNER_ROBBERY_CASH_REWARD(eRobbery, FALSE)
	INT iGoonReward = GET_TUNER_ROBBERY_CASH_REWARD(eRobbery, TRUE)
	
	IF eRobbery = TR_UNION_DEPOSITORY
		INT iRemainingPlayers = 0
		iPart = -1
		WHILE DO_PARTICIPANT_LOOP(iPart, DPLF_EARLY_END_SPECTATORS) //Looks odd but only way to count players efficiently after the mission is over as iNumberOfPlayingPlayers = 0
			IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
				RELOOP
			ENDIF
		
			IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
				RELOOP
			ENDIF
			iRemainingPlayers++
		ENDWHILE
		
		IF iRemainingPlayers > 0
			iRemainingPlayers-- //Remove 1 as tunable includes 1 bag's reward
		ENDIF
		
		PRINTLN("[Rewards_Cash] - CALCULATE_TUNER_ROBBERY_REWARD - Union Dep, players remaining: ", iRemainingPlayers, " Goon reward boost: ", iRemainingPlayers * g_sMPTunables.iTUNER_ROBBERY_GOON_UNION_DEP_BAG_BONUS , " Leader reward boost: ", iRemainingPlayers * g_sMPTunables.iTUNER_ROBBERY_LEADER_UNION_DEP_BAG_BONUS)
		iGoonReward += iRemainingPlayers * g_sMPTunables.iTUNER_ROBBERY_GOON_UNION_DEP_BAG_BONUS
		iLeaderReward += iRemainingPlayers * g_sMPTunables.iTUNER_ROBBERY_LEADER_UNION_DEP_BAG_BONUS
	ENDIF
	
	PRINTLN("[Rewards_Cash] - CALCULATE_TUNER_ROBBERY_REWARD - Robbery: ", TUNER_ROBBERIES_TO_STRING(eRobbery), " Leader reward: $", iLeaderReward, " Goon reward: $", iGoonReward)
	
	missionEndShardData.bRunOnReturnToFreemode = TRUE
	missionEndShardData.eType = MISSION_END_SHARD_TYPE_TUNER_ROBBERY
	missionEndShardData.bPass = bPass
	missionEndShardData.iID = ENUM_TO_INT(eRobbery)
	missionEndShardData.iApproachID = ENUM_TO_INT(g_mnMyRaceModel)
	missionEndShardData.iTargetID = ENUM_TO_INT(eRobbery)
	missionEndShardData.iEliteChallengeCash = 0
	missionEndShardData.iTake[0] = iLeaderReward
	
	FLOAT fContactFee = g_sMPTunables.fTUNER_ROBBERY_CONTACT_FEE
	INT iContactFee = ROUND(TO_FLOAT(iLeaderReward) * fContactFee)
	IF eRobbery = TR_FREIGHT_TRAIN
		PRINTLN("[Rewards_Cash] - CALCULATE_TUNER_ROBBERY_REWARD - ECU Job, squashing contact fee")
		fContactFee = 0.0
		iContactFee = 0
		missionEndShardData.npcTake[0].iTake = 0
	ELSE
		iLeaderReward -= iContactFee
		missionEndShardData.npcTake[0].iTake = iContactFee * -1
	ENDIF
	PRINTLN("[Rewards_Cash] - CALCULATE_TUNER_ROBBERY_REWARD - Contact Fee %: ", fContactFee, " Contact Fee: $", iContactFee, " Adjusted Leader Reward: $", iLeaderReward)
	
	INT iGangBoss = NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())
	IF iGangBoss > -1
		INT iPlayer
		FOR iPlayer = 0 TO MISSION_END_SHARD_MAX_PLAYERS - 1
			missionEndShardData.playerCut[iPlayer].bLeader 					= FALSE
			missionEndShardData.playerCut[iPlayer].playerID 				= INVALID_PLAYER_INDEX()
			missionEndShardData.playerCut[iPlayer].playerName				= ""
			missionEndShardData.playerCut[iPlayer].iCut						= 0
		ENDFOR
		
		iPart = -1
		INT iEndShardPlayerIndex = 0
		WHILE DO_PARTICIPANT_LOOP(iPart, DPLF_FILL_PLAYER_IDS | DPLF_EARLY_END_SPECTATORS)
			
			IF iEndShardPlayerIndex >= MISSION_END_SHARD_MAX_PLAYERS
				PRINTLN("[Rewards_Cash] - CALCULATE_TUNER_ROBBERY_REWARD - Exceeded max!")
				RELOOP
			ENDIF
			
			PLAYER_INDEX playerID = piParticipantLoop_PlayerIndex
			iPlayer = NATIVE_TO_INT(playerID)
			
			missionEndShardData.playerCut[iEndShardPlayerIndex].bLeader = (iGangBoss = iPlayer)
			missionEndShardData.playerCut[iEndShardPlayerIndex].playerID = playerID
			IF IS_NET_PLAYER_OK(playerID, FALSE)
				missionEndShardData.playerCut[iEndShardPlayerIndex].playerName = GET_PLAYER_NAME(playerID)
			ENDIF
			
			missionEndShardData.playerCut[iEndShardPlayerIndex].iCut = PICK_INT(iGangBoss = iPlayer, iLeaderReward, iGoonReward)
			
			PRINTLN("[Rewards_Cash] - CALCULATE_TUNER_ROBBERY_REWARD - Player ", iEndShardPlayerIndex, " Cut: $", 		missionEndShardData.playerCut[iEndShardPlayerIndex].iCut)
			PRINTLN("[Rewards_Cash] - CALCULATE_TUNER_ROBBERY_REWARD - Player ", iEndShardPlayerIndex, " playerName: ", missionEndShardData.playerCut[iEndShardPlayerIndex].playerName)
			PRINTLN("[Rewards_Cash] - CALCULATE_TUNER_ROBBERY_REWARD - Player ", iEndShardPlayerIndex, " playerID: ", 	NATIVE_TO_INT(missionEndShardData.playerCut[iEndShardPlayerIndex].playerID))
			PRINTLN("[Rewards_Cash] - CALCULATE_TUNER_ROBBERY_REWARD - Player ", iEndShardPlayerIndex, " bLeader: ", 	BOOL_TO_STRING(missionEndShardData.playerCut[iEndShardPlayerIndex].bLeader))
			
			iEndShardPlayerIndex++
						
		ENDWHILE
		
	ENDIF
	
	INT iMyReward = PICK_INT(bLeader, iLeaderReward, iGoonReward)
	PRINTLN("[Rewards_Cash] - CALCULATE_TUNER_ROBBERY_REWARD - My Reward: $", iMyReward)
	
	IF bLeader
		INCREMENT_MP_INT_CHARACHTER_STAT(iMyReward, MP_STAT_TUNER_EARNINGS)
	ENDIF
	
	RETURN iMyReward
	
ENDFUNC

PROC PROCESS_GIVING_TUNER_CONTRACT_CASH_REWARD(INT iCachedReward)
	
	INT iScriptTransactionIndex
	
	IF USE_SERVER_TRANSACTIONS()
		TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_TUNER_ROBBERY_FINALE, iCachedReward, iScriptTransactionIndex, DEFAULT, DEFAULT)
		g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
		g_cashTransactionData[iScriptTransactionIndex].eEssentialData.iItemID = iCachedReward
		g_cashTransactionData[iScriptTransactionIndex].eEssentialData.iCost = iCachedReward
	ELSE
		NETWORK_EARN_TUNER_ROBBERY(iCachedReward, iCachedReward, GET_HASH_KEY(g_FMMC_STRUCT.tl31LoadedContentID),  -1, -1)
	ENDIF	
	
ENDPROC

//Fixer
FUNC INT CALCULATE_FIXER_FINALE_REWARD(BOOL bPass)

	IF NOT bPass
		PRINTLN("[Rewards_Cash] - CALCULATE_FIXER_FINALE_REWARD - FAILED: No Cash Reward")
		RETURN 0
	ENDIF
	
	IF GET_FIXER_STORY_MISSION_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash) != FSM_FINALE
		PRINTLN("[Rewards_Cash] - CALCULATE_FIXER_FINALE_REWARD - FAILED: Not on fixer finale.")
		RETURN 0
	ENDIF
	
	BOOL bLeader = (GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer)
	INT iLeaderReward = GET_FIXER_FINALE_CASH_REWARD(FALSE)
	INT iGoonReward = GET_FIXER_FINALE_CASH_REWARD(TRUE)
	
	PRINTLN("[Rewards_Cash] - CALCULATE_FIXER_FINALE_REWARD - Leader reward: $", iLeaderReward, " Goon reward: $", iGoonReward)
		
	INT iMyReward = PICK_INT(bLeader, iLeaderReward, iGoonReward)
	PRINTLN("[Rewards_Cash] - CALCULATE_FIXER_FINALE_REWARD - My Reward: $", iMyReward)
	
	IF bLeader
		INCREMENT_MP_INT_CHARACHTER_STAT(iMyReward, MP_STAT_FIXER_EARNINGS)
	ENDIF
	
	RETURN iMyReward
	
ENDFUNC

FUNC INT CALCULATE_SHORT_TRIPS_REWARD(BOOL bPass)

	IF NOT bPass
		PRINTLN("[Rewards_Cash] - CALCULATE_SHORT_TRIPS_REWARD - FAILED: No Cash Reward")
		RETURN 0
	ENDIF

	INT iMyReward = 0
	SWITCH GET_FIXER_SHORT_TRIP_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash)
		CASE FST_SHORT_TRIP_1 //Seed Capital
			iMyReward = g_sMPTunables.iFIXER_SHORT_TRIP_1_CASH_REWARD
		BREAK
		CASE FST_SHORT_TRIP_2 //Fire It Up
			iMyReward = g_sMPTunables.iFIXER_SHORT_TRIP_2_CASH_REWARD
		BREAK
		CASE FST_SHORT_TRIP_3 //OG Kush
			iMyReward = g_sMPTunables.iFIXER_SHORT_TRIP_3_CASH_REWARD
		BREAK
	ENDSWITCH
	
	IF g_FMMC_STRUCT.iDifficulity = DIFF_HARD
		PRINTLN("[Rewards_Cash] - CALCULATE_SHORT_TRIPS_REWARD - Hard, scaling by ", g_sMPTunables.fFIXER_SHORT_TRIP_HARD_MULTIPLIER)
		iMyReward = FLOOR(iMyReward * g_sMPTunables.fFIXER_SHORT_TRIP_HARD_MULTIPLIER)
	ENDIF
	
	PRINTLN("[Rewards_Cash] - CALCULATE_SHORT_TRIPS_REWARD - My Reward: $", iMyReward)
	
	RETURN iMyReward
	
ENDFUNC

PROC PROCESS_GIVING_FIXER_CONTRACT_CASH_REWARD(INT iCachedReward)
	
	INT iScriptTransactionIndex
	
	IF USE_SERVER_TRANSACTIONS()
		TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AGENCY_STORY_FINALE, iCachedReward, iScriptTransactionIndex, DEFAULT, DEFAULT)
		g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
		g_cashTransactionData[iScriptTransactionIndex].eEssentialData.iItemID = iCachedReward
		g_cashTransactionData[iScriptTransactionIndex].eEssentialData.iCost = iCachedReward
		g_cashTransactionData[iScriptTransactionIndex].eEssentialData.iExtraItemID = g_FMMC_STRUCT.iRootContentIDHash
	ELSE
		NETWORK_EARN_FINALE(iCachedReward, g_FMMC_STRUCT.iRootContentIDHash)
	ENDIF	
	
ENDPROC

PROC PROCESS_GIVING_FIXER_SHORT_TRIP_CASH_REWARD(INT iCachedReward)
	
	INT iScriptTransactionIndex
	
	IF USE_SERVER_TRANSACTIONS()
		TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_MUSIC_STUDIO_SHORT_TRIP, iCachedReward, iScriptTransactionIndex, DEFAULT, DEFAULT)
		g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
		g_cashTransactionData[iScriptTransactionIndex].eEssentialData.iItemID = iCachedReward
		g_cashTransactionData[iScriptTransactionIndex].eEssentialData.iCost = iCachedReward
		g_cashTransactionData[iScriptTransactionIndex].eEssentialData.iExtraItemID = g_FMMC_STRUCT.iRootContentIDHash
	ELSE
		NETWORK_EARN_FIXER_AGENCY_SHORT_TRIP(iCachedReward, g_FMMC_STRUCT.iRootContentIDHash)
	ENDIF	
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: RP Rewards
// ##### Description: Functions for calculating cash rewards
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Awards and Challenges
// ##### Description: Functions for giving awards/achievements
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_TUNER_ROBBERY_AWARDS_CONTRACTUAL_CRIMINAL()
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != LocalPlayer
		EXIT
	ENDIF
	
	INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_ROBBERY_CONTRACT)
	
	PRINTLN("[Rewards] - PROCESS_TUNER_ROBBERY_AWARDS_CONTRACTUAL_CRIMINAL - Incremented to: ", GET_MP_INT_CHARACTER_AWARD(MP_AWARD_ROBBERY_CONTRACT))
	
ENDPROC

PROC PROCESS_TUNER_ROBBERY_MISSION_COMPLETION_AWARDS()
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != LocalPlayer
		EXIT
	ENDIF
	
	TUNER_ROBBERIES eCompletedMission = GET_TUNER_ROBBERY_FINALE_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash)
	MP_BOOL_AWARD eAward = MAX_NUM_MP_BOOL_AWARD
	
	SWITCH eCompletedMission
		CASE TR_UNION_DEPOSITORY
			eAward = MP_AWARD_UNION_DEPOSITORY
		BREAK
		CASE TR_MILITARY_CONVOY
			eAward = MP_AWARD_MILITARY_CONVOY
		BREAK
		CASE TR_FLEECA_BANK
			eAward = MP_AWARD_FLEECA_BANK
		BREAK
		CASE TR_FREIGHT_TRAIN
			eAward = MP_AWARD_FREIGHT_TRAIN
		BREAK
		CASE TR_BOLINGBROKE
			eAward = MP_AWARD_BOLINGBROKE_ASS
		BREAK
		CASE TR_IAA_RAID
			eAward = MP_AWARD_IAA_RAID
		BREAK
		CASE TR_METH_JOB
			eAward = MP_AWARD_METH_JOB
		BREAK
		CASE TR_BUNKER
			eAward = MP_AWARD_BUNKER_RAID
		BREAK
	ENDSWITCH
	
	IF eAward = MAX_NUM_MP_BOOL_AWARD
		PRINTLN("[Rewards] - PROCESS_TUNER_ROBBERY_MISSION_COMPLETION_AWARDS - Mission: ", TUNER_ROBBERIES_TO_STRING(eCompletedMission), " - No award set up.")
		EXIT
	ENDIF
	
	PRINTLN("[Rewards] - PROCESS_TUNER_ROBBERY_MISSION_COMPLETION_AWARDS - Mission: ", TUNER_ROBBERIES_TO_STRING(eCompletedMission), " Completed.")
	
	PROCESS_GIVING_FMMC_CHARACTER_AWARD(eAward)
	
ENDPROC


PROC PROCESS_TUNER_ROBBERY_AWARDS()

	PROCESS_TUNER_ROBBERY_MISSION_COMPLETION_AWARDS()
	PROCESS_TUNER_ROBBERY_AWARDS_CONTRACTUAL_CRIMINAL()
	AWARD_CAR_CLUB_REP(EARN_REP_TYPE_AUTO_SHOP_CONTRACT_FINALE)
	
ENDPROC
