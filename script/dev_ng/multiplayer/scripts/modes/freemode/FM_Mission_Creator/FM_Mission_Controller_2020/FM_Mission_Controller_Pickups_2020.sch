// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Pickups -------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Handles pickups in mission controller                                                                                                                     
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_EntityObjectives_2020.sch"


				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Script Pickups
// ##### Description: Functions for controller created pickups
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT FIND_FREE_INDEX_FOR_PED_AMMO_PICKUP()
	INT i
	FOR i = 0 TO ciFMMC_MAX_PED_AMMO_DROPS-1
		IF NOT DOES_PICKUP_EXIST(sLocalPedAmmoDrop[i].piAmmoPickup)
			RETURN i
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

PROC DROP_AMMO_FROM_PED(PED_INDEX PedVictim)
	INT iChance = GET_RANDOM_INT_IN_RANGE(0, 100)
	IF iChance <= g_FMMC_STRUCT.sPedAmmoDrop.iChanceToDrop
		INT iFreeIndex = FIND_FREE_INDEX_FOR_PED_AMMO_PICKUP()
		IF iFreeIndex != -1
			VECTOR vAmmoPos = GET_ENTITY_COORDS(pedVictim, FALSE)
			vAmmoPos.x += GET_RANDOM_FLOAT_IN_RANGE(-0.5, 0.5)
			vAmmoPos.y += GET_RANDOM_FLOAT_IN_RANGE(-0.5, 0.5)
			FLOAT fGroundZ
			IF GET_GROUND_Z_FOR_3D_COORD(vAmmoPos, fGroundZ)
				vAmmoPos.z = fGroundZ
			ENDIF
			BROADCAST_FMMC_SPAWN_PED_AMMO_PICKUP(iFreeIndex, vAmmoPos)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PED_DROP_AMMO_MKII(INT iAmmoType)
	SWITCH iAmmoType
		CASE FMMC_PED_AMMO_DROP_TYPE_RIFLE_FMJ
		CASE FMMC_PED_AMMO_DROP_TYPE_RIFLE_AP
		CASE FMMC_PED_AMMO_DROP_TYPE_RIFLE_INCEN
		CASE FMMC_PED_AMMO_DROP_TYPE_RIFLE_TRACER
		CASE FMMC_PED_AMMO_DROP_TYPE_SMG_FMJ
		CASE FMMC_PED_AMMO_DROP_TYPE_SMG_HOLLOW
		CASE FMMC_PED_AMMO_DROP_TYPE_SMG_INCEN
		CASE FMMC_PED_AMMO_DROP_TYPE_SMG_TRACER
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC AMMO_TYPE GET_PED_DROP_AMMO_TYPE(INT iAmmoType)
	SWITCH iAmmoType
		CASE FMMC_PED_AMMO_DROP_TYPE_PISTOL				RETURN	AMMOTYPE_PISTOL
		CASE FMMC_PED_AMMO_DROP_TYPE_RIFLE				RETURN	AMMOTYPE_RIFLE
		CASE FMMC_PED_AMMO_DROP_TYPE_MACHINE_GUN 		RETURN	AMMOTYPE_MG
		CASE FMMC_PED_AMMO_DROP_TYPE_SHOTGUN 			RETURN	AMMOTYPE_SHOTGUN
		CASE FMMC_PED_AMMO_DROP_TYPE_SNIPER 			RETURN	AMMOTYPE_SNIPER
		CASE FMMC_PED_AMMO_DROP_TYPE_GRENADE_LAUNCHER 	RETURN	AMMOTYPE_GRENADE_LAUNCHER
		CASE FMMC_PED_AMMO_DROP_TYPE_RPG 				RETURN	AMMOTYPE_RPG
		CASE FMMC_PED_AMMO_DROP_TYPE_MINIGUN 			RETURN 	AMMOTYPE_MINIGUN
		CASE FMMC_PED_AMMO_DROP_TYPE_RIFLE_FMJ			RETURN	AMMOTYPE_DLC_RIFLE_FMJ
		CASE FMMC_PED_AMMO_DROP_TYPE_RIFLE_AP			RETURN	AMMOTYPE_DLC_RIFLE_ARMORPIERCING
		CASE FMMC_PED_AMMO_DROP_TYPE_RIFLE_INCEN		RETURN	AMMOTYPE_DLC_RIFLE_INCENDIARY
		CASE FMMC_PED_AMMO_DROP_TYPE_RIFLE_TRACER		RETURN	AMMOTYPE_DLC_RIFLE_TRACER
		CASE FMMC_PED_AMMO_DROP_TYPE_SMG_FMJ			RETURN	AMMOTYPE_DLC_SMG_FMJ
		CASE FMMC_PED_AMMO_DROP_TYPE_SMG_HOLLOW			RETURN	AMMOTYPE_DLC_SMG_HOLLOWPOINT
		CASE FMMC_PED_AMMO_DROP_TYPE_SMG_INCEN			RETURN	AMMOTYPE_DLC_SMG_INCENDIARY
		CASE FMMC_PED_AMMO_DROP_TYPE_SMG_TRACER			RETURN	AMMOTYPE_DLC_SMG_TRACER
	ENDSWITCH
	RETURN AMMOTYPE_INVALID
ENDFUNC

PROC PROCESS_PED_DROPPED_AMMO(INT iAmmoPickup)
	
	IF g_FMMC_STRUCT.sPedAmmoDrop.iAmmoPickupType = FMMC_PED_AMMO_DROP_TYPE_INVALID
		EXIT
	ENDIF
	
	IF DOES_PICKUP_EXIST(sLocalPedAmmoDrop[iAmmoPickup].piAmmoPickup)
		IF HAS_NET_TIMER_STARTED(sLocalPedAmmoDrop[iAmmoPickup].tdLifeSpan)
		AND HAS_NET_TIMER_EXPIRED_READ_ONLY(sLocalPedAmmoDrop[iAmmoPickup].tdLifeSpan, (g_FMMC_STRUCT.sPedAmmoDrop.iLifeTime * 1000))
			BROADCAST_FMMC_REMOVE_PED_AMMO_PICKUP(iAmmoPickup)
		ENDIF
	ENDIF

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Pickups and Weapon HUD --------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to pickups and weapon HUD systems  ---------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_PICKUP_MARKERS(FMMC_PICKUP_STATE sPickupState)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iPlacedBitset, ciFMMC_WEP_ShowArrowMarkerAbovePickup)
	AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iPlacedMarkerIndex = -1
		EXIT
	ENDIF
	
	IF NOT sPickupState.bPickupExists		
	OR NATIVE_TO_INT(GET_FMMC_PICKUP_OBJECT(sPickupState)) = -1
		EXIT
	ENDIF
		
	IF GET_FMMC_PICKUP_OBJECT(sPickupState) != NULL
	AND NATIVE_TO_INT(GET_FMMC_PICKUP_OBJECT(sPickupState)) > -1
		IF NOT IS_ENTITY_VISIBLE(GET_FMMC_PICKUP_OBJECT(sPickupState))	
			EXIT
		ENDIF
	ENDIF

	INT iIndex = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iPlacedMarkerIndex
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iPlacedBitset, ciFMMC_WEP_ShowArrowMarkerAbovePickup)
		iIndex = 0
	ENDIF
	
	PROCESS_PLACED_MARKER(g_FMMC_STRUCT_ENTITIES.sPlacedMarker[iIndex], GET_FMMC_PICKUP_COORDS(sPickupState.iIndex), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iPlacedBitset, ciFMMC_WEP_ShowArrowMarkerAbovePickup))
	
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Placed Pickups
// ##### Description: Functions for creator placed pickups
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_PICKUP_RESPAWN_AND_COLLECTION(FMMC_PICKUP_STATE &sPickupState)
	IF sPickupState.bPickupExists
		IF HAS_FMMC_PICKUP_BEEN_COLLECTED(sPickupState)
			IF NOT FMMC_IS_LONG_BIT_SET(iPickupCollected, sPickupState.iIndex)
				FMMC_SET_LONG_BIT(iPickupCollected, sPickupState.iIndex)
				PRINTLN("[Pickups][Pickup ",sPickupState.iIndex,"] PROCESS_PICKUP_RESPAWN_AND_COLLECTION - Collected Pickup.")
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iCustomPickupType = ciCUSTOM_PICKUP_TYPE__MISSION_EQUIPMENT
					INCREMENT_MEDAL_EQUIPMENT_COLLECTED()
				ENDIF			
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF FMMC_IS_LONG_BIT_SET(iPickupCollected, sPickupState.iIndex)
				PRINTLN("[Pickups][Pickup ",sPickupState.iIndex,"] PROCESS_PICKUP_RESPAWN_AND_COLLECTION - Respawned Pickup.")
			ENDIF
			#ENDIF
			FMMC_CLEAR_LONG_BIT(iPickupCollected, sPickupState.iIndex)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_STATIC_PICKUPS(FMMC_PICKUP_STATE &sPickupState)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_STATIC_PICKUPS)
		IF DOES_FMMC_PICKUP_OBJECT_ENTITY_EXIST(sPickupState)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iPlacedBitset, ciFMMC_WEP_Rotation_Override)
			SET_ENTITY_ROTATION(GET_FMMC_PICKUP_OBJECT(sPickupState), <<0,0,0>>)
		ENDIF
	ENDIF
ENDPROC

PROC SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME(INT iPickup, BLIP_SPRITE &bsSprite, TEXT_LABEL_15 &tl15BlipNameOverride)
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iVehicleWeaponPickupType
		CASE ciVEH_WEP_ROCKETS
			bsSprite = RADAR_TRACE_ROCKETS
			tl15BlipNameOverride = "BLIP_368"
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Rockets")
		BREAK
		
		CASE ciVEH_WEP_SPEED_BOOST
			bsSprite = RADAR_TRACE_BOOST
			tl15BlipNameOverride = "BLIP_354"
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Speed Boost")
		BREAK
		
		CASE ciVEH_WEP_GHOST
			bsSprite = RADAR_TRACE_PICKUP_GHOST
			tl15BlipNameOverride = "BLIP_484"
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Ghost")
		BREAK
		
		CASE ciVEH_WEP_BEAST
			bsSprite = RADAR_TRACE_PICKUP_ARMOURED
			tl15BlipNameOverride = "BLIP_487"
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Armoured")
		BREAK
		
		CASE ciVEH_WEP_FORCE_ACCELERATE
			bsSprite = RADAR_TRACE_PICKUP_ACCELERATOR
			tl15BlipNameOverride = "BLIP_483"
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Accelerator")
		BREAK
		
		CASE ciVEH_WEP_FLIPPED_CONTROLS
			bsSprite = RADAR_TRACE_PICKUP_SWAP
			tl15BlipNameOverride = "PU_SW2"
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Flipped")
		BREAK
		
		CASE ciVEH_WEP_ZONED
			bsSprite = RADAR_TRACE_PICKUP_ZONED
			tl15BlipNameOverride = "PU_BT"
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Zoned")
		BREAK
		
		CASE ciVEH_WEP_DETONATE
			bsSprite = RADAR_TRACE_PICKUP_DETONATOR
			tl15BlipNameOverride = "BLIP_485"
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Detonator")
		BREAK
		
		CASE ciVEH_WEP_BOMB
			bsSprite = RADAR_TRACE_PICKUP_BOMB
			tl15BlipNameOverride = "BLIP_486"
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Bomb")
		BREAK
		
		CASE ciVEH_WEP_BOUNCE
			bsSprite = RADAR_TRACE_PICKUP_JUMP
			tl15BlipNameOverride = "BLIP_515"
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Hop")
		BREAK
		
		CASE ciVEH_WEP_PRON
			bsSprite = RADAR_TRACE_PICKUP_DEADLINE
			tl15BlipNameOverride = "BLIP_522"
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Deadline")
		BREAK
		
		CASE ciVEH_WEP_REPAIR
			//repair
			bsSprite = RADAR_TRACE_PICKUP_REPAIR
			tl15BlipNameOverride = "BLIP_524"
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Repair")
		BREAK
		
		CASE ciVEH_WEP_BOMB_LENGTH
			//Bomb Length
			IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iVehBombLengthIncrease < 0
				bsSprite = RADAR_TRACE_PICKUP_DTB_BLAST_DECREASE
				tl15BlipNameOverride = "BLIP_623"
			ELSE
				bsSprite = RADAR_TRACE_PICKUP_DTB_BLAST_INCREASE
				tl15BlipNameOverride = "BLIP_622"
			ENDIF
			
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Bomb Length")
		BREAK
		
		CASE ciVEH_WEP_BOMB_MAX
			//Bomb Max	
			IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iVehBombMaxIncrease < 0
				bsSprite = RADAR_TRACE_PICKUP_DTB_BOMB_DECREASE
				tl15BlipNameOverride = "BLIP_625"
			ELSE
				bsSprite = RADAR_TRACE_PICKUP_DTB_BOMB_INCREASE
				tl15BlipNameOverride = "BLIP_624"
			ENDIF
			
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Bomb Max")
		BREAK
		
		CASE ciVEH_WEP_EXTRA_LIFE
			//Increase life
			bsSprite = RADAR_TRACE_PICKUP_DTB_HEALTH
			tl15BlipNameOverride = "BLIP_621"
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Extra Life")
		BREAK
		
		CASE ciVEH_WEP_RANDOM
			bsSprite = RADAR_TRACE_PICKUP_RANDOM
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Random")
		BREAK
		
		CASE ciVEH_WEP_MACHINE_GUN
			bsSprite = RADAR_TRACE_PICKUP_MACHINEGUN
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Machine gun")
		BREAK
		
		CASE ciVEH_WEP_RANDOM_SPECIAL_VEH
			bsSprite = RADAR_TRACE_STEERINGWHEEL
			tl15BlipNameOverride = "BLIP_RSV"
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Random special Veh")
		BREAK
		
		CASE ciVEH_WEP_RUINER_SPECIAL_VEH
			bsSprite = RADAR_TRACE_EX_VECH_3
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Ruiner")
		BREAK
		
		CASE ciVEH_WEP_RAMP_SPECIAL_VEH
			bsSprite = RADAR_TRACE_EX_VECH_4
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Ramp Buggy")
		BREAK
	ENDSWITCH
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iCustomPickupType
		CASE ciCUSTOM_PICKUP_TYPE__WEAPON_BAG
			tl15BlipNameOverride = "PU_WPBG"
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Weapons Bag")
		BREAK
		CASE ciCUSTOM_PICKUP_TYPE__MISSION_EQUIPMENT
			tl15BlipNameOverride = GET_PUBLIC_FACING_NAME_FOR_MISSION_EQUIPMENT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iCustomPickupModel)
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Mission Equipment")
		BREAK
		CASE ciCUSTOM_PICKUP_TYPE__EXTRA_TAKE
			tl15BlipNameOverride = "PU_XTTK"
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Extra Take")
		BREAK
	ENDSWITCH
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].pt
		CASE PICKUP_WEAPON_DLC_FERTILIZERCAN 
			tl15BlipNameOverride = "WT_FERTCAN"
			PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME Setting custom blip name - Fertilizer Can")
		BREAK
	ENDSWITCH
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iVehicleWeaponPickupType = -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iCustomPickupType = ciCUSTOM_PICKUP_TYPE__NONE
		bsSprite = GET_CORRECT_BLIP_SPRITE_FMMC(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].pt)
		PRINTLN("[Pickups][Pickup ",iPickup,"]  SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME no rage/vehicle pickup type set")
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iCustomPickupType != ciCUSTOM_PICKUP_TYPE__NONE
		bsSprite = GET_CORRECT_BLIP_SPRITE_FMMC(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].pt, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iSubType, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iCustomPickupType)
	ENDIF
ENDPROC

PROC CREATE_PICKUP_BLIP(FMMC_PICKUP_STATE &sPickupState, PICKUP_TYPE ptType)
	
	VECTOR vPickupCoords = GET_FMMC_PICKUP_COORDS(sPickupState.iIndex)
	PRINTLN("[Pickups][Pickup ",sPickupState.iIndex,"] [Blips] CREATE_PICKUP_BLIP - Adding Blip at ", vPickupCoords)

	CREATE_BLIP_WITH_CREATOR_SETTINGS(
		FMMC_BLIP_PICKUP, 
		biPickup[sPickupState.iIndex], 
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].sWeaponBlipStruct, 
		sMissionPickupsLocalVars[sPickupState.iIndex].sBlipRuntimeVars, 
		NULL, 
		sPickupState.iIndex, 
		vPickupCoords, 
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iAggroIndexBS_Entity_Weapon
	)
	
	IF NOT DOES_BLIP_EXIST(biPickup[sPickupState.iIndex])
		EXIT
	ENDIF
	
	IF NOT IS_BLIP_SPRITE_OVERRIDE_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].sWeaponBlipStruct)

		IF ptType = PICKUP_CUSTOM_SCRIPT
		OR ptType = PICKUP_VEHICLE_CUSTOM_SCRIPT
		OR ptType = PICKUP_VEHICLE_CUSTOM_SCRIPT_LOW_GLOW

			BLIP_SPRITE bsSprite
			TEXT_LABEL_15 tl15BlipNameOverride
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciRANDOM_PICKUP_TYPES_OPTION)
			AND g_FMMC_STRUCT_ENTITIES.bRandomPickupTypes = TRUE
				bsSprite = RADAR_TRACE_PICKUP_RANDOM
			ELSE
				
				SET_DEFAULT_PICKUP_BLIP_SPRITE_AND_NAME(sPickupState.iIndex, bsSprite, tl15BlipNameOverride)
				
				SET_BLIP_SPRITE(biPickup[sPickupState.iIndex], bsSprite)
			
				IF NOT IS_STRING_NULL_OR_EMPTY(tl15BlipNameOverride)
					SET_BLIP_NAME_FROM_TEXT_FILE(biPickup[sPickupState.iIndex], tl15BlipNameOverride)
				ENDIF
			ENDIF
			
		ELSE
			SET_BLIP_SPRITE(biPickup[sPickupState.iIndex], GET_CORRECT_BLIP_SPRITE_FMMC(ptType))
		ENDIF
		
		IF NOT MC_IS_PICKUP_TYPE_INVALID_NAME(ptType)
			MC_SET_WEAPON_BLIP_NAME_FROM_PICKUP_TYPE(biPickup[sPickupState.iIndex], ptType)
		ENDIF
		
		IF IS_PICKUP_HEALTH(ptType)
			SET_BLIP_COLOUR(biPickup[sPickupState.iIndex], BLIP_COLOUR_GREEN)
		ELIF IS_PICKUP_ARMOUR(ptType)
		OR ptType = PICKUP_CUSTOM_SCRIPT
			IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iCustomPickupType = ciCUSTOM_PICKUP_TYPE__NONE
				SET_BLIP_COLOUR(biPickup[sPickupState.iIndex], BLIP_COLOUR_BLUE)
			ENDIF
		ENDIF
	ENDIF
			
	SET_BLIP_AS_SHORT_RANGE(biPickup[sPickupState.iIndex], TRUE)
	
ENDPROC

FUNC BOOL SHOULD_PICKUP_BE_BLIPPED(FMMC_PICKUP_STATE &sPickupState, BLIP_RUNTIME_VARS_STRUCT &sBlipRuntimeVars)

	IF SHOULD_ENTITY_BLIP_BE_FORCED_OFF(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].sWeaponBlipStruct)
		RETURN FALSE
	ENDIF

	IF SHOULD_ENTITY_BLIP_BE_FORCED_ON(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].sWeaponBlipStruct, GET_FMMC_PICKUP_OBJECT(sPickupState))
		RETURN TRUE
	ENDIF
	
	IF IS_BLIP_SET_AS_HIDDEN(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].sWeaponBlipStruct)
		RETURN FALSE
	ENDIF
	
	IF NOT SHOULD_ENTITY_BE_BLIPPED(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].sWeaponBlipStruct, sBlipRuntimeVars, GET_FMMC_PICKUP_OBJECT(sPickupState), FALSE)
		RETURN FALSE
	ENDIF
	
	IF NOT WAS_PLAYER_PREVIOUSLY_IN_RANGE_OF_BLIP(sBlipRuntimeVars)
		
		IF NOT IS_PLAYER_WITHIN_COORD_BLIP_RANGE(GET_FMMC_PICKUP_COORDS(sPickupState.iIndex), g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].sWeaponBlipStruct)
			RETURN FALSE
		ENDIF
		
		IF NOT IS_COORD_WITHIN_HEIGHT_DIFFERENCE_RANGE_FOR_BLIP(GET_FMMC_PICKUP_COORDS(sPickupState.iIndex), g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].sWeaponBlipStruct)
			RETURN FALSE
		ENDIF
		
	ENDIF
	
	IF NOT IS_BLIP_COORD_IN_SAME_INTERIOR(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].sWeaponBlipStruct, GET_FMMC_PICKUP_COORDS(sPickupState.iIndex))
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iPlacedBitset, ciFMMC_WEP_DroppedByEvent)
	AND IS_VECTOR_ZERO(vDroppedPickupLocation[sPickupState.iIndex])
		// Don't blip the pickup yet, it isn't placed properly yet
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_PICKUP_BLIPS(FMMC_PICKUP_STATE &sPickupState)

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciHIDE_BLIP_PICKUPS)
		EXIT
	ENDIF
	
	IF sPickupState.bPickupExists
		
		IF NOT DOES_FMMC_PICKUP_OBJECT_EXIST(sPickupState)
			IF DOES_BLIP_EXIST(biPickup[sPickupState.iIndex])
				PRINTLN("[Pickups][Pickup ",sPickupState.iIndex,"] [Blips] Pickup object no longer exists - deleting pickup blip")
				REMOVE_BLIP(biPickup[sPickupState.iIndex])
			ENDIF
		ELSE
			IF SHOULD_PICKUP_BE_BLIPPED(sPickupState, sMissionPickupsLocalVars[sPickupState.iIndex].sBlipRuntimeVars)
				IF NOT DOES_BLIP_EXIST(biPickup[sPickupState.iIndex])
					CREATE_PICKUP_BLIP(sPickupState, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].pt)
				ELSE
					PROCESS_ENTITY_BLIP(biPickup[sPickupState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].sWeaponBlipStruct, GET_FMMC_PICKUP_COORDS(sPickupState.iIndex), g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iAggroIndexBS_Entity_Weapon)
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(biPickup[sPickupState.iIndex])
					REMOVE_BLIP(biPickup[sPickupState.iIndex])
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(biPickup[sPickupState.iIndex])
			REMOVE_BLIP(biPickup[sPickupState.iIndex])
		ENDIF
	ENDIF

ENDPROC

PROC ACTIVATE_PICKUP(FMMC_PICKUP_STATE &sPickupState)
	
	PRINTLN("[MCSpawning][Pickups][Pickup ",sPickupState.iIndex,"]  ACTIVATE_PICKUP - [ ",sPickupState.iIndex," ] Making visible and collectable")
	SET_PICKUP_UNCOLLECTABLE(sPickupState.puiIndex, FALSE)
	SET_PICKUP_HIDDEN_WHEN_UNCOLLECTABLE(sPickupState.puiIndex, FALSE)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciENABLE_POWER_DESPAWN_SOUND)
		PLAY_SOUND_FROM_COORD(-1, "Powerup_Respawn", GET_FMMC_PICKUP_COORDS(sPickupState.iIndex), "POWER_PLAY_General_Soundset")
	ENDIF
	
	FMMC_SET_LONG_BIT(iActiveWeapons, sPickupState.iIndex)
	
ENDPROC

PROC DEACTIVATE_PICKUP(FMMC_PICKUP_STATE &sPickupState)
	
	STRING sPickUpSoundSet = "POWER_PLAY_General_Soundset"

	PRINTLN("[MCSpawning][Pickups][Pickup ",sPickupState.iIndex,"]  DEACTIVATE_PICKUP - [",sPickupState.iIndex,"] Making invisible and uncollectable")
	SET_PICKUP_UNCOLLECTABLE(sPickupState.puiIndex, TRUE)
	SET_PICKUP_HIDDEN_WHEN_UNCOLLECTABLE(sPickupState.puiIndex, TRUE)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciENABLE_POWER_DESPAWN_SOUND)
		PLAY_SOUND_FROM_COORD(-1, "Powerup_Despawn", GET_FMMC_PICKUP_COORDS(sPickupState.iIndex), sPickUpSoundSet)
		PRINTLN("[MCSpawning][Pickups][Pickup ",sPickupState.iIndex,"]  DEACTIVATE_PICKUP - playing despawn sound")
	ENDIF
	
	IF DOES_BLIP_EXIST(biPickup[sPickupState.iIndex])
		REMOVE_BLIP(biPickup[sPickupState.iIndex])
	ENDIF
	
	FMMC_CLEAR_LONG_BIT(iActiveWeapons, sPickupState.iIndex)
	
ENDPROC

PROC PROCESS_PICKUP_SPAWNING(FMMC_PICKUP_STATE &sPickupState)
	
	IF sPickupState.bPickupExists
		IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iAssociatedObjective != -1
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iPlacedBitset, ciFMMC_WEP_SPAWN_IF_OPPONENT_SCORES)
		OR IS_BIT_SET(iLocalBoolCheck17, LBOOL17_TEAM_SWAP_PICKUP_PROCESS)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iPlacedBitset, ciFMMC_WEP_DestroyLocallyWhenCollected)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iPlacedBitset, ciFMMC_WEP_DestroyGloballyWhenCollected)
		OR g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iPickupSpawnWithEntity_Type != ciENTITY_TYPE_NONE
			IF NOT FMMC_IS_LONG_BIT_SET(iActiveWeapons, sPickupState.iIndex)
				IF MC_SHOULD_WEP_SPAWN_NOW(sPickupState.iIndex)
				AND MC_IS_THIS_PICKUP_VISIBLE_TO_THIS_TEAM(sPickupState.iIndex, MC_playerBD[iLocalPart].iTeam)
					PRINTLN("[MCSpawning][Pickups][Pickup ",sPickupState.iIndex,"]  PROCESS_PICKUP_SPAWNING - Weapon should load now - ", sPickupState.iIndex)
					IF NOT SHOULD_CLEANUP_PICKUP(sPickupState)
						ACTIVATE_PICKUP(sPickupState)
					ELSE
						PRINTLN("[MCSpawning][Pickups][Pickup ",sPickupState.iIndex,"]  PROCESS_PICKUP_SPAWNING - The following weapon should cleanup now! - ", sPickupState.iIndex)
					ENDIF
				ENDIF
			ELSE					
				IF SHOULD_CLEANUP_PICKUP(sPickupState)
					DEACTIVATE_PICKUP(sPickupState)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_PICKUP_BEING_UNCOLLECTABLE(FMMC_PICKUP_STATE &sPickupState)
	
	IF NOT DOES_PICKUP_EXIST(sPickupState.puiIndex)
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iUncollectableUntilRule = 0
		EXIT
	ENDIF
	
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]
	IF iRule >= g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iUncollectableUntilRule
		IF IS_LONG_BIT_SET(iPickupUncollectableBS, sPickupState.iIndex)
			PRINTLN("[Pickups][Pickup ",sPickupState.iIndex,"]  PROCESS_PICKUP_BEING_UNCOLLECTABLE - Setting to be collectable")
			SET_PICKUP_UNCOLLECTABLE(sPickupState.puiIndex, FALSE)
			SET_PICKUP_TRANSPARENT_WHEN_UNCOLLECTABLE(sPickupState.puiIndex, FALSE)
			CLEAR_LONG_BIT(iPickupUncollectableBS, sPickupState.iIndex)
		ENDIF
	ELSE
		IF NOT IS_LONG_BIT_SET(iPickupUncollectableBS, sPickupState.iIndex)
			PRINTLN("[Pickups][Pickup ",sPickupState.iIndex,"]  PROCESS_PICKUP_BEING_UNCOLLECTABLE - Setting to be uncollectable")
			SET_PICKUP_UNCOLLECTABLE(sPickupState.puiIndex, TRUE)
			SET_PICKUP_TRANSPARENT_WHEN_UNCOLLECTABLE(sPickupState.puiIndex, TRUE)
			SET_LONG_BIT(iPickupUncollectableBS, sPickupState.iIndex)
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_PICKUPS_STAGGERED_CLIENT(INT iPickup)
	
	FMMC_PICKUP_STATE sPickupState
	FILL_FMMC_PICKUP_STATE_STRUCT(sPickupState, iPickup)
	
	PROCESS_PICKUP_BLIPS(sPickupState)
	
	PROCESS_PICKUP_WEAPON_ATTACHMENTS(GET_FMMC_PICKUP_OBJECT(sPickupState), sPickupState.iIndex, DOES_FMMC_PICKUP_OBJECT_EXIST(sPickupState))
	
	PROCESS_PICKUP_SPAWNING(sPickupState)
	
	PROCESS_PICKUP_BEING_UNCOLLECTABLE(sPickupState)
	
	IF (IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME) AND IS_TEAM_ACTIVE(0))
	OR (IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_1_HAS_NEW_PRIORITY_THIS_FRAME) AND IS_TEAM_ACTIVE(1))
	OR (IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_2_HAS_NEW_PRIORITY_THIS_FRAME) AND IS_TEAM_ACTIVE(2))
	OR (IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_3_HAS_NEW_PRIORITY_THIS_FRAME) AND IS_TEAM_ACTIVE(3))
		iStaggeredWeaponIterator = 0
		INT iTeamMovedOn
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME)
			iTeamMovedOn = 1
		ELIF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_1_HAS_NEW_PRIORITY_THIS_FRAME)
			iTeamMovedOn = 2
		ELIF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_2_HAS_NEW_PRIORITY_THIS_FRAME)
			iTeamMovedOn = 3
		ELIF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_3_HAS_NEW_PRIORITY_THIS_FRAME)
			iTeamMovedOn = 4
		ENDIF
		PRINTLN("[Pickups][Pickup ",sPickupState.iIndex,"][SPAWNING] Resetting iStaggeredWeaponIterator to 0 as team: ", iTeamMovedOn," has moved to the next rule: ", MC_serverBD_4.iCurrentHighestPriority[iTeamMovedOn - 1])
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck17, LBOOL17_TEAM_SWAP_PICKUP_PROCESS)
		IF NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_PICKUP_STAGGERED_LOOP_INDEX_SET)
			iWeaponPickupStaggeredLoopIndex = iStaggeredWeaponIterator
			
			SET_BIT(iLocalBoolCheck17, LBOOL17_PICKUP_STAGGERED_LOOP_INDEX_SET)
		ELSE
			IF iWeaponPickupStaggeredLoopIndex = iStaggeredWeaponIterator
				CLEAR_BIT(iLocalBoolCheck17, LBOOL17_TEAM_SWAP_PICKUP_PROCESS)
				
				iWeaponPickupStaggeredLoopIndex = -1
				
				CLEAR_BIT(iLocalBoolCheck17, LBOOL17_PICKUP_STAGGERED_LOOP_INDEX_SET)
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iPickupPrerequisiteRequired != ciPREREQ_None
	AND sPickupState.bPickupExists
		SET_PICKUP_UNCOLLECTABLE(sPickupState.puiIndex, NOT IS_PREREQUISITE_COMPLETED(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iPickupPrerequisiteRequired))
	ENDIF
	
ENDPROC

PROC PROCESS_PICKUPS_IN_VEHICLES(FMMC_PICKUP_STATE& sPickupState)
	
	IF NOT sPickupState.bPickupExists
	OR HAS_FMMC_PICKUP_BEEN_COLLECTED(sPickupState)
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iPickupSpawnWithEntity_Type != ciENTITY_TYPE_VEHICLE
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iPickupDroppedFromVehicleBS, sPickupState.iIndex)
		// This pickup has already fallen out of its vehicle!
		EXIT
	ENDIF
	
	VEHICLE_INDEX viAttachedVehicle = GET_FMMC_ENTITY_VEHICLE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iPickupSpawnWithEntity_Index)
	VECTOR vStartCoords = GET_PICKUP_COORDS(sPickupState.puiIndex)
	VECTOR vTargetCoords = GET_FMMC_PICKUP_COORDS(sPickupState.iIndex)
	
	BOOL bDropPickupNow = FALSE
	
	IF DOES_ENTITY_EXIST(viAttachedVehicle)
	
		FLOAT fVehicleVelocityMagnitude = FMMC_GET_ENTITY_CURRENT_VELOCITY_MAGNITUDE(viAttachedVehicle)
		IF fVehicleVelocityMagnitude > cfPickupDropFromVehicleVelocity
		OR VDIST2(vStartCoords, vTargetCoords) >= cfPickupDropFromVehicleDistance
			// The vehicle is moving too fast to keep up, or has moved too far away from the pickup origin. Drop the pickup out of the back
			bDropPickupNow = TRUE
			
		ELSE
			// Comment this back in if we decide we need to try to keep the pickup in the back of the vehicle (the coords given by GET_FMMC_PICKUP_COORDS).
			// This sort of thing hasn't networked nicely in the past, but should be fine for small movements like this if needed.
			// The main problem is that pickups will put themselves on the ground if you change their coords, so these need to be called every frame
			//SET_ENTITY_COORDS_NO_OFFSET(sPickupState.oiPickupObject, vTargetCoords, DEFAULT, DEFAULT, FALSE)
			//SET_ENTITY_ROTATION(sPickupState.oiPickupObject, MC_GET_ROTATION_FOR_MISSION_PICKUP(sPickupState.iIndex))
			
		ENDIF
		
	ELSE
		// The "attached" vehicle no longer exists
		bDropPickupNow = TRUE
		
	ENDIF
	
	IF bDropPickupNow
		// Drop the pickup on the floor
		PRINTLN("[Pickups][Pickup ", sPickupState.iIndex, "] PROCESS_PICKUPS_IN_VEHICLES | Dropping pickup now!")
		PLACE_OBJECT_ON_GROUND_OR_OBJECT_PROPERLY(GET_FMMC_PICKUP_OBJECT(sPickupState))
		SET_BIT(iPickupDroppedFromVehicleBS, sPickupState.iIndex)
	ENDIF
	
ENDPROC

PROC PROCESS_EVERY_FRAME_PICKUPS(INT iPickup)
	
	FMMC_PICKUP_STATE sPickupState
	FILL_FMMC_PICKUP_STATE_STRUCT(sPickupState, iPickup)
	
	PROCESS_RESPAWNING_MISSION_PICKUPS(sPickupState)
	
	PROCESS_PICKUP_RESPAWN_AND_COLLECTION(sPickupState)
	
	PROCESS_STATIC_PICKUPS(sPickupState)
	
	PROCESS_PICKUP_MARKERS(sPickupState)
	
	PROCESS_PICKUPS_IN_VEHICLES(sPickupState)
	
ENDPROC
