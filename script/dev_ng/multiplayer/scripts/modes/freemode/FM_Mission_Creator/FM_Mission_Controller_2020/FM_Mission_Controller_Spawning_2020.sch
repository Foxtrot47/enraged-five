// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Player Spawning / Respawning --------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description:
// ##### Controls and processes the procedures involved with respawning the player after death or manually forcing a respawn.
// ##### Wrappers for the players start location can also be found here for use in round or mini-round restarts.
// ##### This includes the creation of the Team Vehicle.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
USING "FM_Mission_Controller_Include_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Local Player Utility Functions and Wrappers																						
// ##### Description: Wrappers and Helper Functions that are used in various places of spawning/respawning.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC CACHE_PLAYER_ALIVE_AND_IN_VEHICLE()
			
	// IS_PED_IN_ANY_VEHICLE will assert if we use it while the player is dead...
	IF IS_PED_IN_ANY_VEHICLE(localPlayerPed, TRUE)
		IF NOT g_bMissionWasInVehWhenAlive
			PRINTLN("Player is now in a Vehicle.")
			g_bMissionWasInVehWhenAlive = TRUE
		ENDIF
	ELSE
		IF g_bMissionWasInVehWhenAlive
			PRINTLN("Player NO longer in a Vehicle.")
			g_bMissionWasInVehWhenAlive = FALSE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_RULE_VALID_FOR_CUSTOM_SPAWN_POINT(INT iSpawnPoint, INT iTeam, INT iRule)

	IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].iValidForRules_Start != -1
		IF iRule < g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].iValidForRules_Start
			PRINTLN("[MC_CustomSpawning_Spam][iSpawnpoint ", iSpawnpoint, " ] - IS_RULE_VALID_FOR_CUSTOM_SPAWN_POINT - Not yet past iValidForRules_Start ",g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].iValidForRules_Start," for point ",iSpawnPoint)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].iValidForRules_End != -1
		IF iRule >= g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].iValidForRules_End
			PRINTLN("[MC_CustomSpawning_Spam][iSpawnpoint ", iSpawnpoint, " ] - IS_RULE_VALID_FOR_CUSTOM_SPAWN_POINT - Reached iValidForRules_End ",g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].iValidForRules_End," for point ",iSpawnPoint)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_SPAWN_POINT_BELONGING_TO_PARTICIPANT(INT iSpawnPoint, INT iTeam, INT iPartTarget)
	
	INT iFlag = 0
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
		IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_I_AM_PLAYER_0
		OR g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_I_AM_PLAYER_1
		OR g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_I_AM_PLAYER_2
		OR g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_I_AM_PLAYER_3
			INT iPart
			iPart = 3 + (ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].eSpawnConditionFlag[iFlag]) - ENUM_TO_INT(SPAWN_CONDITION_FLAG_I_AM_PLAYER_3))			
			iPart = GET_ACTIVE_PARTICIPANT_INDEX_FROM_NUMBER(iPart)
			
			IF iPart = iPartTarget
				RETURN TRUE
			ENDIF			
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC
     
FUNC BOOL IS_SPAWN_POINT_A_VALID_START_POINT(INT iSpawnpoint, INT iTeam = -1, BOOL bCheckContinuity = TRUE)
	
	IF iTeam = -1
		iTeam = MC_playerBD[iLocalPart].iteam
	ENDIF

	IF bCheckContinuity
	AND NOT DOES_START_POSITION_MEET_CONTINUITY_REQUIREMENTS(iSpawnpoint)
		PRINTLN("[MC_CustomSpawning_Spam][iSpawnpoint ", iSpawnpoint, " ] - IS_SPAWN_POINT_A_VALID_START_POINT - Returning FALSE due to DOES_START_POSITION_MEET_CONTINUITY_REQUIREMENTS")
		RETURN FALSE
	ENDIF
			
	IF MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].eSpawnConditionFlag, TRUE, FALSE, MC_PlayerBD[iPartToUse].iTeam)
		PRINTLN("[MC_CustomSpawning_Spam][iSpawnpoint ", iSpawnpoint, " ] - IS_SPAWN_POINT_A_VALID_START_POINT - Returning FALSE due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")				
		RETURN FALSE
	ENDIF
	
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iSpawnPoint, eSGET_TeamSpawnPoint, iTeam)
		PRINTLN("[MC_CustomSpawning_Spam][iSpawnpoint ", iSpawnpoint, " ] - IS_SPAWN_POINT_A_VALID_START_POINT - Returning FALSE due to MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP")
		RETURN FALSE
	ENDIF
		
	PRINTLN("[MC_CustomSpawning_Spam][iSpawnpoint ", iSpawnpoint, " ] - IS_SPAWN_POINT_A_VALID_START_POINT - Returning ", IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].iSpawnBitset, ci_SpawnBS_Initial_Start_Spawn_Point))
	RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].iSpawnBitset, ci_SpawnBS_Initial_Start_Spawn_Point)
ENDFUNC

PROC CACHE_RESPAWN_POINT_BLOCKED(INT i, INT iTeam)
	FMMC_SET_LONG_BIT(iRespawnPointIsBlockedBS[iTeam], i)
	PRINTLN("[MC_CustomSpawning_Spam][iSpawnpoint ", i, " ] - IS_SPAWN_POINT_A_VALID_RESPAWN_POINT - CACHE_RESPAWN_POINT_BLOCKED - Caching that the Respawn Point has been blocked from working.")
ENDPROC

PROC CACHE_RESPAWN_POINT_SHOULD_NOT_BE_VALID_THIS_FRAME(INT i, INT iTeam)
	FMMC_SET_LONG_BIT(iRespawnPointIsBlockedThisFrameBS[iTeam], i)
ENDPROC

PROC CACHE_RESPAWN_POINT_SHOULD_BE_VALID_THIS_FRAME(INT i, INT iTeam)
	FMMC_SET_LONG_BIT(iRespawnPointShouldBeValidNowBS[iTeam], i)
ENDPROC

FUNC BOOL IS_SPAWN_POINT_A_VALID_RESPAWN_POINT(INT iSpawnpoint, INT iTeam = -1, BOOL bCheckActivationRadius = TRUE)
	
	IF iTeam = -1
		iTeam = MC_playerBD[iLocalPart].iteam
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(iRespawnPointIsBlockedBS[iTeam], iSpawnpoint)
		RETURN FALSE
	ENDIF	
	IF FMMC_IS_LONG_BIT_SET(iRespawnPointIsBlockedThisFrameBS[iTeam], iSpawnpoint)
		RETURN FALSE
	ENDIF	
	IF FMMC_IS_LONG_BIT_SET(iRespawnPointShouldBeValidNowBS[iTeam], iSpawnpoint)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck34, LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES)
		CACHE_RESPAWN_POINT_SHOULD_NOT_BE_VALID_THIS_FRAME(iSpawnpoint, iTeam)
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_START_POSITION_MEET_CONTINUITY_REQUIREMENTS(iSpawnpoint #IF IS_DEBUG_BUILD , FALSE #ENDIF)
		PRINTLN("[MC_CustomSpawning_Spam][iSpawnpoint ", iSpawnpoint, " ] - IS_SPAWN_POINT_A_VALID_RESPAWN_POINT - Returning FALSE due to DOES_START_POSITION_MEET_CONTINUITY_REQUIREMENTS")
		CACHE_RESPAWN_POINT_BLOCKED(iSpawnpoint, iTeam)
		RETURN FALSE
	ENDIF
	
	IF MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].eSpawnConditionFlag, FALSE, FALSE, MC_PlayerBD[iPartToUse].iTeam)
		PRINTLN("[MC_CustomSpawning_Spam][iSpawnpoint ", iSpawnpoint, " ] - IS_SPAWN_POINT_A_VALID_RESPAWN_POINT - Returning FALSE due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")
		IF MC_ARE_SPAWN_CONDITION_FLAGS_MET_DURING_GAMEPLAY(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].eSpawnConditionFlag)			
			CACHE_RESPAWN_POINT_SHOULD_NOT_BE_VALID_THIS_FRAME(iSpawnpoint, iTeam)
		ELSE
			CACHE_RESPAWN_POINT_BLOCKED(iSpawnpoint, iTeam)
		ENDIF			
		RETURN FALSE
	ENDIF
	
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iSpawnPoint, eSGET_TeamSpawnPoint, iTeam)
		PRINTLN("[MC_CustomSpawning_Spam][iSpawnpoint ", iSpawnpoint, " ] - IS_SPAWN_POINT_A_VALID_RESPAWN_POINT - Returning FALSE due to MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP")
		CACHE_RESPAWN_POINT_BLOCKED(iSpawnpoint, iTeam)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_RULE_VALID_FOR_CUSTOM_SPAWN_POINT(iSpawnpoint, iTeam, MC_ServerBD_4.iCurrentHighestPriority[iTeam])
		PRINTLN("[MC_CustomSpawning_Spam][iSpawnpoint ", iSpawnpoint, " ] - IS_SPAWN_POINT_A_VALID_RESPAWN_POINT - Returning FALSE due to IS_RULE_VALID_FOR_CUSTOM_SPAWN_POINT")
		CACHE_RESPAWN_POINT_SHOULD_NOT_BE_VALID_THIS_FRAME(iSpawnpoint, iTeam)
		RETURN FALSE
	ENDIF
	
	IF bCheckActivationRadius
		IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].iActivationRadius > 0
		AND NOT FMMC_IS_LONG_BIT_SET(iBSRespawnActiveFromRadius[iTeam], iSpawnpoint)
			PRINTLN("[MC_CustomSpawning_Spam][iSpawnpoint ", iSpawnpoint, " ] - IS_SPAWN_POINT_A_VALID_RESPAWN_POINT - Returning FALSE has not been activated via radius yet.")			
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].iSpawnBitset, ci_SpawnBS_Treat_As_Respawn_Point)
ENDFUNC

FUNC BOOL APPLY_OUTFIT_TO_LOCAL_PLAYER(MP_OUTFIT_ENUM eOutfit)
	MP_OUTFITS_APPLY_DATA   sApplyData
	sApplyData.pedID		= LocalPlayerPed
	sApplyData.eApplyStage 	= AOS_SET
	sApplyData.eOutfit		= eOutfit
	RETURN SET_PED_MP_OUTFIT(sApplyData, FALSE, FALSE, FALSE, FALSE)
ENDFUNC

/// PURPOSE:
///    Returns the seat the player should be spawned in if they ended the previous mission in a tracked vehicle.
/// PARAMS:
///    iVehToEnter - The vehicle the player is being placed in
///    eDefaultSeat - Returned if the seat is invalid or not tracked
FUNC VEHICLE_SEAT GET_MISSION_CONTINUITY_VEHICLE_SEAT(INT iVehToEnter, VEHICLE_SEAT eDefaultSeat = VS_DRIVER)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_VehicleSeatTracking)
		RETURN eDefaultSeat
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehToEnter].iContinuityID = -1
		RETURN eDefaultSeat
	ENDIF
	
	IF sMissionLocalContinuityVars.iEndVehicleContinuityId != g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehToEnter].iContinuityID
		RETURN eDefaultSeat
	ENDIF
	
	RETURN sMissionLocalContinuityVars.eEndVehicleSeat
	
ENDFUNC

FUNC INT GET_CONTINUITY_START_POINT_INDEX(INT iTeam)
	
	IF (IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_LocationTracking)
	AND MC_serverBD_1.sMissionContinuityVars.iLocationTrackingBitset = 0)
	AND (IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ObjectTracking)
	AND MC_serverBD_1.sMissionContinuityVars.iObjectTrackingBitset[0] = 0
	AND MC_serverBD_1.sMissionContinuityVars.iObjectTrackingBitset[1] = 0)
		//No entities tracked
		RETURN -1
	ENDIF

	INT i
	INT iPartIndexOnTeam = GET_PARTICIPANT_NUMBER_IN_TEAM()
	INT iReturnIndex = -1
	INT iFoundForTeam = 0
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTeam] - 1
	
		IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][i].iLinkedContinuityID = -1
			RELOOP
		ENDIF
	
		IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][i].iLinkedContinuityEntityType = ciCONTINUITY_ENTITY_TYPE_LOCATION
			IF NOT IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iLocationTrackingBitset, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][i].iLinkedContinuityID)
				RELOOP
			ENDIF
		ELIF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][i].iLinkedContinuityEntityType = ciCONTINUITY_ENTITY_TYPE_OBJECT
			IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iObjectTrackingBitset, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][i].iLinkedContinuityID)
				RELOOP
			ENDIF
		ELSE
			RELOOP
		ENDIF
		
		IF NOT IS_SPAWN_POINT_A_VALID_START_POINT(i, iTeam, FALSE)
			RELOOP
		ENDIF
		
		iReturnIndex = i
		IF iFoundForTeam = iPartIndexOnTeam
			PRINTLN("[RCC MISSION][CONTINUITY] - GET_CONTINUITY_START_POINT_INDEX - point ", i," is valid for team ", iTeam, " and part on team ", iPartIndexOnTeam)
			BREAKLOOP
		ENDIF
		PRINTLN("[RCC MISSION][CONTINUITY] - GET_CONTINUITY_START_POINT_INDEX - point ", i," is valid for team ", iTeam)
		iFoundForTeam++
	ENDFOR

	RETURN iReturnIndex
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Special Case - General																						
// ##### Description: Wrappers and Helper Functions that are used in various places of spawning/respawning.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC INITIALISE_BOSS_TRUCK_SECTIONS()
	IF NETWORK_IS_ACTIVITY_SESSION()
		IF LocalPlayer = GB_GET_LOCAL_PLAYER_GANG_BOSS() 
			ARMORY_TRUCK_SECTION_TYPE_ENUM sectionOne, sectionTwo, sectionThree
			sectionOne = GET_TRUCK_SECTION_FROM_CREATOR_SELECTION(0)
			sectionTwo = GET_TRUCK_SECTION_FROM_CREATOR_SELECTION(1)
			sectionThree = GET_TRUCK_SECTION_FROM_CREATOR_SELECTION(2)
			VALIDATE_TRUCK_EMPTY_SECTIONS(sectionOne, sectionTwo, sectionThree)
			SET_PLAYER_TRUCK_SECTIONS_FOR_CREATOR_TRAILER(sectionOne, sectionTwo, sectionThree)
			SET_PLAYER_TRUCK_TINT_FOR_CREATOR_TRAILER(GET_TRUCK_TINT_FROM_CREATOR_SELECTION())
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_MANUAL_RESPAWN_RADIO()
	IF IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SHOULD_TURN_RADIO_BACK_ON_AFTER_ROUND_RESTART)
	
		//Cache the current radio station (MANUAL RESPAWN)
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_KEEP_RADIO_STATION_ON_RESPAWN)
			IF manualRespawnState = eManualRespawnState_OKAY_TO_SPAWN
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					IF DOES_PLAYER_VEH_HAVE_RADIO()
					AND IS_PLAYER_VEH_RADIO_ENABLE()
						IF bPlayerToUseOK
							IF bLocalPlayerPedOK
								IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
									IF iManRespawnLastRadioStation != GET_PLAYER_RADIO_STATION_INDEX()
										IF IS_VEHICLE_RADIO_ON(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
											iManRespawnLastRadioStation = GET_PLAYER_RADIO_STATION_INDEX()
											g_SpawnData.bRadioStationOverScore = TRUE
											PRINTLN("[RADIO] storing radio station as : ", GET_PLAYER_RADIO_STATION_INDEX())
										ELSE
											iManRespawnLastRadioStation = 255
											g_SpawnData.bRadioStationOverScore = FALSE
											PRINTLN("[RADIO] storing radio station as 255")
										ENDIF
										SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
										PRINTLN("[RADIO][JJT][ManRespawn] Setting radio station before respawn to: ", iManRespawnLastRadioStation )
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Special Case - Vehicle Passenger/Driver swap system.
// ##### Description: Swaps the passenger(s) and driver based on various pieces of data and inputs.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL USING_MULTIPLE_PASSENGER_TEAM_VEHICLE(INT iPlayerTeam)
	IF g_FMMC_STRUCT.iTeamVehicleRespawnPassengers[iPlayerTeam] >= 1
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
		PRINTLN("[LM][MULTI_VEH_RESPAWN] USING_MULTIPLE_PASSENGER_TEAM_VEHICLE - Using System, iPlayerTeam: ", iPlayerTeam, " g_FMMC_STRUCT.iTeamVehicleRespawnPassengers: ", g_FMMC_STRUCT.iTeamVehicleRespawnPassengers[iPlayerTeam])
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DRIVER_FOR_MULTIPLE_TEAM_VEHICLE(INT iPlayerTeam)	
	IF iPlayerTeam > -1
		IF USING_MULTIPLE_PASSENGER_TEAM_VEHICLE(iPlayerTeam)		
			IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) = iLocalPart
				PRINTLN("[LM][MULTI_VEH_RESPAWN] IS_DRIVER_FOR_MULTIPLE_TEAM_VEHICLE - Returning True.")
				RETURN TRUE
			ENDIF
			
			PRINTLN("[LM][MULTI_VEH_RESPAWN] IS_DRIVER_FOR_MULTIPLE_TEAM_VEHICLE - Returning False ")
			
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL SET_PASSENGER_INTO_VEHICLE()
	PRINTLN("[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - Called.")
	
	INT iDriverPart = GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())
	
	IF iDriverPart > -1
		PRINTLN("[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - (Final) iDriverPart: ", iDriverPart)
		
		VEHICLE_INDEX veh
		PED_INDEX ped = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDriverPart)))
		
		//Part checks already performed in the above functions.	
		IF NOT IS_PED_INJURED(ped)
		AND NOT IS_PLAYER_RESPAWNING(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDriverPart)))
			IF IS_PED_IN_ANY_VEHICLE(ped)
				PRINTLN("[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - Target is in Vehicle.")
				
				veh = GET_VEHICLE_PED_IS_IN(ped)
				
				PARTICIPANT_INDEX piPart
				PLAYER_INDEX piPlayer
				PED_INDEX pedPlayer
				
				// Stops the players from caching the same position.
				BOOL bSafe = TRUE
				INT iPartner = 0
				FOR iPartner = 0 TO MAX_VEHICLE_PARTNERS-1
					IF GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iVehiclePartners[iPartner] > -1
						piPart = INT_TO_PARTICIPANTINDEX(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iVehiclePartners[iPartner])
						
						IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
						AND GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iVehiclePartners[iPartner] > -1
						AND GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iVehiclePartners[iPartner] != iLocalPart
							piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
							pedPlayer = GET_PLAYER_PED(piPlayer)
							
							IF IS_PED_IN_ANY_VEHICLE_OR_WAITING_TO_START_TASK_ENTER_VEHICLE(pedPlayer)
							AND NOT IS_PED_IN_ANY_VEHICLE(pedPlayer, FALSE)
								bSafe = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				
				IF DOES_ENTITY_EXIST(veh)
				AND IS_VEHICLE_DRIVEABLE(veh)
					PRINTLN("[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - Vehicle Target: ", NATIVE_TO_INT(veh))
										
					IF NOT IS_PED_IN_ANY_VEHICLE_OR_WAITING_TO_START_TASK_ENTER_VEHICLE(localPlayerPed)
					AND bSafe
						// check player is near to vehicle
						VECTOR vPlayerCoords = GET_ENTITY_COORDS(localPlayerPed, FALSE)
						VECTOR vCarCoords = GET_ENTITY_COORDS(veh, FALSE)
						
						PRINTLN("[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE WarpPlayerIntoCar - vPlayerCoords = ") 
						PRINTLN(vPlayerCoords, ", vCarCoords = ", vCarCoords)
						
				 		IF NOT (VDIST(vPlayerCoords, vCarCoords) < 5.0)
							vCarCoords.z += -4.0

							SET_ENTITY_COORDS(localPlayerPed, vCarCoords, FALSE)
							PRINTLN("[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE WarpPlayerIntoCar - moving player closer to car.")
						ENDIF
												
						PRINTLN("[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - Calling to start task: Enter Vehicle")
						
						BOOL bShouldIgnoreTurretSeat = (GlobalplayerBD_FM[iLocalPart].sClientCoronaData.iPreferredRole = ciMISSION_CLIENT_SEAT_OPTION_PASSENGER_REGULAR)						
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(localPlayerPed)						
						SET_ENTITY_COLLISION(localPlayerPed, TRUE)	
						FREEZE_ENTITY_POSITION(localPlayerPed, FALSE)	
						SET_PED_CAN_RAGDOLL(localPlayerPed, FALSE)
						SET_PED_RESET_FLAG(localPlayerPed, PRF_SuppressInAirEvent, TRUE) // 3309718
						TASK_ENTER_VEHICLE(LocalPlayerPed, veh, 1, INT_TO_ENUM(VEHICLE_SEAT, GetFirstFreeSeatOfVehicle(veh, TRUE, bShouldIgnoreTurretSeat)), DEFAULT, ECF_WARP_PED)
						SET_PED_RESET_FLAG(localPlayerPed, PRF_SuppressInAirEvent, TRUE) // 31146107						
					ENDIF
				ELSE
					PRINTLN("[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - No Vehicle Target.")
				ENDIF
			ELSE
				PRINTLN("[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - Target Ped is not in vehicle yet.")
			ENDIF
		ELSE
			PRINTLN("[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - Target Ped is dead or Respawning.")
		ENDIF
	ELSE
		PRINTLN("[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - Something went wrong becase iDriverPart = ", iDriverPart)
	ENDIF

	IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
	AND iDriverPart > -1
		PRINTLN("[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - in vehicle:						returning TRUE")
		RETURN TRUE
	ELSE
		PRINTLN("[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - not in vehicle:					returning FALSE")
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL GET_FREE_TURRET_VEHICLE_SEAT(VEHICLE_INDEX VehicleID, VEHICLE_SEAT &vs_ToAssign)
	INT i
	IF DOES_ENTITY_EXIST(VehicleID)
	AND NOT IS_ENTITY_DEAD(VehicleID)
		REPEAT GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(VehicleID) i
			IF IS_VEHICLE_SEAT_FREE(VehicleID, INT_TO_ENUM(VEHICLE_SEAT, i))
				IF IS_TURRET_SEAT(VehicleID, INT_TO_ENUM(VEHICLE_SEAT, i))
					vs_ToAssign = INT_TO_ENUM(VEHICLE_SEAT, i)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHUFFLE_MULTIPLE_PLAYER_SAME_VEHICLE_DATA_CYCLE_SEATS()

	INT iPartOnTeam = iCachedPartOnTeamVehicleRespawn
	INT i
		
	PRINTLN("[LM][MULTI_VEH_RESPAWN][SHUFFLE_MULTIPLE_PLAYER_SAME_VEHICLE_DATA_CYCLE_SEATS][SWAP_SEAT] - iPartOnTeam Pointer = ", iPartOnTeam)
		
	iPartOnTeam+=1
		
	PRINTLN("[LM][MULTI_VEH_RESPAWN][SHUFFLE_MULTIPLE_PLAYER_SAME_VEHICLE_DATA_CYCLE_SEATS][SWAP_SEAT] - iPartOnTeam Incremented to = ", iPartOnTeam)

	// loop until we come back round or find a valid selection.
	FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
		IF iPartOnTeam >= MAX_VEHICLE_PARTNERS
			iPartOnTeam = 0
			
			PRINTLN("[LM][MULTI_VEH_RESPAWN][SHUFFLE_MULTIPLE_PLAYER_SAME_VEHICLE_DATA_CYCLE_SEATS][SWAP_SEAT] - (iPartOnTeam) Cylcing round to: ", iPartOnTeam, " because the index was greater than the MAX_VEHICLE_PARTNERS: ", MAX_VEHICLE_PARTNERS)
			
			BREAKLOOP
		ENDIF
		IF GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iVehiclePartners[iPartOnTeam] = -1
			iPartOnTeam+=1
		
			PRINTLN("[LM][MULTI_VEH_RESPAWN][SHUFFLE_MULTIPLE_PLAYER_SAME_VEHICLE_DATA_CYCLE_SEATS][SWAP_SEAT] - (Invalid Player) (iPartOnTeam) Incrementing to ", iPartOnTeam)
		ELSE 
			BREAKLOOP
		ENDIF
		IF iPartOnTeam >= MAX_VEHICLE_PARTNERS
			iPartOnTeam = 0
			
			PRINTLN("[LM][MULTI_VEH_RESPAWN][SHUFFLE_MULTIPLE_PLAYER_SAME_VEHICLE_DATA_CYCLE_SEATS][SWAP_SEAT] - (iPartOnTeam) Cylcing round to: ", iPartOnTeam, " because the index was greater than the MAX_VEHICLE_PARTNERS: ", MAX_VEHICLE_PARTNERS)
			
			BREAKLOOP
		ENDIF
	ENDFOR
	
	IF iPartOnTeam > -1 AND iPartOnTeam < MAX_VEHICLE_PARTNERS
		PRINTLN("[LM][MULTI_VEH_RESPAWN][SHUFFLE_MULTIPLE_PLAYER_SAME_VEHICLE_DATA_CYCLE_SEATS][SWAP_SEAT] - Old Seat INT: ", GlobalplayerBD[iLocalPart].iRespawnSeatPreference, " New Seat INT: ", GlobalplayerBD[GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iVehiclePartners[iPartOnTeam]].iRespawnSeatPreference)
				
		iCachedNewVehSeatPref = GlobalplayerBD[GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iVehiclePartners[iPartOnTeam]].iRespawnSeatPreference
		
		RETURN TRUE
	ELSE
		PRINTLN("[LM][MULTI_VEH_RESPAWN][SHUFFLE_MULTIPLE_PLAYER_SAME_VEHICLE_DATA_CYCLE_SEATS][SWAP_SEAT] - Cannot find passenger... emergency cleanup.")
		
		DO_SCREEN_FADE_IN(250)
		CLEANUP_MULTI_VEHICLE_SEAT_SWAP()
	ENDIF	
	
	RETURN FALSE	
ENDFUNC

FUNC BOOL IS_PART_IN_SAME_VEHICLE_AS_PART(INT iPart1, INT iPart2)
	
	IF iPart1 != -1
	AND iPart2 != -1
		PLAYER_INDEX player1 = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart1))
		PLAYER_INDEX player2 = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart2))
				
		IF IS_NET_PLAYER_OK(player1)
		AND IS_NET_PLAYER_OK(player2)
			PED_INDEX ped1 = GET_PLAYER_PED(player1)
			PED_INDEX ped2 = GET_PLAYER_PED(player2)
			
			IF NOT IS_PED_INJURED(ped1)
			AND NOT IS_PED_INJURED(ped2)
				VEHICLE_INDEX vehPlayer1
				IF IS_PED_IN_ANY_VEHICLE(ped1)
					vehPlayer1 = GET_VEHICLE_PED_IS_IN(ped1)
				ENDIF
				
				VEHICLE_INDEX vehPlayer2
				IF IS_PED_IN_ANY_VEHICLE(ped2)
					vehPlayer2 = GET_VEHICLE_PED_IS_IN(ped2)
				ENDIF
				
				IF vehPlayer1 != vehPlayer2
					RETURN FALSE
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	// True because even if the above stuff fails, we don't want to execute what this function will do if it returns false.
	RETURN TRUE
ENDFUNC

// This function gets the driver number for the player and team passed in (team part)
FUNC INT SET_DRIVER_AND_VEHICLE_PARTNERS_FROM_MY_PART_AND_TEAM_CORONA_PREFS(INT iPlayerTeam, INT iPartNumberInTeam)
	INT iPlayer
	INT iPlayerStartFrom = 0
	INT iDriverNumberIBelongTo = 0
	INT iDrivers[6]
	INT iDriverPartTeamNumber = -1
	
	// Init
	INT i, ii, iii
	FOR i = 0 TO 5
		iDrivers[i] = -1
	ENDFOR
	
	PLAYER_INDEX tempPlayer1
	PLAYER_INDEX tempPlayer2
	
	BOOL bDoneFindingPartners = FALSE
	
	INT iVehiclePartner = 0
	
	// Loop
	PRINTLN("[PLAYER_LOOP] - SET_DRIVER_AND_VEHICLE_PARTNERS_FROM_MY_PART_AND_TEAM_CORONA_PREFS 2")
	FOR i = 0 TO NUM_NETWORK_PLAYERS-1
		IF NOT bDoneFindingPartners
			
			// Initialize our saved drivers.
			FOR iii = 0 TO MAX_VEHICLE_PARTNERS-1
				SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iii, -1)
			ENDFOR
			iVehiclePartner = 0
			
			tempPlayer1 = INT_TO_PLAYERINDEX(i)
		
			IF IS_NET_PLAYER_OK(tempPlayer1, FALSE)
			AND NOT IS_PLAYER_SPECTATING(tempPlayer1)
			
				IF GlobalplayerBD_FM[i].sClientCoronaData.iTeamChosen = iPlayerTeam
					iDriverPartTeamNumber++
				
					// Found a participant. Increment our number.
					IF GlobalplayerBD_FM[i].sClientCoronaData.iPreferredRole = ciMISSION_CLIENT_SEAT_OPTION_DRIVER
					AND NOT IS_DRIVER_IN_LIST(iDrivers, i)
						iDrivers[iDriverNumberIBelongTo] = i
						
						PRINTLN("[LM][MULTI_VEH_RESPAWN][GET_DRIVER_PART_NUMBER_IN_TEAM_FROM_MY_PART_IN_TEAM_CORONA_PREF] Found a Driver iPart: ", iDrivers[iDriverNumberIBelongTo])
					ENDIF
					
					// We have a Driver Participant number on our team now, check which players are going to be in there.
					IF iDrivers[iDriverNumberIBelongTo] > -1			
						
						// Start from the player index we left off at. This is so that we don't look at players who belong to other drivers.
						PRINTLN("[PLAYER_LOOP] - SET_DRIVER_AND_VEHICLE_PARTNERS_FROM_MY_PART_AND_TEAM_CORONA_PREFS 1")
						FOR ii = iPlayerStartFrom TO NUM_NETWORK_PLAYERS-1
							tempPlayer2 = INT_TO_PLAYERINDEX(ii)							
							
							IF IS_NET_PLAYER_OK(tempPlayer2, FALSE)
							AND NOT IS_PLAYER_SPECTATING(tempPlayer2)
								
								IF GlobalplayerBD_FM[ii].sClientCoronaData.iTeamChosen = iPlayerTeam
									SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iVehiclePartner, ii)
									iVehiclePartner++
									
									// If we find our participant number in the loop, then we know we can use iDriverPartTeamNumber as our driver. We don't break here because we want to add the rest of our vehicle partners.
									IF iPlayer = iPartNumberInTeam
										bDoneFindingPartners = TRUE
									ENDIF
									
									// Increment our Participant Number in Team var.
									iPlayer++
									
									// If we exceed the number of players allowed in a particular vehicle then breakloop and move onto the next driver.
									IF iPlayer >= (g_FMMC_STRUCT.iTeamVehicleRespawnPassengers[iPlayerTeam]*(iDriverNumberIBelongTo+1))
										iDriverNumberIBelongTo++
										iPlayerStartFrom++
										BREAKLOOP
									ENDIF
								ENDIF
							ENDIF
							iPlayerStartFrom++
						ENDFOR
					ENDIF
				ENDIF
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDFOR
	
	PRINTLN("[LM][MULTI_VEH_RESPAWN][GET_DRIVER_PART_NUMBER_IN_TEAM_FROM_MY_PART_IN_TEAM_CORONA_PREF] (Team Participant number) Returning iDriverPartTeamNumber: ", iDriverPartTeamNumber, 
																									" (is this the first, or second driver etc) iDriverNumberIBelongTo: ", (iDriverNumberIBelongTo+1), // starts at 0 because of array.
																									" (How many players we looped through) iPlayer: " , iPlayer, 
																									" (Our Participant Number) iPartNumberInTeam: " , iPartNumberInTeam)
	
	RETURN iDriverPartTeamNumber
ENDFUNC

FUNC BOOL IS_OK_TO_MULTI_TEAM_VEHICLE_SEAT_SWAP()
	INT i = 0 
	PARTICIPANT_INDEX driverPart
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		IF GET_NUM_PEDS_IN_VEHICLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) < 2
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
	OR IS_CELLPHONE_CAMERA_IN_USE()
		RETURN FALSE
	ENDIF
	
	IF IS_INTERACTION_MENU_OPEN()
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_OUT_OF_BOUNDS()
	OR IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_OO_BOUNDS)
		PRINT_HELP("CONSET_NO_2", ci_VEH_SEAT_SWAP_INVALID_TIME)
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())))
		driverPart = INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()))
		
		IF driverPart != INVALID_PARTICIPANT_INDEX()
			IF IS_PED_CARRYING_ANY_OBJECTS(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(driverPart)))
				PRINT_HELP("CONSET_NO_1", ci_VEH_SEAT_SWAP_INVALID_TIME)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
		IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) > -1
		AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i)))
			IF IS_PART_IN_SAME_VEHICLE_AS_PART(iLocalPart, GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_IN_PROGRESS()
		
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_DONE)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_INITIATED_VEHICLE_SEAT_SWAP)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_FINAL)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_PAST_INITIATED()
		
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_DONE)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_FINAL)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH()
	
	PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - Calling to re-cache our current vehicle seat.")
	
	IF bLocalPlayerPedOK
		IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
			
			VEHICLE_SEAT vehSeat = GET_SEAT_PED_IS_IN(localPlayerPed, TRUE)
			VEHICLE_SEAT vehSeatToCheck
			PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - Current Vehicle Seat: ", GET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE(), " changing to actual seat: ", ENUM_TO_INT(vehSeat))
				
			PARTICIPANT_INDEX piPart
			PLAYER_INDEX piPlayer
			PED_INDEX pedPlayer
			
			INT iDriver = -1
			INT i = 0			
			FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
				IF GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iVehiclePartners[i] > -1
					PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - Valid player: ", i)
					
					piPart = INT_TO_PARTICIPANTINDEX(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iVehiclePartners[i])
					piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
					pedPlayer = GET_PLAYER_PED(piPlayer)
					
					IF NOT IS_PED_INJURED(pedPlayer)
						vehSeatToCheck = GET_SEAT_PED_IS_IN(pedPlayer, TRUE)
						
						PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - Player: ", GET_PLAYER_NAME(piPlayer), " is in seat: ", ENUM_TO_INT(vehSeatToCheck))
						
						IF vehSeatToCheck = VS_DRIVER
							PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - Found our driver in Partner slot: ", i)
							iDriver = i
							BREAKLOOP
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
			IF iDriver > -1
				PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - iDriver is in Partner Slot: ", iDriver, " and is true participant number: ", GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iVehiclePartners[iDriver])
			ENDIF
			
			IF vehSeat = VS_DRIVER
				PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - We are now cached as a Driver")
				SET_RACE_PASSENGER_GLOBAL(FALSE)
				SET_RACE_DRIVER_GLOBAL(TRUE)
			ELSE
				PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - We are now cached as a Passenger")
				SET_RACE_PASSENGER_GLOBAL(TRUE)
				SET_RACE_DRIVER_GLOBAL(FALSE)
			ENDIF
			
			IF iDriver > -1
				SET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER(iDriver)
			ELSE
				PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - iDriver IS -1, something funky happen!")
			ENDIF
			
			SET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE(ENUM_TO_INT(vehSeat))
		ELSE
			PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - Cannot re-cache, we are not in a vehicle.")
		ENDIF
	ELSE
		PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - Cannot re-cache, we are dead.")
	ENDIF
ENDPROC

PROC CACHE_AND_SETUP_VEHICLE_PARTNERS()

	PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - Calling to initialise the data.")
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ iLocalPart ].iteam].iTeamBitSet3, ciBS3_ENABLE_STOP_PLAYER_FROM_SHUFFLING_SEATS)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat, TRUE)
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ iLocalPart ].iteam].iTeamBitSet3, ciBS3_ENABLE_CHOOSE_SEAT_PREFERENCE_IN_CORONA)
		// Default (No corona settings): 
		INT iPlayerTeam = MC_playerBD[ iLocalPart ].iteam
		INT iPartNumberInTeam = GET_PARTICIPANT_NUMBER_IN_TEAM_PRIVATE()	
		INT iDriverPartTeam = GET_DRIVER_PART_NUMBER_IN_TEAM_FROM_MY_PART_IN_TEAM(iPlayerTeam, iPartNumberInTeam)
		INT iDriverPart = GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER(iPlayerTeam, iDriverPartTeam)
				
		PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - iDriver:  ", iDriverPart)
		
		// The driver may need to be mindful of all his passengers. We will know how many he is supposed to have based on a creator setting.
		IF iDriverPart = iLocalPart
			PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Driver) I am a Driver. Adding Driver Part: ", iDriverPart, " iDriverPartTeam: " ,iDriverPartTeam)
			
			SET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER(0)
			
			INT i = 0
			FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
				IF i+iDriverPartTeam < NUM_NETWORK_PLAYERS
					IF i < g_FMMC_STRUCT.iTeamVehicleRespawnPassengers[iPlayerTeam]
						SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i, GET_PASSENGER_PARTICIPANT_NUMBER_FROM_MY_PARTICIPANT_NUMBER(i+iDriverPartTeam))
						PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Driver) Adding :  ", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i), " to my Vehicle Partners List.")
					ELSE
						SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i, -1)
						PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Driver) Adding :  -1 (Forced (1)) - No more passengers")
					ENDIF
				ELSE
					SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i, -1)
					PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Driver) Adding :  -1 (Forced (2))")
				ENDIF
				
				IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) = iLocalPart
					iCachedPartOnTeamVehicleRespawn = i
					PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - iCachedPartOnTeamVehicleRespawn:  ", iCachedPartOnTeamVehicleRespawn)
				ENDIF
			ENDFOR
			
			SET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE(ENUM_TO_INT(VS_DRIVER))
			SET_RACE_DRIVER_GLOBAL(TRUE)
		ELSE
			PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Passenger) I am a passenger. Adding Driver Part: ", iDriverPart, " iDriverPartTeam: " ,iDriverPartTeam)
			
			SET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER(0)

			INT i = 0
			FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
				IF i+iDriverPartTeam < NUM_NETWORK_PLAYERS
					IF i < g_FMMC_STRUCT.iTeamVehicleRespawnPassengers[iPlayerTeam]
						SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i, GET_PASSENGER_PARTICIPANT_NUMBER_FROM_MY_PARTICIPANT_NUMBER(i+iDriverPartTeam))
						PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Passenger) Adding :  ", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i), " to my Vehicle Partners List.")
					ELSE
						SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i, -1)
						PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Passenger) Adding :  -1 (Forced (1)) - No more passengers")
					ENDIF
				ELSE
					SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i, -1)
					PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Passenger) Adding :  -1 (Forced (2))")
				ENDIF
				
				IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) = NATIVE_TO_INT(LocalPlayer)
					iCachedPartOnTeamVehicleRespawn = i
					PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - iCachedPartOnTeamVehicleRespawn:  ", iCachedPartOnTeamVehicleRespawn)
				ENDIF
			ENDFOR
			
			SET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE(ENUM_TO_INT(VS_ANY_PASSENGER))
			
			SET_RACE_PASSENGER_GLOBAL(TRUE)
		ENDIF
	ELSE
		// Default (No corona settings): 
		INT iPlayerTeam = MC_playerBD[ iLocalPart ].iteam
		INT iPartNumberInTeam = GET_PARTICIPANT_NUMBER_IN_TEAM_PRIVATE()
		
		// Populating the Vehicle Partners is done in this function here;
		SET_DRIVER_AND_VEHICLE_PARTNERS_FROM_MY_PART_AND_TEAM_CORONA_PREFS(iPlayerTeam, iPartNumberInTeam)
		
		INT i = 0		
		FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
			IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) > -1
				IF GlobalplayerBD_FM[GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i)].sClientCoronaData.iPreferredRole = ciMISSION_CLIENT_SEAT_OPTION_DRIVER
					SET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER(i)
				ENDIF
			ENDIF
			
			IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) = NATIVE_TO_INT(LocalPlayer)
				iCachedPartOnTeamVehicleRespawn = i
			ENDIF
		ENDFOR
				
		IF GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iPreferredRole = ciMISSION_CLIENT_SEAT_OPTION_DRIVER
			SET_RACE_DRIVER_GLOBAL(TRUE)
			SET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE(ENUM_TO_INT(VS_DRIVER))
			
			PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Corona) iPreferredRole: ciMISSION_CLIENT_SEAT_OPTION_DRIVER")
		ELSE
			IF GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iPreferredRole = ciMISSION_CLIENT_SEAT_OPTION_PASSENGER_TURRET
				SET_RACE_PASSENGER_GLOBAL(TRUE)								
				SET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE(ENUM_TO_INT(VS_ANY_PASSENGER))
				
				PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Corona) iPreferredRole: ciMISSION_CLIENT_SEAT_OPTION_PASSENGER_TURRET")
			ELIF GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iPreferredRole = ciMISSION_CLIENT_SEAT_OPTION_PASSENGER_REGULAR
				SET_RACE_PASSENGER_GLOBAL(TRUE)
				SET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE(ENUM_TO_INT(VS_ANY_PASSENGER))
				
				PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Corona) iPreferredRole: ciMISSION_CLIENT_SEAT_OPTION_PASSENGER_REGULAR")
			ENDIF
		ENDIF	
	ENDIF
	
	// ############################################## CONTINGENCIES ##############################################
	// We have no driver, and no vehicle partners promote ourself.
	BOOL bDriverPromoteContingency = TRUE
	BOOL bDriverPromoteContingencyB = FALSE
	
	INT i = 0		
	FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
		IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) > -1
		AND GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) != iLocalPart
			bDriverPromoteContingency = FALSE
		ENDIF
	ENDFOR
	
	IF bDriverPromoteContingency
		PRINTLN("[LM][MULTI_VEH_RESPAWN][CACHE_AND_SETUP_VEHICLE_PARTNERS] - Setting bDriverPromoteContingencyA true (1)")
	ENDIF
	
	IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) = -1
		bDriverPromoteContingencyB = TRUE
		PRINTLN("[LM][MULTI_VEH_RESPAWN][CACHE_AND_SETUP_VEHICLE_PARTNERS] - Setting bDriverPromoteContingencyB true (1)")
	ELSE
		IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) > -1
		AND iLocalPart = GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())
		
			INT iPlayer = GET_PLAYER_INDEX_NUMBER_FROM_PARTICIPANT_INDEX_NUMBER(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()))
			IF GlobalplayerBD_FM[iPlayer].sClientCoronaData.iPreferredRole != ciMISSION_CLIENT_SEAT_OPTION_DRIVER
				bDriverPromoteContingencyB = TRUE	
				PRINTLN("[LM][MULTI_VEH_RESPAWN][CACHE_AND_SETUP_VEHICLE_PARTNERS] - Setting bDriverPromoteContingencyB true (2)")
			ENDIF
		ENDIF
	ENDIF
	
	IF bDriverPromoteContingency
		PRINTLN("[LM][MULTI_VEH_RESPAWN][CACHE_AND_SETUP_VEHICLE_PARTNERS] - INITIATING CONTINGENCY A - Player has no vehicle partners and no driver.")
		
		GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iPreferredRole = ciMISSION_CLIENT_SEAT_OPTION_DRIVER
		SET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER(0)
		SET_RACE_DRIVER_GLOBAL(TRUE)
		SET_RACE_PASSENGER_GLOBAL(FALSE)
		
		SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(0, iLocalPart)
	ELIF bDriverPromoteContingencyB
		PRINTLN("[LM][MULTI_VEH_RESPAWN][CACHE_AND_SETUP_VEHICLE_PARTNERS] - INITIATING CONTINGENCY B - Player has vehicle partners but no driver.")
		
		INT iOldDriverPointer = GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()
		INT iNewDriverPointer = (iOldDriverPointer+1)
		
		PRINTLN("[LM][MULTI_VEH_RESPAWN][CACHE_AND_SETUP_VEHICLE_PARTNERS] - DriverPointer is old and is pointing to an invalid participant. Reassigning iOldDriverPointer: ", iOldDriverPointer)			
		PRINTLN("[LM][MULTI_VEH_RESPAWN][CACHE_AND_SETUP_VEHICLE_PARTNERS] - iNewDriverPointer: ", iNewDriverPointer, " PartNum: ", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iNewDriverPointer))
		
		SET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER(iNewDriverPointer)
		
		// If we are the new Driver then we need to call some stuff...
		IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) = iLocalPart
			GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iPreferredRole = ciMISSION_CLIENT_SEAT_OPTION_DRIVER
			SET_RACE_PASSENGER_GLOBAL(FALSE)
			SET_RACE_DRIVER_GLOBAL(TRUE)
			SET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE(ENUM_TO_INT(VS_DRIVER))
			PRINTLN("[LM][MULTI_VEH_RESPAWN][CACHE_AND_SETUP_VEHICLE_PARTNERS] - I am now promoted as the new Driver")
		ENDIF
	ENDIF
	
	SET_BIT(iLocalBoolCheck25, LBOOL25_INITIALIZED_SEATING_DATA_FOR_MULTI_VEH)
	
ENDPROC

FUNC BOOL IS_PARTICIPANT_DRIVING_SEAT_SWITCH_VEHICLE(PARTICIPANT_INDEX partToCheck)
	IF NETWORK_IS_PARTICIPANT_ACTIVE(partToCheck)
		IF partToCheck = INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()))
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Player State Setup 																						
// ##### Description: Functions which modify the attributes of the player or the way the world behaves around them. Player Proofs, Config Flags, Etc..
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SETUP_REBREATHER_FOR_RESPAWN()
	PRINTLN("[RCC MISSION] SETUP_REBREATHER_FOR_RESPAWN Called. ", IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_NON_HEIST_REBREATHERS), ", ", IS_MP_HEIST_GEAR_EQUIPPED(LocalPlayerPed, HEIST_GEAR_REBREATHER))
	DEBUG_PRINTCALLSTACK()

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_NON_HEIST_REBREATHERS)
		IF IS_MP_HEIST_GEAR_EQUIPPED(LocalPlayerPed, HEIST_GEAR_REBREATHER)
			INT iRebreatherCount = GET_MP_INT_CHARACTER_STAT(MP_STAT_BREATHING_APPAR_BOUGHT)
			
			PRINTLN("[RCC MISSION] SETUP_REBREATHER_FOR_RESPAWN - Respawned with a rebreather on. Canister count is ", iRebreatherCount)
			
			IF iRebreatherCount > 1
			
				PRINTLN("[RCC MISSION] SETUP_REBREATHER_FOR_RESPAWN -  Setting to not drown in water.")
			
				SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DO_GEAR_BERD_CHECK)
				
				SET_PLAYER_UNDERWATER_BREATH_PERCENT_REMAINING(LocalPlayer, 100.0)
				SET_PED_DIES_IN_WATER(LocalPlayerPed, FALSE)
				SET_PED_DIES_IN_SINKING_VEHICLE(LocalPlayerPed, FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_REBREATHER_ON_RULE(INT iTeam, INT iRule)
	IF IS_MP_HEIST_GEAR_EQUIPPED(LocalPlayerPed, HEIST_GEAR_REBREATHER)
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_FORCE_REMOVE_REBREATHER)
		REMOVE_MP_HEIST_GEAR(LocalPlayerPed, HEIST_GEAR_REBREATHER)
		PREVENT_GANG_OUTFIT_CHANGING(FALSE)
	ENDIF
ENDPROC

FUNC INT DROP_MONEY_GET_MINIMUM_BAG_VALUE()
	RETURN ((MC_playerBD[iLocalPart].sLootBag.iTotalCashGrabbed/100) * g_sMPTunables.iIsland_Heist_Minimum_Bag_Capacity_Percentage)
ENDFUNC

FUNC BOOL DROP_MONEY_IS_CASH_REMAINING_BELOW_MINIMUM(INT iCashToRemove)
	INT iCashMin = DROP_MONEY_GET_MINIMUM_BAG_VALUE()
	RETURN (MC_playerBD[iLocalPart].sLootBag.iCashGrabbed - iCashToRemove) < iCashMin
ENDFUNC

FUNC INT DROP_MONEY_GET_CASH_TO_REMOVE(INT iCash)
	RETURN ((iCash/100) * g_sMPTunables.iIsland_Heist_Respawn_Bag_Loss_Percentage)
ENDFUNC

PROC DROP_MONEY_ON_RESPAWN()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_UseIndividualBagValues)
		EXIT
	ENDIF
		
	INT iCashToRemove = DROP_MONEY_GET_CASH_TO_REMOVE(MC_playerBD[iLocalPart].sLootBag.iTotalCashGrabbed)
	
	IF DROP_MONEY_IS_CASH_REMAINING_BELOW_MINIMUM(iCashToRemove)
		iCashToRemove = DROP_MONEY_GET_MINIMUM_BAG_VALUE() - MC_playerBD[iLocalPart].sLootBag.iCashGrabbed
	ENDIF
	
	MC_playerBD[iLocalPart].sLootBag.iCashGrabbed -= iCashToRemove
	sMissionLocalContinuityVars.sLootBag.iCashGrabbed = MC_playerBD[iLocalPart].sLootBag.iCashGrabbed
	
	PRINTLN("[CashGrab][DropMoney] DROP_MONEY_ON_RESPAWN - Removed: ", iCashToRemove, " MC_playerBD[iLocalPart].sLootBag.iCashGrabbed: ", MC_playerBD[iLocalPart].sLootBag.iCashGrabbed)
	
	IF iCashToRemove > 0
		BROADCAST_FMMC_REMOVE_GRABBED_CASH(iCashToRemove, 2000)
	ENDIF
	
ENDPROC

PROC REMOVE_BAG_CONTENTS_ON_RESPAWN()
	
	DROP_MONEY_ON_RESPAWN()
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Team / Personal Vehicle Processing 
// ##### Description: This section covers the processing of non-creator vehicles associated with the local player.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_ACTUAL_DEATH_RESPAWN_RADIO()
	IF IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_KEEP_RADIO_STATION_ON_RESPAWN)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SHOULD_TURN_RADIO_BACK_ON_AFTER_ROUND_RESTART)
	
		//Cache the current radio station (ACTUAL RESPAWN)
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_KEEP_RADIO_STATION_ON_RESPAWN)
			IF NOT g_bMissionEnding
				IF IS_SCREEN_FADED_IN()
					IF bPlayerToUseOK
						IF bLocalPlayerPedOK
						AND NOT IS_PED_DEAD_OR_DYING(localPlayerPed)
							IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
								IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
								
									INT iRadioStationIndex = 255
									
									IF DOES_PLAYER_VEH_HAVE_RADIO()
									AND IS_PLAYER_VEH_RADIO_ENABLE()
										iRadioStationIndex = GET_PLAYER_RADIO_STATION_INDEX()
										SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
									ENDIF
									
									IF g_SpawnData.iRadioStation != iRadioStationIndex
										g_SpawnData.bRadioStationOverScore = TRUE						
										g_SpawnData.iRadioStation = iRadioStationIndex
										PRINTLN("[RADIO] Mission - Storing Radio Station ... g_SpawnData.iRadioStation = ", iRadioStationIndex)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			g_SpawnData.bRadioStationOverScore = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC SET_VEHICLE_HEALTH_FROM_RULE_AND_TEAM(INT iTeam, INT iRule, VEHICLE_INDEX veh)
	INT iVehHealth
	iVehHealth = GET_FMMC_PLAYER_VEHICLE_HEALTH_FOR_TEAM_AND_RULE(iTeam, iRule)
	SET_VEHICLE_FIXED(veh)
	SET_ENTITY_HEALTH(veh, iVehHealth)
	SET_VEHICLE_WEAPON_DAMAGE_SCALE(veh, GET_FMMC_PLAYER_VEHICLE_WEAPON_DAMAGE_SCALE_FOR_TEAM(iTeam))
	SET_VEHICLE_ENGINE_HEALTH(veh, iVehHealth * 1.0)
	SET_VEHICLE_PETROL_TANK_HEALTH(veh, iVehHealth * 1.0)
	
	IF iVehHealth >= 3000 
		SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(veh,FALSE)
	ENDIF
ENDPROC

PROC SET_VEHICLE_WEAPON_MODS_FROM_RULE_AND_TEAM(INT iTeam, INT iRule, VEHICLE_INDEX veh)
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_VEHICLE_SWAP_MODS)
		INT iVeh = -1
		INT iVModTurret = MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleTurretSwap
		INT iVModArmour = MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleArmourSwap
		INT iVModFrontBumper = MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleAirCounterSwap
		INT iVModExhaust = MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleExhaustSwap
		INT iVModBombbay = MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleBombBaySwap
		INT iVModSpoiler = -1
		INT iVehBitset = 0
		BOOL bForceUseBombBayMod = FALSE
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_VEHICLE_SWAP_BOMB_BAY)
			bForceUseBombBayMod = TRUE
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_VEHICLE_SWAP_USE_REAR_WEAPON_MOD)
			SET_BIT(iVehBitset, ciFMMC_VEHICLE5_USE_REAR_WEAPON_MOD)
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_VEHICLE_SWAP_USE_FRONT_WEAPON_MOD)
			SET_BIT(iVehBitset, ciFMMC_VEHICLE5_USE_FRONT_WEAPON_MOD)
		ENDIF
		PRINTLN("[VehSwap][SET_VEHICLE_WEAPON_MODS_FROM_RULE_AND_TEAM] iVModTurret: ", iVModTurret,
			" iVModArmour: ", iVModArmour, " iVehBitset: ", iVehBitset, " iVModFrontBumper: ", iVModFrontBumper, " iVModExhaust: ", iVModExhaust,
			" iVModBombbay: ", iVModBombbay, " iVModSpoiler: ", iVModSpoiler, " bForceUseBombBayMod: ", bForceUseBombBayMod)
		SET_VEHICLE_WEAPON_MODS(veh, iVModTurret, iVModArmour, iVehBitset, iVeh, iVModFrontBumper, iVModExhaust,
			iVModBombbay, iVModSpoiler, iTeam, bForceUseBombBayMod)
	ENDIF
ENDPROC

///PURPOSE: This function will unfreeze the plane/heli the player is spawned in
///    so long as it is airborne, then given them a boost forwards
PROC UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS( FLOAT fSpeed )

	IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_FORCED_MISSION_START_MOVING_FORWARD)
		EXIT
	ENDIF
	
	VEHICLE_INDEX vehPlayer
	
	IF IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
	OR NETWORK_DOES_NETWORK_ID_EXIST(netRespawnVehicle)
		
		IF IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
			vehPlayer = GET_VEHICLE_PED_IS_IN( LocalPlayerPed )
		ELSE
			PRINTLN("[RCC MISSION] UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS - player isn't in a vehicle so use netRespawnVehicle! LocalPlayerPed = ",NATIVE_TO_INT(LocalPlayerPed),", player ped = ",NATIVE_TO_INT(LocalPlayerPed))
			vehPlayer = NET_TO_VEH(netRespawnVehicle)
		ENDIF
		
		IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
			PRINTLN("[RCC MISSION] UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS - unfrozen player spawn vehicle.")
			FREEZE_ENTITY_POSITION(vehPlayer, FALSE)
			
			IF IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAERIAL_VEHICLE_SPAWN )
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_RESPAWN_HYDRAS_IN_FLIGHT_MODE)
				
				IF (IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehPlayer))
				OR IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer)) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_RESPAWN_HYDRAS_IN_FLIGHT_MODE))
				AND IS_ENTITY_ALIVE(vehPlayer)
					SET_VEHICLE_FORWARD_SPEED(vehPlayer, fSpeed)
					IF GET_VEHICLE_HAS_LANDING_GEAR(vehPlayer)
						CONTROL_LANDING_GEAR(vehPlayer, LGC_RETRACT_INSTANT)
					ENDIF
					PRINTLN("[RCC MISSION] UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS - Setting vehicle speed to ", fSpeed)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_RESPAWN_HYDRAS_IN_FLIGHT_MODE)
			AND GET_ENTITY_MODEL(vehPlayer) = HYDRA
				SET_VEHICLE_FLIGHT_NOZZLE_POSITION_IMMEDIATE(vehPlayer, 0.0)
				PRINTLN("[RCC MISSION] UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS - Calling SET_VEHICLE_FLIGHT_NOZZLE_POSITION_IMMEDIATE with 0.0 on our Hydra.")
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			INT iveh = IS_VEH_A_MISSION_CREATOR_VEH(vehPlayer)
			
			IF iveh != -1
				PRINTLN("[RCC MISSION] UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS - We're starting in vehicle ",iveh,", we don't have control so we can't unfreeze it but it will be unfrozen later in SET_ALL_MISSION_ENTITIES_UNFROZEN_AND_ABLE_TO_MIGRATE")
			ELSE
				PRINTLN("[RCC MISSION] UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS - Starting in a non-MC vehicle, but we don't have control over it??")
			ENDIF
		#ENDIF
		ENDIF
		
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[RCC MISSION] UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS - player isn't in a vehicle, and netRespawnVehicle doesn't exist! LocalPlayerPed = ",NATIVE_TO_INT(LocalPlayerPed),", player ped = ",NATIVE_TO_INT(LocalPlayerPed))
	#ENDIF
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Spawning / Respawning / Restarting Wrappers and helper functions.																						
// ##### Description: Wrappers and Helper Functions that are used in various places of spawning/respawning.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC CLEAR_PLAYER_IN_POSSESSION_OF_MISSION_CRITICAL_ENTITY()
	IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
		SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(FALSE)
		CPRINTLN(DEBUG_MISSION, "[RCC MISSION] Clearing SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY as the local player is dead" )
	ENDIF
ENDPROC

FUNC BOOL IS_CARGOBOB_MANUAL_RESPAWN_OVERRIDE()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	IF iTeam < FMMC_MAX_TEAMS
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		IF iRule < FMMC_MAX_RULES
			RETURN (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCBManRespawnExclusionRadius[iRule] != 0
				AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][0].iSpawnBitSet, ci_SpawnBS_AirSpawn))
		ENDIF
		RETURN FALSE
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL SHOULD_PROCESS_ENTER_EOM_AMBIENT_VEHICLE_CHECKS()
	
	#IF IS_DEBUG_BUILD
		IF NOT IS_THIS_A_CONTACT_MISSION()
			NET_SCRIPT_ASSERT("SHOULD_PROCESS_ENTER_EOM_AMBIENT_VEHICLE_CHECKS being called while not on a contact mission.")
		ENDIF
	#ENDIF
	
	IF g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bIAmDriverForContactMission
		IF NOT g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bFinishedInPersonalVehicle
		AND NOT g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bFinishedInSomeoneElsesPersonalVehicle
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC VECTOR GET_TEAM_SLOT_QUICK_RESTART_START_POS(INT iPlayerTeam, INT iTeamSlot)
	
	IF FMMC_CHECKPOINTS_IS_ANY_RETRY_START_POINT_SET()
		
		INT iCheckpoint = MC_GET_MISSION_STARTING_CHECKPOINT()
		
		PRINTLN("[RCC MISSION] GET_TEAM_SLOT_QUICK_RESTART_START_POS - Using iCheckpoint: ", iCheckpoint)
		
		RETURN MC_GET_TEAM_CHECKPOINT_QUICK_RESTART_START_POS(iPlayerTeam, iTeamSlot, iCheckpoint, MC_serverBD_4.rsgSpawnSeed)
		
	ELSE
		RETURN g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[ iPlayerTeam ][ iTeamSlot ].vPos
	ENDIF
	
ENDFUNC

FUNC FLOAT GET_TEAM_CHECKPOINT_QUICK_RESTART_HEADING(INT iPlayerTeam, INT iTeamSlot, INT iCheckpoint)
	
	IF g_FMMC_STRUCT.iNumberOfQRCSpawnPoints > 0
		INT iQRCSpawnPoint = MC_GET_QRC_SPAWN_POINT_FOR_PLAYER(iPlayerTeam, iTeamSlot, iCheckpoint)
		
		IF iQRCSpawnPoint != -1
			RETURN g_FMMC_STRUCT.sQRCSpawnPoint[iQRCSpawnPoint].fSpawnHeading
		ENDIF
	ENDIF
	
	RETURN g_FMMC_STRUCT.fTeamRestartHeading[iPlayerTeam][iCheckpoint][iTeamSlot]
	
ENDFUNC

FUNC FLOAT GET_TEAM_SLOT_QUICK_RESTART_HEADING(INT iPlayerTeam, INT iTeamSlot)
	
	IF FMMC_CHECKPOINTS_IS_ANY_RETRY_START_POINT_SET()
	
		INT iCheckpoint = MC_GET_MISSION_STARTING_CHECKPOINT()
		
		RETURN GET_TEAM_CHECKPOINT_QUICK_RESTART_HEADING(iPlayerTeam, iTeamSlot, iCheckpoint)
	
	ELSE
		RETURN g_FMMC_STRUCT.sFMMCEndConditions[ iPlayerTeam ].fStartHeading
	ENDIF
	
ENDFUNC

FUNC BOOL IS_TEAM_SLOT_QUICK_RESTART_START_POS_SAFE(INT iPlayerTeam, INT iTeamSlot)

	IF IS_VECTOR_ZERO(GET_TEAM_SLOT_QUICK_RESTART_START_POS(iPlayerTeam, iTeamSlot))
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC VECTOR GET_TEAM_CHECKPOINT_QUICK_RESTART_FACING_DIRECTION(INT iPlayerTeam, INT iTeamSlot, INT iCheckpoint)
	
	IF g_FMMC_STRUCT.iNumberOfQRCSpawnPoints > 0
		INT iQRCSpawnPoint = MC_GET_QRC_SPAWN_POINT_FOR_PLAYER(iPlayerTeam, iTeamSlot, iCheckpoint)
		
		IF iQRCSpawnPoint != -1
			
			VECTOR vSpawnPos = g_FMMC_STRUCT.sQRCSpawnPoint[iQRCSpawnPoint].vSpawnPos
			FLOAT fHeading = g_FMMC_STRUCT.sQRCSpawnPoint[iQRCSpawnPoint].fSpawnHeading
			
			VECTOR vDirectionVector = GET_DIRECTIONAL_VECTOR_FROM_ROTATION(<<0, 0, fHeading>>)
			vDirectionVector = NORMALISE_VECTOR(vDirectionVector)
			vDirectionVector = vDirectionVector * <<5,5,5>>
			
			RETURN (vSpawnPos + vDirectionVector)
		ENDIF
	ENDIF
	
	RETURN g_FMMC_STRUCT.vTeamRestartFacing[iPlayerTeam][iCheckpoint][iTeamSlot]
	
ENDFUNC

FUNC VECTOR GET_SPAWN_FACING_DIRECTION(INT iPlayerTeam, INT iTeamSlot)
	
	IF FMMC_CHECKPOINTS_IS_ANY_RETRY_START_POINT_SET()
		
		INT iCheckpoint = MC_GET_MISSION_STARTING_CHECKPOINT()
		
		RETURN GET_TEAM_CHECKPOINT_QUICK_RESTART_FACING_DIRECTION(iPlayerTeam, iTeamSlot, iCheckpoint)
	ELSE
		RETURN <<0, 0, 0>>
	ENDIF
	
ENDFUNC

FUNC INT GetMyPVPassengerNumber(INT iOwnerPart, INT iHash)
	INT iCount
	INT iPart
	PRINTLN("[PLAYER_LOOP] - GetMyPVPassengerNumber	")
	REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iPart
		IF (MC_playerBD[iPart].iRestartInPV = iHash)
		AND NOT (iOwnerPart = iPart)
			IF (iPart = iLocalPart)
				RETURN iCount
			ELSE
				iCount++
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC

FUNC BOOL GET_PLACED_VEH_FOR_QUICK_RESTART_SPAWN()
	INT i, iCarsChecked
	INT iPartOnTeam = GET_PARTICIPANT_NUMBER_IN_TEAM()
	BOOL bCheckTeams
	FOR i = 0 TO FMMC_MAX_VEHICLES - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iTeamSpawnReq != -1
			bCheckTeams = TRUE
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetFive, ciFMMC_VEHICLE5_QUICK_RESTART_VEHICLE_TEAM_0 + MC_playerBD[iLocalPart].iteam)
		AND MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_VEHICLE(i, bCheckTeams)	
			IF iCarsChecked = iPartOnTeam
				iVehToSpawnIn = i
				RETURN TRUE
			ENDIF
			iCarsChecked++
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC SET_RESTART_VEH_STAGE(SPAWN_IN_PLACED_VEH_STAGE eNewStage)
	PRINTLN("[JS] [RESTARTVEH] - SET_RESTART_VEH_STAGE - moving from ", GET_RESTART_VEH_STAGE_STRING(eRestartVehStage), " to ", GET_RESTART_VEH_STAGE_STRING(eNewStage))
	eRestartVehStage = eNewStage
ENDPROC

FUNC INT GetFirstFreeSeatOfVehicleData(INT iVeh, BOOL bTurretSecondPriority = FALSE, BOOL bIgnoreTurretSeat = FALSE, BOOL bIgnoreDriver = FALSE)
	
	VEHICLE_INDEX VehicleID
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
		VehicleID = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
					
		IF DOES_ENTITY_EXIST(VehicleID)
		AND NOT IS_ENTITY_DEAD(VehicleID)
		AND IS_VEHICLE_DRIVEABLE(VehicleID)
			INT i
			INT iCachedSeat = -3
			IF bTurretSecondPriority
			
				FOR i = -1 TO GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(VehicleID)-1
					
					BOOL bSeatOccupied = FALSE
					INT iPart = 0	
					PRINTLN("[PLAYER_LOOP] - GetFirstFreeSeatOfVehicleData 2")
					FOR iPart = 0 TO NUM_NETWORK_PLAYERS-1
						IF MC_PlayerBD[iPart].iteam = MC_PlayerBD[iLocalPart].iteam
						AND MC_PlayerBD[iPart].iVehicleCurrentForRestart = iVeh
							IF i = MC_PlayerBD[iPart].iVehicleCurrentSeatForRestart
								bSeatOccupied = TRUE
								PRINTLN("[LM][RCC MISSION][GetFirstFreeSeatOfVehicleData] (1) - iVeh: ", iVeh, " Seat: ", i, " is occupied by player: ", iPart)
							ENDIF
						ENDIF
					ENDFOR
					
					IF NOT bSeatOccupied
					AND (INT_TO_ENUM(VEHICLE_SEAT, i) != VS_DRIVER OR NOT bIgnoreDriver)
						IF IS_TURRET_SEAT(VehicleID, INT_TO_ENUM(VEHICLE_SEAT, i))
						AND NOT bIgnoreTurretSeat
							RETURN i
						ENDIF
						
						IF iCachedSeat = -3
							iCachedSeat = i
						ENDIF
					ENDIF
				ENDFOR
				
				RETURN iCachedSeat
			ELSE
				FOR i = -1 TO GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(VehicleID)-1
					BOOL bSeatOccupied = FALSE
					INT iPart = 0	
					PRINTLN("[PLAYER_LOOP] - GetFirstFreeSeatOfVehicleData")
					FOR iPart = 0 TO NUM_NETWORK_PLAYERS-1
						IF MC_PlayerBD[iPart].iteam = MC_PlayerBD[iLocalPart].iteam
						AND MC_PlayerBD[iPart].iVehicleCurrentForRestart = iVeh
							IF i = MC_PlayerBD[iPart].iVehicleCurrentSeatForRestart
								bSeatOccupied = TRUE
								PRINTLN("[LM][RCC MISSION][GetFirstFreeSeatOfVehicleData] (2) - iVeh: ", iVeh, " Seat: ", i, " is occupied by player: ", iPart)
							ENDIF
						ENDIF
					ENDFOR
				
					IF NOT bSeatOccupied
					AND (INT_TO_ENUM(VEHICLE_SEAT, i) != VS_DRIVER OR NOT bIgnoreDriver)
						RETURN i
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
	
	RETURN -3
ENDFUNC

PROC SET_PLAYER_INTO_THIS_NEAREST_PLACED_VEHICLE(VEHICLE_INDEX tempVeh, INT iVehicleSeat)
	SCRIPTTASKSTATUS TaskStatus = GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_ENTER_VEHICLE)
	IF (TaskStatus = FINISHED_TASK)
		CLEAR_PED_TASKS_IMMEDIATELY(localPlayerPed)
		SET_PED_CONFIG_FLAG(localPlayerPed, PCF_PreventAutoShuffleToDriversSeat, TRUE)
		SET_PED_CONFIG_FLAG(localPlayerPed, PCF_PreventAutoShuffleToTurretSeat, TRUE)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_AllowAutoShuffleToDriversSeat, FALSE)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(localPlayerPed)
		SET_ENTITY_COLLISION(localPlayerPed, TRUE)	
		FREEZE_ENTITY_POSITION(localPlayerPed, FALSE)	
		SET_PED_CAN_RAGDOLL(localPlayerPed, FALSE)
		SET_PED_RESET_FLAG(localPlayerPed, PRF_SuppressInAirEvent, TRUE)
		TASK_ENTER_VEHICLE(LocalPlayerPed, tempVeh, 1, INT_TO_ENUM(VEHICLE_SEAT, iVehicleSeat), DEFAULT, ECF_WARP_PED )
		SET_PED_RESET_FLAG(localPlayerPed, PRF_SuppressInAirEvent, TRUE)
		g_bBlockSeatShufflePlacedVeh = TRUE
	ENDIF
ENDPROC

PROC SET_PLAYER_INTO_NEAREST_PLACED_VEHICLE_SIMPLE()
	
	IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		PRINTLN("[LM][RCC MISSION][SET_PLAYER_INTO_NEAREST_PLACED_VEHICLE_SIMPLE] - We are now in the vehicle. EXIT.")			
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles = 0
		PRINTLN("[LM][RCC MISSION][SET_PLAYER_INTO_NEAREST_PLACED_VEHICLE_SIMPLE] - There are no vehicles. EXIT.")
		
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("[LM] Using SET_PLAYER_INTO_NEAREST_PLACED_VEHICLE_SIMPLE without any placed vehicles. Content need to sort this out.")
				
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_NO_VEHICLES_PLACE_IN_NEAREST, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "Trying to spawn in nearest placed vehicle. No Placed Vehicles exist, check start/check settings.")
		#ENDIF
				
		EXIT
	ENDIF
	
	INT iVeh	
	FLOAT fDistCached = 999999
	INT iVehAssigned = -1
	VEHICLE_INDEX tempVeh
	
	IF bLocalPlayerPedOK		
		FOR iVeh = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
				tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
				
				IF DOES_ENTITY_EXIST(tempVeh)
				AND IS_VEHICLE_DRIVEABLE(tempVeh)
				AND DoesVehicleHaveFreeSeat(tempVeh)
					FLOAT fDistFromVeh = VDIST2(GET_ENTITY_COORDS(localPlayerPed), GET_ENTITY_COORDS(tempVeh))
					
					IF fDistFromVeh < fDistCached
						fDistCached = fDistFromVeh
						iVehAssigned = iVeh
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF iVehAssigned > -1
		PRINTLN("[LM][RCC MISSION][SET_PLAYER_INTO_NEAREST_PLACED_VEHICLE_SIMPLE] - (placed vehicle) iVehAssigned: ", iVehAssigned)
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehAssigned])
			tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehAssigned])
			IF DOES_ENTITY_EXIST(tempVeh)
			AND NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed, TRUE)
			AND GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
			AND GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
				INT iVehicleSeat = GetFirstFreeSeatOfVehicleData(iVehAssigned)
				PRINTLN("[LM][RCC MISSION][SET_PLAYER_INTO_NEAREST_PLACED_VEHICLE_SIMPLE] - Entering Vehicle at first available seat. iSeat: ", iVehicleSeat)
				SET_PLAYER_INTO_THIS_NEAREST_PLACED_VEHICLE(tempVeh, iVehicleSeat)				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_CUSTOM_PERSONAL_VEHICLE_START_POSITION(INT iTeam, INT iTeamSlot)
	VECTOR vRet
	
	IF iTeamSlot < FMMC_PERSONAL_VEHICLE_PLACEMENT_PER_INSTANCE
	AND iTeam < FMMC_MAX_TEAMS
	
		INT iPVCheckpoint = 0
		vRet = g_FMMC_STRUCT.vPersonalVehiclePosition[iTeam][iPVCheckpoint][iTeamSlot]
		
		PRINTLN("GET_CUSTOM_PERSONAL_VEHICLE_START_POSITION - Called w team ",iTeam,", slot ",iTeamSlot)
		
		IF MC_SHOULD_RESPAWN_AT_CHECKPOINT(iTeam)
		OR (MC_SHOULD_WE_START_FROM_CHECKPOINT() AND SHOULD_USE_CUSTOM_START_POINT_CHECKPOINTS())
			IF (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
			AND FMMC_CHECKPOINTS_IS_ANY_RETRY_START_POINT_SET()
				iPVCheckpoint = MC_GET_MISSION_STARTING_CHECKPOINT() + 1
				PRINTLN("GET_CUSTOM_PERSONAL_VEHICLE_START_POSITION - We're on a quick restart, iPVCheckpoint = ",iPVCheckpoint)
			ENDIF
			
			IF (iPVCheckpoint < FMMC_PERSONAL_VEHICLE_PLACEMENT_INSTANCES)
			AND (iTeamSlot < FMMC_PERSONAL_VEHICLE_PLACEMENT_PER_INSTANCE)			
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vPersonalVehiclePosition[iTeam][iPVCheckpoint][iTeamSlot])
					PRINTLN("GET_CUSTOM_PERSONAL_VEHICLE_START_POSITION - Assigning vector from PV Point: ", g_FMMC_STRUCT.vPersonalVehiclePosition[iTeam][iPVCheckpoint][iTeamSlot])
					vRet = g_FMMC_STRUCT.vPersonalVehiclePosition[iTeam][iPVCheckpoint][iTeamSlot]
				ENDIF
			ENDIF
		ENDIF
		
		// If it's mission start.
		IF MC_GET_MISSION_STARTING_CHECKPOINT() = 0
		
			INT iPVDynamicSlot = GET_ORDERED_PARTICIPANT_NUMBER(iLocalPart)
			PLAYER_INDEX piPlayerBoss = MC_ServerBD_4.piGangBossID
			DEFUNCT_BASE_ID eDefunctBaseID
			SIMPLE_INTERIORS siAutoshop
			
			SWITCH g_FMMC_STRUCT.ePVSpawnProperty
				CASE PVSP_DEFUNCT_BASE
					
					PRINTLN("GET_CUSTOM_PERSONAL_VEHICLE_START_POSITION - PVSP_DEFUNCT_BASE")
					eDefunctBaseID = GET_PLAYERS_OWNED_DEFUNCT_BASE(piPlayerBoss)
					
					#IF IS_DEBUG_BUILD			
					PRINTLN("GET_CUSTOM_PERSONAL_VEHICLE_START_POSITION - Boss PlayerNum: ", NATIVE_TO_INT(piPlayerBoss))
					PRINTLN("GET_CUSTOM_PERSONAL_VEHICLE_START_POSITION - Facility Int: ", ENUM_TO_INT(eDefunctBaseID))
					
					IF ENUM_TO_INT(eDefunctBaseID) = 0
						PRINTLN("[LM] - GET_CUSTOM_PERSONAL_VEHICLE_START_POSITION wont work - eDefunctBaseID is invalid. This is because we are not spawning at our bosses facility.")				
					ENDIF
					#ENDIF
					
					IF NOT IS_VECTOR_ZERO(GET_HEIST_FACILITY_SPAWN_LOCATION(eDefunctBaseID, TRUE, iPVDynamicSlot))				
						vRet = GET_HEIST_FACILITY_SPAWN_LOCATION(eDefunctBaseID, TRUE, iPVDynamicSlot)
						PRINTLN("GET_CUSTOM_PERSONAL_VEHICLE_START_POSITION - (Override) Facility Custom PV start pos ", vRet)
					ENDIF
				BREAK
				
				CASE PVSP_TUNER_AUTOSHOP
					siAutoshop = GET_OWNED_AUTO_SHOP_SIMPLE_INTERIOR(GB_GET_LOCAL_PLAYER_GANG_BOSS()) 
					FLOAT fHeadDummy
					fHeadDummy = 0.0
					GET_AUTO_SHOP_EXTERIOR_SPAWN_POINT_IN_VEHICLE(siAutoshop, iTeamSlot, vRet, fHeadDummy)		
					PRINTLN("GET_CUSTOM_PERSONAL_VEHICLE_START_POSITION - PVSP_TUNER_AUTOSHOP - Using vCoord: ", vRet, " From iTeamSlot: ", iTeamSlot)
				BREAK
			ENDSWITCH
		ENDIF		
	ENDIF
		
	PRINTLN("GET_CUSTOM_PERSONAL_VEHICLE_START_POSITION - Returning: ", vRet)
	RETURN vRet
	
ENDFUNC

FUNC FLOAT GET_CUSTOM_PERSONAL_VEHICLE_START_HEADING(INT iTeam, INT iTeamSlot)
	
	INT iPVCheckpoint = 0
	FLOAT fHeading
	
	IF MC_SHOULD_RESPAWN_AT_CHECKPOINT(iTeam)
	OR (MC_SHOULD_WE_START_FROM_CHECKPOINT() AND SHOULD_USE_CUSTOM_START_POINT_CHECKPOINTS())
		IF (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
		AND FMMC_CHECKPOINTS_IS_ANY_RETRY_START_POINT_SET()
			iPVCheckpoint = MC_GET_MISSION_STARTING_CHECKPOINT() + 1
		ENDIF
	ENDIF
	
	IF (iPVCheckpoint < FMMC_PERSONAL_VEHICLE_PLACEMENT_INSTANCES)
	AND (iTeamSlot < FMMC_PERSONAL_VEHICLE_PLACEMENT_PER_INSTANCE)
		IF g_FMMC_STRUCT.fPersonalVehicleHeading[iTeam][iPVCheckpoint][iTeamSlot] <> 0.0
			PRINTLN("GET_CUSTOM_PERSONAL_VEHICLE_START_HEADING - Assigning heading from PV Point: ", g_FMMC_STRUCT.fPersonalVehicleHeading[iTeam][iPVCheckpoint][iTeamSlot])
			fHeading = g_FMMC_STRUCT.fPersonalVehicleHeading[iTeam][iPVCheckpoint][iTeamSlot]
		ENDIF
	ENDIF
	
	// If it's mission start.
	IF MC_GET_MISSION_STARTING_CHECKPOINT() = 0
	
		INT iPVDynamicSlot = GET_ORDERED_PARTICIPANT_NUMBER(iLocalPart)
		PLAYER_INDEX piPlayerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		DEFUNCT_BASE_ID eDefunctBaseID
		SIMPLE_INTERIORS siAutoshop
		
		SWITCH g_FMMC_STRUCT.ePVSpawnProperty
			CASE PVSP_DEFUNCT_BASE
				eDefunctBaseID = GET_PLAYERS_OWNED_DEFUNCT_BASE(piPlayerBoss)			
				
				#IF IS_DEBUG_BUILD			
				PRINTLN("GET_CUSTOM_PERSONAL_VEHICLE_START_HEADING - Boss PlayerNum: ", NATIVE_TO_INT(piPlayerBoss))
				PRINTLN("GET_CUSTOM_PERSONAL_VEHICLE_START_HEADING - Facility Int: ", ENUM_TO_INT(eDefunctBaseID))
				#ENDIF
				
				PRINTLN("GET_CUSTOM_PERSONAL_VEHICLE_START_HEADING - [MISSION START POS] - (Override) Facility Custom PV start heading ", GET_HEIST_FACILITY_SPAWN_HEADING(eDefunctBaseID, TRUE, iPVDynamicSlot))
				fHeading =  GET_HEIST_FACILITY_SPAWN_HEADING(eDefunctBaseID, TRUE, iPVDynamicSlot)
			BREAK
						
			CASE PVSP_TUNER_AUTOSHOP			
				siAutoshop = GET_OWNED_AUTO_SHOP_SIMPLE_INTERIOR(GB_GET_LOCAL_PLAYER_GANG_BOSS()) 
				VECTOR vRetDummy
				vRetDummy = <<0.0, 0.0, 0.0>>
				GET_AUTO_SHOP_EXTERIOR_SPAWN_POINT_IN_VEHICLE(siAutoshop, iTeamSlot, vRetDummy, fHeading)		
				
				PRINTLN("GET_CUSTOM_PERSONAL_VEHICLE_START_HEADING - PVSP_TUNER_AUTOSHOP - Using Heading: ", fHeading, " From iTeamSlot: ", iTeamSlot)
			BREAK
		ENDSWITCH
	ENDIF
	
	PRINTLN("GET_CUSTOM_PERSONAL_VEHICLE_START_HEADING - Returning: ", fHeading)
	RETURN fHeading
	
ENDFUNC

FUNC BOOL SHOULD_SPAWN_IN_NON_PLACED_VEHICLE_AT_MISSION_START(INT iMyTeam)
	
	BOOL bReturnValue
	
	// Options for "Team" Vehicle.
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciSPAWN_INTO_VEH)
		PRINTLN("[RCC MISSION] - [MISSION START POS] - SHOULD_SPAWN_IN_NON_PLACED_VEHICLE_AT_MISSION_START - ciSPAWN_INTO_VEH is set")
		
		// Creator "Team" Vehicle - Default is ciSPAWN_INTO_VEH
		IF g_FMMC_STRUCT.mnVehicleModel[iMyTeam] != DUMMY_MODEL_FOR_SCRIPT
			PRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] - [MISSION START POS] - SHOULD_SPAWN_IN_NON_PLACED_VEHICLE_AT_MISSION_START - g_FMMC_STRUCT.mnVehicleModel[ ",iMyTeam," ] != DUMMY_MODEL_FOR_SCRIPT")
			bReturnValue = TRUE
		ELSE
			PRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] - [MISSION START POS] - SHOULD_SPAWN_IN_NON_PLACED_VEHICLE_AT_MISSION_START - g_FMMC_STRUCT.mnVehicleModel[ ",iMyTeam," ] = DUMMY_MODEL_FOR_SCRIPT")
		ENDIF
		
		// Corona Starting Vehicle choice - Used more for Adversaries or Personal Vehicle override
		IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iMyTeam ].iTeamBitset, ciBS_TEAM_USES_CORONA_VEHICLE_LIST )
			PRINTLN("[RCC MISSION] - [MISSION START POS] - SHOULD_SPAWN_IN_NON_PLACED_VEHICLE_AT_MISSION_START - ciALLOW_VEHICLE_CHOICE.")
			IF g_mnMyRaceModel != DUMMY_MODEL_FOR_SCRIPT
				PRINTLN("[RCC MISSION] - [MISSION START POS] - SHOULD_SPAWN_IN_NON_PLACED_VEHICLE_AT_MISSION_START - g_mnMyRaceModel is valid")
				bReturnValue = TRUE
			ENDIF
		ENDIF		
		
		// Corona Vehicle choice - Used a little bit for Mission Logic and to start the player in a specific vehicle
		IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iMyTeam ].iTeamBitset, ciBS_TEAM_USES_CORONA_VEHICLE_LIST )
			PRINTLN("[RCC MISSION] - [MISSION START POS] - SHOULD_SPAWN_IN_NON_PLACED_VEHICLE_AT_MISSION_START - ciBS_TEAM_USES_CORONA_VEHICLE_LIST.")
			IF g_mnCoronaMissionVehicle != DUMMY_MODEL_FOR_SCRIPT
				PRINTLN("[RCC MISSION] - [MISSION START POS] - SHOULD_SPAWN_IN_NON_PLACED_VEHICLE_AT_MISSION_START - g_mnCoronaMissionVehicle is valid")
				bReturnValue = TRUE
			ENDIF
		ENDIF
	ENDIF
		
	IF bReturnValue
		IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0) 
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_SPAWN_INTO_VEH_DISABLE_FOR_CHECKPOINT_1)
			PRINTLN("[RCC MISSION] - [MISSION START POS] - SHOULD_SPAWN_IN_NON_PLACED_VEHICLE_AT_MISSION_START - ciOptionsBS28_SPAWN_INTO_VEH_DISABLE_FOR_CHECKPOINT_1 is set - Disabling spawn in veh.")
			bReturnValue = FALSE
		ENDIF
		
		IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1) 
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_SPAWN_INTO_VEH_DISABLE_FOR_CHECKPOINT_2)
			PRINTLN("[RCC MISSION] - [MISSION START POS] - SHOULD_SPAWN_IN_NON_PLACED_VEHICLE_AT_MISSION_START - ciOptionsBS28_SPAWN_INTO_VEH_DISABLE_FOR_CHECKPOINT_2 is set - Disabling spawn in veh.")
			bReturnValue = FALSE
		ENDIF
		
		IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2) 
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_SPAWN_INTO_VEH_DISABLE_FOR_CHECKPOINT_3)
			PRINTLN("[RCC MISSION] - [MISSION START POS] - SHOULD_SPAWN_IN_NON_PLACED_VEHICLE_AT_MISSION_START - ciOptionsBS28_SPAWN_INTO_VEH_DISABLE_FOR_CHECKPOINT_3 is set - Disabling spawn in veh.")
			bReturnValue = FALSE
		ENDIF
		
		IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3) 
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_SPAWN_INTO_VEH_DISABLE_FOR_CHECKPOINT_4)
			PRINTLN("[RCC MISSION] - [MISSION START POS] - SHOULD_SPAWN_IN_NON_PLACED_VEHICLE_AT_MISSION_START - ciOptionsBS28_SPAWN_INTO_VEH_DISABLE_FOR_CHECKPOINT_4 is set - Disabling spawn in veh.")
			bReturnValue = FALSE
		ENDIF
	ENDIF
	
	PRINTLN("[RCC MISSION] - [MISSION START POS] - SHOULD_SPAWN_IN_NON_PLACED_VEHICLE_AT_MISSION_START - returning value ", bReturnValue)
	RETURN bReturnValue
	
ENDFUNC

FUNC BOOL SHOULD_PLAYER_ENTER_COVER_AFTER_START_POSITION()
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciSPAWN_IN_COVER)
ENDFUNC

PROC CLEAR_ROOM_AFTER_START_POSITION_WARP(BOOL bForce = FALSE)
	IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_CLEARED_INTERIOR_FLAG_FOR_ENTITY)
	OR bForce
		IF bLocalPlayerPedOK
			VECTOR vCurrentCoord = GET_ENTITY_COORDS(localPlayerPed)
			IF IS_COLLISION_MARKED_OUTSIDE(vCurrentCoord)
			OR GET_INTERIOR_AT_COORDS(vCurrentCoord) = NULL // (false positive) sometimes.
			OR GET_INTERIOR_FROM_COLLISION(vCurrentCoord) = NULL
				
				PRINTLN("[LM][RCC MISSION]  [MISSION START POS] - CLEAR_ROOM_AFTER_START_POSITION_WARP - There is no Interior at the location we are spawning (vCurrentCoord: ", vCurrentCoord, ")")
				
				PRINTLN("[LM][RCC MISSION]  [MISSION START POS] - CLEAR_ROOM_AFTER_START_POSITION_WARP - Calling CLEAR_ROOM_FOR_ENTITY for Ped.")
				
				CLEAR_ROOM_FOR_ENTITY(localPlayerPed)
				
				IF IS_PED_IN_ANY_VEHICLE(localPlayerPed, TRUE)
					VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(localPlayerPed, TRUE)
					
					IF IS_VEHICLE_DRIVEABLE(vehTemp)
						PRINTLN("[LM][RCC MISSION]  [MISSION START POS] - CLEAR_ROOM_AFTER_START_POSITION_WARP - Calling CLEAR_ROOM_FOR_ENTITY for Vehicle.")
						// IGNORE OWNERSHIP ASSERTS RELATED TO THIS FUNCTION HERE - THIS IS INTENTIONAL. CODE STILL FUNCTIONS EVEN IF IT ASSERTS. url:bugstar:5879562
						CLEAR_ROOM_FOR_ENTITY(vehTemp)
						
						IF NETWORK_HAS_CONTROL_OF_ENTITY(vehTemp)
							PRINTLN("[LM][RCC MISSION]  [MISSION START POS] - CLEAR_ROOM_AFTER_START_POSITION_WARP - Calling SET_ENTITY_VISIBLE for Vehicle.")
							SET_ENTITY_VISIBLE(vehTemp, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(localPlayerPed)
					PRINTLN("[LM][RCC MISSION]  [MISSION START POS] - CLEAR_ROOM_AFTER_START_POSITION_WARP - Calling SET_ENTITY_VISIBLE for Ped.")
					SET_ENTITY_VISIBLE(localPlayerPed, TRUE)
				ENDIF
				
				BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ClearRoomForPlayerPed)
			ELSE
				PRINTLN("[RCC MISSION]  [MISSION START POS] - CLEAR_ROOM_AFTER_START_POSITION_WARP - There is an Interior at the location we are spawning (vCurrentCoord: ", vCurrentCoord, ")")
			ENDIF
		ENDIF
		
		SET_BIT(iLocalBoolCheck31, LBOOL31_CLEARED_INTERIOR_FLAG_FOR_ENTITY)
	ENDIF
ENDPROC

PROC ADD_VARIATION_TO_START_HEADING()

	IF GET_RANDOM_INT_IN_RANGE(0,11) >= 5
		ftempStartHeading = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].fStartHeading + GET_RANDOM_INT_IN_RANGE(0,11)
		IF ftempStartHeading > 360
			ftempStartHeading = (ftempStartHeading - 360)
		ENDIF
	ELSE
		ftempStartHeading = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].fStartHeading - GET_RANDOM_INT_IN_RANGE(0,11)
		IF ftempStartHeading < 0
			ftempStartHeading = (ftempStartHeading + 360)
		ENDIF
	ENDIF

ENDPROC

PROC ADD_RANDOM_VARIATION_TO_SPAWN_POSITION(FLOAT fCircleRadius)

	//GET_RANDOM_POINT_IN_SPHERE()
	
	INT ivariance = ROUND(fCircleRadius)
	FLOAT fvariance
	
	IF GET_RANDOM_INT_IN_RANGE(0,11) >= 5
		ivariance = GET_RANDOM_INT_IN_RANGE(0,ivariance)
		fvariance = TO_FLOAT(ivariance/5)
		vWarpPos.x = (vWarpPos.x + fvariance)
	ELSE
		ivariance = GET_RANDOM_INT_IN_RANGE(0,ivariance)
		fvariance = TO_FLOAT(ivariance/5)
		vWarpPos.x = (vWarpPos.x - fvariance)
	ENDIF
	IF GET_RANDOM_INT_IN_RANGE(0,11) >= 5
		ivariance = GET_RANDOM_INT_IN_RANGE(0,ivariance)
		fvariance = TO_FLOAT(ivariance/5)
		vWarpPos.y = (vWarpPos.y + fvariance)
	ELSE
		ivariance = GET_RANDOM_INT_IN_RANGE(0,ivariance)
		fvariance = TO_FLOAT(ivariance/5)
		vWarpPos.y = (vWarpPos.y - fvariance)
	ENDIF
	
ENDPROC

PROC ADD_FALLBACK_SPAWN_POINTS()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciFALLBACK_SPAWN_POINTS_ENABLE)
		INT i
		
		REPEAT FMMC_MAX_SPAWNPOINTS i
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[i].vPosition)
				ADD_FALLBACK_SPAWN_POINT(g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[i].vPosition, g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[i].fHeading)
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC SETUP_RC_SPAWN_LOCATION(Vector vSpawnLoc ,BOOL bFaceHost)

	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	#ENDIF

	VECTOR vSpawnHeading
	
	IF bFaceHost
		IF NOT g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bIAmDriverForContactMission
			vSpawnHeading = g_TransitionSessionNonResetVars.PlayerMissionStartLocation
			PRINTLN("[RCC MISSION] - [MISSION START POS] - SETUP_RC_SPAWN_LOCATION - setting start spawn heading towards host: ", vSpawnHeading)
		ELSE
			vSpawnHeading = GET_COORDS_OF_CLOSEST_OBJECTIVE()
			PRINTLN("[RCC MISSION] - [MISSION START POS] - SETUP_RC_SPAWN_LOCATION - setting start spawn towards objective cause corona host: ", vSpawnHeading)
		ENDIF
	ELSE
		vSpawnHeading = GET_COORDS_OF_CLOSEST_OBJECTIVE()
		PRINTLN("[RCC MISSION] - [MISSION START POS] - SETUP_RC_SPAWN_LOCATION - setting start spawn towards objective: ", vSpawnHeading)
	ENDIF
	
	IF IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].sRuleBounds[GET_LOCAL_PLAYER_CURRENT_RULE()][0].vHeadingVec)
		SET_PLAYER_WILL_SPAWN_FACING_COORDS(vSpawnHeading, TRUE, FALSE)
		PRINTLN("[RCC MISSION] No valid bounds heading for this rule- SET_PLAYER_WILL_SPAWN_FACING_COORDS - vspawnheading: ", vSpawnHeading)
	ENDIF
	
	PRINTLN("[RCC MISSION] - [MISSION START POS] - SETUP_RC_SPAWN_LOCATION - setting start spawn passing in 15 meters radius: ", vSpawnLoc)
	SET_MISSION_SPAWN_SPHERE(vSpawnLoc, 15.0, DEFAULT, g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bIAmDriverForContactMission, FALSE, 3.0)
	
	SET_BIT(ilocalboolcheck, LBOOL_MISSION_READY_TO_WARP_START_POSITION)
	
ENDPROC

PROC SET_START_CUSTOM_SPAWN_POINTS(INT iplayerteam)

	INT iteamslot
	CLEAR_SPECIFIC_SPAWN_LOCATION()

	iteamslot = GET_PARTICIPANT_NUMBER_IN_TEAM() 

	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iplayerteam].iTeamBitset2, ciBS2_TEAM_ONE_PLAYER_RAND_SPAWN)
		INT iBit
		FOR iBit = 0 TO 31
			IF IS_BIT_SET(MC_ServerBD.iSpawnPointsUsed[iplayerteam], iBit)
			OR IS_BIT_SET(LocalRandomSpawnBitset[iplayerteam], iBit)
				iteamslot = iBit
				iBit = 31 //Break out
			ENDIF
		ENDFOR
	ENDIF
	
	IF iteamslot < FMMC_MAX_TEAMSPAWNPOINTS
        PRINTLN("[RCC MISSION] SET_UP_CUSTOM_SPAWN_POINTS,  ", "iteamslot = ", iteamslot, " iplayerteam = ", iplayerteam)
		
		VECTOR vSpawn
		FLOAT fHeading
		
		//Params to pass into SETUP_SPECIFIC_SPAWN_LOCATION
		FLOAT fSearchRadius = 100.0
		BOOL bDoVisibleChecks = TRUE
		BOOL bConsiderInteriors = FALSE
		BOOL bDoNearARoadChecks = TRUE
		FLOAT fMinDistFromOtherPlayers = 65.0
	
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iplayerteam][iteamslot].vPos)
			vSpawn = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iplayerteam][iteamslot].vPos
			fHeading = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iplayerteam][iteamslot].fHead
			
			fSearchRadius = 5
			bDoVisibleChecks = FALSE
			bConsiderInteriors = TRUE
			bDoNearARoadChecks = FALSE
			fMinDistFromOtherPlayers = 0
   		ELSE
			PRINTLN("[RCC MISSION]  USING BACKUP SPAWN SINCE TEAM POINTS ZERO!!!")
			vSpawn = g_FMMC_STRUCT.sFMMCEndConditions[iplayerteam].vStartPos
			fHeading = g_FMMC_STRUCT.sFMMCEndConditions[iplayerteam].fStartHeading
		ENDIF
		
		SETUP_SPECIFIC_SPAWN_LOCATION(vSpawn, fHeading, fSearchRadius, bDoVisibleChecks, DEFAULT, bConsiderInteriors, bDoNearARoadChecks, fMinDistFromOtherPlayers)
	ENDIF
	
ENDPROC

PROC DEFAULT_ON_FOOT_CONTACT_START_POSITION()
	
	IF NOT IS_VECTOR_ZERO(g_TransitionSessionNonResetVars.PlayerMissionStartLocation)
		
		PRINTLN("[RCC MISSION] - [MISSION START POS] - DEFAULT_ON_FOOT_CONTACT_START_POSITION - g_TransitionSessionNonResetVars.PlayerMissionStartLocation = ", g_TransitionSessionNonResetVars.PlayerMissionStartLocation)
		
		IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_CHECKED_ZONE)
		
			PRINTLN("[RCC MISSION] - [MISSION START POS] - DEFAULT_ON_FOOT_CONTACT_START_POSITION - not checked restriction zone yet.")
			IF IS_ENTITY_IN_AIR(LocalPlayerPed)
				PRINTLN("[RCC MISSION] - [MISSION START POS] - IS_ENTITY_IN_AIR - we are mid-air, this is a restricted area.")
			ENDIF
			
			IF IS_POSITION_IN_MISSION_RESTRICTED_AREA(g_TransitionSessionNonResetVars.PlayerMissionStartLocation)
			OR IS_ENTITY_IN_AIR(LocalPlayerPed)
			OR (IS_THIS_A_CONTACT_MISSION() AND g_TransitionSessionNonResetVars.PlayerMissionStartLocation.z < 0 AND IS_PLAYER_IN_MP_PROPERTY(LocalPlayer, FALSE))
			
				PRINTLN("[RCC MISSION] - [MISSION START POS] - DEFAULT_ON_FOOT_CONTACT_START_POSITION - ", g_TransitionSessionNonResetVars.PlayerMissionStartLocation, " is in a restricted zone.")
				PRINTLN("[RCC MISSION] - [MISSION START POS] - DEFAULT_ON_FOOT_CONTACT_START_POSITION - using SETUP_RC_SPAWN_LOCATION with g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].vStartPos = ", g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].vStartPos)
				SETUP_RC_SPAWN_LOCATION(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].vStartPos, FALSE)		
				
				SET_BIT(ilocalboolcheck,LBOOL_MISSION_READY_TO_WARP_START_POSITION)
				
				EXIT
			ELSE
				PRINTLN("[RCC MISSION] - [MISSION START POS] - DEFAULT_ON_FOOT_CONTACT_START_POSITION - ", g_TransitionSessionNonResetVars.PlayerMissionStartLocation, " is not in a restricted zone.")
	
				SETUP_RC_SPAWN_LOCATION(g_TransitionSessionNonResetVars.PlayerMissionStartLocation,TRUE)
				SET_BIT(ilocalboolcheck,LBOOL_MISSION_READY_TO_WARP_START_POSITION)
			ENDIF
			
			PRINTLN("[RCC MISSION] - [MISSION START POS] - set LBOOL2_CHECKED_ZONE.")
			SET_BIT(iLocalBoolCheck2,LBOOL2_CHECKED_ZONE)
		ENDIF
	ELSE
	
		PRINTLN("[RCC MISSION] - [MISSION START POS] - DEFAULT_ON_FOOT_CONTACT_START_POSITION - MISSION - TRANSITION TIME - TEAM SPAWN 2  - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
		
		PRINTLN("[RCC MISSION] - [MISSION START POS] - DEFAULT_ON_FOOT_CONTACT_START_POSITION - g_TransitionSessionNonResetVars.PlayerMissionStartLocation = 0.")
		PRINTLN("[RCC MISSION] - [MISSION START POS] - DEFAULT_ON_FOOT_CONTACT_START_POSITION - using SETUP_RC_SPAWN_LOCATION with g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].vStartPos = ", g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].vStartPos)
		
		SETUP_RC_SPAWN_LOCATION(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].vStartPos,FALSE)		
		SET_BIT(ilocalboolcheck,LBOOL_MISSION_READY_TO_WARP_START_POSITION)
		
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Custom Spawn Points
// ##### Description: Wrappers and Helper Functions that are used for Custon Spawn/Respawn Points
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_CUSTOM_RESPAWN_POINT_A_LINKED_POINT(INT iTeam, INT iTeamSpawnPoint)
	
	INT i = 0
	FOR i = 0 TO ciFMMC_MAX_TEAM_SPAWN_POINT_BS-1
		IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPoint].iLinkedActivationSpawnPoints[i] != 0
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL ARE_CUSTOM_RESPAWN_POINTS_LINKED(INT iTeam, INT iTeamSpawnPoint1, INT iTeamSpawnPoint2)
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPoint1].iLinkedActivationSpawnPoints, iTeamSpawnPoint2)
		RETURN TRUE
	ENDIF

	RETURN FALSE
	
ENDFUNC

PROC ACTIVATE_LINKED_CUSTOM_RESPAWN_POINTS(INT iTeam, INT iTeamSpawnPoint)
	
	IF NOT IS_CUSTOM_RESPAWN_POINT_A_LINKED_POINT(iTeam, iTeamSpawnPoint)
		EXIT
	ENDIF
	
	PRINTLN("[MC_CustomSpawning] - ACTIVATE_LINKED_CUSTOM_RESPAWN_POINTS START ----------------")
	
	INT i = 0
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTeam]-1
		IF ARE_CUSTOM_RESPAWN_POINTS_LINKED(iTeam, iTeamSpawnPoint, i)
			FMMC_SET_LONG_BIT(iBSRespawnActiveFromRadius[iTeam], i)
			FMMC_CLEAR_LONG_BIT(iBSRespawnDeactivated[iTeam], i)
			
			PRINTLN("[MC_CustomSpawning] - ACTIVATE_LINKED_CUSTOM_RESPAWN_POINTS iTeam: ", iTeam, " ITeamSpawnPoint: ", iTeamSpawnPoint, " is linked to point: ", i, " Activating it!")
		ENDIF
	ENDFOR
	
	PRINTLN("[MC_CustomSpawning] - ACTIVATE_LINKED_CUSTOM_RESPAWN_POINTS END ----------------")
	
ENDPROC

FUNC VECTOR GET_CUSTOM_RESPAWN_POSITION(FLOAT &fNewHead, INT iTeamSpawnPointCounter, INT iTeam, INT iRespawnPoint, BOOL bPrintOut = FALSE)

	VECTOR vCoord				= g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].vPos
	FLOAT fHead					= g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].fHead
		
	IF iRespawnPoint > -1
		INT iAreaType				= g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iAreaType
		INT iAreaScale				= g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iAreaScale
		INT iSpawnPointQuantity 	= g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iSpawnPointQuantity
		INT iGetGroundFromHeight 	= g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iGetGroundFromHeight
		// Stagger the print so we don't spam
		IF GET_FRAME_COUNT() % 60 = 0
		OR bPrintOut
			PRINTLN("[MC_CustomSpawning] - [LM][MAINTAIN_CUSTOM_RESPAWN_DECALS] - Maintaining the Decals for Custom Spawn Points, Point: ", iTeamSpawnPointCounter)
			PRINTLN("[MC_CustomSpawning] - [LM][MAINTAIN_CUSTOM_RESPAWN_DECALS] - vCoord:            		", vCoord)
			PRINTLN("[MC_CustomSpawning] - [LM][MAINTAIN_CUSTOM_RESPAWN_DECALS] - fHead:             		", fHead)
			PRINTLN("[MC_CustomSpawning] - [LM][MAINTAIN_CUSTOM_RESPAWN_DECALS] - iAreaType:              ", iAreaType)
			PRINTLN("[MC_CustomSpawning] - [LM][MAINTAIN_CUSTOM_RESPAWN_DECALS] - iAreaScale: 			", iAreaScale)
			PRINTLN("[MC_CustomSpawning] - [LM][MAINTAIN_CUSTOM_RESPAWN_DECALS] - iSpawnPointQuantity:    ", iSpawnPointQuantity)
		ENDIF
		
		VECTOR vNewPos
		SWITCH INT_TO_ENUM(eCustomRespawnPointType, iAreaType)
			CASE eCustomRespawnPointType_Box
				vNewPos = GET_VECTOR_CUSTOM_RESPAWN_AREA_BOX(iRespawnPoint, iSpawnPointQuantity, iAreaScale, vCoord, fHead)		BREAK
			CASE eCustomRespawnPointType_LineX
				vNewPos = GET_VECTOR_CUSTOM_RESPAWN_AREA_LINEX(iRespawnPoint, iSpawnPointQuantity, iAreaScale, vCoord, fHead)		BREAK
			CASE eCustomRespawnPointType_LineY
				vNewPos = GET_VECTOR_CUSTOM_RESPAWN_AREA_LINEY(iRespawnPoint, iSpawnPointQuantity, iAreaScale, vCoord, fHead)		BREAK
			CASE eCustomRespawnPointType_Scattered
				vNewPos = GET_VECTOR_CUSTOM_RESPAWN_AREA_SCATTERED(iRespawnPoint, iSpawnPointQuantity, iAreaScale, vCoord, fHead)	BREAK
		ENDSWITCH
		
		IF iGetGroundFromHeight > 0
			VECTOR vStart = vNewPos
			vStart += <<0.0, 0.0, GET_GROUND_Z_HEIGHT_FROM_INDEX(iGetGroundFromHeight)>>
			
			GET_GROUND_Z_FOR_3D_COORD(vStart, vNewPos.z)
		ENDIF
			
		fNewHead = fHead
		RETURN vNewPos
	ENDIF

	fNewHead = fHead
	RETURN vCoord
ENDFUNC

FUNC BOOL SHOULD_THIS_RULE_USE_CUSTOM_SPAWN_POINTS(INT iRuleMod = 0, INT iCustomRule = -1)
	
	INT iRuleToUse = MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] + iRuleMod
	
	IF iCustomRule > -1
		iRuleToUse = iCustomRule
		PRINTLN("[MC_CustomSpawning_Spam] - SHOULD_THIS_RULE_USE_CUSTOM_SPAWN_POINTS - Using iCustomRule: ", iCustomRule)
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck34, LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES)
		PRINTLN("[MC_CustomSpawning_Spam] - SHOULD_THIS_RULE_USE_CUSTOM_SPAWN_POINTS - LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES_TRIGGERED is set so returning false")
		RETURN FALSE
	ENDIF
	
	INT iTeamSpawnPointCounter
	FOR iTeamSpawnPointCounter = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[MC_playerBD[iLocalPart].iteam] - 1
		IF iRuleToUse < FMMC_MAX_RULES
			IF IS_SPAWN_POINT_A_VALID_RESPAWN_POINT(iTeamSpawnPointCounter)
				PRINTLN("[MC_CustomSpawning_Spam] - SHOULD_THIS_RULE_USE_CUSTOM_SPAWN_POINTS - Returning TRUE as point ", iTeamSpawnPointCounter, " is valid for use for rule ", iRuleToUse)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("[MC_CustomSpawning_Spam] - SHOULD_THIS_RULE_USE_CUSTOM_SPAWN_POINTS - Returning FALSE for rule ", iRuleToUse)
	RETURN FALSE
ENDFUNC

PROC CACHE__SHOULD_USE_CUSTOM_SPAWN_POINTS()
	IF SHOULD_THIS_RULE_USE_CUSTOM_SPAWN_POINTS()
		SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_USE_CUSTOM_SPAWN_POINTS)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_USE_CUSTOM_START_SPAWN_POINTS()

	INT iTeamSpawnPointCounter
	FOR iTeamSpawnPointCounter = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[MC_playerBD[iLocalPart].iteam]-1
		IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
			IF IS_SPAWN_POINT_A_VALID_START_POINT(iTeamSpawnPointCounter, MC_playerBD[iLocalPart].iteam)
				PRINTLN("[MC_CustomSpawning][SHOULD_USE_CUSTOM_START_SPAWN_POINTS] Using Custom START Points")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("[MC_CustomSpawning][SHOULD_USE_CUSTOM_START_SPAWN_POINTS] RETURNING FALSE")
	RETURN FALSE
ENDFUNC

PROC ADD_ALL_CUSTOM_SPAWN_POINTS(INT iRuleOverride = -1)
	
	#IF IS_DEBUG_BUILD
		DEBUG_RESET_LOCAL_PLAYER_USING_SPAWN_POINTS()
	#ENDIF
	
	PRINTLN("[MC_CustomSpawning][ADD_ALL_CUSTOM_SPAWN_POINTS][LM] - Resetting the Custom Spawn Points and adding new ones. Calling USE_CUSTOM_SPAWN_POINTS ...")

	DEBUG_PRINTCALLSTACK()
	
	USE_CUSTOM_SPAWN_POINTS(TRUE, default, default, default, default, default, default, default, default, default, default, TRUE)
	
	SET_BIT(iLocalBoolCheck21, LBOOL21_USING_CUSTOM_RESPAWN_POINTS)
	
	INT iTeam = MC_playerBD[iLocalPart].iteam
	INT iRuleToUse = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	BOOL bAddedPoint = FALSE
	PRINTLN("[MC_CustomSpawning][ADD_ALL_CUSTOM_SPAWN_POINTS][LM] iRuleToUse = ", iRuleToUse, " for team ", iTeam)
	
	IF iRuleOverride > -1
		PRINTLN("[MC_CustomSpawning][ADD_ALL_CUSTOM_SPAWN_POINTS][LM] - Overriding rule from ", iRuleToUse, " to ", iRuleOverride)
		iRuleToUse = iRuleOverride
	ENDIF
	
	INT iTeamSpawnPointCounter
	FOR iTeamSpawnPointCounter = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTeam]-1
		FMMC_CLEAR_LONG_BIT(iRespawnPointShouldBeValidNowBS[iTeam], iTeamSpawnPointCounter)
		
		IF iRuleToUse < FMMC_MAX_RULES
			IF IS_SPAWN_POINT_A_VALID_RESPAWN_POINT(iTeamSpawnPointCounter, iTeam)
				PRINTLN("[MC_CustomSpawning][ADD_ALL_CUSTOM_SPAWN_POINTS] - This Point: ", iTeamSpawnPointCounter)
				PRINTLN("[MC_CustomSpawning][ADD_ALL_CUSTOM_SPAWN_POINTS] - iRule: ", iRuleToUse, ", iRuleOverride: ", iRuleOverride)
				PRINTLN("[MC_CustomSpawning][ADD_ALL_CUSTOM_SPAWN_POINTS] - Valid from: ", g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iValidForRules_Start)
				PRINTLN("[MC_CustomSpawning][ADD_ALL_CUSTOM_SPAWN_POINTS] - Valid until: ", g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iValidForRules_End)
				PRINTLN("[MC_CustomSpawning][ADD_ALL_CUSTOM_SPAWN_POINTS] - iActivationRadius: ", g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iActivationRadius)
				
				IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iActivationRadius = 0
				OR FMMC_IS_LONG_BIT_SET(iBSRespawnActiveFromRadius[iTeam], iTeamSpawnPointCounter)
					
					IF g_bMissionEnding //If the mission is ending and we're still trying to respawn, just spawn somewhere
					OR NOT FMMC_IS_LONG_BIT_SET(iBSRespawnDeactivated[iTeam], iTeamSpawnPointCounter)
						VECTOR vCoord = <<0.0, 0.0, 0.0>>
						FLOAT fHead = 0.0
						PRINTLN("[MC_CustomSpawning][ADD_ALL_CUSTOM_SPAWN_POINTS] iTeamSpawnPointCounter:, ", iTeamSpawnPointCounter, " adding some custom respawn points..")
						// If we have an area, generate multiple points.
						IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iSpawnPointQuantity > 0
							PRINTLN("[MC_CustomSpawning][ADD_ALL_CUSTOM_SPAWN_POINTS] iSpawnPointQuantity > 0: generate multiple respawn points.")
							INT i
							FOR i = 1 TO g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iSpawnPointQuantity	//Skip 0 as that is the marker
								IF GET_NUMBER_OF_CUSTOM_SPAWN_POINTS() < MAX_NUMBER_OF_CUSTOM_SPAWN_POINTS
									vCoord = GET_CUSTOM_RESPAWN_POSITION(fHead, iTeamSpawnPointCounter, iTeam, i, TRUE)
									ADD_CUSTOM_SPAWN_POINT(vCoord, fHead)
									bAddedPoint = TRUE
									PRINTLN("[MC_CustomSpawning][ADD_ALL_CUSTOM_SPAWN_POINTS] adding point")
																
									#IF IS_DEBUG_BUILD
										DEBUG_SET_LOCAL_PLAYER_USING_SPAWN_POINT(iTeamSpawnPointCounter)
									#ENDIF
								ELSE
									PRINTLN("[MC_CustomSpawning][ADD_ALL_CUSTOM_SPAWN_POINTS] RESPAWN POINTS FULL")
								ENDIF
							ENDFOR
						ELSE
							PRINTLN("[MC_CustomSpawning][ADD_ALL_CUSTOM_SPAWN_POINTS] iTeamSpawnPointCounter:, ", iTeamSpawnPointCounter, " adding a custom respawn point..")
							IF GET_NUMBER_OF_CUSTOM_SPAWN_POINTS() < MAX_NUMBER_OF_CUSTOM_SPAWN_POINTS
								vCoord = GET_CUSTOM_RESPAWN_POSITION(fHead, iTeamSpawnPointCounter, iTeam, -1, TRUE)
								ADD_CUSTOM_SPAWN_POINT(vCoord, fHead)
								bAddedPoint = TRUE
								PRINTLN("[MC_CustomSpawning][ADD_ALL_CUSTOM_SPAWN_POINTS] adding point")
								
								#IF IS_DEBUG_BUILD
									DEBUG_SET_LOCAL_PLAYER_USING_SPAWN_POINT(iTeamSpawnPointCounter)
								#ENDIF
							ELSE
								PRINTLN("[MC_CustomSpawning][ADD_ALL_CUSTOM_SPAWN_POINTS] RESPAWN POINTS FULL")									
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[MC_CustomSpawning][ADD_ALL_CUSTOM_SPAWN_POINTS] iTeamSpawnPointCounter:, ", iTeamSpawnPointCounter, " Is deactivated. Will not add..")
					ENDIF
				ELSE
					PRINTLN("[MC_CustomSpawning][ADD_ALL_CUSTOM_SPAWN_POINTS] - Currently Deactivated. Can be activated Via Radius.")
				ENDIF				
			ENDIF
		ENDIF
		PRINTLN("[MC_CustomSpawning][ADD_ALL_CUSTOM_SPAWN_POINTS] - ------------------------------------------------------------------------------------------------------------------------------")
	ENDFOR
	
	IF NOT bAddedPoint
		USE_CUSTOM_SPAWN_POINTS(FALSE, default, default, default, default, default, default, default, default, default, default, TRUE)	
		CLEAR_BIT(iLocalBoolCheck21, LBOOL21_USING_CUSTOM_RESPAWN_POINTS)
	ENDIF
	
ENDPROC

PROC CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED()
	BOOL bMustDeactivateAllOtherPoints
	BOOL bMustActivateSpawnPoint
	BOOL bMustUnflagValidDisabledPoints
	INT iIndexActive
	INT iTeamSpawnPointCounter	
	INT iTeam = MC_playerBD[iLocalPart].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]	
	BOOL bPlayerPedInjured = !bLocalPlayerPedOK
	VECTOR vPlayerCoords = GET_ENTITY_COORDS(localPlayerPed, FALSE)
	
	IF NOT bPlayerPedInjured
	
		// Check to see if the player is close enough to have activated a set of respawn points.
		IF NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_CAN_TEAMS_ACTIVATE_OTHER_TEAMS_RESPAWN_POINTS)
			FOR iTeamSpawnPointCounter = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTeam]-1
				IF iRule < FMMC_MAX_RULES
					IF IS_SPAWN_POINT_A_VALID_RESPAWN_POINT(iTeamSpawnPointCounter, iTeam, FALSE)
						FLOAT fRadius = GET_ACTIVATION_RADIUS_RESPAWN_POINT_FROM_INDEX(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iActivationRadius)
						
						IF fRadius > 0
							// If the respawn point is already active, then do not run the below script. We don't want to set off the flow of removing respawn points and such, if we're not activating one.
						
							VECTOR vPos = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].vPos									
							FLOAT fDist = VDIST2(vPlayerCoords, vPos)
							
							//PRINTLN("[LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - iTeamSpawnPointCounter: ", iTeamSpawnPointCounter, " We have a respawn point that activates on a radial distance to it,  fRadius: ", fRadius, " vPos: ",  vPos, "  and vPlayerCoords: ", vPlayerCoords, " fDist: ", fDist)
							
							// If we are within the distance check.
							IF fDist < POW(fRadius, 2)
								IF NOT FMMC_IS_LONG_BIT_SET(iBSRespawnActiveFromRadius[iTeam], iTeamSpawnPointCounter)
									PRINTLN("[MC_CustomSpawning] - [LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - We are setting iTeamSpawnPointCounter: ", iTeamSpawnPointCounter, " To now be active..")										
									FMMC_SET_LONG_BIT(iBSRespawnActiveFromRadius[iTeam], iTeamSpawnPointCounter)
									FMMC_CLEAR_LONG_BIT(iBSRespawnDeactivated[iTeam], iTeamSpawnPointCounter)
									
									ACTIVATE_LINKED_CUSTOM_RESPAWN_POINTS(iTeam, iTeamSpawnPointCounter)
									bMustActivateSpawnPoint = TRUE
									
									// If this point is set in the creator to deactivate other points.
									IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iSpawnBitset, ci_SpawnBS_Deactivate_All_Others_When_Activated)
										PRINTLN("[MC_CustomSpawning] - [LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - iTeamSpawnPointCounter: ", iTeamSpawnPointCounter, " spawn point wants to deactivate others: ci_SpawnBS_Deactivate_All_Others_When_Activated")
										iIndexActive = iTeamSpawnPointCounter
										bMustDeactivateAllOtherPoints = TRUE
										bMustActivateSpawnPoint = TRUE
									ENDIF
								ENDIF
							ELSE
								IF FMMC_IS_LONG_BIT_SET(iBSRespawnActiveFromRadius[iTeam], iTeamSpawnPointCounter)
								AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iSpawnBitSet, ci_SpawnBS_When_Leaving_Activation_Radius_Unflag_Disabled_Points)
									PRINTLN("[MC_CustomSpawning] - [LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - iTeamSpawnPointCounter: ", iTeamSpawnPointCounter, " This spawn point wants to re-activate deactivated spawn points: ci_SpawnBS_When_Leaving_Activation_Radius_Unflag_Disabled_Points")
									bMustUnflagValidDisabledPoints = TRUE
									bMustActivateSpawnPoint = TRUE
								ENDIF
								
								IF FMMC_IS_LONG_BIT_SET(iBSRespawnActiveFromRadius[iTeam], iTeamSpawnPointCounter)
								AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iSpawnBitSet, ci_SpawnBS_Only_Active_While_In_Area)
									PRINTLN("[MC_CustomSpawning] - [LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - iTeamSpawnPointCounter: ", iTeamSpawnPointCounter, " This spawn point is only active while we're in the area, removing Active From Radius Flag... ci_SpawnBS_Only_Active_While_In_Area")
									bMustActivateSpawnPoint = TRUE
									FMMC_CLEAR_LONG_BIT(iBSRespawnActiveFromRadius[iTeam], iTeamSpawnPointCounter)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						// Make sure previous points are cleared up	
						FMMC_CLEAR_LONG_BIT(iBSRespawnActiveFromRadius[iTeam], iTeamSpawnPointCounter)
						FMMC_CLEAR_LONG_BIT(iBSRespawnDeactivated[iTeam], iTeamSpawnPointCounter)
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
		// Remotely add points. !!!!! Has to be separate from the above chunk as we need to check other teams points !!!!!
		IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_CAN_TEAMS_ACTIVATE_OTHER_TEAMS_RESPAWN_POINTS)		
			INT iTeamLoop = 0
			FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
				// Check to see if the player is close enough to have activated a set of respawn points.
				FOR iTeamSpawnPointCounter = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTeamLoop]-1
					IF MC_ServerBD_4.iCurrentHighestPriority[iTeamLoop] < FMMC_MAX_RULES
						IF IS_SPAWN_POINT_A_VALID_RESPAWN_POINT(iTeamSpawnPointCounter, iTeamLoop, FALSE)
							FLOAT fRadius = GET_ACTIVATION_RADIUS_RESPAWN_POINT_FROM_INDEX(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeamLoop][iTeamSpawnPointCounter].iActivationRadius)

							IF fRadius > 0
								// If the respawn point is already active, then do not run the below script. We don't want to set off the flow of removing respawn points and such, if we're not activating one.									
								VECTOR vPos = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeamLoop][iTeamSpawnPointCounter].vPos
								
								// If we are within the distance check.
								IF VDIST2(vPlayerCoords, vPos) < POW(fRadius, 2)										
									// Send out event iActivationTeam
									IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeamLoop][iTeamSpawnPointCounter].iActivationTeam > -2
										IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeamLoop][iTeamSpawnPointCounter].iActivationTeam = iTeam
										OR g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeamLoop][iTeamSpawnPointCounter].iActivationTeam = -1
											IF NOT FMMC_IS_LONG_BIT_SET(iBSRespawnActiveFromRadiusRemoteToggle[iTeamLoop], iTeamSpawnPointCounter)
												BROADCAST_FMMC_REMOTELY_ADD_RESPAWN_POINTS(iTeamLoop, iTeamSpawnPointCounter, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeamLoop][iTeamSpawnPointCounter].iSpawnBitset, ci_SpawnBS_Deactivate_All_Others_When_Activated))												
												FMMC_SET_LONG_BIT(iBSRespawnActiveFromRadiusRemoteToggle[iTeamLoop], iTeamSpawnPointCounter)
											ENDIF
										ENDIF
									ENDIF
								ELSE					
									FMMC_CLEAR_LONG_BIT(iBSRespawnActiveFromRadiusRemoteToggle[iTeamLoop], iTeamSpawnPointCounter)
								ENDIF
							ENDIF
						ELSE
							// Make sure previous points are cleared up							
							FMMC_CLEAR_LONG_BIT(iBSRespawnActiveFromRadius[iTeamLoop], iTeamSpawnPointCounter)
							FMMC_CLEAR_LONG_BIT(iBSRespawnDeactivated[iTeamLoop], iTeamSpawnPointCounter)
							FMMC_CLEAR_LONG_BIT(iBSRespawnActiveFromRadiusRemoteToggle[iTeamLoop], iTeamSpawnPointCounter)
						ENDIF
					ENDIF
				ENDFOR
			ENDFOR
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_CAN_TEAMS_ACTIVATE_OTHER_TEAMS_RESPAWN_POINTS)
		OR iRespawnPointIndexToKeep > -1
		OR IS_BIT_SET(iLocalBoolCheck28, LBOOL28_OTHER_TEAM_ACTIVATED_MY_RESPAWN_POINTS)
			
			// If activated from a different team..
			IF iRespawnPointIndexToKeep > -1
				PRINTLN("[MC_CustomSpawning] - [LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - iRespawnPointIndexToKeep: ", iRespawnPointIndexToKeep)
				iIndexActive = iRespawnPointIndexToKeep
				bMustDeactivateAllOtherPoints = TRUE
			ENDIF
			
			// When leaving the trigger area of some spawn points they can unflag the points that were disabled by their activation.
			IF bMustUnflagValidDisabledPoints
				FOR iTeamSpawnPointCounter = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTeam]-1
					IF IS_SPAWN_POINT_A_VALID_RESPAWN_POINT(iTeamSpawnPointCounter, DEFAULT, FALSE)
						#IF IS_DEBUG_BUILD
						IF FMMC_IS_LONG_BIT_SET(iBSRespawnDeactivated[iTeam], iTeamSpawnPointCounter)			
							PRINTLN("[MC_CustomSpawning] - [LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - Unflagging Deactivation for Spawn Point: ", iTeamSpawnPointCounter, " For team: ", MC_playerBD[iLocalPart].iteam)
						ENDIF
						#ENDIF
						FMMC_CLEAR_LONG_BIT(iBSRespawnDeactivated[iTeam], iTeamSpawnPointCounter)
					ENDIF
				ENDFOR
			ENDIF
			
			// A Respawn Point we have reached wants to deactivate all other respawn points. Loop through and deactivate them.
			IF bMustDeactivateAllOtherPoints
				PRINTLN("[MC_CustomSpawning] - [LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - We are going to disable all the -OTHER- Respawn points for this team: ", MC_playerBD[iLocalPart].iteam, " and Rule: ", MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam])
				
				FOR iTeamSpawnPointCounter = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[MC_playerBD[iLocalPart].iteam]-1-1					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][iTeamSpawnPointCounter].iSpawnBitset, ci_SpawnBS_Cannot_Be_Deactived_Ever)
						IF IS_SPAWN_POINT_A_VALID_RESPAWN_POINT(iTeamSpawnPointCounter, DEFAULT, FALSE)
							IF iIndexActive != iTeamSpawnPointCounter
							AND NOT IS_CUSTOM_RESPAWN_POINT_A_LINKED_POINT(iTeam, iTeamSpawnPointCounter)
								// Putting this in a check to avoid log print spam
								#IF IS_DEBUG_BUILD
								IF NOT FMMC_IS_LONG_BIT_SET(iBSRespawnDeactivated[iTeam], iTeamSpawnPointCounter)
								OR FMMC_IS_LONG_BIT_SET(iBSRespawnActiveFromRadius[iTeam], iTeamSpawnPointCounter)
									PRINTLN("[MC_CustomSpawning] - [LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - Deactivating: ", iTeamSpawnPointCounter, " For team: ", MC_playerBD[iLocalPart].iteam)
								ENDIF
								#ENDIF
								FMMC_SET_LONG_BIT(iBSRespawnDeactivated[iTeam], iTeamSpawnPointCounter)
								FMMC_CLEAR_LONG_BIT(iBSRespawnActiveFromRadius[iTeam], iTeamSpawnPointCounter)
							ELSE
								PRINTLN("[MC_CustomSpawning] - [LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - Don't Deactivate the newly activated point: ", iTeamSpawnPointCounter, " For team: ", MC_playerBD[iLocalPart].iteam)
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
			
			// Add points to the Freemode Respawning System.
			IF bMustActivateSpawnPoint
			OR IS_BIT_SET(iLocalBoolCheck28, LBOOL28_OTHER_TEAM_ACTIVATED_MY_RESPAWN_POINTS)
				PRINTLN("[MC_CustomSpawning] - [LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - Calling ADD_ALL_CUSTOM_SPAWN_POINTS to reinitialize the respawn points in Neils system. bMustActivateSpawnPoint")
				
				ADD_ALL_CUSTOM_SPAWN_POINTS()
				
				iRespawnPointIndexToKeep = -1
				CLEAR_BIT(iLocalBoolCheck28, LBOOL28_OTHER_TEAM_ACTIVATED_MY_RESPAWN_POINTS)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SET_UP_CUSTOM_RESPAWN_POINTS()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		
		BOOL bShouldCheckForNewPoints
		
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		OR IS_BIT_SET(iLocalBoolCheck34, LBOOL34_SPAWN_GROUPS_CHANGED_REVALIDATE_SPAWN_POINTS)
			
			CLEAR_BIT(iLocalBoolCheck34, LBOOL34_SPAWN_GROUPS_CHANGED_REVALIDATE_SPAWN_POINTS)
			
			IF NOT IS_BIT_SET(iLocalBoolCheck, PLAYER_DEATH_TOGGLE)
				bShouldCheckForNewPoints = TRUE
				CLEAR_BIT(iLocalBoolCheck33, LBOOL33_DELAYED_ADD_SPAWN_POINTS)				
				PRINTLN("[MC_CustomSpawning] - [LM][USE_CUSTOM_SPAWN_POINTS] New Rule - Checking for new Custom Spawn Points")
			ELSE
				IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_DELAYED_ADD_SPAWN_POINTS)					
					SET_BIT(iLocalBoolCheck33, LBOOL33_DELAYED_ADD_SPAWN_POINTS)
					PRINTLN("[MC_CustomSpawning] - [LM][USE_CUSTOM_SPAWN_POINTS] New Rule - We are dead so delayed checking for new Custom Spawn Points")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_DELAYED_ADD_SPAWN_POINTS)
			IF NOT IS_BIT_SET(iLocalBoolCheck, PLAYER_DEATH_TOGGLE)
				CLEAR_BIT(iLocalBoolCheck33, LBOOL33_DELAYED_ADD_SPAWN_POINTS)
				bShouldCheckForNewPoints = TRUE
				PRINTLN("[MC_CustomSpawning] - [LM][USE_CUSTOM_SPAWN_POINTS] New Rule - We are alive, check for new Custom Spawn Points.")
			ELSE
				PRINTLN("[MC_CustomSpawning] - [LM][USE_CUSTOM_SPAWN_POINTS] New Rule - Waiting until we are alive before checking for new Custom Spawn Points.")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck34, LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES)
			IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_USING_CUSTOM_RESPAWN_POINTS)
				PRINTLN("[MC_CustomSpawning] - [LM][USE_CUSTOM_SPAWN_POINTS] LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES is set, and we're currently using them. Flagging to check new points.")
				bShouldCheckForNewPoints = TRUE
				SET_BIT(iLocalBoolCheck34, LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES_TRIGGERED)
				CLEAR_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_USE_CUSTOM_SPAWN_POINTS)
			ELSE
				PRINTLN("[MC_CustomSpawning] - [LM][USE_CUSTOM_SPAWN_POINTS] LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES is set")
			ENDIF
			
		ELIF IS_BIT_SET(iLocalBoolCheck34, LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES_TRIGGERED)
			IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_USING_CUSTOM_RESPAWN_POINTS)
				PRINTLN("[MC_CustomSpawning] - [LM][USE_CUSTOM_SPAWN_POINTS] LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES is set, and we're currently using them. Flagging to check new points.")
				bShouldCheckForNewPoints = TRUE
				CLEAR_BIT(iLocalBoolCheck34, LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES_TRIGGERED)
				CACHE__SHOULD_USE_CUSTOM_SPAWN_POINTS()
			ELSE
				PRINTLN("[MC_CustomSpawning] - [LM][USE_CUSTOM_SPAWN_POINTS] LBOOL34_CUSTOM_SPAWN_POINTS_DISABLED_BY_ZONES is set")
			ENDIF
			
		ENDIF		
		
		// Create Spawn Points when the Rule Changes.
		// Checks to see if there are any Valid custom Respawn Points on the Rule.
		IF bShouldCheckForNewPoints
			IF SHOULD_USE_CUSTOM_SPAWN_POINTS()	
				PRINTLN("[MC_CustomSpawning] - [USE_CUSTOM_SPAWN_POINTS] calling from main ADD_ALL_CUSTOM_SPAWN_POINTS")
				ADD_ALL_CUSTOM_SPAWN_POINTS()
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_USING_CUSTOM_RESPAWN_POINTS)
					PRINTLN("[MC_CustomSpawning] - [USE_CUSTOM_SPAWN_POINTS] We are using Custom Respawn Points, but they are not valid on this Rule: ", iRule, " Setting should clear")
					SET_BIT(iLocalBoolCheck21, LBOOL21_SHOULD_CLEAR_CUSTOM_RESPAWN_POINTS)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_USING_CUSTOM_RESPAWN_POINTS)
		AND IS_BIT_SET(iLocalBoolCheck21, LBOOL21_SHOULD_CLEAR_CUSTOM_RESPAWN_POINTS)
			IF bLocalPlayerPedOK
			AND (NOT IS_PLAYER_RESPAWNING(LocalPlayer) OR IS_FAKE_MULTIPLAYER_MODE_SET(TRUE))
				PRINTLN("[MC_CustomSpawning] - [LM][USE_CUSTOM_SPAWN_POINTS] Removing custom respawn points")
				CLEAR_CUSTOM_SPAWN_POINTS()
				USE_CUSTOM_SPAWN_POINTS(FALSE)
				CLEAR_BIT(iLocalBoolCheck21, LBOOL21_USING_CUSTOM_RESPAWN_POINTS)
				CLEAR_BIT(iLocalBoolCheck21, LBOOL21_SHOULD_CLEAR_CUSTOM_RESPAWN_POINTS)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: General Respawn Processing.																			
// ##### Description: Shared respawn and manual respawn functionality / processing.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_PLAYER_RESPAWN_SETTINGS(INT iPlayerTeam)
	
	IF iPlayerTeam = -1
		PRINTLN("PROCESS_PLAYER_RESPAWN_SETTINGS - iPlayerTeam = -1, couldn't apply settings!!")
		SCRIPT_ASSERT("PROCESS_PLAYER_RESPAWN_SETTINGS - iPlayerTeam = -1, couldn't apply settings!!")
		   
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_IGNORE, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "iPlayerTeam = -1, couldn't apply settings!!")	
		#ENDIF

		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_VEHICLE_TARGETTING_RETICULE)
		USE_VEHICLE_TARGETING_RETICULE(TRUE)
		PRINTLN("[spawning] - PROCESS_PLAYER_RESPAWN_SETTINGS - USE_VEHICLE_TARGETING_RETICULE(TRUE)")
	ENDIF
	
	
ENDPROC

PROC PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS(VEHICLE_INDEX vehIndex, BOOL bInitialSpawn, INT iPlayerTeam, BOOL bTeamVehicle)
	
	IF iPlayerTeam = -1
		PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - iPlayerTeam = -1, couldn't apply settings!!")
		SCRIPT_ASSERT("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - iPlayerTeam = -1, couldn't apply settings!!")
		   
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_IGNORE, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "iPlayerTeam = -1, couldn't apply settings!!")	
		#ENDIF

		EXIT
	ENDIF
	
	IF IS_VEHICLE_FUCKED_MP(vehIndex)
		PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Vehicle was fucked on respawn, couldn't apply settings!!")
		SCRIPT_ASSERT("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Vehicle was fucked on respawn, couldn't apply settings!!")
		   
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_IGNORE, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "Vehicle was fucked on respawn, couldn't apply settings!!")	
		#ENDIF

		EXIT
	ENDIF
	
	BOOL bHasControl = NETWORK_HAS_CONTROL_OF_ENTITY(vehIndex)
	MODEL_NAMES mnVehicle = GET_ENTITY_MODEL(vehIndex)
		
	// For modes which require unsynced network natives to be called on the vehicle we are in which we don't have control over, move them above this exit.
	IF NOT bHasControl
		EXIT
	ENDIF
	
	SET_VEHICLE_ENGINE_ON(vehIndex, TRUE, TRUE)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciDISABLE_VEHICLE_KERS)
		PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS -  Disabling vehicle KERS - ciDISABLE_VEHICLE_KERS")
		BLOCK_VEHICLE_KERS(vehIndex, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_Monster3IncreasedCrushDamage)
		IF IS_VEHICLE_A_MONSTER(mnVehicle)
			PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Calling SET_INCREASE_WHEEL_CRUSH_DAMAGE - ciOptionsBS22_Monster3IncreasedCrushDamage")
			SET_INCREASE_WHEEL_CRUSH_DAMAGE(vehIndex, TRUE)			
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_HIGH_SPEED_EDGE_FALL_DETECTION)
		PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Enabling vehicle HIGH SPEED EDGE FALL DETECTION - ciOptionsBS19_ENABLE_HIGH_SPEED_EDGE_FALL_DETECTION")
		SET_NETWORK_ENABLE_HIGH_SPEED_EDGE_FALL_DETECTION(vehIndex, TRUE)		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciVEHICLE_EXPLOSION_PROOF)
		SET_ENTITY_PROOFS(vehIndex, FALSE, FALSE, TRUE, FALSE, FALSE)
		PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - EXPLODE ON OUT OF BOUNDS - EXPLOSION PROOF SWITCHED ON")
	ENDIF	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciDISABLE_RUINER_ROCKETS)
		IF IS_VEHICLE_MODEL(vehIndex, RUINER2)
			// This doesn't work 
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET, vehIndex, LocalPlayerPed)
			SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_VEHICLE_RUINER_BULLET)
			SET_VEHICLE_WEAPON_RESTRICTED_AMMO(vehIndex, 1, -1)
			PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - RUINER ROCKETS DISABLED")
		ELSE
			PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - NOT A RUINER!")
		ENDIF
	ELSE
		PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - RUINER ROCKETS ALLOWED")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].iTeamBitset, ciBS_RESPAWN_VEHICLE_BULLETPROOF_TIRES)
		PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - setting SET_VEHICLE_TYRES_CAN_BURST to false - ciBS_RESPAWN_VEHICLE_BULLETPROOF_TIRES")
		SET_VEHICLE_TYRES_CAN_BURST(vehIndex, FALSE)
	ENDIF

	//Check if we need to blip this vehicle.
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].iTeamBitset, ciBS_BLIP_RESPAWN_VEHICLES)
		PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Resetting respawn vehicle blip - ciBS_BLIP_RESPAWN_VEHICLES")
		IF DOES_BLIP_EXIST(biSpawnVehicle)
			REMOVE_BLIP(biSpawnVehicle)
		ENDIF
		vehSpawnVehicle = vehIndex
		bManageSpawnVehicleBlip = TRUE
	ENDIF
		
	IF SHOULD_SET_PLAYER_VEHICLE_DOORS_LOCKED(iPlayerTeam)
		INT iTeamLoop
		FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			IF iTeamLoop != iPlayerTeam
				SET_VEHICLE_DOORS_LOCKED_FOR_TEAM(vehIndex, iTeamLoop, TRUE )
				SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehIndex, TRUE)
				SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehIndex, TRUE)
				PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Locking respawn vehicle doors for team ", iTeamLoop)
			ENDIF
		ENDFOR
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].iTeamBitset2, ciBS2_ALLOW_SPAWN_VEHICLE_TO_TARGET_OBJECTS)
		SET_VEHICLE_WEAPON_CAN_TARGET_OBJECTS(vehIndex, TRUE)
		PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Setting vehicle to be able to target objects with homing weapons.")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_PlayerVehiclesExplosionResistant)
		SET_DISABLE_VEHICLE_EXPLOSIONS_DAMAGE(TRUE)
		PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Calling SET_DISABLE_VEHICLE_EXPLOSIONS_DAMAGE - SPAWN_PLAYER_IN_TEAM_VEHICLE")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_WEAPON_BULLETPROOF_PLAYER)
		PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - setting bulletproof")
		SET_ENTITY_PROOFS(LocalPlayerPed, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE)
	ENDIF
	
	IF mnVehicle = DELUXO
		IF IS_ENTITY_IN_AIR(vehIndex)
		AND GET_ENTITY_HEIGHT_ABOVE_GROUND(vehIndex) > 5
			SET_SPECIAL_FLIGHT_MODE_ALLOWED(vehIndex, TRUE)
			SET_DISABLE_HOVER_MODE_FLIGHT(vehIndex, FALSE)
			SET_SPECIAL_FLIGHT_MODE_TARGET_RATIO(vehIndex, 1)
			PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Deluxo in Air. Spawning with wheels up. (3)")
		ENDIF
	ENDIF
	
	// For Vehicles created at mission start or during the respawn procedures that are set up on the LEFT hand menu or RULE menu.
	IF bTeamVehicle
		IF (g_FMMC_STRUCT.iTeamVehicleModCountermeasure_AmmoPerLife[iPlayerTeam] > 0)
			IF DOES_ENTITY_EXIST(vehIndex)
				SET_VEHICLE_COUNTERMEASURE_AMMO(vehIndex, g_FMMC_STRUCT.iTeamVehicleModCountermeasure_AmmoPerLife[iPlayerTeam])
				PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Overriding ammo for plane! SPAWN_PLAYER_IN_TEAM_VEHICLE")
			ENDIF
		ENDIF
		
		IF mnVehicle = DELUXO
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].iTeamBitSet3, ciBS3_TEAM_VEH_DELUXO_HOVER_MODE)
				SET_SPECIAL_FLIGHT_MODE_RATIO(vehIndex, 1)
				SET_SPECIAL_FLIGHT_MODE_TARGET_RATIO(vehIndex, 1)
				PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Deluxo start in hover.")
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT.iVehicleColour[iPlayerTeam] > -1
			APPLY_TEAM_COLOUR_TO_VEHICLE(vehIndex, iPlayerTeam)
		ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_AllowTeamVehicleColourFromCorona)
			PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Calling SET_UP_PLAYERS_VEHICLE_COLOURS")
			SET_UP_PLAYERS_VEHICLE_COLOURS(vehIndex)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].iTeamBitset, ciBS_TEAM_DISABLE_TEAM_VEHICLE_LIVERIES)
			IF GET_NUM_VEHICLE_MODS(vehIndex, MOD_LIVERY) > 0
				SET_VEHICLE_MOD(vehIndex, MOD_LIVERY, -1, FALSE)
				PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Calling SET_VEHICLE_MOD(vehIndex,MOD_LIVERY,-1,FALSE)")
			ELIF GET_VEHICLE_LIVERY_COUNT(vehIndex) > 0
				SET_VEHICLE_LIVERY(vehIndex, 0)
				PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Calling SET_VEHICLE_LIVERY(vehToSpawnPlayerIn,0)")
			ELSE
				PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - ciBS_TEAM_DISABLE_TEAM_VEHICLE_LIVERIES BUT THE VEHICLE DOESN'T HAVE LIVERY ")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciMODDED_CORONA_VEHS)
			IF mnVehicle = BANSHEE2
			OR mnVehicle = SULTANRS
				INT iPreset = 0
				FMMC_SET_VEH_MOD_PRESET(vehIndex, mnVehicle, iPreset)
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT.iTeamHorn[iPlayerTeam] > -1
			IF g_FMMC_STRUCT.iTeamHorn[iPlayerTeam] != GET_VEHICLE_MOD(vehIndex, MOD_HORN)
				PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Applying horn: ", GET_HORN_LABEL_FROM_MOD_INDEX(g_FMMC_STRUCT.iTeamHorn[iPlayerTeam]))
				SET_VEHICLE_MOD(vehIndex, MOD_HORN, g_FMMC_STRUCT.iTeamHorn[iPlayerTeam], FALSE)
			ENDIF
		ENDIF
	
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].iTeamBitset3, ciBS3_TEAM_VEHICLE_RESIST_EXPLOSION)
			SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehIndex, FALSE)
			PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Setting team vehicles to resist explosions")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_FORCE_CONVERT_ROOF_DOWN)
			PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Forcing corvertible roof down - ciOptionsBS19_FORCE_CONVERT_ROOF_DOWN")
			IF IS_VEHICLE_A_CONVERTIBLE(vehIndex)
				LOWER_CONVERTIBLE_ROOF(vehIndex, TRUE)
			ENDIF						
			DISABLE_VEHICLE_ROOFS(vehIndex, mnVehicle)
		ENDIF
		
		//Restrict teams from entering other's vehicles
		INT i
		FOR i = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			IF IS_BIT_SET(g_FMMC_STRUCT.iRestrictTeamVehicleEntry[iPlayerTeam], i)
				PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Locking vehicle doors for team: ", i)
				SET_VEHICLE_DOORS_LOCKED_FOR_TEAM(vehIndex, i, TRUE )
			ENDIF
		ENDFOR
		
		INT ihealth = GET_FMMC_PLAYER_VEHICLE_HEALTH_FOR_TEAM(iPlayerTeam)
		FLOAT fhealthEngine = TO_FLOAT(ihealth)
		FLOAT fBodyHealth = GET_FMMC_PLAYER_VEHICLE_BODY_HEALTH_FOR_TEAM(iPlayerTeam)

		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].iTeamBitSet3, ciBS3_TEAM_VEHICLE_USE_ONLY_HEALTH_BODY)
			fhealthEngine = 5000.0
			SET_DISABLE_VEHICLE_PETROL_TANK_DAMAGE(vehIndex, TRUE)
			SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(vehIndex, TRUE)
			PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - ciBS3_TEAM_VEHICLE_USE_ONLY_HEALTH_BODY PETROL_TANK_DAMAGE and PETROL_TANK_FIRES Disabled")
		ENDIF
		
		IF ihealth >= 3000 
			SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehIndex, FALSE)
		ENDIF
		
		PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - SET_ENTITY_HEALTH : ", ihealth)
		SET_ENTITY_HEALTH(vehIndex, ihealth)
		PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - SET_VEHICLE_BODY_HEALTH : ", fBodyHealth)
		SET_VEHICLE_BODY_HEALTH(vehIndex, fBodyHealth)
		PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - SET_VEHICLE_ENGINE_HEALTH : ", fhealthEngine)
		SET_VEHICLE_ENGINE_HEALTH(vehIndex, fhealthEngine)
		SET_VEHICLE_PETROL_TANK_HEALTH(vehIndex, fhealthEngine)
		SET_VEHICLE_WEAPON_DAMAGE_SCALE(vehIndex, GET_FMMC_PLAYER_VEHICLE_WEAPON_DAMAGE_SCALE_FOR_TEAM(iPlayerTeam))
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].iVehicleHealthSwap[MC_ServerBD_4.iCurrentHighestPriority[iPlayerTeam]] > -1
			PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Setting respawn vehicle health from rule/team")
			SET_VEHICLE_HEALTH_FROM_RULE_AND_TEAM(iPlayerTeam, MC_ServerBD_4.iCurrentHighestPriority[iPlayerTeam], vehIndex)
		ENDIF							
		
		IF g_FMMC_STRUCT.fEnvEffScale[iPlayerTeam] != 1.0
			SET_VEHICLE_ENVEFF_SCALE(vehIndex, g_FMMC_STRUCT.fEnvEffScale[iPlayerTeam])
			PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - Setting Env Eff Scale to: ", g_FMMC_STRUCT.fEnvEffScale[iPlayerTeam])
		ENDIF

		IF MC_ServerBD_4.iCurrentHighestPriority[iPlayerTeam] < FMMC_MAX_RULES
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].iRuleBitsetTen[MC_ServerBD_4.iCurrentHighestPriority[iPlayerTeam]], ciBS_RULE10_VEHICLE_SWAP_MODS)
			PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] Applying Vehicle Swap Mods")
			SET_VEHICLE_WEAPON_MODS_FROM_RULE_AND_TEAM(iPlayerTeam, MC_ServerBD_4.iCurrentHighestPriority[iPlayerTeam], vehIndex)
		ELSE						
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_AllowTeamVehicleModPreset)
				APPLY_ARENA_SETTINGS_TO_PLAYER_VEH(vehIndex, IS_BIT_SET(iLocalBoolCheck28, LBOOL28_USING_PV))
			ENDIF
			
			PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Neon Colour Creator: ", g_FMMC_STRUCT.iVehicleNeonColour[iPlayerTeam])
			PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Neon Colour Converted: ", GET_VEHICLE_COLOR_FROM_SELECTION(g_FMMC_STRUCT.iVehicleNeonColour[iPlayerTeam]))
			
			SET_VEHICLE_WEAPON_MODS(vehIndex, g_FMMC_STRUCT.iTeamVehicleModTurret[iPlayerTeam], g_FMMC_STRUCT.iTeamVehicleModArmour[iPlayerTeam],0,-1, g_FMMC_STRUCT.iTeamVehicleModAirCountermeasure[iPlayerTeam], g_FMMC_STRUCT.iTeamVehicleModExhaust[iPlayerTeam], g_FMMC_STRUCT.iTeamVehicleModBombBay[iPlayerTeam], g_FMMC_STRUCT.iTeamVehicleModSpoiler[iPlayerTeam], iPlayerTeam, DEFAULT, GET_VEHICLE_COLOR_FROM_SELECTION(g_FMMC_STRUCT.iVehicleNeonColour[iPlayerTeam]))
			
			INT iModPreset = g_FMMC_STRUCT.iTeamVehicleModPreset[iPlayerTeam]
			
			IF iModPreset != -1
				PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - iModPreset: ", iModPreset)
				FMMC_SET_VEH_MOD_PRESET(vehIndex, mnVehicle, iModPreset, GET_VEHICLE_COLOR_FROM_SELECTION(g_FMMC_STRUCT.iVehicleNeonColour[iPlayerTeam]))
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_Struct.iTeamVehicleBitSet, ciFMMC_TeamVehBS_DISABLE_HOMING_MISSILE)
			SET_PLAYER_HOMING_DISABLED_FOR_ALL_VEHICLE_WEAPONS(LocalPlayer, TRUE)
			SET_BIT(iLocalBoolCheck25, LBOOL25_VEHICLE_WEAPONS_HOMING_DISABLED)
			IF DOES_ENTITY_EXIST(vehIndex)
				IF mnVehicle = HUNTER
					DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_HUNTER_MISSILE, vehIndex, LocalPlayerPed)
					PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - WEAPONTYPE_DLC_VEHICLE_HUNTER_MISSILE")
				ENDIF
			ENDIF
			
			PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Making respawn vehicle immune to homing lock on")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_Struct.iTeamVehicleBitSet, ciFMMC_TeamVehVS_LOCK_FOR_ALL_OTHERS)
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehIndex, TRUE)
			SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(vehIndex, localPlayer, FALSE)
		ENDIF
		
		IF IS_VEHICLE_SUITABLE_FOR_CHAFF(vehIndex)
		AND g_FMMC_STRUCT.iTeamVehicleModAirCountermeasure[iPlayerTeam] > -1
			SET_VEHICLE_COUNTERMEASURE_AMMO(vehIndex, 50)
			PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Setting Countermeasure ammo to 50")
		ENDIF
		
		IF (g_FMMC_STRUCT.iTeamVehicleModCountermeasure_AmmoPerLife[iPlayerTeam] > 0)
			IF DOES_ENTITY_EXIST(vehIndex)
				SET_VEHICLE_COUNTERMEASURE_AMMO(vehIndex, g_FMMC_STRUCT.iTeamVehicleModCountermeasure_AmmoPerLife[iPlayerTeam])
				PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - SECOND TIME Overriding ammo for plane! SPAWN_PLAYER_IN_TEAM_VEHICLE")
			ENDIF
		ENDIF
			
		IF IS_BIT_SET(g_FMMC_Struct.iTeamVehicleBitSet, ciFMMC_TeamVehBS_DISABLE_TEAM_RESPAWN_VEHICLE_SPACE_ROCKETS)
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_VEHICLE_SPACE_ROCKET, vehIndex, LocalPlayerPed)
			SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
			PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Disabling space rockets")
		ENDIF
		
		
	ENDIF
	
	// Initial Spawn Section #######
	IF bInitialSpawn
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAERIAL_VEHICLE_SPAWN)
			IF IS_THIS_MODEL_A_PLANE(mnVehicle)
				CONTROL_LANDING_GEAR(vehIndex, LGC_RETRACT_INSTANT)
			ENDIF
			IF IS_THIS_MODEL_A_HELI(mnVehicle)
				SET_HELI_BLADES_FULL_SPEED(vehIndex)
			ENDIF
		ENDIF
	
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_RESPAWN_HYDRAS_IN_FLIGHT_MODE)
		AND NOT IS_THIS_A_QUICK_RESTART_JOB()
			IF GET_VEHICLE_HAS_LANDING_GEAR(vehIndex)
				CONTROL_LANDING_GEAR(vehIndex, LGC_RETRACT_INSTANT) 
			ENDIF
			
			IF IS_SKYSWOOP_AT_GROUND()
				IF GET_MC_CLIENT_GAME_STATE(iLocalPart) > GAME_STATE_INTRO_CUTSCENE
					IF bLocalPlayerPedOK
						IF IS_PED_IN_ANY_PLANE(localPlayerPed)
						OR IS_PED_IN_ANY_HELI(localPlayerPed)
							
							IF IS_PED_IN_ANY_HELI(localPlayerPed)
								UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS(1.0)
								PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Calling SET_VEHICLE_FORWARD_SPEED with 1.0")
							ELSE
								UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS(60.0)
								PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Calling SET_VEHICLE_FORWARD_SPEED with 60.0")
							ENDIF
							
							IF IS_THIS_MODEL_A_HELI(mnVehicle)
								SET_HELI_BLADES_FULL_SPEED(vehIndex)
							ENDIF

							IF IS_VEHICLE_MODEL(vehIndex, HYDRA)
							OR IS_VEHICLE_MODEL(vehIndex, TULA)
								SET_VEHICLE_FLIGHT_NOZZLE_POSITION_IMMEDIATE(vehIndex, 0.0)
								PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Calling SET_VEHICLE_FLIGHT_NOZZLE_POSITION_IMMEDIATE with 0.0 on our Hydra.")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	
	// Respawn Section #######
	ELSE
		
		IF GET_SEAT_PED_IS_IN(LocalPlayerPed) = VS_DRIVER
		AND NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SHOULD_TURN_RADIO_BACK_ON_AFTER_ROUND_RESTART)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_KEEP_RADIO_STATION_ON_RESPAWN)
				IF iManRespawnLastRadioStation != 255
					SET_RADIO_TO_STATION_INDEX(iManRespawnLastRadioStation)
					PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Setting new car radio station to previous station: ", iManRespawnLastRadioStation )
				ELSE
					PRINTLN("PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS - Setting radio station OFF")
					SET_VEH_RADIO_STATION(vehIndex, "OFF")
				ENDIF
			ENDIF
		ENDIF

		IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_SIREN_ACTIVE_MANUAL_RESPAWN)
			PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Setting siren back on after death")
			SET_VEHICLE_SIREN(vehIndex, TRUE)
			CLEAR_BIT(iLocalBoolCheck28, LBOOL28_SIREN_ACTIVE_MANUAL_RESPAWN)
		ENDIF
		
		IF g_mnMyRaceModel = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES]- Setting Vehicle colour", g_FMMC_STRUCT.iVehicleColour[iPlayerTeam])
			
			IF g_FMMC_STRUCT.iVehicleColour[iPlayerTeam] > -1
				sLastVehicleInformation.iVehicleColour1 = GET_VEHICLE_COLOR_FROM_SELECTION(g_FMMC_STRUCT.iVehicleColour[iPlayerTeam])
				sLastVehicleInformation.iVehicleColour2 = sLastVehicleInformation.iVehicleColour1
				PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Using Team Respawn Colour of ", sLastVehicleInformation.iVehicleColour1)
				
				IF g_FMMC_STRUCT.iVehicleSecondaryColour[iPlayerTeam] > -1
					sLastVehicleInformation.iVehicleColour2 = GET_VEHICLE_COLOR_FROM_SELECTION( g_FMMC_STRUCT.iVehicleSecondaryColour[iPlayerTeam])
					PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - VEHICLE RESPAWN SECONDARY COLOUR - iColour:", sLastVehicleInformation.iVehicleColour2)
				ENDIF			
			ENDIF
			
			IF g_FMMC_STRUCT.iVehicleColour[iPlayerTeam] > -1
				
				PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Setting Vehicle colour LastVehicleInformation.iVehicleColour1: ",sLastVehicleInformation.iVehicleColour1)
				
				SET_VEHICLE_COLOURS(vehIndex, sLastVehicleInformation.iVehicleColour1, sLastVehicleInformation.iVehicleColour2)
				SET_VEHICLE_EXTRA_COLOURS(vehIndex, sLastVehicleInformation.iVehicleColour1, sLastVehicleInformation.iVehicleColour2)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_RESPAWN_HYDRAS_IN_FLIGHT_MODE)
				IF bLocalPlayerPedOK
					IF IS_PED_IN_ANY_PLANE(localPlayerPed)
					OR IS_PED_IN_ANY_HELI(localPlayerPed)
						SET_VEHICLE_FORWARD_SPEED(vehIndex, 30.0)
						
						IF GET_VEHICLE_HAS_LANDING_GEAR(vehIndex)
							CONTROL_LANDING_GEAR(vehIndex, LGC_RETRACT_INSTANT)
						ENDIF
						
						PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Calling SET_VEHICLE_FORWARD_SPEED with 30.0")
						
						IF IS_VEHICLE_MODEL(vehIndex, HYDRA)
						OR IS_VEHICLE_MODEL(vehIndex, TULA)
							SET_VEHICLE_FLIGHT_NOZZLE_POSITION_IMMEDIATE(vehIndex, 0.0)
							PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Calling SET_VEHICLE_FLIGHT_NOZZLE_POSITION_IMMEDIATE with 0.0 on our Hydra.")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Vehicle is a personal vehicle, colour is already set.")
		ENDIF
	ENDIF
		
	SET_VEHICLE_DIRT_LEVEL(vehIndex, 0.0)
	
	//Cache the new respawn vehicle
	IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_CACHE_VEHICLE_COMPLETED)
		SET_BIT(MC_PlayerBD[iLocalPart].iClientBitSet2, PBBOOL2_CACHE_RESPAWN_VEHICLE)
		CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_CACHE_VEHICLE_COMPLETED)
	ENDIF
ENDPROC

PROC SET_PLAYER_INTO_COVER_AFTER_INITIAL_SPAWNING(BOOL bForce = FALSE)
	UNUSED_PARAMETER(bForce)
	IF NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed)
	AND NOT IS_ENTITY_IN_AIR(localPlayerPed)
	AND NOT IS_ENTITY_IN_WATER(localPlayerPed)
	AND IS_BIT_SET(iLocalBoolCheck30, LBOOL30_SET_PLAYER_INTO_COVER_START_POSITION)
	AND NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_HAS_BEEN_SET_INTO_COVER_START_POSITION)
	AND (IS_SKYSWOOP_AT_GROUND() OR IS_SKYCAM_ON_LAST_CUT())
		VECTOR vCoverCoords = GET_ENTITY_COORDS(LocalPlayerPed)		
		
		PRINTLN("[MISSION START POS] - [SET_PLAYER_INTO_COVER_AFTER_INITIAL_SPAWNING] - Entering Cover.")
		PRINTLN("[MISSION START POS] - [SET_PLAYER_INTO_COVER_AFTER_INITIAL_SPAWNING] - Force Left: ", IS_BIT_SET(iLocalBoolCheck33, LBOOL33_SET_PLAYER_INTO_COVER_START_POSITION_FORCE_LEFT))
		PRINTLN("[MISSION START POS] - [SET_PLAYER_INTO_COVER_AFTER_INITIAL_SPAWNING] - Attempting to re-equip weapon: ", GET_WEAPON_NAME(gweapon_type_CurrentlyHeldWeapon))				
		COVERPOINT_INDEX cpIndex = GET_CLOSEST_COVER_POINT_TO_LOCATION(GET_ENTITY_COORDS(LocalPlayerPed), vCoverCoords)
		SET_PED_TO_LOAD_COVER(LocalPlayerPed, TRUE)
		
		IF cpIndex != NULL
			TASK_WARP_PED_DIRECTLY_INTO_COVER(localPlayerPed, -1, TRUE, TRUE, IS_BIT_SET(iLocalBoolCheck33, LBOOL33_SET_PLAYER_INTO_COVER_START_POSITION_FORCE_LEFT), cpIndex)
			PRINTLN("[MISSION START POS] - [SET_PLAYER_INTO_COVER_AFTER_INITIAL_SPAWNING] - using TASK_WARP_PED_DIRECTLY_INTO_COVER, with cpIndex at vCoverPos: ", vCoverCoords)
		ELSE
			IF IS_VECTOR_ZERO(vCoverCoords)
				vCoverCoords = GET_ENTITY_COORDS(LocalPlayerPed)
			ENDIF
			TASK_PUT_PED_DIRECTLY_INTO_COVER(LocalPlayerPed, vCoverCoords, -1, TRUE, DEFAULT, TRUE, IS_BIT_SET(iLocalBoolCheck33, LBOOL33_SET_PLAYER_INTO_COVER_START_POSITION_FORCE_LEFT))
			PRINTLN("[MISSION START POS] - [SET_PLAYER_INTO_COVER_AFTER_INITIAL_SPAWNING] - using TASK_PUT_PED_DIRECTLY_INTO_COVER, with vCoverPos: ", vCoverCoords)		
		ENDIF
		FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, TRUE, FALSE)
				
		SET_BIT(iLocalBoolCheck30, LBOOL30_HAS_BEEN_SET_INTO_COVER_START_POSITION)
	ENDIF
ENDPROC

FUNC BOOL IS_START_PLAYER_TEAM_VEHICLE_MODEL_VALID(INT iTeam)
	
	IF g_mnCoronaMissionVehicle != DUMMY_MODEL_FOR_SCRIPT
	OR g_FMMC_STRUCT.mnVehicleModel[iTeam] != DUMMY_MODEL_FOR_SCRIPT
		RETURN TRUE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.iPersonalVehicleIndexToUse != -1
		IF GET_CUSTOM_VEHICLE_MODEL_NAME(g_TransitionSessionNonResetVars.iPersonalVehicleIndexToUse) != DUMMY_MODEL_FOR_SCRIPT
			RETURN TRUE
		ENDIF	
	ENDIF
	
	RETURN FALSE	
ENDFUNC

/// PURPOSE: This function returns true once a vehicle is spawned for them, and they are placed inside.
FUNC BOOL SPAWN_PLAYER_IN_TEAM_VEHICLE( VECTOR vLocationToSpawn, FLOAT fHeadingToSpawnAt, BOOL bOnlyCreateVehicle = FALSE)
	BOOL bReturn = FALSE
	
	INT iPlayerTeam = MC_playerBD[iLocalPart].iteam
	VEHICLE_INDEX 	vehToSpawnPlayerIn 	= NULL
	MODEL_NAMES 	mnTeamVeh 			= g_FMMC_STRUCT.mnVehicleModel[iPlayerTeam]
	NETWORK_INDEX	niTemp 				= NULL
	
	IF g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed = 0
		g_TransitionSessionNonResetVars.iMonsterJamFirstRoundTeam = iPlayerTeam
	ENDIF
	
	IF IS_MODEL_VALID(g_FMMC_STRUCT.mnVehicleModel[iPlayerTeam])
		PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - initial spawn vehicle g_FMMC_STRUCT.mnVehicleModel[iPlayerTeam] = ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT.mnVehicleModel[iPlayerTeam]), " model number: ", g_FMMC_STRUCT.mnVehicleModel[iPlayerTeam])
	ELSE
		PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - initial spawn vehicle g_FMMC_STRUCT.mnVehicleModel[iPlayerTeam] is NOT VALID - Probably a removed vehicle. model number: ", g_FMMC_STRUCT.mnVehicleModel[iPlayerTeam])
	ENDIF
		
	IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iPlayerTeam ].iTeamBitset, ciBS_TEAM_USES_CORONA_VEHICLE_LIST )
	AND (NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_Force_Respawn_Veh_As_Start_Veh) OR iPlayerTeam = 0)
		IF g_mnCoronaMissionVehicle != DUMMY_MODEL_FOR_SCRIPT
		AND IS_MODEL_VALID(g_mnCoronaMissionVehicle)
			PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - ciBS_TEAM_USES_CORONA_VEHICLE_LIST is set - g_mnCoronaMissionVehicle = ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_mnCoronaMissionVehicle))
			mnTeamVeh = g_mnCoronaMissionVehicle
		ELSE
			PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - ciBS_TEAM_USES_CORONA_VEHICLE_LIST is set - g_mnCoronaMissionVehicle is dummy_model or invalid! ",ENUM_TO_INT(g_mnCoronaMissionVehicle))
		ENDIF
	ENDIF
	
	BOOL bShouldUsePV = TRUE
		
	IF g_iMyRaceModelChoice != -1
	AND g_mnMyRaceModel != DUMMY_MODEL_FOR_SCRIPT
	AND g_TransitionSessionNonResetVars.iPersonalVehicleIndexToUse = -1
	AND bShouldUsePV
		g_TransitionSessionNonResetVars.iPersonalVehicleIndexToUse = g_iMyRaceModelChoice
		PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - Storing personal vehicle selection for next round: ", g_TransitionSessionNonResetVars.iPersonalVehicleIndexToUse)
	ENDIF
	
	IF g_TransitionSessionNonResetVars.iPersonalVehicleIndexToUse != -1
	AND (NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_Force_Respawn_Veh_As_Start_Veh) OR iPlayerTeam = 0)
		MODEL_NAMES mnPVToUse = GET_CUSTOM_VEHICLE_MODEL_NAME(g_TransitionSessionNonResetVars.iPersonalVehicleIndexToUse)
		IF mnTeamVeh = mnPVToUse		
			g_mnMyRaceModel = mnPVToUse
			g_iMyRaceModelChoice = g_TransitionSessionNonResetVars.iPersonalVehicleIndexToUse
			PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - USING CACHED PERSONAL VEHICLE INFO: g_mnMyRaceModel: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_mnMyRaceModel), " g_iMyRaceModelChoice: ", g_iMyRaceModelChoice)
		ENDIF
	ENDIF
	
	IF g_iMyRaceModelChoice = -1
		bShouldUsePV = FALSE
		PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - Setting bShouldUsePV to false as g_iMyRaceModelChoice = -1")
	ENDIF

	IF g_mnMyRaceModel != DUMMY_MODEL_FOR_SCRIPT
	AND bShouldUsePV
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
	AND (NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_Force_Respawn_Veh_As_Start_Veh) OR iPlayerTeam = 0)
		
		mnTeamVeh = g_mnMyRaceModel		
		
		IF NOT IS_MODEL_VALID(mnTeamVeh)
			PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - Vehicle Model is not valid, will not be able to spawn from team vehicle, BAILING WITH TRUE. (1)")
			RETURN TRUE	
		ENDIF
		
		IF NOT USING_MULTIPLE_PASSENGER_TEAM_VEHICLE(iPlayerTeam)
		OR (USING_MULTIPLE_PASSENGER_TEAM_VEHICLE(iPlayerTeam) AND IS_DRIVER_FOR_MULTIPLE_TEAM_VEHICLE(iPlayerTeam))			
			IF CREATE_MP_RACE_SAVED_VEHICLE( niTemp, vLocationToSpawn, fHeadingToSpawnAt, TRUE )
				PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - g_mnMyRaceModel is valid, spawning in that ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_mnMyRaceModel))
				
				PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS(vehToSpawnPlayerIn, TRUE, iPlayerTeam, TRUE)
				
				IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iPlayerTeam ].iTeamBitset, ciBS_TEAM_USES_CORONA_VEHICLE_LIST )
					vehToSpawnPlayerIn = NET_TO_VEH(niTemp)
										
					FREEZE_ENTITY_POSITION( LocalPlayerPed, FALSE )
					SET_ENTITY_COLLISION( LocalPlayerPed, TRUE )
					
					SET_BIT(iLocalBoolCheck28, LBOOL28_USING_PV)
					PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - Setting LBOOL28_USING_PV")
					
					IF NOT bOnlyCreateVehicle
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_PutPlayersInVehicleFast)
							SET_PED_INTO_VEHICLE(LocalPlayerPed, vehToSpawnPlayerIn)
						ELSE
							TASK_ENTER_VEHICLE(LocalPlayerPed, vehToSpawnPlayerIn, 1, DEFAULT, DEFAULT, ECF_WARP_PED )
						ENDIF
					ENDIF
					
					PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - SET_VEHICLE_ON_GROUND_PROPERLY")
					PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - Setting FREEZE_ENTITY_POSITION TRUE")
					SET_VEHICLE_ENGINE_ON(vehToSpawnplayerIn, TRUE, TRUE)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehToSpawnPlayerIn)
					FREEZE_ENTITY_POSITION(vehToSpawnplayerIn, TRUE)
								
					#IF IS_DEBUG_BUILD
						VECTOR vVehPos = GET_ENTITY_COORDS(vehToSpawnPlayerIn)
						PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - Freezing entity waiting on collision at pos: ", vVehPos)
					#ENDIF					
				ENDIF
				
				bReturn = TRUE
			ELSE
				PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - Waiting for CREATE_MP_RACE_SAVED_VEHICLE, creating g_mnMyRaceModel = ",ENUM_TO_INT(g_mnMyRaceModel))
			ENDIF
		ELSE
			RETURN SET_PASSENGER_INTO_VEHICLE()			
		ENDIF
	ELSE
		IF NOT IS_MODEL_VALID(mnTeamVeh)
			PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - Vehicle Model is not valid, will not be able to spawn from team vehicle, BAILING WITH TRUE. (2)")
			RETURN TRUE	
		ENDIF
		
		REQUEST_MODEL( mnTeamVeh )
		CLEAR_BIT(iLocalBoolCheck28, LBOOL28_USING_PV)
		
		// Create a vehicle and plop this player into it
		IF HAS_MODEL_LOADED( mnTeamVeh )
			
			// Create the vehicle if the model is loaded, using the parameters we found above
			
			//Make sure the spawn vehicle blip management is reset to a clean state
			//as we create a new spawn vehicle.
			CLEAN_UP_SPAWN_VEHICLE_BLIPS()			
			
			IF NOT USING_MULTIPLE_PASSENGER_TEAM_VEHICLE(iPlayerTeam)
			OR (USING_MULTIPLE_PASSENGER_TEAM_VEHICLE(iPlayerTeam) AND IS_DRIVER_FOR_MULTIPLE_TEAM_VEHICLE(iPlayerTeam))
				
				PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - (CREATE_VEHICLE) Spawning the Vehicle ", FMMC_GET_MODEL_NAME_FOR_DEBUG(mnTeamVeh), " at vLocationToSpawn: ", vLocationToSpawn, " fHeadingToSpawnAt:", fHeadingToSpawnAt, " iTeam: ", iPlayerTeam)
				
				vehToSpawnPlayerIn = CREATE_VEHICLE(mnTeamVeh, vLocationToSpawn, fHeadingToSpawnAt, DEFAULT, FALSE)
				SET_MODEL_AS_NO_LONGER_NEEDED(mnTeamVeh)
				
				IF IS_THIS_MODEL_A_HELI(mnTeamVeh)
				OR IS_THIS_MODEL_A_PLANE(mnTeamVeh)
				OR IS_THIS_MODEL_A_BOAT(mnTeamVeh)
					FREEZE_ENTITY_POSITION(vehToSpawnPlayerIn, TRUE)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehToSpawnPlayerIn)
					PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - boat, heli or plane")
				ELSE
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehToSpawnPlayerIn, TRUE)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehToSpawnPlayerIn)
					#IF IS_DEBUG_BUILD
					VECTOR vPostGroundPos = GET_ENTITY_COORDS(vehToSpawnPlayerIn)
					PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - not a b,h or p - Grounded Coords: ", vPostGroundPos)
					#ENDIF
				ENDIF
					
				IF NOT IS_VEHICLE_FUCKED_MP(vehToSpawnPlayerIn)
				AND bLocalPlayerOK					
					
					FREEZE_ENTITY_POSITION(LocalPlayerPed, FALSE)
					
					IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
						SET_ENTITY_COLLISION(LocalPlayerPed, TRUE)
					ENDIF
					
					// once the vehicle exists, put the player into it
					IF NOT bOnlyCreateVehicle
						IF IS_THIS_MODEL_A_HELI(mnTeamVeh)
						OR IS_THIS_MODEL_A_PLANE(mnTeamVeh)
						OR IS_THIS_MODEL_A_BOAT(mnTeamVeh)
							PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - Setting ped into vehicle.")
							SET_PED_INTO_VEHICLE(LocalPlayerPed, vehToSpawnPlayerIn)
						ELSE
							PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - Tasking ped to enter vehicle.")
							TASK_ENTER_VEHICLE(LocalPlayerPed, vehToSpawnPlayerIn, 1, DEFAULT, DEFAULT, ECF_WARP_PED)
						ENDIF
					ELSE
						PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - Only creating vehicle, not warping into it." )
					ENDIF
					
					IF NOT IS_PED_IN_ANY_VEHICLE( LocalPlayerPed, TRUE )
						PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - just tried put the local player in a vehicle but they're not in it!" )
					ENDIF
						
					PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS(vehToSpawnPlayerIn, TRUE, iPlayerTeam, TRUE)
					
					netRespawnVehicle = VEH_TO_NET(vehToSpawnPlayerIn)
					SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(netRespawnVehicle, TRUE)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					
					bReturn = TRUE

					#IF IS_DEBUG_BUILD
					VECTOR vSpawnedCoords = GET_ENTITY_COORDS(vehToSpawnPlayerIn)
					FLOAT fSpawnedHead = GET_ENTITY_HEADING(vehToSpawnPlayerIn)
					PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - Spawned the local player in a team vehicle Vehicle is at vLocation: ", vSpawnedCoords, " fHeading:", fSpawnedHead, " IS_PED_IN_VEH: ", IS_PED_IN_VEHICLE(localPlayerPed, vehToSpawnPlayerIn))					
					#ENDIF
					
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - vehicle creation failed: vehicle is fucked = ",IS_VEHICLE_FUCKED_MP(vehToSpawnPlayerIn),", bLocalPlayerOK = ",bLocalPlayerOK)
				#ENDIF
				ENDIF
			ELSE
				RETURN SET_PASSENGER_INTO_VEHICLE()
			ENDIF	
		ELSE
			PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - Waiting for model ", FMMC_GET_MODEL_NAME_FOR_DEBUG(mnTeamVeh)," to load")
		ENDIF
	ENDIF
	
	MC_playerBD_1[ iLocalPart ].mnCoronaVehicle = mnTeamVeh
	PRINTLN("[InitPosition] - SPAWN_PLAYER_IN_TEAM_VEHICLE - mnCoronaVehicle = ", FMMC_GET_MODEL_NAME_FOR_DEBUG(mnTeamVeh))

	RETURN bReturn
ENDFUNC

PROC PROCESS_ADDING_WATER_NODES_FOR_AMBIENT_SPAWNING()

	INT iTeam = MC_playerBD[iLocalPart].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_RESPAWN_IN_NEARBY_WATER_ON_FOOT)
		OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_RESPAWN_IN_NEARBY_WATER_VEHICLE)
		
			PRINTLN("[spawning][WaterNodeSpawn] Starting the search for some custom vehicle nodes, so that we can ambiently spawn on water.")
			CLEAR_CUSTOM_VEHICLE_NODES()
			
			INT iAdded = 0
			VECTOR vNodeCoord, vGroundCoord
			FLOAT fX, fY
			FLOAT fMod = 1.0
			INT iVehNode = 0
			BOOL bNotWater
			
			FOR iVehNode = 0 TO 60
				
				IF iAdded <= 3
					IF iVehNode >= 15
						fMod = 3.0
					ELIF iVehNode >= 30
						fMod = 10.0
					ENDIF
				ENDIF
				
				vNodeCoord = GET_ENTITY_COORDS(localPlayerPed, FALSE)
				vGroundCoord = GET_ENTITY_COORDS(localPlayerPed, FALSE)
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_RESPAWN_NEAR_OBJECTIVE)
					vNodeCoord = GET_COORDS_OF_CLOSEST_OBJECTIVE()
					PRINTLN("[spawning][WaterNodeSpawn] Setting vNodeCoord to ", vNodeCoord, " due to ciBS_RULE14_RESPAWN_IN_NEARBY_WATER_VEHICLE")
				ENDIF
				
				FLOAT fDistanceMax, fDistanceMin
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRespawnOnWaterDistanceFromCoord[iRule] != 0
					fDistanceMin = TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRespawnOnWaterDistanceFromCoord[iRule]) / 2
					fDistanceMax = TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRespawnOnWaterDistanceFromCoord[iRule])
				ELSE
					fDistanceMin = 40
					fDistanceMax = 80
				ENDIF
				
				IF GET_RANDOM_INT_IN_RANGE(0, 100) > 50
					fX = GET_RANDOM_FLOAT_IN_RANGE(fDistanceMin*fMod, fDistanceMax*fMod)
				ELSE
					fX = GET_RANDOM_FLOAT_IN_RANGE(-fDistanceMax*fMod, -fDistanceMin*fMod)
				ENDIF
				IF GET_RANDOM_INT_IN_RANGE(0, 100) > 50
					fY = GET_RANDOM_FLOAT_IN_RANGE(fDistanceMin*fMod, fDistanceMax*fMod)
				ELSE
					fY = GET_RANDOM_FLOAT_IN_RANGE(-fDistanceMax*fMod, -fDistanceMin*fMod)
				ENDIF
				
				PRINTLN("[spawning][WaterNodeSpawn] iAttempt: ", iVehNode, " fX: ",  fX, " and fY: ", fY)
				
				vNodeCoord.x += fX
				vNodeCoord.y += fY
				vNodeCoord.z += 10.0
				
				vGroundCoord.x += fX
				vGroundCoord.y += fY
				vGroundCoord.z += 75.0
				
				GET_GROUND_Z_FOR_3D_COORD(vGroundCoord, vGroundCoord.z, FALSE, FALSE)
				
				IF NOT GET_WATER_HEIGHT_NO_WAVES(vNodeCoord, vNodeCoord.z)
					bNotWater = TRUE
				ELSE
					bNotWater = FALSE
				ENDIF
									
				PRINTLN("[spawning][WaterNodeSpawn] vNodeCoord.z: ",  vNodeCoord.z, " and  vGroundCoord.z: ",  vGroundCoord.z)
				
				IF ABSF(vNodeCoord.z -  vGroundCoord.z) < 4.0
				OR vGroundCoord.z > vNodeCoord.z
				OR vGroundCoord.z = 0.0
				OR bNotWater
					PRINTLN("[spawning][WaterNodeSpawn] vNodeCoord: ",  vNodeCoord, " Water is below some ground. Or it is too shallow. Not adding.")
				ELSE
					PRINTLN("[spawning][WaterNodeSpawn] Hit Water vNodeCoord: ",  vNodeCoord, " Adding Custom Vehicle Node")
					ADD_CUSTOM_VEHICLE_NODE(vNodeCoord, GET_HEADING_FROM_COORDS_LA(vNodeCoord, GET_COORDS_OF_CLOSEST_OBJECTIVE()))
					SET_PLAYER_WILL_SPAWN_FACING_COORDS(GET_COORDS_OF_CLOSEST_OBJECTIVE(), TRUE, FALSE)									
					iAdded++
				ENDIF
				
				IF iAdded >= 10
					PRINTLN("[spawning][WaterNodeSpawn] Added 15, that's enough BREAKLOOP")
					BREAKLOOP
				ENDIF
			ENDFOR	
			#IF IS_DEBUG_BUILD
			IF iAdded = 0
				#IF IS_DEBUG_BUILD
				ASSERTLN("LM - Player has spawned where they were standing due to improper rule setup with water spawning system.")
				PRINTLN("[spawning][WaterNodeSpawn] Player has spawned where they were standing due to improper rule setup with water spawning system.")
				ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_COULD_NOT_FIND_VALID_WATER_POSITION, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "Player has spawned where they were standing due to improper rule setup with water spawning system")
				#ENDIF
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Manual Respawn																				
// ##### Description: Wrappers and Helper Functions that are used in various places of spawning/respawning.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

///PURPOSE: This function checks all the necessary requirements for allowing manual respawn to occur
///    eg 15 seconds has to pass, the mission can't be over and the player must not be carrying
///    an object, or if they are, they must not be in the air.
FUNC BOOL IS_PLAYER_SAFE_TO_MANUALLY_RESPAWN()
	BOOL bReturn = FALSE

	// if default respawning system is running then return false
	// This will always return false in test mode as the respawn system isn't active there
	IF IS_PLAYER_RESPAWNING(LocalPlayer)
	OR IS_PLAYER_DEAD(LocalPlayer)
		PRINTLN("[spawning] IS_PLAYER_SAFE_TO_MANUALLY_RESPAWN - system respawning in control.")
		RETURN(FALSE)
	ENDIF
	
	IF IS_SPECIAL_CONTROLLER_RESPAWN_REQUESTED()
		RETURN TRUE
	ENDIF 
	
	IF bIsAnySpectator
		RETURN FALSE
	ENDIF
	
	IF TURRET_MANAGER_GET_GROUP(LocalPlayer) = TGT_ARENA_CONTESTANT
	AND TURRET_CAM_IS_READY()
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_BlockManualRespawnAtEnd)
	AND NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		IF GET_LOCAL_PLAYER_MULTIRULE_TIMER_TIME_REMAINING() < 5000
			PRINTLN("[spawning] IS_PLAYER_SAFE_TO_MANUALLY_RESPAWN - 5 seconds left in a Arena War mission. Returning FALSE")
			RETURN FALSE
		ENDIF
	ENDIF
	
	VEHICLE_INDEX playerVeh = NULL
	
	// Grab the vehicle the player is in	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		playerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
	#IF IS_DEBUG_BUILD
		PRINTLN("[spawning] IS_PLAYER_SAFE_TO_MANUALLY_RESPAWN - g_FMMC_STRUCT.iManualRespawnDelay : ", g_FMMC_STRUCT.iManualRespawnDelay)
		PRINTLN("[spawning] IS_PLAYER_SAFE_TO_MANUALLY_RESPAWN - tdMissionLengthTimer : ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer))
		PRINTLN("[spawning] IS_PLAYER_SAFE_TO_MANUALLY_RESPAWN OBJECTIVE_TIME : ", GET_OBJECTIVE_TIME_LIMIT_FOR_LOCAL_PLAYER_CURRENT_RULE())
		PRINTLN("[spawning] IS_PLAYER_SAFE_TO_MANUALLY_RESPAWN tdObjectiveLimitTimer : ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iPartToUse].iTeam]) - MC_serverBD_3.iTimerPenalty[MC_playerBD[iPartToUse].iteam])
	#ENDIF
	
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer) > MAX_VALUE_INT(g_FMMC_STRUCT.iManualRespawnDelay * 1000, 0)
		OR ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		OR (DOES_ENTITY_EXIST(playerVeh) AND IS_VEHICLE_FUCKED_MP(playerVeh))
			IF GET_CURRENT_OBJECTIVE_TIMER_TIME_REMAINING(GET_LOCAL_PLAYER_TEAM()) > MANUAL_RESPAWN_END_DELAY * 1000
			OR g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iObjectiveTimeLimitRule[MC_serverBD_4.iCurrentHighestPriority[GET_LOCAL_PLAYER_TEAM()]] = 0
			OR ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
				IF NOT g_bMissionOver AND NOT g_bMissionEnding
					// Allow manual respawn if we're not carrying an object
					IF MC_playerBD[iPartToUse].iObjCarryCount = 0
						bReturn = TRUE
					// Or if we are holding objects, and we're in a vehicle, and not in the air
					ELIF (MC_playerBD[iPartToUse].iObjCarryCount > 0 AND DOES_ENTITY_EXIST(playerVeh) AND ((NOT IS_ENTITY_IN_AIR(playerVeh)) OR (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset2, ciBS2_ALLOW_MANUAL_RESPAWN_WITH_PACKAGES))))
						bReturn = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		AND GET_SUDDEN_DEATH_TIME_REMAINING(GET_LOCAL_PLAYER_TEAM(), GET_LOCAL_PLAYER_CURRENT_RULE()) < 2000
			RETURN FALSE
		ENDIF
	ENDIF

	//Finish a manual respawn at the end if we've started one.
	IF g_bMissionOver OR g_bMissionEnding
		IF manualRespawnState != eManualRespawnState_OKAY_TO_SPAWN
			bReturn = TRUE
		ENDIF
	ENDIF
	
	PRINTLN("[spawning] IS_PLAYER_SAFE_TO_MANUALLY_RESPAWN - Returning ", BOOL_TO_STRING(bReturn))
	RETURN bReturn
ENDFUNC

///PURPOSE: This function checks if we're supposed to respawn if we come off our vehicle
FUNC BOOL SHOULD_PLAYER_BE_FORCED_TO_RESPAWN()
	
	BOOL bReturn = FALSE
	INT iTeam = MC_playerBD[iPartToUse].iteam
	
	INT iRespawnTimer = 2250
	BOOL bIgnoreTimer = IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_IMMEDIATE_UNDRIVEABLE_DEATH)
	
	IF IS_SPECIAL_CONTROLLER_RESPAWN_REQUESTED()
		RETURN TRUE
	ENDIF
	
	IF g_bMissionOver
		RETURN FALSE
	ENDIF
			
	IF IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
	OR bIsAnySpectator
		RETURN FALSE
	ENDIF	
	
	IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iTeamBitset, ciBS_FORCE_RESPAWN_WHEN_NOT_IN_RESPAWN_VEHICLE )
	AND (g_FMMC_STRUCT.mnVehicleModel[ iTeam ] != DUMMY_MODEL_FOR_SCRIPT)
		
		IF ( manualRespawnState = eManualRespawnState_OKAY_TO_SPAWN
			AND NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			AND (NOT g_bMissionOver) AND (NOT g_bMissionEnding) )
			
			IF bIgnoreTimer
				bReturn = TRUE
			ELSE
				IF HAS_NET_TIMER_STARTED(tdForceVehicleRespawnTimer)
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdForceVehicleRespawnTimer) >= iRespawnTimer
					OR (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2, ciBS2_KILL_PLAYER_OUT_OF_VEH)
					AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdForceVehicleRespawnTimer) >= 250)
						bReturn = TRUE
					ENDIF
				ELSE
					REINIT_NET_TIMER(tdForceVehicleRespawnTimer)
				ENDIF
			ENDIF
			
		ELSE
			IF HAS_NET_TIMER_STARTED(tdForceVehicleRespawnTimer)
				RESET_NET_TIMER(tdForceVehicleRespawnTimer)
			ENDIF	
		ENDIF
	ENDIF
	
	// Force respawn vehicle and driver when undriveable in water
	IF IS_BIT_SET(g_FMMC_STRUCT.iRuleOptionsBitSet, ciFORCE_RESPAWN_SUBMERGED_VEH_TEAM0 + iTeam)
	AND g_FMMC_STRUCT.mnVehicleModel[ iTeam ] <> DUMMY_MODEL_FOR_SCRIPT
		IF (manualRespawnState = eManualRespawnState_OKAY_TO_SPAWN
		AND IS_PED_IN_UNDRIVEABLE_VEHICLE_IN_WATER(LocalPlayerPed)
		AND NOT g_bMissionOver 
		AND NOT g_bMissionEnding)
			
			IF (HAS_NET_TIMER_STARTED(tdForceVehicleRespawnInWaterTimer))
				IF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdForceVehicleRespawnInWaterTimer) >= iRespawnTimer)
					bReturn = TRUE
				ENDIF
			ELSE
				REINIT_NET_TIMER(tdForceVehicleRespawnInWaterTimer)
			ENDIF
		ELSE
			IF (HAS_NET_TIMER_STARTED(tdForceVehicleRespawnInWaterTimer))
				RESET_NET_TIMER(tdForceVehicleRespawnInWaterTimer)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndCOnditions[iTeam].iTeamBitset2, ciBS2_TEAM_FORCE_RESPAWN_IN_UNDRIVEABLE_VEHICLE)
	AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		
		IF( manualRespawnState = eManualRespawnState_OKAY_TO_SPAWN
		AND NOT IS_VEHICLE_DRIVEABLE(tempVeh)
		AND NOT g_bMissionOver 
		AND NOT g_bMissionEnding )
		
			IF bIgnoreTimer
				bReturn = TRUE
			ELSE
				IF HAS_NET_TIMER_STARTED(tdForceUndriveableVehicleRespawnTimer)
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdForceUndriveableVehicleRespawnTimer) >= iRespawnTimer
						bReturn = TRUE
					ENDIF
				ELSE
					REINIT_NET_TIMER(tdForceUndriveableVehicleRespawnTimer)
				ENDIF
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(tdForceUndriveableVehicleRespawnTimer)
				RESET_NET_TIMER(tdForceUndriveableVehicleRespawnTimer)
			ENDIF	
		ENDIF
	ENDIF
	
	IF IS_PLAYER_RESPAWNING(LocalPlayer)
	OR IS_PLAYER_DEAD(LocalPlayer)
		PRINTLN("[spawning] SHOULD_PLAYER_BE_FORCED_TO_RESPAWN - system respawning in control.")
		RETURN FALSE
	ENDIF
	
	RETURN bReturn
	
ENDFUNC

/// PURPOSE: This function checks the vehicle stuck timers that normally occur on mission vehicles for respawn vehicles (as they aren't
///    checked in the normal staggered loop vehicle checks)
PROC CHECK_MANUAL_RESPAWN_VEHICLE()
	
	IF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_IN_PLACED_VEHICLE )
		IF NETWORK_DOES_NETWORK_ID_EXIST(netRespawnVehicle)
			
			VEHICLE_INDEX tempVeh = NET_TO_VEH(netRespawnVehicle)
			
			IF IS_VEHICLE_DRIVEABLE(tempVeh)
			AND tempVeh != g_vehStandbyVehicle
				CHECK_VEHICLE_STUCK_TIMERS( tempVeh, 666, NETWORK_HAS_CONTROL_OF_NETWORK_ID(netRespawnVehicle) )
				
			//Fail the mission if the team vehicle is critical
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ iPartToUse ].iteam].iTeamBitset2, ciBS2_TEAM_VEHICLE_CRITICAL)
			
				INT iTeam = MC_playerBD[ iPartToUse ].iteam
				INT iPriority = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
				
				IF iPriority < FMMC_MAX_RULES					
					REQUEST_SET_TEAM_FAILED(iTeam, mFail_VEH_DEAD, TRUE)
					SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,iPriority,MC_serverBD.iReasonForObjEnd[iTeam],TRUE)
					BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam],
														 iTeam,
														 iPriority,
														 DID_TEAM_PASS_OBJECTIVE(iTeam,iPriority),TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC CHECK_AND_CLEAR_MANUAL_RESPAWN_TEXT()
	//Check and clear, so we're not spamming.
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_AIR_OT_09")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_HELP_44")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VENI_HELP_1")
		CLEAR_HELP(TRUE)
		CLEAR_BIT(iLocalBoolCheck9, LBOOL9_CAN_SHOW_MANUAL_RESPAWN_HELP_TEXT)
	ELSE
		CLEAR_BIT(iLocalBoolCheck9, LBOOL9_CAN_SHOW_MANUAL_RESPAWN_HELP_TEXT)
	ENDIF	
ENDPROC

// PURPOSE: Checks to see if the respawn timer help text should show.
// This includes at the start of the match, when OOB, STUCK, or not on a vehicle (For cases like in Dirty Sanchez).
FUNC BOOL IS_MANUAL_RESPAWN_TEXT_RELEVANT()
	INT iTeam = MC_playerBD[ iPartToUse ].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
		
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_BLOCK_MANUAL_RESPAWN_DUE_TO_VEHICLE)	
		OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_BLOCK_MANUAL_RESPAWN)
		OR NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
		OR IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
			RETURN FALSE
		ENDIF
	ENDIF	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
	AND NOT IS_PARTICIPANT_DRIVING_SEAT_SWITCH_VEHICLE(INT_TO_PARTICIPANTINDEX(iPartToUse))
		RETURN FALSE
	ENDIF
	
	IF IS_CELLPHONE_CAMERA_IN_USE()
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_OUT_OF_BOUNDS()
	OR IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_OO_BOUNDS)
		PRINTLN("[MISSION CONTROLLER][IS_RESPAWN_TEXT_RELEVANT] - OOB Timer")
		RETURN TRUE							//OO Bounds timer started - Show Help Text...
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, FALSE)
		VEHICLE_INDEX vehMy = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		
		IF DOES_ENTITY_EXIST(vehMy)
			IF (IS_VEHICLE_STUCK_TIMER_UP( vehMy, VEH_STUCK_ON_ROOF, 5000 )
			OR IS_VEHICLE_STUCK_TIMER_UP( vehMy, VEH_STUCK_ON_SIDE, 5000 )
			OR IS_VEHICLE_STUCK_TIMER_UP( vehMy, VEH_STUCK_HUNG_UP, 5000 )
			OR IS_VEHICLE_STUCK_TIMER_UP( vehMy, VEH_STUCK_JAMMED, 5000 ))
			OR NOT IS_VEHICLE_DRIVEABLE(vehMy)
				PRINTLN("[MISSION CONTROLLER][IS_RESPAWN_TEXT_RELEVANT] - STUCK/BROKE VEHICLE")
				RETURN TRUE				//Players Vehicle is stuck - Show Help Text...
			ENDIF	
		ENDIF
	ELSE
		RETURN TRUE						//Players Vehicle is stuck - Show Help Text...
	ENDIF
	
	IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer) < 25000
	AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer) > 18000
		RETURN TRUE
	ENDIF	
	
	PRINTLN("[MISSION CONTROLLER][IS_RESPAWN_TEXT_RELEVANT] - RETURNED FALSE")
	RETURN FALSE
ENDFUNC

PROC RESET_RESPAWN_TIMER(SCRIPT_TIMER& myRespawnTimer)
	IF HAS_NET_TIMER_STARTED( myRespawnTimer )
		RESET_NET_TIMER( myRespawnTimer )
	ENDIF
ENDPROC

FUNC BOOL HAS_FORCED_RESPAWN_STARTED_FOR_MY_DRIVER()
	IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) > -1
	
		IF HAS_NET_TIMER_STARTED(tdManualRespawnTeamVehicle_FailTimedOut)
			IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdManualRespawnTeamVehicle_FailTimedOut, ci_MAN_RESP_FAIL_TIMED_OUT)
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(MC_PlayerBD[GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())].iManualRespawnBitset, ciBS_MAN_RESPAWN_FORCED)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_MANUAL_RESPAWN_STARTED_FOR_MY_DRIVER()
	IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) > -1
	
		IF HAS_NET_TIMER_STARTED(tdManualRespawnTeamVehicle_FailTimedOut)
			IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdManualRespawnTeamVehicle_FailTimedOut, ci_MAN_RESP_FAIL_TIMED_OUT)
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(MC_PlayerBD[GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())].iManualRespawnBitset, ciBS_MAN_RESPAWN_STARTED)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_MANUAL_RESPAWN_VEH_READY_FOR_MY_DRIVER()
	IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) > -1
	
		IF HAS_NET_TIMER_STARTED(tdManualRespawnTeamVehicle_FailTimedOut)
			IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdManualRespawnTeamVehicle_FailTimedOut, ci_MAN_RESP_FAIL_TIMED_OUT)
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(MC_PlayerBD[GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())].iManualRespawnBitset, ciBS_MAN_RESPAWN_ENTERED_VEHICLE)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_MANUAL_RESPAWN_DRIVER_FINISHED()
	IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) > -1
	
		IF HAS_NET_TIMER_STARTED(tdManualRespawnTeamVehicle_FailTimedOut)
			IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdManualRespawnTeamVehicle_FailTimedOut, ci_MAN_RESP_FAIL_TIMED_OUT)
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(MC_PlayerBD[GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())].iManualRespawnBitset, ciBS_MAN_RESPAWN_FINISHED)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_MANUAL_RESPAWN_PASSENGERS_LEFT_VEHICLE()
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		PRINTLN("[RCC Mission][HAS_MANUAL_RESPAWN_PASSENGERS_ENTERED_VEHICLE] - Mission Over Returning True")
		RETURN TRUE
	ENDIF
	
	INT i = 0
	INT iPartToCheck	
	FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
		
		iPartToCheck = GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i)
		
		PRINTLN("[RCC Mission][HAS_MANUAL_RESPAWN_PASSENGERS_LEFT_VEHICLE] - iPartToCheck: ", iPartToCheck)
		
		IF iPartToCheck > -1
			IF IS_BIT_SET(MC_PlayerBD[iPartToCheck].iManualRespawnBitset, ciBS_MAN_RESPAWN_PASSENGERS_LEFT_VEHICLE)
			OR NOT IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToCheck)))
			OR IS_PLAYER_DEAD(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToCheck)))
				PRINTLN("[RCC Mission][HAS_MANUAL_RESPAWN_PASSENGERS_LEFT_VEHICLE] - Player is ready")
			ELSE
				IF iPartToCheck != iLocalPart
					PRINTLN("[RCC Mission][HAS_MANUAL_RESPAWN_PASSENGERS_LEFT_VEHICLE] - Someone isn't ready. Returning False")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("[RCC Mission][HAS_MANUAL_RESPAWN_PASSENGERS_LEFT_VEHICLE] - Returning True")
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_THERE_ENOUGH_TIME_LEFT_TO_DECREMENT_REMAINING_TIME(INT iRule, INT iTeam)
	IF iRule < FMMC_MAX_RULES
		IF IS_OBJECTIVE_TIMER_RUNNING(iTeam)
			IF GET_OBJECTIVE_TIMER_TIME_REMAINING(iTeam, iRule) < (g_FMMC_STRUCT.fManualRespawnTimeLose[iTeam] * 1000) * 1.5
				RETURN FALSE
			ENDIF
		ELIF IS_MULTIRULE_TIMER_RUNNING(iTeam)
			IF GET_LOCAL_PLAYER_MULTIRULE_TIMER_TIME_REMAINING() < (g_FMMC_STRUCT.fManualRespawnTimeLose[iTeam] * 1000) * 1.5
				RETURN FALSE
			ENDIF
		ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THERE_ENOUGH_TIME_LEFT_TO_DECREMENT_REMAINING_TIME_FOR_FORCED_RESPAWN(INT iRule, INT iTeam)
	IF iRule < FMMC_MAX_RULES
		IF IS_OBJECTIVE_TIMER_RUNNING(iTeam)
			IF GET_OBJECTIVE_TIMER_TIME_REMAINING(iTeam, iRule) < (g_FMMC_STRUCT.fForcedRespawnTimeLose[iTeam] * 1000) * 1.5
				RETURN FALSE
			ENDIF
		ELIF IS_MULTIRULE_TIMER_RUNNING(iTeam)
			IF GET_LOCAL_PLAYER_MULTIRULE_TIMER_TIME_REMAINING() < (g_FMMC_STRUCT.fForcedRespawnTimeLose[iTeam] * 1000) * 1.5
				RETURN FALSE
			ENDIF
		ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_BLOCK_NEW_MANUAL_RESPAWN(INT iTeam, INT iRule)
	IF iRule < FMMC_MAX_RULES
		IF g_FMMC_STRUCT.fManualRespawnTimeLose[iTeam] != 0
			RETURN !IS_THERE_ENOUGH_TIME_LEFT_TO_DECREMENT_REMAINING_TIME(iRule, iTeam)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_WAIT_IN_SPECTATE_ENABLED_FOR_THIS_TEAM(INT iTeamToCheck)
	RETURN IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamToCheck].iRuleBitsetFifteen[GET_TEAM_CURRENT_RULE(iTeamToCheck)], ciBS_RULE15_WAIT_IN_SPECTATE) AND NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_BLOCK_WAIT_IN_SPECTATE_UNTIL_PLAYER_RESPAWNS) 
ENDFUNC

FUNC BOOL DID_PART_JOINED_AS_A_SPECTATOR(INT iPart)
	RETURN DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
ENDFUNC

FUNC BOOL IS_LAST_PLAYER_ALIVE_SENT_TO_SPECTATE()
	
	INT iPlayersInSpectateFromThisTeam = 0
	INT iNumOfPlayersInThisTeam = 0
	INT i
	FOR i = 0 TO MAX_NUM_MC_PLAYERS - 1
		IF MC_playerBD[iLocalPart].iTeam = MC_playerBD[i].iTeam
			PARTICIPANT_INDEX piTemp = INT_TO_PARTICIPANTINDEX(i)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(piTemp)
			AND NOT DID_PART_JOINED_AS_A_SPECTATOR(i)
				PLAYER_INDEX piTempPlayer = NETWORK_GET_PLAYER_INDEX(piTemp)
				iNumOfPlayersInThisTeam++
				IF IS_PART_WAITING_IN_SPECTATE(i)
				OR NOT IS_PLAYER_PLAYING(piTempPlayer)
					iPlayersInSpectateFromThisTeam++
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

	IF iNumOfPlayersInThisTeam = iPlayersInSpectateFromThisTeam
		PRINTLN("[SPECTATE] IS_LAST_PLAYER_ALIVE_SENT_TO_SPECTATE = TRUE, iNumOfPlayersInThisTeam = ", iNumOfPlayersInThisTeam, ", iPlayersInSpectateFromThisTeam = ", iPlayersInSpectateFromThisTeam)
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

//Check if we can ignore the manual respawn check
FUNC BOOL SHOULD_IGNORE_MANUAL_RESPAWN_DUE_TO_WAIT_IN_SPECTATE_FLAG()
	IF IS_WAIT_IN_SPECTATE_ENABLED_FOR_THIS_TEAM(MC_playerBD[iLocalPart].iTeam)
	AND IS_LAST_PLAYER_ALIVE_SENT_TO_SPECTATE()
		PRINTLN("[SPECTATE] SHOULD_IGNORE_MANUAL_RESPAWN_DUE_TO_WAIT_IN_SPECTATE_FLAG = TRUE")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[SPECTATE] SHOULD_IGNORE_MANUAL_RESPAWN_DUE_TO_WAIT_IN_SPECTATE_FLAG = FALSE")
	RETURN FALSE
ENDFUNC

///PURPOSE: This function deals with manual respawns in VS Missions
///    There's a 15 second timer at the start before you can respawn.
// [FIX_2020_CONTROLLER] - Add a shared print tag for PROCESS_PLAYER_LIVES and PROCESS_MANUAL_RESPAWN
PROC PROCESS_MANUAL_RESPAWN(SCRIPT_TIMER& myRespawnTimer) 
	
	BOOL bIsDriver = IS_DRIVER_FOR_MULTIPLE_TEAM_VEHICLE(MC_PlayerBD[iLocalPart].iTeam)
	INT iTeam = MC_playerBD[ iPartToUse ].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	INT iTeamBitsetForMyTeam = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iTeamBitset
	INT iAllowRespawnTime_ms = 0
	INT iTimeAfterLastRespawn = 0
	INT ms = 0
	BOOL bForceRespawnNow
	BOOL bPlayerSafeToRespawn
	BOOL bRespawnIntoLastVehicleModel
	VEHICLE_INDEX vehPlayer
	MODEL_NAMES mnToRespawnInto = g_FMMC_STRUCT.mnVehicleModel[ iTeam ]	
	
	IF iRule < FMMC_MAX_RULES
		bRespawnIntoLastVehicleModel = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetThree[ iRule ], ciBS_RULE3_RESPAWN_IN_LAST_VEHICLE )
	ENDIF
	
	// If it's not supposed to show, then clear it!
	IF IS_MANUAL_RESPAWN_TEXT_RELEVANT() = FALSE
		CHECK_AND_CLEAR_MANUAL_RESPAWN_TEXT()
	ENDIF	
	
	IF NOT IS_CARGOBOB_MANUAL_RESPAWN_OVERRIDE()
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
	AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Using_RC_CAR)
		GET_LAST_VEHICLE_INFO(bRespawnIntoLastVehicleModel) 
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_BLOCK_MANUAL_RESPAWN_DUE_TO_VEHICLE)
		PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] LBOOL25_BLOCK_MANUAL_RESPAWN_DUE_TO_VEHICLE Bailing out.")
		EXIT	
	ENDIF
		
	IF sVehicleSwap.eVehicleSwapState != eVehicleSwapState_IDLE
		PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Swapping Vehicle, we do not want to process manual respawn, especially to avoid being force respawned as we are outside the vehicle.")
		EXIT
	ENDIF
	
	IF iInvestigateContainerState != ciHICInit
		PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Player interacting with a container: iInvestigateContainerState = ", iInvestigateContainerState)
		RESET_RESPAWN_TIMER(myRespawnTimer)
		EXIT
	ENDIF
	
	IF IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_IN_PROGRESS()
		PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_IN_PROGRESS returning true. Don't allow manual respawning")
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		bIsDriver = TRUE
	ENDIF
	
	IF manualRespawnState = eManualRespawnState_OKAY_TO_SPAWN
		IF IS_PLAYER_RESPAWNING(LocalPlayer)
		AND NOT IS_SPECIAL_CONTROLLER_RESPAWN_REQUESTED()
			PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - Exit because IS_PLAYER_RESPAWNING")
			RESET_RESPAWN_TIMER(myRespawnTimer)
			EXIT
		ENDIF
				
		IF IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
			PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - Exit because Test mode")
			RESET_RESPAWN_TIMER(myRespawnTimer)
			EXIT
		ENDIF
		
		bForceRespawnNow = SHOULD_PLAYER_BE_FORCED_TO_RESPAWN()
		
		IF bForceRespawnNow
			PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Setting LBOOL28_FORCED_MANUAL_RESPAWN")
			SET_BIT(iLocalBoolCheck28, LBOOL28_FORCED_MANUAL_RESPAWN)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
		AND NOT IS_PLAYER_RESPAWNING(localPlayer)
		AND bLocalPlayerPedOK
			IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_STARTED)
			AND bIsDriver	
				PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] (Driver) Setting bForceRespawnNow = TRUE")
				SET_BIT(iLocalBoolCheck25, LBOOL25_PASSENGER_MANUALLY_RESPAWNING)
				PRINTLN("[RCC MISSION] [MANUAL RESPAWN] (DRIVER) Setting iLocalBoolCheck25, LBOOL25_PASSENGER_MANUALLY_RESPAWNING")
				bForceRespawnNow = TRUE
			ENDIF
			
			IF NOT bIsDriver
				IF HAS_MANUAL_RESPAWN_STARTED_FOR_MY_DRIVER()
					PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] (Passenger) Setting bForceRespawnNow = TRUE (HAS_MANUAL_RESPAWN_STARTED_FOR_MY_DRIVER)")	
					bForceRespawnNow = TRUE
					START_NET_TIMER(tdManualRespawnTeamVehicle_FailTimedOut)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitSet3, ciBS3_ENABLE_FORCE_PASSENGERS_TO_RESPAWN_ON_DRIVER_RESP)
				IF bIsDriver
		
				ELSE
					INT iDriverPart = GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())
					IF iDriverPart > -1
					
						PLAYER_INDEX playerDriver = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDriverPart))
						PED_INDEX pedDriver = GET_PLAYER_PED(playerDriver)
						
						IF IS_BIT_SET(MC_PlayerBD[iDriverPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_STARTED)
							SET_BIT(iLocalBoolCheck25, LBOOL25_PASSENGER_MANUALLY_RESPAWNING)
							PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Setting iLocalBoolCheck25, LBOOL25_PASSENGER_MANUALLY_RESPAWNING")
						ENDIF
						
						IF NOT IS_PART_IN_SAME_VEHICLE_AS_PART(iLocalPart, iDriverPart)
						AND HAS_FORCED_RESPAWN_STARTED_FOR_MY_DRIVER()
						AND NOT IS_PLAYER_RESPAWNING(playerDriver)
						AND NOT IS_PED_INJURED(pedDriver)
							PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] (Passenger) Setting bForceRespawnNow = TRUE (HAS_FORCED_RESPAWN_STARTED_FOR_MY_DRIVER)")					
							bForceRespawnNow = TRUE
							START_NET_TIMER(tdManualRespawnTeamVehicle_FailTimedOut)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bForceRespawnNow
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2, ciBS2_KILL_PLAYER_OUT_OF_VEH)
		AND NOT IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_PAST_INITIATED()
		AND NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_PASSENGER_MANUALLY_RESPAWNING)
		AND NOT IS_SPECIAL_CONTROLLER_RESPAWN_REQUESTED()
		AND NOT IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
		AND NOT HAS_LOCAL_PLAYER_FINISHED_OR_REACHED_END()
			IF IS_SKYSWOOP_AT_GROUND()
				PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN][JS] Killing player as they aren't in their vehicle")
				VEHICLE_INDEX viLastVeh = GET_LAST_DRIVEN_VEHICLE()
				
				IF bLocalPlayerPedOK
					PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN][JS] Setting Player not invincible.")
					SET_ENTITY_INVINCIBLE(localPlayerPed, FALSE)
				ENDIF
				
				IF DOES_ENTITY_EXIST(viLastVeh)
					PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN][JS] Blowing up vehicle")
					IF IS_ENTITY_ALIVE(viLastVeh)	
						SET_ENTITY_INVINCIBLE(viLastVeh, FALSE)
					ENDIF
					
					NETWORK_EXPLODE_VEHICLE(viLastVeh, TRUE, TRUE)
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLEAR_UP_VEHICLE_ON_DEATH)
						IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(viLastVeh)
							NETWORK_INDEX niVeh = VEH_TO_NET(viLastVeh)
							IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID( niVeh )
									PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN][JS] Marking bike for cleanup (fell off)")	
									START_NET_TIMER(vehicleCleanUpTimer)
									netVehicleToCleanUp = niVeh
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN][JS] No net ID for vehicle. No need to Net Cleanup the vehicle.")
						ENDIF
					ENDIF
				ENDIF
				
				SET_ENTITY_HEALTH(LocalPlayerPed, 0)
			ENDIF
			EXIT
		ENDIF

		IF mnToRespawnInto = DUMMY_MODEL_FOR_SCRIPT
		AND NOT bRespawnIntoLastVehicleModel
		AND NOT IS_CARGOBOB_MANUAL_RESPAWN_OVERRIDE()
		AND NOT IS_SPECIAL_CONTROLLER_RESPAWN_REQUESTED()
			RESET_RESPAWN_TIMER(myRespawnTimer)
			EXIT
		ENDIF
		
		IF NOT bForceRespawnNow
			IF NOT IS_BIT_SET( iTeamBitsetForMyTeam, ciBS_TEAM_MANUAL_RESPAWN )			
				PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - ciBS_TEAM_MANUAL_RESPAWN not set")
				RESET_RESPAWN_TIMER(myRespawnTimer)
				EXIT	
			ENDIF
			
			IF iRule < FMMC_MAX_RULES
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_BLOCK_MANUAL_RESPAWN)
					PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - ciBS_RULE10_BLOCK_MANUAL_RESPAWN is set")
					RESET_RESPAWN_TIMER(myRespawnTimer)
					EXIT	
				ENDIF
			ENDIF
		ENDIF

		#IF IS_DEBUG_BUILD
			IF IS_FRAME_COUNT_STAGGER_READY(ciFRAME_STAGGERED_DEBUG_PLAYER_SPAWNING)
				IF bForceRespawnNow
					PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] -  We're being forced to manual respawn" )
				ENDIF
				
				IF IS_BIT_SET( iTeamBitsetForMyTeam, ciBS_TEAM_MANUAL_RESPAWN )
					PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] -  ciBS_TEAM_MANUAL_RESPAWN set" )
				ENDIF
			ENDIF
		#ENDIF
		
		bPlayerSafeToRespawn = IS_PLAYER_SAFE_TO_MANUALLY_RESPAWN()
		
		IF NOT bPlayerSafeToRespawn
			PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - Unsafe to respawn")
			RESET_RESPAWN_TIMER(myRespawnTimer)
			EXIT
		ENDIF
	ENDIF

	#IF IS_DEBUG_BUILD
	IF IS_FRAME_COUNT_STAGGER_READY(ciFRAME_STAGGERED_DEBUG_PLAYER_SPAWNING)
		PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Player is Okay to Spawn, and has nothing blocking it")
	ENDIF
	#ENDIF
	
	IF NOT IS_BIT_SET( iLocalBoolCheck13, LBOOL13_EARLY_STREAM_RESPAWN_BAR )
		PRINTLN("[BCTIMERS] Requesting early streaming of TimerBars texture" )
		REQUEST_STREAMED_TEXTURE_DICT( "TimerBars" )
		
		SET_BIT( iLocalBoolCheck13, LBOOL13_EARLY_STREAM_RESPAWN_BAR )
	ENDIF
	
	IF bLocalPlayerOK
		SET_PED_RESET_FLAG( LocalPlayerPed, PRF_OnlyExitVehicleOnButtonRelease, TRUE )
	ENDIF
	
	// The state of the manual respawn
	
	// Display manual respawn help text and get the vehicle colour combination/livery information 
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciUSE_DISABLE_MANUAL_RESPAWN_HELPTEXT)
		IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		AND bPlayerSafeToRespawn
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iTeamBitset, ciBS_TEAM_MANUAL_RESPAWN)
		AND NOT IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iTeamBitset, ciBS_TEAM_BLOCK_HELP_TEXT )
			IF IS_MANUAL_RESPAWN_TEXT_RELEVANT()	
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					SHOW_MANUAL_RESPAWN_TEXT()												
				ENDIF
			ELSE
				CHECK_AND_CLEAR_MANUAL_RESPAWN_TEXT()
			ENDIF
		ELSE
			CHECK_AND_CLEAR_MANUAL_RESPAWN_TEXT()
		ENDIF
	ENDIF

	IF ( manualRespawnState = eManualRespawnState_OKAY_TO_SPAWN )
		BOOL bBlockRespawnForPassenger, bBlockRespawnForDriver, bBlockStartingNewSpawn
		
		IF NOT bIsDriver
		AND NOT bForceRespawnNow
			bBlockRespawnForPassenger = TRUE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
			IF bIsDriver			 
				INT i = 0 
				FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
					IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) > -1
					AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i)))
						IF NOT IS_PART_IN_SAME_VEHICLE_AS_PART(iLocalPart, GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i))	
							bBlockRespawnForDriver = TRUE						
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
		
		bBlockStartingNewSpawn = SHOULD_BLOCK_NEW_MANUAL_RESPAWN(iTeam, iRule)

		// And we're currently holding triangle
		IF (( IS_CONTROL_PRESSED( PLAYER_CONTROL, INPUT_VEH_EXIT )
		OR IS_DISABLED_CONTROL_PRESSED( PLAYER_CONTROL, INPUT_VEH_EXIT ) ) AND NOT bBlockRespawnForDriver)
		AND NOT bBlockRespawnForPassenger
		AND NOT bBlockStartingNewSpawn
		OR bForceRespawnNow
			// And we're okay
			IF bLocalPlayerOK
			AND iRule < FMMC_MAX_RULES
				// And not doing anything crafty
				IF NOT IS_PED_JACKING( LocalPlayerPed )
				AND NOT IS_PED_BEING_JACKED( LocalPlayerPed )
				AND (NOT IS_PAUSE_MENU_ACTIVE() OR bForceRespawnNow)
				AND NOT (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2, ciBS2_NO_MANUAL_RESPAWN_IN_SUDDEN_DEATH) AND ARE_WE_CURRENTLY_IN_SUDDEN_DEATH())
				AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
					// Start out restart timer if it hasn't been started
					START_NET_TIMER( myRespawnTimer )

					#IF IS_DEBUG_BUILD
					IF NOT bForceRespawnNow
						PRINTLN( "[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Player holding down manual respawn button" )						
					ENDIF
					#ENDIF				
					
					// Stop the player from leaving the vehicle if they decide mid way through the respawn timer they no longer
					// want to respawn Bugstar url: 2759392
					IF HAS_NET_TIMER_STARTED(myRespawnTimer)
					AND GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(myRespawnTimer) > 1000
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INT_TO_ENUM(CONTROL_ACTION, INPUT_VEH_EXIT))
					ENDIF					
					
					// If we're over a delay
					IF HAS_NET_TIMER_EXPIRED( myRespawnTimer, FORCE_WARP_DELAY )
					OR bForceRespawnNow
						
						ms = GET_TIME_DIFFERENCE( GET_NETWORK_TIME(), myRespawnTimer.Timer )
						
						IF NOT bForceRespawnNow
						AND NOT IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
							HUDORDER eHudOrder = HUDORDER_EIGHTHBOTTOM
	
							// Draw our bar if we're not being forced to respawn
							DRAW_GENERIC_METER( ms - FORCE_WARP_DELAY, 	// Current time
												TRIANGLE_RESPAWN_TIME, 	// Max time
												"TRI_WARP", 			// Title string
												HUD_COLOUR_RED, 		// Placement colour
												DEFAULT, 				// Flashing time
												eHudOrder, 				// Hud order
												-1, 					// X pos
												-1, 					// Y pos
												FALSE, 					// IsPlayer
												TRUE )					// bOnlyZeroIsEmpty
							PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Timer bar UI should now be on screen" )
						ENDIF										   
						
						BOOL bTeamVehicleRespawnReady = TRUE
													
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
							IF NOT HAS_MANUAL_RESPAWN_PASSENGERS_LEFT_VEHICLE()
							AND bIsDriver
								PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] (Driver) Preventing Moving forward until all passengers have left the vehicle.")
								bTeamVehicleRespawnReady = FALSE
							ENDIF

							IF bIsDriver
								IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_FINISHED)
									PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] (Driver) Clearing Respawn bitset. ciBS_MAN_RESPAWN_FINISHED")
									CLEAR_BIT(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_FINISHED)
								ENDIF
								IF ( ms - FORCE_WARP_DELAY ) >= TRIANGLE_RESPAWN_TIME
								OR bForceRespawnNow
									IF NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_STARTED)
										DO_SCREEN_FADE_OUT(250)
										SET_BIT(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_STARTED)	
										PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] (Driver) Setting Respawn bitset. ciBS_MAN_RESPAWN_STARTED")
									ENDIF
								ENDIF
							ENDIF							
						ENDIF

						IF bTeamVehicleRespawnReady
							// Once time has passed/we've been forced to respawn						
							IF ( ms - FORCE_WARP_DELAY ) >= TRIANGLE_RESPAWN_TIME
							OR bForceRespawnNow
														
								PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Starting manual respawn - fading screen out" )
								
								// And doubly make sure we don't get out just yet
								DISABLE_VEHICLE_EXIT_THIS_FRAME()
								
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset2, ciBS2_LOSE_LIFE_ON_MANUAL_RESPAWN)
								OR (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset2, ciBS2_TEAM_LOSE_LIFE_ON_FORCE_RESPAWN)
								AND bForceRespawnNow)
									IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciVEHICLE_WEAPON_DEATHMATCH_TOGGLE)
									AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciPRON_DEATHMATCH_TOGGLE_CORONA))
										IF MC_PlayerBD[iPartToUse].iPlayerScore > 0
											PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - Removing a kill from the local player for manually respawning")
											REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(-1)											
										ELSE
											PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - Not removing a kill from the player for manually respawning as they have no kills")
										ENDIF
									ELSE
										PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - Adding a death for forcing/manual respawn")
										MC_PlayerBD[iLocalPart].iNumPlayerDeaths++
									ENDIF
								ENDIF
								
								PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Setting player control to false" )
								
								// Make sure the player doesn't do any funny business
								NET_SET_PLAYER_CONTROL( LocalPlayer, FALSE )
								
								IF bRespawnIntoLastVehicleModel
								AND (NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_Force_Respawn_Veh_As_Start_Veh) OR iTeam = 1)
									IF sLastVehicleInformation.mnModel != DUMMY_MODEL_FOR_SCRIPT
										mnToRespawnInto = sLastVehicleInformation.mnModel
										PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN][LVEH] Setting player to spawn in their last-used vehicle, which is mnToRespawnInto" )
									ELSE
										PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN][LVEH] Player was supposed to spawn in their last used vehicle, but it's not set, using the default" )
									ENDIF
								ENDIF
								
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciUSE_PERSONAL_VEHICLES_FOR_TEAM_VEHICLE)
								OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciUSE_PERSONAL_VEHICLES_FOR_TEAM_VEHICLE) AND g_mnMyRaceModel = DUMMY_MODEL_FOR_SCRIPT)
									IF g_FMMC_STRUCT.iVehicleColour[ MC_playerBD[ iPartToUse ].iteam ] > -1
										sLastVehicleInformation.iVehicleColour1 = GET_VEHICLE_COLOR_FROM_SELECTION(g_FMMC_STRUCT.iVehicleColour[ MC_playerBD[ iPartToUse ].iteam ])
										INT iColour2 = sLastVehicleInformation.iVehicleColour1
										IF g_FMMC_STRUCT.iVehicleSecondaryColour[MC_playerBD[ iPartToUse ].iteam] > -1
											PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - VEHICLE RESPAWN SECONDARY COLOUR - iColour:", g_FMMC_STRUCT.iVehicleSecondaryColour[MC_playerBD[ iPartToUse ].iteam])
											iColour2 = GET_VEHICLE_COLOR_FROM_SELECTION( g_FMMC_STRUCT.iVehicleSecondaryColour[MC_playerBD[ iPartToUse ].iteam])
										ENDIF
										sLastVehicleInformation.iVehicleColour2 = iColour2
										PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - Using Team Respawn Colour of ", sLastVehicleInformation.iVehicleColour1 )
									ENDIF
								ENDIF
																
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_USE_PER_RULE_VEHICLE_RESPAWN_SETTINGS)
								AND g_FMMC_STRUCT.iPerRuleRespawnVehicle[iRule][iTeam] != -1
									mnToRespawnInto = INT_TO_ENUM(MODEL_NAMES, g_FMMC_STRUCT.iPerRuleRespawnVehicle[iRule][iTeam])
								ENDIF
								
								IF NOT IS_CARGOBOB_MANUAL_RESPAWN_OVERRIDE()
								AND NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_CLEANUP_RESPAWN_SPAWN_AT_START_NO_VEH)
								AND IS_MODEL_VALID(mnToRespawnInto)
									IF bIsDriver
									OR NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
										// Make sure the team respawn vehicle is what we want it to be
										SET_PLAYER_RESPAWN_IN_VEHICLE(TRUE, mnToRespawnInto, FALSE, DEFAULT, DEFAULT,
										IS_BIT_SET(g_fmmc_struct.iTeamVehicleBitSet, ciFMMC_TeamVehBS_MAKE_TEAM_RESPAWN_VEHICLE_STRONG),
										sLastVehicleInformation.iVehicleColour1,
										IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ iPartToUse ].iteam].iTeamBitset, ciBS_RESPAWN_VEHICLE_BULLETPROOF_TIRES),
										DEFAULT, DEFAULT, DEFAULT)
										
										SET_BIT(iLocalBoolCheck30, LBOOL30_MANUAL_RESPAWN_USING_TEAM_VEHICLE)
									ENDIF
								ENDIF
								PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Detaching anything the player might be carrying" )
								
								IF detachPortablePickupFromSpecificPed != NULL
									CALL detachPortablePickupFromSpecificPed(LocalPlayerPed)
								ENDIF
								
								IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
									IF IS_VEHICLE_SIREN_ON(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
										SET_BIT(iLocalBoolCheck28, LBOOL28_SIREN_ACTIVE_MANUAL_RESPAWN)
										PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Siren is active, setting LBOOL28_SIREN_ACTIVE_MANUAL_RESPAWN")
									ENDIF
								ENDIF
								
								PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] going to eManualRespawnState_FADING_OUT" )
								
								START_NET_TIMER(tdManualRespawnTeamVehicle_FailTimedOut)

								manualRespawnState = eManualRespawnState_FADING_OUT
								
								g_SpawnData.bIsSwoopedUp = FALSE // to ensure the fade out happens
							ENDIF
						ENDIF
					ENDIF
				ELSE // Else we're not currently in a vehicle, we're pressing triangle for another reason
					RESET_NET_TIMER( myRespawnTimer )
				ENDIF
			ENDIF
		ELSE // Else the button for leaving a vehicle is not pressed			
			RESET_NET_TIMER( myRespawnTimer )
		ENDIF
	ENDIF // End of eManualRespawnState_OKAY_TO_SPAWN
	
	// Don't play animations (putting on helmets, closing doors, etc.)
	IF (manualRespawnState >= eManualRespawnState_FADING_OUT)
		SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableInVehicleActions, TRUE)
	ENDIF
	
	// update the spawn facing coords constantly after this point - fix for 2454785
	IF iRule < FMMC_MAX_RULES
		IF (manualRespawnState >= eManualRespawnState_FADING_OUT)
		AND NOT IS_CARGOBOB_MANUAL_RESPAWN_OVERRIDE()
		AND NOT USING_SPAWN_AREA_ZONE_RESPAWN_HEADING()
			// Make sure we spawn towards an interesting feature
			PRINTLN("[Spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Setting player respawn facing their current objective")
			SET_PLAYER_WILL_SPAWN_FACING_COORDS(GET_COORDS_OF_CLOSEST_OBJECTIVE(), TRUE, FALSE)
		ENDIF
	ENDIF
	
	IF manualRespawnState = eManualRespawnState_FADING_OUT
	
		BOOL bShouldVehicleFadeOut = FALSE
		BOOL bisReady = TRUE
		
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			// Leave Vehicle so driver can clean it up...
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
			AND NOT bIsDriver
				SET_ENTITY_VISIBLE(localPlayerPed, FALSE)
				SET_ENTITY_ALPHA(localPlayerPed, 0, FALSE)
				
				IF NOT IS_ENTITY_VISIBLE(localPlayerPed)
				AND NOT IS_ENTITY_VISIBLE_TO_SCRIPT(localPlayerPed)
				AND GET_ENTITY_ALPHA(localPlayerPed) <= 0
					PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] (Passenger) Tasking to Leave the Vehicle.")
					bisReady = FALSE
					CLEAR_PED_TASKS_IMMEDIATELY(localPlayerPed)
				ENDIF
			ELSE			
				VEHICLE_INDEX vehToCheck = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				IF IS_VEHICLE_EMPTY(vehToCheck, FALSE, FALSE, TRUE)
					bShouldVehicleFadeOut = TRUE
				ELSE
					PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Vehicle is not empty, not fading out")
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] (Passenger) Setting Respawn bitset. ciBS_MAN_RESPAWN_PASSENGERS_LEFT_VEHICLE")
			SET_BIT(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_PASSENGERS_LEFT_VEHICLE)
		ENDIF
		
		// Clear/Remove held objects on manual respawn. (Actual Removal of object(s))
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_ANY_MANUAL_RESPAWN_DROP_PACKAGES)
			IF NOT bIsAnySpectator
				IF NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_MANUAL_RESPAWN_DROP_PICKUPS)
					PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - LBOOL24_MANUAL_RESPAWN_DROP_PICKUPS Calling to detach.")
					
					IF detachPortablePickupFromSpecificPed != NULL
						CALL detachPortablePickupFromSpecificPed(localPlayerPed)
					ENDIF
					IF detachAndResetAnyPickups != NULL
						CALL detachAndResetAnyPickups()
					ENDIF

					MC_playerBD[iLocalPart].iObjCarryCount = 0
					SET_BIT(iLocalBoolCheck24, LBOOL24_MANUAL_RESPAWN_DROP_PICKUPS)
					
					// Reset This...
					INT iObj
					FOR iobj = 0 TO ( MC_serverBD.iNumObjCreated - 1 )					
						OBJECT_INDEX tempObj
						IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iObj))
							tempObj = NET_TO_OBJ(GET_OBJECT_NET_ID(iObj))
							PREVENT_COLLECTION_OF_PORTABLE_PICKUP(tempObj, TRUE, TRUE)
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
		
		IF FadeOutForRespawn( 250, bShouldVehicleFadeOut )
		AND bisReady
			PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] going to eManualRespawnState_RESPAWNING" )
		
			IF iRule >= FMMC_MAX_RULES
			AND SHOULD_THIS_RULE_USE_CUSTOM_SPAWN_POINTS(DEFAULT, iPrevValidRule)
			AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_FORCE_CUSTOM_SPAWNPOINTS_AT_END)
				eManualRespawnState_SpawnLocation = SPAWN_LOCATION_CUSTOM_SPAWN_POINTS
				PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Mission's ending, pretend it's the previous rule we were on")
			ELSE
				IF iRule < FMMC_MAX_RULES
					
					IF NOT IS_CARGOBOB_MANUAL_RESPAWN_OVERRIDE()
						IF SHOULD_USE_CUSTOM_SPAWN_POINTS()
						
							CLEAR_SPECIFIC_SPAWN_LOCATION()	
							ADD_ALL_CUSTOM_SPAWN_POINTS()
							
							eManualRespawnState_SpawnLocation = SPAWN_LOCATION_CUSTOM_SPAWN_POINTS
							PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - storing eManualRespawnState_SpawnLocation as SPAWN_LOCATION_CUSTOM_SPAWN_POINTS") 
						ELIF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetTwo[ iRule ], ciBS_RULE2_RESPAWN_AT_START_POINT )
							INT iteamslot = GET_PARTICIPANT_NUMBER_IN_TEAM()

							CLEAR_SPECIFIC_SPAWN_LOCATION()

							IF iteamslot < FMMC_MAX_TEAMSPAWNPOINTS          
						        IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iteamslot].vPos)
									SETUP_SPECIFIC_SPAWN_LOCATION(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iteamslot].vPos, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iteamslot].fHead,5,FALSE,0,TRUE,FALSE,0)
						   		ELSE
									SETUP_SPECIFIC_SPAWN_LOCATION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vStartPos, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fStartHeading)
								ENDIF
							ENDIF
							
							eManualRespawnState_SpawnLocation = SPAWN_LOCATION_AT_SPECIFIC_COORDS
							PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN]- storing eManualRespawnState_SpawnLocation as SPAWN_LOCATION_AT_SPECIFIC_COORDS") 
						ELIF IS_SPAWN_AREA_LARGE()
							eManualRespawnState_SpawnLocation = SPAWN_LOCATION_MISSION_AREA_NEAR_CURRENT_POSITION
							PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - storing eManualRespawnState_SpawnLocation as SPAWN_LOCATION_MISSION_AREA_NEAR_CURRENT_POSITION") 
						ELIF DOES_SPAWN_AREA_EXIST()
							eManualRespawnState_SpawnLocation = SPAWN_LOCATION_MISSION_AREA
							PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - storing eManualRespawnState_SpawnLocation as SPAWN_LOCATION_MISSION_AREA") 
						ELSE
							eManualRespawnState_SpawnLocation = SPAWN_LOCATION_NEAR_CURRENT_POSITION
							PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - storing eManualRespawnState_SpawnLocation as SPAWN_LOCATION_NEAR_CURRENT_POSITION") 
						ENDIF
					ELSE
						INT iSlotInTeam = GET_PARTICIPANT_NUMBER_IN_TEAM() 
						PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - Player slot = ", iSlotInTeam)
						
						VECTOR vCustomSpawnPoint
						FLOAT fCustomHeading

						CLEAR_SPECIFIC_SPAWN_LOCATION()
						eManualRespawnState_SpawnLocation = SPAWN_LOCATION_AT_SPECIFIC_COORDS

						IF g_SpawnData.bCargobobUseBehaviour
							g_SpawnData.iClosestCBPlayerIndex = -1
							g_SpawnData.fClosestCBPlayerDist = -1
							
							PRINTLN("=*=*=*=*=*= [spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Trying for cargobob respawn point =*=*=*=*=*=")
							INT i, y
							FOR i = 0 TO 3
								PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] >>>> Respawn point ", i)
								
								IF NOT ARE_VECTORS_EQUAL(g_SpawnData.vCargobobSpawnCoord[i], <<-1,-1,-1>>)
									
									//Get the closest ped to the spawn point
									PRINTLN("[PLAYER_LOOP] - PROCESS_MANUAL_RESPAWN")
									FOR y = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS()-1
										IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(y))
										AND NOT (PARTICIPANT_ID() = INT_TO_PARTICIPANTINDEX(y))
										 	IF NOT IS_PED_INJURED(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(y))))
												IF g_SpawnData.fClosestCBPlayerDist = -1
													g_SpawnData.fClosestCBPlayerDist = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(y)))), g_SpawnData.vCargobobSpawnCoord[i])
													g_SpawnData.iClosestCBPlayerIndex = y
													
													#IF IS_DEBUG_BUILD
														PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] ",y,": Distance to closest player not set. Setting to player ", y, ". Distance = ", g_SpawnData.fClosestCBPlayerDist)
													#ENDIF
												ELSE
													IF VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(y)))), g_SpawnData.vCargobobSpawnCoord[i]) 
													< g_SpawnData.fClosestCBPlayerDist
													
														g_SpawnData.iClosestCBPlayerIndex = y
														g_SpawnData.fClosestCBPlayerDist = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(y)))), g_SpawnData.vCargobobSpawnCoord[i])
													
														#IF IS_DEBUG_BUILD
															PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] ",y,": Setting to player ", y, ". Distance is ", g_SpawnData.fClosestCBPlayerDist, ".")
															
														#ENDIF
													ELSE
														#IF IS_DEBUG_BUILD
															PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] ",y,": Player distance is ",VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(y)))), g_SpawnData.vCargobobSpawnCoord[i]) 
																	," which isn't closer than player ", g_SpawnData.iClosestCBPlayerIndex, ".")
														#ENDIF
													ENDIF
												ENDIF
											#IF IS_DEBUG_BUILD
											ELSE
												PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] ",y,": Ped is injured.")
											#ENDIF
											ENDIF
										ENDIF
									ENDFOR
									
									//1.002768 is on the spawn point
									//1.111159 is right next to the spawn point
									//1.350259 is distance between slot 1 and 2
									IF g_SpawnData.fClosestCBPlayerDist > 1.2
										PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Closest player is player ", g_SpawnData.iClosestCBPlayerIndex, " to spawn slot ", i, ". They are above the 1.2m cutoff so spawning here.")
										vCustomSpawnPoint = g_SpawnData.vCargobobSpawnCoord[i]
										fCustomHeading = g_SpawnData.fCargobobSpawnHeading[i]
										g_SpawnData.iClosestCBPlayerIndex = CB_VALID_SLOT_CONFIRM
										i = 4
									ENDIF
								ELSE
									PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Respawn point ", i, " is equal to <<-1,-1,-1>>! Not good.")
								ENDIF
								
								//If it hasn't found a valid slot and is moving onto the next one, reset these
								IF g_SpawnData.iClosestCBPlayerIndex != CB_VALID_SLOT_CONFIRM
									
									g_SpawnData.iClosestCBPlayerIndex = -1
									g_SpawnData.fClosestCBPlayerDist = -1
								ENDIF
							ENDFOR
							
							IF g_SpawnData.iClosestCBPlayerIndex != CB_VALID_SLOT_CONFIRM
								PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] We haven't found a suitable spawn slot so using the backup one.")
								vCustomSpawnPoint = g_SpawnData.vCargobobBackupCoord
								fCustomHeading = g_SpawnData.fCargobobSpawnHeading[0]
							ENDIF
							
						ENDIF
						
						PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - Manual respawn in cargobob: ", vCustomSpawnPoint, ", heading: ", fCustomHeading)
						SETUP_SPECIFIC_SPAWN_LOCATION(vCustomSpawnPoint + << 0,0,0.25 >>, fCustomHeading, 5,FALSE,0,TRUE,FALSE,0)
					ENDIF
				ELSE
					eManualRespawnState_SpawnLocation = SPAWN_LOCATION_NEAR_CURRENT_POSITION
					PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - storing eManualRespawnState_SpawnLocation as SPAWN_LOCATION_NEAR_CURRENT_POSITION, out of rules!") 
				ENDIF
			ENDIF
			
			IF g_SpawnData.bUseVehStealthMode
				SET_BIT(iVehStealthMode_MC_BS, ci_VehStealthMode_ActiveBeforeManualRespawn)
				PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Remembering that stealth mode was active!")
			ELSE
				CLEAR_BIT(iVehStealthMode_MC_BS, ci_VehStealthMode_ActiveBeforeManualRespawn)
				PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Remembering that stealth mode was NOT active!")
			ENDIF
			
			SET_ENTITY_VISIBLE(LocalPlayerPed, FALSE)
			manualRespawnState = eManualRespawnState_RESPAWNING
		ELSE
			DISABLE_VEHICLE_EXIT_THIS_FRAME()
			PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] fading out...." )	
		ENDIF
	ENDIF
	
	// This actually does the repositioning and sets up a timer
	IF ( manualRespawnState = eManualRespawnState_RESPAWNING )
								
		// And doubly make sure we don't get out just yet
		DISABLE_VEHICLE_EXIT_THIS_FRAME()
		
		IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)		//  B* 2547527 - Make sure the player isn't in a vehicle when setting collision as this causes an assert
			SET_ENTITY_COLLISION( LocalPlayerPed, FALSE )	//	B* 2529713 - Ensure that the player themselves don't have collision when respawning
		ENDIF
			
		IF NOT IS_ENTITY_VISIBLE(LocalPlayerPed)
			PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - Local player is not visible, warping...")
			
			BOOL bReadyToWarp = TRUE
			BOOL bCompletedWarp = FALSE

			IF NOT bIsDriver
				bReadyToWarp = FALSE
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
				AND (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitSet3, ciBS3_ENABLE_FORCE_PASSENGERS_TO_RESPAWN_ON_DRIVER_RESP) OR HAS_MANUAL_RESPAWN_VEH_READY_FOR_MY_DRIVER())
				AND (HAS_FORCED_RESPAWN_STARTED_FOR_MY_DRIVER() OR HAS_MANUAL_RESPAWN_VEH_READY_FOR_MY_DRIVER())
					INT iDriverPart = GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())
					
					IF iDriverPart > -1
						bReadyToWarp = FALSE
						
						PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] (Passenger) bReadyToWarp = FALSE")
						
						PED_INDEX pedDriver = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDriverPart)))
						
						IF IS_PED_IN_ANY_VEHICLE(pedDriver)
							VEHICLE_INDEX vehDriver = GET_VEHICLE_PED_IS_IN(pedDriver)
							
							IF DOES_ENTITY_EXIST(vehDriver)
							AND IS_VEHICLE_DRIVEABLE(vehDriver)
							AND NOT IS_ENTITY_IN_WATER(vehDriver)
								bReadyToWarp = TRUE
								PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] (Passenger) bReadyToWarp = TRUE")
							ENDIF
						ENDIF
					ENDIF
				ENDIF				
			ENDIF
			
			IF bReadyToWarp
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
				OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN) AND bIsDriver
					
					IF WARP_TO_SPAWN_LOCATION( 	eManualRespawnState_SpawnLocation,		// Spawn location
						FALSE,	// Leave ped behind?
						NOT IS_CARGOBOB_MANUAL_RESPAWN_OVERRIDE(),	// Warp into spawn vehicle?
						FALSE,	// Do a teammate visibility check?
						FALSE,	// Do a quick warp? Neilf: we need to do a load scene as we could be some distance away.
						FALSE,	// Don't ask permission? Neilf: we need to ask permission so we dont have 2 players respawn in the same place
						FALSE,	// Don't use vehicle nodes?
						NOT IS_CARGOBOB_MANUAL_RESPAWN_OVERRIDE(),	// Keep current vehicle?
						FALSE,	// Use car node offset?
						DEFAULT,// Respot time
						DEFAULT,// Move game camera near player if close to wall?
						TRUE	)// bMinTimeHasPassed - this must be true else it won't work :D	
						
						PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] (Driver) WARP_TO_SPAWN_LOCATION complete")
						
						bCompletedWarp = TRUE
						
						PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] (Driver) Setting gameplay cam relative pitch and heading to 0.0")
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					ENDIF
				ELSE
					// Multipassenger.
					IF SET_PASSENGER_INTO_VEHICLE()
						PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] (Passenger) Setting Respawn bitset. ciBS_MAN_RESPAWN_ENTERED_VEHICLE")
						SET_BIT(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_ENTERED_VEHICLE)
						
						bCompletedWarp = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF bCompletedWarp
				PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] The player has been respawned" )
				
				IF NOT IS_CARGOBOB_MANUAL_RESPAWN_OVERRIDE()
					IF bIsDriver OR NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
						
						PROCESS_PLAYER_RESPAWN_SETTINGS(MC_playerBD[iLocalPart].iTeam)
						
						IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							vehPlayer = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
							
							PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS(vehPlayer, FALSE, MC_playerBD[iLocalPart].iTeam, IS_BIT_SET(iLocalBoolCheck30, LBOOL30_MANUAL_RESPAWN_USING_TEAM_VEHICLE))
							
							// Make sure the vehicle the player is in actually exists
							IF NOT IS_VEHICLE_FUCKED( vehPlayer )							
								// 2050089
								
								IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
									
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset, ciBS_FORCE_RESPAWN_WHEN_NOT_IN_RESPAWN_VEHICLE)
										REMOVE_WEAPON_FROM_PED(LocalPlayerPed, GADGETTYPE_PARACHUTE)
										PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - Removing parachute on respawn, because of ciBS_FORCE_RESPAWN_WHEN_NOT_IN_RESPAWN_VEHICLE")
									ENDIF
									
									PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] About to handle Stealth Vehicle state due to manually respawning!")
									HANDLE_VEH_STEALTH_SPAWNING(IS_BIT_SET(iVehStealthMode_MC_BS, ci_VehStealthMode_ActiveBeforeManualRespawn))
									
								ENDIF
									
								IF (g_SpawnData.MissionSpawnDetails.bUseRaceRespotAfterRespawns)
									SET_PLAYER_HAS_JUST_RESPAWNED_IN_RACE()
								ENDIF
								
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset, ciBS_FORCE_RESPAWN_WHEN_NOT_IN_RESPAWN_VEHICLE)
									REMOVE_WEAPON_FROM_PED(LocalPlayerPed, GADGETTYPE_PARACHUTE)
									PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - Removing parachute on respawn, because of ciBS_FORCE_RESPAWN_WHEN_NOT_IN_RESPAWN_VEHICLE")
								ENDIF
								
								PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] going to eManualRespawnState_FADING_IN" )
								manualRespawnState = eManualRespawnState_FADING_IN
							
							ELSE
								PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] IS_VEHICLE_FUCKED = TRUE " )	
							ENDIF
						ENDIF
					ELSE
						IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							manualRespawnState = eManualRespawnState_FADING_IN
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] going to eManualRespawnState_FADING_IN. This is a cargobob man respawn." )	
					manualRespawnState = eManualRespawnState_FADING_IN
				ENDIF
			ELSE
				IF g_bMissionOver
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_DONT_CANCEL_MANUAL_RESPAWN_AT_END)
					PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] going to eManualRespawnState_FADING_IN. The mission is ending!!" )
					SET_ENTITY_VISIBLE(LocalPlayerPed, TRUE)
					IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed) //SET_ENTITY_COLLISION asserts if the ped is in a vehicle
						SET_ENTITY_COLLISION(LocalPlayerPed, TRUE)
					ENDIF
					
					manualRespawnState = eManualRespawnState_FADING_IN
				ENDIF
			ENDIF // End of IF we've successfully respawned
		ELSE
			PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - Local player is still visible, setting invisible!")
			SET_ENTITY_VISIBLE(LocalPlayerPed, FALSE)
		ENDIF


	ENDIF // End of eManualRespawnState_RESPAWNING
	
	IF (manualRespawnState = eManualRespawnState_FADING_IN)
		
		BOOL bReady = TRUE
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
			IF bIsDriver
			
			ELSE
				BOOL bForced = FALSE
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitSet3, ciBS3_ENABLE_FORCE_PASSENGERS_TO_RESPAWN_ON_DRIVER_RESP)
				AND HAS_FORCED_RESPAWN_STARTED_FOR_MY_DRIVER()
					INT iDriverPart = GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())
					IF iDriverPart > -1
						PED_INDEX pedDriver = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDriverPart)))
							
						IF IS_PED_IN_ANY_VEHICLE(pedDriver)
							VEHICLE_INDEX vehDriver = GET_VEHICLE_PED_IS_IN(pedDriver)
							
							IF DOES_ENTITY_EXIST(vehDriver)
							AND IS_VEHICLE_DRIVEABLE(vehDriver)
							AND NOT IS_ENTITY_IN_WATER(vehDriver)
								PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] [MANUAL RESPAWN] (Passenger) Force Us Through.")
								bForced = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT HAS_MANUAL_RESPAWN_DRIVER_FINISHED()
				AND NOT bForced
					PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] [MANUAL RESPAWN] (Passenger) Prevent Us.")
					bReady = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF bReady
			IF FadeInAfterRespawn(250)
	
				PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] The player's vehicle exists" )
				
				NET_SET_PLAYER_CONTROL( LocalPlayer, TRUE )
				
				IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					SET_ENTITY_COLLISION( LocalPlayerPed, TRUE )	//	Resetting collision state.
				ENDIF
				
				
				PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Setting player control to true" )
				
				IF NOT IS_CARGOBOB_MANUAL_RESPAWN_OVERRIDE()
				
					REVEAL_PLAYER_AND_VEHICLE_FOR_WARP()
					
					// Clear/Remove held objects on manual respawn. (Reset Prevent recollection stuff)
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_ANY_MANUAL_RESPAWN_DROP_PACKAGES)
						IF NOT bIsAnySpectator
							IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_MANUAL_RESPAWN_DROP_PICKUPS)
								PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - LBOOL24_MANUAL_RESPAWN_DROP_PICKUPS Calling to reverse prevention measures (PREVENT_COLLECTION_OF_PORTABLE_PICKUP).")
								CLEAR_BIT(iLocalBoolCheck24, LBOOL24_MANUAL_RESPAWN_DROP_PICKUPS)
								
								INT iObj
								FOR iobj = 0 TO ( MC_serverBD.iNumObjCreated - 1 )					
									OBJECT_INDEX tempObj
									IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iObj))
										tempObj = NET_TO_OBJ(GET_OBJECT_NET_ID(iObj))
										PREVENT_COLLECTION_OF_PORTABLE_PICKUP(tempObj, FALSE, TRUE)
									ENDIF
								ENDFOR
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						vehPlayer = GET_VEHICLE_PED_IS_IN( LocalPlayerPed )
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehPlayer)
					AND NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
						// Then do some model specific stuff
						IF IS_THIS_MODEL_A_PLANE( GET_ENTITY_MODEL( vehPlayer ) )
						OR IS_THIS_MODEL_A_HELI( GET_ENTITY_MODEL( vehPlayer ) )
							PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Spawned on a Heli or Plane Vehicle" )
							
							BOOST_AIR_VEHICLE( vehPlayer )		
							
						ELIF IS_THIS_MODEL_A_CAR( GET_ENTITY_MODEL( vehPlayer ) )
						OR IS_THIS_MODEL_A_BIKE( GET_ENTITY_MODEL( vehPlayer ) )
						OR IS_THIS_MODEL_A_QUADBIKE( GET_ENTITY_MODEL( vehPlayer ) )
							PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] This entity is a car or bike" )

							SET_VEHICLE_ENGINE_ON( vehPlayer, TRUE, TRUE )
							FREEZE_ENTITY_POSITION( vehPlayer, FALSE )
							
						ELIF IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(vehPlayer))
						OR IS_THIS_MODEL_A_JETSKI(GET_ENTITY_MODEL(vehPlayer))
							PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Spawned on a Water-based Vehicle" )
						ELIF IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(vehPlayer))
							PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Spawned on a Bicycle" )
						ELIF IS_THIS_MODEL_A_TRAIN(GET_ENTITY_MODEL(vehPlayer))
							PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Spawned on a Train" )
						ELSE
							PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Vehicle Check Failed - we didn't spawn on a valid vehicle which the native checks recognise" )
						ENDIF
						
					ENDIF
					
					IF g_vehStandbyVehicle != vehPlayer
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLEAR_UP_VEHICLE_ON_DEATH)
							PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - ciCLEAR_UP_VEHICLE_ON_DEATH is set")
							IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_VEHICLE_MARKED_FOR_CLEAN_UP)
								SET_BIT(iLocalBoolCheck16, LBOOL16_VEHICLE_MARKED_FOR_CLEAN_UP)
							ENDIF
							START_NET_TIMER(vehicleCleanUpTimer)
							netVehicleToCleanUp = netRespawnVehicle
							
							PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - ciCLEAR_UP_VEHICLE_ON_DEATH setting netRespawnVehicle")
							SET_ENTITY_AS_MISSION_ENTITY(vehPlayer, FALSE, TRUE)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
								SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(VEH_TO_NET(vehPlayer), TRUE)
							ENDIF
							netRespawnVehicle = VEH_TO_NET(vehPlayer)
						ELSE
							IF NETWORK_DOES_NETWORK_ID_EXIST(netRespawnVehicle)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID( netRespawnVehicle )
									PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - Cleaning up old respawn vehicle")
									SET_ENTITY_COLLISION( NET_TO_VEH( netRespawnVehicle ), FALSE ) 
									SET_ENTITY_VISIBLE( NET_TO_VEH( netRespawnVehicle ), FALSE ) 
									DELETE_NET_ID(netRespawnVehicle)
								ENDIF
							ENDIF
							
							IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(vehPlayer)
								netRespawnVehicle = VEH_TO_NET(vehPlayer)
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
				
				IF g_FMMC_STRUCT.fManualRespawnTimeLose[MC_playerBD[iLocalPart].iteam] != 0
				AND IS_THERE_ENOUGH_TIME_LEFT_TO_DECREMENT_REMAINING_TIME(iRule, iTeam)
				AND NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_FORCED_MANUAL_RESPAWN)
					BROADCAST_FMMC_DECREMENT_REMAINING_TIME(MC_playerBD[iLocalPart].iteam, ROUND(g_FMMC_STRUCT.fManualRespawnTimeLose[MC_playerBD[iLocalPart].iteam] * 1000))
				ENDIF
				
				// If we're forced to respawn, give us full health
				IF bForceRespawnNow
					SET_ENTITY_HEALTH(LocalPlayerPed, GET_ENTITY_MAX_HEALTH(LocalPlayerPed))
				ENDIF

				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciRESPAWN_WITH_FULL_HEALTH)
					SET_ENTITY_HEALTH(LocalPlayerPed, GET_PED_MAX_HEALTH(LocalPlayerPed))
				ENDIF
				
				PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - Resetting or starting the timer that allows the next respawn to happen" )
				
				IF HAS_NET_TIMER_STARTED( timerForAllowingRespawn )
					RESET_NET_TIMER( timerForAllowingRespawn )
				ELSE
					START_NET_TIMER( timerForAllowingRespawn )
				ENDIF
				
				//-- Give player weapons when respawning?
				IF IS_BIT_SET( g_FMMC_STRUCT.iRuleOptionsBitSet, ciGIVE_WEAPONS_ON_RESPAWN)
					IF NOT IS_BIT_SET(iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY)
						PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - Giving player weapons - no mid-mission inventory" )
						GIVE_FORCED_WEAPON_AND_AMMO(FALSE, iDlcWeaponBitSet, wtWeaponToRemove, TRUE)
					ELSE
						PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - Giving player weapons - with mid-mission inventory" )
						GIVE_FORCED_WEAPON_AND_AMMO(TRUE, iDlcWeaponBitSet, wtWeaponToRemove, TRUE)
						GIVE_FORCED_WEAPON_AND_AMMO(TRUE, iDlcWeaponBitSet, wtWeaponToRemove, FALSE)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset3, ciBS3_TEAM_DISABLE_HELMET_ARMOUR_FROM_OUTFIT)
					PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - ciBS3_TEAM_DISABLE_HELMET_ARMOUR_FROM_OUTFIT is set. Calling: PCF_DisableHelmetArmor")
					SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableHelmetArmor, TRUE)
				ENDIF
				
				RUN_LOCAL_PLAYER_PROOF_SETTING()
				
				SET_UP_LOCAL_PLAYER_PROOF_SETTINGS()
				
				PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Fading the screen back in" )
				
				CLEAR_BIT(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS1_CHOSEN_IN_FRONT)
				CLEAR_BIT(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS1_CHOSEN_NOT_IN_FRONT)
				CLEAR_BIT(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS2_CHOSEN_IN_FRONT)
				CLEAR_BIT(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS2_CHOSEN_NOT_IN_FRONT)
				CLEAR_BIT(iLocalBoolCheck30, LBOOL30_MANUAL_RESPAWN_USING_TEAM_VEHICLE)
				
				IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_PASSENGER_MANUALLY_RESPAWNING)
					PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Clearing iLocalBoolCheck25, LBOOL25_PASSENGER_MANUALLY_RESPAWNING")
					CLEAR_BIT(iLocalBoolCheck25, LBOOL25_PASSENGER_MANUALLY_RESPAWNING)
				ENDIF
				
				IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_FORCED_MANUAL_RESPAWN)
					PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Clearing LBOOL28_FORCED_MANUAL_RESPAWN")
					CLEAR_BIT(iLocalBoolCheck28, LBOOL28_FORCED_MANUAL_RESPAWN)
				ENDIF
				
				// [SPEC_SPEC] - If we want to force players back into playing even if they went to proper spectator due to Bounds Fail or Out of Lives fail.
				IF IS_SPECIAL_CONTROLLER_RESPAWN_REQUESTED()
					PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN][SPEC_SPEC] Clearing Spectator bits from Special Spectator Respawn.")
					SET_BIT(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Cleanup)
					NET_SET_PLAYER_CONTROL(localPlayer, TRUE)
					REAPPLY_SETTINGS_FROM_SPECTATOR()
					SET_ENTITY_VISIBLE(LocalPlayerPed, TRUE)
					CLEAR_TIMECYCLE_MODIFIER()
				ENDIF				
				
				manualRespawnState = eManualRespawnState_WAITING_AFTER_RESPAWN
				
			ELSE
				PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] fading in after respawn..." )
			ENDIF
		ENDIF
	ENDIF
	
	// This is the final state, post-respawn, that waits for the player to let go of the triangle button
	// and waits for a timer to end in order to let them respawn again
	IF ( manualRespawnState = eManualRespawnState_WAITING_AFTER_RESPAWN ) 
	                                                               
		PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Waiting for the timer to end so the player can respawn again" )
			
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
			PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] Clearing Respawn bitsets. and setting that we are finished: ciBS_MAN_RESPAWN_FINISHED")
			CLEAR_BIT(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_STARTED)
			CLEAR_BIT(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_ENTERED_VEHICLE)
			CLEAR_BIT(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_PASSENGERS_LEFT_VEHICLE)
			CLEAR_BIT(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_FORCED)
			
			RESET_NET_TIMER(tdManualRespawnTeamVehicle_FailTimedOut)
			
			SET_BIT(iLocalBoolCheck25, LBOOL25_DRIVER_DIED_OR_RESPAWNED_INTO_NEW_VEH)
			
			IF bIsDriver
				SET_BIT(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_FINISHED)
			ENDIF
		ENDIF
			
		iAllowRespawnTime_ms = 3 * 1000
		iTimeAfterLastRespawn = GET_TIME_DIFFERENCE( GET_NETWORK_TIME(), timerForAllowingRespawn.Timer )
	
		IF( iTimeAfterLastRespawn >= iAllowRespawnTime_ms )
		AND IS_SCREEN_FADED_IN()
			
			IF ( NOT IS_CONTROL_PRESSED( PLAYER_CONTROL, INPUT_VEH_EXIT )
			AND NOT IS_DISABLED_CONTROL_PRESSED( PLAYER_CONTROL, INPUT_VEH_EXIT ) )
				PRINTLN("[RCC MISSION] [MANUAL RESPAWN] Timer has finished - we should be good to respawn again now" )
				
				RESET_NET_TIMER( tdRespawnFailTimer )
				RESET_NET_TIMER( myRespawnTimer )
				
				RESET_NET_TIMER( tdForceVehicleRespawnTimer )
				RESET_NET_TIMER( tdForceVehicleRespawnInWaterTimer )
				RESET_NET_TIMER( tdForceUndriveableVehicleRespawnTimer )
								
				manualRespawnState = eManualRespawnState_OKAY_TO_SPAWN
			ELSE
				IF HAS_NET_TIMER_STARTED( tdRespawnFailTimer )
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdRespawnFailTimer) > iAllowRespawnTime_ms
						PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - Timer has finished - we should be good to respawn again now, even though player was trying to exploit holding down respawn button" )
						RESET_NET_TIMER( tdRespawnFailTimer )
						RESET_NET_TIMER( myRespawnTimer )
						
						RESET_NET_TIMER( tdForceVehicleRespawnTimer )
						RESET_NET_TIMER( tdForceVehicleRespawnInWaterTimer )
						RESET_NET_TIMER( tdForceUndriveableVehicleRespawnTimer )
							
						manualRespawnState = eManualRespawnState_OKAY_TO_SPAWN
					ELSE
						PRINTLN("[spawning][RCC MISSION][PROCESS_MANUAL_RESPAWN] - Someone's trying an exploit. Let them think its working for three seconds." )
					ENDIF
				ELSE
					REINIT_NET_TIMER( tdRespawnFailTimer )
				ENDIF
			ENDIF
		ENDIF
	ENDIF // End of eManualRespawnState_WAITING_AFTER_RESPAWN
ENDPROC

FUNC BOOL SHOULD_THIS_TEAM_RESPAWN_NOW(INT iTeam)
	RETURN IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_RESPAWN_ALL_TEAM_MEMBERS_TEAM_0 + iTeam)
ENDFUNC

FUNC BOOL IS_RESPAWN_ALL_PLAYERS_ON_KILL_ENABLED_FOR_ANY_TEAM()
	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[GET_TEAM_CURRENT_RULE(iTeam)], ciBS_RULE15_RESPAWN_ALL_PLAYERS_ON_KILL)
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_RESPAWN_ALL_PLAYERS_ON_KILL_ENABLED_FOR_THIS_TEAM(INT iTeamToCheck)
	RETURN IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamToCheck].iRuleBitsetFifteen[GET_TEAM_CURRENT_RULE(iTeamToCheck)], ciBS_RULE15_RESPAWN_ALL_PLAYERS_ON_KILL)
ENDFUNC

//Check if any other team has the option enabled
FUNC BOOL SHOULD_PLAYER_PROCESS_KILL_DUE_RESPAWN_ALL_PLAYERS_ON_KILL_RULE()
	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		IF iTeam != MC_playerBD[iLocalPart].iTeam
		AND IS_RESPAWN_ALL_PLAYERS_ON_KILL_ENABLED_FOR_THIS_TEAM(iTeam)
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_WAIT_IN_SPECTATOR_FOR_RESPAWN()
	INT iTeam = MC_playerBD[iLocalPart].iTeam

	IF iTeam < 0
		EXIT
	ENDIF
	
	//Wait for the killstrip - start the timer
	IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
	AND NOT HAS_NET_TIMER_STARTED(tdSpectateKillStripTimer)
		START_NET_TIMER(tdSpectateKillStripTimer)
		g_bMissionHideRespawnBar = TRUE
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
		PRINTLN("[SPECTATE] - PROCESS_WAIT_IN_SPECTATOR_FOR_RESPAWN - Waiting for the kill strip to be over (Start Timer)")
		EXIT
	ENDIF
	
	//Wait for the killstrip - wait for the timer to expire
	IF HAS_NET_TIMER_STARTED(tdSpectateKillStripTimer)
	AND NOT HAS_NET_TIMER_EXPIRED(tdSpectateKillStripTimer, KILL_STRIP_RESPAWN_TIME)
		g_bMissionHideRespawnBar = TRUE
		PRINTLN("[SPECTATE] - PROCESS_WAIT_IN_SPECTATOR_FOR_RESPAWN - Waiting for the kill strip to be over")
		EXIT
	ENDIF
	
	PLAYER_INDEX toSpectate = INVALID_PLAYER_INDEX()
	BOOL bNewSpectateTarget = FALSE
	
	//Find a new target to spectate
	IF MPGlobalsAmbience.piHeistSpectateTarget = INVALID_PLAYER_INDEX()
	OR MPGlobalsAmbience.piHeistSpectateTarget = LocalPlayer
	OR NOT IS_NET_PLAYER_OK(MPGlobalsAmbience.piHeistSpectateTarget)
	OR NOT USING_HEIST_SPECTATE()
		
		PRINTLN("[SPECTATE] - PROCESS_WAIT_IN_SPECTATOR_FOR_RESPAWN - Finding a new target to spectate, DO SCREEN FADE OUT")
		DO_SCREEN_FADE_OUT(500)
		BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("") 
		END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING)) 
		
		INT iPart
		PARTICIPANT_INDEX tempPart
		PLAYER_INDEX tempPlayer
		FOR iPart = 0 TO MAX_NUM_MC_PLAYERS - 1
			IF iPart != iLocalPart AND MC_playerBD[iPart].iTeam = iTeam
				tempPart = INT_TO_PARTICIPANTINDEX(iPart)
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
				AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
				AND IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
				AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
					tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
					IF IS_NET_PLAYER_OK(tempPlayer)
					AND NOT IS_PARTICIPANT_A_SPECTATOR(iPart)
					AND NOT	IS_PART_WAITING_IN_SPECTATE(iPart)
						//Target found
						toSpectate = tempPlayer
						bNewSpectateTarget = TRUE
						PRINTLN("[SPECTATE] - PROCESS_WAIT_IN_SPECTATOR_FOR_RESPAWN - Found a valid target to spectate - player ", GET_PLAYER_NAME(tempPlayer))
						BREAKLOOP
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
		
	IF bNewSpectateTarget
		MPGlobalsAmbience.piHeistSpectateTarget = toSpectate
		PRINTLN("[SPECTATE] - PROCESS_WAIT_IN_SPECTATOR_FOR_RESPAWN - ENABLING STATIC CAM")
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_USING_BIRDS_EYE_CAMERA)
		SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_TURF_WARS_CAMER_ACTIVE)
		g_iFMMCCurrentStaticCam = 0		
		DISABLE_HEIST_SPECTATE(FALSE)	
		SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DO_HEIST_SPECTATE)
		SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iSpecInfoBitset, SPEC_INFO_BS_USING_HEIST_SPECTATE)
		SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_HEIST_SPECTATE_WHILE_DEAD_ENABLED)
		CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_STOP_HEIST_SPECTATE_NOW)
		SET_BIT(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitset[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]], ciBS_RULE_ENABLE_SPECTATE)
		NETWORK_SET_OVERRIDE_SPECTATOR_MODE(TRUE)
		RESET_NET_TIMER(tdSpectatorDelayTimer)
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
		PRINTLN("[SPECTATE] - PROCESS_WAIT_IN_SPECTATOR_FOR_RESPAWN - Going to heist spectate, chosen player ", GET_PLAYER_NAME(toSpectate))
		SET_SPECTATOR_CAN_QUIT(FALSE)
		SET_SPECTATOR_HUD_BLOCK_CIRCLE_INSTRUCTION(g_BossSpecData.specHUDData, TRUE)
	ENDIF
ENDPROC

PROC RESPAWN_LOCAL_PLAYER_FROM_SPECTATE()
	IF NOT HAS_TEAM_FAILED(MC_playerBD[iLocalPart].iTeam)
	AND NOT HAS_TEAM_FINISHED(MC_playerBD[iLocalPart].iTeam)
		//Clear heist spectate
		DISABLE_HEIST_SPECTATE(TRUE)
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iSpecInfoBitset, SPEC_INFO_BS_USING_HEIST_SPECTATE)
		CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_HEIST_SPECTATE_WHILE_DEAD_ENABLED)
		SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_STOP_HEIST_SPECTATE_NOW)
		CLEAR_BIT(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitset[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]], ciBS_RULE_ENABLE_SPECTATE)
		MPGlobalsAmbience.piHeistSpectateTarget = INVALID_PLAYER_INDEX()
		
		RESET_NET_TIMER(tdSpectateKillStripTimer)
		
		CLEAR_BIT(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
		CLEAR_BIT(iLocalBoolCheck,LBOOL_COUNTED_FINAL_DEATH)

		BROADCAST_FMMC_SPAWN_PROTECTION_ALPHA(FALSE, iLocalPart, GET_FRAME_COUNT())
		REINIT_NET_TIMER(tdStuckRespawningTimer)
		
		//Clear Birds Eye Camera
		CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_TURF_WARS_CAMER_ACTIVE)
		IF DOES_CAM_EXIST(g_birdsEyeCam)
		AND IS_CAM_ACTIVE(g_birdsEyeCam)
			SET_CAM_ACTIVE(g_birdsEyeCam, FALSE)
		ENDIF
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_HALLOWEEN_ADVERSARY_2022(g_FMMC_STRUCT.iAdversaryModeType)
			SET_BIT(iLocalBoolCheck14, LBOOL14_TRIGGER_HUNTED_RESPAWN_EFFECT)
		ENDIF
		
		SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN_AFTER_TIME)
		PRINTLN("[SPECTATE] - RESPAWN_LOCAL_PLAYER_FROM_SPECTATE - Respawning player ", GET_PLAYER_NAME(LocalPlayer))
	ENDIF
ENDPROC

//This will fire once after the local player respawned from spectate and is alive
PROC PROCESS_LOCAL_PLAYER_JUST_RESPAWNED_FROM_SPECTATE()
	//Reset player weapons
	IF IS_BIT_SET( g_FMMC_STRUCT.iRuleOptionsBitSet, ciGIVE_WEAPONS_ON_RESPAWN)
		REMOVE_ALL_PLAYERS_WEAPONS()
		GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_STARTING, WEAPONINHAND_LASTWEAPON_BOTH)
		PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES][SPECTATE] - PROCESS_LOCAL_PLAYER_JUST_RESPAWNED_FROM_SPECTATE - Reseting player's weapons")
	ENDIF
	SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN)
	CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
ENDPROC

FUNC BOOL IS_ANY_PLAYER_IN_THIS_TEAM_STILL_WAITING_TO_RESPAWN(INT iTeamToCheck)
	INT i
	FOR i = 0 TO MAX_NUM_MC_PLAYERS - 1
		IF  iTeamToCheck = MC_playerBD[i].iTeam
		AND IS_PART_WAITING_IN_SPECTATE(i)
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_PLAYER_STILL_WAITING_TO_RESPAWN()
	INT i
	FOR i = 0 TO MAX_NUM_MC_PLAYERS - 1
		IF IS_PART_WAITING_IN_SPECTATE(i)
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

PROC DISABLE_SPAWN_ALL_TEAM_MEMBERS_FLAG()
	IF bLocalPlayerOK
	AND IS_RESPAWN_ALL_PLAYERS_ON_KILL_ENABLED_FOR_THIS_TEAM(MC_playerBD[iLocalPart].iTeam)
	AND NOT IS_ANY_PLAYER_IN_THIS_TEAM_STILL_WAITING_TO_RESPAWN(MC_playerBD[iLocalPart].iTeam)
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SpawnAllTeamMembers, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE, MC_playerBD[iLocalPart].iTeam)
		PRINTLN("[SPECTATE] DISABLE_SPAWN_ALL_TEAM_MEMBERS_FLAG - called by player ", GET_PLAYER_NAME(LocalPlayer))
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_RESPAWN_FROM_SPECTATE()
	IF SHOULD_THIS_TEAM_RESPAWN_NOW(MC_playerBD[iLocalPart].iTeam)
		//PRINTLN("[SPECTATE] SHOULD_THIS_TEAM_RESPAWN_NOW = TRUE - " , GET_PLAYER_NAME(LocalPlayer))
		//PRINTLN("[SPECTATE] MC_PlayerBD[iLocalPart].iTimeOfLastDeathInSec = ", MC_PlayerBD[iLocalPart].iTimeOfLastDeathInSec, " | MC_serverBD_4.iTeamTimeOfDeath[0] = ", MC_serverBD_4.iTeamTimeOfDeath[0], " | MC_serverBD_4.iTeamTimeOfDeath[1] = ", MC_serverBD_4.iTeamTimeOfDeath[1], " | MC_serverBD_4.iTeamTimeOfDeath[2] = ", MC_serverBD_4.iTeamTimeOfDeath[2], " | MC_serverBD_4.iTeamTimeOfDeath[3] = ", MC_serverBD_4.iTeamTimeOfDeath[3], " |")
		IF IS_PART_WAITING_IN_SPECTATE(iLocalPart)
		AND NOT bLocalPlayerOK
			RESPAWN_LOCAL_PLAYER_FROM_SPECTATE()
		ENDIF
		
		DISABLE_SPAWN_ALL_TEAM_MEMBERS_FLAG()
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PROCESS_PLAYER_RESPAWN_FROM_SPECTATE_CHECKS()
	//Basic checks for all modes
	IF IS_WAIT_IN_SPECTATE_ENABLED_FOR_THIS_TEAM(MC_playerBD[iLocalPart].iTeam)
		//Mode specific checks
		IF IS_RESPAWN_ALL_PLAYERS_ON_KILL_ENABLED_FOR_THIS_TEAM(MC_PlayerBD[iLocalPart].iTeam)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RESET_SPECTATOR_FLAGS()
		//Clear heist spectate
		DISABLE_HEIST_SPECTATE(TRUE)
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iSpecInfoBitset, SPEC_INFO_BS_USING_HEIST_SPECTATE)
		CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_HEIST_SPECTATE_WHILE_DEAD_ENABLED)
		SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_STOP_HEIST_SPECTATE_NOW)
		CLEAR_BIT(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitset[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]], ciBS_RULE_ENABLE_SPECTATE)
		MPGlobalsAmbience.piHeistSpectateTarget = INVALID_PLAYER_INDEX()
		
		//Clear Birds Eye Camera
		CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_TURF_WARS_CAMER_ACTIVE)
		IF DOES_CAM_EXIST(g_birdsEyeCam)
		AND IS_CAM_ACTIVE(g_birdsEyeCam)
			SET_CAM_ACTIVE(g_birdsEyeCam, FALSE)
		ENDIF
		
		SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN)
		PRINTLN("[SPECTATE] - RESET_SPECTATOR_FLAGS - Reseting spectator flags for ", GET_PLAYER_NAME(LocalPlayer))
ENDPROC

FUNC BOOL CAN_ANY_TEAM_RESPAWN_FROM_SPECTATE()
	IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME()
		CLEAR_BIT(iLocalBoolCheck30, LBOOL30_CAN_ANY_TEAM_RESPAWN_FROM_SPECTATE)
		IF IS_RESPAWN_ALL_PLAYERS_ON_KILL_ENABLED_FOR_ANY_TEAM()
			SET_BIT(iLocalBoolCheck30, LBOOL30_CAN_ANY_TEAM_RESPAWN_FROM_SPECTATE)
		ENDIF
	ENDIF
	
	RETURN IS_BIT_SET(iLocalBoolCheck30, LBOOL30_CAN_ANY_TEAM_RESPAWN_FROM_SPECTATE)
ENDFUNC

FUNC BOOL SHOULD_PROCESS_WAIT_IN_SPECTATOR_CHECKS()
	//Are we forcing players on this team to wait in spactate?
	IF IS_WAIT_IN_SPECTATE_ENABLED_FOR_THIS_TEAM(MC_playerBD[iLocalPart].iTeam)
	AND NOT bLocalPlayerOK
		//Make sure we didn't enable the rule while already respawning - url:bugstar:7731771
		IF DOES_LOCAL_PLAYER_HAVE_NEW_PRIORITY_THIS_FRAME()
			//Check if wait in spectate was already enabled for this player on previous rule
			IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_WAIT_IN_SPECTATE_ENABLED)
				//We enabled wait in spectate while the player was respawning! Block it until the player respawns! 
				PRINTLN("[SPECTATE] - Player can't be send to spectate as they entered a new rule while already respawnig!")
				SET_BIT(iLocalBoolCheck30, LBOOL30_BLOCK_WAIT_IN_SPECTATE_UNTIL_PLAYER_RESPAWNS)
				SET_BIT(iLocalBoolCheck30, LBOOL30_WAIT_IN_SPECTATE_ENABLED)
				RETURN FALSE
			ENDIF
		ENDIF
		
		//Rules that allow manual respawn
		IF IS_RESPAWN_ALL_PLAYERS_ON_KILL_ENABLED_FOR_THIS_TEAM(MC_PlayerBD[iLocalPart].iTeam)
		AND SHOULD_THIS_TEAM_RESPAWN_NOW(MC_playerBD[iLocalPart].iTeam)
			RETURN FALSE
		ENDIF
		
		IF IS_LAST_PLAYER_ALIVE_SENT_TO_SPECTATE()
			RESET_SPECTATOR_FLAGS()
			RETURN FALSE
		ENDIF
		
		SET_BIT(iLocalBoolCheck30, LBOOL30_WAIT_IN_SPECTATE_ENABLED)
		
		RETURN TRUE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iRuleBitsetFifteen[GET_TEAM_CURRENT_RULE(MC_playerBD[iLocalPart].iTeam)], ciBS_RULE15_WAIT_IN_SPECTATE)
	AND IS_BIT_SET(iLocalBoolCheck30, LBOOL30_WAIT_IN_SPECTATE_ENABLED)
	AND GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
	AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
	AND NOT IS_LAST_PLAYER_ALIVE_SENT_TO_SPECTATE()
		//We are no longer using wait in spectate on this rule - reset the flag
		CLEAR_BIT(iLocalBoolCheck30, LBOOL30_WAIT_IN_SPECTATE_ENABLED)
		PRINTLN("[SPECTATE] - Clear LBOOL30_WAIT_IN_SPECTATE_ENABLED flag!")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_RESURRECTION_SHARD()
	IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_DISPLAYING_RESURRECTION_SHARD)
		//Display Resurrection Shard - Hunters
		IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_DISPLAY_RESURRECTION_SHARD_FRIENDLY_TEAM) AND IS_SCREEN_FADED_IN()
			IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_ANY_PLAYER_STILL_WAITING_TO_RESPAWN()
				
				IF IS_THIS_ROCKSTAR_MISSION_NEW_HALLOWEEN_ADVERSARY_2022(g_FMMC_STRUCT.iAdversaryModeType)
					PLAY_SOUND_FRONTEND(-1, "Hunted_Resurrected", "Halloween_Adversary_Sounds", FALSE)
				ENDIF
				
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "GR_RES", "GR_RES_MT")
				START_NET_TIMER(tdResurrectionShardTimer)
				SET_BIT(iLocalBoolCheck27, LBOOL27_DISPLAYING_RESURRECTION_SHARD)
				CLEAR_BIT(iLocalBoolCheck27, LBOOL27_DISPLAY_RESURRECTION_SHARD_FRIENDLY_TEAM)
				PRINTLN("[SPECTATE] PROCESS_PLAYER_LIVES - PROCESS_RESPAWN_SHARD - Display Shard Team Respawn")
				EXIT
			ENDIF
		//Display Resurrection Shard - Hunted
		ELIF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_DISPLAY_RESURRECTION_SHARD_ENEMY_TEAM) AND IS_SCREEN_FADED_IN()
			IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_ANY_PLAYER_STILL_WAITING_TO_RESPAWN()
				
				IF IS_THIS_ROCKSTAR_MISSION_NEW_HALLOWEEN_ADVERSARY_2022(g_FMMC_STRUCT.iAdversaryModeType)
					PLAY_SOUND_FRONTEND(-1, "Hunted_Resurrected", "Halloween_Adversary_Sounds", FALSE)
				ENDIF
				
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "GR_RES", "GR_RES_ET")
				START_NET_TIMER(tdResurrectionShardTimer)
				SET_BIT(iLocalBoolCheck27, LBOOL27_DISPLAYING_RESURRECTION_SHARD)
				CLEAR_BIT(iLocalBoolCheck27, LBOOL27_DISPLAY_RESURRECTION_SHARD_ENEMY_TEAM)
				PRINTLN("[SPECTATE] PROCESS_PLAYER_LIVES - PROCESS_RESPAWN_SHARD - Display Shard Enemy Team Respawn")
				EXIT
			ENDIF
		ENDIF
		
		//Check if any team is being resurrected - if yes setup the resurrection shard
		IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_DISPLAY_RESURRECTION_SHARD_FRIENDLY_TEAM)
		AND NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_DISPLAY_RESURRECTION_SHARD_ENEMY_TEAM)
			INT i
			FOR i = 0 TO FMMC_MAX_TEAMS - 1
				IF SHOULD_THIS_TEAM_RESPAWN_NOW(i)
				AND IS_ANY_PLAYER_IN_THIS_TEAM_STILL_WAITING_TO_RESPAWN(i)
					IF MC_playerBD[iLocalPart].iTeam = i
						SET_BIT(iLocalBoolCheck27, LBOOL27_DISPLAY_RESURRECTION_SHARD_FRIENDLY_TEAM)
					ELSE
						SET_BIT(iLocalBoolCheck27, LBOOL27_DISPLAY_RESURRECTION_SHARD_ENEMY_TEAM)
					ENDIF
					EXIT
				ENDIF
			ENDFOR
		ENDIF
	
	//Cleanup the resurrection shard
	ELIF HAS_NET_TIMER_STARTED_AND_EXPIRED(tdResurrectionShardTimer, 5000)
		RESET_NET_TIMER(tdResurrectionShardTimer)
		CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_TEXT)
		CLEAR_BIT(iLocalBoolCheck27, LBOOL27_DISPLAYING_RESURRECTION_SHARD)
		PRINTLN("[SPECTATE] PROCESS_PLAYER_LIVES - PROCESS_RESPAWN_SHARD - Clear Team Respawn Shard")
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_SPECTATE()
	IF SHOULD_PROCESS_PLAYER_RESPAWN_FROM_SPECTATE_CHECKS()
		PROCESS_PLAYER_RESPAWN_FROM_SPECTATE()
	ENDIF
	
	IF SHOULD_PROCESS_WAIT_IN_SPECTATOR_CHECKS()
		PROCESS_WAIT_IN_SPECTATOR_FOR_RESPAWN()
	ENDIF
	
	IF CAN_ANY_TEAM_RESPAWN_FROM_SPECTATE()
		PROCESS_RESURRECTION_SHARD()
	ENDIF

ENDPROC

//Check if we are respawning players based on a specific condition instead of player lives
FUNC BOOL CAN_SPECTATING_PLAYER_RESPAWN_ON_CONDITION()
	INT iTeam = MC_playerBD[iLocalPart].iTeam
	IF IS_WAIT_IN_SPECTATE_ENABLED_FOR_THIS_TEAM(iTeam) AND NOT IS_LAST_PLAYER_ALIVE_SENT_TO_SPECTATE()
		PRINTLN("[SPECTATE] PROCESS_PLAYER_LIVES - CAN_SPECTATING_PLAYER_RESPAWN_NEXT_RULE = TRUE, due to IS_WAIT_IN_SPECTATE_ENABLED_FOR_THIS_TEAM")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[SPECTATE] PROCESS_PLAYER_LIVES - CAN_SPECTATING_PLAYER_RESPAWN_NEXT_RULE = FALSE")
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Respawning and Death																					
// ##### Description: Wrappers and Helper Functions that are used in various places of spawning/respawning.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
// [FIX_2020_CONTROLLER] - Add a shared print tag for PROCESS_PLAYER_LIVES and PROCESS_MANUAL_RESPAWN
PROC PROCESS_PLAYER_LIVES()
	VECTOR vspawnheading
	INT iplayerlives, iTeamLives
	
	INT iCleanUpLoop 
	BOOL bIsDriver = IS_DRIVER_FOR_MULTIPLE_TEAM_VEHICLE(MC_PlayerBD[iLocalPart].iTeam)
		
	// looking after player deaths and number of respawns (if limited)
	IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END	
	AND MC_serverBD_4.iCurrentHighestPriority[ MC_playerBD[iLocalPart].iteam ] < FMMC_MAX_RULES
	
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iLocalPart].iteam)
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]], ciBS_RULE8_RESET_PLAYER_LIVES)
			MC_playerBD[iLocalPart].iAdditionalPlayerLives = MC_playerBD[iLocalPart].iNumPlayerDeaths
			PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES][JS] - new rule giving player ",MC_playerBD[iLocalPart].iNumPlayerDeaths," lives back")
		ENDIF
		
		IF iHighKillStreak < GlobalplayerBD_FM[NATIVE_TO_INT(PlayerToUse)].iKillStreak
			iHighKillStreak = GlobalplayerBD_FM[NATIVE_TO_INT(PlayerToUse)].iKillStreak
		ENDIF
		
		PROCESS_PLAYER_SPECTATE()
		
		IF NOT bLocalPlayerOK
			IF ((NOT bIsAnySpectator)
			OR USING_HEIST_SPECTATE())
				IF NOT IS_BIT_SET(iLocalBoolCheck, PLAYER_DEATH_TOGGLE)
					CLEAR_PLAYER_IN_POSSESSION_OF_MISSION_CRITICAL_ENTITY()
					
					CLEANUP_OBJECT_INVENTORIES()
					
					CLEANUP_CACHED_VEHICLE_FLAGS()
					
					DESTROY_FLARE_ATTACHMENT_FOR_PLAYER()
					
					IF SHOULD_COUNT_PLAYER_DEATHS()
						MC_PlayerBD[iLocalPart].iNumPlayerDeaths++
					ENDIF
					
					IF IS_THIS_ROCKSTAR_MISSION_NEW_HALLOWEEN_ADVERSARY_2022(g_FMMC_STRUCT.iAdversaryModeType)
						SET_BIT(iLocalBoolCheck29, LBOOL29_HalloweenAdversary2022ResetProximitySounds)
						SET_BIT(iLocalBoolCheck14, LBOOL14_TRIGGER_HUNTED_DEATH_EFFECT)
						IF MC_playerBD[iLocalPart].iTeam = ciHALLOWEEN_ADVERSARY_HUNTER_TEAM
							VECTOR vTemp = GET_ENTITY_COORDS(LocalPlayerPed)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_PlayHalloweenAdversary2022HunterDeathSound, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, vTemp.X, vTemp.Y, vTemp.Z)
						ENDIF
					ENDIF
					
					IF MC_playerBD[iPartToUse].iSynchSceneID != -1 
						NETWORK_STOP_SYNCHRONISED_SCENE(MC_playerBD[iPartToUse].iSynchSceneID)
					ENDIF
					IF IS_PARTICIPANT_A_JUGGERNAUT(iLocalPart)
						PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] Setting PBBOOL3_WAS_JUGGERNAUT_ON_DEATH")
						SET_BIT(MC_PlayerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAS_JUGGERNAUT_ON_DEATH)
					ENDIF

					IF g_FMMC_STRUCT.fForcedRespawnTimeLose[MC_playerBD[iLocalPart].iteam] != 0
						IF IS_THERE_ENOUGH_TIME_LEFT_TO_DECREMENT_REMAINING_TIME_FOR_FORCED_RESPAWN(MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam], MC_playerBD[iLocalPart].iteam)
							PRINTLN("[MJL][PROCESS_PLAYER_LIVES] - Enough time left for respawn")
							BROADCAST_FMMC_DECREMENT_REMAINING_TIME(MC_playerBD[iLocalPart].iteam, ROUND(g_FMMC_STRUCT.fForcedRespawnTimeLose[MC_playerBD[iLocalPart].iteam] * 1000), DEFAULT, DEFAULT, DEFAULT, TRUE)
						ENDIF
					ENDIF
					
					INT iCurObjHacking = MC_playerBD[ iLocalPart ].iObjHacking
					IF iCurObjHacking > -1 AND iCurObjHacking < FMMC_MAX_NUM_OBJECTS
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCurObjHacking].mn != HEI_PROP_HEI_TIMETABLE
							//resetting 
							RUN_HACKING_CLEANUP()
						ENDIF
					ELSE	
						RUN_HACKING_CLEANUP(TRUE)
					ENDIF

					IF IS_SOUND_ID_VALID(iBulletSoundID)
						STOP_SOUND(iBulletSoundID)
						RELEASE_SOUND_ID(iBulletSoundID)
						iBulletSoundID = -1
					ENDIF

					FOR iCleanUpLoop = 0 TO MAX_TRACKIFY_TARGETS -1
						
						//If you investigated the object but didn't find it then you died while investigating...need to resync everyones data.
						IF IS_BIT_SET(iBSContainerBeingInvestigated,iCleanUpLoop)
							BROADCAST_CONTAINER_MINIGAME_DATA_CHANGE(iCleanUpLoop,iLocalPart,bIsCaseInContainer,FALSE,TRUE) 
						ENDIF
					
						CLEAR_BIT(iBSContainerBeingInvestigated,iCleanUpLoop)
					ENDFOR
					
					// If we're dead and we died just before/after the casco cutscene
					// then kill the special camera
					IF DOES_CAM_EXIST( cutscenecam )
						RENDER_SCRIPT_CAMS( FALSE, FALSE )
						DESTROY_CAM( cutscenecam )
					ENDIF
					
					IF IS_BIT_SET(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
						PRINTLN("[JS][spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - LBOOL_ON_LAST_LIFE")
						SET_BIT(iLocalBoolCheck,LBOOL_COUNTED_FINAL_DEATH)
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciEND_ROUND_ON_ZERO_LIVES)
							PRINTLN("[JS][spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - SETTING PBBOOL2_PLAYER_DEATH_CAUSED_FAIL")
							SET_BIT(MC_PlayerBD[iLocalPart].iClientBitSet2,PBBOOL2_PLAYER_DEATH_CAUSED_FAIL)
						ENDIF
					ENDIF
					iVehSeatPreferenceCheckBS = 0 //Check through seat preferences again on respawn
					
					IF g_FMMC_STRUCT.iTeamRespawnMinDist[MC_playerBD[iLocalPart].iteam] != 0
						FLOAT fminspawndist = TO_FLOAT(g_FMMC_STRUCT.iTeamRespawnMinDist[MC_playerBD[iLocalPart].iteam])
						SET_MIN_DIST_TO_SPAWN_FROM_ENEMY_PLAYER_NEAR_DEATH(fminspawndist)
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitsetFour[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]], ciBS_RULE4_IGNORE_RESPAWN_VISIBILITY_CHECKS)
						IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_IGNORE_RESPAWN_VISIBILITY_CHECKS)
							PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] Calling SET_DO_VISIBILITY_CHECKS_FOR_SPAWNING w FALSE, team ",MC_playerBD[iLocalPart].iteam," / rule ",MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam])
							SET_DO_VISIBILITY_CHECKS_FOR_SPAWNING(FALSE)
							SET_BIT(iLocalBoolCheck7, LBOOL7_IGNORE_RESPAWN_VISIBILITY_CHECKS)
						ENDIF
					ELSE
						IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_IGNORE_RESPAWN_VISIBILITY_CHECKS)
							PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] Calling SET_DO_VISIBILITY_CHECKS_FOR_SPAWNING w TRUE, team ",MC_playerBD[iLocalPart].iteam," / rule ",MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam])
							SET_DO_VISIBILITY_CHECKS_FOR_SPAWNING(TRUE)
							CLEAR_BIT(iLocalBoolCheck7, LBOOL7_IGNORE_RESPAWN_VISIBILITY_CHECKS)
						ENDIF
					ENDIF
					
					// Need to make a record of the position of the player's death so that
					vLastPlayersPos = GET_ENTITY_COORDS( LocalPlayerPed, FALSE )
					
					IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_CLEANUP_RESPAWN_SPAWN_AT_START_NO_VEH)
						IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
						AND ((IS_BIT_SET(iLocalBoolCheck,LBOOL_ON_LAST_LIFE))			
							OR NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_ON_LAST_LIFE))
							
							INT iTeam = MC_playerBD[iLocalPart].iteam
							INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
							
							MODEL_NAMES vehModel = g_FMMC_STRUCT.mnVehicleModel[iTeam]
							
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_USE_PER_RULE_VEHICLE_RESPAWN_SETTINGS)
							AND g_FMMC_STRUCT.iPerRuleRespawnVehicle[iRule][iTeam] != -1
								PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - iPerRuleRespawnVehicle overriding vehicle to spawn in.")
								vehModel = INT_TO_ENUM(MODEL_NAMES, g_FMMC_STRUCT.iPerRuleRespawnVehicle[iRule][iTeam])
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRespawnInVehicleBitset, iRule)
							AND vehModel != DUMMY_MODEL_FOR_SCRIPT
							OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_RESPAWN_IN_LAST_VEHICLE)	
							OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_Force_Respawn_Veh_As_Start_Veh) AND iTeam = 1)
								IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSwapToTeamOnDeath[iRule] != -1
									iTeam = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSwapToTeamOnDeath[iRule]
									PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] Swapped to team : ", iTeam)
								ENDIF
								
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_Force_Respawn_Veh_As_Start_Veh)
								OR iTeam = 0
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_RESPAWN_IN_LAST_VEHICLE )
										IF sLastVehicleInformation.mnModel != DUMMY_MODEL_FOR_SCRIPT	
											vehModel = sLastVehicleInformation.mnModel
											PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES][LVEH] Respawning player into last vehicle with model name ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(vehModel))
										ELSE
											PRINTLN("[LVEH] sLastVehicleInformation.mnModel = DUMMY MODEL")
										ENDIF
									ENDIF										
								ENDIF
								
								IF g_mnMyRaceModel = DUMMY_MODEL_FOR_SCRIPT
									IF g_FMMC_STRUCT.iVehicleColour[ iTeam ] > -1
										sLastVehicleInformation.iVehicleColour1 = GET_VEHICLE_COLOR_FROM_SELECTION(g_FMMC_STRUCT.iVehicleColour[ iTeam ])
										PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Using Team Respawn Colour of ", sLastVehicleInformation.iVehicleColour1 )
										PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] VEHICLE RESPAWN COLOUR - iColour:", g_FMMC_STRUCT.iVehicleColour[ iTeam ])
										PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] VEHICLE RESPAWN COLOUR - sLastVehicleInformation.iVehicleColour1:", sLastVehicleInformation.iVehicleColour1)											
										IF g_FMMC_STRUCT.iVehicleSecondaryColour[iTeam] > -1
											PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - VEHICLE RESPAWN SECONDARY COLOUR - iColour:", g_FMMC_STRUCT.iVehicleSecondaryColour[iTeam])											
											sLastVehicleInformation.iVehicleColour2 = GET_VEHICLE_COLOR_FROM_SELECTION( g_FMMC_STRUCT.iVehicleSecondaryColour[iTeam])
										ENDIF											
									ENDIF
								ELSE
									sLastVehicleInformation.iVehicleColour1 = -1
									PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] VEHICLE RESPAWN COLOUR - Not setting colour as this is a personal vehicle")
								ENDIF
								
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLEAR_UP_VEHICLE_ON_DEATH)
									PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - ciCLEAR_UP_VEHICLE_ON_DEATH is set")
									IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_VEHICLE_MARKED_FOR_CLEAN_UP)
										SET_BIT(iLocalBoolCheck16, LBOOL16_VEHICLE_MARKED_FOR_CLEAN_UP)
									ENDIF
									START_NET_TIMER(vehicleCleanUpTimer)
									netVehicleToCleanUp = netRespawnVehicle
								ENDIF
								
								SET_BIT(iLocalBoolCheck4, LBOOL4_WAIT_FOR_RESPAWN_IN_VEH)
								
								PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - setting LBOOL4_WAIT_FOR_RESPAWN_IN_VEH")
								
								SET_PLAYER_RESPAWN_IN_VEHICLE(TRUE, 
															vehModel, 
															FALSE,
															FALSE, 
															FALSE, 
															IS_BIT_SET(g_FMMC_Struct.iTeamVehicleBitSet, ciFMMC_TeamVehBS_MAKE_TEAM_RESPAWN_VEHICLE_STRONG), 
															sLastVehicleInformation.iVehicleColour1,
															IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_RESPAWN_VEHICLE_BULLETPROOF_TIRES),
															DEFAULT, DEFAULT, DEFAULT)
							ELSE
								SET_PLAYER_RESPAWN_IN_VEHICLE(FALSE)
							ENDIF
							
							INT iSpecificRespawnVehicle = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRespawnInMissionVehicle[iRule]
							
							IF (iSpecificRespawnVehicle != -1)
							AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iSpecificRespawnVehicle])									
								PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Player should respawn in mission vehicle ",iSpecificRespawnVehicle,", calling SET_PLAYER_TO_RESPAWN_IN_SPECIFIC_RESPAWN_VEHICLE, vsLastOccupiedVehicleSeat: ", ENUM_TO_INT(vsLastOccupiedVehicleSeat))
								
								INT iSeat = ENUM_TO_INT(vsLastOccupiedVehicleSeat)
								IF iPlayersInsideSeatPref > -3
								AND iVehSeatPrefOverrideVeh > -1
									iSeat = iPlayersInsideSeatPref
									PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Using Special Seat Pref Override (1) iSeat: ", iPlayersInsideSeatPref)
								ENDIF
								
								SET_PLAYER_TO_RESPAWN_IN_SPECIFIC_RESPAWN_VEHICLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iSpecificRespawnVehicle]), iSeat)
								
								VEHICLE_INDEX vehSpawn = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iSpecificRespawnVehicle])
								IF GET_ENTITY_MODEL(vehSpawn) = DELUXO
									IF IS_ENTITY_IN_AIR(vehSpawn)
									AND GET_ENTITY_HEIGHT_ABOVE_GROUND(vehSpawn) > 5
										SET_SPECIAL_FLIGHT_MODE_ALLOWED(vehSpawn, TRUE)
										SET_DISABLE_HOVER_MODE_FLIGHT(vehSpawn, FALSE)
										SET_SPECIAL_FLIGHT_MODE_TARGET_RATIO(vehSpawn, 1)
										PRINTLN("[RCC MISSION] PROCESS_PLAYER_LIVES - Deluxo in Air. Spawning with wheels up. (1)")
									ENDIF
								ENDIF
								
							ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_RESPAWN_IN_LAST_MISSION_VEHICLE_FROM_LIST)
							AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRespawnInLastMissionVehicleFromBS != 0
							AND (iLastOccupiedRespawnListVehicle != -1)
							AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iLastOccupiedRespawnListVehicle])
								PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Player should respawn in mission vehicle ",iLastOccupiedRespawnListVehicle," from list, calling SET_PLAYER_TO_RESPAWN_IN_SPECIFIC_RESPAWN_VEHICLE")
								
								INT iSeat = -3
								IF iPlayersInsideSeatPref > -3
								AND iVehSeatPrefOverrideVeh > -1
									iSeat = iPlayersInsideSeatPref
									PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Using Special Seat Pref Override (2) iSeat: ", iPlayersInsideSeatPref)
								ENDIF
								SET_PLAYER_TO_RESPAWN_IN_SPECIFIC_RESPAWN_VEHICLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iLastOccupiedRespawnListVehicle]), iSeat)
								
								VEHICLE_INDEX vehSpawn = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iLastOccupiedRespawnListVehicle])
								IF GET_ENTITY_MODEL(vehSpawn) = DELUXO
									IF IS_ENTITY_IN_AIR(vehSpawn)
									AND GET_ENTITY_HEIGHT_ABOVE_GROUND(vehSpawn) > 5
										SET_SPECIAL_FLIGHT_MODE_ALLOWED(vehSpawn, TRUE)
										SET_DISABLE_HOVER_MODE_FLIGHT(vehSpawn, FALSE)
										SET_SPECIAL_FLIGHT_MODE_TARGET_RATIO(vehSpawn, 1)
										PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Deluxo in Air. Spawning with wheels up. (2)")
									ENDIF
								ENDIF
							ELSE
								CLEAR_SPECIFIC_VEHICLE_TO_RESPAWN_IN()
							ENDIF
						ELSE
							SET_PLAYER_RESPAWN_IN_VEHICLE(FALSE)
							CLEAR_SPECIFIC_VEHICLE_TO_RESPAWN_IN()
						ENDIF
					ELSE
						PRINTLN("[LVEH] LBOOL21_SPECTATOR_RESPAWN_WAIT_IN_SPECTATE")
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_RUGBY_SOUNDS)
					AND MC_playerBD[iLocalPart].iObjCarryCount > 0
						// The player is about to drop any pickups they are carrying (as they are dead), so play the drop ball rugby sound:
						PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Call BROADCAST_RUGBY_PLAY_SOUND with ciRUGBY_SOUND_DROPPED_BALL as player has died while carrying the ball")
						BROADCAST_RUGBY_PLAY_SOUND(ciRUGBY_SOUND_DROPPED_BALL)
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
					
						PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] Clearing Respawn bitsets. and setting that we are finished: ciBS_MAN_RESPAWN_FINISHED (PROCESS PLAYER LIVES, player died)")
						
						SET_BIT(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_FORCED)
					
						CLEAR_BIT(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_STARTED)
						CLEAR_BIT(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_ENTERED_VEHICLE)
						CLEAR_BIT(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_PASSENGERS_LEFT_VEHICLE)
						CLEAR_BIT(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_FINISHED)
						
						RESET_NET_TIMER(tdManualRespawnTeamVehicle_FailTimedOut)
					ENDIF
					
					IF DOES_BLIP_EXIST(blipSafeProp)
						REMOVE_BLIP(blipSafeProp)
					ENDIF
					
					PROCESS_ADDING_WATER_NODES_FOR_AMBIENT_SPAWNING()
										
					IF IS_BIT_SET(sAltitudeBarLocal.iBS, ciALTITUDE_BAR_PREV_RULE_USED_ALTITUDE_SYSTEMS)
						CLEAN_UP_ALTITUDE_SYSTEMS()
					ENDIF
										
					IF NOT IS_BIT_SET(iLocalBoolCheck3, LBOOL3_SET_RESPAWN_WEAPON)
						WEAPON_TYPE wtWeapon
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciRUGBY_WEAPON_RESPAWN)
						AND NOT (wtRugbyRespawnWeapon = WEAPONTYPE_INVALID)
							wtWeapon = wtRugbyRespawnWeapon
						ELSE
							GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtWeapon)
						ENDIF
						SET_PLAYER_CURRENT_HELD_WEAPON(wtWeapon)

						PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Player died, set weapon from respawn to ", GET_WEAPON_NAME(INT_TO_ENUM(WEAPON_TYPE, wtWeapon)))
					ENDIF
					
					#IF IS_DEBUG_BUILD
						DEBUG_SET_LOCAL_PLAYER_LAST_DEATH_COORD_AND_HEADING(GET_ENTITY_COORDS(localPlayerPed), GET_ENTITY_HEADING(localPlayerPed))
					#ENDIF
					
					SET_BIT(iLocalBoolCheck3, LBOOL3_SET_RESPAWN_WEAPON)
					SET_BIT(iLocalBoolCheck, LBOOL_SETTING_AFTER_DEATH_WANTED)
					
					PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Player is dead!")
					SET_BIT(iLocalBoolCheck, PLAYER_DEATH_TOGGLE)
				ENDIF
				
				//-- Was a spawn heading set in the creator?
				IF IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].sRuleBounds[GET_LOCAL_PLAYER_CURRENT_RULE()][0].vHeadingVec)
				OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iTeamBitset2, ciBS2_CUSTOM_RESPAWN_FACE_NEAREST_OBJECTIVE)
					IF NOT USING_SPAWN_AREA_ZONE_RESPAWN_HEADING()
						vspawnheading = GET_COORDS_OF_CLOSEST_OBJECTIVE()
						PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES][SET_PLAYER_WILL_SPAWN_FACING_COORDS] setting dead spawn heading: ", vspawnheading)
						SET_PLAYER_WILL_SPAWN_FACING_COORDS(vspawnheading, TRUE, FALSE)
					ELSE
						PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES][SET_PLAYER_WILL_SPAWN_FACING_COORDS] Letting zone ", iCurrentSpawnAreaZone, " handle the player's respawn heading")
					ENDIF
				ENDIF
				
				IF IS_MISSION_TEAM_LIVES()
					iTeamlives = GET_FMMC_MISSION_LIVES_FOR_TEAM(MC_playerBD[iLocalPart].iteam)
					IF iTeamlives != ciFMMC_UNLIMITED_LIVES
						IF IS_BIT_SET(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
						AND NOT CAN_SPECTATING_PLAYER_RESPAWN_ON_CONDITION()
							IF GET_TEAM_DEATHS(MC_playerBD[iLocalPart].iTeam) > iTeamlives 
								IF iSpectatorTarget = -1
									IF NOT (IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
									AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
									AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_LAST_LIFE))
									#IF IS_DEBUG_BUILD
									IF NOT IS_BIT_SET(MC_serverBD.iDebugBitset, SBDEBUG_DONTFAILWHENOUTOFLIVES)
									#ENDIF
										MC_playerBD[iLocalPart].iReasonForPlayerFail = PLAYER_FAIL_OUT_OF_LIVES
										CPRINTLN(DEBUG_MISSION,"[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] OUT OF TEAM LIVES - player fail") 
										SET_LOCAL_PLAYER_FAILED(4)
										SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
										SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_LAST_LIFE)
										
										IF IS_THIS_A_HEIST_MISSION()
											SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[iLocalPart].iteam,MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam],OBJ_END_REASON_HEIST_TEAM_GONE_T0 + MC_playerBD[iLocalPart].iteam,true)
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(OBJ_END_REASON_HEIST_TEAM_GONE_T0 + MC_playerBD[iLocalPart].iteam,MC_playerBD[iLocalPart].iteam,MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam],FALSE,true)
										ELSE
											SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[iLocalPart].iteam,MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam],OBJ_END_REASON_OUT_OF_LIVES)
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(OBJ_END_REASON_OUT_OF_LIVES,MC_playerBD[iLocalPart].iteam,MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam],FALSE)
										ENDIF
									#IF IS_DEBUG_BUILD
									ENDIF
									#ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ELIF IS_MISSION_COOP_TEAM_LIVES()
					iTeamlives = GET_FMMC_MISSION_LIVES_FOR_COOP_TEAMS()
					IF iTeamlives != ciFMMC_UNLIMITED_LIVES
						IF IS_BIT_SET(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
							IF GET_COOP_TEAMS_TOTAL_DEATHS(MC_playerBD[iLocalPart].iTeam) > iTeamlives
								IF iSpectatorTarget = -1
									IF NOT (IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
									AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_PLAYER_OUT_OF_LIVES)
									AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_LAST_LIFE))
									#IF IS_DEBUG_BUILD
									IF NOT IS_BIT_SET(MC_serverBD.iDebugBitset, SBDEBUG_DONTFAILWHENOUTOFLIVES)
									#ENDIF
										MC_playerBD[iLocalPart].iReasonForPlayerFail = PLAYER_FAIL_OUT_OF_LIVES
										CPRINTLN(DEBUG_MISSION,"[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] OUT OF COOP TEAM LIVES - player fail")
										SET_LOCAL_PLAYER_FAILED(5)
										SET_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_PLAYER_OUT_OF_LIVES)
										SET_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_LAST_LIFE)
										
										IF IS_THIS_A_HEIST_MISSION()
											SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[iLocalPart].iteam,MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam],OBJ_END_REASON_HEIST_TEAM_GONE_T0 + MC_playerBD[iLocalPart].iteam,true)
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(OBJ_END_REASON_HEIST_TEAM_GONE_T0 + MC_playerBD[iLocalPart].iteam,MC_playerBD[iLocalPart].iteam,MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam],FALSE,true)
										ELSE
											SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[iLocalPart].iteam,MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam],OBJ_END_REASON_OUT_OF_LIVES,true)
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(OBJ_END_REASON_OUT_OF_LIVES,MC_playerBD[iLocalPart].iteam,MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam],FALSE,true)
										ENDIF
									#IF IS_DEBUG_BUILD
									ENDIF
									#ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					iplayerlives = GET_FMMC_MISSION_LIVES_FOR_LOCAL_PLAYER()
					IF iplayerlives != ciFMMC_UNLIMITED_LIVES 
						IF IS_BIT_SET(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
							IF MC_PlayerBD[iLocalPart].iNumPlayerDeaths >= iplayerlives
								IF iSpectatorTarget = -1
									IF NOT (IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
									AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_PLAYER_OUT_OF_LIVES)
									AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_LAST_LIFE))
									#IF IS_DEBUG_BUILD
									IF NOT IS_BIT_SET(MC_serverBD.iDebugBitset, SBDEBUG_DONTFAILWHENOUTOFLIVES)
									#ENDIF
										MC_playerBD[iLocalPart].iReasonForPlayerFail = PLAYER_FAIL_OUT_OF_LIVES
										CPRINTLN(DEBUG_MISSION,"[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] OUT OF PLAYER LIVES - player fail")
										SET_LOCAL_PLAYER_FAILED(6)
										SET_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_PLAYER_OUT_OF_LIVES)
										SET_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_LAST_LIFE)
										
										IF IS_THIS_A_HEIST_MISSION()
											SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[iLocalPart].iteam,MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam],OBJ_END_REASON_HEIST_TEAM_GONE_T0 + MC_playerBD[iLocalPart].iteam,true)
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(OBJ_END_REASON_HEIST_TEAM_GONE_T0 + MC_playerBD[iLocalPart].iteam,MC_playerBD[iLocalPart].iteam,MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam],FALSE,true)
										ELSE
											SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[iLocalPart].iteam,MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam],OBJ_END_REASON_OUT_OF_LIVES)
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(OBJ_END_REASON_OUT_OF_LIVES,MC_playerBD[iLocalPart].iteam,MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam],FALSE)
										ENDIF
									#IF IS_DEBUG_BUILD
									ENDIF
									#ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset4, ciBS4_TEAM_CRITICAL_LIVES_WILL_FAIL_TEAM)
				AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_LAST_LIFE)
				AND IS_BIT_SET(iLocalBoolCheck, PLAYER_DEATH_TOGGLE)
				AND NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet2, PBBOOL2_PLAYER_DEATH_CAUSED_FAIL)
					PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Failing player. Died with ciBS4_TEAM_CRITICAL_LIVES_WILL_FAIL_TEAM")
					SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[iLocalPart].iteam,MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam],OBJ_END_REASON_OUT_OF_LIVES)
					BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(OBJ_END_REASON_OUT_OF_LIVES,MC_playerBD[iLocalPart].iteam,MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam],FALSE)
					MC_playerBD[iLocalPart].iReasonForPlayerFail = PLAYER_FAIL_OUT_OF_LIVES
					SET_BIT(MC_PlayerBD[iLocalPart].iClientBitSet2, PBBOOL2_PLAYER_DEATH_CAUSED_FAIL)
					SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
				ENDIF
				
				IF NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_REMOVED_TAKE_ON_NO_LIVES_DEATH)
				AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_UseIndividualBagValues)
				AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
					BROADCAST_FMMC_REMOVE_GRABBED_CASH(MC_playerBD[iLocalPart].sLootBag.iCashGrabbed)
					SET_BIT(iLocalBoolCheck34, LBOOL34_REMOVED_TAKE_ON_NO_LIVES_DEATH)
					PRINTLN("[RCC MISSION][PROCESS_PLAYER_LIVES] Removing cash grabbed from total take")
				ENDIF
				
				IF IS_WAIT_IN_SPECTATE_ENABLED_FOR_THIS_TEAM(MC_playerBD[iLocalPart].iTeam)
				AND NOT SHOULD_THIS_TEAM_RESPAWN_NOW(MC_playerBD[iLocalPart].iTeam)
					SET_GAME_STATE_ON_DEATH(AFTERLIFE_MANUAL_RESPAWN)
					manualRespawnState = eManualRespawnState_WAITING_AFTER_RESPAWN
					PRINTLN("[SPECTATE] - Player is dead - SET_GAME_STATE_ON_DEATH(AFTERLIFE_MANUAL_RESPAWN)")
				ENDIF
			ENDIF
		ELSE // Else this player is okay
			CACHE_PLAYER_ALIVE_AND_IN_VEHICLE()
			
			IF IS_PART_WAITING_IN_SPECTATE(iLocalPart)
				PROCESS_LOCAL_PLAYER_JUST_RESPAWNED_FROM_SPECTATE()
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck30, LBOOL30_BLOCK_WAIT_IN_SPECTATE_UNTIL_PLAYER_RESPAWNS)
				CLEAR_BIT(iLocalBoolCheck30, LBOOL30_BLOCK_WAIT_IN_SPECTATE_UNTIL_PLAYER_RESPAWNS)
				PRINTLN("[SPECTATE] - Clearing LBOOL30_BLOCK_WAIT_IN_SPECTATE_UNTIL_PLAYER_RESPAWNS flag!")
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck, PLAYER_DEATH_TOGGLE)
				CLEAR_BIT(iLocalBoolCheck27, LBOOL27_PLAYER_WEAPON_DAMAGE_MODIFIER_APPLIED)
				PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Clear weapon damage modifier flag!")
			ENDIF
			
			IF iSpectatorTarget = -1
			OR USING_HEIST_SPECTATE()			
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciTEAM_SWITCH_SHARDS)
					IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_SHOULD_SHOW_TEAM_SWITCH_SHARD)
					AND NOT IS_SCREEN_FADED_OUT()
						PRINTLN("[RCC MISSION][spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - [KH] LBOOL16_SHOULD_SHOW_TEAM_SWITCH_SHARD is set")
						PLAY_SOUND_FRONTEND(-1, "Losing_Team_Shard", "DLC_Exec_TP_SoundSet", FALSE)
						SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GENERIC_TEXT, "", "TS_LOSE", "TGIG_LOSER")
						CLEAR_BIT(iLocalBoolCheck16, LBOOL16_SHOULD_SHOW_TEAM_SWITCH_SHARD)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(iLocalBoolCheck, PLAYER_DEATH_TOGGLE)
					REINIT_NET_TIMER(tdRespawnTimer)
					
					//Clear up all the crate opening minigame stuff on player death
					iThisContainerNumBeingInvestigated = - 1
					bIsCaseInContainer = FALSE
					iInvestigateContainerState = ciHICInit
					
					IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAS_JUGGERNAUT_ON_DEATH)
						PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] Clearing PBBOOL3_WAS_JUGGERNAUT_ON_DEATH")
						CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAS_JUGGERNAUT_ON_DEATH)
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset3, ciBS3_TEAM_DISABLE_HELMET_ARMOUR_FROM_OUTFIT)
						PRINTLN("spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - ciBS3_TEAM_DISABLE_HELMET_ARMOUR_FROM_OUTFIT is set. Calling: PCF_DisableHelmetArmor")
						SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableHelmetArmor, TRUE)
					ENDIF
					
					IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_BLIP_FORCED_ON_BY_ZONE)
						PRINTLN("[JS][spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - clearing LBOOL23_BLIP_FORCED_ON_BY_ZONE")
						CLEAR_BIT(iLocalBoolCheck23, LBOOL23_BLIP_FORCED_ON_BY_ZONE)
						INT i
						FOR i = 0 TO FMMC_MAX_TEAMS - 1
							IF i != MC_playerBD[iLocalPart].iTeam
								HIDE_MY_PLAYER_BLIP_FROM_TEAM(TRUE, i)
							ENDIF
						ENDFOR
					ENDIF
					
					IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_DIED_BEFORE_SUDDEN_DEATH)
						CLEAR_BIT(iLocalBoolCheck23, LBOOL23_DIED_BEFORE_SUDDEN_DEATH)
						
						PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] CLEAR_BIT(iLocalBoolCheck23, LBOOL23_DIED_BEFORE_SUDDEN_DEATH)")
					ENDIF
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset2, ciBS2_JUGGERNAUTS) 
						SET_PED_USING_ACTION_MODE(LocalPlayerPed, TRUE)
					ENDIF
					
					RUN_LOCAL_PLAYER_PROOF_SETTING()
					
					CLEAR_BIT(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS1_CHOSEN_IN_FRONT)
					CLEAR_BIT(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS1_CHOSEN_NOT_IN_FRONT)
					CLEAR_BIT(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS2_CHOSEN_IN_FRONT)
					CLEAR_BIT(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS2_CHOSEN_NOT_IN_FRONT)
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_BLOCK_PLAYER_DROPPED_AMMO)
						SET_PED_DROPS_WEAPONS_WHEN_DEAD(LocalPlayerPed, FALSE)
					ENDIF
					
					PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Player is alive again")
					
					CLEAR_BIT(iLocalBoolCheck,PLAYER_DEATH_TOGGLE)
					
					SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_FULL_ZONES_AND_BOUNDS_LOOP_REQUIRED)
					PRINTLN("[spawning] PROCESS_PLAYER_LIVES - Requesting zone loop")
					
					BROADCAST_FMMC_SPAWN_PROTECTION_ALPHA(FALSE, iLocalPart, GET_FRAME_COUNT())
					
					#IF IS_DEBUG_BUILD
						DEBUG_SET_LOCAL_PLAYER_LAST_SPAWNED_COORD_AND_HEADING(GET_ENTITY_COORDS(localPlayerPed), GET_ENTITY_HEADING(localPlayerPed))
					#ENDIF
					
					SETUP_REBREATHER_FOR_RESPAWN()
					
					IF GIVE_AND_CACHE_LOCAL_PLAYER_HEIST_BAG(MC_playerBD[iLocalPart].iTeam)
						PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Giving back bag on respawn")
					ENDIF
					
					SET_UP_LOCAL_PLAYER_PROOF_SETTINGS()
					
					CLEAR_BIT(iLocalBoolCheck25, LBOOL25_CLEANUP_RESPAWN_SPAWN_AT_START_NO_VEH)
					
					SET_BIT(iLocalBoolCheck25, LBOOL25_DRIVER_DIED_OR_RESPAWNED_INTO_NEW_VEH)
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
					
						PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] Clearing Respawn bitsets. and setting that we are finished: ciBS_MAN_RESPAWN_FINISHED (PROCESS PLAYER LIVES, player IS ALIVE again)")
					
						CLEAR_BIT(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_STARTED)
						CLEAR_BIT(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_ENTERED_VEHICLE)
						CLEAR_BIT(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_PASSENGERS_LEFT_VEHICLE)
						CLEAR_BIT(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_FINISHED)
						
						RESET_NET_TIMER(tdManualRespawnTeamVehicle_FailTimedOut)
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet2, ciBS2_KILL_PLAYER_IN_WATER)
						PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] Setting player dies instantly in water.")
						SET_PED_DIES_INSTANTLY_IN_WATER(LocalPlayerPed, TRUE)
					ENDIF
					
					//Remove grabbed cash and capacity on respawn
					REMOVE_BAG_CONTENTS_ON_RESPAWN()
					
					IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_SET_RESPAWN_WEAPON)
						GIVE_FORCED_WEAPON_AND_AMMO(FALSE, iDlcWeaponBitSet, wtWeaponToRemove, DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY(MC_playerBD[iPartToUse].iTeam))
						CLEAR_BIT(iLocalBoolCheck3, LBOOL3_SET_RESPAWN_WEAPON)
					ENDIF
					
					PROCESS_PLAYER_RESPAWN_SETTINGS(MC_playerBD[iLocalPart].iTeam)
					
					IF IS_RULE_INDEX_VALID(GET_LOCAL_PLAYER_CURRENT_RULE())
						START_PLAYER_MODEL_SWAP(GET_LOCAL_PLAYER_TEAM(), g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].sRulePlayerModelSwap[GET_LOCAL_PLAYER_CURRENT_RULE()].iPlayerModelToSwapToOnRespawn)
					ENDIF
						
				ELIF IS_BIT_SET(iLocalBoolCheck4, LBOOL4_WAIT_FOR_RESPAWN_IN_VEH)
					PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - LBOOL4_WAIT_FOR_RESPAWN_IN_VEH")
										
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					AND (NOT IS_PLAYER_RESPAWNING(LocalPlayer))
					AND IS_PLAYER_CONTROL_ON(LocalPlayer)
						
						VEHICLE_INDEX vehSpawn = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
							
						BOOL bOverridenVehicleModel
						
						IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES				
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]], ciBS_RULE8_VEHICLE_SWAP_ENABLED)
							AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]], ciBS_RULE8_VEHICLE_SWAP_RESPAWN_IN)
								PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Overriden Vehicle respawn model")
								bOverridenVehicleModel = TRUE
							ENDIF
						ENDIF
						
						IF (GET_ENTITY_MODEL(vehSpawn) != g_FMMC_STRUCT.mnVehicleModel[MC_playerBD[iLocalPart].iteam])
						AND g_mnMyRaceModel = GET_ENTITY_MODEL(vehSpawn)
							PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Default vehicle not chosen at vehicle selection.")
							bOverridenVehicleModel = TRUE
						ENDIF
						
						IF ((GET_ENTITY_MODEL(vehSpawn) = g_FMMC_STRUCT.mnVehicleModel[MC_playerBD[iLocalPart].iteam]) OR bOverridenVehicleModel)
						AND NETWORK_HAS_CONTROL_OF_ENTITY(vehSpawn)
									
						ELSE
							PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - NOT ((GET_ENTITY_MODEL(vehSpawn) = g_FMMC_STRUCT.mnVehicleModel[MC_playerBD[iLocalPart].iteam]) OR bOverridenVehicleModel) AND NETWORK_HAS_CONTROL_OF_ENTITY(vehSpawn)")
							IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(vehSpawn)
								PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Don't have control of my vehicle")
							ENDIF
							IF NOT (GET_ENTITY_MODEL(vehSpawn) = g_FMMC_STRUCT.mnVehicleModel[MC_playerBD[iLocalPart].iteam])
								PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - Vehicles don't match - vehSpawn: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehSpawn)), " g_FMMC_STRUCT.mnVehicleModel: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT.mnVehicleModel[MC_playerBD[iLocalPart].iteam]))
							ENDIF
						ENDIF
																		
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLEAR_UP_VEHICLE_ON_DEATH)
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
							OR bIsDriver
								PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - ciCLEAR_UP_VEHICLE_ON_DEATH setting netRespawnVehicle")
								SET_ENTITY_AS_MISSION_ENTITY(vehSpawn, FALSE, TRUE)
								SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(VEH_TO_NET(vehSpawn), TRUE)
							ENDIF
							
							netRespawnVehicle = VEH_TO_NET(vehSpawn)
						ENDIF
						
						PROCESS_PLAYER_RESPAWN_VEHICLE_SETTINGS(vehSpawn, FALSE, MC_playerBD[iLocalPart].iTeam, ((GET_ENTITY_MODEL(vehSpawn) = g_FMMC_STRUCT.mnVehicleModel[MC_playerBD[iLocalPart].iteam]) OR bOverridenVehicleModel) AND NETWORK_HAS_CONTROL_OF_ENTITY(vehSpawn))
						
						CLEAR_BIT(iLocalBoolCheck4, LBOOL4_WAIT_FOR_RESPAWN_IN_VEH)
					ENDIF
				ENDIF
				
				IF GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iRespawnState = RESPAWN_STATE_PLAYING
					IF iCurrentInterior != LocalPlayerCurrentInterior
						IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
							IF LocalPlayerCurrentInterior != NULL
								SET_PLAYER_RESPAWN_TO_CONSIDER_INTERIORS_ON_THIS_MISSION(TRUE)
								CPRINTLN(DEBUG_MISSION,"[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] setting dead spawn to consider interiors ") 
							ELSE
								SET_PLAYER_RESPAWN_TO_CONSIDER_INTERIORS_ON_THIS_MISSION(FALSE)
								CPRINTLN(DEBUG_MISSION,"[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] setting dead spawn to NOT consider interiors")
				      		ENDIF
						ENDIF
						iCurrentInterior = LocalPlayerCurrentInterior
					ENDIF
					
					IF IS_BIT_SET(iLocalBoolCheck, LBOOL_SETTING_AFTER_DEATH_WANTED)
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciWANTED_CLEARED_ON_DEATH)
							IF iWantedBeforeDeath > 0
								IF GET_PLAYER_WANTED_LEVEL(LocalPlayer) != iWantedBeforeDeath
									SET_PLAYER_WANTED_LEVEL(LocalPlayer, iWantedBeforeDeath)
									SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
									PRINTLN("[spawning][WANTED][PROCESS_PLAYER_LIVES] - Resetting after death wanted to: ", iWantedBeforeDeath)
								ENDIF
								IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iRuleBitsetThirteen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]], ciBS_RULE13_FORCE_SEARCH_NO_AMBIENT_COPS)
										IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_FORCED_SEARCH_WANTED_AND_SEEN)
											FORCE_START_HIDDEN_EVASION(LocalPlayer)
											PRINTLN("[spawning][WANTED][PROCESS_PLAYER_LIVES] -  player is on a ciBS_RULE13_FORCE_SEARCH_NO_AMBIENT_COPS rule and was previously not spotted by cops.")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						CLEAR_BIT(iLocalBoolCheck, LBOOL_SETTING_AFTER_DEATH_WANTED)
					ENDIF
				ENDIF
				
			ENDIF
			
			IF IS_MISSION_TEAM_LIVES()
				iTeamlives = GET_FMMC_MISSION_LIVES_FOR_TEAM(MC_playerBD[iLocalPart].iteam)
				IF iTeamlives != ciFMMC_UNLIMITED_LIVES
					#IF IS_DEBUG_BUILD
					IF NOT IS_BIT_SET(MC_serverBD.iDebugBitset, SBDEBUG_DONTFAILWHENOUTOFLIVES)
					#ENDIF
					IF iSpectatorTarget = -1
						IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
							IF (iTeamlives - GET_TEAM_DEATHS(MC_playerBD[iLocalPart].iTeam)) <= 0
								SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP)
								SET_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_LAST_LIFE)
								SET_BIT(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
							ENDIF
						ENDIF
					ENDIF
					#IF IS_DEBUG_BUILD
					ELSE
						CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_LAST_LIFE)
						CLEAR_BIT(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
						SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN)
					ENDIF
					#ENDIF
				ENDIF
			ELIF IS_MISSION_COOP_TEAM_LIVES()
				iTeamlives = GET_FMMC_MISSION_LIVES_FOR_COOP_TEAMS()
				IF iTeamlives != ciFMMC_UNLIMITED_LIVES
					#IF IS_DEBUG_BUILD
					IF NOT IS_BIT_SET(MC_serverBD.iDebugBitset, SBDEBUG_DONTFAILWHENOUTOFLIVES)
					#ENDIF
					IF iSpectatorTarget = -1
						IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
							IF (iTeamlives - GET_COOP_TEAMS_TOTAL_DEATHS(MC_playerBD[iLocalPart].iTeam)) <= 0
								SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP)
								SET_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_LAST_LIFE)
								SET_BIT(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
							ENDIF
						ENDIF
					ENDIF
					#IF IS_DEBUG_BUILD
					ELSE
						CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_LAST_LIFE)
						CLEAR_BIT(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
						SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN)
					ENDIF
					#ENDIF
				ENDIF
			ELSE
				iplayerlives = GET_FMMC_MISSION_LIVES_FOR_LOCAL_PLAYER()
				IF iplayerlives != ciFMMC_UNLIMITED_LIVES
					#IF IS_DEBUG_BUILD
					IF NOT IS_BIT_SET(MC_serverBD.iDebugBitset, SBDEBUG_DONTFAILWHENOUTOFLIVES)
					#ENDIF
						IF MC_PlayerBD[iLocalPart].iNumPlayerDeaths <= iplayerlives
							IF iSpectatorTarget = -1
								IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
									IF iplayerlives - MC_PlayerBD[iLocalPart].iNumPlayerDeaths <= 1
										SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP)
										SET_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_LAST_LIFE)
										PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] - LBOOL_ON_LAST_LIFE")
										SET_BIT(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
									ENDIF
								ENDIF
							ENDIF
						ELSE 
							IF IS_SCREEN_FADED_IN()
								IF iSpectatorTarget = -1								
									PRINTLN("[spawning][RCC MISSION][PROCESS_PLAYER_LIVES] OUT OF PLAYER LIVES - mission fail")
									IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_PLAYER_OUT_OF_LIVES)
										MC_playerBD[iLocalPart].iReasonForPlayerFail = PLAYER_FAIL_OUT_OF_LIVES
										SET_LOCAL_PLAYER_FAILED(8)
										SET_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_PLAYER_OUT_OF_LIVES)
										SET_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_LAST_LIFE)
										SET_BIT(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
										SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[iLocalPart].iteam,MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam],OBJ_END_REASON_OUT_OF_LIVES,true)
										BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(OBJ_END_REASON_OUT_OF_LIVES,MC_playerBD[iLocalPart].iteam,MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam],FALSE,true)											
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD
					ELSE
						CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_LAST_LIFE)
						CLEAR_BIT(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
						SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN)
					ENDIF
					#ENDIF
				ENDIF
			
			ENDIF
			
		ENDIF // End of the IS_PLAYER_OKAY check
	ENDIF
	
	IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		g_bMissionInstantRespawn = FALSE
	ENDIF
		
	//Lives Pickup				
	IF IS_MISSION_COOP_TEAM_LIVES()
		IF (GET_FMMC_MISSION_LIVES_FOR_COOP_TEAMS() - GET_COOP_TEAMS_TOTAL_DEATHS(MC_playerBD[iLocalPart].iTeam)) > 1
			IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_LAST_LIFE)
			OR IS_BIT_SET(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_LAST_LIFE)
				CLEAR_BIT(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
				SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN)
			ENDIF
		ENDIF
	ELIF IS_MISSION_TEAM_LIVES()
		IF (GET_FMMC_MISSION_LIVES_FOR_TEAM(MC_playerBD[iLocalPart].iteam) -  GET_TEAM_DEATHS(MC_playerBD[iLocalPart].iTeam)) > 1
			IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_LAST_LIFE)
			OR IS_BIT_SET(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_LAST_LIFE)
				CLEAR_BIT(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
				SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN)
			ENDIF
		ENDIF
	ELIF IS_MISSION_PLAYER_LIVES()
		IF (GET_FMMC_MISSION_LIVES_FOR_LOCAL_PLAYER() - MC_PlayerBD[iLocalPart].iNumPlayerDeaths) > 1
			IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_LAST_LIFE)
			OR IS_BIT_SET(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_LAST_LIFE)
				CLEAR_BIT(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
				SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN)
			ENDIF
		ENDIF
	ENDIF
		
	//To avoid dying in test when out of bounds: (bug 1793986)
	IF IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
		IF NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_OUTOFBOUNDS_TESTMODE)
			IF NOT IS_VECTOR_IN_WORLD_BOUNDS(GET_PLAYER_COORDS(LocalPlayer))
				SET_CONTROL_SHAKE(PLAYER_CONTROL,0,0) //Needs to be called every frame
				NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_PREVENT_EVERYBODY_BACKOFF|NSPC_LEAVE_CAMERA_CONTROL_ON|NSPC_REENABLE_CONTROL_ON_DEATH)
				VECTOR vSpawnLoc
				
				IF IS_VECTOR_IN_WORLD_BOUNDS(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].vStartPos)
					vSpawnLoc = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].vStartPos
				ELSE
					vSpawnLoc = <<0,0,80>>
				ENDIF
				
				IF NEW_LOAD_SCENE_START_SPHERE(vSpawnLoc,20) //true once it's started loading
					SET_BIT(iLocalBoolCheck3,LBOOL3_OUTOFBOUNDS_TESTMODE)
					DO_SCREEN_FADE_OUT(1000)
					PRINTLN("[RCC MISSION] Out of bounds in test mode! - new spawn location started loading")
				ELSE
					PRINTLN("[RCC MISSION] Out of bounds in test mode! - new spawn location not started loading")
				ENDIF
			ENDIF
		ELSE
			IF IS_SCREEN_FADING_OUT()
			OR IS_SCREEN_FADED_OUT()
				SET_CONTROL_SHAKE(PLAYER_CONTROL,0,0)
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_IN()
				
				IF NOT HAS_NET_TIMER_STARTED(tdOutOfBoundsWarpTimer)
					IF IS_VECTOR_IN_WORLD_BOUNDS(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].vStartPos)
						SET_ENTITY_COORDS(LocalPlayerPed,g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].vStartPos,FALSE)
					ELSE		
						PRINTLN("[RCC MISSION] Trying to teleport player back in bounds during test mode, but start position is invalid!")
						SET_ENTITY_COORDS(LocalPlayerPed,<<0,0,80>>)
					ENDIF
					
					IF IS_VECTOR_IN_WORLD_BOUNDS(GET_PLAYER_COORDS(LocalPlayer))
					AND IS_NEW_LOAD_SCENE_LOADED()
						REINIT_NET_TIMER(tdOutOfBoundsWarpTimer)
						PRINTLN("[RCC MISSION] Out of bounds in test mode! - new spawn location loaded in")
					ENDIF
				ENDIF
				
				IF HAS_NET_TIMER_STARTED(tdOutOfBoundsWarpTimer)
				AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdOutOfBoundsWarpTimer) >= 1000
					RESET_NET_TIMER(tdOutOfBoundsWarpTimer)
					DO_SCREEN_FADE_IN(1000)
				ENDIF
				
			ENDIF
			
			IF IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_OUT()
				NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
				CLEAR_BIT(iLocalBoolCheck3,LBOOL3_OUTOFBOUNDS_TESTMODE)
				PRINTLN("[RCC MISSION] Out of bounds in test mode! - ready for player to resume")
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Main Respawn Process																		
// ##### Description: The main process loop for anything player spawn related.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// process add/remove respawn points.
// process manual
// process lives
PROC PROCESS_LOCAL_PLAYER_SPAWNING()
	
	PROCESS_SET_UP_CUSTOM_RESPAWN_POINTS()
	
	PROCESS_PLAYER_LIVES()
	
	PROCESS_ACTUAL_DEATH_RESPAWN_RADIO()
	
	PROCESS_MANUAL_RESPAWN(timerManualRespawn)
	
	PROCESS_MANUAL_RESPAWN_RADIO()
		
ENDPROC
