// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - HUD Utility -------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Wrappers and helpers for blipping, shards, etc. 
// ##### Functionality in this header should set data that is then processed in _HUDProcessing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020                                                                                       
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Assets_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Hint Cam ------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description:  Functions relating to Forcing hint cams
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC CLEAR_FORCED_HINT_CAM_VARS()
	CLEAR_BIT(iLocalBoolCheck30, LBOOL30_FORCED_HINT_CAM_TRIGGERED)
	CLEAR_BIT(iLocalBoolCheck30, LBOOL30_FORCED_HINT_CAM_ACTIVE)
	iForcedHintCamTargetType = -1
	iForcedHintCamTargetID = -1
	iForcedHintCamTime = ciFMMC_DEFAULT_FORCED_HINT_CAM_LENGTH
	RESET_NET_TIMER(tdForcedHintCamReset)
	FORCE_CHASE_HINT_CAM(sForcedHintCam, FALSE)
	KILL_CHASE_HINT_CAM(sForcedHintCam)
ENDPROC

PROC TRIGGER_FORCED_HINT_CAM(INT iTargetType, INT iTargetID, INT iTime)
	CLEAR_FORCED_HINT_CAM_VARS()
	iForcedHintCamTargetType = iTargetType
	iForcedHintCamTargetID = iTargetID
	iForcedHintCamTime = iTime
	RESET_NET_TIMER(tdForcedHintCamReset)
	SET_BIT(iLocalBoolCheck30, LBOOL30_FORCED_HINT_CAM_TRIGGERED)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: MINI_MAP & GPS -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to xxxxxxxxxxxxxxxx ---------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC HIDE_RADAR_THIS_FRAME()
	SET_BIT(iLocalBoolCheck28, LBBOOL28_HIDE_RADAR_THIS_FRAME)
	iRadarHiddenFrame = GET_FRAME_COUNT() + 1
ENDPROC

PROC IGNORE_NO_GPS_FLAG(BOOL bSet)
	IF bSet
		PRINTLN("[JT] SET_IGNORE_NO_GPS_FLAG(TRUE)")
		SET_IGNORE_NO_GPS_FLAG(TRUE)
	ELSE
		PRINTLN("[JT] SET_IGNORE_NO_GPS_FLAG(FALSE)")
		SET_IGNORE_NO_GPS_FLAG(FALSE)
	ENDIF
ENDPROC

PROC ADD_FAKE_VISION_CONE_TO_BLIP(BLIP_INDEX biToUse, FLOAT fVisualFieldMinAzimuthAngle, FLOAT fVisualFieldMaxAzimuthAngle, FLOAT fCentreOfGazeMaxAngle, FLOAT fPeripheralRange, FLOAT fFocusRange, FLOAT fRotation, BOOL bContinuousUpdate)
	
	IF NOT DOES_BLIP_EXIST(biToUse)
		PRINTLN("[RCC MISSION] ADD_FAKE_VISION_CONE_TO_ENTITY - biToUse does not exist, early out")	
		EXIT
	ENDIF
	
	PRINTLN("[RCC MISSION] ADD_FAKE_VISION_CONE_TO_ENTITY - Cone being added with the following settings:")
	PRINTLN("[RCC MISSION] ADD_FAKE_VISION_CONE_TO_ENTITY - fVisualFieldMinAzimuthAngle: ", fVisualFieldMinAzimuthAngle)
	PRINTLN("[RCC MISSION] ADD_FAKE_VISION_CONE_TO_ENTITY - fVisualFieldMaxAzimuthAngle: ", fVisualFieldMaxAzimuthAngle)
	PRINTLN("[RCC MISSION] ADD_FAKE_VISION_CONE_TO_ENTITY - fCentreOfGazeMaxAngle: ", fCentreOfGazeMaxAngle)
	PRINTLN("[RCC MISSION] ADD_FAKE_VISION_CONE_TO_ENTITY - fPeripheralRange: ", fPeripheralRange)
	PRINTLN("[RCC MISSION] ADD_FAKE_VISION_CONE_TO_ENTITY - fFocusRange: ", fFocusRange)
	PRINTLN("[RCC MISSION] ADD_FAKE_VISION_CONE_TO_ENTITY - fRotation: ", fRotation)
	PRINTLN("[RCC MISSION] ADD_FAKE_VISION_CONE_TO_ENTITY - bContinuousUpdate: ", BOOL_TO_STRING(bContinuousUpdate))

	//Clear fake cone array before calling this for the first time
	IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_CLEAR_FAKE_CONE_ARRAY_CALLED)
		PRINTLN("[RCC MISSION] ADD_FAKE_VISION_CONE_TO_ENTITY - calling CLEAR_FAKE_CONE_ARRAY")	
		CLEAR_FAKE_CONE_ARRAY()
		SET_BIT(iLocalBoolCheck25, LBOOL25_CLEAR_FAKE_CONE_ARRAY_CALLED)
	ENDIF
	
	SETUP_FAKE_CONE_DATA(biToUse,fVisualFieldMinAzimuthAngle,fVisualFieldMaxAzimuthAngle,fCentreOfGazeMaxAngle,fPeripheralRange, fFocusRange, fRotation, bContinuousUpdate)
	SET_BLIP_SHOW_CONE(biToUse, TRUE)
	
ENDPROC

PROC REMOVE_CCTV_VISION_CONE(BLIP_INDEX biToUse, INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eType)
	
	IF NOT DOES_BLIP_EXIST(biToUse)
		PRINTLN("[RCC MISSION] REMOVE_CCTV_VISION_CONE - iIndex ", iIndex, " Blip does not exist, exitting.")
		EXIT
	ENDIF
	
	INT iCam = GET_CCTV_INDEX_FOR_ENTITY(iIndex, eType)
	IF iCam = -1
		PRINTLN("[RCC MISSION] REMOVE_CCTV_VISION_CONE - iIndex ", iIndex, " no iCam found")
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iCCTVConeBitSet, ci_CCTV_CONE_BLIP_ADDED_0 + iCam)
		PRINTLN("[RCC MISSION] REMOVE_CCTV_VISION_CONE - iIndex ", iIndex, " is iCam ", iCam, ", but cone blip not yet added")
		EXIT
	ENDIF
	
	PRINTLN("[RCC MISSION] REMOVE_CCTV_VISION_CONE - iIndex ", iIndex, " is iCam ", iCam, ", cone being removed from blip")
	
	SET_BLIP_SHOW_CONE(biToUse, FALSE)
	REMOVE_FAKE_CONE_DATA(biToUse)
	
	CLEAR_BIT(iCCTVConeBitSet, ci_CCTV_CONE_BLIP_ADDED_0 + iCam)
	
ENDPROC

FUNC INT GET_FREE_INDEX_FOR_DISTANCE_TRACKING_BAR()
	INT i = 0
	FOR i = 0 TO ci_TOTAL_DISTANCE_CHECK_BARS-1
		IF iGPSTrackingMeterPedIndex[i] = -1
			RETURN i
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

FUNC BOOL DOES_PED_REQUIRE_DISTANCE_TRACKING(INT iPed, INT iRule)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPED_BSEight_DistanceGPSTracking) 
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedTrackingRule = iRule
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPED_BSEight_DistanceGPSTracking) 
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedTrackingRule = -1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: General Helpers ---------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to filters ------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL CAN_MANUALLY_TOGGLE_HUD()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_AllowHUDToggle)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_THE_LAST_VEHICLE_DELIVERY( INT iTeam, INT iRule )

	IF iRule >= MC_serverBD.iMaxObjectives[ iTeam ]
	AND MC_ServerBD.iNumVehHighestPriority[ iTeam ] = 1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL AM_I_DELIVERING_THE_LAST_VEHICLE_ON_A_GET_AND_DELIVER( INT iTeam, INT iRule )
		
	IF  MC_playerBD[iPartToUse].iVehNear != -1 
	AND (amIWaitingForPlayersToEnterVeh != NULL AND NOT CALL amIWaitingForPlayersToEnterVeh())
	AND IS_THIS_THE_LAST_VEHICLE_DELIVERY( iTeam, iRule ) 
	AND ( 	IS_BIT_SET(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF) OR IS_BIT_SET(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF) )
	AND ( GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_COLLECT_VEH OR GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_DELIVER_VEH )
	AND ( NOT AM_I_IN_A_BOUNCING_PLANE_OR_HELI() )
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
	
ENDFUNC

/// PURPOSE:
///    This should only ever be called from SHOULD_HIDE_THE_HUD_THIS_FRAME
///    Do not call this elsewhere!
FUNC BOOL CHECK_IF_HUD_SHOULD_BE_HIDDEN_THIS_FRAME()
	
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iCurrentRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	BOOL bShouldPrint
	
	iHUDHiddenReason = ciHUDHIDDEN_NOT_HIDDEN
	
	#IF IS_DEBUG_BUILD
		bShouldPrint = TRUE //used to be behind sc_HUDHidingReasons but this can only get hit once per frame now
	#ENDIF
	
	IF IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
	OR (g_bEndOfMissionCleanUpHUD
    AND (GET_STRAND_MISSION_TRANSITION_TYPE() = FMMC_BRANCHING_MISSION_TRANSITION_TYPE_AIRLOCK
	OR ((MC_serverBD.iNextMission > -1 AND MC_serverBD.iNextMission < FMMC_MAX_STRAND_MISSIONS)
	AND g_FMMC_STRUCT.eBranchingTransitionType[MC_serverBD.iNextMission] = FMMC_BRANCHING_MISSION_TRANSITION_TYPE_AIRLOCK))
    AND IS_STRAND_MISSION_READY_TO_START_DOWNLOAD()) //Works earlier
		IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - RETURN FALSE EARLY - AIRLOCK") ENDIF
		RETURN FALSE
	ENDIF
	
	IF bIsAnySpectator
		IF IS_SPECTATOR_HUD_HIDDEN()
			iHUDHiddenReason = ciHUDHIDDEN_SPECTATORHUDHIDDEN
			IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (1)") ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
			
	IF (iCurrentRule >= FMMC_MAX_RULES)
		IF bIsAnySpectator
			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_EARLY_END_SPECTATOR)
				IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (endofmission - 1)") ENDIF
				g_bEndOfMissionCleanUpHUD = TRUE
			ENDIF
		ELSE
			IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (endofmission - 2)") ENDIF
			g_bEndOfMissionCleanUpHUD = TRUE
		ENDIF
	ENDIF
	
	IF (iCurrentRule < FMMC_MAX_RULES)
		IF g_bEndOfMissionCleanUpHUD = TRUE
			g_bEndOfMissionCleanUpHUD = FALSE
		ENDIF
	ENDIF
	
	IF NOT bIsAnySpectator
		IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_MANUALLY_HIDDEN_HUD)
			IF GET_FRAME_COUNT() % 10 = 0
				PRINTLN("SHOULD_HIDE_THE_HUD_THIS_FRAME - Hiding due to LBOOL28_MANUALLY_HIDDEN_HUD")
			ENDIF
			IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (7)") ENDIF
			iHUDHiddenReason = ciHUDHIDDEN_MANUALLY_HIDDEN_HUD
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF MC_playerBD[iPartToUse].iObjHacking != -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_playerBD[iPartToUse].iObjHacking].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__DOWNLOAD
			iHUDHiddenReason = ciHUDHIDDEN_INTERACT_WITH_DOWNLOAD
			IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (8)") ENDIF
			RETURN TRUE
		ENDIF
		IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_STARTED_BEAM_HACK)
			iHUDHiddenReason = ciHUDHIDDEN_BEAMHACK
			IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (9)") ENDIF
			RETURN TRUE
		ENDIF
		IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_STARTED_HOTWIRE_HACK)
			iHUDHiddenReason = ciHUDHIDDEN_HOTWIRE
			IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (10)") ENDIF
			RETURN TRUE
		ENDIF	
		
		IF sVaultDrillData.iCurrentState = VAULT_DRILL_MINIGAME_STATE_WAIT_FOR_INTRO
		OR sVaultDrillData.iCurrentState = VAULT_DRILL_MINIGAME_STATE_WAIT_FOR_OUTRO
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sInteractWithVars.iInteractWith_Bitset, ciInteractWith_Bitset__HideHUD)
	AND sInteractWithVars.eInteractWith_CurrentState != IW_STATE_IDLE
		iHUDHiddenReason = ciHUDHIDDEN_INTERACT_WITH
		IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - Interact-With") ENDIF
		RETURN TRUE
	ENDIF

	// General checks for everyone
	BOOL bHideTheHud
	
	IF g_bEndOfMissionCleanUpHUD
		IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (11)") ENDIF
		bHideTheHud = TRUE
	ENDIF
	
	IF HAS_TEAM_FINISHED( iTeam ) //url:bugstar:2233889
		IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (12)") ENDIF
		bHideTheHud = TRUE
	ENDIF
			
	IF IS_BIT_SET( MC_playerBD[ iLocalPart ].iClientBitSet, PBBOOL_FINISHED )
		IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (13)") ENDIF
		bHideTheHud = TRUE
	ENDIF		
	
	IF IS_CUTSCENE_PLAYING()
		IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (14)") ENDIF
		bHideTheHud = TRUE
	ENDIF		
	
	IF AM_I_DELIVERING_THE_LAST_VEHICLE_ON_A_GET_AND_DELIVER( iTeam, iCurrentRule )
		IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (15)") ENDIF
		bHideTheHud = TRUE
	ENDIF		
	
	IF NOT IS_BIT_SET( MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING )
	AND NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
		IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (16)") ENDIF
		bHideTheHud = TRUE
	ENDIF		
	
	IF IS_BIT_SET( iLocalBoolCheck13, LBOOL13_DRAW_DISTANCE_TO_CROSS_LINE ) AND NOT IS_BIT_SET( iLocalBoolCheck13, LBOOL13_FIRST_STAGGERED_LOOP_DONE_CLIENTSIDE )	
		IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (17)") ENDIF
		bHideTheHud = TRUE
	ENDIF
	
	IF IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
		IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (18)") ENDIF
		bHideTheHud = TRUE
	ENDIF
	
	IF bHideTheHud
		IF bIsAnySpectator
			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_EARLY_END_SPECTATOR)
			OR NOT IS_A_SPECTATOR_CAM_RUNNING()
				iHUDHiddenReason = ciHUDHIDDEN_GENERAL_CHECKS_1
				IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (19)") ENDIF	
				RETURN TRUE
			ENDIF
		ELSE
			iHUDHiddenReason = ciHUDHIDDEN_GENERAL_CHECKS_2
			IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (20)") ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	iHUDHiddenReason = ciHUDHIDDEN_NOT_HIDDEN
	IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - RETURN FALSE") ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_HIDE_THE_HUD_THIS_FRAME()
	
	IF IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_HUD_SHOULD_BE_HIDDEN_THIS_FRAME_CHECKED)
		RETURN IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_HUD_SHOULD_BE_HIDDEN_THIS_FRAME)
	ENDIF
	
	SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_HUD_SHOULD_BE_HIDDEN_THIS_FRAME_CHECKED)
	
	IF CHECK_IF_HUD_SHOULD_BE_HIDDEN_THIS_FRAME()
		SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_HUD_SHOULD_BE_HIDDEN_THIS_FRAME)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC INT GET_REASON_HUD_IS_HIDDEN()
	RETURN iHUDHiddenReason
ENDFUNC

PROC DISABLE_HUD(BOOL bPreventPhoneHangup = FALSE, BOOL bKeepFeed = FALSE)

	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	IF GET_REASON_HUD_IS_HIDDEN() != ciHUDHIDDEN_MANUALLY_HIDDEN_HUD //Don't disable the D-pad down menu if the hud is just manually hidden
		DISABLE_DPADDOWN_THIS_FRAME()
	ENDIF
	
	IF NOT bPreventPhoneHangup
	AND GET_REASON_HUD_IS_HIDDEN() != ciHUDHIDDEN_MANUALLY_HIDDEN_HUD //Don't disable the phone if the hud is just manually hidden
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	ENDIF
	
	DISABLE_SELECTOR_THIS_FRAME() 
	
	IF GET_REASON_HUD_IS_HIDDEN() != ciHUDHIDDEN_MANUALLY_HIDDEN_HUD //Don't disable the main menu if the hud is just manually hidden
		DISABLE_FRONTEND_THIS_FRAME()
	ENDIF
	
	CLEAR_INVENTORY_BOX_CONTENT()
	DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
	
	IF GET_REASON_HUD_IS_HIDDEN() != ciHUDHIDDEN_MANUALLY_HIDDEN_HUD //Don't hide all hud if the hud is only manually hidden
		HIDE_HUD_AND_RADAR_THIS_FRAME()
	ELSE
		HIDE_RADAR_THIS_FRAME()
	ENDIF
	
	DISABLE_ALL_MP_HUD_THIS_FRAME(GET_REASON_HUD_IS_HIDDEN() != ciHUDHIDDEN_MANUALLY_HIDDEN_HUD) //Don't disable the D-pad down menu if the hud is just manually hidden
	IF NOT bKeepFeed
		THEFEED_HIDE_THIS_FRAME()
		IF GET_REASON_HUD_IS_HIDDEN() != ciHUDHIDDEN_INTERACT_WITH
			THEFEED_FLUSH_QUEUE()
		ENDIF
		THEFEED_FORCE_RENDER_OFF()
	ENDIF
	
ENDPROC

FUNC INT GET_TEAM_SCORE_FOR_DISPLAY(INT iTeam)
	RETURN MC_serverBD.iTeamScore[iTeam] - MC_ServerBD.iPointsGivenToPass[iTeam]
ENDFUNC


FUNC PERCENTAGE_METER_LINE GET_PERCENTAGE_METER_LINE_FROM_PERCENTAGE_INT(INT iPercent)
	
	PERCENTAGE_METER_LINE percentLine = PERCENTAGE_METER_LINE_NONE
		
	SWITCH iPercent
		CASE 0
			percentLine = PERCENTAGE_METER_LINE_NONE
		BREAK
		CASE 10
			percentLine = PERCENTAGE_METER_LINE_10
		BREAK
		CASE 20
			percentLine = PERCENTAGE_METER_LINE_20
		BREAK
		CASE 30
			percentLine = PERCENTAGE_METER_LINE_30
		BREAK
		CASE 40
			percentLine = PERCENTAGE_METER_LINE_40
		BREAK
		CASE 50
			percentLine = PERCENTAGE_METER_LINE_50
		BREAK
		CASE 60
			percentLine = PERCENTAGE_METER_LINE_60
		BREAK
		CASE 70
			percentLine = PERCENTAGE_METER_LINE_70
		BREAK
		CASE 80
			percentLine = PERCENTAGE_METER_LINE_80
		BREAK
		CASE 90
			percentLine = PERCENTAGE_METER_LINE_90
		BREAK
	ENDSWITCH
	
	RETURN percentLine
	
ENDFUNC

FUNC FLOAT GET_SCORE_PERCENTAGE_FOR_HUD(INT iTeam)
	IF iTeam < FMMC_MAX_TEAMS
		FLOAT fScorePercentage
		INT iCurrentScore 
		iCurrentScore = GET_TEAM_SCORE_FOR_DISPLAY(iTeam) - g_FMMC_STRUCT.iInitialPoints[iTeam]
		INT iTargetScore
		INT iRule 
		iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule < FMMC_MAX_RULES
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore > 0
				iTargetScore = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore
			ELSE
				iTargetScore = GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[0], MC_ServerBD.iNumberOfPlayingPlayers[iTeam], MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam])
			ENDIF
			
			fScorePercentage =  TO_FLOAT(iCurrentScore) / TO_FLOAT(iTargetScore)
			
		ENDIF
		
		RETURN fScorePercentage
	ENDIF
	RETURN 0.0
ENDFUNC

FUNC BOOL IS_FINAL_COUNTDOWN_ACTIVE(INT iTeam, INT iRule, INT iTimeRemaining)
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_COUNTDOWN_TIMER_SOUND_THIRTYSECONDS) AND iTimeRemaining <= 30000
		OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_COUNTDOWN_TIMER_SOUND_TENSECOND) AND iTimeRemaining <= 10000
		OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_COUNTDOWN_TIMER_SOUND_FIVESECOND) AND iTimeRemaining <= 5000
		OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_COUNTDOWN_TIMER_SOUND_ONESHOT) AND iTimeRemaining <= 1000
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC TEXT_LABEL_63 GET_ENTITY_CUSTOM_NAME(INT iEntityType, INT iEntityIndex)
	
	TEXT_LABEL_63 tlCustomName = ""
	
	SWITCH iEntityType
		CASE ciENTITY_TYPE_PED
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntityIndex].iCustomPedName > -1
				tlCustomName = g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntityIndex].iCustomPedName]
			ENDIF
		BREAK
		
		CASE ciENTITY_TYPE_VEHICLE
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityIndex].iCustomVehName > -1
				tlCustomName = g_FMMC_STRUCT_ENTITIES.tlCustomVehName[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityIndex].iCustomVehName]
			ENDIF
		BREAK
		
		CASE ciENTITY_TYPE_OBJECT
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityIndex].iCustomObjName > -1
				tlCustomName = g_FMMC_STRUCT_ENTITIES.tlCustomObjName[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityIndex].iCustomObjName]
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN tlCustomName
ENDFUNC

FUNC BOOL DOES_ENTITY_HAVE_CUSTOM_NAME(INT iEntityType, INT iEntityIndex)
	TEXT_LABEL_63 tlCustomName = GET_ENTITY_CUSTOM_NAME(iEntityType, iEntityIndex)
	RETURN NOT IS_STRING_NULL_OR_EMPTY(tlCustomName)
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: FILTERS ---------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to filters ------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC STRING GET_FILTER_NAME(INT iFilter)
	SWITCH iFilter
		CASE ciFILTER_BITSET_BLUEISH		RETURN "spectator1"
		CASE ciFILTER_BITSET_OLDTV			RETURN "spectator2"
		CASE ciFILTER_BITSET_SEPIA			RETURN "spectator3"
		CASE ciFILTER_BITSET_BLACKWHITE	 	RETURN "spectator4"
		CASE ciFILTER_BITSET_TRIPPIN		RETURN "spectator5"
		CASE ciFILTER_BITSET_LOWSAT		 	RETURN "spectator6"
		CASE ciFILTER_BITSET_BLUEISHPURP	RETURN "spectator7"
		CASE ciFILTER_BITSET_NOIRE			RETURN "spectator8"
		CASE ciFILTER_BITSET_HIGHSAT		RETURN "spectator9"
		CASE ciFILTER_BITSET_NIGHTVISION	RETURN "spectator10"
		CASE ciFILTER_BITSET_WHITE			RETURN "InchPickup"
		CASE ciFILTER_BITSET_ORANGE		 	RETURN "PPOrange"
		CASE ciFILTER_BITSET_PURPLE			RETURN "PPPurple"
		CASE ciFILTER_BITSET_GREEN			RETURN "PPGreen"
		CASE ciFILTER_BITSET_PINK			RETURN "PPPink"
		CASE ciFILTER_BITSET_DEADLINE_NEON	RETURN "DeadlineNeon"
		CASE ciFILTER_BITSET_STONED			RETURN "STONED"
		CASE ciFILTER_BITSET_SHORT_TRIP		RETURN "FixerShortTrip"
		CASE ciFILTER_BITSET_DARK_PARTY		RETURN "DLC_MpSecurity_Penthouse_Party"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Checks if there is any filter playing - please add more filters to this as needed
FUNC BOOL IS_ANY_FILTER_ACTIVE()
	
	IF ANIMPOSTFX_IS_RUNNING("InchPickup")
	OR ANIMPOSTFX_IS_RUNNING("PPOrange")
	OR ANIMPOSTFX_IS_RUNNING("PPPurple")
	OR ANIMPOSTFX_IS_RUNNING("PPGreen")
	OR ANIMPOSTFX_IS_RUNNING("PPPink")
	OR ANIMPOSTFX_IS_RUNNING("DeadlineNeon")
	OR GET_TIMECYCLE_MODIFIER_INDEX() != -1
		//PRINTLN("[KH][FILTERS] IS_ANY_FILTER_ACTIVE - TRUE")
		RETURN TRUE
	ENDIF
	
	//PRINTLN("[KH][FILTERS] IS_ANY_FILTER_ACTIVE - FALSE")
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_FILTER_A_TIMECYCLE_MODIFIER(INT iFilterSelection)
	//Include 0 because it takes time to transition out of the timecycle modifier
	SWITCH iFilterSelection
		CASE ciFILTER_OFF
		CASE ciFILTER_BITSET_BLUEISH
		CASE ciFILTER_BITSET_OLDTV
		CASE ciFILTER_BITSET_SEPIA
		CASE ciFILTER_BITSET_BLACKWHITE
		CASE ciFILTER_BITSET_TRIPPIN
		CASE ciFILTER_BITSET_LOWSAT
		CASE ciFILTER_BITSET_BLUEISHPURP
		CASE ciFILTER_BITSET_NOIRE
		CASE ciFILTER_BITSET_HIGHSAT
		CASE ciFILTER_BITSET_NIGHTVISION
		CASE ciFILTER_BITSET_STONED
		CASE ciFILTER_BITSET_SHORT_TRIP
		CASE ciFILTER_BITSET_DARK_PARTY
	
		RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_NEXT_VALID_FILTER(INT iCurrentFilter)
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	INT iCacheFilter = iCurrentFilter
	
	IF iRule < FMMC_MAX_RULES
		INT iFilterLoop
		
		FOR iFilterLoop = 0 TO (ciMAX_FILTERS - 1)
			IF iCurrentFilter != iCacheFilter
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iFilterBitset[iRule], iCurrentFilter)
					RETURN iCurrentFilter
				ENDIF
			ENDIF
			
			IF iCurrentFilter < (ciMAX_FILTERS - 1)
				iCurrentFilter++
			ELSE
				iCurrentFilter = 0
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC BOOL SHOULD_RED_FILTER_SHOW(INT iTeam, INT iRule, INT iLoc)
	IF ((NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_ONLY_USE_VFX_WHEN_CAPTURED))
	OR (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_ONLY_USE_VFX_WHEN_CAPTURED) AND MC_serverBD.iLocOwner[iLoc] = iTeam))
	AND NOT HAS_TEAM_FINISHED(iTeam)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: TAGGED BLIPS ----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to xxxxxxxxxxxxxxxx ---------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_TAG_BLIP_COLOUR(INT iEntType, INT iEntIndex)
	SWITCH iEntType
		CASE 1 RETURN GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntIndex].iTagColour)
		CASE 2 RETURN GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntIndex].iTagColour)
		CASE 3 RETURN GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iEntIndex].iTagColour)
		CASE 4 RETURN GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntIndex].iTagColour)
	ENDSWITCH
	RETURN BLIP_COLOUR_RED
ENDFUNC

FUNC HUD_COLOURS GET_TAG_MARKER_COLOUR(INT iEntType, INT iEntIndex)
	SWITCH iEntType
		CASE 1 RETURN GET_HUD_COLOUR_FROM_CREATOR_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntIndex].iTagColour)
		CASE 2 RETURN GET_HUD_COLOUR_FROM_CREATOR_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntIndex].iTagColour)
		CASE 3 RETURN GET_HUD_COLOUR_FROM_CREATOR_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iEntIndex].iTagColour)
		CASE 4 RETURN GET_HUD_COLOUR_FROM_CREATOR_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntIndex].iTagColour)
	ENDSWITCH
	RETURN HUD_COLOUR_RED
ENDFUNC

PROC DRAW_TAGGED_MARKER(VECTOR vTaggedEntityPos, ENTITY_INDEX EntityToUse, INT iEntType, INT iEntIndex)
	VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
	
	INT r, g, b, a
	GET_HUD_COLOUR(GET_TAG_MARKER_COLOUR(iEntType, iEntIndex), r, g, b, a)
	
	IF (vPlayerPos.z - vTaggedEntityPos.z) >= 30
		DRAW_MARKER(MARKER_RING, vTaggedEntityPos, <<0,0,1>>, <<0,0,0>>, <<5,5,5>>, r, g, b, 100, TRUE)
	ELSE
		VECTOR returnMin, returnMax
		FLOAT fOffset
		
		fOffset = 0.5 
		
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(EntityToUse), returnMin, returnMax)

		FLOAT fCentreHeight = (returnMax.z - returnMin.z)/2
		FLOAT fZdiff = returnMax.z - fCentreHeight
		
		IF fOffset <= (fZdiff + 0.1)
			fOffset = fZdiff + 0.4
		ENDIF
		VECTOR vScale = <<1,1,1>>
		IF VDIST2(vPlayerPos, vTaggedEntityPos) > POW(75,2)
			vScale = <<1.5,1.5,1.5>>
		ENDIF
		
		vTaggedEntityPos.z += ((returnMax.z - returnMin.z)/2) + fOffset

		DRAW_MARKER(MARKER_ARROW, vTaggedEntityPos, <<0,0,0>>, <<180,0,0>>,vScale, r, g, b, 100, TRUE, TRUE)
	ENDIF
ENDPROC

FUNC BOOL IS_ENTITY_ALREADY_TAGGED(INT iEntityType, INT iEntIndexToUse, INT iEntityGangChaseUnit = -1)
	INT iTagged
	FOR iTagged = 0 TO FMMC_MAX_TAGGED_ENTITIES-1
		
		IF MC_serverBD_3.iTaggedEntityType[iTagged] != iEntityType
			RELOOP
		ENDIF
		
		IF MC_serverBD_3.iTaggedEntityIndex[iTagged] != iEntIndexToUse
			RELOOP
		ENDIF		
				
		IF iEntityGangChaseUnit >= 0
		AND MC_serverBD_3.iTaggedEntityGangChaseUnit[iTagged] != iEntityGangChaseUnit
			RELOOP
		ENDIF
		
		PRINTLN("[RCC MISSION][TAGGING] - Tag Check - This Entity is already tagged. Tag index: ", iTagged)
		RETURN TRUE
		
	ENDFOR
	
	RETURN FALSE
ENDFUNC

PROC ADD_TAGGED_BLIP(ENTITY_INDEX eiEntityToUse, INT iEntityType, INT iEntIndexToUse, INT iTagIndex)
	IF NOT DOES_BLIP_EXIST(biTaggedEntity[iTagIndex])
	
		REMOVE_BLIP(biTaggedEntity[iTagIndex])
		
		biTaggedEntity[iTagIndex] = ADD_BLIP_FOR_ENTITY(eiEntityToUse)
		SET_BLIP_COLOUR(biTaggedEntity[iTagIndex], GET_TAG_BLIP_COLOUR(iEntityType, iEntIndexToUse))
		
		IF DOES_BLIP_EXIST(biTaggedEntity[iTagIndex])
			PRINTLN("[RCC MISSION][TAGGING] - Blip ",iTagIndex, " Added")
		ENDIF
	ENDIF	
ENDPROC

PROC REMOVE_TAGGED_BLIP(INT iEntityType, INT iEntIndexToUse)
	INT iTagged
	FOR iTagged = 0 TO FMMC_MAX_TAGGED_ENTITIES-1
		IF DOES_BLIP_EXIST(biTaggedEntity[iTagged])
			IF MC_serverBD_3.iTaggedEntityType[iTagged] = iEntityType
				IF MC_serverBD_3.iTaggedEntityIndex[iTagged] = iEntIndexToUse
					IF DOES_BLIP_EXIST(biTaggedEntity[iTagged])
						REMOVE_BLIP(biTaggedEntity[iTagged])
						IF NOT DOES_BLIP_EXIST(biTaggedEntity[iTagged])
							PRINTLN("[RCC MISSION][TAGGING] - Blip ",iTagged, " removed")
						ENDIF
						IF bIsLocalPlayerHost
							CLEAR_BIT(MC_serverBD_3.iTaggedEntityBitset, iTagged)
							MC_serverBD_3.iTaggedEntityType[iTagged] = 0
							MC_serverBD_3.iTaggedEntityIndex[iTagged] = -1
							MC_serverBD_3.iTaggedEntityGangChaseUnit[iTagged] = -1
						ENDIF
						BREAKLOOP
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL IS_IT_SAFE_TO_ADD_TAG_BLIP(INT iRule)
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitsetTen[iRule], ciBS_RULE10_TAGGING_HIDE_NOT_ACTIVE) AND NOT IS_TARGET_TAGGING_ENABLED_THIS_RULE(MC_playerBD[iLocalPart].iteam, iRule)
			PRINTLN("[RCC MISSION] IS_IT_SAFE_TO_ADD_TAG_BLIP - RETURNING FALSE - Option Set")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC


PROC PROCESS_PED_TAGGING_HUD(FMMC_PED_STATE &sPedState)
	
	IF NOT SHOULD_SHOW_TAGGED_TARGET()
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF NOT IS_IT_SAFE_TO_ADD_TAG_BLIP(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam])
		EXIT
	ENDIF

	INT iTagged
	FOR iTagged = 0 TO FMMC_MAX_TAGGED_ENTITIES-1
		
		IF MC_serverBD_3.iTaggedEntityType[iTagged] != ENUM_TO_INT(GET_ENTITY_TYPE(sPedState.pedIndex))
			RELOOP
		ENDIF
		
		IF MC_serverBD_3.iTaggedEntityIndex[iTagged] != sPedState.iIndex
			RELOOP
		ENDIF
		
		// if ped state has a valid gang chase unit 
		IF sPedState.iGangChaseUnit >= 0
			IF MC_serverBD_3.iTaggedEntityGangChaseUnit[iTagged] != sPedState.iGangChaseUnit
				RELOOP
			ENDIF
		// if ped state has a invalid gang chase unit but the equivalent gang chase ped has been tagged
		ELIF MC_serverBD_3.iTaggedEntityGangChaseUnit[iTagged] >= 0
			RELOOP
		ENDIF				
		
		IF NOT sPedState.bInjured
			VECTOR vEntityCoords = GET_FMMC_PED_COORDS(sPedState)
			vEntityCoords.z = (vEntityCoords.z + 1.0)
			DRAW_TAGGED_MARKER(vEntityCoords, sPedState.pedIndex, ENUM_TO_INT(GET_ENTITY_TYPE(sPedState.pedIndex)), sPedState.iIndex)
					
			IF NOT DOES_BLIP_EXIST(biTaggedEntity[iTagged])
			AND NOT DOES_BLIP_EXIST(GET_BLIP_FROM_ENTITY(sPedState.pedIndex))
				ADD_TAGGED_BLIP(sPedState.pedIndex, ENUM_TO_INT(GET_ENTITY_TYPE(sPedState.pedIndex)), sPedState.iIndex, iTagged)
			ENDIF
		ENDIF
			
	ENDFOR
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Player Lives and Respawning ------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to xxxxxxxxxxxxxxxx ---------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//Badly named
PROC PROCESS_REGEN_HUD(BOOL bFlash, PLAYER_INDEX tempPlayer, INT iTeam, INT iRule)
	
	IF bFlash
		IF NOT IS_BLIP_FLASHING_FOR_PLAYER(tempPlayer)
			FLASH_BLIP_FOR_PLAYER(tempPlayer, TRUE, 1000)
		ENDIF
	ELSE		
		IF IS_BLIP_FLASHING_FOR_PLAYER(tempPlayer)
			FLASH_BLIP_FOR_PLAYER(tempPlayer, FALSE, -1)
		ENDIF
	ENDIF
	
	IF NOT DOES_BLIP_EXIST(biRegenBlip)
		PRINTLN("[JS][TEAMREGEN] - PROCESS_REGEN_HUD - Adding zone blip")
		biRegenBlip = ADD_BLIP_FOR_RADIUS(GET_ENTITY_COORDS(GET_PLAYER_PED(tempPlayer)), g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sRegenStruct[ iRule ].fDistance)
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(biRegenBlip, GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, tempPlayer))
		SET_BLIP_ALPHA(biRegenBlip, 128)
	ELSE
		SET_BLIP_COORDS(biRegenBlip, GET_ENTITY_COORDS(GET_PLAYER_PED(tempPlayer)))
	ENDIF

ENDPROC

PROC DRAIN_KILL_TICKER_MESSAGE()
	// Ticker: The Health Drain System killed a player.
	SCRIPT_EVENT_DATA_TICKER_CUSTOM_MESSAGE TickerMessage											
	TickerMessage.TickerEvent = TICKER_EVENT_RATC_HDRAIN_KILL					
	BROADCAST_CUSTOM_TICKER_EVENT(TickerMessage, ALL_PLAYERS_ON_SCRIPT(TRUE))
ENDPROC

PROC DRAIN_HEALTH_WASTED_SHARD(INT iHealth)
	
	// If I have been shot in the past 10 seconds then set the Health Drain Override bit to the iLocalPart
	IF HAS_NET_TIMER_EXPIRED(tdNotKilledByHealthDrainTimer, 5000)
		IF IS_BIT_SET(iNotKilledByHealthDrainOverrideBS, iLocalPart)
			CLEAR_BIT(iNotKilledByHealthDrainOverrideBS, iLocalPart)
			IF bLocalPlayerPedOK
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(LocalPlayerPed)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iNotKilledByHealthDrainOverrideBS, iLocalPart)
			SET_BIT(iNotKilledByHealthDrainOverrideBS, iLocalPart)
		ENDIF
	ENDIF
	
	INT iTeam = MC_PlayerBD[iPartToUse].iteam
	IF iTeam < FMMC_MAX_TEAMS
		IF iHealth < 100
		AND IS_BIT_SET(iKilledByHealthDrainBS, iLocalPart)	
		AND NOT IS_BIT_SET(iNotKilledByHealthDrainOverrideBS, iLocalPart)
		AND !bLocalPlayerPedOK
		AND NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_CUSTOM_FAILED)
		AND NOT IS_SCREEN_FADING_IN()
			SET_BIG_MESSAGE_SUPPRESS_WASTED(TRUE)
			CHANGE_WASTED_SHARD_TEXT(TRUE)
			SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_CUSTOM_FAILED, g_FMMC_STRUCT.tlCustomWastedStrapline[iTeam], "", DEFAULT, DEFAULT, "RESPAWN_W_MP")
			DRAIN_KILL_TICKER_MESSAGE()
			PRINTLN("[LM][MISSION_CONTROLLER][DRAIN_HEALTH] - Health drain killed player, set bit.")
		ELSE
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_CUSTOM_FAILED)
			CLEAR_BIT(iKilledByHealthDrainBS, iLocalPart)
			SET_BIG_MESSAGE_SUPPRESS_WASTED(FALSE)
			CHANGE_WASTED_SHARD_TEXT(FALSE)
		ENDIF
	ENDIF
ENDPROC

PROC SET_TEAM_LIVES_REMAINING(INT iPlayer, INT iTeam)
	BOOL bTeamLives = IS_MISSION_TEAM_LIVES()
	BOOL bCoopLives = IS_MISSION_COOP_TEAM_LIVES()

	IF bTeamLives
		iTeamLivesHUD[iTeam] = (GET_FMMC_MISSION_LIVES_FOR_LOCAL_PLAYER() - GET_TEAM_DEATHS(iTeam))
	ELIF bCoopLives
		iTeamLivesHUD[iTeam] = (GET_FMMC_MISSION_LIVES_FOR_COOP_TEAMS() - GET_COOP_TEAMS_TOTAL_DEATHS(iTeam))
	ELSE
		iTeamLivesHUD[iTeam] = (GET_FMMC_MISSION_LIVES_FOR_LOCAL_PLAYER() - MC_PlayerBD[iPlayer].iNumPlayerDeaths)
	ENDIF
ENDPROC

PROC SHOW_MANUAL_RESPAWN_TEXT()

	STRING sManualRespawnHelptextLabel = "HEIST_HELP_44"
	
	IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_CAN_SHOW_MANUAL_RESPAWN_HELP_TEXT)

		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciUSE_SHORT_MANUAL_RESPAWN_TIME)
			PRINT_HELP(sManualRespawnHelptextLabel, 5000)
			SET_BIT(iLocalBoolCheck9, LBOOL9_CAN_SHOW_MANUAL_RESPAWN_HELP_TEXT)
		ELSE
			PRINT_HELP(sManualRespawnHelptextLabel, 1000000)
			SET_BIT(iLocalBoolCheck9, LBOOL9_CAN_SHOW_MANUAL_RESPAWN_HELP_TEXT)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciUSE_SHORT_MANUAL_RESPAWN_TIME)
		// IF there is no message being displayed, and we're supposed to be sending this help message, then clear the bit.
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_BIT(iLocalBoolCheck9, LBOOL9_CAN_SHOW_MANUAL_RESPAWN_HELP_TEXT)	
			PRINTLN("[MISSION CONTROLLER][IS_RESPAWN_TEXT_RELEVANT] - CLEARING TEXT")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_SHOW_TEAM_LIVES()
	IF IS_LOCAL_PLAYER_USING_TURRET_CAM()
		PRINTLN("SHOULD_SHOW_TEAM_LIVES - Fail 0")
		RETURN FALSE
	ENDIF
	IF IS_SPECTATOR_HUD_HIDDEN()
		PRINTLN("SHOULD_SHOW_TEAM_LIVES - Fail 1")
		RETURN FALSE
	ENDIF
	IF IS_SPECTATOR_SHOWING_NEWS_HUD()
		PRINTLN("SHOULD_SHOW_TEAM_LIVES - Fail 2")
		RETURN FALSE
	ENDIF
	IF IS_SPECTATOR_HUD_HIDDEN()
		PRINTLN("SHOULD_SHOW_TEAM_LIVES - Fail 3")
		RETURN FALSE
	ENDIF
	IF g_bCelebrationScreenIsActive
		PRINTLN("SHOULD_SHOW_TEAM_LIVES - Fail 4")
		RETURN FALSE
	ENDIF
	IF IS_CIRCUIT_HACKING_MINIGAME_RUNNING(hackingMinigameData)
		PRINTLN("SHOULD_SHOW_TEAM_LIVES - Fail 5")
		RETURN FALSE
	ENDIF
	IF IS_CUTSCENE_PLAYING()
		PRINTLN("SHOULD_SHOW_TEAM_LIVES - Fail 6")
		RETURN FALSE
	ENDIF
	IF IS_BIT_SET( iLocalBoolCheck5, LBOOL5_PED_INVOLVED_IN_CUTSCENE )
		PRINTLN("SHOULD_SHOW_TEAM_LIVES - Fail 7")
		RETURN FALSE
	ENDIF
	IF NOT IS_BIT_SET( iLocalBoolCheck9, LBOOL9_HAS_FIRST_OBJECTIVE_TEXT_BEEN_SHOWN )
		PRINTLN("SHOULD_SHOW_TEAM_LIVES - Fail 8")
		RETURN FALSE
	ENDIF
	IF SHOULD_HIDE_THE_HUD_THIS_FRAME()
		PRINTLN("SHOULD_SHOW_TEAM_LIVES - Fail 9")
		RETURN FALSE
	ENDIF
	IF g_bEndOfMissionCleanUpHUD
		PRINTLN("SHOULD_SHOW_TEAM_LIVES - Fail 10")
		RETURN FALSE
	ENDIF
	IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		PRINTLN("SHOULD_SHOW_TEAM_LIVES - Fail 11")
		RETURN FALSE
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciHIDE_PLAYER_LIVES_HUD)
		PRINTLN("SHOULD_SHOW_TEAM_LIVES - Fail 12")
		RETURN FALSE
	ENDIF
	IF IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
		PRINTLN("SHOULD_SHOW_TEAM_LIVES - Fail 13")
		RETURN FALSE
	ENDIF	
	IF IS_PLAYER_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD)
		PRINTLN("SHOULD_SHOW_TEAM_LIVES - Fail 14")
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE: This function draws HUD elements in the bottom right displaying items which indicate how many players are left alive on each team
PROC DRAW_FMMC_PLAYERS_REMAINING_HUD()
	
	BOOL skip
	TEXT_LABEL_63 LocalTitle
	BOOL bLiteral = TRUE
	BOOL bCustomString
	HUD_COLOURS hudColour
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset2, ciBS2_USE_BLUE_SCOREBOARD)
		hudColour = HUD_COLOUR_WHITE
	ELSE
		hudColour = HUD_COLOUR_BLUE
	ENDIF
	HUD_COLOURS teamColour
	HUD_COLOURS PackageColour[8]
	HUDORDER scoreHudOrder
	
	INT iLoop
	INT i
	INT iPlayers[FMMC_MAX_TEAMS]
	
	FOR iLoop = 0 TO (FMMC_MAX_TEAMS - 1)
		iPlayers[iLoop] = 0
	ENDFOR
	PRINTLN("[PLAYER_LOOP] - DRAW_FMMC_PLAYERS_REMAINING_HUD")
	FOR i = 0 TO NUM_NETWORK_PLAYERS-1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))			
			BOOL bInstantRespawn = FALSE
			IF MC_playerBD[i].iteam > -1
				bInstantRespawn = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[i].iteam].iTeamBitSet2, ciBS2_ALLOW_INSTANT_RESPAWN_ON_DEATH)
			ENDIF
			
			IF (IS_ENTITY_ALIVE(GET_PLAYER_PED(INT_TO_PLAYERINDEX(i))) OR (bInstantRespawn AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_PLAYER_FAIL)))
			AND NOT IS_PLAYER_SPECTATING(INT_TO_PLAYERINDEX(i))
			AND NOT IS_PARTICIPANT_A_SPECTATOR(i)
				iPlayers[MC_playerBD[i].iteam]++
			ENDIF
		ENDIF
	ENDFOR
	FOR i = 0 TO (FMMC_MAX_TEAMS - 1)
		
		skip = FALSE
		
		// If the bit is not set, then skip the displaying of that particular Team.
		SWITCH(i)
		CASE 0  
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iCanSeeTeam1Lives, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
				skip = TRUE
			ENDIF
			BREAK
		CASE 1  
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iCanSeeTeam2Lives, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
				skip = TRUE
			ENDIF
			BREAK
		CASE 2  
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iCanSeeTeam3Lives, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
				skip = TRUE
			ENDIF
			BREAK
		CASE 3  
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iCanSeeTeam4Lives, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
				skip = TRUE
			ENDIF
			BREAK
		ENDSWITCH

		IF !skip
			IF iPlayers[i] > 0
				IF MC_serverBD_4.iCurrentHighestPriority[i] < FMMC_MAX_RULES
					bCustomString = ( IS_BIT_SET(g_FMMC_STRUCT.iCustomTargetStringRuleBitset[i],MC_serverBD_4.iCurrentHighestPriority[i])
				   					  AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23CustomTargetStringTwo[i]) )
				ENDIF
				bLiteral = TRUE
				
				IF bCustomString
					LocalTitle = TEXT_LABEL_TO_STRING(g_FMMC_STRUCT.tl23CustomTargetStringTwo[i])
					hudColour = HUD_COLOUR_WHITE
				ELSE
					IF IS_BIT_SET(iCelebrationTeamNameIsNotLiteral, i)
						LocalTitle = g_sMission_TeamName[i]
						LocalTitle = TEXT_LABEL_TO_STRING(g_sMission_TeamName[i])
						bLiteral = FALSE
					ELSE
						LocalTitle = g_sMission_TeamName[i]
					ENDIF
				ENDIF
				
				scoreHudOrder = INT_TO_ENUM(HUDORDER, ENUM_TO_INT(HUDORDER_FOURTHBOTTOM) - i)
				
				teamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(i, PlayerToUse)
				
				IF NOT bCustomString
					hudColour = teamColour
				ENDIF
				
				g_b_ChangePlayerNameToTeamName = TRUE
				IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
				AND NOT IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
					IF iPlayers[i] <= 8
						
						FOR iLoop = 0 TO 7
							IF iLoop < iPlayers[i]
								PackageColour[iLoop] = teamColour
							ELSE
								PackageColour[iLoop] = HUD_COLOUR_BLACK
							ENDIF
						ENDFOR
						
						DRAW_ONE_PACKAGES_EIGHT_HUD(iPlayers[i], LocalTitle, bLiteral, 
										PackageColour[0], PackageColour[1], PackageColour[2], PackageColour[3], PackageColour[4], PackageColour[5], PackageColour[6], PackageColour[7],
										-1, hudColour,
										FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, // If each circle should have a cross through it (all FALSE)
										scoreHudOrder, TRUE)
					ELSE
						DRAW_GENERIC_BIG_NUMBER(iPlayers[i], LocalTitle, -1, hudColour, scoreHudOrder, bLiteral)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

/// PURPOSE: This function draws HUD elements in the bottom right displaying items which indicate how many objects are left to defend
PROC DRAW_FMMC_OBJECTS_LEFT_TO_DEFEND(INT iDefendingTeam)

	IF MC_playerBD[iPartToUse].iteam = iDefendingTeam - 1
		
		PRINTLN("[AW][OBJECTS_TO_DEFEND_HUD] DRAW_FMMC_OBJECTS_LEFT_TO_DEFEND() iDefendingTeam - ", iDefendingTeam)
		PRINTLN("[AW][OBJECTS_TO_DEFEND_HUD] MC_playerBD[iPartToUse].iteam:",MC_playerBD[iPartToUse].iteam)
		PRINTLN("[AW][OBJECTS_TO_DEFEND_HUD] MC_serverBD_1.iTotalPackages:",MC_serverBD_1.iTotalPackages)
		
		IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
			IF iTotalObjectsToDefend = - 1
				iTotalObjectsToDefend = MC_serverBD_1.iTotalPackages
				PRINTLN("[AW][OBJECTS_TO_DEFEND_HUD] DRAW_FMMC_OBJECTS_LEFT_TO_DEFEND() iTotalObjectsToDefend - ", iTotalObjectsToDefend)
			ELSE
				//hardcoded to 8 here because iTotalPackages is being pulled in wrong for spectators and we always have 8 here.
				PRINTLN("[AW][OBJECTS_TO_DEFEND_HUD]DRAW_FMMC_OBJECTS_LEFT_TO_DEFEND - Packages :", MC_serverBD.iNumobjHighestPriority[iDefendingTeam - 1] , "/" , MC_serverBD_1.iTotalPackages)
				DRAW_GENERIC_BIG_DOUBLE_NUMBER(MC_serverBD.iNumobjHighestPriority[iDefendingTeam - 1] , 8 , "D_PACK_HU", -1, HUD_COLOUR_WHITE , HUDORDER_TOP, FALSE)
			ENDIF
		ENDIF
		//hardcoded to 8 for now.
		PRINTLN("[AW][OBJECTS_TO_DEFEND_HUD]DRAW_FMMC_OBJECTS_LEFT_TO_DEFEND - Packages :", MC_serverBD.iNumobjHighestPriority[iDefendingTeam - 1] , "/" , MC_serverBD_1.iTotalPackages)
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(MC_serverBD.iNumobjHighestPriority[iDefendingTeam - 1] , 8 , "D_PACK_HU", -1, HUD_COLOUR_WHITE , HUDORDER_TOP, FALSE)
	ENDIF

ENDPROC

FUNC BOOL IS_MY_TEAM_LIVES_SHOWING()
	// If we can see our own Team Lives (Coloured Variation) then we don't want to see the Generic Team Lives (White Text Version)..
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		SWITCH MC_playerBD[iPartToUse].iteam
			// We can see our own lives. Return True.
			CASE 0
				RETURN IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iCanSeeTeam1Lives, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
			CASE 1
				RETURN IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iCanSeeTeam2Lives, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
			CASE 2
				RETURN IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iCanSeeTeam3Lives, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
			CASE 3
				RETURN IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iCanSeeTeam4Lives, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: This function draws HUD elements in the bottom right displaying items which indicate how many players are left alive on each team
PROC DRAW_FMMC_TEAM_LIVES_REMAINING_HUD()
	
	BOOL skip
	TEXT_LABEL_63 LocalTitle
	BOOL bLiteral = TRUE
	BOOL bCustomString
	HUD_COLOURS hudColour
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset2, ciBS2_USE_BLUE_SCOREBOARD)
		hudColour = HUD_COLOUR_WHITE
	ELSE
		hudColour = HUD_COLOUR_BLUE
	ENDIF
	HUD_COLOURS teamColour
	HUD_COLOURS PackageColour[8]
	HUDORDER scoreHudOrder
	
	INT iLoop
	INT i
		
	INT iOrder = 0
		
	FOR i = 0 TO (FMMC_MAX_TEAMS - 1)
		
		IF IS_TEAM_ACTIVE(i)
		
			skip = FALSE
			
			// If the bit is not set, then skip the displaying of that particular Team.
			SWITCH(i)
			CASE 0  
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iCanSeeTeam1Lives, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
					skip = TRUE
				ENDIF
				BREAK
			CASE 1  
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iCanSeeTeam2Lives, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
					skip = TRUE
				ENDIF
				BREAK
			CASE 2  
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iCanSeeTeam3Lives, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
					skip = TRUE
				ENDIF
				BREAK
			CASE 3  
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iCanSeeTeam4Lives, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
					skip = TRUE
				ENDIF
				BREAK
			ENDSWITCH
			
			IF !skip
				IF MC_serverBD_4.iCurrentHighestPriority[i] < FMMC_MAX_RULES
					bCustomString = ( IS_BIT_SET(g_FMMC_STRUCT.iCustomTargetStringRuleBitset[i],MC_serverBD_4.iCurrentHighestPriority[i])
				   					  AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23CustomTargetStringTwo[i]) )
				ENDIF
				bLiteral = TRUE
				
				IF bCustomString
					LocalTitle = TEXT_LABEL_TO_STRING(g_FMMC_STRUCT.tl23CustomTargetStringTwo[i])
					hudColour = HUD_COLOUR_WHITE
				ELSE
					IF IS_BIT_SET(iCelebrationTeamNameIsNotLiteral, i)
						LocalTitle = g_sMission_TeamName[i]
						//LocalTitle = GET_FILENAME_FOR_AUDIO_CONVERSATION("MBOX_LIVES0")
						LocalTitle = TEXT_LABEL_TO_STRING(g_sMission_TeamName[i])
						PRINTLN("[RCC MISSION] DRAW_FMMC_TEAM_LIVES_REMAINING_HUD() - not literal")
						IF NOT IS_BIT_SET(g_iTeamNameBitSetPlayerName, i)
							bLiteral = FALSE
						ENDIF
					ELSE
						//LocalTitle = GET_FILENAME_FOR_AUDIO_CONVERSATION("MBOX_LIVES0")
						//LocalTitle = ""
						LocalTitle = g_sMission_TeamName[i]
						PRINTLN("[RCC MISSION] DRAW_FMMC_TEAM_LIVES_REMAINING_HUD() - literal")
					ENDIF
				ENDIF
				
				SWITCH iOrder
					CASE 0
						scoreHudOrder = HUDORDER_FOURTHBOTTOM
					BREAK
					CASE 1
						scoreHudOrder = HUDORDER_THIRDBOTTOM
					BREAK
					CASE 2
						scoreHudOrder = HUDORDER_SECONDBOTTOM
					BREAK
					CASE 3
						scoreHudOrder = HUDORDER_BOTTOM
					BREAK
				ENDSWITCH
				iOrder++
				
				teamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(i, PlayerToUse)
				
				IF NOT bCustomString
					hudColour = teamColour
				ENDIF
				
				INT iRule = MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iteam]
				IF IS_BIT_SET(g_iTeamNameBitSetPlayerName, i)
					g_b_ChangePlayerNameToTeamName = FALSE
				ELSE
					g_b_ChangePlayerNameToTeamName = TRUE
				ENDIF
				
				INT iMaxLives = GET_FMMC_MISSION_LIVES_FOR_LOCAL_PLAYER()
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciEND_ROUND_ON_ZERO_LIVES)
					iTeamLivesHUD[i] = iTeamLivesHUD[i] + 1
					iMaxLives = iMaxLives + 1
				ENDIF
				
				IF iRule < FMMC_MAX_RULES
				
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iPartToUse].iteam].iRuleBitsetEleven[iRule], ciBS_RULE11_SHOW_FRACTION_HUD_TEAM_PLAYERS_REMAINING)
						iTeamLivesHUD[i] = GET_REMAINING_PLAYERS_ON_TEAM(i)
						iMaxLives = GET_PLAYERS_ON_TEAM(i)
					ENDIF
					
					IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
					AND NOT IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iPartToUse].iteam].iRuleBitsetSeven[iRule], ciBS_RULE7_SHOW_FRACTION_HUD)
							IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()	
							AND IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN)
							AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iPartToUse].iteam].iRuleBitsetEleven[iRule], ciBS_RULE11_SHOW_FRACTION_HUD_TEAM_PLAYERS_REMAINING)
								DRAW_GENERIC_BIG_DOUBLE_NUMBER(iTeamLivesHUD[i], iTeamLivesHUD[i], LocalTitle, -1, hudColour, scoreHudOrder, bLiteral, DEFAULT, DEFAULT, DEFAULT, DEFAULT, teamColour)// url:bugstar:3043840 Didn't want to change GET_FMMC_MISSION_LIVES_FOR_LOCAL_PLAYER since it's a very old function used elsewhere.
							ELSE
								IF iTeamLivesHUD[i] >= 0
									DRAW_GENERIC_BIG_DOUBLE_NUMBER(iTeamLivesHUD[i], iMaxLives, LocalTitle, -1, hudColour, scoreHudOrder, bLiteral, DEFAULT, DEFAULT, DEFAULT, DEFAULT, teamColour)
								ELSE
									DRAW_GENERIC_BIG_DOUBLE_NUMBER(0, iMaxLives, LocalTitle, -1, hudColour, scoreHudOrder, bLiteral, DEFAULT, DEFAULT, DEFAULT, DEFAULT, teamColour)
								ENDIF
							ENDIF
						ELSE
							IF iTeamLivesHUD[i] <= 8
								FOR iLoop = 0 TO 7
									IF iLoop < iTeamLivesHUD[i]
										PackageColour[iLoop] = teamColour
									ELSE
										PackageColour[iLoop] = HUD_COLOUR_BLACK
									ENDIF
								ENDFOR
								
								DRAW_ONE_PACKAGES_EIGHT_HUD(iTeamLivesHUD[i], LocalTitle, bLiteral, 
												PackageColour[0], PackageColour[1], PackageColour[2], PackageColour[3], PackageColour[4], PackageColour[5], PackageColour[6], PackageColour[7],
												-1, hudColour,
												FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, // If each circle should have a cross through it (all FALSE)
												scoreHudOrder, TRUE)
							ELSE
								IF iTeamLivesHUD[i] >= 0
									DRAW_GENERIC_BIG_NUMBER(iTeamLivesHUD[i], LocalTitle, -1, hudColour, scoreHudOrder, bLiteral, DEFAULT, teamColour)
								ELSE
									DRAW_GENERIC_BIG_NUMBER(0, LocalTitle, -1, hudColour, scoreHudOrder, bLiteral, DEFAULT, teamColour)
								ENDIF
							ENDIF
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC DRAW_TEAM_SCOREBOARD_HUD(INT iTeam, BOOL bUseColour)

	TEXT_LABEL_63 LocalTitle
	HUD_COLOURS hudColour, titleColour
	INT iFlashTime = -1
	INT iAlpha = 255
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset2, ciBS2_USE_BLUE_SCOREBOARD)
		hudColour = HUD_COLOUR_WHITE
	ELSE
		hudColour = HUD_COLOUR_BLUE
	ENDIF
	HUDORDER eHUDOrder = INT_TO_ENUM(HUDORDER, ENUM_TO_INT(HUDORDER_FOURTHBOTTOM) - iTeam)
	BOOL bLiteral = TRUE
	
	INT iScore = GET_TEAM_SCORE_FOR_DISPLAY(iTeam) - g_FMMC_STRUCT.iInitialPoints[ iTeam ]
	INT iTarget = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore
	
	// Don't delete this. If it's annoying you just slap it in a commandline.
	PRINTLN("[url:bugstar:4612880] [DRAW_TEAM_SCOREBOARD_HUD] - Printing out iTeam: ", iTeam, "  ##########################")
	PRINTLN("[url:bugstar:4612880] [DRAW_TEAM_SCOREBOARD_HUD] - iRule: ", MC_serverBD_4.iCurrentHighestPriority[iTeam])
	PRINTLN("[url:bugstar:4612880] [DRAW_TEAM_SCOREBOARD_HUD] - iTeamScore: ", MC_serverBD.iTeamScore[iTeam])
	PRINTLN("[url:bugstar:4612880] [DRAW_TEAM_SCOREBOARD_HUD] - iPointsGivenToPass: ", MC_ServerBD.iPointsGivenToPass[iTeam])
	PRINTLN("[url:bugstar:4612880] [DRAW_TEAM_SCOREBOARD_HUD] - iInitialPoints: ", g_FMMC_STRUCT.iInitialPoints[iTeam])
	PRINTLN("[url:bugstar:4612880] [DRAW_TEAM_SCOREBOARD_HUD] - iScore: ", iScore)
	PRINTLN("[url:bugstar:4612880] [DRAW_TEAM_SCOREBOARD_HUD] - iTarget: ", iTarget)
	PRINTLN("[url:bugstar:4612880] [DRAW_TEAM_SCOREBOARD_HUD] - ############################################################")
	
	g_b_ChangePlayerNameToTeamName = TRUE
	
	IF iTarget > 9
		SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(g_sMission_TeamName[iTeam])
	AND IS_BIT_SET(iLocalBoolCheck13, LBOOL13_FIRST_STAGGERED_LOOP_DONE_CLIENTSIDE)
		PRINTLN("[KH] DRAW_TEAM_SCOREBOARD_HUD - Filling team names for scoreboard iTeam: ", iTeam)
		IF fillTeamNames != NULL
			CALL fillTeamNames(iTeam, PlayerToUse ) //was localPlayer	
		ENDIF
		//FMMC2020 this shouldnt need done here...
		EXIT
	ENDIF
	
	IF SHOULD_USE_CUSTOM_STRING_FOR_HUD(iteam, MC_serverBD_4.iCurrentHighestPriority[iteam])
		LocalTitle = TEXT_LABEL_TO_STRING(g_FMMC_STRUCT.tl23CustomTargetString[iteam])
	ELSE
		IF IS_BIT_SET(iCelebrationTeamNameIsNotLiteral, iTeam)
			LocalTitle = TEXT_LABEL_TO_STRING(g_sMission_TeamName[iteam])
			IF NOT IS_BIT_SET(g_iTeamNameBitSetPlayerName, iTeam)
				bLiteral = FALSE
			ENDIF
		ELSE
			LocalTitle = g_sMission_TeamName[iteam]
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_iTeamNameBitSetPlayerName, iTeam)
		g_b_ChangePlayerNameToTeamName = FALSE
	ENDIF
		
	IF bUseColour
		hudColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iteam, PlayerToUse)
		titleColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iteam, PlayerToUse)
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
	AND NOT IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSET_LINES_AS_RUGBY_LINES)
			PRINTLN("[AW][DRAW_TEAM_SCOREBOARD_HUD]g_b_UseBottomRightTitleColour:  ",g_b_UseBottomRightTitleColour )
			g_b_UseBottomRightTitleColour = TRUE
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(iScore, iTarget, LocalTitle, -1,hudColour, eHUDOrder, bLiteral,HUDFLASHING_NONE,0,FALSE,FALSE,HUD_COLOUR_WHITE)
		ELSE
			
			IF NOT ARE_STRINGS_EQUAL(LocalTitle, "**Invalid**")
				DRAW_GENERIC_BIG_DOUBLE_NUMBER(iScore, iTarget, LocalTitle, iFlashTime,hudColour, eHUDOrder, bLiteral,HUDFLASHING_NONE,0,FALSE,FALSE,titleColour,DEFAULT,DEFAULT,iAlpha)
			ENDIF

		ENDIF
	ENDIF

ENDPROC

PROC DRAW_INDIVIDUAL_PLAYER_SCORE()
	IF NOT IS_SPECTATOR_HUD_HIDDEN()
	AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE_HIDE_HUD)
	AND NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
	AND NOT IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
		INT i
		INT iRow
		PRINTLN("[PLAYER_LOOP] - DRAW_INDIVIDUAL_PLAYER_SCORE")
		FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
			IF g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant = iPartToUse
				iRow = i
			ENDIF
		ENDFOR

		INT iScore = g_MissionControllerserverBD_LB.sleaderboard[iRow].iPlayerScore
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetSeven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]],ciBS_RULE7_INDIVIDUAL_SCORES_WHITE_LOWER_CASE)
			DRAW_GENERIC_SCORE(iScore, "MYSCOR", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE)
		ELSE		
			DRAW_GENERIC_SCORE(iScore, "FMMC_INDSC", -1, HUD_COLOUR_YELLOW, HUDORDER_DONTCARE)
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_INDIVIDUAL_PLAYER_KILLS()
	IF NOT IS_SPECTATOR_HUD_HIDDEN()
	AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE_HIDE_HUD)
	AND NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
	AND NOT IS_LOCAL_PLAYER_SPECTATOR()
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetSeven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]],ciBS_RULE7_INDIVIDUAL_SCORES_LOWER_CASE)		
			DRAW_GENERIC_SCORE(MC_playerBD[iPartToUse].iPureKills, "MYKILL")
		ELSE
			DRAW_GENERIC_SCORE(MC_playerBD[iPartToUse].iPureKills, "FMMC_INDKC", -1, HUD_COLOUR_YELLOW)
		ENDIF
	ENDIF		
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Shards ----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to Shards  -------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC RESET_PLAY_CUSTOM_SHARD_REQUEST_INDEX(INT i)
	PLAY_SHARD_DATA sBlank
	sPlayShardData[i] = sBlank
ENDPROC

FUNC BOOL IS_PLAY_CUSTOM_SHARD_REQUESTED(INT i)
	RETURN sPlayShardData[i].eShardType != PLAY_SHARD_TYPE_INVALID	 
ENDFUNC

FUNC BOOL IS_PLAY_CUSTOM_SHARD_DATA_ARRAY_FREE(INT i)
	RETURN sPlayShardData[i].eShardType = PLAY_SHARD_TYPE_INVALID
ENDFUNC

FUNC INT GET_FREE_PLAY_CUSTOM_SHARD_DATA_ARRAY()
	INT i = 0
	FOR i = 0 TO ciMAX_PLAY_SHARD_REQUESTS-1
		IF IS_PLAY_CUSTOM_SHARD_DATA_ARRAY_FREE(i)
			RETURN i
		ENDIF
	ENDFOR
	
	ASSERTLN("[LM][GET_FREE_INCREMENT_SCORE_DATA_ARRAY] - Request Failed. No free score increment request index. Likely something is spammning requests.")
	
	RETURN -1
ENDFUNC

PROC REQUEST_PLAY_CUSTOM_SHARD(PLAY_SHARD_TYPE ePlayShardType, INT iVictimTeam, INT iVictim, INT iKiller)
	
	PRINTLN("[LM][SETUP_PLAY_CUSTOM_WASTED_SHARD] - ePlayShardType: ", (ePlayShardType), " iVictimTeam: ", iVictimTeam, " iVictim: ", iVictim, " iKiller: ", iKiller)
	
	DEBUG_PRINTCALLSTACK()
	
	INT i = GET_FREE_PLAY_CUSTOM_SHARD_DATA_ARRAY()
	
	IF i != -1	
		sPlayShardData[i].eShardType = ePlayShardType
		sPlayShardData[i].iVictimTeam = iVictimTeam
		sPlayShardData[i].iVictim = iVictim
		sPlayShardData[i].iKiller = iKiller
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: SUDDEN DEATH ----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to Sudden Death -------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC DISPLAY_SUDDEN_DEATH_SHARD(HUD_COLOURS sdColour = HUD_COLOUR_WHITE)
	UNUSED_PARAMETER(sdColour)
	IF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_DISPLAY_SUDDEN_DEATH)
		EXIT
	ENDIF

	PRINTLN("[SDShard] DISPLAY_SUDDEN_DEATH_SHARD - Has been called!")
	DEBUG_PRINTCALLSTACK()
	
	SET_BIT(iLocalBoolCheck3, LBOOL3_DISPLAY_SUDDEN_DEATH)
	STRING tlStrapline = NULL
	
	IF IS_BIT_SET(MC_serverBD.iServerBitset2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES)
		tlStrapline = "SUDDTH_KILL"
	ELIF IS_BIT_SET(MC_serverBD.iServerBitset2, SBBOOL2_SUDDEN_DEATH_MOREINLOC)
		tlStrapline = "SUDDTH_LOC"
	ELIF IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN)
		tlStrapline = "SUDDTH_SSDS_C"
	ELIF IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES)
		tlStrapline = "SUDDTH_BMV"
	ELIF IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_SUDDEN_DEATH_TARGET)
		tlStrapline = "SUDDTH_RGT"
	ELIF IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_SUDDEN_DEATH_IN_AND_OUT)	
		tlStrapline = "SUDDTH_IAO"
	ELIF IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_SUDDEN_DEATH_POWER_PLAY)
		tlStrapline = "SUDDTH_PP"
	ELIF IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)	
		tlStrapline = "SUDDTH_KILL"
	ELIF IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_JUGGERNAUT)
		tlStrapline = "SUDDTH_TLAD"
	ENDIF
	
	PRINTLN("[KH] PROCESS_OBJECTIVE_LIMITS - SUDDEN DEATH SHARD STRAPLINE: ", tlStrapline) 	
									
	IF NOT HAS_TEAM_FAILED(MC_playerBD[iPartToUse].iteam)
	AND NOT IS_PLAYER_ON_PAUSE_MENU(LocalPlayer)
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "CTF_SD", tlStrapline)
	ELSE
		PRINTLN("[KH] PROCESS_OBJECTIVE_LIMITS - TEAM HAS FAILED, NOT SHOWING SUDDEN DEATH STRAPLINE")
	ENDIF
ENDPROC

PROC PROCESS_TIME_REMAINING_CACHE()
	INT iTimeMs = -1
	
	IF NOT g_bMissionClientGameStateRunning
		IF (GET_FRAME_COUNT() % 20) = 0
			PRINTLN("[LM][PROCESS_TIME_REMAINING_CACHE] g_bMissionClientGameStateRunning: ", g_bMissionClientGameStateRunning)
		ENDIF
		iTimeMs = -1
	ENDIF
	
	IF g_bCelebrationScreenIsActive
	OR g_bMissionEnding
		IF (GET_FRAME_COUNT() % 20) = 0
			PRINTLN("[LM][PROCESS_TIME_REMAINING_CACHE] g_bMissionEnding: ", g_bMissionEnding, " g_bCelebrationScreenIsActive: ", g_bCelebrationScreenIsActive)
		ENDIF
		iTimeMs = 0
	ENDIF
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	IF iTeam < FMMC_MAX_TEAMS
	AND iTeam > -1
		iTeam = 0
	ENDIF
	
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	IF iRule < FMMC_MAX_RULES
		iTimeMs = GET_OBJECTIVE_TIMER_TIME_REMAINING(iTeam, iRule) 
		
		IF iTimeMs = -1
			iTimeMs = GET_CURRENT_MULTIRULE_TIMER_TIME_REMAINING(iTeam)
			IF (GET_FRAME_COUNT() % 20) = 0
				PRINTLN("[LM][PROCESS_TIME_REMAINING_CACHE] Using Multirule Timer")
			ENDIF
		ELSE
			IF (GET_FRAME_COUNT() % 20) = 0
				PRINTLN("[LM][PROCESS_TIME_REMAINING_CACHE] Using Objective Timer")
			ENDIF
		ENDIF
	ENDIF	
	
	SET_TIME_LEFT_FOR_INSTANCED_CONTENT_IN_MILISECONDS(iTimeMs)
ENDPROC

// PURPOSE:
//    Adjust the size of the area from fStartSize to fEndSize over fTime ms
//FUNC BOOL ADJUST_BLIP_RADIUS_OVER_TIME(FLOAT fStartSize, FLOAT fEndSize, INT fTime)
FUNC BOOL ADJUST_BLIP_RADIUS_OVER_TIME()
	FLOAT fCurrentProgress = 1.0 - ((fCurrentSphereRadius - fSphereEndRadius) / (fSphereStartRadius - fSphereEndRadius))
	FLOAT fDiff = (ABSF(fSphereStartRadius - fSphereEndRadius) * 1000)
	IF fCurrentProgress < 0.33
		fDiff = (ABSF(fSphereStartRadius - fSphereEndRadius) * 1000) + ((ABSF(fSphereStartRadius - fSphereEndRadius) * 1000) / 2)
	ELIF fCurrentProgress < 0.66
		fDiff = (ABSF(fSphereStartRadius - fSphereEndRadius) * 1000)
	ELSE
		fDiff = (ABSF(fSphereStartRadius - fSphereEndRadius) * 1000) - ((ABSF(fSphereStartRadius - fSphereEndRadius) * 1000) / 2)
	ENDIF
	
	FLOAT fDiffThisFrame = (fDiff / fSphereShrinkTime) * GET_FRAME_TIME()
	IF fDiffThisFrame < 0.0025
		fDiffThisFrame = 0.0025
	ENDIF
	
	IF fCurrentSphereRadius > fSphereEndRadius
		fCurrentSphereRadius -= fDiffThisFrame
		IF fCurrentSphereRadius < fSphereEndRadius
			fCurrentSphereRadius = fSphereEndRadius
		ENDIF
	ELSE
		fCurrentSphereRadius = fSphereEndRadius
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: LEADERBOARDS --------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to xxxxxxxxxxxxxxxx ---------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// Stop processing the lbd to prevent bugs like 2706396
PROC SET_STOP_PROCESSING_LBD_BIT()
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_LBD_STOP)
		IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_LBD_DISPLAYED)	
			IF IS_PLAYER_ON_A_PLAYLIST(LocalPlayer)
				IF NOT HAS_NET_TIMER_STARTED(stLbdStop)
					PRINTLN("[RCC MISSION] [PLY] SET_STOP_PROCESSING_LBD_BIT, START_NET_TIMER ")
					START_NET_TIMER(stLbdStop)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(stLbdStop, 3000)
						PRINTLN("[RCC MISSION] [PLY] SET_STOP_PROCESSING_LBD_BIT, SBBOOL3_LBD_STOP ")
						SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_LBD_STOP)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC LBD_SUB_MODE GET_LEADERBOARD_SUB_MODE()

	IF g_FMMC_STRUCT.iLeaderboardStyle = ciMISSION_LEADERBOARD_STYLE_HEIST
		RETURN SUB_MISSIONS_HEIST
	ENDIF
	
	IF g_FinalRoundsLbd
		RETURN SUB_FINAL_ROUNDS
	ENDIF

	IF g_FMMC_STRUCT.iLeaderboardStyle = ciMISSION_LEADERBOARD_STYLE_LTS
		RETURN SUB_MISSIONS_LTS
	ENDIF
	
	IF g_FMMC_STRUCT.iLeaderboardStyle = ciMISSION_LEADERBOARD_STYLE_VERSUS
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciSHOW_SCORE_COLUMN_ON_LEADERBOARD)
			RETURN SUB_MISSIONS_VERSUS
		ELSE
			IF IS_BIT_SET(MC_serverBD.iOptionsMenuBitSet,ciNO_POINTS_HUD)
				RETURN SUB_MISSIONS_NO_HUD
			ELSE
				RETURN SUB_MISSIONS_CTF
			ENDIF
		ENDIF
	ENDIF

	IF g_FMMC_STRUCT.iMissionEndType != ciMISSION_SCORING_TYPE_TIME
		IF NOT IS_BIT_SET(MC_serverBD.iOptionsMenuBitSet,ciNO_POINTS_HUD)
			RETURN SUB_MISSIONS_NORMAL
		ELSE
			RETURN SUB_MISSIONS_NO_HUD
		ENDIF
	ELSE
		RETURN SUB_MISSIONS_TIMED
	ENDIF
	
	RETURN SUB_MISSIONS_NORMAL	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: DPAD_DOWN / MINI LEADERBOARD ------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to xxxxxxxxxxxxxxxx ---------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// Return player index of player in position (0 for leader and so on)
FUNC PLAYER_INDEX GET_PLAYER_IN_LEADERBOARD_POS(INT iPos)

	PLAYER_INDEX PlayerToGet = INVALID_PLAYER_INDEX()
	
	IF g_MissionControllerserverBD_LB.sleaderboard[iPos].iParticipant <> -1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(g_MissionControllerserverBD_LB.sleaderboard[iPos].iParticipant))
			PlayerToGet = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(g_MissionControllerserverBD_LB.sleaderboard[iPos].iParticipant))
		ENDIF
	ENDIF
	
	
	IF PlayerToGet != INVALID_PLAYER_INDEX()
		PRINTLN("[GET_PLAYER_IN_LEADERBOARD_POS][RH] PlayerToGet is invalid")
	ELSE
		PRINTLN("[GET_PLAYER_IN_LEADERBOARD_POS][RH] PlayerToGet is not invalid")
	ENDIF
	
	RETURN PlayerToGet
	
ENDFUNC

FUNC BOOL SHOULD_PLAYER_BE_DISPLAYED_ON_MISSION_DPAD_DOWN_MENU(INT leaderboardIndex, MC_ServerBroadcastData_LB &serverBDpassed)	//	, MC_PlayerBroadcastData &playerBDPassed[])

	PLAYER_INDEX PlayerId = serverBDpassed.sleaderBoard[leaderboardIndex].playerID
	INT participantID = serverBDpassed.sleaderBoard[leaderboardIndex].iParticipant

#IF IS_DEBUG_BUILD
		INT iPlayer = NATIVE_TO_INT(PlayerId)
		
		IF participantID <> -1
			PRINTLN("[RCC MISSION] [CS_DPAD] SHOULD_PLAYER_BE_DISPLAYED_ON_MISSION_DPAD_DOWN_MENU leaderboard index = ", leaderboardIndex, " participantID = ", participantID, "PlayerId = ", iPlayer, " PlayerName = ", MC_serverBD_2.tParticipantNames[participantID])
		ELSE
			PRINTLN("[RCC MISSION] [CS_DPAD] SHOULD_PLAYER_BE_DISPLAYED_ON_MISSION_DPAD_DOWN_MENU leaderboard index = ", leaderboardIndex, " participantID = ", participantID, "PlayerId = ", iPlayer)
		ENDIF
#ENDIF	//	IS_DEBUG_BUILD


	IF PlayerId != INVALID_PLAYER_INDEX()
		IF participantID <> -1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(participantID)) 
				IF NOT IS_BIT_SET(MC_Playerbd[participantID].iClientBitSet,PBBOOL_TERMINATED_SCRIPT)
					IF NOT IS_PLAYER_SPECTATOR_ONLY(playerId)
						IF IS_NET_PLAYER_OK(playerId, FALSE) 	

							PRINTLN("[RCC MISSION] [CS_DPAD] SHOULD_PLAYER_BE_DISPLAYED_ON_MISSION_DPAD_DOWN_MENU returning TRUE for leaderboard index = ", leaderboardIndex)
					
							RETURN TRUE
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] [CS_DPAD] SHOULD_PLAYER_BE_DISPLAYED_ON_MISSION_DPAD_DOWN_MENU returning FALSE for leaderboard index = ", leaderboardIndex, " PBBOOL_TERMINATED_SCRIPT bit is set")
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] [CS_DPAD] SHOULD_PLAYER_BE_DISPLAYED_ON_MISSION_DPAD_DOWN_MENU returning FALSE for leaderboard index = ", leaderboardIndex, " NETWORK_IS_PARTICIPANT_ACTIVE returned FALSE")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] [CS_DPAD] SHOULD_PLAYER_BE_DISPLAYED_ON_MISSION_DPAD_DOWN_MENU returning FALSE for leaderboard index = ", leaderboardIndex, " iParticipant = -1")
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION] [CS_DPAD] SHOULD_PLAYER_BE_DISPLAYED_ON_MISSION_DPAD_DOWN_MENU returning FALSE for leaderboard index = ", leaderboardIndex, " playerID = INVALID_PLAYER_INDEX")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RESET_DPAD()
	INT i
	PRINTLN("[PLAYER_LOOP] - RESET_DPAD")
	REPEAT NUM_NETWORK_PLAYERS i
		PRINTLN("[DPAD] Clearing slot ", i)
		SCALEFORM_SET_DATA_SLOT_EMPTY(scaleformDpadMovie, 0, i)
	ENDREPEAT
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: End Messages --------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to xxxxxxxxxxxxxxxx ---------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_LOCAL_OBJECTIVE_END_MESSAGE(INT iteam,INT ipriority,INT ireasonforend, BOOL bFailMission = FALSE)
	
	PRINTLN("[RCC MISSION] SET_LOCAL_OBJECTIVE_END_MESSAGE - team ", iteam, ", priority ",ipriority, ", reason ",ireasonforend, ", failmission ",bFailMission)

	IF bFailMission
	AND (ireasonObjEnd[iTeam] = -1)
		iObjEndPriority[iteam] = ipriority
		ireasonObjEnd[iTeam] = ireasonforend
		PRINTLN("[RCC MISSION] SET_LOCAL_OBJECTIVE_END_MESSAGE - setting fail reason for team ",iteam)
	ENDIF
									
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Help Text --------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to xxxxxxxxxxxxxxxx ---------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SKIP_SKIPPABLE_HELP_TEXT()
	IF NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_SKIP_HELP_TEXT)
		EXIT
	ENDIF
	
	IF IS_STRING_EMPTY(sTLToSkip)
		EXIT
	ENDIF
	
	IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sTLToSkip)
		EXIT		
	ENDIF
	
	PRINTLN("[JT HELP] HELP TEXT BEING CLEARED")
	CLEAR_HELP(TRUE)
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Vehicle HUD --------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to xxxxxxxxxxxxxxxx ---------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC GENERATE_AND_DISPLAY_TEAM_VEH_DATA(INT iThisTeam = 1)

	INT iPartLoop
	PLAYER_INDEX playerThisPlayer
	PED_INDEX pedThisPed
	PARTICIPANT_INDEX partThisParticipant
	VEHICLE_INDEX vehThisVeh
	TEXT_LABEL_23 tl23_Temp
	BOOL isLiteralString=FALSE
	
	PRINTLN( "[AW] GENERATE_AND_DISPLAY_TEAM_VEH_DATA - 0" )
	
	PRINTLN("[PLAYER_LOOP] - GENERATE_AND_DISPLAY_TEAM_VEH_DATA")
	FOR iPartLoop = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
		partThisParticipant = INT_TO_PARTICIPANTINDEX(iPartLoop)
		IF( NETWORK_IS_PARTICIPANT_ACTIVE( partThisParticipant ) )
			PRINTLN( "[AW] GENERATE_AND_DISPLAY_TEAM_VEH_DATA - 1" )
			playerThisPlayer = INT_TO_PLAYERINDEX(iPartLoop)
			pedThisPed = GET_PLAYER_PED(playerThisPlayer)
			IF NOT IS_PED_INJURED(pedThisPed)
				PRINTLN( "[AW] GENERATE_AND_DISPLAY_TEAM_VEH_DATA - 2" )
				IF MC_playerBD[iPartLoop].iteam = iThisTeam
					PRINTLN( "[AW] GENERATE_AND_DISPLAY_TEAM_VEH_DATA - 3" )
					IF IS_PED_IN_ANY_VEHICLE(pedThisPed)
						PRINTLN( "[AW] GENERATE_AND_DISPLAY_TEAM_VEH_DATA - 4" )
						vehThisVeh = GET_VEHICLE_PED_IS_IN(pedThisPed)
						IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[ iThisTeam  ].tl23ObjBlip[MC_ServerBD_4.iCurrentHighestPriority[ iThisTeam  ]])
							IF IS_PED_A_PLAYER(pedThisPed)
								PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedThisPed)
								tl63_VehicleHealthString[iPartLoop] = GET_PLAYER_NAME(tempplayer)
							ENDIF
							tl23_Temp = g_FMMC_STRUCT.sFMMCEndConditions[ iThisTeam].tl23ObjBlip[MC_ServerBD_4.iCurrentHighestPriority[ iThisTeam ]]
							tl23_Temp = CONVERT_STRING_TO_UPPERCASE(tl23_Temp)
							tl63_VehicleHealthString[iPartLoop] += tl23_Temp
							isLiteralString=TRUE
						ELSE
							tl63_VehicleHealthString[iPartLoop] = "VEH_HEALTH"
						ENDIF
						
						PRINTLN( "[AW] GENERATE_AND_DISPLAY_TEAM_VEH_DATA - 5" )
						
						IF IS_VEHICLE_DRIVEABLE(vehThisVeh)
							iTeamVehCurrentHealth[iPartLoop] = GET_ENTITY_HEALTH(vehThisVeh)
						ELSE
							iTeamVehCurrentHealth[iPartLoop] = 0
						ENDIF
						DRAW_GENERIC_METER(iTeamVehCurrentHealth[iPartLoop], GET_FMMC_PLAYER_VEHICLE_HEALTH_FOR_TEAM(iThisTeam), tl63_VehicleHealthString[iPartLoop], HUD_COLOUR_WHITE, -1, HUDORDER_DONTCARE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, isLiteralString)
					ENDIF
				ENDIF
			ENDIF
		ENDIF

	ENDFOR
ENDPROC

//Team with tanks is team 1 for use in David and Goliath
PROC DISPLAY_TEAM_VEHICLE_HUD(INT iTeamToShow = 1)	
	IF MC_ServerBD_4.iCurrentHighestPriority[ iTeamToShow ] < FMMC_MAX_RULES
	AND NOT IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
		PRINTLN( "[AW] DISPLAY_TEAM_VEHICLE_HUD - 0" )
		GENERATE_AND_DISPLAY_TEAM_VEH_DATA(iTeamToShow)		
	ENDIF
ENDPROC

//[FMMC2020] Rename
FUNC STRING GET_CREATOR_NAME_STRING_FOR_TEAM(INT iteam, BOOL bSingular = FALSE, BOOL bGenericName = FALSE)
	STRING steamname
	TEXT_LABEL_63 tl63TeamName 

	IF NOT bGenericName
		IF bSingular
			tl63TeamName = "FMMC_TEAM_S_"
		ELSE 
			tl63TeamName = "FMMC_TEAM_"
		ENDIF
		
		tl63TeamName += g_FMMC_STRUCT.iTeamNames[iTeam]
		
		IF g_FMMC_STRUCT.iTeamNames[iTeam] = ciTEAM_NAME_MAX
			TEXT_LABEL_63 originalString
			TEXT_LABEL_15 tl15 = "FMMC_MN"			
			IF DOES_TEXT_LABEL_EXIST(g_FMMC_STRUCT.tlCustomName[iTeam])
				originalString = GET_FILENAME_FOR_AUDIO_CONVERSATION(g_FMMC_STRUCT.tlCustomName[iTeam])
			ELSE
				originalString = g_FMMC_STRUCT.tlCustomName[iTeam]
			ENDIF
			RETURN GET_CROPPED_LITERAL_STRING_FOR_MENU(tl15, originalString)
		ENDIF
	ELSE
		SWITCH iTeam
			CASE 0
				tl63TeamName = "MC_GT_NAME_1"
			BREAK
			CASE 1
				tl63TeamName = "MC_GT_NAME_2"
			BREAK
			CASE 2
				tl63TeamName = "MC_GT_NAME_3"
			BREAK
			CASE 3
				tl63TeamName = "MC_GT_NAME_4"
			BREAK
		ENDSWITCH
	ENDIF
	
	steamname = GET_FILENAME_FOR_AUDIO_CONVERSATION(tl63TeamName)
	
	RETURN steamname
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Ped HUD --------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to xxxxxxxxxxxxxxxx ---------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_AGGRO_HELP_TEXT_DATA(INT iSpookReason, INT iPartFail, INT iPed)
	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	INT iTeam = MC_playerBD[iLocalPart].iTeam
	INT iCurrentRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]
	PRINTLN("SET_AGGRO_HELP_TEXT_DATA Current rule = ", iCurrentRule, " || iSpookReason = ", iSpookReason, " iPartFail = ", iPartFail)
	
	IF iCurrentRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iCurrentRule], ciBS_RULE13_RULE_SHOW_PLAYER_SPOTTED_HELPTEXT)
		AND NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_AGGRO_HELPTEXT_PLAYED)
			IF MC_serverBD_2.iAggroText_PartCausingSpook = -1
			AND MC_serverBD_2.iAggroText_PedSpooked = -1
			AND MC_serverBD_2.iAggroText_SpookReason = -1
				MC_serverBD_2.iAggroText_PartCausingSpook = iPartFail
				MC_serverBD_2.iAggroText_PedSpooked = iPed
				MC_serverBD_2.iAggroText_SpookReason = iSpookReason
				PRINTLN("SET_AGGRO_HELP_TEXT_DATA - MC_serverBD_2.iAggroText_PartCausingSpook: ", MC_serverBD_2.iAggroText_PartCausingSpook)
				PRINTLN("SET_AGGRO_HELP_TEXT_DATA - MC_serverBD_2.iAggroText_PedSpooked: ", MC_serverBD_2.iAggroText_PedSpooked)
				PRINTLN("SET_AGGRO_HELP_TEXT_DATA - MC_serverBD_2.iAggroText_SpookReason: ", MC_serverBD_2.iAggroText_SpookReason)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_AGGRO_HELP_TEXT_DATA()
	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	MC_serverBD_2.iAggroText_PartCausingSpook = -1
	MC_serverBD_2.iAggroText_PedSpooked = -1
	MC_serverBD_2.iAggroText_SpookReason = -1
	PRINTLN("SET_AGGRO_HELP_TEXT_DATA - Clearing ped aggro text data")
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Player HUD --------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to xxxxxxxxxxxxxxxx ---------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC REFRESH_PLAYER_BLIPS()
	PRINTLN("[RCC MISSION] REFRESH_PLAYER_BLIPS - Setting LBOOL7_REFRESH_PLAYER_BLIPS")
	SET_BIT(iLocalBoolCheck7, LBOOL7_REFRESH_PLAYER_BLIPS)
ENDPROC

PROC PLAYER_STATE_LIST_INIT(PLAYER_STATE_LIST_DATA &playerStateListData, BOOL bSortLocalPlayerTeamTop = TRUE, INT iNumberOfPlayersToDisplayPerTeam = 4, INT iNumberOfTeamsToCheck = FMMC_MAX_TEAMS, INT iNumberOfPlayersToCheck = MAX_NUM_MC_PLAYERS)
	//Initialise Variables
	INT i
	
	PRINTLN("[PLAYER_LOOP] - PLAYER_STATE_LIST_INIT")
	FOR i = 0 TO MAX_NUM_MC_PLAYERS - 1
		playerStateListData.uiSlot[i].sName = NULL_STRING()
		playerStateListData.uiSlot[i].iValue = -3	//None
		
		playerStateListData.piPlayerStateList[i] = INVALID_PLAYER_INDEX()
		playerStateListData.iPlayerStateList[i] = -3	//None
	ENDFOR
	
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		playerStateListData.iCurrentPlayerCheck[i] = 0
		
		playerStateListData.hcTeam[i] = HUD_COLOUR_PURE_WHITE
	ENDFOR
	
	playerStateListData.bSortLocalPlayerTeamTop = bSortLocalPlayerTeamTop
	playerStateListData.iNumberOfPlayersToDisplayPerTeam = iNumberOfPlayersToDisplayPerTeam
	playerStateListData.iNumberOfTeamsToCheck = iNumberOfTeamsToCheck
	playerStateListData.iNumberOfPlayersToCheck = iNumberOfPlayersToCheck
ENDPROC

PROC PLAYER_STATE_LIST_ADD_PLAYER(PLAYER_STATE_LIST_DATA &playerStateListData, INT iParticipant, INT iValue)
	PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(iParticipant)
	
	IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
		PLAYER_INDEX playerIndex = NETWORK_GET_PLAYER_INDEX(participantIndex)
		
		IF MC_playerBD[iParticipant].iTeam != -1 AND MC_playerBD[iParticipant].iTeam < FMMC_MAX_TEAMS	//Valid Team
			IF playerStateListData.iCurrentPlayerCheck[MC_playerBD[iParticipant].iTeam] < playerStateListData.iNumberOfPlayersToDisplayPerTeam	//
				INT iTeamCheckOffset = MC_playerBD[iParticipant].iTeam * playerStateListData.iNumberOfPlayersToDisplayPerTeam
				
				playerStateListData.piPlayerStateList[playerStateListData.iCurrentPlayerCheck[MC_playerBD[iParticipant].iTeam] + iTeamCheckOffset] = playerIndex
				playerStateListData.iPlayerStateList[playerStateListData.iCurrentPlayerCheck[MC_playerBD[iParticipant].iTeam] + iTeamCheckOffset] = iValue
				
				playerStateListData.iCurrentPlayerCheck[MC_playerBD[iParticipant].iTeam]++
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_PLAYER_CARRY_COUNT()

INT icarrycount

	IF MC_playerBD[iPartToUse].iPedCarryCount > 0
		icarrycount = MC_playerBD[iPartToUse].iPedCarryCount
	ENDIF
	IF MC_playerBD[iPartToUse].iObjCarryCount > 0
		icarrycount = MC_playerBD[iPartToUse].iObjCarryCount
	ENDIF
	IF MC_playerBD[iPartToUse].iVehCarryCount > 0
		icarrycount = MC_playerBD[iPartToUse].iVehCarryCount
	ENDIF

	RETURN icarrycount
	
ENDFUNC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Team HUD --------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to xxxxxxxxxxxxxxxx ---------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//Gets the INT of a players crew
FUNC TEXT_LABEL_63 GET_PLAYER_CREW_NAME_RC(PLAYER_INDEX aPlayer)
	GAMER_HANDLE GamerHandle = GET_GAMER_HANDLE_PLAYER(aPlayer)
	NETWORK_CLAN_DESC retDesc
	NETWORK_CLAN_PLAYER_GET_DESC(retDesc, 0, GamerHandle)
	RETURN retDesc.ClanName
ENDFUNC

FUNC BOOL HAS_CREW_NAME_ALREADY_BEEN_USED(TEXT_LABEL_63 screwname)

INT iteam

	FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		IF NOT IS_STRING_NULL_OR_EMPTY(g_sMission_TeamName[iteam])
			IF ARE_STRINGS_EQUAL(screwname,g_sMission_TeamName[iteam])
				PRINTLN("[RCC MISSION] crew name already in use: ",screwname) 
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR

RETURN FALSE

ENDFUNC

FUNC BOOL HAS_TEAM_GOT_CREATOR_NAME(INT iteam)

    IF g_FMMC_STRUCT.iTeamNames[iTeam] != 0
         RETURN TRUE
    ENDIF
                
    RETURN FALSE

ENDFUNC

FUNC STRING GET_CREATOR_NAME_STRING(INT iteam, BOOL bSingular = FALSE, BOOL bGenericName = FALSE)

STRING steamname
TEXT_LABEL_63 tl63TeamName 

	IF NOT bGenericName
		IF bSingular
			tl63TeamName = "FMMC_TEAM_S_"
		ELSE 
			tl63TeamName = "FMMC_TEAM_"
		ENDIF
		
		tl63TeamName += g_FMMC_STRUCT.iTeamNames[iTeam]
		
		IF g_FMMC_STRUCT.iTeamNames[iTeam] = ciTEAM_NAME_MAX
			TEXT_LABEL_63 originalString
			TEXT_LABEL_15 tl15 = "FMMC_MN"			
			IF DOES_TEXT_LABEL_EXIST(g_FMMC_STRUCT.tlCustomName[iTeam])
				originalString = GET_FILENAME_FOR_AUDIO_CONVERSATION(g_FMMC_STRUCT.tlCustomName[iTeam])
			ELSE
				originalString = g_FMMC_STRUCT.tlCustomName[iTeam]
			ENDIF
			RETURN GET_CROPPED_LITERAL_STRING_FOR_MENU(tl15, originalString)
		ENDIF
	ELSE
		SWITCH iTeam
			CASE 0
				tl63TeamName = "MC_GT_NAME_1"
			BREAK
			CASE 1
				tl63TeamName = "MC_GT_NAME_2"
			BREAK
			CASE 2
				tl63TeamName = "MC_GT_NAME_3"
			BREAK
			CASE 3
				tl63TeamName = "MC_GT_NAME_4"
			BREAK
		ENDSWITCH
	ENDIF
	
	steamname = GET_FILENAME_FOR_AUDIO_CONVERSATION(tl63TeamName)
	
	RETURN steamname
	
ENDFUNC

FUNC STRING GET_STRING_FROM_OVERRIDE_COLOUR(INT iTeam)
	STRING steamname
	TEXT_LABEL_63 tl63TeamName 
	
	IF MC_playerBD[iLocalPart].iteam = iTeam
	AND NOT bIsAnySpectator
		PRINTLN( "[RCC MISSION] [FILL_TEAM_NAMES][GET_STRING_FROM_OVERRIDE_COLOUR] Local players team (Team ", iTeam, ") called - YOUR TEAM")
		tl63TeamName = "DZ_Y_TEAM"
	ELSE
		IF bIsAnySpectator
			PRINTLN( "[RCC MISSION] [FILL_TEAM_NAMES][GET_STRING_FROM_OVERRIDE_COLOUR] I'm a spectator so I'm using the team's colour to name them")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciTEAM_COLOUR_NAMES)
			IF g_FMMC_STRUCT.iTeamColourOverride[iTeam] = 0
				PRINTLN( "[RCC MISSION] [FILL_TEAM_NAMES][GET_STRING_FROM_OVERRIDE_COLOUR] Team: " , iTeam , " team called - ORANGE TEAM")
				tl63TeamName = "DZ_O_TEAM"
			ELIF g_FMMC_STRUCT.iTeamColourOverride[iTeam] = 1
				PRINTLN( "[RCC MISSION] [FILL_TEAM_NAMES][GET_STRING_FROM_OVERRIDE_COLOUR] Team: " , iTeam , " team called - GREEN TEAM")
				tl63TeamName = "DZ_G_TEAM"
			ELIF g_FMMC_STRUCT.iTeamColourOverride[iTeam] = 2
				PRINTLN( "[RCC MISSION] [FILL_TEAM_NAMES][GET_STRING_FROM_OVERRIDE_COLOUR] Team: " , iTeam , " team called - PINK TEAM")
				tl63TeamName = "DZ_P_TEAM"
			ELIF g_FMMC_STRUCT.iTeamColourOverride[iTeam] = 4
				PRINTLN( "[RCC MISSION] [FILL_TEAM_NAMES][GET_STRING_FROM_OVERRIDE_COLOUR] Team: " , iTeam , " team called - PURPLE TEAM")
				tl63TeamName = "DZ_PU_TEAM"
			ENDIF
		ELSE
			tl63TeamName = "DZ_E_TEAM"
		ENDIF
	ENDIF
	
	steamname = GET_FILENAME_FOR_AUDIO_CONVERSATION(tl63TeamName)
	
	RETURN steamname
ENDFUNC

PROC FILL_TEAM_NAMES( INT iTeam, PLAYER_INDEX thisPlayer )

	BOOL bSingular 
	
	IF MC_serverBD.iNumStartingPlayers[iTeam] > 1
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciOptionsBS12_DONT_USE_PLAYER_NAME)
		bSingular = FALSE
	ELSE
		bSingular = TRUE
	ENDIF
	
	PRINTLN("[RCC MISSION] [FILL_TEAM_NAMES] NEW IMAGE VERSION iTeam: ", iTeam, " Player: ", GET_PLAYER_NAME(thisPlayer))
	

	//Team name based on overriden team colour 
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciALTERNATIVE_TEAM_NAMING)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciTEAM_COLOUR_NAMES)
		PRINTLN( "[RCC MISSION] [FILL_TEAM_NAMES] ciALTERNATIVE_TEAM_NAMING is set to TRUE ")
		g_sMission_TeamName[iTeam] = GET_STRING_FROM_OVERRIDE_COLOUR(iTeam)
	ELSE
		//Creator based team names 
		IF HAS_TEAM_GOT_CREATOR_NAME( iTeam )
			g_sMission_TeamName[ iTeam ] = GET_CREATOR_NAME_STRING( iTeam , bSingular )
			PRINTLN( "[RCC MISSION] [FILL_TEAM_NAMES] using custom creator team name: ", g_sMission_TeamName[ iTeam ] )
		// Otherwise we're on a mission alone
		ELIF bSingular
			//g_sMission_TeamName[ iTeam ] = GET_PLAYER_NAME( thisPlayer ) url:bugstar:3347263
			
			PRINTLN( "[RCC MISSION] [FILL_TEAM_NAMES] iTeam: ", iTeam)
			
			// Gets the first player on the team we passed through to show as the hud name..
			PLAYER_INDEX piToUse
			
			INT i = -1
			DO_PARTICIPANT_LOOP_FLAGS eFlags = GET_PARTICIPANT_LOOP_FLAGS_TO_CHECK_SINGLE_TEAM(iTeam)
			WHILE DO_PARTICIPANT_LOOP(i, eFlags | DPLF_CHECK_PLAYER_OK | DPLF_FILL_PLAYER_IDS)
				PRINTLN( "[RCC MISSION] [FILL_TEAM_NAMES] Set")
				piToUse = piParticipantLoop_PlayerIndex
				BREAKLOOP
			ENDWHILE
			
			IF piToUse = INVALID_PLAYER_INDEX()
				PRINTLN( "[RCC MISSION] [FILL_TEAM_NAMES] piToUse is -1 for iTeam: ", iTeam)
				piToUse = thisPlayer
			ENDIF
			
			g_sMission_TeamName[ iTeam ] = GET_PLAYER_NAME( piToUse )
			SET_BIT(g_iTeamNameBitSetPlayerName, iTeam)
			PRINTLN( "[RCC MISSION] [FILL_TEAM_NAMES] using players name as team name: ", g_sMission_TeamName[ iTeam ] )
		// Otherwise we're on a non-heist mission with >1 player
		ELSE
			IF ( GET_IS_EVERONE_ON_MY_TEAM_IN_THE_SAME_CREW( thisPlayer )
			AND NOT HAS_CREW_NAME_ALREADY_BEEN_USED( GET_PLAYER_CREW_NAME_RC( thisPlayer ) ) )
			AND NOT bSingular // 4720299
				g_sMission_TeamName[ iTeam ] = GET_PLAYER_CREW_NAME_RC( thisPlayer )
				SET_BIT( iLocalBoolCheck2, LBOOL2_MY_TEAM_NAME_IS_CREW )
				PRINTLN( "[RCC MISSION] [FILL_TEAM_NAMES] (1) GET_PLAYER_TEAM_NAME_RC -  ",g_sMission_TeamName[ iTeam ] )
			ELSE
				g_sMission_TeamName[ iTeam ] = GET_CREATOR_NAME_STRING( iTeam , FALSE, TRUE )
				PRINTLN( "[RCC MISSION] [FILL_TEAM_NAMES] (2) GET_PLAYER_TEAM_NAME_RC - ",g_sMission_TeamName[ iTeam ] )
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC TEXT_LABEL_63 GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(INT iteam, BOOL bSingular = FALSE, BOOL bGenericName = FALSE, BOOL bForCelebrationScreen = FALSE)

	TEXT_LABEL_63 tlReturnName, tl63DefaultName, tl63TeamName
	

	
	IF !bGenericName
	
		PRINTLN("GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN, Team ",iteam," - Not a generic name, iTeamName: ",g_FMMC_STRUCT.iTeamNames[iTeam])
		
		IF bSingular
			tl63TeamName = "FMMC_TEAM_S_"
		ELSE 
			tl63TeamName = "FMMC_TEAM_"
		ENDIF
		
		tl63TeamName += g_FMMC_STRUCT.iTeamNames[iTeam]
		
		IF g_FMMC_STRUCT.iTeamNames[iTeam] = ciTEAM_NAME_MAX
			tl63TeamName = g_FMMC_STRUCT.tlCustomName[iTeam]
			PRINTLN("GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN, Team ",iteam," - Custom name, text label: ",tl63TeamName)			
		ENDIF
		
		tlReturnName = GET_FILENAME_FOR_AUDIO_CONVERSATION(tl63TeamName)
		
		IF bForCelebrationScreen
			CLEAR_BIT(iCelebrationTeamNameIsNotLiteral, iteam)
		ENDIF
		
		PRINTLN("GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN, Team ",iteam," - Non-generic Team Name string: ",tlReturnName)
		
		IF ARE_STRINGS_EQUAL(tlReturnName,"NULL")
		OR IS_STRING_NULL_OR_EMPTY(tlReturnName)
			TEXT_LABEL_63 originalString
			TEXT_LABEL_15 tl15 = "FMMC_MN"
			
			originalString = g_FMMC_STRUCT.tlCustomName[iTeam]
			tlReturnName = GET_CROPPED_LITERAL_STRING_FOR_MENU(tl15, originalString)
			PRINTLN("GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN, Team ",iteam," - Non-generic Team Name string is NULL, getting non-stored string from creator: ",tlReturnName)
		ENDIF
		
		//make sure that if the string is outputting default we use generic names.
		IF ARE_STRINGS_EQUAL(tlReturnName,"FMMC_TEAM_0")
		OR ARE_STRINGS_EQUAL(tlReturnName,"FMMC_TEAM_S_0")
		OR ARE_STRINGS_EQUAL(tl63TeamName,"FMMC_TEAM_0")
		OR ARE_STRINGS_EQUAL(tl63TeamName,"FMMC_TEAM_S_0")
			bGenericName =  true
			PRINTLN("GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN, Team ",iteam," is using default. Switch to generic team names instead. ")
		ENDIF
		
	ENDIF
	
	if bGenericName		
		SWITCH iTeam
			CASE 0
				tl63TeamName = "MC_GT_NAME_1"
			BREAK
			CASE 1
				tl63TeamName = "MC_GT_NAME_2"
			BREAK
			CASE 2
				tl63TeamName = "MC_GT_NAME_3"
			BREAK
			CASE 3
				tl63TeamName = "MC_GT_NAME_4"
			BREAK
		ENDSWITCH
		
		PRINTLN("GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN, Team ",iteam," - A generic name, text label: ",tl63TeamName)
		
		tlReturnName = GET_FILENAME_FOR_AUDIO_CONVERSATION(tl63TeamName)
		
		IF bForCelebrationScreen
			CLEAR_BIT(iCelebrationTeamNameIsNotLiteral, iteam)
		ENDIF
		
		PRINTLN("GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN, Team ",iteam," - Generic Team Name string: ",tlReturnName)
		
	ENDIF
	
	tl63DefaultName = GET_FILENAME_FOR_AUDIO_CONVERSATION("DEFAULT_LABEL")
	
	IF bForCelebrationScreen
		IF ARE_STRINGS_EQUAL(tlReturnName, tl63DefaultName)
			tlReturnName = ""
			PRINTLN("GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN, name = ", tl63DefaultName, ", this is debug, setting to blank.")
		ENDIF
	ENDIF
		
	PRINTLN("GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN, Team ",iteam," - Returning string: ",tlReturnName)
	
	RETURN tlReturnName
	
ENDFUNC

PROC UPDATE_STORED_TEAM_SCORE_FOR_SHARD()
	
	iStoreLastTeam0Score = GET_TEAM_SCORE_FOR_DISPLAY(0)
	PRINTLN("[RCC MISSION][INCH SHARD] UPDATE_STORED_TEAM_SCORE_FOR_SHARD - Team Score updated - iStoreLastTeam0Score taem 0: ", iStoreLastTeam0Score)

	iStoreLastTeam1Score = GET_TEAM_SCORE_FOR_DISPLAY(1)
	PRINTLN("[RCC MISSION][INCH SHARD] UPDATE_STORED_TEAM_SCORE_FOR_SHARD - Team Score updated - iStoreLastTeam1Score team 1: ", iStoreLastTeam1Score)

ENDPROC

PROC SHOW_POINTS_ON_TEAM_BLIP()
	INT i
	FOR i = 0 TO NETWORK_GET_NUM_PARTICIPANTS() - 1 //FMMC2020
		IF MC_playerBD[i].iteam = MC_playerBD[iPartToUse].iteam
			IF iPTLTeamMateScore[i] != MC_playerBD[i].iPlayerScore
				SET_CUSTOM_BLIP_NUMBER_FOR_PLAYER(INT_TO_PLAYERINDEX(i),MC_playerBD[i].iPlayerScore, TRUE)
				iPTLTeamMateScore[i] = MC_playerBD[i].iPlayerScore
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Player Abilities HUD --------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to HUD for player abilities ---------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC BOOL SHOULD_SUPPORT_SNIPER_SHOW_TIMER()
	
	IF sLocalPlayerAbilities.sSupportSniperData.eSSState = SS_DEPLOY
	OR sLocalPlayerAbilities.sSupportSniperData.eSSState = SS_READY
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_SUPPORT_SNIPER_TIMER_INFO(TIMER_INFO_STRUCT& sTimerInfo)
	sTimerInfo.tTimer = sLocalPlayerAbilities.sSupportSniperData.tDurationTimer
	sTimerInfo.iTimerDuration = 0
	sTimerInfo.sTimerTitle = ""
	SWITCH sLocalPlayerAbilities.sSupportSniperData.eSSState
		CASE SS_DEPLOY
			sTimerInfo.iTimerDuration = g_FMMC_STRUCT.sPlayerAbilities.sSupportSniper.iDeployDuration * 1000
			sTimerInfo.sTimerTitle = "SNIPER_READY_IN"
		RETURN TRUE
		CASE SS_AIM
			sTimerInfo.iTimerDuration = g_FMMC_STRUCT.sPlayerAbilities.sSupportSniper.iAimDuration * 1000
			sTimerInfo.sTimerTitle = "SNIPER_AIM"
		RETURN TRUE
		DEFAULT
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Bottom-Right Healthbars ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to entity health bars in the bottom right -----------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC RESET_ENTITY_HEALTHBAR_STRUCT(ENTITY_HEALTHBAR_VARS &sEntityHealthbarVars)
	sEntityHealthbarVars.eiEntity = NULL
	sEntityHealthbarVars.vEntityCoords = <<0,0,0>>
	sEntityHealthbarVars.iEntityIndex = 0
	sEntityHealthbarVars.iEntityType = ciENTITY_TYPE_NONE
	sEntityHealthbarVars.iOnlyDisplayOnRule = -1
	sEntityHealthbarVars.iOnlyDisplayOnRuleBS = -1
	sEntityHealthbarVars.iOnlyDisplayOnRule_Team = -1
	sEntityHealthbarVars.iEntityHealthbarConfigBS = 0
	sEntityHealthbarVars.iDisplayInRange = -1
	sEntityHealthbarVars.ePercentageMeterLine = PERCENTAGE_METER_LINE_NONE
	sEntityHealthbarVars.iPretendZeroIs = 0
	sEntityHealthbarVars.iBlipName = -1
	sEntityHealthbarVars.iFrameAdded = -1
	sEntityHealthbarVars.piDriver = NULL
	sEntityHealthbarVars.tlHealthbarTitle = ""
	sEntityHealthbarVars.bIsLiteralString = FALSE
	sEntityHealthbarVars.bIsPlayerName = FALSE
	sEntityHealthbarVars.iPercentageToDisplay = 0
	sEntityHealthbarVars.iHealthbarColour = HUD_COLOUR_WHITE
ENDPROC

FUNC BOOL SHOULD_HEALTHBAR_NAME_BE_SHARED_BETWEEN_TEAMS(ENTITY_HEALTHBAR_VARS& sEntityHealthbarVars)

	SWITCH sEntityHealthbarVars.iEntityType
		CASE ciENTITY_TYPE_OBJECT
			RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sEntityHealthbarVars.iEntityIndex].iObjectBitSetThree, cibsOBJ3_ShareHealthbarNameBetweenTeams)
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_RANGE_OF_ENTITY_FOR_HEALTH_BAR(ENTITY_HEALTHBAR_VARS& sEntityHealthbarVars)
	IF sEntityHealthbarVars.iDisplayInRange <= 0
		RETURN TRUE
	ENDIF
	
	RETURN VDIST2(sEntityHealthbarVars.vEntityCoords, GET_ENTITY_COORDS(LocalPlayerPed)) < POW(TO_FLOAT(sEntityHealthbarVars.iDisplayInRange), 2)
ENDFUNC

FUNC BOOL SHOULD_HEALTHBAR_BE_HIDDEN_AT_CURRENT_PERCENTAGE(ENTITY_HEALTHBAR_VARS& sEntityHealthbarVars)
	
	// Hide by default when empty. Usually a healthbar is naturally hidden when empty purely due to the entity being dead and no longer having its healthbar processed.
	// This functionality is just to keep this behaviour consistent when using healthbars in any special-case ways where the entity's health bar can be empty without actually killing the entity.
	INT iHideBelowValue = 0
	
	IF sEntityHealthbarVars.ePercentageMeterLine != PERCENTAGE_METER_LINE_NONE
	AND IS_BIT_SET(sEntityHealthbarVars.iEntityHealthbarConfigBS, ciEntityHealthbarConfigBS_HideUnderPercentageLine)
		SWITCH sEntityHealthbarVars.ePercentageMeterLine
			CASE PERCENTAGE_METER_LINE_10
				iHideBelowValue = 10
			BREAK
			CASE PERCENTAGE_METER_LINE_20
				iHideBelowValue = 20
			BREAK
			CASE PERCENTAGE_METER_LINE_30
				iHideBelowValue = 30
			BREAK
			CASE PERCENTAGE_METER_LINE_40
				iHideBelowValue = 40
			BREAK
			CASE PERCENTAGE_METER_LINE_50
				iHideBelowValue = 50
			BREAK
			CASE PERCENTAGE_METER_LINE_60
				iHideBelowValue = 60
			BREAK
			CASE PERCENTAGE_METER_LINE_70
				iHideBelowValue = 70
			BREAK
			CASE PERCENTAGE_METER_LINE_80
				iHideBelowValue = 80
			BREAK
			CASE PERCENTAGE_METER_LINE_90
				iHideBelowValue = 90
			BREAK
		ENDSWITCH
	ENDIF
	
	IF IS_BIT_SET(sEntityHealthbarVars.iEntityHealthbarConfigBS, ciEntityHealthbarConfigBS_InvertPercentage)
		iHideBelowValue = (100 - iHideBelowValue)
	ENDIF
	
	RETURN sEntityHealthbarVars.iPercentageToDisplay <= iHideBelowValue
ENDFUNC

FUNC BOOL SHOULD_DRAW_ENTITY_HEALTHBAR(ENTITY_HEALTHBAR_VARS& sEntityHealthbarVars, INT& iTeam, INT& iRule)
	
	IF NOT bPlayerToUseOK
		PRINTLN("[Healthbars] SHOULD_DRAW_ENTITY_HEALTHBAR | Blocked by bPlayerToUseOK")
		RETURN FALSE
	ENDIF
		
	IF g_bMissionEnding
		PRINTLN("[Healthbars] SHOULD_DRAW_ENTITY_HEALTHBAR | Blocked by g_bMissionEnding")
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(sEntityHealthbarVars.eiEntity)
		PRINTLN("[Healthbars] SHOULD_DRAW_ENTITY_HEALTHBAR | Blocked by DOES_ENTITY_EXIST")
		RETURN FALSE
	ENDIF
	
	IF iRule >= FMMC_MAX_RULES
		PRINTLN("[Healthbars] SHOULD_DRAW_ENTITY_HEALTHBAR | Blocked by iRule >= FMMC_MAX_RULES")
		RETURN FALSE
	ENDIF
	
	IF sEntityHealthbarVars.iOnlyDisplayOnRule > -1 // Check if this entity is set to only display its healthbar on a particular Rule
		IF sEntityHealthbarVars.iOnlyDisplayOnRule_Team > -1
			// A particular Team has been specified
			IF GET_TEAM_CURRENT_RULE(sEntityHealthbarVars.iOnlyDisplayOnRule_Team, FALSE) != sEntityHealthbarVars.iOnlyDisplayOnRule
				PRINTLN("[Healthbars] SHOULD_DRAW_ENTITY_HEALTHBAR | Blocked by iOnlyDisplayOnRule: ", sEntityHealthbarVars.iOnlyDisplayOnRule, " / iOnlyDisplayOnRule_Team: ", sEntityHealthbarVars.iOnlyDisplayOnRule_Team)
				RETURN FALSE
			ENDIF
		ELSE
			// Check the local player's team
			IF iRule != sEntityHealthbarVars.iOnlyDisplayOnRule
				PRINTLN("[Healthbars] SHOULD_DRAW_ENTITY_HEALTHBAR | Blocked by iOnlyDisplayOnRule: ", sEntityHealthbarVars.iOnlyDisplayOnRule)
				RETURN FALSE
			ENDIF
		ENDIF
		
	ELIF sEntityHealthbarVars.iOnlyDisplayOnRuleBS != -1 // Check if this entity is set to only display its healthbar on a particular set of Rules
		IF sEntityHealthbarVars.iOnlyDisplayOnRule_Team > -1
			// A particular Team has been specified
			IF NOT IS_BIT_SET(sEntityHealthbarVars.iOnlyDisplayOnRuleBS, GET_TEAM_CURRENT_RULE(sEntityHealthbarVars.iOnlyDisplayOnRule_Team))
				PRINTLN("[Healthbars] SHOULD_DRAW_ENTITY_HEALTHBAR | Blocked by iOnlyDisplayOnRuleBS: ", sEntityHealthbarVars.iOnlyDisplayOnRuleBS, " / iOnlyDisplayOnRule_Team: ", sEntityHealthbarVars.iOnlyDisplayOnRule_Team)
				RETURN FALSE
			ENDIF
		ELSE
			// Check the local player's team
			IF NOT IS_BIT_SET(sEntityHealthbarVars.iOnlyDisplayOnRuleBS, iRule)
				PRINTLN("[Healthbars] SHOULD_DRAW_ENTITY_HEALTHBAR | Blocked by iOnlyDisplayOnRuleBS: ", sEntityHealthbarVars.iOnlyDisplayOnRuleBS)
				RETURN FALSE
			ENDIF
		ENDIF
		
	ELIF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_ENABLE_HEALTH_BAR) // If not handled by other rule-based settings set on this Entity, check the settings of the local player's Rule itself
		PRINTLN("[Healthbars] SHOULD_DRAW_ENTITY_HEALTHBAR | Blocked by ciBS_RULE_ENABLE_HEALTH_BAR not being set")
		RETURN FALSE
		
	ENDIF
	
	IF NOT IS_LOCAL_PLAYER_IN_RANGE_OF_ENTITY_FOR_HEALTH_BAR(sEntityHealthbarVars)
		PRINTLN("[Healthbars] SHOULD_DRAW_ENTITY_HEALTHBAR | Blocked by IS_LOCAL_PLAYER_IN_RANGE_OF_ENTITY_FOR_HEALTH_BAR. sEntityHealthbarVars.iDisplayInRange: ", sEntityHealthbarVars.iDisplayInRange)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC INT GET_TEAM_TO_USE_FOR_ENTITY_HEALTHBAR(ENTITY_HEALTHBAR_VARS& sEntityHealthbarVars, INT iLocalTeam)

	IF SHOULD_HEALTHBAR_NAME_BE_SHARED_BETWEEN_TEAMS(sEntityHealthbarVars)
		IF NOT IS_ENTITY_A_CURRENT_PRIORITY_FOR_TEAM(iLocalTeam, sEntityHealthbarVars.iEntityType, sEntityHealthbarVars.iEntityIndex)
			// This entity isn't our Team's priority, check if it is someone else's and use their Team if so
			INT iTeamLoop
			FOR iTeamLoop = 0 TO MC_ServerBD.iNumberOfTeams - 1
				IF IS_ENTITY_A_CURRENT_PRIORITY_FOR_TEAM(iTeamLoop, sEntityHealthbarVars.iEntityType, sEntityHealthbarVars.iEntityIndex)
					RETURN iTeamLoop
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
	
	RETURN iLocalTeam
ENDFUNC

FUNC TEXT_LABEL_63 GET_ENTITY_HEALTHBAR_TITLE(ENTITY_HEALTHBAR_VARS& sEntityHealthbarVars, INT& iTeamToUse, BOOL& bIsPlayerName, BOOL& bIsLiteralString)
	
	TEXT_LABEL_63 tlResult = ""
	INT iRuleToUse = GET_TEAM_CURRENT_RULE(iTeamToUse)
	bIsLiteralString = FALSE
	bIsPlayerName = FALSE
	
	IF sEntityHealthbarVars.iCustomTitleCustomStringIndex > -1
		// If this entity has a custom healthbar string, that takes priority over everything
		bIsLiteralString = TRUE
		tlResult = GET_CUSTOM_STRING_LIST_TEXT_LABEL(sEntityHealthbarVars.iCustomTitleCustomStringIndex)
		
	ELIF DOES_ENTITY_HAVE_CUSTOM_NAME(sEntityHealthbarVars.iEntityType, sEntityHealthbarVars.iEntityIndex)
		// If this entity has a custom name set, use that here
		bIsLiteralString = TRUE
		tlResult = GET_ENTITY_CUSTOM_NAME(sEntityHealthbarVars.iEntityType, sEntityHealthbarVars.iEntityIndex)
	
	ELIF sEntityHealthbarVars.iBlipName > -1
		// If this entity has a blip name set from the Custom String List, use that here
		bIsLiteralString = TRUE
		tlResult = GET_CUSTOM_STRING_LIST_TEXT_LABEL(sEntityHealthbarVars.iBlipName)
		
	ELIF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeamToUse].tl63ObjectiveEntityHUDMessage[iRuleToUse])
		// Use the text set for this Rule if any is set
		bIsLiteralString = TRUE
		tlResult = g_FMMC_STRUCT.sFMMCEndConditions[iTeamToUse].tl63ObjectiveEntityHUDMessage[iRuleToUse]
		
	ELIF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23CustomTargetString[iTeamToUse])
		// Use the custom target string if there is one
		bIsLiteralString = TRUE
		tlResult = g_FMMC_STRUCT.tl23CustomTargetString[iTeamToUse]
		
	ELIF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeamToUse].tl23ObjBlip[iRuleToUse])
		// If this rule has an objective blip name to use, display that here
		bIsLiteralString = TRUE
		tlResult = g_FMMC_STRUCT.sFMMCEndConditions[iTeamToUse].tl23ObjBlip[iRuleToUse]
		
	ELSE
		// Use these as defaults if nothing custom is set up
		SWITCH sEntityHealthbarVars.iEntityType
			CASE ciENTITY_TYPE_PED
				tlResult = GET_CREATOR_NAME_FOR_PED_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sEntityHealthbarVars.iEntityIndex].mn)
			BREAK
			CASE ciENTITY_TYPE_VEHICLE
				tlResult = "VEH_HEALTH"
			BREAK
			CASE ciENTITY_TYPE_OBJECT
				tlResult = "OBJ_HEALTH"
			BREAK
		ENDSWITCH
	ENDIF
	
	// TITLE POST-PROCESSING //
	
	// If this entity is a Vehicle with a player driving it, we insert that player's name in front of the title
	IF DOES_ENTITY_EXIST(sEntityHealthbarVars.piDriver)
		IF NOT IS_PED_INJURED(sEntityHealthbarVars.piDriver)
			IF IS_PED_A_PLAYER(sEntityHealthbarVars.piDriver)
				PLAYER_INDEX piDriverPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(sEntityHealthbarVars.piDriver)
				
				tlResult = GET_FILENAME_FOR_AUDIO_CONVERSATION(tlResult)
				bIsLiteralString = TRUE
				
				TEXT_LABEL_63 tlCurrentResult = tlResult
				tlResult = GET_PLAYER_NAME(piDriverPlayer)
				tlResult += " - "
				tlResult += tlCurrentResult
			ENDIF
		ENDIF
	ENDIF
	
	// Convert all literal string titles into uppercase text
	IF bIsLiteralString
		tlResult = CONVERT_STRING_TO_UPPERCASE(tlResult)
	ENDIF
	
	RETURN tlResult
ENDFUNC

FUNC INT GET_ENTITY_HEALTHBAR_HEALTH_OFFSET(ENTITY_HEALTHBAR_VARS& sEntityHealthbarVars)

	SWITCH sEntityHealthbarVars.iEntityType
		CASE ciENTITY_TYPE_PED
			RETURN -100
		BREAK
		CASE ciENTITY_TYPE_VEHICLE
			RETURN ROUND(MC_GET_VEHICLE_MAX_BODY_HEALTH(sEntityHealthbarVars.iEntityIndex) * g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sEntityHealthbarVars.iEntityIndex].iHealthBarPretendZeroIs * 0.1 * -1)
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC
		
FUNC INT GET_ENTITY_HEALTHBAR_PERCENTAGE_TO_DISPLAY(ENTITY_HEALTHBAR_VARS& sEntityHealthbarVars)
	
	FLOAT fPercentage = 0.0
	
	INT iCurrentHealth
	INT iMaxHealth
	
	SWITCH sEntityHealthbarVars.iEntityType
	
		CASE ciENTITY_TYPE_VEHICLE
		
			VEHICLE_INDEX viVehicle
			viVehicle = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityHealthbarVars.eiEntity)
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sEntityHealthbarVars.iEntityIndex].iHealthBarPretendZeroIs > 0
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sEntityHealthbarVars.iEntityIndex].iVehBitsetSix, ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR)		
					ASSERTLN("GET_ENTITY_HEALTHBAR_PERCENTAGE_TO_DISPLAY - Please enable 'Only Check Vehicle Body Health' when using the 'Pretend Zero Is' option")
				ENDIF
				
				iCurrentHealth = ROUND(GET_VEHICLE_BODY_HEALTH(viVehicle) + GET_ENTITY_HEALTHBAR_HEALTH_OFFSET(sEntityHealthbarVars))
				iMaxHealth = ROUND(MC_GET_VEHICLE_MAX_BODY_HEALTH(sEntityHealthbarVars.iEntityIndex, GET_TOTAL_STARTING_PLAYERS()) + GET_ENTITY_HEALTHBAR_HEALTH_OFFSET(sEntityHealthbarVars))
				
				IF iCurrentHealth > 0 AND iMaxHealth > 0
					fPercentage = (TO_FLOAT(iCurrentHealth) / TO_FLOAT(iMaxHealth)) * 100
				ENDIF
				
			ELSE
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sEntityHealthbarVars.iEntityIndex].iHealthDamageCombinedEntityIndex > -1
					fPercentage = GET_CURRENT_HEALTH_PERCENTAGES_OF_VEHICLE_AND_LINKED_VEHICLES(sEntityHealthbarVars.iEntityIndex)
				ELSE
					fPercentage = MC_GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(viVehicle, sEntityHealthbarVars.iEntityIndex, GET_TOTAL_STARTING_PLAYERS(), IS_VEHICLE_A_TRAILER(viVehicle))
				ENDIF
			ENDIF
		BREAK
		
		DEFAULT
		
			iCurrentHealth = GET_ENTITY_HEALTH(sEntityHealthbarVars.eiEntity) + GET_ENTITY_HEALTHBAR_HEALTH_OFFSET(sEntityHealthbarVars)
			iMaxHealth = GET_ENTITY_MAX_HEALTH(sEntityHealthbarVars.eiEntity) + GET_ENTITY_HEALTHBAR_HEALTH_OFFSET(sEntityHealthbarVars)
			
			IF iCurrentHealth > 0
			AND iMaxHealth > 0
				fPercentage = (TO_FLOAT(iCurrentHealth) / TO_FLOAT(iMaxHealth)) * 100
			ENDIF
			
		BREAK
	ENDSWITCH
	
	RETURN CEIL(fPercentage)
	
ENDFUNC

PROC PROCESS_ENTITY_HEALTHBAR_TURN_RED_AT_PERCENT(ENTITY_HEALTHBAR_VARS& sEntityHealthbarVars, INT iHealthBarTurnRedAtPercent)
		
	IF iHealthBarTurnRedAtPercent > 0
	AND sEntityHealthbarVars.iPercentageToDisplay <= iHealthBarTurnRedAtPercent * 10
		sEntityHealthbarVars.iHealthbarColour = HUD_COLOUR_RED
	ELSE
		sEntityHealthbarVars.iHealthbarColour = HUD_COLOUR_WHITE
	ENDIF
	
ENDPROC

//This function supports max num of 8 players in the team
PROC BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER_TEAM(INT iTeamToDisplay = 0, INT iHudOffset = 0)									
	IF SHOULD_HIDE_THE_HUD_THIS_FRAME()
		EXIT
	ENDIF
	
	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
	AND IS_SPECTATOR_SHOWING_NEWS_HUD()
		EXIT
	ENDIF
	
	INT iHudOrder = ENUM_TO_INT(HUDORDER_BOTTOM)
	iHudOrder += iHudOffset
	TEXT_LABEL_63 tl63LocalTitle
	BOOL bDisplayingEnemyTeam = NOT DOES_TEAM_LIKE_TEAM(MC_playerBD[iPartToUse].iTeam, iTeamToDisplay)
	HUD_COLOURS hudColour
	
	HIDE_BOTTOM_RIGHT_CODE_UI()
	
	ACTIVITY_POWERUP eActivityPowerup[ciHALLOWEEN_ADVERSARY_MAX_PLAYERS_PER_TEAM]
	PLAYER_INDEX piPlayers[ciHALLOWEEN_ADVERSARY_MAX_PLAYERS_PER_TEAM]
	INT iNumOfAliveTeamMembers
	INT iNextPlayerIndex
	
	//Check player status
	INT iPlayerIndex
	INT iTeam = -1
	FOR iPlayerIndex = 0 TO MAX_NUM_MC_PLAYERS - 1
		PARTICIPANT_INDEX piTemp = INT_TO_PARTICIPANTINDEX(iPlayerIndex)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(piTemp)
			PLAYER_INDEX piTempPlayer = NETWORK_GET_PLAYER_INDEX(piTemp)
			iTeam = MC_playerBD[iPlayerIndex].iTeam
									
			IF iTeam = -1
			OR iTeam != iTeamToDisplay
			OR NOT DOES_ENTITY_EXIST(GET_PLAYER_PED(piTempPlayer))
			OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piTempPlayer)
			OR IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR(piTempPlayer)
				RELOOP
			ENDIF
			
			IF iNextPlayerIndex >= ciHALLOWEEN_ADVERSARY_MAX_PLAYERS_PER_TEAM
				ASSERTLN("BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER_TEAM - Too many players in the team! The current max num of players per team is ", ciHALLOWEEN_ADVERSARY_MAX_PLAYERS_PER_TEAM)
				BREAKLOOP
			ENDIF
			
			piPlayers[iNextPlayerIndex] = piTempPlayer
			
			IF NOT IS_NET_PLAYER_OK(piTempPlayer)
				eActivityPowerup[iNextPlayerIndex] = ACTIVITY_POWERUP_PED_HEADSHOT_DEAD
			ELSE
				eActivityPowerup[iNextPlayerIndex] = ACTIVITY_POWERUP_PED_HEADSHOT
				iNumOfAliveTeamMembers++
			ENDIF
			
			iNextPlayerIndex++
		ENDIF
	ENDFOR
	
	IF bDisplayingEnemyTeam
		tl63LocalTitle = "DZ_E_TEAM"
		hudColour = HUD_COLOUR_ENEMY
	ELSE
		tl63LocalTitle = "DZ_Y_TEAM"
		hudColour = HUD_COLOUR_FRIENDLY
	ENDIF
	
	//Draw HUD
	IF iNextPlayerIndex + 1 > 6
		//We have more than 6 players in the team - draw 2 rows of 4
		DRAW_GENERIC_FOUR_ICON_BAR(hudColour, piPlayers[0], piPlayers[1], piPlayers[2], piPlayers[3], eActivityPowerup[0], eActivityPowerup[1], eActivityPowerup[2], eActivityPowerup[3], INT_TO_ENUM(HUDORDER, iHudOrder))
		DRAW_GENERIC_FOUR_ICON_BAR(hudColour, piPlayers[4], piPlayers[5], piPlayers[6], piPlayers[7], eActivityPowerup[4], eActivityPowerup[5], eActivityPowerup[6], eActivityPowerup[7], INT_TO_ENUM(HUDORDER, iHudOrder))
	ELSE 
		DRAW_GENERIC_SIX_ICON_BAR(hudColour, piPlayers[0], piPlayers[1], piPlayers[2], piPlayers[3], piPlayers[4], piPlayers[5], eActivityPowerup[0], eActivityPowerup[1], eActivityPowerup[2], eActivityPowerup[3], eActivityPowerup[4], eActivityPowerup[5], INT_TO_ENUM(HUDORDER, iHudOrder))
	ENDIF
	
	iHudOrder++
	
	HUD_COLOURS eTextColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamToDisplay, PlayerToUse)				
	BOOL bLiteral = FALSE
	
	HUDORDER eHudOrder = INT_TO_ENUM(HUDORDER, iHudOrder)

	DRAW_GENERIC_BIG_DOUBLE_NUMBER(iNumOfAliveTeamMembers, MC_serverBD.iNumberOfPlayingPlayers[iTeamToDisplay], tl63LocalTitle, DEFAULT, DEFAULT, eHudOrder, bLiteral, DEFAULT, DEFAULT, DEFAULT, DEFAULT, eTextColour)
ENDPROC

//Currently supports 2 teams only
PROC PROCESS_TEAMS_HUD()
	INT iTeam = MC_playerBD[iLocalPart].iTeam
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	//Is the option disabled?
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeam1EnableResurrectionStyleHUD, MC_serverBD_4.iCurrentHighestPriority[iTeam])
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeam2EnableResurrectionStyleHUD, MC_serverBD_4.iCurrentHighestPriority[iTeam])
		EXIT
	ENDIF
	
	INT iHudOffset = 0
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeam1EnableResurrectionStyleHUD, MC_serverBD_4.iCurrentHighestPriority[iTeam])
		BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER_TEAM(0)
		iHudOffset += ciBOTTOM_RIGHT_PLAYER_HUD_NUM_OF_ELEMENTS
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeam2EnableResurrectionStyleHUD, MC_serverBD_4.iCurrentHighestPriority[iTeam])
		BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER_TEAM(1, iHudOffset)
		iHudOffset += ciBOTTOM_RIGHT_PLAYER_HUD_NUM_OF_ELEMENTS
	ENDIF
ENDPROC

PROC STORE_ENTITY_HEALTHBAR_STRUCT(ENTITY_HEALTHBAR_VARS& sEntityHealthbarVars, BOOL bPersist = FALSE)
	
	IF bPersist
		INT i
		FOR i = 0 TO ciFMMC_MAX_ENTITY_HEALTHBARS - 1
			IF sHealthBars[i].eiEntity = sEntityHealthbarVars.eiEntity
				INT iPreviousPercentage = sHealthBars[i].iPercentageToDisplay
				sHealthBars[i] = sEntityHealthbarVars
				sHealthBars[i].iFrameAdded = GET_FRAME_COUNT()
				IF sHealthBars[i].iPercentageToDisplay > iPreviousPercentage
					sHealthBars[i].iPercentageToDisplay = iPreviousPercentage
					PRINTLN("[Healthbars] STORE_ENTITY_HEALTHBAR_STRUCT - Health has increased, ignoring. Entity type: ", sHealthBars[i].iEntityType, ", Entity Index: ", sHealthBars[i].iEntityIndex)
				ENDIF
				EXIT
			ENDIF
		ENDFOR
	ENDIF
	
	IF iCurrentHealthBars >= ciFMMC_MAX_ENTITY_HEALTHBARS
		ASSERTLN("STORE_ENTITY_HEALTHBAR_STRUCT - TOO MANY HEALTHBARS ADDED THIS FRAME.")
		EXIT
	ENDIF
	
	sHealthBars[iCurrentHealthBars] = sEntityHealthbarVars
	iCurrentHealthBars++
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Securoserv App
// ##### Description: Functions for interacting with the securoserv app
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_SECUROSERV_APP_ACTIVATED()
	RETURN IS_BIT_SET(iLocalBoolCheck35, LBOOL35_SECUROHACK_ACTIVE)
ENDFUNC

PROC REQUEST_SECUROSERV_APP_ACTIVATION()
#IF IS_DEBUG_BUILD
	IF NOT IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_SECUROHACK_REQUEST_ACTIVATION)
		PRINTLN("[SECUROHACK] REQUEST_SECUROSERV_APP_ACTIVATION - Activation Requested")
		DEBUG_PRINTCALLSTACK()
	ENDIF
#ENDIF
	SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_SECUROHACK_REQUEST_ACTIVATION)
	CLEAR_BIT(iLocalBoolCheck35, LBOOL35_SECUROHACK_REQUEST_STOP)
ENDPROC

PROC REQUEST_SECUROSERV_APP_STOP()
#IF IS_DEBUG_BUILD
	IF NOT IS_BIT_SET(iLocalBoolCheck35, LBOOL35_SECUROHACK_REQUEST_STOP)
	PRINTLN("[SECUROHACK] REQUEST_SECUROSERV_APP_STOP - Stop Requested")
		PRINTLN("[SECUROHACK] REQUEST_SECUROSERV_APP_ACTIVATION - Activation Requested")
		DEBUG_PRINTCALLSTACK()
	ENDIF
#ENDIF	
	iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
	SET_BIT(iLocalBoolCheck35, LBOOL35_SECUROHACK_REQUEST_STOP)
	CLEAR_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_SECUROHACK_REQUEST_ACTIVATION)
ENDPROC

PROC SET_SECUROSERV_APP_LOSING_SIGNAL()
#IF IS_DEBUG_BUILD
	IF NOT IS_BIT_SET(iLocalBoolCheck35, LBOOL35_SECUROHACK_LOSING_SIGNAL)
		PRINTLN("[SECUROHACK] SET_SECUROSERV_APP_LOSING_SIGNAL - Losing Signal")
		DEBUG_PRINTCALLSTACK()
	ENDIF
#ENDIF	
	SET_BIT(iLocalBoolCheck35, LBOOL35_SECUROHACK_LOSING_SIGNAL)
ENDPROC

PROC CLEAR_SECUROSERV_APP_LOSING_SIGNAL()
#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_SECUROHACK_LOSING_SIGNAL)
		PRINTLN("[SECUROHACK] CLEAR_SECUROSERV_APP_LOSING_SIGNAL - No Longer Losing Signal")
		DEBUG_PRINTCALLSTACK()
	ENDIF
#ENDIF	
	CLEAR_BIT(iLocalBoolCheck35, LBOOL35_SECUROHACK_LOSING_SIGNAL)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Custom HUDs
// ##### Description: Bottom Right HUDs content can make in the creator
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC RESET_CUSTOM_HUD_STRUCTS()
	INT i
	FOR i = 0 TO FMMC_MAX_CUSTOM_HUDS - 1
		sCustomHUDStructs[i].iScore = 0
		sCustomHUDStructs[i].iScoreSecondary = 0
		sCustomHUDStructs[i].iMaxScore = -1
		sCustomHUDStructs[i].iMaxScoreSecondary = -1
		sCustomHUDStructs[i].eHUDColour = HUD_COLOUR_PURE_WHITE
	ENDFOR
ENDPROC
