// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Trains ----------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Contains both server & client functions used by placed trains. Handles train objectives, blips, spawning and more.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020                                                                                       
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_EntityObjectives_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Misc Helpers
// ##### Description: General helpers and wrappers for train functionality
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC FLOAT GET_TRAIN_MOVING_CRUISE_SPEED(INT iTrain)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iPlacedTrain_StopMovingOnRuleTeam > -1
		INT iTeamPriority = GET_TEAM_CURRENT_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iPlacedTrain_StopMovingOnRuleTeam)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iPlacedTrain_StopMovingOnRuleBS, iTeamPriority)
			PRINTLN("[Trains_Speed][Train_Speed ", iTrain, "] GET_TRAIN_MOVING_CRUISE_SPEED - Staying still due to iPlacedTrain_StopMovingOnRuleBS")
			RETURN 0.0
		ENDIF
	ENDIF
	
	IF MC_serverBD_3.iCurrentTrainLocation[iTrain] != -1
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].sLocations[MC_serverBD_3.iCurrentTrainLocation[iTrain]].iBitset, ciTRAIN_LOCATION_BS__OVERRIDE_SPEED)
			PRINTLN("[Trains_Speed][Train_Speed ", iTrain, "] GET_TRAIN_MOVING_CRUISE_SPEED - Returning speed from location ", MC_serverBD_3.iCurrentTrainLocation[iTrain])
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].sLocations[MC_serverBD_3.iCurrentTrainLocation[iTrain]].fCruiseSpeed
		ENDIF
	ENDIF
	
	RETURN g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].fPlacedTrainCruiseSpeed
	
ENDFUNC

FUNC FLOAT GET_CURRENT_TRAIN_CRUISE_SPEED(INT iTrain)
	
	FLOAT fMovingSpeed = GET_TRAIN_MOVING_CRUISE_SPEED(iTrain)
	
	IF MC_serverBD_3.iTrainStopTime[iTrain] != 0
		INT iTime = NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE())
		IF iTime > MC_serverBD_3.iTrainStopTime[iTrain] + ciFMMC_TRAIN_STOP_TIME
			RETURN 0.0
		ENDIF
		
		FLOAT fAlpha = TO_FLOAT(iTime - MC_serverBD_3.iTrainStopTime[iTrain]) / ciFMMC_TRAIN_STOP_TIME
		RETURN INTERP_FLOAT(fMovingSpeed, 0.0, fAlpha)
	ENDIF
	
	PRINTLN("[Trains_Speed][Train_Speed ", iTrain, "] GET_CURRENT_TRAIN_CRUISE_SPEED - Returning ", fMovingSpeed)
	RETURN fMovingSpeed
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Train Locations
// ##### Description: Functions and helpers for processing train locations and their associated behaviors
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_NEXT_TRAIN_LOCATION(INT iTrain)
	
	INT iNextLocationToProcess = MC_serverBD_3.iCurrentTrainLocation[iTrain] + 1
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iPlacedTrainBS, ciPLACED_TRAIN_BS__LOOP_LOCATIONS)
		IF iNextLocationToProcess >= ciPLACED_TRAIN_MAX_LOCATIONS
		OR g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].sLocations[iNextLocationToProcess].iLinkedZone = -1
			iNextLocationToProcess = 0
		ENDIF
	ENDIF
	
	RETURN iNextLocationToProcess
	
ENDFUNC

PROC PROCESS_SERVER_TRAIN_OBJECTIVE_SKIP(INT iTrain)
	
	INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].sLocations[MC_serverBD_3.iCurrentTrainLocation[iTrain]].iObjectiveSkip_Team
	INT iRule = g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].sLocations[MC_serverBD_3.iCurrentTrainLocation[iTrain]].iObjectiveSkip_Rule
	
	IF iTeam = -1
	OR iRule = -1
		EXIT
	ENDIF
	
	IF NOT IS_TEAM_ACTIVE(iTeam)
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] != iRule
		PRINTLN("[Trains][Train ", iTrain, "] PROCESS_SERVER_TRAIN_OBJECTIVE_SKIP - Team: ", iTeam, " is not on the correct rule: ", iRule, " actual rule: ", MC_serverBD_4.iCurrentHighestPriority[iTeam])
		EXIT
	ENDIF
	
	BOOL bPass = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].sLocations[MC_serverBD_3.iCurrentTrainLocation[iTrain]].iBitset, ciTRAIN_LOCATION_BS__OBJ_SKIP_PASS)
	
	PRINTLN("[Trains][Train ", iTrain, "] PROCESS_SERVER_TRAIN_OBJECTIVE_SKIP - Team: ", iTeam, " is skipping obective: ", iRule, " pass: ", BOOL_TO_STRING(bPass))
	INVALIDATE_SINGLE_OBJECTIVE(iTeam, iRule, DEFAULT, bPass)
	
ENDPROC

/// PURPOSE:
///    Called on the frame a train's location changes by the host
PROC PROCESS_SERVER_TRAIN_ARRIVED_AT_NEW_LOCATION(INT iTrain)
	
	PROCESS_SERVER_TRAIN_OBJECTIVE_SKIP(iTrain)
	
ENDPROC

PROC PROCESS_SERVER_TRAIN_LOCATIONS(FMMC_TRAIN_STATE &sTrainState)
	
	IF !sTrainState.bTrainAlive
		EXIT
	ENDIF
	
	INT iNextLocationToProcess = GET_NEXT_TRAIN_LOCATION(sTrainState.iIndex)
	
	IF iNextLocationToProcess = MC_serverBD_3.iCurrentTrainLocation[sTrainState.iIndex]
		PRINTLN("[Trains][Train ", sTrainState.iIndex, "] PROCESS_TRAIN_LOCATIONS - Single zone loop detected this isn't supported.")
		ASSERTLN("[Trains][Train ", sTrainState.iIndex, "] PROCESS_TRAIN_LOCATIONS - Single zone loop detected this isn't supported.")
		EXIT
	ENDIF
	
	IF iNextLocationToProcess >= ciPLACED_TRAIN_MAX_LOCATIONS
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].sLocations[iNextLocationToProcess].iLinkedZone = -1
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_IN_FMMC_ZONE(sTrainState.viTrainIndex, g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].sLocations[iNextLocationToProcess].iLinkedZone)
		EXIT
	ENDIF
	
	PRINTLN("[Trains][Train ", sTrainState.iIndex, "] PROCESS_TRAIN_LOCATIONS - Train is in a new location: ", iNextLocationToProcess, " zone: ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].sLocations[iNextLocationToProcess].iLinkedZone)
	MC_serverBD_3.iCurrentTrainLocation[sTrainState.iIndex] = iNextLocationToProcess
	
	PROCESS_SERVER_TRAIN_ARRIVED_AT_NEW_LOCATION(sTrainState.iIndex)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Mid Mission Spawning
// ##### Description: Functions and helpers for creating trains mid-mission
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL ARE_WE_WAITING_FOR_TRAINS_TO_CLEAN_UP()
	
	IF MC_serverBD.iTrainCleanup_NeedOwnershipBS != 0
		RETURN TRUE
	ENDIF
	
	IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
	OR IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HOST_APPLIED_NEW_PRIORITY_MIDFRAME)
	OR IS_BIT_SET(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SPAWN_MID_MISSION_TRAIN(INT iTrain)
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niTrain[iTrain])
		PRINTLN("[Trains][Train ", iTrain, "] SPAWN_MID_MISSION_TRAIN - Already Exists.")
		SET_BIT(MC_serverBD.iTrainSpawnBitset, iTrain)
		RETURN TRUE
	ENDIF
	
	IF IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].vPlacedTrainPos)
		PRINTLN("[Trains][Train ", iTrain, "] SPAWN_MID_MISSION_TRAIN - No location set.")
		RETURN FALSE
	ENDIF
	
	IF NOT REQUEST_FMMC_MID_MISSION_TRAIN_ASSETS(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iPlacedTrainConfig)
		PRINTLN("[Trains][Train ", iTrain, "] SPAWN_MID_MISSION_TRAIN - Waiting for assets to load.")
		RETURN FALSE
	ENDIF
	
	IF ARE_WE_WAITING_FOR_TRAINS_TO_CLEAN_UP()
		PRINTLN("[Trains][Train ", iTrain, "] SPAWN_MID_MISSION_TRAIN - Waiting for trains to cleanup.")
		RETURN FALSE
	ENDIF
	
	IF NOT CAN_REGISTER_MISSION_VEHICLES(FMMC_MAX_NUM_OF_TRAIN_CARRIAGES + 1)
		#IF IS_DEBUG_BUILD
		ASSERTLN("[Trains][Train ", iTrain, "] SPAWN_MID_MISSION_TRAIN - Cannot register enough vehicles to spawn.")
		PRINTLN("[Trains][Train ", iTrain, "] SPAWN_MID_MISSION_TRAIN - Cannot register enough vehicles to spawn.")
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_TOO_MANY_VEHICLES_TO_SPAWN_TRAIN, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_TRAIN, "Too Many Vehicles exist to register a new train!")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT FMMC_CREATE_NET_TRAIN(MC_serverBD_1.sFMMC_SBD.niTrain[iTrain], g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain], GET_COORDS_FOR_ENTITY_CHECKPOINT_CONTINUITY(ciENTITY_TYPE_TRAIN, iTrain))
		PRINTLN("[Trains][Train ", iTrain, "] SPAWN_MID_MISSION_TRAIN - Creation Failed.")
		RETURN FALSE
	ENDIF
	
	PRINTLN("[Trains][Train ", iTrain, "] SPAWN_MID_MISSION_TRAIN - Train Created!")
	
	MC_SET_UP_TRAIN(iTrain, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niTrain[iTrain]))
	RELEASE_FMMC_TRAIN_ASSETS()
	SET_BIT(MC_serverBD.iTrainSpawnBitset, iTrain)
	
	RETURN TRUE
	
ENDFUNC				

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Train HUD and Blips
// ##### Description: Processing of HUD and blips for trains
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_TRAIN_BLIP_CLEANUP(FMMC_TRAIN_STATE &sTrainState, BLIP_RUNTIME_VARS_STRUCT sBlipRuntimeVars)

	IF SHOULD_ENTITY_BLIP_BE_FORCED_OFF(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].sTrainBlipStruct)
		RETURN TRUE
	ENDIF
	
	IF SHOULD_ENTITY_BLIP_BE_FORCED_ON(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].sTrainBlipStruct, sTrainState.viTrainIndex)
		RETURN FALSE
	ENDIF
	
	IF NOT WAS_PLAYER_PREVIOUSLY_IN_RANGE_OF_BLIP(sBlipRuntimeVars)
	
		IF NOT IS_PLAYER_WITHIN_ENTITY_BLIP_RANGE(sTrainState.viTrainIndex, g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].sTrainBlipStruct)
			PRINTLN("[Blips][Trains_SPAM][Train_SPAM ", sTrainState.iIndex, "] SHOULD_TRAIN_BLIP_CLEANUP - Out of blipping range")
			RETURN TRUE
		ENDIF
		IF NOT IS_ENTITY_WITHIN_HEIGHT_DIFFERENCE_RANGE_FOR_BLIP(sTrainState.viTrainIndex, g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].sTrainBlipStruct)
			PRINTLN("[Blips][Trains_SPAM][Train_SPAM ", sTrainState.iIndex, "] SHOULD_TRAIN_BLIP_CLEANUP - Out of height range")
			RETURN TRUE
		ENDIF
				
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_TRAIN_HAVE_BLIP(FMMC_TRAIN_STATE &sTrainState, BLIP_RUNTIME_VARS_STRUCT &sBlipRuntimeVars)
	
	IF SHOULD_TRAIN_BLIP_CLEANUP(sTrainState, sBlipRuntimeVars)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_ENTITY_BLIP_BE_FORCED_OFF(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].sTrainBlipStruct)
		RETURN FALSE
	ENDIF
	
	IF NOT SHOULD_ENTITY_BLIP_BE_FORCED_ON(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].sTrainBlipStruct, sTrainState.viTrainIndex)
	AND NOT SHOULD_ENTITY_BE_BLIPPED(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].sTrainBlipStruct, sBlipRuntimeVars, sTrainState.viTrainIndex, FALSE)
		RETURN FALSE
	ENDIF
	
	IF IS_BLIP_SET_AS_HIDDEN(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].sTrainBlipStruct)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_TRAIN_BLIP_HEADING(FMMC_TRAIN_STATE &sTrainState)
	IF SHOULD_BLIP_BE_MANUALLY_ROTATED(biTrainBlip[sTrainState.iIndex])
		SET_BLIP_ROTATION(biTrainBlip[sTrainState.iIndex], ROUND(GET_ENTITY_HEADING(sTrainState.viTrainIndex)))
	ENDIF
ENDPROC

PROC SET_TRAIN_BLIP_SETTINGS(FMMC_TRAIN_STATE &sTrainState)
	
	IF NOT IS_BLIP_SPRITE_OVERRIDE_SET(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].sTrainBlipStruct)
		SET_BLIP_SPRITE(biTrainBlip[sTrainState.iIndex], RADAR_TRACE_TRAIN)
	ENDIF
	
	INT iBlipColour = GET_DEFAULT_ENTITY_BLIP_COLOUR(FMMC_BLIP_TRAIN)
	
	IF GET_CORRECT_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].sTrainBlipStruct, g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].iAggroIndexBS_Entity_Train) != BLIP_COLOUR_DEFAULT
		iBlipColour =  GET_BLIP_COLOUR_FROM_CREATOR(GET_CORRECT_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].sTrainBlipStruct, g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].iAggroIndexBS_Entity_Train))
	ENDIF
	
	PRINTLN("[Blips][Trains][Train ", sTrainState.iIndex, "] SET_TRAIN_BLIP_SETTINGS - Blip setup: Colour is ", iBlipColour)
	SET_BLIP_COLOUR(biTrainBlip[sTrainState.iIndex], iBlipColour)
	
	SET_BLIP_HEIGHT_INDICATOR(biTrainBlip[sTrainState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].sTrainBlipStruct)
	
	PROCESS_TRAIN_BLIP_HEADING(sTrainState)
	
	SET_NAME_FOR_BLIP(biTrainBlip[sTrainState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].sTrainBlipStruct, FMMC_BLIP_TRAIN, sTrainState.iIndex, GET_LOCAL_PLAYER_TEAM(TRUE))

ENDPROC

PROC CREATE_BLIP_FOR_FMMC_TRAIN(FMMC_TRAIN_STATE &sTrainState)
	
	CREATE_BLIP_WITH_CREATOR_SETTINGS(
		FMMC_BLIP_TRAIN, 
		biTrainBlip[sTrainState.iIndex], 
		g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].sTrainBlipStruct, 
		sMissionTrainsLocalVars[sTrainState.iIndex].sBlipRuntimeVars, 
		sTrainState.viTrainIndex, 
		sTrainState.iIndex, 
		EMPTY_VEC(), 
		g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].iAggroIndexBS_Entity_Train
	)
	
	IF NOT DOES_BLIP_EXIST(biTrainBlip[sTrainState.iIndex])
		EXIT
	ENDIF
	
	SET_TRAIN_BLIP_SETTINGS(sTrainState)
	
ENDPROC

PROC PROCESS_TRAIN_BLIP_CREATION(FMMC_TRAIN_STATE &sTrainState, BLIP_RUNTIME_VARS_STRUCT &sBlipRuntimveVars)
	
	IF NOT DOES_BLIP_EXIST(biTrainBlip[sTrainState.iIndex])
		IF SHOULD_TRAIN_HAVE_BLIP(sTrainState, sBlipRuntimveVars)
			CREATE_BLIP_FOR_FMMC_TRAIN(sTrainState)
		ENDIF
	ELSE
		IF NOT SHOULD_TRAIN_HAVE_BLIP(sTrainState, sBlipRuntimveVars)
			REMOVE_TRAIN_BLIP(sTrainState.iIndex)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_TRAIN_BLIPS_EVERY_FRAME(FMMC_TRAIN_STATE &sTrainState)
	
	IF NOT DOES_BLIP_EXIST(biTrainBlip[sTrainState.iIndex])
		EXIT
	ENDIF
	
	PROCESS_TRAIN_BLIP_HEADING(sTrainState)
	
ENDPROC

PROC PROCESS_TRAIN_BLIPS_STAGGERED(FMMC_TRAIN_STATE &sTrainState)
	
	PROCESS_TRAIN_BLIP_CREATION(sTrainState, sMissionTrainsLocalVars[sTrainState.iIndex].sBlipRuntimeVars)
	
	IF DOES_BLIP_EXIST(biTrainBlip[sTrainState.iIndex])
		PROCESS_ENTITY_BLIP(biTrainBlip[sTrainState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].sTrainBlipStruct, GET_FMMC_TRAIN_COORDS(sTrainState), g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].iAggroIndexBS_Entity_Train)
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Warp on Rule
// ##### Description: Processing for the Warp on Rule functionality
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_TRAIN_WARP_ON_RULE(FMMC_TRAIN_STATE& sTrainState)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].iPlacedTrain_WarpOnRuleTeam = -1
		EXIT
	ENDIF
	
	VECTOR vWarpCoords = g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].vPlacedTrain_WarpOnRuleCoords[GET_TEAM_CURRENT_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].iPlacedTrain_WarpOnRuleTeam)]
	
	IF DOES_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].iPlacedTrain_WarpOnRuleTeam)
	AND NOT IS_VECTOR_ZERO(vWarpCoords)
		IF sTrainState.bHaveControlOfTrain
			PRINTLN("[Trains][Train ", sTrainState.iIndex, "] PROCESS_TRAIN_WARP_ON_RULE - Train ", sTrainState.iIndex, " warping on rule ", GET_TEAM_CURRENT_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].iPlacedTrain_WarpOnRuleTeam), " start! Moving to ", vWarpCoords)
			SET_MISSION_TRAIN_COORDS(sTrainState.viTrainIndex, vWarpCoords)
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Train Start-Moving and Start-Stopping Processing
// ##### Description: Keeping track of whether the train is moving or not, and processing things that should happen when the train starts moving or starts stopping.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC ENABLE_OR_DISABLE_TRAIN_MOVING_COLLISION_CARRIAGE_EXTRAS(FMMC_TRAIN_STATE& sTrainState, BOOL bEnable)

	IF NOT sTrainState.bHaveControlOfTrain
		PRINTLN("[Trains][Train ", sTrainState.iIndex, "] ENABLE_OR_DISABLE_TRAIN_MOVING_COLLISION_CARRIAGE_EXTRAS | I don't have control of this train")
		EXIT
	ENDIF
	
	INT iCarriage
	FOR iCarriage = 0 TO FMMC_MAX_NUM_OF_TRAIN_CARRIAGES - 1
		VEHICLE_INDEX viCarriage = GET_TRAIN_CARRIAGE(sTrainState.viTrainIndex, iCarriage)
		
		IF NOT DOES_ENTITY_EXIST(viCarriage)
			PRINTLN("[Trains][Train ", sTrainState.iIndex, "] ENABLE_OR_DISABLE_TRAIN_MOVING_COLLISION_CARRIAGE_EXTRAS | Carriage ", iCarriage, " doesn't exist")
			EXIT
		ENDIF
		
		INT iExtra = 0
		FOR iExtra = 1 TO FMMC_MAX_NUM_OF_TRAIN_CARRIAGE_EXTRAS - 1
		
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[sTrainState.iIndex].iPlacedTrain_CarriageExtrasToUseBS[iCarriage], iExtra)
				// Content don't want this extra enabled anyway - leave it as-is
				PRINTLN("[Trains_SPAM][Train_SPAM ", sTrainState.iIndex, "] ENABLE_OR_DISABLE_TRAIN_MOVING_COLLISION_CARRIAGE_EXTRAS | Extra ", iExtra, " for carriage ", iCarriage, " isn't set up in the Controller anyway")
				RELOOP
			ENDIF
			
			IF SHOULD_TRAIN_CARRIAGE_EXTRA_ONLY_BE_ACTIVE_WHILE_MOVING(viCarriage, iExtra)
				PRINTLN("[Trains][Train ", sTrainState.iIndex, "] ENABLE_OR_DISABLE_TRAIN_MOVING_COLLISION_CARRIAGE_EXTRAS | Toggling extra ", iExtra, " for carriage ", iCarriage, ". bEnable: ", bEnable)
				SET_VEHICLE_EXTRA(viCarriage, iExtra, !bEnable)
			ELSE
				// This carriage extra is permanant, not just a collision box - leave it as-is
				PRINTLN("[Trains][Train ", sTrainState.iIndex, "] ENABLE_OR_DISABLE_TRAIN_MOVING_COLLISION_CARRIAGE_EXTRAS | Extra ", iExtra, " for carriage ", iCarriage, " isn't enabled/disabled based on speed")
			ENDIF
		ENDFOR
	ENDFOR
ENDPROC

PROC PROCESS_TRAIN_START_MOVING_AND_START_STOPPING_FUNCTIONALITY(FMMC_TRAIN_STATE& sTrainState)
	BOOL bTrainMoving = (FMMC_GET_ENTITY_CURRENT_VELOCITY_MAGNITUDE(sTrainState.viTrainIndex) > 0.1)
	
	IF bTrainMoving
		IF NOT IS_BIT_SET(iTrainInMotionBS, sTrainState.iIndex)
		
			PRINTLN("[Trains][Train ", sTrainState.iIndex, "] PROCESS_TRAIN_START_MOVING_AND_START_STOPPING_FUNCTIONALITY | Train has started moving!")
			
			// Train has just started moving!
			ENABLE_OR_DISABLE_TRAIN_MOVING_COLLISION_CARRIAGE_EXTRAS(sTrainState, TRUE)
			SET_BIT(iTrainInMotionBS, sTrainState.iIndex)
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(tdTrainFullyStoppedTimer[sTrainState.iIndex])
			RESET_NET_TIMER(tdTrainFullyStoppedTimer[sTrainState.iIndex])
			PRINTLN("[Trains][Train ", sTrainState.iIndex, "] PROCESS_TRAIN_START_MOVING_AND_START_STOPPING_FUNCTIONALITY | Resetting tdTrainFullyStoppedTimer[", sTrainState.iIndex, "]!")
		ENDIF
		
	ELSE
		IF IS_BIT_SET(iTrainInMotionBS, sTrainState.iIndex)
			IF IS_TRAIN_PERFECTLY_STILL(sTrainState.iIndex)
				
				PRINTLN("[Trains][Train ", sTrainState.iIndex, "] PROCESS_TRAIN_START_MOVING_AND_START_STOPPING_FUNCTIONALITY | Train has completely stopped moving!")
				
				// Train has just stopped moving!
				ENABLE_OR_DISABLE_TRAIN_MOVING_COLLISION_CARRIAGE_EXTRAS(sTrainState, FALSE)
				CLEAR_BIT(iTrainInMotionBS, sTrainState.iIndex)
			ELSE
				PRINTLN("[Trains][Train ", sTrainState.iIndex, "] PROCESS_TRAIN_START_MOVING_AND_START_STOPPING_FUNCTIONALITY | Waiting for train to be perfectly still!")
				
			ENDIF
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(tdTrainFullyStoppedTimer[sTrainState.iIndex])
			REINIT_NET_TIMER(tdTrainFullyStoppedTimer[sTrainState.iIndex])
			PRINTLN("[Trains][Train ", sTrainState.iIndex, "] PROCESS_TRAIN_START_MOVING_AND_START_STOPPING_FUNCTIONALITY | Starting tdTrainFullyStoppedTimer[", sTrainState.iIndex, "]!")
		ENDIF
		
	ENDIF
ENDPROC

PROC PROCESS_TRAIN_STOPPING_ON_RULE(INT iTrain)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iPlacedTrain_StopMovingOnRuleTeam = -1
		EXIT
	ENDIF
	
	INT iTeamPriority = GET_TEAM_CURRENT_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iPlacedTrain_StopMovingOnRuleTeam)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iPlacedTrain_StopMovingOnRuleBS, iTeamPriority)
		IF MC_serverBD_3.iTrainStopTime[iTrain] != 0
			PRINTLN("[Trains][Train ", iTrain, "] PROCESS_TRAIN_STOPPING_ON_RULE - Train should be moving again, clearing timestamp.")
			MC_serverBD_3.iTrainStopTime[iTrain] = 0
		ENDIF
		EXIT
	ENDIF
	
	IF MC_serverBD_3.iTrainStopTime[iTrain] = 0
		MC_serverBD_3.iTrainStopTime[iTrain] = NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE())
		PRINTLN("[Trains][Train ", iTrain, "] PROCESS_TRAIN_STOPPING_ON_RULE - Train should stop, setting timestamp: ", MC_serverBD_3.iTrainStopTime[iTrain])
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Train Every Frame Processing
// ##### Description: Client and Server Train Every Frame Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_TRAIN_PRE_EVERY_FRAME()
	
	#IF IS_DEBUG_BUILD
	iTempProfilerActiveTrains = 0
	#ENDIF	
	
ENDPROC

PROC PROCESS_TRAIN_POST_EVERY_FRAME()
	
	#IF IS_DEBUG_BUILD
	iProfilerActiveTrains = iTempProfilerActiveTrains
	#ENDIF
	
ENDPROC

PROC PROCESS_TRAIN_EVERY_FRAME_CLIENT(INT iTrain)
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		EXIT
	ENDIF
	
	FMMC_TRAIN_STATE sTrainState
	FILL_FMMC_TRAIN_STATE_STRUCT(sTrainState, iTrain)
	
	IF NOT sTrainState.bTrainExists
		EXIT
	ENDIF
	
	IF sTrainState.bHaveControlOfTrain
	AND sTrainState.bTrainAlive
		SET_TRAIN_CRUISE_SPEED(sTrainState.viTrainIndex, GET_CURRENT_TRAIN_CRUISE_SPEED(iTrain))
	ENDIF
	
	PROCESS_TRAIN_BLIPS_EVERY_FRAME(sTrainState)
	
	PROCESS_TRAIN_WARP_ON_RULE(sTrainState)
	
	PROCESS_TRAIN_START_MOVING_AND_START_STOPPING_FUNCTIONALITY(sTrainState)
	
	#IF IS_DEBUG_BUILD
	iTempProfilerActiveVehicles++
	#ENDIF	
	
ENDPROC

PROC PROCESS_TRAIN_EVERY_FRAME_SERVER(INT iTrain)
	
	IF !bIsLocalPlayerHost
		EXIT
	ENDIF

	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		EXIT
	ENDIF
	
	FMMC_TRAIN_STATE sTrainState
	FILL_FMMC_TRAIN_STATE_STRUCT(sTrainState, iTrain)
	
	IF NOT sTrainState.bTrainExists
		
		IF IS_BIT_SET(MC_serverBD.iTrainCleanup_NeedOwnershipBS, sTrainState.iIndex)
			PRINTLN("[Trains][Train ", sTrainState.iIndex, "] PROCESS_TRAIN_EVERY_FRAME_SERVER - We were trying to request ownership of train for cleanup no longer exists!")
			CLEAR_BIT(MC_serverBD.iTrainCleanup_NeedOwnershipBS, sTrainState.iIndex)
		ENDIF

		IF IS_BIT_SET(MC_serverBD.iTrainSpawnBitset, sTrainState.iIndex)
			PRINTLN("[Trains][Train ", sTrainState.iIndex, "] PROCESS_TRAIN_EVERY_FRAME_SERVER - Clear iTrainSpawnBitset for train, net ID doesn't exist")
			CLEAR_BIT(MC_serverBD.iTrainSpawnBitset, sTrainState.iIndex)
		ENDIF
		
		// Spawn Trains mid-mission if set up to do so
		IF SHOULD_TRAIN_SPAWN_NOW(iTrain, FALSE)
			PRINTLN("[Trains][Train ", iTrain, "] PROCESS_TRAIN_EVERY_FRAME_SERVER | Spawning train mid-mission!")
			SPAWN_MID_MISSION_TRAIN(iTrain)
		ENDIF
	
		EXIT
	ENDIF

	IF !sTrainState.bTrainAlive
		IF IS_BIT_SET(MC_serverBD.iTrainSpawnBitset, sTrainState.iIndex)
			PRINTLN("[Trains][Train ", sTrainState.iIndex, "] PROCESS_TRAIN_EVERY_FRAME_SERVER - Clear iTrainSpawnBitset for train, not alive")
			CLEAR_BIT(MC_serverBD.iTrainSpawnBitset, sTrainState.iIndex)
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(MC_serverBD.iTrainCleanup_NeedOwnershipBS, sTrainState.iIndex)
		IF NOT sTrainState.bHaveControlOfTrain
			IF IS_FMMC_TRAIN_EMPTY(sTrainState)
				PRINTLN("[Trains][Train ", sTrainState.iIndex, "] PROCESS_TRAIN_EVERY_FRAME_SERVER - Requesting control of train for cleanup")
				NETWORK_REQUEST_CONTROL_OF_ENTITY(sTrainState.viTrainIndex)
			ENDIF
		ELSE
			IF IS_FMMC_TRAIN_EMPTY(sTrainState)
				PRINTLN("[Trains][Train ", sTrainState.iIndex, "] PROCESS_TRAIN_EVERY_FRAME_SERVER -  Deleting train.")
				DELETE_FMMC_TRAIN(sTrainState.iIndex)
			ENDIF
			EXIT
		ENDIF
	ENDIF	
	
	PROCESS_SERVER_TRAIN_LOCATIONS(sTrainState)
	PROCESS_TRAIN_STOPPING_ON_RULE(sTrainState.iIndex)
	
ENDPROC	

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Train Staggered Processing
// ##### Description: Client and Server Train Staggered Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_TRAIN_STAGGERED_CLIENT(INT iTrain)

	FMMC_TRAIN_STATE sTrainState
	FILL_FMMC_TRAIN_STATE_STRUCT(sTrainState, iTrain)
	
	PROCESS_TRAIN_BLIPS_STAGGERED(sTrainState)
	
	IF !sTrainState.bTrainExists
		EXIT
	ENDIF
		
	IF !sTrainState.bHaveControlOfTrain
		EXIT
	ENDIF
	
	// Enable high-precision blending for trains that are stopping or stopped, turn collision-box carriage extras on for trains that are moving
	IF GET_CURRENT_TRAIN_CRUISE_SPEED(iTrain) < 1.0
		IF NOT IS_BIT_SET(iTrainEnabledHighPrecisionBlendingBS, iTrain)
			// Train has just started stopping!
			SET_BIT(iTrainEnabledHighPrecisionBlendingBS, iTrain)
			NETWORK_USE_HIGH_PRECISION_TRAIN_BLENDING(sTrainState.niIndex, TRUE)
			PRINTLN("[Trains][Train ", iTrain, "] PROCESS_TRAIN_STAGGERED_CLIENT | Enabling high-precision blending for train!")
		ENDIF
	ELSE
		IF IS_BIT_SET(iTrainEnabledHighPrecisionBlendingBS, iTrain)
			// Train has just started moving!
			CLEAR_BIT(iTrainEnabledHighPrecisionBlendingBS, iTrain)
			NETWORK_USE_HIGH_PRECISION_TRAIN_BLENDING(sTrainState.niIndex, FALSE)
			PRINTLN("[Trains][Train ", iTrain, "] PROCESS_TRAIN_STAGGERED_CLIENT | Disabling high-precision blending for train!")
		ENDIF
	ENDIF
	
ENDPROC	

PROC PROCESS_TRAIN_STAGGERED_SERVER(INT iTrain)
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		EXIT
	ENDIF
	
	FMMC_TRAIN_STATE sTrainState
	FILL_FMMC_TRAIN_STATE_STRUCT(sTrainState, iTrain)
	
	IF !sTrainState.bTrainExists
		IF IS_BIT_SET(MC_serverBD.iTrainCleanup_NeedOwnershipBS, sTrainState.iIndex)
			PRINTLN("[Trains][Train ", sTrainState.iIndex, "] PROCESS_TRAIN_STAGGERED_SERVER - Clearing Need Ownership BS, Train has been cleaned up.")
			CLEAR_BIT(MC_serverBD.iTrainCleanup_NeedOwnershipBS, sTrainState.iIndex)
		ENDIF
		
		EXIT
	ENDIF
	
	IF SHOULD_CLEANUP_TRAIN(iTrain)
		IF NOT IS_BIT_SET(MC_serverBD.iTrainCleanup_NeedOwnershipBS, sTrainState.iIndex)
			PRINTLN("[Trains][Train ", sTrainState.iIndex, "] PROCESS_TRAIN_STAGGERED_SERVER - Setting Need Ownership BS.")
			SET_BIT(MC_serverBD.iTrainCleanup_NeedOwnershipBS, sTrainState.iIndex)
		ENDIF
	ELSE
		IF IS_BIT_SET(MC_serverBD.iTrainCleanup_NeedOwnershipBS, sTrainState.iIndex)
			PRINTLN("[Trains][Train ", sTrainState.iIndex, "] PROCESS_TRAIN_STAGGERED_SERVER - Clearing Need Ownership BS, Train no longer needs cleaned up.")
			CLEAR_BIT(MC_serverBD.iTrainCleanup_NeedOwnershipBS, sTrainState.iIndex)
		ENDIF
	ENDIF

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Train Objective Processing
// ##### Description: Client and Server Train Objective Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_TRAIN_OBJECTIVES_CLIENT(INT iTrain)
	
	//Currently no train objectives
	
	UNUSED_PARAMETER(iTrain)
	
//	IF !sTrainState.bTrainExists
//		EXIT
//	ENDIF
//
//	IF NOT SHOULD_TRAIN_BE_BLIPPED(sTrainState)
//		EXIT
//	ENDIF
//	
//	INT iTeam = GET_LOCAL_PLAYER_TEAM(TRUE)
//	
//	IF MC_serverBD_4.iTrainPriority[iTrain][iTeam] > MC_serverBD_4.iCurrentHighestPriority[iTeam]
//		EXIT
//	ENDIF
//	
//	IF MC_serverBD_4.iTrainPriority[iTrain][iTeam] >= FMMC_MAX_RULES
//		EXIT
//	ENDIF
//	
//	IF MC_serverBD_4.iTrainRule[iTrain][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
//		EXIT
//	ENDIF
//					
//	FLOAT fTrainDist2 = GET_DIST2_BETWEEN_ENTITIES(LocalPlayerPed, sTrainState.viTrainIndex)
//	IF fTrainDist2 > fNearestTargetDist2Temp
//		EXIT
//	ENDIF
//	
//	iNearestTargetTemp = iTrain
//	iNearestTargetTypeTemp = ci_TARGET_OBJECT
//	fNearestTargetDist2Temp = fTrainDist2
	
ENDPROC	

/// PURPOSE:
///    Processes whether the passed in placed train should be the passed in team's current objective.
PROC PROCESS_TRAIN_OBJECTIVES_SERVER(INT iTrain, INT iTeam)
	
	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	
	//Currently no train objectives
	
	UNUSED_PARAMETER(iTrain)
	UNUSED_PARAMETER(iTeam)
	
//	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niTrain[iTrain])
//		EXIT
//	ENDIF
//	
//	IF IS_ENTITY_DEAD(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niTrain[iTrain]))
//		EXIT
//	ENDIF
//		
//	IF MC_serverBD_4.iTrainRule[iTrain][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
//		EXIT
//	ENDIF
//	
//	IF MC_serverBD_4.iTrainPriority[iTrain][iTeam] >= FMMC_MAX_RULES
//		EXIT
//	ENDIF
//	
//	IF MC_serverBD_4.iTrainPriority[iTrain][iTeam] > iCurrentHighPriority[iTeam]
//		//Has an objective but the team has not reached it yet
//		EXIT
//	ENDIF
//	
//	PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_TRAIN_OBJECTIVES_SERVER - Train: ", iTrain, " Team: ", iTeam, " Objective is valid - Assigning. Priority: ",
//				MC_serverBD_4.iTrainPriority[iTrain][iTeam], " Rule: ", MC_serverBD_4.iTrainRule[iTrain][iTeam])
//		
//	iCurrentHighPriority[iTeam] = MC_serverBD_4.iTrainPriority[iTrain][iTeam]
//	iTempTrainMissionLogic[iTeam] = MC_serverBD_4.iTrainRule[iTrain][iTeam]
//	
//	IF iOldHighPriority[iTeam] > iCurrentHighPriority[iTeam]
//		iNumHighPriorityTrain[iTeam] = 0
//		iOldHighPriority[iTeam] = iCurrentHighPriority[iTeam]
//	ENDIF
//	
//	iNumHighPriorityTrain[iTeam]++
//	
//	RESET_TEMP_OBJECTIVE_VARS(iTeam, ciRULE_TYPE_TRAIN)
	
ENDPROC
