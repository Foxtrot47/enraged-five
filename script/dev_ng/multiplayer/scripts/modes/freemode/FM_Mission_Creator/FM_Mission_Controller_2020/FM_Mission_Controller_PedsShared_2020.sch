// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Peds Shared -------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Holds the shared functions for the headers PedsServer_2020.sch and PedsClient_2020.sch 																																										  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_EntityObjectives_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Goto Custom Scenario Processing ---------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions for running Custom Scenarios as part of a Ass Rule Ped Goto
// ##### 			This was going to be in a seperate header to contain pack specific logic - maybe later
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_SKIP_GOTO_RANGE_CHECK_FOR_CUSTOM_SCENARIO(FMMC_PED_STATE &sPedState)
	IF GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex]) = FMMC_PED_CUSTOM_SCENARIO_NONE
		RETURN FALSE
	ENDIF
	
	IF GET_CUSTOM_SCENARIO_INDEX_FOR_PED(sPedState.iIndex, TRUE) = -1
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC ENTITY_INDEX FMMC_PED_CUSTOM_SCENARIO_GET_TARGET_ENTITY(INT iPed, PED_INDEX piPed, INT iEntityTypeIndex, INT iEntityIDIndex)
	IF GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO_INT_VALUE(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], iEntityTypeIndex) = ciFMMC_ENTITY_LINK_TYPE_MAX //Nearest Player
		INT iPlayer = GET_NEAREST_PLAYER_TO_ENTITY(piPed)
		RETURN GET_ENTITY_FROM_PED_OR_VEHICLE(GET_PLAYER_PED(INT_TO_NATIVE(PLAYER_INDEX, iPlayer)))
	ENDIF
	RETURN GET_ENTITY_FROM_LINKED_ENTITY_TYPE(GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO_INT_VALUE(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], iEntityTypeIndex), GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO_INT_VALUE(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], iEntityIDIndex))
ENDFUNC

FUNC VECTOR FMMC_PED_CUSTOM_SCENARIO_GET_TARGET_COORDS(INT iPed, PED_INDEX piPed, INT iEntityTypeIndex, INT iEntityIDIndex)
	IF GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO_INT_VALUE(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], iEntityTypeIndex) = ciFMMC_ENTITY_LINK_TYPE_MAX //Nearest Player
		INT iPlayer = GET_NEAREST_PLAYER_TO_ENTITY(piPed)
		RETURN GET_PLAYER_COORDS(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))
	ENDIF
	RETURN GET_COORDS_FROM_LINKED_ENTITY_TYPE(GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO_INT_VALUE(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], iEntityTypeIndex), GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO_INT_VALUE(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], iEntityIDIndex))
ENDFUNC

PROC FMMC_PED_CUSTOM_SCENARIO_SET_INT(INT iPed, INT iScenarioIndex, INT iIntIndex, INT iIntValue)
	iAssociatedGOTOCustomScenarioInt[iScenarioIndex][iIntIndex] = iIntValue
	BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetCustomScenarioIntForPed, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iIntValue, iIntIndex)
ENDPROC

PROC FMMC_PED_CUSTOM_SCENARIO_REINIT_TIMER(INT iPed, INT iScenarioIndex, INT iTimerIndex)
	BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_StartCustomScenarioTimerForPed, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iTimerIndex)	
	REINIT_NET_TIMER(tdAssociatedGOTOCustomScenarioTimer[iScenarioIndex][iTimerIndex])
ENDPROC

FUNC BOOL FMMC_PED_CUSTOM_SCENARIO_PROCESS_SHOOT_AT_ENTITY(FMMC_PED_STATE &sPedState, INT iScenarioIndex, BOOL bSkip)
	
	UNUSED_PARAMETER(iScenarioIndex)
	
	IF bSkip
		RETURN TRUE
	ENDIF

	ENTITY_INDEX eiTarget = FMMC_PED_CUSTOM_SCENARIO_GET_TARGET_ENTITY(sPedState.iIndex, sPedState.pedIndex, ciFMMC_CUSTOM_SCENARIO_SHOOT_AT_ENTITY_MENU_ENTITY_TYPE, ciFMMC_CUSTOM_SCENARIO_SHOOT_AT_ENTITY_MENU_ENTITY_ID)
	
	IF DOES_ENTITY_EXIST(eiTarget)
		IF IS_ENTITY_DEAD(eiTarget)
			PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_SHOOT_AT_ENTITY - Target entity is dead, return true")
			RETURN TRUE
		ENDIF
		
		IF NOT IS_PED_PERFORMING_TASK(sPedState.pedIndex, SCRIPT_TASK_SHOOT_AT_ENTITY)
			PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_STAY_IN_COVER - Tasking ped to shoot at entity. Type: ", GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO_INT_VALUE(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], ciFMMC_CUSTOM_SCENARIO_SHOOT_AT_ENTITY_MENU_ENTITY_ID))
			SET_CURRENT_PED_WEAPON(sPedState.pedIndex, GET_BEST_PED_WEAPON(sPedState.pedIndex), TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPedState.pedIndex, TRUE) //Clean this up?
			SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
			TASK_SHOOT_AT_ENTITY(sPedState.pedIndex, eiTarget, -1, FIRING_TYPE_CONTINUOUS)
		ENDIF
	ELSE
		VECTOR vCoord = GET_COORDS_FROM_LINKED_ENTITY_TYPE(GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO_INT_VALUE(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], ciFMMC_CUSTOM_SCENARIO_SHOOT_AT_ENTITY_MENU_ENTITY_TYPE), GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO_INT_VALUE(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], ciFMMC_CUSTOM_SCENARIO_SHOOT_AT_ENTITY_MENU_ENTITY_ID))
		IF IS_VECTOR_ZERO(vCoord)
			PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_SHOOT_AT_ENTITY - No valid coord to shoot, return true")
			RETURN TRUE
		ENDIF
		
		IF NOT IS_PED_PERFORMING_TASK(sPedState.pedIndex, SCRIPT_TASK_SHOOT_AT_COORD)
			PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_SHOOT_AT_ENTITY - Tasking ped to shoot at coord ", vCoord)
			SET_CURRENT_PED_WEAPON(sPedState.pedIndex, GET_BEST_PED_WEAPON(sPedState.pedIndex), TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPedState.pedIndex, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
			TASK_SHOOT_AT_COORD(sPedState.pedIndex, vCoord, -1, FIRING_TYPE_CONTINUOUS)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC FMMC_PED_CUSTOM_SCENARIO_STAY_IN_COVER_CLEANUP(PED_INDEX piPed)
	SET_PED_TO_LOAD_COVER(piPed, FALSE)
ENDPROC

FUNC BOOL FMMC_PED_CUSTOM_SCENARIO_PROCESS_STAY_IN_COVER(FMMC_PED_STATE &sPedState, INT iScenarioIndex, BOOL bSkip)
	
	UNUSED_PARAMETER(iScenarioIndex)
	
	IF bSkip
		RETURN TRUE
	ENDIF
	
	IF NOT IS_PED_PERFORMING_TASK(sPedState.pedIndex, SCRIPT_TASK_STAY_IN_COVER)
		VECTOR VGotoCoords = GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex])
		SET_PED_TO_LOAD_COVER(sPedState.pedIndex, TRUE)
		IF NOT IS_PED_DEFENSIVE_AREA_ACTIVE(sPedState.pedIndex, FALSE)
		OR NOT ARE_VECTORS_ALMOST_EQUAL(GET_PED_DEFENSIVE_AREA_POSITION(sPedState.pedIndex), VGotoCoords)
			SET_PED_SPHERE_DEFENSIVE_AREA(sPedState.pedIndex, VGotoCoords, 10.0)
		ENDIF
		SET_PED_CAN_PEEK_IN_COVER(sPedState.pedIndex, FALSE)
		TASK_STAY_IN_COVER(sPedState.pedIndex)
		PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_STAY_IN_COVER - Tasking ped into cover")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC FMMC_PED_CUSTOM_SCENARIO_SUPPRESSING_FIRE_CLEANUP(PED_INDEX piPed)
	SET_PED_TO_LOAD_COVER(piPed, FALSE)
	SET_PED_INFINITE_AMMO_CLIP(piPed, FALSE)
ENDPROC

FUNC BOOL FMMC_PED_CUSTOM_SCENARIO_PROCESS_SUPPRESSING_FIRE(FMMC_PED_STATE &sPedState, INT iScenarioIndex, BOOL bSkip)
	
	IF bSkip
		RETURN TRUE
	ENDIF
	
	IF GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO_INT_VALUE(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], ciFMMC_CUSTOM_SCENARIO_SUPPRESSING_FIRE_MENU_SHOOT_TIME) = -1
	OR GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO_INT_VALUE(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], ciFMMC_CUSTOM_SCENARIO_SUPPRESSING_FIRE_MENU_COVER_TIME) = -1
		PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_SUPPRESSING_FIRE - No intervals set, ending immediately")
		RETURN TRUE
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(tdAssociatedGOTOCustomScenarioTimer[iScenarioIndex][ciFMMC_CUSTOM_SCENARIO_TIMER_SUPPRESSING_FIRE_INTERVAL]) 
		PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_SUPPRESSING_FIRE - Start Timer")
		FMMC_PED_CUSTOM_SCENARIO_REINIT_TIMER(sPedState.iIndex, iScenarioIndex, ciFMMC_CUSTOM_SCENARIO_TIMER_SUPPRESSING_FIRE_INTERVAL)
	ENDIF
	
	VECTOR vCoord, VGotoCoords
	
	SWITCH iAssociatedGOTOCustomScenarioInt[iScenarioIndex][ciFMMC_CUSTOM_SCENARIO_INT_INDEX_SUPPRESSING_FIRE_STATE]
	
		CASE ciFMMC_CUSTOM_SCENARIO_INT_VALUE_SUPPRESSING_FIRE_STATE_COVER
			
			IF NOT IS_PED_PERFORMING_TASK(sPedState.pedIndex, SCRIPT_TASK_STAY_IN_COVER)
				
				vCoord = FMMC_PED_CUSTOM_SCENARIO_GET_TARGET_COORDS(sPedState.iIndex, sPedState.pedIndex, ciFMMC_CUSTOM_SCENARIO_SUPPRESSING_FIRE_MENU_ENTITY_TYPE, ciFMMC_CUSTOM_SCENARIO_SUPPRESSING_FIRE_MENU_ENTITY_ID)
				
				IF IS_VECTOR_ZERO(vCoord)
					PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_SUPPRESSING_FIRE - No valid coord to cover from, return true")
					RETURN TRUE
				ENDIF
			
				VGotoCoords = GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex])
				SET_PED_TO_LOAD_COVER(sPedState.pedIndex, TRUE)
				SET_PED_INFINITE_AMMO_CLIP(sPedState.pedIndex, FALSE)
				IF NOT IS_PED_DEFENSIVE_AREA_ACTIVE(sPedState.pedIndex, FALSE)
				OR NOT ARE_VECTORS_ALMOST_EQUAL(GET_PED_DEFENSIVE_AREA_POSITION(sPedState.pedIndex), VGotoCoords)
					SET_PED_SPHERE_DEFENSIVE_AREA(sPedState.pedIndex, VGotoCoords, 10.0)
				ENDIF
				SET_PED_DEFENSIVE_AREA_DIRECTION(sPedState.pedIndex, vCoord)
				SET_PED_CAN_PEEK_IN_COVER(sPedState.pedIndex, FALSE)
				TASK_STAY_IN_COVER(sPedState.pedIndex)
				PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_SUPPRESSING_FIRE - Tasking ped into cover")
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(tdAssociatedGOTOCustomScenarioTimer[iScenarioIndex][ciFMMC_CUSTOM_SCENARIO_TIMER_SUPPRESSING_FIRE_INTERVAL], GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO_INT_VALUE(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], ciFMMC_CUSTOM_SCENARIO_SUPPRESSING_FIRE_MENU_COVER_TIME))
				PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_SUPPRESSING_FIRE - Cover Time Expired")
				FMMC_PED_CUSTOM_SCENARIO_SET_INT(sPedState.iIndex, iScenarioIndex, ciFMMC_CUSTOM_SCENARIO_INT_INDEX_SUPPRESSING_FIRE_STATE, ciFMMC_CUSTOM_SCENARIO_INT_VALUE_SUPPRESSING_FIRE_STATE_SHOOT)
				RESET_NET_TIMER(tdAssociatedGOTOCustomScenarioTimer[iScenarioIndex][ciFMMC_CUSTOM_SCENARIO_TIMER_SUPPRESSING_FIRE_INTERVAL])
			ENDIF
			
		BREAK
		
		CASE ciFMMC_CUSTOM_SCENARIO_INT_VALUE_SUPPRESSING_FIRE_STATE_SHOOT
			
			IF NOT IS_PED_PERFORMING_TASK(sPedState.pedIndex, SCRIPT_TASK_SHOOT_AT_COORD)
				
				vCoord = FMMC_PED_CUSTOM_SCENARIO_GET_TARGET_COORDS(sPedState.iIndex, sPedState.pedIndex, ciFMMC_CUSTOM_SCENARIO_SUPPRESSING_FIRE_MENU_ENTITY_TYPE, ciFMMC_CUSTOM_SCENARIO_SUPPRESSING_FIRE_MENU_ENTITY_ID)

				IF IS_VECTOR_ZERO(vCoord)
					PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_SUPPRESSING_FIRE - No valid coord to shoot, return true")
					SET_PED_TO_LOAD_COVER(sPedState.pedIndex, FALSE)
					SET_PED_INFINITE_AMMO_CLIP(sPedState.pedIndex, FALSE)
					RETURN TRUE
				ENDIF
				
				PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_SUPPRESSING_FIRE - Tasking ped to shoot at coord ", vCoord)
				SET_PED_INFINITE_AMMO_CLIP(sPedState.pedIndex, TRUE)
				SET_PED_CAN_PEEK_IN_COVER(sPedState.pedIndex, TRUE)
				SET_CURRENT_PED_WEAPON(sPedState.pedIndex, GET_BEST_PED_WEAPON(sPedState.pedIndex), TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPedState.pedIndex, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
				TASK_SHOOT_AT_COORD(sPedState.pedIndex, vCoord, -1, FIRING_TYPE_CONTINUOUS)
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(tdAssociatedGOTOCustomScenarioTimer[iScenarioIndex][ciFMMC_CUSTOM_SCENARIO_TIMER_SUPPRESSING_FIRE_INTERVAL], GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO_INT_VALUE(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], ciFMMC_CUSTOM_SCENARIO_SUPPRESSING_FIRE_MENU_SHOOT_TIME))
				PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_SUPPRESSING_FIRE - Shoot Time Expired")
				FMMC_PED_CUSTOM_SCENARIO_SET_INT(sPedState.iIndex, iScenarioIndex, ciFMMC_CUSTOM_SCENARIO_INT_INDEX_SUPPRESSING_FIRE_STATE, ciFMMC_CUSTOM_SCENARIO_INT_VALUE_SUPPRESSING_FIRE_STATE_COVER)
				RESET_NET_TIMER(tdAssociatedGOTOCustomScenarioTimer[iScenarioIndex][ciFMMC_CUSTOM_SCENARIO_TIMER_SUPPRESSING_FIRE_INTERVAL])
			ENDIF
			
		BREAK
		
		DEFAULT
			FMMC_PED_CUSTOM_SCENARIO_SET_INT(sPedState.iIndex, iScenarioIndex, ciFMMC_CUSTOM_SCENARIO_INT_INDEX_SUPPRESSING_FIRE_STATE, ciFMMC_CUSTOM_SCENARIO_INT_VALUE_SUPPRESSING_FIRE_STATE_COVER)
		BREAK
		
	ENDSWITCH

	RETURN FALSE
ENDFUNC

PROC FMMC_PED_CUSTOM_SCENARIO_BLIND_FIRE_CLEANUP(PED_INDEX piPed)
	SET_PED_TO_LOAD_COVER(piPed, FALSE)
	SET_COMBAT_FLOAT(piPed, CCF_BLIND_FIRE_CHANCE, 0.1) //Default
	SET_COMBAT_FLOAT(piPed, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 1.25) //Default
	SET_PED_COMBAT_ATTRIBUTES(piPed, CA_BLIND_FIRE_IN_COVER, FALSE)
ENDPROC

FUNC BOOL FMMC_PED_CUSTOM_SCENARIO_PROCESS_BLIND_FIRE(FMMC_PED_STATE &sPedState, INT iScenarioIndex, BOOL bSkip)
	
	UNUSED_PARAMETER(iScenarioIndex)
	
	IF bSkip
		RETURN TRUE
	ENDIF
	
	VECTOR vCoord, VGotoCoords
	
	IF NOT IS_PED_PERFORMING_TASK(sPedState.pedIndex, SCRIPT_TASK_STAY_IN_COVER)
		
		VGotoCoords = GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex])
		SET_PED_TO_LOAD_COVER(sPedState.pedIndex, TRUE)
		IF NOT IS_PED_DEFENSIVE_AREA_ACTIVE(sPedState.pedIndex, FALSE)
		OR NOT ARE_VECTORS_ALMOST_EQUAL(GET_PED_DEFENSIVE_AREA_POSITION(sPedState.pedIndex), VGotoCoords)
			SET_PED_SPHERE_DEFENSIVE_AREA(sPedState.pedIndex, VGotoCoords, 10.0)
		ENDIF
		
		vCoord = FMMC_PED_CUSTOM_SCENARIO_GET_TARGET_COORDS(sPedState.iIndex, sPedState.pedIndex, ciFMMC_CUSTOM_SCENARIO_BLIND_FIRE_MENU_ENTITY_TYPE, ciFMMC_CUSTOM_SCENARIO_BLIND_FIRE_MENU_ENTITY_ID)
		IF IS_VECTOR_ZERO(vCoord)
			SET_PED_DEFENSIVE_AREA_DIRECTION(sPedState.pedIndex, vCoord)
		ENDIF
		
		PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_BLIND_FIRE - Tasking ped to shoot at coord ", vCoord)
		SET_PED_CAN_PEEK_IN_COVER(sPedState.pedIndex, FALSE)
		SET_CURRENT_PED_WEAPON(sPedState.pedIndex, GET_BEST_PED_WEAPON(sPedState.pedIndex), TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPedState.pedIndex, TRUE)
		SET_COMBAT_FLOAT(sPedState.pedIndex, CCF_BLIND_FIRE_CHANCE, 1.0) //100%
		SET_COMBAT_FLOAT(sPedState.pedIndex, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 4.25) //Stolen from Heists 1
		SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_BLIND_FIRE_IN_COVER, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
		TASK_STAY_IN_COVER(sPedState.pedIndex)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC WEAPON_TYPE GET_WEAPON_TYPE_FOR_BLIND_EXPLOSIVE_CUSTOM_SCENARIO(INT iWeaponType)
	
	SWITCH iWeaponType
		CASE ciFMMC_CUSTOM_SCENARIO_BLIND_EXPLOSIVE_MENU_WEAPON_PROX_MINE
			RETURN WEAPONTYPE_DLC_PROXMINE
		CASE ciFMMC_CUSTOM_SCENARIO_BLIND_EXPLOSIVE_MENU_WEAPON_MOLOTOV
			RETURN WEAPONTYPE_MOLOTOV
	ENDSWITCH
	
	RETURN WEAPONTYPE_GRENADE
	
ENDFUNC

PROC FMMC_PED_CUSTOM_SCENARIO_BLIND_EXPLOSIVE_CLEANUP(PED_INDEX piPed)
	SET_CURRENT_PED_WEAPON(piPed, GET_BEST_PED_WEAPON(piPed))
	SET_PED_TO_LOAD_COVER(piPed, FALSE)
ENDPROC

FUNC BOOL FMMC_PED_CUSTOM_SCENARIO_PROCESS_BLIND_EXPLOSIVE(FMMC_PED_STATE &sPedState, INT iScenarioIndex, BOOL bSkip)
	
	IF bSkip
		RETURN TRUE
	ENDIF
	
	IF GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO_INT_VALUE(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], ciFMMC_CUSTOM_SCENARIO_BLIND_EXPLOSIVE_MENU_COUNT) = -1
	OR GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO_INT_VALUE(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], ciFMMC_CUSTOM_SCENARIO_BLIND_EXPLOSIVE_MENU_WEAPON) = -1
		PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_BLIND_EXPLOSIVE - No data set, ending immediately")
		RETURN TRUE
	ENDIF
	
	VECTOR vCoord, VGotoCoords
	WEAPON_TYPE wtWeapon = GET_WEAPON_TYPE_FOR_BLIND_EXPLOSIVE_CUSTOM_SCENARIO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData.sPointData[MC_serverBD_2.iPedGotoProgress[sPedState.iIndex]].iCustomScenarioInts[ciFMMC_CUSTOM_SCENARIO_BLIND_EXPLOSIVE_MENU_WEAPON])
	
	SWITCH iAssociatedGOTOCustomScenarioInt[iScenarioIndex][ciFMMC_CUSTOM_SCENARIO_INT_INDEX_BLIND_EXPLOSIVE_STATE]
	
		CASE ciFMMC_CUSTOM_SCENARIO_INT_VALUE_BLIND_EXPLOSIVE_STATE_COVER
			
			IF NOT IS_PED_PERFORMING_TASK(sPedState.pedIndex, SCRIPT_TASK_THROW_PROJECTILE)
				IF NOT IS_PED_PERFORMING_TASK(sPedState.pedIndex, SCRIPT_TASK_STAY_IN_COVER)
					vCoord = FMMC_PED_CUSTOM_SCENARIO_GET_TARGET_COORDS(sPedState.iIndex, sPedState.pedIndex, ciFMMC_CUSTOM_SCENARIO_BLIND_EXPLOSIVE_MENU_ENTITY_TYPE, ciFMMC_CUSTOM_SCENARIO_BLIND_EXPLOSIVE_MENU_ENTITY_ID)
					
					IF IS_VECTOR_ZERO(vCoord)
						PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_BLIND_EXPLOSIVE - No valid coord to cover from, return true")
						RETURN TRUE
					ENDIF
				
					VGotoCoords = GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex])
					SET_PED_TO_LOAD_COVER(sPedState.pedIndex, TRUE)
					SET_PED_INFINITE_AMMO_CLIP(sPedState.pedIndex, FALSE)
					IF NOT IS_PED_DEFENSIVE_AREA_ACTIVE(sPedState.pedIndex, FALSE)
					OR NOT ARE_VECTORS_ALMOST_EQUAL(GET_PED_DEFENSIVE_AREA_POSITION(sPedState.pedIndex), VGotoCoords)
						SET_PED_SPHERE_DEFENSIVE_AREA(sPedState.pedIndex, VGotoCoords, 10.0)
					ENDIF
					SET_PED_DEFENSIVE_AREA_DIRECTION(sPedState.pedIndex, vCoord)
					SET_PED_CAN_PEEK_IN_COVER(sPedState.pedIndex, FALSE)
					TASK_STAY_IN_COVER(sPedState.pedIndex)
					PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_BLIND_EXPLOSIVE - Tasking ped into cover")
				ENDIF

				FMMC_PED_CUSTOM_SCENARIO_REINIT_TIMER(sPedState.iIndex, iScenarioIndex, ciFMMC_CUSTOM_SCENARIO_TIMER_BLIND_EXPLOSIVE_INTERVAL)
				FMMC_PED_CUSTOM_SCENARIO_SET_INT(sPedState.iIndex, iScenarioIndex, ciFMMC_CUSTOM_SCENARIO_INT_INDEX_BLIND_EXPLOSIVE_STATE, ciFMMC_CUSTOM_SCENARIO_INT_VALUE_BLIND_EXPLOSIVE_STATE_WAIT)
			ENDIF		
			
		BREAK
		
		CASE ciFMMC_CUSTOM_SCENARIO_INT_VALUE_BLIND_EXPLOSIVE_STATE_WAIT
			
			IF HAS_NET_TIMER_EXPIRED(tdAssociatedGOTOCustomScenarioTimer[iScenarioIndex][ciFMMC_CUSTOM_SCENARIO_TIMER_BLIND_EXPLOSIVE_INTERVAL], 3000)
				PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_BLIND_EXPLOSIVE - Wait Time Expired")
				IF iAssociatedGOTOCustomScenarioInt[iScenarioIndex][ciFMMC_CUSTOM_SCENARIO_INT_INDEX_BLIND_EXPLOSIVE_COUNT] >= GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO_INT_VALUE(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], ciFMMC_CUSTOM_SCENARIO_BLIND_EXPLOSIVE_MENU_COUNT)
					PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_BLIND_EXPLOSIVE - Thrown All ", GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO_INT_VALUE(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], ciFMMC_CUSTOM_SCENARIO_BLIND_EXPLOSIVE_MENU_COUNT))
					RETURN TRUE
				ENDIF
				
				FMMC_PED_CUSTOM_SCENARIO_SET_INT(sPedState.iIndex, iScenarioIndex, ciFMMC_CUSTOM_SCENARIO_INT_INDEX_BLIND_EXPLOSIVE_STATE, ciFMMC_CUSTOM_SCENARIO_INT_VALUE_BLIND_EXPLOSIVE_STATE_THROW)
				RESET_NET_TIMER(tdAssociatedGOTOCustomScenarioTimer[iScenarioIndex][ciFMMC_CUSTOM_SCENARIO_TIMER_BLIND_EXPLOSIVE_INTERVAL])
			ENDIF
			
		BREAK
		
		CASE ciFMMC_CUSTOM_SCENARIO_INT_VALUE_BLIND_EXPLOSIVE_STATE_THROW
			
			IF NOT IS_PED_PERFORMING_TASK(sPedState.pedIndex, SCRIPT_TASK_THROW_PROJECTILE)
				
				vCoord = FMMC_PED_CUSTOM_SCENARIO_GET_TARGET_COORDS(sPedState.iIndex, sPedState.pedIndex, ciFMMC_CUSTOM_SCENARIO_BLIND_EXPLOSIVE_MENU_ENTITY_TYPE, ciFMMC_CUSTOM_SCENARIO_BLIND_EXPLOSIVE_MENU_ENTITY_ID)

				IF IS_VECTOR_ZERO(vCoord)
					PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_BLIND_EXPLOSIVE - No valid coord to shoot, return true")
					RETURN TRUE
				ENDIF
				
				PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_BLIND_EXPLOSIVE - Tasking ped to throw at coord ", vCoord)
				SET_CURRENT_PED_WEAPON(sPedState.pedIndex, wtWeapon, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPedState.pedIndex, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
				TASK_THROW_PROJECTILE(sPedState.pedIndex, vCoord)
				
				FMMC_PED_CUSTOM_SCENARIO_SET_INT(sPedState.iIndex, iScenarioIndex, ciFMMC_CUSTOM_SCENARIO_INT_INDEX_BLIND_EXPLOSIVE_COUNT, iAssociatedGOTOCustomScenarioInt[iScenarioIndex][ciFMMC_CUSTOM_SCENARIO_INT_INDEX_BLIND_EXPLOSIVE_COUNT] + 1)
				FMMC_PED_CUSTOM_SCENARIO_SET_INT(sPedState.iIndex, iScenarioIndex, ciFMMC_CUSTOM_SCENARIO_INT_INDEX_BLIND_EXPLOSIVE_STATE, ciFMMC_CUSTOM_SCENARIO_INT_VALUE_BLIND_EXPLOSIVE_STATE_COVER)
			ENDIF
			
		BREAK
		
		DEFAULT									
			GIVE_WEAPON_TO_PED(sPedState.pedIndex, wtWeapon, GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO_INT_VALUE(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], ciFMMC_CUSTOM_SCENARIO_BLIND_EXPLOSIVE_MENU_COUNT))
			FMMC_PED_CUSTOM_SCENARIO_SET_INT(sPedState.iIndex, iScenarioIndex, ciFMMC_CUSTOM_SCENARIO_INT_INDEX_BLIND_EXPLOSIVE_STATE, ciFMMC_CUSTOM_SCENARIO_INT_VALUE_BLIND_EXPLOSIVE_STATE_COVER)
			FMMC_PED_CUSTOM_SCENARIO_SET_INT(sPedState.iIndex, iScenarioIndex, ciFMMC_CUSTOM_SCENARIO_INT_INDEX_BLIND_EXPLOSIVE_COUNT, 0)
		BREAK
		
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL FMMC_PED_CUSTOM_SCENARIO_PROCESS_SKIP_CONDITIONS(FMMC_PED_STATE &sPedState, INT iScenarioIndex)
	
	IF g_iFMMC_SkipPedScenario = sPedState.iIndex
		PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_SKIP_CONDITIONS - Emergency Global Set!")
		g_iFMMC_SkipPedScenario = -1
		RETURN TRUE
	ENDIF
	
	INT iCustomScenarioSkipData = GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO_SKIP_DATA(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex])
	
	SWITCH GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO_SKIP_CONDITION(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex])
		
		CASE FMMC_PED_CUSTOM_SCENARIO_SKIP_CONDITION_TIMER
			
			IF iCustomScenarioSkipData > -1
				IF NOT HAS_NET_TIMER_STARTED(tdAssociatedGOTOCustomScenarioTimer[iScenarioIndex][ciFMMC_CUSTOM_SCENARIO_TIMER_SKIP]) 
					PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_SKIP_CONDITIONS - Start Timer: ", iCustomScenarioSkipData)
					FMMC_PED_CUSTOM_SCENARIO_REINIT_TIMER(sPedState.iIndex, iScenarioIndex, ciFMMC_CUSTOM_SCENARIO_TIMER_SKIP)
				ENDIF
				
				IF HAS_NET_TIMER_EXPIRED(tdAssociatedGOTOCustomScenarioTimer[iScenarioIndex][ciFMMC_CUSTOM_SCENARIO_TIMER_SKIP], iCustomScenarioSkipData)
					PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_SKIP_CONDITIONS - Expired: ", iCustomScenarioSkipData)
					RETURN TRUE
				ENDIF
			ENDIF
			
		BREAK
				
		CASE FMMC_PED_CUSTOM_SCENARIO_SKIP_CONDITION_PREREQ
			IF IS_PREREQUISITE_COMPLETED(iCustomScenarioSkipData)
				PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] FMMC_PED_CUSTOM_SCENARIO_PROCESS_SKIP_CONDITIONS - Prereq Complete: ", iCustomScenarioSkipData)
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Runs a custom scenario during a ped's goto.
/// RETURNS:
///    True once the scenario has finished.

FUNC BOOL PROCESS_CLIENT_ASSOCIATED_GOTO_CUSTOM_SCENARIO(FMMC_PED_STATE &sPedState)	

	IF FMMC_IS_LONG_BIT_SET(iPedGotoAssCompletedCustomScenarioBitset, sPedState.iIndex)
		RETURN TRUE
	ENDIF
	
	INT iScenarioIndex = GET_CUSTOM_SCENARIO_INDEX_FOR_PED(sPedState.iIndex)
	IF iScenarioIndex = -1
		RETURN FALSE
	ENDIF
	
	BOOL bScenarioComplete = FALSE
	BOOL bSkipScenario = FALSE
	
	IF FMMC_PED_CUSTOM_SCENARIO_PROCESS_SKIP_CONDITIONS(sPedState, iScenarioIndex)
		bSkipScenario = TRUE
	ENDIF
	
	FMMC_PED_CUSTOM_SCENARIO eCustomScenarioType = GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_SCENARIO(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex])
	
	SWITCH eCustomScenarioType
		CASE FMMC_PED_CUSTOM_SCENARIO_SHOOT_AT_ENTITY
			IF FMMC_PED_CUSTOM_SCENARIO_PROCESS_SHOOT_AT_ENTITY(sPedState, iScenarioIndex, bSkipScenario)
				bScenarioComplete = TRUE
			ENDIF
		BREAK
		CASE FMMC_PED_CUSTOM_SCENARIO_SUPPRESSING_FIRE
			IF FMMC_PED_CUSTOM_SCENARIO_PROCESS_SUPPRESSING_FIRE(sPedState, iScenarioIndex, bSkipScenario)
				FMMC_PED_CUSTOM_SCENARIO_SUPPRESSING_FIRE_CLEANUP(sPedState.pedIndex)
				bScenarioComplete = TRUE
			ENDIF
		BREAK
		CASE FMMC_PED_CUSTOM_SCENARIO_STAY_IN_COVER
			IF FMMC_PED_CUSTOM_SCENARIO_PROCESS_STAY_IN_COVER(sPedState, iScenarioIndex, bSkipScenario)
				FMMC_PED_CUSTOM_SCENARIO_STAY_IN_COVER_CLEANUP(sPedState.pedIndex)
				bScenarioComplete = TRUE
			ENDIF
		BREAK
		CASE FMMC_PED_CUSTOM_SCENARIO_BLIND_FIRE
			IF FMMC_PED_CUSTOM_SCENARIO_PROCESS_BLIND_FIRE(sPedState, iScenarioIndex, bSkipScenario)
				FMMC_PED_CUSTOM_SCENARIO_BLIND_FIRE_CLEANUP(sPedState.pedIndex)
				bScenarioComplete = TRUE
			ENDIF
		BREAK
		CASE FMMC_PED_CUSTOM_SCENARIO_BLIND_EXPLOSIVE
			IF FMMC_PED_CUSTOM_SCENARIO_PROCESS_BLIND_EXPLOSIVE(sPedState, iScenarioIndex, bSkipScenario)
				FMMC_PED_CUSTOM_SCENARIO_BLIND_EXPLOSIVE_CLEANUP(sPedState.pedIndex)
				bScenarioComplete = TRUE
			ENDIF
		BREAK
		DEFAULT
			ASSERTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] PROCESS_CLIENT_ASSOCIATED_GOTO_CUSTOM_SCENARIO - Unknown Scenario Type: ", eCustomScenarioType)
			PRINTLN("[CustomScenario][Peds][Ped ", sPedState.iIndex, "] PROCESS_CLIENT_ASSOCIATED_GOTO_CUSTOM_SCENARIO - Unknown Scenario Type: ", eCustomScenarioType)
			bScenarioComplete = TRUE
		BREAK
	ENDSWITCH
	
	IF bScenarioComplete
		RELEASE_PED_CUSTOM_SCENARIO_INDEX(sPedState.iIndex, iScenarioIndex)
	ENDIF
	
	RETURN bScenarioComplete
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Animation and Cutscene Functions --------------------------------------------------------------------------------------------------------------------------------
// ##### Description: This section should have the majority of helper functions that are used in multiple ped headers   ----------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_ANIMATION_INTERRUPTIBLE(INT iAnimation)
	
	BOOL bInterruptible = TRUE
	
	//Not for:
	SWITCH iAnimation
		CASE ciPED_IDLE_ANIM__BRIEFCASE
		CASE ciPED_IDLE_ANIM__PRISONER_BREAKOUT_COWER
		CASE ciPED_IDLE_ANIM__PRISONER_BREAKOUT_FREED
		CASE ciPED_IDLE_ANIM__KARENCODES_KARENIDLE
		CASE ciPED_IDLE_ANIM__KARENCODES_DRIVERIDLE
		CASE ciPED_IDLE_ANIM__PRISONBREAK_RASHKOVSKY_IDLE
		CASE ciPED_IDLE_ANIM__PRISONPLANE_AGENT_A_IDLE
		CASE ciPED_IDLE_ANIM__PRISONPLANE_AGENT_A_REACT
		CASE ciPED_IDLE_ANIM__PRISONPLANE_AGENT_B_IDLE
		CASE ciPED_IDLE_ANIM__PRISONPLANE_AGENT_B_REACT
		CASE ciPED_IDLE_ANIM__WITSEC_CHAIR_SNOOZE
		CASE ciPED_IDLE_ANIM__WITSEC_CHAIR_STAND_UP
		CASE ciPED_IDLE_ANIM__WITSEC_CHAIR_FALL
		CASE ciPED_IDLE_ANIM__UNFSHDBIZ_JUDGE_IDLE
		CASE ciPED_IDLE_ANIM__UNFSHDBIZ_JUDGE_BREAKOUT
		CASE ciPED_IDLE_ANIM__SIT_ON_FLOOR
		CASE ciPED_IDLE_ANIM__FLOOR_STAND_REACT
		CASE ciPED_IDLE_ANIM__HOSTAGE_1
		CASE ciPED_IDLE_ANIM__HOSTAGE_2
		CASE ciPED_IDLE_ANIM__HOSTAGE_3
		CASE ciPED_IDLE_ANIM__HOSTAGE_4
		CASE ciPED_IDLE_ANIM__HOSTAGE_5
		CASE ciPED_IDLE_ANIM__HOSTAGE_6
		CASE ciPED_IDLE_ANIM__HOSTAGE_7
		CASE ciPED_IDLE_ANIM__HOSTAGE_8
		CASE ciPED_IDLE_ANIM__HOSTAGE_9
		CASE ciPED_IDLE_ANIM__HEIST_TUT_LESTER_PHONE_INTRO
		CASE ciPED_IDLE_ANIM__HEIST_TUT_LESTER_PHONE_OUTRO
		CASE ciPED_IDLE_ANIM__STATION_DRUNK_LADY_REACT
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_1
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_2
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_3
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_4
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_5
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_6
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_7
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_8
		CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_1
		CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_2
		CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_3
		CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_4
		CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_5
		CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_6
		CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_7
		CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_8
		CASE ciPED_IDLE_ANIM__HANG_OUT_STREET
		CASE ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_PERP_IDLE
		CASE ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_VICTIM_IDLE
		CASE ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_PERP_EXECUTE
		CASE ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_VICTIM_FREED		
		CASE ciPED_IDLE_ANIM__GOLF_CLUB_INTERROGATION_FRANKLIN
		CASE ciPED_IDLE_ANIM__GOLF_CLUB_INTERROGATION_GOLFER
			bInterruptible = FALSE
		BREAK
	ENDSWITCH
	
	RETURN bInterruptible
	
ENDFUNC

FUNC BOOL DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(INT iAnimation)
	
	BOOL bSpecialBreakout
	
	SWITCH iAnimation
		CASE ciPED_IDLE_ANIM__WITSEC_CHAIR_SNOOZE
		CASE ciPED_IDLE_ANIM__UNFSHDBIZ_JUDGE_IDLE
		CASE ciPED_IDLE_ANIM__SIT_ON_FLOOR
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_BODHI_LOOP
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_1
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_2
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_3
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_4
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_5
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_6
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_7
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_8
		CASE ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_PERP_IDLE
		CASE ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_VICTIM_IDLE
			bSpecialBreakout = TRUE
		BREAK
	ENDSWITCH
	
	RETURN bSpecialBreakout
	
ENDFUNC

FUNC BOOL DOES_SPECIAL_ANIM_LOOP(INT iAnimation)
	
	BOOL bLooping = TRUE
	
	SWITCH iAnimation
		CASE ciPED_IDLE_ANIM__WITSEC_CHAIR_STAND_UP
		CASE ciPED_IDLE_ANIM__WITSEC_CHAIR_FALL
		CASE ciPED_IDLE_ANIM__UNFSHDBIZ_JUDGE_BREAKOUT
		CASE ciPED_IDLE_ANIM__FLOOR_STAND_REACT
		CASE ciPED_IDLE_ANIM__PRISONER_BREAKOUT_FREED
			bLooping = FALSE
		BREAK
	ENDSWITCH
	
	RETURN bLooping
	
ENDFUNC

FUNC BOOL SHOULD_SPECIAL_ANIM_CONTINUE_PAST_SET_RULE(INT iAnimation)
	
	SWITCH iAnimation
		CASE ciPED_IDLE_ANIM__GOLF_CLUB_INTERROGATION_FRANKLIN
		CASE ciPED_IDLE_ANIM__GOLF_CLUB_INTERROGATION_GOLFER
		CASE ciPED_IDLE_ANIM__JOHNNY_GUNS_WOUNDED
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL CAN_ANIMATION_PLAY_WHILE_GROUPED(INT iAnimation)
	
	BOOL bPlay
	
	SWITCH iAnimation
		CASE ciPED_IDLE_ANIM__HEIST_TUT_LESTER_PHONE_INTRO
		CASE ciPED_IDLE_ANIM__HEIST_TUT_LESTER_PHONE_OUTRO
		CASE ciPED_IDLE_ANIM__PRISONER_BREAKOUT_FREED
		CASE ciPED_IDLE_ANIM__SHOW_OFF_VEHICLE_HYDRAULICS
			bPlay = TRUE
		BREAK
	ENDSWITCH
	
	RETURN bPlay
	
ENDFUNC

FUNC BOOL CAN_ANIMATION_PLAY_IN_CAR(INT iAnimation)
	
	BOOL bPlay
	
	SWITCH iAnimation
		CASE ciPED_IDLE_ANIM__HEIST_TUT_LESTER_PHONE_INTRO
		CASE ciPED_IDLE_ANIM__HEIST_TUT_LESTER_PHONE_OUTRO
		CASE ciPED_IDLE_ANIM__SHOW_OFF_VEHICLE_HYDRAULICS
			bPlay = TRUE
		BREAK
	ENDSWITCH
	
	RETURN bPlay
	
ENDFUNC

FUNC BOOL IS_ANIMATION_A_COWER(INT iAnimation)
	
	BOOL bCower
	
	SWITCH iAnimation
		CASE ciPED_IDLE_ANIM__HOSTAGE_1
		CASE ciPED_IDLE_ANIM__HOSTAGE_2
		CASE ciPED_IDLE_ANIM__HOSTAGE_3
		CASE ciPED_IDLE_ANIM__HOSTAGE_4
		CASE ciPED_IDLE_ANIM__HOSTAGE_5
		CASE ciPED_IDLE_ANIM__HOSTAGE_6
		CASE ciPED_IDLE_ANIM__HOSTAGE_7
		CASE ciPED_IDLE_ANIM__HOSTAGE_8
		CASE ciPED_IDLE_ANIM__HOSTAGE_9
			bCower = TRUE
		BREAK
		
		CASE ciPED_IDLE_ANIM__GENERIC_COWER_NEUTRAL
		CASE ciPED_IDLE_ANIM__GENERIC_COWER_FRONT
		CASE ciPED_IDLE_ANIM__GENERIC_COWER_BACK
			bCower = TRUE
		BREAK
	ENDSWITCH
	
	RETURN bCower
	
ENDFUNC

FUNC INT GET_GO_FOUNDRY_VICTIM_PED(INT iPerpPed)
	
	INT iPedLoop
	
	INT iClosestPed = -1
	FLOAT fClosestDist2 = 100 // Must be within 10m, don't want to steal anybody else's hostage
	FLOAT fTempDist2
	VECTOR vPerpCoords = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPerpPed].vPos
	
	FOR iPedLoop = 0 TO (MC_serverBD.iNumPedCreated - 1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedLoop].iIdleAnim = ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_VICTIM_IDLE
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedLoop].vPos)
			
			fTempDist2 = VDIST2(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedLoop].vPos, vPerpCoords)
			
			IF fTempDist2 < fClosestDist2
				iClosestPed = iPedLoop
				fClosestDist2 = fTempDist2
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iClosestPed
	
ENDFUNC

FUNC BOOL SHOULD_SPECIAL_ANIM_TRIGGER_BASED_ON_RULE_DURATION(INT iTeam, INT iPed)
	IF iTeam > -1
	AND IS_TEAM_ACTIVE(iTeam)
	AND MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule > -1				
		//Always
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwentyOne, ciPED_BSTwentyOne_SpecialAnimAlwaysActive)			
			PRINTLN("SHOULD_SPECIAL_ANIM_TRIGGER_BASED_ON_RULE_DURATION - PED_", iPed, " - Special anim should trigger on this rule (always trigger)")
			RETURN TRUE
			
		//Before or on this rule
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwentyOne, ciPED_BSTwentyOne_SpecialAnimBeforeAndInclThisRule)
			IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule
				PRINTLN("SHOULD_SPECIAL_ANIM_TRIGGER_BASED_ON_RULE_DURATION - PED_", iPed, " - Special anim should trigger on this rule (before or on rule ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule, ")")
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
			
		//On or after this rule
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwentyOne, ciPED_BSTwentyOne_SpecialAnimAfterAndInclThisRule)
			IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule
				PRINTLN("SHOULD_SPECIAL_ANIM_TRIGGER_BASED_ON_RULE_DURATION - PED_", iPed, " - Special anim should trigger on this rule (on or after rule ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule, ")")
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		//Just on this rule	
		ELSE			
			IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule
				PRINTLN("SHOULD_SPECIAL_ANIM_TRIGGER_BASED_ON_RULE_DURATION - PED_", iPed, " - Special anim should trigger on this rule (just rule ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule, ")")
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_GO_FOUNDRY_PERP_PED(INT iVictimPed)
	
	INT iPedLoop
	
	INT iClosestPed = -1
	FLOAT fClosestDist2 = 100 // Must be within 10m, don't want to steal anybody else's perp
	FLOAT fTempDist2
	VECTOR vVictimCoords = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iVictimPed].vPos
	
	FOR iPedLoop = 0 TO (MC_serverBD.iNumPedCreated - 1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedLoop].iIdleAnim = ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_PERP_IDLE
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedLoop].vPos)
			
			fTempDist2 = VDIST2(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedLoop].vPos, vVictimCoords)
			
			IF fTempDist2 < fClosestDist2
				iClosestPed = iPedLoop
				fClosestDist2 = fTempDist2
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iClosestPed
	
ENDFUNC

FUNC BOOL IS_PED_AT_RIGHT_POINT_FOR_SPECIAL_ANIM(INT iped, PED_INDEX tempPed, BOOL bInVehicle = FALSE, BOOL bAtMidPoint = FALSE)
	
	BOOL bAtRightPoint = FALSE
	
	#IF IS_DEBUG_BUILD	
		//Make sure that the range teams are set if we're using the range check as the trigger for the special anim - url:bugstar:7346795
		IF 	g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim != -1
		AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fSpecialAnimTriggerRange < cfMAX_SPECIALANIM_TRIGGER_DIST
		AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimTriggerTeamBitset = 0
			ASSERTLN("IS_PED_AT_RIGHT_POINT_FOR_SPECIAL_ANIM - PED_", iped, " - Special Animation Range was set without Special Animation Range Teams. Please make sure that you select at least one team in Animations>Special Animations Range Teams or disable Animations>Special Animation Range")
			PRINTLN("IS_PED_AT_RIGHT_POINT_FOR_SPECIAL_ANIM - PED_", iped, " - Special Animation Range was set without Special Animation Range Teams. Please make sure that you select at least one team in Animations>Special Animations Range Teams or disable Animations>Special Animation Range")
		ENDIF
	#ENDIF
	
	IF SHOULD_SPECIAL_ANIM_TRIGGER_BASED_ON_RULE_DURATION(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimTeam, iPed)
	OR (DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim) AND (MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule))
	OR (SHOULD_SPECIAL_ANIM_CONTINUE_PAST_SET_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpecialAnim) AND MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwo,ciPED_BSTwo_SpecialAnim_On_Midpoint)
			IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimTeam],g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule)
			OR bAtMidPoint
			OR (MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimTeam] > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule)
				bAtRightPoint = TRUE
			ELSE
				//Hack to get the prisoner breakout animation triggering properly (the VIP can be picked up by either of two teams)
				IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim = ciPED_IDLE_ANIM__PRISONER_BREAKOUT_FREED)
				AND IS_PED_IN_GROUP(tempPed)
					bAtRightPoint = TRUE
				ENDIF
			ENDIF
		ELSE
			bAtRightPoint = TRUE
		ENDIF
	ENDIF
	
	IF bInVehicle
	AND NOT CAN_ANIMATION_PLAY_IN_CAR(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim)
		bAtRightPoint = FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpecialAnim = ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_VICTIM_FREED
		
		// Only play this once the perp holding this ped hostage is dead		
		INT iPerp = GET_GO_FOUNDRY_PERP_PED(iped)
		
		IF iPerp != -1
		AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPerp])
			
			PED_INDEX PerpPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPerp])
			
			IF IS_PED_INJURED(PerpPed)
				// The perp has been killed
				PRINTLN("[Peds][Ped ", iPed, "] IS_PED_AT_RIGHT_POINT_FOR_SPECIAL_ANIM - GO Foundry Hostage ",iped,"'s perp ",iPerp," is dead, return TRUE")
				bAtRightPoint = TRUE
			ELIF FMMC_IS_LONG_BIT_SET(ipedAnimBitset, iPerp)
			AND SHOULD_PED_BE_GIVEN_TASK(PerpPed, iPerp, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE)
				// The perp has broken out somehow - possibly set on fire on something
				PRINTLN("[Peds][Ped ", iPed, "] IS_PED_AT_RIGHT_POINT_FOR_SPECIAL_ANIM - GO Foundry Hostage ",iped,"'s perp ",iPerp," is no longer animating, return TRUE")
				bAtRightPoint = TRUE
			ENDIF
		ELSE
			PRINTLN("[Peds][Ped ", iPed, "] IS_PED_AT_RIGHT_POINT_FOR_SPECIAL_ANIM - GO Foundry Hostage ",iped,"'s perp ",iPerp," does not exist, return TRUE")
			bAtRightPoint = FALSE
		ENDIF
	ENDIF
	
	RETURN bAtRightPoint
	
ENDFUNC

//Checks if ped tasks shouldn't clear up due to a smooth animation transition
//bAtMidpoint is a bool that overrides the midpoint check - as sometimes the midpoint bitset gets set late
FUNC BOOL IS_PED_DOING_ANIMATION_TRANSITION(INT iped, PED_INDEX tempPed, BOOL bAtMidpoint = FALSE, BOOL bBusyReaction = FALSE)
	
	BOOL bIsTransition = FALSE
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim != ciPED_IDLE_ANIM__NONE
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim != ciPED_IDLE_ANIM__NONE
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule != -1
		
		INT iIdleAnim = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim
		INT iSpecialAnim = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim
		
		//Are these animations that should transition smoothly?
		IF ( (iIdleAnim = ciPED_IDLE_ANIM__PRISONER_BREAKOUT_COWER) AND (iSpecialAnim = ciPED_IDLE_ANIM__PRISONER_BREAKOUT_FREED) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__GUARD_WAVING_IDLE) AND ( (iSpecialAnim = ciPED_IDLE_ANIM__GUARD_WAVING_WAVE) OR (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim = ciPED_IDLE_ANIM__GUARD_WAVING_REACTINBOOTH) ) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__PRISONPLANE_AGENT_A_IDLE) AND (iSpecialAnim = ciPED_IDLE_ANIM__PRISONPLANE_AGENT_A_REACT) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__PRISONPLANE_AGENT_B_IDLE) AND (iSpecialAnim = ciPED_IDLE_ANIM__PRISONPLANE_AGENT_B_REACT) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__PRISONBREAK_BUS_GUARD_A_IDLE_CHAT) AND (iSpecialAnim = ciPED_IDLE_ANIM__PRISONBREAK_BUS_GUARD_A_REACT_TO_BUS) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__WITSEC_CHAIR_SNOOZE) AND ((iSpecialAnim = ciPED_IDLE_ANIM__WITSEC_CHAIR_FALL) OR (iSpecialAnim = ciPED_IDLE_ANIM__WITSEC_CHAIR_STAND_UP)) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__UNFSHDBIZ_JUDGE_IDLE) AND (iSpecialAnim = ciPED_IDLE_ANIM__UNFSHDBIZ_JUDGE_BREAKOUT) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__SIT_ON_FLOOR) AND (iSpecialAnim = ciPED_IDLE_ANIM__FLOOR_STAND_REACT) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__STATION_COP_BEHIND_DESK_IDLE) AND (iSpecialAnim = ciPED_IDLE_ANIM__STATION_COP_BEHIND_DESK_REACT ) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__STATION_COP_BESIDE_LADY_IDLE) AND (iSpecialAnim = ciPED_IDLE_ANIM__STATION_COP_BESIDE_LADY_REACT ) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__VALKYRIE_AGENT_IDLE) AND (iSpecialAnim = ciPED_IDLE_ANIM__VALKYRIE_AGENT_HANDOVER ) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_PERP_IDLE) AND (iSpecialAnim = ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_PERP_EXECUTE) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_VICTIM_IDLE) AND (iSpecialAnim = ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_VICTIM_FREED) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__CROUCH_AIM) AND (iSpecialAnim = ciPED_IDLE_ANIM__STAND_AIM) )
		
			//Check if team is just about to trigger the special
			IF SHOULD_SPECIAL_ANIM_TRIGGER_BASED_ON_RULE_DURATION(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimTeam, iPed)
				//If we haven't yet launched the special animation:
				IF NOT FMMC_IS_LONG_BIT_SET(iPedPerformingSpecialAnimBS, iPed)
					
					IF IS_PED_AT_RIGHT_POINT_FOR_SPECIAL_ANIM(iped,tempPed,DEFAULT,bAtMidPoint)
						
						//Are we just coming out of the idle:
						IF NOT ( IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_IdleAnimOnJustThisRule) OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_IdleAnimBeforeAndInclThisRule) OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_IdleAnimAfterAndInclThisRule) )
							//Idle anim always running
							bIsTransition = TRUE
						ELSE
							INT iRuleToCheck
							
							IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwo,ciPED_BSTwo_SpecialAnim_On_Midpoint)
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fSpecialAnimTriggerRange = cfMAX_SPECIALANIM_TRIGGER_DIST
									//if not triggering on midpoint, was the prev rule idle
									iRuleToCheck = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule - 1
								ELSE
									//Triggering on range, check this rule:
									iRuleToCheck = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule
								ENDIF
							ELSE
								//if triggering on midpoint, was this rule idle
								iRuleToCheck = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_IdleAnimOnJustThisRule)
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnimRule = iRuleToCheck
								AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpecialAnimTeam
									bIsTransition = TRUE
								ENDIF
							ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_IdleAnimBeforeAndInclThisRule)
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnimRule >= iRuleToCheck
								AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpecialAnimTeam
									bIsTransition = TRUE
								ENDIF
							ELSE
								//ciPED_BSTwo_IdleAnimBeforeAndInclThisRule:
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnimRule <= iRuleToCheck
								AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpecialAnimTeam
									bIsTransition = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELIF ( (iIdleAnim = ciPED_IDLE_ANIM__HEIST_TUT_LESTER_PHONE_INTRO) AND (iSpecialAnim = ciPED_IDLE_ANIM__HEIST_TUT_LESTER_PHONE_OUTRO) )
			//Special case, the idle task needs to last for a while after it gets called:
			
			//If we haven't yet launched the special animation:
			IF NOT FMMC_IS_LONG_BIT_SET(iPedPerformingSpecialAnimBS, iPed)
				INT iIdleTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnimTeam
				INT iIdleRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnimRule
				INT iSpecialTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimTeam
				INT iSpecialRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule
				
				IF iIdleTeam != -1
				AND iIdleRule != -1
				AND iSpecialTeam != -1
				AND iSpecialRule != -1
					IF MC_serverBD_4.iCurrentHighestPriority[iIdleTeam] >= iIdleRule
					AND SHOULD_SPECIAL_ANIM_TRIGGER_BASED_ON_RULE_DURATION(iSpecialTeam, iPed)
						//Should probably be more rigorous checks for animation order here, but not needed for this case (yet)
						bIsTransition = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bIsTransition
			IF ( NOT ( SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
			    AND SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE) )
				AND DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(iIdleAnim) )
		
				IF NOT FMMC_IS_LONG_BIT_SET(g_iFMMCPedAnimBreakoutBS, iped)			
					bIsTransition = TRUE			
				ENDIF
			ENDIF		
		ENDIF
		
	ENDIF
	
	IF bBusyReaction
		bIsTransition = TRUE
	ENDIF
	
	RETURN bIsTransition
	
ENDFUNC

FUNC BOOL IS_PED_IN_A_CUTSCENE(INT iped)
	
	BOOL bInCutscene
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpecialAnim = ciPED_IDLE_ANIM__PRISONER_BREAKOUT_FREED
		IF FMMC_IS_LONG_BIT_SET(iPedPerformingSpecialAnimBS, iPed)
			bInCutscene = TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim = ciPED_IDLE_ANIM__PRISONER_BREAKOUT_COWER
		IF FMMC_IS_LONG_BIT_SET(iPedAnimBitset, iped)
			bInCutscene = TRUE
		ENDIF
	ENDIF
	
	RETURN bInCutscene
	
ENDFUNC

FUNC BOOL IS_PED_ANIMATION_PLAYING(PED_INDEX tempPed, INT iped)
	
	BOOL bPlaying = FALSE

	IF (NOT SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE))
	OR (NOT SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE))
		bPlaying = TRUE
	ENDIF
	
	RETURN bPlaying
	
ENDFUNC

FUNC BOOL CHECK_PLAYERS_IN_RANGE_OF_PED(INT iPed, INT iRange)
	INT iPlayersToCheck, iPlayersChecked, i

	iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
	
	PED_INDEX pedToCheck
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iped])
		pedToCheck = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
	ELSE
		RETURN FALSE
	ENDIF

	PRINTLN("[PLAYER_LOOP] - CHECK_PLAYERS_IN_RANGE_OF_PED")
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
		IF NOT (IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)
		OR IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				iPlayersChecked++
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX( tempPart )
				
				IF IS_NET_PLAYER_OK(tempPlayer)
					PED_INDEX tempPed = GET_PLAYER_PED(tempPlayer)
					IF NOT IS_PED_INJURED(tempPed) 
						IF VDIST2(GET_ENTITY_COORDS(tempPed), GET_ENTITY_COORDS(pedToCheck)) < POW(TO_FLOAT(iRange),2)
							PRINTLN("[JS] CHECK_PLAYERS_IN_RANGE_OF_PED - part ",i," is in range of ped ", iPed)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
				IF iPlayersChecked >= iPlayersToCheck 
					BREAKLOOP
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

	RETURN FALSE
ENDFUNC

FUNC INT GET_CLOSEST_PART_TO_PED_FROM_TEAMS(PED_INDEX pedToCheck, INT iTeamsBS, FLOAT& fClosestDistance2)

	INT iClosestPart = -1
	FLOAT fClosestDist2 = 999999999 //Furthest distance apart on map is O(10,000)
	FLOAT fTempDist2
	
	INT iTeam
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		
		IF IS_BIT_SET(iTeamsBS,iTeam)
			INT iClosestPlyrOnTeam = GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(pedToCheck,iTeam)
			PLAYER_INDEX tempPlayer = INT_TO_PLAYERINDEX(iClosestPlyrOnTeam)
			IF (tempPlayer != INVALID_PLAYER_INDEX())
			AND IS_NET_PLAYER_OK(tempPlayer)
				fTempDist2 = VDIST2(GET_PLAYER_COORDS(tempPlayer),GET_ENTITY_COORDS(pedToCheck))
				IF (fTempDist2 < fClosestDist2)
					fClosestDist2 = fTempDist2
					iClosestPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(tempPlayer))
				ENDIF
			ENDIF
		ENDIF
		
	ENDFOR
	
	fClosestDistance2 = fClosestDist2
	
	RETURN iClosestPart
	
ENDFUNC

FUNC BOOL IS_USING_FRIENDLY_PEDS_NEARBY_FOR_ACTIVATION(INT iTaskTriggerFriendliesCount, FLOAT fTaskTriggerFriendliesRange)
	
	IF iTaskTriggerFriendliesCount > -1
	AND fTaskTriggerFriendliesRange > 0.0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ENOUGH_FRIENDLY_PEDS_NEARBY_FOR_ACTIVATION(FMMC_PED_STATE &sPedState, INT iTaskTriggerFriendliesCount, FLOAT fTaskTriggerFriendliesRange)
 
 	NETWORK_INDEX niPed
	PED_INDEX piPed
 	INT iCount
 	INT i
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1
		niPed = MC_serverBD_1.sFMMC_SBD.niPed[i]
		
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niPed)
			RELOOP
		ENDIF
		
		piPed = NET_TO_PED(niPed)
		
		IF IS_PED_INJURED(piPed)
			RELOOP
		ENDIF
		
		IF piPed = sPedState.pedIndex
			RELOOP
		ENDIF
		
		IF VDIST2(GET_FMMC_PED_COORDS(sPedState), GET_ENTITY_COORDS(piPed, FALSE))	< POW(fTaskTriggerFriendliesRange, 2.0)
			iCount++
		ENDIF
		
		IF iCount > iTaskTriggerFriendliesCount 
			RETURN FALSE
		ENDIF
	
	ENDFOR
	
	IF iCount <= iTaskTriggerFriendliesCount 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_PROCESS_PED_IDLE_ANIMATION(FMMC_PED_STATE &sPedState)
	
	IF FMMC_IS_LONG_BIT_SET(iPedShouldProcessIdleAnim_CheckedThisFrame, sPedstate.iIndex)
		RETURN FMMC_IS_LONG_BIT_SET(iPedShouldProcessIdleAnim_ResultThisFrame, sPedstate.iIndex)
	ENDIF
	
	IF IS_USING_FRIENDLY_PEDS_NEARBY_FOR_ACTIVATION(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iIdleAnimTriggerFriendliesCount, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].fIdleAnimTriggerFriendliesRange)
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - SHOULD_PROCESS_PED_IDLE_ANIMATION - Ped is using logic to check how many nearby friendly peds we have.")
		IF NOT ARE_ENOUGH_FRIENDLY_PEDS_NEARBY_FOR_ACTIVATION(sPedState, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iIdleAnimTriggerFriendliesCount, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].fIdleAnimTriggerFriendliesRange)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - SHOULD_PROCESS_PED_IDLE_ANIMATION - Not enough peds in friendly range. Returning False")			
			FMMC_SET_LONG_BIT(iPedShouldProcessIdleAnim_CheckedThisFrame, sPedstate.iIndex)
			RETURN FALSE
		ENDIF
	ENDIF
	
	FMMC_SET_LONG_BIT(iPedShouldProcessIdleAnim_CheckedThisFrame, sPedstate.iIndex)
	FMMC_SET_LONG_BIT(iPedShouldProcessIdleAnim_ResultThisFrame, sPedstate.iIndex)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_PROCESS_PED_SPECIAL_ANIMATION(FMMC_PED_STATE &sPedState)
	
	IF FMMC_IS_LONG_BIT_SET(iPedShouldProcessSpecialAnim_CheckedThisFrame, sPedstate.iIndex)
		RETURN FMMC_IS_LONG_BIT_SET(iPedShouldProcessSpecialAnim_ResultThisFrame, sPedstate.iIndex)
	ENDIF
	
	IF IS_USING_FRIENDLY_PEDS_NEARBY_FOR_ACTIVATION(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iSpecialAnimTriggerFriendliesCount, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].fSpecialAnimTriggerFriendliesRange)
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - SHOULD_PROCESS_PED_SPECIAL_ANIMATION - Ped is using logic to check how many nearby friendly peds we have.")
		IF NOT ARE_ENOUGH_FRIENDLY_PEDS_NEARBY_FOR_ACTIVATION(sPedState, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iSpecialAnimTriggerFriendliesCount, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].fSpecialAnimTriggerFriendliesRange)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - SHOULD_PROCESS_PED_SPECIAL_ANIMATION - Not enough peds in friendly range. Returning False")
			FMMC_SET_LONG_BIT(iPedShouldProcessSpecialAnim_CheckedThisFrame, sPedstate.iIndex)
			RETURN FALSE
		ENDIF
	ENDIF
	
	FMMC_SET_LONG_BIT(iPedShouldProcessSpecialAnim_CheckedThisFrame, sPedstate.iIndex)
	FMMC_SET_LONG_BIT(iPedShouldProcessSpecialAnim_ResultThisFrame, sPedstate.iIndex)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(FMMC_PED_STATE &sPedState, BOOL bCalledFromCombat = FALSE #IF IS_DEBUG_BUILD , INT iDebugCall = -1 #ENDIF)
	
	PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] - iDebugCall: ", iDebugCall, " --------------------")
	
	INT iPed = sPedState.iIndex
	
	// This function is called in places where I couldn't pass in a tempPed or ThisPed reference so I had to create a reference to the ped here.
	IF sPedState.bExists
		BOOL bReady = FALSE
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPed_BSEleven_PrioritizeSpecialAnimOverTaskForRule)					
			INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpecialAnimTeam
			
			PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] - iPed: ", iPed, " ciPed_BSEleven_PrioritizeSpecialAnimOverTaskForRule Set. Checking Rule & Teams to see if Anim should take priority. iSpecialAnimTeam: ", iTeam)
			
			IF iTeam > -1
				IF IS_TEAM_ACTIVE(iTeam)
				AND MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
				AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpecialAnimRule > -1
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo, ciPED_BSTwo_SpecialAnim_On_Midpoint)
						IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], MC_ServerBD_4.iCurrentHighestPriority[iTeam])
							bReady = TRUE
						ENDIF
					ELSE
						bReady = TRUE
					ENDIF
					
					IF bCalledFromCombat
						IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThirteen, ciPed_BSThirteen_SpecialAnimPrioritizedOverCombat)
							PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] - iPed: ", iPed, " Called from Combat - ciPed_BSThirteen_SpecialAnimPrioritizedOverCombat NOT Set. bReady = FALSE")
							bReady = FALSE
						ELSE
							PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] - iPed: ", iPed, " Called from Combat - ciPed_BSThirteen_SpecialAnimPrioritizedOverCombat Set. Should be ready.")
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncSceneAnimationFinished)
						bReady = FALSE
					ENDIF
					
					IF NOT SHOULD_PROCESS_PED_SPECIAL_ANIMATION(sPedState)
						bReady = FALSE
						PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] - iPed: ", iPed, " SHOULD_PROCESS_PED_SPECIAL_ANIMATION is false. bReady = FALSE.")
					ENDIF
		
					IF bReady
						IF SHOULD_SPECIAL_ANIM_TRIGGER_BASED_ON_RULE_DURATION(iTeam, iPed)
							IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fSpecialAnimTriggerRange < cfMAX_SPECIALANIM_TRIGGER_DIST
								
								// Copied from start - PROCESS_PED_ANIMATION
								FLOAT fClosestDistance2
								INT iClosestPart
								
								iClosestPart = GET_CLOSEST_PART_TO_PED_FROM_TEAMS(sPedState.pedIndex, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimTriggerTeamBitset, fClosestDistance2)
								
								IF iClosestPart != -1								
									IF (fClosestDistance2 <= ((g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fSpecialAnimTriggerRange)*(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fSpecialAnimTriggerRange)) )
										PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] iPed: ", iPed, " Returning TRUE iSpecialAnimRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpecialAnimRule, " = iCurrentHighestPriority of iTeam: ", iTeam)
										RETURN TRUE
									ENDIF
								ENDIF
								// Copied from end.
							ELSE
								PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] iPed: ", iPed, " Returning TRUE iSpecialAnimRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpecialAnimRule, " = iCurrentHighestPriority of iTeam: ", iTeam)
								RETURN TRUE
							ENDIF
						ENDIF			
					ENDIF
					
					IF SHOULD_SPECIAL_ANIM_CONTINUE_PAST_SET_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim)
					AND MC_ServerBD_4.iCurrentHighestPriority[iTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule
						PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] iPed: ", iPed, " Returning TRUE iSpecialAnimRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule, " due to SHOULD_SPECIAL_ANIM_CONTINUE_PAST_SET_RULE")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPed_BSEleven_PrioritizeIdleAnimOverTaskForRule)				
			INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimTeam
			
			PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] iPed: ", iPed, " ciPed_BSEleven_PrioritizeIdleAnimOverTaskForRule Set. Checking Rule & Teams to see if Anim should take priority - iIdleAnimTeam: ", iTeam)
			
			IF iTeam > -1
				IF IS_TEAM_ACTIVE(iTeam)
				AND MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo, ciPED_BSTwo_IdleAnim_On_Midpoint)
						IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], MC_ServerBD_4.iCurrentHighestPriority[iTeam])
							bReady = TRUE
						ENDIF
					ELSE
						bReady = TRUE
					ENDIF
					
					IF bCalledFromCombat
						IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThirteen, ciPed_BSThirteen_IdleAnimPrioritizedOverCombat)							
							PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] - iPed: ", iPed, " Called from Combat - ciPed_BSThirteen_IdleAnimPrioritizedOverCombat NOT Set. bReady = FALSE")
							bReady = FALSE
						ELSE
							PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] - iPed: ", iPed, " Called from Combat - ciPed_BSThirteen_IdleAnimPrioritizedOverCombat Set. Should be ready.")
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncSceneAnimationFinished)
						bReady = FALSE
					ENDIF
									
					IF NOT SHOULD_PROCESS_PED_IDLE_ANIMATION(sPedState)
						PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] - iPed: ", iPed, " SHOULD_PROCESS_PED_IDLE_ANIMATION is false. bReady = FALSE.")
						bReady = FALSE
					ENDIF
							
					IF bReady		
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo, ciPED_BSTwo_IdleAnimOnJustThisRule)
							IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimRule					
								PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] iPed: ", iPed, " Returning TRUE iIdleAnimRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimRule, " = iCurrentHighestPriority of iTeam: ", iTeam)
								RETURN TRUE
							ENDIF
						ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo, ciPED_BSTwo_IdleAnimBeforeAndInclThisRule)
							IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimRule
								PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] iPed: ", iPed, " Returning TRUE iIdleAnimRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimRule, " <= iCurrentHighestPriority of iTeam: ", iTeam)
								RETURN TRUE
							ENDIF
						ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo, ciPED_BSTwo_IdleAnimAfterAndInclThisRule)
							IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimRule
								PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] iPed: ", iPed, " Returning TRUE iIdleAnimRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimRule, " >= iCurrentHighestPriority of iTeam: ", iTeam)
								RETURN TRUE
							ENDIF
						ELSE
							PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] iPed: ", iPed, " Returning TRUE iIdleAnimRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimRule, " >= iCurrentHighestPriority of iTeam: ", iTeam)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Animals --------------------------------------------------------------------------------------------------------------------------
// ##### Description: This section should have the majority of helper functions that are used in multiple ped headers   ----------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_ANIMAL_PED_LOGIC(FMMC_PED_STATE &sPedState)

	IF NOT MC_IS_PED_AN_ANIMAL(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn)
		EXIT
	ENDIF

	IF sPedState.bIsInAnyVehicle
		SET_PED_RESET_FLAG(sPedState.pedIndex, PRF_CannotBeTargetedByAI, TRUE)
		
		IF NOT IS_PED_PERFORMING_TASK(sPedState.pedIndex, SCRIPT_TASK_PLAY_ANIM)		
			REQUEST_ANIM_DICT(GET_ANIMAL_SIT_IN_VEHICLE_DICT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn))	
			IF HAS_ANIM_DICT_LOADED(GET_ANIMAL_SIT_IN_VEHICLE_DICT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn))			
				TASK_PLAY_ANIM(sPedState.pedIndex, GET_ANIMAL_SIT_IN_VEHICLE_DICT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn), GET_ANIMAL_SIT_IN_VEHICLE_ANIM(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)		
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - PROCESS_ANIMAL_PED_LOGIC - Re-tasking vehicle animation for animal ped.")
			ELSE
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - PROCESS_ANIMAL_PED_LOGIC - WAITING FOR ANIM DICT TO LOAD!!!")
			ENDIF
		ENDIF
	ELSE
		SET_PED_RESET_FLAG(sPedState.pedIndex, PRF_CannotBeTargetedByAI, FALSE)
	ENDIF
		
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Helper Functions and Utility Functions --------------------------------------------------------------------------------------------------------------------------
// ##### Description: This section should have the majority of helper functions that are used in multiple ped headers   ----------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE: Set the Peds's state
//PARAMS: Use iOption if there are multiple places this state change can be called from and you want to debug which is happening
PROC SET_PED_STATE(INT iPedID, INT iPedState #IF IS_DEBUG_BUILD, INT iOption = -1 #ENDIF )

	#IF IS_DEBUG_BUILD
		IF iPedState != MC_serverBD_2.iPedState[iPedID]
			TEXT_LABEL_63 l_sDebug
			TEXT_LABEL_63 l_sState
			
			INT iOldState = MC_serverBD_2.iPedState[iPedID]
			
			l_sDebug += "[PedTask: "
			l_sDebug += GET_STRING_FROM_INT(iPedID)
			l_sDebug += "][SERVER] - SET_PED_STATE "
			l_sDebug += " "
			l_sState = GET_PED_STATE_NAME(iOldState)
			l_sDebug += l_sState
			l_sDebug += ">"
			l_sState = GET_PED_STATE_NAME(iPedState)
			l_sDebug += l_sState
			
			IF iOption != -1
				l_sDebug += " nr"
				l_sDebug += GET_STRING_FROM_INT(iOption)
			ENDIF
			
			NET_PRINT(l_sDebug) NET_NL()
		ENDIF
	#ENDIF
	
	MC_serverBD_2.iPedState[iPedID] = iPedState

ENDPROC

PROC INIT_BOAT(VEHICLE_INDEX tempVeh,INT iveh,BOOL bGoto = FALSE)

	IF bGoto
		SET_ENTITY_REQUIRES_MORE_EXPENSIVE_RIVER_CHECK(tempVeh, TRUE)
	ENDIF
	
	LOAD_COLLISION_ON_VEH_IF_POSSIBLE(tempVeh,iveh,bGoto)
	
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVeh, TRUE)
	SET_BOAT_LOW_LOD_ANCHOR_DISTANCE(tempVeh,99999.0)
	SET_BOAT_ANCHOR(tempVeh,FALSE)
	SET_ENTITY_DYNAMIC(tempVeh, TRUE)
	ACTIVATE_PHYSICS(tempVeh)
	SET_VEHICLE_ENGINE_ON(tempVeh, TRUE,TRUE)
	PRINTLN("init boat called")
				
ENDPROC

PROC INIT_PLANE(VEHICLE_INDEX tempVeh,INT iveh, BOOL bGoto= FALSE)
	IF NOT IS_ENTITY_IN_AIR(tempVeh)
		LOAD_COLLISION_ON_VEH_IF_POSSIBLE(tempVeh,iveh,bGoto)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVeh, TRUE)
	ELSE
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVeh, FALSE)
	ENDIF
	SET_ENTITY_DYNAMIC(tempVeh, TRUE)
	ACTIVATE_PHYSICS(tempVeh)
	SET_VEHICLE_ENGINE_ON(tempVeh, TRUE,TRUE)
	SET_HELI_BLADES_FULL_SPEED(tempVeh)
	PRINTLN("init plane called")

ENDPROC

FUNC BOOL HAS_AIRCRAFT_LANDED(VEHICLE_INDEX tempVeh)
	
	// This native sometimes returns a false "false" when in the air on helis.
	//PRINTLN("HAS_AIRCRAFT_LANDED - IS_ENTITY_IN_AIR: ", IS_ENTITY_IN_AIR(tempVeh))

	IF GET_ENTITY_SPEED(tempVeh) < 2.0
	AND (NOT IS_ENTITY_IN_AIR(tempVeh)
	AND GET_PLANE_ALTITUDE(tempVeh) < 2.0)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_VEHICLE_FRONT_COORD( VEHICLE_INDEX veh )
	VECTOR vPos, vDimMin, vDimMax, vForward
	IF( IS_ENTITY_ALIVE( veh ) )
		GET_MODEL_DIMENSIONS( GET_ENTITY_MODEL( veh ), vDimMin, vDimMax )
		vForward 	= GET_ENTITY_FORWARD_VECTOR( veh )
		vPos 		= GET_ENTITY_COORDS( veh, FALSE )
		vPos 		= vPos + ( vForward * -vDimMin.y )
	ENDIF
	RETURN vPos
ENDFUNC

FUNC BOOL WAS_THIS_VEHICLE_ON_A_KILL_RULE(INT iVeh, INT iTeam)
	
	BOOL bWasOnKillRule = FALSE
	BOOl bCheckExtraObjectives = FALSE
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iPriority[iTeam] < FMMC_MAX_RULES
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
		IF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iPriority[iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam])
			IF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_KILL)
				bWasOnKillRule = TRUE
			ELSE
				bCheckExtraObjectives = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bCheckExtraObjectives
		INT iextraObjectiveNum
		// Loop through all potential entites that have multiple rules
		FOR iextraObjectiveNum = 0 TO ( MAX_NUM_EXTRA_OBJECTIVE_ENTITIES-1 )
			// If the type matches the one passed
			IF g_FMMC_STRUCT.iExtraObjectiveEntityType[iextraObjectiveNum] = ciRULE_TYPE_VEHICLE
			AND g_FMMC_STRUCT.iExtraObjectiveEntityId[iextraObjectiveNum] = iVeh
				INT iextraObjectiveLoop
				
				FOR iextraObjectiveLoop = 0 TO (MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY - 1)
					IF g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam] < FMMC_MAX_RULES
					AND (GET_FMMC_RULE_FROM_CREATOR_RULE(g_FMMC_STRUCT.iExtraObjectiveRule[iextraObjectiveNum][iextraObjectiveLoop][iTeam]) != FMMC_OBJECTIVE_LOGIC_NONE)
						
						IF (g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam])
							IF (GET_FMMC_RULE_FROM_CREATOR_RULE(g_FMMC_STRUCT.iExtraObjectiveRule[iextraObjectiveNum][iextraObjectiveLoop][iTeam]) = FMMC_OBJECTIVE_LOGIC_KILL)
								bWasOnKillRule = TRUE
								iextraObjectiveLoop = MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY //Break out
							ENDIF
						ELSE
							iextraObjectiveLoop = MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY //Break out
						ENDIF
						
					ENDIF
				ENDFOR
				//We found the multi-rule number for this entity, there won't be any more so break out of the loop:
				iextraObjectiveNum = MAX_NUM_EXTRA_OBJECTIVE_ENTITIES
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN bWasOnKillRule
	
ENDFUNC

FUNC BOOL IS_PED_ARMED_BY_FMMC(INT iped, BOOL bGunOnRuleOverride = FALSE)
	
	BOOL bArmed = FALSE
	
	IF (GET_PED_WEAPON_TYPE_FROM_CREATOR_OPTION(iPed) != WEAPONTYPE_UNARMED AND GET_PED_WEAPON_TYPE_FROM_CREATOR_OPTION(iPed) != WEAPONTYPE_INVALID)
		//The ped will be armed straight awway
		bArmed = TRUE
	ENDIF
	
	IF NOT bArmed
		IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iGunOnRuleTeam != -1)
		AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iGunOnRuleRule != -1)
		AND ((g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].wtGunOnRule != WEAPONTYPE_UNARMED) AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].wtGunOnRule != WEAPONTYPE_INVALID))
			IF bGunOnRuleOverride
				bArmed = TRUE
			ELSE
				IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedGunOnRuleGiven, iped)
					bArmed = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bArmed
	
ENDFUNC

FUNC BOOL GET_PED_HAS_FREE_MOVEMENT_IN_COMBAT(INT iPed)
	
	IF FMMC_IS_LONG_BIT_SET(iPedZoneAggroed, iPed)
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFourteen, ciPed_BSFourteen_RemoveDefensiveAreaOnCombat)
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Team != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Rule != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_NewStyle != -1
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedCombatStyleChanged, iped)
			RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPED_BSEleven_CombatStyleChange_FreeMovementInCombat)
		ENDIF
	ENDIF
	
	RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_FreeMovementInCombat)
	
ENDFUNC

FUNC INT GET_PED_COMBAT_STYLE(INT iped)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Team != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Rule != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_NewStyle != -1
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedCombatStyleChanged, iped)
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_NewStyle
		ENDIF
	ENDIF
	
	RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyle
	
ENDFUNC

FUNC BOOL HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(INT iPed, PED_INDEX piPed, ENTITY_INDEX eiEntity, INT iLOSFlags = SCRIPT_INCLUDE_ALL)
	
	IF NOT IS_ENTITY_ALIVE(piPed)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFourteen, ciPED_BSFourteen_IgnoreCoverForLOS)
		RETURN HAS_ENTITY_CLEAR_LOS_TO_ENTITY(piPed, eiEntity, iLOSFlags)
	ELSE
		RETURN HAS_ENTITY_CLEAR_LOS_TO_ENTITY_ADJUST_FOR_COVER(piPed, eiEntity, iLOSFlags)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PED_SPAWN_DELAY_EXPIRED(INT iped)
	
	BOOL bExpired
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iRespawnDelay != 0
	AND (FMMC_IS_LONG_BIT_SET(MC_serverBD.iObjSpawnPedBitset, iPed) OR MC_ARE_SPAWN_CONDITION_FLAGS_MET_DURING_GAMEPLAY(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].eSpawnConditionFlag) OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwentyOne, ciPed_BSTwentyOne_AlwaysUseRespawnDelay))
		IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_3.iPedSpawnDelay[iped])
			MC_REINIT_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_3.iPedSpawnDelay[iped])
		ELSE
			INT iTimeScale = 1000
			IF MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_3.iPedSpawnDelay[iped]) >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iRespawnDelay*iTimeScale
				bExpired = TRUE
			ENDIF
		ENDIF
	ELSE
		bExpired = TRUE
	ENDIF
	
	RETURN bExpired
	
ENDFUNC

FUNC BOOL IS_TARGET_BEHIND_ENTITY(ENTITY_INDEX ent, ENTITY_INDEX target, FLOAT fAngleThatCountsAsBehind = 90.0)
	VECTOR entDir = GET_ENTITY_FORWARD_VECTOR(ent)
	entDir.z = 0
	entDir = NORMALISE_VECTOR(entDir)
	VECTOR targetDir = GET_ENTITY_COORDS(target) - GET_ENTITY_COORDS(ent)
	targetDir.z = 0
	targetDir = NORMALISE_VECTOR(targetDir)
	FLOAT direction = ATAN2(entDir.y, entDir.x) - ATAN2(targetDir.y, targetDir.x)
	IF direction < 0.0
		direction += 360.0
	ENDIF
	IF direction < 180.0
		direction = 180.0 - direction
	ELSE
		direction = direction - 180.0
	ENDIF
	RETURN direction < fAngleThatCountsAsBehind
ENDFUNC

FUNC BOOL IS_ENTITY_IN_CENTER_ANGLE(PED_INDEX pedId, ENTITY_INDEX entId)
	IF IS_ENTITY_ALIVE(pedId)
		VECTOR vToTrail = NORMALISE_VECTOR((GET_ENTITY_COORDS(entId) - GET_ENTITY_COORDS(pedId)))
		FLOAT fDot = DOT_PRODUCT(GET_ENTITY_FORWARD_VECTOR(pedId), vToTrail)
		FLOAT fCenterAngle
		fCenterAngle = DEG_TO_RAD(GET_PED_VISUAL_FIELD_CENTER_ANGLE(pedId))
		RETURN fDot >= 1-fCenterAngle
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PED_FOLLOWING_ANYONE(INT iped)
	INT ipart	

	PRINTLN("[PLAYER_LOOP] - IS_PED_FOLLOWING_ANYONE")
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() ipart
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(ipart))
			IF FMMC_IS_LONG_BIT_SET(MC_playerBD[ipart].iPedFollowingBitset, iped)
				
				#IF IS_DEBUG_BUILD
				IF bAddToGroupPrints
					PRINTLN("[Peds][Ped ", iPed, "] IS_PED_FOLLOWING_ANYONE / bAddToGroupPrints - found to be following part ",ipart)
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC

FUNC INT GET_NUMBER_PEDS_FOLLOWING_YOU() // FMMC2020_Refactor (shouldn't need this big loop)

	INT iped	
	INT ipedcount	

	FOR iped = 0 TO (MC_serverBD.iNumPedCreated - 1)
		IF FMMC_IS_LONG_BIT_SET(MC_playerBD[iPartToUse].iPedFollowingBitset, iped)
			ipedcount++
		ENDIF
	ENDFOR
	
	RETURN ipedcount
	
ENDFUNC

FUNC PED_INDEX GET_CLOSEST_PLAYER_PED_TO_THIS_PED(PED_INDEX tempPed)

	INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(tempPed)
	PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
	 
	IF IS_NET_PLAYER_OK(ClosestPlayer)		
		RETURN GET_PLAYER_PED(ClosestPlayer)
	ENDIF
	
	RETURN NULL
	
ENDFUNC

		
FUNC PED_INDEX GET_CLOSEST_VISIBLE_PLAYER(INT iPed, PED_INDEX tempPed, BOOL bIgnorePerceptionCheck = FALSE, FLOAT maxDistance = 30.0, BOOL bIgnoreLOSCheck = FALSE)

	INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(tempPed)
	PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
	 
	IF IS_NET_PLAYER_OK(ClosestPlayer)
		
		PED_INDEX ClosestPlayerPed = GET_PLAYER_PED(ClosestPlayer)
		
		IF GET_DISTANCE_BETWEEN_ENTITIES(tempPed,ClosestPlayerPed) < maxDistance
		AND (bIgnorePerceptionCheck OR
			IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,ClosestPlayerPed)) //added a perception check so peds don't head-track the player while sneaking up for a stealth kill
		AND (bIgnoreLOSCheck OR
			HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed,ClosestPlayerPed,SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE))
			RETURN ClosestPlayerPed
		ENDIF
	ENDIF
	RETURN NULL
ENDFUNC

FUNC BOOL SHOULD_PED_SHUFFLE_TO_TURRET_SEAT_FROM_BEING_ALERTED(INT iPed, PED_INDEX ThisPed)
	RETURN ((IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetNine, ciPed_BSNine_Shuffle_To_Turret_On_Alert))
	AND (FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedBitset, iPed) OR IS_PED_IN_COMBAT(ThisPed)))	
ENDFUNC
	
FUNC BOOL SHOULD_PED_SPEED_UP_FROM_BEING_ALERTED(INT iPed, PED_INDEX ThisPed) 
	RETURN ((IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetNine, ciPed_BSNine_Speed_Override_On_Spooked_Alerted))
	AND (FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedBitset, iPed) OR IS_PED_IN_COMBAT(ThisPed)))
ENDFUNC

FUNC BOOL IS_ANY_PED_BEING_CAPTURED() // FMMC2020_Refactor (shouldn't need this big loop)
	INT iped
	FOR iped = 0 TO FMMC_MAX_PEDS - 1
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iCapturePedRequestPartBS, iped)
			RETURN TRUE
		ENDIF
	ENDFOR	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PED_FLICKER(INT iPed)

	PED_INDEX piCurPed = NET_TO_PED(mc_serverBD_1.sFMMC_SBD.niPed[iPed])
	INT iPedHealth = GET_ENTITY_HEALTH(piCurPed)
	INT iPedMaxHealth = GET_ENTITY_MAX_HEALTH(piCurPed)
	
	FLOAT fPedHealthPercentage = (TO_FLOAT(iPedHealth) / TO_FLOAT(iPedMaxHealth)) * 100
	
	SET_RANDOM_SEED(NATIVE_TO_INT(GET_NETWORK_TIME()))
	FLOAT fRand = GET_RANDOM_FLOAT_IN_RANGE(0.0, 100.0)
	
	PRINTLN("[FlickeringPeds] HP = ", iPedHealth, " | MAX HP: ", iPedMaxHealth, " | Ped ", iPed)
	PRINTLN("[FlickeringPeds] fRand = ", fRand, " | Health Percentage: ", fPedHealthPercentage, " | Ped ", iPed)
	
	IF fPedHealthPercentage > 75.0
		PRINTLN("[FlickeringPeds] Returning FALSE because health is larger than 75% | Ped ", iPed)
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlickeringInvisiblePedEffects[iPed])
			REMOVE_PARTICLE_FX(ptfxFlickeringInvisiblePedEffects[iPed])
			PRINTLN("[FlickeringPeds] - removing particles for ped ", iPed)
		ENDIF
		
		RETURN FALSE
	ELSE
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_xm_heat")
		AND NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlickeringInvisiblePedEffects[iPed])
			USE_PARTICLE_FX_ASSET("scr_xm_heat")
			
			ptfxFlickeringInvisiblePedEffects[iPed] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_xm_heat_camo", piCurPed, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>)
			PRINTLN("[FlickeringPeds] - starting particles for ped ", iPed)
		ENDIF
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlickeringInvisiblePedEffects[iPed])
			PRINTLN("[FlickeringPeds] - flicker particles already exist for ped ", iPed)
		ENDIF
	ENDIF
	
	IF fPedHealthPercentage <= 25.0
		PRINTLN("[FlickeringPeds] Returning TRUE because health is less than 25% | Ped ", iPed)
		RETURN TRUE
	ENDIF
	
	RETURN fRand > fPedHealthPercentage
ENDFUNC

FUNC BOOL DOES_PED_CARE_IF_THIS_PLAYER_IS_HACKING_SOMETHING(PLAYER_INDEX piPlayer, INT iPed)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_SpookOnHack)
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)	
		PARTICIPANT_INDEX piPart = NETWORK_GET_PARTICIPANT_INDEX(piPlayer)
		INT iPart = NATIVE_TO_INT(piPart)
		
		IF iPart != -1
			IF MC_playerBD[iPart].iObjHacking > -1
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL WAS_PED_NEARLY_RUN_OVER(PED_INDEX tempPed)

	ENTITY_INDEX tempEnt
	VEHICLE_INDEX targetVeh
	PED_INDEX targetPed

	IF IS_PED_EVASIVE_DIVING(tempPed,tempEnt)
		IF DOES_ENTITY_EXIST(tempEnt)
			IF IS_ENTITY_A_VEHICLE(tempEnt)
				targetVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(tempEnt)
				IF IS_VEHICLE_DRIVEABLE(targetVeh)
					targetPed = GET_PED_IN_VEHICLE_SEAT(targetVeh)
				ENDIF
			ELSE
				//Bumping into a ped gets them into combat!
				targetPed = GET_PED_INDEX_FROM_ENTITY_INDEX(tempEnt)
			ENDIF
			IF NOT IS_PED_INJURED(targetPed)
				IF IS_PED_A_PLAYER(targetPed)
				AND (GET_PED_GROUP_INDEX(tempPed) != GET_PED_GROUP_INDEX(targetPed))
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

// fmmc2020 - Refactor this, it's VERY expensive. Every ped on a gotolocate potentially calling it. (TASK_PED_GOTO_ANY_MEANS)
FUNC BOOL IS_ANY_PED_ENTERING_VEHICLE(VEHICLE_INDEX tempveh)

	INT iped
	PED_INDEX tempPed

	IF GET_ENTITY_SPEED(tempveh) < 5.0
		FOR iped = 0 TO (MC_serverBD.iNumPedCreated - 1)
			IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
				tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
				IF GET_VEHICLE_PED_IS_ENTERING(tempPed) = tempveh
					PRINTLN("[Peds][Ped ", iPed, "] - [IS_ANY_PED_ENTERING_VEHICLE] - ped ", iPed, " is trying to enter vehicle.")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF

	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PED_ON_A_RULE_ON_WHICH_THEY_SHOULD_JOIN_MY_PLAYER_GROUP(INT iped)
	
	BOOL bJoinGroup = FALSE
	INT iTeam = MC_playerBD[iPartToUse].iteam
	
	IF (MC_serverBD_4.iPedPriority[iped][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam])
	AND (MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES)
		SWITCH MC_serverBD_4.iPedRule[iped][iTeam]
			CASE FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
			CASE FMMC_OBJECTIVE_LOGIC_CAPTURE
				bJoinGroup = TRUE
			BREAK
			
			CASE FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
				IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAtYourHolding[iTeam], iped)
					bJoinGroup = TRUE
				ENDIF
			BREAK
			
			CASE FMMC_OBJECTIVE_LOGIC_GO_TO
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFollow = ciPED_FOLLOW_ON
					bJoinGroup = TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN bJoinGroup
	
ENDFUNC

FUNC BOOL IS_PED_UNFRIENDLY_TO_ANY_TEAM(INT iped)
	
	BOOL bUnfriendly
	
	INT iTeam
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[iTeam] != ciPED_RELATION_SHIP_LIKE
			bUnfriendly = TRUE
			iTeam = MC_serverBD.iNumberOfTeams // Break out!
		ENDIF
	ENDFOR
	
	RETURN bUnfriendly
	
ENDFUNC

FUNC INT GET_PED_PREFERRED_SEAT_FROM_CREATOR_OPTION_INT(INT iPed)
	
	INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iVehicleSeatPreferenceTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	INT iSeat = ciFMMC_SeatPreference_None
	
	IF iRule >= FMMC_MAX_RULES
	OR iRule < 0
		RETURN iSeat		
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iVehicleSeatPreferenceRule = 0
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iVehicleSeatPreferenceRule, iRule)
	
		iSeat = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iVehicleSeatPreference
	
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAltVehicleSeatPreferenceRule = 0
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAltVehicleSeatPreferenceRule, iRule)
	
		iSeat = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAltVehicleSeatPreference
	
	ENDIF
	
	RETURN iSeat
ENDFUNC

FUNC VEHICLE_SEAT GET_PED_PREFERRED_SEAT_FROM_CREATOR(INT iVehicleSeatPreference) 
	VEHICLE_SEAT vsToReturn = VS_ANY_PASSENGER
	
	SWITCH iVehicleSeatPreference
		CASE ciFMMC_SeatPreference_None
			PRINTLN("GET_PED_PREFERRED_SEAT_FROM_CREATOR - Ped preferred seat ciFMMC_SeatPreference_None")
		BREAK
		
		CASE ciFMMC_SeatPreference_Driver
			vsToReturn = VS_DRIVER			
		BREAK
		
		CASE ciFMMC_SeatPreference_Passenger
		CASE ciFMMC_SeatPreference_Front_Seats
			vsToReturn = VS_FRONT_RIGHT			
		BREAK
		
		CASE ciFMMC_SeatPreference_Rear_Left
			vsToReturn = VS_BACK_LEFT			
		BREAK
		
		CASE ciFMMC_SeatPreference_Rear_Right
			vsToReturn = VS_BACK_RIGHT			
		BREAK
		
		CASE ciFMMC_SeatPreference_Extra_left_1
			vsToReturn = VS_EXTRA_LEFT_1		
		BREAK
		CASE ciFMMC_SeatPreference_Extra_left_2
			vsToReturn = VS_EXTRA_LEFT_2		
		BREAK
		CASE ciFMMC_SeatPreference_Extra_left_3
			vsToReturn = VS_EXTRA_LEFT_3		
		BREAK
		
		CASE ciFMMC_SeatPreference_Extra_right_1
			vsToReturn = VS_EXTRA_RIGHT_1		
		BREAK
		CASE ciFMMC_SeatPreference_Extra_right_2
			vsToReturn = VS_EXTRA_RIGHT_2		
		BREAK
		CASE ciFMMC_SeatPreference_Extra_right_3
			vsToReturn = VS_EXTRA_RIGHT_3		
		BREAK
	ENDSWITCH
	
	PRINTLN("GET_PED_PREFERRED_SEAT_FROM_CREATOR - vsToReturn: ", GET_SEAT_NAME_FOR_FORCE_OPTION(ENUM_TO_INT(vsToReturn)))
		
	RETURN vsToReturn
ENDFUNC

FUNC VEHICLE_SEAT GET_VEHICLE_SEAT_PED_SHOULD_ENTER(VEHICLE_INDEX tempVeh, INT iped #IF IS_DEBUG_BUILD , BOOL bPrints = FALSE #ENDIF )
	
	VEHICLE_SEAT vsSeatToUse = VS_ANY_PASSENGER
	BOOL bUseSeatPreference
	
	IF GET_PED_PREFERRED_SEAT_FROM_CREATOR_OPTION_INT(iPed) >= ciFMMC_SeatPreference_Driver
		bUseSeatPreference = TRUE
		vsSeatToUse = GET_PED_PREFERRED_SEAT_FROM_CREATOR(GET_PED_PREFERRED_SEAT_FROM_CREATOR_OPTION_INT(iPed))
		
		#IF IS_DEBUG_BUILD
		IF bPrints
			PRINTLN("[Peds][Ped ", iPed, "] GET_VEHICLE_SEAT_PED_SHOULD_ENTER - Seat preference from the creator as " ,ENUM_TO_INT(vsSeatToUse))
		ENDIF
		#ENDIF
		
		IF ENUM_TO_INT(vsSeatToUse) >= GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(tempveh) // Ensures we don't go out of range with creator settings.
			#IF IS_DEBUG_BUILD
			IF bPrints
				PRINTLN("[Peds][Ped ", iPed, "] GET_VEHICLE_SEAT_PED_SHOULD_ENTER - Seat does not exist.")
			ENDIF
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_SEAT_OUT_OF_RANGE_FOR_PED, ENTITY_RUNTIME_ERROR_TYPE_WARNING_PED, "Creator Seat Preference incorrect for vehicle", iPed)
			#ENDIF
			
			bUseSeatPreference = FALSE
		ENDIF
		
		IF bUseSeatPreference
		AND NOT IS_VEHICLE_SEAT_FREE(tempveh, vsSeatToUse)
			PED_INDEX VehPed = GET_PED_IN_VEHICLE_SEAT(tempveh, vsSeatToUse)
			IF NOT IS_PED_INJURED(VehPed)
				#IF IS_DEBUG_BUILD
				IF bPrints
					PRINTLN("[Peds][Ped ", iPed, "] GET_VEHICLE_SEAT_PED_SHOULD_ENTER - Creator seat preference is already taken, find first free seat...")
				ENDIF
				#ENDIF
				
				bUseSeatPreference = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bUseSeatPreference
		vsSeatToUse = MC_GET_FIRST_FREE_VEHICLE_SEAT(tempVeh, GET_PED_PREFERRED_SEAT_FROM_CREATOR_OPTION_INT(iPed) > ciFMMC_SeatPreference_Driver)
		
		#IF IS_DEBUG_BUILD
		IF bPrints
			PRINTLN("[Peds][Ped ", iPed, "] GET_VEHICLE_SEAT_PED_SHOULD_ENTER - First free vehicle seat for  found as ", ENUM_TO_INT(vsSeatToUse))
		ENDIF
		#ENDIF
				
		IF ENUM_TO_INT(vsSeatToUse) >= GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(tempveh)			
			vsSeatToUse = VS_ANY_PASSENGER // Means none are free in other functions.
		ENDIF
	ENDIF
	
	RETURN vsSeatToUse
	
ENDFUNC

FUNC BOOL DOES_PED_HAVE_VALID_GOTO(INT iped)
 	
	IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAtGotoLocBitset, iPed)
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.sPointData[0].vPosition)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_ANY_FMMC_PLAYER_WITHIN_AREA(VECTOR vCentre, INT iRadius)
	
	VECTOR vTempCoords
	
	INT iPart = -1
	WHILE DO_PARTICIPANT_LOOP(iPart, DPLF_CHECK_PLAYER_OK | DPLF_FILL_PLAYER_IDS)
		
		vTempCoords = GET_ENTITY_COORDS(piParticipantLoop_PedIndex, FALSE)
		
		IF VDIST2(vTempCoords, vCentre) <= (iRadius * iRadius)
			PRINTLN("IS_ANY_FMMC_PLAYER_WITHIN_AREA - Part ", iPart, " w coords ", vTempCoords, " is in area centered at ", vCentre, " w radius ", iRadius)
			RETURN TRUE
		ENDIF
			
	ENDWHILE
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PED_AT_ANY_TEAM_HOLDING(INT iped)

	IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAtYourHolding[0], iped)
	AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAtYourHolding[1], iped)
	AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAtYourHolding[2], iped)
	AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAtYourHolding[3], iped)
		RETURN FALSE
	ENDIF

	RETURN TRUE

ENDFUNC

FUNC BOOL IS_RAPPEL_SEAT_FREE(VEHICLE_INDEX tempVeh, VEHICLE_SEAT eSeat, INT iPed)
	
	INT iSeat = ENUM_TO_INT(eSeat)
	iSeat += 1 // VEHICLE_SEAT enum begins at -1, but iVehicleRappelSeatsBitset begins at 0
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle < (FMMC_MAX_VEHICLES - 1)
			
			IF NOT IS_BIT_SET(MC_serverBD.iVehicleRappelSeatsBitset[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle], iSeat)
				PRINTLN("[Peds][Ped ", iPed, "] ped ", iPed, " IS_RAPPEL_SEAT_FREE - bit not set.")
				RETURN TRUE
			ENDIF
			
			IF IS_VEHICLE_SEAT_FREE(tempVeh, eSeat)
				PRINTLN("[Peds][Ped ", iPed, "] ped ", iPed, " IS_RAPPEL_SEAT_FREE - seat is free.")
				RETURN TRUE
			ENDIF
			
		ENDIF
	ENDIF
	
	PRINTLN("[Peds][Ped ", iPed, "] ped ", iPed, " IS_RAPPEL_SEAT_FREE - bit set but seat not free.")
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_PED_COUNT_AS_HELD_FOR_TEAM(INT iped, INT iteam)

	IF MC_serverBD_4.iPedRule[iped][iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
	OR MC_serverBD_4.iPedRule[iped][iteam] = FMMC_OBJECTIVE_LOGIC_CAPTURE	
	OR MC_serverBD_4.iPedRule[iped][iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD	
		IF MC_serverBD_4.iPedPriority[iped][iteam]  <= MC_serverBD_4.iCurrentHighestPriority[iteam]
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PED_STATE_OK_FOR_PHOTO(INT iped)

	IF IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[MC_playerBD[iPartToUse].iteam], iPed)
			RETURN TRUE
		ENDIF
	ELSE
		IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[MC_playerBD[iPartToUse].iteam], iPed)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
	
ENDFUNC

FUNC INT GET_PED_PHOTO_RANGE_SQUARED(INT iped)

	INT iRange = ciFMMC_PHOTO_RANGE
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPhotoRange > 0
		iRange = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPhotoRange 
	ENDIF

	RETURN (iRange * iRange)
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: High Priority Ped Functions  ------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: This section should have the majority of helper functions that are used in multiple ped headers for high priority peds. ------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_PED_SHOULD_BE_HIGH_PRIORITY_PED_NEXT_FRAME(FMMC_PED_STATE &sPedState, BOOL bSetAsPriority)
	IF bSetAsPriority
		#IF IS_DEBUG_BUILD
		IF NOT FMMC_IS_LONG_BIT_SET(iFlaggedForHighPriorityProcessingNextFrameBS, sPedState.iIndex)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][HighPriorityPeds] - SET_PED_SHOULD_BE_HIGH_PRIORITY_PED_NEXT_FRAME - Setting as High Priority Ped for the next frame.")
			DEBUG_PRINTCALLSTACK()
		ENDIF
		#ENDIF
		FMMC_SET_LONG_BIT(iFlaggedForHighPriorityProcessingBS, sPedState.iIndex)
		FMMC_SET_LONG_BIT(iFlaggedForHighPriorityProcessingNextFrameBS, sPedState.iIndex)
	ELSE
		#IF IS_DEBUG_BUILD
		IF FMMC_IS_LONG_BIT_SET(iFlaggedForHighPriorityProcessingNextFrameBS, sPedState.iIndex)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][HighPriorityPeds] - SET_PED_SHOULD_BE_HIGH_PRIORITY_PED_NEXT_FRAME - Clearing as High Priority Ped for the next frame.")
			DEBUG_PRINTCALLSTACK()
		ENDIF
		#ENDIF
		FMMC_CLEAR_LONG_BIT(iFlaggedForHighPriorityProcessingBS, sPedState.iIndex)
		FMMC_CLEAR_LONG_BIT(iFlaggedForHighPriorityProcessingNextFrameBS, sPedState.iIndex)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PED_BE_ADDED_AS_HIGH_PRIORITY_PED(FMMC_PED_STATE &sPedState)
	
	IF sPedState.bExists
		IF sPedState.bInjured
			RETURN FALSE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
	
	// Creator Set Logic -------	
	IF IS_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, ciENTIRE_TASK_BITSET_GOTO_REQ_CONTINUOUS_UPDATE)
	AND MC_serverBD_2.iPedState[sPedState.iIndex] = ciTASK_GOTO_COORDS
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][HighPriorityPeds] - PROCESS_ADDING_HIGH_PRIORITY_PEDS - ciTASK_GOTO_COORDS and requires quick response times - Should be a High Priority Ped this frame. (ciENTIRE_TASK_BITSET_GOTO_REQ_CONTINUOUS_UPDATE)")
		RETURN TRUE	
	ENDIF
	
	INT iTeam
	FOR iTeam = 0 TO MC_ServerBD.iNumberOfTeams-1
		INT iRule =	MC_serverBD_4.iCurrentHighestPriority[iTeam] 
		IF iRule < FMMC_MAX_RULES
			IF IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHighPriorityPed[iRule], sPedState.iIndex) //Used Externally IS_LONG_BIT_SET
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][HighPriorityPeds] - PROCESS_ADDING_HIGH_PRIORITY_PEDS - Should be a High Priority Ped this Rule.")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PED_FLAGGED_AS_HIGH_PRIORITY_PED(FMMC_PED_STATE &sPedState)
	RETURN FMMC_IS_LONG_BIT_SET(iFlaggedForHighPriorityProcessingBS, sPedState.iIndex)
ENDFUNC

PROC PROCESS_ADDING_HIGH_PRIORITY_PEDS(FMMC_PED_STATE &sPedState)	
			
	IF SHOULD_PED_BE_ADDED_AS_HIGH_PRIORITY_PED(sPedState)
		FMMC_SET_LONG_BIT(iFlaggedForHighPriorityProcessingBS, sPedState.iIndex)
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Task related shared utility functions  --------------------------------------------------------------------------------------------------------------------------
// ##### Description: GoTo Locate, Combat, Helper functions, etc.   --------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_PED_RESET_ANIMS_AFTER_RESPAWNING(FMMC_PED_STATE &sPedState)

	RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetTwentyTwo, ciPED_BSTwentyTwo_ResetAnimsOnRespawn)
	
ENDFUNC

FUNC BOOL SHOULD_PED_START_TASK_AFTER_RESPAWNING(FMMC_PED_STATE &sPedState)

	// Default Functionality reported as a bug and therefore changed to use an option. Seems like earlier content was using this default functionality as intended, even though it was expressed as a bug/unexpected in Fixer pack.
	IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_FIXER)
		RETURN TRUE
	ENDIF
	
	RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetTwentyOne, ciPED_BSTwentyOne_StartTasksInstantlyOnRespawn)
	
ENDFUNC

PROC ASSIGN_DRIVING_MODE_FLAGS_FROM_CREATOR_OPTIONS(INT &iPed, DRIVINGMODE &dmFlags)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwelve, ciPed_BSTwelve_DisableDriveIntoOncomingTraffic)
		PRINTLN("[Peds][Ped ", iPed, "][Task][CLIENT] - ASSIGN_DRIVING_MODE_FLAGS_FROM_CREATOR_OPTIONS - NOT ciPed_BSTwelve_DisableDriveIntoOncomingTraffic - Adding DF_DriveIntoOncomingTraffic")
		dmFlags = dmFlags | DF_DriveIntoOncomingTraffic		
	ENDIF
	
	IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_FORCENAVMESH) 
		IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_IGNORE_NAVMESH) 
			PRINTLN("[Peds][Ped ", iPed, "][Task][CLIENT] - ASSIGN_DRIVING_MODE_FLAGS_FROM_CREATOR_OPTIONS - PED_ASSOCIATED_GOTO_TASK_TYPE_FORCENAVMESH+PED_ASSOCIATED_GOTO_TASK_TYPE_IGNORE_NAVMESH - Adding DF_ForceStraightLine")
			dmFlags = dmFlags | DF_ForceStraightLine
		ELSE
			PRINTLN("[Peds][Ped ", iPed, "][Task][CLIENT] - ASSIGN_DRIVING_MODE_FLAGS_FROM_CREATOR_OPTIONS - PED_ASSOCIATED_GOTO_TASK_TYPE_FORCENAVMESH - Adding DF_PreferNavmeshRoute")
			dmFlags = dmFlags | DF_PreferNavmeshRoute			
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwelve, ciPed_BSTwelve_ForceAggroAvoidanceFlag)
		PRINTLN("[Peds][Ped ", iPed, "][Task][CLIENT] - ASSIGN_DRIVING_MODE_FLAGS_FROM_CREATOR_OPTIONS - ciPed_BSTwelve_ForceAggroAvoidanceFlag - Adding DF_SwerveAroundAllCars | DF_SteerAroundStationaryCars | DF_SteerAroundObjects | DF_SteerAroundPeds")
		PRINTLN("[Peds][Ped ", iPed, "][Task][CLIENT] - ASSIGN_DRIVING_MODE_FLAGS_FROM_CREATOR_OPTIONS - ciPed_BSTwelve_ForceAggroAvoidanceFlag - Adding DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions")
		dmFlags = dmFlags | DF_SwerveAroundAllCars | DF_SteerAroundStationaryCars | DF_SteerAroundObjects | DF_SteerAroundPeds | DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwentyOne, ciPed_BSTwentyOne_EnableForceJoinInRoadDirection)
		PRINTLN("[Peds][Ped ", iPed, "][Task][CLIENT] - ASSIGN_DRIVING_MODE_FLAGS_FROM_CREATOR_OPTIONS - ciPed_BSTwentyOne_EnableForceJoinInRoadDirection - Adding DF_ForceJoinInRoadDirection")
		dmFlags = dmFlags | DF_ForceJoinInRoadDirection
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFifteen, ciPED_BSFifteen_EnablePloughThroughMode)
		PRINTLN("[Peds][Ped ", iPed, "][Task][CLIENT] - ASSIGN_DRIVING_MODE_FLAGS_FROM_CREATOR_OPTIONS - ciPED_BSFifteen_EnablePloughThroughMode - Adding DRIVINGMODE_PLOUGHTHROUGH")
		dmFlags = dmFlags | DRIVINGMODE_PLOUGHTHROUGH		
	ENDIF		
	
ENDPROC

PROC ASSIGN_TASK_GO_TO_COORD_ANY_MEANS_FLAGS_FROM_CREATOR_OPTIONS(PED_INDEX &tempPed, INT &iPed, TASK_GO_TO_COORD_ANY_MEANS_FLAGS &tgcamFlags, BOOL bGetOut = FALSE, FLOAT fShootRange = -1.0)

	IF NOT bGetOut
		tgcamFlags = tgcamFlags | TGCAM_REMAIN_IN_VEHICLE_AT_DESTINATION
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSix, ciPed_BSSix_DontAbandonVehicleOnGoto_IfMoving)
		tgcamFlags = tgcamFlags | TGCAM_NEVER_ABANDON_VEHICLE_IF_MOVING
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPed_BSEight_NeverLeaveVehicle)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEighteen, ciPed_BSEighteen_DontLeaveVehicleUnlessExplicitCommand)
		tgcamFlags = tgcamFlags | TGCAM_NEVER_ABANDON_VEHICLE
	ENDIF
	
	IF fshootrange > -1
		SET_PED_CONFIG_FLAG(tempPed, PCF_BroadcastRepondedToThreatWhenGoingToPointShooting,TRUE)
		tgcamFlags = tgcamFlags | TGCAM_USE_AI_TARGETING_FOR_THREATS
		PRINTLN("[Peds][Ped ", iPed, "][Task][CLIENT] - TASK_PED_GOTO_ANY_MEANS on foot only - calling TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS Set PCF_BroadcastRepondedToThreatWhenGoingToPointShooting (2) for ped: ",iPed, " fshootrange = ", fshootrange)
	ENDIF 
	
ENDPROC


FUNC FLOAT GET_PED_GOTO_MOVE_SPEED_RATIO(FMMC_PED_STATE &sPedState)
	
	FLOAT fOnFootSpeed	
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitset, ciPed_BS_AssActionWalk)
		fOnFootSpeed = PEDMOVEBLENDRATIO_WALK
	ELSE
		fOnFootSpeed = PEDMOVEBLENDRATIO_RUN
	ENDIF
				
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].fPedGotoMoveSpeedRatioOnFoot > 0.0
		fOnFootSpeed = g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].fPedGotoMoveSpeedRatioOnFoot
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetThree, ciPed_BSThree_SprintTaskOnAgro)
	AND FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAggroedBitset, sPedState.iIndex)
		fOnFootSpeed = PEDMOVEBLENDRATIO_SPRINT
	ENDIF
	
	RETURN fOnFootSpeed
ENDFUNC

PROC SET_PED_RETASK_DIRTY_FLAG(FMMC_PED_STATE &sPedState)
	
	#IF IS_DEBUG_BUILD
	IF sPedState.iIndex < 0
		SCRIPT_ASSERT("[RCC MISSION] - [DIRTY FLAG] - SET_PED_RETASK_DIRTY_FLAG has been called with iPed < 0. Please pass a valid ped identifier.")
	ENDIF
	#ENDIF
	
	IF NOT FMMC_IS_LONG_BIT_SET(sRetaskDirtyFlagData.iDirtyFlagPedNeedsRetask, sPedState.iIndex)
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - [DIRTY FLAG] - SET_PED_RETASK_DIRTY_FLAG - iDirtyFlagPedNeedsRetask bit is not set. Setting dirty flag.")
		
		FMMC_SET_LONG_BIT(sRetaskDirtyFlagData.iDirtyFlagPedNeedsRetask, sPedState.iIndex)
		RESET_NET_TIMER(maxAccelerationUpdateTimers[sPedState.iIndex])
		
		IF sPedState.bExists
		AND NOT sPedState.bInjured
		AND NOT sPedState.bHasControl
			BROADCAST_FMMC_PED_DIRTY_FLAG_SET(sPedState.iIndex)
		ENDIF
	ENDIF
	
ENDPROC

PROC CLEAR_RETASK_DIRTY_FLAG(FMMC_PED_STATE &sPedState)
	
	#IF IS_DEBUG_BUILD
	IF sPedState.iIndex < 0
		SCRIPT_ASSERT("[RCC MISSION] - [DIRTY FLAG] - CLEAR_RETASK_DIRTY_FLAG has been called with iPed < 0. Please pass a valid ped identifier.")
	ENDIF
	#ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(sRetaskDirtyFlagData.iDirtyFlagPedNeedsRetask, sPedState.iIndex)		
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - [DIRTY FLAG] - CLEAR_RETASK_DIRTY_FLAG - iDirtyFlagPedNeedsRetask bit is set. Clearing dirty flag.")
		DEBUG_PRINTCALLSTACK()
		FMMC_CLEAR_LONG_BIT(sRetaskDirtyFlagData.iDirtyFlagPedNeedsRetask, sPedState.iIndex)		
	ENDIF
	
ENDPROC

PROC SUB_TASK_FIRE_TAMPA_MORTAR(PED_INDEX tempPed, VEHICLE_INDEX tempVeh, INT iPed)
	IF GET_ENTITY_MODEL(tempVeh) = TAMPA3
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPed_BSEight_DisableBuzzardRocketUse)
		PED_INDEX target = GET_CLOSEST_VISIBLE_PLAYER(iPed, tempPed, TRUE, PED_MAX_TAMPA_MORTAR_RANGE, TRUE)
		ASSERTLN("FMMC2020 - this is WRONG - Needs to be REFACTORED.")
		// Default settings
		WEAPON_TYPE desiredWeapon = WEAPONTYPE_DLC_VEHICLE_TAMPA_DUALMINIGUN //WEAPONTYPE_DLC_VEHICLE_TAMPA_MORTAR //WEAPONTYPE_DLC_VEHICLE_TAMPA_DUALMINIGUN
		FIRING_PATTERN_HASH firingPattern = FIRING_PATTERN_FULL_AUTO

		// Check target and decide state / weapon / firing pattern
		IF DOES_ENTITY_EXIST(target)
			IF IS_TARGET_BEHIND_ENTITY(tempPed, target, 22.5)
				PRINTLN("TAMPA_COMBAT - Target found behind us, fire using MORTAR weapon")
				desiredWeapon = WEAPONTYPE_DLC_VEHICLE_TAMPA_MORTAR
				firingPattern = FIRING_PATTERN_TAMPA_MORTAR
			ELSE
				PRINTLN("TAMPA_COMBAT - Target found but not behind us, fire using MINIGUN weapon default settings")
			ENDIF
		ENDIF
		
		// Make sure we have control of the weapon
		IF NOT IS_MOUNTED_WEAPON_TASK_UNDERNEATH_DRIVING_TASK(tempPed)
			PRINTLN("TAMPA_COMBAT - We don't have control of mounted weapon, so taking control")
			CONTROL_MOUNTED_WEAPON(tempPed)
		ENDIF
		
		// There seems to be a frame delay actually creating the task, so check here too
		IF IS_MOUNTED_WEAPON_TASK_UNDERNEATH_DRIVING_TASK(tempPed)		
		
			// See if we need to switch weapon
			WEAPON_TYPE weaponType = WEAPONTYPE_INVALID
			GET_CURRENT_PED_VEHICLE_WEAPON(tempPed, weaponType)
			IF weaponType != desiredWeapon
				PRINTLN("TAMPA_COMBAT - We don't have the correct weapon, attempting to switch")
				SET_PED_FIRING_PATTERN(tempPed, firingPattern)
				IF NOT SET_CURRENT_PED_VEHICLE_WEAPON(tempPed, desiredWeapon)
					PRINTLN("TAMPA_COMBAT - Failed to change weapon, ped ", iPed)
					SCRIPT_ASSERT("TAMPA_COMBAT - Failed to change weapon")
				ELSE
					PRINTLN("TAMPA_COMBAT - Successfully changed weapon")
				ENDIF
				PRINTLN("TAMPA_COMBAT - Restarting control mounted weapon task after a weapon switch")
				CONTROL_MOUNTED_WEAPON(tempPed)
			ENDIF

			// Fire if we have target, otherwise set weapon as idle
			IF target = NULL
				// Can't pass in null as target entity, so passing in itself...
				SET_MOUNTED_WEAPON_TARGET(tempPed, tempPed, NULL, <<0,0,0>>, TASK_IDLE) 
			ELSE
				SET_MOUNTED_WEAPON_TARGET(tempPed, target, NULL, <<0,0,0>>, TASK_FIRE)
			ENDIF
				
		ENDIF
	ENDIF
ENDPROC

PROC GET_PED_CURRENT_VEHICLE_DATA(FMMC_PED_STATE &sPedState, FMMC_PED_VEHICLE_DATA &sPedVehicleData)
	
	// Grabbing info about the vehicle the current ped is in
	IF sPedState.bIsInAnyVehicle
		
		sPedVehicleData.bInVeh = TRUE
		
		IF IS_VEHICLE_DRIVEABLE(sPedState.vehIndexPedIsIn)
		
			sPedVehicleData.bVehOK = TRUE
			
			IF IS_PED_IN_ANY_HELI(sPedState.pedIndex)
				sPedVehicleData.bHeli = TRUE
			ELIF IS_PED_IN_ANY_PLANE(sPedState.pedIndex)
				sPedVehicleData.bPlane = TRUE
			ENDIF
			
			IF IS_PED_IN_ANY_BOAT(sPedState.pedIndex)
				sPedVehicleData.bBoat = TRUE
			ENDIF
			
			IF IS_COMBAT_VEHICLE(sPedState.vehIndexPedIsIn)
				sPedVehicleData.bCombatVeh = TRUE
				SUB_TASK_FIRE_TAMPA_MORTAR(sPedState.pedIndex, sPedState.vehIndexPedIsIn, sPedState.iIndex) // why the hell is this here...
			ENDIF
			
			sPedVehicleData.driverPed = GET_PED_IN_VEHICLE_SEAT(sPedState.vehIndexPedIsIn)
			
			IF sPedVehicleData.driverPed = sPedState.pedIndex			
				sPedVehicleData.bDriver = TRUE
				sPedVehicleData.bDriverOk = TRUE
			ELSE
				IF NOT IS_PED_INJURED(sPedVehicleData.driverPed)
				AND DOES_ENTITY_EXIST(sPedVehicleData.driverPed)
					sPedVehicleData.bDriverOk = TRUE
				ENDIF
			ENDIF
			
			sPedVehicleData.fSpeed = GET_ENTITY_SPEED(sPedState.vehIndexPedIsIn)
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_GOTO_RANGE_DEFAULT_VALUE(FMMC_PED_STATE &sPedState, FMMC_PED_VEHICLE_DATA &sPedVehicleData)
	
	FLOAT fGotoRange

	// This is to do with the ending of goto tasks in various situations
	IF FMMC_IS_LONG_BIT_SET(iPedArrivedBitset, sPedState.iIndex)
		IF sPedVehicleData.bPlane
			fGotoRange = ciPed_PLANE_LEAVE_GOTO_RANGE
		ELIF (sPedVehicleData.bBoat OR sPedVehicleData.bHeli)
			fGotoRange = ciPed_BOAT_HELI_LEAVE_GOTO_RANGE
		ELSE
			fGotoRange  = ciPed_LEAVE_GOTO_RANGE
		ENDIF
		IF NOT FMMC_IS_LONG_BIT_SET(iPedChangeTaskBitset, sPedState.iIndex)
			IF CAN_PED_TASKS_BE_CLEARED(sPedState.pedIndex)
				SET_PED_RETASK_DIRTY_FLAG(sPedState)
			ENDIF
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "] GET_GOTO_RANGE_DEFAULT_VALUE - ped arrived clearing tasks: ", sPedState.iIndex)
			FMMC_SET_LONG_BIT(iPedChangeTaskBitset, sPedState.iIndex)
		ENDIF
	ELSE
		IF FMMC_IS_LONG_BIT_SET(iPedChangeTaskBitset, sPedState.iIndex)
			IF CAN_PED_TASKS_BE_CLEARED(sPedState.pedIndex)
				SET_PED_RETASK_DIRTY_FLAG(sPedState)
			ENDIF
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "] GET_GOTO_RANGE_DEFAULT_VALUE - ped left clearing tasks: ", sPedState.iIndex)
			FMMC_CLEAR_LONG_BIT(iPedChangeTaskBitset, sPedState.iIndex)
		ENDIF
		IF sPedVehicleData.bPlane
			fGotoRange = ciPed_PLANE_ARRIVE_GOTO_RANGE
		ELIF (sPedVehicleData.bBoat OR sPedVehicleData.bHeli)
			fGotoRange = ciPed_BOAT_HELI_ARRIVE_GOTO_RANGE
		ELSE
			fGotoRange  = ciPed_ARRIVE_GOTO_RANGE
		ENDIF
	ENDIF
	
	RETURN fGotoRange
	
ENDFUNC

PROC GET_PED_TARGET_DATA(FMMC_PED_STATE &sPedState, FMMC_PED_TARGET_DATA &sPedTargetData)

	// AI navigation subclause to do with this ped's target
	IF MC_serverBD.niTargetID[sPedState.iIndex] != NULL
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD.niTargetID[sPedState.iIndex])
			
			sPedTargetData.tempTarget = NET_TO_ENT(MC_serverBD.niTargetID[sPedState.iIndex])
			
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - GET_PED_TARGET_DATA - Entity target: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(sPedTargetData.tempTarget)))			
			
			sPedTargetData.bExists = TRUE
			
			IF NOT IS_ENTITY_DEAD(sPedTargetData.tempTarget)
				sPedTargetData.bTargetOK = TRUE
				sPedTargetData.fTargetSpeed = GET_ENTITY_SPEED(sPedTargetData.tempTarget)
				sPedTargetData.vTempTargetCoords = GET_ENTITY_COORDS(sPedTargetData.tempTarget)
								
				IF IS_THIS_MODEL_A_TRAIN(GET_ENTITY_MODEL(sPedTargetData.tempTarget))
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - GET_PED_TARGET_DATA - Entity target: TRAIN")			
					
					sPedTargetData.targetVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sPedTargetData.tempTarget)				
				
				ELIF IS_ENTITY_A_VEHICLE(sPedTargetData.tempTarget)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - GET_PED_TARGET_DATA - Entity target: VEHICLE")			
					
					sPedTargetData.targetVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sPedTargetData.tempTarget)
					
					IF IS_VEHICLE_A_TRAILER(sPedTargetData.targetVeh)
					AND NOT IS_ENTITY_DEAD(GET_ENTITY_ATTACHED_TO(sPedTargetData.targetVeh))
						sPedTargetData.targetVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO(sPedTargetData.targetVeh))
					ENDIF

					IF IS_VEHICLE_A_CREATOR_AIRCRAFT(sPedTargetData.targetVeh)
					AND g_iHostOfam_mp_armory_aircraft != -1
					AND NOT IS_PED_INJURED(MPGlobals.RemoteFakeAvengerPed[g_iHostOfam_mp_armory_aircraft])
						sPedTargetData.tempPedTarget = MPGlobals.RemoteFakeAvengerPed[g_iHostOfam_mp_armory_aircraft]
					ELSE
						IF IS_VEHICLE_A_TRAILER(sPedTargetData.targetVeh)
							ENTITY_INDEX attachEnt = GET_ENTITY_ATTACHED_TO(sPedTargetData.targetVeh)							
							IF DOES_ENTITY_EXIST(attachEnt)
							AND IS_ENTITY_A_VEHICLE(attachEnt)
								VEHICLE_INDEX attachVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(attachEnt)
								IF IS_VEHICLE_DRIVEABLE(attachVeh)
									sPedTargetData.tempPedTarget = GET_PED_IN_VEHICLE_SEAT(attachVeh)
								ENDIF
							ENDIF						
						ELSE
							sPedTargetData.tempPedTarget = GET_PED_IN_VEHICLE_SEAT(sPedTargetData.targetVeh)
						ENDIF
					ENDIF

				ELIF IS_ENTITY_AN_OBJECT(sPedTargetData.tempTarget)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - GET_PED_TARGET_DATA - Entity target: OBJECT")
					IF IS_ENTITY_ATTACHED(sPedTargetData.tempTarget)
						sPedTargetData.temp_carrier = GET_ENTITY_ATTACHED_TO(sPedTargetData.tempTarget)
						IF NOT IS_ENTITY_DEAD(sPedTargetData.temp_carrier)
							IF IS_ENTITY_A_PED(sPedTargetData.temp_carrier)
								sPedTargetData.tempPedTarget = GET_PED_INDEX_FROM_ENTITY_INDEX(sPedTargetData.temp_carrier)
							ELIF IS_ENTITY_A_VEHICLE(sPedTargetData.temp_carrier)
								sPedTargetData.tempPedTarget = GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sPedTargetData.temp_carrier))
							ELSE
								sPedTargetData.temp_carrier = NULL
							ENDIF
							sPedTargetData.fTargetSpeed = GET_ENTITY_SPEED(sPedTargetData.temp_carrier)
						ELSE
							sPedTargetData.temp_carrier = NULL
						ENDIF
					ENDIF
				ELIF IS_ENTITY_A_PED(sPedTargetData.tempTarget)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - GET_PED_TARGET_DATA - Entity target: PED")
					sPedTargetData.tempPedTarget = GET_PED_INDEX_FROM_ENTITY_INDEX(sPedTargetData.tempTarget)
					IF IS_PED_IN_ANY_VEHICLE(sPedTargetData.tempPedTarget)
						sPedTargetData.targetVeh = GET_VEHICLE_PED_IS_IN(sPedTargetData.tempPedTarget)
					ENDIF
				ENDIF
								
				// DIRTY - target coords have changed
				IF NOT FMMC_IS_LONG_BIT_SET(iGotoObjectBitSet, sPedState.iIndex)
				AND (IS_VECTOR_ZERO(vPedTargetCoords[sPedState.iIndex]) OR GET_DISTANCE_BETWEEN_COORDS(sPedTargetData.vTempTargetCoords, vPedTargetCoords[sPedState.iIndex]) > 5.0)
					
					IF NOT IS_BIT_SET(sPedState.iBitset, FMMC_PED_STATE_IN_COMBAT)
					AND FMMC_IS_LONG_BIT_SET(iBSTaskUsingCoordTarget, sPedState.iIndex)
					AND CAN_PED_TASKS_BE_CLEARED(sPedState.pedIndex)
						SET_PED_RETASK_DIRTY_FLAG(sPedState)
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - GET_PED_TARGET_DATA - target moved, clearing tasks") 
					ENDIF
					
					vPedTargetCoords[sPedState.iIndex] = sPedTargetData.vTempTargetCoords
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - GET_PED_TARGET_DATA - target moved, Targets new coords = ", vPedTargetCoords[sPedState.iIndex])
				ELSE
					vPedTargetCoords[sPedState.iIndex] = sPedTargetData.vTempTargetCoords
				ENDIF
				
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - GET_PED_TARGET_DATA - Entity Target Coords: ", vPedTargetCoords[sPedState.iIndex])
				
				// DIRTY - Target entity has changed
				IF piOldTargetPed[sPedState.iIndex] != sPedTargetData.tempPedTarget
					SET_PED_RETASK_DIRTY_FLAG(sPedState)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - GET_PED_TARGET_DATA - target updated, clearing tasks")
					piOldTargetPed[sPedState.iIndex] = sPedTargetData.tempPedTarget
				ENDIF
			ELSE
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - GET_PED_TARGET_DATA - Entity target: is dead")
			ENDIF
		ENDIF
	ELIF MC_serverBD.TargetPlayerID[sPedState.iIndex] != INVALID_PLAYER_INDEX()
		IF IS_NET_PLAYER_OK(MC_serverBD.TargetPlayerID[sPedState.iIndex])
			sPedTargetData.bTargetOK = TRUE
			sPedTargetData.tempPedTarget = GET_PLAYER_PED(MC_serverBD.TargetPlayerID[sPedState.iIndex])
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - GET_PED_TARGET_DATA - Player target:", GET_PLAYER_NAME(MC_serverBD.TargetPlayerID[sPedState.iIndex])) 
			sPedTargetData.fTargetSpeed = GET_ENTITY_SPEED(sPedTargetData.tempPedTarget)
			sPedTargetData.vTempTargetCoords = GET_ENTITY_COORDS(sPedTargetData.tempPedTarget)
			
			IF NOT FMMC_IS_LONG_BIT_SET(iGotoObjectBitSet, sPedState.iIndex)
			AND (IS_VECTOR_ZERO(vPedTargetCoords[sPedState.iIndex]) OR GET_DISTANCE_BETWEEN_COORDS(sPedTargetData.vTempTargetCoords, vPedTargetCoords[sPedState.iIndex]) > 50)
				IF IS_PED_INJURED(sPedTargetData.tempPedTarget)
					IF NOT IS_BIT_SET(sPedState.iBitset, FMMC_PED_STATE_IN_COMBAT)
					AND FMMC_IS_LONG_BIT_SET(iBSTaskUsingCoordTarget, sPedState.iIndex)
					AND CAN_PED_TASKS_BE_CLEARED(sPedState.pedIndex)
						SET_PED_RETASK_DIRTY_FLAG(sPedState)
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - GET_PED_TARGET_DATA - target moved, clearing tasks") 
					ENDIF
				ENDIF
				vPedTargetCoords[sPedState.iIndex] = sPedTargetData.vTempTargetCoords
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - GET_PED_TARGET_DATA - target moved, Targets new coords = ", vPedTargetCoords[sPedState.iIndex])
			ENDIF
			
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(sPedTargetData.tempPedTarget)
		sPedTargetData.bPedTargetOK = TRUE
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - GET_PED_TARGET_DATA - sPedTargetData.bPedTargetOK = TRUE")
	ELSE
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - GET_PED_TARGET_DATA - sPedTargetData.bPedTargetOK = FALSE")
	ENDIF
ENDPROC

FUNC BOOL WAIT_FORCARGOBOB_DETACH(PED_INDEX ThisPed, INT iPed, INT iVeh)
	// If this function is returning false you'd see it in the logs
	IF IS_PED_IN_ANY_HELI(ThisPed)
		INT iIndex = MC_serverBD_2.iPedGotoProgress[iped]
		IF iIndex = GET_ASSOCIATED_GOTO_TASK_DATA__DETACH_CARGOBOB_ON_GOTO_INDEX(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData)
			IF NOT IS_BIT_SET(MC_serverBD.iCargobobDettachedBitset, iVeh)
				PRINTLN("[Peds][Ped ", iPed, "] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - arrived, waiting for cargobob detach: ", iVeh)
				IF HAS_NET_TIMER_STARTED(cargobobDetachTimer)
					IF HAS_NET_TIMER_EXPIRED(cargobobDetachTimer, 2000)
						SET_BIT(MC_serverBD.iCargobobShouldDetachBitset, iVeh)
						PRINTLN("[Peds][Ped ", iPed, "] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - cargobob detached! Should continue next frame: ", iVeh)
					ENDIF
				ELSE
					START_NET_TIMER(cargobobDetachTimer)
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_ASSOCIATED_GOTO_ENTER_VEH_VALID(INT iPed, INT iIndex, VEHICLE_INDEX &vehIndex)

	IF iIndex < MAX_ASSOCIATED_GOTO_TASKS
	AND iIndex >= 0
		BOOL bReady = TRUE
		INT iVehicle = GET_ASSOCIATED_GOTO_TASK_DATA__CHOSEN_VEHICLE(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, iIndex)
		INT iAssociatedRule = GET_ASSOCIATED_GOTO_TASK_DATA__ASSOCIATED_RULE(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, iIndex)
		
		// This logic was added after content had been set up without caring about the rule. This check is just a safety precaution.
		BOOL bDependsOnRule = (iAssociatedRule != -1) AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam != -1)
		IF bDependsOnRule
			IF (MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam] >= iAssociatedRule)
				bReady = TRUE
			ELSE
				bReady = FALSE
			ENDIF
		ENDIF
		
		PRINTLN("[LM] - IS_ASSOCIATED_GOTO_ENTER_VEH_VALID - Checking - iPed: ", iPed, " GoTo Progress: ", iIndex, " iVeh: ", iVehicle, " Associated Team: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam, " Associated Rule: ", iAssociatedRule, " bReady: ", bReady)
		
		IF bReady			
			IF iVehicle > -1		
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehicle])
					vehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehicle])			
					IF DOES_ENTITY_EXIST(vehIndex)
					AND IS_VEHICLE_DRIVEABLE(vehIndex)
						PRINTLN("[LM] - IS_ASSOCIATED_GOTO_ENTER_VEH_VALID - Returning TRUE - iPed: ", iPed, " GoTo Progress: ", iIndex, " iVeh: ", iVehicle)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PED_DO_DEFENSIVE_AREA_GOTO(PED_INDEX tempPed, INT iped)
	
	BOOL bDefensiveAreaGoto = FALSE
	
	IF MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO
	AND (MC_serverBD_2.iPedGotoProgress[iped] < MAX_ASSOCIATED_GOTO_TASKS)
	AND IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_COMBAT_GOTO) 
		IF (NOT IS_PED_IN_ANY_VEHICLE(tempPed))
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].ipedBitsetEight, ciPED_BSEight_CanDoCombatGotoInVeh)
			bDefensiveAreaGoto = TRUE
		ENDIF
	ENDIF
	
	RETURN bDefensiveAreaGoto
	
ENDFUNC

PROC PROCESS_PREVENT_PED_FROM_SHOOTING_GROUND_TARGETS_WITH_ROCKETS(FMMC_PED_STATE &sPedState, FMMC_PED_VEHICLE_DATA &sPedVehicleData)
	
	IF NOT sPedState.bIsInAnyVehicle
		EXIT
	ENDIF
			
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitset, ciPED_BS_RocketPedsAndVehicles)
		EXIT
	ENDIF
	
	IF NOT sPedVehicleData.bVehOk
		EXIT
	ENDIF
		
	MODEL_NAMES mnVeh = GET_ENTITY_MODEL(sPedState.vehIndexPedIsIn)
	WEAPON_TYPE wtToUse
	WEAPON_TYPE wtToReEnable
	WEAPON_TYPE wtToDisable
	WEAPON_TYPE wtRockets
	WEAPON_TYPE wtTemp
	
	IF mnVeh = BUZZARD
	OR mnVeh = BUZZARD2		
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetEight, ciPed_BSEight_DisableBuzzardRocketUse)
			EXIT
		ENDIF
		
		GET_CURRENT_PED_VEHICLE_WEAPON(sPedState.pedIndex, wtTemp)
		wtRockets = WEAPONTYPE_VEHICLE_SPACE_ROCKET

		IF wtTemp = WEAPONTYPE_INVALID
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "] PROCESS_PREVENT_PED_FROM_SHOOTING_GROUND_TARGETS_WITH_ROCKETS - Assigning from BUZZARD/BUZZARD2 - Currently INVALID. Exitting.")
			EXIT
			
		ELIF wtTemp = WEAPONTYPE_VEHICLE_SPACE_ROCKET
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "] PROCESS_PREVENT_PED_FROM_SHOOTING_GROUND_TARGETS_WITH_ROCKETS - Assigning from BUZZARD/BUZZARD2 - Currently using Rockets.")
			wtToDisable = WEAPONTYPE_VEHICLE_SPACE_ROCKET
			wtToUse = WEAPONTYPE_VEHICLE_PLAYER_BUZZARD
		ELSE 
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "] PROCESS_PREVENT_PED_FROM_SHOOTING_GROUND_TARGETS_WITH_ROCKETS - Assigning from BUZZARD/BUZZARD2 - Currently NOT using Rockets.")
			wtToReEnable = WEAPONTYPE_VEHICLE_PLAYER_BUZZARD
			wtToUse = WEAPONTYPE_VEHICLE_SPACE_ROCKET
		ENDIF
		
	ELIF mnVeh = SAVAGE
		
		GET_CURRENT_PED_VEHICLE_WEAPON(sPedState.pedIndex, wtTemp)
		wtRockets = WEAPONTYPE_VEHICLE_SPACE_ROCKET
		
		IF wtTemp = WEAPONTYPE_INVALID
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "] PROCESS_PREVENT_PED_FROM_SHOOTING_GROUND_TARGETS_WITH_ROCKETS - Assigning from SAVAGE - Currently INVALID. Exitting.")
			EXIT
			
		ELIF wtTemp = WEAPONTYPE_VEHICLE_SPACE_ROCKET
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "] PROCESS_PREVENT_PED_FROM_SHOOTING_GROUND_TARGETS_WITH_ROCKETS - Assigning from SAVAGE - Currently using Rockets.")
			wtToDisable = WEAPONTYPE_VEHICLE_SPACE_ROCKET
			wtToUse = WEAPONTYPE_DLC_VEHICLE_PLAYER_SAVAGE
		ELSE
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "] PROCESS_PREVENT_PED_FROM_SHOOTING_GROUND_TARGETS_WITH_ROCKETS - Assigning from SAVAGE - Currently NOT using Rockets.")
			wtToReEnable = WEAPONTYPE_DLC_VEHICLE_PLAYER_SAVAGE
			wtToUse = WEAPONTYPE_VEHICLE_SPACE_ROCKET
		ENDIF		
		
	ELSE		
		EXIT
	ENDIF
			
	PED_INDEX pedTarget = GET_CLOSEST_PLAYER_PED_TO_THIS_PED(sPedState.pedIndex)
	
	IF IS_PED_INJURED(pedTarget)
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "] PROCESS_PREVENT_PED_FROM_SHOOTING_GROUND_TARGETS_WITH_ROCKETS - Target injured or does not exist. Exitting.")
		EXIT	
	ENDIF
	
	BOOL bTargetInVeh = IS_PED_IN_ANY_VEHICLE(pedTarget, FALSE)
	BOOL bRocketsDisabled = IS_VEHICLE_WEAPON_DISABLED(wtRockets, sPedState.vehIndexPedIsIn, sPedState.pedIndex)
	
	PRINTLN("[Peds][Ped ", sPedState.iIndex, "] PROCESS_PREVENT_PED_FROM_SHOOTING_GROUND_TARGETS_WITH_ROCKETS - bTargetInVeh: ", bTargetInVeh, " bRocketsDisabled: ", bRocketsDisabled)
	
	IF bTargetInVeh = bRocketsDisabled	
		
		IF wtToDisable != WEAPONTYPE_INVALID
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "] PROCESS_PREVENT_PED_FROM_SHOOTING_GROUND_TARGETS_WITH_ROCKETS - Disabling wtToDisable")
			DISABLE_VEHICLE_WEAPON(TRUE, wtToDisable, sPedState.vehIndexPedIsIn, sPedState.pedIndex)
		ENDIF
		
		IF wtToReEnable != WEAPONTYPE_INVALID
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "] PROCESS_PREVENT_PED_FROM_SHOOTING_GROUND_TARGETS_WITH_ROCKETS - Enabling wtToReEnable and allowing to switch")
			DISABLE_VEHICLE_WEAPON(FALSE, wtToUse, sPedState.vehIndexPedIsIn, sPedState.pedIndex)
			SET_PED_CAN_SWITCH_WEAPON(sPedState.pedIndex, TRUE)
		ELSE
			SET_PED_CAN_SWITCH_WEAPON(sPedState.pedIndex, FALSE)
		ENDIF
		
		IF wtToUse != WEAPONTYPE_INVALID
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "] PROCESS_PREVENT_PED_FROM_SHOOTING_GROUND_TARGETS_WITH_ROCKETS - Enabling wtToUse and setting wtToUse")
			DISABLE_VEHICLE_WEAPON(FALSE, wtToUse, sPedState.vehIndexPedIsIn, sPedState.pedIndex)
			SET_CURRENT_PED_VEHICLE_WEAPON(sPedState.pedIndex, wtToUse)
		ENDIF
		
	ENDIF
ENDPROC

PROC PROCESS_GIVING_PED_WEAPON_ON_RULE(FMMC_PED_STATE &sPedState)
		
	PED_INDEX tempPed = sPedState.pedIndex
	INT iped = sPedState.iIndex
	
	IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iGunOnRuleTeam = -1)
	OR (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iGunOnRuleRule = -1)
	OR (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].wtGunOnRule = WEAPONTYPE_UNARMED)
	OR (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].wtGunOnRule = WEAPONTYPE_INVALID)
		EXIT
	ENDIF
	
	WEAPON_TYPE wtGunToGive = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].wtGunOnRule
	
	IF NOT sPedState.bInjured
	AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedGunOnRuleGiven, iped)
		
		INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iGunOnRuleTeam
		INT iRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iGunOnRuleRule
		
		IF (MC_serverBD_4.iCurrentHighestPriority[iTeam] >= iRule)
		AND (MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES)
			
			BOOL bAtRightPoint = FALSE
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_GunOnRule_OnMidpoint)
				IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],iRule)
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_GunOnRule_OnAggro)
						IF IS_BIT_SET(MC_serverBD.iObjectivePedAggroedBitset[iTeam],iRule)
							PRINTLN("[Peds][Ped ", iPed, "] PROCESS_GIVING_PED_WEAPON_ON_RULE - Ped ", iped," should get a gun because at midpoint and someone has been aggroed, team ",iTeam," rule ",iRule)
							bAtRightPoint = TRUE
						ENDIF
					ELSE
						PRINTLN("[Peds][Ped ", iPed, "] PROCESS_GIVING_PED_WEAPON_ON_RULE - Ped ", iped," should get a gun because at midpoint, team ",iTeam," rule ",iRule)
						bAtRightPoint = TRUE
					ENDIF
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_GunOnRule_OnAggro)
				IF IS_BIT_SET(MC_serverBD.iObjectivePedAggroedBitset[iTeam],iRule)
					PRINTLN("[Peds][Ped ", iPed, "] PROCESS_GIVING_PED_WEAPON_ON_RULE - Ped ", iped," should get a gun someone has been aggroed, team ",iTeam," rule ",iRule)
					bAtRightPoint = TRUE
				ENDIF
			ELSE
				PRINTLN("[Peds][Ped ", iPed, "] PROCESS_GIVING_PED_WEAPON_ON_RULE - Ped ", iped," should get a gun because past rule, team ",iTeam," rule ",iRule)
				bAtRightPoint = TRUE
			ENDIF
			
			
			IF bAtRightPoint
				
				BOOL bGroup = IS_PED_IN_GROUP(tempPed)
				
				IF bGroup
				OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour, ciPED_BSFour_UseActionModeWhenArmedByGOR)
					SET_PED_USING_ACTION_MODE(tempPed, TRUE, -1, "DEFAULT_ACTION")
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour, ciPED_BSFour_UseActionModeWhenArmedByGOR)
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_PLAY_REACTION_ANIMS, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_DISABLE_ENTRY_REACTIONS, TRUE)
				ENDIF
				
				IF bGroup
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,FALSE)
				ENDIF
				
				
				IF GET_PED_WEAPON_TYPE_FROM_CREATOR_OPTION(iPed) = WEAPONTYPE_UNARMED
					INT iCombatStyle = GET_PED_COMBAT_STYLE(iped)
					
					IF iCombatStyle = ciPED_DEFENSIVE
						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FLEE, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FIGHT, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, TRUE)
					ENDIF
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee = ciPED_FLEE_ON
					OR iCombatStyle = ciPED_DEFENSIVE
						INT iTeamLoop
						FOR iTeamLoop = 0 TO (FMMC_MAX_TEAMS-1)
							IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[iTeamLoop] = ciPED_RELATION_SHIP_DISLIKE
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee != ciPED_FLEE_ON
									IF iCombatStyle = ciPED_DEFENSIVE
										SET_PED_CAN_BE_TARGETTED_BY_TEAM(tempPed, iTeamLoop, TRUE)
									ENDIF
								ELSE
									SET_PED_CAN_BE_TARGETTED_BY_TEAM(tempPed, iTeamLoop, TRUE)
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
				
				IF wtGunToGive = WEAPONTYPE_MINIGUN
					SET_PED_FIRING_PATTERN(tempPed,FIRING_PATTERN_FULL_AUTO)
				ELIF wtGunToGive = WEAPONTYPE_RPG
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_RocketPedsAndVehicles)
						GIVE_DELAYED_WEAPON_TO_PED(tempPed, WEAPONTYPE_PISTOL, 25000, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_ROCKETS_AGAINST_VEHICLES_ONLY, TRUE)
						PRINTLN("[Peds][Ped ", iPed, "] PROCESS_GIVING_PED_WEAPON_ON_RULE - setting ped ", iped," with RPG to only fire it at vehicles")
					ENDIF
				ELIF wtGunToGive = WEAPONTYPE_STUNGUN
					SET_PED_DROPS_WEAPONS_WHEN_DEAD(tempPed,FALSE)
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_GunOnRule_Holstered)
					PRINTLN("[Peds][Ped ", iPed, "] PROCESS_GIVING_PED_WEAPON_ON_RULE - giving ped ", iped," a holstered gun")
					GIVE_DELAYED_WEAPON_TO_PED(tempPed, wtGunToGive, 25000, FALSE)
				ELSE
					PRINTLN("[Peds][Ped ", iPed, "] PROCESS_GIVING_PED_WEAPON_ON_RULE - giving ped ", iped," a non-holstered gun")
					GIVE_DELAYED_WEAPON_TO_PED(tempPed, wtGunToGive, 25000, TRUE)
					SET_CURRENT_PED_WEAPON(tempPed, wtGunToGive, TRUE)
				ENDIF
				
				SET_PED_RETASK_DIRTY_FLAG(sPedState)
				
				IF bIsLocalPlayerHost
					PRINTLN("[Peds][Ped ", iPed, "] PROCESS_GIVING_PED_WEAPON_ON_RULE - I am the server; setting iPedGunOnRuleGiven bit for ped ", iped," myself")
					FMMC_SET_LONG_BIT(MC_serverBD.iPedGunOnRuleGiven, iped)
				ELSE
					PRINTLN("[Peds][Ped ", iPed, "] PROCESS_GIVING_PED_WEAPON_ON_RULE - I am a client; broadcasting iPedGunOnRuleGiven bit change for ped ", iped)
					BROADCAST_FMMC_PED_GIVEN_GUNONRULE(iped)
				ENDIF
				
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE(FMMC_PED_STATE &sPedState)
	
	PED_INDEX tempPed = sPedState.pedIndex
	INT iped = sPedState.iIndex
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCombatStyleChange_Team = -1
	OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCombatStyleChange_Rule = -1
	OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCombatStyleChange_NewStyle = -1
		EXIT
	ENDIF
	
	IF sPedState.bExists
	AND NOT sPedState.bInjured
	AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedCombatStyleChanged, iped)
		
		INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Team
		INT iRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Rule
		
		IF (MC_serverBD_4.iCurrentHighestPriority[iTeam] >= iRule)
		AND (MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES)
			
			BOOL bAtRightPoint = FALSE
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPED_BSEleven_CombatStyleChange_OnMidpoint)
				IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iRule)
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPED_BSEleven_CombatStyleChange_OnAggro)
						IF IS_BIT_SET(MC_serverBD.iObjectivePedAggroedBitset[iTeam], iRule)
							PRINTLN("[Peds][Ped ", iPed, "] PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE - Ped ", iped," should get new combat style because at midpoint and someone has been aggroed, team ",iTeam," rule ",iRule)
							bAtRightPoint = TRUE
						ENDIF
					ELSE
						PRINTLN("[Peds][Ped ", iPed, "] PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE - Ped ", iped," should get new combat style because at midpoint, team ",iTeam," rule ",iRule)
						bAtRightPoint = TRUE
					ENDIF
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPED_BSEleven_CombatStyleChange_OnAggro)
				IF IS_BIT_SET(MC_serverBD.iObjectivePedAggroedBitset[iTeam],iRule)
					PRINTLN("[Peds][Ped ", iPed, "] PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE - Ped ", iped," should get new combat style someone has been aggroed, team ",iTeam," rule ",iRule)
					bAtRightPoint = TRUE
				ENDIF
			ELSE
				PRINTLN("[Peds][Ped ", iPed, "] PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE - Ped ", iped," should get new combat style because past rule, team ",iTeam," rule ",iRule)
				bAtRightPoint = TRUE
			ENDIF
			
			
			IF bAtRightPoint
				
				INT iCombatStyle = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_NewStyle
				
				IF iCombatStyle != g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyle
					
					WEAPON_TYPE wtGun = GET_BEST_PED_WEAPON(tempPed, TRUE)
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee != ciPED_FLEE_ON
						INT iTeamLoop
						FOR iTeamLoop = 0 TO (FMMC_MAX_TEAMS-1)
							IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[iTeamLoop] = ciPED_RELATION_SHIP_DISLIKE
								//Only auto-target if they're going to respond with hostility
								IF iCombatStyle = ciPED_AGRESSIVE
								OR iCombatStyle = ciPED_BERSERK
								OR ( iCombatStyle = ciPED_DEFENSIVE AND wtGun != WEAPONTYPE_UNARMED AND wtGun != WEAPONTYPE_INVALID )
									SET_PED_CAN_BE_TARGETTED_BY_TEAM(tempPed, iTeamLoop, TRUE)
								ELSE
									SET_PED_CAN_BE_TARGETTED_BY_TEAM(tempPed, iTeamLoop, FALSE)
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyle = ciPED_BERSERK // If this ped used to be berserk
						SET_PED_CONFIG_FLAG(tempPed, PCF_ShouldChargeNow, FALSE)
						// If we ever let peds go back to being berserk again after this removal, we need to make sure we clear this ped's bit in iPedHasCharged across all machines
					ENDIF
					
					BOOL bCombatGoto
					
					IF MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS
					AND SHOULD_PED_DO_DEFENSIVE_AREA_GOTO(tempPed, iped)
						bCombatGoto = TRUE
					ENDIF
					
					IF NOT bCombatGoto
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_FreeMovementInCombat)
						AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPED_BSEleven_CombatStyleChange_FreeMovementInCombat)
							PRINTLN("[Peds][Ped ", iPed, "] PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE - turning off combat free movement, call MC_SET_UP_FMMC_PED_DEFENSIVE_AREA")
							MC_SET_UP_FMMC_PED_DEFENSIVE_AREA(tempPed, iped, GET_ENTITY_COORDS(tempPed))
						ENDIF
					ENDIF
					
					MC_APPLY_PED_COMBAT_STYLE(tempPed, iCombatStyle, wtGun, iped, bCombatGoto)
					
					SET_PED_RETASK_DIRTY_FLAG(sPedState)
					
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Peds][Ped ", iPed, "] PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE - already has combat style ",iCombatStyle,", don't need to do anything")
				#ENDIF	
				ENDIF
				
				IF bIsLocalPlayerHost
					PRINTLN("[Peds][Ped ", iPed, "] PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE - I am the server; setting iPedCombatStyleChanged bit for ped ", iped," myself")
					FMMC_SET_LONG_BIT(MC_serverBD.iPedCombatStyleChanged, iped)
				ELSE
					PRINTLN("[Peds][Ped ", iPed, "] PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE - I am a client; broadcasting iPedCombatStyleChanged bit change for ped ", iped)
					BROADCAST_FMMC_PED_COMBAT_STYLE_CHANGED(iped)
				ENDIF
				
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_PED_BE_ALLOWED_TO_START_USING_VEHICLE_WEAPON(PED_INDEX ThisPed, INT iPed)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwelve, ciPed_BSTwelve_ReenableVehicleWeaponsWhenAloneInVehicle)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(ThisPed)
		RETURN FALSE
	ENDIF
	
	VEHICLE_INDEX viVeh = GET_VEHICLE_PED_IS_USING(ThisPed)
	MODEL_NAMES mnVeh = GET_ENTITY_MODEL(viVeh)
	
	IF IS_ENTITY_ATTACHED(viVeh)
		PRINTLN("[Peds][Ped ", iPed, "] SHOULD_PED_BE_ALLOWED_TO_START_USING_VEHICLE_WEAPON - Ped's vehicle is attached to something - blocking weapon usage")
		RETURN FALSE
	ENDIF
	
	INT iSeatToCheck = 0
	INT iLivingPeds = 0
	
	FOR iSeatToCheck = ENUM_TO_INT(VS_DRIVER) TO GET_VEHICLE_MODEL_NUMBER_OF_SEATS(mnVeh) - 1
		PED_INDEX piPedInVeh
		piPedInVeh = GET_PED_IN_VEHICLE_SEAT(viVeh, INT_TO_ENUM(VEHICLE_SEAT, iSeatToCheck), TRUE)
		
		IF IS_ENTITY_ALIVE(piPedInVeh)
			iLivingPeds++
		ENDIF
	ENDFOR
	
	IF iLivingPeds = 1
		PRINTLN("[Peds][Ped ", iPed, "] SHOULD_PED_BE_ALLOWED_TO_START_USING_VEHICLE_WEAPON - Returning TRUE")
		RETURN TRUE
	ELSE
		PRINTLN("[Peds][Ped ", iPed, "] SHOULD_PED_BE_ALLOWED_TO_START_USING_VEHICLE_WEAPON - Returning FALSE")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PED_BE_BLOCKED_FROM_USING_VEH_WEAPON(PED_INDEX ThisPed, INT iPed)

	IF SHOULD_PED_BE_ALLOWED_TO_START_USING_VEHICLE_WEAPON(ThisPed, iPed)
		PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_PED_BE_BLOCKED_FROM_USING_VEH_WEAPON - SHOULD_PED_BE_ALLOWED_TO_START_USING_VEHICLE_WEAPON returning TRUE")
		RETURN FALSE
	ENDIF
	
	BOOL bReturn = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_PreventVehicleWeaponUsage)
	
	PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_PED_BE_BLOCKED_FROM_USING_VEH_WEAPON | ", GET_STRING_FROM_BOOL(bReturn))
	RETURN bReturn
ENDFUNC

FUNC BOOL IS_PED_ON_GOTO_ENTITY_TASK_OUT_OF_RANGE(INT iped, FLOAT fTargetDist, FLOAT fGotoRange, FLOAT fSpeed, FLOAT fTargetSpeed, BOOL bHeli, BOOL bPlane)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFifteen, ciPED_BSFifteen_DontUseRangeCheckForGoToEntities)
		RETURN FALSE
	ENDIF
	
	IF (fTargetDist > FMAX( fGotoRange, 15.0 ) // If we're too far away
	OR ((fSpeed > 5.0 OR fTargetSpeed > 5.0) AND NOT bHeli AND NOT bPlane)) // If the target is moving quickly, we can't keep up
		RETURN TRUE		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC VEHICLE_INDEX GET_ORIGINAL_VALID_VEHICLE(INT iped, PED_INDEX tempPed)
	
	VEHICLE_INDEX tempVeh = NULL
	
	INT iVeh = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle
	
	IF iVeh != -1
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			VEHICLE_INDEX tempVeh2 = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			
			IF IS_VEHICLE_DRIVEABLE(tempVeh2, TRUE) AND (NOT IS_ENTITY_ON_FIRE(tempVeh2))
				IF NOT IS_PED_IN_VEHICLE(tempPed, tempVeh2, TRUE)
					IF GET_DISTANCE_BETWEEN_ENTITIES(tempVeh2, tempPed) < 40.0
						IF NOT IS_ANY_PLAYER_IN_VEHICLE(tempVeh2)
							IF GET_ENTITY_SPEED(tempVeh2) < 5.0
								IF IS_VEHICLE_ON_ALL_WHEELS(tempVeh2) OR IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(tempVeh2))
									IF DOES_VEHICLE_HAVE_FREE_SEAT(tempVeh2)
										IF IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(tempVeh2))
											IF IS_ENTITY_IN_WATER(tempVeh2)
												tempVeh = tempVeh2
											ENDIF
										ELSE
											tempVeh = tempVeh2
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN tempVeh
	
ENDFUNC

FUNC FLOAT GET_DRIVE_SPEED(INT iped, FLOAT fDistanceToCompletion, FLOAT fDefaultSpeed)
	
	FLOAT fCruiseSpeed = fDefaultSpeed
	FLOAT fSpeed15 = 5
	FLOAT fSpeed30 = 8
	FLOAT fSpeed50 = 15//25
	FLOAT fSpeed80 = 20//30
	
	//If we're getting close, slow down! - perhaps add an option to change these (a reckless driver or a super-safe driver?)
	
	IF fCruiseSpeed > cfMAX_SPEED_PED_VEH
		fCruiseSpeed = cfMAX_SPEED_PED_VEH
	ENDIF
	IF fDefaultSpeed > cfMAX_SPEED_PED_VEH
		fDefaultSpeed = cfMAX_SPEED_PED_VEH
	ENDIF
	
	IF MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO
	AND MC_serverBD_2.iPedGotoProgress[iped] < MAX_ASSOCIATED_GOTO_TASKS
		IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iped], PED_ASSOCIATED_GOTO_TASK_TYPE_NOSLOWDOWN) 
			RETURN fDefaultSpeed
		ENDIF
	ENDIF
	
	IF fDistanceToCompletion <= 15
		IF fCruiseSpeed > fSpeed15
			fCruiseSpeed = fSpeed15
		ENDIF
	ELIF fDistanceToCompletion <= 30
		IF fCruiseSpeed > fSpeed30
			fCruiseSpeed = fSpeed30
		ENDIF
	ELIF fDistanceToCompletion <= 50
		IF fCruiseSpeed > fSpeed50
			fCruiseSpeed = fSpeed50
		ENDIF
	ELIF fDistanceToCompletion <= 80
		IF fCruiseSpeed > fSpeed80
			fCruiseSpeed = fSpeed80
		ENDIF
	ENDIF
	
	RETURN fCruiseSpeed
	
ENDFUNC

FUNC FLOAT GET_HELI_DRIVE_SPEED(INT iped, FLOAT fdist, FLOAT fDefaultSpeed)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedSpeedOverride > -1
	AND FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iPedSpeedOverride, iPed)
		RETURN TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedSpeedOverride)
	ENDIF	
	RETURN GET_DRIVE_SPEED(iped, fdist, fDefaultSpeed)
ENDFUNC

/// RETURNS:
///    If ped is currently trying to catchup with another ped and the speed that it should use.
FUNC BOOL IS_PED_TRYING_TO_CATCHUP_WITH_ANOTHER_PED(FLOAT &fOutSpeed, PED_INDEX tempPed, INT iThisPed, INT iPedTargetForSpeedMatching)
	FLOAT fCurrentSpeed = fOutSpeed
	IF iPedTargetForSpeedMatching > -1		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPedTargetForSpeedMatching])
			PED_INDEX pedFollowing = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPedTargetForSpeedMatching])
			
			VEHICLE_INDEX vehFollowing, vehTemp	
			FLOAT fRatio
			IF IS_PED_IN_ANY_VEHICLE(pedFollowing)
				vehTemp = GET_VEHICLE_PED_IS_IN(tempPed)
				vehFollowing = GET_VEHICLE_PED_IS_IN(pedFollowing)
				
				IF IS_VEHICLE_DRIVEABLE(vehFollowing)
					VECTOR vForward = GET_ENTITY_FORWARD_VECTOR(vehTemp)
					VECTOR vCoordFollowing = GET_ENTITY_COORDS(vehFollowing)
					VECTOR vCoordTemp = GET_ENTITY_COORDS(vehTemp)
					
					// How far in front or behind we are. Positive Value we are in front, Negative Value we are behind.  (ignoring Z completely)
					// DOT_PRODUCT CROSS_PRODUCT
					fRatio = ((vCoordTemp.x - vCoordFollowing.x) * vForward.x) + ((vCoordTemp.y - vCoordFollowing.y) * vForward.y)
					
    				PRINTLN("[Peds][Ped ", iThisPed, "] - GET_ADJUSTED_CRUISE_SPEED - fRatio: ", fRatio, " vForward: ", vForward, " vCoordFollowing: ", vCoordFollowing, " vCoordTemp: ", vCoordTemp)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(pedFollowing)
			AND ((MC_serverBD_2.iPedGotoProgress[iPedTargetForSpeedMatching] > -1 AND MC_serverBD_2.iPedGotoProgress[iPedTargetForSpeedMatching] < MAX_ASSOCIATED_GOTO_TASKS) OR NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iThisPed].iPedBitsetSeventeen, ciPed_BSSeventeen_AdjustSpeedWithTargetOnlyIfTheyAreDoingGoto))
			
				FLOAT fDist = VDIST2(GET_ENTITY_COORDS(pedFollowing, FALSE), GET_ENTITY_COORDS(tempPed, FALSE))				
				FLOAT DistanceThreshold = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iThisPed].fDistanceThreshForSpeedMatching
				
				BOOL bSpeedUp
				
				IF fRatio < 0
				AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iThisPed].iPedBitsetSeventeen, ciPed_BSSeventeen_AdjustSpeedWithTarget_SpeedDirection_InvertWhenBehindTarget)
					PRINTLN("[Peds][Ped ", iThisPed, "] - GET_ADJUSTED_CRUISE_SPEED - Behind target, inverting speed toggle.")
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iThisPed].iPedBitsetSeventeen, ciPed_BSSeventeen_AdjustSpeedWithTarget_SpeedDirection_Faster)
						bSpeedUp = FALSE
					ELSE
						bSpeedUp = TRUE
					ENDIF
					
				ELIF fRatio > 0
				AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iThisPed].iPedBitsetSeventeen, ciPed_BSSeventeen_AdjustSpeedWithTarget_SpeedDirection_InvertWhenInfrontTarget)
					PRINTLN("[Peds][Ped ", iThisPed, "] - GET_ADJUSTED_CRUISE_SPEED - In front of target, inverting speed toggle.")
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iThisPed].iPedBitsetSeventeen, ciPed_BSSeventeen_AdjustSpeedWithTarget_SpeedDirection_Faster)
						bSpeedUp = FALSE
					ELSE
						bSpeedUp = TRUE
					ENDIF
					
				ELSE
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iThisPed].iPedBitsetSeventeen, ciPed_BSSeventeen_AdjustSpeedWithTarget_SpeedDirection_Faster)
						bSpeedUp = TRUE
					ELSE
						bSpeedUp = FALSE
					ENDIF
				ENDIF
				
				IF bSpeedUp
					// Speed Up
					IF fDist >= POW(DistanceThreshold, 2.0)
						fOutSpeed *= 3.0
						PRINTLN("[Peds][Ped ", iThisPed, "] - GET_ADJUSTED_CRUISE_SPEED - (Speed Up) (5) Ped is quite far, adjusting speed to catch up return: ", fOutSpeed, " fDist: ", fDist)
					ELIF fDist >= POW(DistanceThreshold*0.9, 2.0)
						fOutSpeed *= 2.5
						PRINTLN("[Peds][Ped ", iThisPed, "] - GET_ADJUSTED_CRUISE_SPEED - (Speed Up) (4) Ped is quite far, adjusting speed to catch up return: ", fOutSpeed, " fDist: ", fDist)
					ELIF fDist >= POW(DistanceThreshold*0.8, 2.0)
						fOutSpeed *= 2.0
						PRINTLN("[Peds][Ped ", iThisPed, "] - GET_ADJUSTED_CRUISE_SPEED - (Speed Up) (3) Ped is quite far, adjusting speed to catch up return: ", fOutSpeed, " fDist: ", fDist)
					ELIF fDist >= POW(DistanceThreshold*0.7, 2.0)
						fOutSpeed *= 1.5
						PRINTLN("[Peds][Ped ", iThisPed, "] - GET_ADJUSTED_CRUISE_SPEED - (Speed Up) (2) Ped is quite far, adjusting speed to catch up return: ", fOutSpeed, " fDist: ", fDist)
					ELIF fDist >= POW(DistanceThreshold*0.6, 2.0)
						fOutSpeed *= 1.25
						PRINTLN("[Peds][Ped ", iThisPed, "] - GET_ADJUSTED_CRUISE_SPEED - (Speed Up) (1) Ped is quite far, adjusting speed to catch up return: ", fOutSpeed, " fDist: ", fDist)
					ELSE
						PRINTLN("[Peds][Ped ", iThisPed, "] - GET_ADJUSTED_CRUISE_SPEED - (Speed Up) Not Adjusting speed, fDist: ", fDist)
					ENDIF
				ELSE
					// Slow Down	
					IF fDist >= POW(DistanceThreshold, 2.0)
						fOutSpeed *= 0.5
						PRINTLN("[Peds][Ped ", iThisPed, "] - GET_ADJUSTED_CRUISE_SPEED - (Slow Down) (5) Ped is quite far, adjusting speed to catch up return: ", fOutSpeed, " fDist: ", fDist)
					ELIF fDist >= POW(DistanceThreshold*0.9, 2.0)
						fOutSpeed *= 0.6
						PRINTLN("[Peds][Ped ", iThisPed, "] - GET_ADJUSTED_CRUISE_SPEED - (Slow Down) (4) Ped is quite far, adjusting speed to catch up return: ", fOutSpeed, " fDist: ", fDist)
					ELIF fDist >= POW(DistanceThreshold*0.8, 2.0)
						fOutSpeed *= 0.7
						PRINTLN("[Peds][Ped ", iThisPed, "] - GET_ADJUSTED_CRUISE_SPEED - (Slow Down) (3) Ped is quite far, adjusting speed to catch up return: ", fOutSpeed, " fDist: ", fDist)
					ELIF fDist >= POW(DistanceThreshold*0.7, 2.0)
						fOutSpeed *= 0.8
						PRINTLN("[Peds][Ped ", iThisPed, "] - GET_ADJUSTED_CRUISE_SPEED - (Slow Down) (2) Ped is quite far, adjusting speed to catch up return: ", fOutSpeed, " fDist: ", fDist)
					ELIF fDist >= POW(DistanceThreshold*0.6, 2.0)
						fOutSpeed *= 0.9
						PRINTLN("[Peds][Ped ", iThisPed, "] - GET_ADJUSTED_CRUISE_SPEED - (Slow Down) (1) Ped is quite far, adjusting speed to catch up return: ", fOutSpeed, " fDist: ", fDist)
					ELSE
						PRINTLN("[Peds][Ped ", iThisPed, "] - GET_ADJUSTED_CRUISE_SPEED - (Slow Down) Not Adjusting speed, fDist: ", fDist)
					ENDIF
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
	
	//Did we modified the speed?
	RETURN fCurrentSpeed != fOutSpeed
ENDFUNC

FUNC FLOAT GET_ADJUSTED_CRUISE_SPEED(PED_INDEX tempPed, INT iPed, FLOAT speed)
	IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iped], PED_ASSOCIATED_GOTO_TASK_TYPE_ACCELCAP) 
	AND GET_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_PED_ACCELERATION_CAP(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData) > 0.0
		FLOAT currentSpeed = GET_ENTITY_SPEED(tempPed)
		FLOAT maxAcceleration = GET_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_PED_ACCELERATION_CAP(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData)
		PRINTLN("[Peds][Ped ", iPed, "] - GET_ADJUSTED_CRUISE_SPEED - ped ",iped, " desired speed: ", speed, " acc cap: ", maxAcceleration, " current: ", currentSpeed)
		
		IF HAS_NET_TIMER_STARTED(maxAccelerationUpdateTimers[iPed])
			IF HAS_NET_TIMER_EXPIRED(maxAccelerationUpdateTimers[iPed], 1000)
				maxAccelerationUpdateTimers[iPed].Timer = GET_TIME_OFFSET(maxAccelerationUpdateTimers[iPed].Timer, 1000)//INT_TO_NATIVE(TIME_DATATYPE, NATIVE_TO_INT(maxAccelerationUpdateTimers[iPed].Timer) + 1000)
			ELSE
				PRINTLN("[Peds][Ped ", iPed, "] - GET_ADJUSTED_CRUISE_SPEED - wait timer, return prev value: ", maxAccelerationCurrentSpeed[iPed])
				RETURN maxAccelerationCurrentSpeed[iPed]
			ENDIF
		ELSE
			START_NET_TIMER(maxAccelerationUpdateTimers[iPed], DEFAULT, TRUE)
		ENDIF
		
		IF speed < currentSpeed-maxAcceleration
			speed = currentSpeed-maxAcceleration
		ENDIF
		IF speed > currentSpeed+maxAcceleration
			speed = currentSpeed+maxAcceleration
		ENDIF
		maxAccelerationCurrentSpeed[iPed] = speed
		PRINTLN("[Peds][Ped ", iPed, "] - GET_ADJUSTED_CRUISE_SPEED - return: ",speed)
	ENDIF
	
	INT iTeam = 0
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSeventeen, ciPed_BSSeventeen_UseRuleVehicleSpeedOverrideIfValid_Team0 + iTeam)		
			INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
			IF iRule >= FMMC_MAX_RULES
				RELOOP
			ENDIF			
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fPedDriveSpeedOverride[iRule] = 0.0
				RELOOP					
			ENDIF
			PRINTLN("[Peds][Ped ", iPed, "] - GET_ADJUSTED_CRUISE_SPEED - Rule fPedDriveSpeedOverride return: ",g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fPedDriveSpeedOverride[iRule])
			RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fPedDriveSpeedOverride[iRule]
		ENDIF
	ENDFOR
	
	IF NOT IS_PED_TRYING_TO_CATCHUP_WITH_ANOTHER_PED(speed, tempPed, iPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedTargetForSpeedMatching)
		//If we are not trying to match the speed with the first target check the second target
		IS_PED_TRYING_TO_CATCHUP_WITH_ANOTHER_PED(speed, tempPed, iPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedTargetForSpeedMatching2)
	ENDIF
	
	RETURN speed
ENDFUNC

FUNC BOOL SHOULD_PED_LEAVE_VEHICLE_ON_CURRENT_OBJECTIVE(PED_INDEX &pIndex, INT &iIndex)
	//Is ped in a vehicle and is the iExitVehicleAtRule rule set for this ped?
	IF IS_PED_IN_ANY_VEHICLE(pIndex, TRUE) AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iIndex].iExitVehicleAtRule > -1
		//Did the selected team reached the objective the rule is associated with?
		INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iIndex].iExitVehicleTeam
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iIndex].iExitVehicleAtRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
			//Ped should leave the vehicle
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PED_GETOUT(INT iped)
	
	BOOL bShouldGetOut = FALSE
	
	PED_INDEX tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
	
	IF IS_PED_IN_ANY_VEHICLE(tempPed)
		IF MC_serverBD_2.iTargetID[iped] != ciSECONDARY_GOTO
			IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_DontGetOutAtEndOfGoto))
			AND (MC_serverBD_2.iPedGotoProgress[iped] >= (GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, FALSE) - 1)) //If the ped is reaching the end of their gotos
			AND NOT (IS_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, ciENTIRE_TASK_BITSET_LOOP_GOTO)) //If they shouldn't continue looping through them
			AND NOT ((g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRange = cfMAX_PATROL_RANGE) OR (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee = ciPED_FLEE_ON) ) //If they aren't set to wander/flee at the end
				MODEL_NAMES mnVeh = GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(tempPed))
				IF NOT (IS_THIS_MODEL_A_BOAT(mnVeh) OR (mnVeh = SUBMERSIBLE) ) //If they're not in a boat/sub
					bShouldGetOut = TRUE //They should get out!
				ENDIF
			ENDIF
		ELSE
			IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset,ciPED_BS_DontGetOutAtEndOfGoto) )
			AND NOT (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetNine, ciPed_BSNine_DontExitOnSecondaryArrival) AND FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSecondaryTaskActive, iPed))
				MODEL_NAMES mnVeh = GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(tempPed))
				IF NOT (IS_THIS_MODEL_A_BOAT(mnVeh) OR (mnVeh = SUBMERSIBLE) ) //If they're not in a boat/sub
					bShouldGetOut = TRUE //They should get out!
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_NeverLeaveVehicle)
		PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_PED_GETOUT - iPed: ", iPed, " Forcing bShouldGetOut to FALSE due to ciPed_BSEight_NeverLeaveVehicle")
		bShouldGetOut = FALSE
	ENDIF
	
	PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_PED_GETOUT - Returning: ", BOOL_TO_STRING(bShouldGetOut))
	RETURN bShouldGetOut
	
ENDFUNC

FUNC BOOL HAS_PED_EXITED_VEHICLE_FOR_ASSOCIATED_GOTO(FMMC_PED_STATE &sPedState)

	INT iPed = sPedState.iIndex
	
	IF NOT sPedState.bIsInAnyVehicle
		PRINTLN("[Peds][Ped ", iPed, "] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - returning TRUE, should get out of vehicle & no longer in vehicle")
		RETURN TRUE
	ELSE
		//If we're leaving due to completion of gotos		
		IF NOT IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.sPointData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_EXIT_VEHICLE)
			IF IS_COMBAT_VEHICLE(sPedState.vehIndexPedIsIn)
			OR IS_PED_IN_ANY_HELI(sPedState.pedIndex)
			OR IS_PED_IN_ANY_PLANE(sPedState.pedIndex)
				PRINTLN("[Peds][Ped ", iPed, "] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - returning TRUE, done with GoTos and in a vehicle we don't need to get out of")
				RETURN TRUE
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[Peds][Ped ", iPed, "] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - returning TRUE, done with GoTos but still need to get out of this vehicle")
			#ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[Peds][Ped ", iPed, "] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - returning FALSE, exit bitset is true and still need to get out of this vehicle")
		#ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_CLEANUP_ASSOCIATED_GOTO_NON_ACTION_OPTIONS(FMMC_PED_STATE &sPedState)

	IF MC_serverBD_2.iPedState[sPedState.iIndex] = ciTASK_GOTO_COORDS
		EXIT
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(iPedGotoAssInvincibilityBitset, sPedState.iIndex)
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][CLIENT] - PROCESS_CLEANUP_ASSOCIATED_GOTO_NON_ACTION_OPTIONS - Clearing Ped Invincibility.")
		FMMC_CLEAR_LONG_BIT(iPedGotoAssInvincibilityBitset, sPedState.iIndex)
		IF DOES_ENTITY_EXIST(sPedState.pedIndex)
			SET_ENTITY_INVINCIBLE(sPedState.pedIndex, FALSE)
		ENDIF
		IF DOES_ENTITY_EXIST(sPedState.vehIndexPedIsIn)
			SET_ENTITY_INVINCIBLE(sPedState.vehIndexPedIsIn, FALSE)
		ENDIF
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ToggleGotoInvincible, DEFAULT, sPedState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE)
	ENDIF
	
ENDPROC

PROC PROCESS_ASSOCIATED_GOTO_NON_ACTION_OPTIONS(FMMC_PED_STATE &sPedState)
	
	IF NOT FMMC_IS_LONG_BIT_SET(iPedGotoAssInvincibilityBitset, sPedState.iIndex)		
		IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_INVINCIBILITY_ENABLE) 
		AND IS_ENTITY_ALIVE(sPedState.pedIndex)
			FMMC_SET_LONG_BIT(iPedGotoAssInvincibilityBitset, sPedState.iIndex)
			SET_ENTITY_INVINCIBLE(sPedState.pedIndex, TRUE)
			IF IS_ENTITY_ALIVE(sPedState.vehIndexPedIsIn)
				SET_ENTITY_INVINCIBLE(sPedState.vehIndexPedIsIn, TRUE)
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][CLIENT] - PROCESS_ASSOCIATED_GOTO_NON_ACTION_OPTIONS - Setting Ped and Ped Vehicle Invincibility.")
			ELSE 
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][CLIENT] - PROCESS_ASSOCIATED_GOTO_NON_ACTION_OPTIONS - Setting Ped Invincibility.")
			ENDIF
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ToggleGotoInvincible, DEFAULT, sPedState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
		ENDIF
	ELIF FMMC_IS_LONG_BIT_SET(iPedGotoAssInvincibilityBitset, sPedState.iIndex)		
		IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_INVINCIBILITY_DISABLE) 
		AND IS_ENTITY_ALIVE(sPedState.pedIndex)
			FMMC_CLEAR_LONG_BIT(iPedGotoAssInvincibilityBitset, sPedState.iIndex)
			SET_ENTITY_INVINCIBLE(sPedState.pedIndex, FALSE)
			IF IS_ENTITY_ALIVE(sPedState.vehIndexPedIsIn)
				SET_ENTITY_INVINCIBLE(sPedState.vehIndexPedIsIn, FALSE)
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][CLIENT] - PROCESS_ASSOCIATED_GOTO_NON_ACTION_OPTIONS - Clearing Ped and Ped Vehicle Invincibility.")
			ELSE
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][CLIENT] - PROCESS_ASSOCIATED_GOTO_NON_ACTION_OPTIONS - Clearing Ped Invincibility.")
			ENDIF
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ToggleGotoInvincible, DEFAULT, sPedState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE)
		ENDIF
	ENDIF
		
ENDPROC 
						
FUNC BOOL ARE_ANY_ASSOCIATED_GOTO_ACTIONS_SET(FMMC_PED_STATE &sPedState)

	VEHICLE_INDEX viVeh
	viVeh = NULL
	INT iPed = sPedState.iIndex
	
	IF MC_serverBD_2.iPedGotoProgress[iPed] >= MAX_ASSOCIATED_GOTO_TASKS
		RETURN FALSE
	ENDIF
	
	IF IS_ASSOCIATED_GOTO_ENTER_VEH_VALID(iPed, MC_serverBD_2.iPedGotoProgress[iPed], viVeh)
	OR IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_HOVER)
	OR IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_RAPPEL)
	OR IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_LANDVEH) 
	OR IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_WAIT_FOR_PLAYERS_TO_LEAVE)
	OR IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_EXIT_VEHICLE)
	OR IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_PLAY_IDLE_ANIM_ON_WAIT)
	OR IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_ACHIEVEHEADING)
	OR IS_ASSOCIATED_GOTO_TASK_DATA__WAIT_TIME_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed])
	OR IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_ENABLE_RAPPELLING)
	OR IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_FORCE_RAPPELLING)
	OR IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_CLEANUP)
	OR IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_COMBAT_GOTO)
	OR IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_COMBAT_AT_WAIT)
	OR IS_ASSOCIATED_GOTO_TASK_DATA__A_CUSTOM_SCENARIO(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed])
	OR IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_DISABLELOCKON) 
		RETURN TRUE	
	ENDIF
	
	RETURN FALSE
ENDFUNC 

FUNC BOOL HAS_SERVER_ASSOCIATED_GOTO_ACTIONS_COMPLETED(FMMC_PED_STATE &sPedState, FLOAT fDist, FLOAT fGotoSize)
	BOOL bArrived = TRUE
	INT iPed = sPedState.iIndex
	
	IF (fDist < fGotoSize OR SHOULD_SKIP_GOTO_RANGE_CHECK_FOR_CUSTOM_SCENARIO(sPedState))
	AND WAIT_FORCARGOBOB_DETACH(sPedState.pedIndex, iped, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle)		
		IF ARE_ANY_ASSOCIATED_GOTO_ACTIONS_SET(sPedState)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Checking Doing GOTO (Action) -------- ")
						
			IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_DISABLELOCKON) 
			
				IF NOT FMMC_IS_LONG_BIT_SET(iPedGotoAssCompletedDisableLockOnBitset, sPedState.iIndex)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][DisableLockOn] - HAS_SERVER_ASSOCIATED_GOTO_ACTIONS_COMPLETED - Returning false. Still waiting to disable lock on to ped")
					bArrived = FALSE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][DisableLockOn] - HAS_SERVER_ASSOCIATED_GOTO_ACTIONS_COMPLETED - No longer waiting to disable lock on to ped")
				#ENDIF
				ENDIF
			ENDIF
			
			VEHICLE_INDEX viVeh
			IF IS_ASSOCIATED_GOTO_ENTER_VEH_VALID(sPedState.iIndex, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], viVeh)
				IF NOT FMMC_IS_LONG_BIT_SET(iPedGotoAssCompletedEnterVehicleBitset, sPedState.iIndex)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][EnterVeh] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Returning false. Still waiting to Enter Veh.")
					bArrived = FALSE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][EnterVeh] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - No longer waiting for peds to Enter Veh.")
				#ENDIF
				ENDIF
			ENDIF
						
			IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_ENABLE_RAPPELLING) 
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_ALLOW_RAPPELLING_FROM_HELICOPTERS)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][PlayerRappel] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Setting SBBOOL8_ALLOW_RAPPELLING_FROM_HELICOPTERS")
					SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_ALLOW_RAPPELLING_FROM_HELICOPTERS)
				ENDIF
			ENDIF
						
			IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_FORCE_RAPPELLING) 
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_FORCE_RAPPELLING_FROM_HELICOPTERS)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][PlayerRappelForce] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Setting SBBOOL8_FORCE_RAPPELLING_FROM_HELICOPTERS")
					SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_ALLOW_RAPPELLING_FROM_HELICOPTERS)
					SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_FORCE_RAPPELLING_FROM_HELICOPTERS)
				ENDIF
			ENDIF
						
			IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_HOVER)
				IF NOT FMMC_IS_LONG_BIT_SET(iPedGotoAssCompletedHoverHeliBitset, sPedState.iIndex)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][Hover] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Returning false. Still waiting to hover at destination.")
					bArrived = FALSE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][Hover] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - No longer waiting for peds to hover at destination.")
				#ENDIF
				ENDIF
			ENDIF
			
			IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_RAPPEL) 
				IF NOT FMMC_IS_LONG_BIT_SET(iPedGotoAssCompletedRappelFromVehBitset, sPedState.iIndex)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][Rappel] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Returning false. Still waiting for peds to Rappel from the vehicle.")
					bArrived = FALSE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][Rappel] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - No longer waiting for peds to Rappel from the vehicle.")
				#ENDIF
				ENDIF
			ENDIF
			
			IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_LANDVEH) 
				IF NOT FMMC_IS_LONG_BIT_SET(iPedGotoAssCompletedLandVehicleBitset, sPedState.iIndex)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][LandVeh] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Returning false. Still waiting to land veh")
					bArrived = FALSE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][LandVeh] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - No longer waiting to land veh")
				#ENDIF
				ENDIF
			ENDIF
						
			IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_WAIT_FOR_PLAYERS_TO_LEAVE) 
				IF NOT FMMC_IS_LONG_BIT_SET(iPedGotoAssCompletedWaitPlayerVehExitBitset, sPedState.iIndex)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][PlayerLeaves] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Returning false. Still waiting for players to leave veh")
					bArrived = FALSE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][PlayerLeaves] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - No longer waiting for players to leave veh")
				#ENDIF
				ENDIF
			ENDIF
			
			IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_EXIT_VEHICLE) 				
				IF NOT FMMC_IS_LONG_BIT_SET(iPedGotoAssCompletedLeaveVehicleBitset, sPedState.iIndex)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][LeaveVeh] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Returning false. Still waiting for ped to exit vehicle.")
					bArrived = FALSE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][LeaveVeh] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - No longer waiting for ped to exit vehicle.")
				#ENDIF
				ENDIF
			ENDIF
			
			IF (MC_serverBD_2.iPedGotoProgress[iPed] < MAX_ASSOCIATED_GOTO_TASKS)
			AND IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_ACHIEVEHEADING) 
				IF NOT FMMC_IS_LONG_BIT_SET(iPedGotoAssCompletedAchieveHeadingBitset, sPedState.iIndex)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][AchieveHeading] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Returning false. Still waiting for Heading.")
					bArrived = FALSE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][AchieveHeading] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - No longer waiting for Heading.")
				#ENDIF
				ENDIF
			ENDIF
			
			IF (MC_serverBD_2.iPedGotoProgress[iPed] < MAX_ASSOCIATED_GOTO_TASKS)			
			AND IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_COMBAT_GOTO) 
				IF NOT FMMC_IS_LONG_BIT_SET(iPedGotoAssCompletedCombatGotoBitset, sPedState.iIndex)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][CombatGotoDefensiveAreaRefresh] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Returning false. Still waiting for Defensive Area Refresh.")
					bArrived = FALSE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][CombatGotoDefensiveAreaRefresh] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - No longer waiting for Defensive Area Refresh.")
				#ENDIF
				ENDIF				
			ENDIF

			IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_PLAY_IDLE_ANIM_ON_WAIT) 
				IF NOT FMMC_IS_LONG_BIT_SET(iPedGotoAssCompletedWaitPlayIdleBitset, sPedState.iIndex)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][WaitIdleAnim] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Returning false. Still waiting for IdleAnim to play.")
					bArrived = FALSE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][WaitIdleAnim] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - No longer waiting for Heading.")
				#ENDIF
				ENDIF
			ENDIF
				
			IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_COMBAT_AT_WAIT) 
				IF NOT FMMC_IS_LONG_BIT_SET(iPedGotoAssCompletedWaitCombatBitset, sPedState.iIndex)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][WaitCombat] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Returning false. Still waiting for ped to be tasked to combat.")
					bArrived = FALSE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][WaitCombat] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - No longer waiting for Combat checks.")
				#ENDIF
				ENDIF
			ENDIF
			
			IF IS_ASSOCIATED_GOTO_TASK_DATA__WAIT_TIME_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex])
				IF NOT FMMC_IS_LONG_BIT_SET(iPedGotoAssCompletedWaitBitset, sPedState.iIndex)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][Wait] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Returning false. Still waiting on Wait Time.")
					bArrived = FALSE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][Wait] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - No longer waiting on Wait Time")
				#ENDIF
				ENDIF
			ENDIF			
						
			IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_CLEANUP) 
				IF NOT FMMC_IS_LONG_BIT_SET(iPedGotoAssCompletedCleanupBitset, sPedState.iIndex)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][Wait] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Returning false. Still waiting on Cleanup.")
					bArrived = FALSE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][Wait] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - No longer waiting on Cleanup")
				#ENDIF
				ENDIF				
			ENDIF
			
			IF IS_ASSOCIATED_GOTO_TASK_DATA__A_CUSTOM_SCENARIO(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex])
				IF NOT FMMC_IS_LONG_BIT_SET(iPedGotoAssCompletedCustomScenarioBitset, sPedState.iIndex)
					PRINTLN("[Ped ", sPedState.iIndex, " Task][SERVER][CustomScenario] - HAS_SERVER_ASSOCIATED_GOTO_ACTIONS_COMPLETED - Returning false. Still waiting on Custom Scenario.")
					bArrived = FALSE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Ped ", sPedState.iIndex, " Task][SERVER][CustomScenario] - HAS_SERVER_ASSOCIATED_GOTO_ACTIONS_COMPLETED - No longer waiting on Custom Scenario")
				#ENDIF
				ENDIF				
			ENDIF
		ENDIF
	ELSE
		bArrived = FALSE
	ENDIF
	
	RETURN bArrived
ENDFUNC

FUNC FLOAT GET_CUSTOM_GOTO_ARRIVAL_RANGE(FMMC_PED_STATE &sPedState, FMMC_PED_VEHICLE_DATA &sPedVehicleData, FLOAT &fGotoSize)
	FLOAT fCustomGotoSize = 0
	
	IF (MC_serverBD_2.iPedGotoProgress[sPedState.iIndex] < MAX_ASSOCIATED_GOTO_TASKS)
		fCustomGotoSize = GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_ARRIVAL_RADIUS(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex])
	ENDIF
		
	IF fCustomGotoSize = 0
		INT iCustomGotoSize = GET_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_ARRIVAL_RADIUS(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData)
		fCustomGotoSize = TO_FLOAT(iCustomGotoSize)
	ENDIF
	
	IF fCustomGotoSize != 0
		fGotoSize = fCustomGotoSize
	ELSE 
		fGotoSize = 2.0
		
		IF(sPedVehicleData.bInVeh)
			fGotoSize = 10
			
			IF sPedVehicleData.bPlane			
			AND NOT IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[sPedState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_PLANETAXI) 
				fGotoSize = 130
			ENDIF
			IF sPedVehicleData.bHeli
				fGotoSize = 60
			ENDIF
		ENDIF
	ENDIF
	
	IF fGotoSize < 1.5
		fGotoSize = 1.5
	ENDIF
		
	RETURN fGotoSize
ENDFUNC

FUNC BOOL HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION(FMMC_PED_STATE &sPedState, FMMC_PED_VEHICLE_DATA &sPedVehicleData)
	
	INT iPed = sPedState.iIndex
	
	BOOL bArrived = TRUE
	VECTOR vGotoCoords
	FLOAT fGotoSize = GET_GOTO_RANGE_DEFAULT_VALUE(sPedState, sPedVehicleData)	
	 
	fGotoSize = GET_CUSTOM_GOTO_ARRIVAL_RANGE(sPedState, sPedVehicleData, fGotoSize)
		
	IF MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO
		
		vGotoCoords = GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed])
		fGotoSize = GET_CUSTOM_GOTO_ARRIVAL_RANGE(sPedState, sPedVehicleData, fGotoSize)
		VECTOR vPed = GET_FMMC_PED_COORDS(sPedState)
		FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(vPed, vGotoCoords)
		
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - at ", vPed, " going to ", vGotoCoords,", dist ", fDist, " vs fGotoSize ", fGotoSize)
		
		bArrived = HAS_SERVER_ASSOCIATED_GOTO_ACTIONS_COMPLETED(sPedState, fDist, fGotoSize)
		
	ELIF MC_serverBD_2.iTargetID[iped] = ciSECONDARY_GOTO // Go To Interrupt
				
		vGotoCoords = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vSecondaryVector
		
		IF GET_DISTANCE_BETWEEN_COORDS(GET_FMMC_PED_COORDS(sPedState), vGotoCoords) < fGotoSize //iGotoSize
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - at - returning TRUE for secondary goto; vGotoCoords ",vGotoCoords,", distance to coords ",GET_DISTANCE_BETWEEN_COORDS(GET_FMMC_PED_COORDS(sPedState), vGotoCoords),", goto size ",fGotoSize)
			bArrived = TRUE
		ELSE
			bArrived = FALSE
		ENDIF
				
	ENDIF
	
	PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - bArrived: ", bArrived)
		
	RETURN bArrived
	
ENDFUNC

PROC PROCESS_PED_SHOULD_MOVE_ON_TO_SPECIFIC_GOTO_PROGRESS_INDEX_CLIENT(FMMC_PED_STATE &sPedState)
	
	IF bIsAnySpectator
		EXIT
	ENDIF
		
	IF FMMC_IS_LONG_BIT_SET(iPedGotoAssProgressOverrideEventFired, sPedState.iIndex)
		IF NOT DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME()
			EXIT
		ELSE
			FMMC_CLEAR_LONG_BIT(iPedGotoAssProgressOverrideEventFired, sPedState.iIndex)
		ENDIF
	ENDIF
	
	IF NOT sPedState.bExists
	OR sPedState.bInjured
		EXIT
	ENDIF
		
	IF NOT sPedState.bHasControl
		EXIT
	ENDIF
		
	INT iTeam = MC_PlayerBD[iLocalPart].iTeam
	INT iRule = GET_LOCAL_PLAYER_CURRENT_RULE()
	
	IF iRule >= FMMC_MAX_RULES
	OR iRule <= -1
		EXIT
	ENDIF
	
	INT iAssignmentIndex = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPedAssGotoTask_RulePoolAssignmentIndex[iRule][sPedState.iIndex]	
	INT iAssignmentStartingGoto = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPedAssGotoTask_RulePoolAssignmentStartingGoto[iRule][sPedState.iIndex]
	
	IF iAssignmentIndex = ASSOCIATED_GOTO_TASK_POOL_INDEX__OFF
		EXIT
	ENDIF
	
	PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][CLIENT] - PROCESS_PED_SHOULD_MOVE_ON_TO_SPECIFIC_GOTO_PROGRESS_INDEX_CLIENT - Broadcasting event to override peds current gotos. iPedAssGotoTask_RulePoolAssignmentIndex: ", iAssignmentIndex, " iPedAssGotoTask_RulePoolAssignmentStartingGoto: ", GET_INDEX_FROM_MULTIPLE_INDEX_OPTION(iAssignmentStartingGoto, ASSOCIATED_GOTO_TASK_POOL_START__AMOUNT_OF_OPTIONS, TRUE))
		
	BOOL bInterrupt = (iAssignmentStartingGoto % 2 = 1) OR (iAssignmentStartingGoto = ASSOCIATED_GOTO_TASK_POOL_START__STAY_ON_CURRENT_AND_INTERRUPT)

	FMMC_SET_LONG_BIT(iPedGotoAssProgressOverrideEventFired, sPedState.iIndex)
	BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_OverridePedAssGotoProgress, DEFAULT, sPedState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bInterrupt, iAssignmentIndex, GET_INDEX_FROM_MULTIPLE_INDEX_OPTION(iAssignmentStartingGoto, ASSOCIATED_GOTO_TASK_POOL_START__AMOUNT_OF_OPTIONS, TRUE))
	
ENDPROC

PROC PROCESS_PED_SHOULD_MOVE_ON_TO_SPECIFIC_GOTO_PROGRESS_INDEX_SERVER(FMMC_PED_STATE &sPedState)
	
	IF FMMC_IS_LONG_BIT_SET(iPedGotoAssProgressOverrideEventComplete, sPedState.iIndex)
		IF NOT DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME()
			EXIT
		ELSE
			FMMC_CLEAR_LONG_BIT(iPedGotoAssProgressOverrideEventComplete, sPedState.iIndex)
		ENDIF
	ENDIF
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF NOT FMMC_IS_LONG_BIT_SET(iPedGotoAssProgressOverrideEventFired, sPedState.iIndex)
		EXIT
	ENDIF
	
	IF NOT sPedState.bExists
	OR sPedState.bInjured
		EXIT
	ENDIF
	
	IF iPedGotoAssProgressOverridePool[sPedState.iIndex] = ASSOCIATED_GOTO_TASK_POOL_INDEX__OFF
		EXIT
	ENDIF
	
	PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER] - PROCESS_PED_SHOULD_MOVE_ON_TO_SPECIFIC_GOTO_PROGRESS_INDEX_SERVER - Broadcasting event to override peds current gotos. MC_serverBD_2.iPedGotoProgress: ", iPedGotoAssProgressOverrideProgress[sPedState.iIndex], " MC_serverBD_2.iAssociatedGotoPoolIndex: ", iPedGotoAssProgressOverridePool[sPedState.iIndex], " Interrupt/Retask: ", FMMC_IS_LONG_BIT_SET(iPedGotoAssProgressOverrideInterrupt, sPedState.iIndex))
		
	IF iPedGotoAssProgressOverrideProgress[sPedState.iIndex] >= ASSOCIATED_GOTO_TASK_POOL_START__0
		MC_serverBD_2.iPedGotoProgress[sPedState.iIndex] = iPedGotoAssProgressOverrideProgress[sPedState.iIndex]
	ENDIF
	
	MC_serverBD_2.iAssociatedGotoPoolIndex[sPedState.iIndex] = iPedGotoAssProgressOverridePool[sPedState.iIndex]
	
	IF sPedState.bIsInAnyVehicle
		IF IS_VEHICLE_DRIVEABLE(sPedState.vehIndexPedIsIn)
		AND (IS_PED_IN_ANY_HELI(sPedState.pedIndex)
		OR IS_PED_IN_ANY_PLANE(sPedState.pedIndex))			
			SET_PED_RETASK_DIRTY_FLAG(sPedState)
			
			// TASK_HELI_MISSION / TASK_PLANE_MISSION does not finish when we arrive at a destination. We need to interrupt it otherwise the mission checks the new position we are not tasked to go toward.
			#IF IS_DEBUG_BUILD
			IF NOT FMMC_IS_LONG_BIT_SET(iPedGotoAssProgressOverrideInterrupt, sPedState.iIndex)
				ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_IGNORE, ENTITY_RUNTIME_ERROR_TYPE_WARNING_PED, "In Plane/Heli with new goto pool. Task Auto Interrupted otherwise they get stuck.", sPedState.iIndex)
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(iPedGotoAssProgressOverrideInterrupt, sPedState.iIndex)	
		SET_PED_RETASK_DIRTY_FLAG(sPedState)
	ENDIF
	
	FMMC_SET_LONG_BIT(iPedGotoAssProgressOverrideEventComplete, sPedState.iIndex)
	BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ClearOverridePedAssGotoProgress, DEFAULT, sPedState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
	
ENDPROC 

///Works out if a ped has finished all their gotos - returns false if not yet, true if they have but have no loop,
/// and false with bGoToIsLooped as true if they have but they're supposed to loop around
FUNC BOOL SHOULD_PED_MOVE_ON_FROM_GOTO_COORDS_STATE(INT iPed, BOOL &bGoToIsLooped, BOOL bProgressServerData = FALSE)
	
	BOOL bShouldMoveOn = TRUE
	INT iTemp
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAtGotoLocBitset, iPed)
		bShouldMoveOn = FALSE
	ENDIF
	
	PRINTLN("[Peds][Ped ", iPed, "] SHOULD_PED_MOVE_ON_FROM_GOTO_COORDS_STATE - arrived at goto loc (just completed MC_serverBD_2.iPedGotoProgress[iped] = ",MC_serverBD_2.iPedGotoProgress[iped],", bProgressServerData = ",bProgressServerData,")")
	
	INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedTeam	
	INT iRule = -1	
	IF iTeam != -1
		iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	ENDIF
	
	INT iLoop	
	FOR iLoop = 0 TO (MAX_ASSOCIATED_GOTO_TASKS - 1) // Max number of times we might need to do this
		
		INT iStart = MC_serverBD_2.iPedGotoProgress[iped]
		
		IF bProgressServerData
		AND bIsLocalPlayerHost
			SET_BIT(MC_ServerBD_4.iPedCurrentPriorityGotoLocCompletedBS[iPed], MC_serverBD_2.iPedGotoProgress[iPed])			
		
			IF IS_ASSOCIATED_GOTO_LOOPED_ON_RULE(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], MC_serverBD_2.iPedGotoProgress[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.sPointData, iRule)
			AND NOT IS_ASSOCIATED_GOTO_LOOPED_ON_RULE(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], MC_serverBD_2.iPedGotoProgress[iPed]+1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.sPointData, iRule)							
				MC_serverBD_2.iPedGotoProgress[iped] = GET_FIRST_ASSOCIATED_GOTO_LOOP_INDEX_FOR_RULE(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.sPointData, iRule)
				PRINTLN("[Peds][Ped ", iPed, "] SHOULD_PED_MOVE_ON_FROM_GOTO_COORDS_STATE - Reached end of looped rule goto. Starting back on: ", MC_serverBD_2.iPedGotoProgress[iped])	
			ELSE
				WHILE SHOULD_INCREMENT_ASSOCIATED_GOTO(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], MC_serverBD_2.iPedGotoProgress[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.sPointData, iRule, iStart, IS_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, ciENTIRE_TASK_BITSET_SKIP_RULE_GOTOS_WHEN_RULE_INVALID)) AND MC_serverBD_2.iPedGotoProgress[iped] < MAX_ASSOCIATED_GOTO_TASKS
					MC_serverBD_2.iPedGotoProgress[iped] += 1
					PRINTLN("[Peds][Ped ", iPed, "] SHOULD_PED_MOVE_ON_FROM_GOTO_COORDS_STATE - Incrementing to: ", MC_serverBD_2.iPedGotoProgress[iped])	
				ENDWHILE
			ENDIF
			
			iTemp = MC_serverBD_2.iPedGotoProgress[iped]
		ELSE
			IF IS_ASSOCIATED_GOTO_LOOPED_ON_RULE(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], MC_serverBD_2.iPedGotoProgress[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.sPointData, iRule)
			AND NOT IS_ASSOCIATED_GOTO_LOOPED_ON_RULE(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], MC_serverBD_2.iPedGotoProgress[iPed]+1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.sPointData, iRule)
				iTemp = GET_FIRST_ASSOCIATED_GOTO_LOOP_INDEX_FOR_RULE(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.sPointData, iRule)
				PRINTLN("[Peds][Ped ", iPed, "] SHOULD_PED_MOVE_ON_FROM_GOTO_COORDS_STATE - (iTemp) Reached end of looped rule goto. Starting back on: ", iTemp)	
			ELSE
				iTemp = MC_serverBD_2.iPedGotoProgress[iped]
				WHILE SHOULD_INCREMENT_ASSOCIATED_GOTO(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], iTemp, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.sPointData, iRule, iStart, IS_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, ciENTIRE_TASK_BITSET_SKIP_RULE_GOTOS_WHEN_RULE_INVALID)) AND iTemp < MAX_ASSOCIATED_GOTO_TASKS
					iTemp += 1
					PRINTLN("[Peds][Ped ", iPed, "] SHOULD_PED_MOVE_ON_FROM_GOTO_COORDS_STATE - (iTemp) Incrementing to: ", iTemp)	
				ENDWHILE
			ENDIF
		ENDIF
		
		IF iTemp < GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, FALSE)
			
			IF NOT IS_VECTOR_ZERO(GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, iTemp))
				PRINTLN("[Peds][Ped ", iPed, "] SHOULD_PED_MOVE_ON_FROM_GOTO_COORDS_STATE - next goto found (MC_serverBD_2.iPedGotoProgress[iped] = ",iTemp,") is non-zero & valid")
				bShouldMoveOn = FALSE
				iLoop = MAX_ASSOCIATED_GOTO_TASKS // Break out, this vector is non-zero (use it)
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[Peds][Ped ", iPed, "] SHOULD_PED_MOVE_ON_FROM_GOTO_COORDS_STATE - next goto found (MC_serverBD_2.iPedGotoProgress[iped] = ",iTemp,") had a zero vector!")
				CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] SHOULD_PED_MOVE_ON_FROM_GOTO_COORDS_STATE - Ped ",iped," next goto found (MC_serverBD_2.iPedGotoProgress[iped] = ",iTemp,") had a zero vector!")
			#ENDIF
			ENDIF
			
		ELSE
			PRINTLN("[Peds][Ped ", iPed, "] SHOULD_PED_MOVE_ON_FROM_GOTO_COORDS_STATE - has reached placed number of gotos - MC_serverBD_2.iPedGotoProgress[iped] = ",iTemp,", number of placed gotos = ", GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, FALSE))
			iLoop = MAX_ASSOCIATED_GOTO_TASKS // Break out, we're out of vectors
		ENDIF
		
	ENDFOR
	
	IF IS_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_BIT_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, ciENTIRE_TASK_BITSET_LOOP_GOTO)
	AND bShouldMoveOn
		bGoToIsLooped = TRUE
		bShouldMoveOn = FALSE
	ENDIF
	
	RETURN bShouldMoveOn
			
ENDFUNC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Ped Companions
// ##### Description: Logic, and helper functions for the Ped Companion system.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC ASSIGN_PED_COMPANION_INDEX(INT iCompanionIndex, INT iPed)

	PRINTLN("[Peds][Ped ", iPed, "][Task][SERVER][Companion] - ASSIGN_PED_COMPANION_INDEX - iCompanionIndex: ", iCompanionIndex)
	MC_serverBD_4.iCompanionPedIndex[iCompanionIndex] = iPed
	
ENDPROC

PROC CLEAR_PED_COMPANION_INDEX(INT iCompanionIndex)

	PRINTLN("[SERVER][Companion] - CLEAR_PED_COMPANION_INDEX - iCompanionIndex: ", iCompanionIndex, " being reset.")
	MC_serverBD_4.iCompanionPedIndex[iCompanionIndex] = -1
	
ENDPROC

FUNC INT GET_PED_COMPANION_INDEX(INT iPed)

	INT i
	FOR i = 0 TO FMMC_MAX_COMPANIONS-1
		IF MC_serverBD_4.iCompanionPedIndex[i] = iPed
			RETURN i
		ENDIF
	ENDFOR
	
	RETURN -1	
ENDFUNC

FUNC INT GET_FREE_PED_COMPANION_INDEX()
	INT i
	FOR i = 0 TO FMMC_MAX_COMPANIONS-1
		IF MC_serverBD_4.iCompanionPedIndex[i] = -1	
			RETURN i
		ENDIF
	ENDFOR
	
	RETURN -1	
ENDFUNC

FUNC BOOL SHOULD_PED_GO_TO_COMPANION_STATE(FMMC_PED_STATE &sPedState)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sCompanionSettings.iBS, ciPED_Companion_Enable_System)
		RETURN FALSE
	ENDIF
	
	
		// Add various 

	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_PED_BE_FORCED_TO_COMPANION_STATE(FMMC_PED_STATE &sPedState)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sCompanionSettings.iBS, ciPED_Companion_Enable_System)
		RETURN FALSE
	ENDIF
	
	INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sCompanionSettings.iTeam
	
	IF iTeam = -1
		RETURN FALSE		
	ENDIF
	
	INT iRuleStart = g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sCompanionSettings.iRuleStart_ForceState
	INT iRuleEnd = g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sCompanionSettings.iRuleEnd_ForceState
	
	IF (MC_serverBD_4.iCurrentHighestPriority[iTeam] >= iRuleStart OR iRuleStart = -1)
	AND (MC_serverBD_4.iCurrentHighestPriority[iTeam] < iRuleEnd OR iRuleEnd = -1)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_PED_BE_FORCED_OUT_OF_COMPANION_STATE(FMMC_PED_STATE &sPedState)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sCompanionSettings.iBS, ciPED_Companion_Enable_System)
		RETURN FALSE
	ENDIF
	
	INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sCompanionSettings.iTeam
	
	IF iTeam = -1
		RETURN FALSE		
	ENDIF
	
	INT iRuleEnd = g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sCompanionSettings.iRuleEnd_ForceState
	
	IF (MC_serverBD_4.iCurrentHighestPriority[iTeam] >= iRuleEnd AND iRuleEnd > -1)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// Action/Tasks should be able to put the ped into the companion state later down the line.
PROC PROCESS_FORCE_SWITCH_TO_COMPANION_STATE(FMMC_PED_STATE &sPedState)	

	IF SHOULD_PED_BE_FORCED_TO_COMPANION_STATE(sPedState)
		IF MC_serverBD_2.iPedState[sPedState.iIndex] != ciTASK_COMPANION
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][Companion] - PROCESS_FORCE_SWITCH_TO_COMPANION_STATE - Should go into Companion State")
			SET_PED_STATE(sPedState.iIndex, ciTASK_COMPANION #IF IS_DEBUG_BUILD , 1 #ENDIF )		
		ENDIF
	ELIF SHOULD_PED_BE_FORCED_OUT_OF_COMPANION_STATE(sPedState)
		IF MC_serverBD_2.iPedState[sPedState.iIndex] = ciTASK_COMPANION
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][SERVER][Companion] - PROCESS_FORCE_SWITCH_TO_COMPANION_STATE - Should be removed from Companion State")
			SET_PED_STATE(sPedState.iIndex, ciTASK_CHOOSE_NEW_TASK #IF IS_DEBUG_BUILD , 1 #ENDIF )	
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_CLEAN_UP_COMPANION_SERVER(INT iPed)	

	IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
		PRINTLN("[Peds][Ped ", iPed, "][Task][SERVER][Companion] - PROCESS_CLEAN_UP_COMPANION_SERVER - Waiting for Net Control before we can clean up dead companion. Likely client needs to cleanup data first.")
		EXIT
	ENDIF
	
	INT iCompanionIndex = GET_PED_COMPANION_INDEX(iPed)
	IF iCompanionIndex > -1
		CLEAR_PED_COMPANION_INDEX(iCompanionIndex)
	ENDIF
	
ENDPROc

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: (SAS) Stealth and Aggro Settings System -------------------------------------------------------------------------------------------------------------------------
// ##### Description: Most of the Spook and Aggro related functions shared between Client and Server are declared here.   --------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_PED_SETUP_WITH_STEALTH_AND_AGGRO_SYSTEMS(FMMC_PED_STATE &sPedState)
	IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_EnableStealthAggroSettings)
	OR IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_EnableStealthAggroSettingsOnlyInDisguise)
	OR IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_EnableStealthAggroSettingsOnlyInDisguiseVehicle)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_STEALTH_AND_AGGRO_SYSTEMS_CURRENTLY_ACTIVE(FMMC_PED_STATE &sPedState)	
	RETURN FMMC_IS_LONG_BIT_SET(iPedSASCurrentlyActive, sPedstate.iIndex)
ENDFUNC

FUNC BOOL IS_STEALTH_AND_AGGRO_SYSTEMS_ACTIVE_ON_THIS_RULE(FMMC_PED_STATE &sPedState)
	
	IF FMMC_IS_LONG_BIT_SET(iPedSASFinished, sPedState.iIndex)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_SETUP_WITH_STEALTH_AND_AGGRO_SYSTEMS(sPedState)
		RETURN FALSE
	ENDIF	
	
	INT iTeam = MC_PlayerBD[iLocalPart].iTeam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		IF (iRule >= sPedState.sStealthAndAggroSystemPed.iRuleFrom OR sPedState.sStealthAndAggroSystemPed.iRuleFrom = -1)
		AND (iRule < sPedState.sStealthAndAggroSystemPed.iRuleTo OR sPedState.sStealthAndAggroSystemPed.iRuleTo = -1)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE	
ENDFUNC

FUNC BOOL DOES_THIS_PLAYER_HAVE_FULL_STEALTH_AGGRO_IMMUNITY_FOR_PED(FMMC_PED_STATE &sPedState, INT iPart)

	IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(sMissionPedsLocalVars[sPedState.iIndex].iTimeStampSASDisguiseGracePeriodFull)
	AND NOT MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(sMissionPedsLocalVars[sPedState.iIndex].iTimeStampSASDisguiseGracePeriodFull, ciSASDisguiseGracePeriod)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_AggroImmunityDisguise_IslandGuard_Full)
	AND IS_THIS_PLAYER_WEARING_AN_ISLAND_GUARD_OUTFIT(iPart)
		MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sMissionPedsLocalVars[sPedState.iIndex].iTimeStampSASDisguiseGracePeriodFull)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_AggroImmunityDisguise_IslandSmuggler_Full)
	AND IS_THIS_PLAYER_WEARING_AN_ISLAND_SMUGGLER_OUTFIT(iPart)
		MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sMissionPedsLocalVars[sPedState.iIndex].iTimeStampSASDisguiseGracePeriodFull)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_AggroImmunityDisguise_TunerSecurity_Full)
	AND IS_THIS_PLAYER_WEARING_A_TUNER_SECURITY_OUTFIT(iPart)
		MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sMissionPedsLocalVars[sPedState.iIndex].iTimeStampSASDisguiseGracePeriodFull)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS2, ciPED_SAS2_AggroImmunityDisguise_ULPMaintenance_Full)
	AND IS_THIS_PLAYER_WEARING_AN_ULP_MAINTENANCE_OUTFIT(iPart)
		MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sMissionPedsLocalVars[sPedState.iIndex].iTimeStampSASDisguiseGracePeriodFull)
		RETURN TRUE
	ENDIF	
		
	IF sPedState.sStealthAndAggroSystemPed.mnVehicleFullDiguise != DUMMY_MODEL_FOR_SCRIPT
	AND IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS2, ciPED_SAS2_AggroImmunityDisguise_VehicleModel_Full)
	AND IS_THIS_PLAYER_INSIDE_A_VALID_DISGUISE_VEHICLE_MODEL_TYPE(iPart, sPedState.sStealthAndAggroSystemPed.mnVehicleFullDiguise)	
		MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sMissionPedsLocalVars[sPedState.iIndex].iTimeStampSASDisguiseGracePeriodFull)
		RETURN TRUE
	ENDIF
	
	IF sPedState.sStealthAndAggroSystemPed.iVehicleFullDisguiseBS > -1
	AND IS_THIS_PLAYER_INSIDE_A_VALID_DISGUISE_VEHICLE_INDEX(iPart, sPedState.sStealthAndAggroSystemPed.iVehicleFullDisguiseBS)
		MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sMissionPedsLocalVars[sPedState.iIndex].iTimeStampSASDisguiseGracePeriodFull)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_AggroImmunityDisguise_PersonalVehicle_Full)
	AND IS_THIS_PLAYER_INSIDE_A_VALID_DISGUISE_PERSONAL_VEHICLE(iPart)
		MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sMissionPedsLocalVars[sPedState.iIndex].iTimeStampSASDisguiseGracePeriodFull)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_THIS_PLAYER_HAVE_PARTIAL_STEALTH_AGGRO_IMMUNITY_FOR_PED(FMMC_PED_STATE &sPedState, INT iPart)

	IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(sMissionPedsLocalVars[sPedState.iIndex].iTimeStampSASDisguiseGracePeriodPartial)
	AND NOT MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(sMissionPedsLocalVars[sPedState.iIndex].iTimeStampSASDisguiseGracePeriodPartial, ciSASDisguiseGracePeriod)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_AggroImmunityDisguise_IslandGuard_Partial)
	AND IS_THIS_PLAYER_WEARING_AN_ISLAND_GUARD_OUTFIT(iPart)
		MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sMissionPedsLocalVars[sPedState.iIndex].iTimeStampSASDisguiseGracePeriodPartial)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_AggroImmunityDisguise_IslandSmuggler_Partial)
	AND IS_THIS_PLAYER_WEARING_AN_ISLAND_SMUGGLER_OUTFIT(iPart)
		MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sMissionPedsLocalVars[sPedState.iIndex].iTimeStampSASDisguiseGracePeriodPartial)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_AggroImmunityDisguise_TunerSecurity_Partial)
	AND IS_THIS_PLAYER_WEARING_A_TUNER_SECURITY_OUTFIT(iPart)	
		MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sMissionPedsLocalVars[sPedState.iIndex].iTimeStampSASDisguiseGracePeriodPartial)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS2, ciPED_SAS2_AggroImmunityDisguise_ULPMaintenance_Partial)
	AND IS_THIS_PLAYER_WEARING_AN_ULP_MAINTENANCE_OUTFIT(iPart)	
		MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sMissionPedsLocalVars[sPedState.iIndex].iTimeStampSASDisguiseGracePeriodPartial)
		RETURN TRUE
	ENDIF	
	
	IF sPedState.sStealthAndAggroSystemPed.mnVehiclePartialDiguise != DUMMY_MODEL_FOR_SCRIPT
	AND IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS2, ciPED_SAS2_AggroImmunityDisguise_VehicleModel_Partial)
	AND IS_THIS_PLAYER_INSIDE_A_VALID_DISGUISE_VEHICLE_MODEL_TYPE(iPart, sPedState.sStealthAndAggroSystemPed.mnVehiclePartialDiguise)	
		MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sMissionPedsLocalVars[sPedState.iIndex].iTimeStampSASDisguiseGracePeriodPartial)
		RETURN TRUE
	ENDIF
	
	IF sPedState.sStealthAndAggroSystemPed.iVehiclePartialDisguiseBS != 0
	AND IS_THIS_PLAYER_INSIDE_A_VALID_DISGUISE_VEHICLE_INDEX(iPart, sPedState.sStealthAndAggroSystemPed.iVehiclePartialDisguiseBS)
		MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sMissionPedsLocalVars[sPedState.iIndex].iTimeStampSASDisguiseGracePeriodPartial)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_AggroImmunityDisguise_PersonalVehicle_Partial)
	AND IS_THIS_PLAYER_INSIDE_A_VALID_DISGUISE_PERSONAL_VEHICLE(iPart)
		MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sMissionPedsLocalVars[sPedState.iIndex].iTimeStampSASDisguiseGracePeriodPartial)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_THIS_PLAYER_PED_HAVE_A_VALID_DISGUISE_FOR_PED(FMMC_PED_STATE &sPedState, INT iPart)
	IF NOT IS_STEALTH_AND_AGGRO_SYSTEMS_CURRENTLY_ACTIVE(sPedState)
		RETURN FALSE
	ENDIF
	
	IF DOES_THIS_PLAYER_HAVE_PARTIAL_STEALTH_AGGRO_IMMUNITY_FOR_PED(sPedState, iPart)
	OR DOES_THIS_PLAYER_HAVE_FULL_STEALTH_AGGRO_IMMUNITY_FOR_PED(sPedState, iPart)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_STEALTH_AND_AGGRO_SETTINGS_SYSTEM(FMMC_PED_STATE &sPedState)
	IF FMMC_IS_LONG_BIT_SET(iPedSASCleanedup, sPedstate.iIndex)
		EXIT
	ENDIF
	
	PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - calling CLEANUP_STEALTH_AND_AGGRO_SETTINGS_SYSTEM.")
	
	// Some alertness flags can be retained while blocking the non temprary events. If we didn't aggro through SAS then reset alertness when dropping back to code tasks.
	IF NOT FMMC_IS_LONG_BIT_SET(iPedAggroedWithScriptTrigger, sPedState.iIndex)
		PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - We did not trigger combat through SAS, resetting alertness.")
		SET_PED_ALERTNESS(sPedState.pedIndex, AS_NOT_ALERT)
	ENDIF
	
	IF sPedState.bHasControl
	
		PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - We have control Cleaning Up and calling Stealth related Natives.")
					
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPedState.pedIndex, FALSE)	
		
		MC_SET_UP_PED_VISUAL_PROPERTIES(sPedState.pedIndex, sPedState.iIndex, TRUE)
		
		// Default code Values
		SET_PED_VISUAL_FIELD_MIN_ELEVATION_ANGLE(sPedState.pedIndex, -75.0)
		SET_PED_VISUAL_FIELD_MAX_ELEVATION_ANGLE(sPedState.pedIndex, 60.0)		
		SET_PED_HEARING_RANGE(sPedState.pedIndex, 60.0)
	
	ENDIF
	
	IF DOES_BLIP_EXIST(biPedBlip[sPedState.iIndex])
		SET_BLIP_SHOW_CONE(biPedBlip[sPedState.iIndex], FALSE)
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(iPedSASBlipColour, sPedState.iIndex)
		IF DOES_BLIP_EXIST(biPedBlip[sPedState.iIndex])	
			PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Cleaning up Disguise Blip")
			REMOVE_BLIP(biPedBlip[sPedState.iIndex])
		ENDIF	
	ENDIF
	
	FMMC_CLEAR_LONG_BIT(iPedSASWithinSight, sPedState.iIndex)
	FMMC_CLEAR_LONG_BIT(iPedSASCurrentlyActive, sPedstate.iIndex)
	FMMC_SET_LONG_BIT(iPedSASCleanedup, sPedstate.iIndex)
	FMMC_CLEAR_LONG_BIT(iPedSASInitialised, sPedstate.iIndex)
	FMMC_CLEAR_LONG_BIT(iPedSASBlipCone, sPedState.iIndex)
	FMMC_CLEAR_LONG_BIT(iPedSASBlipColour, sPedState.iIndex)
ENDPROC

PROC INIT_STEALTH_AND_AGGRO_SETTINGS_ON_PED(FMMC_PED_STATE &sPedState)
	
	IF FMMC_IS_LONG_BIT_SET(iPedSASInitialised, sPedState.iIndex)
		EXIT
	ENDIF
	
	IF sPedState.bHasControl
		PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Calling SET_PED_HEARING_RANGE with: ", sPedState.sStealthAndAggroSystemPed.fWeaponUsageRespondingDistance)
		SET_PED_CONFIG_FLAG(sPedState.pedIndex, PCF_PreventReactingToSilencedCloneBullets, TRUE)
		SET_PED_HEARING_RANGE(sPedState.pedIndex, sPedState.sStealthAndAggroSystemPed.fWeaponUsageRespondingDistance)		
	ENDIF
	
	IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_UseVeryWideElevationAnglesForDetection)
		PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Using very wide angles for elevation. Should only be used on vehicle or peds in high places!")
		SET_PED_VISUAL_FIELD_MIN_ELEVATION_ANGLE(sPedState.pedIndex, -90.0)
		SET_PED_VISUAL_FIELD_MAX_ELEVATION_ANGLE(sPedState.pedIndex, 90.0)
	ENDIF
		
	FMMC_SET_LONG_BIT(iPedSASInitialised, sPedState.iIndex)
ENDPROC

FUNC BOOL GET_SAS_IS_TARGET_PED_IN_PERCEPTION_AREA(FMMC_PED_STATE &sPedState, PED_INDEX pedPlayer, FLOAT fLineOfSightConeAngle, FLOAT fDetectionRange, FLOAT fPeripheralConeAngle, FLOAT fPeripheralRange, FLOAT fHeightAngleMin, FLOAT fHeightAngleMax)
	
	IF FMMC_IS_LONG_BIT_SET(iSASPlayerInPerceptionAreaCheckedBS, sPedState.iIndex)
		PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Sight Checking Cached")
		RETURN FMMC_IS_LONG_BIT_SET(iSASPlayerInPerceptionAreaBS, sPedState.iIndex)
	ENDIF
		
	FMMC_SET_LONG_BIT(iSASPlayerInPerceptionAreaCheckedBS, sPedState.iIndex)
	
	IF sPedState.bHasControl
		SET_PED_VISUAL_FIELD_MAX_ELEVATION_ANGLE(sPedState.pedIndex, fHeightAngleMax)
		SET_PED_VISUAL_FIELD_MIN_ELEVATION_ANGLE(sPedState.pedIndex, -fHeightAngleMin)
	ENDIF
	
	IF IS_TARGET_PED_IN_PERCEPTION_AREA(sPedState.pedIndex, pedPlayer, fLineOfSightConeAngle, fDetectionRange, fPeripheralConeAngle, fPeripheralRange)
		FMMC_SET_LONG_BIT(iSASPlayerInPerceptionAreaBS, sPedState.iIndex)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL GET_SAS_IS_TARGET_PED_IN_PERCEPTION_AREA_INSTANT(FMMC_PED_STATE &sPedState, PED_INDEX pedPlayer, FLOAT fLineOfSightConeAngle, FLOAT fDetectionRange, FLOAT fPeripheralConeAngle, FLOAT fPeripheralRange, FLOAT fHeightAngleMin, FLOAT fHeightAngleMax)
	
	IF FMMC_IS_LONG_BIT_SET(iSASPlayerInPerceptionInstantAreaCheckedBS, sPedState.iIndex)
		PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Instant Checking Cached")
		RETURN FMMC_IS_LONG_BIT_SET(iSASPlayerInPerceptionInstantAreaBS, sPedState.iIndex)
	ENDIF
		
	FMMC_SET_LONG_BIT(iSASPlayerInPerceptionInstantAreaCheckedBS, sPedState.iIndex)
				
	IF sPedState.bHasControl
		SET_PED_VISUAL_FIELD_MAX_ELEVATION_ANGLE(sPedState.pedIndex, fHeightAngleMax)
		SET_PED_VISUAL_FIELD_MIN_ELEVATION_ANGLE(sPedState.pedIndex, -fHeightAngleMin)
	ENDIF
		
	IF IS_TARGET_PED_IN_PERCEPTION_AREA(sPedState.pedIndex, pedPlayer, fLineOfSightConeAngle, fDetectionRange, fPeripheralConeAngle, fPeripheralRange)
		FMMC_SET_LONG_BIT(iSASPlayerInPerceptionInstantAreaBS, sPedState.iIndex)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_SAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(FMMC_PED_STATE &sPedState, ENTITY_INDEX eiEntity, INT iLOSFlags = SCRIPT_INCLUDE_ALL)
		
	IF FMMC_IS_LONG_BIT_SET(iSASPlayerInLOSInstantAreaCheckedBS, sPedState.iIndex)
		PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - LOS Checking Cached")
		RETURN FMMC_IS_LONG_BIT_SET(iSASPlayerInLOSInstantAreaBS, sPedState.iIndex)
	ENDIF
	
	IF NOT IS_ENTITY_ALIVE(sPedState.pedIndex)
		FMMC_SET_LONG_BIT(iSASPlayerInLOSInstantAreaCheckedBS, sPedState.iIndex)
		RETURN FALSE
	ENDIF
	
	FMMC_SET_LONG_BIT(iSASPlayerInLOSInstantAreaCheckedBS, sPedState.iIndex)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetFourteen, ciPED_BSFourteen_IgnoreCoverForLOS)
		IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(sPedState.pedIndex, eiEntity, iLOSFlags)
			FMMC_SET_LONG_BIT(iSASPlayerInLOSInstantAreaBS, sPedState.iIndex)
			RETURN TRUE
		ENDIF
	ELSE
		IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_ADJUST_FOR_COVER(sPedState.pedIndex, eiEntity, iLOSFlags)
			FMMC_SET_LONG_BIT(iSASPlayerInLOSInstantAreaBS, sPedState.iIndex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_STEALTH_AND_AGGRO_SETTINGS_SYSTEM_MARKERS(FMMC_PED_STATE &sPedState)	

	// Very good for visualisation. If not being kept for Release then keep it for DEBUG. Bind to a debug only commandline/widget/creator option for content or ourselves.
	IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_LOSStealthMarkers)
		FLOAT fScaleMod					= 0.8
		INT iStealthAggroDelayHUD 		= RETURN_INT_CUSTOM_VARIABLE_VALUE(sPedState.sStealthAndAggroSystemPed.iStealthAggroDelay, CREATION_TYPE_PEDS, sPedState.iIndex)
		VECTOR vMarkerScaleArrow 		= (<<-0.275, -0.4, -0.275>>) * fScaleMod
		VECTOR vMarkerScaleRing 		= (<<-0.65, -0.275, -0.35>>) * fScaleMod
		VECTOR vMarkerScaleSphere 		= (<<-0.50, -0.05, -0.22>>) * fScaleMod
		VECTOR vMarkerCoord 			= GET_FMMC_PED_COORDS(sPedState)
		
		vMarkerCoord.z += 1.35 + (COS(TO_FLOAT(GET_FRAME_COUNT())) / 6)
		FLOAT fPercentage = fSASArousedSuspicionTime[sPedState.iIndex] / TO_FLOAT(iStealthAggroDelayHUD)
		FLOAT fPulse = (COS(TO_FLOAT(GET_FRAME_COUNT())) / 10)
		
		vMarkerScaleArrow *= 1.0+(fPercentage*0.5) + fPulse
		vMarkerScaleRing *= 1.0+(fPercentage*0.5) + fPulse
		vMarkerScaleSphere *= 1.0+(fPercentage*0.5)	+ fPulse
		
		fPercentage *= 100
		
		HUD_COLOURS hdColour		
		IF fPercentage <= 20
			hdColour = HUD_COLOUR_BLUELIGHT
		ELIF fPercentage <= 40
			hdColour = HUD_COLOUR_GREEN
		ELIF fPercentage <= 60
			hdColour = HUD_COLOUR_YELLOW
		ELIF fPercentage <= 80
			hdColour = HUD_COLOUR_ORANGE
		ELSE
			hdColour = HUD_COLOUR_RED
		ENDIF
		
		IF fPercentage <= 130
			VECTOR vTargetRot = GET_GAMEPLAY_CAM_ROT()
			vTargetRot.x = 0.0
			vTargetRot.y = 0.0
			
			INT ir, ig, ib, ia
			GET_HUD_COLOUR(hdColour, ir, ig, ib, ia)
				
			IF fPercentage < 100
				DRAW_MARKER(MARKER_SPHERE, vMarkerCoord, (<<0.0, 0.0, 0.0>>), vTargetRot, vMarkerScaleSphere,  0, 0, 0, 50, DEFAULT, DEFAULT)
				DRAW_MARKER(MARKER_RING, vMarkerCoord, (<<0.0, 0.0, 0.0>>), vTargetRot, vMarkerScaleArrow,  ir, ig, ib, 225, DEFAULT, DEFAULT)
				DRAW_MARKER(MARKER_RING, vMarkerCoord, (<<0.0, 0.0, 0.0>>), vTargetRot, vMarkerScaleRing,  ir, ig, ib, 150, DEFAULT, DEFAULT)
			ELSE
				DRAW_MARKER(MARKER_QUESTION_MARK, vMarkerCoord, (<<0.0, 0.0, 0.0>>), vTargetRot, <<0.6, 0.6, 0.6>>, ir, ig, ib, 120, DEFAULT, DEFAULT)
			ENDIF
			
			// DEBUG ONLY - Only debugging sight issues.  This is to give a quick feel for the ranges being used. Longer ranges may not be 100% accurate due to how code looks at the cone.
			#IF IS_DEBUG_BUILD
			
			IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_LOSStealthLines)
				FLOAT fDetectionRangeHUD 				= RETURN_FLOAT_CUSTOM_VARIABLE_VALUE_DIV_10(sPedState.sStealthAndAggroSystemPed.fDetectionRange, CREATION_TYPE_PEDS, sPedState.iIndex)
				FLOAT fLineOfSightConeAngleHUD			= RETURN_FLOAT_CUSTOM_VARIABLE_VALUE_DIV_10(sPedState.sStealthAndAggroSystemPed.fLineOfSightConeAngle, CREATION_TYPE_PEDS, sPedState.iIndex)	
				FLOAT fInstantDetectionRangeHUD			= RETURN_FLOAT_CUSTOM_VARIABLE_VALUE_DIV_10(sPedState.sStealthAndAggroSystemPed.fInstantDetectionRange, CREATION_TYPE_PEDS, sPedState.iIndex)	
				FLOAT fInstantLineOfSightConeAngleHUD	= RETURN_FLOAT_CUSTOM_VARIABLE_VALUE_DIV_10(sPedState.sStealthAndAggroSystemPed.fInstantLineOfSightConeAngle, CREATION_TYPE_PEDS, sPedState.iIndex)
				
				IF FMMC_IS_LONG_BIT_SET(iPedSASUsingTrueSight, sPedState.iIndex)
					// Acts as the "Peripheral" angle/range
					fDetectionRangeHUD 				= 5.0 // default of SET_PED_VISUAL_FIELD_PROPERTIES
					fLineOfSightConeAngleHUD		= GET_FMMC_PED_PERIPHERAL_EXTREME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedPerceptionSettings)*2
					
					// Main Sight
					fInstantDetectionRangeHUD		= GET_FMMC_PED_PERCEPTION_RANGE(sPedState.iIndex, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedPerceptionSettings, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].fPedPerceptionCentreRange, iPedSightModifierTOD, iPedSightModifierWeather)
					fInstantLineOfSightConeAngleHUD	= GET_FMMC_PED_PERCEPTION_FOV(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedPerceptionSettings, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].fPedPerceptionCentreAngle)
				ENDIF
								
				fLineOfSightConeAngleHUD = (fLineOfSightConeAngleHUD/2)
				fInstantLineOfSightConeAngleHUD = (fInstantLineOfSightConeAngleHUD/2)
				
				// GET_ENTITY_MATRIX, can try to use UP+forward to make a cone for vertical ranges.
				
				VECTOR vForward	= GET_ENTITY_FORWARD_VECTOR(sPedState.pedIndex)				
				VECTOR vSight1, vSight2, vSight3, vSight4, vSight5
				
				IF FMMC_IS_LONG_BIT_SET(iPedSASWithinSight, sPedState.iIndex)
					GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
				ELSE
					GET_HUD_COLOUR(HUD_COLOUR_BLUE, iR, iG, iB, iA)
				ENDIF
				
				iA = 255
				
				// HORIZONTAL
				vSight1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vMarkerCoord, -fLineOfSightConeAngleHUD, vForward*fDetectionRangeHUD)
				vSight2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vMarkerCoord, fLineOfSightConeAngleHUD, vForward*fDetectionRangeHUD)
				vSight3 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vMarkerCoord, 0.0, vForward*fDetectionRangeHUD)
				vSight4 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vMarkerCoord, -fLineOfSightConeAngleHUD/2, vForward*fDetectionRangeHUD)
				vSight5 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vMarkerCoord, fLineOfSightConeAngleHUD/2, vForward*fDetectionRangeHUD)
				
				DRAW_DEBUG_LINE(vMarkerCoord, vSight1, iR, iG, iB, iA)
				DRAW_DEBUG_LINE(vMarkerCoord, vSight2, iR, iG, iB, iA)
				DRAW_DEBUG_LINE(vSight2, vSight5, iR, iG, iB, iA)
				DRAW_DEBUG_LINE(vSight5, vSight3, iR, iG, iB, iA)
				DRAW_DEBUG_LINE(vSight3, vSight4, iR, iG, iB, iA)
				DRAW_DEBUG_LINE(vSight4, vSight1, iR, iG, iB, iA)
					
				IF fInstantDetectionRangeHUD > 0 
					
					IF FMMC_IS_LONG_BIT_SET(iPedSASWithinSight, sPedState.iIndex)
						GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
					ELSE
						GET_HUD_COLOUR(HUD_COLOUR_BLUE, iR, iG, iB, iA)
					ENDIF
					
					iA = 255
					
					vSight1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vMarkerCoord, -fInstantLineOfSightConeAngleHUD, vForward*fInstantDetectionRangeHUD)
					vSight2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vMarkerCoord, fInstantLineOfSightConeAngleHUD, vForward*fInstantDetectionRangeHUD)
					vSight3 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vMarkerCoord, 0.0, vForward*fInstantDetectionRangeHUD)
					vSight4 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vMarkerCoord, -fInstantLineOfSightConeAngleHUD/2, vForward*fInstantDetectionRangeHUD)
					vSight5 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vMarkerCoord, fInstantLineOfSightConeAngleHUD/2, vForward*fInstantDetectionRangeHUD)
					
					DRAW_DEBUG_LINE(vMarkerCoord, vSight1, iR, iG, iB, iA)
					DRAW_DEBUG_LINE(vMarkerCoord, vSight2, iR, iG, iB, iA)
					DRAW_DEBUG_LINE(vMarkerCoord, vSight3, iR, iG, iB, iA)
					DRAW_DEBUG_LINE(vSight2, vSight5, iR, iG, iB, iA)
					DRAW_DEBUG_LINE(vSight5, vSight3, iR, iG, iB, iA)
					DRAW_DEBUG_LINE(vSight3, vSight4, iR, iG, iB, iA)
					DRAW_DEBUG_LINE(vSight4, vSight1, iR, iG, iB, iA)
				ENDIF
			ENDIF
			
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_STEALTH_AND_AGGRO_PED_HEARD_OR_SEEN_HOSTILE_ACTIONS(FMMC_PED_STATE &sPedState, BOOL bLineOfSight, PLAYER_INDEX pedPlayer, BOOL bWithinDetectionAngle, BOOL bWithinInstantDetectionAngle)
	
	IF IS_BIT_SET(sMissionPedsLocalVars[sPedState.iIndex].iPedBS, ci_MissionPedBS_HasBeenHit)
		PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Ped has been hit. Aggroing.")
		RETURN TRUE
	ENDIF	
	
	IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_RespondToTeamAggroSettings)
		IF HAS_TEAM_TRIGGERED_AGGRO(MC_playerBD[iLocalPart].iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iAggroIndexBS_Entity_Ped)
			IF SHOULD_AGRO_FAIL_FOR_TEAM(sPedState.iIndex, MC_PlayerBD[iLocalPart].iTeam)
				PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Responding to Team Aggro Fail settings - SHOULD_AGRO_FAIL_FOR_TEAM = TRUE. Aggroing.")
				RETURN TRUE
			ELSE
				PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Responding to Team Aggro Fail settings - SHOULD_AGRO_FAIL_FOR_TEAM - The team has aggroed but this ped is not flagged to aggro on this rule.")
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_FMMC_DIST2_FROM_LOCAL_PLAYER_PED(sPedState) <= POW(3.0, 2.0)
		IF IS_ENTITY_TOUCHING_ENTITY(GET_PLAYER_PED(pedPlayer), sPedState.pedIndex)
			PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Responding to unsolicited contact with a player. Aggroing.")
			RETURN TRUE
		ENDIF
	ENDIF
		
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetThree, ciPed_BSThree_StopShotFiredEventsAggroing)
		FLOAT fWeaponUsageRespondingDistance = RETURN_FLOAT_CUSTOM_VARIABLE_VALUE_DIV_10(sPedState.sStealthAndAggroSystemPed.fWeaponUsageRespondingDistance, CREATION_TYPE_PEDS, sPedState.iIndex)
		
		IF fWeaponUsageRespondingDistance != 0.0
			
			BOOL bForce = FALSE
			EVENT_NAMES eventReceived = EVENT_INVALID
				
			IF HAS_PED_RECEIVED_EVENT(sPedState.pedIndex, EVENT_SHOT_FIRED)
				PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Detected a EVENT_SHOT_FIRED.")
				eventReceived = EVENT_SHOT_FIRED				
				
			ELIF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_RespondToBulletWhizzedBy)
			AND HAS_PED_RECEIVED_EVENT(sPedState.pedIndex, EVENT_SHOT_FIRED_WHIZZED_BY)
				PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Detected a EVENT_SHOT_FIRED_WHIZZED_BY")
				eventReceived = EVENT_SHOT_FIRED_WHIZZED_BY
				
			ELIF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_RespondToBulletImpacts)
			AND HAS_PED_RECEIVED_EVENT(sPedState.pedIndex, EVENT_SHOT_FIRED_BULLET_IMPACT)
				PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Detected a EVENT_SHOT_FIRED_BULLET_IMPACT")
				eventReceived = EVENT_SHOT_FIRED_BULLET_IMPACT
			
			ELIF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_RespondToExplosion)
			AND (HAS_PED_RECEIVED_EVENT(sPedState.pedIndex, EVENT_EXPLOSION) OR HAS_PED_RECEIVED_EVENT(sPedState.pedIndex, EVENT_SHOCKING_EXPLOSION) OR HAS_PED_RECEIVED_EVENT(sPedState.pedIndex, EVENT_EXPLOSION_HEARD))
			//IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_FMMC_PED_COORDS(sPedState), fWeaponUsageRespondingDistance)
				PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Detected a EVENT_EXPLOSION")
				eventReceived = EVENT_EXPLOSION
				bForce = TRUE
				
			ENDIF
			
			IF eventReceived != EVENT_INVALID
			
				PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - ped to be spooked by shot fired: ", sPedState.iIndex)
				
				VECTOR vPosition
				
				IF bForce
				OR GET_POS_FROM_FIRED_EVENT(sPedState.pedIndex, eventReceived, vPosition)
					
					FLOAT fWeaponUsageRespondingDistance2 = POW(fWeaponUsageRespondingDistance, 2.0)
					FLOAT fDist
					
					IF NOT bForce
						fDist = VDIST2(GET_FMMC_PED_COORDS(sPedState), vPosition)
						PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Distance - From Ped Coord: ", fDist)
					ELSE
						fDist = GET_FMMC_DIST2_FROM_LOCAL_PLAYER_PED(sPedState)
						PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Distance - Forcing response with an fDist check to local player ", fDist)
					ENDIF
					
					PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - GET_POS_FROM_FIRED_EVENT vPosition: ", vPosition, " fWeaponUsageRespondingDistance2: ", fWeaponUsageRespondingDistance2, " fDist: ", fDist)
					
					IF fDist <= fWeaponUsageRespondingDistance2
						PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Heard the event, aggroing.")
						FMMC_SET_LONG_BIT(iPedLocalReceivedEvent_ShotFiredORThreatResponse, sPedState.iIndex)
						RETURN TRUE
					ENDIF
				ELSE
					PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "]- GET_POS_FROM_FIRED_EVENT returned false, unable to find where shot came from")
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - ciPed_BSThree_StopShotFiredEventsAggroing is being used so shots will be ignored.")
	ENDIF
		
	IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_RespondToDeadBodies)
	AND HAS_PED_RECEIVED_EVENT(sPedState.pedIndex, EVENT_PED_SEEN_DEAD_PED)
		PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Has spotted a dead body and has LOS to an Enemy Player.")
		RETURN TRUE
	ENDIF
	
	IF bLineOfSight
		IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_RespondToSeenPedKilled)
		AND HAS_PED_RECEIVED_EVENT(sPedState.pedIndex, EVENT_SHOCKING_SEEN_PED_KILLED)
			PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Has seen a ped killed and has LOS to an Enemy Player.")
			RETURN TRUE
		ENDIF
		
		IF bWithinDetectionAngle
		OR bWithinInstantDetectionAngle
			IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_RespondToSeeingHacking)
				IF DOES_PED_CARE_IF_THIS_PLAYER_IS_HACKING_SOMETHING(pedPlayer, sPedState.iIndex)
					PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Has spotted a player hacking something and has LOS.")
					RETURN TRUE
				ENDIF
			ENDIF
			
			WEAPON_TYPE wtWeap
			IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_RespondToSeeingPlayerWeaponOut)
			AND GET_CURRENT_PED_WEAPON(GET_PLAYER_PED(pedPlayer), wtWeap, TRUE)
				IF wtWeap != WEAPONTYPE_UNARMED
				AND wtWeap != WEAPONTYPE_INVALID
					PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Has spotted the player flashing their weapon.")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PED_USE_DISGUISED_BLIP_WHITE_COLOUR(FMMC_PED_STATE &sPedState, INT iPart)
	
	IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_UseWhiteBlips_WhenPartiallyImmune)
	AND DOES_THIS_PLAYER_HAVE_PARTIAL_STEALTH_AGGRO_IMMUNITY_FOR_PED(sPedState, iPart)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_UseWhiteBlips_WhenFullyImmune)
	AND DOES_THIS_PLAYER_HAVE_FULL_STEALTH_AGGRO_IMMUNITY_FOR_PED(sPedState, iPart)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_STEALTH_AND_AGGRO_SETTINGS_BLIP(FMMC_PED_STATE &sPedState, INT iPart, FLOAT fConeRange, FLOAT fConeAngle, BOOL bUseTrueSight)
		
	IF NOT DOES_BLIP_EXIST(biPedBlip[sPedState.iIndex])	
		FMMC_CLEAR_LONG_BIT(iPedSASBlipCone, sPedState.iIndex)
		FMMC_CLEAR_LONG_BIT(iPedSASBlipColour, sPedState.iIndex)
		EXIT
	ENDIF
	
	IF SHOULD_PED_USE_DISGUISED_BLIP_WHITE_COLOUR(sPedState, iPart)
	AND NOT FMMC_IS_LONG_BIT_SET(iPedAggroedWithScriptTrigger, sPedState.iIndex)
		IF NOT FMMC_IS_LONG_BIT_SET(iPedSASBlipColour, sPedState.iIndex)		
			PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Setting up the Blip Disguised Colour!")
			SET_BLIP_COLOUR(biPedBlip[sPedState.iIndex], BLIP_COLOUR_WHITE)
			FMMC_SET_LONG_BIT(iPedSASBlipColour, sPedState.iIndex)		
		ENDIF
		
	ELIF FMMC_IS_LONG_BIT_SET(iPedSASBlipColour, sPedState.iIndex)
		// Cleaner to remove and have it recreated automatically with correct settings.	
		IF DOES_BLIP_EXIST(biPedBlip[sPedState.iIndex])	
			PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Cleaning up Disguise Blip")
			REMOVE_BLIP(biPedBlip[sPedState.iIndex])
		ENDIF
		FMMC_CLEAR_LONG_BIT(iPedSASBlipColour, sPedState.iIndex)		
	ENDIF
	
	IF NOT IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_UseRadarConeBlip)		
		EXIT
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(iPedSASBlipCone, sPedState.iIndex)
	OR FMMC_IS_LONG_BIT_SET(iPedSASCleanedup, sPedstate.iIndex)
		EXIT
	ENDIF
		
	PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Setting up the visual cone data!")
	
	IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_UseRadarConeBlipForceInstantAggro)
	AND NOT bUseTrueSight
		fConeRange = sPedState.sStealthAndAggroSystemPed.fInstantDetectionRange
		fConeAngle = sPedState.sStealthAndAggroSystemPed.fInstantLineOfSightConeAngle
	ENDIF
	    
	PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Cone settings - fConeRange: ", fConeRange, " fConeAngle: ", fConeAngle)
		
	IF sPedState.bHasControl
		SET_PED_SEEING_RANGE(sPedState.pedIndex, fConeRange) // NOTE: UI Code caps this value at 20.0 internally. Bug needs to be put into code to circumvent this restriction if Content requires it. The bug here results in no cone showing up. url:bugstar:6653582		
		SET_PED_VISUAL_FIELD_CENTER_ANGLE(sPedState.pedIndex, fConeAngle*0.5)
	ENDIF
	
	IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_UseRedBlipCone)	
		PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Cone settings - Use Red Cone")
		SET_BLIP_SHOW_CONE(biPedBlip[sPedState.iIndex], TRUE, ENUM_TO_INT(HUD_COLOUR_RED))
	ELSE
		PRINTLN("[SAS][SpookAggro][Ped ", sPedState.iIndex, "] - Cone settings - Use Default Cone")
		SET_BLIP_SHOW_CONE(biPedBlip[sPedState.iIndex], TRUE)
	ENDIF	
			
	FMMC_SET_LONG_BIT(iPedSASBlipCone, sPedState.iIndex)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Aggro Related Functions -----------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Most of the Spook and Aggro related functions shared between Client and Server are declared here.   --------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// Should be used with animations where aggro'ing is a trigger to play the animation breakout.
FUNC BOOL SHOULD_THIS_SYNC_SCENE_ANIMATION_SUPPRESS_AGGRO_AND_ACTIONS(FMMC_PED_STATE &sPedState)
	
	IF FMMC_IS_LONG_BIT_SET(iPedAggroedWithScriptTrigger, sPedState.iIndex)
		RETURN FALSE
	ENDIF
	
	INT iAnimation	
	IF FMMC_IS_LONG_BIT_SET(iPedPerformingSpecialAnimBS, sPedState.iIndex)		
		iAnimation = g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iSpecialAnim
	ELSE
		iAnimation = g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iIdleAnim
	ENDIF
	
	SWITCH iAnimation
		CASE ciPED_IDLE_ANIM__JUGGERNAUT_POWERED_DOWN		
			IF sMissionPedsLocalVars[sPedState.iIndex].ePedSyncSceneState = PED_SYNC_SCENE_STATE_INIT
			OR sMissionPedsLocalVars[sPedState.iIndex].ePedSyncSceneState >= PED_SYNC_SCENE_STATE_CLEANUP
				RETURN FALSE
			ENDIF
			RETURN TRUE
		BREAK
		
		// Handles how animations dealt with aggro previously in RUN_PED_BODY_SPOOKED_AND_CANCEL_TASKS_CHECKS, this should never be modified. 
		DEFAULT
			IF iAnimation < ciPED_IDLE_ANIM__JUGGERNAUT_POWERED_DOWN
				IF sMissionPedsLocalVars[sPedState.iIndex].ePedSyncSceneState > PED_SYNC_SCENE_STATE_INIT
				AND sMissionPedsLocalVars[sPedState.iIndex].ePedSyncSceneState < PED_SYNC_SCENE_STATE_PROCESS_END
				AND sMissionPedsLocalVars[sPedState.iIndex].iSyncScene > -1
				AND NOT IS_BIT_SET(sMissionPedsLocalVars[sPedState.iIndex].iPedBS, ci_MissionPedBS_iSyncSceneAnimationFinished)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PED_SUPPRESS_NON_SCRIPT_TRIGGERED_AGGRO_EVENTS(FMMC_PED_STATE &sPedState, PED_INDEX pedPlayer = NULL)
	
	IF SHOULD_THIS_SYNC_SCENE_ANIMATION_SUPPRESS_AGGRO_AND_ACTIONS(sPedState)
		RETURN TRUE	
	ENDIF
	
	IF IS_STEALTH_AND_AGGRO_SYSTEMS_CURRENTLY_ACTIVE(sPedState)
		IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_EnableStealthAggroSettingsOnlyInDisguise)	
		AND NOT FMMC_IS_LONG_BIT_SET(iPedAggroedWithScriptTrigger, sPedState.iIndex)
			INT iPart = GET_PARTICIPANT_FROM_PED_INDEX(pedPlayer)
			IF iPart != -1
				IF IS_THIS_PLAYER_WEARING_A_DISGUISE_OUTFIT(iPart)
					RETURN TRUE	
				ENDIF
			ENDIF
		ENDIF
		IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_EnableStealthAggroSettingsOnlyInDisguiseVehicle)	
		AND NOT FMMC_IS_LONG_BIT_SET(iPedAggroedWithScriptTrigger, sPedState.iIndex)
			INT iPart = GET_PARTICIPANT_FROM_PED_INDEX(pedPlayer)
			IF iPart != -1
				IF IS_THIS_PLAYER_INSIDE_A_VALID_DISGUISE_VEHICLE(iPart, sPedState)
					RETURN TRUE	
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_EnableStealthAggroSettings) 
		AND NOT FMMC_IS_LONG_BIT_SET(iPedAggroedWithScriptTrigger, sPedState.iIndex)		
			RETURN TRUE	
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetFifteen, ciPED_BSFifteen_OnlyAggroWithScriptEvents)
	AND NOT FMMC_IS_LONG_BIT_SET(iPedAggroedWithScriptTrigger, sPedState.iIndex)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL WILL_ANY_AGGRO_PED_FAIL_THE_MISSION()
	
	INT iPed
	FOR iPed = 0 TO FMMC_MAX_PEDS - 1
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
		AND NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroDoesntFail)
				PRINTLN("[SpookAggro] WILL_ANY_AGGRO_PED_FAIL_THE_MISSION - Ped ", iPed, " will fail the mission!")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("[SpookAggro] WILL_ANY_AGGRO_PED_FAIL_THE_MISSION - No valid peds will fail the mission!")
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ANY_PEDS_AGGROED()
	INT iPed = 0
	FOR iPed = 0 TO (MC_serverBD.iNumPedCreated - 1)
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAggroedFromEventBitset, iPed)
			PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] ARE_ANY_PEDS_AGGROED - iped: ", iped, " was aggro'd from an event. Setting to spawn aggro'd.")
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC RESET_SPOOK_AT_COORD_IN_RADIUS()
	INT iSpookIndex
	FOR iSpookIndex = 0 TO ciMAX_PED_SPOOK_COORDS-1	
		IF HAS_NET_TIMER_STARTED(sPedSpookAtCoord[iSpookIndex].tdPedSpookAtCoordTimer)
			IF HAS_NET_TIMER_EXPIRED_READ_ONLY(sPedSpookAtCoord[iSpookIndex].tdPedSpookAtCoordTimer, 5000)
				sPedSpookAtCoord[iSpookIndex].vPedSpookCoord = <<0.0, 0.0, 0.0>>
				sPedSpookAtCoord[iSpookIndex].fPedSpookRadius = 0.0
				sPedSpookAtCoord[iSpookIndex].iPedSpookCoordID = ciSpookCoordID_GENERIC
				RESET_NET_TIMER(sPedSpookAtCoord[iSpookIndex].tdPedSpookAtCoordTimer)
				sPedSpookAtCoord[iSpookIndex].entPedSpookTarget = NULL				
				PRINTLN("[LM][RCC MISSION][SpookAggro] RESET_SPOOK_AT_COORD_IN_RADIUS - with iSpookID: ", GET_SPOOK_AT_COORD_AND_RADIUS_DEBUG_NAME(sPedSpookAtCoord[iSpookIndex].iPedSpookCoordID), " vCoord: ", sPedSpookAtCoord[iSpookIndex].vPedSpookCoord, " fRadius: ", sPedSpookAtCoord[iSpookIndex].fPedSpookRadius, " Removing from the Array.")
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL IS_RULE_VALID_FOR_PED_FIXATION_STATE(INT iPed)

	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule =	MC_serverBD_4.iCurrentHighestPriority[iTeam] 

	IF iRule < FMMC_MAX_RULES
		IF (iRule >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationStartRule OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationStartRule = -1)
		AND (iRule < g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationEndRule OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationEndRule = -1)
			PRINTLN("IS_RULE_VALID_FOR_PED_FIXATION_STATE TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationStartRule > -1
		PRINTLN("IS_RULE_VALID_FOR_PED_FIXATION_STATE - iPed: ", iPed, " iPedFixationStartRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationStartRule)
	ENDIF
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationEndRule > -1
		PRINTLN("IS_RULE_VALID_FOR_PED_FIXATION_STATE - iPed: ", iPed, " iPedFixationEndRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationEndRule)
	ENDIF
	
	PRINTLN("IS_RULE_VALID_FOR_PED_FIXATION_STATE FALSE")
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_STAY_IN_FIXATION_STATE(INT iPed)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationTarget > -1
	AND FMMC_IS_LONG_BIT_SET(MC_serverBD_1.iPedFixationTaskStarted, iPed)	
	AND NOT FMMC_IS_LONG_BIT_SET(iPedFixationStateCleanedUpBS, iped)	
	AND IS_RULE_VALID_FOR_PED_FIXATION_STATE(iPed)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_PART_PED_IS_IN_COMBAT_WITH(PED_INDEX ThisPed)
	
	INT iPartCausingFail = -1
	
	VECTOR vPed = GET_ENTITY_COORDS(ThisPed)
	
	FLOAT fClosestDist2 = 90000 //Players will need to be closer than 300m
	INT iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
	INT iPlayersChecked
	
	INT iPart
	FLOAT fTempDist2
	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX tempPlayer
	PED_INDEX tempPed
	
	PRINTLN("[PLAYER_LOOP] - GET_PART_PED_IS_IN_COMBAT_WITH")
	FOR iPart = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
		
		tempPart = INT_TO_PARTICIPANTINDEX(iPart)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			
			IF (NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR))
			OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR)
				
				iPlayersChecked++
				
				tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
				
				IF IS_NET_PLAYER_OK(tempPlayer)
					
					tempPed = GET_PLAYER_PED(tempPlayer)
					
					fTempDist2 = VDIST2(GET_ENTITY_COORDS(tempPed), vPed)
					
					IF fTempDist2 < fClosestDist2
					AND IS_PED_IN_COMBAT(ThisPed, tempPed)
						fClosestDist2 = fTempDist2
						iPartCausingFail = iPart
					ENDIF
				ENDIF
				
				IF iPlayersChecked >= iPlayersToCheck
					BREAKLOOP
				ENDIF
				
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iPartCausingFail
	
ENDFUNC

FUNC INT GET_PART_CLOSEST_TO_ALERTED_PED(PED_INDEX ThisPed)
	
	INT iPartCausingFail = -1
	
	VECTOR vPed = GET_ENTITY_COORDS(ThisPed)
	
	FLOAT fClosestDist2 = 90000 //Players will need to be closer than 300m
	
	FLOAT fTempDist2
	
	INT iPart = -1
	WHILE DO_PARTICIPANT_LOOP(iPart, DPLF_CHECK_PLAYER_OK | DPLF_FILL_PLAYER_IDS)
		
		fTempDist2 = VDIST2(GET_ENTITY_COORDS(piParticipantLoop_PedIndex), vPed)
		
		IF fTempDist2 < fClosestDist2
			fClosestDist2 = fTempDist2
			iPartCausingFail = iPart
		ENDIF
				
	ENDWHILE
	
	RETURN iPartCausingFail
	
ENDFUNC


FUNC BOOL SHOULD_TEAM_AGRO_FAIL_FOR_TOUCHING_PED(INT iped, INT iTeam)
	
	BOOL bFail = FALSE
		
	SWITCH iTeam
		CASE 0
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIgnoreTouchingT0)
				bFail = TRUE
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIgnoreTouchingT1)
				bFail = TRUE
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIgnoreTouchingT2)
				bFail = TRUE
			ENDIF
		BREAK
		
		CASE 3
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIgnoreTouchingT3)
				bFail = TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN bFail
	
ENDFUNC

FUNC INT GET_NEAREST_AGGRO_PART_TO_PED(PED_INDEX pedToCheck, INT iped, FLOAT& fDistance2, INT& iTeamOfPart)

	INT iClosestAggroPart = -1
	FLOAT fClosestDist2 = 999999999 //Furthest distance apart on map is O(10,000)
	FLOAT fTempDist2
	
	INT iTeam
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		
		IF SHOULD_AGRO_FAIL_FOR_TEAM(iped,iTeam)
			INT iClosestPlyrOnTeam = GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(pedToCheck,iTeam)
			PLAYER_INDEX tempPlayer = INT_TO_PLAYERINDEX(iClosestPlyrOnTeam)
			IF (tempPlayer != INVALID_PLAYER_INDEX())
			AND IS_NET_PLAYER_OK(tempPlayer)
			AND NETWORK_IS_PLAYER_A_PARTICIPANT(tempPlayer)
				fTempDist2 = VDIST2(GET_PLAYER_COORDS(tempPlayer),GET_ENTITY_COORDS(pedToCheck))
				IF (fTempDist2 < fClosestDist2)
					fClosestDist2 = fTempDist2
					iClosestAggroPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(tempPlayer))
					iTeamOfPart = iTeam
				ENDIF
			ENDIF
		ENDIF
		
	ENDFOR
	
	fDistance2 = fClosestDist2
	
	RETURN iClosestAggroPart

ENDFUNC

FUNC BOOL HAVE_CLEAR_LOS_TO_ANY_PLAYER_NEARBY(PED_INDEX tempPed)
	
	INT i = -1
	WHILE DO_PARTICIPANT_LOOP(i, DPLF_CHECK_PED_ALIVE | DPLF_FILL_PLAYER_IDS)
		
		IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(tempPed, piParticipantLoop_PedIndex)
			RETURN TRUE			
		ENDIF
		
	ENDWHILE
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PED_AN_AGGRO_PED(INT iped)
	
	BOOL bAggroPed = FALSE //Will this ped be an aggro ped at some point in future?
	
	INT iTeam
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam] > -1
			IF NOT ( IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroAfterAndInclRuleT0 + iTeam * 2) OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroBeforeAndInclRuleT0 + iTeam * 2) )
				IF MC_serverBD_4.iCurrentHighestPriority[iTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam]
					bAggroPed = TRUE
				ELIF MC_serverBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam]
					
					IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_Aggro_SwitchesOnMidpoint_T0 + iTeam))
					OR (NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam])) // If we haven't reached the midpoint yet
						//It's still going on
						bAggroPed = TRUE
					ENDIF
					
				ENDIF
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroAfterAndInclRuleT0 + iTeam * 2)
					//If the rule is before the current one, they will still be an aggro ped
					//If after, they are going to be an aggro ped
					//So no matter what, it's true!
					bAggroPed = TRUE
				ELSE //Before and incl:
					IF MC_serverBD_4.iCurrentHighestPriority[iTeam] > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam]
						//If we've passed it already, we're done with the Aggro
					ELSE
						IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_Aggro_SwitchesOnMidpoint_T0 + iTeam))
						OR (NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam])) // If we haven't reached the midpoint yet
							//It's still going on
							bAggroPed = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF bAggroPed
				//Break out, as we already know this is an aggro ped:
				iTeam = FMMC_MAX_TEAMS
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN bAggroPed
	
ENDFUNC

FUNC BOOL IS_PED_SPOOKED_AT_COORD_IN_RADIUS(FMMC_PED_STATE &sPedState)	
	VEcTOR vPed = GET_FMMC_PED_COORDS(sPedState)
	FLOAT fDist
	IF NOT sPedState.bInjured
		INT iSpookIndex
		FOR iSpookIndex = 0 TO ciMAX_PED_SPOOK_COORDS-1		
			IF NOT IS_VECTOR_ZERO(sPedSpookAtCoord[iSpookIndex].vPedSpookCoord)
				fDist = VDIST2(vPed, sPedSpookAtCoord[iSpookIndex].vPedSpookCoord)
				PRINTLN("[LM][RCC MISSION][SpookAggro] IS_PED_SPOOKED_AT_COORD_IN_RADIUS - iSpookIndex: ", iSpookIndex, " with iSpookID: ", GET_SPOOK_AT_COORD_AND_RADIUS_DEBUG_NAME(sPedSpookAtCoord[iSpookIndex].iPedSpookCoordID), " vCoord: ", sPedSpookAtCoord[iSpookIndex].vPedSpookCoord, " fRadius: ", sPedSpookAtCoord[iSpookIndex].fPedSpookRadius, " fDist: ", fDist)
				IF fDist < POW(sPedSpookAtCoord[iSpookIndex].fPedSpookRadius, 2.0)
					IF DOES_ENTITY_EXIST(sPedSpookAtCoord[iSpookIndex].entPedSpookTarget)
						PRINTLN("[LM][RCC MISSION][SpookAggro] IS_PED_SPOOKED_AT_COORD_IN_RADIUS - iSpookIndex: ", iSpookIndex, " Has an entity associated with it")
						IF sPedSpookAtCoord[iSpookIndex].bPedNeedsLOS
						AND NOT HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(sPedState.iIndex, sPedState.pedIndex, sPedSpookAtCoord[iSpookIndex].entPedSpookTarget, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
							PRINTLN("[LM][RCC MISSION][SpookAggro] IS_PED_SPOOKED_AT_COORD_IN_RADIUS - iSpookIndex: ", iSpookIndex, " No clear LOS, returning FALSE.")
							RETURN FALSE
						ENDIF
					ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PED_START_COMBAT(FMMC_PED_STATE &sPedState)
	
	IF FMMC_IS_LONG_BIT_SET(iPedShouldNotStartCombat, sPedState.iIndex)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(sPedState, TRUE #IF IS_DEBUG_BUILD , 5 #ENDIF)
		FMMC_SET_LONG_BIT(iPedShouldNotStartCombat, sPedState.iIndex)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_COMBAT(sPedState.pedIndex)
	OR IS_PED_FLEEING(sPedState.pedIndex)
	OR (GET_PED_ALERTNESS(sPedState.pedIndex) != AS_NOT_ALERT AND GET_PED_ALERTNESS(sPedState.pedIndex) != INT_TO_ENUM(ALERTNESS_STATE, -1))
		
		SWITCH GET_PED_COMBAT_STYLE(sPedState.iIndex)
			CASE ciPED_AGRESSIVE
			CASE ciPED_BERSERK				
				RETURN TRUE			
			CASE ciPED_DEFENSIVE
				IF IS_PED_ARMED_BY_FMMC(sPedState.iIndex)
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
		
	ENDIF
	
	FMMC_SET_LONG_BIT(iPedShouldNotStartCombat, sPedState.iIndex)
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_PED_GO_TO_COVER_STATE(FMMC_PED_STATE &sPedState)
	
	IF SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(sPedState, DEFAULT #IF IS_DEBUG_BUILD , 4 #ENDIF)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetFour, ciPED_BSFour_TakeCover_AlwaysTakeCover)
	AND NOT (g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn = IG_RASHCOSVKI) // Want Rashkovsky to get into cover at the start, and then let normal AI take over
	AND SHOULD_PED_START_COMBAT(sPedState)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iAssociatedAction = ciACTION_TAKE_COVER
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_SAS_ALLOW_COMBAT_TASK_ACTIVATION(FMMC_PED_STATE &sPedState)

	IF NOT IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_BlockTaskActivationWhileStealthSystemActive)
	AND FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedTaskActive, sPedState.iIndex)
		IF MC_serverBD_2.iAssociatedAction[sPedState.iIndex] = ciACTION_ATTACK
		OR MC_serverBD_2.iAssociatedAction[sPedState.iIndex] = ciACTION_COMBAT_MODE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PED_GO_FROM_NOTHING_STATE_TO_COMBAT_STATE(FMMC_PED_STATE &sPedState)
	
	IF SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(sPedState, DEFAULT #IF IS_DEBUG_BUILD , 3 #ENDIF)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetEleven, ciPED_BSEleven_CanHaveDefensiveAreaInVehicle)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_PED_SUPPRESS_NON_SCRIPT_TRIGGERED_AGGRO_EVENTS(sPedState)
	AND NOT SHOULD_SAS_ALLOW_COMBAT_TASK_ACTIVATION(sPedState)
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "] SHOULD_PED_GO_FROM_NOTHING_STATE_TO_COMBAT_STATE - Returning FALSE as ped ", sPedState.iIndex," Is suppressed with SHOULD_PED_SUPPRESS_NON_SCRIPT_TRIGGERED_AGGRO_EVENTS")
		RETURN FALSE
	ENDIF

	IF MC_IS_PED_AN_ANIMAL(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn)
	AND sPedState.bIsInAnyVehicle
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "] SHOULD_PED_GO_FROM_NOTHING_STATE_TO_COMBAT_STATE - Returning FALSE as ped is an animal in a vehicle.")
		RETURN FALSE
	ENDIF
	
	IF SHOULD_PED_START_COMBAT(sPedState)	
		IF (SHOULD_PED_BE_GIVEN_TASK(sPedState.pedIndex, sPedState.iIndex, SCRIPT_TASK_PLAY_ANIM, TRUE)
		OR SHOULD_PED_BE_GIVEN_TASK(sPedState.pedIndex, sPedState.iIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE))
		AND DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iIdleAnim)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "] SHOULD_PED_GO_FROM_NOTHING_STATE_TO_COMBAT_STATE - Returning FALSE as ped ", sPedState.iIndex," is being given an anim task!")
			RETURN FALSE
		ENDIF
		
		IF FMMC_IS_LONG_BIT_SET(iPedHitByStunGun, sPedState.iIndex)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "] SHOULD_PED_GO_FROM_NOTHING_STATE_TO_COMBAT_STATE - Returning FALSE as ped ", sPedState.iIndex," is stunned!")
			RETURN FALSE
		ENDIF		
		
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(sPedState.iBitset, FMMC_PED_STATE_IN_COMBAT)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "] SHOULD_PED_GO_FROM_NOTHING_STATE_TO_COMBAT_STATE - Returning TRUE as ped ", sPedState.iIndex," is in combat!")
		ENDIF
		IF IS_PED_FLEEING(sPedState.pedIndex)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "] SHOULD_PED_GO_FROM_NOTHING_STATE_TO_COMBAT_STATE - Returning TRUE as ped ", sPedState.iIndex," is fleeing!")
		ENDIF
		IF (GET_PED_ALERTNESS(sPedState.pedIndex) != AS_NOT_ALERT AND GET_PED_ALERTNESS(sPedState.pedIndex) != INT_TO_ENUM(ALERTNESS_STATE, -1))
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "] SHOULD_PED_GO_FROM_NOTHING_STATE_TO_COMBAT_STATE - Returning TRUE as ped ", sPedState.iIndex," is alerted!")
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetThirteen, ciPED_BSThirteen_CombatAutomaticallyWhenInCombatVehicle)
		IF sPedState.bIsInAnyVehicle
			IF IS_VEHICLE_DRIVEABLE(sPedState.vehIndexPedIsIn)
				IF IS_COMBAT_VEHICLE(sPedState.vehIndexPedIsIn)
					
					#IF IS_DEBUG_BUILD
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "] SHOULD_PED_GO_FROM_NOTHING_STATE_TO_COMBAT_STATE - Returning TRUE as ped ", sPedState.iIndex," in a driveable combat vehicle!")
					#ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PLAY_PED_SPOOKED_AUDIO(INT iped)

	INT iscream
	
	PED_INDEX tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
	
	IF NOT IS_ENTITY_DEAD(tempPed)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee = ciPED_FLEE_ON
			iscream = GET_RANDOM_INT_IN_RANGE(0, 10)
		ELSE
			iscream = GET_RANDOM_INT_IN_RANGE(4, 8)
		ENDIF
		
		SWITCH iscream
			CASE 0
				PLAY_PAIN(tempPed, AUD_DAMAGE_REASON_SCREAM_PANIC)
				PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Case 0 for ped: ", iped)
			BREAK
			CASE 1
				PLAY_PAIN(tempPed, AUD_DAMAGE_REASON_SCREAM_SCARED)
				PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Case 1 for ped: ", iped)
			BREAK
			CASE 2
				PLAY_PAIN(tempPed, AUD_DAMAGE_REASON_SCREAM_TERROR)
				PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Case 2 for ped: ", iped)
			BREAK
			CASE 3
				PLAY_PAIN(tempPed, AUD_DAMAGE_REASON_SCREAM_PANIC_SHORT)
				PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Case 3 for ped: ", iped)
			BREAK
			CASE 4
				PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed, "GENERIC_CURSE_MED", "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
				PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Case 4 for ped: ", iped)
			BREAK
			CASE 5
				PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed, "GENERIC_CURSE_HIGH", "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
				PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Case 5 for ped: ", iped)
			BREAK
			CASE 6
				PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed, "GENERIC_SHOCKED_MED", "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
				PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Case 6 for ped: ", iped)
			BREAK
			CASE 7
				PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed, "GENERIC_SHOCKED_HIGH", "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
				PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Case 7 for ped: ", iped)
			BREAK
			CASE 8
				PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed, "GENERIC_FRIGHTENED_MED", "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
				PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Case 8 for ped: ", iped)
			BREAK
			CASE 9
				PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed, "GENERIC_FRIGHTENED_HIGH", "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
				PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Case 9 for ped: ", iped)
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Ped: ", iped, " model is ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(tempPed)))
		#ENDIF
	
	ENDIF
	
ENDPROC

PROC PLAY_PED_AGGROED_AUDIO(FMMC_PED_STATE &sPedState, INT iSpookedReason)
	
	IF NOT FMMC_IS_LONG_BIT_SET(iPedLocalSpookedBitset, sPedState.iIndex)	
		IF NOT sPedState.bInjured			
			IF iSpookedReason = ciSPOOK_SEEN_BODY
				PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(sPedState.pedIndex, "GENERIC_CURSE_MED", "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
			ELIF (iSpookedReason = ciSPOOK_PLAYER_HEARD
				  OR iSpookedReason = ciSPOOK_PLAYER_TOUCHING_ME)
				PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(sPedState.pedIndex, "GENERIC_FRIGHTENED_MED", "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
				println("[MJM][SpookAggro] PLAY_PED_AGGROED_AUDIO - PLAY_PED_AMBIENT_SPEECH")
			ELSE
				INT iSound = GET_RANDOM_INT_IN_RANGE(0, 9)
				IF (iSound < 6)
					PLAY_PAIN(sPedState.pedIndex, AUD_DAMAGE_REASON_SCREAM_SHOCKED)
				ELSE
					PLAY_PAIN(sPedState.pedIndex, AUD_DAMAGE_REASON_SCREAM_PANIC)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC SPOOK_LINKED_PED(FMMC_PED_STATE &sPedState, INT iPartCausingFail)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iSpookLinkedPed > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iSpookLinkedPed < FMMC_MAX_PEDS
		INT iLinkedPed = g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iSpookLinkedPed
		IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedBitset, iLinkedPed)
			IF sPedState.bExists
			AND NOT sPedState.bInjured
				#IF IS_DEBUG_BUILD 
				SET_PED_IS_SPOOKED_DEBUG(sPedState.iIndex, TRUE, ciSPOOK_PLAYER_CAR_CRASH)
				#ENDIF
				FMMC_SET_LONG_BIT(iPedLocalSpookedBitset, iLinkedPed)
				FMMC_SET_LONG_BIT(MC_serverBD.iPedSpookedBitset, iLinkedPed)
				BROADCAST_FMMC_PED_SPOOKED(iLinkedPed, ciSPOOK_PLAYER_CAR_CRASH, iPartCausingFail)
				PRINTLN("[JS][SpookAggro] SPOOK_LINKED_PED - SPOOKING LINKED PED ", iLinkedPed, " from ped: ", sPedState.iIndex)
			ELSE
				PRINTLN("[JS][SpookAggro] SPOOK_LINKED_PED - LINKED PED IS DEAD OR DOESN'T EXIST ", iLinkedPed, " from ped: ", sPedState.iIndex)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SPOOK_PEDS_AT_COORD_IN_RADIUS(VECTOR vCoord, FLOAT fRadius, INT iSpookID = ciSpookCoordID_GENERIC, ENTITY_INDEX entTarget = NULL, BOOL bPedNeedsLOS = TRUE)
	BOOL bAssigned
	INT iSpookIndex
	FOR iSpookIndex = 0 TO ciMAX_PED_SPOOK_COORDS-1
		// Stop special spooks from being assigned in this array twice.
		IF iSpookID = sPedSpookAtCoord[iSpookIndex].iPedSpookCoordID
		AND iSpookID != ciSpookCoordID_GENERIC
			PRINTLN("[LM][RCC MISSION][SpookAggro] SPOOK_PEDS_AT_COORD_IN_RADIUS - set to spook at range and coord with ID: ", GET_SPOOK_AT_COORD_AND_RADIUS_DEBUG_NAME(iSpookID), " but it already exists. Bailing.")
			BREAKLOOP
		ENDIF
		
		IF IS_VECTOR_ZERO(sPedSpookAtCoord[iSpookIndex].vPedSpookCoord)
			PRINTLN("[LM][RCC MISSION][SpookAggro] SPOOK_PEDS_AT_COORD_IN_RADIUS - set to spook at range and coord with ID: ", GET_SPOOK_AT_COORD_AND_RADIUS_DEBUG_NAME(iSpookID), " vCoord: ", vCoord, " fRadius: ", fRadius, " being assigned at index: ", iSpookIndex)
			sPedSpookAtCoord[iSpookIndex].vPedSpookCoord = vCoord
			sPedSpookAtCoord[iSpookIndex].fPedSpookRadius = fRadius
			sPedSpookAtCoord[iSpookIndex].iPedSpookCoordID = iSpookID
			sPedSpookAtCoord[iSpookIndex].entPedSpookTarget = entTarget
			sPedSpookAtCoord[iSpookIndex].bPedNeedsLOS = bPedNeedsLOS
			START_NET_TIMER(sPedSpookAtCoord[iSpookIndex].tdPedSpookAtCoordTimer)
			bAssigned = TRUE
			BREAKLOOP
		ENDIF
	ENDFOR
	IF NOT bAssigned
		PRINTLN("[LM][RCC MISSION][SpookAggro] SPOOK_PEDS_AT_COORD_IN_RADIUS - with iSpookID: ", GET_SPOOK_AT_COORD_AND_RADIUS_DEBUG_NAME(iSpookID), " vCoord: ", vCoord, " fRadius: ", fRadius, " has not been assigned for some reason. Array Might be full.")
	ENDIF
ENDPROC

PROC PROCESS_SERVER_PED_AGGRO_TASK_CHOOSING(FMMC_PED_STATE &sPedState)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetTwo,ciPED_BSTwo_TriggerTasksOnAggro)
		FMMC_SET_LONG_BIT(MC_serverBD.iPedTaskActive, sPedState.iIndex)
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] MANAGE_PED_TASK_ACTIVATION - (PROCESS_SERVER_PED_AGGRO_TASK_CHOOSING) Task Activation Triggered for ped: ", sPedState.iIndex)
	ELSE
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetFour,ciPED_BSFour_StayInTasks_On_Aggro)
		AND NOT SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(sPedState, TRUE #IF IS_DEBUG_BUILD , 15 #ENDIF) // If we also have set up to prioritise over combat then this will prevent peds from having their anims interrupted.
			//Finish task progress:
			MC_ServerBD_2.iAssociatedAction[sPedState.iIndex] = ciACTION_NOTHING
			MC_serverBD.niTargetID[sPedState.iIndex] = NULL
			MC_serverBD_2.iTargetID[sPedState.iIndex] = -1
			FMMC_SET_LONG_BIT(MC_serverBD.iPedAtGotoLocBitset, sPedState.iIndex)
			
			IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedByDeadBodyBitset, sPedState.iIndex)
				//Don't throw the ped straight into combat if they need to breakout of their anim
				IF SHOULD_PED_BE_GIVEN_TASK(sPedState.pedIndex, sPedState.iIndex, SCRIPT_TASK_PLAY_ANIM, TRUE)
				AND SHOULD_PED_BE_GIVEN_TASK(sPedState.pedIndex, sPedState.iIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE)
				OR NOT DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iIdleAnim)
					SET_PED_STATE(sPedState.iIndex,ciTASK_GENERAL_COMBAT #IF IS_DEBUG_BUILD , 2 #ENDIF )
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SERVER_PED_AGGRO_SETTING_DATA(INT iPed, INT iTeam, INT iPartCausingFail = -1)
	
	IF FMMC_IS_LONG_BIT_SET(iPedHitByStunGun, iPed)
		PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] PROCESS_SERVER_PED_AGGRO_SETTING_DATA - Don't set iObjectivePedAggroedBitset bit due to stunned ",iped )
		EXIT 
	ENDIF
	
	PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] PROCESS_SERVER_PED_AGGRO_SETTING_DATA - setting iObjectivePedAggroedBitset bit due to  - for team ",iTeam,", priority ",MC_serverBD_4.iCurrentHighestPriority[iTeam],
			", iPartCausingFail: ", iPartCausingFail)
	
	#IF IS_DEBUG_BUILD
		IF iPartCausingFail != -1
			PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] PROCESS_SERVER_PED_AGGRO_SETTING_DATA - Player causing fail: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPartCausingFail)))
		ENDIF
		
		DEBUG_PRINTCALLSTACK()
	#ENDIF
			
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPed_BSEleven_FailDelayForWanted)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree, ciPED_BSThree_AggroFailHasDelay)			
			SET_BIT(MC_serverBD.iObjectivePedAggroedBitset[iTeam],MC_serverBD_4.iCurrentHighestPriority[iTeam])
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroDoesntFail)
	OR (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AggroAlertsAllOtherAggroPeds) AND WILL_ANY_AGGRO_PED_FAIL_THE_MISSION())
		PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] PROCESS_SERVER_PED_AGGRO_SETTING_DATA - ciPED_BSTwo_AggroDoesntFail Not set. MC_serverBD.iPartCausingFail[",iTeam, "] = ", MC_serverBD.iPartCausingFail[iTeam])
		IF (iPartCausingFail >= 0)
		AND (MC_serverBD.iPartCausingFail[iTeam] = -1)
			MC_serverBD.iPartCausingFail[iTeam] = iPartCausingFail
		ENDIF
	ELSE
		PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] PROCESS_SERVER_PED_AGGRO_SETTING_DATA ciPED_BSTwo_AggroDoesntFail is set on ped ", iped)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree, ciPED_BSThree_AggroFailHasDelay)	
		PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] PROCESS_SERVER_PED_AGGRO_SETTING_DATA ciPED_BSTwo_AggroDoesntFail is set on ped starting aggro timer START_AGGRO_INDEX_TIMER_FROM_BITSET, with AggroBS: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAggroIndexBS_Entity_Ped)
		START_AGGRO_INDEX_TIMER_FROM_BITSET(iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAggroIndexBS_Entity_Ped)
	ELSE
		SET_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET(iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAggroIndexBS_Entity_Ped)
		
		IF iTeam > -1 AND iTeam < FMMC_MAX_TEAMS
		AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE13_DISABLE_CCTV_CAMERAS_TRIGGERING)							
			AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSeventeen, ciPed_BSSeventeen_DisableTriggeringCCTVOnSameAggroIndexes)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAggroIndexBS_Entity_Ped = ciMISSION_AGGRO_INDEX_DEFAULT
					PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] PROCESS_SERVER_PED_AGGRO_SETTING_DATA ped ", iped, " Setting SBBOOL7_CCTV_TEAM_AGGROED_PEDS")
					SET_BIT(MC_serverBD.iServerBitSet7, SBBOOL7_CCTV_TEAM_AGGROED_PEDS)
				ELSE
					PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] PROCESS_SERVER_PED_AGGRO_SETTING_DATA ped ", iped, " Calling SET_TEAM_AS_TRIGGERED_CCTV_FROM_PEDS_AGGRO_BITSET with their aggro BS.")
					SET_TEAM_AS_TRIGGERED_CCTV_FROM_PEDS_AGGRO_BITSET(iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAggroIndexBS_Entity_Ped)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroDoesntFail)
			//Fail!
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree, ciPED_BSThree_AggroFailHasDelay)
				IF MC_serverBD.iNumTeamRuleAttempts[iTeam] > g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamRuleAttempts
				OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamRuleAttempts = 1	
					IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedByDeadBodyBitset, iPed)
						MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_AGRO
						REQUEST_SET_TEAM_FAILED(iTeam,int_to_enum(eFailMissionEnum,enum_to_int(mFail_PED_AGRO_T0)+iTeam),false,MC_serverBD.iPartCausingFail[iTeam],iped)						
					ELSE
						MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_FOUND_BODY
						REQUEST_SET_TEAM_FAILED(iTeam,mFail_PED_FOUND_BODY)
					ENDIF
					
					SET_TEAM_OBJECTIVE_OUTCOME(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam],FALSE)
					SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam],MC_serverBD.iReasonForObjEnd[iTeam],TRUE)
					BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam],iTeam,mc_serverBD_4.iCurrentHighestPriority[iTeam],DID_TEAM_PASS_OBJECTIVE(iTeam,mc_serverBD_4.iCurrentHighestPriority[iTeam]),TRUE)
					MC_serverBD.iEntityCausingFail[iTeam] = iped
					PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] PROCESS_SERVER_PED_AGGRO_SETTING_DATA - MISSION FAILED FOR TEAM:",iTeam," BECAUSE PED AGGROED: ",iped)
				ELSE
					IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_RETRY_CURRENT_RULE_TEAM_0 + iTeam)
						MC_serverBD.iNumTeamRuleAttempts[iTeam]++
						IF MC_serverBD.iNumTeamRuleAttempts[iTeam] <= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamRuleAttempts
							CLEAR_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET(iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAggroIndexBS_Entity_Ped)
							CLEAR_BIT(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_WILL_FAIL_FOR_TEAM_0 + iTeam)							
							STOP_AGGRO_INDEX_TIMER_FROM_BITSET(iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAggroIndexBS_Entity_Ped)
							SET_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_RETRY_CURRENT_RULE_TEAM_0 + iTeam)
							MC_serverBD.iAggroCountdownTimerSlack[iTeam] = 0
							MC_serverBD.iPartCausingFail[iTeam] = -1
							MC_serverBD.iEntityCausingFail[iteam] = -1
							PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] SETTING TEAM TO RESTART RULE. CURRENT ATTEMPTS - ", MC_serverBD.iNumTeamRuleAttempts[iTeam])
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: Run by the owner of the ped (not necessarily the server) to trigger the ped Aggro audio shout, and change various ped flags/properties
PROC TRIGGER_AGGRO_TASKS_ON_PED(FMMC_PED_STATE &sPedState, INT iSpookedReason, BOOL bPlayAudio = TRUE)
	
	PED_INDEX tempPed = sPedState.pedIndex
	INT iped = sPedState.iIndex	
	
	IF bPlayAudio
		PLAY_PED_AGGROED_AUDIO(sPedState, iSpookedReason)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_TriggerTasksOnAggro)
		PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] TRIGGER_AGGRO_TASKS_ON_PED - Aggro Task Activation Triggered for ped: ",iped) 
		IF MC_serverBD_2.iPedState[iped] != ciTASK_CHOOSE_NEW_TASK
			IF NOT IS_BIT_SET(sPedState.iBitset, FMMC_PED_STATE_IN_COMBAT)
			AND CAN_PED_TASKS_BE_CLEARED(tempPed)
				SET_PED_RETASK_DIRTY_FLAG(sPedState)
			ENDIF
			PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] TRIGGER_AGGRO_TASKS_ON_PED - Aggro triggered, clearing tasks",iped)
		ENDIF
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(iPedZoneAggroed, iped)
		PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] TRIGGER_AGGRO_TASKS_ON_PED - SET_BLOCKING_OF_NON_TEMPORARY_EVENTS Called with false: ",iped)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, FALSE)
	ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_StayInTasks_On_Aggro)
		PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] TRIGGER_AGGRO_TASKS_ON_PED - SET_BLOCKING_OF_NON_TEMPORARY_EVENTS Called with true: ",iped)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
	ENDIF
	
	IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings != ciPED_PERCEPTION_DEFAULT)
		FLOAT fRange = GET_FMMC_PED_PERCEPTION_RANGE(iPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fPedPerceptionCentreRange, iPedSightModifierTOD, iPedSightModifierWeather)
		
		IF fRange < 60 //Default combat range
			fRange = CLAMP(fRange*1.5,0,60)
		ENDIF
		
		SET_PED_VISUAL_FIELD_PROPERTIES(tempPed,fRange)
	ENDIF
	
ENDPROC

PROC ALERT_AGGRO_PED(INT iPed, INT iTeam)
	INT iTeamLoop
	FMMC_PED_STATE sPedState
	FILL_FMMC_PED_STATE_STRUCT(sPedState, iPed)
	
	IF sPedState.bExists
	AND NOT sPedState.bInjured
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive,ciPED_BSFive_BlockAggroAlertBroadcast)
	AND SHOULD_AGRO_FAIL_FOR_TEAM(iPed, iTeam)
	AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAggroedBitset, iPed)
	AND NOT FMMC_IS_LONG_BIT_SET(iPedHitByStunGun, iPed)
	AND NOT SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(sPedState, TRUE #IF IS_DEBUG_BUILD , 2 #ENDIF) // If we also have set up to prioritise over combat then this will prevent peds from having their anims interrupted.
		PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] ALERT_ALL_AGGRO_PEDS - Setting  as spooked/aggroed")
		
		#IF IS_DEBUG_BUILD 
		IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedBitset, iPed)
			SET_PED_IS_SPOOKED_DEBUG(iPed, TRUE)
		ENDIF
		#ENDIF
		
		FMMC_SET_LONG_BIT(iPedLocalSpookedBitset, iPed)
		FMMC_SET_LONG_BIT(MC_serverBD.iPedSpookedBitset, iPed)			
		FMMC_SET_LONG_BIT(MC_serverBD.iPedAggroedFromEventBitset, iPed)
		BROADCAST_FMMC_PED_SPOOKED(iPed)
		
		//Trigger ped aggro stuff here to avoid recursion (don't want to call ALERT_ALL_AGGRO_PEDS again and end up in an endless loop)
		
		PROCESS_SERVER_PED_AGGRO_TASK_CHOOSING(sPedState)
		
		FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			IF (MC_serverBD_4.iCurrentHighestPriority[iTeamLoop] < FMMC_MAX_RULES)
			AND SHOULD_AGRO_FAIL_FOR_TEAM(iped,iTeamLoop)
				PROCESS_SERVER_PED_AGGRO_SETTING_DATA(iped, iTeamLoop)
			ENDIF
		ENDFOR
		
		IF sPedState.bHasControl
			TRIGGER_AGGRO_TASKS_ON_PED(sPedState, ciSPOOK_NONE, FALSE)
			IF NOT FMMC_IS_LONG_BIT_SET(iPedLocalAggroedBitset, iPed)
				BROADCAST_FMMC_PED_AGGROED(iped, -2)
			ENDIF
		ELSE
			IF NOT FMMC_IS_LONG_BIT_SET(iPedLocalAggroedBitset, iPed)
				BROADCAST_FMMC_PED_AGGROED(iped, -3)
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD 
		IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAggroedBitset, iPed)
			SET_PED_IS_AGGROD_DEBUG(iPed, TRUE)
		ENDIF
		#ENDIF
		
		FMMC_SET_LONG_BIT(iPedPerceptionBitset, iPed)
		FMMC_CLEAR_LONG_BIT(iPedHeardBitset, iPed)
		FMMC_SET_LONG_BIT(iPedLocalAggroedBitset, iPed)
		FMMC_SET_LONG_BIT(MC_serverBD.iPedAggroedBitset, iPed)			
	ENDIF
ENDPROC

PROC ALERT_ALL_AGGRO_PEDS(INT iTeam)
	
	INT iPed	
	FOR iPed = 0 TO (MC_serverBD.iNumPedCreated - 1)
		ALERT_AGGRO_PED(iPed, iTeam)
	ENDFOR
	
ENDPROC

PROC TRIGGER_AGGRO_SERVERDATA_ON_PED(FMMC_PED_STATE &sPedState, INT iPartCausingFail = -1)

	IF sPedState.bInjured
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetFive, ciPED_BSFive_AggroWhenHurtOrKilled)
		EXIT
	ENDIF
	
	IF NOT sPedState.bInjured
	AND sPedState.bExists
		PROCESS_SERVER_PED_AGGRO_TASK_CHOOSING(sPedState)
	ENDIF

	#IF IS_DEBUG_BUILD 
	SET_PED_IS_AGGROD_DEBUG(sPedState.iIndex)
	#ENDIF
	
	FMMC_SET_LONG_BIT(MC_serverBD.iPedAggroedBitset, sPedState.iIndex)
	
	INT iTeamLoop
	FOR iTeamLoop = 0 TO (FMMC_MAX_TEAMS-1)
		IF (MC_serverBD_4.iCurrentHighestPriority[iTeamLoop] < FMMC_MAX_RULES)
		AND SHOULD_AGRO_FAIL_FOR_TEAM(sPedState.iIndex, iTeamLoop)
			PROCESS_SERVER_PED_AGGRO_SETTING_DATA(sPedState.iIndex, iTeamLoop, iPartCausingFail)
			SPOOK_LINKED_PED(sPedState, iPartCausingFail)
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetThree,ciPED_BSThree_AggroFailHasDelay)
				//Alert all other aggro peds if creator setting tells us to:
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetFive, ciPED_BSFive_AggroAlertsAllOtherAggroPeds)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] aggroed, now alert all other aggro peds!")
					ALERT_ALL_AGGRO_PEDS(iTeamLoop)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC PROCESS_SERVER_PED_SPEECH_AGGRO(PED_INDEX tempPed, INT iPed)
	BOOL bDead = IS_PED_DEAD_OR_DYING(tempPed)
	
	IF NOT bDead
		MC_ServerBD_2.iAssociatedAction[iped] = ciACTION_NOTHING
		MC_serverBD.niTargetID[iped] = NULL
		MC_serverBD_2.iTargetID[iped] = -1
		FMMC_SET_LONG_BIT(MC_serverBD.iPedAtGotoLocBitset, iPed)
		
		IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedByDeadBodyBitset, iPed)
			//Don't throw the ped straight into combat if they need to breakout of their anim
			IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
			AND SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE)
			OR NOT DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim)
				SET_PED_STATE(iped,ciTASK_GENERAL_COMBAT #IF IS_DEBUG_BUILD , 2 #ENDIF )
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TRIGGER_SPEECH_AGGRO_SERVERDATA_ON_PED(PED_INDEX tempPed, INT iped, INT iPartCausingFail = -1)
	
	BOOL bInjured = IS_PED_INJURED(tempPed)
	
	IF bInjured
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AggroWhenHurtOrKilled)
		EXIT
	ENDIF
	
	IF NOT bInjured
		PROCESS_SERVER_PED_SPEECH_AGGRO(tempPed, iped)
	ENDIF

	#IF IS_DEBUG_BUILD 
	SET_PED_IS_AGGROD_DEBUG(iPed)
	#ENDIF
	FMMC_SET_LONG_BIT(MC_serverBD.iPedAggroedBitset, iPed)	
	
	INT iTeamLoop
	FOR iTeamLoop = 0 TO (FMMC_MAX_TEAMS-1)
		IF (MC_serverBD_4.iCurrentHighestPriority[iTeamLoop] < FMMC_MAX_RULES)
		AND SHOULD_AGRO_FAIL_FOR_TEAM(iped,iTeamLoop)
		AND NOT FMMC_IS_LONG_BIT_SET(iPedHitByStunGun, iPed)
			PROCESS_SERVER_PED_AGGRO_SETTING_DATA(iped, iTeamLoop, iPartCausingFail)
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_AggroFailHasDelay)
				//Alert all other aggro peds if creator setting tells us to:
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AggroAlertsAllOtherAggroPeds)
					PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] - aggroed, now alert all other aggro peds!")
					ALERT_ALL_AGGRO_PEDS(iTeamLoop)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

/// PURPOSE: When the ped owner realises that their ped has been aggroed, they will run this function which will send out an Event to the server (or just
///    run the server function if the ped owner is also already the server)
PROC TRIGGER_AGGRO_ON_PED(FMMC_PED_STATE &sPedState, INT iPartCausingFail = -1)
	
	BOOL bServer = bIsLocalPlayerHost
	
	INT iBroadcastNum = iPartCausingFail
	
	PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] TRIGGER_AGGRO_ON_PED (bServer ",bServer,") - has been aggroed")
	
	IF bServer		
		TRIGGER_AGGRO_SERVERDATA_ON_PED(sPedState, iPartCausingFail)
		
		IF iBroadcastNum != (-3)
			iBroadcastNum = -2 //-2 is a value which means that the server has already taken care of the stuff above, and the ped has already been tasked
		ENDIF
	ENDIF
	
	IF NOT FMMC_IS_LONG_BIT_SET(iPedLocalAggroedBitset, sPedState.iIndex)
		BROADCAST_FMMC_PED_AGGROED(sPedState.iIndex, iBroadcastNum)
	ENDIF
	
	FMMC_SET_LONG_BIT(iPedPerceptionBitset, sPedState.iIndex)
	FMMC_CLEAR_LONG_BIT(iPedHeardBitset, sPedState.iIndex)	
	FMMC_SET_LONG_BIT(iPedLocalAggroedBitset, sPedState.iIndex)
	
ENDPROC

PROC TRIGGER_SERVER_AGGRO_HUNTING_ON_PED(FMMC_PED_STATE &sPedState, INT iPartCausingFail = -1)
	
	IF SHOULD_PED_BE_GIVEN_TASK(sPedState.pedIndex, sPedState.iIndex, SCRIPT_TASK_PLAY_ANIM, TRUE)
	OR NOT DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iIdleAnim)
		SET_PED_STATE(sPedState.iIndex, ciTASK_HUNT_FOR_PLAYER)
	ELSE
		//Can't do the 'hunt for player' stuff if this ped needs to breakout of an anim first, just head straight into aggro:
		TRIGGER_AGGRO_ON_PED(sPedState, iPartCausingFail)
	ENDIF
	
ENDPROC

PROC TRIGGER_SERVER_AGGRO_DEAD_BODY_RESPONSE_ON_PED(FMMC_PED_STATE &sPedState, INT iPartCausingFail = -1)
	
	FMMC_SET_LONG_BIT(MC_serverBD.iPedSpookedByDeadBodyBitset, sPedState.iIndex)
	
	IF (SHOULD_PED_BE_GIVEN_TASK(sPedState.pedIndex, sPedState.iIndex, SCRIPT_TASK_PLAY_ANIM, TRUE)
	OR NOT DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iIdleAnim))
	AND NOT sPedState.bIsInAnyVehicle
		SET_PED_STATE(sPedState.iIndex, ciTASK_FOUND_BODY_RESPONSE)
		
		//Finish task progress:
		MC_ServerBD_2.iAssociatedAction[sPedState.iIndex] = ciACTION_NOTHING
		MC_serverBD.niTargetID[sPedState.iIndex] = NULL
		MC_serverBD_2.iTargetID[sPedState.iIndex] = -1
		FMMC_SET_LONG_BIT(MC_serverBD.iPedAtGotoLocBitset, sPedState.iIndex)
	ELSE
		//Can't do the 'find a body' stuff if this ped needs to breakout of an anim first, just head straight into aggro:
		TRIGGER_AGGRO_ON_PED(sPedState, iPartCausingFail)
	ENDIF
	
ENDPROC

/// PURPOSE: This function checks if the current ped has been 'spooked' - bAggroCheat includes the aggro checks without the ped actually being an
///    aggro ped (this is used for the Cancel Tasks when Alarmed check)
FUNC BOOL HAS_PED_BEEN_SPOOKED(FMMC_PED_STATE &sPedState, BOOL bIgnoreGrouping = FALSE, BOOL bAggroCheat = FALSE, BOOL bVehicle = FALSE)
				
	IF sPedState.bInjured
		RETURN FALSE
	ENDIF
	
	INT iPed 					= sPedState.iIndex
	PED_INDEX tempPed 			= sPedState.pedIndex

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_AggroOnSpawnIfAggroAlertEventWasSentOut)
		IF FMMC_IS_LONG_BIT_SET(iPedLocalCheckedSpawnedAggroed, iped)
		OR ARE_ANY_PEDS_AGGROED()
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - Ped Spawning into aggro'd iPedLocalSpawnedAggroed")		
			FMMC_SET_LONG_BIT(iPedLocalCheckedSpawnedAggroed, iped)
			RETURN TRUE
		ENDIF
	ENDIF
	
	INT iTeamLoop
	FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF HAVE_CCTV_CAMERAS_WITH_AGGRO_BS_BEEN_ALERTED(iTeamLoop, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAggroIndexBS_Entity_Ped)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_BlockThreatenEvents)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped: a player was spotted by a CCTV camera")
			RETURN TRUE
		ENDIF
	ENDFOR
	
	IF IS_PED_SPOOKED_AT_COORD_IN_RADIUS(sPedState)
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped: is spooked by special script event at Coord and Radius.")
		RETURN TRUE
	ENDIF
	
	INT iClosestPlayer 			= GET_NEAREST_PLAYER_TO_ENTITY(tempPed)
	PLAYER_INDEX ClosestPlayer 	= INT_TO_PLAYERINDEX(iClosestPlayer)
	PED_INDEX ClosestPlayerPed 	= GET_PLAYER_PED(ClosestPlayer)
	
	IF IS_PED_INJURED(ClosestPlayerPed)
	OR (NOT NETWORK_IS_PLAYER_A_PARTICIPANT(ClosestPlayer))
		RETURN FALSE
	ENDIF
	
	PARTICIPANT_INDEX tempPart = NETWORK_GET_PARTICIPANT_INDEX(ClosestPlayer)
		
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
		RETURN FALSE
	ENDIF
	
	INT iClosestPart = NATIVE_TO_INT(tempPart)
		
	IF IS_PED_IN_A_CUTSCENE(iped)
		RETURN FALSE
	ENDIF
	
	VECTOR vPed = GET_FMMC_PED_COORDS(sPedState)
	VECTOR vClosestPlayer = GET_ENTITY_COORDS( ClosestPlayerPed, FALSE )
	
	IF IS_PED_A_COP_MC(iped)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AggroOnWanted)
		IF MC_playerBD[iClosestPart].iWanted > 0
		OR MC_playerBD[iClosestPart].iFakeWanted > 0
			IF VDIST2( vPed, vClosestPlayer ) < 2500 //(50 * 50)
				IF IS_TARGET_PED_IN_PERCEPTION_AREA( tempPed, ClosestPlayerPed ) AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed, ClosestPlayerPed,SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by closest player having a wanted rating: ",iped) 
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bVehicle
	AND NOT SHOULD_PED_SUPPRESS_NON_SCRIPT_TRIGGERED_AGGRO_EVENTS(sPedState, ClosestPlayerPed)
		
		VEHICLE_INDEX tempVeh = sPedState.vehIndexPedIsIn
		
		IF NOT IS_VEHICLE_DRIVEABLE(tempVeh)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by vehicle being undrivable") 
			RETURN TRUE
		ENDIF
		
		IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(tempVeh, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(tempVeh,ClosestPlayerPed)
		OR FMMC_IS_LONG_BIT_SET(iPedLocalReceivedEvent_VehicleDamageFromPlayer, iped)
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_VEHICLE_DAMAGE_WEAPON) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_VEHICLE_DAMAGE_WEAPON)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by vehicle damage")
			#IF IS_DEBUG_BUILD
			IF FMMC_IS_LONG_BIT_SET(iPedLocalReceivedEvent_VehicleDamageFromPlayer, iped)
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by iPedLocalReceivedEvent_VehicleDamageFromPlayer set")
			ENDIF
			#ENDIF
			IF FMMC_IS_LONG_BIT_SET(iPedLocalReceivedEvent_VehicleDamageFromPlayer, iped)
			OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_VEHICLE_DAMAGE_WEAPON) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_VEHICLE_DAMAGE_WEAPON)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[MC_playerBD[iClosestPart].iTeam] = ciPED_RELATION_SHIP_LIKE
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped in vehicle with ciPED_RELATION_SHIP_LIKE has iPedLocalReceivedEvent_VehicleDamageFromPlayer set, not spooking ped")
					RETURN FALSE
				ENDIF
			ENDIF
			
			RETURN TRUE
		ENDIF
		
		IF IS_PED_ON_SPECIFIC_VEHICLE(ClosestPlayerPed, tempVeh)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by player on vehicle") 
			RETURN TRUE
		ENDIF
		
		IF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_DRAGGED_OUT_CAR) 
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_PED_ON_CAR_ROOF)
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_PED_ENTERED_MY_VEHICLE) 
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_PED_JACKING_MY_VEHICLE)
		OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_DRAGGED_OUT_CAR)
		OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_PED_ON_CAR_ROOF)
		OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_PED_ENTERED_MY_VEHICLE)
		OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_PED_JACKING_MY_VEHICLE)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by car jacking")
			RETURN TRUE
		ENDIF
		
		IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(tempVeh)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by car being attached to something")
			RETURN TRUE
		ENDIF
		
		//If we need to do any of the vehicle stopped checks:
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped)
		OR (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings != ciPED_PERCEPTION_TOTALLY_BLIND)
		OR (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIgnoreBlockingVehicleMovemt) )
			
			IF IS_VEHICLE_STOPPED(tempVeh)
				
				IF IS_ENTITY_IN_ANGLED_AREA(ClosestPlayerPed, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempVeh, <<0.0, 0.0, -1.5>>),
							 		  GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempVeh, <<0.0, 10.0, 3.0>>), 6.0)
					  
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[MC_playerBD[iClosestPart].iTeam] = ciPED_RELATION_SHIP_LIKE
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped in vehicle with ciPED_RELATION_SHIP_LIKE is blocked by player, not spooking ped")
						RETURN FALSE
					ENDIF
					
					//check if closest player has weapon equipped in front of the stopped ped vehicle
					WEAPON_TYPE eClosestPlayerWeapon
					
					IF IS_PED_SHOOTING(ClosestPlayerPed)
					OR ((g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings != ciPED_PERCEPTION_TOTALLY_BLIND)
						AND (IS_PLAYER_TARGETTING_ENTITY(ClosestPlayer, tempPed)
							OR IS_PLAYER_FREE_AIMING_AT_ENTITY(ClosestPlayer, tempPed) ) )
					OR ( IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped)
						AND GET_CURRENT_PED_WEAPON(ClosestPlayerPed, eClosestPlayerWeapon) )
						
						IF NOT IS_CELLPHONE_CAMERA_IN_USE()
							
							#IF IS_DEBUG_BUILD
							IF IS_PED_SHOOTING(ClosestPlayerPed)
								PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by player shooting & vehicle stopped")
							ENDIF
							IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings != ciPED_PERCEPTION_TOTALLY_BLIND)
								IF IS_PLAYER_TARGETTING_ENTITY(ClosestPlayer, tempPed)
									PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by player targetting them")
								ENDIF
								IF IS_PLAYER_FREE_AIMING_AT_ENTITY(ClosestPlayer, tempPed)
									PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by player free aiming at them")
								ENDIF
							ENDIF
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped)
							AND GET_CURRENT_PED_WEAPON(ClosestPlayerPed, eClosestPlayerWeapon)
								PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by player having a weapon equipped")
							ENDIF
							#ENDIF
							RETURN TRUE
						ENDIF
					ENDIF
										
					IF NOT FMMC_IS_LONG_BIT_SET(iPedBlockedBitset, iped)
						IF iPedVehStoppedTimer[iped] >= 5000
							PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed,"BLOCKED_GENERIC","SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
							FMMC_SET_LONG_BIT(iPedBlockedBitset, iped)
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIgnoreBlockingVehicleMovemt)
						//check if closest player is blocking the stopped ped car for some time
						iPedVehStoppedTimer[iped] += MC_serverBD.iNumPedCreated * ROUND(fLastFrameTime * 1000)
						
						IF iPedVehStoppedTimer[iped] >= 12000
							PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by blocked time") 
							RETURN TRUE
						ENDIF
					ENDIF
					
				ENDIF
				
			ELSE
				IF iPedVehStoppedTimer[iped] != 0
				AND GET_ENTITY_SPEED(tempVeh) > 5.0
					iPedVehStoppedTimer[iped] = 0
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF // End of vehicle checks
	
	//Only check for these if grouped up with the player, otherwise they'll be handled by combat stuff - bug 1881474
	BOOL bGrouped = (GET_PED_GROUP_INDEX(tempPed) = GET_PLAYER_GROUP(ClosestPlayer))
	IF (bIgnoreGrouping OR bGrouped)
	AND (bAggroCheat OR IS_PED_AN_AGGRO_PED(iped))
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_BlockThreatenEvents)
	AND NOT SHOULD_PED_SUPPRESS_NON_SCRIPT_TRIGGERED_AGGRO_EVENTS(sPedState, ClosestPlayerPed)
		IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings != ciPED_PERCEPTION_TOTALLY_BLIND)
		AND (IS_PLAYER_FREE_AIMING_AT_ENTITY(ClosestPlayer,tempPed) OR IS_PLAYER_TARGETTING_ENTITY(ClosestPlayer,tempPed))
			IF (NOT IS_CELLPHONE_CAMERA_IN_USE())
			AND ((NOT bIgnoreGrouping) OR (IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,ClosestPlayerPed) AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed, ClosestPlayerPed,SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)))
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - Ped spooked by being aimed at by grouped player")
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF bGrouped
			IF IS_PED_SHOOTING(ClosestPlayerPed)
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - Ped spooked by shots fired by grouped player")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_serverBD_2.iCamObjDestroyed > 0
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedAggroWhenCCTVCamsDestroyed > 0
		IF MC_serverBD_2.iCamObjDestroyed >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedAggroWhenCCTVCamsDestroyed
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - Ped iCamObjDestroyed: ", MC_serverBD_2.iCamObjDestroyed, " is greater than equal to iPedAggroWhenCCTVCamsDestroyed: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedAggroWhenCCTVCamsDestroyed)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings != ciPED_PERCEPTION_TOTALLY_BLIND)
	AND HAS_PED_RECEIVED_EVENT(tempPed, EVENT_GUN_AIMED_AT) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_GUN_AIMED_AT)
	AND NOT SHOULD_PED_SUPPRESS_NON_SCRIPT_TRIGGERED_AGGRO_EVENTS(sPedState, ClosestPlayerPed)
		IF (NOT IS_CELLPHONE_CAMERA_IN_USE())
		AND ((NOT bIgnoreGrouping) OR (IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,ClosestPlayerPed) AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed, ClosestPlayerPed,SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)) )
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by gun aimed at")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ( NOT IS_ANIMATION_INTERRUPTIBLE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim) AND DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim) )
	OR ( MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedBitset, iped))
		IF NOT FMMC_IS_LONG_BIT_SET(iPedLocalReceivedEvent_SeenMeleeActionOrPedKilled, iped)
			IF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_RESPONDED_TO_THREAT) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_RESPONDED_TO_THREAT)
			OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_INJURED_CRY_FOR_HELP) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_INJURED_CRY_FOR_HELP)
			OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOCKING_SEEN_PED_KILLED) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOCKING_SEEN_PED_KILLED)
			OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOCKING_SEEN_MELEE_ACTION) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOCKING_SEEN_MELEE_ACTION)
				IF HAVE_CLEAR_LOS_TO_ANY_PLAYER_NEARBY(tempPed)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped with non interruptible idle animation or active go to spooked by melee action seen")
					RETURN TRUE
				ELSE
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped has received a code even for melee attacks but does not have Line of Sight of the melee action Ped")
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by iPedLocalReceivedEvent_SeenMeleeActionOrPedKilled set")
			FMMC_CLEAR_LONG_BIT(iPedLocalReceivedEvent_SeenMeleeActionOrPedKilled, iped)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_BlockThreatenEvents)
	AND NOT SHOULD_PED_SUPPRESS_NON_SCRIPT_TRIGGERED_AGGRO_EVENTS(sPedState, ClosestPlayerPed)
		IF NOT FMMC_IS_LONG_BIT_SET(iPedLocalReceivedEvent_ShotFiredORThreatResponse, iped)
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_StopShotFiredEventsAggroing)
				#IF IS_DEBUG_BUILD
					EVENT_NAMES eventReceived
					
					IF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOT_FIRED) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOT_FIRED)
						eventReceived = EVENT_SHOT_FIRED
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by EVENT_SHOT_FIRED")
					ELIF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOT_FIRED_WHIZZED_BY) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOT_FIRED_WHIZZED_BY)
						eventReceived = EVENT_SHOT_FIRED_WHIZZED_BY
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by EVENT_SHOT_FIRED_WHIZZED_BY")
					ELIF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOT_FIRED_BULLET_IMPACT) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOT_FIRED_BULLET_IMPACT)
						eventReceived = EVENT_SHOT_FIRED_BULLET_IMPACT
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by EVENT_SHOT_FIRED_BULLET_IMPACT")
					ENDIF
				#ENDIF
				
				IF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOT_FIRED) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOT_FIRED)
				OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOT_FIRED_WHIZZED_BY) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOT_FIRED_WHIZZED_BY)
				OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOT_FIRED_BULLET_IMPACT) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOT_FIRED_BULLET_IMPACT)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by shot fired")
					#IF IS_DEBUG_BUILD
						VECTOR vGunshot
						IF GET_POS_FROM_FIRED_EVENT(tempPed, eventReceived, vGunshot)
							PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - gunshot from coord ", vGunshot)
						ELSE
							PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - GET_POS_FROM_FIRED_EVENT returned false, unable to find where shot came from")
						ENDIF
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - GET_PED_INDEX_FROM_FIRED_EVENT = ",GET_PED_INDEX_FROM_FIRED_EVENT(tempPed, eventReceived))
					#ENDIF
					RETURN TRUE
				ENDIF
			ELSE
				IF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_RESPONDED_TO_THREAT) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_RESPONDED_TO_THREAT)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by receiving event_responded_to_threat")
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by iPedLocalReceivedEvent_ShotFiredORThreatResponse set")
			FMMC_CLEAR_LONG_BIT(iPedLocalReceivedEvent_ShotFiredORThreatResponse, iped)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT SHOULD_PED_SUPPRESS_NON_SCRIPT_TRIGGERED_AGGRO_EVENTS(sPedState, ClosestPlayerPed)
		IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vPed, 32.0)
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_EXPLOSION)	OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_EXPLOSION)
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_EXPLOSION_HEARD) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_EXPLOSION_HEARD)
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_POTENTIAL_BLAST) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_POTENTIAL_BLAST)
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOCKING_EXPLOSION) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOCKING_EXPLOSION)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by explosion") 
			RETURN TRUE
		ENDIF
		
		IF IS_PROJECTILE_TYPE_WITHIN_DISTANCE(vPed, WEAPONTYPE_DLC_FLAREGUN, 20.0)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by flare")
			RETURN TRUE
		ENDIF
			
		IF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_POTENTIAL_GET_RUN_OVER) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_POTENTIAL_GET_RUN_OVER)
			INT iSearchFlags = VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED + VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK + VEHICLE_SEARCH_FLAG_ALLOW_LAW_ENFORCER_VEHICLES_WITH_WANTED_LEVEL
			VEHICLE_INDEX hitVeh = GET_CLOSEST_VEHICLE(vPed,20,DUMMY_MODEL_FOR_SCRIPT,iSearchFlags)
			IF DOES_ENTITY_EXIST(hitVeh)
				PED_INDEX hitPed = GET_PED_IN_VEHICLE_SEAT(hitVeh)
				PLAYER_INDEX playerInVeh
				
				IF NOT IS_PED_INJURED(hitPed)
					playerInVeh = NETWORK_GET_PLAYER_INDEX_FROM_PED(hitPed)
				ENDIF
				
				IF playerInVeh = INVALID_PLAYER_INDEX() //If there isn't a player in the driver's seat
					PED_INDEX lastPedInVeh = GET_LAST_PED_IN_VEHICLE_SEAT(hitVeh)
					IF NOT IS_PED_INJURED(lastPedInVeh)
						playerInVeh = NETWORK_GET_PLAYER_INDEX_FROM_PED(lastPedInVeh)
					ENDIF
				ENDIF
				
				IF playerInVeh != INVALID_PLAYER_INDEX()
				AND NETWORK_IS_PLAYER_ACTIVE(playerInVeh)
					
					PARTICIPANT_INDEX tempParticipant = NETWORK_GET_PARTICIPANT_INDEX(playerInVeh)
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(tempParticipant)
						
						INT iPart = NATIVE_TO_INT(tempParticipant)
						
						IF SHOULD_AGRO_FAIL_FOR_TEAM(iped,MC_playerBD[iPart].iteam)
						AND ( NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,(ciPED_BSThree_AggroIgnorePotentialRunoverT0 + MC_playerBD[iPart].iteam)) )
							PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - Ped spooked by evasive dive due to part ",iPart)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF MC_IS_ENTITY_TOUCHING_ENTITY(tempPed, ClosestPlayerPed, 3.0)
			
			//Lukasz: Fix for B*2193873, stop friendly peds from being spooked by touching player and being blocked by player
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[MC_playerBD[iClosestPart].iTeam] = ciPED_RELATION_SHIP_LIKE
				IF IS_PED_IN_ANY_VEHICLE(tempPed, TRUE) OR IS_PED_IN_ANY_VEHICLE(ClosestPlayerPed, TRUE)
					//PRINTLN("[Peds][Ped ", iPed, "] HAS_PED_BEEN_SPOOKED - player is touching ped in vehicle with ciPED_RELATION_SHIP_LIKE, not spooking ped: ",iped)
					RETURN FALSE
				ENDIF
			ENDIF	
		
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by player touching")
			RETURN TRUE
		ELSE
			IF bVehicle
				IF iPedScratchVehTimer[iped] > 0
					IF iPedScratchVehTimer[iped] > 0
						iPedScratchVehTimer[iped] = CLAMP_INT(iPedScratchVehTimer[iped] - CEIL(5 * (fLastFrameTime*30) * MC_serverBD.iNumPedCreated),0,1000)
					ENDIF
				ENDIF
			ELSE
				iPedScratchVehTimer[iped] = 0
			ENDIF
		ENDIF
		
		IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(tempPed, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(tempPed,ClosestPlayerPed)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by player damage") 
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroWithSound)
		
		FLOAT fDistance = GET_DISTANCE_BETWEEN_ENTITIES(ClosestPlayerPed,tempPed)
		
		IF (GET_PLAYER_CURRENT_STEALTH_NOISE(ClosestPlayer) >= (fDistance / 1.5))
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - Ped spooked by hearing player")
			RETURN TRUE
		ENDIF
		
		IF fDistance <= 50
		AND IS_PED_IN_ANY_VEHICLE(ClosestPlayerPed)
			VEHICLE_INDEX closestPlayerVeh = GET_VEHICLE_PED_IS_IN(ClosestPlayerPed)
			IF IS_HORN_ACTIVE(closestPlayerVeh)
			OR IS_VEHICLE_IN_BURNOUT(closestPlayerVeh)
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - Ped spooked by hearing car")
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_AggroOnSeeingDeadBody)
		IF FMMC_IS_LONG_BIT_SET(iPedSeenDeadBodyBitset, iped)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - Ped spooked by seeing dead body")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF iClosestPart > -1

		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - Checking ped, Closest part ", iClosestPart, " Which is player ", GET_PLAYER_NAME(ClosestPlayer))

		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour, ciPED_BSFour_AggroIf_T0_DroppedOutofSecondaryAnim + MC_playerBD[iClosestPart].iteam)

			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ciPED_BSFour_AggroIf_T0_DroppedOutofSecondaryAnim is set ped ")

			IF IS_BIT_SET(MC_playerBD[iClosestPart].iClientBitSet,PBBOOL_DROPPED_OUT_OF_SECONDARY_ANIM)
			AND IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,ClosestPlayerPed)
			AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed,ClosestPlayerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by closest player dropped out of secondary anim")
				RETURN TRUE
			ELSE
				#IF IS_DEBUG_BUILD
					IF bHandcuff
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - Not spooked because... ",iped)
						
						IF NOT IS_BIT_SET(MC_playerBD[iClosestPart].iClientBitSet,PBBOOL_DROPPED_OUT_OF_SECONDARY_ANIM)
							PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - PBBOOL_DROPPED_OUT_OF_SECONDARY_ANIM not set!")
						ENDIF
						
						IF NOT IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,ClosestPlayerPed)
							PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - IS_TARGET_PED_IN_PERCEPTION_AREA ")
						ENDIF
						
						IF NOT HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed,ClosestPlayerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
							PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - HAS_ENTITY_CLEAR_LOS_TO_ENTITY ")
						ENDIF
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	
	IF ((NOT bVehicle) AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped))
		IF (NOT IS_PED_INJURED(ClosestPlayerPed))
		AND IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,ClosestPlayerPed)
		AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed, ClosestPlayerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
			//check if player has weapon equipped in front of the ped
			WEAPON_TYPE eWeapon
			IF GET_CURRENT_PED_WEAPON(ClosestPlayerPed, eWeapon)
				IF NOT IS_PED_IN_ANY_VEHICLE(ClosestPlayerPed)
					IF NOT SHOULD_PED_SUPPRESS_NON_SCRIPT_TRIGGERED_AGGRO_EVENTS(sPedState, ClosestPlayerPed)
						PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by closest player with weapon out")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Part loop:
	IF (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_AggroIf_T0_DroppedOutofSecondaryAnim)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_AggroIf_T1_DroppedOutofSecondaryAnim)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_AggroIf_T2_DroppedOutofSecondaryAnim)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_AggroIf_T3_DroppedOutofSecondaryAnim))
	OR ((NOT bVehicle) AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped))
				
		WEAPON_TYPE eWeapon //Placed here rather than in FOR loop below to avoid 'value will persist across iterations' info annoyance when compiling
		INT iPartLoop
		
		INT iCount
		
		FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLoop))
				//Don't check closest player again
				IF (iPartLoop != iClosestPart)
				AND (bAggroCheat OR SHOULD_AGRO_FAIL_FOR_TEAM(iped,MC_playerBD[iPartLoop].iteam))
					PLAYER_INDEX TempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLoop))
					PED_INDEX PlayerPed = GET_PLAYER_PED(TempPlayer)
					IF (NOT IS_PED_INJURED(PlayerPed))
					AND IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,PlayerPed)
					AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed, ClosestPlayerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_AggroIf_T0_DroppedOutofSecondaryAnim + MC_playerBD[iPartLoop].iteam)
							IF IS_BIT_SET(MC_playerBD[iPartLoop].iClientBitSet,PBBOOL_DROPPED_OUT_OF_SECONDARY_ANIM)
								PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by seeing part ",iPartLoop," not in secondary anim")
								RETURN TRUE
							ENDIF
						ENDIF
						
						IF (NOT bVehicle AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped))
							//check if player has weapon equipped in front of the ped
							IF GET_CURRENT_PED_WEAPON(PlayerPed, eWeapon)
								IF NOT IS_PED_IN_ANY_VEHICLE(PlayerPed)
									PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by seeing part ",iPartLoop," with weapon")
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				iCount++
			ENDIF
			
			IF iCount >= (MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3])
				BREAKLOOP
			ENDIF
		ENDFOR
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedAggroZone > -1
		IF IS_BIT_SET(iAlertPedsZoneBitSet, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedAggroZone)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped Spooked by entering Alert Zone: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedAggroZone)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iHackFailsForAggro != -1
		IF GET_HACK_FAILS() >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iHackFailsForAggro
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by iHackingFails ",GET_HACK_FAILS()," >= iHackFailsForAggro ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iHackFailsForAggro)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSixteen, ciPED_BSSixteen_AggroWithMultipleFingerprintQuits)
		IF iFingerprintClone_Exits >= 2
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED - ped spooked by iFingerprintClone_Exits >= 2 - ciPED_BSSixteen_AggroWithMultipleFingerprintQuits is set.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC INT GET_AGGRO_FAIL_TEAM_BITSET_FOR_PED(INT iPed)
	INT iTeam, iTeamsBitset
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		IF SHOULD_AGRO_FAIL_FOR_TEAM(iPed, iTeam)
 			SET_BIT(iTeamsBitset, iTeam)
		ENDIF
	ENDFOR
	
	RETURN iTeamsBitset
ENDFUNC

/// PURPOSE: This function checks if the current ped has been 'aggroed' - iPartCausingFail returns the participant we guess to be doing the Aggro,
///    and iSpookedReason returns the reason the ped has been aggroed (an INT in the list of ciSPOOK_etc spook reasons)
FUNC BOOL HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER(FMMC_PED_STATE &sPedState, INT& iPartCausingFail, INT& iSpookedReason, BOOL bVehicle, INT iTeam)
	
	iPartCausingFail = -1
	iSpookedReason = ciSPOOK_NONE
	
	INT iPed = sPedState.iIndex
	PED_INDEX tempPed = sPedState.pedIndex
	FLOAT fDistance2
	INT iTeamOfPart
	INT iClosestAggroPart = GET_NEAREST_AGGRO_PART_TO_PED(tempPed, iped, fDistance2, iTeamOfPart)
		
	IF iClosestAggroPart = -1 //No players on an aggro rule on this ped!
		PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - no aggro players")
		RETURN FALSE
	ELSE
		IF (fDistance2 >= 90000) //(300 * 300)
			PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - closest aggro player is over 300m away (don't bother checking)")
			RETURN FALSE
		ENDIF
	ENDIF
	
	PLAYER_INDEX ClosestPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iClosestAggroPart))
	PED_INDEX ClosestPlayerPed 	= GET_PLAYER_PED(ClosestPlayer)
	
	IF IS_ENTITY_DEAD(tempPed)
		PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - is dead")
		RETURN FALSE
	ENDIF
	
	VECTOR vPed = GET_FMMC_PED_COORDS(sPedState)
	
	IF IS_PED_A_COP_MC(iped)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AggroOnWanted)
		IF MC_playerBD[iClosestAggroPart].iWanted > 0
		OR MC_playerBD[iClosestAggroPart].iFakeWanted > 0
			IF (fDistance2 < 2500) //(50 * 50)
				PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by closest aggro part having a wanted rating")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bVehicle
		
		VEHICLE_INDEX tempVeh = sPedState.vehIndexPedIsIn
		
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(tempVeh,ClosestPlayerPed)
		OR FMMC_IS_LONG_BIT_SET(iPedLocalReceivedEvent_VehicleDamageFromPlayer, iped)
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_VEHICLE_DAMAGE_WEAPON) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_VEHICLE_DAMAGE_WEAPON)
			FMMC_CLEAR_LONG_BIT(iPedLocalReceivedEvent_VehicleDamageFromPlayer, iped)
			PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by car crash with player via iPedLocalReceivedEvent_VehicleDamageFromPlayer set")
			iSpookedReason = ciSPOOK_PLAYER_CAR_CRASH
			RETURN TRUE
		ENDIF
		//Car jacking and ped on roof is handled in the IS_ENTITY_TOUCHING_ENTITY stuff below
		
		IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(tempVeh)
			ENTITY_INDEX attachEnt = GET_ENTITY_ATTACHED_TO(tempVeh)
			IF DOES_ENTITY_EXIST(attachEnt)
			AND IS_ENTITY_A_VEHICLE(attachEnt)
				VEHICLE_INDEX attachVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(attachEnt)
				
				PED_INDEX driverPed = GET_PED_IN_VEHICLE_SEAT(attachVeh)
				PLAYER_INDEX playerInVeh
				
				IF NOT IS_PED_INJURED(driverPed)
					playerInVeh = NETWORK_GET_PLAYER_INDEX_FROM_PED(driverPed)
				ENDIF
				
				IF playerInVeh = INVALID_PLAYER_INDEX() //If there isn't a player in the driver's seat
					PED_INDEX lastPedInSeat = GET_LAST_PED_IN_VEHICLE_SEAT(attachVeh)
					IF NOT IS_PED_INJURED(lastPedInSeat)
						playerInVeh = NETWORK_GET_PLAYER_INDEX_FROM_PED(lastPedInSeat)
					ENDIF
				ENDIF
				
				IF playerInVeh != INVALID_PLAYER_INDEX()
				AND NETWORK_IS_PLAYER_ACTIVE(playerInVeh)
					
					PARTICIPANT_INDEX tempPart = NETWORK_GET_PARTICIPANT_INDEX(playerInVeh)
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
						
						INT iPart = NATIVE_TO_INT(tempPart)
						
						IF SHOULD_AGRO_FAIL_FOR_TEAM(iped,MC_playerBD[iPart].iteam)
						AND SHOULD_TEAM_AGRO_FAIL_FOR_TOUCHING_PED(iped,MC_playerBD[iPart].iteam)
							PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by vehicle attached to vehicle of part ",iPart)
							iPartCausingFail = iPart
							iSpookedReason = ciSPOOK_VEHICLE_ATTACHED
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
		
		//If we need to do any of the vehicle stopped checks:
		IF (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped) )
		OR (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings != ciPED_PERCEPTION_TOTALLY_BLIND)
		OR (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIgnoreBlockingVehicleMovemt) )
			IF IS_VEHICLE_STOPPED(tempVeh)
				
				IF IS_ENTITY_IN_ANGLED_AREA(ClosestPlayerPed, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempVeh, <<0.0, 0.0, -1.5>>),
													 		  GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempVeh, <<0.0, 10.0, 3.0>>), 6.0)
					
					//check if closest player has weapon equipped in front of the stopped ped vehicle
					WEAPON_TYPE eClosestPlayerWeapon
					
					IF IS_PED_SHOOTING(ClosestPlayerPed)
					OR ((g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings != ciPED_PERCEPTION_TOTALLY_BLIND)
						AND (IS_PLAYER_TARGETTING_ENTITY(ClosestPlayer, tempPed)
							OR IS_PLAYER_FREE_AIMING_AT_ENTITY(ClosestPlayer, tempPed) ) )
					OR ( IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped)
						AND GET_CURRENT_PED_WEAPON(ClosestPlayerPed, eClosestPlayerWeapon) )
						
						IF NOT IS_CELLPHONE_CAMERA_IN_USE()
							
							#IF IS_DEBUG_BUILD
							IF IS_PED_SHOOTING(ClosestPlayerPed)
								PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by part ", iClosestAggroPart, " shooting & vehicle stopped")
							ENDIF
							IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings != ciPED_PERCEPTION_TOTALLY_BLIND)
								IF IS_PLAYER_TARGETTING_ENTITY(ClosestPlayer, tempPed)
									PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by part ",iClosestAggroPart," targetting them")
								ENDIF
								IF IS_PLAYER_FREE_AIMING_AT_ENTITY(ClosestPlayer, tempPed)
									PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by part ",iClosestAggroPart," free aiming at them")
								ENDIF
							ENDIF
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped)
							AND GET_CURRENT_PED_WEAPON(ClosestPlayerPed, eClosestPlayerWeapon)
								PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by part ",iClosestAggroPart," having a weapon equipped")
							ENDIF
							#ENDIF
							iPartCausingFail = iClosestAggroPart
							iSpookedReason = ciSPOOK_PLAYER_WITH_WEAPON
							RETURN TRUE
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIgnoreBlockingVehicleMovemt)
						//Check if closest player is blocking the stopped ped car for some time (this stuff is already incremented / reset in HAS_PED_BEEN_SPOOKED_BY_PLAYER)
						IF iPedVehStoppedTimer[iped] >= 12000
							PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by blocked time") 
							iPartCausingFail = iClosestAggroPart
							iSpookedReason = ciSPOOK_PLAYER_BLOCKED_VEHICLE
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	FLOAT fExplosionPerceptionRadius = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fExplosionPerceptionRadius	
	IF fExplosionPerceptionRadius > 0.0
		IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vPed, fExplosionPerceptionRadius)
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_EXPLOSION)	OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_EXPLOSION)
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_EXPLOSION_HEARD) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_EXPLOSION_HEARD)
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_POTENTIAL_BLAST) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_POTENTIAL_BLAST)
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOCKING_EXPLOSION) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOCKING_EXPLOSION)
			
			ENTITY_INDEX expOwner = GET_OWNER_OF_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE,vPed + <<fExplosionPerceptionRadius,0,-fExplosionPerceptionRadius>>,vPed + <<-fExplosionPerceptionRadius,0,fExplosionPerceptionRadius>>,64)
			
			IF IS_ENTITY_A_PED(expOwner)
				PED_INDEX expPed = GET_PED_INDEX_FROM_ENTITY_INDEX(expOwner)
				IF IS_PED_A_PLAYER(expPed)
					
					PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(expPed)
					
					IF NETWORK_IS_PLAYER_ACTIVE(tempPlayer)
						
						PARTICIPANT_INDEX tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
						
						IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
							
							INT iPart = NATIVE_TO_INT(tempPart)
							
							IF SHOULD_AGRO_FAIL_FOR_TEAM(iped,MC_playerBD[iPart].iteam)
								PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by explosion due to part ", iPart, " fExplosionPerceptionRadius: ", fExplosionPerceptionRadius)
								iPartCausingFail = iPart
								iSpookedReason = ciSPOOK_PLAYER_CAUSED_EXPLOSION
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PROJECTILE_TYPE_WITHIN_DISTANCE(vPed, WEAPONTYPE_DLC_FLAREGUN, 20.0)
		PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by flare")
		iSpookedReason = ciSPOOK_SHOTS_FIRED
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_TRIGGERED_EMP_CURRENTLY_ACTIVE()
		PRINTLN("[Peds][Ped ", iPed, "] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by EMP")
		iSpookedReason = ciSPOOK_NONE
		RETURN TRUE
	ENDIF
	
	IF (HAS_PED_RECEIVED_EVENT(tempPed, EVENT_POTENTIAL_GET_RUN_OVER) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_POTENTIAL_GET_RUN_OVER))
		INT iSearchFlags = VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED + VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK + VEHICLE_SEARCH_FLAG_ALLOW_LAW_ENFORCER_VEHICLES_WITH_WANTED_LEVEL
		VEHICLE_INDEX hitVeh = GET_CLOSEST_VEHICLE(vPed,20,DUMMY_MODEL_FOR_SCRIPT,iSearchFlags)
		IF DOES_ENTITY_EXIST(hitVeh)
			PED_INDEX hitPed = GET_PED_IN_VEHICLE_SEAT(hitVeh)
			PLAYER_INDEX playerInVeh
			
			IF NOT IS_PED_INJURED(hitPed)
				playerInVeh = NETWORK_GET_PLAYER_INDEX_FROM_PED(hitPed)
			ENDIF
			
			IF playerInVeh = INVALID_PLAYER_INDEX() //If there isn't a player in the driver's seat
				PED_INDEX lastPedInVeh = GET_LAST_PED_IN_VEHICLE_SEAT(hitVeh)
				IF NOT IS_PED_INJURED(lastPedInVeh)
					playerInVeh = NETWORK_GET_PLAYER_INDEX_FROM_PED(lastPedInVeh)
				ENDIF
			ENDIF
			
			IF playerInVeh != INVALID_PLAYER_INDEX()
			AND NETWORK_IS_PLAYER_ACTIVE(playerInVeh)
				
				PARTICIPANT_INDEX tempPart = NETWORK_GET_PARTICIPANT_INDEX(playerInVeh)
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					
					INT iPart = NATIVE_TO_INT(tempPart)
					
					IF SHOULD_AGRO_FAIL_FOR_TEAM(iped,MC_playerBD[iPart].iteam)
					AND ( NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,(ciPED_BSThree_AggroIgnorePotentialRunoverT0 + MC_playerBD[iPart].iteam)) )
						PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by evasive dive due to part ",iPart)
						iPartCausingFail = iPart
						iSpookedReason = ciSPOOK_PLAYER_POTENTIAL_RUN_OVER
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Only check for these if grouped up with the player, otherwise they'll be handled by combat stuff - bug 1881474
	IF (GET_PED_GROUP_INDEX(tempPed) = GET_PLAYER_GROUP(ClosestPlayer))
		IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings != ciPED_PERCEPTION_TOTALLY_BLIND)
		AND (IS_PLAYER_FREE_AIMING_AT_ENTITY(ClosestPlayer,tempPed) OR IS_PLAYER_TARGETTING_ENTITY(ClosestPlayer,tempPed))
			IF (NOT IS_CELLPHONE_CAMERA_IN_USE())
				PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by being aimed at by grouped player ",iClosestAggroPart)
				iPartCausingFail = iClosestAggroPart
				iSpookedReason = ciSPOOK_PLAYER_AIMED_AT
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF IS_PED_SHOOTING(ClosestPlayerPed)
			PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by shots fired by grouped player ",iClosestAggroPart)
			iPartCausingFail = iClosestAggroPart
			iSpookedReason = ciSPOOK_SHOTS_FIRED
			RETURN TRUE
		ENDIF
		
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(tempPed,ClosestPlayerPed)
			PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by damage from grouped player ",iClosestAggroPart)
			iPartCausingFail = iClosestAggroPart
			iSpookedReason = ciSPOOK_DAMAGED
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT bVehicle
		IF MC_IS_ENTITY_TOUCHING_ENTITY(tempPed, ClosestPlayerPed, 3.0)
			IF SHOULD_AGRO_FAIL_FOR_TEAM(iped,iTeamOfPart)
			AND SHOULD_TEAM_AGRO_FAIL_FOR_TOUCHING_PED(iped,iTeamOfPart)
				PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by part ",iClosestAggroPart," touching")
				iPartCausingFail = iClosestAggroPart
				iSpookedReason = ciSPOOK_PLAYER_TOUCHING_ME
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		
		IF IS_PED_IN_ANY_VEHICLE(ClosestPlayerPed)
			
			VEHICLE_INDEX hitVeh = GET_VEHICLE_PED_IS_IN(ClosestPlayerPed)
			
			PED_INDEX pedInSeat = GET_PED_IN_VEHICLE_SEAT(hitVeh)
			PLAYER_INDEX playerInVeh = INVALID_PLAYER_INDEX()
			IF NOT IS_PED_INJURED(pedInSeat)
				playerInVeh = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedInSeat)
			ENDIF
			
			IF playerInVeh = INVALID_PLAYER_INDEX() //If there isn't a player in the driver's seat
				PED_INDEX lastPedInSeat = GET_LAST_PED_IN_VEHICLE_SEAT(hitVeh)
				IF NOT IS_PED_INJURED(lastPedInSeat)
					playerInVeh = NETWORK_GET_PLAYER_INDEX_FROM_PED(lastPedInSeat)
				ENDIF
			ENDIF
			
			IF playerInVeh != INVALID_PLAYER_INDEX()
			AND NETWORK_IS_PLAYER_ACTIVE(playerInVeh)
				
				PARTICIPANT_INDEX tempPart = NETWORK_GET_PARTICIPANT_INDEX(playerInVeh)
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				
					INT iPart = NATIVE_TO_INT(tempPart)
					
					IF SHOULD_AGRO_FAIL_FOR_TEAM(iped,MC_playerBD[iPart].iteam)
					AND SHOULD_TEAM_AGRO_FAIL_FOR_TOUCHING_PED(iped,MC_playerBD[iPart].iteam)

						IF MC_IS_ENTITY_TOUCHING_ENTITY(hitVeh, GET_VEHICLE_PED_IS_IN(tempPed), 30.0)
							VECTOR vPlayerVel = GET_ENTITY_VELOCITY(hitVeh)
							VECTOR vPedVel = GET_ENTITY_VELOCITY(GET_VEHICLE_PED_IS_IN(tempPed))
							
							//Speed diff in the direction of the ped's car
							FLOAT fSpeedDifference = ABSF(DOT_PRODUCT((vPlayerVel - vPedVel),vPedVel) / VMAG(vPedVel))
							
							IF fSpeedDifference >= 4
							OR (VMAG2(vPlayerVel - vPedVel) >= 25)
								PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by car crash due to aggro player",iPart)
								iPartCausingFail = iPart
								iSpookedReason = ciSPOOK_PLAYER_CAR_CRASH
								RETURN TRUE
							ELSE
								// fLastFrameTime * 30 scales the thing by FPS, scaling it by 1 if we're running at 30 FPS
								iPedScratchVehTimer[iped] += FLOOR(((fSpeedDifference * 5) + 40) * (fLastFrameTime*30) ) * MC_serverBD.iNumPedCreated
								
								IF iPedScratchVehTimer[iped] >= 150
									PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by car scratch due to aggro player",iPart)
									iPartCausingFail = iPart
									iSpookedReason = ciSPOOK_PLAYER_CAR_CRASH
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF MC_IS_ENTITY_TOUCHING_ENTITY(tempPed, ClosestPlayerPed, 3.0)
			AND SHOULD_AGRO_FAIL_FOR_TEAM(iped,iTeamOfPart)
			AND SHOULD_TEAM_AGRO_FAIL_FOR_TOUCHING_PED(iped,iTeamOfPart)
				PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by part ",iClosestAggroPart," touching vehicle")
				iPartCausingFail = iClosestAggroPart
				iSpookedReason = ciSPOOK_PLAYER_TOUCHING_MY_VEHICLE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroWithSound)
		IF (GET_PLAYER_CURRENT_STEALTH_NOISE(ClosestPlayer) >= (SQRT(fDistance2) / 1.5))
			PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by hearing part ",iClosestAggroPart)
			iPartCausingFail = iClosestAggroPart
			iSpookedReason = ciSPOOK_PLAYER_HEARD
			RETURN TRUE
		ENDIF
		
		IF fDistance2 <= 2500 // 50 * 50
		AND IS_PED_IN_ANY_VEHICLE(ClosestPlayerPed)
			VEHICLE_INDEX closestPlayerVeh = GET_VEHICLE_PED_IS_IN(ClosestPlayerPed)
			IF IS_HORN_ACTIVE(closestPlayerVeh)
			OR IS_VEHICLE_IN_BURNOUT(closestPlayerVeh)
				PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by hearing car of part ",iClosestAggroPart)
				iPartCausingFail = iClosestAggroPart
				iSpookedReason = ciSPOOK_PLAYER_HEARD
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedAggroZone > -1
		IF IS_BIT_SET(iAlertPedsZoneBitSet, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedAggroZone)
			PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - Alert Zone: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedAggroZone, " returning TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF HAVE_CCTV_CAMERAS_WITH_AGGRO_BS_BEEN_ALERTED(iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAggroIndexBS_Entity_Ped)
		PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - Alerted by CCTV Camera. MC_serverBD.iPartCausingFail[",iTeam,"] = ", MC_serverBD.iPartCausingFail[iTeam])
		
		IF MC_serverBD.iPartCausingFail[iTeam] != -1
			PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - Player who alerted CCTV was ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MC_serverBD.iPartCausingFail[iTeam])))
			iPartCausingFail = MC_serverBD.iPartCausingFail[iTeam]
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iHackFailsForAggro != -1
		IF GET_HACK_FAILS() >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iHackFailsForAggro
			PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by iHackingFails ",GET_HACK_FAILS()," >= iHackFailsForAggro ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iHackFailsForAggro)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSixteen, ciPED_BSSixteen_AggroWithMultipleFingerprintQuits)
		IF iFingerprintClone_Exits >= 2
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][SpookAggro] - HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped spooked by iFingerprintClone_Exits >= 2 - ciPED_BSSixteen_AggroWithMultipleFingerprintQuits is set.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetNine, ciPed_BSNine_Check_For_Aggro_On_Spawn)
	AND (FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedBitset, iPed) OR FMMC_IS_LONG_BIT_SET(iPedLocalSpookedBitset, iPed))
		PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - Spawning into aggro'd world state")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_AggroOnSeeingDeadBody)
	
		IF FMMC_IS_LONG_BIT_SET(iPedSeenDeadBodyBitset, iped)
			PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by seeing dead body")
			iSpookedReason = ciSPOOK_SEEN_BODY
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour, ciPED_BSFour_AggroIf_T0_DroppedOutofSecondaryAnim + MC_playerBD[iClosestAggroPart].iteam)
		IF IS_BIT_SET(MC_playerBD[iClosestAggroPart].iClientBitSet,PBBOOL_DROPPED_OUT_OF_SECONDARY_ANIM)
		AND IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,ClosestPlayerPed)
		AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed, ClosestPlayerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
			PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by seeing part ",iClosestAggroPart," not performing secondary anim")
			iPartCausingFail = iClosestAggroPart
			iSpookedReason = ciSPOOK_SEEN_PLAYER_NOT_IN_SECONDARY_ANIM
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ((NOT bVehicle) AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped))
		IF (NOT IS_PED_INJURED(ClosestPlayerPed))
		AND IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,ClosestPlayerPed)
		AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed, ClosestPlayerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
			//check if player has weapon equipped in front of the ped
			WEAPON_TYPE eWeapon
			IF GET_CURRENT_PED_WEAPON(ClosestPlayerPed, eWeapon)
				IF NOT IS_PED_IN_ANY_VEHICLE(ClosestPlayerPed)
					PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by seeing part ",iClosestAggroPart," with weapon ", GET_WEAPON_NAME(eWeapon))
					iPartCausingFail = iClosestAggroPart
					iSpookedReason = ciSPOOK_PLAYER_WITH_WEAPON
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Part loop:
	IF (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_AggroIf_T0_DroppedOutofSecondaryAnim)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_AggroIf_T1_DroppedOutofSecondaryAnim)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_AggroIf_T2_DroppedOutofSecondaryAnim)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_AggroIf_T3_DroppedOutofSecondaryAnim))
	OR ((NOT bVehicle) AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped))
		
		WEAPON_TYPE eWeapon //Placed here rather than in FOR loop below to avoid 'value will persist across iterations' info annoyance when compiling
		
		INT iTeamAggroBitset = GET_AGGRO_FAIL_TEAM_BITSET_FOR_PED(iPed)
		DO_PARTICIPANT_LOOP_FLAGS eFlags = GET_PARTICIPANT_LOOP_FLAGS_FROM_VALID_TEAM_BITSET(iTeamAggroBitset)
		
		INT iPartLoop = -1
		WHILE DO_PARTICIPANT_LOOP(iPartLoop, eFlags | DPLF_CHECK_PED_ALIVE | DPLF_FILL_PLAYER_IDS)
			
			IF iPartLoop = iClosestAggroPart
				//Don't check closest player again
				RELOOP
			ENDIF
		
			PED_INDEX PlayerPed = piParticipantLoop_PedIndex
			IF IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,PlayerPed)
			AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed, PlayerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_AggroIf_T0_DroppedOutofSecondaryAnim + MC_playerBD[iPartLoop].iteam)
					IF IS_BIT_SET(MC_playerBD[iPartLoop].iClientBitSet,PBBOOL_DROPPED_OUT_OF_SECONDARY_ANIM)
						PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by seeing part ",iPartLoop," not performing secondary anim")
						iPartCausingFail = iPartLoop
						iSpookedReason = ciSPOOK_SEEN_PLAYER_NOT_IN_SECONDARY_ANIM
						RETURN TRUE
					ENDIF
				ENDIF
				
				IF ((NOT bVehicle) AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped))
					//check if player has weapon equipped in front of the ped
					IF GET_CURRENT_PED_WEAPON(PlayerPed, eWeapon)
						IF NOT IS_PED_IN_ANY_VEHICLE(PlayerPed)
							PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by seeing part ",iPartLoop," with weapon ", GET_WEAPON_NAME(eWeapon))
							iPartCausingFail = iPartLoop
							iSpookedReason = ciSPOOK_PLAYER_WITH_WEAPON
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDWHILE
	ENDIF
	
	// === ALWAYS KEEP THIS LAST ===
	// We don't want to overwrite a 'more' valid participant causing failure.
	IF FMMC_IS_LONG_BIT_SET(iPedSASTriggeredLocally, sPedState.iIndex)
		PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - spooked by part: ", iPartCausingFail, " doing something hostile.")
		iPartCausingFail = iLocalPart
		iSpookedReason = ciSPOOK_PLAYER_GENERIC
		RETURN TRUE
	ENDIF
	
	PRINTLN("[Peds][Ped ", iPed, "] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - FALSE on ped ",iped)
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_SERVER_PED_CANCEL_TASKS(INT iped)
	
	IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedCancelTasksBitset, iPed)
		FMMC_SET_LONG_BIT(MC_serverBD.iPedCancelTasksBitset, iPed)
		
		PRINTLN("[Peds][Ped ", iPed, "] PROCESS_SERVER_PED_CANCEL_TASKS - cancelling tasks to get into combat...")
		MC_ServerBD_2.iAssociatedAction[iped] = ciACTION_NOTHING
		MC_serverBD.niTargetID[iped] = NULL
		MC_serverBD_2.iTargetID[iped] = -1
		SET_PED_STATE(iped,ciTASK_GENERAL_COMBAT #IF IS_DEBUG_BUILD , 4 #ENDIF )
		FMMC_SET_LONG_BIT(MC_serverBD.iPedAtGotoLocBitset, iPed)
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Dialogue Helpers
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL LOAD_TEXT_FOR_CURRENT_SCRIPTED_ANIM_CONVERSATION()

	IF HAS_THIS_ADDITIONAL_TEXT_LOADED(sCurrentScriptedAnimConversationData.tl23_Block, DLC_MISSION_DIALOGUE_TEXT_SLOT)
		PRINTLN("[ScriptedAnimConversation_SPAM] LOAD_TEXT_FOR_CURRENT_SCRIPTED_ANIM_CONVERSATION | We've already loaded ", sCurrentScriptedAnimConversationData.tl23_Block, " in the standard slot")
		RETURN TRUE
	ENDIF
	
	REQUEST_ADDITIONAL_TEXT_FOR_DLC(sCurrentScriptedAnimConversationData.tl23_Block, DLC_MISSION_DIALOGUE_TEXT_SLOT2) 
	IF HAS_THIS_ADDITIONAL_TEXT_LOADED(sCurrentScriptedAnimConversationData.tl23_Block, DLC_MISSION_DIALOGUE_TEXT_SLOT2)
		PRINTLN("[ScriptedAnimConversation_SPAM] LOAD_TEXT_FOR_CURRENT_SCRIPTED_ANIM_CONVERSATION | Loaded text block: ", sCurrentScriptedAnimConversationData.tl23_Block)
		RETURN TRUE
	ENDIF
	
	PRINTLN("[ScriptedAnimConversation] LOAD_TEXT_FOR_CURRENT_SCRIPTED_ANIM_CONVERSATION | Haven't loaded text block: ", sCurrentScriptedAnimConversationData.tl23_Block, " yet")
	RETURN FALSE
ENDFUNC

FUNC BOOL START_CURRENT_SCRIPTED_ANIM_CONVERSATION()
	
	IF IS_THIS_CONVERSATION_ROOT_PLAYING(sCurrentScriptedAnimConversationData.tl23_Root)
		PRINTLN("[ScriptedAnimConversation] - START_CURRENT_SCRIPTED_ANIM_CONVERSATION - Conversation ", sCurrentScriptedAnimConversationData.tl23_Root, " is already playing!")
		RETURN TRUE
	ENDIF
	
	IF NOT LOAD_TEXT_FOR_CURRENT_SCRIPTED_ANIM_CONVERSATION()
		RETURN FALSE
	ENDIF
		
	// Add all the relevant peds
	INT iScriptedAnimConversationPed
	FOR iScriptedAnimConversationPed = 0 TO ciScriptedAnimConversation_MaxPeds - 1
		INT iPedIndex = sCurrentScriptedAnimConversationData.sConversationPedData[iScriptedAnimConversationPed].iConversationPedIndex
		
		IF iPedIndex = -1
			RELOOP
		ENDIF
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPedIndex])
			PRINTLN("[ScriptedAnimConversation] - START_CURRENT_SCRIPTED_ANIM_CONVERSATION - Ped ", iPedIndex, " doesn't exist!")
			CLEAR_SCRIPTED_ANIM_CONVERSATION(sCurrentScriptedAnimConversationData)
			RETURN FALSE
		ENDIF
		
		PED_INDEX piPedSpeaker = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPedIndex])
		
		IF IS_PED_INJURED(piPedSpeaker)
			PRINTLN("[ScriptedAnimConversation] - START_CURRENT_SCRIPTED_ANIM_CONVERSATION - Ped ", iPedIndex, " is injured!")
			CLEAR_SCRIPTED_ANIM_CONVERSATION(sCurrentScriptedAnimConversationData)
			RETURN FALSE
		ENDIF
		
		PRINTLN("[ScriptedAnimConversation_SPAM] - START_CURRENT_SCRIPTED_ANIM_CONVERSATION - Adding ped ", iPedIndex, " / ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(piPedSpeaker)), " / Conversation ped ", iScriptedAnimConversationPed, " || iSpeakerID: ", sCurrentScriptedAnimConversationData.sConversationPedData[iScriptedAnimConversationPed].iSpeakerID, " || Voice: ", sCurrentScriptedAnimConversationData.sConversationPedData[iScriptedAnimConversationPed].tl23_VoiceName)
		ADD_PED_FOR_DIALOGUE(sScriptedAnimConversation_ConversationStruct, sCurrentScriptedAnimConversationData.sConversationPedData[iScriptedAnimConversationPed].iSpeakerID, piPedSpeaker, sCurrentScriptedAnimConversationData.sConversationPedData[iScriptedAnimConversationPed].tl23_VoiceName, sCurrentScriptedAnimConversationData.sConversationPedData[iScriptedAnimConversationPed].bPedPlayAmbientAnims, sCurrentScriptedAnimConversationData.sConversationPedData[iScriptedAnimConversationPed].bPedCanUseAutoLookAt)
	ENDFOR
	
	// Add all the relevant participants
	INT iScriptedAnimConversationParticipant
	FOR iScriptedAnimConversationParticipant = 0 TO ciScriptedAnimConversation_MaxPeds - 1
		INT iParticipantIndex = sCurrentScriptedAnimConversationData.sConversationPedData[iScriptedAnimConversationParticipant].iConversationParticipantIndex
		
		IF iParticipantIndex = -1
			RELOOP
		ENDIF
		
		PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iParticipantIndex)
		PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
		
		IF NOT IS_NET_PLAYER_OK(piPlayer, TRUE, FALSE)
			PRINTLN("[ScriptedAnimConversation] - START_CURRENT_SCRIPTED_ANIM_CONVERSATION - Participant ", iParticipantIndex, " isn't ok!")
			CLEAR_SCRIPTED_ANIM_CONVERSATION(sCurrentScriptedAnimConversationData)
			RETURN FALSE
		ENDIF
		
		PED_INDEX piParticipantSpeaker = GET_PLAYER_PED(piPlayer)
		
		IF NOT DOES_ENTITY_EXIST(piParticipantSpeaker)
			PRINTLN("[ScriptedAnimConversation] - START_CURRENT_SCRIPTED_ANIM_CONVERSATION - Participant ", iParticipantIndex, " doesn't exist!")
			CLEAR_SCRIPTED_ANIM_CONVERSATION(sCurrentScriptedAnimConversationData)
			RETURN FALSE
		ENDIF
		
		IF IS_PED_INJURED(piParticipantSpeaker)
			PRINTLN("[ScriptedAnimConversation] - START_CURRENT_SCRIPTED_ANIM_CONVERSATION - Participant ", iParticipantIndex, " is injured!")
			CLEAR_SCRIPTED_ANIM_CONVERSATION(sCurrentScriptedAnimConversationData)
			RETURN FALSE
		ENDIF
		
		PRINTLN("[ScriptedAnimConversation_SPAM] - START_CURRENT_SCRIPTED_ANIM_CONVERSATION - Adding Participant ", iParticipantIndex, " / ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(piParticipantSpeaker)), " / Conversation ped ", iScriptedAnimConversationParticipant, " || iSpeakerID: ", sCurrentScriptedAnimConversationData.sConversationPedData[iScriptedAnimConversationParticipant].iSpeakerID)
		ADD_PED_FOR_DIALOGUE(sScriptedAnimConversation_ConversationStruct, sCurrentScriptedAnimConversationData.sConversationPedData[iScriptedAnimConversationParticipant].iSpeakerID, piParticipantSpeaker, sCurrentScriptedAnimConversationData.sConversationPedData[iScriptedAnimConversationParticipant].tl23_VoiceName, sCurrentScriptedAnimConversationData.sConversationPedData[iScriptedAnimConversationParticipant].bPedPlayAmbientAnims, sCurrentScriptedAnimConversationData.sConversationPedData[iScriptedAnimConversationParticipant].bPedCanUseAutoLookAt)
	ENDFOR
	
	// Kick off the conversation
	IF CREATE_CONVERSATION(sScriptedAnimConversation_ConversationStruct, sCurrentScriptedAnimConversationData.tl23_Block, sCurrentScriptedAnimConversationData.tl23_Root, sCurrentScriptedAnimConversationData.ePriority)
		PRINTLN("[ScriptedAnimConversation] - START_CURRENT_SCRIPTED_ANIM_CONVERSATION - Started Conversation || tl23_Block: ", sCurrentScriptedAnimConversationData.tl23_Block, " | tl23_Root: ", sCurrentScriptedAnimConversationData.tl23_Root, " | ePriority: ", sCurrentScriptedAnimConversationData.ePriority)
		RETURN TRUE
	ELSE
		PRINTLN("[ScriptedAnimConversation] - START_CURRENT_SCRIPTED_ANIM_CONVERSATION - Failed to start conversation this frame || tl23_Block: ", sCurrentScriptedAnimConversationData.tl23_Block, " | tl23_Root: ", sCurrentScriptedAnimConversationData.tl23_Root, " | ePriority: ", sCurrentScriptedAnimConversationData.ePriority)		
		RETURN FALSE
	ENDIF
ENDFUNC

PROC PROCESS_SCRIPTED_ANIM_CONVERSATION()
	
	IF NOT IS_SCRIPTED_ANIM_CONVERSATION_IN_USE()
		EXIT
	ENDIF
	
	PRINTLN("[ScriptedAnimConversation_SPAM] - PROCESS_SCRIPTED_ANIM_CONVERSATION - Processing ", sCurrentScriptedAnimConversationData.tl23_Root, "! | iScriptedAnimConversationState: ", iScriptedAnimConversationState)
	
	IF IS_BIT_SET(gMC_LocalVariables_VARS.iLocalBGScriptBS, ciLocalBGScriptBS_ScriptedAnimConv_CancelCurrentConversation)
		PRINTLN("[ScriptedAnimConversation] - PROCESS_SCRIPTED_ANIM_CONVERSATION - Trying to end current conversation due to ciLocalBGScriptBS_ScriptedAnimConv_CancelCurrentConversation")
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		CLEAR_BIT(gMC_LocalVariables_VARS.iLocalBGScriptBS, ciLocalBGScriptBS_ScriptedAnimConv_CancelCurrentConversation)
	ENDIF
	
	SWITCH iScriptedAnimConversationState
		CASE ciScriptedAnimConversationState_WaitingToStart
			IF START_CURRENT_SCRIPTED_ANIM_CONVERSATION()
				SET_SCRIPTED_ANIM_CONVERSATION_STATE(ciScriptedAnimConversationState_StartedConversation)
			ENDIF
		BREAK
		
		CASE ciScriptedAnimConversationState_StartedConversation
			// Wait for the conversation to have started and then move on
			IF IS_SCRIPTED_CONVERSATION_ONGOING()					
				IF IS_BIT_SET(sCurrentScriptedAnimConversationData.iBS, ciScriptedAnimConversationBS_StartPaused)
					PRINTLN("[ScriptedAnimConversation] - PROCESS_SCRIPTED_ANIM_CONVERSATION - Conversation has started but is paused for syncing!")
					SET_SCRIPTED_ANIM_CONVERSATION_STATE(ciScriptedAnimConversationState_PaussedConversation)
					PAUSE_SCRIPTED_CONVERSATION(TRUE)
				ELSE
					PRINTLN("[ScriptedAnimConversation] - PROCESS_SCRIPTED_ANIM_CONVERSATION - Conversation has officially started!")
					SET_SCRIPTED_ANIM_CONVERSATION_STATE(ciScriptedAnimConversationState_ConversationPlaying)
				ENDIF
				
			ELIF NOT IS_STRING_NULL_OR_EMPTY(g_ConversationData.ConversationSegmentToGrab) // If a conversation is stored in g_ConversationData.ConversationSegmentToGrab...
			AND NOT ARE_STRINGS_EQUAL(g_ConversationData.ConversationSegmentToGrab, sCurrentScriptedAnimConversationData.tl23_Root) // And that conversation ISN'T the one we're hoping for right now...
				PRINTLN("[ScriptedAnimConversation] - PROCESS_SCRIPTED_ANIM_CONVERSATION - A different conversation ('", sCurrentScriptedAnimConversationData.tl23_Root, "' != '", g_ConversationData.ConversationSegmentToGrab, "') has started! (1)")
				CLEAR_SCRIPTED_ANIM_CONVERSATION(sCurrentScriptedAnimConversationData)
				
			ELSE
				PRINTLN("[ScriptedAnimConversation_SPAM] - PROCESS_SCRIPTED_ANIM_CONVERSATION - Waiting for conversation ", sCurrentScriptedAnimConversationData.tl23_Root, " to officially start...")
				
			ENDIF
		BREAK
		
		CASE ciScriptedAnimConversationState_PaussedConversation
			PRINTLN("[ScriptedAnimConversation] - PROCESS_SCRIPTED_ANIM_CONVERSATION - Conversation is currently paused.")
			IF NOT IS_BIT_SET(sCurrentScriptedAnimConversationData.iBS, ciScriptedAnimConversationBS_StartPaused)
				SET_SCRIPTED_ANIM_CONVERSATION_STATE(ciScriptedAnimConversationState_ConversationPlaying)			
				RESTART_SCRIPTED_CONVERSATION()	
			ENDIF
		BREAK
		
		CASE ciScriptedAnimConversationState_ConversationPlaying			
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				PRINTLN("[ScriptedAnimConversation] - PROCESS_SCRIPTED_ANIM_CONVERSATION - Conversation has ended!")
				CLEAR_SCRIPTED_ANIM_CONVERSATION(sCurrentScriptedAnimConversationData)
			
			ELIF NOT IS_THIS_CONVERSATION_ROOT_PLAYING(sCurrentScriptedAnimConversationData.tl23_Root)
			AND NOT IS_STRING_NULL_OR_EMPTY(g_ConversationData.ConversationSegmentToGrab)
				PRINTLN("[ScriptedAnimConversation] - PROCESS_SCRIPTED_ANIM_CONVERSATION - A different conversation (", sCurrentScriptedAnimConversationData.tl23_Root, " != ", g_ConversationData.ConversationSegmentToGrab, ") has started! (2)")
				CLEAR_SCRIPTED_ANIM_CONVERSATION(sCurrentScriptedAnimConversationData)
				
			ELSE
				PRINTLN("[ScriptedAnimConversation_SPAM] - PROCESS_SCRIPTED_ANIM_CONVERSATION - Scripted anim conversation ", sCurrentScriptedAnimConversationData.tl23_Root, " still playing...")
				
			ENDIF
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF bScriptedAnimConversationDebug
		TEXT_LABEL_63 tlVisualDebug = "iScriptedAnimConversationState: "
		tlVisualDebug += iScriptedAnimConversationState
		DRAW_DEBUG_TEXT_2D(tlVisualDebug, (<<0.25, 0.175, 0.5>>), 255, 255, 255, 255)
		
		tlVisualDebug = "tl23_Root: "
		tlVisualDebug += sCurrentScriptedAnimConversationData.tl23_Root
		DRAW_DEBUG_TEXT_2D(tlVisualDebug, (<<0.25, 0.2, 0.5>>), 255, 255, 255, 255)
		
		tlVisualDebug = "Current Playing Conversation: "
		tlVisualDebug += g_ConversationData.ConversationSegmentToGrab
		DRAW_DEBUG_TEXT_2D(tlVisualDebug, (<<0.25, 0.25, 0.5>>), 255, 0, 0, 255)
		
		tlVisualDebug = "GET_CURRENT_SCRIPTED_CONVERSATION_LINE: "
		tlVisualDebug += GET_CURRENT_SCRIPTED_CONVERSATION_LINE()
		DRAW_DEBUG_TEXT_2D(tlVisualDebug, (<<0.25, 0.275, 0.5>>), 255, 0, 0, 255)
	ENDIF
	#ENDIF
ENDPROC


