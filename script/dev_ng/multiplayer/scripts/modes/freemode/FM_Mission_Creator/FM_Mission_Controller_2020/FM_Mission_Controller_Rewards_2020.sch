// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Rewards -----------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script Name: FM_Mission_Controller_2020.sc ------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Money, RP, Rewards, Stats & Telemetry. Pack specific functionality should go in a new header inside RewardsHeaders. 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Rewards_CayoPerico_2020.sch"
USING "FM_Mission_Controller_Rewards_Tuners_TheContract_2020.sch"
		
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Medals/Ranks
// ##### Description: Functions for calculating heist medals/ranks
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_SCORE_CONTRIBUTION_TIME_START_VALUE()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_TIME_START_VALUE
	ENDIF

	RETURN 5000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_TIME_MULTIPLIER()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_TIME_MULTIPLIER
	ENDIF
	
	RETURN 2
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_KILLS_MULTIPLIER()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_KILLS_MULTIPLIER
	ENDIF
	
	RETURN 100
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_KILLS_CAP()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_KILLS_CAP
	ENDIF
	
	RETURN 100
ENDFUNC

FUNC FLOAT GET_SCORE_CONTRIBUTION_AMBIENT_COP_KILL_MULTIPLIER()
	RETURN 1.0
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_AMBIENT_COP_KILL_CAP()
	RETURN HIGHEST_INT
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_HEADSHOT_MULTIPLIER()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_HEADSHOT_MULTIPLIER
	ENDIF
	
	RETURN 200
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_HEADSHOT_CAP()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_HEADSHOT_CAP
	ENDIF
	
	RETURN 25
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_SAFE_DRIVE_CAP()
	RETURN 2000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_SAFE_DRIVE_MULTIPLIER()
	RETURN 1
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_ACCURACY_MULTIPLIER()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_ACCURACY_MULTIPLIER
	ENDIF
	
	RETURN 50
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_CIV_KILLS_START()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_CIV_KILLS_START
	ENDIF
	
	RETURN 2000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_CIV_KILLS_MULTIPLIER()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_CIV_KILLS_MULTIPLIER
	ENDIF
	
	RETURN 100
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_CIV_KILLS_CAP()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_CIV_KILLS_CAP
	ENDIF
	
	RETURN HIGHEST_INT
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_HEALTH_START()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_HEALTH_START
	ENDIF
	
	RETURN 3000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_HEALTH_MULTIPLIER_0()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_HEALTH_MULTIPLIER_0
	ENDIF
	
	RETURN 1000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_HEALTH_MULTIPLIER_1()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_HEALTH_MULTIPLIER_1
	ENDIF
	
	RETURN 5
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_HACK_SCORE_START_VALUE()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_HACK_SCORE_START_VALUE
	ENDIF
	
	RETURN 500
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_HACK_SCORE_MULTIPLIER()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_HACK_SCORE_MULTIPLIER
	ENDIF
	
	RETURN 100
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_HACK_SCORE_CAP()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_HACK_SCORE_CAP
	ENDIF
	
	RETURN 5
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_WANTED_LEVEL_MAX()
	RETURN 10
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_WANTED_LEVEL_MULTIPLIER()
	RETURN 500
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_WANTED_LEVEL_TIME_MULTIPLIER()
	RETURN 1
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_WANTED_LEVEL_SCORE_CAP()
	RETURN 5000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_PLAYER_SCORE_MULTIPLIER()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_PLAYER_SCORE_MULTIPLIER
	ENDIF
	
	RETURN 2000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_PLAYER_SCORE_CAP()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_PLAYER_SCORE_CAP
	ENDIF
	
	RETURN 10000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_STEALTH_KILLS_MULTIPLIER()
	RETURN 500
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_STEALTH_KILLS_CAP()
	RETURN 5000
ENDFUNC
	
FUNC INT GET_SCORE_CONTRIBUTION_PED_DAMAGE_SCORE_CAP()
	RETURN 10000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_PED_DAMAGE_SCORE_MULTIPLIER()
	RETURN 15
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_VEH_DAMAGE_SCORE_CAP()
	RETURN 10000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_VEH_DAMAGE_SCORE_MULTIPLIER()
	RETURN 15
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_INTERACTABLES_CAP()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_INTERACTABLES_CAP
	ENDIF
	
	RETURN 20000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_INTERACTABLES_MULTIPLIER()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_INTERACTABLES_MULTIPLIER
	ENDIF
	
	RETURN 100
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_LOOT_GRABBED_CAP()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_LOOT_GRABBED_CAP
	ENDIF
	
	RETURN 20000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_LOOT_GRABBED_MULTIPLIER()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_LOOT_GRABBED_MULTIPLIER
	ENDIF
	
	RETURN 100
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_EQUIPMENT_COLLECTED_CAP()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_EQUIPMENT_COLLECTED_CAP
	ENDIF
	
	RETURN 20000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_EQUIPMENT_COLLECTED_MULTIPLIER()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_EQUIPMENT_COLLECTED_MULTIPLIER
	ENDIF
	
	RETURN 1000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_OBJECTIVE_CAP()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_OBJECTIVE_CAP
	ENDIF
	
	RETURN 100000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_OBJECTIVE_MULTIPLIER()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN g_sMPTunables.iIH_MEDAL_RANKING_OBJECTIVE_MULTIPLIER
	ENDIF
	
	RETURN 2000
ENDFUNC

PROC INCREMENT_MEDAL_OBJECTIVES_COMPLETED(INT iTeam, INT iRule)

	IF iRule < 0
	OR iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_DONT_COUNT_TOWARDS_SCORE)  
		EXIT
	ENDIF
	
	MC_playerBD[iLocalPart].iMedalObjectives++
	PRINTLN("[Rewards_Rank] - INCREMENT_MEDAL_OBJECTIVES_COMPLETED - Medal Objectives: ", MC_playerBD[iLocalPart].iMedalObjectives)

ENDPROC

PROC INCREMENT_MEDAL_INTERACTABLES_COMPLETED()
	
	MC_playerBD[iLocalPart].iMedalInteractables++
	PRINTLN("[Rewards_Rank] - INCREMENT_MEDAL_INTERACTABLES_COMPLETED - Medal Interactables: ", MC_playerBD[iLocalPart].iMedalInteractables)

ENDPROC

PROC INCREMENT_MEDAL_EQUIPMENT_COLLECTED()
	
	MC_playerBD[iLocalPart].iMedalEquipment++
	PRINTLN("[Rewards_Rank] - INCREMENT_MEDAL_EQUIPMENT_COLLECTED - Medal Equipment: ", MC_playerBD[iLocalPart].iMedalEquipment)

ENDPROC 

PROC GET_LOCAL_PLAYER_ACCURACY_DATA(INT &iShots, INT &iHits)
	
	iShots = GET_MP_INT_CHARACTER_STAT(MP_STAT_SHOTS) 
			+ GET_MP_INT_CHARACTER_STAT(MP_STAT_INSURGENT_TURR_SHOTS)
			+ GET_MP_INT_CHARACTER_STAT(MP_STAT_VALKYRIE_CANNON_SHOTS)
			+ GET_MP_INT_CHARACTER_STAT(MP_STAT_VALKYRIE_TURR_SHOTS)
	
	iHits = GET_MP_INT_CHARACTER_STAT(MP_STAT_HITS_PEDS_VEHICLES) 
			+ GET_MP_INT_CHARACTER_STAT(MP_STAT_INSURGENT_TURR_HITS)
			+ GET_MP_INT_CHARACTER_STAT(MP_STAT_VALKYRIE_CANNON_KILLS)
			+ GET_MP_INT_CHARACTER_STAT(MP_STAT_VALKYRIE_TURR_HITS)
	
	PRINTLN("[Rewards_Rank] - GET_LOCAL_PLAYER_ACCURACY_DATA - Shots: ", iShots, ", Hits: ", iHits)
	
ENDPROC

PROC PROCESS_LOCAL_PLAYER_END_ACCURACY()

	IF MC_playerBD[iLocalPart].iEndAccuracy != -1
		//Already Done
		EXIT
	ENDIF
	
	INT iEndHits, iEndShots
	GET_LOCAL_PLAYER_ACCURACY_DATA(iEndShots, iEndHits)
	
	IF iEndShots - MC_playerBD[iLocalPart].iStartShots >= 1
		FLOAT fAccuracy = (TO_FLOAT(iEndHits - MC_playerBD[iLocalPart].iStartHits) / TO_FLOAT(iEndShots - MC_playerBD[iLocalPart].iStartShots))
		MC_playerBD[iLocalPart].iEndAccuracy = ROUND(fAccuracy * 100.0)
	ELSE
		MC_playerBD[iLocalPart].iEndAccuracy = 0
	ENDIF
	
	PRINTLN("[Rewards_Rank] - PROCESS_LOCAL_PLAYER_END_ACCURACY - Accuracy: ", MC_playerBD[iLocalPart].iEndAccuracy)
	
ENDPROC

PROC PROCESS_LOCAL_PLAYER_END_HEALTH()

	IF MC_playerBD[iLocalPart].iEndHealthLoss != -1
		//Already Done
		EXIT
	ENDIF
	
	MC_playerBD[iLocalPart].iEndHealthLoss = 0
	
	IF bLocalPlayerPedOk
		INT iHealthLost = GET_PED_MAX_HEALTH(LocalPlayerPed) - GET_ENTITY_HEALTH(LocalPlayerPed)
		IF iHealthLost > 0
			MC_playerBD[iLocalPart].iEndHealthLoss = iHealthLost
		ENDIF
	ENDIF
	
	PRINTLN("[Rewards_Rank] - PROCESS_LOCAL_PLAYER_END_HEALTH - Health Lost: ", MC_playerBD[iLocalPart].iEndHealthLoss)
	
ENDPROC

FUNC INT GET_LOCAL_PLAYER_STEALTH_KILLS_DATA()

	RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_KILLS_STEALTH)
	
ENDFUNC

PROC PROCESS_LOCAL_PLAYER_END_STEALTH_KILLS()

	IF MC_playerBD[iLocalPart].iStealthKills != -1
		//Already Done
		EXIT
	ENDIF
	
	INT iEndKills = GET_LOCAL_PLAYER_STEALTH_KILLS_DATA()
	
	INT iStealthKills = iEndKills - MC_playerBD[iLocalPart].iStartStealthKills
	IF iStealthKills >= 1
		MC_playerBD[iLocalPart].iStealthKills = iStealthKills
	ELSE
		MC_playerBD[iLocalPart].iStealthKills = 0
	ENDIF
	
	PRINTLN("[Rewards_Rank] - PROCESS_LOCAL_PLAYER_END_STEALTH_KILLS - Kills: ", MC_playerBD[iLocalPart].iStealthKills)
	
ENDPROC

FUNC INT GET_MEDAL_SCORE_CAP_FOR_CONTENT()
	
	IF IS_THIS_A_VERSUS_MISSION()
		RETURN g_sMPtunables.iGLOBAL_LEADERBOARD_CAP_ADVERSARY_MODES
	ENDIF
	
	RETURN g_sMPtunables.iGLOBAL_LEADERBOARD_CAP_CONTACT_MISSIONS
	
ENDFUNC

FUNC INT GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT(INT iParticipant)

	INT iHighScore
	INT iBonus

	IF MC_PlayerBD[iParticipant].iPlayerScore > 0
		iBonus = GET_SCORE_CONTRIBUTION_PLAYER_SCORE_MULTIPLIER() * MC_PlayerBD[iParticipant].iPlayerScore
		iBonus = CLAMP_INT(iBonus, 0, GET_SCORE_CONTRIBUTION_PLAYER_SCORE_CAP())
		IF iBonus > 0
			iHighScore += iBonus
			PRINTLN("[Rewards_Rank] - GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT - Player Score Bonus - Score:  ", MC_PlayerBD[iParticipant].iPlayerScore, ", Adding points: ", iBonus, ", New total: ", iHighScore)
		ENDIF
	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_IGNORE_SCORE_TIME)
		INT iTimeSeconds = MC_PlayerBD[iParticipant].iMissionEndTime / 1000
		iBonus = GET_SCORE_CONTRIBUTION_TIME_START_VALUE() - (iTimeSeconds * GET_SCORE_CONTRIBUTION_TIME_MULTIPLIER())
		IF iBonus > 0
			iHighScore += iBonus
			PRINTLN("[Rewards_Rank] - GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT - Completion Time Bonus - Time:  ", iTimeSeconds, ", Adding points: ", iBonus, ", New total: ", iHighScore)
		ENDIF
	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_IGNORE_SCORE_ENEMIES)
		INT iKills = MC_PlayerBD[iParticipant].iNumPlayerKills + MC_PlayerBD[iParticipant].iNumPedKills + FLOOR(TO_FLOAT(CLAMP_INT(MC_playerBD[iParticipant].iAmbientCopsKilled, 0, GET_SCORE_CONTRIBUTION_AMBIENT_COP_KILL_CAP())) * GET_SCORE_CONTRIBUTION_AMBIENT_COP_KILL_MULTIPLIER())
		iBonus = iKills * GET_SCORE_CONTRIBUTION_KILLS_MULTIPLIER()
		iBonus = CLAMP_INT(iBonus, 0, (GET_SCORE_CONTRIBUTION_KILLS_MULTIPLIER() * GET_SCORE_CONTRIBUTION_KILLS_CAP()))
		IF iBonus > 0
			iHighScore += iBonus
			PRINTLN("[Rewards_Rank] - GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT - Kill Bonus - Kills:  ", iKills, ", Adding points: ", iBonus, ", New total: ", iHighScore)
		ENDIF
	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_IGNORE_SCORE_HEADSHOTS)
		INT iHeadshots = CLAMP_INT(MC_PlayerBD[iParticipant].iNumHeadshots, 0, GET_SCORE_CONTRIBUTION_HEADSHOT_CAP())
		iBonus = iHeadshots * GET_SCORE_CONTRIBUTION_HEADSHOT_MULTIPLIER()
		IF iBonus > 0
			iHighScore += iBonus
			PRINTLN("[Rewards_Rank] - GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT - Headshot Bonus - Headshots:  ", iHeadshots, ", Adding points: ", iBonus, ", New total: ", iHighScore)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_IGNORE_SCORE_STEALTH_KILLS)
		iBonus = MC_PlayerBD[iParticipant].iStealthKills * GET_SCORE_CONTRIBUTION_STEALTH_KILLS_MULTIPLIER()
		iBonus = CLAMP_INT(iBonus, 0, GET_SCORE_CONTRIBUTION_STEALTH_KILLS_CAP())
		IF iBonus > 0
			iHighScore += iBonus
			PRINTLN("[Rewards_Rank] - GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT - Stealth Kills Bonus - Kills:  ", MC_PlayerBD[iParticipant].iStealthKills, ", Adding points: ", iBonus, ", New total: ", iHighScore)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_IGNORE_SCORE_SAFE_DRIVE)
		iBonus = GET_SCORE_CONTRIBUTION_SAFE_DRIVE_CAP() - (MC_PlayerBD[iParticipant].iVehDamage * GET_SCORE_CONTRIBUTION_SAFE_DRIVE_MULTIPLIER())
		IF iBonus > 0
			iHighScore += iBonus
			PRINTLN("[Rewards_Rank] - GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT - Safe Driving Bonus - Vehicle Damage:  ", MC_PlayerBD[iParticipant].iVehDamage, ", Adding points: ", iBonus, ", New total: ", iHighScore)
		ENDIF
	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_IGNORE_SCORE_ACCURACY)
		iBonus = MC_playerBD[iParticipant].iEndAccuracy * GET_SCORE_CONTRIBUTION_ACCURACY_MULTIPLIER()
		IF iBonus > 0
			iHighScore += iBonus
			PRINTLN("[Rewards_Rank] - GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT - Accuracy Bonus - Accuracy:  ", MC_playerBD[iParticipant].iEndAccuracy, ", Adding points: ", iBonus, ", New total: ", iHighScore)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_IGNORE_SCORE_CIVILIANS)
		iBonus = GET_SCORE_CONTRIBUTION_CIV_KILLS_START() - (CLAMP_INT(MC_playerBD[iParticipant].iCivilianKills, 0, GET_SCORE_CONTRIBUTION_CIV_KILLS_CAP()) * GET_SCORE_CONTRIBUTION_CIV_KILLS_MULTIPLIER())
		IF iBonus > 0
			iHighScore += iBonus
			PRINTLN("[Rewards_Rank] - GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT - Civilian Kills Bonus - Kills:  ", MC_playerBD[iParticipant].iCivilianKills, ", Adding points: ", iBonus, ", New total: ", iHighScore)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_IGNORE_SCORE_HEALTH)
		iBonus = GET_SCORE_CONTRIBUTION_HEALTH_START() - (MC_PlayerBD[iParticipant].iNumPlayerDeaths * GET_SCORE_CONTRIBUTION_HEALTH_MULTIPLIER_0())
		iBonus -= GET_SCORE_CONTRIBUTION_HEALTH_MULTIPLIER_1() * MC_playerBD[iParticipant].iEndHealthLoss
		IF iBonus > 0
			iHighScore += iBonus
			PRINTLN("[Rewards_Rank] - GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT - Health/Lives Bonus - Deaths:  ", MC_playerBD[iParticipant].iNumPlayerDeaths, ", Health Lost: ", MC_playerBD[iParticipant].iEndHealthLoss, ", Adding points: ", iBonus, ", New total: ", iHighScore)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_IGNORE_SCORE_HACK_SPEED)
		IF MC_playerBD[iParticipant].iNumHacks > 0
			INT iAvgHackTime = (MC_playerBD[iParticipant].iHackTime / MC_playerBD[iParticipant].iNumHacks) / 1000
			iBonus = GET_SCORE_CONTRIBUTION_HACK_SCORE_START_VALUE() - iAvgHackTime
			iBonus += CLAMP_INT(MC_playerBD[iParticipant].iNumHacks, 0, GET_SCORE_CONTRIBUTION_HACK_SCORE_CAP()) * GET_SCORE_CONTRIBUTION_HACK_SCORE_MULTIPLIER()
			iBonus = CLAMP_INT(iBonus, 0, GET_SCORE_CONTRIBUTION_HACK_SCORE_START_VALUE() * 2)
			IF iBonus > 0
				iHighScore += iBonus
				PRINTLN("[Rewards_Rank] - GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT - Hacking Bonus - Hacks:  ", MC_playerBD[iParticipant].iNumHacks, ", Hack Time: ", MC_playerBD[iParticipant].iHackTime, ", Adding points: ", iBonus, ", New total: ", iHighScore)
			ENDIF
		ENDIF
	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS,ciFMMC_IGNORE_SCORE_WANTED_EVADE_SPEED)
		IF MC_playerBD[iParticipant].iNumWantedLose > 0
			INT iAvgWantedLossTime = (MC_playerBD[iParticipant].iWantedTime / MC_playerBD[iParticipant].iNumWantedLose) / 1000
			INT iStarsLost = CLAMP_INT(MC_playerBD[iParticipant].iNumWantedStars, 0, GET_SCORE_CONTRIBUTION_WANTED_LEVEL_MAX())
			iBonus = (iStarsLost * GET_SCORE_CONTRIBUTION_WANTED_LEVEL_MULTIPLIER()) - (iAvgWantedLossTime / GET_SCORE_CONTRIBUTION_WANTED_LEVEL_TIME_MULTIPLIER())
			iBonus = CLAMP_INT(iBonus, 0, GET_SCORE_CONTRIBUTION_WANTED_LEVEL_SCORE_CAP())
			IF iBonus > 0
				iHighScore += iBonus
				PRINTLN("[Rewards_Rank] - GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT - Evade Wanted Bonus - Stars Lost:  ", MC_playerBD[iParticipant].iNumWantedLose, ", Wanted Time: ", MC_playerBD[iParticipant].iWantedTime, ", Adding points: ", iBonus, ", New total: ", iHighScore)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_USE_SCORE_DAMAGE_TO_PEDS)
		iBonus = MC_playerBD_1[iParticipant].iDamageToPedsForMedal * GET_SCORE_CONTRIBUTION_PED_DAMAGE_SCORE_MULTIPLIER()
		iBonus = CLAMP_INT(iBonus, 0, GET_SCORE_CONTRIBUTION_PED_DAMAGE_SCORE_CAP())
		IF iBonus > 0
			iHighScore += iBonus
			PRINTLN("[Rewards_Rank] - GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT - Ped Damage Bonus - Damage:  ", MC_playerBD_1[iParticipant].iDamageToPedsForMedal, ", Adding points: ", iBonus, ", New total: ", iHighScore)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_USE_SCORE_DAMAGE_TO_VEHS)
		iBonus = MC_playerBD_1[iParticipant].iDamageToVehsForMedal * GET_SCORE_CONTRIBUTION_VEH_DAMAGE_SCORE_MULTIPLIER()
		iBonus = CLAMP_INT(iBonus, 0, GET_SCORE_CONTRIBUTION_VEH_DAMAGE_SCORE_CAP())
		IF iBonus > 0
			iHighScore += iBonus
			PRINTLN("[Rewards_Rank] - GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT - Veh Damage Bonus - Damage:  ", MC_playerBD_1[iParticipant].iDamageToVehsForMedal, ", Adding points: ", iBonus, ", New total: ", iHighScore)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_IGNORE_SCORE_OBJECTIVE_COMPLETED)
		iBonus = MC_playerBD[iParticipant].iMedalObjectives * GET_SCORE_CONTRIBUTION_OBJECTIVE_MULTIPLIER()
		iBonus = CLAMP_INT(iBonus, 0, GET_SCORE_CONTRIBUTION_OBJECTIVE_CAP())
		IF iBonus > 0
			iHighScore += iBonus
			PRINTLN("[Rewards_Rank] - GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT - Objective Bonus - Objectives Complete:  ", MC_playerBD[iParticipant].iMedalObjectives, ", Adding points: ", iBonus, ", New total: ", iHighScore)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_USE_SCORE_INTERACTABLES)
		iBonus = MC_playerBD[iParticipant].iMedalInteractables * GET_SCORE_CONTRIBUTION_INTERACTABLES_MULTIPLIER()
		iBonus = CLAMP_INT(iBonus, 0, GET_SCORE_CONTRIBUTION_INTERACTABLES_CAP())
		IF iBonus > 0
			iHighScore += iBonus
			PRINTLN("[Rewards_Rank] - GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT - Interactable Completion Bonus - Interactables Complete:  ", MC_playerBD[iParticipant].iMedalInteractables, ", Adding points: ", iBonus, ", New total: ", iHighScore)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_USE_SCORE_LOOT_GRABBED)
		iBonus = MC_playerBD[iParticipant].sLootBag.iCashGrabbed * GET_SCORE_CONTRIBUTION_LOOT_GRABBED_MULTIPLIER()
		iBonus = CLAMP_INT(iBonus, 0, GET_SCORE_CONTRIBUTION_LOOT_GRABBED_CAP())
		IF iBonus > 0
			iHighScore += iBonus
			PRINTLN("[Rewards_Rank] - GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT - Loot Grabbed Bonus - Loot Value:  $", MC_playerBD[iParticipant].sLootBag.iCashGrabbed, ", Adding points: ", iBonus, ", New total: ", iHighScore)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_USE_SCORE_EQUIPMENT_COLLECTED)
		iBonus = MC_playerBD[iParticipant].iMedalEquipment * GET_SCORE_CONTRIBUTION_EQUIPMENT_COLLECTED_MULTIPLIER()
		iBonus = CLAMP_INT(iBonus, 0, GET_SCORE_CONTRIBUTION_EQUIPMENT_COLLECTED_CAP())
		IF iBonus > 0
			iHighScore += iBonus
			PRINTLN("[Rewards_Rank] - GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT - Equipment Collected Bonus - Equipment Collected:  ", MC_playerBD[iParticipant].iMedalEquipment, ", Adding points: ", iBonus, ", New total: ", iHighScore)
		ENDIF
	ENDIF

	INT iScoreCap = GET_MEDAL_SCORE_CAP_FOR_CONTENT()
	IF iScoreCap != 0
		IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
			PRINTLN("[Rewards_Rank] - GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT - Player out of lives - halving score cap.")
			iScoreCap /= 2
		ENDIF
		
		iHighScore = CLAMP_INT(iHighScore, 0, iScoreCap)
		PRINTLN("[Rewards_Rank] - GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT - Score Cap: ", iScoreCap, ", New total: ", iHighScore)
	ENDIF
	
	RETURN iHighScore
	
ENDFUNC

PROC SORT_MEDAL_RANKINGS_STRUCTS(MEDAL_RANKINGS_STRUCT &sArray[], INT iLeft, INT iRight)

	INT i, j
	INT p = sArray[((iLeft + iRight) / 2)].iScore
	MEDAL_RANKINGS_STRUCT q
	i = iLeft
	j = iRight

	WHILE (i <= j)
	
		WHILE ((sArray[i].iScore > p) AND (i < iRight))
			i++
		ENDWHILE
		
		WHILE ((p > sArray[j].iScore) AND (j > iLeft))
			j--
		ENDWHILE
		
		IF (i <= j)
			q = sArray[i]
			sArray[i] = sArray[j]
			sArray[j] = q
			
			i++
			j--
		ENDIF
	
	ENDWHILE
	
	IF (i < iRight)
		SORT_MEDAL_RANKINGS_STRUCTS(sArray, i, iRight)
	ENDIF
	
	IF (iLeft < j)
		SORT_MEDAL_RANKINGS_STRUCTS(sArray, iLeft, j)
	ENDIF

ENDPROC

PROC PROCESS_MEDAL_RANKINGS()
	
	IF IS_BIT_SET(iLocalBoolCheck9, LBOOL9_MEDAL_RANKINGS_DONE)
		//Already Done
		EXIT
	ENDIF
	
	INT iPart, iPartCount = 0
	MEDAL_RANKINGS_STRUCT sMedalRankings[MAX_NUM_MC_PLAYERS]
	FOR iPart = 0 TO MAX_NUM_MC_PLAYERS - 1
		
		IF IS_STRING_NULL_OR_EMPTY(MC_serverBD_2.tParticipantNames[iPart])
			RELOOP
		ENDIF
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
			RELOOP
		ENDIF
		
		sMedalRankings[iPartCount].bOnMission = TRUE
		sMedalRankings[iPartCount].iPartIndex = iPart
		sMedalRankings[iPartCount].iScore = GET_MEDAL_RANKINGS_SCORE_FOR_PARTICIPANT(iPart)
		
		PRINTLN("[Rewards_Rank] - PROCESS_MEDAL_RANKINGS - adding participant to rankings. Index: ", iPartCount, ", PartId: ", iPart, ", Score: ", sMedalRankings[iPartCount].iScore)
		iPartCount++
		
	ENDFOR
		
	SORT_MEDAL_RANKINGS_STRUCTS(sMedalRankings, 0, MAX_NUM_MC_PLAYERS - 1)
		
	INT i
	FOR i = 0 TO ci_MEDALRANK_MAX - 1
		iPartMedalRanks[i] = sMedalRankings[i].iPartIndex
		PRINTLN("[Rewards_Rank] - PROCESS_MEDAL_RANKINGS - Rank: ", i," = Participant: ", iPartMedalRanks[i])
	ENDFOR
	
	SET_BIT(iLocalBoolCheck9, LBOOL9_MEDAL_RANKINGS_DONE)
		
ENDPROC

FUNC INT GET_MEDAL_RANKING_FOR_PARTICIPANT(INT iPart)
	
	PROCESS_MEDAL_RANKINGS()
	
	INT i
	FOR i = 0 TO ci_MEDALRANK_BAD - 1
		IF iPartMedalRanks[i] = iPart
			BREAKLOOP
		ENDIF
	ENDFOR
	
	PRINTLN("[Rewards_Rank] - GET_MEDAL_RANKING_FOR_PARTICIPANT - Participant = ", iPart, " = ", i)
	RETURN i

ENDFUNC

FUNC HUD_COLOURS GET_MEDAL_RANKING_HUD_COLOUR(INT iParticipant)
	
	INT iRating = GET_MEDAL_RANKING_FOR_PARTICIPANT(iParticipant)
	
	SWITCH iRating
		CASE ci_MEDALRANK_PLATINUM 	RETURN HUD_COLOUR_PLATINUM
		CASE ci_MEDALRANK_GOLD 		RETURN HUD_COLOUR_GOLD
		CASE ci_MEDALRANK_SILVER 	RETURN HUD_COLOUR_SILVER
		CASE ci_MEDALRANK_BRONZE 	RETURN HUD_COLOUR_BRONZE
		CASE ci_MEDALRANK_BAD 		RETURN HUD_COLOUR_WHITE
	ENDSWITCH
	
	RETURN HUD_COLOUR_BLACK
	
ENDFUNC

FUNC STRING GET_MEDAL_RANKING_STRING_FROM_COLOUR(HUD_COLOURS eHUDColour)

	SWITCH eHUDColour
		CASE HUD_COLOUR_PLATINUM 	RETURN "CELEB_PLATINUM"
		CASE HUD_COLOUR_GOLD 		RETURN "CELEB_GOLD" 
		CASE HUD_COLOUR_SILVER 		RETURN "CELEB_SILVER"
		CASE HUD_COLOUR_BRONZE 		RETURN "CELEB_BRONZE" 
	ENDSWITCH
	
	RETURN "CELEB_NONE" 
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cash Rewards
// ##### Description: Functions for calculating cash rewards
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Sub-Section Name: Contact Missions
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT CALCULATE_TRACKED_VEHICLE_DAMAGE()

	INT i = 0
	INT TOTAL_TRACKED_VEHICLES = 0
	FLOAT fhealth
	INT iReturn
	VEHICLE_INDEX vehPercentage
	FLOAT fPercentage
	
	FOR i = 0 TO (FMMC_MAX_VEHICLES - 1)
		IF IS_BIT_SET(G_FMMC_STRUCT.iHeistCSVehDmgTrackBitSet, i)
			IF IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
				vehPercentage = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
				
				fPercentage = MC_GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(vehPercentage, i, GET_TOTAL_STARTING_PLAYERS())
				
				fhealth += fPercentage
				
				PRINTLN("[Rewards_Cash] - CALCULATE_TRACKED_VEHICLE_DAMAGE iveh = ", i ,"   fhealth   " , fhealth)
				TOTAL_TRACKED_VEHICLES++
			ENDIF
		ENDIF 
	ENDFOR 
	
	iReturn = ROUND(fhealth / TOTAL_TRACKED_VEHICLES)
	
	PRINTLN("[Rewards_Cash] - CALCULATE_TRACKED_VEHICLE_DAMAGE = ", iReturn )
	RETURN iReturn
	
ENDFUNC

FUNC INT CALCULATE_CONTACT_MISSION_CASH_REWARD(BOOL bPass, INT iTotalMissionEndTime)

	INT iCashReturn
	FLOAT fCashReward
	FLOAT fPlayerMulti = 1.0
	
	IF bPass
		IF IS_THIS_A_CONTACT_MISSION()
		AND Is_This_Contact_Mission_First_Play()
			Set_Contact_Mission_First_Play_Full_Cash_Given()
        ENDIF
	ENDIF
	
	IF HAS_LOCAL_PLAYER_FAILED()
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_BLOCK_REWARDS_ON_FAIL)
		iCashReturn = 0
		PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - Block Rewards On Player Fail is set, returning 0 as cash reward")
		RETURN iCashReturn
	ENDIF

	INT iTotalPlayers = MC_serverBD.iNumberOfLBPlayers[0] + MC_serverBD.iNumberOfLBPlayers[1]+ MC_serverBD.iNumberOfLBPlayers[2]+ MC_serverBD.iNumberOfLBPlayers[3]
	IF iTotalPlayers = 1
		fPlayerMulti = g_sMPTunables.fContact_Mission_Cash_Player_Multiplier_1         
	ELIF iTotalPlayers = 2
		fPlayerMulti = g_sMPTunables.fContact_Mission_Cash_Player_Multiplier_2 
	ELIF iTotalPlayers = 3
		fPlayerMulti = g_sMPTunables.fContact_Mission_Cash_Player_Multiplier_3 
	ELSE
		fPlayerMulti = g_sMPTunables.fContact_Mission_Cash_Player_Multiplier_4 
	ENDIF
	PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - fPlayerMulti: ",fPlayerMulti)
	FLOAT fBaseMulti = g_sMPTunables.fContact_Mission_Cash_Base_Multiplier     
	PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - fBaseMulti: ",fBaseMulti)

	INT iMissionRank = g_FMMC_STRUCT.iRank
	PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - mission rank: ",iMissionRank)
	
	IF iMissionRank > g_sMPTunables.iContact_Mission_Cash_Rank_Cap 
		iMissionRank = g_sMPTunables.iContact_Mission_Cash_Rank_Cap 
		PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - mission rank cap: ",iMissionRank)
	ENDIF

	iMissionRank = iMissionRank+g_sMPTunables.iContact_Mission_Cash_Basic_Value
	
	PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - modded mission rank: ",iMissionRank)
	
	FLOAT fDiffMulti = 1.0
	IF g_FMMC_STRUCT.iDifficulity = DIFF_EASY
		fDiffMulti = g_sMPTunables.fContact_Mission_Cash_Difficulty_Multiplier_Easy  
	ELIF g_FMMC_STRUCT.iDifficulity = DIFF_NORMAL
		fDiffMulti = g_sMPTunables.fContact_Mission_Cash_Difficulty_Multiplier_Normal 
	ELSE
		fDiffMulti = g_sMPTunables.fContact_Mission_Cash_Difficulty_Multiplier_Hard 
	ENDIF
	PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - iDifficulity multi: ",fDiffMulti)
	
	PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - iTotalMissionEndTime used for fTimeMulti: ",iTotalMissionEndTime)
	
	FLOAT fTimeMulti = 1.0
	IF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_1*60000)		
		fTimeMulti = g_sMPTunables.fContact_Mission_Cash_Time_Period_1_Percentage    
	ELIF iTotalMissionEndTime<= (g_sMPTunables.iContact_Mission_Time_Period_2*60000)	
		fTimeMulti = g_sMPTunables.fContact_Mission_Cash_Time_Period_2_Percentage  
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_3*60000)	
		fTimeMulti = g_sMPTunables.fContact_Mission_Cash_Time_Period_3_Percentage  
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_4*60000)	
		fTimeMulti = g_sMPTunables.fContact_Mission_Cash_Time_Period_4_Percentage  
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_5*60000)	
		fTimeMulti = g_sMPTunables.fContact_Mission_Cash_Time_Period_5_Percentage  
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_6*60000)	
		fTimeMulti = g_sMPTunables.fContact_Mission_Cash_Time_Period_6_Percentage  
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_7*60000)	
		fTimeMulti = g_sMPTunables.fContact_Mission_Cash_Time_Period_7_Percentage  
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_8*60000)	
		fTimeMulti = g_sMPTunables.fContact_Mission_Cash_Time_Period_8_Percentage  
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_9*60000)	
		fTimeMulti = g_sMPTunables.fContact_Mission_Cash_Time_Period_9_Percentage  
	ELSE
		fTimeMulti = g_sMPTunables.fContact_Mission_Cash_Time_Period_10_Percentage  
	ENDIF
	
	PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - fTimeMulti : ",fTimeMulti)
	
	fCashReward = fPlayerMulti*fBaseMulti*iMissionRank*fDiffMulti*fTimeMulti
	
	
	IF NOT bPass
		
		IF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_1*60000)	
		
			// added to fix 1930038 KW 18/7/2014
			iCashReturn = 200
			fCashReward = 200
			IF iCashReturn > g_sMPTunables.iContact_Mission_Fail_Cash_Minimum 
				iCashReturn =g_sMPTunables.iContact_Mission_Fail_Cash_Minimum 
				fCashReward = TO_FLOAT(g_sMPTunables.iContact_Mission_Fail_Cash_Minimum )
			ENDIF
			
			
			PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - LOSE contact setting cash reward to 200 since under 1 mins: ",fCashReward)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_2*60000)	
			fCashReward = fCashReward/g_sMPTunables.fContact_Mission_Fail_Cash_Time_Period_2_Divider  
			PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - LOSE contact setting cash reward since under 2 mins to: ",fCashReward)
		ELIF iTotalMissionEndTime<= (g_sMPTunables.iContact_Mission_Time_Period_3*60000)	
			fCashReward = fCashReward/g_sMPTunables.fContact_Mission_Fail_Cash_Time_Period_3_Divider  
			PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - LOSE contact setting cash reward since under 3 mins to: ",fCashReward)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_4*60000)	
			fCashReward = fCashReward/g_sMPTunables.fContact_Mission_Fail_Cash_Time_Period_4_Divider  
			PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - LOSE contact setting cash reward since under 4 mins to: ",fCashReward)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_5*60000)	
			fCashReward = fCashReward/g_sMPTunables.fContact_Mission_Fail_Cash_Time_Period_5_Divider  
			PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - LOSE contact setting cash reward since under 6 mins to: ",fCashReward)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_6*60000)	
			fCashReward = fCashReward/g_sMPTunables.fContact_Mission_Fail_Cash_Time_Period_6_Divider  
			PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - LOSE contact setting cash reward since under 8 mins to: ",fCashReward)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_7*60000)		
			fCashReward = fCashReward/g_sMPTunables.fContact_Mission_Fail_Cash_Time_Period_7_Divider  
			PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - LOSE contact setting cash reward since under 10 mins to: ",fCashReward)	
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_8*60000)	
			fCashReward = fCashReward/g_sMPTunables.fContact_Mission_Fail_Cash_Time_Period_8_Divider  
			PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - LOSE contact setting cash reward since under 12 mins to: ",fCashReward)							
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_9*60000)		
			fCashReward = fCashReward/g_sMPTunables.fContact_Mission_Fail_Cash_Time_Period_9_Divider  
			PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - LOSE contact setting cash reward since under 15 mins to: ",fCashReward)						
		ELSE
			fCashReward = fCashReward/g_sMPTunables.fContact_Mission_Fail_Cash_Time_Period_10_Divider  
			PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - LOSE contact setting cash reward since over 15 mins to: ",fCashReward)
		ENDIF
	ENDIF
		
	iCashReturn = ROUND(fCashReward)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_PENALISE_FOR_DMG)
	
		//Reduced Reward= 'Normal Cash Reward' * (1-((100-('Vehicle Health Percentage'))/2)/100)
		//g_sMPTunables.ilow_flow_damage_reward_reduction_divider			= 2
		//g_sMPTunables.ilow_flow_damage_reward_reduction_max_threshold 	= 90
		//g_sMPTunables.ilow_flow_damage_reward_reduction_min_threshold 	= 50
	
		PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - ciMISSION_PENALISE_FOR_DM is turned on")
	
		INT iAvgPercentageVehHealth = CALCULATE_TRACKED_VEHICLE_DAMAGE()
		
		PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - ciMISSION_PENALISE_FOR_DM iDamageCashReduction: ",iAvgPercentageVehHealth)
		
		IF iAvgPercentageVehHealth < g_sMPTunables.ilow_flow_damage_reward_reduction_max_threshold

			//Cap this value to 100 if we have somehow gone over it.
			IF iAvgPercentageVehHealth > 100
				iAvgPercentageVehHealth = 100
				PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - ciMISSION_PENALISE_FOR_DM is turned on -  Average health above 100? Capping to 100.")
			ENDIF
			
			//If vehicle health very low cap it to the min threshold.
			IF iAvgPercentageVehHealth < g_sMPTunables.ilow_flow_damage_reward_reduction_min_threshold 
				iAvgPercentageVehHealth = g_sMPTunables.ilow_flow_damage_reward_reduction_min_threshold 
				PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - ciMISSION_PENALISE_FOR_DM is turned on -  Capping to min threshold.")
			ENDIF	
			
			PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - Unmodified iCashReturn",iCashReturn)
			
			//(1-(((100-iAvgPercentageVehHealth)/g_sMPTunables.ilow_flow_damage_reward_reduction_divider)/100))
			
			FLOAT fVeh1 = TO_FLOAT(100-iAvgPercentageVehHealth)
			PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - fVeh1", fVeh1)
			
			FLOAT fTune = TO_FLOAT(g_sMPTunables.ilow_flow_damage_reward_reduction_divider)
			PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - fTune", fTune)
			
			FLOAT fVeh2 = fVeh1/fTune
			PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - fVeh2", fVeh2)
			
			FLOAT fVeh3 = fVeh2 / 100.0
			PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - fVeh3", fVeh3)
			
			FLOAT fMulti = 1- fVeh3
			PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - fMulti", fMulti)
			
			FLOAT fCalculatedCash = iCashReturn * fMulti
			PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - fCalculatedCash", fCalculatedCash)
			
			iCashReturn= ROUND(fCalculatedCash)
			
		ENDIF

	ENDIF
	
	IF iCashReturn < g_sMPTunables.iContact_Mission_Fail_Cash_Minimum 
		iCashReturn = g_sMPTunables.iContact_Mission_Fail_Cash_Minimum 
		PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - iCashReturn setting min value : ",iCashReturn)
	ENDIF
	
	PRINTLN("[Rewards_Cash] - CALCULATE_CONTACT_MISSION_CASH_REWARD - iCashReturn : ",iCashReturn)
	RETURN iCashReturn
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Sub-Section Name: Versus Missions
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL ARE_WE_ON_WINNING_ROUNDS_TEAM()
	INT iLoop
	FOR iLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
		IF GlobalplayerBD_FM_2[iLoop].sJobRoundData.iRoundScore > g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds
			IF MC_playerBD[iPartToUse].iTeam = MC_playerBD[iLoop].iTeam
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_USE_CLOUD_OVERRIDE()

	IF g_sMPTunables.boverwritecloudvsmissionawards
		PRINTLN("[Rewards_Cash] - SHOULD_USE_CLOUD_OVERRIDE - using cloud override ")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_VS_TIME_BELOW_MIN_REWARD_THRESHOLD(INT iTotalMissionEndTime)
	
	IF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_1*60000)
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC INT CALCULATE_VS_CASH_REWARD(BOOL bPass,INT iTeamPos, INT iTotalMissionEndTime)

	INT iCashGained
	FLOAT fCashGained

	IF HAS_LOCAL_PLAYER_FAILED()
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_BLOCK_REWARDS_ON_FAIL)
		iCashGained = 0
		PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - Block Rewards On Player Fail is set, returning 0 as cash reward")
		RETURN iCashGained
	ENDIF
	
	iCashGained = GET_CASH_VALUE_FROM_CREATOR(g_FMMC_STRUCT.iCashReward)
	fCashGained = TO_FLOAT(iCashGained)
	PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - Base from cloud: ",fCashGained)

	IF SHOULD_USE_CLOUD_OVERRIDE()
		iCashGained = 20000
		fCashGained = TO_FLOAT(iCashGained)
		PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - SHOULD_USE_CLOUD_OVERRIDE: 20000 ")
	ENDIF
	
	IF bPass
		
		IF IS_VS_TIME_BELOW_MIN_REWARD_THRESHOLD(iTotalMissionEndTime)
			PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - set to 200 since under min time: ")
			iCashGained = g_sMPTunables.iVERSUS_WINNER_MINIMUM_CASH 	
			fCashGained = TO_FLOAT(iCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_2*60000)
			fCashGained = fCashGained/g_sMPTunables.fVersus_Winner_Cash_divider_time_1
			PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - reduced since under 3 mins: ",fCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_3*60000)
			fCashGained = fCashGained/g_sMPTunables.fVersus_Winner_Cash_divider_time_2
			PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - reduced since under 5 mins: ",fCashGained)
		ELIF iTotalMissionEndTime <=  (g_sMPTunables.fVersus_Mission_time_4*60000)
			fCashGained = fCashGained/g_sMPTunables.fVersus_Winner_Cash_divider_time_3
			PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - reduced since under 7.5 mins: ",fCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_5*60000)
			fCashGained = fCashGained/g_sMPTunables.fVersus_Winner_Cash_divider_time_4
			PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - reduced since under 10 mins: ",fCashGained)	
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_6*60000)	
			fCashGained = fCashGained/g_sMPTunables.fVersus_Winner_Cash_divider_time_5
			PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - reduced since under 15 mins: ",fCashGained)	
		ELSE
			fCashGained = fCashGained/g_sMPTunables.fVersus_Winner_Cash_divider_time_6
			PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - reduced since over 15 mins: ",fCashGained)
		ENDIF
		iCashGained = ROUND(fCashGained)
		IF ARE_WE_ON_WINNING_ROUNDS_TEAM()
			IF NOT g_sMPTunables.bADVERSARY_MATCH_BONUS_DISABLE
				FLOAT fTimeScratch = TO_FLOAT(g_iRoundsCombinedTime/1000)
				fTimeScratch = fTimeScratch/60
				INT iTimeToCalc = FLOOR(fTimeScratch)
				IF iTimeToCalc > g_sMPTunables.iADVERSARY_MATCH_BONUS_TIME_CAP
					iTimeToCalc = g_sMPTunables.iADVERSARY_MATCH_BONUS_TIME_CAP
				ENDIF
				g_iCachedMatchBonus = iTimeToCalc*g_sMPTunables.iADVERSARY_MATCH_BONUS_TIME_MULTIPLIER
				iCashGained += g_iCachedMatchBonus
				PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - g_iRoundsCombinedTime - match bonus ", g_iRoundsCombinedTime)
				PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - g_iCachedMatchBonus - match bonus ", g_iCachedMatchBonus)
			ELSE
				PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - bonus disabled")
			ENDIF
		ELSE
			PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - not the end of the round")
		ENDIF
	ELSE
		IF IS_VS_TIME_BELOW_MIN_REWARD_THRESHOLD(iTotalMissionEndTime)
			iCashGained = 0 	
			fCashGained = TO_FLOAT(iCashGained)
			PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - setting cash reward since under 30 secs: ",fCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_2*60000)
			IF iTeamPos = 2 OR ALL_TEAMS_FAILED() 
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_1_cash_divider_time_1	
			ELIF iTeamPos = 3
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_2_cash_divider_time_1
			ELSE
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_3_cash_divider_time_1
			ENDIF
			PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - setting cash reward to: ",fCashGained, " since under 3 mins ")	
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_3*60000)
			IF iTeamPos = 2 OR ALL_TEAMS_FAILED()
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_1_cash_divider_time_2
			ELIF iTeamPos = 3
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_2_cash_divider_time_2
			ELSE
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_3_cash_divider_time_2
			ENDIF
			PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - setting cash reward to: ",fCashGained, " since under 5 mins ")	
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_4*60000)
			IF iTeamPos = 2 OR ALL_TEAMS_FAILED()
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_1_cash_divider_time_3
			ELIF iTeamPos = 3
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_2_cash_divider_time_3
			ELSE
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_3_cash_divider_time_3
			ENDIF
			PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - setting cash reward to: ",fCashGained, " since under 7.5 mins ")	
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_5*60000)
			IF iTeamPos = 2 OR ALL_TEAMS_FAILED()
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_1_cash_divider_time_4
			ELIF iTeamPos = 3
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_2_cash_divider_time_4
			ELSE
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_3_cash_divider_time_4
			ENDIF
			PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - setting cash reward to: ",fCashGained, " since under 10 mins ")
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_6*60000)
			IF iTeamPos = 2 OR ALL_TEAMS_FAILED()
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_1_cash_divider_time_5
			ELIF iTeamPos = 3
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_2_cash_divider_time_5
			ELSE
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_3_cash_divider_time_5
			ENDIF
			PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - setting cash reward to: ",fCashGained, " since under 15 mins ")
		ELSE
			IF iTeamPos = 2 OR ALL_TEAMS_FAILED()
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_1_cash_divider_time_6
			ELIF iTeamPos = 3
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_2_cash_divider_time_6
			ELSE
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_3_cash_divider_time_6
			ENDIF
			PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - setting cash reward to: ",fCashGained, " since over 15 mins ")
		ENDIF
		iCashGained = ROUND(fCashGained)
		IF ARE_WE_ON_WINNING_ROUNDS_TEAM()
			IF NOT g_sMPTunables.bADVERSARY_MATCH_BONUS_DISABLE
				PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - g_iRoundsCombinedTime - start ", g_iRoundsCombinedTime)
				FLOAT fTimeScratch = TO_FLOAT(g_iRoundsCombinedTime/1000)
				fTimeScratch = fTimeScratch/60
				INT iTimeToCalc = FLOOR(fTimeScratch)
				IF iTimeToCalc > g_sMPTunables.iADVERSARY_MATCH_BONUS_TIME_CAP
					iTimeToCalc = g_sMPTunables.iADVERSARY_MATCH_BONUS_TIME_CAP
				ENDIF
				g_iCachedMatchBonus = iTimeToCalc*g_sMPTunables.iADVERSARY_MATCH_BONUS_TIME_MULTIPLIER
				iCashGained += g_iCachedMatchBonus
				PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - g_iCachedMatchBonus - match bonus ", g_iCachedMatchBonus)
			ELSE
				PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - bonus disabled")
			ENDIF
		ELSE
			PRINTLN("[Rewards_Cash] -CALCULATE_VS_CASH_REWARD - not the end of the round")
		ENDIF
		PRINTLN("[Rewards_Cash] - CALCULATE_VS_CASH_REWARD - rounded cash reward returned: ",iCashGained)
	ENDIF
	
	RETURN iCashGained
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Sub-Section Name: Final Cash Reward
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL DOES_CONTENT_SKIP_REWARD_ROUNDING()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_FINAL_CASH_REWARD(BOOL bPass, INT iTeamPos, INT iTotalMissionEndTime)

	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		PRINTLN("[Rewards_Cash] - GET_FINAL_CASH_REWARD - Test Mode - Returning $0")
		RETURN 0
	ENDIF
	
	PRINTLN("[Rewards_Cash] - GET_FINAL_CASH_REWARD - Passed: ", BOOL_TO_STRING(bPass), ", Team Pos: ", iTeamPos, ", Total Mission Time: ", iTotalMissionEndTime)

	INT iCashGained = 0

	IF g_bVSMission
		iCashGained = CALCULATE_VS_CASH_REWARD(bPass, iTeamPos, iTotalMissionEndTime)
		PRINTLN("[Rewards_Cash] - GET_FINAL_CASH_REWARD - Versus Mission : $", iCashGained)
	ELIF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		iCashGained = CALCULATE_ISLAND_HEIST_CASH_REWARD(bPass, GET_TOTAL_CASH_GRAB_TAKE(), iTotalMissionEndTime)
		PRINTLN("[Rewards_Cash] - GET_FINAL_CASH_REWARD - Island Heist Finale: $", iCashGained)
	ELIF IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_FMMC_STRUCT.iRootContentIDHash)
		iCashGained = CALCULATE_TUNER_ROBBERY_REWARD(bPass)
		PRINTLN("[Rewards_Cash] - GET_FINAL_CASH_REWARD - Tuner Robbery: $", iCashGained)
	ELIF GET_FIXER_STORY_MISSION_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash) = FSM_FINALE
		iCashGained = CALCULATE_FIXER_FINALE_REWARD(bPass)
		PRINTLN("[Rewards_Cash] - GET_FINAL_CASH_REWARD - Fixer Finale: $", iCashGained)
	ELIF IS_THIS_A_FIXER_SHORT_TRIP()
		iCashGained = CALCULATE_SHORT_TRIPS_REWARD(bPass)
		PRINTLN("[Rewards_Cash] - GET_FINAL_CASH_REWARD - Short Trip: $", iCashGained)
	ELSE
		iCashGained = CALCULATE_CONTACT_MISSION_CASH_REWARD(bPass, iTotalMissionEndTime)
		PRINTLN("[Rewards_Cash] - GET_FINAL_CASH_REWARD - Contact Mission: $", iCashGained)
	ENDIF

	iCashGained = MULTIPLY_CASH_BY_TUNABLE(iCashGained) 
	
	IF g_sMPTunables.fearnings_High_Rockstar_Missions_modifier != 0.0
	AND iCashGained > g_sMPTunables.fearnings_High_Rockstar_Missions_modifier
		iCashGained = ROUND(g_sMPTunables.fearnings_High_Rockstar_Missions_modifier)
		PRINTLN("[Rewards_Cash] - GET_FINAL_CASH_REWARD - g_sMPTunables.fearnings_High_Rockstar_Missions_modifier is set capping cash to: ", iCashGained)
	ENDIF
	
	IF g_sMPTunables.fearnings_Low_Rockstar_Missions_modifier != 0.0
	AND iCashGained < g_sMPTunables.fearnings_Low_Rockstar_Missions_modifier
		iCashGained = ROUND(g_sMPTunables.fearnings_Low_Rockstar_Missions_modifier)
		PRINTLN("[Rewards_Cash] - GET_FINAL_CASH_REWARD - g_sMPTunables.fearnings_Low_Rockstar_Missions_modifier is set capping cash to: ", iCashGained)
	ENDIF
	PRINTLN("[Rewards_Cash] - GET_FINAL_CASH_REWARD - Value after tunable scaling: $", iCashGained)
	
	IF NOT DOES_CONTENT_SKIP_REWARD_ROUNDING()
		iCashGained = ROUND(TO_FLOAT(iCashGained) / ciROUND_CASH_TO) * ciROUND_CASH_TO
		PRINTLN("[Rewards_Cash] - GET_FINAL_CASH_REWARD - Value after rounding: $", iCashGained)
	ENDIF
	
	iCashGainedForCelebration = iCashGained
	
	#IF IS_DEBUG_BUILD
	iDebugRewardCash = iCashGained
	#ENDIF
	
	RETURN iCashGained

ENDFUNC

PROC GIVE_END_OF_MISSION_CASH_REWARD(INT iCashEarned)
	
	IF iCashEarned <= 0
		EXIT
	ENDIF
	
	GIVE_LOCAL_PLAYER_FM_CASH_END_OF_MISSION(iCashEarned) //This seems to just be the ticker - doesn't actually add cash...
	ADD_TO_JOB_CASH_EARNINGS(iCashEarned)
			
	INT iCachedMatchBonus = ROUND(TO_FLOAT(g_iCachedMatchBonus) * g_sMPTunables.cashMultiplier)
	
	PRINTLN("[Rewards_Cash] - GIVE_END_OF_MISSION_CASH_REWARD - Cash Earned: $", iCashEarned, ", Cached Match Bonus: $", iCachedMatchBonus, ", PlayerBD Cash Reward (Before): $", MC_playerBD[iLocalPart].iCashReward)
	
	INT iCachedReward = MC_playerBD[iLocalPart].iCashReward + iCashEarned
	MC_playerBD[iLocalPart].iCashReward = iCachedReward - iCachedMatchBonus
		
	IF MC_playerBD[iPartToUse].iCashReward <= 0
		PRINTLN("[Rewards_Cash] - GIVE_END_OF_MISSION_CASH_REWARD - No cash to give.")
		EXIT
	ENDIF
	
	PRINTLN("[Rewards_Cash] - GIVE_END_OF_MISSION_CASH_REWARD - iCashReward = ", MC_playerBD[iLocalPart].iCashReward, "  g_FMMC_STRUCT.tl31LoadedContentID = ", g_FMMC_STRUCT.tl31LoadedContentID)
	
	INT iScriptTransactionIndex
		
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		PRINTLN("[Rewards_Cash] - GIVE_END_OF_MISSION_CASH_REWARD - Island Heist EARN")
		PROCESS_GIVING_ISLAND_HEIST_CASH_REWARD(iCachedReward)
		EXIT
	ELIF IS_THIS_A_TUNER_ROBBERY_FINALE()
		PRINTLN("[Rewards_Cash] - GIVE_END_OF_MISSION_CASH_REWARD - Tuner Finale EARN")
		PROCESS_GIVING_TUNER_CONTRACT_CASH_REWARD(iCachedReward)
		EXIT
	ELIF IS_THIS_A_FIXER_STORY_MISSION()
		PRINTLN("[Rewards_Cash] - GIVE_END_OF_MISSION_CASH_REWARD - Fixer Contract EARN")
		PROCESS_GIVING_FIXER_CONTRACT_CASH_REWARD(iCachedReward)
		EXIT
	ELIF IS_THIS_A_FIXER_SHORT_TRIP()
		PRINTLN("[Rewards_Cash] - GIVE_END_OF_MISSION_CASH_REWARD - Fixer Short Trip EARN")
		PROCESS_GIVING_FIXER_SHORT_TRIP_CASH_REWARD(iCachedReward)
		EXIT
	ENDIF
	
	PRINTLN("[Rewards_Cash] - GIVE_END_OF_MISSION_CASH_REWARD - Default EARN")
	IF USE_SERVER_TRANSACTIONS()
		TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_JOBS, iCachedReward, iScriptTransactionIndex, DEFAULT, DEFAULT)
		g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
	ELSE
		NETWORK_EARN_FROM_JOB(iCachedReward, g_FMMC_STRUCT.tl31LoadedContentID)
	ENDIF
	
ENDPROC	

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Sub-Section Name: HUD Only Cash
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_MAIN_TARGET_VALUE_FOR_TAKE_HUD()
	
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		HEIST_ISLAND_PRIMARY_TARGETS eIHTarget = GET_PLAYER_HEIST_ISLAND_PRIMARY_TARGET(GB_GET_LOCAL_PLAYER_GANG_BOSS())
		IF eIHTarget != HIPT_NONE
			RETURN ROUND(GET_HEIST_ISLAND_PRIMARY_TARGET_VALUE(eIHTarget) * GET_HEIST_ISLAND_PRIMARY_TARGET_VALUE_DIFFICULTY_MODIFIER(g_FMMC_STRUCT.iDifficulity))
		ENDIF
	ENDIF
	
	//Add other heist main target values in a similar way
	//heist target type -> valid -> return
	
	RETURN 0
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: RP Rewards
// ##### Description: Functions for calculating cash rewards
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_MISSION_TIME_FOR_REWARDS()

	INT iTotalMissionTime = 0
	
	IF HAS_FIRST_STRAND_MISSION_BEEN_PASSED() OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
		//Strand Mission, use time cached in Continuity Vars
		iTotalMissionTime = MC_serverBD_1.sMissionContinuityVars.iTotalMissionTime
		IF MC_serverBD.iTotalMissionEndTime <= 0
			iTotalMissionTime += GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer)
			PRINTLN("[Rewards] - GET_MISSION_TIME_FOR_REWARDS - Strand Mission - End Timer not cached so using local time for last mission")
		ENDIF
	ELSE
		iTotalMissionTime = MC_serverBD.iTotalMissionEndTime
		IF iTotalMissionTime <= 0
			iTotalMissionTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer)
			PRINTLN("[Rewards] - GET_MISSION_TIME_FOR_REWARDS - End Timer not cached so using local time for last mission")
		ENDIF
	ENDIF
	
	iTotalMissionTime += 10000 //This 10 second bonus has always existed with no explanation... 
	
	PRINTLN("[Rewards] - GET_MISSION_TIME_FOR_REWARDS - Mission Time: ", iTotalMissionTime)
	RETURN iTotalMissionTime
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Sub-Section Name: Contact Missions
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL ALL_TEAM_ONLY_DID_HEADSHOTS(INT iteam)

	INT iparticipant
	PARTICIPANT_INDEX tempPart
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		tempPart = INT_TO_PARTICIPANTINDEX(iParticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			IF MC_PlayerBD[iParticipant].iteam = iteam
				IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_ALL_HEADSHOTS)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
	
ENDFUNC

FUNC INT GIVE_ALL_ENEMY_KILL_XP()

	INT iParticipant
	INT iTotalKills
	INT iped
	INT iTotalxpreward,ixpKillreward,ixpHeadshotreward,ixpTHeadshotreward
	BOOL ballkill = TRUE
	FLOAT fXPBeforeRound

	FOR iped = 0 TO (FMMC_MAX_PEDS-1)
		IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_HATE
				PRINTLN("[Rewards_RP] - GIVE_ALL_ENEMY_KILL_XP - Hostile ped still alive : ",iped," for team : ",MC_PlayerBD[iPartToUse].iteam) 
				ballkill = FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	IF ballkill
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
			IF MC_PlayerBD[iPartToUse].iteam = MC_PlayerBD[iParticipant].iteam
				
				iTotalKills = iTotalKills + MC_PlayerBD[iParticipant].iNumPedKills
				
				IF iTotalKills >= g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP
					iTotalKills = g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP
				ENDIF
				
			ENDIF
		ENDREPEAT

		PRINTLN("[Rewards_RP] - GIVE_ALL_ENEMY_KILL_XP - total kills : ",iTotalKills," for team : ",MC_PlayerBD[iPartToUse].iteam)
		
		IF iTotalKills > 0
			ixpKillreward = iTotalKills*10
			PRINTLN("[Rewards_RP] - GIVE_ALL_ENEMY_KILL_XP - ixpKillreward before tune: ",ixpKillreward) 
			PRINTLN("[Rewards_RP] - GIVE_ALL_ENEMY_KILL_XP - all kills tunable value: ",g_sMPTunables.fxp_tunable_Kill_all_enemies_on_a_mission) 
			fXPBeforeRound = (ixpKillreward*g_sMPTunables.fxp_tunable_Kill_all_enemies_on_a_mission)  
			ixpKillreward = ROUND(fXPBeforeRound)   
			#IF IS_DEBUG_BUILD
				iAllKillXPBonus = ixpKillreward
			#ENDIF
			PRINTLN("[Rewards_RP] - GIVE_ALL_ENEMY_KILL_XP - total kills XP : ",ixpKillreward)
		ENDIF
	ENDIF
	
	IF g_MissionControllerserverBD_LB.sleaderboard[iPartToUse].iKills >= 5
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_ALL_HEADSHOTS)
			INT iLBkills = g_MissionControllerserverBD_LB.sleaderboard[iPartToUse].iKills
			IF iLBkills >= g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP
				iLBkills = g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP
			ENDIF
			ixpHeadshotreward = ixpHeadshotreward + (iLBkills*20)
			PRINTLN("[Rewards_RP] - GIVE_ALL_ENEMY_KILL_XP - giving all headshot player bonus XP: ",ixpHeadshotreward) 
			PRINTLN("[Rewards_RP] - GIVE_ALL_ENEMY_KILL_XP - all headshot individual tunable: ",g_sMPTunables.fxp_tunable_Only_kill_enemies_with_headshots_Individual) 
			fXPBeforeRound = (ixpHeadshotreward*g_sMPTunables.fxp_tunable_Only_kill_enemies_with_headshots_Individual)
			ixpHeadshotreward = ROUND(fXPBeforeRound)
			#IF IS_DEBUG_BUILD
				iAllHeadshotXPPlayer = (ixpHeadshotreward)
			#ENDIF
		ENDIF
		IF ALL_TEAM_ONLY_DID_HEADSHOTS(MC_playerBD[iPartToUse].iteam)
			INT iLBTeamkills = GET_TEAM_KILLS(MC_playerBD[iPartToUse].iTeam)
			IF iLBTeamkills >= g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP
				iLBTeamkills = g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP
			ENDIF
			ixpTHeadshotreward = ixpTHeadshotreward + (iLBTeamkills*10)
			PRINTLN("[Rewards_RP] - GIVE_ALL_ENEMY_KILL_XP - giving all headshot team bonus XP: ",ixpTHeadshotreward) 
			PRINTLN("[Rewards_RP] - GIVE_ALL_ENEMY_KILL_XP - all headshot individual tunable: ",g_sMPTunables.fxp_tunable_Only_kill_enemies_with_headshots_Team) 
			fXPBeforeRound =(ixpTHeadshotreward*g_sMPTunables.fxp_tunable_Only_kill_enemies_with_headshots_Team) 
			ixpTHeadshotreward = ROUND(fXPBeforeRound)
			#IF IS_DEBUG_BUILD
				iAllHeadshotXPTeam = ixpTHeadshotreward
			#ENDIF
		ENDIF
	ENDIF
	iTotalxpreward = (ixpKillreward+ixpHeadshotreward+ixpTHeadshotreward)
	
	PRINTLN("[Rewards_RP] - GIVE_ALL_ENEMY_KILL_XP - iTotalxpreward: ",iTotalxpreward) 
	
	IF iTotalxpreward > 0
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD,"REWXP_MKILLS",XPTYPE_ACTION, XPCATEGORY_ACTION_KILLS, iTotalxpreward)
		ENDIF
	ENDIF
	
	RETURN iTotalxpreward

ENDFUNC

FUNC INT GET_LIVES_XP_BONUS()

	INT ixpAdded
	FLOAT fLivesXP
	
	IF GET_TEAM_DEATHS(MC_playerBD[iPartToUse].iTeam) = 0
		PRINTLN("[Rewards_RP] - GET_LIVES_XP_BONUS - g_sMPTunables.fxp_tunable_Not_losing_any_lives_bonus", g_sMPTunables.fxp_tunable_Not_losing_any_lives_bonus)
		fLivesXP = 200 * g_sMPTunables.fxp_tunable_Not_losing_any_lives_bonus   
		ixpAdded = ROUND(fLivesXP)
		PRINTLN("[Rewards_RP] - GET_LIVES_XP_BONUS - ixpAdded for lives: ",ixpAdded)
	ENDIF

	RETURN ixpAdded
	
ENDFUNC

FUNC FLOAT GET_CONTACT_MISSION_RP(BOOL bPass, INT iTotalMissionEndTime)

	FLOAT fDifficultyMod = 1.0
	FLOAT fBaseMulti = g_sMPTunables.fContact_Mission_RP_Base_Multiplier 
	PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP - RP fBaseMulti: ",fBaseMulti)
	FLOAT fTimeMulti = 0.2
	FLOAT fxpgained
	FLOAT fMissionRank = TO_FLOAT(g_FMMC_STRUCT.iRank)
	
	PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP - RP mission rank: ",fMissionRank)
	fMissionRank= fMissionRank/2
	PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP - RP mission modded rank: ",fMissionRank)
	
	IF fMissionRank > g_sMPTunables.iContact_Mission_RP_Rank_Cap 
		fMissionRank = TO_FLOAT(g_sMPTunables.iContact_Mission_RP_Rank_Cap )
		PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP - RP mission rank cap: ",fMissionRank)
	ENDIF
	fMissionRank = fMissionRank + g_sMPTunables.iContact_Mission_RP_Basic_Value
	IF MC_serverBD.iDifficulty = DIFF_EASY
		fDifficultyMod = g_sMPTunables.fContact_Mission_RP_Difficulty_Multiplier_Easy
		PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP -  easy xp mod: ",fDifficultyMod) 
	ELIF MC_serverBD.iDifficulty = DIFF_HARD
		fDifficultyMod = g_sMPTunables.fContact_Mission_RP_Difficulty_Multiplier_Hard
		PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP -  hard xp mod: ",fDifficultyMod)  
	ELSE
		fDifficultyMod = g_sMPTunables.fContact_Mission_RP_Difficulty_Multiplier_Normal
		PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP -  normal xp mod: ",fDifficultyMod)  
	ENDIF
	
	IF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_1*60000)	
		fTimeMulti =g_sMPTunables.fContact_Mission_RP_Time_Period_1_Percentage
		PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP - contact time multi under 1 mins: ",fTimeMulti)
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_2*60000)	
		fTimeMulti =g_sMPTunables.fContact_Mission_RP_Time_Period_2_Percentage
		PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP - contact time multi under 2 mins: ",fTimeMulti)
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_3*60000)	
		fTimeMulti =g_sMPTunables.fContact_Mission_RP_Time_Period_3_Percentage
		PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP - contact time multi under 3 mins: ",fTimeMulti)
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_4*60000)	
		fTimeMulti =g_sMPTunables.fContact_Mission_RP_Time_Period_4_Percentage
		PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP - contact time multi under 4 mins: ",fTimeMulti)
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_5*60000)	
		fTimeMulti =g_sMPTunables.fContact_Mission_RP_Time_Period_5_Percentage
		PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP - contact time multi under 6 mins: ",fTimeMulti)
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_6*60000)	
		fTimeMulti =g_sMPTunables.fContact_Mission_RP_Time_Period_6_Percentage
		PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP - contact time multi under 8 mins: ",fTimeMulti)
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_7*60000)		
		fTimeMulti =g_sMPTunables.fContact_Mission_RP_Time_Period_7_Percentage
		PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP - contact time multi under 10 mins: ",fTimeMulti)	
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_8*60000)	
		fTimeMulti =g_sMPTunables.fContact_Mission_RP_Time_Period_8_Percentage
		PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP - contact time multi under 12 mins: ",fTimeMulti)							
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_9*60000)		
		fTimeMulti =g_sMPTunables.fContact_Mission_RP_Time_Period_9_Percentage
		PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP - contact time multi under 15 mins: ",fTimeMulti)								
	ELSE
		fTimeMulti =g_sMPTunables.fContact_Mission_RP_Time_Period_10_Percentage
		PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP - contact time multi over 15 mins: ",fTimeMulti)	
	ENDIF
	
	fxpgained = fBaseMulti*fMissionRank*fDifficultyMod*fTimeMulti
	
	PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP - contact fxpgained: ",fxpgained)	
	
	IF NOT bPass
	
		IF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_1*60000)	
			fxpgained = 100.0
			
			IF fxpgained > g_sMPTunables.iContact_Mission_Fail_RP_Minimum
				fxpgained = TO_FLOAT(g_sMPTunables.iContact_Mission_Fail_RP_Minimum)
				PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP -  Fail contact mission RP under 1 mins rounding: ",fxpgained) 
			ENDIF
			
			PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP -  Fail contact mission RP under 1 mins: ",fxpgained) 
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_2*60000)	
			fxpgained = fxpgained/g_sMPTunables.fContact_Mission_Fail_RP_Time_Period_2_Divider    
			PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP -  Fail contact mission RP under 2 mins: ",fxpgained) 
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_3*60000)	
			fxpgained = fxpgained/g_sMPTunables.fContact_Mission_Fail_RP_Time_Period_3_Divider    
			PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP -  Fail contact mission RP under 3 mins: ",fxpgained) 
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_4*60000)	
			fxpgained = fxpgained/g_sMPTunables.fContact_Mission_Fail_RP_Time_Period_4_Divider    
			PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP -  Fail contact mission RP under 4 mins: ",fxpgained) 
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_5*60000)	
			fxpgained = fxpgained/g_sMPTunables.fContact_Mission_Fail_RP_Time_Period_5_Divider    
			PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP -  Fail contact mission RP under 6 mins: ",fxpgained) 
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_6*60000)	
			fxpgained = fxpgained/g_sMPTunables.fContact_Mission_Fail_RP_Time_Period_6_Divider    
			PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP -  Fail contact mission RP under 8 mins: ",fxpgained) 
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_7*60000)		
			fxpgained = fxpgained/g_sMPTunables.fContact_Mission_Fail_RP_Time_Period_7_Divider    
			PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP -  Fail contact mission RP under 10 mins: ",fxpgained) 	
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_8*60000)		
			fxpgained = fxpgained/g_sMPTunables.fContact_Mission_Fail_RP_Time_Period_8_Divider    
			PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP -  Fail contact mission RP under 12 mins: ",fxpgained) 							
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_9*60000)		
			fxpgained = fxpgained/g_sMPTunables.fContact_Mission_Fail_RP_Time_Period_9_Divider    
			PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP -  Fail contact mission RP under 15 mins: ",fxpgained) 						
		ELSE
			fxpgained = fxpgained/g_sMPTunables.fContact_Mission_Fail_RP_Time_Period_10_Divider    
			PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP -  Fail contact mission RP over 15 mins: ",fxpgained) 
		ENDIF

		IF fxpgained < g_sMPTunables.iContact_Mission_Fail_RP_Minimum
			fxpgained = TO_FLOAT(g_sMPTunables.iContact_Mission_Fail_RP_Minimum)
			PRINTLN("[Rewards_RP] - GET_CONTACT_MISSION_RP -  Fail contact mission RP under min: ",fxpgained) 
		ENDIF
	
	
	ENDIF
	
	RETURN fxpgained
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Sub-Section Name: Versus Missions
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC FLOAT GET_VS_MISSION_RP(BOOL bPass,INT iTeamPos,INT iTotalMissionEndTime)

	FLOAT fxpgained =  TO_FLOAT(g_FMMC_STRUCT.iXPReward)
	PRINTLN("[Rewards_RP] - GET_VS_MISSION_RP -  base VS ixpreward: ",g_FMMC_STRUCT.iXPReward) 
	
	IF SHOULD_USE_CLOUD_OVERRIDE()
		fxpgained = 3000
		PRINTLN("[Rewards_RP] - GET_VS_MISSION_RP - USING CLOUD OVERRIDE lose base fxpgained: ",fxpgained) 
	ENDIF

	IF bPass
		IF IS_VS_TIME_BELOW_MIN_REWARD_THRESHOLD(iTotalMissionEndTime)
			fxpgained = 100
			PRINTLN("[Rewards_RP] - GET_VS_MISSION_RP - VS mission ended too quickly setting XP to 100")
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_2*60000)	
			fxpgained = fxpgained/g_sMPTunables.fVersus_Winner_RP_divider_time_1
			IF fxpgained < 100
				fxpgained = 100
			ENDIF
			PRINTLN("[Rewards_RP] - GET_VS_MISSION_RP - PASS VS mission under 3 mins setting XP to: ",fxpgained)	
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_3*60000)
			fxpgained = fxpgained/g_sMPTunables.fVersus_Winner_RP_divider_time_2	
			IF fxpgained < 100
				fxpgained = 100
			ENDIF			
			PRINTLN("[Rewards_RP] - GET_VS_MISSION_RP - PASS VS mission under 5 mins setting XP to: ",fxpgained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_4*60000)
			fxpgained = fxpgained/g_sMPTunables.fVersus_Winner_RP_divider_time_3
			IF fxpgained < 100
				fxpgained = 100
			ENDIF
			PRINTLN("[Rewards_RP] - GET_VS_MISSION_RP - PASS VS mission under 7.5 mins setting XP to: ",fxpgained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_5*60000)
			fxpgained = fxpgained/g_sMPTunables.fVersus_Winner_RP_divider_time_4
			IF fxpgained < 100
				fxpgained = 100
			ENDIF			
			PRINTLN("[Rewards_RP] - GET_VS_MISSION_RP - PASS VS mission under 10 mins setting XP to: ",fxpgained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_6*60000)
			fxpgained = fxpgained/g_sMPTunables.fVersus_Winner_RP_divider_time_5
			IF fxpgained < 200
				fxpgained = 200
			ENDIF	
			PRINTLN("[Rewards_RP] - GET_VS_MISSION_RP - PASS VS mission under 15 mins setting XP to: ",fxpgained)	
		
		ELSE
			fxpgained = fxpgained/g_sMPTunables.fVersus_Winner_RP_divider_time_6
			IF fxpgained < 300
				fxpgained = 300
			ENDIF	
			PRINTLN("[Rewards_RP] - GET_VS_MISSION_RP - PASS VS mission over 15 mins setting XP to: ",fxpgained)	
		ENDIF

	ELSE
		IF IS_VS_TIME_BELOW_MIN_REWARD_THRESHOLD(iTotalMissionEndTime)
			fxpgained = 100
			PRINTLN("[Rewards_RP] - GET_VS_MISSION_RP -  Fail VS mission RP under 10 secs: ",fxpgained) 
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_2*60000)
			IF iteampos = 2 OR ALL_TEAMS_FAILED()
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_1_RP_divider_time_1
			ELIF iteampos = 3
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_2_RP_divider_time_1	
			ELSE
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_3_RP_divider_time_1
			ENDIF	
			IF fxpgained < 100
				fxpgained = 100
			ENDIF

			PRINTLN("[Rewards_RP] - GET_VS_MISSION_RP -  Fail VS mission RP under 3 mins: ",fxpgained) 
			
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_3*60000)
			IF iteampos = 2 OR ALL_TEAMS_FAILED()
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_1_RP_divider_time_2
			ELIF iteampos = 3
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_2_RP_divider_time_2
			ELSE
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_3_RP_divider_time_2
			ENDIF	
			IF fxpgained < 100
				fxpgained = 100
			ENDIF
			PRINTLN("[Rewards_RP] - GET_VS_MISSION_RP -  Fail VS mission RP under 5 mins: ",fxpgained) 
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_4*60000)
			IF iteampos = 2 OR ALL_TEAMS_FAILED()
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_1_RP_divider_time_3	
			ELIF iteampos = 3
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_2_RP_divider_time_3
			ELSE
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_3_RP_divider_time_3
			ENDIF	
			IF fxpgained < 100
				fxpgained = 100
			ENDIF
			PRINTLN("[Rewards_RP] - GET_VS_MISSION_RP -  Fail VS mission RP under 7.5 mins: ",fxpgained) 
		
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_5*60000)
			IF iteampos = 2 OR ALL_TEAMS_FAILED()
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_1_RP_divider_time_4
			ELIF iteampos = 3
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_2_RP_divider_time_4
			ELSE
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_3_RP_divider_time_4
			ENDIF	
			IF fxpgained < 100
				fxpgained = 100
			ENDIF
			PRINTLN("[Rewards_RP] - GET_VS_MISSION_RP -  Fail VS mission RP under 10 mins: ",fxpgained) 
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_6*60000)
			IF iteampos = 2 OR ALL_TEAMS_FAILED()
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_1_RP_divider_time_5	
			ELIF iteampos = 3
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_2_RP_divider_time_5
			ELSE
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_3_RP_divider_time_5
			ENDIF	
			IF fxpgained < 200
				fxpgained = 200
			ENDIF
			PRINTLN("[Rewards_RP] - GET_VS_MISSION_RP -  Fail VS mission RP under 15 mins: ",fxpgained) 
		ELSE
			IF iteampos = 2 OR ALL_TEAMS_FAILED()
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_1_RP_divider_time_6
			ELIF iteampos = 3
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_2_RP_divider_time_6
			ELSE
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_3_RP_divider_time_6
			ENDIF	
			IF fxpgained < 300
				fxpgained = 300
			ENDIF
			PRINTLN("[Rewards_RP] - GET_VS_MISSION_RP -  Fail VS mission RP Over 15 mins: ",fxpgained) 
		ENDIF
	ENDIF
	
	RETURN fxpgained
	
ENDFUNC

PROC AWARD_EXTRA_MISSION_RP(BOOL bPass)

	IF iDelayedDeliveryXPToAward > 0
		FLOAT fdeliveryXP =(iDelayedDeliveryXPToAward*100*g_sMPTunables.fxp_tunable_Deliver_a_package_bonus)
		INT ideliveryXP = ROUND(fdeliveryXP)  
		iXPExtraRewardGained = iXPExtraRewardGained+ideliveryXP
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD,"XPT_MEDIUMT", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_MISSION_DELIVERED_VEHICLE,  ideliveryXP,1)
		ENDIF
		#IF IS_DEBUG_BUILD
			iDebugXPDelVeh = iDebugXPDelVeh + ideliveryXP
		#ENDIF
		PRINTLN("[Rewards_RP] - AWARD_EXTRA_MISSION_RP - iDelayedDeliveryXPToAward ",ideliveryXP)
	ENDIF
	
	IF bPass
		IF NOT IS_THIS_A_VERSUS_MISSION()
			INT iLivesBonus = GET_LIVES_XP_BONUS()
			iXPExtraRewardGained = iXPExtraRewardGained +iLivesBonus
			PRINTLN("[Rewards_RP] - AWARD_EXTRA_MISSION_RP - adding lives bonus of: iLivesBonus ",iLivesBonus)
			#IF IS_DEBUG_BUILD
				iDebugXPLivesBonus = ilivesbonus
			#ENDIF
			IF iLivesBonus > 0
				IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
					GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD,"XPT_MEDIUMT", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_MISSION_PASS,  ilivesbonus,1)
				ENDIF
			ENDIF
			
			INT iKillBonus = GIVE_ALL_ENEMY_KILL_XP()
			iXPExtraRewardGained = iXPExtraRewardGained + iKillBonus
			PRINTLN("[Rewards_RP] - AWARD_EXTRA_MISSION_RP - adding kill bonus of: iKillBonus ",iKillBonus)
			IF g_bVSMission
				IF MC_serverBD.iTotalMissionEndTime <= ciMIN_REWARD_TIME
					iXPExtraRewardGained = 0
					PRINTLN("[Rewards_RP] - AWARD_EXTRA_MISSION_RP - VS mission ended too quickly setting lives and kill XP to 0")
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
		
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Sub-Section Name: Final RP Reward
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_FINAL_RP_REWARD(BOOL bPass, INT iTeamPos, INT iTotalMissionEndTime)
	
	PRINTLN("[Rewards_RP] - GET_FINAL_RP_REWARD - Passed: ", BOOL_TO_STRING(bPass), ", Team Pos: ", iTeamPos, ", Total Mission Time: ", iTotalMissionEndTime)
	
	FLOAT fRPGained
	
	IF g_bVSMission
		fRPGained = GET_VS_MISSION_RP(bPass, iTeamPos, iTotalMissionEndTime)
	ELIF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		INT iMedal = GET_MEDAL_RANKING_FOR_PARTICIPANT(iLocalPart)
		fRPGained = CALCULATE_ISLAND_HEIST_RP(bPass, iTotalMissionEndTime, iMedal)
	ELSE
		fRPGained = GET_CONTACT_MISSION_RP(bPass, iTotalMissionEndTime)
	ENDIF
	
	IF fRPGained < 0.0
		fRPGained = 0.0
	ENDIF
	
	PRINTLN("[Rewards_RP] - GET_FINAL_RP_REWARD - Final RP value: ", ROUND(fRPGained)) 
		
	RETURN ROUND(fRPGained)

ENDFUNC	

PROC GIVE_END_OF_MISSION_RP_REWARD(INT iRPReward, BOOL bPass)

	IF iRPReward <= 0
		PRINTLN("[Rewards_RP] - GIVE_END_OF_MISSION_RP_REWARD - No RP to give.")
	ENDIF

	XPCATEGORY eCategory
	IF bPass
		PRINTLN("[Rewards_RP] - GIVE_END_OF_MISSION_RP_REWARD - Mission Passed, Category: XPCATEGORY_COMPLETE_MISSION_PASS.")
		ROUND_MAX_XP_AMOUNT_PER_JOB(iRPReward)
		eCategory = XPCATEGORY_COMPLETE_MISSION_PASS
	ELSE
		PRINTLN("[Rewards_RP] - GIVE_END_OF_MISSION_RP_REWARD - Mission Failed, Category: XPCATEGORY_COMPLETE_MISSION_FAIL.")
		eCategory = XPCATEGORY_COMPLETE_MISSION_FAIL
	ENDIF
	
	PRINTLN("[Rewards_RP] - GIVE_END_OF_MISSION_RP_REWARD - Attempting to give RP: ", iRPReward)
	
	iRPReward = GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_JOBFM, "REWXP_MISS", XPTYPE_COMPLETE, eCategory, iRPReward, 1, -1, TRUE)
	STORE_OLD_XP_VALUE()
	
	PRINTLN("[Rewards_RP] - GIVE_END_OF_MISSION_RP_REWARD - RP Given: ", iRPReward)
	
	AWARD_EXTRA_MISSION_RP(bPass)
	
	iRPGainedForCelebration = iRPReward
	MC_playerBD[iLocalPart].iRewardXP = iRPReward + iBonusXP
	
ENDPROC
		
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Elite Challenges
// ##### Description: Functions for checkign if players have compelted Elite Challenges and giving those rewards
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL HAVE_CONTENT_SPECIFIC_ELITE_CHALLENGE_CONDITIONS_BEEN_MET(INT &iNumEliteComponents)
	
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN HAVE_ISLAND_HEIST_ELITE_CHALLENGE_CONDITIONS_BEEN_MET(iNumEliteComponents)
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC INT GET_ELITE_CHALLENGE_NUM_HACK_FAILS_THRESHOLD()
	
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
	AND g_sMPTunables.iIH_ELITE_CHALLENGE_HACK_FAILS_OVERRIDE > 0
		RETURN g_sMPTunables.iIH_ELITE_CHALLENGE_HACK_FAILS_OVERRIDE
	ENDIF
	
	RETURN g_FMMC_STRUCT.iHeistRewardHackThreshold
	
ENDFUNC

FUNC INT GET_ELITE_CHALLENGE_NUM_HEADSHOTS_THRESHOLD()
	
	//Content Specific Tunables Here
	
	RETURN g_FMMC_STRUCT.iHeistRewardHeadshotThreshold
	
ENDFUNC

FUNC INT GET_ELITE_CHALLENGE_NUM_KILLS_THRESHOLD()

	//Content Specific Tunables Here
	
	RETURN g_FMMC_STRUCT.iHeistRewardKillsThreshold
	
ENDFUNC

FUNC INT GET_ELITE_CHALLENGE_TIME_THRESHOLD()

	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
	AND g_sMPTunables.iIH_ELITE_CHALLENGE_TIME_OVERRIDE > 0
		RETURN g_sMPTunables.iIH_ELITE_CHALLENGE_TIME_OVERRIDE
	ENDIF
	
	RETURN g_FMMC_STRUCT.iHeistRewardTimeThreshold
	
ENDFUNC

FUNC INT GET_ELITE_CHALLENGE_LOOT_THRESHOLD()

	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
	AND g_sMPTunables.iIH_ELITE_CHALLENGE_LOOT_BAG_OVERRIDE > 0
		RETURN g_sMPTunables.iIH_ELITE_CHALLENGE_LOOT_BAG_OVERRIDE
	ENDIF
	
	RETURN 100
	
ENDFUNC

FUNC BOOL HAS_ELITE_CHALLENGE_BEEN_COMPLETED()

	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Not on the last mission: FALSE")
		RETURN FALSE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.bHasQuickRestartedDuringStrandMission
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentDidQuickRestart = TRUE
		
		IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
			missionEndShardData.iEliteChallenge = ISLAND_HEIST_END_ELITE_RESTART
		ENDIF
		
		PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Quick Restarted: FALSE")
		RETURN FALSE
	ENDIF

	IF g_FMMC_STRUCT.iDifficulity = DIFF_EASY
		PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Difficulty = Easy: FALSE")
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDifficultyHighEnoughForEliteChallenge = FALSE
		RETURN FALSE
	ENDIF
	
	INT iNumEliteComponents
	INT i
	BOOL bGiveReward = TRUE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDifficultyHighEnoughForEliteChallenge = TRUE
	
	IF NOT HAVE_CONTENT_SPECIFIC_ELITE_CHALLENGE_CONDITIONS_BEEN_MET(iNumEliteComponents)
		bGiveReward = FALSE
		PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Content Specific Checks Failed: FALSE")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistRewardBonusBitset, ci_bs_HeistTimeReward)
		INT iMissionTime = GET_MISSION_TIME_FOR_REWARDS()
		
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentMissionTime = TRUE
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iTargetMissionTime = GET_ELITE_CHALLENGE_TIME_THRESHOLD()
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMissionTime = iMissionTime
		iNumEliteComponents++
		PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Time Bonus Set - Time: ", iMissionTime, " Requirement: ", GET_ELITE_CHALLENGE_TIME_THRESHOLD())
		
		IF iMissionTime <= GET_ELITE_CHALLENGE_TIME_THRESHOLD()
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bMissionTimeChallengeComplete = TRUE
			PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Time Bonus Valid")
		ELSE
			PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Time Bonus Invalid: FALSE")
			bGiveReward = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistRewardBonusBitset, ci_bs_KillsReward)
	
		INT iKills
		FOR i = 0 TO FMMC_MAX_TEAMS - 1
			iKills += GET_TEAM_KILLS(i)
		ENDFOR
	
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentNumPedKills = TRUE
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedKillsTarget = GET_ELITE_CHALLENGE_NUM_KILLS_THRESHOLD()
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedKills = iKills
		iNumEliteComponents++
		PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Kill Bonus Set - Kills: ", iKills, " Requirement: ", GET_ELITE_CHALLENGE_NUM_KILLS_THRESHOLD())
		
		IF iKills >= GET_ELITE_CHALLENGE_NUM_KILLS_THRESHOLD()
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumPedKillsChallengeComplete = TRUE
			PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Kill Bonus Valid")
		ELSE
			PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Kill Bonus Invalid: FALSE")
			bGiveReward = FALSE		
		ENDIF
	
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistRewardBonusBitset, ci_bs_RewardsVoidOnTeamDeath)
		
		INT iDeaths
		FOR i = 0 TO FMMC_MAX_TEAMS - 1
			iDeaths += GET_TEAM_DEATHS(i)
		ENDFOR
		
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentLivesLost = TRUE
		iNumEliteComponents++
		PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - No Lives Lost Bonus Set - Lost: ", iDeaths, " Requirement: 0")
		
		IF iDeaths = 0
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumLivesLostChallengeComplete = TRUE
			PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - No Lives Lost Bonus Valid")
		ELSE
			PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - No Lives Lost Bonus Invalid: FALSE")
			bGiveReward = FALSE
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistRewardBonusBitset, ci_bs_RewardsNoHackFails)
	
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentHackFailed = TRUE
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumHacksFailedTarget = GET_ELITE_CHALLENGE_NUM_HACK_FAILS_THRESHOLD()
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumHacksFailed = GET_HACK_FAILS()
		iNumEliteComponents++
		PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Hack Bonus Set - Hack Fails: ", GET_HACK_FAILS(), " Requirement: ", GET_ELITE_CHALLENGE_NUM_HACK_FAILS_THRESHOLD())

		IF GET_HACK_FAILS() <= GET_BONUS_NUM_HACK_FAILS_THRESHOLD()
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumHackFailedChallengeComplete = TRUE
			PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Hack Bonus Valid")
		ELSE
			PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Hack Bonus Invalid: FALSE")
			bGiveReward = FALSE		
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistRewardBonusBitset, ci_bs_RewardsHeadshots)
		
		INT iHeadshots
		FOR i = 0 TO FMMC_MAX_TEAMS - 1
			iHeadshots += GET_TEAM_HEADSHOTS(i)
		ENDFOR
	
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentNumPedHeadshots = TRUE
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedHeadshotsTarget = GET_ELITE_CHALLENGE_NUM_HEADSHOTS_THRESHOLD()
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedHeadshots = iHeadshots
		iNumEliteComponents++
		PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Headshots Bonus Set - Headshots: ", iHeadshots, " Requirement: ", GET_ELITE_CHALLENGE_NUM_HEADSHOTS_THRESHOLD())
		
		IF iHeadshots >= GET_BONUS_NUM_HEADSHOTS_THRESHOLD()
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumPedHeadshotsChallengeComplete = TRUE
			PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Headshots Bonus Valid")
		ELSE
			PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Headshots Bonus Invalid: FALSE")
			bGiveReward = FALSE		
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistRewardBonusBitset, ci_bs_RewardsLootBagFull)
		
		PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Loot Bag Full Bonus Set - Requirement: ", GET_ELITE_CHALLENGE_LOOT_THRESHOLD())
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentLootBagFull = TRUE
		iNumEliteComponents++
		
		BOOL bAllBagsFull = TRUE
		INT iPart
		PARTICIPANT_INDEX partId
		PLAYER_INDEX playerId
		FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
		
			partId = INT_TO_NATIVE(PARTICIPANT_INDEX, iPart)
			IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(partId)
				RELOOP
			ENDIF
			
			playerId = NETWORK_GET_PLAYER_INDEX(partId)
			IF NOT IS_NET_PLAYER_OK(playerId, FALSE)
				RELOOP
			ENDIF
			
			IF IS_PLAYER_SCTV(playerId)
			OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerId)
				RELOOP
			ENDIF
			
			FLOAT fPercent = (FLOOR(MC_playerBD[iPart].sLootBag.fCurrentBagCapacity) / TO_FLOAT(BAG_CAPACITY__GET_MAX_CAPACITY())) * 100
			IF fPercent < GET_ELITE_CHALLENGE_LOOT_THRESHOLD()
				PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Loot Bag Full Bonus - Part ", iPart, "'s bag is not full")
				bAllBagsFull = FALSE
				BREAKLOOP
			ENDIF
			
		ENDFOR
		
		IF bAllBagsFull
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bLootBagFullChallengeComplete = TRUE
			PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Loot Bag Full Bonus Valid")
		ELSE
			PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Loot Bag Full Bonus Invalid: FALSE")
			bGiveReward = FALSE		
		ENDIF
		
	ENDIF
	
	IF MC_playerBD[iLocalPart].TSPlayerData.iStage > TSS_WAIT //Trip Skip Available
	AND MC_playerBD[iLocalPart].TSPlayerData.iCost > 0 //Trip Skip Used
		
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentTripSkip = TRUE
		bGiveReward = FALSE
		PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Used Trip Skip: FALSE.")
		
	ENDIF
	
	IF iNumEliteComponents <= 0
	
		PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - No Elite Challenge Requirements Set: FALSE")
		RETURN FALSE
		
	ENDIF
	
	IF !bGiveReward
		IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
			missionEndShardData.iEliteChallenge = ISLAND_HEIST_END_ELITE_NO_TRIED
		ENDIF
	ENDIF
	
	PRINTLN("[Rewards_Elite] - HAS_ELITE_CHALLENGE_BEEN_COMPLETED - Give Reward: ", BOOL_TO_STRING(bGiveReward), " - Num Components: ", iNumEliteComponents)
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumEliteChallengeComponents = iNumEliteComponents
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bEliteChallengeComplete = bGiveReward
	
	RETURN bGiveReward
	
ENDFUNC

FUNC INT GET_ELITE_CHALLENGE_CASH_FOR_CURRENT_CONTENT()
	
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN GET_ISLAND_HEIST_ELITE_CHALLENGE_CASH()
	ENDIF
	
	PRINTLN("[Rewards_Elite] - GET_ELITE_CHALLENGE_CASH_FOR_CURRENT_CONTENT - No specific reward set for this content.")
	
	RETURN g_FMMC_STRUCT.iHeistEliteChallengeReward
	
ENDFUNC

PROC GIVE_PLAYER_ELITE_CHALLENGE_REWARD(INT iEliteChallengeReward)
	
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		MARK_ISLAND_HEIST_ELITE_CHALLENGE_AS_COMPLETE()
		GIVE_PLAYER_ISLAND_HEIST_ELITE_CHALLENGE_REWARD(iEliteChallengeReward)
		EXIT
	ENDIF
	
	ASSERTLN("[Rewards_Elite] - GIVE_PLAYER_ELITE_CHALLENGE_REWARD - No EARN functions setup for this content.")
	PRINTLN("[Rewards_Elite] - GIVE_PLAYER_ELITE_CHALLENGE_REWARD - No EARN functions setup for this content.")
	
ENDPROC
		
PROC PROCESS_ELITE_CHALLENGES()

	IF NOT CHECK_TO_SEE_IF_THIS_IS_THE_LAST_STRAND_MISSION()
		EXIT
	ENDIF

	IF NOT HAS_ELITE_CHALLENGE_BEEN_COMPLETED()
		EXIT
	ENDIF
	
	INT iEliteChallengeReward = GET_ELITE_CHALLENGE_CASH_FOR_CURRENT_CONTENT()
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iEliteChallengeBonusValue = iEliteChallengeReward
	
	IF iEliteChallengeReward <= 0
		EXIT
	ENDIF
	
	PRINTLN("[Rewards_Elite] - PROCESS_ELITE_CHALLENGES - Awarding player $", iEliteChallengeReward)
	
	GIVE_PLAYER_ELITE_CHALLENGE_REWARD(iEliteChallengeReward)
	
ENDPROC
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Awards and Challenges
// ##### Description: Functions for giving awards/achievements
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_DAILY_OBJECTIVES(BOOL bPass)
	
	IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_FEATURED_ADVERSARY_MODE_JOB(g_FMMC_STRUCT.iRootContentIDHash)
		SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_FEATURED_SERIES)
		PRINTLN("[Rewards] - PROCESS_DAILY_OBJECTIVES - This was in a Featured series: ", g_FMMC_STRUCT.iRootContentIDHash)
	ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ADVERSARY_SERIES_JOB(g_FMMC_STRUCT.iRootContentIDHash)
		SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_ADVERSARY_SERIES)
		PRINTLN("[Rewards] - PROCESS_DAILY_OBJECTIVES - This was in a Adversary series: ", g_FMMC_STRUCT.iRootContentIDHash)
	ENDIF

	IF IS_THIS_A_VERSUS_MISSION()
		SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_VERSUS_MISSION)
		PRINTLN("[Rewards] - PROCESS_DAILY_OBJECTIVES - MP_DAILY_JOB_PARTICIPATE_IN_VERSUS_MISSION COMPLETE")
	ELIF bPass
		SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_COOP_MISSION)
		PRINTLN("[Rewards] - PROCESS_DAILY_OBJECTIVES - MP_DAILY_JOB_PARTICIPATE_IN_COOP_MISSION COMPLETE")
	ENDIF
	
	IF IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_FMMC_STRUCT.iRootContentIDHash)
		SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_COMPLETE_TUNER_FINALE)
		PRINTLN("[Rewards] - PROCESS_DAILY_OBJECTIVES - MP_DAILY_AM_COMPLETE_TUNER_FINALE COMPLETE")
	ENDIF
	
	#IF FEATURE_FIXER
	IF IS_THIS_A_FIXER_STORY_MISSION()
		SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_COMPLETE_INVESTIGATION)
		PRINTLN("[Rewards] - PROCESS_DAILY_OBJECTIVES - MP_DAILY_AM_COMPLETE_INVESTIGATION COMPLETE")
	ENDIF
	
	IF IS_THIS_A_FIXER_SHORT_TRIP()
		SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_COMPLETE_SHORT_TRIP)
		PRINTLN("[Rewards] - PROCESS_DAILY_OBJECTIVES - MP_DAILY_AM_COMPLETE_SHORT_TRIP COMPLETE")
	ENDIF
	#ENDIF
ENDPROC

PROC PROCESS_COMPLETED_SPECIFIC_MISSION_TYPE()	
	
	//Track playthroughs
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer //Heist Leader only
		INT iCurrentHeistType = GET_CURRENT_HEIST_MISSION(TRUE)
		IF iCurrentHeistType > -1
			MP_INT_STATS mpStat = GET_HEIST_MISSION_COMPLETION_STAT(iCurrentHeistType)
			IF mpStat != INT_TO_ENUM(MP_INT_STATS, 0)
				INCREMENT_MP_INT_CHARACTER_STAT(mpStat)
				PRINTLN("[Rewards] - PROCESS_COMPLETED_SPECIFIC_MISSION_TYPE - Current Heist Type: ", iCurrentHeistType, " Incrementing Stat to: ", GET_MP_INT_CHARACTER_STAT(mpStat))
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_THIS_ROOT_CONTENT_ID_A_YACHT_MISSION(g_FMMC_STRUCT.iRootContentIDHash)	
		PRINTLN("[Rewards] - PROCESS_COMPLETED_SPECIFIC_MISSION_TYPE - Yacht Mission - Passed")
		PRINTLN("[Rewards] - PROCESS_COMPLETED_SPECIFIC_MISSION_TYPE - Yacht Mission - g_bIamYachtMissionLeader: ", GET_STRING_FROM_BOOL(g_bIamYachtMissionLeader))
		SET_PLAYER_HAS_COMPLETED_THIS_YACHT_MISSION(GET_YACHT_MISSION_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash), g_bIamYachtMissionLeader)
		g_bIamYachtMissionLeader = FALSE
		
	ELIF IS_THIS_MISSION_HEIST_ISLAND_FINAL_STAGE(g_FMMC_STRUCT.iRootContentIDHash)		
		PROCESS_ISLAND_HEIST_COMPLETION()
	
	ELIF IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_FMMC_STRUCT.iRootContentIDHash)
		PROCESS_TUNER_ROBBERY_FINALE_COMPLETION()
	
	#IF FEATURE_FIXER
	ELIF IS_THIS_A_FIXER_STORY_MISSION()
		PROCESS_FIXER_STORY_MISSION_COMPLETION(GET_FIXER_STORY_MISSION_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash))
	ELIF IS_THIS_A_FIXER_SHORT_TRIP()
		PROCESS_FIXER_SHORT_TRIP_COMPLETION(GET_FIXER_SHORT_TRIP_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash), AM_I_FMMC_LOBBY_LEADER())
		
		IF IS_FIXER_EVENT_REWARD_ACTIVE(FIXER_EVENT_REWARD_SHORT_TRIP)
		AND NOT DOES_LOCAL_PLAYER_OWN_FIXER_EVENT_REWARD(FIXER_EVENT_REWARD_SHORT_TRIP)
			GIVE_LOCAL_PLAYER_FIXER_EVENT_REWARD(FIXER_EVENT_REWARD_SHORT_TRIP)
		ENDIF
	#ENDIF
	
	#IF FEATURE_DLC_1_2022
	ELIF IS_THIS_ROOT_CONTENT_ID_A_ULP_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		PROCESS_ULP_MISSION_COMPLETION(GET_ULP_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_FMMC_STRUCT.iRootContentIDHash), AM_I_FMMC_LOBBY_LEADER())
	#ENDIF
	
	#IF FEATURE_DLC_2_2022
	ELIF IS_THIS_ROOT_CONTENT_ID_A_XMAS22_STORY_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		PROCESS_XMAS_STORY_MISSION_COMPLETION(GET_XMAS22_STORY_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_FMMC_STRUCT.iRootContentIDHash), AM_I_FMMC_LOBBY_LEADER())
	#ENDIF
	
	ENDIF
	
ENDPROC

PROC PROCESS_CREW_CUT_ACHIEVEMENT(BOOL bPass)

	//Crew Cut Achievement should be given out when completing a job - on heists it should only be given if you pass
	IF bPass OR NOT IS_THIS_A_HEIST_MISSION()
		PRINTLN("[Rewards] - PROCESS_CREW_CUT_ACHIEVEMENT - Calling")
		DO_CREW_CUT_ACHIEVEMENT()
	ENDIF
	
ENDPROC

PROC PROCESS_CONTENT_SPECIFIC_AWARDS()
	
	IF IS_THIS_MISSION_HEIST_ISLAND_FINAL_STAGE(g_FMMC_STRUCT.iRootContentIDHash)	
		PROCESS_ISLAND_HEIST_AWARDS()
	ELIF IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_FMMC_STRUCT.iRootContentIDHash)	
		PROCESS_TUNER_ROBBERY_AWARDS()
	ENDIF
	
ENDPROC

PROC PROCESS_END_OF_MISSION_AWARDS(BOOL bPass)
			
	PROCESS_DAILY_OBJECTIVES(bPass)
	
	PROCESS_CREW_CUT_ACHIEVEMENT(bPass)
	
	SET_BIT(iLocalBoolCheck10, LBOOL10_PROCESSED_END_OF_MISSION_AWARDS)
	
	IF !bPass
		IF IS_THIS_MISSION_HEIST_ISLAND_FINAL_STAGE(g_FMMC_STRUCT.iRootContentIDHash)
		AND g_bIslandHeistCheaterIncremented
			DECREMENT_CURRENT_ISLAND_HEIST_CHEATER_BITSET()
		ENDIF
		
		EXIT
	ENDIF
	
	IF NOT CHECK_TO_SEE_IF_THIS_IS_THE_LAST_STRAND_MISSION()
		EXIT
	ENDIF
	
	PROCESS_CONTENT_SPECIFIC_AWARDS()
	
	PROCESS_COMPLETED_SPECIFIC_MISSION_TYPE()
	
ENDPROC
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: End Reward Processing
// ##### Description: General processing for giving end rewards
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_END_OF_MISSION_REWARDS(BOOL bPass, INT iTeamPos, INT iTotalMissionEndTime)
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		PRINTLN("[Rewards] - PROCESS_END_OF_MISSION_REWARDS - Test Mode - exitting.")
		EXIT
	ENDIF
		
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		PRINTLN("[Rewards] - PROCESS_END_OF_MISSION_REWARDS - Strand Mission being initialised - exitting.")
		EXIT
	ENDIF

	IF bPass
		PROCESS_ELITE_CHALLENGES()
	ENDIF
	
	INT	iCashReward = GET_FINAL_CASH_REWARD(bPass, iTeamPos, iTotalMissionEndTime)
	INT iRPReward = GET_FINAL_RP_REWARD(bPass, iTeamPos, iTotalMissionEndTime)
	
	PROCESS_END_OF_MISSION_AWARDS(bPass)

	GIVE_END_OF_MISSION_CASH_REWARD(iCashReward)
	GIVE_END_OF_MISSION_RP_REWARD(iRPReward, bPass)
		
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Telemetry
// ##### Description: Functions for tracking and setting telemetry
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Sub Section Name: Generic Instanced Mission End (Fixer onwards)
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_FAIL_REASON_GENERIC_INSTANCED_MISSION_TELEMETRY(eFailMissionEnum eFailReason)
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET() 
		EXIT
	ENDIF
	
	g_sInstanced_Mission_End_Telemetry_data.endingReason = ENUM_TO_INT(eFailReason)
	PRINTLN("[TEL] - PROCESS_FAIL_REASON_GENERIC_INSTANCED_MISSION_TELEMETRY - endingReason - ", g_sInstanced_Mission_End_Telemetry_data.endingReason)
	
ENDPROC

PROC PROCESS_START_OF_MISSION_GENERIC_INSTANCED_MISSION_TELEMETRY()
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET() 
		EXIT
	ENDIF
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
		g_sInstanced_Mission_End_Telemetry_data.bossId1 = GB_GET_GANG_BOSS_UUID_INT_A(GB_GET_LOCAL_PLAYER_GANG_BOSS())
		g_sInstanced_Mission_End_Telemetry_data.bossId2 = GB_GET_GANG_BOSS_UUID_INT_B(GB_GET_LOCAL_PLAYER_GANG_BOSS())
	ELSE
		g_sInstanced_Mission_End_Telemetry_data.bossId1 = -1
		g_sInstanced_Mission_End_Telemetry_data.bossId2 = -1
	ENDIF
	PRINTLN("[TEL] - PROCESS_START_OF_MISSION_GENERIC_INSTANCED_MISSION_TELEMETRY - bossId1 ", g_sInstanced_Mission_End_Telemetry_data.bossId1)
	PRINTLN("[TEL] - PROCESS_START_OF_MISSION_GENERIC_INSTANCED_MISSION_TELEMETRY - bossId2 ", g_sInstanced_Mission_End_Telemetry_data.bossId2)
	
	g_sInstanced_Mission_End_Telemetry_data.playerRole = GET_PLAYER_ROLE_FOR_TELEMETRY()
	PRINTLN("[TEL] - PROCESS_START_OF_MISSION_GENERIC_INSTANCED_MISSION_TELEMETRY - playerRole ", g_sInstanced_Mission_End_Telemetry_data.playerRole)

	g_sInstanced_Mission_End_Telemetry_data.bosstype = iBossType
	PRINTLN("[TEL] - PROCESS_START_OF_MISSION_GENERIC_INSTANCED_MISSION_TELEMETRY - bosstype ", g_sInstanced_Mission_End_Telemetry_data.bosstype)
	
	//Reset so they are not set on potential quits/bails
	g_sInstanced_Mission_End_Telemetry_data.endingReason = 0
	g_sInstanced_Mission_End_Telemetry_data.won = FALSE 
	
ENDPROC

PROC PROCESS_END_OF_MISSION_GENERIC_INSTANCED_MISSION_TELEMETRY(BOOL bPassed, INT iTotalMissionTime)
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET() 
		EXIT
	ENDIF
	
	g_sInstanced_Mission_End_Telemetry_data.timeTakenToComplete = iTotalMissionTime
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_GENERIC_INSTANCED_MISSION_TELEMETRY - timeTakenToComplete ", g_sInstanced_Mission_End_Telemetry_data.timeTakenToComplete)
	
	g_sInstanced_Mission_End_Telemetry_data.timeLimit = 0 //No use for this currently
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_GENERIC_INSTANCED_MISSION_TELEMETRY - timeLimit ", g_sInstanced_Mission_End_Telemetry_data.timeLimit)
	
	g_sInstanced_Mission_End_Telemetry_data.targetsKilled = MC_Playerbd[iLocalPart].iNumPedKills + MC_Playerbd[iLocalPart].iAmbientCopsKilled
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_GENERIC_INSTANCED_MISSION_TELEMETRY - targetsKilled ", g_sInstanced_Mission_End_Telemetry_data.targetsKilled)
	
	g_sInstanced_Mission_End_Telemetry_data.innocentsKilled = MC_playerBD[iLocalPart].iCivilianKills
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_GENERIC_INSTANCED_MISSION_TELEMETRY - innocentsKilled ", g_sInstanced_Mission_End_Telemetry_data.innocentsKilled)

	g_sInstanced_Mission_End_Telemetry_data.deaths = MC_Playerbd[iLocalPart].iNumPlayerDeaths
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_GENERIC_INSTANCED_MISSION_TELEMETRY - deaths ", g_sInstanced_Mission_End_Telemetry_data.deaths)
	
	g_sInstanced_Mission_End_Telemetry_data.failedStealth = HAS_ANY_TEAM_TRIGGERED_AGGRO(ciMISSION_AGGRO_INDEX_ALL)
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_GENERIC_INSTANCED_MISSION_TELEMETRY - failedStealth ", g_sInstanced_Mission_End_Telemetry_data.failedStealth)
	
	IF g_sInstanced_Mission_End_Telemetry_data.sessionID = -1
		g_sInstanced_Mission_End_Telemetry_data.sessionID = g_MissionControllerserverBD_LB.iHashMAC
		PRINTLN("[TEL] - PROCESS_END_OF_MISSION_GENERIC_INSTANCED_MISSION_TELEMETRY - sessionID ", g_sInstanced_Mission_End_Telemetry_data.sessionID)
	ENDIF
	
	IF bPassed
		g_sInstanced_Mission_End_Telemetry_data.endingReason = 0
		PRINTLN("[TEL] - PROCESS_END_OF_MISSION_GENERIC_INSTANCED_MISSION_TELEMETRY - endingReason ", g_sInstanced_Mission_End_Telemetry_data.endingReason)
		
		g_sInstanced_Mission_End_Telemetry_data.won = TRUE
		PRINTLN("[TEL] - PROCESS_END_OF_MISSION_GENERIC_INSTANCED_MISSION_TELEMETRY - won ", BOOL_TO_STRING(g_sInstanced_Mission_End_Telemetry_data.won))
	ENDIF
	
	g_sInstanced_Mission_End_Telemetry_data.cashEarned = iCashGainedForCelebration
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_GENERIC_INSTANCED_MISSION_TELEMETRY - cashEarned ", g_sInstanced_Mission_End_Telemetry_data.cashEarned)

	g_sInstanced_Mission_End_Telemetry_data.playerCount = GET_TOTAL_STARTING_PLAYERS()
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_GENERIC_INSTANCED_MISSION_TELEMETRY - playerCount ", g_sInstanced_Mission_End_Telemetry_data.playerCount)

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Sub Section Name: Robbery Telemetry Processing (e.g. Tuners, The Contract... 1-4 player missions with freemode preps)
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_ROBBERY_MISSION_CATEGORY()

	IF IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_FMMC_STRUCT.iRootContentIDHash)	
		RETURN  ciROBBERY_MISSION_CATEGORY_TUNER
	#IF FEATURE_FIXER
	ELIF IS_THIS_A_FIXER_STORY_MISSION()
		RETURN ciROBBERY_MISSION_CATEGORY_FIXER
	#ENDIF
	ENDIF
	
	ASSERTLN("GET_ROBBERY_MISSION_CATEGORY - Invalid Mission Type")
	RETURN -1
	
ENDFUNC

PROC PROCESS_START_OF_MISSION_ROBBERY_TELEMETRY()

	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
		g_sTuner_Robbery_Finale_Telemetry_data.bossId1 = GB_GET_GANG_BOSS_UUID_INT_A(GB_GET_LOCAL_PLAYER_GANG_BOSS())
		g_sTuner_Robbery_Finale_Telemetry_data.bossId2 = GB_GET_GANG_BOSS_UUID_INT_B(GB_GET_LOCAL_PLAYER_GANG_BOSS())
		PRINTLN("[TEL] - PROCESS_START_OF_MISSION_ROBBERY_TELEMETRY - bossId1 ", g_sTuner_Robbery_Finale_Telemetry_data.bossId1)
		PRINTLN("[TEL] - PROCESS_START_OF_MISSION_ROBBERY_TELEMETRY - bossId2 ", g_sTuner_Robbery_Finale_Telemetry_data.bossId2)
		IF IS_THIS_A_FIXER_STORY_MISSION()
			g_sTuner_Robbery_Finale_Telemetry_data.strandID = GET_FIXER_STRAND_HASH(GET_PLAYER_CURRENT_FIXER_STRAND(GB_GET_LOCAL_PLAYER_GANG_BOSS()))	
			PRINTLN("[TEL] - PROCESS_START_OF_MISSION_ROBBERY_TELEMETRY - strandID ", g_sTuner_Robbery_Finale_Telemetry_data.strandID)
		ENDIF
	ENDIF
	
	g_sTuner_Robbery_Finale_Telemetry_data.playerRole = GET_PLAYER_ROLE_FOR_TELEMETRY()
	PRINTLN("[TEL] - PROCESS_START_OF_MISSION_ROBBERY_TELEMETRY - playerRole ", g_sTuner_Robbery_Finale_Telemetry_data.playerRole)

	g_sTuner_Robbery_Finale_Telemetry_data.bosstype = iBossType
	PRINTLN("[TEL] - PROCESS_START_OF_MISSION_ROBBERY_TELEMETRY - bosstype ", g_sTuner_Robbery_Finale_Telemetry_data.bosstype)
	
	g_sTuner_Robbery_Finale_Telemetry_data.missionCategory = GET_ROBBERY_MISSION_CATEGORY()
	PRINTLN("[TEL] - PROCESS_START_OF_MISSION_ROBBERY_TELEMETRY - bosstype ", g_sTuner_Robbery_Finale_Telemetry_data.bosstype)
	
	//Reset so they are not set on potential quits/bails
	g_sTuner_Robbery_Finale_Telemetry_data.endingReason = 0
	g_sTuner_Robbery_Finale_Telemetry_data.won = FALSE
	
ENDPROC

PROC PROCESS_FAIL_REASON_ROBBERY_TELEMETRY(eFailMissionEnum eFailReason)
	
	g_sTuner_Robbery_Finale_Telemetry_data.endingReason = ENUM_TO_INT(eFailReason)
	PRINTLN("[TEL] - PROCESS_FAIL_REASON_ROBBERY_TELEMETRY - endingReason - ", g_sTuner_Robbery_Finale_Telemetry_data.endingReason)
	
ENDPROC

PROC PROCESS_END_OF_MISSION_ROBBERY_TELEMETRY(BOOL bPassed, INT iTotalMissionTime)
	
	g_sTuner_Robbery_Finale_Telemetry_data.timeTakenToComplete = iTotalMissionTime
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ROBBERY_TELEMETRY - timeTakenToComplete ", g_sTuner_Robbery_Finale_Telemetry_data.timeTakenToComplete)
	
	g_sTuner_Robbery_Finale_Telemetry_data.targetsKilled = MC_Playerbd[iLocalPart].iNumPedKills + MC_Playerbd[iLocalPart].iAmbientCopsKilled
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ROBBERY_TELEMETRY - targetsKilled ", g_sTuner_Robbery_Finale_Telemetry_data.targetsKilled)
	
	g_sTuner_Robbery_Finale_Telemetry_data.innocentsKilled = MC_playerBD[iLocalPart].iCivilianKills
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ROBBERY_TELEMETRY - innocentsKilled ", g_sTuner_Robbery_Finale_Telemetry_data.innocentsKilled)

	g_sTuner_Robbery_Finale_Telemetry_data.deaths = MC_Playerbd[iLocalPart].iNumPlayerDeaths
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ROBBERY_TELEMETRY - deaths ", g_sTuner_Robbery_Finale_Telemetry_data.deaths)
	
	g_sTuner_Robbery_Finale_Telemetry_data.failedStealth = HAS_ANY_TEAM_TRIGGERED_AGGRO(ciMISSION_AGGRO_INDEX_ALL)
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ROBBERY_TELEMETRY - failedStealth ", g_sTuner_Robbery_Finale_Telemetry_data.failedStealth)
	
	IF bPassed
		g_sTuner_Robbery_Finale_Telemetry_data.checkpoint = 0
		PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ROBBERY_TELEMETRY - checkpoint ", g_sTuner_Robbery_Finale_Telemetry_data.checkpoint)
		
		g_sTuner_Robbery_Finale_Telemetry_data.endingReason = 0
		PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ROBBERY_TELEMETRY - endingReason ", g_sTuner_Robbery_Finale_Telemetry_data.endingReason)
		
		g_sTuner_Robbery_Finale_Telemetry_data.won = TRUE
		PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ROBBERY_TELEMETRY - won ", BOOL_TO_STRING(g_sTuner_Robbery_Finale_Telemetry_data.won))
	ELSE
		g_sTuner_Robbery_Finale_Telemetry_data.checkpoint = GET_CURRENT_CHECKPOINT_INDEX()
		PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ROBBERY_TELEMETRY - checkpoint ", g_sTuner_Robbery_Finale_Telemetry_data.checkpoint)
	ENDIF
	
	g_sTuner_Robbery_Finale_Telemetry_data.moneyEarned = iCashGainedForCelebration
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_ROBBERY_TELEMETRY - moneyEarned ", g_sTuner_Robbery_Finale_Telemetry_data.moneyEarned)

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Sub Section Name: General Telemetry Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_SPECIFIC_CONTENT_CONTINUITY_TYPE_TELEMETRY(INT iContentContinuityType)

	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		PROCESS_ISLAND_HEIST_CONTINUITY_TYPE_TELEMETRY(iContentContinuityType)
		EXIT
	ENDIF

ENDPROC

FUNC BOOL DOES_CONTENT_REQUIRE_HEIST_POSIX_AND_HASHED_MAC()
	
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN TRUE
	ELIF IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_FMMC_STRUCT.iRootContentIDHash)	
		RETURN TRUE
	#IF FEATURE_FIXER
	ELIF IS_THIS_A_FIXER_STORY_MISSION()
		RETURN TRUE
	#ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_FAIL_REASON_TELEMETRY(eFailMissionEnum eFailReason)

	PRINTLN("[TEL] - PROCESS_FAIL_REASON_TELEMETRY - Called with fail reason: ", eFailReason)
	
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		PROCESS_FAIL_REASON_ISLAND_HEIST_TELEMETRY(eFailReason)
		EXIT
	ELIF IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_FMMC_STRUCT.iRootContentIDHash)	
	#IF FEATURE_FIXER
	OR IS_THIS_A_FIXER_STORY_MISSION()
	#ENDIF
		PROCESS_FAIL_REASON_ROBBERY_TELEMETRY(eFailReason)
		EXIT
	ELSE
		IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_FIXER)
			PROCESS_FAIL_REASON_GENERIC_INSTANCED_MISSION_TELEMETRY(eFailReason)
			EXIT
		ENDIF
	ENDIF
	
	g_sJobHeistInfo.m_failureReason  = ENUM_TO_INT(eFailReason)	
	PRINTLN("[TEL] - PROCESS_FAIL_REASON_TELEMETRY - m_failureReason - ", g_sJobHeistInfo.m_failureReason)
	
ENDPROC

PROC PROCESS_START_OF_MISSION_TELEMETRY()

	IF IS_FAKE_MULTIPLAYER_MODE_SET() 
		EXIT
	ENDIF
	   
	PRINTLN("[TEL] - PROCESS_START_OF_MISSION_TELEMETRY - Called")
	
	ASSIGN_GANG_BOSS_TYPE()
	
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		PROCESS_START_OF_MISSION_ISLAND_HEIST_TELEMETRY()
	ELIF IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_FMMC_STRUCT.iRootContentIDHash)
	#IF FEATURE_FIXER
	OR IS_THIS_A_FIXER_STORY_MISSION()
	#ENDIF
		PROCESS_START_OF_MISSION_ROBBERY_TELEMETRY()
	ELSE
		IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_FIXER)
			PROCESS_START_OF_MISSION_GENERIC_INSTANCED_MISSION_TELEMETRY()
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_END_OF_MISSION_TELEMETRY(BOOL bPassed, INT iTotalMissionTime)

	IF IS_FAKE_MULTIPLAYER_MODE_SET() 
		EXIT
	ENDIF
	   
	PRINTLN("[TEL] - PROCESS_END_OF_MISSION_TELEMETRY - Called with passed: ", BOOL_TO_STRING(bPassed), ", Total mission time: ", iTotalMissionTime)
	
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		PROCESS_END_OF_MISSION_ISLAND_HEIST_TELEMETRY(bPassed, iTotalMissionTime)
	ELIF IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_FMMC_STRUCT.iRootContentIDHash)	
	#IF FEATURE_FIXER
	OR IS_THIS_A_FIXER_STORY_MISSION()
	#ENDIF
		PROCESS_END_OF_MISSION_ROBBERY_TELEMETRY(bPassed, iTotalMissionTime)
	ELSE
		IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_FIXER)
			PROCESS_END_OF_MISSION_GENERIC_INSTANCED_MISSION_TELEMETRY(bPassed, iTotalMissionTime)
		ENDIF
	ENDIF

ENDPROC
