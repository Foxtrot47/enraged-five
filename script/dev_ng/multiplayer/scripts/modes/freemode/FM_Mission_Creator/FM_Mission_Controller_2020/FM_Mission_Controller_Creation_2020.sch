// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Creation ------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script Name: FM_Mission_Controller_2020.sc ------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Holds all the creation functions and set up functions for various entities and non-entities.
// ##### Peds, Vehicles, Objects, Props, etc.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_SpawnGroups_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entity Creation Wrappers
// ##### Description: Mission Controller specific wrappers for the CREATE_NET_ functions in net_include.sch
// ##### These should be used in place of the CREATE_NET_ functions so we can manage per-frame entity creation
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL FMMC_CREATE_NET_VEHICLE(NETWORK_INDEX &vehicleNetId, 
								MODEL_NAMES vehModel, 
								VECTOR vPos, 
								FLOAT fHeading, 
								BOOL bScriptHostVeh = TRUE, 
								BOOL bRegisterAsNetworkObject = TRUE, 
								BOOL bSetExistOnAllMachines = TRUE,  
								BOOL bSetAsStolen = FALSE, 
								BOOL bFreezeWaitingOnCollision = TRUE, 
								BOOL bClearArea = TRUE, 
								BOOL bOpenBoot = FALSE, 
								BOOL bAlwaysExistForLocalPlayer = FALSE, 
								BOOL bIgnoreGroundCheck = FALSE, 
								BOOL bOnlyExistForParticipants = FALSE,
								BOOL bFMMCForceCreate = FALSE) 
								
	IF iEntitiesSpawnedThisFrame >= ciFMMC_MAX_ENTITIES_SPAWNED_PER_FRAME
	AND NOT bFMMCForceCreate
		PRINTLN("[ENTITY_CREATION] FMMC_CREATE_NET_VEHICLE - CREATION BLOCKED - Entities spawned this frame: ", iEntitiesSpawnedThisFrame, "/", ciFMMC_MAX_ENTITIES_SPAWNED_PER_FRAME)
		RETURN FALSE
	ENDIF
	
	IF CREATE_NET_VEHICLE(vehicleNetId, 
				vehModel, 
				vPos,
				fHeading, 
				bScriptHostVeh, 
				bRegisterAsNetworkObject, 
				bSetExistOnAllMachines,  
				bSetAsStolen, 
				bFreezeWaitingOnCollision, 
				bClearArea, 
				bOpenBoot, 
				bAlwaysExistForLocalPlayer, 
				bIgnoreGroundCheck, 
				bOnlyExistForParticipants)
				
		iEntitiesSpawnedThisFrame++
		   
		PRINTLN("[ENTITY_CREATION] FMMC_CREATE_NET_VEHICLE - CREATED - Entities spawned this frame: ", iEntitiesSpawnedThisFrame, "/", ciFMMC_MAX_ENTITIES_SPAWNED_PER_FRAME, " Forced: ", BOOL_TO_STRING(bFMMCForceCreate))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL FMMC_CREATE_NET_TRAIN(NETWORK_INDEX &niNewTrain, 
								FMMCPlacedTrainsStruct&	sTrainData,
								VECTOR vCustomSpawnCoords,
								BOOL bFMMCForceCreate = FALSE) 
								
	IF iEntitiesSpawnedThisFrame >= ciFMMC_MAX_ENTITIES_SPAWNED_PER_FRAME
	AND NOT bFMMCForceCreate
		PRINTLN("[ENTITY_CREATION] FMMC_CREATE_NET_TRAIN - CREATION BLOCKED - Entities spawned this frame: ", iEntitiesSpawnedThisFrame, "/", ciFMMC_MAX_ENTITIES_SPAWNED_PER_FRAME)
		RETURN FALSE
	ENDIF
	
	VEHICLE_INDEX viNewTrain = CREATE_FMMC_TRAIN(sTrainData, vCustomSpawnCoords, FALSE, FALSE)
	IF DOES_ENTITY_EXIST(viNewTrain)
		niNewTrain = VEH_TO_NET(viNewTrain)
		iEntitiesSpawnedThisFrame++
		PRINTLN("[ENTITY_CREATION] FMMC_CREATE_NET_TRAIN - CREATED - Entities spawned this frame: ", iEntitiesSpawnedThisFrame, "/", ciFMMC_MAX_ENTITIES_SPAWNED_PER_FRAME, " Forced: ", BOOL_TO_STRING(bFMMCForceCreate))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC
		
FUNC BOOL FMMC_CREATE_NET_PED(NETWORK_INDEX &pedNetId, 
								PED_TYPE pedType, 
								MODEL_NAMES pedModel, 
								VECTOR vPos, 
								FLOAT fHeading, 
								BOOL bScriptHostPed = TRUE, 
								BOOL bRegisterAsNetworkObject = TRUE, 
								BOOL bFreezeWaitingOnCollision = TRUE,
								BOOL bFMMCForceCreate = FALSE)

	IF iEntitiesSpawnedThisFrame >= ciFMMC_MAX_ENTITIES_SPAWNED_PER_FRAME
	AND NOT bFMMCForceCreate
		PRINTLN("[ENTITY_CREATION] FMMC_CREATE_NET_PED - CREATION BLOCKED - Entities spawned this frame: ", iEntitiesSpawnedThisFrame, "/", ciFMMC_MAX_ENTITIES_SPAWNED_PER_FRAME)
		RETURN FALSE
	ENDIF
	
	IF CREATE_NET_PED(pedNetId, 
				pedType,
				pedModel,
				vPos,
				fHeading,
				bScriptHostPed,
				bRegisterAsNetworkObject,
				bFreezeWaitingOnCollision)
				
		iEntitiesSpawnedThisFrame++
		   
		PRINTLN("[ENTITY_CREATION] FMMC_CREATE_NET_PED - CREATED - Entities spawned this frame: ", iEntitiesSpawnedThisFrame, "/", ciFMMC_MAX_ENTITIES_SPAWNED_PER_FRAME, " Forced: ", BOOL_TO_STRING(bFMMCForceCreate))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL FMMC_CREATE_NET_PED_IN_VEHICLE(NETWORK_INDEX &pedNetId, 
								NETWORK_INDEX vehicleNetId, 
								PED_TYPE pedType, 
								MODEL_NAMES pedModel, 
								VEHICLE_SEAT vehseat, 
								BOOL bScriptHostPed = TRUE, 
								BOOL bRegisterAsNetworkObject = TRUE, 
								BOOL bFreezeWaitingOnCollision = TRUE,
								BOOL bFMMCForceCreate = FALSE)

	IF iEntitiesSpawnedThisFrame >= ciFMMC_MAX_ENTITIES_SPAWNED_PER_FRAME
	AND NOT bFMMCForceCreate
		PRINTLN("[ENTITY_CREATION] FMMC_CREATE_NET_PED_IN_VEHICLE - CREATION BLOCKED - Entities spawned this frame: ", iEntitiesSpawnedThisFrame, "/", ciFMMC_MAX_ENTITIES_SPAWNED_PER_FRAME)
		RETURN FALSE
	ENDIF
	
	IF MC_IS_PED_AN_ANIMAL(pedModel)
	
		REQUEST_ANIM_DICT(GET_ANIMAL_SIT_IN_VEHICLE_DICT(pedModel))
		
		IF NOT HAS_ANIM_DICT_LOADED(GET_ANIMAL_SIT_IN_VEHICLE_DICT(pedModel))
			PRINTLN("[ENTITY_CREATION] FMMC_CREATE_NET_PED_IN_VEHICLE - CREATED (ANIMAL) - WAITING FOR ANIM DICT TO LOAD!!!")
			RETURN FALSE
		ENDIF
		
		PED_INDEX pedAnimal
		VEHICLE_INDEX vehToEnter = NET_TO_VEH(vehicleNetId)
		
		VECTOR vPos = GET_ENTITY_COORDS(vehToEnter)
		vPos.z += 10.0
		
		IF FMMC_CREATE_NET_PED(pedNetId, PEDTYPE_MISSION, pedModel, vPos, 0.0,
					DEFAULT, DEFAULT, DEFAULT, TRUE) //Force Creation
						
			pedAnimal = NET_TO_PED(pedNetId)
			
			SET_PED_RESET_FLAG(pedAnimal, PRF_SuppressInAirEvent, TRUE)	
			CLEAR_PED_TASKS_IMMEDIATELY(pedAnimal)
			SET_PED_ALLOW_VEHICLES_OVERRIDE(pedAnimal, TRUE)
			SET_PED_INTO_VEHICLE(pedAnimal, vehToEnter, GET_SUITABLE_SEAT_TYPE_FOR_CHOP(pedAnimal, vehToEnter))
			TASK_PLAY_ANIM(pedAnimal, GET_ANIMAL_SIT_IN_VEHICLE_DICT(pedModel), GET_ANIMAL_SIT_IN_VEHICLE_ANIM(pedModel), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedAnimal)
			
			PRINTLN("[ENTITY_CREATION] FMMC_CREATE_NET_PED_IN_VEHICLE - CREATED (ANIMAL) - Entities spawned this frame: ", iEntitiesSpawnedThisFrame, "/", ciFMMC_MAX_ENTITIES_SPAWNED_PER_FRAME, " Forced: ", BOOL_TO_STRING(bFMMCForceCreate))
			RETURN TRUE
		ENDIF
	ELSE	
		IF CREATE_NET_PED_IN_VEHICLE(pedNetId,
									vehicleNetId,
									pedType,
									pedModel,
									vehseat,
									bScriptHostPed = TRUE,
									bRegisterAsNetworkObject = TRUE,
									bFreezeWaitingOnCollision = TRUE)
					
			iEntitiesSpawnedThisFrame++
			   
			PRINTLN("[ENTITY_CREATION] FMMC_CREATE_NET_PED_IN_VEHICLE - CREATED - Entities spawned this frame: ", iEntitiesSpawnedThisFrame, "/", ciFMMC_MAX_ENTITIES_SPAWNED_PER_FRAME, " Forced: ", BOOL_TO_STRING(bFMMCForceCreate))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL FMMC_CREATE_NET_OBJ(NETWORK_INDEX &objNetId,
						MODEL_NAMES objModel, 
						VECTOR vPos, 
						BOOL bScriptHostObject = TRUE, 
						BOOL bRegisterAsNetworkObject = TRUE, 
						BOOL bForceToBeObject = FALSE, 
						BOOL bFreezeWaitingOnCollision = TRUE, 
						BOOL bCreateNoOffset = FALSE, 
						BOOL bOnlyExistForParticipants = FALSE, 
						BOOL bStartInvisible = FALSE,
						BOOL bFMMCForceCreate = FALSE) 
						
	IF iEntitiesSpawnedThisFrame >= ciFMMC_MAX_ENTITIES_SPAWNED_PER_FRAME
	AND NOT bFMMCForceCreate
		PRINTLN("[ENTITY_CREATION] FMMC_CREATE_NET_OBJ - CREATION BLOCKED - Entities spawned this frame: ", iEntitiesSpawnedThisFrame, "/", ciFMMC_MAX_ENTITIES_SPAWNED_PER_FRAME)
		RETURN FALSE
	ENDIF
	
	IF CREATE_NET_OBJ(objNetId,
					objModel, 
					vPos, 
					bScriptHostObject, 
					bRegisterAsNetworkObject, 
					bForceToBeObject, 
					bFreezeWaitingOnCollision, 
					bCreateNoOffset, 
					bOnlyExistForParticipants, 
					bStartInvisible)
				
		iEntitiesSpawnedThisFrame++
		   
		PRINTLN("[ENTITY_CREATION] FMMC_CREATE_NET_OBJ - CREATED - Entities spawned this frame: ", iEntitiesSpawnedThisFrame, "/", ciFMMC_MAX_ENTITIES_SPAWNED_PER_FRAME, " Forced: ", BOOL_TO_STRING(bFMMCForceCreate))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC						
	
FUNC BOOL FMMC_CREATE_NET_PORTABLE_PICKUP(NETWORK_INDEX& niPortablePickup, 
										MODEL_NAMES model, 
										VECTOR vCreationCoords, 
										FLOAT fCreationHeading, 
										PICKUP_TYPE eType, 
										BOOL bExistsOnAllMachines, 
										BOOL bSnapToGround, 
										BOOL bMakeVehsAvoid, 
										BOOL bCreateInvincible,
										BOOL bFMMCForceCreate = FALSE)
						
	IF iEntitiesSpawnedThisFrame >= ciFMMC_MAX_ENTITIES_SPAWNED_PER_FRAME
	AND NOT bFMMCForceCreate
		PRINTLN("[ENTITY_CREATION] FMMC_CREATE_NET_PORTABLE_PICKUP - CREATION BLOCKED - Entities spawned this frame: ", iEntitiesSpawnedThisFrame, "/", ciFMMC_MAX_ENTITIES_SPAWNED_PER_FRAME)
		RETURN FALSE
	ENDIF
	
	IF CREATE_NET_PORTABLE_PICKUP(niPortablePickup, 
								model, 
								vCreationCoords, 
								fCreationHeading, 
								eType, 
								bExistsOnAllMachines, 
								bSnapToGround, 
								bMakeVehsAvoid, 
								bCreateInvincible)
				
		iEntitiesSpawnedThisFrame++
		   
		PRINTLN("[ENTITY_CREATION] FMMC_CREATE_NET_PORTABLE_PICKUP - CREATED - Entities spawned this frame: ", iEntitiesSpawnedThisFrame, "/", ciFMMC_MAX_ENTITIES_SPAWNED_PER_FRAME, " Forced: ", BOOL_TO_STRING(bFMMCForceCreate))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC	

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Checkpoints
// ##### Description: The main processing function.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT MC_GET_QRC_SPAWN_POINT_FOR_PLAYER(INT iTeam, INT iTeamSlot, INT iCheckpoint)
	
	INT iSpawnPoint, iPointsFoundForTeam
	
	FOR iSpawnPoint = 0 TO (g_FMMC_STRUCT.iNumberOfQRCSpawnPoints - 1)
		
		IF IS_VECTOR_ZERO(g_FMMC_STRUCT.sQRCSpawnPoint[iSpawnPoint].vSpawnPos)
			PRINTLN("[RCC MISSION][QRCSP] MC_GET_QRC_SPAWN_POINT_FOR_PLAYER - Spawn point ",iSpawnPoint," rejected as vSpawnPos is empty")
			RELOOP
		ENDIF
		
		IF g_FMMC_STRUCT.sQRCSpawnPoint[iSpawnPoint].iActiveForTeam != iTeam
			PRINTLN("[RCC MISSION][QRCSP] MC_GET_QRC_SPAWN_POINT_FOR_PLAYER - Spawn point ",iSpawnPoint," rejected as not for my team ",iTeam,", iActiveForTeam = ",g_FMMC_STRUCT.sQRCSpawnPoint[iSpawnPoint].iActiveForTeam)
			RELOOP
		ENDIF
		
		IF g_FMMC_STRUCT.sQRCSpawnPoint[iSpawnPoint].iActiveOnCheckpointBS != 0
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sQRCSpawnPoint[iSpawnPoint].iActiveOnCheckpointBS, iCheckpoint)
			PRINTLN("[RCC MISSION][QRCSP] MC_GET_QRC_SPAWN_POINT_FOR_PLAYER - Spawn point ",iSpawnPoint," rejected as not for checkpoint ",iCheckpoint,", iActiveOnCheckpointBS = ",g_FMMC_STRUCT.sQRCSpawnPoint[iSpawnPoint].iActiveOnCheckpointBS)
			RELOOP
		ENDIF
		
		IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iSpawnPoint, eSGET_QRCPoints, iTeam)
			PRINTLN("[RCC MISSION][QRCSP] MC_GET_QRC_SPAWN_POINT_FOR_PLAYER - Spawn point ",iSpawnPoint," rejected as does not have a suitable spawn group")
			RELOOP
		ENDIF
		
		IF iPointsFoundForTeam != iTeamSlot
			
			PRINTLN("[RCC MISSION][QRCSP] MC_GET_QRC_SPAWN_POINT_FOR_PLAYER - Spawn point ",iSpawnPoint," rejected as not for my team slot ",iTeamSlot,", this is for slot ",iPointsFoundForTeam)
			iPointsFoundForTeam++
			
			RELOOP
		ENDIF
		
		PRINTLN("[RCC MISSION][QRCSP] MC_GET_QRC_SPAWN_POINT_FOR_PLAYER - Spawn point ",iSpawnPoint," found for my team slot ",iTeamSlot,", return spawn point",iSpawnPoint)
		RETURN iSpawnPoint
		
	ENDFOR
	
	PRINTLN("[RCC MISSION][QRCSP] MC_GET_QRC_SPAWN_POINT_FOR_PLAYER - No spawn point found, return -1")
	RETURN -1
	
ENDFUNC

FUNC VECTOR MC_GET_TEAM_CHECKPOINT_QUICK_RESTART_START_POS(INT iPlayerTeam, INT iTeamSlot, INT iCheckpoint)
	
	IF g_FMMC_STRUCT.iNumberOfQRCSpawnPoints > 0
		INT iQRCSpawnPoint = MC_GET_QRC_SPAWN_POINT_FOR_PLAYER(iPlayerTeam, iTeamSlot, iCheckpoint)
		
		IF iQRCSpawnPoint != -1
			RETURN g_FMMC_STRUCT.sQRCSpawnPoint[iQRCSpawnPoint].vSpawnPos
		ENDIF
	ENDIF
	
	RETURN g_FMMC_STRUCT.vTeamRestartPoint[iPlayerTeam][iCheckpoint][iTeamSlot]
	
ENDFUNC

FUNC BOOL MC_SHOULD_RESPAWN_AT_CHECKPOINT(INT iTeam, BOOL bDebugInfo = TRUE)

	IF bDebugInfo
		PRINTLN("[MMacK][Checkpoint] Checking MC_SHOULD_RESPAWN_AT_CHECKPOINT") 
	ENDIF
	
	IF ( HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
	OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED() )
	AND NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iRestartBitset, FMMC_RESTART_BITSET_RETRY_STRAND_PASS_START_POINT)
		IF bDebugInfo
			PRINTLN("[MMacK][Checkpoint] Passed First Strand, But Not Checkpoint")
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
		IF bDebugInfo
			PRINTLN("[MMacK][Checkpoint] bRetryFourthStartPoint = TRUE")
		ENDIF
		IF NOT IS_VECTOR_ZERO(MC_GET_TEAM_CHECKPOINT_QUICK_RESTART_START_POS(iTeam, 0, 3))
			IF bDebugInfo
				PRINTLN("[MMacK][Checkpoint] MC_GET_TEAM_CHECKPOINT_QUICK_RESTART_START_POS NOT ZERO")
			ENDIF
			RETURN TRUE
		ENDIF
	ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2)
		IF bDebugInfo
			PRINTLN("[MMacK][Checkpoint] bRetryThirdStartPoint = TRUE")
		ENDIF
		IF NOT IS_VECTOR_ZERO(MC_GET_TEAM_CHECKPOINT_QUICK_RESTART_START_POS(iTeam, 0, 2))
			IF bDebugInfo
				PRINTLN("[MMacK][Checkpoint] MC_GET_TEAM_CHECKPOINT_QUICK_RESTART_START_POS NOT ZERO")
			ENDIF
			RETURN TRUE
		ENDIF
	ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
		IF bDebugInfo
			PRINTLN("[MMacK][Checkpoint] bRetrySecondStartPoint = TRUE")
		ENDIF
		IF NOT IS_VECTOR_ZERO(MC_GET_TEAM_CHECKPOINT_QUICK_RESTART_START_POS(iTeam, 0, 1))
			IF bDebugInfo
				PRINTLN("[MMacK][Checkpoint] MC_GET_TEAM_CHECKPOINT_QUICK_RESTART_START_POS NOT ZERO")
			ENDIF
			RETURN TRUE
		ENDIF
	ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
		IF bDebugInfo
			PRINTLN("[MMacK][Checkpoint] bRetryStartPoint = TRUE")
		ENDIF
		IF NOT IS_VECTOR_ZERO(MC_GET_TEAM_CHECKPOINT_QUICK_RESTART_START_POS(iTeam, 0, 0))
			IF bDebugInfo
				PRINTLN("[MMacK][Checkpoint] MC_GET_TEAM_CHECKPOINT_QUICK_RESTART_START_POS NOT ZERO")
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MC_SHOULD_WE_START_FROM_CHECKPOINT()
	
	IF FMMC_CHECKPOINTS_IS_ANY_RETRY_START_POINT_SET()
		PRINTLN("[MMacK][Checkpoint] MC_SHOULD_WE_START_FROM_CHECKPOINT = TRUE")
		RETURN TRUE
	ENDIF
	
	IF ( HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET() OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED() )
	AND IS_BIT_SET(g_TransitionSessionNonResetVars.iRestartBitset, FMMC_RESTART_BITSET_RETRY_STRAND_PASS_START_POINT)
		PRINTLN("[MMacK][Checkpoint] MC_SHOULD_WE_START_FROM_CHECKPOINT (STRAND) = TRUE")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[MMacK][Checkpoint] MC_SHOULD_WE_START_FROM_CHECKPOINT = FALSE")
	RETURN FALSE
ENDFUNC

FUNC INT MC_GET_MISSION_STARTING_CHECKPOINT()
	
	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
		RETURN 3
	ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2)
		RETURN 2
	ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
		RETURN 1
	ELSE
		RETURN 0
	ENDIF
	
ENDFUNC

PROC MC_RESET_MISSION_CHECKPOINT_GLOBALS()
	
	INT i
	FOR i = 0 TO FMMC_MAX_RESTART_CHECKPOINTS - 1
		CLEAR_BIT(g_TransitionSessionNonResetVars.iRestartBitset, i)
		g_TransitionSessionNonResetVars.iRetryMultiRuleTimeStamp[i] = 0
		g_TransitionSessionNonResetVars.iRetryMultiRuleTimeStamp_Checkpoint[i] = 0
	ENDFOR
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Warp Location Functions.
// ##### Description: Process the creation of Mission Controller Peds.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// Reposition based on check points.
FUNC BOOL DOES_CHECKPOINT_FROM_WARP_LOCATION_SETTINGS_ACTIVATE(WARP_LOCATION_SETTINGS &sWarpLocationSettingsData, INT &iIndex  #IF IS_DEBUG_BUILD , INT iEntityType, INT iEntityIndex #ENDIF )
	
	IF NOT IS_THIS_A_QUICK_RESTART_JOB() 
	AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
		RETURN FALSE
	ENDIF
	
	iIndex = sWarpLocationSettingsData.iCheckPointPositionIDStart
	
	IF iIndex = -1
		RETURN FALSE
	ENDIF
	
	IF NOT MC_SHOULD_WE_START_FROM_CHECKPOINT()
		RETURN FALSE
	ENDIF
	
	INT iCheckpointOffset = MC_GET_MISSION_STARTING_CHECKPOINT()
	
	IF NOT IS_BIT_SET(sWarpLocationSettingsData.iWarpLocationBS, ciWARP_LOCATION_BITSET_UseCheckpointReposition_0 + iCheckpointOffset)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(sWarpLocationSettingsData.iWarpLocationBS, ciWARP_LOCATION_BITSET_DontIncrementCheckpointIndex)
		iIndex += iCheckpointOffset
	ENDIF
	
	IF iIndex >= FMMC_MAX_RULE_VECTOR_WARPS		
		PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntityIndex, "] - DOES_CHECKPOINT_FROM_WARP_LOCATION_SETTINGS_ACTIVATE -  Index out of Range!")
		RETURN FALSE
	ENDIF
	
	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntityIndex, "] - DOES_CHECKPOINT_FROM_WARP_LOCATION_SETTINGS_ACTIVATE - Using iIndex: ", iIndex)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ASSIGN_CHECKPOINT_POSITION_FROM_WARP_LOCATION_SETTINGS(WARP_LOCATION_SETTINGS &sWarpLocationSettingsData, VECTOR &vPosition #IF IS_DEBUG_BUILD , INT iEntityType, INT iEntityIndex #ENDIF )	
	
	INT iIndex	
	IF NOT DOES_CHECKPOINT_FROM_WARP_LOCATION_SETTINGS_ACTIVATE(sWarpLocationSettingsData, iIndex #IF IS_DEBUG_BUILD , iEntityType, iEntityIndex #ENDIF )
		RETURN FALSE
	ENDIF
	
	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntityIndex, "] - ASSIGN_CHECKPOINT_POSITION_FROM_WARP_LOCATION_SETTINGS - Using vPosition: ", vPosition)
	
	vPosition = sWarpLocationSettingsData.vPosition[iIndex]
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ASSIGN_CHECKPOINT_HEADING_FROM_WARP_LOCATION_SETTINGS(WARP_LOCATION_SETTINGS &sWarpLocationSettingsData, FLOAT &fHeading #IF IS_DEBUG_BUILD , INT iEntityType, INT iEntityIndex #ENDIF )	
	
	INT iIndex	
	IF NOT DOES_CHECKPOINT_FROM_WARP_LOCATION_SETTINGS_ACTIVATE(sWarpLocationSettingsData, iIndex  #IF IS_DEBUG_BUILD , iEntityType, iEntityIndex #ENDIF )
		RETURN FALSE
	ENDIF
	
	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntityIndex, "] - ASSIGN_CHECKPOINT_HEADING_FROM_WARP_LOCATION_SETTINGS - Using fHeading: ", fHeading)
	
	fHeading = sWarpLocationSettingsData.fHeading[iIndex]
	
	RETURN TRUE
ENDFUNC

// REPOSITION FROM LAST USED WARP POS.
FUNC BOOL SHOULD_REPOSITION_ENTITY_RESPAWN_POSITION_FROM_WARP_LOCATION_JUST_USED(WARP_LOCATION_SETTINGS &sWarpLocationSettings, BOOL bInitialSpawn)	

	// No repositions set.
	IF NOT IS_BIT_SET(sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_UseCurrentWarpPosAsRespawnPos)
		RETURN FALSE
	ENDIF	
	
	// Not respawn spawning.
	IF bInitialSpawn
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC	

FUNC INT GET_WARP_POS_FOR_ENTITY_RESPAWN_POSITION_FROM_WARP_LOCATION_JUST_USED(INT iEntityType, INT iEntityIndex)
	INT iWarpPos = -1
	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		IF iRule >= 0 AND iRule < FMMC_MAX_RULES			
			SWITCH iEntityType
				CASE CREATION_TYPE_PEDS
					iWarpPos = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRulePedWarp[iRule][iEntityIndex]
					BREAKLOOP
				BREAK				
				CASE CREATION_TYPE_VEHICLES
					iWarpPos = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleVehicleWarp[iRule][iEntityIndex]
					BREAKLOOP
				BREAK				
				CASE CREATION_TYPE_OBJECTS	
					iWarpPos = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleObjWarp[iRule][iEntityIndex]
					BREAKLOOP
				BREAK				
				CASE CREATION_TYPE_GOTO_LOC
					iWarpPos = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleLocWarp[iRule][iEntityIndex]
					BREAKLOOP
				BREAK				
				CASE CREATION_TYPE_INTERACTABLE	
					iWarpPos = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleInteractableWarp[iRule][iEntityIndex]
					BREAKLOOP
				BREAK				
				CASE CREATION_TYPE_END_CUTSCENE	
				
				BREAK
			ENDSWITCH
		ENDIF
	ENDFOR	
	RETURN iWarpPos
ENDFUNC

FUNC BOOL REPOSITION_ENTITY_RESPAWN_POSITION_FROM_WARP_LOCATION_JUST_USED(WARP_LOCATION_SETTINGS &sWarpLocationSettingsData, BOOL bInitialSpawn, INT iEntityType, INT iEntityIndex, VECTOR &vPosition)	
	
	IF NOT SHOULD_REPOSITION_ENTITY_RESPAWN_POSITION_FROM_WARP_LOCATION_JUST_USED(sWarpLocationSettingsData, bInitialSpawn)
		RETURN FALSE
	ENDIF
	
	INT iWarpPos = GET_WARP_POS_FOR_ENTITY_RESPAWN_POSITION_FROM_WARP_LOCATION_JUST_USED(iEntityType, iEntityIndex)
	
	IF iWarpPos > -1		
		vPosition = sWarpLocationSettingsData.vPosition[iWarpPos]
		PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntityIndex, "] - REPOSITION_ENTITY_RESPAWN_POSITION_FROM_WARP_LOCATION_JUST_USED - Getting spawn location vPosition: ", vPosition)		
		RETURN TRUE
	ELSE
		PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntityIndex, "] - REPOSITION_ENTITY_RESPAWN_POSITION_FROM_WARP_LOCATION_JUST_USED - iWarpPos is -1. Rule has no assigned warp pos.")
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL REPOSITION_ENTITY_RESPAWN_HEADING_FROM_WARP_LOCATION_JUST_USED(WARP_LOCATION_SETTINGS &sWarpLocationSettingsData, BOOL bInitialSpawn, INT iEntityType, INT iEntityIndex, FLOAT &fHeading)	
	IF NOT SHOULD_REPOSITION_ENTITY_RESPAWN_POSITION_FROM_WARP_LOCATION_JUST_USED(sWarpLocationSettingsData, bInitialSpawn)
		RETURN FALSE
	ENDIF
	
	INT iWarpPos = GET_WARP_POS_FOR_ENTITY_RESPAWN_POSITION_FROM_WARP_LOCATION_JUST_USED(iEntityType, iEntityIndex)
	
	IF iWarpPos > -1		
		fHeading = sWarpLocationSettingsData.fHeading[iWarpPos]
		PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntityIndex, "] - REPOSITION_ENTITY_RESPAWN_HEADING_FROM_WARP_LOCATION_JUST_USED - Getting spawn heading fHeading: ", fHeading)		
		RETURN TRUE
	ELSE
		PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntityIndex, "] - REPOSITION_ENTITY_RESPAWN_HEADING_FROM_WARP_LOCATION_JUST_USED - iWarpPos is -1. Rule has no assigned warp Heading.")
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// Reposition based on random warp locations
FUNC BOOL SHOULD_REPOSITION_ENTITY_BASED_ON_WARP_LOCATIONS_RANDOM_POOL(WARP_LOCATION_SETTINGS &sWarpLocationSettings, BOOL bInitialSpawn)

	// No repositions set.
	IF NOT IS_BIT_SET(sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_UseRandomPoolPosAsInitialSpawn)
	AND NOT IS_BIT_SET(sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_UseRandomPoolPosAsRespawnSpawn)
		RETURN FALSE
	ENDIF
	
	// Not initial spawning.
	IF IS_BIT_SET(sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_UseRandomPoolPosAsInitialSpawn)
	AND NOT bInitialSpawn
		RETURN FALSE
	ENDIF
	
	// Not respawn spawning.
	IF IS_BIT_SET(sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_UseRandomPoolPosAsRespawnSpawn)
	AND bInitialSpawn
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC	

FUNC INT GET_REPOSITION_RANDOM_SPAWN_POOL_INDEX_BASED_ON_WARP_LOCATIONS(WARP_LOCATION_SETTINGS &sWarpLocationSettings, INT iEntityType #IF IS_DEBUG_BUILD , INT iEntity #ENDIF)

	SWITCH iEntityType
		CASE CREATION_TYPE_PEDS
			IF iSpawnPoolPedIndex = -1 // Reset when spawned.
				iSpawnPoolPedIndex = GET_RANDOM_SELECTION_FROM_SPAWN_POOL(sWarpLocationSettings.vPosition #IF IS_DEBUG_BUILD , iEntity, CREATION_TYPE_PEDS #ENDIF )
			ENDIF
			RETURN iSpawnPoolPedIndex
		BREAK
		
		CASE CREATION_TYPE_VEHICLES
			IF iSpawnPoolVehIndex = -1 // Reset when spawned.
				iSpawnPoolVehIndex = GET_RANDOM_SELECTION_FROM_SPAWN_POOL(sWarpLocationSettings.vPosition #IF IS_DEBUG_BUILD , iEntity, CREATION_TYPE_VEHICLES #ENDIF )
			ENDIF				
			RETURN iSpawnPoolVehIndex
		BREAK
		
		CASE CREATION_TYPE_OBJECTS		
			IF iSpawnPoolObjIndex = -1 // Reset when spawned.
				iSpawnPoolObjIndex = GET_RANDOM_SELECTION_FROM_SPAWN_POOL(sWarpLocationSettings.vPosition #IF IS_DEBUG_BUILD , iEntity, CREATION_TYPE_OBJECTS #ENDIF )
			ENDIF
			RETURN iSpawnPoolObjIndex
		BREAK
		
		CASE CREATION_TYPE_GOTO_LOC
			iSpawnPoolLocIndex = GET_RANDOM_SELECTION_FROM_SPAWN_POOL(sWarpLocationSettings.vPosition #IF IS_DEBUG_BUILD , iEntity, CREATION_TYPE_GOTO_LOC #ENDIF )		
			RETURN iSpawnPoolLocIndex
		BREAK
		
		CASE CREATION_TYPE_INTERACTABLE	
			IF iSpawnPoolInteractableIndex = -1 // Reset when spawned.
				iSpawnPoolInteractableIndex = GET_RANDOM_SELECTION_FROM_SPAWN_POOL(sWarpLocationSettings.vPosition #IF IS_DEBUG_BUILD , iEntity, CREATION_TYPE_INTERACTABLE #ENDIF )
			ENDIF
			RETURN iSpawnPoolInteractableIndex	
		BREAK
		
		CASE CREATION_TYPE_END_CUTSCENE	
		
		BREAK
	ENDSWITCH

	RETURN -1
ENDFUNC

FUNC BOOL REPOSITION_COORD_BASED_ON_WARP_LOCATIONS_RANDOM_POOL(WARP_LOCATION_SETTINGS &sWarpLocationSettings, INT iEntityType, BOOL bInitialSpawn, VECTOR &vPos #IF IS_DEBUG_BUILD , INT iEntity #ENDIF)
	
	IF NOT SHOULD_REPOSITION_ENTITY_BASED_ON_WARP_LOCATIONS_RANDOM_POOL(sWarpLocationSettings, bInitialSpawn)
		RETURN FALSE
	ENDIF
	
	INT iSpawnPoolIndex = GET_REPOSITION_RANDOM_SPAWN_POOL_INDEX_BASED_ON_WARP_LOCATIONS(sWarpLocationSettings, iEntityType #IF IS_DEBUG_BUILD , iEntity #ENDIF )	
	IF iSpawnPoolIndex != -1
		vPos = sWarpLocationSettings.vPosition[iSpawnPoolIndex]
		PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntity, "] REPOSITION_COORD_BASED_ON_WARP_LOCATIONS_RANDOM_POOL - Grabbing random position from pool. iSpawnPoolIndex: ", iSpawnPoolIndex, " vPos: ", vPos)
		RETURN TRUE	
	ENDIF
	
	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntity, "] REPOSITION_COORD_BASED_ON_WARP_LOCATIONS_RANDOM_POOL - iSpawnPoolIndex was -1.")
	
	RETURN FALSE
ENDFUNC

FUNC BOOL REPOSITION_HEADING_BASED_ON_WARP_LOCATIONS_RANDOM_POOL(WARP_LOCATION_SETTINGS &sWarpLocationSettings, INT iEntityType, BOOL bInitialSpawn, FLOAT &fHead #IF IS_DEBUG_BUILD , INT iEntity #ENDIF)
	
	IF NOT SHOULD_REPOSITION_ENTITY_BASED_ON_WARP_LOCATIONS_RANDOM_POOL(sWarpLocationSettings, bInitialSpawn)
		RETURN FALSE
	ENDIF
	
	INT iSpawnPoolIndex = GET_REPOSITION_RANDOM_SPAWN_POOL_INDEX_BASED_ON_WARP_LOCATIONS(sWarpLocationSettings, iEntityType #IF IS_DEBUG_BUILD , iEntity #ENDIF )	
	IF iSpawnPoolIndex != -1
		fHead = sWarpLocationSettings.fHeading[iSpawnPoolIndex]				
		PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntity, "] REPOSITION_HEADING_BASED_ON_WARP_LOCATIONS_RANDOM_POOL - Grabbing random heading from pool. iSpawnPoolIndex: ", iSpawnPoolIndex, " fHead: ", fHead)
		RETURN TRUE
	ENDIF
	
	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntity, "] REPOSITION_HEADING_BASED_ON_WARP_LOCATIONS_RANDOM_POOL - iSpawnPoolIndex was -1.")
	
	RETURN FALSE
ENDFUNC

FUNC BOOL REPOSITION_COORD_BASED_ON_FORCED_WARP_LOCATION_INDEX_ON_RESPAWN(WARP_LOCATION_SETTINGS &sWarpLocationSettings, INT iEntityType, BOOL bInitialSpawn, VECTOR &vPos, INT iEntity)
	
	// This only works for "respawns". Note that a "Respawn" can be set up for an entities initial spawn using spawning flags.
	IF bInitialSpawn
		RETURN FALSE
	ENDIF
	
	INT iSpawnPoolIndex = RETURN_INT_CUSTOM_VARIABLE_VALUE(sWarpLocationSettings.iForcedRespawnLocation, iEntityType, iEntity)	
	
	IF iSpawnPoolIndex != -1
		vPos = sWarpLocationSettings.vPosition[iSpawnPoolIndex]
		PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntity, "] REPOSITION_COORD_BASED_ON_FORCED_WARP_LOCATION_INDEX_ON_RESPAWN - Grabbing random position from pool. iSpawnPoolIndex: ", iSpawnPoolIndex, " vPos: ", vPos)
		RETURN TRUE	
	ENDIF
	
	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntity, "] REPOSITION_COORD_BASED_ON_FORCED_WARP_LOCATION_INDEX_ON_RESPAWN - iSpawnPoolIndex was -1.")
	
	RETURN FALSE
ENDFUNC

FUNC BOOL REPOSITION_HEADING_BASED_ON_FORCED_WARP_LOCATION_INDEX_ON_RESPAWN(WARP_LOCATION_SETTINGS &sWarpLocationSettings, INT iEntityType, BOOL bInitialSpawn, FLOAT &fHead, INT iEntity)
		
	// This only works for "respawns". Note that a "Respawn" can be set up for an entities initial spawn using spawning flags.
	IF bInitialSpawn
		RETURN FALSE
	ENDIF
	
	INT iSpawnPoolIndex = RETURN_INT_CUSTOM_VARIABLE_VALUE(sWarpLocationSettings.iForcedRespawnLocation, iEntityType, iEntity)	
	
	IF iSpawnPoolIndex != -1
		fHead = sWarpLocationSettings.fHeading[iSpawnPoolIndex]				
		PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntity, "] REPOSITION_HEADING_BASED_ON_FORCED_WARP_LOCATION_INDEX_ON_RESPAWN - Grabbing random heading from pool. iSpawnPoolIndex: ", iSpawnPoolIndex, " fHead: ", fHead)
		RETURN TRUE
	ENDIF
	
	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntity, "] REPOSITION_HEADING_BASED_ON_FORCED_WARP_LOCATION_INDEX_ON_RESPAWN - iSpawnPoolIndex was -1.")
	
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Shared Creation Utility Functions 
// ##### Description: Process the creation of Mission Controller Peds.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC CLEANUP_GENERAL_RESPAWNING_DATA(MODEL_NAMES mn)
	
	PRINTLN("[MCSpawning] - CLEANUP_GENERAL_RESPAWNING_DATA - Cleaning up Respawning Data.")
	
	IF mn != DUMMY_MODEL_FOR_SCRIPT
		SET_MODEL_AS_NO_LONGER_NEEDED(mn)
	ENDIF
	
	IF NETWORK_ENTITY_AREA_DOES_EXIST(MC_serverBD.iSpawnArea)
		NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
	ENDIF
	
	iEntityRespawnRandomSelected = 0	
	MC_serverBD.iSpawnArea = -1
	RESET_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)	
	SpawnInterior = NULL
	vEntityRespawnLoc = <<0, 0, 0>>
	
ENDPROC

PROC MC_SET_ID_INT_DECOR_ON_ENTITY(ENTITY_INDEX eiPassed, INT iMC_EntityID, BOOL bChasePed = FALSE)

	IF bChasePed
		IF DECOR_IS_REGISTERED_AS_TYPE("MC_ChasePedID", DECOR_TYPE_INT)
			IF NOT DECOR_EXIST_ON(eiPassed, "MC_ChasePedID")
				DECOR_SET_INT(eiPassed, "MC_ChasePedID", iMC_EntityID)
				PRINTLN("[RCC MISSION] MC_SET_ID_INT_DECOR_ON_ENTITY: ", iMC_EntityID)
			ENDIF
		ENDIF	
	ELSE
		IF DECOR_IS_REGISTERED_AS_TYPE("MC_EntityID", DECOR_TYPE_INT)
			IF NOT DECOR_EXIST_ON(eiPassed, "MC_EntityID")
				DECOR_SET_INT(eiPassed, "MC_EntityID", iMC_EntityID)
				PRINTLN("[RCC MISSION] MC_SET_ID_INT_DECOR_ON_ENTITY: ", iMC_EntityID)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC INT GET_MC_TEAM_STARTING_RULE(INT iTeam)
	
	INT iStartingRule = 0
	
	IF IS_THIS_A_QUICK_RESTART_JOB()
		IF MC_SHOULD_RESPAWN_AT_CHECKPOINT(iTeam, FALSE)
		OR (MC_SHOULD_WE_START_FROM_CHECKPOINT() AND SHOULD_USE_CUSTOM_START_POINT_CHECKPOINTS())
			iStartingRule = g_FMMC_STRUCT.iRestartRule[iTeam][MC_GET_MISSION_STARTING_CHECKPOINT()]
		ENDIF
	ENDIF
	
	PRINTLN("[GET_MC_TEAM_STARTING_RULE] - Returning iRule: ", iStartingRule)
	
	RETURN iStartingRule
	
ENDFUNC


// [LM] helper function for finding a valid random position/heading spawn out of a pool.
// Can easily modify this function to include more features such an options for Smart Spawning, for example, selecting the position furthest from players or out of line of sight etc.
// FMMC2020 Potential port Function
// GET_RANDOM_SELECTION_FROM_SPAWN_POOL
// I think the above comments are meant ot be somewhere else?

// This function uses an INT that is actually one less than the number of players that need to be playing.
FUNC BOOL MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_ENTITY(INT iNumberOfPlayersRequired, INT iStartingPlayers)
	 
	IF iNumberOfPlayersRequired > 0 
		IF iStartingPlayers < (iNumberOfPlayersRequired + 1)
			PRINTLN("[MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_ENTITY] Not enough players to spawn entity. Min players = ", iNumberOfPlayersRequired + 1, " total players = ", iStartingPlayers)
			RETURN FALSE
		ENDIF
	ELIF iNumberOfPlayersRequired = -1
		IF iStartingPlayers != 1
			PRINTLN("[MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_ENTITY] Too many players to spawn entity. Players Requires = 1, total players = ", iStartingPlayers)
			RETURN FALSE
		ENDIF		
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL MC_WILL_CHECKPOINTS_STOP_THIS_SPAWN(INT iEntity, BOOL bDontSpawnIfCheckpoint1, BOOL bDontSpawnIfCheckpoint2 = FALSE, BOOL bDontSpawnIfCheckpoint3 = FALSE, BOOL bDontSpawnIfCheckpoint4 = FALSE, BOOL bBlockSpawnIfNotACheckpoint = FALSE)
	
	IF bBlockSpawnIfNotACheckpoint
		IF NOT FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
		AND NOT FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
		AND NOT FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2)
		AND NOT FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
			PRINTLN("MC_WILL_CHECKPOINTS_STOP_THIS_SPAWN - iEntity: ", iEntity, " - Returning True bBlockSpawnIfNotACheckpoint, due to checkpoint us not respawning at any checkpoint.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF bDontSpawnIfCheckpoint1
	OR bDontSpawnIfCheckpoint2
	OR bDontSpawnIfCheckpoint3
	OR bDontSpawnIfCheckpoint4
		
		IF IS_THIS_A_QUICK_RESTART_JOB()
			
			IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
				PRINTLN("MC_WILL_CHECKPOINTS_STOP_THIS_SPAWN - iEntity: ", iEntity, " - Returning ", GET_STRING_FROM_BOOL(bDontSpawnIfCheckpoint4), " due to checkpoint 4")
				RETURN bDontSpawnIfCheckpoint4
			ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2)
				PRINTLN("MC_WILL_CHECKPOINTS_STOP_THIS_SPAWN - iEntity: ", iEntity, " - Returning ", GET_STRING_FROM_BOOL(bDontSpawnIfCheckpoint3), " due to checkpoint 3")
				RETURN bDontSpawnIfCheckpoint3
			ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
				PRINTLN("MC_WILL_CHECKPOINTS_STOP_THIS_SPAWN - iEntity: ", iEntity, " - Returning ", GET_STRING_FROM_BOOL(bDontSpawnIfCheckpoint2), " due to checkpoint 2")
				RETURN bDontSpawnIfCheckpoint2
			ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
				PRINTLN("MC_WILL_CHECKPOINTS_STOP_THIS_SPAWN - iEntity: ", iEntity, " - Returning ", GET_STRING_FROM_BOOL(bDontSpawnIfCheckpoint1), " due to checkpoint 1")
				RETURN bDontSpawnIfCheckpoint1
			ENDIF
			
		ENDIF
	ENDIF
	
	PRINTLN("MC_WILL_CHECKPOINTS_STOP_THIS_SPAWN - iEntity: ", iEntity, " - Returning FALSE")
	RETURN FALSE
	
ENDFUNC

FUNC BOOL MC_SHOULD_ENTITY_SPAWN(INT iEntityType, INT iEntityIndex, INT iSpawnTeam, INT iSpawnRule, INT iAssociatedSpawn, BOOL bLaterNotStart, BOOL bCheckpoints, INT iScoreRequired, INT iAlwaysForceSpawnOnRule)
	
	BOOL bSpawn = FALSE
	BOOL bSpawnLater = FALSE
	
	INT iTeamRule = 0
	
	IF bCheckpoints
	AND iSpawnTeam != -1
	AND GET_MC_TEAM_STARTING_RULE(iSpawnTeam) > 0
		iTeamRule = GET_MC_TEAM_STARTING_RULE(iSpawnTeam)
	ENDIF
	
	iScoreRequired = RETURN_INT_CUSTOM_VARIABLE_VALUE(iScoreRequired, iEntityType, iEntityIndex)
	
	SWITCH iAssociatedSpawn
		
		CASE ciOBJECTIVE_SPAWN_LIMIT_OFF
			bSpawn = TRUE
		BREAK
		
		CASE ciOBJECTIVE_SPAWN_LIMIT_ON
			IF iSpawnRule <= iTeamRule
				bSpawn = TRUE
			ELSE
				bSpawnLater = TRUE
			ENDIF
		BREAK
		
		CASE ciOBJECTIVE_SPAWN_LIMIT_ON_COLLECTION
			IF iTeamRule > 0
			AND iTeamRule > iSpawnRule
				bSpawn = TRUE
			ELSE
				bSpawnLater = TRUE
			ENDIF
		BREAK
		
		CASE ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamRule > 0
				IF iTeamRule = iSpawnRule
					bSpawn = TRUE
				ELIF iTeamRule < iSpawnRule
					bSpawnLater = TRUE
				ENDIF
			ELSE
				IF iSpawnRule <= 0
					bSpawn = TRUE
				ELSE
					bSpawnLater = TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE ciOBJECTIVE_SPAWN_LIMIT_OnImminentAggroFail
			IF iTeamRule > 0
				IF iTeamRule < iSpawnRule
					bSpawnLater = TRUE
				ENDIF
			ELSE
				bSpawnLater = TRUE
			ENDIF
		BREAK
		
		CASE ciOBJECTIVE_SPAWN_LIMIT_ON_QUICK_RESTART_0
			IF IS_THIS_A_QUICK_RESTART_JOB()
			OR IS_CORONA_INITIALISING_A_QUICK_RESTART()
				IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
					bSpawn = TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE ciOBJECTIVE_SPAWN_LIMIT_ON_QUICK_RESTART_1
			IF IS_THIS_A_QUICK_RESTART_JOB()
			OR IS_CORONA_INITIALISING_A_QUICK_RESTART()
				IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
					bSpawn = TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE ciOBJECTIVE_SPAWN_LIMIT_ON_QUICK_RESTART_2
			IF IS_THIS_A_QUICK_RESTART_JOB()
			OR IS_CORONA_INITIALISING_A_QUICK_RESTART()
				IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2)
					bSpawn = TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE ciOBJECTIVE_SPAWN_LIMIT_ON_QUICK_RESTART_3
			IF IS_THIS_A_QUICK_RESTART_JOB()
			OR IS_CORONA_INITIALISING_A_QUICK_RESTART()
				IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
					bSpawn = TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE ciOBJECTIVE_SPAWN_LIMIT_ON_SCORE_REACHED
			IF iScoreRequired > 0
				bSpawnLater = TRUE
			ENDIF
			
			IF iTeamRule > -1
			AND iAlwaysForceSpawnOnRule > -1
			AND iAlwaysForceSpawnOnRule <= iTeamRule
				PRINTLN("[RCC MISSION][MC_SHOULD_ENTITY_SPAWN] - Using Special Override to spawn on this rule. iTeamRule = ", iTeamRule, " iSpawnRule = ", iSpawnRule, " iAlwaysForceSpawnOnRule = ", iAlwaysForceSpawnOnRule)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE ciOBJECTIVE_SPAWN_LIMIT_ON_NUMBER_OF_KILLS
			IF iScoreRequired > 0
				bSpawnLater = TRUE
			ENDIF
			
			IF iTeamRule > -1
			AND iAlwaysForceSpawnOnRule > -1
			AND iAlwaysForceSpawnOnRule <= iTeamRule
				PRINTLN("[RCC MISSION][MC_SHOULD_ENTITY_SPAWN] - Using Special Override to spawn on this rule. iTeamRule = ", iTeamRule, " iSpawnRule = ", iSpawnRule, " iAlwaysForceSpawnOnRule = ", iAlwaysForceSpawnOnRule)
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
		
	IF NOT bLaterNotStart
		RETURN bSpawn
	ELSE
		RETURN bSpawnLater
	ENDIF
	
ENDFUNC

//Helper functions for MC_SHOULD_ENTITY_SPAWN
FUNC BOOL MC_SHOULD_ENTITY_SPAWN_AT_START(INT iEntityType, INT iEntityIndex, INT iSpawnTeam, INT iSpawnRule, INT iAssociatedSpawn, BOOL bCheckpoints, INT iScoreRequired = 0, INT iAlwaysForceSpawnOnRule = -1)
	RETURN MC_SHOULD_ENTITY_SPAWN(iEntityType, iEntityIndex, iSpawnTeam, iSpawnRule, iAssociatedSpawn, FALSE, bCheckpoints, iScoreRequired, iAlwaysForceSpawnOnRule)
ENDFUNC
FUNC BOOL MC_SHOULD_ENTITY_SPAWN_LATER(INT iEntityType, INT iEntityIndex, INT iSpawnTeam, INT iSpawnRule, INT iAssociatedSpawn, BOOL bCheckpoints, INT iScoreRequired = 0, INT iAlwaysForceSpawnOnRule = -1)
	RETURN MC_SHOULD_ENTITY_SPAWN(iEntityType, iEntityIndex, iSpawnTeam, iSpawnRule, iAssociatedSpawn, TRUE, bCheckpoints, iScoreRequired, iAlwaysForceSpawnOnRule)
ENDFUNC

/// PURPOSE: Checks if the entity will cleanup if we spawned it at the start of the mission
///    
/// PARAMS:
///    iCleanupTeam - The team which cleanup of this entity is based off (from 0 to 3 / FMMC_MAX_TEAMS), can be -1 for no cleanup
///    iCleanupRule - The rule which the entity will clean up on when the team reaches it
///    bCleanupOnMidpoint - If the entity only cleans up once the midpoint is reached
///    
/// RETURNS: TRUE if the entity is going to clean up as soon as it gets spawned, or FALSE if it will stick around
///    
FUNC BOOL MC_WILL_ENTITY_CLEANUP_IMMEDIATELY(INT iCleanupTeam, INT iCleanupRule, BOOL bCleanupOnMidpoint)
	
	BOOL bCleanup = FALSE
	
	IF iCleanupRule != -1
	AND iCleanupTeam != -1
	AND GET_MC_TEAM_STARTING_RULE(iCleanupTeam) > 0
		
		INT iTeamRule = GET_MC_TEAM_STARTING_RULE(iCleanupTeam)
		
		INT iRuleToBeat = iCleanupRule
		
		IF bCleanupOnMidpoint
			iRuleToBeat++
		ENDIF
		
		IF iTeamRule >= iRuleToBeat
			//This entity will clean up immediately on spawning, don't do it!
			bCleanup = TRUE
		ENDIF
		
	ENDIF
	
	RETURN bCleanup
	
ENDFUNC

FUNC BOOL MC_IS_ENTITY_SPAWN_TRIGGERED_BY_ZONE_ENTRY(INT iZone)
	IF iZone > -1
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC VECTOR MC_MC_GET_SPAWN_NEAR_LOCATION_VECTOR(INT iLoc)
	
	VECTOR vLoc

	VECTOR vPlacedGotoPosition = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vloc[0]
	
	IF IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].vLoc1)
	OR IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].vLoc2)
		vLoc = vPlacedGotoPosition
	ELSE
	    vLoc = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vLoc1 + g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vLoc2
        vLoc *= << 0.5, 0.5, 0.5 >>
	ENDIF
	
	RETURN vLoc
	
ENDFUNC

FUNC VECTOR MC_GET_SPAWN_NEAR_LOCATION(INT iObj, INT iSpawnNearType = ciRULE_TYPE_NONE, INT iSpawnNearEntityID = -1)
	
	VECTOR vEntityLoc
	
	SWITCH iSpawnNearType
		CASE ciRULE_TYPE_PED
			IF iSpawnNearEntityID < FMMC_MAX_PEDS
				vEntityLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iSpawnNearEntityID].vPos
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_VEHICLE
			IF iSpawnNearEntityID < FMMC_MAX_VEHICLES
				vEntityLoc = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iSpawnNearEntityID].vPos
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_OBJECT
			IF iSpawnNearEntityID < FMMC_MAX_NUM_OBJECTS
				vEntityLoc = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iSpawnNearEntityID].vPos
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_GOTO
			IF iSpawnNearEntityID < FMMC_MAX_RULES
				vEntityLoc = MC_MC_GET_SPAWN_NEAR_LOCATION_VECTOR(iSpawnNearEntityID)
			ENDIF
		BREAK
		
		DEFAULT
			vEntityLoc = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vPos
		BREAK
		
	ENDSWITCH
	
	RETURN vEntityLoc
	
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_STAY_FROZEN_FOR_MIGRATION(INT iVeh)

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_FREEZE_POS)
		PRINTLN("[Vehicle ",iVeh,"]SHOULD_VEHICLE_STAY_FROZEN_FOR_MIGRATION - ciFMMC_VEHICLE4_VEHICLE_FREEZE_POS is set")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD_4.iVehicleImmovableBitset, iVeh)
		PRINTLN("[Vehicle ",iVeh,"]SHOULD_VEHICLE_STAY_FROZEN_FOR_MIGRATION - MC_serverBD_4.iVehicleImmovableBitset is set for this vehicle")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	

ENDFUNC

PROC SET_ALL_MISSION_VEHICLES_ABLE_TO_MIGRATE(FMMC_MISSION_SERVER_DATA_STRUCT &sSDSpassed, BOOL bSpecialCaseOnly = FALSE)

	PRINTLN("[RCC MISSION] SET_ALL_MISSION_VEHICLES_ABLE_TO_MIGRATE - bSpecialCaseOnly: ", bSpecialCaseOnly)

	INT i
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles i
		
		IF bSpecialCaseOnly
			IF NOT IS_BIT_SET(iVehCloneFromLobbyBS, i)
				RELOOP
			ENDIF
			
			PRINTLN("[RCC MISSION] SET_ALL_MISSION_VEHICLES_ABLE_TO_MIGRATE - Passed special case checks")
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(sSDSpassed.niVehicle[i])	
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSDSpassed.niVehicle[i])
				SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE(sSDSpassed.niVehicle[i], FALSE, SHOULD_VEHICLE_STAY_FROZEN_FOR_MIGRATION(i))
				SET_ENTITY_VISIBLE( NET_TO_VEH(sSDSpassed.niVehicle[i]), TRUE )
				PRINTLN("[RCC MISSION] SET_ALL_MISSION_VEHICLES_ABLE_TO_MIGRATE - setting vehicle as unfrozen and migratable " ,i )
				
				IF GET_ENTITY_MODEL(NET_TO_VEH(sSDSpassed.niVehicle[i])) = TRAILERLARGE
					IF NOT IS_ENTITY_DEAD(NET_TO_VEH(sSDSpassed.niVehicle[i]))
						SET_TRAILER_LEGS_RAISED(NET_TO_VEH(sSDSpassed.niVehicle[i]))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC SET_ALL_MISSION_ENTITIES_UNFROZEN_AND_ABLE_TO_MIGRATE(FMMC_MISSION_SERVER_DATA_STRUCT &sSDSpassed)

	INT i

	SET_ALL_MISSION_VEHICLES_ABLE_TO_MIGRATE(sSDSpassed)
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfObjects i
		IF IS_OBJECT_A_MINIGAME_OBJECT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i])
		AND SHOULD_PLACED_OBJECT_BE_PORTABLE(i, FALSE)
			// these ones want to stay frozen.
			IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(i))	
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GET_OBJECT_NET_ID(i))
					SET_NETWORK_ID_CAN_MIGRATE(GET_OBJECT_NET_ID(i), TRUE)
				ENDIF
			ENDIF
		ELSE
			IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(i))	
				BOOL bDontUnFreeze = IS_ENTITY_MODEL_SET_AS_FIXED(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn) OR IS_MODEL_A_SEA_MINE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn) OR IS_THIS_MODEL_A_FUSEBOX(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn)
				
				IF IS_OBJECT_A_PICKUP(NET_TO_OBJ(GET_OBJECT_NET_ID(i)))
					bDontUnFreeze = TRUE
					PRINTLN("Setting bDontUnFreeze to TRUE because it's a pickup")
				ENDIF
				
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GET_OBJECT_NET_ID(i))
					SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE(GET_OBJECT_NET_ID(i), FALSE, bDontUnFreeze)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps i
		IF NETWORK_DOES_NETWORK_ID_EXIST(GET_DYNOPROP_NET_ID(i))	
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GET_DYNOPROP_NET_ID(i))
				IF SHOULD_THIS_MODEL_BE_FROZEN(GET_DYNOPROP_MODEL_TO_USE(i))
				OR IS_ENTITY_MODEL_SET_AS_FIXED(GET_DYNOPROP_MODEL_TO_USE(i))
				OR IS_THIS_MODEL_A_TRAP_DYNO_PROP(GET_DYNOPROP_MODEL_TO_USE(i))
					SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE(GET_DYNOPROP_NET_ID(i), FALSE, TRUE)
				ELSE
					SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE(GET_DYNOPROP_NET_ID(i), FALSE, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables i
		IF NETWORK_DOES_NETWORK_ID_EXIST(GET_INTERACTABLE_NET_ID(i))	
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GET_INTERACTABLE_NET_ID(i))
				IF SHOULD_THIS_MODEL_BE_FROZEN(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].mnInteractable_Model, FALSE)
				OR IS_ENTITY_MODEL_SET_AS_FIXED(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].mnInteractable_Model)
					PRINTLN("[Interactables][Interactable ", i, "] SET_ALL_MISSION_ENTITIES_UNFROZEN_AND_ABLE_TO_MIGRATE | Letting Interactable migrate!")
					SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE(GET_INTERACTABLE_NET_ID(i), FALSE, TRUE)
				ELSE
					PRINTLN("[Interactables][Interactable ", i, "] SET_ALL_MISSION_ENTITIES_UNFROZEN_AND_ABLE_TO_MIGRATE | Unfreezing Interactable and letting it migrate!")
					SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE(GET_INTERACTABLE_NET_ID(i), FALSE, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds i
		IF NETWORK_DOES_NETWORK_ID_EXIST(sSDSpassed.niPed[i])	
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSDSpassed.niPed[i])
				BOOL bDontUnFreeze = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFive, ciPED_BSFive_StartDead)
				SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE(sSDSpassed.niPed[i],FALSE, bDontUnFreeze)
				
				IF FMMC_IS_LONG_BIT_SET(iPedStartedInCoverBitset, i)
					SET_NETWORK_ID_CAN_MIGRATE(sSDSpassed.niPed[i], FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfTrains i
		IF NETWORK_DOES_NETWORK_ID_EXIST(sSDSpassed.niTrain[i])	
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSDSpassed.niTrain[i])
				SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE(sSDSpassed.niTrain[i], FALSE, TRUE)
				PRINTLN("[RCC MISSION] SET_ALL_MISSION_ENTITIES_UNFROZEN_AND_ABLE_TO_MIGRATE - setting train as migratable ", i)
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iPlacedTrain_StopMovingOnRuleTeam = -1
				OR NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iPlacedTrain_StopMovingOnRuleBS, 0)
					VEHICLE_INDEX viTrain = NET_TO_VEH(sSDSpassed.niTrain[i])
					IF IS_ENTITY_ALIVE(viTrain)
						PRINTLN("[RCC MISSION] SET_ALL_MISSION_ENTITIES_UNFROZEN_AND_ABLE_TO_MIGRATE - setting train moving ", i)
						SET_TRAIN_CRUISE_SPEED(viTrain, g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].fPlacedTrainCruiseSpeed)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN( "**************[RCC MISSION] SET_ALL_MISSION_ENTITIES_UNFROZEN_AND_ABLE_TO_MIGRATE*************")
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Peds (Utility and Helper Functions) 
// ##### Description: Process the creation of Mission Controller Peds.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC TASK_COMBAT_PED_FLAGS MC_GET_TASK_COMBAT_PED_FLAGS(PED_INDEX pedIndex)
			
	IF NOT IS_PED_INJURED(pedIndex)
		WEAPON_TYPE wtWeap	
		IF GET_CURRENT_PED_WEAPON(pedIndex, wtWeap)
			// Prevents incorrect aim intro anim being used on MG, seen in: url:bugstar:7784670 
			IF wtWeap = WEAPONTYPE_MG
				RETURN COMBAT_PED_DISABLE_AIM_INTRO
			ENDIF
		ENDIF	
	ENDIF
			
	RETURN COMBAT_PED_NONE
ENDFUNC

///PURPOSE: this function checks to see if a mission critical ped is moving on
///    to a kill rule, and if so, it sets a bit that stops this team for failing if it's destroyed
PROC SET_PED_CAN_BE_KILLED_ON_THIS_RULE(INT iPedIndex, INT iTeam)
	
	IF iPedIndex > -1
		

		PRINTLN("[RCC MISSION] SET_PED_CAN_BE_KILLED_ON_THIS_RULE 1 (iPed: ", iPedIndex, ")")
		IF MC_serverBD_4.iPedRule[iPedIndex][iteam] = FMMC_OBJECTIVE_LOGIC_KILL
			IF iteam = 0
				PRINTLN("[RCC MISSION] SET_PED_CAN_BE_KILLED_ON_THIS_RULE 2")
				SET_BIT(MC_serverBD.iPedteamFailBitset[iPedIndex], SBBOOL1_TEAM0_NEEDS_KILL)
			ELIF iteam = 1
				SET_BIT(MC_serverBD.iPedteamFailBitset[iPedIndex], SBBOOL1_TEAM1_NEEDS_KILL)
			ELIF iteam = 2
				SET_BIT(MC_serverBD.iPedteamFailBitset[iPedIndex], SBBOOL1_TEAM2_NEEDS_KILL)
			ELIF iteam = 3
				SET_BIT(MC_serverBD.iPedteamFailBitset[iPedIndex], SBBOOL1_TEAM3_NEEDS_KILL)
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL IS_PED_A_PLAYER_ABILITY_PED(INT iPedNumber)
	
	INT i
	FOR i = 0 TO ciAMBUSH_ABILITY_PEDS - 1
		IF sLocalPlayerAbilities.sAmbushData.iAmbushPedIndexes[i] = iPedNumber
			// This is an Ambush ped!
			RETURN TRUE
		ENDIF
	ENDFOR
	
	FOR i = 0 TO ciRIDE_ALONG_ABILITY_PEDS - 1
		IF sLocalPlayerAbilities.sRideAlongData.iRideAlongPedIndexes[i] = iPedNumber
			// This is a Ride Along ped!
			RETURN TRUE
		ENDIF
	ENDFOR
	
	FOR i = 0 TO ciDISTRACTION_ABILITY_VEHICLES - 1
		IF sLocalPlayerAbilities.sDistractionData.iDistractionPedIndexes[i] = iPedNumber
			// This is a Distraction ped!
			RETURN TRUE
		ENDIF
	ENDFOR
	
	IF g_FMMC_STRUCT.sPlayerAbilities.sVehicleRepair.iMechanicPedIndex = iPedNumber
		// This is the Vehicle Repair ped!
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC MC_REMOVE_FMMC_PED_DEFENSIVE_AREA(PED_INDEX piPassed, INT iPedNumber, BOOL bPrimary, BOOL bSecondary)		
	UNUSED_PARAMETER(iPedNumber)
	IF bPrimary
		IF IS_PED_DEFENSIVE_AREA_ACTIVE(piPassed, FALSE)
			REMOVE_PED_DEFENSIVE_AREA(piPassed, FALSE)
			SET_PED_CONFIG_FLAG(piPassed, PCF_AvoidTearGas, TRUE) // Default is on.
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_REMOVE_FMMC_PED_DEFENSIVE_AREA - Removing defensive area.")
			DEBUG_PRINTCALLSTACK()
		ENDIF
	ENDIF
	IF bSecondary
		IF IS_PED_DEFENSIVE_AREA_ACTIVE(piPassed, TRUE)
			REMOVE_PED_DEFENSIVE_AREA(piPassed, TRUE)
			SET_PED_CONFIG_FLAG(piPassed, PCF_AvoidTearGas, TRUE) // Default is on.
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_REMOVE_FMMC_PED_DEFENSIVE_AREA - Removing secondary defensive area.")			
			DEBUG_PRINTCALLSTACK()			
		ENDIF		
	ENDIF
ENDPROC 

PROC MC_SET_UP_FMMC_PED_DEFENSIVE_AREA(PED_INDEX piPassed, INT iPedNumber, VECTOR vDefensiveAreaCentre, BOOL bIgnoreRange = FALSE)
	
	WEAPON_TYPE wtEquipped
	GET_CURRENT_PED_WEAPON(piPassed, wtEquipped, FALSE)
	
	IF (NOT IS_PED_IN_ANY_VEHICLE(piPassed)) OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEleven, ciPED_BSEleven_CanHaveDefensiveAreaInVehicle)
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].fRange != cfMAX_PATROL_RANGE OR bIgnoreRange
	AND (GET_PED_WEAPON_TYPE_FROM_CREATOR_OPTION(iPedNumber) != WEAPONTYPE_UNARMED OR (wtEquipped != WEAPONTYPE_UNARMED AND wtEquipped != WEAPONTYPE_INVALID))
		PRINTLN("[RCC MISSION] MC_SET_UP_FMMC_PED_DEFENSIVE_AREA - ped ", iPedNumber, " called w vDefensiveAreaCentre: ",vDefensiveAreaCentre, ", fRange ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].fRange)
		
		FLOAT fTempRange = 0.75
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].fRange > 0.75
			fTempRange = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].fRange
		ENDIF
		
		SET_PED_SPHERE_DEFENSIVE_AREA(piPassed, vDefensiveAreaCentre, fTempRange)
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEleven, ciPED_BSEleven_CanHaveDefensiveAreaInVehicle)
			PRINTLN("[RCC MISSION] MC_SET_UP_FMMC_PED_DEFENSIVE_AREA - ped ", iPedNumber, " set PCF_StayInDefensiveAreaWhenInVehicle")
			SET_PED_CONFIG_FLAG(piPassed, PCF_StayInDefensiveAreaWhenInVehicle, TRUE)
		ENDIF
		
		SET_PED_CONFIG_FLAG(piPassed, PCF_AvoidTearGas, FALSE) // Stay in the defensive area.
	ENDIF
	
ENDPROC

FUNC BOOL CREATE_PLAYER_ABILITY_PED_RELATIONSHIP_GROUP()

	IF DOES_RELATIONSHIP_GROUP_EXIST(relPlayerAbilityPed)
		// Group has already been initialised!
		RETURN TRUE
	ENDIF
	
	PRINTLN("[Peds] CREATE_PLAYER_ABILITY_PED_RELATIONSHIP_GROUP - Creating the relPlayerAbilityPed group!")
	ADD_RELATIONSHIP_GROUP("relPlayerAbilityPed", relPlayerAbilityPed)
	
	// Like the player
	INT iPlayerTeam
	FOR iPlayerTeam = 0 TO FM_MAX_PLAYER_REL_GROUPS - 1
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_PlayerTeam[iPlayerTeam], relPlayerAbilityPed)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relPlayerAbilityPed, rgFM_PlayerTeam[iPlayerTeam])
	ENDFOR
	
	// Like civilians
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_CIVMALE, relPlayerAbilityPed)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relPlayerAbilityPed, RELGROUPHASH_CIVMALE)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_CIVFEMALE, relPlayerAbilityPed)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relPlayerAbilityPed, RELGROUPHASH_CIVFEMALE)
	
	// Like friendly peds
	INT iCopRelationship
	FOR iCopRelationship = 0 TO FM_RelationshipLikeCops
		// Make the ability ped like friendly peds
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relPlayerAbilityPed, rgFM_AiPed[FM_RelationshipLike][FM_RelationshipDislike][FM_RelationshipDislike][FM_RelationshipDislike][iCopRelationship])
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relPlayerAbilityPed, rgFM_AiPed[FM_RelationshipLike][FM_RelationshipLike][FM_RelationshipDislike][FM_RelationshipDislike][iCopRelationship])
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relPlayerAbilityPed, rgFM_AiPed[FM_RelationshipLike][FM_RelationshipLike][FM_RelationshipLike][FM_RelationshipDislike][iCopRelationship])
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relPlayerAbilityPed, rgFM_AiPed[FM_RelationshipLike][FM_RelationshipLike][FM_RelationshipLike][FM_RelationshipLike][iCopRelationship])
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relPlayerAbilityPed, rgFM_AiPed[FM_RelationshipLike][FM_RelationshipHate][FM_RelationshipHate][FM_RelationshipHate][iCopRelationship])
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relPlayerAbilityPed, rgFM_AiPed[FM_RelationshipLike][FM_RelationshipLike][FM_RelationshipHate][FM_RelationshipHate][iCopRelationship])
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relPlayerAbilityPed, rgFM_AiPed[FM_RelationshipLike][FM_RelationshipLike][FM_RelationshipLike][FM_RelationshipHate][iCopRelationship])
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relPlayerAbilityPed, rgFM_AiPed[FM_RelationshipLike][FM_RelationshipLike][FM_RelationshipLike][FM_RelationshipLike][iCopRelationship])
		
		// Make friendly peds like ability peds
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_AiPed[FM_RelationshipLike][FM_RelationshipDislike][FM_RelationshipDislike][FM_RelationshipDislike][iCopRelationship], relPlayerAbilityPed)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_AiPed[FM_RelationshipLike][FM_RelationshipLike][FM_RelationshipDislike][FM_RelationshipDislike][iCopRelationship], relPlayerAbilityPed)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_AiPed[FM_RelationshipLike][FM_RelationshipLike][FM_RelationshipLike][FM_RelationshipDislike][iCopRelationship], relPlayerAbilityPed)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_AiPed[FM_RelationshipLike][FM_RelationshipLike][FM_RelationshipLike][FM_RelationshipLike][iCopRelationship], relPlayerAbilityPed)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_AiPed[FM_RelationshipLike][FM_RelationshipHate][FM_RelationshipHate][FM_RelationshipHate][iCopRelationship], relPlayerAbilityPed)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_AiPed[FM_RelationshipLike][FM_RelationshipLike][FM_RelationshipHate][FM_RelationshipHate][iCopRelationship], relPlayerAbilityPed)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_AiPed[FM_RelationshipLike][FM_RelationshipLike][FM_RelationshipLike][FM_RelationshipHate][iCopRelationship], relPlayerAbilityPed)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_AiPed[FM_RelationshipLike][FM_RelationshipLike][FM_RelationshipLike][FM_RelationshipLike][iCopRelationship], relPlayerAbilityPed)
	ENDFOR
	
	// Hate cops
	SET_AMBIENT_COP_REL_TO_THIS_RELGROUP(ACQUAINTANCE_TYPE_PED_HATE, relPlayerAbilityPed)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_PED_STILL_SPAWN_IN_MISSION(INT iped, INT iTeamOverride = -1, INT iRuleOverride = -1)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedObjective
					// The ped will have a chance to spawn:
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedObjective
					// The ped still has a chance to spawn:
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE // Spawn type won't prevent ped from spawning after the rule
			RETURN TRUE
		ENDIF
	ELSE
		//Ped will just spawn at mission start!
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iThirdAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iThirdAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iThirdAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iThirdAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iThirdAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iThirdAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iThirdAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFourthAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFourthAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFourthAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFourthAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFourthAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFourthAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFourthAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_PED(INT iPedIndex, INT iStartingPlayers)
	RETURN MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_ENTITY(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iSpawnPlayerNum, iStartingPlayers)
ENDFUNC

FUNC BOOL MC_BLOCK_SPAWN_FOR_PED_MISSION_OVERRIDE(INT iPed)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThirteen, ciPED_BSThirteen_MissionVariation_UseAsociatedSelectionSpawn)
		PRINTLN("[AltVars] MC_BLOCK_SPAWN_FOR_PED_MISSION_OVERRIDE - Ped: ", iPed, ", blocked - Returning TRUE (Associated Selection)")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

//Used to check if any of the despawn options was set and if yes was its requirment met
FUNC BOOL MC_WAS_DESPAWN_CONDITION_REACHED(CLEANUP_STRUCT& sCleanupData, INT &iAggroIndex #IF IS_DEBUG_BUILD , STRING tlObjectType #ENDIF )
	INT iLinkedIndex = sCleanupData.iCleanupValue1
	INT iTeamRequired = sCleanupData.iNumOfPlayersRequired	
	
	//All checks require this value to be a valid index
	IF iLinkedIndex < 0
		PRINTLN("[RCC MISSION] MC_WAS_DESPAWN_CONDITION_REACHED - Invalid idex. iLinkedIndex = ", iLinkedIndex, "| iTeamRequired = ", iTeamRequired)
		RETURN FALSE
	ENDIF
	
	//Cleanup zone
	IF sCleanupData.eDespawnOption = FMMC_DESPAWN_USING_ZONE
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfZones > iLinkedIndex
		AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[iLinkedIndex].iType = ciFMMC_ZONE_TYPE__CLEAR_ENTITY	
			IF HAS_ENTITY_SPAWN_ZONE_REQUIREMENT_BEEN_MET(iLinkedIndex, iTeamRequired)
				PRINTLN("[RCC MISSION] MC_WAS_DESPAWN_CONDITION_REACHED - Cleanup entity ", tlObjectType, " due to ciFMMC_ZONE_TYPE__CLEAR_ENTITY zone being triggered.")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	//Rule
	IF sCleanupData.eDespawnOption = FMMC_DESPAWN_USING_RULE
		IF  MC_serverBD_4.iCurrentHighestPriority[iTeamRequired] >= iLinkedIndex
		AND MC_serverBD_4.iCurrentHighestPriority[iTeamRequired] <= sCleanupData.iRuleEnd
			IF NOT IS_BIT_SET(sCleanupData.iCleanupBS, ciDESPAWN_AT_RULE_MIDPOINT)
				PRINTLN("[RCC MISSION] MC_WAS_DESPAWN_CONDITION_REACHED - Cleanup entity ", tlObjectType, " due to required objective being reached.")
				RETURN TRUE
			//Despawn at midpoint
			ELIF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeamRequired], iLinkedIndex)
				PRINTLN("[RCC MISSION] MC_WAS_DESPAWN_CONDITION_REACHED - Cleanup entity ", tlObjectType, " due to required objective being reached (midpoint).")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	//Agro
	IF sCleanupData.eDespawnOption = FMMC_DESPAWN_USING_AGGRO		
		IF HAS_TEAM_TRIGGERED_AGGRO(iLinkedIndex, iAggroIndex)
			PRINTLN("[RCC MISSION] MC_WAS_DESPAWN_CONDITION_REACHED - Cleanup entity ", tlObjectType, " due to aggro.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	//Score
	IF sCleanupData.eDespawnOption = FMMC_DESPAWN_USING_SCORE
		IF MC_ServerBD.iScoreOnThisRule[iTeamRequired] >= iLinkedIndex
			PRINTLN("[RCC MISSION] MC_WAS_DESPAWN_CONDITION_REACHED - Cleanup entity ", tlObjectType, " due to required score being reached. iScoreOnThisRule = ", MC_ServerBD.iScoreOnThisRule[iTeamRequired], " | Score required = ", iLinkedIndex, " | Team ", iTeamRequired)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MC_IS_PED_GOING_TO_SPAWN_LATER(INT i)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn = DUMMY_MODEL_FOR_SCRIPT
		RETURN FALSE
	ENDIF
	
	IF MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].eSpawnConditionFlag, FALSE, TRUE, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAggroIndexBS_Entity_Ped)
		PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_IS_PED_GOING_TO_SPAWN_LATER - Returning FALSE due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")
		RETURN FALSE
	ENDIF
	
	BOOL bSuitableSpawnGroup = MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_Ped, DEFAULT, DEFAULT, TRUE)
	
	IF MC_WILL_ENTITY_CLEANUP_IMMEDIATELY(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCleanupTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCleanupObjective, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitset, ciPED_BS_CleanupAtMidpoint))
	OR MC_WILL_CHECKPOINTS_STOP_THIS_SPAWN(i, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetSix, ciPED_BSSix_DontSpawnOnCheckpoint_0), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetSix, ciPED_BSSix_DontSpawnOnCheckpoint_1), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetEleven, ciPed_BSEleven_DontSpawnOnCheckpoint_2), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetEleven, ciPed_BSEleven_DontSpawnOnCheckpoint_3), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFifteen, ciPED_BSFifteen_BlockSpawnIfNotACheckpoint))
	OR NOT bSuitableSpawnGroup
	OR NOT MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_PED(i, GET_TOTAL_STARTING_PLAYERS())	
	OR MC_BLOCK_SPAWN_FOR_PED_MISSION_OVERRIDE(i)
		RETURN FALSE
	ENDIF
	
	IF MC_ARE_SPAWN_CONDITION_FLAGS_MET_DURING_GAMEPLAY(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].eSpawnConditionFlag)
		IF MC_SHOULD_ENTITY_SPAWN_AT_START(CREATION_TYPE_PEDS, i, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedObjective, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedSpawn, TRUE, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedAlwaysForceSpawnOnRule)		
			PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_IS_PED_GOING_TO_SPAWN_LATER - Returning TRUE due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")			
			RETURN TRUE
		ELSE
			PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_IS_PED_GOING_TO_SPAWN_LATER - Returning FALSE due to not being set up to spawn at the start ....")
		ENDIF
	ENDIF
	
	IF MC_IS_ENTITY_SPAWN_TRIGGERED_BY_ZONE_ENTRY(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iZoneBlockEntitySpawn)
		RETURN TRUE
	ENDIF
	
	IF MC_SHOULD_ENTITY_SPAWN_LATER(CREATION_TYPE_PEDS, i, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedObjective, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedSpawn, TRUE, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedAlwaysForceSpawnOnRule)		
		IF bSuitableSpawnGroup
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCSRespawnSceneIndex > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCSRespawnShotIndex > -1
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sProximitySpawningData.iSpawnRange != 0
		PRINTLN("[MCSpawning][Peds][Ped ", i, "][PROXSPAWN] - MC_IS_PED_GOING_TO_SPAWN_LATER - Returning TRUE due to using Proximity Spawning")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetSeventeen, ciPed_BSSeventeen_SpawnedByEvent)
		PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_IS_PED_GOING_TO_SPAWN_LATER - Returning TRUE due to ciPed_BSSeventeen_SpawnedByEvent")
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT.sPlayerAbilities.sVehicleRepair.iMechanicPedIndex = i
		PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_IS_PED_GOING_TO_SPAWN_LATER - Returning TRUE, this ped is set to be the mechanic for the Vehicle Repair ability")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL MC_SHOULD_PED_SPAWN_AT_START(INT i, BOOL bArrayCheck)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn = DUMMY_MODEL_FOR_SCRIPT
		RETURN FALSE
	ENDIF
	
	IF MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].eSpawnConditionFlag, TRUE, FALSE, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAggroIndexBS_Entity_Ped)
		PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_SHOULD_PED_SPAWN_AT_START - Returning FALSE due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_PedDeathTracking)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iContinuityId > -1
		AND FMMC_IS_LONG_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iPedDeathBitset, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iContinuityId)			
			PRINTLN("[MCSpawning][Peds][Ped ", i, "][CONTINUITY] - MC_SHOULD_PED_SPAWN_AT_START - with continuity ID ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iContinuityId, " not spawning due to death of previous stand mission")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sProximitySpawningData.iSpawnRange != 0
		PRINTLN("[MCSpawning][Peds][Ped ", i, "][PROXSPAWN] - MC_SHOULD_PED_SPAWN_AT_START - Returning FALSE due to using Proximity Spawning")
		RETURN FALSE
	ENDIF
	
	IF NOT bArrayCheck
		IF MC_WILL_CHECKPOINTS_STOP_THIS_SPAWN(i, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetSix, ciPED_BSSix_DontSpawnOnCheckpoint_0), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetSix, ciPED_BSSix_DontSpawnOnCheckpoint_1), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetEleven, ciPed_BSEleven_DontSpawnOnCheckpoint_2), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetEleven, ciPed_BSEleven_DontSpawnOnCheckpoint_3), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFifteen, ciPED_BSFifteen_BlockSpawnIfNotACheckpoint))
			PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_SHOULD_PED_SPAWN_AT_START - return FALSE - MC_WILL_CHECKPOINTS_STOP_THIS_SPAWN = TRUE")
			RETURN FALSE
		ENDIF
		
		IF MC_WILL_ENTITY_CLEANUP_IMMEDIATELY(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCleanupTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCleanupObjective, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitset, ciPED_BS_CleanupAtMidpoint))
			PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_SHOULD_PED_SPAWN_AT_START - return FALSE - MC_WILL_ENTITY_CLEANUP_IMMEDIATELY = TRUE")
			RETURN FALSE
		ENDIF
		
		IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_Ped)
			PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_SHOULD_PED_SPAWN_AT_START - return FALSE - MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP = FALSE")
			RETURN FALSE
		ENDIF
		
		IF NOT MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_PED(i, GET_TOTAL_STARTING_PLAYERS())
			PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_SHOULD_PED_SPAWN_AT_START - return FALSE - MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_PED = TRUE")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF MC_BLOCK_SPAWN_FOR_PED_MISSION_OVERRIDE(i)
		PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_SHOULD_PED_SPAWN_AT_START - return FALSE - MC_BLOCK_SPAWN_FOR_PED_MISSION_OVERRIDE = TRUE")
		RETURN FALSE
	ENDIF
	
	IF DOES_PED_HAVE_PLAYER_VARIABLE_SETTING(i)
		IF (MC_serverBD_1.sCreatedCount.inumpedcreated[0] >= MC_serverBD.iNumStartingPlayers[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedTeam])  
		OR (IS_FAKE_MULTIPLAYER_MODE_SET() AND MC_serverBD_1.sCreatedCount.inumpedcreated[0] >= 1)	
			SET_BIT(MC_serverBD_1.sCreatedCount.iSkipPedBitset[GET_LONG_BITSET_INDEX_ALT(i)], GET_LONG_BITSET_BIT_ALT(i))
			PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_SHOULD_PED_SPAWN_AT_START - return FALSE - DOES_PED_HAVE_PLAYER_VARIABLE_SETTING = true")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF MC_IS_ENTITY_SPAWN_TRIGGERED_BY_ZONE_ENTRY(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iZoneBlockEntitySpawn)
		PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_SHOULD_PED_SPAWN_AT_START - return FALSE - MC_IS_ENTITY_SPAWN_TRIGGERED_BY_ZONE_ENTRY")
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCSRespawnSceneIndex > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCSRespawnShotIndex > -1
		PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_SHOULD_PED_SPAWN_AT_START - return FALSE - iCSRespawnSceneIndex AND iCSRespawnShotIndex > -1")
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sTrainAttachmentData.iTrainIndex > -1
 		IF NOT IS_TRAIN_ATTACHMENT_READY(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sTrainAttachmentData)
			PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_SHOULD_PED_SPAWN_AT_START - return FALSE - Train isn't ready")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetSeventeen, ciPed_BSSeventeen_SpawnedByEvent)
		PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_SHOULD_PED_SPAWN_AT_START - return FALSE - Ped is dropped by event")
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT.sPlayerAbilities.sVehicleRepair.iMechanicPedIndex = i
		PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_SHOULD_PED_SPAWN_AT_START - return FALSE - Ped is the mechanic for the Vehicle Repair event")
		RETURN FALSE
	ENDIF
	
	IF NOT MC_SHOULD_ENTITY_SPAWN_AT_START(CREATION_TYPE_PEDS, i, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedObjective, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedSpawn, NOT bArrayCheck, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedAlwaysForceSpawnOnRule)
		PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_SHOULD_PED_SPAWN_AT_START - return FALSE - MC_SHOULD_ENTITY_SPAWN_AT_START = FALSE")
		RETURN FALSE
	ENDIF
	
	PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_SHOULD_PED_SPAWN_AT_START - return TRUE")
	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_PED_CLEANUP_DUE_TO_RANGE(FMMC_PED_STATE &sPedState)
	
	INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(sPedState.pedIndex)
	IF iClosestPlayer != -1
		PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
		PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
		IF sPedState.pedIndex != NULL
			IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed, sPedState.pedIndex) > g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iCleanupRange
			AND NOT (IS_PLAYER_IN_PROPERTY(ClosestPlayer, TRUE) OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(ClosestPlayer))
				PRINTLN("[RCC MISSION] SHOULD_CLEANUP_PED: Cleanup ped ", sPedState.iIndex, " is too far away (ped)") 
				RETURN TRUE
			ENDIF
		ELSE
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].vPos) > g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iCleanupRange
			AND NOT (IS_PLAYER_IN_PROPERTY(ClosestPlayer, TRUE) OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(ClosestPlayer))
				PRINTLN("[RCC MISSION] SHOULD_CLEANUP_PED: Cleanup ped ", sPedState.iIndex, " is too far away (spawn pos)") 
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC CACHE_PED_SHOULD_CLEANUP_THIS_FRAME(INT i)
	FMMC_SET_LONG_BIT(iPedShouldCleanupThisFrameBS, i)
ENDPROC

PROC CACHE_PED_SHOULD_NOT_CLEANUP_THIS_FRAME(INT i)
	FMMC_SET_LONG_BIT(iPedShouldNotCleanupThisFrameBS, i)
ENDPROC

FUNC BOOL SHOULD_PED_CLEANUP_BE_DELAYED(INT iPed)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupDelay > 0
		IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_3.iPedSpawnDelay[iPed])
			PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Starting Delay.")
			MC_REINIT_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_3.iPedSpawnDelay[iPed])
		ENDIF
		
		IF NOT MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(MC_serverBD_3.iPedSpawnDelay[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupDelay)
			PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Waiting for delay to expire. Miliseconds: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_3.iPedSpawnDelay[iPed]), " / ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupDelay)			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CLEANUP_PED(FMMC_PED_STATE &sPedState)
	INT iPed = sPedState.iIndex
	
	IF FMMC_IS_LONG_BIT_SET(iPedShouldCleanupThisFrameBS, iPed)
		RETURN TRUE
	ENDIF
	IF FMMC_IS_LONG_BIT_SET(iPedShouldNotCleanupThisFrameBS, iPed)
		RETURN FALSE
	ENDIF
	
	IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSix, ciPED_BSSix_CleanupAtMissionEnd)
		PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. GAME_STATE_END") 
		CACHE_PED_SHOULD_CLEANUP_THIS_FRAME(iPed)
		RETURN TRUE
	ENDIF
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAtSecondaryGotoLocBitset, iPed)
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetNine, ciPed_BSNine_CleanupOnSecondaryTaskCompleted)
		PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. Secondary task completed") 
		CACHE_PED_SHOULD_CLEANUP_THIS_FRAME(iPed)
		RETURN TRUE
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwelve, ciPed_BSTwelve_ShouldCleanupPedWhenTrailerDetached)
	AND FMMC_IS_LONG_BIT_SET(iPedTrailerDetachedBitset, iPed)
	AND (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwelve, ciPed_BSTwelve_ClearPedTasksWhenTrailerDetached) OR FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedTaskStopped, iPed))
		PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. ciPed_BSTwelve_ShouldCleanupPedWhenTrailerDetached and iPedTrailerDetachedBitset is set.") 
		CACHE_PED_SHOULD_CLEANUP_THIS_FRAME(iPed)
		RETURN TRUE
	ENDIF	
	
	//Despawn options	
	IF MC_WAS_DESPAWN_CONDITION_REACHED(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sCleanupData, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAggroIndexBS_Entity_Ped #IF IS_DEBUG_BUILD ,"ped" #ENDIF )
		IF NOT SHOULD_PED_CLEANUP_BE_DELAYED(iPed)
			PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. Despawn Condition Reached.")
			CACHE_PED_SHOULD_CLEANUP_THIS_FRAME(iPed)
			RETURN TRUE
		ELSE
			PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - MC_WAS_DESPAWN_CONDITION_REACHED = TRUE - Waiting on Delay.")
		ENDIF
	ENDIF
	
	BOOL bCheckEarlyCleanup

	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupObjective != -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupTeam != -1
			IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupTeam] < FMMC_MAX_RULES
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupObjective <= MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupTeam]
					PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. Ped has been flagged to cleanup by Creator Set Objective/Priorities.") 
					bCheckEarlyCleanup = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSixteen, ciPED_BSSixteen_CleanUpOnFlee)
		IF MC_serverBD_2.iPedState[iPed] = ciTASK_FLEE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFive, ciPed_BSFive_DontFleeInvehicle)
				IF sPedState.bIsInAnyVehicle			 
					PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Ped can't be cleaned up yet as they are exiting their vehicle.")
					bCheckEarlyCleanup = FALSE
				ELSE
					PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. Ped has been flagged to cleanup by clean up on flee.") 
					bCheckEarlyCleanup = TRUE
				ENDIF
			ELSE
				PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. Ped has been flagged to cleanup by clean up on flee.") 
				bCheckEarlyCleanup = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(iPedGotoAssCompletedCleanupBitset, iPed)
		PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. Ped has been flagged to cleanup by ass goto system; iPedGotoAssCompletedCleanupBitset.") 
		bCheckEarlyCleanup = TRUE
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(iPedShouldBeForcedToCleanup, iPed)
		PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. Ped has been flagged to cleanup by generic system/event; iPedShouldBeForcedToCleanup.") 
		bCheckEarlyCleanup = TRUE
	ENDIF
	
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iPed, eSGET_Ped)
		PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. Ped no longer has a valid spawn group active.") 
		bCheckEarlyCleanup = TRUE
	ENDIF
	
	IF bCheckEarlyCleanup
	
		// Reusing Spawn Delay to save on Broadcast Data.
		IF SHOULD_PED_CLEANUP_BE_DELAYED(iPed)
			PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Waiting for delay to expire. Miliseconds: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_3.iPedSpawnDelay[iPed]), " / ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupDelay)
			CACHE_PED_SHOULD_NOT_CLEANUP_THIS_FRAME(iPed)
			RETURN FALSE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitset, ciPED_BS_CleanupAtMidpoint)
			IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupTeam], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupObjective)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupVehicle != -1
					PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupVehicle = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupVehicle)
					VEHICLE_INDEX viTempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupVehicle])
					IF NOT IS_VEHICLE_FUCKED_MP(viTempVeh)
						PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. not fucked")
						IF IS_PED_A_PLAYER(GET_PED_IN_VEHICLE_SEAT(viTempVeh)) 
							PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. a player is driving")
							IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupRange <= 0
								CACHE_PED_SHOULD_CLEANUP_THIS_FRAME(iPed)
								MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_3.iPedSpawnDelay[iPed])
								RETURN TRUE
							ELSE
								INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(sPedState.pedIndex)
								IF iClosestPlayer != -1
									PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
									PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
									IF sPedState.pedIndex != NULL
										IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed, sPedState.pedIndex) > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupRange
										AND NOT (IS_PLAYER_IN_PROPERTY(ClosestPlayer, TRUE) OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(ClosestPlayer))
											PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. is too far away (in veh ped)") 
											CACHE_PED_SHOULD_CLEANUP_THIS_FRAME(iPed)
											MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_3.iPedSpawnDelay[iPed])
											RETURN TRUE
										ENDIF
									ELSE
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vPos) > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupRange
										AND NOT (IS_PLAYER_IN_PROPERTY(ClosestPlayer, TRUE) OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(ClosestPlayer))
											PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. is too far away (in veh spawn pos)") 
											CACHE_PED_SHOULD_CLEANUP_THIS_FRAME(iPed)
											MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_3.iPedSpawnDelay[iPed])
											RETURN TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupRange <= 0
					PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. iCleanupRange <= 0 (midpoint)") 
					CACHE_PED_SHOULD_CLEANUP_THIS_FRAME(iPed)
					MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_3.iPedSpawnDelay[iPed])
					RETURN TRUE
				ELSE
					INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(sPedState.pedIndex)
					IF iClosestPlayer != -1
						PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
						PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
						IF sPedState.pedIndex != NULL
							IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed, sPedState.pedIndex) > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupRange
							AND NOT (IS_PLAYER_IN_PROPERTY(ClosestPlayer, TRUE) OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(ClosestPlayer))
								PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. Cleanup at midpoint is too far away (ped)") 
								CACHE_PED_SHOULD_CLEANUP_THIS_FRAME(iPed)
								MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_3.iPedSpawnDelay[iPed])
								RETURN TRUE
							ENDIF
						ELSE
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vPos) > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupRange
							AND NOT (IS_PLAYER_IN_PROPERTY(ClosestPlayer, TRUE) OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(ClosestPlayer))
								PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. Cleanup at midpoint is too far away (spawn pos)") 
								CACHE_PED_SHOULD_CLEANUP_THIS_FRAME(iPed)
								MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_3.iPedSpawnDelay[iPed])
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			//Midpoint doesn't matter
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupVehicle != -1
				PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupVehicle = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupVehicle)
				VEHICLE_INDEX viTempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupVehicle])
				IF NOT IS_VEHICLE_FUCKED_MP(viTempVeh)
					PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. not fucked")
					IF IS_PED_A_PLAYER(GET_PED_IN_VEHICLE_SEAT(viTempVeh)) 
						PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. a player is driving")
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupRange <= 0
							CACHE_PED_SHOULD_CLEANUP_THIS_FRAME(iPed)
							MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_3.iPedSpawnDelay[iPed])
							RETURN TRUE
						ELSE
							INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(sPedState.pedIndex)
							IF iClosestPlayer != -1
								PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
								PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
								IF sPedState.pedIndex != NULL
									IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed, sPedState.pedIndex) > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupRange
									AND NOT (IS_PLAYER_IN_PROPERTY(ClosestPlayer, TRUE) OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(ClosestPlayer))
										PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. is too far away (in veh ped)") 
										CACHE_PED_SHOULD_CLEANUP_THIS_FRAME(iPed)
										MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_3.iPedSpawnDelay[iPed])
										RETURN TRUE
									ENDIF
								ELSE
									IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vPos) > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupRange
									AND NOT (IS_PLAYER_IN_PROPERTY(ClosestPlayer, TRUE) OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(ClosestPlayer))
										PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. is too far away (in veh spawn pos)") 
										CACHE_PED_SHOULD_CLEANUP_THIS_FRAME(iPed)
										MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_3.iPedSpawnDelay[iPed])
										RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupRange <= 0
				PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. iCleanupRange <= 0") 
				CACHE_PED_SHOULD_CLEANUP_THIS_FRAME(iPed)				
				MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_3.iPedSpawnDelay[iPed])
				RETURN TRUE
			ELSE
				INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(sPedState.pedIndex)
				IF iClosestPlayer != -1
					PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
					PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
					IF sPedState.pedIndex != NULL
						IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed, sPedState.pedIndex) > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupRange
						AND NOT (IS_PLAYER_IN_PROPERTY(ClosestPlayer, TRUE) OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(ClosestPlayer))
							PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. is too far away (ped)") 
							CACHE_PED_SHOULD_CLEANUP_THIS_FRAME(iPed)
							MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_3.iPedSpawnDelay[iPed])
							RETURN TRUE
						ENDIF
					ELSE
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vPos) > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupRange
						AND NOT (IS_PLAYER_IN_PROPERTY(ClosestPlayer, TRUE) OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(ClosestPlayer))
							PRINTLN("[Peds][Ped ", iPed, "] - SHOULD_CLEANUP_PED - Cleaning up. is too far away (spawn pos)") 
							CACHE_PED_SHOULD_CLEANUP_THIS_FRAME(iPed)
							MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_3.iPedSpawnDelay[iPed])
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	CACHE_PED_SHOULD_NOT_CLEANUP_THIS_FRAME(iPed)
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DOES_ANY_TEAM_HAVE_PHOTO_DEAD_RULE(INT iPed)

	INT iteam

	FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[iteam], iPed)
			RETURN TRUE
		ENDIF
	ENDFOR

	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_PED_CLEANUP_ON_DEATH(FMMC_PED_STATE &sPedState)
	
	INT iPed = sPedState.iIndex
	BOOL bShouldCleanup = TRUE
		
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThree, ciPED_BSThree_DontCleanupOnDeath)
	OR DOES_ANY_TEAM_HAVE_PHOTO_DEAD_RULE(iPed)
		bShouldCleanup = FALSE
	ELSE
		IF sPedState.bExists
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iInventoryBS != 0
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iInventoryBS, ciPED_INV_Briefcase_prop_ld_case_01)
					OBJECT_INDEX oiBrief = GET_CLOSEST_OBJECT_OF_TYPE(GET_FMMC_PED_COORDS(sPedState), 10, PROP_LD_CASE_01, false, false, false)
					
					IF DOES_ENTITY_EXIST(oiBrief)
					AND IS_ENTITY_ATTACHED_TO_ENTITY(oiBrief, sPedState.pedIndex)
						bShouldCleanup = FALSE
					ENDIF
				ENDIF
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim = ciPED_IDLE_ANIM__UNFSHDBIZ_JUDGE_IDLE
				VECTOR vCoords = <<-1806.5146, 427.71, 131.810>>
				OBJECT_INDEX oiTablet
				oiTablet = GET_CLOSEST_OBJECT_OF_TYPE(vCoords,5,PROP_CS_TABLET, false, false, false)
				
				IF DOES_ENTITY_EXIST(oiTablet)
				AND IS_ENTITY_ATTACHED_TO_ENTITY(oiTablet, sPedState.pedIndex)
					bShouldCleanup = FALSE
				ENDIF
			ELIF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim = ciPED_IDLE_ANIM__WITSEC_CHAIR_SNOOZE)
				VECTOR vCoords = GET_FMMC_PED_COORDS(sPedState)
				
				OBJECT_INDEX oiChair
				oiChair = GET_CLOSEST_OBJECT_OF_TYPE(vCoords,5,HEI_PROP_HEI_SKID_CHAIR, false, false, false)
				
				IF DOES_ENTITY_EXIST(oiChair)
				AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(oiChair, FALSE)
					bShouldCleanup = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldCleanup
	
ENDFUNC

PROC CLEANUP_IDLE_ANIM_PROPS(FMMC_PED_STATE &sPedState)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iIdleAnim = ciPED_IDLE_ANIM__INVALID
		EXIT //Ped has no Idle Anim
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(iPedIdleAnimPropBitset, sPedState.iIndex)
	AND NETWORK_DOES_NETWORK_ID_EXIST(sMissionPedsLocalVars[sPedState.iIndex].niAnimObjs[ciMissionPedAnimObject])
		IF DOES_ENTITY_EXIST(sMissionPedsLocalVars[sPedState.iIndex].oiAnimObjs[ciMissionPedAnimObject])
		AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(sMissionPedsLocalVars[sPedState.iIndex].oiAnimObjs[ciMissionPedAnimObject], FALSE)
			NETWORK_ALLOW_REMOTE_ATTACHMENT_MODIFICATION(sMissionPedsLocalVars[sPedState.iIndex].oiAnimObjs[ciMissionPedAnimObject], TRUE)
			DETACH_ENTITY(sMissionPedsLocalVars[sPedState.iIndex].oiAnimObjs[ciMissionPedAnimObject])
			SET_OBJECT_AS_NO_LONGER_NEEDED(sMissionPedsLocalVars[sPedState.iIndex].oiAnimObjs[ciMissionPedAnimObject])
			MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
			FMMC_CLEAR_LONG_BIT(iPedIdleAnimPropBitset, sPedState.iIndex)
			PRINTLN("[DELETE_FMMC_PED] CLEANUP_IDLE_ANIM_PROPS - Prop was successfully detached from PED_", sPedState.iIndex)
		ELSE
			PRINTLN("[DELETE_FMMC_PED] CLEANUP_IDLE_ANIM_PROPS - PED_", sPedState.iIndex, " - Entity doesn't exist!")
		ENDIF
	ELSE
		PRINTLN("[DELETE_FMMC_PED] CLEANUP_IDLE_ANIM_PROPS - PED_", sPedState.iIndex, " had no attached props")
	ENDIF	
ENDPROC

PROC DELETE_FMMC_PED(FMMC_PED_STATE &sPedState)
	
	START_ENTITY_PTFX_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sEntityDespawnPTFX, GET_ENTITY_COORDS(sPedState.pedIndex, FALSE), GET_ENTITY_ROTATION(sPedState.pedIndex))
	
	CLEANUP_IDLE_ANIM_PROPS(sPedState)
	
	INT iPed = sPedState.iIndex	
	INT iteam
	
	FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iPed], iteam)
		FMMC_CLEAR_LONG_BIT(MC_serverBD.iMissionCriticalPed[iteam], iPed)
	ENDFOR	
	
	FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS, iPed)
	
	DELETE_NET_ID(sPedState.niIndex)
	
	#IF IS_DEBUG_BUILD
		IF biHostilePedBlip[iPed].PedID != NULL
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] DELETE_FMMC_PED - Ped ", iped, " AI blip cleaning up due to CLEANUP_PED_EARLY call.")
		ENDIF
	#ENDIF
	CLEANUP_AI_PED_BLIP(biHostilePedBlip[iPed])
	
	FILL_FMMC_PED_STATE_STRUCT(sPedState, iPed, TRUE)
	
	PRINTLN("[RCC MISSION] DELETE_FMMC_PED - ped getting cleaned up early - delete, objective: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupObjective, " team: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupTeam, " ped: ", iPed)
	
ENDPROC

/// PURPOSE:
///    Cleans up a ped mid mission
///    You should check if the ped needs to be cleaned up before calling this function! Before the 2020 split this fucntion included a check for SHOULD_CLEANUP_PED()
/// PARAMS:
///    sPedState - Cached ped state of the ped to cleanup
///    bFinalCleanup - If the ped is to be fully cleaned up so they do not spawn again on the mission
/// RETURNS:
///    TRUE when the ped is deleted or set as no longer needed
FUNC BOOL CLEANUP_PED_EARLY(FMMC_PED_STATE &sPedState, BOOL bFinalCleanup = TRUE, BOOL bResetFirstSpawn = FALSE)
	INT iPed = sPedState.iIndex
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitset, ciPED_BS_IgnoreVisCheckCleanup)
		IF NOT sPedState.bHasControl
			PRINTLN("[RCC MISSION] CLEANUP_PED_EARLY - Requesting control of ped ", iped, " for deletion; setting MC_serverBD.iPedCleanup_NeedOwnershipBS bit")
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			FMMC_SET_LONG_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS, iPed)
		ELSE
			PRINTLN("[RCC MISSION] CLEANUP_PED_EARLY - Deleting ped ", iPed)
			DELETE_FMMC_PED(sPedState)
			RETURN TRUE
		ENDIF
	ELSE
		INT iteam
		FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
			CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iPed], iteam)
			FMMC_CLEAR_LONG_BIT(MC_serverBD.iMissionCriticalPed[iteam], iPed)
		ENDFOR
		
		START_ENTITY_PTFX_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sEntityDespawnPTFX, GET_FMMC_PED_COORDS(sPedState), GET_ENTITY_ROTATION(sPedState.pedIndex))
		
		// If a ped is already fleeing, or is set to flee on cleanup
		IF NOT sPedState.bInjured
			IF IS_PED_FLEEING(sPedState.pedIndex)
			OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFive, ciPED_BSFive_FleeOnCleanup)
				// Force ped to always flee (Added to fix url:bugstar:2117976)
				SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_ALWAYS_FIGHT, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_DISABLE_FLEE_FROM_COMBAT, FALSE)
				SET_PED_FLEE_ATTRIBUTES(sPedState.pedIndex, FA_NEVER_FLEE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_ALWAYS_FLEE, TRUE)
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iPedBitsetFive, ciPed_BSFive_DontFleeInvehicle)
					SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_FLEE_WHILST_IN_VEHICLE, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sPedState.pedIndex, FA_USE_VEHICLE, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sPedState.pedIndex, FA_FORCE_EXIT_VEHICLE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_USE_VEHICLE, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_LEAVE_VEHICLES, TRUE)
				ELSE
					SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_FLEE_WHILST_IN_VEHICLE, TRUE)
				ENDIF
				SET_PED_FLEE_ATTRIBUTES(sPedState.pedIndex, FA_COWER_INSTEAD_OF_FLEE, FALSE)
				SET_PED_FLEE_ATTRIBUTES(sPedState.pedIndex, FA_DISABLE_COWER, TRUE)
				PRINTLN("[RCC MISSION] Ped ", iped, " setup to flee (w CA_ALWAYS_FLEE) on cleanup.")
				IF IS_PED_IN_ANY_HELI(sPedState.pedIndex)
				OR IS_PED_IN_ANY_PLANE(sPedState.pedIndex)
					PRINTLN("[RCC MISSION] [JS] Ped ", iped, " setup to flee far away in heli")
					CLEAR_PED_TASKS(sPedState.pedIndex)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPedState.pedIndex, TRUE)
					VECTOR fleeDirection = GET_ENTITY_FORWARD_VECTOR(sPedState.pedIndex)
					fleeDirection.z = 0
					fleeDirection = fleeDirection / VMAG(fleeDirection)
					fleeDirection.z = 0.2
					fleeDirection = fleeDirection / VMAG(fleeDirection)
					TASK_SMART_FLEE_COORD(sPedState.pedIndex, GET_FMMC_PED_COORDS(sPedState) - (fleeDirection*100.0), 10000.0, 999999)						
				ENDIF
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFlee != ciPED_FLEE_ON
				//This should already be set, but make sure it is anyway
				PRINTLN("[RCC MISSION] Ped ", iped, " setup to NOT flee Making sure ped doesn't flee on cleanup.")
				SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_DISABLE_FLEE_FROM_COMBAT, TRUE)
				SET_PED_FLEE_ATTRIBUTES(sPedState.pedIndex, FA_NEVER_FLEE, TRUE)
				SET_PED_FLEE_ATTRIBUTES(sPedState.pedIndex, FA_DISABLE_COWER, TRUE)
			ENDIF
			IF NOT IS_PED_IN_ANY_HELI(sPedState.pedIndex)
			AND NOT IS_PED_IN_ANY_PLANE(sPedState.pedIndex)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPedState.pedIndex, FALSE)
			ENDIF
		ENDIF
		
		IF bFinalCleanup
			MC_serverBD_2.iCurrentPedRespawnLives[iPed] = 0
			PRINTLN("[RCC MISSION] ped ", iped, " getting cleaned up early - set MC_serverBD_2.iCurrentPedRespawnLives[", iPed, "] = 0. Call 1.")
		ENDIF
		
		IF bResetFirstSpawn
			FMMC_SET_LONG_BIT(MC_serverBD.iPedFirstSpawnBitset, iPed)
			PRINTLN("[RCC MISSION] ped ", iped, " getting cleaned up early - resetting iPedFirstSpawnBitset")
		ENDIF
		
		FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS, iPed)
		
		CLEANUP_NET_ID(sPedState.niIndex)
		
		PRINTLN("[RCC MISSION] ped getting cleaned up early - no longer needed, objective: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupObjective, " team: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCleanupTeam, " ped: ", iPed)
		RETURN TRUE
	ENDIF
	
	//FMMC2020 - Why is this here?
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSeven, ciPED_BSSeven_LoudPropellorPlanes)
		STOP_AUDIO_SCENE("Speed_Race_Desert_Airport_Scene")
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC VECTOR MC_GET_PED_SPAWN_LOCATION(INT iPed, INT iServerPropertyIDPassedIn, INT &iSpawnRandomHeadingToSelectPassedIn, BOOL bIsInitialSpawn = FALSE)
	
	VECTOR vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vPos
	
	IF SHOULD_ENTITY_USE_CHECKPOINT_CONTINUITY_VARS(ciENTITY_TYPE_PED, iPed)
		// This ped has got a position saved from the last checkpoint!
		VECTOR vEntityCheckpointPos = GET_COORDS_FOR_ENTITY_CHECKPOINT_CONTINUITY(ciENTITY_TYPE_PED, iPed)
		PRINTLN("[Peds][Ped ", iPed, "][CONTINUITY][EntityCheckpointContinuity] - MC_GET_PED_SPAWN_LOCATION - Ped ", iPed, " using saved checkpoint position ", vEntityCheckpointPos)
		RETURN vEntityCheckpointPos
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sTrainAttachmentData.iTrainIndex != -1
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sTrainAttachmentData.vCarriageOffsetPosition)		
		VECTOR vTemp = <<0.0, 0.0, 0.0>>
		MC_GET_TRAIN_ATTACHMENT_POINT_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sTrainAttachmentData, vSpawnLoc, vTemp)
		PRINTLN("[RCC MISSION][Trains_SPAM][Train_SPAM ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sTrainAttachmentData.iTrainIndex, "][Ped ", iPed, "] MC_GET_PED_SPAWN_LOCATION - Spawn Position (on train) is ", vSpawnLoc)
		RETURN vSpawnLoc
	ENDIF
	
	IF g_FMMC_STRUCT.sPlayerAbilities.sVehicleRepair.iMechanicPedIndex = iPed
		// This ped is set to be the mechanic of the Vehicle Repair player ability! Spawn at the current coords for that
		ENTITY_INDEX eiRepairDynoprop = GET_FMMC_ENTITY(ciENTITY_TYPE_DYNO_PROPS, g_FMMC_STRUCT.sPlayerAbilities.sVehicleRepair.iDynopropIndex)
		IF DOES_ENTITY_EXIST(eiRepairDynoprop)
			vSpawnLoc = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiRepairDynoprop, <<-2.0, 1.0, 0>>)
			PRINTLN("[RCC MISSION][Ped ", iPed, "] MC_GET_PED_SPAWN_LOCATION - Spawning next to Vehicle Repair dynoprop! vSpawnLoc: ", vSpawnLoc)
			RETURN vSpawnLoc
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwelve, ciPed_BSTwelve_UseCheckpointsAsRandomSpawn)
		
		INT iNumberOfSpawns = GET_PED_NUMBER_OF_CHECKPOINT_SPAWN_POSITIONS(iPed) + 1 // 1 for the normal starting position
		
		PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_LOCATION - RANDOM - Picking random spawn from possible ",iNumberOfSpawns," (",iNumberOfSpawns-1," checkpoint locations & 1 starting location)")
		
		SET_RANDOM_SEED(NATIVE_TO_INT(GET_NETWORK_TIME()))
		INT iChoice = GET_RANDOM_INT_IN_RANGE(0,iNumberOfSpawns)
		
		#IF IS_DEBUG_BUILD
		IF g_iMissionForcedCheckpointSpawn != -1
			iChoice = g_iMissionForcedCheckpointSpawn
			PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_LOCATION - RANDOM - Overriding the random checkpoint spawn selection with rag widget value: ", g_iMissionForcedCheckpointSpawn)
		ENDIF
		#ENDIF
		
		SWITCH iChoice
			CASE 0
				iSpawnRandomHeadingToSelectPassedIn = 0
				PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_LOCATION - RANDOM -Spawning at placed location ", vSpawnLoc)
			BREAK
			CASE 1
				iSpawnRandomHeadingToSelectPassedIn = 1
				vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vRestartPos[0]
				PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_LOCATION - RANDOM -Spawning at quick restart point 1 ", vSpawnLoc)
			BREAK
			CASE 2
				iSpawnRandomHeadingToSelectPassedIn = 2
				vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vRestartPos[1]
				PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_LOCATION - RANDOM -Spawning at quick restart point 2 ", vSpawnLoc)
			BREAK
			CASE 3
				iSpawnRandomHeadingToSelectPassedIn = 3
				vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vRestartPos[2]
				PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_LOCATION - RANDOM -Spawning at quick restart point 3 ", vSpawnLoc)
			BREAK
			CASE 4
				iSpawnRandomHeadingToSelectPassedIn = 4
				vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vRestartPos[3]
				PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_LOCATION - RANDOM -Spawning at quick restart point 4 ", vSpawnLoc)
			BREAK
		ENDSWITCH
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_DynamicApartmentSpawnOverride)
		AND ( Is_Player_Currently_On_MP_Heist(PLAYER_ID()) OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID()) OR IS_FAKE_MULTIPLAYER_MODE_SET() )
			
			INT iPropertyIndex = -1
			
			IF IS_FAKE_MULTIPLAYER_MODE_SET()
				iPropertyIndex = 1
			ELSE
				iPropertyIndex = iServerPropertyIDPassedIn
			ENDIF
			
			IF iPropertyIndex > 0
			AND iPropertyIndex < MAX_MP_PROPERTIES
				PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_LOCATION - Dynamic apt spawn override, spawning at property index ",iPropertyIndex)
				vSpawnLoc = GET_HEIST_PROPERTY_SPAWN_LOCATION(iPropertyIndex, FALSE)
			ELSE
				PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_LOCATION - Dynamic apt spawn override, invalid property index! iPropertyIndex = ",iPropertyIndex)
			ENDIF
			
		ENDIF
		
		IF( IS_THIS_A_QUICK_RESTART_JOB() 
		OR IS_CORONA_INITIALISING_A_QUICK_RESTART() )
			IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 3 ])
					vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 3 ]
					PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_LOCATION - on a quick restart checkpoint 4 and ped ", iPed, " has a restart pos = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 3 ] )
				ENDIF
			ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2)
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 2 ])
					vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 2 ]
					PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_LOCATION - on a quick restart checkpoint 3 and ped ", iPed, " has a restart pos = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 2 ] )
				ENDIF
			ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 1 ])
					vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 1 ]
					PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_LOCATION - on a quick restart checkpoint 2 and ped ", iPed, " has a restart pos = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 1 ] )
				ENDIF
			ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 0 ])
					vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 0 ]
					PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_LOCATION - on a quick restart and ped ", iPed, " has a restart pos = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 0 ] )
				ENDIF
			ENDIF
		ENDIF
						
		IF REPOSITION_COORD_BASED_ON_FORCED_WARP_LOCATION_INDEX_ON_RESPAWN(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sWarpLocationSettings, CREATION_TYPE_PEDS, bIsInitialSpawn, vSpawnLoc, iPed)
	
		ELIF REPOSITION_COORD_BASED_ON_WARP_LOCATIONS_RANDOM_POOL(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sWarpLocationSettings, CREATION_TYPE_PEDS, bIsInitialSpawn, vSpawnLoc #IF IS_DEBUG_BUILD , iPed #ENDIF )
			// don't do ambush stuff below if this is true
					
		ELIF IS_ENTITY_USING_THIS_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].eSpawnConditionFlag, SPAWN_CONDITION_FLAG_AMBUSH_ABILITY_ACTIVATED)
		AND sLocalPlayerAbilities.sAmbushData.iWarpPositionToUse > -1
			vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sWarpLocationSettings.vPosition[sLocalPlayerAbilities.sAmbushData.iWarpPositionToUse]
			PRINTLN("[RCC MISSION][Peds][Ped ", iPed, "][PlayerAbilities][AmbushAbility] MC_GET_PED_SPAWN_LOCATION - Ambush ped using coords from Warp Location ", sLocalPlayerAbilities.sAmbushData.iWarpPositionToUse)
		ENDIF

	ENDIF
	
	ASSIGN_CHECKPOINT_POSITION_FROM_WARP_LOCATION_SETTINGS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sWarpLocationSettings, vSpawnLoc #IF IS_DEBUG_BUILD , CREATION_TYPE_PEDS, iped #ENDIF )	
	
	REPOSITION_ENTITY_RESPAWN_POSITION_FROM_WARP_LOCATION_JUST_USED(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sWarpLocationSettings, bIsInitialSpawn, CREATION_TYPE_PEDS, iPed, vSpawnLoc)
		
	RETURN vSpawnLoc
	
ENDFUNC

FUNC VECTOR MC_GET_PED_SPAWN_LOCATION_FOR_PROXIMITY_SPAWNING(INT iPed)
	
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwelve, ciPed_BSTwelve_UseCheckpointsAsRandomSpawn)
		ASSERTLN("MC_GET_PED_SPAWN_LOCATION_FOR_PROXIMITY_SPAWNING - ciPed_BSTwelve_UseCheckpointsAsRandomSpawn is set on ped ", iPed, " this is not compatible")
		PRINTLN("[PROXSPAWN][Ped ", iPed, "] MC_GET_PED_SPAWN_LOCATION_FOR_PROXIMITY_SPAWNING - ciPed_BSTwelve_UseCheckpointsAsRandomSpawn is set on ped ", iPed, " this is not compatible")
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_PROXIMITY_SPAWNING_INCOMPATIBILITY_PED, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_PED, "ciPed_BSTwelve_UseCheckpointsAsRandomSpawn is set on ped NOT compatible", iPed)
		#ENDIF
		RETURN <<0,0,0>>
	ENDIF
	#ENDIF
	
	INT iTemp
	RETURN MC_GET_PED_SPAWN_LOCATION(iPed, -1, iTemp, TRUE)
	
ENDFUNC

FUNC VECTOR GET_PED_FMMC_SPAWN_LOCATION(INT iPed, BOOL& bIgnoreAreaCheck, INT &iRandomHeadingSelected)
	
	VECTOR vSpawnLoc = MC_GET_PED_SPAWN_LOCATION(iPed, -1, iRandomHeadingSelected)
	
	bIgnoreAreaCheck = FALSE
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sTrainAttachmentData.iTrainIndex != -1
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sTrainAttachmentData.vCarriageOffsetPosition)		
		VECTOR vTemp = <<0.0, 0.0, 0.0>>
		MC_GET_TRAIN_ATTACHMENT_POINT_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sTrainAttachmentData, vSpawnLoc, vTemp)		
		RETURN vSpawnLoc
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vOverrideSpawnPos)
		PRINTLN("[PED] GET_PED_FMMC_SPAWN_LOCATION - Ped ", iPed, " using vOverrideSpawnPos")
		bIgnoreAreaCheck = TRUE
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vOverrideSpawnPos
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vSecondSpawnPos)
	AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedFirstSpawnBitset, iPed) // Isn't this ped's first spawn
		IF GET_RANDOM_INT_IN_RANGE(0, 100) < 50
			SET_BIT(iLocalBoolCheck10, LBOOL10_PED_SPAWN_AT_SECOND_POSITION)
			vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vSecondSpawnPos
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpawnNearType != ciRULE_TYPE_NONE
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpawnNearEntityID > -1
		
		IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFive, ciPed_BSFive_FirstSpawnNotNearEntity))
		OR FMMC_IS_LONG_BIT_SET(MC_serverBD.iObjSpawnPedBitset, iPed) // Gets set once a ped has spawned
			
			VECTOR vNewSpawn = GET_FMMC_ENTITY_LOCATION(vSpawnLoc, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpawnNearType, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpawnNearEntityID)
			
			IF NOT IS_VECTOR_ZERO(vNewSpawn)
				vSpawnLoc = vNewSpawn
				bIgnoreAreaCheck = TRUE
			ENDIF
			
		ENDIF
	ENDIF
	
	IF( IS_THIS_A_QUICK_RESTART_JOB() 
	OR IS_CORONA_INITIALISING_A_QUICK_RESTART() )
		IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
			IF NOT IS_VECTOR_ZERO( g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 3 ] )
				vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 3 ]
				PRINTLN("[RCC MISSION] GET_PED_FMMC_SPAWN_LOCATION - on a quick restart and ped ", iPed, " has a restart vector = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 3 ] )
			ENDIF
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2)
			IF NOT IS_VECTOR_ZERO( g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 2 ] )
				vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 2 ]
				PRINTLN("[RCC MISSION] GET_PED_FMMC_SPAWN_LOCATION - on a quick restart and ped ", iPed, " has a restart vector = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 2 ] )
			ENDIF
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
			IF NOT IS_VECTOR_ZERO( g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 1 ] )
				vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 1 ]
				PRINTLN("[RCC MISSION] GET_PED_FMMC_SPAWN_LOCATION - on a quick restart and ped ", iPed, " has a restart vector = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 1 ] )
			ENDIF
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
			IF NOT IS_VECTOR_ZERO( g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 0 ] )
				vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 0 ]
				PRINTLN("[RCC MISSION] GET_PED_FMMC_SPAWN_LOCATION - on a quick restart and ped ", iPed, " has a restart vector = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 0 ] )
			ENDIF
		ENDIF
	ENDIF
	 	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_UseRandomPoolPosAsRespawnSpawn)	
		IF iSpawnPoolPedIndex = -1
			iSpawnPoolPedIndex = GET_RANDOM_SELECTION_FROM_SPAWN_POOL(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sWarpLocationSettings.vPosition #IF IS_DEBUG_BUILD , iPed, CREATION_TYPE_PEDS #ENDIF )
		ENDIF
		IF iSpawnPoolPedIndex != -1
			vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sWarpLocationSettings.vPosition[iSpawnPoolPedIndex]			
		ENDIF
		PRINTLN( "[RCC MISSION] GET_PED_FMMC_SPAWN_LOCATION - Grabbing random position from pool for ped ", iPed, " iSpawnPoolPedIndex: ", iSpawnPoolPedIndex, " vSpawnLoc: ", vSpawnLoc)
	ENDIF
	
	RETURN vSpawnLoc
	
ENDFUNC

FUNC FLOAT MC_GET_PED_SPAWN_HEADING(INT iPed, INT iServerPropertyIDPassedIn, BOOL bSecondarySpawn = FALSE, INT iSpawnRandomHeadingToSelectPassedIn = 0, BOOL bIsInitialSpawn = FALSE)
	
	FLOAT fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fHead
	
	IF SHOULD_ENTITY_USE_CHECKPOINT_CONTINUITY_VARS(ciENTITY_TYPE_PED, iPed)
		FLOAT fEntityCheckpointHeading = GET_HEADING_FOR_ENTITY_CHECKPOINT_CONTINUITY(ciENTITY_TYPE_PED, iPed)
		// This ped has got a heading saved from the last checkpoint!
		PRINTLN("[CONTINUITY][EntityCheckpointContinuity][Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_HEADING - using saved checkpoint heading ", fEntityCheckpointHeading)
		RETURN fEntityCheckpointHeading
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sTrainAttachmentData.iTrainIndex != -1
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sTrainAttachmentData.vCarriageOffsetPosition)		
		VECTOR vTempPos = <<0.0, 0.0, 0.0>>
		VECTOR vTempRot = <<0.0, 0.0, 0.0>>
		MC_GET_TRAIN_ATTACHMENT_POINT_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sTrainAttachmentData, vTempPos, vTempRot)
		fSpawnHeading = vTempRot.z
		RETURN fSpawnHeading
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_DynamicApartmentSpawnOverride)
	AND ( Is_Player_Currently_On_MP_Heist(PLAYER_ID()) OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID()) OR IS_FAKE_MULTIPLAYER_MODE_SET() )
		
		INT iPropertyIndex = -1
		
		IF IS_FAKE_MULTIPLAYER_MODE_SET()
			iPropertyIndex = 1
		ELSE
			iPropertyIndex = iServerPropertyIDPassedIn
		ENDIF
		
		IF iPropertyIndex > 0
		AND iPropertyIndex < MAX_MP_PROPERTIES
			fSpawnHeading = GET_HEIST_PROPERTY_SPAWN_HEADING(iPropertyIndex, FALSE)
		ENDIF
		
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vOverrideSpawnPos)
		PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_HEADING - using fOverrideHeading")
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fOverrideHeading
	ENDIF
	
	IF bSecondarySpawn
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vSecondSpawnPos)
		fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fSecondHeading
	ENDIF
	
	
	IF( IS_THIS_A_QUICK_RESTART_JOB() 
	OR IS_CORONA_INITIALISING_A_QUICK_RESTART() )
		IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 3 ])
				fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].fRestartHeading[ 3 ]
				PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_HEADING - on a quick restart checkpoint 4 and ped ", iPed, " has a restart heading = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].fRestartHeading[ 3 ] )
			ENDIF
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2)
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 2 ])
				fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].fRestartHeading[ 2 ]
				PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_HEADING - on a quick restart checkpoint 3 and ped ", iPed, " has a restart heading = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].fRestartHeading[ 2 ] )
			ENDIF
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 1 ])
				fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].fRestartHeading[ 1 ]
				PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_HEADING - on a quick restart checkpoint 2 and ped ", iPed, " has a restart heading = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].fRestartHeading[ 1 ] )
			ENDIF
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 0 ])
				fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].fRestartHeading[ 0 ]
				PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_HEADING - on a quick restart and ped ", iPed, " has a restart heading = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].fRestartHeading[ 0 ] )
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwelve, ciPed_BSTwelve_UseCheckpointsAsRandomSpawn)
		PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_HEADING - RANDOM - Picking random spawn heading - iSpawnRandomHeadingToSelectPassedIn: ", iSpawnRandomHeadingToSelectPassedIn)
		
		#IF IS_DEBUG_BUILD		
		IF g_iMissionForcedCheckpointSpawn != -1
			iSpawnRandomHeadingToSelectPassedIn = g_iMissionForcedCheckpointSpawn
			PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_HEADING - RANDOM - Overriding the random checkpoint spawn selection with rag widget value: ", g_iMissionForcedCheckpointSpawn)
		ENDIF
		#ENDIF
		
		SWITCH iSpawnRandomHeadingToSelectPassedIn
			CASE 0
				fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fHead
				PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_HEADING - RANDOM - Spawning at placed location ", fSpawnHeading)
			BREAK
			CASE 1
				fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fRestartHeading[ 0 ]
				PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_HEADING - RANDOM - Spawning at quick restart point 1 ", fSpawnHeading)
			BREAK
			CASE 2
				fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fRestartHeading[ 1 ]
				PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_HEADING - RANDOM - Spawning at quick restart point 2 ", fSpawnHeading)
			BREAK
			CASE 3
				fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fRestartHeading[ 2 ]
				PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_HEADING - RANDOM - Spawning at quick restart point 3 ", fSpawnHeading)
			BREAK
			CASE 4
				fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fRestartHeading[ 3 ]
				PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_HEADING - RANDOM - Spawning at quick restart point 4 ", fSpawnHeading)
			BREAK
			DEFAULT
				PRINTLN("[Peds][Ped ", iPed, "] - MC_GET_PED_SPAWN_HEADING - RANDOM - Not Using this.")
			BREAK
		ENDSWITCH	
	ENDIF
	
	IF REPOSITION_HEADING_BASED_ON_FORCED_WARP_LOCATION_INDEX_ON_RESPAWN(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sWarpLocationSettings, CREATION_TYPE_PEDS, bisInitialSpawn, fSpawnHeading, iped)
		
	ELIF REPOSITION_HEADING_BASED_ON_WARP_LOCATIONS_RANDOM_POOL(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sWarpLocationSettings, CREATION_TYPE_PEDS, bIsInitialSpawn, fSpawnHeading #IF IS_DEBUG_BUILD , iped #ENDIF )
		// Don't do ambush stuff below if this is true.
		
	ELIF IS_ENTITY_USING_THIS_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].eSpawnConditionFlag, SPAWN_CONDITION_FLAG_AMBUSH_ABILITY_ACTIVATED)
	AND sLocalPlayerAbilities.sAmbushData.iWarpPositionToUse > -1
		fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sWarpLocationSettings.fHeading[sLocalPlayerAbilities.sAmbushData.iWarpPositionToUse]
		PRINTLN("[Peds][Ped ", iPed, "][PlayerAbilities][AmbushAbility] - MC_GET_PED_SPAWN_HEADING - Ambush ped using heading from Warp Location ", sLocalPlayerAbilities.sAmbushData.iWarpPositionToUse)
		
	ENDIF	
	
	ASSIGN_CHECKPOINT_HEADING_FROM_WARP_LOCATION_SETTINGS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sWarpLocationSettings, fSpawnHeading  #IF IS_DEBUG_BUILD , CREATION_TYPE_PEDS, iPed #ENDIF )	
	
	REPOSITION_ENTITY_RESPAWN_HEADING_FROM_WARP_LOCATION_JUST_USED(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sWarpLocationSettings, bIsInitialSpawn, CREATION_TYPE_PEDS, iPed, fSpawnHeading)			
	
	RETURN fSpawnHeading
	
ENDFUNC

PROC CACHE_PED_RESPAWN_BLOCKED(INT i)
	IF NOT IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iMiscBS, ciRandomSpawnGroupBS_HasBeenSet)
		EXIT
	ENDIF	
	FMMC_SET_LONG_BIT(iPedRespawnIsBlockedBS, i)
	PRINTLN("[MCSpawning][Peds][Ped ", i, "] SHOULD_PED_RESPAWN_NOW - CACHE_PED_RESPAWN_BLOCKED - Caching that the Ped has been blocked from respawning.")
ENDPROC

PROC CACHE_PED_SHOULD_NOT_RESPAWN_THIS_FRAME(INT i)
	FMMC_SET_LONG_BIT(iPedRespawnIsBlockedThisFrameBS, i)
ENDPROC

PROC CACHE_PED_SHOULD_RESPAWN_THIS_FRAME(INT i)
	FMMC_SET_LONG_BIT(iPedShouldRespawnNowBS, i)
ENDPROC

FUNC BOOL SHOULD_PED_RESPAWN_NOW(FMMC_PED_STATE &sPedState)
	
	INT i = sPedState.iIndex
	
	IF FMMC_IS_LONG_BIT_SET(iPedRespawnIsBlockedBS, i)
		RETURN FALSE
	ENDIF	
	IF FMMC_IS_LONG_BIT_SET(iPedRespawnIsBlockedThisFrameBS, i)
		RETURN FALSE
	ENDIF	
	IF FMMC_IS_LONG_BIT_SET(iPedShouldRespawnNowBS, i)
		RETURN TRUE
	ENDIF
		
	IF MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].eSpawnConditionFlag, FALSE, FALSE, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAggroIndexBS_Entity_Ped)
		IF MC_ARE_SPAWN_CONDITION_FLAGS_MET_DURING_GAMEPLAY(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].eSpawnConditionFlag)
			CACHE_PED_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
		ELSE
			CACHE_PED_RESPAWN_BLOCKED(i)
		ENDIF
		
		PRINTLN("[MCSpawning][Peds][Ped ", i, "] SHOULD_PED_RESPAWN_NOW - Returning FALSE due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_PedDeathTracking)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iContinuityId > -1
		AND FMMC_IS_LONG_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iPedDeathBitset, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iContinuityId)
			PRINTLN("[MCSpawning][Peds][Ped ", i, "][JS][CONTINUITY] - SHOULD_PED_RESPAWN_NOW - with continuity ID ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iContinuityId, " not spawning due to death of previous stand mission")
			CACHE_PED_RESPAWN_BLOCKED(i)
			RETURN FALSE
		ENDIF
	ENDIF
	
	//Don't respawn if you were told to not respawn after an objective that is before this objective
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iStopRespawningRule != -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iStopRespawningTeam != -1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iStopRespawningRule <= MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iStopRespawningTeam]
				
				IF MC_serverBD_2.iCurrentPedRespawnLives[i] > 0
					MC_serverBD_2.iCurrentPedRespawnLives[i] = 0
					PRINTLN("[MCSpawning][Peds][Ped ", i, "] SHOULD_PED_RESPAWN_NOW - setting lives for ped ", i, " to 0 because it shouldn't respawn past rule ", MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iStopRespawningTeam])
				ENDIF
				
				PRINTLN("[MCSpawning][Peds][Ped ", i, "] SHOULD_PED_RESPAWN_NOW - Refusing ped spawn for ped ", i, " because we are past the rule where they were told to clean up")
				
				CACHE_PED_RESPAWN_BLOCKED(i)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF SHOULD_CLEANUP_PED(sPedState)
		PRINTLN("[MCSpawning][Peds][Ped ", i, "] SHOULD_PED_RESPAWN_NOW - SHOULD_CLEANUP_PED is TRUE")
		CACHE_PED_SHOULD_NOT_RESPAWN_THIS_FRAME(i) // could move back a rule.
		RETURN FALSE
	ENDIF
	
	IF NOT MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_PED(i, GET_TOTAL_STARTING_PLAYERS())
		PRINTLN("[MCSpawning][Peds][Ped ", i, "] SHOULD_PED_RESPAWN_NOW - MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_PED = FALSE")
		CACHE_PED_RESPAWN_BLOCKED(i)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_PROXIMITY_BLOCKING_SPAWN_ARRAYED(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sProximitySpawningData, i, MC_serverBD_3.iProximitySpawning_PedSpawnAllBS, MC_serverBD_3.iProximitySpawning_PedSpawnAnyBS, MC_serverBD_3.iProximitySpawning_PedSpawnOneBS, MC_serverBD_3.iProximitySpawning_PedCleanupBS)
		PRINTLN("[PROXSPAWN_SERVER][MCSpawning][Peds][Ped ", i, "] SHOULD_PED_RESPAWN_NOW - IS_PLAYER_PROXIMITY_BLOCKING_SPAWN")
		CACHE_PED_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fPedPlayerSpawnBlockrange > 0
		INT iPlayer = -1
		BOOL bIgnoreAreaCheckTemp
		INT iRandomHeadingTemp
		WHILE DO_PARTICIPANT_LOOP(iPlayer, DPLF_CHECK_PED_ALIVE | DPLF_FILL_PLAYER_IDS)			
			IF (VDIST2(GET_PED_FMMC_SPAWN_LOCATION(i, bIgnoreAreaCheckTemp, iRandomHeadingTemp), GET_ENTITY_COORDS(piParticipantLoop_PedIndex)) < POW(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fPedPlayerSpawnBlockrange, 2))
				PRINTLN("[MCSpawning][Peds][Ped ", i, "] SHOULD_PED_RESPAWN_NOW - Participant ", iPlayer, " is too close. Not spawning due to fPedPlayerSpawnBlockrange")
				CACHE_PED_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
				RETURN FALSE
			ENDIF
		ENDWHILE
	ENDIF
	
	IF DOES_PED_HAVE_PLAYER_VARIABLE_SETTING(i)
		IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedObjective > -1
			AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedTeam > -1
				IF MC_serverBD_1.sCreatedCount.iNumPedCreated[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedObjective] >= MC_serverBD.iNumberOfPart[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedTeam]
					PRINTLN("[MCSpawning][Peds][Ped ", i, "] SHOULD_PED_RESPAWN_NOW - RETURN FALSE, number create is more than players : ",MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedTeam], " FOR TEAM: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedTeam)
					FMMC_SET_LONG_BIT(MC_serverBD_1.sCreatedCount.iSkipPedBitset, i)
					CACHE_PED_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_Ped)
		PRINTLN("[MCSpawning][Peds][Ped ", i, "] SHOULD_PED_RESPAWN_NOW - RETURN FALSE, Ped does not have suitable spawn group")
		CACHE_PED_RESPAWN_BLOCKED(i)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sTrainAttachmentData.iTrainIndex != -1
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sTrainAttachmentData.vCarriageOffsetPosition)
		IF NOT IS_TRAIN_ATTACHMENT_READY(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sTrainAttachmentData)
			PRINTLN("[MCSpawning][Peds][Ped ", i, "] SHOULD_PED_RESPAWN_NOW - RETURN FALSE, Waiting for Train: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sTrainAttachmentData.iTrainIndex, " to be ready.")
			CACHE_PED_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF MC_IS_ENTITY_SPAWN_TRIGGERED_BY_ZONE_ENTRY(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iZoneBlockEntitySpawn)
		IF NOT HAS_ENTITY_SPAWN_ZONE_REQUIREMENT_BEEN_MET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iZoneBlockEntitySpawn, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iZoneBlockEntitySpawnPlayerReq)
			PRINTLN("[MCSpawning][Peds][Ped ", i, "] SHOULD_PED_RESPAWN_NOW - RETURN FALSE, HAS_ENTITY_SPAWN_ZONE_REQUIREMENT_BEEN_MET = FALSE.. iZone: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iZoneBlockEntitySpawn, " iPlayerReq: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iZoneBlockEntitySpawnPlayerReq)
			CACHE_PED_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCSRespawnSceneIndex > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCSRespawnShotIndex > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCSRespawnSceneIndex = MC_serverBD.iSpawnScene
		AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCSRespawnShotIndex <= MC_serverBD.iSpawnShot
		AND (iCamShotLoaded >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCSRespawnShotIndex AND IS_BIT_SET(iLocalBoolCheck7, LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED))
			CACHE_PED_SHOULD_RESPAWN_THIS_FRAME(i)
			RETURN TRUE
		ELSE
			PRINTLN("[MCSpawning][Peds][Ped ", i, "] SHOULD_PED_RESPAWN_NOW Ped - RETURNING FALSE - iCSRespawnSceneIndex: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCSRespawnSceneIndex, " SpawnScene: ", MC_serverBD.iSpawnScene,
					" iCSRespawnShotIndex: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCSRespawnShotIndex, " SpawnShot: ", MC_serverBD.iSpawnShot, 
					" Camshot: ", iCamShotLoaded, " LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED: ", IS_BIT_SET(iLocalBoolCheck7, LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED))
			CACHE_PED_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetSeventeen, ciPed_BSSeventeen_SpawnedByEvent)
		IF NOT FMMC_IS_LONG_BIT_SET(iDroppedPedBS, i)
			PRINTLN("[MCSpawning][Peds][Ped ", i, "] SHOULD_PED_RESPAWN_NOW Ped - RETURNING FALSE - This ped is set to be spawned by an event, and iDroppedPedBS hasn't been set for them yet")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle].iVehicleInVehicle_ContainerVehicleIndex > -1
			IF iVehicleInVehicleState != ciVEHICLE_IN_VEHICLE_STATE__WAITING_FOR_PEDS
				PRINTLN("[MCSpawning][Peds][Ped ", i, "][VehicleInVehicle] SHOULD_PED_RESPAWN_NOW Ped - RETURNING FALSE - It's not time to spawn in the MOC peds yet")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.sPlayerAbilities.sVehicleRepair.iMechanicPedIndex = i
		// This ped is the engineer for the Repair ability
		ENTITY_INDEX eiRepairDynoprop = GET_FMMC_ENTITY(ciENTITY_TYPE_DYNO_PROPS, g_FMMC_STRUCT.sPlayerAbilities.sVehicleRepair.iDynopropIndex)
		IF NOT DOES_ENTITY_EXIST(eiRepairDynoprop)
			PRINTLN("[MCSpawning][Peds][Ped ", i, "] SHOULD_PED_RESPAWN_NOW Ped - RETURNING FALSE - The Repair dynoprop hasn't spawned yet!")
			RETURN FALSE
		ENDIF
	ENDIF
		
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_PEDS, i, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedAlwaysForceSpawnOnRule, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAggroIndexBS_Entity_Ped, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedObjectiveEnd)
			CACHE_PED_SHOULD_RESPAWN_THIS_FRAME(i)
			RETURN TRUE
		ELSE
			#IF IS_DEBUG_BUILD
			IF IS_FRAME_COUNT_STAGGER_READY(ciFRAME_STAGGERED_DEBUG_PED)
				PRINTLN("[MCSpawning][Peds][Ped ", i, "] SHOULD_PED_RESPAWN_NOW - RETURN FALSE, HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS = FALSE, ", 
				" iAssociatedSpawn: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedSpawn,
				" iAssociatedTeam: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedTeam,
				" iAssociatedObjective: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedObjective,
				" iAssociatedScoreRequired: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedScoreRequired,
				" iAssociatedAlwaysForceSpawnOnRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedAlwaysForceSpawnOnRule,
				" iAssociatedObjectiveEnd: "  , g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedObjectiveEnd)
			ENDIF
			#ENDIF
		ENDIF
	ELSE
		CACHE_PED_SHOULD_RESPAWN_THIS_FRAME(i)
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_PEDS, i, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAlwaysForceSpawnOnRule, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAggroIndexBS_Entity_Ped, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondiRuleEnd)
			CACHE_PED_SHOULD_RESPAWN_THIS_FRAME(i)
			RETURN TRUE
		ELSE
			#IF IS_DEBUG_BUILD
			IF IS_FRAME_COUNT_STAGGER_READY(ciFRAME_STAGGERED_DEBUG_PED)
				PRINTLN("[MCSpawning][Peds][Ped ", i, "] SHOULD_PED_RESPAWN_NOW - RETURN FALSE, HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS = FALSE, ", 
				" iSecondAssociatedSpawn: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedSpawn,
				" iSecondAssociatedTeam: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedTeam,
				" iSecondAssociatedObjective: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedObjective, 
				" iSecondScoreRequired: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondScoreRequired,
				" iSecondAlwaysForceSpawnOnRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAlwaysForceSpawnOnRule,
				" iSecondiRuleEnd: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondiRuleEnd)	
			ENDIF
			#ENDIF
		ENDIF	
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_PEDS, i, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAlwaysForceSpawnOnRule, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAggroIndexBS_Entity_Ped, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdiRuleEnd)
			CACHE_PED_SHOULD_RESPAWN_THIS_FRAME(i)
			RETURN TRUE
		ELSE
			#IF IS_DEBUG_BUILD
			IF IS_FRAME_COUNT_STAGGER_READY(ciFRAME_STAGGERED_DEBUG_PED)
				PRINTLN("[MCSpawning][Peds][Ped ", i, "] SHOULD_PED_RESPAWN_NOW - RETURN FALSE, HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS = FALSE, ", 
				" iThirdAssociatedSpawn: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedSpawn,
				" iThirdAssociatedTeam: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedTeam,
				" iThirdAssociatedObjective: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedObjective,
				" iThirdScoreRequired: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdScoreRequired,
				" iThirdAlwaysForceSpawnOnRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAlwaysForceSpawnOnRule,
				" iThirdiRuleEnd: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdiRuleEnd)
			ENDIF
			#ENDIF
		ENDIF	
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_PEDS, i, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAlwaysForceSpawnOnRule, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAggroIndexBS_Entity_Ped, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthiRuleEnd)
			CACHE_PED_SHOULD_RESPAWN_THIS_FRAME(i)
			RETURN TRUE
		ELSE
			#IF IS_DEBUG_BUILD
			IF IS_FRAME_COUNT_STAGGER_READY(ciFRAME_STAGGERED_DEBUG_PED)
				PRINTLN("[MCSpawning][Peds][Ped ", i, "] SHOULD_PED_RESPAWN_NOW - RETURN FALSE, HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS = FALSE, ", 
				" iFourthAssociatedSpawn: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedSpawn,
				" iFourthAssociatedTeam: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedTeam,
				" iFourthAssociatedObjective: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedObjective,
				" iFourthScoreRequired: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthScoreRequired,
				" iFourthAlwaysForceSpawnOnRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAlwaysForceSpawnOnRule,
				" iFourthiRuleEnd: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthiRuleEnd)
			ENDIF
			#ENDIF
		ENDIF	
	ENDIF
	
	CACHE_PED_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
	RETURN FALSE

ENDFUNC

FUNC BOOL CHECK_IF_PED_SHOULD_SPAWN_NOW_FOR_VEHICLE_SPAWN(INT iPed)
	FMMC_PED_STATE sPedState
	FILL_FMMC_PED_STATE_STRUCT(sPedState, iPed)
	
	IF NOT sPedState.bExists
	AND MC_serverBD_2.iCurrentPedRespawnLives[iped] > 0
	AND (shouldPedRespawnNowCallback != NULL AND CALL shouldPedRespawnNowCallback(sPedState))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_NEXT_SPAWNING_PED_AFTER_VEHICLE_SPAWN(INT iVeh)
	
	INT iped
	
	INT iDriverPedFound = -1
	INT iAnySeatPedFound = -1
	INT iForcedOtherSeatPedFound = -1
	
	FOR iped = 0 TO (MC_serverBD.iNumPedCreated - 1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle = iVeh
		AND SHOULD_PED_SPAWN_IN_VEHICLE(iped)
			
			IF CHECK_IF_PED_SHOULD_SPAWN_NOW_FOR_VEHICLE_SPAWN(iPed)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicleSeat = ENUM_TO_INT(VS_DRIVER)
					iDriverPedFound = iped
					BREAKLOOP // We've found the driver, we don't need to look for anyone else. Break out!
					
				ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicleSeat = -3
					IF iAnySeatPedFound = -1
						iAnySeatPedFound = iped
					ENDIF
					
				ELSE
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iForcedVehicleSeat != -1
						IF iForcedOtherSeatPedFound = -1
							iForcedOtherSeatPedFound = iped
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDFOR
	
	IF iDriverPedFound != -1
		PRINTLN("[RCC MISSION] GET_NEXT_SPAWNING_PED_AFTER_VEHICLE_SPAWN - found driver ",iDriverPedFound)
		IF setPedToSpawnNext != NULL
			CALL setPedToSpawnNext(iDriverPedFound)
		ENDIF
	ELIF iAnySeatPedFound != -1
		PRINTLN("[RCC MISSION] GET_NEXT_SPAWNING_PED_AFTER_VEHICLE_SPAWN - found ped in any seat ",iAnySeatPedFound)
		IF setPedToSpawnNext != NULL
			CALL setPedToSpawnNext(iAnySeatPedFound)
		ENDIF
	ELIF iForcedOtherSeatPedFound != -1
		PRINTLN("[RCC MISSION] GET_NEXT_SPAWNING_PED_AFTER_VEHICLE_SPAWN - found ped forced into other seat ",iForcedOtherSeatPedFound)
		IF setPedToSpawnNext != NULL
			CALL setPedToSpawnNext(iForcedOtherSeatPedFound)
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[RCC MISSION] GET_NEXT_SPAWNING_PED_AFTER_VEHICLE_SPAWN - Didn't find any peds for vehicle ",iveh)
	#ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL MC_IS_MODEL_AMBIENT_MISSION_COP(MODEL_NAMES mn)
	SWITCH mn
		CASE S_M_Y_COP_01
		CASE S_F_Y_COP_01
		CASE S_M_Y_SWAT_01
		CASE S_M_M_FIBOFFICE_01
		CASE S_M_M_FIBOFFICE_02
		CASE S_M_M_FIBSEC_01
		CASE S_M_Y_SHERIFF_01
		CASE S_F_Y_SHERIFF_01
		CASE S_M_Y_Ranger_01
		CASE S_F_Y_Ranger_01
		CASE S_M_Y_BlackOps_01
		CASE S_M_Y_BlackOps_02
		CASE S_M_Y_BlackOps_03
		CASE S_M_M_CCREW_01
		CASE S_M_M_ARMOURED_01
		CASE S_M_Y_Pilot_01
		CASE S_M_M_Pilot_02
		CASE S_M_Y_Marine_03
		CASE S_M_M_CIASec_01
		CASE S_M_M_chemsec_01
		CASE S_M_M_PrisGuard_01
		CASE S_M_M_Security_01	
		CASE S_M_M_HighSec_01
		CASE A_M_Y_Business_01
		CASE A_F_Y_Business_02
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MC_IS_PED_A_SCRIPTED_COP(INT iPedNumber)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThree, ciPED_BSThree_OverrideNotACop)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThree, ciPED_BSThree_OverrideIsACop)
		RETURN TRUE
	ENDIF
	
	IF MC_IS_MODEL_AMBIENT_MISSION_COP(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC TEXT_LABEL_15 MC_GET_PED_AMBIENT_VOICE(INT idialogue)

	TEXT_LABEL_23 sRoot = g_FMMC_STRUCT.sDialogueTriggers[idialogue].tlRoot
	IF IS_STRING_NULL_OR_EMPTY(sRoot)
	OR IS_STRING_BLANK_SPACES(sRoot, GET_LENGTH_OF_LITERAL_STRING(sRoot))
		sRoot = GET_DIALOGUE_LABEL_FROM_ID(g_FMMC_STRUCT.sDialogueTriggers[idialogue].iFileID, g_FMMC_STRUCT.sDialogueTriggers[idialogue].iDialogueID)
	ENDIF
	INT ispeaker = GET_SPEAKER_INT_FROM_ROOT(sRoot, FMMC_MAX_SPEAKERS)
	TEXT_LABEL_15 sSpeaker
	sSpeaker = g_FMMC_STRUCT.sSpeakers[ispeaker]
	
	PRINTLN("LMCHECK - MC_GET_PED_AMBIENT_VOICE - sSpeaker: ", sSpeaker, " ispeaker: ", ispeaker, " iDialogue: ", iDialogue, " iFileID: ", g_FMMC_STRUCT.sDialogueTriggers[idialogue].iFileID, " tlRoot: ", g_FMMC_STRUCT.sDialogueTriggers[idialogue].tlRoot, " iDialogueID: ", g_FMMC_STRUCT.sDialogueTriggers[idialogue].iDialogueID)
	
	IF IS_STRING_NULL_OR_EMPTY(sSpeaker)
	AND g_FMMC_STRUCT.sDialogueTriggers[idialogue].iFileID != -1
		PRINTLN("LMCHECK - FALLBACK - MC_GET_PED_AMBIENT_VOICE - iDialogue: ", iDialogue, " iFileID: ", g_FMMC_STRUCT.sDialogueTriggers[idialogue].iFileID, " tlRoot: ", g_FMMC_STRUCT.sDialogueTriggers[idialogue].tlRoot)
		sSpeaker = GET_PED_VOICE_ID(g_FMMC_STRUCT.sDialogueTriggers[idialogue].iFileID, g_FMMC_STRUCT.sDialogueTriggers[idialogue].tlRoot)
	ENDIF
	
	RETURN sSpeaker

ENDFUNC

#IF IS_DEBUG_BUILD
PROC MC_GIVE_PED_DEBUG_NAME(PED_INDEX piPassed, INT iPedNumber)
	
	TEXT_LABEL_23 tl23
	
	tl23 = "Ped Number: "
	tl23 += iPedNumber 
	SET_PED_NAME_DEBUG(piPassed, tl23)
	
ENDPROC
#ENDIF

FUNC BOOL MC_SHOULD_PED_SPAWN_INVISIBLE(INT iPed, PED_INDEX piPedIndex)

	INT iTeamToCheck = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCloakTeamIndex

	IF iTeamToCheck = -1
		RETURN FALSE
	ENDIF
	
	//Exit if the feature isn't used or if the current rule index is invalid
	IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iBecomeCloakedOnRule < 0) OR (gBG_MC_serverBD_VARS.iCurrentHighestPriority[iTeamToCheck] >= FMMC_MAX_RULES) OR NOT DOES_ENTITY_EXIST(piPedIndex)
		RETURN FALSE
	ENDIF
	
	//If the current rule number is higher than the "become cloaked on this rule", you might want to be cloaked right now
	BOOL bCurrentlyInvisible = (gBG_MC_serverBD_VARS.iCurrentHighestPriority[iTeamToCheck] >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iBecomeCloakedOnRule)
	
	//Unless the rule number has gone past the "remove cloak" rule number (if it's larger than -1)
	IF (gBG_MC_serverBD_VARS.iCurrentHighestPriority[iTeamToCheck] >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iLoseCloakOnRule) AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iLoseCloakOnRule > -1)
		bCurrentlyInvisible = FALSE
	ENDIF
	
	RETURN bCurrentlyInvisible
ENDFUNC

FUNC STRING MC_GET_DEAD_PED_ANIM_DICT_NAME(INT iDeadAnim)
	SWITCH iDeadAnim
		CASE ci_DEAD_PED_ANIM_FBI_CPR_IDLE			RETURN "missfbi1"
		CASE ci_DEAD_PED_ANIM_FBI_DEAD_C			RETURN "missfbi5ig_12"
		CASE ci_DEAD_PED_ANIM_DEAD_A				RETURN "dead"
		CASE ci_DEAD_PED_ANIM_DEAD_B				RETURN "dead"
		CASE ci_DEAD_PED_ANIM_DEAD_C				RETURN "dead"
		CASE ci_DEAD_PED_ANIM_DEAD_D				RETURN "dead"
		CASE ci_DEAD_PED_ANIM_DEAD_E				RETURN "dead"
		CASE ci_DEAD_PED_ANIM_DEAD_F				RETURN "dead"
		CASE ci_DEAD_PED_ANIM_DEAD_G				RETURN "dead"
		CASE ci_DEAD_PED_ANIM_DEAD_H				RETURN "dead"
		CASE ci_DEAD_PED_ANIM_IAA_MORGUE_SEARCH		RETURN "anim@GangOps@Morgue@Table@"
	ENDSWITCH
	RETURN "missfbi1"
ENDFUNC
FUNC STRING MC_GET_DEAD_PED_ANIM_NAME(INT iDeadAnim)
	SWITCH iDeadAnim
		CASE ci_DEAD_PED_ANIM_FBI_CPR_IDLE 			RETURN "cpr_pumpchest_idle"
		CASE ci_DEAD_PED_ANIM_FBI_DEAD_C 			RETURN "dead_c"
		CASE ci_DEAD_PED_ANIM_DEAD_A 				RETURN "dead_a"
		CASE ci_DEAD_PED_ANIM_DEAD_B 				RETURN "dead_b"
		CASE ci_DEAD_PED_ANIM_DEAD_C 				RETURN "dead_c"
		CASE ci_DEAD_PED_ANIM_DEAD_D 				RETURN "dead_d"
		CASE ci_DEAD_PED_ANIM_DEAD_E 				RETURN "dead_e"
		CASE ci_DEAD_PED_ANIM_DEAD_F 				RETURN "dead_f"
		CASE ci_DEAD_PED_ANIM_DEAD_G 				RETURN "dead_g"
		CASE ci_DEAD_PED_ANIM_DEAD_H 				RETURN "dead_h"
		CASE ci_DEAD_PED_ANIM_IAA_MORGUE_SEARCH 	RETURN "Body_Search"
	ENDSWITCH
	RETURN "cpr_pumpchest_idle"
ENDFUNC

//////////////////////////////      Making PEDS           //////////////////////////////

PROC MC_APPLY_PED_COMBAT_STYLE(PED_INDEX piPassed, INT iCombatStyle, WEAPON_TYPE wtGun, INT iPedNumber, BOOL bOnCombatGoto = FALSE)
	
	UNUSED_PARAMETER(iPedNumber)
	
	IF iCombatStyle = ciPED_AGRESSIVE
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_ALWAYS_FIGHT, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, TRUE)
		IF NOT bOnCombatGoto
			SET_PED_COMBAT_MOVEMENT(piPassed, CM_WILLADVANCE)
		ENDIF
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_CHARGE, TRUE)
		PRINTLN("[RCC MISSION] MC_APPLY_PED_COMBAT_STYLE - setting ped to agressive: ", iPedNumber)
	ELIF iCombatStyle = ciPED_BERSERK
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_ALWAYS_FIGHT, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, TRUE)
		IF NOT bOnCombatGoto
			SET_PED_COMBAT_MOVEMENT(piPassed, CM_WILLADVANCE)
		ENDIF
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_CHARGE, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_PERMIT_CHARGE_BEYOND_DEFENSIVE_AREA, TRUE)
		PRINTLN("[RCC MISSION] MC_APPLY_PED_COMBAT_STYLE - setting ped to berserk: ", iPedNumber)
	ELSE // ciPED_DEFENSIVE
		SET_PED_COMBAT_MOVEMENT(piPassed, CM_DEFENSIVE)
		IF wtGun = WEAPONTYPE_UNARMED
			SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_ALWAYS_FLEE, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_ALWAYS_FIGHT, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, FALSE)
			PRINTLN("[RCC MISSION] MC_APPLY_PED_COMBAT_STYLE - setting ped to defensive unarmed: ", iPedNumber)
		ELSE
			SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_ALWAYS_FIGHT, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, TRUE)
			PRINTLN("[RCC MISSION] MC_APPLY_PED_COMBAT_STYLE - setting ped to defensive armed: ", iPedNumber)
		ENDIF
	ENDIF
	
ENDPROC

FUNC INT MC_GET_PED_NUMBER_OF_CHECKPOINT_SPAWN_POSITIONS(INT iPed)
	
	INT iCheckpoint
	INT iSpawnPositionsFound
	
	FOR iCheckpoint = 0 TO (FMMC_MAX_RESTART_CHECKPOINTS - 1)
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vRestartPos[iCheckpoint])
			iSpawnPositionsFound++
		ELSE
			RETURN iSpawnPositionsFound
		ENDIF
	ENDFOR
	
	RETURN iSpawnPositionsFound
	
ENDFUNC

PROC MC_GIVE_PED_A_FLASHLIGHT_ATTACHMENT(PED_INDEX piPassed, INT iPedNumber)
	WEAPON_TYPE wtGun = GET_PED_WEAPON_TYPE_FROM_CREATOR_OPTION(iPedNumber)
	
	IF wtGun = WEAPONTYPE_PISTOL
	OR wtGun = WEAPONTYPE_APPISTOL
	OR wtGun = WEAPONTYPE_DLC_PISTOL50
	OR wtGun = WEAPONTYPE_MICROSMG
	OR wtGun = WEAPONTYPE_DLC_HEAVYPISTOL
	OR wtGun = WEAPONTYPE_DLC_SNSPISTOL
	OR wtGun = WEAPONTYPE_DLC_VINTAGEPISTOL
	OR wtGun = WEAPONTYPE_DLC_MARKSMANPISTOL
	
		IF HAS_PED_GOT_WEAPON_COMPONENT(piPassed, wtGun, WEAPONCOMPONENT_AT_PI_FLSH)
			EXIT
		ENDIF
	
		GIVE_WEAPON_COMPONENT_TO_PED(piPassed, wtGun, WEAPONCOMPONENT_AT_PI_FLSH)
		PRINTLN("MC_GIVE_PED_A_FLASHLIGHT_ATTACHMENT - Giving ped ", iPedNumber, " a WEAPONCOMPONENT_AT_PI_FLSH")
		
	ELIF wtGun = WEAPONTYPE_SMG
	OR wtGun = WEAPONTYPE_ASSAULTRIFLE
	OR wtGun = WEAPONTYPE_CARBINERIFLE
	OR wtGun = WEAPONTYPE_ADVANCEDRIFLE
	OR wtGun = WEAPONTYPE_PUMPSHOTGUN
	OR wtGun = WEAPONTYPE_ASSAULTSHOTGUN
	OR wtGun = WEAPONTYPE_GRENADELAUNCHER
	OR wtGun = WEAPONTYPE_DLC_ASSAULTSMG
	OR wtGun = WEAPONTYPE_DLC_BULLPUPSHOTGUN
	OR wtGun = WEAPONTYPE_DLC_SPECIALCARBINE
	OR wtGun = WEAPONTYPE_DLC_BULLPUPRIFLE
	OR wtGun = WEAPONTYPE_DLC_HEAVYSHOTGUN
	OR wtGun = WEAPONTYPE_DLC_MARKSMANRIFLE
	OR wtGun = WEAPONTYPE_DLC_COMBATPDW
	
		IF HAS_PED_GOT_WEAPON_COMPONENT(piPassed, wtGun, WEAPONCOMPONENT_AT_AR_FLSH)
			EXIT
		ENDIF
		
		GIVE_WEAPON_COMPONENT_TO_PED(piPassed, wtGun, WEAPONCOMPONENT_AT_AR_FLSH)
		PRINTLN("MC_GIVE_PED_A_FLASHLIGHT_ATTACHMENT - Giving ped ", iPedNumber, " a WEAPONCOMPONENT_AT_AR_FLSH")
	ELSE
		PRINTLN("MC_GIVE_PED_A_FLASHLIGHT_ATTACHMENT - INVALID WEAPON Could not give ped Flashlight attachment: ", iPedNumber)
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Peds
// ##### Description: Process the creation of Mission Controller Peds.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_SCALE_PED_HEALTH_AMOUNT(FLOAT fScaleAmount, INT iStartHealth)

	INT iHealthMod = 0
	
	IF fScaleAmount != 0
		iStartHealth -= 100
		FLOAT fNewHealth = TO_FLOAT(iStartHealth)
		FLOAT fPercent = fScaleAmount
		
		fPercent *= 0.01
		fNewHealth *= fPercent
		
		iHealthMod = ROUND(fNewHealth)
		
		PRINTLN("[RCC MISSION][AltVars] - MC_SET_UP_PED - iStartHealth: ", iStartHealth+100, " iHealthMod: ", iHealthMod, " Using Percent: ", fScaleAmount)		
		
	ENDIF
	
	RETURN iHealthMod
ENDFUNC

PROC MC_SET_UP_PED_VISUAL_PROPERTIES(PED_INDEX piPassed, INT iPedNumber, BOOL bDefault = FALSE)
	IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedPerceptionSettings != ciPED_PERCEPTION_DEFAULT)
		
		FLOAT fCentreRange = GET_FMMC_PED_PERCEPTION_RANGE(iPedNumber, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedPerceptionSettings, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].fPedPerceptionCentreRange, iPedSightModifierTOD, iPedSightModifierWeather)
		FLOAT fCentreAngle = GET_FMMC_PED_PERCEPTION_FOV(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedPerceptionSettings, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].fPedPerceptionCentreAngle)
		FLOAT fPeripheralAngle = GET_FMMC_PED_PERIPHERAL_EXTREME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedPerceptionSettings)

		PRINTLN("[RCC MISSION] MC_SET_UP_PED - setting ped ", iPedNumber, " perception profile = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedPerceptionSettings)
		PRINTLN("[RCC MISSION] MC_SET_UP_PED - fCentreRange: ", fCentreRange)
		PRINTLN("[RCC MISSION] MC_SET_UP_PED - fCentreAngle: ", fCentreAngle)
		PRINTLN("[RCC MISSION] MC_SET_UP_PED - fPeripheralAngle: ", fPeripheralAngle)
		
		SET_PED_VISUAL_FIELD_PROPERTIES(piPassed, fCentreRange, DEFAULT, fCentreAngle, -fPeripheralAngle, fPeripheralAngle)
		
		IF fCentreRange >= 90
			SET_PED_COMBAT_RANGE(piPassed, CR_FAR)
		ENDIF
	ELIF bDefault
		SET_PED_VISUAL_FIELD_PROPERTIES(piPassed)
	ENDIF
ENDPROC

FUNC BOOL IS_STEALTH_AND_AGGRO_SYSTEMS_VALID_ON_THIS_RULE(INT iPed)	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sStealthAndAggroSystem.iBS, ciPED_SAS_EnableStealthAggroSettings)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sStealthAndAggroSystem.iBS, ciPED_SAS_EnableStealthAggroSettingsOnlyInDisguise)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sStealthAndAggroSystem.iBS, ciPED_SAS_EnableStealthAggroSettingsOnlyInDisguiseVehicle)
		RETURN FALSE
	ENDIF
	
	INT iTeam = MC_PlayerBD[iLocalPart].iTeam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		IF (iRule < g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sStealthAndAggroSystem.iRuleFrom AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sStealthAndAggroSystem.iRuleFrom != -1)
		OR (iRule >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sStealthAndAggroSystem.iRuleTo AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sStealthAndAggroSystem.iRuleTo != -1)
			RETURN FALSE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_PED_STATS_FROM_CONTAINER_VEHICLE(PED_INDEX piPassed, INT iVeh, BOOL bSet)

	SET_ENTITY_INVINCIBLE(piPassed, bSet)
	SET_ENTITY_DYNAMIC(piPassed, NOT bSet)
	SET_PED_CONFIG_FLAG(piPassed, PCF_DontActivateRagdollFromVehicleImpact, bSet)
	SET_PED_CONFIG_FLAG(piPassed, PCF_DontActivateRagdollFromBulletImpact, bSet)
	SET_PED_CONFIG_FLAG(piPassed, PCF_DontActivateRagdollFromExplosions, bSet)
	SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(piPassed, KNOCKOFFVEHICLE_NEVER)
	
	VEHICLE_INDEX viContainerVehicle = GET_FMMC_ENTITY_VEHICLE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleInVehicle_ContainerVehicleIndex)
	
	IF DOES_ENTITY_EXIST(viContainerVehicle)
		SET_ENTITY_NO_COLLISION_ENTITY(piPassed, viContainerVehicle, NOT bSet)
	ENDIF
	
	PRINTLN("[Peds] - SET_PED_STATS_FROM_CONTAINER_VEHICLE - bSet: ", bSet)

ENDPROC

PROC PROCESS_PED_STARTING_FROM_CONTAINER(FMMC_PED_STATE &sPedState)
		
	IF NOT IS_BIT_SET(sMissionPedsLocalVars[sPedState.iIndex].iPedBS, ci_MissionPedBS_StartedInContainerVehicleWithInvincibility)
		EXIT
	ENDIF
	
	IF sPedState.bIsInAnyVehicle
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iVehicle = -1
		EXIT
	ENDIF
		
	IF sPedState.bHasControl
		SET_PED_STATS_FROM_CONTAINER_VEHICLE(sPedState.pedIndex, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].iVehicle, FALSE)		
	ENDIF
	
	CLEAR_BIT(sMissionPedsLocalVars[sPedState.iIndex].iPedBS, ci_MissionPedBS_StartedInContainerVehicleWithInvincibility)
	
	PRINTLN("[Peds][Ped ", sPedState.iIndex, "] - PROCESS_PED_STARTING_FROM_CONTAINER - Setting ci_MissionPedBS_StartedInContainerVehicleWithInvincibility")
	
ENDPROC

PROC MC_SET_UP_PED(PED_INDEX piPassed, INT iPedNumber, BOOL bVisible = TRUE, VEHICLE_INDEX tempVeh = NULL)
	
	INT iteamrel[FMMC_MAX_TEAMS]
	INT irepeat
	INT iAccuracy
	INT icop
	COMBAT_ABILITY_LEVEL eCombatAbility
	FLOAT fstartingplayers 
	fstartingplayers = TO_FLOAT(GET_TOTAL_STARTING_PLAYERS())
	FLOAT fNumParticipants
	fNumParticipants = TO_FLOAT(g_FMMC_STRUCT.iNumParticipants)
	FLOAT fDifficultyMod, fhealth, ftunablehealth
	fDifficultyMod = (fstartingplayers / fNumParticipants)
	CONST_FLOAT fLesMultiplier 0.8
	VECTOR vPos = GET_ENTITY_COORDS(piPassed)
	
	PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - g_FMMC_STRUCT.iNumParticipants = ", g_FMMC_STRUCT.iNumParticipants) 
	PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - GET_TOTAL_STARTING_PLAYERS() = ", GET_TOTAL_STARTING_PLAYERS()) 
	PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - fDifficultyMod = ", fDifficultyMod) 
	PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Corona difficulty = ", g_FMMC_STRUCT.iDifficulity)
	PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Model Name: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn))
	PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - iModelVariation: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iModelVariation)
	
	IF g_FMMC_STRUCT.iDifficulity = DIFF_EASY
		IF NOT IS_PED_IN_ANY_PLANE(piPassed)
			SET_PED_SHOOT_RATE(piPassed, 60)
		ENDIF
		fDifficultyMod = fDifficultyMod - 1.0
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - fDifficultyMod after easy mod = ") NET_PRINT_FLOAT(fDifficultyMod) NET_NL()
	ELIF g_FMMC_STRUCT.iDifficulity = DIFF_HARD
		fDifficultyMod = fDifficultyMod + 1.0
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - fDifficultyMod after hard mod = ") NET_PRINT_FLOAT(fDifficultyMod) NET_NL()
	ENDIF
	
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_DoNotTriggerBarrier)
		SET_PED_CONFIG_FLAG(piPassed, PCF_IgnoredByAutoOpenDoors, TRUE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - ciPED_BSFive_DoNotTriggerBarrier bit is set. Calling PCF_IgnoredByAutoOpenDoors.")
	ENDIF
	
	//Check if we are using custom ped variation
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].sPedVariationSettings.iPedVariationBS, ciPV_USING_CUSTOM_VARIATION)
		FMMC_SET_PED_CUSTOM_VARIATION(piPassed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].sPedVariationSettings)
	ELSE
		FMMC_SET_PED_VARIATION(piPassed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iModelVariation)
	ENDIF
	
	//If this is the juggernaut
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("U_M_Y_Juggernaut_01"))
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetSixteen, ciPED_BSSixteen_UseJuggernautMoveset)	
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Juggernaut anim, audio and component variations applied")		
		
		// Movement changes for the bulky armour
		SET_PED_MOVEMENT_CLIPSET(piPassed, "ANIM_GROUP_MOVE_BALLISTIC")
		SET_PED_STRAFE_CLIPSET(piPassed, "MOVE_STRAFE_BALLISTIC")
		SET_WEAPON_ANIMATION_OVERRIDE(piPassed, HASH("BALLISTIC"))		
		
		// Audio and footsteps.
		REQUEST_SCRIPT_AUDIO_BANK("DLC_MPSUM2/DLC_MPSum2_Juggernaut")
		USE_FOOTSTEP_SCRIPT_SWEETENERS(piPassed, TRUE, HASH("DLC_MPSUM2_Juggernaut_NPC_Footstep_Sounds"))
		
		// Prevents weapon swap and injured anims sets at low health.
		SET_PED_CONFIG_FLAG(piPassed, PCF_DisableHurt, TRUE)
		
		// Prevents dialogue or screaming in pain.
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("U_M_Y_Juggernaut_01"))
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Juggernaut Preventing them from speaking.")	
			DISABLE_PED_PAIN_AUDIO(piPassed, TRUE)
			STOP_PED_SPEAKING(piPassed, TRUE)
			BLOCK_ALL_SPEECH_FROM_PED(piPassed, TRUE, TRUE)
		ENDIF
	ENDIF
		
	IF MC_IS_PED_A_SCRIPTED_COP(iPedNumber)

		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iFlee != ciPED_FLEE_ON
			IF g_FMMC_STRUCT.iPolice != 1 // not wanted off
				IF IS_OBJECTIVE_PED(iPedNumber)
					PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting up as objective cop, ped: ", iPedNumber) 
					SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetTwo, ciPED_BSTwo_IS_OBJECTIVE_PED)
				ELSE
					PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting up as non-objective cop, ped: ", iPedNumber) 
					CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetTwo, ciPED_BSTwo_IS_OBJECTIVE_PED)
				ENDIF
			ENDIF
			
			SET_PED_CONFIG_FLAG(piPassed, PCF_DontBlip, FALSE)
			SET_PED_CONFIG_FLAG(piPassed, PCF_DontBlipCop, FALSE)
			SET_PED_AS_COP(piPassed, FALSE)
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetTwelve, ciPed_BSTwelve_NeverLoseTarget)
				SET_PED_TARGET_LOSS_RESPONSE(piPassed, TLR_SEARCH_FOR_TARGET)
			ENDIF
			//SET_PED_CONFIG_FLAG(piPassed,PCF_DontBehaveLikeLaw, TRUE)
			SET_PED_CONFIG_FLAG(piPassed,PCF_CanAttackNonWantedPlayerAsLaw, TRUE)
			SET_PED_CONFIG_FLAG(piPassed,PCF_OnlyUpdateTargetWantedIfSeen, TRUE)
			SET_PED_CONFIG_FLAG(piPassed,PCF_AllowContinuousThreatResponseWantedLevelUpdates, TRUE)
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting PCF_CanAttackNonWantedPlayerAsLaw on, ped: ", iPedNumber)
		ENDIF
		icop = FM_RelationshipLikeCops
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting up as cop, ped: ", iPedNumber)
	ELSE
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - not a scripted cop, setting PCF_DontBlipCop ped : ", iPedNumber)
		SET_PED_CONFIG_FLAG(piPassed, PCF_DontBlipCop, TRUE)
		SET_PED_CONFIG_FLAG(piPassed, PCF_CanAttackNonWantedPlayerAsLaw, TRUE)
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThree, ciPED_BSThree_OverrideNotACop)		
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - ciPED_BSThree_OverrideNotACop is set for ped : ", iPedNumber, " Forcing off Cop and Law behaviours")
			SET_PED_CONFIG_FLAG(piPassed, PCF_DontInfluenceWantedLevel, TRUE) // Blocks the Death Event from triggering a SetWantedLevel in Code.
			SET_PED_CONFIG_FLAG(piPassed, PCF_DontBehaveLikeLaw, TRUE)		  // Blocks Code from calling SetWantedLevel based purely on sight of the player comitting a crime.			
		ENDIF
		
		icop = FM_RelationshipNothingCops
	ENDIF
			
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = mp_g_m_pros_01
		SET_PED_COMPONENT_VARIATION(piPassed,PED_COMP_SPECIAL2,0,0)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - removing prof mask")
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = IG_LESTERCREST
		SET_PED_PROP_INDEX(piPassed, ANCHOR_EYES, 0,0)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Ped is lester, giving glasses")
		SET_PED_MOVEMENT_CLIPSET(piPassed, "move_heist_lester")
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = PLAYER_TWO
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Ped is Trevor, giving outfit")
		SET_PED_COMPONENT_VARIATION(piPassed,PED_COMP_TORSO,3, 3)
		SET_PED_COMPONENT_VARIATION(piPassed,PED_COMP_LEG,18, 9)
		SET_PED_COMPONENT_VARIATION(piPassed,PED_COMP_FEET,1, 0)
		SET_PED_PROP_INDEX(piPassed, ANCHOR_EYES, 8,0)
		SET_COMBAT_FLOAT(piPassed, CCF_TIME_TO_INVALIDATE_INJURED_TARGET, 1.0)	//B*2219732
		SETUP_PLAYER_PED_DEFAULTS_FOR_MP(piPassed)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iIdleAnim = ciPED_IDLE_ANIM__TREVOR_NARCFIN_BODHI_LOOP
			//set a nice dirty flag here
		ENDIF
	ENDIF
	PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - ped: ", iPedNumber) 
	PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - associated team: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAssociatedTeam) 
	PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - associated rule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAssociatedObjective)
	PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - associated spawn: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAssociatedSpawn) 
	PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Associated Action: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAssociatedAction) 
	
	
	IF MC_IS_PED_AN_ANIMAL(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn)	
		REMOVE_ALL_PED_WEAPONS(piPassed)
		GIVE_WEAPON_TO_PED(piPassed, WEAPONTYPE_ANIMAL, INFINITE_AMMO, TRUE, TRUE)
		SET_PED_DROPS_WEAPONS_WHEN_DEAD(piPassed, FALSE)
		
		SET_PED_FLEE_ATTRIBUTES(piPassed, FA_DISABLE_FLEE_FROM_INDIRECT_THREATS, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_FLEE_FROM_COMBAT, TRUE)
		SET_PED_FLEE_ATTRIBUTES(piPassed, FA_DISABLE_COWER, TRUE)		
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, TRUE)	
		
	ELSE
		
		#IF IS_DEBUG_BUILD
			INT iweapon = enum_to_int(GET_PED_WEAPON_TYPE_FROM_CREATOR_OPTION(iPedNumber))
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - has weapon: ", iweapon)
		#ENDIF
		IF GET_PED_WEAPON_TYPE_FROM_CREATOR_OPTION(iPedNumber) != WEAPONTYPE_INVALID
			IF GET_PED_WEAPON_TYPE_FROM_CREATOR_OPTION(iPedNumber) = WEAPONTYPE_MINIGUN
				SET_PED_FIRING_PATTERN(piPassed, fIRING_PATTERN_FULL_AUTO)
				PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting ped with minigun to fire in FIRING_PATTERN_FULL_AUTO for ped")
			ELIF GET_PED_WEAPON_TYPE_FROM_CREATOR_OPTION(iPedNumber) = WEAPONTYPE_RPG
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitset, ciPED_BS_RocketPedsAndVehicles)
					GIVE_DELAYED_WEAPON_TO_PED(piPassed, WEAPONTYPE_PISTOL, 25000, FALSE)
				ENDIF
			ENDIF
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iWepHolstered = ciPED_WEAPON_HOLSTERED_ON
				PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - giving ped holstered gun")
				GIVE_DELAYED_WEAPON_TO_PED(piPassed, GET_PED_WEAPON_TYPE_FROM_CREATOR_OPTION(iPedNumber), 25000, FALSE)
			ELSE
				GIVE_DELAYED_WEAPON_TO_PED(piPassed, GET_PED_WEAPON_TYPE_FROM_CREATOR_OPTION(iPedNumber), 25000, TRUE)
				SET_CURRENT_PED_WEAPON(piPassed, GET_PED_WEAPON_TYPE_FROM_CREATOR_OPTION(iPedNumber), TRUE)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetSix, ciPED_BSSix_FlashlightsNGOnly)
				PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - giving ped Flashlight attachment")
				MC_GIVE_PED_A_FLASHLIGHT_ATTACHMENT(piPassed, iPedNumber)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitset, ciPED_BS_RocketPedsAndVehicles)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_ROCKETS_AGAINST_VEHICLES_ONLY, TRUE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - with RPG to only fire it at vehicles")
	ENDIF
	
	//See if this ped should turn invisible immediately
	IF MC_SHOULD_PED_SPAWN_INVISIBLE(iPedNumber, piPassed)
		SET_ENTITY_ALPHA(piPassed, 0, FALSE)
		SET_PED_CAN_BE_TARGETTED(piPassed, FALSE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - invisible")
	ENDIF
	
	FLOAT fBreakLockAngle
	FLOAT fBreakLockAngleClose
	FLOAT fBreakLockCloseDistance
	FLOAT fTurnRateModifier
	
	IF IS_PED_IN_ANY_VEHICLE(piPassed)
		MODEL_NAMES mnVeh = DUMMY_MODEL_FOR_SCRIPT
		
		IF NOT DOES_ENTITY_EXIST(tempVeh)
			tempVeh = GET_VEHICLE_PED_IS_IN(piPassed)
		ENDIF
		
		IF DOES_ENTITY_EXIST(tempVeh)
			mnVeh = GET_ENTITY_MODEL(tempVeh)
		ENDIF
		
		//-- Homing missile accuracy
		IF IS_THIS_MODEL_A_HELI(mnVeh)
		OR IS_THIS_MODEL_A_PLANE(mnVeh)
		
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - ped homing missile - Getting tunable data - vehicle model = ", ENUM_TO_INT(mnVeh))
			Fill_MP_Float_Tunable(TUNE_CONTEXT_MP_GLOBALS, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE, HASH("CCF_HOMING_ROCKET_TURN_RATE_MODIFIER"), g_sMPTunables.fCCF_HOMING_ROCKET_TURN_RATE_MODIFIER)
			Fill_MP_Float_Tunable(TUNE_CONTEXT_MP_GLOBALS, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE, HASH("HOMING_ROCKET_BREAK_LOCK_ANGLE"), g_sMPTunables.fHOMING_ROCKET_BREAK_LOCK_ANGLE)
			Fill_MP_Float_Tunable(TUNE_CONTEXT_MP_GLOBALS, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE, HASH("HOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE"), g_sMPTunables.fHOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE)
			Fill_MP_Float_Tunable(TUNE_CONTEXT_MP_GLOBALS, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE, HASH("HOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE"), g_sMPTunables.fHOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE)

			fBreakLockAngle				= g_sMPTunables.fHOMING_ROCKET_BREAK_LOCK_ANGLE
			fBreakLockAngleClose		= g_sMPTunables.fHOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE	
			fBreakLockCloseDistance		= g_sMPTunables.fHOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE
			fTurnRateModifier			= g_sMPTunables.fCCF_HOMING_ROCKET_TURN_RATE_MODIFIER
		
			SET_COMBAT_FLOAT(piPassed, CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE, fBreakLockAngle)
			SET_COMBAT_FLOAT(piPassed, CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE, fBreakLockAngleClose)
			SET_COMBAT_FLOAT(piPassed, CCF_HOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE, fBreakLockCloseDistance)
			SET_COMBAT_FLOAT(piPassed, CCF_HOMING_ROCKET_TURN_RATE_MODIFIER, fTurnRateModifier)
			
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - ped homing missile CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE = ", fBreakLockAngle, " CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE = ", fBreakLockAngleClose,  " CCF_HOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE = ", fBreakLockAngleClose, " CCF_HOMING_ROCKET_TURN_RATE_MODIFIER = ", fTurnRateModifier) 
		ELSE
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - NOT setting homing missile tunables ")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThree, ciPED_BSThree_UseRocketsInJets)
			SET_CURRENT_PED_VEHICLE_WEAPON(piPassed, WEAPONTYPE_VEHICLE_SPACE_ROCKET)
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - ciPED_BSThree_UseRocketsInJets bit is set. Calling SET_CURRENT_PED_VEHICLE_WEAPON(piPassed, WEAPONTYPE_VEHICLE_SPACE_ROCKET).")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEight, ciPed_BSEight_DisableBuzzardRocketUse)
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - ciPed_BSEight_DisableBuzzardRocketUse set!")
			
			IF tempVeh != NULL
				MODEL_NAMES eVehModel = GET_ENTITY_MODEL(tempVeh)
				
				IF eVehModel = BUZZARD
				OR eVehModel = BUZZARD2
					PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - disabling use of buzzard rockets!")
					
					DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_VEHICLE_SPACE_ROCKET, tempVeh, piPassed)
					SET_CURRENT_PED_VEHICLE_WEAPON(piPassed, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
					SET_PED_CAN_SWITCH_WEAPON(piPassed, FALSE) 				
				ELIF eVehModel = SAVAGE
					PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - disabling use of savage cannons!")
				
					DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_PLAYER_SAVAGE, tempVeh, piPassed)
					SET_CURRENT_PED_VEHICLE_WEAPON(piPassed, WEAPONTYPE_VEHICLE_SPACE_ROCKET)
					SET_PED_CAN_SWITCH_WEAPON(piPassed, FALSE) 
				ELIF eVehModel = TAMPA3
					PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - forcing use of tampa mortar!")
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - ped veh is not a buzzard or a savage!")
				#ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - ped veh is null!")
			#ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitset, ciPED_BS_RocketPedsAndVehicles)	
			IF mnVeh != SAVAGE
				SET_CURRENT_PED_VEHICLE_WEAPON(piPassed, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
				PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - ped only use mini guns in a heli")
			ELSE
				SET_CURRENT_PED_VEHICLE_WEAPON(piPassed, WEAPONTYPE_DLC_VEHICLE_PLAYER_SAVAGE)
				PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - ped only use mini guns in a savage heli")
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iVehicle != -1
		//-- For vehicles created in the air
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iVehicle].iVehBitsetThree, ciFMMC_VEHICLE3_VehicleStartsAirborne)	
		AND IS_PED_IN_ANY_VEHICLE(piPassed)
			IF NOT DOES_ENTITY_EXIST(tempVeh)
				tempVeh = GET_VEHICLE_PED_IS_IN(piPassed)
			ENDIF
			
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - ciFMMC_VEHICLE3_VehicleStartsAirborne is set for veh ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iVehicle)
			
			SET_VEHICLE_ENGINE_ON(tempVeh, TRUE, TRUE)
			
			SET_ENTITY_DYNAMIC(tempVeh, TRUE)
			ACTIVATE_PHYSICS(tempVeh)
			
			IF IS_THIS_MODEL_A_HELI(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iVehicle].mn)
				SET_HELI_BLADES_FULL_SPEED(tempVeh)
			ELIF IS_THIS_MODEL_A_PLANE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iVehicle].mn)
				SET_HELI_BLADES_FULL_SPEED(tempVeh)
				SET_VEHICLE_FORWARD_SPEED(tempVeh, 30.0)     
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iVehicle].iVehicleInVehicle_ContainerVehicleIndex > -1
			SET_PED_STATS_FROM_CONTAINER_VEHICLE(piPassed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iVehicle, TRUE)			
			SET_BIT(sMissionPedsLocalVars[iPedNumber].iPedBS, ci_MissionPedBS_StartedInContainerVehicleWithInvincibility)
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_PedStartsInContainer, DEFAULT, iPedNumber, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ciVEHICLE_IN_VEHICLE_STATE__PREPARING)			
			PRINTLN("[Peds][Ped ", iPedNumber, "][VehicleInVehicle] - MC_SET_UP_PED - Setting up ped to be placed in MOC-contained vehicle Veh ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iVehicle)
		ENDIF
	ENDIF
	
	FOR irepeat = 0 TO (FMMC_MAX_TEAMS-1)
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iRule[irepeat] = FMMC_OBJECTIVE_LOGIC_CROWD_CONTROL
			SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitset, ciPED_BS_InvolvedInCrowdControl)
		ENDIF
		
		INT iRelTeamGlobal = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iTeam[irepeat]		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iNewRelationshipGroupOnRule != -1
		AND (GET_MC_TEAM_STARTING_RULE(iRepeat) = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iNewRelationshipGroupOnRule 
		OR GET_MC_TEAM_STARTING_RULE(iRepeat) >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iNewRelationshipGroupOnRule
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetTwentyOne, ciPED_BSTwentyOne_NewRuleRelGroupAfterAndIncluding))
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iNewRelationshipGroupType != -1
				iRelTeamGlobal = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iNewRelationshipGroupType
			ENDIF
		ENDIF
		
		IF iRelTeamGlobal = ciPED_RELATION_SHIP_LIKE
			iteamrel[irepeat] = FM_RelationshipLike 
			SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, FALSE)
			SET_ENTITY_IS_TARGET_PRIORITY(piPassed, FALSE)
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - ped likes team: ", irepeat)
		ELIF iRelTeamGlobal = ciPED_RELATION_SHIP_DISLIKE
			iteamrel[irepeat] = FM_RelationshipDislike
			
			//Only auto-target if they're going to respond with hostility
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iFlee != ciPED_FLEE_ON
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iCombatStyle = ciPED_AGRESSIVE
				OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iCombatStyle = ciPED_BERSERK
				OR (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iCombatStyle = ciPED_DEFENSIVE AND GET_PED_WEAPON_TYPE_FROM_CREATOR_OPTION(iPedNumber) != WEAPONTYPE_UNARMED AND GET_PED_WEAPON_TYPE_FROM_CREATOR_OPTION(iPedNumber) != WEAPONTYPE_INVALID)
					SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, TRUE)
				ELSE
					SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, FALSE)
				ENDIF
			ELSE
				IF (GET_PED_WEAPON_TYPE_FROM_CREATOR_OPTION(iPedNumber) != WEAPONTYPE_UNARMED AND GET_PED_WEAPON_TYPE_FROM_CREATOR_OPTION(iPedNumber) != WEAPONTYPE_INVALID)
					SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, TRUE)
				ELSE
					SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, FALSE)
				ENDIF
			ENDIF
			
			SET_ENTITY_IS_TARGET_PRIORITY(piPassed, FALSE)
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - ped dislikes team: ", irepeat)
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iTeam[irepeat] = ciPED_RELATION_SHIP_HATE
			iteamrel[irepeat] = FM_RelationshipHate
			SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, TRUE)
			SET_ENTITY_IS_TARGET_PRIORITY(piPassed, TRUE)
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - ped hates team: ", irepeat)
		ENDIF
	ENDFOR
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetSeventeen, ciPed_BSSeventeen_ForceFriendshipWithCops)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - forcing ped friendship with cops FM_RelationshipLikeCops.")
		icop = FM_RelationshipLikeCops
	ENDIF
	
	SET_PED_RELATIONSHIP_GROUP_HASH(piPassed, rgFM_AiPed[iteamrel[0]][iteamrel[1]][iteamrel[2]][iteamrel[3]][icop])	//Use FM_RelationshipNothingCops or FM_RelationshipLikeCops
	
	#IF IS_DEBUG_BUILD
		MC_GIVE_PED_DEBUG_NAME(piPassed, iPedNumber)
	#ENDIF
		
	IF(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedCashAmount != -1)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting ped cash amount ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedCashAmount)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting ped cash amount ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedCashAmount)
		SET_PED_MONEY(piPassed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedCashAmount)
	ENDIF
	
	MC_SET_UP_FMMC_PED_DEFENSIVE_AREA(piPassed, iPedNumber, vPos)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iFlee = ciPED_FLEE_ON
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_ALWAYS_FLEE, TRUE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting ped to flee")
	ELSE
		MC_APPLY_PED_COMBAT_STYLE(piPassed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iCombatStyle, GET_PED_WEAPON_TYPE_FROM_CREATOR_OPTION(iPedNumber), iPedNumber)
		
		SET_PED_FLEE_ATTRIBUTES(piPassed, FA_DISABLE_FLEE_FROM_INDIRECT_THREATS, TRUE)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = IG_LESTERCREST
			SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_FLEE_FROM_COMBAT, FALSE)
			SET_PED_FLEE_ATTRIBUTES(piPassed, FA_DISABLE_COWER, TRUE)
		ELSE	
			SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_FLEE_FROM_COMBAT, TRUE)
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThirteen, ciPed_BSThirteen_DisableCowering)
				SET_PED_FLEE_ATTRIBUTES(piPassed, FA_DISABLE_COWER, FALSE)
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThirteen, ciPed_BSThirteen_DisableCowering)
			SET_PED_FLEE_ATTRIBUTES(piPassed, FA_COWER_INSTEAD_OF_FLEE, TRUE)
		ENDIF
	ENDIF
		
	IF IS_PED_IN_ANY_HELI(piPassed)
	OR IS_PED_IN_ANY_PLANE(piPassed)
	OR IS_PED_IN_ANY_BOAT(piPassed)
	OR IS_PED_IN_ANY_SUB(piPassed)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_CanUseVehiclesForTasks)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting CA_LEAVE_VEHICLES = false & CA_USE_VEHICLE = TRUE")
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_VEHICLE, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_LEAVE_VEHICLES, FALSE)		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEight, ciPed_BSEight_NeverLeaveVehicle)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEighteen, ciPed_BSEighteen_DontLeaveVehicleUnlessExplicitCommand)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting CA_LEAVE_VEHICLES = false")
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_LEAVE_VEHICLES, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_DontFleeInvehicle)
		SET_PED_FLEE_ATTRIBUTES(piPassed, FA_USE_VEHICLE, FALSE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting FA_USE_VEHICLE = false via ciPED_BSFive_DontFleeInvehicle")
	ELSE
		SET_PED_FLEE_ATTRIBUTES(piPassed, FA_USE_VEHICLE, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThirteen, ciPed_BSThirteen_DisableCowering)
		SET_PED_FLEE_ATTRIBUTES(piPassed, FA_DISABLE_COWER, TRUE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Setting FA_DISABLE_COWER")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_DontAllowToBeDraggedOutOfVehicle)
		SET_PED_CONFIG_FLAG(piPassed, PCF_DontAllowToBeDraggedOutOfVehicle, TRUE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Setting ped config flag PCF_DontAllowToBeDraggedOutOfVehicle")
	ELSE
		SET_PED_CONFIG_FLAG(piPassed, PCF_DontAllowToBeDraggedOutOfVehicle, FALSE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Setting ped config flag PCF_DontAllowToBeDraggedOutOfVehicle")
	ENDIF
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piPassed, FALSE)
	
	//Check to see if the mission creator has selected this ped to die in water or not:
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitset, ciPed_BS_DoesntDieInWater)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThree, ciPED_BSThree_OnlyDamagedByPlayers)
	
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Does not die instantly in Water.")
		
		SET_PED_DIES_IN_WATER(piPassed, FALSE)
		SET_PED_DIES_INSTANTLY_IN_WATER(piPassed, FALSE)
	
	ELSE
	
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Dies instantly in Water.")
		
		SET_PED_DIES_IN_WATER(piPassed, TRUE)
		SET_PED_DIES_INSTANTLY_IN_WATER(piPassed, TRUE) 
	
	ENDIF
	
	
	SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(piPassed)
	
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_TAUNT_IN_VEHICLE, FALSE)
	
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_VEHICLE, TRUE)
	
	IF(NOT IS_PED_IN_ANY_BOAT(piPassed))
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetTwelve, ciPed_BSTwelve_PreventVehicleWeaponUsage)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_VEHICLE_ATTACK, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetSix, ciPED_BSSix_AllowDogFighting)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_ALLOW_DOG_FIGHTING, TRUE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Setting ped config flag CA_ALLOW_DOG_FIGHTING")
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_DontDoDriveBys)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DO_DRIVEBYS, TRUE)
	ELSE
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DO_DRIVEBYS, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetSix, ciPED_BSSix_PreferAirCombat)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_PREFER_AIR_COMBAT_WHEN_IN_AIRCRAFT, TRUE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting CA_PREFER_AIR_COMBAT_WHEN_IN_AIRCRAFT")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetSix, ciPED_BSSix_PreferNonAircraftTargets)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_PREFER_AIR_COMBAT_WHEN_IN_AIRCRAFT, TRUE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting ciPED_BSSix_PreferNonAircraftTargets")
	ENDIF
	
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_BlockAggroDueToAmbientPeds)
		SET_PED_CONFIG_FLAG(piPassed,PCF_DontRespondToRandomPedsDamage, TRUE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting PCF_DontRespondToRandomPedsDamage TRUE")
	ENDIF
	
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_ALL_RANDOMS_FLEE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_PREFER_KNOWN_TARGETS_WHEN_COMBAT_CLOSEST_TARGET, TRUE)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThree, ciPED_BSThree_AggroOnSeeingDeadBody)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_WILL_GENERATE_DEAD_PED_SEEN_SCRIPT_EVENTS, TRUE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting ciPED_BSThree_AggroOnSeeingDeadBody")
	ENDIF
	
	SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(piPassed,KNOCKOFFVEHICLE_HARD)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iIdleAnim = ciPED_IDLE_ANIM__PHONE
	OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iSpecialAnim = ciPED_IDLE_ANIM__PHONE
		SET_PED_CONFIG_FLAG(piPassed,PCF_PhoneDisableTextingAnimations, FALSE)
		SET_PED_CONFIG_FLAG(piPassed,PCF_PhoneDisableTalkingAnimations, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEight, ciPed_BSEight_NeverLeaveVehicle)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEighteen, ciPed_BSEighteen_DontLeaveVehicleUnlessExplicitCommand)
		SET_PED_CONFIG_FLAG(piPassed, PCF_GetOutUndriveableVehicle, FALSE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting PED flag PCF_GetOutUndriveableVehicle to FALSE")
	ELSE
		SET_PED_CONFIG_FLAG(piPassed, PCF_GetOutUndriveableVehicle, TRUE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting PED flag PCF_GetOutUndriveableVehicle to TRUE")
	ENDIF
	
	SET_PED_CONFIG_FLAG(piPassed,PCF_OnlyWritheFromWeaponDamage, TRUE)
	SET_PED_CONFIG_FLAG(piPassed,PCF_DisableGoToWritheWhenInjured, TRUE)
	
	SET_PED_CONFIG_FLAG(piPassed,PCF_ListensToSoundEvents, TRUE)
	SET_PED_CONFIG_FLAG(piPassed,PCF_CheckLoSForSoundEvents, TRUE)
	
	SET_PED_CONFIG_FLAG(piPassed, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
	SET_PED_CONFIG_FLAG(piPassed, PCF_CanBeAgitated, FALSE)
	SET_PED_CONFIG_FLAG(piPassed,PCF_KeepTargetLossResponseOnCleanup, TRUE)

	SET_PED_CONFIG_FLAG(piPassed,PCF_AICanDrivePlayerAsRearPassenger, TRUE)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn <> IG_LESTERCREST
		SET_PED_CONFIG_FLAG(piPassed,PCF_PreventUsingLowerPrioritySeats, FALSE)
	ELSE
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting PCF_PreventUsingLowerPrioritySeats as it's Lester")
		SET_PED_CONFIG_FLAG(piPassed,PCF_PreventUsingLowerPrioritySeats, TRUE)
	ENDIF
	
	SET_PED_CONFIG_FLAG(piPassed,PCF_DisableEventInteriorStatusCheck, TRUE)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetSeven, ciPED_BSSeven_DisableDislikeAsHateWhenInCombat)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting PCF_TreatDislikeAsHateWhenInCombat")
		SET_PED_CONFIG_FLAG(piPassed,PCF_TreatDislikeAsHateWhenInCombat, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetSix, ciPED_BSSix_HateAllOtherPedGroups)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting ciPED_BSSix_HateAllOtherPedGroups")
		SET_PED_CONFIG_FLAG(piPassed, PCF_TreatNonFriendlyAsHateWhenInCombat, TRUE)
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_IgnoreExplosions)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting ciPED_BSFive_IgnoreExplosions")
		SET_PED_CONFIG_FLAG(piPassed, PCF_DisableExplosionReactions, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_RunFromFiresAndExplosions)
		SET_PED_CONFIG_FLAG(piPassed, PCF_RunFromFiresAndExplosions, TRUE)
		SET_PED_CONFIG_FLAG(piPassed, PCF_GetOutBurningVehicle, TRUE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting PCF_RunFromFiresAndExplosions TRUE")
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetNine, ciPed_BSNine_DisableRunFromFiresAndExplosions)
		SET_PED_CONFIG_FLAG(piPassed, PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(piPassed, PCF_GetOutBurningVehicle, FALSE)
		SET_PED_CONFIG_FLAG(piPassed, PCF_DisableExplosionReactions, TRUE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting PCF_RunFromFiresAndExplosions FALSE")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEleven, ciPed_BSEleven_DontShuffleIntoTurret)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting PCF_PreventAutoShuffleToTurretSeat TRUE")
		SET_PED_CONFIG_FLAG(piPassed, PCF_PreventAutoShuffleToTurretSeat, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEleven, ciPed_BSEleven_DontCommandeerVehicles)
		SET_PED_CONFIG_FLAG(piPassed, PCF_NotAllowedToJackAnyPlayers, TRUE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Setting PCF_NotAllowedToJackAnyPlayers")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEleven, ciPed_BSEleven_DontSpeak)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting STOP_PED_SPEAKING_SYNCED TRUE")
		STOP_PED_SPEAKING_SYNCED(piPassed, TRUE)
	ENDIF
	
	INT ihealth = GET_PED_HEALTH_FROM_CREATOR_OPTION(iPedNumber)
				
	PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Creator Option - ihealth: ", ihealth)
		
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedPreciseHealth > 100
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Using the PRECISE health value of: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedPreciseHealth)
		ihealth = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedPreciseHealth
	ENDIF
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - starting ihealth = ", ihealth)
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_UseHeistEnemyAccuracyAndHealthSettings)
			
			FLOAT fPlayerAddMulti = 0.0, fDifficultyAddMulti = 0.0
			
			SWITCH GET_TOTAL_STARTING_PLAYERS()
				CASE 4
				CASE 3
					fPlayerAddMulti = 0.5
				BREAK
				CASE 2
					fPlayerAddMulti = 0.1
				BREAK
			ENDSWITCH
			
			IF g_FMMC_STRUCT.iDifficulity = DIFF_EASY // No easy mode in Gang Ops heist missions
				fDifficultyAddMulti = -0.25
			ELIF g_FMMC_STRUCT.iDifficulity = DIFF_HARD
				fDifficultyAddMulti = 0.25
			ELSE
				fDifficultyAddMulti = 0.0
			ENDIF
			
			fhealth = ihealth + ((fPlayerAddMulti + fDifficultyAddMulti) * (ihealth - 100))
			ihealth = ROUND(fhealth)
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Gang ops health: GET_TOTAL_STARTING_PLAYERS(): ", GET_TOTAL_STARTING_PLAYERS(), " means fPlayerAddMulti ", fPlayerAddMulti, " + fDifficultyAddMulti ", fDifficultyAddMulti, " -> modded ihealth ", ihealth)
			
		ELSE
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThirteen, ciPED_BSThirteen_IgnoreDifficultyHealthModifier)
				fhealth = ihealth + (0.5*fDifficultyMod*(ihealth-100))
				
				PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - modded fhealth = ", fhealth) 
				
				ihealth = ROUND(fhealth)
				
				#IF IS_DEBUG_BUILD 
					IF lw_fSetMissPedHealth != 1.0
				
						fhealth = ihealth * lw_fSetMissPedHealth
						ihealth = ROUND(fhealth)
						PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - debug health override starting ihealth = ", ihealth) 
						
					ENDIF
				#ENDIF
			ELSE
				PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - has ciPED_BSThirteen_IgnoreDifficultyHealthModifier SET. iHealth = ", iHealth) 
			ENDIF
		ENDIF
			
		IF ihealth < 101
			ihealth = 101
		ENDIF
		
	ENDIF
	
	PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - natural modded ihealth = ", ihealth) 
	
	ftunablehealth = ihealth*g_sMPTunables.fAiHealthModifier
	
	ihealth = ROUND(ftunablehealth)
	
	PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - tunable modded ihealth = ", ihealth)
	
	INT iHealthDiff1, iHealthDiff2
	
	iHealthDiff1 = GET_SCALE_PED_HEALTH_AMOUNT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].fHealthVarMultiplier, ihealth)
	iHealthDiff2 = GET_SCALE_PED_HEALTH_AMOUNT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].fHealthVarMultiplier2, ihealth)
	
	iHealth += iHealthDiff1 + iHealthDiff2
	
	PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - iHealth After Scaling: ", iHealth)
	
	SET_ENTITY_MAX_HEALTH(piPassed, ihealth)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_OneHitKill)
		ihealth = ciPED_OneHitKill_Health
			
		SET_ENTITY_MAX_HEALTH(piPassed, ihealth)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - one hit kill set | Setting ped health to ", ihealth)
	ENDIF
	
	SET_ENTITY_HEALTH(piPassed, ihealth)
	
	IF IS_JOB_FORCED_WEAPON_ONLY()
    OR IS_JOB_FORCED_WEAPON_PLUS_PICKUPS()
	OR GET_PED_WEAPON_TYPE_FROM_CREATOR_OPTION(iPedNumber) = WEAPONTYPE_STUNGUN
	OR GET_PED_WEAPON_TYPE_FROM_CREATOR_OPTION(iPedNumber) = WEAPONTYPE_DLC_RAILGUN
		SET_PED_DROPS_WEAPONS_WHEN_DEAD(piPassed, FALSE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting ped to not drop weapon when dead: ", iPedNumber) 
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThree, ciPED_BSThree_OnlyDamagedByPlayers)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(piPassed, TRUE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting ped only damaged by player: ", iPedNumber)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitset, ciPED_BS_SufferCritsOverride)
		SET_PED_SUFFERS_CRITICAL_HITS(piPassed, FALSE)
		SET_RAGDOLL_BLOCKING_FLAGS(piPassed, RBF_BULLET_IMPACT)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting ped immune to critical hits: ", iPedNumber)
	ENDIF
	
	BOOL bBullet = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_BulletProofFlag)
	BOOL bFlame = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_FlameProofFlag)
	BOOL bExplosion = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_ExplosionProofFlag)
	BOOL bCollision = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_CollisionProofFlag)
	BOOL bMelee = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_MeleeProofFlag)
	BOOL bSteam = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_SteamProofFlag)
	BOOL bSmoke = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_SmokeProofFlag)
	SET_ENTITY_PROOFS(piPassed, bBullet, bFlame, bExplosion, bCollision, bMelee, bSteam,DEFAULT, bSmoke)
	
	PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - proofs: bullet ", bBullet, ", flame ", bFlame, ", explosion ", bExplosion, ", collision ", bCollision, ", melee ", bMelee, ", steam ", bSteam, ", smoke ", bSmoke)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_DisableRagdolling)

		SET_RAGDOLL_BLOCKING_FLAGS(piPassed,
			RBF_PLAYER_IMPACT | RBF_PLAYER_BUMP | RBF_PLAYER_RAGDOLL_BUMP | RBF_PED_RAGDOLL_BUMP | RBF_ALLOW_BLOCK_DEAD_PED |
			RBF_BULLET_IMPACT | RBF_RUBBER_BULLET | RBF_MELEE | RBF_RUBBER_BULLET | RBF_WATER_JET | 
			RBF_FIRE | RBF_ELECTROCUTION | RBF_EXPLOSION | 
			RBF_FALLING | RBF_DROWNING | 
			RBF_VEHICLE_IMPACT | RBF_VEHICLE_GRAB | RBF_IMPACT_OBJECT)
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetTwentyOne, ciPED_BSTwentyOne_DoubleBlockRagdolling)
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Setting the 'DontActivateRagdoll' flags!")
			SET_PED_CONFIG_FLAG(piPassed, PCF_DontActivateRagdollFromVehicleImpact, TRUE)
			SET_PED_CONFIG_FLAG(piPassed, PCF_DontActivateRagdollFromBulletImpact, TRUE)
			SET_PED_CONFIG_FLAG(piPassed, PCF_DontActivateRagdollFromExplosions, TRUE)
			SET_PED_CONFIG_FLAG(piPassed, PCF_DontActivateRagdollFromFire, TRUE)
			SET_PED_CONFIG_FLAG(piPassed, PCF_DontActivateRagdollFromElectrocution, TRUE)
		ENDIF
		
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - SET_RAGDOLL_BLOCKING_FLAGS = ciPED_BSFour_DisableRagdolling")
		
	ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetNine, ciPed_BSNine_ImmuneToForcesFireExposionCollision)
	
		SET_RAGDOLL_BLOCKING_FLAGS(piPassed, RBF_FIRE | RBF_PLAYER_IMPACT | RBF_PED_RAGDOLL_BUMP | RBF_PLAYER_BUMP | RBF_PLAYER_IMPACT | RBF_PLAYER_RAGDOLL_BUMP | RBF_EXPLOSION)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - SET_RAGDOLL_BLOCKING_FLAGS = ciPed_BSNine_ImmuneToForcesFireExposionCollision")
		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetNine, ciPed_BSNine_ImmuneToForcesFireExposionCollision)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - ciPed_BSNine_ImmuneToForcesFireExposionCollision set on ped. disable ragdoll flags, call PCF_IgnoreBeingOnFire")
		SET_PED_UPPER_BODY_DAMAGE_ONLY(piPassed, TRUE)
		SET_PED_CONFIG_FLAG(piPassed, PCF_IgnoreBeingOnFire, TRUE)
	ENDIF
	
	INT iAccuracyCreator = GET_PED_ACCURACY_FROM_CREATOR_OPTION(iPedNumber)
	
	IF iAccuracyCreator = FMMC_PED_ACCURACY_EXTRA_LOW
		iAccuracy = 5
		eCombatAbility = CAL_POOR
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting ped combat ability extra poor and accuracy 5")
	ELIF iAccuracyCreator = FMMC_PED_ACCURACY_LOW
		iAccuracy = 15
		eCombatAbility = CAL_POOR
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting ped combat ability poor and accuracy 15")
	ELIF iAccuracyCreator = FMMC_PED_ACCURACY_HIGH
		iAccuracy = 40
		eCombatAbility = CAL_PROFESSIONAL
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting ped combat ability pro and accuracy 40")
	ELIF iAccuracyCreator = FMMC_PED_ACCURACY_EXTRA_HIGH
		iAccuracy = 60
		eCombatAbility = CAL_PROFESSIONAL
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting ped combat ability pro and accuracy 60")
	ELIF iAccuracyCreator = FMMC_PED_ACCURACY_100_PERCENT
		iAccuracy = 100
		eCombatAbility = CAL_PROFESSIONAL
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting ped combat ability pro and accuracy 100")
	ELSE
		iAccuracy = 25
		eCombatAbility = CAL_AVERAGE
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting ped combat ability av and accuracy 25")
	ENDIF
		
	FLOAT fAccuracy
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_UseHeistEnemyAccuracyAndHealthSettings)
		fAccuracy = iAccuracy + (0.25*iAccuracy*fDifficultyMod)
		
		IF GET_PED_ACCURACY_FROM_CREATOR_OPTION(iPedNumber) != FMMC_PED_ACCURACY_100_PERCENT
			fAccuracy = fAccuracy * fLesMultiplier
			
			iAccuracy = ROUND(fAccuracy)
		ENDIF
		
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - normal modded iAccuracy = ", iAccuracy)
		
	ELSE
		FLOAT fDifficultyMulti
		
		IF g_FMMC_STRUCT.iDifficulity = DIFF_EASY // No easy mode in Gang Ops heist missions
			fDifficultyMulti = 0.8
		ELIF g_FMMC_STRUCT.iDifficulity = DIFF_HARD
			fDifficultyMulti = 1.2
		ELSE // Normal
			fDifficultyMulti = 1.0
		ENDIF
		
		fAccuracy = iAccuracy * fDifficultyMulti
		iAccuracy = ROUND(fAccuracy)
		
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Gang Ops w fDifficultyMulti ", fDifficultyMulti, " -> modded iAccuracy = ", iAccuracy)
	ENDIF
	
	#IF IS_DEBUG_BUILD 
		IF lw_fSetMissPedAccuracy != 1.0
			
			faccuracy = iAccuracy * lw_fSetMissPedAccuracy
			
			iAccuracy = ROUND(faccuracy)
			
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - debug accuracy override starting iAccuracy = ", iAccuracy)
			
			IF iAccuracy <=15
				eCombatAbility = CAL_POOR
			ELIF iAccuracy <=25
				eCombatAbility = CAL_AVERAGE
			ELSE
				eCombatAbility = CAL_PROFESSIONAL
			ENDIF
		ENDIF
	#ENDIF
	
	IF iAccuracy < 0
		iAccuracy = 0
	ENDIF
	IF iAccuracy > 100
		iAccuracy = 100
	ENDIF
	
	SET_PED_COMBAT_ABILITY(piPassed, eCombatAbility)
	SET_PED_ACCURACY(piPassed, iAccuracy)
	PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - with accuracy ", iAccuracy, ", combat ability ", eNUM_TO_INT(eCombatAbility))
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetSeven, ciPED_BSSeven_LoudPropellorPlanes)
		START_AUDIO_SCENE("Speed_Race_Desert_Airport_Scene")
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_RocketAccuracy")
		fBreakLockAngle = g_fDebugBreakLockAngle
		fBreakLockAngleClose = g_fDebugBreakLockAngleClose
		fTurnRateModifier =  g_fDebugTurnModifier
		fBreakLockCloseDistance = g_fDebugLockCloseDitance
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Overriding ped homing missile details from widget iPedNumber = ", iPedNumber, " CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE = ", fBreakLockAngle, " CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE = ", fBreakLockAngleClose,  " CCF_HOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE = ", fBreakLockCloseDistance, " CCF_HOMING_ROCKET_TURN_RATE_MODIFIER = ", fTurnRateModifier) 
		
		SET_COMBAT_FLOAT(piPassed, CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE, fBreakLockAngle)
		SET_COMBAT_FLOAT(piPassed, CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE, fBreakLockAngleClose)
		SET_COMBAT_FLOAT(piPassed, CCF_HOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE, fBreakLockCloseDistance)
		SET_COMBAT_FLOAT(piPassed, CCF_HOMING_ROCKET_TURN_RATE_MODIFIER, fTurnRateModifier)
	ENDIF
	#ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThree, ciPED_BSThree_LoweredShootRate)
		IF g_FMMC_STRUCT.iDifficulity = DIFF_EASY //default shoot rate is 60
			SET_PED_SHOOT_RATE(piPassed,30)
		ELSE
			SET_PED_SHOOT_RATE(piPassed,50)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_DontBlindFireFromCover)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - as unable to blind fire from cover")
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_BLIND_FIRE_IN_COVER, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_UseStrictLOSAimChecks)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_DEFAULT_BLOCKED_LOS_POSITION_AND_DIRECTION, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_REQUIRES_LOS_TO_AIM, TRUE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - with CA_USE_DEFAULT_BLOCKED_LOS_POSITION_AND_DIRECTION and CA_REQUIRES_LOS_TO_AIM")
	ENDIF
	
	SET_PED_DIES_WHEN_INJURED(piPassed, TRUE)
	SET_PED_KEEP_TASK(piPassed, TRUE)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThree, ciPED_BSThree_DisableEvasiveDive)
		SET_PED_CAN_EVASIVE_DIVE(piPassed, FALSE)
	ENDIF
	
	IF IS_PED_IN_ANY_HELI(piPassed)
	OR IS_PED_IN_ANY_PLANE(piPassed)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThirteen, ciPed_BSThirteen_UseSmallPlaneAndHeliSpookRange)
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Setting heli or plane seeing range 299")
			SET_PED_SEEING_RANGE(piPassed, 299)
		ELSE			
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Setting heli or plane seeing range 100 and ciPed_BSThirteen_UseSmallPlaneAndHeliSpookRange")
			SET_PED_SEEING_RANGE(piPassed, 100)
		ENDIF
		
		IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(piPassed)) = piPassed			
			IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(piPassed)) = HUNTER
				SET_PED_FIRING_PATTERN(piPassed, INT_TO_ENUM(FIRING_PATTERN_HASH, HASH("FIRING_PATTERN_AKULA_BARRAGE")))
				PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Setting heli or plane ped to FIRING_PATTERN_AKULA_BARRAGE")
			ELSE
				SET_PED_FIRING_PATTERN(piPassed, FIRING_PATTERN_BURST_FIRE_HELI)
				PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Setting heli or plane ped to FIRING_PATTERN_BURST_FIRE_HELI")
			ENDIF
		ENDIF
		IF NOT MC_IS_PED_A_SCRIPTED_COP(iPedNumber)
			SET_PED_TARGET_LOSS_RESPONSE(piPassed, TLR_NEVER_LOSE_TARGET)
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Setting heli or plane ped to TLR_NEVER_LOSE_TARGET")
		ENDIF
	ENDIF
	
	FLOAT fInformRange = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iInformRange)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThree, ciPED_BSThree_AggroFailHasDelay)
		fInformRange = 5.0
	ENDIF
	
	PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - with inform_respected_friends range of ", fInformRange)
	SET_PED_TO_INFORM_RESPECTED_FRIENDS(piPassed, fInformRange, 50)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_UseInformRangeWhenReceivingEvents)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_MAX_SENSE_RANGE_WHEN_RECEIVING_EVENTS, TRUE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - with CA_USE_MAX_SENSE_RANGE_WHEN_RECEIVING_EVENTS")
	ELSE
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - not setting with CA_USE_MAX_SENSE_RANGE_WHEN_RECEIVING_EVENTS")
	ENDIF
	
	IF NOT IS_THIS_A_HEIST_MISSION()
		SET_ENTITY_VISIBLE(piPassed, bVisible)
	ELSE
		SET_ENTITY_VISIBLE(piPassed, TRUE)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_bogdangoon"))
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Adding PVG for model mp_m_bogdangoon and using G_M_M_X17_RSO_PVG")
		SET_PED_VOICE_GROUP(piPassed, GET_HASH_KEY("G_M_M_X17_RSO_PVG"))
	ENDIF	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Adding PVG for model mp_m_avongoon and using G_M_M_X17_AGuard_PVG")
		SET_PED_VOICE_GROUP(piPassed, GET_HASH_KEY("G_M_M_X17_AGuard_PVG"))
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_UseBlackopsVoice)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("S_M_Y_WestSec_01"))
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Adding PVG for model S_M_Y_WestSec_01 and using MP_BLACKOPS_R2PVG")
			SET_PED_VOICE_GROUP(piPassed, GET_HASH_KEY("MP_BLACKOPS_R2PVG"))
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("MP_G_M_Pros_01"))
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Adding PVG for model MP_G_M_Pros_01 and using MP_BLACKOPS_R2PVG")
			SET_PED_VOICE_GROUP(piPassed, GET_HASH_KEY("MP_BLACKOPS_R2PVG"))
		ENDIF
	ENDIF
	
	INT i
	TEXT_LABEL_63 tlAmbientVoice
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_G_VagFun_01"))
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Adding voice for model MP_M_G_VagFun_01")
		tlAmbientVoice = "GANG_MEXGOON_R2PVG"
	ENDIF
		
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("ig_taocheng2"))
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Adding voice for model ig_TAOCHENG2 because it sometimes sounds like lester...")
		tlAmbientVoice = "CHENG"
	ENDIF

	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAmbientVoice > -1
		tlAmbientVoice = g_FMMC_STRUCT.sSpeakers[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAmbientVoice]
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAmbientVoice > -1, to voice num: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAmbientVoice, " voice: ",tlAmbientVoice)
	ENDIF
		
	IF NOT IS_STRING_NULL_OR_EMPTY(tlAmbientVoice)	
		SET_AMBIENT_VOICE_NAME(piPassed,tlAmbientVoice)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting ped ambient voice for ped not in dialogue struct. to voice: ",tlAmbientVoice)	
	ELSE
		FOR i = 0  TO g_FMMC_STRUCT.iNumberOfDialogueTriggers - 1
			
			IF IS_VECTOR_ZERO(g_FMMC_STRUCT.sDialogueTriggers[i].vPosition)
				RELOOP
			ENDIF
			
			PRINTLN("[RCC MISSION] [DLG] - i = ", i)
			
			IF g_FMMC_STRUCT.sDialogueTriggers[i].iPedVoice = iPedNumber
				PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - [DLG] i = ", i)

				tlAmbientVoice = MC_GET_PED_AMBIENT_VOICE(i)

				IF NOT IS_STRING_NULL_OR_EMPTY(tlAmbientVoice)
					SET_AMBIENT_VOICE_NAME(piPassed,tlAmbientVoice)
					PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting ped ambient voice to voice: ",tlAmbientVoice)
				ENDIF
			ENDIF
			IF g_FMMC_STRUCT.sDialogueTriggers[i].iSecondPedVoice = iPedNumber
				PRINTLN("[Peds][Ped ", iPedNumber, "] - [DLG] i = ", i)

				tlAmbientVoice = MC_GET_PED_AMBIENT_VOICE(i)

				IF NOT IS_STRING_NULL_OR_EMPTY(tlAmbientVoice)
					SET_AMBIENT_VOICE_NAME(piPassed,tlAmbientVoice)
					PRINTLN("[RCC MISSION] MC_SET_UP_PED - setting second ped ambient voice for ped: ", iPedNumber, " to voice: ",tlAmbientVoice)
				ENDIF
			ENDIF
		ENDFOR
	
	ENDIF
		
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iDialogueLine = FMMC_DIALOGUE_LINE_PROVOKE_TRESPASS_FIB
	OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = S_M_M_FIBSEC_01
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Adding PVG using S_M_M_FIBOFFICE_01_R2PVG")
		SET_PED_VOICE_GROUP_FROM_RACE_TO_PVG(piPassed, GET_HASH_KEY("S_M_M_FIBOFFICE_01_R2PVG"))
	ENDIF	
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEleven, ciPed_BSEleven_UseAlternateComponentsForPedOutfit)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("S_M_Y_BLACKOPS_01"))
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Overriding the components of S_M_Y_BLACKOPS_01.")
			
			INT iRand = GET_RANDOM_INT_IN_RANGE(0,3)
			SWITCH iRand
				CASE 0
					PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Overriding the components of S_M_Y_BLACKOPS_01 (0).")
					SET_PED_COMPONENT_VARIATION(piPassed, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0)				
				BREAK
				
				CASE 1
					PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Overriding the components of S_M_Y_BLACKOPS_01 (1).")
					SET_PED_COMPONENT_VARIATION(piPassed, INT_TO_ENUM(PED_COMPONENT,8), 0, 1, 0) 				
				BREAK

				CASE 2
					PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Overriding the components of S_M_Y_BLACKOPS_01 (2).")
					SET_PED_COMPONENT_VARIATION(piPassed, INT_TO_ENUM(PED_COMPONENT,8), 0, 2, 0)				
				BREAK
			ENDSWITCH
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("G_M_M_CartelGuards_01"))
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Overriding the components of G_M_M_CartelGuards_01. Removing the Body Armour.")
			SET_PED_COMPONENT_VARIATION(piPassed, PED_COMP_SPECIAL2, 1, 0, 0)	
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("G_M_M_CartelGuards_02"))
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Overriding the components of G_M_M_CartelGuards_02. Removing the Body Armour.")
			SET_PED_COMPONENT_VARIATION(piPassed, PED_COMP_SPECIAL2, 0, 0, 0)	
		ENDIF
	ENDIF

	MC_SET_UP_PED_VISUAL_PROPERTIES(piPassed, iPedNumber)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitset, ciPED_BS_ExtendedLOS)
		SET_PED_SEEING_RANGE(piPassed, 150) 
		SET_PED_COMBAT_RANGE(piPassed, CR_FAR)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - ped set as long range combat")
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iMaxShootingRange != 0
		SET_COMBAT_FLOAT(piPassed, CCF_MAX_SHOOTING_DISTANCE, TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iMaxShootingRange))
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - CCF_MAX_SHOOTING_DISTANCE iMaxShootingRange = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iMaxShootingRange)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_PreventReactingToSilencedCloneBullets)
		SET_PED_CONFIG_FLAG(piPassed, PCF_PreventReactingToSilencedCloneBullets, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetTwentyOne, ciPED_BSTwentyOne_DisablePedSpeaking)
		STOP_PED_SPEAKING_SYNCED(piPassed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_DisableScreams)
		DISABLE_PED_PAIN_AUDIO(piPassed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_DontPlayHeadOnHornAnimWhenDiesInVeh)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - ped set to not play head on horn anim when dying in a vehicle: ", iPedNumber)
		SET_PED_PLAYS_HEAD_ON_HORN_ANIM_WHEN_DIES_IN_VEHICLE(piPassed, FALSE)
		SET_PED_CONFIG_FLAG(piPassed, PCF_DisableHornAudioWhenDead, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_Stop_Weapon_Firing_When_Dropped)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - ped set to stop weapon firing when dropped: ", iPedNumber)
		STOP_PED_WEAPON_FIRING_WHEN_DROPPED(piPassed)
		SET_PED_CONFIG_FLAG(piPassed, PCF_DisableBlindFiringInShotReactions, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEleven, ciPED_BSEleven_UsePerceptionConeForAimedAtEvents)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - ped set to use perception for aimed at events: ", iPedNumber)
		SET_PED_CONFIG_FLAG(piPassed, PCF_UseTargetPerceptionForCreatingAimedAtEvents, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_StartDead)
		IF HAS_ANIM_DICT_LOADED(MC_GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iDeadAnim))
			
			VECTOR vAnimPos = vPos
			
			SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iDeadAnim
				CASE ci_DEAD_PED_ANIM_FBI_CPR_IDLE
					vAnimPos.z -= 1.025
					PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Dead Ped - Moving down 1.025 to play anim")
				BREAK
				CASE ci_DEAD_PED_ANIM_IAA_MORGUE_SEARCH
					vAnimPos.z -= 2.06
					PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Dead Ped - Moving down 2 to play anim")
				BREAK
			ENDSWITCH
			
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Dead Ped - Using AnimDict: ", MC_GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iDeadAnim), " AnimName: ", MC_GET_DEAD_PED_ANIM_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iDeadAnim), " g_FMMC_STRUCT_ENTITIES.sPlacedPed[", iPedNumber, "].iDeadAnim: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iDeadAnim)
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Dead Ped - call TASK_PLAY_ANIM_ADVANCED at vAnimPos ",vAnimPos)
			TASK_PLAY_ANIM_ADVANCED(piPassed, MC_GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iDeadAnim), MC_GET_DEAD_PED_ANIM_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iDeadAnim), vAnimPos, GET_ENTITY_ROTATION(piPassed), INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS|AF_ENDS_IN_DEAD_POSE|AF_OVERRIDE_PHYSICS|AF_EXTRACT_INITIAL_OFFSET|AF_NOT_INTERRUPTABLE, 0.99) 
			FORCE_PED_AI_AND_ANIMATION_UPDATE(piPassed)
			
		#IF IS_DEBUG_BUILD
		ELSE
			CASSERTLN(DEBUG_CONTROLLER, "[RCC MISSION] MC_SET_UP_PED - Dead Ped ", iPedNumber, " anim dictionary has not loaded!")
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Dead Ped - anim dictionary has not loaded!")
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Dead Ped - Using AnimDict: ", MC_GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iDeadAnim), " AnimName: ", MC_GET_DEAD_PED_ANIM_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iDeadAnim), " g_FMMC_STRUCT_ENTITIES.sPlacedPed[", iPedNumber, "].iDeadAnim: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iDeadAnim)
		#ENDIF
		ENDIF
		
		SET_ENTITY_CAN_BE_DAMAGED(piPassed, TRUE)
		SET_ENTITY_INVINCIBLE(piPassed, TRUE)
		SET_PED_CAN_RAGDOLL(piPassed, FALSE)
		FREEZE_ENTITY_POSITION(piPassed, TRUE)
		
		SET_ENTITY_DYNAMIC(piPassed, FALSE)
		
		SET_PED_CONFIG_FLAG(piPassed, PCF_BlockDroppingHealthSnacksOnDeath, TRUE)
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFifteen, ciPED_BSFifteen_StartDeadWithPhysics)
		
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Dead Ped - Using Physics: ciPED_BSFifteen_StartDeadWithPhysics")
		
		TASK_PLAY_ANIM_ADVANCED(piPassed, "dead", "dead_a", vPos, GET_ENTITY_ROTATION(piPassed), INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_ENDS_IN_DEAD_POSE|AF_EXTRACT_INITIAL_OFFSET, 0.99) 
		FORCE_PED_AI_AND_ANIMATION_UPDATE(piPassed)
		
		SET_ENTITY_CAN_BE_DAMAGED(piPassed, FALSE)
		SET_ENTITY_INVINCIBLE(piPassed, FALSE)
		SET_PED_CAN_RAGDOLL(piPassed, TRUE)
		
		SET_PED_CONFIG_FLAG(piPassed, PCF_BlockDroppingHealthSnacksOnDeath, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEleven, ciPED_BSEleven_BlockSnacksDrop)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Setting PCF_BlockDroppingHealthSnacksOnDeath on ped ", iPedNumber)
		SET_PED_CONFIG_FLAG(piPassed, PCF_BlockDroppingHealthSnacksOnDeath, TRUE)
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEight, ciPed_BSEight_RemoveArmourOnCreation)
		SET_PED_ARMOUR(piPassed, 0)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Removing Ped armour on Creation via: ciPed_BSEight_RemoveArmourOnCreation, for ped: ", iPedNumber)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciPEDS_CANNOT_HAVE_ARMOUR)
		SET_PED_ARMOUR(piPassed, 0)
		PRINTLN("[RCC MISSION] MC_SET_UP_PED - Global Ped Override set for Armour. Removing it on creation., for ped: ", iPedNumber)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iInventoryBS != 0
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iInventoryBS, ciPED_INV_Briefcase_prop_ld_case_01)
		AND HAS_MODEL_LOADED(PROP_LD_CASE_01)
		AND HAS_ANIM_DICT_LOADED("weapons@misc@jerrycan@mp_male")
		AND CAN_REGISTER_MISSION_OBJECTS(1)
			RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+1)
			
			VECTOR vBriefcaseOffset = <<0.094,0.020,-0.005>>
			
			VECTOR vBriefcaseRot = << -92.240, 63.640, 150.240 >>
			
			NETWORK_INDEX niBriefcase
			
			FMMC_CREATE_NET_OBJ(niBriefcase,PROP_LD_CASE_01,vPos, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE) //Force Creation
			SET_ENTITY_LOD_DIST(NET_TO_ENT(niBriefcase),500)
			ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(niBriefcase),piPassed, GET_PED_BONE_INDEX(piPassed, bONETAG_PH_R_HAND),vBriefcaseOffset,vBriefcaseRot, TRUE)
			
			//TASK_PLAY_ANIM(piPassed, "anim@heists@biolab@", "Contact_Gives_Codes",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_UPPERBODY|AF_LOOPING|AF_SECONDARY)
			
			ANIM_DATA LocalAnimData, EmptyAnimData
			
			LocalAnimData.type = APT_SINGLE_ANIM
			LocalAnimData.dictionary0 = "weapons@misc@jerrycan@mp_male"
			LocalAnimData.anim0 = "idle"
			
			LocalAnimData.flags = AF_LOOPING | AF_UPPERBODY | AF_SECONDARY | AF_NOT_INTERRUPTABLE | AF_HIDE_WEAPON
			
			LocalAnimData.rate0 = 0.5
			
			LocalAnimData.filter = GET_HASH_KEY("BONEMASK_ARMONLY_R")
			
			TASK_SCRIPTED_ANIMATION(piPassed, LocalAnimData, EmptyAnimData, EmptyAnimData, INSTANT_BLEND_DURATION, SLOW_BLEND_DURATION)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LD_CASE_01)
			REMOVE_ANIM_DICT("anim@heists@biolab@")
			
		ENDIF
		//More inventory items go here:
	ENDIF

	MC_SET_ID_INT_DECOR_ON_ENTITY(GET_ENTITY_FROM_PED_OR_VEHICLE(piPassed), iPedNumber)
	
	IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iRespawnOnRule, -1)
		SET_ALLOW_MIGRATE_TO_SPECTATOR(piPassed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEight, ciPed_BSEight_DontDropWeaponOnDeath)
	OR IS_BIT_SET(g_FMMC_STRUCT.iWepRestBS, REST_DISABLE_PED_DROP)
		SET_PED_DROPS_WEAPONS_WHEN_DEAD(piPassed, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetNine, ciPed_BSNine_CantBeDamagedByPlayers)
		PRINTLN("[RCC MISSION] MC_SET_UP_PED - setting cant be damaged by players for ped: ", iPedNumber)
		SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(piPassed, FALSE, rgfm_PlayerTeam[0])
		SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, 0, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetTwelve, ciPed_BSTwelve_OnlyDamagedByPlayers)
		PRINTLN("[RCC MISSION] MC_SET_UP_PED - setting Can Only be damaged by players for ped: ", iPedNumber)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(piPassed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEleven, ciPED_BSEleven_BlockHomingLockon)
		PRINTLN("[RCC MISSION] MC_SET_UP_PED - setting cant be locked on for ped: ", iPedNumber)
		SET_PED_CONFIG_FLAG(piPassed, PCF_DisableHomingMissileLockon, TRUE)
	ENDIF
			
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetTwelve, ciPed_BSTwelve_IgnoreMaxNumberOfMeleeCombatants)
		PRINTLN("[RCC MISSION] MC_SET_UP_PED - setting PCF_ForceIgnoreMaxMeleeActiveSupportCombatants for iPed: ", iPedNumber)
		SET_PED_CONFIG_FLAG(piPassed, PCF_ForceIgnoreMaxMeleeActiveSupportCombatants, TRUE)
	ENDIF	
	
	// If any of these are set, then we want to trigger them all.
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iEntityRelGroupDamageRestrictions <> 0
		INT iEntDmgRestriction = 0
		FOR iEntDmgRestriction = 0 TO OPTION_ENTITY_CAN_ONLY_BE_DAMAGE_BY_RELGR_MAX
			PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Relationship Group Number: ", iEntDmgRestriction, " Set: ", IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iEntityRelGroupDamageRestrictions, iEntDmgRestriction))
			SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(piPassed, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iEntityRelGroupDamageRestrictions, iEntDmgRestriction), GET_REL_GROUP_HASH_FROM_INT(iEntDmgRestriction))			
		ENDFOR
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetTwelve, ciPed_BSTwelve_DisableRagdollingFromPlayerImpacts)	
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - setting ped to ignore player impacts causing ragdoll (SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT) ped: ", iPedNumber)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(piPassed, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFourteen, ciPED_BSFourteen_ForceActionModeUnholsterWeapon)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - (ForceActionModeUnholsterWeapon) - Setting SET_PED_USING_ACTION_MODE to DEFAULT_ACTION, and equipping current weapon.")		
		SET_PED_USING_ACTION_MODE(piPassed, TRUE, -1, "DEFAULT_ACTION")
		SET_CURRENT_PED_WEAPON(piPassed, GET_PED_WEAPON_TYPE_FROM_CREATOR_OPTION(iPedNumber), TRUE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_UN_Wanted_Level_bug")
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Setting PCF_DontInfluenceWantedLevel...")
		SET_PED_CONFIG_FLAG(piPassed, PCF_DontInfluenceWantedLevel, TRUE)
	ENDIF
	#ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFourteen, ciPED_BSFourteen_DisablePedFromBeingTargeted)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Using ciPED_BSFourteen_DisablePedFromBeingTargeted...")
		SET_PED_CAN_BE_TARGETTED(piPassed, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFifteen, ciPED_BSFifteen_DisableClimbingLadders)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Using ciPED_BSFifteen_DisableClimbingLadders...")
		SET_PED_CONFIG_FLAG(piPassed, PCF_DisableLadderClimbing, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetTwentyOne, ciPED_BSTwentyOne_BlockCoverCombatAttribute)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - CA_USE_COVER = FALSE...")
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_COVER, FALSE)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iBloodPreset != ciPED_BLOOD_PRESET_NONE
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEighteen, ciPed_BSEighteen_PedBlood_AfterIdleAnimPlays)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEighteen, ciPed_BSEighteen_PedBlood_AfterSpecialAnimPlays)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Applying Blood Preset ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iBloodPreset, " to the Ped")
		APPLY_BLOOD_TO_PED(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iBloodPreset, piPassed)
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_PedSpecialTracking)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iContinuityId > -1
		AND FMMC_IS_LONG_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iPedSpecialBitset, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iContinuityId)
			SWITCH g_FMMC_STRUCT.eContinuitySpecialPedType
				CASE FMMC_MISSION_CONTINUITY_PED_TRACKING_TYPE_TRANQUILIZED
					PRINTLN("[JS][CONTINUITY][Ped ", iPedNumber, "] - MC_SET_UP_PED - Ped was tranquilised on a previous strand mission!")
					SET_PED_USING_ACTION_MODE(piPassed, FALSE, -1, "DEFAULT_ACTION")	
					SET_PED_MOVEMENT_CLIPSET(piPassed, "move_m@drunk@moderatedrunk", 0.75)					
					SET_PED_COMBAT_ABILITY(piPassed, CAL_POOR)					
				BREAK
				DEFAULT
					PRINTLN("[JS][CONTINUITY][Ped ", iPedNumber, "] - MC_SET_UP_PED - Tracking special but type is invalid")
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
	IF IS_STEALTH_AND_AGGRO_SYSTEMS_VALID_ON_THIS_RULE(iPedNumber)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piPassed, TRUE)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Calling SET_BLOCKING_OF_NON_TEMPORARY_EVENTS with TRUE")
	ENDIF
	
	IF IS_PED_A_PLAYER_ABILITY_PED(iPedNumber)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Setting up ped with the player ability relationship group!")
		SET_PED_RELATIONSHIP_GROUP_HASH(piPassed, relPlayerAbilityPed)
	ENDIF	
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetTwentyTwo, ciPED_BSTwentyTwo_PreventWantedLevelInfluenceWhenNotAggroed)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Setting up ped with PCF_DontInfluenceWantedLevel true due to ciPED_BSTwentyTwo_PreventWantedLevelInfluenceWhenNotAggroed")
		SET_PED_CONFIG_FLAG(piPassed, PCF_DontInfluenceWantedLevel, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetTwentyTwo, ciPED_BSTwentyTwo_EnableCanChaseTargetCombatFlag)
		PRINTLN("[Peds][Ped ", iPedNumber, "] - MC_SET_UP_PED - Setting up ped with CA_CAN_CHASE_TARGET_ON_FOOT true due to ciPED_BSTwentyTwo_EnableCanChaseTargetCombatFlag")
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_CHASE_TARGET_ON_FOOT, TRUE)
	ENDIF
	
ENDPROC

FUNC BOOL FMMC_ADD_A_NEW_CLONE_TARGET_TO_THE_SERVER_ARRAY(PED_INDEX piTarget, PLAYER_INDEX playerSource)
		//Make sure we don't go over the limit
		IF MC_serverBD_4.sPedCloningData.iNumOfEntsToClone >= FMMC_MAX_PEDS
			ASSERTLN("[PlayerPedClone] - FMMC_ADD_A_NEW_CLONE_TARGET_TO_THE_SERVER_ARRAY - We are over the array limit. Current arr size = ", MC_serverBD_4.sPedCloningData.iNumOfEntsToClone, ", Max size = ", FMMC_MAX_PEDS)
			RETURN FALSE
		ENDIF
		
		//Make sure the local player is the host
		IF !bIsLocalPlayerHost
			ASSERTLN("[PlayerPedClone] - FMMC_ADD_A_NEW_CLONE_TARGET_TO_THE_SERVER_ARRAY - Only a host can add to the server array!")
			RETURN FALSE
		ENDIF
		
		INT iIndex 
		iIndex = MC_serverBD_4.sPedCloningData.iNumOfEntsToClone
		MC_serverBD_4.sPedCloningData.playerSource[iIndex]  = playerSource
		MC_serverBD_4.sPedCloningData.niTarget[iIndex] 		= PED_TO_NET(piTarget)
		MC_serverBD_4.sPedCloningData.iNumOfEntsToClone++
		PRINTLN("[PlayerPedClone] - FMMC_ADD_A_NEW_CLONE_TARGET_TO_THE_SERVER_ARRAY - Adding the source and the target to the sPedCloningData. Arr Index - ", iIndex ,", Target - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(piTarget), ", Source - ", GET_PLAYER_NAME(playerSource))
		RETURN TRUE
ENDFUNC

/// PURPOSE:
///    This function takes care of the ped cloning process. It's capable of cloning peds as well as players (target needs to be networked)
/// PARAMS:
///    piTarget - Ped that will become a clone
///    piSource - Ped/Player that we want to clone
///    bUseLocalHeadBlendIniFromServerArray - set this to true if creating a large num of player clones at once (e.g. when using a loop). That way each player will init the head blend from the server array. The max num of peds we can clone at once is = FMMC_MAX_PEDS
FUNC BOOL FMMC_CLONE_PED(PED_INDEX piTarget, PED_INDEX piSource, BOOL bUseLocalHeadBlendIniFromServerArray = FALSE)
#IF IS_DEBUG_BUILD
	STRING sDebugTag = "[PedClone] "
	
	IF DOES_ENTITY_EXIST(piSource) AND IS_PED_A_PLAYER(piSource)
		sDebugTag = "[PlayerPedClone] "
	ENDIF
#ENDIF
	
	PRINTLN(sDebugTag, "FMMC_CLONE_PED - bUseLocalHeadBlendIniFromServerArraytialisation = ", BOOL_TO_STRING(bUseLocalHeadBlendIniFromServerArray))
	
	IF NOT IS_ENTITY_ALIVE(piTarget) OR NOT IS_ENTITY_ALIVE(piSource)
		PRINTLN(sDebugTag, "FMMC_CLONE_PED - Invalid entity!")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(GET_ENTITY_MODEL(piTarget))
		PRINTLN(sDebugTag, "FMMC_CLONE_PED - Waiting for the model to load!")
		RETURN FALSE
	ENDIF
	
	IF NETWORK_GET_ENTITY_IS_NETWORKED(piTarget)
	AND NOT NETWORK_REQUEST_CONTROL_OF_ENTITY(piTarget)
		PRINTLN(sDebugTag, "FMMC_CLONE_PED - Requesting control of the entity!")
		RETURN FALSE
	ENDIF
	
	CLEAR_PED_BLOOD_DAMAGE(piTarget)
	CLONE_PED_TO_TARGET_ALT(piSource, piTarget)
	FINALIZE_HEAD_BLEND(piTarget)

	IF IS_PED_A_PLAYER(piSource)
		IF NETWORK_HAS_CACHED_PLAYER_HEAD_BLEND_DATA(NETWORK_GET_PLAYER_INDEX_FROM_PED(piSource))
			INT iTargetPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(NETWORK_GET_PLAYER_INDEX_FROM_PED(piSource)))			
			IF bUseLocalHeadBlendIniFromServerArray			
				IF NOT FMMC_ADD_A_NEW_CLONE_TARGET_TO_THE_SERVER_ARRAY(piTarget, NETWORK_GET_PLAYER_INDEX_FROM_PED(piSource))
					PRINTLN(sDebugTag, "FMMC_CLONE_PED - FMMC_ADD_A_NEW_CLONE_TARGET_TO_THE_SERVER_ARRAY failed! We will try to use broadcasting to set the blend instead of using the server array!")
					bUseLocalHeadBlendIniFromServerArray = FALSE
				ENDIF
			ENDIF
			
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iTargetPart, g_ciInstancedcontentEventType_ApplyCachedPlayerHeadBlendDataToPlayerPedClones, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bUseLocalHeadBlendIniFromServerArray, DEFAULT, DEFAULT, PED_TO_NET(piTarget))
			PRINTLN(sDebugTag, "FMMC_CLONE_PED - cloning player's ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(piSource)), " model to SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(piTarget), ", Model Name - ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(piTarget)))
		ELSE
			PRINTLN(sDebugTag, "FMMC_CLONE_PED - The target player has no cahced head blend data! - Unable to clone player's ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(piSource)), " model to SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(piTarget), ", Model Name - ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(piTarget)))		
			RETURN FALSE
		ENDIF
	ELSE
		PRINTLN(sDebugTag, "FMMC_CLONE_PED - cloning ped's SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(piSource), " model to SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(piTarget), ", Model Name - ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(piTarget)))
	ENDIF
	
	RETURN TRUE
ENDFUNC

//////////////////////////////      MP PEDS           //////////////////////////////
FUNC BOOL MC_CREATE_PEDS(NETWORK_INDEX &niPed[], NETWORK_INDEX &niVeh[])

	INT i
	VEHICLE_SEAT tempVehSeat
	
	//Don't stagger on fade transitions
	BOOL bStrand = IS_A_STRAND_MISSION_BEING_INITIALISED() AND (GET_STRAND_MISSION_TRANSITION_TYPE() != FMMC_BRANCHING_MISSION_TRANSITION_TYPE_FADEOUT)
	BOOL bVisible
	
	IF bStrand
		bVisible = TRUE
	ENDIF
	INT iRequestThisFrame
		
	PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_CREATE_PEDS - number of peds: ", g_FMMC_STRUCT_ENTITIES.iNumberOfPeds)
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds i
					
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn != DUMMY_MODEL_FOR_SCRIPT
			IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRespawnOnRule, -1)
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ENTITIES_CAN_RESPAWN_ON_RULE_CHANGE)
				SET_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_ENTITIES_CAN_RESPAWN_ON_RULE_CHANGE)
			ENDIF
			
			MODEL_NAMES mnPedModelToUse = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn
			PED_INDEX piPedToClone = NULL
			IF IS_PLACED_PED_A_PLAYER_CLONE(mnPedModelToUse, i)
				piPedToClone = GET_PLAYER_PED(SWAP_PED_MODEL_FOR_A_PLAYER_CLONE_AND_GET_TARGET_PLAYER_INDEX(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i], mnPedModelToUse))
			ENDIF

			IF MC_SHOULD_PED_SPAWN_AT_START(i, FALSE)
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niPed[i])
					BOOL success = FALSE
					VEHICLE_INDEX tempVeh = NULL
					BOOL bDontFreeze = FALSE
					STRING printOut
					UNUSED_PARAMETER(printOut)
					BOOL bReadyToMakePed = TRUE
					
					//If this ped is the Juggernaut, hold up the function until the anims are loaded
					IF mnPedModelToUse = INT_TO_ENUM(MODEL_NAMES, HASH("U_M_Y_Juggernaut_01"))
					OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetSixteen, ciPED_BSSixteen_UseJuggernautMoveset)	
						IF NOT HAVE_JUGGERNAUT_ANIMS_LOADED_FOR_CREATION()
							bReadyToMakePed = FALSE
							PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_CREATE_PEDS - Holding back creation while loading Juggernaut animations")
						ENDIF
					ENDIF
					
					INT iRandomHeadingSelected = 0
					VECTOR vPos = MC_GET_PED_SPAWN_LOCATION(i, iServerPropertyID, iRandomHeadingSelected, TRUE)
					FLOAT fHead = MC_GET_PED_SPAWN_HEADING(i, iServerPropertyID, DEFAULT, iRandomHeadingSelected, TRUE)
					
					PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_CREATE_PEDS - vPos: ", vPos)
					
					IF bReadyToMakePed
						IF NOT SHOULD_PED_SPAWN_IN_VEHICLE(i)					
							IF FMMC_CREATE_NET_PED(niPed[i], PEDTYPE_MISSION, mnPedModelToUse, vPos, fHead,
										DEFAULT, DEFAULT, DEFAULT, TRUE) //Force Creation
										
								success = TRUE								
								PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_CREATE_PEDS - FMMC PED CREATED")
							ENDIF
						ELSE
							IF IS_NET_VEHICLE_DRIVEABLE(niVeh[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle])
								IF NOT IS_VEHICLE_FULL(NET_TO_VEH(niVeh[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle]))
									IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicleSeat = -3
										tempVehSeat = MC_GET_FIRST_FREE_VEHICLE_SEAT(NET_TO_VEH(niVeh[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle]))
									ELSE
										VEHICLE_SEAT seat = INT_TO_ENUM(VEHICLE_SEAT, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicleSeat)
										IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicleSeat = VS_ANCHORED_1
										OR IS_VEHICLE_SEAT_FREE(NET_TO_VEH(niVeh[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle]),seat)
											tempVehSeat = INT_TO_ENUM(VEHICLE_SEAT, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicleSeat)
										ELSE
											tempVehSeat = MC_GET_FIRST_FREE_VEHICLE_SEAT(NET_TO_VEH(niVeh[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle]))
										ENDIF
									ENDIF
									IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicleSeat = VS_ANCHORED_1
										vPos = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos
										IF FMMC_CREATE_NET_PED(niPed[i], PEDTYPE_MISSION, mnPedModelToUse, vPos, MC_GET_PED_SPAWN_HEADING(i, iServerPropertyID, DEFAULT, DEFAULT, TRUE),
													DEFAULT, DEFAULT, DEFAULT, TRUE) //Force Creation
													
											VECTOR offset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(NET_TO_VEH(niVeh[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle]), vPos)
											offset = offset + <<0.0,0.0, 1.85*0.5>>
											FLOAT heading = MC_GET_PED_SPAWN_HEADING(i, iServerPropertyID, DEFAULT, DEFAULT, TRUE) - GET_ENTITY_HEADING(NET_TO_VEH(niVeh[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle]))											
											ATTACH_ENTITY_TO_ENTITY(NET_TO_PED(niPed[i]), NET_TO_VEH(niVeh[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle]), 0, offset, <<0,0,heading>>, TRUE, TRUE)
											success = TRUE											
											PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_CREATE_PEDS - FMMC PED CREATED ANCHORED")
										ENDIF	
									ELIF FMMC_CREATE_NET_PED_IN_VEHICLE(niPed[i], niVeh[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle], PEDTYPE_MISSION, mnPedModelToUse,tempVehSeat,
												DEFAULT, DEFAULT, DEFAULT, TRUE) //Force Creation
												
										success = TRUE
										tempVeh = NET_TO_VEH(niVeh[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle])
										bDontFreeze = TRUE
										PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_CREATE_PEDS - FMMC PED CREATED IN VEHICLE")
									ENDIF
								ELSE
									IF FMMC_CREATE_NET_PED(niPed[i], PEDTYPE_MISSION, mnPedModelToUse, vPos, fHead,
												DEFAULT, DEFAULT, DEFAULT, TRUE) //Force Creation
												
										success = TRUE										
										PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_CREATE_PEDS - FMMC PED CREATED VEHICLE FULL")
									ENDIF	
								ENDIF
							ELSE
								IF FMMC_CREATE_NET_PED(niPed[i], PEDTYPE_MISSION, mnPedModelToUse, vPos, fHead,
											DEFAULT, DEFAULT, DEFAULT, TRUE) //Force Creation
											
									success = TRUE									
									PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_CREATE_PEDS - FMMC PED CREATED VEHICLE DEAD")
								ENDIF	
								PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_CREATE_PEDS - spawn vehicle not drivable")
							ENDIF
						ENDIF		
						
					ENDIF
					IF success
						MC_SET_UP_PED(NET_TO_PED(niPed[i]), i, bVisible, tempVeh)	
						SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE(niPed[i], TRUE, bDontFreeze)
						
						IF piPedToClone != NULL
							FMMC_CLONE_PED(NET_TO_PED(niPed[i]), piPedToClone, TRUE)
							SET_MODEL_AS_NO_LONGER_NEEDED(mnPedModelToUse)
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetTwo, ciPed_BSTwo_RespawnInNewVehicle)
							IF NOT IS_BIT_SET(MC_serverBD.iPedFirstSpawnVehBitset, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle)
								SET_BIT(MC_serverBD.iPedFirstSpawnVehBitset, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle)						
							ENDIF
						ENDIF
												
						IF MC_PROCESS_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sWarpLocationSettings, CREATION_TYPE_PEDS, i, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSpawnGroup, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSpawnSubGroupBS, NET_TO_PED(niPed[i]))	
							MC_REMOVE_FMMC_PED_DEFENSIVE_AREA(NET_TO_PED(niPed[i]), i, TRUE, TRUE)
							MC_SET_UP_FMMC_PED_DEFENSIVE_AREA(NET_TO_PED(niPed[i]), i, GET_ENTITY_COORDS(NET_TO_PED(niPed[i]), FALSE), TRUE)	
						ENDIF
						
						MC_serverBD_1.sCreatedCount.iNumPedCreated[0]++
						iSpawnPoolPedIndex = -1
						PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_CREATE_PEDS - Added to iNumPedCreated, is now: ", MC_serverBD_1.sCreatedCount.iNumPedCreated[0], " for iRule: ", 0)						
						IF bStrand
							IF iRequestThisFrame >= PICK_INT(IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING(), ciFMMC_MAX_PER_FRAME_CREATES_AIRLOCK, ciFMMC_MAX_PER_FRAME_CREATES)
								RETURN FALSE
							ELSE
								iRequestThisFrame++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_CREATE_PEDS - is not set to spawn at start")				
			ENDIF
		ELSE
			PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_CREATE_PEDS - is dummy model")			
		ENDIF	
	ENDREPEAT
	//Check we have created all Security Guards
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds i
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niPed[i])
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn != DUMMY_MODEL_FOR_SCRIPT
				IF MC_SHOULD_PED_SPAWN_AT_START(i, FALSE)
					PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_CREATE_PEDS - Stuck on this ped.")								
					RETURN FALSE
				ELSE
					PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_CREATE_PEDS - is NOT set to spawn at start 2.")					
				ENDIF
			ELSE
				PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_CREATE_PEDS - is dummy model 2.")				
			ENDIF
		ELSE
			PRINTLN("[MCSpawning][Peds][Ped ", i, "] - MC_CREATE_PEDS - exists.")			
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicles (Utility and Helper Functions) 
// ##### Description: Process the creation of Mission Controller Vehicles.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

///PURPOSE: this function checks to see if a mission critical vehicle is moving on
///    to a kill rule, and if so, it sets a bit that stops this team for failing if it's destroyed
PROC SET_VEHICLE_CAN_BE_KILLED_ON_THIS_RULE(INT iVehicleIndex, INT iTeam)

	IF iVehicleIndex > -1
		IF MC_serverBD_4.ivehRule[iVehicleIndex][iteam] = FMMC_OBJECTIVE_LOGIC_KILL
			IF iteam = 0
				SET_BIT(MC_serverBD.iVehteamFailBitset[iVehicleIndex], SBBOOL1_TEAM0_NEEDS_KILL)
			ELIF iteam = 1
				SET_BIT(MC_serverBD.iVehteamFailBitset[iVehicleIndex], SBBOOL1_TEAM1_NEEDS_KILL)
			ELIF iteam = 2
				SET_BIT(MC_serverBD.iVehteamFailBitset[iVehicleIndex], SBBOOL1_TEAM2_NEEDS_KILL)
			ELIF iteam = 3
				SET_BIT(MC_serverBD.iVehteamFailBitset[iVehicleIndex], SBBOOL1_TEAM3_NEEDS_KILL)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL CAN_VEH_STILL_SPAWN_IN_MISSION(INT iveh, INT iTeamOverride = -1, INT iRuleOverride = -1)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAssociatedObjective
					// The vehicle will have a chance to spawn:
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAssociatedObjective
					// The vehicle still has a chance to spawn:
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE // Spawn type won't prevent vehicle from spawning after the rule
			RETURN TRUE
		ENDIF
	ELSE
		//Vehicle will just spawn at mission start!
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSecondAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSecondAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSecondAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSecondAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSecondAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSecondAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSecondAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iThirdAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iThirdAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iThirdAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iThirdAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iThirdAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iThirdAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iThirdAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iFourthAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iFourthAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iFourthAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iFourthAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iFourthAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iFourthAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iFourthAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


FUNC BOOL MC_HAS_VEHICLE_CREATION_FINISHED(NETWORK_INDEX niVehicle,NETWORK_INDEX niVehicleCrate, iNT i)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
	AND NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle)	
		RETURN FALSE
	ENDIF
	
	IF GET_VEHICLE_CARGO_MODEL_FROM_INT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCargo, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn) != DUMMY_MODEL_FOR_SCRIPT
	AND NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicleCrate)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_VEHICLE(INT iVehicleIndex, BOOL bCheckTeams)
	BOOL bReturn
	bReturn = MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_ENTITY(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehicleIndex].iSpawnPlayerNum, GET_TOTAL_STARTING_PLAYERS())
	
	IF bCheckTeams
		IF MC_serverBD.iNumStartingPlayers[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehicleIndex].iTeamSpawnReq] >= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehicleIndex].iTeamSpawnPlayersReq
			bReturn = TRUE
		ELSE
			bReturn = FALSE
		ENDIF
	ENDIF
	
	RETURN bReturn
ENDFUNC

FUNC BOOL MC_IS_VEH_GOING_TO_SPAWN_LATER(INT i)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = DUMMY_MODEL_FOR_SCRIPT
		RETURN FALSE
	ENDIF
	
	IF MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].eSpawnConditionFlag, FALSE, TRUE, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAggroIndexBS_Entity_Veh)
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_IS_VEH_GOING_TO_SPAWN_LATER - Returning FALSE due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")
		RETURN FALSE
	ENDIF
	
	BOOL bSuitableSpawnGroup = MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_Vehicle, DEFAULT, DEFAULT, TRUE)
	
	BOOL bCheckTeams
	IF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iTeamSpawnReq != -1)
		bCheckTeams = TRUE
	ENDIF
	
	IF MC_WILL_ENTITY_CLEANUP_IMMEDIATELY(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCleanupTeam, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCleanupObjective, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetTwo, ciFMMC_VEHICLE2_CleanupAtMidpoint))
	OR MC_WILL_CHECKPOINTS_STOP_THIS_SPAWN(i, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetThree, ciFMMC_VEHICLE3_DontSpawnOnCheckpoint_0), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetThree, ciFMMC_VEHICLE3_DontSpawnOnCheckpoint_1), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetSeven, ciFMMC_VEHICLE3_DontSpawnOnCheckpoint_2), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetSeven, ciFMMC_VEHICLE7_DontSpawnOnCheckpoint_3), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetNine, ciFMMC_VEHICLE9_BlockSpawnIfNotACheckpoint))
	OR NOT bSuitableSpawnGroup
	OR NOT MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_VEHICLE(i, bCheckTeams)
		RETURN FALSE
	ENDIF
		
	IF MC_ARE_SPAWN_CONDITION_FLAGS_MET_DURING_GAMEPLAY(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].eSpawnConditionFlag)
		IF MC_SHOULD_ENTITY_SPAWN_AT_START(CREATION_TYPE_VEHICLES, i, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedObjective, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedSpawn, TRUE, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedAlwaysForceSpawnOnRule)
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_IS_VEH_GOING_TO_SPAWN_LATER - Returning TRUE due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")			
			RETURN TRUE
		ELSE
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_IS_VEH_GOING_TO_SPAWN_LATER - Returning FALSE due to not being set up to spawn at the start ....")
		ENDIF
	ENDIF
				
	IF MC_IS_ENTITY_SPAWN_TRIGGERED_BY_ZONE_ENTRY(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iZoneBlockEntitySpawn)
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_IS_VEH_GOING_TO_SPAWN_LATER - Returning TRUE due to iZoneBlockEntitySpawn: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iZoneBlockEntitySpawn)
		RETURN TRUE
	ENDIF
	
	IF MC_SHOULD_ENTITY_SPAWN_LATER(CREATION_TYPE_VEHICLES, i, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedObjective, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedSpawn, TRUE, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedAlwaysForceSpawnOnRule)		
		IF bSuitableSpawnGroup
			RETURN TRUE
		ENDIF	
	ENDIF

	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCSRespawnSceneIndex > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCSRespawnShotIndex > -1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL MC_SHOULD_VEH_SPAWN_AT_START(INT i, BOOL bArrayCheck)
	
	IF IS_BIT_SET(g_TransitionSessionNonResetVars.iDoNotRespawnVehBitSet, i)
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_SHOULD_VEH_SPAWN_AT_START - FALSE iDoNotRespawnVehBitSet SET")
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = DUMMY_MODEL_FOR_SCRIPT
		RETURN FALSE
	ENDIF

	IF MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].eSpawnConditionFlag, TRUE, FALSE, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAggroIndexBS_Entity_Veh)
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_SHOULD_VEH_SPAWN_AT_START - Returning FALSE due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_VehicleDestroyedTracking)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iContinuityId > -1
		AND IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iVehicleDestroyedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iContinuityId)
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_SHOULD_VEH_SPAWN_AT_START - with continuity ID ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iContinuityId, " not spawning due to death of previous stand mission")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT bArrayCheck
		BOOL bCheckTeams
		IF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iTeamSpawnReq != -1)
			bCheckTeams = TRUE
		ENDIF
		
		IF MC_WILL_CHECKPOINTS_STOP_THIS_SPAWN(i, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetThree, ciFMMC_VEHICLE3_DontSpawnOnCheckpoint_0), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetThree, ciFMMC_VEHICLE3_DontSpawnOnCheckpoint_1), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetSeven, ciFMMC_VEHICLE3_DontSpawnOnCheckpoint_2), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetSeven, ciFMMC_VEHICLE7_DontSpawnOnCheckpoint_3), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetNine, ciFMMC_VEHICLE9_BlockSpawnIfNotACheckpoint))
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_SHOULD_VEH_SPAWN_AT_START - Returning FALSE due to MC_WILL_CHECKPOINTS_STOP_THIS_SPAWN")
			RETURN FALSE
		ENDIF
		
		IF MC_WILL_ENTITY_CLEANUP_IMMEDIATELY(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCleanupTeam, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCleanupObjective, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetTwo, ciFMMC_VEHICLE2_CleanupAtMidpoint))
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_SHOULD_VEH_SPAWN_AT_START - Returning FALSE due to MC_WILL_ENTITY_CLEANUP_IMMEDIATELY")
			RETURN FALSE
		ENDIF
		
		IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_Vehicle)
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_SHOULD_VEH_SPAWN_AT_START - Returning FALSE due to MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP")
			RETURN FALSE
		ENDIF
		
		IF NOT MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_VEHICLE(i, bCheckTeams)
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_SHOULD_VEH_SPAWN_AT_START - Returning FALSE due to MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_VEHICLE")
			RETURN FALSE
		ENDIF
	ENDIF
		
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sTrainAttachmentData.iTrainIndex > -1
 		IF NOT IS_TRAIN_ATTACHMENT_READY(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sTrainAttachmentData)
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_SHOULD_VEH_SPAWN_AT_START - return FALSE - Train isn't ready")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF DOES_VEH_HAVE_PLAYER_VARIABLE_SETTING(i)
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_SHOULD_VEH_SPAWN_AT_START - DOES_VEH_HAVE_PLAYER_VARIABLE_SETTING: TRUE, iNumStartingPlayers: ",MC_serverBD.iNumStartingPlayers[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedTeam], "inumvehcreated[0]: ",MC_serverBD_1.sCreatedCount.inumvehcreated[0])		
		IF (MC_serverBD_1.sCreatedCount.inumvehcreated[0] >= MC_serverBD.iNumStartingPlayers[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedTeam]) OR (IS_FAKE_MULTIPLAYER_MODE_SET()	AND MC_serverBD_1.sCreatedCount.inumvehcreated[0] >= 1)
			SET_BIT(MC_serverBD_1.sCreatedCount.iSkipVehBitset, i)
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_SHOULD_VEH_SPAWN_AT_START - inumvehcreated[0] >= MC_serverBD.iNumStartingPlayers: FALSE")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCSRespawnSceneIndex > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCSRespawnShotIndex > -1
	AND g_FMMC_STRUCT.iMissionSubType != FMMC_MISSION_TYPE_CTF
		RETURN FALSE
	ENDIF
	
	IF MC_IS_ENTITY_SPAWN_TRIGGERED_BY_ZONE_ENTRY(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iZoneBlockEntitySpawn)
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_SHOULD_VEH_SPAWN_AT_START - MC_IS_ENTITY_SPAWN_TRIGGERED_BY_ZONE_ENTRY")
		RETURN FALSE
	ENDIF	
	
	IF MC_SHOULD_ENTITY_SPAWN_AT_START(CREATION_TYPE_VEHICLES, i, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedObjective, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedSpawn, NOT bArrayCheck, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedAlwaysForceSpawnOnRule)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC VECTOR MC_GET_VEH_SPAWN_LOCATION(INT iVeh, INT &iRandomHeadingToSelect, INT &iInterior, BOOL bisInitialSpawn = FALSE)
	
	VECTOR vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vPos
	iInterior = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehInterior
	
	IF bisInitialSpawn
		IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_VehiclePositionTracking)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iContinuityId > -1
			AND NOT IS_VECTOR_ZERO(sMissionLocalContinuityVars.vVehicleLocations[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iContinuityId])
				PRINTLN("[JS][CONTINUITY] - MC_GET_VEH_SPAWN_LOCATION - Vehicle ", iVeh, " (Continuity ID: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iContinuityID, "'s  Position overridden to: ", sMissionLocalContinuityVars.vVehicleLocations[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iContinuityID])
				iInterior = -1
				RETURN sMissionLocalContinuityVars.vVehicleLocations[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iContinuityId]
			ENDIF
		ENDIF
	ENDIF
		
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sTrainAttachmentData.iTrainIndex != -1
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sTrainAttachmentData.vCarriageOffsetPosition)		
		VECTOR vTemp = <<0.0, 0.0, 0.0>>
		MC_GET_TRAIN_ATTACHMENT_POINT_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sTrainAttachmentData, vSpawnLoc, vTemp)		
		RETURN vSpawnLoc
	ENDIF
	
	IF SHOULD_ENTITY_USE_CHECKPOINT_CONTINUITY_VARS(ciENTITY_TYPE_VEHICLE, iVeh)
		// This vehicle has got a position saved from the last checkpoint!
		VECTOR vEntityCheckpointPos = GET_COORDS_FOR_ENTITY_CHECKPOINT_CONTINUITY(ciENTITY_TYPE_VEHICLE, iVeh)
		PRINTLN("[CONTINUITY][EntityCheckpointContinuity] - MC_GET_VEH_SPAWN_LOCATION - Vehicle ", iVeh, " using saved checkpoint position ", vEntityCheckpointPos)
		RETURN vEntityCheckpointPos
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_USE_CHECKPOINTS_FOR_RANDOM_SPAWN)
		
		INT iNumberOfSpawns = GET_VEHICLE_NUMBER_OF_CHECKPOINT_SPAWN_POSITIONS(iVeh) + 1 // 1 for the normal starting position
		
		PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_LOCATION - RANDOM - Picking random spawn from possible ", iNumberOfSpawns, " (", iNumberOfSpawns-1, " checkpoint locations & 1 starting location)")
		
		SET_RANDOM_SEED(NATIVE_TO_INT(GET_NETWORK_TIME()))
		INT iChoice = GET_RANDOM_INT_IN_RANGE(0, iNumberOfSpawns)
		SWITCH iChoice
			CASE 0
				iRandomHeadingToSelect = 0
				iInterior = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehInterior
				PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_LOCATION - RANDOM -Spawning at placed location ", vSpawnLoc)
			BREAK
			CASE 1
				iRandomHeadingToSelect = 1
				vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vRestartPos[0]
				iInterior = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRestartInterior[0]
				PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_LOCATION - RANDOM -Spawning at quick restart point 1 ", vSpawnLoc)
			BREAK
			CASE 2
				iRandomHeadingToSelect = 2
				vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vRestartPos[1]
				iInterior = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRestartInterior[1]
				PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_LOCATION - RANDOM -Spawning at quick restart point 2 ", vSpawnLoc)
			BREAK
			CASE 3
				iRandomHeadingToSelect = 3
				vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vRestartPos[2]
				iInterior = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRestartInterior[2]
				PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_LOCATION - RANDOM -Spawning at quick restart point 3 ", vSpawnLoc)
			BREAK
			CASE 4
				iRandomHeadingToSelect = 4
				vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vRestartPos[3]
				iInterior = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRestartInterior[3]
				PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_LOCATION - RANDOM -Spawning at quick restart point 4 ", vSpawnLoc)
			BREAK
		ENDSWITCH
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_DYNAMIC_FACILITY_SPAWN_OVERRIDE)
			PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_LOCATION - Using Facility Veh Spawn Override..")
			PLAYER_INDEX piPlayerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
			DEFUNCT_BASE_ID eDefunctBaseID = GET_PLAYERS_OWNED_DEFUNCT_BASE(piPlayerBoss)
			
			IF eDefunctBaseID = DEFUNCT_BASE_ID_INVALID
				eDefunctBaseID = GET_DEFUNCT_BASE_ID_FROM_SIMPLE_INTERIOR_ID(g_TransitionSessionNonResetVars.eSMPLinteriorHeistID)
			ENDIF
			
			#IF IS_DEBUG_BUILD			
			PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_LOCATION - Boss PlayerNum: ", NATIVE_TO_INT(piPlayerBoss))
			PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_LOCATION - Facility Int: ", ENUM_TO_INT(eDefunctBaseID))
			IF ENUM_TO_INT(eDefunctBaseID) = 0
				PRINTLN("[LM] - MC_GET_VEH_SPAWN_LOCATION ciFMMC_VEHICLE7_DYNAMIC_FACILITY_SPAWN_OVERRIDE wont work - eDefunctBaseID is invalid. This is because we are not spawning at our bosses facility.")
			ENDIF
			#ENDIF
		
			INT iSlot = 0		
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn != BALLER5
				iSlot = 1
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn != STROMBERG
				IF NOT IS_VECTOR_ZERO(GET_HEIST_FACILITY_SPAWN_LOCATION_PLACED(eDefunctBaseID, TRUE, iSlot))
					vSpawnLoc = GET_HEIST_FACILITY_SPAWN_LOCATION_PLACED(eDefunctBaseID, TRUE, iSlot)
					PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_LOCATION - Successfully using override 1")
				ELSE
					PRINTLN("[LM] - Something went wrong with the placed vehicle override spawns for facilities. Likely too many bits set. Ran out of COORDS...")
				ENDIF
			ELSE
				IF NOT IS_VECTOR_ZERO(GET_HEIST_FACILITY_SPAWN_LOCATION(eDefunctBaseID, TRUE, MC_ServerBD_4.iFacilityPosVehSpawned))
					vSpawnLoc = GET_HEIST_FACILITY_SPAWN_LOCATION(eDefunctBaseID, TRUE, MC_ServerBD_4.iFacilityPosVehSpawned)
					PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_LOCATION (stromberg) - Successfully using override 2")
				ELSE
					PRINTLN("[LM] - (stromberg) Something went wrong with the placed vehicle override spawns for facilities. Likely too many bits set. Ran out of COORDS...")
				ENDIF
			ENDIF
			
			iInterior = -1
			
			PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_LOCATION - Spawn Location.x: ", vSpawnLoc.x, " vSpawnLoc.y: ", vSpawnLoc.y, " vSpawnLoc.z: ", vSpawnLoc.z)
		ENDIF
		
		#IF FEATURE_CASINO_HEIST
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_UseDynamicArcadeSpawnLoc)
			PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_LOCATION - Using Arcade Property Veh Spawn Override - Getting Vector")
			PLAYER_INDEX piPlayerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
			SIMPLE_INTERIORS eArcadeID = GET_OWNED_ARCADE_SIMPLE_INTERIOR(piPlayerBoss)
			
			IF IS_SIMPLE_INTERIOR_OF_TYPE(eArcadeID, SIMPLE_INTERIOR_TYPE_ARCADE)
				VECTOR vArcadeSpawnPos = GET_ARCADE_PLACED_VEHICLE_SPAWN(eArcadeID)
				IF NOT IS_VECTOR_ZERO(vArcadeSpawnPos)
					vSpawnLoc = vArcadeSpawnPos
					PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_LOCATION - Successfully using Arcade position - ", vSpawnLoc)
				ELSE
					PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_LOCATION - Failed to get Arcade Property spawn pos.")
				ENDIF
			ENDIF
			
			iInterior = -1
			
		ENDIF
		#ENDIF
		
		IF IS_THIS_A_QUICK_RESTART_JOB() 
		OR IS_CORONA_INITIALISING_A_QUICK_RESTART()
			IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vRestartPos[3])
					vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vRestartPos[3]
					iInterior = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRestartInterior[3]
					PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_LOCATION - on a quick restart and veh ", iVeh, " has a restart vector = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vRestartPos[3])
				ENDIF
			ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2)
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vRestartPos[2])
					vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vRestartPos[2]
					iInterior = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRestartInterior[2]
					PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_LOCATION - on a quick restart and veh ", iVeh, " has a restart vector = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vRestartPos[2])
				ENDIF
			ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vRestartPos[1])
					vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vRestartPos[1]
					iInterior = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRestartInterior[1]
					PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_LOCATION - on a quick restart and veh ", iVeh, " has a restart vector = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vRestartPos[1])
				ENDIF
			ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vRestartPos[0])
					vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vRestartPos[0]
					iInterior = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRestartInterior[0]					
					PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_LOCATION - on a quick restart and veh ", iVeh, " has a restart vector = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vRestartPos[0])
				ENDIF
			ENDIF
		ENDIF
		
		IF REPOSITION_COORD_BASED_ON_FORCED_WARP_LOCATION_INDEX_ON_RESPAWN(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings, CREATION_TYPE_VEHICLES, bIsInitialSpawn, vSpawnLoc, iVeh)
	
		ELIF REPOSITION_COORD_BASED_ON_WARP_LOCATIONS_RANDOM_POOL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings, CREATION_TYPE_VEHICLES, bIsInitialSpawn, vSpawnLoc #IF IS_DEBUG_BUILD , iVeh #ENDIF )
			// Don't do ambush stuff below if this is true.
			
		ELIF IS_ENTITY_USING_THIS_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].eSpawnConditionFlag, SPAWN_CONDITION_FLAG_AMBUSH_ABILITY_ACTIVATED)
		AND sLocalPlayerAbilities.sAmbushData.iWarpPositionToUse > -1
			vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings.vPosition[sLocalPlayerAbilities.sAmbushData.iWarpPositionToUse]
			PRINTLN("[RCC MISSION][Vehicles][Vehicle ", iVeh, "][PlayerAbilities][AmbushAbility] MC_GET_VEH_SPAWN_LOCATION - Ambush vehicle using coords from Warp Location ", sLocalPlayerAbilities.sAmbushData.iWarpPositionToUse)
		ENDIF
	ENDIF
	
	ASSIGN_CHECKPOINT_POSITION_FROM_WARP_LOCATION_SETTINGS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings, vSpawnLoc #IF IS_DEBUG_BUILD , CREATION_TYPE_VEHICLES, iVeh #ENDIF )	
	
	REPOSITION_ENTITY_RESPAWN_POSITION_FROM_WARP_LOCATION_JUST_USED(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings, bIsInitialSpawn, CREATION_TYPE_VEHICLES, iVeh, vSpawnLoc)
		
	RETURN vSpawnLoc
	
ENDFUNC

FUNC VECTOR MC_GET_VEH_SPAWN_LOCATION_FOR_PROXIMITY_SPAWNING(INT iVeh)
	
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_USE_CHECKPOINTS_FOR_RANDOM_SPAWN)
		ASSERTLN("MC_GET_VEH_SPAWN_LOCATION_FOR_PROXIMITY_SPAWNING - ciFMMC_VEHICLE6_USE_CHECKPOINTS_FOR_RANDOM_SPAWN is set on veh ", iVeh, " this is not compatible")
		PRINTLN("[PROXSPAWN][Veh: ", iVeh, "] MC_GET_VEH_SPAWN_LOCATION_FOR_PROXIMITY_SPAWNING - ciFMMC_VEHICLE6_USE_CHECKPOINTS_FOR_RANDOM_SPAWN is set on veh ", iVeh, " this is not compatible")
		RETURN <<0,0,0>>
	ENDIF
	#ENDIF
	
	INT iTemp, iTemp2
	RETURN MC_GET_VEH_SPAWN_LOCATION(iVeh, iTemp, iTemp2, TRUE)
	
ENDFUNC

FUNC FLOAT MC_GET_VEH_SPAWN_HEADING(INT iVeh, BOOL bSecondarySpawn = FALSE, INT iRandomHeadingToSelect = -1, BOOL bisInitialSpawn = FALSE)
	
	FLOAT fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHead
	
	IF SHOULD_ENTITY_USE_CHECKPOINT_CONTINUITY_VARS(ciENTITY_TYPE_VEHICLE, iVeh)
		FLOAT fEntityCheckpointHeading = GET_HEADING_FOR_ENTITY_CHECKPOINT_CONTINUITY(ciENTITY_TYPE_VEHICLE, iVeh)
		// This vehicle has got a heading saved from the last checkpoint!
		PRINTLN("[CONTINUITY][EntityCheckpointContinuity] - MC_GET_VEH_SPAWN_HEADING - Vehicle ", iVeh, " using saved checkpoint heading ", fEntityCheckpointHeading)
		RETURN fEntityCheckpointHeading
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sTrainAttachmentData.iTrainIndex != -1
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sTrainAttachmentData.vCarriageOffsetPosition)		
		VECTOR vTempPos = <<0.0, 0.0, 0.0>>
		VECTOR vTempRot = <<0.0, 0.0, 0.0>>
		MC_GET_TRAIN_ATTACHMENT_POINT_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sTrainAttachmentData, vTempPos, vTempRot)
		fSpawnHeading = vTempRot.z
		RETURN fSpawnHeading
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_DYNAMIC_FACILITY_SPAWN_OVERRIDE)
		PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_HEADING - Using Facility Veh Spawn Override..")
		PLAYER_INDEX piPlayerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		DEFUNCT_BASE_ID eDefunctBaseID = GET_PLAYERS_OWNED_DEFUNCT_BASE(piPlayerBoss)			
				
		#IF IS_DEBUG_BUILD			
		PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_HEADING - Boss PlayerNum: ", NATIVE_TO_INT(piPlayerBoss))
		PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_HEADING - Facility Int: ", ENUM_TO_INT(eDefunctBaseID))
		#ENDIF
		
		INT iSlot = 0		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn != BALLER5
			iSlot = 1
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn != STROMBERG
			fSpawnHeading = GET_HEIST_FACILITY_SPAWN_HEADING_PLACED(eDefunctBaseID, TRUE, iSlot)
		ELSE
			fSpawnHeading = GET_HEIST_FACILITY_SPAWN_HEADING(eDefunctBaseID, TRUE, MC_ServerBD_4.iFacilityPosVehSpawned)
		ENDIF
		PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_HEADING - Facility Spawn Heading: ", fSpawnHeading)
	ENDIF
	
	#IF FEATURE_CASINO_HEIST
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_UseDynamicArcadeSpawnLoc)
		PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_LOCATION - Using Arcade Property Veh Spawn Override - Getting Heading")
		PLAYER_INDEX piPlayerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		SIMPLE_INTERIORS eArcadeID = GET_OWNED_ARCADE_SIMPLE_INTERIOR(piPlayerBoss)
		
		IF IS_SIMPLE_INTERIOR_OF_TYPE(eArcadeID, SIMPLE_INTERIOR_TYPE_ARCADE)
			fSpawnHeading = GET_ARCADE_PLACED_VEHICLE_HEADING(eArcadeID)
		ENDIF
		PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_HEADING - Arcade Property Spawn Heading: ", fSpawnHeading)
	ENDIF
	#ENDIF
	
	IF bSecondarySpawn
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vSecondSpawnPos)
		fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fSecondHeading
	ENDIF
	
	IF(IS_THIS_A_QUICK_RESTART_JOB() 
	OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
		IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vRestartPos[3])
				fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fRestartHeading[3]
				PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_HEADING - on a quick restart (checkpoint 4) and veh ", iVeh, " has a restart heading = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fRestartHeading[3])
			ENDIF
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2)
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vRestartPos[2])
				fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fRestartHeading[2]
				PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_HEADING - on a quick restart (checkpoint 3) and veh ", iVeh, " has a restart heading = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fRestartHeading[2])
			ENDIF
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vRestartPos[1])
				fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fRestartHeading[1]
				PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_HEADING - on a quick restart (checkpoint 2) and veh ", iVeh, " has a restart heading = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fRestartHeading[1])
			ENDIF
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vRestartPos[0])
				fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fRestartHeading[0]
				PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_HEADING - on a quick restart (checkpoint 1) and veh ", iVeh, " has a restart heading = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fRestartHeading[0])
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_USE_CHECKPOINTS_FOR_RANDOM_SPAWN)
		PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_HEADING - RANDOM - Picking random spawn!")
		SWITCH iRandomHeadingToSelect
			CASE 0
				fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHead
				PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_HEADING - RANDOM - Spawning at placed location ", fSpawnHeading)
			BREAK
			CASE 1
				fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fRestartHeading[0]
				PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_HEADING - RANDOM - Spawning at quick restart point 1 ", fSpawnHeading)
			BREAK
			CASE 2
				fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fRestartHeading[1]
				PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_HEADING - RANDOM - Spawning at quick restart point 2 ", fSpawnHeading)
			BREAK
			CASE 3
				fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fRestartHeading[2]
				PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_HEADING - RANDOM - Spawning at quick restart point 3 ", fSpawnHeading)
			BREAK
			CASE 4
				fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fRestartHeading[3]
				PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_HEADING - RANDOM - Spawning at quick restart point 4 ", fSpawnHeading)
			BREAK
			DEFAULT
				PRINTLN("[RCC MISSION] MC_GET_VEH_SPAWN_HEADING - RANDOM - Not Using this.")
			BREAK
		ENDSWITCH	
	ENDIF
	
	IF REPOSITION_HEADING_BASED_ON_FORCED_WARP_LOCATION_INDEX_ON_RESPAWN(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings, CREATION_TYPE_VEHICLES, bisInitialSpawn, fSpawnHeading, iVeh)
	
	ELIF REPOSITION_HEADING_BASED_ON_WARP_LOCATIONS_RANDOM_POOL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings, CREATION_TYPE_VEHICLES, bisInitialSpawn, fSpawnHeading #IF IS_DEBUG_BUILD , iVeh #ENDIF )
		// Don't do ambush below if this is true.
		
	ELIF IS_ENTITY_USING_THIS_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].eSpawnConditionFlag, SPAWN_CONDITION_FLAG_AMBUSH_ABILITY_ACTIVATED)
	AND sLocalPlayerAbilities.sAmbushData.iWarpPositionToUse > -1
		fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings.fHeading[sLocalPlayerAbilities.sAmbushData.iWarpPositionToUse]
		PRINTLN("[RCC MISSION][Vehicles][Vehicle ", iVeh, "][PlayerAbilities][AmbushAbility] MC_GET_VEH_SPAWN_HEADING - Ambush vehicle using heading from Warp Location ", sLocalPlayerAbilities.sAmbushData.iWarpPositionToUse)
		
	ENDIF
	
	ASSIGN_CHECKPOINT_HEADING_FROM_WARP_LOCATION_SETTINGS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings, fSpawnHeading #IF IS_DEBUG_BUILD , CREATION_TYPE_VEHICLES, iVeh #ENDIF )	
	
	REPOSITION_ENTITY_RESPAWN_HEADING_FROM_WARP_LOCATION_JUST_USED(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings, bIsInitialSpawn, CREATION_TYPE_VEHICLES, iVeh, fSpawnHeading)		
		
	RETURN fSpawnHeading
	
ENDFUNC

FUNC BOOL MC_IS_VEHICLE_A_DROP_OFF_VEHICLE(INT iVeh, BOOL& bHookDropOff)
	
	BOOL bDropOff = FALSE
	
	INT iTeam
	
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		
		INT iRule
		
		FOR iRule = 0 TO (FMMC_MAX_RULES - 1)
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_VEHICLE
			AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iRule] = iVeh
				
				bDropOff = TRUE
				
				IF (NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehDropOff_GetInBS, iRule))
				AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehDropOff_HookUpBS, iRule)
					bHookDropOff = TRUE
					//Break out, no more information is needed:
					iRule = FMMC_MAX_RULES
					iTeam = FMMC_MAX_TEAMS
				ENDIF
				
			ENDIF
		ENDFOR
		
	ENDFOR
	
	RETURN bDropOff
	
ENDFUNC

FUNC BOOL MC_DOES_VEHICLE_RULE_REQUIRE_A_KILL(INT iRule, INT iTeam, INT iPriority)
	
	SWITCH iRule
		CASE FMMC_OBJECTIVE_LOGIC_KILL
			RETURN TRUE
		
		CASE FMMC_OBJECTIVE_LOGIC_CAPTURE
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iPriority], ciBS_RULE11_SECUROHACK_REQUIRES_KILL)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

//With a Team of -1, this will check through all teams:
FUNC BOOL MC_IS_VEHICLE_MISSION_KILL(INT iVeh, INT iTeam = -1)
	
	BOOL bKillVeh = FALSE
	
	IF iTeam = -1
		
		INT iTeamLoop
		
		FOR iTeamLoop = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams - 1)
			
			//Recursion!
			IF MC_IS_VEHICLE_MISSION_KILL(iVeh, iTeamLoop)
				bKillVeh = TRUE
				iTeamLoop = FMMC_MAX_TEAMS//Break out
			ENDIF
			
		ENDFOR
		
	ELSE
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iPriority[iTeam] < FMMC_MAX_RULES
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
			IF MC_DOES_VEHICLE_RULE_REQUIRE_A_KILL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRule[iTeam], iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iPriority[iTeam])
				bKillVeh = TRUE
				iTeam = FMMC_MAX_TEAMS //Break out, we've found a team with this on a kill rule!
			ELSE
				//Check extra objectives:
				INT iextraObjectiveNum
				// Loop through all potential entites that have multiple rules
				FOR iextraObjectiveNum = 0 TO (MAX_NUM_EXTRA_OBJECTIVE_ENTITIES-1)
					// If the type matches the one passed
					IF g_FMMC_STRUCT.iExtraObjectiveEntityType[iextraObjectiveNum] = ciRULE_TYPE_VEHICLE
					AND g_FMMC_STRUCT.iExtraObjectiveEntityId[iextraObjectiveNum] = iVeh
						INT iextraObjectiveLoop
						
						FOR iextraObjectiveLoop = 0 TO (MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY - 1)
							IF g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam] < FMMC_MAX_RULES
							AND (GET_FMMC_RULE_FROM_CREATOR_RULE(g_FMMC_STRUCT.iExtraObjectiveRule[iextraObjectiveNum][iextraObjectiveLoop][iTeam]) != FMMC_OBJECTIVE_LOGIC_NONE)
								
								IF MC_DOES_VEHICLE_RULE_REQUIRE_A_KILL(GET_FMMC_RULE_FROM_CREATOR_RULE(g_FMMC_STRUCT.iExtraObjectiveRule[iextraObjectiveNum][iextraObjectiveLoop][iTeam]), iTeam, g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam])
									bKillVeh = TRUE
									iextraObjectiveLoop = MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY //Break out - we've found a kill rule
								ENDIF
								
							ELSE
								iextraObjectiveLoop = MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY //Break out - we've reached the number of extra rules
							ENDIF
						ENDFOR
						//We found the multi-rule number for this entity, there won't be any more so break out of the loop:
						iextraObjectiveNum = MAX_NUM_EXTRA_OBJECTIVE_ENTITIES
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
		
	ENDIF
	
	RETURN bKillVeh

ENDFUNC

PROC MC_BURST_VEHICLE_TYRES(VEHICLE_INDEX tempVeh)
	MODEL_NAMES mn = GET_ENTITY_MODEL(tempVeh)
	IF NOT IS_THIS_MODEL_A_BOAT(mn) AND NOT IS_THIS_MODEL_A_PLANE(mn) AND NOT IS_THIS_MODEL_A_HELI(mn) IS_THIS_MODEL_A_JETSKI(mn)
		IF NOT IS_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_FRONT_LEFT)
			SET_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_FRONT_LEFT, TRUE)
		ENDIF
		IF NOT IS_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_FRONT_RIGHT)
			SET_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_FRONT_RIGHT, TRUE)
		ENDIF
		IF NOT IS_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_MID_LEFT)
			SET_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_MID_LEFT, TRUE)
		ENDIF
		IF NOT IS_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_MID_RIGHT)
			SET_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_MID_RIGHT, TRUE)
		ENDIF
		IF NOT IS_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_REAR_LEFT)
			SET_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_REAR_LEFT, TRUE)
		ENDIF
		IF NOT IS_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_REAR_RIGHT)
			SET_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_REAR_RIGHT, TRUE)
		ENDIF
		IF NOT IS_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_BIKE_FRONT)
			SET_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_BIKE_FRONT, TRUE)
		ENDIF
		IF NOT IS_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_BIKE_REAR)
			SET_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_BIKE_REAR, TRUE)
		ENDIF
	ENDIF
	PRINTLN("[JR][BTOS]Popping tyres because ciFMMC_VEHICLE7_BURST_TYRES_ON_SPAWN is set to TRUE.")
ENDPROC

FUNC BOOL SHOULD_VEHICLE_CLEANUP_BE_DELAYED(INT iVeh)
	// Reusing Spawn Delay to save on Broadcast Data.
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupDelay > 0
		IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_3.iVehSpawnDelay[iVeh])
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] SHOULD_CLEANUP_VEH - Starting Delay.")
			MC_REINIT_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_3.iVehSpawnDelay[iVeh])
		ENDIF
		
		IF NOT MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(MC_serverBD_3.iVehSpawnDelay[iVeh], g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupDelay)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] SHOULD_CLEANUP_VEH - Waiting for delay to expire. Miliseconds: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_3.iVehSpawnDelay[iVeh]), " / ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupDelay)			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_IF_PED_IN_VEHICLE_SHOULD_CLEANUP(INT iPed)
	FMMC_PED_STATE sPedState
	FILL_FMMC_PED_STATE_STRUCT(sPedState, iPed)
	IF sPedState.bExists
		IF NOT SHOULD_CLEANUP_PED(sPedState)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CACHE_VEH_SHOULD_CLEANUP_THIS_FRAME(INT i)
	SET_BIT(iVehShouldCleanupThisFrameBS, i)
ENDPROC

PROC CACHE_VEH_SHOULD_NOT_CLEANUP_THIS_FRAME(INT i)
	SET_BIT(iVehShouldNotCleanupThisFrameBS, i)
ENDPROC

FUNC BOOL SHOULD_CLEANUP_VEH(FMMC_VEHICLE_STATE &sVehState)
	
	INT iVeh = sVehState.iIndex
	
	IF IS_BIT_SET(iVehShouldCleanupThisFrameBS, iVeh)
		RETURN TRUE
	ENDIF
	IF IS_BIT_SET(iVehShouldNotCleanupThisFrameBS, iVeh)
		RETURN FALSE
	ENDIF
	
	IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTwo, ciFMMC_VEHICLE3_CleanupAtMissionEnd)
		PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_CLEANUP_VEH - Cleaning up as game state = GAME_STATE_END")
		CACHE_VEH_SHOULD_CLEANUP_THIS_FRAME(iVeh)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iAmbientOverrideVehicle, iVeh)
		CACHE_VEH_SHOULD_NOT_CLEANUP_THIS_FRAME(iVeh)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupDetachedTrailerOnRule != -1
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupDetachedTrailerOnRule
			IF NOT IS_BIT_SET(bsCleanupVehicleRule, iVeh)
				IF NOT IS_ENTITY_ATTACHED(sVehState.vehIndex)
					PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_CLEANUP_VEH - Model = ", FMMC_GET_MODEL_NAME_FOR_DEBUG(sVehState.mn), " | iCleanupDetachedTrailerOnRule ... ", MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam], " >= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupDetachedTrailerOnRule)
					
					SET_BIT(bsCleanupVehicleRule, iVeh)
					
					CACHE_VEH_SHOULD_CLEANUP_THIS_FRAME(iVeh)
					RETURN TRUE
				ELSE
					PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_CLEANUP_VEH - Model = ", FMMC_GET_MODEL_NAME_FOR_DEBUG(sVehState.mn), " | iCleanupDetachedTrailerOnRule ... Attached!")
					
					SET_BIT(bsCleanupVehicleRule, iVeh)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetNine, ciFMMC_VEHICLE9_CleanupWithDriver)
		IF DOES_ENTITY_EXIST(sVehState.vehIndex)
			PED_INDEX pedInVeh = GET_PED_IN_VEHICLE_SEAT(sVehState.vehIndex, VS_DRIVER)			
			IF DOES_ENTITY_EXIST(pedInVeh)
			AND NOT IS_PED_INJURED(pedInVeh)
				INT iPed = IS_ENTITY_A_MISSION_CREATOR_ENTITY(pedInVeh)
				IF iPed != -1
					IF NOT CHECK_IF_PED_IN_VEHICLE_SHOULD_CLEANUP(iPed)
						CACHE_VEH_SHOULD_CLEANUP_THIS_FRAME(iVeh)
						RETURN TRUE
					ELSE
						CACHE_VEH_SHOULD_NOT_CLEANUP_THIS_FRAME(iVeh)
						RETURN FALSE
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupVehicleDistance > 0
		IF CHECK_ALL_PLAYERS_EXCEED_DISTANCE_FROM_COORDS(GET_FMMC_VEHICLE_COORDS(sVehState), g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupVehicleDistance)
			IF IS_BIT_SET(bsCleanupVehicleDistance, iVeh)
				PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_CLEANUP_VEH - Model = ", FMMC_GET_MODEL_NAME_FOR_DEBUG(sVehState.mn), " | iCleanupVehicleDistance ... CHECK_ALL_PLAYERS_EXCEED_DISTANCE_FROM_COORDS = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupVehicleDistance)
				CACHE_VEH_SHOULD_CLEANUP_THIS_FRAME(iVeh)
				RETURN TRUE
			ENDIF
		ELIF NOT CHECK_ALL_PLAYERS_EXCEED_DISTANCE_FROM_COORDS(GET_FMMC_VEHICLE_COORDS(sVehState), ROUND((g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupVehicleDistance / 4.0) * 3.0))	//75% of the distance to create a theshold to ensure it won't trigger incorrectly
			IF NOT IS_BIT_SET(bsCleanupVehicleDistance, iVeh)
				PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_CLEANUP_VEH - Model = ", FMMC_GET_MODEL_NAME_FOR_DEBUG(sVehState.mn), " | iCleanupVehicleDistance ... SET_BIT bsCleanupVehicleDistance")
				
				SET_BIT(bsCleanupVehicleDistance, iVeh)
			ENDIF
		ENDIF
	ENDIF
		
	//Despawn options
	IF MC_WAS_DESPAWN_CONDITION_REACHED(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sCleanupData, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAggroIndexBS_Entity_Veh #IF IS_DEBUG_BUILD ,"vehicle" #ENDIF )
		IF NOT SHOULD_VEHICLE_CLEANUP_BE_DELAYED(sVehState.iIndex)
			PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_CLEANUP_VEH - Cleanup vehicle via MC_WAS_DESPAWN_CONDITION_REACHED")
			CACHE_VEH_SHOULD_CLEANUP_THIS_FRAME(iVeh)
			RETURN TRUE
		ELSE
			PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_CLEANUP_VEH - Cleanup vehicle via MC_WAS_DESPAWN_CONDITION_REACHED - Waiting for Delay to finish.")
		ENDIF
	ENDIF
	
	INT iTeamToCheckCleanUp = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupTeam
	INT iRuleToCleanUpOn = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupObjective
	BOOL bCheckEarlyCleanup

	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupObjective != -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupTeam != -1
			IF iRuleToCleanUpOn <= MC_serverBD_4.iCurrentHighestPriority[iTeamToCheckCleanUp]
				bCheckEarlyCleanup = TRUE
			ENDIF
		ENDIF
	ENDIF
		
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iVeh, eSGET_Vehicle)
		PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_CLEANUP_VEH - Veh no longer has a valid spawn group active.") 
		bCheckEarlyCleanup = TRUE
	ENDIF	
	
	IF !bCheckEarlyCleanup
		CACHE_VEH_SHOULD_NOT_CLEANUP_THIS_FRAME(iVeh)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_VEHICLE_CLEANUP_BE_DELAYED(sVehState.iIndex)
		CACHE_VEH_SHOULD_NOT_CLEANUP_THIS_FRAME(iVeh)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTwo, ciFMMC_VEHICLE2_CleanupAtMidpoint)
		IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeamToCheckCleanUp], iRuleToCleanUpOn)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupVehicle != -1
				PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_CLEANUP_VEH - Cleanup Vehicle = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupVehicle)
				VEHICLE_INDEX viTempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupVehicle])
				IF NOT IS_VEHICLE_FUCKED_MP(viTempVeh)
					PRINTLN("[MMacK][CleanupVeh] not fucked")
					IF IS_PED_A_PLAYER(GET_PED_IN_VEHICLE_SEAT(viTempVeh)) 
						PRINTLN("[MMacK][CleanupVeh] a player is driving")
						IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupRange <= 0
							PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_CLEANUP_VEH - Midpoint Cleaning up in vehicle | iCleanupRange <= 0")
							CACHE_VEH_SHOULD_CLEANUP_THIS_FRAME(iVeh)
							RETURN TRUE
						ELSE
							INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(sVehState.vehIndex)
							PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
							PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
							IF sVehState.vehIndex != NULL
								IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed, sVehState.vehIndex) > g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupRange
									PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_CLEANUP_VEH - Midpoint Distance check cleanup range 1")
									CACHE_VEH_SHOULD_CLEANUP_THIS_FRAME(iVeh)
									RETURN TRUE
								ENDIF
							ELSE
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vPos) > g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupRange
									PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_CLEANUP_VEH - Midpoint Distance check cleanup range 2")
									CACHE_VEH_SHOULD_CLEANUP_THIS_FRAME(iVeh)
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupRange <= 0
				PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_CLEANUP_VEH - Midpoint base cleanup range <= 0")
				CACHE_VEH_SHOULD_CLEANUP_THIS_FRAME(iVeh)
				RETURN TRUE
			ELSE
				INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(sVehState.vehIndex)
				PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
				PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
				IF sVehState.vehIndex != NULL
					IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed, sVehState.vehIndex) > g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupRange
						PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_CLEANUP_VEH - Midpoint Else 1")
						CACHE_VEH_SHOULD_CLEANUP_THIS_FRAME(iVeh)
						RETURN TRUE
					ENDIF
				ELSE
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vPos) > g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupRange
						PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_CLEANUP_VEH - Midpoint Else 2")
						CACHE_VEH_SHOULD_CLEANUP_THIS_FRAME(iVeh)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupRange <= 0
			PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_CLEANUP_VEH - Cleanup range <= 0")
			CACHE_VEH_SHOULD_CLEANUP_THIS_FRAME(iVeh)
			RETURN TRUE
		ELSE
			INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(sVehState.vehIndex)
			PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
			PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
			IF sVehState.vehIndex != NULL
				IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed, sVehState.vehIndex) > g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupRange
					PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_CLEANUP_VEH - Distance check 1")
					CACHE_VEH_SHOULD_CLEANUP_THIS_FRAME(iVeh)
					RETURN TRUE
				ENDIF
			ELSE
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vPos) > g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupRange
					PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_CLEANUP_VEH - Distance check 2")
					CACHE_VEH_SHOULD_CLEANUP_THIS_FRAME(iVeh)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	CACHE_VEH_SHOULD_NOT_CLEANUP_THIS_FRAME(iVeh)
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Cleans up a vehicle mid mission
///    You should check if the vehicle needs to be cleaned up before calling this function! Before the 2020 split this fucntion included a check for SHOULD_CLEANUP_VEH()
/// PARAMS:
///    sPedState - Cached ped state of the ped to cleanup
///    bFinalCleanup - If the ped is to be fully cleaned up so they do not spawn again on the mission
/// RETURNS:
///    TRUE when the ped is deleted or set as no longer needed
FUNC BOOL CLEANUP_VEH_EARLY(FMMC_VEHICLE_STATE &sVehState, BOOL bFinalCleanup = TRUE)
		
	INT iVeh = sVehState.iIndex
		
	// Do not visibly clean up, set as no longer needed
	BOOL bSetAsNoLongerNeeded = NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet, ciFMMC_VEHICLE_IgnoreVisCheckCleanup)
	PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] CLEANUP_VEH_EARLY - Set as no longer needed (creator): ", BOOL_TO_STRING(bSetAsNoLongerNeeded))
	
	IF !bSetAsNoLongerNeeded
	AND sVehState.bExists
	
		//Check that any peds in the vehicle are going to clean up too before deleting it (if not, then only mark it as no longer needed / clean it up)
		BOOL bAllPassengersWantToCleanup = TRUE
		INT iSeat
		INT iPassengerSeats = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(sVehState.vehIndex)
		PED_INDEX pedInVeh
		INT iPed
		
		FOR iSeat = -1 TO iPassengerSeats - 1 // All VEHICLE_SEATs for this car apart from VS_ANY_PASSENGER
			pedInVeh = GET_PED_IN_VEHICLE_SEAT(sVehState.vehIndex, INT_TO_ENUM(VEHICLE_SEAT, iSeat))
			
			IF IS_PED_INJURED(pedInVeh)
				RELOOP
			ENDIF
			
			//Check for players being in passenger seats B*2175272
			IF IS_PED_A_PLAYER(pedInVeh)
				bAllPassengersWantToCleanup = FALSE
				BREAKLOOP
			ELSE
				iPed = IS_ENTITY_A_MISSION_CREATOR_ENTITY(pedInVeh)
				IF iPed != -1
					IF CHECK_IF_PED_IN_VEHICLE_SHOULD_CLEANUP(iPed)
						bAllPassengersWantToCleanup = FALSE
						BREAKLOOP
					ENDIF
				ENDIF	
			ENDIF
		ENDFOR
		
		//Only set as no longer needed if at least one of the peds inside doesn't want to clean up
		bSetAsNoLongerNeeded = !bAllPassengersWantToCleanup
		PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] CLEANUP_VEH_EARLY - Set as no longer needed (passengers): ", BOOL_TO_STRING(bSetAsNoLongerNeeded))
	
	ENDIF
	
	IF bSetAsNoLongerNeeded
	
		INT iTeam
		FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
			CLEAR_BIT(MC_serverBD.iVehteamFailBitset[iVeh], iTeam)
			CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iVeh)
		ENDFOR
		
		IF bFinalCleanup
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] CLEANUP_VEH_EARLY - Setting respawns to 0")
			SET_VEHICLE_RESPAWNS(iVeh, 0)
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD.iVehCleanup_NeedOwnershipBS, iVeh)
			CLEAR_BIT(MC_serverBD.iVehCleanup_NeedOwnershipBS, iVeh)
		ENDIF
		
		SET_BIT(MC_ServerBD.iVehAboutToCleanup_BS, iVeh)
		
		IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRespawnOnRule, -1)
		AND NOT IS_BIT_SET(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, iVeh)
			PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] CLEANUP_VEH_EARLY - Setting iServerBS_PassRuleVehAfterRespawn")
			SET_BIT(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, iVeh)
		ENDIF
		
		PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] CLEANUP_VEH_EARLY - (No longer needed) Objective: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupObjective, " Team: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupTeam)
		CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
		
		CLEANUP_VEHICLE_CARGO(iVeh)
		
		RETURN TRUE
		
	ENDIF

	IF NOT sVehState.bHasControl
		PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] CLEANUP_VEH_EARLY - Requesting control of vehicle for deletion")
		NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sVehState.niIndex)
		SET_BIT(MC_serverBD.iVehCleanup_NeedOwnershipBS, iVeh)
	ELSE
		START_ENTITY_PTFX_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sEntityDespawnPTFX, GET_FMMC_VEHICLE_COORDS(sVehState), GET_ENTITY_ROTATION(sVehState.vehIndex))
		DELETE_FMMC_VEHICLE(iVeh, bFinalCleanup)
		RETURN TRUE
	ENDIF
				
	RETURN FALSE

ENDFUNC

PROC CACHE_VEH_RESPAWN_BLOCKED(INT i)
	IF NOT IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iMiscBS, ciRandomSpawnGroupBS_HasBeenSet)
		EXIT
	ENDIF	
	SET_BIT(iVehRespawnIsBlockedBS, i)	
	PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] SHOULD_VEH_RESPAWN_NOW - CACHE_VEH_RESPAWN_BLOCKED - Caching that the Veh has been blocked from respawning.")
ENDPROC

PROC CACHE_VEH_SHOULD_NOT_RESPAWN_THIS_FRAME(INT i)
	SET_BIT(iVehRespawnIsBlockedThisFrameBS, i)
ENDPROC

PROC CACHE_VEH_SHOULD_RESPAWN_THIS_FRAME(INT i)
	SET_BIT(iVehShouldRespawnNowBS, i)
ENDPROC

FUNC BOOL SHOULD_VEH_RESPAWN_NOW(FMMC_VEHICLE_STATE &sVehState)

	INT i = sVehState.iIndex
	
	IF IS_BIT_SET(iVehRespawnIsBlockedBS, i)
		RETURN FALSE
	ENDIF	
	IF IS_BIT_SET(iVehRespawnIsBlockedThisFrameBS, i)		
		RETURN FALSE
	ENDIF	
	IF IS_BIT_SET(iVehShouldRespawnNowBS, i)
		RETURN TRUE
	ENDIF	
		
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] SHOULD_VEH_RESPAWN_NOW - SBBOOL_MISSION_OVER")
		CACHE_VEH_RESPAWN_BLOCKED(i)
		RETURN FALSE
	ENDIF
		
	IF MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].eSpawnConditionFlag, FALSE, FALSE, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAggroIndexBS_Entity_Veh)
		IF MC_ARE_SPAWN_CONDITION_FLAGS_MET_DURING_GAMEPLAY(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].eSpawnConditionFlag)
			CACHE_VEH_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
		ELSE
			CACHE_VEH_RESPAWN_BLOCKED(i)
		ENDIF
	
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] SHOULD_VEH_RESPAWN_NOW - Returning FALSE due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")
		RETURN FALSE
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_VehicleDestroyedTracking)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iContinuityId > -1
		AND IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iVehicleDestroyedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iContinuityId)
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] SHOULD_VEH_RESPAWN_NOW - [JS][CONTINUITY] with continuity ID ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iContinuityId, " not spawning due to death of previous stand mission")
			CACHE_VEH_RESPAWN_BLOCKED(i)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF SHOULD_CLEANUP_VEH(sVehState)
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] SHOULD_VEH_RESPAWN_NOW - Should be cleaning up")
		CACHE_VEH_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
		RETURN FALSE
	ENDIF
		
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sTrainAttachmentData.iTrainIndex != -1
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sTrainAttachmentData.vCarriageOffsetPosition)
		IF NOT IS_TRAIN_ATTACHMENT_READY(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sTrainAttachmentData)
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] SHOULD_VEH_RESPAWN_NOW - RETURN FALSE, Waiting for Train: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sTrainAttachmentData.iTrainIndex, " to be ready.")
			CACHE_VEH_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetSeven, ciFMMC_VEHICLE7_SPAWN_WITH_LINKED_VEH)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iLinkedDestroyVeh  != -1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = TRAILERSMALL2
				IF NOT IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iLinkedDestroyVeh])
					PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] SHOULD_VEH_RESPAWN_NOW - Waiting for linked vehicle")
					CACHE_VEH_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetSeven, ciFMMC_VEHICLE7_ONLY_RESPAWN_WITH_PEDS)
	AND IS_BIT_SET(MC_serverBD_2.iVehSpawnedOnce, i) // Vehicle has already spawned once
		INT iped
		FOR iPed = 0 TO (MC_serverBD.iNumPedCreated  - 1)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iVehicle = i
			AND SHOULD_PED_SPAWN_IN_VEHICLE(iPed)
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
					PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] SHOULD_VEH_RESPAWN_NOW - RETURNING FALSE - Linked ped: ", iPed, " is still alive. Waiting for them to be dead.")
					CACHE_VEH_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF DOES_VEH_HAVE_PLAYER_VARIABLE_SETTING(i)
		IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedObjective > -1
			AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedTeam > -1
				IF MC_serverBD_1.sCreatedCount.iNumVehCreated[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedObjective] >= MC_serverBD.iNumberOfPart[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedTeam]
					SET_BIT(MC_serverBD_1.sCreatedCount.iSkipVehBitset, i)
					PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] SHOULD_VEH_RESPAWN_NOW - RETURNING FALSE - DOES_VEH_HAVE_PLAYER_VARIABLE_SETTING")
					CACHE_VEH_RESPAWN_BLOCKED(i)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PLAYER_PROXIMITY_BLOCKING_SPAWN(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sProximitySpawningData, i, MC_serverBD_3.iProximitySpawning_VehSpawnAllBS, MC_serverBD_3.iProximitySpawning_VehSpawnAnyBS, MC_serverBD_3.iProximitySpawning_VehSpawnOneBS, MC_serverBD_3.iProximitySpawning_VehCleanupBS)
		PRINTLN("[PROXSPAWN_SERVER][MCSpawning][Vehicles][Vehicle ", i, "] SHOULD_VEH_RESPAWN_NOW - IS_PLAYER_PROXIMITY_BLOCKING_SPAWN")
		CACHE_VEH_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
		RETURN FALSE
	ENDIF
	
	BOOL bCheckTeams
	IF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iTeamSpawnReq != -1)
		bCheckTeams = TRUE
	ENDIF
	
	IF NOT MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_VEHICLE(i, bCheckTeams)
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] SHOULD_VEH_RESPAWN_NOW - RETURNING FALSE - MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_VEHICLE")
		CACHE_VEH_RESPAWN_BLOCKED(i)
		RETURN FALSE
	ENDIF
	
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_Vehicle)
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] SHOULD_VEH_RESPAWN_NOW - RETURNING FALSE - Vehicle does not have suitable spawn group")
		CACHE_VEH_RESPAWN_BLOCKED(i)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCSRespawnSceneIndex > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCSRespawnShotIndex > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCSRespawnSceneIndex = MC_serverBD.iSpawnScene
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCSRespawnShotIndex <= MC_serverBD.iSpawnShot
		AND (iCamShotLoaded >= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCSRespawnShotIndex AND IS_BIT_SET(iLocalBoolCheck7, LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED))
			CACHE_VEH_SHOULD_RESPAWN_THIS_FRAME(i)
			RETURN TRUE
		ELSE
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] SHOULD_VEH_RESPAWN_NOW - RETURNING FALSE - iCSRespawnSceneIndex: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCSRespawnSceneIndex, " SpawnScene: ", MC_serverBD.iSpawnScene,
					" iCSRespawnShotIndex: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCSRespawnShotIndex, " SpawnShot: ", MC_serverBD.iSpawnShot, 
					" Camshot: ", iCamShotLoaded, " LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED: ", IS_BIT_SET(iLocalBoolCheck7, LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED))
			CACHE_VEH_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF MC_IS_ENTITY_SPAWN_TRIGGERED_BY_ZONE_ENTRY(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iZoneBlockEntitySpawn)
		IF NOT HAS_ENTITY_SPAWN_ZONE_REQUIREMENT_BEEN_MET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iZoneBlockEntitySpawn, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iZoneBlockEntitySpawnPlayerReq)
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] SHOULD_VEH_RESPAWN_NOW - RETURN FALSE, HAS_ENTITY_SPAWN_ZONE_REQUIREMENT_BEEN_MET = FALSE.. iZone: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iZoneBlockEntitySpawn, " iPlayerReq: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iZoneBlockEntitySpawnPlayerReq)
			CACHE_VEH_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_VEHICLES, i, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedAlwaysForceSpawnOnRule, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAggroIndexBS_Entity_Veh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedObjectiveEnd)
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] SHOULD_VEH_RESPAWN_NOW - RETURNING TRUE - Team has reached entity spawn conditions (1)")
			CACHE_VEH_SHOULD_RESPAWN_THIS_FRAME(i)
			RETURN TRUE
		ENDIF
	ELSE
		CACHE_VEH_SHOULD_RESPAWN_THIS_FRAME(i)
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_VEHICLES, i, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedAlwaysForceSpawnOnRule, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAggroIndexBS_Entity_Veh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedObjectiveEnd)
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] SHOULD_VEH_RESPAWN_NOW - RETURNING TRUE - Team has reached entity spawn conditions (2)")
			CACHE_VEH_SHOULD_RESPAWN_THIS_FRAME(i)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_VEHICLES, i, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedAlwaysForceSpawnOnRule, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAggroIndexBS_Entity_Veh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedObjectiveEnd)
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] SHOULD_VEH_RESPAWN_NOW - RETURNING TRUE - Team has reached entity spawn conditions (3)")
			CACHE_VEH_SHOULD_RESPAWN_THIS_FRAME(i)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_VEHICLES, i, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedAlwaysForceSpawnOnRule, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAggroIndexBS_Entity_Veh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedObjectiveEnd)
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] SHOULD_VEH_RESPAWN_NOW - RETURNING TRUE - Team has reached entity spawn conditions (4)")
			CACHE_VEH_SHOULD_RESPAWN_THIS_FRAME(i)
			RETURN TRUE
		ENDIF
	ENDIF
	
	CACHE_VEH_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
	RETURN FALSE

ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicles
// ##### Description: Process the creation of Mission Controller Peds.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC APPLY_YACHT_COLOURS_TO_VEHICLE(VEHICLE_INDEX viNewVeh, INT iYachtIndex, INT iExtraVehicle = 0)
	
	PRINTLN("[MYACHT] APPLY_YACHT_COLOURS_TO_VEHICLE | Applying yacht colours to ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(viNewVeh)))
	INT Colour1, Colour2, Colour3, Colour4, Colour5, Colour6
	
	IF NOT (gPrivateYachtSpawnVehicleDetails[iExtraVehicle].iLivery = -1)
		SET_VEHICLE_LIVERY(viNewVeh, gPrivateYachtSpawnVehicleDetails[iExtraVehicle].iLivery)
	ENDIF
	
	GET_PRIVATE_YACHT_VEHICLE_CAR_COLOURS(GET_ENTITY_MODEL(viNewVeh), sMissionYachtData[iYachtIndex].Appearance.iTint, Colour1, Colour2, Colour3, Colour4, Colour5, Colour6)
	SET_VEHICLE_COLOURS(viNewVeh, Colour1, Colour2)
	SET_VEHICLE_EXTRA_COLOURS(viNewVeh, Colour3, Colour4)
	SET_VEHICLE_EXTRA_COLOUR_5(viNewVeh, Colour5)
	SET_VEHICLE_EXTRA_COLOUR_6(viNewVeh, Colour6)
ENDPROC

FUNC BOOL SHOULD_VEHICLE_BE_FIRE_PROOF(VEHICLE_INDEX viPassed, INT iVeh)
	
	IF GET_ENTITY_MODEL(viPassed) = AVENGER
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_SPAWN_BURNING)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTen, ciFMMC_VEHICLE10_SetVehicleAsFlameProof)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_VEHICLE_INITIAL_CONTINUITY(INT iVehicle, VEHICLE_INDEX viVehicle)

	IF iVehicle = -1
		EXIT
	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ContentSpecificTracking)
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehicle].iContentContinuityType = 0
		EXIT
	ENDIF
	
	SWITCH g_FMMC_STRUCT.eContinuityContentSpecificType
		CASE FMMC_MISSION_CONTINUITY_CONTENT_SPECIFIC_TYPE_FIXER
			SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehicle].iContentContinuityType
				CASE ciFMMC_MISSION_CONTINUITY_FIXER_ID_DATA_LEAK_2_HELI_TAKEOFF
					IF NOT IS_AUDIO_SCENE_ACTIVE("TAIL_HELICOPTER_SCENE")
						PRINTLN("[CONTINUITY][Vehicles][Vehicle", iVehicle, "] - PROCESS_VEHICLE_INITIAL_CONTINUITY - START_AUDIO_SCENE: TAIL_HELICOPTER_SCENE")
						START_AUDIO_SCENE("TAIL_HELICOPTER_SCENE")
						
						PRINTLN("[CONTINUITY][Vehicles][Vehicle", iVehicle, "] - PROCESS_VEHICLE_INITIAL_CONTINUITY - ADD_ENTITY_TO_AUDIO_MIX_GROUP: TAIL_HELICOPTER_MIXGROUP")
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(viVehicle, "TAIL_HELICOPTER_MIXGROUP")
					ENDIF					
				BREAK
				CASE ciFMMC_MISSION_CONTINUITY_FIXER_ID_SHORT_TRIP_1_MULE
					IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_FIXER_Short_Trip_01_Back_Of_Truck_Scene")
						PRINTLN("[CONTINUITY][Vehicles][Vehicle", iVehicle, "] - PROCESS_VEHICLE_INITIAL_CONTINUITY - START_AUDIO_SCENE: DLC_FIXER_Short_Trip_01_Back_Of_Truck_Scene")
						START_AUDIO_SCENE("DLC_FIXER_Short_Trip_01_Back_Of_Truck_Scene")
						
						PRINTLN("[CONTINUITY][Vehicles][Vehicle", iVehicle, "] - PROCESS_VEHICLE_INITIAL_CONTINUITY - ADD_ENTITY_TO_AUDIO_MIX_GROUP: MP_Scripted_Mixgroup_Focus_Entity")
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(viVehicle, "MP_Scripted_Mixgroup_Focus_Entity")
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_VEHICLE_EVERY_FRAME_CONTINUITY(FMMC_VEHICLE_STATE &sVehState)

	IF sVehState.iIndex = -1
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ContentSpecificTracking)
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iContentContinuityType = 0
		EXIT
	ENDIF
	
	SWITCH g_FMMC_STRUCT.eContinuityContentSpecificType
		CASE FMMC_MISSION_CONTINUITY_CONTENT_SPECIFIC_TYPE_SUMMER_2022
			SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iContentContinuityType
				CASE ciFMMC_MISSION_CONTINUITY_SUMMER_2022_ID_BOMB_VAN
					IF IS_AUDIO_SCENE_ACTIVE("ULP2_Rogue_Drones_Van_Interior_Scene")
						
						IF GET_VEHICLE_DOOR_ANGLE_RATIO(sVehState.vehIndex, SC_DOOR_REAR_LEFT) > 0.05
						OR GET_VEHICLE_DOOR_ANGLE_RATIO(sVehState.vehIndex, SC_DOOR_REAR_RIGHT) > 0.05
						OR NOT IS_VEHICLE_WINDOW_INTACT(sVehState.vehIndex, SC_WINDOW_REAR_LEFT)
						OR NOT IS_VEHICLE_WINDOW_INTACT(sVehState.vehIndex, SC_WINDOW_REAR_RIGHT)
							PRINTLN("[CONTINUITY][Vehicles][Vehicle", sVehState.iIndex, "] - PROCESS_VEHICLE_EVERY_FRAME_CONTINUITY - START_AUDIO_SCENE: ULP2_Rogue_Drones_Van_Interior_Scene")
							STOP_AUDIO_SCENE("ULP2_Rogue_Drones_Van_Interior_Scene")
						ENDIF
						
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMMC_MISSION_CONTINUITY_CONTENT_SPECIFIC_TYPE_WINTER_2022
			SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iContentContinuityType
				CASE ciFMMC_MISSION_CONTINUITY_WINTER_2022_ID_CARGO_PLANE
					PROCESS_CACHE_CARGO_PLANE(sVehState.vehIndex)				
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_VEHICLE_CLEANUP_CONTINUITY(FMMC_VEHICLE_STATE &sVehState)

	IF sVehState.iIndex = -1
		EXIT
	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ContentSpecificTracking)
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iContentContinuityType = 0
		EXIT
	ENDIF
			
	SWITCH g_FMMC_STRUCT.eContinuityContentSpecificType		
		CASE FMMC_MISSION_CONTINUITY_CONTENT_SPECIFIC_TYPE_FIXER
			SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iContentContinuityType
				CASE ciFMMC_MISSION_CONTINUITY_FIXER_ID_DATA_LEAK_2_HELI_TAKEOFF
					IF IS_AUDIO_SCENE_ACTIVE("TAIL_HELICOPTER_SCENE")
						PRINTLN("[CONTINUITY][Vehicles][Vehicle", sVehState.iIndex, "] - PROCESS_VEHICLE_CLEANUP_CONTINUITY - STOP_AUDIO_SCENE: TAIL_HELICOPTER_SCENE")
						STOP_AUDIO_SCENE("TAIL_HELICOPTER_SCENE")
					ENDIF	
				BREAK
				CASE ciFMMC_MISSION_CONTINUITY_FIXER_ID_SHORT_TRIP_1_MULE
					IF IS_AUDIO_SCENE_ACTIVE("DLC_FIXER_Short_Trip_01_Back_Of_Truck_Scene")					
						PRINTLN("[CONTINUITY][Vehicles][Vehicle", sVehState.iIndex, "] - PROCESS_VEHICLE_CLEANUP_CONTINUITY - STOP_AUDIO_SCENE: DLC_FIXER_Short_Trip_01_Back_Of_Truck_Scene")
						STOP_AUDIO_SCENE("DLC_FIXER_Short_Trip_01_Back_Of_Truck_Scene")
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC

PROC MC_SET_VEHICLE_PROOFS(VEHICLE_INDEX viPassed, INT iVeh)
	
	BOOL bBulletProof, bFlameProof, bExplosionProof, bCollisionProof, bMeleeProof, bSteamProof, bSmokeProof
	
	bBulletProof = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_SET_VEHICLE_AS_BULLET_PROOF)
	bFlameProof = SHOULD_VEHICLE_BE_FIRE_PROOF(viPassed, iVeh)
	bExplosionProof = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTen, ciFMMC_VEHICLE10_SetVehicleAsExplosionProof)
	bCollisionProof = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTen, ciFMMC_VEHICLE10_SetVehicleAsCollisionProof)
	bMeleeProof = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTen, ciFMMC_VEHICLE10_SetVehicleAsMeleeProof)
	bSteamProof = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTen, ciFMMC_VEHICLE10_SetVehicleAsSteamProof)
	bSmokeProof = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTen, ciFMMC_VEHICLE10_SetVehicleAsSmokeProof)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_MAKE_INVINCIBLE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_VEHICLE_PROOFS - Setting all proofs as ciFMMC_VEHICLE4_MAKE_INVINCIBLE set ")
		bBulletProof = TRUE
		bFlameProof = TRUE
		bExplosionProof = TRUE
		bCollisionProof = TRUE
		bMeleeProof = TRUE
		bSteamProof = TRUE
		bSmokeProof = TRUE
	ENDIF
	
	PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_VEHICLE_PROOFS - bBulletProof: ", bBulletProof, " - bFlameProof: ", bFlameProof, " - bExplosionProof: ", 
				bExplosionProof, " - bCollisionProof: ", bCollisionProof, " - bMeleeProof: ", bMeleeProof, " - bSteamProof: ", bSteamProof, " - bSmokeProof: ", bSmokeProof)
	SET_ENTITY_PROOFS(viPassed, bBulletProof, bFlameProof, bExplosionProof, bCollisionProof, bMeleeProof, bSteamProof, FALSE, bSmokeProof)
	
ENDPROC

PROC MC_SET_UP_VEH(VEHICLE_INDEX viPassed, INT iveh, NETWORK_INDEX &niVehicle[], VECTOR vSpawnLoc, BOOL bUsingRestartPoint, INT iSpawnInterior = -1, BOOL bVisible = TRUE, INT iNumPlayers = -1)
	
	UNUSED_PARAMETER(bUsingRestartPoint)
	
	INT iHealth, i
	PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Veh: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(viPassed)), " / iVeh: ", iVeh)
	
	IF IS_VEHICLE_MODEL(viPassed, MULE)
		FMMC_SET_THIS_VEHICLE_EXTRAS(viPassed, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iLivery)
	ELSE
		FMMC_SET_THIS_VEHICLE_EXTRAS(viPassed, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet)
		IF IS_VEHICLE_MODEL(viPassed, TRAILERLARGE)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciALLOW_MOC_ENTRY_THIS_MISSION)
			AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_ALLOW_MOC_ENTRY)
				IF DECOR_IS_REGISTERED_AS_TYPE("Creator_Trailer", DECOR_TYPE_INT)
					IF NOT DECOR_EXIST_ON(viPassed, "Creator_Trailer")
						IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
							DECOR_SET_INT(viPassed, "Creator_Trailer", NETWORK_HASH_FROM_PLAYER_HANDLE(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
							PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - setting ID decorator on created trailer to Creator_Trailer")
						ELSE
							PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - player's boss is invalid! Make sure you are in an MC.")
							SCRIPT_ASSERT("[RCC MISSION][MC_SET_UP_VEH] - player's boss is invalid! Make sure you are in an MC.")
						ENDIF
					ELSE
						PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - DECOR_EXIST_ON Already exists for Creator_Trailer.")
					ENDIF
				ELSE
					PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - DECOR_IS_REGISTERED_AS_TYPE is false for Creator_Trailer")
				ENDIF
				SET_VEHICLE_WEAPON_MODS(viPassed, 1, 0, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, iveh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModAirCounter, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModExhaust, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModBombBay, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModSpoiler)
			ENDIF
		ELIF IS_VEHICLE_MODEL(viPassed, AVENGER)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_AVENGER_INTERIOR_ENTRY)
				IF DECOR_IS_REGISTERED_AS_TYPE("Creator_Trailer", DECOR_TYPE_INT)
					IF NOT DECOR_EXIST_ON(viPassed, "Creator_Trailer")
						IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
							DECOR_SET_INT(viPassed, "Creator_Trailer", NETWORK_HASH_FROM_PLAYER_HANDLE(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
							PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - setting ID decorator on created Avenger to Creator_Trailer")
						ELSE
							SCRIPT_ASSERT("[RCC MISSION][MC_SET_UP_VEH] - player's boss is invalid! Make sure you are in an MC.")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_MODEL(viPassed, CARGOBOB4)
		SET_FORCE_HD_VEHICLE(viPassed, TRUE)
	ENDIF
	
	BOOL bInstantRLDoorOpen = !IS_VEHICLE_MODEL(viPassed, CARGOBOB4)	
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleDoorSettingsOnRule = -1 	// [ML] Only set up the vehicle doors like this from spawn if there is not a rule set where these settings should be applied
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Setting up initial door settings (open) to what is set in creator")
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet, enum_to_int(SC_DOOR_FRONT_LEFT))
			SET_VEHICLE_DOOR_OPEN(viPassed,SC_DOOR_FRONT_LEFT, FALSE, TRUE)
			SET_VEHICLE_DOOR_CONTROL(viPassed, SC_DOOR_FRONT_LEFT, DT_DOOR_INTACT, 1.0)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - (visual)- opening FL DOOR for vehicle: ", iVeh) 
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet, enum_to_int(SC_DOOR_FRONT_RIGHT))
			SET_VEHICLE_DOOR_OPEN(viPassed,SC_DOOR_FRONT_RIGHT, FALSE, TRUE)
			SET_VEHICLE_DOOR_CONTROL(viPassed, SC_DOOR_FRONT_RIGHT, DT_DOOR_INTACT, 1.0)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - (visual)- opening FR DOOR for vehicle: ", iVeh) 
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet, enum_to_int(SC_DOOR_REAR_LEFT))
			SET_VEHICLE_DOOR_OPEN(viPassed,SC_DOOR_REAR_LEFT, FALSE, bInstantRLDoorOpen)
			SET_VEHICLE_DOOR_CONTROL(viPassed, SC_DOOR_REAR_LEFT, DT_DOOR_INTACT, 1.0)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - (visual)- opening RL DOOR for vehicle: ", iVeh) 
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet, enum_to_int(SC_DOOR_REAR_RIGHT))
			SET_VEHICLE_DOOR_OPEN(viPassed,SC_DOOR_REAR_RIGHT, FALSE, TRUE)
			SET_VEHICLE_DOOR_CONTROL(viPassed, SC_DOOR_REAR_RIGHT, DT_DOOR_INTACT, 1.0)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - (visual)- opening RR DOOR for vehicle: ", iVeh) 
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet, enum_to_int(SC_DOOR_BONNET))
			SET_VEHICLE_DOOR_OPEN(viPassed,SC_DOOR_BONNET, FALSE, TRUE)
			SET_VEHICLE_DOOR_CONTROL(viPassed, SC_DOOR_BONNET, DT_DOOR_INTACT, 1.0)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - (visual)- opening BONNET for vehicle: ", iVeh) 
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet, enum_to_int(SC_DOOR_BOOT))
			SET_VEHICLE_DOOR_OPEN(viPassed,SC_DOOR_BOOT, FALSE, TRUE)
			SET_VEHICLE_DOOR_CONTROL(viPassed, SC_DOOR_BOOT, DT_DOOR_INTACT, 1.0)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - (visual) - opening BOOT for vehicle: ", iVeh) 
		ENDIF		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet, ciFMMC_VEHICLE_HEADLIGHTS_ON)
		SET_VEHICLE_LIGHTS(viPassed, SET_VEHICLE_LIGHTS_ON)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - turning on lights for vehicle: ", iVeh)
	ENDIF
	
	// Set the Vehicle Window Tints. creator should guard against vehicles that can't have tints: GET_NUM_VEHICLE_WINDOW_TINTS()
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleWindowTintColourIndex > -1
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Setting tinted windows for vehicle: ", iveh, " iVehicleWindowTintColourIndex: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleWindowTintColourIndex)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleWindowTintColourIndex = 0
			SET_VEHICLE_WINDOW_TINT(viPassed, 0)
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleWindowTintColourIndex = 1
			SET_VEHICLE_WINDOW_TINT(viPassed, 3)
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleWindowTintColourIndex = 2
			SET_VEHICLE_WINDOW_TINT(viPassed, 2)
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleWindowTintColourIndex = 3
			SET_VEHICLE_WINDOW_TINT(viPassed, 1)
		ENDIF		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_DELOREAN_SEATING_PREF_OVERRIDE)		
		IF g_FMMC_STRUCT.iNumberOfTeams = 3
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iTeamSeatPreference[2] = ciFMMC_SeatPreference_Driver
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Overriding iTeamSeatPreference to be ciFMMC_SeatPreference_Driver for Team: 2")
		ENDIF
	ENDIF
	
	IF DECOR_IS_REGISTERED_AS_TYPE("Heist_Veh_ID", DECOR_TYPE_INT)
		IF NOT DECOR_EXIST_ON(viPassed, "Heist_Veh_ID")
			DECOR_SET_INT(viPassed, "Heist_Veh_ID", i)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - setting ID decorator on created vehicle")
		ENDIF
	ENDIF
	
	IF DOES_USE_OLD_STYLE_CAMO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn)
		FMMC_SET_THIS_VEHICLE_COLOURS(viPassed, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iColour, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iLivery,  g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iColourCombination, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iColour2, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iColour3, 0.5, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCustomModSettingsIndex)	
	ELSE					
		FMMC_SET_THIS_VEHICLE_COLOURS(viPassed, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iColour, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iLivery,  g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iColourCombination, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iColour2, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iColour3, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fEnvEff, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCustomModSettingsIndex)
	ENDIF
	
	SET_VEHICLE_HAS_STRONG_AXLES(viPassed, TRUE)
		
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_VEHICLE_NOT_CONSIDERED_BY_PLAYERS)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(viPassed, FALSE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Considered by player FALSE")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_VEHICLE_NOT_CONSIDERED_BY_FLEEING_PEDS)
		SET_VEHICLE_CAN_BE_USED_BY_FLEEING_PEDS(viPassed, FALSE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - setting to not be usable by fleeing peds")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_BULLETPROOF_TYRES)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - setting Tyres cannot burst.")
		SET_VEHICLE_TYRES_CAN_BURST(viPassed, FALSE)
	ENDIF
	
	CHECK_IF_VEHICLE_SHOULD_BE_RETAINED_IN_INTERIOR(viPassed, vSpawnLoc, iSpawnInterior #IF IS_DEBUG_BUILD , iVeh #ENDIF)
	
	IF IS_THIS_MODEL_A_BOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn)
	OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = SUBMERSIBLE
	OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = SUBMERSIBLE2
		IF NOT (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = SUBMERSIBLE)
		AND NOT (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = SUBMERSIBLE2)
			//Asserts if not called on a boat
			SET_FORCE_LOW_LOD_ANCHOR_MODE(viPassed, FALSE)
			SET_BOAT_SINKS_WHEN_WRECKED(vipassed, TRUE)
		ENDIF
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive,  ciFMMC_VEHICLE5_DONT_PLACE_ON_GROUND)
			SET_VEHICLE_ON_GROUND_PROPERLY(viPassed)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - calling SET_VEHICLE_ON_GROUND_PROPERLY for boat veh")
		ENDIF
		IF CAN_ANCHOR_BOAT_HERE (viPassed)
		AND GET_ENTITY_MODEL(viPassed) != STROMBERG
			SET_BOAT_ANCHOR(viPassed, TRUE)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - calling SET_BOAT_ANCHOR for boat veh")
		ENDIF
		IF NOT (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = SUBMERSIBLE)
		AND NOT (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = SUBMERSIBLE2)
			SET_BOAT_LOW_LOD_ANCHOR_DISTANCE(viPassed,99999.0)
		ENDIF
		
		IF GET_ENTITY_MODEL(viPassed) = SEASHARK
		OR GET_ENTITY_MODEL(viPassed) = SEASHARK2
		OR GET_ENTITY_MODEL(viPassed) = SEASHARK3
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Jetski has been created, setting engine off")
			SET_VEHICLE_ENGINE_ON(viPassed, FALSE, TRUE)
		ENDIF
	ELSE		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_VehicleStartsAirborne)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(viPassed, FALSE)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Setting SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION false for veh")
		ELSE
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(viPassed, TRUE)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Setting SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION true for veh")
		ENDIF
	ENDIF
	
	IF IS_THIS_MODEL_A_PLANE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn)
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_VehicleStartsAirborne)
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_FREEZE_POS)	
		SET_HELI_BLADES_FULL_SPEED(viPassed)
	ENDIF
	
	IF IS_THIS_MODEL_A_PLANE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn)
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_VehicleStartsAirborne)
		CONTROL_LANDING_GEAR(viPassed, LGC_RETRACT_INSTANT)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Setting landing gear to LGC_RETRACT_INSTANT for plane, ", iVeh, " because of ciFMMC_VEHICLE3_VehicleStartsAirborne")
	ENDIF
	
	SET_DISABLE_VEHICLE_PETROL_TANK_DAMAGE(viPassed, FALSE)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_DISABLE_BREAKING)
		SET_VEHICLE_CAN_BREAK(viPassed, FALSE)
		IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(viPassed))
			SET_VEHICLE_BROKEN_PARTS_DONT_AFFECT_AI_HANDLING(viPassed, TRUE)
		ENDIF
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Setting vehicle can break to false")
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_DISABLE_HELI_EXPLODE_FROM_BODY_DAMAGE)
		SET_DISABLE_HELI_EXPLODE_FROM_BODY_DAMAGE(viPassed, TRUE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Setting heli disable explode from body damage")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetNine, ciFMMC_VEHICLE9_SpawnIgnoreHeightmapOptimisations)	
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Calling SET_AIRCRAFT_IGNORE_HIGHTMAP_OPTIMISATION")
		SET_AIRCRAFT_IGNORE_HIGHTMAP_OPTIMISATION(viPassed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTwo, ciFMMC_VEHICLE2_USE_CUSTOM_EXTRAS)
		SET_VEHICLE_EXTRAS_FROM_BITSET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleExtrasBitset, viPassed)
	ENDIF

	iHealth = GET_FMMC_VEHICLE_MAX_HEALTH(iveh, iNumPlayers)
	
	SET_ENTITY_HEALTH(viPassed, iHealth)
	SET_ENTITY_MAX_HEALTH(viPassed, iHealth)

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_RIGHT_SELF_WHEN_OUT_OF_SIGHT)
		ADD_VEHICLE_STUCK_CHECK_WITH_WARP(viPassed, 1.0, 1000, TRUE, TRUE, TRUE, 1)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Calling ADD_VEHICLE_STUCK_CHECK_WITH_WARP on Vehicle: ", iVeh)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet, ciFMMC_VEHICLE_STOLEN)
		SET_VEHICLE_IS_STOLEN(viPassed, TRUE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - is stolen: true ")
	ELSE
		SET_VEHICLE_IS_STOLEN(viPassed, FALSE)
		SET_VEHICLE_INFLUENCES_WANTED_LEVEL(viPassed, FALSE)
		SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(viPassed, TRUE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - is stolen: false ")
	ENDIF
	
	SET_VEHICLE_MAY_BE_USED_BY_GOTO_POINT_ANY_MEANS(viPassed, TRUE)
	SET_VEHICLE_CAN_BE_USED_BY_FLEEING_PEDS(viPassed, TRUE)
	
	PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - ciFMMC_VEHICLE6_EXPLODE_ON_CONTACT: ", iS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_EXPLODE_ON_CONTACT))
	SET_ALLOW_VEHICLE_EXPLODES_ON_CONTACT(viPassed, iS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_EXPLODE_ON_CONTACT))
	
	SET_VEHICLE_ENGINE_CAN_DEGRADE(viPassed, FALSE)
	
	IF iHealth >= 4000
		SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(viPassed, FALSE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE: false ")
		IF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = TANKER)
		OR (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = TANKER2)
		OR (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn =ARMYTANKER)
			SET_VEHICLE_BODY_HEALTH(viPassed,1000)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - TANKER, set_vehicle_body_health as 1000 to stop it exploding so easily")
		ENDIF
	ENDIF
		
	PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - health is: ", ihealth, " model is: ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(viPassed)))
	
	IF GET_ENTITY_MODEL(viPassed) = LUXOR2
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Setting decor for LUXOR2 - 'DECOR_SET_INT(viPassed, 'EnableVehLuxeActs', 1)'")
		DECOR_SET_INT(viPassed, "EnableVehLuxeActs", 1)
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_OPEN_PERSONAL_VEHICLE_DOORS)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Setting vehicle as peacock")
		IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
			INT iMPDecorator
			
			IF DECOR_EXIST_ON(viPassed, "MPBitset") 
				iMPDecorator = DECOR_GET_INT(viPassed, "MPBitset")
			ENDIF
			
			SET_BIT(iMPDecorator,MP_DECORATOR_BS_PIM_DOOR_CONTROL)
			DECOR_SET_INT(viPassed, "MPBitset", iMPDecorator)
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fEngineHealth <> 1000	
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR)		
		SET_VEHICLE_ENGINE_HEALTH(viPassed,MC_GET_FMMC_VEHICLE_MAX_ENGINE_HEALTH(iveh, iNumPlayers))
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - engine health changed to ",MC_GET_FMMC_VEHICLE_MAX_ENGINE_HEALTH(iveh, iNumPlayers))
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fPetrolHealth <> 1000
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR)		
		SET_VEHICLE_PETROL_TANK_HEALTH(viPassed,MC_GET_FMMC_VEHICLE_MAX_PETROL_TANK_HEALTH(iveh, iNumPlayers))
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - petrol health changed to ",MC_GET_FMMC_VEHICLE_MAX_PETROL_TANK_HEALTH(iveh, iNumPlayers))
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fBodyHealth <> 1000
		SET_VEHICLE_BODY_HEALTH(viPassed,MC_GET_VEHICLE_MAX_BODY_HEALTH(iveh, iNumPlayers))
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - body health changed to ",MC_GET_VEHICLE_MAX_BODY_HEALTH(iveh, iNumPlayers))
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR)
		IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(viPassed))
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHeliMainRotorhealth <> 1000
				SET_HELI_MAIN_ROTOR_HEALTH(viPassed,MC_GET_FMMC_HELI_MAX_MAIN_ROTOR_HEALTH(iveh, iNumPlayers))
				PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - main rotor health changed to ",MC_GET_FMMC_HELI_MAX_MAIN_ROTOR_HEALTH(iveh, iNumPlayers))
			ENDIF
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHeliTailRotorhealth <> 1000
				SET_HELI_TAIL_ROTOR_HEALTH(viPassed,MC_GET_FMMC_HELI_MAX_TAIL_ROTOR_HEALTH(iveh, iNumPlayers))
				PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - tail rotor health changed to ",MC_GET_FMMC_HELI_MAX_TAIL_ROTOR_HEALTH(iveh, iNumPlayers))
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_HELI_ROTOR_BOOM_UNBREAKABLE)
				SET_HELI_TAIL_BOOM_CAN_BREAK_OFF(viPassed, FALSE)
				PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - tail boom set to unbreakable")
			ENDIF
		ELIF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(viPassed))
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHeliMainRotorhealth <> 1000
				SET_PLANE_PROPELLER_HEALTH(viPassed,MC_GET_FMMC_HELI_MAX_MAIN_ROTOR_HEALTH(iveh, iNumPlayers))
				PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - propeller health changed to ",MC_GET_FMMC_HELI_MAX_MAIN_ROTOR_HEALTH(iveh, iNumPlayers))
			ENDIF
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fEngineHealth <> 1000
				SET_PLANE_ENGINE_HEALTH(viPassed,MC_GET_FMMC_VEHICLE_MAX_ENGINE_HEALTH(iveh, iNumPlayers))
				PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - engine health changed to ",MC_GET_FMMC_VEHICLE_MAX_ENGINE_HEALTH(iveh, iNumPlayers))
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet, ciFMMC_VEHICLE_EASYEXPLODER)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - ciFMMC_VEHICLE_EASYEXPLODER ")
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fEngineHealth = 1001
			SET_VEHICLE_ENGINE_HEALTH(viPassed,400)
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fPetrolHealth = 1001
			SET_VEHICLE_PETROL_TANK_HEALTH(viPassed,400)
		ENDIF
		SET_FORCE_VEHICLE_ENGINE_DAMAGE_BY_BULLET(viPassed, TRUE)
	ENDIF
	
	PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Passing Vehicle petrol bit in car's creation")
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_PETROL_CANNOT_BE_DAMAGED)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - ciFMMC_VEHICLE4_PETROL_CANNOT_BE_DAMAGED")
		SET_DISABLE_VEHICLE_PETROL_TANK_DAMAGE(viPassed, TRUE)
		SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(viPassed, TRUE)
	ENDIF		
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_PreventEngineFromMisfiring)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - ciFMMC_VEHICLE3_PreventEngineFromMisfiring is set, turn off engine misfiring")
		SET_VEHICLE_CAN_ENGINE_MISSFIRE(viPassed, FALSE)
	ENDIF
		
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fDamageScalarWithMap <> 1.0
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Calling to scale the incoming damage, setting map to: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fDamageScalarWithMap)
		SET_VEHICLE_DAMAGE_SCALES(viPassed, 1.0, 1.0, 1.0, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fDamageScalarWithMap)		
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR)
		
		SET_ENTITY_HEALTH(viPassed, ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS)
		SET_ENTITY_MAX_HEALTH(viPassed, ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - (forced - ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR) vehicle ", iveh, " Entity Health changed to ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS (we are using body)")
		
		SET_VEHICLE_ENGINE_HEALTH(viPassed, ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - (forced - ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR) vehicle ", iveh, " engine health changed to ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS")
		
		SET_VEHICLE_PETROL_TANK_HEALTH(viPassed, ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - (forced - ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR) vehicle ", iveh, " petrol health changed to ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS")
		
		IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(viPassed))
			SET_PLANE_PROPELLER_HEALTH(viPassed, ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - (forced - ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR) plane ", iveh, " propeller health changed to ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS ")
		
			SET_PLANE_ENGINE_HEALTH(viPassed, ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - (forced - ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR) plane ", iveh, " engine health changed to ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS")
			
			IF GET_ENTITY_MODEL(viPassed) = AVENGER
				//Block Wing Damage
				PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - (forced - ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR) Blocking Wing Damage For Avenger")
				SET_PLANE_SECTION_DAMAGE_SCALE(viPassed, WING_L, 0.0)
				SET_PLANE_SECTION_DAMAGE_SCALE(viPassed, WING_R, 0.0)
				SET_PLANE_SECTION_DAMAGE_SCALE(viPassed, ENGINE_L, 0.0)
				SET_PLANE_SECTION_DAMAGE_SCALE(viPassed, ENGINE_R, 0.0)
			ENDIF
		ENDIF
		
		IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(viPassed))
			SET_HELI_MAIN_ROTOR_HEALTH(viPassed, ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - (forced - ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR) heli ", iveh, " main rotor health changed to ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS")
		
			SET_HELI_TAIL_ROTOR_HEALTH(viPassed, ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - (forced - ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR) heli ", iveh, " tail rotor health changed to ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS")	
		
			SET_HELI_TAIL_BOOM_CAN_BREAK_OFF(viPassed, FALSE)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - (forced - ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR) heli ", iveh, " tail boom set to unbreakable")
		ENDIF
		
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - (forced - ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR) vehicle ", iveh, " ciFMMC_VEHICLE4_PETROL_CANNOT_BE_DAMAGED")
		SET_DISABLE_VEHICLE_PETROL_TANK_DAMAGE(viPassed, TRUE)
		SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(viPassed, TRUE)
	ENDIF	
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = DELUXO
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - turning on flight mode for deluxo")
		SET_SPECIAL_FLIGHT_MODE_ALLOWED(viPassed, TRUE)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_DELOREAN_START_HOVERING)
			SET_SPECIAL_FLIGHT_MODE_TARGET_RATIO(viPassed, 1)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - created with wheels in special flight position")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet, ciFMMC_VEHICLE_SIREN_LIGHT)
		SET_VEHICLE_SIREN(viPassed, TRUE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - turning on lights for vehicle")
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet, ciFMMC_VEHICLE_SIREN_SOUND)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - turning Off Siren sounds for vehicle")
			SET_VEHICLE_HAS_MUTED_SIRENS(viPassed, TRUE)		
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet, ciFMMC_VEHICLE_SIREN_SOUND)
		SET_SIREN_WITH_NO_DRIVER(viPassed, TRUE)
		SET_SIREN_BYPASS_MP_DRIVER_CHECK(viPassed, TRUE)
		TRIGGER_SIREN_AUDIO(viPassed)			
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - turning on Siren sounds for vehicle")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet, ciFMMC_VEHICLE_DOORS_LOCKED)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_LOCKED_TO_PLAYERS_ONLY)
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(viPassed, TRUE)
			SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(viPassed, TRUE)
			SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(viPassed, TRUE)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - locking the doors for vehicle (players only): ", iVeh, " model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(viPassed)))
		ELSE
			SET_VEHICLE_DOORS_LOCKED(viPassed, VEHICLELOCK_LOCKED)
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(viPassed, TRUE)
			SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(viPassed, TRUE)
			SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(viPassed, TRUE)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - locking the doors for vehicle: ", iVeh, " model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(viPassed)))
		ENDIF
	ELSE
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleDoorSettingsOnRule = -1 	// [ML] Only set up the vehicle doors like this from spawn if there is not a rule set where these settings should be applied
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Setting up initial door settings (locked/unlocked) to what is set in creator")
			FOR i = SC_DOOR_FRONT_LEFT TO ENUM_TO_INT(SC_DOOR_BOOT)
				IF DOES_VEHICLE_HAVE_DOOR(viPassed, INT_TO_ENUM(SC_DOOR_LIST, i))
				AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_DOOR_FRONT_LEFT_LOCKED + i) 
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_LOCKED_TO_PLAYERS_ONLY)
						SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(viPassed, i, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
						PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - locking door (players only) ", i , " for vehicle: ", iveh, " Model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(viPassed)))
						SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(viPassed, TRUE)
					ELSE
						PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - locking door ", i , " for vehicle: ", iveh, " Model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(viPassed)))
						SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(viPassed, i, VEHICLELOCK_LOCKED)
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		HANDLE_VEH_TEAM_RESTRICTION(iveh, viPassed)
	ENDIF
	
	IF MC_IS_VEHICLE_MISSION_KILL(iVeh)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - SET_VEHICLE_ALLOW_NO_PASSENGERS_LOCKON for vehicle")
		SET_VEHICLE_ALLOW_NO_PASSENGERS_LOCKON(viPassed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.iVehicleTireBitSet , iVeh)
		SET_VEHICLE_TYRES_CAN_BURST(viPassed, FALSE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Tyres are bullet proof for vehicle") 
	ENDIF
	
	SET_VEHICLE_AI_CAN_USE_EXCLUSIVE_SEATS(viPassed, TRUE)
	
	IF IS_VEHICLE_MODEL_A_CARGOBOB(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn)
		BOOL bHookDropOff
		IF MC_IS_VEHICLE_A_DROP_OFF_VEHICLE(iVeh, bHookDropOff)
			IF bHookDropOff
				//Create magnet to attach to:
				CREATE_PICK_UP_ROPE_FOR_CARGOBOB(viPassed, PICKUP_MAGNET)
				SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(viPassed, 4, 6)
				
				PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Cargobob vehicle needs to be used as a drop off w magnet, creating magnet...")
				
				SET_CARGOBOB_PICKUP_MAGNET_SET_AMBIENT_MODE(viPassed, TRUE, TRUE)
				SET_CARGOBOB_PICKUP_MAGNET_ACTIVE(viPassed, FALSE)
				SET_CARGOBOB_PICKUP_MAGNET_REDUCED_STRENGTH(viPassed, 0)
				SET_CARGOBOB_PICKUP_MAGNET_PULL_STRENGTH(viPassed, 0) // Default 2.0
				
				SET_CARGOBOB_PICKUP_MAGNET_STRENGTH(viPassed, 0.65) // Default 1.0
				SET_CARGOBOB_PICKUP_MAGNET_FALLOFF(viPassed, -0.5) // Default -0.5
				SET_CARGOBOB_PICKUP_MAGNET_REDUCED_FALLOFF(viPassed, -0.4)
				SET_CARGOBOB_PICKUP_MAGNET_PULL_ROPE_LENGTH(viPassed, 1.5) // Default 3.0
				
				SET_CARGOBOB_PICKUP_ROPE_DAMPING_MULTIPLIER(viPassed, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fCargobobDamp) //default 1.0
				
				SET_CARGOBOB_PICKUP_MAGNET_ENSURE_PICKUP_ENTITY_UPRIGHT(viPassed, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_ENTITY_BONE_INDEX_BY_NAME(viPassed, "attach_female") <> -1
		SET_VEHICLE_AUTOMATICALLY_ATTACHES(viPassed, TRUE, TRUE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - setting attachment for veh")
	ENDIF
	
	IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh",DECOR_TYPE_INT)
		DECOR_SET_INT(viPassed, "Not_Allow_As_Saved_Veh",1)
	ENDIF
	
	IF NOT IS_THIS_A_HEIST_MISSION()
		SET_ENTITY_VISIBLE(viPassed, bVisible)
	ELSE
		SET_ENTITY_VISIBLE(viPassed, TRUE)
	ENDIF
	
	FMMC_SET_VEH_MOD_PRESET(viPassed,
							g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn,
							g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iModPreset,
							g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iNeonColour)
	
	//Vehicle window removal
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_WINDOW_FRONT_LEFT)
		REMOVE_VEHICLE_WINDOW(viPassed, SC_WINDOW_FRONT_LEFT)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_WINDOW_FRONT_RIGHT)
		REMOVE_VEHICLE_WINDOW(viPassed, SC_WINDOW_FRONT_RIGHT)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_WINDOW_REAR_LEFT)
		REMOVE_VEHICLE_WINDOW(viPassed, SC_WINDOW_REAR_LEFT)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_WINDOW_REAR_RIGHT)
		REMOVE_VEHICLE_WINDOW(viPassed, SC_WINDOW_REAR_RIGHT)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_WINDOW_MIDDLE_LEFT)
		REMOVE_VEHICLE_WINDOW(viPassed, SC_WINDOW_MIDDLE_LEFT)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_WINDOW_MIDDLE_RIGHT)
		REMOVE_VEHICLE_WINDOW(viPassed, SC_WINDOW_MIDDLE_RIGHT)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_WINDSCREEN_FRONT)
		REMOVE_VEHICLE_WINDOW(viPassed, SC_WINDSCREEN_FRONT)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_WINDSCREEN_REAR)
		REMOVE_VEHICLE_WINDOW(viPassed, SC_WINDSCREEN_REAR)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTwo, ciFMMC_VEHICLE2_DYNAMIC_APARTMENT_SPAWN_OVERRIDE)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_DYNAMIC_FACILITY_SPAWN_OVERRIDE)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_UseDynamicArcadeSpawnLoc)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive,  ciFMMC_VEHICLE5_DONT_PLACE_ON_GROUND)
			SET_VEHICLE_ON_GROUND_PROPERLY(viPassed)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - SET_VEHICLE_ON_GROUND_PROPERLY - ciFMMC_VEHICLE2_DYNAMIC_APARTMENT_SPAWN_OVERRIDE/ciFMMC_VEHICLE7_DYNAMIC_FACILITY_SPAWN_OVERRIDE/ciFMMC_VEHICLE8_UseDynamicArcadeSpawnLoc")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_RESIST_EXPLOSIONS)
		IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(viPassed))
			SET_PLANE_RESIST_TO_EXPLOSION(viPassed, TRUE)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - SET_PLANE_RESIST_TO_EXPLOSION ")
		ELIF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(viPassed))
			SET_HELI_RESIST_TO_EXPLOSION(viPassed, TRUE)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - SET_HELI_RESIST_TO_EXPLOSION ")
		ELSE
			IF GET_ENTITY_MODEL(viPassed) = INT_TO_ENUM(MODEL_NAMES, HASH("PHANTOM2"))
				SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(viPassed, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_REQUIRES_HOTWIRE)
		SET_VEHICLE_DOORS_LOCKED(viPassed,VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
		SET_VEHICLE_NEEDS_TO_BE_HOTWIRED(viPassed, TRUE)		
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Set as requiring Hotwire")
	ELSE
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - ciFMMC_VEHICLE8_REQUIRES_HOTWIRE not set")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_MAKE_INVINCIBLE)
		SET_ENTITY_INVINCIBLE(viPassed, TRUE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - SET_ENTITY_INVINCIBLE ")
	ELSE
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - ciFMMC_VEHICLE4_MAKE_INVINCIBLE not set ")
	ENDIF
	
	MC_SET_VEHICLE_PROOFS(viPassed, iVeh)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_MAKE_STRONG)
		SET_VEHICLE_STRONG(viPassed, TRUE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - SET_VEHICLE_STRONG ")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_RESIST_EXPLODING_VEHICLES)
		SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(viPassed, FALSE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - SET_VEHICLE RESIST EXPLODING VEHICLES")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTwo, ciFMMC_VEHICLE2_AUTO_IGNITION_ON)
		SET_VEHICLE_ENGINE_ON(viPassed, TRUE, TRUE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - ciFMMC_VEHICLE2_AUTO_IGNITION_ON")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_FREEZE_POS)
		FREEZE_ENTITY_POSITION(viPassed, TRUE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - ciFMMC_VEHICLE4_VEHICLE_FREEZE_POS")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_PLACE_CHEVRON)
		FM_EVENTDRAW_MARKER_ABOVE_VEHICLE(viPassed, 
		g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iChevronColours[0],
		g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iChevronColours[1],
		g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iChevronColours[2])
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - ciFMMC_VEHICLE4_VEHICLE_PLACE_CHEVRON")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RESIST_RAMP_CARS)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - ciFMMC_VEHICLE4_VEHICLE_RESIST_RAMP_CARS")
		SET_ALLOW_RAMMING_SOOP_OR_RAMP(viPassed, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_IMMUNE_HOMING_LAUNCHER)
		SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(viPassed, FALSE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - ciFMMC_VEHICLE4_VEHICLE_IMMUNE_HOMING_LAUNCHER - SET")
	ENDIF	
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_ALLOW_EMPTY_VEH_HOMING_LOCKON)
		SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(viPassed, TRUE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - ciFMMC_VEHICLE5_ALLOW_EMPTY_VEH_HOMING_LOCKON - SET")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_IS_CARGOBOB_ATTACHED_TO_VEH)
		IF NOT DOES_CARGOBOB_HAVE_PICKUP_MAGNET(viPassed)
			SET_VEHICLE_ENGINE_ON(viPassed, TRUE, TRUE)
			SET_HELI_BLADES_FULL_SPEED(viPassed)
		
			CREATE_PICK_UP_ROPE_FOR_CARGOBOB(viPassed, PICKUP_MAGNET)
			SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(viPassed, 4, 6, TRUE)
		ENDIF
	ENDIF
							
	SET_VEHICLE_ALL_CUSTOM_MODS(viPassed, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCustomModSettingsIndex)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = INT_TO_ENUM(MODEL_NAMES, HASH("dune4"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = INT_TO_ENUM(MODEL_NAMES, HASH("dune5"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = INT_TO_ENUM(MODEL_NAMES, HASH("phantom2"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = INT_TO_ENUM(MODEL_NAMES, HASH("boxville5"))
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Setting VEHICLE_SET_RAMP_AND_RAMMING_CARS_TAKE_DAMAGE FALSE ... Model = ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn))
		VEHICLE_SET_RAMP_AND_RAMMING_CARS_TAKE_DAMAGE(viPassed, FALSE)
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_VehicleStartsAirborne)	
		BOOL bShouldPlaceOnGround = TRUE
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive,  ciFMMC_VEHICLE5_DONT_PLACE_ON_GROUND)
			bShouldPlaceOnGround = FALSE
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Not calling SET_VEHICLE_ON_GROUND_PROPERLY due to ciFMMC_VEHICLE5_DONT_PLACE_ON_GROUND | iVeh: ", iVeh)
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehInterior = 275201 //NATIVE_TO_INT of the main Casino Interior
			bShouldPlaceOnGround = FALSE
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Not calling SET_VEHICLE_ON_GROUND_PROPERLY due to Casino interior issues | iVeh: ", iVeh)
		ENDIF
		
		IF bShouldPlaceOnGround
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - calling SET_VEHICLE_ON_GROUND_PROPERLY for veh: ", iVeh)
			VECTOR tempVec
			tempVec = GET_ENTITY_COORDS(viPassed)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - pre on ground properly position: ", tempVec)
			SET_VEHICLE_ON_GROUND_PROPERLY(viPassed)
			tempVec = GET_ENTITY_COORDS(viPassed)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - post on ground properly position: ", tempVec)
		ENDIF
	ELSE
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Not calling SET_VEHICLE_ON_GROUND_PROPERLY as ciFMMC_VEHICLE3_VehicleStartsAirborne | iVeh: ", iVeh)
	ENDIF
	
	MC_SET_ID_INT_DECOR_ON_ENTITY(GET_ENTITY_FROM_PED_OR_VEHICLE(viPassed), iVeh)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetNine, ciFMMC_VEHICLE9_UseEnableRampCarSideImpulse)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = INT_TO_ENUM(MODEL_NAMES, HASH("dune4"))
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Enabling ramp car side impulse")
			VEHICLE_SET_ENABLE_RAMP_CAR_SIDE_IMPULSE(viPassed, TRUE)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive,  coFMMC_VEHICLE5_ENABLE_NORMALISED_RAMPS)
		VEHICLE_SET_ENABLE_NORMALISE_RAMP_CAR_VERTICAL_VELOCTIY(viPassed, TRUE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Normalising ramp car vertical velocity for vehicle: ", iVeh)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iDuneFlipScale != 10
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Calling SET_SCRIPT_RAMP_IMPULSE_SCALE with creator set value of: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iDuneFlipScale)
		FLOAT fScale
		fScale = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iDuneFlipScale) / 10
		SET_SCRIPT_RAMP_IMPULSE_SCALE(viPassed, fScale)
	ENDIF
	IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRespawnOnRule, -1)
		SET_ALLOW_MIGRATE_TO_SPECTATOR(viPassed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciDISABLE_VEHICLE_KERS)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Disabling KERS on vehicle: ", iVeh)
		BLOCK_VEHICLE_KERS(viPassed, FALSE)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iHealth > 4999
		SET_VEHICLE_STRONG(viPassed, TRUE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Setting vehicle strong")
	ENDIF
	
	SET_UNBREAKABLE_DOORS_FOR_VEHICLE(viPassed, iVeh)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetNine, ciFMMC_VEHICLE9_DisableSuperDummy)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Disabling dummy for vehicle")
		SET_DISABLE_SUPERDUMMY(viPassed, TRUE)
	ENDIF
	
	// If we have an attached object
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAttachParent > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAttachParentType = CREATION_TYPE_VEHICLES

		IF IS_NET_VEHICLE_DRIVEABLE(niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAttachParent])
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - This vehicle is a trailer set to be attached to Veh ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAttachParent)
			
			IF GET_ENTITY_MODEL(viPassed) = CARGOBOB2
				FMMC_ATTACH_CARGOBOB_TO_VEHICLE(viPassed, NET_TO_VEH(niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAttachParent]), TRUE)
			ELSE
				FMMC_ATTACH_VEHICLE_TO_VEHICLE(viPassed, NET_TO_VEH(niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAttachParent]), TRUE, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleInVehicle_ContainerVehicleIndex > -1
		// This vehicle wants to initially be inside a specified MOC! Send an event here to get our system to handle this to kick in for all players
		PRINTLN("[VehicleInVehicle][Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Vehicle is set up to initially be inside MOC vehicle: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleInVehicle_ContainerVehicleIndex)
		SET_DISABLE_SUPERDUMMY(viPassed, TRUE)
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_VehicleInVehicle_StateChange, iVeh, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ciVEHICLE_IN_VEHICLE_STATE__PREPARING)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_HOMING_ROCKET_PRIORITY_TARGET)
		SET_ENTITY_IS_TARGET_PRIORITY(viPassed, TRUE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Setting SET_ENTITY_IS_TARGET_PRIORITY on placed vehicle")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_CANT_BE_DAMAGED_BY_PLAYERS)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - setting cant be damaged by players for veh")
		SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(viPassed, FALSE, rgfm_PlayerTeam[0])
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_DONT_TERMINATE_TASK_WHEN_ACHIEVED)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - setting dont terminate task when achieved for veh")
		SET_VEHICLE_DONT_TERMINATE_TASK_WHEN_ACHIEVED(viPassed)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_RESIST_OWNED_EXPLOSION_DAMAGE)
		SET_VEHICLE_NO_EXPLOSION_DAMAGE_FROM_DRIVER(viPassed, TRUE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - setting cant be damaged owned explosions for veh")
	ENDIF
	
	IF DOES_VEHICLE_HAVE_ANY_WEAPON_MODS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn)
		SET_VEHICLE_WEAPON_MODS(viPassed, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModTurret, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModArmour, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, iveh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModAirCounter, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModExhaust, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModBombBay, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModSpoiler, DEFAULT, DEFAULT, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iWaterBombStartingAmmo)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciALLOW_BOMBUSHKA_TURRETS_IN_MISSION)
	AND IS_VEHICLE_MODEL(viPassed, BOMBUSHKA)
		SET_SHOULD_RESET_TURRET_IN_SCRIPTED_CAMERAS(viPassed, TRUE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Setting SET_SHOULD_RESET_TURRET_IN_SCRIPTED_CAMERAS on vehicle")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_DISABLE_SPACE_ROCKETS)
		DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_VEHICLE_SPACE_ROCKET, viPassed, NULL)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Disabling space rockets on vehicle")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_SPAWN_IN_VTOL_MODE)
	AND IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(viPassed))
		SET_VEHICLE_FLIGHT_NOZZLE_POSITION_IMMEDIATE(viPassed, 1.0)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Setting vehicle to start in Flight Mode Vertical")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_ALLOW_OBJECT_TARGETING)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Setting vehicle able to target objects")
		SET_VEHICLE_WEAPON_CAN_TARGET_OBJECTS(viPassed, TRUE)
	ENDIF
	
	IF IS_MISSION_VEHICLE_TO_BE_USED_AS_A_RESPAWN_VEHICLE_BY_ANYBODY(iVeh)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Setting vehicle as a specific respawn vehicle")
		SET_VEHICLE_AS_A_SPECIFIC_RESPAWN_VEHICLE(viPassed)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iBurstTyresRespawnIndex = 0
		MC_BURST_VEHICLE_TYRES(viPassed)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iBurstTyresRespawnIndex > 0
		IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iBurstTyresRespawnIndex = 4
			MC_BURST_VEHICLE_TYRES(viPassed)
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2)
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iBurstTyresRespawnIndex = 3
			MC_BURST_VEHICLE_TYRES(viPassed)
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iBurstTyresRespawnIndex = 2
			MC_BURST_VEHICLE_TYRES(viPassed)
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iBurstTyresRespawnIndex = 1
			MC_BURST_VEHICLE_TYRES(viPassed)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_HIGH_SPEED_EDGE_FALL_DETECTION)
		SET_NETWORK_ENABLE_HIGH_SPEED_EDGE_FALL_DETECTION(viPassed, TRUE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Enabling vehicle HIGH SPEED EDGE FALL DETECTION")
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_PlayerVehiclesExplosionResistant)
		SET_DISABLE_VEHICLE_EXPLOSIONS_DAMAGE(TRUE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Calling SET_DISABLE_VEHICLE_EXPLOSIONS_DAMAGE")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_Monster3IncreasedCrushDamage)
		IF IS_VEHICLE_A_MONSTER(GET_ENTITY_MODEL(viPassed))
			SET_INCREASE_WHEEL_CRUSH_DAMAGE(viPassed, TRUE)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_CanOnlyBeDamagedByPlayers)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - setting Can Only be damaged by players for veh")
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(viPassed, TRUE)
	ENDIF
		
	// If any of these are set, then we want to trigger them all.
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iEntityRelGroupDamageRestrictions <> 0
		INT iEntDmgRestriction = 0
		FOR iEntDmgRestriction = 0 TO OPTION_ENTITY_CAN_ONLY_BE_DAMAGE_BY_RELGR_MAX						
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - veh: ", iVeh, " Relationship Group Number: ", iEntDmgRestriction, " Set: ", IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iEntityRelGroupDamageRestrictions, iEntDmgRestriction))
			SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(viPassed, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iEntityRelGroupDamageRestrictions, iEntDmgRestriction), GET_REL_GROUP_HASH_FROM_INT(iEntDmgRestriction))
		ENDFOR
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehInterior = -1
		VECTOR vCurrentCoord = GET_ENTITY_COORDS(viPassed)
		IF IS_COLLISION_MARKED_OUTSIDE(vCurrentCoord)
		OR GET_INTERIOR_AT_COORDS(vCurrentCoord) = NULL // (false positive) sometimes.
		OR GET_INTERIOR_FROM_COLLISION(vCurrentCoord) = NULL
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - There is no Interior at the location we are spawning Calling CLEAR_ROOM_FOR_ENTITY (vCurrentCoord: ", vCurrentCoord, ")")
			CLEAR_ROOM_FOR_ENTITY(viPassed)	
		ELSE
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - There is an Interior at the location we are spawning (vCurrentCoord: ", vCurrentCoord, ")")
		ENDIF
	ELSE
		INTERIOR_INSTANCE_INDEX iiVehicleInterior = INT_TO_NATIVE(INTERIOR_INSTANCE_INDEX, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehInterior)
		IF IS_VALID_INTERIOR(iiVehicleInterior)
			IF IS_INTERIOR_READY(iiVehicleInterior)
				RETAIN_ENTITY_IN_INTERIOR(viPassed, iiVehicleInterior)
				PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iInterior: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehInterior, " / Calling RETAIN_ENTITY_IN_INTERIOR")
			ELSE
				PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iInterior: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehInterior, " / Was not ready!")
			ENDIF
		ELSE
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iInterior: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehInterior, " / Couldn't find interior")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_MatchPlayerYachtColour)
		APPLY_YACHT_COLOURS_TO_VEHICLE(viPassed, ciYACHT_LOBBY_HOST_YACHT_INDEX)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = ALKONOST
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Calling SET_VEHICLE_USES_LARGE_REAR_RAMP on vehicle")
		SET_VEHICLE_USES_LARGE_REAR_RAMP(viPassed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetNine, ciFMMC_VEHICLE9_LowerCentreOfGravity)
		SET_CGOFFSET(viPassed, <<0.0, 0.0, -0.3>>)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Lowered centre of gravity")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTen, ciFMMC_VEHICLE10_DisableForPedNavigation)
		SET_VEHICLE_ACTIVE_FOR_PED_NAVIGATION(viPassed, FALSE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Vehicle disabled for ped navigation!")
	ENDIF
		
	SET_VEHICLE_ALL_CUSTOM_MODS(viPassed, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCustomModSettingsIndex)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTen, ciFMMC_VEHICLE10_EnableLoudRadio)
		SET_VEHICLE_RADIO_ENABLED(viPassed, TRUE)
		SET_VEHICLE_RADIO_LOUD(viPassed, TRUE)
		SET_VEH_RADIO_STATION(viPassed, GET_RADIO_STATION_NAME_FOR_EMITTER(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehRadio))
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(viPassed, "MP_Scripted_Mixgroup_Focus_Entity")
		SET_VEHICLE_ENGINE_ON(viPassed, TRUE, TRUE)
		SET_VEH_FORCED_RADIO_THIS_FRAME(viPassed)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTen, ciFMMC_VEHICLE10_SwitchRadioOff)		
		SET_VEH_RADIO_STATION(viPassed, "OFF")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_ALARMED)
		SET_VEHICLE_ALARM(viPassed, TRUE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - Vehicle alarmed!")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTen, ciFMMC_VEHICLE10_EnableAlternateHandling)
		SET_VEHICLE_USE_ALTERNATE_HANDLING(viPassed, TRUE)
		PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - SET_VEHICLE_USE_ALTERNATE_HANDLING!")
	ENDIF
	
	PRINTLN("[Vehicles][Vehicle ", iVeh, "] - MC_SET_UP_VEH - SET_VEHICLE_DIRT_LEVEL - ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fDirtLevel)
	SET_VEHICLE_DIRT_LEVEL(viPassed, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fDirtLevel)
	
	PROCESS_VEHICLE_INITIAL_CONTINUITY(iVeh, viPassed)
	
ENDPROC

FUNC BOOL CREATE_THIS_VEHICLE_CARGO(NETWORK_INDEX &niCargo, VEHICLE_INDEX veh, INT iVeh)	
	
	MODEL_NAMES mnCargo = GET_VEHICLE_CARGO_MODEL_FROM_INT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCargo, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn)
	STRING sBone = GET_VEHICLE_CARGO_BONE_FROM_INT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCargo, GET_ENTITY_MODEL(veh))
	
	IF mnCargo != DUMMY_MODEL_FOR_SCRIPT
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niCargo)
			IF CAN_REGISTER_MISSION_OBJECTS(1)
				REQUEST_MODEL(mnCargo)
				PRINTLN("CREATE_THIS_VEHICLE_CRATES sBone: ", sBone)
				IF HAS_MODEL_LOADED(mnCargo)
					MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
					IF CREATE_NET_OBJ(niCargo, mnCargo, GET_WORLD_POSITION_OF_ENTITY_BONE(veh, GET_ENTITY_BONE_INDEX_BY_NAME(veh, sBone)))
						OBJECT_INDEX oiCargo = NET_TO_OBJ(niCargo)
						ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(niCargo), veh, GET_ENTITY_BONE_INDEX_BY_NAME(veh, sBone), GET_CRATE_OFFSET_FOR_VEHICLE(veh, oiCargo), GET_CRATE_ROTATION_FOR_VEHICLE(veh, oiCargo), TRUE, FALSE, SHOULD_CRATE_COLLISION_BE_ENABLED(veh))
						SET_ENTITY_LOD_DIST(NET_TO_OBJ(niCargo), 100)
						SET_ENTITY_VISIBLE(NET_TO_OBJ(niCargo), FALSE)
						SET_MODEL_AS_NO_LONGER_NEEDED(mnCargo)
						PRINTLN("CREATE_THIS_VEHICLE_CRATES - Cargo created")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("CREATE_THIS_VEHICLE_CRATES - Crate exists")
			RETURN TRUE
		ENDIF
	ELSE
		PRINTLN("NO VEHICLE CRATES. return false")
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//////////////////////////////      MP  VEHICLES           //////////////////////////////
FUNC BOOL MC_CREATE_VEHICLES(NETWORK_INDEX &niVehicle[], NETWORK_INDEX &niVehicleCrate[])

	INT i
	//Don't stagger on fade transitions
	BOOL bStrand = IS_A_STRAND_MISSION_BEING_INITIALISED() AND (GET_STRAND_MISSION_TRANSITION_TYPE() != FMMC_BRANCHING_MISSION_TRANSITION_TYPE_FADEOUT)
	BOOL bVisible
	BOOL bCallAgain
	IF bStrand
		bVisible = TRUE
	ENDIF
	INT iRequestThisFrame
	INT iRandomHeadingToSelect, iSpawnInterior
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
			
			//Catch to detect blocked vehicles in UGC content
			IF g_sMPTunables.bENABLE_CREATOR_BLOCK
				IF NOT IS_CURRENT_MISSION_ROCKSTAR_CREATED()
				AND SHOULD_THIS_VEHICLE_BE_BLOCKED_FROM_THE_CREATORS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					ASSERTLN("[CREATOR_BLOCK] INVALID VEHICLE DETECTED SWITCHING MODEL TO NINEF")
					PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_CREATE_VEHICLES - INVALID VEHICLE DETECTED SWITCHING MODEL TO NINEF - Vehicle index: ", i, " Model: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn))
					g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = NINEF
				ENDIF
			ENDIF
			
			IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRespawnOnRule, -1)
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ENTITIES_CAN_RESPAWN_ON_RULE_CHANGE)
				SET_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_ENTITIES_CAN_RESPAWN_ON_RULE_CHANGE)
			ENDIF
			
			IF MC_SHOULD_VEH_SPAWN_AT_START(i, FALSE)
			AND NOT IS_BIT_SET(MC_ServerBD.iVehicleNotSpawningDueToRandomBS, i)
				IF NOT MC_HAS_VEHICLE_CREATION_FINISHED(niVehicle[i],niVehicleCrate[i], i)
					
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle[i])
						iRandomHeadingToSelect = -1
						iSpawnInterior = -1
						VECTOR vSpawnPos = MC_GET_VEH_SPAWN_LOCATION(i, iRandomHeadingToSelect, iSpawnInterior, TRUE)
						
						IF IS_VECTOR_ZERO(vSpawnPos)
						AND NOT IS_BIT_SET(MC_ServerBD.iVehicleNotSpawningDueToRandomBS, i)
							SET_BIT(MC_ServerBD.iVehicleNotSpawningDueToRandomBS, i)
						ENDIF
						
						IF IS_BIT_SET(MC_ServerBD.iVehicleNotSpawningDueToRandomBS, i)
							PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_CREATE_VEHICLES - not spawning vehicle due to random spawn at origin")
							RELOOP
						ENDIF
						
						IF NOT bStrand
							CLEAR_AREA_LEAVE_VEHICLE_HEALTH(vSpawnPos, 20.0, FALSE)
						ENDIF
						
						BOOL bFreezeWaitingOnCollision = TRUE
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetThree, ciFMMC_VEHICLE3_VehicleStartsAirborne)
							// Don't call SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION if the vehicle is being created in the air
							bFreezeWaitingOnCollision = FALSE
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetFive, ciFMMC_VEHICLE5_RESTART_WITH_TRAILER)
						AND g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i] != -1
						AND (IS_THIS_A_QUICK_RESTART_JOB() 
						OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetSix, ciFMMC_VEHICLE6_DONT_RESTART_WITH_TRAILER_MISSION_START)
							AND NOT FMMC_CHECKPOINTS_IS_ANY_RETRY_START_POINT_SET()
								PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_CREATE_VEHICLES - Mission start - Clearing iAttachedVehicleTrailers for Vehicle ", i)
								g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i] = -1
							ELSE
								IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle[g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i]])
								AND MC_SHOULD_VEH_SPAWN_AT_START(i, FALSE)
									PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_CREATE_VEHICLES - is waiting for Trailer: ", g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i], " To spawn.")
									RELOOP
								ENDIF
							ENDIF
						ENDIF
						
						IF FMMC_CREATE_NET_VEHICLE(niVehicle[i], g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn, vSpawnPos, MC_GET_VEH_SPAWN_HEADING(i, DEFAULT, iRandomHeadingToSelect, TRUE),
									DEFAULT, DEFAULT, DEFAULT, DEFAULT, bFreezeWaitingOnCollision, FALSE, DEFAULT, DEFAULT,
									SHOULD_VEHICLE_CREATION_IGNORE_GROUND_CHECK(i, vSpawnPos, iSpawnInterior), DEFAULT, TRUE) //Force Creation
							
							MC_SET_UP_VEH(NET_TO_VEH(niVehicle[i]), i, niVehicle, vSpawnPos, (iRandomHeadingToSelect > 0), iSpawnInterior, bVisible, GET_TOTAL_STARTING_PLAYERS())
							MC_serverBD_1.sCreatedCount.iNumVehCreated[0]++
							
							SET_ENTITY_LOAD_COLLISION_FLAG(NET_TO_VEH(niVehicle[i]), TRUE, FALSE)
														
							MC_PROCESS_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sWarpLocationSettings, CREATION_TYPE_VEHICLES, i, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSpawnGroup, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSpawnSubGroupBS, NET_TO_VEH(niVehicle[i]))	
							
							SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE(niVehicle[i], TRUE)
							iSpawnPoolVehIndex = -1
							PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_CREATE_VEHICLES - created vehicle at this position: ") NET_PRINT_VECTOR(vSpawnPos)NET_PRINT(" vehicle number: ") NET_PRINT_INT(i)  NET_NL()
							IF bStrand
								IF iRequestThisFrame >= PICK_INT(IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING(), ciFMMC_MAX_PER_FRAME_CREATES_AIRLOCK, ciFMMC_MAX_PER_FRAME_CREATES)
									RETURN FALSE
								ELSE
									iRequestThisFrame++
								ENDIF
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetSeven, ciFMMC_VEHICLE7_DYNAMIC_FACILITY_SPAWN_OVERRIDE)
							AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = STROMBERG
								PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_CREATE_VEHICLES - Incrementing the iFacilityPosVehSpawned to: ", (MC_ServerBD_4.iFacilityPosVehSpawned+1))
								MC_ServerBD_4.iFacilityPosVehSpawned++
							ENDIF
						ENDIF
					ELSE
						IF IS_NET_VEHICLE_DRIVEABLE(niVehicle[i])
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niVehicle[i])
								IF CREATE_THIS_VEHICLE_CARGO(niVehicleCrate[i], NET_TO_VEH(niVehicle[i]), i)
									PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_CREATE_VEHICLES - created vehicle crates for veh: ") NET_PRINT_INT(i)  NET_NL()
								ELSE
									NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION]      ---------->     FMMC VEHICLE WAITING FOR CRATES TO SPAWN    <----------     ", "FMMC veh: ") NET_PRINT_INT(i) NET_NL()
								ENDIF
								IF(STRAND_MISSION_MPH_HUM_KEY_MCS1_CUT_PLAYING()) // B* 2164633
									PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_CREATE_VEHICLES - Calling SET_VEHICLE_ON_GROUND_PROPERLY because STRAND_MISSION_MPH_HUM_KEY_MCS1_CUT_PLAYING()")
									SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(niVehicle[i]))
								ENDIF
								IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = TRAILERLARGE
									SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(niVehicle[i]))
								ENDIF
							ELSE
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(niVehicle[i])
							ENDIF
						ELSE
							NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION]      ---------->     FMMC VEHICLE IS NOT DRIVABLE    <----------     ", "FMMC veh: ") NET_PRINT_INT(i) NET_NL()
						ENDIF
					ENDIF
				ENDIF
			ELSE
				NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION]     ---------->     FMMC VEHICLE NOT SET TO SPAWN AT START     <----------     ", "FMMC veh: ") NET_PRINT_INT(i) NET_NL()
			ENDIF
		ENDIF
	ENDREPEAT

	//Check we have created the Truck
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles i
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle[i])
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
			IF MC_SHOULD_VEH_SPAWN_AT_START(i, FALSE)
			AND NOT IS_BIT_SET(MC_ServerBD.iVehicleNotSpawningDueToRandomBS, i)
				RETURN FALSE
			ENDIF
		ENDIF
		IF NETWORK_DOES_NETWORK_ID_EXIST(niVehicle[i])
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niVehicle[i])
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicleCrate[i])
				AND GET_VEHICLE_CARGO_MODEL_FROM_INT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCargo, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn) != DUMMY_MODEL_FOR_SCRIPT
					RETURN FALSE
				ENDIF
				IF NOT IS_ENTITY_DEAD(NET_TO_VEH(niVehicle[i]))
					IF DOES_VEHICLE_HAVE_ANY_WEAPON_MODS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
						PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_CREATE_VEHICLES - HAVE_VEHICLE_WEAPON_MODS_LOADED (3) - Attempting to load weapon vehicle mods for iVeh: ", i)
						IF NOT HAVE_VEHICLE_WEAPON_MODS_LOADED(NET_TO_VEH(niVehicle[i]), g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVModTurret, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVModArmour, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetFive, i, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVModAirCounter, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVModExhaust, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVModBombBay, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVModSpoiler)
							PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_CREATE_VEHICLES - Weapon mods not loaded for vehicle ", i)
							RETURN FALSE
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_CREATE_VEHICLES - Vehicle is dead! ", i)
				ENDIF
			ENDIF
		ENDIF
		
		IF MC_SHOULD_VEH_SPAWN_AT_START(i, FALSE)
		AND NOT IS_BIT_SET(MC_ServerBD.iVehicleNotSpawningDueToRandomBS, i)
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetFive, ciFMMC_VEHICLE5_RESTART_WITH_TRAILER)
			AND g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i] != -1
			AND (IS_THIS_A_QUICK_RESTART_JOB() 
			OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
				PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_CREATE_VEHICLES - RESPAWN_WITH_TRAILER - Vehicle ", g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i], " needs to be attached to me")
				
				IF IS_NET_VEHICLE_DRIVEABLE(niVehicle[g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i]])
					VEHICLE_INDEX trailer = NET_TO_VEH(niVehicle[g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i]])
					FLOAT fOffset = 4.0
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = HAULER
					OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = HAULER2
						fOffset = 8.55 // Length of the hauler
					ENDIF
					SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(niVehicle[i]), 8.0)
					SET_ENTITY_COORDS(trailer, GET_ENTITY_COORDS(NET_TO_VEH(niVehicle[i])) - (GET_ENTITY_FORWARD_VECTOR(NET_TO_VEH(niVehicle[i]))*fOffset))
					SET_ENTITY_HEADING(trailer, GET_ENTITY_HEADING(NET_TO_VEH(niVehicle[i])))
					SET_VEHICLE_ON_GROUND_PROPERLY(trailer, 8.0)
					ATTACH_VEHICLE_TO_TRAILER(NET_TO_VEH(niVehicle[i]), trailer)
					PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_CREATE_VEHICLES - RESPAWN_WITH_TRAILER - Trailer attached!")
					g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i] = -1
				ELSE
					PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_CREATE_VEHICLES - RESPAWN_WITH_TRAILER - Failed to attach trailer to me, its not spawned!")
				ENDIF								
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetFive, ciFMMC_VEHICLE5_RESTART_WITH_TRAILER)
			AND g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i] != -1
			AND (IS_THIS_A_QUICK_RESTART_JOB() 
			OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
			AND (MC_SHOULD_VEH_SPAWN_AT_START(i, FALSE)
			AND NOT IS_BIT_SET(MC_ServerBD.iVehicleNotSpawningDueToRandomBS, g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i]))
				PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_CREATE_VEHICLES - iVehicle: ", i, " is still waiting for trailer: ", g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i], " To be attached.")
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle[g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i]])
					PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_CREATE_VEHICLES - Trailer is not even Spawned yet!")
				ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(niVehicle[i])
			VEHICLE_INDEX vehTemp = NET_TO_VEH(niVehicle[i])
			IF NOT IS_ENTITY_DEAD(vehTemp)
				IF IS_ENTITY_IN_AIR(vehTemp)
					PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_CREATE_VEHICLES - Vehicle is in the air. iVeh", i)
					IF NOT IS_THIS_MODEL_A_PLANE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					AND NOT IS_THIS_MODEL_A_BOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					AND NOT IS_THIS_MODEL_A_HELI(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					AND NOT IS_THIS_MODEL_A_TRAIN(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					AND NOT IS_THIS_MODEL_AN_AMPHIBIOUS_CAR(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					AND NOT IS_THIS_MODEL_AN_AMPHIBIOUS_QUADBIKE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					AND NOT IS_THIS_MODEL_A_JETSKI(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
						PRINTLN("[MCSpawning][Vehicles][Vehicle ", i, "] - MC_CREATE_VEHICLES - Vehicle is NOT an air vehicle. Calling: SET_VEHICLE_ON_GROUND_PROPERLY on iVeh", i)
						SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(niVehicle[i]), 8.0)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	IF(bCallAgain)
		RETURN FALSE
	ENDIF
	FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
		g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i] = -1
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Trains (Utility and Helper Functions) 
// ##### Description: Process the creation of Mission Controller Trains.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC MC_SET_UP_TRAIN(INT iTrain, VEHICLE_INDEX viTrain)

	APPLY_TRAIN_MISSION_SPECIFIC_CONFIG(viTrain, iTrain)

ENDPROC

FUNC BOOL SHOULD_CLEANUP_TRAIN(INT iTrain)
	
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iTrain, eSGET_Train)
		PRINTLN("[Trains_Respawning][MCRespawnTrain ", iTrain, "] SHOULD_CLEANUP_TRAIN |  Train does not have suitable spawn group")
		RETURN TRUE
	ENDIF
	
	INT iCleanupTeam = g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iTrain_CleanupTeam
	INT iCleanupRule = g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iTrain_CleanupRule
	
	IF iCleanupTeam > -1
		IF GET_TEAM_CURRENT_RULE(iCleanupTeam) >= iCleanupRule
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iPlacedTrainBS, ciPLACED_TRAIN_BS__CLEANUP_AT_MIDPOINT)
				// Need it to be the midpoint
				IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iCleanupTeam], iCleanupRule)
					PRINTLN("[Trains_Respawning][MCRespawnTrain ", iTrain, "] SHOULD_CLEANUP_TRAIN | We're on or past the cleanup rule AND past the midpoint (Current Rule: ", GET_TEAM_CURRENT_RULE(iCleanupTeam), ", Cleanup Rule: ", iCleanupRule)
					RETURN TRUE
				ELSE
					PRINTLN("[Trains_Respawning][MCRespawnTrain ", iTrain, "] SHOULD_CLEANUP_TRAIN | We're on or past the cleanup rule, but we need the midpoint of the cleanup rule to be reached (Current Rule: ", GET_TEAM_CURRENT_RULE(iCleanupTeam), ", Cleanup Rule: ", iCleanupRule)
				ENDIF
			ELSE
				// Midpoint doesn't matter
				PRINTLN("[Trains_Respawning][MCRespawnTrain ", iTrain, "] SHOULD_CLEANUP_TRAIN | We're on or past the cleanup rule (Current Rule: ", GET_TEAM_CURRENT_RULE(iCleanupTeam), ", Cleanup Rule: ", iCleanupRule)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_TRAIN_SPAWN_AT_START(INT iTrain)
	
	IF MC_SHOULD_ENTITY_SPAWN_AT_START(CREATION_TYPE_TRAINS, iTrain, g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iTrain_AssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iTrain_AssociatedRule, g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iTrain_AssociatedRuleSpawnLimit, FALSE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_TRAIN_SPAWN_LATER(INT iTrain)
	
	IF MC_SHOULD_ENTITY_SPAWN_LATER(CREATION_TYPE_TRAINS, iTrain, g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iTrain_AssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iTrain_AssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iTrain_AssociatedRuleSpawnLimit, FALSE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_TRAIN_SPAWN_NOW(INT iTrain, BOOL bStart)
	
	IF SHOULD_CLEANUP_TRAIN(iTrain)
		RETURN FALSE
	ENDIF

	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iTrain, eSGET_Train)
		RETURN FALSE
	ENDIF
	
	IF bStart		
		RETURN MC_SHOULD_ENTITY_SPAWN_AT_START(CREATION_TYPE_TRAINS, iTrain, g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iTrain_AssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iTrain_AssociatedRule, g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iTrain_AssociatedRuleSpawnLimit, TRUE, DEFAULT, DEFAULT)
	ELSE
		RETURN HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_TRAINS, iTrain, g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iTrain_AssociatedRuleSpawnLimit, 
				g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iTrain_AssociatedTeam,
				g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iTrain_AssociatedRule,
				DEFAULT, DEFAULT, DEFAULT,
				g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].iAggroIndexBS_Entity_Train)
	ENDIF	
			
ENDFUNC

FUNC VECTOR GET_TRAIN_SPAWN_COORDS(INT iTrain)
	VECTOR vCoords = g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].vPlacedTrainPos
	VECTOR vCheckpointContinuityCoords = GET_COORDS_FOR_ENTITY_CHECKPOINT_CONTINUITY(ciENTITY_TYPE_TRAIN, iTrain)
	
	IF NOT IS_VECTOR_ZERO(vCheckpointContinuityCoords)
		vCoords = vCheckpointContinuityCoords
		PRINTLN("[Trains][Train ", iTrain, "] GET_TRAIN_SPAWN_COORDS | Using checkpoint continuity coords: ", vCoords)
	ENDIF
	
	RETURN vCoords
ENDFUNC

FUNC BOOL MC_CREATE_TRAINS(NETWORK_INDEX& niTrains[])
	
	INT iTrain
	INT iCreatedTrains = 0
	INT iRequiredTrains = g_FMMC_STRUCT_ENTITIES.iNumberOfTrains
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfTrains iTrain
	
		IF IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain].vPlacedTrainPos)
			RELOOP
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niTrain[iTrain])
			iCreatedTrains++
			RELOOP
		ENDIF
		
		IF NOT SHOULD_TRAIN_SPAWN_NOW(iTrain, TRUE)
			iRequiredTrains--
			RELOOP
		ENDIF
				
		IF FMMC_CREATE_NET_TRAIN(niTrains[iTrain], g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iTrain], GET_TRAIN_SPAWN_COORDS(iTrain))
			
			VEHICLE_INDEX viTrain = NET_TO_VEH(niTrains[iTrain])
			MC_SET_UP_TRAIN(iTrain, viTrain)
			
			SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE(niTrains[iTrain], TRUE, TRUE)
			
			MC_serverBD_1.sCreatedCount.iNumTrainsCreated++ 
			SET_ENTITY_LOAD_COLLISION_FLAG(viTrain, TRUE, FALSE)
			
			iCreatedTrains++
			
		ENDIF
	ENDREPEAT
	
	RETURN (iCreatedTrains = iRequiredTrains)
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Objects (Utility and Helper Functions) 
// ##### Description: Process the creation of Mission Controller Peds.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_OBJECT_CAN_BE_KILLED_ON_THIS_RULE(INT iObjectIndex, INT iTeam)
	
	IF iObjectIndex > -1
		IF MC_serverBD_4.iObjRule[iObjectIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_KILL
			IF iTeam = 0
				SET_BIT(MC_serverBD.iObjteamFailBitset[iObjectIndex],SBBOOL1_TEAM0_NEEDS_KILL)
			ELIF iTeam = 1
				SET_BIT(MC_serverBD.iObjteamFailBitset[iObjectIndex],SBBOOL1_TEAM1_NEEDS_KILL)
			ELIF iTeam = 2
				SET_BIT(MC_serverBD.iObjteamFailBitset[iObjectIndex],SBBOOL1_TEAM2_NEEDS_KILL)
			ELIF iTeam = 3
				SET_BIT(MC_serverBD.iObjteamFailBitset[iObjectIndex],SBBOOL1_TEAM3_NEEDS_KILL)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CAN_OBJ_STILL_SPAWN_IN_MISSION(INT iobj, INT iTeamOverride = -1, INT iRuleOverride = -1)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAssociatedObjective
					// The object will have a chance to spawn:
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAssociatedObjective
					// The object still has a chance to spawn:
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE // Spawn type won't prevent object from spawning after the rule
			RETURN TRUE
		ENDIF
	ELSE
		//Object will just spawn at mission start!
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iSecondAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iSecondAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iSecondAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iSecondAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iSecondAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iSecondAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iSecondAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iThirdAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iThirdAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iThirdAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iThirdAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iThirdAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iThirdAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iThirdAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iFourthAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iFourthAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iFourthAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iFourthAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iFourthAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iFourthAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iFourthAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL MC_DOES_OBJ_HAVE_PLAYER_VARIABLE_SETTING(INT iObj)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAssociatedTeam > -1
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSet, cibsOBJ_PlayerNumCreated)	
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_OBJECT(INT iObjectIndex, INT iStartingPlayers)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectIndex].iDontSpawnPlayerNum > 0
	AND (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectIndex].iDontSpawnPlayerNum + 1) = GET_TOTAL_STARTING_PLAYERS()
		PRINTLN("[MCSpawning][Objects][Object ", iObjectIndex, "] - MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_OBJECT - Not spawning as the creator blocking spawn of this object on this number of players")
		RETURN FALSE
	ELSE
		RETURN MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_ENTITY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectIndex].iSpawnPlayerNum, iStartingPlayers)
	ENDIF
ENDFUNC

FUNC BOOL MC_IS_OBJ_GOING_TO_SPAWN_LATER(INT i)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn = DUMMY_MODEL_FOR_SCRIPT
		RETURN FALSE
	ENDIF
	
	BOOL bSuitableSpawnGroup = MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_Object, DEFAULT, DEFAULT, TRUE)
		
	IF MC_WILL_ENTITY_CLEANUP_IMMEDIATELY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCleanupTeam, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCleanupObjective, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitset, cibsOBJ_CleanupAtMidpoint))
	OR MC_WILL_CHECKPOINTS_STOP_THIS_SPAWN(i, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitset, cibsOBJ_DontSpawnOnCheckpoint_0), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitset, cibsOBJ_DontSpawnOnCheckpoint_1), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSetTwo, cibsOBJ2_DontSpawnOnCheckpoint_2), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSetTwo, cibsOBJ2_DontSpawnOnCheckpoint_3), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitsetFour, cibsOBJ4_BlockSpawnIfNotACheckpoint))
	OR NOT bSuitableSpawnGroup
	OR NOT MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_OBJECT(i, GET_TOTAL_STARTING_PLAYERS())
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_TransitionSessionNonResetVars.iDestroyedTurretBitSet, i)
		PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_IS_OBJ_GOING_TO_SPAWN_LATER - iDestroyedTurretBitSet is set for object ", i)
		RETURN FALSE
	ENDIF
				
	IF MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].eSpawnConditionFlag, FALSE, TRUE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAggroIndexBS_Entity_Obj)
	 	PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_IS_OBJ_GOING_TO_SPAWN_LATER - Returning FALSE due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")
		RETURN FALSE
	ENDIF
	
	IF MC_ARE_SPAWN_CONDITION_FLAGS_MET_DURING_GAMEPLAY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].eSpawnConditionFlag)
		IF MC_SHOULD_ENTITY_SPAWN_AT_START(CREATION_TYPE_OBJECTS, i, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedObjective, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedSpawn, TRUE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedRuleForceAlwaysSpawnOnScore)
			PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_IS_OBJ_GOING_TO_SPAWN_LATER - Returning TRUE due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")			
			RETURN TRUE
		ELSE
			PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_IS_OBJ_GOING_TO_SPAWN_LATER - Returning FALSE due to not being set up to spawn at the start ....")
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjectTrainAttachmentData.iTrainIndex > -1
		IF SHOULD_PLACED_OBJECT_BE_PORTABLE(i)
			PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_IS_OBJ_GOING_TO_SPAWN_LATER - Train-attached pickup Objects do not spawn at the start")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF MC_IS_ENTITY_SPAWN_TRIGGERED_BY_ZONE_ENTRY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iZoneBlockEntitySpawn)
		PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_IS_OBJ_GOING_TO_SPAWN_LATER - MC_IS_ENTITY_SPAWN_TRIGGERED_BY_ZONE_ENTRY = TRUE")
		RETURN TRUE
	ENDIF
	
	IF MC_SHOULD_ENTITY_SPAWN_LATER(CREATION_TYPE_OBJECTS, i, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedObjective, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedSpawn, TRUE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedRuleForceAlwaysSpawnOnScore)		
		IF bSuitableSpawnGroup
			PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_IS_OBJ_GOING_TO_SPAWN_LATER - bSuitableSpawnGroup = TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL MC_SHOULD_OBJ_SPAWN_AT_START(INT i, BOOL bArrayCheck, BOOL bCheckTrainAttachment = TRUE)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn = DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_SHOULD_OBJ_SPAWN_AT_START - DUMMY_MODEL_FOR_SCRIPT object ", i)
		RETURN FALSE
	ENDIF
	
	IF NOT bArrayCheck
		IF MC_WILL_CHECKPOINTS_STOP_THIS_SPAWN(i, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitset, cibsOBJ_DontSpawnOnCheckpoint_0), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitset, cibsOBJ_DontSpawnOnCheckpoint_1), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSetTwo, cibsOBJ2_DontSpawnOnCheckpoint_2), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSetTwo, cibsOBJ2_DontSpawnOnCheckpoint_3), IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitsetFour, cibsOBJ4_BlockSpawnIfNotACheckpoint))
			PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_SHOULD_OBJ_SPAWN_AT_START - MC_WILL_CHECKPOINTS_STOP_THIS_SPAWN object ", i)
			RETURN FALSE
		ENDIF
		
		IF MC_WILL_ENTITY_CLEANUP_IMMEDIATELY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCleanupTeam, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCleanupObjective, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitset, cibsOBJ_CleanupAtMidpoint))
			PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_SHOULD_OBJ_SPAWN_AT_START - MC_WILL_ENTITY_CLEANUP_IMMEDIATELY object ", i)
			RETURN FALSE
		ENDIF
		
		IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_Object)
			PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_SHOULD_OBJ_SPAWN_AT_START - MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP object ", i)
			RETURN FALSE
		ENDIF
		
		IF NOT MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_OBJECT(i, GET_TOTAL_STARTING_PLAYERS())
			PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_SHOULD_OBJ_SPAWN_AT_START - MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_OBJECT object ", i)
			RETURN FALSE
		ENDIF
		
	ENDIF
	
	IF bCheckTrainAttachment
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjectTrainAttachmentData.iTrainIndex > -1
			IF SHOULD_PLACED_OBJECT_BE_PORTABLE(i)
				PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_SHOULD_OBJ_SPAWN_AT_START - Train-attached pickup Objects do not spawn at the start")
				RETURN FALSE
			ENDIF
			
			IF NOT IS_TRAIN_ATTACHMENT_READY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjectTrainAttachmentData)
				PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_SHOULD_OBJ_SPAWN_AT_START - Blocked by IS_TRAIN_ATTACHMENT_READY")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_TransitionSessionNonResetVars.iDestroyedTurretBitSet, i)
		PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_SHOULD_OBJ_SPAWN_AT_START - iDestroyedTurretBitSet is set for object ", i)
		RETURN FALSE
	ENDIF
	
	IF MC_DOES_OBJ_HAVE_PLAYER_VARIABLE_SETTING(i)
		IF (MC_serverBD_1.sCreatedCount.inumobjcreated[0] >= MC_serverBD.iNumStartingPlayers[g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedTeam]) OR (IS_FAKE_MULTIPLAYER_MODE_SET() AND MC_serverBD_1.sCreatedCount.inumobjcreated[0] >= 1)
			SET_BIT(MC_serverBD_1.sCreatedCount.iSkipObjBitset, i)
			PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_SHOULD_OBJ_SPAWN_AT_START - MC_DOES_OBJ_HAVE_PLAYER_VARIABLE_SETTING object ", i)
			RETURN FALSE
		ENDIF
	ENDIF
			
	IF MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].eSpawnConditionFlag, TRUE, FALSE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAggroIndexBS_Entity_Obj)
		PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_SHOULD_OBJ_SPAWN_AT_START - Returning FALSE due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")
		RETURN FALSE
	ENDIF
	
	IF MC_IS_ENTITY_SPAWN_TRIGGERED_BY_ZONE_ENTRY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iZoneBlockEntitySpawn)
		PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_SHOULD_OBJ_SPAWN_AT_START - MC_IS_ENTITY_SPAWN_TRIGGERED_BY_ZONE_ENTRY")
		RETURN FALSE
	ENDIF	
	
	IF MC_SHOULD_ENTITY_SPAWN_AT_START(CREATION_TYPE_OBJECTS, i, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedObjective, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedSpawn, NOT bArrayCheck, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedRuleForceAlwaysSpawnOnScore)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC CACHE_OBJ_SHOULD_CLEANUP_THIS_FRAME(INT i)
	SET_BIT(iObjShouldCleanupThisFrameBS, i)
ENDPROC

PROC CACHE_OBJ_SHOULD_NOT_CLEANUP_THIS_FRAME(INT i)
	SET_BIT(iObjShouldNotCleanupThisFrameBS, i)
ENDPROC

FUNC BOOL SHOULD_OBJECT_CLEANUP_BE_DELAYED(INT iObj)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCleanupDelay > 0
		IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_3.iObjSpawnDelay[iObj])
			PRINTLN("[Obj ", iObj, "] - SHOULD_CLEANUP_OBJ - Starting Delay.")
			MC_REINIT_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_3.iObjSpawnDelay[iObj])
		ENDIF
		
		IF NOT MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(MC_serverBD_3.iObjSpawnDelay[iObj], g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCleanupDelay)
			PRINTLN("[Obj ", iObj, "] - SHOULD_CLEANUP_OBJ - Waiting for delay to expire. Miliseconds: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_3.iObjSpawnDelay[iObj]), " / ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCleanupDelay)			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CLEANUP_OBJ(FMMC_OBJECT_STATE &sObjState)

	INT iObj = sObjState.iIndex

	IF IS_BIT_SET(iObjShouldCleanupThisFrameBS, iObj)
		RETURN TRUE
	ENDIF
	IF IS_BIT_SET(iObjShouldNotCleanupThisFrameBS, iObj)
		RETURN FALSE
	ENDIF
	
	IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSet, cibsOBJ_CleanupAtMissionEnd)
		CACHE_OBJ_SHOULD_CLEANUP_THIS_FRAME(iObj)
		RETURN TRUE
	ENDIF
	
	//Despawn options
	IF MC_WAS_DESPAWN_CONDITION_REACHED(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sCleanupData, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAggroIndexBS_Entity_Obj #IF IS_DEBUG_BUILD ,"object" #ENDIF )
		IF NOT SHOULD_OBJECT_CLEANUP_BE_DELAYED(iObj)
			PRINTLN("[SHOULD_CLEANUP_OBJ] Cleanup Obj ", iObj, " MC_WAS_DESPAWN_CONDITION_REACHED = TRUE")
			CACHE_OBJ_SHOULD_CLEANUP_THIS_FRAME(iObj)
			RETURN TRUE
		ELSE
			PRINTLN("[SHOULD_CLEANUP_OBJ] Cleanup Obj ", iObj, " MC_WAS_DESPAWN_CONDITION_REACHED = TRUE - Waiting on Delay.")
		ENDIF
	ENDIF
	
	BOOL bCheckEarlyCleanup
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCleanupObjective !=-1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCleanupTeam !=-1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCleanupObjective <= MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCleanupTeam]				
				bCheckEarlyCleanup = TRUE
			ENDIF
		ENDIF			
	ENDIF	
	
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iObj, eSGET_Object)
		PRINTLN("[SHOULD_CLEANUP_OBJ] Cleanup Obj ", iObj, " Obj no longer has a valid spawn group active.") 
		bCheckEarlyCleanup = TRUE
	ENDIF	
	
	IF !bCheckEarlyCleanup
		CACHE_OBJ_SHOULD_NOT_CLEANUP_THIS_FRAME(iObj)
		RETURN FALSE
	ENDIF
	
	// Reusing Spawn Delay to save on Broadcast Data.
	IF SHOULD_OBJECT_CLEANUP_BE_DELAYED(iObj)
		CACHE_OBJ_SHOULD_NOT_CLEANUP_THIS_FRAME(iObj)
		RETURN FALSE
	ENDIF
	
	IF (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitset, cibsOBJ_CleanupAtMidpoint)
	AND IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCleanupTeam], g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCleanupObjective))
	OR NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitset, cibsOBJ_CleanupAtMidpoint)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCleanupRange <= 0
			CACHE_OBJ_SHOULD_CLEANUP_THIS_FRAME(iObj)
			RETURN TRUE
		ELSE
			INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(sObjState.objIndex)
			PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
			PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
			
			VECTOR vObjCoord
			IF sObjState.bObjExists
				vObjCoord = GET_FMMC_OBJECT_COORDS(sObjState)
			ELSE
				vObjCoord = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vPos
			ENDIF
			
			IF GET_DIST2_BETWEEN_ENTITY_AND_COORD(playerPed, vObjCoord) > POW(TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCleanupRange), 2.0)
				CACHE_OBJ_SHOULD_CLEANUP_THIS_FRAME(iObj)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	CACHE_OBJ_SHOULD_NOT_CLEANUP_THIS_FRAME(iObj)
	RETURN FALSE

ENDFUNC

FUNC BOOL HAS_OBJ_RESPAWN_DELAY_EXPIRED(INT iObj)
	
	BOOL bExpired
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iRespawnDelay != 0
	AND (IS_BIT_SET(MC_serverBD_2.iObjSpawnedOnce, iObj) OR MC_ARE_SPAWN_CONDITION_FLAGS_MET_DURING_GAMEPLAY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].eSpawnConditionFlag) OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetFour, cibsOBJ4_AlwaysUseRespawnDelay))
		IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_3.iObjSpawnDelay[iobj])
			MC_REINIT_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_3.iObjSpawnDelay[iobj])
			PRINTLN("[HAS_OBJ_RESPAWN_DELAY_EXPIRED] start spawn delay for Obj ", iObj) 
		ELSE
			INT iTimeScale = 1000
			
			IF MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_3.iObjSpawnDelay[iobj]) >= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iRespawnDelay*iTimeScale
				PRINTLN("[HAS_OBJ_RESPAWN_DELAY_EXPIRED] spawn delay finished for Obj ", iObj) 
				bExpired = TRUE
			ENDIF
		ENDIF
	ELSE
		bExpired = TRUE
	ENDIF
	
	RETURN bExpired
	
ENDFUNC

PROC CACHE_OBJ_RESPAWN_BLOCKED(INT i)
	IF NOT IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iMiscBS, ciRandomSpawnGroupBS_HasBeenSet)
		EXIT
	ENDIF	
	SET_BIT(iObjRespawnIsBlockedBS, i)
	PRINTLN("[MCSpawning][Objects][Object ", i, "] SHOULD_OBJ_RESPAWN_NOW - CACHE_OBJ_RESPAWN_BLOCKED - Caching that the Obj has been blocked from respawning.")
ENDPROC

PROC CACHE_OBJ_SHOULD_NOT_RESPAWN_THIS_FRAME(INT i)
	SET_BIT(iObjRespawnIsBlockedThisFrameBS, i)
ENDPROC

PROC CACHE_OBJ_SHOULD_RESPAWN_THIS_FRAME(INT i)
	SET_BIT(iObjShouldRespawnNowBS, i)
ENDPROC

FUNC BOOL SHOULD_OBJ_RESPAWN_NOW(INT i, FMMC_OBJECT_STATE &sObjState)
	
	IF NOT CAN_WE_SPAWN_A_NETWORKED_OBJECT()
		PRINTLN("[MCRespawnObj ", i, "] SHOULD_OBJ_RESPAWN_NOW | Too many networked objects!!")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iObjRespawnIsBlockedBS, i)
		RETURN FALSE
	ENDIF	
	IF IS_BIT_SET(iObjRespawnIsBlockedThisFrameBS, i)
		RETURN FALSE
	ENDIF	
	IF IS_BIT_SET(iObjShouldRespawnNowBS, i)
		RETURN TRUE
	ENDIF
	
	FILL_FMMC_OBJECT_STATE_STRUCT(sObjState, i, TRUE)
	
	IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSetThree, cibsOBJ3_SpawnInSuddenDeath)
			PRINTLN("[MCSpawning][Objects][Object ", i, "] SHOULD_OBJ_RESPAWN_NOW - ciOptionsBS22_SpawnInSuddenDeath")
		ELSE
			CACHE_OBJ_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
			RETURN FALSE
		ENDIF
	ENDIF

	IF SHOULD_CLEANUP_OBJ(sObjState)
		PRINTLN("[MCSpawning][Objects][Object ", i, "] SHOULD_OBJ_RESPAWN_NOW - SHOULD_CLEANUP_OBJ = TRUE")
		CACHE_OBJ_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectAttachmentPed > -1
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectAttachmentPed])
			PRINTLN("[MCSpawning][Objects][Object ", i, "] SHOULD_OBJ_RESPAWN_NOW - Waiting for the attached ped (", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectAttachmentPed, ") to exist")
			CACHE_OBJ_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
			RETURN FALSE
		ENDIF
		
		IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectAttachmentPed])
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectAttachmentPed])
			PRINTLN("[MCSpawning][Objects][Object ", i, "] SHOULD_OBJ_RESPAWN_NOW - Waiting for the attached ped (", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectAttachmentPed, ") to be controlled by us")
			CACHE_OBJ_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjectTrainAttachmentData.iTrainIndex > -1
		
		IF IS_TRAIN_ATTACHMENT_READY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjectTrainAttachmentData)
			IF SHOULD_PLACED_OBJECT_BE_PORTABLE(i)
				// Portable objects are not actually attached, they are simply spawned in the expected position once the train has come to a stop and continue as normal
				IF NOT IS_TRAIN_PERFECTLY_STILL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjectTrainAttachmentData.iTrainIndex)
					PRINTLN("[MCSpawning][Objects][Object ", i, "] SHOULD_OBJ_RESPAWN_NOW - Waiting for the attached Train (", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjectTrainAttachmentData.iTrainIndex, ") to slow down to a stop")
					CACHE_OBJ_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
					RETURN FALSE
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[MCSpawning][Objects][Object ", i, "] SHOULD_OBJ_RESPAWN_NOW - Waiting for the attached Train (", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjectTrainAttachmentData.iTrainIndex, ") to be ready")
			CACHE_OBJ_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF MC_DOES_OBJ_HAVE_PLAYER_VARIABLE_SETTING(i)
		IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedObjective > -1
			AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedTeam > -1
				IF MC_serverBD_1.sCreatedCount.iNumObjCreated[g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedObjective] >= MC_serverBD.iNumberOfPart[g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedTeam]
					SET_BIT(MC_serverBD_1.sCreatedCount.iSkipObjBitset, i)
					PRINTLN("[MCSpawning][Objects][Object ", i, "] SHOULD_OBJ_RESPAWN_NOW - MC_DOES_OBJ_HAVE_PLAYER_VARIABLE_SETTING")	
					CACHE_OBJ_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].eSpawnConditionFlag, TRUE, FALSE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAggroIndexBS_Entity_Obj)
		IF MC_ARE_SPAWN_CONDITION_FLAGS_MET_DURING_GAMEPLAY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].eSpawnConditionFlag)
			CACHE_OBJ_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
		ELSE
			CACHE_OBJ_RESPAWN_BLOCKED(i)
		ENDIF
		
		PRINTLN("[MCSpawning][Objects][Object ", i, "] SHOULD_OBJ_RESPAWN_NOW - Returning FALSE due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")
		RETURN FALSE
	ENDIF
	
	IF NOT MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_OBJECT(i, GET_TOTAL_STARTING_PLAYERS())
		PRINTLN("[MCSpawning][Objects][Object ", i, "] SHOULD_OBJ_RESPAWN_NOW - Not enough players")
		CACHE_OBJ_RESPAWN_BLOCKED(i)
		RETURN FALSE
	ENDIF
	
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_Object)
		PRINTLN("[MCSpawning][Objects][Object ", i, "] SHOULD_OBJ_RESPAWN_NOW - No suitable spawn group")
		CACHE_OBJ_RESPAWN_BLOCKED(i)
		RETURN FALSE
	ENDIF
		
	IF MC_IS_ENTITY_SPAWN_TRIGGERED_BY_ZONE_ENTRY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iZoneBlockEntitySpawn)
		IF NOT HAS_ENTITY_SPAWN_ZONE_REQUIREMENT_BEEN_MET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iZoneBlockEntitySpawn, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iZoneBlockEntitySpawnPlayerReq)
			PRINTLN("[MCSpawning][Objects][Object ", i, "] SHOULD_OBJ_RESPAWN_NOW - RETURN FALSE, HAS_ENTITY_SPAWN_ZONE_REQUIREMENT_BEEN_MET = FALSE.. iZone: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iZoneBlockEntitySpawn, " iPlayerReq: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iZoneBlockEntitySpawnPlayerReq)
			CACHE_OBJ_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_OBJECTS, i, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedRuleForceAlwaysSpawnOnScore, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAggroIndexBS_Entity_Obj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedRuleAlwaysSpawnOnScoreEnd)
			CACHE_OBJ_SHOULD_RESPAWN_THIS_FRAME(i)
			RETURN TRUE
		ENDIF
	ELSE
		PRINTLN("[MCSpawning][Objects][Object ", i, "] SHOULD_OBJ_RESPAWN_NOW - SINCE NO ASSOCIATED RULES")
		CACHE_OBJ_SHOULD_RESPAWN_THIS_FRAME(i)
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_OBJECTS, i, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedRuleAlwaysForceSpawnOnScore, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAggroIndexBS_Entity_Obj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedRuleAlwaysSpawnOnScoreEnd)
			PRINTLN("[MCSpawning][Objects][Object ", i, "] SHOULD_OBJ_RESPAWN_NOW - TRUE HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS (1)")
			CACHE_OBJ_SHOULD_RESPAWN_THIS_FRAME(i)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_OBJECTS, i, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedRuleAlwaysForceSpawnOnScore, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAggroIndexBS_Entity_Obj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedRuleAlwaysSpawnOnScoreEnd)
			PRINTLN("[MCSpawning][Objects][Object ", i, "] SHOULD_OBJ_RESPAWN_NOW - TRUE HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS (2)")
			CACHE_OBJ_SHOULD_RESPAWN_THIS_FRAME(i)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_OBJECTS, i, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedRuleAlwaysForceSpawnOnScore, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAggroIndexBS_Entity_Obj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedRuleAlwaysSpawnOnScoreEnd)
			PRINTLN("[MCSpawning][Objects][Object ", i, "] SHOULD_OBJ_RESPAWN_NOW - TRUE HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS (3)")
			CACHE_OBJ_SHOULD_RESPAWN_THIS_FRAME(i)
			RETURN TRUE
		ENDIF
	ENDIF
	
	PRINTLN("[OBJSPAM] - (OBJECT ", i, ") || SHOULD_OBJ_RESPAWN_NOW returning FALSE")
	CACHE_OBJ_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
	RETURN FALSE

ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Objects 
// ##### Description: Process the creation of Mission Controller Objects.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC MC_PROCESS_OBJECT_CONTINUITY(OBJECT_INDEX oiPassed, INT iObj, FMMC_MISSION_CONTINUITY_VARS &sMissionContinuityVars)
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ObjectDestroyedTracking)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iContinuityId > -1
		AND IS_LONG_BIT_SET(sMissionContinuityVars.iObjectDestroyedBitset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iContinuityId)
			PRINTLN("[JS][CONTINUITY] - MC_PROCESS_OBJECT_CONTINUITY - obj: ", iObj, " was destroyed in a previous mission")
			IF GET_IS_ENTITY_A_FRAG(oiPassed)
				SET_DISABLE_FRAG_DAMAGE(oiPassed, FALSE)
			ENDIF
			SET_ENTITY_INVINCIBLE(oiPassed, FALSE)
			SET_ENTITY_HEALTH(oiPassed, 0)
		ENDIF
	ENDIF
ENDPROC

PROC MC_SET_OBJECT_SETTINGS_AFTER_CREATION(INT index, NETWORK_INDEX& niVeh[], OBJECT_INDEX obj, FMMC_CREATED_COUNT_DATA& sCount)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[index].mn = Prop_Contr_03b_LD
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[index].mn = Prop_Container_LD_PU
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[index].mn = prop_container_ld
		SET_ENTITY_LOD_DIST(obj, 500)
	ENDIF
	
	//If we're attaching a car into this container:
	FIND_AND_ATTACH_CAR_TO_CONTAINER(index, niVeh, obj)
	
	INT iAttachParentIndex = g_FMMC_STRUCT_ENTITIES.sPlacedObject[index].iAttachParent
	
	PRINTLN("[RCC MISSION][Object ", index, "][dsw] [MC_SET_OBJECT_SETTINGS_AFTER_CREATION] Object ", index, " has iAttachParentIndex = ", iAttachParentIndex)
	
	IF iAttachParentIndex != -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[index].iAttachParentType = CREATION_TYPE_VEHICLES
		AND NETWORK_DOES_NETWORK_ID_EXIST(niVeh[iAttachParentIndex])
			//We're attaching onto a flatbed!
			VEHICLE_INDEX vehFlatbed = NET_TO_VEH(niVeh[iAttachParentIndex])
			ATTACH_OBJECT_TO_FLATBED(obj, vehFlatbed, g_FMMC_STRUCT_ENTITIES.sPlacedObject[index].vAttachOffset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[index].vAttachOffsetRotation, TRUE)
		ENDIF
	ENDIF
	
	MC_PROCESS_OBJECT_CONTINUITY(obj, index, MC_serverBD_1.sMissionContinuityVars)
	
	SET_UP_TROLLEY_STARTING_AMOUNT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[index].mn, obj)
	
	sCount.iNumObjCreated[0]++
	
	#IF IS_DEBUG_BUILD
		NET_PRINT_TIME() NET_PRINT_STRINGS("     ---------->     FMMC MINI GAME OBJECT CREATED    <----------     ", "FMMC: ") NET_PRINT_INT(index) NET_NL()
	#ENDIF
ENDPROC

/// PURPOSE: This function takes in settings to do with a particular pickup and returns the Network Index
///    of a created pickup. 
FUNC NETWORK_INDEX MC_CREATE_PORTABLE_PICKUP_BASED_ON_SETTINGS(INT iObj, BOOL bOnGround, VECTOR vSpawnLoc, BOOL bInvincible = FALSE)
	OBJECT_INDEX	objPickup	= NULL
	NETWORK_INDEX 	niToReturn 	= NULL
	
	// Default this pickup to the on-ground version
	PICKUP_TYPE ePickupType	= PICKUP_PORTABLE_PACKAGE
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSet, cibsOBJ_Airborne)
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjectTrainAttachmentData.iTrainIndex > -1
		bOnGround = FALSE		// Ensure this isn't stuck to the ground if we want the pickup to be airborne
		PRINTLN("[Object ", iObj, "][Objects] MC_CREATE_PORTABLE_PICKUP_BASED_ON_SETTINGS - Setting bOnGround to FALSE")
	ENDIF
	
	// Currently only make a vehicle pickup if the pickup type is for planes
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iVehiclePickup = FMMC_PICKUP_ALL 
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSet, cibsOBJ_SnapToGround)
			ePickupType = PICKUP_PORTABLE_CRATE_UNFIXED_INCAR
			PRINTLN("[Object ", iObj, "][Objects] MC_CREATE_PORTABLE_PICKUP_BASED_ON_SETTINGS - Using type: PICKUP_PORTABLE_CRATE_UNFIXED_INCAR.")
		ELSE
			ePickupType = PICKUP_PORTABLE_DLC_VEHICLE_PACKAGE	// The new pickup type that can be picked up by vehicles
			PRINTLN("[Object ", iObj, "][Objects] MC_CREATE_PORTABLE_PICKUP_BASED_ON_SETTINGS - Using type: PICKUP_PORTABLE_DLC_VEHICLE_PACKAGE")
		ENDIF
	ENDIF
	
	IF DOES_PICKUP_NEED_BIGGER_RADIUS(iObj)
		PRINTLN("[Object ", iObj, "][Objects] MC_CREATE_PORTABLE_PICKUP_BASED_ON_SETTINGS - Using type: PICKUP_PORTABLE_PACKAGE_LARGE_RADIUS.")
		ePickupType	= PICKUP_PORTABLE_PACKAGE_LARGE_RADIUS
	ENDIF
	
	IF FMMC_CREATE_NET_PORTABLE_PICKUP(niToReturn, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn, vSpawnLoc, 0.0, ePickupType, TRUE, bOnGround, FALSE, TRUE)
		objPickup = NET_TO_OBJ(niToReturn)
		SET_WAIT_FOR_COLLISIONS_BEFORE_PROBE(objPickup, TRUE)
		
		IF bInvincible
			SET_ENTITY_INVINCIBLE(objPickup, TRUE)
			PRINTLN("[Object ", iObj, "][Objects] MC_CREATE_PORTABLE_PICKUP_BASED_ON_SETTINGS - Making pickup object invincible")
		ENDIF
		
		RETURN niToReturn
	ELSE
		PRINTLN("[Object ", iObj, "][Objects] MC_CREATE_PORTABLE_PICKUP_BASED_ON_SETTINGS - Failed to create portable pickup")
		
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC BOOL DOES_MC_OBJ_NEED_EXTENDED_LOD_DISTANCE(INT iObj)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("pil_Prop_Pilot_Icon_01"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("prop_ic_cp_bag"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("ar_prop_ar_cp_bag"))
	OR DOES_PICKUP_NEED_BIGGER_RADIUS(iObj)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_MC_OBJ_NEED_PRECISE_ROTATION(MODEL_NAMES mn)

	IF IS_THIS_MODEL_A_CCTV_CAMERA(mn)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_MC_OBJ_NEED_COORDS_TO_BE_SET_AFTER_CREATION(MODEL_NAMES mn)

	IF IS_MODEL_A_SEA_MINE(mn)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC ADD_OBJ_TO_EMITTER_ARRAY(INT iObj)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_EmitRadio)
		EXIT
	ENDIF
	
	OBJECT_INDEX oiObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(iObj)])
	
	IF NOT DOES_ENTITY_EXIST(oiObj)
		EXIT
	ENDIF
		
	INT iEmitter = FIND_A_FREE_EMITTER_FOR_OBJECT(iObj, RADIO_EMITTER_TYPE_OBJECT)
			
	IF iEmitter != -1
		MC_serverBD_3.sRadioEmitters[iEmitter].iRadioStation = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iRadioStation
		PRINTLN("ADD_OBJ_TO_EMITTER_ARRAY - Adding object ",iObj," to emitter system")
	ENDIF
	
ENDPROC

PROC MC_SET_UP_OBJ_PROOFS(OBJECT_INDEX oiPassed, INT iObj)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_OverrideDefaultObjectProofs)
		BOOL bBullet = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_BulletProofFlag)
		BOOL bFlame = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_FlameProofFlag)
		BOOL bExplosion = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_ExplosionProofFlag)
		BOOL bCollision = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_CollisionProofFlag)
		BOOL bMelee = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_MeleeProofFlag)
		BOOL bSteam = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_SteamProofFlag)
		BOOL bSmoke = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_SmokeProofFlag)
		SET_ENTITY_PROOFS(oiPassed, bBullet, bFlame, bExplosion, bCollision, bMelee, bSteam,DEFAULT, bSmoke)
		PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ_PROOFS - setting proofs: bullet ", bBullet, ", flame ", bFlame, ", explosion ", bExplosion, ", collision ", bCollision, ", melee ", bMelee, ", steam ", bSteam, ", smoke ", bSmoke)
	ELSE
		PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ_PROOFS - cibsOBJ3_OverrideDefaultObjectProofs is NOT SET for obj ", iObj)
		SET_ENTITY_PROOFS(oiPassed, FALSE, FALSE, FALSE, TRUE, TRUE)
	ENDIF
ENDPROC

PROC MC_SET_UP_OBJ(OBJECT_INDEX oiPassed, INT iObj, BOOL bOnGround = TRUE, bOOL bVisible = TRUE, BOOL bSecondary = FALSE, BOOL bSpawnNear = FALSE, BOOL bIsInitialSpawn = FALSE)
	
	PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ | Setting up object ", iObj, " / ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(oiPassed)))
	
	VECTOR vCoordsToUse
	FLOAT fHeadingToUse
	BOOL bOverrideCoords = DOES_MC_OBJ_NEED_COORDS_TO_BE_SET_AFTER_CREATION(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn)
	BOOL bOverrideHeading = FALSE
	BOOL bIgnoreSavedRotation = FALSE
	
	IF bOnGround
	AND NOT bSpawnNear
		vCoordsToUse = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos
		fHeadingToUse = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].fHead
		
		IF bSecondary
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vSecondSpawnPos)
			vCoordsToUse = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vSecondSpawnPos
			fHeadingToUse = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].fSecondHeading
			bOverrideCoords = TRUE
			bOverrideHeading = TRUE
		ENDIF
	ENDIF
		
	VECTOR vCheckpointPosition
	IF REPOSITION_COORD_BASED_ON_WARP_LOCATIONS_RANDOM_POOL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sWarpLocationSettings, CREATION_TYPE_OBJECTS, bIsInitialSpawn, vCheckpointPosition #IF IS_DEBUG_BUILD , iObj #ENDIF )			
		PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - Using Checkpoint Vector vCheckpointPosition: ", vCheckpointPosition)
		vCoordsToUse = vCheckpointPosition
		bOverrideCoords = TRUE
	ENDIF
	IF REPOSITION_HEADING_BASED_ON_WARP_LOCATIONS_RANDOM_POOL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sWarpLocationSettings, CREATION_TYPE_OBJECTS, bIsInitialSpawn, fHeadingToUse #IF IS_DEBUG_BUILD , iObj #ENDIF )			
		bOverrideHeading = TRUE
		PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - Grabbing random heading from pool for obj ", iobj, " iSpawnPoolObjIndex: ", iSpawnPoolObjIndex, " fHeadingToUse: ", fHeadingToUse)
	ENDIF
	
	IF ASSIGN_CHECKPOINT_POSITION_FROM_WARP_LOCATION_SETTINGS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sWarpLocationSettings, vCheckpointPosition #IF IS_DEBUG_BUILD , CREATION_TYPE_OBJECTS, iobj #ENDIF )
		PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - Using Checkpoint Vector vCheckpointPosition: ", vCheckpointPosition)
		vCoordsToUse = vCheckpointPosition
		bOverrideCoords = TRUE
	ENDIF
	FLOAT fCheckpointHeading
	IF ASSIGN_CHECKPOINT_HEADING_FROM_WARP_LOCATION_SETTINGS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sWarpLocationSettings, fCheckpointHeading  #IF IS_DEBUG_BUILD , CREATION_TYPE_OBJECTS, iobj #ENDIF )
		PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - Using Checkpoint Heading fCheckpointHeading: ", fCheckpointHeading)
		fHeadingToUse = fCheckpointHeading
		bOverrideHeading = TRUE
	ENDIF
	
	IF REPOSITION_ENTITY_RESPAWN_POSITION_FROM_WARP_LOCATION_JUST_USED(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sWarpLocationSettings, bIsInitialSpawn, CREATION_TYPE_OBJECTS, iObj, vCheckpointPosition)
		PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - Using Last Used Warp Pos: ", vCheckpointPosition)
		vCoordsToUse = vCheckpointPosition
		bOverrideCoords = TRUE
	ENDIF		
	IF REPOSITION_ENTITY_RESPAWN_HEADING_FROM_WARP_LOCATION_JUST_USED(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sWarpLocationSettings, bIsInitialSpawn, CREATION_TYPE_OBJECTS, iObj, fCheckpointHeading)
		PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - Using Last Used Warp Heading: ", fCheckpointHeading)
		fHeadingToUse = fCheckpointHeading
		bOverrideHeading = TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjectTrainAttachmentData.iTrainIndex != -1
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjectTrainAttachmentData.vCarriageOffsetPosition)		
		VECTOR vTempRot = <<0.0, 0.0, 0.0>>
		MC_GET_TRAIN_ATTACHMENT_POINT_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjectTrainAttachmentData, vCoordsToUse, vTempRot)
		bOverrideCoords = FALSE // Make sure we don't set the coords of this pickup at any point - setting coords on a pickup can make it get automatically "put on ground properly"
		bOverrideHeading = TRUE
		fHeadingToUse = vTempRot.z
		PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - Using coords & heading from train attachment: ", vCoordsToUse, " / ", fHeadingToUse)
	ENDIF
	
	IF REPOSITION_COORD_BASED_ON_FORCED_WARP_LOCATION_INDEX_ON_RESPAWN(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sWarpLocationSettings, CREATION_TYPE_OBJECTS, bIsInitialSpawn, vCheckpointPosition, iObj)
		PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - Using forced index Vector vCheckpointPosition: ", vCheckpointPosition)
		vCoordsToUse = vCheckpointPosition
		bOverrideCoords = TRUE
	ENDIF
	IF REPOSITION_HEADING_BASED_ON_FORCED_WARP_LOCATION_INDEX_ON_RESPAWN(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sWarpLocationSettings, CREATION_TYPE_OBJECTS, bIsInitialSpawn, fHeadingToUse, iObj)			
		bOverrideHeading = TRUE
		PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - forced index heading from pool fHeadingToUse: ", fHeadingToUse)
	ENDIF
	
	IF bOverrideCoords
		SET_ENTITY_COORDS_NO_OFFSET(oiPassed, vCoordsToUse)
		PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - Setting coords to ", vCoordsToUse, " using SET_ENTITY_COORDS_NO_OFFSET")
	ENDIF
	
	IF SHOULD_ENTITY_USE_CHECKPOINT_CONTINUITY_VARS(ciENTITY_TYPE_OBJECT, iObj)
		FLOAT fEntityCheckpointHeading = GET_HEADING_FOR_ENTITY_CHECKPOINT_CONTINUITY(ciENTITY_TYPE_OBJECT, iObj)
		// This object has got a heading saved from the last checkpoint!
		PRINTLN("[CONTINUITY][EntityCheckpointContinuity] - MC_SET_UP_OBJ - Object ", iObj, " using saved checkpoint heading ", fEntityCheckpointHeading)
		fHeadingToUse = fEntityCheckpointHeading
		
		IF IS_BIT_SET_FOR_ENTITY_CHECKPOINT_CONTINUITY(ciENTITY_TYPE_OBJECT, iObj, ciFMMCContinuityEntityBitset_FraggedObject)
			PRINTLN("[CONTINUITY][EntityCheckpointContinuity] - MC_SET_UP_OBJ - Object ", iObj, " getting fragged due to checkpoint continuity!")
			FRAG_ENTITY_COMPLETELY(oiPassed)
		ENDIF
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vRot)
	AND NOT bIgnoreSavedRotation
		SET_ENTITY_ROTATION(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vRot)
	ELIF bOverrideHeading
		SET_ENTITY_HEADING(oiPassed, fHeadingToUse)
	ENDIF
	
	SET_ENTITY_VISIBLE(oiPassed, bVisible)
	
	// Proofs!
	MC_SET_UP_OBJ_PROOFS(oiPassed, iObj)

	IF DOES_MC_OBJ_NEED_EXTENDED_LOD_DISTANCE(iObj)
		SET_ENTITY_LOD_DIST(oiPassed, 1999)
	ELSE
		SET_ENTITY_LOD_DIST(oiPassed, 150)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectHealth = -1
		SET_ENTITY_INVINCIBLE(oiPassed, TRUE)
		SET_ENTITY_PROOFS(oiPassed, TRUE, TRUE, TRUE, TRUE, TRUE)
		SET_ENTITY_HEALTH(oiPassed, 500)
		
	ELSE
		INT iHealthToUse = (GET_ENTITY_HEALTH(oiPassed) / 100) * g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectHealth
		PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - Setting object's health to ", iHealthToUse, " (", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectHealth, ")%")
		SET_ENTITY_HEALTH(oiPassed, iHealthToUse)
		SET_ENTITY_INVINCIBLE(oiPassed, FALSE)
		// iObjHealthOverride not required any more since the iObjectHealth refactor
		
		IF IS_SCRIPTED_FLAMMABLE_ENTITY_DAMAGE_SCRIPT_CONTROLLED(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn)
			PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - Setting object's max health to ", iHealthToUse)
			SET_ENTITY_MAX_HEALTH(oiPassed, iHealthToUse)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSet, cibsOBJ_Unfraggable)
	OR DOES_ENTITY_USE_CUSTOM_DEATH_EXPLOSION_PTFX(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn)
	OR IS_SCRIPTED_FLAMMABLE_ENTITY_DAMAGE_SCRIPT_CONTROLLED(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn)
		IF GET_IS_ENTITY_A_FRAG(oiPassed)
			PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - portable fraggable object ", iObj, " being made unfraggable!")
			SET_DISABLE_FRAG_DAMAGE(oiPassed, TRUE)
		ENDIF
	ENDIF
	
	IF SHOULD_PLACED_OBJECT_BE_PORTABLE(iObj)
		PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - portable object ", iObj, " being frozen in position!")
		FREEZE_ENTITY_POSITION(oiPassed, TRUE)
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSet, cibsOBJ_ShowChevron)
		PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - adding chevron to object")
		SET_PICKUP_OBJECT_ARROW_MARKER(oiPassed, TRUE)
	ENDIF
	
	IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iRespawnOnRule, -1)
		SET_ALLOW_MIGRATE_TO_SPECTATOR(oiPassed, TRUE)
	ENDIF
	
	IF IS_OBJECT_A_PICKUP(oiPassed)
	OR IS_OBJECT_A_PORTABLE_PICKUP(oiPassed)
		
		PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - Object ", iObj, " is a pickup/portable pickup, so performing pickup setup now!")
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciALPHA_OUT_PACKAGES_NOT_COLLECTABLE)
			PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - ciALPHA_OUT_PACKAGES_NOT_COLLECTABLE")
			SET_PICKUP_OBJECT_TRANSPARENT_WHEN_UNCOLLECTABLE(oiPassed, TRUE)
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].fObjectStrictPickupDistance > 0.05
			PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - Calling SET_PICKUP_OBJECT_GLOW_WHEN_UNCOLLECTABLE and ALLOW_PICKUP_ARROW_MARKER_WHEN_UNCOLLECTABLE due to fObjectStrictPickupDistance")
			SET_PICKUP_OBJECT_GLOW_WHEN_UNCOLLECTABLE(oiPassed, TRUE)
			ALLOW_PICKUP_ARROW_MARKER_WHEN_UNCOLLECTABLE(oiPassed, TRUE)
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed > -1
		AND NOT bOnGround
			ATTACH_PORTABLE_PICKUP_TO_PED(oiPassed, NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed]))
			FMMC_SET_LONG_BIT(MC_serverBD_4.iPedHasAttachedObject, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectAttachmentPed)
			PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - Attaching obj: ", iObj, " to ped: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectAttachmentPed)
		ENDIF		

		INT iTeamLoop
		FOR iTeamLoop = 0 TO (g_FMMC_STRUCT.iNumberOfTeams - 1)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iPriority[iTeamLoop] > 0
			OR NOT DOES_OBJECT_RULE_TYPE_REQUIRE_PICKUP(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iRule[iTeamLoop])
				PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - SET_TEAM_PICKUP_OBJECT called with FALSE on team ", iTeamLoop, " for object ", iObj)
				SET_TEAM_PICKUP_OBJECT(oiPassed, iTeamLoop, FALSE)
			ENDIF
		ENDFOR
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciFLASH_PICKUPS_FOR_ALL_TEAMS)
			PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - Turning on flashing pickup on same team for object ", iObj)
			SET_OBJECT_GLOW_IN_SAME_TEAM(oiPassed)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSet, cibsOBJ_ShowChevron)
			PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - CREATE_FMMC_OBJECTS_MP_RC - adding chevron to object")
			SET_PICKUP_OBJECT_ARROW_MARKER(oiPassed, TRUE)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetFour, cibsOBJ4_SuppressPickupSound)
			PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - Setting SUPPRESS_PICKUP_SOUND_FOR_PICKUP on pickup ", iObj)
			SUPPRESS_PICKUP_SOUND_FOR_PICKUP(oiPassed, TRUE)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSet, cibsOBJ_Reset_If_Dropped_Out_Of_Bounds)
			PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - Caching an initial safe drop pos!")
			CACHE_OBJECT_SAFE_DROP_POSITION(iObj, GET_ENTITY_COORDS(oiPassed), TRUE)
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_OnlyDamagedByPlayers)
		PRINTLN("[Objects][Object ", iObj, "][RCC MISSION] MC_SET_UP_OBJ - Making object only damagable by players: ", iObj)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(oiPassed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_CanBeLockedOn)
		SET_OBJECT_TARGETTABLE(oiPassed, TRUE)
		SET_ENTITY_IS_TARGET_PRIORITY(oiPassed, TRUE, 50)
		PRINTLN("[Objects][Object ", iObj, "][RCC MISSION] - MC_SET_UP_OBJ - Setting object ", iObj, " as priority target")
	ENDIF
	
	IF DOES_MC_OBJ_NEED_PRECISE_ROTATION(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn)
		IF NETWORK_IS_ACTIVITY_SESSION()
			NETWORK_ENTITY_USE_HIGH_PRECISION_ROTATION(OBJ_TO_NET(oiPassed), TRUE)
			PRINTLN("[Objects][Object ", iObj, "][CCTV] MC_SET_UP_OBJ - Setting NETWORK_ENTITY_USE_HIGH_PRECISION_ROTATION on obj ", iObj)
		ENDIF
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vInteriorCoords)
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iInterior != -1
	
		INTERIOR_INSTANCE_INDEX tempInterior
		
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vInteriorCoords)
			PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - object ", iObj, " has interior coords ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vInteriorCoords, " & typehash ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iInteriorTypeHash)
			tempInterior = GET_INTERIOR_AT_COORDS_WITH_TYPEHASH(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vInteriorCoords, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iInteriorTypeHash)
		ELSE
			PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - object ", iObj, " has iInterior ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iInterior)
			tempInterior = INT_TO_NATIVE(INTERIOR_INSTANCE_INDEX, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iInterior)
		ENDIF
		
		IF IS_VALID_INTERIOR(tempInterior)
			PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - object ", iObj, " RETAIN_ENTITY_IN_INTERIOR called on interior ", NATIVE_TO_INT(tempInterior))
			RETAIN_ENTITY_IN_INTERIOR(oiPassed, tempInterior)
		ELSE
			PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - object ", iObj, " supposed to be retained in interior ", NATIVE_TO_INT(tempInterior), " but it isn't valid!")
		ENDIF
	ENDIF
	
	IF SHOULD_DRONE_SYSTEMS_BE_ACTIVE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjDroneData)
		PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - object ", iObj, " Drone frozen waiting for collision.")
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(oiPassed, TRUE)
		PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - object ", iObj, " Drone SET_CAN_CLIMB_ON_ENTITY = FALSE")		
		SET_CAN_CLIMB_ON_ENTITY(oiPassed, FALSE)
	ENDIF
	
	IF DOES_MODEL_HAVE_A_SHARED_RENDER_TARGET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn)
		BROADCAST_FMMC_SHARED_RENDERTARGET_EVENT(ciSHARED_RT_EVENT_TYPE__REGISTER_RENDERTARGET, ciENTITY_TYPE_OBJECT, iObj, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn)
	ENDIF
		
	MC_PROCESS_OBJECT_CONTINUITY(oiPassed, iObj, MC_serverBD_1.sMissionContinuityVars)
	
	MC_SET_ID_INT_DECOR_ON_ENTITY(oiPassed, GET_OBJECT_NET_ID_INDEX(iObj))
	
	ADD_OBJ_TO_EMITTER_ARRAY(iObj)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFive, cibsOBJ5_CantBeDamagedByPlayers)
		PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - Object ", iObj, " Setting can't be damaged by players.")
		SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(oiPassed, FALSE, rgfm_PlayerTeam[0])
	ENDIF
	
	IF CAN_SET_MODEL_TINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn)
		PRINTLN("[Objects][Object ", iObj, "] MC_SET_UP_OBJ - Setting TINT g_FMMC_STRUCT_ENTITIES.sPlacedObject[", iObj, "] with tint index ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectColour)
		SET_OBJECT_TINT_INDEX(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectColour)
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Interactables
// ##### Description: Functions for creating and setting up placed Interactables
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL CAN_INTERACTABLE_STILL_SPAWN_IN_MISSION(INT iInteractable, INT iTeamOverride = -1, INT iRuleOverride = -1)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_AssociatedRule > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_AssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_AssociatedRuleSpawnLimit = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_AssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_AssociatedRule
					// The Interactable will have a chance to spawn:
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_AssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_AssociatedRule
					// The Interactable still has a chance to spawn:
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE // Spawn type won't prevent Interactable from spawning after the rule
			RETURN TRUE
		ENDIF
	ELSE
		//Interactable will just spawn at mission start!
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_CLEANUP_INTERACTABLE(INT iInteractable)
	
	IF IS_INTERACTABLE_OCCUPIED(iInteractable)
		// Don't despawn it if somebody is currently using it
		RETURN FALSE
	ENDIF
	
	INT iCleanupTeam = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_CleanupTeam
	INT iCleanupRule = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_CleanupRule
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_CleanupTeam > -1
		IF GET_TEAM_CURRENT_RULE(iCleanupTeam) >= iCleanupRule
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractableBS, ciInteractableBS_CleanupAtMidpoint)
				// Need it to be the midpoint
				IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iCleanupTeam], iCleanupRule)
					PRINTLN("[Interactables_Respawning][MCSpawning][Interactables][Interactable ", iInteractable, "] SHOULD_CLEANUP_INTERACTABLE | We're on or past the cleanup rule AND past the midpoint (Current Rule: ", GET_TEAM_CURRENT_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_CleanupTeam), ", Cleanup Rule: ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_CleanupRule)
					RETURN TRUE
				ELSE
					PRINTLN("[Interactables_Respawning][MCSpawning][Interactables][Interactable ", iInteractable, "] SHOULD_CLEANUP_INTERACTABLE | We're on or past the cleanup rule, but we need the midpoint of the cleanup rule to be reached (Current Rule: ", GET_TEAM_CURRENT_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_CleanupTeam), ", Cleanup Rule: ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_CleanupRule)
				ENDIF
			ELSE
				// Midpoint doesn't matter
				PRINTLN("[Interactables_Respawning][MCSpawning][Interactables][Interactable ", iInteractable, "] SHOULD_CLEANUP_INTERACTABLE | We're on or past the cleanup rule (Current Rule: ", GET_TEAM_CURRENT_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_CleanupTeam), ", Cleanup Rule: ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_CleanupRule)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iInteractable, eSGET_Interactable)
		PRINTLN("[Interactables_Respawning][MCSpawning][Interactables][Interactable ", iInteractable, "] SHOULD_CLEANUP_INTERACTABLE | Interactable no longer has a valid spawn group active.") 
		RETURN TRUE
	ENDIF	
		
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_INTERACTABLE_SPAWN_NOW(INT iInteractable, BOOL bMidMissionCheck = FALSE)
	
	IF MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].eSpawnConditionFlag, FALSE, FALSE, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAggroIndexBS_Entity_Interactable, MC_PlayerBD[iPartToUse].iTeam)
		PRINTLN("[Interactables_Respawning][MCSpawning][Interactables][Interactable ", iInteractable, "] SHOULD_INTERACTABLE_SPAWN_NOW | Returning FALSE due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")		
		RETURN FALSE
	ENDIF
	
	IF SHOULD_CLEANUP_INTERACTABLE(iInteractable)
		RETURN FALSE
	ENDIF
	
	IF NOT MC_ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_ENTITY(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_RequiredPlayers, GET_TOTAL_STARTING_PLAYERS())
		PRINTLN("[Interactables_Respawning][MCSpawning][Interactables][Interactable ", iInteractable, "] SHOULD_INTERACTABLE_SPAWN_NOW | Not enough players! || Players: ", GET_TOTAL_STARTING_PLAYERS() , "/", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_RequiredPlayers)
		RETURN FALSE
	ENDIF
	
	IF bMidMissionCheck
		IF g_bMissionEnding
			PRINTLN("[Interactables_Respawning][MCSpawning][Interactables][Interactable ", iInteractable, "] SHOULD_INTERACTABLE_SPAWN_NOW | (Mid-Mission) Returning FALSE due to g_bMissionEnding")		
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT CAN_WE_SPAWN_A_NETWORKED_OBJECT()
		PRINTLN("[Interactables_Respawning][MCSpawning][Interactables][Interactable ", iInteractable, "] SHOULD_INTERACTABLE_SPAWN_NOW | Too many networked objects!!")
		RETURN FALSE
	ENDIF

	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iInteractable, eSGET_Interactable)
		PRINTLN("[Interactables_Respawning][MCSpawning][Interactables][Interactable ", iInteractable, "] SHOULD_INTERACTABLE_SPAWN_NOW - RETURN FALSE, Interactable does not have suitable spawn group")
		RETURN FALSE
	ENDIF
		
	PRINTLN("[Interactables_Respawning][MCSpawning][Interactables][Interactable ", iInteractable, "] SHOULD_INTERACTABLE_SPAWN_NOW | iInteractable_AssociatedRuleSpawnLimit: ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_AssociatedRuleSpawnLimit, " / iInteractable_AssociatedTeam: ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_AssociatedTeam, " / iInteractable_AssociatedRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_AssociatedRule)
	RETURN HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_INTERACTABLE, iInteractable, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_AssociatedRuleSpawnLimit, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_AssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_AssociatedRule, DEFAULT, DEFAULT, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAggroIndexBS_Entity_Interactable)
ENDFUNC

PROC INIT_INTERACTABLE_DOOR_CONNECTIONS(INT iInteractable)
	INT iDoor
	FOR iDoor = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDoors - 1
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_DoorsToLockBS, iDoor)
			SET_BIT(iDoorHasLinkedEntityBS, iDoor)
			PRINTLN("[INIT_SERVER][Interactables][Interactable ", iInteractable, "][Doors][Door ", iDoor, "] INIT_INTERACTABLE_DOOR_CONNECTIONS - Interactable: ", iInteractable, " Linked to Door ", iDoor, " by iInteractable_DoorsToLockBS")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_DoorsToUnlockBS, iDoor)
			SET_BIT(iDoorHasLinkedEntityBS, iDoor)
			PRINTLN("[INIT_SERVER][Interactables][Interactable ", iInteractable, "][Doors][Door ", iDoor, "] INIT_INTERACTABLE_DOOR_CONNECTIONS - Interactable: ", iInteractable, " Linked to Door ", iDoor, " by iInteractable_DoorsToUnlockBS")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_DoorsToSwitchConfigBS, iDoor)
			SET_BIT(iDoorHasLinkedEntityBS, iDoor)
			PRINTLN("[INIT_SERVER][Interactables][Interactable ", iInteractable, "][Doors][Door ", iDoor, "] INIT_INTERACTABLE_DOOR_CONNECTIONS - Interactable: ", iInteractable, " Linked to Door ", iDoor, " by iInteractable_DoorsToSwitchConfigBS")
		ENDIF
	ENDFOR
ENDPROC

FUNC VECTOR MC_GET_INTERACTABLE_POSITION(INT iInteractable, BOOL bIsInitialSpawn)
	VECTOR vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].vInteractable_Position
		
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableTrainAttachmentData.iTrainIndex != -1
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableTrainAttachmentData.vCarriageOffsetPosition)		
		VECTOR vTemp = <<0.0, 0.0, 0.0>>
		MC_GET_TRAIN_ATTACHMENT_POINT_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableTrainAttachmentData, vSpawnLoc, vTemp)		
		RETURN vSpawnLoc
	ENDIF
	
	REPOSITION_COORD_BASED_ON_WARP_LOCATIONS_RANDOM_POOL(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sWarpLocationSettings, CREATION_TYPE_INTERACTABLE, bIsInitialSpawn, vSpawnLoc #IF IS_DEBUG_BUILD , iInteractable #ENDIF )
		
	ASSIGN_CHECKPOINT_POSITION_FROM_WARP_LOCATION_SETTINGS(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sWarpLocationSettings, vSpawnLoc #IF IS_DEBUG_BUILD , CREATION_TYPE_INTERACTABLE, iInteractable #ENDIF )	
	
	REPOSITION_ENTITY_RESPAWN_POSITION_FROM_WARP_LOCATION_JUST_USED(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sWarpLocationSettings, bIsInitialSpawn, CREATION_TYPE_INTERACTABLE, iInteractable, vSpawnLoc)
	
	REPOSITION_COORD_BASED_ON_FORCED_WARP_LOCATION_INDEX_ON_RESPAWN(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sWarpLocationSettings, CREATION_TYPE_INTERACTABLE, bIsInitialSpawn, vSpawnLoc, iInteractable)	
	
	RETURN vSpawnLoc
ENDFUNC

FUNC FLOAT MC_GET_INTERACTABLE_HEADING(INT iInteractable, BOOL bIsInitialSpawn)
	FLOAT fSpawnHead = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].fInteractable_Heading
			
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableTrainAttachmentData.iTrainIndex != -1
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableTrainAttachmentData.vCarriageOffsetPosition)		
		VECTOR vTemp = <<0.0, 0.0, 0.0>>
		VECTOR vTempRotation = <<0.0, 0.0, 0.0>>
		MC_GET_TRAIN_ATTACHMENT_POINT_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableTrainAttachmentData, vTemp, vTempRotation)		
		fSpawnHead = vTempRotation.z
		RETURN fSpawnHead
	ENDIF
	
	REPOSITION_HEADING_BASED_ON_WARP_LOCATIONS_RANDOM_POOL(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sWarpLocationSettings, CREATION_TYPE_INTERACTABLE, bIsInitialSpawn, fSpawnHead #IF IS_DEBUG_BUILD , iInteractable #ENDIF )
	
	ASSIGN_CHECKPOINT_HEADING_FROM_WARP_LOCATION_SETTINGS(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sWarpLocationSettings, fSpawnHead #IF IS_DEBUG_BUILD , CREATION_TYPE_INTERACTABLE, iInteractable #ENDIF )
		
	REPOSITION_ENTITY_RESPAWN_HEADING_FROM_WARP_LOCATION_JUST_USED(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sWarpLocationSettings, bIsInitialSpawn, CREATION_TYPE_INTERACTABLE, iInteractable, fSpawnHead)
	
	REPOSITION_HEADING_BASED_ON_FORCED_WARP_LOCATION_INDEX_ON_RESPAWN(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sWarpLocationSettings, CREATION_TYPE_INTERACTABLE, bisInitialSpawn, fSpawnHead, iInteractable)
	
	RETURN fSpawnHead	
ENDFUNC

PROC PROCESS_INTERACTABLE_COMPLETION_MODEL_SWAP(INT iInteractable, OBJECT_INDEX oiInteractable)

	IF FMMC_IS_LONG_BIT_SET(iInteractable_CompletionModelSwapRegisteredBS, iInteractable)
		EXIT
	ENDIF
	
	INT iModelSwapHash = GET_INTERACTABLE_COMPLETION_MODEL_SWAP_HASH(iInteractable)
	
	IF GET_ACTIVE_MODEL_SWAP_INDEX(iModelSwapHash) > -1
		PRINTLN("[ModelSwaps][Interactables][Interactable ", iInteractable, "] PROCESS_INTERACTABLE_COMPLETION_MODEL_SWAP | Model swap ", iModelSwapHash, " is already registered!")
		FMMC_SET_LONG_BIT(iInteractable_CompletionModelSwapRegisteredBS, iInteractable)
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_fingerkeypad_01a"))
		IF REGISTER_SIMPLE_MODEL_SWAP(iModelSwapHash, OBJ_TO_NET(oiInteractable), INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_fingerkeypad_01b")))
			PRINTLN("[ModelSwaps][Interactables][Interactable ", iInteractable, "] PROCESS_INTERACTABLE_COMPLETION_MODEL_SWAP | Successfully registered keypad model swap! Hash: ", iModelSwapHash)
			FMMC_SET_LONG_BIT(iInteractable_CompletionModelSwapRegisteredBS, iInteractable)
		ENDIF
	
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_InteractWithAnimPreset = ciINTERACT_WITH_PRESET__DEAL_WITH_CHAIN_LOCK
	AND g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_InteractWithAltAnimSet = ciIW_ALTANIMSET__DEALWITHCHAIN_CUTTER_AND_CONTAINER
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_AttachedEntityIndex = -1
			PRINTLN("[Interactables_SPAM][Interactable_SPAM ", iInteractable, "] PROCESS_INTERACTABLE_COMPLETION_MODEL_SWAP | No assigned dynoprop!")
			EXIT
		ENDIF
		
		IF DOES_ENTITY_EXIST(GET_FMMC_ENTITY_DYNOPROP(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_AttachedEntityIndex))
			FMMC_MODEL_SWAP_CONFIG sContainerSwapConfig
			sContainerSwapConfig.iModelSwapHash = iModelSwapHash
			sContainerSwapConfig.sEntityToSwapOut.eModelSwapType = MODEL_SWAP_ENTITY_TYPE__NETWORKED_OBJECT
			sContainerSwapConfig.sEntityToSwapOut.niEntityNetID = OBJ_TO_NET(GET_FMMC_ENTITY_DYNOPROP(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_AttachedEntityIndex))
			
			sContainerSwapConfig.sEntityToSwapIn.eModelSwapType = MODEL_SWAP_ENTITY_TYPE__MODEL_SWAP_PROP
			sContainerSwapConfig.sEntityToSwapIn.mnModelSwapPropModelName = INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_Cont_Coll_01a"))
			
			SET_BIT(sContainerSwapConfig.iModelSwapBS, ciMODEL_SWAP_BS__COLLISION_ONLY)
			
			IF REGISTER_MODEL_SWAP(sContainerSwapConfig)
				PRINTLN("[ModelSwaps][Interactables][Interactable ", iInteractable, "] PROCESS_INTERACTABLE_COMPLETION_MODEL_SWAP | Successfully registered Container model swap! Hash: ", iModelSwapHash)
				FMMC_SET_LONG_BIT(iInteractable_CompletionModelSwapRegisteredBS, iInteractable)
			ENDIF
		ELSE
			PRINTLN("[Interactables_SPAM][Interactable_SPAM ", iInteractable, "] PROCESS_INTERACTABLE_COMPLETION_MODEL_SWAP | Attached dynoprop doesn't exist!")
		ENDIF
	ENDIF
	
ENDPROC

PROC TRIGGER_INTERACTABLE_COMPLETION_MODEL_SWAP(INT iInteractable)
	INT iModelSwapHash = GET_INTERACTABLE_COMPLETION_MODEL_SWAP_HASH(iInteractable)
	START_MODEL_SWAP(iModelSwapHash)
ENDPROC

PROC MC_SET_UP_INTERACTABLE(INT iInteractable, BOOL bInitialSpawn)

	PRINTLN("[Interactables][Interactable ", iInteractable, "] MC_SET_UP_INTERACTABLE | Setting up Interactable ", iInteractable)
	OBJECT_INDEX oiInteractable = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_INTERACTABLE_NET_ID_INDEX(iInteractable)])
	MC_SET_ID_INT_DECOR_ON_ENTITY(oiInteractable, GET_INTERACTABLE_NET_ID_INDEX(iInteractable))
		
	SET_UP_INTERACTABLE_ROTATION(iInteractable, oiInteractable, MC_GET_INTERACTABLE_HEADING(iInteractable, bInitialSpawn))
	
	IF SHOULD_INTERACTABLE_BE_INVINCIBLE(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable])
		PRINTLN("[Interactables][Interactable ", iInteractable, "] MC_SET_UP_INTERACTABLE | Interactable is invincible")
		SET_INTERACTABLE_INVINCIBILITY(oiInteractable, TRUE)
		SET_ENTITY_PROOFS(oiInteractable, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, DEFAULT, TRUE)
		
	ELSE
		PRINTLN("[Interactables][Interactable ", iInteractable, "] MC_SET_UP_INTERACTABLE | Interactable has ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_Health, " health")
		SET_INTERACTABLE_INVINCIBILITY(oiInteractable, FALSE)
		SET_ENTITY_HEALTH(oiInteractable, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_Health)
		SET_ENTITY_MAX_HEALTH(oiInteractable, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_Health)
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractableBS, ciInteractableBS_OverrideDefaultProofs)
			BOOL bBullet = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractableBS, ciInteractableBS_BulletProofFlag)
			BOOL bFlame = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractableBS, ciInteractableBS_FlameProofFlag)
			BOOL bExplosion = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractableBS, ciInteractableBS_ExplosionProofFlag)
			BOOL bCollision = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractableBS, ciInteractableBS_CollisionProofFlag)
			BOOL bMelee = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractableBS, ciInteractableBS_MeleeProofFlag)
			BOOL bSteam = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractableBS, ciInteractableBS_SteamProofFlag)
			BOOL bSmoke = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractableBS, ciInteractableBS_SmokeProofFlag)
			SET_ENTITY_PROOFS(oiInteractable, bBullet, bFlame, bExplosion, bCollision, bMelee, bSteam, DEFAULT, bSmoke)
			PRINTLN("[Interactables][Interactable ", iInteractable, "] MC_SET_UP_INTERACTABLE | Overriding Interactable proofs! bBullet: ", bBullet, " bFlame: ", bFlame, " bExplosion: ", bExplosion, " bCollision: ", bCollision, " bMelee: ", bMelee, " bSteam: ", bSteam, " bSmoke: ", bSmoke)
		ELSE
			SET_ENTITY_PROOFS(oiInteractable, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE, DEFAULT, TRUE)
		ENDIF
	ENDIF
	
	IF SHOULD_INTERACTABLE_BE_INVISIBLE(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable], FALSE)
		SET_ENTITY_VISIBLE(oiInteractable, FALSE)
		SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiInteractable, FALSE)
		PRINTLN("[Interactables][Interactable ", iInteractable, "] MC_SET_UP_INTERACTABLE | Making Interactable invisible due to SHOULD_INTERACTABLE_BE_INVISIBLE")
	ENDIF
	
	IF SHOULD_INTERACTABLE_BE_COMPLETELY_FROZEN(iInteractable)
		SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiInteractable, FALSE)
		PRINTLN("[Interactables][Interactable ", iInteractable, "] MC_SET_UP_INTERACTABLE | Disabling Interactable's collision due to SHOULD_INTERACTABLE_BE_COMPLETELY_FROZEN")
	ENDIF
	
	INIT_INTERACTABLE_DOOR_CONNECTIONS(iInteractable)
	
	PROCESS_INTERACTABLE_COMPLETION_MODEL_SWAP(iInteractable, oiInteractable)
			
	// Freeze it last to ensure no other setup processes unfreeze it
	IF SHOULD_THIS_MODEL_BE_FROZEN(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model, FALSE)
		FREEZE_ENTITY_POSITION(oiInteractable, TRUE)
	ENDIF
	
ENDPROC

FUNC BOOL MC_CREATE_INTERACTABLE(INT iInteractable, BOOL bInitialSpawn)
	
	REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model)
	IF NOT HAS_MODEL_LOADED(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model)
		PRINTLN("[MCSpawning][Interactables][Interactable ", iInteractable, "] MC_CREATE_INTERACTABLE | Loading model... (", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model), ")")
		RETURN FALSE
	ENDIF
	
	IF NOT MC_LOAD_INTERACTABLE_EXTRA_ASSETS(iInteractable)
		PRINTLN("[MCSpawning][Interactables][Interactable ", iInteractable, "] MC_CREATE_INTERACTABLE | Waiting for MC_LOAD_INTERACTABLE_EXTRA_ASSETS!")
		RETURN FALSE
	ENDIF
	
	IF FMMC_CREATE_NET_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_INTERACTABLE_NET_ID_INDEX(iInteractable)], g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model,
		MC_GET_INTERACTABLE_POSITION(iInteractable, bInitialSpawn), DEFAULT, DEFAULT, TRUE, DEFAULT, TRUE, DEFAULT, DEFAULT, bInitialSpawn) //Force creation if bInitialSpawn
		
		PRINTLN("[MCSpawning][Interactables][Interactable ", iInteractable, "] MC_CREATE_INTERACTABLE | Creating Interactable ", iInteractable)
		
		MC_SET_UP_INTERACTABLE(iInteractable, bInitialSpawn)
		
		SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model)
		iSpawnPoolInteractableIndex = -1
				
		MC_PROCESS_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sWarpLocationSettings, CREATION_TYPE_INTERACTABLE, iInteractable, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_SpawnGroup, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_SpawnSubGroupBS, NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_INTERACTABLE_NET_ID_INDEX(iInteractable)]))
		
		// If SHOULD_THIS_MODEL_BE_FROZEN is TRUE, the entity stays frozen from MC_SET_UP_INTERACTABLE so we block the freezing/unfreezing here using that - otherwise have it be frozen here and unfrozen later
		SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE(GET_INTERACTABLE_NET_ID(iInteractable), bInitialSpawn, SHOULD_THIS_MODEL_BE_FROZEN(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model, FALSE))
		
		RETURN TRUE
	ENDIF
		
	PRINTLN("[MCSpawning][Interactables][Interactable ", iInteractable, "] MC_CREATE_INTERACTABLE | Interactable failed to be created!")
	RETURN FALSE
ENDFUNC

FUNC BOOL MC_CREATE_INTERACTABLES()
										
	INT iInteractable
	BOOL bCreationFailed = FALSE
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables iInteractable
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(GET_INTERACTABLE_NET_ID(iInteractable))
			// Already exists!
			RELOOP
		ENDIF
		
		IF NOT SHOULD_INTERACTABLE_SPAWN_NOW(iInteractable)
			// Shouldn't spawn yet!
			RELOOP
		ENDIF
		
		IF MC_CREATE_INTERACTABLE(iInteractable, TRUE)
			// Interactable created!
			MC_serverBD_1.sCreatedCount.iNumObjCreated[0]++
			
		ELSE
			// Creation failed!
			bCreationFailed = TRUE
		ENDIF
		
	ENDREPEAT

	//Check we have created all Interactables
	IF bCreationFailed
		PRINTLN("[MCSpawning][Interactables][Interactable ", iInteractable, "] MC_CREATE_INTERACTABLES | One or more required Interactables haven't been created yet!")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


//////////////////////////////      MP OBJECTS           //////////////////////////////
FUNC BOOL MC_CREATE_OBJECTS(
								NETWORK_INDEX &niPed[],
								NETWORK_INDEX &niVeh[],
								NETWORK_INDEX &niCrateDoors[]) // Passing through the index where any blow-up-crate minigame doors will get made
										
	INT i
	BOOL bOnGround = TRUE
	//Don't stagger on fade transitions
	BOOL bStrand = IS_A_STRAND_MISSION_BEING_INITIALISED() AND (GET_STRAND_MISSION_TRANSITION_TYPE() != FMMC_BRANCHING_MISSION_TRANSITION_TYPE_FADEOUT)
	BOOL bVisible	
	IF bStrand
		bVisible = TRUE
	ENDIF
	FLOAT fHeading
	SPAWN_SEARCH_PARAMS sSearchParams
	INT iRequestThisFrame
	VECTOR vDummyRot
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfObjects i
			
		IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRespawnOnRule, -1)
		AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ENTITIES_CAN_RESPAWN_ON_RULE_CHANGE)
			SET_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_ENTITIES_CAN_RESPAWN_ON_RULE_CHANGE)
		ENDIF
		
		IF MC_SHOULD_OBJ_SPAWN_AT_START(i, FALSE)
			IF NOT HAS_OBJECT_CREATION_FINISHED(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)], i, niCrateDoors, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn)
				VECTOR vObjPos = <<0,0,0>>	
				
				IF SHOULD_PLACED_OBJECT_BE_PORTABLE(i)
					bOnGround = TRUE
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSet, cibsOBJ_SnapToGround)
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectAttachmentPed != -1
							IF NOT IS_NET_PED_INJURED(niPed[g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectAttachmentPed])
								bOnGround = FALSE
							ENDIF
						ENDIF
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSet, cibsOBJ_Position_Override)
						OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSet, cibsOBJ_Rotation_Override)
							bOnGround = FALSE
						ENDIF
					ELSE
						PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_CREATE_OBJECTS - cibsOBJ_SnapToGround is TRUE")
					ENDIF
					
					BOOL bSpawnNear
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearType != ciRULE_TYPE_NONE
					AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearEntityID > -1
						vObjPos = MC_GET_SPAWN_NEAR_LOCATION(i, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearType, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearEntityID)

						sSearchParams.bConsiderInteriors = TRUE
						sSearchParams.fMinDistFromPlayer = 2			
						sSearchParams.vFacingCoords = vObjPos
						sSearchParams.vAvoidCoords[0] = vObjPos
						sSearchParams.fAvoidRadius[0] = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fRespawnMinRange			
						IF NOT GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vObjPos, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fRespawnRange, vObjPos, fHeading, sSearchParams)
							vObjPos = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vPos
						ELSE
							bSpawnNear = TRUE
						ENDIF
					ELSE
						vObjPos = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vPos
					ENDIF
					
					REPOSITION_COORD_BASED_ON_WARP_LOCATIONS_RANDOM_POOL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sWarpLocationSettings, CREATION_TYPE_OBJECTS, TRUE, vObjPos #IF IS_DEBUG_BUILD , i #ENDIF )
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjectTrainAttachmentData.iTrainIndex > -1
						MC_GET_TRAIN_ATTACHMENT_POINT_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjectTrainAttachmentData, vObjPos, vDummyRot)
						PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_CREATE_OBJECTS - Using train-attached position as vObjPos: ", vObjPos)
					ENDIF
					
					IF SHOULD_ENTITY_USE_CHECKPOINT_CONTINUITY_VARS(ciENTITY_TYPE_OBJECT, i)
						// This object has got a position saved from the last checkpoint!
						VECTOR vEntityCheckpointPos = GET_COORDS_FOR_ENTITY_CHECKPOINT_CONTINUITY(ciENTITY_TYPE_OBJECT, i)
						PRINTLN("[MCSpawning][Objects][Object ", i, "][CONTINUITY][EntityCheckpointContinuity] 1 - MC_CREATE_OBJECTS - using saved checkpoint position ", vEntityCheckpointPos)
						vObjPos = vEntityCheckpointPos
					ENDIF
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearType != ciRULE_TYPE_NONE
					AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearEntityID > -1
						IF bSpawnNear
							MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)] = MC_CREATE_PORTABLE_PICKUP_BASED_ON_SETTINGS(i, bOnGround, vObjPos)
						ENDIF
					ELSE
						MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)] = MC_CREATE_PORTABLE_PICKUP_BASED_ON_SETTINGS(i, bOnGround, vObjPos)
					ENDIF
					
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)])
						SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)], TRUE)
						MC_SET_UP_OBJ(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)]), i, bOnGround, bVisible, FALSE, bSpawnNear, TRUE)
						
						MC_serverBD_1.sCreatedCount.iNumObjCreated[0]++
												
						MC_PROCESS_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sWarpLocationSettings, CREATION_TYPE_OBJECTS, i, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnGroup, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnSubGroupBS, NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)]))
						
						SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)], TRUE)
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_CREATE_OBJECTS - FMMC PICKUP OBJECT CREATED")
						#ENDIF
						IF bStrand
							IF iRequestThisFrame >= PICK_INT(IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING(), ciFMMC_MAX_PER_FRAME_CREATES_AIRLOCK, ciFMMC_MAX_PER_FRAME_CREATES)
								RETURN FALSE
							ELSE
								iRequestThisFrame++
							ENDIF
						ENDIF
					ENDIF
					
				ELSE
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)])
						BOOL bSpawnNear
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearType != ciRULE_TYPE_NONE
						AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearEntityID > -1
							vObjPos = MC_GET_SPAWN_NEAR_LOCATION(i, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearType, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearEntityID)
							sSearchParams.bConsiderInteriors = TRUE
							sSearchParams.fMinDistFromPlayer = 2			
							sSearchParams.vFacingCoords = vObjPos
							sSearchParams.vAvoidCoords[0] = vObjPos
							sSearchParams.fAvoidRadius[0] = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fRespawnMinRange			
							IF NOT GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vObjPos, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fRespawnRange, vObjPos, fHeading, sSearchParams)
								vObjPos = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vPos
							ELSE
								bSpawnNear = TRUE
							ENDIF
						ELSE
							vObjPos = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vPos
						ENDIF
						
						IF SHOULD_ENTITY_USE_CHECKPOINT_CONTINUITY_VARS(ciENTITY_TYPE_OBJECT, i)
							// This object has got a position saved from the last checkpoint!
							VECTOR vEntityCheckpointPos = GET_COORDS_FOR_ENTITY_CHECKPOINT_CONTINUITY(ciENTITY_TYPE_OBJECT, i)
							PRINTLN("[MCSpawning][Objects][Object ", i, "][CONTINUITY][EntityCheckpointContinuity] 2 - MC_CREATE_OBJECTS - using saved checkpoint position ", vEntityCheckpointPos)
							vObjPos = vEntityCheckpointPos
						ENDIF
						
						REPOSITION_COORD_BASED_ON_WARP_LOCATIONS_RANDOM_POOL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sWarpLocationSettings, CREATION_TYPE_OBJECTS, TRUE, vObjPos #IF IS_DEBUG_BUILD , i #ENDIF )
						
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearType != ciRULE_TYPE_NONE
						AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearEntityID > -1
						AND bSpawnNear
							IF FMMC_CREATE_NET_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)], g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn, vObjPos,
										DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, DEFAULT, DEFAULT, TRUE) //Force Creation
										
								MC_SET_OBJECT_SETTINGS_AFTER_CREATION(i, niVeh, NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)]), MC_serverBD_1.sCreatedCount)
								MC_SET_UP_OBJ(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)]), i, bOnGround, bVisible, FALSE, bSpawnNear, TRUE)
								
								MC_PROCESS_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sWarpLocationSettings, CREATION_TYPE_OBJECTS, i, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnGroup, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnSubGroupBS, NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)]))
								SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)], TRUE)
							ENDIF
						ELSE
							IF FMMC_CREATE_NET_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)], g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn,
										vObjPos, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, DEFAULT, DEFAULT, TRUE) //Force Creation
										
								MC_SET_OBJECT_SETTINGS_AFTER_CREATION(i, niVeh, NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)]), MC_serverBD_1.sCreatedCount)
								MC_SET_UP_OBJ(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)]), i, bOnGround, bVisible, FALSE, bSpawnNear, TRUE)
																
								MC_PROCESS_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sWarpLocationSettings, CREATION_TYPE_OBJECTS, i, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnGroup, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnSubGroupBS, NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)]))
								SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)], TRUE)
							ENDIF
						ENDIF
					ELSE
						IF IS_THIS_OBJECT_A_HACK_CONTAINER(i)
							IF HAS_CONTAINER_AND_ASSETS_LOADED()
								IF CREATE_AND_ATTACH_CRATE_DOORS(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(i)], niCrateDoors, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vPos)									
									MC_serverBD_1.sCreatedCount.iNumObjCreated[0] += 3
									IF bStrand
										IF iRequestThisFrame >= PICK_INT(IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING(), ciFMMC_MAX_PER_FRAME_CREATES_AIRLOCK, ciFMMC_MAX_PER_FRAME_CREATES)
											RETURN FALSE
										ELSE
											iRequestThisFrame++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[MCSpawning][Objects][Object ", i, "] - MC_CREATE_OBJECTS - FMMC PICKUP OBJECT NOT SET TO SPAWN AT START")
		ENDIF
	ENDREPEAT

	//Check we have created all the things
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfObjects 	 i
		IF MC_SHOULD_OBJ_SPAWN_AT_START(i, FALSE)
			IF NOT HAS_OBJECT_CREATION_FINISHED(GET_OBJECT_NET_ID(i), i, niCrateDoors, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Dialogue Triggers
// ##### Description: Functions for creating and setting up placed Interactables
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC VECTOR MC_GET_DIALOGUE_TRIGGER_POSITION(INT iDialogue)
	VECTOR vPos = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].vPosition
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset6, iBS_Dialogue6_AttachToEntityTypeAndID) 
	AND g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex != -1
		SWITCH g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsType
			CASE ciENTITY_EXISTS_DT_PEDS
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex])					
					vPos = GET_ENTITY_COORDS(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex]), FALSE)
				ENDIF
			BREAK
			CASE ciENTITY_EXISTS_DT_VEHICLES
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex])
					vPos = GET_ENTITY_COORDS(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex]), FALSE)
				ENDIF
			BREAK
			CASE ciENTITY_EXISTS_DT_OBJECTS
				IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex))
					vPos = GET_ENTITY_COORDS(NET_TO_OBJ(GET_OBJECT_NET_ID(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex)), FALSE)
				ENDIF
			BREAK			
			CASE ciENTITY_EXISTS_DT_PROPS
				IF DOES_ENTITY_EXIST(oiProps[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex])	
					vPos = GET_ENTITY_COORDS(oiProps[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex], FALSE)
				ENDIF
			BREAK
			CASE ciENTITY_EXISTS_DT_WEAPONS
				IF DOES_PICKUP_EXIST(pipickup[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex])
					vPos = GET_PICKUP_COORDS(pipickup[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex])
				ENDIF
			BREAK	
		ENDSWITCH
	ENDIF
	
	RETURN vPos
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Dyno Props (Utility and Helper Functions)
// ##### Description: Process the creation of Mission Controller Dyno Props.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL CAN_DYNOPROP_STILL_SPAWN_IN_MISSION(INT iDynoProp, INT iTeamOverride = -1, INT iRuleOverride = -1)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iAssociatedActionStart = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iAssociatedObjective
					// The DynoProp will have a chance to spawn:
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iAssociatedObjective
					// The DynoProp still has a chance to spawn:
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE // Spawn type won't prevent DynoProp from spawning after the rule
			RETURN TRUE
		ENDIF
	ELSE
		//DynoProp will just spawn at mission start!
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iSecondAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iSecondAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iSecondAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iSecondAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iSecondAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iSecondAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iSecondAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iThirdAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iThirdAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iThirdAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iThirdAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iThirdAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iThirdAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iThirdAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iFourthAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iFourthAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iFourthAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iFourthAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iFourthAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iFourthAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iFourthAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL MC_SHOULD_DYNOPROP_SPAWN_AT_START(INT i)
	
	IF NOT MC_SHOULD_ENTITY_SPAWN_AT_START(CREATION_TYPE_DYNOPROPS, i, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iAssociatedObjective, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iAssociatedSpawn, TRUE)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sProximitySpawningData.iSpawnRange != 0
		PRINTLN("[MCSpawning][Dynoprops][Dynoprop ", i, "][PROXSPAWN] - MC_SHOULD_DYNOPROP_SPAWN_AT_START - Returning FALSE due to using Proximity Spawning")
		SET_BIT(MC_serverBD.iDynoPropShouldRespawnNowBS, i)
		RETURN FALSE
	ENDIF
	
	IF MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].eSpawnConditionFlag, TRUE, FALSE, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iAggroIndexBS_Entity_DynoProp)
		PRINTLN("[MCSpawning][Dynoprops][Dynoprop ", i, "] - MC_SHOULD_DYNOPROP_SPAWN_AT_START - Returning FALSE due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")
		RETURN FALSE
	ENDIF
	
	IF GET_DYNOPROP_MODEL_TO_USE(i) = DUMMY_MODEL_FOR_SCRIPT
		RETURN FALSE
	ENDIF
	
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_DynoProp)
		PRINTLN("[MCSpawning][Dynoprops][Dynoprop ", i, "] - MC_SHOULD_DYNOPROP_SPAWN_AT_START - RETURNING FALSE - MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP = FALSE")
		RETURN FALSE
	ENDIF
	
	IF MC_WILL_ENTITY_CLEANUP_IMMEDIATELY(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iCleanupTeam, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iCleanupObjective, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iDynoPropBitset, ciFMMC_DYNOPROP_CleanupAtMidpoint))
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iPropSpawnLap != 0
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_DynoPropDestroyedTracking)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iContinuityId > -1
		AND IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iDynoPropDestroyedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iContinuityId)
			PRINTLN("[MCSpawning][Dynoprops][Dynoprop ", i, "] - MC_SHOULD_DYNOPROP_SPAWN_AT_START - RETURNING FALSE - Dynoprop was destroyed in a previous mission")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iDynopropBitset, ciFMMC_DYNOPROP_DroppedByEvent)
		PRINTLN("[MCSpawning][Dynoprops][Dynoprop ", i, "] - MC_SHOULD_DYNOPROP_SPAWN_AT_START - RETURNING FALSE - Dynoprop is dropped by event")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Props (Utility and Helper Functions)
// ##### Description: Process the creation of Mission Controller Props.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL CAN_PROP_STILL_SPAWN_IN_MISSION(INT iProp, INT iTeamOverride = -1, INT iRuleOverride = -1)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedActionStart = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedObjective
					// The Prop will have a chance to spawn:
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedObjective
					// The Prop still has a chance to spawn:
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE // Spawn type won't prevent Prop from spawning after the rule
			RETURN TRUE
		ENDIF
	ELSE
		//Prop will just spawn at mission start!
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


//////////////////////////////      LOAD ALL MODELS 		//////////////////////////////
//FMMC2020 REWRITE THIS SO THAT IT IS ONLY CALLED ONCE for the Local Client (not just host)
FUNC BOOL MC_REQUEST_LOAD_MODELS()
	BOOL bReturn = TRUE
	
	INT i	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn != DUMMY_MODEL_FOR_SCRIPT
		
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFive, ciPED_BSFive_StartDead)
				REQUEST_ANIM_DICT("missfbi1")
				IF NOT HAS_ANIM_DICT_LOADED("missfbi1")
					bReturn = FALSE
				ENDIF
			ENDIF
			
			PRINTLN("[RCC MISSION] - requesting model for ped : ", i, " || ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn))
			IF MC_SHOULD_PED_SPAWN_AT_START(i, FALSE)
				IF NOT REQUEST_LOAD_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn)
					PRINTLN("[RCC MISSION] REQUEST_LOAD_FMMC_MODELS - FAILING ON - g_FMMC_STRUCT_ENTITIES.sPlacedPed[", i, "].mn = ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn))
					bReturn = FALSE
				ENDIF
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iInventoryBS != 0
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iInventoryBS, ciPED_INV_Briefcase_prop_ld_case_01)
						REQUEST_MODEL(PROP_LD_CASE_01)
						REQUEST_ANIM_DICT("weapons@misc@jerrycan@mp_male")
						IF (NOT HAS_MODEL_LOADED(PROP_LD_CASE_01))
							PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON load Ped inventory Briefcase model PROP_LD_CASE_01")
							bReturn = FALSE
						ENDIF
						IF (NOT HAS_ANIM_DICT_LOADED("weapons@misc@jerrycan@mp_male"))
							PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON load Ped inventory Briefcase anim dict weapons@misc@jerrycan@mp_male")
							bReturn = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn = IG_LESTERCREST
				REQUEST_CLIP_SET("move_heist_lester")
				IF NOT HAS_CLIP_SET_LOADED("move_heist_lester")
					PRINTLN("[RCC MISSION] REQUEST_LOAD_FMMC_MODELS - FAILING ON - lester ped clipset ", i)
					bReturn = FALSE
				ENDIF
			ENDIF
						
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFive, ciPED_BSFive_StartDead)
				REQUEST_ANIM_DICT(MC_GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iDeadAnim))
				IF NOT HAS_ANIM_DICT_LOADED(MC_GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iDeadAnim))
					PRINTLN("[RCC MISSION] REQUEST_LOAD_FMMC_MODELS - FAILING ON - dead ped ", i, " anim dict ",MC_GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iDeadAnim))
					bReturn = FALSE
				ENDIF
			ENDIF			
		ENDIF
	ENDREPEAT	
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles i	
		IF MC_SHOULD_VEH_SPAWN_AT_START(i, FALSE)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
				PRINTLN("[RCC MISSION] - setting requesting model for veh : ", i)
				IF NOT REQUEST_LOAD_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON - g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[", i, "].mn = ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn))
					bReturn = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfObjects i
		IF MC_SHOULD_OBJ_SPAWN_AT_START(i, FALSE, FALSE)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn != DUMMY_MODEL_FOR_SCRIPT
				PRINTLN("[RCC MISSION] - setting requesting model for object : ", i, " || ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn))
				IF NOT REQUEST_LOAD_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn)
					PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON - g_FMMC_STRUCT_ENTITIES.sPlacedObject[", i, "].mn = ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn))
					bReturn = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables i
		IF SHOULD_INTERACTABLE_SPAWN_NOW(i)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].mnInteractable_Model != DUMMY_MODEL_FOR_SCRIPT
				PRINTLN("[RCC MISSION] - setting requesting model for Interactable : ", i, " || ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].mnInteractable_Model))
				IF NOT REQUEST_LOAD_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].mnInteractable_Model)
					PRINTLN("[RCC MISSION] - Still loading model for Interactable : ", i, " || ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].mnInteractable_Model))
					bReturn = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps i	
		IF MC_SHOULD_DYNOPROP_SPAWN_AT_START(i)
		AND GET_DYNOPROP_MODEL_TO_USE(i) != DUMMY_MODEL_FOR_SCRIPT
			IF NOT REQUEST_LOAD_MODEL(GET_DYNOPROP_MODEL_TO_USE(i))
				PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON - GET_DYNOPROP_MODEL_TO_USE(", i, ") = ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_DYNOPROP_MODEL_TO_USE(i)))
				bReturn = FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfProps i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn != DUMMY_MODEL_FOR_SCRIPT		
			IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Speed"))
				IF NOT REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Speeda")))
				AND NOT REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Speedb")))
					PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON - g_FMMC_STRUCT_ENTITIES.sPlacedProp[", i, "].mn = ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn))
					bReturn = FALSE
				ELSE
					PRINTLN("[LM] LOADED: MC_REQUEST_LOAD_MODELS")
				ENDIF
			ENDIF
			IF NOT REQUEST_LOAD_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn)
				PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON - g_FMMC_STRUCT_ENTITIES.sPlacedProp[", i, "].mn = ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn))
				bReturn = FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN bReturn 
ENDFUNC

//////////////////////////////      LOAD ALL PROP MODELS 		//////////////////////////////
FUNC BOOL MC_REQUEST_LOAD_PROP_MODELS()
	
	BOOL bLoaded = TRUE
	INT i

	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfProps i
	
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Speed"))
				IF NOT REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Speeda")))
				AND NOT REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Speedb")))
					PRINTLN("[RCC MISSION] MC_REQUEST_LOAD_PROP_MODELS - Failing to load model for prop ", i, ", model name ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn))
					bLoaded = FALSE
				ELSE
					PRINTLN("[LM] LOADED: MC_REQUEST_LOAD_PROP_MODELS: stt_Prop_Stunt_Tube_Speeda + stt_Prop_Stunt_Tube_Speedb")
				ENDIF
			ENDIF
			
			IF NOT REQUEST_LOAD_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn)
				PRINTLN("[RCC MISSION] MC_REQUEST_LOAD_PROP_MODELS - Failing to load model for prop ", i, ", model name ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn))
				bLoaded = FALSE
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_barge_01"))
				IF NOT REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Barge_Col_01")))
				AND NOT REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Barge_Col_02")))
					PRINTLN("[RCC MISSION] MC_REQUEST_LOAD_PROP_MODELS - Failing to load extra barge collision models for prop ", i, ", model name ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn))
					bLoaded = FALSE
				ELSE
					PRINTLN("[RCC MISSION] MC_REQUEST_LOAD_PROP_MODELS - LOADED: xm_prop_x17_Barge_Col_02 + xm_prop_x17_Barge_Col_01")
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	
	return bLoaded
	
ENDFUNC

FUNC FLOAT MC_GET_SPEED_BOOST_TIME(INT iCurrentSpeedAmount)
	
#IF IS_DEBUG_BUILD
	IF f_TIME_OverrideAll > 0
		RETURN f_TIME_OverrideAll
	ENDIF
	SWITCH iCurrentSpeedAmount
		CASE 1			
		IF f_TIME_Small > 0
			PRINTLN("[LM][MC_GET_SPEED_BOOST_TIME] Using Rag Time Value: ", f_TIME_Small)	
			RETURN f_TIME_Small
		ENDIF
		BREAK
		CASE 2			
		IF f_TIME_Medium > 0
			PRINTLN("[LM][MC_GET_SPEED_BOOST_TIME] Using Rag Time Value: ", f_TIME_Medium)	
			RETURN f_TIME_Medium
		ENDIF
		BREAK
		CASE 3			
		IF f_TIME_Large > 0
			PRINTLN("[LM][MC_GET_SPEED_BOOST_TIME] Using Rag Time Value: ", f_TIME_Large)	
			RETURN f_TIME_Large
		ENDIF
		BREAK
		CASE 4			
		IF f_TIME_Extra > 0
			PRINTLN("[LM][MC_GET_SPEED_BOOST_TIME] Using Rag Time Value: ", f_TIME_Extra)	
			RETURN f_TIME_Extra
		ENDIF
		BREAK
		CASE 5			
		IF f_TIME_Ultra > 0
			PRINTLN("[LM][MC_GET_SPEED_BOOST_TIME] Using Rag Time Value: ", f_TIME_Ultra)	
			RETURN f_TIME_Ultra			
		ENDIF
		BREAK
	ENDSWITCH
#ENDIF

	SWITCH iCurrentSpeedAmount
		CASE 1			
		RETURN 0.3
		
		CASE 2			
		RETURN 0.4
		
		CASE 3			
		RETURN 0.5
		
		CASE 4			
		RETURN 0.5		
		
		CASE 5			
		RETURN 0.5
	ENDSWITCH
	
	RETURN 0.4
ENDFUNC

PROC MC_INITIALIZE_SPEED_BOOST_PROPS(INT iProp, OBJECT_INDEX oiProp)
	// Speed Up Pad
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SpeedUp"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SpeedUp_T1"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SpeedUp_T2"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_speed"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_speedb"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("ar_prop_ar_speed_ring"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("ar_Prop_AR_Tube_Speed"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("ar_Prop_AR_Tube_2x_Speed"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("ar_Prop_AR_Tube_4x_Speed"))
		IF DOES_ENTITY_EXIST(oiProp)	
			PRINTLN("MC_INITIALIZE_SPEED_BOOST_PROPS, SET_OBJECT_SPEED_BOOST_AMOUNT | iProp = ", iProp, " GET_SPEED_DOWN_AMOUNT = ", GET_SPEED_UP_AMOUNT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropSpeedBoostAmount))
			SET_OBJECT_SPEED_BOOST_AMOUNT(oiProp, GET_SPEED_UP_AMOUNT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropSpeedBoostAmount))
			PRINTLN("MC_INITIALIZE_SPEED_BOOST_PROPS, SET_OBJECT_SPEED_BOOST_DURATION | iProp = ", iProp, " MC_GET_SPEED_BOOST_TIME = ", MC_GET_SPEED_BOOST_TIME(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropSpeedBoostAmount))
			SET_OBJECT_SPEED_BOOST_DURATION(oiProp, MC_GET_SPEED_BOOST_TIME(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropSpeedBoostAmount))
		
			SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Requires_Alpha_Flash)
			PRINTLN("[LM][PROCESS_PROPS] - ciFMMC_PROP_Requires_Alpha_Flash has been set on iProp: ", iProp)
		ENDIF
	ENDIF
	// Slow Down Pad
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown_T1"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown_T2"))
	OR IS_PROP_CAPABLE_OF_SLOW_DOWN(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn)
		PRINTLN("MC_INITIALIZE_SPEED_BOOST_PROPS, SET_OBJECT_SPEED_BOOST_AMOUNT | iProp = ", iProp, " GET_SPEED_DOWN_AMOUNT = ", GET_SLOW_DOWN_AMOUNT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropSpeedBoostAmount))
		SET_OBJECT_SPEED_BOOST_AMOUNT(oiProp, GET_SLOW_DOWN_AMOUNT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropSpeedBoostAmount))
	ENDIF
ENDPROC

PROC MC_INITIALIZE_SOUND_TRIGGER_PROPS(INT iProp, OBJECT_INDEX oiProp, BOOL bReveal)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_speakerstack_01a"))
		IF DOES_ENTITY_EXIST(oiProp)
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSTMTriggerCounter = 0
			CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Sound_Trigger_Played_This_Lap)
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Sound_Trigger_Is_Invisible)
				IF bReveal = TRUE
					SET_ENTITY_ALPHA(oiProp, 255, bReveal)
					SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiProp, bReveal)
				ELSE
					SET_ENTITY_ALPHA(oiProp, 0, bReveal)
					SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiProp, bReveal)
				ENDIF	
			ENDIF	
		ENDIF		
	ENDIF
ENDPROC

FUNC BOOL MC_SHOULD_PROP_SPAWN_AT_START(INT i, INT iNumberOfTeams)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = DUMMY_MODEL_FOR_SCRIPT
		RETURN FALSE
	ENDIF
	
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_Prop)
		PRINTLN("[RCC MISSION] MC_SHOULD_PROP_SPAWN_AT_START Prop ", i, " - RETURNING FALSE - MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP = FALSE")
		RETURN FALSE
	eLSE
		PRINTLN("[RCC MISSION] MC_SHOULD_PROP_SPAWN_AT_START Prop ", i, " - RETURNING TRUE - MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP = TRUE")
	ENDIF
	
	IF MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].eSpawnConditionFlag, TRUE, FALSE, g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iAggroIndexBS_Entity_Prop, MC_PlayerBD[iPartToUse].iTeam)
		PRINTLN("[RCC MISSION] MC_SHOULD_PROP_SPAWN_AT_START Prop ", i, " - Returning FALSE due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iPropBitset, ciFMMC_PROP_AssociatedSpawn)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iPropSpawnNumber > -1
			IF IS_BIT_SET(MC_ServerBD.iSpawnPointsUsed[g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iPropTeamNumber], g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iPropSpawnNumber)
			OR IS_BIT_SET(LocalRandomSpawnBitset[g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iPropTeamNumber], g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iPropSpawnNumber)
				RETURN TRUE
			ELSE
				PRINTLN("[RCC MISSION] MC_SHOULD_PROP_SPAWN_AT_START Prop ", i, " - Blocked by iPropSpawnNumber functionality")
				RETURN FALSE
			ENDIF
		ELSE
			IF iNumberOfTeams > g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iPropTeamNumber
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF

	IF MC_SHOULD_ENTITY_SPAWN_AT_START(CREATION_TYPE_PROPS, i, g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iAssociatedObjective, g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iAssociatedSpawn, TRUE)
		IF MC_WILL_ENTITY_CLEANUP_IMMEDIATELY(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iCleanupTeam, g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iCleanupObjective, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iPropBitset, ciFMMC_PROP_CleanupAtMidpoint))
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Props
// ##### Description: Process the creation of Mission Controller Props.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC ADD_PROP_TO_EMITTER_ARRAY(INT iProp)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_EmitRadio)
		EXIT
	ENDIF
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(oiProps[iProp])
		EXIT
	ENDIF
		
	INT iEmitter = FIND_A_FREE_EMITTER_FOR_OBJECT(iProp, RADIO_EMITTER_TYPE_PROP)
			
	IF iEmitter != -1
		MC_serverBD_3.sRadioEmitters[iEmitter].iRadioStation = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iRadioStation
		PRINTLN("ADD_PROP_TO_EMITTER_ARRAY - Adding prop ", iProp," to emitter system")
	ENDIF
	
ENDPROC

/// PURPOSE: This function sets the passed prop up correctly and visible/invisible as required
///    EXCEPT for the model PROP_LD_ALARM_01, which will ALWAYS be set as invisible!
PROC MC_SET_UP_PROP(OBJECT_INDEX oiPassed, INT iPropNumber, BOOL bVisible = TRUE)

	PRINTLN("[MC_SET_UP_PROP] iPropNumber: ", iPropNumber, " mn: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].mn))

	SET_ENTITY_COORDS(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].vPos)
	
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].vRot)
		SET_ENTITY_ROTATION(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].vRot)
	ELSE
		SET_ENTITY_HEADING(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].fHead)
	ENDIF
	
	IF GET_ENTITY_MODEL(oiPassed) != PROP_LD_ALARM_01
		IF HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
			SET_ENTITY_VISIBLE(oiPassed, TRUE)
		ELSE
			SET_ENTITY_VISIBLE(oiPassed, bVisible)
		ENDIF
	ELSE
		SET_ENTITY_VISIBLE(oiPassed, FALSE)
	ENDIF

	MC_INITIALIZE_SPEED_BOOST_PROPS(iPropNumber, oiPassed)
	MC_INITIALIZE_SOUND_TRIGGER_PROPS(iPropNumber, oiPassed, bVisible)
	CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iPropBitset, ciFMMC_PROP_PTFX_Played)

	SET_OBJECT_TARGETTABLE(oiPassed, FALSE)
	SET_WAIT_FOR_COLLISIONS_BEFORE_PROBE(oiPassed, TRUE)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iLODDistance > 0
		PRINTLN("MC_SET_UP_PROP - Setting prop ", iPropNumber, "'s LOD distance to ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iLODDistance)
		SET_ENTITY_LOD_DIST(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iLODDistance)
	ENDIF
		
	IF NOT IS_PROP_WATER_TYPE(GET_ENTITY_MODEL(oiPassed)) //Water props need to behave correctly in water
		IF SHOULD_OBJECT_BE_FROZEN(oiPassed)
			SET_ENTITY_INVINCIBLE(oiPassed, TRUE)
			SET_ENTITY_PROOFS(oiPassed, TRUE, TRUE, TRUE, TRUE, TRUE)
			FREEZE_ENTITY_POSITION(oiPassed, TRUE)
			SET_OBJECT_ALLOW_LOW_LOD_BUOYANCY(oiPassed, TRUE)
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bEnableAmericanFlagStuntRaces
		PRINTLN("[LH][FlagColours] g_sMPTunables.bEnableAmericanFlagStuntRaces = TRUE")
		PRINTLN("[LH][FlagColours] Setting g_FMMC_STRUCT_ENTITIES.sPlacedProp[", iPropNumber, "] from ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iPropColour, " to 0")
		g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iPropColour = 0
	ENDIF
	
	IF CAN_SET_MODEL_TINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].mn)
		PRINTLN("[MC_SET_UP_PROP] Setting TINT g_FMMC_STRUCT_ENTITIES.sPlacedProp[", iPropNumber, "] with tint index ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iPropColour)
		SET_OBJECT_TINT_INDEX(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iPropColour)
	ENDIF
	
	IF IS_MODEL_STUNT_PROP(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].mn)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_ARENA)
		SET_IS_EXTERIOR_ONLY(oiPassed, TRUE)
	ENDIF
	
	IF IS_PROP_CAPABLE_OF_SLOW_DOWN(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].mn)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iPropBitSet2,  ciFMMC_PROP2_SLOW_DOWN_ENABLE_EFFECT)
			SET_SLOW_DOWN_EFFECT_DISABLED(FALSE)
		ELSE
			SET_SLOW_DOWN_EFFECT_DISABLED(TRUE)
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iRespawnOnRuleChangeBS != 0
		SET_ALLOW_MIGRATE_TO_SPECTATOR(oiPassed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iPropBitSet,  ciFMMC_PROP_SetInvisible)
		SET_ENTITY_VISIBLE(oiPassed, FALSE)
		PRINTLN("MC_SET_UP_PROP - Prop ", iPropNumber, " should be invisible.")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iPropBitSet2,  ciFMMC_PROP2_NoCollision)
		SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiPassed, FALSE)
		PRINTLN("MC_SET_UP_PROP - Completely disabling collision for prop ", iPropNumber, "!")
	ENDIF
	
	IF IS_PROP_A_CCTV_WALL_ATTACHMENT(iPropNumber)
		SET_ENTITY_COLLISION(oiPassed, FALSE)
		PRINTLN("MC_SET_UP_PROP - Disabling standard collision for camera attachment prop ", iPropNumber, "!")
	ENDIF
	
	IF IS_PROP_A_BOMBABLE_BLOCK(iPropNumber)
		PRINTLN("SET_UP_FMMC_PROP - Setting bombable block prop proofs")
		SET_ENTITY_PROOFS(oiPassed, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE)
	ENDIF
	
	IF GET_ENTITY_MODEL(oiPassed) = XM_PROP_X17_BARGE_01
		PRINTLN("SET_UP_FMMC_PROP - Creating extra collision for barge")
		VECTOR vCoords = GET_ENTITY_COORDS(oiPassed)
		
		g_FMMC_STRUCT_ENTITIES.oiBargeCollisionProps[0] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Barge_Col_01")), vCoords, FALSE, FALSE, TRUE)
		ATTACH_ENTITY_TO_ENTITY(g_FMMC_STRUCT_ENTITIES.oiBargeCollisionProps[0], oiPassed, 0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, TRUE, TRUE, TRUE, FALSE, DEFAULT)
		
		g_FMMC_STRUCT_ENTITIES.oiBargeCollisionProps[1] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Barge_Col_02")), vCoords, FALSE, FALSE, TRUE)
		ATTACH_ENTITY_TO_ENTITY(g_FMMC_STRUCT_ENTITIES.oiBargeCollisionProps[1], oiPassed, 0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, TRUE, TRUE, TRUE, FALSE, DEFAULT)
	ENDIF
	
	IF IS_THIS_MODEL_A_TRAP_PRESSURE_PAD(GET_ENTITY_MODEL(oiPassed))
		SET_ENTITY_INVINCIBLE(oiPassed, TRUE)
		FREEZE_ENTITY_POSITION(oiPassed, FALSE)
		
		SET_OBJECT_IS_A_PRESSURE_PLATE(oiPassed, TRUE)
		PRINTLN("[ARENATRAPS]", "[PRESSUREPAD ", iPropNumber, "]", " Initialising! 2")
	ENDIF
	
	IF CONTENT_IS_USING_ARENA()
		PRINTLN("SET_UP_FMMC_PROP - SET_ENTITY_LOD_DIST 750 - Forced on Arena.")
		SET_ENTITY_LOD_DIST(oiPassed, 750)
	ENDIF
	
	IF IS_PROP_GLASS_CUT_MINIGAME_LOOT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].mn)
		sGlassCuttingData.iGlassCuttingLootPropIndex = iPropNumber
		PRINTLN("[GlassCutting] SET_UP_FMMC_PROP - Caching iGlassCuttingLootPropIndex as prop ", iPropNumber)
	ENDIF
	
	IF IS_MODEL_AN_ARENA_TURRET_TOWER(GET_ENTITY_MODEL(oiPassed))
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iPropBitSet2, ciFMMC_PROP2_UseAsTurret)
		ARENA_CONTESTANT_TURRET_STACK_PUSH(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].fTurretHeading)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iPropBitSet2, ciFMMC_PROP2_HavePropMatchLobbyHostVehicle)
		IF GET_FMMC_LOBBY_LEADER_PARTICIPANT_INDEX() > -1
			SET_OBJECT_TINT_INDEX(oiPassed, MC_playerBD_1[GET_FMMC_LOBBY_LEADER_PARTICIPANT_INDEX()].iLobbyHostOwnedVehicle_Colour)
			PRINTLN("[Kosatka][LobbyHostVehicle] SET_UP_FMMC_PROP | Setting tint index to ", MC_playerBD_1[GET_FMMC_LOBBY_LEADER_PARTICIPANT_INDEX()].iLobbyHostOwnedVehicle_Colour)
		ELSE
			PRINTLN("[Kosatka][LobbyHostVehicle] SET_UP_FMMC_PROP | GET_FMMC_LOBBY_LEADER_PARTICIPANT_INDEX returned -1! Sticking to default prop setup")
		ENDIF
	ENDIF
	
	IF DOES_MODEL_HAVE_A_SHARED_RENDER_TARGET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].mn)
		ADD_SHARED_RENDERTARGET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].mn, ciENTITY_TYPE_PROPS, iPropNumber)
	ENDIF
	
	ADD_PROP_TO_EMITTER_ARRAY(iPropNumber)
	
ENDPROC

//////////////////////////////      MP PROPS           //////////////////////////////
FUNC BOOL MC_IS_READY_TO_CREATE_PROP(INT iProp)

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitSet2, ciFMMC_PROP2_HavePropMatchLobbyHostVehicle)
		IF GET_FMMC_LOBBY_LEADER() = INVALID_PLAYER_INDEX()
		OR GET_FMMC_LOBBY_LEADER_PARTICIPANT_INDEX() = -1
			PRINTLN("[LobbyHostVehicle] MC_IS_READY_TO_CREATE_PROP | Waiting to identify lobby host!")
			RETURN FALSE
		ENDIF
		
		IF MC_playerBD_1[GET_FMMC_LOBBY_LEADER_PARTICIPANT_INDEX()].iLobbyHostOwnedVehicle_Colour = -1
			PRINTLN("[LobbyHostVehicle] GET_FMMC_YACHT_DATA_FROM_LOBBY_HOST | Waiting for lobby host's colour variable to be set!")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL MC_CREATE_PROPS(OBJECT_INDEX &oiProp[], INT &iSound_ID[], INT &iCrashSoundPropIndex, INT iNumberOfTeams)
	INT i
	
	IF MC_REQUEST_LOAD_PROP_MODELS()
	
		REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfProps 	 i
			
			IF NOT MC_IS_READY_TO_CREATE_PROP(i)
				IF NOT RUN_TIMER(stReadyToCreatePropBailTimer, ciReadyToCreatePropBailTimer_Length)
					PRINTLN("[RCC MISSION] MC_CREATE_PROPS - Prop ", i, " not getting created yet due to MC_IS_READY_TO_CREATE_PROP!")
					RELOOP
				ELSE
					ASSERTLN("[RCC MISSION] MC_CREATE_PROPS - Prop ", i, " MC_IS_READY_TO_CREATE_PROP returned FALSE, but we're proceeding anyway due to bail timer!")
					PRINTLN("[RCC MISSION] MC_CREATE_PROPS - Prop ", i, " MC_IS_READY_TO_CREATE_PROP returned FALSE, but we're proceeding anyway due to bail timer!")
				ENDIF
			ENDIF
			
			iSound_ID[i] = -1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
			AND MC_SHOULD_PROP_SPAWN_AT_START(i, iNumberOfTeams)

				IF NOT DOES_ENTITY_EXIST(oiProp[i])
					IF DOES_PROP_HAVE_A_SUB_COMPONENT(i)
						oiProp[i] = CREATE_PROP_WITH_SUB_COMPONENT(i, oiPropsChildren)
					ELSE
						oiProp[i] = CREATE_OBJECT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn, g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].vPos, FALSE, FALSE, TRUE)
					ENDIF
					
					MC_SET_UP_PROP(oiProp[i], i, FALSE)
					
					IF GET_PROP_LOD_OVERRIDE_DISTANCE(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn) !=-1
						SET_ENTITY_LOD_DIST(oiProp[i], GET_PROP_LOD_OVERRIDE_DISTANCE(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn))
						PRINTLN("[RCC MISSION] MC_CREATE_PROPS - SETTING PROP LOD DISTANCE: ", GET_PROP_LOD_OVERRIDE_DISTANCE(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn))
					ENDIF
 					
					IF(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = PROP_FLARE_01)
					OR(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = ind_prop_firework_03)
						REQUEST_NAMED_PTFX_ASSET("scr_biolab_heist")
					ENDIF
					
					//Turn on crashed plane sound if the crashed plane prop is created					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = PROP_SHAMAL_CRASH
					OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = INT_TO_ENUM(MODEL_NAMES, HASH("apa_MP_Apa_Crashed_USAF_01a"))
					OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Shamal_crash"))
						IF DOES_ENTITY_EXIST(oiProp[i])
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iPropBitset, ciFMMC_PROP_EmitCrashSound)
								iCrashSoundPropIndex = i
							ENDIF
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[RCC MISSION] MC_CREATE_PROPS - Prop ", i, " created!")
						NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION]  ---------->     FMMC PROP CREATED     <----------     ", "FMMC") NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Check we have created all the things
		REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfProps 	 i
			IF NOT DOES_ENTITY_EXIST(oiProp[i])
			AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
			AND MC_SHOULD_PROP_SPAWN_AT_START(i, iNumberOfTeams)
				RETURN FALSE
			ENDIF
		ENDREPEAT
	ELSE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Dyno Props
// ##### Description: Process the creation of Mission Controller Dyno Props.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC CACHE_DYNO_SHOULD_CLEANUP_THIS_FRAME(INT i)
	SET_BIT(iDynoShouldCleanupThisFrameBS, i)
ENDPROC

PROC CACHE_DYNO_SHOULD_NOT_CLEANUP_THIS_FRAME(INT i)
	SET_BIT(iDynoShouldNotCleanupThisFrameBS, i)
ENDPROC

FUNC BOOL SHOULD_CLEANUP_DYNOPROP(FMMC_DYNOPROP_STATE& sDynopropState)
	
	INT iDynoprop = sDynopropState.iIndex
	
	IF IS_BIT_SET(iDynoShouldCleanupThisFrameBS, iDynoprop)
		RETURN TRUE
	ENDIF
	IF IS_BIT_SET(iDynoShouldNotCleanupThisFrameBS, iDynoprop)
		RETURN FALSE
	ENDIF
	
	IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].iDynoPropBitset, ciFMMC_DYNOPROP_CleanupAtMissionEnd)
		CACHE_DYNO_SHOULD_CLEANUP_THIS_FRAME(iDynoprop)
		RETURN TRUE
	ENDIF
	
	IF MC_WAS_DESPAWN_CONDITION_REACHED(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].sCleanupData, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].iAggroIndexBS_Entity_DynoProp #IF IS_DEBUG_BUILD ,"dyno prop" #ENDIF )
		PRINTLN("[RCC MISSION][SHOULD_CLEANUP_DYNOPROP] - iDynoprop: ", iDynoprop, ", removing dynoprop.")
		CACHE_DYNO_SHOULD_CLEANUP_THIS_FRAME(iDynoprop)
		RETURN TRUE
	ENDIF
	
	BOOL bCheckEarlyCleanup
	
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iDynoprop, eSGET_DynoProp)
		PRINTLN("[RCC MISSION][SHOULD_CLEANUP_DYNOPROP] - iDynoprop: ", iDynoprop, ", DynoProp no longer has a valid spawn group active.") 
		bCheckEarlyCleanup = TRUE
	ENDIF	
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].iCleanupObjective != -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].iCleanupTeam != -1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].iCleanupObjective <= MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].iCleanupTeam]
				bCheckEarlyCleanup = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bCheckEarlyCleanup
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].iDynoPropBitset, ciFMMC_DYNOPROP_CleanupAtMidpoint)
			IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].iCleanupTeam], g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].iCleanupObjective)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].iCleanupRange <= 0
					CACHE_DYNO_SHOULD_CLEANUP_THIS_FRAME(iDynoprop)
					RETURN TRUE
				ELSE
					IF sDynopropState.objIndex != NULL
						INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(sDynopropState.objIndex)
						PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
						PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
						IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed, sDynopropState.objIndex) > g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].iCleanupRange
							CACHE_DYNO_SHOULD_CLEANUP_THIS_FRAME(iDynoprop)
							RETURN TRUE
						ENDIF
					ELSE
						CACHE_DYNO_SHOULD_CLEANUP_THIS_FRAME(iDynoprop)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			//Midpoint doesn't matter
			IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].iCleanupRange <= 0
				CACHE_DYNO_SHOULD_CLEANUP_THIS_FRAME(iDynoprop)
				RETURN TRUE
			ELSE
				IF sDynopropState.objIndex != NULL
					INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(sDynopropState.objIndex)
					PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
					PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
					IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed, sDynopropState.objIndex) > g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].iCleanupRange
						CACHE_DYNO_SHOULD_CLEANUP_THIS_FRAME(iDynoprop)
						RETURN TRUE
					ENDIF
				ELSE
					CACHE_DYNO_SHOULD_CLEANUP_THIS_FRAME(iDynoprop)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	CACHE_DYNO_SHOULD_NOT_CLEANUP_THIS_FRAME(iDynoprop)
	RETURN FALSE

ENDFUNC

FUNC BOOL ARE_WE_WAITING_FOR_DYNOPROPS_TO_CLEAN_UP()
	
	BOOL bWaiting
	
	IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
	OR IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HOST_APPLIED_NEW_PRIORITY_MIDFRAME)
	OR IS_BIT_SET(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
	OR MC_serverBD.iDynoPropCleanup_NeedOwnershipBS != 0
		bWaiting = TRUE
	ENDIF
	
	RETURN bWaiting
	
ENDFUNC

PROC CACHE_DYNO_RESPAWN_BLOCKED(INT i)
	IF NOT IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iMiscBS, ciRandomSpawnGroupBS_HasBeenSet)
		EXIT
	ENDIF	
	SET_BIT(iDynoRespawnIsBlockedBS, i)
	PRINTLN("[Dynoprops][MCRespawnDynoProp ", i, "] SHOULD_DYNOPROP_SPAWN_NOW | CACHE_OBJ_RESPAWN_BLOCKED - Caching that the Dyno Prop has been blocked from respawning.")
ENDPROC

PROC CACHE_DYNO_SHOULD_NOT_RESPAWN_THIS_FRAME(INT i)
	SET_BIT(iDynoRespawnIsBlockedThisFrameBS, i)
ENDPROC

PROC CACHE_DYNO_SHOULD_RESPAWN_THIS_FRAME(INT i)
	SET_BIT(iDynoShouldRespawnNowBS, i)
ENDPROC
		
FUNC BOOL SHOULD_DYNOPROP_SPAWN_NOW(FMMC_DYNOPROP_STATE& sDynopropState)
	
	INT i = sDynopropState.iIndex
	
	IF NOT CAN_WE_SPAWN_A_NETWORKED_OBJECT()
		PRINTLN("[Dynoprops][MCRespawnDynoProp ", i, "] SHOULD_DYNOPROP_SPAWN_NOW | Too many networked objects!!")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iDynoRespawnIsBlockedBS, i)
		PRINTLN("[Dynoprops][MCRespawnDynoProp ", i, "] SHOULD_DYNOPROP_SPAWN_NOW | Respawn is blocked!")
		RETURN FALSE
	ENDIF	
	IF IS_BIT_SET(iDynoRespawnIsBlockedThisFrameBS, i)
		PRINTLN("[Dynoprops][MCRespawnDynoProp ", i, "] SHOULD_DYNOPROP_SPAWN_NOW | Respawn is blocked this frame!")
		RETURN FALSE
	ENDIF	
	IF IS_BIT_SET(iDynoShouldRespawnNowBS, i)
		PRINTLN("[Dynoprops][MCRespawnDynoProp ", i, "] SHOULD_DYNOPROP_SPAWN_NOW | Should respawn now.")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iDynopropBitset, ciFMMC_DYNOPROP_DroppedByEvent)
		IF FMMC_IS_LONG_BIT_SET(iDroppedDynoPropBS, i)
			PRINTLN("[Dynoprops][MCRespawnDynoProp ", i, "] SHOULD_DYNOPROP_SPAWN_NOW | Dropped dyno prop, should respawn.")
			RETURN TRUE
		ELSE
			PRINTLN("[Dynoprops][MCRespawnDynoProp ", i, "] SHOULD_DYNOPROP_SPAWN_NOW | Not dropped dyno prop, should not respawn.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF SHOULD_CLEANUP_DYNOPROP(sDynopropState)	
		CACHE_DYNO_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
		RETURN FALSE
	ENDIF
			
	IF sDynopropState.mn = Prop_Container_LD_PU
	AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iObjReference = -1
		PRINTLN("[MCRespawnDynoProp: ", i, "][Dynoprops] SHOULD_DYNOPROP_SPAWN_NOW - Returning FALSE iObjReference is -1 with model Prop_Container_LD_PU")
		CACHE_DYNO_RESPAWN_BLOCKED(i)
		RETURN FALSE
	ENDIF	
	
	IF ARE_WE_WAITING_FOR_DYNOPROPS_TO_CLEAN_UP()
		CACHE_DYNO_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sTrapInfo_Local.iTrapExistsBS, i)
		PRINTLN("[MCRespawnDynoProp: ", i, "][Dynoprops] SHOULD_DYNOPROP_SPAWN_NOW - Returning FALSE due to iTrapExistsBS - let the trap system handle respawning this one")
		CACHE_DYNO_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
		RETURN FALSE
	ENDIF
				
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_DynoProp)
		PRINTLN("[MCRespawnDynoProp: ", i, "][Dynoprops] SHOULD_DYNOPROP_SPAWN_NOW - RETURNING FALSE - MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP = FALSE")
		CACHE_DYNO_RESPAWN_BLOCKED(i)
		RETURN FALSE
	ENDIF	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_DynoPropDestroyedTracking)
		PRINTLN("[MCRespawnDynoProp: ", i, "][Dynoprops] SHOULD_DYNOPROP_SPAWN_NOW - Has a iContinuityId of ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iContinuityId)
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iContinuityId > -1
			IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iDynoPropDestroyedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iContinuityId)
				PRINTLN("[MCRespawnDynoProp: ", i, "][Dynoprops] SHOULD_DYNOPROP_SPAWN_NOW - RETURNING FALSE - Dynoprop was destroyed in a previous mission")
				CACHE_DYNO_RESPAWN_BLOCKED(i)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PLAYER_PROXIMITY_BLOCKING_SPAWN(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sProximitySpawningData, i, MC_serverBD_3.iProximitySpawning_DynoPropSpawnAllBS, MC_serverBD_3.iProximitySpawning_DynoPropSpawnAnyBS, MC_serverBD_3.iProximitySpawning_DynoPropSpawnOneBS, MC_serverBD_3.iProximitySpawning_DynoPropCleanupBS)
		PRINTLN("[PROXSPAWN_SERVER][MCRespawnDynoProp: ", i, "][Dynoprops] SHOULD_DYNOPROP_SPAWN_NOW - IS_PLAYER_PROXIMITY_BLOCKING_SPAWN")
		CACHE_DYNO_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_DYNOPROPS, i, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iAssociatedSpawn, 
												    g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iAssociatedTeam, 
													g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iAssociatedObjective,
													DEFAULT, DEFAULT, DEFAULT, 
													g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iAggroIndexBS_Entity_DynoProp)
			CACHE_DYNO_SHOULD_RESPAWN_THIS_FRAME(i)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iSecondAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iSecondAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_DYNOPROPS, i, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iSecondAssociatedSpawn, 
												    g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iSecondAssociatedTeam, 
													g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iSecondAssociatedObjective,
													DEFAULT, DEFAULT, DEFAULT, 
													g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iAggroIndexBS_Entity_DynoProp)
			CACHE_DYNO_SHOULD_RESPAWN_THIS_FRAME(i)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iThirdAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iThirdAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_DYNOPROPS, i, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iThirdAssociatedSpawn, 
		                                            g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iThirdAssociatedTeam, 
													g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iThirdAssociatedObjective,
													DEFAULT, DEFAULT, DEFAULT, 
													g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iAggroIndexBS_Entity_DynoProp)
			CACHE_DYNO_SHOULD_RESPAWN_THIS_FRAME(i)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iFourthAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iFourthAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_DYNOPROPS, i, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iFourthAssociatedSpawn, 
											        g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iFourthAssociatedTeam, 
													g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iFourthAssociatedObjective,
													DEFAULT, DEFAULT, DEFAULT, 
													g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iAggroIndexBS_Entity_DynoProp)
			CACHE_DYNO_SHOULD_RESPAWN_THIS_FRAME(i)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iDynoPropShouldRespawnNowBS, i)
		PRINTLN("[MCRespawnDynoProp: ", i, "][Dynoprops] SHOULD_DYNOPROP_SPAWN_NOW - should respawn now")
		CACHE_DYNO_SHOULD_RESPAWN_THIS_FRAME(i)
		RETURN TRUE
	ENDIF
	
	CACHE_DYNO_SHOULD_NOT_RESPAWN_THIS_FRAME(i)
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_DYNOPROP_BE_PUT_ON_GROUND_PROPERLY_WHEN_SPAWNED(INT iPropNumber)

	IF g_FMMC_STRUCT.sPlayerAbilities.sVehicleRepair.iDynopropIndex = iPropNumber
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_UP_DYNOPROP_AUDIO(OBJECT_INDEX oiPassed, MODEL_NAMES mnPassed)
	UNUSED_PARAMETER(oiPassed)
	UNUSED_PARAMETER(mnPassed)
ENDPROC

PROC MC_SET_UP_DYNOPROP(OBJECT_INDEX oiPassed, INT iPropNumber, MODEL_NAMES mnPassed, BOOL bVisible = TRUE)
	SET_ENTITY_COORDS(oiPassed, GET_DYNO_PROP_SPAWN_LOCATION(iPropNumber))
	
	FLOAT fHead = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].fHead
	VECTOR vRotOverride = <<0,0,0>>
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].sDynopropTrainAttachmentData.iTrainIndex != -1
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].sDynopropTrainAttachmentData.vCarriageOffsetPosition)		
		VECTOR vTemp = <<0.0, 0.0, 0.0>>
		VECTOR vTempRot = <<0.0, 0.0, 0.0>>
		MC_GET_TRAIN_ATTACHMENT_POINT_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].sDynopropTrainAttachmentData, vTemp, vTempRot)		
		fHead = vTempRot.z
		vRotOverride = vTempRot
	ENDIF
	
	IF g_FMMC_STRUCT.sPlayerAbilities.sVehicleRepair.iDynopropIndex = iPropNumber
		fHead = sLocalPlayerAbilities.sVehicleRepairData.fVehicleRepairHeading
		vRotOverride.z = fHead
		PRINTLN("[Dynoprops][Dynoprop ", iPropNumber, "][PlayerAbilities][RepairAbility] MC_SET_UP_DYNOPROP - Using fVehicleRepairHeading: ", sLocalPlayerAbilities.sVehicleRepairData.fVehicleRepairHeading, " / vRotOverride: ", vRotOverride)
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vRotOverride)
		SET_ENTITY_ROTATION(oiPassed, vRotOverride)
		PRINTLN("[Dynoprops][Dynoprop ", iPropNumber, "] MC_SET_UP_DYNOPROP - Overriding dynoprop's rotation to ", vRotOverride)
		
	ELIF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].vRot)
		SET_ENTITY_ROTATION(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].vRot)
		PRINTLN("[Dynoprops][Dynoprop ", iPropNumber, "] MC_SET_UP_DYNOPROP - Setting dynoprop's rotation to ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].vRot)
		
	ELSE
		PRINTLN("[Dynoprops][Dynoprop ", iPropNumber, "] MC_SET_UP_DYNOPROP - Setting dynoprop's heading to ", fHead)
		SET_ENTITY_HEADING(oiPassed, fHead)
	ENDIF
	
	SET_ENTITY_VISIBLE(oiPassed, bVisible)
	SET_OBJECT_TARGETTABLE(oiPassed, FALSE)
	
	IF NOT SHOULD_THIS_MODEL_BE_FROZEN(mnPassed)
		SET_ENTITY_INVINCIBLE(oiPassed, FALSE)
		SET_ENTITY_PROOFS(oiPassed, FALSE, FALSE, FALSE, FALSE, FALSE)
		
		IF NOT IS_THIS_MODEL_A_TRAP_DYNO_PROP(mnPassed)
			FREEZE_ENTITY_POSITION(oiPassed, FALSE)
		ENDIF
	ELSE
		SET_ENTITY_INVINCIBLE(oiPassed, TRUE)
		SET_ENTITY_PROOFS(oiPassed, TRUE, TRUE, TRUE, TRUE, TRUE)
		
		IF SHOULD_THIS_MODEL_BE_FROZEN(mnPassed)
		AND NOT IS_THIS_MODEL_A_TRAP_DYNO_PROP(mnPassed)
			FREEZE_ENTITY_POSITION(oiPassed, TRUE)
		ENDIF
	ENDIF
	
	//Set the colour of the prop to the american flags if it is independence day
	IF g_sMPTunables.bEnableAmericanFlagStuntRaces
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		PRINTLN("[LH][FlagColours] g_sMPTunables.bEnableAmericanFlagStuntRaces = TRUE")
		PRINTLN("[LH][FlagColours] Setting g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[", iPropNumber, "] from ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].iPropColour, " to 0")
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].iPropColour = 0
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].iPropColour = -1
		// Random colour
		INT iRandomColour = GET_RANDOM_INT_IN_RANGE(0, GET_PROP_NUMBER_OF_COLOURS(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].mn))
		PRINTLN("[Dynoprops][Dynoprop ", iPropNumber, "] MC_SET_UP_DYNOPROP - Random Colour: ", iRandomColour)
		SET_OBJECT_TINT_INDEX(oiPassed, iRandomColour)
		
	ELSE
		SET_OBJECT_TINT_INDEX(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].iPropColour)
	ENDIF
	
	IF IS_MODEL_TARGETABLE(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].mn)
		SET_OBJECT_TARGETTABLE(oiPassed, TRUE)
	ENDIF
		
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].iPropHealthOverride > 0
		PRINTLN("[Dynoprops][Dynoprop ", iPropNumber, "] MC_SET_UP_DYNOPROP - iPropNumber: ", iPropNumber, " Setting Health to iPropHealthOverride: ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].iPropHealthOverride)
		SET_ENTITY_INVINCIBLE(oiPassed, FALSE)
		SET_ENTITY_CAN_BE_DAMAGED(oiPassed, TRUE)
		SET_ENTITY_PROOFS(oiPassed, FALSE, FALSE, TRUE, FALSE, FALSE)
		SET_ENTITY_HEALTH(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].iPropHealthOverride)
		SET_ENTITY_MAX_HEALTH(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].iPropHealthOverride)
	#IF IS_DEBUG_BUILD
		IF SHOULD_THIS_MODEL_BE_FROZEN(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].mn)
			ASSERTLN("MC_SET_UP_DYNOPROP - iPropNumber = ", iPropNumber, " - This model is frozen and should use default health settings!")
		ENDIF
	#ENDIF
	ENDIF

	IF GET_MODEL_NAME_OF_CHILD_PROP_FOR_DYNO(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].mn) != DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("[LM] MC_SET_UP_DYNOPROP - Freezing Prop")
		FREEZE_ENTITY_POSITION(oiPassed, TRUE)
	ENDIF
	
	IF IS_THIS_MODEL_A_CCTV_CAMERA(mnPassed)
		SET_ENTITY_PROOFS(oiPassed, FALSE, FALSE, TRUE, TRUE, FALSE)
		FREEZE_ENTITY_POSITION(oiPassed, FALSE)
		PRINTLN("[CCTV] MC_SET_UP_DYNOPROP - Setting dynoprop ", iPropNumber, " to have CCTV proofs")
		
		IF NETWORK_IS_ACTIVITY_SESSION()
			NETWORK_ENTITY_USE_HIGH_PRECISION_ROTATION(OBJ_TO_NET(oiPassed), TRUE)
			PRINTLN("[CCTV] MC_SET_UP_DYNOPROP - Setting NETWORK_ENTITY_USE_HIGH_PRECISION_ROTATION on dynoprop ", iPropNumber)
		ENDIF
	ENDIF
	
	IF IS_THIS_MODEL_A_TRAP_DYNO_PROP(GET_ENTITY_MODEL(oiPassed))
		INIT_ARENA_TRAP(oiPassed, iPropNumber)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].fDynoPropLODDistance > 0.0
		PRINTLN("MC_SET_UP_DYNOPROP - Setting LOD Distance to ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].fDynoPropLODDistance)
		SET_ENTITY_LOD_DIST(oiPassed, ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].fDynoPropLODDistance))
	ENDIF
	
	IF SHOULD_DYNOPROP_BE_PUT_ON_GROUND_PROPERLY_WHEN_SPAWNED(iPropNumber)
		PRINTLN("MC_SET_UP_DYNOPROP - Putting dynoprop ", iPropNumber, " on ground properly")
		PLACE_OBJECT_ON_GROUND_PROPERLY(oiPassed)
	ENDIF
	
	IF DOES_MODEL_HAVE_A_SHARED_RENDER_TARGET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].mn)
		BROADCAST_FMMC_SHARED_RENDERTARGET_EVENT(ciSHARED_RT_EVENT_TYPE__REGISTER_RENDERTARGET, ciENTITY_TYPE_DYNO_PROPS, iPropNumber, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].mn)
	ENDIF
	
	SET_UP_DYNOPROP_AUDIO(oiPassed, mnPassed)
	
	RETAIN_ENTITY_INSIDE_ITS_CURRENT_INTERIOR(oiPassed)
	
	MC_SET_ID_INT_DECOR_ON_ENTITY(oiPassed, GET_DYNOPROP_NET_ID_INDEX(iPropNumber))
	
ENDPROC

PROC PROCESS_DYNO_PROP_RESPAWNING_SPECIAL(FMMC_DYNOPROP_STATE& sDynopropState)

	INT iDynoprop = sDynopropState.iIndex	
	IF NOT IS_MODEL_SPECIAL_RACES_DESTRUCTIBLE_PROP(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn)
		EXIT
	ENDIF	
	IF NOT CHECK_ALL_PLAYERS_EXCEED_DISTANCE_FROM_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].vPos, ciSPECIAL_RACES_DESTRUCTIBLE_PROP_RESPAWN_DISTANCE)
		EXIT
	ENDIF
	
	REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn)				
	IF NOT HAS_MODEL_LOADED(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn)
		PRINTLN("[RCC MISSION][Dynoprops][Dynoprop ", iDynoprop, "] PROCESS_DYNO_PROP_RESPAWNING_SPECIAL - iDynoprop = ", iDynoprop, " Waiting for Prop to load. Returning FALSE.")
		EXIT
	ENDIF	
	
	IF FMMC_CREATE_NET_OBJ(sDynopropState.niIndex, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].vPos, TRUE, TRUE, TRUE, DEFAULT, DEFAULT, TRUE, DEFAULT, TRUE)
		MC_SET_UP_DYNOPROP(NET_TO_OBJ(sDynopropState.niIndex), iDynoprop, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn, TRUE)
		SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn)
	ENDIF		
ENDPROC 

FUNC BOOL MC_CREATE_DYNOPROPS()
									
	INT i

	//Don't stagger on fade transitions
	BOOL bStrand = IS_A_STRAND_MISSION_BEING_INITIALISED() AND (GET_STRAND_MISSION_TRANSITION_TYPE() != FMMC_BRANCHING_MISSION_TRANSITION_TYPE_FADEOUT)
	BOOL bVisible	
	IF bStrand
		bVisible = TRUE
	ENDIF
	INT iRequestThisFrame
	
	IF MC_REQUEST_LOAD_MODELS()
		REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps 	 i
			
			IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iRespawnOnRule, -1)
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ENTITIES_CAN_RESPAWN_ON_RULE_CHANGE)
				SET_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_ENTITIES_CAN_RESPAWN_ON_RULE_CHANGE)
			ENDIF
			
			MODEL_NAMES mnDynopropModel = GET_DYNOPROP_MODEL_TO_USE(i)
			
			IF MC_SHOULD_DYNOPROP_SPAWN_AT_START(i)
				IF mnDynopropModel != DUMMY_MODEL_FOR_SCRIPT
				AND mnDynopropModel != Prop_Container_LD_PU
				AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iObjReference = -1
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(GET_DYNOPROP_NET_ID(i))
						IF FMMC_CREATE_NET_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_DYNOPROP_NET_ID_INDEX(i)], 
									mnDynopropModel, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].vPos, TRUE, TRUE, TRUE, DEFAULT, DEFAULT, TRUE, DEFAULT, TRUE)
									
							MC_SET_UP_DYNOPROP(NET_TO_OBJ(GET_DYNOPROP_NET_ID(i)), i, mnDynopropModel, bVisible)
							#IF IS_DEBUG_BUILD
								PRINTLN("[MCSpawning][Dynoprops][Dynoprop ", i, "] - MC_CREATE_DYNOPROPS - DYNO PROPS CREATED")
							#ENDIF
							
							MC_serverBD_1.sCreatedCount.iNumObjCreated[0]++
							SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE(GET_DYNOPROP_NET_ID(i), TRUE)
							
							IF bStrand
								IF iRequestThisFrame >= PICK_INT(IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING(), ciFMMC_MAX_PER_FRAME_CREATES_AIRLOCK, ciFMMC_MAX_PER_FRAME_CREATES)
									RETURN FALSE
								ELSE
									iRequestThisFrame++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[MCSpawning][Dynoprops][Dynoprop ", i, "] - MC_CREATE_DYNOPROPS - failing on model = ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_DYNOPROP_MODEL_TO_USE(i)))
					
					IF mnDynopropModel = DUMMY_MODEL_FOR_SCRIPT
						PRINTLN("[MCSpawning][Dynoprops][Dynoprop ", i, "] - MC_CREATE_DYNOPROPS - DUMMY_MODEL_FOR_SCRIPT ")
					ELIF mnDynopropModel = Prop_Container_LD_PU
						PRINTLN("[MCSpawning][Dynoprops][Dynoprop ", i, "] - MC_CREATE_DYNOPROPS - Prop_Container_LD_PU ")
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iObjReference = -1
						PRINTLN("[MCSpawning][Dynoprops][Dynoprop ", i, "] - MC_CREATE_DYNOPROPS - iObjReference = -1 ")
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Check we have created all the things
		REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps i
		
			IF MC_SHOULD_DYNOPROP_SPAWN_AT_START(i)
				
				MODEL_NAMES mnDynopropModel = GET_DYNOPROP_MODEL_TO_USE(i)
				
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(GET_DYNOPROP_NET_ID(i))
				AND mnDynopropModel != DUMMY_MODEL_FOR_SCRIPT
				AND mnDynopropModel != Prop_Container_LD_PU
				AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iObjReference = -1
					RETURN FALSE
				ENDIF
			
			ENDIF
			
		ENDREPEAT
	ELSE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cutscene (Utility and Helper Functions)
// ##### Description: Creation/Startup Mission Controller Utility functions.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_CUTSCENE_ENTITY_BE_SPAWNED(INT iEntityType, INT iEntityIndex)
	
	FMMC_OBJECT_STATE sObjState
	FMMC_VEHICLE_STATE sVehState
	FMMC_DYNOPROP_STATE sDynopropState
	
	SWITCH iEntityType
		CASE CREATION_TYPE_PEDS
			IF MC_serverBD_2.iCurrentPedRespawnLives[iEntityIndex] > 0
				FMMC_PED_STATE sPedState
				FILL_FMMC_PED_STATE_STRUCT(sPedState, iEntityIndex)
				IF SHOULD_PED_RESPAWN_NOW(sPedState)
					IF NOT sPedState.bExists
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF					
		BREAK		
		CASE CREATION_TYPE_VEHICLES
			IF GET_VEHICLE_RESPAWNS(iEntityIndex) > 0
				FILL_FMMC_VEHICLE_STATE_STRUCT(sVehState, iEntityIndex, TRUE)
				IF SHOULD_VEH_RESPAWN_NOW(sVehState)		
					IF NOT sVehState.bExists
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE CREATION_TYPE_OBJECTS
			IF MC_serverBD_2.iCurrentObjRespawnLives[iEntityIndex] > 0
				IF SHOULD_OBJ_RESPAWN_NOW(iEntityIndex, sObjState)
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iEntityIndex))
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE CREATION_TYPE_DYNOPROPS
			FILL_FMMC_DYNOPROP_STATE_STRUCT(sDynopropState, iEntityIndex)
			IF SHOULD_DYNOPROP_SPAWN_NOW(sDynopropState)
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(GET_DYNOPROP_NET_ID(iEntityIndex))
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Doors (Utility and Helper Functions)
// ##### Description: Process the creation of Mission Controller Doors.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Doors
// ##### Description: Process the creation of Mission Controller Doors.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Pickups & Weapons (Utility and Helper Functions)
// ##### Description: Process the creation of Mission Controller Pickups & Weapons.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC MC_SET_WEAPON_BLIP_NAME_FROM_PICKUP_TYPE(BLIP_INDEX blipToUse, PICKUP_TYPE ptWeapon)
	IF DOES_BLIP_EXIST(blipToUse)
		TEXT_LABEL_15 tlWepName
		tlWepName = GET_WEAPON_NAME(FMMC_GET_WEAPON_TYPE_FROM_PICKUP_TYPE(ptWeapon, TRUE), FALSE)
		IF NOT IS_STRING_NULL_OR_EMPTY(tlWepName)
		AND NOT ARE_STRINGS_EQUAL(tlWepName, "WT_INVALID")
			SET_BLIP_NAME_FROM_TEXT_FILE(blipToUse, tlWepName)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL MC_IS_PICKUP_TYPE_INVALID_NAME(PICKUP_TYPE pt)
	IF pt != PICKUP_ARMOUR_STANDARD					
	AND pt != PICKUP_HEALTH_STANDARD
	AND pt != PICKUP_PARACHUTE
	AND pt != PICKUP_CUSTOM_SCRIPT
	AND pt != PICKUP_VEHICLE_HEALTH_STANDARD
	AND pt != PICKUP_VEHICLE_HEALTH_STANDARD_LOW_GLOW
	AND pt != PICKUP_VEHICLE_CUSTOM_SCRIPT
	AND pt != PICKUP_WEAPON_DLC_FERTILIZERCAN
		PRINTLN("[5669725] MC_IS_PICKUP_TYPE_INVALID_NAME returning TRUE because pt is ", GET_WEAPON_NAME(GET_WEAPON_TYPE_FROM_PICKUP_TYPE(pt)))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MC_SHOULD_WEP_SPAWN_NOW(INT iWep)
	
	BOOL bReturn = FALSE

	IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_WEAPONS, iWep, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iAssociatedObjective, DEFAULT, DEFAULT, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iAggroIndexBS_Entity_Weapon)
			bReturn = TRUE
		ENDIF
	ELSE
		bReturn = TRUE
	ENDIF
	
	IF NOT bReturn // If we already know it's going to spawn, don't bother with checking again
	AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iSecondAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iSecondAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_WEAPONS, iWep, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iSecondAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iSecondAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iSecondAssociatedObjective, DEFAULT, DEFAULT, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iAggroIndexBS_Entity_Weapon)
			bReturn = TRUE
		ENDIF
	ENDIF
	
	IF NOT bReturn
	AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iThirdAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iThirdAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_WEAPONS, iWep, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iThirdAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iThirdAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iThirdAssociatedObjective, DEFAULT, DEFAULT, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iAggroIndexBS_Entity_Weapon)
			bReturn = TRUE
		ENDIF
	ENDIF
	
	IF NOT bReturn
	AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iFourthAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iFourthAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_WEAPONS, iWep, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iFourthAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iFourthAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iFourthAssociatedObjective, DEFAULT, DEFAULT, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iAggroIndexBS_Entity_Weapon)
			bReturn = TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iPlacedBitset, ciFMMC_WEP_SPAWN_IF_OPPONENT_SCORES)
		IF MC_serverBD_3.iTeamToScoreLast > -1
			IF MC_serverBD_3.iTeamToScoreLast != MC_playerBD[iPartToUse].iteam
			AND MC_IS_THIS_PICKUP_VISIBLE_TO_THIS_TEAM(iWep, MC_playerBD[iPartToUse].iteam)
				bReturn = TRUE
			ELSE
				bReturn = FALSE
			ENDIF
		ELSE
			bReturn = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iPlacedBitset, ciFMMC_WEP_ONLY_SPAWN_WHEN_LOSING)
		IF MC_serverBD.iWinningTeam != MC_playerBD[iPartToUse].iteam
		AND MC_IS_THIS_PICKUP_VISIBLE_TO_THIS_TEAM(iWep, MC_playerBD[iPartToUse].iteam)
			bReturn = TRUE
		ELSE
			bReturn = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciCONDEMNED_MODE_ENABLED)
		bReturn = MC_IS_THIS_PICKUP_VISIBLE_TO_THIS_TEAM(iWep, MC_playerBD[iPartToUse].iteam)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iPickupSpawnWithEntity_Type != ciENTITY_TYPE_NONE
		IF NOT DOES_ENTITY_EXIST(GET_FMMC_ENTITY(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iPickupSpawnWithEntity_Type, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iPickupSpawnWithEntity_Index))
			bReturn = FALSE
			PRINTLN("[Pickups][Pickup ", iWep, "] MC_SHOULD_WEP_SPAWN_NOW | Waiting for SpawnWith entity to exist. Type: ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iPickupSpawnWithEntity_Type, " / Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iPickupSpawnWithEntity_Index)
		ENDIF
	ENDIF
	
	//Spawngroup
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iWep, eSGET_Weapon)
		bReturn = FALSE
	ENDIF
	
	RETURN bReturn
	
ENDFUNC

FUNC INT MC_GET_TEAM_TO_USE_FOR_PICKUP_CHECK(BOOL bUseCoronaTeam, INT PlayerID)
	IF bUseCoronaTeam
		RETURN GlobalplayerBD_FM[PlayerID].sClientCoronaData.iTeamChosen
	ELSE
		RETURN MC_playerBD[PlayerID].iTeam
	ENDIF
ENDFUNC

FUNC VECTOR MC_GET_ROTATION_FOR_MISSION_PICKUP(INT iPickup)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPickupSpawnWithEntity_Type != ciENTITY_TYPE_NONE
		ENTITY_INDEX eiEntityToSpawnWith = GET_FMMC_ENTITY(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPickupSpawnWithEntity_Type, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPickupSpawnWithEntity_Index)
		
		IF DOES_ENTITY_EXIST(eiEntityToSpawnWith)
			VECTOR vSpawnWithRotation = GET_ENTITY_ROTATION(eiEntityToSpawnWith)
			vSpawnWithRotation += g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].vPickupSpawnWithEntity_LocalRotation
			PRINTLN("[Pickups][Pickup ", iPickup, "] MC_GET_ROTATION_FOR_MISSION_PICKUP | Spawning with entity. Type: ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPickupSpawnWithEntity_Type, " | Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPickupSpawnWithEntity_Index, " || vSpawnWithRotation: ", vSpawnWithRotation)
			RETURN vSpawnWithRotation
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPlacedBitset, ciFMMC_WEP_Rotation_Override)
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].vRot
	ENDIF
	
	RETURN <<0,360,0>>
ENDFUNC

PROC MC_SET_PLACEMENT_FLAGS_FOR_MISSION_PICKUPS(INT iPickup, INT &iPlacementFlags)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPlacedBitset, ciFMMC_WEP_Rotation_Override)
		CLEAR_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ROTATE))
		PRINTLN("[WEAPON PICKUPS] MC_SET_PLACEMENT_FLAGS_FOR_MISSION_PICKUPS - Rotation Overridden - Rotation disabled - ", iPickup)
	ELSE
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ROTATE))
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_STATIC_PICKUPS)
		CLEAR_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ROTATE))
		PRINTLN("[WEAPON PICKUPS] MC_SET_PLACEMENT_FLAGS_FOR_MISSION_PICKUPS - ciENABLE_STATIC_PICKUPS - Rotation disabled - ", iPickup)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPlacedBitset, ciFMMC_WEP_Can_Pickup_in_vehicle)
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_CREATION_FLAG_COLLECTABLE_IN_VEHICLE))
		PRINTLN("[WEAPON PICKUPS] MC_SET_PLACEMENT_FLAGS_FOR_MISSION_PICKUPS setting PLACEMENT_CREATION_FLAG_COLLECTABLE_IN_VEHICLE for weapon ", iPickup)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPlacedBitset, ciFMMC_WEP_Grounded)
		CLEAR_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ROTATE))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
		PRINTLN("[WEAPON PICKUPS] MC_SET_PLACEMENT_FLAGS_FOR_MISSION_PICKUPS - Stopping rotation. Snapping and Orienting pickup to ground. - ", iPickup)
	ENDIF
	PRINTLN("[WEAPON PICKUPS] MC_SET_PLACEMENT_FLAGS_FOR_MISSION_PICKUPS - iPlacementFlags: ", iPlacementFlags)
ENDPROC

FUNC BOOL IS_CONTINUITY_BLOCKING_PICKUP_SPAWN(INT iPickup)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_WeaponPickupTracking)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iContinuityId = -1
		RETURN FALSE
	ENDIF
	
	IF NOT FMMC_IS_LONG_BIT_SET(sMissionLocalContinuityVars.iPickupCollectedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iContinuityId)
		RETURN FALSE
	ENDIF
	
	PRINTLN("[JS][CONTINUITY] - IS_CONTINUITY_BLOCKING_PICKUP_SPAWN - Pickup ", iPickup, " blocked")
	RETURN TRUE
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Pickups & Weapons
// ##### Description: Process the creation of Mission Controller Pickups & Weapons.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC VECTOR GET_FMMC_PICKUP_COORDS(INT i)
		
	VECTOR vPos = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedBitset, ciFMMC_WEP_DroppedByEvent)
	AND NOT IS_VECTOR_ZERO(vDroppedPickupLocation[i])
		vPos = vDroppedPickupLocation[i]
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupSpawnWithEntity_Type != ciENTITY_TYPE_NONE
		ENTITY_INDEX eiEntityToSpawnWith = GET_FMMC_ENTITY(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupSpawnWithEntity_Type, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupSpawnWithEntity_Index)
		
		IF DOES_ENTITY_EXIST(eiEntityToSpawnWith)
			vPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiEntityToSpawnWith, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPickupSpawnWithEntity_LocalOffset)
			PRINTLN("[Pickups_SPAM][Pickup_SPAM ", i, "] GET_FMMC_PICKUP_COORDS | Spawning with entity. Type: ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupSpawnWithEntity_Type, " | Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupSpawnWithEntity_Index)
		ENDIF
	ENDIF
	
	RETURN vPos
ENDFUNC

FUNC BOOL MC_CREATE_WEAPON(INT i, INT iTeam, INT iPlacementFlags)
	
	VECTOR vPos = GET_FMMC_PICKUP_COORDS(i)
		
	MODEL_NAMES mn
	
	IF NOT IS_VECTOR_ZERO(vPos)
	AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt <> NUM_PICKUPS
		
		PICKUP_TYPE tempPickup = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt
		
		IF IS_PICKUP_WEAPON_MK2(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt)
			SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_CREATION_FLAG_FORCE_DEFERRED_MODEL))
		ELSE
			CLEAR_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_CREATION_FLAG_FORCE_DEFERRED_MODEL))
		ENDIF
		
		PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON Making pickup: ", i, " at the coordinates: ",vPos)
	
		INT iammo
		IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iClips != 0
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciMINIGUN_AMMO_OVERRIDE)
			AND tempPickup = PICKUP_WEAPON_MINIGUN
				iammo = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iClips * 100
				PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON ammo override - Minigun special override. Ammo = ", iammo)
			ELSE
				iammo = GET_AMMO_IN_WEAPON_NUM_CLIPS(GET_WEAPON_TYPE_FROM_PICKUP_TYPE(tempPickup), g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iClips)
				PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON ammo override: ", iammo)
			ENDIF
		ELSE
			iammo = GET_AMMO_AMOUNT_FOR_MP_PICKUP(GET_WEAPON_TYPE_FROM_PICKUP_TYPE(tempPickup))
			PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON standard ammo: ", iammo)
		ENDIF
		
		IF SHOULD_PICKUP_BE_SPAWNED_AS_AMMO(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedBitset)
			tempPickup = GET_AMMO_TYPE_FROM_WEAPON_TYPE(GET_WEAPON_TYPE_FROM_PICKUP_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt))
			PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON Making pickup: ", i, " into an ammo box (Pickup Type ", tempPickup, ") due to SHOULD_PICKUP_BE_SPAWNED_AS_AMMO")
		ENDIF
		
		INT iTempCustomPickupType = -1
		INT iTempVehicleWeaponPickupType = -1
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt = PICKUP_CUSTOM_SCRIPT
			iTempCustomPickupType = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupType
			iTempVehicleWeaponPickupType = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehicleWeaponPickupType
		ENDIF
		
		//Make Health/Armour Full
		IF tempPickup = PICKUP_ARMOUR_STANDARD
		OR tempPickup = PICKUP_HEALTH_STANDARD
		OR tempPickup = PICKUP_CUSTOM_SCRIPT
			iammo = 500
			PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON Health/Armour ammo: ", iammo)
		ENDIF
		
		PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON - Pickup ammo: ", iammo)
	
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedBitset, ciPLACED_WEAPON_LOCAL_ONLY)
			PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON - ciPLACED_WEAPON_LOCAL_ONLY set for pickup ", i)
			SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY))
		ELSE
			CLEAR_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY))
		ENDIF
							
		PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON - Weapon Name: ", GET_WEAPON_NAME(FMMC_GET_WEAPON_TYPE_FROM_PICKUP_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt, TRUE)))
		
		//set the type to the random type if the option has been switched on
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciRANDOM_PICKUP_TYPES_OPTION)
		AND g_FMMC_STRUCT_ENTITIES.bRandomPickupTypes = TRUE
			PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON g_FMMC_STRUCT_ENTITIES.bRandomPickupTypes is TRUE - Setting to random pickup types")
			IF iTempCustomPickupType > -1
				iTempCustomPickupType = 7
			ENDIF
		ENDIF
		
		BOOL bHasLoadedModel = TRUE
		IF iTempCustomPickupType != -1
			mn = MC_GET_MODEL_FOR_CUSTOM_PICKUP(iTempCustomPickupType, iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupModel)
			IF IS_MODEL_VALID(mn)
				REQUEST_MODEL(mn)
			ENDIF
			PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON - Checking model ", FMMC_GET_MODEL_NAME_FOR_DEBUG(mn), " has loaded")
			IF NOT HAS_MODEL_LOADED(mn)
				bHasLoadedModel = FALSE
			ENDIF
		ENDIF

		VECTOR vRotation = MC_GET_ROTATION_FOR_MISSION_PICKUP(i)
		
		MC_SET_PLACEMENT_FLAGS_FOR_MISSION_PICKUPS(i, iPlacementFlags)
		
		IF bHasLoadedModel
			IF iTempCustomPickupType = -1
			AND iTempVehicleWeaponPickupType = -1
			AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt != PICKUP_CUSTOM_SCRIPT
			
				VECTOR vOffset = <<0,0,0.10>>
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedBitset, ciFMMC_WEP_Position_Override)
					vOffset = <<0,0,0>>
				ENDIF
				
				pipickup[i] = CREATE_PICKUP_ROTATE(tempPickup, vPos + vOffset, vRotation, iPlacementFlags, iammo, EULER_YXZ, FALSE)
				PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON - [", i, "] Pickup created - Weapon/Armour/Health pickup type: ", ENUM_TO_INT(tempPickup))
			
			ELSE
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupType = ciCUSTOM_PICKUP_TYPE__MISSION_EQUIPMENT
					CLEAR_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES))

					PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON - [", i, "] Mission Equipment Pickup created - Mission Equipment Type: ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupModel, " | iMissionEquipmentPickupsMax[", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupModel, "]: ", iMissionEquipmentPickupsMax[g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupModel], " || iPlacementFlags: ", iPlacementFlags)
					pipickup[i] = CREATE_PICKUP_ROTATE(tempPickup, vPos, vRotation, iPlacementFlags, 1, EULER_YXZ, FALSE, MC_GET_MODEL_FOR_CUSTOM_PICKUP(iTempCustomPickupType, iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupModel))
					
				ELIF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupType != ciCUSTOM_PICKUP_TYPE__NONE
					PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON - [", i, "] Pickup created - Rage pickup type: ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupType, " iPlacementFlags: ", iPlacementFlags)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupType = ciCUSTOM_PICKUP_TYPE__PLAYER_LIVES
						vPos.z -= 0.4
					ENDIF
					
					pipickup[i] = CREATE_PICKUP_ROTATE(tempPickup, vPos + <<0,0,0.40>>, vRotation, iPlacementFlags, iammo, eULER_YXZ, FALSE, MC_GET_MODEL_FOR_CUSTOM_PICKUP(iTempCustomPickupType, iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupModel))
				
				ELIF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehicleWeaponPickupType != -1
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_PICKUPS_CORONA_OPTION) AND g_FMMC_STRUCT.bAllowPickUpsCorona
					OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_PICKUPS_CORONA_OPTION)
						PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON - [", i, "] Pickup created - Vehicle Weapon type: ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehicleWeaponPickupType)
						
						MODEL_NAMES mnPickupModel = GET_MODEL_FOR_VEHICLE_WEAPON(iTempVehicleWeaponPickupType, iTeam, i)
						FLOAT fZOffset = 0.40
						
						IF IS_DROP_THE_BOMB_PICKUP(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehicleWeaponPickupType)
							fZOffset = 3.0
						ENDIF
						
						pipickup[i] = CREATE_PICKUP_ROTATE(tempPickup, vPos + <<0.0, 0.0, fZOffset>>, vRotation, iPlacementFlags, iAmmo, EULER_YXZ, FALSE, mnPickupModel)
						
						IF GET_MODEL_FOR_VEHICLE_WEAPON(iTempVehicleWeaponPickupType, iTeam, i) = INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton"))
							PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON Detonator pickup index: ", NATIVE_TO_INT(pipickup[i]))
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON - [", i, "] Pickup created - Weapon/Armour/Health pickup type: ", ENUM_TO_INT(tempPickup))
					pipickup[i] = CREATE_PICKUP_ROTATE(tempPickup, vPos + <<0,0,0.10>>, vRotation, iPlacementFlags, iammo, eULER_YXZ, FALSE, MC_GET_MODEL_FOR_CUSTOM_PICKUP(iTempCustomPickupType, iTeam, i))
				ENDIF
			ENDIF
			
			//Pickup Setup
			IF DOES_PICKUP_EXIST(pipickup[i])
				SET_PICKUP_GLOW_OFFSET(pipickup[i], 0.51)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iWepRespawnCount = -1
					SET_PICKUP_REGENERATION_TIME(pipickup[i], GET_MC_SPAWN_TIME_FROM_INT(g_FMMC_STRUCT_ENTITIES.iWeaponRespawnTime))
				ENDIF
			ENDIF
										
			IF (g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].fDamageMultiplier != 1.0)
				SET_WEAPON_DAMAGE_MODIFIER(GET_WEAPON_TYPE_FROM_PICKUP_TYPE(tempPickup), g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].fDamageMultiplier)
				PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON increasing damage modifier of ", GET_WEAPON_NAME_FROM_PT(tempPickup), " to ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].fDamageMultiplier)
			ENDIF
			
			// Flush pickup history.
			IF HAS_PICKUP_BEEN_COLLECTED(pipickup[i])
				PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON HAS_PICKUP_BEEN_COLLECTED was True, Flushed.")
			ELSE
				PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON HAS_PICKUP_BEEN_COLLECTED was False, No need to Flush..")
			ENDIF
			
			g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].id = pipickup[i]			
			
			FMMC_CLEAR_LONG_BIT(iDroppedPickupBS, i)
			
			//Stops the pickup from being collectable
			IF NOT MC_SHOULD_WEP_SPAWN_NOW(i)
			OR NOT MC_IS_THIS_PICKUP_VISIBLE_TO_THIS_TEAM(i, iTeam)
				PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON - [", i, "] Making invisible and uncollectable")
				SET_PICKUP_UNCOLLECTABLE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].id, TRUE)
				SET_PICKUP_HIDDEN_WHEN_UNCOLLECTABLE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].id, TRUE)
			ELSE
				PRINTLN("[MCSpawning][Pickups][Pickup ",i,"] - MC_CREATE_WEAPON - [", i, "] Making visible and collectable")
				FMMC_SET_LONG_BIT(iActiveWeapons, i)
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CLEANUP_PICKUP(FMMC_PICKUP_STATE &sPickupState)

	IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
	AND IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iPlacedBitset, ciFMMC_WEP_CleanupAtMissionEnd )
		RETURN TRUE
	ENDIF

	IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iCleanupObjective !=-1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iCleanupTeam !=-1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iCleanupObjective <= MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iCleanupTeam]
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iPlacedBitset, ciFMMC_WEP_SPAWN_IF_OPPONENT_SCORES)
		IF MC_serverBD_3.iTeamToScoreLast = MC_playerBD[iPartToUse].iteam
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iPlacedBitset, ciFMMC_WEP_ONLY_SPAWN_WHEN_LOSING)
		IF MC_serverBD.iWinningTeam = MC_playerBD[iPartToUse].iteam
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck17, LBOOL17_TEAM_SWAP_PICKUP_PROCESS)
		IF MC_playerBD[iPartToUse].iTeam = 0	//Losing Team
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(sPickupState.iIndex, eSGET_Weapon)
		PRINTLN("[Pickups][Pickup ", sPickupState.iIndex, "]  SHOULD_CLEANUP_PICKUP - pickup no longer has a valid spawn group active.") 
		RETURN TRUE
	ENDIF		
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iPlacedBitset, ciFMMC_WEP_DestroyGloballyWhenCollected)
	AND FMMC_IS_LONG_BIT_SET(iPickupCollected, sPickupState.iIndex)
		PRINTLN("[Pickups][Pickup ", sPickupState.iIndex, "]  SHOULD_CLEANUP_PICKUP - Returning TRUE due to iPickupCollected & ciFMMC_WEP_DestroyGloballyWhenCollected")
		RETURN TRUE	
	ENDIF
		
	IF (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iPlacedBitset, ciFMMC_WEP_DestroyLocallyWhenCollected)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iPlacedBitset, ciFMMC_WEP_DestroyGloballyWhenCollected))
	AND FMMC_IS_LONG_BIT_SET(iPickupCollectedCleanupLocally, sPickupState.iIndex)
		PRINTLN("[Pickups][Pickup ", sPickupState.iIndex, "]  SHOULD_CLEANUP_PICKUP - Returning TRUE due to iPickupCollectedCleanupLocally")
		RETURN TRUE	
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_PICKUP_RESPAWN(INT i)
	
	IF IS_CONTINUITY_BLOCKING_PICKUP_SPAWN(i)
		RETURN FALSE
	ENDIF
		
	IF MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].eSpawnConditionFlag, FALSE, FALSE, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iAggroIndexBS_Entity_Weapon, MC_PlayerBD[iPartToUse].iTeam)
		RETURN FALSE
	ENDIF
		
	IF (GET_NUMBER_OF_PLAYING_PLAYERS() < g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupSpawnTotalNumberOfPlayers
	AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupSpawnTotalNumberOfPlayers != 0)
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		RETURN FALSE
	ENDIF
	
	IF NOT MC_SHOULD_WEP_SPAWN_NOW(i)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedBitset, ciFMMC_WEP_DroppedByEvent)
	AND NOT FMMC_IS_LONG_BIT_SET(iDroppedPickupBS, i)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_PICKUP_SPAWN_AT_START(INT i)
	
	IF IS_CONTINUITY_BLOCKING_PICKUP_SPAWN(i)
		RETURN FALSE
	ENDIF
	
	IF MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].eSpawnConditionFlag, TRUE, FALSE, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iAggroIndexBS_Entity_Weapon, MC_PlayerBD[iPartToUse].iTeam)
		RETURN FALSE
	ENDIF
	
	IF (GET_NUMBER_OF_PLAYING_PLAYERS() < g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupSpawnTotalNumberOfPlayers
	AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupSpawnTotalNumberOfPlayers != 0)
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedBitset, ciFMMC_WEP_DroppedByEvent)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupSpawnWithEntity_Type != ciENTITY_TYPE_NONE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC CLEANUP_AND_DESTROY_PICKUP(FMMC_PICKUP_STATE &sPickupState)
	
	PRINTLN("[Pickups][Pickup ",sPickupState.iIndex,"] - CLEANUP_AND_DESTROY_PICKUP - Cleaning up this pickup for good. (Destroying)")
	
	IF DOES_PICKUP_EXIST(sPickupState.puiIndex)
		PRINTLN("[Pickups][Pickup ",sPickupState.iIndex,"] - CLEANUP_AND_DESTROY_PICKUP - Setting as uncollectable.")
		SET_PICKUP_UNCOLLECTABLE(sPickupState.puiIndex, TRUE)
		SET_PICKUP_HIDDEN_WHEN_UNCOLLECTABLE(sPickupState.puiIndex, TRUE)
	ENDIF	
	IF DOES_BLIP_EXIST(biPickup[sPickupState.iIndex])
		PRINTLN("[Pickups][Pickup ",sPickupState.iIndex,"] - CLEANUP_AND_DESTROY_PICKUP - Removing Blip for Pickup.")
		REMOVE_BLIP(biPickup[sPickupState.iIndex])
	ENDIF
	IF DOES_ENTITY_EXIST(oiLargePowerups[sPickupState.iIndex])
		PRINTLN("[Pickups][Pickup ",sPickupState.iIndex,"] - CLEANUP_AND_DESTROY_PICKUP - Removing #LARGE# Pickup.")
		DELETE_OBJECT(oiLargePowerups[sPickupState.iIndex])
	ENDIF	
	IF DOES_PICKUP_OBJECT_EXIST(pipickup[sPickupState.iIndex])
		OBJECT_INDEX puObject = GET_PICKUP_OBJECT(pipickup[sPickupState.iIndex])
		IF DOES_ENTITY_EXIST(puObject)
			PRINTLN("[Pickups][Pickup ",sPickupState.iIndex,"] - CLEANUP_AND_DESTROY_PICKUP - Removing Pickup Object.")
			DELETE_OBJECT(puObject)
		ENDIF
	ENDIF
	IF DOES_PICKUP_EXIST(pipickup[sPickupState.iIndex])
		PRINTLN("[Pickups][Pickup ",sPickupState.iIndex,"] - CLEANUP_AND_DESTROY_PICKUP - Removing Pickup.")
		REMOVE_PICKUP(pipickup[sPickupState.iIndex])
	ENDIF
	
	FMMC_CLEAR_LONG_BIT(iPickupCollected, sPickupState.iIndex)
	FMMC_CLEAR_LONG_BIT(iPickupCollectedCleanupLocally, sPickupState.iIndex)
	FMMC_CLEAR_LONG_BIT(iSpawnedWeapons, sPickupState.iIndex)
	FMMC_CLEAR_LONG_BIT(iActiveWeapons, sPickupState.iIndex)
	FMMC_CLEAR_LONG_BIT(iDroppedPickupBS, sPickupState.iIndex)
	vDroppedPickupLocation[sPickupState.iIndex] = <<0.0, 0.0, 0.0>> 

ENDPROC

PROC RESPAWN_MISSION_PICKUP(FMMC_PICKUP_STATE &sPickupState)
	
	PRINTLN("[MCSpawning][Pickups][Pickup ",sPickupState.iIndex,"] - RESPAWN_MISSION_PICKUP Calling function.")
	
	INT iPlacementFlags = 0
	IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sPickupState.iIndex].iWepRespawnCount = -1
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES))
	ENDIF
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_MAP)) 
	
	SET_FM_PICKUP_ORIENTATION_FLAGS(iPlacementFlags)
	
	IF MC_CREATE_WEAPON(sPickupState.iIndex, MC_playerBD[iPartToUse].iTeam, iPlacementFlags)
		FMMC_SET_LONG_BIT(iSpawnedWeapons, sPickupState.iIndex)
		iPickupRespawns[sPickupState.iIndex]--
		PRINTLN("[MCSpawning][Pickups][Pickup ",sPickupState.iIndex,"] - RESPAWN_MISSION_PICKUP Respawned Pickup. Respawns Left: ", iPickupRespawns[sPickupState.iIndex])
	ENDIF
	
ENDPROC

PROC PROCESS_RESPAWNING_MISSION_PICKUPS(FMMC_PICKUP_STATE &sPickupState)
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		EXIT
	ENDIF
	
	IF iPickupRespawns[sPickupState.iIndex] <= -1
		EXIT
	ENDIF
	
	IF SHOULD_CLEANUP_PICKUP(sPickupState)
		CLEANUP_AND_DESTROY_PICKUP(sPickupState)
		EXIT
	ENDIF
	
	IF iPickupRespawns[sPickupState.iIndex] <= 0
		EXIT
	ENDIF
	
	IF sPickupState.bPickupExists
		EXIT
	ENDIF
	
	IF NOT SHOULD_PICKUP_RESPAWN(sPickupState.iIndex)
		EXIT
	ENDIF
	
	RESPAWN_MISSION_PICKUP(sPickupState)
	
ENDPROC

// FMMC2020 refactor - rename....

///PURPOSE: This function creates all pickups in a mission and returns true once that's one
///    Attachments are done in PROCESS_PICKUP_BLIPS_AND_ATTACHMENTS
FUNC BOOL MC_CREATE_WEAPONS(INT iTeam = -1) 
	
	WEAPON_TYPE wt	
	
	INT i
	VECTOR vPos

	IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons = 0
		RETURN TRUE
	ENDIF
		
	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
		IF MC_playerBD[iPartToUse].iTeam < FMMC_MAX_TEAMS
			iTeam = MC_playerBD[iPartToUse].iTeam
		ENDIF
	ENDIF
	
	IF iTeam = -1
		iTeam = GET_PRE_ASSIGNMENT_PLAYER_TEAM()
	ENDIF
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i
		IF i < FMMC_MAX_WEAPONS
			
			INT iPlacementFlags = 0
			
			SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_MAP)) 
			
			iPickupRespawns[i] = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iWepRespawnCount
			
			IF iPickupRespawns[i] = -1
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES))
			ENDIF
			
			SET_FM_PICKUP_ORIENTATION_FLAGS(iPlacementFlags)
			
			//Catch to detect blocked pickups in UGC content
			IF g_sMPTunables.bENABLE_CREATOR_BLOCK
				IF NOT IS_CURRENT_MISSION_ROCKSTAR_CREATED()
					wt = GET_WEAPON_TYPE_FROM_PICKUP_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt)
					IF IS_WEAPON_SELECTION_DEV_ONLY(wt)
					AND NOT IS_ROCKSTAR_DEV()
						ASSERTLN("[CREATOR_BLOCK] INVALID WEAPON DETECTED SWITCHING TO PISTOL")
						PRINTLN("[CREATOR_BLOCK] INVALID WEAPON DETECTED SWITCHING TO PISTOL")
						g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt = PICKUP_VEHICLE_WEAPON_PISTOL
					ENDIF
				ENDIF
			ENDIF
			
			PRINTLN("[RCC MISSION] HAS_PLAYER_CREATED_PICKUPS - iWeap: ", i, " iPickupSpawnTotalNumberOfPlayers: ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupSpawnTotalNumberOfPlayers, " GET_NUMBER_OF_PLAYING_PLAYERS: ", GET_NUMBER_OF_PLAYING_PLAYERS())
			
			IF SHOULD_PICKUP_SPAWN_AT_START(i)
				IF NOT DOES_PICKUP_EXIST(pipickup[i])
					IF MC_CREATE_WEAPON(i, iTeam, iPlacementFlags)
						FMMC_SET_LONG_BIT(iSpawnedWeapons, i)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_PICKUPS_CORONA_OPTION) AND g_FMMC_STRUCT.bAllowPickUpsCorona
	OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_PICKUPS_CORONA_OPTION)
		REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i
			IF i < FMMC_MAX_WEAPONS
				IF IS_CONTINUITY_BLOCKING_PICKUP_SPAWN(i)
					RELOOP
				ENDIF
				
				vPos = GET_FMMC_PICKUP_COORDS(i)
				
				IF NOT DOES_PICKUP_EXIST(pipickup[i])
				AND SHOULD_PICKUP_SPAWN_AT_START(i)
					IF NOT IS_VECTOR_ZERO(vPos)
					AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt <> NUM_PICKUPS
					AND MC_IS_THIS_PICKUP_VISIBLE_TO_THIS_TEAM(i, iTeam)
					AND (g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupSpawnTotalNumberOfPlayers >= GET_NUMBER_OF_PLAYING_PLAYERS() OR g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupSpawnTotalNumberOfPlayers = 0 OR IS_FAKE_MULTIPLAYER_MODE_SET())
						PRINTLN("[KH][WEAPON PICKUPS] MC_CREATE_WEAPONS - [", i, "] - WEAPON NOT CREATED!!")
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
		
	RETURN TRUE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: World Props
// ##### Description: World Prop Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC INITIALISE_WORLD_PROP_MODEL_SWAP(INT iWorldProp)

	IF NOT IS_CUSTOM_STRING_LIST_STRING_VALID(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].iModelSwapStringIndex)
		EXIT
	ENDIF
	
	MODEL_NAMES mnWorldPropModelToSwapTo = GET_WORLD_PROP_MODEL_SWAP_MODEL_NAME(iWorldProp)
	
	IF IS_MODEL_VALID(mnWorldPropModelToSwapTo)
		#IF IS_DEBUG_BUILD
		PRINTLN("[WORLDPROPS][WorldPropModelSwap][WorldProp ", iWorldProp, "] INITIALISE_WORLD_PROP_MODEL_SWAP | Creating model swap from ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].mn), " to ", FMMC_GET_MODEL_NAME_FOR_DEBUG(mnWorldPropModelToSwapTo))
		#ENDIF
		
		CREATE_MODEL_SWAP(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].vPos, 2.5, g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].mn, mnWorldPropModelToSwapTo, TRUE)
	ELSE
		#IF IS_DEBUG_BUILD
		PRINTLN("[WORLDPROPS][WorldPropModelSwap][WorldProp ", iWorldProp, "] INITIALISE_WORLD_PROP_MODEL_SWAP | Model was invalid!")
		#ENDIF
	ENDIF
ENDPROC

PROC INITIALISE_WORLD_PROPS()
	
	INT iWorldProp
	PRINTLN("[WorldProps] INITIALISE_WORLD_PROPS | iNumberOfWorldProps: ", g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps)
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps iWorldProp
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].iBitset, ciFMMC_WorldPropHidden)
		
			VECTOR vWorldPropPos = g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].vPos
			FLOAT fWorldPropSearchRadius = GET_WORLD_PROP_SEARCH_RADIUS(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp])
			
			#IF IS_DEBUG_BUILD
			STRING sWorldPropModelName = "(Not loaded yet)"
			IF IS_MODEL_VALID(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].mn)
				sWorldPropModelName = FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].mn)
			ENDIF
			#ENDIF
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_DontHideScriptObjectsInModelHide)
				#IF IS_DEBUG_BUILD
				PRINTLN("[WorldProps][WorldProp ", iWorldProp, "] INITIALISE_WORLD_PROPS | Creating model hide at ", vWorldPropPos, " for world prop ", iWorldProp, ": ", sWorldPropModelName, " with a search radius of ", fWorldPropSearchRadius)
				#ENDIF
				
				CREATE_MODEL_HIDE(vWorldPropPos, fWorldPropSearchRadius, g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].mn, TRUE)
			ELSE
				#IF IS_DEBUG_BUILD
				PRINTLN("[WorldProps][WorldProp ", iWorldProp, "] INITIALISE_WORLD_PROPS | Creating model hide (excluding script objects) at ", vWorldPropPos, " for world prop ", iWorldProp, ": ", sWorldPropModelName, " with a search radius of ", fWorldPropSearchRadius)
				#ENDIF
				
				CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(vWorldPropPos, fWorldPropSearchRadius, g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].mn, TRUE)
			ENDIF
		ENDIF
		
		INITIALISE_WORLD_PROP_MODEL_SWAP(iWorldProp)
		
	ENDREPEAT
ENDPROC

FUNC BOOL SHOULD_WORLD_PROP_OBJECT_REFERENCE_BE_USED(INT iWorldProp)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].iBitset, ciFMMC_WorldPropFrozen)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].iBitset, ciFMMC_WorldPropInvincible)
		// We should only grab an object reference if we're using an option that requires one
		RETURN FALSE
	ENDIF
	
	VECTOR vPlayerCoords = GET_ENTITY_COORDS(LocalPlayerPed, FALSE)
	RETURN VDIST2(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].vPos, vPlayerCoords) < POW(100.0, 2.0)
ENDFUNC

PROC CREATE_WORLD_PROP_HIDDEN_COVER_BLOCKING_ZONE(INT iWorldProp)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].iBitset, ciFMMC_WorldPropHidden)
		EXIT
	ENDIF
	
	IF IS_LONG_BIT_SET(sRunTimeWorldPropData.iWorldPropCoverBlockBS, iWorldProp)
		EXIT
	ENDIF
	
	MODEL_NAMES mnProp = g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].mn
	VECTOR vPos = g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].vPos
	VECTOR vPos1, vPos2
	VECTOR vMax, vMin	
	
	IF IS_VECTOR_ZERO(vPos)
		PRINTLN("[LM][CoverBlockWorldProp][WorldProps][WorldProp ", iWorldProp, "] CREATE_WORLD_PROP_HIDDEN_COVER_BLOCKING_ZONE - World iWorldProp: ", iWorldProp, " Cannot Create - Vector is Zero...")
		EXIT
	ENDIF
	
	IF NOT IS_MODEL_VALID(mnProp)
		// Some models (especially models for world props) aren't valid until the player is in the matching part of the map, so this check will make sure we don't try to create cover for models that haven't become valid yet
		PRINTLN("[LM][CoverBlockWorldProp][WorldProps_SPAM][WorldProp_SPAM ", iWorldProp, "] CREATE_WORLD_PROP_HIDDEN_COVER_BLOCKING_ZONE - World iWorldProp: ", iWorldProp, " Cannot Create Yet - Invalid Model Name...")
		EXIT
	ENDIF
	
	GET_MODEL_DIMENSIONS(mnProp, vMin, vMax)	
	
	vMin *= 2.0
	vMax *= 2.0
	
	IF vMax.x > vMax.y
		vMax.y = vMax.x
	ELSE
		vMax.x = vMax.y
	ENDIF
	
	IF vMin.x > vMin.y
		vMin.x = vMin.y
	ELSE
		vMin.y = vMin.x
	ENDIF

	vPos1 = vPos+vMin
	vPos2 = vPos+vMax
	
	vPos1.z -= 0.5
	vPos2.z += 0.5
	
	ADD_COVER_BLOCKING_AREA(vPos1, vPos2, FALSE, FALSE, TRUE, FALSE)
	SET_LONG_BIT(sRunTimeWorldPropData.iWorldPropCoverBlockBS, iWorldProp)
	
	PRINTLN("[LM][CoverBlockWorldProp][WorldProps][WorldProp ", iWorldProp, "] CREATE_WORLD_PROP_HIDDEN_COVER_BLOCKING_ZONE - World iWorldProp: ", iWorldProp, " Added Cover Blocking Area Between Coords - vPos1: ", vPos1, " vPos2: ", vPos2)
	
ENDPROC

PROC PROCESS_WORLD_PROPS_CUTSCENE(INT iWorldProp, BOOL bPrintDebug = TRUE)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].iBitset, ciFMMC_WorldHiddenInCutscenes)
	AND IS_CUTSCENE_PLAYING()
		IF NOT IS_LONG_BIT_SET(sRunTimeWorldPropData.iWorldPropHiddenForCutsceneBitSet, iWorldProp)
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].vPos, GET_WORLD_PROP_SEARCH_RADIUS(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp], TRUE), g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].mn)
				IF NOT DOES_ENTITY_EXIST(sRunTimeWorldPropData.oiWorldProps[iWorldProp])
			
					IF NETWORK_IS_ACTIVITY_SESSION()
						RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS() + 1)
					ELSE
						RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS() + 1)
					ENDIF
					
					sRunTimeWorldPropData.oiWorldProps[iWorldProp] = GET_CLOSEST_OBJECT_OF_TYPE(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].vPos, GET_WORLD_PROP_SEARCH_RADIUS(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp], TRUE), g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].mn, FALSE)
					
					IF DOES_ENTITY_EXIST(sRunTimeWorldPropData.oiWorldProps[iWorldProp])
						
						IF NOT IS_ENTITY_A_MISSION_ENTITY(sRunTimeWorldPropData.oiWorldProps[iWorldProp])
							SET_ENTITY_AS_MISSION_ENTITY(sRunTimeWorldPropData.oiWorldProps[iWorldProp], FALSE)
						ENDIF
							
						SET_ENTITY_VISIBLE(sRunTimeWorldPropData.oiWorldProps[iWorldProp], FALSE)
						
						SET_LONG_BIT(sRunTimeWorldPropData.iWorldPropHiddenForCutsceneBitSet, iWorldProp)
						IF bPrintDebug PRINTLN("[WorldProps][WorldProp ", iWorldProp, "] PROCESS_WORLD_PROPS - Calling SET_ENTITY_VISIBLE FALSE for ", iWorldProp, " as we are currently in a cutscene.") ENDIF
					ELSE
						IF bPrintDebug PRINTLN("[WorldProps][WorldProp ", iWorldProp, "] PROCESS_WORLD_PROPS - Could not Create Model Hide for ", iWorldProp, " GET_CLOSEST_OBJECT_OF_TYPE detecting no objects..") ENDIF
					ENDIF				
				ELSE
					IF bPrintDebug PRINTLN("[WorldProps][WorldProp ", iWorldProp, "] PROCESS_WORLD_PROPS - Could not Create Model Hide for ", iWorldProp, " DOES_ENTITY_EXIST Object does not exist.") ENDIF
				ENDIF
			ELSE
				IF bPrintDebug PRINTLN("[WorldProps][WorldProp ", iWorldProp, "] PROCESS_WORLD_PROPS - Could not Create Model Hide for ", iWorldProp, " DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS detecting no objects..") ENDIF
			ENDIF
		ENDIF
		
	ELIF IS_LONG_BIT_SET(sRunTimeWorldPropData.iWorldPropHiddenForCutsceneBitSet, iWorldProp)		
		SET_OBJECT_AS_NO_LONGER_NEEDED(sRunTimeWorldPropData.oiWorldProps[iWorldProp])		
		CLEAR_LONG_BIT(sRunTimeWorldPropData.iWorldPropHiddenForCutsceneBitSet, iWorldProp)		
		IF bPrintDebug PRINTLN("[WorldProps][WorldProp ", iWorldProp, "] PROCESS_WORLD_PROPS - Clearing SET_OBJECT_AS_NO_LONGER_NEEDED for ", iWorldProp, " as we are no longer in a cutscene.") ENDIF
	ENDIF
ENDPROC

FUNC BOOL GRAB_WORLD_PROP_OBJECT_REFERENCE(INT iWorldProp)
	
	IF DOES_ENTITY_EXIST(sRunTimeWorldPropData.oiWorldProps[iWorldProp])
		PRINTLN("[WorldProps][WorldProp ", iWorldProp, "] PROCESS_WORLD_PROP_OBJECT - We've already got an object reference for World Prop ", iWorldProp)
		RETURN TRUE
	ENDIF
	
	IF NOT DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].vPos, GET_WORLD_PROP_SEARCH_RADIUS(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp]), g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].mn)
		PRINTLN("[WorldProps][WorldProp ", iWorldProp, "] PROCESS_WORLD_PROP_OBJECT - No object seems to exist for World Prop ", iWorldProp)
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS() + 1)
	ELSE
		RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS() + 1)
	ENDIF
	
	sRunTimeWorldPropData.oiWorldProps[iWorldProp] = GET_CLOSEST_OBJECT_OF_TYPE(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].vPos, GET_WORLD_PROP_SEARCH_RADIUS(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp]), g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].mn, FALSE)
	
	IF DOES_ENTITY_EXIST(sRunTimeWorldPropData.oiWorldProps[iWorldProp])
		PRINTLN("[WorldProps][WorldProp ", iWorldProp, "] PROCESS_WORLD_PROP_OBJECT - Successfully grabbed object for World Prop ", iWorldProp)
		
		IF NOT IS_ENTITY_A_MISSION_ENTITY(sRunTimeWorldPropData.oiWorldProps[iWorldProp])
			SET_ENTITY_AS_MISSION_ENTITY(sRunTimeWorldPropData.oiWorldProps[iWorldProp],FALSE)
		ENDIF
		
		IF GET_INTERIOR_FROM_ENTITY(sRunTimeWorldPropData.oiWorldProps[iWorldProp]) != NULL
			RETAIN_ENTITY_IN_INTERIOR(sRunTimeWorldPropData.oiWorldProps[iWorldProp], GET_INTERIOR_FROM_ENTITY(sRunTimeWorldPropData.oiWorldProps[iWorldProp]))
			PRINTLN("[WorldProps][WorldProp ", iWorldProp, "] PROCESS_WORLD_PROP_OBJECT - Prop ", iWorldProp, " set to retain in its interior")
		ENDIF
		
		RETURN TRUE
	ELSE
		PRINTLN("[WorldProps][WorldProp ", iWorldProp, "] PROCESS_WORLD_PROP_OBJECT - Failed to grab object for World Prop ", iWorldProp)
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_WORLD_PROP_OBJECT(INT iWorldProp, BOOL bPrintDebug = TRUE)
	IF NOT DOES_ENTITY_EXIST(sRunTimeWorldPropData.oiWorldProps[iWorldProp])
		IF NOT GRAB_WORLD_PROP_OBJECT_REFERENCE(iWorldProp)
			// Can't find an object for this one
			EXIT
		ELSE
			// We found an object reference for this world prop - continue now
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].iBitset, ciFMMC_WorldPropFrozen)
		IF bPrintDebug PRINTLN("[WorldProps][WorldProp ", iWorldProp, "] PROCESS_WORLD_PROP_OBJECT - Prop ", iWorldProp, " has been tagged to be frozen") ENDIF
		
		IF NOT IS_LONG_BIT_SET(sRunTimeWorldPropData.iWorldPropFrozenBitSet, iWorldProp)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(sRunTimeWorldPropData.oiWorldProps[iWorldProp])
			PRINTLN("[RCC MISSION][WorldProps][WorldProp ", iWorldProp, "] Freezing PROP ", iWorldProp, " at position: ",g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].vPos)
			FREEZE_ENTITY_POSITION(sRunTimeWorldPropData.oiWorldProps[iWorldProp], TRUE)
			SET_LONG_BIT(sRunTimeWorldPropData.iWorldPropFrozenBitSet, iWorldProp)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].iBitset, ciFMMC_WorldPropInvincible)
		IF NOT IS_LONG_BIT_SET(sRunTimeWorldPropData.iWorldPropInvincibleBitSet, iWorldProp)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(sRunTimeWorldPropData.oiWorldProps[iWorldProp])
			PRINTLN("[RCC MISSION][WorldProps][WorldProp ", iWorldProp, "] PROCESS_WORLD_PROP_OBJECT - Setting world prop ", iWorldProp, " invincible at position: ", g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].vPos)
			SET_ENTITY_INVINCIBLE(sRunTimeWorldPropData.oiWorldProps[iWorldProp], TRUE)
			
			IF GET_IS_ENTITY_A_FRAG(sRunTimeWorldPropData.oiWorldProps[iWorldProp])
				PRINTLN("[RCC MISSION][WorldProps][WorldProp ", iWorldProp, "] PROCESS_WORLD_PROP_OBJECT - Disabling frag damage on world prop ", iWorldProp, " at position: ", g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].vPos)
				SET_DISABLE_FRAG_DAMAGE(sRunTimeWorldPropData.oiWorldProps[iWorldProp], TRUE)
			ENDIF

			SET_LONG_BIT(sRunTimeWorldPropData.iWorldPropInvincibleBitSet, iWorldProp)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_WORLD_PROP(INT iWorldProp, BOOL bPrintDebug = TRUE)
	
	IF g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].mn = DUMMY_MODEL_FOR_SCRIPT
		EXIT
	ENDIF
	
	IF bPrintDebug PRINTLN("[WorldProps_SPAM][WorldProp_SPAM ", iWorldProp, "] PROCESS_WORLD_PROP - Processing world prop ", iWorldProp) ENDIF
	
	IF SHOULD_WORLD_PROP_OBJECT_REFERENCE_BE_USED(iWorldProp)
		PROCESS_WORLD_PROP_OBJECT(iWorldProp, bPrintDebug)
	ELSE
		CLEAN_UP_WORLD_PROP_OBJECT_REFERENCE(iWorldProp, bPrintDebug)
	ENDIF
	
	CREATE_WORLD_PROP_HIDDEN_COVER_BLOCKING_ZONE(iWorldProp)
	
	PROCESS_WORLD_PROPS_CUTSCENE(iWorldProp, bPrintDebug)
	
	#IF IS_DEBUG_BUILD
	IF bWorldPropDebug
		VECTOR vWorldPropDebugPos = g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].vPos
		
		IF DOES_ENTITY_EXIST(sRunTimeWorldPropData.oiWorldProps[iWorldProp])
			vWorldPropDebugPos = GET_ENTITY_COORDS(sRunTimeWorldPropData.oiWorldProps[iWorldProp], FALSE)
			DRAW_DEBUG_TEXT("Grabbed Obj", vWorldPropDebugPos + <<0,0,-0.85>>, 255, 255, 255, 255)
		ENDIF
		
		TEXT_LABEL_63 tlVisualDebug = "World Prop "
		tlVisualDebug += iWorldProp
		DRAW_DEBUG_TEXT(tlVisualDebug, vWorldPropDebugPos, 255, 255, 255, 255)
	ENDIF
	#ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Main Processing Function
// ##### Description: The main processing function.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC MC_SET_ALL_MISSION_ENTITIES_VISIBLITY(BOOL bVisible)

	INT i
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles i
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])	
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
				SET_ENTITY_VISIBLE(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[i]), bVisible)
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfObjects i
		IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(i))
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GET_OBJECT_NET_ID(i))
				SET_ENTITY_VISIBLE(NET_TO_ENT(GET_OBJECT_NET_ID(i)), bVisible AND NOT SHOULD_OBJECT_BE_INVISIBLE(i))
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps i
		IF NETWORK_DOES_NETWORK_ID_EXIST(GET_DYNOPROP_NET_ID(i))	
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GET_DYNOPROP_NET_ID(i))
				SET_ENTITY_VISIBLE(NET_TO_ENT(GET_DYNOPROP_NET_ID(i)), bVisible)
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicleCrates i
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[i])	
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[i])
				SET_ENTITY_VISIBLE(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[i]), bVisible)
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds i
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[i])	
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[i])
				SET_ENTITY_VISIBLE(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niPed[i]), bVisible)
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfProps 	 i
		IF DOES_ENTITY_EXIST(oiProps[i])
			IF GET_ENTITY_MODEL(oiProps[i]) != PROP_LD_ALARM_01
			AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iPropBitSet, ciFMMC_PROP_SetInvisible)
				SET_ENTITY_VISIBLE(oiProps[i], bVisible)
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("**************ALL MISSION ENTITIES SET VISIBLITY*************")
ENDPROC

PROC MC_HIDE_ALL_MISSION_ENTITIES_LOCALLY_FOR_STRAND_MISSION()
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
	AND g_sTransitionSessionData.sStrandMissionData.iSwitchState <= ciMAINTAIN_STRAND_MISSION_CUT_WAIT
		INT i
		REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles i
			IF IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
				SET_ENTITY_LOCALLY_INVISIBLE(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
			ENDIF
		ENDREPEAT
		REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfObjects i
			IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(i))	
				SET_ENTITY_LOCALLY_INVISIBLE(NET_TO_ENT(GET_OBJECT_NET_ID(i)))
			ENDIF
		ENDREPEAT
		REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps i
			IF NETWORK_DOES_NETWORK_ID_EXIST(GET_DYNOPROP_NET_ID(i))	
				SET_ENTITY_LOCALLY_INVISIBLE(NET_TO_ENT(GET_DYNOPROP_NET_ID(i)))
			ENDIF
		ENDREPEAT
		REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicleCrates i
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[i])	
				SET_ENTITY_LOCALLY_INVISIBLE(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[i]))
			ENDIF
		ENDREPEAT
		REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds i
			IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
				SET_ENTITY_LOCALLY_INVISIBLE(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niPed[i]))
			ENDIF
		ENDREPEAT
		REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfProps i
			IF DOES_ENTITY_EXIST(oiProps[i])
			AND NETWORK_GET_ENTITY_IS_NETWORKED(oiProps[i])
				SET_ENTITY_LOCALLY_INVISIBLE(oiProps[i])
			ENDIF
		ENDREPEAT
	ENDIF
	
ENDPROC

FUNC BOOL MC_LOAD_AND_CREATE_ALL_LOCAL_ENTITIES()
	
	//make the props
	IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_INITIAL_IPL_SET_UP_COMPLETE)
		
		// We want a new 2020 version of MC_CREATE_PROPS that does not use the MonkeyPeds or these weird passed in vars at all.
		IF NOT MC_CREATE_PROPS(oiProps, isoundid, iCrashSoundProp, MC_serverBD.iNumberOfTeams)
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] MISSION - TRANSITION TIME - MC_CREATE_PROPS  - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] not created props !!!!!!")
			RETURN FALSE
		ELSE
			SET_AIR_CHECKPOINT_COLLISION(oiProps, FALSE)
		ENDIF
		
	ELSE
		PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] LBOOL8_INITIAL_IPL_SET_UP_COMPLETE not set")
		RETURN FALSE		
	ENDIF
	
	IF MISSION_CONTAINS_A_PLACED_YACHT()
		IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_FMMC_YACHTS_READY)
			PRINTLN("[RCC MISSION][MYACHT][HostOwnedYacht][MC_LOAD_AND_CREATE_ALL_LOCAL_ENTITIES] - Waiting for PBBOOL4_FMMC_YACHTS_READY")
			RETURN FALSE
		ENDIF
	ENDIF
		
	RETURN TRUE
ENDFUNC

FUNC BOOL MC_LOAD_AND_CREATE_ALL_ENTITIES()
	
	IF IS_SKYSWOOP_IN_SKY()
		IF NOT BUSYSPINNER_IS_ON()
			BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("MP_SPINLOADING")
			END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
		ENDIF
	ENDIF
			
	IF MC_REQUEST_LOAD_MODELS()
		//Set the number of entities to load
		
		INT iNumberOfPeds = CLAMP_INT(g_FMMC_STRUCT_ENTITIES.iNumberOfPeds, 0, g_FMMC_STRUCT_ENTITIES.iPedSpawnCap)
		INT iNumberOfVehicles = g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles + (g_FMMC_STRUCT_ENTITIES.iNumberOfTrains * (FMMC_MAX_NUM_OF_TRAIN_CARRIAGES + 1))
		INT iNumberOfObjects = GET_NETWORKED_OBJECT_COUNT() + g_FMMC_STRUCT_ENTITIES.iNumberOfVehicleCrates
		NET_PRINT("MC_LOAD_AND_CREATE_ALL_ENTITIES - g_FMMC_STRUCT_ENTITIES.iNumberOfObjects: ") NET_PRINT_INT(g_FMMC_STRUCT_ENTITIES.iNumberOfObjects) NET_NL()
		NET_PRINT("MC_LOAD_AND_CREATE_ALL_ENTITIES - MC_serverBD_1.sCreatedCount.iNumObjCreated[0]: ") NET_PRINT_INT(MC_serverBD_1.sCreatedCount.iNumObjCreated[0]) NET_NL()		
		NET_PRINT("MC_LOAD_AND_CREATE_ALL_ENTITIES - g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps: ") NET_PRINT_INT(g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps) NET_NL()
		NET_PRINT("MC_LOAD_AND_CREATE_ALL_ENTITIES - g_FMMC_STRUCT_ENTITIES.iNumberOfVehicleCrates: ") NET_PRINT_INT(g_FMMC_STRUCT_ENTITIES.iNumberOfVehicleCrates) NET_NL()
		NET_PRINT("MC_LOAD_AND_CREATE_ALL_ENTITIES - g_FMMC_STRUCT_ENTITIES.iNumberOfPeds: ") NET_PRINT_INT(g_FMMC_STRUCT_ENTITIES.iNumberOfPeds) NET_NL()
		NET_PRINT("MC_LOAD_AND_CREATE_ALL_ENTITIES - MC_serverBD_1.sCreatedCount.iNumPedCreated[0] ") NET_PRINT_INT(MC_serverBD_1.sCreatedCount.iNumPedCreated[0]) NET_NL()
		NET_PRINT("MC_LOAD_AND_CREATE_ALL_ENTITIES - g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles: ") NET_PRINT_INT(g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles) NET_NL()
		NET_PRINT("MC_LOAD_AND_CREATE_ALL_ENTITIES - MC_serverBD_1.sCreatedCount.iNumVehCreated[0]: ") NET_PRINT_INT(MC_serverBD_1.sCreatedCount.iNumVehCreated[0]) NET_NL()
		NET_PRINT("MC_LOAD_AND_CREATE_ALL_ENTITIES - g_FMMC_STRUCT_ENTITIES.iNumberOfTrains: ") NET_PRINT_INT(g_FMMC_STRUCT_ENTITIES.iNumberOfTrains) NET_NL()
		NET_PRINT("MC_LOAD_AND_CREATE_ALL_ENTITIES - MC_serverBD_1.sCreatedCount.iNumTrainsCreated: ") NET_PRINT_INT(MC_serverBD_1.sCreatedCount.iNumTrainsCreated * (FMMC_MAX_NUM_OF_TRAIN_CARRIAGES + 1)) NET_NL()
		NET_PRINT("MC_LOAD_AND_CREATE_ALL_ENTITIES - g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons: ") NET_PRINT_INT(g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons) NET_NL()
				
		iNumberOfPeds -= MC_serverBD_1.sCreatedCount.iNumPedCreated[0]
		iNumberOfVehicles -= MC_serverBD_1.sCreatedCount.iNumVehCreated[0]
		iNumberOfVehicles -= MC_serverBD_1.sCreatedCount.iNumTrainsCreated * (FMMC_MAX_NUM_OF_TRAIN_CARRIAGES + 1)
		iNumberOfObjects -= MC_serverBD_1.sCreatedCount.iNumObjCreated[0]
	
		//If we need to make container doors:
		iNumberOfObjects += (COUNT_SET_BITS(MC_serverBD.iIsHackContainer) * 3) 
		
		NET_PRINT("MC_LOAD_AND_CREATE_ALL_ENTITIES - iNumberOfPeds to register: ") NET_PRINT_INT(iNumberOfPeds) NET_NL()
		NET_PRINT("MC_LOAD_AND_CREATE_ALL_ENTITIES - iNumberOfVehicles to register: ") NET_PRINT_INT(iNumberOfVehicles) NET_NL()
		NET_PRINT("MC_LOAD_AND_CREATE_ALL_ENTITIES - iNumberOfObjects to register: ") NET_PRINT_INT(iNumberOfObjects) NET_NL()
		
		IF CAN_REGISTER_MISSION_ENTITIES(iNumberOfPeds, iNumberOfVehicles, iNumberOfObjects,  0)
			IF MC_CREATE_TRAINS(MC_serverBD_1.sFMMC_SBD.niTrain)
				IF MC_CREATE_VEHICLES(MC_serverBD_1.sFMMC_SBD.niVehicle, MC_serverBD_1.sFMMC_SBD.niVehicleCrate)
					IF MC_CREATE_PEDS(MC_serverBD_1.sFMMC_SBD.niPed, MC_serverBD_1.sFMMC_SBD.niVehicle)
						IF MC_CREATE_OBJECTS(MC_serverBD_1.sFMMC_SBD.niPed, MC_serverBD_1.sFMMC_SBD.niVehicle, MC_serverBD_1.niMinigameCrateDoors)
							IF MC_CREATE_DYNOPROPS()
								IF MC_CREATE_INTERACTABLES()
									NET_PRINT_TIME() NET_PRINT_STRINGS("     ---------->     MC_LOAD_AND_CREATE_ALL_ENTITIES RETURNING TRUE    <----------     ", "LOAD_AND_CREATE_ALL_FMMC_ENTITIES") NET_NL()
									IF BUSYSPINNER_IS_ON()
										BUSYSPINNER_OFF()
									ENDIF
									
									RETURN TRUE
								ELSE
									NET_PRINT_TIME() NET_PRINT_STRINGS("-----> Waiting to create Interactables <-----", "MC_LOAD_AND_CREATE_ALL_ENTITIES") NET_NL()
									RETURN FALSE
								ENDIF
							ELSE
								NET_PRINT_TIME() NET_PRINT_STRINGS("-----> Waiting to create Dyno props <-----", "MC_LOAD_AND_CREATE_ALL_ENTITIES") NET_NL()
								RETURN FALSE
							ENDIF
						ELSE
							NET_PRINT_TIME() NET_PRINT_STRINGS("-----> Waiting to create OBJECTS <-----", "MC_LOAD_AND_CREATE_ALL_ENTITIES") NET_NL()
							RETURN FALSE
						ENDIF
					ELSE
						NET_PRINT_TIME() NET_PRINT_STRINGS("-----> Waiting to create PEDS <-----", "MC_LOAD_AND_CREATE_ALL_ENTITIES") NET_NL()
						RETURN FALSE
					ENDIF
				ELSE
					NET_PRINT_TIME() NET_PRINT_STRINGS("-----> Waiting to create VEHICLES <-----", "MC_LOAD_AND_CREATE_ALL_ENTITIES") NET_NL()
					RETURN FALSE
				ENDIF
			ELSE
				NET_PRINT_TIME() NET_PRINT_STRINGS("-----> Waiting to create TRAINS <-----", "MC_LOAD_AND_CREATE_ALL_ENTITIES") NET_NL()
				RETURN FALSE
			ENDIF
		ELSE
			NET_PRINT_TIME() NET_PRINT_STRINGS("-----> Waiting for CAN_REGISTER_MISSION_ENTITIES <-----", "MC_LOAD_AND_CREATE_ALL_ENTITIES") NET_NL()
			RETURN FALSE
		ENDIF
	ELSE
		NET_PRINT_TIME() NET_PRINT_STRINGS("-----> Waiting for Model Requests <-----", "MC_LOAD_AND_CREATE_ALL_ENTITIES") NET_NL()
		RETURN FALSE
	ENDIF	
	
	NET_PRINT_TIME() NET_PRINT_STRINGS("----->EVERYTHING LOADED! <-----", "MC_LOAD_AND_CREATE_ALL_ENTITIES") NET_NL()
	RETURN TRUE
	
ENDFUNC
