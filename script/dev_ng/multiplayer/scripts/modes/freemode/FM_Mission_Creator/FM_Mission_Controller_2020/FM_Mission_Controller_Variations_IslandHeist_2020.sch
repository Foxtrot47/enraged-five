// ##### ------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Variations Utility -----------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc ----------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Shared Creator / Controller logic relating to the Alternate Variables System          
// ##### ------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020                                                                                     
// ##### ------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "fm_mission_controller_include_2020.sch"
	
// ##### Island Heist Server Bitsets - These decide what index is selected for Alternate Variable globals. They should reflect Lobby Decisions, Freemode Event Outcomes, Character Stats, etc. 
// Server Bitset: iAltVarsBS

CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_SyncedData									0
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_MainGate						1
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_SideGateNorth					2
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_SideGateSouth					3
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_RappelNorth					4
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_RappelSouth					5
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_Tunnel						6
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SubmarineEastDocks			7
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SubmarineWestDocks			8
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SubmarineWestBeach			9
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_StealthPlaneHALOJump		10
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_StealthChopperNorthDropZone	11
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_StealthChopperSouthDropZone	12
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SmugglerPlaneAirstrip		13
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SmugglerBoatMainDocks		14
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SmugglerBoaSmallDocks		15
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_GunBoatMainDocks			16
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_GunBoatSmallDocks			17
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_DrainageTunnel				18 
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_GrappelEquipmentScoped_1						19 
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_GrappelEquipmentScoped_2						20
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_GrappelEquipmentScoped_3						21 
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_GrappelEquipmentScoped_4						22 
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_UniformScoped_1								23 
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_UniformScoped_2								24 
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_UniformScoped_3								25 
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_UniformScoped_4								26
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_BoltCutterScoped_1							27
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_BoltCutterScoped_2							28
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_BoltCutterScoped_3							29
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_BoltCutterScoped_4							30
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponLoadout_1								31
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponLoadout_2								32
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponLoadout_3								33
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponLoadout_4								34
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponLoadout_5								35
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponSuppressors							36	
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_TimeOfDayOverrideDay							37
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_TimeOfDayOverrideNight						38
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_SecondaryLootLocations						39
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemyWeaponDisruption_None					40
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemyWeaponDisruption_Low					41
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemyWeaponDisruption_Medium					42
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemyWeaponDisruption_High					43
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_PoisonedWaterSupply							44
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemiesArmourDisruption						45
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemiesHelicoptorDisruption					46
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_PowerStationScoped							47
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_TrojanVehicleScoped							48
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_ControlTowerScoped							49
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_CarpetBomb									50
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_AbilitiesSniper								51
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_AbilitiesHeliBackup							52
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_AbilitiesReconnaissanceDrone					53
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_AbilitiesHeavyLoadout						54
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponStash									55
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_FirstPlaythroughPickTargetInSafe				56
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_MadrazoFiles							57
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_Bonds									58
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_Tequila								59
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_PearlNecklace							60
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_SapphirePantherStatue					61
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_PinkDiamond							62
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_GoldenGunDrawKey								63
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteAirstrip							64
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteMainDocks							65
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteSmallDocks						66
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteSubmarine							67 
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceExplosivesFrontGate			68
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceKeycardSouth					69
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceKeycardNorth					70
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceRappelSouth					71
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceRappelNorth					72
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceVehicleFrontGate				73
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceSewerGrate					74
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_TrojanVehicleLocation_1						75
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_TrojanVehicleLocation_2						76
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_TrojanVehicleLocation_3						77
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_TrojanVehicleLocation_4						78
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_TrojanVehicleLocation_5						79
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_HasAcetyleneTorch							80
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_DemolitionCharges							81
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitFrontGate						82
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitKeycardSouth						83
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitKeycardNorth						84
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitRappelSouth						85
CONST_INT ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitRappelNorth						86

#IF FEATURE_HEIST_ISLAND 	

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Enemy Health	Poisoned																															 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_HEALTH_POISONED_WATER(INT iPed)	
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAltVarsBitsetPed, ciAltVarsBS_IslandHeist_Peds_Flag_HealthPoisoned)
		EXIT
	ENDIF
		
	INT iIndex = -1
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_PoisonedWaterSupply)
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Ped_Health_Poisoned_0
	ENDIF
		
	IF iIndex = -1
		EXIT
	ENDIF
	
	INT iMod = g_FMMC_STRUCT.sPoolVars.iInt[iIndex]
	
	IF iMod = 0
		PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_HEALTH_POISONED_WATER_ISLAND_HEIST - Modifier not set")
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_HEALTH_POISONED_WATER_ISLAND_HEIST - iIndex: ", iIndex, " iMod: ", iMod)
	
	g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fHealthVarMultiplier = TO_FLOAT(iMod)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Enemy Health	Armour Disrupted																															 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_HEALTH_ARMOUR_DISRUPTED(INT iPed)			
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAltVarsBitsetPed, ciAltVarsBS_IslandHeist_Peds_Flag_HealthArmourDisruption)		
		EXIT
	ENDIF
		
	INT iIndex = -1
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemiesArmourDisruption)
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Ped_Health_Armour_0
	ENDIF
	
	IF iIndex = -1
		EXIT
	ENDIF
	
	INT iMod = g_FMMC_STRUCT.sPoolVars.iInt[iIndex]
	
	IF iMod = 0
		PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_HEALTH_ARMOUR_DISRUPTED_ISLAND_HEIST - Modifier not set")
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_HEALTH_ARMOUR_DISRUPTED_ISLAND_HEIST - iIndex: ", iIndex, " iMod: ", iMod)
	
	g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fHealthVarMultiplier2 = TO_FLOAT(iMod)
	SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEleven, ciPed_BSEleven_UseAlternateComponentsForPedOutfit)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Enemy Accuracy	Poisoned																															 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_ACCURACY_POISONED_WATER(INT iPed)	
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAltVarsBitsetPed, ciAltVarsBS_IslandHeist_Peds_Flag_AccuracyPoisoned)
		EXIT
	ENDIF
		
	INT iIndex = -1
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_PoisonedWaterSupply)
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Ped_Accuracy_Poisoned_0
	ENDIF
		
	IF iIndex = -1
		EXIT
	ENDIF
	
	INT iMod = g_FMMC_STRUCT.sPoolVars.iInt[iIndex]
	
	IF iMod = -1
		PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_ACCURACY_POISONED_WATER - Modifier not set")
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_ACCURACY_POISONED_WATER - iIndex: ", iIndex, " iMod: ", iMod)
	
	g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAccuracy = iMod
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Enemy Sight TOD/Weather 																													-----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_SIGHT_RANGE_TOD_AND_WEATHER(INT iPed)	
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAltVarsBitsetPed, ciAltVarsBS_IslandHeist_Peds_Flag_SightAffectedByWeather)
		EXIT
	ENDIF
	
	INT iIndexTOD = -1
	INT iIndexWeather = -1

	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_TimeOfDayOverrideDay)
		iIndexTOD = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Ped_Sight_NightTimePercentage
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_TimeOfDayOverrideNight)
		iIndexWeather = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Ped_Sight_StormPercentage
	ENDIF
	
	FLOAT fNewStealthDetectionRange1, fNewStealthDetectionRange2, fNewInstaStealthDetectionRange1, fNewInstaStealthDetectionRange2
	
	INT iModTOD = -1 
	IF iIndexTOD != -1
		iModTOD = g_FMMC_STRUCT.sPoolVars.iInt[iIndexTOD]
		IF iModTOD != 0
			fNewStealthDetectionRange1 = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sStealthAndAggroSystem.fDetectionRange * (TO_FLOAT(iModTOD)*0.01)
			fNewInstaStealthDetectionRange1 = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sStealthAndAggroSystem.fInstantDetectionRange * (TO_FLOAT(iModTOD)*0.01)
			iPedSightModifierTOD = iModTOD // Array if we have to apply different values
			SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFifteen, ciPED_BSFifteen_TOD_AffectsSight)			
			PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_SIGHT_RANGE_TOD_AND_WEATHER - iPedSightModifierTOD: ", iPedSightModifierTOD)	
		ENDIF
	ELSE
		PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_SIGHT_RANGE_TOD_AND_WEATHER - No Modifications from TOD.")	
	ENDIF
	
	INT iModWeather = -1 
	IF iIndexWeather != -1
		iModWeather = g_FMMC_STRUCT.sPoolVars.iInt[iIndexWeather]
		IF iModWeather != 0
			fNewStealthDetectionRange2 = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sStealthAndAggroSystem.fDetectionRange * (TO_FLOAT(iModWeather)*0.01)
			fNewInstaStealthDetectionRange2	= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sStealthAndAggroSystem.fInstantDetectionRange * (TO_FLOAT(iModWeather)*0.01)
			iPedSightModifierWeather = iModWeather // Array if we have to apply different values
			SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFifteen, ciPED_BSFifteen_Weather_AffectsSight)
			PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_SIGHT_RANGE_TOD_AND_WEATHER - iPedSightModifierWeather: ", iPedSightModifierWeather)
		ENDIF
	ELSE
		PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_SIGHT_RANGE_TOD_AND_WEATHER - No Modifications from Weather.")	
	ENDIF
	
	IF iModTOD != 0
		g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sStealthAndAggroSystem.fDetectionRange += fNewStealthDetectionRange1
		g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sStealthAndAggroSystem.fInstantDetectionRange += fNewInstaStealthDetectionRange1
	ENDIF
	IF iModWeather != 0		
		g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sStealthAndAggroSystem.fDetectionRange += fNewStealthDetectionRange2
		g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sStealthAndAggroSystem.fInstantDetectionRange += fNewInstaStealthDetectionRange2
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_SIGHT_RANGE_TOD_AND_WEATHER - iIndexTOD: ", iIndexTOD, " iModTOD: ", iModTOD, " iIndexWeather: ", iIndexWeather, " iModWeather: ", iModWeather)	
	PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_SIGHT_RANGE_TOD_AND_WEATHER - fNewStealthDetectionRange1: ", fNewStealthDetectionRange1, " fNewStealthDetectionRange2: ", fNewStealthDetectionRange2)
	PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_SIGHT_RANGE_TOD_AND_WEATHER - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sStealthAndAggroSystem.fDetectionRange: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sStealthAndAggroSystem.fDetectionRange)
	PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_SIGHT_RANGE_TOD_AND_WEATHER - GET_FMMC_PED_PERCEPTION_RANGE: ", GET_FMMC_PED_PERCEPTION_RANGE(iPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedPerceptionSettings, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fPedPerceptionCentreRange, iPedSightModifierTOD, iPedSightModifierWeather))
	
ENDPROC
	
	
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Enemy Accuracy	Disrupted 																												-----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_WEAPONS_DISRUPTED(INT iPed)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAltVarsBitsetPed, ciAltVarsBS_IslandHeist_Peds_Flag_UseDisruptedWeaponInventory)
		EXIT
	ENDIF
	
	INT iIndex = -1
	
	IF GET_RANDOM_INT_IN_RANGE(0, 100) > 50
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemyWeaponDisruption_Low)
			iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Ped_Weapon_Disruption_Low_1
		ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemyWeaponDisruption_Medium)
			iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Ped_Weapon_Disruption_Med_1
		ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemyWeaponDisruption_High)
			iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Ped_Weapon_Disruption_High_1
		ENDIF
	ELSE
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemyWeaponDisruption_Low)
			iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Ped_Weapon_Disruption_Low_2
		ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemyWeaponDisruption_Medium)
			iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Ped_Weapon_Disruption_Med_2
		ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemyWeaponDisruption_High)
			iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Ped_Weapon_Disruption_High_2
		ENDIF
	ENDIF
		
	IF iIndex = -1
		EXIT
	ENDIF
	
	INT iMod = g_FMMC_STRUCT.sPoolVars.iInt[iIndex]
	
	PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_WEAPONS_DISRUPTED - iIndex: ", iIndex, " iMod: ", iMod)
	
	g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].gun = GET_ACTOR_WEAPON_TYPE_FOR_CREATOR_MODEL_ARRAY(iMod)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Location Scoped																															 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_LOCATION_SCOPED_SIDE_GATE_NORTH(INT iLoc)	
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iAltVarsBitsetLocation, ciAltVarsBS_IslandHeist_Locations_Flag_ScopedSideGateNorth)
		EXIT
	ENDIF
	
	IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_SideGateNorth)
		EXIT
	ENDIF
		
	PRINTLN("[LM][AltVarsSystem][Loc: ", iLoc, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_LOCATION_SCOPED_SIDE_GATE_NORTH - Setting Blip Range to 0.0, should always display.")
	
	g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipRange = 0.0
	g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.iShowBlipPreReqRequired = -1
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_LOCATION_SCOPED_SIDE_GATE_SOUTH(INT iLoc)	
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iAltVarsBitsetLocation, ciAltVarsBS_IslandHeist_Locations_Flag_ScopedSideGateSouth)
		EXIT
	ENDIF
	
	IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_SideGateSouth)
		EXIT
	ENDIF
		
	PRINTLN("[LM][AltVarsSystem][Loc: ", iLoc, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_LOCATION_SCOPED_SIDE_GATE_SOUTH - Setting Blip Range to 0.0, should always display.")
	
	g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipRange = 0.0
	g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.iShowBlipPreReqRequired = -1
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_LOCATION_SCOPED_RAPPEL_POINT_NORTH(INT iLoc)	
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iAltVarsBitsetLocation, ciAltVarsBS_IslandHeist_Locations_Flag_ScopedRappelPointNorth)
		EXIT
	ENDIF
	
	IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_RappelNorth)
		EXIT
	ENDIF
		
	PRINTLN("[LM][AltVarsSystem][Loc: ", iLoc, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_LOCATION_SCOPED_RAPPEL_POINT_NORTH - Setting Blip Range to 0.0, should always display.")
	
	g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipRange = 0.0
	g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.iShowBlipPreReqRequired = -1
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_LOCATION_SCOPED_RAPPEL_POINT_SOUTH(INT iLoc)	
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iAltVarsBitsetLocation, ciAltVarsBS_IslandHeist_Locations_Flag_ScopedRappelPointSouth)
		EXIT
	ENDIF
	
	IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_RappelSouth)
		EXIT
	ENDIF
		
	PRINTLN("[LM][AltVarsSystem][Loc: ", iLoc, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_LOCATION_SCOPED_RAPPEL_POINT_SOUTH - Setting Blip Range to 0.0, should always display.")
	
	g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipRange = 0.0
	g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.iShowBlipPreReqRequired = -1
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_LOCATION_SCOPED_UNDERWATER_TUNNEL(INT iLoc)	
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iAltVarsBitsetLocation, ciAltVarsBS_IslandHeist_Locations_Flag_ScopedUnderwaterTunnel)
		EXIT
	ENDIF
	
	IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_Tunnel)
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Loc: ", iLoc, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_LOCATION_SCOPED_UNDERWATER_TUNNEL_ISLAND_HEIST - Setting Blip Range to 0.0, should always display.")
	
	g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipRange = 0.0
	g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.iShowBlipPreReqRequired = -1
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Approach Chosen																															 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_MC_ALT_VAR_ISLAND_HEIST_DEFINED_SPAWN_GROUP_SET(SPAWN_GROUP_DEFINE_ACTIVATION eSpawnGroupActivationType)
	
	SWITCH eSpawnGroupActivationType
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_SUB_EAST_DOCK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_SUB_WEST_DOCK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_STEALTH_PLANE_HALO_JUMP
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_STEALTH_CHOPPER_NDROPZONE
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_STEALTH_CHOPPER_SDROPZONE
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_SMUGGLER_PLANE_AIRSTRIP
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_SMUGGLER_PLANE_MAIN_DOCKS
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_SMUGGLER_PLANE_SMALL_DOCKS
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_GUN_BOAT_MAIN_DOCKS
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_GUN_BOAT_SMALL_DOCKS
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_DRAINAGE_TUNNEL
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_ONLY_SUBMARINES
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_ONLY_NO_SUBMARINES
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_ONLY_HELICOPTORS
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_ONLY_BOATS		
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_MORNING_OR_NOON_TIME
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_NIGHT_OR_MIDNIGHT_TIME		
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TARGET_MADRAZO_DOCS
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TARGET_BONDS
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TARGET_DIAMOND
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TARGET_STATUE
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TARGET_NECKLACE
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TARGET_ALCOHOL 
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_WEAPON_STASH
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_ESCAPE_AIRSTRIP
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_ESCAPE_MAIN_DOCKS
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_ESCAPE_SMALL_DOCKS
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_ESCAPE_SUBMARINE
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TROJAN_VEH_AIRSTRIP	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TROJAN_VEH_NORTHDOCK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TROJAN_VEH_DRUGPROCESS
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TROJAN_VEH_MAINDOCK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TROJAN_VEH_COMPOUND
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_SUB_WEST_BEACH
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_GATE_EXPLOSIVES_ENTER	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_KEYCARD_SOUTH_ENTER		
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_KEYCARD_NORTH_ENTER		
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_RAPPEL_SOUTH_ENTER		
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_RAPPEL_NORTH_ENTER		
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_VEHICLE_FRONT_GATE_ENTER	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_SEWER_GRATE_ENTER		
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_GATE_EXIT		
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_KEYCARD_SOUTH_EXIT		
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_KEYCARD_NORTH_EXIT		
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_RAPPEL_SOUTH_EXIT			
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_RAPPEL_NORTH_EXIT			
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DOES_MC_ALT_VAR_ISLAND_HEIST_DEFINED_SPAWN_GROUP_MATCH_WITH_CHOSEN(SPAWN_GROUP_DEFINE_ACTIVATION eSpawnGroupActivationType)
		
	SWITCH eSpawnGroupActivationType
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_SUB_EAST_DOCK	
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SubmarineEastDocks)
		BREAK	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_SUB_WEST_DOCK	
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SubmarineWestDocks)
		BREAK	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_STEALTH_PLANE_HALO_JUMP	
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_StealthPlaneHALOJump)
		BREAK	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_STEALTH_CHOPPER_NDROPZONE	
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_StealthChopperNorthDropZone)
		BREAK	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_STEALTH_CHOPPER_SDROPZONE	
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_StealthChopperSouthDropZone)
		BREAK	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_SMUGGLER_PLANE_AIRSTRIP	
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SmugglerPlaneAirstrip)
		BREAK	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_SMUGGLER_PLANE_MAIN_DOCKS	
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SmugglerBoatMainDocks)
		BREAK	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_SMUGGLER_PLANE_SMALL_DOCKS	
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SmugglerBoaSmallDocks)
		BREAK	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_GUN_BOAT_MAIN_DOCKS	
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_GunBoatMainDocks)
		BREAK	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_GUN_BOAT_SMALL_DOCKS	
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_GunBoatSmallDocks)
		BREAK	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_DRAINAGE_TUNNEL
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_DrainageTunnel)
		BREAK	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_ONLY_SUBMARINES	
			IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_DrainageTunnel)
			OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SubmarineEastDocks)
			OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SubmarineWestDocks)
			OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SubmarineWestBeach)	
				RETURN TRUE
			ENDIF
		BREAK	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_ONLY_NO_SUBMARINES	
			IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_DrainageTunnel)
			AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SubmarineEastDocks)
			AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SubmarineWestDocks)
			AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SubmarineWestBeach)	
				RETURN TRUE
			ENDIF
		BREAK	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_ONLY_HELICOPTORS	
			IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_StealthChopperNorthDropZone)
			OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_StealthChopperSouthDropZone)
				RETURN TRUE
			ENDIF
		BREAK	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_ONLY_BOATS
			IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SmugglerBoatMainDocks)
			OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SmugglerBoaSmallDocks)
			OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_GunBoatMainDocks)
			OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_GunBoatSmallDocks)
				RETURN TRUE
			ENDIF
		BREAK	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_MORNING_OR_NOON_TIME
			IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_TimeOfDayOverrideDay)
				RETURN TRUE
			ENDIF
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_NIGHT_OR_MIDNIGHT_TIME			
			IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_TimeOfDayOverrideNight)
				RETURN TRUE
			ENDIF
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TARGET_MADRAZO_DOCS	
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_MadrazoFiles)
		BREAK	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TARGET_BONDS	
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_Bonds)
		BREAK	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TARGET_DIAMOND	
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_PinkDiamond)
		BREAK	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TARGET_STATUE	
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_SapphirePantherStatue)
		BREAK	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TARGET_NECKLACE	
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_PearlNecklace)
		BREAK	
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TARGET_ALCOHOL	
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_Tequila)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_WEAPON_STASH
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponStash)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_ESCAPE_AIRSTRIP
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteAirstrip)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_ESCAPE_MAIN_DOCKS
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteMainDocks)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_ESCAPE_SMALL_DOCKS
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteSmallDocks)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_ESCAPE_SUBMARINE
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteSubmarine)
		BREAK		
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TROJAN_VEH_AIRSTRIP	
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_TrojanVehicleLocation_1)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TROJAN_VEH_NORTHDOCK
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_TrojanVehicleLocation_2)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TROJAN_VEH_DRUGPROCESS
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_TrojanVehicleLocation_3)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TROJAN_VEH_MAINDOCK
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_TrojanVehicleLocation_4)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_TROJAN_VEH_COMPOUND
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_TrojanVehicleLocation_5)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_APPROACH_SUB_WEST_BEACH
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SubmarineWestBeach)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_GATE_EXPLOSIVES_ENTER
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceExplosivesFrontGate)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_KEYCARD_SOUTH_ENTER		
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceKeycardSouth)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_KEYCARD_NORTH_ENTER		
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceKeycardNorth)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_RAPPEL_SOUTH_ENTER				
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceRappelSouth)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_RAPPEL_NORTH_ENTER				
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceRappelNorth)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_VEHICLE_FRONT_GATE_ENTER			
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceVehicleFrontGate)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_SEWER_GRATE_ENTER				
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceSewerGrate)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_GATE_EXIT				
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitFrontGate)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_KEYCARD_SOUTH_EXIT				
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitKeycardSouth)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_KEYCARD_NORTH_EXIT				
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitKeycardNorth)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_RAPPEL_SOUTH_EXIT					
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitRappelSouth)
		BREAK
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_ISLAND_HEIST_COMPOUND_RAPPEL_NORTH_EXIT					
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitRappelNorth)
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_SUB_SPAWN_GROUP_ACTIVATION(INT iSpawnGroup, INT iSubSpawnGroup)	
	
	IF NOT IS_MC_ALT_VAR_ISLAND_HEIST_DEFINED_SPAWN_GROUP_SET(g_FMMC_STRUCT.eRandomEntitySpawnSub_DefineActivation[iSpawnGroup][iSubSpawnGroup])
		EXIT
	ENDIF
	
	IF NOT DOES_MC_ALT_VAR_ISLAND_HEIST_DEFINED_SPAWN_GROUP_MATCH_WITH_CHOSEN(g_FMMC_STRUCT.eRandomEntitySpawnSub_DefineActivation[iSpawnGroup][iSubSpawnGroup])
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][SpawnGroup: ", iSpawnGroup, "][SubSpawnGroup: ", iSubSpawnGroup, "][DefinedAction: ", GET_SPAWN_GROUP_DEFINE_ACTIVATION_NAME_FOR_PRINT(g_FMMC_STRUCT.eRandomEntitySpawnSub_DefineActivation[iSpawnGroup][iSubSpawnGroup]), "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_SUB_SPAWN_GROUP_ACTIVATION - Setting Sub Spawn Group to SPAWN_GROUP_DEFINE_ACTIVATION_FORCED_ON")
	
	g_FMMC_STRUCT.eRandomEntitySpawnSub_DefineActivation[iSpawnGroup][iSubSpawnGroup] = SPAWN_GROUP_DEFINE_ACTIVATION_FORCED_ON
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_SPAWN_GROUP_ACTIVATION(INT iSpawnGroup)	
	
	IF NOT IS_MC_ALT_VAR_ISLAND_HEIST_DEFINED_SPAWN_GROUP_SET(g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[iSpawnGroup])
		EXIT
	ENDIF
	
	IF NOT DOES_MC_ALT_VAR_ISLAND_HEIST_DEFINED_SPAWN_GROUP_MATCH_WITH_CHOSEN(g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[iSpawnGroup])
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][SpawnGroup: ", iSpawnGroup, "][DefinedAction: ", GET_SPAWN_GROUP_DEFINE_ACTIVATION_NAME_FOR_PRINT(g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[iSpawnGroup]), "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_SPAWN_GROUP_ACTIVATION - Setting Spawn Group to SPAWN_GROUP_DEFINE_ACTIVATION_FORCED_ON")
	
	g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[iSpawnGroup] = SPAWN_GROUP_DEFINE_ACTIVATION_FORCED_ON
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Approach Type																															 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_LOADOUT_FORCED_SUPPRESSOR_APPROACH_TYPE(INT iInventory, INT iWeapon, INT iComponent)	
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_IslandHeist_Generic_Flag_StealthApproachForceSuppressors)
		EXIT
	ENDIF
	
	IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponSuppressors)
		EXIT
	ENDIF
		
	IF g_FMMC_STRUCT.sPlayerWeaponInventories[iInventory].sWeaponStruct[iWeapon].wtWeapon = WEAPONTYPE_INVALID
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.sPlayerWeaponInventories[iInventory].sWeaponStruct[iWeapon].eWeaponComponents[iComponent] != WEAPONCOMPONENT_INVALID
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][iInventoryIndex: ", iInventory, "][Weapon: ", iWeapon, "][Component: ", iComponent, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_LOADOUT_FORCED_SUPPRESSOR_APPROACH_TYPE - Forcing Suppressor onto weapon.")
	
	g_FMMC_STRUCT.sPlayerWeaponInventories[iInventory].sWeaponStruct[iWeapon].eWeaponComponents[iComponent] = GET_WEAPONCOMPONENT_TYPE_SUPPRESSOR_FROM_WEAPON_TYPE(g_FMMC_STRUCT.sPlayerWeaponInventories[iInventory].sWeaponStruct[iWeapon].wtWeapon)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Objective Text																														 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_OBJECTIVE_TEXT_FOR_TARGET(INT iRule, INT iTeam)

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_Use_PrimaryObjectiveText_Target)
	AND NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_Use_SecondaryObjectiveText_Target)
	AND NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_Use_WaitObjectiveText_Target)	
		EXIT
	ENDIF
		
	INT iIndex = -1
		
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_MadrazoFiles)
		iIndex = 0
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_Bonds)
		iIndex = 1
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_PinkDiamond)
		iIndex = 2
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_SapphirePantherStatue)
		iIndex = 3
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_PearlNecklace)
		iIndex = 4
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_Tequila)
		iIndex = 5
	ENDIF
	
	IF iIndex = -1
		EXIT
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_Use_PrimaryObjectiveText_Target)
		PRINTLN("[LM][AltVarsSystem][iTeam: ", iTeam,"][iRule: ", iRule, "][ObjectiveText] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_OBJECTIVE_TEXT_FOR_TARGET - Overwriting Primary text with iIndex: " , iIndex, " text: ", g_FMMC_STRUCT.tlCustomStringList[iIndex])
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective[iRule] = g_FMMC_STRUCT.tlCustomStringList[iIndex]
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_Use_SecondaryObjectiveText_Target)
		PRINTLN("[LM][AltVarsSystem][iTeam: ", iTeam,"][iRule: ", iRule, "][ObjectiveText] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_OBJECTIVE_TEXT_FOR_TARGET - Overwriting Secondary text with iIndex: " , iIndex, " text: ", g_FMMC_STRUCT.tlCustomStringList[iIndex])
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective1[iRule] = g_FMMC_STRUCT.tlCustomStringList[iIndex]
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_Use_WaitObjectiveText_Target)	
		PRINTLN("[LM][AltVarsSystem][iTeam: ", iTeam,"][iRule: ", iRule, "][ObjectiveText] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_OBJECTIVE_TEXT_FOR_TARGET - Overwriting Wait text with iIndex: " , iIndex, " text: ", g_FMMC_STRUCT.tlCustomStringList[iIndex])
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63WaitMessage[iRule] = g_FMMC_STRUCT.tlCustomStringList[iIndex]
	ENDIF
	
ENDPROC


PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_OBJECTIVE_TEXT_FOR_TARGET_ALT(INT iRule, INT iTeam)

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_Use_PrimaryObjectiveText_TargetAlt)
	AND NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_Use_WaitObjectiveText_TargetAlt)
	AND NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_Use_SecondaryObjectiveText_TargetAlt)	
		EXIT
	ENDIF
		
	INT iIndex = -1
		
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_MadrazoFiles)
		iIndex = 10
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_Bonds)
		iIndex = 11
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_PinkDiamond)
		iIndex = 12
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_SapphirePantherStatue)
		iIndex = 13
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_PearlNecklace)
		iIndex = 14
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_Tequila)
		iIndex = 15
	ENDIF
	
	IF iIndex = -1
		EXIT
	ENDIF
		
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_Use_PrimaryObjectiveText_TargetAlt)
		PRINTLN("[LM][AltVarsSystem][iTeam: ", iTeam,"][iRule: ", iRule, "][ObjectiveText] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_OBJECTIVE_TEXT_FOR_TARGET_ALT - Overwriting Primary text with iIndex: " , iIndex, " text: ", g_FMMC_STRUCT.tlCustomStringList[iIndex])
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective[iRule] = g_FMMC_STRUCT.tlCustomStringList[iIndex]
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_Use_SecondaryObjectiveText_TargetAlt)
		PRINTLN("[LM][AltVarsSystem][iTeam: ", iTeam,"][iRule: ", iRule, "][ObjectiveText] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_OBJECTIVE_TEXT_FOR_TARGET_ALT - Overwriting Secondary text with iIndex: " , iIndex, " text: ", g_FMMC_STRUCT.tlCustomStringList[iIndex])
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective1[iRule] = g_FMMC_STRUCT.tlCustomStringList[iIndex]
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_Use_WaitObjectiveText_TargetAlt)	
		PRINTLN("[LM][AltVarsSystem][iTeam: ", iTeam,"][iRule: ", iRule, "][ObjectiveText] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_OBJECTIVE_TEXT_FOR_TARGET_ALT - Overwriting Wait text with iIndex: " , iIndex, " text: ", g_FMMC_STRUCT.tlCustomStringList[iIndex])
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63WaitMessage[iRule] = g_FMMC_STRUCT.tlCustomStringList[iIndex]
	ENDIF
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_OBJECTIVE_TEXT_FOR_EXIT(INT iRule, INT iTeam)

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_Use_PrimaryObjectiveText_Exit)
	AND NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_Use_SecondaryObjectiveText_Exit) 
	AND NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_Use_WaitObjectiveText_Exit)
		EXIT
	ENDIF
		
	INT iIndex = -1
		
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteAirstrip)
		iIndex = 6
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteMainDocks)
		iIndex = 7
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteSmallDocks)
		iIndex = 8
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteSubmarine)
		iIndex = 9
	ENDIF
	
	IF iIndex = -1
		EXIT
	ENDIF
		
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_Use_PrimaryObjectiveText_Exit)
		PRINTLN("[LM][AltVarsSystem][iTeam: ", iTeam,"][iRule: ", iRule, "][ObjectiveText] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_OBJECTIVE_TEXT_FOR_TARGET - Overwriting Primary text with iIndex: " , iIndex, " text: ", g_FMMC_STRUCT.tlCustomStringList[iIndex])
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective[iRule] = g_FMMC_STRUCT.tlCustomStringList[iIndex]
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_Use_SecondaryObjectiveText_Exit)
		PRINTLN("[LM][AltVarsSystem][iTeam: ", iTeam,"][iRule: ", iRule, "][ObjectiveText] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_OBJECTIVE_TEXT_FOR_TARGET - Overwriting Secondary text with iIndex: " , iIndex, " text: ", g_FMMC_STRUCT.tlCustomStringList[iIndex])
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective1[iRule] = g_FMMC_STRUCT.tlCustomStringList[iIndex]
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_Use_WaitObjectiveText_Exit)	
		PRINTLN("[LM][AltVarsSystem][iTeam: ", iTeam,"][iRule: ", iRule, "][ObjectiveText] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_OBJECTIVE_TEXT_FOR_TARGET - Overwriting Wait text with iIndex: " , iIndex, " text: ", g_FMMC_STRUCT.tlCustomStringList[iIndex])
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63WaitMessage[iRule] = g_FMMC_STRUCT.tlCustomStringList[iIndex]
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Gang Chase																														 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_CUSTOM_GANG_CHASE_TYPE(INT iGangChaseType)
	
	IF (iGangChaseType = ciBACKUP_TYPE_CUSTOM_1)
	OR (iGangChaseType = ciBACKUP_TYPE_CUSTOM_2)
	OR (iGangChaseType = ciBACKUP_TYPE_CUSTOM_3)
	OR (iGangChaseType = ciBACKUP_TYPE_CUSTOM_4)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_HELICOPTER_DISRUPTION(INT iRule, INT iTeam)

	// Check that helicopter disruption rule flag has been set
	IF NOT IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_GangChase_HelicoptersDisrupted)
		EXIT
	ENDIF
	
	// Check that helicopters have been disrupted
	IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemiesHelicoptorDisruption)
		EXIT
	ENDIF
	
	// remove air type gang chases (single unit type)
	INT iConfigIndex = -1
	IF IS_CUSTOM_GANG_CHASE_TYPE(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupType[iRule])
		IF GET_GANG_CHASE_UNIT_CREATOR_CONFIG_INDEX_FROM_GANG_TYPE(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupType[iRule], iConfigIndex)
		
			IF g_FMMC_STRUCT.sGangChaseUnitConfigs[iConfigIndex].eType = FMMC_GANG_CHASE_TYPE_AIR
				PRINTLN("[RM][AltVarsSystem][Gang Chase] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_HELICOPTER_DISRUPTION - clearing gang chase type")
				g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupType[iRule] = ciBACKUP_TYPE_NONE
			ENDIF
			
		ENDIF
	ENDIF
	
	// remove air type gang chases (multi unit type)
	INT iMultipleGangChaseTypeIndex
	FOR iMultipleGangChaseTypeIndex = 0 TO FMMC_MAX_GANG_CHASE_UNIT_TYPES - 1
		
		IF NOT IS_CUSTOM_GANG_CHASE_TYPE(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sMultipleGangChaseSettings[iRule][iMultipleGangChaseTypeIndex].iType)
			RELOOP
		ENDIF
		
		IF NOT GET_GANG_CHASE_UNIT_CREATOR_CONFIG_INDEX_FROM_GANG_TYPE(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sMultipleGangChaseSettings[iRule][iMultipleGangChaseTypeIndex].iType, iConfigIndex)
			RELOOP
		ENDIF
		
		IF g_FMMC_STRUCT.sGangChaseUnitConfigs[iConfigIndex].eType = FMMC_GANG_CHASE_TYPE_AIR
			PRINTLN("[RM][AltVarsSystem][Gang Chase] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_HELICOPTER_DISRUPTION - clearing gang chase type")
			g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sMultipleGangChaseSettings[iRule][iMultipleGangChaseTypeIndex].iType = ciBACKUP_TYPE_NONE
		ENDIF
		
	ENDFOR
		
ENDPROC


PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_PED_HEALTH_POISONED_WATER(INT iRule, INT iTeam)
	
	IF NOT IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_GangChase_Peds_HealthPoisoned)		
		EXIT
	ENDIF
	
	INT iIndex = -1
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_PoisonedWaterSupply)
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Ped_Health_Poisoned_0
	ENDIF
		
	IF iIndex = -1
		EXIT
	ENDIF
	
	INT iMod = g_FMMC_STRUCT.sPoolVars.iInt[iIndex]
	
	IF iMod = 0
		PRINTLN("[RM][AltVarsSystem][Gang Chase] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_PED_HEALTH_POISONED_WATER - Modifier not set")
		EXIT
	ENDIF
	
	PRINTLN("[RM][AltVarsSystem][Gang Chase] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_PED_HEALTH_POISONED_WATER - iIndex: ", iIndex, " iMod: ", iMod)
	fGangChasePedHealthMod += (TO_FLOAT(iMod)*0.01)
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_PED_ACCURACY_POISONED_WATER(INT iRule, INT iTeam)
	
	IF NOT IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_GangChase_Peds_AccuracyPoisoned)
		EXIT
	ENDIF
	
	INT iIndex = -1
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_PoisonedWaterSupply)
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Ped_Accuracy_Poisoned_0
	ENDIF
		
	IF iIndex = -1
		EXIT
	ENDIF
	
	INT iMod = g_FMMC_STRUCT.sPoolVars.iInt[iIndex]
	
	IF iMod = 0
		PRINTLN("[RM][AltVarsSystem][Gang Chase] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_PED_ACCURACY_POISONED_WATER - Modifier not set")
		EXIT
	ENDIF
	
	PRINTLN("[RM][AltVarsSystem][Gang Chase] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_PED_ACCURACY_POISONED_WATER - iIndex: ", iIndex, " iMod: ", iMod)
	
	fGangChasePedAccuracyMod += (TO_FLOAT(iMod) * 0.01)
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_PED_ARMOUR_DISRUPTED(INT iRule, INT ITeam)
	PRINTLN("[RM][AltVarsSystem][Gang Chase] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_PED_ARMOUR_DISRUPTED")
	
	IF NOT IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_GangChase_Peds_ArmourDisrupted)
		EXIT
	ENDIF
	
	INT iIndex = -1
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemiesArmourDisruption)
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Ped_Health_Armour_0
	ENDIF
	
	IF iIndex = -1
		EXIT
	ENDIF
	
	INT iMod = g_FMMC_STRUCT.sPoolVars.iInt[iIndex]
	
	IF iMod = 0
		PRINTLN("[RM][AltVarsSystem][Gang Chase] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_PED_ARMOUR_DISRUPTED - Modifier not set")
		EXIT
	ENDIF
	
	PRINTLN("[RM][AltVarsSystem][Gang Chase] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_PED_ARMOUR_DISRUPTED - iIndex: ", iIndex, " iMod: ", iMod)
	
	// get the gang chase unit types that have been set for this rule
	INT iGangChaseType = -1
	INT iConfigIndex = -1
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupType[iRule] != ciBACKUP_TYPE_NONE
		iGangChaseType = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupType[iRule]
		IF GET_GANG_CHASE_UNIT_CREATOR_CONFIG_INDEX_FROM_GANG_TYPE(iGangChaseType, iConfigIndex)
			g_FMMC_STRUCT.sGangChaseUnitConfigs[iConfigIndex].fPedArmour = TO_FLOAT(iMod) * 0.01
			g_FMMC_STRUCT.sGangChaseUnitConfigs[iConfigIndex].bUseAlternateComponentsForPedOutfit = TRUE
		ENDIF
	ENDIF
	
	INT iMultipleGangChaseTypeIndex
	FOR iMultipleGangChaseTypeIndex = 0 TO FMMC_MAX_GANG_CHASE_UNIT_TYPES - 1
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sMultipleGangChaseSettings[iRule][iMultipleGangChaseTypeIndex].iType != ciBACKUP_TYPE_NONE
			iGangChaseType = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sMultipleGangChaseSettings[iRule][iMultipleGangChaseTypeIndex].iType
			IF GET_GANG_CHASE_UNIT_CREATOR_CONFIG_INDEX_FROM_GANG_TYPE(iGangChaseType, iConfigIndex)
				g_FMMC_STRUCT.sGangChaseUnitConfigs[iConfigIndex].fPedArmour = TO_FLOAT(iMod) * 0.01
				g_FMMC_STRUCT.sGangChaseUnitConfigs[iConfigIndex].bUseAlternateComponentsForPedOutfit = TRUE
			ENDIF 		
		ENDIF
	ENDFOR
	
ENDPROC

FUNC BOOL GET_GANG_CHASE_WEAPON_DISRUPTION_BIT_INDEX_FROM_CONFIG_INDEX(INT iConfigIndex, INT& iBitIndex)
	
	iBitIndex = -1
	SWITCH iConfigIndex
		CASE 0
			iBitIndex = ciAltVarsBS_IslandHeist_GangChase_Flag_Peds_WeaponDisruption_Unit_1
		BREAK
		CASE 1
			iBitIndex = ciAltVarsBS_IslandHeist_GangChase_Flag_Peds_WeaponDisruption_Unit_2
		BREAK
		CASE 2
			iBitIndex = ciAltVarsBS_IslandHeist_GangChase_Flag_Peds_WeaponDisruption_Unit_3
		BREAK
		CASE 3
			iBitIndex = ciAltVarsBS_IslandHeist_GangChase_Flag_Peds_WeaponDisruption_Unit_4
		BREAK
	ENDSWITCH
	
	IF iBitIndex >= 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_WEAPON_DISRUPTION()		 
	
	INT i, iBitIndex, iAltVarIndex1, iAltVarIndex2, iWeaponIndex1, iWeaponIndex2 
	FOR i = 0 TO FMMC_MAX_GANG_CHASE_UNIT_CONFIGS -1
		
		// if this config is affected by weapon disruption
		IF NOT GET_GANG_CHASE_WEAPON_DISRUPTION_BIT_INDEX_FROM_CONFIG_INDEX(i, iBitIndex)
			RELOOP
		ENDIF
		
		IF NOT IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGangChase, iBitIndex)
			RELOOP
		ENDIF
		
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemyWeaponDisruption_Low)
			g_FMMC_STRUCT.sGangChaseUnitConfigs[i].bWeaponsDisrupted = TRUE			
			IF g_FMMC_STRUCT.sGangChaseUnitConfigs[i].eType = FMMC_GANG_CHASE_TYPE_ON_FOOT
				PRINTLN("[RM][AltVarsSystem][Gang Chase] PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_WEAPON_DISRUPTION: (Low) Setting weapons from ped loadout")
				iAltVarIndex1 = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Ped_Weapon_Disruption_Low_1
				iAltVarIndex2 = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Ped_Weapon_Disruption_Low_2
			ELSE
				PRINTLN("[RM][AltVarsSystem][Gang Chase] PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_WEAPON_DISRUPTION: (Low) Setting weapons from gang chase loadout")
				iAltVarIndex1 = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Gang_Chase_Ped_Weapon_Disruption_Low_1
				iAltVarIndex2 = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Gang_Chase_Ped_Weapon_Disruption_Low_2
			ENDIF			
		ENDIF
		
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemyWeaponDisruption_Medium)
			g_FMMC_STRUCT.sGangChaseUnitConfigs[i].bWeaponsDisrupted = TRUE
			
			IF g_FMMC_STRUCT.sGangChaseUnitConfigs[i].eType = FMMC_GANG_CHASE_TYPE_ON_FOOT
				PRINTLN("[RM][AltVarsSystem][Gang Chase] PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_WEAPON_DISRUPTION: (Med) Setting weapons from ped loadout")
				iAltVarIndex1 = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Ped_Weapon_Disruption_Med_1
				iAltVarIndex2 = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Ped_Weapon_Disruption_Med_2
			ELSE
				PRINTLN("[RM][AltVarsSystem][Gang Chase] PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_WEAPON_DISRUPTION: (Med) Setting weapons from gang chase loadout")
				iAltVarIndex1 = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Gang_Chase_Ped_Weapon_Disruption_Med_1
				iAltVarIndex2 = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Gang_Chase_Ped_Weapon_Disruption_Med_2				
			ENDIF			
		ENDIF
	
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemyWeaponDisruption_High)
			g_FMMC_STRUCT.sGangChaseUnitConfigs[i].bWeaponsDisrupted = TRUE
			IF g_FMMC_STRUCT.sGangChaseUnitConfigs[i].eType = FMMC_GANG_CHASE_TYPE_ON_FOOT
				PRINTLN("[RM][AltVarsSystem][Gang Chase] PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_WEAPON_DISRUPTION: (High) Setting weapons from ped loadout")
				iAltVarIndex1 = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Ped_Weapon_Disruption_High_1
				iAltVarIndex2 = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Ped_Weapon_Disruption_High_2
			ELSE
				PRINTLN("[RM][AltVarsSystem][Gang Chase] PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_WEAPON_DISRUPTION: (High) Setting weapons from gang chase loadout")
				iAltVarIndex1 = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Gang_Chase_Ped_Weapon_Disruption_High_1
				iAltVarIndex2 = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Gang_Chase_Ped_Weapon_Disruption_High_2					
			ENDIF			
		ENDIF
				
		// override random weapons
		IF g_FMMC_STRUCT.sGangChaseUnitConfigs[i].bWeaponsDisrupted = TRUE
			iWeaponIndex1 = g_FMMC_STRUCT.sPoolVars.iInt[iAltVarIndex1]
			iWeaponIndex2 = g_FMMC_STRUCT.sPoolVars.iInt[iAltVarIndex2]
			
			g_FMMC_STRUCT.sGangChaseUnitConfigs[i].wtRandomWeapon1 = GET_ACTOR_WEAPON_TYPE_FOR_CREATOR_MODEL_ARRAY(iWeaponIndex1)
			PRINTLN("[RM][AltVarsSystem][Gang Chase] PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_WEAPON_DISRUPTION: Disrupted weapon 1: ",  GET_THE_WEAPON_NAME(g_FMMC_STRUCT.sGangChaseUnitConfigs[i].wtRandomWeapon1))
			
			g_FMMC_STRUCT.sGangChaseUnitConfigs[i].wtRandomWeapon2 = GET_ACTOR_WEAPON_TYPE_FOR_CREATOR_MODEL_ARRAY(iWeaponIndex2)
			PRINTLN("[RM][AltVarsSystem][Gang Chase] PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_WEAPON_DISRUPTION: Disrupted weapon 2: ",  GET_THE_WEAPON_NAME(g_FMMC_STRUCT.sGangChaseUnitConfigs[i].wtRandomWeapon2))
		ENDIF				
	ENDFOR

ENDPROC
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Abilities																														 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_CARPET_BOMB_ABILITY_ENABLED(INT iRule, INT iTeam)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_IslandHeist_Generic_Flag_PlanningBoardSelectedCarpetBomb)
		EXIT
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CarpetBomb)
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][iTeam: ", iTeam,"][iRule: ", iRule, "][Ability] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_CARPET_BOMB_ABILITY_ENABLED - Disabling Carpet Bombing.")
	
	CLEAR_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_PLAYER_ABILITY_ENABLE_AIRSTRIKE)
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_SNIPER_ABILITY_ENABLED(INT iRule, INT iTeam)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_IslandHeist_Generic_Flag_PlanningBoardSelectedSniper)
		EXIT
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_AbilitiesSniper)
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][iTeam: ", iTeam,"][iRule: ", iRule, "][Ability] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_SNIPER_ABILITY_ENABLED - Disabling Sniper")
	
	CLEAR_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_PLAYER_ABILITY_ENABLE_SUPPORT_SNIPER)
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_HELI_BACKUP_ABILITY_ENABLED(INT iRule, INT iTeam)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_IslandHeist_Generic_Flag_PlanningBoardSelectedHeliBackup)
		EXIT
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_AbilitiesHeliBackup)
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][iTeam: ", iTeam,"][iRule: ", iRule, "][Ability] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_HELI_BACKUP_ABILITY_ENABLED - Disabling Sniper")
	
	CLEAR_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_PLAYER_ABILITY_ENABLE_HELI_BACKUP)
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_RECON_DRONE_ABILITY_ENABLED(INT iRule, INT iTeam)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_IslandHeist_Generic_Flag_PlanningBoardSelectedReconDrone)
		EXIT
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_AbilitiesReconnaissanceDrone)
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][iTeam: ", iTeam,"][iRule: ", iRule, "][Ability] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_RECON_DRONE_ABILITY_ENABLED - Disabling Recon Drone")
	
	CLEAR_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_PLAYER_ABILITY_ENABLE_RECON_DRONE)
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_HEAVY_LOADOUT_ABILITY_ENABLED(INT iRule, INT iTeam)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_IslandHeist_Generic_Flag_PlanningBoardSelectedHeavyLoadout)
		EXIT
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_AbilitiesHeavyLoadout)
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][iTeam: ", iTeam,"][iRule: ", iRule, "][Ability] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_HEAVY_LOADOUT_ABILITY_ENABLED - Disabling Recon Drone")
	
	CLEAR_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_PLAYER_ABILITY_ENABLE_HEAVY_LOADOUT)
	
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: End Cutscene Override																														 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_END_CUTSCENE_WITH_PLAYTHROUGH()

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_IslandHeist_Generic_Flag_OverrideEndCutsceneWithPlaythroughs)
		EXIT
	ENDIF
	
	INT iStat = GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER(#IF IS_DEBUG_BUILD TRUE #ENDIF)
	
	IF iStat <= 0
		PRINTLN("[LM][AltVarsSystem][EndCutscene] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_END_CUTSCENE_WITH_PLAYTHROUGH - Not overriding. Playthrough Stat is: ", iStat, " for leader.")
		EXIT
	ENDIF
	
	INT iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_EndCutscene_PlaythroughOverride	
	
	INT iMod = g_FMMC_STRUCT.sPoolVars.iInt[iIndex]
	
	IF iMod <= 0
		PRINTLN("[LM][AltVarsSystem][EndCutscene] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_END_CUTSCENE_WITH_PLAYTHROUGH - Not overriding. iMod is 0.")
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][EndCutscene] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_END_CUTSCENE_WITH_PLAYTHROUGH - Overriding. iMod: ", iMod, " Playthrough Stat is: ", iStat)
	
	g_FMMC_STRUCT.iEndCutscene = iMod
	
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: End Cutscene Override																														 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_INTRO_CUTSCENE_BASED_ON_APPROACH(INT iRule, INT iTeam)

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAltVarsBitsetRule[iRule], ciAltVarsBS_IslandHeist_Rule_Flag_Use_Approach_IntroCutsceneOverride)
		EXIT
	ENDIF
	
	INT iIndex = -1
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SubmarineEastDocks)
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_IntroCutsceneIndex_ApproachOverride_SubmarineEastBeach
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SubmarineWestDocks)
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_IntroCutsceneIndex_ApproachOverride_SubmarineWestBeach
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_StealthPlaneHALOJump)
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_IntroCutsceneIndex_ApproachOverride_PlaneHaloJump
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_StealthChopperNorthDropZone)
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_IntroCutsceneIndex_ApproachOverride_ChopperNorth
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_StealthChopperSouthDropZone)
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_IntroCutsceneIndex_ApproachOverride_ChopperSouth
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SmugglerPlaneAirstrip)
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_IntroCutsceneIndex_ApproachOverride_PlaneSmuggler
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SmugglerBoatMainDocks)
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_IntroCutsceneIndex_ApproachOverride_SmugglerBoatMain
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SmugglerBoaSmallDocks)
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_IntroCutsceneIndex_ApproachOverride_SmugglerBoatSmall
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_GunBoatMainDocks)
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_IntroCutsceneIndex_ApproachOverride_GunBoatMain
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_GunBoatSmallDocks)
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_IntroCutsceneIndex_ApproachOverride_GunBoatSmall
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_DrainageTunnel)
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_IntroCutsceneIndex_ApproachOverride_DrainageTunnel
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SubmarineWestBeach)	
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_IntroCutsceneIndex_ApproachOverride_WestBeach
	ENDIF
	
	IF iIndex = -1
		PRINTLN("[LM][AltVarsSystem][IntroCutscene] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_INTRO_CUTSCENE_BASED_ON_APPROACH - Rule Flagged but no valid approach.")
		EXIT
	ENDIF
	
	INT iMod = g_FMMC_STRUCT.sPoolVars.iInt[iIndex]
	
	IF iMod = -1
		PRINTLN("[LM][AltVarsSystem][IntroCutscene] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_INTRO_CUTSCENE_BASED_ON_APPROACH - iIndex: ", iIndex, " iMod is -1. INVALID CREATOR SETUP.")	
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][IntroCutscene] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_INTRO_CUTSCENE_BASED_ON_APPROACH - Overriding, using Pool Var iIndex: ", iIndex, " Cutscene: ", iMod)
	
	g_FMMC_STRUCT.sPlayerRuleData[iRule].iPlayerRuleLimit[iTeam] = iMod
	
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Planning Board Inventory Selected																															 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_AMMO_TYPE_FOR_PED_AMMO_DROPS(INT iInventoryIndex)
	SWITCH iInventoryIndex
		CASE 0
			RETURN FMMC_PED_AMMO_DROP_TYPE_SHOTGUN
		CASE 1
			RETURN FMMC_PED_AMMO_DROP_TYPE_RIFLE
		CASE 2
			RETURN FMMC_PED_AMMO_DROP_TYPE_SNIPER
		CASE 3
			RETURN FMMC_PED_AMMO_DROP_TYPE_SMG_HOLLOW
		CASE 4
			RETURN FMMC_PED_AMMO_DROP_TYPE_RIFLE_FMJ
	ENDSWITCH
	
	RETURN -1
ENDFUNC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_FORCE_INVENTORY_INDEX_PLANNING_BOARD_SELECTION()

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_IslandHeist_Generic_Flag_PlanningBoardSelectedInventory)
		EXIT
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_IslandHeist_Generic_Flag_PlanningBoardSelectedWeaponStash)
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponStash)
		AND FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SmugglerPlaneAirstrip)
			EXIT
		ENDIF
	ENDIF
		
	INT iIndex = -1
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponLoadout_1)
		iIndex = 0
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponLoadout_2)
		iIndex = 1
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponLoadout_3)
		iIndex = 2
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponLoadout_4)
		iIndex = 3
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponLoadout_5)
		iIndex = 4
	ENDIF
	
	IF iIndex = -1
		EXIT
	ENDIF
	
	INT iTeam = 0
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		
		PRINTLN("[LM][AltVarsSystem][Team: ", iTeam, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_FORCE_INVENTORY_INDEX_PLANNING_BOARD_SELECTION - Forcing iStartingInventoryIndex to: ", iIndex)
	
		iInventory_Starting[iTeam] = iIndex
		
	ENDFOR
	
	g_FMMC_STRUCT.sPedAmmoDrop.iAmmoPickupType = GET_AMMO_TYPE_FOR_PED_AMMO_DROPS(iIndex)
	PRINTLN("[LM][AltVarsSystem] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_FORCE_INVENTORY_INDEX_PLANNING_BOARD_SELECTION - Forced sPedAmmoDrop.iAmmoPickupType to ", g_FMMC_STRUCT.sPedAmmoDrop.iAmmoPickupType)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Scoped Trojan Vehicle																														 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEHICLE_SCOPED_TROJAN(INT iVeh)

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAltVarsBitsetVeh, ciAltVarsBS_IslandHeist_Vehicles_Flag_TrojanVehicleScopedBlipRange)
		EXIT
	ENDIF
		
	INT iIndex = -1
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_TrojanVehicleScoped)
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Vehicle_BlipRange_ScopedTrojan
	ENDIF
	
	IF iIndex = -1
		EXIT
	ENDIF
	
	INT iMod = -1
	
	iMod = g_FMMC_STRUCT.sPoolVars.iInt[iIndex]
	
	IF iMod = -1
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][iVeh: ", iVeh, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEHICLE_SCOPED_TROJAN - Forcing fBlipRange to: ", iMod, " and ciBLIP_INFO_Always_Blip_On_Pause_Map set")
	
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.iBlipShowZonesBS = 0
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.iBlipHideZonesBS = 0	
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.fBlipRange = TO_FLOAT(iMod)
	SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.iBlipBitset, ciBLIP_INFO_Always_Blip_On_Pause_Map)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Scoped Power Station																													 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SCOPED_POWER_STATION(INT iInteractable)

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_PowerStationScopedBlipRange)
		EXIT
	ENDIF
		
	INT iIndex = -1
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_PowerStationScoped)
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Interactable_BlipRange_ScopedPowerStation
	ENDIF
	
	IF iIndex = -1
		EXIT
	ENDIF
	
	INT iMod = -1
	
	iMod = g_FMMC_STRUCT.sPoolVars.iInt[iIndex]
	
	IF iMod = -1
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][iInteractable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SCOPED_POWER_STATION - Forcing fBlipRange to: ", iMod, " and ciBLIP_INFO_Always_Blip_On_Pause_Map set")
	
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipShowZonesBS = 0
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipHideZonesBS = 0	
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.fBlipRange = TO_FLOAT(iMod)
	SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipBitset, ciBLIP_INFO_Always_Blip_On_Pause_Map)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Scoped Control Tower																													 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SCOPED_CONTROL_TOWER(INT iInteractable)

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_ControlTowerScopedBlipRange)
		EXIT
	ENDIF
		
	INT iIndex = -1
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ControlTowerScoped)
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Interactable_BlipRange_ScopedControlTower
	ENDIF
	
	IF iIndex = -1
		EXIT
	ENDIF
	
	INT iMod = -1
	
	iMod = g_FMMC_STRUCT.sPoolVars.iInt[iIndex]
	
	IF iMod = -1
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][iInteractable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SCOPED_CONTROL_TOWER - Forcing fBlipRange to: ", iMod, " and ciBLIP_INFO_Always_Blip_On_Pause_Map set")
	
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipShowZonesBS = 0
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipHideZonesBS = 0	
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.fBlipRange = TO_FLOAT(iMod)
	SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipBitset, ciBLIP_INFO_Always_Blip_On_Pause_Map)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Planning Board Selection: Time of Day override																											 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_FORCE_TIME_OF_DAY()
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_IslandHeist_Generic_Flag_PlanningBoardSelectedTimeOfDay)
		EXIT
	ENDIF
	
	INT iMod = -1
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_TimeOfDayOverrideDay)
		iMod = TIME_NOON
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_TimeOfDayOverrideNight)
		iMod = TIME_NIGHT
	ENDIF
	
	IF iMod = -1
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_FORCE_TIME_OF_DAY - Setting new Time of Day to: ", iMod)
	
	g_FMMC_STRUCT.iTimeOfDay = iMod
	g_FMMC_STRUCT.iTODOverrideHours = PICK_INT(iMod = TIME_NOON, ciTIME_OF_DAY_EARLY_EVENING, ciTIME_OF_DAY_PRE_DAWN) //Setting for use with ciOptionsBS27_ProgressTimeToPoint
	
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Golden Gun Drawer Key																									 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_GOLDEN_GUN_DRAW_KEY(INT iInteractable)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_GoldenGunDrawKey)
		EXIT
	ENDIF
	
	INT iMod = -2
	
	IF IS_BIT_SET(g_sHeistIslandConfig.iBitSet, HEIST_ISLAND_CONFIG_BITSET__GOLDEN_GUN_DRAW_KEY) // local only, don't care a bout leader data.
		iMod = ciPREREQ_None
	ENDIF
	
	IF iMod = -2
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "]  - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_GOLDEN_GUN_DRAW_KEY - Setting new PreRequisite to: ", iMod)
	
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_PreReqsRequired[0] = iMod
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Spawn And Position Gear Groups (Interactable)																							 			-----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SPAWN_GEAR_POSITION_INDEX(INT iGroup)
	
	INT iOffset = (iGroup*4)

	IF IS_BIT_SET(g_sHeistIslandConfig.iUniformLocationBitset, ENUM_TO_INT(HIGL_GROUP_ONE_LOCATION_ONE) + iOffset)
		RETURN 0
	ELIF IS_BIT_SET(g_sHeistIslandConfig.iUniformLocationBitset, ENUM_TO_INT(HIGL_GROUP_ONE_LOCATION_TWO) + iOffset)
		RETURN 1
	ELIF IS_BIT_SET(g_sHeistIslandConfig.iUniformLocationBitset, ENUM_TO_INT(HIGL_GROUP_ONE_LOCATION_THREE) + iOffset)
		RETURN 2
	ELIF IS_BIT_SET(g_sHeistIslandConfig.iUniformLocationBitset, ENUM_TO_INT(HIGL_GROUP_ONE_LOCATION_FOUR) + iOffset)
		RETURN 3
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC INT GET_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SPAWN_GEAR_GROUP(INT iInteractable)
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Group_1)
		RETURN 0
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Group_2)
		RETURN 1
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Group_3)
		RETURN 2
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Group_4)
		RETURN 3
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC INT GET_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SPAWN_GEAR_INDEX(INT iInteractable)
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_1)
		RETURN 0
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_2)
		RETURN 1
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_3)
		RETURN 2
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Index_4)
		RETURN 3
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC BOOL IS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_FLAGGED_AS_SPAWN_GEAR(INT iInteractable)
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Spawn_Uniform)
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SPAWN_GEAR_GROUPS(INT iInteractable)
	
	IF NOT IS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_FLAGGED_AS_SPAWN_GEAR(iInteractable)
		EXIT
	ENDIF
	
	INT iGroup = GET_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SPAWN_GEAR_GROUP(iInteractable)
	
	IF iGroup = -1
		EXIT
	ENDIF
	
	INT iLocation = GET_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SPAWN_GEAR_POSITION_INDEX(iGroup)
	
	IF iLocation = -1
		
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "][iGroup: ", iGroup, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SPAWN_GEAR_GROUPS - Set up to be a gear that has not spawned (iLocation = -1).")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].eSpawnConditionFlag[0] = SPAWN_CONDITION_FLAG_NEVER_SPAWN		
		
		EXIT
	ENDIF
		
	PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "][iGroup: ", iGroup, "][Location: ", iLocation, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SPAWN_GEAR_GROUPS - Set up to spawn")
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Spawn And Position Gear Groups (Interactable)																							 			-----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_FLAGGED_AS_REPOSITION_GEAR(INT iInteractable)
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_Position_Uniform)
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_REPOSITION_GEAR(INT iInteractable)
	
	IF NOT IS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_FLAGGED_AS_REPOSITION_GEAR(iInteractable)
		EXIT
	ENDIF
	
	INT iGroup = GET_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SPAWN_GEAR_GROUP(iInteractable)
	
	IF iGroup = -1
		EXIT
	ENDIF
	
	INT iLocation = GET_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SPAWN_GEAR_POSITION_INDEX(iGroup)
	
	IF iLocation = -1
		
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "][iGroup: ", iGroup, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_REPOSITION_GEAR - Set up to be a gear that has not spawned (iLocation = -1).")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].eSpawnConditionFlag[0] = SPAWN_CONDITION_FLAG_NEVER_SPAWN		
		
		EXIT
	ENDIF
	
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].vInteractable_Position = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sWarpLocationSettings.vPosition[iLocation]	
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].fInteractable_Heading = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sWarpLocationSettings.fHeading[iLocation]
	
	PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "][iGroup: ", iGroup, "][Location: ", iLocation, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_REPOSITION_GEAR - Set up to reposition to: ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].vInteractable_Position)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Spawn And Position Gear Groups (Weapon)																							 			-----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_FLAGGED_AS_SPAWN_GEAR(INT iWeapon)
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].iAltVarsBitsetWeapon, ciAltVarsBS_IslandHeist_Pickups_Flag_Spawn_BoltCutters)
		PRINTLN("[LM][AltVarsSystem][Weapon: ", iWeapon, "] - IS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_FLAGGED_AS_SPAWN_GEAR - Flagged as Bolt Cutters")	
		RETURN TRUE
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].iAltVarsBitsetWeapon, ciAltVarsBS_IslandHeist_Pickups_Flag_Spawn_RappelGear)
		PRINTLN("[LM][AltVarsSystem][Weapon: ", iWeapon, "] - IS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_FLAGGED_AS_SPAWN_GEAR - Flagged as Rappel Gear")	
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SPAWN_GEAR_GROUP(INT iWeapon)
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].iAltVarsBitsetWeapon, ciAltVarsBS_IslandHeist_Pickups_Flag_Group_1)
		RETURN 0
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].iAltVarsBitsetWeapon, ciAltVarsBS_IslandHeist_Pickups_Flag_Group_2)
		RETURN 1
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].iAltVarsBitsetWeapon, ciAltVarsBS_IslandHeist_Pickups_Flag_Group_3)
		RETURN 2
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].iAltVarsBitsetWeapon, ciAltVarsBS_IslandHeist_Pickups_Flag_Group_4)
		RETURN 3
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC INT GET_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SPAWN_GEAR_POSITION_INDEX(INT iWeapon, INT iGroup)
	
	INT iOffset = (iGroup*4)
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].iAltVarsBitsetWeapon, ciAltVarsBS_IslandHeist_Pickups_Flag_Spawn_BoltCutters)
		IF IS_BIT_SET(g_sHeistIslandConfig.iBoltCutterLocationBitset, ENUM_TO_INT(HIGL_GROUP_ONE_LOCATION_ONE) + iOffset)
			RETURN 0
		ELIF IS_BIT_SET(g_sHeistIslandConfig.iBoltCutterLocationBitset, ENUM_TO_INT(HIGL_GROUP_ONE_LOCATION_TWO) + iOffset)
			RETURN 1
		ELIF IS_BIT_SET(g_sHeistIslandConfig.iBoltCutterLocationBitset, ENUM_TO_INT(HIGL_GROUP_ONE_LOCATION_THREE) + iOffset)
			RETURN 2
		ELIF IS_BIT_SET(g_sHeistIslandConfig.iBoltCutterLocationBitset, ENUM_TO_INT(HIGL_GROUP_ONE_LOCATION_FOUR) + iOffset)
			RETURN 3
		ENDIF
	ENDIF
		
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].iAltVarsBitsetWeapon, ciAltVarsBS_IslandHeist_Pickups_Flag_Spawn_RappelGear)		
		IF IS_BIT_SET(g_sHeistIslandConfig.iGrappelEquipmentLocationBitset, ENUM_TO_INT(HIGL_GROUP_ONE_LOCATION_ONE) + iOffset)
			RETURN 0
		ELIF IS_BIT_SET(g_sHeistIslandConfig.iGrappelEquipmentLocationBitset, ENUM_TO_INT(HIGL_GROUP_ONE_LOCATION_TWO) + iOffset)
			RETURN 1
		ELIF IS_BIT_SET(g_sHeistIslandConfig.iGrappelEquipmentLocationBitset, ENUM_TO_INT(HIGL_GROUP_ONE_LOCATION_THREE) + iOffset)
			RETURN 2
		ELIF IS_BIT_SET(g_sHeistIslandConfig.iGrappelEquipmentLocationBitset, ENUM_TO_INT(HIGL_GROUP_ONE_LOCATION_FOUR) + iOffset)
			RETURN 3
		ENDIF
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC INT GET_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SPAWN_GEAR_INDEX(INT iWeapon)
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].iAltVarsBitsetWeapon, ciAltVarsBS_IslandHeist_Pickups_Flag_Index_1)
		RETURN 0
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].iAltVarsBitsetWeapon, ciAltVarsBS_IslandHeist_Pickups_Flag_Index_2)
		RETURN 1
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].iAltVarsBitsetWeapon, ciAltVarsBS_IslandHeist_Pickups_Flag_Index_3)
		RETURN 2
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].iAltVarsBitsetWeapon, ciAltVarsBS_IslandHeist_Pickups_Flag_Index_4)
		RETURN 3
	ENDIF
	
	RETURN -1
ENDFUNC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SPAWN_GEAR_GROUPS(INT iWeapon)

	IF NOT IS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_FLAGGED_AS_SPAWN_GEAR(iWeapon)
		EXIT
	ENDIF
	
	INT iGroup = GET_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SPAWN_GEAR_GROUP(iWeapon)
	
	IF iGroup = -1
		EXIT
	ENDIF
	
	INT iLocation = GET_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SPAWN_GEAR_POSITION_INDEX(iWeapon, iGroup)
	
	IF iLocation = -1
		
		PRINTLN("[LM][AltVarsSystem][Weapon: ", iWeapon, "][iGroup: ", iGroup, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SPAWN_GEAR_GROUPS - Set up to be a gear that has not spawned (iLocation = -1).")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].eSpawnConditionFlag[0] = SPAWN_CONDITION_FLAG_NEVER_SPAWN		
		
		EXIT
	ENDIF
	
	INT iIndex = GET_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SPAWN_GEAR_INDEX(iWeapon)
	
	IF iLocation != iIndex
		
		PRINTLN("[LM][AltVarsSystem][Weapon: ", iWeapon, "][iGroup: ", iGroup, "][iIndex: ", iIndex, "][Location: ", iLocation, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SPAWN_GEAR_GROUPS - Set up to be a gear that has not spawned. (INDEX != LOCATION)")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].eSpawnConditionFlag[0] = SPAWN_CONDITION_FLAG_NEVER_SPAWN
		
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Weapon: ", iWeapon, "][iGroup: ", iGroup, "][iIndex: ", iIndex, "][Location: ", iLocation, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SPAWN_GEAR_GROUPS - Set up to spawn")
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Scoped Gear																														 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL HAS_MC_ALT_VAR_ISLAND_HEIST_GRAPPEL_BEEN_SCOPED(INT iGroup)
	RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_GrappelEquipmentScoped_1 + iGroup)
ENDFUNC
FUNC BOOL HAS_MC_ALT_VAR_ISLAND_HEIST_BOLT_CUTTERS_BEEN_SCOPED(INT iGroup)
	RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_BoltCutterScoped_1 + iGroup)
ENDFUNC
FUNC BOOL HAS_MC_ALT_VAR_ISLAND_HEIST_UNIFORM_BEEN_SCOPED(INT iGroup)		
	RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_UniformScoped_1 + iGroup)
ENDFUNC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SCOPED_GRAPPEL_EQUIPMENT(INT iWeapon)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].iAltVarsBitsetWeapon, ciAltVarsBS_IslandHeist_Pickups_Flag_Spawn_RappelGear)		
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Weapon: ", iWeapon, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SCOPED_GRAPPEL_EQUIPMENT - Flagged as Rappel Gear")
	
	INT iGroup = GET_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SPAWN_GEAR_GROUP(iWeapon)
	
	IF iGroup = -1
		PRINTLN("[LM][AltVarsSystem][Weapon: ", iWeapon, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SCOPED_GRAPPEL_EQUIPMENT - iGroup: ", iGroup, " No valid group")
		EXIT
	ENDIF
		
	IF NOT HAS_MC_ALT_VAR_ISLAND_HEIST_GRAPPEL_BEEN_SCOPED(iGroup)
		EXIT
	ENDIF
	
	INT iMod = g_FMMC_STRUCT.sPoolVars.iInt[ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Weapon_BlipRange_ScopedGrappelEquipment]
	
	IF iMod = -1
		PRINTLN("[LM][AltVarsSystem][Weapon: ", iWeapon, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SCOPED_GRAPPEL_EQUIPMENT - iGroup: ", iGroup, " Not been scoped")	
		EXIT
	ENDIF
		
	PRINTLN("[LM][AltVarsSystem][Weapon: ", iWeapon, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SCOPED_GRAPPEL_EQUIPMENT - Using Pool Var Index: ", ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Weapon_BlipRange_ScopedGrappelEquipment, " Blip Range Set to: ", iMod, " and ciBLIP_INFO_Always_Blip_On_Pause_Map set")

	g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].sWeaponBlipStruct.iBlipShowZonesBS = 0
	g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].sWeaponBlipStruct.iBlipHideZonesBS = 0	
	g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].sWeaponBlipStruct.fBlipRange = TO_FLOAT(iMod)
	SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].sWeaponBlipStruct.iBlipBitset, ciBLIP_INFO_Always_Blip_On_Pause_Map)
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SCOPED_BOLT_CUTTERS(INT iWeapon)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].iAltVarsBitsetWeapon, ciAltVarsBS_IslandHeist_Pickups_Flag_Spawn_BoltCutters)		
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Weapon: ", iWeapon, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SCOPED_BOLT_CUTTERS - Flagged as Bolt Cutters")
	
	INT iGroup = GET_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SPAWN_GEAR_GROUP(iWeapon)
	
	IF iGroup = -1
		PRINTLN("[LM][AltVarsSystem][Weapon: ", iWeapon, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SCOPED_BOLT_CUTTERS - iGroup: ", iGroup, " No valid group")	
		EXIT
	ENDIF
	
	IF NOT HAS_MC_ALT_VAR_ISLAND_HEIST_BOLT_CUTTERS_BEEN_SCOPED(iGroup)			
		PRINTLN("[LM][AltVarsSystem][Weapon: ", iWeapon, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SCOPED_BOLT_CUTTERS - iGroup: ", iGroup, " Not been scoped")	
		EXIT
	ENDIF
	
	INT iMod = g_FMMC_STRUCT.sPoolVars.iInt[ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Weapon_BlipRange_ScopedBoltCutters]
		
	PRINTLN("[LM][AltVarsSystem][Weapon: ", iWeapon, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SCOPED_BOLT_CUTTERS - Using Pool Var Index: ", ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Weapon_BlipRange_ScopedBoltCutters, " Blip Range Set to: ", iMod, " and ciBLIP_INFO_Always_Blip_On_Pause_Map set")
	
	g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].sWeaponBlipStruct.iBlipShowZonesBS = 0
	g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].sWeaponBlipStruct.iBlipHideZonesBS = 0	
	g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].sWeaponBlipStruct.fBlipRange = TO_FLOAT(iMod)
	SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].sWeaponBlipStruct.iBlipBitset, ciBLIP_INFO_Always_Blip_On_Pause_Map)

ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SCOPED_UNIFORM(INT iInteractable)
	
	IF NOT IS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_FLAGGED_AS_SPAWN_GEAR(iInteractable)
		EXIT
	ENDIF
	
	INT iGroup = GET_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SPAWN_GEAR_GROUP(iInteractable)
	
	IF iGroup = -1
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - GET_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SPAWN_GEAR_GROUP - iGroup: ", iGroup, " No valid group")
		EXIT
	ENDIF
	
	IF NOT HAS_MC_ALT_VAR_ISLAND_HEIST_UNIFORM_BEEN_SCOPED(iGroup)
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SCOPED_UNIFORM - iGroup: ", iGroup, " Not been scoped")	
		EXIT
	ENDIF
	
	INT iMod = g_FMMC_STRUCT.sPoolVars.iInt[ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Interactable_BlipRange_ScopedUniform]
	
	PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SCOPED_UNIFORM - Using Pool Var Index: ", ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Interactable_BlipRange_ScopedUniform, " Blip Range Set to: ", iMod, " and ciBLIP_INFO_Always_Blip_On_Pause_Map set")
	
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipShowZonesBS = 0
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipHideZonesBS = 0	
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.fBlipRange = TO_FLOAT(iMod)
	SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipBitset, ciBLIP_INFO_Always_Blip_On_Pause_Map)

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Approach Entity Position																													 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SMUGGLED_WEAPON_INVENTORY(INT iWeapon)
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].iAltVarsBitsetWeapon, ciAltVarsBS_IslandHeist_Pickups_Flag_FreemodeWeaponInventory)
		EXIT
	ENDIF
				
	INT iIndex = -1
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponLoadout_1)
		iIndex = 0
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponLoadout_2)
		iIndex = 1
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponLoadout_3)
		iIndex = 2
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponLoadout_4)
		iIndex = 3
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponLoadout_5)
		iIndex = 4
	ENDIF
	
	IF iIndex = -1		
		PRINTLN("[LM][AltVarsSystem][Weapon: ", iWeapon, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SMUGGLED_WEAPON_INVENTORY - No valid lobby/freemode selection. Exitting.")	
		EXIT
	ENDIF
	
	INT iTeam = 0
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		
		PRINTLN("[LM][AltVarsSystem][Weapon: ", iWeapon, "][Team: ", iTeam, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SMUGGLED_WEAPON_INVENTORY - Forcing iMidMissionInventoryIndex to: ", iIndex)
	
		g_FMMC_STRUCT.sTeamData[iTeam].iMidMissionInventoryIndex = iIndex
		
	ENDFOR
	
	g_FMMC_STRUCT.sPedAmmoDrop.iAmmoPickupType = GET_AMMO_TYPE_FOR_PED_AMMO_DROPS(iIndex)
	PRINTLN("[LM][AltVarsSystem][Weapon: ", iWeapon, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SMUGGLED_WEAPON_INVENTORY - Forced sPedAmmoDrop.iAmmoPickupType to ", g_FMMC_STRUCT.sPedAmmoDrop.iAmmoPickupType)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Approach Entity Position																													 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_MC_ALT_VAR_ISLAND_HEIST_APPROACH_TAKEN_INDEX()
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SubmarineEastDocks)
		RETURN 0
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SubmarineWestDocks)
		RETURN 1
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_StealthPlaneHALOJump)
		RETURN 2
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_StealthChopperNorthDropZone)
		RETURN 3
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_StealthChopperSouthDropZone)
		RETURN 4
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SmugglerPlaneAirstrip)
		RETURN 5
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SmugglerBoatMainDocks)
	OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_GunBoatMainDocks)
		RETURN 6
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_GunBoatSmallDocks)
	OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SmugglerBoaSmallDocks)
		RETURN 7
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_DrainageTunnel)
		RETURN 8
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SubmarineWestBeach)
		RETURN 9	
	ENDIF
	
	RETURN -1
ENDFUNC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_POSITION_FORCED_BY_APPROACH(INT iPed)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAltVarsBitsetPed, ciAltVarsBS_IslandHeist_Peds_Flag_PositionBasedOnApproach)
		EXIT
	ENDIF
	
	INT iIndex = GET_MC_ALT_VAR_ISLAND_HEIST_APPROACH_TAKEN_INDEX()
	
	IF iIndex = -1
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_POSITION_FORCED_BY_APPROACH - Setting new position to: ", iIndex)
	
	g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vPos = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sWarpLocationSettings.vPosition[iIndex]
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEH_POSITION_FORCED_BY_APPROACH(INT iVeh)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAltVarsBitsetVeh, ciAltVarsBS_IslandHeist_Vehicles_Flag_PositionBasedOnApproach)
		EXIT
	ENDIF
	
	INT iIndex = GET_MC_ALT_VAR_ISLAND_HEIST_APPROACH_TAKEN_INDEX()
	
	IF iIndex = -1
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEH_POSITION_FORCED_BY_APPROACH - Setting new position to: ", iIndex)
	
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vPos = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings.vPosition[iIndex]
	
ENDPROC
	
PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_OBJ_POSITION_FORCED_BY_APPROACH(INT iObj)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAltVarsBitsetObj, ciAltVarsBS_IslandHeist_Objects_PositionBasedOnApproach)
		EXIT
	ENDIF
	
	INT iIndex = GET_MC_ALT_VAR_ISLAND_HEIST_APPROACH_TAKEN_INDEX()
	
	IF iIndex = -1
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Object: ", iObj, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_OBJ_POSITION_FORCED_BY_APPROACH - Setting new position to: ", iIndex)
	
	g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vPos = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sWarpLocationSettings.vPosition[iIndex]
	
ENDPROC
	
PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_POSITION_FORCED_BY_APPROACH(INT iInteractable)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_PositionBasedOnApproach)
		EXIT
	ENDIF
	
	INT iIndex = GET_MC_ALT_VAR_ISLAND_HEIST_APPROACH_TAKEN_INDEX()
	
	IF iIndex = -1
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_POSITION_FORCED_BY_APPROACH - Setting new position to: ", iIndex)
	
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].vInteractable_Position = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sWarpLocationSettings.vPosition[iIndex]
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Compound Entity Position																															 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_MC_ALT_VAR_ISLAND_HEIST_COMPOUND_ENTRANCE_TAKEN_INDEX()
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceExplosivesFrontGate)
		RETURN 0
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceKeycardSouth)
		RETURN 1
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceKeycardNorth)
		RETURN 2
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceRappelSouth)
		RETURN 3
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceRappelNorth)
		RETURN 4
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceVehicleFrontGate)
		RETURN 5
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceSewerGrate)
		RETURN 6
	ENDIF
	
	RETURN -1
ENDFUNC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_POSITION_FORCED_BY_COMPOUND_ENTRANCE_TAKEN(INT iPed)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAltVarsBitsetPed, ciAltVarsBS_IslandHeist_Peds_Flag_PositionBasedOnCompoundEntrance)
		EXIT
	ENDIF
	
	INT iIndex = GET_MC_ALT_VAR_ISLAND_HEIST_COMPOUND_ENTRANCE_TAKEN_INDEX()
	
	IF iIndex = -1
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_POSITION_FORCED_BY_COMPOUND_ENTRANCE_TAKEN - Setting new position to: ", iIndex)
	
	g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vPos = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sWarpLocationSettings.vPosition[iIndex]
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEH_POSITION_FORCED_BY_COMPOUND_ENTRANCE_TAKEN(INT iVeh)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAltVarsBitsetVeh, ciAltVarsBS_IslandHeist_Vehicles_Flag_PositionBasedOnCompoundEntrance)
		EXIT
	ENDIF
	
	INT iIndex = GET_MC_ALT_VAR_ISLAND_HEIST_COMPOUND_ENTRANCE_TAKEN_INDEX()
	
	IF iIndex = -1
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEH_POSITION_FORCED_BY_COMPOUND_ENTRANCE_TAKEN - Setting new position to: ", iIndex)
	
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vPos = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings.vPosition[iIndex]
	
ENDPROC
	
PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_OBJ_POSITION_FORCED_BY_COMPOUND_ENTRANCE_TAKEN(INT iObj)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAltVarsBitsetObj, ciAltVarsBS_IslandHeist_Objects_PositionBasedOnCompoundEntrance)
		EXIT
	ENDIF
	
	INT iIndex = GET_MC_ALT_VAR_ISLAND_HEIST_COMPOUND_ENTRANCE_TAKEN_INDEX()
	
	IF iIndex = -1
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Object: ", iObj, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_OBJ_POSITION_FORCED_BY_COMPOUND_ENTRANCE_TAKEN - Setting new position to: ", iIndex)
	
	g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vPos = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sWarpLocationSettings.vPosition[iIndex]
	
ENDPROC
	
PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_POSITION_FORCED_BY_COMPOUND_ENTRANCE_TAKEN(INT iInteractable)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_PositionBasedOnCompoundEntrance)
		EXIT
	ENDIF
	
	INT iIndex = GET_MC_ALT_VAR_ISLAND_HEIST_COMPOUND_ENTRANCE_TAKEN_INDEX()
	
	IF iIndex = -1
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_POSITION_FORCED_BY_COMPOUND_ENTRANCE_TAKEN - Setting new position to: ", iIndex)
	
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].vInteractable_Position = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sWarpLocationSettings.vPosition[iIndex]
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicle Blip On Exit Chosen																														 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_MC_ALT_VAR_ISLAND_HEIST_VEH_FLAGGED_TO_BLIP_ON_EXIT_CHOSEN(INT iVeh)
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAltVarsBitsetVeh, ciAltVarsBS_IslandHeist_Vehicles_Flag_BlipOnExitChosen_MainDock)
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteMainDocks)
			RETURN TRUE
		ELSE
			PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEH_BLIP_ON_EXIT_CHOSEN - Flagged to be blipped for Main Docks but that exit was not chosen.")
		ENDIF
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAltVarsBitsetVeh, ciAltVarsBS_IslandHeist_Vehicles_Flag_BlipOnExitChosen_NorthDock)
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteSmallDocks)
			RETURN TRUE
		ELSE
			PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEH_BLIP_ON_EXIT_CHOSEN - Flagged to be blipped for North Docks but that exit was not chosen.")
		ENDIF
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAltVarsBitsetVeh, ciAltVarsBS_IslandHeist_Vehicles_Flag_BlipOnExitChosen_Airstrip)
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteAirstrip)
			RETURN TRUE
		ELSE
			PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEH_BLIP_ON_EXIT_CHOSEN - Flagged to be blipped for Airstrip but that exit was not chosen.")
		ENDIF
	ENDIF
		
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAltVarsBitsetVeh, ciAltVarsBS_IslandHeist_Vehicles_Flag_BlipOnExitChosen_Submarine)
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteSubmarine)
			RETURN TRUE
		ELSE
			PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEH_BLIP_ON_EXIT_CHOSEN - Flagged to be blipped for Submarine but that exit was not chosen.")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEH_BLIP_ON_EXIT_CHOSEN(INT iVeh)
	
	IF NOT IS_MC_ALT_VAR_ISLAND_HEIST_VEH_FLAGGED_TO_BLIP_ON_EXIT_CHOSEN(iVeh)
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEH_BLIP_ON_EXIT_CHOSEN - Flagged to be blipped for if exit was chosen, and it was. Modifying blip.")
	
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.iBlipShowZonesBS = 0
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.iBlipHideZonesBS = 0	
	SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.iBlipBitset, ciBLIP_INFO_Always_Blip_On_Pause_Map)	
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.sTeamBlip[0].iBlipOverrideBitSet = -1
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicle Model Swap																														 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEH_MODEL_SWAP(INT iVeh)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAltVarsBitsetVeh, ciAltVarsBS_IslandHeist_Vehicles_Flag_ModelSwapOnApproach)
		EXIT
	ENDIF
	
	INT iIndex = -1
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_GunBoatMainDocks)
	OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_GunBoatSmallDocks)	
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Vehicle_Model_Swap_1
	ENDIF
	
	IF iIndex = -1
		PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEH_MODEL_SWAP - Approach not Gunboat.")
		EXIT
	ENDIF
	
	MODEL_NAMES eMod = INT_TO_ENUM(MODEL_NAMES, g_FMMC_STRUCT.sPoolVars.iInt[iIndex])
	
	IF eMod = DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEH_MODEL_SWAP - Poolvar not set! Exitting")	
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEH_MODEL_SWAP - Overriding Model to: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(eMod))
	
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = eMod
	
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Spawn Block Flags																													 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_MC_ALT_VAR_ISLAND_HEIST_SPAWN_BLOCKING_FLAG_SET(SPAWN_CONDITION_FLAG eSpawnConditionFlag)
	
	SWITCH eSpawnConditionFlag
		CASE SPAWN_CONDITION_FLAG_COMPOUND_GATE_EXPLOSIVES
		CASE SPAWN_CONDITION_FLAG_COMPOUND_KEYCARD_SOUTH
		CASE SPAWN_CONDITION_FLAG_COMPOUND_KEYCARD_NORTH
		CASE SPAWN_CONDITION_FLAG_COMPOUND_RAPPEL_SOUTH
		CASE SPAWN_CONDITION_FLAG_COMPOUND_RAPPEL_NORTH
		CASE SPAWN_CONDITION_FLAG_COMPOUND_VEHICLE_FRONT_GATE
		CASE SPAWN_CONDITION_FLAG_COMPOUND_SEWER_GRATE
		CASE SPAWN_CONDITION_FLAG_ESCAPE_ROUTE_AIRSTRIP		
		CASE SPAWN_CONDITION_FLAG_ESCAPE_ROUTE_MAINDOCKS		
		CASE SPAWN_CONDITION_FLAG_ESCAPE_ROUTE_SMALLDOCKS	
		CASE SPAWN_CONDITION_FLAG_ESCAPE_ROUTE_SUBMARINE	
		CASE SPAWN_CONDITION_FLAG_PLAYER_HAS_ACETYLENE_TORCH
		CASE SPAWN_CONDITION_FLAG_BOLT_CUTTERS_SCOPED
		CASE SPAWN_CONDITION_FLAG_BOLT_CUTTERS_UNSCOPED
		CASE SPAWN_CONDITION_FLAG_RAPPEL_SCOPED
		CASE SPAWN_CONDITION_FLAG_RAPPEL_UNSCOPED
		CASE SPAWN_CONDITION_FLAG_UNIFORM_SCOPED
		CASE SPAWN_CONDITION_FLAG_UNIFORM_UNSCOPED		
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DOES_MC_ALT_VAR_ISLAND_HEIST_FLAGGED_MATCH_WITH_CHOSEN(SPAWN_CONDITION_FLAG eSpawnConditionFlag)
		
	SWITCH eSpawnConditionFlag
		CASE SPAWN_CONDITION_FLAG_COMPOUND_GATE_EXPLOSIVES
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceExplosivesFrontGate)
		BREAK
		CASE SPAWN_CONDITION_FLAG_COMPOUND_KEYCARD_SOUTH
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceKeycardSouth)
		BREAK
		CASE SPAWN_CONDITION_FLAG_COMPOUND_KEYCARD_NORTH
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceKeycardNorth)
		BREAK
		CASE SPAWN_CONDITION_FLAG_COMPOUND_RAPPEL_SOUTH
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceRappelSouth)
		BREAK
		CASE SPAWN_CONDITION_FLAG_COMPOUND_RAPPEL_NORTH
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceRappelNorth)
		BREAK
		CASE SPAWN_CONDITION_FLAG_COMPOUND_VEHICLE_FRONT_GATE
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceVehicleFrontGate)
		BREAK
		CASE SPAWN_CONDITION_FLAG_COMPOUND_SEWER_GRATE
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceSewerGrate)
		BREAK
		CASE SPAWN_CONDITION_FLAG_ESCAPE_ROUTE_AIRSTRIP	
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteAirstrip)
		BREAK
		CASE SPAWN_CONDITION_FLAG_ESCAPE_ROUTE_MAINDOCKS	
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteMainDocks)
		BREAK
		CASE SPAWN_CONDITION_FLAG_ESCAPE_ROUTE_SMALLDOCKS
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteSmallDocks)
		BREAK
		CASE SPAWN_CONDITION_FLAG_ESCAPE_ROUTE_SUBMARINE	
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteSubmarine)
		BREAK	
		CASE SPAWN_CONDITION_FLAG_PLAYER_HAS_ACETYLENE_TORCH
			RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_HasAcetyleneTorch)
		BREAK
		CASE SPAWN_CONDITION_FLAG_BOLT_CUTTERS_SCOPED
			RETURN (FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_BoltCutterScoped_1)
				OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_BoltCutterScoped_2)
				OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_BoltCutterScoped_3)
				OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_BoltCutterScoped_4))		
		BREAK		
		CASE SPAWN_CONDITION_FLAG_BOLT_CUTTERS_UNSCOPED
			RETURN (NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_BoltCutterScoped_1)
				AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_BoltCutterScoped_2)
				AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_BoltCutterScoped_3)
				AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_BoltCutterScoped_4))
		BREAK
		CASE SPAWN_CONDITION_FLAG_RAPPEL_SCOPED
			RETURN (FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_GrappelEquipmentScoped_1)
				OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_GrappelEquipmentScoped_2)
				OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_GrappelEquipmentScoped_3)
				OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_GrappelEquipmentScoped_4))
		BREAK
		CASE SPAWN_CONDITION_FLAG_RAPPEL_UNSCOPED
			RETURN (NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_GrappelEquipmentScoped_1)
				AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_GrappelEquipmentScoped_2)
				AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_GrappelEquipmentScoped_3)
				AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_GrappelEquipmentScoped_4))
		BREAK
		CASE SPAWN_CONDITION_FLAG_UNIFORM_SCOPED
			RETURN (FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_UniformScoped_1)
				OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_UniformScoped_2)
				OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_UniformScoped_3)
				OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_UniformScoped_4))
		BREAK
		CASE SPAWN_CONDITION_FLAG_UNIFORM_UNSCOPED
			RETURN (NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_UniformScoped_1)
				AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_UniformScoped_2)
				AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_UniformScoped_3)
				AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_UniformScoped_4))
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_SPAWN_BLOCKING_FLAG_CHECKS(INT iPed)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1	
		IF NOT IS_MC_ALT_VAR_ISLAND_HEIST_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_ISLAND_HEIST_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][Ped ", iPed, "][Flag: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].eSpawnConditionFlag[iFlag]), "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET	
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEHICLE_SPAWN_BLOCKING_FLAG_CHECKS(INT iVeh)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1		
		IF NOT IS_MC_ALT_VAR_ISLAND_HEIST_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_ISLAND_HEIST_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][Vehicle ", iVeh, "][iFlag: ", iFlag, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEHICLE_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_OBJECT_SPAWN_BLOCKING_FLAG_CHECKS(INT iObj)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1	
		IF NOT IS_MC_ALT_VAR_ISLAND_HEIST_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_ISLAND_HEIST_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][Object: ", iObj, "][iFlag: ", iFlag, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_OBJECT_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SPAWN_BLOCKING_FLAG_CHECKS(INT iInteractable)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1		
		IF NOT IS_MC_ALT_VAR_ISLAND_HEIST_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_ISLAND_HEIST_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "][iFlag: ", iFlag, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_PROP_SPAWN_BLOCKING_FLAG_CHECKS(INT iProp)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1		
		IF NOT IS_MC_ALT_VAR_ISLAND_HEIST_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_ISLAND_HEIST_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][Prop: ", iProp, "][iFlag: ", iFlag, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_PROP_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_DYNO_PROP_SPAWN_BLOCKING_FLAG_CHECKS(INT iDynoProp)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1		
		IF NOT IS_MC_ALT_VAR_ISLAND_HEIST_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_ISLAND_HEIST_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
			
		PRINTLN("[LM][AltVarsSystem][DynoProp: ", iDynoProp, "][iFlag: ", iFlag, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DYNO_PROP_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_TEAM_SPAWN_POINT_SPAWN_BLOCKING_FLAG_CHECKS(INT iSpawnPoint, INT iTeam)
	INT iFlag 
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1	
		IF NOT IS_MC_ALT_VAR_ISLAND_HEIST_SPAWN_BLOCKING_FLAG_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		IF NOT DOES_MC_ALT_VAR_ISLAND_HEIST_FLAGGED_MATCH_WITH_CHOSEN(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].eSpawnConditionFlag[iFlag])
			RELOOP
		ENDIF
		
		PRINTLN("[LM][AltVarsSystem][SpawnPoint: ", iSpawnPoint, "][iTeam: ", iTeam, "][iFlag: ", iFlag, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_TEAM_SPAWN_POINT_SPAWN_BLOCKING_FLAG_CHECKS - Removing Spawn Blocking Flag")
		
		g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Get Index from Group/Index bitset	(Helper Function)																									 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_INDEX_BASED_ON_GROUP_AND_INDEX(INT iInteractable)
	
	RETURN GET_ISLAND_HEIST_SECONDARY_LOOT_INDEX_FROM_INTERACTABLE(iInteractable)
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Flagged as Secondary Loot																											 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_ISLAND(INT iInteractable)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_AsSecondaryLoot_Island)
		EXIT
	ENDIF
	
	INT iIndex = GET_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_INDEX_BASED_ON_GROUP_AND_INDEX(iInteractable)
	
	IF iIndex = -1
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_ISLAND - iIndex: ", iIndex, " Incorrect setup.")
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].eSpawnConditionFlag[0] = SPAWN_CONDITION_FLAG_NEVER_SPAWN
		EXIT
	ENDIF
		
	IF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iCashIslandLocationsBitSet, iIndex)
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_ISLAND - iIndex: ", iIndex, " Cash Loot Enabled.")
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_CashConsequence = (g_sHeistIslandConfig.sAdditionalLootData.iCashValue / ciLootGrabTotalItems_Base)
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_cash_stack_01a"))
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipSpriteOverride = ENUM_TO_INT(RADAR_TRACE_CASH_PICKUP)
		
	ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iWeedIslandLocationsBitSet, iIndex)
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_ISLAND - iIndex: ", iIndex, " Weed Loot Enabled.")
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_CashConsequence = (g_sHeistIslandConfig.sAdditionalLootData.iWeedValue / ciLootGrabTotalItems_Base)
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_weed_stack_01a"))
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipSpriteOverride = ENUM_TO_INT(RADAR_TRACE_PRODUCTION_WEED)
		
	ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iCokeIslandLocationsBitset, iIndex)
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_ISLAND - iIndex: ", iIndex, " Coke Loot Enabled.")
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_CashConsequence = (g_sHeistIslandConfig.sAdditionalLootData.iCokeValue / ciLootGrabTotalItems_Base)
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_coke_stack_01a"))
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipSpriteOverride = ENUM_TO_INT(RADAR_TRACE_DRUGS_PACKAGE)
		
	ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iGoldIslandLocationsBitSet, iIndex)
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_ISLAND - iIndex: ", iIndex, " Gold Loot Enabled.")		
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_CashConsequence = (g_sHeistIslandConfig.sAdditionalLootData.iGoldValue / ciLootGrabTotalItems_Gold)
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_gold_stack_01a"))
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipSpriteOverride = ENUM_TO_INT(RADAR_TRACE_BAT_HACK_GOLD)
		
	ELSE	
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_ISLAND - No freemode loot associated with iIndex: ", iIndex, " is enabled.")
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].eSpawnConditionFlag[0] = SPAWN_CONDITION_FLAG_NEVER_SPAWN
	ENDIF
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_COMPOUND(INT iInteractable)

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_AsSecondaryLoot_Compound)
		EXIT
	ENDIF
	
	INT iIndex = GET_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_INDEX_BASED_ON_GROUP_AND_INDEX(iInteractable)
	
	IF iIndex = -1
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_COMPOUND - iIndex: ", iIndex, " Incorrect setup.")
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].eSpawnConditionFlag[0] = SPAWN_CONDITION_FLAG_NEVER_SPAWN
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iCashCompoundLocationsBitSet, iIndex)
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_COMPOUND - iIndex: ", iIndex, " Cash Loot Enabled.")
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_CashConsequence = (g_sHeistIslandConfig.sAdditionalLootData.iCashValue / ciLootGrabTotalItems_Base)
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_cash_stack_01a"))
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipSpriteOverride = ENUM_TO_INT(RADAR_TRACE_CASH_PICKUP)
		
	ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iWeedCompoundLocationsBitSet, iIndex)
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_COMPOUND - iIndex: ", iIndex, " Weed Loot Enabled.")
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_CashConsequence = (g_sHeistIslandConfig.sAdditionalLootData.iWeedValue / ciLootGrabTotalItems_Base)
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_weed_stack_01a"))
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipSpriteOverride = ENUM_TO_INT(RADAR_TRACE_PRODUCTION_WEED)
		
	ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iCokeCompoundLocationsBitset, iIndex)
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_COMPOUND - iIndex: ", iIndex, " Coke Loot Enabled.")
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_CashConsequence = (g_sHeistIslandConfig.sAdditionalLootData.iCokeValue / ciLootGrabTotalItems_Base)
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_coke_stack_01a"))
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipSpriteOverride = ENUM_TO_INT(RADAR_TRACE_DRUGS_PACKAGE)
		
	ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iGoldCompoundLocationsBitSet, iIndex)
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_COMPOUND - iIndex: ", iIndex, " Gold Loot Enabled.")		
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_CashConsequence = (g_sHeistIslandConfig.sAdditionalLootData.iGoldValue / ciLootGrabTotalItems_Gold)
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_gold_stack_01a"))
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipSpriteOverride = ENUM_TO_INT(RADAR_TRACE_BAT_HACK_GOLD)
		
	ELSE	
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_COMPOUND - No freemode loot associated with iIndex: ", iIndex, " is enabled.")
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].eSpawnConditionFlag[0] = SPAWN_CONDITION_FLAG_NEVER_SPAWN
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Flagged as Painting																											 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_PAINTING(INT iInteractable)
		
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_AsPainting)
		EXIT
	ENDIF
	
	INT iIndex = GET_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_INDEX_BASED_ON_GROUP_AND_INDEX(iInteractable)
	
	IF iIndex = -1
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_PAINTING - iIndex: ", iIndex, " Incorrect setup.")
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].eSpawnConditionFlag[0] = SPAWN_CONDITION_FLAG_NEVER_SPAWN
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iPaintingsLocationsBitSet, iIndex)
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_PAINTING - iIndex: ", iIndex, " Painting Enabled.")
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_CashConsequence = g_sHeistIslandConfig.sAdditionalLootData.iPaintingsValue
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipSpriteOverride = ENUM_TO_INT(RADAR_TRACE_PAINTING)
		
	ELSE
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_PAINTING - No freemode loot associated with iIndex: ", iIndex, " is enabled.")
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].eSpawnConditionFlag[0] = SPAWN_CONDITION_FLAG_NEVER_SPAWN
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Secondary Loot Scoped		 																													 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_ISLAND_SCOPED(INT iInteractable)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_AsSecondaryLoot_Island)
		EXIT
	ENDIF
	
	INT iIndex = -1
	INT iLootIndex = GET_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_INDEX_BASED_ON_GROUP_AND_INDEX(iInteractable)
	
	IF iLootIndex = -1
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_ISLAND_SCOPED - iLootIndex: ", iLootIndex, " Incorrect setup.")
		EXIT
	ENDIF
		
	IF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iCashIslandLocationsBitSetScoped, iLootIndex)
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_ISLAND_SCOPED - iLootIndex: ", iLootIndex, " Cash Loot Scoped.")
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Interactable_BlipRange_ScopedSecondaryLoot
		
	ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iWeedIslandLocationsBitSetScoped, iLootIndex)
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_ISLAND_SCOPED - iLootIndex: ", iLootIndex, " Weed Loot Scoped.")
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Interactable_BlipRange_ScopedSecondaryLoot
		
	ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iCokeIslandLocationsBitsetScoped, iLootIndex)
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_ISLAND_SCOPED - iLootIndex: ", iLootIndex, " Coke Loot Scoped.")
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Interactable_BlipRange_ScopedSecondaryLoot
		
	ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iGoldIslandLocationsBitSetScoped, iLootIndex)
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_ISLAND_SCOPED - iLootIndex: ", iLootIndex, " Gold Loot Scoped.")		
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Interactable_BlipRange_ScopedSecondaryLoot
		
	ENDIF
	
	IF iIndex = -1
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_ISLAND_SCOPED - iLootIndex: ", iLootIndex, " Not Scoped")		
		EXIT
	ENDIF
	
	INT iMod = g_FMMC_STRUCT.sPoolVars.iInt[iIndex]
	
	PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_ISLAND_SCOPED - iLootIndex: ", iLootIndex, " having Blip Range overriden to: ", iMod, " and ciBLIP_INFO_Always_Blip_On_Pause_Map set")
	
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipShowZonesBS = 0
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipHideZonesBS = 0	
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.fBlipRange = TO_FLOAT(iMod)
	SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipBitset, ciBLIP_INFO_Always_Blip_On_Pause_Map)
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_COMPOUND_SCOPED(INT iInteractable)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_AsSecondaryLoot_Compound)
		EXIT
	ENDIF
	
	INT iIndex = -1
	INT iLootIndex = GET_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_INDEX_BASED_ON_GROUP_AND_INDEX(iInteractable)
	
	IF iLootIndex = -1
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_COMPOUND_SCOPED - iLootIndex: ", iLootIndex, " Incorrect setup.")		
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iCashCompoundLocationsBitSetScoped, iLootIndex)
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_COMPOUND_SCOPED - iLootIndex: ", iLootIndex, " Cash Loot Scoped.")
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Interactable_BlipRange_ScopedSecondaryLoot
		
	ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iWeedCompoundLocationsBitSetScoped, iLootIndex)
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_COMPOUND_SCOPED - iLootIndex: ", iLootIndex, " Weed Loot Scoped.")
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Interactable_BlipRange_ScopedSecondaryLoot
		
	ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iCokeCompoundLocationsBitsetScoped, iLootIndex)
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_COMPOUND_SCOPED - iLootIndex: ", iLootIndex, " Coke Loot Scoped.")
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Interactable_BlipRange_ScopedSecondaryLoot
		
	ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iGoldCompoundLocationsBitSetScoped, iLootIndex)
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_COMPOUND_SCOPED - iLootIndex: ", iLootIndex, " Gold Loot Scoped.")		
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Interactable_BlipRange_ScopedSecondaryLoot
		
	ENDIF
	
	IF iIndex = -1
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_COMPOUND_SCOPED - iLootIndex: ", iLootIndex, " Not Scoped")	
		EXIT
	ENDIF
	
	INT iMod = g_FMMC_STRUCT.sPoolVars.iInt[iIndex]
	
	PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_COMPOUND_SCOPED - iLootIndex: ", iLootIndex, " having Blip Range overriden to: ", iMod, " and ciBLIP_INFO_Always_Blip_On_Pause_Map set")
	
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipShowZonesBS = 0
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipHideZonesBS = 0	
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.fBlipRange = TO_FLOAT(iMod)
	SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipBitset, ciBLIP_INFO_Always_Blip_On_Pause_Map)
	
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Painting Scoped		 																													 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_PAINTING_SCOPED(INT iInteractable)

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_AsPainting)
		EXIT
	ENDIF
	
	INT iPaintingIndex = GET_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_INDEX_BASED_ON_GROUP_AND_INDEX(iInteractable)	
	
	IF iPaintingIndex = -1
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_PAINTING_SCOPED - iPaintingIndex: ", iPaintingIndex, " Incorrect setup.")		
		EXIT
	ENDIF
	
	INT iIndex = -1	
	
	IF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iPaintingsLocationsBitSetScoped, iPaintingIndex)
		iIndex = ciARRAY_INT_ALT_VAR_ISLAND_HEIST_Interactable_BlipRange_ScopedPainting
	ENDIF
	
	IF iIndex = -1
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_PAINTING_SCOPED - iIndex: ", iIndex, " No Freemode Data.")		
		EXIT
	ENDIF
	
	INT iMod = g_FMMC_STRUCT.sPoolVars.iInt[iIndex]
	
	PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_PAINTING_SCOPED - iPaintingIndex: ", iPaintingIndex, " having Blip Range overriden to: ", iMod, " and ciBLIP_INFO_Always_Blip_On_Pause_Map set")
	
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipShowZonesBS = 0
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipHideZonesBS = 0	
	g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.fBlipRange = TO_FLOAT(iMod)
	SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sInteractableBlipStruct.iBlipBitset, ciBLIP_INFO_Always_Blip_On_Pause_Map)
	
ENDPROC



// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Acetylene Torch		 																													 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_ACETYLENE_TORCH(INT iInteractable)

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iAltVarsBitsetInteractable, ciAltVarsBS_IslandHeist_Interactables_Flag_RequiresAcetyleneTorch)
		EXIT
	ENDIF
	
	IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_HasAcetyleneTorch)
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_InteractionType = ciInteractableInteraction_MultiSolutionLock
	AND GET_INTERACTABLE_MULTI_SOLUTION_LOCK_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable]) = ciInteractable_MultiSolutionLockType__ChainLock
		PRINTLN("[LM][AltVarsSystem][Interactable: ", iInteractable, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_ACETYLENE_TORCH - We have the Acetylene Torch, so we're now clearing the PreReq for this chain lock Interactable's Solution B (Using the torch to melt the chain)")
		g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_SolutionBPreReq = ciPREREQ_None
	ENDIF
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_ACETYLENE_TORCH_LOCATION(INT iLoc)

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iAltVarsBitsetLocation, ciAltVarsBS_IslandHeist_Locations_Flag_RequiresAcetyleneTorch)
		EXIT
	ENDIF

	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_HasAcetyleneTorch)
		EXIT
	ENDIF

	PRINTLN("[LM][AltVarsSystem][Loc: ", iLoc, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_ACETYLENE_TORCH_LOCATION - We do not have the Acetylene Torch, setting location as an invalid priority")
	g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iPriority[0] = FMMC_PRIORITY_IGNORE
	g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vLoc[0] = <<0.0, 0.0, 0.0>> 
	
ENDPROC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_ACETYLENE_TORCH_OBJECT(INT iObj)

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAltVarsBitsetObj, ciAltVarsBS_IslandHeist_Objects_Flag_RequiresAcetyleneTorch)
		EXIT
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_HasAcetyleneTorch)
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Obj: ", iObj, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_ACETYLENE_TORCH_OBJECT - We do not have the Acetylene Torch, setting Object as an invalid priority")	
	g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPriority[0] = FMMC_PRIORITY_IGNORE
	
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Demolition Charges	 																													 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_DEMOLITION_CHARGES_LOCATION(INT iLoc)

	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iAltVarsBitsetLocation, ciAltVarsBS_IslandHeist_Locations_Flag_RequiresDemolitionCharges)
		EXIT
	ENDIF

	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_DemolitionCharges)
		EXIT
	ENDIF

	PRINTLN("[LM][AltVarsSystem][Loc: ", iLoc, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DEMOLITION_CHARGES_LOCATION - We do not have the Demolition Charges, setting location as an invalid priority")
	g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iPriority[0] = FMMC_PRIORITY_IGNORE
	g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iRule[0] = FMMC_OBJECTIVE_LOGIC_NONE
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_DisplayMarkerAsVisualOnly)
		PRINTLN("[LM][AltVarsSystem][Loc: ", iLoc, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DEMOLITION_CHARGES_LOCATION - Clearing ciLoc_BS2_DisplayMarkerAsVisualOnly")
		CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_DisplayMarkerAsVisualOnly)
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Dialogue Trigger alt Var Helpers	 																													 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_INDEX_FROM_GROUP_AND_INDEX(INT iDialogue)
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Group_1)
		IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_1)
			RETURN 0
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_2)
			RETURN 1
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_3)
			RETURN 2
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_4)
			RETURN 3
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_5)
			RETURN 4
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_6)
			RETURN 5
		ENDIF
	
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Group_2)
		IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_1)
			RETURN 6
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_2)
			RETURN 7
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_3)
			RETURN 8
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_4)
			RETURN 9
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_5)
			RETURN 10
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_6)
			RETURN 11
		ENDIF
		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Group_3)
		IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_1)
			RETURN 12
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_2)
			RETURN 13
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_3)
			RETURN 14
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_4)
			RETURN 15
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_5)
			RETURN 16
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_6)
			RETURN 17
		ENDIF
		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Group_4)
		IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_1)
			RETURN 18
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_2)
			RETURN 19
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_3)
			RETURN 20
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_4)
			RETURN 21
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_5)
			RETURN 22
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_6)
			RETURN 23
		ENDIF
		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Group_5)
		IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_1)
			RETURN 24
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_2)
			RETURN 25
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_3)
			RETURN 26
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_4)
			RETURN 27
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_5)
			RETURN 28
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_6)
			RETURN 29
		ENDIF
		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Group_6)
		IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_1)
			RETURN 30
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_2)
			RETURN 31
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_3)
			RETURN 32
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_4)
			RETURN 33
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_5)
			RETURN 34
		ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_6)
			RETURN 35
		ENDIF
		
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC INT GET_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_GET_GROUP(INT iDialogue)
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Group_1)
		RETURN 0	
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Group_2)
		RETURN 1		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Group_3)
		RETURN 2		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Group_4)
		RETURN 3		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Group_5)
		RETURN 4		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Group_6)
		RETURN 5		
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC INT GET_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_GET_INDEX(INT iDialogue)
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_1)
		RETURN 0	
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_2)
		RETURN 1		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_3)
		RETURN 2		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_4)
		RETURN 3		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_5)
		RETURN 4		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Index_6)
		RETURN 5		
	ENDIF
	
	RETURN -1
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Dialogue Trigger Append if Scoped	 																													 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT MC_ALT_VAR_ISLAND_HEIST_GET_DIALOGUE_TRIGEGR_APPEND_VALUE_FOR_TARGET_TYPE()
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_MadrazoFiles)
		RETURN 1
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_Bonds)
		RETURN 4
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_Tequila)
		RETURN 2
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_PearlNecklace)
		RETURN 3
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_SapphirePantherStatue)
		RETURN 5
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_PinkDiamond)	
		RETURN 6
	ENDIF
	
	RETURN -1	
ENDFUNC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_APPEND_FOR_TARGET_TYPE(INT iDialogue)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_TargetTypeDeterminesCustomAppend)
		EXIT
	ENDIF
	
	INT iMod = MC_ALT_VAR_ISLAND_HEIST_GET_DIALOGUE_TRIGEGR_APPEND_VALUE_FOR_TARGET_TYPE()
	
	IF iMod = -1
		PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_APPEND_FOR_TARGET_TYPE - No valid target type iMod: ", iMod)
		EXIT
	ENDIF
	
	TEXT_LABEL_3 tl3 = ""
	tl3 += iMod
	
	g_FMMC_STRUCT.sDialogueTriggers[iDialogue].tlAppendCustomCharacter = tl3
	FMMC_SET_LONG_BIT(iLocalDialogueForceCustomAppend, iDialogue)
		
	PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_APPEND_FOR_TARGET_TYPE - Target Type Assigned Custom Append to: ", tl3, " iMod: ", iMod)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Dialogue Trigger Append if Scoped	 																													 -----------------------
// ##### Description: Index could be 0, 1, 2, 3 depending on, for example, a freemode option that was selected. Some are based on simple Yes/No checks.			 -----------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_FLAGGED_TO_APPEND_IF_SCOPED(INT iDialogue)
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_GrappleScoped)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_VehicleScoped)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_BoltCuttersScoped)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_UniformScoped)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_ControlTowerScoped)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_PowerStationScoped)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_SideGateNorthScoped)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_SideGateSouthScoped)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_RappelPointNorthScoped)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_RappelPointSouthScoped)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_UnderwaterTunnelScoped)		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_APPEND_IF_SCOPED(INT iDialogue)
	
	INT iGroup = GET_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_GET_GROUP(iDialogue)
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_GrappleScoped)
		RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_GrappelEquipmentScoped_1 + iGroup)
	
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_VehicleScoped)
		RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_TrojanVehicleScoped)
	
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_BoltCuttersScoped)
		RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_BoltCutterScoped_1 + iGroup)	
	
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_UniformScoped)
		RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_UniformScoped_1 + iGroup)	
	
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_ControlTowerScoped)
		RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ControlTowerScoped)		
	
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_PowerStationScoped)
		RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_PowerStationScoped)
	
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_SideGateNorthScoped)
		RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_SideGateNorth)
	
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_SideGateSouthScoped)
		RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_SideGateSouth)
	
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_RappelPointNorthScoped)
		RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_RappelNorth)
	
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_RappelPointSouthScoped)
		RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_RappelSouth)
	
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_AppendCustomChar_UnderwaterTunnelScoped)
		RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_Tunnel)
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_APPEND_IF_SCOPED - IndexLong(entity): ", GET_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_INDEX_FROM_GROUP_AND_INDEX(iDialogue), " Group: ", iGroup, " iIndex: ", GET_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_GET_INDEX(iDialogue))
	#ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_APPEND_IF_SCOPED(INT iDialogue)
	
	IF NOT IS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_FLAGGED_TO_APPEND_IF_SCOPED(iDialogue)
		EXIT
	ENDIF
	
	IF NOT SHOULD_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_APPEND_IF_SCOPED(iDialogue)
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_APPEND_IF_SCOPED - Flagged as Scoped. Appending Custom Character to Dialogue.")	
	
	FMMC_SET_LONG_BIT(iLocalDialogueForceCustomAppend, iDialogue)
	
ENDPROC

FUNC BOOL IS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_FLAGGED_TO_BE_BLOCKED(INT iDialogue)
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_PoisonedWater)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Scoped_CCTV)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_NotScoped_CCTV)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_PurchasedAbility_CarpetBomb)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_NotPurchasedAbility_CarpetBomb)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_PurchasedAbility_HeavyLoadout)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_NotPurchasedAbility_HeavyLoadout)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_PurchasedAbility_Sniper)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_NotPurchasedAbility_Sniper)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_PurchasedAbility_ReconDrone)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_NotPurchasedAbility_ReconDrone)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_PurchasedAbility_BackupHeli)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_NotPurchasedAbility_BackupHeli)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Prep_DisruptionBackupResponse)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Prep_NotDisruptionBackupResponse)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_RequiresMultipleCompoundExits)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_RequiresOnlyMainGateCompoundExit)
	OR FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_UnscopedEntranceOnly)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL WAS_ENTRANCE_USED_SCOPED()

	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceExplosivesFrontGate)
		RETURN TRUE
	
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceKeycardSouth)
		RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_SideGateSouth)
	
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceKeycardNorth)
		RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_SideGateNorth)
	
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceRappelSouth)
		RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_RappelSouth)
	
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceRappelNorth)
		RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_RappelNorth)
	
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceVehicleFrontGate)
		RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_MainGate)
	
	ELIF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceSewerGrate)
		RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_Tunnel)
		
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_BE_BLOCKED(INT iDialogue)
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_PoisonedWater)
	AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_PoisonedWaterSupply)	
		PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING - Poisoned Water Supply - returning TRUE")	
		RETURN TRUE
		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Scoped_CCTV)
		// Pending freemode data.		
		PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING - Scoped CCTV - returning TRUE")	
		RETURN TRUE
		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_NotScoped_CCTV)
		// Pending freemode data.	
		PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING - Not Scoped CCTV - returning TRUE")	
		RETURN TRUE
		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_PurchasedAbility_CarpetBomb)
	AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CarpetBomb)
		PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING - Carpet Bomb - returning TRUE")	
		RETURN TRUE
		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_NotPurchasedAbility_CarpetBomb)
	AND FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CarpetBomb)
		PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING - Not Carpet Bomb - returning TRUE")	
		RETURN TRUE		
		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_PurchasedAbility_HeavyLoadout)
	AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_AbilitiesHeavyLoadout)
		PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING - Heavy Loadout - returning TRUE")	
		RETURN TRUE
		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_NotPurchasedAbility_HeavyLoadout)
	AND FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_AbilitiesHeavyLoadout)
		PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING - Not Heavy Loadout - returning TRUE")	
		RETURN TRUE
				
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_PurchasedAbility_Sniper)
	AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_AbilitiesSniper)
		PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING - Purchased Sniper - returning TRUE")	
		RETURN TRUE

	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_NotPurchasedAbility_Sniper)
	AND FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_AbilitiesSniper)	
		PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING - Not Purchased Sniper - returning TRUE")	
		RETURN TRUE		
		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_PurchasedAbility_ReconDrone)
	AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_AbilitiesReconnaissanceDrone)
		PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING - Purchased Recon Drone - returning TRUE")
		RETURN TRUE		
		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_NotPurchasedAbility_ReconDrone)
	AND FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_AbilitiesReconnaissanceDrone)
		PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING - Not Purchased Recon Drone - returning TRUE")	
		RETURN TRUE
		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_PurchasedAbility_BackupHeli)
	AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_AbilitiesHeliBackup)
		PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING - Purchased Heli Backup - returning TRUE")	
		RETURN TRUE
		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_NotPurchasedAbility_BackupHeli)	
	AND FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_AbilitiesHeliBackup)
		PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING - Not Purchased Heli Backup - returning TRUE")
		RETURN TRUE		

	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Prep_DisruptionBackupResponse)		
	AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemiesHelicoptorDisruption)
		PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING - Heli Disruption - returning TRUE")	
		RETURN TRUE
		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_Prep_NotDisruptionBackupResponse)	
	AND FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemiesHelicoptorDisruption)
		PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING - Not Heli Disruption - returning TRUE")	
		RETURN TRUE	
		
	ELIF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_UnscopedEntranceOnly)
	AND WAS_ENTRANCE_USED_SCOPED()
		PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING - Scoped Entrance - returning TRUE")		
		RETURN TRUE
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_RequiresMultipleCompoundExits)			
		INT iExits = 0		
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitFrontGate)
			iExits++
		ENDIF
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitKeycardSouth)
			iExits++
		ENDIF
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitKeycardNorth)
			iExits++
		ENDIF
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitRappelSouth)
			iExits++
		ENDIF
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitRappelNorth)
			iExits++
		ENDIF
		IF iExits < 2
			PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING - Requiring Multiple Compound Exits.")	
			RETURN TRUE
		ENDIF		
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iAltVarsBitsetDialogueTrigger, ciAltVarsBS_IslandHeist_DialogueTriggers_Flag_RequiresOnlyMainGateCompoundExit)			
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitKeycardSouth)
		OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitKeycardNorth)
		OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitRappelSouth)
		OR FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitRappelNorth)
			PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING - Requires only the Main Gate to be an active Exit from the Compound.")	
			RETURN TRUE
		ENDIF		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING(INT iDialogue)
		
	IF NOT IS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_FLAGGED_TO_BE_BLOCKED(iDialogue)
		EXIT
	ENDIF
	
	IF NOT SHOULD_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_BE_BLOCKED(iDialogue)
		PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING - Flagged to be Allowed")	
		EXIT
	ENDIF
	
	PRINTLN("[LM][AltVarsSystem][Dialogue: ", iDialogue, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING - Flagged to be Blocked/Skipped")	
	
	FMMC_SET_LONG_BIT(iLocalDialogueForceSkip, iDialogue)

ENDPROC


				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Compound Entrance Used
// ##### Description: <DESCRIPTION>
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MC_ALT_VAR_ISLAND_HEIST_STARTING_WEAPON_COMPOUND_ENTRY_TYPE(INT iInventory)
	
	IF NOT FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT.iAltVarsBitsetGeneric, ciAltVarsBS_IslandHeist_Generic_Flag_CompoundEntryStartingWeapon)
		EXIT
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceVehicleFrontGate)
		g_FMMC_STRUCT.sPlayerWeaponInventories[iInventory].iStartWeapon = -1
		SET_BIT(g_FMMC_STRUCT.sPlayerWeaponInventories[iInventory].iInventoryBitset, ciFMMC_INVENTORY_BS_START_UNARMED)
		INT i
		FOR i = 0 TO FMMC_MAX_TEAMS-1
			SET_BIT(g_FMMC_STRUCT.sTeamData[i].iTeamBitSet, ciTEAM_BS_SET_STARTING_WEAPON_ON_STRAND)
		ENDFOR
		PRINTLN("[JT][AltVarsSystem][Inventory: ", iInventory, "] - PROCESS_MC_ALT_VAR_ISLAND_HEIST_STARTING_WEAPON_COMPOUND_ENTRY_TYPE - Setting start weapon to unarmed")
	ENDIF
	
ENDPROC

#ENDIF

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: (SERVER) AltVars Start up Process Server ------------------------------------------------------------------------------------------------------------------------
// ##### Description: This is called to decide certain variables before all the variations globals are applied to the mission globals. Done only by Server.		------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST()
	
	FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_SyncedData)	

	#IF FEATURE_HEIST_ISLAND 
	
	INT i
	
	SWITCH g_sHeistIslandConfig.eApproachVehicle
		CASE HIAV_SUBMARINE
			SWITCH g_sHeistIslandConfig.eInfiltrationPoint
				CASE HIIP_WEST_BEACH
					FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SubmarineWestBeach)
					PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - APPROACH - Setting Submarine West Beach")
				BREAK
				CASE HIIP_MAIN_DOCK
					FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SubmarineWestDocks)
					PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - APPROACH - Setting Submarine West Docks")
				BREAK
				CASE HIIP_NORTH_DOCK
					FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SubmarineEastDocks)
					PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - APPROACH - Setting Submarine East Docks")
				BREAK
				CASE HIIP_DRAINAGE_TUNNEL
					FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_DrainageTunnel)
					PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - APPROACH - Setting Drainage Tunnel")
				BREAK
			ENDSWITCH
		BREAK
		
		CASE HIAV_STRATEGIC_BOMBER
			SWITCH g_sHeistIslandConfig.eInfiltrationPoint
				CASE HIIP_PARACHUTING
					FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_StealthPlaneHALOJump)
					PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - APPROACH - Setting Stealth Chopper HALO Jump")
				BREAK
			ENDSWITCH
		BREAK
		
		CASE HIAV_SMUGGLER_PLANE
			SWITCH g_sHeistIslandConfig.eInfiltrationPoint
				CASE HIIP_AIRSTRIP
					FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SmugglerPlaneAirstrip)
					PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - APPROACH - Setting Smuggler Plane Airstrip")
				BREAK			
			ENDSWITCH
		BREAK
		
		CASE HIAV_STEALTH_HELICOPTER
			SWITCH g_sHeistIslandConfig.eInfiltrationPoint				
				CASE HIIP_NORTH_DROP_ZONE
					FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_StealthChopperNorthDropZone)
					PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - APPROACH - Setting Stealth Chopper North Drop")
				BREAK
				CASE HIIP_SOUTH_DROP_ZONE
					FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_StealthChopperSouthDropZone)
					PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - APPROACH - Setting Stealth Chopper South Drop")				
				BREAK
			ENDSWITCH
		BREAK
		
		CASE HIAV_PATROL_BOAT
			SWITCH g_sHeistIslandConfig.eInfiltrationPoint
				CASE HIIP_MAIN_DOCK
					FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_GunBoatMainDocks)
					PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - APPROACH - Setting Gun Boat Main Docks")
				BREAK
				CASE HIIP_NORTH_DOCK
					FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_GunBoatSmallDocks)
					PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - APPROACH - Setting Gun Boat Small Docks")
				BREAK
			ENDSWITCH
		BREAK
		
		CASE HIAV_SMUGGLER_BOAT		
			SWITCH g_sHeistIslandConfig.eInfiltrationPoint
				CASE HIIP_MAIN_DOCK
					FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SmugglerBoatMainDocks)
					PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - APPROACH - Setting Smuggler Boat Main Docks")
				BREAK
				CASE HIIP_NORTH_DOCK
					FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ApproachSelected_SmugglerBoaSmallDocks)
					PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - APPROACH - Setting Smugger Boat Small Docks")
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	SWITCH g_sHeistIslandConfig.eWeaponLoadout
		CASE HIWL_SHOTGUN
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponLoadout_1)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - LOADOUT - Setting Weapon Loadout 1")
		BREAK
		CASE HIWL_RIFLE
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponLoadout_2)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - LOADOUT - Setting Weapon Loadout 2")
		BREAK
		CASE HIWL_SNIPER
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponLoadout_3)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - LOADOUT - Setting Weapon Loadout 3")
		BREAK
		CASE HIWL_MKII_SMG
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponLoadout_4)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - LOADOUT - Setting Weapon Loadout 4")
		BREAK
		CASE HIWL_MKII_RIFLE
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponLoadout_5)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - LOADOUT - Setting Weapon Loadout 5")
		BREAK
	ENDSWITCH
	
	SWITCH g_sHeistIslandConfig.eTimeOfDay
		CASE HIT_DAY
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_TimeOfDayOverrideDay)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - TIME OF DAY - Setting Day Time")
		BREAK
		CASE HIT_NIGHT
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_TimeOfDayOverrideNight)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - TIME OF DAY - Setting Night Time")
		BREAK
	ENDSWITCH
	
	SWITCH g_sHeistIslandConfig.eEnemyWeaponDisruptionLevel
		CASE HIDL_LOW
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemyWeaponDisruption_Low)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - WEAPON DIRUPTION - Setting Low Disruption")
		BREAK		
		CASE HIDL_MEDIUM
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemyWeaponDisruption_Medium)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - WEAPON DIRUPTION - Setting Medium Disruption")
		BREAK
		CASE HIDL_HIGH
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemyWeaponDisruption_High)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - WEAPON DIRUPTION - Setting High Disruption")
		BREAK
	ENDSWITCH
	
	SWITCH g_sHeistIslandConfig.eEnemyArmourDisruptionLevel
		CASE HIDL_LOW
		CASE HIDL_MEDIUM
		CASE HIDL_HIGH
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemiesArmourDisruption)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - ARMOUR DIRUPTION - Setting Armour Disrupted")
		BREAK
	ENDSWITCH
	
	IF IS_BIT_SET(g_sHeistIslandConfig.iBitSet, HEIST_ISLAND_CONFIG_BITSET__POISON_WATER_SUPPLY)
		FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_PoisonedWaterSupply)
		PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - POISONED WATER - Setting Poisoned Water Supply.")		
	ENDIF
	
	SWITCH g_sHeistIslandConfig.eEnemyHelicopterDisruptionLevel
		CASE HIDL_LOW
		CASE HIDL_MEDIUM
		CASE HIDL_HIGH
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EnemiesHelicoptorDisruption)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - HELI DISRUPTION - Setting Enemy Heli Disrupted.")		
		BREAK
	ENDSWITCH
		
	IF g_sHeistIslandConfig.bUseSuppressors
		FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponSuppressors)
		PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - SUPPRESSORS - Setting Weapon Suppressors.")		
	ENDIF
	
	IF IS_BIT_SET(g_sHeistIslandConfig.iBitSet, HEIST_ISLAND_CONFIG_BITSET__GOLDEN_GUN_DRAW_KEY)
		FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_GoldenGunDrawKey)
		PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - GOLDEN GUN - Setting Golden Gun Draw Key.")		
	ENDIF
	
	FOR i = 0 TO 3
		IF IS_BIT_SET(g_sHeistIslandConfig.iBitSet, HEIST_ISLAND_CONFIG_BITSET__GRAPPEL_EQUIPMENT_G1_SCOPED + i)
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_GrappelEquipmentScoped_1 + i)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - SCOPED - Setting Grappel Equipment ", i, " as Scoped.")		
		ENDIF
	ENDFOR
	
	FOR i = 0 TO 3
		IF IS_BIT_SET(g_sHeistIslandConfig.iBitSet, HEIST_ISLAND_CONFIG_BITSET__UNIFORM_G1_SCOPED + i)		
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_UniformScoped_1 + i)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - SCOPED - Setting Uniform ", i, " as scoped.")		
		ENDIF
	ENDFOR
	
	FOR i = 0 TO 3
		IF IS_BIT_SET(g_sHeistIslandConfig.iBitSet, HEIST_ISLAND_CONFIG_BITSET__BOLT_CUTTER_G1_SCOPED + i)
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_BoltCutterScoped_1 + i)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - SCOPED - Setting Bolt Cutter ", i, " as scoped.")		
		ENDIF
	ENDFOR
	
	FOR i = 0 TO 24
		IF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iCashIslandLocationsBitSet, i)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - LOOT - Island Loot is Cash ", i)
			
		ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iWeedIslandLocationsBitSet, i)			
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - LOOT - Island Loot is Weed ", i)
			
		ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iCokeIslandLocationsBitset, i)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - LOOT - Island Loot is Coke ", i)
			
		ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iGoldIslandLocationsBitSet, i)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - LOOT - Island Loot is Gold ", i)
		ENDIF
	ENDFOR
	
	FOR i = 0 TO 24
		IF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iCashIslandLocationsBitSetScoped, i)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - SCOPED - Island Loot is Cash ", i, " and is scoped.")
																																					 
		ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iWeedIslandLocationsBitSetScoped, i)												 
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - SCOPED - Island Loot is Weed ", i, " and is scoped.")
																																					 
		ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iCokeIslandLocationsBitsetScoped, i)												 
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - SCOPED - Island Loot is Coke ", i, " and is scoped.")
																																					 
		ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iGoldIslandLocationsBitSetScoped, i)												 
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - SCOPED - Island Loot is Gold ", i, " and is scoped.")
		ENDIF
	ENDFOR
	
	FOR i = 0 TO 8
		IF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iCashCompoundLocationsBitSet, i)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - LOOT - Compound Loot is Cash ", i)
			
		ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iWeedCompoundLocationsBitSet, i)			
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - LOOT - Compound Loot is Weed ", i)
			
		ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iCokeCompoundLocationsBitset, i)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - LOOT - Compound Loot is Coke ", i)
			
		ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iGoldCompoundLocationsBitSet, i)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - LOOT - Compound Loot is Gold ", i)
		ENDIF
	ENDFOR
	
	FOR i = 0 TO 8
		IF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iCashCompoundLocationsBitSetScoped, i)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - SCOPED - Compound Loot is Cash ", i, " and is scoped.")
																																					 
		ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iWeedCompoundLocationsBitSetScoped, i)												 
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - SCOPED - Compound Loot is Weed ", i, " and is scoped.")
																																					 
		ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iCokeCompoundLocationsBitsetScoped, i)												 
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - SCOPED - Compound Loot is Coke ", i, " and is scoped.")
																																					 
		ELIF IS_BIT_SET(g_sHeistIslandConfig.sAdditionalLootData.iGoldCompoundLocationsBitSetScoped, i)												 
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - SCOPED - Compound Loot is Gold ", i, " and is scoped.")
		ENDIF
	ENDFOR
	
	IF IS_BIT_SET(g_sHeistIslandConfig.iBitSet, HEIST_ISLAND_CONFIG_BITSET__POWER_STATION_SCOPED)
		FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_PowerStationScoped)
		PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - SCOPED - Setting Power Station.")		
	ENDIF
	
	IF IS_BIT_SET(g_sHeistIslandConfig.iBitSet, HEIST_ISLAND_CONFIG_BITSET__TROJAN_VEHICLE_SCOPED)
		FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_TrojanVehicleScoped)
		PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - SCOPED - Setting Trojan Vehicle.")		
	ENDIF
	
	IF IS_BIT_SET(g_sHeistIslandConfig.iBitSet, HEIST_ISLAND_CONFIG_BITSET__CONTROL_TOWER_SCOPED)
		FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_ControlTowerScoped)
		PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - SCOPED - Setting Control Tower.")		
	ENDIF
		
	IF IS_BIT_SET(g_sHeistIslandConfig.iEntrancesScopedBitSet, ENUM_TO_INT(HICE_MAIN_GATE))
		FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_MainGate)
		PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - ENTRANCE SCOPED - Setting Main Gate Scoped.")
	ENDIF
	IF IS_BIT_SET(g_sHeistIslandConfig.iEntrancesScopedBitSet, ENUM_TO_INT(HICE_SOUTH_WALL))
		FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_RappelSouth)
		PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - ENTRANCE SCOPED - Setting Rappel south (south wall) Scoped.")
	ENDIF
	IF IS_BIT_SET(g_sHeistIslandConfig.iEntrancesScopedBitSet, ENUM_TO_INT(HICE_SOUTH_SIDE_GATE))
		FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_SideGateSouth)
		PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - ENTRANCE SCOPED - Setting Side Gate south (south gate) Scoped.")
	ENDIF
	IF IS_BIT_SET(g_sHeistIslandConfig.iEntrancesScopedBitSet, ENUM_TO_INT(HICE_NORTH_WALL))
		FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_RappelNorth)
		PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - ENTRANCE SCOPED - Setting Rappel North (North Wall) Scoped.")
	ENDIF
	IF IS_BIT_SET(g_sHeistIslandConfig.iEntrancesScopedBitSet, ENUM_TO_INT(HICE_NORTH_SIDE_GATE))
		FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_SideGateNorth)
		PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - ENTRANCE SCOPED - Setting Side Gate North (North gate) Scoped.")
	ENDIF
	IF IS_BIT_SET(g_sHeistIslandConfig.iEntrancesScopedBitSet, ENUM_TO_INT(HICE_TUNNEL))
		FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EntranceScoped_Tunnel)
		PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - ENTRANCE SCOPED - Setting Tunnel Scoped.")
	ENDIF
	
	SWITCH g_sHeistIslandConfig.ePrimaryTarget
		CASE HIPT_MADRAZO_FILES
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_MadrazoFiles)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - TARGET - Setting Madrazo Files")
		BREAK		
		CASE HIPT_BEARER_BONDS
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_Bonds)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - TARGET - Setting Bonds")
		BREAK
		CASE HIPT_TEQUILA
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_Tequila)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - TARGET - Setting Tequila")
		BREAK
		CASE HIPT_PEARL_NECKLACE
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_PearlNecklace)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - TARGET - Setting Pearl Necklace")
		BREAK
		CASE HIPT_SAPPHIRE_PANTHER_STATUE
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_SapphirePantherStatue)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - TARGET - Setting Panther Statue")
		BREAK
		CASE HIPT_PINK_DIAMOND
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_Target_PinkDiamond)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - TARGET - Setting Pink Diamond")
		BREAK
	ENDSWITCH
	
	IF IS_BIT_SET(g_sHeistIslandConfig.iAbilitiesBitset, HEIST_ISLAND_CONFIG_ABILITIES_BITSET__CARPET_BOMB)
		FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CarpetBomb)
		PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - ABILITIES - Setting Carpet Bomb Enabled.")
	ENDIF
	
	IF IS_BIT_SET(g_sHeistIslandConfig.iAbilitiesBitset, HEIST_ISLAND_CONFIG_ABILITIES_BITSET__SNIPER)
		FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_AbilitiesSniper)
		PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - ABILITIES - Setting Sniper Enabled.")
	ENDIF	
	
	IF IS_BIT_SET(g_sHeistIslandConfig.iAbilitiesBitset, HEIST_ISLAND_CONFIG_ABILITIES_BITSET__HELI_BACKUP)
		FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_AbilitiesHeliBackup)
		PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - ABILITIES - Setting Heli Backup Enabled.")
	ENDIF	
	
	IF IS_BIT_SET(g_sHeistIslandConfig.iAbilitiesBitset, HEIST_ISLAND_CONFIG_ABILITIES_BITSET__RECON_DRONE)
		FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_AbilitiesReconnaissanceDrone)
		PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - ABILITIES - Setting Recon Drone Enabled.")
	ENDIF
	
	IF IS_BIT_SET(g_sHeistIslandConfig.iAbilitiesBitset, HEIST_ISLAND_CONFIG_ABILITIES_BITSET__HEAVY_LOADOUT)
		FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_AbilitiesHeavyLoadout)
		PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - ABILITIES - Setting Heavy Loadout Enabled.")
	ENDIF
	
	IF IS_BIT_SET(g_sHeistIslandConfig.iAbilitiesBitset, HEIST_ISLAND_CONFIG_ABILITIES_BITSET__WEAPON_STASH)
		FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_WeaponStash)
		PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - ABILITIES - Setting Weapon Stash Enabled.")
	ENDIF

	SWITCH g_sHeistIslandConfig.eTrojanVehicleLocation
		CASE HITVL_AIRSTRIP
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_TrojanVehicleLocation_1)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - TROJAN LOCATION - Setting Trojan Vehicle Location to 1.")
		BREAK
		CASE HITVL_NORTH_DOCK
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_TrojanVehicleLocation_2)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - TROJAN LOCATION - Setting Trojan Vehicle Location to 2.")
		BREAK
		CASE HITVL_DRUG_PROCESSING
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_TrojanVehicleLocation_3)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - TROJAN LOCATION - Setting Trojan Vehicle Location to 3.")
		BREAK
		CASE HITVL_MAIN_DOCK
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_TrojanVehicleLocation_4)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - TROJAN LOCATION - Setting Trojan Vehicle Location to 4.")
		BREAK
		CASE HITVL_COMPOUND
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_TrojanVehicleLocation_5)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - TROJAN LOCATION - Setting Trojan Vehicle Location to 5.")			// Fifth needs adding here as there are now 5 total
		BREAK
	ENDSWITCH
		
	SWITCH g_sHeistIslandConfig.eEscapePoint
		CASE HIEP_AIRSTRIP
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteAirstrip)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - ESCAPE ROUTE - Airstrip has been selected.")
		BREAK
		CASE HIEP_MAIN_DOCK
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteMainDocks)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - ESCAPE ROUTE - Main Docks has been selected.")
		BREAK
		CASE HIEP_NORTH_DOCK
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteSmallDocks)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - ESCAPE ROUTE - North Docks has been selected.")
		BREAK
		CASE HIEP_SUBMARINE
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_EscapeRouteSubmarine)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - ESCAPE ROUTE - Submarine has been selected.")
		BREAK
	ENDSWITCH	
	
	IF g_sHeistIslandConfig.bAcetyleneTorchAcquired
		FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_HasAcetyleneTorch)
		PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - ACETYLENE TORCH - Acquired Torch for Mission.")		
	ENDIF
	
	IF g_sHeistIslandConfig.bDemolitionChargesAcquired
		FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_DemolitionCharges)
		PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - DEMOLITION CHARGES - Acquired Charges for Mission.")		
	ENDIF
	
	//Continuity Based Alt Vars
	IF g_FMMC_STRUCT.eContinuityContentSpecificType = FMMC_MISSION_CONTINUITY_CONTENT_SPECIFIC_TYPE_ISLAND_HEIST
		IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_EXPLOSIVES_FRONT_GATE)
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceExplosivesFrontGate)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - COMPOUND ENTRANCE - Explosives Front Gate.")
		ENDIF
			
		IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_KEYCARD_SOUTH)
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceKeycardSouth)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - COMPOUND ENTRANCE - Keycard South.")
		ENDIF
			
		IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_KEYCARD_NORTH)
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceKeycardNorth)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - COMPOUND ENTRANCE - Keycard North.")
		ENDIF
			
		IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_RAPPEL_SOUTH)
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceRappelSouth)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - COMPOUND ENTRANCE - Rappel South.")
		ENDIF
			
		IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_RAPPEL_NORTH)
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceRappelNorth)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - COMPOUND ENTRANCE - Rappel North.")
		ENDIF
			
		IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_VEHICLE_FRONT_GATE)
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceVehicleFrontGate)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - COMPOUND ENTRANCE - Front Gate.")
		ENDIF
			
		IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_ENTRANCE_SEWER_GRATE)
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundEntranceSewerGrate)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - COMPOUND ENTRANCE - Sewer Grate.")
		ENDIF
			
		IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_EXIT_FRONT_GATE)
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitFrontGate)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - COMPOUND EXIT - Front Gate.")
		ENDIF
																															  
		IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_EXIT_KEYCARD_SOUTH)
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitKeycardSouth)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - COMPOUND EXIT - Keycard South.")
		ENDIF
																															  
		IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_EXIT_KEYCARD_NORTH)
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitKeycardNorth)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - COMPOUND EXIT - Keycard North.")
		ENDIF
																															  
		IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_EXIT_RAPPEL_SOUTH)
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitRappelSouth)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - COMPOUND EXIT - Rappel South.")
		ENDIF
																															  
		IF IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, ciFMMC_MISSION_CONTINUITY_ISLAND_HEIST_ID_EXIT_RAPPEL_NORTH)
			FMMC_SET_LONG_BIT(MC_serverBD_4.iAltVarsBS, ciMC_ALT_VAR_ISLAND_HEIST_BS_CompoundExitRappelNorth)
			PRINTLN("[LM][AltVarsSystem][Server] - PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST - COMPOUND EXIT - Rappel North.")
		ENDIF

	ENDIF
	
	#ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: (CLIENT) AltVars Main Process / Loops ---------------------------------------------------------------------------------------------------------------------------
// ##### Description: All the global overrides and trigger checks are carried out here.											 								------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#IF FEATURE_HEIST_ISLAND 
	
PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_GLOBALS_ISLAND_HEIST()
	INT i, ii, iii

	PROCESS_MC_ALT_VAR_ISLAND_HEIST_FORCE_TIME_OF_DAY()		
			
	PROCESS_MC_ALT_VAR_ISLAND_HEIST_FORCE_INVENTORY_INDEX_PLANNING_BOARD_SELECTION()
	
	FOR i = 0 TO FMMC_MAX_RULES-1
		FOR ii = 0 TO FMMC_MAX_TEAMS-1
			PROCESS_MC_ALT_VAR_ISLAND_HEIST_HEAVY_LOADOUT_ABILITY_ENABLED(i, ii)
			PROCESS_MC_ALT_VAR_ISLAND_HEIST_RECON_DRONE_ABILITY_ENABLED(i, ii)
			PROCESS_MC_ALT_VAR_ISLAND_HEIST_HELI_BACKUP_ABILITY_ENABLED(i, ii)
			PROCESS_MC_ALT_VAR_ISLAND_HEIST_SNIPER_ABILITY_ENABLED(i, ii)
			PROCESS_MC_ALT_VAR_ISLAND_HEIST_CARPET_BOMB_ABILITY_ENABLED(i, ii)
			PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_OBJECTIVE_TEXT_FOR_TARGET(i, ii)
			PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_OBJECTIVE_TEXT_FOR_TARGET_ALT(i, ii)
			PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_OBJECTIVE_TEXT_FOR_EXIT(i, ii)
			PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_INTRO_CUTSCENE_BASED_ON_APPROACH(i, ii)
			PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_HELICOPTER_DISRUPTION(i, ii)
			PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_PED_HEALTH_POISONED_WATER(i, ii)
			PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_PED_ACCURACY_POISONED_WATER(i, ii)
			PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_PED_ARMOUR_DISRUPTED(i, ii)
		ENDFOR
	ENDFOR
		
	FOR i = 0 TO FMMC_MAX_INVENTORIES-1
		FOR ii = 0 TO FMMC_MAX_INVENTORY_WEAPONS-1 
			FOR iii = 0 TO FMMC_MAX_WEAPON_COMPONENTS-1
				PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_LOADOUT_FORCED_SUPPRESSOR_APPROACH_TYPE(i, ii, iii)	
			ENDFOR
		ENDFOR
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_STARTING_WEAPON_COMPOUND_ENTRY_TYPE(i)
	ENDFOR
	
	PROCESS_MC_ALT_VAR_ISLAND_HEIST_GANG_CHASE_WEAPON_DISRUPTION()
	
	PROCESS_MC_ALT_VAR_ISLAND_HEIST_OVERRIDE_END_CUTSCENE_WITH_PLAYTHROUGH()
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_PEDS_ISLAND_HEIST()
	INT i
	FOR i = 0 TO FMMC_MAX_PEDS-1	
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_HEALTH_ARMOUR_DISRUPTED(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_HEALTH_POISONED_WATER(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_ACCURACY_POISONED_WATER(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_POSITION_FORCED_BY_APPROACH(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_SPAWN_BLOCKING_FLAG_CHECKS(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_POSITION_FORCED_BY_COMPOUND_ENTRANCE_TAKEN(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_WEAPONS_DISRUPTED(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_PED_SIGHT_RANGE_TOD_AND_WEATHER(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_VEHICLES_ISLAND_HEIST()
	INT i
	FOR i = 0 TO FMMC_MAX_VEHICLES-1	
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEHICLE_SCOPED_TROJAN(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEH_POSITION_FORCED_BY_APPROACH(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEHICLE_SPAWN_BLOCKING_FLAG_CHECKS(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEH_POSITION_FORCED_BY_COMPOUND_ENTRANCE_TAKEN(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEH_MODEL_SWAP(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_VEH_BLIP_ON_EXIT_CHOSEN(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_OBJECTS_ISLAND_HEIST()
	INT i
	FOR i = 0 TO FMMC_MAX_NUM_OBJECTS-1
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_OBJ_POSITION_FORCED_BY_APPROACH(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_OBJECT_SPAWN_BLOCKING_FLAG_CHECKS(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_OBJ_POSITION_FORCED_BY_COMPOUND_ENTRANCE_TAKEN(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_ACETYLENE_TORCH_OBJECT(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_DYNOPROPS_ISLAND_HEIST()
	INT i
	FOR i = 0 TO FMMC_MAX_NUM_DYNOPROPS-1
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_DYNO_PROP_SPAWN_BLOCKING_FLAG_CHECKS(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_PROPS_ISLAND_HEIST()
	INT i
	FOR i = 0 TO GET_FMMC_MAX_NUM_PROPS()-1
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_PROP_SPAWN_BLOCKING_FLAG_CHECKS(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_LOCATIONS_ISLAND_HEIST()		
	INT i
	FOR i = 0 TO FMMC_MAX_GO_TO_LOCATIONS-1			
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_LOCATION_SCOPED_SIDE_GATE_NORTH(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_LOCATION_SCOPED_SIDE_GATE_SOUTH(i)	
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_LOCATION_SCOPED_RAPPEL_POINT_NORTH(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_LOCATION_SCOPED_RAPPEL_POINT_SOUTH(i)		
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_LOCATION_SCOPED_UNDERWATER_TUNNEL(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_ACETYLENE_TORCH_LOCATION(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_DEMOLITION_CHARGES_LOCATION(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_DIALOGUE_TRIGGERS_ISLAND_HEIST()
	INT i
	FOR i = 0 TO FMMC_MAX_DIALOGUES-1
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_APPEND_IF_SCOPED(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_BLOCK_FROM_PLAYING(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_DIALOGUE_TRIGGER_APPEND_FOR_TARGET_TYPE(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PTFX_ISLAND_HEIST()	
	/*INT i
	FOR i = 0 TO g_FMMC_STRUCT.iNumPlacedPTFX-1
		IF i < FMMC_MAX_PLACED_PTFX
			
		ENDIF
	ENDFOR	*/
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_ZONES_ISLAND_HEIST()
	/*INT i
	FOR i = 0 TO FMMC_MAX_NUM_ZONES - 1			
		
	ENDFOR	*/		
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_DUMMY_BLIPS_ISLAND_HEIST()
	/*INT i
	FOR i = 0 TO FMMC_MAX_DUMMY_BLIPS - 1
	
	ENDFOR*/
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_WARP_PORTALS_ISLAND_HEIST()
	/*INT i
	FOR i = 0 TO FMMC_MAX_WARP_PORTALS - 1			
		
	ENDFOR	*/		
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_PICKUPS_ISLAND_HEIST()
	INT i
	FOR i = 0 TO FMMC_MAX_WEAPONS-1
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SPAWN_GEAR_GROUPS(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SCOPED_GRAPPEL_EQUIPMENT(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SCOPED_BOLT_CUTTERS(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_WEAPON_SMUGGLED_WEAPON_INVENTORY(i)		
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_SPAWN_POINTS_ISLAND_HEIST()
	INT i, ii
	FOR ii = 0 TO FMMC_MAX_TEAMS-1
		FOR i = 0 TO FMMC_MAX_TEAMSPAWNPOINTS-1
			PROCESS_MC_ALT_VAR_ISLAND_HEIST_TEAM_SPAWN_POINT_SPAWN_BLOCKING_FLAG_CHECKS(i, ii)
		ENDFOR
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_DOORS_ISLAND_HEIST()
	/*INT i
	FOR i = 0 TO FMMC_MAX_NUM_DOORS-1
		
	ENDFOR*/
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_INTERACTABLES_ISLAND_HEIST()
	INT i
	FOR i = 0 TO FMMC_MAX_NUM_INTERACTABLES-1
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SCOPED_POWER_STATION(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SCOPED_CONTROL_TOWER(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_POSITION_FORCED_BY_APPROACH(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SPAWN_BLOCKING_FLAG_CHECKS(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_POSITION_FORCED_BY_COMPOUND_ENTRANCE_TAKEN(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_GOLDEN_GUN_DRAW_KEY(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SPAWN_GEAR_GROUPS(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_REPOSITION_GEAR(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_COMPOUND(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_ISLAND(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_PAINTING(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SCOPED_UNIFORM(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_ACETYLENE_TORCH(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_ISLAND_SCOPED(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_SECONDARY_LOOT_COMPOUND_SCOPED(i)
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_INTERACTABLE_PAINTING_SCOPED(i)
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_SPAWN_GROUPS_ISLAND_HEIST()
	INT i, ii

	FOR i = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
		PROCESS_MC_ALT_VAR_ISLAND_HEIST_SPAWN_GROUP_ACTIVATION(i)
		FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
			PROCESS_MC_ALT_VAR_ISLAND_HEIST_SUB_SPAWN_GROUP_ACTIVATION(i, ii)
		ENDFOR
	ENDFOR
ENDPROC 

PROC PROCESS_APPLY_ALT_VAR_PRE_SPAWN_GROUP_SETTINGS_ISLAND_HEIST()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_SPAWN_GROUPS_ISLAND_HEIST()
	
ENDPROC

#ENDIF

PROC PROCESS_APPLY_ALT_VAR_SETTINGS_ISLAND_HEIST()
		
	#IF FEATURE_HEIST_ISLAND 
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1
		PRINTLN("[LM][AltVarsSystem] - ###########################################################################")	
		PRINTLN("[LM][AltVarsSystem] - ###### Something has gone seriously wrong. We have no valid boss INT ######")		
		PRINTLN("[LM][AltVarsSystem] - ###########################################################################")		
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_NO_GANGBOSS, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "No Gang Boss, value is -1. This will cause a lot of ALT VARS To function improperly.")
		#ENDIF
	ELSE
		PRINTLN("[LM][AltVarsSystem] - Boss INT: ", GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT())
	ENDIF
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_GLOBALS_ISLAND_HEIST()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_PEDS_ISLAND_HEIST()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_OBJECTS_ISLAND_HEIST()
		
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_VEHICLES_ISLAND_HEIST()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_DYNOPROPS_ISLAND_HEIST()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_PROPS_ISLAND_HEIST()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_LOCATIONS_ISLAND_HEIST()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PTFX_ISLAND_HEIST()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_DIALOGUE_TRIGGERS_ISLAND_HEIST()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_ZONES_ISLAND_HEIST()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_DUMMY_BLIPS_ISLAND_HEIST()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_WARP_PORTALS_ISLAND_HEIST()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_PICKUPS_ISLAND_HEIST()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_SPAWN_POINTS_ISLAND_HEIST()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_DOORS_ISLAND_HEIST()
	
	PROCESS_APPLY_ALT_VAR_SETTINGS_FOR_PLACED_INTERACTABLES_ISLAND_HEIST()
	
	#ENDIF
	
ENDPROC
